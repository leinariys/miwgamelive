package game.script.newmarket.store;

import game.network.message.externalizable.aCE;
import game.network.message.serializable.C1556Wo;
import game.network.message.serializable.C3438ra;
import game.script.Actor;
import game.script.TaikodomObject;
import game.script.TaskletImpl;
import game.script.item.Item;
import game.script.item.ItemType;
import game.script.newmarket.BuyOrder;
import game.script.newmarket.CommercialOrder;
import game.script.newmarket.ListContainer;
import game.script.newmarket.SellOrder;
import game.script.player.Player;
import game.script.ship.Station;
import logic.baa.*;
import logic.data.mbean.C0432G;
import logic.data.mbean.C1095Q;
import logic.data.mbean.C1539Wc;
import logic.data.mbean.C2315dv;
import logic.res.code.C5640aRc;
import logic.res.code.C5663aRz;
import logic.res.code.C6494any;
import logic.res.html.C2491fm;
import p001a.*;

import java.util.*;

@C5511aMd
@C6485anp
/* renamed from: a.hf */
/* compiled from: a */
public class Store extends TaikodomObject implements C0468GU, C0847MM, C1616Xf {

    /* renamed from: Pf */
    public static final C2491fm f7989Pf = null;
    public static final C2491fm _f_dispose_0020_0028_0029V = null;
    public static final C2491fm _f_onResurrect_0020_0028_0029V = null;
    public static final int _m_fieldCount = 0;
    public static final C5663aRz[] _m_fields = null;
    public static final int _m_methodCount = 0;
    public static final C2491fm[] _m_methods = null;
    public static final C2491fm bHl = null;
    /* renamed from: bL */
    public static final C5663aRz f7991bL = null;
    /* renamed from: bM */
    public static final C5663aRz f7992bM = null;
    /* renamed from: bN */
    public static final C2491fm f7993bN = null;
    /* renamed from: bO */
    public static final C2491fm f7994bO = null;
    /* renamed from: bP */
    public static final C2491fm f7995bP = null;
    /* renamed from: bQ */
    public static final C2491fm f7996bQ = null;
    public static final C2491fm bkF = null;
    public static final C5663aRz dNL = null;
    public static final C2491fm dNZ = null;
    public static final C2491fm dOa = null;
    public static final C5663aRz ffW = null;
    public static final C2491fm fgg = null;
    public static final C2491fm fgq = null;
    public static final C5663aRz jbH = null;
    public static final C5663aRz jbI = null;
    public static final C5663aRz jbJ = null;
    public static final C5663aRz jbK = null;
    public static final C5663aRz jbL = null;
    public static final C5663aRz jbM = null;
    public static final C5663aRz jbN = null;
    public static final C5663aRz jbO = null;
    public static final C5663aRz jbP = null;
    public static final C5663aRz jbQ = null;
    public static final C2491fm jbS = null;
    public static final C2491fm jbT = null;
    public static final C2491fm jbU = null;
    public static final C2491fm jbV = null;
    public static final C2491fm jbW = null;
    public static final C2491fm jbX = null;
    public static final C2491fm jbY = null;
    public static final C2491fm jbZ = null;
    public static final C2491fm jca = null;
    public static final C2491fm jcb = null;
    public static final C2491fm jcc = null;
    public static final C2491fm jcd = null;
    public static final C2491fm jce = null;
    public static final C2491fm jcf = null;
    public static final C2491fm jcg = null;
    public static final C2491fm jch = null;
    public static final C2491fm jci = null;
    public static final C2491fm jcj = null;
    public static final C2491fm jck = null;
    public static final C2491fm jcl = null;
    public static final C2491fm jcm = null;
    public static final C2491fm jcn = null;
    public static final C2491fm jco = null;
    public static final C2491fm jcp = null;
    public static final C2491fm jcq = null;
    public static final long serialVersionUID = 0;
    public static C6494any ___iScriptClass;
    @C0064Am(aul = "1200946c5cdb83247e2852418df88e2e", aum = 8)

    /* renamed from: bK */
    private static UUID f7990bK;
    @C0064Am(aul = "44ea959afaf142a1c8a7cc61e03f29e7", aum = 7)
    @C5566aOg
    private static boolean ewA;
    @C0064Am(aul = "15d0572fcca3884681ef0e7711181b6b", aum = 10)
    @C5566aOg
    private static C1556Wo<C0847MM, ListContainer<BuyOrder>> ewB;
    @C0064Am(aul = "36ec59d7360195d01c95b322a140ab37", aum = 11)
    private static ListContainer<BuyOrder> ewC;
    @C0064Am(aul = "e3fce79e603982e4e6e72fe99bc61d0f", aum = 12)
    private static ListContainer<SellOrder> ewD;
    @C0064Am(aul = "a09cb4ed9c01fc772a2c3459fbdfe8b7", aum = 13)
    private static Station ewE;
    @C0064Am(aul = "455161cdf4ff8301fb616a7645e0cc1a", aum = 0)
    private static boolean ewt;
    @C0064Am(aul = "d8f0b8070b41be2abf7058413559ea0a", aum = 1)
    private static C3438ra<ItemType> ewu;
    @C0064Am(aul = "03ccaaacd93698e4e05202082e631cd1", aum = 2)
    private static C3438ra<ItemType> ewv;
    @C0064Am(aul = "221630fc0dd5f7462ea163868ad494e4", aum = 3)
    private static C3438ra<ItemType> eww;
    @C5566aOg
    @C0064Am(aul = "a31678d86f84e93031ebe20a941e1214", aum = 4)
    @C1020Ot(drz = C1020Ot.C1021a.NOT_PERSISTED)
    private static C1556Wo<ItemType, AnyBuyInfo> ewx;
    @C0064Am(aul = "55750e07dba7d621aa8689b9c89376e0", aum = 5)
    @C5566aOg
    private static C1556Wo<ItemType, PostponedOrderTasklet> ewy;
    @C0064Am(aul = "c434e18426030733e82e928d1fadf0e6", aum = 6)
    @C5566aOg
    private static boolean ewz;
    @C0064Am(aul = "dbee64527c885d559cf2053031f2669d", aum = 9)
    private static String handle;
    private static /* synthetic */ int[] jbR;

    static {
        m32767V();
    }

    public Store() {
        super((C5540aNg) null);
        super.mo10S();
    }

    public Store(C5540aNg ang) {
        super(ang);
    }

    /* renamed from: V */
    static void m32767V() {
        serialVersionUID = (long) 1;
        _m_fieldCount = TaikodomObject._m_fieldCount + 14;
        _m_methodCount = TaikodomObject._m_methodCount + 38;
        int i = TaikodomObject._m_fieldCount;
        C5663aRz[] arzArr = new C5663aRz[(i + 14)];
        C5663aRz b = C5640aRc.m17844b(Store.class, "455161cdf4ff8301fb616a7645e0cc1a", i);
        jbH = b;
        arzArr[i] = b;
        int i2 = i + 1;
        C5663aRz b2 = C5640aRc.m17844b(Store.class, "d8f0b8070b41be2abf7058413559ea0a", i2);
        jbI = b2;
        arzArr[i2] = b2;
        int i3 = i2 + 1;
        C5663aRz b3 = C5640aRc.m17844b(Store.class, "03ccaaacd93698e4e05202082e631cd1", i3);
        jbJ = b3;
        arzArr[i3] = b3;
        int i4 = i3 + 1;
        C5663aRz b4 = C5640aRc.m17844b(Store.class, "221630fc0dd5f7462ea163868ad494e4", i4);
        jbK = b4;
        arzArr[i4] = b4;
        int i5 = i4 + 1;
        C5663aRz b5 = C5640aRc.m17844b(Store.class, "a31678d86f84e93031ebe20a941e1214", i5);
        jbL = b5;
        arzArr[i5] = b5;
        int i6 = i5 + 1;
        C5663aRz b6 = C5640aRc.m17844b(Store.class, "55750e07dba7d621aa8689b9c89376e0", i6);
        jbM = b6;
        arzArr[i6] = b6;
        int i7 = i6 + 1;
        C5663aRz b7 = C5640aRc.m17844b(Store.class, "c434e18426030733e82e928d1fadf0e6", i7);
        jbN = b7;
        arzArr[i7] = b7;
        int i8 = i7 + 1;
        C5663aRz b8 = C5640aRc.m17844b(Store.class, "44ea959afaf142a1c8a7cc61e03f29e7", i8);
        jbO = b8;
        arzArr[i8] = b8;
        int i9 = i8 + 1;
        C5663aRz b9 = C5640aRc.m17844b(Store.class, "1200946c5cdb83247e2852418df88e2e", i9);
        f7991bL = b9;
        arzArr[i9] = b9;
        int i10 = i9 + 1;
        C5663aRz b10 = C5640aRc.m17844b(Store.class, "dbee64527c885d559cf2053031f2669d", i10);
        f7992bM = b10;
        arzArr[i10] = b10;
        int i11 = i10 + 1;
        C5663aRz b11 = C5640aRc.m17844b(Store.class, "15d0572fcca3884681ef0e7711181b6b", i11);
        jbP = b11;
        arzArr[i11] = b11;
        int i12 = i11 + 1;
        C5663aRz b12 = C5640aRc.m17844b(Store.class, "36ec59d7360195d01c95b322a140ab37", i12);
        jbQ = b12;
        arzArr[i12] = b12;
        int i13 = i12 + 1;
        C5663aRz b13 = C5640aRc.m17844b(Store.class, "e3fce79e603982e4e6e72fe99bc61d0f", i13);
        dNL = b13;
        arzArr[i13] = b13;
        int i14 = i13 + 1;
        C5663aRz b14 = C5640aRc.m17844b(Store.class, "a09cb4ed9c01fc772a2c3459fbdfe8b7", i14);
        ffW = b14;
        arzArr[i14] = b14;
        int i15 = i14 + 1;
        _m_fields = arzArr;
        C1634Xv.m11725a((Object[]) TaikodomObject._m_fields, (Object[]) _m_fields);
        int i16 = TaikodomObject._m_methodCount;
        C2491fm[] fmVarArr = new C2491fm[(i16 + 38)];
        C2491fm a = C4105zY.m41624a(Store.class, "7decb6a01bb0353116e1ee7bc7eafc1a", i16);
        f7993bN = a;
        fmVarArr[i16] = a;
        int i17 = i16 + 1;
        C2491fm a2 = C4105zY.m41624a(Store.class, "057c67e28ded17883e430dafabc5fa55", i17);
        f7994bO = a2;
        fmVarArr[i17] = a2;
        int i18 = i17 + 1;
        C2491fm a3 = C4105zY.m41624a(Store.class, "8351543526ca0033222e61a3625b3d14", i18);
        f7995bP = a3;
        fmVarArr[i18] = a3;
        int i19 = i18 + 1;
        C2491fm a4 = C4105zY.m41624a(Store.class, "c2a8d4d350efbafa0ffb49aa508fff4e", i19);
        f7996bQ = a4;
        fmVarArr[i19] = a4;
        int i20 = i19 + 1;
        C2491fm a5 = C4105zY.m41624a(Store.class, "a52a0c53554ad36ca73d1275cc7b93b9", i20);
        jbS = a5;
        fmVarArr[i20] = a5;
        int i21 = i20 + 1;
        C2491fm a6 = C4105zY.m41624a(Store.class, "1850f8e832913e1f6fc4be6c08c47126", i21);
        jbT = a6;
        fmVarArr[i21] = a6;
        int i22 = i21 + 1;
        C2491fm a7 = C4105zY.m41624a(Store.class, "36ae821af3408aae93308b5a5ed528b5", i22);
        jbU = a7;
        fmVarArr[i22] = a7;
        int i23 = i22 + 1;
        C2491fm a8 = C4105zY.m41624a(Store.class, "4c625b7a8d78070d808a5493811011f4", i23);
        jbV = a8;
        fmVarArr[i23] = a8;
        int i24 = i23 + 1;
        C2491fm a9 = C4105zY.m41624a(Store.class, "02652eff9c4b34020b1254e962995fc6", i24);
        jbW = a9;
        fmVarArr[i24] = a9;
        int i25 = i24 + 1;
        C2491fm a10 = C4105zY.m41624a(Store.class, "1c1ab443072ad412d3a80afa8f489d2e", i25);
        jbX = a10;
        fmVarArr[i25] = a10;
        int i26 = i25 + 1;
        C2491fm a11 = C4105zY.m41624a(Store.class, "24ab2f3126a96594d2565e05c5f96d58", i26);
        jbY = a11;
        fmVarArr[i26] = a11;
        int i27 = i26 + 1;
        C2491fm a12 = C4105zY.m41624a(Store.class, "754516a44e10d267340d725efb084a08", i27);
        f7989Pf = a12;
        fmVarArr[i27] = a12;
        int i28 = i27 + 1;
        C2491fm a13 = C4105zY.m41624a(Store.class, "c7acdf9c1cac01709a0ac003cd0ef83a", i28);
        jbZ = a13;
        fmVarArr[i28] = a13;
        int i29 = i28 + 1;
        C2491fm a14 = C4105zY.m41624a(Store.class, "a76b90e57274df8ac30500991c3950ea", i29);
        jca = a14;
        fmVarArr[i29] = a14;
        int i30 = i29 + 1;
        C2491fm a15 = C4105zY.m41624a(Store.class, "57ecc611842d8fc3461b283379224370", i30);
        jcb = a15;
        fmVarArr[i30] = a15;
        int i31 = i30 + 1;
        C2491fm a16 = C4105zY.m41624a(Store.class, "158e41c599b89afe1d55785511328368", i31);
        jcc = a16;
        fmVarArr[i31] = a16;
        int i32 = i31 + 1;
        C2491fm a17 = C4105zY.m41624a(Store.class, "0c973fcfd922a7aba65e7c695fa9c69a", i32);
        jcd = a17;
        fmVarArr[i32] = a17;
        int i33 = i32 + 1;
        C2491fm a18 = C4105zY.m41624a(Store.class, "d05c5d5278aafcc15aaccae99b458092", i33);
        jce = a18;
        fmVarArr[i33] = a18;
        int i34 = i33 + 1;
        C2491fm a19 = C4105zY.m41624a(Store.class, "58726664885705b2767fb1298094e554", i34);
        jcf = a19;
        fmVarArr[i34] = a19;
        int i35 = i34 + 1;
        C2491fm a20 = C4105zY.m41624a(Store.class, "eeb5c508dfbfb92b65af6b82ad0cc03d", i35);
        jcg = a20;
        fmVarArr[i35] = a20;
        int i36 = i35 + 1;
        C2491fm a21 = C4105zY.m41624a(Store.class, "9429494a5a6e1b7b1ddd5d3b6557a8ad", i36);
        jch = a21;
        fmVarArr[i36] = a21;
        int i37 = i36 + 1;
        C2491fm a22 = C4105zY.m41624a(Store.class, "ae85dca8f2222afc758456500635b58a", i37);
        fgq = a22;
        fmVarArr[i37] = a22;
        int i38 = i37 + 1;
        C2491fm a23 = C4105zY.m41624a(Store.class, "1739f2f2de2384c789cbca7a23806832", i38);
        dOa = a23;
        fmVarArr[i38] = a23;
        int i39 = i38 + 1;
        C2491fm a24 = C4105zY.m41624a(Store.class, "06361e7a4e9875c603f07303ef8f9061", i39);
        dNZ = a24;
        fmVarArr[i39] = a24;
        int i40 = i39 + 1;
        C2491fm a25 = C4105zY.m41624a(Store.class, "232abf11192fca7a093d19b59f6abf8b", i40);
        jci = a25;
        fmVarArr[i40] = a25;
        int i41 = i40 + 1;
        C2491fm a26 = C4105zY.m41624a(Store.class, "e6d8ca5dadfa4fe1b4f6ffd70fbce159", i41);
        jcj = a26;
        fmVarArr[i41] = a26;
        int i42 = i41 + 1;
        C2491fm a27 = C4105zY.m41624a(Store.class, "01d2bf98be81ca76c0158a6dfdd606ce", i42);
        jck = a27;
        fmVarArr[i42] = a27;
        int i43 = i42 + 1;
        C2491fm a28 = C4105zY.m41624a(Store.class, "5c3b558fcaf7e66bd56e3da05694207d", i43);
        jcl = a28;
        fmVarArr[i43] = a28;
        int i44 = i43 + 1;
        C2491fm a29 = C4105zY.m41624a(Store.class, "6a3bf3477698c895c46e08a3ec50de34", i44);
        bkF = a29;
        fmVarArr[i44] = a29;
        int i45 = i44 + 1;
        C2491fm a30 = C4105zY.m41624a(Store.class, "840ec90e2fe3a82ed4b0fdeecc8a1aa6", i45);
        bHl = a30;
        fmVarArr[i45] = a30;
        int i46 = i45 + 1;
        C2491fm a31 = C4105zY.m41624a(Store.class, "e33b5ca8b27e1b88eafb1486c4280b09", i46);
        jcm = a31;
        fmVarArr[i46] = a31;
        int i47 = i46 + 1;
        C2491fm a32 = C4105zY.m41624a(Store.class, "f46964d1c19f61b6183572d0780d9953", i47);
        jcn = a32;
        fmVarArr[i47] = a32;
        int i48 = i47 + 1;
        C2491fm a33 = C4105zY.m41624a(Store.class, "b62c11fcbf42e7d6034b7a7b288032e3", i48);
        jco = a33;
        fmVarArr[i48] = a33;
        int i49 = i48 + 1;
        C2491fm a34 = C4105zY.m41624a(Store.class, "40aab3d7683903ee698e210c79731ffa", i49);
        jcp = a34;
        fmVarArr[i49] = a34;
        int i50 = i49 + 1;
        C2491fm a35 = C4105zY.m41624a(Store.class, "eac95972259b83bd40d16d046b9d4ab8", i50);
        jcq = a35;
        fmVarArr[i50] = a35;
        int i51 = i50 + 1;
        C2491fm a36 = C4105zY.m41624a(Store.class, "507c3d28bc965d5f67f040901625de85", i51);
        _f_dispose_0020_0028_0029V = a36;
        fmVarArr[i51] = a36;
        int i52 = i51 + 1;
        C2491fm a37 = C4105zY.m41624a(Store.class, "c64dca8c0cc72217a8401e7b8e1df7b8", i52);
        _f_onResurrect_0020_0028_0029V = a37;
        fmVarArr[i52] = a37;
        int i53 = i52 + 1;
        C2491fm a38 = C4105zY.m41624a(Store.class, "0671e210c839218b36f8f0ed48be559d", i53);
        fgg = a38;
        fmVarArr[i53] = a38;
        int i54 = i53 + 1;
        _m_methods = fmVarArr;
        C1634Xv.m11725a((Object[]) TaikodomObject._m_methods, (Object[]) _m_methods);
        ___iScriptClass = aUO.m18566a(Store.class, C1539Wc.class, _m_fields, _m_methods);
    }

    static /* synthetic */ int[] dCI() {
        int[] iArr = jbR;
        if (iArr == null) {
            iArr = new int[C2618a.values().length];
            try {
                iArr[C2618a.ACTIVE_BUY.ordinal()] = 1;
            } catch (NoSuchFieldError e) {
            }
            try {
                iArr[C2618a.ANY_BUY.ordinal()] = 2;
            } catch (NoSuchFieldError e2) {
            }
            try {
                iArr[C2618a.CHEAP_SELL.ordinal()] = 3;
            } catch (NoSuchFieldError e3) {
            }
            try {
                iArr[C2618a.EXPENSIVE_SELL.ordinal()] = 4;
            } catch (NoSuchFieldError e4) {
            }
            try {
                iArr[C2618a.NONE.ordinal()] = 5;
            } catch (NoSuchFieldError e5) {
            }
            jbR = iArr;
        }
        return iArr;
    }

    @C0064Am(aul = "e33b5ca8b27e1b88eafb1486c4280b09", aum = 0)
    @C5566aOg
    /* renamed from: a */
    private BuyOrder m32768a(C0847MM mm, ItemType jCVar, int i) {
        throw new aWi(new aCE(this, jcm, new Object[]{mm, jCVar, new Integer(i)}));
    }

    @C0064Am(aul = "e6d8ca5dadfa4fe1b4f6ffd70fbce159", aum = 0)
    @C5566aOg
    /* renamed from: a */
    private void m32769a(C2618a aVar, ItemType jCVar, int i, int i2, long j, long j2, float f) {
        throw new aWi(new aCE(this, jcj, new Object[]{aVar, jCVar, new Integer(i), new Integer(i2), new Long(j), new Long(j2), new Float(f)}));
    }

    /* renamed from: a */
    private void m32771a(ListContainer zPVar) {
        bFf().mo5608dq().mo3197f(jbQ, zPVar);
    }

    /* renamed from: a */
    private void m32772a(String str) {
        bFf().mo5608dq().mo3197f(f7992bM, str);
    }

    /* renamed from: a */
    private void m32773a(UUID uuid) {
        bFf().mo5608dq().mo3197f(f7991bL, uuid);
    }

    /* renamed from: aM */
    private void m32775aM(Station bf) {
        bFf().mo5608dq().mo3197f(ffW, bf);
    }

    @C0064Am(aul = "24ab2f3126a96594d2565e05c5f96d58", aum = 0)
    @C5566aOg
    /* renamed from: aN */
    private void m32776aN(Station bf) {
        throw new aWi(new aCE(this, jbY, new Object[]{bf}));
    }

    @C3248pc(aYR = C3248pc.C3250b.ADDER, displayName = "Cheap Sell Items")
    @C0064Am(aul = "158e41c599b89afe1d55785511328368", aum = 0)
    @C5566aOg
    /* renamed from: ab */
    private void m32777ab(ItemType jCVar) {
        throw new aWi(new aCE(this, jcc, new Object[]{jCVar}));
    }

    @C3248pc(aYR = C3248pc.C3250b.ADDER, displayName = "Expensive Sell Items")
    @C0064Am(aul = "0c973fcfd922a7aba65e7c695fa9c69a", aum = 0)
    @C5566aOg
    /* renamed from: ad */
    private void m32778ad(ItemType jCVar) {
        throw new aWi(new aCE(this, jcd, new Object[]{jCVar}));
    }

    @C0064Am(aul = "6a3bf3477698c895c46e08a3ec50de34", aum = 0)
    @C5566aOg
    private void adk() {
        throw new aWi(new aCE(this, bkF, new Object[0]));
    }

    @C3248pc(aYR = C3248pc.C3250b.ADDER, displayName = "Active Buy Items")
    @C0064Am(aul = "d05c5d5278aafcc15aaccae99b458092", aum = 0)
    @C5566aOg
    /* renamed from: af */
    private void m32779af(ItemType jCVar) {
        throw new aWi(new aCE(this, jce, new Object[]{jCVar}));
    }

    @C3248pc(aYR = C3248pc.C3250b.REMOVER, displayName = "Cheap Sell Items")
    @C0064Am(aul = "58726664885705b2767fb1298094e554", aum = 0)
    @C5566aOg
    /* renamed from: ah */
    private void m32780ah(ItemType jCVar) {
        throw new aWi(new aCE(this, jcf, new Object[]{jCVar}));
    }

    @C3248pc(aYR = C3248pc.C3250b.REMOVER, displayName = "Expensive Sell Items")
    @C0064Am(aul = "eeb5c508dfbfb92b65af6b82ad0cc03d", aum = 0)
    @C5566aOg
    /* renamed from: aj */
    private void m32781aj(ItemType jCVar) {
        throw new aWi(new aCE(this, jcg, new Object[]{jCVar}));
    }

    @C3248pc(aYR = C3248pc.C3250b.REMOVER, displayName = "Active Buy Items")
    @C0064Am(aul = "9429494a5a6e1b7b1ddd5d3b6557a8ad", aum = 0)
    @C5566aOg
    /* renamed from: al */
    private void m32782al(ItemType jCVar) {
        throw new aWi(new aCE(this, jch, new Object[]{jCVar}));
    }

    /* renamed from: an */
    private UUID m32783an() {
        return (UUID) bFf().mo5608dq().mo3214p(f7991bL);
    }

    @C3248pc(aYR = C3248pc.C3250b.ACTION, displayName = "WarmUp")
    @C0064Am(aul = "840ec90e2fe3a82ed4b0fdeecc8a1aa6", aum = 0)
    @C5566aOg
    private void anO() {
        throw new aWi(new aCE(this, bHl, new Object[0]));
    }

    /* renamed from: ao */
    private String m32784ao() {
        return (String) bFf().mo5608dq().mo3214p(f7992bM);
    }

    /* renamed from: ax */
    private void m32787ax(C1556Wo wo) {
        bFf().mo5608dq().mo3197f(jbL, wo);
    }

    /* renamed from: ay */
    private void m32788ay(C1556Wo wo) {
        bFf().mo5608dq().mo3197f(jbM, wo);
    }

    /* renamed from: az */
    private void m32789az(C1556Wo wo) {
        bFf().mo5608dq().mo3197f(jbP, wo);
    }

    /* access modifiers changed from: private */
    @C5566aOg
    /* renamed from: b */
    public void m32790b(C2618a aVar, ItemType jCVar, int i, int i2, long j, long j2, float f) {
        switch (bFf().mo6893i(jcj)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, jcj, new Object[]{aVar, jCVar, new Integer(i), new Integer(i2), new Long(j), new Long(j2), new Float(f)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, jcj, new Object[]{aVar, jCVar, new Integer(i), new Integer(i2), new Long(j), new Long(j2), new Float(f)}));
                break;
        }
        m32769a(aVar, jCVar, i, i2, j, j2, f);
    }

    /* renamed from: b */
    private void m32791b(ListContainer zPVar) {
        bFf().mo5608dq().mo3197f(dNL, zPVar);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Handle")
    @C0064Am(aul = "c2a8d4d350efbafa0ffb49aa508fff4e", aum = 0)
    @C5566aOg
    /* renamed from: b */
    private void m32792b(String str) {
        throw new aWi(new aCE(this, f7996bQ, new Object[]{str}));
    }

    @C0064Am(aul = "0671e210c839218b36f8f0ed48be559d", aum = 0)
    private Actor bQC() {
        return dCt();
    }

    @C0064Am(aul = "1739f2f2de2384c789cbca7a23806832", aum = 0)
    @C5566aOg
    /* renamed from: c */
    private void m32794c(BuyOrder kl) {
        throw new aWi(new aCE(this, dOa, new Object[]{kl}));
    }

    @C0064Am(aul = "06361e7a4e9875c603f07303ef8f9061", aum = 0)
    @C5566aOg
    /* renamed from: c */
    private void m32795c(SellOrder awh) {
        throw new aWi(new aCE(this, dNZ, new Object[]{awh}));
    }

    /* renamed from: c */
    private void m32796c(UUID uuid) {
        switch (bFf().mo6893i(f7994bO)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f7994bO, new Object[]{uuid}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f7994bO, new Object[]{uuid}));
                break;
        }
        m32793b(uuid);
    }

    /* renamed from: cJ */
    private void m32797cJ(C3438ra raVar) {
        bFf().mo5608dq().mo3197f(jbI, raVar);
    }

    /* renamed from: cK */
    private void m32798cK(C3438ra raVar) {
        bFf().mo5608dq().mo3197f(jbJ, raVar);
    }

    /* renamed from: cL */
    private void m32799cL(C3438ra raVar) {
        bFf().mo5608dq().mo3197f(jbK, raVar);
    }

    @C5566aOg
    @C0064Am(aul = "b62c11fcbf42e7d6034b7a7b288032e3", aum = 0)
    @C2499fr
    private ListContainer<BuyOrder> dCE() {
        throw new aWi(new aCE(this, jco, new Object[0]));
    }

    @C0064Am(aul = "40aab3d7683903ee698e210c79731ffa", aum = 0)
    @C5566aOg
    private Map<C0847MM, ListContainer<BuyOrder>> dCG() {
        throw new aWi(new aCE(this, jcp, new Object[0]));
    }

    private boolean dCa() {
        return bFf().mo5608dq().mo3201h(jbH);
    }

    private C3438ra dCb() {
        return (C3438ra) bFf().mo5608dq().mo3214p(jbI);
    }

    private C3438ra dCc() {
        return (C3438ra) bFf().mo5608dq().mo3214p(jbJ);
    }

    private C3438ra dCd() {
        return (C3438ra) bFf().mo5608dq().mo3214p(jbK);
    }

    private C1556Wo dCe() {
        return (C1556Wo) bFf().mo5608dq().mo3214p(jbL);
    }

    /* access modifiers changed from: private */
    public C1556Wo dCf() {
        return (C1556Wo) bFf().mo5608dq().mo3214p(jbM);
    }

    private boolean dCg() {
        return bFf().mo5608dq().mo3201h(jbN);
    }

    private boolean dCh() {
        return bFf().mo5608dq().mo3201h(jbO);
    }

    /* access modifiers changed from: private */
    public C1556Wo dCi() {
        return (C1556Wo) bFf().mo5608dq().mo3214p(jbP);
    }

    private ListContainer dCj() {
        return (ListContainer) bFf().mo5608dq().mo3214p(jbQ);
    }

    private ListContainer dCk() {
        return (ListContainer) bFf().mo5608dq().mo3214p(dNL);
    }

    private Station dCl() {
        return (Station) bFf().mo5608dq().mo3214p(ffW);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "No Delay")
    @C0064Am(aul = "1850f8e832913e1f6fc4be6c08c47126", aum = 0)
    @C5566aOg
    private boolean dCm() {
        throw new aWi(new aCE(this, jbT, new Object[0]));
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Nursery Store")
    @C0064Am(aul = "4c625b7a8d78070d808a5493811011f4", aum = 0)
    @C5566aOg
    private boolean dCo() {
        throw new aWi(new aCE(this, jbV, new Object[0]));
    }

    @C0064Am(aul = "02652eff9c4b34020b1254e962995fc6", aum = 0)
    @C5566aOg
    private GlobalEconomyInfo dCq() {
        throw new aWi(new aCE(this, jbW, new Object[0]));
    }

    @C5566aOg
    private GlobalEconomyInfo dCr() {
        switch (bFf().mo6893i(jbW)) {
            case 0:
                return null;
            case 2:
                return (GlobalEconomyInfo) bFf().mo5606d(new aCE(this, jbW, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, jbW, new Object[0]));
                break;
        }
        return dCq();
    }

    @C0064Am(aul = "eac95972259b83bd40d16d046b9d4ab8", aum = 0)
    @C5566aOg
    /* renamed from: do */
    private void m32801do(Player aku) {
        throw new aWi(new aCE(this, jcq, new Object[]{aku}));
    }

    @C0064Am(aul = "232abf11192fca7a093d19b59f6abf8b", aum = 0)
    @C5566aOg
    /* renamed from: g */
    private void m32805g(CommercialOrder aJVar) {
        throw new aWi(new aCE(this, jci, new Object[]{aJVar}));
    }

    @C5566aOg
    /* renamed from: h */
    private void m32806h(CommercialOrder aJVar) {
        switch (bFf().mo6893i(jci)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, jci, new Object[]{aJVar}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, jci, new Object[]{aJVar}));
                break;
        }
        m32805g(aJVar);
    }

    @C0064Am(aul = "f46964d1c19f61b6183572d0780d9953", aum = 0)
    @C5566aOg
    /* renamed from: i */
    private void m32807i(CommercialOrder aJVar) {
        throw new aWi(new aCE(this, jcn, new Object[]{aJVar}));
    }

    @C5566aOg
    /* renamed from: j */
    private void m32808j(CommercialOrder aJVar) {
        switch (bFf().mo6893i(jcn)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, jcn, new Object[]{aJVar}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, jcn, new Object[]{aJVar}));
                break;
        }
        m32807i(aJVar);
    }

    /* renamed from: kE */
    private void m32809kE(boolean z) {
        bFf().mo5608dq().mo3153a(jbH, z);
    }

    /* renamed from: kF */
    private void m32810kF(boolean z) {
        bFf().mo5608dq().mo3153a(jbN, z);
    }

    /* renamed from: kG */
    private void m32811kG(boolean z) {
        bFf().mo5608dq().mo3153a(jbO, z);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "No Delay")
    @C0064Am(aul = "a52a0c53554ad36ca73d1275cc7b93b9", aum = 0)
    @C5566aOg
    /* renamed from: kH */
    private void m32812kH(boolean z) {
        throw new aWi(new aCE(this, jbS, new Object[]{new Boolean(z)}));
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Nursery Store")
    @C0064Am(aul = "36ae821af3408aae93308b5a5ed528b5", aum = 0)
    @C5566aOg
    /* renamed from: kJ */
    private void m32813kJ(boolean z) {
        throw new aWi(new aCE(this, jbU, new Object[]{new Boolean(z)}));
    }

    /* renamed from: T */
    public C6494any mo11T() {
        return ___iScriptClass;
    }

    /* renamed from: U */
    public C2491fm[] mo12U() {
        return _m_methods;
    }

    /* renamed from: W */
    public Object mo13W() {
        return new C1539Wc(this);
    }

    /* renamed from: a */
    public Object mo14a(C0495Gr gr) {
        Object[] args = gr.getArgs();
        switch (gr.mo2417hq() - TaikodomObject._m_methodCount) {
            case 0:
                return m32785ap();
            case 1:
                m32793b((UUID) args[0]);
                return null;
            case 2:
                return m32786ar();
            case 3:
                m32792b((String) args[0]);
                return null;
            case 4:
                m32812kH(((Boolean) args[0]).booleanValue());
                return null;
            case 5:
                return new Boolean(dCm());
            case 6:
                m32813kJ(((Boolean) args[0]).booleanValue());
                return null;
            case 7:
                return new Boolean(dCo());
            case 8:
                return dCq();
            case 9:
                return dCs();
            case 10:
                m32776aN((Station) args[0]);
                return null;
            case 11:
                return m32814uV();
            case 12:
                return dCu();
            case 13:
                return dCw();
            case 14:
                return dCy();
            case 15:
                m32777ab((ItemType) args[0]);
                return null;
            case 16:
                m32778ad((ItemType) args[0]);
                return null;
            case 17:
                m32779af((ItemType) args[0]);
                return null;
            case 18:
                m32780ah((ItemType) args[0]);
                return null;
            case 19:
                m32781aj((ItemType) args[0]);
                return null;
            case 20:
                m32782al((ItemType) args[0]);
                return null;
            case 21:
                m32800d((CommercialOrder) args[0]);
                return null;
            case 22:
                m32794c((BuyOrder) args[0]);
                return null;
            case 23:
                m32795c((SellOrder) args[0]);
                return null;
            case 24:
                m32805g((CommercialOrder) args[0]);
                return null;
            case 25:
                m32769a((C2618a) args[0], (ItemType) args[1], ((Integer) args[2]).intValue(), ((Integer) args[3]).intValue(), ((Long) args[4]).longValue(), ((Long) args[5]).longValue(), ((Float) args[6]).floatValue());
                return null;
            case 26:
                return dCA();
            case 27:
                return dCC();
            case 28:
                adk();
                return null;
            case 29:
                anO();
                return null;
            case 30:
                return m32768a((C0847MM) args[0], (ItemType) args[1], ((Integer) args[2]).intValue());
            case 31:
                m32807i((CommercialOrder) args[0]);
                return null;
            case 32:
                return dCE();
            case 33:
                return dCG();
            case 34:
                m32801do((Player) args[0]);
                return null;
            case 35:
                m32804fg();
                return null;
            case 36:
                m32774aG();
                return null;
            case 37:
                return bQC();
            default:
                return super.mo14a(gr);
        }
    }

    /* renamed from: a */
    public void mo3918a(CommercialOrder aJVar) {
        switch (bFf().mo6893i(fgq)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, fgq, new Object[]{aJVar}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, fgq, new Object[]{aJVar}));
                break;
        }
        m32800d(aJVar);
    }

    /* renamed from: a */
    public void mo15a(Collection collection, C0495Gr gr) {
        super.mo15a(collection, gr);
    }

    /* renamed from: aH */
    public void mo70aH() {
        switch (bFf().mo6893i(_f_onResurrect_0020_0028_0029V)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, _f_onResurrect_0020_0028_0029V, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, _f_onResurrect_0020_0028_0029V, new Object[0]));
                break;
        }
        m32774aG();
    }

    @C5566aOg
    /* renamed from: aO */
    public void mo19293aO(Station bf) {
        switch (bFf().mo6893i(jbY)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, jbY, new Object[]{bf}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, jbY, new Object[]{bf}));
                break;
        }
        m32776aN(bf);
    }

    @C3248pc(aYR = C3248pc.C3250b.ADDER, displayName = "Cheap Sell Items")
    @C5566aOg
    /* renamed from: ac */
    public void mo19294ac(ItemType jCVar) {
        switch (bFf().mo6893i(jcc)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, jcc, new Object[]{jCVar}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, jcc, new Object[]{jCVar}));
                break;
        }
        m32777ab(jCVar);
    }

    @C3248pc(aYR = C3248pc.C3250b.ADDER, displayName = "Expensive Sell Items")
    @C5566aOg
    /* renamed from: ae */
    public void mo19295ae(ItemType jCVar) {
        switch (bFf().mo6893i(jcd)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, jcd, new Object[]{jCVar}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, jcd, new Object[]{jCVar}));
                break;
        }
        m32778ad(jCVar);
    }

    @C3248pc(aYR = C3248pc.C3250b.ADDER, displayName = "Active Buy Items")
    @C5566aOg
    /* renamed from: ag */
    public void mo19296ag(ItemType jCVar) {
        switch (bFf().mo6893i(jce)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, jce, new Object[]{jCVar}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, jce, new Object[]{jCVar}));
                break;
        }
        m32779af(jCVar);
    }

    @C3248pc(aYR = C3248pc.C3250b.REMOVER, displayName = "Cheap Sell Items")
    @C5566aOg
    /* renamed from: ai */
    public void mo19297ai(ItemType jCVar) {
        switch (bFf().mo6893i(jcf)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, jcf, new Object[]{jCVar}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, jcf, new Object[]{jCVar}));
                break;
        }
        m32780ah(jCVar);
    }

    @C3248pc(aYR = C3248pc.C3250b.REMOVER, displayName = "Expensive Sell Items")
    @C5566aOg
    /* renamed from: ak */
    public void mo19298ak(ItemType jCVar) {
        switch (bFf().mo6893i(jcg)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, jcg, new Object[]{jCVar}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, jcg, new Object[]{jCVar}));
                break;
        }
        m32781aj(jCVar);
    }

    @C3248pc(aYR = C3248pc.C3250b.REMOVER, displayName = "Active Buy Items")
    @C5566aOg
    /* renamed from: am */
    public void mo19299am(ItemType jCVar) {
        switch (bFf().mo6893i(jch)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, jch, new Object[]{jCVar}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, jch, new Object[]{jCVar}));
                break;
        }
        m32782al(jCVar);
    }

    @C3248pc(aYR = C3248pc.C3250b.ACTION, displayName = "WarmUp")
    @C5566aOg
    public void anP() {
        switch (bFf().mo6893i(bHl)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, bHl, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, bHl, new Object[0]));
                break;
        }
        anO();
    }

    /* renamed from: aq */
    public UUID mo18aq() {
        switch (bFf().mo6893i(f7993bN)) {
            case 0:
                return null;
            case 2:
                return (UUID) bFf().mo5606d(new aCE(this, f7993bN, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f7993bN, new Object[0]));
                break;
        }
        return m32785ap();
    }

    @C5566aOg
    /* renamed from: b */
    public BuyOrder mo19301b(C0847MM mm, ItemType jCVar, int i) {
        switch (bFf().mo6893i(jcm)) {
            case 0:
                return null;
            case 2:
                return (BuyOrder) bFf().mo5606d(new aCE(this, jcm, new Object[]{mm, jCVar, new Integer(i)}));
            case 3:
                bFf().mo5606d(new aCE(this, jcm, new Object[]{mm, jCVar, new Integer(i)}));
                break;
        }
        return m32768a(mm, jCVar, i);
    }

    public /* bridge */ /* synthetic */ Actor bhE() {
        switch (bFf().mo6893i(fgg)) {
            case 0:
                return null;
            case 2:
                return (Actor) bFf().mo5606d(new aCE(this, fgg, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, fgg, new Object[0]));
                break;
        }
        return bQC();
    }

    /* renamed from: c */
    public C5663aRz[] mo25c() {
        return _m_fields;
    }

    @C5566aOg
    /* renamed from: d */
    public void mo19302d(BuyOrder kl) {
        switch (bFf().mo6893i(dOa)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, dOa, new Object[]{kl}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, dOa, new Object[]{kl}));
                break;
        }
        m32794c(kl);
    }

    @C5566aOg
    /* renamed from: d */
    public void mo19303d(SellOrder awh) {
        switch (bFf().mo6893i(dNZ)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, dNZ, new Object[]{awh}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, dNZ, new Object[]{awh}));
                break;
        }
        m32795c(awh);
    }

    public ListContainer<BuyOrder> dCB() {
        switch (bFf().mo6893i(jck)) {
            case 0:
                return null;
            case 2:
                return (ListContainer) bFf().mo5606d(new aCE(this, jck, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, jck, new Object[0]));
                break;
        }
        return dCA();
    }

    public ListContainer<SellOrder> dCD() {
        switch (bFf().mo6893i(jcl)) {
            case 0:
                return null;
            case 2:
                return (ListContainer) bFf().mo5606d(new aCE(this, jcl, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, jcl, new Object[0]));
                break;
        }
        return dCC();
    }

    @C5566aOg
    @C2499fr
    public ListContainer<BuyOrder> dCF() {
        switch (bFf().mo6893i(jco)) {
            case 0:
                return null;
            case 2:
                return (ListContainer) bFf().mo5606d(new aCE(this, jco, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, jco, new Object[0]));
                break;
        }
        return dCE();
    }

    @C5566aOg
    public Map<C0847MM, ListContainer<BuyOrder>> dCH() {
        switch (bFf().mo6893i(jcp)) {
            case 0:
                return null;
            case 2:
                return (Map) bFf().mo5606d(new aCE(this, jcp, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, jcp, new Object[0]));
                break;
        }
        return dCG();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "No Delay")
    @C5566aOg
    public boolean dCn() {
        switch (bFf().mo6893i(jbT)) {
            case 0:
                return false;
            case 2:
                return ((Boolean) bFf().mo5606d(new aCE(this, jbT, new Object[0]))).booleanValue();
            case 3:
                bFf().mo5606d(new aCE(this, jbT, new Object[0]));
                break;
        }
        return dCm();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Nursery Store")
    @C5566aOg
    public boolean dCp() {
        switch (bFf().mo6893i(jbV)) {
            case 0:
                return false;
            case 2:
                return ((Boolean) bFf().mo5606d(new aCE(this, jbV, new Object[0]))).booleanValue();
            case 3:
                bFf().mo5606d(new aCE(this, jbV, new Object[0]));
                break;
        }
        return dCo();
    }

    public Station dCt() {
        switch (bFf().mo6893i(jbX)) {
            case 0:
                return null;
            case 2:
                return (Station) bFf().mo5606d(new aCE(this, jbX, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, jbX, new Object[0]));
                break;
        }
        return dCs();
    }

    @C3248pc(aYR = C3248pc.C3250b.ITERATOR, displayName = "Cheap Sell Items")
    public List<ItemType> dCv() {
        switch (bFf().mo6893i(jbZ)) {
            case 0:
                return null;
            case 2:
                return (List) bFf().mo5606d(new aCE(this, jbZ, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, jbZ, new Object[0]));
                break;
        }
        return dCu();
    }

    @C3248pc(aYR = C3248pc.C3250b.ITERATOR, displayName = "Expensive Sell Items")
    public List<ItemType> dCx() {
        switch (bFf().mo6893i(jca)) {
            case 0:
                return null;
            case 2:
                return (List) bFf().mo5606d(new aCE(this, jca, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, jca, new Object[0]));
                break;
        }
        return dCw();
    }

    @C3248pc(aYR = C3248pc.C3250b.ITERATOR, displayName = "Active Buy Items")
    public List<ItemType> dCz() {
        switch (bFf().mo6893i(jcb)) {
            case 0:
                return null;
            case 2:
                return (List) bFf().mo5606d(new aCE(this, jcb, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, jcb, new Object[0]));
                break;
        }
        return dCy();
    }

    public void dispose() {
        switch (bFf().mo6893i(_f_dispose_0020_0028_0029V)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, _f_dispose_0020_0028_0029V, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, _f_dispose_0020_0028_0029V, new Object[0]));
                break;
        }
        m32804fg();
    }

    @C5566aOg
    /* renamed from: dp */
    public void mo19314dp(Player aku) {
        switch (bFf().mo6893i(jcq)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, jcq, new Object[]{aku}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, jcq, new Object[]{aku}));
                break;
        }
        m32801do(aku);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Handle")
    public String getHandle() {
        switch (bFf().mo6893i(f7995bP)) {
            case 0:
                return null;
            case 2:
                return (String) bFf().mo5606d(new aCE(this, f7995bP, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f7995bP, new Object[0]));
                break;
        }
        return m32786ar();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Handle")
    @C5566aOg
    public void setHandle(String str) {
        switch (bFf().mo6893i(f7996bQ)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f7996bQ, new Object[]{str}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f7996bQ, new Object[]{str}));
                break;
        }
        m32792b(str);
    }

    public String getName() {
        switch (bFf().mo6893i(f7989Pf)) {
            case 0:
                return null;
            case 2:
                return (String) bFf().mo5606d(new aCE(this, f7989Pf, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f7989Pf, new Object[0]));
                break;
        }
        return m32814uV();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "No Delay")
    @C5566aOg
    /* renamed from: kI */
    public void mo19315kI(boolean z) {
        switch (bFf().mo6893i(jbS)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, jbS, new Object[]{new Boolean(z)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, jbS, new Object[]{new Boolean(z)}));
                break;
        }
        m32812kH(z);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Nursery Store")
    @C5566aOg
    /* renamed from: kK */
    public void mo19316kK(boolean z) {
        switch (bFf().mo6893i(jbU)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, jbU, new Object[]{new Boolean(z)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, jbU, new Object[]{new Boolean(z)}));
                break;
        }
        m32813kJ(z);
    }

    @C5566aOg
    public void reset() {
        switch (bFf().mo6893i(bkF)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, bkF, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, bkF, new Object[0]));
                break;
        }
        adk();
    }

    @C0064Am(aul = "7decb6a01bb0353116e1ee7bc7eafc1a", aum = 0)
    /* renamed from: ap */
    private UUID m32785ap() {
        return m32783an();
    }

    @C0064Am(aul = "057c67e28ded17883e430dafabc5fa55", aum = 0)
    /* renamed from: b */
    private void m32793b(UUID uuid) {
        m32773a(uuid);
    }

    /* renamed from: S */
    public void mo10S() {
        super.mo10S();
        m32809kE(false);
        m32773a(UUID.randomUUID());
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Handle")
    @C0064Am(aul = "8351543526ca0033222e61a3625b3d14", aum = 0)
    /* renamed from: ar */
    private String m32786ar() {
        return m32784ao();
    }

    @C0064Am(aul = "1c1ab443072ad412d3a80afa8f489d2e", aum = 0)
    private Station dCs() {
        return dCl();
    }

    @C0064Am(aul = "754516a44e10d267340d725efb084a08", aum = 0)
    /* renamed from: uV */
    private String m32814uV() {
        return "Store - " + dCl().getName();
    }

    @C3248pc(aYR = C3248pc.C3250b.ITERATOR, displayName = "Cheap Sell Items")
    @C0064Am(aul = "c7acdf9c1cac01709a0ac003cd0ef83a", aum = 0)
    private List<ItemType> dCu() {
        return Collections.unmodifiableList(dCb());
    }

    @C3248pc(aYR = C3248pc.C3250b.ITERATOR, displayName = "Expensive Sell Items")
    @C0064Am(aul = "a76b90e57274df8ac30500991c3950ea", aum = 0)
    private List<ItemType> dCw() {
        return Collections.unmodifiableList(dCc());
    }

    @C3248pc(aYR = C3248pc.C3250b.ITERATOR, displayName = "Active Buy Items")
    @C0064Am(aul = "57ecc611842d8fc3461b283379224370", aum = 0)
    private List<ItemType> dCy() {
        return Collections.unmodifiableList(dCd());
    }

    @C0064Am(aul = "ae85dca8f2222afc758456500635b58a", aum = 0)
    /* renamed from: d */
    private void m32800d(CommercialOrder aJVar) {
        if (aJVar instanceof BuyOrder) {
            mo19302d((BuyOrder) aJVar);
        } else if (aJVar instanceof SellOrder) {
            mo19303d((SellOrder) aJVar);
        }
    }

    @C0064Am(aul = "01d2bf98be81ca76c0158a6dfdd606ce", aum = 0)
    private ListContainer<BuyOrder> dCA() {
        if (dCj() == null) {
            ListContainer zPVar = (ListContainer) bFf().mo6865M(ListContainer.class);
            zPVar.mo10S();
            m32771a(zPVar);
        }
        return dCj();
    }

    @C0064Am(aul = "5c3b558fcaf7e66bd56e3da05694207d", aum = 0)
    private ListContainer<SellOrder> dCC() {
        if (dCk() == null) {
            ListContainer zPVar = (ListContainer) bFf().mo6865M(ListContainer.class);
            zPVar.mo10S();
            m32791b(zPVar);
        }
        return dCk();
    }

    @C0064Am(aul = "507c3d28bc965d5f67f040901625de85", aum = 0)
    /* renamed from: fg */
    private void m32804fg() {
        super.dispose();
        mo8359ma("Removing old store from " + dCt());
        if (dCf().size() > 0) {
            for (PostponedOrderTasklet cVar : dCf().values()) {
                cVar.stop();
                cVar.dispose();
            }
            dCf().clear();
        }
        ArrayList<CommercialOrder> arrayList = new ArrayList<>();
        for (BuyOrder add : dCB().getList()) {
            arrayList.add(add);
        }
        dCB().clear();
        for (SellOrder add2 : dCD().getList()) {
            arrayList.add(add2);
        }
        for (CommercialOrder fk : arrayList) {
            fk.mo9446fk();
        }
        dCD().clear();
    }

    @C0064Am(aul = "c64dca8c0cc72217a8401e7b8e1df7b8", aum = 0)
    /* renamed from: aG */
    private void m32774aG() {
        super.mo70aH();
        if (dCt().azJ() != this) {
            dispose();
        }
    }

    @C5566aOg
    /* renamed from: a.hf$a */
    public enum C2618a {
        ACTIVE_BUY,
        ANY_BUY,
        CHEAP_SELL,
        EXPENSIVE_SELL,
        NONE
    }

    @C5566aOg
    @C6485anp
    @C5511aMd
    /* renamed from: a.hf$b */
    /* compiled from: a */
    public static class AnyBuyInfo extends aDJ implements C1616Xf {
        public static final int _m_fieldCount = 0;
        public static final C5663aRz[] _m_fields = null;
        public static final int _m_methodCount = 0;
        public static final C2491fm[] _m_methods = null;
        public static final C5663aRz aGo = null;
        public static final C5663aRz aGp = null;
        public static final C5663aRz aGq = null;
        public static final C2491fm aGr = null;
        public static final C2491fm aGs = null;
        public static final C2491fm aGt = null;
        public static final C2491fm aGu = null;
        public static final C2491fm aGv = null;
        public static final C2491fm aGw = null;
        public static final long serialVersionUID = 0;
        public static C6494any ___iScriptClass;
        @C0064Am(aul = "aa4d02a3a86af5b6441c7ff470d656fb", aum = 2)

        /* renamed from: cZ */
        private static int f8003cZ;
        @C0064Am(aul = "a48e391cdaf131a5c700eab0f2059dc7", aum = 0)

        /* renamed from: ee */
        private static long f8004ee;
        @C0064Am(aul = "b65190e2e51f3fb47523f7562550e396", aum = 1)

        /* renamed from: ef */
        private static long f8005ef;

        static {
            m32844V();
        }

        public AnyBuyInfo() {
            super((C5540aNg) null);
            super.mo10S();
        }

        public AnyBuyInfo(C5540aNg ang) {
            super(ang);
        }

        /* renamed from: V */
        static void m32844V() {
            serialVersionUID = (long) 1;
            _m_fieldCount = aDJ._m_fieldCount + 3;
            _m_methodCount = aDJ._m_methodCount + 6;
            int i = aDJ._m_fieldCount;
            C5663aRz[] arzArr = new C5663aRz[(i + 3)];
            C5663aRz b = C5640aRc.m17844b(AnyBuyInfo.class, "a48e391cdaf131a5c700eab0f2059dc7", i);
            aGo = b;
            arzArr[i] = b;
            int i2 = i + 1;
            C5663aRz b2 = C5640aRc.m17844b(AnyBuyInfo.class, "b65190e2e51f3fb47523f7562550e396", i2);
            aGp = b2;
            arzArr[i2] = b2;
            int i3 = i2 + 1;
            C5663aRz b3 = C5640aRc.m17844b(AnyBuyInfo.class, "aa4d02a3a86af5b6441c7ff470d656fb", i3);
            aGq = b3;
            arzArr[i3] = b3;
            int i4 = i3 + 1;
            _m_fields = arzArr;
            C1634Xv.m11725a((Object[]) aDJ._m_fields, (Object[]) _m_fields);
            int i5 = aDJ._m_methodCount;
            C2491fm[] fmVarArr = new C2491fm[(i5 + 6)];
            C2491fm a = C4105zY.m41624a(AnyBuyInfo.class, "07728c65adacab73768f10c366d1720a", i5);
            aGr = a;
            fmVarArr[i5] = a;
            int i6 = i5 + 1;
            C2491fm a2 = C4105zY.m41624a(AnyBuyInfo.class, "93dc55ce7ba76bd6b98bc460d3376d35", i6);
            aGs = a2;
            fmVarArr[i6] = a2;
            int i7 = i6 + 1;
            C2491fm a3 = C4105zY.m41624a(AnyBuyInfo.class, "64362b58c96267254ca23c6ba9875f8b", i7);
            aGt = a3;
            fmVarArr[i7] = a3;
            int i8 = i7 + 1;
            C2491fm a4 = C4105zY.m41624a(AnyBuyInfo.class, "223cb85780531dd70d2e98eb26a14242", i8);
            aGu = a4;
            fmVarArr[i8] = a4;
            int i9 = i8 + 1;
            C2491fm a5 = C4105zY.m41624a(AnyBuyInfo.class, "8f8f21e184eb5268ff0cd208156e77d5", i9);
            aGv = a5;
            fmVarArr[i9] = a5;
            int i10 = i9 + 1;
            C2491fm a6 = C4105zY.m41624a(AnyBuyInfo.class, "4cd3c26ed5a5fb612f4b979b0d6eb71b", i10);
            aGw = a6;
            fmVarArr[i10] = a6;
            int i11 = i10 + 1;
            _m_methods = fmVarArr;
            C1634Xv.m11725a((Object[]) aDJ._m_methods, (Object[]) _m_methods);
            ___iScriptClass = aUO.m18566a(AnyBuyInfo.class, C1095Q.class, _m_fields, _m_methods);
        }

        /* renamed from: PD */
        private long m32838PD() {
            return bFf().mo5608dq().mo3213o(aGo);
        }

        /* renamed from: PE */
        private long m32839PE() {
            return bFf().mo5608dq().mo3213o(aGp);
        }

        /* renamed from: PF */
        private int m32840PF() {
            return bFf().mo5608dq().mo3212n(aGq);
        }

        /* renamed from: cZ */
        private void m32845cZ(int i) {
            bFf().mo5608dq().mo3183b(aGq, i);
        }

        /* renamed from: cn */
        private void m32846cn(long j) {
            bFf().mo5608dq().mo3184b(aGo, j);
        }

        /* renamed from: co */
        private void m32847co(long j) {
            bFf().mo5608dq().mo3184b(aGp, j);
        }

        /* renamed from: Iq */
        public int mo19319Iq() {
            switch (bFf().mo6893i(aGt)) {
                case 0:
                    return 0;
                case 2:
                    return ((Integer) bFf().mo5606d(new aCE(this, aGt, new Object[0]))).intValue();
                case 3:
                    bFf().mo5606d(new aCE(this, aGt, new Object[0]));
                    break;
            }
            return m32843PK();
        }

        /* renamed from: PH */
        public long mo19320PH() {
            switch (bFf().mo6893i(aGr)) {
                case 0:
                    return 0;
                case 2:
                    return ((Long) bFf().mo5606d(new aCE(this, aGr, new Object[0]))).longValue();
                case 3:
                    bFf().mo5606d(new aCE(this, aGr, new Object[0]));
                    break;
            }
            return m32841PG();
        }

        /* renamed from: PJ */
        public long mo19321PJ() {
            switch (bFf().mo6893i(aGs)) {
                case 0:
                    return 0;
                case 2:
                    return ((Long) bFf().mo5606d(new aCE(this, aGs, new Object[0]))).longValue();
                case 3:
                    bFf().mo5606d(new aCE(this, aGs, new Object[0]));
                    break;
            }
            return m32842PI();
        }

        /* renamed from: T */
        public C6494any mo11T() {
            return ___iScriptClass;
        }

        /* renamed from: U */
        public C2491fm[] mo12U() {
            return _m_methods;
        }

        /* renamed from: W */
        public Object mo13W() {
            return new C1095Q(this);
        }

        /* renamed from: a */
        public Object mo14a(C0495Gr gr) {
            Object[] args = gr.getArgs();
            switch (gr.mo2417hq() - aDJ._m_methodCount) {
                case 0:
                    return new Long(m32841PG());
                case 1:
                    return new Long(m32842PI());
                case 2:
                    return new Integer(m32843PK());
                case 3:
                    m32848cp(((Long) args[0]).longValue());
                    return null;
                case 4:
                    m32849cr(((Long) args[0]).longValue());
                    return null;
                case 5:
                    m32850da(((Integer) args[0]).intValue());
                    return null;
                default:
                    return super.mo14a(gr);
            }
        }

        /* renamed from: a */
        public void mo15a(Collection collection, C0495Gr gr) {
            super.mo15a(collection, gr);
        }

        /* renamed from: c */
        public C5663aRz[] mo25c() {
            return _m_fields;
        }

        /* renamed from: cq */
        public void mo19322cq(long j) {
            switch (bFf().mo6893i(aGu)) {
                case 0:
                    return;
                case 2:
                    bFf().mo5606d(new aCE(this, aGu, new Object[]{new Long(j)}));
                    return;
                case 3:
                    bFf().mo5606d(new aCE(this, aGu, new Object[]{new Long(j)}));
                    break;
            }
            m32848cp(j);
        }

        /* renamed from: cs */
        public void mo19323cs(long j) {
            switch (bFf().mo6893i(aGv)) {
                case 0:
                    return;
                case 2:
                    bFf().mo5606d(new aCE(this, aGv, new Object[]{new Long(j)}));
                    return;
                case 3:
                    bFf().mo5606d(new aCE(this, aGv, new Object[]{new Long(j)}));
                    break;
            }
            m32849cr(j);
        }

        /* renamed from: db */
        public void mo19324db(int i) {
            switch (bFf().mo6893i(aGw)) {
                case 0:
                    return;
                case 2:
                    bFf().mo5606d(new aCE(this, aGw, new Object[]{new Integer(i)}));
                    return;
                case 3:
                    bFf().mo5606d(new aCE(this, aGw, new Object[]{new Integer(i)}));
                    break;
            }
            m32850da(i);
        }

        /* renamed from: S */
        public void mo10S() {
            super.mo10S();
        }

        @C0064Am(aul = "07728c65adacab73768f10c366d1720a", aum = 0)
        /* renamed from: PG */
        private long m32841PG() {
            return m32838PD();
        }

        @C0064Am(aul = "93dc55ce7ba76bd6b98bc460d3376d35", aum = 0)
        /* renamed from: PI */
        private long m32842PI() {
            return m32839PE();
        }

        @C0064Am(aul = "64362b58c96267254ca23c6ba9875f8b", aum = 0)
        /* renamed from: PK */
        private int m32843PK() {
            return m32840PF();
        }

        @C0064Am(aul = "223cb85780531dd70d2e98eb26a14242", aum = 0)
        /* renamed from: cp */
        private void m32848cp(long j) {
            m32846cn(j);
        }

        @C0064Am(aul = "8f8f21e184eb5268ff0cd208156e77d5", aum = 0)
        /* renamed from: cr */
        private void m32849cr(long j) {
            m32847co(j);
        }

        @C0064Am(aul = "4cd3c26ed5a5fb612f4b979b0d6eb71b", aum = 0)
        /* renamed from: da */
        private void m32850da(int i) {
            m32845cZ(i);
        }
    }

    @C5566aOg
    @C6485anp
    @C5511aMd
    /* renamed from: a.hf$c */
    /* compiled from: a */
    public class PostponedOrderTasklet extends TaskletImpl implements C1616Xf {

        /* renamed from: JD */
        public static final C2491fm f8006JD = null;

        /* renamed from: MT */
        public static final C5663aRz f8007MT = null;

        /* renamed from: MY */
        public static final C2491fm f8008MY = null;
        public static final C2491fm _f_dispose_0020_0028_0029V = null;
        public static final C5663aRz _f_type = null;
        public static final int _m_fieldCount = 0;
        public static final C5663aRz[] _m_fields = null;
        public static final int _m_methodCount = 0;
        public static final C2491fm[] _m_methods = null;
        public static final C5663aRz aGq = null;
        public static final C2491fm aGt = null;
        /* renamed from: aT */
        public static final C5663aRz f8009aT = null;
        public static final C5663aRz bxM = null;
        public static final C5663aRz bxN = null;
        public static final C2491fm bxO = null;
        public static final C2491fm bxP = null;
        /* renamed from: kO */
        public static final C5663aRz f8017kO = null;
        /* renamed from: lk */
        public static final C2491fm f8018lk = null;
        /* renamed from: ln */
        public static final C2491fm f8019ln = null;
        public static final long serialVersionUID = 0;
        public static C6494any ___iScriptClass;
        @C0064Am(aul = "988b77fba2c917a3583d602de8a1a5d2", aum = 6)

        /* renamed from: de */
        static /* synthetic */ Store f8016de;
        @C0064Am(aul = "e57122d18ae6547cf59afb9c82a34aeb", aum = 0)

        /* renamed from: cX */
        private static C2618a f8010cX;
        @C0064Am(aul = "88537e974aeea6b3b0e3830c25880d66", aum = 1)

        /* renamed from: cY */
        private static ItemType f8011cY;
        @C0064Am(aul = "70f769658435bd7b7af92c351925160e", aum = 2)

        /* renamed from: cZ */
        private static int f8012cZ;
        @C0064Am(aul = "7cdcf0fb5e429addb60f1e3a599e6a04", aum = 3)

        /* renamed from: da */
        private static int f8013da;
        @C0064Am(aul = "f4f6627b9b7a62e189fcbcab13817572", aum = 4)

        /* renamed from: db */
        private static long f8014db;
        @C0064Am(aul = "1f22d90bd3f021fd99f55a699b4b20b5", aum = 5)

        /* renamed from: dd */
        private static long f8015dd;

        static {
            m32867V();
        }

        public PostponedOrderTasklet() {
            super((C5540aNg) null);
            super.mo10S();
        }

        public PostponedOrderTasklet(C5540aNg ang) {
            super(ang);
        }

        public PostponedOrderTasklet(Store hfVar, C2618a aVar, ItemType jCVar, int i, int i2, long j, long j2) {
            super((C5540aNg) null);
            super._m_script_init(hfVar, aVar, jCVar, i, i2, j, j2);
        }

        /* renamed from: V */
        static void m32867V() {
            serialVersionUID = (long) 1;
            _m_fieldCount = TaskletImpl._m_fieldCount + 7;
            _m_methodCount = TaskletImpl._m_methodCount + 8;
            int i = TaskletImpl._m_fieldCount;
            C5663aRz[] arzArr = new C5663aRz[(i + 7)];
            C5663aRz b = C5640aRc.m17844b(PostponedOrderTasklet.class, "e57122d18ae6547cf59afb9c82a34aeb", i);
            _f_type = b;
            arzArr[i] = b;
            int i2 = i + 1;
            C5663aRz b2 = C5640aRc.m17844b(PostponedOrderTasklet.class, "88537e974aeea6b3b0e3830c25880d66", i2);
            f8017kO = b2;
            arzArr[i2] = b2;
            int i3 = i2 + 1;
            C5663aRz b3 = C5640aRc.m17844b(PostponedOrderTasklet.class, "70f769658435bd7b7af92c351925160e", i3);
            aGq = b3;
            arzArr[i3] = b3;
            int i4 = i3 + 1;
            C5663aRz b4 = C5640aRc.m17844b(PostponedOrderTasklet.class, "7cdcf0fb5e429addb60f1e3a599e6a04", i4);
            bxM = b4;
            arzArr[i4] = b4;
            int i5 = i4 + 1;
            C5663aRz b5 = C5640aRc.m17844b(PostponedOrderTasklet.class, "f4f6627b9b7a62e189fcbcab13817572", i5);
            f8007MT = b5;
            arzArr[i5] = b5;
            int i6 = i5 + 1;
            C5663aRz b6 = C5640aRc.m17844b(PostponedOrderTasklet.class, "1f22d90bd3f021fd99f55a699b4b20b5", i6);
            bxN = b6;
            arzArr[i6] = b6;
            int i7 = i6 + 1;
            C5663aRz b7 = C5640aRc.m17844b(PostponedOrderTasklet.class, "988b77fba2c917a3583d602de8a1a5d2", i7);
            f8009aT = b7;
            arzArr[i7] = b7;
            int i8 = i7 + 1;
            _m_fields = arzArr;
            C1634Xv.m11725a((Object[]) TaskletImpl._m_fields, (Object[]) _m_fields);
            int i9 = TaskletImpl._m_methodCount;
            C2491fm[] fmVarArr = new C2491fm[(i9 + 8)];
            C2491fm a = C4105zY.m41624a(PostponedOrderTasklet.class, "50d10a8e31373aa7ceb2385bf98bdb0c", i9);
            f8006JD = a;
            fmVarArr[i9] = a;
            int i10 = i9 + 1;
            C2491fm a2 = C4105zY.m41624a(PostponedOrderTasklet.class, "3f4412ebbfa962f356d4c1f85756e49a", i10);
            _f_dispose_0020_0028_0029V = a2;
            fmVarArr[i10] = a2;
            int i11 = i10 + 1;
            C2491fm a3 = C4105zY.m41624a(PostponedOrderTasklet.class, "992e445624d5261b08949d4b3b911c0a", i11);
            bxO = a3;
            fmVarArr[i11] = a3;
            int i12 = i11 + 1;
            C2491fm a4 = C4105zY.m41624a(PostponedOrderTasklet.class, "1ec7ec814f0aa9e2a0c34d5244b1f97a", i12);
            f8018lk = a4;
            fmVarArr[i12] = a4;
            int i13 = i12 + 1;
            C2491fm a5 = C4105zY.m41624a(PostponedOrderTasklet.class, "fdd3078a0c529de08718039291af9600", i13);
            aGt = a5;
            fmVarArr[i13] = a5;
            int i14 = i13 + 1;
            C2491fm a6 = C4105zY.m41624a(PostponedOrderTasklet.class, "a92d75062ec3840f918cfd823f84920e", i14);
            bxP = a6;
            fmVarArr[i14] = a6;
            int i15 = i14 + 1;
            C2491fm a7 = C4105zY.m41624a(PostponedOrderTasklet.class, "8f62a981edde73edb3b91f90d03ea9b5", i15);
            f8008MY = a7;
            fmVarArr[i15] = a7;
            int i16 = i15 + 1;
            C2491fm a8 = C4105zY.m41624a(PostponedOrderTasklet.class, "598d422090266360ed04bb22a2529a71", i16);
            f8019ln = a8;
            fmVarArr[i16] = a8;
            int i17 = i16 + 1;
            _m_methods = fmVarArr;
            C1634Xv.m11725a((Object[]) TaskletImpl._m_methods, (Object[]) _m_methods);
            ___iScriptClass = aUO.m18566a(PostponedOrderTasklet.class, C0432G.class, _m_fields, _m_methods);
        }

        /* renamed from: G */
        private void m32864G(long j) {
            bFf().mo5608dq().mo3184b(f8007MT, j);
        }

        /* renamed from: PF */
        private int m32865PF() {
            return bFf().mo5608dq().mo3212n(aGq);
        }

        /* renamed from: a */
        private void m32868a(C2618a aVar) {
            bFf().mo5608dq().mo3197f(_f_type, aVar);
        }

        /* renamed from: a */
        private void m32869a(Store hfVar) {
            bFf().mo5608dq().mo3197f(f8009aT, hfVar);
        }

        private C2618a akg() {
            return (C2618a) bFf().mo5608dq().mo3214p(_f_type);
        }

        private int akh() {
            return bFf().mo5608dq().mo3212n(bxM);
        }

        private long aki() {
            return bFf().mo5608dq().mo3213o(bxN);
        }

        private Store akj() {
            return (Store) bFf().mo5608dq().mo3214p(f8009aT);
        }

        /* renamed from: cZ */
        private void m32870cZ(int i) {
            bFf().mo5608dq().mo3183b(aGq, i);
        }

        /* renamed from: dl */
        private void m32871dl(long j) {
            bFf().mo5608dq().mo3184b(bxN, j);
        }

        /* renamed from: eA */
        private ItemType m32872eA() {
            return (ItemType) bFf().mo5608dq().mo3214p(f8017kO);
        }

        /* renamed from: eU */
        private void m32875eU(int i) {
            bFf().mo5608dq().mo3183b(bxM, i);
        }

        /* renamed from: f */
        private void m32876f(ItemType jCVar) {
            bFf().mo5608dq().mo3197f(f8017kO, jCVar);
        }

        /* renamed from: rU */
        private long m32879rU() {
            return bFf().mo5608dq().mo3213o(f8007MT);
        }

        /* renamed from: Iq */
        public int mo19325Iq() {
            switch (bFf().mo6893i(aGt)) {
                case 0:
                    return 0;
                case 2:
                    return ((Integer) bFf().mo5606d(new aCE(this, aGt, new Object[0]))).intValue();
                case 3:
                    bFf().mo5606d(new aCE(this, aGt, new Object[0]));
                    break;
            }
            return m32866PK();
        }

        /* renamed from: S */
        public void mo10S() {
            super.mo10S();
        }

        /* renamed from: T */
        public C6494any mo11T() {
            return ___iScriptClass;
        }

        /* renamed from: U */
        public C2491fm[] mo12U() {
            return _m_methods;
        }

        /* renamed from: W */
        public Object mo13W() {
            return new C0432G(this);
        }

        /* renamed from: a */
        public Object mo14a(C0495Gr gr) {
            gr.getArgs();
            switch (gr.mo2417hq() - TaskletImpl._m_methodCount) {
                case 0:
                    m32878pi();
                    return null;
                case 1:
                    m32877fg();
                    return null;
                case 2:
                    return akk();
                case 3:
                    return m32873eO();
                case 4:
                    return new Integer(m32866PK());
                case 5:
                    return new Integer(akm());
                case 6:
                    return new Long(m32880rY());
                case 7:
                    return new Long(m32874eU());
                default:
                    return super.mo14a(gr);
            }
        }

        /* renamed from: a */
        public void mo15a(Collection collection, C0495Gr gr) {
            super.mo15a(collection, gr);
        }

        public C2618a akl() {
            switch (bFf().mo6893i(bxO)) {
                case 0:
                    return null;
                case 2:
                    return (C2618a) bFf().mo5606d(new aCE(this, bxO, new Object[0]));
                case 3:
                    bFf().mo5606d(new aCE(this, bxO, new Object[0]));
                    break;
            }
            return akk();
        }

        public int akn() {
            switch (bFf().mo6893i(bxP)) {
                case 0:
                    return 0;
                case 2:
                    return ((Integer) bFf().mo5606d(new aCE(this, bxP, new Object[0]))).intValue();
                case 3:
                    bFf().mo5606d(new aCE(this, bxP, new Object[0]));
                    break;
            }
            return akm();
        }

        /* renamed from: c */
        public C5663aRz[] mo25c() {
            return _m_fields;
        }

        public void dispose() {
            switch (bFf().mo6893i(_f_dispose_0020_0028_0029V)) {
                case 0:
                    return;
                case 2:
                    bFf().mo5606d(new aCE(this, _f_dispose_0020_0028_0029V, new Object[0]));
                    return;
                case 3:
                    bFf().mo5606d(new aCE(this, _f_dispose_0020_0028_0029V, new Object[0]));
                    break;
            }
            m32877fg();
        }

        /* renamed from: eP */
        public ItemType mo19329eP() {
            switch (bFf().mo6893i(f8018lk)) {
                case 0:
                    return null;
                case 2:
                    return (ItemType) bFf().mo5606d(new aCE(this, f8018lk, new Object[0]));
                case 3:
                    bFf().mo5606d(new aCE(this, f8018lk, new Object[0]));
                    break;
            }
            return m32873eO();
        }

        /* renamed from: eV */
        public long mo19330eV() {
            switch (bFf().mo6893i(f8019ln)) {
                case 0:
                    return 0;
                case 2:
                    return ((Long) bFf().mo5606d(new aCE(this, f8019ln, new Object[0]))).longValue();
                case 3:
                    bFf().mo5606d(new aCE(this, f8019ln, new Object[0]));
                    break;
            }
            return m32874eU();
        }

        /* access modifiers changed from: protected */
        /* renamed from: pj */
        public void mo667pj() {
            switch (bFf().mo6893i(f8006JD)) {
                case 0:
                    return;
                case 2:
                    bFf().mo5606d(new aCE(this, f8006JD, new Object[0]));
                    return;
                case 3:
                    bFf().mo5606d(new aCE(this, f8006JD, new Object[0]));
                    break;
            }
            m32878pi();
        }

        /* renamed from: rZ */
        public long mo19331rZ() {
            switch (bFf().mo6893i(f8008MY)) {
                case 0:
                    return 0;
                case 2:
                    return ((Long) bFf().mo5606d(new aCE(this, f8008MY, new Object[0]))).longValue();
                case 3:
                    bFf().mo5606d(new aCE(this, f8008MY, new Object[0]));
                    break;
            }
            return m32880rY();
        }

        /* renamed from: a */
        public void mo19326a(Store hfVar, C2618a aVar, ItemType jCVar, int i, int i2, long j, long j2) {
            m32869a(hfVar);
            super.mo4706l(hfVar);
            m32868a(aVar);
            m32876f(jCVar);
            m32870cZ(i);
            m32875eU(i2);
            m32864G(j);
            m32871dl(j2);
        }

        @C0064Am(aul = "50d10a8e31373aa7ceb2385bf98bdb0c", aum = 0)
        /* renamed from: pi */
        private void m32878pi() {
            akj().dCf().remove(m32872eA());
            akj().m32790b(akg(), m32872eA(), m32865PF(), akh(), m32879rU(), aki(), 0.0f);
            dispose();
        }

        @C0064Am(aul = "3f4412ebbfa962f356d4c1f85756e49a", aum = 0)
        /* renamed from: fg */
        private void m32877fg() {
            m32876f((ItemType) null);
            m32868a((C2618a) null);
            super.dispose();
        }

        @C0064Am(aul = "992e445624d5261b08949d4b3b911c0a", aum = 0)
        private C2618a akk() {
            return akg();
        }

        @C0064Am(aul = "1ec7ec814f0aa9e2a0c34d5244b1f97a", aum = 0)
        /* renamed from: eO */
        private ItemType m32873eO() {
            return m32872eA();
        }

        @C0064Am(aul = "fdd3078a0c529de08718039291af9600", aum = 0)
        /* renamed from: PK */
        private int m32866PK() {
            return m32865PF();
        }

        @C0064Am(aul = "a92d75062ec3840f918cfd823f84920e", aum = 0)
        private int akm() {
            return akh();
        }

        @C0064Am(aul = "8f62a981edde73edb3b91f90d03ea9b5", aum = 0)
        /* renamed from: rY */
        private long m32880rY() {
            return m32879rU();
        }

        @C0064Am(aul = "598d422090266360ed04bb22a2529a71", aum = 0)
        /* renamed from: eU */
        private long m32874eU() {
            return aki();
        }
    }

    @C6485anp
    @C5511aMd
    /* renamed from: a.hf$d */
    /* compiled from: a */
    public class StoreAnyBuyOrder extends BuyOrder implements C1616Xf {
        public static final C2491fm _f_dispose_0020_0028_0029V = null;
        public static final int _m_fieldCount = 0;
        public static final C5663aRz[] _m_fields = null;
        public static final int _m_methodCount = 0;
        public static final C2491fm[] _m_methods = null;
        /* renamed from: aT */
        public static final C5663aRz f8020aT = null;
        public static final C5663aRz bIq = null;
        public static final C2491fm dpF = null;
        public static final long serialVersionUID = 0;
        public static C6494any ___iScriptClass;
        @C0064Am(aul = "9f6649969be1ce23f46d42fc9eb1cbaf", aum = 1)

        /* renamed from: de */
        static /* synthetic */ Store f8021de;
        @C0064Am(aul = "345c6e819d410019011fd4e3ad1f6428", aum = 0)

        /* renamed from: zF */
        private static C0847MM f8022zF;

        static {
            m32894V();
        }

        public StoreAnyBuyOrder() {
            super((C5540aNg) null);
            super.mo10S();
        }

        public StoreAnyBuyOrder(C5540aNg ang) {
            super(ang);
        }

        public StoreAnyBuyOrder(Store hfVar, C0847MM mm, C0847MM mm2, ItemType jCVar, int i, int i2, long j, Station bf, long j2) {
            super((C5540aNg) null);
            super._m_script_init(hfVar, mm, mm2, jCVar, i, i2, j, bf, j2);
        }

        /* renamed from: V */
        static void m32894V() {
            serialVersionUID = (long) 1;
            _m_fieldCount = BuyOrder._m_fieldCount + 2;
            _m_methodCount = BuyOrder._m_methodCount + 2;
            int i = BuyOrder._m_fieldCount;
            C5663aRz[] arzArr = new C5663aRz[(i + 2)];
            C5663aRz b = C5640aRc.m17844b(StoreAnyBuyOrder.class, "345c6e819d410019011fd4e3ad1f6428", i);
            bIq = b;
            arzArr[i] = b;
            int i2 = i + 1;
            C5663aRz b2 = C5640aRc.m17844b(StoreAnyBuyOrder.class, "9f6649969be1ce23f46d42fc9eb1cbaf", i2);
            f8020aT = b2;
            arzArr[i2] = b2;
            int i3 = i2 + 1;
            _m_fields = arzArr;
            C1634Xv.m11725a((Object[]) BuyOrder._m_fields, (Object[]) _m_fields);
            int i4 = BuyOrder._m_methodCount;
            C2491fm[] fmVarArr = new C2491fm[(i4 + 2)];
            C2491fm a = C4105zY.m41624a(StoreAnyBuyOrder.class, "f10d43fb1d2e412e057f5e166618c44c", i4);
            _f_dispose_0020_0028_0029V = a;
            fmVarArr[i4] = a;
            int i5 = i4 + 1;
            C2491fm a2 = C4105zY.m41624a(StoreAnyBuyOrder.class, "5dc2bfb38840910258338ec0deb13d3a", i5);
            dpF = a2;
            fmVarArr[i5] = a2;
            int i6 = i5 + 1;
            _m_methods = fmVarArr;
            C1634Xv.m11725a((Object[]) BuyOrder._m_methods, (Object[]) _m_methods);
            ___iScriptClass = aUO.m18566a(StoreAnyBuyOrder.class, C2315dv.class, _m_fields, _m_methods);
        }

        /* renamed from: a */
        private void m32897a(Store hfVar) {
            bFf().mo5608dq().mo3197f(f8020aT, hfVar);
        }

        private Store akj() {
            return (Store) bFf().mo5608dq().mo3214p(f8020aT);
        }

        /* access modifiers changed from: private */
        public C0847MM aoW() {
            return (C0847MM) bFf().mo5608dq().mo3214p(bIq);
        }

        /* renamed from: b */
        private void m32898b(C0847MM mm) {
            bFf().mo5608dq().mo3197f(bIq, mm);
        }

        /* renamed from: S */
        public void mo10S() {
            super.mo10S();
        }

        /* renamed from: T */
        public C6494any mo11T() {
            return ___iScriptClass;
        }

        /* renamed from: U */
        public C2491fm[] mo12U() {
            return _m_methods;
        }

        /* renamed from: W */
        public Object mo13W() {
            return new C2315dv(this);
        }

        /* renamed from: a */
        public Object mo14a(C0495Gr gr) {
            Object[] args = gr.getArgs();
            switch (gr.mo2417hq() - BuyOrder._m_methodCount) {
                case 0:
                    m32899fg();
                    return null;
                case 1:
                    m32896a((C0847MM) args[0], (Item) args[1]);
                    return null;
                default:
                    return super.mo14a(gr);
            }
        }

        /* renamed from: a */
        public void mo15a(Collection collection, C0495Gr gr) {
            super.mo15a(collection, gr);
        }

        /* renamed from: b */
        public void mo3666b(C0847MM mm, Item auq) {
            switch (bFf().mo6893i(dpF)) {
                case 0:
                    return;
                case 2:
                    bFf().mo5606d(new aCE(this, dpF, new Object[]{mm, auq}));
                    return;
                case 3:
                    bFf().mo5606d(new aCE(this, dpF, new Object[]{mm, auq}));
                    break;
            }
            m32896a(mm, auq);
        }

        /* renamed from: c */
        public C5663aRz[] mo25c() {
            return _m_fields;
        }

        public void dispose() {
            switch (bFf().mo6893i(_f_dispose_0020_0028_0029V)) {
                case 0:
                    return;
                case 2:
                    bFf().mo5606d(new aCE(this, _f_dispose_0020_0028_0029V, new Object[0]));
                    return;
                case 3:
                    bFf().mo5606d(new aCE(this, _f_dispose_0020_0028_0029V, new Object[0]));
                    break;
            }
            m32899fg();
        }

        /* renamed from: a */
        public void mo19332a(Store hfVar, C0847MM mm, C0847MM mm2, ItemType jCVar, int i, int i2, long j, Station bf, long j2) {
            m32897a(hfVar);
            super.mo3664a(mm2, jCVar, i, i2, j, bf, j2);
            m32898b(mm);
        }

        @C0064Am(aul = "f10d43fb1d2e412e057f5e166618c44c", aum = 0)
        /* renamed from: fg */
        private void m32899fg() {
            ((ListContainer) akj().dCi().get(aoW())).remove(this);
            if (((ListContainer) akj().dCi().get(aoW())).getList().isEmpty()) {
                akj().dCi().remove(aoW());
            }
            m32898b((C0847MM) null);
            super.dispose();
        }

        @C0064Am(aul = "5dc2bfb38840910258338ec0deb13d3a", aum = 0)
        /* renamed from: a */
        private void m32896a(C0847MM mm, Item auq) {
            if (mm != aoW()) {
                throw new C3402rH(C3402rH.C3403a.INVALID_CALL_PLAYER);
            }
            super.mo3666b(mm, auq);
        }
    }
}
