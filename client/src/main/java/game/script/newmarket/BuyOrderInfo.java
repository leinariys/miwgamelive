package game.script.newmarket;

import game.network.message.externalizable.aCE;
import game.script.player.Player;
import logic.baa.C1616Xf;
import logic.baa.C1634Xv;
import logic.baa.C5511aMd;
import logic.baa.C6485anp;
import logic.data.mbean.C3393rB;
import logic.res.code.C5640aRc;
import logic.res.code.C5663aRz;
import logic.res.code.C6494any;
import logic.res.html.C2491fm;
import p001a.*;

import java.util.Collection;

@C5511aMd
@C6485anp
/* renamed from: a.sf */
/* compiled from: a */
public class BuyOrderInfo extends CommercialOrderInfo implements C1616Xf {
    public static final C2491fm _f_dispose_0020_0028_0029V = null;
    public static final int _m_fieldCount = 0;
    public static final C5663aRz[] _m_fields = null;
    public static final int _m_methodCount = 0;
    public static final C2491fm[] _m_methods = null;
    public static final C5663aRz bfD = null;
    public static final C5663aRz bfE = null;
    public static final C2491fm bfF = null;
    public static final C2491fm bfG = null;
    public static final C2491fm bfH = null;
    public static final C2491fm bfI = null;
    public static final C2491fm bfJ = null;
    /* renamed from: gQ */
    public static final C2491fm f9146gQ = null;
    public static final long serialVersionUID = 0;
    public static C6494any ___iScriptClass;
    @C0064Am(aul = "e9a202f1ce243242187751f25b0833b9", aum = 0)
    @C5566aOg
    private static BuyOrder bbh;
    @C0064Am(aul = "546b1c5782cc302b179e877446748f61", aum = 1)
    private static C0847MM bbi;

    static {
        m39058V();
    }

    public BuyOrderInfo() {
        super((C5540aNg) null);
        super.mo10S();
    }

    public BuyOrderInfo(BuyOrder kl) {
        super((C5540aNg) null);
        super._m_script_init(kl);
    }

    public BuyOrderInfo(C5540aNg ang) {
        super(ang);
    }

    /* renamed from: V */
    static void m39058V() {
        serialVersionUID = (long) 1;
        _m_fieldCount = CommercialOrderInfo._m_fieldCount + 2;
        _m_methodCount = CommercialOrderInfo._m_methodCount + 7;
        int i = CommercialOrderInfo._m_fieldCount;
        C5663aRz[] arzArr = new C5663aRz[(i + 2)];
        C5663aRz b = C5640aRc.m17844b(BuyOrderInfo.class, "e9a202f1ce243242187751f25b0833b9", i);
        bfD = b;
        arzArr[i] = b;
        int i2 = i + 1;
        C5663aRz b2 = C5640aRc.m17844b(BuyOrderInfo.class, "546b1c5782cc302b179e877446748f61", i2);
        bfE = b2;
        arzArr[i2] = b2;
        int i3 = i2 + 1;
        _m_fields = arzArr;
        C1634Xv.m11725a((Object[]) CommercialOrderInfo._m_fields, (Object[]) _m_fields);
        int i4 = CommercialOrderInfo._m_methodCount;
        C2491fm[] fmVarArr = new C2491fm[(i4 + 7)];
        C2491fm a = C4105zY.m41624a(BuyOrderInfo.class, "c3016770fd1dd141b9c2bc0e23582e7f", i4);
        bfF = a;
        fmVarArr[i4] = a;
        int i5 = i4 + 1;
        C2491fm a2 = C4105zY.m41624a(BuyOrderInfo.class, "21afd7977b9e4c5b2c7476b8b8fbe440", i5);
        bfG = a2;
        fmVarArr[i5] = a2;
        int i6 = i5 + 1;
        C2491fm a3 = C4105zY.m41624a(BuyOrderInfo.class, "d30e5970ea0d6cdcb4fdbe2ede3aa5c2", i6);
        _f_dispose_0020_0028_0029V = a3;
        fmVarArr[i6] = a3;
        int i7 = i6 + 1;
        C2491fm a4 = C4105zY.m41624a(BuyOrderInfo.class, "45835b76b0fd9769247e3aa4c9f0eab4", i7);
        bfH = a4;
        fmVarArr[i7] = a4;
        int i8 = i7 + 1;
        C2491fm a5 = C4105zY.m41624a(BuyOrderInfo.class, "22fbf04aac0e88262248ddd0a00ec354", i8);
        bfI = a5;
        fmVarArr[i8] = a5;
        int i9 = i8 + 1;
        C2491fm a6 = C4105zY.m41624a(BuyOrderInfo.class, "0f882f7c10470a38a4d0b03820584d2d", i9);
        f9146gQ = a6;
        fmVarArr[i9] = a6;
        int i10 = i9 + 1;
        C2491fm a7 = C4105zY.m41624a(BuyOrderInfo.class, "41da9b8f9836ed881a43d332e65011b6", i10);
        bfJ = a7;
        fmVarArr[i10] = a7;
        int i11 = i10 + 1;
        _m_methods = fmVarArr;
        C1634Xv.m11725a((Object[]) CommercialOrderInfo._m_methods, (Object[]) _m_methods);
        ___iScriptClass = aUO.m18566a(BuyOrderInfo.class, C3393rB.class, _m_fields, _m_methods);
    }

    /* renamed from: a */
    private void m39059a(BuyOrder kl) {
        bFf().mo5608dq().mo3197f(bfD, kl);
    }

    /* renamed from: a */
    private void m39060a(C0847MM mm) {
        bFf().mo5608dq().mo3197f(bfE, mm);
    }

    private BuyOrder abb() {
        return (BuyOrder) bFf().mo5608dq().mo3214p(bfD);
    }

    private C0847MM abc() {
        return (C0847MM) bFf().mo5608dq().mo3214p(bfE);
    }

    @C0064Am(aul = "41da9b8f9836ed881a43d332e65011b6", aum = 0)
    @C5566aOg
    private void abl() {
        throw new aWi(new aCE(this, bfJ, new Object[0]));
    }

    @C0064Am(aul = "0f882f7c10470a38a4d0b03820584d2d", aum = 0)
    @C5566aOg
    /* renamed from: dd */
    private void m39061dd() {
        throw new aWi(new aCE(this, f9146gQ, new Object[0]));
    }

    /* renamed from: S */
    public void mo10S() {
        super.mo10S();
    }

    /* renamed from: T */
    public C6494any mo11T() {
        return ___iScriptClass;
    }

    /* renamed from: U */
    public C2491fm[] mo12U() {
        return _m_methods;
    }

    /* renamed from: W */
    public Object mo13W() {
        return new C3393rB(this);
    }

    /* renamed from: a */
    public Object mo14a(C0495Gr gr) {
        gr.getArgs();
        switch (gr.mo2417hq() - CommercialOrderInfo._m_methodCount) {
            case 0:
                return abd();
            case 1:
                return abf();
            case 2:
                m39062fg();
                return null;
            case 3:
                return abh();
            case 4:
                abj();
                return null;
            case 5:
                m39061dd();
                return null;
            case 6:
                abl();
                return null;
            default:
                return super.mo14a(gr);
        }
    }

    /* renamed from: a */
    public void mo15a(Collection collection, C0495Gr gr) {
        super.mo15a(collection, gr);
    }

    public BuyOrder abe() {
        switch (bFf().mo6893i(bfF)) {
            case 0:
                return null;
            case 2:
                return (BuyOrder) bFf().mo5606d(new aCE(this, bfF, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, bfF, new Object[0]));
                break;
        }
        return abd();
    }

    public C0847MM abg() {
        switch (bFf().mo6893i(bfG)) {
            case 0:
                return null;
            case 2:
                return (C0847MM) bFf().mo5606d(new aCE(this, bfG, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, bfG, new Object[0]));
                break;
        }
        return abf();
    }

    public C0847MM abi() {
        switch (bFf().mo6893i(bfH)) {
            case 0:
                return null;
            case 2:
                return (C0847MM) bFf().mo5606d(new aCE(this, bfH, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, bfH, new Object[0]));
                break;
        }
        return abh();
    }

    public void abk() {
        switch (bFf().mo6893i(bfI)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, bfI, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, bfI, new Object[0]));
                break;
        }
        abj();
    }

    @C5566aOg
    public void abm() {
        switch (bFf().mo6893i(bfJ)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, bfJ, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, bfJ, new Object[0]));
                break;
        }
        abl();
    }

    /* renamed from: c */
    public C5663aRz[] mo25c() {
        return _m_fields;
    }

    @C5566aOg
    /* renamed from: de */
    public void mo22013de() {
        switch (bFf().mo6893i(f9146gQ)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f9146gQ, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f9146gQ, new Object[0]));
                break;
        }
        m39061dd();
    }

    public void dispose() {
        switch (bFf().mo6893i(_f_dispose_0020_0028_0029V)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, _f_dispose_0020_0028_0029V, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, _f_dispose_0020_0028_0029V, new Object[0]));
                break;
        }
        m39062fg();
    }

    /* renamed from: b */
    public void mo22012b(BuyOrder kl) {
        super.mo8624f((CommercialOrder) kl);
        m39059a(kl);
        m39060a(kl.abg());
    }

    @C0064Am(aul = "c3016770fd1dd141b9c2bc0e23582e7f", aum = 0)
    private BuyOrder abd() {
        return abb();
    }

    @C0064Am(aul = "21afd7977b9e4c5b2c7476b8b8fbe440", aum = 0)
    private C0847MM abf() {
        return abc();
    }

    @C0064Am(aul = "d30e5970ea0d6cdcb4fdbe2ede3aa5c2", aum = 0)
    /* renamed from: fg */
    private void m39062fg() {
        super.dispose();
        abb().mo9448g(this);
        m39059a((BuyOrder) null);
        m39060a((C0847MM) null);
    }

    @C0064Am(aul = "45835b76b0fd9769247e3aa4c9f0eab4", aum = 0)
    private C0847MM abh() {
        return abg();
    }

    @C0064Am(aul = "22fbf04aac0e88262248ddd0a00ec354", aum = 0)
    private void abj() {
        m39059a((BuyOrder) null);
        if (abc() instanceof Player) {
            m39060a((C0847MM) null);
        }
    }
}
