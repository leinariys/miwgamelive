package game.script.newmarket;

import game.network.message.externalizable.aCE;
import game.script.bank.Bank;
import game.script.item.Item;
import game.script.item.ItemType;
import game.script.player.Player;
import game.script.ship.Station;
import logic.baa.C1616Xf;
import logic.baa.C1634Xv;
import logic.baa.C5511aMd;
import logic.baa.C6485anp;
import logic.data.mbean.C5259aCl;
import logic.res.code.C5640aRc;
import logic.res.code.C5663aRz;
import logic.res.code.C6494any;
import logic.res.html.C2491fm;
import logic.thred.LogPrinter;
import org.apache.commons.logging.Log;
import p001a.*;

import java.util.Collection;

@C5566aOg
@C5511aMd
@C6485anp
/* renamed from: a.Kl */
/* compiled from: a */
public class BuyOrder extends CommercialOrder implements C1616Xf {
    public static final C2491fm _f_dispose_0020_0028_0029V = null;
    public static final int _m_fieldCount = 0;
    public static final C5663aRz[] _m_fields = null;
    public static final int _m_methodCount = 0;
    public static final C2491fm[] _m_methods = null;
    public static final C5663aRz bfE = null;
    public static final C2491fm bfG = null;
    public static final C2491fm dpF = null;
    /* renamed from: lD */
    public static final C2491fm f1011lD = null;
    /* renamed from: li */
    public static final C2491fm f1012li = null;
    /* renamed from: ly */
    public static final C2491fm f1013ly = null;
    public static final long serialVersionUID = 0;
    private static final String dpD = "Buy Order Supplied";
    private static final String dpE = "Supplying Buy Order";
    private static final Log logger = LogPrinter.setClass(BuyOrder.class);
    public static C6494any ___iScriptClass = null;
    @C0064Am(aul = "536a14b1f91d3d929d6d0b330045e4b1", aum = 0)
    private static C0847MM bbi = null;

    static {
        m6610V();
    }

    public BuyOrder() {
        super((C5540aNg) null);
        super.mo10S();
    }

    public BuyOrder(C0847MM mm, ItemType jCVar, int i, int i2, long j, Station bf, long j2) {
        super((C5540aNg) null);
        super._m_script_init(mm, jCVar, i, i2, j, bf, j2);
    }

    public BuyOrder(C5540aNg ang) {
        super(ang);
    }

    /* renamed from: V */
    static void m6610V() {
        serialVersionUID = (long) 1;
        _m_fieldCount = CommercialOrder._m_fieldCount + 1;
        _m_methodCount = CommercialOrder._m_methodCount + 6;
        int i = CommercialOrder._m_fieldCount;
        C5663aRz[] arzArr = new C5663aRz[(i + 1)];
        C5663aRz b = C5640aRc.m17844b(BuyOrder.class, "536a14b1f91d3d929d6d0b330045e4b1", i);
        bfE = b;
        arzArr[i] = b;
        int i2 = i + 1;
        _m_fields = arzArr;
        C1634Xv.m11725a((Object[]) CommercialOrder._m_fields, (Object[]) _m_fields);
        int i3 = CommercialOrder._m_methodCount;
        C2491fm[] fmVarArr = new C2491fm[(i3 + 6)];
        C2491fm a = C4105zY.m41624a(BuyOrder.class, "c92c1538edef565296496b6b7e57d573", i3);
        f1013ly = a;
        fmVarArr[i3] = a;
        int i4 = i3 + 1;
        C2491fm a2 = C4105zY.m41624a(BuyOrder.class, "83a771a6b3b9215326f6751143ee3b05", i4);
        f1012li = a2;
        fmVarArr[i4] = a2;
        int i5 = i4 + 1;
        C2491fm a3 = C4105zY.m41624a(BuyOrder.class, "470da1d402f84323795adbcee5fba067", i5);
        dpF = a3;
        fmVarArr[i5] = a3;
        int i6 = i5 + 1;
        C2491fm a4 = C4105zY.m41624a(BuyOrder.class, "f619806b8f4e2e174e3629d470a82481", i6);
        _f_dispose_0020_0028_0029V = a4;
        fmVarArr[i6] = a4;
        int i7 = i6 + 1;
        C2491fm a5 = C4105zY.m41624a(BuyOrder.class, "28cf0aac12aa8ef1e4a6f02efb3ee4bc", i7);
        bfG = a5;
        fmVarArr[i7] = a5;
        int i8 = i7 + 1;
        C2491fm a6 = C4105zY.m41624a(BuyOrder.class, "4247d3b8dc46645b8e74d8a9ade5ebcc", i8);
        f1011lD = a6;
        fmVarArr[i8] = a6;
        int i9 = i8 + 1;
        _m_methods = fmVarArr;
        C1634Xv.m11725a((Object[]) CommercialOrder._m_methods, (Object[]) _m_methods);
        ___iScriptClass = aUO.m18566a(BuyOrder.class, C5259aCl.class, _m_fields, _m_methods);
    }

    /* renamed from: a */
    private void m6611a(C0847MM mm) {
        bFf().mo5608dq().mo3197f(bfE, mm);
    }

    @C0064Am(aul = "470da1d402f84323795adbcee5fba067", aum = 0)
    @C5566aOg
    /* renamed from: a */
    private void m6612a(C0847MM mm, Item auq) {
        throw new aWi(new aCE(this, dpF, new Object[]{mm, auq}));
    }

    private C0847MM abc() {
        return (C0847MM) bFf().mo5608dq().mo3214p(bfE);
    }

    @C0064Am(aul = "c92c1538edef565296496b6b7e57d573", aum = 0)
    @C5566aOg
    /* renamed from: fl */
    private void m6615fl() {
        throw new aWi(new aCE(this, f1013ly, new Object[0]));
    }

    /* renamed from: S */
    public void mo10S() {
        super.mo10S();
    }

    /* renamed from: T */
    public C6494any mo11T() {
        return ___iScriptClass;
    }

    /* renamed from: U */
    public C2491fm[] mo12U() {
        return _m_methods;
    }

    /* renamed from: W */
    public Object mo13W() {
        return new C5259aCl(this);
    }

    /* renamed from: a */
    public Object mo14a(C0495Gr gr) {
        Object[] args = gr.getArgs();
        switch (gr.mo2417hq() - CommercialOrder._m_methodCount) {
            case 0:
                m6615fl();
                return null;
            case 1:
                m6613eM();
                return null;
            case 2:
                m6612a((C0847MM) args[0], (Item) args[1]);
                return null;
            case 3:
                m6614fg();
                return null;
            case 4:
                return abf();
            case 5:
                return m6616fp();
            default:
                return super.mo14a(gr);
        }
    }

    /* renamed from: a */
    public void mo15a(Collection collection, C0495Gr gr) {
        super.mo15a(collection, gr);
    }

    public C0847MM abg() {
        switch (bFf().mo6893i(bfG)) {
            case 0:
                return null;
            case 2:
                return (C0847MM) bFf().mo5606d(new aCE(this, bfG, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, bfG, new Object[0]));
                break;
        }
        return abf();
    }

    @C5566aOg
    /* renamed from: b */
    public void mo3666b(C0847MM mm, Item auq) {
        switch (bFf().mo6893i(dpF)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, dpF, new Object[]{mm, auq}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, dpF, new Object[]{mm, auq}));
                break;
        }
        m6612a(mm, auq);
    }

    /* renamed from: c */
    public C5663aRz[] mo25c() {
        return _m_fields;
    }

    public void dispose() {
        switch (bFf().mo6893i(_f_dispose_0020_0028_0029V)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, _f_dispose_0020_0028_0029V, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, _f_dispose_0020_0028_0029V, new Object[0]));
                break;
        }
        m6614fg();
    }

    /* access modifiers changed from: protected */
    /* renamed from: eN */
    public void mo3667eN() {
        switch (bFf().mo6893i(f1012li)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f1012li, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f1012li, new Object[0]));
                break;
        }
        m6613eM();
    }

    @C5566aOg
    /* renamed from: fm */
    public void mo3668fm() {
        switch (bFf().mo6893i(f1013ly)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f1013ly, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f1013ly, new Object[0]));
                break;
        }
        m6615fl();
    }

    /* renamed from: fq */
    public CommercialOrderInfo mo3669fq() {
        switch (bFf().mo6893i(f1011lD)) {
            case 0:
                return null;
            case 2:
                return (CommercialOrderInfo) bFf().mo5606d(new aCE(this, f1011lD, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f1011lD, new Object[0]));
                break;
        }
        return m6616fp();
    }

    /* renamed from: a */
    public void mo3664a(C0847MM mm, ItemType jCVar, int i, int i2, long j, Station bf, long j2) {
        super.mo9434a(jCVar, i, i2, j, bf, j2);
        m6611a(mm);
        if ((mm instanceof Player) && !ala().aIS().mo12608d((C1165RF) abc(), mo9438eR() * ((long) i), Bank.C1861a.MARKET_BUY_ORDER_CREATE, abc().bhE().getName(), mo9441eZ())) {
            throw new C6172aho(C6172aho.C1894a.NO_BALANCE);
        }
    }

    @C0064Am(aul = "83a771a6b3b9215326f6751143ee3b05", aum = 0)
    /* renamed from: eM */
    private void m6613eM() {
        if (!isDisposed()) {
            if ((abc() instanceof Player) && mo9444ff() > 0 && !((Player) abc()).isDisposed()) {
                ala().aIS().mo12602b((C1165RF) abc(), mo9438eR() * ((long) mo9444ff()), Bank.C1861a.MARKET_BUY_ORDER_RETURN, abc().bhE().getName(), mo9444ff());
            }
            mo9445fi().mo4379d(this);
            abc().mo3918a(this);
            dispose();
        }
    }

    @C0064Am(aul = "f619806b8f4e2e174e3629d470a82481", aum = 0)
    /* renamed from: fg */
    private void m6614fg() {
        super.dispose();
        m6611a((C0847MM) null);
    }

    @C0064Am(aul = "28cf0aac12aa8ef1e4a6f02efb3ee4bc", aum = 0)
    private C0847MM abf() {
        return abc();
    }

    @C0064Am(aul = "4247d3b8dc46645b8e74d8a9ade5ebcc", aum = 0)
    /* renamed from: fp */
    private CommercialOrderInfo m6616fp() {
        BuyOrderInfo sfVar = (BuyOrderInfo) bFf().mo6865M(BuyOrderInfo.class);
        sfVar.mo22012b(this);
        return sfVar;
    }
}
