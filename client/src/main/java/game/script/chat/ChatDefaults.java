package game.script.chat;

import game.network.message.externalizable.aCE;
import game.network.message.serializable.C1556Wo;
import logic.baa.*;
import logic.data.mbean.C5959adj;
import logic.res.code.C5640aRc;
import logic.res.code.C5663aRz;
import logic.res.code.C6494any;
import logic.res.html.C2491fm;
import p001a.*;
import taikodom.infra.script.I18NString;

import java.util.*;

@C5511aMd
@C6485anp
/* renamed from: a.kh */
/* compiled from: a */
public class ChatDefaults extends TaikodomObject implements C0468GU, C1616Xf {
    public static final String EMPTY = "<empty>";
    public static final C2491fm _f_onResurrect_0020_0028_0029V = null;
    public static final int _m_fieldCount = 0;
    public static final C5663aRz[] _m_fields = null;
    public static final int _m_methodCount = 0;
    public static final C2491fm[] _m_methods = null;
    public static final C5663aRz asL = null;
    public static final C2491fm asM = null;
    public static final C2491fm asN = null;
    /* renamed from: bL */
    public static final C5663aRz f8454bL = null;
    /* renamed from: bN */
    public static final C2491fm f8455bN = null;
    /* renamed from: bO */
    public static final C2491fm f8456bO = null;
    /* renamed from: bP */
    public static final C2491fm f8457bP = null;
    public static final long serialVersionUID = 0;
    public static C6494any ___iScriptClass;
    @C0064Am(aul = "2bf94b76aef044335864931bc8601bad", aum = 0)
    private static C1556Wo<aFU, I18NString> asK;
    @C0064Am(aul = "b074cd21520b1bc5f594520f49f58d84", aum = 1)

    /* renamed from: bK */
    private static UUID f8453bK;

    static {
        m34520V();
    }

    public ChatDefaults() {
        super((C5540aNg) null);
        super.mo10S();
    }

    public ChatDefaults(C5540aNg ang) {
        super(ang);
    }

    /* renamed from: V */
    static void m34520V() {
        serialVersionUID = (long) 1;
        _m_fieldCount = TaikodomObject._m_fieldCount + 2;
        _m_methodCount = TaikodomObject._m_methodCount + 6;
        int i = TaikodomObject._m_fieldCount;
        C5663aRz[] arzArr = new C5663aRz[(i + 2)];
        C5663aRz b = C5640aRc.m17844b(ChatDefaults.class, "2bf94b76aef044335864931bc8601bad", i);
        asL = b;
        arzArr[i] = b;
        int i2 = i + 1;
        C5663aRz b2 = C5640aRc.m17844b(ChatDefaults.class, "b074cd21520b1bc5f594520f49f58d84", i2);
        f8454bL = b2;
        arzArr[i2] = b2;
        int i3 = i2 + 1;
        _m_fields = arzArr;
        C1634Xv.m11725a((Object[]) TaikodomObject._m_fields, (Object[]) _m_fields);
        int i4 = TaikodomObject._m_methodCount;
        C2491fm[] fmVarArr = new C2491fm[(i4 + 6)];
        C2491fm a = C4105zY.m41624a(ChatDefaults.class, "239fce3055663ce4a52a02a988a610a8", i4);
        f8455bN = a;
        fmVarArr[i4] = a;
        int i5 = i4 + 1;
        C2491fm a2 = C4105zY.m41624a(ChatDefaults.class, "a82703547d2294466288eb6de6b1d128", i5);
        f8456bO = a2;
        fmVarArr[i5] = a2;
        int i6 = i5 + 1;
        C2491fm a3 = C4105zY.m41624a(ChatDefaults.class, "57e959160722627ade9ef5c74491898a", i6);
        f8457bP = a3;
        fmVarArr[i6] = a3;
        int i7 = i6 + 1;
        C2491fm a4 = C4105zY.m41624a(ChatDefaults.class, "bf84b461a41966da6c77097dc90e90d1", i7);
        asM = a4;
        fmVarArr[i7] = a4;
        int i8 = i7 + 1;
        C2491fm a5 = C4105zY.m41624a(ChatDefaults.class, "63749dce91e847ee50d75b23f9d7b2d7", i8);
        asN = a5;
        fmVarArr[i8] = a5;
        int i9 = i8 + 1;
        C2491fm a6 = C4105zY.m41624a(ChatDefaults.class, "83a9b45c425d9214af2e30cf4f912c8e", i9);
        _f_onResurrect_0020_0028_0029V = a6;
        fmVarArr[i9] = a6;
        int i10 = i9 + 1;
        _m_methods = fmVarArr;
        C1634Xv.m11725a((Object[]) TaikodomObject._m_methods, (Object[]) _m_methods);
        ___iScriptClass = aUO.m18566a(ChatDefaults.class, C5959adj.class, _m_fields, _m_methods);
    }

    /* renamed from: JE */
    private C1556Wo m34518JE() {
        return (C1556Wo) bFf().mo5608dq().mo3214p(asL);
    }

    @C5566aOg
    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Contexts")
    @C0064Am(aul = "63749dce91e847ee50d75b23f9d7b2d7", aum = 0)
    /* renamed from: a */
    private void m34521a(Map<aFU, I18NString> map) {
        throw new aWi(new aCE(this, asN, new Object[]{map}));
    }

    /* renamed from: a */
    private void m34522a(UUID uuid) {
        bFf().mo5608dq().mo3197f(f8454bL, uuid);
    }

    /* renamed from: an */
    private UUID m34524an() {
        return (UUID) bFf().mo5608dq().mo3214p(f8454bL);
    }

    /* renamed from: c */
    private void m34528c(UUID uuid) {
        switch (bFf().mo6893i(f8456bO)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f8456bO, new Object[]{uuid}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f8456bO, new Object[]{uuid}));
                break;
        }
        m34527b(uuid);
    }

    /* renamed from: h */
    private void m34529h(C1556Wo wo) {
        bFf().mo5608dq().mo3197f(asL, wo);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Contexts")
    /* renamed from: JG */
    public Map<aFU, I18NString> mo20117JG() {
        switch (bFf().mo6893i(asM)) {
            case 0:
                return null;
            case 2:
                return (Map) bFf().mo5606d(new aCE(this, asM, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, asM, new Object[0]));
                break;
        }
        return m34519JF();
    }

    /* renamed from: T */
    public C6494any mo11T() {
        return ___iScriptClass;
    }

    /* renamed from: U */
    public C2491fm[] mo12U() {
        return _m_methods;
    }

    /* renamed from: W */
    public Object mo13W() {
        return new C5959adj(this);
    }

    /* renamed from: a */
    public Object mo14a(C0495Gr gr) {
        Object[] args = gr.getArgs();
        switch (gr.mo2417hq() - TaikodomObject._m_methodCount) {
            case 0:
                return m34525ap();
            case 1:
                m34527b((UUID) args[0]);
                return null;
            case 2:
                return m34526ar();
            case 3:
                return m34519JF();
            case 4:
                m34521a((Map<aFU, I18NString>) (Map) args[0]);
                return null;
            case 5:
                m34523aG();
                return null;
            default:
                return super.mo14a(gr);
        }
    }

    /* renamed from: a */
    public void mo15a(Collection collection, C0495Gr gr) {
        super.mo15a(collection, gr);
    }

    /* renamed from: aH */
    public void mo70aH() {
        switch (bFf().mo6893i(_f_onResurrect_0020_0028_0029V)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, _f_onResurrect_0020_0028_0029V, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, _f_onResurrect_0020_0028_0029V, new Object[0]));
                break;
        }
        m34523aG();
    }

    /* renamed from: aq */
    public UUID mo18aq() {
        switch (bFf().mo6893i(f8455bN)) {
            case 0:
                return null;
            case 2:
                return (UUID) bFf().mo5606d(new aCE(this, f8455bN, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f8455bN, new Object[0]));
                break;
        }
        return m34525ap();
    }

    /* renamed from: c */
    public C5663aRz[] mo25c() {
        return _m_fields;
    }

    public String getHandle() {
        switch (bFf().mo6893i(f8457bP)) {
            case 0:
                return null;
            case 2:
                return (String) bFf().mo5606d(new aCE(this, f8457bP, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f8457bP, new Object[0]));
                break;
        }
        return m34526ar();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Contexts")
    @C5566aOg
    public void setContextMap(Map<aFU, I18NString> map) {
        switch (bFf().mo6893i(asN)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, asN, new Object[]{map}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, asN, new Object[]{map}));
                break;
        }
        m34521a(map);
    }

    @C0064Am(aul = "239fce3055663ce4a52a02a988a610a8", aum = 0)
    /* renamed from: ap */
    private UUID m34525ap() {
        return m34524an();
    }

    @C0064Am(aul = "a82703547d2294466288eb6de6b1d128", aum = 0)
    /* renamed from: b */
    private void m34527b(UUID uuid) {
        m34522a(uuid);
    }

    @C0064Am(aul = "57e959160722627ade9ef5c74491898a", aum = 0)
    /* renamed from: ar */
    private String m34526ar() {
        return "chat_defaults";
    }

    /* renamed from: S */
    public void mo10S() {
        super.mo10S();
        for (aFU put : aFU.values()) {
            m34518JE().put(put, new I18NString(EMPTY));
        }
        m34522a(UUID.randomUUID());
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Contexts")
    @C0064Am(aul = "bf84b461a41966da6c77097dc90e90d1", aum = 0)
    /* renamed from: JF */
    private Map<aFU, I18NString> m34519JF() {
        HashMap hashMap = new HashMap();
        hashMap.putAll(m34518JE());
        return Collections.unmodifiableMap(hashMap);
    }

    @C0064Am(aul = "83a9b45c425d9214af2e30cf4f912c8e", aum = 0)
    /* renamed from: aG */
    private void m34523aG() {
        for (aFU afu : aFU.values()) {
            if (!m34518JE().containsKey(afu) && afu != aFU.TRADE) {
                m34518JE().put(afu, new I18NString(EMPTY));
            }
        }
        m34518JE().remove(aFU.TRADE);
        super.mo70aH();
    }
}
