package game.script.mines;

import game.network.message.externalizable.aCE;
import game.script.item.ProjectileWeaponType;
import logic.baa.*;
import logic.data.mbean.C5670aSg;
import logic.res.code.C5663aRz;
import logic.res.code.C6494any;
import logic.res.html.C2491fm;
import p001a.*;

import java.util.Collection;

@C0566Hp
@C5829abJ("2.1.1")
@C6485anp
@C5511aMd
/* renamed from: a.LH */
/* compiled from: a */
public class MineThrowerWeaponType extends ProjectileWeaponType implements C1616Xf {
    public static final int _m_fieldCount = 0;
    public static final C5663aRz[] _m_fields = null;
    public static final int _m_methodCount = 0;
    public static final C2491fm[] _m_methods = null;
    /* renamed from: dN */
    public static final C2491fm f1018dN = null;
    public static final C2491fm dwe = null;
    public static final long serialVersionUID = 0;
    public static C6494any ___iScriptClass;

    static {
        m6662V();
    }

    public MineThrowerWeaponType() {
        super((C5540aNg) null);
        super.mo10S();
    }

    public MineThrowerWeaponType(C5540aNg ang) {
        super(ang);
    }

    /* renamed from: V */
    static void m6662V() {
        serialVersionUID = (long) 1;
        _m_fieldCount = ProjectileWeaponType._m_fieldCount + 0;
        _m_methodCount = ProjectileWeaponType._m_methodCount + 2;
        _m_fields = new C5663aRz[(ProjectileWeaponType._m_fieldCount + 0)];
        C1634Xv.m11725a((Object[]) ProjectileWeaponType._m_fields, (Object[]) _m_fields);
        int i = ProjectileWeaponType._m_methodCount;
        C2491fm[] fmVarArr = new C2491fm[(i + 2)];
        C2491fm a = C4105zY.m41624a(MineThrowerWeaponType.class, "25e27909a5972e175971f5291284ce03", i);
        dwe = a;
        fmVarArr[i] = a;
        int i2 = i + 1;
        C2491fm a2 = C4105zY.m41624a(MineThrowerWeaponType.class, "3886ca1f079732d6f850e73b56cc58ad", i2);
        f1018dN = a2;
        fmVarArr[i2] = a2;
        int i3 = i2 + 1;
        _m_methods = fmVarArr;
        C1634Xv.m11725a((Object[]) ProjectileWeaponType._m_methods, (Object[]) _m_methods);
        ___iScriptClass = aUO.m18566a(MineThrowerWeaponType.class, C5670aSg.class, _m_fields, _m_methods);
    }

    /* renamed from: T */
    public C6494any mo11T() {
        return ___iScriptClass;
    }

    /* renamed from: U */
    public C2491fm[] mo12U() {
        return _m_methods;
    }

    /* renamed from: W */
    public Object mo13W() {
        return new C5670aSg(this);
    }

    /* renamed from: a */
    public Object mo14a(C0495Gr gr) {
        gr.getArgs();
        switch (gr.mo2417hq() - ProjectileWeaponType._m_methodCount) {
            case 0:
                return bfW();
            case 1:
                return m6663aT();
            default:
                return super.mo14a(gr);
        }
    }

    /* renamed from: a */
    public void mo15a(Collection collection, C0495Gr gr) {
        super.mo15a(collection, gr);
    }

    /* access modifiers changed from: protected */
    /* renamed from: aU */
    public <T extends aDJ> T mo745aU() {
        switch (bFf().mo6893i(f1018dN)) {
            case 0:
                return null;
            case 2:
                return (aDJ) bFf().mo5606d(new aCE(this, f1018dN, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f1018dN, new Object[0]));
                break;
        }
        return m6663aT();
    }

    public MineThrowerWeapon bfX() {
        switch (bFf().mo6893i(dwe)) {
            case 0:
                return null;
            case 2:
                return (MineThrowerWeapon) bFf().mo5606d(new aCE(this, dwe, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, dwe, new Object[0]));
                break;
        }
        return bfW();
    }

    /* renamed from: c */
    public C5663aRz[] mo25c() {
        return _m_fields;
    }

    /* renamed from: S */
    public void mo10S() {
        super.mo10S();
    }

    @C0064Am(aul = "25e27909a5972e175971f5291284ce03", aum = 0)
    private MineThrowerWeapon bfW() {
        return (MineThrowerWeapon) mo745aU();
    }

    @C0064Am(aul = "3886ca1f079732d6f850e73b56cc58ad", aum = 0)
    /* renamed from: aT */
    private <T extends aDJ> T m6663aT() {
        T t = (MineThrowerWeapon) bFf().mo6865M(MineThrowerWeapon.class);
        t.mo5354a((ProjectileWeaponType) this);
        return t;
    }
}
