package game.script.mines;

import game.network.message.externalizable.aCE;
import game.script.resource.Asset;
import game.script.ship.HullType;
import logic.baa.*;
import logic.data.link.C1655YN;
import logic.data.mbean.C1153Qu;
import logic.res.code.C5640aRc;
import logic.res.code.C5663aRz;
import logic.res.code.C6494any;
import logic.res.html.C2491fm;
import p001a.*;

import java.util.Collection;

@C0566Hp
@C5829abJ("2.1.1")
@C6485anp
@C5511aMd
/* renamed from: a.aMx  reason: case insensitive filesystem */
/* compiled from: a */
public abstract class MineType extends BaseMineType implements C1616Xf {

    /* renamed from: VW */
    public static final C5663aRz f3407VW = null;
    /* renamed from: VY */
    public static final C5663aRz f3409VY = null;
    /* renamed from: Wa */
    public static final C5663aRz f3411Wa = null;
    /* renamed from: Wc */
    public static final C5663aRz f3413Wc = null;
    /* renamed from: We */
    public static final C5663aRz f3415We = null;
    /* renamed from: Wt */
    public static final C2491fm f3416Wt = null;
    public static final int _m_fieldCount = 0;
    public static final C5663aRz[] _m_fields = null;
    public static final int _m_methodCount = 0;
    public static final C2491fm[] _m_methods = null;
    public static final C2491fm aDR = null;
    public static final C2491fm aTr = null;
    public static final C2491fm dGQ = null;
    public static final C2491fm dGR = null;
    public static final C2491fm dGS = null;
    public static final C2491fm dGT = null;
    public static final C2491fm dHz = null;
    public static final C2491fm ipA = null;
    public static final C2491fm ipB = null;
    public static final C2491fm ipx = null;
    public static final C2491fm ipy = null;
    public static final C2491fm ipz = null;
    /* renamed from: kZ */
    public static final C5663aRz f3417kZ = null;
    public static final long serialVersionUID = 0;
    public static C6494any ___iScriptClass;
    @C0064Am(aul = "233911ad115afc77dea3fefe824440f3", aum = 1)

    /* renamed from: VX */
    private static float f3408VX;
    @C0064Am(aul = "2d1b97e3d181e3736505d0cb445eea50", aum = 2)

    /* renamed from: VZ */
    private static float f3410VZ;
    @C0064Am(aul = "0e416e861eff36de18ffca610ebe9c43", aum = 3)

    /* renamed from: Wb */
    private static Asset f3412Wb;
    @C0064Am(aul = "7b659f6462b71be36847e6f0fa51e249", aum = 4)

    /* renamed from: Wd */
    private static Asset f3414Wd;
    @C0064Am(aul = "5e830366332c5befbb4de2675d38f0ca", aum = 0)
    private static HullType bhn;
    @C0064Am(aul = "c68aaa071185a3673807c76f01760562", aum = 5)
    private static int lifeTime;

    static {
        m16493V();
    }

    public MineType() {
        super((C5540aNg) null);
        super.mo10S();
    }

    public MineType(C5540aNg ang) {
        super(ang);
    }

    /* renamed from: V */
    static void m16493V() {
        serialVersionUID = (long) 1;
        _m_fieldCount = BaseMineType._m_fieldCount + 6;
        _m_methodCount = BaseMineType._m_methodCount + 13;
        int i = BaseMineType._m_fieldCount;
        C5663aRz[] arzArr = new C5663aRz[(i + 6)];
        C5663aRz b = C5640aRc.m17844b(MineType.class, "5e830366332c5befbb4de2675d38f0ca", i);
        f3407VW = b;
        arzArr[i] = b;
        int i2 = i + 1;
        C5663aRz b2 = C5640aRc.m17844b(MineType.class, "233911ad115afc77dea3fefe824440f3", i2);
        f3409VY = b2;
        arzArr[i2] = b2;
        int i3 = i2 + 1;
        C5663aRz b3 = C5640aRc.m17844b(MineType.class, "2d1b97e3d181e3736505d0cb445eea50", i3);
        f3411Wa = b3;
        arzArr[i3] = b3;
        int i4 = i3 + 1;
        C5663aRz b4 = C5640aRc.m17844b(MineType.class, "0e416e861eff36de18ffca610ebe9c43", i4);
        f3413Wc = b4;
        arzArr[i4] = b4;
        int i5 = i4 + 1;
        C5663aRz b5 = C5640aRc.m17844b(MineType.class, "7b659f6462b71be36847e6f0fa51e249", i5);
        f3415We = b5;
        arzArr[i5] = b5;
        int i6 = i5 + 1;
        C5663aRz b6 = C5640aRc.m17844b(MineType.class, "c68aaa071185a3673807c76f01760562", i6);
        f3417kZ = b6;
        arzArr[i6] = b6;
        int i7 = i6 + 1;
        _m_fields = arzArr;
        C1634Xv.m11725a((Object[]) BaseMineType._m_fields, (Object[]) _m_fields);
        int i8 = BaseMineType._m_methodCount;
        C2491fm[] fmVarArr = new C2491fm[(i8 + 13)];
        C2491fm a = C4105zY.m41624a(MineType.class, "a4edc0c853049e2f8dc8d110b865864b", i8);
        aTr = a;
        fmVarArr[i8] = a;
        int i9 = i8 + 1;
        C2491fm a2 = C4105zY.m41624a(MineType.class, "1ee3cc1c72248c6207b07f4c30dec968", i9);
        dHz = a2;
        fmVarArr[i9] = a2;
        int i10 = i9 + 1;
        C2491fm a3 = C4105zY.m41624a(MineType.class, "0f3726b2c247a203cc1a2f2184ae4740", i10);
        f3416Wt = a3;
        fmVarArr[i10] = a3;
        int i11 = i10 + 1;
        C2491fm a4 = C4105zY.m41624a(MineType.class, "10c792f1d325f283ca67e1a505327aac", i11);
        ipx = a4;
        fmVarArr[i11] = a4;
        int i12 = i11 + 1;
        C2491fm a5 = C4105zY.m41624a(MineType.class, "20a0c2144be4303658b23bdbfe103d4b", i12);
        ipy = a5;
        fmVarArr[i12] = a5;
        int i13 = i12 + 1;
        C2491fm a6 = C4105zY.m41624a(MineType.class, "86bc9a5fd84ccaf7292c9800f9ce8f17", i13);
        ipz = a6;
        fmVarArr[i13] = a6;
        int i14 = i13 + 1;
        C2491fm a7 = C4105zY.m41624a(MineType.class, "e41764666d089de0a4b7714f2c1bbb4a", i14);
        dGQ = a7;
        fmVarArr[i14] = a7;
        int i15 = i14 + 1;
        C2491fm a8 = C4105zY.m41624a(MineType.class, "6e20e07909c2d27d4645ad16f2d38491", i15);
        dGR = a8;
        fmVarArr[i15] = a8;
        int i16 = i15 + 1;
        C2491fm a9 = C4105zY.m41624a(MineType.class, "678cbb4bacf4dca2192cf469d8887b33", i16);
        dGS = a9;
        fmVarArr[i16] = a9;
        int i17 = i16 + 1;
        C2491fm a10 = C4105zY.m41624a(MineType.class, "1742da107a126ef7fd5ee6822182e077", i17);
        dGT = a10;
        fmVarArr[i17] = a10;
        int i18 = i17 + 1;
        C2491fm a11 = C4105zY.m41624a(MineType.class, "474dc50f57ae0e69b83b42120877d03b", i18);
        ipA = a11;
        fmVarArr[i18] = a11;
        int i19 = i18 + 1;
        C2491fm a12 = C4105zY.m41624a(MineType.class, "f655530542f888952b5dfcb0b346cbb9", i19);
        ipB = a12;
        fmVarArr[i19] = a12;
        int i20 = i19 + 1;
        C2491fm a13 = C4105zY.m41624a(MineType.class, "5822473d15bdecfc2864403e28933b84", i20);
        aDR = a13;
        fmVarArr[i20] = a13;
        int i21 = i20 + 1;
        _m_methods = fmVarArr;
        C1634Xv.m11725a((Object[]) BaseMineType._m_methods, (Object[]) _m_methods);
        ___iScriptClass = aUO.m18566a(MineType.class, C1153Qu.class, _m_fields, _m_methods);
    }

    /* renamed from: A */
    private void m16492A(Asset tCVar) {
        bFf().mo5608dq().mo3197f(f3415We, tCVar);
    }

    /* renamed from: aw */
    private void m16497aw(float f) {
        bFf().mo5608dq().mo3150a(f3409VY, f);
    }

    /* renamed from: ax */
    private void m16498ax(float f) {
        bFf().mo5608dq().mo3150a(f3411Wa, f);
    }

    private HullType bjZ() {
        return (HullType) bFf().mo5608dq().mo3214p(f3407VW);
    }

    /* renamed from: br */
    private void m16499br(int i) {
        bFf().mo5608dq().mo3183b(f3417kZ, i);
    }

    /* renamed from: f */
    private void m16501f(HullType wHVar) {
        bFf().mo5608dq().mo3197f(f3407VW, wHVar);
    }

    /* renamed from: z */
    private void m16506z(Asset tCVar) {
        bFf().mo5608dq().mo3197f(f3413Wc, tCVar);
    }

    /* renamed from: zj */
    private float m16508zj() {
        return bFf().mo5608dq().mo3211m(f3409VY);
    }

    /* renamed from: zk */
    private float m16509zk() {
        return bFf().mo5608dq().mo3211m(f3411Wa);
    }

    /* renamed from: zl */
    private Asset m16510zl() {
        return (Asset) bFf().mo5608dq().mo3214p(f3413Wc);
    }

    /* renamed from: zm */
    private Asset m16511zm() {
        return (Asset) bFf().mo5608dq().mo3214p(f3415We);
    }

    /* renamed from: zn */
    private int m16512zn() {
        return bFf().mo5608dq().mo3212n(f3417kZ);
    }

    /* renamed from: T */
    public C6494any mo11T() {
        return ___iScriptClass;
    }

    /* renamed from: U */
    public C2491fm[] mo12U() {
        return _m_methods;
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Hull")
    /* renamed from: VT */
    public HullType mo10187VT() {
        switch (bFf().mo6893i(aTr)) {
            case 0:
                return null;
            case 2:
                return (HullType) bFf().mo5606d(new aCE(this, aTr, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, aTr, new Object[0]));
                break;
        }
        return m16494VS();
    }

    /* renamed from: W */
    public Object mo13W() {
        return new C1153Qu(this);
    }

    /* renamed from: a */
    public Object mo14a(C0495Gr gr) {
        Object[] args = gr.getArgs();
        switch (gr.mo2417hq() - BaseMineType._m_methodCount) {
            case 0:
                return m16494VS();
            case 1:
                m16502g((HullType) args[0]);
                return null;
            case 2:
                return new Float(m16507zC());
            case 3:
                m16503nb(((Float) args[0]).floatValue());
                return null;
            case 4:
                return new Float(djt());
            case 5:
                m16504nd(((Float) args[0]).floatValue());
                return null;
            case 6:
                return bkg();
            case 7:
                m16495aR((Asset) args[0]);
                return null;
            case 8:
                return bki();
            case 9:
                m16496aT((Asset) args[0]);
                return null;
            case 10:
                return new Integer(djv());
            case 11:
                m16505yJ(((Integer) args[0]).intValue());
                return null;
            case 12:
                m16500e((aDJ) args[0]);
                return null;
            default:
                return super.mo14a(gr);
        }
    }

    /* renamed from: a */
    public void mo15a(Collection collection, C0495Gr gr) {
        super.mo15a(collection, gr);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Explosion G F X")
    /* renamed from: aS */
    public void mo10188aS(Asset tCVar) {
        switch (bFf().mo6893i(dGR)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, dGR, new Object[]{tCVar}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, dGR, new Object[]{tCVar}));
                break;
        }
        m16495aR(tCVar);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Explosion S F X")
    /* renamed from: aU */
    public void mo10189aU(Asset tCVar) {
        switch (bFf().mo6893i(dGT)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, dGT, new Object[]{tCVar}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, dGT, new Object[]{tCVar}));
                break;
        }
        m16496aT(tCVar);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Explosion G F X")
    public Asset bkh() {
        switch (bFf().mo6893i(dGQ)) {
            case 0:
                return null;
            case 2:
                return (Asset) bFf().mo5606d(new aCE(this, dGQ, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, dGQ, new Object[0]));
                break;
        }
        return bkg();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Explosion S F X")
    public Asset bkj() {
        switch (bFf().mo6893i(dGS)) {
            case 0:
                return null;
            case 2:
                return (Asset) bFf().mo5606d(new aCE(this, dGS, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, dGS, new Object[0]));
                break;
        }
        return bki();
    }

    /* renamed from: c */
    public C5663aRz[] mo25c() {
        return _m_fields;
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Detonation Radius")
    public float dju() {
        switch (bFf().mo6893i(ipy)) {
            case 0:
                return 0.0f;
            case 2:
                return ((Float) bFf().mo5606d(new aCE(this, ipy, new Object[0]))).floatValue();
            case 3:
                bFf().mo5606d(new aCE(this, ipy, new Object[0]));
                break;
        }
        return djt();
    }

    /* renamed from: f */
    public void mo2631f(aDJ adj) {
        switch (bFf().mo6893i(aDR)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, aDR, new Object[]{adj}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, aDR, new Object[]{adj}));
                break;
        }
        m16500e(adj);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Life Time")
    public int getLifeTime() {
        switch (bFf().mo6893i(ipA)) {
            case 0:
                return 0;
            case 2:
                return ((Integer) bFf().mo5606d(new aCE(this, ipA, new Object[0]))).intValue();
            case 3:
                bFf().mo5606d(new aCE(this, ipA, new Object[0]));
                break;
        }
        return djv();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Life Time")
    public void setLifeTime(int i) {
        switch (bFf().mo6893i(ipB)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, ipB, new Object[]{new Integer(i)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, ipB, new Object[]{new Integer(i)}));
                break;
        }
        m16505yJ(i);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Hull")
    /* renamed from: h */
    public void mo10194h(HullType wHVar) {
        switch (bFf().mo6893i(dHz)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, dHz, new Object[]{wHVar}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, dHz, new Object[]{wHVar}));
                break;
        }
        m16502g(wHVar);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Explosion Radius")
    /* renamed from: nc */
    public void mo10195nc(float f) {
        switch (bFf().mo6893i(ipx)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, ipx, new Object[]{new Float(f)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, ipx, new Object[]{new Float(f)}));
                break;
        }
        m16503nb(f);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Detonation Radius")
    /* renamed from: ne */
    public void mo10196ne(float f) {
        switch (bFf().mo6893i(ipz)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, ipz, new Object[]{new Float(f)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, ipz, new Object[]{new Float(f)}));
                break;
        }
        m16504nd(f);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Explosion Radius")
    /* renamed from: zD */
    public float mo10198zD() {
        switch (bFf().mo6893i(f3416Wt)) {
            case 0:
                return 0.0f;
            case 2:
                return ((Float) bFf().mo5606d(new aCE(this, f3416Wt, new Object[0]))).floatValue();
            case 3:
                bFf().mo5606d(new aCE(this, f3416Wt, new Object[0]));
                break;
        }
        return m16507zC();
    }

    /* renamed from: S */
    public void mo10S() {
        super.mo10S();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Hull")
    @C0064Am(aul = "a4edc0c853049e2f8dc8d110b865864b", aum = 0)
    /* renamed from: VS */
    private HullType m16494VS() {
        return bjZ();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Hull")
    @C0064Am(aul = "1ee3cc1c72248c6207b07f4c30dec968", aum = 0)
    /* renamed from: g */
    private void m16502g(HullType wHVar) {
        m16501f(wHVar);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Explosion Radius")
    @C0064Am(aul = "0f3726b2c247a203cc1a2f2184ae4740", aum = 0)
    /* renamed from: zC */
    private float m16507zC() {
        return m16508zj();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Explosion Radius")
    @C0064Am(aul = "10c792f1d325f283ca67e1a505327aac", aum = 0)
    /* renamed from: nb */
    private void m16503nb(float f) {
        m16497aw(f);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Detonation Radius")
    @C0064Am(aul = "20a0c2144be4303658b23bdbfe103d4b", aum = 0)
    private float djt() {
        return m16509zk();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Detonation Radius")
    @C0064Am(aul = "86bc9a5fd84ccaf7292c9800f9ce8f17", aum = 0)
    /* renamed from: nd */
    private void m16504nd(float f) {
        m16498ax(f);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Explosion G F X")
    @C0064Am(aul = "e41764666d089de0a4b7714f2c1bbb4a", aum = 0)
    private Asset bkg() {
        return m16510zl();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Explosion G F X")
    @C0064Am(aul = "6e20e07909c2d27d4645ad16f2d38491", aum = 0)
    /* renamed from: aR */
    private void m16495aR(Asset tCVar) {
        m16506z(tCVar);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Explosion S F X")
    @C0064Am(aul = "678cbb4bacf4dca2192cf469d8887b33", aum = 0)
    private Asset bki() {
        return m16511zm();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Explosion S F X")
    @C0064Am(aul = "1742da107a126ef7fd5ee6822182e077", aum = 0)
    /* renamed from: aT */
    private void m16496aT(Asset tCVar) {
        m16492A(tCVar);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Life Time")
    @C0064Am(aul = "474dc50f57ae0e69b83b42120877d03b", aum = 0)
    private int djv() {
        return m16512zn();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Life Time")
    @C0064Am(aul = "f655530542f888952b5dfcb0b346cbb9", aum = 0)
    /* renamed from: yJ */
    private void m16505yJ(int i) {
        m16499br(i);
    }

    @C0064Am(aul = "5822473d15bdecfc2864403e28933b84", aum = 0)
    /* renamed from: e */
    private void m16500e(aDJ adj) {
        super.mo2631f(adj);
        if (adj instanceof Mine) {
            C1616Xf xf = adj;
            if (bjZ() != null) {
                xf.mo6765g(C1655YN.aMH, bjZ().mo7459NK());
            }
        }
    }
}
