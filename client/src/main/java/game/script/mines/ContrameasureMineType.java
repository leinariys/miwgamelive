package game.script.mines;

import game.network.message.externalizable.aCE;
import logic.baa.*;
import logic.data.mbean.C4126zq;
import logic.res.code.C5663aRz;
import logic.res.code.C6494any;
import logic.res.html.C2491fm;
import p001a.*;

import java.util.Collection;

@C0566Hp
@C5829abJ("2.1.1")
@C6485anp
@C5511aMd
/* renamed from: a.Lh */
/* compiled from: a */
public class ContrameasureMineType extends MineType implements C1616Xf {
    public static final int _m_fieldCount = 0;
    public static final C5663aRz[] _m_fields = null;
    public static final int _m_methodCount = 0;
    public static final C2491fm[] _m_methods = null;
    /* renamed from: dN */
    public static final C2491fm f1055dN = null;
    public static final C2491fm dtr = null;
    public static final long serialVersionUID = 0;
    public static C6494any ___iScriptClass;

    static {
        m6842V();
    }

    public ContrameasureMineType() {
        super((C5540aNg) null);
        super.mo10S();
    }

    public ContrameasureMineType(C5540aNg ang) {
        super(ang);
    }

    /* renamed from: V */
    static void m6842V() {
        serialVersionUID = (long) 1;
        _m_fieldCount = MineType._m_fieldCount + 0;
        _m_methodCount = MineType._m_methodCount + 2;
        _m_fields = new C5663aRz[(MineType._m_fieldCount + 0)];
        C1634Xv.m11725a((Object[]) MineType._m_fields, (Object[]) _m_fields);
        int i = MineType._m_methodCount;
        C2491fm[] fmVarArr = new C2491fm[(i + 2)];
        C2491fm a = C4105zY.m41624a(ContrameasureMineType.class, "a8790adbfe4f124fbc33ff5c92a7ac30", i);
        dtr = a;
        fmVarArr[i] = a;
        int i2 = i + 1;
        C2491fm a2 = C4105zY.m41624a(ContrameasureMineType.class, "dc061ae135ffdd27547da4835ada1788", i2);
        f1055dN = a2;
        fmVarArr[i2] = a2;
        int i3 = i2 + 1;
        _m_methods = fmVarArr;
        C1634Xv.m11725a((Object[]) MineType._m_methods, (Object[]) _m_methods);
        ___iScriptClass = aUO.m18566a(ContrameasureMineType.class, C4126zq.class, _m_fields, _m_methods);
    }

    /* renamed from: T */
    public C6494any mo11T() {
        return ___iScriptClass;
    }

    /* renamed from: U */
    public C2491fm[] mo12U() {
        return _m_methods;
    }

    /* renamed from: W */
    public Object mo13W() {
        return new C4126zq(this);
    }

    /* renamed from: a */
    public Object mo14a(C0495Gr gr) {
        gr.getArgs();
        switch (gr.mo2417hq() - MineType._m_methodCount) {
            case 0:
                return beY();
            case 1:
                return m6843aT();
            default:
                return super.mo14a(gr);
        }
    }

    /* renamed from: a */
    public void mo15a(Collection collection, C0495Gr gr) {
        super.mo15a(collection, gr);
    }

    /* access modifiers changed from: protected */
    /* renamed from: aU */
    public <T extends aDJ> T mo745aU() {
        switch (bFf().mo6893i(f1055dN)) {
            case 0:
                return null;
            case 2:
                return (aDJ) bFf().mo5606d(new aCE(this, f1055dN, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f1055dN, new Object[0]));
                break;
        }
        return m6843aT();
    }

    public ContrameasureMine beZ() {
        switch (bFf().mo6893i(dtr)) {
            case 0:
                return null;
            case 2:
                return (ContrameasureMine) bFf().mo5606d(new aCE(this, dtr, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, dtr, new Object[0]));
                break;
        }
        return beY();
    }

    /* renamed from: c */
    public C5663aRz[] mo25c() {
        return _m_fields;
    }

    /* renamed from: S */
    public void mo10S() {
        super.mo10S();
    }

    @C0064Am(aul = "a8790adbfe4f124fbc33ff5c92a7ac30", aum = 0)
    private ContrameasureMine beY() {
        return (ContrameasureMine) mo745aU();
    }

    @C0064Am(aul = "dc061ae135ffdd27547da4835ada1788", aum = 0)
    /* renamed from: aT */
    private <T extends aDJ> T m6843aT() {
        T t = (ContrameasureMine) bFf().mo6865M(ContrameasureMine.class);
        t.mo8695a(this);
        return t;
    }
}
