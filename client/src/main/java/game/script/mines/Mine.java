package game.script.mines;

import com.hoplon.geometry.Vec3f;
import game.geometry.Vec3d;
import game.network.message.externalizable.aCE;
import game.script.Actor;
import game.script.MineAreaTrigger;
import game.script.resource.Asset;
import game.script.ship.Hull;
import game.script.ship.Shield;
import game.script.ship.Ship;
import game.script.simulation.Space;
import game.script.space.Node;
import game.script.space.StellarSystem;
import logic.aaa.C2235dB;
import logic.baa.*;
import logic.bbb.C6348alI;
import logic.data.link.C2530gR;
import logic.data.mbean.C5766aVy;
import logic.res.code.C5640aRc;
import logic.res.code.C5663aRz;
import logic.res.code.C6494any;
import logic.res.html.C2491fm;
import org.mozilla1.javascript.ScriptRuntime;
import p001a.*;

import java.util.Collection;
import java.util.List;

@C5829abJ("2.1.1")
@C6485anp
@C2712iu(mo19786Bt = BaseMineType.class, version = "2.1.1")
@C5511aMd
/* renamed from: a.hS */
/* compiled from: a */
public abstract class Mine extends Actor implements C1616Xf, aDA, C2530gR.C2531a {

    /* renamed from: AG */
    public static final C5663aRz f7900AG = null;

    /* renamed from: Lm */
    public static final C2491fm f7901Lm = null;

    /* renamed from: Mq */
    public static final C2491fm f7902Mq = null;
    /* renamed from: VU */
    public static final C5663aRz f7904VU = null;
    /* renamed from: VW */
    public static final C5663aRz f7906VW = null;
    /* renamed from: VY */
    public static final C5663aRz f7908VY = null;
    /* renamed from: Wa */
    public static final C5663aRz f7910Wa = null;
    /* renamed from: Wc */
    public static final C5663aRz f7912Wc = null;
    /* renamed from: We */
    public static final C5663aRz f7914We = null;
    /* renamed from: Wh */
    public static final C5663aRz f7916Wh = null;
    /* renamed from: Wj */
    public static final C2491fm f7918Wj = null;
    /* renamed from: Wk */
    public static final C2491fm f7919Wk = null;
    /* renamed from: Wl */
    public static final C2491fm f7920Wl = null;
    /* renamed from: Wm */
    public static final C2491fm f7921Wm = null;
    /* renamed from: Wn */
    public static final C2491fm f7922Wn = null;
    /* renamed from: Wo */
    public static final C2491fm f7923Wo = null;
    /* renamed from: Wp */
    public static final C2491fm f7924Wp = null;
    /* renamed from: Wq */
    public static final C2491fm f7925Wq = null;
    /* renamed from: Wr */
    public static final C2491fm f7926Wr = null;
    /* renamed from: Ws */
    public static final C2491fm f7927Ws = null;
    /* renamed from: Wt */
    public static final C2491fm f7928Wt = null;
    /* renamed from: Wu */
    public static final C2491fm f7929Wu = null;
    /* renamed from: Wv */
    public static final C2491fm f7930Wv = null;
    /* renamed from: Ww */
    public static final C2491fm f7931Ww = null;
    /* renamed from: Wx */
    public static final C2491fm f7932Wx = null;
    /* renamed from: Wy */
    public static final C2491fm f7933Wy = null;
    /* renamed from: Wz */
    public static final C2491fm f7934Wz = null;
    public static final C2491fm _f_dispose_0020_0028_0029V = null;
    public static final C2491fm _f_getType_0020_0028_0029Ljava_002flang_002fObject_003b = null;
    public static final C2491fm _f_onResurrect_0020_0028_0029V = null;
    public static final C2491fm _f_step_0020_0028F_0029V = null;
    public static final int _m_fieldCount = 0;
    public static final C5663aRz[] _m_fields = null;
    public static final int _m_methodCount = 0;
    public static final C2491fm[] _m_methods = null;
    /* renamed from: kZ */
    public static final C5663aRz f7935kZ = null;
    public static final long serialVersionUID = 0;
    /* renamed from: uK */
    public static final C5663aRz f7937uK = null;
    /* renamed from: uW */
    public static final C2491fm f7938uW = null;
    /* renamed from: uY */
    public static final C2491fm f7939uY = null;
    /* renamed from: vc */
    public static final C2491fm f7940vc = null;
    /* renamed from: ve */
    public static final C2491fm f7941ve = null;
    /* renamed from: vi */
    public static final C2491fm f7942vi = null;
    /* renamed from: vm */
    public static final C2491fm f7943vm = null;
    /* renamed from: vn */
    public static final C2491fm f7944vn = null;
    /* renamed from: vo */
    public static final C2491fm f7945vo = null;
    /* renamed from: vs */
    public static final C2491fm f7946vs = null;
    public static C6494any ___iScriptClass;
    @C0064Am(aul = "4b198774fbf9c3518f3a4b6451f9810b", aum = 0)
    @C5566aOg

    /* renamed from: VT */
    private static MineAreaTrigger f7903VT;
    @C0064Am(aul = "ef5520d81f7a1a928e424e9ef86bdfa7", aum = 1)
    @C0803Ld

    /* renamed from: VV */
    private static Hull f7905VV;
    @C0064Am(aul = "c1a63fd572b12a5b744fbd6e3e7ccb56", aum = 2)

    /* renamed from: VX */
    private static float f7907VX;
    @C0064Am(aul = "e1d44812e8dbd862992599290c491f59", aum = 3)

    /* renamed from: VZ */
    private static float f7909VZ;
    @C0064Am(aul = "2b104f988c368f2abb733401f734c049", aum = 4)

    /* renamed from: Wb */
    private static Asset f7911Wb;
    @C0064Am(aul = "45de38e54254c7c5b8942a819d1a56cc", aum = 5)

    /* renamed from: Wd */
    private static Asset f7913Wd;
    @C0064Am(aul = "eb1c061aa36d0cb1be31d67fb179c480", aum = 7)
    @C5256aCi

    /* renamed from: Wg */
    private static boolean f7915Wg;
    @C0064Am(aul = "ff8153164faab4dbdcc845f1a6d7b026", aum = 9)

    /* renamed from: Wi */
    private static MineThrowerWeapon f7917Wi;
    @C0064Am(aul = "b569c45f09b80399d8d3ae61a7f2d846", aum = 6)
    @C5256aCi
    private static int lifeTime;
    @C0064Am(aul = "8bdc871591a9956c3ab1714cd8a58a13", aum = 8)
    @C1020Ot(drz = C1020Ot.C1021a.NOT_PERSISTED)

    /* renamed from: uJ */
    private static C5260aCm f7936uJ;

    static {
        m32608V();
    }

    @ClientOnly

    /* renamed from: Wf */
    private float f7947Wf;

    public Mine() {
        super((C5540aNg) null);
        super.mo10S();
    }

    public Mine(MineType amx) {
        super((C5540aNg) null);
        super._m_script_init(amx);
    }

    public Mine(C5540aNg ang) {
        super(ang);
    }

    /* renamed from: V */
    static void m32608V() {
        serialVersionUID = (long) 1;
        _m_fieldCount = Actor._m_fieldCount + 10;
        _m_methodCount = Actor._m_methodCount + 32;
        int i = Actor._m_fieldCount;
        C5663aRz[] arzArr = new C5663aRz[(i + 10)];
        C5663aRz b = C5640aRc.m17844b(Mine.class, "4b198774fbf9c3518f3a4b6451f9810b", i);
        f7904VU = b;
        arzArr[i] = b;
        int i2 = i + 1;
        C5663aRz b2 = C5640aRc.m17844b(Mine.class, "ef5520d81f7a1a928e424e9ef86bdfa7", i2);
        f7906VW = b2;
        arzArr[i2] = b2;
        int i3 = i2 + 1;
        C5663aRz b3 = C5640aRc.m17844b(Mine.class, "c1a63fd572b12a5b744fbd6e3e7ccb56", i3);
        f7908VY = b3;
        arzArr[i3] = b3;
        int i4 = i3 + 1;
        C5663aRz b4 = C5640aRc.m17844b(Mine.class, "e1d44812e8dbd862992599290c491f59", i4);
        f7910Wa = b4;
        arzArr[i4] = b4;
        int i5 = i4 + 1;
        C5663aRz b5 = C5640aRc.m17844b(Mine.class, "2b104f988c368f2abb733401f734c049", i5);
        f7912Wc = b5;
        arzArr[i5] = b5;
        int i6 = i5 + 1;
        C5663aRz b6 = C5640aRc.m17844b(Mine.class, "45de38e54254c7c5b8942a819d1a56cc", i6);
        f7914We = b6;
        arzArr[i6] = b6;
        int i7 = i6 + 1;
        C5663aRz b7 = C5640aRc.m17844b(Mine.class, "b569c45f09b80399d8d3ae61a7f2d846", i7);
        f7935kZ = b7;
        arzArr[i7] = b7;
        int i8 = i7 + 1;
        C5663aRz b8 = C5640aRc.m17844b(Mine.class, "eb1c061aa36d0cb1be31d67fb179c480", i8);
        f7916Wh = b8;
        arzArr[i8] = b8;
        int i9 = i8 + 1;
        C5663aRz b9 = C5640aRc.m17844b(Mine.class, "8bdc871591a9956c3ab1714cd8a58a13", i9);
        f7937uK = b9;
        arzArr[i9] = b9;
        int i10 = i9 + 1;
        C5663aRz b10 = C5640aRc.m17844b(Mine.class, "ff8153164faab4dbdcc845f1a6d7b026", i10);
        f7900AG = b10;
        arzArr[i10] = b10;
        int i11 = i10 + 1;
        _m_fields = arzArr;
        C1634Xv.m11725a((Object[]) Actor._m_fields, (Object[]) _m_fields);
        int i12 = Actor._m_methodCount;
        C2491fm[] fmVarArr = new C2491fm[(i12 + 32)];
        C2491fm a = C4105zY.m41624a(Mine.class, "ae6344f8f81a7b6bd7c65ede1d4c2245", i12);
        f7918Wj = a;
        fmVarArr[i12] = a;
        int i13 = i12 + 1;
        C2491fm a2 = C4105zY.m41624a(Mine.class, "5b0222dbf66fbe8d1ada3554978ad0a5", i13);
        f7919Wk = a2;
        fmVarArr[i13] = a2;
        int i14 = i13 + 1;
        C2491fm a3 = C4105zY.m41624a(Mine.class, "39f048c1d63aa92d99e440f80138d397", i14);
        f7920Wl = a3;
        fmVarArr[i14] = a3;
        int i15 = i14 + 1;
        C2491fm a4 = C4105zY.m41624a(Mine.class, "1a42e95648c705af688c5f88b542d205", i15);
        f7921Wm = a4;
        fmVarArr[i15] = a4;
        int i16 = i15 + 1;
        C2491fm a5 = C4105zY.m41624a(Mine.class, "b64178637719a9609fec57a581a8db48", i16);
        f7902Mq = a5;
        fmVarArr[i16] = a5;
        int i17 = i16 + 1;
        C2491fm a6 = C4105zY.m41624a(Mine.class, "a8b86ade5bd7628337d7f8970714e041", i17);
        f7922Wn = a6;
        fmVarArr[i17] = a6;
        int i18 = i17 + 1;
        C2491fm a7 = C4105zY.m41624a(Mine.class, "169be3caa9df029b96323d8d2504dc6f", i18);
        f7923Wo = a7;
        fmVarArr[i18] = a7;
        int i19 = i18 + 1;
        C2491fm a8 = C4105zY.m41624a(Mine.class, "77ef2e413afb7585ed54ad19bd71d4da", i19);
        f7924Wp = a8;
        fmVarArr[i19] = a8;
        int i20 = i19 + 1;
        C2491fm a9 = C4105zY.m41624a(Mine.class, "ca2ac59cf92ae987b6df6c88547d0396", i20);
        f7943vm = a9;
        fmVarArr[i20] = a9;
        int i21 = i20 + 1;
        C2491fm a10 = C4105zY.m41624a(Mine.class, "37e47562ca054e451d1c0a1adb69f9f1", i21);
        f7944vn = a10;
        fmVarArr[i21] = a10;
        int i22 = i21 + 1;
        C2491fm a11 = C4105zY.m41624a(Mine.class, "c9d2bac8ea804acd8e44520a7f261d63", i22);
        f7945vo = a11;
        fmVarArr[i22] = a11;
        int i23 = i22 + 1;
        C2491fm a12 = C4105zY.m41624a(Mine.class, "c4fd82a678544b66a21109e6dca33fe4", i23);
        f7940vc = a12;
        fmVarArr[i23] = a12;
        int i24 = i23 + 1;
        C2491fm a13 = C4105zY.m41624a(Mine.class, "8a911a438934ab0356c359dc8a00197c", i24);
        f7941ve = a13;
        fmVarArr[i24] = a13;
        int i25 = i24 + 1;
        C2491fm a14 = C4105zY.m41624a(Mine.class, "76abb8e3fd4fb7afc79ebcaa00bc1e07", i25);
        f7942vi = a14;
        fmVarArr[i25] = a14;
        int i26 = i25 + 1;
        C2491fm a15 = C4105zY.m41624a(Mine.class, "00a2471ddd7a27564c7125073cfd635a", i26);
        f7925Wq = a15;
        fmVarArr[i26] = a15;
        int i27 = i26 + 1;
        C2491fm a16 = C4105zY.m41624a(Mine.class, "e0bb6f8425bd267f742b78d4f0583479", i27);
        f7926Wr = a16;
        fmVarArr[i27] = a16;
        int i28 = i27 + 1;
        C2491fm a17 = C4105zY.m41624a(Mine.class, "31dd3b8944fd85a40075ba637de539dd", i28);
        f7939uY = a17;
        fmVarArr[i28] = a17;
        int i29 = i28 + 1;
        C2491fm a18 = C4105zY.m41624a(Mine.class, "8c23d64c8739fc9a108503e572928ac8", i29);
        _f_onResurrect_0020_0028_0029V = a18;
        fmVarArr[i29] = a18;
        int i30 = i29 + 1;
        C2491fm a19 = C4105zY.m41624a(Mine.class, "17108ac4747992b384c615b9757d996b", i30);
        _f_step_0020_0028F_0029V = a19;
        fmVarArr[i30] = a19;
        int i31 = i30 + 1;
        C2491fm a20 = C4105zY.m41624a(Mine.class, "3dcb88b708e11a6acdf6d6ef195fd572", i31);
        f7927Ws = a20;
        fmVarArr[i31] = a20;
        int i32 = i31 + 1;
        C2491fm a21 = C4105zY.m41624a(Mine.class, "4c0d247aaa6fe43b1bb57981ff6add85", i32);
        f7946vs = a21;
        fmVarArr[i32] = a21;
        int i33 = i32 + 1;
        C2491fm a22 = C4105zY.m41624a(Mine.class, "0493c3d86c2144dac7546526764b0791", i33);
        f7928Wt = a22;
        fmVarArr[i33] = a22;
        int i34 = i33 + 1;
        C2491fm a23 = C4105zY.m41624a(Mine.class, "f4f15d0c43fa30e10ceeb0b38aed606f", i34);
        f7929Wu = a23;
        fmVarArr[i34] = a23;
        int i35 = i34 + 1;
        C2491fm a24 = C4105zY.m41624a(Mine.class, "18e18d85d3c8fca46b6de9f4236e5c17", i35);
        f7930Wv = a24;
        fmVarArr[i35] = a24;
        int i36 = i35 + 1;
        C2491fm a25 = C4105zY.m41624a(Mine.class, "f75330b394b12ea6c7f28ce7ec1057c9", i36);
        f7901Lm = a25;
        fmVarArr[i36] = a25;
        int i37 = i36 + 1;
        C2491fm a26 = C4105zY.m41624a(Mine.class, "841126ae654445ce524d6a9982cfd313", i37);
        f7931Ww = a26;
        fmVarArr[i37] = a26;
        int i38 = i37 + 1;
        C2491fm a27 = C4105zY.m41624a(Mine.class, "fae3b3cf8b132597b5b8d26be0d803bc", i38);
        _f_dispose_0020_0028_0029V = a27;
        fmVarArr[i38] = a27;
        int i39 = i38 + 1;
        C2491fm a28 = C4105zY.m41624a(Mine.class, "10e403bef2e60ca66b4b144c55011c0b", i39);
        f7938uW = a28;
        fmVarArr[i39] = a28;
        int i40 = i39 + 1;
        C2491fm a29 = C4105zY.m41624a(Mine.class, "dba1e9941f7680570853d0d2cb45ba2b", i40);
        f7932Wx = a29;
        fmVarArr[i40] = a29;
        int i41 = i40 + 1;
        C2491fm a30 = C4105zY.m41624a(Mine.class, "01787720fbba7c3d530ab07b5b595407", i41);
        f7933Wy = a30;
        fmVarArr[i41] = a30;
        int i42 = i41 + 1;
        C2491fm a31 = C4105zY.m41624a(Mine.class, "72339145cc38d7c660b23551254e40cc", i42);
        f7934Wz = a31;
        fmVarArr[i42] = a31;
        int i43 = i42 + 1;
        C2491fm a32 = C4105zY.m41624a(Mine.class, "adebb1ef36db3eb246ef79b71737216f", i43);
        _f_getType_0020_0028_0029Ljava_002flang_002fObject_003b = a32;
        fmVarArr[i43] = a32;
        int i44 = i43 + 1;
        _m_methods = fmVarArr;
        C1634Xv.m11725a((Object[]) Actor._m_methods, (Object[]) _m_methods);
        ___iScriptClass = aUO.m18566a(Mine.class, C5766aVy.class, _m_fields, _m_methods);
    }

    /* renamed from: A */
    private void m32607A(Asset tCVar) {
        throw new C6039afL();
    }

    /* renamed from: V */
    private void m32609V(boolean z) {
        bFf().mo5608dq().mo3153a(f7916Wh, z);
    }

    /* renamed from: a */
    private void m32613a(C5260aCm acm) {
        bFf().mo5608dq().mo3197f(f7937uK, acm);
    }

    /* renamed from: a */
    private void m32615a(MineThrowerWeapon aue) {
        bFf().mo5608dq().mo3197f(f7900AG, aue);
    }

    /* renamed from: a */
    private void m32616a(Hull jRVar) {
        bFf().mo5608dq().mo3197f(f7906VW, jRVar);
    }

    /* renamed from: a */
    private void m32617a(MineAreaTrigger okVar) {
        bFf().mo5608dq().mo3197f(f7904VU, okVar);
    }

    /* renamed from: aw */
    private void m32619aw(float f) {
        throw new C6039afL();
    }

    /* renamed from: ax */
    private void m32620ax(float f) {
        throw new C6039afL();
    }

    @C0064Am(aul = "5b0222dbf66fbe8d1ada3554978ad0a5", aum = 0)
    @C5566aOg
    /* renamed from: b */
    private void m32624b(Hull jRVar, C5260aCm acm, Actor cr) {
        throw new aWi(new aCE(this, f7919Wk, new Object[]{jRVar, acm, cr}));
    }

    /* renamed from: br */
    private void m32625br(int i) {
        throw new C6039afL();
    }

    @C4034yP
    @C5566aOg
    @C0064Am(aul = "a8b86ade5bd7628337d7f8970714e041", aum = 0)
    @C2499fr
    /* renamed from: d */
    private void m32628d(Vec3d ajr) {
        throw new aWi(new aCE(this, f7922Wn, new Object[]{ajr}));
    }

    @C4034yP
    @C5566aOg
    @C0064Am(aul = "f4f15d0c43fa30e10ceeb0b38aed606f", aum = 0)
    @C2499fr(mo18857qh = 2)
    /* renamed from: e */
    private void m32629e(Actor cr, C5260aCm acm, Vec3f vec3f, Vec3f vec3f2) {
        throw new aWi(new aCE(this, f7929Wu, new Object[]{cr, acm, vec3f, vec3f2}));
    }

    @C0064Am(aul = "00a2471ddd7a27564c7125073cfd635a", aum = 0)
    /* renamed from: e */
    private void m32630e(List<Actor> list) {
        throw new aWi(new aCE(this, f7925Wq, new Object[]{list}));
    }

    /* renamed from: id */
    private C5260aCm m32632id() {
        return (C5260aCm) bFf().mo5608dq().mo3214p(f7937uK);
    }

    @C0064Am(aul = "f75330b394b12ea6c7f28ce7ec1057c9", aum = 0)
    @C5566aOg
    @C2499fr
    /* renamed from: qU */
    private void m32638qU() {
        throw new aWi(new aCE(this, f7901Lm, new Object[0]));
    }

    @C0064Am(aul = "adebb1ef36db3eb246ef79b71737216f", aum = 0)
    /* renamed from: qW */
    private Object m32639qW() {
        return mo19264zF();
    }

    /* renamed from: z */
    private void m32640z(Asset tCVar) {
        throw new C6039afL();
    }

    @C1253SX
    /* renamed from: zB */
    private void m32642zB() {
        switch (bFf().mo6893i(f7927Ws)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f7927Ws, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f7927Ws, new Object[0]));
                break;
        }
        m32641zA();
    }

    /* renamed from: zh */
    private MineAreaTrigger m32647zh() {
        return (MineAreaTrigger) bFf().mo5608dq().mo3214p(f7904VU);
    }

    /* renamed from: zi */
    private Hull m32648zi() {
        return (Hull) bFf().mo5608dq().mo3214p(f7906VW);
    }

    /* renamed from: zj */
    private float m32649zj() {
        return ((MineType) getType()).mo10198zD();
    }

    /* renamed from: zk */
    private float m32650zk() {
        return ((MineType) getType()).dju();
    }

    /* renamed from: zl */
    private Asset m32651zl() {
        return ((MineType) getType()).bkh();
    }

    /* renamed from: zm */
    private Asset m32652zm() {
        return ((MineType) getType()).bkj();
    }

    /* renamed from: zn */
    private int m32653zn() {
        return ((MineType) getType()).getLifeTime();
    }

    /* renamed from: zo */
    private boolean m32654zo() {
        return bFf().mo5608dq().mo3201h(f7916Wh);
    }

    /* renamed from: zp */
    private MineThrowerWeapon m32655zp() {
        return (MineThrowerWeapon) bFf().mo5608dq().mo3214p(f7900AG);
    }

    /* renamed from: S */
    public void mo10S() {
        super.mo10S();
    }

    /* renamed from: T */
    public C6494any mo11T() {
        return ___iScriptClass;
    }

    /* renamed from: U */
    public C2491fm[] mo12U() {
        return _m_methods;
    }

    /* renamed from: W */
    public Object mo13W() {
        return new C5766aVy(this);
    }

    /* renamed from: a */
    public Object mo14a(C0495Gr gr) {
        Object[] args = gr.getArgs();
        switch (gr.mo2417hq() - Actor._m_methodCount) {
            case 0:
                m32656zq();
                return null;
            case 1:
                m32624b((Hull) args[0], (C5260aCm) args[1], (Actor) args[2]);
                return null;
            case 2:
                return m32657zs();
            case 3:
                return m32658zu();
            case 4:
                m32612a((StellarSystem) args[0]);
                return null;
            case 5:
                m32628d((Vec3d) args[0]);
                return null;
            case 6:
                m32614a((Vec3d) args[0], (Node) args[1], (StellarSystem) args[2]);
                return null;
            case 7:
                m32659zw();
                return null;
            case 8:
                m32611a((Actor) args[0], (C5260aCm) args[1], (Vec3f) args[2], (Vec3f) args[3]);
                return null;
            case 9:
                m32627c((Actor) args[0], (C5260aCm) args[1], (Vec3f) args[2], (Vec3f) args[3]);
                return null;
            case 10:
                return m32637iz();
            case 11:
                return m32634io();
            case 12:
                return m32635iq();
            case 13:
                return m32636it();
            case 14:
                m32630e((List<Actor>) (List) args[0]);
                return null;
            case 15:
                return m32660zy();
            case 16:
                return m32626c((Space) args[0], ((Long) args[1]).longValue());
            case 17:
                m32618aG();
                return null;
            case 18:
                m32621ay(((Float) args[0]).floatValue());
                return null;
            case 19:
                m32641zA();
                return null;
            case 20:
                m32610a((Actor.C0200h) args[0]);
                return null;
            case 21:
                return new Float(m32643zC());
            case 22:
                m32629e((Actor) args[0], (C5260aCm) args[1], (Vec3f) args[2], (Vec3f) args[3]);
                return null;
            case 23:
                return m32644zE();
            case 24:
                m32638qU();
                return null;
            case 25:
                return new Boolean(m32645zG());
            case 26:
                m32631fg();
                return null;
            case 27:
                return m32633ik();
            case 28:
                m32622b((C5260aCm) args[0]);
                return null;
            case 29:
                return m32646zI();
            case 30:
                m32623b((MineThrowerWeapon) args[0]);
                return null;
            case 31:
                return m32639qW();
            default:
                return super.mo14a(gr);
        }
    }

    @C5566aOg
    /* renamed from: a */
    public void mo8536a(Hull jRVar, C5260aCm acm, Actor cr) {
        switch (bFf().mo6893i(f7919Wk)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f7919Wk, new Object[]{jRVar, acm, cr}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f7919Wk, new Object[]{jRVar, acm, cr}));
                break;
        }
        m32624b(jRVar, acm, cr);
    }

    /* renamed from: a */
    public void mo15a(Collection collection, C0495Gr gr) {
        super.mo15a(collection, gr);
    }

    /* renamed from: aH */
    public void mo70aH() {
        switch (bFf().mo6893i(_f_onResurrect_0020_0028_0029V)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, _f_onResurrect_0020_0028_0029V, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, _f_onResurrect_0020_0028_0029V, new Object[0]));
                break;
        }
        m32618aG();
    }

    /* renamed from: b */
    public void mo620b(Actor.C0200h hVar) {
        switch (bFf().mo6893i(f7946vs)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f7946vs, new Object[]{hVar}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f7946vs, new Object[]{hVar}));
                break;
        }
        m32610a(hVar);
    }

    /* renamed from: b */
    public void mo621b(Actor cr, C5260aCm acm, Vec3f vec3f, Vec3f vec3f2) {
        switch (bFf().mo6893i(f7943vm)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f7943vm, new Object[]{cr, acm, vec3f, vec3f2}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f7943vm, new Object[]{cr, acm, vec3f, vec3f2}));
                break;
        }
        m32611a(cr, acm, vec3f, vec3f2);
    }

    /* renamed from: b */
    public void mo993b(StellarSystem jj) {
        switch (bFf().mo6893i(f7902Mq)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f7902Mq, new Object[]{jj}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f7902Mq, new Object[]{jj}));
                break;
        }
        m32612a(jj);
    }

    /* access modifiers changed from: protected */
    @C4034yP
    @C2499fr(mo18855qf = {"b4248dc0637bb4cbfc146f6209da6d2d"})
    @ClientOnly
    /* renamed from: b */
    public void mo19258b(Vec3d ajr, Node rPVar, StellarSystem jj) {
        switch (bFf().mo6893i(f7923Wo)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f7923Wo, new Object[]{ajr, rPVar, jj}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f7923Wo, new Object[]{ajr, rPVar, jj}));
                break;
        }
        m32614a(ajr, rPVar, jj);
    }

    /* renamed from: c */
    public void mo19259c(C5260aCm acm) {
        switch (bFf().mo6893i(f7932Wx)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f7932Wx, new Object[]{acm}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f7932Wx, new Object[]{acm}));
                break;
        }
        m32622b(acm);
    }

    /* renamed from: c */
    public void mo19260c(MineThrowerWeapon aue) {
        switch (bFf().mo6893i(f7934Wz)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f7934Wz, new Object[]{aue}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f7934Wz, new Object[]{aue}));
                break;
        }
        m32623b(aue);
    }

    /* renamed from: c */
    public C5663aRz[] mo25c() {
        return _m_fields;
    }

    /* access modifiers changed from: protected */
    @C1253SX
    /* renamed from: d */
    public C0520HN mo635d(Space ea, long j) {
        switch (bFf().mo6893i(f7939uY)) {
            case 0:
                return null;
            case 2:
                return (C0520HN) bFf().mo5606d(new aCE(this, f7939uY, new Object[]{ea, new Long(j)}));
            case 3:
                bFf().mo5606d(new aCE(this, f7939uY, new Object[]{ea, new Long(j)}));
                break;
        }
        return m32626c(ea, j);
    }

    /* renamed from: d */
    public void mo636d(Actor cr, C5260aCm acm, Vec3f vec3f, Vec3f vec3f2) {
        switch (bFf().mo6893i(f7944vn)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f7944vn, new Object[]{cr, acm, vec3f, vec3f2}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f7944vn, new Object[]{cr, acm, vec3f, vec3f2}));
                break;
        }
        m32627c(cr, acm, vec3f, vec3f2);
    }

    public void dispose() {
        switch (bFf().mo6893i(_f_dispose_0020_0028_0029V)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, _f_dispose_0020_0028_0029V, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, _f_dispose_0020_0028_0029V, new Object[0]));
                break;
        }
        m32631fg();
    }

    /* access modifiers changed from: protected */
    @C4034yP
    @C5566aOg
    @C2499fr
    /* renamed from: e */
    public void mo19261e(Vec3d ajr) {
        switch (bFf().mo6893i(f7922Wn)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f7922Wn, new Object[]{ajr}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f7922Wn, new Object[]{ajr}));
                break;
        }
        m32628d(ajr);
    }

    @C4034yP
    @C5566aOg
    @C2499fr(mo18857qh = 2)
    /* renamed from: f */
    public void mo1064f(Actor cr, C5260aCm acm, Vec3f vec3f, Vec3f vec3f2) {
        switch (bFf().mo6893i(f7929Wu)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f7929Wu, new Object[]{cr, acm, vec3f, vec3f2}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f7929Wu, new Object[]{cr, acm, vec3f, vec3f2}));
                break;
        }
        m32629e(cr, acm, vec3f, vec3f2);
    }

    /* renamed from: f */
    public void mo8696f(List<Actor> list) {
        switch (bFf().mo6893i(f7925Wq)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f7925Wq, new Object[]{list}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f7925Wq, new Object[]{list}));
                break;
        }
        m32630e(list);
    }

    public /* bridge */ /* synthetic */ Object getType() {
        switch (bFf().mo6893i(_f_getType_0020_0028_0029Ljava_002flang_002fObject_003b)) {
            case 0:
                return null;
            case 2:
                return bFf().mo5606d(new aCE(this, _f_getType_0020_0028_0029Ljava_002flang_002fObject_003b, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, _f_getType_0020_0028_0029Ljava_002flang_002fObject_003b, new Object[0]));
                break;
        }
        return m32639qW();
    }

    /* renamed from: iA */
    public String mo647iA() {
        switch (bFf().mo6893i(f7945vo)) {
            case 0:
                return null;
            case 2:
                return (String) bFf().mo5606d(new aCE(this, f7945vo, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f7945vo, new Object[0]));
                break;
        }
        return m32637iz();
    }

    /* renamed from: il */
    public C5260aCm mo19262il() {
        switch (bFf().mo6893i(f7938uW)) {
            case 0:
                return null;
            case 2:
                return (C5260aCm) bFf().mo5606d(new aCE(this, f7938uW, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f7938uW, new Object[0]));
                break;
        }
        return m32633ik();
    }

    /* renamed from: ip */
    public String mo648ip() {
        switch (bFf().mo6893i(f7940vc)) {
            case 0:
                return null;
            case 2:
                return (String) bFf().mo5606d(new aCE(this, f7940vc, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f7940vc, new Object[0]));
                break;
        }
        return m32634io();
    }

    /* renamed from: ir */
    public String mo649ir() {
        switch (bFf().mo6893i(f7941ve)) {
            case 0:
                return null;
            case 2:
                return (String) bFf().mo5606d(new aCE(this, f7941ve, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f7941ve, new Object[0]));
                break;
        }
        return m32635iq();
    }

    /* renamed from: iu */
    public String mo651iu() {
        switch (bFf().mo6893i(f7942vi)) {
            case 0:
                return null;
            case 2:
                return (String) bFf().mo5606d(new aCE(this, f7942vi, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f7942vi, new Object[0]));
                break;
        }
        return m32636it();
    }

    @C5566aOg
    @C2499fr
    /* renamed from: qV */
    public void mo656qV() {
        switch (bFf().mo6893i(f7901Lm)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f7901Lm, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f7901Lm, new Object[0]));
                break;
        }
        m32638qU();
    }

    public void step(float f) {
        switch (bFf().mo6893i(_f_step_0020_0028F_0029V)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, _f_step_0020_0028F_0029V, new Object[]{new Float(f)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, _f_step_0020_0028F_0029V, new Object[]{new Float(f)}));
                break;
        }
        m32621ay(f);
    }

    /* renamed from: zD */
    public float mo19263zD() {
        switch (bFf().mo6893i(f7928Wt)) {
            case 0:
                return 0.0f;
            case 2:
                return ((Float) bFf().mo5606d(new aCE(this, f7928Wt, new Object[0]))).floatValue();
            case 3:
                bFf().mo5606d(new aCE(this, f7928Wt, new Object[0]));
                break;
        }
        return m32643zC();
    }

    /* renamed from: zF */
    public MineType mo19264zF() {
        switch (bFf().mo6893i(f7930Wv)) {
            case 0:
                return null;
            case 2:
                return (MineType) bFf().mo5606d(new aCE(this, f7930Wv, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f7930Wv, new Object[0]));
                break;
        }
        return m32644zE();
    }

    /* renamed from: zH */
    public boolean mo19265zH() {
        switch (bFf().mo6893i(f7931Ww)) {
            case 0:
                return false;
            case 2:
                return ((Boolean) bFf().mo5606d(new aCE(this, f7931Ww, new Object[0]))).booleanValue();
            case 3:
                bFf().mo5606d(new aCE(this, f7931Ww, new Object[0]));
                break;
        }
        return m32645zG();
    }

    /* renamed from: zJ */
    public MineThrowerWeapon mo19266zJ() {
        switch (bFf().mo6893i(f7933Wy)) {
            case 0:
                return null;
            case 2:
                return (MineThrowerWeapon) bFf().mo5606d(new aCE(this, f7933Wy, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f7933Wy, new Object[0]));
                break;
        }
        return m32646zI();
    }

    /* access modifiers changed from: protected */
    /* renamed from: zr */
    public void mo19267zr() {
        switch (bFf().mo6893i(f7918Wj)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f7918Wj, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f7918Wj, new Object[0]));
                break;
        }
        m32656zq();
    }

    /* renamed from: zt */
    public Hull mo8287zt() {
        switch (bFf().mo6893i(f7920Wl)) {
            case 0:
                return null;
            case 2:
                return (Hull) bFf().mo5606d(new aCE(this, f7920Wl, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f7920Wl, new Object[0]));
                break;
        }
        return m32657zs();
    }

    /* renamed from: zv */
    public Shield mo8288zv() {
        switch (bFf().mo6893i(f7921Wm)) {
            case 0:
                return null;
            case 2:
                return (Shield) bFf().mo5606d(new aCE(this, f7921Wm, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f7921Wm, new Object[0]));
                break;
        }
        return m32658zu();
    }

    /* renamed from: zx */
    public void mo1099zx() {
        switch (bFf().mo6893i(f7924Wp)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f7924Wp, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f7924Wp, new Object[0]));
                break;
        }
        m32659zw();
    }

    /* renamed from: zz */
    public MineAreaTrigger mo19268zz() {
        switch (bFf().mo6893i(f7926Wr)) {
            case 0:
                return null;
            case 2:
                return (MineAreaTrigger) bFf().mo5606d(new aCE(this, f7926Wr, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f7926Wr, new Object[0]));
                break;
        }
        return m32660zy();
    }

    /* renamed from: a */
    public void mo19257a(MineType amx) {
        super.mo967a((C2961mJ) amx);
        MineAreaTrigger okVar = (MineAreaTrigger) bFf().mo6865M(MineAreaTrigger.class);
        okVar.mo21034b(this);
        m32617a(okVar);
        m32647zh().setRadius(m32650zk());
        m32648zi().mo8348d(C2530gR.C2531a.class, this);
        setStatic(false);
        mo958IL().setTransparent(true);
    }

    @C0064Am(aul = "ae6344f8f81a7b6bd7c65ede1d4c2245", aum = 0)
    /* renamed from: zq */
    private void m32656zq() {
        m32609V(true);
    }

    @C0064Am(aul = "39f048c1d63aa92d99e440f80138d397", aum = 0)
    /* renamed from: zs */
    private Hull m32657zs() {
        return m32648zi();
    }

    @C0064Am(aul = "1a42e95648c705af688c5f88b542d205", aum = 0)
    /* renamed from: zu */
    private Shield m32658zu() {
        return null;
    }

    @C0064Am(aul = "b64178637719a9609fec57a581a8db48", aum = 0)
    /* renamed from: a */
    private void m32612a(StellarSystem jj) {
        super.mo993b(jj);
        if (bHa()) {
            m32647zh().mo993b(mo960Nc());
        }
    }

    @C4034yP
    @ClientOnly
    @C0064Am(aul = "169be3caa9df029b96323d8d2504dc6f", aum = 0)
    @C2499fr(mo18855qf = {"b4248dc0637bb4cbfc146f6209da6d2d"})
    /* renamed from: a */
    private void m32614a(Vec3d ajr, Node rPVar, StellarSystem jj) {
        if (mo1000b(rPVar, jj) && mo19264zF() != null) {
            new C3257pi().mo21209a(m32651zl(), ajr, C3257pi.C3261d.MID_RANGE);
            new C3257pi().mo21210a(m32652zm(), ajr, C3257pi.C3261d.MID_RANGE, mo19263zD());
        }
    }

    @C0064Am(aul = "77ef2e413afb7585ed54ad19bd71d4da", aum = 0)
    /* renamed from: zw */
    private void m32659zw() {
        if (bHa() && m32647zh() != null) {
            m32647zh().mo1099zx();
            m32647zh().dispose();
            m32617a((MineAreaTrigger) null);
        }
        super.mo1099zx();
    }

    @C0064Am(aul = "ca2ac59cf92ae987b6df6c88547d0396", aum = 0)
    /* renamed from: a */
    private void m32611a(Actor cr, C5260aCm acm, Vec3f vec3f, Vec3f vec3f2) {
        mo8360mc("takeClientDamage : causer " + cr + " on " + getName());
        mo1073h(cr, acm, vec3f, vec3f2);
    }

    @C0064Am(aul = "37e47562ca054e451d1c0a1adb69f9f1", aum = 0)
    /* renamed from: c */
    private void m32627c(Actor cr, C5260aCm acm, Vec3f vec3f, Vec3f vec3f2) {
    }

    @C0064Am(aul = "c9d2bac8ea804acd8e44520a7f261d63", aum = 0)
    /* renamed from: iz */
    private String m32637iz() {
        return null;
    }

    @C0064Am(aul = "c4fd82a678544b66a21109e6dca33fe4", aum = 0)
    /* renamed from: io */
    private String m32634io() {
        return mo19264zF().mo3521Nu().getHandle();
    }

    @C0064Am(aul = "8a911a438934ab0356c359dc8a00197c", aum = 0)
    /* renamed from: iq */
    private String m32635iq() {
        return mo19264zF().mo3521Nu().getFile();
    }

    @C0064Am(aul = "76abb8e3fd4fb7afc79ebcaa00bc1e07", aum = 0)
    /* renamed from: it */
    private String m32636it() {
        return mo19264zF().mo11042iu();
    }

    @C0064Am(aul = "e0bb6f8425bd267f742b78d4f0583479", aum = 0)
    /* renamed from: zy */
    private MineAreaTrigger m32660zy() {
        return m32647zh();
    }

    @C0064Am(aul = "31dd3b8944fd85a40075ba637de539dd", aum = 0)
    @C1253SX
    /* renamed from: c */
    private C0520HN m32626c(Space ea, long j) {
        Ship al = m32655zp().mo7855al();
        mo988aj(al.getPosition().mo9531q(al.getOrientation().mo15209aT(new Vec3f(0.0f, 0.0f, 1.0f).mo23510mS((float) Math.sqrt((double) al.mo958IL().getBoundingBox().lengthSquared())).mo23503i(new Vec3f(0.0f, 0.0f, 30.0f)))));
        C3312qB qBVar = new C3312qB(ea, this, mo958IL());
        qBVar.mo2449a(ea.cKn().mo5725a(j, (C2235dB) qBVar, 15));
        return qBVar;
    }

    @C0064Am(aul = "8c23d64c8739fc9a108503e572928ac8", aum = 0)
    /* renamed from: aG */
    private void m32618aG() {
        mo19261e(new Vec3d(ScriptRuntime.NaN, ScriptRuntime.NaN, ScriptRuntime.NaN));
        super.mo70aH();
    }

    @C0064Am(aul = "17108ac4747992b384c615b9757d996b", aum = 0)
    /* renamed from: ay */
    private void m32621ay(float f) {
        super.step(f);
        if (bGY() && !isDisposed() && !mo19265zH()) {
            this.f7947Wf += f;
            if (this.f7947Wf >= ((float) m32653zn())) {
                this.f7947Wf = 0.0f;
                m32642zB();
            }
        }
    }

    @C0064Am(aul = "3dcb88b708e11a6acdf6d6ef195fd572", aum = 0)
    @C1253SX
    /* renamed from: zA */
    private void m32641zA() {
        mo19261e(getPosition());
    }

    @C0064Am(aul = "4c0d247aaa6fe43b1bb57981ff6add85", aum = 0)
    /* renamed from: a */
    private void m32610a(Actor.C0200h hVar) {
        hVar.mo1103c(new C6348alI(10.0f));
    }

    @C0064Am(aul = "0493c3d86c2144dac7546526764b0791", aum = 0)
    /* renamed from: zC */
    private float m32643zC() {
        return m32649zj();
    }

    @C0064Am(aul = "18e18d85d3c8fca46b6de9f4236e5c17", aum = 0)
    /* renamed from: zE */
    private MineType m32644zE() {
        return (MineType) super.getType();
    }

    @C0064Am(aul = "841126ae654445ce524d6a9982cfd313", aum = 0)
    /* renamed from: zG */
    private boolean m32645zG() {
        return m32654zo();
    }

    @C0064Am(aul = "fae3b3cf8b132597b5b8d26be0d803bc", aum = 0)
    /* renamed from: fg */
    private void m32631fg() {
        if (isDisposed()) {
            mo6317hy("Mine " + bFY() + " is being disposed again. Please find out why");
            return;
        }
        m32648zi().dispose();
        m32616a((Hull) null);
        super.dispose();
    }

    @C0064Am(aul = "10e403bef2e60ca66b4b144c55011c0b", aum = 0)
    /* renamed from: ik */
    private C5260aCm m32633ik() {
        return m32632id();
    }

    @C0064Am(aul = "dba1e9941f7680570853d0d2cb45ba2b", aum = 0)
    /* renamed from: b */
    private void m32622b(C5260aCm acm) {
        m32613a(acm);
    }

    @C0064Am(aul = "01787720fbba7c3d530ab07b5b595407", aum = 0)
    /* renamed from: zI */
    private MineThrowerWeapon m32646zI() {
        return m32655zp();
    }

    @C0064Am(aul = "72339145cc38d7c660b23551254e40cc", aum = 0)
    /* renamed from: b */
    private void m32623b(MineThrowerWeapon aue) {
        m32615a(aue);
    }
}
