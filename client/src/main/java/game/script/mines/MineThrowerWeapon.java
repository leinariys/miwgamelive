package game.script.mines;

import com.hoplon.geometry.Vec3f;
import game.geometry.Vec3d;
import game.network.message.externalizable.aCE;
import game.script.item.ProjectileWeapon;
import game.script.item.ProjectileWeaponType;
import logic.baa.C1616Xf;
import logic.baa.C1634Xv;
import logic.baa.C5511aMd;
import logic.baa.C6485anp;
import logic.data.mbean.C1120QT;
import logic.res.code.C5663aRz;
import logic.res.code.C6494any;
import logic.res.html.C2491fm;
import p001a.*;

import java.util.Collection;

@C5829abJ("2.1.1")
@C6485anp
@C2712iu(version = "2.1.1")
@C5511aMd
/* renamed from: a.aUE */
/* compiled from: a */
public class MineThrowerWeapon extends ProjectileWeapon implements C1616Xf {
    public static final int _m_fieldCount = 0;
    public static final C5663aRz[] _m_fields = null;
    public static final int _m_methodCount = 0;
    public static final C2491fm[] _m_methods = null;
    public static final C2491fm clv = null;
    public static final C2491fm clw = null;
    public static final C2491fm iXw = null;
    public static final C2491fm iXx = null;
    public static final long serialVersionUID = 0;
    public static C6494any ___iScriptClass;

    static {
        m18510V();
    }

    public MineThrowerWeapon() {
        super((C5540aNg) null);
        super.mo10S();
    }

    public MineThrowerWeapon(ProjectileWeaponType agj) {
        super((C5540aNg) null);
        super.mo5354a(agj);
    }

    public MineThrowerWeapon(C5540aNg ang) {
        super(ang);
    }

    /* renamed from: V */
    static void m18510V() {
        serialVersionUID = (long) 1;
        _m_fieldCount = ProjectileWeapon._m_fieldCount + 0;
        _m_methodCount = ProjectileWeapon._m_methodCount + 4;
        _m_fields = new C5663aRz[(ProjectileWeapon._m_fieldCount + 0)];
        C1634Xv.m11725a((Object[]) ProjectileWeapon._m_fields, (Object[]) _m_fields);
        int i = ProjectileWeapon._m_methodCount;
        C2491fm[] fmVarArr = new C2491fm[(i + 4)];
        C2491fm a = C4105zY.m41624a(MineThrowerWeapon.class, "87ee80c14ab63f26c888e41721e4ef21", i);
        clw = a;
        fmVarArr[i] = a;
        int i2 = i + 1;
        C2491fm a2 = C4105zY.m41624a(MineThrowerWeapon.class, "d7b8d27d8d88fb82b63179fca141b577", i2);
        clv = a2;
        fmVarArr[i2] = a2;
        int i3 = i2 + 1;
        C2491fm a3 = C4105zY.m41624a(MineThrowerWeapon.class, "2fa1a34ea1a871fecd0065ad7aee75ea", i3);
        iXw = a3;
        fmVarArr[i3] = a3;
        int i4 = i3 + 1;
        C2491fm a4 = C4105zY.m41624a(MineThrowerWeapon.class, "18d9549dfad1e2f37703648b4df8e923", i4);
        iXx = a4;
        fmVarArr[i4] = a4;
        int i5 = i4 + 1;
        _m_methods = fmVarArr;
        C1634Xv.m11725a((Object[]) ProjectileWeapon._m_methods, (Object[]) _m_methods);
        ___iScriptClass = aUO.m18566a(MineThrowerWeapon.class, C1120QT.class, _m_fields, _m_methods);
    }

    @C4034yP
    @C5566aOg
    @C0064Am(aul = "18d9549dfad1e2f37703648b4df8e923", aum = 0)
    @C2499fr(mo18857qh = 2)
    /* renamed from: aP */
    private void m18511aP(Vec3d ajr) {
        throw new aWi(new aCE(this, iXx, new Object[]{ajr}));
    }

    @C4034yP
    @C5566aOg
    @C2499fr(mo18857qh = 2)
    /* renamed from: aQ */
    private void m18512aQ(Vec3d ajr) {
        switch (bFf().mo6893i(iXx)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, iXx, new Object[]{ajr}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, iXx, new Object[]{ajr}));
                break;
        }
        m18511aP(ajr);
    }

    @C0064Am(aul = "2fa1a34ea1a871fecd0065ad7aee75ea", aum = 0)
    @C5566aOg
    private Mine dAv() {
        throw new aWi(new aCE(this, iXw, new Object[0]));
    }

    @C5566aOg
    private Mine dAw() {
        switch (bFf().mo6893i(iXw)) {
            case 0:
                return null;
            case 2:
                return (Mine) bFf().mo5606d(new aCE(this, iXw, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, iXw, new Object[0]));
                break;
        }
        return dAv();
    }

    /* renamed from: S */
    public void mo10S() {
        super.mo10S();
    }

    /* renamed from: T */
    public C6494any mo11T() {
        return ___iScriptClass;
    }

    /* renamed from: U */
    public C2491fm[] mo12U() {
        return _m_methods;
    }

    /* renamed from: W */
    public Object mo13W() {
        return new C1120QT(this);
    }

    /* renamed from: a */
    public Object mo14a(C0495Gr gr) {
        Object[] args = gr.getArgs();
        switch (gr.mo2417hq() - ProjectileWeapon._m_methodCount) {
            case 0:
                ayw();
                return null;
            case 1:
                m18513ex(((Float) args[0]).floatValue());
                return null;
            case 2:
                return dAv();
            case 3:
                m18511aP((Vec3d) args[0]);
                return null;
            default:
                return super.mo14a(gr);
        }
    }

    /* renamed from: a */
    public void mo15a(Collection collection, C0495Gr gr) {
        super.mo15a(collection, gr);
    }

    /* access modifiers changed from: protected */
    public void ayx() {
        switch (bFf().mo6893i(clw)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, clw, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, clw, new Object[0]));
                break;
        }
        ayw();
    }

    /* renamed from: c */
    public C5663aRz[] mo25c() {
        return _m_fields;
    }

    /* access modifiers changed from: protected */
    @C4034yP
    @C2499fr
    @C1253SX
    /* renamed from: ey */
    public void mo831ey(float f) {
        switch (bFf().mo6893i(clv)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, clv, new Object[]{new Float(f)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, clv, new Object[]{new Float(f)}));
                break;
        }
        m18513ex(f);
    }

    /* renamed from: a */
    public void mo5354a(ProjectileWeaponType agj) {
        super.mo5354a(agj);
    }

    @C0064Am(aul = "87ee80c14ab63f26c888e41721e4ef21", aum = 0)
    private void ayw() {
        m18512aQ(cUn().getPosition());
        if (bGX()) {
            mo12911b(cUn().agv(), new Vec3f(0.0f, 0.0f, 0.0f));
        }
    }

    @C4034yP
    @C0064Am(aul = "d7b8d27d8d88fb82b63179fca141b577", aum = 0)
    @C2499fr
    @C1253SX
    /* renamed from: ex */
    private void m18513ex(float f) {
        super.mo831ey(f);
        bai();
    }
}
