package game.script.mines;

import com.hoplon.geometry.Vec3f;
import game.network.message.externalizable.aCE;
import game.script.Actor;
import game.script.damage.DamageType;
import logic.baa.C1616Xf;
import logic.baa.C1634Xv;
import logic.baa.C5511aMd;
import logic.baa.C6485anp;
import logic.data.mbean.C6564apQ;
import logic.res.code.C5663aRz;
import logic.res.code.C6494any;
import logic.res.html.C2491fm;
import p001a.*;

import java.util.Collection;
import java.util.List;
import java.util.Map;

@C5829abJ("2.1.1")
@C6485anp
@C2712iu(version = "2.1.1")
@C5511aMd
/* renamed from: a.aFh  reason: case insensitive filesystem */
/* compiled from: a */
public class ExplodingMine extends Mine implements C1616Xf {

    /* renamed from: Wq */
    public static final C2491fm f2839Wq = null;
    public static final int _m_fieldCount = 0;
    public static final C5663aRz[] _m_fields = null;
    public static final int _m_methodCount = 0;
    public static final C2491fm[] _m_methods = null;
    public static final long serialVersionUID = 0;
    public static C6494any ___iScriptClass;

    static {
        m14624V();
    }

    public ExplodingMine() {
        super((C5540aNg) null);
        super.mo10S();
    }

    public ExplodingMine(ExplodingMineType tg) {
        super((C5540aNg) null);
        super._m_script_init(tg);
    }

    public ExplodingMine(C5540aNg ang) {
        super(ang);
    }

    /* renamed from: V */
    static void m14624V() {
        serialVersionUID = (long) 1;
        _m_fieldCount = Mine._m_fieldCount + 0;
        _m_methodCount = Mine._m_methodCount + 1;
        _m_fields = new C5663aRz[(Mine._m_fieldCount + 0)];
        C1634Xv.m11725a((Object[]) Mine._m_fields, (Object[]) _m_fields);
        int i = Mine._m_methodCount;
        C2491fm[] fmVarArr = new C2491fm[(i + 1)];
        C2491fm a = C4105zY.m41624a(ExplodingMine.class, "df2269039d328a00bd77eea1b55f2437", i);
        f2839Wq = a;
        fmVarArr[i] = a;
        int i2 = i + 1;
        _m_methods = fmVarArr;
        C1634Xv.m11725a((Object[]) Mine._m_methods, (Object[]) _m_methods);
        ___iScriptClass = aUO.m18566a(ExplodingMine.class, C6564apQ.class, _m_fields, _m_methods);
    }

    /* renamed from: S */
    public void mo10S() {
        super.mo10S();
    }

    /* renamed from: T */
    public C6494any mo11T() {
        return ___iScriptClass;
    }

    /* renamed from: U */
    public C2491fm[] mo12U() {
        return _m_methods;
    }

    /* renamed from: W */
    public Object mo13W() {
        return new C6564apQ(this);
    }

    /* renamed from: a */
    public Object mo14a(C0495Gr gr) {
        Object[] args = gr.getArgs();
        switch (gr.mo2417hq() - Mine._m_methodCount) {
            case 0:
                m14625e((List) args[0]);
                return null;
            default:
                return super.mo14a(gr);
        }
    }

    /* renamed from: a */
    public void mo15a(Collection collection, C0495Gr gr) {
        super.mo15a(collection, gr);
    }

    /* renamed from: c */
    public C5663aRz[] mo25c() {
        return _m_fields;
    }

    /* renamed from: f */
    public void mo8696f(List<Actor> list) {
        switch (bFf().mo6893i(f2839Wq)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f2839Wq, new Object[]{list}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f2839Wq, new Object[]{list}));
                break;
        }
        m14625e(list);
    }

    /* renamed from: a */
    public void mo8801a(ExplodingMineType tg) {
        super.mo19257a((MineType) tg);
    }

    @C0064Am(aul = "df2269039d328a00bd77eea1b55f2437", aum = 0)
    /* renamed from: e */
    private void m14625e(List<Actor> list) {
        if (bGY() && !mo19265zH()) {
            for (Actor next : list) {
                if (next instanceof aDA) {
                    aDA ada = (aDA) next;
                    if (!ada.equals(cLw()) && !ada.equals(this) && cLw().cMG() != null && !cLw().cMG().contains(ada)) {
                        if (mo19262il() != null) {
                            next.mo995b(next.aiD().mo2472kQ(), (Actor) cLw(), mo19262il(), new Vec3f(0.0f, 0.0f, 0.0f), (Vec3f) null);
                        } else {
                            next.mo995b(next.aiD().mo2472kQ(), (Actor) cLw(), new C5260aCm((Map<DamageType, Float>) mo19264zF().bpA(), mo651iu(), next), new Vec3f(0.0f, 0.0f, 0.0f), (Vec3f) null);
                        }
                        if (!mo19265zH()) {
                            mo19267zr();
                        }
                    }
                }
            }
            if (mo19265zH()) {
                mo19261e(getPosition());
            }
        }
    }
}
