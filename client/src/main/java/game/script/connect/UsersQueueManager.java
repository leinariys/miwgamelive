package game.script.connect;

import game.network.message.externalizable.aCE;
import game.network.message.serializable.C2686iZ;
import game.network.message.serializable.C3438ra;
import game.script.login.UserConnection;
import game.script.player.User;
import logic.baa.C1616Xf;
import logic.baa.C1634Xv;
import logic.baa.C5511aMd;
import logic.baa.C6485anp;
import logic.data.mbean.C6993ayg;
import logic.res.code.C5640aRc;
import logic.res.code.C5663aRz;
import logic.res.code.C6494any;
import logic.res.html.C2491fm;
import p001a.*;

import java.util.ArrayList;
import java.util.Collection;

@C5566aOg
@C5511aMd
@C6485anp
/* renamed from: a.aaJ  reason: case insensitive filesystem */
/* compiled from: a */
public class UsersQueueManager extends TaikodomObject implements C1616Xf {
    public static final int _m_fieldCount = 0;
    public static final C5663aRz[] _m_fields = null;
    public static final int _m_methodCount = 0;
    public static final C2491fm[] _m_methods = null;
    public static final C5663aRz eVX = null;
    public static final C5663aRz eVZ = null;
    public static final C2491fm eWA = null;
    public static final C2491fm eWB = null;
    public static final C2491fm eWC = null;
    public static final C2491fm eWD = null;
    public static final C2491fm eWE = null;
    public static final C2491fm eWF = null;
    public static final C2491fm eWG = null;
    public static final C2491fm eWH = null;
    public static final C2491fm eWI = null;
    public static final C5663aRz eWb = null;
    public static final C5663aRz eWd = null;
    public static final C5663aRz eWf = null;
    public static final C5663aRz eWh = null;
    public static final C2491fm eWi = null;
    public static final C2491fm eWj = null;
    public static final C2491fm eWk = null;
    public static final C2491fm eWl = null;
    public static final C2491fm eWm = null;
    public static final C2491fm eWn = null;
    public static final C2491fm eWo = null;
    public static final C2491fm eWp = null;
    public static final C2491fm eWq = null;
    public static final C2491fm eWr = null;
    public static final C2491fm eWs = null;
    public static final C2491fm eWt = null;
    public static final C2491fm eWu = null;
    public static final C2491fm eWv = null;
    public static final C2491fm eWw = null;
    public static final C2491fm eWx = null;
    public static final C2491fm eWy = null;
    public static final C2491fm eWz = null;
    public static final long serialVersionUID = 0;
    private static final long eVU = 30000;
    private static final long eVV = 60000;
    public static C6494any ___iScriptClass = null;
    @C0064Am(aul = "5cc5e08fbcf64f3ec0ac68856e54b524", aum = 0)
    private static int eVW;
    @C0064Am(aul = "2629e4d4e947082c3b29409b05cc6cf5", aum = 1)
    private static boolean eVY;
    @C0064Am(aul = "94dae7f617049662558c788905ae671d", aum = 2)
    private static long eWa;
    @C0064Am(aul = "691a048981fb66c94f8e6696539ac314", aum = 3)
    @C1020Ot(drz = C1020Ot.C1021a.NOT_PERSISTED)
    private static C3438ra<User> eWc;
    @C0064Am(aul = "e9f397e2a23b1c715cf8c781b514d2d6", aum = 4)
    @C1020Ot(drz = C1020Ot.C1021a.NOT_PERSISTED)
    private static C2686iZ<User> eWe;
    @C0064Am(aul = "0efee0083364cad8535b0c6f7292d899", aum = 5)
    @C1020Ot(drz = C1020Ot.C1021a.NOT_PERSISTED)
    private static C2686iZ<UserConnection> eWg;

    static {
        m19388V();
    }

    public UsersQueueManager() {
        super((C5540aNg) null);
        super.mo10S();
    }

    public UsersQueueManager(C5540aNg ang) {
        super(ang);
    }

    /* renamed from: V */
    static void m19388V() {
        serialVersionUID = (long) 1;
        _m_fieldCount = TaikodomObject._m_fieldCount + 6;
        _m_methodCount = TaikodomObject._m_methodCount + 27;
        int i = TaikodomObject._m_fieldCount;
        C5663aRz[] arzArr = new C5663aRz[(i + 6)];
        C5663aRz b = C5640aRc.m17844b(UsersQueueManager.class, "5cc5e08fbcf64f3ec0ac68856e54b524", i);
        eVX = b;
        arzArr[i] = b;
        int i2 = i + 1;
        C5663aRz b2 = C5640aRc.m17844b(UsersQueueManager.class, "2629e4d4e947082c3b29409b05cc6cf5", i2);
        eVZ = b2;
        arzArr[i2] = b2;
        int i3 = i2 + 1;
        C5663aRz b3 = C5640aRc.m17844b(UsersQueueManager.class, "94dae7f617049662558c788905ae671d", i3);
        eWb = b3;
        arzArr[i3] = b3;
        int i4 = i3 + 1;
        C5663aRz b4 = C5640aRc.m17844b(UsersQueueManager.class, "691a048981fb66c94f8e6696539ac314", i4);
        eWd = b4;
        arzArr[i4] = b4;
        int i5 = i4 + 1;
        C5663aRz b5 = C5640aRc.m17844b(UsersQueueManager.class, "e9f397e2a23b1c715cf8c781b514d2d6", i5);
        eWf = b5;
        arzArr[i5] = b5;
        int i6 = i5 + 1;
        C5663aRz b6 = C5640aRc.m17844b(UsersQueueManager.class, "0efee0083364cad8535b0c6f7292d899", i6);
        eWh = b6;
        arzArr[i6] = b6;
        int i7 = i6 + 1;
        _m_fields = arzArr;
        C1634Xv.m11725a((Object[]) TaikodomObject._m_fields, (Object[]) _m_fields);
        int i8 = TaikodomObject._m_methodCount;
        C2491fm[] fmVarArr = new C2491fm[(i8 + 27)];
        C2491fm a = C4105zY.m41624a(UsersQueueManager.class, "6f3800877196285433baf09b9e6be5f0", i8);
        eWi = a;
        fmVarArr[i8] = a;
        int i9 = i8 + 1;
        C2491fm a2 = C4105zY.m41624a(UsersQueueManager.class, "b92a5a0661f3c2fb35b2604b45524f8f", i9);
        eWj = a2;
        fmVarArr[i9] = a2;
        int i10 = i9 + 1;
        C2491fm a3 = C4105zY.m41624a(UsersQueueManager.class, "fbe9cad9686eff6d10b64e4aaabbb8d0", i10);
        eWk = a3;
        fmVarArr[i10] = a3;
        int i11 = i10 + 1;
        C2491fm a4 = C4105zY.m41624a(UsersQueueManager.class, "43bf085ecc9c55dbf8d8d93ceeb61ae9", i11);
        eWl = a4;
        fmVarArr[i11] = a4;
        int i12 = i11 + 1;
        C2491fm a5 = C4105zY.m41624a(UsersQueueManager.class, "06b858326c33aefb36d5577e456759be", i12);
        eWm = a5;
        fmVarArr[i12] = a5;
        int i13 = i12 + 1;
        C2491fm a6 = C4105zY.m41624a(UsersQueueManager.class, "670b5cc141fe1783eee965962ce77d8d", i13);
        eWn = a6;
        fmVarArr[i13] = a6;
        int i14 = i13 + 1;
        C2491fm a7 = C4105zY.m41624a(UsersQueueManager.class, "d0b04179d5944ae87a9ae4a8a08e6048", i14);
        eWo = a7;
        fmVarArr[i14] = a7;
        int i15 = i14 + 1;
        C2491fm a8 = C4105zY.m41624a(UsersQueueManager.class, "b2e8763f8bc7080d2ca1332290e4a6be", i15);
        eWp = a8;
        fmVarArr[i15] = a8;
        int i16 = i15 + 1;
        C2491fm a9 = C4105zY.m41624a(UsersQueueManager.class, "7fe055014c8025cd3352b9e43fe49be2", i16);
        eWq = a9;
        fmVarArr[i16] = a9;
        int i17 = i16 + 1;
        C2491fm a10 = C4105zY.m41624a(UsersQueueManager.class, "43add910163b10a635c09d0e58d2b366", i17);
        eWr = a10;
        fmVarArr[i17] = a10;
        int i18 = i17 + 1;
        C2491fm a11 = C4105zY.m41624a(UsersQueueManager.class, "2c4cb133e552cba957ab668db0d88af8", i18);
        eWs = a11;
        fmVarArr[i18] = a11;
        int i19 = i18 + 1;
        C2491fm a12 = C4105zY.m41624a(UsersQueueManager.class, "e262abc4e7bba960ec9b082c384ffca0", i19);
        eWt = a12;
        fmVarArr[i19] = a12;
        int i20 = i19 + 1;
        C2491fm a13 = C4105zY.m41624a(UsersQueueManager.class, "1b57eb92ee4e8d2aa0d24db80782a4e3", i20);
        eWu = a13;
        fmVarArr[i20] = a13;
        int i21 = i20 + 1;
        C2491fm a14 = C4105zY.m41624a(UsersQueueManager.class, "acdac95bee7bc9faa764cfb180847ac4", i21);
        eWv = a14;
        fmVarArr[i21] = a14;
        int i22 = i21 + 1;
        C2491fm a15 = C4105zY.m41624a(UsersQueueManager.class, "b157adb64d7f54f51cf0c0beb87bfe57", i22);
        eWw = a15;
        fmVarArr[i22] = a15;
        int i23 = i22 + 1;
        C2491fm a16 = C4105zY.m41624a(UsersQueueManager.class, "718993d45f6b62de2c0d8d1f9a8bd185", i23);
        eWx = a16;
        fmVarArr[i23] = a16;
        int i24 = i23 + 1;
        C2491fm a17 = C4105zY.m41624a(UsersQueueManager.class, "ae06038984eaa96fe9ce37f858a030b4", i24);
        eWy = a17;
        fmVarArr[i24] = a17;
        int i25 = i24 + 1;
        C2491fm a18 = C4105zY.m41624a(UsersQueueManager.class, "08107f7b6077faf2486fe5b7b7ee789d", i25);
        eWz = a18;
        fmVarArr[i25] = a18;
        int i26 = i25 + 1;
        C2491fm a19 = C4105zY.m41624a(UsersQueueManager.class, "c692bb110d4071415907e790f7c5019f", i26);
        eWA = a19;
        fmVarArr[i26] = a19;
        int i27 = i26 + 1;
        C2491fm a20 = C4105zY.m41624a(UsersQueueManager.class, "8720e88cbc748297647c7110c4df5646", i27);
        eWB = a20;
        fmVarArr[i27] = a20;
        int i28 = i27 + 1;
        C2491fm a21 = C4105zY.m41624a(UsersQueueManager.class, "f2c645b108701e7b60218dc3c1830f99", i28);
        eWC = a21;
        fmVarArr[i28] = a21;
        int i29 = i28 + 1;
        C2491fm a22 = C4105zY.m41624a(UsersQueueManager.class, "9d398d05a88bf2131022e930b0dd7f56", i29);
        eWD = a22;
        fmVarArr[i29] = a22;
        int i30 = i29 + 1;
        C2491fm a23 = C4105zY.m41624a(UsersQueueManager.class, "b398f75896391314d991b0b9b8a26bad", i30);
        eWE = a23;
        fmVarArr[i30] = a23;
        int i31 = i30 + 1;
        C2491fm a24 = C4105zY.m41624a(UsersQueueManager.class, "f4576df76c3f2f0cd3b6e77a85d19e76", i31);
        eWF = a24;
        fmVarArr[i31] = a24;
        int i32 = i31 + 1;
        C2491fm a25 = C4105zY.m41624a(UsersQueueManager.class, "956524608dc9b2d894dedc14fdcddd08", i32);
        eWG = a25;
        fmVarArr[i32] = a25;
        int i33 = i32 + 1;
        C2491fm a26 = C4105zY.m41624a(UsersQueueManager.class, "6a10d455236048799e5c7049ba87c7f7", i33);
        eWH = a26;
        fmVarArr[i33] = a26;
        int i34 = i33 + 1;
        C2491fm a27 = C4105zY.m41624a(UsersQueueManager.class, "bd88ee6501c3bfb3c3379feb7ea7d411", i34);
        eWI = a27;
        fmVarArr[i34] = a27;
        int i35 = i34 + 1;
        _m_methods = fmVarArr;
        C1634Xv.m11725a((Object[]) TaikodomObject._m_methods, (Object[]) _m_methods);
        ___iScriptClass = aUO.m18566a(UsersQueueManager.class, C6993ayg.class, _m_fields, _m_methods);
    }

    /* renamed from: N */
    private void m19386N(C2686iZ iZVar) {
        bFf().mo5608dq().mo3197f(eWf, iZVar);
    }

    /* renamed from: O */
    private void m19387O(C2686iZ iZVar) {
        bFf().mo5608dq().mo3197f(eWh, iZVar);
    }

    /* renamed from: bI */
    private void m19389bI(C3438ra raVar) {
        bFf().mo5608dq().mo3197f(eWd, raVar);
    }

    private int bLI() {
        return bFf().mo5608dq().mo3212n(eVX);
    }

    private boolean bLJ() {
        return bFf().mo5608dq().mo3201h(eVZ);
    }

    private long bLK() {
        return bFf().mo5608dq().mo3213o(eWb);
    }

    private C3438ra bLL() {
        return (C3438ra) bFf().mo5608dq().mo3214p(eWd);
    }

    private C2686iZ bLM() {
        return (C2686iZ) bFf().mo5608dq().mo3214p(eWf);
    }

    private C2686iZ bLN() {
        return (C2686iZ) bFf().mo5608dq().mo3214p(eWh);
    }

    private void bLP() {
        switch (bFf().mo6893i(eWm)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, eWm, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, eWm, new Object[0]));
                break;
        }
        bLO();
    }

    private void bLR() {
        switch (bFf().mo6893i(eWn)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, eWn, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, eWn, new Object[0]));
                break;
        }
        bLQ();
    }

    private void bLZ() {
        switch (bFf().mo6893i(eWr)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, eWr, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, eWr, new Object[0]));
                break;
        }
        bLY();
    }

    private void bMb() {
        switch (bFf().mo6893i(eWs)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, eWs, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, eWs, new Object[0]));
                break;
        }
        bMa();
    }

    private void bMd() {
        switch (bFf().mo6893i(eWt)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, eWt, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, eWt, new Object[0]));
                break;
        }
        bMc();
    }

    /* renamed from: dQ */
    private void m19391dQ(boolean z) {
        bFf().mo5608dq().mo3153a(eVZ, z);
    }

    /* renamed from: go */
    private void m19394go(long j) {
        bFf().mo5608dq().mo3184b(eWb, j);
    }

    /* renamed from: pt */
    private void m19399pt(int i) {
        bFf().mo5608dq().mo3183b(eVX, i);
    }

    /* renamed from: px */
    private void m19402px(int i) {
        switch (bFf().mo6893i(eWD)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, eWD, new Object[]{new Integer(i)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, eWD, new Object[]{new Integer(i)}));
                break;
        }
        m19401pw(i);
    }

    /* renamed from: pz */
    private void m19404pz(int i) {
        switch (bFf().mo6893i(eWE)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, eWE, new Object[]{new Integer(i)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, eWE, new Object[]{new Integer(i)}));
                break;
        }
        m19403py(i);
    }

    /* renamed from: T */
    public C6494any mo11T() {
        return ___iScriptClass;
    }

    /* renamed from: U */
    public C2491fm[] mo12U() {
        return _m_methods;
    }

    /* renamed from: W */
    public Object mo13W() {
        return new C6993ayg(this);
    }

    /* renamed from: a */
    public Object mo14a(C0495Gr gr) {
        Object[] args = gr.getArgs();
        switch (gr.mo2417hq() - TaikodomObject._m_methodCount) {
            case 0:
                return new Integer(m19395j((User) args[0]));
            case 1:
                return new Integer(m19396l((User) args[0]));
            case 2:
                return new Integer(m19397n((User) args[0]));
            case 3:
                m19390d((UserConnection) args[0]);
                return null;
            case 4:
                bLO();
                return null;
            case 5:
                bLQ();
                return null;
            case 6:
                return new Integer(bLS());
            case 7:
                return new Integer(bLU());
            case 8:
                bLW();
                return null;
            case 9:
                bLY();
                return null;
            case 10:
                bMa();
                return null;
            case 11:
                bMc();
                return null;
            case 12:
                return new Integer(bMe());
            case 13:
                return new Boolean(bMg());
            case 14:
                return new Boolean(m19398p((User) args[0]));
            case 15:
                return bMi();
            case 16:
                m19393f((UserConnection) args[0]);
                return null;
            case 17:
                return new Integer(m19405r((User) args[0]));
            case 18:
                return new Boolean(bMk());
            case 19:
                return new Integer(bMm());
            case 20:
                m19400pu(((Integer) args[0]).intValue());
                return null;
            case 21:
                m19401pw(((Integer) args[0]).intValue());
                return null;
            case 22:
                m19403py(((Integer) args[0]).intValue());
                return null;
            case 23:
                return new Integer(bMn());
            case 24:
                return new Boolean(bMp());
            case 25:
                m19392dR(((Boolean) args[0]).booleanValue());
                return null;
            case 26:
                return new Long(bMr());
            default:
                return super.mo14a(gr);
        }
    }

    /* renamed from: a */
    public void mo15a(Collection collection, C0495Gr gr) {
        super.mo15a(collection, gr);
    }

    public int bLT() {
        switch (bFf().mo6893i(eWo)) {
            case 0:
                return 0;
            case 2:
                return ((Integer) bFf().mo5606d(new aCE(this, eWo, new Object[0]))).intValue();
            case 3:
                bFf().mo5606d(new aCE(this, eWo, new Object[0]));
                break;
        }
        return bLS();
    }

    public int bLV() {
        switch (bFf().mo6893i(eWp)) {
            case 0:
                return 0;
            case 2:
                return ((Integer) bFf().mo5606d(new aCE(this, eWp, new Object[0]))).intValue();
            case 3:
                bFf().mo5606d(new aCE(this, eWp, new Object[0]));
                break;
        }
        return bLU();
    }

    public void bLX() {
        switch (bFf().mo6893i(eWq)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, eWq, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, eWq, new Object[0]));
                break;
        }
        bLW();
    }

    public int bMf() {
        switch (bFf().mo6893i(eWu)) {
            case 0:
                return 0;
            case 2:
                return ((Integer) bFf().mo5606d(new aCE(this, eWu, new Object[0]))).intValue();
            case 3:
                bFf().mo5606d(new aCE(this, eWu, new Object[0]));
                break;
        }
        return bMe();
    }

    public boolean bMh() {
        switch (bFf().mo6893i(eWv)) {
            case 0:
                return false;
            case 2:
                return ((Boolean) bFf().mo5606d(new aCE(this, eWv, new Object[0]))).booleanValue();
            case 3:
                bFf().mo5606d(new aCE(this, eWv, new Object[0]));
                break;
        }
        return bMg();
    }

    public C2686iZ<UserConnection> bMj() {
        switch (bFf().mo6893i(eWx)) {
            case 0:
                return null;
            case 2:
                return (C2686iZ) bFf().mo5606d(new aCE(this, eWx, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, eWx, new Object[0]));
                break;
        }
        return bMi();
    }

    public boolean bMl() {
        switch (bFf().mo6893i(eWA)) {
            case 0:
                return false;
            case 2:
                return ((Boolean) bFf().mo5606d(new aCE(this, eWA, new Object[0]))).booleanValue();
            case 3:
                bFf().mo5606d(new aCE(this, eWA, new Object[0]));
                break;
        }
        return bMk();
    }

    public int bMo() {
        switch (bFf().mo6893i(eWF)) {
            case 0:
                return 0;
            case 2:
                return ((Integer) bFf().mo5606d(new aCE(this, eWF, new Object[0]))).intValue();
            case 3:
                bFf().mo5606d(new aCE(this, eWF, new Object[0]));
                break;
        }
        return bMn();
    }

    public boolean bMq() {
        switch (bFf().mo6893i(eWG)) {
            case 0:
                return false;
            case 2:
                return ((Boolean) bFf().mo5606d(new aCE(this, eWG, new Object[0]))).booleanValue();
            case 3:
                bFf().mo5606d(new aCE(this, eWG, new Object[0]));
                break;
        }
        return bMp();
    }

    public long bMs() {
        switch (bFf().mo6893i(eWI)) {
            case 0:
                return 0;
            case 2:
                return ((Long) bFf().mo5606d(new aCE(this, eWI, new Object[0]))).longValue();
            case 3:
                bFf().mo5606d(new aCE(this, eWI, new Object[0]));
                break;
        }
        return bMr();
    }

    /* renamed from: c */
    public C5663aRz[] mo25c() {
        return _m_fields;
    }

    /* renamed from: dS */
    public void mo12159dS(boolean z) {
        switch (bFf().mo6893i(eWH)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, eWH, new Object[]{new Boolean(z)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, eWH, new Object[]{new Boolean(z)}));
                break;
        }
        m19392dR(z);
    }

    /* renamed from: e */
    public void mo12160e(UserConnection apk) {
        switch (bFf().mo6893i(eWl)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, eWl, new Object[]{apk}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, eWl, new Object[]{apk}));
                break;
        }
        m19390d(apk);
    }

    /* renamed from: g */
    public void mo12161g(UserConnection apk) {
        switch (bFf().mo6893i(eWy)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, eWy, new Object[]{apk}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, eWy, new Object[]{apk}));
                break;
        }
        m19393f(apk);
    }

    public int getMaxConnections() {
        switch (bFf().mo6893i(eWB)) {
            case 0:
                return 0;
            case 2:
                return ((Integer) bFf().mo5606d(new aCE(this, eWB, new Object[0]))).intValue();
            case 3:
                bFf().mo5606d(new aCE(this, eWB, new Object[0]));
                break;
        }
        return bMm();
    }

    /* renamed from: k */
    public int mo12163k(User adk) {
        switch (bFf().mo6893i(eWi)) {
            case 0:
                return 0;
            case 2:
                return ((Integer) bFf().mo5606d(new aCE(this, eWi, new Object[]{adk}))).intValue();
            case 3:
                bFf().mo5606d(new aCE(this, eWi, new Object[]{adk}));
                break;
        }
        return m19395j(adk);
    }

    /* renamed from: m */
    public int mo12164m(User adk) {
        switch (bFf().mo6893i(eWj)) {
            case 0:
                return 0;
            case 2:
                return ((Integer) bFf().mo5606d(new aCE(this, eWj, new Object[]{adk}))).intValue();
            case 3:
                bFf().mo5606d(new aCE(this, eWj, new Object[]{adk}));
                break;
        }
        return m19396l(adk);
    }

    /* renamed from: o */
    public int mo12165o(User adk) {
        switch (bFf().mo6893i(eWk)) {
            case 0:
                return 0;
            case 2:
                return ((Integer) bFf().mo5606d(new aCE(this, eWk, new Object[]{adk}))).intValue();
            case 3:
                bFf().mo5606d(new aCE(this, eWk, new Object[]{adk}));
                break;
        }
        return m19397n(adk);
    }

    /* renamed from: pv */
    public void mo12166pv(int i) {
        switch (bFf().mo6893i(eWC)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, eWC, new Object[]{new Integer(i)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, eWC, new Object[]{new Integer(i)}));
                break;
        }
        m19400pu(i);
    }

    /* renamed from: q */
    public boolean mo12167q(User adk) {
        switch (bFf().mo6893i(eWw)) {
            case 0:
                return false;
            case 2:
                return ((Boolean) bFf().mo5606d(new aCE(this, eWw, new Object[]{adk}))).booleanValue();
            case 3:
                bFf().mo5606d(new aCE(this, eWw, new Object[]{adk}));
                break;
        }
        return m19398p(adk);
    }

    /* renamed from: s */
    public int mo12168s(User adk) {
        switch (bFf().mo6893i(eWz)) {
            case 0:
                return 0;
            case 2:
                return ((Integer) bFf().mo5606d(new aCE(this, eWz, new Object[]{adk}))).intValue();
            case 3:
                bFf().mo5606d(new aCE(this, eWz, new Object[]{adk}));
                break;
        }
        return m19405r(adk);
    }

    /* renamed from: S */
    public void mo10S() {
        super.mo10S();
        m19399pt(10000);
        m19391dQ(false);
    }

    @C0064Am(aul = "6f3800877196285433baf09b9e6be5f0", aum = 0)
    /* renamed from: j */
    private int m19395j(User adk) {
        if (!bLJ()) {
            return -1;
        }
        bLX();
        return mo12168s(adk);
    }

    @C0064Am(aul = "b92a5a0661f3c2fb35b2604b45524f8f", aum = 0)
    /* renamed from: l */
    private int m19396l(User adk) {
        if (!bLL().contains(adk)) {
            bLL().add(adk);
            mo8359ma("User '" + adk.getUsername() + " added in waiting queue.");
        }
        adk.mo8476ks(cVr());
        return mo12165o(adk);
    }

    @C0064Am(aul = "fbe9cad9686eff6d10b64e4aaabbb8d0", aum = 0)
    /* renamed from: n */
    private int m19397n(User adk) {
        return bLL().indexOf(adk);
    }

    @C0064Am(aul = "43bf085ecc9c55dbf8d8d93ceeb61ae9", aum = 0)
    /* renamed from: d */
    private void m19390d(UserConnection apk) {
        try {
            bLP();
            if (apk != null && bLN().contains(apk)) {
                bLN().remove(apk);
            }
        } catch (Exception e) {
            mo8358lY("Exception while trying to add next user from waiting queue into allowed queue.");
            if (apk != null && bLN().contains(apk)) {
                bLN().remove(apk);
            }
        } catch (Throwable th) {
            if (apk != null && bLN().contains(apk)) {
                bLN().remove(apk);
            }
            throw th;
        }
    }

    @C0064Am(aul = "06b858326c33aefb36d5577e456759be", aum = 0)
    private void bLO() {
        if (bLV() > 0) {
            User adk = (User) bLL().remove(0);
            bLM().add(adk);
            mo8359ma("Updating queue. User '" + adk.getUsername() + " is now allowed to connect to server.");
        }
    }

    @C0064Am(aul = "670b5cc141fe1783eee965962ce77d8d", aum = 0)
    private void bLQ() {
        if (bLV() > 0) {
            for (int i = 0; i < bLT(); i++) {
                bLP();
            }
        }
    }

    @C0064Am(aul = "d0b04179d5944ae87a9ae4a8a08e6048", aum = 0)
    private int bLS() {
        return getMaxConnections() - (bMo() + bMf());
    }

    @C0064Am(aul = "b2e8763f8bc7080d2ca1332290e4a6be", aum = 0)
    private int bLU() {
        return bLL().size();
    }

    @C0064Am(aul = "7fe055014c8025cd3352b9e43fe49be2", aum = 0)
    private void bLW() {
        long cVr = cVr();
        if (cVr - bLK() >= eVU) {
            m19394go(cVr);
            bLZ();
            bMd();
            bMb();
        }
    }

    @C0064Am(aul = "43add910163b10a635c09d0e58d2b366", aum = 0)
    private void bLY() {
        ArrayList<UserConnection> arrayList = new ArrayList<>();
        for (UserConnection apk : bLN()) {
            if (apk.bFs() == null || apk.bFs().cWq() == null) {
                arrayList.add(apk);
            }
        }
        for (UserConnection remove : arrayList) {
            bLN().remove(remove);
        }
        bLR();
    }

    @C0064Am(aul = "2c4cb133e552cba957ab668db0d88af8", aum = 0)
    private void bMa() {
        mo8359ma("Allowed queue size: " + bMf());
        ArrayList<User> arrayList = new ArrayList<>();
        for (User adk : bLM()) {
            if (bLK() - adk.cTA() > eVV) {
                arrayList.add(adk);
            }
        }
        for (User adk2 : arrayList) {
            bLM().remove(adk2);
            mo8359ma("User '" + adk2.getUsername() + " has been removed from allowed queue due to timeout connection.");
            bLP();
        }
    }

    @C0064Am(aul = "e262abc4e7bba960ec9b082c384ffca0", aum = 0)
    private void bMc() {
        ArrayList<User> arrayList = new ArrayList<>();
        mo8359ma("Waiting queue size: " + bLV());
        for (User adk : bLL()) {
            if (bLK() - adk.cTA() > eVV) {
                arrayList.add(adk);
            }
        }
        for (User adk2 : arrayList) {
            bLL().remove(adk2);
            mo8359ma("User '" + adk2.getUsername() + " has been removed from waiting queue due to timeout connection.");
        }
    }

    @C0064Am(aul = "1b57eb92ee4e8d2aa0d24db80782a4e3", aum = 0)
    private int bMe() {
        return bLM().size();
    }

    @C0064Am(aul = "acdac95bee7bc9faa764cfb180847ac4", aum = 0)
    private boolean bMg() {
        return bMf() == 0;
    }

    @C0064Am(aul = "b157adb64d7f54f51cf0c0beb87bfe57", aum = 0)
    /* renamed from: p */
    private boolean m19398p(User adk) {
        return bLM().contains(adk);
    }

    @C0064Am(aul = "718993d45f6b62de2c0d8d1f9a8bd185", aum = 0)
    private C2686iZ<UserConnection> bMi() {
        return bLN();
    }

    @C0064Am(aul = "ae06038984eaa96fe9ce37f858a030b4", aum = 0)
    /* renamed from: f */
    private void m19393f(UserConnection apk) {
        if (apk != null) {
            bLN().add(apk);
            User bhe = apk.getUser();
            if (bhe != null && bLM().contains(apk)) {
                bLM().remove(bhe);
            }
        }
    }

    @C0064Am(aul = "08107f7b6077faf2486fe5b7b7ee789d", aum = 0)
    /* renamed from: r */
    private int m19405r(User adk) {
        if ((bMl() || !bMh()) && !mo12167q(adk)) {
            return mo12164m(adk);
        }
        return -1;
    }

    @C0064Am(aul = "c692bb110d4071415907e790f7c5019f", aum = 0)
    private boolean bMk() {
        boolean z = bLN().size() >= bLI();
        if (z) {
            mo6317hy("Server is full! Connections: " + bLN().size());
        }
        return z;
    }

    @C0064Am(aul = "8720e88cbc748297647c7110c4df5646", aum = 0)
    private int bMm() {
        return bLI();
    }

    @C0064Am(aul = "f2c645b108701e7b60218dc3c1830f99", aum = 0)
    /* renamed from: pu */
    private void m19400pu(int i) {
        if (i > bLI()) {
            m19404pz(i);
        } else if (i < bLI()) {
            m19402px(i);
        }
        m19399pt(i);
    }

    @C0064Am(aul = "9d398d05a88bf2131022e930b0dd7f56", aum = 0)
    /* renamed from: pw */
    private void m19401pw(int i) {
        mo8359ma("Server max connections decreased from " + bLI() + " to " + i);
        if (bMf() > 0) {
            int i2 = 0;
            for (int i3 = 0; i3 < bLI() - i; i3++) {
                if (bLM().remove(0)) {
                    i2++;
                }
            }
            if (i2 > 0) {
                mo8359ma(String.valueOf(i2) + " users removed from allowed queue due to servers connections number decreased.");
            }
        }
    }

    @C0064Am(aul = "b398f75896391314d991b0b9b8a26bad", aum = 0)
    /* renamed from: py */
    private void m19403py(int i) {
        mo8359ma("Server max connections increased from " + bLI() + " to " + i);
        bLR();
    }

    @C0064Am(aul = "f4576df76c3f2f0cd3b6e77a85d19e76", aum = 0)
    private int bMn() {
        return bLN().size();
    }

    @C0064Am(aul = "956524608dc9b2d894dedc14fdcddd08", aum = 0)
    private boolean bMp() {
        return bLJ();
    }

    @C0064Am(aul = "6a10d455236048799e5c7049ba87c7f7", aum = 0)
    /* renamed from: dR */
    private void m19392dR(boolean z) {
        mo8359ma("User queuer system is now " + (z ? "activated" : "deactivated"));
        m19391dQ(z);
        if (!bLJ()) {
            bLM().clear();
            bLL().clear();
        }
    }

    @C0064Am(aul = "bd88ee6501c3bfb3c3379feb7ea7d411", aum = 0)
    private long bMr() {
        return bLK();
    }
}
