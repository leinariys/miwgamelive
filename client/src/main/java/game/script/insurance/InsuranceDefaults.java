package game.script.insurance;

import game.network.message.externalizable.aCE;
import logic.baa.*;
import logic.data.mbean.C5270aCw;
import logic.res.code.C5640aRc;
import logic.res.code.C5663aRz;
import logic.res.code.C6494any;
import logic.res.html.C2491fm;
import p001a.*;
import taikodom.infra.script.I18NString;

import java.util.Collection;
import java.util.UUID;

@C5511aMd
@C6485anp
/* renamed from: a.AH */
/* compiled from: a */
public class InsuranceDefaults extends TaikodomObject implements C0468GU, C1616Xf {
    public static final int _m_fieldCount = 0;
    public static final C5663aRz[] _m_fields = null;
    public static final int _m_methodCount = 0;
    public static final C2491fm[] _m_methods = null;
    /* renamed from: bL */
    public static final C5663aRz f16bL = null;
    /* renamed from: bN */
    public static final C2491fm f17bN = null;
    /* renamed from: bO */
    public static final C2491fm f18bO = null;
    /* renamed from: bP */
    public static final C2491fm f19bP = null;
    public static final C5663aRz ciB = null;
    public static final C5663aRz ciD = null;
    public static final C5663aRz ciF = null;
    public static final C5663aRz ciH = null;
    public static final C2491fm ciI = null;
    public static final C2491fm ciJ = null;
    public static final C2491fm ciK = null;
    public static final C2491fm ciL = null;
    public static final C2491fm ciM = null;
    public static final C2491fm ciN = null;
    public static final C2491fm ciO = null;
    public static final C2491fm ciP = null;
    public static final long serialVersionUID = 0;
    public static C6494any ___iScriptClass;
    @C0064Am(aul = "fbba372fc7edca3a45ead54f5c409481", aum = 4)

    /* renamed from: bK */
    private static UUID f15bK;
    @C0064Am(aul = "7b3aed4459fe2156d5572e469e4e0eb5", aum = 0)
    private static long ciA;
    @C0064Am(aul = "ce153f84d7c363135d0a13d64be749dd", aum = 1)
    private static long ciC;
    @C0064Am(aul = "c9799ef61ece871e75ec2f47841ba03a", aum = 2)
    private static I18NString ciE;
    @C0064Am(aul = "749fec917040e3f6686ef2047d0973a7", aum = 3)
    private static I18NString ciG;

    static {
        m198V();
    }

    public InsuranceDefaults() {
        super((C5540aNg) null);
        super.mo10S();
    }

    public InsuranceDefaults(C5540aNg ang) {
        super(ang);
    }

    /* renamed from: V */
    static void m198V() {
        serialVersionUID = (long) 1;
        _m_fieldCount = TaikodomObject._m_fieldCount + 5;
        _m_methodCount = TaikodomObject._m_methodCount + 11;
        int i = TaikodomObject._m_fieldCount;
        C5663aRz[] arzArr = new C5663aRz[(i + 5)];
        C5663aRz b = C5640aRc.m17844b(InsuranceDefaults.class, "7b3aed4459fe2156d5572e469e4e0eb5", i);
        ciB = b;
        arzArr[i] = b;
        int i2 = i + 1;
        C5663aRz b2 = C5640aRc.m17844b(InsuranceDefaults.class, "ce153f84d7c363135d0a13d64be749dd", i2);
        ciD = b2;
        arzArr[i2] = b2;
        int i3 = i2 + 1;
        C5663aRz b3 = C5640aRc.m17844b(InsuranceDefaults.class, "c9799ef61ece871e75ec2f47841ba03a", i3);
        ciF = b3;
        arzArr[i3] = b3;
        int i4 = i3 + 1;
        C5663aRz b4 = C5640aRc.m17844b(InsuranceDefaults.class, "749fec917040e3f6686ef2047d0973a7", i4);
        ciH = b4;
        arzArr[i4] = b4;
        int i5 = i4 + 1;
        C5663aRz b5 = C5640aRc.m17844b(InsuranceDefaults.class, "fbba372fc7edca3a45ead54f5c409481", i5);
        f16bL = b5;
        arzArr[i5] = b5;
        int i6 = i5 + 1;
        _m_fields = arzArr;
        C1634Xv.m11725a((Object[]) TaikodomObject._m_fields, (Object[]) _m_fields);
        int i7 = TaikodomObject._m_methodCount;
        C2491fm[] fmVarArr = new C2491fm[(i7 + 11)];
        C2491fm a = C4105zY.m41624a(InsuranceDefaults.class, "24c0e001000bbf68bf2d4b1d7473af15", i7);
        f17bN = a;
        fmVarArr[i7] = a;
        int i8 = i7 + 1;
        C2491fm a2 = C4105zY.m41624a(InsuranceDefaults.class, "d4ae0e6a7ac00cfe587212b787e9489e", i8);
        f18bO = a2;
        fmVarArr[i8] = a2;
        int i9 = i8 + 1;
        C2491fm a3 = C4105zY.m41624a(InsuranceDefaults.class, "e6204cf6cb0914e5178f3056b2e90cb3", i9);
        f19bP = a3;
        fmVarArr[i9] = a3;
        int i10 = i9 + 1;
        C2491fm a4 = C4105zY.m41624a(InsuranceDefaults.class, "fc893c5756924de3ada83f76b0ff22cd", i10);
        ciI = a4;
        fmVarArr[i10] = a4;
        int i11 = i10 + 1;
        C2491fm a5 = C4105zY.m41624a(InsuranceDefaults.class, "b2811640be7f09675692314350f47e83", i11);
        ciJ = a5;
        fmVarArr[i11] = a5;
        int i12 = i11 + 1;
        C2491fm a6 = C4105zY.m41624a(InsuranceDefaults.class, "d69d41072421bda8d7ebe758c676ebf3", i12);
        ciK = a6;
        fmVarArr[i12] = a6;
        int i13 = i12 + 1;
        C2491fm a7 = C4105zY.m41624a(InsuranceDefaults.class, "dade67c819322d792f8b7e96dc1b7788", i13);
        ciL = a7;
        fmVarArr[i13] = a7;
        int i14 = i13 + 1;
        C2491fm a8 = C4105zY.m41624a(InsuranceDefaults.class, "97557b619ac9d1488cbdf55daf76963d", i14);
        ciM = a8;
        fmVarArr[i14] = a8;
        int i15 = i14 + 1;
        C2491fm a9 = C4105zY.m41624a(InsuranceDefaults.class, "9a20695c7f76271d5cc4da4ccd3bf7ab", i15);
        ciN = a9;
        fmVarArr[i15] = a9;
        int i16 = i15 + 1;
        C2491fm a10 = C4105zY.m41624a(InsuranceDefaults.class, "9072ba79b8a61ecbec0b9b09353e05c2", i16);
        ciO = a10;
        fmVarArr[i16] = a10;
        int i17 = i16 + 1;
        C2491fm a11 = C4105zY.m41624a(InsuranceDefaults.class, "1b20683ad59296136352467597b2b896", i17);
        ciP = a11;
        fmVarArr[i17] = a11;
        int i18 = i17 + 1;
        _m_methods = fmVarArr;
        C1634Xv.m11725a((Object[]) TaikodomObject._m_methods, (Object[]) _m_methods);
        ___iScriptClass = aUO.m18566a(InsuranceDefaults.class, C5270aCw.class, _m_fields, _m_methods);
    }

    /* renamed from: a */
    private void m199a(UUID uuid) {
        bFf().mo5608dq().mo3197f(f16bL, uuid);
    }

    /* renamed from: an */
    private UUID m200an() {
        return (UUID) bFf().mo5608dq().mo3214p(f16bL);
    }

    private long awo() {
        return bFf().mo5608dq().mo3213o(ciB);
    }

    private long awp() {
        return bFf().mo5608dq().mo3213o(ciD);
    }

    private I18NString awq() {
        return (I18NString) bFf().mo5608dq().mo3214p(ciF);
    }

    private I18NString awr() {
        return (I18NString) bFf().mo5608dq().mo3214p(ciH);
    }

    /* renamed from: c */
    private void m204c(UUID uuid) {
        switch (bFf().mo6893i(f18bO)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f18bO, new Object[]{uuid}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f18bO, new Object[]{uuid}));
                break;
        }
        m203b(uuid);
    }

    /* renamed from: dF */
    private void m205dF(long j) {
        bFf().mo5608dq().mo3184b(ciB, j);
    }

    /* renamed from: dG */
    private void m206dG(long j) {
        bFf().mo5608dq().mo3184b(ciD, j);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Price Ceiling")
    @C0064Am(aul = "b2811640be7f09675692314350f47e83", aum = 0)
    @C5566aOg
    /* renamed from: dH */
    private void m207dH(long j) {
        throw new aWi(new aCE(this, ciJ, new Object[]{new Long(j)}));
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Percentage Deductible")
    @C0064Am(aul = "dade67c819322d792f8b7e96dc1b7788", aum = 0)
    @C5566aOg
    /* renamed from: dJ */
    private void m208dJ(long j) {
        throw new aWi(new aCE(this, ciL, new Object[]{new Long(j)}));
    }

    /* renamed from: eG */
    private void m209eG(I18NString i18NString) {
        bFf().mo5608dq().mo3197f(ciF, i18NString);
    }

    /* renamed from: eH */
    private void m210eH(I18NString i18NString) {
        bFf().mo5608dq().mo3197f(ciH, i18NString);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Default Description")
    @C0064Am(aul = "9a20695c7f76271d5cc4da4ccd3bf7ab", aum = 0)
    @C5566aOg
    /* renamed from: eI */
    private void m211eI(I18NString i18NString) {
        throw new aWi(new aCE(this, ciN, new Object[]{i18NString}));
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Default Insurance Description")
    @C0064Am(aul = "1b20683ad59296136352467597b2b896", aum = 0)
    @C5566aOg
    /* renamed from: eK */
    private void m212eK(I18NString i18NString) {
        throw new aWi(new aCE(this, ciP, new Object[]{i18NString}));
    }

    /* renamed from: T */
    public C6494any mo11T() {
        return ___iScriptClass;
    }

    /* renamed from: U */
    public C2491fm[] mo12U() {
        return _m_methods;
    }

    /* renamed from: W */
    public Object mo13W() {
        return new C5270aCw(this);
    }

    /* renamed from: a */
    public Object mo14a(C0495Gr gr) {
        Object[] args = gr.getArgs();
        switch (gr.mo2417hq() - TaikodomObject._m_methodCount) {
            case 0:
                return m201ap();
            case 1:
                m203b((UUID) args[0]);
                return null;
            case 2:
                return m202ar();
            case 3:
                return new Long(aws());
            case 4:
                m207dH(((Long) args[0]).longValue());
                return null;
            case 5:
                return new Long(awu());
            case 6:
                m208dJ(((Long) args[0]).longValue());
                return null;
            case 7:
                return aww();
            case 8:
                m211eI((I18NString) args[0]);
                return null;
            case 9:
                return awy();
            case 10:
                m212eK((I18NString) args[0]);
                return null;
            default:
                return super.mo14a(gr);
        }
    }

    /* renamed from: a */
    public void mo15a(Collection collection, C0495Gr gr) {
        super.mo15a(collection, gr);
    }

    /* renamed from: aq */
    public UUID mo18aq() {
        switch (bFf().mo6893i(f17bN)) {
            case 0:
                return null;
            case 2:
                return (UUID) bFf().mo5606d(new aCE(this, f17bN, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f17bN, new Object[0]));
                break;
        }
        return m201ap();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Price Ceiling")
    public long awt() {
        switch (bFf().mo6893i(ciI)) {
            case 0:
                return 0;
            case 2:
                return ((Long) bFf().mo5606d(new aCE(this, ciI, new Object[0]))).longValue();
            case 3:
                bFf().mo5606d(new aCE(this, ciI, new Object[0]));
                break;
        }
        return aws();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Percentage Deductible")
    public long awv() {
        switch (bFf().mo6893i(ciK)) {
            case 0:
                return 0;
            case 2:
                return ((Long) bFf().mo5606d(new aCE(this, ciK, new Object[0]))).longValue();
            case 3:
                bFf().mo5606d(new aCE(this, ciK, new Object[0]));
                break;
        }
        return awu();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Default Description")
    public I18NString awx() {
        switch (bFf().mo6893i(ciM)) {
            case 0:
                return null;
            case 2:
                return (I18NString) bFf().mo5606d(new aCE(this, ciM, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, ciM, new Object[0]));
                break;
        }
        return aww();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Default Insurance Description")
    public I18NString awz() {
        switch (bFf().mo6893i(ciO)) {
            case 0:
                return null;
            case 2:
                return (I18NString) bFf().mo5606d(new aCE(this, ciO, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, ciO, new Object[0]));
                break;
        }
        return awy();
    }

    /* renamed from: c */
    public C5663aRz[] mo25c() {
        return _m_fields;
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Price Ceiling")
    @C5566aOg
    /* renamed from: dI */
    public void mo177dI(long j) {
        switch (bFf().mo6893i(ciJ)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, ciJ, new Object[]{new Long(j)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, ciJ, new Object[]{new Long(j)}));
                break;
        }
        m207dH(j);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Percentage Deductible")
    @C5566aOg
    /* renamed from: dK */
    public void mo178dK(long j) {
        switch (bFf().mo6893i(ciL)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, ciL, new Object[]{new Long(j)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, ciL, new Object[]{new Long(j)}));
                break;
        }
        m208dJ(j);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Default Description")
    @C5566aOg
    /* renamed from: eJ */
    public void mo179eJ(I18NString i18NString) {
        switch (bFf().mo6893i(ciN)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, ciN, new Object[]{i18NString}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, ciN, new Object[]{i18NString}));
                break;
        }
        m211eI(i18NString);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Default Insurance Description")
    @C5566aOg
    /* renamed from: eL */
    public void mo180eL(I18NString i18NString) {
        switch (bFf().mo6893i(ciP)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, ciP, new Object[]{i18NString}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, ciP, new Object[]{i18NString}));
                break;
        }
        m212eK(i18NString);
    }

    public String getHandle() {
        switch (bFf().mo6893i(f19bP)) {
            case 0:
                return null;
            case 2:
                return (String) bFf().mo5606d(new aCE(this, f19bP, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f19bP, new Object[0]));
                break;
        }
        return m202ar();
    }

    @C0064Am(aul = "24c0e001000bbf68bf2d4b1d7473af15", aum = 0)
    /* renamed from: ap */
    private UUID m201ap() {
        return m200an();
    }

    @C0064Am(aul = "d4ae0e6a7ac00cfe587212b787e9489e", aum = 0)
    /* renamed from: b */
    private void m203b(UUID uuid) {
        m199a(uuid);
    }

    @C0064Am(aul = "e6204cf6cb0914e5178f3056b2e90cb3", aum = 0)
    /* renamed from: ar */
    private String m202ar() {
        return "insurance_defaults";
    }

    /* renamed from: S */
    public void mo10S() {
        super.mo10S();
        m199a(UUID.randomUUID());
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Price Ceiling")
    @C0064Am(aul = "fc893c5756924de3ada83f76b0ff22cd", aum = 0)
    private long aws() {
        return awo();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Percentage Deductible")
    @C0064Am(aul = "d69d41072421bda8d7ebe758c676ebf3", aum = 0)
    private long awu() {
        return awp();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Default Description")
    @C0064Am(aul = "97557b619ac9d1488cbdf55daf76963d", aum = 0)
    private I18NString aww() {
        return awr();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Default Insurance Description")
    @C0064Am(aul = "9072ba79b8a61ecbec0b9b09353e05c2", aum = 0)
    private I18NString awy() {
        return awq();
    }
}
