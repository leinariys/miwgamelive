package game.script.insurance;

import game.network.message.externalizable.aCE;
import game.network.message.serializable.C1556Wo;
import game.script.item.Item;
import game.script.item.ItemLocation;
import game.script.player.Player;
import game.script.ship.Ship;
import logic.baa.C1616Xf;
import logic.baa.C1634Xv;
import logic.baa.C5511aMd;
import logic.baa.C6485anp;
import logic.data.mbean.C1071Pf;
import logic.res.code.C5640aRc;
import logic.res.code.C5663aRz;
import logic.res.code.C6494any;
import logic.res.html.C2491fm;
import p001a.*;

import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;

@C5511aMd
@C6485anp
/* renamed from: a.aVj  reason: case insensitive filesystem */
/* compiled from: a */
public class Insurer extends ItemLocation implements C1616Xf {

    /* renamed from: RA */
    public static final C2491fm f3979RA = null;

    /* renamed from: RD */
    public static final C2491fm f3980RD = null;
    public static final C2491fm _f_onResurrect_0020_0028_0029V = null;
    public static final int _m_fieldCount = 0;
    public static final C5663aRz[] _m_fields = null;
    public static final int _m_methodCount = 0;
    public static final C2491fm[] _m_methods = null;
    public static final C5663aRz iZg = null;
    public static final C2491fm iZh = null;
    public static final C2491fm iZi = null;
    public static final C2491fm iZj = null;
    public static final C2491fm iZk = null;
    public static final C2491fm iZl = null;
    public static final C2491fm iZm = null;
    /* renamed from: ku */
    public static final C2491fm f3981ku = null;
    public static final long serialVersionUID = 0;
    public static C6494any ___iScriptClass;
    @C0064Am(aul = "576ece5fa8083ec025a974135f9d4030", aum = 0)
    private static C1556Wo<Player, Ship> dPC;

    static {
        m19046V();
    }

    public Insurer() {
        super((C5540aNg) null);
        super.mo10S();
    }

    public Insurer(C5540aNg ang) {
        super(ang);
    }

    /* renamed from: V */
    static void m19046V() {
        serialVersionUID = (long) 1;
        _m_fieldCount = ItemLocation._m_fieldCount + 1;
        _m_methodCount = ItemLocation._m_methodCount + 10;
        int i = ItemLocation._m_fieldCount;
        C5663aRz[] arzArr = new C5663aRz[(i + 1)];
        C5663aRz b = C5640aRc.m17844b(Insurer.class, "576ece5fa8083ec025a974135f9d4030", i);
        iZg = b;
        arzArr[i] = b;
        int i2 = i + 1;
        _m_fields = arzArr;
        C1634Xv.m11725a((Object[]) ItemLocation._m_fields, (Object[]) _m_fields);
        int i3 = ItemLocation._m_methodCount;
        C2491fm[] fmVarArr = new C2491fm[(i3 + 10)];
        C2491fm a = C4105zY.m41624a(Insurer.class, "8a998fb2592cc910001a1a980c2fd5cd", i3);
        iZh = a;
        fmVarArr[i3] = a;
        int i4 = i3 + 1;
        C2491fm a2 = C4105zY.m41624a(Insurer.class, "75e0fbded1cef8352ae807023ecbab26", i4);
        iZi = a2;
        fmVarArr[i4] = a2;
        int i5 = i4 + 1;
        C2491fm a3 = C4105zY.m41624a(Insurer.class, "ca413c760dbcc8bd637db48a2f2a5cda", i5);
        iZj = a3;
        fmVarArr[i5] = a3;
        int i6 = i5 + 1;
        C2491fm a4 = C4105zY.m41624a(Insurer.class, "4134364f9a2dac4f59c9f2be052fedd2", i6);
        f3979RA = a4;
        fmVarArr[i6] = a4;
        int i7 = i6 + 1;
        C2491fm a5 = C4105zY.m41624a(Insurer.class, "6d1e3e1bdc6c0d9c5afc983459af828a", i7);
        f3980RD = a5;
        fmVarArr[i7] = a5;
        int i8 = i7 + 1;
        C2491fm a6 = C4105zY.m41624a(Insurer.class, "9fc9ab8a40010c25d65205bfc7ce5b11", i8);
        iZk = a6;
        fmVarArr[i8] = a6;
        int i9 = i8 + 1;
        C2491fm a7 = C4105zY.m41624a(Insurer.class, "b2c0d82e5eb54603dabbeda8e4dfba1b", i9);
        iZl = a7;
        fmVarArr[i9] = a7;
        int i10 = i9 + 1;
        C2491fm a8 = C4105zY.m41624a(Insurer.class, "78ab68804fd565ea75ca402c5ea66806", i10);
        iZm = a8;
        fmVarArr[i10] = a8;
        int i11 = i10 + 1;
        C2491fm a9 = C4105zY.m41624a(Insurer.class, "f01c16def39afeb8a75191f81216b8e5", i11);
        _f_onResurrect_0020_0028_0029V = a9;
        fmVarArr[i11] = a9;
        int i12 = i11 + 1;
        C2491fm a10 = C4105zY.m41624a(Insurer.class, "10226b8df5480261e51517d59328210b", i12);
        f3981ku = a10;
        fmVarArr[i12] = a10;
        int i13 = i12 + 1;
        _m_methods = fmVarArr;
        C1634Xv.m11725a((Object[]) ItemLocation._m_methods, (Object[]) _m_methods);
        ___iScriptClass = aUO.m18566a(Insurer.class, C1071Pf.class, _m_fields, _m_methods);
    }

    @C5566aOg
    @C0064Am(aul = "ca413c760dbcc8bd637db48a2f2a5cda", aum = 0)
    @C2499fr
    /* renamed from: a */
    private List<Item> m19047a(Player aku, List<Item> list) {
        throw new aWi(new aCE(this, iZj, new Object[]{aku, list}));
    }

    /* renamed from: at */
    private void m19050at(C1556Wo wo) {
        bFf().mo5608dq().mo3197f(iZg, wo);
    }

    @C0064Am(aul = "8a998fb2592cc910001a1a980c2fd5cd", aum = 0)
    @C5566aOg
    @C2499fr
    /* renamed from: b */
    private void m19051b(Player aku, Ship fAVar) {
        throw new aWi(new aCE(this, iZh, new Object[]{aku, fAVar}));
    }

    private C1556Wo dBv() {
        return (C1556Wo) bFf().mo5608dq().mo3214p(iZg);
    }

    @C0064Am(aul = "75e0fbded1cef8352ae807023ecbab26", aum = 0)
    @C5566aOg
    /* renamed from: dg */
    private void m19052dg(Player aku) {
        throw new aWi(new aCE(this, iZi, new Object[]{aku}));
    }

    @C5566aOg
    /* renamed from: dh */
    private void m19053dh(Player aku) {
        switch (bFf().mo6893i(iZi)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, iZi, new Object[]{aku}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, iZi, new Object[]{aku}));
                break;
        }
        m19052dg(aku);
    }

    @C0064Am(aul = "9fc9ab8a40010c25d65205bfc7ce5b11", aum = 0)
    @C5566aOg
    @C2499fr
    /* renamed from: di */
    private boolean m19054di(Player aku) {
        throw new aWi(new aCE(this, iZk, new Object[]{aku}));
    }

    @C0064Am(aul = "b2c0d82e5eb54603dabbeda8e4dfba1b", aum = 0)
    @C5566aOg
    @C2499fr
    /* renamed from: dk */
    private Ship m19055dk(Player aku) {
        throw new aWi(new aCE(this, iZl, new Object[]{aku}));
    }

    @C0064Am(aul = "78ab68804fd565ea75ca402c5ea66806", aum = 0)
    @C5566aOg
    /* renamed from: dm */
    private void m19056dm(Player aku) {
        throw new aWi(new aCE(this, iZm, new Object[]{aku}));
    }

    /* renamed from: T */
    public C6494any mo11T() {
        return ___iScriptClass;
    }

    /* renamed from: U */
    public C2491fm[] mo12U() {
        return _m_methods;
    }

    /* renamed from: W */
    public Object mo13W() {
        return new C1071Pf(this);
    }

    /* renamed from: a */
    public Object mo14a(C0495Gr gr) {
        Object[] args = gr.getArgs();
        switch (gr.mo2417hq() - ItemLocation._m_methodCount) {
            case 0:
                m19051b((Player) args[0], (Ship) args[1]);
                return null;
            case 1:
                m19052dg((Player) args[0]);
                return null;
            case 2:
                return m19047a((Player) args[0], (List<Item>) (List) args[1]);
            case 3:
                m19048a((Item) args[0], (ItemLocation) args[1]);
                return null;
            case 4:
                m19058m((Item) args[0]);
                return null;
            case 5:
                return new Boolean(m19054di((Player) args[0]));
            case 6:
                return m19055dk((Player) args[0]);
            case 7:
                m19056dm((Player) args[0]);
                return null;
            case 8:
                m19049aG();
                return null;
            case 9:
                m19057g((Item) args[0]);
                return null;
            default:
                return super.mo14a(gr);
        }
    }

    /* renamed from: a */
    public void mo15a(Collection collection, C0495Gr gr) {
        super.mo15a(collection, gr);
    }

    /* renamed from: aH */
    public void mo70aH() {
        switch (bFf().mo6893i(_f_onResurrect_0020_0028_0029V)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, _f_onResurrect_0020_0028_0029V, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, _f_onResurrect_0020_0028_0029V, new Object[0]));
                break;
        }
        m19049aG();
    }

    @C5566aOg
    @C2499fr
    /* renamed from: b */
    public List<Item> mo11895b(Player aku, List<Item> list) {
        switch (bFf().mo6893i(iZj)) {
            case 0:
                return null;
            case 2:
                return (List) bFf().mo5606d(new aCE(this, iZj, new Object[]{aku, list}));
            case 3:
                bFf().mo5606d(new aCE(this, iZj, new Object[]{aku, list}));
                break;
        }
        return m19047a(aku, list);
    }

    /* access modifiers changed from: protected */
    /* renamed from: b */
    public void mo1887b(Item auq, ItemLocation aag) {
        switch (bFf().mo6893i(f3979RA)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f3979RA, new Object[]{auq, aag}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f3979RA, new Object[]{auq, aag}));
                break;
        }
        m19048a(auq, aag);
    }

    @C5566aOg
    @C2499fr
    /* renamed from: c */
    public void mo11896c(Player aku, Ship fAVar) {
        switch (bFf().mo6893i(iZh)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, iZh, new Object[]{aku, fAVar}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, iZh, new Object[]{aku, fAVar}));
                break;
        }
        m19051b(aku, fAVar);
    }

    /* renamed from: c */
    public C5663aRz[] mo25c() {
        return _m_fields;
    }

    @C5566aOg
    @C2499fr
    /* renamed from: dj */
    public boolean mo11897dj(Player aku) {
        switch (bFf().mo6893i(iZk)) {
            case 0:
                return false;
            case 2:
                return ((Boolean) bFf().mo5606d(new aCE(this, iZk, new Object[]{aku}))).booleanValue();
            case 3:
                bFf().mo5606d(new aCE(this, iZk, new Object[]{aku}));
                break;
        }
        return m19054di(aku);
    }

    @C5566aOg
    @C2499fr
    /* renamed from: dl */
    public Ship mo11898dl(Player aku) {
        switch (bFf().mo6893i(iZl)) {
            case 0:
                return null;
            case 2:
                return (Ship) bFf().mo5606d(new aCE(this, iZl, new Object[]{aku}));
            case 3:
                bFf().mo5606d(new aCE(this, iZl, new Object[]{aku}));
                break;
        }
        return m19055dk(aku);
    }

    @C5566aOg
    /* renamed from: dn */
    public void mo11899dn(Player aku) {
        switch (bFf().mo6893i(iZm)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, iZm, new Object[]{aku}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, iZm, new Object[]{aku}));
                break;
        }
        m19056dm(aku);
    }

    /* access modifiers changed from: protected */
    /* renamed from: h */
    public void mo1889h(Item auq) {
        switch (bFf().mo6893i(f3981ku)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f3981ku, new Object[]{auq}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f3981ku, new Object[]{auq}));
                break;
        }
        m19057g(auq);
    }

    /* access modifiers changed from: protected */
    /* renamed from: n */
    public void mo1890n(Item auq) {
        switch (bFf().mo6893i(f3980RD)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f3980RD, new Object[]{auq}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f3980RD, new Object[]{auq}));
                break;
        }
        m19058m(auq);
    }

    /* renamed from: S */
    public void mo10S() {
        super.mo10S();
    }

    @C0064Am(aul = "4134364f9a2dac4f59c9f2be052fedd2", aum = 0)
    /* renamed from: a */
    private void m19048a(Item auq, ItemLocation aag) {
    }

    @C0064Am(aul = "6d1e3e1bdc6c0d9c5afc983459af828a", aum = 0)
    /* renamed from: m */
    private void m19058m(Item auq) {
    }

    @C0064Am(aul = "f01c16def39afeb8a75191f81216b8e5", aum = 0)
    /* renamed from: aG */
    private void m19049aG() {
        Iterator it = new HashSet(dBv().keySet()).iterator();
        while (it.hasNext()) {
            Player aku = (Player) it.next();
            if (aku.isDisposed()) {
                Ship fAVar = (Ship) dBv().remove(aku);
                if (fAVar != null) {
                    fAVar.dispose();
                }
                mo6317hy("Player '" + aku.bFY() + "' has been removed from Insurer map cause it's disposed");
            }
        }
        super.mo70aH();
    }

    @C0064Am(aul = "10226b8df5480261e51517d59328210b", aum = 0)
    /* renamed from: g */
    private void m19057g(Item auq) {
    }
}
