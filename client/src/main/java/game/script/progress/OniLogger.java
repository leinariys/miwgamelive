package game.script.progress;

import game.network.message.externalizable.aCE;
import game.network.message.serializable.C1556Wo;
import game.network.message.serializable.C2686iZ;
import game.script.TaikodomObject;
import game.script.item.BlueprintType;
import game.script.item.ItemTypeCategory;
import game.script.player.Player;
import game.script.ship.SectorCategory;
import game.script.ship.Station;
import game.script.space.Node;
import logic.baa.C1616Xf;
import logic.baa.C1634Xv;
import logic.baa.C5511aMd;
import logic.baa.C6485anp;
import logic.data.mbean.C4005xy;
import logic.res.code.C5640aRc;
import logic.res.code.C5663aRz;
import logic.res.code.C6494any;
import logic.res.html.C2491fm;
import p001a.*;

import java.util.Collection;
import java.util.Iterator;
import java.util.Set;

@C5511aMd
@C6485anp
/* renamed from: a.aEx  reason: case insensitive filesystem */
/* compiled from: a */
public class OniLogger extends TaikodomObject implements C1616Xf {

    /* renamed from: Lm */
    public static final C2491fm f2776Lm = null;
    public static final C2491fm _f_dispose_0020_0028_0029V = null;
    public static final int _m_fieldCount = 0;
    public static final C5663aRz[] _m_fields = null;
    public static final int _m_methodCount = 0;
    public static final C2491fm[] _m_methods = null;
    public static final C5663aRz avF = null;
    public static final C5663aRz ciZ = null;
    public static final C2491fm cjT = null;
    public static final C5663aRz hFB = null;
    public static final C5663aRz hFC = null;
    public static final C5663aRz hFD = null;
    public static final C5663aRz hFE = null;
    public static final C5663aRz hFF = null;
    public static final C2491fm hFG = null;
    public static final C2491fm hFH = null;
    public static final C2491fm hFI = null;
    public static final C2491fm hFJ = null;
    public static final C2491fm hFK = null;
    public static final C2491fm hFL = null;
    public static final C2491fm hFM = null;
    public static final C2491fm hFN = null;
    public static final C2491fm hFO = null;
    public static final C2491fm hFP = null;
    public static final C2491fm hFQ = null;
    public static final C2491fm hFR = null;
    public static final C2491fm hFS = null;
    public static final C2491fm hFT = null;
    public static final C2491fm hFU = null;
    public static final C2491fm hFV = null;
    public static final C2491fm hFW = null;
    public static final long serialVersionUID = 0;
    public static C6494any ___iScriptClass;
    @C0064Am(aul = "ce39aecba9ecd09dbe9e6feee6506dff", aum = 1)
    private static long bGr;
    @C0064Am(aul = "a3be18c79746f701ae066062242301c6", aum = 2)
    private static C1556Wo<Object, Integer> bGs;
    @C0064Am(aul = "ea42992238ed9899efcf3024adde3413", aum = 3)
    private static C1556Wo<Object, Integer> bGt;
    @C0064Am(aul = "5607ae72736243f6f608e70a49355762", aum = 4)
    private static C1556Wo<Object, Integer> bGu;
    @C0064Am(aul = "71178f5f5acbc223bb8850b5facdc0c2", aum = 5)
    private static C2686iZ<Station> bGv;
    @C0064Am(aul = "07d2d523327766a68f47aa78938be465", aum = 6)
    private static C2686iZ<Node> bGw;
    @C0064Am(aul = "07ccce613595bb4ac15d721ae71195dc", aum = 0)

    /* renamed from: nj */
    private static Player f2777nj;

    static {
        m14422V();
    }

    public OniLogger() {
        super((C5540aNg) null);
        super.mo10S();
    }

    public OniLogger(C5540aNg ang) {
        super(ang);
    }

    public OniLogger(Player aku) {
        super((C5540aNg) null);
        super._m_script_init(aku);
    }

    /* renamed from: V */
    static void m14422V() {
        serialVersionUID = (long) 1;
        _m_fieldCount = TaikodomObject._m_fieldCount + 7;
        _m_methodCount = TaikodomObject._m_methodCount + 20;
        int i = TaikodomObject._m_fieldCount;
        C5663aRz[] arzArr = new C5663aRz[(i + 7)];
        C5663aRz b = C5640aRc.m17844b(OniLogger.class, "07ccce613595bb4ac15d721ae71195dc", i);
        avF = b;
        arzArr[i] = b;
        int i2 = i + 1;
        C5663aRz b2 = C5640aRc.m17844b(OniLogger.class, "ce39aecba9ecd09dbe9e6feee6506dff", i2);
        ciZ = b2;
        arzArr[i2] = b2;
        int i3 = i2 + 1;
        C5663aRz b3 = C5640aRc.m17844b(OniLogger.class, "a3be18c79746f701ae066062242301c6", i3);
        hFB = b3;
        arzArr[i3] = b3;
        int i4 = i3 + 1;
        C5663aRz b4 = C5640aRc.m17844b(OniLogger.class, "ea42992238ed9899efcf3024adde3413", i4);
        hFC = b4;
        arzArr[i4] = b4;
        int i5 = i4 + 1;
        C5663aRz b5 = C5640aRc.m17844b(OniLogger.class, "5607ae72736243f6f608e70a49355762", i5);
        hFD = b5;
        arzArr[i5] = b5;
        int i6 = i5 + 1;
        C5663aRz b6 = C5640aRc.m17844b(OniLogger.class, "71178f5f5acbc223bb8850b5facdc0c2", i6);
        hFE = b6;
        arzArr[i6] = b6;
        int i7 = i6 + 1;
        C5663aRz b7 = C5640aRc.m17844b(OniLogger.class, "07d2d523327766a68f47aa78938be465", i7);
        hFF = b7;
        arzArr[i7] = b7;
        int i8 = i7 + 1;
        _m_fields = arzArr;
        C1634Xv.m11725a((Object[]) TaikodomObject._m_fields, (Object[]) _m_fields);
        int i9 = TaikodomObject._m_methodCount;
        C2491fm[] fmVarArr = new C2491fm[(i9 + 20)];
        C2491fm a = C4105zY.m41624a(OniLogger.class, "4f7fdfcceeb2323cde6e69c038697306", i9);
        _f_dispose_0020_0028_0029V = a;
        fmVarArr[i9] = a;
        int i10 = i9 + 1;
        C2491fm a2 = C4105zY.m41624a(OniLogger.class, "1f74047d976078916b939b9ff0a84042", i10);
        cjT = a2;
        fmVarArr[i10] = a2;
        int i11 = i10 + 1;
        C2491fm a3 = C4105zY.m41624a(OniLogger.class, "a028c55480c505b161ea8ec433766bc4", i11);
        hFG = a3;
        fmVarArr[i11] = a3;
        int i12 = i11 + 1;
        C2491fm a4 = C4105zY.m41624a(OniLogger.class, "805d8db13b4ad9a931bf5ac14ea4788e", i12);
        hFH = a4;
        fmVarArr[i12] = a4;
        int i13 = i12 + 1;
        C2491fm a5 = C4105zY.m41624a(OniLogger.class, "4dc73bd06fef796112272d690efed609", i13);
        hFI = a5;
        fmVarArr[i13] = a5;
        int i14 = i13 + 1;
        C2491fm a6 = C4105zY.m41624a(OniLogger.class, "bbd409b7ca3d2c4fcd77680da128e191", i14);
        hFJ = a6;
        fmVarArr[i14] = a6;
        int i15 = i14 + 1;
        C2491fm a7 = C4105zY.m41624a(OniLogger.class, "d7e4cae51af7a9eebe85c6cefea9d829", i15);
        hFK = a7;
        fmVarArr[i15] = a7;
        int i16 = i15 + 1;
        C2491fm a8 = C4105zY.m41624a(OniLogger.class, "66426c73bb946bf1195fd41de06785b3", i16);
        hFL = a8;
        fmVarArr[i16] = a8;
        int i17 = i16 + 1;
        C2491fm a9 = C4105zY.m41624a(OniLogger.class, "d08cfa7dc4a9423cc62a8a7dcfbb4939", i17);
        hFM = a9;
        fmVarArr[i17] = a9;
        int i18 = i17 + 1;
        C2491fm a10 = C4105zY.m41624a(OniLogger.class, "e87bff0fa421c80bbb0ef17192d971d0", i18);
        hFN = a10;
        fmVarArr[i18] = a10;
        int i19 = i18 + 1;
        C2491fm a11 = C4105zY.m41624a(OniLogger.class, "c5909eabe815bf718075afa9def0834b", i19);
        hFO = a11;
        fmVarArr[i19] = a11;
        int i20 = i19 + 1;
        C2491fm a12 = C4105zY.m41624a(OniLogger.class, "9dd818236ee4aa056bb4aea9555e66e1", i20);
        hFP = a12;
        fmVarArr[i20] = a12;
        int i21 = i20 + 1;
        C2491fm a13 = C4105zY.m41624a(OniLogger.class, "968e31b5bbdaf026e2de4f204fc0cfd0", i21);
        hFQ = a13;
        fmVarArr[i21] = a13;
        int i22 = i21 + 1;
        C2491fm a14 = C4105zY.m41624a(OniLogger.class, "f93b1e308f28c01d8deb4ff7ed6630d7", i22);
        hFR = a14;
        fmVarArr[i22] = a14;
        int i23 = i22 + 1;
        C2491fm a15 = C4105zY.m41624a(OniLogger.class, "6795c7706bad9ff64293b2f08cc474af", i23);
        hFS = a15;
        fmVarArr[i23] = a15;
        int i24 = i23 + 1;
        C2491fm a16 = C4105zY.m41624a(OniLogger.class, "b718aec1cbc4078825ffb3328f2f9a9f", i24);
        hFT = a16;
        fmVarArr[i24] = a16;
        int i25 = i24 + 1;
        C2491fm a17 = C4105zY.m41624a(OniLogger.class, "afa86de8c7dcaeb86006fd76077fcb66", i25);
        hFU = a17;
        fmVarArr[i25] = a17;
        int i26 = i25 + 1;
        C2491fm a18 = C4105zY.m41624a(OniLogger.class, "7797093b51b93bb2183bd01c6738a646", i26);
        f2776Lm = a18;
        fmVarArr[i26] = a18;
        int i27 = i26 + 1;
        C2491fm a19 = C4105zY.m41624a(OniLogger.class, "8728fc42e5d35064392b71ac46e5dc7e", i27);
        hFV = a19;
        fmVarArr[i27] = a19;
        int i28 = i27 + 1;
        C2491fm a20 = C4105zY.m41624a(OniLogger.class, "8d99134219c4e44034ec57b9c8adc2b3", i28);
        hFW = a20;
        fmVarArr[i28] = a20;
        int i29 = i28 + 1;
        _m_methods = fmVarArr;
        C1634Xv.m11725a((Object[]) TaikodomObject._m_methods, (Object[]) _m_methods);
        ___iScriptClass = aUO.m18566a(OniLogger.class, C4005xy.class, _m_fields, _m_methods);
    }

    /* renamed from: KQ */
    private Player m14421KQ() {
        return (Player) bFf().mo5608dq().mo3214p(avF);
    }

    /* renamed from: aj */
    private void m14426aj(C1556Wo wo) {
        bFf().mo5608dq().mo3197f(hFB, wo);
    }

    /* renamed from: ak */
    private void m14427ak(C1556Wo wo) {
        bFf().mo5608dq().mo3197f(hFC, wo);
    }

    /* renamed from: al */
    private void m14428al(C1556Wo wo) {
        bFf().mo5608dq().mo3197f(hFD, wo);
    }

    /* renamed from: am */
    private void m14429am(C2686iZ iZVar) {
        bFf().mo5608dq().mo3197f(hFE, iZVar);
    }

    /* renamed from: an */
    private void m14430an(C2686iZ iZVar) {
        bFf().mo5608dq().mo3197f(hFF, iZVar);
    }

    @C0064Am(aul = "9dd818236ee4aa056bb4aea9555e66e1", aum = 0)
    @C5566aOg
    /* renamed from: aq */
    private void m14431aq(Station bf) {
        throw new aWi(new aCE(this, hFP, new Object[]{bf}));
    }

    private long awE() {
        return bFf().mo5608dq().mo3213o(ciZ);
    }

    /* renamed from: b */
    private void m14432b(C1556Wo<Object, Integer> wo, Object obj) {
        switch (bFf().mo6893i(hFN)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, hFN, new Object[]{wo, obj}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, hFN, new Object[]{wo, obj}));
                break;
        }
        m14423a(wo, obj);
    }

    /* renamed from: b */
    private void m14433b(C1556Wo<Object, Integer> wo, Object obj, int i) {
        switch (bFf().mo6893i(hFM)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, hFM, new Object[]{wo, obj, new Integer(i)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, hFM, new Object[]{wo, obj, new Integer(i)}));
                break;
        }
        m14424a(wo, obj, i);
    }

    /* renamed from: b */
    private void m14434b(ItemTypeCategory aai, int i) {
        switch (bFf().mo6893i(hFL)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, hFL, new Object[]{aai, new Integer(i)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, hFL, new Object[]{aai, new Integer(i)}));
                break;
        }
        m14425a(aai, i);
    }

    private C1556Wo cXA() {
        return (C1556Wo) bFf().mo5608dq().mo3214p(hFB);
    }

    private C1556Wo cXB() {
        return (C1556Wo) bFf().mo5608dq().mo3214p(hFC);
    }

    private C1556Wo cXC() {
        return (C1556Wo) bFf().mo5608dq().mo3214p(hFD);
    }

    private C2686iZ cXD() {
        return (C2686iZ) bFf().mo5608dq().mo3214p(hFE);
    }

    private C2686iZ cXE() {
        return (C2686iZ) bFf().mo5608dq().mo3214p(hFF);
    }

    /* renamed from: dP */
    private void m14436dP(long j) {
        bFf().mo5608dq().mo3184b(ciZ, j);
    }

    /* renamed from: j */
    private void m14439j(SectorCategory fMVar) {
        switch (bFf().mo6893i(hFI)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, hFI, new Object[]{fMVar}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, hFI, new Object[]{fMVar}));
                break;
        }
        m14438i(fMVar);
    }

    @C0064Am(aul = "a028c55480c505b161ea8ec433766bc4", aum = 0)
    @C5566aOg
    /* renamed from: kA */
    private void m14441kA(long j) {
        throw new aWi(new aCE(this, hFG, new Object[]{new Long(j)}));
    }

    @C0064Am(aul = "c5909eabe815bf718075afa9def0834b", aum = 0)
    @C5566aOg
    /* renamed from: m */
    private void m14442m(Node rPVar) {
        throw new aWi(new aCE(this, hFO, new Object[]{rPVar}));
    }

    @C0064Am(aul = "805d8db13b4ad9a931bf5ac14ea4788e", aum = 0)
    @C5566aOg
    /* renamed from: o */
    private void m14443o(BlueprintType mr) {
        throw new aWi(new aCE(this, hFH, new Object[]{mr}));
    }

    @C0064Am(aul = "7797093b51b93bb2183bd01c6738a646", aum = 0)
    @C5566aOg
    @C2499fr
    /* renamed from: qU */
    private void m14445qU() {
        throw new aWi(new aCE(this, f2776Lm, new Object[0]));
    }

    /* renamed from: r */
    private void m14446r(BlueprintType mr) {
        switch (bFf().mo6893i(hFJ)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, hFJ, new Object[]{mr}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, hFJ, new Object[]{mr}));
                break;
        }
        m14444q(mr);
    }

    /* renamed from: u */
    private void m14449u(Player aku) {
        bFf().mo5608dq().mo3197f(avF, aku);
    }

    /* renamed from: v */
    private void m14450v(ItemTypeCategory aai) {
        switch (bFf().mo6893i(hFK)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, hFK, new Object[]{aai}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, hFK, new Object[]{aai}));
                break;
        }
        m14448u(aai);
    }

    /* renamed from: S */
    public void mo10S() {
        super.mo10S();
    }

    /* renamed from: T */
    public C6494any mo11T() {
        return ___iScriptClass;
    }

    /* renamed from: U */
    public C2491fm[] mo12U() {
        return _m_methods;
    }

    /* renamed from: W */
    public Object mo13W() {
        return new C4005xy(this);
    }

    /* renamed from: a */
    public Object mo14a(C0495Gr gr) {
        Object[] args = gr.getArgs();
        switch (gr.mo2417hq() - TaikodomObject._m_methodCount) {
            case 0:
                m14437fg();
                return null;
            case 1:
                return new Long(axv());
            case 2:
                m14441kA(((Long) args[0]).longValue());
                return null;
            case 3:
                m14443o((BlueprintType) args[0]);
                return null;
            case 4:
                m14438i((SectorCategory) args[0]);
                return null;
            case 5:
                m14444q((BlueprintType) args[0]);
                return null;
            case 6:
                m14448u((ItemTypeCategory) args[0]);
                return null;
            case 7:
                m14425a((ItemTypeCategory) args[0], ((Integer) args[1]).intValue());
                return null;
            case 8:
                m14424a((C1556Wo) args[0], args[1], ((Integer) args[2]).intValue());
                return null;
            case 9:
                m14423a((C1556Wo<Object, Integer>) (C1556Wo) args[0], args[1]);
                return null;
            case 10:
                m14442m((Node) args[0]);
                return null;
            case 11:
                m14431aq((Station) args[0]);
                return null;
            case 12:
                return new Integer(m14435c((C1556Wo) args[0], args[1]));
            case 13:
                return new Integer(m14440k((SectorCategory) args[0]));
            case 14:
                return new Integer(m14451w((ItemTypeCategory) args[0]));
            case 15:
                return new Integer(m14447s((BlueprintType) args[0]));
            case 16:
                return new Integer(cXF());
            case 17:
                m14445qU();
                return null;
            case 18:
                return cXH();
            case 19:
                return cXJ();
            default:
                return super.mo14a(gr);
        }
    }

    /* renamed from: a */
    public void mo15a(Collection collection, C0495Gr gr) {
        super.mo15a(collection, gr);
    }

    @C5566aOg
    /* renamed from: ar */
    public void mo8697ar(Station bf) {
        switch (bFf().mo6893i(hFP)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, hFP, new Object[]{bf}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, hFP, new Object[]{bf}));
                break;
        }
        m14431aq(bf);
    }

    public long axw() {
        switch (bFf().mo6893i(cjT)) {
            case 0:
                return 0;
            case 2:
                return ((Long) bFf().mo5606d(new aCE(this, cjT, new Object[0]))).longValue();
            case 3:
                bFf().mo5606d(new aCE(this, cjT, new Object[0]));
                break;
        }
        return axv();
    }

    /* renamed from: c */
    public C5663aRz[] mo25c() {
        return _m_fields;
    }

    public int cXG() {
        switch (bFf().mo6893i(hFU)) {
            case 0:
                return 0;
            case 2:
                return ((Integer) bFf().mo5606d(new aCE(this, hFU, new Object[0]))).intValue();
            case 3:
                bFf().mo5606d(new aCE(this, hFU, new Object[0]));
                break;
        }
        return cXF();
    }

    public Set<Station> cXI() {
        switch (bFf().mo6893i(hFV)) {
            case 0:
                return null;
            case 2:
                return (Set) bFf().mo5606d(new aCE(this, hFV, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, hFV, new Object[0]));
                break;
        }
        return cXH();
    }

    public C2686iZ<Node> cXK() {
        switch (bFf().mo6893i(hFW)) {
            case 0:
                return null;
            case 2:
                return (C2686iZ) bFf().mo5606d(new aCE(this, hFW, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, hFW, new Object[0]));
                break;
        }
        return cXJ();
    }

    /* renamed from: d */
    public int mo8703d(C1556Wo<Object, Integer> wo, Object obj) {
        switch (bFf().mo6893i(hFQ)) {
            case 0:
                return 0;
            case 2:
                return ((Integer) bFf().mo5606d(new aCE(this, hFQ, new Object[]{wo, obj}))).intValue();
            case 3:
                bFf().mo5606d(new aCE(this, hFQ, new Object[]{wo, obj}));
                break;
        }
        return m14435c(wo, obj);
    }

    public void dispose() {
        switch (bFf().mo6893i(_f_dispose_0020_0028_0029V)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, _f_dispose_0020_0028_0029V, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, _f_dispose_0020_0028_0029V, new Object[0]));
                break;
        }
        m14437fg();
    }

    @C5566aOg
    /* renamed from: kB */
    public void mo8704kB(long j) {
        switch (bFf().mo6893i(hFG)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, hFG, new Object[]{new Long(j)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, hFG, new Object[]{new Long(j)}));
                break;
        }
        m14441kA(j);
    }

    /* renamed from: l */
    public int mo8705l(SectorCategory fMVar) {
        switch (bFf().mo6893i(hFR)) {
            case 0:
                return 0;
            case 2:
                return ((Integer) bFf().mo5606d(new aCE(this, hFR, new Object[]{fMVar}))).intValue();
            case 3:
                bFf().mo5606d(new aCE(this, hFR, new Object[]{fMVar}));
                break;
        }
        return m14440k(fMVar);
    }

    @C5566aOg
    /* renamed from: n */
    public void mo8706n(Node rPVar) {
        switch (bFf().mo6893i(hFO)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, hFO, new Object[]{rPVar}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, hFO, new Object[]{rPVar}));
                break;
        }
        m14442m(rPVar);
    }

    @C5566aOg
    /* renamed from: p */
    public void mo8707p(BlueprintType mr) {
        switch (bFf().mo6893i(hFH)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, hFH, new Object[]{mr}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, hFH, new Object[]{mr}));
                break;
        }
        m14443o(mr);
    }

    @C5566aOg
    @C2499fr
    /* renamed from: qV */
    public void mo8708qV() {
        switch (bFf().mo6893i(f2776Lm)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f2776Lm, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f2776Lm, new Object[0]));
                break;
        }
        m14445qU();
    }

    /* renamed from: t */
    public int mo8709t(BlueprintType mr) {
        switch (bFf().mo6893i(hFT)) {
            case 0:
                return 0;
            case 2:
                return ((Integer) bFf().mo5606d(new aCE(this, hFT, new Object[]{mr}))).intValue();
            case 3:
                bFf().mo5606d(new aCE(this, hFT, new Object[]{mr}));
                break;
        }
        return m14447s(mr);
    }

    /* renamed from: x */
    public int mo8710x(ItemTypeCategory aai) {
        switch (bFf().mo6893i(hFS)) {
            case 0:
                return 0;
            case 2:
                return ((Integer) bFf().mo5606d(new aCE(this, hFS, new Object[]{aai}))).intValue();
            case 3:
                bFf().mo5606d(new aCE(this, hFS, new Object[]{aai}));
                break;
        }
        return m14451w(aai);
    }

    /* renamed from: b */
    public void mo8699b(Player aku) {
        super.mo10S();
        m14449u(aku);
    }

    @C0064Am(aul = "4f7fdfcceeb2323cde6e69c038697306", aum = 0)
    /* renamed from: fg */
    private void m14437fg() {
        m14449u((Player) null);
        super.dispose();
    }

    @C0064Am(aul = "1f74047d976078916b939b9ff0a84042", aum = 0)
    private long axv() {
        return awE();
    }

    @C0064Am(aul = "4dc73bd06fef796112272d690efed609", aum = 0)
    /* renamed from: i */
    private void m14438i(SectorCategory fMVar) {
        m14432b((C1556Wo<Object, Integer>) cXB(), (Object) fMVar);
    }

    @C0064Am(aul = "bbd409b7ca3d2c4fcd77680da128e191", aum = 0)
    /* renamed from: q */
    private void m14444q(BlueprintType mr) {
        m14432b((C1556Wo<Object, Integer>) cXC(), (Object) mr);
    }

    @C0064Am(aul = "d7e4cae51af7a9eebe85c6cefea9d829", aum = 0)
    /* renamed from: u */
    private void m14448u(ItemTypeCategory aai) {
        m14432b((C1556Wo<Object, Integer>) cXA(), (Object) aai);
    }

    @C0064Am(aul = "66426c73bb946bf1195fd41de06785b3", aum = 0)
    /* renamed from: a */
    private void m14425a(ItemTypeCategory aai, int i) {
        m14433b(cXA(), aai, i);
    }

    @C0064Am(aul = "d08cfa7dc4a9423cc62a8a7dcfbb4939", aum = 0)
    /* renamed from: a */
    private void m14424a(C1556Wo<Object, Integer> wo, Object obj, int i) {
        Integer valueOf;
        Integer num = (Integer) wo.get(obj);
        if (num == null) {
            valueOf = Integer.valueOf(i);
        } else {
            valueOf = Integer.valueOf(num.intValue() + i);
        }
        wo.put(obj, valueOf);
    }

    @C0064Am(aul = "e87bff0fa421c80bbb0ef17192d971d0", aum = 0)
    /* renamed from: a */
    private void m14423a(C1556Wo<Object, Integer> wo, Object obj) {
        int valueOf;
        Integer num = (Integer) wo.get(obj);
        if (num == null) {
            valueOf = 1;
        } else {
            valueOf = Integer.valueOf(num.intValue() + 1);
        }
        wo.put(obj, valueOf);
    }

    @C0064Am(aul = "968e31b5bbdaf026e2de4f204fc0cfd0", aum = 0)
    /* renamed from: c */
    private int m14435c(C1556Wo<Object, Integer> wo, Object obj) {
        Integer num = (Integer) wo.get(obj);
        if (num == null) {
            return 0;
        }
        return num.intValue();
    }

    @C0064Am(aul = "f93b1e308f28c01d8deb4ff7ed6630d7", aum = 0)
    /* renamed from: k */
    private int m14440k(SectorCategory fMVar) {
        return mo8703d(cXB(), fMVar);
    }

    @C0064Am(aul = "6795c7706bad9ff64293b2f08cc474af", aum = 0)
    /* renamed from: w */
    private int m14451w(ItemTypeCategory aai) {
        return mo8703d(cXA(), aai);
    }

    @C0064Am(aul = "b718aec1cbc4078825ffb3328f2f9a9f", aum = 0)
    /* renamed from: s */
    private int m14447s(BlueprintType mr) {
        return mo8703d(cXC(), mr);
    }

    @C0064Am(aul = "afa86de8c7dcaeb86006fd76077fcb66", aum = 0)
    private int cXF() {
        int i = 0;
        Iterator it = cXC().values().iterator();
        while (true) {
            int i2 = i;
            if (!it.hasNext()) {
                return i2;
            }
            i = ((Integer) it.next()).intValue() + i2;
        }
    }

    @C0064Am(aul = "8728fc42e5d35064392b71ac46e5dc7e", aum = 0)
    private Set<Station> cXH() {
        return cXD();
    }

    @C0064Am(aul = "8d99134219c4e44034ec57b9c8adc2b3", aum = 0)
    private C2686iZ<Node> cXJ() {
        return cXE();
    }
}
