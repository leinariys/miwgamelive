package game.script;

import game.engine.IEngineGame;
import game.network.message.externalizable.aCE;
import game.script.login.UserConnection;
import game.script.player.Player;
import logic.baa.*;
import logic.data.mbean.C3805vD;
import logic.res.code.C5663aRz;
import logic.res.code.C6494any;
import logic.res.html.C2491fm;
import logic.sql.C5878acG;
import logic.thred.LogPrinter;
import org.apache.commons.logging.Log;
import org.mozilla1.javascript.ScriptRuntime;
import p001a.*;

import java.util.Collection;

@C6968axh
@C5511aMd
@C6485anp
/* renamed from: a.FC */
/* compiled from: a */
public class TaikodomObject extends aDJ implements C1616Xf {
    /* renamed from: _f_appLogOutsideGame_0020_0028Ltaikodom_002fgame_002fscript_002flog_002fLogRecord_003b_0029V */
    public static final C2491fm f518x11b2d208 = null;
    /* renamed from: _f_appLog_0020_0028Ltaikodom_002fgame_002fscript_002flog_002fLogRecord_003b_0029V */
    public static final C2491fm f519x75159c47 = null;
    /* renamed from: _f_clientPlayer_0020_0028_0029Ltaikodom_002fgame_002fscript_002fplayer_002fPlayer_003b */
    public static final C2491fm f520x97abc781 = null;
    /* renamed from: _f_getCallOriginPlayer_0020_0028_0029Ltaikodom_002fgame_002fscript_002fplayer_002fPlayer_003b */
    public static final C2491fm f521x7d549042 = null;
    /* renamed from: _f_getGameIO_0020_0028_0029Ltaikodom_002finfra_002fclient_002fIGameIO_003b */
    public static final C2491fm f522x3cb2b465 = null;
    /* renamed from: _f_getTaikodom_0020_0028_0029Ltaikodom_002fgame_002fscript_002fTaikodom_003b */
    public static final C2491fm f523xa236a942 = null;
    /* renamed from: _f_getUserConnection_0020_0028_0029Ltaikodom_002fgame_002fscript_002flogin_002fUserConnection_003b */
    public static final C2491fm f524x20176b18 = null;
    /* renamed from: _f_pushIfNotNull_0020_0028Ltaikodom_002finfra_002fscript_002fScriptObject_003b_0029V */
    public static final C2491fm f525xe7dd9d67 = null;
    public static final C2491fm _f_simulationTime_0020_0028_0029D = null;
    public static final int _m_fieldCount = 0;
    public static final C5663aRz[] _m_fields = null;
    public static final int _m_methodCount = 0;
    public static final C2491fm[] _m_methods = null;
    public static final long serialVersionUID = 0;
    private static final Log logger = LogPrinter.setClass(TaikodomObject.class);
    public static C6494any ___iScriptClass;

    static {
        m3124V();
    }

    public TaikodomObject() {
        super((C5540aNg) null);
        super.mo10S();
    }

    public TaikodomObject(C5540aNg ang) {
        super(ang);
    }

    public TaikodomObject(C2961mJ mJVar) {
        super((C5540aNg) null);
        super.mo967a(mJVar);
    }

    /* renamed from: V */
    static void m3124V() {
        serialVersionUID = (long) 1;
        _m_fieldCount = aDJ._m_fieldCount + 0;
        _m_methodCount = aDJ._m_methodCount + 9;
        _m_fields = new C5663aRz[(aDJ._m_fieldCount + 0)];
        C1634Xv.m11725a((Object[]) aDJ._m_fields, (Object[]) _m_fields);
        int i = aDJ._m_methodCount;
        C2491fm[] fmVarArr = new C2491fm[(i + 9)];
        C2491fm a = C4105zY.m41624a(TaikodomObject.class, "eb005f32437234d217ff2810be9387b3", i);
        f525xe7dd9d67 = a;
        fmVarArr[i] = a;
        int i2 = i + 1;
        C2491fm a2 = C4105zY.m41624a(TaikodomObject.class, "b1bf753c98802c772c3e6df0ccca9e7c", i2);
        f519x75159c47 = a2;
        fmVarArr[i2] = a2;
        int i3 = i2 + 1;
        C2491fm a3 = C4105zY.m41624a(TaikodomObject.class, "78d0e52ef9eace6b1de4601c0adf2d63", i3);
        f518x11b2d208 = a3;
        fmVarArr[i3] = a3;
        int i4 = i3 + 1;
        C2491fm a4 = C4105zY.m41624a(TaikodomObject.class, "bb97aa49d7efee89a679fa997dc30d20", i4);
        f522x3cb2b465 = a4;
        fmVarArr[i4] = a4;
        int i5 = i4 + 1;
        C2491fm a5 = C4105zY.m41624a(TaikodomObject.class, "9988868d21258a01ab2fc5378aeae1b2", i5);
        f521x7d549042 = a5;
        fmVarArr[i5] = a5;
        int i6 = i5 + 1;
        C2491fm a6 = C4105zY.m41624a(TaikodomObject.class, "4b5f5441b21f6e38f3e4d03573677039", i6);
        f520x97abc781 = a6;
        fmVarArr[i6] = a6;
        int i7 = i6 + 1;
        C2491fm a7 = C4105zY.m41624a(TaikodomObject.class, "12d0ed888908a6ece8694098f0e1bbfd", i7);
        f523xa236a942 = a7;
        fmVarArr[i7] = a7;
        int i8 = i7 + 1;
        C2491fm a8 = C4105zY.m41624a(TaikodomObject.class, "b8b2817f2229440825b7ac5f85ee9b2c", i8);
        f524x20176b18 = a8;
        fmVarArr[i8] = a8;
        int i9 = i8 + 1;
        C2491fm a9 = C4105zY.m41624a(TaikodomObject.class, "f29ee6eeadfb6126a6decf3372979bff", i9);
        _f_simulationTime_0020_0028_0029D = a9;
        fmVarArr[i9] = a9;
        int i10 = i9 + 1;
        _m_methods = fmVarArr;
        C1634Xv.m11725a((Object[]) aDJ._m_methods, (Object[]) _m_methods);
        ___iScriptClass = aUO.m18566a(TaikodomObject.class, C3805vD.class, _m_fields, _m_methods);
    }

    @C0064Am(aul = "9988868d21258a01ab2fc5378aeae1b2", aum = 0)
    @C5566aOg
    private Player aPz() {
        throw new aWi(new aCE(this, f521x7d549042, new Object[0]));
    }

    /* renamed from: T */
    public C6494any mo11T() {
        return ___iScriptClass;
    }

    /* renamed from: U */
    public C2491fm[] mo12U() {
        return _m_methods;
    }

    /* renamed from: W */
    public Object mo13W() {
        return new C3805vD(this);
    }

    /* renamed from: a */
    public Object mo14a(C0495Gr gr) {
        Object[] args = gr.getArgs();
        switch (gr.mo2417hq() - aDJ._m_methodCount) {
            case 0:
                m3127i((aDJ) args[0]);
                return null;
            case 1:
                m3125c((C5878acG) args[0]);
                return null;
            case 2:
                m3126d((C5878acG) args[0]);
                return null;
            case 3:
                return aMu();
            case 4:
                return aPz();
            case 5:
                return aPB();
            case 6:
                return aPD();
            case 7:
                return aMk();
            case 8:
                return new Double(aPE());
            default:
                return super.mo14a(gr);
        }
    }

    @C0401Fa
    /* renamed from: a */
    public void mo2066a(C5878acG acg) {
        switch (bFf().mo6893i(f519x75159c47)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f519x75159c47, new Object[]{acg}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f519x75159c47, new Object[]{acg}));
                break;
        }
        m3125c(acg);
    }

    /* renamed from: a */
    public void mo15a(Collection collection, C0495Gr gr) {
        super.mo15a(collection, gr);
    }

    public UserConnection getUserConnection() {
        switch (bFf().mo6893i(f524x20176b18)) {
            case 0:
                return null;
            case 2:
                return (UserConnection) bFf().mo5606d(new aCE(this, f524x20176b18, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f524x20176b18, new Object[0]));
                break;
        }
        return aMk();
    }

    @C5566aOg
    public Player aPA() {
        switch (bFf().mo6893i(f521x7d549042)) {
            case 0:
                return null;
            case 2:
                return (Player) bFf().mo5606d(new aCE(this, f521x7d549042, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f521x7d549042, new Object[0]));
                break;
        }
        return aPz();
    }

    @ClientOnly
    public Player getPlayer() {
        switch (bFf().mo6893i(f520x97abc781)) {
            case 0:
                return null;
            case 2:
                return (Player) bFf().mo5606d(new aCE(this, f520x97abc781, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f520x97abc781, new Object[0]));
                break;
        }
        return aPB();
    }

    public double aPF() {
        switch (bFf().mo6893i(_f_simulationTime_0020_0028_0029D)) {
            case 0:
                return ScriptRuntime.NaN;
            case 2:
                return ((Double) bFf().mo5606d(new aCE(this, _f_simulationTime_0020_0028_0029D, new Object[0]))).doubleValue();
            case 3:
                bFf().mo5606d(new aCE(this, _f_simulationTime_0020_0028_0029D, new Object[0]));
                break;
        }
        return aPE();
    }

    public Taikodom ala() {
        switch (bFf().mo6893i(f523xa236a942)) {
            case 0:
                return null;
            case 2:
                return (Taikodom) bFf().mo5606d(new aCE(this, f523xa236a942, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f523xa236a942, new Object[0]));
                break;
        }
        return aPD();
    }

    public IEngineGame ald() {
        switch (bFf().mo6893i(f522x3cb2b465)) {
            case 0:
                return null;
            case 2:
                return (IEngineGame) bFf().mo5606d(new aCE(this, f522x3cb2b465, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f522x3cb2b465, new Object[0]));
                break;
        }
        return aMu();
    }

    @C0401Fa
    /* renamed from: b */
    public void mo2071b(C5878acG acg) {
        switch (bFf().mo6893i(f518x11b2d208)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f518x11b2d208, new Object[]{acg}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f518x11b2d208, new Object[]{acg}));
                break;
        }
        m3126d(acg);
    }

    /* renamed from: c */
    public C5663aRz[] mo25c() {
        return _m_fields;
    }

    /* renamed from: j */
    public void mo2072j(aDJ adj) {
        switch (bFf().mo6893i(f525xe7dd9d67)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f525xe7dd9d67, new Object[]{adj}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f525xe7dd9d67, new Object[]{adj}));
                break;
        }
        m3127i(adj);
    }

    /* renamed from: S */
    public void mo10S() {
        super.mo10S();
    }

    /* renamed from: a */
    public void mo967a(C2961mJ mJVar) {
        super.mo967a(mJVar);
    }

    @C0064Am(aul = "eb005f32437234d217ff2810be9387b3", aum = 0)
    /* renamed from: i */
    private void m3127i(aDJ adj) {
        if (adj != null) {
            adj.push();
        }
    }

    @C0401Fa
    @C0064Am(aul = "b1bf753c98802c772c3e6df0ccca9e7c", aum = 0)
    /* renamed from: c */
    private void m3125c(C5878acG acg) {
        ald().bgX().mo4098a(acg);
    }

    @C0401Fa
    @C0064Am(aul = "78d0e52ef9eace6b1de4601c0adf2d63", aum = 0)
    /* renamed from: d */
    private void m3126d(C5878acG acg) {
        ald().bgX().mo4098a(acg);
    }

    @C0064Am(aul = "bb97aa49d7efee89a679fa997dc30d20", aum = 0)
    private IEngineGame aMu() {
        return ala().ald();
    }

    @C0064Am(aul = "4b5f5441b21f6e38f3e4d03573677039", aum = 0)
    @ClientOnly
    private Player aPB() {
        if (bGX()) {
            return ala().getUserConnection().mo15388dL();
        }
        throw new RuntimeException("You can only get the currentPlayer at client side, on server, call getCallOriginPlayer()");
    }

    @C0064Am(aul = "12d0ed888908a6ece8694098f0e1bbfd", aum = 0)
    private Taikodom aPD() {
        return (Taikodom) bFf().mo6866PM().bGz().bFV();
    }

    @C0064Am(aul = "b8b2817f2229440825b7ac5f85ee9b2c", aum = 0)
    private UserConnection aMk() {
        return ala().getUserConnection();
    }

    @C0064Am(aul = "f29ee6eeadfb6126a6decf3372979bff", aum = 0)
    private double aPE() {
        return ((double) currentTimeMillis()) * 0.001d;
    }
}
