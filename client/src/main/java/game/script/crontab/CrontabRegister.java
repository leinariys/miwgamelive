package game.script.crontab;

import game.network.message.externalizable.aCE;
import logic.baa.*;
import logic.data.mbean.C6830auW;
import logic.res.code.C5640aRc;
import logic.res.code.C5663aRz;
import logic.res.code.C6494any;
import logic.res.html.C2491fm;
import p001a.*;

import java.util.Collection;

@C5511aMd
@C6485anp
/* renamed from: a.TF */
/* compiled from: a */
public class CrontabRegister extends aDJ implements C1616Xf {
    public static final int _m_fieldCount = 0;
    public static final C5663aRz[] _m_fields = null;
    public static final int _m_methodCount = 0;
    public static final C2491fm[] _m_methods = null;
    /* renamed from: bM */
    public static final C5663aRz f1693bM = null;
    /* renamed from: bP */
    public static final C2491fm f1694bP = null;
    /* renamed from: bQ */
    public static final C2491fm f1695bQ = null;
    public static final C5663aRz ekc = null;
    public static final C5663aRz eke = null;
    public static final C5663aRz ekg = null;
    public static final C5663aRz eki = null;
    public static final C5663aRz ekk = null;
    public static final C2491fm ekl = null;
    public static final C2491fm ekm = null;
    public static final C2491fm ekn = null;
    public static final C2491fm eko = null;
    public static final C2491fm ekp = null;
    public static final C2491fm ekq = null;
    public static final C2491fm ekr = null;
    public static final C2491fm eks = null;
    public static final C2491fm ekt = null;
    public static final C2491fm eku = null;
    public static final C2491fm ekv = null;
    public static final C2491fm ekw = null;
    public static final C2491fm ekx = null;
    public static final long serialVersionUID = 0;
    public static C6494any ___iScriptClass;
    @C0064Am(aul = "3e43535b4b2fe65f4ae2800994e6e739", aum = 0)
    private static Integer ekb;
    @C0064Am(aul = "d968413caec1634849f861351b237efa", aum = 1)
    private static Integer ekd;
    @C0064Am(aul = "34f4c299d77af1c9561ac7035aacaecb", aum = 2)
    private static Integer ekf;
    @C0064Am(aul = "7dbf4f4509d3521503873891cfb3cdbb", aum = 3)
    private static Integer ekh;
    @C0064Am(aul = "bcd00800a591a827d12aa24345f1de49", aum = 4)
    private static boolean[] ekj;
    @C0064Am(aul = "56fd1f758f1126a1f91f107880fbfc47", aum = 5)
    private static String handle;

    static {
        m9851V();
    }

    public CrontabRegister() {
        super((C5540aNg) null);
        super.mo10S();
    }

    public CrontabRegister(C5540aNg ang) {
        super(ang);
    }

    /* renamed from: V */
    static void m9851V() {
        serialVersionUID = (long) 1;
        _m_fieldCount = aDJ._m_fieldCount + 6;
        _m_methodCount = aDJ._m_methodCount + 15;
        int i = aDJ._m_fieldCount;
        C5663aRz[] arzArr = new C5663aRz[(i + 6)];
        C5663aRz b = C5640aRc.m17844b(CrontabRegister.class, "3e43535b4b2fe65f4ae2800994e6e739", i);
        ekc = b;
        arzArr[i] = b;
        int i2 = i + 1;
        C5663aRz b2 = C5640aRc.m17844b(CrontabRegister.class, "d968413caec1634849f861351b237efa", i2);
        eke = b2;
        arzArr[i2] = b2;
        int i3 = i2 + 1;
        C5663aRz b3 = C5640aRc.m17844b(CrontabRegister.class, "34f4c299d77af1c9561ac7035aacaecb", i3);
        ekg = b3;
        arzArr[i3] = b3;
        int i4 = i3 + 1;
        C5663aRz b4 = C5640aRc.m17844b(CrontabRegister.class, "7dbf4f4509d3521503873891cfb3cdbb", i4);
        eki = b4;
        arzArr[i4] = b4;
        int i5 = i4 + 1;
        C5663aRz b5 = C5640aRc.m17844b(CrontabRegister.class, "bcd00800a591a827d12aa24345f1de49", i5);
        ekk = b5;
        arzArr[i5] = b5;
        int i6 = i5 + 1;
        C5663aRz b6 = C5640aRc.m17844b(CrontabRegister.class, "56fd1f758f1126a1f91f107880fbfc47", i6);
        f1693bM = b6;
        arzArr[i6] = b6;
        int i7 = i6 + 1;
        _m_fields = arzArr;
        C1634Xv.m11725a((Object[]) aDJ._m_fields, (Object[]) _m_fields);
        int i8 = aDJ._m_methodCount;
        C2491fm[] fmVarArr = new C2491fm[(i8 + 15)];
        C2491fm a = C4105zY.m41624a(CrontabRegister.class, "cdeb04b163ee6dcc1d1a847d44109ad0", i8);
        ekl = a;
        fmVarArr[i8] = a;
        int i9 = i8 + 1;
        C2491fm a2 = C4105zY.m41624a(CrontabRegister.class, "0122647a0ff70f046982e866628edc85", i9);
        ekm = a2;
        fmVarArr[i9] = a2;
        int i10 = i9 + 1;
        C2491fm a3 = C4105zY.m41624a(CrontabRegister.class, "60b470d9324a9c9cefce064e6398d466", i10);
        ekn = a3;
        fmVarArr[i10] = a3;
        int i11 = i10 + 1;
        C2491fm a4 = C4105zY.m41624a(CrontabRegister.class, "3eaf65b08cd01cb0fd2f795e199a4203", i11);
        eko = a4;
        fmVarArr[i11] = a4;
        int i12 = i11 + 1;
        C2491fm a5 = C4105zY.m41624a(CrontabRegister.class, "c1b51264694991203f32f6d2d45c33cd", i12);
        ekp = a5;
        fmVarArr[i12] = a5;
        int i13 = i12 + 1;
        C2491fm a6 = C4105zY.m41624a(CrontabRegister.class, "66de1bc72c78f52e0dc8d49d7ec1fad4", i13);
        ekq = a6;
        fmVarArr[i13] = a6;
        int i14 = i13 + 1;
        C2491fm a7 = C4105zY.m41624a(CrontabRegister.class, "87957ab6e3ede754d62b88384bfaf400", i14);
        ekr = a7;
        fmVarArr[i14] = a7;
        int i15 = i14 + 1;
        C2491fm a8 = C4105zY.m41624a(CrontabRegister.class, "81fb211613c9cd97bc635deeb522d9e0", i15);
        eks = a8;
        fmVarArr[i15] = a8;
        int i16 = i15 + 1;
        C2491fm a9 = C4105zY.m41624a(CrontabRegister.class, "cad3457f019344071c388b59c6dcc08a", i16);
        ekt = a9;
        fmVarArr[i16] = a9;
        int i17 = i16 + 1;
        C2491fm a10 = C4105zY.m41624a(CrontabRegister.class, "3ca4b7ee16bc7b5fd6a8c56738b77aaf", i17);
        eku = a10;
        fmVarArr[i17] = a10;
        int i18 = i17 + 1;
        C2491fm a11 = C4105zY.m41624a(CrontabRegister.class, "831fcac811593fe8748e5715b26d81a7", i18);
        ekv = a11;
        fmVarArr[i18] = a11;
        int i19 = i18 + 1;
        C2491fm a12 = C4105zY.m41624a(CrontabRegister.class, "fe1bb5908ef9aca6355f84b5bfde8ef4", i19);
        ekw = a12;
        fmVarArr[i19] = a12;
        int i20 = i19 + 1;
        C2491fm a13 = C4105zY.m41624a(CrontabRegister.class, "b66fd20432cff4ef2c048764f04ef99b", i20);
        f1694bP = a13;
        fmVarArr[i20] = a13;
        int i21 = i20 + 1;
        C2491fm a14 = C4105zY.m41624a(CrontabRegister.class, "d8c301a8be2ffd3b3c7613cc1826290a", i21);
        f1695bQ = a14;
        fmVarArr[i21] = a14;
        int i22 = i21 + 1;
        C2491fm a15 = C4105zY.m41624a(CrontabRegister.class, "bb9343bb81b16ec7010316b8ad9de1c1", i22);
        ekx = a15;
        fmVarArr[i22] = a15;
        int i23 = i22 + 1;
        _m_methods = fmVarArr;
        C1634Xv.m11725a((Object[]) aDJ._m_methods, (Object[]) _m_methods);
        ___iScriptClass = aUO.m18566a(CrontabRegister.class, C6830auW.class, _m_fields, _m_methods);
    }

    /* renamed from: a */
    private void m9852a(String str) {
        bFf().mo5608dq().mo3197f(f1693bM, str);
    }

    /* renamed from: a */
    private void m9853a(boolean[] zArr) {
        bFf().mo5608dq().mo3197f(ekk, zArr);
    }

    /* renamed from: ao */
    private String m9854ao() {
        return (String) bFf().mo5608dq().mo3214p(f1693bM);
    }

    private Integer bvJ() {
        return (Integer) bFf().mo5608dq().mo3214p(ekc);
    }

    private Integer bvK() {
        return (Integer) bFf().mo5608dq().mo3214p(eke);
    }

    private Integer bvL() {
        return (Integer) bFf().mo5608dq().mo3214p(ekg);
    }

    private Integer bvM() {
        return (Integer) bFf().mo5608dq().mo3214p(eki);
    }

    private boolean[] bvN() {
        return (boolean[]) bFf().mo5608dq().mo3214p(ekk);
    }

    /* renamed from: i */
    private void m9859i(Integer num) {
        bFf().mo5608dq().mo3197f(ekc, num);
    }

    /* renamed from: j */
    private void m9860j(Integer num) {
        bFf().mo5608dq().mo3197f(eke, num);
    }

    /* renamed from: k */
    private void m9861k(Integer num) {
        bFf().mo5608dq().mo3197f(ekg, num);
    }

    /* renamed from: l */
    private void m9862l(Integer num) {
        bFf().mo5608dq().mo3197f(eki, num);
    }

    /* renamed from: T */
    public C6494any mo11T() {
        return ___iScriptClass;
    }

    /* renamed from: U */
    public C2491fm[] mo12U() {
        return _m_methods;
    }

    /* renamed from: W */
    public Object mo13W() {
        return new C6830auW(this);
    }

    /* renamed from: a */
    public Object mo14a(C0495Gr gr) {
        Object[] args = gr.getArgs();
        switch (gr.mo2417hq() - aDJ._m_methodCount) {
            case 0:
                return bvO();
            case 1:
                m9863m((Integer) args[0]);
                return null;
            case 2:
                return bvQ();
            case 3:
                m9865o((Integer) args[0]);
                return null;
            case 4:
                return bvS();
            case 5:
                m9866q((Integer) args[0]);
                return null;
            case 6:
                return bvU();
            case 7:
                m9867s((Integer) args[0]);
                return null;
            case 8:
                return bvW();
            case 9:
                m9858b((boolean[]) args[0]);
                return null;
            case 10:
                return new Boolean(m9864nt(((Integer) args[0]).intValue()));
            case 11:
                m9856b(((Integer) args[0]).intValue(), ((Boolean) args[1]).booleanValue());
                return null;
            case 12:
                return m9855ar();
            case 13:
                m9857b((String) args[0]);
                return null;
            case 14:
                return new Boolean(bvY());
            default:
                return super.mo14a(gr);
        }
    }

    /* renamed from: a */
    public void mo15a(Collection collection, C0495Gr gr) {
        super.mo15a(collection, gr);
    }

    public Integer bvP() {
        switch (bFf().mo6893i(ekl)) {
            case 0:
                return null;
            case 2:
                return (Integer) bFf().mo5606d(new aCE(this, ekl, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, ekl, new Object[0]));
                break;
        }
        return bvO();
    }

    public Integer bvR() {
        switch (bFf().mo6893i(ekn)) {
            case 0:
                return null;
            case 2:
                return (Integer) bFf().mo5606d(new aCE(this, ekn, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, ekn, new Object[0]));
                break;
        }
        return bvQ();
    }

    public Integer bvT() {
        switch (bFf().mo6893i(ekp)) {
            case 0:
                return null;
            case 2:
                return (Integer) bFf().mo5606d(new aCE(this, ekp, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, ekp, new Object[0]));
                break;
        }
        return bvS();
    }

    public Integer bvV() {
        switch (bFf().mo6893i(ekr)) {
            case 0:
                return null;
            case 2:
                return (Integer) bFf().mo5606d(new aCE(this, ekr, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, ekr, new Object[0]));
                break;
        }
        return bvU();
    }

    public boolean[] bvX() {
        switch (bFf().mo6893i(ekt)) {
            case 0:
                return null;
            case 2:
                return (boolean[]) bFf().mo5606d(new aCE(this, ekt, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, ekt, new Object[0]));
                break;
        }
        return bvW();
    }

    public boolean bvZ() {
        switch (bFf().mo6893i(ekx)) {
            case 0:
                return false;
            case 2:
                return ((Boolean) bFf().mo5606d(new aCE(this, ekx, new Object[0]))).booleanValue();
            case 3:
                bFf().mo5606d(new aCE(this, ekx, new Object[0]));
                break;
        }
        return bvY();
    }

    /* renamed from: c */
    public void mo5622c(int i, boolean z) {
        switch (bFf().mo6893i(ekw)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, ekw, new Object[]{new Integer(i), new Boolean(z)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, ekw, new Object[]{new Integer(i), new Boolean(z)}));
                break;
        }
        m9856b(i, z);
    }

    /* renamed from: c */
    public void mo5623c(boolean[] zArr) {
        switch (bFf().mo6893i(eku)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, eku, new Object[]{zArr}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, eku, new Object[]{zArr}));
                break;
        }
        m9858b(zArr);
    }

    /* renamed from: c */
    public C5663aRz[] mo25c() {
        return _m_fields;
    }

    public String getHandle() {
        switch (bFf().mo6893i(f1694bP)) {
            case 0:
                return null;
            case 2:
                return (String) bFf().mo5606d(new aCE(this, f1694bP, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f1694bP, new Object[0]));
                break;
        }
        return m9855ar();
    }

    public void setHandle(String str) {
        switch (bFf().mo6893i(f1695bQ)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f1695bQ, new Object[]{str}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f1695bQ, new Object[]{str}));
                break;
        }
        m9857b(str);
    }

    /* renamed from: n */
    public void mo5625n(Integer num) {
        switch (bFf().mo6893i(ekm)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, ekm, new Object[]{num}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, ekm, new Object[]{num}));
                break;
        }
        m9863m(num);
    }

    /* renamed from: nu */
    public boolean mo5626nu(int i) {
        switch (bFf().mo6893i(ekv)) {
            case 0:
                return false;
            case 2:
                return ((Boolean) bFf().mo5606d(new aCE(this, ekv, new Object[]{new Integer(i)}))).booleanValue();
            case 3:
                bFf().mo5606d(new aCE(this, ekv, new Object[]{new Integer(i)}));
                break;
        }
        return m9864nt(i);
    }

    /* renamed from: p */
    public void mo5627p(Integer num) {
        switch (bFf().mo6893i(eko)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, eko, new Object[]{num}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, eko, new Object[]{num}));
                break;
        }
        m9865o(num);
    }

    /* renamed from: r */
    public void mo5628r(Integer num) {
        switch (bFf().mo6893i(ekq)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, ekq, new Object[]{num}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, ekq, new Object[]{num}));
                break;
        }
        m9866q(num);
    }

    /* renamed from: t */
    public void mo5630t(Integer num) {
        switch (bFf().mo6893i(eks)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, eks, new Object[]{num}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, eks, new Object[]{num}));
                break;
        }
        m9867s(num);
    }

    /* renamed from: S */
    public void mo10S() {
        super.mo10S();
        m9853a(new boolean[7]);
    }

    @C0064Am(aul = "cdeb04b163ee6dcc1d1a847d44109ad0", aum = 0)
    private Integer bvO() {
        return bvK();
    }

    @C0064Am(aul = "0122647a0ff70f046982e866628edc85", aum = 0)
    /* renamed from: m */
    private void m9863m(Integer num) {
        m9860j(num);
    }

    @C0064Am(aul = "60b470d9324a9c9cefce064e6398d466", aum = 0)
    private Integer bvQ() {
        return bvJ();
    }

    @C0064Am(aul = "3eaf65b08cd01cb0fd2f795e199a4203", aum = 0)
    /* renamed from: o */
    private void m9865o(Integer num) {
        m9859i(num);
    }

    @C0064Am(aul = "c1b51264694991203f32f6d2d45c33cd", aum = 0)
    private Integer bvS() {
        return bvM();
    }

    @C0064Am(aul = "66de1bc72c78f52e0dc8d49d7ec1fad4", aum = 0)
    /* renamed from: q */
    private void m9866q(Integer num) {
        m9862l(num);
    }

    @C0064Am(aul = "87957ab6e3ede754d62b88384bfaf400", aum = 0)
    private Integer bvU() {
        return bvL();
    }

    @C0064Am(aul = "81fb211613c9cd97bc635deeb522d9e0", aum = 0)
    /* renamed from: s */
    private void m9867s(Integer num) {
        m9861k(num);
    }

    @C0064Am(aul = "cad3457f019344071c388b59c6dcc08a", aum = 0)
    private boolean[] bvW() {
        return bvN();
    }

    @C0064Am(aul = "3ca4b7ee16bc7b5fd6a8c56738b77aaf", aum = 0)
    /* renamed from: b */
    private void m9858b(boolean[] zArr) {
        m9853a(zArr);
    }

    @C0064Am(aul = "831fcac811593fe8748e5715b26d81a7", aum = 0)
    /* renamed from: nt */
    private boolean m9864nt(int i) {
        return bvN()[i];
    }

    @C0064Am(aul = "fe1bb5908ef9aca6355f84b5bfde8ef4", aum = 0)
    /* renamed from: b */
    private void m9856b(int i, boolean z) {
        bvN()[i] = z;
    }

    @C0064Am(aul = "b66fd20432cff4ef2c048764f04ef99b", aum = 0)
    /* renamed from: ar */
    private String m9855ar() {
        return m9854ao();
    }

    @C0064Am(aul = "d8c301a8be2ffd3b3c7613cc1826290a", aum = 0)
    /* renamed from: b */
    private void m9857b(String str) {
        m9852a(str);
    }

    @C0064Am(aul = "bb9343bb81b16ec7010316b8ad9de1c1", aum = 0)
    private boolean bvY() {
        return false;
    }
}
