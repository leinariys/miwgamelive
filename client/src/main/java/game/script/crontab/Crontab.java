package game.script.crontab;

import game.network.message.externalizable.aCE;
import game.network.message.serializable.C1556Wo;
import logic.baa.*;
import logic.data.mbean.aWe;
import logic.res.code.C5640aRc;
import logic.res.code.C5663aRz;
import logic.res.code.C6494any;
import logic.res.html.C2491fm;
import logic.thred.ThreadWrapper;
import p001a.*;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

@C5566aOg
@C5511aMd
@C6485anp
/* renamed from: a.bP */
/* compiled from: a */
public class Crontab extends aDJ implements C1616Xf {
    public static final C2491fm _f_onResurrect_0020_0028_0029V = null;
    public static final int _m_fieldCount = 0;
    public static final C5663aRz[] _m_fields = null;
    public static final int _m_methodCount = 0;
    public static final C2491fm[] _m_methods = null;
    /* renamed from: pm */
    public static final C5663aRz f5777pm = null;
    /* renamed from: po */
    public static final C2491fm f5778po = null;
    /* renamed from: pp */
    public static final C2491fm f5779pp = null;
    /* renamed from: pq */
    public static final C2491fm f5780pq = null;
    /* renamed from: pr */
    public static final C2491fm f5781pr = null;
    public static final long serialVersionUID = 0;
    public static C6494any ___iScriptClass;
    @C0064Am(aul = "b109554772e8d287c4595798519d4f06", aum = 0)

    /* renamed from: pl */
    private static C1556Wo<C5832abM, List<CrontabRegister>> f5776pl;

    static {
        m27823V();
    }

    /* renamed from: pn */
    private transient List<CrontabEvent> f5782pn;

    public Crontab() {
        super((C5540aNg) null);
        super.mo10S();
    }

    public Crontab(C5540aNg ang) {
        super(ang);
    }

    /* renamed from: V */
    static void m27823V() {
        serialVersionUID = (long) 1;
        _m_fieldCount = aDJ._m_fieldCount + 1;
        _m_methodCount = aDJ._m_methodCount + 5;
        int i = aDJ._m_fieldCount;
        C5663aRz[] arzArr = new C5663aRz[(i + 1)];
        C5663aRz b = C5640aRc.m17844b(Crontab.class, "b109554772e8d287c4595798519d4f06", i);
        f5777pm = b;
        arzArr[i] = b;
        int i2 = i + 1;
        _m_fields = arzArr;
        C1634Xv.m11725a((Object[]) aDJ._m_fields, (Object[]) _m_fields);
        int i3 = aDJ._m_methodCount;
        C2491fm[] fmVarArr = new C2491fm[(i3 + 5)];
        C2491fm a = C4105zY.m41624a(Crontab.class, "33e9f4127b9d716ff9d4b5e2174392e1", i3);
        f5778po = a;
        fmVarArr[i3] = a;
        int i4 = i3 + 1;
        C2491fm a2 = C4105zY.m41624a(Crontab.class, "b2f21938e7dbae2c0546b32453ae5d51", i4);
        f5779pp = a2;
        fmVarArr[i4] = a2;
        int i5 = i4 + 1;
        C2491fm a3 = C4105zY.m41624a(Crontab.class, "0354c2554e8497063f002f32499b7db6", i5);
        f5780pq = a3;
        fmVarArr[i5] = a3;
        int i6 = i5 + 1;
        C2491fm a4 = C4105zY.m41624a(Crontab.class, "513a5f9dbbd34b86e226fb66d0280401", i6);
        f5781pr = a4;
        fmVarArr[i6] = a4;
        int i7 = i6 + 1;
        C2491fm a5 = C4105zY.m41624a(Crontab.class, "0c35b97a38d6ca17ec71033a0c08c84c", i7);
        _f_onResurrect_0020_0028_0029V = a5;
        fmVarArr[i7] = a5;
        int i8 = i7 + 1;
        _m_methods = fmVarArr;
        C1634Xv.m11725a((Object[]) aDJ._m_methods, (Object[]) _m_methods);
        ___iScriptClass = aUO.m18566a(Crontab.class, aWe.class, _m_fields, _m_methods);
    }

    /* renamed from: a */
    private void m27825a(C1556Wo wo) {
        bFf().mo5608dq().mo3197f(f5777pm, wo);
    }

    /* renamed from: gV */
    private C1556Wo m27829gV() {
        return (C1556Wo) bFf().mo5608dq().mo3214p(f5777pm);
    }

    /* renamed from: gX */
    private void m27831gX() {
        switch (bFf().mo6893i(f5778po)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f5778po, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f5778po, new Object[0]));
                break;
        }
        m27830gW();
    }

    /* renamed from: T */
    public C6494any mo11T() {
        return ___iScriptClass;
    }

    /* renamed from: U */
    public C2491fm[] mo12U() {
        return _m_methods;
    }

    /* renamed from: W */
    public Object mo13W() {
        return new aWe(this);
    }

    /* renamed from: a */
    public Object mo14a(C0495Gr gr) {
        Object[] args = gr.getArgs();
        switch (gr.mo2417hq() - aDJ._m_methodCount) {
            case 0:
                m27830gW();
                return null;
            case 1:
                m27832gY();
                return null;
            case 2:
                m27826a((C5832abM) args[0], (CrontabRegister) args[1]);
                return null;
            case 3:
                return m27824a((C5832abM) args[0]);
            case 4:
                m27828aG();
                return null;
            default:
                return super.mo14a(gr);
        }
    }

    /* renamed from: a */
    public void mo15a(Collection collection, C0495Gr gr) {
        super.mo15a(collection, gr);
    }

    /* renamed from: aH */
    public void mo70aH() {
        switch (bFf().mo6893i(_f_onResurrect_0020_0028_0029V)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, _f_onResurrect_0020_0028_0029V, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, _f_onResurrect_0020_0028_0029V, new Object[0]));
                break;
        }
        m27828aG();
    }

    /* renamed from: b */
    public List<CrontabRegister> mo17292b(C5832abM abm) {
        switch (bFf().mo6893i(f5781pr)) {
            case 0:
                return null;
            case 2:
                return (List) bFf().mo5606d(new aCE(this, f5781pr, new Object[]{abm}));
            case 3:
                bFf().mo5606d(new aCE(this, f5781pr, new Object[]{abm}));
                break;
        }
        return m27824a(abm);
    }

    /* renamed from: b */
    public void mo17293b(C5832abM abm, CrontabRegister tf) {
        switch (bFf().mo6893i(f5780pq)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f5780pq, new Object[]{abm, tf}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f5780pq, new Object[]{abm, tf}));
                break;
        }
        m27826a(abm, tf);
    }

    /* renamed from: c */
    public C5663aRz[] mo25c() {
        return _m_fields;
    }

    /* access modifiers changed from: protected */
    /* renamed from: gZ */
    public void mo17294gZ() {
        switch (bFf().mo6893i(f5779pp)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f5779pp, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f5779pp, new Object[0]));
                break;
        }
        m27832gY();
    }

    /* renamed from: S */
    public void mo10S() {
        super.mo10S();
        this.f5782pn = new CopyOnWriteArrayList();
        System.out.println("-----> E nasce um crontab!");
        m27831gX();
    }

    @C0064Am(aul = "33e9f4127b9d716ff9d4b5e2174392e1", aum = 0)
    /* renamed from: gW */
    private void m27830gW() {
        C2038a aVar = new C2038a("Crontab Very Own Thread");
        aVar.setDaemon(true);
        aVar.start();
    }

    @C0064Am(aul = "b2f21938e7dbae2c0546b32453ae5d51", aum = 0)
    /* renamed from: gY */
    private void m27832gY() {
    }

    @C0064Am(aul = "0354c2554e8497063f002f32499b7db6", aum = 0)
    /* renamed from: a */
    private void m27826a(C5832abM abm, CrontabRegister tf) {
        List list = (List) m27829gV().get(abm);
        if (list == null) {
            list = new ArrayList();
            m27829gV().put(abm, list);
        }
        list.add(tf);
    }

    @C0064Am(aul = "513a5f9dbbd34b86e226fb66d0280401", aum = 0)
    /* renamed from: a */
    private List<CrontabRegister> m27824a(C5832abM abm) {
        return Collections.unmodifiableList((List) m27829gV().get(abm));
    }

    @C0064Am(aul = "0c35b97a38d6ca17ec71033a0c08c84c", aum = 0)
    /* renamed from: aG */
    private void m27828aG() {
        super.mo70aH();
        System.out.println("-----> E REnasce um crontab!");
        m27831gX();
    }

    /* renamed from: a.bP$a */
    class C2038a extends ThreadWrapper {
        C2038a(String str) {
            super(str);
        }

        public void run() {
            while (true) {
                try {
                    Crontab.this.mo17294gZ();
                    Thread.sleep(60000);
                } catch (InterruptedException e) {
                    Crontab.this.mo8359ma("Crontab thread stopping");
                    return;
                } catch (Throwable th) {
                    Crontab.this.mo8359ma("Crontab thread stopping");
                    throw th;
                }
            }
        }
    }
}
