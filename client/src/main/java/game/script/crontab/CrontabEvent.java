package game.script.crontab;

import game.network.message.externalizable.aCE;
import logic.baa.*;
import logic.data.mbean.C2846ky;
import logic.res.code.C5640aRc;
import logic.res.code.C5663aRz;
import logic.res.code.C6494any;
import logic.res.html.C2491fm;
import p001a.*;

import java.util.Collection;

@C5566aOg
@C5511aMd
@C6485anp
/* renamed from: a.Rd */
/* compiled from: a */
public class CrontabEvent extends aDJ implements C1616Xf {
    public static final int _m_fieldCount = 0;
    public static final C5663aRz[] _m_fields = null;
    public static final int _m_methodCount = 0;
    public static final C2491fm[] _m_methods = null;
    /* renamed from: bM */
    public static final C5663aRz f1500bM = null;
    /* renamed from: bP */
    public static final C2491fm f1501bP = null;
    /* renamed from: bQ */
    public static final C2491fm f1502bQ = null;
    public static final C5663aRz cqr = null;
    public static final C5663aRz dXW = null;
    public static final C2491fm dXX = null;
    public static final C2491fm dXY = null;
    public static final C2491fm dXZ = null;
    public static final C2491fm dYa = null;
    public static final long serialVersionUID = 0;
    public static C6494any ___iScriptClass;
    @C0064Am(aul = "eeb012b36bd5d2632f9d2031e69bf508", aum = 0)
    private static long auv;
    @C0064Am(aul = "766f60179d3f8c1f89811244d2052bb3", aum = 1)
    private static C5832abM auw;
    @C0064Am(aul = "c79ae3304b7f1774e504960237eb6648", aum = 2)
    private static String handle;

    static {
        m9233V();
    }

    public CrontabEvent() {
        super((C5540aNg) null);
        super.mo10S();
    }

    public CrontabEvent(C5540aNg ang) {
        super(ang);
    }

    /* renamed from: V */
    static void m9233V() {
        serialVersionUID = (long) 1;
        _m_fieldCount = aDJ._m_fieldCount + 3;
        _m_methodCount = aDJ._m_methodCount + 6;
        int i = aDJ._m_fieldCount;
        C5663aRz[] arzArr = new C5663aRz[(i + 3)];
        C5663aRz b = C5640aRc.m17844b(CrontabEvent.class, "eeb012b36bd5d2632f9d2031e69bf508", i);
        dXW = b;
        arzArr[i] = b;
        int i2 = i + 1;
        C5663aRz b2 = C5640aRc.m17844b(CrontabEvent.class, "766f60179d3f8c1f89811244d2052bb3", i2);
        cqr = b2;
        arzArr[i2] = b2;
        int i3 = i2 + 1;
        C5663aRz b3 = C5640aRc.m17844b(CrontabEvent.class, "c79ae3304b7f1774e504960237eb6648", i3);
        f1500bM = b3;
        arzArr[i3] = b3;
        int i4 = i3 + 1;
        _m_fields = arzArr;
        C1634Xv.m11725a((Object[]) aDJ._m_fields, (Object[]) _m_fields);
        int i5 = aDJ._m_methodCount;
        C2491fm[] fmVarArr = new C2491fm[(i5 + 6)];
        C2491fm a = C4105zY.m41624a(CrontabEvent.class, "be4b441ef8d13e68f055721f06176a1d", i5);
        f1501bP = a;
        fmVarArr[i5] = a;
        int i6 = i5 + 1;
        C2491fm a2 = C4105zY.m41624a(CrontabEvent.class, "9862a8d25d917f00d4b8538e105420e4", i6);
        f1502bQ = a2;
        fmVarArr[i6] = a2;
        int i7 = i6 + 1;
        C2491fm a3 = C4105zY.m41624a(CrontabEvent.class, "d4e1d8dabf942e539558dfaf9e433bea", i7);
        dXX = a3;
        fmVarArr[i7] = a3;
        int i8 = i7 + 1;
        C2491fm a4 = C4105zY.m41624a(CrontabEvent.class, "de70f6161bbc9be6dacd870861eb67aa", i8);
        dXY = a4;
        fmVarArr[i8] = a4;
        int i9 = i8 + 1;
        C2491fm a5 = C4105zY.m41624a(CrontabEvent.class, "e16a6706934ccd2d45b6c37f35ccff5e", i9);
        dXZ = a5;
        fmVarArr[i9] = a5;
        int i10 = i9 + 1;
        C2491fm a6 = C4105zY.m41624a(CrontabEvent.class, "de5f61f42fd813c5a7e06dd7a0638ea7", i10);
        dYa = a6;
        fmVarArr[i10] = a6;
        int i11 = i10 + 1;
        _m_methods = fmVarArr;
        C1634Xv.m11725a((Object[]) aDJ._m_methods, (Object[]) _m_methods);
        ___iScriptClass = aUO.m18566a(CrontabEvent.class, C2846ky.class, _m_fields, _m_methods);
    }

    /* renamed from: a */
    private void m9234a(String str) {
        bFf().mo5608dq().mo3197f(f1500bM, str);
    }

    /* renamed from: ao */
    private String m9235ao() {
        return (String) bFf().mo5608dq().mo3214p(f1500bM);
    }

    private long brg() {
        return bFf().mo5608dq().mo3213o(dXW);
    }

    private C5832abM brh() {
        return (C5832abM) bFf().mo5608dq().mo3214p(cqr);
    }

    /* renamed from: c */
    private void m9238c(C5832abM abm) {
        bFf().mo5608dq().mo3197f(cqr, abm);
    }

    /* renamed from: fC */
    private void m9240fC(long j) {
        bFf().mo5608dq().mo3184b(dXW, j);
    }

    /* renamed from: T */
    public C6494any mo11T() {
        return ___iScriptClass;
    }

    /* renamed from: U */
    public C2491fm[] mo12U() {
        return _m_methods;
    }

    /* renamed from: W */
    public Object mo13W() {
        return new C2846ky(this);
    }

    /* renamed from: a */
    public Object mo14a(C0495Gr gr) {
        Object[] args = gr.getArgs();
        switch (gr.mo2417hq() - aDJ._m_methodCount) {
            case 0:
                return m9236ar();
            case 1:
                m9237b((String) args[0]);
                return null;
            case 2:
                return bri();
            case 3:
                m9239d((C5832abM) args[0]);
                return null;
            case 4:
                return new Long(brk());
            case 5:
                m9241fD(((Long) args[0]).longValue());
                return null;
            default:
                return super.mo14a(gr);
        }
    }

    /* renamed from: a */
    public void mo15a(Collection collection, C0495Gr gr) {
        super.mo15a(collection, gr);
    }

    public C5832abM brj() {
        switch (bFf().mo6893i(dXX)) {
            case 0:
                return null;
            case 2:
                return (C5832abM) bFf().mo5606d(new aCE(this, dXX, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, dXX, new Object[0]));
                break;
        }
        return bri();
    }

    public long brl() {
        switch (bFf().mo6893i(dXZ)) {
            case 0:
                return 0;
            case 2:
                return ((Long) bFf().mo5606d(new aCE(this, dXZ, new Object[0]))).longValue();
            case 3:
                bFf().mo5606d(new aCE(this, dXZ, new Object[0]));
                break;
        }
        return brk();
    }

    /* renamed from: c */
    public C5663aRz[] mo25c() {
        return _m_fields;
    }

    /* renamed from: e */
    public void mo5263e(C5832abM abm) {
        switch (bFf().mo6893i(dXY)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, dXY, new Object[]{abm}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, dXY, new Object[]{abm}));
                break;
        }
        m9239d(abm);
    }

    /* renamed from: fE */
    public void mo5264fE(long j) {
        switch (bFf().mo6893i(dYa)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, dYa, new Object[]{new Long(j)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, dYa, new Object[]{new Long(j)}));
                break;
        }
        m9241fD(j);
    }

    public String getHandle() {
        switch (bFf().mo6893i(f1501bP)) {
            case 0:
                return null;
            case 2:
                return (String) bFf().mo5606d(new aCE(this, f1501bP, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f1501bP, new Object[0]));
                break;
        }
        return m9236ar();
    }

    public void setHandle(String str) {
        switch (bFf().mo6893i(f1502bQ)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f1502bQ, new Object[]{str}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f1502bQ, new Object[]{str}));
                break;
        }
        m9237b(str);
    }

    /* renamed from: S */
    public void mo10S() {
        super.mo10S();
    }

    @C0064Am(aul = "be4b441ef8d13e68f055721f06176a1d", aum = 0)
    /* renamed from: ar */
    private String m9236ar() {
        return m9235ao();
    }

    @C0064Am(aul = "9862a8d25d917f00d4b8538e105420e4", aum = 0)
    /* renamed from: b */
    private void m9237b(String str) {
        m9234a(str);
    }

    @C0064Am(aul = "d4e1d8dabf942e539558dfaf9e433bea", aum = 0)
    private C5832abM bri() {
        return brh();
    }

    @C0064Am(aul = "de70f6161bbc9be6dacd870861eb67aa", aum = 0)
    /* renamed from: d */
    private void m9239d(C5832abM abm) {
        m9238c(abm);
    }

    @C0064Am(aul = "e16a6706934ccd2d45b6c37f35ccff5e", aum = 0)
    private long brk() {
        return brg();
    }

    @C0064Am(aul = "de5f61f42fd813c5a7e06dd7a0638ea7", aum = 0)
    /* renamed from: fD */
    private void m9241fD(long j) {
        m9240fC(j);
    }
}
