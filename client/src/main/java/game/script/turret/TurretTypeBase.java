package game.script.turret;

import game.CollisionFXSet;
import game.script.item.WeaponType;
import game.script.resource.Asset;
import game.script.ship.HullType;
import game.script.ship.ShieldType;
import game.script.template.BaseTaikodomContent;
import taikodom.infra.script.I18NString;

import java.util.Collection;

@C0566Hp
@C5511aMd
@C6485anp
/* renamed from: a.anK  reason: case insensitive filesystem */
/* compiled from: a */
public abstract class TurretTypeBase extends BaseTaikodomContent implements C1616Xf {

    /* renamed from: AG */
    public static final C5663aRz f4937AG = null;
    /* renamed from: LR */
    public static final C5663aRz f4939LR = null;
    /* renamed from: VW */
    public static final C5663aRz f4940VW = null;
    /* renamed from: Wc */
    public static final C5663aRz f4942Wc = null;
    /* renamed from: We */
    public static final C5663aRz f4944We = null;
    public static final int _m_fieldCount = 0;
    public static final C5663aRz[] _m_fields = null;
    public static final int _m_methodCount = 0;
    public static final C2491fm[] _m_methods = null;
    public static final C2491fm aDR = null;
    public static final C2491fm aDk = null;
    public static final C2491fm aDl = null;
    public static final C2491fm aTf = null;
    public static final C2491fm aTr = null;
    public static final C2491fm aTt = null;
    public static final C5663aRz bnF = null;
    public static final C5663aRz bnP = null;
    public static final C5663aRz bod = null;
    public static final C2491fm dGB = null;
    public static final C2491fm dGK = null;
    public static final C2491fm dGL = null;
    public static final C2491fm dGQ = null;
    public static final C2491fm dGR = null;
    public static final C2491fm dGS = null;
    public static final C2491fm dGT = null;
    public static final C2491fm dHh = null;
    public static final C2491fm dHi = null;
    public static final C2491fm dHz = null;
    /* renamed from: dN */
    public static final C2491fm f4945dN = null;
    public static final C5663aRz dlV = null;
    public static final C5663aRz dlW = null;
    public static final C5663aRz dlY = null;
    public static final C5663aRz dma = null;
    public static final C5663aRz dmj = null;
    public static final C2491fm geA = null;
    public static final C2491fm geB = null;
    public static final C2491fm geC = null;
    public static final C2491fm geD = null;
    public static final C2491fm geE = null;
    public static final C2491fm geF = null;
    public static final C2491fm geG = null;
    public static final C2491fm geH = null;
    public static final C2491fm geI = null;
    public static final C2491fm geJ = null;
    public static final C2491fm geK = null;
    public static final C2491fm gez = null;
    public static final long serialVersionUID = 0;
    /* renamed from: uU */
    public static final C5663aRz f4948uU = null;
    /* renamed from: vi */
    public static final C2491fm f4949vi = null;
    /* renamed from: vj */
    public static final C2491fm f4950vj = null;
    public static C6494any ___iScriptClass;
    @C0064Am(aul = "4aa4b6c56fe92cddc2866bb64b469403", aum = 8)

    /* renamed from: LQ */
    private static Asset f4938LQ;
    @C0064Am(aul = "b760746232ecfd863bd89f93d7fadaef", aum = 2)

    /* renamed from: Wb */
    private static Asset f4941Wb;
    @C0064Am(aul = "5f93644d49cae1b906d8cccbf08cbed9", aum = 3)

    /* renamed from: Wd */
    private static Asset f4943Wd;
    @C0064Am(aul = "d8e9f0453c0a558c4f9d98f1bfeb2d39", aum = 11)
    private static HullType bhn;
    @C0064Am(aul = "9780c1051e8f958ab8cfd87121cdc0e2", aum = 1)
    private static CollisionFXSet bnE;
    @C0064Am(aul = "4dc1d77d7c4c5a959cee60ebc4f56bed", aum = 7)
    private static Asset boc;
    @C0064Am(aul = "3731efd173ccc728ece64ba1bf8441c8", aum = 4)

    /* renamed from: dX */
    private static float f4946dX;
    @C0064Am(aul = "9a0a2e7280df8fada02f8ba8edddfd90", aum = 0)
    private static C0712KI dlU;
    @C0064Am(aul = "eaa3e6b2e7030fa289da40f0331a60a9", aum = 6)
    private static float dlX;
    @C0064Am(aul = "18392ddc462f1d233678720c5d477c80", aum = 10)
    private static I18NString dlZ;
    @C0064Am(aul = "be369f2252cd1991ad5bc88700d2e1db", aum = 12)
    private static ShieldType fPa;
    @C0064Am(aul = "26922fd083d96e3d497ae7ece27b1c54", aum = 13)
    private static WeaponType fPb;
    @C0064Am(aul = "d839bdd4574dd9a72556b40b611ac60b", aum = 5)
    private static float maxPitch;
    @C0064Am(aul = "10bddbe9482fd71cfbfeb7b6da98cbc6", aum = 9)

    /* renamed from: uT */
    private static String f4947uT;

    static {
        m24077V();
    }

    public TurretTypeBase() {
        super((C5540aNg) null);
        super.mo10S();
    }

    public TurretTypeBase(C5540aNg ang) {
        super(ang);
    }

    /* renamed from: V */
    static void m24077V() {
        serialVersionUID = (long) 1;
        _m_fieldCount = BaseTaikodomContent._m_fieldCount + 14;
        _m_methodCount = BaseTaikodomContent._m_methodCount + 31;
        int i = BaseTaikodomContent._m_fieldCount;
        C5663aRz[] arzArr = new C5663aRz[(i + 14)];
        C5663aRz b = C5640aRc.m17844b(TurretTypeBase.class, "9a0a2e7280df8fada02f8ba8edddfd90", i);
        dlV = b;
        arzArr[i] = b;
        int i2 = i + 1;
        C5663aRz b2 = C5640aRc.m17844b(TurretTypeBase.class, "9780c1051e8f958ab8cfd87121cdc0e2", i2);
        bnF = b2;
        arzArr[i2] = b2;
        int i3 = i2 + 1;
        C5663aRz b3 = C5640aRc.m17844b(TurretTypeBase.class, "b760746232ecfd863bd89f93d7fadaef", i3);
        f4942Wc = b3;
        arzArr[i3] = b3;
        int i4 = i3 + 1;
        C5663aRz b4 = C5640aRc.m17844b(TurretTypeBase.class, "5f93644d49cae1b906d8cccbf08cbed9", i4);
        f4944We = b4;
        arzArr[i4] = b4;
        int i5 = i4 + 1;
        C5663aRz b5 = C5640aRc.m17844b(TurretTypeBase.class, "3731efd173ccc728ece64ba1bf8441c8", i5);
        bnP = b5;
        arzArr[i5] = b5;
        int i6 = i5 + 1;
        C5663aRz b6 = C5640aRc.m17844b(TurretTypeBase.class, "d839bdd4574dd9a72556b40b611ac60b", i6);
        dlW = b6;
        arzArr[i6] = b6;
        int i7 = i6 + 1;
        C5663aRz b7 = C5640aRc.m17844b(TurretTypeBase.class, "eaa3e6b2e7030fa289da40f0331a60a9", i7);
        dlY = b7;
        arzArr[i7] = b7;
        int i8 = i7 + 1;
        C5663aRz b8 = C5640aRc.m17844b(TurretTypeBase.class, "4dc1d77d7c4c5a959cee60ebc4f56bed", i8);
        bod = b8;
        arzArr[i8] = b8;
        int i9 = i8 + 1;
        C5663aRz b9 = C5640aRc.m17844b(TurretTypeBase.class, "4aa4b6c56fe92cddc2866bb64b469403", i9);
        f4939LR = b9;
        arzArr[i9] = b9;
        int i10 = i9 + 1;
        C5663aRz b10 = C5640aRc.m17844b(TurretTypeBase.class, "10bddbe9482fd71cfbfeb7b6da98cbc6", i10);
        f4948uU = b10;
        arzArr[i10] = b10;
        int i11 = i10 + 1;
        C5663aRz b11 = C5640aRc.m17844b(TurretTypeBase.class, "18392ddc462f1d233678720c5d477c80", i11);
        dma = b11;
        arzArr[i11] = b11;
        int i12 = i11 + 1;
        C5663aRz b12 = C5640aRc.m17844b(TurretTypeBase.class, "d8e9f0453c0a558c4f9d98f1bfeb2d39", i12);
        f4940VW = b12;
        arzArr[i12] = b12;
        int i13 = i12 + 1;
        C5663aRz b13 = C5640aRc.m17844b(TurretTypeBase.class, "be369f2252cd1991ad5bc88700d2e1db", i13);
        dmj = b13;
        arzArr[i13] = b13;
        int i14 = i13 + 1;
        C5663aRz b14 = C5640aRc.m17844b(TurretTypeBase.class, "26922fd083d96e3d497ae7ece27b1c54", i14);
        f4937AG = b14;
        arzArr[i14] = b14;
        int i15 = i14 + 1;
        _m_fields = arzArr;
        C1634Xv.m11725a((Object[]) BaseTaikodomContent._m_fields, (Object[]) _m_fields);
        int i16 = BaseTaikodomContent._m_methodCount;
        C2491fm[] fmVarArr = new C2491fm[(i16 + 31)];
        C2491fm a = C4105zY.m41624a(TurretTypeBase.class, "f4fffbaeb662825ffd4c10a1e25d9904", i16);
        gez = a;
        fmVarArr[i16] = a;
        int i17 = i16 + 1;
        C2491fm a2 = C4105zY.m41624a(TurretTypeBase.class, "ac1d154e0d9dcdb8523aa76f3275fac1", i17);
        geA = a2;
        fmVarArr[i17] = a2;
        int i18 = i17 + 1;
        C2491fm a3 = C4105zY.m41624a(TurretTypeBase.class, "a38cae0a8f92104a3beeceb3450b3326", i18);
        dGK = a3;
        fmVarArr[i18] = a3;
        int i19 = i18 + 1;
        C2491fm a4 = C4105zY.m41624a(TurretTypeBase.class, "252074c075db5ae207d86bae0e7199e0", i19);
        dGL = a4;
        fmVarArr[i19] = a4;
        int i20 = i19 + 1;
        C2491fm a5 = C4105zY.m41624a(TurretTypeBase.class, "bf8a032daf5d1d9c440938ed1fc8e8ad", i20);
        dGQ = a5;
        fmVarArr[i20] = a5;
        int i21 = i20 + 1;
        C2491fm a6 = C4105zY.m41624a(TurretTypeBase.class, "bfeee550a9e3205742ed2c18617b7354", i21);
        dGR = a6;
        fmVarArr[i21] = a6;
        int i22 = i21 + 1;
        C2491fm a7 = C4105zY.m41624a(TurretTypeBase.class, "69438706bf948f56e20cc7989b6f8f77", i22);
        dGS = a7;
        fmVarArr[i22] = a7;
        int i23 = i22 + 1;
        C2491fm a8 = C4105zY.m41624a(TurretTypeBase.class, "dd70aa72ef7d6c3fae6932bf68304dcc", i23);
        dGT = a8;
        fmVarArr[i23] = a8;
        int i24 = i23 + 1;
        C2491fm a9 = C4105zY.m41624a(TurretTypeBase.class, "83f0e8ca660c6ea0fb0975d12d7e68a1", i24);
        aTf = a9;
        fmVarArr[i24] = a9;
        int i25 = i24 + 1;
        C2491fm a10 = C4105zY.m41624a(TurretTypeBase.class, "1791c6a74d635b1290fb76e37e46d2ae", i25);
        dGB = a10;
        fmVarArr[i25] = a10;
        int i26 = i25 + 1;
        C2491fm a11 = C4105zY.m41624a(TurretTypeBase.class, "16a911484dbf71f07053764554b81650", i26);
        geB = a11;
        fmVarArr[i26] = a11;
        int i27 = i26 + 1;
        C2491fm a12 = C4105zY.m41624a(TurretTypeBase.class, "9b3693dbee70ad7ae4b154e6967c6df2", i27);
        geC = a12;
        fmVarArr[i27] = a12;
        int i28 = i27 + 1;
        C2491fm a13 = C4105zY.m41624a(TurretTypeBase.class, "bfc6c1fac93bfca821580e6e70f88e24", i28);
        geD = a13;
        fmVarArr[i28] = a13;
        int i29 = i28 + 1;
        C2491fm a14 = C4105zY.m41624a(TurretTypeBase.class, "f94e1a9cd81e7b34147b2d155cdc0764", i29);
        geE = a14;
        fmVarArr[i29] = a14;
        int i30 = i29 + 1;
        C2491fm a15 = C4105zY.m41624a(TurretTypeBase.class, "a8fcbd91e581f210ea61e57be9d7e563", i30);
        dHh = a15;
        fmVarArr[i30] = a15;
        int i31 = i30 + 1;
        C2491fm a16 = C4105zY.m41624a(TurretTypeBase.class, "21edafd6b7353c962741399387258235", i31);
        dHi = a16;
        fmVarArr[i31] = a16;
        int i32 = i31 + 1;
        C2491fm a17 = C4105zY.m41624a(TurretTypeBase.class, "fe40f26345e2a2ca525442236cc2993e", i32);
        aDk = a17;
        fmVarArr[i32] = a17;
        int i33 = i32 + 1;
        C2491fm a18 = C4105zY.m41624a(TurretTypeBase.class, "6d62fbdb27ffd3b84305684ab359959e", i33);
        aDl = a18;
        fmVarArr[i33] = a18;
        int i34 = i33 + 1;
        C2491fm a19 = C4105zY.m41624a(TurretTypeBase.class, "327ee52dd61b819359831631dc90560d", i34);
        f4949vi = a19;
        fmVarArr[i34] = a19;
        int i35 = i34 + 1;
        C2491fm a20 = C4105zY.m41624a(TurretTypeBase.class, "8c7d50e7bb87b15b0a66fcfab333691b", i35);
        f4950vj = a20;
        fmVarArr[i35] = a20;
        int i36 = i35 + 1;
        C2491fm a21 = C4105zY.m41624a(TurretTypeBase.class, "bda33332d40f24b211a28c90766882be", i36);
        geF = a21;
        fmVarArr[i36] = a21;
        int i37 = i36 + 1;
        C2491fm a22 = C4105zY.m41624a(TurretTypeBase.class, "c3c0c8fac93b254eb6abfb27a892b81d", i37);
        geG = a22;
        fmVarArr[i37] = a22;
        int i38 = i37 + 1;
        C2491fm a23 = C4105zY.m41624a(TurretTypeBase.class, "f2b373e0a59a27e2bd3434eb225f9407", i38);
        aTr = a23;
        fmVarArr[i38] = a23;
        int i39 = i38 + 1;
        C2491fm a24 = C4105zY.m41624a(TurretTypeBase.class, "0e6c149bd56fae23414918d55768bbb2", i39);
        dHz = a24;
        fmVarArr[i39] = a24;
        int i40 = i39 + 1;
        C2491fm a25 = C4105zY.m41624a(TurretTypeBase.class, "655d5293dacd6cf55482911682a3650a", i40);
        aTt = a25;
        fmVarArr[i40] = a25;
        int i41 = i40 + 1;
        C2491fm a26 = C4105zY.m41624a(TurretTypeBase.class, "f10ab6aa0f0870e14f482d27556f44ca", i41);
        geH = a26;
        fmVarArr[i41] = a26;
        int i42 = i41 + 1;
        C2491fm a27 = C4105zY.m41624a(TurretTypeBase.class, "c5474b3cf86a546f782f1e87c93642d1", i42);
        geI = a27;
        fmVarArr[i42] = a27;
        int i43 = i42 + 1;
        C2491fm a28 = C4105zY.m41624a(TurretTypeBase.class, "f49030a3304a4446b4e69e472e16a625", i43);
        geJ = a28;
        fmVarArr[i43] = a28;
        int i44 = i43 + 1;
        C2491fm a29 = C4105zY.m41624a(TurretTypeBase.class, "2520e98bbf7af1532de86a1f106759f1", i44);
        aDR = a29;
        fmVarArr[i44] = a29;
        int i45 = i44 + 1;
        C2491fm a30 = C4105zY.m41624a(TurretTypeBase.class, "dff69e4c6fcf5688fcc2f5189480d715", i45);
        geK = a30;
        fmVarArr[i45] = a30;
        int i46 = i45 + 1;
        C2491fm a31 = C4105zY.m41624a(TurretTypeBase.class, "a09546a9b9fdc054329022dc692df1ca", i46);
        f4945dN = a31;
        fmVarArr[i46] = a31;
        int i47 = i46 + 1;
        _m_methods = fmVarArr;
        C1634Xv.m11725a((Object[]) BaseTaikodomContent._m_methods, (Object[]) _m_methods);
        ___iScriptClass = aUO.m18566a(TurretTypeBase.class, C6271ajj.class, _m_fields, _m_methods);
    }

    /* renamed from: A */
    private void m24072A(Asset tCVar) {
        bFf().mo5608dq().mo3197f(f4944We, tCVar);
    }

    /* renamed from: H */
    private void m24074H(String str) {
        bFf().mo5608dq().mo3197f(f4948uU, str);
    }

    /* renamed from: W */
    private void m24081W(Asset tCVar) {
        bFf().mo5608dq().mo3197f(bod, tCVar);
    }

    /* renamed from: a */
    private void m24082a(C0712KI ki) {
        bFf().mo5608dq().mo3197f(dlV, ki);
    }

    /* renamed from: a */
    private void m24083a(CollisionFXSet yaVar) {
        bFf().mo5608dq().mo3197f(bnF, yaVar);
    }

    private C0712KI aZC() {
        return (C0712KI) bFf().mo5608dq().mo3214p(dlV);
    }

    private float aZD() {
        return bFf().mo5608dq().mo3211m(dlW);
    }

    private float aZE() {
        return bFf().mo5608dq().mo3211m(dlY);
    }

    private I18NString aZF() {
        return (I18NString) bFf().mo5608dq().mo3214p(dma);
    }

    private CollisionFXSet aeJ() {
        return (CollisionFXSet) bFf().mo5608dq().mo3214p(bnF);
    }

    private float aeP() {
        return bFf().mo5608dq().mo3211m(bnP);
    }

    private Asset aeX() {
        return (Asset) bFf().mo5608dq().mo3214p(bod);
    }

    private HullType bjZ() {
        return (HullType) bFf().mo5608dq().mo3214p(f4940VW);
    }

    private ShieldType clI() {
        return (ShieldType) bFf().mo5608dq().mo3214p(dmj);
    }

    private WeaponType clJ() {
        return (WeaponType) bFf().mo5608dq().mo3214p(f4937AG);
    }

    /* renamed from: du */
    private void m24089du(float f) {
        bFf().mo5608dq().mo3150a(bnP, f);
    }

    /* renamed from: f */
    private void m24091f(ShieldType aph) {
        bFf().mo5608dq().mo3197f(dmj, aph);
    }

    /* renamed from: f */
    private void m24092f(HullType wHVar) {
        bFf().mo5608dq().mo3197f(f4940VW, wHVar);
    }

    /* renamed from: fN */
    private void m24094fN(float f) {
        bFf().mo5608dq().mo3150a(dlW, f);
    }

    /* renamed from: fO */
    private void m24095fO(float f) {
        bFf().mo5608dq().mo3150a(dlY, f);
    }

    /* renamed from: gS */
    private void m24099gS(I18NString i18NString) {
        bFf().mo5608dq().mo3197f(dma, i18NString);
    }

    /* renamed from: ij */
    private String m24100ij() {
        return (String) bFf().mo5608dq().mo3214p(f4948uU);
    }

    /* renamed from: r */
    private void m24105r(WeaponType apt) {
        bFf().mo5608dq().mo3197f(f4937AG, apt);
    }

    /* renamed from: r */
    private void m24106r(Asset tCVar) {
        bFf().mo5608dq().mo3197f(f4939LR, tCVar);
    }

    /* renamed from: rh */
    private Asset m24107rh() {
        return (Asset) bFf().mo5608dq().mo3214p(f4939LR);
    }

    /* renamed from: z */
    private void m24109z(Asset tCVar) {
        bFf().mo5608dq().mo3197f(f4942Wc, tCVar);
    }

    /* renamed from: zl */
    private Asset m24110zl() {
        return (Asset) bFf().mo5608dq().mo3214p(f4942Wc);
    }

    /* renamed from: zm */
    private Asset m24111zm() {
        return (Asset) bFf().mo5608dq().mo3214p(f4944We);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Render Asset")
    /* renamed from: I */
    public void mo14926I(Asset tCVar) {
        switch (bFf().mo6893i(aDl)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, aDl, new Object[]{tCVar}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, aDl, new Object[]{tCVar}));
                break;
        }
        m24073H(tCVar);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Surface Material")
    /* renamed from: N */
    public void mo14927N(String str) {
        switch (bFf().mo6893i(f4950vj)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f4950vj, new Object[]{str}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f4950vj, new Object[]{str}));
                break;
        }
        m24075M(str);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Render Asset")
    /* renamed from: Nu */
    public Asset mo14928Nu() {
        switch (bFf().mo6893i(aDk)) {
            case 0:
                return null;
            case 2:
                return (Asset) bFf().mo5606d(new aCE(this, aDk, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, aDk, new Object[0]));
                break;
        }
        return m24076Nt();
    }

    /* renamed from: T */
    public C6494any mo11T() {
        return ___iScriptClass;
    }

    /* renamed from: U */
    public C2491fm[] mo12U() {
        return _m_methods;
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Max Angular Velocity")
    /* renamed from: VH */
    public float mo14929VH() {
        switch (bFf().mo6893i(aTf)) {
            case 0:
                return 0.0f;
            case 2:
                return ((Float) bFf().mo5606d(new aCE(this, aTf, new Object[0]))).floatValue();
            case 3:
                bFf().mo5606d(new aCE(this, aTf, new Object[0]));
                break;
        }
        return m24078VG();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Hull")
    /* renamed from: VT */
    public HullType mo14930VT() {
        switch (bFf().mo6893i(aTr)) {
            case 0:
                return null;
            case 2:
                return (HullType) bFf().mo5606d(new aCE(this, aTr, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, aTr, new Object[0]));
                break;
        }
        return m24079VS();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Shield")
    /* renamed from: VX */
    public ShieldType mo14931VX() {
        switch (bFf().mo6893i(aTt)) {
            case 0:
                return null;
            case 2:
                return (ShieldType) bFf().mo5606d(new aCE(this, aTt, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, aTt, new Object[0]));
                break;
        }
        return m24080VW();
    }

    /* renamed from: W */
    public Object mo13W() {
        return new C6271ajj(this);
    }

    /* renamed from: a */
    public Object mo14a(C0495Gr gr) {
        Object[] args = gr.getArgs();
        switch (gr.mo2417hq() - BaseTaikodomContent._m_methodCount) {
            case 0:
                return clK();
            case 1:
                m24088b((C0712KI) args[0]);
                return null;
            case 2:
                return bka();
            case 3:
                m24093f((CollisionFXSet) args[0]);
                return null;
            case 4:
                return bkg();
            case 5:
                m24084aR((Asset) args[0]);
                return null;
            case 6:
                return bki();
            case 7:
                m24086aT((Asset) args[0]);
                return null;
            case 8:
                return new Float(m24078VG());
            case 9:
                m24098gD(((Float) args[0]).floatValue());
                return null;
            case 10:
                return new Float(clM());
            case 11:
                m24102jU(((Float) args[0]).floatValue());
                return null;
            case 12:
                return new Float(clN());
            case 13:
                m24103jV(((Float) args[0]).floatValue());
                return null;
            case 14:
                return bkw();
            case 15:
                m24087aV((Asset) args[0]);
                return null;
            case 16:
                return m24076Nt();
            case 17:
                m24073H((Asset) args[0]);
                return null;
            case 18:
                return m24101it();
            case 19:
                m24075M((String) args[0]);
                return null;
            case 20:
                return clP();
            case 21:
                m24104mL((I18NString) args[0]);
                return null;
            case 22:
                return m24079VS();
            case 23:
                m24097g((HullType) args[0]);
                return null;
            case 24:
                return m24080VW();
            case 25:
                m24096g((ShieldType) args[0]);
                return null;
            case 26:
                return clR();
            case 27:
                m24108s((WeaponType) args[0]);
                return null;
            case 28:
                m24090e((aDJ) args[0]);
                return null;
            case 29:
                return clT();
            case 30:
                return m24085aT();
            default:
                return super.mo14a(gr);
        }
    }

    /* renamed from: a */
    public void mo15a(Collection collection, C0495Gr gr) {
        super.mo15a(collection, gr);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Explosion G F X")
    /* renamed from: aS */
    public void mo14932aS(Asset tCVar) {
        switch (bFf().mo6893i(dGR)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, dGR, new Object[]{tCVar}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, dGR, new Object[]{tCVar}));
                break;
        }
        m24084aR(tCVar);
    }

    /* access modifiers changed from: protected */
    /* renamed from: aU */
    public <T extends aDJ> T mo745aU() {
        switch (bFf().mo6893i(f4945dN)) {
            case 0:
                return null;
            case 2:
                return (aDJ) bFf().mo5606d(new aCE(this, f4945dN, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f4945dN, new Object[0]));
                break;
        }
        return m24085aT();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Explosion S F X")
    /* renamed from: aU */
    public void mo14933aU(Asset tCVar) {
        switch (bFf().mo6893i(dGT)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, dGT, new Object[]{tCVar}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, dGT, new Object[]{tCVar}));
                break;
        }
        m24086aT(tCVar);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Physical Asset")
    /* renamed from: aW */
    public void mo14934aW(Asset tCVar) {
        switch (bFf().mo6893i(dHi)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, dHi, new Object[]{tCVar}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, dHi, new Object[]{tCVar}));
                break;
        }
        m24087aV(tCVar);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Max Angular Velocity")
    /* renamed from: as */
    public void mo14935as(float f) {
        switch (bFf().mo6893i(dGB)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, dGB, new Object[]{new Float(f)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, dGB, new Object[]{new Float(f)}));
                break;
        }
        m24098gD(f);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Collision Set")
    public CollisionFXSet bkb() {
        switch (bFf().mo6893i(dGK)) {
            case 0:
                return null;
            case 2:
                return (CollisionFXSet) bFf().mo5606d(new aCE(this, dGK, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, dGK, new Object[0]));
                break;
        }
        return bka();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Explosion G F X")
    public Asset bkh() {
        switch (bFf().mo6893i(dGQ)) {
            case 0:
                return null;
            case 2:
                return (Asset) bFf().mo5606d(new aCE(this, dGQ, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, dGQ, new Object[0]));
                break;
        }
        return bkg();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Explosion S F X")
    public Asset bkj() {
        switch (bFf().mo6893i(dGS)) {
            case 0:
                return null;
            case 2:
                return (Asset) bFf().mo5606d(new aCE(this, dGS, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, dGS, new Object[0]));
                break;
        }
        return bki();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Physical Asset")
    public Asset bkx() {
        switch (bFf().mo6893i(dHh)) {
            case 0:
                return null;
            case 2:
                return (Asset) bFf().mo5606d(new aCE(this, dHh, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, dHh, new Object[0]));
                break;
        }
        return bkw();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Ai Controller Type")
    /* renamed from: c */
    public void mo14940c(C0712KI ki) {
        switch (bFf().mo6893i(geA)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, geA, new Object[]{ki}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, geA, new Object[]{ki}));
                break;
        }
        m24088b(ki);
    }

    /* renamed from: c */
    public C5663aRz[] mo25c() {
        return _m_fields;
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Ai Controller Type")
    public C0712KI clL() {
        switch (bFf().mo6893i(gez)) {
            case 0:
                return null;
            case 2:
                return (C0712KI) bFf().mo5606d(new aCE(this, gez, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, gez, new Object[0]));
                break;
        }
        return clK();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Min Pitch")
    public float clO() {
        switch (bFf().mo6893i(geD)) {
            case 0:
                return 0.0f;
            case 2:
                return ((Float) bFf().mo5606d(new aCE(this, geD, new Object[0]))).floatValue();
            case 3:
                bFf().mo5606d(new aCE(this, geD, new Object[0]));
                break;
        }
        return clN();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Template Name")
    public I18NString clQ() {
        switch (bFf().mo6893i(geF)) {
            case 0:
                return null;
            case 2:
                return (I18NString) bFf().mo5606d(new aCE(this, geF, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, geF, new Object[0]));
                break;
        }
        return clP();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Weapon")
    public WeaponType clS() {
        switch (bFf().mo6893i(geI)) {
            case 0:
                return null;
            case 2:
                return (WeaponType) bFf().mo5606d(new aCE(this, geI, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, geI, new Object[0]));
                break;
        }
        return clR();
    }

    public Turret clU() {
        switch (bFf().mo6893i(geK)) {
            case 0:
                return null;
            case 2:
                return (Turret) bFf().mo5606d(new aCE(this, geK, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, geK, new Object[0]));
                break;
        }
        return clT();
    }

    /* renamed from: f */
    public void mo2631f(aDJ adj) {
        switch (bFf().mo6893i(aDR)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, aDR, new Object[]{adj}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, aDR, new Object[]{adj}));
                break;
        }
        m24090e(adj);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Collision Set")
    /* renamed from: g */
    public void mo14946g(CollisionFXSet yaVar) {
        switch (bFf().mo6893i(dGL)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, dGL, new Object[]{yaVar}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, dGL, new Object[]{yaVar}));
                break;
        }
        m24093f(yaVar);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Max Pitch")
    public float getMaxPitch() {
        switch (bFf().mo6893i(geB)) {
            case 0:
                return 0.0f;
            case 2:
                return ((Float) bFf().mo5606d(new aCE(this, geB, new Object[0]))).floatValue();
            case 3:
                bFf().mo5606d(new aCE(this, geB, new Object[0]));
                break;
        }
        return clM();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Max Pitch")
    public void setMaxPitch(float f) {
        switch (bFf().mo6893i(geC)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, geC, new Object[]{new Float(f)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, geC, new Object[]{new Float(f)}));
                break;
        }
        m24102jU(f);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Shield")
    /* renamed from: h */
    public void mo14948h(ShieldType aph) {
        switch (bFf().mo6893i(geH)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, geH, new Object[]{aph}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, geH, new Object[]{aph}));
                break;
        }
        m24096g(aph);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Hull")
    /* renamed from: h */
    public void mo14949h(HullType wHVar) {
        switch (bFf().mo6893i(dHz)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, dHz, new Object[]{wHVar}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, dHz, new Object[]{wHVar}));
                break;
        }
        m24097g(wHVar);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Surface Material")
    /* renamed from: iu */
    public String mo14950iu() {
        switch (bFf().mo6893i(f4949vi)) {
            case 0:
                return null;
            case 2:
                return (String) bFf().mo5606d(new aCE(this, f4949vi, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f4949vi, new Object[0]));
                break;
        }
        return m24101it();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Min Pitch")
    /* renamed from: jW */
    public void mo14951jW(float f) {
        switch (bFf().mo6893i(geE)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, geE, new Object[]{new Float(f)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, geE, new Object[]{new Float(f)}));
                break;
        }
        m24103jV(f);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Template Name")
    /* renamed from: mM */
    public void mo14952mM(I18NString i18NString) {
        switch (bFf().mo6893i(geG)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, geG, new Object[]{i18NString}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, geG, new Object[]{i18NString}));
                break;
        }
        m24104mL(i18NString);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Weapon")
    /* renamed from: t */
    public void mo14954t(WeaponType apt) {
        switch (bFf().mo6893i(geJ)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, geJ, new Object[]{apt}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, geJ, new Object[]{apt}));
                break;
        }
        m24108s(apt);
    }

    /* renamed from: S */
    public void mo10S() {
        super.mo10S();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Ai Controller Type")
    @C0064Am(aul = "f4fffbaeb662825ffd4c10a1e25d9904", aum = 0)
    private C0712KI clK() {
        return aZC();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Ai Controller Type")
    @C0064Am(aul = "ac1d154e0d9dcdb8523aa76f3275fac1", aum = 0)
    /* renamed from: b */
    private void m24088b(C0712KI ki) {
        m24082a(ki);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Collision Set")
    @C0064Am(aul = "a38cae0a8f92104a3beeceb3450b3326", aum = 0)
    private CollisionFXSet bka() {
        return aeJ();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Collision Set")
    @C0064Am(aul = "252074c075db5ae207d86bae0e7199e0", aum = 0)
    /* renamed from: f */
    private void m24093f(CollisionFXSet yaVar) {
        m24083a(yaVar);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Explosion G F X")
    @C0064Am(aul = "bf8a032daf5d1d9c440938ed1fc8e8ad", aum = 0)
    private Asset bkg() {
        return m24110zl();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Explosion G F X")
    @C0064Am(aul = "bfeee550a9e3205742ed2c18617b7354", aum = 0)
    /* renamed from: aR */
    private void m24084aR(Asset tCVar) {
        m24109z(tCVar);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Explosion S F X")
    @C0064Am(aul = "69438706bf948f56e20cc7989b6f8f77", aum = 0)
    private Asset bki() {
        return m24111zm();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Explosion S F X")
    @C0064Am(aul = "dd70aa72ef7d6c3fae6932bf68304dcc", aum = 0)
    /* renamed from: aT */
    private void m24086aT(Asset tCVar) {
        m24072A(tCVar);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Max Angular Velocity")
    @C0064Am(aul = "83f0e8ca660c6ea0fb0975d12d7e68a1", aum = 0)
    /* renamed from: VG */
    private float m24078VG() {
        return aeP();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Max Angular Velocity")
    @C0064Am(aul = "1791c6a74d635b1290fb76e37e46d2ae", aum = 0)
    /* renamed from: gD */
    private void m24098gD(float f) {
        m24089du(f);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Max Pitch")
    @C0064Am(aul = "16a911484dbf71f07053764554b81650", aum = 0)
    private float clM() {
        return aZD();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Max Pitch")
    @C0064Am(aul = "9b3693dbee70ad7ae4b154e6967c6df2", aum = 0)
    /* renamed from: jU */
    private void m24102jU(float f) {
        m24094fN(f);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Min Pitch")
    @C0064Am(aul = "bfc6c1fac93bfca821580e6e70f88e24", aum = 0)
    private float clN() {
        return aZE();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Min Pitch")
    @C0064Am(aul = "f94e1a9cd81e7b34147b2d155cdc0764", aum = 0)
    /* renamed from: jV */
    private void m24103jV(float f) {
        m24095fO(f);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Physical Asset")
    @C0064Am(aul = "a8fcbd91e581f210ea61e57be9d7e563", aum = 0)
    private Asset bkw() {
        return aeX();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Physical Asset")
    @C0064Am(aul = "21edafd6b7353c962741399387258235", aum = 0)
    /* renamed from: aV */
    private void m24087aV(Asset tCVar) {
        m24081W(tCVar);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Render Asset")
    @C0064Am(aul = "fe40f26345e2a2ca525442236cc2993e", aum = 0)
    /* renamed from: Nt */
    private Asset m24076Nt() {
        return m24107rh();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Render Asset")
    @C0064Am(aul = "6d62fbdb27ffd3b84305684ab359959e", aum = 0)
    /* renamed from: H */
    private void m24073H(Asset tCVar) {
        m24106r(tCVar);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Surface Material")
    @C0064Am(aul = "327ee52dd61b819359831631dc90560d", aum = 0)
    /* renamed from: it */
    private String m24101it() {
        return m24100ij();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Surface Material")
    @C0064Am(aul = "8c7d50e7bb87b15b0a66fcfab333691b", aum = 0)
    /* renamed from: M */
    private void m24075M(String str) {
        m24074H(str);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Template Name")
    @C0064Am(aul = "bda33332d40f24b211a28c90766882be", aum = 0)
    private I18NString clP() {
        return aZF();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Template Name")
    @C0064Am(aul = "c3c0c8fac93b254eb6abfb27a892b81d", aum = 0)
    /* renamed from: mL */
    private void m24104mL(I18NString i18NString) {
        m24099gS(i18NString);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Hull")
    @C0064Am(aul = "f2b373e0a59a27e2bd3434eb225f9407", aum = 0)
    /* renamed from: VS */
    private HullType m24079VS() {
        return bjZ();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Hull")
    @C0064Am(aul = "0e6c149bd56fae23414918d55768bbb2", aum = 0)
    /* renamed from: g */
    private void m24097g(HullType wHVar) {
        m24092f(wHVar);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Shield")
    @C0064Am(aul = "655d5293dacd6cf55482911682a3650a", aum = 0)
    /* renamed from: VW */
    private ShieldType m24080VW() {
        return clI();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Shield")
    @C0064Am(aul = "f10ab6aa0f0870e14f482d27556f44ca", aum = 0)
    /* renamed from: g */
    private void m24096g(ShieldType aph) {
        m24091f(aph);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Weapon")
    @C0064Am(aul = "c5474b3cf86a546f782f1e87c93642d1", aum = 0)
    private WeaponType clR() {
        return clJ();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Weapon")
    @C0064Am(aul = "f49030a3304a4446b4e69e472e16a625", aum = 0)
    /* renamed from: s */
    private void m24108s(WeaponType apt) {
        m24105r(apt);
    }

    @C0064Am(aul = "2520e98bbf7af1532de86a1f106759f1", aum = 0)
    /* renamed from: e */
    private void m24090e(aDJ adj) {
        super.mo2631f(adj);
        if (adj instanceof Turret) {
            C1616Xf xf = adj;
            if (bjZ() != null) {
                xf.mo6765g(C3168oe.aMH, bjZ().mo7459NK());
            }
            if (clI() != null) {
                xf.mo6765g(C3168oe.aMI, clI().mo7459NK());
            }
            if (clJ() != null) {
                xf.mo6765g(C3168oe.aML, clJ().mo7459NK());
            }
        }
    }

    @C0064Am(aul = "dff69e4c6fcf5688fcc2f5189480d715", aum = 0)
    private Turret clT() {
        return (Turret) mo745aU();
    }

    @C0064Am(aul = "a09546a9b9fdc054329022dc692df1ca", aum = 0)
    /* renamed from: aT */
    private <T extends aDJ> T m24085aT() {
        T t = (Turret) bFf().mo6865M(Turret.class);
        t.mo2968a(this);
        return t;
    }
}
