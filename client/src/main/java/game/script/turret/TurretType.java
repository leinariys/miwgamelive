package game.script.turret;

import game.script.resource.Asset;

import java.util.Collection;

@C5511aMd
@C6485anp
/* renamed from: a.aow  reason: case insensitive filesystem */
/* compiled from: a */
public class TurretType extends C6454anK implements C1616Xf {
    public static final C2491fm _f_onResurrect_0020_0028_0029V = null;
    public static final int _m_fieldCount = 0;
    public static final C5663aRz[] _m_fields = null;
    public static final int _m_methodCount = 0;
    public static final C2491fm[] _m_methods = null;
    public static final C2491fm aDl = null;
    public static final C5663aRz dmf = null;
    public static final C5663aRz dmk = null;
    public static final C5663aRz dmn = null;
    public static final C2491fm gig = null;
    public static final C2491fm gih = null;
    public static final C2491fm gii = null;
    public static final C2491fm gij = null;
    public static final C2491fm gik = null;
    public static final C2491fm gil = null;
    public static final C2491fm gim = null;
    public static final long serialVersionUID = 0;
    public static C6494any ___iScriptClass;
    @C0064Am(aul = "678ebfcc30eb88a4d0a8a9eb1b43af18", aum = 0)
    private static C6853aut dcK;
    @C0064Am(aul = "f55e75d0af8055253af7e11ed1cd6ed7", aum = 1)
    private static C6853aut dcL;
    @C0064Am(aul = "b79ded1bb7e19bdab19fbf6bb36c47b9", aum = 2)
    private static C6853aut dcM;

    static {
        m24719V();
    }

    public TurretType() {
        super((C5540aNg) null);
        super.mo10S();
    }

    public TurretType(C5540aNg ang) {
        super(ang);
    }

    /* renamed from: V */
    static void m24719V() {
        serialVersionUID = (long) 1;
        _m_fieldCount = C6454anK._m_fieldCount + 3;
        _m_methodCount = C6454anK._m_methodCount + 9;
        int i = C6454anK._m_fieldCount;
        C5663aRz[] arzArr = new C5663aRz[(i + 3)];
        C5663aRz b = C5640aRc.m17844b(TurretType.class, "678ebfcc30eb88a4d0a8a9eb1b43af18", i);
        dmn = b;
        arzArr[i] = b;
        int i2 = i + 1;
        C5663aRz b2 = C5640aRc.m17844b(TurretType.class, "f55e75d0af8055253af7e11ed1cd6ed7", i2);
        dmf = b2;
        arzArr[i2] = b2;
        int i3 = i2 + 1;
        C5663aRz b3 = C5640aRc.m17844b(TurretType.class, "b79ded1bb7e19bdab19fbf6bb36c47b9", i3);
        dmk = b3;
        arzArr[i3] = b3;
        int i4 = i3 + 1;
        _m_fields = arzArr;
        C1634Xv.m11725a((Object[]) C6454anK._m_fields, (Object[]) _m_fields);
        int i5 = C6454anK._m_methodCount;
        C2491fm[] fmVarArr = new C2491fm[(i5 + 9)];
        C2491fm a = C4105zY.m41624a(TurretType.class, "50c4385e17358af1c793337744ac4e4b", i5);
        gig = a;
        fmVarArr[i5] = a;
        int i6 = i5 + 1;
        C2491fm a2 = C4105zY.m41624a(TurretType.class, "c9e79f6c930d78495472ff7344001886", i6);
        gih = a2;
        fmVarArr[i6] = a2;
        int i7 = i6 + 1;
        C2491fm a3 = C4105zY.m41624a(TurretType.class, "dfa08e7442f5d411aaeaf889682166de", i7);
        gii = a3;
        fmVarArr[i7] = a3;
        int i8 = i7 + 1;
        C2491fm a4 = C4105zY.m41624a(TurretType.class, "d11fd89e8ef23340eb9bb919564dd167", i8);
        gij = a4;
        fmVarArr[i8] = a4;
        int i9 = i8 + 1;
        C2491fm a5 = C4105zY.m41624a(TurretType.class, "237e0bef4961419c6836018ebfd4ff66", i9);
        _f_onResurrect_0020_0028_0029V = a5;
        fmVarArr[i9] = a5;
        int i10 = i9 + 1;
        C2491fm a6 = C4105zY.m41624a(TurretType.class, "aa25ffc462e9cb55c8ee8748197d82e6", i10);
        aDl = a6;
        fmVarArr[i10] = a6;
        int i11 = i10 + 1;
        C2491fm a7 = C4105zY.m41624a(TurretType.class, "e936c234718a2a6f0717b9d19bbd6e44", i11);
        gik = a7;
        fmVarArr[i11] = a7;
        int i12 = i11 + 1;
        C2491fm a8 = C4105zY.m41624a(TurretType.class, "c3c13143cf3371a83e7fd6355c873905", i12);
        gil = a8;
        fmVarArr[i12] = a8;
        int i13 = i12 + 1;
        C2491fm a9 = C4105zY.m41624a(TurretType.class, "5894d89983deefacd8f185b9609d4450", i13);
        gim = a9;
        fmVarArr[i13] = a9;
        int i14 = i13 + 1;
        _m_methods = fmVarArr;
        C1634Xv.m11725a((Object[]) C6454anK._m_methods, (Object[]) _m_methods);
        ___iScriptClass = aUO.m18566a(TurretType.class, C0514HI.class, _m_fields, _m_methods);
    }

    private C6853aut aZI() {
        return (C6853aut) bFf().mo5608dq().mo3214p(dmf);
    }

    private C6853aut aZK() {
        return (C6853aut) bFf().mo5608dq().mo3214p(dmk);
    }

    private C6853aut aZM() {
        return (C6853aut) bFf().mo5608dq().mo3214p(dmn);
    }

    /* renamed from: b */
    private void m24721b(C6853aut aut) {
        bFf().mo5608dq().mo3197f(dmf, aut);
    }

    /* renamed from: c */
    private void m24722c(C6853aut aut) {
        bFf().mo5608dq().mo3197f(dmk, aut);
    }

    @C0064Am(aul = "d11fd89e8ef23340eb9bb919564dd167", aum = 0)
    @C5566aOg
    private void cnB() {
        throw new aWi(new aCE(this, gij, new Object[0]));
    }

    @C5566aOg
    private void cnC() {
        switch (bFf().mo6893i(gij)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, gij, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, gij, new Object[0]));
                break;
        }
        cnB();
    }

    /* renamed from: d */
    private void m24723d(C6853aut aut) {
        bFf().mo5608dq().mo3197f(dmn, aut);
    }

    /* renamed from: I */
    public void mo14926I(Asset tCVar) {
        switch (bFf().mo6893i(aDl)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, aDl, new Object[]{tCVar}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, aDl, new Object[]{tCVar}));
                break;
        }
        m24718H(tCVar);
    }

    /* renamed from: T */
    public C6494any mo11T() {
        return ___iScriptClass;
    }

    /* renamed from: U */
    public C2491fm[] mo12U() {
        return _m_methods;
    }

    /* renamed from: W */
    public Object mo13W() {
        return new C0514HI(this);
    }

    /* renamed from: a */
    public Object mo14a(C0495Gr gr) {
        Object[] args = gr.getArgs();
        switch (gr.mo2417hq() - C6454anK._m_methodCount) {
            case 0:
                return cnv();
            case 1:
                return cnx();
            case 2:
                return cnz();
            case 3:
                cnB();
                return null;
            case 4:
                m24720aG();
                return null;
            case 5:
                m24718H((Asset) args[0]);
                return null;
            case 6:
                m24724g((C6853aut) args[0]);
                return null;
            case 7:
                m24725i((C6853aut) args[0]);
                return null;
            case 8:
                m24726k((C6853aut) args[0]);
                return null;
            default:
                return super.mo14a(gr);
        }
    }

    /* renamed from: a */
    public void mo15a(Collection collection, C0495Gr gr) {
        super.mo15a(collection, gr);
    }

    /* renamed from: aH */
    public void mo70aH() {
        switch (bFf().mo6893i(_f_onResurrect_0020_0028_0029V)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, _f_onResurrect_0020_0028_0029V, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, _f_onResurrect_0020_0028_0029V, new Object[0]));
                break;
        }
        m24720aG();
    }

    /* renamed from: c */
    public C5663aRz[] mo25c() {
        return _m_fields;
    }

    public C6853aut cnA() {
        switch (bFf().mo6893i(gii)) {
            case 0:
                return null;
            case 2:
                return (C6853aut) bFf().mo5606d(new aCE(this, gii, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, gii, new Object[0]));
                break;
        }
        return cnz();
    }

    public C6853aut cnw() {
        switch (bFf().mo6893i(gig)) {
            case 0:
                return null;
            case 2:
                return (C6853aut) bFf().mo5606d(new aCE(this, gig, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, gig, new Object[0]));
                break;
        }
        return cnv();
    }

    public C6853aut cny() {
        switch (bFf().mo6893i(gih)) {
            case 0:
                return null;
            case 2:
                return (C6853aut) bFf().mo5606d(new aCE(this, gih, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, gih, new Object[0]));
                break;
        }
        return cnx();
    }

    /* renamed from: h */
    public void mo15333h(C6853aut aut) {
        switch (bFf().mo6893i(gik)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, gik, new Object[]{aut}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, gik, new Object[]{aut}));
                break;
        }
        m24724g(aut);
    }

    /* renamed from: j */
    public void mo15334j(C6853aut aut) {
        switch (bFf().mo6893i(gil)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, gil, new Object[]{aut}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, gil, new Object[]{aut}));
                break;
        }
        m24725i(aut);
    }

    /* renamed from: l */
    public void mo15335l(C6853aut aut) {
        switch (bFf().mo6893i(gim)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, gim, new Object[]{aut}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, gim, new Object[]{aut}));
                break;
        }
        m24726k(aut);
    }

    /* renamed from: S */
    public void mo10S() {
        super.mo10S();
        m24723d((C6853aut) null);
        m24721b((C6853aut) null);
        m24722c((C6853aut) null);
    }

    @C0064Am(aul = "50c4385e17358af1c793337744ac4e4b", aum = 0)
    private C6853aut cnv() {
        if (aZM() == null) {
            cnC();
        }
        return aZM();
    }

    @C0064Am(aul = "c9e79f6c930d78495472ff7344001886", aum = 0)
    private C6853aut cnx() {
        if (aZI() == null) {
            cnC();
        }
        return aZI();
    }

    @C0064Am(aul = "dfa08e7442f5d411aaeaf889682166de", aum = 0)
    private C6853aut cnz() {
        if (aZK() == null) {
            cnC();
        }
        return aZK();
    }

    @C0064Am(aul = "237e0bef4961419c6836018ebfd4ff66", aum = 0)
    /* renamed from: aG */
    private void m24720aG() {
        super.mo70aH();
        cnC();
    }

    @C0064Am(aul = "aa25ffc462e9cb55c8ee8748197d82e6", aum = 0)
    /* renamed from: H */
    private void m24718H(Asset tCVar) {
        super.mo14926I(tCVar);
        cnC();
    }

    @C0064Am(aul = "e936c234718a2a6f0717b9d19bbd6e44", aum = 0)
    /* renamed from: g */
    private void m24724g(C6853aut aut) {
        m24721b(aut);
    }

    @C0064Am(aul = "c3c13143cf3371a83e7fd6355c873905", aum = 0)
    /* renamed from: i */
    private void m24725i(C6853aut aut) {
        m24723d(aut);
    }

    @C0064Am(aul = "5894d89983deefacd8f185b9609d4450", aum = 0)
    /* renamed from: k */
    private void m24726k(C6853aut aut) {
        m24722c(aut);
    }
}
