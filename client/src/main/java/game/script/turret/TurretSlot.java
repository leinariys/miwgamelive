package game.script.turret;

import java.util.Collection;
import java.util.UUID;

@C5511aMd
@C6485anp
/* renamed from: a.qE */
/* compiled from: a */
public class TurretSlot extends TaikodomObject implements C0468GU, C1616Xf {
    public static final C2491fm _f_toString_0020_0028_0029Ljava_002flang_002fString_003b = null;
    public static final int _m_fieldCount = 0;
    public static final C5663aRz[] _m_fields = null;
    public static final int _m_methodCount = 0;
    public static final C2491fm[] _m_methods = null;
    public static final C5663aRz aWc = null;
    public static final C5663aRz aWd = null;
    public static final C2491fm aWe = null;
    public static final C2491fm aWf = null;
    public static final C2491fm aWg = null;
    public static final C2491fm aWh = null;
    /* renamed from: bL */
    public static final C5663aRz f8885bL = null;
    /* renamed from: bM */
    public static final C5663aRz f8886bM = null;
    /* renamed from: bN */
    public static final C2491fm f8887bN = null;
    /* renamed from: bO */
    public static final C2491fm f8888bO = null;
    /* renamed from: bP */
    public static final C2491fm f8889bP = null;
    /* renamed from: bQ */
    public static final C2491fm f8890bQ = null;
    public static final long serialVersionUID = 0;
    public static C6494any ___iScriptClass;
    @C0064Am(aul = "c4d0b9c325c92ea2fa331d3962edba17", aum = 1)
    private static int aPA;
    @C0064Am(aul = "829c7fb36f3f92337dda5fa97a8b47b4", aum = 0)
    private static C6544aow aPz;
    @C0064Am(aul = "d02fea9e6b1de7133d0c4ea2f51c68b1", aum = 2)

    /* renamed from: bK */
    private static UUID f8884bK;
    @C0064Am(aul = "bfeccb3f18fb52457fe22a931ba61de7", aum = 3)
    private static String handle;

    static {
        m37440V();
    }

    public TurretSlot() {
        super((C5540aNg) null);
        super.mo10S();
    }

    public TurretSlot(C5540aNg ang) {
        super(ang);
    }

    /* renamed from: V */
    static void m37440V() {
        serialVersionUID = (long) 1;
        _m_fieldCount = TaikodomObject._m_fieldCount + 4;
        _m_methodCount = TaikodomObject._m_methodCount + 9;
        int i = TaikodomObject._m_fieldCount;
        C5663aRz[] arzArr = new C5663aRz[(i + 4)];
        C5663aRz b = C5640aRc.m17844b(TurretSlot.class, "829c7fb36f3f92337dda5fa97a8b47b4", i);
        aWc = b;
        arzArr[i] = b;
        int i2 = i + 1;
        C5663aRz b2 = C5640aRc.m17844b(TurretSlot.class, "c4d0b9c325c92ea2fa331d3962edba17", i2);
        aWd = b2;
        arzArr[i2] = b2;
        int i3 = i2 + 1;
        C5663aRz b3 = C5640aRc.m17844b(TurretSlot.class, "d02fea9e6b1de7133d0c4ea2f51c68b1", i3);
        f8885bL = b3;
        arzArr[i3] = b3;
        int i4 = i3 + 1;
        C5663aRz b4 = C5640aRc.m17844b(TurretSlot.class, "bfeccb3f18fb52457fe22a931ba61de7", i4);
        f8886bM = b4;
        arzArr[i4] = b4;
        int i5 = i4 + 1;
        _m_fields = arzArr;
        C1634Xv.m11725a((Object[]) TaikodomObject._m_fields, (Object[]) _m_fields);
        int i6 = TaikodomObject._m_methodCount;
        C2491fm[] fmVarArr = new C2491fm[(i6 + 9)];
        C2491fm a = C4105zY.m41624a(TurretSlot.class, "b7ef518e195cab8b75854d170b67f26c", i6);
        f8887bN = a;
        fmVarArr[i6] = a;
        int i7 = i6 + 1;
        C2491fm a2 = C4105zY.m41624a(TurretSlot.class, "938a25803e2cc7c62a715ddc371760d0", i7);
        f8888bO = a2;
        fmVarArr[i7] = a2;
        int i8 = i7 + 1;
        C2491fm a3 = C4105zY.m41624a(TurretSlot.class, "4bf26347079136b997e5f32a66027eaa", i8);
        f8889bP = a3;
        fmVarArr[i8] = a3;
        int i9 = i8 + 1;
        C2491fm a4 = C4105zY.m41624a(TurretSlot.class, "374900108f47f05d77ae70a1601fb1f8", i9);
        f8890bQ = a4;
        fmVarArr[i9] = a4;
        int i10 = i9 + 1;
        C2491fm a5 = C4105zY.m41624a(TurretSlot.class, "ec45781f58fec0fd3a7d319cab776f99", i10);
        aWe = a5;
        fmVarArr[i10] = a5;
        int i11 = i10 + 1;
        C2491fm a6 = C4105zY.m41624a(TurretSlot.class, "d1a093d76db27929ca10c0206f5eaeb1", i11);
        aWf = a6;
        fmVarArr[i11] = a6;
        int i12 = i11 + 1;
        C2491fm a7 = C4105zY.m41624a(TurretSlot.class, "488341581ca90a2063983ff83bdb8a23", i12);
        aWg = a7;
        fmVarArr[i12] = a7;
        int i13 = i12 + 1;
        C2491fm a8 = C4105zY.m41624a(TurretSlot.class, "acc6c424f0f392ad6c8b9a525a46ae80", i13);
        aWh = a8;
        fmVarArr[i13] = a8;
        int i14 = i13 + 1;
        C2491fm a9 = C4105zY.m41624a(TurretSlot.class, "8a824a113afc3589064847402b2b567a", i14);
        _f_toString_0020_0028_0029Ljava_002flang_002fString_003b = a9;
        fmVarArr[i14] = a9;
        int i15 = i14 + 1;
        _m_methods = fmVarArr;
        C1634Xv.m11725a((Object[]) TaikodomObject._m_methods, (Object[]) _m_methods);
        ___iScriptClass = aUO.m18566a(TurretSlot.class, C3189ou.class, _m_fields, _m_methods);
    }

    /* renamed from: Xs */
    private C6544aow m37441Xs() {
        return (C6544aow) bFf().mo5608dq().mo3214p(aWc);
    }

    /* renamed from: Xt */
    private int m37442Xt() {
        return bFf().mo5608dq().mo3212n(aWd);
    }

    /* renamed from: a */
    private void m37445a(C6544aow aow) {
        bFf().mo5608dq().mo3197f(aWc, aow);
    }

    /* renamed from: a */
    private void m37446a(String str) {
        bFf().mo5608dq().mo3197f(f8886bM, str);
    }

    /* renamed from: a */
    private void m37447a(UUID uuid) {
        bFf().mo5608dq().mo3197f(f8885bL, uuid);
    }

    /* renamed from: an */
    private UUID m37448an() {
        return (UUID) bFf().mo5608dq().mo3214p(f8885bL);
    }

    /* renamed from: ao */
    private String m37449ao() {
        return (String) bFf().mo5608dq().mo3214p(f8886bM);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "TurretType")
    @C0064Am(aul = "d1a093d76db27929ca10c0206f5eaeb1", aum = 0)
    @C5566aOg
    /* renamed from: b */
    private void m37453b(C6544aow aow) {
        throw new aWi(new aCE(this, aWf, new Object[]{aow}));
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Handle")
    @C0064Am(aul = "374900108f47f05d77ae70a1601fb1f8", aum = 0)
    @C5566aOg
    /* renamed from: b */
    private void m37454b(String str) {
        throw new aWi(new aCE(this, f8890bQ, new Object[]{str}));
    }

    /* renamed from: c */
    private void m37456c(UUID uuid) {
        switch (bFf().mo6893i(f8888bO)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f8888bO, new Object[]{uuid}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f8888bO, new Object[]{uuid}));
                break;
        }
        m37455b(uuid);
    }

    /* renamed from: ec */
    private void m37457ec(int i) {
        bFf().mo5608dq().mo3183b(aWd, i);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Slot")
    @C0064Am(aul = "acc6c424f0f392ad6c8b9a525a46ae80", aum = 0)
    @C5566aOg
    /* renamed from: ed */
    private void m37458ed(int i) {
        throw new aWi(new aCE(this, aWh, new Object[]{new Integer(i)}));
    }

    /* renamed from: T */
    public C6494any mo11T() {
        return ___iScriptClass;
    }

    /* renamed from: U */
    public C2491fm[] mo12U() {
        return _m_methods;
    }

    /* renamed from: W */
    public Object mo13W() {
        return new C3189ou(this);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "TurretType")
    /* renamed from: Xv */
    public C6544aow mo21300Xv() {
        switch (bFf().mo6893i(aWe)) {
            case 0:
                return null;
            case 2:
                return (C6544aow) bFf().mo5606d(new aCE(this, aWe, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, aWe, new Object[0]));
                break;
        }
        return m37443Xu();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Slot")
    /* renamed from: Xx */
    public int mo21301Xx() {
        switch (bFf().mo6893i(aWg)) {
            case 0:
                return 0;
            case 2:
                return ((Integer) bFf().mo5606d(new aCE(this, aWg, new Object[0]))).intValue();
            case 3:
                bFf().mo5606d(new aCE(this, aWg, new Object[0]));
                break;
        }
        return m37444Xw();
    }

    /* renamed from: a */
    public Object mo14a(C0495Gr gr) {
        Object[] args = gr.getArgs();
        switch (gr.mo2417hq() - TaikodomObject._m_methodCount) {
            case 0:
                return m37450ap();
            case 1:
                m37455b((UUID) args[0]);
                return null;
            case 2:
                return m37451ar();
            case 3:
                m37454b((String) args[0]);
                return null;
            case 4:
                return m37443Xu();
            case 5:
                m37453b((C6544aow) args[0]);
                return null;
            case 6:
                return new Integer(m37444Xw());
            case 7:
                m37458ed(((Integer) args[0]).intValue());
                return null;
            case 8:
                return m37452au();
            default:
                return super.mo14a(gr);
        }
    }

    /* renamed from: a */
    public void mo15a(Collection collection, C0495Gr gr) {
        super.mo15a(collection, gr);
    }

    /* renamed from: aq */
    public UUID mo18aq() {
        switch (bFf().mo6893i(f8887bN)) {
            case 0:
                return null;
            case 2:
                return (UUID) bFf().mo5606d(new aCE(this, f8887bN, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f8887bN, new Object[0]));
                break;
        }
        return m37450ap();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "TurretType")
    @C5566aOg
    /* renamed from: c */
    public void mo21302c(C6544aow aow) {
        switch (bFf().mo6893i(aWf)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, aWf, new Object[]{aow}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, aWf, new Object[]{aow}));
                break;
        }
        m37453b(aow);
    }

    /* renamed from: c */
    public C5663aRz[] mo25c() {
        return _m_fields;
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Slot")
    @C5566aOg
    /* renamed from: ee */
    public void mo21303ee(int i) {
        switch (bFf().mo6893i(aWh)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, aWh, new Object[]{new Integer(i)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, aWh, new Object[]{new Integer(i)}));
                break;
        }
        m37458ed(i);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Handle")
    public String getHandle() {
        switch (bFf().mo6893i(f8889bP)) {
            case 0:
                return null;
            case 2:
                return (String) bFf().mo5606d(new aCE(this, f8889bP, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f8889bP, new Object[0]));
                break;
        }
        return m37451ar();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Handle")
    @C5566aOg
    public void setHandle(String str) {
        switch (bFf().mo6893i(f8890bQ)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f8890bQ, new Object[]{str}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f8890bQ, new Object[]{str}));
                break;
        }
        m37454b(str);
    }

    public String toString() {
        switch (bFf().mo6893i(_f_toString_0020_0028_0029Ljava_002flang_002fString_003b)) {
            case 0:
                return null;
            case 2:
                return (String) bFf().mo5606d(new aCE(this, _f_toString_0020_0028_0029Ljava_002flang_002fString_003b, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, _f_toString_0020_0028_0029Ljava_002flang_002fString_003b, new Object[0]));
                break;
        }
        return m37452au();
    }

    @C0064Am(aul = "b7ef518e195cab8b75854d170b67f26c", aum = 0)
    /* renamed from: ap */
    private UUID m37450ap() {
        return m37448an();
    }

    @C0064Am(aul = "938a25803e2cc7c62a715ddc371760d0", aum = 0)
    /* renamed from: b */
    private void m37455b(UUID uuid) {
        m37447a(uuid);
    }

    /* renamed from: S */
    public void mo10S() {
        super.mo10S();
        m37447a(UUID.randomUUID());
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Handle")
    @C0064Am(aul = "4bf26347079136b997e5f32a66027eaa", aum = 0)
    /* renamed from: ar */
    private String m37451ar() {
        return m37449ao();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "TurretType")
    @C0064Am(aul = "ec45781f58fec0fd3a7d319cab776f99", aum = 0)
    /* renamed from: Xu */
    private C6544aow m37443Xu() {
        return m37441Xs();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Slot")
    @C0064Am(aul = "488341581ca90a2063983ff83bdb8a23", aum = 0)
    /* renamed from: Xw */
    private int m37444Xw() {
        return m37442Xt();
    }

    @C0064Am(aul = "8a824a113afc3589064847402b2b567a", aum = 0)
    /* renamed from: au */
    private String m37452au() {
        if (m37441Xs() == null) {
            return "null";
        }
        return String.valueOf(m37441Xs().getHandle()) + " in slot " + m37442Xt();
    }
}
