package game.script.turret;

import com.hoplon.geometry.Color;
import com.hoplon.geometry.Vec3f;
import game.CollisionFXSet;
import game.geometry.Matrix4fWrap;
import game.geometry.Quat4fWrap;
import game.geometry.Vec3d;
import game.network.message.externalizable.C6853aut;
import game.network.message.externalizable.aCE;
import game.script.Actor;
import game.script.Pawn;
import game.script.ai.npc.AIController;
import game.script.item.Weapon;
import game.script.login.UserConnection;
import game.script.player.Player;
import game.script.resource.Asset;
import game.script.ship.Hull;
import game.script.ship.Shield;
import game.script.ship.Ship;
import game.script.simulation.Space;
import game.script.space.Node;
import game.script.space.StellarSystem;
import game.script.template.BaseTaikodomContent;
import logic.aaa.C2235dB;
import logic.baa.*;
import logic.bbb.C4029yK;
import logic.bbb.C6348alI;
import logic.data.link.C3168oe;
import logic.data.mbean.C5450aJu;
import logic.render.IEngineGraphics;
import logic.res.code.C5640aRc;
import logic.res.code.C5663aRz;
import logic.res.code.C6494any;
import logic.res.html.C2491fm;
import logic.thred.LogPrinter;
import org.apache.commons.logging.Log;
import p001a.*;
import taikodom.infra.script.I18NString;
import taikodom.render.loader.RenderAsset;
import taikodom.render.scene.RenderObject;
import taikodom.render.scene.Scene;
import taikodom.render.scene.SceneObject;

import java.util.Collection;

@C2712iu(mo19786Bt = BaseTaikodomContent.class, isAbstract = true, typeName = "TurretTypeBase")
@C5511aMd
@C6485anp
/* renamed from: a.JQ */
/* compiled from: a */
public class Turret extends Pawn implements C1616Xf, C1750Zr, C5564aOe, C6661arJ, C2559gm {

    /* renamed from: AG */
    public static final C5663aRz f751AG = null;
    /* renamed from: LR */
    public static final C5663aRz f753LR = null;
    /* renamed from: Lm */
    public static final C2491fm f754Lm = null;
    /* renamed from: Mq */
    public static final C2491fm f755Mq = null;
    /* renamed from: Pf */
    public static final C2491fm f756Pf = null;
    /* renamed from: VW */
    public static final C5663aRz f758VW = null;
    /* renamed from: Wc */
    public static final C5663aRz f760Wc = null;
    /* renamed from: We */
    public static final C5663aRz f762We = null;
    /* renamed from: Wk */
    public static final C2491fm f763Wk = null;
    /* renamed from: Wl */
    public static final C2491fm f764Wl = null;
    /* renamed from: Wm */
    public static final C2491fm f765Wm = null;
    /* renamed from: Wp */
    public static final C2491fm f766Wp = null;
    /* renamed from: Wu */
    public static final C2491fm f767Wu = null;
    public static final C2491fm _f_dispose_0020_0028_0029V = null;
    public static final C2491fm _f_getType_0020_0028_0029Ljava_002flang_002fObject_003b = null;
    public static final C2491fm _f_hasHollowField_0020_0028_0029Z = null;
    public static final C2491fm _f_onResurrect_0020_0028_0029V = null;
    public static final C2491fm _f_step_0020_0028F_0029V = null;
    public static final C2491fm _f_toString_0020_0028_0029Ljava_002flang_002fString_003b = null;
    public static final int _m_fieldCount = 0;
    public static final C5663aRz[] _m_fields = null;
    public static final int _m_methodCount = 0;
    public static final C2491fm[] _m_methods = null;
    public static final C2491fm aTe = null;
    public static final C2491fm aTf = null;
    public static final C5663aRz bnF = null;
    public static final C5663aRz bnP = null;
    public static final C5663aRz boP = null;
    public static final C5663aRz bod = null;
    public static final C2491fm bpJ = null;
    public static final C2491fm bpL = null;
    public static final C2491fm bpN = null;
    public static final C2491fm bpO = null;
    public static final C2491fm bpP = null;
    public static final C2491fm bpx = null;
    public static final C2491fm bqJ = null;
    public static final C2491fm bqO = null;
    public static final C2491fm bqd = null;
    public static final C2491fm bqe = null;
    public static final C2491fm bqh = null;
    public static final C2491fm brA = null;
    public static final C2491fm brR = null;
    public static final C2491fm brS = null;
    public static final C2491fm bri = null;
    public static final C2491fm bsc = null;
    public static final C2491fm bsq = null;
    public static final C2491fm coQ = null;
    public static final C5663aRz dlV = null;
    public static final C5663aRz dlW = null;
    public static final C5663aRz dlY = null;
    public static final C2491fm dmA = null;
    public static final C2491fm dmB = null;
    public static final C2491fm dmC = null;
    public static final C2491fm dmD = null;
    public static final C2491fm dmE = null;
    public static final C2491fm dmF = null;
    public static final C2491fm dmG = null;
    public static final C2491fm dmH = null;
    public static final C2491fm dmI = null;
    public static final C2491fm dmJ = null;
    public static final C2491fm dmK = null;
    public static final C2491fm dmL = null;
    public static final C2491fm dmM = null;
    public static final C2491fm dmN = null;
    public static final C5663aRz dma = null;
    public static final C5663aRz dmc = null;
    public static final C5663aRz dme = null;
    public static final C5663aRz dmf = null;
    public static final C5663aRz dmj = null;
    public static final C5663aRz dmk = null;
    public static final C5663aRz dmm = null;
    public static final C5663aRz dmn = null;
    public static final C2491fm dmo = null;
    public static final C2491fm dmp = null;
    public static final C2491fm dmq = null;
    public static final C2491fm dmr = null;
    public static final C2491fm dms = null;
    public static final C2491fm dmt = null;
    public static final C2491fm dmu = null;
    public static final C2491fm dmv = null;
    public static final C2491fm dmw = null;
    public static final C2491fm dmx = null;
    public static final C2491fm dmy = null;
    public static final C2491fm dmz = null;
    /* renamed from: uU */
    public static final C5663aRz f771uU = null;
    /* renamed from: uY */
    public static final C2491fm f772uY = null;
    /* renamed from: uZ */
    public static final C2491fm f773uZ = null;
    /* renamed from: vc */
    public static final C2491fm f774vc = null;
    /* renamed from: ve */
    public static final C2491fm f775ve = null;
    /* renamed from: vi */
    public static final C2491fm f776vi = null;
    /* renamed from: vm */
    public static final C2491fm f777vm = null;
    /* renamed from: vn */
    public static final C2491fm f778vn = null;
    /* renamed from: vo */
    public static final C2491fm f779vo = null;
    /* renamed from: vs */
    public static final C2491fm f780vs = null;
    private static final Log log = LogPrinter.setClass(Turret.class);

    public static C6494any ___iScriptClass = null;
    @C0064Am(aul = "06b50953535e73f56074a64b3d2aeb7f", aum = 8)

    /* renamed from: LQ */
    private static Asset f752LQ = null;
    @C0064Am(aul = "ca1407e33f95abf4b73f0586565befcd", aum = 13)
    @C0803Ld

    /* renamed from: VV */
    private static Hull f757VV = null;
    @C0064Am(aul = "18b866291739e44fd992728ef6764a48", aum = 2)

    /* renamed from: Wb */
    private static Asset f759Wb = null;
    @C0064Am(aul = "dcc55802fe31146ac3a7dfab41d6c981", aum = 3)

    /* renamed from: Wd */
    private static Asset f761Wd = null;
    @C0064Am(aul = "a5de08ae820252204664f65ebbfdabe3", aum = 17)
    private static Actor ams = null;
    @C0064Am(aul = "37654ef3c1d78cd8786d39a6e152986e", aum = 1)
    private static CollisionFXSet bnE = null;
    @C0064Am(aul = "32196209c66ca81731527a18d589ede1", aum = 7)
    private static Asset boc = null;
    @C0064Am(aul = "876d0a9a5ec733c1cf19e982b4d78eea", aum = 4)

    /* renamed from: dX */
    private static float f768dX = 0.0f;
    @C0064Am(aul = "09cd2897e42eaca288b6d49ee368d59a", aum = 20)
    private static C6853aut dcK = null;
    @C0064Am(aul = "45d3947abce784357294c6034574590b", aum = 14)
    private static C6853aut dcL = null;
    @C0064Am(aul = "4ec1842c9e8eb3b5564f7af42d220996", aum = 16)
    private static C6853aut dcM = null;
    @C0064Am(aul = "c82bdf48929f8a64e00c603e86fff6ba", aum = 0)
    private static C0712KI dlU = null;
    @C0064Am(aul = "8d4ec14a4937ac1374f6d36f4e9ebd9d", aum = 6)
    private static float dlX = 0.0f;
    @C0064Am(aul = "520c0cd302b56ad8b3311da24542949a", aum = 10)
    private static I18NString dlZ = null;
    @C0064Am(aul = "67dd9776c29a68452754b949f32a9109", aum = 11)
    private static Pawn dmb = null;
    @C0064Am(aul = "2a94bdc9a2c1251060e790f5abe7ec26", aum = 12)
    private static C6853aut dmd = null;
    @C0064Am(aul = "9639d5298e8057846561142715c7b97a", aum = 15)
    @C0803Ld
    private static Shield dmi = null;
    @C0064Am(aul = "044df5ab7fe307ca481af58049ad6dad", aum = 18)
    private static AIController dml = null;
    @C0064Am(aul = "40e22df5592d3c6e8868088d69ab8bc0", aum = 5)
    private static float maxPitch = 0.0f;
    @C0064Am(aul = "c80aef915582df7b7cc0df24698d4a90", aum = 19)
    @C0803Ld

    /* renamed from: oX */
    private static Weapon f769oX = null;
    @C0064Am(aul = "c5e08cbacef2a482a2e7657e18002396", aum = 9)

    /* renamed from: uT */
    private static String f770uT;

    static {
        m5612V();
    }

    @ClientOnly
    private transient SceneObject dmg;
    @ClientOnly
    private transient SceneObject dmh;
    private transient C0763Kt stack;

    public Turret() {
        super((C5540aNg) null);
        super.mo10S();
    }

    public Turret(C5540aNg ang) {
        super(ang);
    }

    public Turret(C6454anK ank) {
        super((C5540aNg) null);
        super._m_script_init(ank);
    }

    /* renamed from: V */
    static void m5612V() {
        _m_fieldCount = Pawn._m_fieldCount + 21;
        _m_methodCount = Pawn._m_methodCount + 69;
        int i = Pawn._m_fieldCount;
        C5663aRz[] arzArr = new C5663aRz[(i + 21)];
        C5663aRz b = C5640aRc.m17844b(Turret.class, "c82bdf48929f8a64e00c603e86fff6ba", i);
        dlV = b;
        arzArr[i] = b;
        int i2 = i + 1;
        C5663aRz b2 = C5640aRc.m17844b(Turret.class, "37654ef3c1d78cd8786d39a6e152986e", i2);
        bnF = b2;
        arzArr[i2] = b2;
        int i3 = i2 + 1;
        C5663aRz b3 = C5640aRc.m17844b(Turret.class, "18b866291739e44fd992728ef6764a48", i3);
        f760Wc = b3;
        arzArr[i3] = b3;
        int i4 = i3 + 1;
        C5663aRz b4 = C5640aRc.m17844b(Turret.class, "dcc55802fe31146ac3a7dfab41d6c981", i4);
        f762We = b4;
        arzArr[i4] = b4;
        int i5 = i4 + 1;
        C5663aRz b5 = C5640aRc.m17844b(Turret.class, "876d0a9a5ec733c1cf19e982b4d78eea", i5);
        bnP = b5;
        arzArr[i5] = b5;
        int i6 = i5 + 1;
        C5663aRz b6 = C5640aRc.m17844b(Turret.class, "40e22df5592d3c6e8868088d69ab8bc0", i6);
        dlW = b6;
        arzArr[i6] = b6;
        int i7 = i6 + 1;
        C5663aRz b7 = C5640aRc.m17844b(Turret.class, "8d4ec14a4937ac1374f6d36f4e9ebd9d", i7);
        dlY = b7;
        arzArr[i7] = b7;
        int i8 = i7 + 1;
        C5663aRz b8 = C5640aRc.m17844b(Turret.class, "32196209c66ca81731527a18d589ede1", i8);
        bod = b8;
        arzArr[i8] = b8;
        int i9 = i8 + 1;
        C5663aRz b9 = C5640aRc.m17844b(Turret.class, "06b50953535e73f56074a64b3d2aeb7f", i9);
        f753LR = b9;
        arzArr[i9] = b9;
        int i10 = i9 + 1;
        C5663aRz b10 = C5640aRc.m17844b(Turret.class, "c5e08cbacef2a482a2e7657e18002396", i10);
        f771uU = b10;
        arzArr[i10] = b10;
        int i11 = i10 + 1;
        C5663aRz b11 = C5640aRc.m17844b(Turret.class, "520c0cd302b56ad8b3311da24542949a", i11);
        dma = b11;
        arzArr[i11] = b11;
        int i12 = i11 + 1;
        C5663aRz b12 = C5640aRc.m17844b(Turret.class, "67dd9776c29a68452754b949f32a9109", i12);
        dmc = b12;
        arzArr[i12] = b12;
        int i13 = i12 + 1;
        C5663aRz b13 = C5640aRc.m17844b(Turret.class, "2a94bdc9a2c1251060e790f5abe7ec26", i13);
        dme = b13;
        arzArr[i13] = b13;
        int i14 = i13 + 1;
        C5663aRz b14 = C5640aRc.m17844b(Turret.class, "ca1407e33f95abf4b73f0586565befcd", i14);
        f758VW = b14;
        arzArr[i14] = b14;
        int i15 = i14 + 1;
        C5663aRz b15 = C5640aRc.m17844b(Turret.class, "45d3947abce784357294c6034574590b", i15);
        dmf = b15;
        arzArr[i15] = b15;
        int i16 = i15 + 1;
        C5663aRz b16 = C5640aRc.m17844b(Turret.class, "9639d5298e8057846561142715c7b97a", i16);
        dmj = b16;
        arzArr[i16] = b16;
        int i17 = i16 + 1;
        C5663aRz b17 = C5640aRc.m17844b(Turret.class, "4ec1842c9e8eb3b5564f7af42d220996", i17);
        dmk = b17;
        arzArr[i17] = b17;
        int i18 = i17 + 1;
        C5663aRz b18 = C5640aRc.m17844b(Turret.class, "a5de08ae820252204664f65ebbfdabe3", i18);
        boP = b18;
        arzArr[i18] = b18;
        int i19 = i18 + 1;
        C5663aRz b19 = C5640aRc.m17844b(Turret.class, "044df5ab7fe307ca481af58049ad6dad", i19);
        dmm = b19;
        arzArr[i19] = b19;
        int i20 = i19 + 1;
        C5663aRz b20 = C5640aRc.m17844b(Turret.class, "c80aef915582df7b7cc0df24698d4a90", i20);
        f751AG = b20;
        arzArr[i20] = b20;
        int i21 = i20 + 1;
        C5663aRz b21 = C5640aRc.m17844b(Turret.class, "09cd2897e42eaca288b6d49ee368d59a", i21);
        dmn = b21;
        arzArr[i21] = b21;
        int i22 = i21 + 1;
        _m_fields = arzArr;
        C1634Xv.m11725a((Object[]) Pawn._m_fields, (Object[]) _m_fields);
        int i23 = Pawn._m_methodCount;
        C2491fm[] fmVarArr = new C2491fm[(i23 + 69)];
        C2491fm a = C4105zY.m41624a(Turret.class, "b34a1ea38a806ba4dcc63e2ca2424004", i23);
        bpx = a;
        fmVarArr[i23] = a;
        int i24 = i23 + 1;
        C2491fm a2 = C4105zY.m41624a(Turret.class, "3b68782446c5c121b8c3699de61446bc", i24);
        f777vm = a2;
        fmVarArr[i24] = a2;
        int i25 = i24 + 1;
        C2491fm a3 = C4105zY.m41624a(Turret.class, "214689a9db7e9150ac61e844d88c8734", i25);
        dmo = a3;
        fmVarArr[i25] = a3;
        int i26 = i25 + 1;
        C2491fm a4 = C4105zY.m41624a(Turret.class, "6359f7a270f7fbe6e38d0d12039daea9", i26);
        f778vn = a4;
        fmVarArr[i26] = a4;
        int i27 = i26 + 1;
        C2491fm a5 = C4105zY.m41624a(Turret.class, "41cd897556da85a2c47add94b8c749d5", i27);
        dmp = a5;
        fmVarArr[i27] = a5;
        int i28 = i27 + 1;
        C2491fm a6 = C4105zY.m41624a(Turret.class, "e053c31a14057e542f0fcc0f8015b4c5", i28);
        dmq = a6;
        fmVarArr[i28] = a6;
        int i29 = i28 + 1;
        C2491fm a7 = C4105zY.m41624a(Turret.class, "3b4476a380e0b285f64fb3c1c961a164", i29);
        brR = a7;
        fmVarArr[i29] = a7;
        int i30 = i29 + 1;
        C2491fm a8 = C4105zY.m41624a(Turret.class, "9385397aa626378f0318c3326e0b1b15", i30);
        brS = a8;
        fmVarArr[i30] = a8;
        int i31 = i30 + 1;
        C2491fm a9 = C4105zY.m41624a(Turret.class, "5da557da03329bcf24b805e40f221a10", i31);
        dmr = a9;
        fmVarArr[i31] = a9;
        int i32 = i31 + 1;
        C2491fm a10 = C4105zY.m41624a(Turret.class, "7f81894108ee7ed0032865425ee5b05e", i32);
        _f_dispose_0020_0028_0029V = a10;
        fmVarArr[i32] = a10;
        int i33 = i32 + 1;
        C2491fm a11 = C4105zY.m41624a(Turret.class, "777adf823992628bdf5c281d8ef48b4d", i33);
        dms = a11;
        fmVarArr[i33] = a11;
        int i34 = i33 + 1;
        C2491fm a12 = C4105zY.m41624a(Turret.class, "1b52da56daab7aa19a987f1cf40724a8", i34);
        dmt = a12;
        fmVarArr[i34] = a12;
        int i35 = i34 + 1;
        C2491fm a13 = C4105zY.m41624a(Turret.class, "75aa52f720c643387349a5ac3025b38b", i35);
        bpJ = a13;
        fmVarArr[i35] = a13;
        int i36 = i35 + 1;
        C2491fm a14 = C4105zY.m41624a(Turret.class, "2a0661dad7cc676acec1f2c62e564382", i36);
        bpL = a14;
        fmVarArr[i36] = a14;
        int i37 = i36 + 1;
        C2491fm a15 = C4105zY.m41624a(Turret.class, "5327a3820d38b6b79dab0abace2c3172", i37);
        coQ = a15;
        fmVarArr[i37] = a15;
        int i38 = i37 + 1;
        C2491fm a16 = C4105zY.m41624a(Turret.class, "3fad5ef1a24d8c5ace86953c9ad6685c", i38);
        bpN = a16;
        fmVarArr[i38] = a16;
        int i39 = i38 + 1;
        C2491fm a17 = C4105zY.m41624a(Turret.class, "a112e60b51abb5c7d0089d1c161d2bce", i39);
        bpO = a17;
        fmVarArr[i39] = a17;
        int i40 = i39 + 1;
        C2491fm a18 = C4105zY.m41624a(Turret.class, "8e8f2d6511a4817def97df2d68f64807", i40);
        bpP = a18;
        fmVarArr[i40] = a18;
        int i41 = i40 + 1;
        C2491fm a19 = C4105zY.m41624a(Turret.class, "23c1a6765c56cbc4167faaa028c6976e", i41);
        f764Wl = a19;
        fmVarArr[i41] = a19;
        int i42 = i41 + 1;
        C2491fm a20 = C4105zY.m41624a(Turret.class, "240ec5b570886c9b89a0d2ecd26ff6fe", i42);
        dmu = a20;
        fmVarArr[i42] = a20;
        int i43 = i42 + 1;
        C2491fm a21 = C4105zY.m41624a(Turret.class, "38b01a673732a12f09149b0ca57394be", i43);
        dmv = a21;
        fmVarArr[i43] = a21;
        int i44 = i43 + 1;
        C2491fm a22 = C4105zY.m41624a(Turret.class, "d16021a349ad73d2ccc05de47f325c7d", i44);
        aTe = a22;
        fmVarArr[i44] = a22;
        int i45 = i44 + 1;
        C2491fm a23 = C4105zY.m41624a(Turret.class, "0136dbe81f2be9c45e6e8345229b2a98", i45);
        aTf = a23;
        fmVarArr[i45] = a23;
        int i46 = i45 + 1;
        C2491fm a24 = C4105zY.m41624a(Turret.class, "aa1f7cd7e06f2401dc426b4116c06ebe", i46);
        dmw = a24;
        fmVarArr[i46] = a24;
        int i47 = i46 + 1;
        C2491fm a25 = C4105zY.m41624a(Turret.class, "21ec89b935ac507fd7be06ef2293d1d7", i47);
        f756Pf = a25;
        fmVarArr[i47] = a25;
        int i48 = i47 + 1;
        C2491fm a26 = C4105zY.m41624a(Turret.class, "a18cc8b52907438d1217ead95f086f8c", i48);
        f774vc = a26;
        fmVarArr[i48] = a26;
        int i49 = i48 + 1;
        C2491fm a27 = C4105zY.m41624a(Turret.class, "c0cbdf39040c62f9cd8837026bf1b769", i49);
        f775ve = a27;
        fmVarArr[i49] = a27;
        int i50 = i49 + 1;
        C2491fm a28 = C4105zY.m41624a(Turret.class, "2805ca41fc5f27dec5456c69d75fd52b", i50);
        f779vo = a28;
        fmVarArr[i50] = a28;
        int i51 = i50 + 1;
        C2491fm a29 = C4105zY.m41624a(Turret.class, "935d724bec18a1aa2ec6fcb8a438fad6", i51);
        f765Wm = a29;
        fmVarArr[i51] = a29;
        int i52 = i51 + 1;
        C2491fm a30 = C4105zY.m41624a(Turret.class, "53370461480625282790cfd2b6d36337", i52);
        bqd = a30;
        fmVarArr[i52] = a30;
        int i53 = i52 + 1;
        C2491fm a31 = C4105zY.m41624a(Turret.class, "8f8a4870507ec886a2dac233fe9c3ffe", i53);
        bqe = a31;
        fmVarArr[i53] = a31;
        int i54 = i53 + 1;
        C2491fm a32 = C4105zY.m41624a(Turret.class, "894ad1853b132f1f3028e8a105f030a6", i54);
        dmx = a32;
        fmVarArr[i54] = a32;
        int i55 = i54 + 1;
        C2491fm a33 = C4105zY.m41624a(Turret.class, "c13a467e21900578a6f2133f73523337", i55);
        f776vi = a33;
        fmVarArr[i55] = a33;
        int i56 = i55 + 1;
        C2491fm a34 = C4105zY.m41624a(Turret.class, "a73d540953eac9fdcc401349911fa7fd", i56);
        bqh = a34;
        fmVarArr[i56] = a34;
        int i57 = i56 + 1;
        C2491fm a35 = C4105zY.m41624a(Turret.class, "0fea0137607a3aa971a572f9bc5b0d38", i57);
        dmy = a35;
        fmVarArr[i57] = a35;
        int i58 = i57 + 1;
        C2491fm a36 = C4105zY.m41624a(Turret.class, "2ad28f282c8ed8c6a823836fd6a7e04f", i58);
        dmz = a36;
        fmVarArr[i58] = a36;
        int i59 = i58 + 1;
        C2491fm a37 = C4105zY.m41624a(Turret.class, "9ef3415eaf695f4693cd39d24590771d", i59);
        dmA = a37;
        fmVarArr[i59] = a37;
        int i60 = i59 + 1;
        C2491fm a38 = C4105zY.m41624a(Turret.class, "531c19db8e7fe3441bc71444751fa4f5", i60);
        dmB = a38;
        fmVarArr[i60] = a38;
        int i61 = i60 + 1;
        C2491fm a39 = C4105zY.m41624a(Turret.class, "cc23956c9c5e7b1fbd993df6b6c4f3d7", i61);
        bqJ = a39;
        fmVarArr[i61] = a39;
        int i62 = i61 + 1;
        C2491fm a40 = C4105zY.m41624a(Turret.class, "47498ed518b5deae45a58f82a28d5440", i62);
        dmC = a40;
        fmVarArr[i62] = a40;
        int i63 = i62 + 1;
        C2491fm a41 = C4105zY.m41624a(Turret.class, "e668a63be3780eac7a68329d79cdd0c3", i63);
        f763Wk = a41;
        fmVarArr[i63] = a41;
        int i64 = i63 + 1;
        C2491fm a42 = C4105zY.m41624a(Turret.class, "06f044d23d79dbb3d6fac39e0ede84d0", i64);
        _f_onResurrect_0020_0028_0029V = a42;
        fmVarArr[i64] = a42;
        int i65 = i64 + 1;
        C2491fm a43 = C4105zY.m41624a(Turret.class, "7970c07927a51229879b24ab44f810ac", i65);
        bqO = a43;
        fmVarArr[i65] = a43;
        int i66 = i65 + 1;
        C2491fm a44 = C4105zY.m41624a(Turret.class, "73f62b7aca759db8c020183be3df70a2", i66);
        f773uZ = a44;
        fmVarArr[i66] = a44;
        int i67 = i66 + 1;
        C2491fm a45 = C4105zY.m41624a(Turret.class, "e40636c29036b61b4dfbe13d3e3f8a1d", i67);
        f754Lm = a45;
        fmVarArr[i67] = a45;
        int i68 = i67 + 1;
        C2491fm a46 = C4105zY.m41624a(Turret.class, "18c1f45d9b1677f1c80612f746d21300", i68);
        _f_hasHollowField_0020_0028_0029Z = a46;
        fmVarArr[i68] = a46;
        int i69 = i68 + 1;
        C2491fm a47 = C4105zY.m41624a(Turret.class, "20f761f0aaf57276cda26834270a63d6", i69);
        dmD = a47;
        fmVarArr[i69] = a47;
        int i70 = i69 + 1;
        C2491fm a48 = C4105zY.m41624a(Turret.class, "17562f400ac6771d4187bcdd04c2535c", i70);
        dmE = a48;
        fmVarArr[i70] = a48;
        int i71 = i70 + 1;
        C2491fm a49 = C4105zY.m41624a(Turret.class, "c5a0e766eb8ae4a85911d996cee9593d", i71);
        dmF = a49;
        fmVarArr[i71] = a49;
        int i72 = i71 + 1;
        C2491fm a50 = C4105zY.m41624a(Turret.class, "b337959b732cb53054375a21fffd6dc4", i72);
        bri = a50;
        fmVarArr[i72] = a50;
        int i73 = i72 + 1;
        C2491fm a51 = C4105zY.m41624a(Turret.class, "e4070f7c6c091cb628fe22d563a1d8b7", i73);
        f755Mq = a51;
        fmVarArr[i73] = a51;
        int i74 = i73 + 1;
        C2491fm a52 = C4105zY.m41624a(Turret.class, C3168oe.aME, i74);
        dmG = a52;
        fmVarArr[i74] = a52;
        int i75 = i74 + 1;
        C2491fm a53 = C4105zY.m41624a(Turret.class, "bf1fd203be21228a07dcda4ef204591b", i75);
        brA = a53;
        fmVarArr[i75] = a53;
        int i76 = i75 + 1;
        C2491fm a54 = C4105zY.m41624a(Turret.class, "7d39216c0b3257502a901dff21661f93", i76);
        dmH = a54;
        fmVarArr[i76] = a54;
        int i77 = i76 + 1;
        C2491fm a55 = C4105zY.m41624a(Turret.class, "9af70ae72c7b76c780a56a59455d1408", i77);
        f767Wu = a55;
        fmVarArr[i77] = a55;
        int i78 = i77 + 1;
        C2491fm a56 = C4105zY.m41624a(Turret.class, "f6532a491188feddcbf3e65c998012a4", i78);
        _f_toString_0020_0028_0029Ljava_002flang_002fString_003b = a56;
        fmVarArr[i78] = a56;
        int i79 = i78 + 1;
        C2491fm a57 = C4105zY.m41624a(Turret.class, "a14e9862da77fb1bd27242047e8d1d46", i79);
        dmI = a57;
        fmVarArr[i79] = a57;
        int i80 = i79 + 1;
        C2491fm a58 = C4105zY.m41624a(Turret.class, "9dc5b22ec32d5da319aa127c45ac3003", i80);
        f772uY = a58;
        fmVarArr[i80] = a58;
        int i81 = i80 + 1;
        C2491fm a59 = C4105zY.m41624a(Turret.class, "72b6ed13d79f1d7479978bfdbbd30125", i81);
        dmJ = a59;
        fmVarArr[i81] = a59;
        int i82 = i81 + 1;
        C2491fm a60 = C4105zY.m41624a(Turret.class, "66516e3ef5ed80175fd0ada70de98058", i82);
        f766Wp = a60;
        fmVarArr[i82] = a60;
        int i83 = i82 + 1;
        C2491fm a61 = C4105zY.m41624a(Turret.class, "beabf813bc656889de1098decc5c86b3", i83);
        dmK = a61;
        fmVarArr[i83] = a61;
        int i84 = i83 + 1;
        C2491fm a62 = C4105zY.m41624a(Turret.class, "13bbd3b15a5f6ae979ac7ebd80607e43", i84);
        _f_step_0020_0028F_0029V = a62;
        fmVarArr[i84] = a62;
        int i85 = i84 + 1;
        C2491fm a63 = C4105zY.m41624a(Turret.class, "72a0bb9221b09b1e8f688a787b27bac3", i85);
        dmL = a63;
        fmVarArr[i85] = a63;
        int i86 = i85 + 1;
        C2491fm a64 = C4105zY.m41624a(Turret.class, "c29db78c8057c5484da9888933cd797e", i86);
        dmM = a64;
        fmVarArr[i86] = a64;
        int i87 = i86 + 1;
        C2491fm a65 = C4105zY.m41624a(Turret.class, "c0e14daf97097d788582361f40bbe604", i87);
        f780vs = a65;
        fmVarArr[i87] = a65;
        int i88 = i87 + 1;
        C2491fm a66 = C4105zY.m41624a(Turret.class, "cbdae1910cae6fd32e6ae200a6173256", i88);
        bsc = a66;
        fmVarArr[i88] = a66;
        int i89 = i88 + 1;
        C2491fm a67 = C4105zY.m41624a(Turret.class, "24031980f2ce1217798747c572069d3d", i89);
        dmN = a67;
        fmVarArr[i89] = a67;
        int i90 = i89 + 1;
        C2491fm a68 = C4105zY.m41624a(Turret.class, "597ebd77091fef9a8397f8b2ae154a06", i90);
        _f_getType_0020_0028_0029Ljava_002flang_002fObject_003b = a68;
        fmVarArr[i90] = a68;
        int i91 = i90 + 1;
        C2491fm a69 = C4105zY.m41624a(Turret.class, "f3355cd48fc70b13803cc2a49daf9e9f", i91);
        bsq = a69;
        fmVarArr[i91] = a69;
        int i92 = i91 + 1;
        _m_methods = fmVarArr;
        C1634Xv.m11725a((Object[]) Pawn._m_methods, (Object[]) _m_methods);
        ___iScriptClass = aUO.m18566a(Turret.class, C5450aJu.class, _m_fields, _m_methods);
    }

    /* renamed from: A */
    private void m5608A(Asset tCVar) {
        throw new C6039afL();
    }

    /* renamed from: H */
    private void m5609H(String str) {
        throw new C6039afL();
    }

    /* renamed from: R */
    private void m5611R(Actor cr) {
        bFf().mo5608dq().mo3197f(boP, cr);
    }

    @C4034yP
    @C0064Am(aul = "b337959b732cb53054375a21fffd6dc4", aum = 0)
    @C5566aOg
    /* renamed from: W */
    private void m5615W(Actor cr) {
        throw new aWi(new aCE(this, bri, new Object[]{cr}));
    }

    /* renamed from: W */
    private void m5616W(Asset tCVar) {
        throw new C6039afL();
    }

    /* renamed from: a */
    private void m5621a(AIController jy) {
        bFf().mo5608dq().mo3197f(dmm, jy);
    }

    /* renamed from: a */
    private void m5622a(C0712KI ki) {
        throw new C6039afL();
    }

    /* renamed from: a */
    private void m5623a(Weapon adv) {
        bFf().mo5608dq().mo3197f(f751AG, adv);
    }

    /* renamed from: a */
    private void m5624a(C6853aut aut) {
        bFf().mo5608dq().mo3197f(dme, aut);
    }

    /* renamed from: a */
    private void m5625a(Hull jRVar) {
        bFf().mo5608dq().mo3197f(f758VW, jRVar);
    }

    /* renamed from: a */
    private void m5626a(CollisionFXSet yaVar) {
        throw new C6039afL();
    }

    private C0712KI aZC() {
        return ((C6454anK) getType()).clL();
    }

    private float aZD() {
        return ((C6454anK) getType()).getMaxPitch();
    }

    private float aZE() {
        return ((C6454anK) getType()).clO();
    }

    private I18NString aZF() {
        return ((C6454anK) getType()).clQ();
    }

    private Pawn aZG() {
        return (Pawn) bFf().mo5608dq().mo3214p(dmc);
    }

    private C6853aut aZH() {
        return (C6853aut) bFf().mo5608dq().mo3214p(dme);
    }

    private C6853aut aZI() {
        return (C6853aut) bFf().mo5608dq().mo3214p(dmf);
    }

    private Shield aZJ() {
        return (Shield) bFf().mo5608dq().mo3214p(dmj);
    }

    private C6853aut aZK() {
        return (C6853aut) bFf().mo5608dq().mo3214p(dmk);
    }

    private AIController aZL() {
        return (AIController) bFf().mo5608dq().mo3214p(dmm);
    }

    private C6853aut aZM() {
        return (C6853aut) bFf().mo5608dq().mo3214p(dmn);
    }

    private CollisionFXSet aeJ() {
        return ((C6454anK) getType()).bkb();
    }

    private float aeP() {
        return ((C6454anK) getType()).mo14929VH();
    }

    private Asset aeX() {
        return ((C6454anK) getType()).bkx();
    }

    private Actor afq() {
        return (Actor) bFf().mo5608dq().mo3214p(boP);
    }

    @C0064Am(aul = "f3355cd48fc70b13803cc2a49daf9e9f", aum = 0)
    private C0520HN aiC() {
        return bam();
    }

    /* renamed from: b */
    private void m5631b(C6853aut aut) {
        bFf().mo5608dq().mo3197f(dmf, aut);
    }

    @C0064Am(aul = "24031980f2ce1217798747c572069d3d", aum = 0)
    private Actor bat() {
        return aZS();
    }

    /* renamed from: c */
    private void m5637c(C6853aut aut) {
        bFf().mo5608dq().mo3197f(dmk, aut);
    }

    /* renamed from: c */
    private void m5638c(Shield gjVar) {
        bFf().mo5608dq().mo3197f(dmj, gjVar);
    }

    /* renamed from: d */
    private void m5640d(C6853aut aut) {
        bFf().mo5608dq().mo3197f(dmn, aut);
    }

    /* renamed from: du */
    private void m5641du(float f) {
        throw new C6039afL();
    }

    @C4034yP
    @C5566aOg
    @C0064Am(aul = "9af70ae72c7b76c780a56a59455d1408", aum = 0)
    @C2499fr(mo18857qh = 2)
    /* renamed from: e */
    private void m5642e(Actor cr, C5260aCm acm, Vec3f vec3f, Vec3f vec3f2) {
        throw new aWi(new aCE(this, f767Wu, new Object[]{cr, acm, vec3f, vec3f2}));
    }

    /* renamed from: f */
    private void m5645f(Pawn avi) {
        bFf().mo5608dq().mo3197f(dmc, avi);
    }

    /* renamed from: fN */
    private void m5646fN(float f) {
        throw new C6039afL();
    }

    /* renamed from: fO */
    private void m5647fO(float f) {
        throw new C6039afL();
    }

    @C0064Am(aul = "7f81894108ee7ed0032865425ee5b05e", aum = 0)
    @C5566aOg
    @C2499fr
    /* renamed from: fg */
    private void m5649fg() {
        throw new aWi(new aCE(this, _f_dispose_0020_0028_0029V, new Object[0]));
    }

    /* renamed from: gS */
    private void m5652gS(I18NString i18NString) {
        throw new C6039afL();
    }

    /* renamed from: ij */
    private String m5653ij() {
        return ((C6454anK) getType()).mo14950iu();
    }

    /* renamed from: kk */
    private Weapon m5658kk() {
        return (Weapon) bFf().mo5608dq().mo3214p(f751AG);
    }

    @C0064Am(aul = "597ebd77091fef9a8397f8b2ae154a06", aum = 0)
    /* renamed from: qW */
    private Object m5660qW() {
        return bac();
    }

    /* renamed from: r */
    private void m5661r(Asset tCVar) {
        throw new C6039afL();
    }

    /* renamed from: rh */
    private Asset m5662rh() {
        return ((C6454anK) getType()).mo14928Nu();
    }

    /* renamed from: z */
    private void m5664z(Asset tCVar) {
        throw new C6039afL();
    }

    /* renamed from: zi */
    private Hull m5665zi() {
        return (Hull) bFf().mo5608dq().mo3214p(f758VW);
    }

    /* renamed from: zl */
    private Asset m5666zl() {
        return ((C6454anK) getType()).bkh();
    }

    /* renamed from: zm */
    private Asset m5667zm() {
        return ((C6454anK) getType()).bkj();
    }

    @ClientOnly
    /* renamed from: QW */
    public boolean mo961QW() {
        switch (bFf().mo6893i(_f_hasHollowField_0020_0028_0029Z)) {
            case 0:
                return false;
            case 2:
                return ((Boolean) bFf().mo5606d(new aCE(this, _f_hasHollowField_0020_0028_0029Z, new Object[0]))).booleanValue();
            case 3:
                bFf().mo5606d(new aCE(this, _f_hasHollowField_0020_0028_0029Z, new Object[0]));
                break;
        }
        return m5610QV();
    }

    /* renamed from: T */
    public C6494any mo11T() {
        return ___iScriptClass;
    }

    /* renamed from: U */
    public C2491fm[] mo12U() {
        return _m_methods;
    }

    /* renamed from: VF */
    public float mo962VF() {
        switch (bFf().mo6893i(aTe)) {
            case 0:
                return 0.0f;
            case 2:
                return ((Float) bFf().mo5606d(new aCE(this, aTe, new Object[0]))).floatValue();
            case 3:
                bFf().mo5606d(new aCE(this, aTe, new Object[0]));
                break;
        }
        return m5613VE();
    }

    /* renamed from: VH */
    public float mo963VH() {
        switch (bFf().mo6893i(aTf)) {
            case 0:
                return 0.0f;
            case 2:
                return ((Float) bFf().mo5606d(new aCE(this, aTf, new Object[0]))).floatValue();
            case 3:
                bFf().mo5606d(new aCE(this, aTf, new Object[0]));
                break;
        }
        return m5614VG();
    }

    /* renamed from: W */
    public Object mo13W() {
        return new C5450aJu(this);
    }

    @C4034yP
    @C5566aOg
    /* renamed from: X */
    public void mo2967X(Actor cr) {
        switch (bFf().mo6893i(bri)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, bri, new Object[]{cr}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, bri, new Object[]{cr}));
                break;
        }
        m5615W(cr);
    }

    /* renamed from: a */
    public Object mo14a(C0495Gr gr) {
        Object[] args = gr.getArgs();
        switch (gr.mo2417hq() - Pawn._m_methodCount) {
            case 0:
                afD();
                return null;
            case 1:
                m5619a((Actor) args[0], (C5260aCm) args[1], (Vec3f) args[2], (Vec3f) args[3]);
                return null;
            case 2:
                aZN();
                return null;
            case 3:
                m5635c((Actor) args[0], (C5260aCm) args[1], (Vec3f) args[2], (Vec3f) args[3]);
                return null;
            case 4:
                aZP();
                return null;
            case 5:
                m5639cU(((Boolean) args[0]).booleanValue());
                return null;
            case 6:
                aib();
                return null;
            case 7:
                aid();
                return null;
            case 8:
                m5648fP(((Float) args[0]).floatValue());
                return null;
            case 9:
                m5649fg();
                return null;
            case 10:
                return aZR();
            case 11:
                return aZT();
            case 12:
                return afJ();
            case 13:
                return afM();
            case 14:
                return azV();
            case 15:
                return afQ();
            case 16:
                return afS();
            case 17:
                return afU();
            case 18:
                return m5668zs();
            case 19:
                return aZV();
            case 20:
                return aZW();
            case 21:
                return new Float(m5613VE());
            case 22:
                return new Float(m5614VG());
            case 23:
                return new Float(aZX());
            case 24:
                return m5663uV();
            case 25:
                return m5654io();
            case 26:
                return m5655iq();
            case 27:
                return m5657iz();
            case 28:
                return m5669zu();
            case 29:
                return agu();
            case 30:
                return agw();
            case 31:
                return new Float(aZZ());
            case 32:
                return m5656it();
            case 33:
                return new Float(agC());
            case 34:
                return bab();
            case 35:
                return new Boolean(bad());
            case 36:
                return new Boolean(baf());
            case 37:
                m5627a((Vec3f) args[0], (Node) args[1], (StellarSystem) args[2], (String) args[3]);
                return null;
            case 38:
                m5636c((Vec3d) args[0], (Node) args[1], (StellarSystem) args[2]);
                return null;
            case 39:
                m5643e((Vec3d) args[0], (Node) args[1], (StellarSystem) args[2]);
                return null;
            case 40:
                m5632b((Hull) args[0], (C5260aCm) args[1], (Actor) args[2]);
                return null;
            case 41:
                m5628aG();
                return null;
            case 42:
                ahs();
                return null;
            case 43:
                m5617a(((Long) args[0]).longValue(), ((Boolean) args[1]).booleanValue());
                return null;
            case 44:
                m5659qU();
                return null;
            case 45:
                return new Boolean(m5610QV());
            case 46:
                m5650g((Pawn) args[0]);
                return null;
            case 47:
                m5644e((C6853aut) args[0]);
                return null;
            case 48:
                m5633b((RenderAsset) args[0]);
                return null;
            case 49:
                m5615W((Actor) args[0]);
                return null;
            case 50:
                m5620a((StellarSystem) args[0]);
                return null;
            case 51:
                return new Boolean(m5651g((C1722ZT<UserConnection>) (C1722ZT) args[0]));
            case 52:
                ahD();
                return null;
            case 53:
                bah();
                return null;
            case 54:
                m5642e((Actor) args[0], (C5260aCm) args[1], (Vec3f) args[2], (Vec3f) args[3]);
                return null;
            case 55:
                return m5629au();
            case 56:
                return baj();
            case 57:
                return m5634c((Space) args[0], ((Long) args[1]).longValue());
            case 58:
                return bal();
            case 59:
                m5670zw();
                return null;
            case 60:
                ban();
                return null;
            case 61:
                m5630ay(((Float) args[0]).floatValue());
                return null;
            case 62:
                return new Float(bap());
            case 63:
                return new Float(bar());
            case 64:
                m5618a((Actor.C0200h) args[0]);
                return null;
            case 65:
                return aio();
            case 66:
                return bat();
            case 67:
                return m5660qW();
            case 68:
                return aiC();
            default:
                return super.mo14a(gr);
        }
    }

    /* renamed from: a */
    public void mo2969a(Hull jRVar, C5260aCm acm, Actor cr) {
        switch (bFf().mo6893i(f763Wk)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f763Wk, new Object[]{jRVar, acm, cr}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f763Wk, new Object[]{jRVar, acm, cr}));
                break;
        }
        m5632b(jRVar, acm, cr);
    }

    /* renamed from: a */
    public void mo15a(Collection collection, C0495Gr gr) {
        super.mo15a(collection, gr);
    }

    /* renamed from: a */
    public void mo968a(RenderAsset renderAsset) {
        switch (bFf().mo6893i(dmF)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, dmF, new Object[]{renderAsset}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, dmF, new Object[]{renderAsset}));
                break;
        }
        m5633b(renderAsset);
    }

    /* renamed from: aH */
    public void mo70aH() {
        switch (bFf().mo6893i(_f_onResurrect_0020_0028_0029V)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, _f_onResurrect_0020_0028_0029V, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, _f_onResurrect_0020_0028_0029V, new Object[0]));
                break;
        }
        m5628aG();
    }

    public Vec3f aSf() {
        switch (bFf().mo6893i(dmu)) {
            case 0:
                return null;
            case 2:
                return (Vec3f) bFf().mo5606d(new aCE(this, dmu, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, dmu, new Object[0]));
                break;
        }
        return aZV();
    }

    @ClientOnly
    public void aZO() {
        switch (bFf().mo6893i(dmo)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, dmo, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, dmo, new Object[0]));
                break;
        }
        aZN();
    }

    @ClientOnly
    public void aZQ() {
        switch (bFf().mo6893i(dmp)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, dmp, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, dmp, new Object[0]));
                break;
        }
        aZP();
    }

    public Pawn aZS() {
        switch (bFf().mo6893i(dms)) {
            case 0:
                return null;
            case 2:
                return (Pawn) bFf().mo5606d(new aCE(this, dms, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, dms, new Object[0]));
                break;
        }
        return aZR();
    }

    public C6853aut aZU() {
        switch (bFf().mo6893i(dmt)) {
            case 0:
                return null;
            case 2:
                return (C6853aut) bFf().mo5606d(new aCE(this, dmt, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, dmt, new Object[0]));
                break;
        }
        return aZT();
    }

    public float aZY() {
        switch (bFf().mo6893i(dmw)) {
            case 0:
                return 0.0f;
            case 2:
                return ((Float) bFf().mo5606d(new aCE(this, dmw, new Object[0]))).floatValue();
            case 3:
                bFf().mo5606d(new aCE(this, dmw, new Object[0]));
                break;
        }
        return aZX();
    }

    public void afE() {
        switch (bFf().mo6893i(bpx)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, bpx, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, bpx, new Object[0]));
                break;
        }
        afD();
    }

    public C6853aut afN() {
        switch (bFf().mo6893i(bpL)) {
            case 0:
                return null;
            case 2:
                return (C6853aut) bFf().mo5606d(new aCE(this, bpL, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, bpL, new Object[0]));
                break;
        }
        return afM();
    }

    public Weapon afR() {
        switch (bFf().mo6893i(bpN)) {
            case 0:
                return null;
            case 2:
                return (Weapon) bFf().mo5606d(new aCE(this, bpN, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, bpN, new Object[0]));
                break;
        }
        return afQ();
    }

    public Weapon afT() {
        switch (bFf().mo6893i(bpO)) {
            case 0:
                return null;
            case 2:
                return (Weapon) bFf().mo5606d(new aCE(this, bpO, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, bpO, new Object[0]));
                break;
        }
        return afS();
    }

    public Vec3f afV() {
        switch (bFf().mo6893i(bpP)) {
            case 0:
                return null;
            case 2:
                return (Vec3f) bFf().mo5606d(new aCE(this, bpP, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, bpP, new Object[0]));
                break;
        }
        return afU();
    }

    public float agD() {
        switch (bFf().mo6893i(bqh)) {
            case 0:
                return 0.0f;
            case 2:
                return ((Float) bFf().mo5606d(new aCE(this, bqh, new Object[0]))).floatValue();
            case 3:
                bFf().mo5606d(new aCE(this, bqh, new Object[0]));
                break;
        }
        return agC();
    }

    public Pawn agv() {
        switch (bFf().mo6893i(bqd)) {
            case 0:
                return null;
            case 2:
                return (Pawn) bFf().mo5606d(new aCE(this, bqd, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, bqd, new Object[0]));
                break;
        }
        return agu();
    }

    public Vec3f agx() {
        switch (bFf().mo6893i(bqe)) {
            case 0:
                return null;
            case 2:
                return (Vec3f) bFf().mo5606d(new aCE(this, bqe, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, bqe, new Object[0]));
                break;
        }
        return agw();
    }

    @C4034yP
    @C4114ze
    @C2499fr(mo18855qf = {"be645d6acebea33969e1812f521f744f"})
    public void ahE() {
        switch (bFf().mo6893i(brA)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, brA, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, brA, new Object[0]));
                break;
        }
        ahD();
    }

    /* access modifiers changed from: protected */
    public void aht() {
        switch (bFf().mo6893i(bqO)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, bqO, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, bqO, new Object[0]));
                break;
        }
        ahs();
    }

    public /* bridge */ /* synthetic */ C0520HN aiD() {
        switch (bFf().mo6893i(bsq)) {
            case 0:
                return null;
            case 2:
                return (C0520HN) bFf().mo5606d(new aCE(this, bsq, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, bsq, new Object[0]));
                break;
        }
        return aiC();
    }

    @ClientOnly
    public void aic() {
        switch (bFf().mo6893i(brR)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, brR, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, brR, new Object[0]));
                break;
        }
        aib();
    }

    @ClientOnly
    public void aie() {
        switch (bFf().mo6893i(brS)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, brS, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, brS, new Object[0]));
                break;
        }
        aid();
    }

    public Vec3f aip() {
        switch (bFf().mo6893i(bsc)) {
            case 0:
                return null;
            case 2:
                return (Vec3f) bFf().mo5606d(new aCE(this, bsc, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, bsc, new Object[0]));
                break;
        }
        return aio();
    }

    public Node azW() {
        switch (bFf().mo6893i(coQ)) {
            case 0:
                return null;
            case 2:
                return (Node) bFf().mo5606d(new aCE(this, coQ, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, coQ, new Object[0]));
                break;
        }
        return azV();
    }

    /* renamed from: b */
    public void mo991b(long j, boolean z) {
        switch (bFf().mo6893i(f773uZ)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f773uZ, new Object[]{new Long(j), new Boolean(z)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f773uZ, new Object[]{new Long(j), new Boolean(z)}));
                break;
        }
        m5617a(j, z);
    }

    /* renamed from: b */
    public void mo620b(Actor.C0200h hVar) {
        switch (bFf().mo6893i(f780vs)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f780vs, new Object[]{hVar}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f780vs, new Object[]{hVar}));
                break;
        }
        m5618a(hVar);
    }

    /* renamed from: b */
    public void mo621b(Actor cr, C5260aCm acm, Vec3f vec3f, Vec3f vec3f2) {
        switch (bFf().mo6893i(f777vm)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f777vm, new Object[]{cr, acm, vec3f, vec3f2}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f777vm, new Object[]{cr, acm, vec3f, vec3f2}));
                break;
        }
        m5619a(cr, acm, vec3f, vec3f2);
    }

    /* renamed from: b */
    public void mo993b(StellarSystem jj) {
        switch (bFf().mo6893i(f755Mq)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f755Mq, new Object[]{jj}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f755Mq, new Object[]{jj}));
                break;
        }
        m5620a(jj);
    }

    @C4034yP
    @C2499fr
    @ClientOnly
    /* renamed from: b */
    public void mo2984b(Vec3f vec3f, Node rPVar, StellarSystem jj, String str) {
        switch (bFf().mo6893i(dmB)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, dmB, new Object[]{vec3f, rPVar, jj, str}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, dmB, new Object[]{vec3f, rPVar, jj, str}));
                break;
        }
        m5627a(vec3f, rPVar, jj, str);
    }

    public float baa() {
        switch (bFf().mo6893i(dmx)) {
            case 0:
                return 0.0f;
            case 2:
                return ((Float) bFf().mo5606d(new aCE(this, dmx, new Object[0]))).floatValue();
            case 3:
                bFf().mo5606d(new aCE(this, dmx, new Object[0]));
                break;
        }
        return aZZ();
    }

    public C6544aow bac() {
        switch (bFf().mo6893i(dmy)) {
            case 0:
                return null;
            case 2:
                return (C6544aow) bFf().mo5606d(new aCE(this, dmy, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, dmy, new Object[0]));
                break;
        }
        return bab();
    }

    public boolean bae() {
        switch (bFf().mo6893i(dmz)) {
            case 0:
                return false;
            case 2:
                return ((Boolean) bFf().mo5606d(new aCE(this, dmz, new Object[0]))).booleanValue();
            case 3:
                bFf().mo5606d(new aCE(this, dmz, new Object[0]));
                break;
        }
        return bad();
    }

    @ClientOnly
    public boolean bag() {
        switch (bFf().mo6893i(dmA)) {
            case 0:
                return false;
            case 2:
                return ((Boolean) bFf().mo5606d(new aCE(this, dmA, new Object[0]))).booleanValue();
            case 3:
                bFf().mo5606d(new aCE(this, dmA, new Object[0]));
                break;
        }
        return baf();
    }

    @C4034yP
    @C4114ze
    @C2499fr
    public void bai() {
        switch (bFf().mo6893i(dmH)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, dmH, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, dmH, new Object[0]));
                break;
        }
        bah();
    }

    /* access modifiers changed from: package-private */
    public Turret bak() {
        switch (bFf().mo6893i(dmI)) {
            case 0:
                return null;
            case 2:
                return (Turret) bFf().mo5606d(new aCE(this, dmI, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, dmI, new Object[0]));
                break;
        }
        return baj();
    }

    public C1084Ps bam() {
        switch (bFf().mo6893i(dmJ)) {
            case 0:
                return null;
            case 2:
                return (C1084Ps) bFf().mo5606d(new aCE(this, dmJ, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, dmJ, new Object[0]));
                break;
        }
        return bal();
    }

    @C4034yP
    @C2499fr
    @ClientOnly
    public void bao() {
        switch (bFf().mo6893i(dmK)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, dmK, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, dmK, new Object[0]));
                break;
        }
        ban();
    }

    public float baq() {
        switch (bFf().mo6893i(dmL)) {
            case 0:
                return 0.0f;
            case 2:
                return ((Float) bFf().mo5606d(new aCE(this, dmL, new Object[0]))).floatValue();
            case 3:
                bFf().mo5606d(new aCE(this, dmL, new Object[0]));
                break;
        }
        return bap();
    }

    public float bas() {
        switch (bFf().mo6893i(dmM)) {
            case 0:
                return 0.0f;
            case 2:
                return ((Float) bFf().mo5606d(new aCE(this, dmM, new Object[0]))).floatValue();
            case 3:
                bFf().mo5606d(new aCE(this, dmM, new Object[0]));
                break;
        }
        return bar();
    }

    /* renamed from: c */
    public C5663aRz[] mo25c() {
        return _m_fields;
    }

    /* access modifiers changed from: protected */
    /* renamed from: d */
    public C0520HN mo635d(Space ea, long j) {
        switch (bFf().mo6893i(f772uY)) {
            case 0:
                return null;
            case 2:
                return (C0520HN) bFf().mo5606d(new aCE(this, f772uY, new Object[]{ea, new Long(j)}));
            case 3:
                bFf().mo5606d(new aCE(this, f772uY, new Object[]{ea, new Long(j)}));
                break;
        }
        return m5634c(ea, j);
    }

    /* renamed from: d */
    public void mo636d(Actor cr, C5260aCm acm, Vec3f vec3f, Vec3f vec3f2) {
        switch (bFf().mo6893i(f778vn)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f778vn, new Object[]{cr, acm, vec3f, vec3f2}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f778vn, new Object[]{cr, acm, vec3f, vec3f2}));
                break;
        }
        m5635c(cr, acm, vec3f, vec3f2);
    }

    @C4034yP
    /* renamed from: d */
    public void mo2993d(Vec3d ajr, Node rPVar, StellarSystem jj) {
        switch (bFf().mo6893i(bqJ)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, bqJ, new Object[]{ajr, rPVar, jj}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, bqJ, new Object[]{ajr, rPVar, jj}));
                break;
        }
        m5636c(ajr, rPVar, jj);
    }

    @C5566aOg
    @C2499fr
    public void dispose() {
        switch (bFf().mo6893i(_f_dispose_0020_0028_0029V)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, _f_dispose_0020_0028_0029V, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, _f_dispose_0020_0028_0029V, new Object[0]));
                break;
        }
        m5649fg();
    }

    @C4034yP
    @C5566aOg
    @C2499fr(mo18857qh = 2)
    /* renamed from: f */
    public void mo1064f(Actor cr, C5260aCm acm, Vec3f vec3f, Vec3f vec3f2) {
        switch (bFf().mo6893i(f767Wu)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f767Wu, new Object[]{cr, acm, vec3f, vec3f2}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f767Wu, new Object[]{cr, acm, vec3f, vec3f2}));
                break;
        }
        m5642e(cr, acm, vec3f, vec3f2);
    }

    @C4034yP
    @C2499fr
    @ClientOnly
    /* renamed from: f */
    public void mo2994f(Vec3d ajr, Node rPVar, StellarSystem jj) {
        switch (bFf().mo6893i(dmC)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, dmC, new Object[]{ajr, rPVar, jj}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, dmC, new Object[]{ajr, rPVar, jj}));
                break;
        }
        m5643e(ajr, rPVar, jj);
    }

    /* renamed from: f */
    public void mo2995f(C6853aut aut) {
        switch (bFf().mo6893i(dmE)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, dmE, new Object[]{aut}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, dmE, new Object[]{aut}));
                break;
        }
        m5644e(aut);
    }

    @ClientOnly
    /* renamed from: fQ */
    public void mo1067fQ(float f) {
        switch (bFf().mo6893i(dmr)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, dmr, new Object[]{new Float(f)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, dmr, new Object[]{new Float(f)}));
                break;
        }
        m5648fP(f);
    }

    public String getName() {
        switch (bFf().mo6893i(f756Pf)) {
            case 0:
                return null;
            case 2:
                return (String) bFf().mo5606d(new aCE(this, f756Pf, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f756Pf, new Object[0]));
                break;
        }
        return m5663uV();
    }

    public /* bridge */ /* synthetic */ Object getType() {
        switch (bFf().mo6893i(_f_getType_0020_0028_0029Ljava_002flang_002fObject_003b)) {
            case 0:
                return null;
            case 2:
                return bFf().mo5606d(new aCE(this, _f_getType_0020_0028_0029Ljava_002flang_002fObject_003b, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, _f_getType_0020_0028_0029Ljava_002flang_002fObject_003b, new Object[0]));
                break;
        }
        return m5660qW();
    }

    /* renamed from: h */
    public void mo2996h(Pawn avi) {
        switch (bFf().mo6893i(dmD)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, dmD, new Object[]{avi}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, dmD, new Object[]{avi}));
                break;
        }
        m5650g(avi);
    }

    @C5472aKq
    /* renamed from: h */
    public boolean mo2997h(C1722ZT<UserConnection> zt) {
        switch (bFf().mo6893i(dmG)) {
            case 0:
                return false;
            case 2:
                return ((Boolean) bFf().mo5606d(new aCE(this, dmG, new Object[]{zt}))).booleanValue();
            case 3:
                bFf().mo5606d(new aCE(this, dmG, new Object[]{zt}));
                break;
        }
        return m5651g(zt);
    }

    /* renamed from: hb */
    public Controller mo2998hb() {
        switch (bFf().mo6893i(bpJ)) {
            case 0:
                return null;
            case 2:
                return (Controller) bFf().mo5606d(new aCE(this, bpJ, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, bpJ, new Object[0]));
                break;
        }
        return afJ();
    }

    /* renamed from: iA */
    public String mo647iA() {
        switch (bFf().mo6893i(f779vo)) {
            case 0:
                return null;
            case 2:
                return (String) bFf().mo5606d(new aCE(this, f779vo, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f779vo, new Object[0]));
                break;
        }
        return m5657iz();
    }

    /* renamed from: ip */
    public String mo648ip() {
        switch (bFf().mo6893i(f774vc)) {
            case 0:
                return null;
            case 2:
                return (String) bFf().mo5606d(new aCE(this, f774vc, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f774vc, new Object[0]));
                break;
        }
        return m5654io();
    }

    /* renamed from: ir */
    public String mo649ir() {
        switch (bFf().mo6893i(f775ve)) {
            case 0:
                return null;
            case 2:
                return (String) bFf().mo5606d(new aCE(this, f775ve, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f775ve, new Object[0]));
                break;
        }
        return m5655iq();
    }

    /* renamed from: iu */
    public String mo651iu() {
        switch (bFf().mo6893i(f776vi)) {
            case 0:
                return null;
            case 2:
                return (String) bFf().mo5606d(new aCE(this, f776vi, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f776vi, new Object[0]));
                break;
        }
        return m5656it();
    }

    /* renamed from: qV */
    public void mo656qV() {
        switch (bFf().mo6893i(f754Lm)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f754Lm, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f754Lm, new Object[0]));
                break;
        }
        m5659qU();
    }

    /* renamed from: qZ */
    public Vec3f mo1090qZ() {
        switch (bFf().mo6893i(dmv)) {
            case 0:
                return null;
            case 2:
                return (Vec3f) bFf().mo5606d(new aCE(this, dmv, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, dmv, new Object[0]));
                break;
        }
        return aZW();
    }

    public void setVisible(boolean z) {
        switch (bFf().mo6893i(dmq)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, dmq, new Object[]{new Boolean(z)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, dmq, new Object[]{new Boolean(z)}));
                break;
        }
        m5639cU(z);
    }

    public void step(float f) {
        switch (bFf().mo6893i(_f_step_0020_0028F_0029V)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, _f_step_0020_0028F_0029V, new Object[]{new Float(f)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, _f_step_0020_0028F_0029V, new Object[]{new Float(f)}));
                break;
        }
        m5630ay(f);
    }

    public String toString() {
        switch (bFf().mo6893i(_f_toString_0020_0028_0029Ljava_002flang_002fString_003b)) {
            case 0:
                return null;
            case 2:
                return (String) bFf().mo5606d(new aCE(this, _f_toString_0020_0028_0029Ljava_002flang_002fString_003b, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, _f_toString_0020_0028_0029Ljava_002flang_002fString_003b, new Object[0]));
                break;
        }
        return m5629au();
    }

    /* renamed from: vw */
    public /* bridge */ /* synthetic */ Actor mo2999vw() {
        switch (bFf().mo6893i(dmN)) {
            case 0:
                return null;
            case 2:
                return (Actor) bFf().mo5606d(new aCE(this, dmN, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, dmN, new Object[0]));
                break;
        }
        return bat();
    }

    /* renamed from: zt */
    public Hull mo3000zt() {
        switch (bFf().mo6893i(f764Wl)) {
            case 0:
                return null;
            case 2:
                return (Hull) bFf().mo5606d(new aCE(this, f764Wl, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f764Wl, new Object[0]));
                break;
        }
        return m5668zs();
    }

    /* renamed from: zv */
    public Shield mo3001zv() {
        switch (bFf().mo6893i(f765Wm)) {
            case 0:
                return null;
            case 2:
                return (Shield) bFf().mo5606d(new aCE(this, f765Wm, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f765Wm, new Object[0]));
                break;
        }
        return m5669zu();
    }

    /* renamed from: zx */
    public void mo1099zx() {
        switch (bFf().mo6893i(f766Wp)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f766Wp, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f766Wp, new Object[0]));
                break;
        }
        m5670zw();
    }

    /* renamed from: S */
    public void mo10S() {
        super.mo10S();
    }

    /* renamed from: a */
    public void mo2968a(C6454anK ank) {
        super.mo967a((C2961mJ) ank);
        mo999b((C4029yK) new C6348alI(1.0f));
        if (bHa() && aZC() != null) {
            m5621a((AIController) aZC().mo7459NK());
        }
        if (m5658kk() != null) {
            m5658kk().mo12913c((C6661arJ) this);
        }
        m5640d(bac().cnw());
        m5631b(bac().cny());
        m5637c(bac().cnA());
    }

    @C0064Am(aul = "b34a1ea38a806ba4dcc63e2ca2424004", aum = 0)
    private void afD() {
    }

    @C0064Am(aul = "3b68782446c5c121b8c3699de61446bc", aum = 0)
    /* renamed from: a */
    private void m5619a(Actor cr, C5260aCm acm, Vec3f vec3f, Vec3f vec3f2) {
        mo8360mc("takeClientDamage : causer " + cr + " on " + getName());
        mo1073h(cr, acm, vec3f, (Vec3f) null);
    }

    @C0064Am(aul = "214689a9db7e9150ac61e844d88c8734", aum = 0)
    @ClientOnly
    private void aZN() {
        if (bGX() && ald().getEngineGraphics() != null && cMs().mo959Ls()) {
            String ir = cMs().mo649ir();
            String ip = cMs().mo648ip();
            this.hks = (SceneObject) ald().getLoaderTrail().getAsset(ip);
            if (this.hks == null) {
                ald().getLoaderTrail().mo4995a(ir, ip, (Scene) null, this, "TurretInfra:createClientRenderObject ");
            } else {
                aht();
            }
        }
    }

    @C0064Am(aul = "6359f7a270f7fbe6e38d0d12039daea9", aum = 0)
    /* renamed from: c */
    private void m5635c(Actor cr, C5260aCm acm, Vec3f vec3f, Vec3f vec3f2) {
    }

    @C0064Am(aul = "41cd897556da85a2c47add94b8c749d5", aum = 0)
    @ClientOnly
    private void aZP() {
        IEngineGraphics ale;
        if (bGX() && (ale = ald().getEngineGraphics()) != null) {
            if (this.hks != null) {
                ale.adZ().removeChild(this.hks);
                this.hks.dispose();
                this.hks = null;
            }
            if (this.dmg != null) {
                ale.adZ().removeChild(this.dmg);
                this.dmg.dispose();
                this.dmg = null;
            }
            if (this.dmh != null) {
                ale.adZ().removeChild(this.dmh);
                this.dmh.dispose();
                this.dmh = null;
            }
            if (this.hkg != null) {
                this.hkg.hide();
                this.hkg.destroy();
                this.hkg = null;
            }
        }
    }

    @C0064Am(aul = "e053c31a14057e542f0fcc0f8015b4c5", aum = 0)
    /* renamed from: cU */
    private void m5639cU(boolean z) {
        if (this.dmg != null) {
            if (z) {
                this.dmg.show();
            } else {
                this.dmg.hide();
            }
        }
        if (this.dmh != null) {
            if (z) {
                this.dmh.show();
            } else {
                this.dmh.hide();
            }
        }
        super.setVisible(z);
    }

    @C0064Am(aul = "3b4476a380e0b285f64fb3c1c961a164", aum = 0)
    @ClientOnly
    private void aib() {
        if (this.dmg != null) {
            this.dmg.setRender(false);
        }
        if (this.dmh != null) {
            this.dmh.setRender(false);
        }
        super.aic();
    }

    @C0064Am(aul = "9385397aa626378f0318c3326e0b1b15", aum = 0)
    @ClientOnly
    private void aid() {
        if (this.dmg != null) {
            this.dmg.setRender(true);
        }
        if (this.dmh != null) {
            this.dmh.setRender(true);
        }
        super.aie();
    }

    @C0064Am(aul = "5da557da03329bcf24b805e40f221a10", aum = 0)
    @ClientOnly
    /* renamed from: fP */
    private void m5648fP(float f) {
        if (this.dmg != null && (this.dmg instanceof RenderObject)) {
            RenderObject renderObject = (RenderObject) this.dmg;
            Color primitiveColor = renderObject.getPrimitiveColor();
            primitiveColor.w = f;
            renderObject.setPrimitiveColor(primitiveColor);
        }
        if (this.dmh != null && (this.dmh instanceof RenderObject)) {
            RenderObject renderObject2 = (RenderObject) this.dmh;
            Color primitiveColor2 = renderObject2.getPrimitiveColor();
            primitiveColor2.w = f;
            renderObject2.setPrimitiveColor(primitiveColor2);
        }
        super.mo1067fQ(f);
    }

    @C0064Am(aul = "777adf823992628bdf5c281d8ef48b4d", aum = 0)
    private Pawn aZR() {
        return aZG();
    }

    @C0064Am(aul = "1b52da56daab7aa19a987f1cf40724a8", aum = 0)
    private C6853aut aZT() {
        return aZH();
    }

    @C0064Am(aul = "75aa52f720c643387349a5ac3025b38b", aum = 0)
    private Controller afJ() {
        return aZL();
    }

    @C0064Am(aul = "2a0661dad7cc676acec1f2c62e564382", aum = 0)
    private C6853aut afM() {
        return bac().cnA();
    }

    @C0064Am(aul = "5327a3820d38b6b79dab0abace2c3172", aum = 0)
    private Node azV() {
        return aZG().azW();
    }

    @C0064Am(aul = "3fad5ef1a24d8c5ace86953c9ad6685c", aum = 0)
    private Weapon afQ() {
        return m5658kk();
    }

    @C0064Am(aul = "a112e60b51abb5c7d0089d1c161d2bce", aum = 0)
    private Weapon afS() {
        return null;
    }

    @C0064Am(aul = "8e8f2d6511a4817def97df2d68f64807", aum = 0)
    private Vec3f afU() {
        return new Vec3f(0.0f, 0.0f, 0.0f);
    }

    @C0064Am(aul = "23c1a6765c56cbc4167faaa028c6976e", aum = 0)
    /* renamed from: zs */
    private Hull m5668zs() {
        return m5665zi();
    }

    @C0064Am(aul = "240ec5b570886c9b89a0d2ecd26ff6fe", aum = 0)
    private Vec3f aZV() {
        return bam().aSf();
    }

    @C0064Am(aul = "38b01a673732a12f09149b0ca57394be", aum = 0)
    private Vec3f aZW() {
        return new Vec3f(0.0f, 0.0f, 0.0f);
    }

    @C0064Am(aul = "d16021a349ad73d2ccc05de47f325c7d", aum = 0)
    /* renamed from: VE */
    private float m5613VE() {
        return aeP() * 100.0f;
    }

    @C0064Am(aul = "0136dbe81f2be9c45e6e8345229b2a98", aum = 0)
    /* renamed from: VG */
    private float m5614VG() {
        return aeP();
    }

    @C0064Am(aul = "aa1f7cd7e06f2401dc426b4116c06ebe", aum = 0)
    private float aZX() {
        return 1000000.0f;
    }

    @C0064Am(aul = "21ec89b935ac507fd7be06ef2293d1d7", aum = 0)
    /* renamed from: uV */
    private String m5663uV() {
        return String.valueOf(aZS().getName()) + " turret";
    }

    @C0064Am(aul = "a18cc8b52907438d1217ead95f086f8c", aum = 0)
    /* renamed from: io */
    private String m5654io() {
        return m5662rh().getHandle();
    }

    @C0064Am(aul = "c0cbdf39040c62f9cd8837026bf1b769", aum = 0)
    /* renamed from: iq */
    private String m5655iq() {
        return m5662rh().getFile();
    }

    @C0064Am(aul = "2805ca41fc5f27dec5456c69d75fd52b", aum = 0)
    /* renamed from: iz */
    private String m5657iz() {
        return null;
    }

    @C0064Am(aul = "935d724bec18a1aa2ec6fcb8a438fad6", aum = 0)
    /* renamed from: zu */
    private Shield m5669zu() {
        return aZJ();
    }

    @C0064Am(aul = "53370461480625282790cfd2b6d36337", aum = 0)
    private Pawn agu() {
        return aZG();
    }

    @C0064Am(aul = "8f8a4870507ec886a2dac233fe9c3ffe", aum = 0)
    private Vec3f agw() {
        if (bam() == null) {
            return bac().cnA().mo16402gL();
        }
        return bam().agx();
    }

    @C0064Am(aul = "894ad1853b132f1f3028e8a105f030a6", aum = 0)
    private float aZZ() {
        return 10000.0f;
    }

    @C0064Am(aul = "c13a467e21900578a6f2133f73523337", aum = 0)
    /* renamed from: it */
    private String m5656it() {
        return m5653ij();
    }

    @C0064Am(aul = "a73d540953eac9fdcc401349911fa7fd", aum = 0)
    private float agC() {
        return (float) (((double) 5) * 0.017453292519943295d);
    }

    @C0064Am(aul = "0fea0137607a3aa971a572f9bc5b0d38", aum = 0)
    private C6544aow bab() {
        return (C6544aow) super.getType();
    }

    @C0064Am(aul = "2ad28f282c8ed8c6a823836fd6a7e04f", aum = 0)
    private boolean bad() {
        return aZG().bae() && isAlive();
    }

    @C0064Am(aul = "9ef3415eaf695f4693cd39d24590771d", aum = 0)
    @ClientOnly
    private boolean baf() {
        return aZG().bag() && isAlive();
    }

    @C4034yP
    @ClientOnly
    @C0064Am(aul = "531c19db8e7fe3441bc71444751fa4f5", aum = 0)
    @C2499fr
    /* renamed from: a */
    private void m5627a(Vec3f vec3f, Node rPVar, StellarSystem jj, String str) {
        C6909avx cE;
        if (mo1000b(rPVar, jj) && str != null) {
            C6544aow bac = bak().bac();
            if (bac.bkb() != null && (cE = bac.bkb().mo23146cE(str)) != null) {
                Vec3d ajr = new Vec3d();
                mo1018c(vec3f, ajr);
                new C3257pi().mo21209a(cE.czp(), ajr, C3257pi.C3261d.NEAR_ENOUGH);
                new C3257pi().mo21209a(cE.czr(), ajr, C3257pi.C3261d.NEAR_ENOUGH);
            }
        }
    }

    @C4034yP
    @C0064Am(aul = "cc23956c9c5e7b1fbd993df6b6c4f3d7", aum = 0)
    /* renamed from: c */
    private void m5636c(Vec3d ajr, Node rPVar, StellarSystem jj) {
        mo2994f(ajr, rPVar, jj);
        if (mo2998hb() != null) {
            mo2998hb().aYg();
        }
    }

    @C4034yP
    @ClientOnly
    @C0064Am(aul = "47498ed518b5deae45a58f82a28d5440", aum = 0)
    @C2499fr
    /* renamed from: e */
    private void m5643e(Vec3d ajr, Node rPVar, StellarSystem jj) {
        if (mo1000b(rPVar, jj) && bac() != null) {
            new C3257pi().mo21209a(bac().bkh(), ajr, C3257pi.C3261d.MID_RANGE);
            new C3257pi().mo21209a(bac().bkj(), ajr, C3257pi.C3261d.NEAR_ENOUGH);
        }
    }

    @C0064Am(aul = "e668a63be3780eac7a68329d79cdd0c3", aum = 0)
    /* renamed from: b */
    private void m5632b(Hull jRVar, C5260aCm acm, Actor cr) {
    }

    @C0064Am(aul = "06f044d23d79dbb3d6fac39e0ede84d0", aum = 0)
    /* renamed from: aG */
    private void m5628aG() {
        super.mo70aH();
    }

    @C0064Am(aul = "7970c07927a51229879b24ab44f810ac", aum = 0)
    private void ahs() {
        if (bGX()) {
            if (this.hks == null) {
                log.error("Turret " + this + " " + cWm() + " being loaded on scene before renderObject is created.");
                return;
            }
            this.hks.setPosition(bak().aZU().mo16402gL());
            this.hks.setOrientation(bak().aZU().getOrientation());
            ald().getEngineGraphics().adZ().addChild(this.hks);
            if (this.hks.getChild(0) != null) {
                this.dmg = this.hks.getChild(0);
            }
            if (this.hks.getChild(1) != null) {
                C6853aut cny = bak().bac().cny();
                Matrix4fWrap cow = cny.getOrientation().cow();
                cow.setTranslation(cny.mo16402gL());
                Matrix4fWrap ceI = cow.ceI();
                if (this.hks.getChild(1).getChild(0) != null) {
                    this.hks.getChild(1).getChild(0).setTransform(ceI);
                }
                this.dmh = this.hks.getChild(1);
                this.dmh.setPosition(cny.mo16402gL());
            }
            bak().mo957Fe();
        }
    }

    @C0064Am(aul = "73f62b7aca759db8c020183be3df70a2", aum = 0)
    /* renamed from: a */
    private void m5617a(long j, boolean z) {
        if (this.stack == null) {
            this.stack = C0763Kt.bcE();
        }
        this.stack.bcN().push();
        this.stack.bcI().push();
        this.stack.bcH().push();
        try {
            if (((Ship) aZS()).cLJ() == null) {
                logger.error("Turret with no simulatable ship. This turret will be removed from render module.");
                return;
            }
            if (!(this.hks == null || this.dmg == null || this.dmh == null)) {
                this.hks.setOrientation(bam().bnt());
                this.hks.setPosition(bam().bnx().blk());
                Quat4fWrap bnu = bam().bnu();
                Quat4fWrap bnv = bam().bnv();
                Quat4fWrap aoy = (Quat4fWrap) this.stack.bcN().get();
                aoy.mo15242f(bnu);
                aoy.mul(bnv);
                Vec3f vec3f = (Vec3f) this.stack.bcH().get();
                bnu.mo15204F(bak().bac().cny().mo16402gL(), vec3f);
                this.dmh.setOrientation(aoy);
                this.dmh.setPosition(vec3f);
                this.dmg.setOrientation(bnu);
            }
            this.stack.bcN().pop();
            this.stack.bcI().pop();
            this.stack.bcH().pop();
        } finally {
            this.stack.bcN().pop();
            this.stack.bcI().pop();
            this.stack.bcH().pop();
        }
    }

    @C0064Am(aul = "e40636c29036b61b4dfbe13d3e3f8a1d", aum = 0)
    /* renamed from: qU */
    private void m5659qU() {
        super.mo656qV();
        mo2072j(mo2998hb());
        if (m5665zi() != null) {
            m5665zi().mo19936qV();
        }
        if (aZJ() != null) {
            aZJ().mo19093qV();
        }
        if (m5658kk() != null) {
            m5658kk().mo1959qV();
        }
        if (aZG() != null) {
            aZG().push();
        }
    }

    @C0064Am(aul = "18c1f45d9b1677f1c80612f746d21300", aum = 0)
    @ClientOnly
    /* renamed from: QV */
    private boolean m5610QV() {
        if (super.mo961QW()) {
            return true;
        }
        if (m5665zi() != null && C1298TD.m9830t(m5665zi()).bFT()) {
            return true;
        }
        if (aZJ() != null && C1298TD.m9830t(aZJ()).bFT()) {
            return true;
        }
        if (m5658kk() != null && C1298TD.m9830t(m5658kk()).bFT()) {
            return true;
        }
        if (aZG() == null || !C1298TD.m9830t(aZG()).bFT()) {
            return false;
        }
        return true;
    }

    @C0064Am(aul = "20f761f0aaf57276cda26834270a63d6", aum = 0)
    /* renamed from: g */
    private void m5650g(Pawn avi) {
        m5645f(avi);
    }

    @C0064Am(aul = "17562f400ac6771d4187bcdd04c2535c", aum = 0)
    /* renamed from: e */
    private void m5644e(C6853aut aut) {
        m5624a(aut);
    }

    @C0064Am(aul = "c5a0e766eb8ae4a85911d996cee9593d", aum = 0)
    /* renamed from: b */
    private void m5633b(RenderAsset renderAsset) {
        if (((SceneObject) renderAsset) != null) {
            if (this.hks != null) {
                IEngineGraphics ale = ald().getEngineGraphics();
                if (ale == null) {
                    logger.error(this + " has a renderObject but no renderModule!!");
                } else {
                    ale.adZ().removeChild(this.dmh);
                    ale.adZ().removeChild(this.dmg);
                    ale.adZ().removeChild(this.hks);
                }
                this.hks = null;
                this.dmg = null;
                this.dmh = null;
            }
            this.hks = (SceneObject) renderAsset;
            aht();
            return;
        }
        logger.error("render object not found " + cMs().mo648ip());
    }

    @C0064Am(aul = "e4070f7c6c091cb628fe22d563a1d8b7", aum = 0)
    /* renamed from: a */
    private void m5620a(StellarSystem jj) {
        super.mo993b(jj);
        if (aZL() != null) {
            aZL().mo3293e(this);
            aZL().start();
        }
    }

    @C0064Am(aul = "be645d6acebea33969e1812f521f744f", aum = 0)
    @C5472aKq
    /* renamed from: g */
    private boolean m5651g(C1722ZT<UserConnection> zt) {
        Ship bQx;
        boolean z;
        if (zt == null) {
            mo8358lY("Null replication context. Ignoring replication rule");
            return false;
        }
        UserConnection bFq = zt.bFq();
        if (bFq == null) {
            return true;
        }
        Player dL = bFq.mo15388dL();
        if (dL == null || (bQx = dL.bQx()) == null || bQx.mo960Nc() == null || aZS() == null || aZS().azW() == null) {
            return false;
        }
        if (aZS().azW().mo21606Nc() == null) {
            mo6317hy("this shooter " + aZS() + " is not on stellar system");
            return false;
        }
        Node azW = bQx.azW();
        if (azW == null || azW != aZS().azW()) {
            z = false;
        } else {
            z = true;
        }
        return z;
    }

    @C4034yP
    @C4114ze
    @C0064Am(aul = "bf1fd203be21228a07dcda4ef204591b", aum = 0)
    @C2499fr(mo18855qf = {"be645d6acebea33969e1812f521f744f"})
    private void ahD() {
        if (m5658kk() != null) {
            m5658kk().mo12933cc(afq());
            m5658kk().cDL();
        }
    }

    @C4034yP
    @C4114ze
    @C0064Am(aul = "7d39216c0b3257502a901dff21661f93", aum = 0)
    @C2499fr
    private void bah() {
        if (m5658kk() != null) {
            m5658kk().mo12933cc((Actor) null);
            m5658kk().bai();
        }
    }

    @C0064Am(aul = "f6532a491188feddcbf3e65c998012a4", aum = 0)
    /* renamed from: au */
    private String m5629au() {
        return "Turret: [" + aZF() + "]";
    }

    @C0064Am(aul = "a14e9862da77fb1bd27242047e8d1d46", aum = 0)
    private Turret baj() {
        return this;
    }

    @C0064Am(aul = "9dc5b22ec32d5da319aa127c45ac3003", aum = 0)
    /* renamed from: c */
    private C0520HN m5634c(Space ea, long j) {
        C1084Ps ps = new C1084Ps(ea, this, mo958IL());
        ps.mo2449a(ea.cKn().mo5725a(j, (C2235dB) ps, 13));
        return ps;
    }

    @C0064Am(aul = "72b6ed13d79f1d7479978bfdbbd30125", aum = 0)
    private C1084Ps bal() {
        return (C1084Ps) super.aiD();
    }

    @C0064Am(aul = "66516e3ef5ed80175fd0ada70de98058", aum = 0)
    /* renamed from: zw */
    private void m5670zw() {
        super.mo1099zx();
        if (aZL() != null) {
            aZL().stop();
        }
        if (m5658kk() != null && m5658kk().cUB()) {
            m5658kk().bai();
        }
        if (bHa()) {
            bao();
        }
    }

    @C4034yP
    @ClientOnly
    @C0064Am(aul = "beabf813bc656889de1098decc5c86b3", aum = 0)
    @C2499fr
    private void ban() {
        if (bGX()) {
            aZQ();
        }
    }

    @C0064Am(aul = "13bbd3b15a5f6ae979ac7ebd80607e43", aum = 0)
    /* renamed from: ay */
    private void m5630ay(float f) {
        super.step(f);
        if (m5658kk() != null) {
            m5658kk().step(f);
        }
    }

    @C0064Am(aul = "72a0bb9221b09b1e8f688a787b27bac3", aum = 0)
    private float bap() {
        return bam().baq();
    }

    @C0064Am(aul = "c29db78c8057c5484da9888933cd797e", aum = 0)
    private float bar() {
        return bam().bas();
    }

    @C0064Am(aul = "c0e14daf97097d788582361f40bbe604", aum = 0)
    /* renamed from: a */
    private void m5618a(Actor.C0200h hVar) {
        hVar.mo1103c(new C6348alI(1.0f));
    }

    @C0064Am(aul = "cbdae1910cae6fd32e6ae200a6173256", aum = 0)
    private Vec3f aio() {
        return new Vec3f(0.0f, 0.0f, -1.0f);
    }
}
