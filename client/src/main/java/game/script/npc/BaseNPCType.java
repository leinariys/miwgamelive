package game.script.npc;

import game.network.message.externalizable.aCE;
import game.script.itemgen.ItemGenTable;
import game.script.npcchat.NPCChat;
import game.script.player.Player;
import game.script.resource.Asset;
import game.script.template.BaseTaikodomContent;
import logic.baa.*;
import logic.data.mbean.C2047bX;
import logic.res.code.C5663aRz;
import logic.res.code.C6494any;
import logic.res.html.C2491fm;
import p001a.*;
import taikodom.infra.script.I18NString;

import java.util.Collection;
import java.util.Set;

@C5829abJ("1.1.1")
@C5511aMd
@C6485anp
/* renamed from: a.aSw  reason: case insensitive filesystem */
/* compiled from: a */
public abstract class BaseNPCType extends BaseTaikodomContent implements C1616Xf, aOW, C4068yr {

    /* renamed from: Do */
    public static final C2491fm f3783Do = null;

    /* renamed from: MN */
    public static final C2491fm f3784MN = null;

    /* renamed from: QE */
    public static final C2491fm f3785QE = null;
    public static final C2491fm _f_toString_0020_0028_0029Ljava_002flang_002fString_003b = null;
    public static final int _m_fieldCount = 0;
    public static final C5663aRz[] _m_fields = null;
    public static final int _m_methodCount = 0;
    public static final C2491fm[] _m_methods = null;
    public static final C2491fm aMj = null;
    public static final C2491fm aMp = null;
    public static final C2491fm aMq = null;
    public static final C2491fm aMr = null;
    public static final C2491fm eLi = null;
    public static final C2491fm fmJ = null;
    public static final C2491fm fmL = null;
    public static final C2491fm fmN = null;
    public static final C2491fm fmO = null;
    public static final C2491fm iMR = null;
    public static final long serialVersionUID = 0;
    /* renamed from: zT */
    public static final C2491fm f3786zT = null;
    public static C6494any ___iScriptClass;

    static {
        m18292V();
    }

    public BaseNPCType() {
        super((C5540aNg) null);
        super.mo10S();
    }

    public BaseNPCType(C5540aNg ang) {
        super(ang);
    }

    /* renamed from: V */
    static void m18292V() {
        serialVersionUID = (long) 1;
        _m_fieldCount = BaseTaikodomContent._m_fieldCount + 0;
        _m_methodCount = BaseTaikodomContent._m_methodCount + 15;
        _m_fields = new C5663aRz[(BaseTaikodomContent._m_fieldCount + 0)];
        C1634Xv.m11725a((Object[]) BaseTaikodomContent._m_fields, (Object[]) _m_fields);
        int i = BaseTaikodomContent._m_methodCount;
        C2491fm[] fmVarArr = new C2491fm[(i + 15)];
        C2491fm a = C4105zY.m41624a(BaseNPCType.class, "9966e3e109ac5bdc326d5c4e06086039", i);
        fmL = a;
        fmVarArr[i] = a;
        int i2 = i + 1;
        C2491fm a2 = C4105zY.m41624a(BaseNPCType.class, "e771d0710c10b422d7642f8450f06514", i2);
        fmO = a2;
        fmVarArr[i2] = a2;
        int i3 = i2 + 1;
        C2491fm a3 = C4105zY.m41624a(BaseNPCType.class, "e1d70d72ea948cdcd309c57ffd789f42", i3);
        fmN = a3;
        fmVarArr[i3] = a3;
        int i4 = i3 + 1;
        C2491fm a4 = C4105zY.m41624a(BaseNPCType.class, "a00f4e0bca61de0376210fc637a0d5fc", i4);
        _f_toString_0020_0028_0029Ljava_002flang_002fString_003b = a4;
        fmVarArr[i4] = a4;
        int i5 = i4 + 1;
        C2491fm a5 = C4105zY.m41624a(BaseNPCType.class, "375f6485a267863f28a6f85c77f29786", i5);
        f3783Do = a5;
        fmVarArr[i5] = a5;
        int i6 = i5 + 1;
        C2491fm a6 = C4105zY.m41624a(BaseNPCType.class, "1973aa516ff9dc9565c2d4a2c7a807db", i6);
        eLi = a6;
        fmVarArr[i6] = a6;
        int i7 = i6 + 1;
        C2491fm a7 = C4105zY.m41624a(BaseNPCType.class, "8bce1a873349bf3ad999c1d182dfb28d", i7);
        f3786zT = a7;
        fmVarArr[i7] = a7;
        int i8 = i7 + 1;
        C2491fm a8 = C4105zY.m41624a(BaseNPCType.class, "e1d02cdbec50cfd13cd6a9f9352b3eed", i8);
        f3785QE = a8;
        fmVarArr[i8] = a8;
        int i9 = i8 + 1;
        C2491fm a9 = C4105zY.m41624a(BaseNPCType.class, "95d28d93af7ac25269e3b694e149202f", i9);
        fmJ = a9;
        fmVarArr[i9] = a9;
        int i10 = i9 + 1;
        C2491fm a10 = C4105zY.m41624a(BaseNPCType.class, "56a728272ce9d9213137166f149672f7", i10);
        f3784MN = a10;
        fmVarArr[i10] = a10;
        int i11 = i10 + 1;
        C2491fm a11 = C4105zY.m41624a(BaseNPCType.class, "0bc6e0ca4b0b75a367e310e050879679", i11);
        aMj = a11;
        fmVarArr[i11] = a11;
        int i12 = i11 + 1;
        C2491fm a12 = C4105zY.m41624a(BaseNPCType.class, "4d913aea7120285f96d57049aa84b534", i12);
        aMp = a12;
        fmVarArr[i12] = a12;
        int i13 = i12 + 1;
        C2491fm a13 = C4105zY.m41624a(BaseNPCType.class, "b2aa331852680b66c1e879e0915a0e2e", i13);
        aMq = a13;
        fmVarArr[i13] = a13;
        int i14 = i13 + 1;
        C2491fm a14 = C4105zY.m41624a(BaseNPCType.class, "1d7cc00fcf677e12aed9d0b6d94cb4e9", i14);
        aMr = a14;
        fmVarArr[i14] = a14;
        int i15 = i14 + 1;
        C2491fm a15 = C4105zY.m41624a(BaseNPCType.class, "aa5d0f2cad69be3327d564de9ea6ae0a", i15);
        iMR = a15;
        fmVarArr[i15] = a15;
        int i16 = i15 + 1;
        _m_methods = fmVarArr;
        C1634Xv.m11725a((Object[]) BaseTaikodomContent._m_methods, (Object[]) _m_methods);
        ___iScriptClass = aUO.m18566a(BaseNPCType.class, C2047bX.class, _m_fields, _m_methods);
    }

    @C0064Am(aul = "1d7cc00fcf677e12aed9d0b6d94cb4e9", aum = 0)
    /* renamed from: Sd */
    private Asset m18291Sd() {
        throw new aWi(new aCE(this, aMr, new Object[0]));
    }

    @C0064Am(aul = "1973aa516ff9dc9565c2d4a2c7a807db", aum = 0)
    /* renamed from: a */
    private void m18294a(ItemGenTable oqVar) {
        throw new aWi(new aCE(this, eLi, new Object[]{oqVar}));
    }

    @C0064Am(aul = "e771d0710c10b422d7642f8450f06514", aum = 0)
    /* renamed from: b */
    private void m18296b(C6902avq avq) {
        throw new aWi(new aCE(this, fmO, new Object[]{avq}));
    }

    @C0064Am(aul = "95d28d93af7ac25269e3b694e149202f", aum = 0)
    private String bTH() {
        throw new aWi(new aCE(this, fmJ, new Object[0]));
    }

    @C0064Am(aul = "9966e3e109ac5bdc326d5c4e06086039", aum = 0)
    private NPCChat bTJ() {
        throw new aWi(new aCE(this, fmL, new Object[0]));
    }

    @C0064Am(aul = "e1d70d72ea948cdcd309c57ffd789f42", aum = 0)
    private C6902avq bTL() {
        throw new aWi(new aCE(this, fmN, new Object[0]));
    }

    @C0064Am(aul = "8bce1a873349bf3ad999c1d182dfb28d", aum = 0)
    /* renamed from: kd */
    private I18NString m18298kd() {
        throw new aWi(new aCE(this, f3786zT, new Object[0]));
    }

    @C0064Am(aul = "e1d02cdbec50cfd13cd6a9f9352b3eed", aum = 0)
    /* renamed from: vV */
    private I18NString m18300vV() {
        throw new aWi(new aCE(this, f3785QE, new Object[0]));
    }

    /* renamed from: RU */
    public I18NString mo182RU() {
        switch (bFf().mo6893i(aMj)) {
            case 0:
                return null;
            case 2:
                return (I18NString) bFf().mo5606d(new aCE(this, aMj, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, aMj, new Object[0]));
                break;
        }
        return m18288RT();
    }

    /* renamed from: Sa */
    public String mo185Sa() {
        switch (bFf().mo6893i(aMp)) {
            case 0:
                return null;
            case 2:
                return (String) bFf().mo5606d(new aCE(this, aMp, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, aMp, new Object[0]));
                break;
        }
        return m18289RZ();
    }

    /* renamed from: Sc */
    public String mo11463Sc() {
        switch (bFf().mo6893i(aMq)) {
            case 0:
                return null;
            case 2:
                return (String) bFf().mo5606d(new aCE(this, aMq, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, aMq, new Object[0]));
                break;
        }
        return m18290Sb();
    }

    /* renamed from: Se */
    public Asset mo187Se() {
        switch (bFf().mo6893i(aMr)) {
            case 0:
                return null;
            case 2:
                return (Asset) bFf().mo5606d(new aCE(this, aMr, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, aMr, new Object[0]));
                break;
        }
        return m18291Sd();
    }

    /* renamed from: T */
    public C6494any mo11T() {
        return ___iScriptClass;
    }

    /* renamed from: U */
    public C2491fm[] mo12U() {
        return _m_methods;
    }

    /* renamed from: W */
    public Object mo13W() {
        return new C2047bX(this);
    }

    /* renamed from: a */
    public Object mo14a(C0495Gr gr) {
        Object[] args = gr.getArgs();
        switch (gr.mo2417hq() - BaseTaikodomContent._m_methodCount) {
            case 0:
                return bTJ();
            case 1:
                m18296b((C6902avq) args[0]);
                return null;
            case 2:
                return bTL();
            case 3:
                return m18295au();
            case 4:
                m18293a((C0665JT) args[0]);
                return null;
            case 5:
                m18294a((ItemGenTable) args[0]);
                return null;
            case 6:
                return m18298kd();
            case 7:
                return m18300vV();
            case 8:
                return bTH();
            case 9:
                return m18299rO();
            case 10:
                return m18288RT();
            case 11:
                return m18289RZ();
            case 12:
                return m18290Sb();
            case 13:
                return m18291Sd();
            case 14:
                return m18297cD((Player) args[0]);
            default:
                return super.mo14a(gr);
        }
    }

    /* renamed from: a */
    public void mo15a(Collection collection, C0495Gr gr) {
        super.mo15a(collection, gr);
    }

    /* renamed from: b */
    public void mo24b(C0665JT jt) {
        switch (bFf().mo6893i(f3783Do)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f3783Do, new Object[]{jt}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f3783Do, new Object[]{jt}));
                break;
        }
        m18293a(jt);
    }

    /* renamed from: b */
    public void mo11464b(ItemGenTable oqVar) {
        switch (bFf().mo6893i(eLi)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, eLi, new Object[]{oqVar}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, eLi, new Object[]{oqVar}));
                break;
        }
        m18294a(oqVar);
    }

    public String bTI() {
        switch (bFf().mo6893i(fmJ)) {
            case 0:
                return null;
            case 2:
                return (String) bFf().mo5606d(new aCE(this, fmJ, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, fmJ, new Object[0]));
                break;
        }
        return bTH();
    }

    public NPCChat bTK() {
        switch (bFf().mo6893i(fmL)) {
            case 0:
                return null;
            case 2:
                return (NPCChat) bFf().mo5606d(new aCE(this, fmL, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, fmL, new Object[0]));
                break;
        }
        return bTJ();
    }

    public C6902avq bTM() {
        switch (bFf().mo6893i(fmN)) {
            case 0:
                return null;
            case 2:
                return (C6902avq) bFf().mo5606d(new aCE(this, fmN, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, fmN, new Object[0]));
                break;
        }
        return bTL();
    }

    /* renamed from: c */
    public void mo11468c(C6902avq avq) {
        switch (bFf().mo6893i(fmO)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, fmO, new Object[]{avq}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, fmO, new Object[]{avq}));
                break;
        }
        m18296b(avq);
    }

    /* renamed from: c */
    public C5663aRz[] mo25c() {
        return _m_fields;
    }

    @aFW(cZD = true, value = "Stations")
    /* renamed from: cE */
    public Set<aDJ> mo11469cE(Player aku) {
        switch (bFf().mo6893i(iMR)) {
            case 0:
                return null;
            case 2:
                return (Set) bFf().mo5606d(new aCE(this, iMR, new Object[]{aku}));
            case 3:
                bFf().mo5606d(new aCE(this, iMR, new Object[]{aku}));
                break;
        }
        return m18297cD(aku);
    }

    /* renamed from: ke */
    public I18NString mo11470ke() {
        switch (bFf().mo6893i(f3786zT)) {
            case 0:
                return null;
            case 2:
                return (I18NString) bFf().mo5606d(new aCE(this, f3786zT, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f3786zT, new Object[0]));
                break;
        }
        return m18298kd();
    }

    /* renamed from: rP */
    public I18NString mo195rP() {
        switch (bFf().mo6893i(f3784MN)) {
            case 0:
                return null;
            case 2:
                return (I18NString) bFf().mo5606d(new aCE(this, f3784MN, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f3784MN, new Object[0]));
                break;
        }
        return m18299rO();
    }

    public String toString() {
        switch (bFf().mo6893i(_f_toString_0020_0028_0029Ljava_002flang_002fString_003b)) {
            case 0:
                return null;
            case 2:
                return (String) bFf().mo5606d(new aCE(this, _f_toString_0020_0028_0029Ljava_002flang_002fString_003b, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, _f_toString_0020_0028_0029Ljava_002flang_002fString_003b, new Object[0]));
                break;
        }
        return m18295au();
    }

    /* renamed from: vW */
    public I18NString mo11471vW() {
        switch (bFf().mo6893i(f3785QE)) {
            case 0:
                return null;
            case 2:
                return (I18NString) bFf().mo5606d(new aCE(this, f3785QE, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f3785QE, new Object[0]));
                break;
        }
        return m18300vV();
    }

    /* renamed from: S */
    public void mo10S() {
        super.mo10S();
        mo11468c(C6902avq.cze());
    }

    @C0064Am(aul = "a00f4e0bca61de0376210fc637a0d5fc", aum = 0)
    /* renamed from: au */
    private String m18295au() {
        return "[" + (bTK() == null ? "Non-talking" : "Talking") + "] " + getHandle();
    }

    @C0064Am(aul = "375f6485a267863f28a6f85c77f29786", aum = 0)
    /* renamed from: a */
    private void m18293a(C0665JT jt) {
        C0665JT jt2;
        if ("".equals(jt.getVersion()) && (jt2 = (C0665JT) jt.get("itemGenTable")) != null) {
            mo11464b((ItemGenTable) jt2.baw());
        }
        if (jt.mo3117j(1, 0, 1) && bTM() == null) {
            mo11468c(C6902avq.cze());
        }
        if (jt.mo3117j(1, 1, 1) && !(jt.get("name") instanceof I18NString)) {
            String str = (String) jt.get("name");
            I18NString ke = mo11470ke();
            for (C1584XI code : C1584XI.values()) {
                ke.set(code.getCode(), str);
            }
        }
    }

    @C0064Am(aul = "56a728272ce9d9213137166f149672f7", aum = 0)
    /* renamed from: rO */
    private I18NString m18299rO() {
        return mo11470ke();
    }

    @C0064Am(aul = "0bc6e0ca4b0b75a367e310e050879679", aum = 0)
    /* renamed from: RT */
    private I18NString m18288RT() {
        return mo11471vW();
    }

    @C0064Am(aul = "4d913aea7120285f96d57049aa84b534", aum = 0)
    /* renamed from: RZ */
    private String m18289RZ() {
        return bTI();
    }

    @C0064Am(aul = "b2aa331852680b66c1e879e0915a0e2e", aum = 0)
    /* renamed from: Sb */
    private String m18290Sb() {
        if (mo187Se() != null) {
            return mo187Se().getHandle();
        }
        return null;
    }

    @aFW(cZD = true, value = "Stations")
    @C0064Am(aul = "aa5d0f2cad69be3327d564de9ea6ae0a", aum = 0)
    /* renamed from: cD */
    private Set<aDJ> m18297cD(Player aku) {
        return aku.dyl().mo11984g(this);
    }
}
