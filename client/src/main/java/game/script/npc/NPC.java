package game.script.npc;

import game.network.message.externalizable.aCE;
import game.network.message.serializable.C3438ra;
import game.script.Character;
import game.script.*;
import game.script.ai.npc.AIControllerType;
import game.script.faction.Faction;
import game.script.itemgen.ItemGenTable;
import game.script.nls.NLSHUDSelection;
import game.script.nls.NLSManager;
import game.script.npcchat.NPCChat;
import game.script.pda.DatabaseCategory;
import game.script.pda.TaikopediaEntry;
import game.script.player.Player;
import game.script.progression.CharacterProgression;
import game.script.progression.ProgressionCharacterTemplate;
import game.script.resource.Asset;
import game.script.ship.EquippedShipType;
import game.script.ship.Ship;
import game.script.ship.Station;
import game.script.space.StellarSystem;
import logic.baa.*;
import logic.data.link.C0471GX;
import logic.data.mbean.C2850lB;
import logic.res.code.C5640aRc;
import logic.res.code.C5663aRz;
import logic.res.code.C6494any;
import logic.res.html.C2491fm;
import p001a.*;
import taikodom.infra.script.I18NString;

import java.util.Collection;
import java.util.Collections;
import java.util.List;

@C5829abJ("1.1.1")
@C6485anp
@C2712iu(mo19785Bs = {C0405Fe.class}, mo19786Bt = BaseNPCType.class, version = "1.1.1")
@C5511aMd
/* renamed from: a.aUf  reason: case insensitive filesystem */
/* compiled from: a */
public class NPC extends Character implements C0471GX.C0472a, C1616Xf {
    /* renamed from: BP */
    public static final C5663aRz f3871BP = null;
    /* renamed from: Lj */
    public static final C2491fm f3872Lj = null;
    /* renamed from: Np */
    public static final C5663aRz f3873Np = null;
    /* renamed from: Pf */
    public static final C2491fm f3874Pf = null;
    /* renamed from: QA */
    public static final C5663aRz f3875QA = null;
    public static final C2491fm _f_dispose_0020_0028_0029V = null;
    public static final C2491fm _f_getType_0020_0028_0029Ljava_002flang_002fObject_003b = null;
    public static final C2491fm _f_toString_0020_0028_0029Ljava_002flang_002fString_003b = null;
    public static final int _m_fieldCount = 0;
    public static final C5663aRz[] _m_fields = null;
    public static final int _m_methodCount = 0;
    public static final C2491fm[] _m_methods = null;
    public static final C5663aRz aMg = null;
    public static final C5663aRz aMh = null;
    public static final C5663aRz aMi = null;
    public static final C2491fm aMk = null;
    public static final C2491fm aMn = null;
    public static final C5663aRz awd = null;
    public static final C2491fm brK = null;
    public static final C5663aRz cfD = null;
    public static final C5663aRz cnU = null;
    public static final C2491fm cov = null;
    public static final C2491fm cow = null;
    public static final C5663aRz cpo = null;
    public static final C5663aRz dlV = null;
    public static final C2491fm fgi = null;
    public static final C2491fm fgv = null;
    public static final C2491fm fgw = null;
    public static final C5663aRz fmA = null;
    public static final C5663aRz fmB = null;
    public static final C5663aRz fmC = null;
    public static final C2491fm fmD = null;
    public static final C2491fm fmJ = null;
    public static final C2491fm fmN = null;
    public static final C2491fm fmR = null;
    public static final C5663aRz fmv = null;
    public static final C5663aRz fmw = null;
    public static final C5663aRz fmx = null;
    public static final C5663aRz fmy = null;
    public static final C5663aRz fmz = null;
    public static final C2491fm fnb = null;
    public static final C2491fm frd = null;
    public static final C2491fm fre = null;
    public static final C2491fm hmr = null;
    public static final C2491fm iWA = null;
    public static final C2491fm iWB = null;
    public static final C2491fm iWC = null;
    public static final C2491fm iWD = null;
    public static final C2491fm iWE = null;
    public static final C5663aRz iWw = null;
    public static final C5663aRz iWx = null;
    public static final C2491fm iWy = null;
    public static final C2491fm iWz = null;
    /* renamed from: lm */
    public static final C2491fm f3880lm = null;
    /* renamed from: zQ */
    public static final C5663aRz f3883zQ = null;

    public static C6494any ___iScriptClass = null;
    @C0064Am(aul = "e428cc8702ebd8e6d70450c250116c10", aum = 17)

    /* renamed from: BO */
    private static int f3870BO = 0;
    @C0064Am(aul = "3da629e55109bb5c63567550e9232f53", aum = 11)
    @C2616he("Faction")
    private static Faction axU = null;
    @C0064Am(aul = "310396f909cbd02b944576ca74a22bca", aum = 0)
    @C2616he("Ship")
    private static EquippedShipType ayQ = null;
    @C0064Am(aul = "424c55857ecbe8b36cf7ed09aa744cf7", aum = 1)
    @Deprecated
    private static Class ayR = null;
    @C0064Am(aul = "2ab698219ad5f269f64d783b293291be", aum = 2)
    private static AIControllerType ayS = null;
    @C0064Am(aul = "a865a02b57cb43673d58f5963721d938", aum = 4)
    private static String ayT = null;
    @C0064Am(aul = "0db96c1621b7a5259439c3d8cea460ec", aum = 5)
    private static NPCChat ayU = null;
    @C0064Am(aul = "35058ff732f22e4940eca484a77dbd6d", aum = 6)
    private static C6902avq ayV = null;
    @C0064Am(aul = "f1997c098addc2d976f7e88c5c3f5ece", aum = 7)
    private static ProgressionCharacterTemplate ayW = null;
    @C0064Am(aul = "d434a7fff7fd296a5694f6560482914e", aum = 8)
    @C2616he("Occupation")
    private static I18NString ayX = null;
    @C0064Am(aul = "8a3f03f2fa567f4b287542adf426ee92", aum = 12)
    private static boolean ayY = false;
    @C0064Am(aul = "cd1e1a58d136ff3eff9251544c188890", aum = 13)
    private static C3438ra<ItemGenTable> ayZ = null;
    @C0064Am(aul = "9e69d993e8b91bfe483b4cd4aa4efe45", aum = 14)
    private static long aza = 0;
    @C0064Am(aul = "99d8b6ab18a81579b578de3a49271040", aum = 19)
    private static NPCParty azb = null;
    @C0064Am(aul = "3ec5eb395e148962c1c9cc648ca53bd6", aum = 20)
    private static Player azc = null;
    @C0064Am(aul = "7420934d7dd25e1265f5ae40ab7166ec", aum = 21)
    private static String azd = null;
    @C0064Am(aul = "3e4bff6f3242267d225c2edf306d6080", aum = 15)

    /* renamed from: jS */
    private static DatabaseCategory f3876jS = null;
    @C0064Am(aul = "8ade9a37ad8c37c0b9f60781f1a53455", aum = 10)

    /* renamed from: jT */
    private static C3438ra<TaikopediaEntry> f3877jT = null;
    @C0064Am(aul = "0d615363c7f022ccd11c43e8b1a4199b", aum = 16)

    /* renamed from: jU */
    private static Asset f3878jU = null;
    @C0064Am(aul = "7fd19ed7e9d5c445b1ddb4bdef7c0ccc", aum = 18)

    /* renamed from: jV */
    private static float f3879jV = 0.0f;
    @C0064Am(aul = "3573e0ef9f158c42f8dfc25de591e375", aum = 9)

    /* renamed from: nh */
    private static I18NString f3881nh = null;
    @C0064Am(aul = "df05a683a346bae0ad289e835e8460e8", aum = 3)

    /* renamed from: zP */
    private static I18NString f3882zP;

    static {
        m18636V();
    }

    public NPC() {
        super((C5540aNg) null);
        super.mo10S();
    }

    public NPC(C5540aNg ang) {
        super(ang);
    }

    public NPC(NPCType aed) {
        super((C5540aNg) null);
        super._m_script_init(aed);
    }

    /* renamed from: V */
    static void m18636V() {
        _m_fieldCount = Character._m_fieldCount + 22;
        _m_methodCount = Character._m_methodCount + 29;
        int i = Character._m_fieldCount;
        C5663aRz[] arzArr = new C5663aRz[(i + 22)];
        C5663aRz b = C5640aRc.m17844b(NPC.class, "310396f909cbd02b944576ca74a22bca", i);
        fmv = b;
        arzArr[i] = b;
        int i2 = i + 1;
        C5663aRz b2 = C5640aRc.m17844b(NPC.class, "424c55857ecbe8b36cf7ed09aa744cf7", i2);
        fmw = b2;
        arzArr[i2] = b2;
        int i3 = i2 + 1;
        C5663aRz b3 = C5640aRc.m17844b(NPC.class, "2ab698219ad5f269f64d783b293291be", i3);
        dlV = b3;
        arzArr[i3] = b3;
        int i4 = i3 + 1;
        C5663aRz b4 = C5640aRc.m17844b(NPC.class, "df05a683a346bae0ad289e835e8460e8", i4);
        f3883zQ = b4;
        arzArr[i4] = b4;
        int i5 = i4 + 1;
        C5663aRz b5 = C5640aRc.m17844b(NPC.class, "a865a02b57cb43673d58f5963721d938", i5);
        fmx = b5;
        arzArr[i5] = b5;
        int i6 = i5 + 1;
        C5663aRz b6 = C5640aRc.m17844b(NPC.class, "0db96c1621b7a5259439c3d8cea460ec", i6);
        f3873Np = b6;
        arzArr[i6] = b6;
        int i7 = i6 + 1;
        C5663aRz b7 = C5640aRc.m17844b(NPC.class, "35058ff732f22e4940eca484a77dbd6d", i7);
        fmy = b7;
        arzArr[i7] = b7;
        int i8 = i7 + 1;
        C5663aRz b8 = C5640aRc.m17844b(NPC.class, "f1997c098addc2d976f7e88c5c3f5ece", i8);
        fmz = b8;
        arzArr[i8] = b8;
        int i9 = i8 + 1;
        C5663aRz b9 = C5640aRc.m17844b(NPC.class, "d434a7fff7fd296a5694f6560482914e", i9);
        fmA = b9;
        arzArr[i9] = b9;
        int i10 = i9 + 1;
        C5663aRz b10 = C5640aRc.m17844b(NPC.class, "3573e0ef9f158c42f8dfc25de591e375", i10);
        f3875QA = b10;
        arzArr[i10] = b10;
        int i11 = i10 + 1;
        C5663aRz b11 = C5640aRc.m17844b(NPC.class, "8ade9a37ad8c37c0b9f60781f1a53455", i11);
        awd = b11;
        arzArr[i11] = b11;
        int i12 = i11 + 1;
        C5663aRz b12 = C5640aRc.m17844b(NPC.class, "3da629e55109bb5c63567550e9232f53", i12);
        cnU = b12;
        arzArr[i12] = b12;
        int i13 = i12 + 1;
        C5663aRz b13 = C5640aRc.m17844b(NPC.class, "8a3f03f2fa567f4b287542adf426ee92", i13);
        fmB = b13;
        arzArr[i13] = b13;
        int i14 = i13 + 1;
        C5663aRz b14 = C5640aRc.m17844b(NPC.class, "cd1e1a58d136ff3eff9251544c188890", i14);
        fmC = b14;
        arzArr[i14] = b14;
        int i15 = i14 + 1;
        C5663aRz b15 = C5640aRc.m17844b(NPC.class, "9e69d993e8b91bfe483b4cd4aa4efe45", i15);
        cpo = b15;
        arzArr[i15] = b15;
        int i16 = i15 + 1;
        C5663aRz b16 = C5640aRc.m17844b(NPC.class, "3e4bff6f3242267d225c2edf306d6080", i16);
        aMh = b16;
        arzArr[i16] = b16;
        int i17 = i16 + 1;
        C5663aRz b17 = C5640aRc.m17844b(NPC.class, "0d615363c7f022ccd11c43e8b1a4199b", i17);
        aMg = b17;
        arzArr[i17] = b17;
        int i18 = i17 + 1;
        C5663aRz b18 = C5640aRc.m17844b(NPC.class, "e428cc8702ebd8e6d70450c250116c10", i18);
        f3871BP = b18;
        arzArr[i18] = b18;
        int i19 = i18 + 1;
        C5663aRz b19 = C5640aRc.m17844b(NPC.class, "7fd19ed7e9d5c445b1ddb4bdef7c0ccc", i19);
        aMi = b19;
        arzArr[i19] = b19;
        int i20 = i19 + 1;
        C5663aRz b20 = C5640aRc.m17844b(NPC.class, "99d8b6ab18a81579b578de3a49271040", i20);
        cfD = b20;
        arzArr[i20] = b20;
        int i21 = i20 + 1;
        C5663aRz b21 = C5640aRc.m17844b(NPC.class, "3ec5eb395e148962c1c9cc648ca53bd6", i21);
        iWw = b21;
        arzArr[i21] = b21;
        int i22 = i21 + 1;
        C5663aRz b22 = C5640aRc.m17844b(NPC.class, "7420934d7dd25e1265f5ae40ab7166ec", i22);
        iWx = b22;
        arzArr[i22] = b22;
        int i23 = i22 + 1;
        _m_fields = arzArr;
        C1634Xv.m11725a((Object[]) Character._m_fields, (Object[]) _m_fields);
        int i24 = Character._m_methodCount;
        C2491fm[] fmVarArr = new C2491fm[(i24 + 29)];
        C2491fm a = C4105zY.m41624a(NPC.class, "5dc52b1acc4ef0bee1d35ec7dd4e45a7", i24);
        iWy = a;
        fmVarArr[i24] = a;
        int i25 = i24 + 1;
        C2491fm a2 = C4105zY.m41624a(NPC.class, "974a065d074bf98a0da739af9414799e", i25);
        iWz = a2;
        fmVarArr[i25] = a2;
        int i26 = i25 + 1;
        C2491fm a3 = C4105zY.m41624a(NPC.class, "9dff5a6a6133b01e5a73b033057e7b2c", i26);
        iWA = a3;
        fmVarArr[i26] = a3;
        int i27 = i26 + 1;
        C2491fm a4 = C4105zY.m41624a(NPC.class, "0484ebdf02385736a9a5a2d1e2f9a3a8", i27);
        iWB = a4;
        fmVarArr[i27] = a4;
        int i28 = i27 + 1;
        C2491fm a5 = C4105zY.m41624a(NPC.class, "8facca1c9278dc8390ab5064c4e8172a", i28);
        _f_toString_0020_0028_0029Ljava_002flang_002fString_003b = a5;
        fmVarArr[i28] = a5;
        int i29 = i28 + 1;
        C2491fm a6 = C4105zY.m41624a(NPC.class, "eeed2d66969ccd61c3a57b56435cb18b", i29);
        f3874Pf = a6;
        fmVarArr[i29] = a6;
        int i30 = i29 + 1;
        C2491fm a7 = C4105zY.m41624a(NPC.class, "565330a785316d79562f539b13115053", i30);
        fgi = a7;
        fmVarArr[i30] = a7;
        int i31 = i30 + 1;
        C2491fm a8 = C4105zY.m41624a(NPC.class, "6746d4bf680ac3e5314806b9924d1337", i31);
        fmN = a8;
        fmVarArr[i31] = a8;
        int i32 = i31 + 1;
        C2491fm a9 = C4105zY.m41624a(NPC.class, "d33857aca9c610be38efacef9ae71176", i32);
        fmR = a9;
        fmVarArr[i32] = a9;
        int i33 = i32 + 1;
        C2491fm a10 = C4105zY.m41624a(NPC.class, "eff0caaaed5e72481523183bc88ce609", i33);
        brK = a10;
        fmVarArr[i33] = a10;
        int i34 = i33 + 1;
        C2491fm a11 = C4105zY.m41624a(NPC.class, "70e4646b4296dae19638ef7445e72220", i34);
        _f_dispose_0020_0028_0029V = a11;
        fmVarArr[i34] = a11;
        int i35 = i34 + 1;
        C2491fm a12 = C4105zY.m41624a(NPC.class, "8af2c8e5e076129cf7d11b904e33121c", i35);
        fmJ = a12;
        fmVarArr[i35] = a12;
        int i36 = i35 + 1;
        C2491fm a13 = C4105zY.m41624a(NPC.class, "78eeede0cddcfea6e45687b16f349b20", i36);
        iWC = a13;
        fmVarArr[i36] = a13;
        int i37 = i36 + 1;
        C2491fm a14 = C4105zY.m41624a(NPC.class, "ee7381b5e4bb5aa4e557d9da505f9e14", i37);
        frd = a14;
        fmVarArr[i37] = a14;
        int i38 = i37 + 1;
        C2491fm a15 = C4105zY.m41624a(NPC.class, "135313621b1e6fddd2465dcf3a490276", i38);
        fre = a15;
        fmVarArr[i38] = a15;
        int i39 = i38 + 1;
        C2491fm a16 = C4105zY.m41624a(NPC.class, "7694c8a5b1839c8c3de1649f4dc71e25", i39);
        cov = a16;
        fmVarArr[i39] = a16;
        int i40 = i39 + 1;
        C2491fm a17 = C4105zY.m41624a(NPC.class, "6f48a4aaef9f8d3ce4040b1890ecd897", i40);
        f3872Lj = a17;
        fmVarArr[i40] = a17;
        int i41 = i40 + 1;
        C2491fm a18 = C4105zY.m41624a(NPC.class, "a63c9dbbfec0aa461664ffa106eb8763", i41);
        cow = a18;
        fmVarArr[i41] = a18;
        int i42 = i41 + 1;
        C2491fm a19 = C4105zY.m41624a(NPC.class, "3ce07a24abb00284eff437da4f181fc0", i42);
        iWD = a19;
        fmVarArr[i42] = a19;
        int i43 = i42 + 1;
        C2491fm a20 = C4105zY.m41624a(NPC.class, "d181e562aa089605d43dfb731177ad2d", i43);
        fnb = a20;
        fmVarArr[i43] = a20;
        int i44 = i43 + 1;
        C2491fm a21 = C4105zY.m41624a(NPC.class, "f3ad2ec6ce49eedccc372b2ecc7936dc", i44);
        hmr = a21;
        fmVarArr[i44] = a21;
        int i45 = i44 + 1;
        C2491fm a22 = C4105zY.m41624a(NPC.class, "f0683b71f7d063219841cc5517fa06e9", i45);
        aMk = a22;
        fmVarArr[i45] = a22;
        int i46 = i45 + 1;
        C2491fm a23 = C4105zY.m41624a(NPC.class, "71ca3643ade6b1edc172ea373f1483b4", i46);
        aMn = a23;
        fmVarArr[i46] = a23;
        int i47 = i46 + 1;
        C2491fm a24 = C4105zY.m41624a(NPC.class, "92de2fbcb5a29f59b1429687f9c39ac5", i47);
        f3880lm = a24;
        fmVarArr[i47] = a24;
        int i48 = i47 + 1;
        C2491fm a25 = C4105zY.m41624a(NPC.class, "dfefd8c0461360ef6074b441f96128c1", i48);
        fmD = a25;
        fmVarArr[i48] = a25;
        int i49 = i48 + 1;
        C2491fm a26 = C4105zY.m41624a(NPC.class, "e5da2368699c66db0d11b905c131e42d", i49);
        fgv = a26;
        fmVarArr[i49] = a26;
        int i50 = i49 + 1;
        C2491fm a27 = C4105zY.m41624a(NPC.class, "a9613740df2120f14ef136dd8e26c09f", i50);
        iWE = a27;
        fmVarArr[i50] = a27;
        int i51 = i50 + 1;
        C2491fm a28 = C4105zY.m41624a(NPC.class, "d13831975e76386c605ed330e77747ed", i51);
        fgw = a28;
        fmVarArr[i51] = a28;
        int i52 = i51 + 1;
        C2491fm a29 = C4105zY.m41624a(NPC.class, "7615001c1f3726b0eee43190a08152c6", i52);
        _f_getType_0020_0028_0029Ljava_002flang_002fObject_003b = a29;
        fmVarArr[i52] = a29;
        int i53 = i52 + 1;
        _m_methods = fmVarArr;
        C1634Xv.m11725a((Object[]) Character._m_methods, (Object[]) _m_methods);
        ___iScriptClass = aUO.m18566a(NPC.class, C2850lB.class, _m_fields, _m_methods);
    }

    /* renamed from: L */
    private void m18629L(Asset tCVar) {
        throw new C6039afL();
    }

    /* renamed from: Ll */
    private C3438ra m18630Ll() {
        return ((NPCType) getType()).bgF();
    }

    /* renamed from: RQ */
    private Asset m18631RQ() {
        return ((NPCType) getType()).mo187Se();
    }

    /* renamed from: RR */
    private DatabaseCategory m18632RR() {
        return ((NPCType) getType()).mo184RY();
    }

    /* renamed from: RS */
    private float m18633RS() {
        return ((NPCType) getType()).mo188Sg();
    }

    /* renamed from: a */
    private void m18637a(NPCParty dv) {
        bFf().mo5608dq().mo3197f(cfD, dv);
    }

    /* renamed from: a */
    private void m18638a(NPCChat aaq) {
        throw new C6039afL();
    }

    /* renamed from: a */
    private void m18639a(DatabaseCategory aik) {
        throw new C6039afL();
    }

    /* renamed from: a */
    private void m18641a(C6902avq avq) {
        throw new C6039afL();
    }

    /* renamed from: ad */
    private void m18642ad(Class cls) {
        throw new C6039afL();
    }

    /* renamed from: ar */
    private void m18643ar(int i) {
        throw new C6039afL();
    }

    private Faction azq() {
        return ((NPCType) getType()).azN();
    }

    /* renamed from: bL */
    private void m18646bL(C3438ra raVar) {
        throw new C6039afL();
    }

    @C0064Am(aul = "565330a785316d79562f539b13115053", aum = 0)
    @C5566aOg
    private I18NString bQD() {
        throw new aWi(new aCE(this, fgi, new Object[0]));
    }

    @C0064Am(aul = "e5da2368699c66db0d11b905c131e42d", aum = 0)
    @C5566aOg
    private CharacterProgression bQJ() {
        throw new aWi(new aCE(this, fgv, new Object[0]));
    }

    @C0064Am(aul = "d13831975e76386c605ed330e77747ed", aum = 0)
    @C5566aOg
    private void bQL() {
        throw new aWi(new aCE(this, fgw, new Object[0]));
    }

    private long bTA() {
        return ((NPCType) getType()).bTY();
    }

    private EquippedShipType bTq() {
        return ((NPCType) getType()).bTC();
    }

    private Class bTr() {
        return ((NPCType) getType()).bTE();
    }

    private AIControllerType bTs() {
        return ((NPCType) getType()).bTG();
    }

    private String bTt() {
        return ((NPCType) getType()).bTI();
    }

    private NPCChat bTu() {
        return ((NPCType) getType()).bTK();
    }

    private C6902avq bTv() {
        return ((NPCType) getType()).bTM();
    }

    private ProgressionCharacterTemplate bTw() {
        return ((NPCType) getType()).bTO();
    }

    private I18NString bTx() {
        return ((NPCType) getType()).bTQ();
    }

    private boolean bTy() {
        return ((NPCType) getType()).bTS();
    }

    private C3438ra bTz() {
        return ((NPCType) getType()).bTU();
    }

    private NPCParty bUO() {
        return (NPCParty) bFf().mo5608dq().mo3214p(cfD);
    }

    /* renamed from: br */
    private void m18647br(I18NString i18NString) {
        throw new C6039afL();
    }

    /* renamed from: c */
    private void m18648c(ProgressionCharacterTemplate aof) {
        throw new C6039afL();
    }

    /* renamed from: cx */
    private void m18649cx(float f) {
        throw new C6039afL();
    }

    /* renamed from: d */
    private void m18650d(I18NString i18NString) {
        throw new C6039afL();
    }

    /* renamed from: dd */
    private void m18651dd(Player aku) {
        bFf().mo5608dq().mo3197f(iWw, aku);
    }

    private Player dzy() {
        return (Player) bFf().mo5608dq().mo3214p(iWw);
    }

    private String dzz() {
        return (String) bFf().mo5608dq().mo3214p(iWx);
    }

    /* renamed from: e */
    private void m18653e(AIControllerType tz) {
        throw new C6039afL();
    }

    /* renamed from: e */
    private void m18654e(EquippedShipType gfVar) {
        throw new C6039afL();
    }

    /* renamed from: ed */
    private void m18656ed(boolean z) {
        throw new C6039afL();
    }

    /* renamed from: gW */
    private void m18658gW(long j) {
        throw new C6039afL();
    }

    /* renamed from: iy */
    private void m18659iy(String str) {
        throw new C6039afL();
    }

    /* renamed from: kb */
    private I18NString m18660kb() {
        return ((NPCType) getType()).mo11470ke();
    }

    /* renamed from: l */
    private void m18661l(Faction xm) {
        throw new C6039afL();
    }

    /* renamed from: le */
    private int m18662le() {
        return ((NPCType) getType()).mo13131lt();
    }

    /* renamed from: mi */
    private void m18664mi(I18NString i18NString) {
        throw new C6039afL();
    }

    /* renamed from: nN */
    private void m18665nN(String str) {
        bFf().mo5608dq().mo3197f(iWx, str);
    }

    @C0064Am(aul = "7615001c1f3726b0eee43190a08152c6", aum = 0)
    /* renamed from: qW */
    private Object m18667qW() {
        return mo11654Fs();
    }

    /* renamed from: vT */
    private I18NString m18669vT() {
        return ((NPCType) getType()).mo11471vW();
    }

    /* renamed from: x */
    private void m18670x(C3438ra raVar) {
        throw new C6039afL();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Type")
    /* renamed from: Fs */
    public NPCType mo11654Fs() {
        switch (bFf().mo6893i(iWC)) {
            case 0:
                return null;
            case 2:
                return (NPCType) bFf().mo5606d(new aCE(this, iWC, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, iWC, new Object[0]));
                break;
        }
        return dzE();
    }

    /* renamed from: RW */
    public List<TaikopediaEntry> mo11655RW() {
        switch (bFf().mo6893i(aMk)) {
            case 0:
                return null;
            case 2:
                return (List) bFf().mo5606d(new aCE(this, aMk, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, aMk, new Object[0]));
                break;
        }
        return m18634RV();
    }

    /* renamed from: RY */
    public DatabaseCategory mo11656RY() {
        switch (bFf().mo6893i(aMn)) {
            case 0:
                return null;
            case 2:
                return (DatabaseCategory) bFf().mo5606d(new aCE(this, aMn, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, aMn, new Object[0]));
                break;
        }
        return m18635RX();
    }

    /* renamed from: S */
    public void mo10S() {
        super.mo10S();
    }

    /* renamed from: T */
    public C6494any mo11T() {
        return ___iScriptClass;
    }

    /* renamed from: U */
    public C2491fm[] mo12U() {
        return _m_methods;
    }

    /* renamed from: W */
    public Object mo13W() {
        return new C2850lB(this);
    }

    /* renamed from: a */
    public Object mo14a(C0495Gr gr) {
        Object[] args = gr.getArgs();
        switch (gr.mo2417hq() - Character._m_methodCount) {
            case 0:
                return dzA();
            case 1:
                m18666nO((String) args[0]);
                return null;
            case 2:
                m18652de((Player) args[0]);
                return null;
            case 3:
                return dzC();
            case 4:
                return m18644au();
            case 5:
                return m18668uV();
            case 6:
                return bQD();
            case 7:
                return bTL();
            case 8:
                return bTP();
            case 9:
                return ahP();
            case 10:
                m18657fg();
                return null;
            case 11:
                return bTH();
            case 12:
                return dzE();
            case 13:
                return bUU();
            case 14:
                m18645b((NPCParty) args[0]);
                return null;
            case 15:
                return azM();
            case 16:
                m18640a((Pawn) args[0], (Actor) args[1]);
                return null;
            case 17:
                m18663m((Faction) args[0]);
                return null;
            case 18:
                return new Boolean(dzF());
            case 19:
                return bUb();
            case 20:
                cMB();
                return null;
            case 21:
                return m18634RV();
            case 22:
                return m18635RX();
            case 23:
                return m18655eS();
            case 24:
                return bTB();
            case 25:
                return bQJ();
            case 26:
                return dzH();
            case 27:
                bQL();
                return null;
            case 28:
                return m18667qW();
            default:
                return super.mo14a(gr);
        }
    }

    /* renamed from: a */
    public void mo15a(Collection collection, C0495Gr gr) {
        super.mo15a(collection, gr);
    }

    public List<ItemGenTable> aPl() {
        switch (bFf().mo6893i(fnb)) {
            case 0:
                return null;
            case 2:
                return (List) bFf().mo5606d(new aCE(this, fnb, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, fnb, new Object[0]));
                break;
        }
        return bUb();
    }

    @ClientOnly
    public String ahQ() {
        switch (bFf().mo6893i(brK)) {
            case 0:
                return null;
            case 2:
                return (String) bFf().mo5606d(new aCE(this, brK, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, brK, new Object[0]));
                break;
        }
        return ahP();
    }

    public Faction azN() {
        switch (bFf().mo6893i(cov)) {
            case 0:
                return null;
            case 2:
                return (Faction) bFf().mo5606d(new aCE(this, cov, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, cov, new Object[0]));
                break;
        }
        return azM();
    }

    /* renamed from: b */
    public void mo2353b(Pawn avi, Actor cr) {
        switch (bFf().mo6893i(f3872Lj)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f3872Lj, new Object[]{avi, cr}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f3872Lj, new Object[]{avi, cr}));
                break;
        }
        m18640a(avi, cr);
    }

    @C5566aOg
    public I18NString bQE() {
        switch (bFf().mo6893i(fgi)) {
            case 0:
                return null;
            case 2:
                return (I18NString) bFf().mo5606d(new aCE(this, fgi, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, fgi, new Object[0]));
                break;
        }
        return bQD();
    }

    @C5566aOg
    public CharacterProgression bQK() {
        switch (bFf().mo6893i(fgv)) {
            case 0:
                return null;
            case 2:
                return (CharacterProgression) bFf().mo5606d(new aCE(this, fgv, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, fgv, new Object[0]));
                break;
        }
        return bQJ();
    }

    @C5566aOg
    public void bQM() {
        switch (bFf().mo6893i(fgw)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, fgw, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, fgw, new Object[0]));
                break;
        }
        bQL();
    }

    public EquippedShipType bTC() {
        switch (bFf().mo6893i(fmD)) {
            case 0:
                return null;
            case 2:
                return (EquippedShipType) bFf().mo5606d(new aCE(this, fmD, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, fmD, new Object[0]));
                break;
        }
        return bTB();
    }

    public String bTI() {
        switch (bFf().mo6893i(fmJ)) {
            case 0:
                return null;
            case 2:
                return (String) bFf().mo5606d(new aCE(this, fmJ, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, fmJ, new Object[0]));
                break;
        }
        return bTH();
    }

    public C6902avq bTM() {
        switch (bFf().mo6893i(fmN)) {
            case 0:
                return null;
            case 2:
                return (C6902avq) bFf().mo5606d(new aCE(this, fmN, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, fmN, new Object[0]));
                break;
        }
        return bTL();
    }

    public I18NString bTQ() {
        switch (bFf().mo6893i(fmR)) {
            case 0:
                return null;
            case 2:
                return (I18NString) bFf().mo5606d(new aCE(this, fmR, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, fmR, new Object[0]));
                break;
        }
        return bTP();
    }

    public NPCParty bUV() {
        switch (bFf().mo6893i(frd)) {
            case 0:
                return null;
            case 2:
                return (NPCParty) bFf().mo5606d(new aCE(this, frd, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, frd, new Object[0]));
                break;
        }
        return bUU();
    }

    /* renamed from: c */
    public void mo11668c(NPCParty dv) {
        switch (bFf().mo6893i(fre)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, fre, new Object[]{dv}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, fre, new Object[]{dv}));
                break;
        }
        m18645b(dv);
    }

    /* renamed from: c */
    public C5663aRz[] mo25c() {
        return _m_fields;
    }

    public void cMC() {
        switch (bFf().mo6893i(hmr)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, hmr, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, hmr, new Object[0]));
                break;
        }
        cMB();
    }

    /* renamed from: df */
    public void mo11670df(Player aku) {
        switch (bFf().mo6893i(iWA)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, iWA, new Object[]{aku}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, iWA, new Object[]{aku}));
                break;
        }
        m18652de(aku);
    }

    public void dispose() {
        switch (bFf().mo6893i(_f_dispose_0020_0028_0029V)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, _f_dispose_0020_0028_0029V, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, _f_dispose_0020_0028_0029V, new Object[0]));
                break;
        }
        m18657fg();
    }

    public String dzB() {
        switch (bFf().mo6893i(iWy)) {
            case 0:
                return null;
            case 2:
                return (String) bFf().mo5606d(new aCE(this, iWy, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, iWy, new Object[0]));
                break;
        }
        return dzA();
    }

    public Player dzD() {
        switch (bFf().mo6893i(iWB)) {
            case 0:
                return null;
            case 2:
                return (Player) bFf().mo5606d(new aCE(this, iWB, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, iWB, new Object[0]));
                break;
        }
        return dzC();
    }

    public boolean dzG() {
        switch (bFf().mo6893i(iWD)) {
            case 0:
                return false;
            case 2:
                return ((Boolean) bFf().mo5606d(new aCE(this, iWD, new Object[0]))).booleanValue();
            case 3:
                bFf().mo5606d(new aCE(this, iWD, new Object[0]));
                break;
        }
        return dzF();
    }

    public NPCView dzI() {
        switch (bFf().mo6893i(iWE)) {
            case 0:
                return null;
            case 2:
                return (NPCView) bFf().mo5606d(new aCE(this, iWE, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, iWE, new Object[0]));
                break;
        }
        return dzH();
    }

    /* renamed from: eT */
    public Station mo11675eT() {
        switch (bFf().mo6893i(f3880lm)) {
            case 0:
                return null;
            case 2:
                return (Station) bFf().mo5606d(new aCE(this, f3880lm, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f3880lm, new Object[0]));
                break;
        }
        return m18655eS();
    }

    public String getName() {
        switch (bFf().mo6893i(f3874Pf)) {
            case 0:
                return null;
            case 2:
                return (String) bFf().mo5606d(new aCE(this, f3874Pf, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f3874Pf, new Object[0]));
                break;
        }
        return m18668uV();
    }

    public /* bridge */ /* synthetic */ Object getType() {
        switch (bFf().mo6893i(_f_getType_0020_0028_0029Ljava_002flang_002fObject_003b)) {
            case 0:
                return null;
            case 2:
                return bFf().mo5606d(new aCE(this, _f_getType_0020_0028_0029Ljava_002flang_002fObject_003b, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, _f_getType_0020_0028_0029Ljava_002flang_002fObject_003b, new Object[0]));
                break;
        }
        return m18667qW();
    }

    /* renamed from: n */
    public void mo11676n(Faction xm) {
        switch (bFf().mo6893i(cow)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, cow, new Object[]{xm}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, cow, new Object[]{xm}));
                break;
        }
        m18663m(xm);
    }

    /* renamed from: nP */
    public void mo11677nP(String str) {
        switch (bFf().mo6893i(iWz)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, iWz, new Object[]{str}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, iWz, new Object[]{str}));
                break;
        }
        m18666nO(str);
    }

    public String toString() {
        switch (bFf().mo6893i(_f_toString_0020_0028_0029Ljava_002flang_002fString_003b)) {
            case 0:
                return null;
            case 2:
                return (String) bFf().mo5606d(new aCE(this, _f_toString_0020_0028_0029Ljava_002flang_002fString_003b, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, _f_toString_0020_0028_0029Ljava_002flang_002fString_003b, new Object[0]));
                break;
        }
        return m18644au();
    }

    @C0064Am(aul = "5dc52b1acc4ef0bee1d35ec7dd4e45a7", aum = 0)
    private String dzA() {
        return dzz();
    }

    @C0064Am(aul = "974a065d074bf98a0da739af9414799e", aum = 0)
    /* renamed from: nO */
    private void m18666nO(String str) {
        m18665nN(str);
    }

    /* renamed from: y */
    public void mo11678y(NPCType aed) {
        super.mo967a((C2961mJ) aed);
        if (bTs() != null) {
            mo12649c((Controller) bTs().mo7459NK());
        } else if (bTr() != null) {
            mo12649c((Controller) mo8363n(bTr()));
        }
        LDScriptingController lYVar = (LDScriptingController) bFf().mo6865M(LDScriptingController.class);
        lYVar.mo10S();
        mo12654e(lYVar);
        bQG().mo20251Y(this);
        if (bTq() != null) {
            Ship vf = bTq().mo19054vf();
            mo12634P(vf);
            vf.mo18324k((Character) this);
            mo12640aP(vf);
            vf.mo8348d(C0471GX.C0472a.class, this);
        }
    }

    @C0064Am(aul = "9dff5a6a6133b01e5a73b033057e7b2c", aum = 0)
    /* renamed from: de */
    private void m18652de(Player aku) {
        m18651dd(aku);
    }

    @C0064Am(aul = "0484ebdf02385736a9a5a2d1e2f9a3a8", aum = 0)
    private Player dzC() {
        return dzy();
    }

    @C0064Am(aul = "8facca1c9278dc8390ab5064c4e8172a", aum = 0)
    /* renamed from: au */
    private String m18644au() {
        return "NPC: [" + mo11654Fs().getHandle() + "]";
    }

    @C0064Am(aul = "eeed2d66969ccd61c3a57b56435cb18b", aum = 0)
    /* renamed from: uV */
    private String m18668uV() {
        return m18660kb().get();
    }

    @C0064Am(aul = "6746d4bf680ac3e5314806b9924d1337", aum = 0)
    private C6902avq bTL() {
        return bTv();
    }

    @C0064Am(aul = "d33857aca9c610be38efacef9ae71176", aum = 0)
    private I18NString bTP() {
        return bTx();
    }

    @C0064Am(aul = "eff0caaaed5e72481523183bc88ce609", aum = 0)
    @ClientOnly
    private String ahP() {
        if (dzy() == null || dzy() == ald().mo4089dL()) {
            return getName();
        }
        return ((NLSHUDSelection) ala().aIY().mo6310c(NLSManager.C1472a.HUDSELECTION)).bBv().get();
    }

    @C0064Am(aul = "70e4646b4296dae19638ef7445e72220", aum = 0)
    /* renamed from: fg */
    private void m18657fg() {
        Ship bQx = bQx();
        if (bQx != null) {
            if (mo12657hb() != null) {
                mo12657hb().dispose();
            }
            if (bQx.bae()) {
                bQx.mo1099zx();
            }
            bQx.ahI();
            bQx.mo8355h(C0471GX.C0472a.class, this);
            bQx.dispose();
            mo12634P((Ship) null);
        }
        mo12640aP((Actor) null);
        super.dispose();
    }

    @C0064Am(aul = "8af2c8e5e076129cf7d11b904e33121c", aum = 0)
    private String bTH() {
        return bTt();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Type")
    @C0064Am(aul = "78eeede0cddcfea6e45687b16f349b20", aum = 0)
    private NPCType dzE() {
        return (NPCType) super.getType();
    }

    @C0064Am(aul = "ee7381b5e4bb5aa4e557d9da505f9e14", aum = 0)
    private NPCParty bUU() {
        return bUO();
    }

    @C0064Am(aul = "135313621b1e6fddd2465dcf3a490276", aum = 0)
    /* renamed from: b */
    private void m18645b(NPCParty dv) {
        m18637a(dv);
    }

    @C0064Am(aul = "7694c8a5b1839c8c3de1649f4dc71e25", aum = 0)
    private Faction azM() {
        if (azq() == null) {
            return ala().aJe().mo19011xf();
        }
        return azq();
    }

    @C0064Am(aul = "6f48a4aaef9f8d3ce4040b1890ecd897", aum = 0)
    /* renamed from: a */
    private void m18640a(Pawn avi, Actor cr) {
        Ship bQx;
        if (avi == bQx() && (bQx = bQx()) != null) {
            if (bQx.bae()) {
                bQx.mo1099zx();
            }
            bQx.ahI();
            bQx.mo8355h(C0471GX.C0472a.class, this);
        }
    }

    @C0064Am(aul = "a63c9dbbfec0aa461664ffa106eb8763", aum = 0)
    /* renamed from: m */
    private void m18663m(Faction xm) {
    }

    @C0064Am(aul = "3ce07a24abb00284eff437da4f181fc0", aum = 0)
    private boolean dzF() {
        return bTy();
    }

    @C0064Am(aul = "d181e562aa089605d43dfb731177ad2d", aum = 0)
    private List<ItemGenTable> bUb() {
        return Collections.unmodifiableList(bTz());
    }

    @C0064Am(aul = "f3ad2ec6ce49eedccc372b2ecc7936dc", aum = 0)
    private void cMB() {
        Ship bQx;
        StellarSystem Nc;
        if (!isDisposed() && (bQx = bQx()) != null && !bQx.bae() && !bQx.isDead() && !bQx.isDisposed() && (Nc = bQx.mo960Nc()) != null) {
            bQx.mo993b(Nc);
        }
    }

    @C0064Am(aul = "f0683b71f7d063219841cc5517fa06e9", aum = 0)
    /* renamed from: RV */
    private List<TaikopediaEntry> m18634RV() {
        return m18630Ll();
    }

    @C0064Am(aul = "71ca3643ade6b1edc172ea373f1483b4", aum = 0)
    /* renamed from: RX */
    private DatabaseCategory m18635RX() {
        return m18632RR();
    }

    @C0064Am(aul = "92de2fbcb5a29f59b1429687f9c39ac5", aum = 0)
    /* renamed from: eS */
    private Station m18655eS() {
        if (super.bhE() instanceof Station) {
            return (Station) super.bhE();
        }
        return null;
    }

    @C0064Am(aul = "dfefd8c0461360ef6074b441f96128c1", aum = 0)
    private EquippedShipType bTB() {
        return bTq();
    }

    @C0064Am(aul = "a9613740df2120f14ef136dd8e26c09f", aum = 0)
    private NPCView dzH() {
        return (NPCView) bQO();
    }
}
