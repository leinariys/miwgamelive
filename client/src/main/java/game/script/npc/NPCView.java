package game.script.npc;

import game.script.Character;
import game.script.CharacterView;
import logic.baa.C1616Xf;
import logic.baa.C1634Xv;
import logic.baa.C5511aMd;
import logic.baa.C6485anp;
import logic.data.mbean.C6672arU;
import logic.res.code.C5663aRz;
import logic.res.code.C6494any;
import logic.res.html.C2491fm;
import p001a.C0495Gr;
import p001a.C5540aNg;
import p001a.aUO;

import java.util.Collection;

@C5511aMd
@C6485anp
/* renamed from: a.akE  reason: case insensitive filesystem */
/* compiled from: a */
public class NPCView extends CharacterView implements C1616Xf {
    public static final int _m_fieldCount = 0;
    public static final C5663aRz[] _m_fields = null;
    public static final int _m_methodCount = 0;
    public static final C2491fm[] _m_methods = null;
    public static final long serialVersionUID = 0;
    public static C6494any ___iScriptClass;

    static {
        m23119V();
    }

    public NPCView() {
        super((C5540aNg) null);
        super.mo10S();
    }

    public NPCView(C5540aNg ang) {
        super(ang);
    }

    public NPCView(Character acx) {
        super((C5540aNg) null);
        super.mo14234h(acx);
    }

    /* renamed from: V */
    static void m23119V() {
        serialVersionUID = (long) 1;
        _m_fieldCount = CharacterView._m_fieldCount + 0;
        _m_methodCount = CharacterView._m_methodCount + 0;
        _m_fields = new C5663aRz[(CharacterView._m_fieldCount + 0)];
        C1634Xv.m11725a((Object[]) CharacterView._m_fields, (Object[]) _m_fields);
        _m_methods = new C2491fm[(CharacterView._m_methodCount + 0)];
        C1634Xv.m11725a((Object[]) CharacterView._m_methods, (Object[]) _m_methods);
        ___iScriptClass = aUO.m18566a(NPCView.class, C6672arU.class, _m_fields, _m_methods);
    }

    /* renamed from: S */
    public void mo10S() {
        super.mo10S();
    }

    /* renamed from: T */
    public C6494any mo11T() {
        return ___iScriptClass;
    }

    /* renamed from: U */
    public C2491fm[] mo12U() {
        return _m_methods;
    }

    /* renamed from: W */
    public Object mo13W() {
        return new C6672arU(this);
    }

    /* renamed from: a */
    public Object mo14a(C0495Gr gr) {
        return super.mo14a(gr);
    }

    /* renamed from: a */
    public void mo15a(Collection collection, C0495Gr gr) {
        super.mo15a(collection, gr);
    }

    /* renamed from: c */
    public C5663aRz[] mo25c() {
        return _m_fields;
    }

    /* renamed from: h */
    public void mo14234h(Character acx) {
        super.mo14234h(acx);
    }
}
