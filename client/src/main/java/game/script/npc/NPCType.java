package game.script.npc;

import game.network.message.externalizable.aCE;
import game.network.message.serializable.C3438ra;
import game.script.ai.npc.AIControllerType;
import game.script.faction.Faction;
import game.script.itemgen.ItemGenTable;
import game.script.npcchat.NPCChat;
import game.script.pda.DatabaseCategory;
import game.script.pda.TaikopediaEntry;
import game.script.progression.ProgressionCharacterTemplate;
import game.script.resource.Asset;
import game.script.ship.EquippedShipType;
import logic.baa.*;
import logic.data.mbean.C6198aiO;
import logic.res.code.C5640aRc;
import logic.res.code.C5663aRz;
import logic.res.code.C6494any;
import logic.res.html.C2491fm;
import p001a.*;
import taikodom.infra.script.I18NString;

import java.util.Collection;
import java.util.Collections;
import java.util.List;

@C0566Hp
@C5829abJ("1.1.1")
@C6485anp
@C5511aMd
/* renamed from: a.aed  reason: case insensitive filesystem */
/* compiled from: a */
public class NPCType extends BaseNPCType implements C0405Fe, C1616Xf {
    /* renamed from: BP */
    public static final C5663aRz f4398BP = null;
    /* renamed from: Cq */
    public static final C2491fm f4399Cq = null;
    /* renamed from: Cr */
    public static final C2491fm f4400Cr = null;
    /* renamed from: Np */
    public static final C5663aRz f4401Np = null;
    /* renamed from: QA */
    public static final C5663aRz f4402QA = null;
    /* renamed from: QE */
    public static final C2491fm f4403QE = null;
    /* renamed from: QF */
    public static final C2491fm f4404QF = null;
    public static final int _m_fieldCount = 0;
    public static final C5663aRz[] _m_fields = null;
    public static final int _m_methodCount = 0;
    public static final C2491fm[] _m_methods = null;
    public static final C5663aRz aMg = null;
    public static final C5663aRz aMh = null;
    public static final C5663aRz aMi = null;
    public static final C2491fm aMk = null;
    public static final C2491fm aMl = null;
    public static final C2491fm aMm = null;
    public static final C2491fm aMn = null;
    public static final C2491fm aMo = null;
    public static final C2491fm aMr = null;
    public static final C2491fm aMs = null;
    public static final C2491fm aMu = null;
    public static final C2491fm aMv = null;
    public static final C5663aRz awd = null;
    public static final C5663aRz cnU = null;
    public static final C2491fm cov = null;
    public static final C2491fm cow = null;
    public static final C5663aRz cpo = null;
    /* renamed from: dN */
    public static final C2491fm f4405dN = null;
    public static final C5663aRz dlV = null;
    public static final C2491fm dyZ = null;
    public static final C2491fm dza = null;
    public static final C2491fm eLi = null;
    public static final C5663aRz fmA = null;
    public static final C5663aRz fmB = null;
    public static final C5663aRz fmC = null;
    public static final C2491fm fmD = null;
    public static final C2491fm fmE = null;
    public static final C2491fm fmF = null;
    public static final C2491fm fmG = null;
    public static final C2491fm fmH = null;
    public static final C2491fm fmI = null;
    public static final C2491fm fmJ = null;
    public static final C2491fm fmK = null;
    public static final C2491fm fmL = null;
    public static final C2491fm fmM = null;
    public static final C2491fm fmN = null;
    public static final C2491fm fmO = null;
    public static final C2491fm fmP = null;
    public static final C2491fm fmQ = null;
    public static final C2491fm fmR = null;
    public static final C2491fm fmS = null;
    public static final C2491fm fmT = null;
    public static final C2491fm fmU = null;
    public static final C2491fm fmV = null;
    public static final C2491fm fmW = null;
    public static final C2491fm fmX = null;
    public static final C2491fm fmY = null;
    public static final C2491fm fmZ = null;
    public static final C5663aRz fmv = null;
    public static final C5663aRz fmw = null;
    public static final C5663aRz fmx = null;
    public static final C5663aRz fmy = null;
    public static final C5663aRz fmz = null;
    public static final C2491fm fna = null;
    public static final C2491fm fnb = null;
    public static final long serialVersionUID = 0;
    /* renamed from: zQ */
    public static final C5663aRz f4412zQ = null;
    /* renamed from: zT */
    public static final C2491fm f4413zT = null;
    /* renamed from: zU */
    public static final C2491fm f4414zU = null;
    public static C6494any ___iScriptClass;
    @C0064Am(aul = "6d20861cd232ab8c38ee3e5bd8dd5a02", aum = 17)

    /* renamed from: BO */
    private static int f4397BO;
    @C0064Am(aul = "eb21c37020ac0b54a414a32546de14c5", aum = 11)
    private static Faction axU;
    @C0064Am(aul = "6b785477f4df208c5592dbd1676115a8", aum = 0)
    private static EquippedShipType ayQ;
    @C0064Am(aul = "49c602bf2b1b4d1a1637849cd91eacfd", aum = 1)
    private static Class ayR;
    @C0064Am(aul = "0c54c8fa32147d1fab3189c5974676f5", aum = 2)
    private static AIControllerType ayS;
    @C0064Am(aul = "e64ac19c66c82ade4fae5cc1ae8e0262", aum = 4)
    private static String ayT;
    @C0064Am(aul = "56fbba9e19d329bdbcd902b4a1a32486", aum = 5)
    private static NPCChat ayU;
    @C0064Am(aul = "91733e44e95579a982326895d992f2ea", aum = 6)
    private static C6902avq ayV;
    @C0064Am(aul = "f67a1de235b3e04d7a3e8517548191a2", aum = 7)
    private static ProgressionCharacterTemplate ayW;
    @C0064Am(aul = "8383be213fc90fde6aedba590f5689b1", aum = 8)
    private static I18NString ayX;
    @C0064Am(aul = "c6bb25f8b93f247630ba607c7687d358", aum = 12)
    private static boolean ayY;
    @C0064Am(aul = "996184ac8f9800bba1767379bf8ca31a", aum = 13)
    private static C3438ra<ItemGenTable> ayZ;
    @C0064Am(aul = "2e9e5dfdb053a4bd6ce3596cfb1d2b57", aum = 14)
    private static long aza;
    @C0064Am(aul = "22b3edcefdb4cd51406cc13532431a04", aum = 15)

    /* renamed from: jS */
    private static DatabaseCategory f4406jS;
    @C0064Am(aul = "cdc819695afb974eabef82a6717cf44f", aum = 10)

    /* renamed from: jT */
    private static C3438ra<TaikopediaEntry> f4407jT;
    @C0064Am(aul = "13b365c30fddeae5241d4086305af676", aum = 16)

    /* renamed from: jU */
    private static Asset f4408jU;
    @C0064Am(aul = "4fdf297d96bf73c60e9e11faa96d667c", aum = 18)

    /* renamed from: jV */
    private static float f4409jV;
    @C0064Am(aul = "96559a84d548bea4bfb8e0ad02298690", aum = 9)

    /* renamed from: nh */
    private static I18NString f4410nh;
    @C0064Am(aul = "cad09a4b9ad0fa4d616b2ce960d70db5", aum = 3)

    /* renamed from: zP */
    private static I18NString f4411zP;

    static {
        m21249V();
    }

    public NPCType() {
        super((C5540aNg) null);
        super.mo10S();
    }

    public NPCType(C5540aNg ang) {
        super(ang);
    }

    /* renamed from: V */
    static void m21249V() {
        serialVersionUID = (long) 1;
        _m_fieldCount = BaseNPCType._m_fieldCount + 19;
        _m_methodCount = BaseNPCType._m_methodCount + 46;
        int i = BaseNPCType._m_fieldCount;
        C5663aRz[] arzArr = new C5663aRz[(i + 19)];
        C5663aRz b = C5640aRc.m17844b(NPCType.class, "6b785477f4df208c5592dbd1676115a8", i);
        fmv = b;
        arzArr[i] = b;
        int i2 = i + 1;
        C5663aRz b2 = C5640aRc.m17844b(NPCType.class, "49c602bf2b1b4d1a1637849cd91eacfd", i2);
        fmw = b2;
        arzArr[i2] = b2;
        int i3 = i2 + 1;
        C5663aRz b3 = C5640aRc.m17844b(NPCType.class, "0c54c8fa32147d1fab3189c5974676f5", i3);
        dlV = b3;
        arzArr[i3] = b3;
        int i4 = i3 + 1;
        C5663aRz b4 = C5640aRc.m17844b(NPCType.class, "cad09a4b9ad0fa4d616b2ce960d70db5", i4);
        f4412zQ = b4;
        arzArr[i4] = b4;
        int i5 = i4 + 1;
        C5663aRz b5 = C5640aRc.m17844b(NPCType.class, "e64ac19c66c82ade4fae5cc1ae8e0262", i5);
        fmx = b5;
        arzArr[i5] = b5;
        int i6 = i5 + 1;
        C5663aRz b6 = C5640aRc.m17844b(NPCType.class, "56fbba9e19d329bdbcd902b4a1a32486", i6);
        f4401Np = b6;
        arzArr[i6] = b6;
        int i7 = i6 + 1;
        C5663aRz b7 = C5640aRc.m17844b(NPCType.class, "91733e44e95579a982326895d992f2ea", i7);
        fmy = b7;
        arzArr[i7] = b7;
        int i8 = i7 + 1;
        C5663aRz b8 = C5640aRc.m17844b(NPCType.class, "f67a1de235b3e04d7a3e8517548191a2", i8);
        fmz = b8;
        arzArr[i8] = b8;
        int i9 = i8 + 1;
        C5663aRz b9 = C5640aRc.m17844b(NPCType.class, "8383be213fc90fde6aedba590f5689b1", i9);
        fmA = b9;
        arzArr[i9] = b9;
        int i10 = i9 + 1;
        C5663aRz b10 = C5640aRc.m17844b(NPCType.class, "96559a84d548bea4bfb8e0ad02298690", i10);
        f4402QA = b10;
        arzArr[i10] = b10;
        int i11 = i10 + 1;
        C5663aRz b11 = C5640aRc.m17844b(NPCType.class, "cdc819695afb974eabef82a6717cf44f", i11);
        awd = b11;
        arzArr[i11] = b11;
        int i12 = i11 + 1;
        C5663aRz b12 = C5640aRc.m17844b(NPCType.class, "eb21c37020ac0b54a414a32546de14c5", i12);
        cnU = b12;
        arzArr[i12] = b12;
        int i13 = i12 + 1;
        C5663aRz b13 = C5640aRc.m17844b(NPCType.class, "c6bb25f8b93f247630ba607c7687d358", i13);
        fmB = b13;
        arzArr[i13] = b13;
        int i14 = i13 + 1;
        C5663aRz b14 = C5640aRc.m17844b(NPCType.class, "996184ac8f9800bba1767379bf8ca31a", i14);
        fmC = b14;
        arzArr[i14] = b14;
        int i15 = i14 + 1;
        C5663aRz b15 = C5640aRc.m17844b(NPCType.class, "2e9e5dfdb053a4bd6ce3596cfb1d2b57", i15);
        cpo = b15;
        arzArr[i15] = b15;
        int i16 = i15 + 1;
        C5663aRz b16 = C5640aRc.m17844b(NPCType.class, "22b3edcefdb4cd51406cc13532431a04", i16);
        aMh = b16;
        arzArr[i16] = b16;
        int i17 = i16 + 1;
        C5663aRz b17 = C5640aRc.m17844b(NPCType.class, "13b365c30fddeae5241d4086305af676", i17);
        aMg = b17;
        arzArr[i17] = b17;
        int i18 = i17 + 1;
        C5663aRz b18 = C5640aRc.m17844b(NPCType.class, "6d20861cd232ab8c38ee3e5bd8dd5a02", i18);
        f4398BP = b18;
        arzArr[i18] = b18;
        int i19 = i18 + 1;
        C5663aRz b19 = C5640aRc.m17844b(NPCType.class, "4fdf297d96bf73c60e9e11faa96d667c", i19);
        aMi = b19;
        arzArr[i19] = b19;
        int i20 = i19 + 1;
        _m_fields = arzArr;
        C1634Xv.m11725a((Object[]) BaseNPCType._m_fields, (Object[]) _m_fields);
        int i21 = BaseNPCType._m_methodCount;
        C2491fm[] fmVarArr = new C2491fm[(i21 + 46)];
        C2491fm a = C4105zY.m41624a(NPCType.class, "7ff1b04d18304f3f3d7d4f13e54dd28e", i21);
        fmD = a;
        fmVarArr[i21] = a;
        int i22 = i21 + 1;
        C2491fm a2 = C4105zY.m41624a(NPCType.class, "8572de30d97247de9f4c3b457d964253", i22);
        fmE = a2;
        fmVarArr[i22] = a2;
        int i23 = i22 + 1;
        C2491fm a3 = C4105zY.m41624a(NPCType.class, "c32b7e0bc81c21615feba7ebaef02981", i23);
        fmF = a3;
        fmVarArr[i23] = a3;
        int i24 = i23 + 1;
        C2491fm a4 = C4105zY.m41624a(NPCType.class, "05b508a761c86fe70bfdbe8879551053", i24);
        fmG = a4;
        fmVarArr[i24] = a4;
        int i25 = i24 + 1;
        C2491fm a5 = C4105zY.m41624a(NPCType.class, "5bcb607375815267c172326d5428fbab", i25);
        fmH = a5;
        fmVarArr[i25] = a5;
        int i26 = i25 + 1;
        C2491fm a6 = C4105zY.m41624a(NPCType.class, "8c5662e6541daf3ca40962b3dc5de70c", i26);
        fmI = a6;
        fmVarArr[i26] = a6;
        int i27 = i26 + 1;
        C2491fm a7 = C4105zY.m41624a(NPCType.class, "2fd49bc3e43ca5289a9952b0f366c232", i27);
        f4413zT = a7;
        fmVarArr[i27] = a7;
        int i28 = i27 + 1;
        C2491fm a8 = C4105zY.m41624a(NPCType.class, "597cc7c08a281eaddb0175d22dc6c8ba", i28);
        f4414zU = a8;
        fmVarArr[i28] = a8;
        int i29 = i28 + 1;
        C2491fm a9 = C4105zY.m41624a(NPCType.class, "8b1cea31e641c47ca37097598cc24b68", i29);
        fmJ = a9;
        fmVarArr[i29] = a9;
        int i30 = i29 + 1;
        C2491fm a10 = C4105zY.m41624a(NPCType.class, "98bf1542616aaa9005659275d75c2965", i30);
        fmK = a10;
        fmVarArr[i30] = a10;
        int i31 = i30 + 1;
        C2491fm a11 = C4105zY.m41624a(NPCType.class, "4998f47f34bc8410425a32d6e9b39650", i31);
        fmL = a11;
        fmVarArr[i31] = a11;
        int i32 = i31 + 1;
        C2491fm a12 = C4105zY.m41624a(NPCType.class, "6416b99160891c369aa643824d92c663", i32);
        fmM = a12;
        fmVarArr[i32] = a12;
        int i33 = i32 + 1;
        C2491fm a13 = C4105zY.m41624a(NPCType.class, "297ddcc1aa102300b04838ae10624bff", i33);
        fmN = a13;
        fmVarArr[i33] = a13;
        int i34 = i33 + 1;
        C2491fm a14 = C4105zY.m41624a(NPCType.class, "82ece7d9c4ec40c526f9ab733ccf8894", i34);
        fmO = a14;
        fmVarArr[i34] = a14;
        int i35 = i34 + 1;
        C2491fm a15 = C4105zY.m41624a(NPCType.class, "92d824ddf6039de670b787d4f952fdba", i35);
        fmP = a15;
        fmVarArr[i35] = a15;
        int i36 = i35 + 1;
        C2491fm a16 = C4105zY.m41624a(NPCType.class, "099fd6de44355852fae5255d30315d9f", i36);
        fmQ = a16;
        fmVarArr[i36] = a16;
        int i37 = i36 + 1;
        C2491fm a17 = C4105zY.m41624a(NPCType.class, "889bd6a7e8970bb174199188cf684e8e", i37);
        fmR = a17;
        fmVarArr[i37] = a17;
        int i38 = i37 + 1;
        C2491fm a18 = C4105zY.m41624a(NPCType.class, "bf7ec665e238ac6c8541faa54f949558", i38);
        fmS = a18;
        fmVarArr[i38] = a18;
        int i39 = i38 + 1;
        C2491fm a19 = C4105zY.m41624a(NPCType.class, "d660bf77dcff6f7a5505e15b0e512b57", i39);
        f4403QE = a19;
        fmVarArr[i39] = a19;
        int i40 = i39 + 1;
        C2491fm a20 = C4105zY.m41624a(NPCType.class, "23de5654260d3ab4d10d3c10b4797d76", i40);
        f4404QF = a20;
        fmVarArr[i40] = a20;
        int i41 = i40 + 1;
        C2491fm a21 = C4105zY.m41624a(NPCType.class, "d87a38eba7dfc8b010da76031621211b", i41);
        aMl = a21;
        fmVarArr[i41] = a21;
        int i42 = i41 + 1;
        C2491fm a22 = C4105zY.m41624a(NPCType.class, "34eba3407916499c90004cce3ae05771", i42);
        aMm = a22;
        fmVarArr[i42] = a22;
        int i43 = i42 + 1;
        C2491fm a23 = C4105zY.m41624a(NPCType.class, "b5ce2da188d1fd9dca5e69008541b9e6", i43);
        dyZ = a23;
        fmVarArr[i43] = a23;
        int i44 = i43 + 1;
        C2491fm a24 = C4105zY.m41624a(NPCType.class, "89690a9aae8661fac5d33f2f71f01a5d", i44);
        dza = a24;
        fmVarArr[i44] = a24;
        int i45 = i44 + 1;
        C2491fm a25 = C4105zY.m41624a(NPCType.class, "23608a294a0f831be15d47da87334805", i45);
        cov = a25;
        fmVarArr[i45] = a25;
        int i46 = i45 + 1;
        C2491fm a26 = C4105zY.m41624a(NPCType.class, "81674f5c8b4b54bbfc912096a0e97299", i46);
        cow = a26;
        fmVarArr[i46] = a26;
        int i47 = i46 + 1;
        C2491fm a27 = C4105zY.m41624a(NPCType.class, "ac9a52e330071cb51fdae874e7818930", i47);
        fmT = a27;
        fmVarArr[i47] = a27;
        int i48 = i47 + 1;
        C2491fm a28 = C4105zY.m41624a(NPCType.class, "dfa8f404a74087880fdb1cfc726451ce", i48);
        fmU = a28;
        fmVarArr[i48] = a28;
        int i49 = i48 + 1;
        C2491fm a29 = C4105zY.m41624a(NPCType.class, "b0212f500e759daaf6a8df04fb79a4e7", i49);
        eLi = a29;
        fmVarArr[i49] = a29;
        int i50 = i49 + 1;
        C2491fm a30 = C4105zY.m41624a(NPCType.class, "149b305296cfe5e94650e9c0751cea4e", i50);
        fmV = a30;
        fmVarArr[i50] = a30;
        int i51 = i50 + 1;
        C2491fm a31 = C4105zY.m41624a(NPCType.class, "fe70fe7000b0dd2864c8a8721ef1286d", i51);
        fmW = a31;
        fmVarArr[i51] = a31;
        int i52 = i51 + 1;
        C2491fm a32 = C4105zY.m41624a(NPCType.class, "8848803e67da874e1e5ea8955eca9ed7", i52);
        fmX = a32;
        fmVarArr[i52] = a32;
        int i53 = i52 + 1;
        C2491fm a33 = C4105zY.m41624a(NPCType.class, "d1cdb0cdbc63b9f8106d9611bef4dbae", i53);
        fmY = a33;
        fmVarArr[i53] = a33;
        int i54 = i53 + 1;
        C2491fm a34 = C4105zY.m41624a(NPCType.class, "3dc66a9e0d9652f16bd6774d7c474401", i54);
        fmZ = a34;
        fmVarArr[i54] = a34;
        int i55 = i54 + 1;
        C2491fm a35 = C4105zY.m41624a(NPCType.class, "ef4a7221bb96f8acc022ed0052f936de", i55);
        aMn = a35;
        fmVarArr[i55] = a35;
        int i56 = i55 + 1;
        C2491fm a36 = C4105zY.m41624a(NPCType.class, "8f831bb812820c7227ca70ac06920d27", i56);
        aMo = a36;
        fmVarArr[i56] = a36;
        int i57 = i56 + 1;
        C2491fm a37 = C4105zY.m41624a(NPCType.class, "571021f0f8ad74b25224a7c0b94a6e17", i57);
        aMr = a37;
        fmVarArr[i57] = a37;
        int i58 = i57 + 1;
        C2491fm a38 = C4105zY.m41624a(NPCType.class, "56337580607d887b64c064990d4f34d4", i58);
        aMs = a38;
        fmVarArr[i58] = a38;
        int i59 = i58 + 1;
        C2491fm a39 = C4105zY.m41624a(NPCType.class, "0785813a838d22078d6b6af887d5da95", i59);
        f4399Cq = a39;
        fmVarArr[i59] = a39;
        int i60 = i59 + 1;
        C2491fm a40 = C4105zY.m41624a(NPCType.class, "17e5fcdd64a85b88ebc7c433375756e4", i60);
        f4400Cr = a40;
        fmVarArr[i60] = a40;
        int i61 = i60 + 1;
        C2491fm a41 = C4105zY.m41624a(NPCType.class, "6916879631d7f085c145cc4daeeae351", i61);
        aMu = a41;
        fmVarArr[i61] = a41;
        int i62 = i61 + 1;
        C2491fm a42 = C4105zY.m41624a(NPCType.class, "190652b072184876c7b96721cbaaf229", i62);
        aMv = a42;
        fmVarArr[i62] = a42;
        int i63 = i62 + 1;
        C2491fm a43 = C4105zY.m41624a(NPCType.class, "deccc0830daf3189f86adbc89e5878b8", i63);
        fna = a43;
        fmVarArr[i63] = a43;
        int i64 = i63 + 1;
        C2491fm a44 = C4105zY.m41624a(NPCType.class, "58736f85f3babb9e2a49044902223bb4", i64);
        f4405dN = a44;
        fmVarArr[i64] = a44;
        int i65 = i64 + 1;
        C2491fm a45 = C4105zY.m41624a(NPCType.class, "8abccc5f7e934fec842b338a196ac8cc", i65);
        fnb = a45;
        fmVarArr[i65] = a45;
        int i66 = i65 + 1;
        C2491fm a46 = C4105zY.m41624a(NPCType.class, "22e9877f654b503e0ca69aa41203e8fc", i66);
        aMk = a46;
        fmVarArr[i66] = a46;
        int i67 = i66 + 1;
        _m_methods = fmVarArr;
        C1634Xv.m11725a((Object[]) BaseNPCType._m_methods, (Object[]) _m_methods);
        ___iScriptClass = aUO.m18566a(NPCType.class, C6198aiO.class, _m_fields, _m_methods);
    }

    /* renamed from: L */
    private void m21239L(Asset tCVar) {
        bFf().mo5608dq().mo3197f(aMg, tCVar);
    }

    /* renamed from: Ll */
    private C3438ra m21240Ll() {
        return (C3438ra) bFf().mo5608dq().mo3214p(awd);
    }

    /* renamed from: RQ */
    private Asset m21242RQ() {
        return (Asset) bFf().mo5608dq().mo3214p(aMg);
    }

    /* renamed from: RR */
    private DatabaseCategory m21243RR() {
        return (DatabaseCategory) bFf().mo5608dq().mo3214p(aMh);
    }

    /* renamed from: RS */
    private float m21244RS() {
        return bFf().mo5608dq().mo3211m(aMi);
    }

    @C0064Am(aul = "22e9877f654b503e0ca69aa41203e8fc", aum = 0)
    /* renamed from: RV */
    private List m21245RV() {
        return bgF();
    }

    /* renamed from: a */
    private void m21250a(NPCChat aaq) {
        bFf().mo5608dq().mo3197f(f4401Np, aaq);
    }

    /* renamed from: a */
    private void m21251a(DatabaseCategory aik) {
        bFf().mo5608dq().mo3197f(aMh, aik);
    }

    /* renamed from: a */
    private void m21253a(C6902avq avq) {
        bFf().mo5608dq().mo3197f(fmy, avq);
    }

    /* renamed from: ad */
    private void m21256ad(Class cls) {
        bFf().mo5608dq().mo3197f(fmw, cls);
    }

    /* renamed from: ar */
    private void m21258ar(int i) {
        bFf().mo5608dq().mo3183b(f4398BP, i);
    }

    private Faction azq() {
        return (Faction) bFf().mo5608dq().mo3214p(cnU);
    }

    /* renamed from: bL */
    private void m21263bL(C3438ra raVar) {
        bFf().mo5608dq().mo3197f(fmC, raVar);
    }

    private long bTA() {
        return bFf().mo5608dq().mo3213o(cpo);
    }

    private EquippedShipType bTq() {
        return (EquippedShipType) bFf().mo5608dq().mo3214p(fmv);
    }

    private Class bTr() {
        return (Class) bFf().mo5608dq().mo3214p(fmw);
    }

    private AIControllerType bTs() {
        return (AIControllerType) bFf().mo5608dq().mo3214p(dlV);
    }

    private String bTt() {
        return (String) bFf().mo5608dq().mo3214p(fmx);
    }

    private NPCChat bTu() {
        return (NPCChat) bFf().mo5608dq().mo3214p(f4401Np);
    }

    private C6902avq bTv() {
        return (C6902avq) bFf().mo5608dq().mo3214p(fmy);
    }

    private ProgressionCharacterTemplate bTw() {
        return (ProgressionCharacterTemplate) bFf().mo5608dq().mo3214p(fmz);
    }

    private I18NString bTx() {
        return (I18NString) bFf().mo5608dq().mo3214p(fmA);
    }

    private boolean bTy() {
        return bFf().mo5608dq().mo3201h(fmB);
    }

    private C3438ra bTz() {
        return (C3438ra) bFf().mo5608dq().mo3214p(fmC);
    }

    @C0064Am(aul = "8abccc5f7e934fec842b338a196ac8cc", aum = 0)
    private List bUb() {
        return bTU();
    }

    /* renamed from: br */
    private void m21264br(I18NString i18NString) {
        bFf().mo5608dq().mo3197f(f4402QA, i18NString);
    }

    /* renamed from: c */
    private void m21267c(ProgressionCharacterTemplate aof) {
        bFf().mo5608dq().mo3197f(fmz, aof);
    }

    /* renamed from: cx */
    private void m21269cx(float f) {
        bFf().mo5608dq().mo3150a(aMi, f);
    }

    /* renamed from: d */
    private void m21272d(I18NString i18NString) {
        bFf().mo5608dq().mo3197f(f4412zQ, i18NString);
    }

    /* renamed from: e */
    private void m21273e(AIControllerType tz) {
        bFf().mo5608dq().mo3197f(dlV, tz);
    }

    /* renamed from: e */
    private void m21274e(EquippedShipType gfVar) {
        bFf().mo5608dq().mo3197f(fmv, gfVar);
    }

    /* renamed from: ed */
    private void m21276ed(boolean z) {
        bFf().mo5608dq().mo3153a(fmB, z);
    }

    /* renamed from: gW */
    private void m21280gW(long j) {
        bFf().mo5608dq().mo3184b(cpo, j);
    }

    /* renamed from: iy */
    private void m21282iy(String str) {
        bFf().mo5608dq().mo3197f(fmx, str);
    }

    /* renamed from: kb */
    private I18NString m21284kb() {
        return (I18NString) bFf().mo5608dq().mo3214p(f4412zQ);
    }

    /* renamed from: l */
    private void m21286l(Faction xm) {
        bFf().mo5608dq().mo3197f(cnU, xm);
    }

    /* renamed from: le */
    private int m21287le() {
        return bFf().mo5608dq().mo3212n(f4398BP);
    }

    /* renamed from: mi */
    private void m21290mi(I18NString i18NString) {
        bFf().mo5608dq().mo3197f(fmA, i18NString);
    }

    /* renamed from: vT */
    private I18NString m21292vT() {
        return (I18NString) bFf().mo5608dq().mo3214p(f4402QA);
    }

    /* renamed from: x */
    private void m21294x(C3438ra raVar) {
        bFf().mo5608dq().mo3197f(awd, raVar);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Pda Render")
    /* renamed from: N */
    public void mo13101N(Asset tCVar) {
        switch (bFf().mo6893i(aMs)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, aMs, new Object[]{tCVar}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, aMs, new Object[]{tCVar}));
                break;
        }
        m21241M(tCVar);
    }

    /* renamed from: RW */
    public /* bridge */ /* synthetic */ List mo183RW() {
        switch (bFf().mo6893i(aMk)) {
            case 0:
                return null;
            case 2:
                return (List) bFf().mo5606d(new aCE(this, aMk, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, aMk, new Object[0]));
                break;
        }
        return m21245RV();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Database Category")
    /* renamed from: RY */
    public DatabaseCategory mo184RY() {
        switch (bFf().mo6893i(aMn)) {
            case 0:
                return null;
            case 2:
                return (DatabaseCategory) bFf().mo5606d(new aCE(this, aMn, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, aMn, new Object[0]));
                break;
        }
        return m21246RX();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Pda Render")
    /* renamed from: Se */
    public Asset mo187Se() {
        switch (bFf().mo6893i(aMr)) {
            case 0:
                return null;
            case 2:
                return (Asset) bFf().mo5606d(new aCE(this, aMr, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, aMr, new Object[0]));
                break;
        }
        return m21247Sd();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Pda M P Multiplier")
    /* renamed from: Sg */
    public float mo188Sg() {
        switch (bFf().mo6893i(aMu)) {
            case 0:
                return 0.0f;
            case 2:
                return ((Float) bFf().mo5606d(new aCE(this, aMu, new Object[0]))).floatValue();
            case 3:
                bFf().mo5606d(new aCE(this, aMu, new Object[0]));
                break;
        }
        return m21248Sf();
    }

    /* renamed from: T */
    public C6494any mo11T() {
        return ___iScriptClass;
    }

    /* renamed from: U */
    public C2491fm[] mo12U() {
        return _m_methods;
    }

    /* renamed from: W */
    public Object mo13W() {
        return new C6198aiO(this);
    }

    /* renamed from: a */
    public Object mo14a(C0495Gr gr) {
        Object[] args = gr.getArgs();
        switch (gr.mo2417hq() - BaseNPCType._m_methodCount) {
            case 0:
                return bTB();
            case 1:
                m21279f((EquippedShipType) args[0]);
                return null;
            case 2:
                return bTD();
            case 3:
                m21257ae((Class) args[0]);
                return null;
            case 4:
                return bTF();
            case 5:
                m21278f((AIControllerType) args[0]);
                return null;
            case 6:
                return m21285kd();
            case 7:
                m21275e((I18NString) args[0]);
                return null;
            case 8:
                return bTH();
            case 9:
                m21283iz((String) args[0]);
                return null;
            case 10:
                return bTJ();
            case 11:
                m21260b((NPCChat) args[0]);
                return null;
            case 12:
                return bTL();
            case 13:
                m21262b((C6902avq) args[0]);
                return null;
            case 14:
                return bTN();
            case 15:
                m21271d((ProgressionCharacterTemplate) args[0]);
                return null;
            case 16:
                return bTP();
            case 17:
                m21291mj((I18NString) args[0]);
                return null;
            case 18:
                return m21293vV();
            case 19:
                m21265bu((I18NString) args[0]);
                return null;
            case 20:
                m21252a((TaikopediaEntry) args[0]);
                return null;
            case 21:
                m21266c((TaikopediaEntry) args[0]);
                return null;
            case 22:
                return bgE();
            case 23:
                return bgG();
            case 24:
                return azM();
            case 25:
                m21289m((Faction) args[0]);
                return null;
            case 26:
                return new Boolean(bTR());
            case 27:
                m21277ee(((Boolean) args[0]).booleanValue());
                return null;
            case 28:
                m21254a((ItemGenTable) args[0]);
                return null;
            case 29:
                m21268c((ItemGenTable) args[0]);
                return null;
            case 30:
                return bTT();
            case 31:
                return bTV();
            case 32:
                return new Long(bTX());
            case 33:
                m21281gX(((Long) args[0]).longValue());
                return null;
            case 34:
                return m21246RX();
            case 35:
                m21261b((DatabaseCategory) args[0]);
                return null;
            case 36:
                return m21247Sd();
            case 37:
                m21241M((Asset) args[0]);
                return null;
            case 38:
                return new Integer(m21288ls());
            case 39:
                m21259ax(((Integer) args[0]).intValue());
                return null;
            case 40:
                return new Float(m21248Sf());
            case 41:
                m21270cy(((Float) args[0]).floatValue());
                return null;
            case 42:
                return bTZ();
            case 43:
                return m21255aT();
            case 44:
                return bUb();
            case 45:
                return m21245RV();
            default:
                return super.mo14a(gr);
        }
    }

    /* renamed from: a */
    public void mo15a(Collection collection, C0495Gr gr) {
        super.mo15a(collection, gr);
    }

    public /* bridge */ /* synthetic */ List aPl() {
        switch (bFf().mo6893i(fnb)) {
            case 0:
                return null;
            case 2:
                return (List) bFf().mo5606d(new aCE(this, fnb, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, fnb, new Object[0]));
                break;
        }
        return bUb();
    }

    /* access modifiers changed from: protected */
    /* renamed from: aU */
    public <T extends aDJ> T mo745aU() {
        switch (bFf().mo6893i(f4405dN)) {
            case 0:
                return null;
            case 2:
                return (aDJ) bFf().mo5606d(new aCE(this, f4405dN, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f4405dN, new Object[0]));
                break;
        }
        return m21255aT();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Ai Controller Class")
    /* renamed from: af */
    public void mo13102af(Class cls) {
        switch (bFf().mo6893i(fmG)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, fmG, new Object[]{cls}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, fmG, new Object[]{cls}));
                break;
        }
        m21257ae(cls);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Challenge Rating")
    /* renamed from: ay */
    public void mo13103ay(int i) {
        switch (bFf().mo6893i(f4400Cr)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f4400Cr, new Object[]{new Integer(i)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f4400Cr, new Object[]{new Integer(i)}));
                break;
        }
        m21259ax(i);
    }

    @aFW("Faction")
    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Faction")
    public Faction azN() {
        switch (bFf().mo6893i(cov)) {
            case 0:
                return null;
            case 2:
                return (Faction) bFf().mo5606d(new aCE(this, cov, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, cov, new Object[0]));
                break;
        }
        return azM();
    }

    @C3248pc(aYR = C3248pc.C3250b.ADDER, displayName = "Taikopedia Entries")
    /* renamed from: b */
    public void mo13105b(TaikopediaEntry aiz) {
        switch (bFf().mo6893i(aMl)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, aMl, new Object[]{aiz}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, aMl, new Object[]{aiz}));
                break;
        }
        m21252a(aiz);
    }

    @C3248pc(aYR = C3248pc.C3250b.ADDER, displayName = "Item Gen Tables")
    /* renamed from: b */
    public void mo11464b(ItemGenTable oqVar) {
        switch (bFf().mo6893i(eLi)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, eLi, new Object[]{oqVar}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, eLi, new Object[]{oqVar}));
                break;
        }
        m21254a(oqVar);
    }

    @aFW("EquipedShipType")
    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Equiped Ship Type")
    public EquippedShipType bTC() {
        switch (bFf().mo6893i(fmD)) {
            case 0:
                return null;
            case 2:
                return (EquippedShipType) bFf().mo5606d(new aCE(this, fmD, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, fmD, new Object[0]));
                break;
        }
        return bTB();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Ai Controller Class")
    public Class bTE() {
        switch (bFf().mo6893i(fmF)) {
            case 0:
                return null;
            case 2:
                return (Class) bFf().mo5606d(new aCE(this, fmF, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, fmF, new Object[0]));
                break;
        }
        return bTD();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Ai Controller Type")
    public AIControllerType bTG() {
        switch (bFf().mo6893i(fmH)) {
            case 0:
                return null;
            case 2:
                return (AIControllerType) bFf().mo5606d(new aCE(this, fmH, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, fmH, new Object[0]));
                break;
        }
        return bTF();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Avatar Handle")
    public String bTI() {
        switch (bFf().mo6893i(fmJ)) {
            case 0:
                return null;
            case 2:
                return (String) bFf().mo5606d(new aCE(this, fmJ, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, fmJ, new Object[0]));
                break;
        }
        return bTH();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Chat")
    public NPCChat bTK() {
        switch (bFf().mo6893i(fmL)) {
            case 0:
                return null;
            case 2:
                return (NPCChat) bFf().mo5606d(new aCE(this, fmL, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, fmL, new Object[0]));
                break;
        }
        return bTJ();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Specialization")
    public C6902avq bTM() {
        switch (bFf().mo6893i(fmN)) {
            case 0:
                return null;
            case 2:
                return (C6902avq) bFf().mo5606d(new aCE(this, fmN, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, fmN, new Object[0]));
                break;
        }
        return bTL();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Progression Template")
    public ProgressionCharacterTemplate bTO() {
        switch (bFf().mo6893i(fmP)) {
            case 0:
                return null;
            case 2:
                return (ProgressionCharacterTemplate) bFf().mo5606d(new aCE(this, fmP, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, fmP, new Object[0]));
                break;
        }
        return bTN();
    }

    @aFW("Occupation")
    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Occupation")
    public I18NString bTQ() {
        switch (bFf().mo6893i(fmR)) {
            case 0:
                return null;
            case 2:
                return (I18NString) bFf().mo5606d(new aCE(this, fmR, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, fmR, new Object[0]));
                break;
        }
        return bTP();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Is Boss")
    public boolean bTS() {
        switch (bFf().mo6893i(fmT)) {
            case 0:
                return false;
            case 2:
                return ((Boolean) bFf().mo5606d(new aCE(this, fmT, new Object[0]))).booleanValue();
            case 3:
                bFf().mo5606d(new aCE(this, fmT, new Object[0]));
                break;
        }
        return bTR();
    }

    public C3438ra<ItemGenTable> bTU() {
        switch (bFf().mo6893i(fmW)) {
            case 0:
                return null;
            case 2:
                return (C3438ra) bFf().mo5606d(new aCE(this, fmW, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, fmW, new Object[0]));
                break;
        }
        return bTT();
    }

    @C3248pc(aYR = C3248pc.C3250b.ITERATOR, displayName = "Item Gen Tables")
    public List<ItemGenTable> bTW() {
        switch (bFf().mo6893i(fmX)) {
            case 0:
                return null;
            case 2:
                return (List) bFf().mo5606d(new aCE(this, fmX, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, fmX, new Object[0]));
                break;
        }
        return bTV();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Reward")
    public long bTY() {
        switch (bFf().mo6893i(fmY)) {
            case 0:
                return 0;
            case 2:
                return ((Long) bFf().mo5606d(new aCE(this, fmY, new Object[0]))).longValue();
            case 3:
                bFf().mo5606d(new aCE(this, fmY, new Object[0]));
                break;
        }
        return bTX();
    }

    public NPC bUa() {
        switch (bFf().mo6893i(fna)) {
            case 0:
                return null;
            case 2:
                return (NPC) bFf().mo5606d(new aCE(this, fna, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, fna, new Object[0]));
                break;
        }
        return bTZ();
    }

    public C3438ra<TaikopediaEntry> bgF() {
        switch (bFf().mo6893i(dyZ)) {
            case 0:
                return null;
            case 2:
                return (C3438ra) bFf().mo5606d(new aCE(this, dyZ, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, dyZ, new Object[0]));
                break;
        }
        return bgE();
    }

    @C3248pc(aYR = C3248pc.C3250b.ITERATOR, displayName = "Taikopedia Entries")
    public List<TaikopediaEntry> bgH() {
        switch (bFf().mo6893i(dza)) {
            case 0:
                return null;
            case 2:
                return (List) bFf().mo5606d(new aCE(this, dza, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, dza, new Object[0]));
                break;
        }
        return bgG();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Description")
    /* renamed from: bv */
    public void mo13118bv(I18NString i18NString) {
        switch (bFf().mo6893i(f4404QF)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f4404QF, new Object[]{i18NString}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f4404QF, new Object[]{i18NString}));
                break;
        }
        m21265bu(i18NString);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Chat")
    /* renamed from: c */
    public void mo13119c(NPCChat aaq) {
        switch (bFf().mo6893i(fmM)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, fmM, new Object[]{aaq}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, fmM, new Object[]{aaq}));
                break;
        }
        m21260b(aaq);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Database Category")
    /* renamed from: c */
    public void mo13120c(DatabaseCategory aik) {
        switch (bFf().mo6893i(aMo)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, aMo, new Object[]{aik}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, aMo, new Object[]{aik}));
                break;
        }
        m21261b(aik);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Specialization")
    /* renamed from: c */
    public void mo11468c(C6902avq avq) {
        switch (bFf().mo6893i(fmO)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, fmO, new Object[]{avq}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, fmO, new Object[]{avq}));
                break;
        }
        m21262b(avq);
    }

    /* renamed from: c */
    public C5663aRz[] mo25c() {
        return _m_fields;
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Pda M P Multiplier")
    /* renamed from: cz */
    public void mo13121cz(float f) {
        switch (bFf().mo6893i(aMv)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, aMv, new Object[]{new Float(f)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, aMv, new Object[]{new Float(f)}));
                break;
        }
        m21270cy(f);
    }

    @C3248pc(aYR = C3248pc.C3250b.REMOVER, displayName = "Taikopedia Entries")
    /* renamed from: d */
    public void mo13122d(TaikopediaEntry aiz) {
        switch (bFf().mo6893i(aMm)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, aMm, new Object[]{aiz}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, aMm, new Object[]{aiz}));
                break;
        }
        m21266c(aiz);
    }

    @C3248pc(aYR = C3248pc.C3250b.REMOVER, displayName = "Item Gen Tables")
    /* renamed from: d */
    public void mo13123d(ItemGenTable oqVar) {
        switch (bFf().mo6893i(fmV)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, fmV, new Object[]{oqVar}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, fmV, new Object[]{oqVar}));
                break;
        }
        m21268c(oqVar);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Progression Template")
    /* renamed from: e */
    public void mo13124e(ProgressionCharacterTemplate aof) {
        switch (bFf().mo6893i(fmQ)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, fmQ, new Object[]{aof}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, fmQ, new Object[]{aof}));
                break;
        }
        m21271d(aof);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Is Boss")
    /* renamed from: ef */
    public void mo13125ef(boolean z) {
        switch (bFf().mo6893i(fmU)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, fmU, new Object[]{new Boolean(z)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, fmU, new Object[]{new Boolean(z)}));
                break;
        }
        m21277ee(z);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Name")
    /* renamed from: f */
    public void mo13126f(I18NString i18NString) {
        switch (bFf().mo6893i(f4414zU)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f4414zU, new Object[]{i18NString}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f4414zU, new Object[]{i18NString}));
                break;
        }
        m21275e(i18NString);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Ai Controller Type")
    /* renamed from: g */
    public void mo13127g(AIControllerType tz) {
        switch (bFf().mo6893i(fmI)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, fmI, new Object[]{tz}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, fmI, new Object[]{tz}));
                break;
        }
        m21278f(tz);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Equiped Ship Type")
    /* renamed from: g */
    public void mo13128g(EquippedShipType gfVar) {
        switch (bFf().mo6893i(fmE)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, fmE, new Object[]{gfVar}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, fmE, new Object[]{gfVar}));
                break;
        }
        m21279f(gfVar);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Reward")
    /* renamed from: gY */
    public void mo13129gY(long j) {
        switch (bFf().mo6893i(fmZ)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, fmZ, new Object[]{new Long(j)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, fmZ, new Object[]{new Long(j)}));
                break;
        }
        m21281gX(j);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Avatar Handle")
    /* renamed from: iA */
    public void mo13130iA(String str) {
        switch (bFf().mo6893i(fmK)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, fmK, new Object[]{str}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, fmK, new Object[]{str}));
                break;
        }
        m21283iz(str);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Name")
    /* renamed from: ke */
    public I18NString mo11470ke() {
        switch (bFf().mo6893i(f4413zT)) {
            case 0:
                return null;
            case 2:
                return (I18NString) bFf().mo5606d(new aCE(this, f4413zT, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f4413zT, new Object[0]));
                break;
        }
        return m21285kd();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Challenge Rating")
    /* renamed from: lt */
    public int mo13131lt() {
        switch (bFf().mo6893i(f4399Cq)) {
            case 0:
                return 0;
            case 2:
                return ((Integer) bFf().mo5606d(new aCE(this, f4399Cq, new Object[0]))).intValue();
            case 3:
                bFf().mo5606d(new aCE(this, f4399Cq, new Object[0]));
                break;
        }
        return m21288ls();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Occupation")
    /* renamed from: mk */
    public void mo13132mk(I18NString i18NString) {
        switch (bFf().mo6893i(fmS)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, fmS, new Object[]{i18NString}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, fmS, new Object[]{i18NString}));
                break;
        }
        m21291mj(i18NString);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Faction")
    /* renamed from: n */
    public void mo13133n(Faction xm) {
        switch (bFf().mo6893i(cow)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, cow, new Object[]{xm}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, cow, new Object[]{xm}));
                break;
        }
        m21289m(xm);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Description")
    /* renamed from: vW */
    public I18NString mo11471vW() {
        switch (bFf().mo6893i(f4403QE)) {
            case 0:
                return null;
            case 2:
                return (I18NString) bFf().mo5606d(new aCE(this, f4403QE, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f4403QE, new Object[0]));
                break;
        }
        return m21293vV();
    }

    /* renamed from: S */
    public void mo10S() {
        super.mo10S();
    }

    @aFW("EquipedShipType")
    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Equiped Ship Type")
    @C0064Am(aul = "7ff1b04d18304f3f3d7d4f13e54dd28e", aum = 0)
    private EquippedShipType bTB() {
        return bTq();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Equiped Ship Type")
    @C0064Am(aul = "8572de30d97247de9f4c3b457d964253", aum = 0)
    /* renamed from: f */
    private void m21279f(EquippedShipType gfVar) {
        m21274e(gfVar);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Ai Controller Class")
    @C0064Am(aul = "c32b7e0bc81c21615feba7ebaef02981", aum = 0)
    private Class bTD() {
        return bTr();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Ai Controller Class")
    @C0064Am(aul = "05b508a761c86fe70bfdbe8879551053", aum = 0)
    /* renamed from: ae */
    private void m21257ae(Class cls) {
        m21256ad(cls);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Ai Controller Type")
    @C0064Am(aul = "5bcb607375815267c172326d5428fbab", aum = 0)
    private AIControllerType bTF() {
        return bTs();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Ai Controller Type")
    @C0064Am(aul = "8c5662e6541daf3ca40962b3dc5de70c", aum = 0)
    /* renamed from: f */
    private void m21278f(AIControllerType tz) {
        m21273e(tz);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Name")
    @C0064Am(aul = "2fd49bc3e43ca5289a9952b0f366c232", aum = 0)
    /* renamed from: kd */
    private I18NString m21285kd() {
        return m21284kb();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Name")
    @C0064Am(aul = "597cc7c08a281eaddb0175d22dc6c8ba", aum = 0)
    /* renamed from: e */
    private void m21275e(I18NString i18NString) {
        m21272d(i18NString);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Avatar Handle")
    @C0064Am(aul = "8b1cea31e641c47ca37097598cc24b68", aum = 0)
    private String bTH() {
        return bTt();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Avatar Handle")
    @C0064Am(aul = "98bf1542616aaa9005659275d75c2965", aum = 0)
    /* renamed from: iz */
    private void m21283iz(String str) {
        m21282iy(str);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Chat")
    @C0064Am(aul = "4998f47f34bc8410425a32d6e9b39650", aum = 0)
    private NPCChat bTJ() {
        return bTu();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Chat")
    @C0064Am(aul = "6416b99160891c369aa643824d92c663", aum = 0)
    /* renamed from: b */
    private void m21260b(NPCChat aaq) {
        m21250a(aaq);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Specialization")
    @C0064Am(aul = "297ddcc1aa102300b04838ae10624bff", aum = 0)
    private C6902avq bTL() {
        return bTv();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Specialization")
    @C0064Am(aul = "82ece7d9c4ec40c526f9ab733ccf8894", aum = 0)
    /* renamed from: b */
    private void m21262b(C6902avq avq) {
        m21253a(avq);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Progression Template")
    @C0064Am(aul = "92d824ddf6039de670b787d4f952fdba", aum = 0)
    private ProgressionCharacterTemplate bTN() {
        return bTw();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Progression Template")
    @C0064Am(aul = "099fd6de44355852fae5255d30315d9f", aum = 0)
    /* renamed from: d */
    private void m21271d(ProgressionCharacterTemplate aof) {
        m21267c(aof);
    }

    @aFW("Occupation")
    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Occupation")
    @C0064Am(aul = "889bd6a7e8970bb174199188cf684e8e", aum = 0)
    private I18NString bTP() {
        return bTx();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Occupation")
    @C0064Am(aul = "bf7ec665e238ac6c8541faa54f949558", aum = 0)
    /* renamed from: mj */
    private void m21291mj(I18NString i18NString) {
        m21290mi(i18NString);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Description")
    @C0064Am(aul = "d660bf77dcff6f7a5505e15b0e512b57", aum = 0)
    /* renamed from: vV */
    private I18NString m21293vV() {
        return m21292vT();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Description")
    @C0064Am(aul = "23de5654260d3ab4d10d3c10b4797d76", aum = 0)
    /* renamed from: bu */
    private void m21265bu(I18NString i18NString) {
        m21264br(i18NString);
    }

    @C3248pc(aYR = C3248pc.C3250b.ADDER, displayName = "Taikopedia Entries")
    @C0064Am(aul = "d87a38eba7dfc8b010da76031621211b", aum = 0)
    /* renamed from: a */
    private void m21252a(TaikopediaEntry aiz) {
        m21240Ll().add(aiz);
    }

    @C3248pc(aYR = C3248pc.C3250b.REMOVER, displayName = "Taikopedia Entries")
    @C0064Am(aul = "34eba3407916499c90004cce3ae05771", aum = 0)
    /* renamed from: c */
    private void m21266c(TaikopediaEntry aiz) {
        m21240Ll().remove(aiz);
    }

    @C0064Am(aul = "b5ce2da188d1fd9dca5e69008541b9e6", aum = 0)
    private C3438ra<TaikopediaEntry> bgE() {
        return m21240Ll();
    }

    @C3248pc(aYR = C3248pc.C3250b.ITERATOR, displayName = "Taikopedia Entries")
    @C0064Am(aul = "89690a9aae8661fac5d33f2f71f01a5d", aum = 0)
    private List<TaikopediaEntry> bgG() {
        return Collections.unmodifiableList(m21240Ll());
    }

    @aFW("Faction")
    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Faction")
    @C0064Am(aul = "23608a294a0f831be15d47da87334805", aum = 0)
    private Faction azM() {
        return azq();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Faction")
    @C0064Am(aul = "81674f5c8b4b54bbfc912096a0e97299", aum = 0)
    /* renamed from: m */
    private void m21289m(Faction xm) {
        m21286l(xm);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Is Boss")
    @C0064Am(aul = "ac9a52e330071cb51fdae874e7818930", aum = 0)
    private boolean bTR() {
        return bTy();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Is Boss")
    @C0064Am(aul = "dfa8f404a74087880fdb1cfc726451ce", aum = 0)
    /* renamed from: ee */
    private void m21277ee(boolean z) {
        m21276ed(z);
    }

    @C3248pc(aYR = C3248pc.C3250b.ADDER, displayName = "Item Gen Tables")
    @C0064Am(aul = "b0212f500e759daaf6a8df04fb79a4e7", aum = 0)
    /* renamed from: a */
    private void m21254a(ItemGenTable oqVar) {
        bTz().add(oqVar);
    }

    @C3248pc(aYR = C3248pc.C3250b.REMOVER, displayName = "Item Gen Tables")
    @C0064Am(aul = "149b305296cfe5e94650e9c0751cea4e", aum = 0)
    /* renamed from: c */
    private void m21268c(ItemGenTable oqVar) {
        bTz().remove(oqVar);
    }

    @C0064Am(aul = "fe70fe7000b0dd2864c8a8721ef1286d", aum = 0)
    private C3438ra<ItemGenTable> bTT() {
        return bTz();
    }

    @C3248pc(aYR = C3248pc.C3250b.ITERATOR, displayName = "Item Gen Tables")
    @C0064Am(aul = "8848803e67da874e1e5ea8955eca9ed7", aum = 0)
    private List<ItemGenTable> bTV() {
        return Collections.unmodifiableList(bTz());
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Reward")
    @C0064Am(aul = "d1cdb0cdbc63b9f8106d9611bef4dbae", aum = 0)
    private long bTX() {
        return bTA();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Reward")
    @C0064Am(aul = "3dc66a9e0d9652f16bd6774d7c474401", aum = 0)
    /* renamed from: gX */
    private void m21281gX(long j) {
        m21280gW(j);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Database Category")
    @C0064Am(aul = "ef4a7221bb96f8acc022ed0052f936de", aum = 0)
    /* renamed from: RX */
    private DatabaseCategory m21246RX() {
        return m21243RR();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Database Category")
    @C0064Am(aul = "8f831bb812820c7227ca70ac06920d27", aum = 0)
    /* renamed from: b */
    private void m21261b(DatabaseCategory aik) {
        m21251a(aik);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Pda Render")
    @C0064Am(aul = "571021f0f8ad74b25224a7c0b94a6e17", aum = 0)
    /* renamed from: Sd */
    private Asset m21247Sd() {
        return m21242RQ();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Pda Render")
    @C0064Am(aul = "56337580607d887b64c064990d4f34d4", aum = 0)
    /* renamed from: M */
    private void m21241M(Asset tCVar) {
        m21239L(tCVar);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Challenge Rating")
    @C0064Am(aul = "0785813a838d22078d6b6af887d5da95", aum = 0)
    /* renamed from: ls */
    private int m21288ls() {
        return m21287le();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Challenge Rating")
    @C0064Am(aul = "17e5fcdd64a85b88ebc7c433375756e4", aum = 0)
    /* renamed from: ax */
    private void m21259ax(int i) {
        m21258ar(i);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Pda M P Multiplier")
    @C0064Am(aul = "6916879631d7f085c145cc4daeeae351", aum = 0)
    /* renamed from: Sf */
    private float m21248Sf() {
        return m21244RS();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Pda M P Multiplier")
    @C0064Am(aul = "190652b072184876c7b96721cbaaf229", aum = 0)
    /* renamed from: cy */
    private void m21270cy(float f) {
        m21269cx(f);
    }

    @C0064Am(aul = "deccc0830daf3189f86adbc89e5878b8", aum = 0)
    private NPC bTZ() {
        return (NPC) mo745aU();
    }

    @C0064Am(aul = "58736f85f3babb9e2a49044902223bb4", aum = 0)
    /* renamed from: aT */
    private <T extends aDJ> T m21255aT() {
        T t = (NPC) bFf().mo6865M(NPC.class);
        t.mo11678y(this);
        return t;
    }
}
