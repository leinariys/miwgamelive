package game.script.advertise;

import game.network.message.externalizable.aCE;
import game.script.player.Player;
import logic.baa.C1616Xf;
import logic.baa.C1634Xv;
import logic.baa.C5511aMd;
import logic.baa.C6485anp;
import logic.data.mbean.C6029afB;
import logic.res.code.C5663aRz;
import logic.res.code.C6494any;
import logic.res.html.C2491fm;
import p001a.*;

import java.util.Collection;

@C5511aMd
@C6485anp
/* renamed from: a.axA */
/* compiled from: a */
public class AdvertiseViewTable extends TaikodomObject implements C1616Xf {
    public static final int _m_fieldCount = 0;
    public static final C5663aRz[] _m_fields = null;
    public static final int _m_methodCount = 0;
    public static final C2491fm[] _m_methods = null;
    public static final C2491fm gQd = null;
    public static final long serialVersionUID = 0;
    public static C6494any ___iScriptClass;

    static {
        m26992V();
    }

    public AdvertiseViewTable() {
        super((C5540aNg) null);
        super.mo10S();
    }

    public AdvertiseViewTable(C5540aNg ang) {
        super(ang);
    }

    /* renamed from: V */
    static void m26992V() {
        serialVersionUID = (long) 1;
        _m_fieldCount = TaikodomObject._m_fieldCount + 0;
        _m_methodCount = TaikodomObject._m_methodCount + 1;
        _m_fields = new C5663aRz[(TaikodomObject._m_fieldCount + 0)];
        C1634Xv.m11725a((Object[]) TaikodomObject._m_fields, (Object[]) _m_fields);
        int i = TaikodomObject._m_methodCount;
        C2491fm[] fmVarArr = new C2491fm[(i + 1)];
        C2491fm a = C4105zY.m41624a(AdvertiseViewTable.class, "3dc552c130a5c82b2a7898c8d28912da", i);
        gQd = a;
        fmVarArr[i] = a;
        int i2 = i + 1;
        _m_methods = fmVarArr;
        C1634Xv.m11725a((Object[]) TaikodomObject._m_methods, (Object[]) _m_methods);
        ___iScriptClass = aUO.m18566a(AdvertiseViewTable.class, C6029afB.class, _m_fields, _m_methods);
    }

    @C4034yP
    @C5566aOg
    @C0064Am(aul = "3dc552c130a5c82b2a7898c8d28912da", aum = 0)
    @C2499fr
    /* renamed from: a */
    private void m26993a(String str, Player aku, long j) {
        throw new aWi(new aCE(this, gQd, new Object[]{str, aku, new Long(j)}));
    }

    /* renamed from: T */
    public C6494any mo11T() {
        return ___iScriptClass;
    }

    /* renamed from: U */
    public C2491fm[] mo12U() {
        return _m_methods;
    }

    /* renamed from: W */
    public Object mo13W() {
        return new C6029afB(this);
    }

    /* renamed from: a */
    public Object mo14a(C0495Gr gr) {
        Object[] args = gr.getArgs();
        switch (gr.mo2417hq() - TaikodomObject._m_methodCount) {
            case 0:
                m26993a((String) args[0], (Player) args[1], ((Long) args[2]).longValue());
                return null;
            default:
                return super.mo14a(gr);
        }
    }

    /* renamed from: a */
    public void mo15a(Collection collection, C0495Gr gr) {
        super.mo15a(collection, gr);
    }

    @C4034yP
    @C5566aOg
    @C2499fr
    /* renamed from: b */
    public void mo16737b(String str, Player aku, long j) {
        switch (bFf().mo6893i(gQd)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, gQd, new Object[]{str, aku, new Long(j)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, gQd, new Object[]{str, aku, new Long(j)}));
                break;
        }
        m26993a(str, aku, j);
    }

    /* renamed from: c */
    public C5663aRz[] mo25c() {
        return _m_fields;
    }

    /* renamed from: S */
    public void mo10S() {
        super.mo10S();
    }
}
