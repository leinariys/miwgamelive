package game.script.advertise;

import com.hoplon.geometry.Vec3f;
import game.geometry.Vec3d;
import game.network.message.externalizable.aCE;
import game.script.space.Advertise;
import logic.baa.C1616Xf;
import logic.baa.C1634Xv;
import logic.baa.C5511aMd;
import logic.baa.C6485anp;
import logic.data.mbean.C6290akC;
import logic.render.IEngineGraphics;
import logic.res.code.C5640aRc;
import logic.res.code.C5663aRz;
import logic.res.code.C6494any;
import logic.res.html.C2491fm;
import logic.res.sound.C0907NJ;
import org.mozilla1.classfile.C0147Bi;
import p001a.*;
import taikodom.render.BinkTexture;
import taikodom.render.camera.Camera;
import taikodom.render.loader.RenderAsset;
import taikodom.render.loader.provider.C3176ol;
import taikodom.render.scene.RenderObject;
import taikodom.render.scene.Scene;
import taikodom.render.scene.SceneObject;
import taikodom.render.textures.BaseTexture;
import taikodom.render.textures.FlashTexture;

import javax.vecmath.Tuple3d;
import java.io.File;
import java.io.FilenameFilter;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

@C5511aMd
@C6485anp
@ClientOnly
/* renamed from: a.PY */
/* compiled from: a */
public class AdvertiseManager extends TaikodomObject implements C0907NJ, C1616Xf {
    public static final C2491fm _f_dispose_0020_0028_0029V = null;
    public static final C2491fm _f_tick_0020_0028F_0029V = null;
    public static final int _m_fieldCount = 0;
    public static final C5663aRz[] _m_fields = null;
    public static final int _m_methodCount = 0;
    public static final C2491fm[] _m_methods = null;
    public static final C2491fm bdM = null;
    @ClientOnly
    public static final String dTA = "data/gfx/misc/dynamic_advertise_material.pro";
    @ClientOnly
    public static final String dTB = "dynamic_advertise_material";
    public static final float dTC = 1.0f;
    public static final C5663aRz dTG = null;
    public static final C2491fm dTK = null;
    public static final C2491fm dTL = null;
    public static final C2491fm dTM = null;
    public static final C2491fm dTN = null;
    public static final C2491fm dTO = null;
    public static final C2491fm dTP = null;
    public static final C2491fm dTQ = null;
    public static final C2491fm dTR = null;
    public static final C2491fm dTS = null;
    public static final C2491fm dTT = null;
    public static final C2491fm dmF = null;
    public static final long serialVersionUID = 0;
    private static final int dTy = 112;
    private static final float dTz = 0.1f;
    public static C6494any ___iScriptClass = null;
    @C0064Am(aul = "55ebc4af0f1ea011e75b35024dfff7aa", aum = 0)
    private static boolean dTF = false;

    static {
        m8491V();
    }

    private transient List<Advertise> dTD;
    private transient List<String> dTE;
    @ClientOnly
    private AdvertiseViewTable dTH;
    @ClientOnly
    private int dTI;
    @ClientOnly
    private transient long dTJ;
    @ClientOnly
    private transient BaseTexture texture;

    public AdvertiseManager() {
        super((C5540aNg) null);
        super.mo10S();
    }

    public AdvertiseManager(C5540aNg ang) {
        super(ang);
    }

    /* renamed from: V */
    static void m8491V() {
        serialVersionUID = (long) 1;
        _m_fieldCount = TaikodomObject._m_fieldCount + 1;
        _m_methodCount = TaikodomObject._m_methodCount + 14;
        int i = TaikodomObject._m_fieldCount;
        C5663aRz[] arzArr = new C5663aRz[(i + 1)];
        C5663aRz b = C5640aRc.m17844b(AdvertiseManager.class, "55ebc4af0f1ea011e75b35024dfff7aa", i);
        dTG = b;
        arzArr[i] = b;
        int i2 = i + 1;
        _m_fields = arzArr;
        C1634Xv.m11725a((Object[]) TaikodomObject._m_fields, (Object[]) _m_fields);
        int i3 = TaikodomObject._m_methodCount;
        C2491fm[] fmVarArr = new C2491fm[(i3 + 14)];
        C2491fm a = C4105zY.m41624a(AdvertiseManager.class, "c04cdc9bd32c28701a77ba20d2374d30", i3);
        bdM = a;
        fmVarArr[i3] = a;
        int i4 = i3 + 1;
        C2491fm a2 = C4105zY.m41624a(AdvertiseManager.class, "93dc1fc1146e7947c0466890a173c9e1", i4);
        dTK = a2;
        fmVarArr[i4] = a2;
        int i5 = i4 + 1;
        C2491fm a3 = C4105zY.m41624a(AdvertiseManager.class, "1fddabfea0d49234b2460fdaaa3f5515", i5);
        _f_dispose_0020_0028_0029V = a3;
        fmVarArr[i5] = a3;
        int i6 = i5 + 1;
        C2491fm a4 = C4105zY.m41624a(AdvertiseManager.class, "7cac5dfb2f32d65b58d34bd087c0d218", i6);
        dTL = a4;
        fmVarArr[i6] = a4;
        int i7 = i6 + 1;
        C2491fm a5 = C4105zY.m41624a(AdvertiseManager.class, "e4142cd9c93791b50556c32d467dfe47", i7);
        dTM = a5;
        fmVarArr[i7] = a5;
        int i8 = i7 + 1;
        C2491fm a6 = C4105zY.m41624a(AdvertiseManager.class, "ce472c0df21ebf1398996a59ab35fbb9", i8);
        dTN = a6;
        fmVarArr[i8] = a6;
        int i9 = i8 + 1;
        C2491fm a7 = C4105zY.m41624a(AdvertiseManager.class, "f40e1b476a12092363014e6af4544acc", i9);
        dTO = a7;
        fmVarArr[i9] = a7;
        int i10 = i9 + 1;
        C2491fm a8 = C4105zY.m41624a(AdvertiseManager.class, "6deb98eac137fd810ae27b9aaaf54403", i10);
        dTP = a8;
        fmVarArr[i10] = a8;
        int i11 = i10 + 1;
        C2491fm a9 = C4105zY.m41624a(AdvertiseManager.class, "f6ab6bd8dd9a30ebc79418faf45a338a", i11);
        dTQ = a9;
        fmVarArr[i11] = a9;
        int i12 = i11 + 1;
        C2491fm a10 = C4105zY.m41624a(AdvertiseManager.class, "a1545360769c871858700ad0ef1dee50", i12);
        dmF = a10;
        fmVarArr[i12] = a10;
        int i13 = i12 + 1;
        C2491fm a11 = C4105zY.m41624a(AdvertiseManager.class, "8a28250fb7cc00dd01d62bbe50911fc3", i13);
        dTR = a11;
        fmVarArr[i13] = a11;
        int i14 = i13 + 1;
        C2491fm a12 = C4105zY.m41624a(AdvertiseManager.class, "e80483b94d5d42a407b2473543b6c952", i14);
        dTS = a12;
        fmVarArr[i14] = a12;
        int i15 = i14 + 1;
        C2491fm a13 = C4105zY.m41624a(AdvertiseManager.class, "b88495d69113ecd671aa67a5cd3266c4", i15);
        _f_tick_0020_0028F_0029V = a13;
        fmVarArr[i15] = a13;
        int i16 = i15 + 1;
        C2491fm a14 = C4105zY.m41624a(AdvertiseManager.class, "dbd46e07d44dcd5acdd226af104726cc", i16);
        dTT = a14;
        fmVarArr[i16] = a14;
        int i17 = i16 + 1;
        _m_methods = fmVarArr;
        C1634Xv.m11725a((Object[]) TaikodomObject._m_methods, (Object[]) _m_methods);
        ___iScriptClass = aUO.m18566a(AdvertiseManager.class, C6290akC.class, _m_fields, _m_methods);
    }

    @ClientOnly
    /* renamed from: b */
    private void m8495b(SceneObject sceneObject, BaseTexture baseTexture) {
        switch (bFf().mo6893i(dTQ)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, dTQ, new Object[]{sceneObject, baseTexture}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, dTQ, new Object[]{sceneObject, baseTexture}));
                break;
        }
        m8493a(sceneObject, baseTexture);
    }

    private boolean boT() {
        return bFf().mo5608dq().mo3201h(dTG);
    }

    @ClientOnly
    private void boV() {
        switch (bFf().mo6893i(dTK)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, dTK, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, dTK, new Object[0]));
                break;
        }
        boU();
    }

    @ClientOnly
    private long boX() {
        switch (bFf().mo6893i(dTM)) {
            case 0:
                return 0;
            case 2:
                return ((Long) bFf().mo5606d(new aCE(this, dTM, new Object[0]))).longValue();
            case 3:
                bFf().mo5606d(new aCE(this, dTM, new Object[0]));
                break;
        }
        return boW();
    }

    @ClientOnly
    private void boZ() {
        switch (bFf().mo6893i(dTN)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, dTN, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, dTN, new Object[0]));
                break;
        }
        boY();
    }

    @ClientOnly
    private void bpb() {
        switch (bFf().mo6893i(dTR)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, dTR, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, dTR, new Object[0]));
                break;
        }
        bpa();
    }

    /* renamed from: di */
    private void m8496di(boolean z) {
        bFf().mo5608dq().mo3153a(dTG, z);
    }

    private boolean isPlaying() {
        switch (bFf().mo6893i(dTT)) {
            case 0:
                return false;
            case 2:
                return ((Boolean) bFf().mo5606d(new aCE(this, dTT, new Object[0]))).booleanValue();
            case 3:
                bFf().mo5606d(new aCE(this, dTT, new Object[0]));
                break;
        }
        return bpd();
    }

    @ClientOnly
    private void stopLogging() {
        switch (bFf().mo6893i(dTS)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, dTS, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, dTS, new Object[0]));
                break;
        }
        bpc();
    }

    /* renamed from: T */
    public C6494any mo11T() {
        return ___iScriptClass;
    }

    /* renamed from: U */
    public C2491fm[] mo12U() {
        return _m_methods;
    }

    /* renamed from: V */
    public void mo4709V(float f) {
        switch (bFf().mo6893i(_f_tick_0020_0028F_0029V)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, _f_tick_0020_0028F_0029V, new Object[]{new Float(f)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, _f_tick_0020_0028F_0029V, new Object[]{new Float(f)}));
                break;
        }
        m8490U(f);
    }

    /* renamed from: W */
    public Object mo13W() {
        return new C6290akC(this);
    }

    /* renamed from: a */
    public Object mo14a(C0495Gr gr) {
        Object[] args = gr.getArgs();
        switch (gr.mo2417hq() - TaikodomObject._m_methodCount) {
            case 0:
                m8492a((Advertise) args[0]);
                return null;
            case 1:
                boU();
                return null;
            case 2:
                m8497fg();
                return null;
            case 3:
                m8499j((SceneObject) args[0]);
                return null;
            case 4:
                return new Long(boW());
            case 5:
                boY();
                return null;
            case 6:
                m8500q((Collection) args[0]);
                return null;
            case 7:
                m8498hn(((Float) args[0]).floatValue());
                return null;
            case 8:
                m8493a((SceneObject) args[0], (BaseTexture) args[1]);
                return null;
            case 9:
                m8494b((RenderAsset) args[0]);
                return null;
            case 10:
                bpa();
                return null;
            case 11:
                bpc();
                return null;
            case 12:
                m8490U(((Float) args[0]).floatValue());
                return null;
            case 13:
                return new Boolean(bpd());
            default:
                return super.mo14a(gr);
        }
    }

    /* renamed from: a */
    public void mo15a(Collection collection, C0495Gr gr) {
        super.mo15a(collection, gr);
    }

    /* renamed from: a */
    public void mo968a(RenderAsset renderAsset) {
        switch (bFf().mo6893i(dmF)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, dmF, new Object[]{renderAsset}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, dmF, new Object[]{renderAsset}));
                break;
        }
        m8494b(renderAsset);
    }

    @ClientOnly
    /* renamed from: b */
    public void mo4710b(Advertise we) {
        switch (bFf().mo6893i(bdM)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, bdM, new Object[]{we}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, bdM, new Object[]{we}));
                break;
        }
        m8492a(we);
    }

    /* renamed from: c */
    public C5663aRz[] mo25c() {
        return _m_fields;
    }

    @ClientOnly
    public void dispose() {
        switch (bFf().mo6893i(_f_dispose_0020_0028_0029V)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, _f_dispose_0020_0028_0029V, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, _f_dispose_0020_0028_0029V, new Object[0]));
                break;
        }
        m8497fg();
    }

    @ClientOnly
    /* renamed from: ho */
    public void mo4711ho(float f) {
        switch (bFf().mo6893i(dTP)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, dTP, new Object[]{new Float(f)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, dTP, new Object[]{new Float(f)}));
                break;
        }
        m8498hn(f);
    }

    /* renamed from: k */
    public void mo4712k(SceneObject sceneObject) {
        switch (bFf().mo6893i(dTL)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, dTL, new Object[]{sceneObject}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, dTL, new Object[]{sceneObject}));
                break;
        }
        m8499j(sceneObject);
    }

    @ClientOnly
    /* renamed from: r */
    public void mo4713r(Collection<Advertise> collection) {
        switch (bFf().mo6893i(dTO)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, dTO, new Object[]{collection}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, dTO, new Object[]{collection}));
                break;
        }
        m8500q(collection);
    }

    @ClientOnly
    /* renamed from: S */
    public void mo10S() {
        super.mo10S();
        m8496di(false);
        this.dTI = 0;
        boV();
        this.dTH = C5916acs.getSingolton().getTaikodom().aJw();
    }

    @C0064Am(aul = "c04cdc9bd32c28701a77ba20d2374d30", aum = 0)
    @ClientOnly
    /* renamed from: a */
    private void m8492a(Advertise we) {
        this.dTD.add(we);
    }

    @C0064Am(aul = "93dc1fc1146e7947c0466890a173c9e1", aum = 0)
    @ClientOnly
    private void boU() {
        this.dTE = new ArrayList();
        File[] listFiles = new File(String.valueOf(ald().ale().aen().getPath()) + File.separator + ald().ala().aKY().get()).listFiles(new C1063a());
        if (listFiles != null) {
            String str = ald().ala().aKY().get();
            for (File name : listFiles) {
                String name2 = name.getName();
                if (!str.endsWith("\\") && !str.endsWith(C0147Bi.SEPARATOR)) {
                    str = String.valueOf(str) + C0147Bi.SEPARATOR;
                }
                this.dTE.add(String.valueOf(str) + name2);
            }
        }
    }

    @C0064Am(aul = "1fddabfea0d49234b2460fdaaa3f5515", aum = 0)
    @ClientOnly
    /* renamed from: fg */
    private void m8497fg() {
        this.dTD.clear();
    }

    @C0064Am(aul = "7cac5dfb2f32d65b58d34bd087c0d218", aum = 0)
    /* renamed from: j */
    private void m8499j(SceneObject sceneObject) {
        if (sceneObject == null) {
            return;
        }
        if ((this.texture instanceof BinkTexture) || (this.texture instanceof FlashTexture)) {
            m8495b(sceneObject, this.texture);
        }
    }

    @C0064Am(aul = "e4142cd9c93791b50556c32d467dfe47", aum = 0)
    @ClientOnly
    private long boW() {
        return cVr() - this.dTJ;
    }

    @C0064Am(aul = "ce472c0df21ebf1398996a59ab35fbb9", aum = 0)
    @ClientOnly
    private void boY() {
        stopLogging();
        if (!this.dTE.isEmpty()) {
            String str = this.dTE.get(this.dTI);
            ald().alg().mo4995a(str, str, (Scene) null, this, "AdvertiseManager:loadAdvertiseIntoMaterial ");
            this.dTI++;
            if (this.dTI >= this.dTE.size()) {
                this.dTI = 0;
            }
        }
    }

    @C0064Am(aul = "f40e1b476a12092363014e6af4544acc", aum = 0)
    @ClientOnly
    /* renamed from: q */
    private void m8500q(Collection<Advertise> collection) {
        if (this.dTD == null) {
            this.dTD = new ArrayList();
        }
        this.dTD.clear();
        if (collection != null && collection.size() != 0) {
            this.dTD.addAll(collection);
            mo4711ho(1.0f);
            boZ();
        }
    }

    @C0064Am(aul = "6deb98eac137fd810ae27b9aaaf54403", aum = 0)
    @ClientOnly
    /* renamed from: hn */
    private void m8498hn(float f) {
        mo8361ms(1.0f);
    }

    @C0064Am(aul = "f6ab6bd8dd9a30ebc79418faf45a338a", aum = 0)
    @ClientOnly
    /* renamed from: a */
    private void m8493a(SceneObject sceneObject, BaseTexture baseTexture) {
        if (sceneObject instanceof RenderObject) {
            RenderObject renderObject = (RenderObject) sceneObject;
            if (renderObject.getMaterial() != null && renderObject.getMaterial().getName().equals(dTB)) {
                renderObject.getMaterial().setDiffuseTexture(baseTexture);
            }
        }
        for (SceneObject b : sceneObject.getChildren()) {
            m8495b(b, baseTexture);
        }
    }

    @C0064Am(aul = "a1545360769c871858700ad0ef1dee50", aum = 0)
    /* renamed from: b */
    private void m8494b(RenderAsset renderAsset) {
        if (renderAsset == null) {
            return;
        }
        if ((renderAsset instanceof BinkTexture) || (renderAsset instanceof FlashTexture)) {
            for (Advertise next : this.dTD) {
                if (next.cHg() != null) {
                    m8495b(next.cHg(), (BaseTexture) renderAsset);
                }
            }
            this.texture = (BaseTexture) renderAsset;
            if (renderAsset instanceof BinkTexture) {
                ((BinkTexture) this.texture).play();
            } else if (renderAsset instanceof FlashTexture) {
                C3176ol player = ((FlashTexture) this.texture).getPlayer();
                player.play();
                player.mo21041aL(false);
            }
        }
    }

    @C0064Am(aul = "8a28250fb7cc00dd01d62bbe50911fc3", aum = 0)
    @ClientOnly
    private void bpa() {
        this.dTJ = cVr();
        m8496di(true);
    }

    @C0064Am(aul = "e80483b94d5d42a407b2473543b6c952", aum = 0)
    @ClientOnly
    private void bpc() {
        if (boT()) {
            long boX = boX();
            if (this.texture != null && ((float) boX) > dTz) {
                this.dTH.mo16737b(this.texture.getName(), ald().mo4089dL(), boX);
            }
            this.dTJ = 0;
            m8496di(false);
        }
    }

    @C0064Am(aul = "b88495d69113ecd671aa67a5cd3266c4", aum = 0)
    /* renamed from: U */
    private void m8490U(float f) {
        boolean z;
        super.mo4709V(f);
        mo4711ho(1.0f);
        IEngineGraphics ale = ald().ale();
        Camera adY = ale.adY();
        Vec3d position = adY.getPosition();
        Vec3f vectorZ = adY.getTransform().getVectorZ();
        vectorZ.negate();
        Iterator<Advertise> it = this.dTD.iterator();
        while (true) {
            if (!it.hasNext()) {
                z = false;
                break;
            }
            Advertise next = it.next();
            if (next.cLN()) {
                Vec3d ajr = next.cLG().position;
                Vec3f vectorY = next.cLG().getVectorY();
                Vec3d f2 = ajr.mo9517f((Tuple3d) position);
                if (ale.aea().getSizeOnScreen(next.cHg().getGlobalTransform().position, next.cHg().getAabbWSLenght()) >= 112.0f) {
                    f2.normalize();
                    Vec3f dfV = f2.dfV();
                    float dot = vectorZ.dot(dfV);
                    vectorY.normalize();
                    float dot2 = vectorY.dot(dfV);
                    if (dot > 0.7f && ((double) Math.abs(dot2)) < 0.7d) {
                        z = true;
                        break;
                    }
                } else {
                    continue;
                }
            }
        }
        if (!z) {
            stopLogging();
        } else if (!boT()) {
            bpb();
        }
        if (this.texture != null && !isPlaying()) {
            boZ();
        }
    }

    @C0064Am(aul = "dbd46e07d44dcd5acdd226af104726cc", aum = 0)
    private boolean bpd() {
        if (this.texture instanceof BinkTexture) {
            return ((BinkTexture) this.texture).isPlaying();
        }
        if (!(this.texture instanceof FlashTexture)) {
            return false;
        }
        FlashTexture flashTexture = (FlashTexture) this.texture;
        return flashTexture.getPlayer().isPlaying() && !flashTexture.getPlayer().isFinished();
    }

    /* renamed from: a.PY$a */
    class C1063a implements FilenameFilter {
        C1063a() {
        }

        public boolean accept(File file, String str) {
            return str.endsWith(".bik") || str.endsWith(".swf");
        }
    }
}
