package game.script;

import com.hoplon.geometry.Vec3f;
import com.hoplon.taikodom.common.TaikodomVersion;
import game.engine.DataGameEvent;
import game.engine.IEngineGame;
import game.geometry.Vec3d;
import game.network.channel.Connect;
import game.network.channel.client.ClientConnect;
import game.network.exception.C1728ZX;
import game.network.message.C5581aOv;
import game.network.message.externalizable.C2592hH;
import game.network.message.externalizable.aCE;
import game.network.message.serializable.C1556Wo;
import game.network.message.serializable.C2686iZ;
import game.network.message.serializable.C3438ra;
import game.script.advertise.AdvertiseViewTable;
import game.script.ai.npc.AIControllerType;
import game.script.avatar.AvatarManager;
import game.script.backdoor.Backdoor;
import game.script.bank.Bank;
import game.script.citizenship.*;
import game.script.connect.UsersQueueManager;
import game.script.consignment.ConsignmentFeeManager;
import game.script.content.ContentImporterExporter;
import game.script.contract.ContractBoard;
import game.script.corporation.Corporation;
import game.script.corporation.CorporationReservedNames;
import game.script.damage.DamageType;
import game.script.faction.Faction;
import game.script.hazardarea.HazardArea.HazardAreaType;
import game.script.insurance.Insurer;
import game.script.item.*;
import game.script.item.buff.module.AttributeBuffType;
import game.script.itembilling.ItemBilling;
import game.script.itemgen.ItemGenSet;
import game.script.login.UserConnection;
import game.script.main.bot.BotScript;
import game.script.mission.MissionTemplate;
import game.script.newmarket.Market;
import game.script.newmarket.economy.VirtualWarehouse;
import game.script.newmarket.store.GlobalEconomyInfo;
import game.script.nls.NLSManager;
import game.script.npc.NPCType;
import game.script.offload.OffloadUtils;
import game.script.pda.DatabaseCategory;
import game.script.pda.TaikopediaEntry;
import game.script.player.Player;
import game.script.player.PlayerFinder;
import game.script.player.PlayerReservedNames;
import game.script.player.User;
import game.script.progression.Progression;
import game.script.resource.Asset;
import game.script.resource.AssetGroup;
import game.script.ship.*;
import game.script.simulation.Space;
import game.script.space.*;
import game.script.spacezone.AsteroidZoneCategory;
import game.script.spacezone.SpaceZoneTypesManager;
import game.script.stats.StatsManager;
import game.script.tutorial.Tutorial;
import game.script.util.ServerEditorUtility;
import logic.baa.*;
import logic.data.mbean.aNG;
import logic.res.KeyCode;
import logic.res.code.C5640aRc;
import logic.res.code.C5663aRz;
import logic.res.code.C6494any;
import logic.res.code.DataGameEventImpl;
import logic.res.html.C2491fm;
import logic.res.html.C6663arL;
import logic.res.html.MessageContainer;
import logic.thred.LogPrinter;
import logic.thred.PoolThread;
import org.apache.commons.logging.Log;
import org.mozilla1.javascript.ScriptRuntime;
import p001a.*;
import taikodom.geom.Orientation;
import taikodom.infra.script.I18NString;

import javax.vecmath.Tuple3d;
import java.util.*;

@C6968axh
@C5511aMd
@C6485anp
/* renamed from: a.DN */
/* compiled from: a */
public class Taikodom extends TaikodomObject implements C0468GU, C0943Nq, C1616Xf {

    /* renamed from: Ml */
    public static final C5663aRz f397Ml = null;
    /* renamed from: OY */
    public static final C5663aRz f399OY = null;
    /* renamed from: Pi */
    public static final C2491fm f400Pi = null;
    /* renamed from: Pj */
    public static final C2491fm f401Pj = null;
    /* renamed from: Pk */
    public static final C2491fm f402Pk = null;
    /* renamed from: Pl */
    public static final C2491fm f403Pl = null;
    /* renamed from: _f_getGameIO_0020_0028_0029Ltaikodom_002finfra_002fclient_002fIGameIO_003b */
    public static final C2491fm f404x3cb2b465 = null;
    /* renamed from: _f_getUserConnection_0020_0028_0029Ltaikodom_002fgame_002fscript_002flogin_002fUserConnection_003b */
    public static final C2491fm f405x20176b18 = null;
    public static final C2491fm _f_onResurrect_0020_0028_0029V = null;
    public static final C2491fm _f_toString_0020_0028_0029Ljava_002flang_002fString_003b = null;
    public static final int _m_fieldCount = 0;
    public static final C5663aRz[] _m_fields = null;
    public static final int _m_methodCount = 0;
    public static final C2491fm[] _m_methods = null;
    public static final C2491fm aKm = null;
    public static final C2491fm aMk = null;
    public static final C2491fm aMl = null;
    public static final C2491fm aMm = null;
    public static final C5663aRz aPq = null;
    public static final C2491fm aPv = null;
    public static final C5663aRz aQU = null;
    public static final C5663aRz aQn = null;
    public static final C2491fm aQo = null;
    public static final C2491fm aQp = null;
    public static final C2491fm aQq = null;
    public static final C5663aRz awd = null;
    public static final C5663aRz bCb = null;
    /* renamed from: bL */
    public static final C5663aRz f407bL = null;
    /* renamed from: bN */
    public static final C2491fm f408bN = null;
    /* renamed from: bO */
    public static final C2491fm f409bO = null;
    /* renamed from: bP */
    public static final C2491fm f410bP = null;
    public static final C5663aRz bok = null;
    public static final C5663aRz cIZ = null;
    public static final C5663aRz cJB = null;
    public static final C5663aRz cJD = null;
    public static final C5663aRz cJF = null;
    public static final C5663aRz cJH = null;
    public static final C5663aRz cJJ = null;
    public static final C5663aRz cJL = null;
    public static final C5663aRz cJN = null;
    public static final C5663aRz cJP = null;
    public static final C5663aRz cJR = null;
    public static final C5663aRz cJT = null;
    public static final C5663aRz cJV = null;
    public static final C5663aRz cJX = null;
    public static final C5663aRz cJZ = null;
    public static final C5663aRz cJb = null;
    public static final C5663aRz cJd = null;
    public static final C5663aRz cJf = null;
    public static final C5663aRz cJh = null;
    public static final C5663aRz cJj = null;
    public static final C5663aRz cJl = null;
    public static final C5663aRz cJn = null;
    public static final C5663aRz cJp = null;
    public static final C5663aRz cJr = null;
    public static final C5663aRz cJt = null;
    public static final C5663aRz cJv = null;
    public static final C5663aRz cJx = null;
    public static final C5663aRz cJz = null;
    public static final C5663aRz cKB = null;
    public static final C5663aRz cKE = null;
    public static final C5663aRz cKG = null;
    public static final C5663aRz cKI = null;
    public static final C5663aRz cKK = null;
    public static final C5663aRz cKM = null;
    public static final C5663aRz cKO = null;
    public static final C5663aRz cKQ = null;
    public static final C5663aRz cKS = null;
    public static final C5663aRz cKU = null;
    public static final C5663aRz cKW = null;
    public static final C5663aRz cKY = null;
    public static final C5663aRz cKb = null;
    public static final C5663aRz cKd = null;
    public static final C5663aRz cKf = null;
    public static final C5663aRz cKh = null;
    public static final C5663aRz cKj = null;
    public static final C5663aRz cKl = null;
    public static final C5663aRz cKn = null;
    public static final C5663aRz cKp = null;
    public static final C5663aRz cKr = null;
    public static final C5663aRz cKt = null;
    public static final C5663aRz cKv = null;
    public static final C5663aRz cKx = null;
    public static final C5663aRz cKz = null;
    public static final C5663aRz cLB = null;
    public static final C5663aRz cLD = null;
    public static final C5663aRz cLF = null;
    public static final C5663aRz cLH = null;
    public static final C5663aRz cLJ = null;
    public static final C5663aRz cLL = null;
    public static final C5663aRz cLN = null;
    public static final C5663aRz cLP = null;
    public static final C5663aRz cLR = null;
    public static final C5663aRz cLT = null;
    public static final C5663aRz cLV = null;
    public static final C5663aRz cLX = null;
    public static final C5663aRz cLZ = null;
    public static final C5663aRz cLa = null;
    public static final C5663aRz cLc = null;
    public static final C5663aRz cLe = null;
    public static final C5663aRz cLg = null;
    public static final C5663aRz cLi = null;
    public static final C5663aRz cLk = null;
    public static final C5663aRz cLn = null;
    public static final C5663aRz cLp = null;
    public static final C5663aRz cLr = null;
    public static final C5663aRz cLt = null;
    public static final C5663aRz cLv = null;
    public static final C5663aRz cLx = null;
    public static final C5663aRz cLz = null;
    public static final C2491fm cMA = null;
    public static final C2491fm cMB = null;
    public static final C2491fm cMC = null;
    public static final C2491fm cMD = null;
    public static final C2491fm cME = null;
    public static final C2491fm cMF = null;
    public static final C2491fm cMG = null;
    public static final C2491fm cMH = null;
    public static final C2491fm cMI = null;
    public static final C2491fm cMJ = null;
    public static final C2491fm cMK = null;
    public static final C2491fm cML = null;
    public static final C2491fm cMM = null;
    public static final C2491fm cMN = null;
    public static final C2491fm cMO = null;
    public static final C2491fm cMP = null;
    public static final C2491fm cMQ = null;
    public static final C2491fm cMR = null;
    public static final C2491fm cMS = null;
    public static final C2491fm cMT = null;
    public static final C2491fm cMU = null;
    public static final C2491fm cMV = null;
    public static final C2491fm cMW = null;
    public static final C2491fm cMX = null;
    public static final C2491fm cMY = null;
    public static final C2491fm cMZ = null;
    public static final C5663aRz cMb = null;
    public static final C5663aRz cMd = null;
    public static final C5663aRz cMf = null;
    public static final C2491fm cMh = null;
    public static final C2491fm cMi = null;
    public static final C2491fm cMj = null;
    public static final C2491fm cMk = null;
    public static final C2491fm cMl = null;
    public static final C2491fm cMm = null;
    public static final C2491fm cMn = null;
    public static final C2491fm cMo = null;
    public static final C2491fm cMp = null;
    public static final C2491fm cMq = null;
    public static final C2491fm cMr = null;
    public static final C2491fm cMs = null;
    public static final C2491fm cMt = null;
    public static final C2491fm cMu = null;
    public static final C2491fm cMv = null;
    public static final C2491fm cMw = null;
    public static final C2491fm cMx = null;
    public static final C2491fm cMy = null;
    public static final C2491fm cMz = null;
    public static final C2491fm cNA = null;
    public static final C2491fm cNB = null;
    public static final C2491fm cNC = null;
    public static final C2491fm cND = null;
    public static final C2491fm cNE = null;
    public static final C2491fm cNF = null;
    public static final C2491fm cNG = null;
    public static final C2491fm cNH = null;
    public static final C2491fm cNI = null;
    public static final C2491fm cNJ = null;
    public static final C2491fm cNK = null;
    public static final C2491fm cNL = null;
    public static final C2491fm cNM = null;
    public static final C2491fm cNN = null;
    public static final C2491fm cNO = null;
    public static final C2491fm cNP = null;
    public static final C2491fm cNQ = null;
    public static final C2491fm cNR = null;
    public static final C2491fm cNS = null;
    public static final C2491fm cNT = null;
    public static final C2491fm cNU = null;
    public static final C2491fm cNV = null;
    public static final C2491fm cNW = null;
    public static final C2491fm cNX = null;
    public static final C2491fm cNY = null;
    public static final C2491fm cNZ = null;
    public static final C2491fm cNa = null;
    public static final C2491fm cNb = null;
    public static final C2491fm cNc = null;
    public static final C2491fm cNd = null;
    public static final C2491fm cNe = null;
    public static final C2491fm cNf = null;
    public static final C2491fm cNg = null;
    public static final C2491fm cNh = null;
    public static final C2491fm cNi = null;
    public static final C2491fm cNj = null;
    public static final C2491fm cNk = null;
    public static final C2491fm cNl = null;
    public static final C2491fm cNm = null;
    public static final C2491fm cNn = null;
    public static final C2491fm cNo = null;
    public static final C2491fm cNp = null;
    public static final C2491fm cNq = null;
    public static final C2491fm cNr = null;
    public static final C2491fm cNs = null;
    public static final C2491fm cNt = null;
    public static final C2491fm cNu = null;
    public static final C2491fm cNv = null;
    public static final C2491fm cNw = null;
    public static final C2491fm cNx = null;
    public static final C2491fm cNy = null;
    public static final C2491fm cNz = null;
    public static final C2491fm cOA = null;
    public static final C2491fm cOB = null;
    public static final C2491fm cOC = null;
    public static final C2491fm cOD = null;
    public static final C2491fm cOE = null;
    public static final C2491fm cOF = null;
    public static final C2491fm cOG = null;
    public static final C2491fm cOH = null;
    public static final C2491fm cOI = null;
    public static final C2491fm cOJ = null;
    public static final C2491fm cOK = null;
    public static final C2491fm cOL = null;
    public static final C2491fm cOM = null;
    public static final C2491fm cON = null;
    public static final C2491fm cOO = null;
    public static final C2491fm cOP = null;
    public static final C2491fm cOQ = null;
    public static final C2491fm cOR = null;
    public static final C2491fm cOS = null;
    public static final C2491fm cOT = null;
    public static final C2491fm cOU = null;
    public static final C2491fm cOV = null;
    public static final C2491fm cOW = null;
    public static final C2491fm cOX = null;
    public static final C2491fm cOY = null;
    public static final C2491fm cOZ = null;
    public static final C2491fm cOa = null;
    public static final C2491fm cOb = null;
    public static final C2491fm cOc = null;
    public static final C2491fm cOd = null;
    public static final C2491fm cOe = null;
    public static final C2491fm cOf = null;
    public static final C2491fm cOg = null;
    public static final C2491fm cOh = null;
    public static final C2491fm cOi = null;
    public static final C2491fm cOj = null;
    public static final C2491fm cOk = null;
    public static final C2491fm cOl = null;
    public static final C2491fm cOm = null;
    public static final C2491fm cOn = null;
    public static final C2491fm cOo = null;
    public static final C2491fm cOp = null;
    public static final C2491fm cOq = null;
    public static final C2491fm cOr = null;
    public static final C2491fm cOs = null;
    public static final C2491fm cOt = null;
    public static final C2491fm cOu = null;
    public static final C2491fm cOv = null;
    public static final C2491fm cOw = null;
    public static final C2491fm cOx = null;
    public static final C2491fm cOy = null;
    public static final C2491fm cOz = null;
    public static final C2491fm cPA = null;
    public static final C2491fm cPB = null;
    public static final C2491fm cPC = null;
    public static final C2491fm cPD = null;
    public static final C2491fm cPE = null;
    public static final C2491fm cPF = null;
    public static final C2491fm cPG = null;
    public static final C2491fm cPH = null;
    public static final C2491fm cPI = null;
    public static final C2491fm cPJ = null;
    public static final C2491fm cPK = null;
    public static final C2491fm cPL = null;
    public static final C2491fm cPM = null;
    public static final C2491fm cPN = null;
    public static final C2491fm cPO = null;
    public static final C2491fm cPP = null;
    public static final C2491fm cPQ = null;
    public static final C2491fm cPR = null;
    public static final C2491fm cPS = null;
    public static final C2491fm cPT = null;
    public static final C2491fm cPU = null;
    public static final C2491fm cPV = null;
    public static final C2491fm cPW = null;
    public static final C2491fm cPX = null;
    public static final C2491fm cPY = null;
    public static final C2491fm cPZ = null;
    public static final C2491fm cPa = null;
    public static final C2491fm cPb = null;
    public static final C2491fm cPc = null;
    public static final C2491fm cPd = null;
    public static final C2491fm cPe = null;
    public static final C2491fm cPf = null;
    public static final C2491fm cPg = null;
    public static final C2491fm cPh = null;
    public static final C2491fm cPi = null;
    public static final C2491fm cPj = null;
    public static final C2491fm cPk = null;
    public static final C2491fm cPl = null;
    public static final C2491fm cPm = null;
    public static final C2491fm cPn = null;
    public static final C2491fm cPo = null;
    public static final C2491fm cPp = null;
    public static final C2491fm cPq = null;
    public static final C2491fm cPr = null;
    public static final C2491fm cPs = null;
    public static final C2491fm cPt = null;
    public static final C2491fm cPu = null;
    public static final C2491fm cPv = null;
    public static final C2491fm cPw = null;
    public static final C2491fm cPx = null;
    public static final C2491fm cPy = null;
    public static final C2491fm cPz = null;
    public static final C2491fm cQa = null;
    public static final C2491fm cQb = null;
    public static final C2491fm cQc = null;
    public static final C2491fm cQd = null;
    public static final C2491fm cQe = null;
    public static final C2491fm cQf = null;
    public static final C2491fm cQg = null;
    public static final C2491fm cQh = null;
    public static final C2491fm cQi = null;
    public static final C2491fm cQj = null;
    public static final C2491fm cQk = null;
    public static final C2491fm cQl = null;
    public static final C2491fm cQm = null;
    public static final C2491fm cQn = null;
    public static final C2491fm cQo = null;
    public static final C2491fm cQp = null;
    public static final C2491fm cQq = null;
    public static final C2491fm cQr = null;
    public static final C2491fm cQs = null;
    public static final C2491fm cQt = null;
    public static final C5663aRz chW = null;
    public static final C2491fm coL = null;
    public static final C2491fm coN = null;
    /* renamed from: lw */
    public static final C2491fm f412lw = null;
    public static final long serialVersionUID = 0;
    /* renamed from: yH */
    public static final C2491fm f413yH = null;
    private static final Log logger = LogPrinter.setClass(Taikodom.class);
    public static C6494any ___iScriptClass;
    @C0064Am(aul = "77af9d530f16817eaaff51ee29eb9886", aum = 24)

    /* renamed from: OX */
    private static C3438ra<WeaponType> f398OX;
    @C0064Am(aul = "48808b0ce4298a336a69bf717ac4f1a2", aum = 29)
    private static C3438ra<NPCType> aLD;
    @C0064Am(aul = "10a3f06055bc06f565e35233064248aa", aum = 27)
    private static C3438ra<ItemGenSet> aPp;
    @C0064Am(aul = "a108a84de7c23a928c9d83c73a43608c", aum = 34)
    private static C3438ra<AttributeBuffType> aQT;
    @C0064Am(aul = "9f2f4090839475d412dccdf2ba545ea5", aum = 36)
    private static C3438ra<Faction> aQm;
    @C0064Am(aul = "49db357b3491c3b2f57d324f8777a29a", aum = 0)

    /* renamed from: bK */
    private static UUID f406bK;
    @C0064Am(aul = "e1ca85a4136e7c31ccc92de4d9c8d64a", aum = 20)
    private static C3438ra<ShipStructureType> boj;
    @C0064Am(aul = "d739e1c39c595b1cde0f21b1986e10c4", aum = 1)
    private static I18NString cIY;
    @C0064Am(aul = "3ef0d8bd22bf99c59b683834a6b80a4e", aum = 15)
    private static C3438ra<BlueprintType> cJA;
    @C0064Am(aul = "6e6ae2dc34df21f76b0106ad390ba8eb", aum = 16)
    private static C3438ra<SceneryType> cJC;
    @C0064Am(aul = "89f24f1f91af67cbcba7abd8935ea76b", aum = 17)
    private static C3438ra<ShieldType> cJE;
    @C0064Am(aul = "0f58fe747ce815948d418a95b181c12c", aum = 18)
    private static C3438ra<HazardAreaType> cJG;
    @C0064Am(aul = "14b40f53c948437ca676cc4e8a7117d1", aum = 19)
    private static C3438ra<ShipSectorType> cJI;
    @C0064Am(aul = "9f726f508fa0640701cad26f3a54a3df", aum = 21)
    private static C3438ra<ShipType> cJK;
    @C0064Am(aul = "51654e434aeca9c1e8f2f6433383d0aa", aum = 22)
    private static C3438ra<C6544aow> cJM;
    @C0064Am(aul = "c51a8592ec3e25251cd361ecd20ddd63", aum = 23)
    private static C3438ra<StationType> cJO;
    @C0064Am(aul = "3d47036606bd151848f82b06d664d5a2", aum = 25)
    private static C3438ra<RawMaterialType> cJQ;
    @C0064Am(aul = "5180523453849cb135127ad19d27cb56", aum = 26)
    private static C3438ra<ItemTypeCategory> cJS;
    @C0064Am(aul = "55362d1bcb671bd0f68719513c0282cf", aum = 28)
    private static C3438ra<MissionTemplate> cJU;
    @C0064Am(aul = "e4c192227702947200cab4f7af6a1d29", aum = 30)
    private static C3438ra<EquippedShipType> cJW;
    @C0064Am(aul = "74cff4a2d9494c75b9ccaca9aaf341b1", aum = 31)
    private static C3438ra<LootType> cJY;
    @C0064Am(aul = "c3e1df9faf79a29e6eacfdf2bbfbed3d", aum = 2)
    @C5566aOg
    private static C3438ra<User> cJa;
    @C0064Am(aul = "e262cebe0dc7e875a4ea79a3c1f28e87", aum = 3)
    @C5566aOg
    private static C2686iZ<String> cJc;
    @C0064Am(aul = "e1ef1d7d36c2cecf84797b7aea056b86", aum = 4)
    private static C3438ra<CollisionFXSet> cJe;
    @C0064Am(aul = "0ded854935e000dd9a08168691262e98", aum = 5)
    private static C3438ra<StellarSystem> cJg;
    @C0064Am(aul = "0e42d8baef486c82e1d3c3ccef1021aa", aum = 6)
    private static C3438ra<ClipType> cJi;
    @C0064Am(aul = "d0a4baf1e9b6e403c995b7623a41bcae", aum = 7)
    private static C3438ra<ClipBoxType> cJk;
    @C0064Am(aul = "fbc07682f6f68bedf60852c51fa1b0d1", aum = 8)
    private static C3438ra<CargoHoldType> cJm;
    @C0064Am(aul = "8fcf7f889cc8f8b16a80466c352cdbc1", aum = 9)
    private static C3438ra<GateType> cJo;
    @C0064Am(aul = "5c44eeffae3d0021344b9054b9ce02f9", aum = 10)
    private static C3438ra<AdvertiseType> cJq;
    @C0064Am(aul = "aec892e48c3abc8bf5cdf96dcdc0e243", aum = 11)
    private static C3438ra<AsteroidType> cJs;
    @C0064Am(aul = "9ef9ba379474f3b6038b849c2b448e80", aum = 12)
    private static C3438ra<HullType> cJu;
    @C0064Am(aul = "eeae9ed70bc04d039adcf73d14d91bd5", aum = 13)
    private static C3438ra<BagItemType> cJw;
    @C0064Am(aul = "09686e3ba94ca4685f171fb8b85f8dfd", aum = 14)
    private static C3438ra<MerchandiseType> cJy;
    @C0064Am(aul = "a28f92930a2d44a173c526225cbab8d3", aum = 48)
    private static C3438ra<AsteroidZoneCategory> cKA;
    @C0064Am(aul = "20a4fd2705be637288166a055dce7a51", aum = 49)
    private static C3438ra<SpaceCategory> cKC;
    @C0064Am(aul = "b88e86ae0f5115d0b43808c5cace3644", aum = 50)
    private static Progression cKD;
    @C0064Am(aul = "342a7aa7a8a1a5f0e5154253f00f5b34", aum = 51)
    private static ContractBoard cKF;
    @C0064Am(aul = "9dbf638fa17b7568d75ced3a38a7c0b5", aum = 52)
    private static NLSManager cKH;
    @C0064Am(aul = "886d30a753ab5329dd305ed69dbba5e2", aum = 53)
    private static AvatarManager cKJ;
    @C0064Am(aul = "620f87f83db39a669e3d72438169b6c0", aum = 54)
    private static Bank cKL;
    @C0064Am(aul = "b0480a65644dd3f193c9163db4cc94c8", aum = 55)
    private static Bank cKN;
    @C0064Am(aul = "1861ce90dbea8990b5d21f6698b9a27d", aum = 56)
    private static SpaceZoneTypesManager cKP;
    @C0064Am(aul = "b13158b02e82316b66b1504c7fa0f62c", aum = 57)
    private static CitizenshipOffice cKR;
    @C0064Am(aul = "3f0e5812d96db509fdb0e8a654b70fd4", aum = 58)
    private static Insurer cKT;
    @C0064Am(aul = "bd09ebeccb6a3251c782f43106c428bc", aum = 59)
    private static Backdoor cKV;
    @C0064Am(aul = "3513763bd1ebb926802ec674d00342c4", aum = 60)
    private static AssetGroup cKX;
    @C0064Am(aul = "0e8320390b62c71e4b28931f87de514e", aum = 61)
    private static SupportOuterface cKZ;
    @C0064Am(aul = "d4b80533e58f52f4660c4cb93ff6f5df", aum = 32)
    private static C3438ra<AmplifierType> cKa;
    @C0064Am(aul = "b59340dc2df57d292d550899bb82d5c5", aum = 33)
    private static C3438ra<ModuleType> cKc;
    @C0064Am(aul = "ef4a1f3e5a08ed25e21f469005f58d71", aum = 35)
    private static C3438ra<AIControllerType> cKe;
    @C0064Am(aul = "43bc1e9a73af88ba5d1d2b82226598db", aum = 37)
    private static C3438ra<Tutorial> cKg;
    @C0064Am(aul = "cd30acee5d2c2b8a4ec8943c351fb519", aum = 38)
    private static C3438ra<ConsignmentFeeManager> cKi;
    @C0064Am(aul = "dddd86408e7e8541ca440622212b9743", aum = 39)
    private static C3438ra<SectorCategory> cKk;
    @C0064Am(aul = "bc2c4bc191da12c52f3b61e80abb8deb", aum = 40)
    private static C2686iZ<DamageType> cKm;
    @C0064Am(aul = "d63c3bffb361bc982793a74f790b3b0c", aum = 41)
    private static C2686iZ<CruiseSpeedType> cKo;
    @C0064Am(aul = "f5f671ce3f549c1ad63bbecdf219e6f3", aum = 42)
    private static C3438ra<CitizenImprovementType> cKq;
    @C0064Am(aul = "83eed973cd6c30fe56b691f872604039", aum = 43)
    private static C3438ra<CitizenshipType> cKs;
    @C0064Am(aul = "1b69d88d1b32876217ff8c03ad1b5581", aum = 44)
    private static C3438ra<CitizenshipPack> cKu;
    @C0064Am(aul = "44753edf05230f76f825ce1569c5be4f", aum = 45)
    private static C3438ra<CitizenshipReward> cKw;
    @C0064Am(aul = "53a8711a2eca9aea84584ad4e41be6f6", aum = 47)
    private static C3438ra<DatabaseCategory> cKy;
    @C0064Am(aul = "ffa4c6cab09cf0e782b8bfcbb7c31105", aum = 75)
    @C5566aOg
    private static C1077Pl cLA;
    @C0064Am(aul = "85ad077af9999d729561a6702b45fb5c", aum = 76)
    @C5566aOg
    private static C0694Ju cLC;
    @C0064Am(aul = "5010435f23a887886a397977f6a12d83", aum = 77)
    private static Market cLE;
    @C0064Am(aul = "3875f819a2cf015dba3d7a911b4eb4e1", aum = 78)
    private static GlobalEconomyInfo cLG;
    @C0064Am(aul = "793e01f95ef8536602fd252f70c58f98", aum = 79)
    private static GlobalPhysicsTweaks cLI;
    @C0064Am(aul = "746dd8b67eef6d066f76678094cc5eea", aum = 80)
    private static C3438ra<VirtualWarehouse> cLK;
    @C0064Am(aul = "9942eafa3e6cacb784012f7fbfd556d0", aum = 81)
    private static PlayerReservedNames cLM;
    @C0064Am(aul = "229852970fffee012472f9ac45b3a666", aum = 82)
    @C1020Ot(drz = C1020Ot.C1021a.NOT_PERSISTED)
    private static C2686iZ<ItemType> cLO;
    @C0064Am(aul = "b32aeead5c67ecc4d54cd377c3f16af0", aum = 83)
    private static C2686iZ<C0468GU> cLQ;
    @C0064Am(aul = "15cf2e87452024c3e30f40b5eb485ef3", aum = 84)
    @C5566aOg
    private static C1556Wo<String, User> cLS;
    @C0064Am(aul = "41b75b60af5c4dbe52e03ff02c7a5d13", aum = 85)
    @C5566aOg
    private static UsersQueueManager cLU;
    @C0064Am(aul = "cd5906400bdc5032b6f3a93db986b423", aum = 86)
    private static PingScript cLW;
    @C0064Am(aul = "a2fcb21a72ffbbf23789ac1b1f13155e", aum = 87)
    private static DebugFlags cLY;
    @C0064Am(aul = "1746882bafddaeb37b150fc52198e65f", aum = 62)
    private static StatsManager cLb;
    @C0064Am(aul = "1fa89d48b676d419a6c1696ecdef0965", aum = 63)
    private static MarkManager cLd;
    @C0064Am(aul = "adf5a21ac520af8b362e3e1c157764e6", aum = 64)
    private static AdvertiseViewTable cLf;
    @C0064Am(aul = "ac86c32c8d2c6b98e44ef3130c8a6146", aum = 65)
    private static NPCType cLh;
    @C0064Am(aul = "69f3fadf055251b81f1f002984fea596", aum = 66)
    private static ServerEditorUtility cLj;
    @C0064Am(aul = "9a1ca95c232cb49dff50010058711182", aum = 67)
    private static TaikodomDefaultContents cLl;
    @C0064Am(aul = "4619ac4910b1790f48dd025c5c8c1889", aum = 68)
    private static TemporaryFeatureBlock cLm;
    @C0064Am(aul = "8013e63b1d233493b79a1e48a1839b96", aum = 69)
    private static TemporaryFeatureTest cLo;
    @C0064Am(aul = "6d5dad9dec1adaf2c3d3444a5b78dad2", aum = 70)
    private static ItemBilling cLq;
    @C0064Am(aul = "adb9c25d672d3a4151b13847163ae68d", aum = 71)
    private static ContentImporterExporter cLs;
    @C0064Am(aul = "c55786d1ab0e0f4600f15b684b70f284", aum = 72)
    @C5566aOg
    private static C3438ra<Corporation> cLu;
    @C0064Am(aul = "a38c5434b704a05116c57319b9150dcf", aum = 73)
    private static CorporationReservedNames cLw;
    @C0064Am(aul = "78a94494558ec8ebf57bf7dc2bf98d9d", aum = 74)
    private static PlayerFinder cLy;
    @C0064Am(aul = "61402922b0fce6cd61f62bf03d8a786d", aum = 88)
    private static ClientUtils cMa;
    @C0064Am(aul = "fce98b95151dccbc3c6ce5e149c551ff", aum = 89)
    private static OffloadUtils cMc;
    @C0064Am(aul = "35acd4443c8095f0993c08cadd87c467", aum = 90)
    private static BotUtils cMe;
    @C0064Am(aul = "ce3dfa9c23a7b8b97064b2464a4c69c7", aum = 46)

    /* renamed from: jT */
    private static C3438ra<TaikopediaEntry> f411jT;

    static {
        m2019V();
    }

    private transient List<BotScript> cMg;

    public Taikodom() {
        super((C5540aNg) null);
        super.mo10S();
    }

    public Taikodom(C5540aNg ang) {
        super(ang);
    }

    /* renamed from: V */
    static void m2019V() {
        serialVersionUID = (long) 1;
        _m_fieldCount = TaikodomObject._m_fieldCount + 91;
        _m_methodCount = TaikodomObject._m_methodCount + 244;
        int i = TaikodomObject._m_fieldCount;
        C5663aRz[] arzArr = new C5663aRz[(i + 91)];
        C5663aRz b = C5640aRc.m17844b(Taikodom.class, "49db357b3491c3b2f57d324f8777a29a", i);
        f407bL = b;
        arzArr[i] = b;
        int i2 = i + 1;
        C5663aRz b2 = C5640aRc.m17844b(Taikodom.class, "d739e1c39c595b1cde0f21b1986e10c4", i2);
        cIZ = b2;
        arzArr[i2] = b2;
        int i3 = i2 + 1;
        C5663aRz b3 = C5640aRc.m17844b(Taikodom.class, "c3e1df9faf79a29e6eacfdf2bbfbed3d", i3);
        cJb = b3;
        arzArr[i3] = b3;
        int i4 = i3 + 1;
        C5663aRz b4 = C5640aRc.m17844b(Taikodom.class, "e262cebe0dc7e875a4ea79a3c1f28e87", i4);
        cJd = b4;
        arzArr[i4] = b4;
        int i5 = i4 + 1;
        C5663aRz b5 = C5640aRc.m17844b(Taikodom.class, "e1ef1d7d36c2cecf84797b7aea056b86", i5);
        cJf = b5;
        arzArr[i5] = b5;
        int i6 = i5 + 1;
        C5663aRz b6 = C5640aRc.m17844b(Taikodom.class, "0ded854935e000dd9a08168691262e98", i6);
        cJh = b6;
        arzArr[i6] = b6;
        int i7 = i6 + 1;
        C5663aRz b7 = C5640aRc.m17844b(Taikodom.class, "0e42d8baef486c82e1d3c3ccef1021aa", i7);
        cJj = b7;
        arzArr[i7] = b7;
        int i8 = i7 + 1;
        C5663aRz b8 = C5640aRc.m17844b(Taikodom.class, "d0a4baf1e9b6e403c995b7623a41bcae", i8);
        cJl = b8;
        arzArr[i8] = b8;
        int i9 = i8 + 1;
        C5663aRz b9 = C5640aRc.m17844b(Taikodom.class, "fbc07682f6f68bedf60852c51fa1b0d1", i9);
        cJn = b9;
        arzArr[i9] = b9;
        int i10 = i9 + 1;
        C5663aRz b10 = C5640aRc.m17844b(Taikodom.class, "8fcf7f889cc8f8b16a80466c352cdbc1", i10);
        cJp = b10;
        arzArr[i10] = b10;
        int i11 = i10 + 1;
        C5663aRz b11 = C5640aRc.m17844b(Taikodom.class, "5c44eeffae3d0021344b9054b9ce02f9", i11);
        cJr = b11;
        arzArr[i11] = b11;
        int i12 = i11 + 1;
        C5663aRz b12 = C5640aRc.m17844b(Taikodom.class, "aec892e48c3abc8bf5cdf96dcdc0e243", i12);
        cJt = b12;
        arzArr[i12] = b12;
        int i13 = i12 + 1;
        C5663aRz b13 = C5640aRc.m17844b(Taikodom.class, "9ef9ba379474f3b6038b849c2b448e80", i13);
        cJv = b13;
        arzArr[i13] = b13;
        int i14 = i13 + 1;
        C5663aRz b14 = C5640aRc.m17844b(Taikodom.class, "eeae9ed70bc04d039adcf73d14d91bd5", i14);
        cJx = b14;
        arzArr[i14] = b14;
        int i15 = i14 + 1;
        C5663aRz b15 = C5640aRc.m17844b(Taikodom.class, "09686e3ba94ca4685f171fb8b85f8dfd", i15);
        cJz = b15;
        arzArr[i15] = b15;
        int i16 = i15 + 1;
        C5663aRz b16 = C5640aRc.m17844b(Taikodom.class, "3ef0d8bd22bf99c59b683834a6b80a4e", i16);
        cJB = b16;
        arzArr[i16] = b16;
        int i17 = i16 + 1;
        C5663aRz b17 = C5640aRc.m17844b(Taikodom.class, "6e6ae2dc34df21f76b0106ad390ba8eb", i17);
        cJD = b17;
        arzArr[i17] = b17;
        int i18 = i17 + 1;
        C5663aRz b18 = C5640aRc.m17844b(Taikodom.class, "89f24f1f91af67cbcba7abd8935ea76b", i18);
        cJF = b18;
        arzArr[i18] = b18;
        int i19 = i18 + 1;
        C5663aRz b19 = C5640aRc.m17844b(Taikodom.class, "0f58fe747ce815948d418a95b181c12c", i19);
        cJH = b19;
        arzArr[i19] = b19;
        int i20 = i19 + 1;
        C5663aRz b20 = C5640aRc.m17844b(Taikodom.class, "14b40f53c948437ca676cc4e8a7117d1", i20);
        cJJ = b20;
        arzArr[i20] = b20;
        int i21 = i20 + 1;
        C5663aRz b21 = C5640aRc.m17844b(Taikodom.class, "e1ca85a4136e7c31ccc92de4d9c8d64a", i21);
        bok = b21;
        arzArr[i21] = b21;
        int i22 = i21 + 1;
        C5663aRz b22 = C5640aRc.m17844b(Taikodom.class, "9f726f508fa0640701cad26f3a54a3df", i22);
        cJL = b22;
        arzArr[i22] = b22;
        int i23 = i22 + 1;
        C5663aRz b23 = C5640aRc.m17844b(Taikodom.class, "51654e434aeca9c1e8f2f6433383d0aa", i23);
        cJN = b23;
        arzArr[i23] = b23;
        int i24 = i23 + 1;
        C5663aRz b24 = C5640aRc.m17844b(Taikodom.class, "c51a8592ec3e25251cd361ecd20ddd63", i24);
        cJP = b24;
        arzArr[i24] = b24;
        int i25 = i24 + 1;
        C5663aRz b25 = C5640aRc.m17844b(Taikodom.class, "77af9d530f16817eaaff51ee29eb9886", i25);
        f399OY = b25;
        arzArr[i25] = b25;
        int i26 = i25 + 1;
        C5663aRz b26 = C5640aRc.m17844b(Taikodom.class, "3d47036606bd151848f82b06d664d5a2", i26);
        cJR = b26;
        arzArr[i26] = b26;
        int i27 = i26 + 1;
        C5663aRz b27 = C5640aRc.m17844b(Taikodom.class, "5180523453849cb135127ad19d27cb56", i27);
        cJT = b27;
        arzArr[i27] = b27;
        int i28 = i27 + 1;
        C5663aRz b28 = C5640aRc.m17844b(Taikodom.class, "10a3f06055bc06f565e35233064248aa", i28);
        aPq = b28;
        arzArr[i28] = b28;
        int i29 = i28 + 1;
        C5663aRz b29 = C5640aRc.m17844b(Taikodom.class, "55362d1bcb671bd0f68719513c0282cf", i29);
        cJV = b29;
        arzArr[i29] = b29;
        int i30 = i29 + 1;
        C5663aRz b30 = C5640aRc.m17844b(Taikodom.class, "48808b0ce4298a336a69bf717ac4f1a2", i30);
        bCb = b30;
        arzArr[i30] = b30;
        int i31 = i30 + 1;
        C5663aRz b31 = C5640aRc.m17844b(Taikodom.class, "e4c192227702947200cab4f7af6a1d29", i31);
        cJX = b31;
        arzArr[i31] = b31;
        int i32 = i31 + 1;
        C5663aRz b32 = C5640aRc.m17844b(Taikodom.class, "74cff4a2d9494c75b9ccaca9aaf341b1", i32);
        cJZ = b32;
        arzArr[i32] = b32;
        int i33 = i32 + 1;
        C5663aRz b33 = C5640aRc.m17844b(Taikodom.class, "d4b80533e58f52f4660c4cb93ff6f5df", i33);
        cKb = b33;
        arzArr[i33] = b33;
        int i34 = i33 + 1;
        C5663aRz b34 = C5640aRc.m17844b(Taikodom.class, "b59340dc2df57d292d550899bb82d5c5", i34);
        cKd = b34;
        arzArr[i34] = b34;
        int i35 = i34 + 1;
        C5663aRz b35 = C5640aRc.m17844b(Taikodom.class, "a108a84de7c23a928c9d83c73a43608c", i35);
        aQU = b35;
        arzArr[i35] = b35;
        int i36 = i35 + 1;
        C5663aRz b36 = C5640aRc.m17844b(Taikodom.class, "ef4a1f3e5a08ed25e21f469005f58d71", i36);
        cKf = b36;
        arzArr[i36] = b36;
        int i37 = i36 + 1;
        C5663aRz b37 = C5640aRc.m17844b(Taikodom.class, "9f2f4090839475d412dccdf2ba545ea5", i37);
        aQn = b37;
        arzArr[i37] = b37;
        int i38 = i37 + 1;
        C5663aRz b38 = C5640aRc.m17844b(Taikodom.class, "43bc1e9a73af88ba5d1d2b82226598db", i38);
        cKh = b38;
        arzArr[i38] = b38;
        int i39 = i38 + 1;
        C5663aRz b39 = C5640aRc.m17844b(Taikodom.class, "cd30acee5d2c2b8a4ec8943c351fb519", i39);
        cKj = b39;
        arzArr[i39] = b39;
        int i40 = i39 + 1;
        C5663aRz b40 = C5640aRc.m17844b(Taikodom.class, "dddd86408e7e8541ca440622212b9743", i40);
        cKl = b40;
        arzArr[i40] = b40;
        int i41 = i40 + 1;
        C5663aRz b41 = C5640aRc.m17844b(Taikodom.class, "bc2c4bc191da12c52f3b61e80abb8deb", i41);
        cKn = b41;
        arzArr[i41] = b41;
        int i42 = i41 + 1;
        C5663aRz b42 = C5640aRc.m17844b(Taikodom.class, "d63c3bffb361bc982793a74f790b3b0c", i42);
        cKp = b42;
        arzArr[i42] = b42;
        int i43 = i42 + 1;
        C5663aRz b43 = C5640aRc.m17844b(Taikodom.class, "f5f671ce3f549c1ad63bbecdf219e6f3", i43);
        cKr = b43;
        arzArr[i43] = b43;
        int i44 = i43 + 1;
        C5663aRz b44 = C5640aRc.m17844b(Taikodom.class, "83eed973cd6c30fe56b691f872604039", i44);
        cKt = b44;
        arzArr[i44] = b44;
        int i45 = i44 + 1;
        C5663aRz b45 = C5640aRc.m17844b(Taikodom.class, "1b69d88d1b32876217ff8c03ad1b5581", i45);
        cKv = b45;
        arzArr[i45] = b45;
        int i46 = i45 + 1;
        C5663aRz b46 = C5640aRc.m17844b(Taikodom.class, "44753edf05230f76f825ce1569c5be4f", i46);
        cKx = b46;
        arzArr[i46] = b46;
        int i47 = i46 + 1;
        C5663aRz b47 = C5640aRc.m17844b(Taikodom.class, "ce3dfa9c23a7b8b97064b2464a4c69c7", i47);
        awd = b47;
        arzArr[i47] = b47;
        int i48 = i47 + 1;
        C5663aRz b48 = C5640aRc.m17844b(Taikodom.class, "53a8711a2eca9aea84584ad4e41be6f6", i48);
        cKz = b48;
        arzArr[i48] = b48;
        int i49 = i48 + 1;
        C5663aRz b49 = C5640aRc.m17844b(Taikodom.class, "a28f92930a2d44a173c526225cbab8d3", i49);
        cKB = b49;
        arzArr[i49] = b49;
        int i50 = i49 + 1;
        C5663aRz b50 = C5640aRc.m17844b(Taikodom.class, "20a4fd2705be637288166a055dce7a51", i50);
        f397Ml = b50;
        arzArr[i50] = b50;
        int i51 = i50 + 1;
        C5663aRz b51 = C5640aRc.m17844b(Taikodom.class, "b88e86ae0f5115d0b43808c5cace3644", i51);
        cKE = b51;
        arzArr[i51] = b51;
        int i52 = i51 + 1;
        C5663aRz b52 = C5640aRc.m17844b(Taikodom.class, "342a7aa7a8a1a5f0e5154253f00f5b34", i52);
        cKG = b52;
        arzArr[i52] = b52;
        int i53 = i52 + 1;
        C5663aRz b53 = C5640aRc.m17844b(Taikodom.class, "9dbf638fa17b7568d75ced3a38a7c0b5", i53);
        cKI = b53;
        arzArr[i53] = b53;
        int i54 = i53 + 1;
        C5663aRz b54 = C5640aRc.m17844b(Taikodom.class, "886d30a753ab5329dd305ed69dbba5e2", i54);
        cKK = b54;
        arzArr[i54] = b54;
        int i55 = i54 + 1;
        C5663aRz b55 = C5640aRc.m17844b(Taikodom.class, "620f87f83db39a669e3d72438169b6c0", i55);
        cKM = b55;
        arzArr[i55] = b55;
        int i56 = i55 + 1;
        C5663aRz b56 = C5640aRc.m17844b(Taikodom.class, "b0480a65644dd3f193c9163db4cc94c8", i56);
        cKO = b56;
        arzArr[i56] = b56;
        int i57 = i56 + 1;
        C5663aRz b57 = C5640aRc.m17844b(Taikodom.class, "1861ce90dbea8990b5d21f6698b9a27d", i57);
        cKQ = b57;
        arzArr[i57] = b57;
        int i58 = i57 + 1;
        C5663aRz b58 = C5640aRc.m17844b(Taikodom.class, "b13158b02e82316b66b1504c7fa0f62c", i58);
        cKS = b58;
        arzArr[i58] = b58;
        int i59 = i58 + 1;
        C5663aRz b59 = C5640aRc.m17844b(Taikodom.class, "3f0e5812d96db509fdb0e8a654b70fd4", i59);
        cKU = b59;
        arzArr[i59] = b59;
        int i60 = i59 + 1;
        C5663aRz b60 = C5640aRc.m17844b(Taikodom.class, "bd09ebeccb6a3251c782f43106c428bc", i60);
        cKW = b60;
        arzArr[i60] = b60;
        int i61 = i60 + 1;
        C5663aRz b61 = C5640aRc.m17844b(Taikodom.class, "3513763bd1ebb926802ec674d00342c4", i61);
        cKY = b61;
        arzArr[i61] = b61;
        int i62 = i61 + 1;
        C5663aRz b62 = C5640aRc.m17844b(Taikodom.class, "0e8320390b62c71e4b28931f87de514e", i62);
        cLa = b62;
        arzArr[i62] = b62;
        int i63 = i62 + 1;
        C5663aRz b63 = C5640aRc.m17844b(Taikodom.class, "1746882bafddaeb37b150fc52198e65f", i63);
        cLc = b63;
        arzArr[i63] = b63;
        int i64 = i63 + 1;
        C5663aRz b64 = C5640aRc.m17844b(Taikodom.class, "1fa89d48b676d419a6c1696ecdef0965", i64);
        cLe = b64;
        arzArr[i64] = b64;
        int i65 = i64 + 1;
        C5663aRz b65 = C5640aRc.m17844b(Taikodom.class, "adf5a21ac520af8b362e3e1c157764e6", i65);
        cLg = b65;
        arzArr[i65] = b65;
        int i66 = i65 + 1;
        C5663aRz b66 = C5640aRc.m17844b(Taikodom.class, "ac86c32c8d2c6b98e44ef3130c8a6146", i66);
        cLi = b66;
        arzArr[i66] = b66;
        int i67 = i66 + 1;
        C5663aRz b67 = C5640aRc.m17844b(Taikodom.class, "69f3fadf055251b81f1f002984fea596", i67);
        cLk = b67;
        arzArr[i67] = b67;
        int i68 = i67 + 1;
        C5663aRz b68 = C5640aRc.m17844b(Taikodom.class, "9a1ca95c232cb49dff50010058711182", i68);
        chW = b68;
        arzArr[i68] = b68;
        int i69 = i68 + 1;
        C5663aRz b69 = C5640aRc.m17844b(Taikodom.class, "4619ac4910b1790f48dd025c5c8c1889", i69);
        cLn = b69;
        arzArr[i69] = b69;
        int i70 = i69 + 1;
        C5663aRz b70 = C5640aRc.m17844b(Taikodom.class, "8013e63b1d233493b79a1e48a1839b96", i70);
        cLp = b70;
        arzArr[i70] = b70;
        int i71 = i70 + 1;
        C5663aRz b71 = C5640aRc.m17844b(Taikodom.class, "6d5dad9dec1adaf2c3d3444a5b78dad2", i71);
        cLr = b71;
        arzArr[i71] = b71;
        int i72 = i71 + 1;
        C5663aRz b72 = C5640aRc.m17844b(Taikodom.class, "adb9c25d672d3a4151b13847163ae68d", i72);
        cLt = b72;
        arzArr[i72] = b72;
        int i73 = i72 + 1;
        C5663aRz b73 = C5640aRc.m17844b(Taikodom.class, "c55786d1ab0e0f4600f15b684b70f284", i73);
        cLv = b73;
        arzArr[i73] = b73;
        int i74 = i73 + 1;
        C5663aRz b74 = C5640aRc.m17844b(Taikodom.class, "a38c5434b704a05116c57319b9150dcf", i74);
        cLx = b74;
        arzArr[i74] = b74;
        int i75 = i74 + 1;
        C5663aRz b75 = C5640aRc.m17844b(Taikodom.class, "78a94494558ec8ebf57bf7dc2bf98d9d", i75);
        cLz = b75;
        arzArr[i75] = b75;
        int i76 = i75 + 1;
        C5663aRz b76 = C5640aRc.m17844b(Taikodom.class, "ffa4c6cab09cf0e782b8bfcbb7c31105", i76);
        cLB = b76;
        arzArr[i76] = b76;
        int i77 = i76 + 1;
        C5663aRz b77 = C5640aRc.m17844b(Taikodom.class, "85ad077af9999d729561a6702b45fb5c", i77);
        cLD = b77;
        arzArr[i77] = b77;
        int i78 = i77 + 1;
        C5663aRz b78 = C5640aRc.m17844b(Taikodom.class, "5010435f23a887886a397977f6a12d83", i78);
        cLF = b78;
        arzArr[i78] = b78;
        int i79 = i78 + 1;
        C5663aRz b79 = C5640aRc.m17844b(Taikodom.class, "3875f819a2cf015dba3d7a911b4eb4e1", i79);
        cLH = b79;
        arzArr[i79] = b79;
        int i80 = i79 + 1;
        C5663aRz b80 = C5640aRc.m17844b(Taikodom.class, "793e01f95ef8536602fd252f70c58f98", i80);
        cLJ = b80;
        arzArr[i80] = b80;
        int i81 = i80 + 1;
        C5663aRz b81 = C5640aRc.m17844b(Taikodom.class, "746dd8b67eef6d066f76678094cc5eea", i81);
        cLL = b81;
        arzArr[i81] = b81;
        int i82 = i81 + 1;
        C5663aRz b82 = C5640aRc.m17844b(Taikodom.class, "9942eafa3e6cacb784012f7fbfd556d0", i82);
        cLN = b82;
        arzArr[i82] = b82;
        int i83 = i82 + 1;
        C5663aRz b83 = C5640aRc.m17844b(Taikodom.class, "229852970fffee012472f9ac45b3a666", i83);
        cLP = b83;
        arzArr[i83] = b83;
        int i84 = i83 + 1;
        C5663aRz b84 = C5640aRc.m17844b(Taikodom.class, "b32aeead5c67ecc4d54cd377c3f16af0", i84);
        cLR = b84;
        arzArr[i84] = b84;
        int i85 = i84 + 1;
        C5663aRz b85 = C5640aRc.m17844b(Taikodom.class, "15cf2e87452024c3e30f40b5eb485ef3", i85);
        cLT = b85;
        arzArr[i85] = b85;
        int i86 = i85 + 1;
        C5663aRz b86 = C5640aRc.m17844b(Taikodom.class, "41b75b60af5c4dbe52e03ff02c7a5d13", i86);
        cLV = b86;
        arzArr[i86] = b86;
        int i87 = i86 + 1;
        C5663aRz b87 = C5640aRc.m17844b(Taikodom.class, "cd5906400bdc5032b6f3a93db986b423", i87);
        cLX = b87;
        arzArr[i87] = b87;
        int i88 = i87 + 1;
        C5663aRz b88 = C5640aRc.m17844b(Taikodom.class, "a2fcb21a72ffbbf23789ac1b1f13155e", i88);
        cLZ = b88;
        arzArr[i88] = b88;
        int i89 = i88 + 1;
        C5663aRz b89 = C5640aRc.m17844b(Taikodom.class, "61402922b0fce6cd61f62bf03d8a786d", i89);
        cMb = b89;
        arzArr[i89] = b89;
        int i90 = i89 + 1;
        C5663aRz b90 = C5640aRc.m17844b(Taikodom.class, "fce98b95151dccbc3c6ce5e149c551ff", i90);
        cMd = b90;
        arzArr[i90] = b90;
        int i91 = i90 + 1;
        C5663aRz b91 = C5640aRc.m17844b(Taikodom.class, "35acd4443c8095f0993c08cadd87c467", i91);
        cMf = b91;
        arzArr[i91] = b91;
        int i92 = i91 + 1;
        _m_fields = arzArr;
        C1634Xv.m11725a((Object[]) TaikodomObject._m_fields, (Object[]) _m_fields);
        int i93 = TaikodomObject._m_methodCount;
        C2491fm[] fmVarArr = new C2491fm[(i93 + 244)];
        C2491fm a = C4105zY.m41624a(Taikodom.class, "d69e8e00bd325bdefc77f842f302ed10", i93);
        f408bN = a;
        fmVarArr[i93] = a;
        int i94 = i93 + 1;
        C2491fm a2 = C4105zY.m41624a(Taikodom.class, "33ed544663f1cc943366896f57ef921e", i94);
        f409bO = a2;
        fmVarArr[i94] = a2;
        int i95 = i94 + 1;
        C2491fm a3 = C4105zY.m41624a(Taikodom.class, "20d16d2cadf732febc4c3eb792de43d6", i95);
        _f_toString_0020_0028_0029Ljava_002flang_002fString_003b = a3;
        fmVarArr[i95] = a3;
        int i96 = i95 + 1;
        C2491fm a4 = C4105zY.m41624a(Taikodom.class, "51f4cd88bfebbb9c2e94a53e324a7bdb", i96);
        cMh = a4;
        fmVarArr[i96] = a4;
        int i97 = i96 + 1;
        C2491fm a5 = C4105zY.m41624a(Taikodom.class, "7a3551613399e9c56ffb569b7e46ab34", i97);
        cMi = a5;
        fmVarArr[i97] = a5;
        int i98 = i97 + 1;
        C2491fm a6 = C4105zY.m41624a(Taikodom.class, "272614b608fe5f371334ec23fced59ef", i98);
        cMj = a6;
        fmVarArr[i98] = a6;
        int i99 = i98 + 1;
        C2491fm a7 = C4105zY.m41624a(Taikodom.class, "4865936b03ce4547115e188763de7a86", i99);
        cMk = a7;
        fmVarArr[i99] = a7;
        int i100 = i99 + 1;
        C2491fm a8 = C4105zY.m41624a(Taikodom.class, "db15a82ff07585552581859ad8e0cc91", i100);
        cMl = a8;
        fmVarArr[i100] = a8;
        int i101 = i100 + 1;
        C2491fm a9 = C4105zY.m41624a(Taikodom.class, "bb762f518540bf867fc1d762e3900bf1", i101);
        cMm = a9;
        fmVarArr[i101] = a9;
        int i102 = i101 + 1;
        C2491fm a10 = C4105zY.m41624a(Taikodom.class, "b9e31b67cd7ba5687a526c8556902b55", i102);
        cMn = a10;
        fmVarArr[i102] = a10;
        int i103 = i102 + 1;
        C2491fm a11 = C4105zY.m41624a(Taikodom.class, "539a0c91d38375c75cf41f1a061054f4", i103);
        cMo = a11;
        fmVarArr[i103] = a11;
        int i104 = i103 + 1;
        C2491fm a12 = C4105zY.m41624a(Taikodom.class, "5310aa71d6d5b9b3ae7d624da45f2691", i104);
        cMp = a12;
        fmVarArr[i104] = a12;
        int i105 = i104 + 1;
        C2491fm a13 = C4105zY.m41624a(Taikodom.class, "92f8aa40eefc9aebe32ddf9a66ddbb4d", i105);
        cMq = a13;
        fmVarArr[i105] = a13;
        int i106 = i105 + 1;
        C2491fm a14 = C4105zY.m41624a(Taikodom.class, "c80c7adb5a4e72d5db53a222def3e8be", i106);
        cMr = a14;
        fmVarArr[i106] = a14;
        int i107 = i106 + 1;
        C2491fm a15 = C4105zY.m41624a(Taikodom.class, "5a430f3877a5835c56ea67c6dbfe5745", i107);
        cMs = a15;
        fmVarArr[i107] = a15;
        int i108 = i107 + 1;
        C2491fm a16 = C4105zY.m41624a(Taikodom.class, "638bdc97afeef14f8f3ff55e5c69f08d", i108);
        cMt = a16;
        fmVarArr[i108] = a16;
        int i109 = i108 + 1;
        C2491fm a17 = C4105zY.m41624a(Taikodom.class, "80c4d0d3d99cda074f19c90e4c89f2ac", i109);
        cMu = a17;
        fmVarArr[i109] = a17;
        int i110 = i109 + 1;
        C2491fm a18 = C4105zY.m41624a(Taikodom.class, "efb758e1cdf9ebcef446f94661b370dc", i110);
        cMv = a18;
        fmVarArr[i110] = a18;
        int i111 = i110 + 1;
        C2491fm a19 = C4105zY.m41624a(Taikodom.class, "ce488c030cc59c731d1126a577870096", i111);
        cMw = a19;
        fmVarArr[i111] = a19;
        int i112 = i111 + 1;
        C2491fm a20 = C4105zY.m41624a(Taikodom.class, "eb81a71d6b77c193f73dd5216337017b", i112);
        cMx = a20;
        fmVarArr[i112] = a20;
        int i113 = i112 + 1;
        C2491fm a21 = C4105zY.m41624a(Taikodom.class, "65158b9255f711b9643e7a7736f35788", i113);
        cMy = a21;
        fmVarArr[i113] = a21;
        int i114 = i113 + 1;
        C2491fm a22 = C4105zY.m41624a(Taikodom.class, "8e40cd8c4af0b4876ec1bfe00620646b", i114);
        cMz = a22;
        fmVarArr[i114] = a22;
        int i115 = i114 + 1;
        C2491fm a23 = C4105zY.m41624a(Taikodom.class, "54ab51150a3024113395a8fe56803397", i115);
        cMA = a23;
        fmVarArr[i115] = a23;
        int i116 = i115 + 1;
        C2491fm a24 = C4105zY.m41624a(Taikodom.class, "6adbacfa041d14d589b1245fdd7434db", i116);
        cMB = a24;
        fmVarArr[i116] = a24;
        int i117 = i116 + 1;
        C2491fm a25 = C4105zY.m41624a(Taikodom.class, "7f0be395f9deb7ae045676c8e34da557", i117);
        f413yH = a25;
        fmVarArr[i117] = a25;
        int i118 = i117 + 1;
        C2491fm a26 = C4105zY.m41624a(Taikodom.class, "dabf2df70f027861d9528e2383f53c6d", i118);
        cMC = a26;
        fmVarArr[i118] = a26;
        int i119 = i118 + 1;
        C2491fm a27 = C4105zY.m41624a(Taikodom.class, "e6125ae123b3c62cc9028d27fcf3b0bc", i119);
        cMD = a27;
        fmVarArr[i119] = a27;
        int i120 = i119 + 1;
        C2491fm a28 = C4105zY.m41624a(Taikodom.class, "e079555fc63b2996b16dbe579902ecb5", i120);
        cME = a28;
        fmVarArr[i120] = a28;
        int i121 = i120 + 1;
        C2491fm a29 = C4105zY.m41624a(Taikodom.class, "211afaf2542d4cfa5c8317e635adc11a", i121);
        cMF = a29;
        fmVarArr[i121] = a29;
        int i122 = i121 + 1;
        C2491fm a30 = C4105zY.m41624a(Taikodom.class, "b70d7846734bbaec72f4edb18527a8e0", i122);
        cMG = a30;
        fmVarArr[i122] = a30;
        int i123 = i122 + 1;
        C2491fm a31 = C4105zY.m41624a(Taikodom.class, "23b460750c60787cbe8e4066cb778ca2", i123);
        cMH = a31;
        fmVarArr[i123] = a31;
        int i124 = i123 + 1;
        C2491fm a32 = C4105zY.m41624a(Taikodom.class, "f6b6ab843d3c03e4bf502708f1310dbf", i124);
        cMI = a32;
        fmVarArr[i124] = a32;
        int i125 = i124 + 1;
        C2491fm a33 = C4105zY.m41624a(Taikodom.class, "76a975961c3dfa871fbf85d0927dceed", i125);
        cMJ = a33;
        fmVarArr[i125] = a33;
        int i126 = i125 + 1;
        C2491fm a34 = C4105zY.m41624a(Taikodom.class, "d33f962893c32e43f5948f9b4e4f1d35", i126);
        cMK = a34;
        fmVarArr[i126] = a34;
        int i127 = i126 + 1;
        C2491fm a35 = C4105zY.m41624a(Taikodom.class, "6131b837d395d227a8cee234d417112e", i127);
        cML = a35;
        fmVarArr[i127] = a35;
        int i128 = i127 + 1;
        C2491fm a36 = C4105zY.m41624a(Taikodom.class, "d436c4eb2a7e3f6759f5dc82cf4274d9", i128);
        cMM = a36;
        fmVarArr[i128] = a36;
        int i129 = i128 + 1;
        C2491fm a37 = C4105zY.m41624a(Taikodom.class, "244eb411ac9ee28e95381ffc5418468f", i129);
        cMN = a37;
        fmVarArr[i129] = a37;
        int i130 = i129 + 1;
        C2491fm a38 = C4105zY.m41624a(Taikodom.class, "f528446b8f419c6811e2735faf02ea65", i130);
        cMO = a38;
        fmVarArr[i130] = a38;
        int i131 = i130 + 1;
        C2491fm a39 = C4105zY.m41624a(Taikodom.class, "34314ead705bf4abde12b122855944ee", i131);
        cMP = a39;
        fmVarArr[i131] = a39;
        int i132 = i131 + 1;
        C2491fm a40 = C4105zY.m41624a(Taikodom.class, "361d30cfe39989d6a58c19c8d9105ea3", i132);
        cMQ = a40;
        fmVarArr[i132] = a40;
        int i133 = i132 + 1;
        C2491fm a41 = C4105zY.m41624a(Taikodom.class, "8655e09ed64108d7a8e3e3046da8d320", i133);
        cMR = a41;
        fmVarArr[i133] = a41;
        int i134 = i133 + 1;
        C2491fm a42 = C4105zY.m41624a(Taikodom.class, "f176258c704d0588381af8f252f53fa6", i134);
        cMS = a42;
        fmVarArr[i134] = a42;
        int i135 = i134 + 1;
        C2491fm a43 = C4105zY.m41624a(Taikodom.class, "05b3a51760eee99ae4cee39d11cfd2c8", i135);
        cMT = a43;
        fmVarArr[i135] = a43;
        int i136 = i135 + 1;
        C2491fm a44 = C4105zY.m41624a(Taikodom.class, "efe08c36da2557547e0e420f3f3f849b", i136);
        cMU = a44;
        fmVarArr[i136] = a44;
        int i137 = i136 + 1;
        C2491fm a45 = C4105zY.m41624a(Taikodom.class, "609d7cb70853725bb9d437e0f6cbdfc8", i137);
        cMV = a45;
        fmVarArr[i137] = a45;
        int i138 = i137 + 1;
        C2491fm a46 = C4105zY.m41624a(Taikodom.class, "997423d5bbc21ccde07edf072e5f0b57", i138);
        cMW = a46;
        fmVarArr[i138] = a46;
        int i139 = i138 + 1;
        C2491fm a47 = C4105zY.m41624a(Taikodom.class, "c6cc7be15ff7e07bc9ad581e963463a4", i139);
        cMX = a47;
        fmVarArr[i139] = a47;
        int i140 = i139 + 1;
        C2491fm a48 = C4105zY.m41624a(Taikodom.class, "67d68fe993b23c2a16e3b502f1032727", i140);
        cMY = a48;
        fmVarArr[i140] = a48;
        int i141 = i140 + 1;
        C2491fm a49 = C4105zY.m41624a(Taikodom.class, "6c5b6245d3c39a3c4424de8a50083b15", i141);
        cMZ = a49;
        fmVarArr[i141] = a49;
        int i142 = i141 + 1;
        C2491fm a50 = C4105zY.m41624a(Taikodom.class, "4c3697e893db0805dbbd0df061694f94", i142);
        cNa = a50;
        fmVarArr[i142] = a50;
        int i143 = i142 + 1;
        C2491fm a51 = C4105zY.m41624a(Taikodom.class, "c53a82bac15bb95f25c43845979532cb", i143);
        cNb = a51;
        fmVarArr[i143] = a51;
        int i144 = i143 + 1;
        C2491fm a52 = C4105zY.m41624a(Taikodom.class, "48ce123d0d5ff608a5ea4f0cb63ab6c5", i144);
        cNc = a52;
        fmVarArr[i144] = a52;
        int i145 = i144 + 1;
        C2491fm a53 = C4105zY.m41624a(Taikodom.class, "41e4087d5c33270af4faa0713eacf7d5", i145);
        cNd = a53;
        fmVarArr[i145] = a53;
        int i146 = i145 + 1;
        C2491fm a54 = C4105zY.m41624a(Taikodom.class, "1dacba7065972d7b91cf09e510a6ad0c", i146);
        cNe = a54;
        fmVarArr[i146] = a54;
        int i147 = i146 + 1;
        C2491fm a55 = C4105zY.m41624a(Taikodom.class, "bd4803cddc9a37d800f24661b775bcb5", i147);
        cNf = a55;
        fmVarArr[i147] = a55;
        int i148 = i147 + 1;
        C2491fm a56 = C4105zY.m41624a(Taikodom.class, "692279564f23b0483a3b502b27fed5e4", i148);
        cNg = a56;
        fmVarArr[i148] = a56;
        int i149 = i148 + 1;
        C2491fm a57 = C4105zY.m41624a(Taikodom.class, "0de9b4130ca894d3ab446879093a2058", i149);
        cNh = a57;
        fmVarArr[i149] = a57;
        int i150 = i149 + 1;
        C2491fm a58 = C4105zY.m41624a(Taikodom.class, "9503a4f201342dba9a132afef8b75015", i150);
        cNi = a58;
        fmVarArr[i150] = a58;
        int i151 = i150 + 1;
        C2491fm a59 = C4105zY.m41624a(Taikodom.class, "575905ad4b76e6eb158653bc1c1145de", i151);
        cNj = a59;
        fmVarArr[i151] = a59;
        int i152 = i151 + 1;
        C2491fm a60 = C4105zY.m41624a(Taikodom.class, "3f0d61917842675990c5909ef157bfdd", i152);
        cNk = a60;
        fmVarArr[i152] = a60;
        int i153 = i152 + 1;
        C2491fm a61 = C4105zY.m41624a(Taikodom.class, "3546abf3529528ec42ed1630c7880c64", i153);
        cNl = a61;
        fmVarArr[i153] = a61;
        int i154 = i153 + 1;
        C2491fm a62 = C4105zY.m41624a(Taikodom.class, "f15ea3a3140ec99f078831b78a738a5f", i154);
        cNm = a62;
        fmVarArr[i154] = a62;
        int i155 = i154 + 1;
        C2491fm a63 = C4105zY.m41624a(Taikodom.class, "f727800f0997709a92fce5007b046520", i155);
        cNn = a63;
        fmVarArr[i155] = a63;
        int i156 = i155 + 1;
        C2491fm a64 = C4105zY.m41624a(Taikodom.class, "7976c6f96dc5dd7e181c2d9793e63f84", i156);
        cNo = a64;
        fmVarArr[i156] = a64;
        int i157 = i156 + 1;
        C2491fm a65 = C4105zY.m41624a(Taikodom.class, "2fb3982b253392f5d0eab8388fde7cff", i157);
        cNp = a65;
        fmVarArr[i157] = a65;
        int i158 = i157 + 1;
        C2491fm a66 = C4105zY.m41624a(Taikodom.class, "bbdda9c48ddc4baadafea0a05f1432a9", i158);
        cNq = a66;
        fmVarArr[i158] = a66;
        int i159 = i158 + 1;
        C2491fm a67 = C4105zY.m41624a(Taikodom.class, "850d67b18cc06fbdd8ecc1d8edf936be", i159);
        cNr = a67;
        fmVarArr[i159] = a67;
        int i160 = i159 + 1;
        C2491fm a68 = C4105zY.m41624a(Taikodom.class, "4b57fe2ba1ff5e2f6c023e2132963c85", i160);
        cNs = a68;
        fmVarArr[i160] = a68;
        int i161 = i160 + 1;
        C2491fm a69 = C4105zY.m41624a(Taikodom.class, "e36d8b8874825a46e82880bdbcdb1a79", i161);
        cNt = a69;
        fmVarArr[i161] = a69;
        int i162 = i161 + 1;
        C2491fm a70 = C4105zY.m41624a(Taikodom.class, "aac2ab89c7205c143df85fd9a5a0f29e", i162);
        cNu = a70;
        fmVarArr[i162] = a70;
        int i163 = i162 + 1;
        C2491fm a71 = C4105zY.m41624a(Taikodom.class, "4f959bc282709e4b7c9a48713e55ed1d", i163);
        cNv = a71;
        fmVarArr[i163] = a71;
        int i164 = i163 + 1;
        C2491fm a72 = C4105zY.m41624a(Taikodom.class, "c1f661c1347e27f3ac79a449023059fc", i164);
        cNw = a72;
        fmVarArr[i164] = a72;
        int i165 = i164 + 1;
        C2491fm a73 = C4105zY.m41624a(Taikodom.class, "2e5152e22fcd63d51159d8d71925206d", i165);
        cNx = a73;
        fmVarArr[i165] = a73;
        int i166 = i165 + 1;
        C2491fm a74 = C4105zY.m41624a(Taikodom.class, "e8dd761fddd5bee0710e7e030038f0d2", i166);
        cNy = a74;
        fmVarArr[i166] = a74;
        int i167 = i166 + 1;
        C2491fm a75 = C4105zY.m41624a(Taikodom.class, "a909ec5accea41e9e281dfd085901411", i167);
        cNz = a75;
        fmVarArr[i167] = a75;
        int i168 = i167 + 1;
        C2491fm a76 = C4105zY.m41624a(Taikodom.class, "436dd71b67c8dc302ebee48256d2adfd", i168);
        cNA = a76;
        fmVarArr[i168] = a76;
        int i169 = i168 + 1;
        C2491fm a77 = C4105zY.m41624a(Taikodom.class, "87e64380f35cbf0ed909fc94f6f08a8a", i169);
        cNB = a77;
        fmVarArr[i169] = a77;
        int i170 = i169 + 1;
        C2491fm a78 = C4105zY.m41624a(Taikodom.class, "41ba29fde46b935d66a2b1b4b45db0c2", i170);
        cNC = a78;
        fmVarArr[i170] = a78;
        int i171 = i170 + 1;
        C2491fm a79 = C4105zY.m41624a(Taikodom.class, "e479fa387074acab7a3ec253c98df27b", i171);
        cND = a79;
        fmVarArr[i171] = a79;
        int i172 = i171 + 1;
        C2491fm a80 = C4105zY.m41624a(Taikodom.class, "041dc9cc4aa9145fe931a9b7a26be170", i172);
        cNE = a80;
        fmVarArr[i172] = a80;
        int i173 = i172 + 1;
        C2491fm a81 = C4105zY.m41624a(Taikodom.class, "1793d0eaf0dabd1b0f9ab3b11658b4e8", i173);
        cNF = a81;
        fmVarArr[i173] = a81;
        int i174 = i173 + 1;
        C2491fm a82 = C4105zY.m41624a(Taikodom.class, "f7235d762d7968b8918482caa792e612", i174);
        cNG = a82;
        fmVarArr[i174] = a82;
        int i175 = i174 + 1;
        C2491fm a83 = C4105zY.m41624a(Taikodom.class, "ff103412e3e909b73ea2197bb78b0753", i175);
        cNH = a83;
        fmVarArr[i175] = a83;
        int i176 = i175 + 1;
        C2491fm a84 = C4105zY.m41624a(Taikodom.class, "eae5326d78b5a9957cceebae729ed1cf", i176);
        cNI = a84;
        fmVarArr[i176] = a84;
        int i177 = i176 + 1;
        C2491fm a85 = C4105zY.m41624a(Taikodom.class, "23a8efd2c0d7c11d4f195ba583b5cf16", i177);
        cNJ = a85;
        fmVarArr[i177] = a85;
        int i178 = i177 + 1;
        C2491fm a86 = C4105zY.m41624a(Taikodom.class, "302944fd9678bdd9eb14694f6a0ff9b1", i178);
        cNK = a86;
        fmVarArr[i178] = a86;
        int i179 = i178 + 1;
        C2491fm a87 = C4105zY.m41624a(Taikodom.class, "5c416636c98fe4d4afd6de351f73ca14", i179);
        cNL = a87;
        fmVarArr[i179] = a87;
        int i180 = i179 + 1;
        C2491fm a88 = C4105zY.m41624a(Taikodom.class, "9690b8a987b95f0e7eb871da66cbb7a4", i180);
        cNM = a88;
        fmVarArr[i180] = a88;
        int i181 = i180 + 1;
        C2491fm a89 = C4105zY.m41624a(Taikodom.class, "efba55513f08d56fc4c9b4e5eebb5194", i181);
        cNN = a89;
        fmVarArr[i181] = a89;
        int i182 = i181 + 1;
        C2491fm a90 = C4105zY.m41624a(Taikodom.class, "2cb2397925ae0a6d5e0f4d99b8463c82", i182);
        cNO = a90;
        fmVarArr[i182] = a90;
        int i183 = i182 + 1;
        C2491fm a91 = C4105zY.m41624a(Taikodom.class, "84310b42a7f182b49c1acc17ebf0c4b8", i183);
        cNP = a91;
        fmVarArr[i183] = a91;
        int i184 = i183 + 1;
        C2491fm a92 = C4105zY.m41624a(Taikodom.class, "65c9d9358b117a90e22c87c2661979fb", i184);
        cNQ = a92;
        fmVarArr[i184] = a92;
        int i185 = i184 + 1;
        C2491fm a93 = C4105zY.m41624a(Taikodom.class, "1f960a66a76fc0a2cb807e28aef32b15", i185);
        cNR = a93;
        fmVarArr[i185] = a93;
        int i186 = i185 + 1;
        C2491fm a94 = C4105zY.m41624a(Taikodom.class, "c0f0c13411972590223963982ac9ce11", i186);
        cNS = a94;
        fmVarArr[i186] = a94;
        int i187 = i186 + 1;
        C2491fm a95 = C4105zY.m41624a(Taikodom.class, "fbcf11075418da1cdfbe3500eae8f084", i187);
        cNT = a95;
        fmVarArr[i187] = a95;
        int i188 = i187 + 1;
        C2491fm a96 = C4105zY.m41624a(Taikodom.class, "5670a4732d2488103891a78283abf3f0", i188);
        cNU = a96;
        fmVarArr[i188] = a96;
        int i189 = i188 + 1;
        C2491fm a97 = C4105zY.m41624a(Taikodom.class, "9ace36076b9924ad885fc74471b0f093", i189);
        cNV = a97;
        fmVarArr[i189] = a97;
        int i190 = i189 + 1;
        C2491fm a98 = C4105zY.m41624a(Taikodom.class, "5a0842d1607ed728aa38e20f57734e0c", i190);
        cNW = a98;
        fmVarArr[i190] = a98;
        int i191 = i190 + 1;
        C2491fm a99 = C4105zY.m41624a(Taikodom.class, "dd7aadf17000813b8ff34c75ba322804", i191);
        f401Pj = a99;
        fmVarArr[i191] = a99;
        int i192 = i191 + 1;
        C2491fm a100 = C4105zY.m41624a(Taikodom.class, "37bdae7690a79a599cb07a40fb1f866f", i192);
        f402Pk = a100;
        fmVarArr[i192] = a100;
        int i193 = i192 + 1;
        C2491fm a101 = C4105zY.m41624a(Taikodom.class, "f52557f657865ca0252be9d8f27ac406", i193);
        f400Pi = a101;
        fmVarArr[i193] = a101;
        int i194 = i193 + 1;
        C2491fm a102 = C4105zY.m41624a(Taikodom.class, "04f0f6c01c31e0fceeba16cc48719962", i194);
        cNX = a102;
        fmVarArr[i194] = a102;
        int i195 = i194 + 1;
        C2491fm a103 = C4105zY.m41624a(Taikodom.class, "62e09d584e8dd13246e5b4e935340eb8", i195);
        cNY = a103;
        fmVarArr[i195] = a103;
        int i196 = i195 + 1;
        C2491fm a104 = C4105zY.m41624a(Taikodom.class, "1496d4f96ff0dfa2ab378fb33ead2907", i196);
        cNZ = a104;
        fmVarArr[i196] = a104;
        int i197 = i196 + 1;
        C2491fm a105 = C4105zY.m41624a(Taikodom.class, "4509646ac73093604508ffd5c109e57d", i197);
        cOa = a105;
        fmVarArr[i197] = a105;
        int i198 = i197 + 1;
        C2491fm a106 = C4105zY.m41624a(Taikodom.class, "4b3d7235258ca191b8fe6e1ccddb4b72", i198);
        cOb = a106;
        fmVarArr[i198] = a106;
        int i199 = i198 + 1;
        C2491fm a107 = C4105zY.m41624a(Taikodom.class, "10a97fcac5f274044a2ee5e8e3b9516e", i199);
        cOc = a107;
        fmVarArr[i199] = a107;
        int i200 = i199 + 1;
        C2491fm a108 = C4105zY.m41624a(Taikodom.class, "e699dd80ac925f901dc1cdb86606f0ef", i200);
        cOd = a108;
        fmVarArr[i200] = a108;
        int i201 = i200 + 1;
        C2491fm a109 = C4105zY.m41624a(Taikodom.class, "bd4596eae7fbc45d576865851960aa8f", i201);
        cOe = a109;
        fmVarArr[i201] = a109;
        int i202 = i201 + 1;
        C2491fm a110 = C4105zY.m41624a(Taikodom.class, "20b746d8e5cb233f3281b538661101b2", i202);
        aPv = a110;
        fmVarArr[i202] = a110;
        int i203 = i202 + 1;
        C2491fm a111 = C4105zY.m41624a(Taikodom.class, "957dc0835d8ed6877d035c3165836fd1", i203);
        cOf = a111;
        fmVarArr[i203] = a111;
        int i204 = i203 + 1;
        C2491fm a112 = C4105zY.m41624a(Taikodom.class, "3df0e59e8557bb1411dc21cf6eec2267", i204);
        cOg = a112;
        fmVarArr[i204] = a112;
        int i205 = i204 + 1;
        C2491fm a113 = C4105zY.m41624a(Taikodom.class, "983426de6d7c26207aac76bd2c087698", i205);
        cOh = a113;
        fmVarArr[i205] = a113;
        int i206 = i205 + 1;
        C2491fm a114 = C4105zY.m41624a(Taikodom.class, "4b4db5fe0ac84965a877103fe5940f5e", i206);
        cOi = a114;
        fmVarArr[i206] = a114;
        int i207 = i206 + 1;
        C2491fm a115 = C4105zY.m41624a(Taikodom.class, "a6292d0d53885508fa46d760fe643793", i207);
        cOj = a115;
        fmVarArr[i207] = a115;
        int i208 = i207 + 1;
        C2491fm a116 = C4105zY.m41624a(Taikodom.class, "a066889da3652f82a4a30b136d854225", i208);
        aQo = a116;
        fmVarArr[i208] = a116;
        int i209 = i208 + 1;
        C2491fm a117 = C4105zY.m41624a(Taikodom.class, "ab1fcbb497e427d5f0cd3bd5ad231483", i209);
        aQp = a117;
        fmVarArr[i209] = a117;
        int i210 = i209 + 1;
        C2491fm a118 = C4105zY.m41624a(Taikodom.class, "89ae532fe8e7a6b03a39e8d6d0211ff1", i210);
        aQq = a118;
        fmVarArr[i210] = a118;
        int i211 = i210 + 1;
        C2491fm a119 = C4105zY.m41624a(Taikodom.class, "91fcb11d02fbc0fa58a558e4c9c47e35", i211);
        cOk = a119;
        fmVarArr[i211] = a119;
        int i212 = i211 + 1;
        C2491fm a120 = C4105zY.m41624a(Taikodom.class, "a2b697a1f86008a7f8f2460ba25094da", i212);
        cOl = a120;
        fmVarArr[i212] = a120;
        int i213 = i212 + 1;
        C2491fm a121 = C4105zY.m41624a(Taikodom.class, "b952ccc6a181b0222eb5c0bb3429c7da", i213);
        cOm = a121;
        fmVarArr[i213] = a121;
        int i214 = i213 + 1;
        C2491fm a122 = C4105zY.m41624a(Taikodom.class, "776659e30ea3a1c1280ce8100d8ee9c1", i214);
        cOn = a122;
        fmVarArr[i214] = a122;
        int i215 = i214 + 1;
        C2491fm a123 = C4105zY.m41624a(Taikodom.class, "93bfb9f7f755626566e9c645e86ffd6a", i215);
        cOo = a123;
        fmVarArr[i215] = a123;
        int i216 = i215 + 1;
        C2491fm a124 = C4105zY.m41624a(Taikodom.class, "1436e0a25ee6862cd6d679a6ae95ed49", i216);
        cOp = a124;
        fmVarArr[i216] = a124;
        int i217 = i216 + 1;
        C2491fm a125 = C4105zY.m41624a(Taikodom.class, "44e41c0f32fdfad2de69524cfebce44b", i217);
        coN = a125;
        fmVarArr[i217] = a125;
        int i218 = i217 + 1;
        C2491fm a126 = C4105zY.m41624a(Taikodom.class, "002d8f31c870cf67604089607a350983", i218);
        cOq = a126;
        fmVarArr[i218] = a126;
        int i219 = i218 + 1;
        C2491fm a127 = C4105zY.m41624a(Taikodom.class, "5b77b2e2e266edcc0bbfc74589528e20", i219);
        aKm = a127;
        fmVarArr[i219] = a127;
        int i220 = i219 + 1;
        C2491fm a128 = C4105zY.m41624a(Taikodom.class, "18a5cf8603dad388d8f3723083e489d1", i220);
        cOr = a128;
        fmVarArr[i220] = a128;
        int i221 = i220 + 1;
        C2491fm a129 = C4105zY.m41624a(Taikodom.class, "8a980c6f7a2871c94ff05f830de9b0c1", i221);
        cOs = a129;
        fmVarArr[i221] = a129;
        int i222 = i221 + 1;
        C2491fm a130 = C4105zY.m41624a(Taikodom.class, "d384d6bac70707027a9d79ed429916b0", i222);
        cOt = a130;
        fmVarArr[i222] = a130;
        int i223 = i222 + 1;
        C2491fm a131 = C4105zY.m41624a(Taikodom.class, "75596775ced74a250cb02e7397a4537c", i223);
        cOu = a131;
        fmVarArr[i223] = a131;
        int i224 = i223 + 1;
        C2491fm a132 = C4105zY.m41624a(Taikodom.class, "c56bdc4bcac450402181bdcdc9e09f85", i224);
        cOv = a132;
        fmVarArr[i224] = a132;
        int i225 = i224 + 1;
        C2491fm a133 = C4105zY.m41624a(Taikodom.class, "6e104062ae6bceb1ef6bfcbacd3722b9", i225);
        cOw = a133;
        fmVarArr[i225] = a133;
        int i226 = i225 + 1;
        C2491fm a134 = C4105zY.m41624a(Taikodom.class, "485f9c3cf95005c21bec8be0e996f092", i226);
        cOx = a134;
        fmVarArr[i226] = a134;
        int i227 = i226 + 1;
        C2491fm a135 = C4105zY.m41624a(Taikodom.class, "ccaadb59227a1a8b121cb339c0050640", i227);
        cOy = a135;
        fmVarArr[i227] = a135;
        int i228 = i227 + 1;
        C2491fm a136 = C4105zY.m41624a(Taikodom.class, "984b7ec5840a875c3e86df8c1d05a320", i228);
        cOz = a136;
        fmVarArr[i228] = a136;
        int i229 = i228 + 1;
        C2491fm a137 = C4105zY.m41624a(Taikodom.class, "6483eb7a6d215aadb0c2285468e2f52c", i229);
        cOA = a137;
        fmVarArr[i229] = a137;
        int i230 = i229 + 1;
        C2491fm a138 = C4105zY.m41624a(Taikodom.class, "b0800225ed939258cf11919c874de598", i230);
        cOB = a138;
        fmVarArr[i230] = a138;
        int i231 = i230 + 1;
        C2491fm a139 = C4105zY.m41624a(Taikodom.class, "f81a594bbc67f4977c5625e3e0051aec", i231);
        cOC = a139;
        fmVarArr[i231] = a139;
        int i232 = i231 + 1;
        C2491fm a140 = C4105zY.m41624a(Taikodom.class, "536d4648fb5ffc4081ae022351f3d50d", i232);
        cOD = a140;
        fmVarArr[i232] = a140;
        int i233 = i232 + 1;
        C2491fm a141 = C4105zY.m41624a(Taikodom.class, "6328feee933387e4b55b4e841743a00d", i233);
        cOE = a141;
        fmVarArr[i233] = a141;
        int i234 = i233 + 1;
        C2491fm a142 = C4105zY.m41624a(Taikodom.class, "0f2901bb4b8819c133de11f7873a547e", i234);
        cOF = a142;
        fmVarArr[i234] = a142;
        int i235 = i234 + 1;
        C2491fm a143 = C4105zY.m41624a(Taikodom.class, "674c4dd9ce05df715e5b89fa48ce1b85", i235);
        cOG = a143;
        fmVarArr[i235] = a143;
        int i236 = i235 + 1;
        C2491fm a144 = C4105zY.m41624a(Taikodom.class, "fd950c35eb3059cfb078cd89e9c2bc7b", i236);
        cOH = a144;
        fmVarArr[i236] = a144;
        int i237 = i236 + 1;
        C2491fm a145 = C4105zY.m41624a(Taikodom.class, "d296bead2dfb4af60e1bbf7c4a6350cd", i237);
        cOI = a145;
        fmVarArr[i237] = a145;
        int i238 = i237 + 1;
        C2491fm a146 = C4105zY.m41624a(Taikodom.class, "0a0b7f0b72ec3bd59d98fc1e58ab4ce3", i238);
        cOJ = a146;
        fmVarArr[i238] = a146;
        int i239 = i238 + 1;
        C2491fm a147 = C4105zY.m41624a(Taikodom.class, "7c854c0a5049c89e1926a6fc28273d3e", i239);
        cOK = a147;
        fmVarArr[i239] = a147;
        int i240 = i239 + 1;
        C2491fm a148 = C4105zY.m41624a(Taikodom.class, "5ed549dc4068fdfc922c98c64996a1eb", i240);
        cOL = a148;
        fmVarArr[i240] = a148;
        int i241 = i240 + 1;
        C2491fm a149 = C4105zY.m41624a(Taikodom.class, "bf0664fb210e4b80cc58d622cf907e87", i241);
        cOM = a149;
        fmVarArr[i241] = a149;
        int i242 = i241 + 1;
        C2491fm a150 = C4105zY.m41624a(Taikodom.class, "0786c335b725542958ee843b411170f6", i242);
        cON = a150;
        fmVarArr[i242] = a150;
        int i243 = i242 + 1;
        C2491fm a151 = C4105zY.m41624a(Taikodom.class, "dd64bed9e02e41d9b9f77c2d13ae43ba", i243);
        cOO = a151;
        fmVarArr[i243] = a151;
        int i244 = i243 + 1;
        C2491fm a152 = C4105zY.m41624a(Taikodom.class, "8de8f06251f095d154e350c707b67cf7", i244);
        cOP = a152;
        fmVarArr[i244] = a152;
        int i245 = i244 + 1;
        C2491fm a153 = C4105zY.m41624a(Taikodom.class, "daf1005f35882e60e3465a405ed168f8", i245);
        f403Pl = a153;
        fmVarArr[i245] = a153;
        int i246 = i245 + 1;
        C2491fm a154 = C4105zY.m41624a(Taikodom.class, "c1ed7a7ec3adc940330fd2fbb6d04672", i246);
        cOQ = a154;
        fmVarArr[i246] = a154;
        int i247 = i246 + 1;
        C2491fm a155 = C4105zY.m41624a(Taikodom.class, "b347baf7d675c28b3235dcbdf5ed59d4", i247);
        cOR = a155;
        fmVarArr[i247] = a155;
        int i248 = i247 + 1;
        C2491fm a156 = C4105zY.m41624a(Taikodom.class, "e990e803e5722d72c7dd04be2db9003e", i248);
        cOS = a156;
        fmVarArr[i248] = a156;
        int i249 = i248 + 1;
        C2491fm a157 = C4105zY.m41624a(Taikodom.class, "afb80824c81f9ffc3b16311df663633c", i249);
        cOT = a157;
        fmVarArr[i249] = a157;
        int i250 = i249 + 1;
        C2491fm a158 = C4105zY.m41624a(Taikodom.class, "910c0a442e130190b8ede83ac2f6dfcd", i250);
        cOU = a158;
        fmVarArr[i250] = a158;
        int i251 = i250 + 1;
        C2491fm a159 = C4105zY.m41624a(Taikodom.class, "f90830d22a94e2e6892a0f76a8206192", i251);
        cOV = a159;
        fmVarArr[i251] = a159;
        int i252 = i251 + 1;
        C2491fm a160 = C4105zY.m41624a(Taikodom.class, "616b5eb91f76dfff5beb294e8d426d67", i252);
        cOW = a160;
        fmVarArr[i252] = a160;
        int i253 = i252 + 1;
        C2491fm a161 = C4105zY.m41624a(Taikodom.class, "468d7a5d7f7d923769a691082e685aaa", i253);
        cOX = a161;
        fmVarArr[i253] = a161;
        int i254 = i253 + 1;
        C2491fm a162 = C4105zY.m41624a(Taikodom.class, "9d19b155d5b4dd9dd03ad63924560155", i254);
        cOY = a162;
        fmVarArr[i254] = a162;
        int i255 = i254 + 1;
        C2491fm a163 = C4105zY.m41624a(Taikodom.class, "ace4b3b7359b605ce0667f8156ff33f7", i255);
        cOZ = a163;
        fmVarArr[i255] = a163;
        int i256 = i255 + 1;
        C2491fm a164 = C4105zY.m41624a(Taikodom.class, "8601a506401ad5f0f61a03c266a17d36", i256);
        cPa = a164;
        fmVarArr[i256] = a164;
        int i257 = i256 + 1;
        C2491fm a165 = C4105zY.m41624a(Taikodom.class, "c22f47772030089de27f56102d759bfa", i257);
        cPb = a165;
        fmVarArr[i257] = a165;
        int i258 = i257 + 1;
        C2491fm a166 = C4105zY.m41624a(Taikodom.class, "98ddb1d107eecd56e79fe7a178e66e90", i258);
        cPc = a166;
        fmVarArr[i258] = a166;
        int i259 = i258 + 1;
        C2491fm a167 = C4105zY.m41624a(Taikodom.class, "0a4c79bac1958834c342df54bbdc5ac3", i259);
        cPd = a167;
        fmVarArr[i259] = a167;
        int i260 = i259 + 1;
        C2491fm a168 = C4105zY.m41624a(Taikodom.class, "a255afa962529230a413c58b2e2a6fe3", i260);
        cPe = a168;
        fmVarArr[i260] = a168;
        int i261 = i260 + 1;
        C2491fm a169 = C4105zY.m41624a(Taikodom.class, "1c474e1b55d08ae2be89ded40f055d39", i261);
        cPf = a169;
        fmVarArr[i261] = a169;
        int i262 = i261 + 1;
        C2491fm a170 = C4105zY.m41624a(Taikodom.class, "1af04e5b79f255e6c45b9f3fd9e52f52", i262);
        cPg = a170;
        fmVarArr[i262] = a170;
        int i263 = i262 + 1;
        C2491fm a171 = C4105zY.m41624a(Taikodom.class, "becf0b905eeabca2ef650ca6a69cd629", i263);
        cPh = a171;
        fmVarArr[i263] = a171;
        int i264 = i263 + 1;
        C2491fm a172 = C4105zY.m41624a(Taikodom.class, "fa1d68531463c2f79ccf6cfecb28a143", i264);
        cPi = a172;
        fmVarArr[i264] = a172;
        int i265 = i264 + 1;
        C2491fm a173 = C4105zY.m41624a(Taikodom.class, "c55f5fad2682130e2a6d6874de8cc5b8", i265);
        cPj = a173;
        fmVarArr[i265] = a173;
        int i266 = i265 + 1;
        C2491fm a174 = C4105zY.m41624a(Taikodom.class, "092253968265f913f3d9cd990b28c624", i266);
        cPk = a174;
        fmVarArr[i266] = a174;
        int i267 = i266 + 1;
        C2491fm a175 = C4105zY.m41624a(Taikodom.class, "586acb7f596a66a46b5749c28d0916b4", i267);
        cPl = a175;
        fmVarArr[i267] = a175;
        int i268 = i267 + 1;
        C2491fm a176 = C4105zY.m41624a(Taikodom.class, "772cd2faca8dd98f24c70012c8d50a0f", i268);
        cPm = a176;
        fmVarArr[i268] = a176;
        int i269 = i268 + 1;
        C2491fm a177 = C4105zY.m41624a(Taikodom.class, "6135c924731b82b5a6215f5f6c265cda", i269);
        cPn = a177;
        fmVarArr[i269] = a177;
        int i270 = i269 + 1;
        C2491fm a178 = C4105zY.m41624a(Taikodom.class, "88de604c1d3bd8a6e09949426841b7a5", i270);
        cPo = a178;
        fmVarArr[i270] = a178;
        int i271 = i270 + 1;
        C2491fm a179 = C4105zY.m41624a(Taikodom.class, "f67639437824954b9c7649b88c33a049", i271);
        cPp = a179;
        fmVarArr[i271] = a179;
        int i272 = i271 + 1;
        C2491fm a180 = C4105zY.m41624a(Taikodom.class, "4ec504f0d827654374354a2439f53708", i272);
        cPq = a180;
        fmVarArr[i272] = a180;
        int i273 = i272 + 1;
        C2491fm a181 = C4105zY.m41624a(Taikodom.class, "3933dfc5449b049f31bfeb90f2d9ea9f", i273);
        cPr = a181;
        fmVarArr[i273] = a181;
        int i274 = i273 + 1;
        C2491fm a182 = C4105zY.m41624a(Taikodom.class, "681aa3d1268022519bada998b8155dd8", i274);
        cPs = a182;
        fmVarArr[i274] = a182;
        int i275 = i274 + 1;
        C2491fm a183 = C4105zY.m41624a(Taikodom.class, "835319c87e87edcf090ff3b50e3e6ef9", i275);
        cPt = a183;
        fmVarArr[i275] = a183;
        int i276 = i275 + 1;
        C2491fm a184 = C4105zY.m41624a(Taikodom.class, "77165ffbcd699325dd616ebf41f7b122", i276);
        cPu = a184;
        fmVarArr[i276] = a184;
        int i277 = i276 + 1;
        C2491fm a185 = C4105zY.m41624a(Taikodom.class, "7c244eee9db2d5af490870ae756be07a", i277);
        cPv = a185;
        fmVarArr[i277] = a185;
        int i278 = i277 + 1;
        C2491fm a186 = C4105zY.m41624a(Taikodom.class, "d364334c288cdc6d3a2b3178a34d2b6b", i278);
        cPw = a186;
        fmVarArr[i278] = a186;
        int i279 = i278 + 1;
        C2491fm a187 = C4105zY.m41624a(Taikodom.class, "430b1c4734f8cf9416855e08cc5ddd4c", i279);
        cPx = a187;
        fmVarArr[i279] = a187;
        int i280 = i279 + 1;
        C2491fm a188 = C4105zY.m41624a(Taikodom.class, "d8e5dd5809b21e47939e523073ef4f0e", i280);
        cPy = a188;
        fmVarArr[i280] = a188;
        int i281 = i280 + 1;
        C2491fm a189 = C4105zY.m41624a(Taikodom.class, "2f3cd1d13d10225cc3abb20f7da2fd8d", i281);
        cPz = a189;
        fmVarArr[i281] = a189;
        int i282 = i281 + 1;
        C2491fm a190 = C4105zY.m41624a(Taikodom.class, "6361783c8d6b70f95ed784ef70a492b4", i282);
        aMl = a190;
        fmVarArr[i282] = a190;
        int i283 = i282 + 1;
        C2491fm a191 = C4105zY.m41624a(Taikodom.class, "1cfbd9e98fcbcb7c50ce019246630f37", i283);
        aMm = a191;
        fmVarArr[i283] = a191;
        int i284 = i283 + 1;
        C2491fm a192 = C4105zY.m41624a(Taikodom.class, "40541b8e67f77a6e289bf202ca3382c9", i284);
        aMk = a192;
        fmVarArr[i284] = a192;
        int i285 = i284 + 1;
        C2491fm a193 = C4105zY.m41624a(Taikodom.class, "c8afaf0554c32dfbcc544af811a3811f", i285);
        cPA = a193;
        fmVarArr[i285] = a193;
        int i286 = i285 + 1;
        C2491fm a194 = C4105zY.m41624a(Taikodom.class, "e06cd48a86ebf3655441f1f2afae03f2", i286);
        cPB = a194;
        fmVarArr[i286] = a194;
        int i287 = i286 + 1;
        C2491fm a195 = C4105zY.m41624a(Taikodom.class, "7ed52a92dd7331c8079bdb70173be7db", i287);
        cPC = a195;
        fmVarArr[i287] = a195;
        int i288 = i287 + 1;
        C2491fm a196 = C4105zY.m41624a(Taikodom.class, "997cf89e0df74eae6339586bebb771ec", i288);
        cPD = a196;
        fmVarArr[i288] = a196;
        int i289 = i288 + 1;
        C2491fm a197 = C4105zY.m41624a(Taikodom.class, "c3f2152acfcb838349ce0986b5db846b", i289);
        cPE = a197;
        fmVarArr[i289] = a197;
        int i290 = i289 + 1;
        C2491fm a198 = C4105zY.m41624a(Taikodom.class, "3b09f95ed01564af3ebfc571206de526", i290);
        cPF = a198;
        fmVarArr[i290] = a198;
        int i291 = i290 + 1;
        C2491fm a199 = C4105zY.m41624a(Taikodom.class, "d5675d6929bea2bc551b94d84babc402", i291);
        cPG = a199;
        fmVarArr[i291] = a199;
        int i292 = i291 + 1;
        C2491fm a200 = C4105zY.m41624a(Taikodom.class, "10dac824a6c192150d741077b8770d46", i292);
        cPH = a200;
        fmVarArr[i292] = a200;
        int i293 = i292 + 1;
        C2491fm a201 = C4105zY.m41624a(Taikodom.class, "51e7809fcfade36956d65e4217da6654", i293);
        cPI = a201;
        fmVarArr[i293] = a201;
        int i294 = i293 + 1;
        C2491fm a202 = C4105zY.m41624a(Taikodom.class, "96aa00b0720a092d293039ecf34039bf", i294);
        cPJ = a202;
        fmVarArr[i294] = a202;
        int i295 = i294 + 1;
        C2491fm a203 = C4105zY.m41624a(Taikodom.class, "377f46fe0a77057c479943439dffc696", i295);
        cPK = a203;
        fmVarArr[i295] = a203;
        int i296 = i295 + 1;
        C2491fm a204 = C4105zY.m41624a(Taikodom.class, "333cd06a8b0123e0c704b28d7f19ed7a", i296);
        cPL = a204;
        fmVarArr[i296] = a204;
        int i297 = i296 + 1;
        C2491fm a205 = C4105zY.m41624a(Taikodom.class, "ea5da1b25617a71c860f34c6121f7e09", i297);
        cPM = a205;
        fmVarArr[i297] = a205;
        int i298 = i297 + 1;
        C2491fm a206 = C4105zY.m41624a(Taikodom.class, "25b35eaa5aa30c501eb044f18e222494", i298);
        cPN = a206;
        fmVarArr[i298] = a206;
        int i299 = i298 + 1;
        C2491fm a207 = C4105zY.m41624a(Taikodom.class, "4b2830f073ba586bb7f1c493601417ca", i299);
        cPO = a207;
        fmVarArr[i299] = a207;
        int i300 = i299 + 1;
        C2491fm a208 = C4105zY.m41624a(Taikodom.class, "dbe55392c624bd260aa4dfed9a793c74", i300);
        cPP = a208;
        fmVarArr[i300] = a208;
        int i301 = i300 + 1;
        C2491fm a209 = C4105zY.m41624a(Taikodom.class, "d8746ad7fdde1d85abd585e57dc96a08", i301);
        coL = a209;
        fmVarArr[i301] = a209;
        int i302 = i301 + 1;
        C2491fm a210 = C4105zY.m41624a(Taikodom.class, "d4ca218daa775a55c343e7c8fd2d3049", i302);
        cPQ = a210;
        fmVarArr[i302] = a210;
        int i303 = i302 + 1;
        C2491fm a211 = C4105zY.m41624a(Taikodom.class, "e4081ed8e6a99f21836dfc096255a7f3", i303);
        cPR = a211;
        fmVarArr[i303] = a211;
        int i304 = i303 + 1;
        C2491fm a212 = C4105zY.m41624a(Taikodom.class, "73f4bca4c72587d98dc4e1cfad5f34aa", i304);
        cPS = a212;
        fmVarArr[i304] = a212;
        int i305 = i304 + 1;
        C2491fm a213 = C4105zY.m41624a(Taikodom.class, "24883133cf2d55e54496dd9736371910", i305);
        cPT = a213;
        fmVarArr[i305] = a213;
        int i306 = i305 + 1;
        C2491fm a214 = C4105zY.m41624a(Taikodom.class, "14cc99dfa245b2c49bbe50dfd4074fe4", i306);
        cPU = a214;
        fmVarArr[i306] = a214;
        int i307 = i306 + 1;
        C2491fm a215 = C4105zY.m41624a(Taikodom.class, "d2bcd03f6495b68217cefcad5d00f9f2", i307);
        cPV = a215;
        fmVarArr[i307] = a215;
        int i308 = i307 + 1;
        C2491fm a216 = C4105zY.m41624a(Taikodom.class, "f72549ab3f58f6eaef767077e93fc3b6", i308);
        cPW = a216;
        fmVarArr[i308] = a216;
        int i309 = i308 + 1;
        C2491fm a217 = C4105zY.m41624a(Taikodom.class, "987dc1e4ecf861a78b9a85c7609a0708", i309);
        cPX = a217;
        fmVarArr[i309] = a217;
        int i310 = i309 + 1;
        C2491fm a218 = C4105zY.m41624a(Taikodom.class, "3233219dcd84701fc713ae73f7b40182", i310);
        cPY = a218;
        fmVarArr[i310] = a218;
        int i311 = i310 + 1;
        C2491fm a219 = C4105zY.m41624a(Taikodom.class, "e8f30bd772a9bd83a668c8835170c1db", i311);
        cPZ = a219;
        fmVarArr[i311] = a219;
        int i312 = i311 + 1;
        C2491fm a220 = C4105zY.m41624a(Taikodom.class, "42bd8741ed9af1f8a2625de5ed3dad5e", i312);
        cQa = a220;
        fmVarArr[i312] = a220;
        int i313 = i312 + 1;
        C2491fm a221 = C4105zY.m41624a(Taikodom.class, "fc8f935091b7791d54e13b07dbb85776", i313);
        f412lw = a221;
        fmVarArr[i313] = a221;
        int i314 = i313 + 1;
        C2491fm a222 = C4105zY.m41624a(Taikodom.class, "b1a2de9085dcd218bcb64420c705f26f", i314);
        cQb = a222;
        fmVarArr[i314] = a222;
        int i315 = i314 + 1;
        C2491fm a223 = C4105zY.m41624a(Taikodom.class, "8583c4005994c581e4aee58a8cf91ed4", i315);
        cQc = a223;
        fmVarArr[i315] = a223;
        int i316 = i315 + 1;
        C2491fm a224 = C4105zY.m41624a(Taikodom.class, "5afb88cee7105ab604d5609ea483d841", i316);
        cQd = a224;
        fmVarArr[i316] = a224;
        int i317 = i316 + 1;
        C2491fm a225 = C4105zY.m41624a(Taikodom.class, "7420b4388b17b8bc8d5464dfc87cfc89", i317);
        cQe = a225;
        fmVarArr[i317] = a225;
        int i318 = i317 + 1;
        C2491fm a226 = C4105zY.m41624a(Taikodom.class, "962ba4f4f2b7066ec13ccb3cbfb4a6a8", i318);
        cQf = a226;
        fmVarArr[i318] = a226;
        int i319 = i318 + 1;
        C2491fm a227 = C4105zY.m41624a(Taikodom.class, "869b97d0e3e02f04dd7b09446e7086ba", i319);
        cQg = a227;
        fmVarArr[i319] = a227;
        int i320 = i319 + 1;
        C2491fm a228 = C4105zY.m41624a(Taikodom.class, "67218682225f2da2fe778afbbce9f174", i320);
        f405x20176b18 = a228;
        fmVarArr[i320] = a228;
        int i321 = i320 + 1;
        C2491fm a229 = C4105zY.m41624a(Taikodom.class, "a572c19b24adc460aebb74eb4e99ede1", i321);
        cQh = a229;
        fmVarArr[i321] = a229;
        int i322 = i321 + 1;
        C2491fm a230 = C4105zY.m41624a(Taikodom.class, "d8fa460960c6248564a4edf4a92c312b", i322);
        cQi = a230;
        fmVarArr[i322] = a230;
        int i323 = i322 + 1;
        C2491fm a231 = C4105zY.m41624a(Taikodom.class, "5279e1144f4297b06c1259cd45b8dc36", i323);
        cQj = a231;
        fmVarArr[i323] = a231;
        int i324 = i323 + 1;
        C2491fm a232 = C4105zY.m41624a(Taikodom.class, "7ebec3e9cd29fd9a202408e9a4e85641", i324);
        cQk = a232;
        fmVarArr[i324] = a232;
        int i325 = i324 + 1;
        C2491fm a233 = C4105zY.m41624a(Taikodom.class, "fce8f146e812d86d81bd22caf7de44c7", i325);
        cQl = a233;
        fmVarArr[i325] = a233;
        int i326 = i325 + 1;
        C2491fm a234 = C4105zY.m41624a(Taikodom.class, "4cef72a93c0c1c0bd408d43e28719fef", i326);
        cQm = a234;
        fmVarArr[i326] = a234;
        int i327 = i326 + 1;
        C2491fm a235 = C4105zY.m41624a(Taikodom.class, "d555e83d924836f010dbf5858107c638", i327);
        f404x3cb2b465 = a235;
        fmVarArr[i327] = a235;
        int i328 = i327 + 1;
        C2491fm a236 = C4105zY.m41624a(Taikodom.class, "d49c78462ea642fefbc582050d4294dd", i328);
        cQn = a236;
        fmVarArr[i328] = a236;
        int i329 = i328 + 1;
        C2491fm a237 = C4105zY.m41624a(Taikodom.class, "bb54c80318f55c7550c2d8ed3dfe02aa", i329);
        cQo = a237;
        fmVarArr[i329] = a237;
        int i330 = i329 + 1;
        C2491fm a238 = C4105zY.m41624a(Taikodom.class, "29ce30e1f2a5394f9cdf1d2662a9eee7", i330);
        _f_onResurrect_0020_0028_0029V = a238;
        fmVarArr[i330] = a238;
        int i331 = i330 + 1;
        C2491fm a239 = C4105zY.m41624a(Taikodom.class, "2b1edf156136e04a562d67fd0264435a", i331);
        cQp = a239;
        fmVarArr[i331] = a239;
        int i332 = i331 + 1;
        C2491fm a240 = C4105zY.m41624a(Taikodom.class, "1afbecb2c4ccdcaf97d9b1d1b8a2badc", i332);
        f410bP = a240;
        fmVarArr[i332] = a240;
        int i333 = i332 + 1;
        C2491fm a241 = C4105zY.m41624a(Taikodom.class, "14af8d419602e98f07beedb0d2c3020b", i333);
        cQq = a241;
        fmVarArr[i333] = a241;
        int i334 = i333 + 1;
        C2491fm a242 = C4105zY.m41624a(Taikodom.class, "a84b13e439ec89ceb94c8801a3a2ea51", i334);
        cQr = a242;
        fmVarArr[i334] = a242;
        int i335 = i334 + 1;
        C2491fm a243 = C4105zY.m41624a(Taikodom.class, "4266ade80491a775076264b826327df4", i335);
        cQs = a243;
        fmVarArr[i335] = a243;
        int i336 = i335 + 1;
        C2491fm a244 = C4105zY.m41624a(Taikodom.class, "6badb8c0a6aa6cd06afb57f769427d44", i336);
        cQt = a244;
        fmVarArr[i336] = a244;
        int i337 = i336 + 1;
        _m_methods = fmVarArr;
        C1634Xv.m11725a((Object[]) TaikodomObject._m_methods, (Object[]) _m_methods);
        ___iScriptClass = aUO.m18566a(Taikodom.class, aNG.class, _m_fields, _m_methods);
    }

    /* renamed from: A */
    private void m2000A(C2686iZ iZVar) {
        bFf().mo5608dq().mo3197f(cJd, iZVar);
    }

    /* renamed from: B */
    private void m2001B(C2686iZ iZVar) {
        bFf().mo5608dq().mo3197f(cKn, iZVar);
    }

    /* renamed from: B */
    private void m2002B(C3438ra raVar) {
        bFf().mo5608dq().mo3197f(aPq, raVar);
    }

    /* renamed from: C */
    private void m2003C(C1556Wo wo) {
        bFf().mo5608dq().mo3197f(cLT, wo);
    }

    /* renamed from: C */
    private void m2004C(C2686iZ iZVar) {
        bFf().mo5608dq().mo3197f(cKp, iZVar);
    }

    /* renamed from: C */
    private void m2005C(C3438ra raVar) {
        bFf().mo5608dq().mo3197f(aQn, raVar);
    }

    /* renamed from: D */
    private void m2006D(C2686iZ iZVar) {
        bFf().mo5608dq().mo3197f(cLP, iZVar);
    }

    /* renamed from: D */
    private void m2007D(C3438ra raVar) {
        bFf().mo5608dq().mo3197f(aQU, raVar);
    }

    /* renamed from: E */
    private void m2008E(C2686iZ iZVar) {
        bFf().mo5608dq().mo3197f(cLR, iZVar);
    }

    /* renamed from: K */
    private void m2009K(C3438ra raVar) {
        bFf().mo5608dq().mo3197f(bok, raVar);
    }

    /* renamed from: Ll */
    private C3438ra m2010Ll() {
        return (C3438ra) bFf().mo5608dq().mo3214p(awd);
    }

    @C0064Am(aul = "5b77b2e2e266edcc0bbfc74589528e20", aum = 0)
    @C5566aOg
    @C2499fr
    /* renamed from: QR */
    private void m2011QR() {
        throw new aWi(new aCE(this, aKm, new Object[0]));
    }

    /* renamed from: S */
    private void m2013S(C3438ra raVar) {
        bFf().mo5608dq().mo3197f(bCb, raVar);
    }

    /* renamed from: TT */
    private C3438ra m2014TT() {
        return (C3438ra) bFf().mo5608dq().mo3214p(aPq);
    }

    /* renamed from: UG */
    private C3438ra m2016UG() {
        return (C3438ra) bFf().mo5608dq().mo3214p(aQU);
    }

    /* renamed from: Ut */
    private C3438ra m2017Ut() {
        return (C3438ra) bFf().mo5608dq().mo3214p(aQn);
    }

    @C0064Am(aul = "361d30cfe39989d6a58c19c8d9105ea3", aum = 0)
    @C5566aOg
    @C0909NL
    /* renamed from: a */
    private User m2021a(String str, String str2, String str3, String str4) {
        throw new aWi(new aCE(this, cMQ, new Object[]{str, str2, str3, str4}));
    }

    @C0064Am(aul = "d436c4eb2a7e3f6759f5dc82cf4274d9", aum = 0)
    @C5566aOg
    /* renamed from: a */
    private Connect.Status m2022a(User adk, String str, String str2) {
        throw new aWi(new aCE(this, cMM, new Object[]{adk, str, str2}));
    }

    /* renamed from: a */
    private void m2024a(MarkManager as) {
        bFf().mo5608dq().mo3197f(cLe, as);
    }

    /* renamed from: a */
    private void m2025a(ServerEditorUtility dk) {
        bFf().mo5608dq().mo3197f(cLk, dk);
    }

    /* renamed from: a */
    private void m2026a(AvatarManager dq) {
        bFf().mo5608dq().mo3197f(cKK, dq);
    }

    /* renamed from: a */
    private void m2027a(CorporationReservedNames gd) {
        bFf().mo5608dq().mo3197f(cLx, gd);
    }

    /* renamed from: a */
    private void m2029a(C0694Ju ju) {
        bFf().mo5608dq().mo3197f(cLD, ju);
    }

    /* renamed from: a */
    private void m2033a(AssetGroup ne) {
        bFf().mo5608dq().mo3197f(cKY, ne);
    }

    /* renamed from: a */
    private void m2034a(Market oi) {
        bFf().mo5608dq().mo3197f(cLF, oi);
    }

    /* renamed from: a */
    private void m2035a(C1077Pl pl) {
        bFf().mo5608dq().mo3197f(cLB, pl);
    }

    /* renamed from: a */
    private void m2036a(CitizenshipOffice sk) {
        bFf().mo5608dq().mo3197f(cKS, sk);
    }

    /* renamed from: a */
    private void m2038a(NLSManager vf) {
        bFf().mo5608dq().mo3197f(cKI, vf);
    }

    /* renamed from: a */
    private void m2039a(SpaceZoneTypesManager vt) {
        bFf().mo5608dq().mo3197f(cKQ, vt);
    }

    /* renamed from: a */
    private void m2040a(ContentImporterExporter zn) {
        bFf().mo5608dq().mo3197f(cLt, zn);
    }

    /* renamed from: a */
    private void m2041a(StatsManager abk) {
        bFf().mo5608dq().mo3197f(cLc, abk);
    }

    @C0064Am(aul = "f6b6ab843d3c03e4bf502708f1310dbf", aum = 0)
    @C5566aOg
    /* renamed from: a */
    private void m2042a(User adk) {
        throw new aWi(new aCE(this, cMI, new Object[]{adk}));
    }

    /* renamed from: a */
    private void m2044a(TemporaryFeatureTest aiq) {
        bFf().mo5608dq().mo3197f(cLp, aiq);
    }

    /* renamed from: a */
    private void m2046a(PlayerFinder ala) {
        bFf().mo5608dq().mo3197f(cLz, ala);
    }

    /* renamed from: a */
    private void m2047a(SupportOuterface alx) {
        bFf().mo5608dq().mo3197f(cLa, alx);
    }

    /* renamed from: a */
    private void m2048a(PlayerReservedNames amr) {
        bFf().mo5608dq().mo3197f(cLN, amr);
    }

    /* renamed from: a */
    private void m2051a(OffloadUtils aqh) {
        bFf().mo5608dq().mo3197f(cMd, aqh);
    }

    /* renamed from: a */
    private void m2052a(BotUtils arf) {
        bFf().mo5608dq().mo3197f(cMf, arf);
    }

    /* renamed from: a */
    private void m2053a(ClientUtils atp) {
        bFf().mo5608dq().mo3197f(cMb, atp);
    }

    @C3248pc(aYR = C3248pc.C3250b.ADDER, displayName = "AmmoBoxTypes")
    @C0064Am(aul = "0de9b4130ca894d3ab446879093a2058", aum = 0)
    @C5566aOg
    /* renamed from: a */
    private void m2054a(ClipBoxType aty) {
        throw new aWi(new aCE(this, cNh, new Object[]{aty}));
    }

    @C3248pc(aYR = C3248pc.C3250b.ADDER, displayName = "Mission Templates")
    @C0064Am(aul = "f176258c704d0588381af8f252f53fa6", aum = 0)
    @C5566aOg
    /* renamed from: a */
    private void m2056a(MissionTemplate avh) {
        throw new aWi(new aCE(this, cMS, new Object[]{avh}));
    }

    /* renamed from: a */
    private void m2057a(Insurer avj) {
        bFf().mo5608dq().mo3197f(cKU, avj);
    }

    /* renamed from: a */
    private void m2058a(UsersQueueManager aaj) {
        bFf().mo5608dq().mo3197f(cLV, aaj);
    }

    /* renamed from: a */
    private void m2059a(TemporaryFeatureBlock aaz) {
        bFf().mo5608dq().mo3197f(cLn, aaz);
    }

    /* renamed from: a */
    private void m2060a(ItemBilling acVar) {
        bFf().mo5608dq().mo3197f(cLr, acVar);
    }

    /* renamed from: a */
    private void m2061a(Bank acr) {
        bFf().mo5608dq().mo3197f(cKM, acr);
    }

    @C3248pc(aYR = C3248pc.C3250b.ADDER, displayName = "Citizenship Packs")
    @C0064Am(aul = "77165ffbcd699325dd616ebf41f7b122", aum = 0)
    @C5566aOg
    /* renamed from: a */
    private void m2062a(CitizenshipPack acu) {
        throw new aWi(new aCE(this, cPu, new Object[]{acu}));
    }

    @C3248pc(aYR = C3248pc.C3250b.ADDER, displayName = "Taikopedia Entries")
    @C0064Am(aul = "6361783c8d6b70f95ed784ef70a492b4", aum = 0)
    @C5566aOg
    /* renamed from: a */
    private void m2063a(TaikopediaEntry aiz) {
        throw new aWi(new aCE(this, aMl, new Object[]{aiz}));
    }

    @C3248pc(aYR = C3248pc.C3250b.ADDER, displayName = "Tutorials")
    @C0064Am(aul = "91fcb11d02fbc0fa58a558e4c9c47e35", aum = 0)
    @C5566aOg
    /* renamed from: a */
    private void m2064a(Tutorial ajv) {
        throw new aWi(new aCE(this, cOk, new Object[]{ajv}));
    }

    @C0064Am(aul = "bf0664fb210e4b80cc58d622cf907e87", aum = 0)
    @C5566aOg
    @C2499fr
    /* renamed from: a */
    private void m2065a(Player aku, ShipType ng, WeaponType apt) {
        throw new aWi(new aCE(this, cOM, new Object[]{aku, ng, apt}));
    }

    @C5566aOg
    @C0064Am(aul = "0786c335b725542958ee843b411170f6", aum = 0)
    @C2499fr
    /* renamed from: a */
    private void m2066a(Player aku, ShipType ng, List<WeaponType> list) {
        throw new aWi(new aCE(this, cON, new Object[]{aku, ng, list}));
    }

    /* renamed from: a */
    private void m2069a(Progression aqd) {
        bFf().mo5608dq().mo3197f(cKE, aqd);
    }

    /* renamed from: a */
    private void m2071a(AdvertiseViewTable axa) {
        bFf().mo5608dq().mo3197f(cLg, axa);
    }

    /* renamed from: a */
    private void m2072a(TaikodomDefaultContents gVVar) {
        bFf().mo5608dq().mo3197f(chW, gVVar);
    }

    @C3248pc(aYR = C3248pc.C3250b.ADDER, displayName = "EquippedShipTypes")
    @C0064Am(aul = "609d7cb70853725bb9d437e0f6cbdfc8", aum = 0)
    @C5566aOg
    /* renamed from: a */
    private void m2073a(EquippedShipType gfVar) {
        throw new aWi(new aCE(this, cMV, new Object[]{gfVar}));
    }

    /* renamed from: a */
    private void m2074a(ContractBoard hCVar) {
        bFf().mo5608dq().mo3197f(cKG, hCVar);
    }

    /* renamed from: a */
    private void m2075a(Backdoor kDVar) {
        bFf().mo5608dq().mo3197f(cKW, kDVar);
    }

    @C3248pc(aYR = C3248pc.C3250b.ADDER, displayName = "Citizenship Reward")
    @C0064Am(aul = "430b1c4734f8cf9416855e08cc5ddd4c", aum = 0)
    @C5566aOg
    /* renamed from: a */
    private void m2076a(CitizenshipReward lQVar) {
        throw new aWi(new aCE(this, cPx, new Object[]{lQVar}));
    }

    /* renamed from: a */
    private void m2077a(GlobalEconomyInfo mKVar) {
        bFf().mo5608dq().mo3197f(cLH, mKVar);
    }

    @C3248pc(aYR = C3248pc.C3250b.ADDER, displayName = "Asteroid Zone Categories")
    @C0064Am(aul = "997cf89e0df74eae6339586bebb771ec", aum = 0)
    @C5566aOg
    /* renamed from: a */
    private void m2078a(AsteroidZoneCategory nWVar) {
        throw new aWi(new aCE(this, cPD, new Object[]{nWVar}));
    }

    /* renamed from: a */
    private void m2079a(DebugFlags thVar) {
        bFf().mo5608dq().mo3197f(cLZ, thVar);
    }

    /* renamed from: a */
    private void m2080a(GlobalPhysicsTweaks zdVar) {
        bFf().mo5608dq().mo3197f(cLJ, zdVar);
    }

    /* renamed from: a */
    private void m2081a(UUID uuid) {
        bFf().mo5608dq().mo3197f(f407bL, uuid);
    }

    @C0064Am(aul = "4266ade80491a775076264b826327df4", aum = 0)
    @C5566aOg
    /* renamed from: a */
    private boolean m2082a(User adk, String str) {
        throw new aWi(new aCE(this, cQs, new Object[]{adk, str}));
    }

    /* renamed from: aA */
    private void m2083aA(C3438ra raVar) {
        bFf().mo5608dq().mo3197f(cJL, raVar);
    }

    /* renamed from: aB */
    private void m2084aB(C3438ra raVar) {
        bFf().mo5608dq().mo3197f(cJN, raVar);
    }

    /* renamed from: aC */
    private void m2085aC(C3438ra raVar) {
        bFf().mo5608dq().mo3197f(cJP, raVar);
    }

    /* renamed from: aD */
    private void m2086aD(C3438ra raVar) {
        bFf().mo5608dq().mo3197f(cJR, raVar);
    }

    /* renamed from: aE */
    private void m2087aE(C3438ra raVar) {
        bFf().mo5608dq().mo3197f(cJT, raVar);
    }

    /* renamed from: aF */
    private void m2088aF(C3438ra raVar) {
        bFf().mo5608dq().mo3197f(cJV, raVar);
    }

    /* renamed from: aG */
    private void m2090aG(C3438ra raVar) {
        bFf().mo5608dq().mo3197f(cJX, raVar);
    }

    /* renamed from: aH */
    private void m2091aH(C3438ra raVar) {
        bFf().mo5608dq().mo3197f(cJZ, raVar);
    }

    private C3438ra aHA() {
        return (C3438ra) bFf().mo5608dq().mo3214p(cJF);
    }

    private C3438ra aHB() {
        return (C3438ra) bFf().mo5608dq().mo3214p(cJH);
    }

    private C3438ra aHC() {
        return (C3438ra) bFf().mo5608dq().mo3214p(cJJ);
    }

    private C3438ra aHD() {
        return (C3438ra) bFf().mo5608dq().mo3214p(cJL);
    }

    private C3438ra aHE() {
        return (C3438ra) bFf().mo5608dq().mo3214p(cJN);
    }

    private C3438ra aHF() {
        return (C3438ra) bFf().mo5608dq().mo3214p(cJP);
    }

    private C3438ra aHG() {
        return (C3438ra) bFf().mo5608dq().mo3214p(cJR);
    }

    private C3438ra aHH() {
        return (C3438ra) bFf().mo5608dq().mo3214p(cJT);
    }

    private C3438ra aHI() {
        return (C3438ra) bFf().mo5608dq().mo3214p(cJV);
    }

    private C3438ra aHJ() {
        return (C3438ra) bFf().mo5608dq().mo3214p(cJX);
    }

    private C3438ra aHK() {
        return (C3438ra) bFf().mo5608dq().mo3214p(cJZ);
    }

    private C3438ra aHL() {
        return (C3438ra) bFf().mo5608dq().mo3214p(cKb);
    }

    private C3438ra aHM() {
        return (C3438ra) bFf().mo5608dq().mo3214p(cKd);
    }

    private C3438ra aHN() {
        return (C3438ra) bFf().mo5608dq().mo3214p(cKf);
    }

    private C3438ra aHO() {
        return (C3438ra) bFf().mo5608dq().mo3214p(cKh);
    }

    private C3438ra aHP() {
        return (C3438ra) bFf().mo5608dq().mo3214p(cKj);
    }

    private C3438ra aHQ() {
        return (C3438ra) bFf().mo5608dq().mo3214p(cKl);
    }

    private C2686iZ aHR() {
        return (C2686iZ) bFf().mo5608dq().mo3214p(cKn);
    }

    private C2686iZ aHS() {
        return (C2686iZ) bFf().mo5608dq().mo3214p(cKp);
    }

    private C3438ra aHT() {
        return (C3438ra) bFf().mo5608dq().mo3214p(cKr);
    }

    private C3438ra aHU() {
        return (C3438ra) bFf().mo5608dq().mo3214p(cKt);
    }

    private C3438ra aHV() {
        return (C3438ra) bFf().mo5608dq().mo3214p(cKv);
    }

    private C3438ra aHW() {
        return (C3438ra) bFf().mo5608dq().mo3214p(cKx);
    }

    private C3438ra aHX() {
        return (C3438ra) bFf().mo5608dq().mo3214p(cKz);
    }

    private C3438ra aHY() {
        return (C3438ra) bFf().mo5608dq().mo3214p(cKB);
    }

    private C3438ra aHZ() {
        return (C3438ra) bFf().mo5608dq().mo3214p(f397Ml);
    }

    private I18NString aHk() {
        return (I18NString) bFf().mo5608dq().mo3214p(cIZ);
    }

    private C3438ra aHl() {
        return (C3438ra) bFf().mo5608dq().mo3214p(cJb);
    }

    private C2686iZ aHm() {
        return (C2686iZ) bFf().mo5608dq().mo3214p(cJd);
    }

    private C3438ra aHn() {
        return (C3438ra) bFf().mo5608dq().mo3214p(cJf);
    }

    private C3438ra aHo() {
        return (C3438ra) bFf().mo5608dq().mo3214p(cJh);
    }

    private C3438ra aHp() {
        return (C3438ra) bFf().mo5608dq().mo3214p(cJj);
    }

    private C3438ra aHq() {
        return (C3438ra) bFf().mo5608dq().mo3214p(cJl);
    }

    private C3438ra aHr() {
        return (C3438ra) bFf().mo5608dq().mo3214p(cJn);
    }

    private C3438ra aHs() {
        return (C3438ra) bFf().mo5608dq().mo3214p(cJp);
    }

    private C3438ra aHt() {
        return (C3438ra) bFf().mo5608dq().mo3214p(cJr);
    }

    private C3438ra aHu() {
        return (C3438ra) bFf().mo5608dq().mo3214p(cJt);
    }

    private C3438ra aHv() {
        return (C3438ra) bFf().mo5608dq().mo3214p(cJv);
    }

    private C3438ra aHw() {
        return (C3438ra) bFf().mo5608dq().mo3214p(cJx);
    }

    private C3438ra aHx() {
        return (C3438ra) bFf().mo5608dq().mo3214p(cJz);
    }

    private C3438ra aHy() {
        return (C3438ra) bFf().mo5608dq().mo3214p(cJB);
    }

    private C3438ra aHz() {
        return (C3438ra) bFf().mo5608dq().mo3214p(cJD);
    }

    /* renamed from: aI */
    private void m2092aI(C3438ra raVar) {
        bFf().mo5608dq().mo3197f(cKb, raVar);
    }

    private C0694Ju aIA() {
        return (C0694Ju) bFf().mo5608dq().mo3214p(cLD);
    }

    private Market aIB() {
        return (Market) bFf().mo5608dq().mo3214p(cLF);
    }

    private GlobalEconomyInfo aIC() {
        return (GlobalEconomyInfo) bFf().mo5608dq().mo3214p(cLH);
    }

    private GlobalPhysicsTweaks aID() {
        return (GlobalPhysicsTweaks) bFf().mo5608dq().mo3214p(cLJ);
    }

    private C3438ra aIE() {
        return (C3438ra) bFf().mo5608dq().mo3214p(cLL);
    }

    private PlayerReservedNames aIF() {
        return (PlayerReservedNames) bFf().mo5608dq().mo3214p(cLN);
    }

    private C2686iZ aIG() {
        return (C2686iZ) bFf().mo5608dq().mo3214p(cLP);
    }

    private C2686iZ aIH() {
        return (C2686iZ) bFf().mo5608dq().mo3214p(cLR);
    }

    private C1556Wo aII() {
        return (C1556Wo) bFf().mo5608dq().mo3214p(cLT);
    }

    private UsersQueueManager aIJ() {
        return (UsersQueueManager) bFf().mo5608dq().mo3214p(cLV);
    }

    private PingScript aIK() {
        return (PingScript) bFf().mo5608dq().mo3214p(cLX);
    }

    private DebugFlags aIL() {
        return (DebugFlags) bFf().mo5608dq().mo3214p(cLZ);
    }

    private ClientUtils aIM() {
        return (ClientUtils) bFf().mo5608dq().mo3214p(cMb);
    }

    private OffloadUtils aIN() {
        return (OffloadUtils) bFf().mo5608dq().mo3214p(cMd);
    }

    private BotUtils aIO() {
        return (BotUtils) bFf().mo5608dq().mo3214p(cMf);
    }

    private final String aIQ() {
        switch (bFf().mo6893i(cMh)) {
            case 0:
                return null;
            case 2:
                return (String) bFf().mo5606d(new aCE(this, cMh, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, cMh, new Object[0]));
                break;
        }
        return aIP();
    }

    private Progression aIa() {
        return (Progression) bFf().mo5608dq().mo3214p(cKE);
    }

    private ContractBoard aIb() {
        return (ContractBoard) bFf().mo5608dq().mo3214p(cKG);
    }

    private NLSManager aIc() {
        return (NLSManager) bFf().mo5608dq().mo3214p(cKI);
    }

    private AvatarManager aId() {
        return (AvatarManager) bFf().mo5608dq().mo3214p(cKK);
    }

    private Bank aIe() {
        return (Bank) bFf().mo5608dq().mo3214p(cKM);
    }

    private Bank aIf() {
        return (Bank) bFf().mo5608dq().mo3214p(cKO);
    }

    private SpaceZoneTypesManager aIg() {
        return (SpaceZoneTypesManager) bFf().mo5608dq().mo3214p(cKQ);
    }

    private CitizenshipOffice aIh() {
        return (CitizenshipOffice) bFf().mo5608dq().mo3214p(cKS);
    }

    private Insurer aIi() {
        return (Insurer) bFf().mo5608dq().mo3214p(cKU);
    }

    private Backdoor aIj() {
        return (Backdoor) bFf().mo5608dq().mo3214p(cKW);
    }

    private AssetGroup aIk() {
        return (AssetGroup) bFf().mo5608dq().mo3214p(cKY);
    }

    private SupportOuterface aIl() {
        return (SupportOuterface) bFf().mo5608dq().mo3214p(cLa);
    }

    private StatsManager aIm() {
        return (StatsManager) bFf().mo5608dq().mo3214p(cLc);
    }

    private MarkManager aIn() {
        return (MarkManager) bFf().mo5608dq().mo3214p(cLe);
    }

    private AdvertiseViewTable aIo() {
        return (AdvertiseViewTable) bFf().mo5608dq().mo3214p(cLg);
    }

    private NPCType aIp() {
        return (NPCType) bFf().mo5608dq().mo3214p(cLi);
    }

    private ServerEditorUtility aIq() {
        return (ServerEditorUtility) bFf().mo5608dq().mo3214p(cLk);
    }

    private TaikodomDefaultContents aIr() {
        return (TaikodomDefaultContents) bFf().mo5608dq().mo3214p(chW);
    }

    private TemporaryFeatureBlock aIs() {
        return (TemporaryFeatureBlock) bFf().mo5608dq().mo3214p(cLn);
    }

    private TemporaryFeatureTest aIt() {
        return (TemporaryFeatureTest) bFf().mo5608dq().mo3214p(cLp);
    }

    private ItemBilling aIu() {
        return (ItemBilling) bFf().mo5608dq().mo3214p(cLr);
    }

    private ContentImporterExporter aIv() {
        return (ContentImporterExporter) bFf().mo5608dq().mo3214p(cLt);
    }

    private C3438ra aIw() {
        return (C3438ra) bFf().mo5608dq().mo3214p(cLv);
    }

    private CorporationReservedNames aIx() {
        return (CorporationReservedNames) bFf().mo5608dq().mo3214p(cLx);
    }

    private PlayerFinder aIy() {
        return (PlayerFinder) bFf().mo5608dq().mo3214p(cLz);
    }

    private C1077Pl aIz() {
        return (C1077Pl) bFf().mo5608dq().mo3214p(cLB);
    }

    /* renamed from: aJ */
    private void m2093aJ(C3438ra raVar) {
        bFf().mo5608dq().mo3197f(cKd, raVar);
    }

    @C0064Am(aul = "dabf2df70f027861d9528e2383f53c6d", aum = 0)
    @C5566aOg
    private void aJF() {
        throw new aWi(new aCE(this, cMC, new Object[0]));
    }

    @C5566aOg
    private void aJG() {
        switch (bFf().mo6893i(cMC)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, cMC, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, cMC, new Object[0]));
                break;
        }
        aJF();
    }

    @C0064Am(aul = "211afaf2542d4cfa5c8317e635adc11a", aum = 0)
    @C5566aOg
    private List<UserConnection> aJJ() {
        throw new aWi(new aCE(this, cMF, new Object[0]));
    }

    @C0064Am(aul = "b70d7846734bbaec72f4edb18527a8e0", aum = 0)
    @C5566aOg
    private List<Player> aJL() {
        throw new aWi(new aCE(this, cMG, new Object[0]));
    }

    @C0064Am(aul = "6131b837d395d227a8cee234d417112e", aum = 0)
    @C5566aOg
    private Collection<User> aJP() {
        throw new aWi(new aCE(this, cML, new Object[0]));
    }

    @C5566aOg
    @C3248pc(aYR = C3248pc.C3250b.ITERATOR, displayName = "AIClasses")
    @C0064Am(aul = "8655e09ed64108d7a8e3e3046da8d320", aum = 0)
    @C2499fr
    private List<ClassContainer> aJR() {
        throw new aWi(new aCE(this, cMR, new Object[0]));
    }

    /* renamed from: aK */
    private void m2094aK(C3438ra raVar) {
        bFf().mo5608dq().mo3197f(cKf, raVar);
    }

    @C5566aOg
    @C3248pc(aYR = C3248pc.C3250b.ITERATOR, displayName = "ShipTypes")
    @C0064Am(aul = "fbcf11075418da1cdfbe3500eae8f084", aum = 0)
    @C2499fr
    private List<ShipType> aKB() {
        throw new aWi(new aCE(this, cNT, new Object[0]));
    }

    @C0064Am(aul = "6e104062ae6bceb1ef6bfcbacd3722b9", aum = 0)
    @C5566aOg
    @C2499fr
    private BotScript aKR() {
        throw new aWi(new aCE(this, cOw, new Object[0]));
    }

    /* renamed from: aL */
    private void m2095aL(C3438ra raVar) {
        bFf().mo5608dq().mo3197f(cKh, raVar);
    }

    @C0064Am(aul = "dbe55392c624bd260aa4dfed9a793c74", aum = 0)
    @C5566aOg
    @C2499fr
    private void aLL() {
        throw new aWi(new aCE(this, cPP, new Object[0]));
    }

    @C5566aOg
    @C2499fr
    private void aLM() {
        switch (bFf().mo6893i(cPP)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, cPP, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, cPP, new Object[0]));
                break;
        }
        aLL();
    }

    /* renamed from: aM */
    private void m2096aM(C3438ra raVar) {
        bFf().mo5608dq().mo3197f(cKj, raVar);
    }

    @C0064Am(aul = "6badb8c0a6aa6cd06afb57f769427d44", aum = 0)
    @C5566aOg
    private List<User> aMD() {
        throw new aWi(new aCE(this, cQt, new Object[0]));
    }

    @C0064Am(aul = "e8f30bd772a9bd83a668c8835170c1db", aum = 0)
    @C5566aOg
    private int aMd() {
        throw new aWi(new aCE(this, cPZ, new Object[0]));
    }

    @C0064Am(aul = "42bd8741ed9af1f8a2625de5ed3dad5e", aum = 0)
    @C5566aOg
    private void aMf() {
        throw new aWi(new aCE(this, cQa, new Object[0]));
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Economy")
    @C0064Am(aul = "8583c4005994c581e4aee58a8cf91ed4", aum = 0)
    @C5566aOg
    private GlobalEconomyInfo aMg() {
        throw new aWi(new aCE(this, cQc, new Object[0]));
    }

    @C0064Am(aul = "7ebec3e9cd29fd9a202408e9a4e85641", aum = 0)
    @C5566aOg
    @C2085br
    private void aMq() {
        throw new aWi(new aCE(this, cQk, new Object[0]));
    }

    private Taikodom aMt() {
        switch (bFf().mo6893i(cQl)) {
            case 0:
                return null;
            case 2:
                return (Taikodom) bFf().mo5606d(new aCE(this, cQl, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, cQl, new Object[0]));
                break;
        }
        return aMs();
    }

    /* renamed from: aN */
    private void m2097aN(C3438ra raVar) {
        bFf().mo5608dq().mo3197f(cKl, raVar);
    }

    /* renamed from: aO */
    private void m2098aO(C3438ra raVar) {
        bFf().mo5608dq().mo3197f(cKr, raVar);
    }

    /* renamed from: aP */
    private void m2099aP(C3438ra raVar) {
        bFf().mo5608dq().mo3197f(cKt, raVar);
    }

    /* renamed from: aQ */
    private void m2100aQ(C3438ra raVar) {
        bFf().mo5608dq().mo3197f(cKv, raVar);
    }

    /* renamed from: aR */
    private void m2101aR(C3438ra raVar) {
        bFf().mo5608dq().mo3197f(cKx, raVar);
    }

    /* renamed from: aS */
    private void m2102aS(C3438ra raVar) {
        bFf().mo5608dq().mo3197f(cKz, raVar);
    }

    /* renamed from: aT */
    private void m2103aT(C3438ra raVar) {
        bFf().mo5608dq().mo3197f(cKB, raVar);
    }

    /* renamed from: aU */
    private void m2104aU(C3438ra raVar) {
        bFf().mo5608dq().mo3197f(f397Ml, raVar);
    }

    /* renamed from: aV */
    private void m2105aV(C3438ra raVar) {
        bFf().mo5608dq().mo3197f(cLv, raVar);
    }

    /* renamed from: aW */
    private void m2106aW(C3438ra raVar) {
        bFf().mo5608dq().mo3197f(cLL, raVar);
    }

    private C3438ra afb() {
        return (C3438ra) bFf().mo5608dq().mo3214p(bok);
    }

    /* renamed from: aj */
    private void m2107aj(C3438ra raVar) {
        bFf().mo5608dq().mo3197f(cJb, raVar);
    }

    /* renamed from: ak */
    private void m2108ak(C3438ra raVar) {
        bFf().mo5608dq().mo3197f(cJf, raVar);
    }

    /* renamed from: al */
    private void m2109al(C3438ra raVar) {
        bFf().mo5608dq().mo3197f(cJh, raVar);
    }

    private C3438ra alN() {
        return (C3438ra) bFf().mo5608dq().mo3214p(bCb);
    }

    /* renamed from: am */
    private void m2110am(C3438ra raVar) {
        bFf().mo5608dq().mo3197f(cJj, raVar);
    }

    /* renamed from: an */
    private UUID m2111an() {
        return (UUID) bFf().mo5608dq().mo3214p(f407bL);
    }

    /* renamed from: an */
    private void m2112an(C3438ra raVar) {
        bFf().mo5608dq().mo3197f(cJl, raVar);
    }

    /* renamed from: ao */
    private void m2113ao(C3438ra raVar) {
        bFf().mo5608dq().mo3197f(cJn, raVar);
    }

    /* renamed from: ap */
    private void m2115ap(C3438ra raVar) {
        bFf().mo5608dq().mo3197f(cJp, raVar);
    }

    /* renamed from: aq */
    private void m2116aq(C3438ra raVar) {
        bFf().mo5608dq().mo3197f(cJr, raVar);
    }

    /* renamed from: ar */
    private void m2118ar(C3438ra raVar) {
        bFf().mo5608dq().mo3197f(cJt, raVar);
    }

    /* renamed from: as */
    private void m2119as(C3438ra raVar) {
        bFf().mo5608dq().mo3197f(cJv, raVar);
    }

    /* renamed from: at */
    private void m2120at(C3438ra raVar) {
        bFf().mo5608dq().mo3197f(cJx, raVar);
    }

    /* renamed from: au */
    private void m2122au(C3438ra raVar) {
        bFf().mo5608dq().mo3197f(cJz, raVar);
    }

    /* renamed from: av */
    private void m2123av(C3438ra raVar) {
        bFf().mo5608dq().mo3197f(cJB, raVar);
    }

    /* renamed from: aw */
    private void m2124aw(C3438ra raVar) {
        bFf().mo5608dq().mo3197f(cJD, raVar);
    }

    /* renamed from: ax */
    private void m2125ax(C3438ra raVar) {
        bFf().mo5608dq().mo3197f(cJF, raVar);
    }

    /* renamed from: ay */
    private void m2126ay(C3438ra raVar) {
        bFf().mo5608dq().mo3197f(cJH, raVar);
    }

    /* renamed from: az */
    private void m2127az(C3438ra raVar) {
        bFf().mo5608dq().mo3197f(cJJ, raVar);
    }

    /* renamed from: b */
    private void m2129b(long j, String str, long j2, int i) {
        switch (bFf().mo6893i(cOt)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, cOt, new Object[]{new Long(j), str, new Long(j2), new Integer(i)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, cOt, new Object[]{new Long(j), str, new Long(j2), new Integer(i)}));
                break;
        }
        m2023a(j, str, j2, i);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Avatar Manager")
    @C0064Am(aul = "674c4dd9ce05df715e5b89fa48ce1b85", aum = 0)
    @C5566aOg
    /* renamed from: b */
    private void m2130b(AvatarManager dq) {
        throw new aWi(new aCE(this, cOG, new Object[]{dq}));
    }

    @C3248pc(aYR = C3248pc.C3250b.ADDER, displayName = "Citizenship Improvements Types")
    @C0064Am(aul = "96aa00b0720a092d293039ecf34039bf", aum = 0)
    @C5566aOg
    /* renamed from: b */
    private void m2131b(CitizenImprovementType kt) {
        throw new aWi(new aCE(this, cPJ, new Object[]{kt}));
    }

    @C3248pc(aYR = C3248pc.C3250b.ADDER, displayName = "Cruise Speed Types")
    @C0064Am(aul = "88de604c1d3bd8a6e09949426841b7a5", aum = 0)
    @C5566aOg
    /* renamed from: b */
    private void m2132b(CruiseSpeedType nf) {
        throw new aWi(new aCE(this, cPo, new Object[]{nf}));
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Market")
    @C0064Am(aul = "b1a2de9085dcd218bcb64420c705f26f", aum = 0)
    @C5566aOg
    /* renamed from: b */
    private void m2133b(Market oi) {
        throw new aWi(new aCE(this, cQb, new Object[]{oi}));
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Citizenship Office")
    @C0064Am(aul = "bb54c80318f55c7550c2d8ed3dfe02aa", aum = 0)
    @C5566aOg
    /* renamed from: b */
    private void m2134b(CitizenshipOffice sk) {
        throw new aWi(new aCE(this, cQo, new Object[]{sk}));
    }

    @C3248pc(aYR = C3248pc.C3250b.ADDER, displayName = "Warehouses")
    @C0064Am(aul = "7420b4388b17b8bc8d5464dfc87cfc89", aum = 0)
    @C5566aOg
    /* renamed from: b */
    private void m2136b(VirtualWarehouse aor) {
        throw new aWi(new aCE(this, cQe, new Object[]{aor}));
    }

    /* renamed from: b */
    private void m2137b(Bank acr) {
        bFf().mo5608dq().mo3197f(cKO, acr);
    }

    @C3248pc(aYR = C3248pc.C3250b.ADDER, displayName = "Citizenship Types")
    @C0064Am(aul = "3933dfc5449b049f31bfeb90f2d9ea9f", aum = 0)
    @C5566aOg
    /* renamed from: b */
    private void m2138b(CitizenshipType adl) {
        throw new aWi(new aCE(this, cPr, new Object[]{adl}));
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "ContractBoard")
    @C0064Am(aul = "e4081ed8e6a99f21836dfc096255a7f3", aum = 0)
    @C5566aOg
    /* renamed from: b */
    private void m2140b(ContractBoard hCVar) {
        throw new aWi(new aCE(this, cPR, new Object[]{hCVar}));
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Economy")
    @C0064Am(aul = "5afb88cee7105ab604d5609ea483d841", aum = 0)
    @C5566aOg
    /* renamed from: b */
    private void m2141b(GlobalEconomyInfo mKVar) {
        throw new aWi(new aCE(this, cQd, new Object[]{mKVar}));
    }

    /* renamed from: b */
    private void m2142b(PingScript rWVar) {
        bFf().mo5608dq().mo3197f(cLX, rWVar);
    }

    @C3248pc(aYR = C3248pc.C3250b.ADDER, displayName = "Collision Sets")
    @C0064Am(aul = "d296bead2dfb4af60e1bbf7c4a6350cd", aum = 0)
    @C5566aOg
    /* renamed from: b */
    private void m2144b(CollisionFXSet yaVar) {
        throw new aWi(new aCE(this, cOI, new Object[]{yaVar}));
    }

    @C0064Am(aul = "34314ead705bf4abde12b122855944ee", aum = 0)
    @C5566aOg
    @C0909NL
    /* renamed from: c */
    private User m2146c(String str, String str2, String str3) {
        throw new aWi(new aCE(this, cMP, new Object[]{str, str2, str3}));
    }

    @C0064Am(aul = "76a975961c3dfa871fbf85d0927dceed", aum = 0)
    @C5566aOg
    /* renamed from: c */
    private void m2152c(User adk) {
        throw new aWi(new aCE(this, cMJ, new Object[]{adk}));
    }

    @C3248pc(aYR = C3248pc.C3250b.REMOVER, displayName = "AmmoBoxTypes")
    @C0064Am(aul = "9503a4f201342dba9a132afef8b75015", aum = 0)
    @C5566aOg
    /* renamed from: c */
    private void m2158c(ClipBoxType aty) {
        throw new aWi(new aCE(this, cNi, new Object[]{aty}));
    }

    @C3248pc(aYR = C3248pc.C3250b.REMOVER, displayName = "Mission Templates")
    @C0064Am(aul = "05b3a51760eee99ae4cee39d11cfd2c8", aum = 0)
    @C5566aOg
    /* renamed from: c */
    private void m2160c(MissionTemplate avh) {
        throw new aWi(new aCE(this, cMT, new Object[]{avh}));
    }

    @C3248pc(aYR = C3248pc.C3250b.REMOVER, displayName = "Citizenship Packs")
    @C0064Am(aul = "7c244eee9db2d5af490870ae756be07a", aum = 0)
    @C5566aOg
    /* renamed from: c */
    private void m2161c(CitizenshipPack acu) {
        throw new aWi(new aCE(this, cPv, new Object[]{acu}));
    }

    @C3248pc(aYR = C3248pc.C3250b.REMOVER, displayName = "Taikopedia Entries")
    @C0064Am(aul = "1cfbd9e98fcbcb7c50ce019246630f37", aum = 0)
    @C5566aOg
    /* renamed from: c */
    private void m2162c(TaikopediaEntry aiz) {
        throw new aWi(new aCE(this, aMm, new Object[]{aiz}));
    }

    @C3248pc(aYR = C3248pc.C3250b.REMOVER, displayName = "Tutorials")
    @C0064Am(aul = "a2b697a1f86008a7f8f2460ba25094da", aum = 0)
    @C5566aOg
    /* renamed from: c */
    private void m2163c(Tutorial ajv) {
        throw new aWi(new aCE(this, cOl, new Object[]{ajv}));
    }

    @C3248pc(aYR = C3248pc.C3250b.REMOVER, displayName = "EquippedShipTypes")
    @C0064Am(aul = "997423d5bbc21ccde07edf072e5f0b57", aum = 0)
    @C5566aOg
    /* renamed from: c */
    private void m2166c(EquippedShipType gfVar) {
        throw new aWi(new aCE(this, cMW, new Object[]{gfVar}));
    }

    @C3248pc(aYR = C3248pc.C3250b.REMOVER, displayName = "Citizenship Reward")
    @C0064Am(aul = "d8e5dd5809b21e47939e523073ef4f0e", aum = 0)
    @C5566aOg
    /* renamed from: c */
    private void m2167c(CitizenshipReward lQVar) {
        throw new aWi(new aCE(this, cPy, new Object[]{lQVar}));
    }

    @C3248pc(aYR = C3248pc.C3250b.REMOVER, displayName = "Asteroid Zone Categories")
    @C0064Am(aul = "c3f2152acfcb838349ce0986b5db846b", aum = 0)
    @C5566aOg
    /* renamed from: c */
    private void m2168c(AsteroidZoneCategory nWVar) {
        throw new aWi(new aCE(this, cPE, new Object[]{nWVar}));
    }

    /* renamed from: c */
    private void m2169c(UUID uuid) {
        switch (bFf().mo6893i(f409bO)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f409bO, new Object[]{uuid}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f409bO, new Object[]{uuid}));
                break;
        }
        m2145b(uuid);
    }

    @C3248pc(aYR = C3248pc.C3250b.ADDER, displayName = "Space Categories")
    @C0064Am(aul = "d5675d6929bea2bc551b94d84babc402", aum = 0)
    @C5566aOg
    /* renamed from: d */
    private void m2171d(SpaceCategory ak) {
        throw new aWi(new aCE(this, cPG, new Object[]{ak}));
    }

    @C3248pc(aYR = C3248pc.C3250b.REMOVER, displayName = "Citizenship Improvements Types")
    @C0064Am(aul = "377f46fe0a77057c479943439dffc696", aum = 0)
    @C5566aOg
    /* renamed from: d */
    private void m2172d(CitizenImprovementType kt) {
        throw new aWi(new aCE(this, cPK, new Object[]{kt}));
    }

    @C3248pc(aYR = C3248pc.C3250b.REMOVER, displayName = "Cruise Speed Types")
    @C0064Am(aul = "f67639437824954b9c7649b88c33a049", aum = 0)
    @C5566aOg
    /* renamed from: d */
    private void m2173d(CruiseSpeedType nf) {
        throw new aWi(new aCE(this, cPp, new Object[]{nf}));
    }

    @C3248pc(aYR = C3248pc.C3250b.REMOVER, displayName = "Warehouses")
    @C0064Am(aul = "962ba4f4f2b7066ec13ccb3cbfb4a6a8", aum = 0)
    @C5566aOg
    /* renamed from: d */
    private void m2175d(VirtualWarehouse aor) {
        throw new aWi(new aCE(this, cQf, new Object[]{aor}));
    }

    @C3248pc(aYR = C3248pc.C3250b.REMOVER, displayName = "Citizenship Types")
    @C0064Am(aul = "681aa3d1268022519bada998b8155dd8", aum = 0)
    @C5566aOg
    /* renamed from: d */
    private void m2178d(CitizenshipType adl) {
        throw new aWi(new aCE(this, cPs, new Object[]{adl}));
    }

    @C3248pc(aYR = C3248pc.C3250b.ADDER, displayName = "Database Categories")
    @C0064Am(aul = "c8afaf0554c32dfbcc544af811a3811f", aum = 0)
    @C5566aOg
    /* renamed from: d */
    private void m2179d(DatabaseCategory aik) {
        throw new aWi(new aCE(this, cPA, new Object[]{aik}));
    }

    @C3248pc(aYR = C3248pc.C3250b.ADDER, displayName = "TurretTypes")
    @C0064Am(aul = "616b5eb91f76dfff5beb294e8d426d67", aum = 0)
    @C5566aOg
    /* renamed from: d */
    private void m2180d(C6544aow aow) {
        throw new aWi(new aCE(this, cOW, new Object[]{aow}));
    }

    @C3248pc(aYR = C3248pc.C3250b.REMOVER, displayName = "Collision Sets")
    @C0064Am(aul = "0a0b7f0b72ec3bd59d98fc1e58ab4ce3", aum = 0)
    @C5566aOg
    /* renamed from: d */
    private void m2183d(CollisionFXSet yaVar) {
        throw new aWi(new aCE(this, cOJ, new Object[]{yaVar}));
    }

    @C0064Am(aul = "244eb411ac9ee28e95381ffc5418468f", aum = 0)
    @C5566aOg
    /* renamed from: e */
    private int m2184e(User adk) {
        throw new aWi(new aCE(this, cMN, new Object[]{adk}));
    }

    @C0064Am(aul = "ea5da1b25617a71c860f34c6121f7e09", aum = 0)
    @C5566aOg
    /* renamed from: e */
    private void m2187e(Corporation aPVar) {
        throw new aWi(new aCE(this, cPM, new Object[]{aPVar}));
    }

    @C3248pc(aYR = C3248pc.C3250b.ADDER, displayName = "AmmoTypes")
    @C0064Am(aul = "1dacba7065972d7b91cf09e510a6ad0c", aum = 0)
    @C5566aOg
    /* renamed from: e */
    private void m2188e(ClipType aqf) {
        throw new aWi(new aCE(this, cNe, new Object[]{aqf}));
    }

    @C0064Am(aul = "e079555fc63b2996b16dbe579902ecb5", aum = 0)
    @C5566aOg
    @C2499fr
    /* renamed from: eb */
    private Player m2192eb(String str) {
        throw new aWi(new aCE(this, cME, new Object[]{str}));
    }

    @C0064Am(aul = "23b460750c60787cbe8e4066cb778ca2", aum = 0)
    @C5566aOg
    @C2499fr
    /* renamed from: ed */
    private Player m2193ed(String str) {
        throw new aWi(new aCE(this, cMH, new Object[]{str}));
    }

    @C0064Am(aul = "f528446b8f419c6811e2735faf02ea65", aum = 0)
    @C5566aOg
    /* renamed from: ef */
    private User m2194ef(String str) {
        throw new aWi(new aCE(this, cMO, new Object[]{str}));
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Forbidden Names (comma separated)")
    @C0064Am(aul = "c55f5fad2682130e2a6d6874de8cc5b8", aum = 0)
    @C5566aOg
    /* renamed from: eh */
    private void m2195eh(String str) {
        throw new aWi(new aCE(this, cPj, new Object[]{str}));
    }

    @C0064Am(aul = "6135c924731b82b5a6215f5f6c265cda", aum = 0)
    @C5566aOg
    /* renamed from: ej */
    private DamageType m2196ej(String str) {
        throw new aWi(new aCE(this, cPn, new Object[]{str}));
    }

    @C3248pc(aYR = C3248pc.C3250b.REMOVER, displayName = "Space Categories")
    @C0064Am(aul = "10dac824a6c192150d741077b8770d46", aum = 0)
    @C5566aOg
    /* renamed from: f */
    private void m2197f(SpaceCategory ak) {
        throw new aWi(new aCE(this, cPH, new Object[]{ak}));
    }

    @C3248pc(aYR = C3248pc.C3250b.REMOVER, displayName = "Database Categories")
    @C0064Am(aul = "e06cd48a86ebf3655441f1f2afae03f2", aum = 0)
    @C5566aOg
    /* renamed from: f */
    private void m2201f(DatabaseCategory aik) {
        throw new aWi(new aCE(this, cPB, new Object[]{aik}));
    }

    @C3248pc(aYR = C3248pc.C3250b.REMOVER, displayName = "TurretTypes")
    @C0064Am(aul = "468d7a5d7f7d923769a691082e685aaa", aum = 0)
    @C5566aOg
    /* renamed from: f */
    private void m2202f(C6544aow aow) {
        throw new aWi(new aCE(this, cOX, new Object[]{aow}));
    }

    @C3248pc(aYR = C3248pc.C3250b.REMOVER, displayName = "AmmoTypes")
    @C0064Am(aul = "bd4803cddc9a37d800f24661b775bcb5", aum = 0)
    @C5566aOg
    /* renamed from: g */
    private void m2208g(ClipType aqf) {
        throw new aWi(new aCE(this, cNf, new Object[]{aqf}));
    }

    @C0064Am(aul = "25b35eaa5aa30c501eb044f18e222494", aum = 0)
    @C5566aOg
    /* renamed from: g */
    private boolean m2210g(Corporation aPVar) {
        throw new aWi(new aCE(this, cPN, new Object[]{aPVar}));
    }

    /* renamed from: gK */
    private void m2211gK(I18NString i18NString) {
        bFf().mo5608dq().mo3197f(cIZ, i18NString);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Advertise Path")
    @C0064Am(aul = "6328feee933387e4b55b4e841743a00d", aum = 0)
    @C5566aOg
    /* renamed from: gL */
    private void m2212gL(I18NString i18NString) {
        throw new aWi(new aCE(this, cOE, new Object[]{i18NString}));
    }

    @C0064Am(aul = "fd950c35eb3059cfb078cd89e9c2bc7b", aum = 0)
    @C5566aOg
    /* renamed from: gN */
    private void m2213gN(I18NString i18NString) {
        throw new aWi(new aCE(this, cOH, new Object[]{i18NString}));
    }

    /* renamed from: gR */
    private void m2215gR(int i) {
        switch (bFf().mo6893i(cOs)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, cOs, new Object[]{new Integer(i)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, cOs, new Object[]{new Integer(i)}));
                break;
        }
        m2214gQ(i);
    }

    /* renamed from: gT */
    private void m2217gT(int i) {
        switch (bFf().mo6893i(cOu)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, cOu, new Object[]{new Integer(i)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, cOu, new Object[]{new Integer(i)}));
                break;
        }
        m2216gS(i);
    }

    @C3248pc(aYR = C3248pc.C3250b.ADDER, displayName = "Factions")
    @C0064Am(aul = "a066889da3652f82a4a30b136d854225", aum = 0)
    @C5566aOg
    /* renamed from: h */
    private void m2219h(Faction xm) {
        throw new aWi(new aCE(this, aQo, new Object[]{xm}));
    }

    /* renamed from: i */
    private void m2225i(NPCType aed) {
        bFf().mo5608dq().mo3197f(cLi, aed);
    }

    @C3248pc(aYR = C3248pc.C3250b.REMOVER, displayName = "Factions")
    @C0064Am(aul = "ab1fcbb497e427d5f0cd3bd5ad231483", aum = 0)
    @C5566aOg
    /* renamed from: j */
    private void m2226j(Faction xm) {
        throw new aWi(new aCE(this, aQp, new Object[]{xm}));
    }

    @C3248pc(aYR = C3248pc.C3250b.ADDER, displayName = "NPCTypes")
    @C0064Am(aul = "67d68fe993b23c2a16e3b502f1032727", aum = 0)
    @C5566aOg
    /* renamed from: j */
    private void m2228j(NPCType aed) {
        throw new aWi(new aCE(this, cMY, new Object[]{aed}));
    }

    @C3248pc(aYR = C3248pc.C3250b.REMOVER, displayName = "NPCTypes")
    @C0064Am(aul = "6c5b6245d3c39a3c4424de8a50083b15", aum = 0)
    @C5566aOg
    /* renamed from: l */
    private void m2234l(NPCType aed) {
        throw new aWi(new aCE(this, cMZ, new Object[]{aed}));
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Mestre dos Magos")
    @C0064Am(aul = "a6292d0d53885508fa46d760fe643793", aum = 0)
    @C5566aOg
    /* renamed from: n */
    private void m2235n(NPCType aed) {
        throw new aWi(new aCE(this, cOj, new Object[]{aed}));
    }

    /* renamed from: r */
    private void m2236r(C3438ra raVar) {
        bFf().mo5608dq().mo3197f(f399OY, raVar);
    }

    /* renamed from: uQ */
    private C3438ra m2239uQ() {
        return (C3438ra) bFf().mo5608dq().mo3214p(f399OY);
    }

    @C5566aOg
    @C3248pc(aYR = C3248pc.C3250b.ITERATOR, displayName = "WeaponTypes")
    @C0064Am(aul = "f52557f657865ca0252be9d8f27ac406", aum = 0)
    @C2499fr
    /* renamed from: uY */
    private List<WeaponType> m2240uY() {
        throw new aWi(new aCE(this, f400Pi, new Object[0]));
    }

    /* renamed from: x */
    private void m2243x(C3438ra raVar) {
        bFf().mo5608dq().mo3197f(awd, raVar);
    }

    @C5566aOg
    @C2499fr
    /* renamed from: QS */
    public void mo1318QS() {
        switch (bFf().mo6893i(aKm)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, aKm, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, aKm, new Object[0]));
                break;
        }
        m2011QR();
    }

    @C3248pc(aYR = C3248pc.C3250b.ITERATOR, displayName = "Taikopedia Entries")
    /* renamed from: RW */
    public List<TaikopediaEntry> mo1319RW() {
        switch (bFf().mo6893i(aMk)) {
            case 0:
                return null;
            case 2:
                return (List) bFf().mo5606d(new aCE(this, aMk, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, aMk, new Object[0]));
                break;
        }
        return m2012RV();
    }

    /* renamed from: T */
    public C6494any mo11T() {
        return ___iScriptClass;
    }

    @C3248pc(aYR = C3248pc.C3250b.ITERATOR, displayName = "ItemGenSets")
    /* renamed from: TX */
    public List<ItemGenSet> mo1320TX() {
        switch (bFf().mo6893i(aPv)) {
            case 0:
                return null;
            case 2:
                return (List) bFf().mo5606d(new aCE(this, aPv, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, aPv, new Object[0]));
                break;
        }
        return m2015TW();
    }

    /* renamed from: U */
    public C2491fm[] mo12U() {
        return _m_methods;
    }

    @C3248pc(aYR = C3248pc.C3250b.ITERATOR, displayName = "Factions")
    /* renamed from: Uv */
    public List<Faction> mo1321Uv() {
        switch (bFf().mo6893i(aQq)) {
            case 0:
                return null;
            case 2:
                return (List) bFf().mo5606d(new aCE(this, aQq, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, aQq, new Object[0]));
                break;
        }
        return m2018Uu();
    }

    /* renamed from: W */
    public Object mo13W() {
        return new aNG(this);
    }

    /* renamed from: a */
    public Object mo14a(C0495Gr gr) {
        Object[] args = gr.getArgs();
        switch (gr.mo2417hq() - TaikodomObject._m_methodCount) {
            case 0:
                return m2114ap();
            case 1:
                m2145b((UUID) args[0]);
                return null;
            case 2:
                return m2121au();
            case 3:
                return aIP();
            case 4:
                return aIR();
            case 5:
                return aIT();
            case 6:
                return aIV();
            case 7:
                return aIX();
            case 8:
                return aIZ();
            case 9:
                return aJb();
            case 10:
                return aJd();
            case 11:
                return aJf();
            case 12:
                return aJh();
            case 13:
                return aJj();
            case 14:
                return aJl();
            case 15:
                return aJn();
            case 16:
                return aJp();
            case 17:
                return aJr();
            case 18:
                return aJt();
            case 19:
                return aJv();
            case 20:
                return aJx();
            case 21:
                return aJz();
            case 22:
                return aJB();
            case 23:
                return aJD();
            case 24:
                m2230jF();
                return null;
            case 25:
                aJF();
                return null;
            case 26:
                return aJH();
            case 27:
                return m2192eb((String) args[0]);
            case 28:
                return aJJ();
            case 29:
                return aJL();
            case 30:
                return m2193ed((String) args[0]);
            case 31:
                m2042a((User) args[0]);
                return null;
            case 32:
                m2152c((User) args[0]);
                return null;
            case 33:
                return aJN();
            case 34:
                return aJP();
            case 35:
                return m2022a((User) args[0], (String) args[1], (String) args[2]);
            case 36:
                return new Integer(m2184e((User) args[0]));
            case 37:
                return m2194ef((String) args[0]);
            case 38:
                return m2146c((String) args[0], (String) args[1], (String) args[2]);
            case 39:
                return m2021a((String) args[0], (String) args[1], (String) args[2], (String) args[3]);
            case 40:
                return aJR();
            case 41:
                m2056a((MissionTemplate) args[0]);
                return null;
            case 42:
                m2160c((MissionTemplate) args[0]);
                return null;
            case 43:
                return aJT();
            case 44:
                m2073a((EquippedShipType) args[0]);
                return null;
            case 45:
                m2166c((EquippedShipType) args[0]);
                return null;
            case 46:
                return aJV();
            case 47:
                m2228j((NPCType) args[0]);
                return null;
            case 48:
                m2234l((NPCType) args[0]);
                return null;
            case 49:
                return aJX();
            case 50:
                m2223i((StellarSystem) args[0]);
                return null;
            case 51:
                m2231k((StellarSystem) args[0]);
                return null;
            case 52:
                return aJZ();
            case 53:
                m2188e((ClipType) args[0]);
                return null;
            case 54:
                m2208g((ClipType) args[0]);
                return null;
            case 55:
                return aKb();
            case 56:
                m2054a((ClipBoxType) args[0]);
                return null;
            case 57:
                m2158c((ClipBoxType) args[0]);
                return null;
            case 58:
                return aKd();
            case 59:
                return aKf();
            case 60:
                m2157c((CargoHoldType) args[0]);
                return null;
            case 61:
                m2189e((CargoHoldType) args[0]);
                return null;
            case 62:
                m2222h((GateType) args[0]);
                return null;
            case 63:
                m2229j((GateType) args[0]);
                return null;
            case 64:
                return aKh();
            case 65:
                m2143b((HullType) args[0]);
                return null;
            case 66:
                m2182d((HullType) args[0]);
                return null;
            case 67:
                return aKj();
            case 68:
                m2139b((ShieldType) args[0]);
                return null;
            case 69:
                m2181d((ShieldType) args[0]);
                return null;
            case 70:
                return aKl();
            case 71:
                m2203f((HazardAreaType) args[0]);
                return null;
            case 72:
                m2220h((HazardAreaType) args[0]);
                return null;
            case 73:
                return aKn();
            case 74:
                m2032a((MerchandiseType) args[0]);
                return null;
            case 75:
                m2150c((MerchandiseType) args[0]);
                return null;
            case 76:
                return aKp();
            case 77:
                m2177d((BagItemType) args[0]);
                return null;
            case 78:
                m2200f((BagItemType) args[0]);
                return null;
            case 79:
                return aKr();
            case 80:
                m2031a((BlueprintType) args[0]);
                return null;
            case 81:
                m2149c((BlueprintType) args[0]);
                return null;
            case 82:
                return aKt();
            case 83:
                return aKv();
            case 84:
                m2190e((SceneryType) args[0]);
                return null;
            case 85:
                m2209g((SceneryType) args[0]);
                return null;
            case 86:
                m2055a((ShipSectorType) args[0]);
                return null;
            case 87:
                m2159c((ShipSectorType) args[0]);
                return null;
            case 88:
                return aKx();
            case 89:
                m2045a((ShipStructureType) args[0]);
                return null;
            case 90:
                m2154c((ShipStructureType) args[0]);
                return null;
            case 91:
                return aKz();
            case 92:
                m2224i((ShipType) args[0]);
                return null;
            case 93:
                m2232k((ShipType) args[0]);
                return null;
            case 94:
                return aKB();
            case 95:
                m2198f((StationType) args[0]);
                return null;
            case 96:
                m2218h((StationType) args[0]);
                return null;
            case 97:
                return aKD();
            case 98:
                m2049a((WeaponType) args[0]);
                return null;
            case 99:
                m2155c((WeaponType) args[0]);
                return null;
            case 100:
                return m2240uY();
            case 101:
                m2043a((RawMaterialType) args[0]);
                return null;
            case 102:
                m2153c((RawMaterialType) args[0]);
                return null;
            case 103:
                return aKF();
            case 104:
                m2227j((ItemTypeCategory) args[0]);
                return null;
            case 105:
                m2233l((ItemTypeCategory) args[0]);
                return null;
            case 106:
                return aKH();
            case 107:
                m2186e((ItemGenSet) args[0]);
                return null;
            case 108:
                m2207g((ItemGenSet) args[0]);
                return null;
            case 109:
                return m2015TW();
            case 110:
                m2135b((LootType) args[0]);
                return null;
            case 111:
                m2174d((LootType) args[0]);
                return null;
            case 112:
                return aKJ();
            case 113:
                return aKL();
            case 114:
                m2235n((NPCType) args[0]);
                return null;
            case 115:
                m2219h((Faction) args[0]);
                return null;
            case 116:
                m2226j((Faction) args[0]);
                return null;
            case 117:
                return m2018Uu();
            case 118:
                m2064a((Tutorial) args[0]);
                return null;
            case 119:
                m2163c((Tutorial) args[0]);
                return null;
            case 120:
                return aKN();
            case 121:
                return new Boolean(m2238s((ItemType) args[0]));
            case 122:
                return aKP();
            case 123:
                m2028a((C0468GU) args[0]);
                return null;
            case 124:
                return new Boolean(azS());
            case 125:
                return m2170cC(((Boolean) args[0]).booleanValue());
            case 126:
                m2011QR();
                return null;
            case 127:
                m2206g((User) args[0]);
                return null;
            case 128:
                m2214gQ(((Integer) args[0]).intValue());
                return null;
            case 129:
                m2023a(((Long) args[0]).longValue(), (String) args[1], ((Long) args[2]).longValue(), ((Integer) args[3]).intValue());
                return null;
            case 130:
                m2216gS(((Integer) args[0]).intValue());
                return null;
            case 131:
                return m2020a((ClientConnect) args[0], (String) args[1], (String) args[2]);
            case 132:
                return aKR();
            case 133:
                m2147c((AdvertiseType) args[0]);
                return null;
            case 134:
                m2185e((AdvertiseType) args[0]);
                return null;
            case 135:
                return aKT();
            case 136:
                m2176d((AsteroidType) args[0]);
                return null;
            case 137:
                m2199f((AsteroidType) args[0]);
                return null;
            case 138:
                return aKV();
            case 139:
                return aKX();
            case 140:
                m2212gL((I18NString) args[0]);
                return null;
            case 141:
                return aKZ();
            case 142:
                m2130b((AvatarManager) args[0]);
                return null;
            case 143:
                m2213gN((I18NString) args[0]);
                return null;
            case 144:
                m2144b((CollisionFXSet) args[0]);
                return null;
            case 145:
                m2183d((CollisionFXSet) args[0]);
                return null;
            case 146:
                return aLb();
            case 147:
                return m2128b(((Integer) args[0]).intValue(), args[1]);
            case 148:
                m2065a((Player) args[0], (ShipType) args[1], (WeaponType) args[2]);
                return null;
            case 149:
                m2066a((Player) args[0], (ShipType) args[1], (List<WeaponType>) (List) args[2]);
                return null;
            case 150:
                m2204f((AmplifierType) args[0]);
                return null;
            case 151:
                m2221h((AmplifierType) args[0]);
                return null;
            case 152:
                return m2241va();
            case 153:
                m2030a((ModuleType) args[0]);
                return null;
            case 154:
                m2148c((ModuleType) args[0]);
                return null;
            case 155:
                return aLd();
            case 156:
                m2050a((AttributeBuffType) args[0]);
                return null;
            case 157:
                m2156c((AttributeBuffType) args[0]);
                return null;
            case 158:
                return aLf();
            case 159:
                m2180d((C6544aow) args[0]);
                return null;
            case 160:
                m2202f((C6544aow) args[0]);
                return null;
            case 161:
                return aLh();
            case 162:
                m2037a((AIControllerType) args[0]);
                return null;
            case 163:
                m2151c((AIControllerType) args[0]);
                return null;
            case 164:
                return aLj();
            case 165:
                m2070a((ConsignmentFeeManager) args[0]);
                return null;
            case 166:
                m2164c((ConsignmentFeeManager) args[0]);
                return null;
            case 167:
                return aLl();
            case 168:
                m2165c((SectorCategory) args[0]);
                return null;
            case 169:
                m2191e((SectorCategory) args[0]);
                return null;
            case 170:
                return aLn();
            case 171:
                return aLp();
            case 172:
                m2195eh((String) args[0]);
                return null;
            case 173:
                m2242w((DamageType) args[0]);
                return null;
            case 174:
                m2244y((DamageType) args[0]);
                return null;
            case 175:
                return aLr();
            case 176:
                return m2196ej((String) args[0]);
            case 177:
                m2132b((CruiseSpeedType) args[0]);
                return null;
            case 178:
                m2173d((CruiseSpeedType) args[0]);
                return null;
            case 179:
                return aLt();
            case 180:
                m2138b((CitizenshipType) args[0]);
                return null;
            case 181:
                m2178d((CitizenshipType) args[0]);
                return null;
            case 182:
                return aLv();
            case 183:
                m2062a((CitizenshipPack) args[0]);
                return null;
            case 184:
                m2161c((CitizenshipPack) args[0]);
                return null;
            case 185:
                return aLx();
            case KeyCode.cso:
                m2076a((CitizenshipReward) args[0]);
                return null;
            case 187:
                m2167c((CitizenshipReward) args[0]);
                return null;
            case 188:
                return aLz();
            case 189:
                m2063a((TaikopediaEntry) args[0]);
                return null;
            case 190:
                m2162c((TaikopediaEntry) args[0]);
                return null;
            case 191:
                return m2012RV();
            case 192:
                m2179d((DatabaseCategory) args[0]);
                return null;
            case 193:
                m2201f((DatabaseCategory) args[0]);
                return null;
            case 194:
                return aLB();
            case 195:
                m2078a((AsteroidZoneCategory) args[0]);
                return null;
            case 196:
                m2168c((AsteroidZoneCategory) args[0]);
                return null;
            case 197:
                return aLD();
            case 198:
                m2171d((SpaceCategory) args[0]);
                return null;
            case 199:
                m2197f((SpaceCategory) args[0]);
                return null;
            case 200:
                return aLF();
            case 201:
                m2131b((CitizenImprovementType) args[0]);
                return null;
            case 202:
                m2172d((CitizenImprovementType) args[0]);
                return null;
            case KeyCode.csG:
                return aLH();
            case KeyCode.csH:
                m2187e((Corporation) args[0]);
                return null;
            case KeyCode.csI:
                return new Boolean(m2210g((Corporation) args[0]));
            case KeyCode.csJ:
                return aLJ();
            case KeyCode.csK:
                aLL();
                return null;
            case KeyCode.csL:
                m2067a((C6417amZ) args[0]);
                return null;
            case KeyCode.csM:
                return aLN();
            case KeyCode.csN:
                m2140b((ContractBoard) args[0]);
                return null;
            case KeyCode.csO:
                return aLP();
            case KeyCode.csP:
                return aLR();
            case KeyCode.csQ:
                return aLT();
            case KeyCode.csR:
                return aLV();
            case KeyCode.csS:
                return aLX();
            case KeyCode.csT:
                return aLZ();
            case KeyCode.csU:
                return aMb();
            case KeyCode.csV:
                return new Integer(aMd());
            case KeyCode.csW:
                aMf();
                return null;
            case KeyCode.csX:
                return m2205fh();
            case KeyCode.csY:
                m2133b((Market) args[0]);
                return null;
            case KeyCode.csZ:
                return aMg();
            case KeyCode.cta:
                m2141b((GlobalEconomyInfo) args[0]);
                return null;
            case KeyCode.ctb:
                m2136b((VirtualWarehouse) args[0]);
                return null;
            case KeyCode.ctc:
                m2175d((VirtualWarehouse) args[0]);
                return null;
            case KeyCode.ctd:
                return aMi();
            case KeyCode.cte:
                return aMk();
            case KeyCode.ctf:
                m2068a((UserConnection) args[0]);
                return null;
            case KeyCode.ctg:
                aMm();
                return null;
            case KeyCode.cth:
                return aMo();
            case KeyCode.cti:
                aMq();
                return null;
            case KeyCode.ctj:
                return aMs();
            case KeyCode.ctk:
                return m2237s((Class) args[0]);
            case KeyCode.ctl:
                return aMu();
            case KeyCode.ctm:
                return aMv();
            case KeyCode.ctn:
                m2134b((CitizenshipOffice) args[0]);
                return null;
            case KeyCode.cto:
                m2089aG();
                return null;
            case KeyCode.ctp:
                return aMx();
            case KeyCode.ctq:
                return m2117ar();
            case 240:
                return aMz();
            case KeyCode.cts:
                return aMB();
            case 242:
                return new Boolean(m2082a((User) args[0], (String) args[1]));
            case 243:
                return aMD();
            default:
                return super.mo14a(gr);
        }
    }

    /* renamed from: a */
    public void mo15a(Collection collection, C0495Gr gr) {
        super.mo15a(collection, gr);
    }

    /* renamed from: aH */
    public void mo70aH() {
        switch (bFf().mo6893i(_f_onResurrect_0020_0028_0029V)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, _f_onResurrect_0020_0028_0029V, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, _f_onResurrect_0020_0028_0029V, new Object[0]));
                break;
        }
        m2089aG();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Tael Bank")
    public Bank aIS() {
        switch (bFf().mo6893i(cMi)) {
            case 0:
                return null;
            case 2:
                return (Bank) bFf().mo5606d(new aCE(this, cMi, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, cMi, new Object[0]));
                break;
        }
        return aIR();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Hoplon Bank")
    public Bank aIU() {
        switch (bFf().mo6893i(cMj)) {
            case 0:
                return null;
            case 2:
                return (Bank) bFf().mo5606d(new aCE(this, cMj, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, cMj, new Object[0]));
                break;
        }
        return aIT();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "MarkManager")
    public MarkManager aIW() {
        switch (bFf().mo6893i(cMk)) {
            case 0:
                return null;
            case 2:
                return (MarkManager) bFf().mo5606d(new aCE(this, cMk, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, cMk, new Object[0]));
                break;
        }
        return aIV();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "# NLSManager")
    public NLSManager aIY() {
        switch (bFf().mo6893i(cMl)) {
            case 0:
                return null;
            case 2:
                return (NLSManager) bFf().mo5606d(new aCE(this, cMl, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, cMl, new Object[0]));
                break;
        }
        return aIX();
    }

    public Insurer aJA() {
        switch (bFf().mo6893i(cMz)) {
            case 0:
                return null;
            case 2:
                return (Insurer) bFf().mo5606d(new aCE(this, cMz, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, cMz, new Object[0]));
                break;
        }
        return aJz();
    }

    @Deprecated
    public C1077Pl aJC() {
        switch (bFf().mo6893i(cMA)) {
            case 0:
                return null;
            case 2:
                return (C1077Pl) bFf().mo5606d(new aCE(this, cMA, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, cMA, new Object[0]));
                break;
        }
        return aJB();
    }

    public C0694Ju aJE() {
        switch (bFf().mo6893i(cMB)) {
            case 0:
                return null;
            case 2:
                return (C0694Ju) bFf().mo5606d(new aCE(this, cMB, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, cMB, new Object[0]));
                break;
        }
        return aJD();
    }

    public PingScript aJI() {
        switch (bFf().mo6893i(cMD)) {
            case 0:
                return null;
            case 2:
                return (PingScript) bFf().mo5606d(new aCE(this, cMD, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, cMD, new Object[0]));
                break;
        }
        return aJH();
    }

    @C5566aOg
    public List<UserConnection> aJK() {
        switch (bFf().mo6893i(cMF)) {
            case 0:
                return null;
            case 2:
                return (List) bFf().mo5606d(new aCE(this, cMF, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, cMF, new Object[0]));
                break;
        }
        return aJJ();
    }

    @C5566aOg
    public List<Player> aJM() {
        switch (bFf().mo6893i(cMG)) {
            case 0:
                return null;
            case 2:
                return (List) bFf().mo5606d(new aCE(this, cMG, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, cMG, new Object[0]));
                break;
        }
        return aJL();
    }

    @C5794aaa
    public Collection<User> aJO() {
        switch (bFf().mo6893i(cMK)) {
            case 0:
                return null;
            case 2:
                return (Collection) bFf().mo5606d(new aCE(this, cMK, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, cMK, new Object[0]));
                break;
        }
        return aJN();
    }

    @C5566aOg
    public Collection<User> aJQ() {
        switch (bFf().mo6893i(cML)) {
            case 0:
                return null;
            case 2:
                return (Collection) bFf().mo5606d(new aCE(this, cML, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, cML, new Object[0]));
                break;
        }
        return aJP();
    }

    @C5566aOg
    @C3248pc(aYR = C3248pc.C3250b.ITERATOR, displayName = "AIClasses")
    @C2499fr
    public List<ClassContainer> aJS() {
        switch (bFf().mo6893i(cMR)) {
            case 0:
                return null;
            case 2:
                return (List) bFf().mo5606d(new aCE(this, cMR, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, cMR, new Object[0]));
                break;
        }
        return aJR();
    }

    @C3248pc(aYR = C3248pc.C3250b.ITERATOR, displayName = "Mission Templates")
    public List<MissionTemplate> aJU() {
        switch (bFf().mo6893i(cMU)) {
            case 0:
                return null;
            case 2:
                return (List) bFf().mo5606d(new aCE(this, cMU, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, cMU, new Object[0]));
                break;
        }
        return aJT();
    }

    @C3248pc(aYR = C3248pc.C3250b.ITERATOR, displayName = "EquippedShipTypes")
    public List<EquippedShipType> aJW() {
        switch (bFf().mo6893i(cMX)) {
            case 0:
                return null;
            case 2:
                return (List) bFf().mo5606d(new aCE(this, cMX, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, cMX, new Object[0]));
                break;
        }
        return aJV();
    }

    @C3248pc(aYR = C3248pc.C3250b.ITERATOR, displayName = "NPCTypes")
    public List<NPCType> aJY() {
        switch (bFf().mo6893i(cNa)) {
            case 0:
                return null;
            case 2:
                return (List) bFf().mo5606d(new aCE(this, cNa, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, cNa, new Object[0]));
                break;
        }
        return aJX();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Assets")
    public AssetGroup aJa() {
        switch (bFf().mo6893i(cMm)) {
            case 0:
                return null;
            case 2:
                return (AssetGroup) bFf().mo5606d(new aCE(this, cMm, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, cMm, new Object[0]));
                break;
        }
        return aIZ();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Space Zone Types Manager")
    public SpaceZoneTypesManager aJc() {
        switch (bFf().mo6893i(cMn)) {
            case 0:
                return null;
            case 2:
                return (SpaceZoneTypesManager) bFf().mo5606d(new aCE(this, cMn, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, cMn, new Object[0]));
                break;
        }
        return aJb();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "# Default Taikodom Contents")
    public TaikodomDefaultContents aJe() {
        switch (bFf().mo6893i(cMo)) {
            case 0:
                return null;
            case 2:
                return (TaikodomDefaultContents) bFf().mo5606d(new aCE(this, cMo, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, cMo, new Object[0]));
                break;
        }
        return aJd();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Physics Tweaks")
    public GlobalPhysicsTweaks aJg() {
        switch (bFf().mo6893i(cMp)) {
            case 0:
                return null;
            case 2:
                return (GlobalPhysicsTweaks) bFf().mo5606d(new aCE(this, cMp, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, cMp, new Object[0]));
                break;
        }
        return aJf();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Item Billing")
    public ItemBilling aJi() {
        switch (bFf().mo6893i(cMq)) {
            case 0:
                return null;
            case 2:
                return (ItemBilling) bFf().mo5606d(new aCE(this, cMq, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, cMq, new Object[0]));
                break;
        }
        return aJh();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Progression")
    public Progression aJk() {
        switch (bFf().mo6893i(cMr)) {
            case 0:
                return null;
            case 2:
                return (Progression) bFf().mo5606d(new aCE(this, cMr, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, cMr, new Object[0]));
                break;
        }
        return aJj();
    }

    public PlayerFinder aJm() {
        switch (bFf().mo6893i(cMs)) {
            case 0:
                return null;
            case 2:
                return (PlayerFinder) bFf().mo5606d(new aCE(this, cMs, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, cMs, new Object[0]));
                break;
        }
        return aJl();
    }

    public ServerEditorUtility aJo() {
        switch (bFf().mo6893i(cMt)) {
            case 0:
                return null;
            case 2:
                return (ServerEditorUtility) bFf().mo5606d(new aCE(this, cMt, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, cMt, new Object[0]));
                break;
        }
        return aJn();
    }

    public SupportOuterface aJq() {
        switch (bFf().mo6893i(cMu)) {
            case 0:
                return null;
            case 2:
                return (SupportOuterface) bFf().mo5606d(new aCE(this, cMu, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, cMu, new Object[0]));
                break;
        }
        return aJp();
    }

    public Backdoor aJs() {
        switch (bFf().mo6893i(cMv)) {
            case 0:
                return null;
            case 2:
                return (Backdoor) bFf().mo5606d(new aCE(this, cMv, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, cMv, new Object[0]));
                break;
        }
        return aJr();
    }

    public StatsManager aJu() {
        switch (bFf().mo6893i(cMw)) {
            case 0:
                return null;
            case 2:
                return (StatsManager) bFf().mo5606d(new aCE(this, cMw, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, cMw, new Object[0]));
                break;
        }
        return aJt();
    }

    public AdvertiseViewTable aJw() {
        switch (bFf().mo6893i(cMx)) {
            case 0:
                return null;
            case 2:
                return (AdvertiseViewTable) bFf().mo5606d(new aCE(this, cMx, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, cMx, new Object[0]));
                break;
        }
        return aJv();
    }

    public CorporationReservedNames aJy() {
        switch (bFf().mo6893i(cMy)) {
            case 0:
                return null;
            case 2:
                return (CorporationReservedNames) bFf().mo5606d(new aCE(this, cMy, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, cMy, new Object[0]));
                break;
        }
        return aJx();
    }

    @C3248pc(aYR = C3248pc.C3250b.ITERATOR, displayName = "ShipStructureTypes")
    public List<ShipStructureType> aKA() {
        switch (bFf().mo6893i(cNQ)) {
            case 0:
                return null;
            case 2:
                return (List) bFf().mo5606d(new aCE(this, cNQ, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, cNQ, new Object[0]));
                break;
        }
        return aKz();
    }

    @C5566aOg
    @C3248pc(aYR = C3248pc.C3250b.ITERATOR, displayName = "ShipTypes")
    @C2499fr
    public List<ShipType> aKC() {
        switch (bFf().mo6893i(cNT)) {
            case 0:
                return null;
            case 2:
                return (List) bFf().mo5606d(new aCE(this, cNT, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, cNT, new Object[0]));
                break;
        }
        return aKB();
    }

    @C3248pc(aYR = C3248pc.C3250b.ITERATOR, displayName = "StationTypes")
    public List<StationType> aKE() {
        switch (bFf().mo6893i(cNW)) {
            case 0:
                return null;
            case 2:
                return (List) bFf().mo5606d(new aCE(this, cNW, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, cNW, new Object[0]));
                break;
        }
        return aKD();
    }

    @C3248pc(aYR = C3248pc.C3250b.ITERATOR, displayName = "RawMaterialTypes")
    public List<RawMaterialType> aKG() {
        switch (bFf().mo6893i(cNZ)) {
            case 0:
                return null;
            case 2:
                return (List) bFf().mo5606d(new aCE(this, cNZ, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, cNZ, new Object[0]));
                break;
        }
        return aKF();
    }

    @C3248pc(aYR = C3248pc.C3250b.ITERATOR, displayName = "ItemTypeCategories")
    public List<ItemTypeCategory> aKI() {
        switch (bFf().mo6893i(cOc)) {
            case 0:
                return null;
            case 2:
                return (List) bFf().mo5606d(new aCE(this, cOc, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, cOc, new Object[0]));
                break;
        }
        return aKH();
    }

    @C3248pc(aYR = C3248pc.C3250b.ITERATOR, displayName = "LootTypes")
    public List<LootType> aKK() {
        switch (bFf().mo6893i(cOh)) {
            case 0:
                return null;
            case 2:
                return (List) bFf().mo5606d(new aCE(this, cOh, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, cOh, new Object[0]));
                break;
        }
        return aKJ();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Mestre dos Magos")
    public NPCType aKM() {
        switch (bFf().mo6893i(cOi)) {
            case 0:
                return null;
            case 2:
                return (NPCType) bFf().mo5606d(new aCE(this, cOi, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, cOi, new Object[0]));
                break;
        }
        return aKL();
    }

    @C3248pc(aYR = C3248pc.C3250b.ITERATOR, displayName = "Tutorials")
    public List<Tutorial> aKO() {
        switch (bFf().mo6893i(cOm)) {
            case 0:
                return null;
            case 2:
                return (List) bFf().mo5606d(new aCE(this, cOm, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, cOm, new Object[0]));
                break;
        }
        return aKN();
    }

    public Set<ItemType> aKQ() {
        switch (bFf().mo6893i(cOo)) {
            case 0:
                return null;
            case 2:
                return (Set) bFf().mo5606d(new aCE(this, cOo, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, cOo, new Object[0]));
                break;
        }
        return aKP();
    }

    @C5566aOg
    @C2499fr
    public BotScript aKS() {
        switch (bFf().mo6893i(cOw)) {
            case 0:
                return null;
            case 2:
                return (BotScript) bFf().mo5606d(new aCE(this, cOw, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, cOw, new Object[0]));
                break;
        }
        return aKR();
    }

    @C3248pc(aYR = C3248pc.C3250b.ITERATOR, displayName = "AdvertiseTypes")
    public List<AdvertiseType> aKU() {
        switch (bFf().mo6893i(cOz)) {
            case 0:
                return null;
            case 2:
                return (List) bFf().mo5606d(new aCE(this, cOz, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, cOz, new Object[0]));
                break;
        }
        return aKT();
    }

    @C3248pc(aYR = C3248pc.C3250b.ITERATOR, displayName = "AsteroidTypes")
    public List<AsteroidType> aKW() {
        switch (bFf().mo6893i(cOC)) {
            case 0:
                return null;
            case 2:
                return (List) bFf().mo5606d(new aCE(this, cOC, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, cOC, new Object[0]));
                break;
        }
        return aKV();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Advertise Path")
    public I18NString aKY() {
        switch (bFf().mo6893i(cOD)) {
            case 0:
                return null;
            case 2:
                return (I18NString) bFf().mo5606d(new aCE(this, cOD, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, cOD, new Object[0]));
                break;
        }
        return aKX();
    }

    @C3248pc(aYR = C3248pc.C3250b.ITERATOR, displayName = "StellarSystems")
    public List<StellarSystem> aKa() {
        switch (bFf().mo6893i(cNd)) {
            case 0:
                return null;
            case 2:
                return (List) bFf().mo5606d(new aCE(this, cNd, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, cNd, new Object[0]));
                break;
        }
        return aJZ();
    }

    @C3248pc(aYR = C3248pc.C3250b.ITERATOR, displayName = "AmmoTypes")
    public List<ClipType> aKc() {
        switch (bFf().mo6893i(cNg)) {
            case 0:
                return null;
            case 2:
                return (List) bFf().mo5606d(new aCE(this, cNg, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, cNg, new Object[0]));
                break;
        }
        return aKb();
    }

    @C3248pc(aYR = C3248pc.C3250b.ITERATOR, displayName = "AmmoBoxTypes")
    public List<ClipBoxType> aKe() {
        switch (bFf().mo6893i(cNj)) {
            case 0:
                return null;
            case 2:
                return (List) bFf().mo5606d(new aCE(this, cNj, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, cNj, new Object[0]));
                break;
        }
        return aKd();
    }

    @C3248pc(aYR = C3248pc.C3250b.ITERATOR, displayName = "CargoHoldType")
    public List<CargoHoldType> aKg() {
        switch (bFf().mo6893i(cNk)) {
            case 0:
                return null;
            case 2:
                return (List) bFf().mo5606d(new aCE(this, cNk, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, cNk, new Object[0]));
                break;
        }
        return aKf();
    }

    @C3248pc(aYR = C3248pc.C3250b.ITERATOR, displayName = "GateTypes")
    public List<GateType> aKi() {
        switch (bFf().mo6893i(cNp)) {
            case 0:
                return null;
            case 2:
                return (List) bFf().mo5606d(new aCE(this, cNp, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, cNp, new Object[0]));
                break;
        }
        return aKh();
    }

    @C3248pc(aYR = C3248pc.C3250b.ITERATOR, displayName = "HullTypes")
    public List<HullType> aKk() {
        switch (bFf().mo6893i(cNs)) {
            case 0:
                return null;
            case 2:
                return (List) bFf().mo5606d(new aCE(this, cNs, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, cNs, new Object[0]));
                break;
        }
        return aKj();
    }

    @C3248pc(aYR = C3248pc.C3250b.ITERATOR, displayName = "ShieldTypes")
    public List<ShieldType> aKm() {
        switch (bFf().mo6893i(cNv)) {
            case 0:
                return null;
            case 2:
                return (List) bFf().mo5606d(new aCE(this, cNv, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, cNv, new Object[0]));
                break;
        }
        return aKl();
    }

    @C3248pc(aYR = C3248pc.C3250b.ITERATOR, displayName = "HazardAreaTypes")
    public List<HazardAreaType> aKo() {
        switch (bFf().mo6893i(cNy)) {
            case 0:
                return null;
            case 2:
                return (List) bFf().mo5606d(new aCE(this, cNy, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, cNy, new Object[0]));
                break;
        }
        return aKn();
    }

    @C3248pc(aYR = C3248pc.C3250b.ITERATOR, displayName = "MerchandiseTypes")
    public List<MerchandiseType> aKq() {
        switch (bFf().mo6893i(cNB)) {
            case 0:
                return null;
            case 2:
                return (List) bFf().mo5606d(new aCE(this, cNB, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, cNB, new Object[0]));
                break;
        }
        return aKp();
    }

    @C3248pc(aYR = C3248pc.C3250b.ITERATOR, displayName = "BagTypes")
    public List<BagItemType> aKs() {
        switch (bFf().mo6893i(cNE)) {
            case 0:
                return null;
            case 2:
                return (List) bFf().mo5606d(new aCE(this, cNE, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, cNE, new Object[0]));
                break;
        }
        return aKr();
    }

    @C3248pc(aYR = C3248pc.C3250b.ITERATOR, displayName = "BlueprintTypes")
    public List<BlueprintType> aKu() {
        switch (bFf().mo6893i(cNH)) {
            case 0:
                return null;
            case 2:
                return (List) bFf().mo5606d(new aCE(this, cNH, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, cNH, new Object[0]));
                break;
        }
        return aKt();
    }

    @C3248pc(aYR = C3248pc.C3250b.ITERATOR, displayName = "SceneryTypes")
    public List<SceneryType> aKw() {
        switch (bFf().mo6893i(cNI)) {
            case 0:
                return null;
            case 2:
                return (List) bFf().mo5606d(new aCE(this, cNI, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, cNI, new Object[0]));
                break;
        }
        return aKv();
    }

    @C3248pc(aYR = C3248pc.C3250b.ITERATOR, displayName = "ShipSectorTypes")
    public List<ShipSectorType> aKy() {
        switch (bFf().mo6893i(cNN)) {
            case 0:
                return null;
            case 2:
                return (List) bFf().mo5606d(new aCE(this, cNN, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, cNN, new Object[0]));
                break;
        }
        return aKx();
    }

    @C3248pc(aYR = C3248pc.C3250b.ITERATOR, displayName = "Citizenship Reward")
    public List<CitizenshipReward> aLA() {
        switch (bFf().mo6893i(cPz)) {
            case 0:
                return null;
            case 2:
                return (List) bFf().mo5606d(new aCE(this, cPz, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, cPz, new Object[0]));
                break;
        }
        return aLz();
    }

    @C3248pc(aYR = C3248pc.C3250b.ITERATOR, displayName = "Database Categories")
    public List<DatabaseCategory> aLC() {
        switch (bFf().mo6893i(cPC)) {
            case 0:
                return null;
            case 2:
                return (List) bFf().mo5606d(new aCE(this, cPC, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, cPC, new Object[0]));
                break;
        }
        return aLB();
    }

    @C3248pc(aYR = C3248pc.C3250b.ITERATOR, displayName = "Asteroid Zone Categories")
    public List<AsteroidZoneCategory> aLE() {
        switch (bFf().mo6893i(cPF)) {
            case 0:
                return null;
            case 2:
                return (List) bFf().mo5606d(new aCE(this, cPF, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, cPF, new Object[0]));
                break;
        }
        return aLD();
    }

    @C3248pc(aYR = C3248pc.C3250b.ITERATOR, displayName = "Space Categories")
    public List<SpaceCategory> aLG() {
        switch (bFf().mo6893i(cPI)) {
            case 0:
                return null;
            case 2:
                return (List) bFf().mo5606d(new aCE(this, cPI, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, cPI, new Object[0]));
                break;
        }
        return aLF();
    }

    @C3248pc(aYR = C3248pc.C3250b.ITERATOR, displayName = "Citizenship Improvements Types")
    public List<CitizenImprovementType> aLI() {
        switch (bFf().mo6893i(cPL)) {
            case 0:
                return null;
            case 2:
                return (List) bFf().mo5606d(new aCE(this, cPL, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, cPL, new Object[0]));
                break;
        }
        return aLH();
    }

    public List<Corporation> aLK() {
        switch (bFf().mo6893i(cPO)) {
            case 0:
                return null;
            case 2:
                return (List) bFf().mo5606d(new aCE(this, cPO, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, cPO, new Object[0]));
                break;
        }
        return aLJ();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "ContractBoard")
    public ContractBoard aLO() {
        switch (bFf().mo6893i(cPQ)) {
            case 0:
                return null;
            case 2:
                return (ContractBoard) bFf().mo5606d(new aCE(this, cPQ, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, cPQ, new Object[0]));
                break;
        }
        return aLN();
    }

    public TemporaryFeatureTest aLQ() {
        switch (bFf().mo6893i(cPS)) {
            case 0:
                return null;
            case 2:
                return (TemporaryFeatureTest) bFf().mo5606d(new aCE(this, cPS, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, cPS, new Object[0]));
                break;
        }
        return aLP();
    }

    public DebugFlags aLS() {
        switch (bFf().mo6893i(cPT)) {
            case 0:
                return null;
            case 2:
                return (DebugFlags) bFf().mo5606d(new aCE(this, cPT, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, cPT, new Object[0]));
                break;
        }
        return aLR();
    }

    public TemporaryFeatureBlock aLU() {
        switch (bFf().mo6893i(cPU)) {
            case 0:
                return null;
            case 2:
                return (TemporaryFeatureBlock) bFf().mo5606d(new aCE(this, cPU, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, cPU, new Object[0]));
                break;
        }
        return aLT();
    }

    @C2198cg
    public ClientUtils aLW() {
        switch (bFf().mo6893i(cPV)) {
            case 0:
                return null;
            case 2:
                return (ClientUtils) bFf().mo5606d(new aCE(this, cPV, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, cPV, new Object[0]));
                break;
        }
        return aLV();
    }

    @C2198cg
    public OffloadUtils aLY() {
        switch (bFf().mo6893i(cPW)) {
            case 0:
                return null;
            case 2:
                return (OffloadUtils) bFf().mo5606d(new aCE(this, cPW, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, cPW, new Object[0]));
                break;
        }
        return aLX();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Avatar Manager")
    public AvatarManager aLa() {
        switch (bFf().mo6893i(cOF)) {
            case 0:
                return null;
            case 2:
                return (AvatarManager) bFf().mo5606d(new aCE(this, cOF, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, cOF, new Object[0]));
                break;
        }
        return aKZ();
    }

    @C3248pc(aYR = C3248pc.C3250b.ITERATOR, displayName = "Collision Sets")
    public List<CollisionFXSet> aLc() {
        switch (bFf().mo6893i(cOK)) {
            case 0:
                return null;
            case 2:
                return (List) bFf().mo5606d(new aCE(this, cOK, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, cOK, new Object[0]));
                break;
        }
        return aLb();
    }

    @C3248pc(aYR = C3248pc.C3250b.ITERATOR, displayName = "Module Types")
    public List<ModuleType> aLe() {
        switch (bFf().mo6893i(cOS)) {
            case 0:
                return null;
            case 2:
                return (List) bFf().mo5606d(new aCE(this, cOS, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, cOS, new Object[0]));
                break;
        }
        return aLd();
    }

    @C3248pc(aYR = C3248pc.C3250b.ITERATOR, displayName = "AttributeBuffType")
    public List<AttributeBuffType> aLg() {
        switch (bFf().mo6893i(cOV)) {
            case 0:
                return null;
            case 2:
                return (List) bFf().mo5606d(new aCE(this, cOV, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, cOV, new Object[0]));
                break;
        }
        return aLf();
    }

    @C3248pc(aYR = C3248pc.C3250b.ITERATOR, displayName = "TurretTypes")
    public List<C6544aow> aLi() {
        switch (bFf().mo6893i(cOY)) {
            case 0:
                return null;
            case 2:
                return (List) bFf().mo5606d(new aCE(this, cOY, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, cOY, new Object[0]));
                break;
        }
        return aLh();
    }

    @C3248pc(aYR = C3248pc.C3250b.ITERATOR, displayName = "AI Controller Types")
    public List<AIControllerType> aLk() {
        switch (bFf().mo6893i(cPb)) {
            case 0:
                return null;
            case 2:
                return (List) bFf().mo5606d(new aCE(this, cPb, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, cPb, new Object[0]));
                break;
        }
        return aLj();
    }

    @C3248pc(aYR = C3248pc.C3250b.ITERATOR, displayName = "Fee Managers")
    public List<ConsignmentFeeManager> aLm() {
        switch (bFf().mo6893i(cPe)) {
            case 0:
                return null;
            case 2:
                return (List) bFf().mo5606d(new aCE(this, cPe, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, cPe, new Object[0]));
                break;
        }
        return aLl();
    }

    @C3248pc(aYR = C3248pc.C3250b.ITERATOR, displayName = "Sector Categories")
    public List<SectorCategory> aLo() {
        switch (bFf().mo6893i(cPh)) {
            case 0:
                return null;
            case 2:
                return (List) bFf().mo5606d(new aCE(this, cPh, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, cPh, new Object[0]));
                break;
        }
        return aLn();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Forbidden Names (comma separated)")
    public String aLq() {
        switch (bFf().mo6893i(cPi)) {
            case 0:
                return null;
            case 2:
                return (String) bFf().mo5606d(new aCE(this, cPi, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, cPi, new Object[0]));
                break;
        }
        return aLp();
    }

    @C3248pc(aYR = C3248pc.C3250b.ITERATOR, displayName = "Damage Types")
    public Set<DamageType> aLs() {
        switch (bFf().mo6893i(cPm)) {
            case 0:
                return null;
            case 2:
                return (Set) bFf().mo5606d(new aCE(this, cPm, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, cPm, new Object[0]));
                break;
        }
        return aLr();
    }

    @C3248pc(aYR = C3248pc.C3250b.ITERATOR, displayName = "Cruise Speed Types")
    public Set<CruiseSpeedType> aLu() {
        switch (bFf().mo6893i(cPq)) {
            case 0:
                return null;
            case 2:
                return (Set) bFf().mo5606d(new aCE(this, cPq, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, cPq, new Object[0]));
                break;
        }
        return aLt();
    }

    @C3248pc(aYR = C3248pc.C3250b.ITERATOR, displayName = "Citizenship Types")
    public List<CitizenshipType> aLw() {
        switch (bFf().mo6893i(cPt)) {
            case 0:
                return null;
            case 2:
                return (List) bFf().mo5606d(new aCE(this, cPt, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, cPt, new Object[0]));
                break;
        }
        return aLv();
    }

    @C3248pc(aYR = C3248pc.C3250b.ITERATOR, displayName = "Citizenship Packs")
    public List<CitizenshipPack> aLy() {
        switch (bFf().mo6893i(cPw)) {
            case 0:
                return null;
            case 2:
                return (List) bFf().mo5606d(new aCE(this, cPw, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, cPw, new Object[0]));
                break;
        }
        return aLx();
    }

    public ContentImporterExporter aMA() {
        switch (bFf().mo6893i(cQq)) {
            case 0:
                return null;
            case 2:
                return (ContentImporterExporter) bFf().mo5606d(new aCE(this, cQq, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, cQq, new Object[0]));
                break;
        }
        return aMz();
    }

    public UsersQueueManager aMC() {
        switch (bFf().mo6893i(cQr)) {
            case 0:
                return null;
            case 2:
                return (UsersQueueManager) bFf().mo5606d(new aCE(this, cQr, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, cQr, new Object[0]));
                break;
        }
        return aMB();
    }

    @C5566aOg
    public List<User> aME() {
        switch (bFf().mo6893i(cQt)) {
            case 0:
                return null;
            case 2:
                return (List) bFf().mo5606d(new aCE(this, cQt, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, cQt, new Object[0]));
                break;
        }
        return aMD();
    }

    public BotUtils aMa() {
        switch (bFf().mo6893i(cPX)) {
            case 0:
                return null;
            case 2:
                return (BotUtils) bFf().mo5606d(new aCE(this, cPX, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, cPX, new Object[0]));
                break;
        }
        return aLZ();
    }

    @C2198cg
    public Collection<Space> aMc() {
        switch (bFf().mo6893i(cPY)) {
            case 0:
                return null;
            case 2:
                return (Collection) bFf().mo5606d(new aCE(this, cPY, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, cPY, new Object[0]));
                break;
        }
        return aMb();
    }

    @C5566aOg
    public int aMe() {
        switch (bFf().mo6893i(cPZ)) {
            case 0:
                return 0;
            case 2:
                return ((Integer) bFf().mo5606d(new aCE(this, cPZ, new Object[0]))).intValue();
            case 3:
                bFf().mo5606d(new aCE(this, cPZ, new Object[0]));
                break;
        }
        return aMd();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Economy")
    @C5566aOg
    public GlobalEconomyInfo aMh() {
        switch (bFf().mo6893i(cQc)) {
            case 0:
                return null;
            case 2:
                return (GlobalEconomyInfo) bFf().mo5606d(new aCE(this, cQc, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, cQc, new Object[0]));
                break;
        }
        return aMg();
    }

    @C3248pc(aYR = C3248pc.C3250b.ITERATOR, displayName = "Warehouses")
    public List<VirtualWarehouse> aMj() {
        switch (bFf().mo6893i(cQg)) {
            case 0:
                return null;
            case 2:
                return (List) bFf().mo5606d(new aCE(this, cQg, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, cQg, new Object[0]));
                break;
        }
        return aMi();
    }

    //aMl
    public UserConnection getUserConnection() {
        switch (bFf().mo6893i(f405x20176b18)) {
            case 0:
                return null;
            case 2:
                return (UserConnection) bFf().mo5606d(new aCE(this, f405x20176b18, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f405x20176b18, new Object[0]));
                break;
        }
        return aMk();
    }

    @ClientOnly
    public void aMn() {
        switch (bFf().mo6893i(cQi)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, cQi, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, cQi, new Object[0]));
                break;
        }
        aMm();
    }

    @ClientOnly
    public C6663arL aMp() {
        switch (bFf().mo6893i(cQj)) {
            case 0:
                return null;
            case 2:
                return (C6663arL) bFf().mo5606d(new aCE(this, cQj, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, cQj, new Object[0]));
                break;
        }
        return aMo();
    }

    @C5566aOg
    @C2085br
    public void aMr() {
        switch (bFf().mo6893i(cQk)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, cQk, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, cQk, new Object[0]));
                break;
        }
        aMq();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Citizenship Office")
    public CitizenshipOffice aMw() {
        switch (bFf().mo6893i(cQn)) {
            case 0:
                return null;
            case 2:
                return (CitizenshipOffice) bFf().mo5606d(new aCE(this, cQn, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, cQn, new Object[0]));
                break;
        }
        return aMv();
    }

    public PlayerReservedNames aMy() {
        switch (bFf().mo6893i(cQp)) {
            case 0:
                return null;
            case 2:
                return (PlayerReservedNames) bFf().mo5606d(new aCE(this, cQp, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, cQp, new Object[0]));
                break;
        }
        return aMx();
    }

    public IEngineGame ald() {
        switch (bFf().mo6893i(f404x3cb2b465)) {
            case 0:
                return null;
            case 2:
                return (IEngineGame) bFf().mo5606d(new aCE(this, f404x3cb2b465, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f404x3cb2b465, new Object[0]));
                break;
        }
        return aMu();
    }

    /* renamed from: aq */
    public UUID mo18aq() {
        switch (bFf().mo6893i(f408bN)) {
            case 0:
                return null;
            case 2:
                return (UUID) bFf().mo5606d(new aCE(this, f408bN, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f408bN, new Object[0]));
                break;
        }
        return m2114ap();
    }

    /* renamed from: b */
    public User getUserBot(ClientConnect bVar, String userName, String pass) {
        switch (bFf().mo6893i(cOv)) {
            case 0:
                return null;
            case 2:
                return (User) bFf().mo5606d(new aCE(this, cOv, new Object[]{bVar, userName, pass}));
            case 3:
                bFf().mo5606d(new aCE(this, cOv, new Object[]{bVar, userName, pass}));
                break;
        }
        return m2020a(bVar, userName, pass);
    }

    @C5566aOg
    @C0909NL
    /* renamed from: b */
    public User mo1418b(String str, String str2, String str3, String str4) {
        switch (bFf().mo6893i(cMQ)) {
            case 0:
                return null;
            case 2:
                return (User) bFf().mo5606d(new aCE(this, cMQ, new Object[]{str, str2, str3, str4}));
            case 3:
                bFf().mo5606d(new aCE(this, cMQ, new Object[]{str, str2, str3, str4}));
                break;
        }
        return m2021a(str, str2, str3, str4);
    }

    @C5566aOg
    /* renamed from: b */
    public Connect.Status mo1419b(User adk, String str, String str2) {
        switch (bFf().mo6893i(cMM)) {
            case 0:
                return null;
            case 2:
                return (Connect.Status) bFf().mo5606d(new aCE(this, cMM, new Object[]{adk, str, str2}));
            case 3:
                bFf().mo5606d(new aCE(this, cMM, new Object[]{adk, str, str2}));
                break;
        }
        return m2022a(adk, str, str2);
    }

    /* renamed from: b */
    public void mo1420b(C0468GU gu) {
        switch (bFf().mo6893i(cOp)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, cOp, new Object[]{gu}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, cOp, new Object[]{gu}));
                break;
        }
        m2028a(gu);
    }

    @C3248pc(aYR = C3248pc.C3250b.ADDER, displayName = "Module Types")
    /* renamed from: b */
    public void mo1421b(ModuleType kq) {
        switch (bFf().mo6893i(cOQ)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, cOQ, new Object[]{kq}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, cOQ, new Object[]{kq}));
                break;
        }
        m2030a(kq);
    }

    @C3248pc(aYR = C3248pc.C3250b.ADDER, displayName = "BlueprintTypes")
    /* renamed from: b */
    public void mo1422b(BlueprintType mr) {
        switch (bFf().mo6893i(cNF)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, cNF, new Object[]{mr}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, cNF, new Object[]{mr}));
                break;
        }
        m2031a(mr);
    }

    @C3248pc(aYR = C3248pc.C3250b.ADDER, displayName = "MerchandiseTypes")
    /* renamed from: b */
    public void mo1423b(MerchandiseType mf) {
        switch (bFf().mo6893i(cNz)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, cNz, new Object[]{mf}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, cNz, new Object[]{mf}));
                break;
        }
        m2032a(mf);
    }

    @C3248pc(aYR = C3248pc.C3250b.ADDER, displayName = "AI Controller Types")
    /* renamed from: b */
    public void mo1424b(AIControllerType tz) {
        switch (bFf().mo6893i(cOZ)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, cOZ, new Object[]{tz}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, cOZ, new Object[]{tz}));
                break;
        }
        m2037a(tz);
    }

    @C5566aOg
    /* renamed from: b */
    public void mo1425b(User adk) {
        switch (bFf().mo6893i(cMI)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, cMI, new Object[]{adk}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, cMI, new Object[]{adk}));
                break;
        }
        m2042a(adk);
    }

    @C3248pc(aYR = C3248pc.C3250b.ADDER, displayName = "RawMaterialTypes")
    /* renamed from: b */
    public void mo1426b(RawMaterialType agy) {
        switch (bFf().mo6893i(cNX)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, cNX, new Object[]{agy}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, cNX, new Object[]{agy}));
                break;
        }
        m2043a(agy);
    }

    @C3248pc(aYR = C3248pc.C3250b.ADDER, displayName = "ShipStructureTypes")
    /* renamed from: b */
    public void mo1427b(ShipStructureType aks) {
        switch (bFf().mo6893i(cNO)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, cNO, new Object[]{aks}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, cNO, new Object[]{aks}));
                break;
        }
        m2045a(aks);
    }

    @C3248pc(aYR = C3248pc.C3250b.ADDER, displayName = "WeaponTypes")
    /* renamed from: b */
    public void mo1428b(WeaponType apt) {
        switch (bFf().mo6893i(f401Pj)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f401Pj, new Object[]{apt}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f401Pj, new Object[]{apt}));
                break;
        }
        m2049a(apt);
    }

    @C3248pc(aYR = C3248pc.C3250b.ADDER, displayName = "AttributeBuffType")
    /* renamed from: b */
    public void mo1429b(AttributeBuffType api) {
        switch (bFf().mo6893i(cOT)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, cOT, new Object[]{api}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, cOT, new Object[]{api}));
                break;
        }
        m2050a(api);
    }

    @C3248pc(aYR = C3248pc.C3250b.ADDER, displayName = "AmmoBoxTypes")
    @C5566aOg
    /* renamed from: b */
    public void mo1430b(ClipBoxType aty) {
        switch (bFf().mo6893i(cNh)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, cNh, new Object[]{aty}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, cNh, new Object[]{aty}));
                break;
        }
        m2054a(aty);
    }

    @C3248pc(aYR = C3248pc.C3250b.ADDER, displayName = "ShipSectorTypes")
    /* renamed from: b */
    public void mo1431b(ShipSectorType aux) {
        switch (bFf().mo6893i(cNL)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, cNL, new Object[]{aux}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, cNL, new Object[]{aux}));
                break;
        }
        m2055a(aux);
    }

    @C3248pc(aYR = C3248pc.C3250b.ADDER, displayName = "Mission Templates")
    @C5566aOg
    /* renamed from: b */
    public void mo1432b(MissionTemplate avh) {
        switch (bFf().mo6893i(cMS)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, cMS, new Object[]{avh}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, cMS, new Object[]{avh}));
                break;
        }
        m2056a(avh);
    }

    @C3248pc(aYR = C3248pc.C3250b.ADDER, displayName = "Citizenship Packs")
    @C5566aOg
    /* renamed from: b */
    public void mo1433b(CitizenshipPack acu) {
        switch (bFf().mo6893i(cPu)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, cPu, new Object[]{acu}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, cPu, new Object[]{acu}));
                break;
        }
        m2062a(acu);
    }

    @C3248pc(aYR = C3248pc.C3250b.ADDER, displayName = "Taikopedia Entries")
    @C5566aOg
    /* renamed from: b */
    public void mo1434b(TaikopediaEntry aiz) {
        switch (bFf().mo6893i(aMl)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, aMl, new Object[]{aiz}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, aMl, new Object[]{aiz}));
                break;
        }
        m2063a(aiz);
    }

    @C3248pc(aYR = C3248pc.C3250b.ADDER, displayName = "Tutorials")
    @C5566aOg
    /* renamed from: b */
    public void mo1435b(Tutorial ajv) {
        switch (bFf().mo6893i(cOk)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, cOk, new Object[]{ajv}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, cOk, new Object[]{ajv}));
                break;
        }
        m2064a(ajv);
    }

    @C5566aOg
    @C2499fr
    /* renamed from: b */
    public void mo1436b(Player aku, ShipType ng, WeaponType apt) {
        switch (bFf().mo6893i(cOM)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, cOM, new Object[]{aku, ng, apt}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, cOM, new Object[]{aku, ng, apt}));
                break;
        }
        m2065a(aku, ng, apt);
    }

    @C5566aOg
    @C2499fr
    /* renamed from: b */
    public void mo1437b(Player aku, ShipType ng, List<WeaponType> list) {
        switch (bFf().mo6893i(cON)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, cON, new Object[]{aku, ng, list}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, cON, new Object[]{aku, ng, list}));
                break;
        }
        m2066a(aku, ng, list);
    }

    /* renamed from: b */
    public void mo625b(C6417amZ amz) {
        switch (bFf().mo6893i(coL)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, coL, new Object[]{amz}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, coL, new Object[]{amz}));
                break;
        }
        m2067a(amz);
    }

    /* renamed from: b */
    public void mo1438b(UserConnection apk) {
        switch (bFf().mo6893i(cQh)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, cQh, new Object[]{apk}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, cQh, new Object[]{apk}));
                break;
        }
        m2068a(apk);
    }

    @C3248pc(aYR = C3248pc.C3250b.ADDER, displayName = "Fee Managers")
    /* renamed from: b */
    public void mo1439b(ConsignmentFeeManager auVar) {
        switch (bFf().mo6893i(cPc)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, cPc, new Object[]{auVar}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, cPc, new Object[]{auVar}));
                break;
        }
        m2070a(auVar);
    }

    @C3248pc(aYR = C3248pc.C3250b.ADDER, displayName = "EquippedShipTypes")
    @C5566aOg
    /* renamed from: b */
    public void mo1440b(EquippedShipType gfVar) {
        switch (bFf().mo6893i(cMV)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, cMV, new Object[]{gfVar}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, cMV, new Object[]{gfVar}));
                break;
        }
        m2073a(gfVar);
    }

    @C3248pc(aYR = C3248pc.C3250b.ADDER, displayName = "Citizenship Reward")
    @C5566aOg
    /* renamed from: b */
    public void mo1441b(CitizenshipReward lQVar) {
        switch (bFf().mo6893i(cPx)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, cPx, new Object[]{lQVar}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, cPx, new Object[]{lQVar}));
                break;
        }
        m2076a(lQVar);
    }

    @C3248pc(aYR = C3248pc.C3250b.ADDER, displayName = "Asteroid Zone Categories")
    @C5566aOg
    /* renamed from: b */
    public void mo1442b(AsteroidZoneCategory nWVar) {
        switch (bFf().mo6893i(cPD)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, cPD, new Object[]{nWVar}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, cPD, new Object[]{nWVar}));
                break;
        }
        m2078a(nWVar);
    }

    @C5566aOg
    /* renamed from: b */
    public boolean mo1443b(User adk, String str) {
        switch (bFf().mo6893i(cQs)) {
            case 0:
                return false;
            case 2:
                return ((Boolean) bFf().mo5606d(new aCE(this, cQs, new Object[]{adk, str}))).booleanValue();
            case 3:
                bFf().mo5606d(new aCE(this, cQs, new Object[]{adk, str}));
                break;
        }
        return m2082a(adk, str);
    }

    /* renamed from: c */
    public Object mo1444c(int i, Object obj) {
        switch (bFf().mo6893i(cOL)) {
            case 0:
                return null;
            case 2:
                return bFf().mo5606d(new aCE(this, cOL, new Object[]{new Integer(i), obj}));
            case 3:
                bFf().mo5606d(new aCE(this, cOL, new Object[]{new Integer(i), obj}));
                break;
        }
        return m2128b(i, obj);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Avatar Manager")
    @C5566aOg
    /* renamed from: c */
    public void mo1445c(AvatarManager dq) {
        switch (bFf().mo6893i(cOG)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, cOG, new Object[]{dq}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, cOG, new Object[]{dq}));
                break;
        }
        m2130b(dq);
    }

    @C3248pc(aYR = C3248pc.C3250b.ADDER, displayName = "Citizenship Improvements Types")
    @C5566aOg
    /* renamed from: c */
    public void mo1446c(CitizenImprovementType kt) {
        switch (bFf().mo6893i(cPJ)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, cPJ, new Object[]{kt}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, cPJ, new Object[]{kt}));
                break;
        }
        m2131b(kt);
    }

    @C3248pc(aYR = C3248pc.C3250b.ADDER, displayName = "Cruise Speed Types")
    @C5566aOg
    /* renamed from: c */
    public void mo1447c(CruiseSpeedType nf) {
        switch (bFf().mo6893i(cPo)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, cPo, new Object[]{nf}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, cPo, new Object[]{nf}));
                break;
        }
        m2132b(nf);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Market")
    @C5566aOg
    /* renamed from: c */
    public void mo1448c(Market oi) {
        switch (bFf().mo6893i(cQb)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, cQb, new Object[]{oi}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, cQb, new Object[]{oi}));
                break;
        }
        m2133b(oi);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Citizenship Office")
    @C5566aOg
    /* renamed from: c */
    public void mo1449c(CitizenshipOffice sk) {
        switch (bFf().mo6893i(cQo)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, cQo, new Object[]{sk}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, cQo, new Object[]{sk}));
                break;
        }
        m2134b(sk);
    }

    @C3248pc(aYR = C3248pc.C3250b.ADDER, displayName = "LootTypes")
    /* renamed from: c */
    public void mo1450c(LootType ahc) {
        switch (bFf().mo6893i(cOf)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, cOf, new Object[]{ahc}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, cOf, new Object[]{ahc}));
                break;
        }
        m2135b(ahc);
    }

    @C3248pc(aYR = C3248pc.C3250b.ADDER, displayName = "Warehouses")
    @C5566aOg
    /* renamed from: c */
    public void mo1451c(VirtualWarehouse aor) {
        switch (bFf().mo6893i(cQe)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, cQe, new Object[]{aor}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, cQe, new Object[]{aor}));
                break;
        }
        m2136b(aor);
    }

    @C3248pc(aYR = C3248pc.C3250b.ADDER, displayName = "Citizenship Types")
    @C5566aOg
    /* renamed from: c */
    public void mo1452c(CitizenshipType adl) {
        switch (bFf().mo6893i(cPr)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, cPr, new Object[]{adl}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, cPr, new Object[]{adl}));
                break;
        }
        m2138b(adl);
    }

    @C3248pc(aYR = C3248pc.C3250b.ADDER, displayName = "ShieldTypes")
    /* renamed from: c */
    public void mo1453c(ShieldType aph) {
        switch (bFf().mo6893i(cNt)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, cNt, new Object[]{aph}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, cNt, new Object[]{aph}));
                break;
        }
        m2139b(aph);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "ContractBoard")
    @C5566aOg
    /* renamed from: c */
    public void mo1454c(ContractBoard hCVar) {
        switch (bFf().mo6893i(cPR)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, cPR, new Object[]{hCVar}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, cPR, new Object[]{hCVar}));
                break;
        }
        m2140b(hCVar);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Economy")
    @C5566aOg
    /* renamed from: c */
    public void mo1455c(GlobalEconomyInfo mKVar) {
        switch (bFf().mo6893i(cQd)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, cQd, new Object[]{mKVar}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, cQd, new Object[]{mKVar}));
                break;
        }
        m2141b(mKVar);
    }

    @C3248pc(aYR = C3248pc.C3250b.ADDER, displayName = "HullTypes")
    /* renamed from: c */
    public void mo1456c(HullType wHVar) {
        switch (bFf().mo6893i(cNq)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, cNq, new Object[]{wHVar}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, cNq, new Object[]{wHVar}));
                break;
        }
        m2143b(wHVar);
    }

    @C3248pc(aYR = C3248pc.C3250b.ADDER, displayName = "Collision Sets")
    @C5566aOg
    /* renamed from: c */
    public void mo1457c(CollisionFXSet yaVar) {
        switch (bFf().mo6893i(cOI)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, cOI, new Object[]{yaVar}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, cOI, new Object[]{yaVar}));
                break;
        }
        m2144b(yaVar);
    }

    /* renamed from: c */
    public C5663aRz[] mo25c() {
        return _m_fields;
    }

    /* renamed from: cD */
    public List<ItemType> mo1458cD(boolean z) {
        switch (bFf().mo6893i(cOq)) {
            case 0:
                return null;
            case 2:
                return (List) bFf().mo5606d(new aCE(this, cOq, new Object[]{new Boolean(z)}));
            case 3:
                bFf().mo5606d(new aCE(this, cOq, new Object[]{new Boolean(z)}));
                break;
        }
        return m2170cC(z);
    }

    @C5566aOg
    @C0909NL
    /* renamed from: d */
    public User mo1459d(String str, String str2, String str3) {
        switch (bFf().mo6893i(cMP)) {
            case 0:
                return null;
            case 2:
                return (User) bFf().mo5606d(new aCE(this, cMP, new Object[]{str, str2, str3}));
            case 3:
                bFf().mo5606d(new aCE(this, cMP, new Object[]{str, str2, str3}));
                break;
        }
        return m2146c(str, str2, str3);
    }

    @C3248pc(aYR = C3248pc.C3250b.ADDER, displayName = "AdvertiseTypes")
    /* renamed from: d */
    public void mo1460d(AdvertiseType kp) {
        switch (bFf().mo6893i(cOx)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, cOx, new Object[]{kp}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, cOx, new Object[]{kp}));
                break;
        }
        m2147c(kp);
    }

    @C3248pc(aYR = C3248pc.C3250b.REMOVER, displayName = "Module Types")
    /* renamed from: d */
    public void mo1461d(ModuleType kq) {
        switch (bFf().mo6893i(cOR)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, cOR, new Object[]{kq}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, cOR, new Object[]{kq}));
                break;
        }
        m2148c(kq);
    }

    @C3248pc(aYR = C3248pc.C3250b.REMOVER, displayName = "BlueprintTypes")
    /* renamed from: d */
    public void mo1462d(BlueprintType mr) {
        switch (bFf().mo6893i(cNG)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, cNG, new Object[]{mr}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, cNG, new Object[]{mr}));
                break;
        }
        m2149c(mr);
    }

    @C3248pc(aYR = C3248pc.C3250b.REMOVER, displayName = "MerchandiseTypes")
    /* renamed from: d */
    public void mo1463d(MerchandiseType mf) {
        switch (bFf().mo6893i(cNA)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, cNA, new Object[]{mf}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, cNA, new Object[]{mf}));
                break;
        }
        m2150c(mf);
    }

    @C3248pc(aYR = C3248pc.C3250b.REMOVER, displayName = "AI Controller Types")
    /* renamed from: d */
    public void mo1464d(AIControllerType tz) {
        switch (bFf().mo6893i(cPa)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, cPa, new Object[]{tz}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, cPa, new Object[]{tz}));
                break;
        }
        m2151c(tz);
    }

    @C5566aOg
    /* renamed from: d */
    public void mo1465d(User adk) {
        switch (bFf().mo6893i(cMJ)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, cMJ, new Object[]{adk}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, cMJ, new Object[]{adk}));
                break;
        }
        m2152c(adk);
    }

    @C3248pc(aYR = C3248pc.C3250b.REMOVER, displayName = "RawMaterialTypes")
    /* renamed from: d */
    public void mo1466d(RawMaterialType agy) {
        switch (bFf().mo6893i(cNY)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, cNY, new Object[]{agy}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, cNY, new Object[]{agy}));
                break;
        }
        m2153c(agy);
    }

    @C3248pc(aYR = C3248pc.C3250b.REMOVER, displayName = "ShipStructureTypes")
    /* renamed from: d */
    public void mo1467d(ShipStructureType aks) {
        switch (bFf().mo6893i(cNP)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, cNP, new Object[]{aks}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, cNP, new Object[]{aks}));
                break;
        }
        m2154c(aks);
    }

    @C3248pc(aYR = C3248pc.C3250b.REMOVER, displayName = "WeaponTypes")
    /* renamed from: d */
    public void mo1468d(WeaponType apt) {
        switch (bFf().mo6893i(f402Pk)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f402Pk, new Object[]{apt}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f402Pk, new Object[]{apt}));
                break;
        }
        m2155c(apt);
    }

    @C3248pc(aYR = C3248pc.C3250b.REMOVER, displayName = "AttributeBuffType")
    /* renamed from: d */
    public void mo1469d(AttributeBuffType api) {
        switch (bFf().mo6893i(cOU)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, cOU, new Object[]{api}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, cOU, new Object[]{api}));
                break;
        }
        m2156c(api);
    }

    @C3248pc(aYR = C3248pc.C3250b.ADDER, displayName = "CargoHoldType")
    /* renamed from: d */
    public void mo1470d(CargoHoldType arb) {
        switch (bFf().mo6893i(cNl)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, cNl, new Object[]{arb}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, cNl, new Object[]{arb}));
                break;
        }
        m2157c(arb);
    }

    @C3248pc(aYR = C3248pc.C3250b.REMOVER, displayName = "AmmoBoxTypes")
    @C5566aOg
    /* renamed from: d */
    public void mo1471d(ClipBoxType aty) {
        switch (bFf().mo6893i(cNi)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, cNi, new Object[]{aty}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, cNi, new Object[]{aty}));
                break;
        }
        m2158c(aty);
    }

    @C3248pc(aYR = C3248pc.C3250b.REMOVER, displayName = "ShipSectorTypes")
    /* renamed from: d */
    public void mo1472d(ShipSectorType aux) {
        switch (bFf().mo6893i(cNM)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, cNM, new Object[]{aux}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, cNM, new Object[]{aux}));
                break;
        }
        m2159c(aux);
    }

    @C3248pc(aYR = C3248pc.C3250b.REMOVER, displayName = "Mission Templates")
    @C5566aOg
    /* renamed from: d */
    public void mo1473d(MissionTemplate avh) {
        switch (bFf().mo6893i(cMT)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, cMT, new Object[]{avh}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, cMT, new Object[]{avh}));
                break;
        }
        m2160c(avh);
    }

    @C3248pc(aYR = C3248pc.C3250b.REMOVER, displayName = "Citizenship Packs")
    @C5566aOg
    /* renamed from: d */
    public void mo1474d(CitizenshipPack acu) {
        switch (bFf().mo6893i(cPv)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, cPv, new Object[]{acu}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, cPv, new Object[]{acu}));
                break;
        }
        m2161c(acu);
    }

    @C3248pc(aYR = C3248pc.C3250b.REMOVER, displayName = "Taikopedia Entries")
    @C5566aOg
    /* renamed from: d */
    public void mo1475d(TaikopediaEntry aiz) {
        switch (bFf().mo6893i(aMm)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, aMm, new Object[]{aiz}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, aMm, new Object[]{aiz}));
                break;
        }
        m2162c(aiz);
    }

    @C3248pc(aYR = C3248pc.C3250b.REMOVER, displayName = "Tutorials")
    @C5566aOg
    /* renamed from: d */
    public void mo1476d(Tutorial ajv) {
        switch (bFf().mo6893i(cOl)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, cOl, new Object[]{ajv}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, cOl, new Object[]{ajv}));
                break;
        }
        m2163c(ajv);
    }

    @C3248pc(aYR = C3248pc.C3250b.REMOVER, displayName = "Fee Managers")
    /* renamed from: d */
    public void mo1477d(ConsignmentFeeManager auVar) {
        switch (bFf().mo6893i(cPd)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, cPd, new Object[]{auVar}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, cPd, new Object[]{auVar}));
                break;
        }
        m2164c(auVar);
    }

    @C3248pc(aYR = C3248pc.C3250b.ADDER, displayName = "Sector Categories")
    /* renamed from: d */
    public void mo1478d(SectorCategory fMVar) {
        switch (bFf().mo6893i(cPf)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, cPf, new Object[]{fMVar}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, cPf, new Object[]{fMVar}));
                break;
        }
        m2165c(fMVar);
    }

    @C3248pc(aYR = C3248pc.C3250b.REMOVER, displayName = "EquippedShipTypes")
    @C5566aOg
    /* renamed from: d */
    public void mo1479d(EquippedShipType gfVar) {
        switch (bFf().mo6893i(cMW)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, cMW, new Object[]{gfVar}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, cMW, new Object[]{gfVar}));
                break;
        }
        m2166c(gfVar);
    }

    @C3248pc(aYR = C3248pc.C3250b.REMOVER, displayName = "Citizenship Reward")
    @C5566aOg
    /* renamed from: d */
    public void mo1480d(CitizenshipReward lQVar) {
        switch (bFf().mo6893i(cPy)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, cPy, new Object[]{lQVar}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, cPy, new Object[]{lQVar}));
                break;
        }
        m2167c(lQVar);
    }

    @C3248pc(aYR = C3248pc.C3250b.REMOVER, displayName = "Asteroid Zone Categories")
    @C5566aOg
    /* renamed from: d */
    public void mo1481d(AsteroidZoneCategory nWVar) {
        switch (bFf().mo6893i(cPE)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, cPE, new Object[]{nWVar}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, cPE, new Object[]{nWVar}));
                break;
        }
        m2168c(nWVar);
    }

    @C3248pc(aYR = C3248pc.C3250b.ADDER, displayName = "Space Categories")
    @C5566aOg
    /* renamed from: e */
    public void mo1482e(SpaceCategory ak) {
        switch (bFf().mo6893i(cPG)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, cPG, new Object[]{ak}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, cPG, new Object[]{ak}));
                break;
        }
        m2171d(ak);
    }

    @C3248pc(aYR = C3248pc.C3250b.REMOVER, displayName = "Citizenship Improvements Types")
    @C5566aOg
    /* renamed from: e */
    public void mo1483e(CitizenImprovementType kt) {
        switch (bFf().mo6893i(cPK)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, cPK, new Object[]{kt}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, cPK, new Object[]{kt}));
                break;
        }
        m2172d(kt);
    }

    @C3248pc(aYR = C3248pc.C3250b.REMOVER, displayName = "Cruise Speed Types")
    @C5566aOg
    /* renamed from: e */
    public void mo1484e(CruiseSpeedType nf) {
        switch (bFf().mo6893i(cPp)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, cPp, new Object[]{nf}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, cPp, new Object[]{nf}));
                break;
        }
        m2173d(nf);
    }

    @C3248pc(aYR = C3248pc.C3250b.REMOVER, displayName = "LootTypes")
    /* renamed from: e */
    public void mo1485e(LootType ahc) {
        switch (bFf().mo6893i(cOg)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, cOg, new Object[]{ahc}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, cOg, new Object[]{ahc}));
                break;
        }
        m2174d(ahc);
    }

    @C3248pc(aYR = C3248pc.C3250b.REMOVER, displayName = "Warehouses")
    @C5566aOg
    /* renamed from: e */
    public void mo1486e(VirtualWarehouse aor) {
        switch (bFf().mo6893i(cQf)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, cQf, new Object[]{aor}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, cQf, new Object[]{aor}));
                break;
        }
        m2175d(aor);
    }

    @C3248pc(aYR = C3248pc.C3250b.ADDER, displayName = "AsteroidTypes")
    /* renamed from: e */
    public void mo1487e(AsteroidType aqn) {
        switch (bFf().mo6893i(cOA)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, cOA, new Object[]{aqn}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, cOA, new Object[]{aqn}));
                break;
        }
        m2176d(aqn);
    }

    @C3248pc(aYR = C3248pc.C3250b.ADDER, displayName = "BagTypes")
    /* renamed from: e */
    public void mo1488e(BagItemType ato) {
        switch (bFf().mo6893i(cNC)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, cNC, new Object[]{ato}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, cNC, new Object[]{ato}));
                break;
        }
        m2177d(ato);
    }

    @C3248pc(aYR = C3248pc.C3250b.REMOVER, displayName = "Citizenship Types")
    @C5566aOg
    /* renamed from: e */
    public void mo1489e(CitizenshipType adl) {
        switch (bFf().mo6893i(cPs)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, cPs, new Object[]{adl}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, cPs, new Object[]{adl}));
                break;
        }
        m2178d(adl);
    }

    @C3248pc(aYR = C3248pc.C3250b.ADDER, displayName = "Database Categories")
    @C5566aOg
    /* renamed from: e */
    public void mo1490e(DatabaseCategory aik) {
        switch (bFf().mo6893i(cPA)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, cPA, new Object[]{aik}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, cPA, new Object[]{aik}));
                break;
        }
        m2179d(aik);
    }

    @C3248pc(aYR = C3248pc.C3250b.ADDER, displayName = "TurretTypes")
    @C5566aOg
    /* renamed from: e */
    public void mo1491e(C6544aow aow) {
        switch (bFf().mo6893i(cOW)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, cOW, new Object[]{aow}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, cOW, new Object[]{aow}));
                break;
        }
        m2180d(aow);
    }

    @C3248pc(aYR = C3248pc.C3250b.REMOVER, displayName = "ShieldTypes")
    /* renamed from: e */
    public void mo1492e(ShieldType aph) {
        switch (bFf().mo6893i(cNu)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, cNu, new Object[]{aph}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, cNu, new Object[]{aph}));
                break;
        }
        m2181d(aph);
    }

    @C3248pc(aYR = C3248pc.C3250b.REMOVER, displayName = "HullTypes")
    /* renamed from: e */
    public void mo1493e(HullType wHVar) {
        switch (bFf().mo6893i(cNr)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, cNr, new Object[]{wHVar}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, cNr, new Object[]{wHVar}));
                break;
        }
        m2182d(wHVar);
    }

    @C3248pc(aYR = C3248pc.C3250b.REMOVER, displayName = "Collision Sets")
    @C5566aOg
    /* renamed from: e */
    public void mo1494e(CollisionFXSet yaVar) {
        switch (bFf().mo6893i(cOJ)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, cOJ, new Object[]{yaVar}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, cOJ, new Object[]{yaVar}));
                break;
        }
        m2183d(yaVar);
    }

    @C5566aOg
    @C2499fr
    /* renamed from: ec */
    public Player mo1495ec(String str) {
        switch (bFf().mo6893i(cME)) {
            case 0:
                return null;
            case 2:
                return (Player) bFf().mo5606d(new aCE(this, cME, new Object[]{str}));
            case 3:
                bFf().mo5606d(new aCE(this, cME, new Object[]{str}));
                break;
        }
        return m2192eb(str);
    }

    @C5566aOg
    @C2499fr
    /* renamed from: ee */
    public Player mo1496ee(String str) {
        switch (bFf().mo6893i(cMH)) {
            case 0:
                return null;
            case 2:
                return (Player) bFf().mo5606d(new aCE(this, cMH, new Object[]{str}));
            case 3:
                bFf().mo5606d(new aCE(this, cMH, new Object[]{str}));
                break;
        }
        return m2193ed(str);
    }

    @C5566aOg
    /* renamed from: eg */
    public User mo1497eg(String login) {
        switch (bFf().mo6893i(cMO)) {
            case 0:
                return null;
            case 2:
                return (User) bFf().mo5606d(new aCE(this, cMO, new Object[]{login}));
            case 3:
                bFf().mo5606d(new aCE(this, cMO, new Object[]{login}));
                break;
        }
        return m2194ef(login);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Forbidden Names (comma separated)")
    @C5566aOg
    /* renamed from: ei */
    public void mo1498ei(String str) {
        switch (bFf().mo6893i(cPj)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, cPj, new Object[]{str}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, cPj, new Object[]{str}));
                break;
        }
        m2195eh(str);
    }

    @C5566aOg
    /* renamed from: ek */
    public DamageType mo1499ek(String str) {
        switch (bFf().mo6893i(cPn)) {
            case 0:
                return null;
            case 2:
                return (DamageType) bFf().mo5606d(new aCE(this, cPn, new Object[]{str}));
            case 3:
                bFf().mo5606d(new aCE(this, cPn, new Object[]{str}));
                break;
        }
        return m2196ej(str);
    }

    @C5566aOg
    /* renamed from: f */
    public int mo1500f(User adk) {
        switch (bFf().mo6893i(cMN)) {
            case 0:
                return 0;
            case 2:
                return ((Integer) bFf().mo5606d(new aCE(this, cMN, new Object[]{adk}))).intValue();
            case 3:
                bFf().mo5606d(new aCE(this, cMN, new Object[]{adk}));
                break;
        }
        return m2184e(adk);
    }

    @C3248pc(aYR = C3248pc.C3250b.REMOVER, displayName = "AdvertiseTypes")
    /* renamed from: f */
    public void mo1501f(AdvertiseType kp) {
        switch (bFf().mo6893i(cOy)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, cOy, new Object[]{kp}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, cOy, new Object[]{kp}));
                break;
        }
        m2185e(kp);
    }

    @C3248pc(aYR = C3248pc.C3250b.ADDER, displayName = "ItemGenSets")
    /* renamed from: f */
    public void mo1502f(ItemGenSet alq) {
        switch (bFf().mo6893i(cOd)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, cOd, new Object[]{alq}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, cOd, new Object[]{alq}));
                break;
        }
        m2186e(alq);
    }

    @C5566aOg
    /* renamed from: f */
    public void mo1503f(Corporation aPVar) {
        switch (bFf().mo6893i(cPM)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, cPM, new Object[]{aPVar}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, cPM, new Object[]{aPVar}));
                break;
        }
        m2187e(aPVar);
    }

    @C3248pc(aYR = C3248pc.C3250b.ADDER, displayName = "AmmoTypes")
    @C5566aOg
    /* renamed from: f */
    public void mo1504f(ClipType aqf) {
        switch (bFf().mo6893i(cNe)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, cNe, new Object[]{aqf}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, cNe, new Object[]{aqf}));
                break;
        }
        m2188e(aqf);
    }

    @C3248pc(aYR = C3248pc.C3250b.REMOVER, displayName = "CargoHoldType")
    /* renamed from: f */
    public void mo1505f(CargoHoldType arb) {
        switch (bFf().mo6893i(cNm)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, cNm, new Object[]{arb}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, cNm, new Object[]{arb}));
                break;
        }
        m2189e(arb);
    }

    @C3248pc(aYR = C3248pc.C3250b.ADDER, displayName = "SceneryTypes")
    /* renamed from: f */
    public void mo1506f(SceneryType atc) {
        switch (bFf().mo6893i(cNJ)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, cNJ, new Object[]{atc}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, cNJ, new Object[]{atc}));
                break;
        }
        m2190e(atc);
    }

    @C3248pc(aYR = C3248pc.C3250b.REMOVER, displayName = "Sector Categories")
    /* renamed from: f */
    public void mo1507f(SectorCategory fMVar) {
        switch (bFf().mo6893i(cPg)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, cPg, new Object[]{fMVar}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, cPg, new Object[]{fMVar}));
                break;
        }
        m2191e(fMVar);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Market")
    /* renamed from: fi */
    public Market mo1508fi() {
        switch (bFf().mo6893i(f412lw)) {
            case 0:
                return null;
            case 2:
                return (Market) bFf().mo5606d(new aCE(this, f412lw, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f412lw, new Object[0]));
                break;
        }
        return m2205fh();
    }

    @C3248pc(aYR = C3248pc.C3250b.REMOVER, displayName = "Space Categories")
    @C5566aOg
    /* renamed from: g */
    public void mo1509g(SpaceCategory ak) {
        switch (bFf().mo6893i(cPH)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, cPH, new Object[]{ak}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, cPH, new Object[]{ak}));
                break;
        }
        m2197f(ak);
    }

    @C3248pc(aYR = C3248pc.C3250b.ADDER, displayName = "StationTypes")
    /* renamed from: g */
    public void mo1510g(StationType mx) {
        switch (bFf().mo6893i(cNU)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, cNU, new Object[]{mx}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, cNU, new Object[]{mx}));
                break;
        }
        m2198f(mx);
    }

    @C3248pc(aYR = C3248pc.C3250b.REMOVER, displayName = "AsteroidTypes")
    /* renamed from: g */
    public void mo1511g(AsteroidType aqn) {
        switch (bFf().mo6893i(cOB)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, cOB, new Object[]{aqn}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, cOB, new Object[]{aqn}));
                break;
        }
        m2199f(aqn);
    }

    @C3248pc(aYR = C3248pc.C3250b.REMOVER, displayName = "BagTypes")
    /* renamed from: g */
    public void mo1512g(BagItemType ato) {
        switch (bFf().mo6893i(cND)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, cND, new Object[]{ato}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, cND, new Object[]{ato}));
                break;
        }
        m2200f(ato);
    }

    @C3248pc(aYR = C3248pc.C3250b.REMOVER, displayName = "Database Categories")
    @C5566aOg
    /* renamed from: g */
    public void mo1513g(DatabaseCategory aik) {
        switch (bFf().mo6893i(cPB)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, cPB, new Object[]{aik}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, cPB, new Object[]{aik}));
                break;
        }
        m2201f(aik);
    }

    @C3248pc(aYR = C3248pc.C3250b.REMOVER, displayName = "TurretTypes")
    @C5566aOg
    /* renamed from: g */
    public void mo1514g(C6544aow aow) {
        switch (bFf().mo6893i(cOX)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, cOX, new Object[]{aow}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, cOX, new Object[]{aow}));
                break;
        }
        m2202f(aow);
    }

    @C3248pc(aYR = C3248pc.C3250b.ADDER, displayName = "HazardAreaTypes")
    /* renamed from: g */
    public void mo1515g(HazardAreaType ass) {
        switch (bFf().mo6893i(cNw)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, cNw, new Object[]{ass}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, cNw, new Object[]{ass}));
                break;
        }
        m2203f(ass);
    }

    @C3248pc(aYR = C3248pc.C3250b.ADDER, displayName = "Amplifier Types")
    /* renamed from: g */
    public void mo1516g(AmplifierType iDVar) {
        switch (bFf().mo6893i(cOO)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, cOO, new Object[]{iDVar}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, cOO, new Object[]{iDVar}));
                break;
        }
        m2204f(iDVar);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Advertise Path")
    @C5566aOg
    /* renamed from: gM */
    public void mo1517gM(I18NString i18NString) {
        switch (bFf().mo6893i(cOE)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, cOE, new Object[]{i18NString}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, cOE, new Object[]{i18NString}));
                break;
        }
        m2212gL(i18NString);
    }

    @C5566aOg
    /* renamed from: gO */
    public void mo1518gO(I18NString i18NString) {
        switch (bFf().mo6893i(cOH)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, cOH, new Object[]{i18NString}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, cOH, new Object[]{i18NString}));
                break;
        }
        m2213gN(i18NString);
    }

    public String getHandle() {
        switch (bFf().mo6893i(f410bP)) {
            case 0:
                return null;
            case 2:
                return (String) bFf().mo5606d(new aCE(this, f410bP, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f410bP, new Object[0]));
                break;
        }
        return m2117ar();
    }

    /* renamed from: h */
    public void mo1519h(User adk) {
        switch (bFf().mo6893i(cOr)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, cOr, new Object[]{adk}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, cOr, new Object[]{adk}));
                break;
        }
        m2206g(adk);
    }

    @C3248pc(aYR = C3248pc.C3250b.REMOVER, displayName = "ItemGenSets")
    /* renamed from: h */
    public void mo1520h(ItemGenSet alq) {
        switch (bFf().mo6893i(cOe)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, cOe, new Object[]{alq}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, cOe, new Object[]{alq}));
                break;
        }
        m2207g(alq);
    }

    @C3248pc(aYR = C3248pc.C3250b.REMOVER, displayName = "AmmoTypes")
    @C5566aOg
    /* renamed from: h */
    public void mo1521h(ClipType aqf) {
        switch (bFf().mo6893i(cNf)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, cNf, new Object[]{aqf}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, cNf, new Object[]{aqf}));
                break;
        }
        m2208g(aqf);
    }

    @C3248pc(aYR = C3248pc.C3250b.REMOVER, displayName = "SceneryTypes")
    /* renamed from: h */
    public void mo1522h(SceneryType atc) {
        switch (bFf().mo6893i(cNK)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, cNK, new Object[]{atc}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, cNK, new Object[]{atc}));
                break;
        }
        m2209g(atc);
    }

    @C5566aOg
    /* renamed from: h */
    public boolean mo1523h(Corporation aPVar) {
        switch (bFf().mo6893i(cPN)) {
            case 0:
                return false;
            case 2:
                return ((Boolean) bFf().mo5606d(new aCE(this, cPN, new Object[]{aPVar}))).booleanValue();
            case 3:
                bFf().mo5606d(new aCE(this, cPN, new Object[]{aPVar}));
                break;
        }
        return m2210g(aPVar);
    }

    @C3248pc(aYR = C3248pc.C3250b.REMOVER, displayName = "StationTypes")
    /* renamed from: i */
    public void mo1524i(StationType mx) {
        switch (bFf().mo6893i(cNV)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, cNV, new Object[]{mx}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, cNV, new Object[]{mx}));
                break;
        }
        m2218h(mx);
    }

    @C3248pc(aYR = C3248pc.C3250b.ADDER, displayName = "Factions")
    @C5566aOg
    /* renamed from: i */
    public void mo1525i(Faction xm) {
        switch (bFf().mo6893i(aQo)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, aQo, new Object[]{xm}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, aQo, new Object[]{xm}));
                break;
        }
        m2219h(xm);
    }

    @C3248pc(aYR = C3248pc.C3250b.REMOVER, displayName = "HazardAreaTypes")
    /* renamed from: i */
    public void mo1526i(HazardAreaType ass) {
        switch (bFf().mo6893i(cNx)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, cNx, new Object[]{ass}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, cNx, new Object[]{ass}));
                break;
        }
        m2220h(ass);
    }

    @C3248pc(aYR = C3248pc.C3250b.REMOVER, displayName = "Amplifier Types")
    /* renamed from: i */
    public void mo1527i(AmplifierType iDVar) {
        switch (bFf().mo6893i(cOP)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, cOP, new Object[]{iDVar}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, cOP, new Object[]{iDVar}));
                break;
        }
        m2221h(iDVar);
    }

    @C3248pc(aYR = C3248pc.C3250b.ADDER, displayName = "GateTypes")
    /* renamed from: i */
    public void mo1528i(GateType mxVar) {
        switch (bFf().mo6893i(cNn)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, cNn, new Object[]{mxVar}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, cNn, new Object[]{mxVar}));
                break;
        }
        m2222h(mxVar);
    }

    /* access modifiers changed from: protected */
    public void init() {
        switch (bFf().mo6893i(f413yH)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f413yH, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f413yH, new Object[0]));
                break;
        }
        m2230jF();
    }

    public boolean isAlive() {
        switch (bFf().mo6893i(coN)) {
            case 0:
                return false;
            case 2:
                return ((Boolean) bFf().mo5606d(new aCE(this, coN, new Object[0]))).booleanValue();
            case 3:
                bFf().mo5606d(new aCE(this, coN, new Object[0]));
                break;
        }
        return azS();
    }

    @C3248pc(aYR = C3248pc.C3250b.ADDER, displayName = "StellarSystems")
    /* renamed from: j */
    public void mo1531j(StellarSystem jj) {
        switch (bFf().mo6893i(cNb)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, cNb, new Object[]{jj}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, cNb, new Object[]{jj}));
                break;
        }
        m2223i(jj);
    }

    @C3248pc(aYR = C3248pc.C3250b.ADDER, displayName = "ShipTypes")
    /* renamed from: j */
    public void mo1532j(ShipType ng) {
        switch (bFf().mo6893i(cNR)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, cNR, new Object[]{ng}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, cNR, new Object[]{ng}));
                break;
        }
        m2224i(ng);
    }

    @C3248pc(aYR = C3248pc.C3250b.REMOVER, displayName = "Factions")
    @C5566aOg
    /* renamed from: k */
    public void mo1533k(Faction xm) {
        switch (bFf().mo6893i(aQp)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, aQp, new Object[]{xm}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, aQp, new Object[]{xm}));
                break;
        }
        m2226j(xm);
    }

    @C3248pc(aYR = C3248pc.C3250b.ADDER, displayName = "ItemTypeCategories")
    /* renamed from: k */
    public void mo1534k(ItemTypeCategory aai) {
        switch (bFf().mo6893i(cOa)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, cOa, new Object[]{aai}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, cOa, new Object[]{aai}));
                break;
        }
        m2227j(aai);
    }

    @C3248pc(aYR = C3248pc.C3250b.ADDER, displayName = "NPCTypes")
    @C5566aOg
    /* renamed from: k */
    public void mo1535k(NPCType aed) {
        switch (bFf().mo6893i(cMY)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, cMY, new Object[]{aed}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, cMY, new Object[]{aed}));
                break;
        }
        m2228j(aed);
    }

    @C3248pc(aYR = C3248pc.C3250b.REMOVER, displayName = "GateTypes")
    /* renamed from: k */
    public void mo1536k(GateType mxVar) {
        switch (bFf().mo6893i(cNo)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, cNo, new Object[]{mxVar}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, cNo, new Object[]{mxVar}));
                break;
        }
        m2229j(mxVar);
    }

    @C3248pc(aYR = C3248pc.C3250b.REMOVER, displayName = "StellarSystems")
    /* renamed from: l */
    public void mo1537l(StellarSystem jj) {
        switch (bFf().mo6893i(cNc)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, cNc, new Object[]{jj}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, cNc, new Object[]{jj}));
                break;
        }
        m2231k(jj);
    }

    @C3248pc(aYR = C3248pc.C3250b.REMOVER, displayName = "ShipTypes")
    /* renamed from: l */
    public void mo1538l(ShipType ng) {
        switch (bFf().mo6893i(cNS)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, cNS, new Object[]{ng}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, cNS, new Object[]{ng}));
                break;
        }
        m2232k(ng);
    }

    @C3248pc(aYR = C3248pc.C3250b.REMOVER, displayName = "ItemTypeCategories")
    /* renamed from: m */
    public void mo1539m(ItemTypeCategory aai) {
        switch (bFf().mo6893i(cOb)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, cOb, new Object[]{aai}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, cOb, new Object[]{aai}));
                break;
        }
        m2233l(aai);
    }

    @C3248pc(aYR = C3248pc.C3250b.REMOVER, displayName = "NPCTypes")
    @C5566aOg
    /* renamed from: m */
    public void mo1540m(NPCType aed) {
        switch (bFf().mo6893i(cMZ)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, cMZ, new Object[]{aed}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, cMZ, new Object[]{aed}));
                break;
        }
        m2234l(aed);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Mestre dos Magos")
    @C5566aOg
    /* renamed from: o */
    public void mo1541o(NPCType aed) {
        switch (bFf().mo6893i(cOj)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, cOj, new Object[]{aed}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, cOj, new Object[]{aed}));
                break;
        }
        m2235n(aed);
    }

    @C5566aOg
    public void shutdown() {
        switch (bFf().mo6893i(cQa)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, cQa, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, cQa, new Object[0]));
                break;
        }
        aMf();
    }

    /* renamed from: t */
    public <E> Map<Class<? extends E>, Collection<E>> mo1543t(Class<E> cls) {
        switch (bFf().mo6893i(cQm)) {
            case 0:
                return null;
            case 2:
                return (Map) bFf().mo5606d(new aCE(this, cQm, new Object[]{cls}));
            case 3:
                bFf().mo5606d(new aCE(this, cQm, new Object[]{cls}));
                break;
        }
        return m2237s(cls);
    }

    /* renamed from: t */
    public boolean mo1544t(ItemType jCVar) {
        switch (bFf().mo6893i(cOn)) {
            case 0:
                return false;
            case 2:
                return ((Boolean) bFf().mo5606d(new aCE(this, cOn, new Object[]{jCVar}))).booleanValue();
            case 3:
                bFf().mo5606d(new aCE(this, cOn, new Object[]{jCVar}));
                break;
        }
        return m2238s(jCVar);
    }

    public String toString() {
        switch (bFf().mo6893i(_f_toString_0020_0028_0029Ljava_002flang_002fString_003b)) {
            case 0:
                return null;
            case 2:
                return (String) bFf().mo5606d(new aCE(this, _f_toString_0020_0028_0029Ljava_002flang_002fString_003b, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, _f_toString_0020_0028_0029Ljava_002flang_002fString_003b, new Object[0]));
                break;
        }
        return m2121au();
    }

    @C5566aOg
    @C3248pc(aYR = C3248pc.C3250b.ITERATOR, displayName = "WeaponTypes")
    @C2499fr
    /* renamed from: uZ */
    public List<WeaponType> mo1545uZ() {
        switch (bFf().mo6893i(f400Pi)) {
            case 0:
                return null;
            case 2:
                return (List) bFf().mo5606d(new aCE(this, f400Pi, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f400Pi, new Object[0]));
                break;
        }
        return m2240uY();
    }

    @C3248pc(aYR = C3248pc.C3250b.ITERATOR, displayName = "Amplifier Types")
    /* renamed from: vb */
    public List<AmplifierType> mo1546vb() {
        switch (bFf().mo6893i(f403Pl)) {
            case 0:
                return null;
            case 2:
                return (List) bFf().mo5606d(new aCE(this, f403Pl, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f403Pl, new Object[0]));
                break;
        }
        return m2241va();
    }

    @C3248pc(aYR = C3248pc.C3250b.ADDER, displayName = "Damage Types")
    /* renamed from: x */
    public void mo1547x(DamageType fr) {
        switch (bFf().mo6893i(cPk)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, cPk, new Object[]{fr}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, cPk, new Object[]{fr}));
                break;
        }
        m2242w(fr);
    }

    @C3248pc(aYR = C3248pc.C3250b.REMOVER, displayName = "Damage Types")
    /* renamed from: z */
    public void mo1548z(DamageType fr) {
        switch (bFf().mo6893i(cPl)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, cPl, new Object[]{fr}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, cPl, new Object[]{fr}));
                break;
        }
        m2244y(fr);
    }

    @C0064Am(aul = "d69e8e00bd325bdefc77f842f302ed10", aum = 0)
    /* renamed from: ap */
    private UUID m2114ap() {
        return m2111an();
    }

    @C0064Am(aul = "33ed544663f1cc943366896f57ef921e", aum = 0)
    /* renamed from: b */
    private void m2145b(UUID uuid) {
        m2081a(uuid);
    }

    @C0064Am(aul = "20d16d2cadf732febc4c3eb792de43d6", aum = 0)
    /* renamed from: au */
    private String m2121au() {
        return "Taikodom";
    }

    @C0064Am(aul = "51f4cd88bfebbb9c2e94a53e324a7bdb", aum = 0)
    private String aIP() {
        return TaikodomVersion.VERSION;
    }

    /* renamed from: S */
    public void mo10S() {
        super.mo10S();
        init();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Tael Bank")
    @C0064Am(aul = "7a3551613399e9c56ffb569b7e46ab34", aum = 0)
    private Bank aIR() {
        return aIe();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Hoplon Bank")
    @C0064Am(aul = "272614b608fe5f371334ec23fced59ef", aum = 0)
    private Bank aIT() {
        return aIf();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "MarkManager")
    @C0064Am(aul = "4865936b03ce4547115e188763de7a86", aum = 0)
    private MarkManager aIV() {
        return aIn();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "# NLSManager")
    @C0064Am(aul = "db15a82ff07585552581859ad8e0cc91", aum = 0)
    private NLSManager aIX() {
        return aIc();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Assets")
    @C0064Am(aul = "bb762f518540bf867fc1d762e3900bf1", aum = 0)
    private AssetGroup aIZ() {
        return aIk();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Space Zone Types Manager")
    @C0064Am(aul = "b9e31b67cd7ba5687a526c8556902b55", aum = 0)
    private SpaceZoneTypesManager aJb() {
        return aIg();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "# Default Taikodom Contents")
    @C0064Am(aul = "539a0c91d38375c75cf41f1a061054f4", aum = 0)
    private TaikodomDefaultContents aJd() {
        return aIr();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Physics Tweaks")
    @C0064Am(aul = "5310aa71d6d5b9b3ae7d624da45f2691", aum = 0)
    private GlobalPhysicsTweaks aJf() {
        return aID();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Item Billing")
    @C0064Am(aul = "92f8aa40eefc9aebe32ddf9a66ddbb4d", aum = 0)
    private ItemBilling aJh() {
        return aIu();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Progression")
    @C0064Am(aul = "c80c7adb5a4e72d5db53a222def3e8be", aum = 0)
    private Progression aJj() {
        return aIa();
    }

    @C0064Am(aul = "5a430f3877a5835c56ea67c6dbfe5745", aum = 0)
    private PlayerFinder aJl() {
        return aIy();
    }

    @C0064Am(aul = "638bdc97afeef14f8f3ff55e5c69f08d", aum = 0)
    private ServerEditorUtility aJn() {
        return aIq();
    }

    @C0064Am(aul = "80c4d0d3d99cda074f19c90e4c89f2ac", aum = 0)
    private SupportOuterface aJp() {
        return aIl();
    }

    @C0064Am(aul = "efb758e1cdf9ebcef446f94661b370dc", aum = 0)
    private Backdoor aJr() {
        return aIj();
    }

    @C0064Am(aul = "ce488c030cc59c731d1126a577870096", aum = 0)
    private StatsManager aJt() {
        return aIm();
    }

    @C0064Am(aul = "eb81a71d6b77c193f73dd5216337017b", aum = 0)
    private AdvertiseViewTable aJv() {
        return aIo();
    }

    @C0064Am(aul = "65158b9255f711b9643e7a7736f35788", aum = 0)
    private CorporationReservedNames aJx() {
        return aIx();
    }

    @C0064Am(aul = "8e40cd8c4af0b4876ec1bfe00620646b", aum = 0)
    private Insurer aJz() {
        return aIi();
    }

    @C0064Am(aul = "54ab51150a3024113395a8fe56803397", aum = 0)
    @Deprecated
    private C1077Pl aJB() {
        return aIz();
    }

    @C0064Am(aul = "6adbacfa041d14d589b1245fdd7434db", aum = 0)
    private C0694Ju aJD() {
        if (aIA() == null) {
            C0694Ju ju = (C0694Ju) bFf().mo6865M(C0694Ju.class);
            ju.mo10S();
            m2029a(ju);
        }
        return aIA();
    }

    @C0064Am(aul = "7f0be395f9deb7ae045676c8e34da557", aum = 0)
    /* renamed from: jF */
    private void m2230jF() {
        logger.info("Init Taikodom");
        aJG();
    }

    @C0064Am(aul = "e6125ae123b3c62cc9028d27fcf3b0bc", aum = 0)
    private PingScript aJH() {
        return aIK();
    }

    @C0064Am(aul = "d33f962893c32e43f5948f9b4e4f1d35", aum = 0)
    @C5794aaa
    private Collection<User> aJN() {
        return Collections.unmodifiableCollection(aJQ());
    }

    @C3248pc(aYR = C3248pc.C3250b.ITERATOR, displayName = "Mission Templates")
    @C0064Am(aul = "efe08c36da2557547e0e420f3f3f849b", aum = 0)
    private List<MissionTemplate> aJT() {
        return Collections.unmodifiableList(aHI());
    }

    @C3248pc(aYR = C3248pc.C3250b.ITERATOR, displayName = "EquippedShipTypes")
    @C0064Am(aul = "c6cc7be15ff7e07bc9ad581e963463a4", aum = 0)
    private List<EquippedShipType> aJV() {
        return Collections.unmodifiableList(aHJ());
    }

    @C3248pc(aYR = C3248pc.C3250b.ITERATOR, displayName = "NPCTypes")
    @C0064Am(aul = "4c3697e893db0805dbbd0df061694f94", aum = 0)
    private List<NPCType> aJX() {
        return Collections.unmodifiableList(alN());
    }

    @C3248pc(aYR = C3248pc.C3250b.ADDER, displayName = "StellarSystems")
    @C0064Am(aul = "c53a82bac15bb95f25c43845979532cb", aum = 0)
    /* renamed from: i */
    private void m2223i(StellarSystem jj) {
        aHo().add(jj);
    }

    @C3248pc(aYR = C3248pc.C3250b.REMOVER, displayName = "StellarSystems")
    @C0064Am(aul = "48ce123d0d5ff608a5ea4f0cb63ab6c5", aum = 0)
    /* renamed from: k */
    private void m2231k(StellarSystem jj) {
        aHo().remove(jj);
    }

    @C3248pc(aYR = C3248pc.C3250b.ITERATOR, displayName = "StellarSystems")
    @C0064Am(aul = "41e4087d5c33270af4faa0713eacf7d5", aum = 0)
    private List<StellarSystem> aJZ() {
        return Collections.unmodifiableList(aHo());
    }

    @C3248pc(aYR = C3248pc.C3250b.ITERATOR, displayName = "AmmoTypes")
    @C0064Am(aul = "692279564f23b0483a3b502b27fed5e4", aum = 0)
    private List<ClipType> aKb() {
        return Collections.unmodifiableList(aHp());
    }

    @C3248pc(aYR = C3248pc.C3250b.ITERATOR, displayName = "AmmoBoxTypes")
    @C0064Am(aul = "575905ad4b76e6eb158653bc1c1145de", aum = 0)
    private List<ClipBoxType> aKd() {
        return Collections.unmodifiableList(aHq());
    }

    @C3248pc(aYR = C3248pc.C3250b.ITERATOR, displayName = "CargoHoldType")
    @C0064Am(aul = "3f0d61917842675990c5909ef157bfdd", aum = 0)
    private List<CargoHoldType> aKf() {
        return Collections.unmodifiableList(aHr());
    }

    @C3248pc(aYR = C3248pc.C3250b.ADDER, displayName = "CargoHoldType")
    @C0064Am(aul = "3546abf3529528ec42ed1630c7880c64", aum = 0)
    /* renamed from: c */
    private void m2157c(CargoHoldType arb) {
        aHr().add(arb);
    }

    @C3248pc(aYR = C3248pc.C3250b.REMOVER, displayName = "CargoHoldType")
    @C0064Am(aul = "f15ea3a3140ec99f078831b78a738a5f", aum = 0)
    /* renamed from: e */
    private void m2189e(CargoHoldType arb) {
        aHr().remove(arb);
    }

    @C3248pc(aYR = C3248pc.C3250b.ADDER, displayName = "GateTypes")
    @C0064Am(aul = "f727800f0997709a92fce5007b046520", aum = 0)
    /* renamed from: h */
    private void m2222h(GateType mxVar) {
        aHs().add(mxVar);
    }

    @C3248pc(aYR = C3248pc.C3250b.REMOVER, displayName = "GateTypes")
    @C0064Am(aul = "7976c6f96dc5dd7e181c2d9793e63f84", aum = 0)
    /* renamed from: j */
    private void m2229j(GateType mxVar) {
        aHs().remove(mxVar);
    }

    @C3248pc(aYR = C3248pc.C3250b.ITERATOR, displayName = "GateTypes")
    @C0064Am(aul = "2fb3982b253392f5d0eab8388fde7cff", aum = 0)
    private List<GateType> aKh() {
        return Collections.unmodifiableList(aHs());
    }

    @C3248pc(aYR = C3248pc.C3250b.ADDER, displayName = "HullTypes")
    @C0064Am(aul = "bbdda9c48ddc4baadafea0a05f1432a9", aum = 0)
    /* renamed from: b */
    private void m2143b(HullType wHVar) {
        aHv().add(wHVar);
    }

    @C3248pc(aYR = C3248pc.C3250b.REMOVER, displayName = "HullTypes")
    @C0064Am(aul = "850d67b18cc06fbdd8ecc1d8edf936be", aum = 0)
    /* renamed from: d */
    private void m2182d(HullType wHVar) {
        aHv().remove(wHVar);
    }

    @C3248pc(aYR = C3248pc.C3250b.ITERATOR, displayName = "HullTypes")
    @C0064Am(aul = "4b57fe2ba1ff5e2f6c023e2132963c85", aum = 0)
    private List<HullType> aKj() {
        return Collections.unmodifiableList(aHv());
    }

    @C3248pc(aYR = C3248pc.C3250b.ADDER, displayName = "ShieldTypes")
    @C0064Am(aul = "e36d8b8874825a46e82880bdbcdb1a79", aum = 0)
    /* renamed from: b */
    private void m2139b(ShieldType aph) {
        aHA().add(aph);
    }

    @C3248pc(aYR = C3248pc.C3250b.REMOVER, displayName = "ShieldTypes")
    @C0064Am(aul = "aac2ab89c7205c143df85fd9a5a0f29e", aum = 0)
    /* renamed from: d */
    private void m2181d(ShieldType aph) {
        aHA().remove(aph);
    }

    @C3248pc(aYR = C3248pc.C3250b.ITERATOR, displayName = "ShieldTypes")
    @C0064Am(aul = "4f959bc282709e4b7c9a48713e55ed1d", aum = 0)
    private List<ShieldType> aKl() {
        return Collections.unmodifiableList(aHA());
    }

    @C3248pc(aYR = C3248pc.C3250b.ADDER, displayName = "HazardAreaTypes")
    @C0064Am(aul = "c1f661c1347e27f3ac79a449023059fc", aum = 0)
    /* renamed from: f */
    private void m2203f(HazardAreaType ass) {
        aHB().add(ass);
    }

    @C3248pc(aYR = C3248pc.C3250b.REMOVER, displayName = "HazardAreaTypes")
    @C0064Am(aul = "2e5152e22fcd63d51159d8d71925206d", aum = 0)
    /* renamed from: h */
    private void m2220h(HazardAreaType ass) {
        aHB().remove(ass);
    }

    @C3248pc(aYR = C3248pc.C3250b.ITERATOR, displayName = "HazardAreaTypes")
    @C0064Am(aul = "e8dd761fddd5bee0710e7e030038f0d2", aum = 0)
    private List<HazardAreaType> aKn() {
        return Collections.unmodifiableList(aHB());
    }

    @C3248pc(aYR = C3248pc.C3250b.ADDER, displayName = "MerchandiseTypes")
    @C0064Am(aul = "a909ec5accea41e9e281dfd085901411", aum = 0)
    /* renamed from: a */
    private void m2032a(MerchandiseType mf) {
        aHx().add(mf);
    }

    @C3248pc(aYR = C3248pc.C3250b.REMOVER, displayName = "MerchandiseTypes")
    @C0064Am(aul = "436dd71b67c8dc302ebee48256d2adfd", aum = 0)
    /* renamed from: c */
    private void m2150c(MerchandiseType mf) {
        aHx().remove(mf);
    }

    @C3248pc(aYR = C3248pc.C3250b.ITERATOR, displayName = "MerchandiseTypes")
    @C0064Am(aul = "87e64380f35cbf0ed909fc94f6f08a8a", aum = 0)
    private List<MerchandiseType> aKp() {
        return Collections.unmodifiableList(aHx());
    }

    @C3248pc(aYR = C3248pc.C3250b.ADDER, displayName = "BagTypes")
    @C0064Am(aul = "41ba29fde46b935d66a2b1b4b45db0c2", aum = 0)
    /* renamed from: d */
    private void m2177d(BagItemType ato) {
        aHw().add(ato);
    }

    @C3248pc(aYR = C3248pc.C3250b.REMOVER, displayName = "BagTypes")
    @C0064Am(aul = "e479fa387074acab7a3ec253c98df27b", aum = 0)
    /* renamed from: f */
    private void m2200f(BagItemType ato) {
        aHw().remove(ato);
    }

    @C3248pc(aYR = C3248pc.C3250b.ITERATOR, displayName = "BagTypes")
    @C0064Am(aul = "041dc9cc4aa9145fe931a9b7a26be170", aum = 0)
    private List<BagItemType> aKr() {
        return Collections.unmodifiableList(aHw());
    }

    @C3248pc(aYR = C3248pc.C3250b.ADDER, displayName = "BlueprintTypes")
    @C0064Am(aul = "1793d0eaf0dabd1b0f9ab3b11658b4e8", aum = 0)
    /* renamed from: a */
    private void m2031a(BlueprintType mr) {
        aHy().add(mr);
    }

    @C3248pc(aYR = C3248pc.C3250b.REMOVER, displayName = "BlueprintTypes")
    @C0064Am(aul = "f7235d762d7968b8918482caa792e612", aum = 0)
    /* renamed from: c */
    private void m2149c(BlueprintType mr) {
        aHy().remove(mr);
    }

    @C3248pc(aYR = C3248pc.C3250b.ITERATOR, displayName = "BlueprintTypes")
    @C0064Am(aul = "ff103412e3e909b73ea2197bb78b0753", aum = 0)
    private List<BlueprintType> aKt() {
        return Collections.unmodifiableList(aHy());
    }

    @C3248pc(aYR = C3248pc.C3250b.ITERATOR, displayName = "SceneryTypes")
    @C0064Am(aul = "eae5326d78b5a9957cceebae729ed1cf", aum = 0)
    private List<SceneryType> aKv() {
        return Collections.unmodifiableList(aHz());
    }

    @C3248pc(aYR = C3248pc.C3250b.ADDER, displayName = "SceneryTypes")
    @C0064Am(aul = "23a8efd2c0d7c11d4f195ba583b5cf16", aum = 0)
    /* renamed from: e */
    private void m2190e(SceneryType atc) {
        aHz().add(atc);
    }

    @C3248pc(aYR = C3248pc.C3250b.REMOVER, displayName = "SceneryTypes")
    @C0064Am(aul = "302944fd9678bdd9eb14694f6a0ff9b1", aum = 0)
    /* renamed from: g */
    private void m2209g(SceneryType atc) {
        aHz().remove(atc);
    }

    @C3248pc(aYR = C3248pc.C3250b.ADDER, displayName = "ShipSectorTypes")
    @C0064Am(aul = "5c416636c98fe4d4afd6de351f73ca14", aum = 0)
    /* renamed from: a */
    private void m2055a(ShipSectorType aux) {
        aHC().add(aux);
    }

    @C3248pc(aYR = C3248pc.C3250b.REMOVER, displayName = "ShipSectorTypes")
    @C0064Am(aul = "9690b8a987b95f0e7eb871da66cbb7a4", aum = 0)
    /* renamed from: c */
    private void m2159c(ShipSectorType aux) {
        aHC().remove(aux);
    }

    @C3248pc(aYR = C3248pc.C3250b.ITERATOR, displayName = "ShipSectorTypes")
    @C0064Am(aul = "efba55513f08d56fc4c9b4e5eebb5194", aum = 0)
    private List<ShipSectorType> aKx() {
        return Collections.unmodifiableList(aHC());
    }

    @C3248pc(aYR = C3248pc.C3250b.ADDER, displayName = "ShipStructureTypes")
    @C0064Am(aul = "2cb2397925ae0a6d5e0f4d99b8463c82", aum = 0)
    /* renamed from: a */
    private void m2045a(ShipStructureType aks) {
        afb().add(aks);
    }

    @C3248pc(aYR = C3248pc.C3250b.REMOVER, displayName = "ShipStructureTypes")
    @C0064Am(aul = "84310b42a7f182b49c1acc17ebf0c4b8", aum = 0)
    /* renamed from: c */
    private void m2154c(ShipStructureType aks) {
        afb().remove(aks);
    }

    @C3248pc(aYR = C3248pc.C3250b.ITERATOR, displayName = "ShipStructureTypes")
    @C0064Am(aul = "65c9d9358b117a90e22c87c2661979fb", aum = 0)
    private List<ShipStructureType> aKz() {
        return Collections.unmodifiableList(afb());
    }

    @C3248pc(aYR = C3248pc.C3250b.ADDER, displayName = "ShipTypes")
    @C0064Am(aul = "1f960a66a76fc0a2cb807e28aef32b15", aum = 0)
    /* renamed from: i */
    private void m2224i(ShipType ng) {
        aHD().add(ng);
    }

    @C3248pc(aYR = C3248pc.C3250b.REMOVER, displayName = "ShipTypes")
    @C0064Am(aul = "c0f0c13411972590223963982ac9ce11", aum = 0)
    /* renamed from: k */
    private void m2232k(ShipType ng) {
        aHD().remove(ng);
    }

    @C3248pc(aYR = C3248pc.C3250b.ADDER, displayName = "StationTypes")
    @C0064Am(aul = "5670a4732d2488103891a78283abf3f0", aum = 0)
    /* renamed from: f */
    private void m2198f(StationType mx) {
        aHF().add(mx);
    }

    @C3248pc(aYR = C3248pc.C3250b.REMOVER, displayName = "StationTypes")
    @C0064Am(aul = "9ace36076b9924ad885fc74471b0f093", aum = 0)
    /* renamed from: h */
    private void m2218h(StationType mx) {
        aHF().remove(mx);
    }

    @C3248pc(aYR = C3248pc.C3250b.ITERATOR, displayName = "StationTypes")
    @C0064Am(aul = "5a0842d1607ed728aa38e20f57734e0c", aum = 0)
    private List<StationType> aKD() {
        return Collections.unmodifiableList(aHF());
    }

    @C3248pc(aYR = C3248pc.C3250b.ADDER, displayName = "WeaponTypes")
    @C0064Am(aul = "dd7aadf17000813b8ff34c75ba322804", aum = 0)
    /* renamed from: a */
    private void m2049a(WeaponType apt) {
        m2239uQ().add(apt);
    }

    @C3248pc(aYR = C3248pc.C3250b.REMOVER, displayName = "WeaponTypes")
    @C0064Am(aul = "37bdae7690a79a599cb07a40fb1f866f", aum = 0)
    /* renamed from: c */
    private void m2155c(WeaponType apt) {
        m2239uQ().remove(apt);
    }

    @C3248pc(aYR = C3248pc.C3250b.ADDER, displayName = "RawMaterialTypes")
    @C0064Am(aul = "04f0f6c01c31e0fceeba16cc48719962", aum = 0)
    /* renamed from: a */
    private void m2043a(RawMaterialType agy) {
        aHG().add(agy);
    }

    @C3248pc(aYR = C3248pc.C3250b.REMOVER, displayName = "RawMaterialTypes")
    @C0064Am(aul = "62e09d584e8dd13246e5b4e935340eb8", aum = 0)
    /* renamed from: c */
    private void m2153c(RawMaterialType agy) {
        aHG().remove(agy);
    }

    @C3248pc(aYR = C3248pc.C3250b.ITERATOR, displayName = "RawMaterialTypes")
    @C0064Am(aul = "1496d4f96ff0dfa2ab378fb33ead2907", aum = 0)
    private List<RawMaterialType> aKF() {
        return Collections.unmodifiableList(aHG());
    }

    @C3248pc(aYR = C3248pc.C3250b.ADDER, displayName = "ItemTypeCategories")
    @C0064Am(aul = "4509646ac73093604508ffd5c109e57d", aum = 0)
    /* renamed from: j */
    private void m2227j(ItemTypeCategory aai) {
        aHH().add(aai);
    }

    @C3248pc(aYR = C3248pc.C3250b.REMOVER, displayName = "ItemTypeCategories")
    @C0064Am(aul = "4b3d7235258ca191b8fe6e1ccddb4b72", aum = 0)
    /* renamed from: l */
    private void m2233l(ItemTypeCategory aai) {
        aHH().remove(aai);
    }

    @C3248pc(aYR = C3248pc.C3250b.ITERATOR, displayName = "ItemTypeCategories")
    @C0064Am(aul = "10a97fcac5f274044a2ee5e8e3b9516e", aum = 0)
    private List<ItemTypeCategory> aKH() {
        return Collections.unmodifiableList(aHH());
    }

    @C3248pc(aYR = C3248pc.C3250b.ADDER, displayName = "ItemGenSets")
    @C0064Am(aul = "e699dd80ac925f901dc1cdb86606f0ef", aum = 0)
    /* renamed from: e */
    private void m2186e(ItemGenSet alq) {
        m2014TT().add(alq);
    }

    @C3248pc(aYR = C3248pc.C3250b.REMOVER, displayName = "ItemGenSets")
    @C0064Am(aul = "bd4596eae7fbc45d576865851960aa8f", aum = 0)
    /* renamed from: g */
    private void m2207g(ItemGenSet alq) {
        m2014TT().remove(alq);
    }

    @C3248pc(aYR = C3248pc.C3250b.ITERATOR, displayName = "ItemGenSets")
    @C0064Am(aul = "20b746d8e5cb233f3281b538661101b2", aum = 0)
    /* renamed from: TW */
    private List<ItemGenSet> m2015TW() {
        return Collections.unmodifiableList(m2014TT());
    }

    @C3248pc(aYR = C3248pc.C3250b.ADDER, displayName = "LootTypes")
    @C0064Am(aul = "957dc0835d8ed6877d035c3165836fd1", aum = 0)
    /* renamed from: b */
    private void m2135b(LootType ahc) {
        aHK().add(ahc);
    }

    @C3248pc(aYR = C3248pc.C3250b.REMOVER, displayName = "LootTypes")
    @C0064Am(aul = "3df0e59e8557bb1411dc21cf6eec2267", aum = 0)
    /* renamed from: d */
    private void m2174d(LootType ahc) {
        aHK().remove(ahc);
    }

    @C3248pc(aYR = C3248pc.C3250b.ITERATOR, displayName = "LootTypes")
    @C0064Am(aul = "983426de6d7c26207aac76bd2c087698", aum = 0)
    private List<LootType> aKJ() {
        return Collections.unmodifiableList(aHK());
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Mestre dos Magos")
    @C0064Am(aul = "4b4db5fe0ac84965a877103fe5940f5e", aum = 0)
    private NPCType aKL() {
        return aIp();
    }

    @C3248pc(aYR = C3248pc.C3250b.ITERATOR, displayName = "Factions")
    @C0064Am(aul = "89ae532fe8e7a6b03a39e8d6d0211ff1", aum = 0)
    /* renamed from: Uu */
    private List<Faction> m2018Uu() {
        return Collections.unmodifiableList(m2017Ut());
    }

    @C3248pc(aYR = C3248pc.C3250b.ITERATOR, displayName = "Tutorials")
    @C0064Am(aul = "b952ccc6a181b0222eb5c0bb3429c7da", aum = 0)
    private List<Tutorial> aKN() {
        return Collections.unmodifiableList(aHO());
    }

    @C0064Am(aul = "776659e30ea3a1c1280ce8100d8ee9c1", aum = 0)
    /* renamed from: s */
    private boolean m2238s(ItemType jCVar) {
        return aIG().add(jCVar);
    }

    @C0064Am(aul = "93bfb9f7f755626566e9c645e86ffd6a", aum = 0)
    private Set<ItemType> aKP() {
        return MessageContainer.m16003v(aIG());
    }

    @C0064Am(aul = "1436e0a25ee6862cd6d679a6ae95ed49", aum = 0)
    /* renamed from: a */
    private void m2028a(C0468GU gu) {
        aIH().add(gu);
    }

    @C0064Am(aul = "44e41c0f32fdfad2de69524cfebce44b", aum = 0)
    private boolean azS() {
        return true;
    }

    @C0064Am(aul = "002d8f31c870cf67604089607a350983", aum = 0)
    /* renamed from: cC */
    private List<ItemType> m2170cC(boolean z) {
        Map<Class<? extends E>, Collection<E>> t = mo1543t(ItemType.class);
        ArrayList arrayList = new ArrayList();
        for (Collection<E> addAll : t.values()) {
            arrayList.addAll(addAll);
        }
        if (!z) {
            return arrayList;
        }
        Iterator it = arrayList.iterator();
        while (it.hasNext()) {
            ItemType jCVar = (ItemType) it.next();
            if (jCVar.mo19866HC() == null) {
                it.remove();
            } else if (jCVar.mo19866HC().bJS() == null) {
                it.remove();
            }
        }
        return arrayList;
    }

    @C0064Am(aul = "18a5cf8603dad388d8f3723083e489d1", aum = 0)
    /* renamed from: g */
    private void m2206g(User adk) {
        aMC().mo12160e(adk.cSV());
        for (Player aku : adk.cSQ()) {
            PlayerController dxc = aku.dxc();
            if (aku.dyf()) {
                dxc.dispose();
                aku.mo14429ks(false);
            }
        }
    }

    @C0064Am(aul = "8a980c6f7a2871c94ff05f830de9b0c1", aum = 0)
    /* renamed from: gQ */
    private void m2214gQ(int i) {
        int i2 = 0;
        long j = 0;
        for (Node next : aKa().get(0).getNodes()) {
            if (next.mo21665ke().get().startsWith("TNode")) {
                long j2 = (long) next.getPosition().z;
                if (j >= j2) {
                    j2 = j;
                }
                i2++;
                j = j2;
            }
        }
        int i3 = i2 + 1;
        for (int i4 = 0; i4 < i; i4++) {
            m2129b(200000, "TNode", ((long) (i4 + 1)) * j, i3);
            j += 200000;
            i3++;
        }
    }

    @C0064Am(aul = "d384d6bac70707027a9d79ed429916b0", aum = 0)
    /* renamed from: a */
    private void m2023a(long j, String str, long j2, int i) {
        Node rPVar = (Node) bFf().mo6865M(Node.class);
        rPVar.mo10S();
        rPVar.mo21656f(new I18NString(String.valueOf(str) + "_" + i));
        Vec3d ajr = new Vec3d(ScriptRuntime.NaN, (double) j, (double) (j + j2));
        rPVar.setPosition(ajr);
        Asset tCVar = (Asset) bFf().mo6865M(Asset.class);
        tCVar.mo10S();
        tCVar.setHandle("nod_spa_mars");
        tCVar.setFile("data/scene/nod_spa_mars.pro");
        rPVar.mo21601I(tCVar);
        rPVar.mo21625aV(true);
        rPVar.setRadius(100000.0f);
        aKa().get(0).mo3247g(rPVar);
        rPVar.mo21630b(aJe().mo19012xh().azP());
        Node azW = aJe().mo19012xh().azW();
        Gate Ny = aKi().get(0).mo20631Ny();
        Gate Ny2 = aKi().get(0).mo20631Ny();
        Ny.setOrientation(new Orientation(1.0f, 0.0f, 0.0f, 0.0f));
        Ny2.setOrientation(new Orientation(1.0f, 0.0f, 0.0f, 0.0f));
        Ny.setPosition(aJe().mo19012xh().getPosition().mo9531q(new Vec3f((float) (i * 200), (float) (i * 200), (float) (i * 200))));
        Ny2.setPosition(ajr.mo9531q(new Vec3f(10000.0f, 1000.0f, 1000.0f)));
        Ny.mo18379c(Ny2);
        Ny.setEnabled(true);
        Ny2.setEnabled(true);
        Ny.mo18384k(new Vec3f(200.0f, 200.0f, 200.0f));
        Ny2.mo18384k(new Vec3f(200.0f, 200.0f, 200.0f));
        Ny.setName("toSortuda " + i);
        Ny2.setName("fromSortuda " + i);
        azW.mo21653e(aKi().get(0));
        rPVar.mo21653e(aKi().get(0));
        mo8359ma("********* Createing a New Node!!!" + ajr + " :: " + rPVar.mo21665ke());
    }

    @C0064Am(aul = "75596775ced74a250cb02e7397a4537c", aum = 0)
    /* renamed from: gS */
    private void m2216gS(int i) {
        ClipType aqf;
        WeaponType apt;
        ShipType ng;
        Iterator<ClipType> it = aKc().iterator();
        while (true) {
            if (it.hasNext()) {
                ClipType next = it.next();
                if ("EEL Rail".equals(next.mo19891ke().get())) {
                    aqf = next;
                    break;
                }
            } else {
                aqf = null;
                break;
            }
        }
        if (aqf == null) {
            mo6317hy("sorry, couldnt find EEL");
            return;
        }
        Iterator<WeaponType> it2 = mo1545uZ().iterator();
        while (true) {
            if (it2.hasNext()) {
                WeaponType next2 = it2.next();
                mo8359ma("weaponType: " + next2.mo19891ke().get());
                if ("Railgun 50".equals(next2.mo19891ke().get())) {
                    apt = next2;
                    break;
                }
            } else {
                apt = null;
                break;
            }
        }
        if (apt == null) {
            mo8359ma("sorry, couldnt find Railgun 50");
            return;
        }
        StellarSystem jj = aKa().get(0);
        Iterator it3 = aHD().iterator();
        while (true) {
            if (it3.hasNext()) {
                ShipType ng2 = (ShipType) it3.next();
                if ("Bullfrog".equals(ng2.mo19891ke().get())) {
                    ng = ng2;
                    break;
                }
            } else {
                ng = null;
                break;
            }
        }
        if (ng == null) {
            mo8359ma("sorry, couldnt find Bullfrog");
            return;
        }
        Vec3d d = aJe().mo19012xh().getPosition().mo9504d((Tuple3d) new Vec3d(5000.0d, ScriptRuntime.NaN, ScriptRuntime.NaN));
        int i2 = 0;
        while (true) {
            int i3 = i2;
            if (i3 < i) {
                Ship vf = ng.mo4219vf();
                ProjectileWeapon sg = (ProjectileWeapon) apt.mo7459NK();
                vf.mo18320i((Weapon) sg);
                sg.mo5366c(aqf.bIm());
                Vec3d d2 = d.mo9504d((Tuple3d) new Vec3d((double) (i3 * 200), ScriptRuntime.NaN, ScriptRuntime.NaN));
                mo8359ma("spawning at: " + d2);
                vf.setPosition(d2);
                vf.mo993b(jj);
                vf.mo1063f(0.0f, 70.0f, 0.0f);
                vf.mo964W(40.0f);
                sg.cDL();
                C0264a aVar = new C0264a(sg);
                aVar.setDaemon(true);
                aVar.start();
                PoolThread.sleep(200);
                i2 = i3 + 1;
            } else {
                return;
            }
        }
    }

    @C0064Am(aul = "c56bdc4bcac450402181bdcdc9e09f85", aum = 0)
    /* renamed from: a */
    private User m2020a(ClientConnect bVar, String userName, String password) {
        if (!C5877acF.RUNNING_CLIENT_BOT) {
            return null;
        }
        User adk = (User) bFf().mo6865M(User.class);
        adk.mo10S();
        adk.setUsername(userName);
        adk.setPassword(password);
        adk.mo8468dS(true);
        aHl().add(adk);
        return adk;
    }

    @C3248pc(aYR = C3248pc.C3250b.ADDER, displayName = "AdvertiseTypes")
    @C0064Am(aul = "485f9c3cf95005c21bec8be0e996f092", aum = 0)
    /* renamed from: c */
    private void m2147c(AdvertiseType kp) {
        aHt().add(kp);
    }

    @C3248pc(aYR = C3248pc.C3250b.REMOVER, displayName = "AdvertiseTypes")
    @C0064Am(aul = "ccaadb59227a1a8b121cb339c0050640", aum = 0)
    /* renamed from: e */
    private void m2185e(AdvertiseType kp) {
        aHt().remove(kp);
    }

    @C3248pc(aYR = C3248pc.C3250b.ITERATOR, displayName = "AdvertiseTypes")
    @C0064Am(aul = "984b7ec5840a875c3e86df8c1d05a320", aum = 0)
    private List<AdvertiseType> aKT() {
        return Collections.unmodifiableList(aHt());
    }

    @C3248pc(aYR = C3248pc.C3250b.ADDER, displayName = "AsteroidTypes")
    @C0064Am(aul = "6483eb7a6d215aadb0c2285468e2f52c", aum = 0)
    /* renamed from: d */
    private void m2176d(AsteroidType aqn) {
        aHu().add(aqn);
    }

    @C3248pc(aYR = C3248pc.C3250b.REMOVER, displayName = "AsteroidTypes")
    @C0064Am(aul = "b0800225ed939258cf11919c874de598", aum = 0)
    /* renamed from: f */
    private void m2199f(AsteroidType aqn) {
        aHu().remove(aqn);
    }

    @C3248pc(aYR = C3248pc.C3250b.ITERATOR, displayName = "AsteroidTypes")
    @C0064Am(aul = "f81a594bbc67f4977c5625e3e0051aec", aum = 0)
    private List<AsteroidType> aKV() {
        return Collections.unmodifiableList(aHu());
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Advertise Path")
    @C0064Am(aul = "536d4648fb5ffc4081ae022351f3d50d", aum = 0)
    private I18NString aKX() {
        return aHk();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Avatar Manager")
    @C0064Am(aul = "0f2901bb4b8819c133de11f7873a547e", aum = 0)
    private AvatarManager aKZ() {
        return aId();
    }

    @C3248pc(aYR = C3248pc.C3250b.ITERATOR, displayName = "Collision Sets")
    @C0064Am(aul = "7c854c0a5049c89e1926a6fc28273d3e", aum = 0)
    private List<CollisionFXSet> aLb() {
        return Collections.unmodifiableList(aHn());
    }

    @C0064Am(aul = "5ed549dc4068fdfc922c98c64996a1eb", aum = 0)
    /* renamed from: b */
    private Object m2128b(int i, Object obj) {
        return null;
    }

    @C3248pc(aYR = C3248pc.C3250b.ADDER, displayName = "Amplifier Types")
    @C0064Am(aul = "dd64bed9e02e41d9b9f77c2d13ae43ba", aum = 0)
    /* renamed from: f */
    private void m2204f(AmplifierType iDVar) {
        aHL().add(iDVar);
    }

    @C3248pc(aYR = C3248pc.C3250b.REMOVER, displayName = "Amplifier Types")
    @C0064Am(aul = "8de8f06251f095d154e350c707b67cf7", aum = 0)
    /* renamed from: h */
    private void m2221h(AmplifierType iDVar) {
        aHL().remove(iDVar);
    }

    @C3248pc(aYR = C3248pc.C3250b.ITERATOR, displayName = "Amplifier Types")
    @C0064Am(aul = "daf1005f35882e60e3465a405ed168f8", aum = 0)
    /* renamed from: va */
    private List<AmplifierType> m2241va() {
        return Collections.unmodifiableList(aHL());
    }

    @C3248pc(aYR = C3248pc.C3250b.ADDER, displayName = "Module Types")
    @C0064Am(aul = "c1ed7a7ec3adc940330fd2fbb6d04672", aum = 0)
    /* renamed from: a */
    private void m2030a(ModuleType kq) {
        aHM().add(kq);
    }

    @C3248pc(aYR = C3248pc.C3250b.REMOVER, displayName = "Module Types")
    @C0064Am(aul = "b347baf7d675c28b3235dcbdf5ed59d4", aum = 0)
    /* renamed from: c */
    private void m2148c(ModuleType kq) {
        aHM().remove(kq);
    }

    @C3248pc(aYR = C3248pc.C3250b.ITERATOR, displayName = "Module Types")
    @C0064Am(aul = "e990e803e5722d72c7dd04be2db9003e", aum = 0)
    private List<ModuleType> aLd() {
        return Collections.unmodifiableList(aHM());
    }

    @C3248pc(aYR = C3248pc.C3250b.ADDER, displayName = "AttributeBuffType")
    @C0064Am(aul = "afb80824c81f9ffc3b16311df663633c", aum = 0)
    /* renamed from: a */
    private void m2050a(AttributeBuffType api) {
        m2016UG().add(api);
    }

    @C3248pc(aYR = C3248pc.C3250b.REMOVER, displayName = "AttributeBuffType")
    @C0064Am(aul = "910c0a442e130190b8ede83ac2f6dfcd", aum = 0)
    /* renamed from: c */
    private void m2156c(AttributeBuffType api) {
        m2016UG().remove(api);
    }

    @C3248pc(aYR = C3248pc.C3250b.ITERATOR, displayName = "AttributeBuffType")
    @C0064Am(aul = "f90830d22a94e2e6892a0f76a8206192", aum = 0)
    private List<AttributeBuffType> aLf() {
        return Collections.unmodifiableList(m2016UG());
    }

    @C3248pc(aYR = C3248pc.C3250b.ITERATOR, displayName = "TurretTypes")
    @C0064Am(aul = "9d19b155d5b4dd9dd03ad63924560155", aum = 0)
    private List<C6544aow> aLh() {
        return Collections.unmodifiableList(aHE());
    }

    @C3248pc(aYR = C3248pc.C3250b.ADDER, displayName = "AI Controller Types")
    @C0064Am(aul = "ace4b3b7359b605ce0667f8156ff33f7", aum = 0)
    /* renamed from: a */
    private void m2037a(AIControllerType tz) {
        aHN().add(tz);
    }

    @C3248pc(aYR = C3248pc.C3250b.REMOVER, displayName = "AI Controller Types")
    @C0064Am(aul = "8601a506401ad5f0f61a03c266a17d36", aum = 0)
    /* renamed from: c */
    private void m2151c(AIControllerType tz) {
        aHN().remove(tz);
    }

    @C3248pc(aYR = C3248pc.C3250b.ITERATOR, displayName = "AI Controller Types")
    @C0064Am(aul = "c22f47772030089de27f56102d759bfa", aum = 0)
    private List<AIControllerType> aLj() {
        return Collections.unmodifiableList(aHN());
    }

    @C3248pc(aYR = C3248pc.C3250b.ADDER, displayName = "Fee Managers")
    @C0064Am(aul = "98ddb1d107eecd56e79fe7a178e66e90", aum = 0)
    /* renamed from: a */
    private void m2070a(ConsignmentFeeManager auVar) {
        aHP().add(auVar);
    }

    @C3248pc(aYR = C3248pc.C3250b.REMOVER, displayName = "Fee Managers")
    @C0064Am(aul = "0a4c79bac1958834c342df54bbdc5ac3", aum = 0)
    /* renamed from: c */
    private void m2164c(ConsignmentFeeManager auVar) {
        aHP().remove(auVar);
    }

    @C3248pc(aYR = C3248pc.C3250b.ITERATOR, displayName = "Fee Managers")
    @C0064Am(aul = "a255afa962529230a413c58b2e2a6fe3", aum = 0)
    private List<ConsignmentFeeManager> aLl() {
        return Collections.unmodifiableList(aHP());
    }

    @C3248pc(aYR = C3248pc.C3250b.ADDER, displayName = "Sector Categories")
    @C0064Am(aul = "1c474e1b55d08ae2be89ded40f055d39", aum = 0)
    /* renamed from: c */
    private void m2165c(SectorCategory fMVar) {
        aHQ().add(fMVar);
    }

    @C3248pc(aYR = C3248pc.C3250b.REMOVER, displayName = "Sector Categories")
    @C0064Am(aul = "1af04e5b79f255e6c45b9f3fd9e52f52", aum = 0)
    /* renamed from: e */
    private void m2191e(SectorCategory fMVar) {
        aHQ().remove(fMVar);
    }

    @C3248pc(aYR = C3248pc.C3250b.ITERATOR, displayName = "Sector Categories")
    @C0064Am(aul = "becf0b905eeabca2ef650ca6a69cd629", aum = 0)
    private List<SectorCategory> aLn() {
        return Collections.unmodifiableList(aHQ());
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Forbidden Names (comma separated)")
    @C0064Am(aul = "fa1d68531463c2f79ccf6cfecb28a143", aum = 0)
    private String aLp() {
        StringBuffer stringBuffer = new StringBuffer();
        Iterator it = aHm().iterator();
        while (it.hasNext()) {
            stringBuffer.append((String) it.next());
            if (it.hasNext()) {
                stringBuffer.append(",");
            }
        }
        return stringBuffer.toString();
    }

    @C3248pc(aYR = C3248pc.C3250b.ADDER, displayName = "Damage Types")
    @C0064Am(aul = "092253968265f913f3d9cd990b28c624", aum = 0)
    /* renamed from: w */
    private void m2242w(DamageType fr) {
        aHR().add(fr);
    }

    @C3248pc(aYR = C3248pc.C3250b.REMOVER, displayName = "Damage Types")
    @C0064Am(aul = "586acb7f596a66a46b5749c28d0916b4", aum = 0)
    /* renamed from: y */
    private void m2244y(DamageType fr) {
        aHR().remove(fr);
    }

    @C3248pc(aYR = C3248pc.C3250b.ITERATOR, displayName = "Damage Types")
    @C0064Am(aul = "772cd2faca8dd98f24c70012c8d50a0f", aum = 0)
    private Set<DamageType> aLr() {
        return Collections.unmodifiableSet(aHR());
    }

    @C3248pc(aYR = C3248pc.C3250b.ITERATOR, displayName = "Cruise Speed Types")
    @C0064Am(aul = "4ec504f0d827654374354a2439f53708", aum = 0)
    private Set<CruiseSpeedType> aLt() {
        return Collections.unmodifiableSet(aHS());
    }

    @C3248pc(aYR = C3248pc.C3250b.ITERATOR, displayName = "Citizenship Types")
    @C0064Am(aul = "835319c87e87edcf090ff3b50e3e6ef9", aum = 0)
    private List<CitizenshipType> aLv() {
        return Collections.unmodifiableList(aHU());
    }

    @C3248pc(aYR = C3248pc.C3250b.ITERATOR, displayName = "Citizenship Packs")
    @C0064Am(aul = "d364334c288cdc6d3a2b3178a34d2b6b", aum = 0)
    private List<CitizenshipPack> aLx() {
        return Collections.unmodifiableList(aHV());
    }

    @C3248pc(aYR = C3248pc.C3250b.ITERATOR, displayName = "Citizenship Reward")
    @C0064Am(aul = "2f3cd1d13d10225cc3abb20f7da2fd8d", aum = 0)
    private List<CitizenshipReward> aLz() {
        return Collections.unmodifiableList(aHW());
    }

    @C3248pc(aYR = C3248pc.C3250b.ITERATOR, displayName = "Taikopedia Entries")
    @C0064Am(aul = "40541b8e67f77a6e289bf202ca3382c9", aum = 0)
    /* renamed from: RV */
    private List<TaikopediaEntry> m2012RV() {
        return Collections.unmodifiableList(m2010Ll());
    }

    @C3248pc(aYR = C3248pc.C3250b.ITERATOR, displayName = "Database Categories")
    @C0064Am(aul = "7ed52a92dd7331c8079bdb70173be7db", aum = 0)
    private List<DatabaseCategory> aLB() {
        return Collections.unmodifiableList(aHX());
    }

    @C3248pc(aYR = C3248pc.C3250b.ITERATOR, displayName = "Asteroid Zone Categories")
    @C0064Am(aul = "3b09f95ed01564af3ebfc571206de526", aum = 0)
    private List<AsteroidZoneCategory> aLD() {
        return Collections.unmodifiableList(aHY());
    }

    @C3248pc(aYR = C3248pc.C3250b.ITERATOR, displayName = "Space Categories")
    @C0064Am(aul = "51e7809fcfade36956d65e4217da6654", aum = 0)
    private List<SpaceCategory> aLF() {
        return Collections.unmodifiableList(aHZ());
    }

    @C3248pc(aYR = C3248pc.C3250b.ITERATOR, displayName = "Citizenship Improvements Types")
    @C0064Am(aul = "333cd06a8b0123e0c704b28d7f19ed7a", aum = 0)
    private List<CitizenImprovementType> aLH() {
        return Collections.unmodifiableList(aHT());
    }

    @C0064Am(aul = "4b2830f073ba586bb7f1c493601417ca", aum = 0)
    private List<Corporation> aLJ() {
        return Collections.unmodifiableList(aIw());
    }

    @C0064Am(aul = "d8746ad7fdde1d85abd585e57dc96a08", aum = 0)
    /* renamed from: a */
    private void m2067a(C6417amZ amz) {
        if (amz.ape()) {
            List<C0665JT> list = (List) amz.aph().get("allUsers");
            if (list != null) {
                for (C0665JT baw : list) {
                    User adk = (User) baw.baw();
                    if (mo1497eg(adk.getUsername()) == null) {
                        aHl().add(adk);
                    }
                }
            }
            List<C0665JT> list2 = (List) amz.aph().get("corporations");
            if (list2 != null) {
                for (C0665JT baw2 : list2) {
                    Corporation aPVar = (Corporation) baw2.baw();
                    if (!aIw().contains(aPVar)) {
                        aIw().add(aPVar);
                    }
                }
            }
        }
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "ContractBoard")
    @C0064Am(aul = "d4ca218daa775a55c343e7c8fd2d3049", aum = 0)
    private ContractBoard aLN() {
        return aIb();
    }

    @C0064Am(aul = "73f4bca4c72587d98dc4e1cfad5f34aa", aum = 0)
    private TemporaryFeatureTest aLP() {
        return aIt();
    }

    @C0064Am(aul = "24883133cf2d55e54496dd9736371910", aum = 0)
    private DebugFlags aLR() {
        return aIL();
    }

    @C0064Am(aul = "14cc99dfa245b2c49bbe50dfd4074fe4", aum = 0)
    private TemporaryFeatureBlock aLT() {
        return aIs();
    }

    @C0064Am(aul = "d2bcd03f6495b68217cefcad5d00f9f2", aum = 0)
    @C2198cg
    private ClientUtils aLV() {
        return aIM();
    }

    @C0064Am(aul = "f72549ab3f58f6eaef767077e93fc3b6", aum = 0)
    @C2198cg
    private OffloadUtils aLX() {
        return aIN();
    }

    @C0064Am(aul = "987dc1e4ecf861a78b9a85c7609a0708", aum = 0)
    private BotUtils aLZ() {
        return aIO();
    }

    @C0064Am(aul = "3233219dcd84701fc713ae73f7b40182", aum = 0)
    @C2198cg
    private Collection<Space> aMb() {
        ArrayList arrayList = new ArrayList();
        for (StellarSystem nodes : aHo()) {
            for (Node Zc : nodes.getNodes()) {
                arrayList.add(Zc.mo21618Zc());
            }
        }
        return arrayList;
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Market")
    @C0064Am(aul = "fc8f935091b7791d54e13b07dbb85776", aum = 0)
    /* renamed from: fh */
    private Market m2205fh() {
        return aIB();
    }

    @C3248pc(aYR = C3248pc.C3250b.ITERATOR, displayName = "Warehouses")
    @C0064Am(aul = "869b97d0e3e02f04dd7b09446e7086ba", aum = 0)
    private List<VirtualWarehouse> aMi() {
        return Collections.unmodifiableList(aIE());
    }

    @C0064Am(aul = "67218682225f2da2fe778afbbce9f174", aum = 0)
    private UserConnection aMk() {
        DataGameEvent PM = bFf().mo6866PM();
        if (bGX()) {//WhoAmI.CLIENT
            return (UserConnection) PM.bGI().mo6901yn();
        }
        if (PM.bGE() != null) {
            return (UserConnection) PM.bGE().mo6901yn();
        }
        return null;
    }

    @C0064Am(aul = "a572c19b24adc460aebb74eb4e99ede1", aum = 0)
    /* renamed from: a */
    private void m2068a(UserConnection apk) {
        if (bGX()) {
            bFf().mo6866PM().mo3424c(apk.bFf());
            return;
        }
        throw new IllegalAccessError("Not client");
    }

    @C0064Am(aul = "d8fa460960c6248564a4edf4a92c312b", aum = 0)
    @ClientOnly
    private void aMm() {
        try {
            logger.info("Resolve current user");
            C0755Kn kn = (C0755Kn) ((C3582se) bFf()).mo21980c((C5581aOv) new C2592hH(this));
            if (!(ald() == null || ald().getEventManager() == null)) {
                ald().getEventManager().mo13975h(new C3932wv(14));
            }
            logger.info("Current user resolved");
            mo1438b((UserConnection) kn.bcB());
            logger.info("Resolve content");
            aMt().aLM();
            if (!(ald() == null || ald().getEventManager() == null)) {
                ald().getEventManager().mo13975h(new C3932wv(14));
            }
            logger.info("Content resolved");
            logger.info("PID");
            aMt().mo1318QS();
            if (!(ald() == null || ald().getEventManager() == null)) {
                ald().getEventManager().mo13975h(new C3932wv(14));
            }
            logger.info("client init 2");
        } catch (C1728ZX e) {
            throw new RuntimeException(e);
        }
    }

    @C0064Am(aul = "5279e1144f4297b06c1259cd45b8dc36", aum = 0)
    @ClientOnly
    private C6663arL aMo() {
        return bFf().mo6866PM().bGI();
    }

    @C0064Am(aul = "fce8f146e812d86d81bd22caf7de44c7", aum = 0)
    private Taikodom aMs() {
        return this;
    }

    @C0064Am(aul = "4cef72a93c0c1c0bd408d43e28719fef", aum = 0)
    /* renamed from: s */
    private <E> Map<Class<? extends E>, Collection<E>> m2237s(Class<E> cls) {
        HashMap hashMap = new HashMap();
        C1616Xf aMt = aMt();
        HashMap hashMap2 = new HashMap();
        for (C2491fm fmVar : aMt.mo12U()) {
            if (fmVar.mo4759pE() || fmVar.mo4763pI()) {
                String displayName = fmVar.getDisplayName();
                C2491fm[] fmVarArr = (C2491fm[]) hashMap2.get(displayName);
                if (fmVarArr == null) {
                    fmVarArr = new C2491fm[2];
                    hashMap2.put(displayName, fmVarArr);
                }
                if (fmVar.mo4759pE()) {
                    fmVarArr[0] = fmVar;
                } else if (fmVar.mo4763pI()) {
                    fmVarArr[1] = fmVar;
                }
            }
        }
        for (Map.Entry value : hashMap2.entrySet()) {
            C2491fm[] fmVarArr2 = (C2491fm[]) value.getValue();
            if (!(fmVarArr2[0] == null || fmVarArr2[1] == null || fmVarArr2[0].getParameterTypes().length != 1)) {
                Class cls2 = fmVarArr2[0].getParameterTypes()[0];
                if (cls.isAssignableFrom(cls2)) {
                    Object a = aMt.mo14a((C0495Gr) new aCE(aMt, fmVarArr2[1]));
                    Collection collection = (Collection) hashMap.get(cls2);
                    if (collection == null) {
                        hashMap.put(cls2, (Collection) a);
                    } else {
                        collection.addAll((Collection) a);
                    }
                }
            }
        }
        return hashMap;
    }

    @C0064Am(aul = "d555e83d924836f010dbf5858107c638", aum = 0)
    private IEngineGame aMu() {
        return ((DataGameEventImpl) bFf().mo6866PM()).ald();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Citizenship Office")
    @C0064Am(aul = "d49c78462ea642fefbc582050d4294dd", aum = 0)
    private CitizenshipOffice aMv() {
        return aIh();
    }

    @C0064Am(aul = "29ce30e1f2a5394f9cdf1d2662a9eee7", aum = 0)
    /* renamed from: aG */
    private void m2089aG() {
        HashSet hashSet = new HashSet();
        aJE().init();
        C1077Pl aJC = aJC();
        if (aJC != null) {
            aJC.mo4834h(hashSet);
        }
        Iterator it = hashSet.iterator();
        while (it.hasNext()) {
            aJE().mo3269b((C6920awI) it.next());
        }
        super.mo70aH();
    }

    @C0064Am(aul = "2b1edf156136e04a562d67fd0264435a", aum = 0)
    private PlayerReservedNames aMx() {
        return aIF();
    }

    @C0064Am(aul = "1afbecb2c4ccdcaf97d9b1d1b8a2badc", aum = 0)
    /* renamed from: ar */
    private String m2117ar() {
        return "taikodom_root_object";
    }

    @C0064Am(aul = "14af8d419602e98f07beedb0d2c3020b", aum = 0)
    private ContentImporterExporter aMz() {
        return aIv();
    }

    @C0064Am(aul = "a84b13e439ec89ceb94c8801a3a2ea51", aum = 0)
    private UsersQueueManager aMB() {
        return aIJ();
    }

    /* renamed from: a.DN$a */
    class C0264a extends Thread {
        private final /* synthetic */ Weapon iYc;

        C0264a(Weapon adv) {
            this.iYc = adv;
        }

        public void run() {
            while (true) {
                PoolThread.sleep(3000);
                if (this.iYc.cUB()) {
                    this.iYc.bai();
                } else {
                    this.iYc.cDL();
                }
            }
        }
    }
}
