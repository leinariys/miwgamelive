package game.script.cloning;

import game.network.message.externalizable.aCE;
import logic.baa.*;
import logic.data.mbean.C0675Jb;
import logic.res.code.C5640aRc;
import logic.res.code.C5663aRz;
import logic.res.code.C6494any;
import logic.res.html.C2491fm;
import p001a.*;

import java.util.Collection;
import java.util.UUID;

@C5511aMd
@C6485anp
/* renamed from: a.ko */
/* compiled from: a */
public class CloningCenter extends TaikodomObject implements C0468GU, C1616Xf {
    public static final int _m_fieldCount = 0;
    public static final C5663aRz[] _m_fields = null;
    public static final int _m_methodCount = 0;
    public static final C2491fm[] _m_methods = null;
    public static final C5663aRz asX = null;
    public static final C2491fm asY = null;
    public static final C2491fm asZ = null;
    /* renamed from: bL */
    public static final C5663aRz f8462bL = null;
    /* renamed from: bM */
    public static final C5663aRz f8463bM = null;
    /* renamed from: bN */
    public static final C2491fm f8464bN = null;
    /* renamed from: bO */
    public static final C2491fm f8465bO = null;
    /* renamed from: bP */
    public static final C2491fm f8466bP = null;
    /* renamed from: bQ */
    public static final C2491fm f8467bQ = null;
    public static final long serialVersionUID = 0;
    public static C6494any ___iScriptClass;
    @C0064Am(aul = "00db1514eaa1fccb22508c8a8388737d", aum = 0)
    private static long asW;
    @C0064Am(aul = "720ba065f4b318f3d920f4fdf3e8253e", aum = 1)

    /* renamed from: bK */
    private static UUID f8461bK;
    @C0064Am(aul = "d1b56b39d600b447c59f70d1bf374669", aum = 2)
    private static String handle;

    static {
        m34556V();
    }

    public CloningCenter() {
        super((C5540aNg) null);
        super.mo10S();
    }

    public CloningCenter(C5540aNg ang) {
        super(ang);
    }

    /* renamed from: V */
    static void m34556V() {
        serialVersionUID = (long) 1;
        _m_fieldCount = TaikodomObject._m_fieldCount + 3;
        _m_methodCount = TaikodomObject._m_methodCount + 6;
        int i = TaikodomObject._m_fieldCount;
        C5663aRz[] arzArr = new C5663aRz[(i + 3)];
        C5663aRz b = C5640aRc.m17844b(CloningCenter.class, "00db1514eaa1fccb22508c8a8388737d", i);
        asX = b;
        arzArr[i] = b;
        int i2 = i + 1;
        C5663aRz b2 = C5640aRc.m17844b(CloningCenter.class, "720ba065f4b318f3d920f4fdf3e8253e", i2);
        f8462bL = b2;
        arzArr[i2] = b2;
        int i3 = i2 + 1;
        C5663aRz b3 = C5640aRc.m17844b(CloningCenter.class, "d1b56b39d600b447c59f70d1bf374669", i3);
        f8463bM = b3;
        arzArr[i3] = b3;
        int i4 = i3 + 1;
        _m_fields = arzArr;
        C1634Xv.m11725a((Object[]) TaikodomObject._m_fields, (Object[]) _m_fields);
        int i5 = TaikodomObject._m_methodCount;
        C2491fm[] fmVarArr = new C2491fm[(i5 + 6)];
        C2491fm a = C4105zY.m41624a(CloningCenter.class, "26d9f4537ec30a36fb979ee7156fc119", i5);
        f8464bN = a;
        fmVarArr[i5] = a;
        int i6 = i5 + 1;
        C2491fm a2 = C4105zY.m41624a(CloningCenter.class, "3dd7001b44f251b038f563c4a6eda588", i6);
        f8465bO = a2;
        fmVarArr[i6] = a2;
        int i7 = i6 + 1;
        C2491fm a3 = C4105zY.m41624a(CloningCenter.class, "4eba51ac3670d628b5a874e8b31bd835", i7);
        f8466bP = a3;
        fmVarArr[i7] = a3;
        int i8 = i7 + 1;
        C2491fm a4 = C4105zY.m41624a(CloningCenter.class, "6feb1c2fed2a3f27ace193134d8167f3", i8);
        f8467bQ = a4;
        fmVarArr[i8] = a4;
        int i9 = i8 + 1;
        C2491fm a5 = C4105zY.m41624a(CloningCenter.class, "45e6095b83320b8236376f4f1e22f09a", i9);
        asY = a5;
        fmVarArr[i9] = a5;
        int i10 = i9 + 1;
        C2491fm a6 = C4105zY.m41624a(CloningCenter.class, "b364eadd2fdd353cd9b318a0a7483c4e", i10);
        asZ = a6;
        fmVarArr[i10] = a6;
        int i11 = i10 + 1;
        _m_methods = fmVarArr;
        C1634Xv.m11725a((Object[]) TaikodomObject._m_methods, (Object[]) _m_methods);
        ___iScriptClass = aUO.m18566a(CloningCenter.class, C0675Jb.class, _m_fields, _m_methods);
    }

    /* renamed from: JJ */
    private long m34554JJ() {
        return bFf().mo5608dq().mo3213o(asX);
    }

    /* renamed from: a */
    private void m34557a(String str) {
        bFf().mo5608dq().mo3197f(f8463bM, str);
    }

    /* renamed from: a */
    private void m34558a(UUID uuid) {
        bFf().mo5608dq().mo3197f(f8462bL, uuid);
    }

    /* renamed from: an */
    private UUID m34559an() {
        return (UUID) bFf().mo5608dq().mo3214p(f8462bL);
    }

    /* renamed from: ao */
    private String m34560ao() {
        return (String) bFf().mo5608dq().mo3214p(f8463bM);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Handle")
    @C0064Am(aul = "6feb1c2fed2a3f27ace193134d8167f3", aum = 0)
    @C5566aOg
    /* renamed from: b */
    private void m34563b(String str) {
        throw new aWi(new aCE(this, f8467bQ, new Object[]{str}));
    }

    /* renamed from: c */
    private void m34565c(UUID uuid) {
        switch (bFf().mo6893i(f8465bO)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f8465bO, new Object[]{uuid}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f8465bO, new Object[]{uuid}));
                break;
        }
        m34564b(uuid);
    }

    /* renamed from: ca */
    private void m34566ca(long j) {
        bFf().mo5608dq().mo3184b(asX, j);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Cloning price per unit")
    /* renamed from: JL */
    public long mo20141JL() {
        switch (bFf().mo6893i(asY)) {
            case 0:
                return 0;
            case 2:
                return ((Long) bFf().mo5606d(new aCE(this, asY, new Object[0]))).longValue();
            case 3:
                bFf().mo5606d(new aCE(this, asY, new Object[0]));
                break;
        }
        return m34555JK();
    }

    /* renamed from: T */
    public C6494any mo11T() {
        return ___iScriptClass;
    }

    /* renamed from: U */
    public C2491fm[] mo12U() {
        return _m_methods;
    }

    /* renamed from: W */
    public Object mo13W() {
        return new C0675Jb(this);
    }

    /* renamed from: a */
    public Object mo14a(C0495Gr gr) {
        Object[] args = gr.getArgs();
        switch (gr.mo2417hq() - TaikodomObject._m_methodCount) {
            case 0:
                return m34561ap();
            case 1:
                m34564b((UUID) args[0]);
                return null;
            case 2:
                return m34562ar();
            case 3:
                m34563b((String) args[0]);
                return null;
            case 4:
                return new Long(m34555JK());
            case 5:
                m34567cb(((Long) args[0]).longValue());
                return null;
            default:
                return super.mo14a(gr);
        }
    }

    /* renamed from: a */
    public void mo15a(Collection collection, C0495Gr gr) {
        super.mo15a(collection, gr);
    }

    /* renamed from: aq */
    public UUID mo18aq() {
        switch (bFf().mo6893i(f8464bN)) {
            case 0:
                return null;
            case 2:
                return (UUID) bFf().mo5606d(new aCE(this, f8464bN, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f8464bN, new Object[0]));
                break;
        }
        return m34561ap();
    }

    /* renamed from: c */
    public C5663aRz[] mo25c() {
        return _m_fields;
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Cloning price per unit")
    /* renamed from: cc */
    public void mo20142cc(long j) {
        switch (bFf().mo6893i(asZ)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, asZ, new Object[]{new Long(j)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, asZ, new Object[]{new Long(j)}));
                break;
        }
        m34567cb(j);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Handle")
    public String getHandle() {
        switch (bFf().mo6893i(f8466bP)) {
            case 0:
                return null;
            case 2:
                return (String) bFf().mo5606d(new aCE(this, f8466bP, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f8466bP, new Object[0]));
                break;
        }
        return m34562ar();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Handle")
    @C5566aOg
    public void setHandle(String str) {
        switch (bFf().mo6893i(f8467bQ)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f8467bQ, new Object[]{str}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f8467bQ, new Object[]{str}));
                break;
        }
        m34563b(str);
    }

    @C0064Am(aul = "26d9f4537ec30a36fb979ee7156fc119", aum = 0)
    /* renamed from: ap */
    private UUID m34561ap() {
        return m34559an();
    }

    @C0064Am(aul = "3dd7001b44f251b038f563c4a6eda588", aum = 0)
    /* renamed from: b */
    private void m34564b(UUID uuid) {
        m34558a(uuid);
    }

    /* renamed from: S */
    public void mo10S() {
        super.mo10S();
        m34558a(UUID.randomUUID());
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Handle")
    @C0064Am(aul = "4eba51ac3670d628b5a874e8b31bd835", aum = 0)
    /* renamed from: ar */
    private String m34562ar() {
        return m34560ao();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Cloning price per unit")
    @C0064Am(aul = "45e6095b83320b8236376f4f1e22f09a", aum = 0)
    /* renamed from: JK */
    private long m34555JK() {
        return m34554JJ();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Cloning price per unit")
    @C0064Am(aul = "b364eadd2fdd353cd9b318a0a7483c4e", aum = 0)
    /* renamed from: cb */
    private void m34567cb(long j) {
        m34566ca(j);
    }
}
