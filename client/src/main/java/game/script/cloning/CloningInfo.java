package game.script.cloning;

import game.network.manager.C6546aoy;
import game.network.message.externalizable.aCE;
import game.network.message.serializable.C1556Wo;
import game.script.player.Player;
import game.script.ship.Station;
import logic.baa.C1616Xf;
import logic.baa.C1634Xv;
import logic.baa.C5511aMd;
import logic.baa.C6485anp;
import logic.data.mbean.C0299Ds;
import logic.res.code.C5640aRc;
import logic.res.code.C5663aRz;
import logic.res.code.C6494any;
import logic.res.html.C2491fm;
import p001a.*;

import java.util.Collection;

@C5511aMd
@C6485anp
/* renamed from: a.TI */
/* compiled from: a */
public class CloningInfo extends TaikodomObject implements C1616Xf {
    /* renamed from: RH */
    public static final C5663aRz f1699RH = null;
    /* renamed from: Sw */
    public static final C2491fm f1700Sw = null;
    /* renamed from: Sx */
    public static final C2491fm f1701Sx = null;
    public static final C2491fm _f_dispose_0020_0028_0029V = null;
    public static final int _m_fieldCount = 0;
    public static final C5663aRz[] _m_fields = null;
    public static final int _m_methodCount = 0;
    public static final C2491fm[] _m_methods = null;
    /* renamed from: hz */
    public static final C5663aRz f1702hz = null;
    public static final C5663aRz ioS = null;
    public static final C5663aRz ioT = null;
    public static final C2491fm ioU = null;
    public static final C2491fm ioV = null;
    public static final C2491fm ioW = null;
    public static final C2491fm ioX = null;
    public static final C2491fm ioY = null;
    public static final C2491fm ioZ = null;
    public static final C2491fm ipa = null;
    public static final long serialVersionUID = 0;
    public static C6494any ___iScriptClass;
    @C0064Am(aul = "63324418db55bf8438f8ac6752855714", aum = 0)

    /* renamed from: P */
    private static Player f1697P;
    @C0064Am(aul = "7bbae1f0a3220875be9e7b493eeb0e18", aum = 2)

    /* renamed from: RG */
    private static Station f1698RG;
    @C0064Am(aul = "01a2f7e2e2d5e3a02e9c1a03eb38a354", aum = 1)
    private static C1556Wo<Station, Integer> cGU;
    @C0064Am(aul = "367a18845e9af9e9aff4f3ab9931ba4d", aum = 3)
    private static Station cGV;

    static {
        m9906V();
    }

    public CloningInfo() {
        super((C5540aNg) null);
        super.mo10S();
    }

    public CloningInfo(C5540aNg ang) {
        super(ang);
    }

    public CloningInfo(Player aku, Station bf) {
        super((C5540aNg) null);
        super._m_script_init(aku, bf);
    }

    /* renamed from: V */
    static void m9906V() {
        serialVersionUID = (long) 1;
        _m_fieldCount = TaikodomObject._m_fieldCount + 4;
        _m_methodCount = TaikodomObject._m_methodCount + 10;
        int i = TaikodomObject._m_fieldCount;
        C5663aRz[] arzArr = new C5663aRz[(i + 4)];
        C5663aRz b = C5640aRc.m17844b(CloningInfo.class, "63324418db55bf8438f8ac6752855714", i);
        f1702hz = b;
        arzArr[i] = b;
        int i2 = i + 1;
        C5663aRz b2 = C5640aRc.m17844b(CloningInfo.class, "01a2f7e2e2d5e3a02e9c1a03eb38a354", i2);
        ioS = b2;
        arzArr[i2] = b2;
        int i3 = i2 + 1;
        C5663aRz b3 = C5640aRc.m17844b(CloningInfo.class, "7bbae1f0a3220875be9e7b493eeb0e18", i3);
        f1699RH = b3;
        arzArr[i3] = b3;
        int i4 = i3 + 1;
        C5663aRz b4 = C5640aRc.m17844b(CloningInfo.class, "367a18845e9af9e9aff4f3ab9931ba4d", i4);
        ioT = b4;
        arzArr[i4] = b4;
        int i5 = i4 + 1;
        _m_fields = arzArr;
        C1634Xv.m11725a((Object[]) TaikodomObject._m_fields, (Object[]) _m_fields);
        int i6 = TaikodomObject._m_methodCount;
        C2491fm[] fmVarArr = new C2491fm[(i6 + 10)];
        C2491fm a = C4105zY.m41624a(CloningInfo.class, "82f5a25fc10a7bd4516a765e39bd8d42", i6);
        _f_dispose_0020_0028_0029V = a;
        fmVarArr[i6] = a;
        int i7 = i6 + 1;
        C2491fm a2 = C4105zY.m41624a(CloningInfo.class, "d35f9115bae46c15a9e5fe83acf0db7f", i7);
        f1700Sw = a2;
        fmVarArr[i7] = a2;
        int i8 = i7 + 1;
        C2491fm a3 = C4105zY.m41624a(CloningInfo.class, "60ce33341de5bbe9835312de37589119", i8);
        ioU = a3;
        fmVarArr[i8] = a3;
        int i9 = i8 + 1;
        C2491fm a4 = C4105zY.m41624a(CloningInfo.class, "bb0e36100a6071c56e81adb0fa9d4102", i9);
        ioV = a4;
        fmVarArr[i9] = a4;
        int i10 = i9 + 1;
        C2491fm a5 = C4105zY.m41624a(CloningInfo.class, "03347633ab01eeded5005cfbd954e50f", i10);
        ioW = a5;
        fmVarArr[i10] = a5;
        int i11 = i10 + 1;
        C2491fm a6 = C4105zY.m41624a(CloningInfo.class, "5b0be10e77cc4a888e3adcfe8c30f26d", i11);
        ioX = a6;
        fmVarArr[i11] = a6;
        int i12 = i11 + 1;
        C2491fm a7 = C4105zY.m41624a(CloningInfo.class, "56f1eb31bd74212dd6de857d9848f9b4", i12);
        ioY = a7;
        fmVarArr[i12] = a7;
        int i13 = i12 + 1;
        C2491fm a8 = C4105zY.m41624a(CloningInfo.class, "fc77731af3938bd92b68be8e8ad0ca79", i13);
        ioZ = a8;
        fmVarArr[i13] = a8;
        int i14 = i13 + 1;
        C2491fm a9 = C4105zY.m41624a(CloningInfo.class, "79f3b43984f906e0c29e929f4ac4fc80", i14);
        ipa = a9;
        fmVarArr[i14] = a9;
        int i15 = i14 + 1;
        C2491fm a10 = C4105zY.m41624a(CloningInfo.class, "5a920fc972f64cd5eb0465fe4a905ed0", i15);
        f1701Sx = a10;
        fmVarArr[i15] = a10;
        int i16 = i15 + 1;
        _m_methods = fmVarArr;
        C1634Xv.m11725a((Object[]) TaikodomObject._m_methods, (Object[]) _m_methods);
        ___iScriptClass = aUO.m18566a(CloningInfo.class, C0299Ds.class, _m_fields, _m_methods);
    }

    @C0064Am(aul = "fc77731af3938bd92b68be8e8ad0ca79", aum = 0)
    @C5566aOg
    @C2499fr
    /* renamed from: a */
    private void m9907a(Station bf, int i) {
        throw new aWi(new aCE(this, ioZ, new Object[]{bf, new Integer(i)}));
    }

    /* renamed from: a */
    private void m9908a(Player aku) {
        bFf().mo5608dq().mo3197f(f1702hz, aku);
    }

    /* renamed from: aA */
    private boolean m9909aA(Station bf) {
        switch (bFf().mo6893i(ioY)) {
            case 0:
                return false;
            case 2:
                return ((Boolean) bFf().mo5606d(new aCE(this, ioY, new Object[]{bf}))).booleanValue();
            case 3:
                bFf().mo5606d(new aCE(this, ioY, new Object[]{bf}));
                break;
        }
        return m9915az(bf);
    }

    /* renamed from: am */
    private void m9910am(C1556Wo wo) {
        bFf().mo5608dq().mo3197f(ioS, wo);
    }

    /* renamed from: as */
    private void m9911as(Station bf) {
        bFf().mo5608dq().mo3197f(ioT, bf);
    }

    @C0064Am(aul = "bb0e36100a6071c56e81adb0fa9d4102", aum = 0)
    @C5566aOg
    @C2499fr
    /* renamed from: at */
    private void m9912at(Station bf) {
        throw new aWi(new aCE(this, ioV, new Object[]{bf}));
    }

    @C0064Am(aul = "5b0be10e77cc4a888e3adcfe8c30f26d", aum = 0)
    @C5566aOg
    /* renamed from: ax */
    private void m9914ax(Station bf) {
        throw new aWi(new aCE(this, ioX, new Object[]{bf}));
    }

    /* renamed from: b */
    private void m9916b(Station bf) {
        bFf().mo5608dq().mo3197f(f1699RH, bf);
    }

    @C0064Am(aul = "5a920fc972f64cd5eb0465fe4a905ed0", aum = 0)
    @C5566aOg
    /* renamed from: c */
    private void m9917c(Station bf) {
        throw new aWi(new aCE(this, f1701Sx, new Object[]{bf}));
    }

    /* renamed from: dG */
    private Player m9918dG() {
        return (Player) bFf().mo5608dq().mo3214p(f1702hz);
    }

    private C1556Wo djh() {
        return (C1556Wo) bFf().mo5608dq().mo3214p(ioS);
    }

    private Station dji() {
        return (Station) bFf().mo5608dq().mo3214p(ioT);
    }

    /* renamed from: wG */
    private Station m9920wG() {
        return (Station) bFf().mo5608dq().mo3214p(f1699RH);
    }

    /* renamed from: S */
    public void mo10S() {
        super.mo10S();
    }

    /* renamed from: T */
    public C6494any mo11T() {
        return ___iScriptClass;
    }

    /* renamed from: U */
    public C2491fm[] mo12U() {
        return _m_methods;
    }

    /* renamed from: W */
    public Object mo13W() {
        return new C0299Ds(this);
    }

    /* renamed from: a */
    public Object mo14a(C0495Gr gr) {
        Object[] args = gr.getArgs();
        switch (gr.mo2417hq() - TaikodomObject._m_methodCount) {
            case 0:
                m9919fg();
                return null;
            case 1:
                return m9921xg();
            case 2:
                return djj();
            case 3:
                m9912at((Station) args[0]);
                return null;
            case 4:
                return new Integer(m9913av((Station) args[0]));
            case 5:
                m9914ax((Station) args[0]);
                return null;
            case 6:
                return new Boolean(m9915az((Station) args[0]));
            case 7:
                m9907a((Station) args[0], ((Integer) args[1]).intValue());
                return null;
            case 8:
                return djl();
            case 9:
                m9917c((Station) args[0]);
                return null;
            default:
                return super.mo14a(gr);
        }
    }

    /* renamed from: a */
    public void mo15a(Collection collection, C0495Gr gr) {
        super.mo15a(collection, gr);
    }

    @C5566aOg
    @C2499fr
    /* renamed from: au */
    public void mo5645au(Station bf) {
        switch (bFf().mo6893i(ioV)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, ioV, new Object[]{bf}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, ioV, new Object[]{bf}));
                break;
        }
        m9912at(bf);
    }

    /* renamed from: aw */
    public int mo5646aw(Station bf) {
        switch (bFf().mo6893i(ioW)) {
            case 0:
                return 0;
            case 2:
                return ((Integer) bFf().mo5606d(new aCE(this, ioW, new Object[]{bf}))).intValue();
            case 3:
                bFf().mo5606d(new aCE(this, ioW, new Object[]{bf}));
                break;
        }
        return m9913av(bf);
    }

    @C5566aOg
    /* renamed from: ay */
    public void mo5647ay(Station bf) {
        switch (bFf().mo6893i(ioX)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, ioX, new Object[]{bf}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, ioX, new Object[]{bf}));
                break;
        }
        m9914ax(bf);
    }

    @C5566aOg
    @C2499fr
    /* renamed from: b */
    public void mo5648b(Station bf, int i) {
        switch (bFf().mo6893i(ioZ)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, ioZ, new Object[]{bf, new Integer(i)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, ioZ, new Object[]{bf, new Integer(i)}));
                break;
        }
        m9907a(bf, i);
    }

    /* renamed from: c */
    public C5663aRz[] mo25c() {
        return _m_fields;
    }

    @C5566aOg
    /* renamed from: d */
    public void mo5649d(Station bf) {
        switch (bFf().mo6893i(f1701Sx)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f1701Sx, new Object[]{bf}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f1701Sx, new Object[]{bf}));
                break;
        }
        m9917c(bf);
    }

    public void dispose() {
        switch (bFf().mo6893i(_f_dispose_0020_0028_0029V)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, _f_dispose_0020_0028_0029V, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, _f_dispose_0020_0028_0029V, new Object[0]));
                break;
        }
        m9919fg();
    }

    public Station djk() {
        switch (bFf().mo6893i(ioU)) {
            case 0:
                return null;
            case 2:
                return (Station) bFf().mo5606d(new aCE(this, ioU, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, ioU, new Object[0]));
                break;
        }
        return djj();
    }

    public Collection<Station> djm() {
        switch (bFf().mo6893i(ipa)) {
            case 0:
                return null;
            case 2:
                return (Collection) bFf().mo5606d(new aCE(this, ipa, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, ipa, new Object[0]));
                break;
        }
        return djl();
    }

    /* renamed from: xh */
    public Station mo5652xh() {
        switch (bFf().mo6893i(f1700Sw)) {
            case 0:
                return null;
            case 2:
                return (Station) bFf().mo5606d(new aCE(this, f1700Sw, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f1700Sw, new Object[0]));
                break;
        }
        return m9921xg();
    }

    /* renamed from: a */
    public void mo5644a(Player aku, Station bf) {
        super.mo10S();
        m9908a(aku);
        m9916b(bf);
        m9911as(m9920wG());
    }

    @C0064Am(aul = "82f5a25fc10a7bd4516a765e39bd8d42", aum = 0)
    /* renamed from: fg */
    private void m9919fg() {
        m9908a((Player) null);
        djh().clear();
        m9916b((Station) null);
        m9911as((Station) null);
        super.dispose();
    }

    @C0064Am(aul = "d35f9115bae46c15a9e5fe83acf0db7f", aum = 0)
    /* renamed from: xg */
    private Station m9921xg() {
        return m9920wG();
    }

    @C0064Am(aul = "60ce33341de5bbe9835312de37589119", aum = 0)
    private Station djj() {
        return dji();
    }

    @C0064Am(aul = "03347633ab01eeded5005cfbd954e50f", aum = 0)
    /* renamed from: av */
    private int m9913av(Station bf) {
        if (djh().containsKey(bf)) {
            return ((Integer) djh().get(bf)).intValue();
        }
        return 0;
    }

    @C0064Am(aul = "56f1eb31bd74212dd6de857d9848f9b4", aum = 0)
    /* renamed from: az */
    private boolean m9915az(Station bf) {
        if (bf == m9920wG()) {
            return true;
        }
        if (!djh().containsKey(bf)) {
            throw new IllegalStateException("There should be clones for player " + m9918dG().getName() + " in Station " + bf.getName());
        }
        int intValue = ((Integer) djh().get(bf)).intValue();
        if (intValue < 1) {
            throw new IllegalStateException("There should be clones for player " + m9918dG().getName() + " in Station " + bf.getName());
        } else if (intValue == 1) {
            djh().remove(bf);
            m9911as(m9920wG());
            return false;
        } else {
            djh().put(bf, Integer.valueOf(intValue - 1));
            return true;
        }
    }

    @C0064Am(aul = "79f3b43984f906e0c29e929f4ac4fc80", aum = 0)
    private Collection<Station> djl() {
        return djh().keySet();
    }

    @C6546aoy
    /* renamed from: a.TI$a */
    public static class C1318a extends Exception {
    }
}
