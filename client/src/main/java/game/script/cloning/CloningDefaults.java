package game.script.cloning;

import game.network.message.externalizable.aCE;
import game.script.item.WeaponType;
import game.script.mission.MissionTemplate;
import game.script.npc.NPC;
import game.script.resource.Asset;
import game.script.ship.ShipType;
import game.script.ship.Station;
import logic.baa.*;
import logic.data.mbean.C1357Tl;
import logic.res.code.C5640aRc;
import logic.res.code.C5663aRz;
import logic.res.code.C6494any;
import logic.res.html.C2491fm;
import p001a.*;

import java.util.Collection;
import java.util.UUID;

@C5511aMd
@C6485anp
/* renamed from: a.adS  reason: case insensitive filesystem */
/* compiled from: a */
public class CloningDefaults extends aDJ implements C0468GU, C1616Xf {
    public static final int _m_fieldCount = 0;
    public static final C5663aRz[] _m_fields = null;
    public static final int _m_methodCount = 0;
    public static final C2491fm[] _m_methods = null;
    public static final C2491fm aKm = null;
    /* renamed from: bL */
    public static final C5663aRz f4319bL = null;
    /* renamed from: bN */
    public static final C2491fm f4320bN = null;
    /* renamed from: bO */
    public static final C2491fm f4321bO = null;
    /* renamed from: bP */
    public static final C2491fm f4322bP = null;
    public static final C5663aRz flG = null;
    public static final C5663aRz flH = null;
    public static final C5663aRz flI = null;
    public static final C5663aRz flJ = null;
    public static final C5663aRz flK = null;
    public static final C5663aRz flL = null;
    public static final C5663aRz flM = null;
    public static final C5663aRz flN = null;
    public static final C5663aRz flO = null;
    public static final C5663aRz flP = null;
    public static final C5663aRz flQ = null;
    public static final C5663aRz flR = null;
    public static final C5663aRz flS = null;
    public static final C2491fm flT = null;
    public static final C2491fm flU = null;
    public static final C2491fm flV = null;
    public static final C2491fm flW = null;
    public static final C2491fm flX = null;
    public static final C2491fm flY = null;
    public static final C2491fm flZ = null;
    public static final C2491fm fma = null;
    public static final C2491fm fmb = null;
    public static final C2491fm fmc = null;
    public static final C2491fm fmd = null;
    public static final C2491fm fme = null;
    public static final C2491fm fmf = null;
    public static final C2491fm fmg = null;
    public static final C2491fm fmh = null;
    public static final C2491fm fmi = null;
    public static final C2491fm fmj = null;
    public static final C2491fm fmk = null;
    public static final C2491fm fml = null;
    public static final C2491fm fmm = null;
    public static final C2491fm fmn = null;
    public static final C2491fm fmo = null;
    public static final C2491fm fmp = null;
    public static final C2491fm fmq = null;
    public static final C2491fm fmr = null;
    public static final C2491fm fms = null;
    public static final long serialVersionUID = 0;
    public static C6494any ___iScriptClass;
    @C0064Am(aul = "1553d1a9a2406c11ab0fdb28e7f96636", aum = 13)

    /* renamed from: bK */
    private static UUID f4318bK;
    @C0064Am(aul = "4f3d808ae2e81c5b9903d5b3ea57aa4d", aum = 0)
    private static Station eie;
    @C0064Am(aul = "6f9d0e6fd9b75de1fda725cfaec373b7", aum = 1)
    private static Station eif;
    @C0064Am(aul = "cf5715530a6970f86b1f72d24734e44c", aum = 2)
    private static Asset eig;
    @C0064Am(aul = "d3182ec68657a9cf606144250f45ae33", aum = 3)
    private static Asset eih;
    @C0064Am(aul = "c606a3ef0719dfac8d8c25ec170dbe9f", aum = 4)
    private static NPC eii;
    @C0064Am(aul = "3a6bb013438d2e1f0527bb9aa263403c", aum = 5)
    private static NPC eij;
    @C0064Am(aul = "4aa4ff338e56ccb5aa7a5b36155e5c1d", aum = 6)
    private static MissionTemplate eik;
    @C0064Am(aul = "604a284e6c32db1c4766121903c0f67b", aum = 7)
    private static MissionTemplate eil;
    @C0064Am(aul = "a5144cb76b15286de0656c65e0b50735", aum = 8)
    private static ShipType eim;
    @C0064Am(aul = "7b4759c5f86996cd2cfb138a0c618a71", aum = 9)
    private static ShipType ein;
    @C0064Am(aul = "9447344e598bda92f83e9ffad7364feb", aum = 10)
    private static WeaponType eio;
    @C0064Am(aul = "fd6ca174ecf9f49b9d0d00346a68b117", aum = 11)
    private static WeaponType eip;
    @C0064Am(aul = "897a3b81038d80816d4208ab7ef3fd70", aum = 12)
    private static int eiq;

    static {
        m20846V();
    }

    public CloningDefaults() {
        super((C5540aNg) null);
        super.mo10S();
    }

    public CloningDefaults(C5540aNg ang) {
        super(ang);
    }

    /* renamed from: V */
    static void m20846V() {
        serialVersionUID = (long) 1;
        _m_fieldCount = aDJ._m_fieldCount + 14;
        _m_methodCount = aDJ._m_methodCount + 30;
        int i = aDJ._m_fieldCount;
        C5663aRz[] arzArr = new C5663aRz[(i + 14)];
        C5663aRz b = C5640aRc.m17844b(CloningDefaults.class, "4f3d808ae2e81c5b9903d5b3ea57aa4d", i);
        flG = b;
        arzArr[i] = b;
        int i2 = i + 1;
        C5663aRz b2 = C5640aRc.m17844b(CloningDefaults.class, "6f9d0e6fd9b75de1fda725cfaec373b7", i2);
        flH = b2;
        arzArr[i2] = b2;
        int i3 = i2 + 1;
        C5663aRz b3 = C5640aRc.m17844b(CloningDefaults.class, "cf5715530a6970f86b1f72d24734e44c", i3);
        flI = b3;
        arzArr[i3] = b3;
        int i4 = i3 + 1;
        C5663aRz b4 = C5640aRc.m17844b(CloningDefaults.class, "d3182ec68657a9cf606144250f45ae33", i4);
        flJ = b4;
        arzArr[i4] = b4;
        int i5 = i4 + 1;
        C5663aRz b5 = C5640aRc.m17844b(CloningDefaults.class, "c606a3ef0719dfac8d8c25ec170dbe9f", i5);
        flK = b5;
        arzArr[i5] = b5;
        int i6 = i5 + 1;
        C5663aRz b6 = C5640aRc.m17844b(CloningDefaults.class, "3a6bb013438d2e1f0527bb9aa263403c", i6);
        flL = b6;
        arzArr[i6] = b6;
        int i7 = i6 + 1;
        C5663aRz b7 = C5640aRc.m17844b(CloningDefaults.class, "4aa4ff338e56ccb5aa7a5b36155e5c1d", i7);
        flM = b7;
        arzArr[i7] = b7;
        int i8 = i7 + 1;
        C5663aRz b8 = C5640aRc.m17844b(CloningDefaults.class, "604a284e6c32db1c4766121903c0f67b", i8);
        flN = b8;
        arzArr[i8] = b8;
        int i9 = i8 + 1;
        C5663aRz b9 = C5640aRc.m17844b(CloningDefaults.class, "a5144cb76b15286de0656c65e0b50735", i9);
        flO = b9;
        arzArr[i9] = b9;
        int i10 = i9 + 1;
        C5663aRz b10 = C5640aRc.m17844b(CloningDefaults.class, "7b4759c5f86996cd2cfb138a0c618a71", i10);
        flP = b10;
        arzArr[i10] = b10;
        int i11 = i10 + 1;
        C5663aRz b11 = C5640aRc.m17844b(CloningDefaults.class, "9447344e598bda92f83e9ffad7364feb", i11);
        flQ = b11;
        arzArr[i11] = b11;
        int i12 = i11 + 1;
        C5663aRz b12 = C5640aRc.m17844b(CloningDefaults.class, "fd6ca174ecf9f49b9d0d00346a68b117", i12);
        flR = b12;
        arzArr[i12] = b12;
        int i13 = i12 + 1;
        C5663aRz b13 = C5640aRc.m17844b(CloningDefaults.class, "897a3b81038d80816d4208ab7ef3fd70", i13);
        flS = b13;
        arzArr[i13] = b13;
        int i14 = i13 + 1;
        C5663aRz b14 = C5640aRc.m17844b(CloningDefaults.class, "1553d1a9a2406c11ab0fdb28e7f96636", i14);
        f4319bL = b14;
        arzArr[i14] = b14;
        int i15 = i14 + 1;
        _m_fields = arzArr;
        C1634Xv.m11725a((Object[]) aDJ._m_fields, (Object[]) _m_fields);
        int i16 = aDJ._m_methodCount;
        C2491fm[] fmVarArr = new C2491fm[(i16 + 30)];
        C2491fm a = C4105zY.m41624a(CloningDefaults.class, "12edb1f6313320da8e83e1bc8712eba5", i16);
        f4320bN = a;
        fmVarArr[i16] = a;
        int i17 = i16 + 1;
        C2491fm a2 = C4105zY.m41624a(CloningDefaults.class, "0f4a91026456f56f452cc86422dc1e19", i17);
        f4321bO = a2;
        fmVarArr[i17] = a2;
        int i18 = i17 + 1;
        C2491fm a3 = C4105zY.m41624a(CloningDefaults.class, "9382b18dacaf711d1e8af6355adb429c", i18);
        f4322bP = a3;
        fmVarArr[i18] = a3;
        int i19 = i18 + 1;
        C2491fm a4 = C4105zY.m41624a(CloningDefaults.class, "6c4ae24814e5d62ddb1bc48a6ceb23ab", i19);
        flT = a4;
        fmVarArr[i19] = a4;
        int i20 = i19 + 1;
        C2491fm a5 = C4105zY.m41624a(CloningDefaults.class, "495bb3d1fb2e7e09b1b5f3a70b18be85", i20);
        flU = a5;
        fmVarArr[i20] = a5;
        int i21 = i20 + 1;
        C2491fm a6 = C4105zY.m41624a(CloningDefaults.class, "5367d71689c11d94ebf25b98fbb15e01", i21);
        flV = a6;
        fmVarArr[i21] = a6;
        int i22 = i21 + 1;
        C2491fm a7 = C4105zY.m41624a(CloningDefaults.class, "ed337b3583e7cd169bb585a73d207063", i22);
        flW = a7;
        fmVarArr[i22] = a7;
        int i23 = i22 + 1;
        C2491fm a8 = C4105zY.m41624a(CloningDefaults.class, "2bfbbaf50c88657f623fc910ccb5305c", i23);
        flX = a8;
        fmVarArr[i23] = a8;
        int i24 = i23 + 1;
        C2491fm a9 = C4105zY.m41624a(CloningDefaults.class, "e33d0571aa187e5e1deaa17f199523e3", i24);
        flY = a9;
        fmVarArr[i24] = a9;
        int i25 = i24 + 1;
        C2491fm a10 = C4105zY.m41624a(CloningDefaults.class, "bd10615905926ff6d9719b2eab170206", i25);
        flZ = a10;
        fmVarArr[i25] = a10;
        int i26 = i25 + 1;
        C2491fm a11 = C4105zY.m41624a(CloningDefaults.class, "df4cf36246091d5fe1d6a3133187061c", i26);
        fma = a11;
        fmVarArr[i26] = a11;
        int i27 = i26 + 1;
        C2491fm a12 = C4105zY.m41624a(CloningDefaults.class, "ed2fe338a1f7e3b24e96bacfc24acf74", i27);
        fmb = a12;
        fmVarArr[i27] = a12;
        int i28 = i27 + 1;
        C2491fm a13 = C4105zY.m41624a(CloningDefaults.class, "553d7b4b0c85ac27c10f92b03b28a4f9", i28);
        fmc = a13;
        fmVarArr[i28] = a13;
        int i29 = i28 + 1;
        C2491fm a14 = C4105zY.m41624a(CloningDefaults.class, "c45ca9da64c9b9164db44d1c450d3c67", i29);
        fmd = a14;
        fmVarArr[i29] = a14;
        int i30 = i29 + 1;
        C2491fm a15 = C4105zY.m41624a(CloningDefaults.class, "df4fe443ca4ca3ecf71536cd86d60dc1", i30);
        fme = a15;
        fmVarArr[i30] = a15;
        int i31 = i30 + 1;
        C2491fm a16 = C4105zY.m41624a(CloningDefaults.class, "44819ef3ef6b384bb666204818ff0b24", i31);
        fmf = a16;
        fmVarArr[i31] = a16;
        int i32 = i31 + 1;
        C2491fm a17 = C4105zY.m41624a(CloningDefaults.class, "21a1afc49ec597db8089e858ab768f2b", i32);
        fmg = a17;
        fmVarArr[i32] = a17;
        int i33 = i32 + 1;
        C2491fm a18 = C4105zY.m41624a(CloningDefaults.class, "19b8abf1c08eed942bd072ae5eb21fed", i33);
        fmh = a18;
        fmVarArr[i33] = a18;
        int i34 = i33 + 1;
        C2491fm a19 = C4105zY.m41624a(CloningDefaults.class, "6e5c2c0a61ab30dd9c9a1e6851655537", i34);
        fmi = a19;
        fmVarArr[i34] = a19;
        int i35 = i34 + 1;
        C2491fm a20 = C4105zY.m41624a(CloningDefaults.class, "773c9e52b626317217c69ace783a4474", i35);
        fmj = a20;
        fmVarArr[i35] = a20;
        int i36 = i35 + 1;
        C2491fm a21 = C4105zY.m41624a(CloningDefaults.class, "7358952e6390706a30855e838d8b4fe4", i36);
        fmk = a21;
        fmVarArr[i36] = a21;
        int i37 = i36 + 1;
        C2491fm a22 = C4105zY.m41624a(CloningDefaults.class, "1d4103b831fd4678c37e1281a42de379", i37);
        fml = a22;
        fmVarArr[i37] = a22;
        int i38 = i37 + 1;
        C2491fm a23 = C4105zY.m41624a(CloningDefaults.class, "da382cc091cd4234c7251ce19dcdfb88", i38);
        fmm = a23;
        fmVarArr[i38] = a23;
        int i39 = i38 + 1;
        C2491fm a24 = C4105zY.m41624a(CloningDefaults.class, "f31473f1b2f386ca48f8ef4bc23b535e", i39);
        fmn = a24;
        fmVarArr[i39] = a24;
        int i40 = i39 + 1;
        C2491fm a25 = C4105zY.m41624a(CloningDefaults.class, "99ce6ed317462d6af8526345eb22b57e", i40);
        fmo = a25;
        fmVarArr[i40] = a25;
        int i41 = i40 + 1;
        C2491fm a26 = C4105zY.m41624a(CloningDefaults.class, "bf1c1c5b5ab92cc42eec50396017eb68", i41);
        fmp = a26;
        fmVarArr[i41] = a26;
        int i42 = i41 + 1;
        C2491fm a27 = C4105zY.m41624a(CloningDefaults.class, "f689706d8077ed127c18bcf26d86e9bb", i42);
        fmq = a27;
        fmVarArr[i42] = a27;
        int i43 = i42 + 1;
        C2491fm a28 = C4105zY.m41624a(CloningDefaults.class, "17f43a08f9f88cd05310e05ea68acf36", i43);
        fmr = a28;
        fmVarArr[i43] = a28;
        int i44 = i43 + 1;
        C2491fm a29 = C4105zY.m41624a(CloningDefaults.class, "efa89f94db8477ed03c0db3ebb8b0277", i44);
        fms = a29;
        fmVarArr[i44] = a29;
        int i45 = i44 + 1;
        C2491fm a30 = C4105zY.m41624a(CloningDefaults.class, "63d701ffb93c1f951ddbd965e679ab91", i45);
        aKm = a30;
        fmVarArr[i45] = a30;
        int i46 = i45 + 1;
        _m_methods = fmVarArr;
        C1634Xv.m11725a((Object[]) aDJ._m_methods, (Object[]) _m_methods);
        ___iScriptClass = aUO.m18566a(CloningDefaults.class, C1357Tl.class, _m_fields, _m_methods);
    }

    /* renamed from: Q */
    private void m20841Q(Station bf) {
        bFf().mo5608dq().mo3197f(flG, bf);
    }

    /* renamed from: R */
    private void m20843R(Station bf) {
        bFf().mo5608dq().mo3197f(flH, bf);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Default Station 1")
    @C0064Am(aul = "495bb3d1fb2e7e09b1b5f3a70b18be85", aum = 0)
    @C5566aOg
    /* renamed from: S */
    private void m20844S(Station bf) {
        throw new aWi(new aCE(this, flU, new Object[]{bf}));
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Default Station 2")
    @C0064Am(aul = "ed337b3583e7cd169bb585a73d207063", aum = 0)
    @C5566aOg
    /* renamed from: U */
    private void m20845U(Station bf) {
        throw new aWi(new aCE(this, flW, new Object[]{bf}));
    }

    /* renamed from: a */
    private void m20847a(UUID uuid) {
        bFf().mo5608dq().mo3197f(f4319bL, uuid);
    }

    /* renamed from: an */
    private UUID m20848an() {
        return (UUID) bFf().mo5608dq().mo3214p(f4319bL);
    }

    private NPC bSA() {
        return (NPC) bFf().mo5608dq().mo3214p(flK);
    }

    private NPC bSB() {
        return (NPC) bFf().mo5608dq().mo3214p(flL);
    }

    private MissionTemplate bSC() {
        return (MissionTemplate) bFf().mo5608dq().mo3214p(flM);
    }

    private MissionTemplate bSD() {
        return (MissionTemplate) bFf().mo5608dq().mo3214p(flN);
    }

    private ShipType bSE() {
        return (ShipType) bFf().mo5608dq().mo3214p(flO);
    }

    private ShipType bSF() {
        return (ShipType) bFf().mo5608dq().mo3214p(flP);
    }

    private WeaponType bSG() {
        return (WeaponType) bFf().mo5608dq().mo3214p(flQ);
    }

    private WeaponType bSH() {
        return (WeaponType) bFf().mo5608dq().mo3214p(flR);
    }

    private int bSI() {
        return bFf().mo5608dq().mo3212n(flS);
    }

    private Station bSw() {
        return (Station) bFf().mo5608dq().mo3214p(flG);
    }

    private Station bSx() {
        return (Station) bFf().mo5608dq().mo3214p(flH);
    }

    private Asset bSy() {
        return (Asset) bFf().mo5608dq().mo3214p(flI);
    }

    private Asset bSz() {
        return (Asset) bFf().mo5608dq().mo3214p(flJ);
    }

    /* renamed from: bk */
    private void m20852bk(Asset tCVar) {
        bFf().mo5608dq().mo3197f(flI, tCVar);
    }

    /* renamed from: bl */
    private void m20853bl(Asset tCVar) {
        bFf().mo5608dq().mo3197f(flJ, tCVar);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Default Station Image 1")
    @C0064Am(aul = "e33d0571aa187e5e1deaa17f199523e3", aum = 0)
    @C5566aOg
    /* renamed from: bm */
    private void m20854bm(Asset tCVar) {
        throw new aWi(new aCE(this, flY, new Object[]{tCVar}));
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Default Station Image 2")
    @C0064Am(aul = "df4cf36246091d5fe1d6a3133187061c", aum = 0)
    @C5566aOg
    /* renamed from: bo */
    private void m20855bo(Asset tCVar) {
        throw new aWi(new aCE(this, fma, new Object[]{tCVar}));
    }

    /* renamed from: c */
    private void m20856c(UUID uuid) {
        switch (bFf().mo6893i(f4321bO)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f4321bO, new Object[]{uuid}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f4321bO, new Object[]{uuid}));
                break;
        }
        m20851b(uuid);
    }

    /* renamed from: i */
    private void m20857i(WeaponType apt) {
        bFf().mo5608dq().mo3197f(flQ, apt);
    }

    /* renamed from: j */
    private void m20858j(WeaponType apt) {
        bFf().mo5608dq().mo3197f(flR, apt);
    }

    /* renamed from: j */
    private void m20859j(MissionTemplate avh) {
        bFf().mo5608dq().mo3197f(flM, avh);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Default Weapon Type 1")
    @C0064Am(aul = "f689706d8077ed127c18bcf26d86e9bb", aum = 0)
    @C5566aOg
    /* renamed from: k */
    private void m20860k(WeaponType apt) {
        throw new aWi(new aCE(this, fmq, new Object[]{apt}));
    }

    /* renamed from: k */
    private void m20861k(MissionTemplate avh) {
        bFf().mo5608dq().mo3197f(flN, avh);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Default Mission Station 1")
    @C0064Am(aul = "df4fe443ca4ca3ecf71536cd86d60dc1", aum = 0)
    @C5566aOg
    /* renamed from: l */
    private void m20862l(MissionTemplate avh) {
        throw new aWi(new aCE(this, fme, new Object[]{avh}));
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Default Weapon Type 2")
    @C0064Am(aul = "efa89f94db8477ed03c0db3ebb8b0277", aum = 0)
    @C5566aOg
    /* renamed from: m */
    private void m20863m(WeaponType apt) {
        throw new aWi(new aCE(this, fms, new Object[]{apt}));
    }

    /* renamed from: n */
    private void m20864n(NPC auf) {
        bFf().mo5608dq().mo3197f(flK, auf);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Default Mission Station 2")
    @C0064Am(aul = "21a1afc49ec597db8089e858ab768f2b", aum = 0)
    @C5566aOg
    /* renamed from: n */
    private void m20865n(MissionTemplate avh) {
        throw new aWi(new aCE(this, fmg, new Object[]{avh}));
    }

    /* renamed from: o */
    private void m20866o(ShipType ng) {
        bFf().mo5608dq().mo3197f(flO, ng);
    }

    /* renamed from: o */
    private void m20867o(NPC auf) {
        bFf().mo5608dq().mo3197f(flL, auf);
    }

    /* renamed from: p */
    private void m20868p(ShipType ng) {
        bFf().mo5608dq().mo3197f(flP, ng);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Default Agent 1")
    @C0064Am(aul = "6e5c2c0a61ab30dd9c9a1e6851655537", aum = 0)
    @C5566aOg
    /* renamed from: p */
    private void m20869p(NPC auf) {
        throw new aWi(new aCE(this, fmi, new Object[]{auf}));
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Default Ship Type 1")
    @C0064Am(aul = "da382cc091cd4234c7251ce19dcdfb88", aum = 0)
    @C5566aOg
    /* renamed from: q */
    private void m20870q(ShipType ng) {
        throw new aWi(new aCE(this, fmm, new Object[]{ng}));
    }

    /* renamed from: qa */
    private void m20871qa(int i) {
        bFf().mo5608dq().mo3183b(flS, i);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Max clones number allowed per Station")
    @C0064Am(aul = "553d7b4b0c85ac27c10f92b03b28a4f9", aum = 0)
    @C5566aOg
    /* renamed from: qb */
    private void m20872qb(int i) {
        throw new aWi(new aCE(this, fmc, new Object[]{new Integer(i)}));
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Default Agent 2")
    @C0064Am(aul = "7358952e6390706a30855e838d8b4fe4", aum = 0)
    @C5566aOg
    /* renamed from: r */
    private void m20873r(NPC auf) {
        throw new aWi(new aCE(this, fmk, new Object[]{auf}));
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Default Ship Type 2")
    @C0064Am(aul = "99ce6ed317462d6af8526345eb22b57e", aum = 0)
    @C5566aOg
    /* renamed from: s */
    private void m20874s(ShipType ng) {
        throw new aWi(new aCE(this, fmo, new Object[]{ng}));
    }

    /* renamed from: QS */
    public void mo12837QS() {
        switch (bFf().mo6893i(aKm)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, aKm, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, aKm, new Object[0]));
                break;
        }
        m20842QR();
    }

    /* renamed from: T */
    public C6494any mo11T() {
        return ___iScriptClass;
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Default Station 1")
    @C5566aOg
    /* renamed from: T */
    public void mo12838T(Station bf) {
        switch (bFf().mo6893i(flU)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, flU, new Object[]{bf}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, flU, new Object[]{bf}));
                break;
        }
        m20844S(bf);
    }

    /* renamed from: U */
    public C2491fm[] mo12U() {
        return _m_methods;
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Default Station 2")
    @C5566aOg
    /* renamed from: V */
    public void mo12839V(Station bf) {
        switch (bFf().mo6893i(flW)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, flW, new Object[]{bf}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, flW, new Object[]{bf}));
                break;
        }
        m20845U(bf);
    }

    /* renamed from: W */
    public Object mo13W() {
        return new C1357Tl(this);
    }

    /* renamed from: a */
    public Object mo14a(C0495Gr gr) {
        Object[] args = gr.getArgs();
        switch (gr.mo2417hq() - aDJ._m_methodCount) {
            case 0:
                return m20849ap();
            case 1:
                m20851b((UUID) args[0]);
                return null;
            case 2:
                return m20850ar();
            case 3:
                return bSJ();
            case 4:
                m20844S((Station) args[0]);
                return null;
            case 5:
                return bSL();
            case 6:
                m20845U((Station) args[0]);
                return null;
            case 7:
                return bSN();
            case 8:
                m20854bm((Asset) args[0]);
                return null;
            case 9:
                return bSP();
            case 10:
                m20855bo((Asset) args[0]);
                return null;
            case 11:
                return new Integer(bSR());
            case 12:
                m20872qb(((Integer) args[0]).intValue());
                return null;
            case 13:
                return bST();
            case 14:
                m20862l((MissionTemplate) args[0]);
                return null;
            case 15:
                return bSV();
            case 16:
                m20865n((MissionTemplate) args[0]);
                return null;
            case 17:
                return bSX();
            case 18:
                m20869p((NPC) args[0]);
                return null;
            case 19:
                return bSZ();
            case 20:
                m20873r((NPC) args[0]);
                return null;
            case 21:
                return bTb();
            case 22:
                m20870q((ShipType) args[0]);
                return null;
            case 23:
                return bTd();
            case 24:
                m20874s((ShipType) args[0]);
                return null;
            case 25:
                return bTf();
            case 26:
                m20860k((WeaponType) args[0]);
                return null;
            case 27:
                return bTh();
            case 28:
                m20863m((WeaponType) args[0]);
                return null;
            case 29:
                m20842QR();
                return null;
            default:
                return super.mo14a(gr);
        }
    }

    /* renamed from: a */
    public void mo15a(Collection collection, C0495Gr gr) {
        super.mo15a(collection, gr);
    }

    /* renamed from: aq */
    public UUID mo18aq() {
        switch (bFf().mo6893i(f4320bN)) {
            case 0:
                return null;
            case 2:
                return (UUID) bFf().mo5606d(new aCE(this, f4320bN, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f4320bN, new Object[0]));
                break;
        }
        return m20849ap();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Default Station 1")
    public Station bSK() {
        switch (bFf().mo6893i(flT)) {
            case 0:
                return null;
            case 2:
                return (Station) bFf().mo5606d(new aCE(this, flT, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, flT, new Object[0]));
                break;
        }
        return bSJ();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Default Station 2")
    public Station bSM() {
        switch (bFf().mo6893i(flV)) {
            case 0:
                return null;
            case 2:
                return (Station) bFf().mo5606d(new aCE(this, flV, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, flV, new Object[0]));
                break;
        }
        return bSL();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Default Station Image 1")
    public Asset bSO() {
        switch (bFf().mo6893i(flX)) {
            case 0:
                return null;
            case 2:
                return (Asset) bFf().mo5606d(new aCE(this, flX, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, flX, new Object[0]));
                break;
        }
        return bSN();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Default Station Image 2")
    public Asset bSQ() {
        switch (bFf().mo6893i(flZ)) {
            case 0:
                return null;
            case 2:
                return (Asset) bFf().mo5606d(new aCE(this, flZ, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, flZ, new Object[0]));
                break;
        }
        return bSP();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Max clones number allowed per Station")
    public int bSS() {
        switch (bFf().mo6893i(fmb)) {
            case 0:
                return 0;
            case 2:
                return ((Integer) bFf().mo5606d(new aCE(this, fmb, new Object[0]))).intValue();
            case 3:
                bFf().mo5606d(new aCE(this, fmb, new Object[0]));
                break;
        }
        return bSR();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Default Mission Station 1")
    public MissionTemplate bSU() {
        switch (bFf().mo6893i(fmd)) {
            case 0:
                return null;
            case 2:
                return (MissionTemplate) bFf().mo5606d(new aCE(this, fmd, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, fmd, new Object[0]));
                break;
        }
        return bST();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Default Mission Station 2")
    public MissionTemplate bSW() {
        switch (bFf().mo6893i(fmf)) {
            case 0:
                return null;
            case 2:
                return (MissionTemplate) bFf().mo5606d(new aCE(this, fmf, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, fmf, new Object[0]));
                break;
        }
        return bSV();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Default Agent 1")
    public NPC bSY() {
        switch (bFf().mo6893i(fmh)) {
            case 0:
                return null;
            case 2:
                return (NPC) bFf().mo5606d(new aCE(this, fmh, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, fmh, new Object[0]));
                break;
        }
        return bSX();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Default Agent 2")
    public NPC bTa() {
        switch (bFf().mo6893i(fmj)) {
            case 0:
                return null;
            case 2:
                return (NPC) bFf().mo5606d(new aCE(this, fmj, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, fmj, new Object[0]));
                break;
        }
        return bSZ();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Default Ship Type 1")
    public ShipType bTc() {
        switch (bFf().mo6893i(fml)) {
            case 0:
                return null;
            case 2:
                return (ShipType) bFf().mo5606d(new aCE(this, fml, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, fml, new Object[0]));
                break;
        }
        return bTb();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Default Ship Type 2")
    public ShipType bTe() {
        switch (bFf().mo6893i(fmn)) {
            case 0:
                return null;
            case 2:
                return (ShipType) bFf().mo5606d(new aCE(this, fmn, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, fmn, new Object[0]));
                break;
        }
        return bTd();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Default Weapon Type 1")
    public WeaponType bTg() {
        switch (bFf().mo6893i(fmp)) {
            case 0:
                return null;
            case 2:
                return (WeaponType) bFf().mo5606d(new aCE(this, fmp, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, fmp, new Object[0]));
                break;
        }
        return bTf();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Default Weapon Type 2")
    public WeaponType bTi() {
        switch (bFf().mo6893i(fmr)) {
            case 0:
                return null;
            case 2:
                return (WeaponType) bFf().mo5606d(new aCE(this, fmr, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, fmr, new Object[0]));
                break;
        }
        return bTh();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Default Station Image 1")
    @C5566aOg
    /* renamed from: bn */
    public void mo12853bn(Asset tCVar) {
        switch (bFf().mo6893i(flY)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, flY, new Object[]{tCVar}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, flY, new Object[]{tCVar}));
                break;
        }
        m20854bm(tCVar);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Default Station Image 2")
    @C5566aOg
    /* renamed from: bp */
    public void mo12854bp(Asset tCVar) {
        switch (bFf().mo6893i(fma)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, fma, new Object[]{tCVar}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, fma, new Object[]{tCVar}));
                break;
        }
        m20855bo(tCVar);
    }

    /* renamed from: c */
    public C5663aRz[] mo25c() {
        return _m_fields;
    }

    public String getHandle() {
        switch (bFf().mo6893i(f4322bP)) {
            case 0:
                return null;
            case 2:
                return (String) bFf().mo5606d(new aCE(this, f4322bP, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f4322bP, new Object[0]));
                break;
        }
        return m20850ar();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Default Weapon Type 1")
    @C5566aOg
    /* renamed from: l */
    public void mo12855l(WeaponType apt) {
        switch (bFf().mo6893i(fmq)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, fmq, new Object[]{apt}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, fmq, new Object[]{apt}));
                break;
        }
        m20860k(apt);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Default Mission Station 1")
    @C5566aOg
    /* renamed from: m */
    public void mo12856m(MissionTemplate avh) {
        switch (bFf().mo6893i(fme)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, fme, new Object[]{avh}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, fme, new Object[]{avh}));
                break;
        }
        m20862l(avh);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Default Weapon Type 2")
    @C5566aOg
    /* renamed from: n */
    public void mo12857n(WeaponType apt) {
        switch (bFf().mo6893i(fms)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, fms, new Object[]{apt}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, fms, new Object[]{apt}));
                break;
        }
        m20863m(apt);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Default Mission Station 2")
    @C5566aOg
    /* renamed from: o */
    public void mo12858o(MissionTemplate avh) {
        switch (bFf().mo6893i(fmg)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, fmg, new Object[]{avh}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, fmg, new Object[]{avh}));
                break;
        }
        m20865n(avh);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Default Agent 1")
    @C5566aOg
    /* renamed from: q */
    public void mo12859q(NPC auf) {
        switch (bFf().mo6893i(fmi)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, fmi, new Object[]{auf}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, fmi, new Object[]{auf}));
                break;
        }
        m20869p(auf);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Max clones number allowed per Station")
    @C5566aOg
    /* renamed from: qc */
    public void mo12860qc(int i) {
        switch (bFf().mo6893i(fmc)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, fmc, new Object[]{new Integer(i)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, fmc, new Object[]{new Integer(i)}));
                break;
        }
        m20872qb(i);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Default Ship Type 1")
    @C5566aOg
    /* renamed from: r */
    public void mo12861r(ShipType ng) {
        switch (bFf().mo6893i(fmm)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, fmm, new Object[]{ng}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, fmm, new Object[]{ng}));
                break;
        }
        m20870q(ng);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Default Agent 2")
    @C5566aOg
    /* renamed from: s */
    public void mo12862s(NPC auf) {
        switch (bFf().mo6893i(fmk)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, fmk, new Object[]{auf}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, fmk, new Object[]{auf}));
                break;
        }
        m20873r(auf);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Default Ship Type 2")
    @C5566aOg
    /* renamed from: t */
    public void mo12863t(ShipType ng) {
        switch (bFf().mo6893i(fmo)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, fmo, new Object[]{ng}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, fmo, new Object[]{ng}));
                break;
        }
        m20874s(ng);
    }

    @C0064Am(aul = "12edb1f6313320da8e83e1bc8712eba5", aum = 0)
    /* renamed from: ap */
    private UUID m20849ap() {
        return m20848an();
    }

    @C0064Am(aul = "0f4a91026456f56f452cc86422dc1e19", aum = 0)
    /* renamed from: b */
    private void m20851b(UUID uuid) {
        m20847a(uuid);
    }

    @C0064Am(aul = "9382b18dacaf711d1e8af6355adb429c", aum = 0)
    /* renamed from: ar */
    private String m20850ar() {
        return "cloning_defaults";
    }

    /* renamed from: S */
    public void mo10S() {
        super.mo10S();
        m20847a(UUID.randomUUID());
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Default Station 1")
    @C0064Am(aul = "6c4ae24814e5d62ddb1bc48a6ceb23ab", aum = 0)
    private Station bSJ() {
        return bSw();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Default Station 2")
    @C0064Am(aul = "5367d71689c11d94ebf25b98fbb15e01", aum = 0)
    private Station bSL() {
        return bSx();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Default Station Image 1")
    @C0064Am(aul = "2bfbbaf50c88657f623fc910ccb5305c", aum = 0)
    private Asset bSN() {
        return bSy();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Default Station Image 2")
    @C0064Am(aul = "bd10615905926ff6d9719b2eab170206", aum = 0)
    private Asset bSP() {
        return bSz();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Max clones number allowed per Station")
    @C0064Am(aul = "ed2fe338a1f7e3b24e96bacfc24acf74", aum = 0)
    private int bSR() {
        return bSI();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Default Mission Station 1")
    @C0064Am(aul = "c45ca9da64c9b9164db44d1c450d3c67", aum = 0)
    private MissionTemplate bST() {
        return bSC();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Default Mission Station 2")
    @C0064Am(aul = "44819ef3ef6b384bb666204818ff0b24", aum = 0)
    private MissionTemplate bSV() {
        return bSD();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Default Agent 1")
    @C0064Am(aul = "19b8abf1c08eed942bd072ae5eb21fed", aum = 0)
    private NPC bSX() {
        return bSA();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Default Agent 2")
    @C0064Am(aul = "773c9e52b626317217c69ace783a4474", aum = 0)
    private NPC bSZ() {
        return bSB();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Default Ship Type 1")
    @C0064Am(aul = "1d4103b831fd4678c37e1281a42de379", aum = 0)
    private ShipType bTb() {
        return bSE();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Default Ship Type 2")
    @C0064Am(aul = "f31473f1b2f386ca48f8ef4bc23b535e", aum = 0)
    private ShipType bTd() {
        return bSF();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Default Weapon Type 1")
    @C0064Am(aul = "bf1c1c5b5ab92cc42eec50396017eb68", aum = 0)
    private WeaponType bTf() {
        return bSG();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Default Weapon Type 2")
    @C0064Am(aul = "17f43a08f9f88cd05310e05ea68acf36", aum = 0)
    private WeaponType bTh() {
        return bSH();
    }

    @C0064Am(aul = "63d701ffb93c1f951ddbd965e679ab91", aum = 0)
    /* renamed from: QR */
    private void m20842QR() {
        if (bSK() != null) {
            bSK().mo656qV();
        }
        if (bSM() != null) {
            bSM().mo656qV();
        }
        if (bSU() != null) {
            bSU().push();
        }
        if (bSW() != null) {
            bSW().push();
        }
        if (bTc() != null) {
            bTc().push();
        }
        if (bTe() != null) {
            bTe().push();
        }
        if (bTg() != null) {
            bTg().push();
        }
        if (bTi() != null) {
            bTi().push();
        }
    }
}
