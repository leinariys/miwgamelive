package game.script;

import game.network.message.externalizable.C1368Tv;
import game.network.message.externalizable.C2797kG;
import game.network.message.externalizable.aCE;
import game.network.message.serializable.C1556Wo;
import game.network.message.serializable.C2686iZ;
import game.network.message.serializable.C3438ra;
import game.script.citizenship.CitizenshipControl;
import game.script.corporation.Corporation;
import game.script.corporation.CorporationCreationStatus;
import game.script.faction.Faction;
import game.script.item.*;
import game.script.login.UserConnection;
import game.script.mission.*;
import game.script.mission.scripting.MissionScript;
import game.script.mission.scripting.ScriptableMission;
import game.script.npc.NPC;
import game.script.npc.NPCType;
import game.script.npcchat.NPCSpeech;
import game.script.npcchat.PlayerSpeech;
import game.script.npcchat.actions.OpenMissionBriefingSpeechAction;
import game.script.player.LDParameter;
import game.script.player.Player;
import game.script.progression.ProgressionCareer;
import game.script.ship.Ship;
import game.script.ship.ShipType;
import game.script.ship.Station;
import game.script.space.Gate;
import game.script.space.Loot;
import game.script.space.LootType;
import game.script.space.Node;
import logic.aaa.C0085Az;
import logic.aaa.C1506WA;
import logic.aaa.C1606XV;
import logic.baa.*;
import logic.data.link.C3817vO;
import logic.data.mbean.C0320EL;
import logic.res.code.C5640aRc;
import logic.res.code.C5663aRz;
import logic.res.code.C6494any;
import logic.res.html.C2491fm;
import p001a.*;
import taikodom.infra.script.I18NString;

import java.util.*;

@C5829abJ("1.0.0")
@C5511aMd
@C6485anp
/* renamed from: a.lY */
/* compiled from: a */
public class LDScriptingController extends TaikodomObject implements C1616Xf, C5426aIw, aOW {

    /* renamed from: Cq */
    public static final C2491fm f8552Cq = null;

    /* renamed from: Do */
    public static final C2491fm f8553Do = null;

    /* renamed from: Lm */
    public static final C2491fm f8554Lm = null;

    /* renamed from: Pf */
    public static final C2491fm f8555Pf = null;
    public static final C2491fm _f_dispose_0020_0028_0029V = null;
    public static final C2491fm _f_hasHollowField_0020_0028_0029Z = null;
    public static final int _m_fieldCount = 0;
    public static final C5663aRz[] _m_fields = null;
    public static final int _m_methodCount = 0;
    public static final C2491fm[] _m_methods = null;
    public static final C2491fm aLV = null;
    public static final C2491fm aLW = null;
    public static final C2491fm aLg = null;
    public static final C2491fm byJ = null;
    public static final C2491fm byK = null;
    public static final C2491fm byL = null;
    public static final C2491fm byR = null;
    public static final C5663aRz cjh = null;
    public static final C2491fm cnB = null;
    public static final C2491fm cnD = null;
    public static final C2491fm cov = null;
    public static final C2491fm dFi = null;
    public static final C2491fm dFj = null;
    public static final C2491fm fIk = null;
    public static final C2491fm fgb = null;
    /* renamed from: gQ */
    public static final C2491fm f8556gQ = null;
    public static final C2491fm hDA = null;
    public static final C2491fm hDB = null;
    public static final C2491fm hDC = null;
    public static final C2491fm hDD = null;
    public static final C2491fm hDE = null;
    public static final C2491fm hDF = null;
    public static final C2491fm hDG = null;
    public static final C2491fm hDH = null;
    public static final C2491fm hDI = null;
    public static final C2491fm hDJ = null;
    public static final C2491fm hDK = null;
    public static final C2491fm hDL = null;
    public static final C2491fm hDM = null;
    public static final C2491fm hDN = null;
    public static final C2491fm hDO = null;
    public static final C2491fm hDP = null;
    public static final C2491fm hDQ = null;
    public static final C2491fm hDR = null;
    public static final C2491fm hDS = null;
    public static final C2491fm hDT = null;
    public static final C2491fm hDU = null;
    public static final C2491fm hDV = null;
    public static final C2491fm hDW = null;
    public static final C2491fm hDX = null;
    public static final C2491fm hDY = null;
    public static final C2491fm hDZ = null;
    public static final C5663aRz hDi = null;
    public static final C5663aRz hDj = null;
    public static final C5663aRz hDk = null;
    public static final C5663aRz hDl = null;
    public static final C5663aRz hDm = null;
    public static final C5663aRz hDn = null;
    public static final C5663aRz hDo = null;
    public static final C2491fm hDp = null;
    public static final C2491fm hDq = null;
    public static final C2491fm hDr = null;
    public static final C2491fm hDs = null;
    public static final C2491fm hDt = null;
    public static final C2491fm hDu = null;
    public static final C2491fm hDv = null;
    public static final C2491fm hDw = null;
    public static final C2491fm hDx = null;
    public static final C2491fm hDy = null;
    public static final C2491fm hDz = null;
    public static final C2491fm hEA = null;
    public static final C2491fm hEB = null;
    public static final C2491fm hEC = null;
    public static final C2491fm hED = null;
    public static final C2491fm hEE = null;
    public static final C2491fm hEF = null;
    public static final C2491fm hEG = null;
    public static final C2491fm hEH = null;
    public static final C2491fm hEI = null;
    public static final C2491fm hEJ = null;
    public static final C2491fm hEK = null;
    public static final C2491fm hEa = null;
    public static final C2491fm hEb = null;
    public static final C2491fm hEc = null;
    public static final C2491fm hEd = null;
    public static final C2491fm hEe = null;
    public static final C2491fm hEf = null;
    public static final C2491fm hEg = null;
    public static final C2491fm hEh = null;
    public static final C2491fm hEi = null;
    public static final C2491fm hEj = null;
    public static final C2491fm hEk = null;
    public static final C2491fm hEl = null;
    public static final C2491fm hEm = null;
    public static final C2491fm hEn = null;
    public static final C2491fm hEo = null;
    public static final C2491fm hEp = null;
    public static final C2491fm hEq = null;
    public static final C2491fm hEr = null;
    public static final C2491fm hEs = null;
    public static final C2491fm hEt = null;
    public static final C2491fm hEu = null;
    public static final C2491fm hEv = null;
    public static final C2491fm hEw = null;
    public static final C2491fm hEx = null;
    public static final C2491fm hEy = null;
    public static final C2491fm hEz = null;
    public static final long serialVersionUID = 0;
    public static C6494any ___iScriptClass;
    @C0064Am(aul = "e0fa7306514f31dc41465348383e107f", aum = 4)
    private static C2686iZ<MissionTemplate> cTA;
    @C0064Am(aul = "3f89346783a1d6180de8bb8da73cea70", aum = 5)
    private static C3438ra<LDParameter> cTB;
    @C0064Am(aul = "75e1799b374dfaef0b638d6abceca1e2", aum = 6)
    private static C2686iZ<NPC> cTC;
    @C0064Am(aul = "c5ca9d7073a5809df420f82abbb4ad18", aum = 7)
    private static C1556Wo<NPCType, List<C4045yZ>> cTD;
    @C0064Am(aul = "2a51b805ed2f690695087cf15549baaf", aum = 0)
    private static java.lang.Character cTw;
    @C0064Am(aul = "4062c278e371949d2c0f07ac74030db3", aum = 1)
    private static C3438ra<Mission> cTx;
    @C0064Am(aul = "d0eaefff3ab9870c9dfe4a9e37ce39ea", aum = 2)
    private static Mission cTy;
    @C0064Am(aul = "c6b83543bb3e8599ce5249ff1dacd857", aum = 3)
    private static C2686iZ<MissionTemplate> cTz;

    static {
        m34969V();
    }

    public LDScriptingController() {
        super((C5540aNg) null);
        super.mo10S();
    }

    public LDScriptingController(C5540aNg ang) {
        super(ang);
    }

    /* renamed from: V */
    static void m34969V() {
        serialVersionUID = (long) 1;
        _m_fieldCount = TaikodomObject._m_fieldCount + 8;
        _m_methodCount = TaikodomObject._m_methodCount + 95;
        int i = TaikodomObject._m_fieldCount;
        C5663aRz[] arzArr = new C5663aRz[(i + 8)];
        C5663aRz b = C5640aRc.m17844b(LDScriptingController.class, "2a51b805ed2f690695087cf15549baaf", i);
        hDi = b;
        arzArr[i] = b;
        int i2 = i + 1;
        C5663aRz b2 = C5640aRc.m17844b(LDScriptingController.class, "4062c278e371949d2c0f07ac74030db3", i2);
        hDj = b2;
        arzArr[i2] = b2;
        int i3 = i2 + 1;
        C5663aRz b3 = C5640aRc.m17844b(LDScriptingController.class, "d0eaefff3ab9870c9dfe4a9e37ce39ea", i3);
        hDk = b3;
        arzArr[i3] = b3;
        int i4 = i3 + 1;
        C5663aRz b4 = C5640aRc.m17844b(LDScriptingController.class, "c6b83543bb3e8599ce5249ff1dacd857", i4);
        cjh = b4;
        arzArr[i4] = b4;
        int i5 = i4 + 1;
        C5663aRz b5 = C5640aRc.m17844b(LDScriptingController.class, "e0fa7306514f31dc41465348383e107f", i5);
        hDl = b5;
        arzArr[i5] = b5;
        int i6 = i5 + 1;
        C5663aRz b6 = C5640aRc.m17844b(LDScriptingController.class, "3f89346783a1d6180de8bb8da73cea70", i6);
        hDm = b6;
        arzArr[i6] = b6;
        int i7 = i6 + 1;
        C5663aRz b7 = C5640aRc.m17844b(LDScriptingController.class, "75e1799b374dfaef0b638d6abceca1e2", i7);
        hDn = b7;
        arzArr[i7] = b7;
        int i8 = i7 + 1;
        C5663aRz b8 = C5640aRc.m17844b(LDScriptingController.class, "c5ca9d7073a5809df420f82abbb4ad18", i8);
        hDo = b8;
        arzArr[i8] = b8;
        int i9 = i8 + 1;
        _m_fields = arzArr;
        C1634Xv.m11725a((Object[]) TaikodomObject._m_fields, (Object[]) _m_fields);
        int i10 = TaikodomObject._m_methodCount;
        C2491fm[] fmVarArr = new C2491fm[(i10 + 95)];
        C2491fm a = C4105zY.m41624a(LDScriptingController.class, "cca925c3fc2d52f00c6ff51080be2f8e", i10);
        hDp = a;
        fmVarArr[i10] = a;
        int i11 = i10 + 1;
        C2491fm a2 = C4105zY.m41624a(LDScriptingController.class, C3817vO.eAE, i11);
        hDq = a2;
        fmVarArr[i11] = a2;
        int i12 = i11 + 1;
        C2491fm a3 = C4105zY.m41624a(LDScriptingController.class, "11d1ee32e7c80281dc8b4f54d89dd4f4", i12);
        hDr = a3;
        fmVarArr[i12] = a3;
        int i13 = i12 + 1;
        C2491fm a4 = C4105zY.m41624a(LDScriptingController.class, "8eca45f6ec6d72ec68777cf9fed1dfa0", i13);
        hDs = a4;
        fmVarArr[i13] = a4;
        int i14 = i13 + 1;
        C2491fm a5 = C4105zY.m41624a(LDScriptingController.class, "89e71de6891ea2e28da91b847b538726", i14);
        hDt = a5;
        fmVarArr[i14] = a5;
        int i15 = i14 + 1;
        C2491fm a6 = C4105zY.m41624a(LDScriptingController.class, "4f164f531647c9372bbf2171dac434a6", i15);
        hDu = a6;
        fmVarArr[i15] = a6;
        int i16 = i15 + 1;
        C2491fm a7 = C4105zY.m41624a(LDScriptingController.class, "fbcd9b29af90833be5719eb9acdb9544", i16);
        hDv = a7;
        fmVarArr[i16] = a7;
        int i17 = i16 + 1;
        C2491fm a8 = C4105zY.m41624a(LDScriptingController.class, "ce0c92fceed0fc150399027c7f6547a3", i17);
        hDw = a8;
        fmVarArr[i17] = a8;
        int i18 = i17 + 1;
        C2491fm a9 = C4105zY.m41624a(LDScriptingController.class, "b81b68d674ce89323f36460857154aa8", i18);
        hDx = a9;
        fmVarArr[i18] = a9;
        int i19 = i18 + 1;
        C2491fm a10 = C4105zY.m41624a(LDScriptingController.class, "83bf8fbc570aab1c96f94780fde195c6", i19);
        hDy = a10;
        fmVarArr[i19] = a10;
        int i20 = i19 + 1;
        C2491fm a11 = C4105zY.m41624a(LDScriptingController.class, "20673d95696ed053a8cb175d796d9f2b", i20);
        byJ = a11;
        fmVarArr[i20] = a11;
        int i21 = i20 + 1;
        C2491fm a12 = C4105zY.m41624a(LDScriptingController.class, "d4bc44cc5b574e37dbc911648a72bb9f", i21);
        byK = a12;
        fmVarArr[i21] = a12;
        int i22 = i21 + 1;
        C2491fm a13 = C4105zY.m41624a(LDScriptingController.class, "b0979af685df24a0e37e4087955eda7d", i22);
        byL = a13;
        fmVarArr[i22] = a13;
        int i23 = i22 + 1;
        C2491fm a14 = C4105zY.m41624a(LDScriptingController.class, "1011cb0f1ea296d2d2077e1957b403c3", i23);
        hDz = a14;
        fmVarArr[i23] = a14;
        int i24 = i23 + 1;
        C2491fm a15 = C4105zY.m41624a(LDScriptingController.class, "2dc6384ef150e7667bff3379d52769d1", i24);
        hDA = a15;
        fmVarArr[i24] = a15;
        int i25 = i24 + 1;
        C2491fm a16 = C4105zY.m41624a(LDScriptingController.class, "3c76a5b089536c500de561cb6dc5cfad", i25);
        hDB = a16;
        fmVarArr[i25] = a16;
        int i26 = i25 + 1;
        C2491fm a17 = C4105zY.m41624a(LDScriptingController.class, "9e47f75158a39e5814f92488b249b0c6", i26);
        hDC = a17;
        fmVarArr[i26] = a17;
        int i27 = i26 + 1;
        C2491fm a18 = C4105zY.m41624a(LDScriptingController.class, "8abb22118e8b5b27aec0352878ed089a", i27);
        hDD = a18;
        fmVarArr[i27] = a18;
        int i28 = i27 + 1;
        C2491fm a19 = C4105zY.m41624a(LDScriptingController.class, "255860ab43a48c33beb461ee1335e23a", i28);
        byR = a19;
        fmVarArr[i28] = a19;
        int i29 = i28 + 1;
        C2491fm a20 = C4105zY.m41624a(LDScriptingController.class, "36679c52f18609068a8ed2e13a8b932e", i29);
        hDE = a20;
        fmVarArr[i29] = a20;
        int i30 = i29 + 1;
        C2491fm a21 = C4105zY.m41624a(LDScriptingController.class, "64b4fc758396f914158335486f345614", i30);
        hDF = a21;
        fmVarArr[i30] = a21;
        int i31 = i30 + 1;
        C2491fm a22 = C4105zY.m41624a(LDScriptingController.class, "606435fc9682fcfd5647f324c35c955b", i31);
        hDG = a22;
        fmVarArr[i31] = a22;
        int i32 = i31 + 1;
        C2491fm a23 = C4105zY.m41624a(LDScriptingController.class, "5e306658dd07737d44a33476bbd5d23f", i32);
        hDH = a23;
        fmVarArr[i32] = a23;
        int i33 = i32 + 1;
        C2491fm a24 = C4105zY.m41624a(LDScriptingController.class, "373cff67ad6fb6730bd2eaf8eca1a74a", i33);
        hDI = a24;
        fmVarArr[i33] = a24;
        int i34 = i33 + 1;
        C2491fm a25 = C4105zY.m41624a(LDScriptingController.class, "9e96b4f376c381ec671d33a36ce46ed1", i34);
        hDJ = a25;
        fmVarArr[i34] = a25;
        int i35 = i34 + 1;
        C2491fm a26 = C4105zY.m41624a(LDScriptingController.class, "eb2f04988e8fdbc6951336d4f58b2da9", i35);
        hDK = a26;
        fmVarArr[i35] = a26;
        int i36 = i35 + 1;
        C2491fm a27 = C4105zY.m41624a(LDScriptingController.class, "05a6e5538029a6f464cae02d5fa332d3", i36);
        hDL = a27;
        fmVarArr[i36] = a27;
        int i37 = i36 + 1;
        C2491fm a28 = C4105zY.m41624a(LDScriptingController.class, "1925a12e5dcea4dff06243e294a69640", i37);
        hDM = a28;
        fmVarArr[i37] = a28;
        int i38 = i37 + 1;
        C2491fm a29 = C4105zY.m41624a(LDScriptingController.class, "ba887c2489b0ccbf4beebfd8766d55e1", i38);
        hDN = a29;
        fmVarArr[i38] = a29;
        int i39 = i38 + 1;
        C2491fm a30 = C4105zY.m41624a(LDScriptingController.class, "9cdce01a509d9d3cb42743c8d5b0f90d", i39);
        _f_dispose_0020_0028_0029V = a30;
        fmVarArr[i39] = a30;
        int i40 = i39 + 1;
        C2491fm a31 = C4105zY.m41624a(LDScriptingController.class, "7ddf2049c982093c5a8b7d7394778bfe", i40);
        hDO = a31;
        fmVarArr[i40] = a31;
        int i41 = i40 + 1;
        C2491fm a32 = C4105zY.m41624a(LDScriptingController.class, "462435fe2c3ce6cee6e01ce8ecf5f1ca", i41);
        hDP = a32;
        fmVarArr[i41] = a32;
        int i42 = i41 + 1;
        C2491fm a33 = C4105zY.m41624a(LDScriptingController.class, "aa560055cbc86d979a2654faf7772762", i42);
        hDQ = a33;
        fmVarArr[i42] = a33;
        int i43 = i42 + 1;
        C2491fm a34 = C4105zY.m41624a(LDScriptingController.class, "0549d0f8f912ff4567669db46d5c07e7", i43);
        cnB = a34;
        fmVarArr[i43] = a34;
        int i44 = i43 + 1;
        C2491fm a35 = C4105zY.m41624a(LDScriptingController.class, "af354857632c84bfe081859f83c81596", i44);
        hDR = a35;
        fmVarArr[i44] = a35;
        int i45 = i44 + 1;
        C2491fm a36 = C4105zY.m41624a(LDScriptingController.class, "7d5c0f4980145cdb19a0b05c8379ba1f", i45);
        hDS = a36;
        fmVarArr[i45] = a36;
        int i46 = i45 + 1;
        C2491fm a37 = C4105zY.m41624a(LDScriptingController.class, "12f0ef9d4029211c56f98db549104e9f", i46);
        hDT = a37;
        fmVarArr[i46] = a37;
        int i47 = i46 + 1;
        C2491fm a38 = C4105zY.m41624a(LDScriptingController.class, "9a8524e2d9b007e22c067c2ad4725b6e", i47);
        hDU = a38;
        fmVarArr[i47] = a38;
        int i48 = i47 + 1;
        C2491fm a39 = C4105zY.m41624a(LDScriptingController.class, "cd521a8dd880a4cbcf1f785bfe0c9829", i48);
        hDV = a39;
        fmVarArr[i48] = a39;
        int i49 = i48 + 1;
        C2491fm a40 = C4105zY.m41624a(LDScriptingController.class, "2172fa589a8c6623f680365bd1f67650", i49);
        hDW = a40;
        fmVarArr[i49] = a40;
        int i50 = i49 + 1;
        C2491fm a41 = C4105zY.m41624a(LDScriptingController.class, "31d9115430d5f0e79f8362013034fae6", i50);
        hDX = a41;
        fmVarArr[i50] = a41;
        int i51 = i50 + 1;
        C2491fm a42 = C4105zY.m41624a(LDScriptingController.class, "67d3d01d6c9577bdec03a960e7a2e437", i51);
        hDY = a42;
        fmVarArr[i51] = a42;
        int i52 = i51 + 1;
        C2491fm a43 = C4105zY.m41624a(LDScriptingController.class, "bbf5ac9dc7e16035502e6fc7f5f892d4", i52);
        hDZ = a43;
        fmVarArr[i52] = a43;
        int i53 = i52 + 1;
        C2491fm a44 = C4105zY.m41624a(LDScriptingController.class, "64f72032a90ab34ec577793246551d86", i53);
        f8555Pf = a44;
        fmVarArr[i53] = a44;
        int i54 = i53 + 1;
        C2491fm a45 = C4105zY.m41624a(LDScriptingController.class, "556b40a308f34f9766fe8886f3ec609c", i54);
        hEa = a45;
        fmVarArr[i54] = a45;
        int i55 = i54 + 1;
        C2491fm a46 = C4105zY.m41624a(LDScriptingController.class, "c9f994609cd988ef149ac6492f8a8363", i55);
        hEb = a46;
        fmVarArr[i55] = a46;
        int i56 = i55 + 1;
        C2491fm a47 = C4105zY.m41624a(LDScriptingController.class, "ad1687158bb408991dd62f03b30ad2ca", i56);
        cnD = a47;
        fmVarArr[i56] = a47;
        int i57 = i56 + 1;
        C2491fm a48 = C4105zY.m41624a(LDScriptingController.class, "64683214b9cf271ff8dc441bcaf5a206", i57);
        hEc = a48;
        fmVarArr[i57] = a48;
        int i58 = i57 + 1;
        C2491fm a49 = C4105zY.m41624a(LDScriptingController.class, "3a09a1bd04f6c5cb0b7a06de489ecd8b", i58);
        hEd = a49;
        fmVarArr[i58] = a49;
        int i59 = i58 + 1;
        C2491fm a50 = C4105zY.m41624a(LDScriptingController.class, "2a8b9bd8d47aa839f42692d9b232df7f", i59);
        dFj = a50;
        fmVarArr[i59] = a50;
        int i60 = i59 + 1;
        C2491fm a51 = C4105zY.m41624a(LDScriptingController.class, "6eb17557209ce2d1b58fca82c4049552", i60);
        hEe = a51;
        fmVarArr[i60] = a51;
        int i61 = i60 + 1;
        C2491fm a52 = C4105zY.m41624a(LDScriptingController.class, "c2e738a467e9367b5dc629d0de484949", i61);
        dFi = a52;
        fmVarArr[i61] = a52;
        int i62 = i61 + 1;
        C2491fm a53 = C4105zY.m41624a(LDScriptingController.class, "1f59a0b0c2e19d2379a43bb0b4cb7d5b", i62);
        hEf = a53;
        fmVarArr[i62] = a53;
        int i63 = i62 + 1;
        C2491fm a54 = C4105zY.m41624a(LDScriptingController.class, "a12521e1f8dd73f0ab5468b487b871f7", i63);
        fgb = a54;
        fmVarArr[i63] = a54;
        int i64 = i63 + 1;
        C2491fm a55 = C4105zY.m41624a(LDScriptingController.class, "d5aca0522bb2b03e9727b50d62ec1659", i64);
        hEg = a55;
        fmVarArr[i64] = a55;
        int i65 = i64 + 1;
        C2491fm a56 = C4105zY.m41624a(LDScriptingController.class, "c0bec5111ec6c6674032511e35d250a6", i65);
        hEh = a56;
        fmVarArr[i65] = a56;
        int i66 = i65 + 1;
        C2491fm a57 = C4105zY.m41624a(LDScriptingController.class, "b19422863294c152b11d34f93550b7e3", i66);
        hEi = a57;
        fmVarArr[i66] = a57;
        int i67 = i66 + 1;
        C2491fm a58 = C4105zY.m41624a(LDScriptingController.class, "7e9c50bccdd624d2426aac677a3030a3", i67);
        hEj = a58;
        fmVarArr[i67] = a58;
        int i68 = i67 + 1;
        C2491fm a59 = C4105zY.m41624a(LDScriptingController.class, "5765afc26df9eeb6fad9e5bba2e9d5ff", i68);
        hEk = a59;
        fmVarArr[i68] = a59;
        int i69 = i68 + 1;
        C2491fm a60 = C4105zY.m41624a(LDScriptingController.class, "12e7bc5fb148055e97cc4f40fba14794", i69);
        hEl = a60;
        fmVarArr[i69] = a60;
        int i70 = i69 + 1;
        C2491fm a61 = C4105zY.m41624a(LDScriptingController.class, "09e86607b1591dde91e50f35a6c72175", i70);
        hEm = a61;
        fmVarArr[i70] = a61;
        int i71 = i70 + 1;
        C2491fm a62 = C4105zY.m41624a(LDScriptingController.class, "50735566a5ebfed14f8eef0df746a368", i71);
        hEn = a62;
        fmVarArr[i71] = a62;
        int i72 = i71 + 1;
        C2491fm a63 = C4105zY.m41624a(LDScriptingController.class, "e08c31151f485bb2cc0cdd01addfec26", i72);
        hEo = a63;
        fmVarArr[i72] = a63;
        int i73 = i72 + 1;
        C2491fm a64 = C4105zY.m41624a(LDScriptingController.class, "0d61b358cedfa8afab8c4224a0170139", i73);
        hEp = a64;
        fmVarArr[i73] = a64;
        int i74 = i73 + 1;
        C2491fm a65 = C4105zY.m41624a(LDScriptingController.class, "eb71104682a85a3e7edaa106f63eed5c", i74);
        hEq = a65;
        fmVarArr[i74] = a65;
        int i75 = i74 + 1;
        C2491fm a66 = C4105zY.m41624a(LDScriptingController.class, "d6376ba409854687ec6fd6959f860258", i75);
        hEr = a66;
        fmVarArr[i75] = a66;
        int i76 = i75 + 1;
        C2491fm a67 = C4105zY.m41624a(LDScriptingController.class, "9553e069f56748cfeffd5ec72bc823f6", i76);
        hEs = a67;
        fmVarArr[i76] = a67;
        int i77 = i76 + 1;
        C2491fm a68 = C4105zY.m41624a(LDScriptingController.class, "a826e3a735249ca6963290958335d620", i77);
        hEt = a68;
        fmVarArr[i77] = a68;
        int i78 = i77 + 1;
        C2491fm a69 = C4105zY.m41624a(LDScriptingController.class, "b9b70fb8391092bc99f29fb3781110c5", i78);
        cov = a69;
        fmVarArr[i78] = a69;
        int i79 = i78 + 1;
        C2491fm a70 = C4105zY.m41624a(LDScriptingController.class, "fa6fa99f36bf53cebe1a7142e04a5440", i79);
        hEu = a70;
        fmVarArr[i79] = a70;
        int i80 = i79 + 1;
        C2491fm a71 = C4105zY.m41624a(LDScriptingController.class, "8d594824e94ee5fe413e1435cbd829c2", i80);
        fIk = a71;
        fmVarArr[i80] = a71;
        int i81 = i80 + 1;
        C2491fm a72 = C4105zY.m41624a(LDScriptingController.class, "d3a4e2b497b9805bb4175cc9a127083c", i81);
        hEv = a72;
        fmVarArr[i81] = a72;
        int i82 = i81 + 1;
        C2491fm a73 = C4105zY.m41624a(LDScriptingController.class, "745452cfdc39209dd552b54888d67011", i82);
        hEw = a73;
        fmVarArr[i82] = a73;
        int i83 = i82 + 1;
        C2491fm a74 = C4105zY.m41624a(LDScriptingController.class, "094edaa7684f9da3b447db44d8d64c96", i83);
        hEx = a74;
        fmVarArr[i83] = a74;
        int i84 = i83 + 1;
        C2491fm a75 = C4105zY.m41624a(LDScriptingController.class, "99aaacd1ad2ff55e3b57d7bf80c9c0b6", i84);
        hEy = a75;
        fmVarArr[i84] = a75;
        int i85 = i84 + 1;
        C2491fm a76 = C4105zY.m41624a(LDScriptingController.class, "2225d6b136b21c09c87ff2b457987382", i85);
        hEz = a76;
        fmVarArr[i85] = a76;
        int i86 = i85 + 1;
        C2491fm a77 = C4105zY.m41624a(LDScriptingController.class, "aa72cd08001badade1c4bcbd8e2e9fc1", i86);
        hEA = a77;
        fmVarArr[i86] = a77;
        int i87 = i86 + 1;
        C2491fm a78 = C4105zY.m41624a(LDScriptingController.class, "aac85425335664dab70e3ed0e834adee", i87);
        hEB = a78;
        fmVarArr[i87] = a78;
        int i88 = i87 + 1;
        C2491fm a79 = C4105zY.m41624a(LDScriptingController.class, "e18be86c9f86fb80fc9f17d339af19aa", i88);
        hEC = a79;
        fmVarArr[i88] = a79;
        int i89 = i88 + 1;
        C2491fm a80 = C4105zY.m41624a(LDScriptingController.class, "fc02c17f8cc93c7f973f546a4af0bdf8", i89);
        hED = a80;
        fmVarArr[i89] = a80;
        int i90 = i89 + 1;
        C2491fm a81 = C4105zY.m41624a(LDScriptingController.class, "ecde2554063828ec423c3fda62fae44e", i90);
        hEE = a81;
        fmVarArr[i90] = a81;
        int i91 = i90 + 1;
        C2491fm a82 = C4105zY.m41624a(LDScriptingController.class, "4dbb371f6b93c8d1f61475cf2fc78397", i91);
        hEF = a82;
        fmVarArr[i91] = a82;
        int i92 = i91 + 1;
        C2491fm a83 = C4105zY.m41624a(LDScriptingController.class, "4c6168225b34ebbdb054143e1091016f", i92);
        hEG = a83;
        fmVarArr[i92] = a83;
        int i93 = i92 + 1;
        C2491fm a84 = C4105zY.m41624a(LDScriptingController.class, "6e68bf3e667bcc10e34c38cea7635e7f", i93);
        f8553Do = a84;
        fmVarArr[i93] = a84;
        int i94 = i93 + 1;
        C2491fm a85 = C4105zY.m41624a(LDScriptingController.class, "a1012ce7ce7abab56d41bb87d1f6942e", i94);
        aLV = a85;
        fmVarArr[i94] = a85;
        int i95 = i94 + 1;
        C2491fm a86 = C4105zY.m41624a(LDScriptingController.class, "41e49afc5689438e182e522a25937549", i95);
        aLW = a86;
        fmVarArr[i95] = a86;
        int i96 = i95 + 1;
        C2491fm a87 = C4105zY.m41624a(LDScriptingController.class, "496698d61ae903944d6caf46046deab6", i96);
        hEH = a87;
        fmVarArr[i96] = a87;
        int i97 = i96 + 1;
        C2491fm a88 = C4105zY.m41624a(LDScriptingController.class, "96dd87678d907f184248dcb0abcc9e6a", i97);
        f8552Cq = a88;
        fmVarArr[i97] = a88;
        int i98 = i97 + 1;
        C2491fm a89 = C4105zY.m41624a(LDScriptingController.class, "03a345bb607429241146c06bdb9c24b6", i98);
        aLg = a89;
        fmVarArr[i98] = a89;
        int i99 = i98 + 1;
        C2491fm a90 = C4105zY.m41624a(LDScriptingController.class, "1d91a4f4cffe8d6dcb07437f2674a587", i99);
        hEI = a90;
        fmVarArr[i99] = a90;
        int i100 = i99 + 1;
        C2491fm a91 = C4105zY.m41624a(LDScriptingController.class, "c4a2f5a2189c30a7fff4044b29a2de0b", i100);
        hEJ = a91;
        fmVarArr[i100] = a91;
        int i101 = i100 + 1;
        C2491fm a92 = C4105zY.m41624a(LDScriptingController.class, "e824c2c22ef976a87645918769a762e1", i101);
        hEK = a92;
        fmVarArr[i101] = a92;
        int i102 = i101 + 1;
        C2491fm a93 = C4105zY.m41624a(LDScriptingController.class, "997c3a96e0096e038e34c8820eaec64c", i102);
        f8554Lm = a93;
        fmVarArr[i102] = a93;
        int i103 = i102 + 1;
        C2491fm a94 = C4105zY.m41624a(LDScriptingController.class, "0e1155fbe69ae59b23ab22453d2d708c", i103);
        _f_hasHollowField_0020_0028_0029Z = a94;
        fmVarArr[i103] = a94;
        int i104 = i103 + 1;
        C2491fm a95 = C4105zY.m41624a(LDScriptingController.class, "280bf0624ebee4b746111b4c4e21555e", i104);
        f8556gQ = a95;
        fmVarArr[i104] = a95;
        int i105 = i104 + 1;
        _m_methods = fmVarArr;
        C1634Xv.m11725a((Object[]) TaikodomObject._m_methods, (Object[]) _m_methods);
        ___iScriptClass = aUO.m18566a(LDScriptingController.class, C0320EL.class, _m_fields, _m_methods);
    }

    @C0064Am(aul = "c4a2f5a2189c30a7fff4044b29a2de0b", aum = 0)
    @C5566aOg
    @C2499fr
    /* renamed from: A */
    private OpenMissionBriefingSpeechAction m34955A(NPC auf) {
        throw new aWi(new aCE(this, hEJ, new Object[]{auf}));
    }

    @C0064Am(aul = "0d61b358cedfa8afab8c4224a0170139", aum = 0)
    @C5566aOg
    /* renamed from: A */
    private void m34957A(Object[] objArr) {
        throw new aWi(new aCE(this, hEp, new Object[]{objArr}));
    }

    @C5566aOg
    /* renamed from: B */
    private void m34958B(Object... objArr) {
        switch (bFf().mo6893i(hEp)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, hEp, new Object[]{objArr}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, hEp, new Object[]{objArr}));
                break;
        }
        m34957A(objArr);
    }

    @C0064Am(aul = "eb71104682a85a3e7edaa106f63eed5c", aum = 0)
    @C5566aOg
    /* renamed from: C */
    private void m34959C(Object[] objArr) {
        throw new aWi(new aCE(this, hEq, new Object[]{objArr}));
    }

    @C5566aOg
    /* renamed from: D */
    private void m34961D(Object... objArr) {
        switch (bFf().mo6893i(hEq)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, hEq, new Object[]{objArr}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, hEq, new Object[]{objArr}));
                break;
        }
        m34959C(objArr);
    }

    @C0064Am(aul = "fc02c17f8cc93c7f973f546a4af0bdf8", aum = 0)
    @C5566aOg
    /* renamed from: E */
    private NPCSpeech m34962E(List<NPCSpeech> list) {
        throw new aWi(new aCE(this, hED, new Object[]{list}));
    }

    @C5566aOg
    /* renamed from: F */
    private NPCSpeech m34963F(List<NPCSpeech> list) {
        switch (bFf().mo6893i(hED)) {
            case 0:
                return null;
            case 2:
                return (NPCSpeech) bFf().mo5606d(new aCE(this, hED, new Object[]{list}));
            case 3:
                bFf().mo5606d(new aCE(this, hED, new Object[]{list}));
                break;
        }
        return m34962E(list);
    }

    @C0064Am(aul = "c2e738a467e9367b5dc629d0de484949", aum = 0)
    @C5566aOg
    /* renamed from: F */
    private void m34964F(String str, String str2) {
        throw new aWi(new aCE(this, dFi, new Object[]{str, str2}));
    }

    @C0064Am(aul = "ecde2554063828ec423c3fda62fae44e", aum = 0)
    @C5566aOg
    /* renamed from: G */
    private List<PlayerSpeech> m34965G(List<PlayerSpeech> list) {
        throw new aWi(new aCE(this, hEE, new Object[]{list}));
    }

    @C5566aOg
    /* renamed from: H */
    private List<PlayerSpeech> m34966H(List<PlayerSpeech> list) {
        switch (bFf().mo6893i(hEE)) {
            case 0:
                return null;
            case 2:
                return (List) bFf().mo5606d(new aCE(this, hEE, new Object[]{list}));
            case 3:
                bFf().mo5606d(new aCE(this, hEE, new Object[]{list}));
                break;
        }
        return m34965G(list);
    }

    /* renamed from: W */
    private void m34970W(java.lang.Character acx) {
        bFf().mo5608dq().mo3197f(hDi, acx);
    }

    @C0064Am(aul = "11d1ee32e7c80281dc8b4f54d89dd4f4", aum = 0)
    @C5566aOg
    /* renamed from: X */
    private void m34971X(java.lang.Character acx) {
        throw new aWi(new aCE(this, hDr, new Object[]{acx}));
    }

    @C0064Am(aul = "606435fc9682fcfd5647f324c35c955b", aum = 0)
    @C5566aOg
    /* renamed from: a */
    private void m34974a(Mission af, MissionTrigger aus) {
        throw new aWi(new aCE(this, hDG, new Object[]{af, aus}));
    }

    @C0064Am(aul = "5e306658dd07737d44a33476bbd5d23f", aum = 0)
    @C5566aOg
    /* renamed from: a */
    private void m34975a(Mission af, MissionTimer apy) {
        throw new aWi(new aCE(this, hDH, new Object[]{af, apy}));
    }

    @C0064Am(aul = "9e96b4f376c381ec671d33a36ce46ed1", aum = 0)
    @C5566aOg
    /* renamed from: a */
    private void m34976a(Mission af, String str, Station bf) {
        throw new aWi(new aCE(this, hDJ, new Object[]{af, str, bf}));
    }

    @C0064Am(aul = "ba887c2489b0ccbf4beebfd8766d55e1", aum = 0)
    @C5566aOg
    /* renamed from: a */
    private void m34977a(Mission af, String str, java.lang.Character acx) {
        throw new aWi(new aCE(this, hDN, new Object[]{af, str, acx}));
    }

    @C0064Am(aul = "eb2f04988e8fdbc6951336d4f58b2da9", aum = 0)
    @C5566aOg
    /* renamed from: a */
    private void m34978a(Mission af, String str, Ship fAVar) {
        throw new aWi(new aCE(this, hDK, new Object[]{af, str, fAVar}));
    }

    @C0064Am(aul = "373cff67ad6fb6730bd2eaf8eca1a74a", aum = 0)
    @C5566aOg
    /* renamed from: a */
    private void m34979a(Mission af, String str, Ship fAVar, Ship fAVar2) {
        throw new aWi(new aCE(this, hDI, new Object[]{af, str, fAVar, fAVar2}));
    }

    @C0064Am(aul = "1925a12e5dcea4dff06243e294a69640", aum = 0)
    @C5566aOg
    /* renamed from: a */
    private void m34980a(Mission af, String str, String str2) {
        throw new aWi(new aCE(this, hDM, new Object[]{af, str, str2}));
    }

    @C4034yP
    @C5566aOg
    @C0064Am(aul = "64b4fc758396f914158335486f345614", aum = 0)
    @C2499fr(mo18855qf = {"bed0f8f95feabbc8839119acdf8d99ef"})
    /* renamed from: a */
    private void m34981a(Actor cr, Mission af) {
        throw new aWi(new aCE(this, hDF, new Object[]{cr, af}));
    }

    @C0064Am(aul = "255860ab43a48c33beb461ee1335e23a", aum = 0)
    @C5566aOg
    /* renamed from: a */
    private void m34983a(LootType ahc, ItemType jCVar, int i) {
        throw new aWi(new aCE(this, byR, new Object[]{ahc, jCVar, new Integer(i)}));
    }

    @C0064Am(aul = "2225d6b136b21c09c87ff2b457987382", aum = 0)
    @C5566aOg
    @C2499fr
    /* renamed from: a */
    private void m34984a(NPCType aed, C4045yZ yZVar, ItemTypeTableItem zlVar) {
        throw new aWi(new aCE(this, hEz, new Object[]{aed, yZVar, zlVar}));
    }

    @C0064Am(aul = "0549d0f8f912ff4567669db46d5c07e7", aum = 0)
    @C5566aOg
    /* renamed from: a */
    private boolean m34987a(ItemType jCVar, int i) {
        throw new aWi(new aCE(this, cnB, new Object[]{jCVar, new Integer(i)}));
    }

    @C0064Am(aul = "aa560055cbc86d979a2654faf7772762", aum = 0)
    @C5566aOg
    /* renamed from: a */
    private boolean m34988a(ItemType jCVar, int i, Mission af) {
        throw new aWi(new aCE(this, hDQ, new Object[]{jCVar, new Integer(i), af}));
    }

    /* renamed from: ag */
    private void m34989ag(C1556Wo wo) {
        bFf().mo5608dq().mo3197f(hDo, wo);
    }

    /* renamed from: aj */
    private void m34990aj(C2686iZ iZVar) {
        bFf().mo5608dq().mo3197f(cjh, iZVar);
    }

    /* renamed from: ak */
    private void m34991ak(C2686iZ iZVar) {
        bFf().mo5608dq().mo3197f(hDl, iZVar);
    }

    /* renamed from: al */
    private void m34992al(C2686iZ iZVar) {
        bFf().mo5608dq().mo3197f(hDn, iZVar);
    }

    /* renamed from: b */
    private int m34993b(ItemLocation aag, ItemType jCVar) {
        switch (bFf().mo6893i(hDV)) {
            case 0:
                return 0;
            case 2:
                return ((Integer) bFf().mo5606d(new aCE(this, hDV, new Object[]{aag, jCVar}))).intValue();
            case 3:
                bFf().mo5606d(new aCE(this, hDV, new Object[]{aag, jCVar}));
                break;
        }
        return m34972a(aag, jCVar);
    }

    /* renamed from: b */
    private int m34994b(ItemLocation aag, ItemType jCVar, int i) {
        switch (bFf().mo6893i(hDS)) {
            case 0:
                return 0;
            case 2:
                return ((Integer) bFf().mo5606d(new aCE(this, hDS, new Object[]{aag, jCVar, new Integer(i)}))).intValue();
            case 3:
                bFf().mo5606d(new aCE(this, hDS, new Object[]{aag, jCVar, new Integer(i)}));
                break;
        }
        return m34973a(aag, jCVar, i);
    }

    @C4034yP
    @C5566aOg
    @C2499fr(mo18855qf = {"bed0f8f95feabbc8839119acdf8d99ef"})
    /* renamed from: b */
    private void m34995b(Actor cr, Mission af) {
        switch (bFf().mo6893i(hDF)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, hDF, new Object[]{cr, af}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, hDF, new Object[]{cr, af}));
                break;
        }
        m34981a(cr, af);
    }

    @C4034yP
    @C2499fr(mo18855qf = {"bed0f8f95feabbc8839119acdf8d99ef"})
    @ClientOnly
    /* renamed from: b */
    private void m34996b(String str, I18NString i18NString, Object... objArr) {
        switch (bFf().mo6893i(hEs)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, hEs, new Object[]{str, i18NString, objArr}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, hEs, new Object[]{str, i18NString, objArr}));
                break;
        }
        m34986a(str, i18NString, objArr);
    }

    @C0064Am(aul = "a12521e1f8dd73f0ab5468b487b871f7", aum = 0)
    @C5566aOg
    private Ship bQw() {
        throw new aWi(new aCE(this, fgb, new Object[0]));
    }

    @C0064Am(aul = "05a6e5538029a6f464cae02d5fa332d3", aum = 0)
    @C5566aOg
    /* renamed from: c */
    private void m34998c(Mission af, String str, Ship fAVar) {
        throw new aWi(new aCE(this, hDL, new Object[]{af, str, fAVar}));
    }

    @C0064Am(aul = "2dc6384ef150e7667bff3379d52769d1", aum = 0)
    @C5566aOg
    /* renamed from: c */
    private void m34999c(Actor cr, C5260aCm acm) {
        throw new aWi(new aCE(this, hDA, new Object[]{cr, acm}));
    }

    @C0064Am(aul = "36679c52f18609068a8ed2e13a8b932e", aum = 0)
    @C5566aOg
    /* renamed from: c */
    private void m35000c(java.lang.Character acx, Loot ael) {
        throw new aWi(new aCE(this, hDE, new Object[]{acx, ael}));
    }

    private C2686iZ cWA() {
        return (C2686iZ) bFf().mo5608dq().mo3214p(hDn);
    }

    private C1556Wo cWB() {
        return (C1556Wo) bFf().mo5608dq().mo3214p(hDo);
    }

    @C0064Am(aul = "cca925c3fc2d52f00c6ff51080be2f8e", aum = 0)
    @C6580apg(cpn = true)
    private void cWC() {
        bFf().mo5600a((Class<? extends C4062yl>) C3817vO.C3818a.class, (C0495Gr) new aCE(this, hDp, new Object[0]));
    }

    @C6580apg(cpn = true)
    private void cWD() {
        switch (bFf().mo6893i(hDp)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, hDp, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, hDp, new Object[0]));
                break;
        }
        cWC();
    }

    @C0064Am(aul = "7ddf2049c982093c5a8b7d7394778bfe", aum = 0)
    @C5566aOg
    private void cWM() {
        throw new aWi(new aCE(this, hDO, new Object[0]));
    }

    @C0064Am(aul = "462435fe2c3ce6cee6e01ce8ecf5f1ca", aum = 0)
    @C5566aOg
    private void cWO() {
        throw new aWi(new aCE(this, hDP, new Object[0]));
    }

    @C0064Am(aul = "64683214b9cf271ff8dc441bcaf5a206", aum = 0)
    @C5566aOg
    private boolean cWQ() {
        throw new aWi(new aCE(this, hEc, new Object[0]));
    }

    @C0064Am(aul = "3a09a1bd04f6c5cb0b7a06de489ecd8b", aum = 0)
    @C5566aOg
    private boolean cWS() {
        throw new aWi(new aCE(this, hEd, new Object[0]));
    }

    private java.lang.Character cWu() {
        return (java.lang.Character) bFf().mo5608dq().mo3214p(hDi);
    }

    private C3438ra cWv() {
        return (C3438ra) bFf().mo5608dq().mo3214p(hDj);
    }

    private Mission cWw() {
        return (Mission) bFf().mo5608dq().mo3214p(hDk);
    }

    private C2686iZ cWx() {
        return (C2686iZ) bFf().mo5608dq().mo3214p(cjh);
    }

    private C2686iZ cWy() {
        return (C2686iZ) bFf().mo5608dq().mo3214p(hDl);
    }

    private C3438ra cWz() {
        return (C3438ra) bFf().mo5608dq().mo3214p(hDm);
    }

    @C5566aOg
    @C0064Am(aul = "aa72cd08001badade1c4bcbd8e2e9fc1", aum = 0)
    @C2499fr
    private Set<NPC> cXd() {
        throw new aWi(new aCE(this, hEA, new Object[0]));
    }

    @C5566aOg
    @C0064Am(aul = "aac85425335664dab70e3ed0e834adee", aum = 0)
    @C2499fr
    private Set<NPC> cXf() {
        throw new aWi(new aCE(this, hEB, new Object[0]));
    }

    /* renamed from: cs */
    private void m35003cs(C3438ra raVar) {
        bFf().mo5608dq().mo3197f(hDj, raVar);
    }

    /* renamed from: ct */
    private void m35004ct(C3438ra raVar) {
        bFf().mo5608dq().mo3197f(hDm, raVar);
    }

    /* renamed from: d */
    private int m35005d(ItemLocation aag, ItemType jCVar, int i) {
        switch (bFf().mo6893i(hDT)) {
            case 0:
                return 0;
            case 2:
                return ((Integer) bFf().mo5606d(new aCE(this, hDT, new Object[]{aag, jCVar, new Integer(i)}))).intValue();
            case 3:
                bFf().mo5606d(new aCE(this, hDT, new Object[]{aag, jCVar, new Integer(i)}));
                break;
        }
        return m34997c(aag, jCVar, i);
    }

    @C5566aOg
    @C0064Am(aul = "fbcd9b29af90833be5719eb9acdb9544", aum = 0)
    @C2499fr(mo18855qf = {"bed0f8f95feabbc8839119acdf8d99ef"})
    /* renamed from: d */
    private <T extends C4045yZ> Mission<T> m35006d(T t) {
        throw new aWi(new aCE(this, hDv, new Object[]{t}));
    }

    @C0064Am(aul = "b0979af685df24a0e37e4087955eda7d", aum = 0)
    @C5566aOg
    /* renamed from: d */
    private void m35007d(Node rPVar) {
        throw new aWi(new aCE(this, byL, new Object[]{rPVar}));
    }

    @C0064Am(aul = "280bf0624ebee4b746111b4c4e21555e", aum = 0)
    @C5566aOg
    /* renamed from: dd */
    private void m35008dd() {
        throw new aWi(new aCE(this, f8556gQ, new Object[0]));
    }

    @C0064Am(aul = "af354857632c84bfe081859f83c81596", aum = 0)
    @C5566aOg
    /* renamed from: e */
    private int m35010e(ItemType jCVar, int i) {
        throw new aWi(new aCE(this, hDR, new Object[]{jCVar, new Integer(i)}));
    }

    /* renamed from: e */
    private void m35011e(Mission af) {
        bFf().mo5608dq().mo3197f(hDk, af);
    }

    @C0064Am(aul = "e18be86c9f86fb80fc9f17d339af19aa", aum = 0)
    @C5566aOg
    /* renamed from: e */
    private boolean m35012e(List<PlayerSpeech> list, List<NPCSpeech> list2) {
        throw new aWi(new aCE(this, hEC, new Object[]{list, list2}));
    }

    @C0064Am(aul = "ad1687158bb408991dd62f03b30ad2ca", aum = 0)
    @C5566aOg
    /* renamed from: ea */
    private boolean m35013ea(long j) {
        throw new aWi(new aCE(this, cnD, new Object[]{new Long(j)}));
    }

    /* renamed from: f */
    private int m35014f(ItemLocation aag, ItemType jCVar, int i) {
        switch (bFf().mo6893i(hDU)) {
            case 0:
                return 0;
            case 2:
                return ((Integer) bFf().mo5606d(new aCE(this, hDU, new Object[]{aag, jCVar, new Integer(i)}))).intValue();
            case 3:
                bFf().mo5606d(new aCE(this, hDU, new Object[]{aag, jCVar, new Integer(i)}));
                break;
        }
        return m35009e(aag, jCVar, i);
    }

    @C0064Am(aul = "ce0c92fceed0fc150399027c7f6547a3", aum = 0)
    @C5566aOg
    @C2499fr(mo18855qf = {"bed0f8f95feabbc8839119acdf8d99ef"})
    /* renamed from: f */
    private void m35015f(Mission af) {
        throw new aWi(new aCE(this, hDw, new Object[]{af}));
    }

    @C0064Am(aul = "5765afc26df9eeb6fad9e5bba2e9d5ff", aum = 0)
    @C5566aOg
    /* renamed from: f */
    private void m35016f(C4045yZ yZVar) {
        throw new aWi(new aCE(this, hEk, new Object[]{yZVar}));
    }

    @C5566aOg
    /* renamed from: f */
    private boolean m35017f(List<PlayerSpeech> list, List<NPCSpeech> list2) {
        switch (bFf().mo6893i(hEC)) {
            case 0:
                return false;
            case 2:
                return ((Boolean) bFf().mo5606d(new aCE(this, hEC, new Object[]{list, list2}))).booleanValue();
            case 3:
                bFf().mo5606d(new aCE(this, hEC, new Object[]{list, list2}));
                break;
        }
        return m35012e(list, list2);
    }

    @C0064Am(aul = "2a8b9bd8d47aa839f42692d9b232df7f", aum = 0)
    @C5566aOg
    /* renamed from: ff */
    private String m35018ff(String str) {
        throw new aWi(new aCE(this, dFj, new Object[]{str}));
    }

    @C0064Am(aul = "e824c2c22ef976a87645918769a762e1", aum = 0)
    @C5566aOg
    /* renamed from: g */
    private OpenMissionBriefingSpeechAction m35020g(List<PlayerSpeech> list, List<NPCSpeech> list2) {
        throw new aWi(new aCE(this, hEK, new Object[]{list, list2}));
    }

    @C5566aOg
    /* renamed from: g */
    private void m35021g(C4045yZ yZVar) {
        switch (bFf().mo6893i(hEk)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, hEk, new Object[]{yZVar}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, hEk, new Object[]{yZVar}));
                break;
        }
        m35016f(yZVar);
    }

    @C0064Am(aul = "d6376ba409854687ec6fd6959f860258", aum = 0)
    @C5566aOg
    /* renamed from: g */
    private void m35022g(I18NString i18NString, Object[] objArr) {
        throw new aWi(new aCE(this, hEr, new Object[]{i18NString, objArr}));
    }

    @C0064Am(aul = "b19422863294c152b11d34f93550b7e3", aum = 0)
    @C5566aOg
    /* renamed from: g */
    private boolean m35023g(ItemType jCVar, int i) {
        throw new aWi(new aCE(this, hEi, new Object[]{jCVar, new Integer(i)}));
    }

    @C5566aOg
    /* renamed from: h */
    private OpenMissionBriefingSpeechAction m35024h(List<PlayerSpeech> list, List<NPCSpeech> list2) {
        switch (bFf().mo6893i(hEK)) {
            case 0:
                return null;
            case 2:
                return (OpenMissionBriefingSpeechAction) bFf().mo5606d(new aCE(this, hEK, new Object[]{list, list2}));
            case 3:
                bFf().mo5606d(new aCE(this, hEK, new Object[]{list, list2}));
                break;
        }
        return m35020g(list, list2);
    }

    @C0064Am(aul = "b81b68d674ce89323f36460857154aa8", aum = 0)
    @C5566aOg
    @C2499fr(mo18855qf = {"bed0f8f95feabbc8839119acdf8d99ef"})
    /* renamed from: h */
    private void m35025h(Mission af) {
        throw new aWi(new aCE(this, hDx, new Object[]{af}));
    }

    @C0064Am(aul = "7e9c50bccdd624d2426aac677a3030a3", aum = 0)
    @C5566aOg
    /* renamed from: j */
    private void m35026j(Mission af) {
        throw new aWi(new aCE(this, hEj, new Object[]{af}));
    }

    @C5566aOg
    /* renamed from: k */
    private void m35027k(Mission af) {
        switch (bFf().mo6893i(hEj)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, hEj, new Object[]{af}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, hEj, new Object[]{af}));
                break;
        }
        m35026j(af);
    }

    @C0064Am(aul = "556b40a308f34f9766fe8886f3ec609c", aum = 0)
    @C5566aOg
    /* renamed from: kw */
    private boolean m35028kw(long j) {
        throw new aWi(new aCE(this, hEa, new Object[]{new Long(j)}));
    }

    @C0064Am(aul = "c9f994609cd988ef149ac6492f8a8363", aum = 0)
    @C5566aOg
    /* renamed from: ky */
    private boolean m35029ky(long j) {
        throw new aWi(new aCE(this, hEb, new Object[]{new Long(j)}));
    }

    @C0064Am(aul = "12e7bc5fb148055e97cc4f40fba14794", aum = 0)
    @C5566aOg
    @C2499fr
    /* renamed from: l */
    private void m35030l(Mission af) {
        throw new aWi(new aCE(this, hEl, new Object[]{af}));
    }

    @C0064Am(aul = "9e47f75158a39e5814f92488b249b0c6", aum = 0)
    @C5566aOg
    /* renamed from: md */
    private void m35032md(String str) {
        throw new aWi(new aCE(this, hDC, new Object[]{str}));
    }

    @C0064Am(aul = "2172fa589a8c6623f680365bd1f67650", aum = 0)
    @C5566aOg
    /* renamed from: mf */
    private boolean m35033mf(String str) {
        throw new aWi(new aCE(this, hDW, new Object[]{str}));
    }

    @C0064Am(aul = "31d9115430d5f0e79f8362013034fae6", aum = 0)
    @C5566aOg
    /* renamed from: mh */
    private boolean m35034mh(String str) {
        throw new aWi(new aCE(this, hDX, new Object[]{str}));
    }

    @C0064Am(aul = "67d3d01d6c9577bdec03a960e7a2e437", aum = 0)
    @C5566aOg
    /* renamed from: mj */
    private boolean m35035mj(String str) {
        throw new aWi(new aCE(this, hDY, new Object[]{str}));
    }

    @C0064Am(aul = "6eb17557209ce2d1b58fca82c4049552", aum = 0)
    @C5566aOg
    /* renamed from: ml */
    private boolean m35036ml(String str) {
        throw new aWi(new aCE(this, hEe, new Object[]{str}));
    }

    @C0064Am(aul = "c0bec5111ec6c6674032511e35d250a6", aum = 0)
    @C5566aOg
    /* renamed from: mn */
    private void m35037mn(String str) {
        throw new aWi(new aCE(this, hEh, new Object[]{str}));
    }

    @C0064Am(aul = "09e86607b1591dde91e50f35a6c72175", aum = 0)
    @C5566aOg
    @C2499fr
    /* renamed from: n */
    private void m35038n(Mission af) {
        throw new aWi(new aCE(this, hEm, new Object[]{af}));
    }

    @C0064Am(aul = "20673d95696ed053a8cb175d796d9f2b", aum = 0)
    @C5566aOg
    /* renamed from: p */
    private void m35039p(Station bf) {
        throw new aWi(new aCE(this, byJ, new Object[]{bf}));
    }

    @C0064Am(aul = "1011cb0f1ea296d2d2077e1957b403c3", aum = 0)
    @C5566aOg
    /* renamed from: p */
    private void m35040p(Pawn avi) {
        throw new aWi(new aCE(this, hDz, new Object[]{avi}));
    }

    /* renamed from: q */
    private void m35042q(Collection collection, C0495Gr gr) {
        Iterator it = collection.iterator();
        while (it.hasNext()) {
            ((C3817vO.C3818a) it.next()).mo22578a(this);
        }
    }

    @C0064Am(aul = "997c3a96e0096e038e34c8820eaec64c", aum = 0)
    @C5566aOg
    /* renamed from: qU */
    private void m35043qU() {
        throw new aWi(new aCE(this, f8554Lm, new Object[0]));
    }

    @C0064Am(aul = "d4bc44cc5b574e37dbc911648a72bb9f", aum = 0)
    @C5566aOg
    /* renamed from: r */
    private void m35044r(Station bf) {
        throw new aWi(new aCE(this, byK, new Object[]{bf}));
    }

    @C0064Am(aul = "3c76a5b089536c500de561cb6dc5cfad", aum = 0)
    @C5566aOg
    /* renamed from: r */
    private void m35045r(Pawn avi) {
        throw new aWi(new aCE(this, hDB, new Object[]{avi}));
    }

    @C0064Am(aul = "d3a4e2b497b9805bb4175cc9a127083c", aum = 0)
    @C5566aOg
    @C2499fr
    /* renamed from: w */
    private void m35047w(NPC auf) {
        throw new aWi(new aCE(this, hEv, new Object[]{auf}));
    }

    @C0064Am(aul = "50735566a5ebfed14f8eef0df746a368", aum = 0)
    @C5566aOg
    /* renamed from: w */
    private void m35048w(Object[] objArr) {
        throw new aWi(new aCE(this, hEn, new Object[]{objArr}));
    }

    @C5566aOg
    /* renamed from: x */
    private void m35050x(Object... objArr) {
        switch (bFf().mo6893i(hEn)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, hEn, new Object[]{objArr}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, hEn, new Object[]{objArr}));
                break;
        }
        m35048w(objArr);
    }

    @C4034yP
    @C5566aOg
    @C0064Am(aul = "745452cfdc39209dd552b54888d67011", aum = 0)
    @C2499fr
    /* renamed from: y */
    private void m35051y(NPC auf) {
        throw new aWi(new aCE(this, hEw, new Object[]{auf}));
    }

    @C0064Am(aul = "e08c31151f485bb2cc0cdd01addfec26", aum = 0)
    @C5566aOg
    /* renamed from: y */
    private void m35052y(Object[] objArr) {
        throw new aWi(new aCE(this, hEo, new Object[]{objArr}));
    }

    @C5566aOg
    /* renamed from: z */
    private void m35053z(Object... objArr) {
        switch (bFf().mo6893i(hEo)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, hEo, new Object[]{objArr}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, hEo, new Object[]{objArr}));
                break;
        }
        m35052y(objArr);
    }

    @C5566aOg
    @C2499fr
    /* renamed from: B */
    public OpenMissionBriefingSpeechAction mo20249B(NPC auf) {
        switch (bFf().mo6893i(hEJ)) {
            case 0:
                return null;
            case 2:
                return (OpenMissionBriefingSpeechAction) bFf().mo5606d(new aCE(this, hEJ, new Object[]{auf}));
            case 3:
                bFf().mo5606d(new aCE(this, hEJ, new Object[]{auf}));
                break;
        }
        return m34955A(auf);
    }

    /* renamed from: B */
    public void mo9402B(Faction xm) {
        switch (bFf().mo6893i(hEt)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, hEt, new Object[]{xm}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, hEt, new Object[]{xm}));
                break;
        }
        m34956A(xm);
    }

    /* renamed from: E */
    public Mission mo20250E(MissionTemplate avh) {
        switch (bFf().mo6893i(hDZ)) {
            case 0:
                return null;
            case 2:
                return (Mission) bFf().mo5606d(new aCE(this, hDZ, new Object[]{avh}));
            case 3:
                bFf().mo5606d(new aCE(this, hDZ, new Object[]{avh}));
                break;
        }
        return m34960D(avh);
    }

    @ClientOnly
    /* renamed from: QW */
    public boolean mo961QW() {
        switch (bFf().mo6893i(_f_hasHollowField_0020_0028_0029Z)) {
            case 0:
                return false;
            case 2:
                return ((Boolean) bFf().mo5606d(new aCE(this, _f_hasHollowField_0020_0028_0029Z, new Object[0]))).booleanValue();
            case 3:
                bFf().mo5606d(new aCE(this, _f_hasHollowField_0020_0028_0029Z, new Object[0]));
                break;
        }
        return m34967QV();
    }

    /* renamed from: Rn */
    public ProgressionCareer mo9403Rn() {
        switch (bFf().mo6893i(aLg)) {
            case 0:
                return null;
            case 2:
                return (ProgressionCareer) bFf().mo5606d(new aCE(this, aLg, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, aLg, new Object[0]));
                break;
        }
        return m34968Rm();
    }

    /* renamed from: T */
    public C6494any mo11T() {
        return ___iScriptClass;
    }

    /* renamed from: U */
    public C2491fm[] mo12U() {
        return _m_methods;
    }

    /* renamed from: W */
    public Object mo13W() {
        return new C0320EL(this);
    }

    @C5566aOg
    /* renamed from: Y */
    public void mo20251Y(java.lang.Character acx) {
        switch (bFf().mo6893i(hDr)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, hDr, new Object[]{acx}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, hDr, new Object[]{acx}));
                break;
        }
        m34971X(acx);
    }

    /* renamed from: a */
    public Object mo14a(C0495Gr gr) {
        Object[] args = gr.getArgs();
        switch (gr.mo2417hq() - TaikodomObject._m_methodCount) {
            case 0:
                cWC();
                return null;
            case 1:
                return new Boolean(m35049w((C1722ZT<UserConnection>) (C1722ZT) args[0]));
            case 2:
                m34971X((java.lang.Character) args[0]);
                return null;
            case 3:
                return cWE();
            case 4:
                return cWG();
            case 5:
                return cWI();
            case 6:
                return m35006d((C4045yZ) args[0]);
            case 7:
                m35015f((Mission) args[0]);
                return null;
            case 8:
                m35025h((Mission) args[0]);
                return null;
            case 9:
                return cWK();
            case 10:
                m35039p((Station) args[0]);
                return null;
            case 11:
                m35044r((Station) args[0]);
                return null;
            case 12:
                m35007d((Node) args[0]);
                return null;
            case 13:
                m35040p((Pawn) args[0]);
                return null;
            case 14:
                m34999c((Actor) args[0], (C5260aCm) args[1]);
                return null;
            case 15:
                m35045r((Pawn) args[0]);
                return null;
            case 16:
                m35032md((String) args[0]);
                return null;
            case 17:
                m35002cd((Actor) args[0]);
                return null;
            case 18:
                m34983a((LootType) args[0], (ItemType) args[1], ((Integer) args[2]).intValue());
                return null;
            case 19:
                m35000c((java.lang.Character) args[0], (Loot) args[1]);
                return null;
            case 20:
                m34981a((Actor) args[0], (Mission) args[1]);
                return null;
            case 21:
                m34974a((Mission) args[0], (MissionTrigger) args[1]);
                return null;
            case 22:
                m34975a((Mission) args[0], (MissionTimer) args[1]);
                return null;
            case 23:
                m34979a((Mission) args[0], (String) args[1], (Ship) args[2], (Ship) args[3]);
                return null;
            case 24:
                m34976a((Mission) args[0], (String) args[1], (Station) args[2]);
                return null;
            case 25:
                m34978a((Mission) args[0], (String) args[1], (Ship) args[2]);
                return null;
            case 26:
                m34998c((Mission) args[0], (String) args[1], (Ship) args[2]);
                return null;
            case 27:
                m34980a((Mission) args[0], (String) args[1], (String) args[2]);
                return null;
            case 28:
                m34977a((Mission) args[0], (String) args[1], (java.lang.Character) args[2]);
                return null;
            case 29:
                m35019fg();
                return null;
            case 30:
                cWM();
                return null;
            case 31:
                cWO();
                return null;
            case 32:
                return new Boolean(m34988a((ItemType) args[0], ((Integer) args[1]).intValue(), (Mission) args[2]));
            case 33:
                return new Boolean(m34987a((ItemType) args[0], ((Integer) args[1]).intValue()));
            case 34:
                return new Integer(m35010e((ItemType) args[0], ((Integer) args[1]).intValue()));
            case 35:
                return new Integer(m34973a((ItemLocation) args[0], (ItemType) args[1], ((Integer) args[2]).intValue()));
            case 36:
                return new Integer(m34997c((ItemLocation) args[0], (ItemType) args[1], ((Integer) args[2]).intValue()));
            case 37:
                return new Integer(m35009e((ItemLocation) args[0], (ItemType) args[1], ((Integer) args[2]).intValue()));
            case 38:
                return new Integer(m34972a((ItemLocation) args[0], (ItemType) args[1]));
            case 39:
                return new Boolean(m35033mf((String) args[0]));
            case 40:
                return new Boolean(m35034mh((String) args[0]));
            case 41:
                return new Boolean(m35035mj((String) args[0]));
            case 42:
                return m34960D((MissionTemplate) args[0]);
            case 43:
                return m35046uV();
            case 44:
                return new Boolean(m35028kw(((Long) args[0]).longValue()));
            case 45:
                return new Boolean(m35029ky(((Long) args[0]).longValue()));
            case 46:
                return new Boolean(m35013ea(((Long) args[0]).longValue()));
            case 47:
                return new Boolean(cWQ());
            case 48:
                return new Boolean(cWS());
            case 49:
                return m35018ff((String) args[0]);
            case 50:
                return new Boolean(m35036ml((String) args[0]));
            case 51:
                m34964F((String) args[0], (String) args[1]);
                return null;
            case 52:
                return cWU();
            case 53:
                return bQw();
            case 54:
                return cWV();
            case 55:
                m35037mn((String) args[0]);
                return null;
            case 56:
                return new Boolean(m35023g((ItemType) args[0], ((Integer) args[1]).intValue()));
            case 57:
                m35026j((Mission) args[0]);
                return null;
            case 58:
                m35016f((C4045yZ) args[0]);
                return null;
            case 59:
                m35030l((Mission) args[0]);
                return null;
            case 60:
                m35038n((Mission) args[0]);
                return null;
            case 61:
                m35048w((Object[]) args[0]);
                return null;
            case 62:
                m35052y((Object[]) args[0]);
                return null;
            case 63:
                m34957A((Object[]) args[0]);
                return null;
            case 64:
                m34959C((Object[]) args[0]);
                return null;
            case 65:
                m35022g((I18NString) args[0], (Object[]) args[1]);
                return null;
            case 66:
                m34986a((String) args[0], (I18NString) args[1], (Object[]) args[2]);
                return null;
            case 67:
                m34956A((Faction) args[0]);
                return null;
            case 68:
                return azM();
            case 69:
                cWX();
                return null;
            case 70:
                return bYc();
            case 71:
                m35047w((NPC) args[0]);
                return null;
            case 72:
                m35051y((NPC) args[0]);
                return null;
            case 73:
                return cWZ();
            case 74:
                return cXb();
            case 75:
                m34984a((NPCType) args[0], (C4045yZ) args[1], (ItemTypeTableItem) args[2]);
                return null;
            case 76:
                return cXd();
            case 77:
                return cXf();
            case 78:
                return new Boolean(m35012e((List<PlayerSpeech>) (List) args[0], (List<NPCSpeech>) (List) args[1]));
            case 79:
                return m34962E((List<NPCSpeech>) (List) args[0]);
            case 80:
                return m34965G((List) args[0]);
            case 81:
                return cXh();
            case 82:
                return cXj();
            case 83:
                m34982a((C0665JT) args[0]);
                return null;
            case 84:
                m34985a((Item) args[0], (ItemLocation) args[1], (ItemLocation) args[2]);
                return null;
            case 85:
                m35001c((Item) args[0], (ItemLocation) args[1]);
                return null;
            case 86:
                return m35041q((Gate) args[0]);
            case 87:
                return new Integer(m35031ls());
            case 88:
                return m34968Rm();
            case 89:
                return cXl();
            case 90:
                return m34955A((NPC) args[0]);
            case 91:
                return m35020g((List<PlayerSpeech>) (List) args[0], (List<NPCSpeech>) (List) args[1]);
            case 92:
                m35043qU();
                return null;
            case 93:
                return new Boolean(m34967QV());
            case 94:
                m35008dd();
                return null;
            default:
                return super.mo14a(gr);
        }
    }

    /* renamed from: a */
    public void mo15a(Collection collection, C0495Gr gr) {
        switch (gr.mo2417hq() - TaikodomObject._m_methodCount) {
            case 0:
                m35042q(collection, gr);
                return;
            default:
                super.mo15a(collection, gr);
                return;
        }
    }

    public Faction azN() {
        switch (bFf().mo6893i(cov)) {
            case 0:
                return null;
            case 2:
                return (Faction) bFf().mo5606d(new aCE(this, cov, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, cov, new Object[0]));
                break;
        }
        return azM();
    }

    @C5566aOg
    /* renamed from: b */
    public void mo20252b(Mission af, MissionTrigger aus) {
        switch (bFf().mo6893i(hDG)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, hDG, new Object[]{af, aus}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, hDG, new Object[]{af, aus}));
                break;
        }
        m34974a(af, aus);
    }

    @C5566aOg
    /* renamed from: b */
    public void mo20253b(Mission af, MissionTimer apy) {
        switch (bFf().mo6893i(hDH)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, hDH, new Object[]{af, apy}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, hDH, new Object[]{af, apy}));
                break;
        }
        m34975a(af, apy);
    }

    @C5566aOg
    /* renamed from: b */
    public void mo20254b(Mission af, String str, Station bf) {
        switch (bFf().mo6893i(hDJ)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, hDJ, new Object[]{af, str, bf}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, hDJ, new Object[]{af, str, bf}));
                break;
        }
        m34976a(af, str, bf);
    }

    @C5566aOg
    /* renamed from: b */
    public void mo20255b(Mission af, String str, java.lang.Character acx) {
        switch (bFf().mo6893i(hDN)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, hDN, new Object[]{af, str, acx}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, hDN, new Object[]{af, str, acx}));
                break;
        }
        m34977a(af, str, acx);
    }

    @C5566aOg
    /* renamed from: b */
    public void mo20256b(Mission af, String str, Ship fAVar) {
        switch (bFf().mo6893i(hDK)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, hDK, new Object[]{af, str, fAVar}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, hDK, new Object[]{af, str, fAVar}));
                break;
        }
        m34978a(af, str, fAVar);
    }

    @C5566aOg
    /* renamed from: b */
    public void mo20257b(Mission af, String str, Ship fAVar, Ship fAVar2) {
        switch (bFf().mo6893i(hDI)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, hDI, new Object[]{af, str, fAVar, fAVar2}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, hDI, new Object[]{af, str, fAVar, fAVar2}));
                break;
        }
        m34979a(af, str, fAVar, fAVar2);
    }

    @C5566aOg
    /* renamed from: b */
    public void mo20258b(Mission af, String str, String str2) {
        switch (bFf().mo6893i(hDM)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, hDM, new Object[]{af, str, str2}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, hDM, new Object[]{af, str, str2}));
                break;
        }
        m34980a(af, str, str2);
    }

    /* renamed from: b */
    public void mo24b(C0665JT jt) {
        switch (bFf().mo6893i(f8553Do)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f8553Do, new Object[]{jt}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f8553Do, new Object[]{jt}));
                break;
        }
        m34982a(jt);
    }

    @C5566aOg
    /* renamed from: b */
    public void mo20259b(LootType ahc, ItemType jCVar, int i) {
        switch (bFf().mo6893i(byR)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, byR, new Object[]{ahc, jCVar, new Integer(i)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, byR, new Object[]{ahc, jCVar, new Integer(i)}));
                break;
        }
        m34983a(ahc, jCVar, i);
    }

    @C5566aOg
    @C2499fr
    /* renamed from: b */
    public void mo20260b(NPCType aed, C4045yZ yZVar, ItemTypeTableItem zlVar) {
        switch (bFf().mo6893i(hEz)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, hEz, new Object[]{aed, yZVar, zlVar}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, hEz, new Object[]{aed, yZVar, zlVar}));
                break;
        }
        m34984a(aed, yZVar, zlVar);
    }

    /* renamed from: b */
    public void mo20261b(Item auq, ItemLocation aag, ItemLocation aag2) {
        switch (bFf().mo6893i(aLV)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, aLV, new Object[]{auq, aag, aag2}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, aLV, new Object[]{auq, aag, aag2}));
                break;
        }
        m34985a(auq, aag, aag2);
    }

    @C5566aOg
    /* renamed from: b */
    public boolean mo9405b(ItemType jCVar, int i) {
        switch (bFf().mo6893i(cnB)) {
            case 0:
                return false;
            case 2:
                return ((Boolean) bFf().mo5606d(new aCE(this, cnB, new Object[]{jCVar, new Integer(i)}))).booleanValue();
            case 3:
                bFf().mo5606d(new aCE(this, cnB, new Object[]{jCVar, new Integer(i)}));
                break;
        }
        return m34987a(jCVar, i);
    }

    @C5566aOg
    /* renamed from: b */
    public boolean mo9406b(ItemType jCVar, int i, Mission af) {
        switch (bFf().mo6893i(hDQ)) {
            case 0:
                return false;
            case 2:
                return ((Boolean) bFf().mo5606d(new aCE(this, hDQ, new Object[]{jCVar, new Integer(i), af}))).booleanValue();
            case 3:
                bFf().mo5606d(new aCE(this, hDQ, new Object[]{jCVar, new Integer(i), af}));
                break;
        }
        return m34988a(jCVar, i, af);
    }

    @C5566aOg
    public Ship bQx() {
        switch (bFf().mo6893i(fgb)) {
            case 0:
                return null;
            case 2:
                return (Ship) bFf().mo5606d(new aCE(this, fgb, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, fgb, new Object[0]));
                break;
        }
        return bQw();
    }

    public Corporation bYd() {
        switch (bFf().mo6893i(fIk)) {
            case 0:
                return null;
            case 2:
                return (Corporation) bFf().mo5606d(new aCE(this, fIk, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, fIk, new Object[0]));
                break;
        }
        return bYc();
    }

    /* renamed from: c */
    public C5663aRz[] mo25c() {
        return _m_fields;
    }

    public java.lang.Character cWF() {
        switch (bFf().mo6893i(hDs)) {
            case 0:
                return null;
            case 2:
                return (java.lang.Character) bFf().mo5606d(new aCE(this, hDs, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, hDs, new Object[0]));
                break;
        }
        return cWE();
    }

    public List<Mission> cWH() {
        switch (bFf().mo6893i(hDt)) {
            case 0:
                return null;
            case 2:
                return (List) bFf().mo5606d(new aCE(this, hDt, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, hDt, new Object[0]));
                break;
        }
        return cWG();
    }

    public Set<MissionTemplate> cWJ() {
        switch (bFf().mo6893i(hDu)) {
            case 0:
                return null;
            case 2:
                return (Set) bFf().mo5606d(new aCE(this, hDu, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, hDu, new Object[0]));
                break;
        }
        return cWI();
    }

    public Mission cWL() {
        switch (bFf().mo6893i(hDy)) {
            case 0:
                return null;
            case 2:
                return (Mission) bFf().mo5606d(new aCE(this, hDy, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, hDy, new Object[0]));
                break;
        }
        return cWK();
    }

    @C5566aOg
    public void cWN() {
        switch (bFf().mo6893i(hDO)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, hDO, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, hDO, new Object[0]));
                break;
        }
        cWM();
    }

    @C5566aOg
    public void cWP() {
        switch (bFf().mo6893i(hDP)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, hDP, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, hDP, new Object[0]));
                break;
        }
        cWO();
    }

    @C5566aOg
    public boolean cWR() {
        switch (bFf().mo6893i(hEc)) {
            case 0:
                return false;
            case 2:
                return ((Boolean) bFf().mo5606d(new aCE(this, hEc, new Object[0]))).booleanValue();
            case 3:
                bFf().mo5606d(new aCE(this, hEc, new Object[0]));
                break;
        }
        return cWQ();
    }

    @C5566aOg
    public boolean cWT() {
        switch (bFf().mo6893i(hEd)) {
            case 0:
                return false;
            case 2:
                return ((Boolean) bFf().mo5606d(new aCE(this, hEd, new Object[0]))).booleanValue();
            case 3:
                bFf().mo5606d(new aCE(this, hEd, new Object[0]));
                break;
        }
        return cWS();
    }

    public Actor cWW() {
        switch (bFf().mo6893i(hEg)) {
            case 0:
                return null;
            case 2:
                return (Actor) bFf().mo5606d(new aCE(this, hEg, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, hEg, new Object[0]));
                break;
        }
        return cWV();
    }

    public void cWY() {
        switch (bFf().mo6893i(hEu)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, hEu, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, hEu, new Object[0]));
                break;
        }
        cWX();
    }

    public Set<NPC> cXa() {
        switch (bFf().mo6893i(hEx)) {
            case 0:
                return null;
            case 2:
                return (Set) bFf().mo5606d(new aCE(this, hEx, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, hEx, new Object[0]));
                break;
        }
        return cWZ();
    }

    public Map<NPCType, List<C4045yZ>> cXc() {
        switch (bFf().mo6893i(hEy)) {
            case 0:
                return null;
            case 2:
                return (Map) bFf().mo5606d(new aCE(this, hEy, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, hEy, new Object[0]));
                break;
        }
        return cXb();
    }

    @C5566aOg
    @C2499fr
    public Set<NPC> cXe() {
        switch (bFf().mo6893i(hEA)) {
            case 0:
                return null;
            case 2:
                return (Set) bFf().mo5606d(new aCE(this, hEA, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, hEA, new Object[0]));
                break;
        }
        return cXd();
    }

    @C5566aOg
    @C2499fr
    public Set<NPC> cXg() {
        switch (bFf().mo6893i(hEB)) {
            case 0:
                return null;
            case 2:
                return (Set) bFf().mo5606d(new aCE(this, hEB, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, hEB, new Object[0]));
                break;
        }
        return cXf();
    }

    public Station cXi() {
        switch (bFf().mo6893i(hEF)) {
            case 0:
                return null;
            case 2:
                return (Station) bFf().mo5606d(new aCE(this, hEF, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, hEF, new Object[0]));
                break;
        }
        return cXh();
    }

    public CitizenshipControl cXk() {
        switch (bFf().mo6893i(hEG)) {
            case 0:
                return null;
            case 2:
                return (CitizenshipControl) bFf().mo5606d(new aCE(this, hEG, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, hEG, new Object[0]));
                break;
        }
        return cXj();
    }

    public aMU cXm() {
        switch (bFf().mo6893i(hEI)) {
            case 0:
                return null;
            case 2:
                return (aMU) bFf().mo5606d(new aCE(this, hEI, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, hEI, new Object[0]));
                break;
        }
        return cXl();
    }

    @ClientOnly
    /* renamed from: ce */
    public void mo20272ce(Actor cr) {
        switch (bFf().mo6893i(hDD)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, hDD, new Object[]{cr}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, hDD, new Object[]{cr}));
                break;
        }
        m35002cd(cr);
    }

    @C5566aOg
    /* renamed from: d */
    public void mo20273d(Mission af, String str, Ship fAVar) {
        switch (bFf().mo6893i(hDL)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, hDL, new Object[]{af, str, fAVar}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, hDL, new Object[]{af, str, fAVar}));
                break;
        }
        m34998c(af, str, fAVar);
    }

    @C5566aOg
    /* renamed from: d */
    public void mo20274d(Actor cr, C5260aCm acm) {
        switch (bFf().mo6893i(hDA)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, hDA, new Object[]{cr, acm}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, hDA, new Object[]{cr, acm}));
                break;
        }
        m34999c(cr, acm);
    }

    @C5566aOg
    /* renamed from: d */
    public void mo20275d(java.lang.Character acx, Loot ael) {
        switch (bFf().mo6893i(hDE)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, hDE, new Object[]{acx, ael}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, hDE, new Object[]{acx, ael}));
                break;
        }
        m35000c(acx, ael);
    }

    /* renamed from: d */
    public void mo20276d(Item auq, ItemLocation aag) {
        switch (bFf().mo6893i(aLW)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, aLW, new Object[]{auq, aag}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, aLW, new Object[]{auq, aag}));
                break;
        }
        m35001c(auq, aag);
    }

    @C5566aOg
    /* renamed from: de */
    public void mo20277de() {
        switch (bFf().mo6893i(f8556gQ)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f8556gQ, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f8556gQ, new Object[0]));
                break;
        }
        m35008dd();
    }

    public void dispose() {
        switch (bFf().mo6893i(_f_dispose_0020_0028_0029V)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, _f_dispose_0020_0028_0029V, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, _f_dispose_0020_0028_0029V, new Object[0]));
                break;
        }
        m35019fg();
    }

    @C5566aOg
    @C2499fr(mo18855qf = {"bed0f8f95feabbc8839119acdf8d99ef"})
    /* renamed from: e */
    public <T extends C4045yZ> Mission<T> mo20278e(T t) {
        switch (bFf().mo6893i(hDv)) {
            case 0:
                return null;
            case 2:
                return (Mission) bFf().mo5606d(new aCE(this, hDv, new Object[]{t}));
            case 3:
                bFf().mo5606d(new aCE(this, hDv, new Object[]{t}));
                break;
        }
        return m35006d(t);
    }

    @C5566aOg
    /* renamed from: e */
    public void mo20279e(Node rPVar) {
        switch (bFf().mo6893i(byL)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, byL, new Object[]{rPVar}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, byL, new Object[]{rPVar}));
                break;
        }
        m35007d(rPVar);
    }

    @C5566aOg
    /* renamed from: eb */
    public boolean mo9416eb(long j) {
        switch (bFf().mo6893i(cnD)) {
            case 0:
                return false;
            case 2:
                return ((Boolean) bFf().mo5606d(new aCE(this, cnD, new Object[]{new Long(j)}))).booleanValue();
            case 3:
                bFf().mo5606d(new aCE(this, cnD, new Object[]{new Long(j)}));
                break;
        }
        return m35013ea(j);
    }

    @C5566aOg
    /* renamed from: f */
    public int mo9417f(ItemType jCVar, int i) {
        switch (bFf().mo6893i(hDR)) {
            case 0:
                return 0;
            case 2:
                return ((Integer) bFf().mo5606d(new aCE(this, hDR, new Object[]{jCVar, new Integer(i)}))).intValue();
            case 3:
                bFf().mo5606d(new aCE(this, hDR, new Object[]{jCVar, new Integer(i)}));
                break;
        }
        return m35010e(jCVar, i);
    }

    @C5566aOg
    @C2499fr(mo18855qf = {"bed0f8f95feabbc8839119acdf8d99ef"})
    /* renamed from: g */
    public void mo20280g(Mission af) {
        switch (bFf().mo6893i(hDw)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, hDw, new Object[]{af}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, hDw, new Object[]{af}));
                break;
        }
        m35015f(af);
    }

    public String getName() {
        switch (bFf().mo6893i(f8555Pf)) {
            case 0:
                return null;
            case 2:
                return (String) bFf().mo5606d(new aCE(this, f8555Pf, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f8555Pf, new Object[0]));
                break;
        }
        return m35046uV();
    }

    @C5566aOg
    public String getParameter(String str) {
        switch (bFf().mo6893i(dFj)) {
            case 0:
                return null;
            case 2:
                return (String) bFf().mo5606d(new aCE(this, dFj, new Object[]{str}));
            case 3:
                bFf().mo5606d(new aCE(this, dFj, new Object[]{str}));
                break;
        }
        return m35018ff(str);
    }

    public List<LDParameter> getParameters() {
        switch (bFf().mo6893i(hEf)) {
            case 0:
                return null;
            case 2:
                return (List) bFf().mo5606d(new aCE(this, hEf, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, hEf, new Object[0]));
                break;
        }
        return cWU();
    }

    @C5566aOg
    /* renamed from: h */
    public void mo20282h(I18NString i18NString, Object... objArr) {
        switch (bFf().mo6893i(hEr)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, hEr, new Object[]{i18NString, objArr}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, hEr, new Object[]{i18NString, objArr}));
                break;
        }
        m35022g(i18NString, objArr);
    }

    @C5566aOg
    /* renamed from: h */
    public boolean mo9420h(ItemType jCVar, int i) {
        switch (bFf().mo6893i(hEi)) {
            case 0:
                return false;
            case 2:
                return ((Boolean) bFf().mo5606d(new aCE(this, hEi, new Object[]{jCVar, new Integer(i)}))).booleanValue();
            case 3:
                bFf().mo5606d(new aCE(this, hEi, new Object[]{jCVar, new Integer(i)}));
                break;
        }
        return m35023g(jCVar, i);
    }

    @C5566aOg
    @C2499fr(mo18855qf = {"bed0f8f95feabbc8839119acdf8d99ef"})
    /* renamed from: i */
    public void mo20283i(Mission af) {
        switch (bFf().mo6893i(hDx)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, hDx, new Object[]{af}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, hDx, new Object[]{af}));
                break;
        }
        m35025h(af);
    }

    @C5566aOg
    /* renamed from: kx */
    public boolean mo9421kx(long j) {
        switch (bFf().mo6893i(hEa)) {
            case 0:
                return false;
            case 2:
                return ((Boolean) bFf().mo5606d(new aCE(this, hEa, new Object[]{new Long(j)}))).booleanValue();
            case 3:
                bFf().mo5606d(new aCE(this, hEa, new Object[]{new Long(j)}));
                break;
        }
        return m35028kw(j);
    }

    @C5566aOg
    /* renamed from: kz */
    public boolean mo9422kz(long j) {
        switch (bFf().mo6893i(hEb)) {
            case 0:
                return false;
            case 2:
                return ((Boolean) bFf().mo5606d(new aCE(this, hEb, new Object[]{new Long(j)}))).booleanValue();
            case 3:
                bFf().mo5606d(new aCE(this, hEb, new Object[]{new Long(j)}));
                break;
        }
        return m35029ky(j);
    }

    /* renamed from: lt */
    public int mo9423lt() {
        switch (bFf().mo6893i(f8552Cq)) {
            case 0:
                return 0;
            case 2:
                return ((Integer) bFf().mo5606d(new aCE(this, f8552Cq, new Object[0]))).intValue();
            case 3:
                bFf().mo5606d(new aCE(this, f8552Cq, new Object[0]));
                break;
        }
        return m35031ls();
    }

    @C5566aOg
    @C2499fr
    /* renamed from: m */
    public void mo20284m(Mission af) {
        switch (bFf().mo6893i(hEl)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, hEl, new Object[]{af}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, hEl, new Object[]{af}));
                break;
        }
        m35030l(af);
    }

    @C5566aOg
    /* renamed from: me */
    public void mo20285me(String str) {
        switch (bFf().mo6893i(hDC)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, hDC, new Object[]{str}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, hDC, new Object[]{str}));
                break;
        }
        m35032md(str);
    }

    @C5566aOg
    /* renamed from: mg */
    public boolean mo9424mg(String str) {
        switch (bFf().mo6893i(hDW)) {
            case 0:
                return false;
            case 2:
                return ((Boolean) bFf().mo5606d(new aCE(this, hDW, new Object[]{str}))).booleanValue();
            case 3:
                bFf().mo5606d(new aCE(this, hDW, new Object[]{str}));
                break;
        }
        return m35033mf(str);
    }

    @C5566aOg
    /* renamed from: mi */
    public boolean mo9425mi(String str) {
        switch (bFf().mo6893i(hDX)) {
            case 0:
                return false;
            case 2:
                return ((Boolean) bFf().mo5606d(new aCE(this, hDX, new Object[]{str}))).booleanValue();
            case 3:
                bFf().mo5606d(new aCE(this, hDX, new Object[]{str}));
                break;
        }
        return m35034mh(str);
    }

    @C5566aOg
    /* renamed from: mk */
    public boolean mo9426mk(String str) {
        switch (bFf().mo6893i(hDY)) {
            case 0:
                return false;
            case 2:
                return ((Boolean) bFf().mo5606d(new aCE(this, hDY, new Object[]{str}))).booleanValue();
            case 3:
                bFf().mo5606d(new aCE(this, hDY, new Object[]{str}));
                break;
        }
        return m35035mj(str);
    }

    @C5566aOg
    /* renamed from: mm */
    public boolean mo9427mm(String str) {
        switch (bFf().mo6893i(hEe)) {
            case 0:
                return false;
            case 2:
                return ((Boolean) bFf().mo5606d(new aCE(this, hEe, new Object[]{str}))).booleanValue();
            case 3:
                bFf().mo5606d(new aCE(this, hEe, new Object[]{str}));
                break;
        }
        return m35036ml(str);
    }

    @C5566aOg
    /* renamed from: mo */
    public void mo9428mo(String str) {
        switch (bFf().mo6893i(hEh)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, hEh, new Object[]{str}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, hEh, new Object[]{str}));
                break;
        }
        m35037mn(str);
    }

    @C5566aOg
    @C2499fr
    /* renamed from: o */
    public void mo20286o(Mission af) {
        switch (bFf().mo6893i(hEm)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, hEm, new Object[]{af}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, hEm, new Object[]{af}));
                break;
        }
        m35038n(af);
    }

    @C5566aOg
    /* renamed from: q */
    public void mo20287q(Station bf) {
        switch (bFf().mo6893i(byJ)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, byJ, new Object[]{bf}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, byJ, new Object[]{bf}));
                break;
        }
        m35039p(bf);
    }

    @C5566aOg
    /* renamed from: q */
    public void mo20288q(Pawn avi) {
        switch (bFf().mo6893i(hDz)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, hDz, new Object[]{avi}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, hDz, new Object[]{avi}));
                break;
        }
        m35040p(avi);
    }

    @C5566aOg
    /* renamed from: qV */
    public void mo20289qV() {
        switch (bFf().mo6893i(f8554Lm)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f8554Lm, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f8554Lm, new Object[0]));
                break;
        }
        m35043qU();
    }

    /* renamed from: r */
    public GatePass mo20290r(Gate fFVar) {
        switch (bFf().mo6893i(hEH)) {
            case 0:
                return null;
            case 2:
                return (GatePass) bFf().mo5606d(new aCE(this, hEH, new Object[]{fFVar}));
            case 3:
                bFf().mo5606d(new aCE(this, hEH, new Object[]{fFVar}));
                break;
        }
        return m35041q(fFVar);
    }

    @C5566aOg
    /* renamed from: s */
    public void mo20291s(Station bf) {
        switch (bFf().mo6893i(byK)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, byK, new Object[]{bf}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, byK, new Object[]{bf}));
                break;
        }
        m35044r(bf);
    }

    @C5566aOg
    /* renamed from: s */
    public void mo20292s(Pawn avi) {
        switch (bFf().mo6893i(hDB)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, hDB, new Object[]{avi}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, hDB, new Object[]{avi}));
                break;
        }
        m35045r(avi);
    }

    @C5566aOg
    public void setParameter(String str, String str2) {
        switch (bFf().mo6893i(dFi)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, dFi, new Object[]{str, str2}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, dFi, new Object[]{str, str2}));
                break;
        }
        m34964F(str, str2);
    }

    @C5566aOg
    @C2499fr
    /* renamed from: x */
    public void mo9430x(NPC auf) {
        switch (bFf().mo6893i(hEv)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, hEv, new Object[]{auf}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, hEv, new Object[]{auf}));
                break;
        }
        m35047w(auf);
    }

    @C5472aKq
    /* renamed from: x */
    public boolean mo20293x(C1722ZT<UserConnection> zt) {
        switch (bFf().mo6893i(hDq)) {
            case 0:
                return false;
            case 2:
                return ((Boolean) bFf().mo5606d(new aCE(this, hDq, new Object[]{zt}))).booleanValue();
            case 3:
                bFf().mo5606d(new aCE(this, hDq, new Object[]{zt}));
                break;
        }
        return m35049w(zt);
    }

    @C4034yP
    @C5566aOg
    @C2499fr
    /* renamed from: z */
    public void mo20294z(NPC auf) {
        switch (bFf().mo6893i(hEw)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, hEw, new Object[]{auf}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, hEw, new Object[]{auf}));
                break;
        }
        m35051y(auf);
    }

    /* renamed from: S */
    public void mo10S() {
        super.mo10S();
    }

    @C0064Am(aul = "bed0f8f95feabbc8839119acdf8d99ef", aum = 0)
    @C5472aKq
    /* renamed from: w */
    private boolean m35049w(C1722ZT<UserConnection> zt) {
        if (zt == null) {
            return false;
        }
        if (zt.bFq() == null) {
            return false;
        }
        if (zt.bFq().mo15388dL() == null) {
            return false;
        }
        return zt.bFq().mo15388dL() == cWF();
    }

    @C0064Am(aul = "8eca45f6ec6d72ec68777cf9fed1dfa0", aum = 0)
    private java.lang.Character cWE() {
        return cWu();
    }

    @C0064Am(aul = "89e71de6891ea2e28da91b847b538726", aum = 0)
    private List<Mission> cWG() {
        return cWv();
    }

    @C0064Am(aul = "4f164f531647c9372bbf2171dac434a6", aum = 0)
    private Set<MissionTemplate> cWI() {
        return cWx();
    }

    @C0064Am(aul = "83bf8fbc570aab1c96f94780fde195c6", aum = 0)
    private Mission cWK() {
        return cWw();
    }

    @C0064Am(aul = "8abb22118e8b5b27aec0352878ed089a", aum = 0)
    @ClientOnly
    /* renamed from: cd */
    private void m35002cd(Actor cr) {
        for (Mission next : cWH()) {
            if (next.ary()) {
                m34995b(cr, next);
            }
        }
    }

    @C0064Am(aul = "9cdce01a509d9d3cb42743c8d5b0f90d", aum = 0)
    /* renamed from: fg */
    private void m35019fg() {
        if (cWw() != null) {
            cWw().cdv();
            cWw().dispose();
        }
        m35011e((Mission) null);
        for (Mission af : cWv()) {
            af.cdv();
            af.dispose();
        }
        for (LDParameter dispose : cWz()) {
            dispose.dispose();
        }
        cWz().clear();
        cWx().clear();
        cWy().clear();
        cWA().clear();
        cWB().clear();
        super.dispose();
    }

    @C0064Am(aul = "7d5c0f4980145cdb19a0b05c8379ba1f", aum = 0)
    /* renamed from: a */
    private int m34973a(ItemLocation aag, ItemType jCVar, int i) {
        if ((jCVar instanceof ComponentType) || (jCVar instanceof ShipType)) {
            return m35014f(aag, jCVar, i);
        }
        if (jCVar instanceof BulkItemType) {
            return m35005d(aag, jCVar, i);
        }
        throw new IllegalArgumentException("Invalid ItemType to check ownership: " + jCVar.getClass().getName());
    }

    @C0064Am(aul = "12f0ef9d4029211c56f98db549104e9f", aum = 0)
    /* renamed from: c */
    private int m34997c(ItemLocation aag, ItemType jCVar, int i) {
        if (aag == null) {
            return 0;
        }
        Iterator<Item> it = aag.aRz().iterator();
        int i2 = 0;
        while (true) {
            if (!it.hasNext()) {
                break;
            }
            Item next = it.next();
            if (next.bAP() == jCVar) {
                BulkItem atv = (BulkItem) next;
                if (atv.mo302Iq() + i2 > i) {
                    int i3 = i - i2;
                    i2 += i3;
                    atv.mo11564db(atv.mo302Iq() - i3);
                    break;
                } else if (atv.mo302Iq() + i2 == i) {
                    i2 += atv.mo302Iq();
                    atv.dispose();
                    break;
                } else {
                    i2 += atv.mo302Iq();
                    atv.dispose();
                }
            }
        }
        return i2;
    }

    @C0064Am(aul = "9a8524e2d9b007e22c067c2ad4725b6e", aum = 0)
    /* renamed from: e */
    private int m35009e(ItemLocation aag, ItemType jCVar, int i) {
        if (aag == null) {
            return 0;
        }
        int i2 = 0;
        for (Item next : aag.aRz()) {
            if (next.bAP() == jCVar) {
                next.dispose();
                int i3 = i2 + 1;
                if (i3 == i) {
                    return i3;
                }
                i2 = i3;
            }
        }
        return i2;
    }

    @C0064Am(aul = "cd521a8dd880a4cbcf1f785bfe0c9829", aum = 0)
    /* renamed from: a */
    private int m34972a(ItemLocation aag, ItemType jCVar) {
        if (aag == null) {
            return 0;
        }
        int i = 0;
        for (Item next : aag.aRz()) {
            if (next.bAP() == jCVar) {
                if (jCVar instanceof BulkItemType) {
                    i = ((BulkItem) next).mo302Iq() + i;
                } else {
                    i++;
                }
            }
        }
        return i;
    }

    @C0064Am(aul = "bbf5ac9dc7e16035502e6fc7f5f892d4", aum = 0)
    /* renamed from: D */
    private Mission m34960D(MissionTemplate avh) {
        for (Mission next : cWH()) {
            if (next.cdp() == avh) {
                return next;
            }
        }
        return null;
    }

    @C0064Am(aul = "64f72032a90ab34ec577793246551d86", aum = 0)
    /* renamed from: uV */
    private String m35046uV() {
        return cWu().getName();
    }

    @C0064Am(aul = "1f59a0b0c2e19d2379a43bb0b4cb7d5b", aum = 0)
    private List<LDParameter> cWU() {
        return Collections.unmodifiableList(cWz());
    }

    @C0064Am(aul = "d5aca0522bb2b03e9727b50d62ec1659", aum = 0)
    private Actor cWV() {
        return cWu().bhE();
    }

    @C4034yP
    @ClientOnly
    @C0064Am(aul = "9553e069f56748cfeffd5ec72bc823f6", aum = 0)
    @C2499fr(mo18855qf = {"bed0f8f95feabbc8839119acdf8d99ef"})
    /* renamed from: a */
    private void m34986a(String str, I18NString i18NString, Object[] objArr) {
        PlayerSocialController dwU = getPlayer().dwU();
        if (dwU instanceof PlayerSocialController) {
            PlayerSocialController qhVar = dwU;
            if (i18NString != null) {
                qhVar.mo21446j(i18NString, objArr);
                qhVar.mo21443dL().mo14419f((C1506WA) new C2797kG((Player) null, qhVar.mo21443dL(), C1368Tv.C1369a.ACTIONS, i18NString, objArr));
            }
        }
        getPlayer().mo14419f((C1506WA) new C0085Az());
    }

    @C0064Am(aul = "a826e3a735249ca6963290958335d620", aum = 0)
    /* renamed from: A */
    private void m34956A(Faction xm) {
        cWu().mo11676n(xm);
    }

    @C0064Am(aul = "b9b70fb8391092bc99f29fb3781110c5", aum = 0)
    private Faction azM() {
        return cWu().azN();
    }

    @C0064Am(aul = "fa6fa99f36bf53cebe1a7142e04a5440", aum = 0)
    private void cWX() {
        if (cWu() instanceof Player) {
            Player aku = (Player) cWu();
            if (aku.dxC() != null) {
                throw new IllegalStateException("Player '" + aku.getName() + "' is already creating a Corporation");
            } else if (aku.bYd() != null) {
                throw new IllegalStateException("Player '" + aku.getName() + "' has already a Corporation");
            } else {
                CorporationCreationStatus baVar = (CorporationCreationStatus) bFf().mo6865M(CorporationCreationStatus.class);
                baVar.mo17307b(aku);
                aku.mo14359c(baVar);
                aku.mo14419f((C1506WA) new C1606XV());
            }
        }
    }

    @C0064Am(aul = "8d594824e94ee5fe413e1435cbd829c2", aum = 0)
    private Corporation bYc() {
        if (cWu() instanceof Player) {
            return ((Player) cWu()).bYd();
        }
        return null;
    }

    @C0064Am(aul = "094edaa7684f9da3b447db44d8d64c96", aum = 0)
    private Set<NPC> cWZ() {
        return cWA();
    }

    @C0064Am(aul = "99aaacd1ad2ff55e3b57d7bf80c9c0b6", aum = 0)
    private Map<NPCType, List<C4045yZ>> cXb() {
        return Collections.unmodifiableMap(cWB());
    }

    @C0064Am(aul = "4dbb371f6b93c8d1f61475cf2fc78397", aum = 0)
    private Station cXh() {
        if (!(cWu() instanceof Player)) {
            return null;
        }
        Actor bhE = ((Player) cWu()).bhE();
        if (bhE instanceof Station) {
            return (Station) bhE;
        }
        return null;
    }

    @C0064Am(aul = "4c6168225b34ebbdb054143e1091016f", aum = 0)
    private CitizenshipControl cXj() {
        if (cWu() instanceof Player) {
            return ((Player) cWu()).cXk();
        }
        return null;
    }

    @C0064Am(aul = "6e68bf3e667bcc10e34c38cea7635e7f", aum = 0)
    /* renamed from: a */
    private void m34982a(C0665JT jt) {
        if (jt.mo3117j(1, 0, 0) && cWw() == null && cWv().size() > 0) {
            m35011e((Mission) cWv().get(cWv().size() - 1));
        }
    }

    @C0064Am(aul = "a1012ce7ce7abab56d41bb87d1f6942e", aum = 0)
    /* renamed from: a */
    private void m34985a(Item auq, ItemLocation aag, ItemLocation aag2) {
        Iterator it = cWv().iterator();
        while (it.hasNext()) {
            Mission af = (Mission) it.next();
            af.mo90b(auq, aag, aag2);
            if (af.cdi() || af.cdm()) {
                it.remove();
                m35027k(af);
            }
        }
    }

    @C0064Am(aul = "41e49afc5689438e182e522a25937549", aum = 0)
    /* renamed from: c */
    private void m35001c(Item auq, ItemLocation aag) {
        Iterator it = cWv().iterator();
        while (it.hasNext()) {
            Mission af = (Mission) it.next();
            af.mo117d(auq, aag);
            if (af.cdi() || af.cdm()) {
                it.remove();
                m35027k(af);
            }
        }
    }

    @C0064Am(aul = "496698d61ae903944d6caf46046deab6", aum = 0)
    /* renamed from: q */
    private GatePass m35041q(Gate fFVar) {
        GatePass adb = null;
        for (Mission bjF : cWH()) {
            List<GatePass> bjF2 = bjF.bjF();
            if (bjF2 != null) {
                for (GatePass next : bjF2) {
                    if (next.mo8293lb() == fFVar) {
                        if (next.cVv()) {
                            return next;
                        }
                        adb = next;
                    }
                }
                continue;
            }
        }
        return adb;
    }

    @C0064Am(aul = "96dd87678d907f184248dcb0abcc9e6a", aum = 0)
    /* renamed from: ls */
    private int m35031ls() {
        return cWu().mo12658lt();
    }

    @C0064Am(aul = "03a345bb607429241146c06bdb9c24b6", aum = 0)
    /* renamed from: Rm */
    private ProgressionCareer m34968Rm() {
        return cWu().mo12637Rn();
    }

    @C0064Am(aul = "1d91a4f4cffe8d6dcb07437f2674a587", aum = 0)
    private aMU cXl() {
        if (cWu() instanceof Player) {
            return ((Player) cWu()).cXm();
        }
        return null;
    }

    @C0064Am(aul = "0e1155fbe69ae59b23ab22453d2d708c", aum = 0)
    @ClientOnly
    /* renamed from: QV */
    private boolean m34967QV() {
        if (super.mo961QW()) {
            return true;
        }
        for (Mission next : cWH()) {
            if (C1298TD.m9830t(next).bFT()) {
                return true;
            }
            if (next instanceof ScriptableMission) {
                for (MissionObjective next2 : ((ScriptableMission) next).azk()) {
                    if (next2 != null && (next2 instanceof MissionObjective) && C1298TD.m9830t(next2).bFT()) {
                        return true;
                    }
                }
            }
            if (next instanceof MissionScript) {
                for (MissionObjective next3 : ((MissionScript) next).azk()) {
                    if (next3 != null && (next3 instanceof MissionObjective) && C1298TD.m9830t(next3).bFT()) {
                        return true;
                    }
                }
                continue;
            }
        }
        return false;
    }

    /* renamed from: a.lY$b */
    /* compiled from: a */
    class C2896b implements Comparator<NPCSpeech> {
        C2896b() {
        }

        /* renamed from: a */
        public int compare(NPCSpeech agl, NPCSpeech agl2) {
            int compareTo = Integer.valueOf(agl.getPriority()).compareTo(Integer.valueOf(agl2.getPriority()));
            return compareTo != 0 ? compareTo : agl.ddT().get().compareTo(agl2.ddT().get());
        }
    }

    /* renamed from: a.lY$a */
    class C2895a implements Comparator<PlayerSpeech> {
        C2895a() {
        }

        /* renamed from: a */
        public int compare(PlayerSpeech av, PlayerSpeech av2) {
            int compareTo = Integer.valueOf(av.avR()).compareTo(Integer.valueOf(av2.avR()));
            return compareTo != 0 ? compareTo : av.ddT().get().compareTo(av2.ddT().get());
        }
    }
}
