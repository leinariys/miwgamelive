package game.script.citizenship;

import game.network.message.externalizable.aCE;
import game.script.player.Player;
import game.script.resource.Asset;
import logic.baa.*;
import logic.data.mbean.C2057bg;
import logic.res.code.C5640aRc;
import logic.res.code.C5663aRz;
import logic.res.code.C6494any;
import logic.res.html.C2491fm;
import p001a.*;
import taikodom.infra.script.I18NString;

import java.util.Collection;

@C6485anp
@C2712iu(mo19786Bt = BaseCitizenImprovementType.class)
@C5511aMd
/* renamed from: a.cZ */
/* compiled from: a */
public abstract class CitizenImprovement extends TaikodomObject implements C1616Xf {

    /* renamed from: NY */
    public static final C5663aRz f6153NY = null;

    /* renamed from: QA */
    public static final C5663aRz f6154QA = null;

    /* renamed from: Qz */
    public static final C5663aRz f6155Qz = null;
    public static final C2491fm _f_getType_0020_0028_0029Ljava_002flang_002fObject_003b = null;
    public static final int _m_fieldCount = 0;
    public static final C5663aRz[] _m_fields = null;
    public static final int _m_methodCount = 0;
    public static final C2491fm[] _m_methods = null;
    public static final C5663aRz avF = null;
    public static final C2491fm avN = null;
    public static final C2491fm avO = null;
    public static final C2491fm bLo = null;
    public static final C2491fm bLp = null;
    public static final C2491fm bLq = null;
    public static final C2491fm bLs = null;
    public static final C2491fm cIt = null;
    public static final long serialVersionUID = 0;
    public static C6494any ___iScriptClass;
    @C0064Am(aul = "66c936df299eeb8f3fedf268a5bbd9a7", aum = 2)

    /* renamed from: jn */
    private static Asset f6156jn;
    @C0064Am(aul = "f06d0709f96f8a3c115bae6244f1bf0c", aum = 0)

    /* renamed from: nh */
    private static I18NString f6157nh;
    @C0064Am(aul = "f3e862c8a203facd056615dc57f02979", aum = 1)

    /* renamed from: ni */
    private static I18NString f6158ni;
    @C0064Am(aul = "b4608365f5fec71402bdafc90054fbf4", aum = 3)

    /* renamed from: nj */
    private static Player f6159nj;

    static {
        m28398V();
    }

    public CitizenImprovement() {
        super((C5540aNg) null);
        super.mo10S();
    }

    public CitizenImprovement(CitizenImprovementType kt) {
        super((C5540aNg) null);
        super._m_script_init(kt);
    }

    public CitizenImprovement(C5540aNg ang) {
        super(ang);
    }

    /* renamed from: V */
    static void m28398V() {
        serialVersionUID = (long) 1;
        _m_fieldCount = TaikodomObject._m_fieldCount + 4;
        _m_methodCount = TaikodomObject._m_methodCount + 8;
        int i = TaikodomObject._m_fieldCount;
        C5663aRz[] arzArr = new C5663aRz[(i + 4)];
        C5663aRz b = C5640aRc.m17844b(CitizenImprovement.class, "f06d0709f96f8a3c115bae6244f1bf0c", i);
        f6154QA = b;
        arzArr[i] = b;
        int i2 = i + 1;
        C5663aRz b2 = C5640aRc.m17844b(CitizenImprovement.class, "f3e862c8a203facd056615dc57f02979", i2);
        f6155Qz = b2;
        arzArr[i2] = b2;
        int i3 = i2 + 1;
        C5663aRz b3 = C5640aRc.m17844b(CitizenImprovement.class, "66c936df299eeb8f3fedf268a5bbd9a7", i3);
        f6153NY = b3;
        arzArr[i3] = b3;
        int i4 = i3 + 1;
        C5663aRz b4 = C5640aRc.m17844b(CitizenImprovement.class, "b4608365f5fec71402bdafc90054fbf4", i4);
        avF = b4;
        arzArr[i4] = b4;
        int i5 = i4 + 1;
        _m_fields = arzArr;
        C1634Xv.m11725a((Object[]) TaikodomObject._m_fields, (Object[]) _m_fields);
        int i6 = TaikodomObject._m_methodCount;
        C2491fm[] fmVarArr = new C2491fm[(i6 + 8)];
        C2491fm a = C4105zY.m41624a(CitizenImprovement.class, "f7f7ace249c379babfcf27aa83b12242", i6);
        cIt = a;
        fmVarArr[i6] = a;
        int i7 = i6 + 1;
        C2491fm a2 = C4105zY.m41624a(CitizenImprovement.class, "7932f5370af2b91209584c4c9cb92412", i7);
        bLo = a2;
        fmVarArr[i7] = a2;
        int i8 = i7 + 1;
        C2491fm a3 = C4105zY.m41624a(CitizenImprovement.class, "40bb9f5d4202fa22f11c0a112e512282", i8);
        avO = a3;
        fmVarArr[i8] = a3;
        int i9 = i8 + 1;
        C2491fm a4 = C4105zY.m41624a(CitizenImprovement.class, "161ddaf39209da04843963f09d853baa", i9);
        bLq = a4;
        fmVarArr[i9] = a4;
        int i10 = i9 + 1;
        C2491fm a5 = C4105zY.m41624a(CitizenImprovement.class, "96d104691dbad47ba3294187b78d02ff", i10);
        avN = a5;
        fmVarArr[i10] = a5;
        int i11 = i10 + 1;
        C2491fm a6 = C4105zY.m41624a(CitizenImprovement.class, "7a4637b028efa4ec27130670a4aedd95", i11);
        bLp = a6;
        fmVarArr[i11] = a6;
        int i12 = i11 + 1;
        C2491fm a7 = C4105zY.m41624a(CitizenImprovement.class, "7f05eb2630ec3057fc208f71ced7d5a6", i12);
        bLs = a7;
        fmVarArr[i12] = a7;
        int i13 = i12 + 1;
        C2491fm a8 = C4105zY.m41624a(CitizenImprovement.class, "f14a16adebb5b924d75574474bfbe12d", i13);
        _f_getType_0020_0028_0029Ljava_002flang_002fObject_003b = a8;
        fmVarArr[i13] = a8;
        int i14 = i13 + 1;
        _m_methods = fmVarArr;
        C1634Xv.m11725a((Object[]) TaikodomObject._m_methods, (Object[]) _m_methods);
        ___iScriptClass = aUO.m18566a(CitizenImprovement.class, C2057bg.class, _m_fields, _m_methods);
    }

    /* renamed from: KQ */
    private Player m28396KQ() {
        return (Player) bFf().mo5608dq().mo3214p(avF);
    }

    @C0064Am(aul = "7932f5370af2b91209584c4c9cb92412", aum = 0)
    /* renamed from: a */
    private boolean m28399a(CitizenImprovement cZVar) {
        throw new aWi(new aCE(this, bLo, new Object[]{cZVar}));
    }

    @C0064Am(aul = "7a4637b028efa4ec27130670a4aedd95", aum = 0)
    private void apP() {
        throw new aWi(new aCE(this, bLp, new Object[0]));
    }

    @C0064Am(aul = "161ddaf39209da04843963f09d853baa", aum = 0)
    private void apQ() {
        throw new aWi(new aCE(this, bLq, new Object[0]));
    }

    @C0064Am(aul = "7f05eb2630ec3057fc208f71ced7d5a6", aum = 0)
    private C2184a apS() {
        throw new aWi(new aCE(this, bLs, new Object[0]));
    }

    /* renamed from: bq */
    private void m28400bq(I18NString i18NString) {
        throw new C6039afL();
    }

    /* renamed from: br */
    private void m28401br(I18NString i18NString) {
        throw new C6039afL();
    }

    @C0064Am(aul = "f14a16adebb5b924d75574474bfbe12d", aum = 0)
    /* renamed from: qW */
    private Object m28402qW() {
        return aHc();
    }

    /* renamed from: sG */
    private Asset m28403sG() {
        return ((CitizenImprovementType) getType()).mo3548sK();
    }

    /* renamed from: t */
    private void m28404t(Asset tCVar) {
        throw new C6039afL();
    }

    /* renamed from: u */
    private void m28405u(Player aku) {
        bFf().mo5608dq().mo3197f(avF, aku);
    }

    /* renamed from: vS */
    private I18NString m28407vS() {
        return ((CitizenImprovementType) getType()).mo3547rP();
    }

    /* renamed from: vT */
    private I18NString m28408vT() {
        return ((CitizenImprovementType) getType()).mo3550vW();
    }

    /* renamed from: KY */
    public Player mo17622KY() {
        switch (bFf().mo6893i(avN)) {
            case 0:
                return null;
            case 2:
                return (Player) bFf().mo5606d(new aCE(this, avN, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, avN, new Object[0]));
                break;
        }
        return m28397KX();
    }

    /* renamed from: S */
    public void mo10S() {
        super.mo10S();
    }

    /* renamed from: T */
    public C6494any mo11T() {
        return ___iScriptClass;
    }

    /* renamed from: U */
    public C2491fm[] mo12U() {
        return _m_methods;
    }

    /* renamed from: W */
    public Object mo13W() {
        return new C2057bg(this);
    }

    /* renamed from: a */
    public Object mo14a(C0495Gr gr) {
        Object[] args = gr.getArgs();
        switch (gr.mo2417hq() - TaikodomObject._m_methodCount) {
            case 0:
                return aHb();
            case 1:
                return new Boolean(m28399a((CitizenImprovement) args[0]));
            case 2:
                m28406v((Player) args[0]);
                return null;
            case 3:
                apQ();
                return null;
            case 4:
                return m28397KX();
            case 5:
                apP();
                return null;
            case 6:
                return apS();
            case 7:
                return m28402qW();
            default:
                return super.mo14a(gr);
        }
    }

    /* renamed from: a */
    public void mo15a(Collection collection, C0495Gr gr) {
        super.mo15a(collection, gr);
    }

    public CitizenImprovementType aHc() {
        switch (bFf().mo6893i(cIt)) {
            case 0:
                return null;
            case 2:
                return (CitizenImprovementType) bFf().mo5606d(new aCE(this, cIt, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, cIt, new Object[0]));
                break;
        }
        return aHb();
    }

    public void apR() {
        switch (bFf().mo6893i(bLq)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, bLq, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, bLq, new Object[0]));
                break;
        }
        apQ();
    }

    public C2184a apT() {
        switch (bFf().mo6893i(bLs)) {
            case 0:
                return null;
            case 2:
                return (C2184a) bFf().mo5606d(new aCE(this, bLs, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, bLs, new Object[0]));
                break;
        }
        return apS();
    }

    /* renamed from: b */
    public boolean mo550b(CitizenImprovement cZVar) {
        switch (bFf().mo6893i(bLo)) {
            case 0:
                return false;
            case 2:
                return ((Boolean) bFf().mo5606d(new aCE(this, bLo, new Object[]{cZVar}))).booleanValue();
            case 3:
                bFf().mo5606d(new aCE(this, bLo, new Object[]{cZVar}));
                break;
        }
        return m28399a(cZVar);
    }

    /* renamed from: c */
    public C5663aRz[] mo25c() {
        return _m_fields;
    }

    public void cancel() {
        switch (bFf().mo6893i(bLp)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, bLp, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, bLp, new Object[0]));
                break;
        }
        apP();
    }

    public /* bridge */ /* synthetic */ Object getType() {
        switch (bFf().mo6893i(_f_getType_0020_0028_0029Ljava_002flang_002fObject_003b)) {
            case 0:
                return null;
            case 2:
                return bFf().mo5606d(new aCE(this, _f_getType_0020_0028_0029Ljava_002flang_002fObject_003b, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, _f_getType_0020_0028_0029Ljava_002flang_002fObject_003b, new Object[0]));
                break;
        }
        return m28402qW();
    }

    /* renamed from: w */
    public void mo17624w(Player aku) {
        switch (bFf().mo6893i(avO)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, avO, new Object[]{aku}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, avO, new Object[]{aku}));
                break;
        }
        m28406v(aku);
    }

    /* renamed from: a */
    public void mo547a(CitizenImprovementType kt) {
        super.mo967a((C2961mJ) kt);
    }

    @C0064Am(aul = "f7f7ace249c379babfcf27aa83b12242", aum = 0)
    private CitizenImprovementType aHb() {
        return (CitizenImprovementType) super.getType();
    }

    @C0064Am(aul = "40bb9f5d4202fa22f11c0a112e512282", aum = 0)
    /* renamed from: v */
    private void m28406v(Player aku) {
        m28405u(aku);
    }

    @C0064Am(aul = "96d104691dbad47ba3294187b78d02ff", aum = 0)
    /* renamed from: KX */
    private Player m28397KX() {
        return m28396KQ();
    }

    /* renamed from: a.cZ$a */
    public enum C2184a {
        HANGAR,
        HIGHWAY,
        INSURANCE,
        NODE_ACCESS,
        STORAGE,
        WAGE
    }
}
