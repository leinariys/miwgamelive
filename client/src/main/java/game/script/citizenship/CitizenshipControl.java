package game.script.citizenship;

import game.network.message.externalizable.aCE;
import game.network.message.serializable.C1556Wo;
import game.network.message.serializable.C3438ra;
import game.script.citizenship.inprovements.HangarImprovement;
import game.script.citizenship.inprovements.InsuranceImprovement;
import game.script.citizenship.inprovements.StorageImprovement;
import game.script.player.Player;
import game.script.storage.Storage;
import logic.baa.C1616Xf;
import logic.baa.C1634Xv;
import logic.baa.C5511aMd;
import logic.baa.C6485anp;
import logic.data.mbean.C6095agP;
import logic.res.code.C5640aRc;
import logic.res.code.C5663aRz;
import logic.res.code.C6494any;
import logic.res.html.C2491fm;
import logic.sql.C0572Hv;
import logic.sql.C0967OE;
import logic.sql.C2027bF;
import logic.sql.C5878acG;
import p001a.*;

import java.util.ArrayList;
import java.util.Collection;

@C5511aMd
@C6485anp
/* renamed from: a.QO */
/* compiled from: a */
public class CitizenshipControl extends TaikodomObject implements C1616Xf, C5611aPz {

    /* renamed from: Ny */
    public static final C2491fm f1427Ny = null;
    public static final C2491fm _f_dispose_0020_0028_0029V = null;
    public static final int _m_fieldCount = 0;
    public static final C5663aRz[] _m_fields = null;
    public static final int _m_methodCount = 0;
    public static final C2491fm[] _m_methods = null;
    public static final C2491fm aKm = null;
    public static final C2491fm col = null;
    public static final C5663aRz dWS = null;
    public static final C5663aRz dWU = null;
    public static final C5663aRz dWW = null;
    public static final C2491fm dWX = null;
    public static final C2491fm dWY = null;
    public static final C2491fm dWZ = null;
    public static final C2491fm dXa = null;
    public static final C2491fm dXb = null;
    public static final C2491fm dXc = null;
    public static final C2491fm dXd = null;
    public static final C2491fm dXe = null;
    public static final C2491fm dXf = null;
    public static final C2491fm dXg = null;
    public static final C2491fm dXh = null;
    public static final C2491fm dXi = null;
    /* renamed from: hF */
    public static final C2491fm f1429hF = null;
    /* renamed from: hz */
    public static final C5663aRz f1430hz = null;
    public static final long serialVersionUID = 0;
    public static C6494any ___iScriptClass;
    @C0064Am(aul = "759e4630c02b1a4be03fb78a7ec42a7e", aum = 0)

    /* renamed from: P */
    private static Player f1428P;
    @C0064Am(aul = "1513f7f3facc77eec3d20e0eac2a4d39", aum = 1)
    private static C3438ra<Citizenship> dWR;
    @C0064Am(aul = "bf9aa1129e3bdb9cd0e2f60bd22ed444", aum = 2)
    private static C1556Wo<CitizenImprovement.C2184a, CitizenImprovement> dWT;
    @C0064Am(aul = "b7c24e4bc14abbd55437c7f09d50bf84", aum = 3)
    private static C1556Wo<CitizenshipType, Integer> dWV;

    static {
        m8843V();
    }

    public CitizenshipControl() {
        super((C5540aNg) null);
        super.mo10S();
    }

    public CitizenshipControl(C5540aNg ang) {
        super(ang);
    }

    /* renamed from: V */
    static void m8843V() {
        serialVersionUID = (long) 1;
        _m_fieldCount = TaikodomObject._m_fieldCount + 4;
        _m_methodCount = TaikodomObject._m_methodCount + 17;
        int i = TaikodomObject._m_fieldCount;
        C5663aRz[] arzArr = new C5663aRz[(i + 4)];
        C5663aRz b = C5640aRc.m17844b(CitizenshipControl.class, "759e4630c02b1a4be03fb78a7ec42a7e", i);
        f1430hz = b;
        arzArr[i] = b;
        int i2 = i + 1;
        C5663aRz b2 = C5640aRc.m17844b(CitizenshipControl.class, "1513f7f3facc77eec3d20e0eac2a4d39", i2);
        dWS = b2;
        arzArr[i2] = b2;
        int i3 = i2 + 1;
        C5663aRz b3 = C5640aRc.m17844b(CitizenshipControl.class, "bf9aa1129e3bdb9cd0e2f60bd22ed444", i3);
        dWU = b3;
        arzArr[i3] = b3;
        int i4 = i3 + 1;
        C5663aRz b4 = C5640aRc.m17844b(CitizenshipControl.class, "b7c24e4bc14abbd55437c7f09d50bf84", i4);
        dWW = b4;
        arzArr[i4] = b4;
        int i5 = i4 + 1;
        _m_fields = arzArr;
        C1634Xv.m11725a((Object[]) TaikodomObject._m_fields, (Object[]) _m_fields);
        int i6 = TaikodomObject._m_methodCount;
        C2491fm[] fmVarArr = new C2491fm[(i6 + 17)];
        C2491fm a = C4105zY.m41624a(CitizenshipControl.class, "cbf7145d6318536c1fb677fd64297063", i6);
        dWX = a;
        fmVarArr[i6] = a;
        int i7 = i6 + 1;
        C2491fm a2 = C4105zY.m41624a(CitizenshipControl.class, "6de9b896addb7285eb1009afa291ef03", i7);
        dWY = a2;
        fmVarArr[i7] = a2;
        int i8 = i7 + 1;
        C2491fm a3 = C4105zY.m41624a(CitizenshipControl.class, "f162989d78671cac0c827f60035b0ea0", i8);
        dWZ = a3;
        fmVarArr[i8] = a3;
        int i9 = i8 + 1;
        C2491fm a4 = C4105zY.m41624a(CitizenshipControl.class, "25a7e94e96c66af7c24de8d38f2869f1", i9);
        dXa = a4;
        fmVarArr[i9] = a4;
        int i10 = i9 + 1;
        C2491fm a5 = C4105zY.m41624a(CitizenshipControl.class, "4d3a575e11a14ee016bb476261cfc17d", i10);
        dXb = a5;
        fmVarArr[i10] = a5;
        int i11 = i10 + 1;
        C2491fm a6 = C4105zY.m41624a(CitizenshipControl.class, "25b7cfbc002c4e8e930f456f2bbad938", i11);
        dXc = a6;
        fmVarArr[i11] = a6;
        int i12 = i11 + 1;
        C2491fm a7 = C4105zY.m41624a(CitizenshipControl.class, "51742d630c3b61b04823e28b09e39897", i12);
        dXd = a7;
        fmVarArr[i12] = a7;
        int i13 = i12 + 1;
        C2491fm a8 = C4105zY.m41624a(CitizenshipControl.class, "165a2cb8d5dcca0a0288e360416cafd6", i13);
        f1429hF = a8;
        fmVarArr[i13] = a8;
        int i14 = i13 + 1;
        C2491fm a9 = C4105zY.m41624a(CitizenshipControl.class, "b1efad5948fc6e08845dcc05246641e5", i14);
        f1427Ny = a9;
        fmVarArr[i14] = a9;
        int i15 = i14 + 1;
        C2491fm a10 = C4105zY.m41624a(CitizenshipControl.class, "96cfe440e306472b8d2656ea89772a9b", i15);
        dXe = a10;
        fmVarArr[i15] = a10;
        int i16 = i15 + 1;
        C2491fm a11 = C4105zY.m41624a(CitizenshipControl.class, "258e29f95fbe81ff41a50391a7da0e3b", i16);
        dXf = a11;
        fmVarArr[i16] = a11;
        int i17 = i16 + 1;
        C2491fm a12 = C4105zY.m41624a(CitizenshipControl.class, "7812baf668c5ef2929a400007c851f76", i17);
        dXg = a12;
        fmVarArr[i17] = a12;
        int i18 = i17 + 1;
        C2491fm a13 = C4105zY.m41624a(CitizenshipControl.class, "5b702f20ee40ef522498b895a4a13f29", i18);
        _f_dispose_0020_0028_0029V = a13;
        fmVarArr[i18] = a13;
        int i19 = i18 + 1;
        C2491fm a14 = C4105zY.m41624a(CitizenshipControl.class, "98cdf79863ca177b2b60b5bb31ec4050", i19);
        dXh = a14;
        fmVarArr[i19] = a14;
        int i20 = i19 + 1;
        C2491fm a15 = C4105zY.m41624a(CitizenshipControl.class, "c592a74820ff0d6d8b9659306087ef01", i20);
        dXi = a15;
        fmVarArr[i20] = a15;
        int i21 = i20 + 1;
        C2491fm a16 = C4105zY.m41624a(CitizenshipControl.class, "a1a87d0641f998da7ef16982c12f45d6", i21);
        col = a16;
        fmVarArr[i21] = a16;
        int i22 = i21 + 1;
        C2491fm a17 = C4105zY.m41624a(CitizenshipControl.class, "3571c433c22540336bb3aaec3a0d3d72", i22);
        aKm = a17;
        fmVarArr[i22] = a17;
        int i23 = i22 + 1;
        _m_methods = fmVarArr;
        C1634Xv.m11725a((Object[]) TaikodomObject._m_methods, (Object[]) _m_methods);
        ___iScriptClass = aUO.m18566a(CitizenshipControl.class, C6095agP.class, _m_fields, _m_methods);
    }

    /* renamed from: g */
    private static long m8856g(CitizenshipPack acu) {
        return (long) (acu.bOT() * 24.0f * 60.0f * 60.0f * 1000.0f);
    }

    /* renamed from: Q */
    private void m8840Q(C1556Wo wo) {
        bFf().mo5608dq().mo3197f(dWU, wo);
    }

    @C0064Am(aul = "3571c433c22540336bb3aaec3a0d3d72", aum = 0)
    @C5566aOg
    /* renamed from: QR */
    private void m8841QR() {
        throw new aWi(new aCE(this, aKm, new Object[0]));
    }

    /* renamed from: R */
    private void m8842R(C1556Wo wo) {
        bFf().mo5608dq().mo3197f(dWW, wo);
    }

    /* renamed from: a */
    private void m8846a(Player aku) {
        bFf().mo5608dq().mo3197f(f1430hz, aku);
    }

    /* renamed from: b */
    private void m8847b(CitizenshipType adl, Integer num) {
        switch (bFf().mo6893i(dXa)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, dXa, new Object[]{adl, num}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, dXa, new Object[]{adl, num}));
                break;
        }
        m8845a(adl, num);
    }

    /* renamed from: bo */
    private void m8848bo(C3438ra raVar) {
        bFf().mo5608dq().mo3197f(dWS, raVar);
    }

    @C0064Am(aul = "7812baf668c5ef2929a400007c851f76", aum = 0)
    @C5566aOg
    @C2499fr
    private InsuranceImprovement bqA() {
        throw new aWi(new aCE(this, dXg, new Object[0]));
    }

    private C3438ra bqt() {
        return (C3438ra) bFf().mo5608dq().mo3214p(dWS);
    }

    private C1556Wo bqu() {
        return (C1556Wo) bFf().mo5608dq().mo3214p(dWU);
    }

    private C1556Wo bqv() {
        return (C1556Wo) bFf().mo5608dq().mo3214p(dWW);
    }

    /* renamed from: d */
    private void m8850d(CitizenImprovement cZVar) {
        switch (bFf().mo6893i(dXc)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, dXc, new Object[]{cZVar}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, dXc, new Object[]{cZVar}));
                break;
        }
        m8849c(cZVar);
    }

    /* renamed from: dG */
    private Player m8851dG() {
        return (Player) bFf().mo5608dq().mo3214p(f1430hz);
    }

    /* renamed from: g */
    private void m8857g(CitizenshipType adl) {
        switch (bFf().mo6893i(dWZ)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, dWZ, new Object[]{adl}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, dWZ, new Object[]{adl}));
                break;
        }
        m8854f(adl);
    }

    /* renamed from: i */
    private Citizenship m8859i(CitizenshipType adl) {
        switch (bFf().mo6893i(dXb)) {
            case 0:
                return null;
            case 2:
                return (Citizenship) bFf().mo5606d(new aCE(this, dXb, new Object[]{adl}));
            case 3:
                bFf().mo5606d(new aCE(this, dXb, new Object[]{adl}));
                break;
        }
        return m8858h(adl);
    }

    @C5566aOg
    /* renamed from: QS */
    public void mo4975QS() {
        switch (bFf().mo6893i(aKm)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, aKm, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, aKm, new Object[0]));
                break;
        }
        m8841QR();
    }

    /* renamed from: T */
    public C6494any mo11T() {
        return ___iScriptClass;
    }

    /* renamed from: U */
    public C2491fm[] mo12U() {
        return _m_methods;
    }

    /* renamed from: W */
    public Object mo13W() {
        return new C6095agP(this);
    }

    /* renamed from: a */
    public Object mo14a(C0495Gr gr) {
        Object[] args = gr.getArgs();
        switch (gr.mo2417hq() - TaikodomObject._m_methodCount) {
            case 0:
                return bqw();
            case 1:
                m8853e((CitizenshipPack) args[0]);
                return null;
            case 2:
                m8854f((CitizenshipType) args[0]);
                return null;
            case 3:
                m8845a((CitizenshipType) args[0], (Integer) args[1]);
                return null;
            case 4:
                return m8858h((CitizenshipType) args[0]);
            case 5:
                m8849c((CitizenImprovement) args[0]);
                return null;
            case 6:
                m8844a((Citizenship) args[0]);
                return null;
            case 7:
                return m8852dK();
            case 8:
                m8862o((Player) args[0]);
                return null;
            case 9:
                return new Long(m8861j((CitizenshipType) args[0]));
            case 10:
                return new Boolean(bqy());
            case 11:
                return bqA();
            case 12:
                m8855fg();
                return null;
            case 13:
                return new Boolean(bqC());
            case 14:
                return new Integer(bqE());
            case 15:
                m8860i((Storage) args[0]);
                return null;
            case 16:
                m8841QR();
                return null;
            default:
                return super.mo14a(gr);
        }
    }

    /* renamed from: a */
    public void mo15a(Collection collection, C0495Gr gr) {
        super.mo15a(collection, gr);
    }

    /* renamed from: b */
    public void mo4976b(Citizenship vm) {
        switch (bFf().mo6893i(dXd)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, dXd, new Object[]{vm}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, dXd, new Object[]{vm}));
                break;
        }
        m8844a(vm);
    }

    @C5566aOg
    @C2499fr
    public InsuranceImprovement bqB() {
        switch (bFf().mo6893i(dXg)) {
            case 0:
                return null;
            case 2:
                return (InsuranceImprovement) bFf().mo5606d(new aCE(this, dXg, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, dXg, new Object[0]));
                break;
        }
        return bqA();
    }

    public boolean bqD() {
        switch (bFf().mo6893i(dXh)) {
            case 0:
                return false;
            case 2:
                return ((Boolean) bFf().mo5606d(new aCE(this, dXh, new Object[0]))).booleanValue();
            case 3:
                bFf().mo5606d(new aCE(this, dXh, new Object[0]));
                break;
        }
        return bqC();
    }

    public int bqF() {
        switch (bFf().mo6893i(dXi)) {
            case 0:
                return 0;
            case 2:
                return ((Integer) bFf().mo5606d(new aCE(this, dXi, new Object[0]))).intValue();
            case 3:
                bFf().mo5606d(new aCE(this, dXi, new Object[0]));
                break;
        }
        return bqE();
    }

    public C3438ra<Citizenship> bqx() {
        switch (bFf().mo6893i(dWX)) {
            case 0:
                return null;
            case 2:
                return (C3438ra) bFf().mo5606d(new aCE(this, dWX, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, dWX, new Object[0]));
                break;
        }
        return bqw();
    }

    public boolean bqz() {
        switch (bFf().mo6893i(dXf)) {
            case 0:
                return false;
            case 2:
                return ((Boolean) bFf().mo5606d(new aCE(this, dXf, new Object[0]))).booleanValue();
            case 3:
                bFf().mo5606d(new aCE(this, dXf, new Object[0]));
                break;
        }
        return bqy();
    }

    /* renamed from: c */
    public C5663aRz[] mo25c() {
        return _m_fields;
    }

    /* renamed from: dL */
    public Player mo4982dL() {
        switch (bFf().mo6893i(f1429hF)) {
            case 0:
                return null;
            case 2:
                return (Player) bFf().mo5606d(new aCE(this, f1429hF, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f1429hF, new Object[0]));
                break;
        }
        return m8852dK();
    }

    public void dispose() {
        switch (bFf().mo6893i(_f_dispose_0020_0028_0029V)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, _f_dispose_0020_0028_0029V, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, _f_dispose_0020_0028_0029V, new Object[0]));
                break;
        }
        m8855fg();
    }

    /* renamed from: f */
    public void mo4983f(CitizenshipPack acu) {
        switch (bFf().mo6893i(dWY)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, dWY, new Object[]{acu}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, dWY, new Object[]{acu}));
                break;
        }
        m8853e(acu);
    }

    /* renamed from: j */
    public void mo4984j(Storage qzVar) {
        switch (bFf().mo6893i(col)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, col, new Object[]{qzVar}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, col, new Object[]{qzVar}));
                break;
        }
        m8860i(qzVar);
    }

    /* renamed from: k */
    public long mo4985k(CitizenshipType adl) {
        switch (bFf().mo6893i(dXe)) {
            case 0:
                return 0;
            case 2:
                return ((Long) bFf().mo5606d(new aCE(this, dXe, new Object[]{adl}))).longValue();
            case 3:
                bFf().mo5606d(new aCE(this, dXe, new Object[]{adl}));
                break;
        }
        return m8861j(adl);
    }

    /* renamed from: p */
    public void mo4986p(Player aku) {
        switch (bFf().mo6893i(f1427Ny)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f1427Ny, new Object[]{aku}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f1427Ny, new Object[]{aku}));
                break;
        }
        m8862o(aku);
    }

    /* renamed from: S */
    public void mo10S() {
        super.mo10S();
    }

    @C0064Am(aul = "cbf7145d6318536c1fb677fd64297063", aum = 0)
    private C3438ra<Citizenship> bqw() {
        return bqt();
    }

    @C0064Am(aul = "6de9b896addb7285eb1009afa291ef03", aum = 0)
    /* renamed from: e */
    private void m8853e(CitizenshipPack acu) {
        Citizenship i = m8859i(acu.bOV());
        if (i != null) {
            i.mo6064gl(m8856g(acu));
            mo2066a((C5878acG) new C0572Hv(i.bJB(), i.getExpiration(), m8851dG()));
        } else {
            Citizenship bSj = acu.bOV().bSj();
            bSj.mo6065gn(m8856g(acu));
            bSj.mo6060c(this);
            bSj.mo6067w(m8851dG());
            bqt().add(bSj);
            for (CitizenImprovement d : bSj.bJz()) {
                m8850d(d);
            }
            mo2066a((C5878acG) new C0967OE(bSj.bJB(), bSj.getExpiration(), m8851dG()));
            mo4982dL().dxy().mo6849c(bSj.bJB());
        }
        m8857g(acu.bOV());
    }

    @C0064Am(aul = "f162989d78671cac0c827f60035b0ea0", aum = 0)
    /* renamed from: f */
    private void m8854f(CitizenshipType adl) {
        if (!bqv().containsKey(adl)) {
            bqv().put(adl, 0);
        }
        Integer valueOf = Integer.valueOf(((Integer) bqv().get(adl)).intValue() + 1);
        bqv().put(adl, valueOf);
        m8847b(adl, valueOf);
    }

    @C0064Am(aul = "25a7e94e96c66af7c24de8d38f2869f1", aum = 0)
    /* renamed from: a */
    private void m8845a(CitizenshipType adl, Integer num) {
        for (CitizenshipReward lQVar : adl.bSf()) {
            if (lQVar.getAmount() == num.intValue()) {
                lQVar.mo5852y(m8851dG());
            }
        }
    }

    @C0064Am(aul = "4d3a575e11a14ee016bb476261cfc17d", aum = 0)
    /* renamed from: h */
    private Citizenship m8858h(CitizenshipType adl) {
        for (Citizenship vm : bqt()) {
            if (vm.bJB() == adl) {
                return vm;
            }
        }
        return null;
    }

    @C0064Am(aul = "25b7cfbc002c4e8e930f456f2bbad938", aum = 0)
    /* renamed from: c */
    private void m8849c(CitizenImprovement cZVar) {
        cZVar.mo17624w(m8851dG());
        bqu().put(cZVar.apT(), cZVar);
        cZVar.apR();
    }

    @C0064Am(aul = "51742d630c3b61b04823e28b09e39897", aum = 0)
    /* renamed from: a */
    private void m8844a(Citizenship vm) {
        bqt().remove(vm);
        mo4982dL().dxy().mo6852e(vm.bJB());
        mo2066a((C5878acG) new C2027bF(vm.bJB(), m8851dG()));
        ArrayList<CitizenImprovement> arrayList = new ArrayList<>();
        for (CitizenImprovement next : vm.bJz()) {
            if (bqu().values().contains(next)) {
                arrayList.add(next);
                next.cancel();
            }
        }
        for (CitizenImprovement apT : arrayList) {
            bqu().remove(apT.apT());
        }
    }

    @C0064Am(aul = "165a2cb8d5dcca0a0288e360416cafd6", aum = 0)
    /* renamed from: dK */
    private Player m8852dK() {
        return m8851dG();
    }

    @C0064Am(aul = "b1efad5948fc6e08845dcc05246641e5", aum = 0)
    /* renamed from: o */
    private void m8862o(Player aku) {
        m8846a(aku);
    }

    @C0064Am(aul = "96cfe440e306472b8d2656ea89772a9b", aum = 0)
    /* renamed from: j */
    private long m8861j(CitizenshipType adl) {
        for (Citizenship vm : bqt()) {
            if (vm.bJB().equals(adl)) {
                return vm.getExpiration();
            }
        }
        return 0;
    }

    @C0064Am(aul = "258e29f95fbe81ff41a50391a7da0e3b", aum = 0)
    private boolean bqy() {
        return bqu().get(CitizenImprovement.C2184a.NODE_ACCESS) != null;
    }

    @C0064Am(aul = "5b702f20ee40ef522498b895a4a13f29", aum = 0)
    /* renamed from: fg */
    private void m8855fg() {
        m8846a((Player) null);
        if (bqt().size() > 0) {
            for (Citizenship dispose : bqt()) {
                dispose.dispose();
            }
            bqt().clear();
            mo4982dL().dxy().bFM();
        }
        if (bqu().size() > 0) {
            for (CitizenImprovement dispose2 : bqu().values()) {
                dispose2.dispose();
            }
            bqu().clear();
        }
        m8842R((C1556Wo) null);
    }

    @C0064Am(aul = "98cdf79863ca177b2b60b5bb31ec4050", aum = 0)
    private boolean bqC() {
        return bqu().get(CitizenImprovement.C2184a.HIGHWAY) != null;
    }

    @C0064Am(aul = "c592a74820ff0d6d8b9659306087ef01", aum = 0)
    private int bqE() {
        CitizenImprovement cZVar = (CitizenImprovement) bqu().get(CitizenImprovement.C2184a.HANGAR);
        if (cZVar == null || !(cZVar instanceof HangarImprovement)) {
            return 2;
        }
        return ((HangarImprovement) cZVar).blW();
    }

    @C0064Am(aul = "a1a87d0641f998da7ef16982c12f45d6", aum = 0)
    /* renamed from: i */
    private void m8860i(Storage qzVar) {
        CitizenImprovement cZVar = (CitizenImprovement) bqu().get(CitizenImprovement.C2184a.STORAGE);
        if (cZVar != null && (cZVar instanceof StorageImprovement)) {
            ((StorageImprovement) cZVar).mo23181h(qzVar);
        }
    }
}
