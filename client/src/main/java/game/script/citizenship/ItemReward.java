package game.script.citizenship;

import game.network.message.externalizable.aCE;
import game.network.message.serializable.C3438ra;
import game.script.item.Item;
import game.script.item.ItemType;
import game.script.player.Player;
import game.script.ship.Station;
import logic.baa.C1616Xf;
import logic.baa.C1634Xv;
import logic.baa.C5511aMd;
import logic.baa.C6485anp;
import logic.data.mbean.C6461anR;
import logic.res.code.C5640aRc;
import logic.res.code.C5663aRz;
import logic.res.code.C6494any;
import logic.res.html.C2491fm;
import p001a.*;

import java.util.*;

@C5511aMd
@C6485anp
/* renamed from: a.UQ */
/* compiled from: a */
public class ItemReward extends CitizenshipReward implements C1616Xf {
    public static final int _m_fieldCount = 0;
    public static final C5663aRz[] _m_fields = null;
    public static final int _m_methodCount = 0;
    public static final C2491fm[] _m_methods = null;
    public static final C2491fm aAB = null;
    public static final C2491fm eqA = null;
    public static final C2491fm eqB = null;
    public static final C2491fm eqz = null;
    /* renamed from: gz */
    public static final C5663aRz f1751gz = null;
    public static final long serialVersionUID = 0;
    public static C6494any ___iScriptClass;
    @C0064Am(aul = "45816a076d8d222785d29143c657a491", aum = 0)

    /* renamed from: hC */
    private static C3438ra<ItemType> f1752hC;

    static {
        m10261V();
    }

    public ItemReward() {
        super((C5540aNg) null);
        super.mo10S();
    }

    public ItemReward(C5540aNg ang) {
        super(ang);
    }

    /* renamed from: V */
    static void m10261V() {
        serialVersionUID = (long) 1;
        _m_fieldCount = CitizenshipReward._m_fieldCount + 1;
        _m_methodCount = CitizenshipReward._m_methodCount + 4;
        int i = CitizenshipReward._m_fieldCount;
        C5663aRz[] arzArr = new C5663aRz[(i + 1)];
        C5663aRz b = C5640aRc.m17844b(ItemReward.class, "45816a076d8d222785d29143c657a491", i);
        f1751gz = b;
        arzArr[i] = b;
        int i2 = i + 1;
        _m_fields = arzArr;
        C1634Xv.m11725a((Object[]) CitizenshipReward._m_fields, (Object[]) _m_fields);
        int i3 = CitizenshipReward._m_methodCount;
        C2491fm[] fmVarArr = new C2491fm[(i3 + 4)];
        C2491fm a = C4105zY.m41624a(ItemReward.class, "230c6d4b3caef512de9327b887e6c06e", i3);
        aAB = a;
        fmVarArr[i3] = a;
        int i4 = i3 + 1;
        C2491fm a2 = C4105zY.m41624a(ItemReward.class, "ff8bc961f528698b07a38593ece91377", i4);
        eqz = a2;
        fmVarArr[i4] = a2;
        int i5 = i4 + 1;
        C2491fm a3 = C4105zY.m41624a(ItemReward.class, "3a25e4f65b990414365069ca129af640", i5);
        eqA = a3;
        fmVarArr[i5] = a3;
        int i6 = i5 + 1;
        C2491fm a4 = C4105zY.m41624a(ItemReward.class, "586d539d2d1aa35f745ab5aefe10df84", i6);
        eqB = a4;
        fmVarArr[i6] = a4;
        int i7 = i6 + 1;
        _m_methods = fmVarArr;
        C1634Xv.m11725a((Object[]) CitizenshipReward._m_methods, (Object[]) _m_methods);
        ___iScriptClass = aUO.m18566a(ItemReward.class, C6461anR.class, _m_fields, _m_methods);
    }

    @C3248pc(aYR = C3248pc.C3250b.ADDER, displayName = "Items")
    @C0064Am(aul = "ff8bc961f528698b07a38593ece91377", aum = 0)
    @C5566aOg
    /* renamed from: I */
    private void m10259I(ItemType jCVar) {
        throw new aWi(new aCE(this, eqz, new Object[]{jCVar}));
    }

    @C3248pc(aYR = C3248pc.C3250b.REMOVER, displayName = "Items")
    @C0064Am(aul = "3a25e4f65b990414365069ca129af640", aum = 0)
    @C5566aOg
    /* renamed from: K */
    private void m10260K(ItemType jCVar) {
        throw new aWi(new aCE(this, eqA, new Object[]{jCVar}));
    }

    /* renamed from: dI */
    private C3438ra m10262dI() {
        return (C3438ra) bFf().mo5608dq().mo3214p(f1751gz);
    }

    /* renamed from: m */
    private void m10263m(C3438ra raVar) {
        bFf().mo5608dq().mo3197f(f1751gz, raVar);
    }

    @C3248pc(aYR = C3248pc.C3250b.ADDER, displayName = "Items")
    @C5566aOg
    /* renamed from: J */
    public void mo5849J(ItemType jCVar) {
        switch (bFf().mo6893i(eqz)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, eqz, new Object[]{jCVar}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, eqz, new Object[]{jCVar}));
                break;
        }
        m10259I(jCVar);
    }

    @C3248pc(aYR = C3248pc.C3250b.REMOVER, displayName = "Items")
    @C5566aOg
    /* renamed from: L */
    public void mo5850L(ItemType jCVar) {
        switch (bFf().mo6893i(eqA)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, eqA, new Object[]{jCVar}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, eqA, new Object[]{jCVar}));
                break;
        }
        m10260K(jCVar);
    }

    /* renamed from: T */
    public C6494any mo11T() {
        return ___iScriptClass;
    }

    /* renamed from: U */
    public C2491fm[] mo12U() {
        return _m_methods;
    }

    /* renamed from: W */
    public Object mo13W() {
        return new C6461anR(this);
    }

    /* renamed from: a */
    public Object mo14a(C0495Gr gr) {
        Object[] args = gr.getArgs();
        switch (gr.mo2417hq() - CitizenshipReward._m_methodCount) {
            case 0:
                m10264x((Player) args[0]);
                return null;
            case 1:
                m10259I((ItemType) args[0]);
                return null;
            case 2:
                m10260K((ItemType) args[0]);
                return null;
            case 3:
                return bys();
            default:
                return super.mo14a(gr);
        }
    }

    /* renamed from: a */
    public void mo15a(Collection collection, C0495Gr gr) {
        super.mo15a(collection, gr);
    }

    /* renamed from: c */
    public C5663aRz[] mo25c() {
        return _m_fields;
    }

    @C3248pc(aYR = C3248pc.C3250b.ITERATOR, displayName = "Items")
    public List<ItemType> getItems() {
        switch (bFf().mo6893i(eqB)) {
            case 0:
                return null;
            case 2:
                return (List) bFf().mo5606d(new aCE(this, eqB, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, eqB, new Object[0]));
                break;
        }
        return bys();
    }

    /* renamed from: y */
    public void mo5852y(Player aku) {
        switch (bFf().mo6893i(aAB)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, aAB, new Object[]{aku}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, aAB, new Object[]{aku}));
                break;
        }
        m10264x(aku);
    }

    /* renamed from: S */
    public void mo10S() {
        super.mo10S();
    }

    @C0064Am(aul = "230c6d4b3caef512de9327b887e6c06e", aum = 0)
    /* renamed from: x */
    private void m10264x(Player aku) {
        HashSet<Item> hashSet = new HashSet<>();
        for (ItemType NK : m10262dI()) {
            hashSet.add((Item) NK.mo7459NK());
        }
        if (aku.bQx() != null && aku.bQx().afI().mo7623m((Set<? extends Item>) hashSet)) {
            return;
        }
        if (aku.bhE() instanceof Station) {
            for (Item N : hashSet) {
                ((Station) aku.bhE()).mo602am(aku).mo7615N(N);
            }
            return;
        }
        for (Item N2 : hashSet) {
            aku.dxZ().mo5652xh().mo602am(aku).mo7615N(N2);
        }
    }

    @C3248pc(aYR = C3248pc.C3250b.ITERATOR, displayName = "Items")
    @C0064Am(aul = "586d539d2d1aa35f745ab5aefe10df84", aum = 0)
    private List<ItemType> bys() {
        return Collections.unmodifiableList(m10262dI());
    }
}
