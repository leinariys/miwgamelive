package game.script.citizenship;

import game.network.message.externalizable.aCE;
import game.network.message.serializable.C3438ra;
import game.script.resource.Asset;
import logic.baa.*;
import logic.data.mbean.C6576apc;
import logic.res.code.C5640aRc;
import logic.res.code.C5663aRz;
import logic.res.code.C6494any;
import logic.res.html.C2491fm;
import p001a.*;
import taikodom.infra.script.I18NString;

import java.util.Collection;
import java.util.Collections;
import java.util.List;

@C0566Hp
@C5511aMd
@C6485anp
/* renamed from: a.adL  reason: case insensitive filesystem */
/* compiled from: a */
public class CitizenshipType extends BaseCitizenshipType implements C1616Xf {

    /* renamed from: MN */
    public static final C2491fm f4299MN = null;

    /* renamed from: MT */
    public static final C5663aRz f4300MT = null;

    /* renamed from: MY */
    public static final C2491fm f4301MY = null;

    /* renamed from: MZ */
    public static final C2491fm f4302MZ = null;

    /* renamed from: NY */
    public static final C5663aRz f4303NY = null;

    /* renamed from: Ob */
    public static final C2491fm f4304Ob = null;

    /* renamed from: Oc */
    public static final C2491fm f4305Oc = null;

    /* renamed from: QA */
    public static final C5663aRz f4306QA = null;

    /* renamed from: QD */
    public static final C2491fm f4307QD = null;

    /* renamed from: QE */
    public static final C2491fm f4308QE = null;

    /* renamed from: QF */
    public static final C2491fm f4309QF = null;

    /* renamed from: Qz */
    public static final C5663aRz f4310Qz = null;
    public static final int _m_fieldCount = 0;
    public static final C5663aRz[] _m_fields = null;
    public static final int _m_methodCount = 0;
    public static final C2491fm[] _m_methods = null;
    /* renamed from: dN */
    public static final C2491fm f4311dN = null;
    public static final C5663aRz dWU = null;
    public static final C5663aRz eQZ = null;
    public static final C5663aRz eRb = null;
    public static final C5663aRz eRd = null;
    public static final C5663aRz eRg = null;
    public static final C5663aRz eRi = null;
    public static final C5663aRz eRk = null;
    public static final C5663aRz eRm = null;
    public static final C5663aRz eRo = null;
    public static final C5663aRz emT = null;
    public static final C2491fm fbO = null;
    public static final C2491fm fbP = null;
    public static final C2491fm fkJ = null;
    public static final C2491fm fkK = null;
    public static final C2491fm fkL = null;
    public static final C2491fm fkM = null;
    public static final C2491fm fkN = null;
    public static final C2491fm fkO = null;
    public static final C2491fm fkP = null;
    public static final C2491fm fkQ = null;
    public static final C2491fm fkR = null;
    public static final C2491fm fkS = null;
    public static final C2491fm fkT = null;
    public static final C2491fm fkU = null;
    public static final C2491fm fkV = null;
    public static final C2491fm fkW = null;
    public static final C2491fm fkX = null;
    public static final C2491fm fkY = null;
    public static final C2491fm fkZ = null;
    public static final C2491fm fla = null;
    public static final C2491fm flb = null;
    public static final C2491fm flc = null;
    public static final C2491fm fld = null;
    public static final C2491fm fle = null;
    public static final C2491fm flf = null;
    public static final C2491fm flg = null;
    public static final C2491fm flh = null;
    public static final C2491fm fli = null;
    public static final C2491fm flj = null;
    public static final C2491fm flk = null;
    public static final C2491fm fll = null;
    public static final C2491fm flm = null;
    public static final C2491fm fln = null;
    public static final long serialVersionUID = 0;
    public static C6494any ___iScriptClass;
    @C0064Am(aul = "d07a4ba93c30fabda19b556bcdc4365d", aum = 1)

    /* renamed from: db */
    private static long f4312db;
    @C0064Am(aul = "2765773117cd6814ddddaedc5b26b335", aum = 5)
    private static Asset eQY;
    @C0064Am(aul = "bda71d1dcf319613c3fc5f3e522340e9", aum = 6)
    private static Asset eRa;
    @C0064Am(aul = "ba47049ed33b60d394446bade1290ec7", aum = 7)
    private static Asset eRc;
    @C0064Am(aul = "5927681486d14162274755ca31229e15", aum = 8)
    private static C3438ra<CitizenImprovementType> eRe;
    @C0064Am(aul = "d4aadb0f73ba3aa9351bc224f0de5957", aum = 9)
    private static C3438ra<CitizenshipReward> eRf;
    @C0064Am(aul = "31889d3108ff4dd8fef3a1603105016e", aum = 10)
    private static C3438ra<CitizenshipReward> eRh;
    @C0064Am(aul = "fd8f2ca8ea978974663e85094c54d633", aum = 11)
    private static C3438ra<CitizenshipReward> eRj;
    @C0064Am(aul = "20c622621b93f523052dae317d3995dd", aum = 12)
    private static C3438ra<CitizenshipReward> eRl;
    @C0064Am(aul = "2d4862b3c75f18df301e48a0752057a8", aum = 13)
    private static C3438ra<CitizenshipReward> eRn;
    @C0064Am(aul = "060e6c338ec3bf61a3e1c2990926486e", aum = 2)
    private static float egP;
    @C0064Am(aul = "15c2c8ce4a66c98aa77d3210c2fdc322", aum = 4)

    /* renamed from: jn */
    private static Asset f4313jn;
    @C0064Am(aul = "df7eb7dae6704f95765abdde15677aeb", aum = 0)

    /* renamed from: nh */
    private static I18NString f4314nh;
    @C0064Am(aul = "f378837f9cfc21574c10415875586456", aum = 3)

    /* renamed from: ni */
    private static I18NString f4315ni;

    static {
        m20741V();
    }

    public CitizenshipType() {
        super((C5540aNg) null);
        super.mo10S();
    }

    public CitizenshipType(C5540aNg ang) {
        super(ang);
    }

    /* renamed from: V */
    static void m20741V() {
        serialVersionUID = (long) 1;
        _m_fieldCount = BaseCitizenshipType._m_fieldCount + 14;
        _m_methodCount = BaseCitizenshipType._m_methodCount + 42;
        int i = BaseCitizenshipType._m_fieldCount;
        C5663aRz[] arzArr = new C5663aRz[(i + 14)];
        C5663aRz b = C5640aRc.m17844b(CitizenshipType.class, "df7eb7dae6704f95765abdde15677aeb", i);
        f4306QA = b;
        arzArr[i] = b;
        int i2 = i + 1;
        C5663aRz b2 = C5640aRc.m17844b(CitizenshipType.class, "d07a4ba93c30fabda19b556bcdc4365d", i2);
        f4300MT = b2;
        arzArr[i2] = b2;
        int i3 = i2 + 1;
        C5663aRz b3 = C5640aRc.m17844b(CitizenshipType.class, "060e6c338ec3bf61a3e1c2990926486e", i3);
        emT = b3;
        arzArr[i3] = b3;
        int i4 = i3 + 1;
        C5663aRz b4 = C5640aRc.m17844b(CitizenshipType.class, "f378837f9cfc21574c10415875586456", i4);
        f4310Qz = b4;
        arzArr[i4] = b4;
        int i5 = i4 + 1;
        C5663aRz b5 = C5640aRc.m17844b(CitizenshipType.class, "15c2c8ce4a66c98aa77d3210c2fdc322", i5);
        f4303NY = b5;
        arzArr[i5] = b5;
        int i6 = i5 + 1;
        C5663aRz b6 = C5640aRc.m17844b(CitizenshipType.class, "2765773117cd6814ddddaedc5b26b335", i6);
        eQZ = b6;
        arzArr[i6] = b6;
        int i7 = i6 + 1;
        C5663aRz b7 = C5640aRc.m17844b(CitizenshipType.class, "bda71d1dcf319613c3fc5f3e522340e9", i7);
        eRb = b7;
        arzArr[i7] = b7;
        int i8 = i7 + 1;
        C5663aRz b8 = C5640aRc.m17844b(CitizenshipType.class, "ba47049ed33b60d394446bade1290ec7", i8);
        eRd = b8;
        arzArr[i8] = b8;
        int i9 = i8 + 1;
        C5663aRz b9 = C5640aRc.m17844b(CitizenshipType.class, "5927681486d14162274755ca31229e15", i9);
        dWU = b9;
        arzArr[i9] = b9;
        int i10 = i9 + 1;
        C5663aRz b10 = C5640aRc.m17844b(CitizenshipType.class, "d4aadb0f73ba3aa9351bc224f0de5957", i10);
        eRg = b10;
        arzArr[i10] = b10;
        int i11 = i10 + 1;
        C5663aRz b11 = C5640aRc.m17844b(CitizenshipType.class, "31889d3108ff4dd8fef3a1603105016e", i11);
        eRi = b11;
        arzArr[i11] = b11;
        int i12 = i11 + 1;
        C5663aRz b12 = C5640aRc.m17844b(CitizenshipType.class, "fd8f2ca8ea978974663e85094c54d633", i12);
        eRk = b12;
        arzArr[i12] = b12;
        int i13 = i12 + 1;
        C5663aRz b13 = C5640aRc.m17844b(CitizenshipType.class, "20c622621b93f523052dae317d3995dd", i13);
        eRm = b13;
        arzArr[i13] = b13;
        int i14 = i13 + 1;
        C5663aRz b14 = C5640aRc.m17844b(CitizenshipType.class, "2d4862b3c75f18df301e48a0752057a8", i14);
        eRo = b14;
        arzArr[i14] = b14;
        int i15 = i14 + 1;
        _m_fields = arzArr;
        C1634Xv.m11725a((Object[]) BaseCitizenshipType._m_fields, (Object[]) _m_fields);
        int i16 = BaseCitizenshipType._m_methodCount;
        C2491fm[] fmVarArr = new C2491fm[(i16 + 42)];
        C2491fm a = C4105zY.m41624a(CitizenshipType.class, "61cac7568c1d3fddcbcec999c6c733c4", i16);
        f4308QE = a;
        fmVarArr[i16] = a;
        int i17 = i16 + 1;
        C2491fm a2 = C4105zY.m41624a(CitizenshipType.class, "4590f4a3c901f87c028d7a5fe924dc15", i17);
        f4309QF = a2;
        fmVarArr[i17] = a2;
        int i18 = i17 + 1;
        C2491fm a3 = C4105zY.m41624a(CitizenshipType.class, "93175533dd2e6f8171ddb6ae614e29e1", i18);
        f4301MY = a3;
        fmVarArr[i18] = a3;
        int i19 = i18 + 1;
        C2491fm a4 = C4105zY.m41624a(CitizenshipType.class, "132f986f59abaabc64fbd175715f2de5", i19);
        f4302MZ = a4;
        fmVarArr[i19] = a4;
        int i20 = i19 + 1;
        C2491fm a5 = C4105zY.m41624a(CitizenshipType.class, "a30211fcc45545c75225dba098abbb44", i20);
        fbO = a5;
        fmVarArr[i20] = a5;
        int i21 = i20 + 1;
        C2491fm a6 = C4105zY.m41624a(CitizenshipType.class, "b1bc3546d137133c4bf715d67a278ec5", i21);
        fbP = a6;
        fmVarArr[i21] = a6;
        int i22 = i21 + 1;
        C2491fm a7 = C4105zY.m41624a(CitizenshipType.class, "84e4597d22334b5d653378c66d3d4eef", i22);
        f4299MN = a7;
        fmVarArr[i22] = a7;
        int i23 = i22 + 1;
        C2491fm a8 = C4105zY.m41624a(CitizenshipType.class, "87c6f4c6545b6493cb47d705de28ddf7", i23);
        f4307QD = a8;
        fmVarArr[i23] = a8;
        int i24 = i23 + 1;
        C2491fm a9 = C4105zY.m41624a(CitizenshipType.class, "80f1bb50230aff87e0125e2e15d82a1d", i24);
        f4304Ob = a9;
        fmVarArr[i24] = a9;
        int i25 = i24 + 1;
        C2491fm a10 = C4105zY.m41624a(CitizenshipType.class, "17b66ba8dc1747cd88da257764b775aa", i25);
        f4305Oc = a10;
        fmVarArr[i25] = a10;
        int i26 = i25 + 1;
        C2491fm a11 = C4105zY.m41624a(CitizenshipType.class, "39c61f7261b4d64cdb4072e57fb6c6c4", i26);
        fkJ = a11;
        fmVarArr[i26] = a11;
        int i27 = i26 + 1;
        C2491fm a12 = C4105zY.m41624a(CitizenshipType.class, "937bab2e5fe18fe173bab34026ac5352", i27);
        fkK = a12;
        fmVarArr[i27] = a12;
        int i28 = i27 + 1;
        C2491fm a13 = C4105zY.m41624a(CitizenshipType.class, "1018b85621920ff2c2d156a7efa85c30", i28);
        fkL = a13;
        fmVarArr[i28] = a13;
        int i29 = i28 + 1;
        C2491fm a14 = C4105zY.m41624a(CitizenshipType.class, "c92619b4f2d31833c452105354804784", i29);
        fkM = a14;
        fmVarArr[i29] = a14;
        int i30 = i29 + 1;
        C2491fm a15 = C4105zY.m41624a(CitizenshipType.class, "5e70653b5c6cc11e5f9b87ce7e35a386", i30);
        fkN = a15;
        fmVarArr[i30] = a15;
        int i31 = i30 + 1;
        C2491fm a16 = C4105zY.m41624a(CitizenshipType.class, "52bded6f80cb2d5e923202f1c2bee38f", i31);
        fkO = a16;
        fmVarArr[i31] = a16;
        int i32 = i31 + 1;
        C2491fm a17 = C4105zY.m41624a(CitizenshipType.class, "d717516b361202f8dda26409f9fe418e", i32);
        fkP = a17;
        fmVarArr[i32] = a17;
        int i33 = i32 + 1;
        C2491fm a18 = C4105zY.m41624a(CitizenshipType.class, "d4b66e9d4cd49f9209b3906ad2e9c840", i33);
        fkQ = a18;
        fmVarArr[i33] = a18;
        int i34 = i33 + 1;
        C2491fm a19 = C4105zY.m41624a(CitizenshipType.class, "e345f71c23ecbb7fc035d8628fbfa435", i34);
        fkR = a19;
        fmVarArr[i34] = a19;
        int i35 = i34 + 1;
        C2491fm a20 = C4105zY.m41624a(CitizenshipType.class, "bab5f31a441b81776b57ced1429f8745", i35);
        fkS = a20;
        fmVarArr[i35] = a20;
        int i36 = i35 + 1;
        C2491fm a21 = C4105zY.m41624a(CitizenshipType.class, "7309f73781c5ee5d0fc02f5e1dc4a794", i36);
        fkT = a21;
        fmVarArr[i36] = a21;
        int i37 = i36 + 1;
        C2491fm a22 = C4105zY.m41624a(CitizenshipType.class, "aeeeb39099389a1cd1159481169a5040", i37);
        fkU = a22;
        fmVarArr[i37] = a22;
        int i38 = i37 + 1;
        C2491fm a23 = C4105zY.m41624a(CitizenshipType.class, "cb058713b6f102ba0e2061d6b9434577", i38);
        fkV = a23;
        fmVarArr[i38] = a23;
        int i39 = i38 + 1;
        C2491fm a24 = C4105zY.m41624a(CitizenshipType.class, "8c0dd6ee24e95d77e4e17455fedbd64a", i39);
        fkW = a24;
        fmVarArr[i39] = a24;
        int i40 = i39 + 1;
        C2491fm a25 = C4105zY.m41624a(CitizenshipType.class, "2b030efceea8794d124fceed471e688f", i40);
        fkX = a25;
        fmVarArr[i40] = a25;
        int i41 = i40 + 1;
        C2491fm a26 = C4105zY.m41624a(CitizenshipType.class, "361fe9ae99c1d95ad322799e5c2aec30", i41);
        fkY = a26;
        fmVarArr[i41] = a26;
        int i42 = i41 + 1;
        C2491fm a27 = C4105zY.m41624a(CitizenshipType.class, "aa1f9451b3ced9da46fe309f7322133c", i42);
        fkZ = a27;
        fmVarArr[i42] = a27;
        int i43 = i42 + 1;
        C2491fm a28 = C4105zY.m41624a(CitizenshipType.class, "c8dbbac9e2b81d161508e8ead56300f3", i43);
        fla = a28;
        fmVarArr[i43] = a28;
        int i44 = i43 + 1;
        C2491fm a29 = C4105zY.m41624a(CitizenshipType.class, "6366e1b2adcd4232dfe27ebfa02e7170", i44);
        flb = a29;
        fmVarArr[i44] = a29;
        int i45 = i44 + 1;
        C2491fm a30 = C4105zY.m41624a(CitizenshipType.class, "15049eee3742f739d53fe41cacdf3a6c", i45);
        flc = a30;
        fmVarArr[i45] = a30;
        int i46 = i45 + 1;
        C2491fm a31 = C4105zY.m41624a(CitizenshipType.class, "99d03b7c1215c9c3c3c28c9bad6baac3", i46);
        fld = a31;
        fmVarArr[i46] = a31;
        int i47 = i46 + 1;
        C2491fm a32 = C4105zY.m41624a(CitizenshipType.class, "553024800e903df204d8d5bab36ff46e", i47);
        fle = a32;
        fmVarArr[i47] = a32;
        int i48 = i47 + 1;
        C2491fm a33 = C4105zY.m41624a(CitizenshipType.class, "2f3388e4a7161139cac627418b01563b", i48);
        flf = a33;
        fmVarArr[i48] = a33;
        int i49 = i48 + 1;
        C2491fm a34 = C4105zY.m41624a(CitizenshipType.class, "ee2267bcd62f689f77174d3ebf869765", i49);
        flg = a34;
        fmVarArr[i49] = a34;
        int i50 = i49 + 1;
        C2491fm a35 = C4105zY.m41624a(CitizenshipType.class, "3ce4d322aa41c67a900bbcfd6803124d", i50);
        flh = a35;
        fmVarArr[i50] = a35;
        int i51 = i50 + 1;
        C2491fm a36 = C4105zY.m41624a(CitizenshipType.class, "3379d592803e0233602023e9312e81ec", i51);
        fli = a36;
        fmVarArr[i51] = a36;
        int i52 = i51 + 1;
        C2491fm a37 = C4105zY.m41624a(CitizenshipType.class, "cadf97dd2f3400d58c8ebf411aca2f0a", i52);
        flj = a37;
        fmVarArr[i52] = a37;
        int i53 = i52 + 1;
        C2491fm a38 = C4105zY.m41624a(CitizenshipType.class, "686d7966f8d92b0893f81cf7f01e28be", i53);
        flk = a38;
        fmVarArr[i53] = a38;
        int i54 = i53 + 1;
        C2491fm a39 = C4105zY.m41624a(CitizenshipType.class, "a66ac44796df2eb0fa904c679c9dd2e7", i54);
        fll = a39;
        fmVarArr[i54] = a39;
        int i55 = i54 + 1;
        C2491fm a40 = C4105zY.m41624a(CitizenshipType.class, "142345686b4802b102c289f7e8730fe2", i55);
        flm = a40;
        fmVarArr[i55] = a40;
        int i56 = i55 + 1;
        C2491fm a41 = C4105zY.m41624a(CitizenshipType.class, "f2d34351f8864c308fc888ad504b4f16", i56);
        fln = a41;
        fmVarArr[i56] = a41;
        int i57 = i56 + 1;
        C2491fm a42 = C4105zY.m41624a(CitizenshipType.class, "0ac1f834fc2ca9d1319a78886a7643a6", i57);
        f4311dN = a42;
        fmVarArr[i57] = a42;
        int i58 = i57 + 1;
        _m_methods = fmVarArr;
        C1634Xv.m11725a((Object[]) BaseCitizenshipType._m_methods, (Object[]) _m_methods);
        ___iScriptClass = aUO.m18566a(CitizenshipType.class, C6576apc.class, _m_fields, _m_methods);
    }

    /* renamed from: G */
    private void m20739G(long j) {
        bFf().mo5608dq().mo3184b(f4300MT, j);
    }

    /* renamed from: bA */
    private void m20743bA(C3438ra raVar) {
        bFf().mo5608dq().mo3197f(eRi, raVar);
    }

    /* renamed from: bB */
    private void m20744bB(C3438ra raVar) {
        bFf().mo5608dq().mo3197f(eRk, raVar);
    }

    /* renamed from: bC */
    private void m20745bC(C3438ra raVar) {
        bFf().mo5608dq().mo3197f(eRm, raVar);
    }

    /* renamed from: bD */
    private void m20746bD(C3438ra raVar) {
        bFf().mo5608dq().mo3197f(eRo, raVar);
    }

    private Asset bJl() {
        return (Asset) bFf().mo5608dq().mo3214p(eQZ);
    }

    private Asset bJm() {
        return (Asset) bFf().mo5608dq().mo3214p(eRb);
    }

    private Asset bJn() {
        return (Asset) bFf().mo5608dq().mo3214p(eRd);
    }

    private C3438ra bJo() {
        return (C3438ra) bFf().mo5608dq().mo3214p(dWU);
    }

    private C3438ra bJp() {
        return (C3438ra) bFf().mo5608dq().mo3214p(eRg);
    }

    private C3438ra bJq() {
        return (C3438ra) bFf().mo5608dq().mo3214p(eRi);
    }

    private C3438ra bJr() {
        return (C3438ra) bFf().mo5608dq().mo3214p(eRk);
    }

    private C3438ra bJs() {
        return (C3438ra) bFf().mo5608dq().mo3214p(eRm);
    }

    private C3438ra bJt() {
        return (C3438ra) bFf().mo5608dq().mo3214p(eRo);
    }

    /* renamed from: bb */
    private void m20747bb(Asset tCVar) {
        bFf().mo5608dq().mo3197f(eQZ, tCVar);
    }

    /* renamed from: bc */
    private void m20748bc(Asset tCVar) {
        bFf().mo5608dq().mo3197f(eRb, tCVar);
    }

    /* renamed from: bd */
    private void m20749bd(Asset tCVar) {
        bFf().mo5608dq().mo3197f(eRd, tCVar);
    }

    /* renamed from: bq */
    private void m20753bq(I18NString i18NString) {
        bFf().mo5608dq().mo3197f(f4310Qz, i18NString);
    }

    /* renamed from: br */
    private void m20754br(I18NString i18NString) {
        bFf().mo5608dq().mo3197f(f4306QA, i18NString);
    }

    private float bxp() {
        return bFf().mo5608dq().mo3211m(emT);
    }

    /* renamed from: by */
    private void m20757by(C3438ra raVar) {
        bFf().mo5608dq().mo3197f(dWU, raVar);
    }

    /* renamed from: bz */
    private void m20758bz(C3438ra raVar) {
        bFf().mo5608dq().mo3197f(eRg, raVar);
    }

    /* renamed from: ia */
    private void m20765ia(float f) {
        bFf().mo5608dq().mo3150a(emT, f);
    }

    /* renamed from: rU */
    private long m20771rU() {
        return bFf().mo5608dq().mo3213o(f4300MT);
    }

    /* renamed from: sG */
    private Asset m20774sG() {
        return (Asset) bFf().mo5608dq().mo3214p(f4303NY);
    }

    /* renamed from: t */
    private void m20776t(Asset tCVar) {
        bFf().mo5608dq().mo3197f(f4303NY, tCVar);
    }

    /* renamed from: vS */
    private I18NString m20779vS() {
        return (I18NString) bFf().mo5608dq().mo3214p(f4310Qz);
    }

    /* renamed from: vT */
    private I18NString m20780vT() {
        return (I18NString) bFf().mo5608dq().mo3214p(f4306QA);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Price")
    /* renamed from: K */
    public void mo12776K(long j) {
        switch (bFf().mo6893i(f4302MZ)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f4302MZ, new Object[]{new Long(j)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f4302MZ, new Object[]{new Long(j)}));
                break;
        }
        m20740J(j);
    }

    /* renamed from: T */
    public C6494any mo11T() {
        return ___iScriptClass;
    }

    /* renamed from: U */
    public C2491fm[] mo12U() {
        return _m_methods;
    }

    /* renamed from: W */
    public Object mo13W() {
        return new C6576apc(this);
    }

    /* renamed from: a */
    public Object mo14a(C0495Gr gr) {
        Object[] args = gr.getArgs();
        switch (gr.mo2417hq() - BaseCitizenshipType._m_methodCount) {
            case 0:
                return m20781vV();
            case 1:
                m20756bu((I18NString) args[0]);
                return null;
            case 2:
                return new Long(m20772rY());
            case 3:
                m20740J(((Long) args[0]).longValue());
                return null;
            case 4:
                return new Float(bOS());
            case 5:
                m20764iV(((Float) args[0]).floatValue());
                return null;
            case 6:
                return m20770rO();
            case 7:
                m20755bs((I18NString) args[0]);
                return null;
            case 8:
                return m20775sJ();
            case 9:
                m20778u((Asset) args[0]);
                return null;
            case 10:
                return bRE();
            case 11:
                m20750be((Asset) args[0]);
                return null;
            case 12:
                return bRG();
            case 13:
                m20751bg((Asset) args[0]);
                return null;
            case 14:
                return bRI();
            case 15:
                m20752bi((Asset) args[0]);
                return null;
            case 16:
                m20760f((CitizenImprovementType) args[0]);
                return null;
            case 17:
                m20762h((CitizenImprovementType) args[0]);
                return null;
            case 18:
                return bRK();
            case 19:
                return bRM();
            case 20:
                m20759e((CitizenshipReward) args[0]);
                return null;
            case 21:
                m20761g((CitizenshipReward) args[0]);
                return null;
            case 22:
                return bRO();
            case 23:
                return bRQ();
            case 24:
                m20763i((CitizenshipReward) args[0]);
                return null;
            case 25:
                m20766k((CitizenshipReward) args[0]);
                return null;
            case 26:
                return bRS();
            case 27:
                return bRU();
            case 28:
                m20767m((CitizenshipReward) args[0]);
                return null;
            case 29:
                m20768o((CitizenshipReward) args[0]);
                return null;
            case 30:
                return bRW();
            case 31:
                return bRY();
            case 32:
                m20769q((CitizenshipReward) args[0]);
                return null;
            case 33:
                m20773s((CitizenshipReward) args[0]);
                return null;
            case 34:
                return bSa();
            case 35:
                return bSc();
            case 36:
                m20777u((CitizenshipReward) args[0]);
                return null;
            case 37:
                m20782w((CitizenshipReward) args[0]);
                return null;
            case 38:
                return bSe();
            case 39:
                return bSg();
            case 40:
                return bSi();
            case 41:
                return m20742aT();
            default:
                return super.mo14a(gr);
        }
    }

    /* renamed from: a */
    public void mo15a(Collection collection, C0495Gr gr) {
        super.mo15a(collection, gr);
    }

    /* access modifiers changed from: protected */
    /* renamed from: aU */
    public <T extends aDJ> T mo745aU() {
        switch (bFf().mo6893i(f4311dN)) {
            case 0:
                return null;
            case 2:
                return (aDJ) bFf().mo5606d(new aCE(this, f4311dN, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f4311dN, new Object[0]));
                break;
        }
        return m20742aT();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Days")
    public float bOT() {
        switch (bFf().mo6893i(fbO)) {
            case 0:
                return 0.0f;
            case 2:
                return ((Float) bFf().mo5606d(new aCE(this, fbO, new Object[0]))).floatValue();
            case 3:
                bFf().mo5606d(new aCE(this, fbO, new Object[0]));
                break;
        }
        return bOS();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Owned Icon")
    public Asset bRF() {
        switch (bFf().mo6893i(fkJ)) {
            case 0:
                return null;
            case 2:
                return (Asset) bFf().mo5606d(new aCE(this, fkJ, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, fkJ, new Object[0]));
                break;
        }
        return bRE();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Little Icon")
    public Asset bRH() {
        switch (bFf().mo6893i(fkL)) {
            case 0:
                return null;
            case 2:
                return (Asset) bFf().mo5606d(new aCE(this, fkL, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, fkL, new Object[0]));
                break;
        }
        return bRG();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Background")
    public Asset bRJ() {
        switch (bFf().mo6893i(fkN)) {
            case 0:
                return null;
            case 2:
                return (Asset) bFf().mo5606d(new aCE(this, fkN, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, fkN, new Object[0]));
                break;
        }
        return bRI();
    }

    public C3438ra<CitizenImprovementType> bRL() {
        switch (bFf().mo6893i(fkR)) {
            case 0:
                return null;
            case 2:
                return (C3438ra) bFf().mo5606d(new aCE(this, fkR, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, fkR, new Object[0]));
                break;
        }
        return bRK();
    }

    @C3248pc(aYR = C3248pc.C3250b.ITERATOR, displayName = "Improvements")
    public List<CitizenImprovementType> bRN() {
        switch (bFf().mo6893i(fkS)) {
            case 0:
                return null;
            case 2:
                return (List) bFf().mo5606d(new aCE(this, fkS, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, fkS, new Object[0]));
                break;
        }
        return bRM();
    }

    public C3438ra<CitizenshipReward> bRP() {
        switch (bFf().mo6893i(fkV)) {
            case 0:
                return null;
            case 2:
                return (C3438ra) bFf().mo5606d(new aCE(this, fkV, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, fkV, new Object[0]));
                break;
        }
        return bRO();
    }

    @C3248pc(aYR = C3248pc.C3250b.ITERATOR, displayName = "Bomber Reward")
    public List<CitizenshipReward> bRR() {
        switch (bFf().mo6893i(fkW)) {
            case 0:
                return null;
            case 2:
                return (List) bFf().mo5606d(new aCE(this, fkW, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, fkW, new Object[0]));
                break;
        }
        return bRQ();
    }

    public C3438ra<CitizenshipReward> bRT() {
        switch (bFf().mo6893i(fkZ)) {
            case 0:
                return null;
            case 2:
                return (C3438ra) bFf().mo5606d(new aCE(this, fkZ, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, fkZ, new Object[0]));
                break;
        }
        return bRS();
    }

    @C3248pc(aYR = C3248pc.C3250b.ITERATOR, displayName = "Fighter Reward")
    public List<CitizenshipReward> bRV() {
        switch (bFf().mo6893i(fla)) {
            case 0:
                return null;
            case 2:
                return (List) bFf().mo5606d(new aCE(this, fla, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, fla, new Object[0]));
                break;
        }
        return bRU();
    }

    public C3438ra<CitizenshipReward> bRX() {
        switch (bFf().mo6893i(fld)) {
            case 0:
                return null;
            case 2:
                return (C3438ra) bFf().mo5606d(new aCE(this, fld, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, fld, new Object[0]));
                break;
        }
        return bRW();
    }

    @C3248pc(aYR = C3248pc.C3250b.ITERATOR, displayName = "Explorer Reward")
    public List<CitizenshipReward> bRZ() {
        switch (bFf().mo6893i(fle)) {
            case 0:
                return null;
            case 2:
                return (List) bFf().mo5606d(new aCE(this, fle, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, fle, new Object[0]));
                break;
        }
        return bRY();
    }

    public C3438ra<CitizenshipReward> bSb() {
        switch (bFf().mo6893i(flh)) {
            case 0:
                return null;
            case 2:
                return (C3438ra) bFf().mo5606d(new aCE(this, flh, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, flh, new Object[0]));
                break;
        }
        return bSa();
    }

    @C3248pc(aYR = C3248pc.C3250b.ITERATOR, displayName = "Freighter Reward")
    public List<CitizenshipReward> bSd() {
        switch (bFf().mo6893i(fli)) {
            case 0:
                return null;
            case 2:
                return (List) bFf().mo5606d(new aCE(this, fli, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, fli, new Object[0]));
                break;
        }
        return bSc();
    }

    public C3438ra<CitizenshipReward> bSf() {
        switch (bFf().mo6893i(fll)) {
            case 0:
                return null;
            case 2:
                return (C3438ra) bFf().mo5606d(new aCE(this, fll, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, fll, new Object[0]));
                break;
        }
        return bSe();
    }

    @C3248pc(aYR = C3248pc.C3250b.ITERATOR, displayName = "Default Reward")
    public List<CitizenshipReward> bSh() {
        switch (bFf().mo6893i(flm)) {
            case 0:
                return null;
            case 2:
                return (List) bFf().mo5606d(new aCE(this, flm, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, flm, new Object[0]));
                break;
        }
        return bSg();
    }

    public Citizenship bSj() {
        switch (bFf().mo6893i(fln)) {
            case 0:
                return null;
            case 2:
                return (Citizenship) bFf().mo5606d(new aCE(this, fln, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, fln, new Object[0]));
                break;
        }
        return bSi();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Owned Icon")
    /* renamed from: bf */
    public void mo12794bf(Asset tCVar) {
        switch (bFf().mo6893i(fkK)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, fkK, new Object[]{tCVar}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, fkK, new Object[]{tCVar}));
                break;
        }
        m20750be(tCVar);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Little Icon")
    /* renamed from: bh */
    public void mo12795bh(Asset tCVar) {
        switch (bFf().mo6893i(fkM)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, fkM, new Object[]{tCVar}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, fkM, new Object[]{tCVar}));
                break;
        }
        m20751bg(tCVar);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Background")
    /* renamed from: bj */
    public void mo12796bj(Asset tCVar) {
        switch (bFf().mo6893i(fkO)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, fkO, new Object[]{tCVar}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, fkO, new Object[]{tCVar}));
                break;
        }
        m20752bi(tCVar);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Title")
    /* renamed from: bt */
    public void mo12797bt(I18NString i18NString) {
        switch (bFf().mo6893i(f4307QD)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f4307QD, new Object[]{i18NString}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f4307QD, new Object[]{i18NString}));
                break;
        }
        m20755bs(i18NString);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Description")
    /* renamed from: bv */
    public void mo12798bv(I18NString i18NString) {
        switch (bFf().mo6893i(f4309QF)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f4309QF, new Object[]{i18NString}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f4309QF, new Object[]{i18NString}));
                break;
        }
        m20756bu(i18NString);
    }

    /* renamed from: c */
    public C5663aRz[] mo25c() {
        return _m_fields;
    }

    @C3248pc(aYR = C3248pc.C3250b.ADDER, displayName = "Bomber Reward")
    /* renamed from: f */
    public void mo12799f(CitizenshipReward lQVar) {
        switch (bFf().mo6893i(fkT)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, fkT, new Object[]{lQVar}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, fkT, new Object[]{lQVar}));
                break;
        }
        m20759e(lQVar);
    }

    @C3248pc(aYR = C3248pc.C3250b.ADDER, displayName = "Improvements")
    /* renamed from: g */
    public void mo12800g(CitizenImprovementType kt) {
        switch (bFf().mo6893i(fkP)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, fkP, new Object[]{kt}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, fkP, new Object[]{kt}));
                break;
        }
        m20760f(kt);
    }

    @C3248pc(aYR = C3248pc.C3250b.REMOVER, displayName = "Bomber Reward")
    /* renamed from: h */
    public void mo12801h(CitizenshipReward lQVar) {
        switch (bFf().mo6893i(fkU)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, fkU, new Object[]{lQVar}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, fkU, new Object[]{lQVar}));
                break;
        }
        m20761g(lQVar);
    }

    @C3248pc(aYR = C3248pc.C3250b.REMOVER, displayName = "Improvements")
    /* renamed from: i */
    public void mo12802i(CitizenImprovementType kt) {
        switch (bFf().mo6893i(fkQ)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, fkQ, new Object[]{kt}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, fkQ, new Object[]{kt}));
                break;
        }
        m20762h(kt);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Days")
    /* renamed from: iW */
    public void mo12803iW(float f) {
        switch (bFf().mo6893i(fbP)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, fbP, new Object[]{new Float(f)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, fbP, new Object[]{new Float(f)}));
                break;
        }
        m20764iV(f);
    }

    @C3248pc(aYR = C3248pc.C3250b.ADDER, displayName = "Fighter Reward")
    /* renamed from: j */
    public void mo12804j(CitizenshipReward lQVar) {
        switch (bFf().mo6893i(fkX)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, fkX, new Object[]{lQVar}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, fkX, new Object[]{lQVar}));
                break;
        }
        m20763i(lQVar);
    }

    @C3248pc(aYR = C3248pc.C3250b.REMOVER, displayName = "Fighter Reward")
    /* renamed from: l */
    public void mo12805l(CitizenshipReward lQVar) {
        switch (bFf().mo6893i(fkY)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, fkY, new Object[]{lQVar}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, fkY, new Object[]{lQVar}));
                break;
        }
        m20766k(lQVar);
    }

    @C3248pc(aYR = C3248pc.C3250b.ADDER, displayName = "Explorer Reward")
    /* renamed from: n */
    public void mo12806n(CitizenshipReward lQVar) {
        switch (bFf().mo6893i(flb)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, flb, new Object[]{lQVar}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, flb, new Object[]{lQVar}));
                break;
        }
        m20767m(lQVar);
    }

    @C3248pc(aYR = C3248pc.C3250b.REMOVER, displayName = "Explorer Reward")
    /* renamed from: p */
    public void mo12807p(CitizenshipReward lQVar) {
        switch (bFf().mo6893i(flc)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, flc, new Object[]{lQVar}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, flc, new Object[]{lQVar}));
                break;
        }
        m20768o(lQVar);
    }

    @C3248pc(aYR = C3248pc.C3250b.ADDER, displayName = "Freighter Reward")
    /* renamed from: r */
    public void mo12808r(CitizenshipReward lQVar) {
        switch (bFf().mo6893i(flf)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, flf, new Object[]{lQVar}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, flf, new Object[]{lQVar}));
                break;
        }
        m20769q(lQVar);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Title")
    /* renamed from: rP */
    public I18NString mo12809rP() {
        switch (bFf().mo6893i(f4299MN)) {
            case 0:
                return null;
            case 2:
                return (I18NString) bFf().mo5606d(new aCE(this, f4299MN, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f4299MN, new Object[0]));
                break;
        }
        return m20770rO();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Price")
    /* renamed from: rZ */
    public long mo12810rZ() {
        switch (bFf().mo6893i(f4301MY)) {
            case 0:
                return 0;
            case 2:
                return ((Long) bFf().mo5606d(new aCE(this, f4301MY, new Object[0]))).longValue();
            case 3:
                bFf().mo5606d(new aCE(this, f4301MY, new Object[0]));
                break;
        }
        return m20772rY();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Icon")
    /* renamed from: sK */
    public Asset mo12811sK() {
        switch (bFf().mo6893i(f4304Ob)) {
            case 0:
                return null;
            case 2:
                return (Asset) bFf().mo5606d(new aCE(this, f4304Ob, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f4304Ob, new Object[0]));
                break;
        }
        return m20775sJ();
    }

    @C3248pc(aYR = C3248pc.C3250b.REMOVER, displayName = "Freighter Reward")
    /* renamed from: t */
    public void mo12812t(CitizenshipReward lQVar) {
        switch (bFf().mo6893i(flg)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, flg, new Object[]{lQVar}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, flg, new Object[]{lQVar}));
                break;
        }
        m20773s(lQVar);
    }

    @C3248pc(aYR = C3248pc.C3250b.ADDER, displayName = "Default Reward")
    /* renamed from: v */
    public void mo12813v(CitizenshipReward lQVar) {
        switch (bFf().mo6893i(flj)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, flj, new Object[]{lQVar}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, flj, new Object[]{lQVar}));
                break;
        }
        m20777u(lQVar);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Icon")
    /* renamed from: v */
    public void mo12814v(Asset tCVar) {
        switch (bFf().mo6893i(f4305Oc)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f4305Oc, new Object[]{tCVar}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f4305Oc, new Object[]{tCVar}));
                break;
        }
        m20778u(tCVar);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Description")
    /* renamed from: vW */
    public I18NString mo12815vW() {
        switch (bFf().mo6893i(f4308QE)) {
            case 0:
                return null;
            case 2:
                return (I18NString) bFf().mo5606d(new aCE(this, f4308QE, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f4308QE, new Object[0]));
                break;
        }
        return m20781vV();
    }

    @C3248pc(aYR = C3248pc.C3250b.REMOVER, displayName = "Default Reward")
    /* renamed from: x */
    public void mo12816x(CitizenshipReward lQVar) {
        switch (bFf().mo6893i(flk)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, flk, new Object[]{lQVar}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, flk, new Object[]{lQVar}));
                break;
        }
        m20782w(lQVar);
    }

    /* renamed from: S */
    public void mo10S() {
        super.mo10S();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Description")
    @C0064Am(aul = "61cac7568c1d3fddcbcec999c6c733c4", aum = 0)
    /* renamed from: vV */
    private I18NString m20781vV() {
        return m20780vT();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Description")
    @C0064Am(aul = "4590f4a3c901f87c028d7a5fe924dc15", aum = 0)
    /* renamed from: bu */
    private void m20756bu(I18NString i18NString) {
        m20754br(i18NString);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Price")
    @C0064Am(aul = "93175533dd2e6f8171ddb6ae614e29e1", aum = 0)
    /* renamed from: rY */
    private long m20772rY() {
        return m20771rU();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Price")
    @C0064Am(aul = "132f986f59abaabc64fbd175715f2de5", aum = 0)
    /* renamed from: J */
    private void m20740J(long j) {
        m20739G(j);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Days")
    @C0064Am(aul = "a30211fcc45545c75225dba098abbb44", aum = 0)
    private float bOS() {
        return bxp();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Days")
    @C0064Am(aul = "b1bc3546d137133c4bf715d67a278ec5", aum = 0)
    /* renamed from: iV */
    private void m20764iV(float f) {
        m20765ia(f);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Title")
    @C0064Am(aul = "84e4597d22334b5d653378c66d3d4eef", aum = 0)
    /* renamed from: rO */
    private I18NString m20770rO() {
        return m20779vS();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Title")
    @C0064Am(aul = "87c6f4c6545b6493cb47d705de28ddf7", aum = 0)
    /* renamed from: bs */
    private void m20755bs(I18NString i18NString) {
        m20753bq(i18NString);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Icon")
    @C0064Am(aul = "80f1bb50230aff87e0125e2e15d82a1d", aum = 0)
    /* renamed from: sJ */
    private Asset m20775sJ() {
        return m20774sG();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Icon")
    @C0064Am(aul = "17b66ba8dc1747cd88da257764b775aa", aum = 0)
    /* renamed from: u */
    private void m20778u(Asset tCVar) {
        m20776t(tCVar);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Owned Icon")
    @C0064Am(aul = "39c61f7261b4d64cdb4072e57fb6c6c4", aum = 0)
    private Asset bRE() {
        return bJl();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Owned Icon")
    @C0064Am(aul = "937bab2e5fe18fe173bab34026ac5352", aum = 0)
    /* renamed from: be */
    private void m20750be(Asset tCVar) {
        m20747bb(tCVar);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Little Icon")
    @C0064Am(aul = "1018b85621920ff2c2d156a7efa85c30", aum = 0)
    private Asset bRG() {
        return bJm();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Little Icon")
    @C0064Am(aul = "c92619b4f2d31833c452105354804784", aum = 0)
    /* renamed from: bg */
    private void m20751bg(Asset tCVar) {
        m20748bc(tCVar);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Background")
    @C0064Am(aul = "5e70653b5c6cc11e5f9b87ce7e35a386", aum = 0)
    private Asset bRI() {
        return bJn();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Background")
    @C0064Am(aul = "52bded6f80cb2d5e923202f1c2bee38f", aum = 0)
    /* renamed from: bi */
    private void m20752bi(Asset tCVar) {
        m20749bd(tCVar);
    }

    @C3248pc(aYR = C3248pc.C3250b.ADDER, displayName = "Improvements")
    @C0064Am(aul = "d717516b361202f8dda26409f9fe418e", aum = 0)
    /* renamed from: f */
    private void m20760f(CitizenImprovementType kt) {
        bJo().add(kt);
    }

    @C3248pc(aYR = C3248pc.C3250b.REMOVER, displayName = "Improvements")
    @C0064Am(aul = "d4b66e9d4cd49f9209b3906ad2e9c840", aum = 0)
    /* renamed from: h */
    private void m20762h(CitizenImprovementType kt) {
        bJo().remove(kt);
    }

    @C0064Am(aul = "e345f71c23ecbb7fc035d8628fbfa435", aum = 0)
    private C3438ra<CitizenImprovementType> bRK() {
        return bJo();
    }

    @C3248pc(aYR = C3248pc.C3250b.ITERATOR, displayName = "Improvements")
    @C0064Am(aul = "bab5f31a441b81776b57ced1429f8745", aum = 0)
    private List<CitizenImprovementType> bRM() {
        return Collections.unmodifiableList(bJo());
    }

    @C3248pc(aYR = C3248pc.C3250b.ADDER, displayName = "Bomber Reward")
    @C0064Am(aul = "7309f73781c5ee5d0fc02f5e1dc4a794", aum = 0)
    /* renamed from: e */
    private void m20759e(CitizenshipReward lQVar) {
        bJp().add(lQVar);
    }

    @C3248pc(aYR = C3248pc.C3250b.REMOVER, displayName = "Bomber Reward")
    @C0064Am(aul = "aeeeb39099389a1cd1159481169a5040", aum = 0)
    /* renamed from: g */
    private void m20761g(CitizenshipReward lQVar) {
        bJp().remove(lQVar);
    }

    @C0064Am(aul = "cb058713b6f102ba0e2061d6b9434577", aum = 0)
    private C3438ra<CitizenshipReward> bRO() {
        return bJp();
    }

    @C3248pc(aYR = C3248pc.C3250b.ITERATOR, displayName = "Bomber Reward")
    @C0064Am(aul = "8c0dd6ee24e95d77e4e17455fedbd64a", aum = 0)
    private List<CitizenshipReward> bRQ() {
        return Collections.unmodifiableList(bJp());
    }

    @C3248pc(aYR = C3248pc.C3250b.ADDER, displayName = "Fighter Reward")
    @C0064Am(aul = "2b030efceea8794d124fceed471e688f", aum = 0)
    /* renamed from: i */
    private void m20763i(CitizenshipReward lQVar) {
        bJq().add(lQVar);
    }

    @C3248pc(aYR = C3248pc.C3250b.REMOVER, displayName = "Fighter Reward")
    @C0064Am(aul = "361fe9ae99c1d95ad322799e5c2aec30", aum = 0)
    /* renamed from: k */
    private void m20766k(CitizenshipReward lQVar) {
        bJq().remove(lQVar);
    }

    @C0064Am(aul = "aa1f9451b3ced9da46fe309f7322133c", aum = 0)
    private C3438ra<CitizenshipReward> bRS() {
        return bJq();
    }

    @C3248pc(aYR = C3248pc.C3250b.ITERATOR, displayName = "Fighter Reward")
    @C0064Am(aul = "c8dbbac9e2b81d161508e8ead56300f3", aum = 0)
    private List<CitizenshipReward> bRU() {
        return Collections.unmodifiableList(bJq());
    }

    @C3248pc(aYR = C3248pc.C3250b.ADDER, displayName = "Explorer Reward")
    @C0064Am(aul = "6366e1b2adcd4232dfe27ebfa02e7170", aum = 0)
    /* renamed from: m */
    private void m20767m(CitizenshipReward lQVar) {
        bJr().add(lQVar);
    }

    @C3248pc(aYR = C3248pc.C3250b.REMOVER, displayName = "Explorer Reward")
    @C0064Am(aul = "15049eee3742f739d53fe41cacdf3a6c", aum = 0)
    /* renamed from: o */
    private void m20768o(CitizenshipReward lQVar) {
        bJr().remove(lQVar);
    }

    @C0064Am(aul = "99d03b7c1215c9c3c3c28c9bad6baac3", aum = 0)
    private C3438ra<CitizenshipReward> bRW() {
        return bJr();
    }

    @C3248pc(aYR = C3248pc.C3250b.ITERATOR, displayName = "Explorer Reward")
    @C0064Am(aul = "553024800e903df204d8d5bab36ff46e", aum = 0)
    private List<CitizenshipReward> bRY() {
        return Collections.unmodifiableList(bJr());
    }

    @C3248pc(aYR = C3248pc.C3250b.ADDER, displayName = "Freighter Reward")
    @C0064Am(aul = "2f3388e4a7161139cac627418b01563b", aum = 0)
    /* renamed from: q */
    private void m20769q(CitizenshipReward lQVar) {
        bJs().add(lQVar);
    }

    @C3248pc(aYR = C3248pc.C3250b.REMOVER, displayName = "Freighter Reward")
    @C0064Am(aul = "ee2267bcd62f689f77174d3ebf869765", aum = 0)
    /* renamed from: s */
    private void m20773s(CitizenshipReward lQVar) {
        bJs().remove(lQVar);
    }

    @C0064Am(aul = "3ce4d322aa41c67a900bbcfd6803124d", aum = 0)
    private C3438ra<CitizenshipReward> bSa() {
        return bJs();
    }

    @C3248pc(aYR = C3248pc.C3250b.ITERATOR, displayName = "Freighter Reward")
    @C0064Am(aul = "3379d592803e0233602023e9312e81ec", aum = 0)
    private List<CitizenshipReward> bSc() {
        return Collections.unmodifiableList(bJs());
    }

    @C3248pc(aYR = C3248pc.C3250b.ADDER, displayName = "Default Reward")
    @C0064Am(aul = "cadf97dd2f3400d58c8ebf411aca2f0a", aum = 0)
    /* renamed from: u */
    private void m20777u(CitizenshipReward lQVar) {
        bJt().add(lQVar);
    }

    @C3248pc(aYR = C3248pc.C3250b.REMOVER, displayName = "Default Reward")
    @C0064Am(aul = "686d7966f8d92b0893f81cf7f01e28be", aum = 0)
    /* renamed from: w */
    private void m20782w(CitizenshipReward lQVar) {
        bJt().remove(lQVar);
    }

    @C0064Am(aul = "a66ac44796df2eb0fa904c679c9dd2e7", aum = 0)
    private C3438ra<CitizenshipReward> bSe() {
        return bJt();
    }

    @C3248pc(aYR = C3248pc.C3250b.ITERATOR, displayName = "Default Reward")
    @C0064Am(aul = "142345686b4802b102c289f7e8730fe2", aum = 0)
    private List<CitizenshipReward> bSg() {
        return Collections.unmodifiableList(bJt());
    }

    @C0064Am(aul = "f2d34351f8864c308fc888ad504b4f16", aum = 0)
    private Citizenship bSi() {
        return (Citizenship) mo745aU();
    }

    @C0064Am(aul = "0ac1f834fc2ca9d1319a78886a7643a6", aum = 0)
    /* renamed from: aT */
    private <T extends aDJ> T m20742aT() {
        T t = (Citizenship) bFf().mo6865M(Citizenship.class);
        t.mo6066l(this);
        return t;
    }
}
