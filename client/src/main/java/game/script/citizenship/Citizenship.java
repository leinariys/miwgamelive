package game.script.citizenship;

import game.network.message.externalizable.aCE;
import game.network.message.serializable.C3438ra;
import game.script.player.Player;
import game.script.resource.Asset;
import logic.baa.*;
import logic.data.mbean.C1743Zl;
import logic.data.mbean.aWw;
import logic.res.code.C5640aRc;
import logic.res.code.C5663aRz;
import logic.res.code.C6494any;
import logic.res.html.C2491fm;
import p001a.*;
import taikodom.infra.script.I18NString;

import java.util.Collection;
import java.util.Collections;
import java.util.List;

@C6485anp
@C2712iu(mo19786Bt = BaseCitizenshipType.class)
@C5511aMd
/* renamed from: a.VM */
/* compiled from: a */
public class Citizenship extends TaikodomObject implements C1616Xf {

    /* renamed from: MT */
    public static final C5663aRz f1850MT = null;

    /* renamed from: NY */
    public static final C5663aRz f1851NY = null;
    /* renamed from: QA */
    public static final C5663aRz f1853QA = null;
    /* renamed from: Qz */
    public static final C5663aRz f1854Qz = null;
    public static final C2491fm _f_dispose_0020_0028_0029V = null;
    public static final C2491fm _f_getType_0020_0028_0029Ljava_002flang_002fObject_003b = null;
    public static final int _m_fieldCount = 0;
    public static final C5663aRz[] _m_fields = null;
    public static final int _m_methodCount = 0;
    public static final C2491fm[] _m_methods = null;
    public static final C5663aRz avF = null;
    public static final C2491fm avO = null;
    public static final C5663aRz dWU = null;
    public static final C5663aRz eQZ = null;
    public static final C2491fm eRA = null;
    public static final C2491fm eRB = null;
    public static final C2491fm eRC = null;
    public static final C2491fm eRD = null;
    public static final C5663aRz eRb = null;
    public static final C5663aRz eRd = null;
    public static final C5663aRz eRg = null;
    public static final C5663aRz eRi = null;
    public static final C5663aRz eRk = null;
    public static final C5663aRz eRm = null;
    public static final C5663aRz eRo = null;
    public static final C5663aRz eRq = null;
    public static final C5663aRz eRs = null;
    public static final C5663aRz eRu = null;
    public static final C5663aRz eRw = null;
    public static final C2491fm eRx = null;
    public static final C2491fm eRy = null;
    public static final C2491fm eRz = null;
    public static final C5663aRz emT = null;
    /* renamed from: la */
    public static final C5663aRz f1857la = null;
    /* renamed from: li */
    public static final C2491fm f1858li = null;
    /* renamed from: lo */
    public static final C2491fm f1859lo = null;
    public static final long serialVersionUID = 0;
    public static C6494any ___iScriptClass;
    @C0064Am(aul = "30053ca7e7e2f6dc432be7787355a8d6", aum = 14)

    /* renamed from: Q */
    private static long f1852Q;
    @C0064Am(aul = "7e8b6e5259f4d661e059d97d779a801d", aum = 1)
    @Deprecated

    /* renamed from: db */
    private static long f1855db;
    @C0064Am(aul = "91c30a659c7b24fd98b1cf4cf7207179", aum = 5)
    private static Asset eQY;
    @C0064Am(aul = "5f19f19a56ad40c5a6ca6d835146f39f", aum = 6)
    private static Asset eRa;
    @C0064Am(aul = "d60ebff7f99bd1083534cbf93f34c21b", aum = 7)
    private static Asset eRc;
    @C0064Am(aul = "9c5e0db8fbfc5fcb335a7bf502a3f95b", aum = 8)
    private static C3438ra<CitizenImprovementType> eRe;
    @C0064Am(aul = "e6c6789ee9782a0b2d9527a780341bce", aum = 9)
    private static C3438ra<CitizenshipReward> eRf;
    @C0064Am(aul = "8cd230f3505ee24acac172520842a050", aum = 10)
    private static C3438ra<CitizenshipReward> eRh;
    @C0064Am(aul = "83c7c84e302090d9b106f2c607eff2a1", aum = 11)
    private static C3438ra<CitizenshipReward> eRj;
    @C0064Am(aul = "92e73b8b4689e41d243f4ab1a84d6cc8", aum = 12)
    private static C3438ra<CitizenshipReward> eRl;
    @C0064Am(aul = "9975f35dd73849108bedf6d5f8608f7d", aum = 13)
    private static C3438ra<CitizenshipReward> eRn;
    @C0064Am(aul = "393455cd0313fe80969667c9261a46f3", aum = 15)
    private static TaskletImpl eRp;
    @C0064Am(aul = "c2f848381c9ea50455af7627c2658918", aum = 16)
    private static C5611aPz eRr;
    @C0064Am(aul = "898a1d5efcee51cd116469e706818f88", aum = 17)
    private static C3438ra<CitizenImprovement> eRt;
    @C0064Am(aul = "8a15698326e01af47ae1cb642cea6613", aum = 19)
    private static long eRv;
    @C0064Am(aul = "86ac84cdb9d4db53934ad72f73db5152", aum = 2)
    @Deprecated
    private static float egP;
    @C0064Am(aul = "7c74f0e3f9d3dfa10360028d11d8b306", aum = 4)

    /* renamed from: jn */
    private static Asset f1856jn;
    @C0064Am(aul = "ee369b6a941d718c5122c1ee94c3dc8f", aum = 0)

    /* renamed from: nh */
    private static I18NString f1860nh;
    @C0064Am(aul = "5f80dd66c139c1151005f7480e3e2fc2", aum = 3)

    /* renamed from: ni */
    private static I18NString f1861ni;
    @C0064Am(aul = "3f6273761cd37e4a198f0d04be331b80", aum = 18)

    /* renamed from: nj */
    private static Player f1862nj;

    static {
        m10523V();
    }

    public Citizenship() {
        super((C5540aNg) null);
        super.mo10S();
    }

    public Citizenship(C5540aNg ang) {
        super(ang);
    }

    public Citizenship(CitizenshipType adl) {
        super((C5540aNg) null);
        super._m_script_init(adl);
    }

    /* renamed from: V */
    static void m10523V() {
        serialVersionUID = (long) 1;
        _m_fieldCount = TaikodomObject._m_fieldCount + 20;
        _m_methodCount = TaikodomObject._m_methodCount + 12;
        int i = TaikodomObject._m_fieldCount;
        C5663aRz[] arzArr = new C5663aRz[(i + 20)];
        C5663aRz b = C5640aRc.m17844b(Citizenship.class, "ee369b6a941d718c5122c1ee94c3dc8f", i);
        f1853QA = b;
        arzArr[i] = b;
        int i2 = i + 1;
        C5663aRz b2 = C5640aRc.m17844b(Citizenship.class, "7e8b6e5259f4d661e059d97d779a801d", i2);
        f1850MT = b2;
        arzArr[i2] = b2;
        int i3 = i2 + 1;
        C5663aRz b3 = C5640aRc.m17844b(Citizenship.class, "86ac84cdb9d4db53934ad72f73db5152", i3);
        emT = b3;
        arzArr[i3] = b3;
        int i4 = i3 + 1;
        C5663aRz b4 = C5640aRc.m17844b(Citizenship.class, "5f80dd66c139c1151005f7480e3e2fc2", i4);
        f1854Qz = b4;
        arzArr[i4] = b4;
        int i5 = i4 + 1;
        C5663aRz b5 = C5640aRc.m17844b(Citizenship.class, "7c74f0e3f9d3dfa10360028d11d8b306", i5);
        f1851NY = b5;
        arzArr[i5] = b5;
        int i6 = i5 + 1;
        C5663aRz b6 = C5640aRc.m17844b(Citizenship.class, "91c30a659c7b24fd98b1cf4cf7207179", i6);
        eQZ = b6;
        arzArr[i6] = b6;
        int i7 = i6 + 1;
        C5663aRz b7 = C5640aRc.m17844b(Citizenship.class, "5f19f19a56ad40c5a6ca6d835146f39f", i7);
        eRb = b7;
        arzArr[i7] = b7;
        int i8 = i7 + 1;
        C5663aRz b8 = C5640aRc.m17844b(Citizenship.class, "d60ebff7f99bd1083534cbf93f34c21b", i8);
        eRd = b8;
        arzArr[i8] = b8;
        int i9 = i8 + 1;
        C5663aRz b9 = C5640aRc.m17844b(Citizenship.class, "9c5e0db8fbfc5fcb335a7bf502a3f95b", i9);
        dWU = b9;
        arzArr[i9] = b9;
        int i10 = i9 + 1;
        C5663aRz b10 = C5640aRc.m17844b(Citizenship.class, "e6c6789ee9782a0b2d9527a780341bce", i10);
        eRg = b10;
        arzArr[i10] = b10;
        int i11 = i10 + 1;
        C5663aRz b11 = C5640aRc.m17844b(Citizenship.class, "8cd230f3505ee24acac172520842a050", i11);
        eRi = b11;
        arzArr[i11] = b11;
        int i12 = i11 + 1;
        C5663aRz b12 = C5640aRc.m17844b(Citizenship.class, "83c7c84e302090d9b106f2c607eff2a1", i12);
        eRk = b12;
        arzArr[i12] = b12;
        int i13 = i12 + 1;
        C5663aRz b13 = C5640aRc.m17844b(Citizenship.class, "92e73b8b4689e41d243f4ab1a84d6cc8", i13);
        eRm = b13;
        arzArr[i13] = b13;
        int i14 = i13 + 1;
        C5663aRz b14 = C5640aRc.m17844b(Citizenship.class, "9975f35dd73849108bedf6d5f8608f7d", i14);
        eRo = b14;
        arzArr[i14] = b14;
        int i15 = i14 + 1;
        C5663aRz b15 = C5640aRc.m17844b(Citizenship.class, "30053ca7e7e2f6dc432be7787355a8d6", i15);
        f1857la = b15;
        arzArr[i15] = b15;
        int i16 = i15 + 1;
        C5663aRz b16 = C5640aRc.m17844b(Citizenship.class, "393455cd0313fe80969667c9261a46f3", i16);
        eRq = b16;
        arzArr[i16] = b16;
        int i17 = i16 + 1;
        C5663aRz b17 = C5640aRc.m17844b(Citizenship.class, "c2f848381c9ea50455af7627c2658918", i17);
        eRs = b17;
        arzArr[i17] = b17;
        int i18 = i17 + 1;
        C5663aRz b18 = C5640aRc.m17844b(Citizenship.class, "898a1d5efcee51cd116469e706818f88", i18);
        eRu = b18;
        arzArr[i18] = b18;
        int i19 = i18 + 1;
        C5663aRz b19 = C5640aRc.m17844b(Citizenship.class, "3f6273761cd37e4a198f0d04be331b80", i19);
        avF = b19;
        arzArr[i19] = b19;
        int i20 = i19 + 1;
        C5663aRz b20 = C5640aRc.m17844b(Citizenship.class, "8a15698326e01af47ae1cb642cea6613", i20);
        eRw = b20;
        arzArr[i20] = b20;
        int i21 = i20 + 1;
        _m_fields = arzArr;
        C1634Xv.m11725a((Object[]) TaikodomObject._m_fields, (Object[]) _m_fields);
        int i22 = TaikodomObject._m_methodCount;
        C2491fm[] fmVarArr = new C2491fm[(i22 + 12)];
        C2491fm a = C4105zY.m41624a(Citizenship.class, "fedda814427d477d7becd8c56c151fbe", i22);
        eRx = a;
        fmVarArr[i22] = a;
        int i23 = i22 + 1;
        C2491fm a2 = C4105zY.m41624a(Citizenship.class, "05354292c4268a1eb8374e260bcdc1cf", i23);
        f1858li = a2;
        fmVarArr[i23] = a2;
        int i24 = i23 + 1;
        C2491fm a3 = C4105zY.m41624a(Citizenship.class, "04e2df4acff490b2712df35c6eca2582", i24);
        eRy = a3;
        fmVarArr[i24] = a3;
        int i25 = i24 + 1;
        C2491fm a4 = C4105zY.m41624a(Citizenship.class, "b6185852aaad88d8dcb99bbcdd27b90d", i25);
        avO = a4;
        fmVarArr[i25] = a4;
        int i26 = i25 + 1;
        C2491fm a5 = C4105zY.m41624a(Citizenship.class, "3bf4785ffc3cc66712e415963689b2a2", i26);
        f1859lo = a5;
        fmVarArr[i26] = a5;
        int i27 = i26 + 1;
        C2491fm a6 = C4105zY.m41624a(Citizenship.class, "6d338a92c5f0c0956e8a30c2a314ce37", i27);
        eRz = a6;
        fmVarArr[i27] = a6;
        int i28 = i27 + 1;
        C2491fm a7 = C4105zY.m41624a(Citizenship.class, "81c4aa13aeaefa7538c166aa2e92e263", i28);
        eRA = a7;
        fmVarArr[i28] = a7;
        int i29 = i28 + 1;
        C2491fm a8 = C4105zY.m41624a(Citizenship.class, "d525192fac959f222eef170b521994b3", i29);
        eRB = a8;
        fmVarArr[i29] = a8;
        int i30 = i29 + 1;
        C2491fm a9 = C4105zY.m41624a(Citizenship.class, "d8adb43733f73fe17e6234382647f27d", i30);
        _f_dispose_0020_0028_0029V = a9;
        fmVarArr[i30] = a9;
        int i31 = i30 + 1;
        C2491fm a10 = C4105zY.m41624a(Citizenship.class, "48a534f7c7d1ba8facc6882aa3d5ff0f", i31);
        eRC = a10;
        fmVarArr[i31] = a10;
        int i32 = i31 + 1;
        C2491fm a11 = C4105zY.m41624a(Citizenship.class, "926e7aeb51f6c5b08477cc0b91550e1e", i32);
        eRD = a11;
        fmVarArr[i32] = a11;
        int i33 = i32 + 1;
        C2491fm a12 = C4105zY.m41624a(Citizenship.class, "cbf48d27d1a309c1413829c2a5b386c7", i33);
        _f_getType_0020_0028_0029Ljava_002flang_002fObject_003b = a12;
        fmVarArr[i33] = a12;
        int i34 = i33 + 1;
        _m_methods = fmVarArr;
        C1634Xv.m11725a((Object[]) TaikodomObject._m_methods, (Object[]) _m_methods);
        ___iScriptClass = aUO.m18566a(Citizenship.class, aWw.class, _m_fields, _m_methods);
    }

    /* renamed from: G */
    private void m10521G(long j) {
        throw new C6039afL();
    }

    /* renamed from: KQ */
    private Player m10522KQ() {
        return (Player) bFf().mo5608dq().mo3214p(avF);
    }

    /* renamed from: a */
    private void m10524a(C5611aPz apz) {
        bFf().mo5608dq().mo3197f(eRs, apz);
    }

    /* renamed from: b */
    private void m10525b(TaskletImpl pw) {
        bFf().mo5608dq().mo3197f(eRq, pw);
    }

    /* renamed from: bA */
    private void m10527bA(C3438ra raVar) {
        throw new C6039afL();
    }

    /* renamed from: bB */
    private void m10528bB(C3438ra raVar) {
        throw new C6039afL();
    }

    /* renamed from: bC */
    private void m10529bC(C3438ra raVar) {
        throw new C6039afL();
    }

    /* renamed from: bD */
    private void m10530bD(C3438ra raVar) {
        throw new C6039afL();
    }

    /* renamed from: bE */
    private void m10531bE(C3438ra raVar) {
        bFf().mo5608dq().mo3197f(eRu, raVar);
    }

    private Asset bJl() {
        return ((CitizenshipType) getType()).bRF();
    }

    private Asset bJm() {
        return ((CitizenshipType) getType()).bRH();
    }

    private Asset bJn() {
        return ((CitizenshipType) getType()).bRJ();
    }

    private C3438ra bJo() {
        return ((CitizenshipType) getType()).bRL();
    }

    private C3438ra bJp() {
        return ((CitizenshipType) getType()).bRP();
    }

    private C3438ra bJq() {
        return ((CitizenshipType) getType()).bRT();
    }

    private C3438ra bJr() {
        return ((CitizenshipType) getType()).bRX();
    }

    private C3438ra bJs() {
        return ((CitizenshipType) getType()).bSb();
    }

    private C3438ra bJt() {
        return ((CitizenshipType) getType()).bSf();
    }

    private TaskletImpl bJu() {
        return (TaskletImpl) bFf().mo5608dq().mo3214p(eRq);
    }

    private C5611aPz bJv() {
        return (C5611aPz) bFf().mo5608dq().mo3214p(eRs);
    }

    private C3438ra bJw() {
        return (C3438ra) bFf().mo5608dq().mo3214p(eRu);
    }

    private long bJx() {
        return bFf().mo5608dq().mo3213o(eRw);
    }

    /* renamed from: bb */
    private void m10532bb(Asset tCVar) {
        throw new C6039afL();
    }

    /* renamed from: bc */
    private void m10533bc(Asset tCVar) {
        throw new C6039afL();
    }

    /* renamed from: bd */
    private void m10534bd(Asset tCVar) {
        throw new C6039afL();
    }

    /* renamed from: bq */
    private void m10535bq(I18NString i18NString) {
        throw new C6039afL();
    }

    /* renamed from: br */
    private void m10536br(I18NString i18NString) {
        throw new C6039afL();
    }

    private float bxp() {
        return ((CitizenshipType) getType()).bOT();
    }

    /* renamed from: by */
    private void m10537by(C3438ra raVar) {
        throw new C6039afL();
    }

    /* renamed from: bz */
    private void m10538bz(C3438ra raVar) {
        throw new C6039afL();
    }

    /* renamed from: eH */
    private long m10539eH() {
        return bFf().mo5608dq().mo3213o(f1857la);
    }

    /* renamed from: gj */
    private void m10543gj(long j) {
        bFf().mo5608dq().mo3184b(eRw, j);
    }

    @C0064Am(aul = "48a534f7c7d1ba8facc6882aa3d5ff0f", aum = 0)
    @C5566aOg
    /* renamed from: gm */
    private void m10545gm(long j) {
        throw new aWi(new aCE(this, eRC, new Object[]{new Long(j)}));
    }

    /* renamed from: ia */
    private void m10546ia(float f) {
        throw new C6039afL();
    }

    /* renamed from: m */
    private void m10547m(long j) {
        bFf().mo5608dq().mo3184b(f1857la, j);
    }

    @C0064Am(aul = "cbf48d27d1a309c1413829c2a5b386c7", aum = 0)
    /* renamed from: qW */
    private Object m10548qW() {
        return bJB();
    }

    /* renamed from: rU */
    private long m10549rU() {
        return ((CitizenshipType) getType()).mo12810rZ();
    }

    /* renamed from: sG */
    private Asset m10550sG() {
        return ((CitizenshipType) getType()).mo12811sK();
    }

    /* renamed from: t */
    private void m10551t(Asset tCVar) {
        throw new C6039afL();
    }

    /* renamed from: u */
    private void m10552u(Player aku) {
        bFf().mo5608dq().mo3197f(avF, aku);
    }

    /* renamed from: vS */
    private I18NString m10554vS() {
        return ((CitizenshipType) getType()).mo12809rP();
    }

    /* renamed from: vT */
    private I18NString m10555vT() {
        return ((CitizenshipType) getType()).mo12815vW();
    }

    /* renamed from: S */
    public void mo10S() {
        super.mo10S();
    }

    /* renamed from: T */
    public C6494any mo11T() {
        return ___iScriptClass;
    }

    /* renamed from: U */
    public C2491fm[] mo12U() {
        return _m_methods;
    }

    /* renamed from: W */
    public Object mo13W() {
        return new aWw(this);
    }

    /* renamed from: a */
    public Object mo14a(C0495Gr gr) {
        Object[] args = gr.getArgs();
        switch (gr.mo2417hq() - TaikodomObject._m_methodCount) {
            case 0:
                m10526b((C5611aPz) args[0]);
                return null;
            case 1:
                m10540eM();
                return null;
            case 2:
                return bJy();
            case 3:
                m10553v((Player) args[0]);
                return null;
            case 4:
                return new Long(m10541eW());
            case 5:
                return bJA();
            case 6:
                return new Long(bJC());
            case 7:
                m10544gk(((Long) args[0]).longValue());
                return null;
            case 8:
                m10542fg();
                return null;
            case 9:
                m10545gm(((Long) args[0]).longValue());
                return null;
            case 10:
                return new Long(bJD());
            case 11:
                return m10548qW();
            default:
                return super.mo14a(gr);
        }
    }

    /* renamed from: a */
    public void mo15a(Collection collection, C0495Gr gr) {
        super.mo15a(collection, gr);
    }

    public CitizenshipType bJB() {
        switch (bFf().mo6893i(eRz)) {
            case 0:
                return null;
            case 2:
                return (CitizenshipType) bFf().mo5606d(new aCE(this, eRz, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, eRz, new Object[0]));
                break;
        }
        return bJA();
    }

    /* access modifiers changed from: protected */
    public long bJE() {
        switch (bFf().mo6893i(eRD)) {
            case 0:
                return 0;
            case 2:
                return ((Long) bFf().mo5606d(new aCE(this, eRD, new Object[0]))).longValue();
            case 3:
                bFf().mo5606d(new aCE(this, eRD, new Object[0]));
                break;
        }
        return bJD();
    }

    public List<CitizenImprovement> bJz() {
        switch (bFf().mo6893i(eRy)) {
            case 0:
                return null;
            case 2:
                return (List) bFf().mo5606d(new aCE(this, eRy, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, eRy, new Object[0]));
                break;
        }
        return bJy();
    }

    /* renamed from: c */
    public void mo6060c(C5611aPz apz) {
        switch (bFf().mo6893i(eRx)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, eRx, new Object[]{apz}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, eRx, new Object[]{apz}));
                break;
        }
        m10526b(apz);
    }

    /* renamed from: c */
    public C5663aRz[] mo25c() {
        return _m_fields;
    }

    public void dispose() {
        switch (bFf().mo6893i(_f_dispose_0020_0028_0029V)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, _f_dispose_0020_0028_0029V, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, _f_dispose_0020_0028_0029V, new Object[0]));
                break;
        }
        m10542fg();
    }

    /* renamed from: eN */
    public void mo6061eN() {
        switch (bFf().mo6893i(f1858li)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f1858li, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f1858li, new Object[0]));
                break;
        }
        m10540eM();
    }

    public long getCreationTime() {
        switch (bFf().mo6893i(f1859lo)) {
            case 0:
                return 0;
            case 2:
                return ((Long) bFf().mo5606d(new aCE(this, f1859lo, new Object[0]))).longValue();
            case 3:
                bFf().mo5606d(new aCE(this, f1859lo, new Object[0]));
                break;
        }
        return m10541eW();
    }

    public long getExpiration() {
        switch (bFf().mo6893i(eRA)) {
            case 0:
                return 0;
            case 2:
                return ((Long) bFf().mo5606d(new aCE(this, eRA, new Object[0]))).longValue();
            case 3:
                bFf().mo5606d(new aCE(this, eRA, new Object[0]));
                break;
        }
        return bJC();
    }

    public /* bridge */ /* synthetic */ Object getType() {
        switch (bFf().mo6893i(_f_getType_0020_0028_0029Ljava_002flang_002fObject_003b)) {
            case 0:
                return null;
            case 2:
                return bFf().mo5606d(new aCE(this, _f_getType_0020_0028_0029Ljava_002flang_002fObject_003b, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, _f_getType_0020_0028_0029Ljava_002flang_002fObject_003b, new Object[0]));
                break;
        }
        return m10548qW();
    }

    /* renamed from: gl */
    public void mo6064gl(long j) {
        switch (bFf().mo6893i(eRB)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, eRB, new Object[]{new Long(j)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, eRB, new Object[]{new Long(j)}));
                break;
        }
        m10544gk(j);
    }

    @C5566aOg
    /* renamed from: gn */
    public void mo6065gn(long j) {
        switch (bFf().mo6893i(eRC)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, eRC, new Object[]{new Long(j)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, eRC, new Object[]{new Long(j)}));
                break;
        }
        m10545gm(j);
    }

    /* renamed from: w */
    public void mo6067w(Player aku) {
        switch (bFf().mo6893i(avO)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, avO, new Object[]{aku}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, avO, new Object[]{aku}));
                break;
        }
        m10553v(aku);
    }

    /* renamed from: l */
    public void mo6066l(CitizenshipType adl) {
        super.mo967a((C2961mJ) adl);
        m10547m(cVr());
        CitizenshipTimeoutTicker aVar = (CitizenshipTimeoutTicker) bFf().mo6865M(CitizenshipTimeoutTicker.class);
        aVar.mo6068a(this, (aDJ) this);
        m10525b((TaskletImpl) aVar);
        bJu().mo4704hk((float) (bJx() / 1000));
    }

    @C0064Am(aul = "fedda814427d477d7becd8c56c151fbe", aum = 0)
    /* renamed from: b */
    private void m10526b(C5611aPz apz) {
        m10524a(apz);
    }

    @C0064Am(aul = "05354292c4268a1eb8374e260bcdc1cf", aum = 0)
    /* renamed from: eM */
    private void m10540eM() {
        bJv().mo4976b(this);
    }

    @C0064Am(aul = "04e2df4acff490b2712df35c6eca2582", aum = 0)
    private List<CitizenImprovement> bJy() {
        if (bJw().size() < bJo().size()) {
            for (CitizenImprovementType NK : bJo()) {
                CitizenImprovement cZVar = (CitizenImprovement) NK.mo7459NK();
                cZVar.mo17624w(m10522KQ());
                bJw().add(cZVar);
            }
        }
        return Collections.unmodifiableList(bJw());
    }

    @C0064Am(aul = "b6185852aaad88d8dcb99bbcdd27b90d", aum = 0)
    /* renamed from: v */
    private void m10553v(Player aku) {
        m10552u(aku);
    }

    @C0064Am(aul = "3bf4785ffc3cc66712e415963689b2a2", aum = 0)
    /* renamed from: eW */
    private long m10541eW() {
        return m10539eH();
    }

    @C0064Am(aul = "6d338a92c5f0c0956e8a30c2a314ce37", aum = 0)
    private CitizenshipType bJA() {
        return (CitizenshipType) super.getType();
    }

    @C0064Am(aul = "81c4aa13aeaefa7538c166aa2e92e263", aum = 0)
    private long bJC() {
        return (m10539eH() + bJx()) - cVr();
    }

    @C0064Am(aul = "d525192fac959f222eef170b521994b3", aum = 0)
    /* renamed from: gk */
    private void m10544gk(long j) {
        m10543gj((bJx() - (cVr() - m10539eH())) + j);
        m10547m(cVr());
        bJu().mo4704hk((float) (bJx() / 1000));
    }

    @C0064Am(aul = "d8adb43733f73fe17e6234382647f27d", aum = 0)
    /* renamed from: fg */
    private void m10542fg() {
        super.dispose();
        if (bJu() != null) {
            bJu().stop();
            bJu().dispose();
        }
        m10525b((TaskletImpl) null);
        m10524a((C5611aPz) null);
        if (bJw().size() > 0) {
            for (CitizenImprovement dispose : bJw()) {
                dispose.dispose();
            }
        }
        m10552u((Player) null);
    }

    @C0064Am(aul = "926e7aeb51f6c5b08477cc0b91550e1e", aum = 0)
    private long bJD() {
        return bJx();
    }

    @C6485anp
    @C5511aMd
            /* renamed from: a.VM$a */
    class CitizenshipTimeoutTicker extends TaskletImpl implements C1616Xf {

        /* renamed from: JD */
        public static final C2491fm f1863JD = null;
        public static final int _m_fieldCount = 0;
        public static final C5663aRz[] _m_fields = null;
        public static final int _m_methodCount = 0;
        public static final C2491fm[] _m_methods = null;
        /* renamed from: aT */
        public static final C5663aRz f1864aT = null;
        public static final long serialVersionUID = 0;
        public static C6494any ___iScriptClass;
        @C0064Am(aul = "e442ece1cb6f02235baa0b37ee434d55", aum = 0)
        static /* synthetic */ Citizenship evG;

        static {
            m10569V();
        }

        public CitizenshipTimeoutTicker() {
            super((C5540aNg) null);
            super.mo10S();
        }

        public CitizenshipTimeoutTicker(Citizenship vm, aDJ adj) {
            super((C5540aNg) null);
            super._m_script_init(vm, adj);
        }

        public CitizenshipTimeoutTicker(C5540aNg ang) {
            super(ang);
        }

        /* renamed from: V */
        static void m10569V() {
            serialVersionUID = (long) 1;
            _m_fieldCount = TaskletImpl._m_fieldCount + 1;
            _m_methodCount = TaskletImpl._m_methodCount + 1;
            int i = TaskletImpl._m_fieldCount;
            C5663aRz[] arzArr = new C5663aRz[(i + 1)];
            C5663aRz b = C5640aRc.m17844b(CitizenshipTimeoutTicker.class, "e442ece1cb6f02235baa0b37ee434d55", i);
            f1864aT = b;
            arzArr[i] = b;
            int i2 = i + 1;
            _m_fields = arzArr;
            C1634Xv.m11725a((Object[]) TaskletImpl._m_fields, (Object[]) _m_fields);
            int i3 = TaskletImpl._m_methodCount;
            C2491fm[] fmVarArr = new C2491fm[(i3 + 1)];
            C2491fm a = C4105zY.m41624a(CitizenshipTimeoutTicker.class, "e795d1307b1d37b762ab9a788929cc99", i3);
            f1863JD = a;
            fmVarArr[i3] = a;
            int i4 = i3 + 1;
            _m_methods = fmVarArr;
            C1634Xv.m11725a((Object[]) TaskletImpl._m_methods, (Object[]) _m_methods);
            ___iScriptClass = aUO.m18566a(CitizenshipTimeoutTicker.class, C1743Zl.class, _m_fields, _m_methods);
        }

        private Citizenship bCO() {
            return (Citizenship) bFf().mo5608dq().mo3214p(f1864aT);
        }

        /* renamed from: c */
        private void m10570c(Citizenship vm) {
            bFf().mo5608dq().mo3197f(f1864aT, vm);
        }

        /* renamed from: S */
        public void mo10S() {
            super.mo10S();
        }

        /* renamed from: T */
        public C6494any mo11T() {
            return ___iScriptClass;
        }

        /* renamed from: U */
        public C2491fm[] mo12U() {
            return _m_methods;
        }

        /* renamed from: W */
        public Object mo13W() {
            return new C1743Zl(this);
        }

        /* renamed from: a */
        public Object mo14a(C0495Gr gr) {
            gr.getArgs();
            switch (gr.mo2417hq() - TaskletImpl._m_methodCount) {
                case 0:
                    m10571pi();
                    return null;
                default:
                    return super.mo14a(gr);
            }
        }

        /* renamed from: a */
        public void mo15a(Collection collection, C0495Gr gr) {
            super.mo15a(collection, gr);
        }

        /* renamed from: c */
        public C5663aRz[] mo25c() {
            return _m_fields;
        }

        /* access modifiers changed from: protected */
        /* renamed from: pj */
        public void mo667pj() {
            switch (bFf().mo6893i(f1863JD)) {
                case 0:
                    return;
                case 2:
                    bFf().mo5606d(new aCE(this, f1863JD, new Object[0]));
                    return;
                case 3:
                    bFf().mo5606d(new aCE(this, f1863JD, new Object[0]));
                    break;
            }
            m10571pi();
        }

        /* renamed from: a */
        public void mo6068a(Citizenship vm, aDJ adj) {
            m10570c(vm);
            super.mo4706l(adj);
        }

        @C0064Am(aul = "e795d1307b1d37b762ab9a788929cc99", aum = 0)
        /* renamed from: pi */
        private void m10571pi() {
            bCO().mo6061eN();
            stop();
        }
    }
}
