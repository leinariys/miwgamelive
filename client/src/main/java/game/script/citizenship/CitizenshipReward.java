package game.script.citizenship;

import game.network.message.externalizable.aCE;
import game.script.player.Player;
import logic.baa.*;
import logic.data.mbean.C1473Vg;
import logic.res.code.C5640aRc;
import logic.res.code.C5663aRz;
import logic.res.code.C6494any;
import logic.res.html.C2491fm;
import p001a.*;

import java.util.Collection;
import java.util.UUID;

@C5511aMd
@C6485anp
/* renamed from: a.lQ */
/* compiled from: a */
public abstract class CitizenshipReward extends TaikodomObject implements C0468GU, C1616Xf {
    public static final int _m_fieldCount = 0;
    public static final C5663aRz[] _m_fields = null;
    public static final int _m_methodCount = 0;
    public static final C2491fm[] _m_methods = null;
    public static final C2491fm aAB = null;
    /* renamed from: bL */
    public static final C5663aRz f8524bL = null;
    /* renamed from: bM */
    public static final C5663aRz f8525bM = null;
    /* renamed from: bN */
    public static final C2491fm f8526bN = null;
    /* renamed from: bO */
    public static final C2491fm f8527bO = null;
    /* renamed from: bP */
    public static final C2491fm f8528bP = null;
    /* renamed from: bQ */
    public static final C2491fm f8529bQ = null;
    /* renamed from: cA */
    public static final C2491fm f8530cA = null;
    /* renamed from: cB */
    public static final C2491fm f8531cB = null;
    /* renamed from: cz */
    public static final C5663aRz f8533cz = null;
    public static final long serialVersionUID = 0;
    public static C6494any ___iScriptClass;
    @C0064Am(aul = "a302b22f4869ad4ff0aed964012abff7", aum = 2)

    /* renamed from: bK */
    private static UUID f8523bK;
    @C0064Am(aul = "4f7485f169a36d6751e641e9bd2e28ed", aum = 0)

    /* renamed from: cy */
    private static int f8532cy;
    @C0064Am(aul = "41d549f9be4139a3a0b92ed1f191eba7", aum = 1)
    private static String handle;

    static {
        m34847V();
    }

    public CitizenshipReward() {
        super((C5540aNg) null);
        super.mo10S();
    }

    public CitizenshipReward(C5540aNg ang) {
        super(ang);
    }

    /* renamed from: V */
    static void m34847V() {
        serialVersionUID = (long) 1;
        _m_fieldCount = TaikodomObject._m_fieldCount + 3;
        _m_methodCount = TaikodomObject._m_methodCount + 7;
        int i = TaikodomObject._m_fieldCount;
        C5663aRz[] arzArr = new C5663aRz[(i + 3)];
        C5663aRz b = C5640aRc.m17844b(CitizenshipReward.class, "4f7485f169a36d6751e641e9bd2e28ed", i);
        f8533cz = b;
        arzArr[i] = b;
        int i2 = i + 1;
        C5663aRz b2 = C5640aRc.m17844b(CitizenshipReward.class, "41d549f9be4139a3a0b92ed1f191eba7", i2);
        f8525bM = b2;
        arzArr[i2] = b2;
        int i3 = i2 + 1;
        C5663aRz b3 = C5640aRc.m17844b(CitizenshipReward.class, "a302b22f4869ad4ff0aed964012abff7", i3);
        f8524bL = b3;
        arzArr[i3] = b3;
        int i4 = i3 + 1;
        _m_fields = arzArr;
        C1634Xv.m11725a((Object[]) TaikodomObject._m_fields, (Object[]) _m_fields);
        int i5 = TaikodomObject._m_methodCount;
        C2491fm[] fmVarArr = new C2491fm[(i5 + 7)];
        C2491fm a = C4105zY.m41624a(CitizenshipReward.class, "08b531904a4ad005914916c6be815125", i5);
        f8526bN = a;
        fmVarArr[i5] = a;
        int i6 = i5 + 1;
        C2491fm a2 = C4105zY.m41624a(CitizenshipReward.class, "b5ac98c1cebf47fddfe4c41fbcdc28f1", i6);
        f8527bO = a2;
        fmVarArr[i6] = a2;
        int i7 = i6 + 1;
        C2491fm a3 = C4105zY.m41624a(CitizenshipReward.class, "305b3c2c25d645171ab15b2723612e7e", i7);
        f8530cA = a3;
        fmVarArr[i7] = a3;
        int i8 = i7 + 1;
        C2491fm a4 = C4105zY.m41624a(CitizenshipReward.class, "df2342add33a619168d1772758bf728f", i8);
        f8531cB = a4;
        fmVarArr[i8] = a4;
        int i9 = i8 + 1;
        C2491fm a5 = C4105zY.m41624a(CitizenshipReward.class, "c304f893f4f43c177a4ef62f633efa2a", i9);
        f8528bP = a5;
        fmVarArr[i9] = a5;
        int i10 = i9 + 1;
        C2491fm a6 = C4105zY.m41624a(CitizenshipReward.class, "1f82f5c4981a04af0003a259144b391c", i10);
        f8529bQ = a6;
        fmVarArr[i10] = a6;
        int i11 = i10 + 1;
        C2491fm a7 = C4105zY.m41624a(CitizenshipReward.class, "9e7fa7c3ff34879ffe17045d26060a54", i11);
        aAB = a7;
        fmVarArr[i11] = a7;
        int i12 = i11 + 1;
        _m_methods = fmVarArr;
        C1634Xv.m11725a((Object[]) TaikodomObject._m_methods, (Object[]) _m_methods);
        ___iScriptClass = aUO.m18566a(CitizenshipReward.class, C1473Vg.class, _m_fields, _m_methods);
    }

    /* renamed from: a */
    private void m34848a(String str) {
        bFf().mo5608dq().mo3197f(f8525bM, str);
    }

    /* renamed from: a */
    private void m34849a(UUID uuid) {
        bFf().mo5608dq().mo3197f(f8524bL, uuid);
    }

    /* renamed from: an */
    private UUID m34850an() {
        return (UUID) bFf().mo5608dq().mo3214p(f8524bL);
    }

    /* renamed from: ao */
    private String m34851ao() {
        return (String) bFf().mo5608dq().mo3214p(f8525bM);
    }

    /* renamed from: aw */
    private int m34854aw() {
        return bFf().mo5608dq().mo3212n(f8533cz);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Handle")
    @C0064Am(aul = "1f82f5c4981a04af0003a259144b391c", aum = 0)
    @C5566aOg
    /* renamed from: b */
    private void m34856b(String str) {
        throw new aWi(new aCE(this, f8529bQ, new Object[]{str}));
    }

    /* renamed from: c */
    private void m34858c(UUID uuid) {
        switch (bFf().mo6893i(f8527bO)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f8527bO, new Object[]{uuid}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f8527bO, new Object[]{uuid}));
                break;
        }
        m34857b(uuid);
    }

    /* renamed from: f */
    private void m34859f(int i) {
        bFf().mo5608dq().mo3183b(f8533cz, i);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Number of Citizenships to reward")
    @C0064Am(aul = "df2342add33a619168d1772758bf728f", aum = 0)
    @C5566aOg
    /* renamed from: g */
    private void m34860g(int i) {
        throw new aWi(new aCE(this, f8531cB, new Object[]{new Integer(i)}));
    }

    @C0064Am(aul = "9e7fa7c3ff34879ffe17045d26060a54", aum = 0)
    /* renamed from: x */
    private void m34861x(Player aku) {
        throw new aWi(new aCE(this, aAB, new Object[]{aku}));
    }

    /* renamed from: T */
    public C6494any mo11T() {
        return ___iScriptClass;
    }

    /* renamed from: U */
    public C2491fm[] mo12U() {
        return _m_methods;
    }

    /* renamed from: W */
    public Object mo13W() {
        return new C1473Vg(this);
    }

    /* renamed from: a */
    public Object mo14a(C0495Gr gr) {
        Object[] args = gr.getArgs();
        switch (gr.mo2417hq() - TaikodomObject._m_methodCount) {
            case 0:
                return m34852ap();
            case 1:
                m34857b((UUID) args[0]);
                return null;
            case 2:
                return new Integer(m34855ax());
            case 3:
                m34860g(((Integer) args[0]).intValue());
                return null;
            case 4:
                return m34853ar();
            case 5:
                m34856b((String) args[0]);
                return null;
            case 6:
                m34861x((Player) args[0]);
                return null;
            default:
                return super.mo14a(gr);
        }
    }

    /* renamed from: a */
    public void mo15a(Collection collection, C0495Gr gr) {
        super.mo15a(collection, gr);
    }

    /* renamed from: aq */
    public UUID mo18aq() {
        switch (bFf().mo6893i(f8526bN)) {
            case 0:
                return null;
            case 2:
                return (UUID) bFf().mo5606d(new aCE(this, f8526bN, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f8526bN, new Object[0]));
                break;
        }
        return m34852ap();
    }

    /* renamed from: c */
    public C5663aRz[] mo25c() {
        return _m_fields;
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Number of Citizenships to reward")
    public int getAmount() {
        switch (bFf().mo6893i(f8530cA)) {
            case 0:
                return 0;
            case 2:
                return ((Integer) bFf().mo5606d(new aCE(this, f8530cA, new Object[0]))).intValue();
            case 3:
                bFf().mo5606d(new aCE(this, f8530cA, new Object[0]));
                break;
        }
        return m34855ax();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Number of Citizenships to reward")
    @C5566aOg
    public void setAmount(int i) {
        switch (bFf().mo6893i(f8531cB)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f8531cB, new Object[]{new Integer(i)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f8531cB, new Object[]{new Integer(i)}));
                break;
        }
        m34860g(i);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Handle")
    public String getHandle() {
        switch (bFf().mo6893i(f8528bP)) {
            case 0:
                return null;
            case 2:
                return (String) bFf().mo5606d(new aCE(this, f8528bP, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f8528bP, new Object[0]));
                break;
        }
        return m34853ar();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Handle")
    @C5566aOg
    public void setHandle(String str) {
        switch (bFf().mo6893i(f8529bQ)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f8529bQ, new Object[]{str}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f8529bQ, new Object[]{str}));
                break;
        }
        m34856b(str);
    }

    /* renamed from: y */
    public void mo5852y(Player aku) {
        switch (bFf().mo6893i(aAB)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, aAB, new Object[]{aku}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, aAB, new Object[]{aku}));
                break;
        }
        m34861x(aku);
    }

    @C0064Am(aul = "08b531904a4ad005914916c6be815125", aum = 0)
    /* renamed from: ap */
    private UUID m34852ap() {
        return m34850an();
    }

    @C0064Am(aul = "b5ac98c1cebf47fddfe4c41fbcdc28f1", aum = 0)
    /* renamed from: b */
    private void m34857b(UUID uuid) {
        m34849a(uuid);
    }

    /* renamed from: S */
    public void mo10S() {
        super.mo10S();
        m34849a(UUID.randomUUID());
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Number of Citizenships to reward")
    @C0064Am(aul = "305b3c2c25d645171ab15b2723612e7e", aum = 0)
    /* renamed from: ax */
    private int m34855ax() {
        return m34854aw();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Handle")
    @C0064Am(aul = "c304f893f4f43c177a4ef62f633efa2a", aum = 0)
    /* renamed from: ar */
    private String m34853ar() {
        return m34851ao();
    }
}
