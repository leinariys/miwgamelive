package game.script.citizenship;

import game.network.message.externalizable.aCE;
import logic.baa.*;
import logic.data.mbean.C1563Wv;
import logic.res.code.C5640aRc;
import logic.res.code.C5663aRz;
import logic.res.code.C6494any;
import logic.res.html.C2491fm;
import p001a.*;

import java.util.Collection;
import java.util.UUID;

@C5511aMd
@C6485anp
/* renamed from: a.acu  reason: case insensitive filesystem */
/* compiled from: a */
public class CitizenshipPack extends TaikodomObject implements C0468GU, C1616Xf {

    /* renamed from: MT */
    public static final C5663aRz f4284MT = null;

    /* renamed from: MY */
    public static final C2491fm f4285MY = null;

    /* renamed from: MZ */
    public static final C2491fm f4286MZ = null;
    public static final int _m_fieldCount = 0;
    public static final C5663aRz[] _m_fields = null;
    public static final int _m_methodCount = 0;
    public static final C2491fm[] _m_methods = null;
    /* renamed from: bL */
    public static final C5663aRz f4288bL = null;
    /* renamed from: bM */
    public static final C5663aRz f4289bM = null;
    /* renamed from: bN */
    public static final C2491fm f4290bN = null;
    /* renamed from: bO */
    public static final C2491fm f4291bO = null;
    /* renamed from: bP */
    public static final C2491fm f4292bP = null;
    /* renamed from: bQ */
    public static final C2491fm f4293bQ = null;
    public static final C5663aRz cKt = null;
    public static final C5663aRz emT = null;
    public static final C2491fm fbO = null;
    public static final C2491fm fbP = null;
    public static final C2491fm fbQ = null;
    public static final C2491fm fbR = null;
    public static final long serialVersionUID = 0;
    public static C6494any ___iScriptClass;
    @C0064Am(aul = "5499f1ecc1e4ffeaa4c2cc087be9b20e", aum = 1)

    /* renamed from: bK */
    private static UUID f4287bK;
    @C0064Am(aul = "5ccae1a6fe0a2a711d1b85864b89c6e2", aum = 3)

    /* renamed from: db */
    private static long f4294db;
    @C0064Am(aul = "cedc00414bdca7e2946dcad830dc42f2", aum = 4)
    private static float egP;
    @C0064Am(aul = "610a2db94a538f56c61326af5fea22e7", aum = 2)
    private static CitizenshipType exB;
    @C0064Am(aul = "545649474d607ed4d9d5a69f844c66bf", aum = 0)
    private static String handle;

    static {
        m20652V();
    }

    public CitizenshipPack() {
        super((C5540aNg) null);
        super.mo10S();
    }

    public CitizenshipPack(C5540aNg ang) {
        super(ang);
    }

    /* renamed from: V */
    static void m20652V() {
        serialVersionUID = (long) 1;
        _m_fieldCount = TaikodomObject._m_fieldCount + 5;
        _m_methodCount = TaikodomObject._m_methodCount + 10;
        int i = TaikodomObject._m_fieldCount;
        C5663aRz[] arzArr = new C5663aRz[(i + 5)];
        C5663aRz b = C5640aRc.m17844b(CitizenshipPack.class, "545649474d607ed4d9d5a69f844c66bf", i);
        f4289bM = b;
        arzArr[i] = b;
        int i2 = i + 1;
        C5663aRz b2 = C5640aRc.m17844b(CitizenshipPack.class, "5499f1ecc1e4ffeaa4c2cc087be9b20e", i2);
        f4288bL = b2;
        arzArr[i2] = b2;
        int i3 = i2 + 1;
        C5663aRz b3 = C5640aRc.m17844b(CitizenshipPack.class, "610a2db94a538f56c61326af5fea22e7", i3);
        cKt = b3;
        arzArr[i3] = b3;
        int i4 = i3 + 1;
        C5663aRz b4 = C5640aRc.m17844b(CitizenshipPack.class, "5ccae1a6fe0a2a711d1b85864b89c6e2", i4);
        f4284MT = b4;
        arzArr[i4] = b4;
        int i5 = i4 + 1;
        C5663aRz b5 = C5640aRc.m17844b(CitizenshipPack.class, "cedc00414bdca7e2946dcad830dc42f2", i5);
        emT = b5;
        arzArr[i5] = b5;
        int i6 = i5 + 1;
        _m_fields = arzArr;
        C1634Xv.m11725a((Object[]) TaikodomObject._m_fields, (Object[]) _m_fields);
        int i7 = TaikodomObject._m_methodCount;
        C2491fm[] fmVarArr = new C2491fm[(i7 + 10)];
        C2491fm a = C4105zY.m41624a(CitizenshipPack.class, "a6d0bb99f5c88af6277e5714901e6a24", i7);
        f4285MY = a;
        fmVarArr[i7] = a;
        int i8 = i7 + 1;
        C2491fm a2 = C4105zY.m41624a(CitizenshipPack.class, "fea6068caca819e687af989e9bf0ea60", i8);
        f4286MZ = a2;
        fmVarArr[i8] = a2;
        int i9 = i8 + 1;
        C2491fm a3 = C4105zY.m41624a(CitizenshipPack.class, "39cdae9e843559ef55a900b99c86f4d1", i9);
        fbO = a3;
        fmVarArr[i9] = a3;
        int i10 = i9 + 1;
        C2491fm a4 = C4105zY.m41624a(CitizenshipPack.class, "7003b6456a561d40e5ab5addc05c01ab", i10);
        fbP = a4;
        fmVarArr[i10] = a4;
        int i11 = i10 + 1;
        C2491fm a5 = C4105zY.m41624a(CitizenshipPack.class, "e86f740640b6b1d329716bb71111c742", i11);
        f4290bN = a5;
        fmVarArr[i11] = a5;
        int i12 = i11 + 1;
        C2491fm a6 = C4105zY.m41624a(CitizenshipPack.class, "d600c6138e2e5dba8931582666d28ebd", i12);
        f4291bO = a6;
        fmVarArr[i12] = a6;
        int i13 = i12 + 1;
        C2491fm a7 = C4105zY.m41624a(CitizenshipPack.class, "3646d726f857ec8b754f55dfd1690e39", i13);
        f4292bP = a7;
        fmVarArr[i13] = a7;
        int i14 = i13 + 1;
        C2491fm a8 = C4105zY.m41624a(CitizenshipPack.class, "46735029ea1a5f3fb651fdac9997e556", i14);
        f4293bQ = a8;
        fmVarArr[i14] = a8;
        int i15 = i14 + 1;
        C2491fm a9 = C4105zY.m41624a(CitizenshipPack.class, "c24e85e1415b627a0dfdce9c9e034168", i15);
        fbQ = a9;
        fmVarArr[i15] = a9;
        int i16 = i15 + 1;
        C2491fm a10 = C4105zY.m41624a(CitizenshipPack.class, "b2f628ff8f1b0bf2ca37249531d2f61c", i16);
        fbR = a10;
        fmVarArr[i16] = a10;
        int i17 = i16 + 1;
        _m_methods = fmVarArr;
        C1634Xv.m11725a((Object[]) TaikodomObject._m_methods, (Object[]) _m_methods);
        ___iScriptClass = aUO.m18566a(CitizenshipPack.class, C1563Wv.class, _m_fields, _m_methods);
    }

    /* renamed from: G */
    private void m20650G(long j) {
        bFf().mo5608dq().mo3184b(f4284MT, j);
    }

    /* renamed from: a */
    private void m20653a(String str) {
        bFf().mo5608dq().mo3197f(f4289bM, str);
    }

    /* renamed from: a */
    private void m20654a(UUID uuid) {
        bFf().mo5608dq().mo3197f(f4288bL, uuid);
    }

    /* renamed from: an */
    private UUID m20655an() {
        return (UUID) bFf().mo5608dq().mo3214p(f4288bL);
    }

    /* renamed from: ao */
    private String m20656ao() {
        return (String) bFf().mo5608dq().mo3214p(f4289bM);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Handle")
    @C0064Am(aul = "46735029ea1a5f3fb651fdac9997e556", aum = 0)
    @C5566aOg
    /* renamed from: b */
    private void m20659b(String str) {
        throw new aWi(new aCE(this, f4293bQ, new Object[]{str}));
    }

    private CitizenshipType bOR() {
        return (CitizenshipType) bFf().mo5608dq().mo3214p(cKt);
    }

    private float bxp() {
        return bFf().mo5608dq().mo3211m(emT);
    }

    /* renamed from: c */
    private void m20661c(UUID uuid) {
        switch (bFf().mo6893i(f4291bO)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f4291bO, new Object[]{uuid}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f4291bO, new Object[]{uuid}));
                break;
        }
        m20660b(uuid);
    }

    /* renamed from: ia */
    private void m20663ia(float f) {
        bFf().mo5608dq().mo3150a(emT, f);
    }

    /* renamed from: m */
    private void m20664m(CitizenshipType adl) {
        bFf().mo5608dq().mo3197f(cKt, adl);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Citizenship Type")
    @C0064Am(aul = "b2f628ff8f1b0bf2ca37249531d2f61c", aum = 0)
    @C5566aOg
    /* renamed from: n */
    private void m20665n(CitizenshipType adl) {
        throw new aWi(new aCE(this, fbR, new Object[]{adl}));
    }

    /* renamed from: rU */
    private long m20666rU() {
        return bFf().mo5608dq().mo3213o(f4284MT);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Price")
    /* renamed from: K */
    public void mo12733K(long j) {
        switch (bFf().mo6893i(f4286MZ)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f4286MZ, new Object[]{new Long(j)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f4286MZ, new Object[]{new Long(j)}));
                break;
        }
        m20651J(j);
    }

    /* renamed from: T */
    public C6494any mo11T() {
        return ___iScriptClass;
    }

    /* renamed from: U */
    public C2491fm[] mo12U() {
        return _m_methods;
    }

    /* renamed from: W */
    public Object mo13W() {
        return new C1563Wv(this);
    }

    /* renamed from: a */
    public Object mo14a(C0495Gr gr) {
        Object[] args = gr.getArgs();
        switch (gr.mo2417hq() - TaikodomObject._m_methodCount) {
            case 0:
                return new Long(m20667rY());
            case 1:
                m20651J(((Long) args[0]).longValue());
                return null;
            case 2:
                return new Float(bOS());
            case 3:
                m20662iV(((Float) args[0]).floatValue());
                return null;
            case 4:
                return m20657ap();
            case 5:
                m20660b((UUID) args[0]);
                return null;
            case 6:
                return m20658ar();
            case 7:
                m20659b((String) args[0]);
                return null;
            case 8:
                return bOU();
            case 9:
                m20665n((CitizenshipType) args[0]);
                return null;
            default:
                return super.mo14a(gr);
        }
    }

    /* renamed from: a */
    public void mo15a(Collection collection, C0495Gr gr) {
        super.mo15a(collection, gr);
    }

    /* renamed from: aq */
    public UUID mo18aq() {
        switch (bFf().mo6893i(f4290bN)) {
            case 0:
                return null;
            case 2:
                return (UUID) bFf().mo5606d(new aCE(this, f4290bN, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f4290bN, new Object[0]));
                break;
        }
        return m20657ap();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Days")
    public float bOT() {
        switch (bFf().mo6893i(fbO)) {
            case 0:
                return 0.0f;
            case 2:
                return ((Float) bFf().mo5606d(new aCE(this, fbO, new Object[0]))).floatValue();
            case 3:
                bFf().mo5606d(new aCE(this, fbO, new Object[0]));
                break;
        }
        return bOS();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Citizenship Type")
    public CitizenshipType bOV() {
        switch (bFf().mo6893i(fbQ)) {
            case 0:
                return null;
            case 2:
                return (CitizenshipType) bFf().mo5606d(new aCE(this, fbQ, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, fbQ, new Object[0]));
                break;
        }
        return bOU();
    }

    /* renamed from: c */
    public C5663aRz[] mo25c() {
        return _m_fields;
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Handle")
    public String getHandle() {
        switch (bFf().mo6893i(f4292bP)) {
            case 0:
                return null;
            case 2:
                return (String) bFf().mo5606d(new aCE(this, f4292bP, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f4292bP, new Object[0]));
                break;
        }
        return m20658ar();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Handle")
    @C5566aOg
    public void setHandle(String str) {
        switch (bFf().mo6893i(f4293bQ)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f4293bQ, new Object[]{str}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f4293bQ, new Object[]{str}));
                break;
        }
        m20659b(str);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Days")
    /* renamed from: iW */
    public void mo12736iW(float f) {
        switch (bFf().mo6893i(fbP)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, fbP, new Object[]{new Float(f)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, fbP, new Object[]{new Float(f)}));
                break;
        }
        m20662iV(f);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Citizenship Type")
    @C5566aOg
    /* renamed from: o */
    public void mo12737o(CitizenshipType adl) {
        switch (bFf().mo6893i(fbR)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, fbR, new Object[]{adl}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, fbR, new Object[]{adl}));
                break;
        }
        m20665n(adl);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Price")
    /* renamed from: rZ */
    public long mo12738rZ() {
        switch (bFf().mo6893i(f4285MY)) {
            case 0:
                return 0;
            case 2:
                return ((Long) bFf().mo5606d(new aCE(this, f4285MY, new Object[0]))).longValue();
            case 3:
                bFf().mo5606d(new aCE(this, f4285MY, new Object[0]));
                break;
        }
        return m20667rY();
    }

    /* renamed from: S */
    public void mo10S() {
        super.mo10S();
        m20654a(UUID.randomUUID());
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Price")
    @C0064Am(aul = "a6d0bb99f5c88af6277e5714901e6a24", aum = 0)
    /* renamed from: rY */
    private long m20667rY() {
        return m20666rU();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Price")
    @C0064Am(aul = "fea6068caca819e687af989e9bf0ea60", aum = 0)
    /* renamed from: J */
    private void m20651J(long j) {
        m20650G(j);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Days")
    @C0064Am(aul = "39cdae9e843559ef55a900b99c86f4d1", aum = 0)
    private float bOS() {
        return bxp();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Days")
    @C0064Am(aul = "7003b6456a561d40e5ab5addc05c01ab", aum = 0)
    /* renamed from: iV */
    private void m20662iV(float f) {
        m20663ia(f);
    }

    @C0064Am(aul = "e86f740640b6b1d329716bb71111c742", aum = 0)
    /* renamed from: ap */
    private UUID m20657ap() {
        return m20655an();
    }

    @C0064Am(aul = "d600c6138e2e5dba8931582666d28ebd", aum = 0)
    /* renamed from: b */
    private void m20660b(UUID uuid) {
        m20654a(uuid);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Handle")
    @C0064Am(aul = "3646d726f857ec8b754f55dfd1690e39", aum = 0)
    /* renamed from: ar */
    private String m20658ar() {
        return m20656ao();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Citizenship Type")
    @C0064Am(aul = "c24e85e1415b627a0dfdce9c9e034168", aum = 0)
    private CitizenshipType bOU() {
        return bOR();
    }
}
