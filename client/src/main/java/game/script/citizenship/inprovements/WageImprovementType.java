package game.script.citizenship.inprovements;

import game.network.message.externalizable.aCE;
import game.script.citizenship.CitizenImprovementType;
import logic.baa.*;
import logic.data.mbean.C5314aEo;
import logic.res.code.C5640aRc;
import logic.res.code.C5663aRz;
import logic.res.code.C6494any;
import logic.res.html.C2491fm;
import p001a.*;

import java.util.Collection;

@C0566Hp
@C5511aMd
@C6485anp
/* renamed from: a.aGp  reason: case insensitive filesystem */
/* compiled from: a */
public class WageImprovementType extends CitizenImprovementType implements C1616Xf {
    public static final int _m_fieldCount = 0;
    public static final C5663aRz[] _m_fields = null;
    public static final int _m_methodCount = 0;
    public static final C2491fm[] _m_methods = null;
    /* renamed from: dN */
    public static final C2491fm f2888dN = null;
    public static final C5663aRz emS = null;
    public static final C5663aRz emT = null;
    public static final C2491fm emX = null;
    public static final C2491fm fbO = null;
    public static final C2491fm fbP = null;
    public static final C2491fm hPt = null;
    public static final C2491fm hPu = null;
    public static final long serialVersionUID = 0;
    public static C6494any ___iScriptClass;
    @C0064Am(aul = "acea1258a4daf5908bdc95688ce47b0c", aum = 0)
    private static long egO;
    @C0064Am(aul = "e52e48b259a15311c2fa08d0c476d126", aum = 1)
    private static float egP;

    static {
        m14991V();
    }

    public WageImprovementType() {
        super((C5540aNg) null);
        super.mo10S();
    }

    public WageImprovementType(C5540aNg ang) {
        super(ang);
    }

    /* renamed from: V */
    static void m14991V() {
        serialVersionUID = (long) 1;
        _m_fieldCount = CitizenImprovementType._m_fieldCount + 2;
        _m_methodCount = CitizenImprovementType._m_methodCount + 6;
        int i = CitizenImprovementType._m_fieldCount;
        C5663aRz[] arzArr = new C5663aRz[(i + 2)];
        C5663aRz b = C5640aRc.m17844b(WageImprovementType.class, "acea1258a4daf5908bdc95688ce47b0c", i);
        emS = b;
        arzArr[i] = b;
        int i2 = i + 1;
        C5663aRz b2 = C5640aRc.m17844b(WageImprovementType.class, "e52e48b259a15311c2fa08d0c476d126", i2);
        emT = b2;
        arzArr[i2] = b2;
        int i3 = i2 + 1;
        _m_fields = arzArr;
        C1634Xv.m11725a((Object[]) CitizenImprovementType._m_fields, (Object[]) _m_fields);
        int i4 = CitizenImprovementType._m_methodCount;
        C2491fm[] fmVarArr = new C2491fm[(i4 + 6)];
        C2491fm a = C4105zY.m41624a(WageImprovementType.class, "47a78fcfecfa42bb07c078b6ee6ca14f", i4);
        emX = a;
        fmVarArr[i4] = a;
        int i5 = i4 + 1;
        C2491fm a2 = C4105zY.m41624a(WageImprovementType.class, "796cfd0058ea108087106d7edc7ca3c5", i5);
        hPt = a2;
        fmVarArr[i5] = a2;
        int i6 = i5 + 1;
        C2491fm a3 = C4105zY.m41624a(WageImprovementType.class, "fb9ffd8f663e7b15449f8380d3805000", i6);
        fbO = a3;
        fmVarArr[i6] = a3;
        int i7 = i6 + 1;
        C2491fm a4 = C4105zY.m41624a(WageImprovementType.class, "37963584b7dbcfafd8d51c2a546a1a0d", i7);
        fbP = a4;
        fmVarArr[i7] = a4;
        int i8 = i7 + 1;
        C2491fm a5 = C4105zY.m41624a(WageImprovementType.class, "6ce9ad89796b2bc2c1fd577df476d753", i8);
        hPu = a5;
        fmVarArr[i8] = a5;
        int i9 = i8 + 1;
        C2491fm a6 = C4105zY.m41624a(WageImprovementType.class, "031ccf8b9621ab503a149afbd55261d2", i9);
        f2888dN = a6;
        fmVarArr[i9] = a6;
        int i10 = i9 + 1;
        _m_methods = fmVarArr;
        C1634Xv.m11725a((Object[]) CitizenImprovementType._m_methods, (Object[]) _m_methods);
        ___iScriptClass = aUO.m18566a(WageImprovementType.class, C5314aEo.class, _m_fields, _m_methods);
    }

    private long bxo() {
        return bFf().mo5608dq().mo3213o(emS);
    }

    private float bxp() {
        return bFf().mo5608dq().mo3211m(emT);
    }

    /* renamed from: fJ */
    private void m14993fJ(long j) {
        bFf().mo5608dq().mo3184b(emS, j);
    }

    /* renamed from: ia */
    private void m14995ia(float f) {
        bFf().mo5608dq().mo3150a(emT, f);
    }

    /* renamed from: T */
    public C6494any mo11T() {
        return ___iScriptClass;
    }

    /* renamed from: U */
    public C2491fm[] mo12U() {
        return _m_methods;
    }

    /* renamed from: W */
    public Object mo13W() {
        return new C5314aEo(this);
    }

    /* renamed from: a */
    public Object mo14a(C0495Gr gr) {
        Object[] args = gr.getArgs();
        switch (gr.mo2417hq() - CitizenImprovementType._m_methodCount) {
            case 0:
                return new Long(bxu());
            case 1:
                m14996kN(((Long) args[0]).longValue());
                return null;
            case 2:
                return new Float(bOS());
            case 3:
                m14994iV(((Float) args[0]).floatValue());
                return null;
            case 4:
                return dae();
            case 5:
                return m14992aT();
            default:
                return super.mo14a(gr);
        }
    }

    /* renamed from: a */
    public void mo15a(Collection collection, C0495Gr gr) {
        super.mo15a(collection, gr);
    }

    /* access modifiers changed from: protected */
    /* renamed from: aU */
    public <T extends aDJ> T mo745aU() {
        switch (bFf().mo6893i(f2888dN)) {
            case 0:
                return null;
            case 2:
                return (aDJ) bFf().mo5606d(new aCE(this, f2888dN, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f2888dN, new Object[0]));
                break;
        }
        return m14992aT();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Days")
    public float bOT() {
        switch (bFf().mo6893i(fbO)) {
            case 0:
                return 0.0f;
            case 2:
                return ((Float) bFf().mo5606d(new aCE(this, fbO, new Object[0]))).floatValue();
            case 3:
                bFf().mo5606d(new aCE(this, fbO, new Object[0]));
                break;
        }
        return bOS();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Wage")
    public long bxv() {
        switch (bFf().mo6893i(emX)) {
            case 0:
                return 0;
            case 2:
                return ((Long) bFf().mo5606d(new aCE(this, emX, new Object[0]))).longValue();
            case 3:
                bFf().mo5606d(new aCE(this, emX, new Object[0]));
                break;
        }
        return bxu();
    }

    /* renamed from: c */
    public C5663aRz[] mo25c() {
        return _m_fields;
    }

    public WageImprovement daf() {
        switch (bFf().mo6893i(hPu)) {
            case 0:
                return null;
            case 2:
                return (WageImprovement) bFf().mo5606d(new aCE(this, hPu, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, hPu, new Object[0]));
                break;
        }
        return dae();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Days")
    /* renamed from: iW */
    public void mo9121iW(float f) {
        switch (bFf().mo6893i(fbP)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, fbP, new Object[]{new Float(f)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, fbP, new Object[]{new Float(f)}));
                break;
        }
        m14994iV(f);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Wage")
    /* renamed from: kO */
    public void mo9122kO(long j) {
        switch (bFf().mo6893i(hPt)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, hPt, new Object[]{new Long(j)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, hPt, new Object[]{new Long(j)}));
                break;
        }
        m14996kN(j);
    }

    /* renamed from: S */
    public void mo10S() {
        super.mo10S();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Wage")
    @C0064Am(aul = "47a78fcfecfa42bb07c078b6ee6ca14f", aum = 0)
    private long bxu() {
        return bxo();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Wage")
    @C0064Am(aul = "796cfd0058ea108087106d7edc7ca3c5", aum = 0)
    /* renamed from: kN */
    private void m14996kN(long j) {
        m14993fJ(j);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Days")
    @C0064Am(aul = "fb9ffd8f663e7b15449f8380d3805000", aum = 0)
    private float bOS() {
        return bxp();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Days")
    @C0064Am(aul = "37963584b7dbcfafd8d51c2a546a1a0d", aum = 0)
    /* renamed from: iV */
    private void m14994iV(float f) {
        m14995ia(f);
    }

    @C0064Am(aul = "6ce9ad89796b2bc2c1fd577df476d753", aum = 0)
    private WageImprovement dae() {
        return (WageImprovement) mo745aU();
    }

    @C0064Am(aul = "031ccf8b9621ab503a149afbd55261d2", aum = 0)
    /* renamed from: aT */
    private <T extends aDJ> T m14992aT() {
        T t = (WageImprovement) bFf().mo6865M(WageImprovement.class);
        t.mo5897a(this);
        return t;
    }
}
