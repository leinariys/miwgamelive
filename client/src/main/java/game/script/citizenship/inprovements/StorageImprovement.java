package game.script.citizenship.inprovements;

import game.network.message.externalizable.aCE;
import game.network.message.serializable.C1556Wo;
import game.script.citizenship.CitizenImprovement;
import game.script.citizenship.CitizenImprovementType;
import game.script.storage.Storage;
import game.script.storage.StorageAdapter;
import game.script.util.GameObjectAdapter;
import logic.baa.C1616Xf;
import logic.baa.C1634Xv;
import logic.baa.C5511aMd;
import logic.baa.C6485anp;
import logic.data.mbean.C6603aqD;
import logic.data.mbean.aMT;
import logic.res.code.C5640aRc;
import logic.res.code.C5663aRz;
import logic.res.code.C6494any;
import logic.res.html.C2491fm;
import p001a.*;

import java.util.Collection;

@C6485anp
@C2712iu
@C5511aMd
/* renamed from: a.yx */
/* compiled from: a */
public class StorageImprovement extends CitizenImprovement implements C1616Xf {
    /* renamed from: Pv */
    public static final C5663aRz f9617Pv = null;
    /* renamed from: Pw */
    public static final C2491fm f9618Pw = null;
    public static final int _m_fieldCount = 0;
    public static final C5663aRz[] _m_fields = null;
    public static final int _m_methodCount = 0;
    public static final C2491fm[] _m_methods = null;
    public static final C5663aRz bLm = null;
    public static final C2491fm bLn = null;
    public static final C2491fm bLo = null;
    public static final C2491fm bLp = null;
    public static final C2491fm bLq = null;
    public static final C2491fm bLr = null;
    public static final C2491fm bLs = null;
    public static final long serialVersionUID = 0;
    public static C6494any ___iScriptClass;
    @C0064Am(aul = "65b6269dfa5ab950658701bf55d5dedb", aum = 0)

    /* renamed from: DL */
    private static C3892wO f9616DL;
    @C0064Am(aul = "ffe062dbe5c495564985cf8e8b7dee1e", aum = 1)
    private static C1556Wo<Storage, StorageAmplifierAdapter> bLl;

    static {
        m41431V();
    }

    public StorageImprovement() {
        super((C5540aNg) null);
        super.mo10S();
    }

    public StorageImprovement(StorageImprovementType ni) {
        super((C5540aNg) null);
        super._m_script_init(ni);
    }

    public StorageImprovement(C5540aNg ang) {
        super(ang);
    }

    /* renamed from: V */
    static void m41431V() {
        serialVersionUID = (long) 1;
        _m_fieldCount = CitizenImprovement._m_fieldCount + 2;
        _m_methodCount = CitizenImprovement._m_methodCount + 7;
        int i = CitizenImprovement._m_fieldCount;
        C5663aRz[] arzArr = new C5663aRz[(i + 2)];
        C5663aRz b = C5640aRc.m17844b(StorageImprovement.class, "65b6269dfa5ab950658701bf55d5dedb", i);
        f9617Pv = b;
        arzArr[i] = b;
        int i2 = i + 1;
        C5663aRz b2 = C5640aRc.m17844b(StorageImprovement.class, "ffe062dbe5c495564985cf8e8b7dee1e", i2);
        bLm = b2;
        arzArr[i2] = b2;
        int i3 = i2 + 1;
        _m_fields = arzArr;
        C1634Xv.m11725a((Object[]) CitizenImprovement._m_fields, (Object[]) _m_fields);
        int i4 = CitizenImprovement._m_methodCount;
        C2491fm[] fmVarArr = new C2491fm[(i4 + 7)];
        C2491fm a = C4105zY.m41624a(StorageImprovement.class, "bb2ff541ed75312a344b732fd4b7da19", i4);
        bLn = a;
        fmVarArr[i4] = a;
        int i5 = i4 + 1;
        C2491fm a2 = C4105zY.m41624a(StorageImprovement.class, "efbeebde337cbd907732fa165abc7217", i5);
        bLo = a2;
        fmVarArr[i5] = a2;
        int i6 = i5 + 1;
        C2491fm a3 = C4105zY.m41624a(StorageImprovement.class, "9c0c63d7cb34ab4b7d7cdb6ed37ef4ee", i6);
        bLp = a3;
        fmVarArr[i6] = a3;
        int i7 = i6 + 1;
        C2491fm a4 = C4105zY.m41624a(StorageImprovement.class, "ad568d82eb84134abdc07dead6483950", i7);
        bLq = a4;
        fmVarArr[i7] = a4;
        int i8 = i7 + 1;
        C2491fm a5 = C4105zY.m41624a(StorageImprovement.class, "72011268337b7361590e5bc9a5b547e3", i8);
        f9618Pw = a5;
        fmVarArr[i8] = a5;
        int i9 = i8 + 1;
        C2491fm a6 = C4105zY.m41624a(StorageImprovement.class, "ac85f35800cc8892f12eba45b13f8369", i9);
        bLr = a6;
        fmVarArr[i9] = a6;
        int i10 = i9 + 1;
        C2491fm a7 = C4105zY.m41624a(StorageImprovement.class, "9da1b40c9a04f2035aea3138641b22e5", i10);
        bLs = a7;
        fmVarArr[i10] = a7;
        int i11 = i10 + 1;
        _m_methods = fmVarArr;
        C1634Xv.m11725a((Object[]) CitizenImprovement._m_methods, (Object[]) _m_methods);
        ___iScriptClass = aUO.m18566a(StorageImprovement.class, C6603aqD.class, _m_fields, _m_methods);
    }

    /* renamed from: a */
    private void m41433a(C3892wO wOVar) {
        throw new C6039afL();
    }

    /* access modifiers changed from: private */
    public C1556Wo apM() {
        return (C1556Wo) bFf().mo5608dq().mo3214p(bLm);
    }

    /* renamed from: v */
    private void m41436v(C1556Wo wo) {
        bFf().mo5608dq().mo3197f(bLm, wo);
    }

    /* renamed from: vi */
    private C3892wO m41437vi() {
        return ((StorageImprovementType) getType()).mo4262vk();
    }

    /* renamed from: S */
    public void mo10S() {
        super.mo10S();
    }

    /* renamed from: T */
    public C6494any mo11T() {
        return ___iScriptClass;
    }

    /* renamed from: U */
    public C2491fm[] mo12U() {
        return _m_methods;
    }

    /* renamed from: W */
    public Object mo13W() {
        return new C6603aqD(this);
    }

    /* renamed from: a */
    public Object mo14a(C0495Gr gr) {
        Object[] args = gr.getArgs();
        switch (gr.mo2417hq() - CitizenImprovement._m_methodCount) {
            case 0:
                return apN();
            case 1:
                return new Boolean(m41434a((CitizenImprovement) args[0]));
            case 2:
                apP();
                return null;
            case 3:
                apQ();
                return null;
            case 4:
                return m41438vj();
            case 5:
                m41435g((Storage) args[0]);
                return null;
            case 6:
                return apS();
            default:
                return super.mo14a(gr);
        }
    }

    /* renamed from: a */
    public void mo15a(Collection collection, C0495Gr gr) {
        super.mo15a(collection, gr);
    }

    /* access modifiers changed from: protected */
    public C3892wO apO() {
        switch (bFf().mo6893i(bLn)) {
            case 0:
                return null;
            case 2:
                return (C3892wO) bFf().mo5606d(new aCE(this, bLn, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, bLn, new Object[0]));
                break;
        }
        return apN();
    }

    public void apR() {
        switch (bFf().mo6893i(bLq)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, bLq, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, bLq, new Object[0]));
                break;
        }
        apQ();
    }

    public CitizenImprovement.C2184a apT() {
        switch (bFf().mo6893i(bLs)) {
            case 0:
                return null;
            case 2:
                return (CitizenImprovement.C2184a) bFf().mo5606d(new aCE(this, bLs, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, bLs, new Object[0]));
                break;
        }
        return apS();
    }

    /* renamed from: b */
    public boolean mo550b(CitizenImprovement cZVar) {
        switch (bFf().mo6893i(bLo)) {
            case 0:
                return false;
            case 2:
                return ((Boolean) bFf().mo5606d(new aCE(this, bLo, new Object[]{cZVar}))).booleanValue();
            case 3:
                bFf().mo5606d(new aCE(this, bLo, new Object[]{cZVar}));
                break;
        }
        return m41434a(cZVar);
    }

    /* renamed from: c */
    public C5663aRz[] mo25c() {
        return _m_fields;
    }

    public void cancel() {
        switch (bFf().mo6893i(bLp)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, bLp, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, bLp, new Object[0]));
                break;
        }
        apP();
    }

    /* renamed from: h */
    public void mo23181h(Storage qzVar) {
        switch (bFf().mo6893i(bLr)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, bLr, new Object[]{qzVar}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, bLr, new Object[]{qzVar}));
                break;
        }
        m41435g(qzVar);
    }

    /* renamed from: vk */
    public C3892wO mo23182vk() {
        switch (bFf().mo6893i(f9618Pw)) {
            case 0:
                return null;
            case 2:
                return (C3892wO) bFf().mo5606d(new aCE(this, f9618Pw, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f9618Pw, new Object[0]));
                break;
        }
        return m41438vj();
    }

    /* renamed from: a */
    public void mo23179a(StorageImprovementType ni) {
        super.mo547a((CitizenImprovementType) ni);
    }

    @C0064Am(aul = "bb2ff541ed75312a344b732fd4b7da19", aum = 0)
    private C3892wO apN() {
        return m41437vi();
    }

    @C0064Am(aul = "efbeebde337cbd907732fa165abc7217", aum = 0)
    /* renamed from: a */
    private boolean m41434a(CitizenImprovement cZVar) {
        if (!(cZVar instanceof StorageImprovement) || m41437vi().getValue() <= ((StorageImprovement) cZVar).mo23182vk().getValue()) {
            return false;
        }
        return true;
    }

    @C0064Am(aul = "9c0c63d7cb34ab4b7d7cdb6ed37ef4ee", aum = 0)
    private void apP() {
        for (Storage qzVar : apM().keySet()) {
            qzVar.mo21538TJ().mo23262g((GameObjectAdapter) apM().get(qzVar));
        }
        apM().clear();
    }

    @C0064Am(aul = "ad568d82eb84134abdc07dead6483950", aum = 0)
    private void apQ() {
        for (Storage h : mo17622KY().dxg()) {
            mo23181h(h);
        }
    }

    @C0064Am(aul = "72011268337b7361590e5bc9a5b547e3", aum = 0)
    /* renamed from: vj */
    private C3892wO m41438vj() {
        return m41437vi();
    }

    @C0064Am(aul = "ac85f35800cc8892f12eba45b13f8369", aum = 0)
    /* renamed from: g */
    private void m41435g(Storage qzVar) {
        StorageAmplifierAdapter aVar = (StorageAmplifierAdapter) bFf().mo6865M(StorageAmplifierAdapter.class);
        aVar.mo23183c(this);
        apM().put(qzVar, aVar);
        qzVar.mo21538TJ().mo23261e(aVar);
    }

    @C0064Am(aul = "9da1b40c9a04f2035aea3138641b22e5", aum = 0)
    private CitizenImprovement.C2184a apS() {
        return CitizenImprovement.C2184a.STORAGE;
    }

    @C6485anp
    @C5511aMd
    /* renamed from: a.yx$a */
    public class StorageAmplifierAdapter extends StorageAdapter implements C1616Xf {
        public static final int _m_fieldCount = 0;
        public static final C5663aRz[] _m_fields = null;
        public static final int _m_methodCount = 0;
        public static final C2491fm[] _m_methods = null;
        /* renamed from: aT */
        public static final C5663aRz f9619aT = null;
        public static final C2491fm aVS = null;
        public static final long serialVersionUID = 0;
        public static C6494any ___iScriptClass;
        @C0064Am(aul = "195e1228a9e57b83c5d6a7396a81a172", aum = 0)
        static /* synthetic */ StorageImprovement eXc;

        static {
            m41450V();
        }

        public StorageAmplifierAdapter() {
            super((C5540aNg) null);
            super.mo10S();
        }

        public StorageAmplifierAdapter(C5540aNg ang) {
            super(ang);
        }

        public StorageAmplifierAdapter(StorageImprovement yxVar) {
            super((C5540aNg) null);
            super._m_script_init(yxVar);
        }

        /* renamed from: V */
        static void m41450V() {
            serialVersionUID = (long) 1;
            _m_fieldCount = StorageAdapter._m_fieldCount + 1;
            _m_methodCount = StorageAdapter._m_methodCount + 1;
            int i = StorageAdapter._m_fieldCount;
            C5663aRz[] arzArr = new C5663aRz[(i + 1)];
            C5663aRz b = C5640aRc.m17844b(StorageAmplifierAdapter.class, "195e1228a9e57b83c5d6a7396a81a172", i);
            f9619aT = b;
            arzArr[i] = b;
            int i2 = i + 1;
            _m_fields = arzArr;
            C1634Xv.m11725a((Object[]) StorageAdapter._m_fields, (Object[]) _m_fields);
            int i3 = StorageAdapter._m_methodCount;
            C2491fm[] fmVarArr = new C2491fm[(i3 + 1)];
            C2491fm a = C4105zY.m41624a(StorageAmplifierAdapter.class, "f45b33cc795dc5171066d784532e9011", i3);
            aVS = a;
            fmVarArr[i3] = a;
            int i4 = i3 + 1;
            _m_methods = fmVarArr;
            C1634Xv.m11725a((Object[]) StorageAdapter._m_methods, (Object[]) _m_methods);
            ___iScriptClass = aUO.m18566a(StorageAmplifierAdapter.class, aMT.class, _m_fields, _m_methods);
        }

        /* renamed from: b */
        private void m41451b(StorageImprovement yxVar) {
            bFf().mo5608dq().mo3197f(f9619aT, yxVar);
        }

        private StorageImprovement bMN() {
            return (StorageImprovement) bFf().mo5608dq().mo3214p(f9619aT);
        }

        /* renamed from: S */
        public void mo10S() {
            super.mo10S();
        }

        /* renamed from: T */
        public C6494any mo11T() {
            return ___iScriptClass;
        }

        /* renamed from: U */
        public C2491fm[] mo12U() {
            return _m_methods;
        }

        /* renamed from: W */
        public Object mo13W() {
            return new aMT(this);
        }

        /* renamed from: a */
        public Object mo14a(C0495Gr gr) {
            Object[] args = gr.getArgs();
            switch (gr.mo2417hq() - StorageAdapter._m_methodCount) {
                case 0:
                    return new Float(m41452c((Storage) args[0]));
                default:
                    return super.mo14a(gr);
            }
        }

        /* renamed from: a */
        public void mo15a(Collection collection, C0495Gr gr) {
            super.mo15a(collection, gr);
        }

        /* renamed from: c */
        public C5663aRz[] mo25c() {
            return _m_fields;
        }

        /* renamed from: d */
        public float mo7432d(Storage qzVar) {
            switch (bFf().mo6893i(aVS)) {
                case 0:
                    return 0.0f;
                case 2:
                    return ((Float) bFf().mo5606d(new aCE(this, aVS, new Object[]{qzVar}))).floatValue();
                case 3:
                    bFf().mo5606d(new aCE(this, aVS, new Object[]{qzVar}));
                    break;
            }
            return m41452c(qzVar);
        }

        /* renamed from: c */
        public void mo23183c(StorageImprovement yxVar) {
            m41451b(yxVar);
            super.mo10S();
        }

        @C0064Am(aul = "f45b33cc795dc5171066d784532e9011", aum = 0)
        /* renamed from: c */
        private float m41452c(Storage qzVar) {
            return bMN().apO().mo22753v(((StorageAdapter) ((StorageAmplifierAdapter) bMN().apM().get(qzVar)).mo16070IE()).mo7432d(qzVar), super.mo7432d(qzVar));
        }
    }
}
