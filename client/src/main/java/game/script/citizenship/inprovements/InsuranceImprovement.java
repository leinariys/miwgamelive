package game.script.citizenship.inprovements;

import game.network.message.externalizable.aCE;
import game.script.citizenship.CitizenImprovement;
import game.script.citizenship.CitizenImprovementType;
import logic.baa.C1616Xf;
import logic.baa.C1634Xv;
import logic.baa.C5511aMd;
import logic.baa.C6485anp;
import logic.data.mbean.C5353aGb;
import logic.res.code.C5640aRc;
import logic.res.code.C5663aRz;
import logic.res.code.C6494any;
import logic.res.html.C2491fm;
import p001a.*;
import taikodom.infra.script.I18NString;

import java.util.Collection;

@C2712iu
@C5511aMd
@C6485anp
/* renamed from: a.aVm  reason: case insensitive filesystem */
/* compiled from: a */
public class InsuranceImprovement extends CitizenImprovement implements C1616Xf {
    public static final int _m_fieldCount = 0;
    public static final C5663aRz[] _m_fields = null;
    public static final int _m_methodCount = 0;
    public static final C2491fm[] _m_methods = null;
    public static final C2491fm bLo = null;
    public static final C2491fm bLp = null;
    public static final C2491fm bLq = null;
    public static final C2491fm bLs = null;
    public static final C5663aRz ciB = null;
    public static final C5663aRz feR = null;
    public static final C2491fm feS = null;
    public static final C2491fm feU = null;
    public static final long serialVersionUID = 0;
    public static C6494any ___iScriptClass;
    @C0064Am(aul = "900e30743775c0e9d46af6a8c57f93a8", aum = 1)
    private static I18NString cRI;
    @C0064Am(aul = "a7c62d512c6912397cbd2fd191c3558c", aum = 0)
    private static long ciA;

    static {
        m19078V();
    }

    public InsuranceImprovement() {
        super((C5540aNg) null);
        super.mo10S();
    }

    public InsuranceImprovement(C5540aNg ang) {
        super(ang);
    }

    public InsuranceImprovement(InsuranceImprovementType acj) {
        super((C5540aNg) null);
        super._m_script_init(acj);
    }

    /* renamed from: V */
    static void m19078V() {
        serialVersionUID = (long) 1;
        _m_fieldCount = CitizenImprovement._m_fieldCount + 2;
        _m_methodCount = CitizenImprovement._m_methodCount + 6;
        int i = CitizenImprovement._m_fieldCount;
        C5663aRz[] arzArr = new C5663aRz[(i + 2)];
        C5663aRz b = C5640aRc.m17844b(InsuranceImprovement.class, "a7c62d512c6912397cbd2fd191c3558c", i);
        ciB = b;
        arzArr[i] = b;
        int i2 = i + 1;
        C5663aRz b2 = C5640aRc.m17844b(InsuranceImprovement.class, "900e30743775c0e9d46af6a8c57f93a8", i2);
        feR = b2;
        arzArr[i2] = b2;
        int i3 = i2 + 1;
        _m_fields = arzArr;
        C1634Xv.m11725a((Object[]) CitizenImprovement._m_fields, (Object[]) _m_fields);
        int i4 = CitizenImprovement._m_methodCount;
        C2491fm[] fmVarArr = new C2491fm[(i4 + 6)];
        C2491fm a = C4105zY.m41624a(InsuranceImprovement.class, "37d9f096358fad99f4fb746452765288", i4);
        bLo = a;
        fmVarArr[i4] = a;
        int i5 = i4 + 1;
        C2491fm a2 = C4105zY.m41624a(InsuranceImprovement.class, "dad5066a0710e65005c48bef8b6b25f6", i5);
        bLq = a2;
        fmVarArr[i5] = a2;
        int i6 = i5 + 1;
        C2491fm a3 = C4105zY.m41624a(InsuranceImprovement.class, "108263409a49adf541d670ff76a12422", i6);
        feS = a3;
        fmVarArr[i6] = a3;
        int i7 = i6 + 1;
        C2491fm a4 = C4105zY.m41624a(InsuranceImprovement.class, "c26b5cdf94d0f94a1c1177972a8c4951", i7);
        bLp = a4;
        fmVarArr[i7] = a4;
        int i8 = i7 + 1;
        C2491fm a5 = C4105zY.m41624a(InsuranceImprovement.class, "1320c76f941c8ead3c7fa622b0c37861", i8);
        feU = a5;
        fmVarArr[i8] = a5;
        int i9 = i8 + 1;
        C2491fm a6 = C4105zY.m41624a(InsuranceImprovement.class, "ea54830340ee2aae2c6445d68c2ed481", i9);
        bLs = a6;
        fmVarArr[i9] = a6;
        int i10 = i9 + 1;
        _m_methods = fmVarArr;
        C1634Xv.m11725a((Object[]) CitizenImprovement._m_methods, (Object[]) _m_methods);
        ___iScriptClass = aUO.m18566a(InsuranceImprovement.class, C5353aGb.class, _m_fields, _m_methods);
    }

    private long awo() {
        return ((InsuranceImprovementType) getType()).bPR();
    }

    private I18NString bPP() {
        return ((InsuranceImprovementType) getType()).bPT();
    }

    /* renamed from: dF */
    private void m19080dF(long j) {
        throw new C6039afL();
    }

    /* renamed from: lW */
    private void m19081lW(I18NString i18NString) {
        throw new C6039afL();
    }

    /* renamed from: S */
    public void mo10S() {
        super.mo10S();
    }

    /* renamed from: T */
    public C6494any mo11T() {
        return ___iScriptClass;
    }

    /* renamed from: U */
    public C2491fm[] mo12U() {
        return _m_methods;
    }

    /* renamed from: W */
    public Object mo13W() {
        return new C5353aGb(this);
    }

    /* renamed from: a */
    public Object mo14a(C0495Gr gr) {
        Object[] args = gr.getArgs();
        switch (gr.mo2417hq() - CitizenImprovement._m_methodCount) {
            case 0:
                return new Boolean(m19079a((CitizenImprovement) args[0]));
            case 1:
                apQ();
                return null;
            case 2:
                return new Long(bPQ());
            case 3:
                apP();
                return null;
            case 4:
                return bPS();
            case 5:
                return apS();
            default:
                return super.mo14a(gr);
        }
    }

    /* renamed from: a */
    public void mo15a(Collection collection, C0495Gr gr) {
        super.mo15a(collection, gr);
    }

    public void apR() {
        switch (bFf().mo6893i(bLq)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, bLq, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, bLq, new Object[0]));
                break;
        }
        apQ();
    }

    public CitizenImprovement.C2184a apT() {
        switch (bFf().mo6893i(bLs)) {
            case 0:
                return null;
            case 2:
                return (CitizenImprovement.C2184a) bFf().mo5606d(new aCE(this, bLs, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, bLs, new Object[0]));
                break;
        }
        return apS();
    }

    /* renamed from: b */
    public boolean mo550b(CitizenImprovement cZVar) {
        switch (bFf().mo6893i(bLo)) {
            case 0:
                return false;
            case 2:
                return ((Boolean) bFf().mo5606d(new aCE(this, bLo, new Object[]{cZVar}))).booleanValue();
            case 3:
                bFf().mo5606d(new aCE(this, bLo, new Object[]{cZVar}));
                break;
        }
        return m19079a(cZVar);
    }

    public long bPR() {
        switch (bFf().mo6893i(feS)) {
            case 0:
                return 0;
            case 2:
                return ((Long) bFf().mo5606d(new aCE(this, feS, new Object[0]))).longValue();
            case 3:
                bFf().mo5606d(new aCE(this, feS, new Object[0]));
                break;
        }
        return bPQ();
    }

    public I18NString bPT() {
        switch (bFf().mo6893i(feU)) {
            case 0:
                return null;
            case 2:
                return (I18NString) bFf().mo5606d(new aCE(this, feU, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, feU, new Object[0]));
                break;
        }
        return bPS();
    }

    /* renamed from: c */
    public C5663aRz[] mo25c() {
        return _m_fields;
    }

    public void cancel() {
        switch (bFf().mo6893i(bLp)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, bLp, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, bLp, new Object[0]));
                break;
        }
        apP();
    }

    /* renamed from: a */
    public void mo11900a(InsuranceImprovementType acj) {
        super.mo547a((CitizenImprovementType) acj);
    }

    @C0064Am(aul = "37d9f096358fad99f4fb746452765288", aum = 0)
    /* renamed from: a */
    private boolean m19079a(CitizenImprovement cZVar) {
        if (!(cZVar instanceof InsuranceImprovement) || awo() <= ((InsuranceImprovement) cZVar).bPR()) {
            return false;
        }
        return true;
    }

    @C0064Am(aul = "dad5066a0710e65005c48bef8b6b25f6", aum = 0)
    private void apQ() {
    }

    @C0064Am(aul = "108263409a49adf541d670ff76a12422", aum = 0)
    private long bPQ() {
        return awo();
    }

    @C0064Am(aul = "c26b5cdf94d0f94a1c1177972a8c4951", aum = 0)
    private void apP() {
    }

    @C0064Am(aul = "1320c76f941c8ead3c7fa622b0c37861", aum = 0)
    private I18NString bPS() {
        return bPP();
    }

    @C0064Am(aul = "ea54830340ee2aae2c6445d68c2ed481", aum = 0)
    private CitizenImprovement.C2184a apS() {
        return CitizenImprovement.C2184a.INSURANCE;
    }
}
