package game.script.citizenship.inprovements;

import game.network.message.externalizable.aCE;
import game.script.citizenship.CitizenImprovement;
import game.script.citizenship.CitizenImprovementType;
import logic.baa.C1616Xf;
import logic.baa.C1634Xv;
import logic.baa.C5511aMd;
import logic.baa.C6485anp;
import logic.data.mbean.aOV;
import logic.res.code.C5663aRz;
import logic.res.code.C6494any;
import logic.res.html.C2491fm;
import p001a.*;

import java.util.Collection;

@C2712iu
@C5511aMd
@C6485anp
/* renamed from: a.Fg */
/* compiled from: a */
public class NodeAccessImprovement extends CitizenImprovement implements C1616Xf {
    public static final int _m_fieldCount = 0;
    public static final C5663aRz[] _m_fields = null;
    public static final int _m_methodCount = 0;
    public static final C2491fm[] _m_methods = null;
    public static final C2491fm bLo = null;
    public static final C2491fm bLp = null;
    public static final C2491fm bLq = null;
    public static final C2491fm bLs = null;
    public static final long serialVersionUID = 0;
    public static C6494any ___iScriptClass;

    static {
        m3235V();
    }

    public NodeAccessImprovement() {
        super((C5540aNg) null);
        super.mo10S();
    }

    public NodeAccessImprovement(CitizenImprovementType kt) {
        super((C5540aNg) null);
        super.mo547a(kt);
    }

    public NodeAccessImprovement(C5540aNg ang) {
        super(ang);
    }

    /* renamed from: V */
    static void m3235V() {
        serialVersionUID = (long) 1;
        _m_fieldCount = CitizenImprovement._m_fieldCount + 0;
        _m_methodCount = CitizenImprovement._m_methodCount + 4;
        _m_fields = new C5663aRz[(CitizenImprovement._m_fieldCount + 0)];
        C1634Xv.m11725a((Object[]) CitizenImprovement._m_fields, (Object[]) _m_fields);
        int i = CitizenImprovement._m_methodCount;
        C2491fm[] fmVarArr = new C2491fm[(i + 4)];
        C2491fm a = C4105zY.m41624a(NodeAccessImprovement.class, "be914c83899d6f8386ae2eb2b426fd5f", i);
        bLp = a;
        fmVarArr[i] = a;
        int i2 = i + 1;
        C2491fm a2 = C4105zY.m41624a(NodeAccessImprovement.class, "8e4e4d828940e43d5385dfe17c89080e", i2);
        bLq = a2;
        fmVarArr[i2] = a2;
        int i3 = i2 + 1;
        C2491fm a3 = C4105zY.m41624a(NodeAccessImprovement.class, "1773d1fb371aa5200a284dc00b174d5f", i3);
        bLo = a3;
        fmVarArr[i3] = a3;
        int i4 = i3 + 1;
        C2491fm a4 = C4105zY.m41624a(NodeAccessImprovement.class, "a523a20b54695b05ca352d7279a1ad63", i4);
        bLs = a4;
        fmVarArr[i4] = a4;
        int i5 = i4 + 1;
        _m_methods = fmVarArr;
        C1634Xv.m11725a((Object[]) CitizenImprovement._m_methods, (Object[]) _m_methods);
        ___iScriptClass = aUO.m18566a(NodeAccessImprovement.class, aOV.class, _m_fields, _m_methods);
    }

    /* renamed from: S */
    public void mo10S() {
        super.mo10S();
    }

    /* renamed from: T */
    public C6494any mo11T() {
        return ___iScriptClass;
    }

    /* renamed from: U */
    public C2491fm[] mo12U() {
        return _m_methods;
    }

    /* renamed from: W */
    public Object mo13W() {
        return new aOV(this);
    }

    /* renamed from: a */
    public Object mo14a(C0495Gr gr) {
        Object[] args = gr.getArgs();
        switch (gr.mo2417hq() - CitizenImprovement._m_methodCount) {
            case 0:
                apP();
                return null;
            case 1:
                apQ();
                return null;
            case 2:
                return new Boolean(m3236a((CitizenImprovement) args[0]));
            case 3:
                return apS();
            default:
                return super.mo14a(gr);
        }
    }

    /* renamed from: a */
    public void mo15a(Collection collection, C0495Gr gr) {
        super.mo15a(collection, gr);
    }

    public void apR() {
        switch (bFf().mo6893i(bLq)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, bLq, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, bLq, new Object[0]));
                break;
        }
        apQ();
    }

    public CitizenImprovement.C2184a apT() {
        switch (bFf().mo6893i(bLs)) {
            case 0:
                return null;
            case 2:
                return (CitizenImprovement.C2184a) bFf().mo5606d(new aCE(this, bLs, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, bLs, new Object[0]));
                break;
        }
        return apS();
    }

    /* renamed from: b */
    public boolean mo550b(CitizenImprovement cZVar) {
        switch (bFf().mo6893i(bLo)) {
            case 0:
                return false;
            case 2:
                return ((Boolean) bFf().mo5606d(new aCE(this, bLo, new Object[]{cZVar}))).booleanValue();
            case 3:
                bFf().mo5606d(new aCE(this, bLo, new Object[]{cZVar}));
                break;
        }
        return m3236a(cZVar);
    }

    /* renamed from: c */
    public C5663aRz[] mo25c() {
        return _m_fields;
    }

    public void cancel() {
        switch (bFf().mo6893i(bLp)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, bLp, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, bLp, new Object[0]));
                break;
        }
        apP();
    }

    /* renamed from: a */
    public void mo547a(CitizenImprovementType kt) {
        super.mo547a(kt);
    }

    @C0064Am(aul = "be914c83899d6f8386ae2eb2b426fd5f", aum = 0)
    private void apP() {
    }

    @C0064Am(aul = "8e4e4d828940e43d5385dfe17c89080e", aum = 0)
    private void apQ() {
    }

    @C0064Am(aul = "1773d1fb371aa5200a284dc00b174d5f", aum = 0)
    /* renamed from: a */
    private boolean m3236a(CitizenImprovement cZVar) {
        return true;
    }

    @C0064Am(aul = "a523a20b54695b05ca352d7279a1ad63", aum = 0)
    private CitizenImprovement.C2184a apS() {
        return CitizenImprovement.C2184a.NODE_ACCESS;
    }
}
