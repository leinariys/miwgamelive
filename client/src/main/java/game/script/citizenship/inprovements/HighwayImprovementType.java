package game.script.citizenship.inprovements;

import game.network.message.externalizable.aCE;
import game.script.citizenship.CitizenImprovementType;
import logic.baa.*;
import logic.data.mbean.C5252aCe;
import logic.res.code.C5663aRz;
import logic.res.code.C6494any;
import logic.res.html.C2491fm;
import p001a.*;

import java.util.Collection;

@C0566Hp
@C5511aMd
@C6485anp
/* renamed from: a.aBi  reason: case insensitive filesystem */
/* compiled from: a */
public class HighwayImprovementType extends CitizenImprovementType implements C1616Xf {
    public static final int _m_fieldCount = 0;
    public static final C5663aRz[] _m_fields = null;
    public static final int _m_methodCount = 0;
    public static final C2491fm[] _m_methods = null;
    /* renamed from: dN */
    public static final C2491fm f2431dN = null;
    public static final C2491fm hgU = null;
    public static final long serialVersionUID = 0;
    public static C6494any ___iScriptClass;

    static {
        m13042V();
    }

    public HighwayImprovementType() {
        super((C5540aNg) null);
        super.mo10S();
    }

    public HighwayImprovementType(C5540aNg ang) {
        super(ang);
    }

    /* renamed from: V */
    static void m13042V() {
        serialVersionUID = (long) 1;
        _m_fieldCount = CitizenImprovementType._m_fieldCount + 0;
        _m_methodCount = CitizenImprovementType._m_methodCount + 2;
        _m_fields = new C5663aRz[(CitizenImprovementType._m_fieldCount + 0)];
        C1634Xv.m11725a((Object[]) CitizenImprovementType._m_fields, (Object[]) _m_fields);
        int i = CitizenImprovementType._m_methodCount;
        C2491fm[] fmVarArr = new C2491fm[(i + 2)];
        C2491fm a = C4105zY.m41624a(HighwayImprovementType.class, "868f8698ef817aff8fe3da1c346fa2ec", i);
        hgU = a;
        fmVarArr[i] = a;
        int i2 = i + 1;
        C2491fm a2 = C4105zY.m41624a(HighwayImprovementType.class, "7b317576d4316a7cc36fb8bcb0f59094", i2);
        f2431dN = a2;
        fmVarArr[i2] = a2;
        int i3 = i2 + 1;
        _m_methods = fmVarArr;
        C1634Xv.m11725a((Object[]) CitizenImprovementType._m_methods, (Object[]) _m_methods);
        ___iScriptClass = aUO.m18566a(HighwayImprovementType.class, C5252aCe.class, _m_fields, _m_methods);
    }

    /* renamed from: T */
    public C6494any mo11T() {
        return ___iScriptClass;
    }

    /* renamed from: U */
    public C2491fm[] mo12U() {
        return _m_methods;
    }

    /* renamed from: W */
    public Object mo13W() {
        return new C5252aCe(this);
    }

    /* renamed from: a */
    public Object mo14a(C0495Gr gr) {
        gr.getArgs();
        switch (gr.mo2417hq() - CitizenImprovementType._m_methodCount) {
            case 0:
                return cIV();
            case 1:
                return m13043aT();
            default:
                return super.mo14a(gr);
        }
    }

    /* renamed from: a */
    public void mo15a(Collection collection, C0495Gr gr) {
        super.mo15a(collection, gr);
    }

    /* access modifiers changed from: protected */
    /* renamed from: aU */
    public <T extends aDJ> T mo745aU() {
        switch (bFf().mo6893i(f2431dN)) {
            case 0:
                return null;
            case 2:
                return (aDJ) bFf().mo5606d(new aCE(this, f2431dN, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f2431dN, new Object[0]));
                break;
        }
        return m13043aT();
    }

    /* renamed from: c */
    public C5663aRz[] mo25c() {
        return _m_fields;
    }

    public HighwayImprovement cIW() {
        switch (bFf().mo6893i(hgU)) {
            case 0:
                return null;
            case 2:
                return (HighwayImprovement) bFf().mo5606d(new aCE(this, hgU, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, hgU, new Object[0]));
                break;
        }
        return cIV();
    }

    /* renamed from: S */
    public void mo10S() {
        super.mo10S();
    }

    @C0064Am(aul = "868f8698ef817aff8fe3da1c346fa2ec", aum = 0)
    private HighwayImprovement cIV() {
        return (HighwayImprovement) mo745aU();
    }

    @C0064Am(aul = "7b317576d4316a7cc36fb8bcb0f59094", aum = 0)
    /* renamed from: aT */
    private <T extends aDJ> T m13043aT() {
        T t = (HighwayImprovement) bFf().mo6865M(HighwayImprovement.class);
        t.mo547a((CitizenImprovementType) this);
        return t;
    }
}
