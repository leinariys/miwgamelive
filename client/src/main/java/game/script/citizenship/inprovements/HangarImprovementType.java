package game.script.citizenship.inprovements;

import game.network.message.externalizable.aCE;
import game.script.citizenship.CitizenImprovementType;
import logic.baa.*;
import logic.data.mbean.C2236dC;
import logic.res.code.C5640aRc;
import logic.res.code.C5663aRz;
import logic.res.code.C6494any;
import logic.res.html.C2491fm;
import p001a.*;

import java.util.Collection;

@C0566Hp
@C5511aMd
@C6485anp
/* renamed from: a.OD */
/* compiled from: a */
public class HangarImprovementType extends CitizenImprovementType implements C1616Xf {
    public static final int _m_fieldCount = 0;
    public static final C5663aRz[] _m_fields = null;
    public static final int _m_methodCount = 0;
    public static final C2491fm[] _m_methods = null;
    /* renamed from: dN */
    public static final C2491fm f1278dN = null;
    public static final C5663aRz dNa = null;
    public static final C2491fm dNb = null;
    public static final C2491fm dNc = null;
    public static final C2491fm dNd = null;
    public static final long serialVersionUID = 0;
    public static C6494any ___iScriptClass;
    @C0064Am(aul = "2b1d435a6ec596a6bf2ac14bef9f62e9", aum = 0)

    /* renamed from: zI */
    private static int f1279zI;

    static {
        m7805V();
    }

    public HangarImprovementType() {
        super((C5540aNg) null);
        super.mo10S();
    }

    public HangarImprovementType(C5540aNg ang) {
        super(ang);
    }

    /* renamed from: V */
    static void m7805V() {
        serialVersionUID = (long) 1;
        _m_fieldCount = CitizenImprovementType._m_fieldCount + 1;
        _m_methodCount = CitizenImprovementType._m_methodCount + 4;
        int i = CitizenImprovementType._m_fieldCount;
        C5663aRz[] arzArr = new C5663aRz[(i + 1)];
        C5663aRz b = C5640aRc.m17844b(HangarImprovementType.class, "2b1d435a6ec596a6bf2ac14bef9f62e9", i);
        dNa = b;
        arzArr[i] = b;
        int i2 = i + 1;
        _m_fields = arzArr;
        C1634Xv.m11725a((Object[]) CitizenImprovementType._m_fields, (Object[]) _m_fields);
        int i3 = CitizenImprovementType._m_methodCount;
        C2491fm[] fmVarArr = new C2491fm[(i3 + 4)];
        C2491fm a = C4105zY.m41624a(HangarImprovementType.class, "f54b052b00791e015b8a28d3fbf5e758", i3);
        dNb = a;
        fmVarArr[i3] = a;
        int i4 = i3 + 1;
        C2491fm a2 = C4105zY.m41624a(HangarImprovementType.class, "b43587e578dc452306778dbb658c2427", i4);
        dNc = a2;
        fmVarArr[i4] = a2;
        int i5 = i4 + 1;
        C2491fm a3 = C4105zY.m41624a(HangarImprovementType.class, "b98edda68ea613217652de9b93bb0033", i5);
        dNd = a3;
        fmVarArr[i5] = a3;
        int i6 = i5 + 1;
        C2491fm a4 = C4105zY.m41624a(HangarImprovementType.class, "34c7efd48f68e12998b23090e0976ff9", i6);
        f1278dN = a4;
        fmVarArr[i6] = a4;
        int i7 = i6 + 1;
        _m_methods = fmVarArr;
        C1634Xv.m11725a((Object[]) CitizenImprovementType._m_methods, (Object[]) _m_methods);
        ___iScriptClass = aUO.m18566a(HangarImprovementType.class, C2236dC.class, _m_fields, _m_methods);
    }

    private int blU() {
        return bFf().mo5608dq().mo3212n(dNa);
    }

    /* renamed from: mf */
    private void m7807mf(int i) {
        bFf().mo5608dq().mo3183b(dNa, i);
    }

    /* renamed from: T */
    public C6494any mo11T() {
        return ___iScriptClass;
    }

    /* renamed from: U */
    public C2491fm[] mo12U() {
        return _m_methods;
    }

    /* renamed from: W */
    public Object mo13W() {
        return new C2236dC(this);
    }

    /* renamed from: a */
    public Object mo14a(C0495Gr gr) {
        Object[] args = gr.getArgs();
        switch (gr.mo2417hq() - CitizenImprovementType._m_methodCount) {
            case 0:
                return new Integer(blV());
            case 1:
                m7808mg(((Integer) args[0]).intValue());
                return null;
            case 2:
                return blX();
            case 3:
                return m7806aT();
            default:
                return super.mo14a(gr);
        }
    }

    /* renamed from: a */
    public void mo15a(Collection collection, C0495Gr gr) {
        super.mo15a(collection, gr);
    }

    /* access modifiers changed from: protected */
    /* renamed from: aU */
    public <T extends aDJ> T mo745aU() {
        switch (bFf().mo6893i(f1278dN)) {
            case 0:
                return null;
            case 2:
                return (aDJ) bFf().mo5606d(new aCE(this, f1278dN, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f1278dN, new Object[0]));
                break;
        }
        return m7806aT();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Hangar Bays")
    public int blW() {
        switch (bFf().mo6893i(dNb)) {
            case 0:
                return 0;
            case 2:
                return ((Integer) bFf().mo5606d(new aCE(this, dNb, new Object[0]))).intValue();
            case 3:
                bFf().mo5606d(new aCE(this, dNb, new Object[0]));
                break;
        }
        return blV();
    }

    public HangarImprovement blY() {
        switch (bFf().mo6893i(dNd)) {
            case 0:
                return null;
            case 2:
                return (HangarImprovement) bFf().mo5606d(new aCE(this, dNd, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, dNd, new Object[0]));
                break;
        }
        return blX();
    }

    /* renamed from: c */
    public C5663aRz[] mo25c() {
        return _m_fields;
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Hangar Bays")
    /* renamed from: mh */
    public void mo4331mh(int i) {
        switch (bFf().mo6893i(dNc)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, dNc, new Object[]{new Integer(i)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, dNc, new Object[]{new Integer(i)}));
                break;
        }
        m7808mg(i);
    }

    /* renamed from: S */
    public void mo10S() {
        super.mo10S();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Hangar Bays")
    @C0064Am(aul = "f54b052b00791e015b8a28d3fbf5e758", aum = 0)
    private int blV() {
        return blU();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Hangar Bays")
    @C0064Am(aul = "b43587e578dc452306778dbb658c2427", aum = 0)
    /* renamed from: mg */
    private void m7808mg(int i) {
        m7807mf(i);
    }

    @C0064Am(aul = "b98edda68ea613217652de9b93bb0033", aum = 0)
    private HangarImprovement blX() {
        return (HangarImprovement) mo745aU();
    }

    @C0064Am(aul = "34c7efd48f68e12998b23090e0976ff9", aum = 0)
    /* renamed from: aT */
    private <T extends aDJ> T m7806aT() {
        T t = (HangarImprovement) bFf().mo6865M(HangarImprovement.class);
        t.mo6092a(this);
        return t;
    }
}
