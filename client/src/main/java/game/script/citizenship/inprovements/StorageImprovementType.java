package game.script.citizenship.inprovements;

import game.network.message.externalizable.aCE;
import game.script.citizenship.CitizenImprovementType;
import logic.baa.*;
import logic.data.mbean.C5202aAg;
import logic.res.code.C5640aRc;
import logic.res.code.C5663aRz;
import logic.res.code.C6494any;
import logic.res.html.C2491fm;
import p001a.*;

import java.util.Collection;

@C0566Hp
@C5511aMd
@C6485anp
/* renamed from: a.Ni */
/* compiled from: a */
public class StorageImprovementType extends CitizenImprovementType implements C1616Xf {
    /* renamed from: Pv */
    public static final C5663aRz f1251Pv = null;
    /* renamed from: Pw */
    public static final C2491fm f1252Pw = null;
    /* renamed from: Px */
    public static final C2491fm f1253Px = null;
    public static final int _m_fieldCount = 0;
    public static final C5663aRz[] _m_fields = null;
    public static final int _m_methodCount = 0;
    public static final C2491fm[] _m_methods = null;
    public static final C2491fm dDn = null;
    /* renamed from: dN */
    public static final C2491fm f1254dN = null;
    public static final long serialVersionUID = 0;
    public static C6494any ___iScriptClass;
    @C0064Am(aul = "d5e393b0d14d8cd043561b012192fa54", aum = 0)

    /* renamed from: DL */
    private static C3892wO f1250DL;

    static {
        m7700V();
    }

    public StorageImprovementType() {
        super((C5540aNg) null);
        super.mo10S();
    }

    public StorageImprovementType(C5540aNg ang) {
        super(ang);
    }

    /* renamed from: V */
    static void m7700V() {
        serialVersionUID = (long) 1;
        _m_fieldCount = CitizenImprovementType._m_fieldCount + 1;
        _m_methodCount = CitizenImprovementType._m_methodCount + 4;
        int i = CitizenImprovementType._m_fieldCount;
        C5663aRz[] arzArr = new C5663aRz[(i + 1)];
        C5663aRz b = C5640aRc.m17844b(StorageImprovementType.class, "d5e393b0d14d8cd043561b012192fa54", i);
        f1251Pv = b;
        arzArr[i] = b;
        int i2 = i + 1;
        _m_fields = arzArr;
        C1634Xv.m11725a((Object[]) CitizenImprovementType._m_fields, (Object[]) _m_fields);
        int i3 = CitizenImprovementType._m_methodCount;
        C2491fm[] fmVarArr = new C2491fm[(i3 + 4)];
        C2491fm a = C4105zY.m41624a(StorageImprovementType.class, "bea1082db8f731e89f7bb3002585b129", i3);
        f1252Pw = a;
        fmVarArr[i3] = a;
        int i4 = i3 + 1;
        C2491fm a2 = C4105zY.m41624a(StorageImprovementType.class, "5974561f0388a142bd84851f4551be30", i4);
        f1253Px = a2;
        fmVarArr[i4] = a2;
        int i5 = i4 + 1;
        C2491fm a3 = C4105zY.m41624a(StorageImprovementType.class, "3a8662546a73660e69934eaca1db79a0", i5);
        dDn = a3;
        fmVarArr[i5] = a3;
        int i6 = i5 + 1;
        C2491fm a4 = C4105zY.m41624a(StorageImprovementType.class, "e75b64d17c54bfb032532bead70ef8a7", i6);
        f1254dN = a4;
        fmVarArr[i6] = a4;
        int i7 = i6 + 1;
        _m_methods = fmVarArr;
        C1634Xv.m11725a((Object[]) CitizenImprovementType._m_methods, (Object[]) _m_methods);
        ___iScriptClass = aUO.m18566a(StorageImprovementType.class, C5202aAg.class, _m_fields, _m_methods);
    }

    /* renamed from: a */
    private void m7701a(C3892wO wOVar) {
        bFf().mo5608dq().mo3197f(f1251Pv, wOVar);
    }

    /* renamed from: vi */
    private C3892wO m7704vi() {
        return (C3892wO) bFf().mo5608dq().mo3214p(f1251Pv);
    }

    /* renamed from: T */
    public C6494any mo11T() {
        return ___iScriptClass;
    }

    /* renamed from: U */
    public C2491fm[] mo12U() {
        return _m_methods;
    }

    /* renamed from: W */
    public Object mo13W() {
        return new C5202aAg(this);
    }

    /* renamed from: a */
    public Object mo14a(C0495Gr gr) {
        Object[] args = gr.getArgs();
        switch (gr.mo2417hq() - CitizenImprovementType._m_methodCount) {
            case 0:
                return m7705vj();
            case 1:
                m7703b((C3892wO) args[0]);
                return null;
            case 2:
                return bis();
            case 3:
                return m7702aT();
            default:
                return super.mo14a(gr);
        }
    }

    /* renamed from: a */
    public void mo15a(Collection collection, C0495Gr gr) {
        super.mo15a(collection, gr);
    }

    /* access modifiers changed from: protected */
    /* renamed from: aU */
    public <T extends aDJ> T mo745aU() {
        switch (bFf().mo6893i(f1254dN)) {
            case 0:
                return null;
            case 2:
                return (aDJ) bFf().mo5606d(new aCE(this, f1254dN, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f1254dN, new Object[0]));
                break;
        }
        return m7702aT();
    }

    public StorageImprovement bit() {
        switch (bFf().mo6893i(dDn)) {
            case 0:
                return null;
            case 2:
                return (StorageImprovement) bFf().mo5606d(new aCE(this, dDn, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, dDn, new Object[0]));
                break;
        }
        return bis();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Max Volume")
    /* renamed from: c */
    public void mo4261c(C3892wO wOVar) {
        switch (bFf().mo6893i(f1253Px)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f1253Px, new Object[]{wOVar}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f1253Px, new Object[]{wOVar}));
                break;
        }
        m7703b(wOVar);
    }

    /* renamed from: c */
    public C5663aRz[] mo25c() {
        return _m_fields;
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Max Volume")
    /* renamed from: vk */
    public C3892wO mo4262vk() {
        switch (bFf().mo6893i(f1252Pw)) {
            case 0:
                return null;
            case 2:
                return (C3892wO) bFf().mo5606d(new aCE(this, f1252Pw, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f1252Pw, new Object[0]));
                break;
        }
        return m7705vj();
    }

    /* renamed from: S */
    public void mo10S() {
        super.mo10S();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Max Volume")
    @C0064Am(aul = "bea1082db8f731e89f7bb3002585b129", aum = 0)
    /* renamed from: vj */
    private C3892wO m7705vj() {
        return m7704vi();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Max Volume")
    @C0064Am(aul = "5974561f0388a142bd84851f4551be30", aum = 0)
    /* renamed from: b */
    private void m7703b(C3892wO wOVar) {
        m7701a(wOVar);
    }

    @C0064Am(aul = "3a8662546a73660e69934eaca1db79a0", aum = 0)
    private StorageImprovement bis() {
        return (StorageImprovement) mo745aU();
    }

    @C0064Am(aul = "e75b64d17c54bfb032532bead70ef8a7", aum = 0)
    /* renamed from: aT */
    private <T extends aDJ> T m7702aT() {
        T t = (StorageImprovement) bFf().mo6865M(StorageImprovement.class);
        t.mo23179a(this);
        return t;
    }
}
