package game.script.citizenship.inprovements;

import game.network.message.externalizable.aCE;
import game.script.citizenship.CitizenImprovementType;
import logic.baa.*;
import logic.data.mbean.C6107agb;
import logic.res.code.C5663aRz;
import logic.res.code.C6494any;
import logic.res.html.C2491fm;
import p001a.*;

import java.util.Collection;

@C0566Hp
@C5511aMd
@C6485anp
/* renamed from: a.Oc */
/* compiled from: a */
public class NodeAccessImprovementType extends CitizenImprovementType implements C1616Xf {
    public static final int _m_fieldCount = 0;
    public static final C5663aRz[] _m_fields = null;
    public static final int _m_methodCount = 0;
    public static final C2491fm[] _m_methods = null;
    public static final C2491fm dIQ = null;
    /* renamed from: dN */
    public static final C2491fm f1325dN = null;
    public static final long serialVersionUID = 0;
    public static C6494any ___iScriptClass;

    static {
        m8094V();
    }

    public NodeAccessImprovementType() {
        super((C5540aNg) null);
        super.mo10S();
    }

    public NodeAccessImprovementType(C5540aNg ang) {
        super(ang);
    }

    /* renamed from: V */
    static void m8094V() {
        serialVersionUID = (long) 1;
        _m_fieldCount = CitizenImprovementType._m_fieldCount + 0;
        _m_methodCount = CitizenImprovementType._m_methodCount + 2;
        _m_fields = new C5663aRz[(CitizenImprovementType._m_fieldCount + 0)];
        C1634Xv.m11725a((Object[]) CitizenImprovementType._m_fields, (Object[]) _m_fields);
        int i = CitizenImprovementType._m_methodCount;
        C2491fm[] fmVarArr = new C2491fm[(i + 2)];
        C2491fm a = C4105zY.m41624a(NodeAccessImprovementType.class, "b45a8f0189a3008aeab1bb9f6fc20941", i);
        dIQ = a;
        fmVarArr[i] = a;
        int i2 = i + 1;
        C2491fm a2 = C4105zY.m41624a(NodeAccessImprovementType.class, "9509332ce756c11c525a7f72f18d4aad", i2);
        f1325dN = a2;
        fmVarArr[i2] = a2;
        int i3 = i2 + 1;
        _m_methods = fmVarArr;
        C1634Xv.m11725a((Object[]) CitizenImprovementType._m_methods, (Object[]) _m_methods);
        ___iScriptClass = aUO.m18566a(NodeAccessImprovementType.class, C6107agb.class, _m_fields, _m_methods);
    }

    /* renamed from: T */
    public C6494any mo11T() {
        return ___iScriptClass;
    }

    /* renamed from: U */
    public C2491fm[] mo12U() {
        return _m_methods;
    }

    /* renamed from: W */
    public Object mo13W() {
        return new C6107agb(this);
    }

    /* renamed from: a */
    public Object mo14a(C0495Gr gr) {
        gr.getArgs();
        switch (gr.mo2417hq() - CitizenImprovementType._m_methodCount) {
            case 0:
                return bkX();
            case 1:
                return m8095aT();
            default:
                return super.mo14a(gr);
        }
    }

    /* renamed from: a */
    public void mo15a(Collection collection, C0495Gr gr) {
        super.mo15a(collection, gr);
    }

    /* access modifiers changed from: protected */
    /* renamed from: aU */
    public <T extends aDJ> T mo745aU() {
        switch (bFf().mo6893i(f1325dN)) {
            case 0:
                return null;
            case 2:
                return (aDJ) bFf().mo5606d(new aCE(this, f1325dN, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f1325dN, new Object[0]));
                break;
        }
        return m8095aT();
    }

    public NodeAccessImprovement bkY() {
        switch (bFf().mo6893i(dIQ)) {
            case 0:
                return null;
            case 2:
                return (NodeAccessImprovement) bFf().mo5606d(new aCE(this, dIQ, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, dIQ, new Object[0]));
                break;
        }
        return bkX();
    }

    /* renamed from: c */
    public C5663aRz[] mo25c() {
        return _m_fields;
    }

    /* renamed from: S */
    public void mo10S() {
        super.mo10S();
    }

    @C0064Am(aul = "b45a8f0189a3008aeab1bb9f6fc20941", aum = 0)
    private NodeAccessImprovement bkX() {
        return (NodeAccessImprovement) mo745aU();
    }

    @C0064Am(aul = "9509332ce756c11c525a7f72f18d4aad", aum = 0)
    /* renamed from: aT */
    private <T extends aDJ> T m8095aT() {
        T t = (NodeAccessImprovement) bFf().mo6865M(NodeAccessImprovement.class);
        t.mo547a((CitizenImprovementType) this);
        return t;
    }
}
