package game.script.citizenship.inprovements;

import game.network.message.externalizable.aCE;
import game.script.citizenship.CitizenImprovementType;
import logic.baa.*;
import logic.data.mbean.C0357El;
import logic.res.code.C5640aRc;
import logic.res.code.C5663aRz;
import logic.res.code.C6494any;
import logic.res.html.C2491fm;
import p001a.*;
import taikodom.infra.script.I18NString;

import java.util.Collection;

@C0566Hp
@C5511aMd
@C6485anp
/* renamed from: a.acJ  reason: case insensitive filesystem */
/* compiled from: a */
public class InsuranceImprovementType extends CitizenImprovementType implements C1616Xf {
    public static final int _m_fieldCount = 0;
    public static final C5663aRz[] _m_fields = null;
    public static final int _m_methodCount = 0;
    public static final C2491fm[] _m_methods = null;
    public static final C5663aRz ciB = null;
    /* renamed from: dN */
    public static final C2491fm f4215dN = null;
    public static final C5663aRz feR = null;
    public static final C2491fm feS = null;
    public static final C2491fm feT = null;
    public static final C2491fm feU = null;
    public static final C2491fm feV = null;
    public static final C2491fm feW = null;
    public static final long serialVersionUID = 0;
    public static C6494any ___iScriptClass;
    @C0064Am(aul = "22957b91567e2702c58cd9e8288e4aa3", aum = 1)
    private static I18NString cRI;
    @C0064Am(aul = "3427bc96b1539e3315e4c14ac2c39b15", aum = 0)
    private static long ciA;

    static {
        m20301V();
    }

    public InsuranceImprovementType() {
        super((C5540aNg) null);
        super.mo10S();
    }

    public InsuranceImprovementType(C5540aNg ang) {
        super(ang);
    }

    /* renamed from: V */
    static void m20301V() {
        serialVersionUID = (long) 1;
        _m_fieldCount = CitizenImprovementType._m_fieldCount + 2;
        _m_methodCount = CitizenImprovementType._m_methodCount + 6;
        int i = CitizenImprovementType._m_fieldCount;
        C5663aRz[] arzArr = new C5663aRz[(i + 2)];
        C5663aRz b = C5640aRc.m17844b(InsuranceImprovementType.class, "3427bc96b1539e3315e4c14ac2c39b15", i);
        ciB = b;
        arzArr[i] = b;
        int i2 = i + 1;
        C5663aRz b2 = C5640aRc.m17844b(InsuranceImprovementType.class, "22957b91567e2702c58cd9e8288e4aa3", i2);
        feR = b2;
        arzArr[i2] = b2;
        int i3 = i2 + 1;
        _m_fields = arzArr;
        C1634Xv.m11725a((Object[]) CitizenImprovementType._m_fields, (Object[]) _m_fields);
        int i4 = CitizenImprovementType._m_methodCount;
        C2491fm[] fmVarArr = new C2491fm[(i4 + 6)];
        C2491fm a = C4105zY.m41624a(InsuranceImprovementType.class, "ffbe4758f146b095de7f95b421893568", i4);
        feS = a;
        fmVarArr[i4] = a;
        int i5 = i4 + 1;
        C2491fm a2 = C4105zY.m41624a(InsuranceImprovementType.class, "4577c59e831632f1adc350e186240e9c", i5);
        feT = a2;
        fmVarArr[i5] = a2;
        int i6 = i5 + 1;
        C2491fm a3 = C4105zY.m41624a(InsuranceImprovementType.class, "cc25503ff6bfc7bbfd7285f78e9f0acd", i6);
        feU = a3;
        fmVarArr[i6] = a3;
        int i7 = i6 + 1;
        C2491fm a4 = C4105zY.m41624a(InsuranceImprovementType.class, "7319713e1825e6985a5c21dc1415c4db", i7);
        feV = a4;
        fmVarArr[i7] = a4;
        int i8 = i7 + 1;
        C2491fm a5 = C4105zY.m41624a(InsuranceImprovementType.class, "f90f673e3e96c40ed9a1979ee2597d89", i8);
        feW = a5;
        fmVarArr[i8] = a5;
        int i9 = i8 + 1;
        C2491fm a6 = C4105zY.m41624a(InsuranceImprovementType.class, "babc61fc1ec3476ecc4d572cfe9b1f4e", i9);
        f4215dN = a6;
        fmVarArr[i9] = a6;
        int i10 = i9 + 1;
        _m_methods = fmVarArr;
        C1634Xv.m11725a((Object[]) CitizenImprovementType._m_methods, (Object[]) _m_methods);
        ___iScriptClass = aUO.m18566a(InsuranceImprovementType.class, C0357El.class, _m_fields, _m_methods);
    }

    private long awo() {
        return bFf().mo5608dq().mo3213o(ciB);
    }

    private I18NString bPP() {
        return (I18NString) bFf().mo5608dq().mo3214p(feR);
    }

    /* renamed from: dF */
    private void m20303dF(long j) {
        bFf().mo5608dq().mo3184b(ciB, j);
    }

    /* renamed from: lW */
    private void m20305lW(I18NString i18NString) {
        bFf().mo5608dq().mo3197f(feR, i18NString);
    }

    /* renamed from: T */
    public C6494any mo11T() {
        return ___iScriptClass;
    }

    /* renamed from: U */
    public C2491fm[] mo12U() {
        return _m_methods;
    }

    /* renamed from: W */
    public Object mo13W() {
        return new C0357El(this);
    }

    /* renamed from: a */
    public Object mo14a(C0495Gr gr) {
        Object[] args = gr.getArgs();
        switch (gr.mo2417hq() - CitizenImprovementType._m_methodCount) {
            case 0:
                return new Long(bPQ());
            case 1:
                m20304gD(((Long) args[0]).longValue());
                return null;
            case 2:
                return bPS();
            case 3:
                m20306lX((I18NString) args[0]);
                return null;
            case 4:
                return bPU();
            case 5:
                return m20302aT();
            default:
                return super.mo14a(gr);
        }
    }

    /* renamed from: a */
    public void mo15a(Collection collection, C0495Gr gr) {
        super.mo15a(collection, gr);
    }

    /* access modifiers changed from: protected */
    /* renamed from: aU */
    public <T extends aDJ> T mo745aU() {
        switch (bFf().mo6893i(f4215dN)) {
            case 0:
                return null;
            case 2:
                return (aDJ) bFf().mo5606d(new aCE(this, f4215dN, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f4215dN, new Object[0]));
                break;
        }
        return m20302aT();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Price Ceiling")
    public long bPR() {
        switch (bFf().mo6893i(feS)) {
            case 0:
                return 0;
            case 2:
                return ((Long) bFf().mo5606d(new aCE(this, feS, new Object[0]))).longValue();
            case 3:
                bFf().mo5606d(new aCE(this, feS, new Object[0]));
                break;
        }
        return bPQ();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Insurance Description")
    public I18NString bPT() {
        switch (bFf().mo6893i(feU)) {
            case 0:
                return null;
            case 2:
                return (I18NString) bFf().mo5606d(new aCE(this, feU, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, feU, new Object[0]));
                break;
        }
        return bPS();
    }

    public InsuranceImprovement bPV() {
        switch (bFf().mo6893i(feW)) {
            case 0:
                return null;
            case 2:
                return (InsuranceImprovement) bFf().mo5606d(new aCE(this, feW, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, feW, new Object[0]));
                break;
        }
        return bPU();
    }

    /* renamed from: c */
    public C5663aRz[] mo25c() {
        return _m_fields;
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Price Ceiling")
    /* renamed from: gE */
    public void mo12596gE(long j) {
        switch (bFf().mo6893i(feT)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, feT, new Object[]{new Long(j)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, feT, new Object[]{new Long(j)}));
                break;
        }
        m20304gD(j);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Insurance Description")
    /* renamed from: lY */
    public void mo12597lY(I18NString i18NString) {
        switch (bFf().mo6893i(feV)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, feV, new Object[]{i18NString}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, feV, new Object[]{i18NString}));
                break;
        }
        m20306lX(i18NString);
    }

    /* renamed from: S */
    public void mo10S() {
        super.mo10S();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Price Ceiling")
    @C0064Am(aul = "ffbe4758f146b095de7f95b421893568", aum = 0)
    private long bPQ() {
        return awo();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Price Ceiling")
    @C0064Am(aul = "4577c59e831632f1adc350e186240e9c", aum = 0)
    /* renamed from: gD */
    private void m20304gD(long j) {
        m20303dF(j);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Insurance Description")
    @C0064Am(aul = "cc25503ff6bfc7bbfd7285f78e9f0acd", aum = 0)
    private I18NString bPS() {
        return bPP();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Insurance Description")
    @C0064Am(aul = "7319713e1825e6985a5c21dc1415c4db", aum = 0)
    /* renamed from: lX */
    private void m20306lX(I18NString i18NString) {
        m20305lW(i18NString);
    }

    @C0064Am(aul = "f90f673e3e96c40ed9a1979ee2597d89", aum = 0)
    private InsuranceImprovement bPU() {
        return (InsuranceImprovement) mo745aU();
    }

    @C0064Am(aul = "babc61fc1ec3476ecc4d572cfe9b1f4e", aum = 0)
    /* renamed from: aT */
    private <T extends aDJ> T m20302aT() {
        T t = (InsuranceImprovement) bFf().mo6865M(InsuranceImprovement.class);
        t.mo11900a(this);
        return t;
    }
}
