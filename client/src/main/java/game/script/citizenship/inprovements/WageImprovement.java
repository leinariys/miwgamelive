package game.script.citizenship.inprovements;

import game.network.message.externalizable.aCE;
import game.script.bank.Bank;
import game.script.citizenship.CitizenImprovement;
import game.script.citizenship.CitizenImprovementType;
import logic.baa.*;
import logic.data.mbean.C1343Tb;
import logic.data.mbean.C6322aki;
import logic.res.code.C5640aRc;
import logic.res.code.C5663aRz;
import logic.res.code.C6494any;
import logic.res.html.C2491fm;
import p001a.*;

import java.util.Collection;

@C6485anp
@C2712iu
@C5511aMd
/* renamed from: a.Uf */
/* compiled from: a */
public class WageImprovement extends CitizenImprovement implements C1616Xf {
    public static final int _m_fieldCount = 0;
    public static final C5663aRz[] _m_fields = null;
    public static final int _m_methodCount = 0;
    public static final C2491fm[] _m_methods = null;
    public static final C2491fm bLo = null;
    public static final C2491fm bLp = null;
    public static final C2491fm bLq = null;
    public static final C2491fm bLs = null;
    public static final C5663aRz emS = null;
    public static final C5663aRz emT = null;
    public static final C5663aRz emU = null;
    public static final C5663aRz emV = null;
    public static final C2491fm emW = null;
    public static final C2491fm emX = null;
    public static final long serialVersionUID = 0;
    public static C6494any ___iScriptClass;
    @C0064Am(aul = "049d5ba5b3b7d819fe44fcef81a33f00", aum = 0)
    private static long egO;
    @C0064Am(aul = "a4305fb87549dd085be87f50de14591a", aum = 1)
    private static float egP;
    @C0064Am(aul = "19d8e7f1c8af579c9b78153d4c11943d", aum = 2)
    private static TaskletImpl egQ;
    @C0064Am(aul = "1c308d2f46b2e85f79bb677cf69f5a7d", aum = 3)
    private static long egR;

    static {
        m10296V();
    }

    public WageImprovement() {
        super((C5540aNg) null);
        super.mo10S();
    }

    public WageImprovement(WageImprovementType agp) {
        super((C5540aNg) null);
        super._m_script_init(agp);
    }

    public WageImprovement(C5540aNg ang) {
        super(ang);
    }

    /* renamed from: V */
    static void m10296V() {
        serialVersionUID = (long) 1;
        _m_fieldCount = CitizenImprovement._m_fieldCount + 4;
        _m_methodCount = CitizenImprovement._m_methodCount + 6;
        int i = CitizenImprovement._m_fieldCount;
        C5663aRz[] arzArr = new C5663aRz[(i + 4)];
        C5663aRz b = C5640aRc.m17844b(WageImprovement.class, "049d5ba5b3b7d819fe44fcef81a33f00", i);
        emS = b;
        arzArr[i] = b;
        int i2 = i + 1;
        C5663aRz b2 = C5640aRc.m17844b(WageImprovement.class, "a4305fb87549dd085be87f50de14591a", i2);
        emT = b2;
        arzArr[i2] = b2;
        int i3 = i2 + 1;
        C5663aRz b3 = C5640aRc.m17844b(WageImprovement.class, "19d8e7f1c8af579c9b78153d4c11943d", i3);
        emU = b3;
        arzArr[i3] = b3;
        int i4 = i3 + 1;
        C5663aRz b4 = C5640aRc.m17844b(WageImprovement.class, "1c308d2f46b2e85f79bb677cf69f5a7d", i4);
        emV = b4;
        arzArr[i4] = b4;
        int i5 = i4 + 1;
        _m_fields = arzArr;
        C1634Xv.m11725a((Object[]) CitizenImprovement._m_fields, (Object[]) _m_fields);
        int i6 = CitizenImprovement._m_methodCount;
        C2491fm[] fmVarArr = new C2491fm[(i6 + 6)];
        C2491fm a = C4105zY.m41624a(WageImprovement.class, "00085663a76ea7bc9d1096558141740c", i6);
        emW = a;
        fmVarArr[i6] = a;
        int i7 = i6 + 1;
        C2491fm a2 = C4105zY.m41624a(WageImprovement.class, "a1b1ab68a96baa66dc34237469d4f48d", i7);
        bLo = a2;
        fmVarArr[i7] = a2;
        int i8 = i7 + 1;
        C2491fm a3 = C4105zY.m41624a(WageImprovement.class, "0451dec7b4eb1236d188a95c6a4bece1", i8);
        emX = a3;
        fmVarArr[i8] = a3;
        int i9 = i8 + 1;
        C2491fm a4 = C4105zY.m41624a(WageImprovement.class, "34a62e4114c7780306d1539a16091877", i9);
        bLp = a4;
        fmVarArr[i9] = a4;
        int i10 = i9 + 1;
        C2491fm a5 = C4105zY.m41624a(WageImprovement.class, "e7a5453c098a6e60fe02b5e3990e1fe0", i10);
        bLq = a5;
        fmVarArr[i10] = a5;
        int i11 = i10 + 1;
        C2491fm a6 = C4105zY.m41624a(WageImprovement.class, "8cbb8847c42e7acb06a6cc62be8003ed", i11);
        bLs = a6;
        fmVarArr[i11] = a6;
        int i12 = i11 + 1;
        _m_methods = fmVarArr;
        C1634Xv.m11725a((Object[]) CitizenImprovement._m_methods, (Object[]) _m_methods);
        ___iScriptClass = aUO.m18566a(WageImprovement.class, C1343Tb.class, _m_fields, _m_methods);
    }

    /* renamed from: a */
    private void m10297a(TaskletImpl pw) {
        bFf().mo5608dq().mo3197f(emU, pw);
    }

    private long bxo() {
        return ((WageImprovementType) getType()).bxv();
    }

    private float bxp() {
        return ((WageImprovementType) getType()).bOT();
    }

    private TaskletImpl bxq() {
        return (TaskletImpl) bFf().mo5608dq().mo3214p(emU);
    }

    private long bxr() {
        return bFf().mo5608dq().mo3213o(emV);
    }

    /* access modifiers changed from: private */
    public void bxt() {
        switch (bFf().mo6893i(emW)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, emW, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, emW, new Object[0]));
                break;
        }
        bxs();
    }

    /* renamed from: fJ */
    private void m10300fJ(long j) {
        throw new C6039afL();
    }

    /* renamed from: fK */
    private void m10301fK(long j) {
        bFf().mo5608dq().mo3184b(emV, j);
    }

    /* renamed from: ia */
    private void m10302ia(float f) {
        throw new C6039afL();
    }

    /* renamed from: S */
    public void mo10S() {
        super.mo10S();
    }

    /* renamed from: T */
    public C6494any mo11T() {
        return ___iScriptClass;
    }

    /* renamed from: U */
    public C2491fm[] mo12U() {
        return _m_methods;
    }

    /* renamed from: W */
    public Object mo13W() {
        return new C1343Tb(this);
    }

    /* renamed from: a */
    public Object mo14a(C0495Gr gr) {
        Object[] args = gr.getArgs();
        switch (gr.mo2417hq() - CitizenImprovement._m_methodCount) {
            case 0:
                bxs();
                return null;
            case 1:
                return new Boolean(m10299a((CitizenImprovement) args[0]));
            case 2:
                return new Long(bxu());
            case 3:
                apP();
                return null;
            case 4:
                apQ();
                return null;
            case 5:
                return apS();
            default:
                return super.mo14a(gr);
        }
    }

    /* renamed from: a */
    public void mo15a(Collection collection, C0495Gr gr) {
        super.mo15a(collection, gr);
    }

    public void apR() {
        switch (bFf().mo6893i(bLq)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, bLq, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, bLq, new Object[0]));
                break;
        }
        apQ();
    }

    public CitizenImprovement.C2184a apT() {
        switch (bFf().mo6893i(bLs)) {
            case 0:
                return null;
            case 2:
                return (CitizenImprovement.C2184a) bFf().mo5606d(new aCE(this, bLs, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, bLs, new Object[0]));
                break;
        }
        return apS();
    }

    /* renamed from: b */
    public boolean mo550b(CitizenImprovement cZVar) {
        switch (bFf().mo6893i(bLo)) {
            case 0:
                return false;
            case 2:
                return ((Boolean) bFf().mo5606d(new aCE(this, bLo, new Object[]{cZVar}))).booleanValue();
            case 3:
                bFf().mo5606d(new aCE(this, bLo, new Object[]{cZVar}));
                break;
        }
        return m10299a(cZVar);
    }

    public long bxv() {
        switch (bFf().mo6893i(emX)) {
            case 0:
                return 0;
            case 2:
                return ((Long) bFf().mo5606d(new aCE(this, emX, new Object[0]))).longValue();
            case 3:
                bFf().mo5606d(new aCE(this, emX, new Object[0]));
                break;
        }
        return bxu();
    }

    /* renamed from: c */
    public C5663aRz[] mo25c() {
        return _m_fields;
    }

    public void cancel() {
        switch (bFf().mo6893i(bLp)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, bLp, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, bLp, new Object[0]));
                break;
        }
        apP();
    }

    /* renamed from: a */
    public void mo5897a(WageImprovementType agp) {
        super.mo547a((CitizenImprovementType) agp);
    }

    @C0064Am(aul = "00085663a76ea7bc9d1096558141740c", aum = 0)
    private void bxs() {
        m10301fK(cVr());
        ala().aIS().mo12602b(mo17622KY(), bxo(), Bank.C1861a.CITIZENSHIP_WAGE, "Salary", 1);
    }

    @C0064Am(aul = "a1b1ab68a96baa66dc34237469d4f48d", aum = 0)
    /* renamed from: a */
    private boolean m10299a(CitizenImprovement cZVar) {
        if (!(cZVar instanceof WageImprovement) || bxo() <= ((WageImprovement) cZVar).bxv()) {
            return false;
        }
        return true;
    }

    @C0064Am(aul = "0451dec7b4eb1236d188a95c6a4bece1", aum = 0)
    private long bxu() {
        return bxo();
    }

    @C0064Am(aul = "34a62e4114c7780306d1539a16091877", aum = 0)
    private void apP() {
        bxq().stop();
        m10297a((TaskletImpl) null);
    }

    @C0064Am(aul = "e7a5453c098a6e60fe02b5e3990e1fe0", aum = 0)
    private void apQ() {
        WageTicker aVar = (WageTicker) bFf().mo6865M(WageTicker.class);
        aVar.mo5899a(this, (aDJ) this);
        m10297a((TaskletImpl) aVar);
        bxq().mo4704hk(86400.0f * bxp());
    }

    @C0064Am(aul = "8cbb8847c42e7acb06a6cc62be8003ed", aum = 0)
    private CitizenImprovement.C2184a apS() {
        return CitizenImprovement.C2184a.WAGE;
    }

    @C6485anp
    @C5511aMd
            /* renamed from: a.Uf$a */
    class WageTicker extends TaskletImpl implements C1616Xf {

        /* renamed from: JD */
        public static final C2491fm f1769JD = null;
        public static final int _m_fieldCount = 0;
        public static final C5663aRz[] _m_fields = null;
        public static final int _m_methodCount = 0;
        public static final C2491fm[] _m_methods = null;
        /* renamed from: aT */
        public static final C5663aRz f1770aT = null;
        public static final long serialVersionUID = 0;
        public static C6494any ___iScriptClass;
        @C0064Am(aul = "152c69a905bbcbcea81e574e5de863ba", aum = 0)
        static /* synthetic */ WageImprovement fTe;

        static {
            m10312V();
        }

        public WageTicker() {
            super((C5540aNg) null);
            super.mo10S();
        }

        public WageTicker(WageImprovement uf, aDJ adj) {
            super((C5540aNg) null);
            super._m_script_init(uf, adj);
        }

        public WageTicker(C5540aNg ang) {
            super(ang);
        }

        /* renamed from: V */
        static void m10312V() {
            serialVersionUID = (long) 1;
            _m_fieldCount = TaskletImpl._m_fieldCount + 1;
            _m_methodCount = TaskletImpl._m_methodCount + 1;
            int i = TaskletImpl._m_fieldCount;
            C5663aRz[] arzArr = new C5663aRz[(i + 1)];
            C5663aRz b = C5640aRc.m17844b(WageTicker.class, "152c69a905bbcbcea81e574e5de863ba", i);
            f1770aT = b;
            arzArr[i] = b;
            int i2 = i + 1;
            _m_fields = arzArr;
            C1634Xv.m11725a((Object[]) TaskletImpl._m_fields, (Object[]) _m_fields);
            int i3 = TaskletImpl._m_methodCount;
            C2491fm[] fmVarArr = new C2491fm[(i3 + 1)];
            C2491fm a = C4105zY.m41624a(WageTicker.class, "2d3d80c11fedcab7d6ad5e62b5d9ee54", i3);
            f1769JD = a;
            fmVarArr[i3] = a;
            int i4 = i3 + 1;
            _m_methods = fmVarArr;
            C1634Xv.m11725a((Object[]) TaskletImpl._m_methods, (Object[]) _m_methods);
            ___iScriptClass = aUO.m18566a(WageTicker.class, C6322aki.class, _m_fields, _m_methods);
        }

        /* renamed from: b */
        private void m10313b(WageImprovement uf) {
            bFf().mo5608dq().mo3197f(f1770aT, uf);
        }

        private WageImprovement cqw() {
            return (WageImprovement) bFf().mo5608dq().mo3214p(f1770aT);
        }

        /* renamed from: S */
        public void mo10S() {
            super.mo10S();
        }

        /* renamed from: T */
        public C6494any mo11T() {
            return ___iScriptClass;
        }

        /* renamed from: U */
        public C2491fm[] mo12U() {
            return _m_methods;
        }

        /* renamed from: W */
        public Object mo13W() {
            return new C6322aki(this);
        }

        /* renamed from: a */
        public Object mo14a(C0495Gr gr) {
            gr.getArgs();
            switch (gr.mo2417hq() - TaskletImpl._m_methodCount) {
                case 0:
                    m10314pi();
                    return null;
                default:
                    return super.mo14a(gr);
            }
        }

        /* renamed from: a */
        public void mo15a(Collection collection, C0495Gr gr) {
            super.mo15a(collection, gr);
        }

        /* renamed from: c */
        public C5663aRz[] mo25c() {
            return _m_fields;
        }

        /* access modifiers changed from: protected */
        /* renamed from: pj */
        public void mo667pj() {
            switch (bFf().mo6893i(f1769JD)) {
                case 0:
                    return;
                case 2:
                    bFf().mo5606d(new aCE(this, f1769JD, new Object[0]));
                    return;
                case 3:
                    bFf().mo5606d(new aCE(this, f1769JD, new Object[0]));
                    break;
            }
            m10314pi();
        }

        /* renamed from: a */
        public void mo5899a(WageImprovement uf, aDJ adj) {
            m10313b(uf);
            super.mo4706l(adj);
        }

        @C0064Am(aul = "2d3d80c11fedcab7d6ad5e62b5d9ee54", aum = 0)
        /* renamed from: pi */
        private void m10314pi() {
            cqw().bxt();
        }
    }
}
