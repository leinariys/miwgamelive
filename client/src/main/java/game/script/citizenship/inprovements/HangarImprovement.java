package game.script.citizenship.inprovements;

import game.network.message.externalizable.aCE;
import game.script.citizenship.CitizenImprovement;
import game.script.citizenship.CitizenImprovementType;
import game.script.hangar.Hangar;
import logic.baa.C1616Xf;
import logic.baa.C1634Xv;
import logic.baa.C5511aMd;
import logic.baa.C6485anp;
import logic.data.mbean.C1259Sb;
import logic.res.code.C5640aRc;
import logic.res.code.C5663aRz;
import logic.res.code.C6494any;
import logic.res.html.C2491fm;
import p001a.*;

import java.util.Collection;

@C2712iu
@C5511aMd
@C6485anp
/* renamed from: a.VO */
/* compiled from: a */
public class HangarImprovement extends CitizenImprovement implements C1616Xf {
    public static final C2491fm _f_onResurrect_0020_0028_0029V = null;
    public static final int _m_fieldCount = 0;
    public static final C5663aRz[] _m_fields = null;
    public static final int _m_methodCount = 0;
    public static final C2491fm[] _m_methods = null;
    public static final C2491fm bLo = null;
    public static final C2491fm bLp = null;
    public static final C2491fm bLq = null;
    public static final C2491fm bLs = null;
    public static final C5663aRz dNa = null;
    public static final C2491fm dNb = null;
    public static final long serialVersionUID = 0;
    public static C6494any ___iScriptClass;
    @C0064Am(aul = "86aaa23cce06f6e3abf18b8271113036", aum = 0)

    /* renamed from: zI */
    private static int f1866zI;

    static {
        m10608V();
    }

    public HangarImprovement() {
        super((C5540aNg) null);
        super.mo10S();
    }

    public HangarImprovement(HangarImprovementType od) {
        super((C5540aNg) null);
        super._m_script_init(od);
    }

    public HangarImprovement(C5540aNg ang) {
        super(ang);
    }

    /* renamed from: V */
    static void m10608V() {
        serialVersionUID = (long) 1;
        _m_fieldCount = CitizenImprovement._m_fieldCount + 1;
        _m_methodCount = CitizenImprovement._m_methodCount + 6;
        int i = CitizenImprovement._m_fieldCount;
        C5663aRz[] arzArr = new C5663aRz[(i + 1)];
        C5663aRz b = C5640aRc.m17844b(HangarImprovement.class, "86aaa23cce06f6e3abf18b8271113036", i);
        dNa = b;
        arzArr[i] = b;
        int i2 = i + 1;
        _m_fields = arzArr;
        C1634Xv.m11725a((Object[]) CitizenImprovement._m_fields, (Object[]) _m_fields);
        int i3 = CitizenImprovement._m_methodCount;
        C2491fm[] fmVarArr = new C2491fm[(i3 + 6)];
        C2491fm a = C4105zY.m41624a(HangarImprovement.class, "a5b59c6276cfdb3eb1a2c3e27d3d174e", i3);
        bLo = a;
        fmVarArr[i3] = a;
        int i4 = i3 + 1;
        C2491fm a2 = C4105zY.m41624a(HangarImprovement.class, "5a1f57f77ffe554f6f91887f36aa2c40", i4);
        bLq = a2;
        fmVarArr[i4] = a2;
        int i5 = i4 + 1;
        C2491fm a3 = C4105zY.m41624a(HangarImprovement.class, "ce5ba77c8f3ead61dd7cea9500329cad", i5);
        dNb = a3;
        fmVarArr[i5] = a3;
        int i6 = i5 + 1;
        C2491fm a4 = C4105zY.m41624a(HangarImprovement.class, "cec237678e6f82f567c640c162d1e707", i6);
        bLp = a4;
        fmVarArr[i6] = a4;
        int i7 = i6 + 1;
        C2491fm a5 = C4105zY.m41624a(HangarImprovement.class, "f4e6a2789c544122506e0f4fe3731584", i7);
        _f_onResurrect_0020_0028_0029V = a5;
        fmVarArr[i7] = a5;
        int i8 = i7 + 1;
        C2491fm a6 = C4105zY.m41624a(HangarImprovement.class, "180efd66fee45e231fb91abd28cbb25b", i8);
        bLs = a6;
        fmVarArr[i8] = a6;
        int i9 = i8 + 1;
        _m_methods = fmVarArr;
        C1634Xv.m11725a((Object[]) CitizenImprovement._m_methods, (Object[]) _m_methods);
        ___iScriptClass = aUO.m18566a(HangarImprovement.class, C1259Sb.class, _m_fields, _m_methods);
    }

    private int blU() {
        return ((HangarImprovementType) getType()).blW();
    }

    /* renamed from: mf */
    private void m10611mf(int i) {
        throw new C6039afL();
    }

    /* renamed from: S */
    public void mo10S() {
        super.mo10S();
    }

    /* renamed from: T */
    public C6494any mo11T() {
        return ___iScriptClass;
    }

    /* renamed from: U */
    public C2491fm[] mo12U() {
        return _m_methods;
    }

    /* renamed from: W */
    public Object mo13W() {
        return new C1259Sb(this);
    }

    /* renamed from: a */
    public Object mo14a(C0495Gr gr) {
        Object[] args = gr.getArgs();
        switch (gr.mo2417hq() - CitizenImprovement._m_methodCount) {
            case 0:
                return new Boolean(m10609a((CitizenImprovement) args[0]));
            case 1:
                apQ();
                return null;
            case 2:
                return new Integer(blV());
            case 3:
                apP();
                return null;
            case 4:
                m10610aG();
                return null;
            case 5:
                return apS();
            default:
                return super.mo14a(gr);
        }
    }

    /* renamed from: a */
    public void mo15a(Collection collection, C0495Gr gr) {
        super.mo15a(collection, gr);
    }

    /* renamed from: aH */
    public void mo70aH() {
        switch (bFf().mo6893i(_f_onResurrect_0020_0028_0029V)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, _f_onResurrect_0020_0028_0029V, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, _f_onResurrect_0020_0028_0029V, new Object[0]));
                break;
        }
        m10610aG();
    }

    public void apR() {
        switch (bFf().mo6893i(bLq)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, bLq, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, bLq, new Object[0]));
                break;
        }
        apQ();
    }

    public CitizenImprovement.C2184a apT() {
        switch (bFf().mo6893i(bLs)) {
            case 0:
                return null;
            case 2:
                return (CitizenImprovement.C2184a) bFf().mo5606d(new aCE(this, bLs, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, bLs, new Object[0]));
                break;
        }
        return apS();
    }

    /* renamed from: b */
    public boolean mo550b(CitizenImprovement cZVar) {
        switch (bFf().mo6893i(bLo)) {
            case 0:
                return false;
            case 2:
                return ((Boolean) bFf().mo5606d(new aCE(this, bLo, new Object[]{cZVar}))).booleanValue();
            case 3:
                bFf().mo5606d(new aCE(this, bLo, new Object[]{cZVar}));
                break;
        }
        return m10609a(cZVar);
    }

    public int blW() {
        switch (bFf().mo6893i(dNb)) {
            case 0:
                return 0;
            case 2:
                return ((Integer) bFf().mo5606d(new aCE(this, dNb, new Object[0]))).intValue();
            case 3:
                bFf().mo5606d(new aCE(this, dNb, new Object[0]));
                break;
        }
        return blV();
    }

    /* renamed from: c */
    public C5663aRz[] mo25c() {
        return _m_fields;
    }

    public void cancel() {
        switch (bFf().mo6893i(bLp)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, bLp, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, bLp, new Object[0]));
                break;
        }
        apP();
    }

    /* renamed from: a */
    public void mo6092a(HangarImprovementType od) {
        super.mo547a((CitizenImprovementType) od);
    }

    @C0064Am(aul = "a5b59c6276cfdb3eb1a2c3e27d3d174e", aum = 0)
    /* renamed from: a */
    private boolean m10609a(CitizenImprovement cZVar) {
        if (!(cZVar instanceof HangarImprovement) || blU() <= ((HangarImprovement) cZVar).blW()) {
            return false;
        }
        return true;
    }

    @C0064Am(aul = "5a1f57f77ffe554f6f91887f36aa2c40", aum = 0)
    private void apQ() {
        for (Hangar cI : mo17622KY().dxG()) {
            cI.mo20064cI(blU());
        }
    }

    @C0064Am(aul = "ce5ba77c8f3ead61dd7cea9500329cad", aum = 0)
    private int blV() {
        return blU();
    }

    @C0064Am(aul = "cec237678e6f82f567c640c162d1e707", aum = 0)
    private void apP() {
        for (Hangar cI : mo17622KY().dxG()) {
            cI.mo20064cI(2);
        }
    }

    @C0064Am(aul = "f4e6a2789c544122506e0f4fe3731584", aum = 0)
    /* renamed from: aG */
    private void m10610aG() {
        super.mo70aH();
    }

    @C0064Am(aul = "180efd66fee45e231fb91abd28cbb25b", aum = 0)
    private CitizenImprovement.C2184a apS() {
        return CitizenImprovement.C2184a.HANGAR;
    }
}
