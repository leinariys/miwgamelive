package game.script.citizenship;

import game.network.message.externalizable.aCE;
import game.script.resource.Asset;
import logic.baa.*;
import logic.data.mbean.C6071afr;
import logic.res.code.C5640aRc;
import logic.res.code.C5663aRz;
import logic.res.code.C6494any;
import logic.res.html.C2491fm;
import p001a.*;
import taikodom.infra.script.I18NString;

import java.util.Collection;

@C0566Hp
@C5511aMd
@C6485anp
/* renamed from: a.KT */
/* compiled from: a */
public abstract class CitizenImprovementType extends BaseCitizenImprovementType implements C1616Xf {

    /* renamed from: MN */
    public static final C2491fm f960MN = null;

    /* renamed from: NY */
    public static final C5663aRz f961NY = null;

    /* renamed from: Ob */
    public static final C2491fm f962Ob = null;

    /* renamed from: Oc */
    public static final C2491fm f963Oc = null;

    /* renamed from: QA */
    public static final C5663aRz f964QA = null;

    /* renamed from: QD */
    public static final C2491fm f965QD = null;

    /* renamed from: QE */
    public static final C2491fm f966QE = null;

    /* renamed from: QF */
    public static final C2491fm f967QF = null;

    /* renamed from: Qz */
    public static final C5663aRz f968Qz = null;
    public static final int _m_fieldCount = 0;
    public static final C5663aRz[] _m_fields = null;
    public static final int _m_methodCount = 0;
    public static final C2491fm[] _m_methods = null;
    public static final long serialVersionUID = 0;
    public static C6494any ___iScriptClass;
    @C0064Am(aul = "d58d8e51ac17338e1eef3c8ff4b8b202", aum = 2)

    /* renamed from: jn */
    private static Asset f969jn;
    @C0064Am(aul = "aa81013832de638c66e72a49c429f303", aum = 0)

    /* renamed from: nh */
    private static I18NString f970nh;
    @C0064Am(aul = "969fa6e847faa13cf49490588b9a7edc", aum = 1)

    /* renamed from: ni */
    private static I18NString f971ni;

    static {
        m6384V();
    }

    public CitizenImprovementType() {
        super((C5540aNg) null);
        super.mo10S();
    }

    public CitizenImprovementType(C5540aNg ang) {
        super(ang);
    }

    /* renamed from: V */
    static void m6384V() {
        serialVersionUID = (long) 1;
        _m_fieldCount = BaseCitizenImprovementType._m_fieldCount + 3;
        _m_methodCount = BaseCitizenImprovementType._m_methodCount + 6;
        int i = BaseCitizenImprovementType._m_fieldCount;
        C5663aRz[] arzArr = new C5663aRz[(i + 3)];
        C5663aRz b = C5640aRc.m17844b(CitizenImprovementType.class, "aa81013832de638c66e72a49c429f303", i);
        f964QA = b;
        arzArr[i] = b;
        int i2 = i + 1;
        C5663aRz b2 = C5640aRc.m17844b(CitizenImprovementType.class, "969fa6e847faa13cf49490588b9a7edc", i2);
        f968Qz = b2;
        arzArr[i2] = b2;
        int i3 = i2 + 1;
        C5663aRz b3 = C5640aRc.m17844b(CitizenImprovementType.class, "d58d8e51ac17338e1eef3c8ff4b8b202", i3);
        f961NY = b3;
        arzArr[i3] = b3;
        int i4 = i3 + 1;
        _m_fields = arzArr;
        C1634Xv.m11725a((Object[]) BaseCitizenImprovementType._m_fields, (Object[]) _m_fields);
        int i5 = BaseCitizenImprovementType._m_methodCount;
        C2491fm[] fmVarArr = new C2491fm[(i5 + 6)];
        C2491fm a = C4105zY.m41624a(CitizenImprovementType.class, "0ca31badc4c87406552410bd153373c4", i5);
        f966QE = a;
        fmVarArr[i5] = a;
        int i6 = i5 + 1;
        C2491fm a2 = C4105zY.m41624a(CitizenImprovementType.class, "e6333bf801a4e3ba09b88ed0de04d805", i6);
        f967QF = a2;
        fmVarArr[i6] = a2;
        int i7 = i6 + 1;
        C2491fm a3 = C4105zY.m41624a(CitizenImprovementType.class, "a2a3d444b72c7f6f5fa84a4b4f91bbab", i7);
        f960MN = a3;
        fmVarArr[i7] = a3;
        int i8 = i7 + 1;
        C2491fm a4 = C4105zY.m41624a(CitizenImprovementType.class, "16c0a7e6e3b04e3522f373c18c518dd9", i8);
        f965QD = a4;
        fmVarArr[i8] = a4;
        int i9 = i8 + 1;
        C2491fm a5 = C4105zY.m41624a(CitizenImprovementType.class, "4c47177deb35dc597132257ae027b780", i9);
        f962Ob = a5;
        fmVarArr[i9] = a5;
        int i10 = i9 + 1;
        C2491fm a6 = C4105zY.m41624a(CitizenImprovementType.class, "428dd310cb6b52d42e9c6447289d185d", i10);
        f963Oc = a6;
        fmVarArr[i10] = a6;
        int i11 = i10 + 1;
        _m_methods = fmVarArr;
        C1634Xv.m11725a((Object[]) BaseCitizenImprovementType._m_methods, (Object[]) _m_methods);
        ___iScriptClass = aUO.m18566a(CitizenImprovementType.class, C6071afr.class, _m_fields, _m_methods);
    }

    /* renamed from: bq */
    private void m6385bq(I18NString i18NString) {
        bFf().mo5608dq().mo3197f(f968Qz, i18NString);
    }

    /* renamed from: br */
    private void m6386br(I18NString i18NString) {
        bFf().mo5608dq().mo3197f(f964QA, i18NString);
    }

    /* renamed from: sG */
    private Asset m6390sG() {
        return (Asset) bFf().mo5608dq().mo3214p(f961NY);
    }

    /* renamed from: t */
    private void m6392t(Asset tCVar) {
        bFf().mo5608dq().mo3197f(f961NY, tCVar);
    }

    /* renamed from: vS */
    private I18NString m6394vS() {
        return (I18NString) bFf().mo5608dq().mo3214p(f968Qz);
    }

    /* renamed from: vT */
    private I18NString m6395vT() {
        return (I18NString) bFf().mo5608dq().mo3214p(f964QA);
    }

    /* renamed from: T */
    public C6494any mo11T() {
        return ___iScriptClass;
    }

    /* renamed from: U */
    public C2491fm[] mo12U() {
        return _m_methods;
    }

    /* renamed from: W */
    public Object mo13W() {
        return new C6071afr(this);
    }

    /* renamed from: a */
    public Object mo14a(C0495Gr gr) {
        Object[] args = gr.getArgs();
        switch (gr.mo2417hq() - BaseCitizenImprovementType._m_methodCount) {
            case 0:
                return m6396vV();
            case 1:
                m6388bu((I18NString) args[0]);
                return null;
            case 2:
                return m6389rO();
            case 3:
                m6387bs((I18NString) args[0]);
                return null;
            case 4:
                return m6391sJ();
            case 5:
                m6393u((Asset) args[0]);
                return null;
            default:
                return super.mo14a(gr);
        }
    }

    /* renamed from: a */
    public void mo15a(Collection collection, C0495Gr gr) {
        super.mo15a(collection, gr);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Title")
    /* renamed from: bt */
    public void mo3545bt(I18NString i18NString) {
        switch (bFf().mo6893i(f965QD)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f965QD, new Object[]{i18NString}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f965QD, new Object[]{i18NString}));
                break;
        }
        m6387bs(i18NString);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Description")
    /* renamed from: bv */
    public void mo3546bv(I18NString i18NString) {
        switch (bFf().mo6893i(f967QF)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f967QF, new Object[]{i18NString}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f967QF, new Object[]{i18NString}));
                break;
        }
        m6388bu(i18NString);
    }

    /* renamed from: c */
    public C5663aRz[] mo25c() {
        return _m_fields;
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Title")
    /* renamed from: rP */
    public I18NString mo3547rP() {
        switch (bFf().mo6893i(f960MN)) {
            case 0:
                return null;
            case 2:
                return (I18NString) bFf().mo5606d(new aCE(this, f960MN, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f960MN, new Object[0]));
                break;
        }
        return m6389rO();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Icon")
    /* renamed from: sK */
    public Asset mo3548sK() {
        switch (bFf().mo6893i(f962Ob)) {
            case 0:
                return null;
            case 2:
                return (Asset) bFf().mo5606d(new aCE(this, f962Ob, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f962Ob, new Object[0]));
                break;
        }
        return m6391sJ();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Icon")
    /* renamed from: v */
    public void mo3549v(Asset tCVar) {
        switch (bFf().mo6893i(f963Oc)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f963Oc, new Object[]{tCVar}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f963Oc, new Object[]{tCVar}));
                break;
        }
        m6393u(tCVar);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Description")
    /* renamed from: vW */
    public I18NString mo3550vW() {
        switch (bFf().mo6893i(f966QE)) {
            case 0:
                return null;
            case 2:
                return (I18NString) bFf().mo5606d(new aCE(this, f966QE, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f966QE, new Object[0]));
                break;
        }
        return m6396vV();
    }

    /* renamed from: S */
    public void mo10S() {
        super.mo10S();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Description")
    @C0064Am(aul = "0ca31badc4c87406552410bd153373c4", aum = 0)
    /* renamed from: vV */
    private I18NString m6396vV() {
        return m6395vT();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Description")
    @C0064Am(aul = "e6333bf801a4e3ba09b88ed0de04d805", aum = 0)
    /* renamed from: bu */
    private void m6388bu(I18NString i18NString) {
        m6386br(i18NString);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Title")
    @C0064Am(aul = "a2a3d444b72c7f6f5fa84a4b4f91bbab", aum = 0)
    /* renamed from: rO */
    private I18NString m6389rO() {
        return m6394vS();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Title")
    @C0064Am(aul = "16c0a7e6e3b04e3522f373c18c518dd9", aum = 0)
    /* renamed from: bs */
    private void m6387bs(I18NString i18NString) {
        m6385bq(i18NString);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Icon")
    @C0064Am(aul = "4c47177deb35dc597132257ae027b780", aum = 0)
    /* renamed from: sJ */
    private Asset m6391sJ() {
        return m6390sG();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Icon")
    @C0064Am(aul = "428dd310cb6b52d42e9c6447289d185d", aum = 0)
    /* renamed from: u */
    private void m6393u(Asset tCVar) {
        m6392t(tCVar);
    }
}
