package game.script.citizenship;

import game.network.message.externalizable.aCE;
import game.network.message.serializable.C3438ra;
import logic.baa.*;
import logic.data.mbean.C5609aPx;
import logic.res.code.C5640aRc;
import logic.res.code.C5663aRz;
import logic.res.code.C6494any;
import logic.res.html.C2491fm;
import p001a.*;

import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.UUID;

@C5829abJ("1.0.0")
@C5511aMd
@C6485anp
/* renamed from: a.Sk */
/* compiled from: a */
public class CitizenshipOffice extends TaikodomObject implements C0468GU, C1616Xf, aOW {

    /* renamed from: Do */
    public static final C2491fm f1554Do = null;
    public static final int _m_fieldCount = 0;
    public static final C5663aRz[] _m_fields = null;
    public static final int _m_methodCount = 0;
    public static final C2491fm[] _m_methods = null;
    /* renamed from: bL */
    public static final C5663aRz f1556bL = null;
    /* renamed from: bM */
    public static final C5663aRz f1557bM = null;
    /* renamed from: bN */
    public static final C2491fm f1558bN = null;
    /* renamed from: bO */
    public static final C2491fm f1559bO = null;
    /* renamed from: bP */
    public static final C2491fm f1560bP = null;
    /* renamed from: bQ */
    public static final C2491fm f1561bQ = null;
    public static final C2491fm cPt = null;
    public static final C5663aRz edM = null;
    public static final C2491fm edN = null;
    public static final C2491fm edO = null;
    public static final C2491fm edP = null;
    public static final long serialVersionUID = 0;
    public static C6494any ___iScriptClass;
    @C0064Am(aul = "c73e27256ab0980240e199affff14a5b", aum = 1)

    /* renamed from: bK */
    private static UUID f1555bK;
    @C0064Am(aul = "5f8d6be7b6affd0f8b908f241f4d443d", aum = 0)
    private static C3438ra<CitizenshipPack> edL;
    @C0064Am(aul = "33eaa92c84142445e50f53bdfc4c6539", aum = 2)
    private static String handle;

    static {
        m9550V();
    }

    public CitizenshipOffice() {
        super((C5540aNg) null);
        super.mo10S();
    }

    public CitizenshipOffice(C5540aNg ang) {
        super(ang);
    }

    /* renamed from: V */
    static void m9550V() {
        serialVersionUID = (long) 1;
        _m_fieldCount = TaikodomObject._m_fieldCount + 3;
        _m_methodCount = TaikodomObject._m_methodCount + 9;
        int i = TaikodomObject._m_fieldCount;
        C5663aRz[] arzArr = new C5663aRz[(i + 3)];
        C5663aRz b = C5640aRc.m17844b(CitizenshipOffice.class, "5f8d6be7b6affd0f8b908f241f4d443d", i);
        edM = b;
        arzArr[i] = b;
        int i2 = i + 1;
        C5663aRz b2 = C5640aRc.m17844b(CitizenshipOffice.class, "c73e27256ab0980240e199affff14a5b", i2);
        f1556bL = b2;
        arzArr[i2] = b2;
        int i3 = i2 + 1;
        C5663aRz b3 = C5640aRc.m17844b(CitizenshipOffice.class, "33eaa92c84142445e50f53bdfc4c6539", i3);
        f1557bM = b3;
        arzArr[i3] = b3;
        int i4 = i3 + 1;
        _m_fields = arzArr;
        C1634Xv.m11725a((Object[]) TaikodomObject._m_fields, (Object[]) _m_fields);
        int i5 = TaikodomObject._m_methodCount;
        C2491fm[] fmVarArr = new C2491fm[(i5 + 9)];
        C2491fm a = C4105zY.m41624a(CitizenshipOffice.class, "b388ce3d0052efeab4722b86d4db0fdd", i5);
        f1558bN = a;
        fmVarArr[i5] = a;
        int i6 = i5 + 1;
        C2491fm a2 = C4105zY.m41624a(CitizenshipOffice.class, "eb0accadc53d281389ada86d03a21b5c", i6);
        f1559bO = a2;
        fmVarArr[i6] = a2;
        int i7 = i6 + 1;
        C2491fm a3 = C4105zY.m41624a(CitizenshipOffice.class, "6d54089e04f4206faef87f7704266181", i7);
        f1560bP = a3;
        fmVarArr[i7] = a3;
        int i8 = i7 + 1;
        C2491fm a4 = C4105zY.m41624a(CitizenshipOffice.class, "5b138341e32473b067a899cb8f4d9491", i8);
        f1561bQ = a4;
        fmVarArr[i8] = a4;
        int i9 = i8 + 1;
        C2491fm a5 = C4105zY.m41624a(CitizenshipOffice.class, "99eb7188d3b6baf8e11b4d88408b69af", i9);
        edN = a5;
        fmVarArr[i9] = a5;
        int i10 = i9 + 1;
        C2491fm a6 = C4105zY.m41624a(CitizenshipOffice.class, "1f227787a35589abef3b8a0fe12bff4a", i10);
        edO = a6;
        fmVarArr[i10] = a6;
        int i11 = i10 + 1;
        C2491fm a7 = C4105zY.m41624a(CitizenshipOffice.class, "8e46703b0a3f9cb0c4073022e657ddc0", i11);
        edP = a7;
        fmVarArr[i11] = a7;
        int i12 = i11 + 1;
        C2491fm a8 = C4105zY.m41624a(CitizenshipOffice.class, "bd2a3ca9549f0b7bd3c6094ac53bab39", i12);
        cPt = a8;
        fmVarArr[i12] = a8;
        int i13 = i12 + 1;
        C2491fm a9 = C4105zY.m41624a(CitizenshipOffice.class, "009fab011c456b512ff950b5c2823404", i13);
        f1554Do = a9;
        fmVarArr[i13] = a9;
        int i14 = i13 + 1;
        _m_methods = fmVarArr;
        C1634Xv.m11725a((Object[]) TaikodomObject._m_methods, (Object[]) _m_methods);
        ___iScriptClass = aUO.m18566a(CitizenshipOffice.class, C5609aPx.class, _m_fields, _m_methods);
    }

    /* renamed from: a */
    private void m9552a(String str) {
        bFf().mo5608dq().mo3197f(f1557bM, str);
    }

    /* renamed from: a */
    private void m9553a(UUID uuid) {
        bFf().mo5608dq().mo3197f(f1556bL, uuid);
    }

    /* renamed from: an */
    private UUID m9554an() {
        return (UUID) bFf().mo5608dq().mo3214p(f1556bL);
    }

    /* renamed from: ao */
    private String m9555ao() {
        return (String) bFf().mo5608dq().mo3214p(f1557bM);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Handle")
    @C0064Am(aul = "5b138341e32473b067a899cb8f4d9491", aum = 0)
    @C5566aOg
    /* renamed from: b */
    private void m9558b(String str) {
        throw new aWi(new aCE(this, f1561bQ, new Object[]{str}));
    }

    /* renamed from: bq */
    private void m9560bq(C3438ra raVar) {
        bFf().mo5608dq().mo3197f(edM, raVar);
    }

    private C3438ra bts() {
        return (C3438ra) bFf().mo5608dq().mo3214p(edM);
    }

    /* renamed from: c */
    private void m9561c(UUID uuid) {
        switch (bFf().mo6893i(f1559bO)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f1559bO, new Object[]{uuid}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f1559bO, new Object[]{uuid}));
                break;
        }
        m9559b(uuid);
    }

    @C0064Am(aul = "99eb7188d3b6baf8e11b4d88408b69af", aum = 0)
    @C5566aOg
    @C2499fr
    /* renamed from: h */
    private boolean m9562h(CitizenshipPack acu) {
        throw new aWi(new aCE(this, edN, new Object[]{acu}));
    }

    @C3248pc(aYR = C3248pc.C3250b.ADDER, displayName = "Citizenship Pack")
    @C0064Am(aul = "1f227787a35589abef3b8a0fe12bff4a", aum = 0)
    @C5566aOg
    /* renamed from: j */
    private void m9563j(CitizenshipPack acu) {
        throw new aWi(new aCE(this, edO, new Object[]{acu}));
    }

    @C3248pc(aYR = C3248pc.C3250b.REMOVER, displayName = "Citizenship Pack")
    @C0064Am(aul = "8e46703b0a3f9cb0c4073022e657ddc0", aum = 0)
    @C5566aOg
    /* renamed from: l */
    private void m9564l(CitizenshipPack acu) {
        throw new aWi(new aCE(this, edP, new Object[]{acu}));
    }

    /* renamed from: T */
    public C6494any mo11T() {
        return ___iScriptClass;
    }

    /* renamed from: U */
    public C2491fm[] mo12U() {
        return _m_methods;
    }

    /* renamed from: W */
    public Object mo13W() {
        return new C5609aPx(this);
    }

    /* renamed from: a */
    public Object mo14a(C0495Gr gr) {
        Object[] args = gr.getArgs();
        switch (gr.mo2417hq() - TaikodomObject._m_methodCount) {
            case 0:
                return m9556ap();
            case 1:
                m9559b((UUID) args[0]);
                return null;
            case 2:
                return m9557ar();
            case 3:
                m9558b((String) args[0]);
                return null;
            case 4:
                return new Boolean(m9562h((CitizenshipPack) args[0]));
            case 5:
                m9563j((CitizenshipPack) args[0]);
                return null;
            case 6:
                m9564l((CitizenshipPack) args[0]);
                return null;
            case 7:
                return aLv();
            case 8:
                m9551a((C0665JT) args[0]);
                return null;
            default:
                return super.mo14a(gr);
        }
    }

    /* renamed from: a */
    public void mo15a(Collection collection, C0495Gr gr) {
        super.mo15a(collection, gr);
    }

    @C3248pc(aYR = C3248pc.C3250b.ITERATOR, displayName = "Citizenship Pack")
    public List<CitizenshipPack> aLw() {
        switch (bFf().mo6893i(cPt)) {
            case 0:
                return null;
            case 2:
                return (List) bFf().mo5606d(new aCE(this, cPt, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, cPt, new Object[0]));
                break;
        }
        return aLv();
    }

    /* renamed from: aq */
    public UUID mo18aq() {
        switch (bFf().mo6893i(f1558bN)) {
            case 0:
                return null;
            case 2:
                return (UUID) bFf().mo5606d(new aCE(this, f1558bN, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f1558bN, new Object[0]));
                break;
        }
        return m9556ap();
    }

    /* renamed from: b */
    public void mo24b(C0665JT jt) {
        switch (bFf().mo6893i(f1554Do)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f1554Do, new Object[]{jt}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f1554Do, new Object[]{jt}));
                break;
        }
        m9551a(jt);
    }

    /* renamed from: c */
    public C5663aRz[] mo25c() {
        return _m_fields;
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Handle")
    public String getHandle() {
        switch (bFf().mo6893i(f1560bP)) {
            case 0:
                return null;
            case 2:
                return (String) bFf().mo5606d(new aCE(this, f1560bP, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f1560bP, new Object[0]));
                break;
        }
        return m9557ar();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Handle")
    @C5566aOg
    public void setHandle(String str) {
        switch (bFf().mo6893i(f1561bQ)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f1561bQ, new Object[]{str}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f1561bQ, new Object[]{str}));
                break;
        }
        m9558b(str);
    }

    @C5566aOg
    @C2499fr
    /* renamed from: i */
    public boolean mo5462i(CitizenshipPack acu) {
        switch (bFf().mo6893i(edN)) {
            case 0:
                return false;
            case 2:
                return ((Boolean) bFf().mo5606d(new aCE(this, edN, new Object[]{acu}))).booleanValue();
            case 3:
                bFf().mo5606d(new aCE(this, edN, new Object[]{acu}));
                break;
        }
        return m9562h(acu);
    }

    @C3248pc(aYR = C3248pc.C3250b.ADDER, displayName = "Citizenship Pack")
    @C5566aOg
    /* renamed from: k */
    public void mo5463k(CitizenshipPack acu) {
        switch (bFf().mo6893i(edO)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, edO, new Object[]{acu}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, edO, new Object[]{acu}));
                break;
        }
        m9563j(acu);
    }

    @C3248pc(aYR = C3248pc.C3250b.REMOVER, displayName = "Citizenship Pack")
    @C5566aOg
    /* renamed from: m */
    public void mo5464m(CitizenshipPack acu) {
        switch (bFf().mo6893i(edP)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, edP, new Object[]{acu}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, edP, new Object[]{acu}));
                break;
        }
        m9564l(acu);
    }

    /* renamed from: S */
    public void mo10S() {
        super.mo10S();
    }

    @C0064Am(aul = "b388ce3d0052efeab4722b86d4db0fdd", aum = 0)
    /* renamed from: ap */
    private UUID m9556ap() {
        return m9554an();
    }

    @C0064Am(aul = "eb0accadc53d281389ada86d03a21b5c", aum = 0)
    /* renamed from: b */
    private void m9559b(UUID uuid) {
        m9553a(uuid);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Handle")
    @C0064Am(aul = "6d54089e04f4206faef87f7704266181", aum = 0)
    /* renamed from: ar */
    private String m9557ar() {
        return m9555ao();
    }

    @C3248pc(aYR = C3248pc.C3250b.ITERATOR, displayName = "Citizenship Pack")
    @C0064Am(aul = "bd2a3ca9549f0b7bd3c6094ac53bab39", aum = 0)
    private List<CitizenshipPack> aLv() {
        return Collections.unmodifiableList(bts());
    }

    /* JADX WARNING: type inference failed for: r0v8, types: [a.Xf] */
    /* JADX WARNING: Multi-variable type inference failed */
    @p001a.C0064Am(aul = "009fab011c456b512ff950b5c2823404", aum = 0)
    /* renamed from: a */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void m9551a(p001a.C0665JT r7) {
        /*
            r6 = this;
            r1 = 0
            r0 = 1
            boolean r0 = r7.mo3117j(r0, r1, r1)
            if (r0 == 0) goto L_0x001b
            java.lang.String r0 = "citizenships"
            java.lang.Object r0 = r7.get(r0)
            java.util.List r0 = (java.util.List) r0
            r2 = 0
            java.util.Iterator r3 = r0.iterator()
        L_0x0015:
            boolean r0 = r3.hasNext()
            if (r0 != 0) goto L_0x001c
        L_0x001b:
            return
        L_0x001c:
            java.lang.Object r0 = r3.next()
            a.aMe r0 = (game.network.message.serializable.C5512aMe) r0
            a.Xf r0 = r0.mo10098yz()
            r1 = r0
            a.adL r1 = (p001a.C5935adL) r1
            if (r2 != 0) goto L_0x002c
            r2 = r1
        L_0x002c:
            r0 = r6
            a.Xf r0 = (logic.baa.C1616Xf) r0
            a.arL r0 = r0.bFf()
            java.lang.Class<a.acu> r4 = p001a.C5918acu.class
            a.Xf r0 = r0.mo6865M(r4)
            a.acu r0 = (p001a.C5918acu) r0
            r0.mo10S()
            float r4 = r1.bOT()
            r0.mo12736iW(r4)
            long r4 = r1.mo12810rZ()
            r0.mo12733K(r4)
            r0.mo12737o(r2)
            java.lang.StringBuilder r4 = new java.lang.StringBuilder
            java.lang.String r5 = r1.getHandle()
            java.lang.String r5 = java.lang.String.valueOf(r5)
            r4.<init>(r5)
            java.lang.String r5 = "_"
            java.lang.StringBuilder r4 = r4.append(r5)
            float r1 = r1.bOT()
            int r1 = (int) r1
            java.lang.StringBuilder r1 = r4.append(r1)
            java.lang.String r4 = "_pak"
            java.lang.StringBuilder r1 = r1.append(r4)
            java.lang.String r1 = r1.toString()
            r0.setHandle(r1)
            a.ra r1 = r6.bts()
            r1.add(r0)
            a.DN r1 = r6.ala()
            r1.mo1433b((p001a.C5918acu) r0)
            goto L_0x0015
        */
        throw new UnsupportedOperationException("Method not decompiled: p001a.C1268Sk.m9551a(a.JT):void");
    }
}
