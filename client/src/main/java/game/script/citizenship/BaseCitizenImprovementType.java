package game.script.citizenship;

import game.script.template.BaseTaikodomContent;
import logic.baa.C1616Xf;
import logic.baa.C1634Xv;
import logic.baa.C5511aMd;
import logic.baa.C6485anp;
import logic.data.mbean.C1183RV;
import logic.res.code.C5663aRz;
import logic.res.code.C6494any;
import logic.res.html.C2491fm;
import p001a.C0495Gr;
import p001a.C5540aNg;
import p001a.aUO;

import java.util.Collection;

@C5511aMd
@C6485anp
/* renamed from: a.fj */
/* compiled from: a */
public abstract class BaseCitizenImprovementType extends BaseTaikodomContent implements C1616Xf {
    public static final int _m_fieldCount = 0;
    public static final C5663aRz[] _m_fields = null;
    public static final int _m_methodCount = 0;
    public static final C2491fm[] _m_methods = null;
    public static final long serialVersionUID = 0;
    public static C6494any ___iScriptClass;

    static {
        m31291V();
    }

    public BaseCitizenImprovementType() {
        super((C5540aNg) null);
        super.mo10S();
    }

    public BaseCitizenImprovementType(C5540aNg ang) {
        super(ang);
    }

    /* renamed from: V */
    static void m31291V() {
        serialVersionUID = (long) 1;
        _m_fieldCount = BaseTaikodomContent._m_fieldCount + 0;
        _m_methodCount = BaseTaikodomContent._m_methodCount + 0;
        _m_fields = new C5663aRz[(BaseTaikodomContent._m_fieldCount + 0)];
        C1634Xv.m11725a((Object[]) BaseTaikodomContent._m_fields, (Object[]) _m_fields);
        _m_methods = new C2491fm[(BaseTaikodomContent._m_methodCount + 0)];
        C1634Xv.m11725a((Object[]) BaseTaikodomContent._m_methods, (Object[]) _m_methods);
        ___iScriptClass = aUO.m18566a(BaseCitizenImprovementType.class, C1183RV.class, _m_fields, _m_methods);
    }

    /* renamed from: T */
    public C6494any mo11T() {
        return ___iScriptClass;
    }

    /* renamed from: U */
    public C2491fm[] mo12U() {
        return _m_methods;
    }

    /* renamed from: W */
    public Object mo13W() {
        return new C1183RV(this);
    }

    /* renamed from: a */
    public Object mo14a(C0495Gr gr) {
        return super.mo14a(gr);
    }

    /* renamed from: a */
    public void mo15a(Collection collection, C0495Gr gr) {
        super.mo15a(collection, gr);
    }

    /* renamed from: c */
    public C5663aRz[] mo25c() {
        return _m_fields;
    }

    /* renamed from: S */
    public void mo10S() {
        super.mo10S();
    }
}
