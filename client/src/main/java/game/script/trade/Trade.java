package game.script.trade;

import game.network.message.externalizable.aCE;
import game.script.TaikodomObject;
import game.script.item.Item;
import game.script.player.Player;
import logic.aaa.C1506WA;
import logic.baa.*;
import logic.data.link.C1163RD;
import logic.data.link.C3161oY;
import logic.data.mbean.C5769aaB;
import logic.res.code.C5640aRc;
import logic.res.code.C5663aRz;
import logic.res.code.C6494any;
import logic.res.html.C2491fm;
import p001a.*;

import java.util.Collection;

@C5511aMd
@C6485anp
/* renamed from: a.Td */
/* compiled from: a */
public class Trade extends TaikodomObject implements C1616Xf, C3161oY.C3162a {
    public static final C2491fm _f_dispose_0020_0028_0029V = null;
    /* renamed from: _f_onObjectDisposed_0020_0028Ltaikodom_002finfra_002fscript_002fScriptObject_003b_0029V */
    public static final C2491fm f1716x13860637 = null;
    public static final int _m_fieldCount = 0;
    public static final C5663aRz[] _m_fields = null;
    public static final int _m_methodCount = 0;
    public static final C2491fm[] _m_methods = null;
    public static final C5663aRz ehb = null;
    public static final C5663aRz ehd = null;
    public static final C5663aRz ehf = null;
    public static final C2491fm ehg = null;
    public static final C2491fm ehh = null;
    public static final C2491fm ehi = null;
    public static final C2491fm ehj = null;
    public static final C2491fm ehk = null;
    public static final C2491fm ehl = null;
    public static final C2491fm ehm = null;
    public static final C2491fm ehn = null;
    public static final C2491fm eho = null;
    public static final C2491fm ehp = null;
    public static final C2491fm ehq = null;
    public static final C2491fm ehr = null;
    public static final C2491fm ehs = null;
    public static final C2491fm eht = null;
    public static final C2491fm ehu = null;
    public static final C2491fm ehv = null;
    public static final C2491fm ehw = null;
    public static final C2491fm ehx = null;
    public static final C2491fm ehy = null;
    public static final C2491fm ehz = null;
    private static final long serialVersionUID = -7075031004133630902L;
    public static C6494any ___iScriptClass = null;
    @C0064Am(aul = "0cf8f05ee42e1a174fb9b06335132856", aum = 0)
    private static TradePlayerStatus eha = null;
    @C0064Am(aul = "865bef9b4f790f13adc3b65b0bef4e5e", aum = 1)
    private static TradePlayerStatus ehc = null;
    @C0064Am(aul = "c3ad205b1ca9462f1c83442ece61bd07", aum = 2)
    private static TradeTrigger ehe = null;

    static {
        m10045V();
    }

    public Trade() {
        super((C5540aNg) null);
        super.mo10S();
    }

    public Trade(C5540aNg ang) {
        super(ang);
    }

    public Trade(Player aku, Player aku2) {
        super((C5540aNg) null);
        super._m_script_init(aku, aku2);
    }

    /* renamed from: V */
    static void m10045V() {
        _m_fieldCount = TaikodomObject._m_fieldCount + 3;
        _m_methodCount = TaikodomObject._m_methodCount + 22;
        int i = TaikodomObject._m_fieldCount;
        C5663aRz[] arzArr = new C5663aRz[(i + 3)];
        C5663aRz b = C5640aRc.m17844b(Trade.class, "0cf8f05ee42e1a174fb9b06335132856", i);
        ehb = b;
        arzArr[i] = b;
        int i2 = i + 1;
        C5663aRz b2 = C5640aRc.m17844b(Trade.class, "865bef9b4f790f13adc3b65b0bef4e5e", i2);
        ehd = b2;
        arzArr[i2] = b2;
        int i3 = i2 + 1;
        C5663aRz b3 = C5640aRc.m17844b(Trade.class, "c3ad205b1ca9462f1c83442ece61bd07", i3);
        ehf = b3;
        arzArr[i3] = b3;
        int i4 = i3 + 1;
        _m_fields = arzArr;
        C1634Xv.m11725a((Object[]) TaikodomObject._m_fields, (Object[]) _m_fields);
        int i5 = TaikodomObject._m_methodCount;
        C2491fm[] fmVarArr = new C2491fm[(i5 + 22)];
        C2491fm a = C4105zY.m41624a(Trade.class, "26d82719c05b82df1b35841d65d7afdd", i5);
        ehg = a;
        fmVarArr[i5] = a;
        int i6 = i5 + 1;
        C2491fm a2 = C4105zY.m41624a(Trade.class, "830f4efb45d86b810b042fd2dadd7f74", i6);
        ehh = a2;
        fmVarArr[i6] = a2;
        int i7 = i6 + 1;
        C2491fm a3 = C4105zY.m41624a(Trade.class, "0a8967365ced10958692dec89655f6e7", i7);
        ehi = a3;
        fmVarArr[i7] = a3;
        int i8 = i7 + 1;
        C2491fm a4 = C4105zY.m41624a(Trade.class, "d5e10797fc33f65d8d30ee63b3e8c80c", i8);
        ehj = a4;
        fmVarArr[i8] = a4;
        int i9 = i8 + 1;
        C2491fm a5 = C4105zY.m41624a(Trade.class, "f58cb71b27c6e64be40d5035299a81ee", i9);
        ehk = a5;
        fmVarArr[i9] = a5;
        int i10 = i9 + 1;
        C2491fm a6 = C4105zY.m41624a(Trade.class, "8dac046461333df1f867f7a2829fc102", i10);
        ehl = a6;
        fmVarArr[i10] = a6;
        int i11 = i10 + 1;
        C2491fm a7 = C4105zY.m41624a(Trade.class, "81319d36c45680bf2e851cbeace4e698", i11);
        ehm = a7;
        fmVarArr[i11] = a7;
        int i12 = i11 + 1;
        C2491fm a8 = C4105zY.m41624a(Trade.class, "8bef5799f1f268c9b1fc3936e4d0b624", i12);
        ehn = a8;
        fmVarArr[i12] = a8;
        int i13 = i12 + 1;
        C2491fm a9 = C4105zY.m41624a(Trade.class, "665b02a021fe01574d88f14ca0be077e", i13);
        eho = a9;
        fmVarArr[i13] = a9;
        int i14 = i13 + 1;
        C2491fm a10 = C4105zY.m41624a(Trade.class, "7f68dd97008e9b493a37f6d071d11142", i14);
        ehp = a10;
        fmVarArr[i14] = a10;
        int i15 = i14 + 1;
        C2491fm a11 = C4105zY.m41624a(Trade.class, "6123540433ea0c4bcf4f68e63c77f102", i15);
        ehq = a11;
        fmVarArr[i15] = a11;
        int i16 = i15 + 1;
        C2491fm a12 = C4105zY.m41624a(Trade.class, "07797a27b9db0e5fc0b9daa7a879e449", i16);
        ehr = a12;
        fmVarArr[i16] = a12;
        int i17 = i16 + 1;
        C2491fm a13 = C4105zY.m41624a(Trade.class, "9049acd1ce9c522bff6aa3e0be010e95", i17);
        ehs = a13;
        fmVarArr[i17] = a13;
        int i18 = i17 + 1;
        C2491fm a14 = C4105zY.m41624a(Trade.class, "87cb2a1b6182ee92c9b56bee7a2a70ca", i18);
        eht = a14;
        fmVarArr[i18] = a14;
        int i19 = i18 + 1;
        C2491fm a15 = C4105zY.m41624a(Trade.class, "be2e4fb8fc8bbb8ca26ae9646722e74a", i19);
        ehu = a15;
        fmVarArr[i19] = a15;
        int i20 = i19 + 1;
        C2491fm a16 = C4105zY.m41624a(Trade.class, "b48728b63e5669b3fc772daa0626e86d", i20);
        ehv = a16;
        fmVarArr[i20] = a16;
        int i21 = i20 + 1;
        C2491fm a17 = C4105zY.m41624a(Trade.class, "88458da23fbccf162d7c7cee730ef431", i21);
        ehw = a17;
        fmVarArr[i21] = a17;
        int i22 = i21 + 1;
        C2491fm a18 = C4105zY.m41624a(Trade.class, "db392f44d32892729996e92fc1551804", i22);
        ehx = a18;
        fmVarArr[i22] = a18;
        int i23 = i22 + 1;
        C2491fm a19 = C4105zY.m41624a(Trade.class, "721bb457bc18f81b5e99a1a6c61769b1", i23);
        _f_dispose_0020_0028_0029V = a19;
        fmVarArr[i23] = a19;
        int i24 = i23 + 1;
        C2491fm a20 = C4105zY.m41624a(Trade.class, "da0b2fd7312a8aa63bee08e4b83e5b3e", i24);
        f1716x13860637 = a20;
        fmVarArr[i24] = a20;
        int i25 = i24 + 1;
        C2491fm a21 = C4105zY.m41624a(Trade.class, "e9d9517e053f941c2e6ff34fdba431b8", i25);
        ehy = a21;
        fmVarArr[i25] = a21;
        int i26 = i25 + 1;
        C2491fm a22 = C4105zY.m41624a(Trade.class, "59caf6758590409323dd62dd74fa3fe1", i26);
        ehz = a22;
        fmVarArr[i26] = a22;
        int i27 = i26 + 1;
        _m_methods = fmVarArr;
        C1634Xv.m11725a((Object[]) TaikodomObject._m_methods, (Object[]) _m_methods);
        ___iScriptClass = aUO.m18566a(Trade.class, C5769aaB.class, _m_fields, _m_methods);
    }

    /* renamed from: a */
    private void m10047a(TradeTrigger adg) {
        bFf().mo5608dq().mo3197f(ehf, adg);
    }

    /* renamed from: a */
    private void m10048a(TradePlayerStatus aiVar) {
        bFf().mo5608dq().mo3197f(ehb, aiVar);
    }

    @C4034yP
    @C5566aOg
    @C0064Am(aul = "9049acd1ce9c522bff6aa3e0be010e95", aum = 0)
    @C2499fr
    /* renamed from: a */
    private void m10049a(TradePlayerStatus aiVar, long j) {
        throw new aWi(new aCE(this, ehs, new Object[]{aiVar, new Long(j)}));
    }

    @C4034yP
    @C5566aOg
    @C0064Am(aul = "665b02a021fe01574d88f14ca0be077e", aum = 0)
    @C2499fr
    /* renamed from: a */
    private void m10050a(TradePlayerStatus aiVar, Item auq) {
        throw new aWi(new aCE(this, eho, new Object[]{aiVar, auq}));
    }

    @C0064Am(aul = "59caf6758590409323dd62dd74fa3fe1", aum = 0)
    @C5566aOg
    /* renamed from: a */
    private void m10051a(Player aku, Item auq) {
        throw new aWi(new aCE(this, ehz, new Object[]{aku, auq}));
    }

    /* renamed from: b */
    private void m10053b(TradePlayerStatus aiVar) {
        bFf().mo5608dq().mo3197f(ehd, aiVar);
    }

    @C4034yP
    @C5566aOg
    @C2499fr
    /* renamed from: b */
    private void m10054b(TradePlayerStatus aiVar, long j) {
        switch (bFf().mo6893i(ehs)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, ehs, new Object[]{aiVar, new Long(j)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, ehs, new Object[]{aiVar, new Long(j)}));
                break;
        }
        m10049a(aiVar, j);
    }

    @C4034yP
    @C5566aOg
    @C2499fr
    /* renamed from: b */
    private void m10055b(TradePlayerStatus aiVar, Item auq) {
        switch (bFf().mo6893i(eho)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, eho, new Object[]{aiVar, auq}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, eho, new Object[]{aiVar, auq}));
                break;
        }
        m10050a(aiVar, auq);
    }

    @C5566aOg
    /* renamed from: b */
    private void m10056b(Player aku, Item auq) {
        switch (bFf().mo6893i(ehz)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, ehz, new Object[]{aku, auq}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, ehz, new Object[]{aku, auq}));
                break;
        }
        m10051a(aku, auq);
    }

    @C0064Am(aul = "f58cb71b27c6e64be40d5035299a81ee", aum = 0)
    @C5566aOg
    /* renamed from: bl */
    private void m10057bl(Player aku) {
        throw new aWi(new aCE(this, ehk, new Object[]{aku}));
    }

    @C0064Am(aul = "81319d36c45680bf2e851cbeace4e698", aum = 0)
    @C5566aOg
    /* renamed from: bn */
    private void m10058bn(Player aku) {
        throw new aWi(new aCE(this, ehm, new Object[]{aku}));
    }

    @C0064Am(aul = "e9d9517e053f941c2e6ff34fdba431b8", aum = 0)
    @C5566aOg
    /* renamed from: bp */
    private void m10059bp(Player aku) {
        throw new aWi(new aCE(this, ehy, new Object[]{aku}));
    }

    @C5566aOg
    /* renamed from: bq */
    private void m10060bq(Player aku) {
        switch (bFf().mo6893i(ehy)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, ehy, new Object[]{aku}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, ehy, new Object[]{aku}));
                break;
        }
        m10059bp(aku);
    }

    private TradePlayerStatus buF() {
        return (TradePlayerStatus) bFf().mo5608dq().mo3214p(ehb);
    }

    private TradePlayerStatus buG() {
        return (TradePlayerStatus) bFf().mo5608dq().mo3214p(ehd);
    }

    private TradeTrigger buH() {
        return (TradeTrigger) bFf().mo5608dq().mo3214p(ehf);
    }

    @ClientOnly
    private TradePlayerStatus buN() {
        switch (bFf().mo6893i(ehi)) {
            case 0:
                return null;
            case 2:
                return (TradePlayerStatus) bFf().mo5606d(new aCE(this, ehi, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, ehi, new Object[0]));
                break;
        }
        return buM();
    }

    @ClientOnly
    private TradePlayerStatus buP() {
        switch (bFf().mo6893i(ehj)) {
            case 0:
                return null;
            case 2:
                return (TradePlayerStatus) bFf().mo5606d(new aCE(this, ehj, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, ehj, new Object[0]));
                break;
        }
        return buO();
    }

    @C0064Am(aul = "8dac046461333df1f867f7a2829fc102", aum = 0)
    @C5566aOg
    private boolean buQ() {
        throw new aWi(new aCE(this, ehl, new Object[0]));
    }

    @C5566aOg
    private boolean buR() {
        switch (bFf().mo6893i(ehl)) {
            case 0:
                return false;
            case 2:
                return ((Boolean) bFf().mo5606d(new aCE(this, ehl, new Object[0]))).booleanValue();
            case 3:
                bFf().mo5606d(new aCE(this, ehl, new Object[0]));
                break;
        }
        return buQ();
    }

    @C0064Am(aul = "b48728b63e5669b3fc772daa0626e86d", aum = 0)
    @C5566aOg
    @C0909NL
    private void buS() {
        throw new aWi(new aCE(this, ehv, new Object[0]));
    }

    @C5566aOg
    @C0909NL
    private void buT() {
        switch (bFf().mo6893i(ehv)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, ehv, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, ehv, new Object[0]));
                break;
        }
        buS();
    }

    @C0064Am(aul = "db392f44d32892729996e92fc1551804", aum = 0)
    @C5566aOg
    @C2499fr
    private void buU() {
        throw new aWi(new aCE(this, ehx, new Object[0]));
    }

    @C0064Am(aul = "6123540433ea0c4bcf4f68e63c77f102", aum = 0)
    @C5566aOg
    @C2499fr
    /* renamed from: c */
    private void m10063c(TradePlayerStatus aiVar, Item auq) {
        throw new aWi(new aCE(this, ehq, new Object[]{aiVar, auq}));
    }

    /* renamed from: d */
    private void m10064d(C1506WA wa) {
        switch (bFf().mo6893i(ehw)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, ehw, new Object[]{wa}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, ehw, new Object[]{wa}));
                break;
        }
        m10061c(wa);
    }

    @C5566aOg
    @C2499fr
    /* renamed from: d */
    private void m10065d(TradePlayerStatus aiVar, Item auq) {
        switch (bFf().mo6893i(ehq)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, ehq, new Object[]{aiVar, auq}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, ehq, new Object[]{aiVar, auq}));
                break;
        }
        m10063c(aiVar, auq);
    }

    @ClientOnly
    /* renamed from: A */
    public void mo5697A(Item auq) {
        switch (bFf().mo6893i(ehn)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, ehn, new Object[]{auq}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, ehn, new Object[]{auq}));
                break;
        }
        m10068z(auq);
    }

    @ClientOnly
    /* renamed from: C */
    public void mo5698C(Item auq) {
        switch (bFf().mo6893i(ehp)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, ehp, new Object[]{auq}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, ehp, new Object[]{auq}));
                break;
        }
        m10044B(auq);
    }

    /* renamed from: S */
    public void mo10S() {
        super.mo10S();
    }

    /* renamed from: T */
    public C6494any mo11T() {
        return ___iScriptClass;
    }

    /* renamed from: U */
    public C2491fm[] mo12U() {
        return _m_methods;
    }

    /* renamed from: W */
    public Object mo13W() {
        return new C5769aaB(this);
    }

    /* renamed from: a */
    public Object mo14a(C0495Gr gr) {
        Object[] args = gr.getArgs();
        switch (gr.mo2417hq() - TaikodomObject._m_methodCount) {
            case 0:
                return buI();
            case 1:
                return buK();
            case 2:
                return buM();
            case 3:
                return buO();
            case 4:
                m10057bl((Player) args[0]);
                return null;
            case 5:
                return new Boolean(buQ());
            case 6:
                m10058bn((Player) args[0]);
                return null;
            case 7:
                m10068z((Item) args[0]);
                return null;
            case 8:
                m10050a((TradePlayerStatus) args[0], (Item) args[1]);
                return null;
            case 9:
                m10044B((Item) args[0]);
                return null;
            case 10:
                m10063c((TradePlayerStatus) args[0], (Item) args[1]);
                return null;
            case 11:
                m10066fG(((Long) args[0]).longValue());
                return null;
            case 12:
                m10049a((TradePlayerStatus) args[0], ((Long) args[1]).longValue());
                return null;
            case 13:
                m10046a((C5473aKr) args[0], (C6200aiQ) args[1], (C5473aKr) args[2], (C6200aiQ) args[3], (C6200aiQ) args[4]);
                return null;
            case 14:
                m10062c((C5473aKr) args[0], (C6200aiQ) args[1], (C5473aKr) args[2], (C6200aiQ) args[3], (C6200aiQ) args[4]);
                return null;
            case 15:
                buS();
                return null;
            case 16:
                m10061c((C1506WA) args[0]);
                return null;
            case 17:
                buU();
                return null;
            case 18:
                m10067fg();
                return null;
            case 19:
                m10052b((aDJ) args[0]);
                return null;
            case 20:
                m10059bp((Player) args[0]);
                return null;
            case 21:
                m10051a((Player) args[0], (Item) args[1]);
                return null;
            default:
                return super.mo14a(gr);
        }
    }

    /* renamed from: a */
    public void mo15a(Collection collection, C0495Gr gr) {
        super.mo15a(collection, gr);
    }

    @ClientOnly
    /* renamed from: b */
    public void mo5699b(C5473aKr<aDJ, Object> akr, C6200aiQ<TradePlayerStatus> aiq, C5473aKr<aDJ, Object> akr2, C6200aiQ<TradePlayerStatus> aiq2, C6200aiQ<TradePlayerStatus> aiq3) {
        switch (bFf().mo6893i(eht)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, eht, new Object[]{akr, aiq, akr2, aiq2, aiq3}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, eht, new Object[]{akr, aiq, akr2, aiq2, aiq3}));
                break;
        }
        m10046a(akr, aiq, akr2, aiq2, aiq3);
    }

    @C5566aOg
    /* renamed from: bm */
    public void mo5700bm(Player aku) {
        switch (bFf().mo6893i(ehk)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, ehk, new Object[]{aku}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, ehk, new Object[]{aku}));
                break;
        }
        m10057bl(aku);
    }

    @C5566aOg
    /* renamed from: bo */
    public void mo5701bo(Player aku) {
        switch (bFf().mo6893i(ehm)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, ehm, new Object[]{aku}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, ehm, new Object[]{aku}));
                break;
        }
        m10058bn(aku);
    }

    @ClientOnly
    public Player buJ() {
        switch (bFf().mo6893i(ehg)) {
            case 0:
                return null;
            case 2:
                return (Player) bFf().mo5606d(new aCE(this, ehg, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, ehg, new Object[0]));
                break;
        }
        return buI();
    }

    @ClientOnly
    public Player buL() {
        switch (bFf().mo6893i(ehh)) {
            case 0:
                return null;
            case 2:
                return (Player) bFf().mo5606d(new aCE(this, ehh, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, ehh, new Object[0]));
                break;
        }
        return buK();
    }

    @C5566aOg
    @C2499fr
    public void buV() {
        switch (bFf().mo6893i(ehx)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, ehx, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, ehx, new Object[0]));
                break;
        }
        buU();
    }

    /* renamed from: c */
    public void mo98c(aDJ adj) {
        switch (bFf().mo6893i(f1716x13860637)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f1716x13860637, new Object[]{adj}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f1716x13860637, new Object[]{adj}));
                break;
        }
        m10052b(adj);
    }

    /* renamed from: c */
    public C5663aRz[] mo25c() {
        return _m_fields;
    }

    @ClientOnly
    /* renamed from: d */
    public void mo5705d(C5473aKr<aDJ, Object> akr, C6200aiQ<TradePlayerStatus> aiq, C5473aKr<aDJ, Object> akr2, C6200aiQ<TradePlayerStatus> aiq2, C6200aiQ<TradePlayerStatus> aiq3) {
        switch (bFf().mo6893i(ehu)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, ehu, new Object[]{akr, aiq, akr2, aiq2, aiq3}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, ehu, new Object[]{akr, aiq, akr2, aiq2, aiq3}));
                break;
        }
        m10062c(akr, aiq, akr2, aiq2, aiq3);
    }

    public final void dispose() {
        switch (bFf().mo6893i(_f_dispose_0020_0028_0029V)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, _f_dispose_0020_0028_0029V, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, _f_dispose_0020_0028_0029V, new Object[0]));
                break;
        }
        m10067fg();
    }

    @ClientOnly
    /* renamed from: fH */
    public void mo5707fH(long j) {
        switch (bFf().mo6893i(ehr)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, ehr, new Object[]{new Long(j)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, ehr, new Object[]{new Long(j)}));
                break;
        }
        m10066fG(j);
    }

    /* renamed from: e */
    public void mo5706e(Player aku, Player aku2) {
        super.mo10S();
        TradePlayerStatus aiVar = (TradePlayerStatus) bFf().mo6865M(TradePlayerStatus.class);
        aiVar.mo13633b(aku);
        m10048a(aiVar);
        TradePlayerStatus aiVar2 = (TradePlayerStatus) bFf().mo6865M(TradePlayerStatus.class);
        aiVar2.mo13633b(aku2);
        m10053b(aiVar2);
        if (!aku.bQB() && !aku2.bQB()) {
            TradeTrigger adg = (TradeTrigger) bFf().mo6865M(TradeTrigger.class);
            adg.mo12765a(this, aku, aku2);
            m10047a(adg);
            buH().setRadius(1000.0f);
            buH().mo993b(aku.bQx().mo960Nc());
        }
        m10060bq(aku);
        m10060bq(aku2);
    }

    @C0064Am(aul = "26d82719c05b82df1b35841d65d7afdd", aum = 0)
    @ClientOnly
    private Player buI() {
        return buN().mo13636dL();
    }

    @C0064Am(aul = "830f4efb45d86b810b042fd2dadd7f74", aum = 0)
    @ClientOnly
    private Player buK() {
        return buP().mo13636dL();
    }

    @C0064Am(aul = "0a8967365ced10958692dec89655f6e7", aum = 0)
    @ClientOnly
    private TradePlayerStatus buM() {
        Player aPC = ala().getPlayer();
        if (aPC == buF().mo13636dL()) {
            return buF();
        }
        if (aPC == buG().mo13636dL()) {
            return buG();
        }
        throw new IllegalStateException("Player '" + aPC + "' is acessing a trade he's not in");
    }

    @C0064Am(aul = "d5e10797fc33f65d8d30ee63b3e8c80c", aum = 0)
    @ClientOnly
    private TradePlayerStatus buO() {
        if (buN() == buF()) {
            return buG();
        }
        return buF();
    }

    @C0064Am(aul = "8bef5799f1f268c9b1fc3936e4d0b624", aum = 0)
    @ClientOnly
    /* renamed from: z */
    private void m10068z(Item auq) {
        m10055b(buN(), auq);
    }

    @C0064Am(aul = "7f68dd97008e9b493a37f6d071d11142", aum = 0)
    @ClientOnly
    /* renamed from: B */
    private void m10044B(Item auq) {
        m10065d(buN(), auq);
    }

    @C0064Am(aul = "07797a27b9db0e5fc0b9daa7a879e449", aum = 0)
    @ClientOnly
    /* renamed from: fG */
    private void m10066fG(long j) {
        m10054b(buN(), j);
    }

    @C0064Am(aul = "87cb2a1b6182ee92c9b56bee7a2a70ca", aum = 0)
    @ClientOnly
    /* renamed from: a */
    private void m10046a(C5473aKr<aDJ, Object> akr, C6200aiQ<TradePlayerStatus> aiq, C5473aKr<aDJ, Object> akr2, C6200aiQ<TradePlayerStatus> aiq2, C6200aiQ<TradePlayerStatus> aiq3) {
        buN().mo8319a(C1163RD.apm, (C5473aKr<?, ?>) akr);
        buN().mo8320a(C1163RD.eaI, (C6200aiQ<?>) aiq);
        buP().mo8319a(C1163RD.apm, (C5473aKr<?, ?>) akr2);
        buP().mo8320a(C1163RD.eaH, (C6200aiQ<?>) aiq2);
        buP().mo8320a(C1163RD.eaI, (C6200aiQ<?>) aiq3);
    }

    @C0064Am(aul = "be2e4fb8fc8bbb8ca26ae9646722e74a", aum = 0)
    @ClientOnly
    /* renamed from: c */
    private void m10062c(C5473aKr<aDJ, Object> akr, C6200aiQ<TradePlayerStatus> aiq, C5473aKr<aDJ, Object> akr2, C6200aiQ<TradePlayerStatus> aiq2, C6200aiQ<TradePlayerStatus> aiq3) {
        buN().mo8325b(C1163RD.apm, (C5473aKr<?, ?>) akr);
        buN().mo8326b(C1163RD.eaI, (C6200aiQ<?>) aiq);
        buP().mo8325b(C1163RD.apm, (C5473aKr<?, ?>) akr2);
        buP().mo8326b(C1163RD.eaH, (C6200aiQ<?>) aiq2);
        buP().mo8326b(C1163RD.eaI, (C6200aiQ<?>) aiq3);
    }

    @C0064Am(aul = "88458da23fbccf162d7c7cee730ef431", aum = 0)
    /* renamed from: c */
    private void m10061c(C1506WA wa) {
        buF().mo13636dL().mo14419f(wa);
        buG().mo13636dL().mo14419f(wa);
    }

    @C0064Am(aul = "721bb457bc18f81b5e99a1a6c61769b1", aum = 0)
    /* renamed from: fg */
    private void m10067fg() {
        buF().dispose();
        buG().dispose();
        if (buH() != null) {
            buH().mo1099zx();
            buH().dispose();
        }
        super.dispose();
    }

    @C0064Am(aul = "da0b2fd7312a8aa63bee08e4b83e5b3e", aum = 0)
    /* renamed from: b */
    private void m10052b(aDJ adj) {
        Item auq = (Item) adj;
        if (buF().mo13635d(auq)) {
            buV();
        } else if (buG().mo13635d(auq)) {
            buV();
        }
    }
}
