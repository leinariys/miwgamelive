package game.script.trade;

import game.network.message.externalizable.aCE;
import game.network.message.serializable.C3438ra;
import game.script.item.Item;
import game.script.item.ItemLocation;
import game.script.player.Player;
import logic.baa.*;
import logic.data.mbean.C5835abP;
import logic.res.code.C5640aRc;
import logic.res.code.C5663aRz;
import logic.res.code.C6494any;
import logic.res.html.C2491fm;
import p001a.*;

import java.util.Collection;

@C5511aMd
@C6485anp
/* renamed from: a.ai */
/* compiled from: a */
public class TradePlayerStatus extends aDJ implements C1616Xf {
    public static final int _m_fieldCount = 0;
    public static final C5663aRz[] _m_fields = null;
    public static final int _m_methodCount = 0;
    public static final C2491fm[] _m_methods = null;
    /* renamed from: gz */
    public static final C5663aRz f4581gz = null;
    /* renamed from: hB */
    public static final C5663aRz f4583hB = null;
    /* renamed from: hE */
    public static final C5663aRz f4586hE = null;
    /* renamed from: hF */
    public static final C2491fm f4587hF = null;
    /* renamed from: hG */
    public static final C2491fm f4588hG = null;
    /* renamed from: hH */
    public static final C2491fm f4589hH = null;
    /* renamed from: hI */
    public static final C2491fm f4590hI = null;
    /* renamed from: hJ */
    public static final C2491fm f4591hJ = null;
    /* renamed from: hK */
    public static final C2491fm f4592hK = null;
    /* renamed from: hL */
    public static final C2491fm f4593hL = null;
    /* renamed from: hM */
    public static final C2491fm f4594hM = null;
    /* renamed from: hN */
    public static final C2491fm f4595hN = null;
    /* renamed from: hO */
    public static final C2491fm f4596hO = null;
    /* renamed from: hz */
    public static final C5663aRz f4597hz = null;
    public static final long serialVersionUID = 0;
    public static C6494any ___iScriptClass;
    @C0064Am(aul = "7b5371d6377191de44cfd499401908d6", aum = 0)

    /* renamed from: P */
    private static Player f4580P;
    @C0064Am(aul = "fe7a7d72b2a9a94862cb584f5c46cccf", aum = 1)

    /* renamed from: hA */
    private static long f4582hA;
    @C0064Am(aul = "4dbd4ab9cf129aca4fba44ff6a910aca", aum = 2)

    /* renamed from: hC */
    private static C3438ra<Item> f4584hC;
    @C0064Am(aul = "4ec9e48b8dc9a23caf8fd84981507ff7", aum = 3)

    /* renamed from: hD */
    private static boolean f4585hD;

    static {
        m22402V();
    }

    public TradePlayerStatus() {
        super((C5540aNg) null);
        super.mo10S();
    }

    public TradePlayerStatus(C5540aNg ang) {
        super(ang);
    }

    TradePlayerStatus(Player aku) {
        super((C5540aNg) null);
        super._m_script_init(aku);
    }

    /* renamed from: V */
    static void m22402V() {
        serialVersionUID = (long) 1;
        _m_fieldCount = aDJ._m_fieldCount + 4;
        _m_methodCount = aDJ._m_methodCount + 10;
        int i = aDJ._m_fieldCount;
        C5663aRz[] arzArr = new C5663aRz[(i + 4)];
        C5663aRz b = C5640aRc.m17844b(TradePlayerStatus.class, "7b5371d6377191de44cfd499401908d6", i);
        f4597hz = b;
        arzArr[i] = b;
        int i2 = i + 1;
        C5663aRz b2 = C5640aRc.m17844b(TradePlayerStatus.class, "fe7a7d72b2a9a94862cb584f5c46cccf", i2);
        f4583hB = b2;
        arzArr[i2] = b2;
        int i3 = i2 + 1;
        C5663aRz b3 = C5640aRc.m17844b(TradePlayerStatus.class, "4dbd4ab9cf129aca4fba44ff6a910aca", i3);
        f4581gz = b3;
        arzArr[i3] = b3;
        int i4 = i3 + 1;
        C5663aRz b4 = C5640aRc.m17844b(TradePlayerStatus.class, "4ec9e48b8dc9a23caf8fd84981507ff7", i4);
        f4586hE = b4;
        arzArr[i4] = b4;
        int i5 = i4 + 1;
        _m_fields = arzArr;
        C1634Xv.m11725a((Object[]) aDJ._m_fields, (Object[]) _m_fields);
        int i6 = aDJ._m_methodCount;
        C2491fm[] fmVarArr = new C2491fm[(i6 + 10)];
        C2491fm a = C4105zY.m41624a(TradePlayerStatus.class, "69a8bc1e42a2f79589c1e2ccfb5145b6", i6);
        f4587hF = a;
        fmVarArr[i6] = a;
        int i7 = i6 + 1;
        C2491fm a2 = C4105zY.m41624a(TradePlayerStatus.class, "a3ace31932feceae7b02b986a81214bf", i7);
        f4588hG = a2;
        fmVarArr[i7] = a2;
        int i8 = i7 + 1;
        C2491fm a3 = C4105zY.m41624a(TradePlayerStatus.class, "49c7fac7af2e84c773d7bc12a8a22334", i8);
        f4589hH = a3;
        fmVarArr[i8] = a3;
        int i9 = i8 + 1;
        C2491fm a4 = C4105zY.m41624a(TradePlayerStatus.class, "d46c9d8b4795d6b531af570ae72d727c", i9);
        f4590hI = a4;
        fmVarArr[i9] = a4;
        int i10 = i9 + 1;
        C2491fm a5 = C4105zY.m41624a(TradePlayerStatus.class, "03c7e335019b87fc6198cc05df7910eb", i10);
        f4591hJ = a5;
        fmVarArr[i10] = a5;
        int i11 = i10 + 1;
        C2491fm a6 = C4105zY.m41624a(TradePlayerStatus.class, "2f21f584dcaadd05f93e37c2910ffb52", i11);
        f4592hK = a6;
        fmVarArr[i11] = a6;
        int i12 = i11 + 1;
        C2491fm a7 = C4105zY.m41624a(TradePlayerStatus.class, "d9f1ec21af81ec28e95c2bcb47579fc2", i12);
        f4593hL = a7;
        fmVarArr[i12] = a7;
        int i13 = i12 + 1;
        C2491fm a8 = C4105zY.m41624a(TradePlayerStatus.class, "e4c75eed2aaa1f861ea4acab052f7594", i13);
        f4594hM = a8;
        fmVarArr[i13] = a8;
        int i14 = i13 + 1;
        C2491fm a9 = C4105zY.m41624a(TradePlayerStatus.class, "dd445799060e2b605477b75d9263f9cd", i14);
        f4595hN = a9;
        fmVarArr[i14] = a9;
        int i15 = i14 + 1;
        C2491fm a10 = C4105zY.m41624a(TradePlayerStatus.class, "b2713f95a4c13e826d0217e592c32dd2", i15);
        f4596hO = a10;
        fmVarArr[i15] = a10;
        int i16 = i15 + 1;
        _m_methods = fmVarArr;
        C1634Xv.m11725a((Object[]) aDJ._m_methods, (Object[]) _m_methods);
        ___iScriptClass = aUO.m18566a(TradePlayerStatus.class, C5835abP.class, _m_fields, _m_methods);
    }

    /* renamed from: a */
    private void m22403a(Player aku) {
        bFf().mo5608dq().mo3197f(f4597hz, aku);
    }

    /* renamed from: dG */
    private Player m22406dG() {
        return (Player) bFf().mo5608dq().mo3214p(f4597hz);
    }

    /* renamed from: dH */
    private long m22407dH() {
        return bFf().mo5608dq().mo3213o(f4583hB);
    }

    /* renamed from: dI */
    private C3438ra m22408dI() {
        return (C3438ra) bFf().mo5608dq().mo3214p(f4581gz);
    }

    /* renamed from: dJ */
    private boolean m22409dJ() {
        return bFf().mo5608dq().mo3201h(f4586hE);
    }

    @C0064Am(aul = "b2713f95a4c13e826d0217e592c32dd2", aum = 0)
    @C5566aOg
    /* renamed from: e */
    private ItemLocation m22416e(Item auq) {
        throw new aWi(new aCE(this, f4596hO, new Object[]{auq}));
    }

    /* renamed from: h */
    private void m22417h(long j) {
        bFf().mo5608dq().mo3184b(f4583hB, j);
    }

    /* renamed from: m */
    private void m22419m(C3438ra raVar) {
        bFf().mo5608dq().mo3197f(f4581gz, raVar);
    }

    /* renamed from: n */
    private void m22420n(boolean z) {
        bFf().mo5608dq().mo3153a(f4586hE, z);
    }

    /* renamed from: S */
    public void mo10S() {
        super.mo10S();
    }

    /* renamed from: T */
    public C6494any mo11T() {
        return ___iScriptClass;
    }

    /* renamed from: U */
    public C2491fm[] mo12U() {
        return _m_methods;
    }

    /* renamed from: W */
    public Object mo13W() {
        return new C5835abP(this);
    }

    /* renamed from: a */
    public Object mo14a(C0495Gr gr) {
        Object[] args = gr.getArgs();
        switch (gr.mo2417hq() - aDJ._m_methodCount) {
            case 0:
                return m22410dK();
            case 1:
                return new Long(m22411dM());
            case 2:
                return new Boolean(m22418i(((Long) args[0]).longValue()));
            case 3:
                return m22412dO();
            case 4:
                m22413dQ();
                return null;
            case 5:
                m22414dS();
                return null;
            case 6:
                return new Boolean(m22404a((Item) args[0]));
            case 7:
                return new Boolean(m22405c((Item) args[0]));
            case 8:
                return new Boolean(m22415dU());
            case 9:
                return m22416e((Item) args[0]);
            default:
                return super.mo14a(gr);
        }
    }

    /* renamed from: a */
    public void mo15a(Collection collection, C0495Gr gr) {
        super.mo15a(collection, gr);
    }

    /* access modifiers changed from: protected */
    /* renamed from: b */
    public boolean mo13634b(Item auq) {
        switch (bFf().mo6893i(f4593hL)) {
            case 0:
                return false;
            case 2:
                return ((Boolean) bFf().mo5606d(new aCE(this, f4593hL, new Object[]{auq}))).booleanValue();
            case 3:
                bFf().mo5606d(new aCE(this, f4593hL, new Object[]{auq}));
                break;
        }
        return m22404a(auq);
    }

    /* renamed from: c */
    public C5663aRz[] mo25c() {
        return _m_fields;
    }

    /* access modifiers changed from: protected */
    /* renamed from: d */
    public boolean mo13635d(Item auq) {
        switch (bFf().mo6893i(f4594hM)) {
            case 0:
                return false;
            case 2:
                return ((Boolean) bFf().mo5606d(new aCE(this, f4594hM, new Object[]{auq}))).booleanValue();
            case 3:
                bFf().mo5606d(new aCE(this, f4594hM, new Object[]{auq}));
                break;
        }
        return m22405c(auq);
    }

    /* access modifiers changed from: protected */
    /* renamed from: dL */
    public Player mo13636dL() {
        switch (bFf().mo6893i(f4587hF)) {
            case 0:
                return null;
            case 2:
                return (Player) bFf().mo5606d(new aCE(this, f4587hF, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f4587hF, new Object[0]));
                break;
        }
        return m22410dK();
    }

    /* access modifiers changed from: protected */
    /* renamed from: dN */
    public long mo13637dN() {
        switch (bFf().mo6893i(f4588hG)) {
            case 0:
                return 0;
            case 2:
                return ((Long) bFf().mo5606d(new aCE(this, f4588hG, new Object[0]))).longValue();
            case 3:
                bFf().mo5606d(new aCE(this, f4588hG, new Object[0]));
                break;
        }
        return m22411dM();
    }

    /* access modifiers changed from: protected */
    /* renamed from: dP */
    public C3438ra<Item> mo13638dP() {
        switch (bFf().mo6893i(f4590hI)) {
            case 0:
                return null;
            case 2:
                return (C3438ra) bFf().mo5606d(new aCE(this, f4590hI, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f4590hI, new Object[0]));
                break;
        }
        return m22412dO();
    }

    /* access modifiers changed from: protected */
    /* renamed from: dR */
    public void mo13639dR() {
        switch (bFf().mo6893i(f4591hJ)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f4591hJ, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f4591hJ, new Object[0]));
                break;
        }
        m22413dQ();
    }

    /* access modifiers changed from: protected */
    /* renamed from: dT */
    public void mo13640dT() {
        switch (bFf().mo6893i(f4592hK)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f4592hK, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f4592hK, new Object[0]));
                break;
        }
        m22414dS();
    }

    /* access modifiers changed from: protected */
    /* renamed from: dV */
    public boolean mo13641dV() {
        switch (bFf().mo6893i(f4595hN)) {
            case 0:
                return false;
            case 2:
                return ((Boolean) bFf().mo5606d(new aCE(this, f4595hN, new Object[0]))).booleanValue();
            case 3:
                bFf().mo5606d(new aCE(this, f4595hN, new Object[0]));
                break;
        }
        return m22415dU();
    }

    /* access modifiers changed from: protected */
    @C5566aOg
    /* renamed from: f */
    public ItemLocation mo13642f(Item auq) {
        switch (bFf().mo6893i(f4596hO)) {
            case 0:
                return null;
            case 2:
                return (ItemLocation) bFf().mo5606d(new aCE(this, f4596hO, new Object[]{auq}));
            case 3:
                bFf().mo5606d(new aCE(this, f4596hO, new Object[]{auq}));
                break;
        }
        return m22416e(auq);
    }

    /* access modifiers changed from: protected */
    /* renamed from: j */
    public boolean mo13643j(long j) {
        switch (bFf().mo6893i(f4589hH)) {
            case 0:
                return false;
            case 2:
                return ((Boolean) bFf().mo5606d(new aCE(this, f4589hH, new Object[]{new Long(j)}))).booleanValue();
            case 3:
                bFf().mo5606d(new aCE(this, f4589hH, new Object[]{new Long(j)}));
                break;
        }
        return m22418i(j);
    }

    /* access modifiers changed from: package-private */
    /* renamed from: b */
    public void mo13633b(Player aku) {
        super.mo10S();
        m22403a(aku);
        m22420n(false);
    }

    @C0064Am(aul = "69a8bc1e42a2f79589c1e2ccfb5145b6", aum = 0)
    /* renamed from: dK */
    private Player m22410dK() {
        return m22406dG();
    }

    @C0064Am(aul = "a3ace31932feceae7b02b986a81214bf", aum = 0)
    /* renamed from: dM */
    private long m22411dM() {
        return m22407dH();
    }

    @C0064Am(aul = "49c7fac7af2e84c773d7bc12a8a22334", aum = 0)
    /* renamed from: i */
    private boolean m22418i(long j) {
        if (j == m22407dH()) {
            return false;
        }
        m22417h(j);
        return true;
    }

    @C0064Am(aul = "d46c9d8b4795d6b531af570ae72d727c", aum = 0)
    /* renamed from: dO */
    private C3438ra<Item> m22412dO() {
        return m22408dI();
    }

    @C0064Am(aul = "03c7e335019b87fc6198cc05df7910eb", aum = 0)
    /* renamed from: dQ */
    private void m22413dQ() {
        m22420n(false);
    }

    @C0064Am(aul = "2f21f584dcaadd05f93e37c2910ffb52", aum = 0)
    /* renamed from: dS */
    private void m22414dS() {
        m22420n(!m22409dJ());
    }

    @C0064Am(aul = "d9f1ec21af81ec28e95c2bcb47579fc2", aum = 0)
    /* renamed from: a */
    private boolean m22404a(Item auq) {
        if (auq.cxh()) {
            return false;
        }
        if ((m22406dG().bQx() != null && m22406dG().bQx().agr() == auq) || m22408dI().contains(auq)) {
            return false;
        }
        m22408dI().add(auq);
        return true;
    }

    @C0064Am(aul = "e4c75eed2aaa1f861ea4acab052f7594", aum = 0)
    /* renamed from: c */
    private boolean m22405c(Item auq) {
        return m22408dI().remove(auq);
    }

    @C0064Am(aul = "dd445799060e2b605477b75d9263f9cd", aum = 0)
    /* renamed from: dU */
    private boolean m22415dU() {
        return m22409dJ();
    }
}
