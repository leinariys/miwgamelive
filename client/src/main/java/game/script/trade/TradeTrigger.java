package game.script.trade;

import com.hoplon.geometry.Vec3f;
import game.network.message.externalizable.aCE;
import game.script.Actor;
import game.script.AttachedTrigger;
import game.script.player.Player;
import game.script.simulation.Space;
import logic.aaa.C2235dB;
import logic.baa.C1616Xf;
import logic.baa.C1634Xv;
import logic.baa.C5511aMd;
import logic.baa.C6485anp;
import logic.data.mbean.C6309akV;
import logic.res.code.C5640aRc;
import logic.res.code.C5663aRz;
import logic.res.code.C6494any;
import logic.res.html.C2491fm;
import p001a.*;

import java.util.Collection;

@C6485anp
@C3122oB(mo20937Uq = C3122oB.C3123a.NOT_REPLICATED)
@C1020Ot(drz = C1020Ot.C1021a.NOT_PERSISTED)
@C5511aMd
@C0909NL
/* renamed from: a.adG  reason: case insensitive filesystem */
/* compiled from: a */
public class TradeTrigger extends AttachedTrigger implements C1616Xf {
    public static final C2491fm _f_dispose_0020_0028_0029V = null;
    public static final C2491fm _f_step_0020_0028F_0029V = null;
    public static final int _m_fieldCount = 0;
    public static final C5663aRz[] _m_fields = null;
    public static final int _m_methodCount = 0;
    public static final C2491fm[] _m_methods = null;
    public static final C2491fm awD = null;
    public static final C5663aRz fkB = null;
    public static final C5663aRz fkD = null;
    public static final C5663aRz fkz = null;
    public static final long serialVersionUID = 0;
    /* renamed from: uY */
    public static final C2491fm f4296uY = null;
    public static C6494any ___iScriptClass;
    @C0064Am(aul = "5a1c217cf807e6e34509da0dccd9bb92", aum = 1)
    private static Player fkA;
    @C0064Am(aul = "2bc10ed9c429990082555c6ed602f673", aum = 2)
    private static Player fkC;
    @C0064Am(aul = "b54b1883ad06ac89b8ba8af57a5bbaf6", aum = 0)
    private static Trade fky;

    static {
        m20712V();
    }

    public TradeTrigger() {
        super((C5540aNg) null);
        super.mo10S();
    }

    public TradeTrigger(Trade td, Player aku, Player aku2) {
        super((C5540aNg) null);
        super._m_script_init(td, aku, aku2);
    }

    public TradeTrigger(C5540aNg ang) {
        super(ang);
    }

    /* renamed from: V */
    static void m20712V() {
        serialVersionUID = (long) 1;
        _m_fieldCount = AttachedTrigger._m_fieldCount + 3;
        _m_methodCount = AttachedTrigger._m_methodCount + 4;
        int i = AttachedTrigger._m_fieldCount;
        C5663aRz[] arzArr = new C5663aRz[(i + 3)];
        C5663aRz b = C5640aRc.m17844b(TradeTrigger.class, "b54b1883ad06ac89b8ba8af57a5bbaf6", i);
        fkz = b;
        arzArr[i] = b;
        int i2 = i + 1;
        C5663aRz b2 = C5640aRc.m17844b(TradeTrigger.class, "5a1c217cf807e6e34509da0dccd9bb92", i2);
        fkB = b2;
        arzArr[i2] = b2;
        int i3 = i2 + 1;
        C5663aRz b3 = C5640aRc.m17844b(TradeTrigger.class, "2bc10ed9c429990082555c6ed602f673", i3);
        fkD = b3;
        arzArr[i3] = b3;
        int i4 = i3 + 1;
        _m_fields = arzArr;
        C1634Xv.m11725a((Object[]) AttachedTrigger._m_fields, (Object[]) _m_fields);
        int i5 = AttachedTrigger._m_methodCount;
        C2491fm[] fmVarArr = new C2491fm[(i5 + 4)];
        C2491fm a = C4105zY.m41624a(TradeTrigger.class, "116041802bda84a553395c3a2c2d975a", i5);
        _f_step_0020_0028F_0029V = a;
        fmVarArr[i5] = a;
        int i6 = i5 + 1;
        C2491fm a2 = C4105zY.m41624a(TradeTrigger.class, "83359ebec1b7de1c9f097f02c54b841f", i6);
        f4296uY = a2;
        fmVarArr[i6] = a2;
        int i7 = i6 + 1;
        C2491fm a3 = C4105zY.m41624a(TradeTrigger.class, "3b5b082f1d89583e8d11de1a31315fb6", i7);
        awD = a3;
        fmVarArr[i7] = a3;
        int i8 = i7 + 1;
        C2491fm a4 = C4105zY.m41624a(TradeTrigger.class, "fed67a3ec57786547a1cddc02d8b33d9", i8);
        _f_dispose_0020_0028_0029V = a4;
        fmVarArr[i8] = a4;
        int i9 = i8 + 1;
        _m_methods = fmVarArr;
        C1634Xv.m11725a((Object[]) AttachedTrigger._m_methods, (Object[]) _m_methods);
        ___iScriptClass = aUO.m18566a(TradeTrigger.class, C6309akV.class, _m_fields, _m_methods);
    }

    /* renamed from: a */
    private void m20713a(Trade td) {
        bFf().mo5608dq().mo3197f(fkz, td);
    }

    private Trade bRA() {
        return (Trade) bFf().mo5608dq().mo3214p(fkz);
    }

    private Player bRB() {
        return (Player) bFf().mo5608dq().mo3214p(fkB);
    }

    private Player bRC() {
        return (Player) bFf().mo5608dq().mo3214p(fkD);
    }

    /* renamed from: bv */
    private void m20715bv(Player aku) {
        bFf().mo5608dq().mo3197f(fkB, aku);
    }

    /* renamed from: bw */
    private void m20716bw(Player aku) {
        bFf().mo5608dq().mo3197f(fkD, aku);
    }

    /* renamed from: B */
    public void mo7552B(Actor cr) {
        switch (bFf().mo6893i(awD)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, awD, new Object[]{cr}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, awD, new Object[]{cr}));
                break;
        }
        m20711A(cr);
    }

    /* renamed from: S */
    public void mo10S() {
        super.mo10S();
    }

    /* renamed from: T */
    public C6494any mo11T() {
        return ___iScriptClass;
    }

    /* renamed from: U */
    public C2491fm[] mo12U() {
        return _m_methods;
    }

    /* renamed from: W */
    public Object mo13W() {
        return new C6309akV(this);
    }

    /* renamed from: a */
    public Object mo14a(C0495Gr gr) {
        Object[] args = gr.getArgs();
        switch (gr.mo2417hq() - AttachedTrigger._m_methodCount) {
            case 0:
                m20714ay(((Float) args[0]).floatValue());
                return null;
            case 1:
                return m20717c((Space) args[0], ((Long) args[1]).longValue());
            case 2:
                m20711A((Actor) args[0]);
                return null;
            case 3:
                m20718fg();
                return null;
            default:
                return super.mo14a(gr);
        }
    }

    /* renamed from: a */
    public void mo15a(Collection collection, C0495Gr gr) {
        super.mo15a(collection, gr);
    }

    /* renamed from: c */
    public C5663aRz[] mo25c() {
        return _m_fields;
    }

    /* access modifiers changed from: protected */
    /* renamed from: d */
    public C0520HN mo635d(Space ea, long j) {
        switch (bFf().mo6893i(f4296uY)) {
            case 0:
                return null;
            case 2:
                return (C0520HN) bFf().mo5606d(new aCE(this, f4296uY, new Object[]{ea, new Long(j)}));
            case 3:
                bFf().mo5606d(new aCE(this, f4296uY, new Object[]{ea, new Long(j)}));
                break;
        }
        return m20717c(ea, j);
    }

    public void dispose() {
        switch (bFf().mo6893i(_f_dispose_0020_0028_0029V)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, _f_dispose_0020_0028_0029V, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, _f_dispose_0020_0028_0029V, new Object[0]));
                break;
        }
        m20718fg();
    }

    public void step(float f) {
        switch (bFf().mo6893i(_f_step_0020_0028F_0029V)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, _f_step_0020_0028F_0029V, new Object[]{new Float(f)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, _f_step_0020_0028F_0029V, new Object[]{new Float(f)}));
                break;
        }
        m20714ay(f);
    }

    /* renamed from: a */
    public void mo12765a(Trade td, Player aku, Player aku2) {
        super.mo22342a((Actor) aku.bQx(), new Vec3f(0.0f, 0.0f, 0.0f));
        m20713a(td);
        m20715bv(aku);
        m20716bw(aku2);
    }

    @C0064Am(aul = "116041802bda84a553395c3a2c2d975a", aum = 0)
    /* renamed from: ay */
    private void m20714ay(float f) {
        super.step(f);
    }

    @C0064Am(aul = "83359ebec1b7de1c9f097f02c54b841f", aum = 0)
    /* renamed from: c */
    private C0520HN m20717c(Space ea, long j) {
        C2750jU jUVar = new C2750jU(ea, this, mo958IL());
        jUVar.mo2449a(ea.cKn().mo5725a(j, (C2235dB) jUVar, 10));
        return jUVar;
    }

    @C0064Am(aul = "3b5b082f1d89583e8d11de1a31315fb6", aum = 0)
    /* renamed from: A */
    private void m20711A(Actor cr) {
        if (!bGY() || isDisposed()) {
            return;
        }
        if (cr == bRB().bQx()) {
            bRA().mo5701bo(bRB());
        } else if (cr == bRC().bQx()) {
            bRA().mo5701bo(bRC());
        }
    }

    @C0064Am(aul = "fed67a3ec57786547a1cddc02d8b33d9", aum = 0)
    /* renamed from: fg */
    private void m20718fg() {
        m20713a((Trade) null);
        super.dispose();
    }
}
