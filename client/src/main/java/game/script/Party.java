package game.script;

import game.network.message.externalizable.C0352Eg;
import game.network.message.externalizable.aCE;
import game.network.message.serializable.C3438ra;
import game.script.player.Player;
import logic.aaa.C1506WA;
import logic.baa.*;
import logic.data.link.C0422Ft;
import logic.data.mbean.aTG;
import logic.res.code.C5640aRc;
import logic.res.code.C5663aRz;
import logic.res.code.C6494any;
import logic.res.html.C2491fm;
import p001a.*;
import taikodom.infra.script.I18NString;

import java.util.*;

@C5511aMd
@C6485anp
/* renamed from: a.Gt */
/* compiled from: a */
public class Party extends TaikodomObject implements C1616Xf {
    public static final C2491fm _f_dispose_0020_0028_0029V = null;
    public static final int _m_fieldCount = 0;
    public static final C5663aRz[] _m_fields = null;
    public static final int _m_methodCount = 0;
    public static final C2491fm[] _m_methods = null;
    public static final C2491fm aJH = null;
    public static final C5663aRz cHx = null;
    public static final C2491fm cYA = null;
    public static final C2491fm cYB = null;
    public static final C2491fm cYC = null;
    public static final C2491fm cYD = null;
    public static final C2491fm cYE = null;
    public static final C2491fm cYF = null;
    public static final C2491fm cYG = null;
    public static final C2491fm cYH = null;
    public static final C2491fm cYI = null;
    public static final C2491fm cYJ = null;
    public static final C2491fm cYK = null;
    public static final C2491fm cYL = null;
    public static final C2491fm cYM = null;
    public static final C2491fm cYN = null;
    public static final C2491fm cYO = null;
    public static final int cYx = 6;
    public static final C5663aRz cnX = null;

    public static C6494any ___iScriptClass = null;
    @C0064Am(aul = "6457ff9e721a93beb26b4b62ddf4463b", aum = 0)
    private static C3438ra<Player> cYy = null;
    @C0064Am(aul = "6b144f1b9b6e17db4208ee188e73f76e", aum = 1)
    private static Player cYz = null;

    static {
        m3498V();
    }

    public Party() {
        super((C5540aNg) null);
        super.mo10S();
    }

    public Party(C5540aNg ang) {
        super(ang);
    }

    public Party(Player aku) {
        super((C5540aNg) null);
        super._m_script_init(aku);
    }

    /* renamed from: V */
    static void m3498V() {
        _m_fieldCount = TaikodomObject._m_fieldCount + 2;
        _m_methodCount = TaikodomObject._m_methodCount + 17;
        int i = TaikodomObject._m_fieldCount;
        C5663aRz[] arzArr = new C5663aRz[(i + 2)];
        C5663aRz b = C5640aRc.m17844b(Party.class, "6457ff9e721a93beb26b4b62ddf4463b", i);
        cnX = b;
        arzArr[i] = b;
        int i2 = i + 1;
        C5663aRz b2 = C5640aRc.m17844b(Party.class, "6b144f1b9b6e17db4208ee188e73f76e", i2);
        cHx = b2;
        arzArr[i2] = b2;
        int i3 = i2 + 1;
        _m_fields = arzArr;
        C1634Xv.m11725a((Object[]) TaikodomObject._m_fields, (Object[]) _m_fields);
        int i4 = TaikodomObject._m_methodCount;
        C2491fm[] fmVarArr = new C2491fm[(i4 + 17)];
        C2491fm a = C4105zY.m41624a(Party.class, "0d2a8b9e925fadb2dd91b26410a5f172", i4);
        cYA = a;
        fmVarArr[i4] = a;
        int i5 = i4 + 1;
        C2491fm a2 = C4105zY.m41624a(Party.class, "4aaad466b6a3c7b89daf2b24ae07f771", i5);
        cYB = a2;
        fmVarArr[i5] = a2;
        int i6 = i5 + 1;
        C2491fm a3 = C4105zY.m41624a(Party.class, "b87be59639ea36ac2c7bcd4f72ea4b4f", i6);
        cYC = a3;
        fmVarArr[i6] = a3;
        int i7 = i6 + 1;
        C2491fm a4 = C4105zY.m41624a(Party.class, "df3df0d869a4958f73660c9f7bea2d8b", i7);
        cYD = a4;
        fmVarArr[i7] = a4;
        int i8 = i7 + 1;
        C2491fm a5 = C4105zY.m41624a(Party.class, "4557e8974f8071754db314bcb4cf8911", i8);
        cYE = a5;
        fmVarArr[i8] = a5;
        int i9 = i8 + 1;
        C2491fm a6 = C4105zY.m41624a(Party.class, "46727c751f9ac4e98eb07fa6876800b3", i9);
        cYF = a6;
        fmVarArr[i9] = a6;
        int i10 = i9 + 1;
        C2491fm a7 = C4105zY.m41624a(Party.class, "bb9be25a260106f85913073464cb9294", i10);
        aJH = a7;
        fmVarArr[i10] = a7;
        int i11 = i10 + 1;
        C2491fm a8 = C4105zY.m41624a(Party.class, "544e06bbd85c68038c3657b65d4cc6cb", i11);
        cYG = a8;
        fmVarArr[i11] = a8;
        int i12 = i11 + 1;
        C2491fm a9 = C4105zY.m41624a(Party.class, "c5d4d9ffb46d835cb5a0977d60c89953", i12);
        cYH = a9;
        fmVarArr[i12] = a9;
        int i13 = i12 + 1;
        C2491fm a10 = C4105zY.m41624a(Party.class, "87cf976dcd9df658ecc3ecfc5d8b9eb3", i13);
        cYI = a10;
        fmVarArr[i13] = a10;
        int i14 = i13 + 1;
        C2491fm a11 = C4105zY.m41624a(Party.class, "34595bac5f6541f746e78a97435b1170", i14);
        cYJ = a11;
        fmVarArr[i14] = a11;
        int i15 = i14 + 1;
        C2491fm a12 = C4105zY.m41624a(Party.class, "30b4f04a0bacf895a7df0bfd08530184", i15);
        cYK = a12;
        fmVarArr[i15] = a12;
        int i16 = i15 + 1;
        C2491fm a13 = C4105zY.m41624a(Party.class, "0e782a2ba0615ed7d23e4c4d4adf3f1b", i16);
        cYL = a13;
        fmVarArr[i16] = a13;
        int i17 = i16 + 1;
        C2491fm a14 = C4105zY.m41624a(Party.class, "77430f824906ff78f773c86fbc72fefa", i17);
        cYM = a14;
        fmVarArr[i17] = a14;
        int i18 = i17 + 1;
        C2491fm a15 = C4105zY.m41624a(Party.class, "6c7c7825bc17183da14435ba9c4bcf4d", i18);
        cYN = a15;
        fmVarArr[i18] = a15;
        int i19 = i18 + 1;
        C2491fm a16 = C4105zY.m41624a(Party.class, "685e36c2546e9e83f6467cb51cf196e4", i19);
        cYO = a16;
        fmVarArr[i19] = a16;
        int i20 = i19 + 1;
        C2491fm a17 = C4105zY.m41624a(Party.class, "2f22b13185dab9d3683709ce39733341", i20);
        _f_dispose_0020_0028_0029V = a17;
        fmVarArr[i20] = a17;
        int i21 = i20 + 1;
        _m_methods = fmVarArr;
        C1634Xv.m11725a((Object[]) TaikodomObject._m_methods, (Object[]) _m_methods);
        ___iScriptClass = aUO.m18566a(Party.class, aTG.class, _m_fields, _m_methods);
    }

    /* renamed from: aF */
    private void m3499aF(Player aku) {
        bFf().mo5608dq().mo3197f(cHx, aku);
    }

    @C0064Am(aul = "0d2a8b9e925fadb2dd91b26410a5f172", aum = 0)
    @C6580apg(cpn = true)
    /* renamed from: aG */
    private void m3500aG(Player aku) {
        bFf().mo5600a((Class<? extends C4062yl>) C0422Ft.C0425c.class, (C0495Gr) new aCE(this, cYA, new Object[]{aku}));
    }

    @C6580apg(cpn = true)
    /* renamed from: aH */
    private void m3501aH(Player aku) {
        switch (bFf().mo6893i(cYA)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, cYA, new Object[]{aku}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, cYA, new Object[]{aku}));
                break;
        }
        m3500aG(aku);
    }

    @C0064Am(aul = "4aaad466b6a3c7b89daf2b24ae07f771", aum = 0)
    @C6580apg(cpn = true)
    /* renamed from: aI */
    private void m3502aI(Player aku) {
        bFf().mo5600a((Class<? extends C4062yl>) C0422Ft.C0424b.class, (C0495Gr) new aCE(this, cYB, new Object[]{aku}));
    }

    @C6580apg(cpn = true)
    /* renamed from: aJ */
    private void m3503aJ(Player aku) {
        switch (bFf().mo6893i(cYB)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, cYB, new Object[]{aku}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, cYB, new Object[]{aku}));
                break;
        }
        m3502aI(aku);
    }

    @C0064Am(aul = "87cf976dcd9df658ecc3ecfc5d8b9eb3", aum = 0)
    @C5566aOg
    /* renamed from: aM */
    private boolean m3505aM(Player aku) {
        throw new aWi(new aCE(this, cYI, new Object[]{aku}));
    }

    @C0064Am(aul = "34595bac5f6541f746e78a97435b1170", aum = 0)
    @C5566aOg
    /* renamed from: aO */
    private boolean m3506aO(Player aku) {
        throw new aWi(new aCE(this, cYJ, new Object[]{aku}));
    }

    @C0064Am(aul = "30b4f04a0bacf895a7df0bfd08530184", aum = 0)
    @C5566aOg
    /* renamed from: aQ */
    private boolean m3507aQ(Player aku) {
        throw new aWi(new aCE(this, cYK, new Object[]{aku}));
    }

    private C3438ra aQN() {
        return (C3438ra) bFf().mo5608dq().mo3214p(cnX);
    }

    private Player aQO() {
        return (Player) bFf().mo5608dq().mo3214p(cHx);
    }

    @C0064Am(aul = "b87be59639ea36ac2c7bcd4f72ea4b4f", aum = 0)
    @C6580apg(cpn = true)
    private void aQP() {
        bFf().mo5600a((Class<? extends C4062yl>) C0422Ft.C0423a.class, (C0495Gr) new aCE(this, cYC, new Object[0]));
    }

    @C6580apg(cpn = true)
    private void aQQ() {
        switch (bFf().mo6893i(cYC)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, cYC, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, cYC, new Object[0]));
                break;
        }
        aQP();
    }

    @C0064Am(aul = "0e782a2ba0615ed7d23e4c4d4adf3f1b", aum = 0)
    @C5566aOg
    /* renamed from: aS */
    private void m3508aS(Player aku) {
        throw new aWi(new aCE(this, cYL, new Object[]{aku}));
    }

    /* renamed from: aZ */
    private void m3510aZ(C3438ra raVar) {
        bFf().mo5608dq().mo3197f(cnX, raVar);
    }

    @C0064Am(aul = "77430f824906ff78f773c86fbc72fefa", aum = 0)
    @C5566aOg
    /* renamed from: c */
    private void m3511c(Player aku, boolean z) {
        throw new aWi(new aCE(this, cYM, new Object[]{aku, new Boolean(z)}));
    }

    /* renamed from: d */
    private void m3513d(I18NString i18NString, Object... objArr) {
        switch (bFf().mo6893i(cYE)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, cYE, new Object[]{i18NString, objArr}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, cYE, new Object[]{i18NString, objArr}));
                break;
        }
        m3512c(i18NString, objArr);
    }

    @C4034yP
    @C5566aOg
    @C0064Am(aul = "df3df0d869a4958f73660c9f7bea2d8b", aum = 0)
    @C2499fr
    /* renamed from: g */
    private void m3515g(Player aku, String str) {
        throw new aWi(new aCE(this, cYD, new Object[]{aku, str}));
    }

    /* renamed from: g */
    private void m3516g(Collection collection, C0495Gr gr) {
        Iterator it = collection.iterator();
        while (it.hasNext()) {
            ((C0422Ft.C0425c) it.next()).mo2228a(this, (Player) gr.getArgs()[0]);
        }
    }

    /* renamed from: h */
    private void m3517h(Collection collection, C0495Gr gr) {
        Iterator it = collection.iterator();
        while (it.hasNext()) {
            ((C0422Ft.C0424b) it.next()).mo2227b(this, (Player) gr.getArgs()[0]);
        }
    }

    /* renamed from: i */
    private void m3518i(Collection collection, C0495Gr gr) {
        Iterator it = collection.iterator();
        while (it.hasNext()) {
            ((C0422Ft.C0423a) it.next()).mo2226a(this);
        }
    }

    /* renamed from: S */
    public void mo10S() {
        super.mo10S();
    }

    /* renamed from: T */
    public C6494any mo11T() {
        return ___iScriptClass;
    }

    /* renamed from: U */
    public C2491fm[] mo12U() {
        return _m_methods;
    }

    /* renamed from: W */
    public Object mo13W() {
        return new aTG(this);
    }

    /* renamed from: a */
    public Object mo14a(C0495Gr gr) {
        Object[] args = gr.getArgs();
        switch (gr.mo2417hq() - TaikodomObject._m_methodCount) {
            case 0:
                m3500aG((Player) args[0]);
                return null;
            case 1:
                m3502aI((Player) args[0]);
                return null;
            case 2:
                aQP();
                return null;
            case 3:
                m3515g((Player) args[0], (String) args[1]);
                return null;
            case 4:
                m3512c((I18NString) args[0], (Object[]) args[1]);
                return null;
            case 5:
                return aQR();
            case 6:
                return new Integer(m3497QD());
            case 7:
                return aQT();
            case 8:
                m3504aK((Player) args[0]);
                return null;
            case 9:
                return new Boolean(m3505aM((Player) args[0]));
            case 10:
                return new Boolean(m3506aO((Player) args[0]));
            case 11:
                return new Boolean(m3507aQ((Player) args[0]));
            case 12:
                m3508aS((Player) args[0]);
                return null;
            case 13:
                m3511c((Player) args[0], ((Boolean) args[1]).booleanValue());
                return null;
            case 14:
                return new Boolean(m3509aU((Player) args[0]));
            case 15:
                return new Boolean(aQV());
            case 16:
                m3514fg();
                return null;
            default:
                return super.mo14a(gr);
        }
    }

    /* renamed from: a */
    public void mo15a(Collection collection, C0495Gr gr) {
        switch (gr.mo2417hq() - TaikodomObject._m_methodCount) {
            case 0:
                m3516g(collection, gr);
                return;
            case 1:
                m3517h(collection, gr);
                return;
            case 2:
                m3518i(collection, gr);
                return;
            default:
                super.mo15a(collection, gr);
                return;
        }
    }

    /* renamed from: aL */
    public void mo2419aL(Player aku) {
        switch (bFf().mo6893i(cYH)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, cYH, new Object[]{aku}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, cYH, new Object[]{aku}));
                break;
        }
        m3504aK(aku);
    }

    @C5566aOg
    /* renamed from: aN */
    public boolean mo2420aN(Player aku) {
        switch (bFf().mo6893i(cYI)) {
            case 0:
                return false;
            case 2:
                return ((Boolean) bFf().mo5606d(new aCE(this, cYI, new Object[]{aku}))).booleanValue();
            case 3:
                bFf().mo5606d(new aCE(this, cYI, new Object[]{aku}));
                break;
        }
        return m3505aM(aku);
    }

    @C5566aOg
    /* renamed from: aP */
    public boolean mo2421aP(Player aku) {
        switch (bFf().mo6893i(cYJ)) {
            case 0:
                return false;
            case 2:
                return ((Boolean) bFf().mo5606d(new aCE(this, cYJ, new Object[]{aku}))).booleanValue();
            case 3:
                bFf().mo5606d(new aCE(this, cYJ, new Object[]{aku}));
                break;
        }
        return m3506aO(aku);
    }

    public List<Player> aQS() {
        switch (bFf().mo6893i(cYF)) {
            case 0:
                return null;
            case 2:
                return (List) bFf().mo5606d(new aCE(this, cYF, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, cYF, new Object[0]));
                break;
        }
        return aQR();
    }

    public Player aQU() {
        switch (bFf().mo6893i(cYG)) {
            case 0:
                return null;
            case 2:
                return (Player) bFf().mo5606d(new aCE(this, cYG, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, cYG, new Object[0]));
                break;
        }
        return aQT();
    }

    @C5566aOg
    /* renamed from: aR */
    public boolean mo2424aR(Player aku) {
        switch (bFf().mo6893i(cYK)) {
            case 0:
                return false;
            case 2:
                return ((Boolean) bFf().mo5606d(new aCE(this, cYK, new Object[]{aku}))).booleanValue();
            case 3:
                bFf().mo5606d(new aCE(this, cYK, new Object[]{aku}));
                break;
        }
        return m3507aQ(aku);
    }

    @C5566aOg
    /* renamed from: aT */
    public void mo2425aT(Player aku) {
        switch (bFf().mo6893i(cYL)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, cYL, new Object[]{aku}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, cYL, new Object[]{aku}));
                break;
        }
        m3508aS(aku);
    }

    /* renamed from: aV */
    public boolean mo2426aV(Player aku) {
        switch (bFf().mo6893i(cYN)) {
            case 0:
                return false;
            case 2:
                return ((Boolean) bFf().mo5606d(new aCE(this, cYN, new Object[]{aku}))).booleanValue();
            case 3:
                bFf().mo5606d(new aCE(this, cYN, new Object[]{aku}));
                break;
        }
        return m3509aU(aku);
    }

    /* renamed from: c */
    public C5663aRz[] mo25c() {
        return _m_fields;
    }

    @C5566aOg
    /* renamed from: d */
    public void mo2428d(Player aku, boolean z) {
        switch (bFf().mo6893i(cYM)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, cYM, new Object[]{aku, new Boolean(z)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, cYM, new Object[]{aku, new Boolean(z)}));
                break;
        }
        m3511c(aku, z);
    }

    public void dispose() {
        switch (bFf().mo6893i(_f_dispose_0020_0028_0029V)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, _f_dispose_0020_0028_0029V, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, _f_dispose_0020_0028_0029V, new Object[0]));
                break;
        }
        m3514fg();
    }

    @C4034yP
    @C5566aOg
    @C2499fr
    /* renamed from: h */
    public void mo2429h(Player aku, String str) {
        switch (bFf().mo6893i(cYD)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, cYD, new Object[]{aku, str}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, cYD, new Object[]{aku, str}));
                break;
        }
        m3515g(aku, str);
    }

    public boolean isFull() {
        switch (bFf().mo6893i(cYO)) {
            case 0:
                return false;
            case 2:
                return ((Boolean) bFf().mo5606d(new aCE(this, cYO, new Object[0]))).booleanValue();
            case 3:
                bFf().mo5606d(new aCE(this, cYO, new Object[0]));
                break;
        }
        return aQV();
    }

    public int size() {
        switch (bFf().mo6893i(aJH)) {
            case 0:
                return 0;
            case 2:
                return ((Integer) bFf().mo5606d(new aCE(this, aJH, new Object[0]))).intValue();
            case 3:
                bFf().mo5606d(new aCE(this, aJH, new Object[0]));
                break;
        }
        return m3497QD();
    }

    /* renamed from: b */
    public void mo2427b(Player aku) {
        super.mo10S();
        mo2419aL(aku);
    }

    @C0064Am(aul = "4557e8974f8071754db314bcb4cf8911", aum = 0)
    /* renamed from: c */
    private void m3512c(I18NString i18NString, Object[] objArr) {
        for (Player next : aQS()) {
            next.mo14419f((C1506WA) new C0352Eg(next, i18NString, objArr));
        }
    }

    @C0064Am(aul = "46727c751f9ac4e98eb07fa6876800b3", aum = 0)
    private List<Player> aQR() {
        return Collections.unmodifiableList(aQN());
    }

    @C0064Am(aul = "bb9be25a260106f85913073464cb9294", aum = 0)
    /* renamed from: QD */
    private int m3497QD() {
        return aQN().size();
    }

    @C0064Am(aul = "544e06bbd85c68038c3657b65d4cc6cb", aum = 0)
    private Player aQT() {
        return aQO();
    }

    @C0064Am(aul = "c5d4d9ffb46d835cb5a0977d60c89953", aum = 0)
    /* renamed from: aK */
    private void m3504aK(Player aku) {
        m3499aF(aku);
        aQQ();
    }

    @C0064Am(aul = "6c7c7825bc17183da14435ba9c4bcf4d", aum = 0)
    /* renamed from: aU */
    private boolean m3509aU(Player aku) {
        return aQN().contains(aku);
    }

    @C0064Am(aul = "685e36c2546e9e83f6467cb51cf196e4", aum = 0)
    private boolean aQV() {
        return size() == 6;
    }

    @C0064Am(aul = "2f22b13185dab9d3683709ce39733341", aum = 0)
    /* renamed from: fg */
    private void m3514fg() {
        for (Player ko : new ArrayList(aQN())) {
            ko.mo14428ko(false);
        }
        super.dispose();
    }
}
