package game.script.crafting;

import game.network.message.externalizable.aCE;
import game.script.item.ItemType;
import logic.baa.*;
import logic.data.mbean.C3702tz;
import logic.res.code.C5640aRc;
import logic.res.code.C5663aRz;
import logic.res.code.C6494any;
import logic.res.html.C2491fm;
import p001a.*;

import java.util.Collection;
import java.util.UUID;

@C5511aMd
@C6485anp
/* renamed from: a.B */
/* compiled from: a */
public class CraftingRecipe extends aDJ implements C0468GU, C1616Xf {
    public static final C2491fm _f_toString_0020_0028_0029Ljava_002flang_002fString_003b = null;
    public static final int _m_fieldCount = 0;
    public static final C5663aRz[] _m_fields = null;
    public static final int _m_methodCount = 0;
    public static final C2491fm[] _m_methods = null;
    /* renamed from: bL */
    public static final C5663aRz f107bL = null;
    /* renamed from: bM */
    public static final C5663aRz f108bM = null;
    /* renamed from: bN */
    public static final C2491fm f109bN = null;
    /* renamed from: bO */
    public static final C2491fm f110bO = null;
    /* renamed from: bP */
    public static final C2491fm f111bP = null;
    /* renamed from: bQ */
    public static final C2491fm f112bQ = null;
    /* renamed from: cA */
    public static final C2491fm f113cA = null;
    /* renamed from: cB */
    public static final C2491fm f114cB = null;
    /* renamed from: cC */
    public static final C2491fm f115cC = null;
    /* renamed from: cD */
    public static final C2491fm f116cD = null;
    /* renamed from: cx */
    public static final C5663aRz f118cx = null;
    /* renamed from: cz */
    public static final C5663aRz f120cz = null;
    public static final long serialVersionUID = 0;
    public static C6494any ___iScriptClass;
    @C0064Am(aul = "8b1ea148a6b532696cb9b9bc8fcd697a", aum = 0)

    /* renamed from: bK */
    private static UUID f106bK;
    @C0064Am(aul = "94ed4e4b7bfb1a95849305323cffbb4a", aum = 2)

    /* renamed from: cw */
    private static ItemType f117cw;
    @C0064Am(aul = "54635b04f53a4e60307e5e963eaad0ba", aum = 3)

    /* renamed from: cy */
    private static int f119cy;
    @C0064Am(aul = "f7cb00eebe7f51a3772f75e08cfa3ecc", aum = 1)
    private static String handle;

    static {
        m704V();
    }

    public CraftingRecipe() {
        super((C5540aNg) null);
        super.mo10S();
    }

    public CraftingRecipe(C5540aNg ang) {
        super(ang);
    }

    /* renamed from: V */
    static void m704V() {
        serialVersionUID = (long) 1;
        _m_fieldCount = aDJ._m_fieldCount + 4;
        _m_methodCount = aDJ._m_methodCount + 9;
        int i = aDJ._m_fieldCount;
        C5663aRz[] arzArr = new C5663aRz[(i + 4)];
        C5663aRz b = C5640aRc.m17844b(CraftingRecipe.class, "8b1ea148a6b532696cb9b9bc8fcd697a", i);
        f107bL = b;
        arzArr[i] = b;
        int i2 = i + 1;
        C5663aRz b2 = C5640aRc.m17844b(CraftingRecipe.class, "f7cb00eebe7f51a3772f75e08cfa3ecc", i2);
        f108bM = b2;
        arzArr[i2] = b2;
        int i3 = i2 + 1;
        C5663aRz b3 = C5640aRc.m17844b(CraftingRecipe.class, "94ed4e4b7bfb1a95849305323cffbb4a", i3);
        f118cx = b3;
        arzArr[i3] = b3;
        int i4 = i3 + 1;
        C5663aRz b4 = C5640aRc.m17844b(CraftingRecipe.class, "54635b04f53a4e60307e5e963eaad0ba", i4);
        f120cz = b4;
        arzArr[i4] = b4;
        int i5 = i4 + 1;
        _m_fields = arzArr;
        C1634Xv.m11725a((Object[]) aDJ._m_fields, (Object[]) _m_fields);
        int i6 = aDJ._m_methodCount;
        C2491fm[] fmVarArr = new C2491fm[(i6 + 9)];
        C2491fm a = C4105zY.m41624a(CraftingRecipe.class, "bd1b3a6de6edccc95dbf58c910bc9773", i6);
        f109bN = a;
        fmVarArr[i6] = a;
        int i7 = i6 + 1;
        C2491fm a2 = C4105zY.m41624a(CraftingRecipe.class, "96e7f090e694777ddc87c0fc8bc19ff9", i7);
        f110bO = a2;
        fmVarArr[i7] = a2;
        int i8 = i7 + 1;
        C2491fm a3 = C4105zY.m41624a(CraftingRecipe.class, "ca57085bf4c55adaa82de3200aafd347", i8);
        f111bP = a3;
        fmVarArr[i8] = a3;
        int i9 = i8 + 1;
        C2491fm a4 = C4105zY.m41624a(CraftingRecipe.class, "e93f33834496d268623a95572875cd4d", i9);
        f112bQ = a4;
        fmVarArr[i9] = a4;
        int i10 = i9 + 1;
        C2491fm a5 = C4105zY.m41624a(CraftingRecipe.class, "35745e23fec6bda02185d6c9f2e50721", i10);
        _f_toString_0020_0028_0029Ljava_002flang_002fString_003b = a5;
        fmVarArr[i10] = a5;
        int i11 = i10 + 1;
        C2491fm a6 = C4105zY.m41624a(CraftingRecipe.class, "e16cdab07cebe1ab142680b055a0e12b", i11);
        f113cA = a6;
        fmVarArr[i11] = a6;
        int i12 = i11 + 1;
        C2491fm a7 = C4105zY.m41624a(CraftingRecipe.class, "b439e11750f7d3ae4adf87b0fe7a6294", i12);
        f114cB = a7;
        fmVarArr[i12] = a7;
        int i13 = i12 + 1;
        C2491fm a8 = C4105zY.m41624a(CraftingRecipe.class, "2772e7c6e78729e6006e3964bc7d751f", i13);
        f115cC = a8;
        fmVarArr[i13] = a8;
        int i14 = i13 + 1;
        C2491fm a9 = C4105zY.m41624a(CraftingRecipe.class, "c2571f09aba45ce355cf2862903de08e", i14);
        f116cD = a9;
        fmVarArr[i14] = a9;
        int i15 = i14 + 1;
        _m_methods = fmVarArr;
        C1634Xv.m11725a((Object[]) aDJ._m_methods, (Object[]) _m_methods);
        ___iScriptClass = aUO.m18566a(CraftingRecipe.class, C3702tz.class, _m_fields, _m_methods);
    }

    /* renamed from: a */
    private void m705a(ItemType jCVar) {
        bFf().mo5608dq().mo3197f(f118cx, jCVar);
    }

    /* renamed from: a */
    private void m706a(String str) {
        bFf().mo5608dq().mo3197f(f108bM, str);
    }

    /* renamed from: a */
    private void m707a(UUID uuid) {
        bFf().mo5608dq().mo3197f(f107bL, uuid);
    }

    /* renamed from: an */
    private UUID m708an() {
        return (UUID) bFf().mo5608dq().mo3214p(f107bL);
    }

    /* renamed from: ao */
    private String m709ao() {
        return (String) bFf().mo5608dq().mo3214p(f108bM);
    }

    /* renamed from: av */
    private ItemType m713av() {
        return (ItemType) bFf().mo5608dq().mo3214p(f118cx);
    }

    /* renamed from: aw */
    private int m714aw() {
        return bFf().mo5608dq().mo3212n(f120cz);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Item Type")
    @C0064Am(aul = "c2571f09aba45ce355cf2862903de08e", aum = 0)
    @C5566aOg
    /* renamed from: b */
    private void m717b(ItemType jCVar) {
        throw new aWi(new aCE(this, f116cD, new Object[]{jCVar}));
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Handle")
    @C0064Am(aul = "e93f33834496d268623a95572875cd4d", aum = 0)
    @C5566aOg
    /* renamed from: b */
    private void m718b(String str) {
        throw new aWi(new aCE(this, f112bQ, new Object[]{str}));
    }

    /* renamed from: c */
    private void m720c(UUID uuid) {
        switch (bFf().mo6893i(f110bO)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f110bO, new Object[]{uuid}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f110bO, new Object[]{uuid}));
                break;
        }
        m719b(uuid);
    }

    /* renamed from: f */
    private void m721f(int i) {
        bFf().mo5608dq().mo3183b(f120cz, i);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Amount")
    @C0064Am(aul = "b439e11750f7d3ae4adf87b0fe7a6294", aum = 0)
    @C5566aOg
    /* renamed from: g */
    private void m722g(int i) {
        throw new aWi(new aCE(this, f114cB, new Object[]{new Integer(i)}));
    }

    /* renamed from: T */
    public C6494any mo11T() {
        return ___iScriptClass;
    }

    /* renamed from: U */
    public C2491fm[] mo12U() {
        return _m_methods;
    }

    /* renamed from: W */
    public Object mo13W() {
        return new C3702tz(this);
    }

    /* renamed from: a */
    public Object mo14a(C0495Gr gr) {
        Object[] args = gr.getArgs();
        switch (gr.mo2417hq() - aDJ._m_methodCount) {
            case 0:
                return m710ap();
            case 1:
                m719b((UUID) args[0]);
                return null;
            case 2:
                return m711ar();
            case 3:
                m718b((String) args[0]);
                return null;
            case 4:
                return m712au();
            case 5:
                return new Integer(m715ax());
            case 6:
                m722g(((Integer) args[0]).intValue());
                return null;
            case 7:
                return m716ay();
            case 8:
                m717b((ItemType) args[0]);
                return null;
            default:
                return super.mo14a(gr);
        }
    }

    /* renamed from: a */
    public void mo15a(Collection collection, C0495Gr gr) {
        super.mo15a(collection, gr);
    }

    /* renamed from: aq */
    public UUID mo18aq() {
        switch (bFf().mo6893i(f109bN)) {
            case 0:
                return null;
            case 2:
                return (UUID) bFf().mo5606d(new aCE(this, f109bN, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f109bN, new Object[0]));
                break;
        }
        return m710ap();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Item Type")
    /* renamed from: az */
    public ItemType mo554az() {
        switch (bFf().mo6893i(f115cC)) {
            case 0:
                return null;
            case 2:
                return (ItemType) bFf().mo5606d(new aCE(this, f115cC, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f115cC, new Object[0]));
                break;
        }
        return m716ay();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Item Type")
    @C5566aOg
    /* renamed from: c */
    public void mo555c(ItemType jCVar) {
        switch (bFf().mo6893i(f116cD)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f116cD, new Object[]{jCVar}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f116cD, new Object[]{jCVar}));
                break;
        }
        m717b(jCVar);
    }

    /* renamed from: c */
    public C5663aRz[] mo25c() {
        return _m_fields;
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Amount")
    public int getAmount() {
        switch (bFf().mo6893i(f113cA)) {
            case 0:
                return 0;
            case 2:
                return ((Integer) bFf().mo5606d(new aCE(this, f113cA, new Object[0]))).intValue();
            case 3:
                bFf().mo5606d(new aCE(this, f113cA, new Object[0]));
                break;
        }
        return m715ax();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Amount")
    @C5566aOg
    public void setAmount(int i) {
        switch (bFf().mo6893i(f114cB)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f114cB, new Object[]{new Integer(i)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f114cB, new Object[]{new Integer(i)}));
                break;
        }
        m722g(i);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Handle")
    public String getHandle() {
        switch (bFf().mo6893i(f111bP)) {
            case 0:
                return null;
            case 2:
                return (String) bFf().mo5606d(new aCE(this, f111bP, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f111bP, new Object[0]));
                break;
        }
        return m711ar();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Handle")
    @C5566aOg
    public void setHandle(String str) {
        switch (bFf().mo6893i(f112bQ)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f112bQ, new Object[]{str}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f112bQ, new Object[]{str}));
                break;
        }
        m718b(str);
    }

    public String toString() {
        switch (bFf().mo6893i(_f_toString_0020_0028_0029Ljava_002flang_002fString_003b)) {
            case 0:
                return null;
            case 2:
                return (String) bFf().mo5606d(new aCE(this, _f_toString_0020_0028_0029Ljava_002flang_002fString_003b, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, _f_toString_0020_0028_0029Ljava_002flang_002fString_003b, new Object[0]));
                break;
        }
        return m712au();
    }

    @C0064Am(aul = "bd1b3a6de6edccc95dbf58c910bc9773", aum = 0)
    /* renamed from: ap */
    private UUID m710ap() {
        return m708an();
    }

    @C0064Am(aul = "96e7f090e694777ddc87c0fc8bc19ff9", aum = 0)
    /* renamed from: b */
    private void m719b(UUID uuid) {
        m707a(uuid);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Handle")
    @C0064Am(aul = "ca57085bf4c55adaa82de3200aafd347", aum = 0)
    /* renamed from: ar */
    private String m711ar() {
        return m709ao();
    }

    /* renamed from: S */
    public void mo10S() {
        super.mo10S();
        m721f(1);
        m707a(UUID.randomUUID());
    }

    @C0064Am(aul = "35745e23fec6bda02185d6c9f2e50721", aum = 0)
    /* renamed from: au */
    private String m712au() {
        return "[" + (m713av() == null ? "null" : m713av().getHandle()) + ", " + m714aw() + "]";
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Amount")
    @C0064Am(aul = "e16cdab07cebe1ab142680b055a0e12b", aum = 0)
    /* renamed from: ax */
    private int m715ax() {
        return m714aw();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Item Type")
    @C0064Am(aul = "2772e7c6e78729e6006e3964bc7d751f", aum = 0)
    /* renamed from: ay */
    private ItemType m716ay() {
        return m713av();
    }
}
