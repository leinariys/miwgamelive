package game.script.avatar;

import game.network.message.externalizable.aCE;
import game.network.message.serializable.C2686iZ;
import game.script.Character;
import game.script.avatar.body.AvatarAppearance;
import game.script.avatar.cloth.Cloth;
import game.script.resource.Asset;
import logic.baa.C1616Xf;
import logic.baa.C1634Xv;
import logic.baa.C5511aMd;
import logic.baa.C6485anp;
import logic.data.mbean.C6631aqf;
import logic.res.code.C5640aRc;
import logic.res.code.C5663aRz;
import logic.res.code.C6494any;
import logic.res.html.C2491fm;
import logic.res.sound.C0907NJ;
import logic.thred.LogPrinter;
import logic.ui.IBaseUiTegXml;
import p001a.*;
import taikodom.render.scene.RModel;

import javax.swing.*;
import java.awt.*;
import java.util.Collection;
import java.util.Collections;

@C5511aMd
@C6485anp
/* renamed from: a.Ev */
/* compiled from: a */
public class Avatar extends TaikodomObject implements C1616Xf, aVE {

    /* renamed from: Lm */
    public static final C2491fm f513Lm = null;
    public static final int _m_fieldCount = 0;
    public static final C5663aRz[] _m_fields = null;
    public static final int _m_methodCount = 0;
    public static final C2491fm[] _m_methods = null;
    public static final C5663aRz boI = null;
    public static final C2491fm bpW = null;
    public static final C2491fm brf = null;
    public static final C2491fm cGn = null;
    public static final C2491fm cSA = null;
    public static final C2491fm cSB = null;
    public static final C2491fm cSC = null;
    public static final C2491fm cSD = null;
    public static final C2491fm cSE = null;
    public static final C2491fm cSF = null;
    public static final C2491fm cSG = null;
    public static final C2491fm cSH = null;
    public static final C2491fm cSI = null;
    public static final C2491fm cSJ = null;
    public static final C2491fm cSK = null;
    public static final C2491fm cSL = null;
    public static final C2491fm cSM = null;
    public static final C2491fm cSN = null;
    public static final C2491fm cSO = null;
    public static final C2491fm cSP = null;
    public static final C2491fm cSQ = null;
    public static final C2491fm cSR = null;
    public static final C5663aRz cSl = null;
    public static final C5663aRz cSn = null;
    public static final C5663aRz cSp = null;
    public static final C5663aRz cSr = null;
    public static final C5663aRz cSt = null;
    public static final C5663aRz cSv = null;
    public static final C2491fm cSy = null;
    public static final C2491fm cSz = null;
    /* renamed from: gm */
    public static final C2491fm f514gm = null;
    /* access modifiers changed from: private */
    public static final LogPrinter logger = LogPrinter.setClass(Avatar.class);
    public static final long serialVersionUID = 0;
    public static C6494any ___iScriptClass;
    @C0064Am(aul = "a06a8e34e212fd07c85ab02bdcb90904", aum = 0)
    private static Character boH;
    @C0064Am(aul = "add67362962d94402b0ef0632d35aa6f", aum = 1)
    private static AvatarAppearance cSk;
    @C0064Am(aul = "b45a7e5036112d875e27aec02029a92f", aum = 2)
    private static C2686iZ<Cloth> cSm;
    @C0064Am(aul = "93b53c11698512e20ac7bb37a235b977", aum = 3)
    private static Cloth cSo;
    @C0064Am(aul = "6706c65d39d4eb9d35611bfb1aca6570", aum = 4)
    private static Cloth cSq;
    @C0064Am(aul = "c5b52155c860f963bec2150a4cfb933f", aum = 5)
    private static String cSs;
    @C0064Am(aul = "c9cbb477bf9b92b454e59f8ba86d00e5", aum = 6)
    private static C2686iZ<Cloth> cSu;

    static {
        m3072V();
    }

    /* access modifiers changed from: private */
    @ClientOnly
    public transient ImageIcon cSx;
    @ClientOnly
    private transient C1009Os cSw;

    public Avatar() {
        super((C5540aNg) null);
        super.mo10S();
    }

    public Avatar(C5540aNg ang) {
        super(ang);
    }

    /* renamed from: V */
    static void m3072V() {
        serialVersionUID = (long) 1;
        _m_fieldCount = TaikodomObject._m_fieldCount + 7;
        _m_methodCount = TaikodomObject._m_methodCount + 25;
        int i = TaikodomObject._m_fieldCount;
        C5663aRz[] arzArr = new C5663aRz[(i + 7)];
        C5663aRz b = C5640aRc.m17844b(Avatar.class, "a06a8e34e212fd07c85ab02bdcb90904", i);
        boI = b;
        arzArr[i] = b;
        int i2 = i + 1;
        C5663aRz b2 = C5640aRc.m17844b(Avatar.class, "add67362962d94402b0ef0632d35aa6f", i2);
        cSl = b2;
        arzArr[i2] = b2;
        int i3 = i2 + 1;
        C5663aRz b3 = C5640aRc.m17844b(Avatar.class, "b45a7e5036112d875e27aec02029a92f", i3);
        cSn = b3;
        arzArr[i3] = b3;
        int i4 = i3 + 1;
        C5663aRz b4 = C5640aRc.m17844b(Avatar.class, "93b53c11698512e20ac7bb37a235b977", i4);
        cSp = b4;
        arzArr[i4] = b4;
        int i5 = i4 + 1;
        C5663aRz b5 = C5640aRc.m17844b(Avatar.class, "6706c65d39d4eb9d35611bfb1aca6570", i5);
        cSr = b5;
        arzArr[i5] = b5;
        int i6 = i5 + 1;
        C5663aRz b6 = C5640aRc.m17844b(Avatar.class, "c5b52155c860f963bec2150a4cfb933f", i6);
        cSt = b6;
        arzArr[i6] = b6;
        int i7 = i6 + 1;
        C5663aRz b7 = C5640aRc.m17844b(Avatar.class, "c9cbb477bf9b92b454e59f8ba86d00e5", i7);
        cSv = b7;
        arzArr[i7] = b7;
        int i8 = i7 + 1;
        _m_fields = arzArr;
        C1634Xv.m11725a((Object[]) TaikodomObject._m_fields, (Object[]) _m_fields);
        int i9 = TaikodomObject._m_methodCount;
        C2491fm[] fmVarArr = new C2491fm[(i9 + 25)];
        C2491fm a = C4105zY.m41624a(Avatar.class, "0ef13c5c22f994f3f8162b67f701e277", i9);
        cSy = a;
        fmVarArr[i9] = a;
        int i10 = i9 + 1;
        C2491fm a2 = C4105zY.m41624a(Avatar.class, "14bf1a6c0ab0155b40c14cff3ea9642a", i10);
        cSz = a2;
        fmVarArr[i10] = a2;
        int i11 = i10 + 1;
        C2491fm a3 = C4105zY.m41624a(Avatar.class, "90cea825db1a03dfa074eae268626ce1", i11);
        cSA = a3;
        fmVarArr[i11] = a3;
        int i12 = i11 + 1;
        C2491fm a4 = C4105zY.m41624a(Avatar.class, "7110e50ebbb9d7ee65d8f7816acf2232", i12);
        cSB = a4;
        fmVarArr[i12] = a4;
        int i13 = i12 + 1;
        C2491fm a5 = C4105zY.m41624a(Avatar.class, "d51c217ef501ad44eac9a0aff7bcf1ed", i13);
        bpW = a5;
        fmVarArr[i13] = a5;
        int i14 = i13 + 1;
        C2491fm a6 = C4105zY.m41624a(Avatar.class, "d2d87a69244b73b17936f9b05d6ec25f", i14);
        brf = a6;
        fmVarArr[i14] = a6;
        int i15 = i14 + 1;
        C2491fm a7 = C4105zY.m41624a(Avatar.class, "93bedbe887450ddddf3ee23ae056535f", i15);
        cSC = a7;
        fmVarArr[i15] = a7;
        int i16 = i15 + 1;
        C2491fm a8 = C4105zY.m41624a(Avatar.class, "1570c6d1e353fc8345dd549d63b4a019", i16);
        cSD = a8;
        fmVarArr[i16] = a8;
        int i17 = i16 + 1;
        C2491fm a9 = C4105zY.m41624a(Avatar.class, "5ac3dacecfc0c6ce1a8eaaf1c4e8c1e3", i17);
        cSE = a9;
        fmVarArr[i17] = a9;
        int i18 = i17 + 1;
        C2491fm a10 = C4105zY.m41624a(Avatar.class, "8ffcda8fd9a9feeaa11a5f3c29fe1354", i18);
        cSF = a10;
        fmVarArr[i18] = a10;
        int i19 = i18 + 1;
        C2491fm a11 = C4105zY.m41624a(Avatar.class, "c7ee9dbb1fb20ea4908b94d4890753b9", i19);
        cSG = a11;
        fmVarArr[i19] = a11;
        int i20 = i19 + 1;
        C2491fm a12 = C4105zY.m41624a(Avatar.class, "0ee7258a60664d386ae5076e9479929f", i20);
        cSH = a12;
        fmVarArr[i20] = a12;
        int i21 = i20 + 1;
        C2491fm a13 = C4105zY.m41624a(Avatar.class, "68394520a0c58fa3ea344a24b74026a0", i21);
        cSI = a13;
        fmVarArr[i21] = a13;
        int i22 = i21 + 1;
        C2491fm a14 = C4105zY.m41624a(Avatar.class, "fd99002b4e826b37ae444113258d7e94", i22);
        cSJ = a14;
        fmVarArr[i22] = a14;
        int i23 = i22 + 1;
        C2491fm a15 = C4105zY.m41624a(Avatar.class, "a781df9733699714f4b3ba7fd0c773af", i23);
        cSK = a15;
        fmVarArr[i23] = a15;
        int i24 = i23 + 1;
        C2491fm a16 = C4105zY.m41624a(Avatar.class, "f494e0aaafa9de986312c71537688ad8", i24);
        cSL = a16;
        fmVarArr[i24] = a16;
        int i25 = i24 + 1;
        C2491fm a17 = C4105zY.m41624a(Avatar.class, "6ebca36624f2dcd03a192856ed234dd0", i25);
        f514gm = a17;
        fmVarArr[i25] = a17;
        int i26 = i25 + 1;
        C2491fm a18 = C4105zY.m41624a(Avatar.class, "30e8a95b860d72cc66508adf3054c175", i26);
        cGn = a18;
        fmVarArr[i26] = a18;
        int i27 = i26 + 1;
        C2491fm a19 = C4105zY.m41624a(Avatar.class, "091dc1c2ee33b5a2abf83f6d9c78b370", i27);
        cSM = a19;
        fmVarArr[i27] = a19;
        int i28 = i27 + 1;
        C2491fm a20 = C4105zY.m41624a(Avatar.class, "7eb8d7860083e4fc758cd3cc75c8d373", i28);
        cSN = a20;
        fmVarArr[i28] = a20;
        int i29 = i28 + 1;
        C2491fm a21 = C4105zY.m41624a(Avatar.class, "8717e35bf38b53689a518698cbfb47c5", i29);
        cSO = a21;
        fmVarArr[i29] = a21;
        int i30 = i29 + 1;
        C2491fm a22 = C4105zY.m41624a(Avatar.class, "c4974d8277a622e036fc8b9a3ce8c327", i30);
        cSP = a22;
        fmVarArr[i30] = a22;
        int i31 = i30 + 1;
        C2491fm a23 = C4105zY.m41624a(Avatar.class, "6ec33adec16b1f8527a11b23be168f59", i31);
        cSQ = a23;
        fmVarArr[i31] = a23;
        int i32 = i31 + 1;
        C2491fm a24 = C4105zY.m41624a(Avatar.class, "5461ca454901c1c2ba1445c7944075c2", i32);
        cSR = a24;
        fmVarArr[i32] = a24;
        int i33 = i32 + 1;
        C2491fm a25 = C4105zY.m41624a(Avatar.class, "4ac2aa831e16c028741a1d3ae7996bb3", i33);
        f513Lm = a25;
        fmVarArr[i33] = a25;
        int i34 = i33 + 1;
        _m_methods = fmVarArr;
        C1634Xv.m11725a((Object[]) TaikodomObject._m_methods, (Object[]) _m_methods);
        ___iScriptClass = aUO.m18566a(Avatar.class, C6631aqf.class, _m_fields, _m_methods);
    }

    /* renamed from: F */
    private void m3070F(C2686iZ iZVar) {
        bFf().mo5608dq().mo3197f(cSn, iZVar);
    }

    /* renamed from: G */
    private void m3071G(C2686iZ iZVar) {
        bFf().mo5608dq().mo3197f(cSv, iZVar);
    }

    /* renamed from: a */
    private void m3076a(AvatarAppearance aoz) {
        bFf().mo5608dq().mo3197f(cSl, aoz);
    }

    private AvatarAppearance aOd() {
        return (AvatarAppearance) bFf().mo5608dq().mo3214p(cSl);
    }

    private C2686iZ aOe() {
        return (C2686iZ) bFf().mo5608dq().mo3214p(cSn);
    }

    private Cloth aOf() {
        return (Cloth) bFf().mo5608dq().mo3214p(cSp);
    }

    private Cloth aOg() {
        return (Cloth) bFf().mo5608dq().mo3214p(cSr);
    }

    private String aOh() {
        return (String) bFf().mo5608dq().mo3214p(cSt);
    }

    private C2686iZ aOi() {
        return (C2686iZ) bFf().mo5608dq().mo3214p(cSv);
    }

    @C0064Am(aul = "7eb8d7860083e4fc758cd3cc75c8d373", aum = 0)
    @C5566aOg
    @C2499fr
    private void aOz() {
        throw new aWi(new aCE(this, cSN, new Object[0]));
    }

    private Character afm() {
        return (Character) bFf().mo5608dq().mo3214p(boI);
    }

    @C0064Am(aul = "7110e50ebbb9d7ee65d8f7816acf2232", aum = 0)
    @C5566aOg
    /* renamed from: b */
    private void m3078b(AvatarAppearance aoz) {
        throw new aWi(new aCE(this, cSB, new Object[]{aoz}));
    }

    /* renamed from: el */
    private void m3081el(String str) {
        bFf().mo5608dq().mo3197f(cSt, str);
    }

    /* renamed from: i */
    private void m3082i(Character acx) {
        bFf().mo5608dq().mo3197f(boI, acx);
    }

    @C0064Am(aul = "d2d87a69244b73b17936f9b05d6ec25f", aum = 0)
    @C5566aOg
    @C2499fr
    /* renamed from: j */
    private void m3083j(Character acx) {
        throw new aWi(new aCE(this, brf, new Object[]{acx}));
    }

    /* renamed from: o */
    private void m3084o(Cloth wj) {
        bFf().mo5608dq().mo3197f(cSp, wj);
    }

    /* renamed from: p */
    private void m3085p(Cloth wj) {
        bFf().mo5608dq().mo3197f(cSr, wj);
    }

    @C0064Am(aul = "5ac3dacecfc0c6ce1a8eaaf1c4e8c1e3", aum = 0)
    @C5566aOg
    @C2499fr
    /* renamed from: q */
    private void m3086q(Cloth wj) {
        throw new aWi(new aCE(this, cSE, new Object[]{wj}));
    }

    @C0064Am(aul = "8ffcda8fd9a9feeaa11a5f3c29fe1354", aum = 0)
    @C5566aOg
    @C2499fr
    /* renamed from: s */
    private void m3088s(Cloth wj) {
        throw new aWi(new aCE(this, cSF, new Object[]{wj}));
    }

    /* renamed from: T */
    public C6494any mo11T() {
        return ___iScriptClass;
    }

    /* renamed from: U */
    public C2491fm[] mo12U() {
        return _m_methods;
    }

    /* renamed from: W */
    public Object mo13W() {
        return new C6631aqf(this);
    }

    /* renamed from: a */
    public Object mo14a(C0495Gr gr) {
        Object[] args = gr.getArgs();
        switch (gr.mo2417hq() - TaikodomObject._m_methodCount) {
            case 0:
                return aOj();
            case 1:
                m3079c((ImageIcon) args[0]);
                return null;
            case 2:
                return aOl();
            case 3:
                m3078b((AvatarAppearance) args[0]);
                return null;
            case 4:
                return agi();
            case 5:
                m3083j((Character) args[0]);
                return null;
            case 6:
                return aOn();
            case 7:
                return aOp();
            case 8:
                m3086q((Cloth) args[0]);
                return null;
            case 9:
                m3088s((Cloth) args[0]);
                return null;
            case 10:
                m3089u((Cloth) args[0]);
                return null;
            case 11:
                m3090w((Cloth) args[0]);
                return null;
            case 12:
                m3077aj((Asset) args[0]);
                return null;
            case 13:
                return aOr();
            case 14:
                return aOt();
            case 15:
                return aOv();
            case 16:
                return m3080cI();
            case 17:
                return aFH();
            case 18:
                return aOx();
            case 19:
                aOz();
                return null;
            case 20:
                return new Float(aOB());
            case 21:
                return new Float(aOC());
            case 22:
                m3074a((C0907NJ) args[0]);
                return null;
            case 23:
                m3075a((C1009Os.C1019i) args[0]);
                return null;
            case 24:
                m3087qU();
                return null;
            default:
                return super.mo14a(gr);
        }
    }

    /* renamed from: a */
    public void mo15a(Collection collection, C0495Gr gr) {
        super.mo15a(collection, gr);
    }

    public Collection<AvatarSector> aFI() {
        switch (bFf().mo6893i(cGn)) {
            case 0:
                return null;
            case 2:
                return (Collection) bFf().mo5606d(new aCE(this, cGn, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, cGn, new Object[0]));
                break;
        }
        return aFH();
    }

    @C5566aOg
    @C2499fr
    public void aOA() {
        switch (bFf().mo6893i(cSN)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, cSN, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, cSN, new Object[0]));
                break;
        }
        aOz();
    }

    public float aOD() {
        switch (bFf().mo6893i(cSP)) {
            case 0:
                return 0.0f;
            case 2:
                return ((Float) bFf().mo5606d(new aCE(this, cSP, new Object[0]))).floatValue();
            case 3:
                bFf().mo5606d(new aCE(this, cSP, new Object[0]));
                break;
        }
        return aOC();
    }

    @ClientOnly
    public ImageIcon aOk() {
        switch (bFf().mo6893i(cSy)) {
            case 0:
                return null;
            case 2:
                return (ImageIcon) bFf().mo5606d(new aCE(this, cSy, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, cSy, new Object[0]));
                break;
        }
        return aOj();
    }

    public C5207aAl aOm() {
        switch (bFf().mo6893i(cSA)) {
            case 0:
                return null;
            case 2:
                return (C5207aAl) bFf().mo5606d(new aCE(this, cSA, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, cSA, new Object[0]));
                break;
        }
        return aOl();
    }

    @ClientOnly
    public RModel aOo() {
        switch (bFf().mo6893i(cSC)) {
            case 0:
                return null;
            case 2:
                return (RModel) bFf().mo5606d(new aCE(this, cSC, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, cSC, new Object[0]));
                break;
        }
        return aOn();
    }

    @ClientOnly
    public C1009Os aOq() {
        switch (bFf().mo6893i(cSD)) {
            case 0:
                return null;
            case 2:
                return (C1009Os) bFf().mo5606d(new aCE(this, cSD, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, cSD, new Object[0]));
                break;
        }
        return aOp();
    }

    public Cloth aOs() {
        switch (bFf().mo6893i(cSJ)) {
            case 0:
                return null;
            case 2:
                return (Cloth) bFf().mo5606d(new aCE(this, cSJ, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, cSJ, new Object[0]));
                break;
        }
        return aOr();
    }

    public Collection<Cloth> aOu() {
        switch (bFf().mo6893i(cSK)) {
            case 0:
                return null;
            case 2:
                return (Collection) bFf().mo5606d(new aCE(this, cSK, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, cSK, new Object[0]));
                break;
        }
        return aOt();
    }

    public Cloth aOw() {
        switch (bFf().mo6893i(cSL)) {
            case 0:
                return null;
            case 2:
                return (Cloth) bFf().mo5606d(new aCE(this, cSL, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, cSL, new Object[0]));
                break;
        }
        return aOv();
    }

    public String aOy() {
        switch (bFf().mo6893i(cSM)) {
            case 0:
                return null;
            case 2:
                return (String) bFf().mo5606d(new aCE(this, cSM, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, cSM, new Object[0]));
                break;
        }
        return aOx();
    }

    public Character agj() {
        switch (bFf().mo6893i(bpW)) {
            case 0:
                return null;
            case 2:
                return (Character) bFf().mo5606d(new aCE(this, bpW, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, bpW, new Object[0]));
                break;
        }
        return agi();
    }

    @C4034yP
    @C2499fr
    @ClientOnly
    /* renamed from: ak */
    public void mo2026ak(Asset tCVar) {
        switch (bFf().mo6893i(cSI)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, cSI, new Object[]{tCVar}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, cSI, new Object[]{tCVar}));
                break;
        }
        m3077aj(tCVar);
    }

    @ClientOnly
    /* renamed from: b */
    public void mo2027b(C0907NJ nj) {
        switch (bFf().mo6893i(cSQ)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, cSQ, new Object[]{nj}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, cSQ, new Object[]{nj}));
                break;
        }
        m3074a(nj);
    }

    /* renamed from: b */
    public void mo2028b(C1009Os.C1019i iVar) {
        switch (bFf().mo6893i(cSR)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, cSR, new Object[]{iVar}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, cSR, new Object[]{iVar}));
                break;
        }
        m3075a(iVar);
    }

    @C5566aOg
    /* renamed from: c */
    public void mo2029c(AvatarAppearance aoz) {
        switch (bFf().mo6893i(cSB)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, cSB, new Object[]{aoz}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, cSB, new Object[]{aoz}));
                break;
        }
        m3078b(aoz);
    }

    /* renamed from: c */
    public C5663aRz[] mo25c() {
        return _m_fields;
    }

    /* renamed from: cJ */
    public Asset mo2030cJ() {
        switch (bFf().mo6893i(f514gm)) {
            case 0:
                return null;
            case 2:
                return (Asset) bFf().mo5606d(new aCE(this, f514gm, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f514gm, new Object[0]));
                break;
        }
        return m3080cI();
    }

    @ClientOnly
    /* renamed from: d */
    public void mo2031d(ImageIcon imageIcon) {
        switch (bFf().mo6893i(cSz)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, cSz, new Object[]{imageIcon}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, cSz, new Object[]{imageIcon}));
                break;
        }
        m3079c(imageIcon);
    }

    public float getHeight() {
        switch (bFf().mo6893i(cSO)) {
            case 0:
                return 0.0f;
            case 2:
                return ((Float) bFf().mo5606d(new aCE(this, cSO, new Object[0]))).floatValue();
            case 3:
                bFf().mo5606d(new aCE(this, cSO, new Object[0]));
                break;
        }
        return aOB();
    }

    @C5566aOg
    @C2499fr
    /* renamed from: k */
    public void mo2033k(Character acx) {
        switch (bFf().mo6893i(brf)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, brf, new Object[]{acx}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, brf, new Object[]{acx}));
                break;
        }
        m3083j(acx);
    }

    /* renamed from: qV */
    public void mo2034qV() {
        switch (bFf().mo6893i(f513Lm)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f513Lm, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f513Lm, new Object[0]));
                break;
        }
        m3087qU();
    }

    @C5566aOg
    @C2499fr
    /* renamed from: r */
    public void mo2035r(Cloth wj) {
        switch (bFf().mo6893i(cSE)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, cSE, new Object[]{wj}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, cSE, new Object[]{wj}));
                break;
        }
        m3086q(wj);
    }

    @C5566aOg
    @C2499fr
    /* renamed from: t */
    public void mo2036t(Cloth wj) {
        switch (bFf().mo6893i(cSF)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, cSF, new Object[]{wj}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, cSF, new Object[]{wj}));
                break;
        }
        m3088s(wj);
    }

    @C4034yP
    @C2499fr
    @ClientOnly
    /* renamed from: v */
    public void mo2037v(Cloth wj) {
        switch (bFf().mo6893i(cSG)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, cSG, new Object[]{wj}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, cSG, new Object[]{wj}));
                break;
        }
        m3089u(wj);
    }

    @C4034yP
    @C2499fr
    @ClientOnly
    /* renamed from: x */
    public void mo2038x(Cloth wj) {
        switch (bFf().mo6893i(cSH)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, cSH, new Object[]{wj}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, cSH, new Object[]{wj}));
                break;
        }
        m3090w(wj);
    }

    /* renamed from: S */
    public void mo10S() {
        super.mo10S();
    }

    @C0064Am(aul = "0ef13c5c22f994f3f8162b67f701e277", aum = 0)
    @ClientOnly
    private ImageIcon aOj() {
        if (this.cSx == null) {
            C3649tJ.m39572a(afm(), (C3649tJ.C3650a) new C0369a());
        }
        return this.cSx;
    }

    @C0064Am(aul = "14bf1a6c0ab0155b40c14cff3ea9642a", aum = 0)
    @ClientOnly
    /* renamed from: c */
    private void m3079c(ImageIcon imageIcon) {
        this.cSx = imageIcon;
    }

    @C0064Am(aul = "90cea825db1a03dfa074eae268626ce1", aum = 0)
    private C5207aAl aOl() {
        return aOd();
    }

    @C0064Am(aul = "d51c217ef501ad44eac9a0aff7bcf1ed", aum = 0)
    private Character agi() {
        return afm();
    }

    @C0064Am(aul = "93bedbe887450ddddf3ee23ae056535f", aum = 0)
    @ClientOnly
    private RModel aOn() {
        if (aOq() == null) {
            return null;
        }
        return aOq().aOo();
    }

    @C0064Am(aul = "1570c6d1e353fc8345dd549d63b4a019", aum = 0)
    @ClientOnly
    private C1009Os aOp() {
        if (aOd() == null) {
            return null;
        }
        if (this.cSw == null) {
            this.cSw = new C1009Os(this);
        }
        return this.cSw;
    }

    @C4034yP
    @ClientOnly
    @C0064Am(aul = "c7ee9dbb1fb20ea4908b94d4890753b9", aum = 0)
    @C2499fr
    /* renamed from: u */
    private void m3089u(Cloth wj) {
        if (this.cSw != null) {
            this.cSw.mo4535B(wj);
        }
    }

    @C4034yP
    @ClientOnly
    @C0064Am(aul = "0ee7258a60664d386ae5076e9479929f", aum = 0)
    @C2499fr
    /* renamed from: w */
    private void m3090w(Cloth wj) {
        if (this.cSw != null) {
            this.cSw.mo4548y(wj);
        }
    }

    @C4034yP
    @ClientOnly
    @C0064Am(aul = "68394520a0c58fa3ea344a24b74026a0", aum = 0)
    @C2499fr
    /* renamed from: aj */
    private void m3077aj(Asset tCVar) {
        if (this.cSw != null && tCVar != null) {
            this.cSw.mo4538ak(tCVar);
        }
    }

    @C0064Am(aul = "fd99002b4e826b37ae444113258d7e94", aum = 0)
    private Cloth aOr() {
        return aOf();
    }

    @C0064Am(aul = "a781df9733699714f4b3ba7fd0c773af", aum = 0)
    private Collection<Cloth> aOt() {
        return Collections.unmodifiableCollection(aOe());
    }

    @C0064Am(aul = "f494e0aaafa9de986312c71537688ad8", aum = 0)
    private Cloth aOv() {
        return aOg();
    }

    @C0064Am(aul = "6ebca36624f2dcd03a192856ed234dd0", aum = 0)
    /* renamed from: cI */
    private Asset m3080cI() {
        return aOd().mo7725cJ();
    }

    @C0064Am(aul = "30e8a95b860d72cc66508adf3054c175", aum = 0)
    private Collection<AvatarSector> aFH() {
        return aOd().aFI();
    }

    @C0064Am(aul = "091dc1c2ee33b5a2abf83f6d9c78b370", aum = 0)
    private String aOx() {
        return aOh();
    }

    @C0064Am(aul = "8717e35bf38b53689a518698cbfb47c5", aum = 0)
    private float aOB() {
        return aOm().getHeight();
    }

    @C0064Am(aul = "c4974d8277a622e036fc8b9a3ce8c327", aum = 0)
    private float aOC() {
        return aOm().aOD();
    }

    @C0064Am(aul = "6ec33adec16b1f8527a11b23be168f59", aum = 0)
    @ClientOnly
    /* renamed from: a */
    private void m3074a(C0907NJ nj) {
        aOq().mo4539b(nj);
    }

    @C0064Am(aul = "5461ca454901c1c2ba1445c7944075c2", aum = 0)
    /* renamed from: a */
    private void m3075a(C1009Os.C1019i iVar) {
        if (this.cSw != null) {
            this.cSw.mo4544c(iVar);
        }
    }

    @C0064Am(aul = "4ac2aa831e16c028741a1d3ae7996bb3", aum = 0)
    /* renamed from: qU */
    private void m3087qU() {
        push();
        if (aOd() != null) {
            aOd().mo15268qV();
        }
        for (Cloth push : aOe()) {
            push.push();
        }
        for (Cloth push2 : aOi()) {
            push2.push();
        }
    }

    /* renamed from: a.Ev$a */
    class C0369a implements C3649tJ.C3650a {
        C0369a() {
        }

        /* renamed from: d */
        public void mo2039d(String str, boolean z) {
            Image image = IBaseUiTegXml.initBaseUItegXML().adz().getImage(str);
            if (image == null) {
                Avatar.logger.error("Avatar snapshot file exists, but wasnt loaded properly: data/avatarSnapshots/" + Avatar.this.aOy());
                image = C3649tJ.cFy();
            }
            Avatar.this.cSx = new ImageIcon(image);
        }
    }
}
