package game.script.avatar.body;

import game.network.message.externalizable.aCE;
import game.script.template.BaseTaikodomContent;
import logic.baa.C1616Xf;
import logic.baa.C1634Xv;
import logic.baa.C5511aMd;
import logic.baa.C6485anp;
import logic.data.mbean.C3905wZ;
import logic.res.code.C5663aRz;
import logic.res.code.C6494any;
import logic.res.html.C2491fm;
import p001a.*;
import taikodom.infra.script.I18NString;

import java.util.Collection;

@C5511aMd
@C6485anp
/* renamed from: a.apx  reason: case insensitive filesystem */
/* compiled from: a */
public abstract class BaseAvatarAppearanceType extends BaseTaikodomContent implements C1616Xf {
    public static final C2491fm _f_toString_0020_0028_0029Ljava_002flang_002fString_003b = null;
    public static final int _m_fieldCount = 0;
    public static final C5663aRz[] _m_fields = null;
    public static final int _m_methodCount = 0;
    public static final C2491fm[] _m_methods = null;
    /* renamed from: eZ */
    public static final C2491fm f5139eZ = null;
    public static final long serialVersionUID = 0;
    public static C6494any ___iScriptClass;

    static {
        m25029V();
    }

    public BaseAvatarAppearanceType() {
        super((C5540aNg) null);
        super.mo10S();
    }

    public BaseAvatarAppearanceType(C5540aNg ang) {
        super(ang);
    }

    /* renamed from: V */
    static void m25029V() {
        serialVersionUID = (long) 1;
        _m_fieldCount = BaseTaikodomContent._m_fieldCount + 0;
        _m_methodCount = BaseTaikodomContent._m_methodCount + 2;
        _m_fields = new C5663aRz[(BaseTaikodomContent._m_fieldCount + 0)];
        C1634Xv.m11725a((Object[]) BaseTaikodomContent._m_fields, (Object[]) _m_fields);
        int i = BaseTaikodomContent._m_methodCount;
        C2491fm[] fmVarArr = new C2491fm[(i + 2)];
        C2491fm a = C4105zY.m41624a(BaseAvatarAppearanceType.class, "08f80e3cf9546ffbd545e5cacc1d4a84", i);
        _f_toString_0020_0028_0029Ljava_002flang_002fString_003b = a;
        fmVarArr[i] = a;
        int i2 = i + 1;
        C2491fm a2 = C4105zY.m41624a(BaseAvatarAppearanceType.class, "a1c932f72b967a8dbd4168a9ae2a01c4", i2);
        f5139eZ = a2;
        fmVarArr[i2] = a2;
        int i3 = i2 + 1;
        _m_methods = fmVarArr;
        C1634Xv.m11725a((Object[]) BaseTaikodomContent._m_methods, (Object[]) _m_methods);
        ___iScriptClass = aUO.m18566a(BaseAvatarAppearanceType.class, C3905wZ.class, _m_fields, _m_methods);
    }

    @C0064Am(aul = "a1c932f72b967a8dbd4168a9ae2a01c4", aum = 0)
    /* renamed from: bx */
    private I18NString m25031bx() {
        throw new aWi(new aCE(this, f5139eZ, new Object[0]));
    }

    /* renamed from: T */
    public C6494any mo11T() {
        return ___iScriptClass;
    }

    /* renamed from: U */
    public C2491fm[] mo12U() {
        return _m_methods;
    }

    /* renamed from: W */
    public Object mo13W() {
        return new C3905wZ(this);
    }

    /* renamed from: a */
    public Object mo14a(C0495Gr gr) {
        gr.getArgs();
        switch (gr.mo2417hq() - BaseTaikodomContent._m_methodCount) {
            case 0:
                return m25030au();
            case 1:
                return m25031bx();
            default:
                return super.mo14a(gr);
        }
    }

    /* renamed from: a */
    public void mo15a(Collection collection, C0495Gr gr) {
        super.mo15a(collection, gr);
    }

    /* renamed from: by */
    public I18NString mo5543by() {
        switch (bFf().mo6893i(f5139eZ)) {
            case 0:
                return null;
            case 2:
                return (I18NString) bFf().mo5606d(new aCE(this, f5139eZ, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f5139eZ, new Object[0]));
                break;
        }
        return m25031bx();
    }

    /* renamed from: c */
    public C5663aRz[] mo25c() {
        return _m_fields;
    }

    public String toString() {
        switch (bFf().mo6893i(_f_toString_0020_0028_0029Ljava_002flang_002fString_003b)) {
            case 0:
                return null;
            case 2:
                return (String) bFf().mo5606d(new aCE(this, _f_toString_0020_0028_0029Ljava_002flang_002fString_003b, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, _f_toString_0020_0028_0029Ljava_002flang_002fString_003b, new Object[0]));
                break;
        }
        return m25030au();
    }

    /* renamed from: S */
    public void mo10S() {
        super.mo10S();
    }

    @C0064Am(aul = "08f80e3cf9546ffbd545e5cacc1d4a84", aum = 0)
    /* renamed from: au */
    private String m25030au() {
        return String.valueOf(super.toString()) + " " + mo5543by().get();
    }
}
