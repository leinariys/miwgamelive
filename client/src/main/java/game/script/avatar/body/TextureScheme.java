package game.script.avatar.body;

import game.network.message.externalizable.aCE;
import game.script.avatar.AvatarProperty;
import game.script.avatar.ColorWrapper;
import game.script.resource.Asset;
import logic.baa.*;
import logic.data.mbean.C0692Js;
import logic.res.code.C5640aRc;
import logic.res.code.C5663aRz;
import logic.res.code.C6494any;
import logic.res.html.C2491fm;
import p001a.*;

import java.awt.*;
import java.util.Collection;
import java.util.UUID;

@C5511aMd
@C6485anp
/* renamed from: a.ark  reason: case insensitive filesystem */
/* compiled from: a */
public class TextureScheme extends AvatarProperty implements C0468GU, C1616Xf {

    /* renamed from: Lm */
    public static final C2491fm f5252Lm = null;
    public static final int _m_fieldCount = 0;
    public static final C5663aRz[] _m_fields = null;
    public static final int _m_methodCount = 0;
    public static final C2491fm[] _m_methods = null;
    /* renamed from: bL */
    public static final C5663aRz f5254bL = null;
    /* renamed from: bN */
    public static final C2491fm f5255bN = null;
    /* renamed from: bO */
    public static final C2491fm f5256bO = null;
    /* renamed from: bP */
    public static final C2491fm f5257bP = null;
    /* renamed from: bQ */
    public static final C2491fm f5258bQ = null;
    public static final C5663aRz eqe = null;
    public static final C2491fm eqm = null;
    public static final C2491fm eqo = null;
    public static final C2491fm eqp = null;
    public static final C5663aRz grA = null;
    public static final C5663aRz grB = null;
    public static final C2491fm grC = null;
    public static final C2491fm grD = null;
    public static final C2491fm grE = null;
    public static final C2491fm grF = null;
    public static final long serialVersionUID = 0;
    /* renamed from: zS */
    public static final C5663aRz f5260zS = null;
    public static C6494any ___iScriptClass;
    @C0064Am(aul = "fa018aa533dace6962db65aff6422619", aum = 4)

    /* renamed from: bK */
    private static UUID f5253bK;
    @C0064Am(aul = "5748d72707de21234359a6c93ec01b1f", aum = 0)
    private static Asset djx;
    @C0064Am(aul = "c83abb15ddf1a9d75eaf9a5a76dc0936", aum = 1)
    private static Asset djy;
    @C0064Am(aul = "c02c4cc0a0082b5e7a6bfe7a2e0800c2", aum = 2)
    private static ColorWrapper djz;
    @C0064Am(aul = "0d1f7e3ca2d2d0a065268055aff3b740", aum = 3)

    /* renamed from: zR */
    private static String f5259zR;

    static {
        m25541V();
    }

    public TextureScheme() {
        super((C5540aNg) null);
        super.mo10S();
    }

    public TextureScheme(C5540aNg ang) {
        super(ang);
    }

    /* renamed from: V */
    static void m25541V() {
        serialVersionUID = (long) 1;
        _m_fieldCount = AvatarProperty._m_fieldCount + 5;
        _m_methodCount = AvatarProperty._m_methodCount + 12;
        int i = AvatarProperty._m_fieldCount;
        C5663aRz[] arzArr = new C5663aRz[(i + 5)];
        C5663aRz b = C5640aRc.m17844b(TextureScheme.class, "5748d72707de21234359a6c93ec01b1f", i);
        grA = b;
        arzArr[i] = b;
        int i2 = i + 1;
        C5663aRz b2 = C5640aRc.m17844b(TextureScheme.class, "c83abb15ddf1a9d75eaf9a5a76dc0936", i2);
        grB = b2;
        arzArr[i2] = b2;
        int i3 = i2 + 1;
        C5663aRz b3 = C5640aRc.m17844b(TextureScheme.class, "c02c4cc0a0082b5e7a6bfe7a2e0800c2", i3);
        eqe = b3;
        arzArr[i3] = b3;
        int i4 = i3 + 1;
        C5663aRz b4 = C5640aRc.m17844b(TextureScheme.class, "0d1f7e3ca2d2d0a065268055aff3b740", i4);
        f5260zS = b4;
        arzArr[i4] = b4;
        int i5 = i4 + 1;
        C5663aRz b5 = C5640aRc.m17844b(TextureScheme.class, "fa018aa533dace6962db65aff6422619", i5);
        f5254bL = b5;
        arzArr[i5] = b5;
        int i6 = i5 + 1;
        _m_fields = arzArr;
        C1634Xv.m11725a((Object[]) AvatarProperty._m_fields, (Object[]) _m_fields);
        int i7 = AvatarProperty._m_methodCount;
        C2491fm[] fmVarArr = new C2491fm[(i7 + 12)];
        C2491fm a = C4105zY.m41624a(TextureScheme.class, "404a1aa7864ba28c721d9c92f0f40534", i7);
        f5255bN = a;
        fmVarArr[i7] = a;
        int i8 = i7 + 1;
        C2491fm a2 = C4105zY.m41624a(TextureScheme.class, "e60740972210c741083ec0d3d0b1c423", i8);
        f5256bO = a2;
        fmVarArr[i8] = a2;
        int i9 = i8 + 1;
        C2491fm a3 = C4105zY.m41624a(TextureScheme.class, "8337bd61ec33a63b6a6a85471e2f7945", i9);
        grC = a3;
        fmVarArr[i9] = a3;
        int i10 = i9 + 1;
        C2491fm a4 = C4105zY.m41624a(TextureScheme.class, "848e232fa53faa6b0299bf948c54cef9", i10);
        grD = a4;
        fmVarArr[i10] = a4;
        int i11 = i10 + 1;
        C2491fm a5 = C4105zY.m41624a(TextureScheme.class, "12088a7192f68c65c25b3fdb2da4cb0c", i11);
        grE = a5;
        fmVarArr[i11] = a5;
        int i12 = i11 + 1;
        C2491fm a6 = C4105zY.m41624a(TextureScheme.class, "83254d53b8695203d105def64716b834", i12);
        grF = a6;
        fmVarArr[i12] = a6;
        int i13 = i12 + 1;
        C2491fm a7 = C4105zY.m41624a(TextureScheme.class, "30d25b922cc2798f55e7a03273784575", i13);
        eqo = a7;
        fmVarArr[i13] = a7;
        int i14 = i13 + 1;
        C2491fm a8 = C4105zY.m41624a(TextureScheme.class, "af36c3be4ba22760d46832cb730b64cb", i14);
        eqp = a8;
        fmVarArr[i14] = a8;
        int i15 = i14 + 1;
        C2491fm a9 = C4105zY.m41624a(TextureScheme.class, "600c7365e08ad20f90aef5e9c9cb8793", i15);
        eqm = a9;
        fmVarArr[i15] = a9;
        int i16 = i15 + 1;
        C2491fm a10 = C4105zY.m41624a(TextureScheme.class, "143f2c23418f868a973bbb0677d2431c", i16);
        f5257bP = a10;
        fmVarArr[i16] = a10;
        int i17 = i16 + 1;
        C2491fm a11 = C4105zY.m41624a(TextureScheme.class, "ae21a19143617089510f1aa855440ecc", i17);
        f5258bQ = a11;
        fmVarArr[i17] = a11;
        int i18 = i17 + 1;
        C2491fm a12 = C4105zY.m41624a(TextureScheme.class, "d88e61c3a286466fc9dd02de88a5ad83", i18);
        f5252Lm = a12;
        fmVarArr[i18] = a12;
        int i19 = i18 + 1;
        _m_methods = fmVarArr;
        C1634Xv.m11725a((Object[]) AvatarProperty._m_methods, (Object[]) _m_methods);
        ___iScriptClass = aUO.m18566a(TextureScheme.class, C0692Js.class, _m_fields, _m_methods);
    }

    /* renamed from: S */
    private void m25540S(String str) {
        bFf().mo5608dq().mo3197f(f5260zS, str);
    }

    /* renamed from: a */
    private void m25542a(UUID uuid) {
        bFf().mo5608dq().mo3197f(f5254bL, uuid);
    }

    /* renamed from: an */
    private UUID m25543an() {
        return (UUID) bFf().mo5608dq().mo3214p(f5254bL);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, aYS = "Consistency", displayName = "Handle (Consistency checker)")
    @C0064Am(aul = "ae21a19143617089510f1aa855440ecc", aum = 0)
    @C5566aOg
    /* renamed from: b */
    private void m25546b(String str) {
        throw new aWi(new aCE(this, f5258bQ, new Object[]{str}));
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Texture (Head)")
    @C0064Am(aul = "848e232fa53faa6b0299bf948c54cef9", aum = 0)
    @C5566aOg
    /* renamed from: bA */
    private void m25548bA(Asset tCVar) {
        throw new aWi(new aCE(this, grD, new Object[]{tCVar}));
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Texture (Body)")
    @C0064Am(aul = "83254d53b8695203d105def64716b834", aum = 0)
    @C5566aOg
    /* renamed from: bC */
    private void m25549bC(Asset tCVar) {
        throw new aWi(new aCE(this, grF, new Object[]{tCVar}));
    }

    /* renamed from: by */
    private void m25550by(Asset tCVar) {
        bFf().mo5608dq().mo3197f(grA, tCVar);
    }

    private ColorWrapper byf() {
        return (ColorWrapper) bFf().mo5608dq().mo3214p(eqe);
    }

    /* renamed from: bz */
    private void m25551bz(Asset tCVar) {
        bFf().mo5608dq().mo3197f(grB, tCVar);
    }

    /* renamed from: c */
    private void m25552c(UUID uuid) {
        switch (bFf().mo6893i(f5256bO)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f5256bO, new Object[]{uuid}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f5256bO, new Object[]{uuid}));
                break;
        }
        m25547b(uuid);
    }

    private Asset crS() {
        return (Asset) bFf().mo5608dq().mo3214p(grA);
    }

    private Asset crT() {
        return (Asset) bFf().mo5608dq().mo3214p(grB);
    }

    /* renamed from: e */
    private void m25553e(ColorWrapper apf) {
        bFf().mo5608dq().mo3197f(eqe, apf);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Color (GUI feedback)")
    @C0064Am(aul = "af36c3be4ba22760d46832cb730b64cb", aum = 0)
    @C5566aOg
    /* renamed from: f */
    private void m25554f(ColorWrapper apf) {
        throw new aWi(new aCE(this, eqp, new Object[]{apf}));
    }

    /* renamed from: kc */
    private String m25555kc() {
        return (String) bFf().mo5608dq().mo3214p(f5260zS);
    }

    /* renamed from: T */
    public C6494any mo11T() {
        return ___iScriptClass;
    }

    /* renamed from: U */
    public C2491fm[] mo12U() {
        return _m_methods;
    }

    /* renamed from: W */
    public Object mo13W() {
        return new C0692Js(this);
    }

    /* renamed from: a */
    public Object mo14a(C0495Gr gr) {
        Object[] args = gr.getArgs();
        switch (gr.mo2417hq() - AvatarProperty._m_methodCount) {
            case 0:
                return m25544ap();
            case 1:
                m25547b((UUID) args[0]);
                return null;
            case 2:
                return crU();
            case 3:
                m25548bA((Asset) args[0]);
                return null;
            case 4:
                return crW();
            case 5:
                m25549bC((Asset) args[0]);
                return null;
            case 6:
                return byq();
            case 7:
                m25554f((ColorWrapper) args[0]);
                return null;
            case 8:
                return bym();
            case 9:
                return m25545ar();
            case 10:
                m25546b((String) args[0]);
                return null;
            case 11:
                m25556qU();
                return null;
            default:
                return super.mo14a(gr);
        }
    }

    /* renamed from: a */
    public void mo15a(Collection collection, C0495Gr gr) {
        super.mo15a(collection, gr);
    }

    /* renamed from: aq */
    public UUID mo18aq() {
        switch (bFf().mo6893i(f5255bN)) {
            case 0:
                return null;
            case 2:
                return (UUID) bFf().mo5606d(new aCE(this, f5255bN, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f5255bN, new Object[0]));
                break;
        }
        return m25544ap();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Texture (Head)")
    @C5566aOg
    /* renamed from: bB */
    public void mo15849bB(Asset tCVar) {
        switch (bFf().mo6893i(grD)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, grD, new Object[]{tCVar}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, grD, new Object[]{tCVar}));
                break;
        }
        m25548bA(tCVar);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Texture (Body)")
    @C5566aOg
    /* renamed from: bD */
    public void mo15850bD(Asset tCVar) {
        switch (bFf().mo6893i(grF)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, grF, new Object[]{tCVar}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, grF, new Object[]{tCVar}));
                break;
        }
        m25549bC(tCVar);
    }

    @ClientOnly
    public Color byn() {
        switch (bFf().mo6893i(eqm)) {
            case 0:
                return null;
            case 2:
                return (Color) bFf().mo5606d(new aCE(this, eqm, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, eqm, new Object[0]));
                break;
        }
        return bym();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Color (GUI feedback)")
    public ColorWrapper byr() {
        switch (bFf().mo6893i(eqo)) {
            case 0:
                return null;
            case 2:
                return (ColorWrapper) bFf().mo5606d(new aCE(this, eqo, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, eqo, new Object[0]));
                break;
        }
        return byq();
    }

    /* renamed from: c */
    public C5663aRz[] mo25c() {
        return _m_fields;
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Texture (Head)")
    public Asset crV() {
        switch (bFf().mo6893i(grC)) {
            case 0:
                return null;
            case 2:
                return (Asset) bFf().mo5606d(new aCE(this, grC, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, grC, new Object[0]));
                break;
        }
        return crU();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Texture (Body)")
    public Asset crX() {
        switch (bFf().mo6893i(grE)) {
            case 0:
                return null;
            case 2:
                return (Asset) bFf().mo5606d(new aCE(this, grE, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, grE, new Object[0]));
                break;
        }
        return crW();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Color (GUI feedback)")
    @C5566aOg
    /* renamed from: g */
    public void mo15855g(ColorWrapper apf) {
        switch (bFf().mo6893i(eqp)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, eqp, new Object[]{apf}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, eqp, new Object[]{apf}));
                break;
        }
        m25554f(apf);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, aYS = "Consistency", displayName = "Handle (Consistency checker)")
    public String getHandle() {
        switch (bFf().mo6893i(f5257bP)) {
            case 0:
                return null;
            case 2:
                return (String) bFf().mo5606d(new aCE(this, f5257bP, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f5257bP, new Object[0]));
                break;
        }
        return m25545ar();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, aYS = "Consistency", displayName = "Handle (Consistency checker)")
    @C5566aOg
    public void setHandle(String str) {
        switch (bFf().mo6893i(f5258bQ)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f5258bQ, new Object[]{str}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f5258bQ, new Object[]{str}));
                break;
        }
        m25546b(str);
    }

    /* renamed from: qV */
    public void mo15856qV() {
        switch (bFf().mo6893i(f5252Lm)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f5252Lm, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f5252Lm, new Object[0]));
                break;
        }
        m25556qU();
    }

    @C0064Am(aul = "404a1aa7864ba28c721d9c92f0f40534", aum = 0)
    /* renamed from: ap */
    private UUID m25544ap() {
        return m25543an();
    }

    @C0064Am(aul = "e60740972210c741083ec0d3d0b1c423", aum = 0)
    /* renamed from: b */
    private void m25547b(UUID uuid) {
        m25542a(uuid);
    }

    /* renamed from: S */
    public void mo10S() {
        super.mo10S();
        m25542a(UUID.randomUUID());
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Texture (Head)")
    @C0064Am(aul = "8337bd61ec33a63b6a6a85471e2f7945", aum = 0)
    private Asset crU() {
        return crS();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Texture (Body)")
    @C0064Am(aul = "12088a7192f68c65c25b3fdb2da4cb0c", aum = 0)
    private Asset crW() {
        return crT();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Color (GUI feedback)")
    @C0064Am(aul = "30d25b922cc2798f55e7a03273784575", aum = 0)
    private ColorWrapper byq() {
        return byf();
    }

    @C0064Am(aul = "600c7365e08ad20f90aef5e9c9cb8793", aum = 0)
    @ClientOnly
    private Color bym() {
        return byf() == null ? Color.WHITE : byf().getColor();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, aYS = "Consistency", displayName = "Handle (Consistency checker)")
    @C0064Am(aul = "143f2c23418f868a973bbb0677d2431c", aum = 0)
    /* renamed from: ar */
    private String m25545ar() {
        return m25555kc();
    }

    @C0064Am(aul = "d88e61c3a286466fc9dd02de88a5ad83", aum = 0)
    /* renamed from: qU */
    private void m25556qU() {
        push();
        if (crS() != null) {
            crS().push();
        }
        if (crT() != null) {
            crT().push();
        }
        if (byf() != null) {
            byf().push();
        }
    }
}
