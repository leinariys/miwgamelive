package game.script.avatar.body;

import game.network.message.externalizable.aCE;
import game.network.message.serializable.C3438ra;
import game.script.avatar.AvatarProperty;
import game.script.avatar.AvatarSector;
import game.script.avatar.TextureGrid;
import logic.baa.*;
import logic.data.mbean.C1651YJ;
import logic.res.code.C5640aRc;
import logic.res.code.C5663aRz;
import logic.res.code.C6494any;
import logic.res.html.C2491fm;
import p001a.*;

import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.UUID;

@C5511aMd
@C6485anp
/* renamed from: a.Qb */
/* compiled from: a */
public class BodyPiece extends AvatarProperty implements C0468GU, C1616Xf {
    public static final int _m_fieldCount = 0;
    public static final C5663aRz[] _m_fields = null;
    public static final int _m_methodCount = 0;
    public static final C2491fm[] _m_methods = null;
    /* renamed from: bL */
    public static final C5663aRz f1443bL = null;
    /* renamed from: bM */
    public static final C5663aRz f1444bM = null;
    /* renamed from: bN */
    public static final C2491fm f1445bN = null;
    /* renamed from: bO */
    public static final C2491fm f1446bO = null;
    /* renamed from: bP */
    public static final C2491fm f1447bP = null;
    /* renamed from: bQ */
    public static final C2491fm f1448bQ = null;
    public static final C5663aRz dTW = null;
    public static final C5663aRz dTY = null;
    public static final C2491fm dTZ = null;
    public static final C2491fm dUa = null;
    public static final C2491fm dUb = null;
    public static final C2491fm dUc = null;
    public static final C2491fm dUd = null;
    public static final C2491fm dUe = null;
    public static final C2491fm dUf = null;
    public static final C2491fm dUg = null;
    public static final C2491fm dUh = null;
    public static final C2491fm dUi = null;
    public static final long serialVersionUID = 0;
    /* renamed from: zS */
    public static final C5663aRz f1450zS = null;
    public static C6494any ___iScriptClass;
    @C0064Am(aul = "6a381dd68a2050f85a0037165de453b9", aum = 4)

    /* renamed from: bK */
    private static UUID f1442bK;
    @C0064Am(aul = "6801c57f2db6da4fd1745dbbcfd44347", aum = 2)
    private static AvatarSector dTV;
    @C0064Am(aul = "b6aa394c386d92b00aea50ed103e3a7a", aum = 3)
    private static C3438ra<TextureGrid> dTX;
    @C0064Am(aul = "0c7e6883f284bf9ab00e88d1b5449081", aum = 1)
    private static int handle;
    @C0064Am(aul = "76b406b7c04e4d8d0d1e2c8960abac97", aum = 0)

    /* renamed from: zR */
    private static String f1449zR;

    static {
        m8933V();
    }

    public BodyPiece() {
        super((C5540aNg) null);
        super.mo10S();
    }

    public BodyPiece(C5540aNg ang) {
        super(ang);
    }

    /* renamed from: V */
    static void m8933V() {
        serialVersionUID = (long) 1;
        _m_fieldCount = AvatarProperty._m_fieldCount + 5;
        _m_methodCount = AvatarProperty._m_methodCount + 14;
        int i = AvatarProperty._m_fieldCount;
        C5663aRz[] arzArr = new C5663aRz[(i + 5)];
        C5663aRz b = C5640aRc.m17844b(BodyPiece.class, "76b406b7c04e4d8d0d1e2c8960abac97", i);
        f1450zS = b;
        arzArr[i] = b;
        int i2 = i + 1;
        C5663aRz b2 = C5640aRc.m17844b(BodyPiece.class, "0c7e6883f284bf9ab00e88d1b5449081", i2);
        f1444bM = b2;
        arzArr[i2] = b2;
        int i3 = i2 + 1;
        C5663aRz b3 = C5640aRc.m17844b(BodyPiece.class, "6801c57f2db6da4fd1745dbbcfd44347", i3);
        dTW = b3;
        arzArr[i3] = b3;
        int i4 = i3 + 1;
        C5663aRz b4 = C5640aRc.m17844b(BodyPiece.class, "b6aa394c386d92b00aea50ed103e3a7a", i4);
        dTY = b4;
        arzArr[i4] = b4;
        int i5 = i4 + 1;
        C5663aRz b5 = C5640aRc.m17844b(BodyPiece.class, "6a381dd68a2050f85a0037165de453b9", i5);
        f1443bL = b5;
        arzArr[i5] = b5;
        int i6 = i5 + 1;
        _m_fields = arzArr;
        C1634Xv.m11725a((Object[]) AvatarProperty._m_fields, (Object[]) _m_fields);
        int i7 = AvatarProperty._m_methodCount;
        C2491fm[] fmVarArr = new C2491fm[(i7 + 14)];
        C2491fm a = C4105zY.m41624a(BodyPiece.class, "76d13eb7675a46a85ba94588e7eebd4e", i7);
        f1445bN = a;
        fmVarArr[i7] = a;
        int i8 = i7 + 1;
        C2491fm a2 = C4105zY.m41624a(BodyPiece.class, "44a334ea43fb021c26ee6c86c5252b63", i8);
        f1446bO = a2;
        fmVarArr[i8] = a2;
        int i9 = i8 + 1;
        C2491fm a3 = C4105zY.m41624a(BodyPiece.class, "d3842ee79a7e67b506e8a07b56134b0a", i9);
        dTZ = a3;
        fmVarArr[i9] = a3;
        int i10 = i9 + 1;
        C2491fm a4 = C4105zY.m41624a(BodyPiece.class, "ad046470eb97d05da11fe77f5067ca37", i10);
        dUa = a4;
        fmVarArr[i10] = a4;
        int i11 = i10 + 1;
        C2491fm a5 = C4105zY.m41624a(BodyPiece.class, "e51d576f1171b5d6fc4435124f3330c4", i11);
        dUb = a5;
        fmVarArr[i11] = a5;
        int i12 = i11 + 1;
        C2491fm a6 = C4105zY.m41624a(BodyPiece.class, "4097890bad43a476a6aa4c5eb425c678", i12);
        dUc = a6;
        fmVarArr[i12] = a6;
        int i13 = i12 + 1;
        C2491fm a7 = C4105zY.m41624a(BodyPiece.class, "0c680f0f16854fec9bc30512d09696fb", i13);
        dUd = a7;
        fmVarArr[i13] = a7;
        int i14 = i13 + 1;
        C2491fm a8 = C4105zY.m41624a(BodyPiece.class, "fb8df8d0901934a8193c40a9659f6f03", i14);
        dUe = a8;
        fmVarArr[i14] = a8;
        int i15 = i14 + 1;
        C2491fm a9 = C4105zY.m41624a(BodyPiece.class, "f5ab929a34041c6f26bb0dcc76aab0c6", i15);
        dUf = a9;
        fmVarArr[i15] = a9;
        int i16 = i15 + 1;
        C2491fm a10 = C4105zY.m41624a(BodyPiece.class, "ba4960dabee4a9875d2b06498bc600a3", i16);
        dUg = a10;
        fmVarArr[i16] = a10;
        int i17 = i16 + 1;
        C2491fm a11 = C4105zY.m41624a(BodyPiece.class, "2d819d40cd529b1690658d70d55b6b7e", i17);
        dUh = a11;
        fmVarArr[i17] = a11;
        int i18 = i17 + 1;
        C2491fm a12 = C4105zY.m41624a(BodyPiece.class, "16bdbc2e5802e9dd86e84acb08d0e9a3", i18);
        dUi = a12;
        fmVarArr[i18] = a12;
        int i19 = i18 + 1;
        C2491fm a13 = C4105zY.m41624a(BodyPiece.class, "fd78b6d62f2b212b16473499ed8ad2fe", i19);
        f1448bQ = a13;
        fmVarArr[i19] = a13;
        int i20 = i19 + 1;
        C2491fm a14 = C4105zY.m41624a(BodyPiece.class, "f7fc1001b5140fe87478ef223618f48d", i20);
        f1447bP = a14;
        fmVarArr[i20] = a14;
        int i21 = i20 + 1;
        _m_methods = fmVarArr;
        C1634Xv.m11725a((Object[]) AvatarProperty._m_methods, (Object[]) _m_methods);
        ___iScriptClass = aUO.m18566a(BodyPiece.class, C1651YJ.class, _m_fields, _m_methods);
    }

    /* renamed from: S */
    private void m8932S(String str) {
        bFf().mo5608dq().mo3197f(f1450zS, str);
    }

    /* renamed from: a */
    private void m8934a(UUID uuid) {
        bFf().mo5608dq().mo3197f(f1443bL, uuid);
    }

    /* renamed from: an */
    private UUID m8935an() {
        return (UUID) bFf().mo5608dq().mo3214p(f1443bL);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Handle (Consistency checker)")
    @C0064Am(aul = "fd78b6d62f2b212b16473499ed8ad2fe", aum = 0)
    @C5566aOg
    /* renamed from: b */
    private void m8938b(String str) {
        throw new aWi(new aCE(this, f1448bQ, new Object[]{str}));
    }

    /* renamed from: bn */
    private void m8940bn(C3438ra raVar) {
        bFf().mo5608dq().mo3197f(dTY, raVar);
    }

    private int bpe() {
        return bFf().mo5608dq().mo3212n(f1444bM);
    }

    private AvatarSector bpf() {
        return (AvatarSector) bFf().mo5608dq().mo3214p(dTW);
    }

    private C3438ra bpg() {
        return (C3438ra) bFf().mo5608dq().mo3214p(dTY);
    }

    /* renamed from: c */
    private void m8941c(UUID uuid) {
        switch (bFf().mo6893i(f1446bO)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f1446bO, new Object[]{uuid}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f1446bO, new Object[]{uuid}));
                break;
        }
        m8939b(uuid);
    }

    /* renamed from: kc */
    private String m8944kc() {
        return (String) bFf().mo5608dq().mo3214p(f1450zS);
    }

    /* renamed from: mO */
    private void m8945mO(int i) {
        bFf().mo5608dq().mo3183b(f1444bM, i);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Internal Mesh Handle (Index)")
    @C0064Am(aul = "ad046470eb97d05da11fe77f5067ca37", aum = 0)
    @C5566aOg
    /* renamed from: mP */
    private void m8946mP(int i) {
        throw new aWi(new aCE(this, dUa, new Object[]{new Integer(i)}));
    }

    /* renamed from: q */
    private void m8947q(AvatarSector nw) {
        bFf().mo5608dq().mo3197f(dTW, nw);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Avatar Sector Descriptor")
    @C0064Am(aul = "4097890bad43a476a6aa4c5eb425c678", aum = 0)
    @C5566aOg
    /* renamed from: r */
    private void m8948r(AvatarSector nw) {
        throw new aWi(new aCE(this, dUc, new Object[]{nw}));
    }

    /* renamed from: T */
    public C6494any mo11T() {
        return ___iScriptClass;
    }

    /* renamed from: U */
    public C2491fm[] mo12U() {
        return _m_methods;
    }

    /* renamed from: W */
    public Object mo13W() {
        return new C1651YJ(this);
    }

    /* renamed from: a */
    public Object mo14a(C0495Gr gr) {
        Object[] args = gr.getArgs();
        switch (gr.mo2417hq() - AvatarProperty._m_methodCount) {
            case 0:
                return m8936ap();
            case 1:
                m8939b((UUID) args[0]);
                return null;
            case 2:
                return new Integer(bph());
            case 3:
                m8946mP(((Integer) args[0]).intValue());
                return null;
            case 4:
                return bpj();
            case 5:
                m8948r((AvatarSector) args[0]);
                return null;
            case 6:
                m8942e((TextureGrid) args[0]);
                return null;
            case 7:
                m8943g((TextureGrid) args[0]);
                return null;
            case 8:
                return bpl();
            case 9:
                return bpn();
            case 10:
                return new Integer(bpp());
            case 11:
                return new Boolean(bpr());
            case 12:
                m8938b((String) args[0]);
                return null;
            case 13:
                return m8937ar();
            default:
                return super.mo14a(gr);
        }
    }

    /* renamed from: a */
    public void mo15a(Collection collection, C0495Gr gr) {
        super.mo15a(collection, gr);
    }

    /* renamed from: aq */
    public UUID mo18aq() {
        switch (bFf().mo6893i(f1445bN)) {
            case 0:
                return null;
            case 2:
                return (UUID) bFf().mo5606d(new aCE(this, f1445bN, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f1445bN, new Object[0]));
                break;
        }
        return m8936ap();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Internal Mesh Handle (Index)")
    public int bpi() {
        switch (bFf().mo6893i(dTZ)) {
            case 0:
                return 0;
            case 2:
                return ((Integer) bFf().mo5606d(new aCE(this, dTZ, new Object[0]))).intValue();
            case 3:
                bFf().mo5606d(new aCE(this, dTZ, new Object[0]));
                break;
        }
        return bph();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Avatar Sector Descriptor")
    public AvatarSector bpk() {
        switch (bFf().mo6893i(dUb)) {
            case 0:
                return null;
            case 2:
                return (AvatarSector) bFf().mo5606d(new aCE(this, dUb, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, dUb, new Object[0]));
                break;
        }
        return bpj();
    }

    @C3248pc(aYR = C3248pc.C3250b.ITERATOR, displayName = "Color list")
    public List<TextureGrid> bpm() {
        switch (bFf().mo6893i(dUf)) {
            case 0:
                return null;
            case 2:
                return (List) bFf().mo5606d(new aCE(this, dUf, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, dUf, new Object[0]));
                break;
        }
        return bpl();
    }

    @ClientOnly
    public String bpo() {
        switch (bFf().mo6893i(dUg)) {
            case 0:
                return null;
            case 2:
                return (String) bFf().mo5606d(new aCE(this, dUg, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, dUg, new Object[0]));
                break;
        }
        return bpn();
    }

    @ClientOnly
    public int bpq() {
        switch (bFf().mo6893i(dUh)) {
            case 0:
                return 0;
            case 2:
                return ((Integer) bFf().mo5606d(new aCE(this, dUh, new Object[0]))).intValue();
            case 3:
                bFf().mo5606d(new aCE(this, dUh, new Object[0]));
                break;
        }
        return bpp();
    }

    public boolean bps() {
        switch (bFf().mo6893i(dUi)) {
            case 0:
                return false;
            case 2:
                return ((Boolean) bFf().mo5606d(new aCE(this, dUi, new Object[0]))).booleanValue();
            case 3:
                bFf().mo5606d(new aCE(this, dUi, new Object[0]));
                break;
        }
        return bpr();
    }

    /* renamed from: c */
    public C5663aRz[] mo25c() {
        return _m_fields;
    }

    @C3248pc(aYR = C3248pc.C3250b.ADDER, displayName = "Color list")
    /* renamed from: f */
    public void mo5065f(TextureGrid uk) {
        switch (bFf().mo6893i(dUd)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, dUd, new Object[]{uk}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, dUd, new Object[]{uk}));
                break;
        }
        m8942e(uk);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Handle (Consistency checker)")
    public String getHandle() {
        switch (bFf().mo6893i(f1447bP)) {
            case 0:
                return null;
            case 2:
                return (String) bFf().mo5606d(new aCE(this, f1447bP, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f1447bP, new Object[0]));
                break;
        }
        return m8937ar();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Handle (Consistency checker)")
    @C5566aOg
    public void setHandle(String str) {
        switch (bFf().mo6893i(f1448bQ)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f1448bQ, new Object[]{str}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f1448bQ, new Object[]{str}));
                break;
        }
        m8938b(str);
    }

    @C3248pc(aYR = C3248pc.C3250b.REMOVER, displayName = "Color list")
    /* renamed from: h */
    public void mo5066h(TextureGrid uk) {
        switch (bFf().mo6893i(dUe)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, dUe, new Object[]{uk}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, dUe, new Object[]{uk}));
                break;
        }
        m8943g(uk);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Internal Mesh Handle (Index)")
    @C5566aOg
    /* renamed from: mQ */
    public void mo5067mQ(int i) {
        switch (bFf().mo6893i(dUa)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, dUa, new Object[]{new Integer(i)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, dUa, new Object[]{new Integer(i)}));
                break;
        }
        m8946mP(i);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Avatar Sector Descriptor")
    @C5566aOg
    /* renamed from: s */
    public void mo5068s(AvatarSector nw) {
        switch (bFf().mo6893i(dUc)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, dUc, new Object[]{nw}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, dUc, new Object[]{nw}));
                break;
        }
        m8948r(nw);
    }

    @C0064Am(aul = "76d13eb7675a46a85ba94588e7eebd4e", aum = 0)
    /* renamed from: ap */
    private UUID m8936ap() {
        return m8935an();
    }

    @C0064Am(aul = "44a334ea43fb021c26ee6c86c5252b63", aum = 0)
    /* renamed from: b */
    private void m8939b(UUID uuid) {
        m8934a(uuid);
    }

    /* renamed from: S */
    public void mo10S() {
        super.mo10S();
        m8934a(UUID.randomUUID());
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Internal Mesh Handle (Index)")
    @C0064Am(aul = "d3842ee79a7e67b506e8a07b56134b0a", aum = 0)
    private int bph() {
        return bpe();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Avatar Sector Descriptor")
    @C0064Am(aul = "e51d576f1171b5d6fc4435124f3330c4", aum = 0)
    private AvatarSector bpj() {
        return bpf();
    }

    @C3248pc(aYR = C3248pc.C3250b.ADDER, displayName = "Color list")
    @C0064Am(aul = "0c680f0f16854fec9bc30512d09696fb", aum = 0)
    /* renamed from: e */
    private void m8942e(TextureGrid uk) {
        bpg().add(uk);
    }

    @C3248pc(aYR = C3248pc.C3250b.REMOVER, displayName = "Color list")
    @C0064Am(aul = "fb8df8d0901934a8193c40a9659f6f03", aum = 0)
    /* renamed from: g */
    private void m8943g(TextureGrid uk) {
        bpg().remove(uk);
    }

    @C3248pc(aYR = C3248pc.C3250b.ITERATOR, displayName = "Color list")
    @C0064Am(aul = "f5ab929a34041c6f26bb0dcc76aab0c6", aum = 0)
    private List<TextureGrid> bpl() {
        return Collections.unmodifiableList(bpg());
    }

    @C0064Am(aul = "ba4960dabee4a9875d2b06498bc600a3", aum = 0)
    @ClientOnly
    private String bpn() {
        return String.valueOf(bpf().getPrefix()) + (bpe() < 10 ? "0" + bpe() : Integer.valueOf(bpe()));
    }

    @C0064Am(aul = "2d819d40cd529b1690658d70d55b6b7e", aum = 0)
    @ClientOnly
    private int bpp() {
        return bpf().biV() + bpe();
    }

    @C0064Am(aul = "16bdbc2e5802e9dd86e84acb08d0e9a3", aum = 0)
    private boolean bpr() {
        return bpe() >= 0;
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Handle (Consistency checker)")
    @C0064Am(aul = "f7fc1001b5140fe87478ef223618f48d", aum = 0)
    /* renamed from: ar */
    private String m8937ar() {
        return m8944kc();
    }
}
