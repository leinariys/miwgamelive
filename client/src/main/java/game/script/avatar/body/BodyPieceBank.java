package game.script.avatar.body;

import game.network.message.externalizable.aCE;
import game.network.message.serializable.C3438ra;
import game.script.avatar.AvatarSector;
import logic.baa.*;
import logic.data.mbean.C3964xR;
import logic.res.code.C5640aRc;
import logic.res.code.C5663aRz;
import logic.res.code.C6494any;
import logic.res.html.C2491fm;
import p001a.*;

import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.UUID;

@C5511aMd
@C6485anp
/* renamed from: a.aRp  reason: case insensitive filesystem */
/* compiled from: a */
public class BodyPieceBank extends aDJ implements C0468GU, C1616Xf {
    public static final C2491fm _f_toString_0020_0028_0029Ljava_002flang_002fString_003b = null;
    public static final int _m_fieldCount = 0;
    public static final C5663aRz[] _m_fields = null;
    public static final int _m_methodCount = 0;
    public static final C2491fm[] _m_methods = null;
    /* renamed from: bL */
    public static final C5663aRz f3719bL = null;
    /* renamed from: bM */
    public static final C5663aRz f3720bM = null;
    /* renamed from: bN */
    public static final C2491fm f3721bN = null;
    /* renamed from: bO */
    public static final C2491fm f3722bO = null;
    /* renamed from: bP */
    public static final C2491fm f3723bP = null;
    /* renamed from: bQ */
    public static final C2491fm f3724bQ = null;
    public static final C5663aRz iHe = null;
    public static final C5663aRz iHf = null;
    public static final C5663aRz iHg = null;
    public static final C2491fm iHh = null;
    public static final C2491fm iHi = null;
    public static final C2491fm iHj = null;
    public static final C2491fm iHk = null;
    public static final C2491fm iHl = null;
    public static final C2491fm iHm = null;
    public static final C2491fm iHn = null;
    public static final C2491fm iHo = null;
    public static final long serialVersionUID = 0;
    public static C6494any ___iScriptClass;
    @C0064Am(aul = "0fc7a5f86a8eca3f8ea1ab791e0f5be4", aum = 0)
    private static String bIf;
    @C0064Am(aul = "9c7a73cd3171cd461f3baa0b5f899497", aum = 1)
    private static AvatarSector bIg;
    @C0064Am(aul = "8d2cdc8e2c7ffdc13e0a0c128824bba3", aum = 2)
    private static C3438ra<BodyPiece> bIh;
    @C0064Am(aul = "4a89a98bbf4d973d42664aa7acf63f2c", aum = 3)

    /* renamed from: bK */
    private static UUID f3718bK;
    @C0064Am(aul = "bbd8bdaf75b704d8d5a69d2d7dbe0ef2", aum = 4)
    private static String handle;

    static {
        m17940V();
    }

    public BodyPieceBank() {
        super((C5540aNg) null);
        super.mo10S();
    }

    public BodyPieceBank(C5540aNg ang) {
        super(ang);
    }

    /* renamed from: V */
    static void m17940V() {
        serialVersionUID = (long) 1;
        _m_fieldCount = aDJ._m_fieldCount + 5;
        _m_methodCount = aDJ._m_methodCount + 13;
        int i = aDJ._m_fieldCount;
        C5663aRz[] arzArr = new C5663aRz[(i + 5)];
        C5663aRz b = C5640aRc.m17844b(BodyPieceBank.class, "0fc7a5f86a8eca3f8ea1ab791e0f5be4", i);
        iHe = b;
        arzArr[i] = b;
        int i2 = i + 1;
        C5663aRz b2 = C5640aRc.m17844b(BodyPieceBank.class, "9c7a73cd3171cd461f3baa0b5f899497", i2);
        iHf = b2;
        arzArr[i2] = b2;
        int i3 = i2 + 1;
        C5663aRz b3 = C5640aRc.m17844b(BodyPieceBank.class, "8d2cdc8e2c7ffdc13e0a0c128824bba3", i3);
        iHg = b3;
        arzArr[i3] = b3;
        int i4 = i3 + 1;
        C5663aRz b4 = C5640aRc.m17844b(BodyPieceBank.class, "4a89a98bbf4d973d42664aa7acf63f2c", i4);
        f3719bL = b4;
        arzArr[i4] = b4;
        int i5 = i4 + 1;
        C5663aRz b5 = C5640aRc.m17844b(BodyPieceBank.class, "bbd8bdaf75b704d8d5a69d2d7dbe0ef2", i5);
        f3720bM = b5;
        arzArr[i5] = b5;
        int i6 = i5 + 1;
        _m_fields = arzArr;
        C1634Xv.m11725a((Object[]) aDJ._m_fields, (Object[]) _m_fields);
        int i7 = aDJ._m_methodCount;
        C2491fm[] fmVarArr = new C2491fm[(i7 + 13)];
        C2491fm a = C4105zY.m41624a(BodyPieceBank.class, "730cd6c3cd4b9b190adfa480815444e4", i7);
        _f_toString_0020_0028_0029Ljava_002flang_002fString_003b = a;
        fmVarArr[i7] = a;
        int i8 = i7 + 1;
        C2491fm a2 = C4105zY.m41624a(BodyPieceBank.class, "8417822b6f1904d2d7cc58283f63d86c", i8);
        f3721bN = a2;
        fmVarArr[i8] = a2;
        int i9 = i8 + 1;
        C2491fm a3 = C4105zY.m41624a(BodyPieceBank.class, "b6f4480d89aac021d6160e7130f11b81", i9);
        f3722bO = a3;
        fmVarArr[i9] = a3;
        int i10 = i9 + 1;
        C2491fm a4 = C4105zY.m41624a(BodyPieceBank.class, "d9484af359db0215901c25dd633ad1ff", i10);
        f3723bP = a4;
        fmVarArr[i10] = a4;
        int i11 = i10 + 1;
        C2491fm a5 = C4105zY.m41624a(BodyPieceBank.class, "7a6076faf299f9b7f7417dbd507bae66", i11);
        f3724bQ = a5;
        fmVarArr[i11] = a5;
        int i12 = i11 + 1;
        C2491fm a6 = C4105zY.m41624a(BodyPieceBank.class, "c0ff37a66c238e4aedfb4fac94e547f9", i12);
        iHh = a6;
        fmVarArr[i12] = a6;
        int i13 = i12 + 1;
        C2491fm a7 = C4105zY.m41624a(BodyPieceBank.class, "50eb687d576f6bd002710c330524af5f", i13);
        iHi = a7;
        fmVarArr[i13] = a7;
        int i14 = i13 + 1;
        C2491fm a8 = C4105zY.m41624a(BodyPieceBank.class, "fd931363598e62d3add0f230b2b8a181", i14);
        iHj = a8;
        fmVarArr[i14] = a8;
        int i15 = i14 + 1;
        C2491fm a9 = C4105zY.m41624a(BodyPieceBank.class, "6187a3256b15f2b709cc9c2668175847", i15);
        iHk = a9;
        fmVarArr[i15] = a9;
        int i16 = i15 + 1;
        C2491fm a10 = C4105zY.m41624a(BodyPieceBank.class, "9481d8fb3d3916a47dec07a5f5868abf", i16);
        iHl = a10;
        fmVarArr[i16] = a10;
        int i17 = i16 + 1;
        C2491fm a11 = C4105zY.m41624a(BodyPieceBank.class, "25055cc106940fbe0e39b77cd5b756de", i17);
        iHm = a11;
        fmVarArr[i17] = a11;
        int i18 = i17 + 1;
        C2491fm a12 = C4105zY.m41624a(BodyPieceBank.class, "bbf2453ecbd2e5e846b116000eeb52be", i18);
        iHn = a12;
        fmVarArr[i18] = a12;
        int i19 = i18 + 1;
        C2491fm a13 = C4105zY.m41624a(BodyPieceBank.class, "fd1f308ba6d82b7948ee5e6aa4451346", i19);
        iHo = a13;
        fmVarArr[i19] = a13;
        int i20 = i19 + 1;
        _m_methods = fmVarArr;
        C1634Xv.m11725a((Object[]) aDJ._m_methods, (Object[]) _m_methods);
        ___iScriptClass = aUO.m18566a(BodyPieceBank.class, C3964xR.class, _m_fields, _m_methods);
    }

    /* renamed from: a */
    private void m17941a(String str) {
        bFf().mo5608dq().mo3197f(f3720bM, str);
    }

    /* renamed from: a */
    private void m17942a(UUID uuid) {
        bFf().mo5608dq().mo3197f(f3719bL, uuid);
    }

    /* renamed from: an */
    private UUID m17943an() {
        return (UUID) bFf().mo5608dq().mo3214p(f3719bL);
    }

    /* renamed from: ao */
    private String m17944ao() {
        return (String) bFf().mo5608dq().mo3214p(f3720bM);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Handle")
    @C0064Am(aul = "7a6076faf299f9b7f7417dbd507bae66", aum = 0)
    @C5566aOg
    /* renamed from: b */
    private void m17948b(String str) {
        throw new aWi(new aCE(this, f3724bQ, new Object[]{str}));
    }

    /* renamed from: c */
    private void m17950c(UUID uuid) {
        switch (bFf().mo6893i(f3722bO)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f3722bO, new Object[]{uuid}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f3722bO, new Object[]{uuid}));
                break;
        }
        m17949b(uuid);
    }

    /* renamed from: cA */
    private void m17951cA(C3438ra raVar) {
        bFf().mo5608dq().mo3197f(iHg, raVar);
    }

    private String drq() {
        return (String) bFf().mo5608dq().mo3214p(iHe);
    }

    private AvatarSector drr() {
        return (AvatarSector) bFf().mo5608dq().mo3214p(iHf);
    }

    private C3438ra drs() {
        return (C3438ra) bFf().mo5608dq().mo3214p(iHg);
    }

    /* renamed from: nt */
    private void m17952nt(String str) {
        bFf().mo5608dq().mo3197f(iHe, str);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Bank Name")
    @C0064Am(aul = "50eb687d576f6bd002710c330524af5f", aum = 0)
    @C5566aOg
    /* renamed from: nu */
    private void m17953nu(String str) {
        throw new aWi(new aCE(this, iHi, new Object[]{str}));
    }

    /* renamed from: y */
    private void m17954y(AvatarSector nw) {
        bFf().mo5608dq().mo3197f(iHf, nw);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Sector Filter")
    @C0064Am(aul = "6187a3256b15f2b709cc9c2668175847", aum = 0)
    @C5566aOg
    /* renamed from: z */
    private void m17955z(AvatarSector nw) {
        throw new aWi(new aCE(this, iHk, new Object[]{nw}));
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Sector Filter")
    @C5566aOg
    /* renamed from: A */
    public void mo11211A(AvatarSector nw) {
        switch (bFf().mo6893i(iHk)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, iHk, new Object[]{nw}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, iHk, new Object[]{nw}));
                break;
        }
        m17955z(nw);
    }

    @C3248pc(aYR = C3248pc.C3250b.ADDER, displayName = "Bodypiece Bank")
    /* renamed from: N */
    public void mo11212N(BodyPiece qb) {
        switch (bFf().mo6893i(iHl)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, iHl, new Object[]{qb}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, iHl, new Object[]{qb}));
                break;
        }
        m17937M(qb);
    }

    @C3248pc(aYR = C3248pc.C3250b.REMOVER, displayName = "Bodypiece Bank")
    /* renamed from: P */
    public void mo11213P(BodyPiece qb) {
        switch (bFf().mo6893i(iHm)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, iHm, new Object[]{qb}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, iHm, new Object[]{qb}));
                break;
        }
        m17938O(qb);
    }

    /* renamed from: R */
    public boolean mo11214R(BodyPiece qb) {
        switch (bFf().mo6893i(iHo)) {
            case 0:
                return false;
            case 2:
                return ((Boolean) bFf().mo5606d(new aCE(this, iHo, new Object[]{qb}))).booleanValue();
            case 3:
                bFf().mo5606d(new aCE(this, iHo, new Object[]{qb}));
                break;
        }
        return m17939Q(qb);
    }

    /* renamed from: T */
    public C6494any mo11T() {
        return ___iScriptClass;
    }

    /* renamed from: U */
    public C2491fm[] mo12U() {
        return _m_methods;
    }

    /* renamed from: W */
    public Object mo13W() {
        return new C3964xR(this);
    }

    /* renamed from: a */
    public Object mo14a(C0495Gr gr) {
        Object[] args = gr.getArgs();
        switch (gr.mo2417hq() - aDJ._m_methodCount) {
            case 0:
                return m17947au();
            case 1:
                return m17945ap();
            case 2:
                m17949b((UUID) args[0]);
                return null;
            case 3:
                return m17946ar();
            case 4:
                m17948b((String) args[0]);
                return null;
            case 5:
                return drt();
            case 6:
                m17953nu((String) args[0]);
                return null;
            case 7:
                return drv();
            case 8:
                m17955z((AvatarSector) args[0]);
                return null;
            case 9:
                m17937M((BodyPiece) args[0]);
                return null;
            case 10:
                m17938O((BodyPiece) args[0]);
                return null;
            case 11:
                return drx();
            case 12:
                return new Boolean(m17939Q((BodyPiece) args[0]));
            default:
                return super.mo14a(gr);
        }
    }

    /* renamed from: a */
    public void mo15a(Collection collection, C0495Gr gr) {
        super.mo15a(collection, gr);
    }

    /* renamed from: aq */
    public UUID mo18aq() {
        switch (bFf().mo6893i(f3721bN)) {
            case 0:
                return null;
            case 2:
                return (UUID) bFf().mo5606d(new aCE(this, f3721bN, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f3721bN, new Object[0]));
                break;
        }
        return m17945ap();
    }

    /* renamed from: c */
    public C5663aRz[] mo25c() {
        return _m_fields;
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Bank Name")
    public String dru() {
        switch (bFf().mo6893i(iHh)) {
            case 0:
                return null;
            case 2:
                return (String) bFf().mo5606d(new aCE(this, iHh, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, iHh, new Object[0]));
                break;
        }
        return drt();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Sector Filter")
    public AvatarSector drw() {
        switch (bFf().mo6893i(iHj)) {
            case 0:
                return null;
            case 2:
                return (AvatarSector) bFf().mo5606d(new aCE(this, iHj, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, iHj, new Object[0]));
                break;
        }
        return drv();
    }

    @C3248pc(aYR = C3248pc.C3250b.ITERATOR, displayName = "Bodypiece Bank")
    public List<BodyPiece> dry() {
        switch (bFf().mo6893i(iHn)) {
            case 0:
                return null;
            case 2:
                return (List) bFf().mo5606d(new aCE(this, iHn, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, iHn, new Object[0]));
                break;
        }
        return drx();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Handle")
    public String getHandle() {
        switch (bFf().mo6893i(f3723bP)) {
            case 0:
                return null;
            case 2:
                return (String) bFf().mo5606d(new aCE(this, f3723bP, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f3723bP, new Object[0]));
                break;
        }
        return m17946ar();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Handle")
    @C5566aOg
    public void setHandle(String str) {
        switch (bFf().mo6893i(f3724bQ)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f3724bQ, new Object[]{str}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f3724bQ, new Object[]{str}));
                break;
        }
        m17948b(str);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Bank Name")
    @C5566aOg
    /* renamed from: nv */
    public void mo11218nv(String str) {
        switch (bFf().mo6893i(iHi)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, iHi, new Object[]{str}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, iHi, new Object[]{str}));
                break;
        }
        m17953nu(str);
    }

    public String toString() {
        switch (bFf().mo6893i(_f_toString_0020_0028_0029Ljava_002flang_002fString_003b)) {
            case 0:
                return null;
            case 2:
                return (String) bFf().mo5606d(new aCE(this, _f_toString_0020_0028_0029Ljava_002flang_002fString_003b, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, _f_toString_0020_0028_0029Ljava_002flang_002fString_003b, new Object[0]));
                break;
        }
        return m17947au();
    }

    @C0064Am(aul = "730cd6c3cd4b9b190adfa480815444e4", aum = 0)
    /* renamed from: au */
    private String m17947au() {
        return drq();
    }

    /* renamed from: S */
    public void mo10S() {
        super.mo10S();
        m17942a(UUID.randomUUID());
    }

    @C0064Am(aul = "8417822b6f1904d2d7cc58283f63d86c", aum = 0)
    /* renamed from: ap */
    private UUID m17945ap() {
        return m17943an();
    }

    @C0064Am(aul = "b6f4480d89aac021d6160e7130f11b81", aum = 0)
    /* renamed from: b */
    private void m17949b(UUID uuid) {
        m17942a(uuid);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Handle")
    @C0064Am(aul = "d9484af359db0215901c25dd633ad1ff", aum = 0)
    /* renamed from: ar */
    private String m17946ar() {
        return m17944ao();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Bank Name")
    @C0064Am(aul = "c0ff37a66c238e4aedfb4fac94e547f9", aum = 0)
    private String drt() {
        return drq();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Sector Filter")
    @C0064Am(aul = "fd931363598e62d3add0f230b2b8a181", aum = 0)
    private AvatarSector drv() {
        return drr();
    }

    @C3248pc(aYR = C3248pc.C3250b.ADDER, displayName = "Bodypiece Bank")
    @C0064Am(aul = "9481d8fb3d3916a47dec07a5f5868abf", aum = 0)
    /* renamed from: M */
    private void m17937M(BodyPiece qb) {
        if (drr() == null || qb.bpk() == null || drr().equals(qb.bpk())) {
            drs().add(qb);
        }
    }

    @C3248pc(aYR = C3248pc.C3250b.REMOVER, displayName = "Bodypiece Bank")
    @C0064Am(aul = "25055cc106940fbe0e39b77cd5b756de", aum = 0)
    /* renamed from: O */
    private void m17938O(BodyPiece qb) {
        drs().remove(qb);
    }

    @C3248pc(aYR = C3248pc.C3250b.ITERATOR, displayName = "Bodypiece Bank")
    @C0064Am(aul = "bbf2453ecbd2e5e846b116000eeb52be", aum = 0)
    private List<BodyPiece> drx() {
        return Collections.unmodifiableList(drs());
    }

    @C0064Am(aul = "fd1f308ba6d82b7948ee5e6aa4451346", aum = 0)
    /* renamed from: Q */
    private boolean m17939Q(BodyPiece qb) {
        return drs().contains(qb);
    }
}
