package game.script.avatar.body;

import game.network.message.externalizable.aCE;
import game.network.message.serializable.C1556Wo;
import game.network.message.serializable.C2686iZ;
import game.network.message.serializable.C3438ra;
import game.script.avatar.AvatarSector;
import game.script.avatar.ColorWrapper;
import game.script.avatar.TextureGrid;
import game.script.resource.Asset;
import logic.baa.*;
import logic.data.mbean.aEC;
import logic.res.code.C5640aRc;
import logic.res.code.C5663aRz;
import logic.res.code.C6494any;
import logic.res.html.C2491fm;
import p001a.*;
import taikodom.infra.script.I18NString;

import java.awt.geom.Rectangle2D;
import java.util.Collection;
import java.util.List;

@C2712iu(mo19786Bt = BaseAvatarAppearanceType.class)
@C5511aMd
@C6485anp
/* renamed from: a.aoZ  reason: case insensitive filesystem */
/* compiled from: a */
public class AvatarAppearance extends TaikodomObject implements C1616Xf, C5207aAl {

    /* renamed from: Lm */
    public static final C2491fm f5016Lm = null;
    public static final C2491fm _f_getType_0020_0028_0029Ljava_002flang_002fObject_003b = null;
    /* renamed from: _f_setType_0020_0028Ltaikodom_002finfra_002fscript_002fTemplateObject_003b_0029V */
    public static final C2491fm f5017x6a821b1 = null;
    public static final int _m_fieldCount = 0;
    public static final C5663aRz[] _m_fields = null;
    public static final int _m_methodCount = 0;
    public static final C2491fm[] _m_methods = null;
    public static final C2491fm cGP = null;
    public static final C2491fm cGn = null;
    public static final C2491fm cGr = null;
    public static final C2491fm cGt = null;
    public static final C2491fm cSO = null;
    public static final C2491fm cSP = null;
    /* renamed from: eA */
    public static final C5663aRz f5018eA = null;
    /* renamed from: eC */
    public static final C5663aRz f5020eC = null;
    /* renamed from: eE */
    public static final C5663aRz f5022eE = null;
    /* renamed from: eG */
    public static final C5663aRz f5024eG = null;
    /* renamed from: eI */
    public static final C5663aRz f5026eI = null;
    /* renamed from: eK */
    public static final C5663aRz f5028eK = null;
    /* renamed from: eM */
    public static final C5663aRz f5030eM = null;
    /* renamed from: eO */
    public static final C5663aRz f5032eO = null;
    /* renamed from: eQ */
    public static final C5663aRz f5034eQ = null;
    /* renamed from: eS */
    public static final C5663aRz f5036eS = null;
    /* renamed from: eU */
    public static final C5663aRz f5038eU = null;
    /* renamed from: eW */
    public static final C5663aRz f5040eW = null;
    /* renamed from: eY */
    public static final C5663aRz f5042eY = null;
    /* renamed from: em */
    public static final C5663aRz f5044em = null;
    /* renamed from: en */
    public static final C5663aRz f5045en = null;
    /* renamed from: ep */
    public static final C5663aRz f5047ep = null;
    /* renamed from: eq */
    public static final C5663aRz f5048eq = null;
    /* renamed from: es */
    public static final C5663aRz f5050es = null;
    /* renamed from: eu */
    public static final C5663aRz f5052eu = null;
    /* renamed from: ew */
    public static final C5663aRz f5054ew = null;
    /* renamed from: ey */
    public static final C5663aRz f5056ey = null;
    /* renamed from: ff */
    public static final C2491fm f5058ff = null;
    public static final C5663aRz gjT = null;
    public static final C5663aRz gjV = null;
    public static final C5663aRz gjX = null;
    public static final C5663aRz gjZ = null;
    public static final C2491fm gkA = null;
    public static final C2491fm gkB = null;
    public static final C2491fm gkC = null;
    public static final C2491fm gkD = null;
    public static final C2491fm gkE = null;
    public static final C2491fm gkF = null;
    public static final C2491fm gkG = null;
    public static final C2491fm gkH = null;
    public static final C2491fm gkI = null;
    public static final C2491fm gkJ = null;
    public static final C2491fm gkK = null;
    public static final C2491fm gkL = null;
    public static final C2491fm gkM = null;
    public static final C2491fm gkN = null;
    public static final C2491fm gkO = null;
    public static final C2491fm gkP = null;
    public static final C2491fm gkQ = null;
    public static final C5663aRz gkb = null;
    public static final C5663aRz gkd = null;
    public static final C5663aRz gkf = null;
    public static final C5663aRz gkh = null;
    public static final C5663aRz gkj = null;
    public static final C5663aRz gkl = null;
    public static final C5663aRz gkn = null;
    public static final C2491fm gko = null;
    public static final C2491fm gkp = null;
    public static final C2491fm gkq = null;
    public static final C2491fm gkr = null;
    public static final C2491fm gks = null;
    public static final C2491fm gkt = null;
    public static final C2491fm gku = null;
    public static final C2491fm gkv = null;
    public static final C2491fm gkw = null;
    public static final C2491fm gkx = null;
    public static final C2491fm gky = null;
    public static final C2491fm gkz = null;
    /* renamed from: gm */
    public static final C2491fm f5059gm = null;
    public static final long serialVersionUID = 0;
    /* renamed from: zT */
    public static final C2491fm f5060zT = null;
    public static C6494any ___iScriptClass;
    @C0064Am(aul = "08164fd57362d155c5abf19ca9b552c4", aum = 9)

    /* renamed from: eB */
    private static C3438ra<BodyPiece> f5019eB;
    @C0064Am(aul = "999c9b5e77b0dae3804413895a6232a7", aum = 10)

    /* renamed from: eD */
    private static C3438ra<BodyPiece> f5021eD;
    @C0064Am(aul = "0c9aaf3659c04c691601bc30f4e6f789", aum = 11)

    /* renamed from: eF */
    private static C3438ra<BodyPiece> f5023eF;
    @C0064Am(aul = "0509677607734841d2fca382aeb73b53", aum = 12)

    /* renamed from: eH */
    private static C3438ra<BodyPiece> f5025eH;
    @C0064Am(aul = "59eeb1ac18428c16c7ef8f3316917602", aum = 13)

    /* renamed from: eJ */
    private static C3438ra<BodyPiece> f5027eJ;
    @C0064Am(aul = "6937b0dbbdba07eade40b24ebbe2e489", aum = 14)

    /* renamed from: eL */
    private static C2686iZ<BodyPiece> f5029eL;
    @C0064Am(aul = "fe8f63d1af21a977e18666722479392a", aum = 15)

    /* renamed from: eN */
    private static C3438ra<TextureScheme> f5031eN;
    @C0064Am(aul = "3bd72ee71e301be45f75f48088dcf6f1", aum = 16)

    /* renamed from: eP */
    private static C3438ra<TextureGrid> f5033eP;
    @C0064Am(aul = "74618cef49e43b25d63a795fb0462bec", aum = 17)

    /* renamed from: eR */
    private static C3438ra<ColorWrapper> f5035eR;
    @C0064Am(aul = "2009a468b2cc5c86345cb08185592b9a", aum = 18)

    /* renamed from: eT */
    private static C3438ra<Asset> f5037eT;
    @C0064Am(aul = "821c51428fb194975e54abd394035bf6", aum = 19)

    /* renamed from: eV */
    private static float f5039eV;
    @C0064Am(aul = "e905d30c994b016479630107354a26c2", aum = 20)

    /* renamed from: eX */
    private static Asset f5041eX;
    @C0064Am(aul = "49f3fd0276f618c949d7340aba06aa72", aum = 0)

    /* renamed from: el */
    private static I18NString f5043el;
    @C0064Am(aul = "17ff3dc597145d9eea000462502bcc03", aum = 2)

    /* renamed from: eo */
    private static boolean f5046eo;
    @C0064Am(aul = "f5779d598c29e6a46c8ea093cbb3bf52", aum = 4)

    /* renamed from: er */
    private static Asset f5049er;
    @C0064Am(aul = "5d5565d4493c2c4052711a954bcf5cf2", aum = 5)

    /* renamed from: et */
    private static Asset f5051et;
    @C0064Am(aul = "f35e8ceb36e98cef9048f53a021726bf", aum = 6)

    /* renamed from: ev */
    private static Asset f5053ev;
    @C0064Am(aul = "f4029875bdc047ddd89b9b4eff2912cc", aum = 7)

    /* renamed from: ex */
    private static C3438ra<BodyPiece> f5055ex;
    @C0064Am(aul = "67182cdac66f84345d906a5e4e2a29b0", aum = 8)

    /* renamed from: ez */
    private static C3438ra<BodyPiece> f5057ez;
    @C0064Am(aul = "ae2e07d27bfb3c3e08603aa0163a374d", aum = 21)
    private static C1556Wo<AvatarSector, BodyPiece> gjS;
    @C0064Am(aul = "76ebdf713a51856ff3e1d5c9b06acb6b", aum = 22)
    private static TextureScheme gjU;
    @C0064Am(aul = "a527ef2b669812ad99cb7eb5bc879169", aum = 23)
    private static TextureGrid gjW;
    @C0064Am(aul = "52805eb1529344e1bfdc651ea3a88889", aum = 24)
    private static ColorWrapper gjY;
    @C0064Am(aul = "ab04d990ac1f5d0746b417cfb640ac91", aum = 25)
    private static Asset gka;
    @C0064Am(aul = "44a55af1d7f9783dad5151b005b2a503", aum = 26)
    private static TextureGrid gkc;
    @C0064Am(aul = "f81d74b9cffc734ae543afc3c32df71d", aum = 27)
    private static TextureGrid gke;
    @C0064Am(aul = "864089646154f93c1344e53941f10ca2", aum = 28)
    private static TextureGrid gkg;
    @C0064Am(aul = "433d08afe51f11771cf7d66637820b31", aum = 29)
    private static float gki;
    @C0064Am(aul = "bb85ed2cc22e3b46c936c5c106849244", aum = 30)
    private static float gkk;
    @C0064Am(aul = "f7f06b2896868c7be000076ee51f0efb", aum = 31)
    private static float gkm;
    @C0064Am(aul = "a603ed45210b02818d3ef158e868d939", aum = 3)
    private static String prefix;
    @C0064Am(aul = "ad79dd00a1c5e5dde271875f9da3f124", aum = 1)
    private static boolean restricted;

    static {
        m24461V();
    }

    public AvatarAppearance() {
        super((C5540aNg) null);
        super.mo10S();
    }

    public AvatarAppearance(AvatarAppearanceType t) {
        super((C5540aNg) null);
        super._m_script_init(t);
    }

    public AvatarAppearance(C5540aNg ang) {
        super(ang);
    }

    /* renamed from: V */
    static void m24461V() {
        serialVersionUID = (long) 1;
        _m_fieldCount = TaikodomObject._m_fieldCount + 32;
        _m_methodCount = TaikodomObject._m_methodCount + 41;
        int i = TaikodomObject._m_fieldCount;
        C5663aRz[] arzArr = new C5663aRz[(i + 32)];
        C5663aRz b = C5640aRc.m17844b(AvatarAppearance.class, "49f3fd0276f618c949d7340aba06aa72", i);
        f5044em = b;
        arzArr[i] = b;
        int i2 = i + 1;
        C5663aRz b2 = C5640aRc.m17844b(AvatarAppearance.class, "ad79dd00a1c5e5dde271875f9da3f124", i2);
        f5045en = b2;
        arzArr[i2] = b2;
        int i3 = i2 + 1;
        C5663aRz b3 = C5640aRc.m17844b(AvatarAppearance.class, "17ff3dc597145d9eea000462502bcc03", i3);
        f5047ep = b3;
        arzArr[i3] = b3;
        int i4 = i3 + 1;
        C5663aRz b4 = C5640aRc.m17844b(AvatarAppearance.class, "a603ed45210b02818d3ef158e868d939", i4);
        f5048eq = b4;
        arzArr[i4] = b4;
        int i5 = i4 + 1;
        C5663aRz b5 = C5640aRc.m17844b(AvatarAppearance.class, "f5779d598c29e6a46c8ea093cbb3bf52", i5);
        f5050es = b5;
        arzArr[i5] = b5;
        int i6 = i5 + 1;
        C5663aRz b6 = C5640aRc.m17844b(AvatarAppearance.class, "5d5565d4493c2c4052711a954bcf5cf2", i6);
        f5052eu = b6;
        arzArr[i6] = b6;
        int i7 = i6 + 1;
        C5663aRz b7 = C5640aRc.m17844b(AvatarAppearance.class, "f35e8ceb36e98cef9048f53a021726bf", i7);
        f5054ew = b7;
        arzArr[i7] = b7;
        int i8 = i7 + 1;
        C5663aRz b8 = C5640aRc.m17844b(AvatarAppearance.class, "f4029875bdc047ddd89b9b4eff2912cc", i8);
        f5056ey = b8;
        arzArr[i8] = b8;
        int i9 = i8 + 1;
        C5663aRz b9 = C5640aRc.m17844b(AvatarAppearance.class, "67182cdac66f84345d906a5e4e2a29b0", i9);
        f5018eA = b9;
        arzArr[i9] = b9;
        int i10 = i9 + 1;
        C5663aRz b10 = C5640aRc.m17844b(AvatarAppearance.class, "08164fd57362d155c5abf19ca9b552c4", i10);
        f5020eC = b10;
        arzArr[i10] = b10;
        int i11 = i10 + 1;
        C5663aRz b11 = C5640aRc.m17844b(AvatarAppearance.class, "999c9b5e77b0dae3804413895a6232a7", i11);
        f5022eE = b11;
        arzArr[i11] = b11;
        int i12 = i11 + 1;
        C5663aRz b12 = C5640aRc.m17844b(AvatarAppearance.class, "0c9aaf3659c04c691601bc30f4e6f789", i12);
        f5024eG = b12;
        arzArr[i12] = b12;
        int i13 = i12 + 1;
        C5663aRz b13 = C5640aRc.m17844b(AvatarAppearance.class, "0509677607734841d2fca382aeb73b53", i13);
        f5026eI = b13;
        arzArr[i13] = b13;
        int i14 = i13 + 1;
        C5663aRz b14 = C5640aRc.m17844b(AvatarAppearance.class, "59eeb1ac18428c16c7ef8f3316917602", i14);
        f5028eK = b14;
        arzArr[i14] = b14;
        int i15 = i14 + 1;
        C5663aRz b15 = C5640aRc.m17844b(AvatarAppearance.class, "6937b0dbbdba07eade40b24ebbe2e489", i15);
        f5030eM = b15;
        arzArr[i15] = b15;
        int i16 = i15 + 1;
        C5663aRz b16 = C5640aRc.m17844b(AvatarAppearance.class, "fe8f63d1af21a977e18666722479392a", i16);
        f5032eO = b16;
        arzArr[i16] = b16;
        int i17 = i16 + 1;
        C5663aRz b17 = C5640aRc.m17844b(AvatarAppearance.class, "3bd72ee71e301be45f75f48088dcf6f1", i17);
        f5034eQ = b17;
        arzArr[i17] = b17;
        int i18 = i17 + 1;
        C5663aRz b18 = C5640aRc.m17844b(AvatarAppearance.class, "74618cef49e43b25d63a795fb0462bec", i18);
        f5036eS = b18;
        arzArr[i18] = b18;
        int i19 = i18 + 1;
        C5663aRz b19 = C5640aRc.m17844b(AvatarAppearance.class, "2009a468b2cc5c86345cb08185592b9a", i19);
        f5038eU = b19;
        arzArr[i19] = b19;
        int i20 = i19 + 1;
        C5663aRz b20 = C5640aRc.m17844b(AvatarAppearance.class, "821c51428fb194975e54abd394035bf6", i20);
        f5040eW = b20;
        arzArr[i20] = b20;
        int i21 = i20 + 1;
        C5663aRz b21 = C5640aRc.m17844b(AvatarAppearance.class, "e905d30c994b016479630107354a26c2", i21);
        f5042eY = b21;
        arzArr[i21] = b21;
        int i22 = i21 + 1;
        C5663aRz b22 = C5640aRc.m17844b(AvatarAppearance.class, "ae2e07d27bfb3c3e08603aa0163a374d", i22);
        gjT = b22;
        arzArr[i22] = b22;
        int i23 = i22 + 1;
        C5663aRz b23 = C5640aRc.m17844b(AvatarAppearance.class, "76ebdf713a51856ff3e1d5c9b06acb6b", i23);
        gjV = b23;
        arzArr[i23] = b23;
        int i24 = i23 + 1;
        C5663aRz b24 = C5640aRc.m17844b(AvatarAppearance.class, "a527ef2b669812ad99cb7eb5bc879169", i24);
        gjX = b24;
        arzArr[i24] = b24;
        int i25 = i24 + 1;
        C5663aRz b25 = C5640aRc.m17844b(AvatarAppearance.class, "52805eb1529344e1bfdc651ea3a88889", i25);
        gjZ = b25;
        arzArr[i25] = b25;
        int i26 = i25 + 1;
        C5663aRz b26 = C5640aRc.m17844b(AvatarAppearance.class, "ab04d990ac1f5d0746b417cfb640ac91", i26);
        gkb = b26;
        arzArr[i26] = b26;
        int i27 = i26 + 1;
        C5663aRz b27 = C5640aRc.m17844b(AvatarAppearance.class, "44a55af1d7f9783dad5151b005b2a503", i27);
        gkd = b27;
        arzArr[i27] = b27;
        int i28 = i27 + 1;
        C5663aRz b28 = C5640aRc.m17844b(AvatarAppearance.class, "f81d74b9cffc734ae543afc3c32df71d", i28);
        gkf = b28;
        arzArr[i28] = b28;
        int i29 = i28 + 1;
        C5663aRz b29 = C5640aRc.m17844b(AvatarAppearance.class, "864089646154f93c1344e53941f10ca2", i29);
        gkh = b29;
        arzArr[i29] = b29;
        int i30 = i29 + 1;
        C5663aRz b30 = C5640aRc.m17844b(AvatarAppearance.class, "433d08afe51f11771cf7d66637820b31", i30);
        gkj = b30;
        arzArr[i30] = b30;
        int i31 = i30 + 1;
        C5663aRz b31 = C5640aRc.m17844b(AvatarAppearance.class, "bb85ed2cc22e3b46c936c5c106849244", i31);
        gkl = b31;
        arzArr[i31] = b31;
        int i32 = i31 + 1;
        C5663aRz b32 = C5640aRc.m17844b(AvatarAppearance.class, "f7f06b2896868c7be000076ee51f0efb", i32);
        gkn = b32;
        arzArr[i32] = b32;
        int i33 = i32 + 1;
        _m_fields = arzArr;
        C1634Xv.m11725a((Object[]) TaikodomObject._m_fields, (Object[]) _m_fields);
        int i34 = TaikodomObject._m_methodCount;
        C2491fm[] fmVarArr = new C2491fm[(i34 + 41)];
        C2491fm a = C4105zY.m41624a(AvatarAppearance.class, "4d26878dccaf66a276f8a441d324d03a", i34);
        f5017x6a821b1 = a;
        fmVarArr[i34] = a;
        int i35 = i34 + 1;
        C2491fm a2 = C4105zY.m41624a(AvatarAppearance.class, "77a546db8375cbbd7ffa9f6d8c5eb4f6", i35);
        gko = a2;
        fmVarArr[i35] = a2;
        int i36 = i35 + 1;
        C2491fm a3 = C4105zY.m41624a(AvatarAppearance.class, "e3dc6a4a7bc59ddacc25a80e72eb4c0f", i36);
        gkp = a3;
        fmVarArr[i36] = a3;
        int i37 = i36 + 1;
        C2491fm a4 = C4105zY.m41624a(AvatarAppearance.class, "058c37822b0db3ef7acef7b35d193add", i37);
        gkq = a4;
        fmVarArr[i37] = a4;
        int i38 = i37 + 1;
        C2491fm a5 = C4105zY.m41624a(AvatarAppearance.class, "a5c244d0f309c6c99434968a5cbe28fd", i38);
        gkr = a5;
        fmVarArr[i38] = a5;
        int i39 = i38 + 1;
        C2491fm a6 = C4105zY.m41624a(AvatarAppearance.class, "50b586dec5b37bb817043d28b3acab72", i39);
        gks = a6;
        fmVarArr[i39] = a6;
        int i40 = i39 + 1;
        C2491fm a7 = C4105zY.m41624a(AvatarAppearance.class, "5c1a9579fb93ca231146de44740f37b3", i40);
        gkt = a7;
        fmVarArr[i40] = a7;
        int i41 = i40 + 1;
        C2491fm a8 = C4105zY.m41624a(AvatarAppearance.class, "6723370af69be941bfaddc3ceb74b7eb", i41);
        gku = a8;
        fmVarArr[i41] = a8;
        int i42 = i41 + 1;
        C2491fm a9 = C4105zY.m41624a(AvatarAppearance.class, "9f62dd28a633a58f63bfb771b60df46f", i42);
        gkv = a9;
        fmVarArr[i42] = a9;
        int i43 = i42 + 1;
        C2491fm a10 = C4105zY.m41624a(AvatarAppearance.class, "ac4ba8e137e4f63bd12742eefd4d6148", i43);
        gkw = a10;
        fmVarArr[i43] = a10;
        int i44 = i43 + 1;
        C2491fm a11 = C4105zY.m41624a(AvatarAppearance.class, "f9d25d4a255569394972616fd7166258", i44);
        gkx = a11;
        fmVarArr[i44] = a11;
        int i45 = i44 + 1;
        C2491fm a12 = C4105zY.m41624a(AvatarAppearance.class, "b6566974f65a38a51181c7ddf0ae79c2", i45);
        gky = a12;
        fmVarArr[i45] = a12;
        int i46 = i45 + 1;
        C2491fm a13 = C4105zY.m41624a(AvatarAppearance.class, "ed90f0e6be6bdc41c09f02e6d60a1bb2", i46);
        gkz = a13;
        fmVarArr[i46] = a13;
        int i47 = i46 + 1;
        C2491fm a14 = C4105zY.m41624a(AvatarAppearance.class, "dbad2f64c85b18d0830962e90834c081", i47);
        cGP = a14;
        fmVarArr[i47] = a14;
        int i48 = i47 + 1;
        C2491fm a15 = C4105zY.m41624a(AvatarAppearance.class, "cc0f6c1810a853118c88022efd7ee9af", i48);
        gkA = a15;
        fmVarArr[i48] = a15;
        int i49 = i48 + 1;
        C2491fm a16 = C4105zY.m41624a(AvatarAppearance.class, "b5d071aaffd1a4677405ce56dae66a4e", i49);
        cSP = a16;
        fmVarArr[i49] = a16;
        int i50 = i49 + 1;
        C2491fm a17 = C4105zY.m41624a(AvatarAppearance.class, "b05cc598fcb974e533adf007ccc7d520", i50);
        gkB = a17;
        fmVarArr[i50] = a17;
        int i51 = i50 + 1;
        C2491fm a18 = C4105zY.m41624a(AvatarAppearance.class, "6db0da67f4145feca8acc9f582b9a5b5", i51);
        gkC = a18;
        fmVarArr[i51] = a18;
        int i52 = i51 + 1;
        C2491fm a19 = C4105zY.m41624a(AvatarAppearance.class, "ec6d40d914c087351e1df534bdc3752a", i52);
        gkD = a19;
        fmVarArr[i52] = a19;
        int i53 = i52 + 1;
        C2491fm a20 = C4105zY.m41624a(AvatarAppearance.class, "ce5672936b8a8afa65cef467614174f4", i53);
        gkE = a20;
        fmVarArr[i53] = a20;
        int i54 = i53 + 1;
        C2491fm a21 = C4105zY.m41624a(AvatarAppearance.class, "dfeaedd9d4b2c27d0eefd3417fb15f8c", i54);
        gkF = a21;
        fmVarArr[i54] = a21;
        int i55 = i54 + 1;
        C2491fm a22 = C4105zY.m41624a(AvatarAppearance.class, "8168cd790c762048a1209a317294f0c2", i55);
        gkG = a22;
        fmVarArr[i55] = a22;
        int i56 = i55 + 1;
        C2491fm a23 = C4105zY.m41624a(AvatarAppearance.class, "7696120de29067ce857a1920acf04136", i56);
        gkH = a23;
        fmVarArr[i56] = a23;
        int i57 = i56 + 1;
        C2491fm a24 = C4105zY.m41624a(AvatarAppearance.class, "2047e4d5efce23b984744da9485884e6", i57);
        gkI = a24;
        fmVarArr[i57] = a24;
        int i58 = i57 + 1;
        C2491fm a25 = C4105zY.m41624a(AvatarAppearance.class, "3b6c9d576cf19a92d1fe7e956a867fd2", i58);
        gkJ = a25;
        fmVarArr[i58] = a25;
        int i59 = i58 + 1;
        C2491fm a26 = C4105zY.m41624a(AvatarAppearance.class, "2a5d14b345e225671f0e35b91b20f359", i59);
        gkK = a26;
        fmVarArr[i59] = a26;
        int i60 = i59 + 1;
        C2491fm a27 = C4105zY.m41624a(AvatarAppearance.class, "098254157edd989a5198988820b392c9", i60);
        cGr = a27;
        fmVarArr[i60] = a27;
        int i61 = i60 + 1;
        C2491fm a28 = C4105zY.m41624a(AvatarAppearance.class, "f6d148bf5945a1dc30569eb3ad6c172c", i61);
        cGt = a28;
        fmVarArr[i61] = a28;
        int i62 = i61 + 1;
        C2491fm a29 = C4105zY.m41624a(AvatarAppearance.class, "b359e709f676082800de88aced46f93e", i62);
        cGn = a29;
        fmVarArr[i62] = a29;
        int i63 = i62 + 1;
        C2491fm a30 = C4105zY.m41624a(AvatarAppearance.class, "cd48552cd3f989161a69d225807c8407", i63);
        gkL = a30;
        fmVarArr[i63] = a30;
        int i64 = i63 + 1;
        C2491fm a31 = C4105zY.m41624a(AvatarAppearance.class, "c721a146b3079e5646ebb067253ec07c", i64);
        gkM = a31;
        fmVarArr[i64] = a31;
        int i65 = i64 + 1;
        C2491fm a32 = C4105zY.m41624a(AvatarAppearance.class, "4c82c5e3943014a30dd3c9c0c58affce", i65);
        gkN = a32;
        fmVarArr[i65] = a32;
        int i66 = i65 + 1;
        C2491fm a33 = C4105zY.m41624a(AvatarAppearance.class, "36f0122f39ec2dfd82feb77569361312", i66);
        f5060zT = a33;
        fmVarArr[i66] = a33;
        int i67 = i66 + 1;
        C2491fm a34 = C4105zY.m41624a(AvatarAppearance.class, "33565b0b47b6f478edad0e40616b1722", i67);
        gkO = a34;
        fmVarArr[i67] = a34;
        int i68 = i67 + 1;
        C2491fm a35 = C4105zY.m41624a(AvatarAppearance.class, "b9821a84e746f3113bd4e645321cdb87", i68);
        gkP = a35;
        fmVarArr[i68] = a35;
        int i69 = i68 + 1;
        C2491fm a36 = C4105zY.m41624a(AvatarAppearance.class, "d5e25794a341814ff1c32b2f81f45b0f", i69);
        f5058ff = a36;
        fmVarArr[i69] = a36;
        int i70 = i69 + 1;
        C2491fm a37 = C4105zY.m41624a(AvatarAppearance.class, "7231351ec03c58b60c35fac7aedde7dc", i70);
        gkQ = a37;
        fmVarArr[i70] = a37;
        int i71 = i70 + 1;
        C2491fm a38 = C4105zY.m41624a(AvatarAppearance.class, "c2a6a3227b39f9842eee9a650df84b62", i71);
        f5059gm = a38;
        fmVarArr[i71] = a38;
        int i72 = i71 + 1;
        C2491fm a39 = C4105zY.m41624a(AvatarAppearance.class, "57ebf99f6fcc46f6956ca511f31d7331", i72);
        cSO = a39;
        fmVarArr[i72] = a39;
        int i73 = i72 + 1;
        C2491fm a40 = C4105zY.m41624a(AvatarAppearance.class, "7077ad82a3e052fbdd03931612fb1086", i73);
        f5016Lm = a40;
        fmVarArr[i73] = a40;
        int i74 = i73 + 1;
        C2491fm a41 = C4105zY.m41624a(AvatarAppearance.class, "64991a9fad27fefe17675c801f9ec956", i74);
        _f_getType_0020_0028_0029Ljava_002flang_002fObject_003b = a41;
        fmVarArr[i74] = a41;
        int i75 = i74 + 1;
        _m_methods = fmVarArr;
        C1634Xv.m11725a((Object[]) TaikodomObject._m_methods, (Object[]) _m_methods);
        ___iScriptClass = aUO.m18566a(AvatarAppearance.class, aEC.class, _m_fields, _m_methods);
    }

    /* renamed from: Y */
    private void m24462Y(C1556Wo wo) {
        bFf().mo5608dq().mo3197f(gjT, wo);
    }

    @C0064Am(aul = "50b586dec5b37bb817043d28b3acab72", aum = 0)
    @C5566aOg
    @C2499fr
    /* renamed from: a */
    private void m24463a(AvatarSector nw, BodyPiece qb) {
        throw new aWi(new aCE(this, gks, new Object[]{nw, qb}));
    }

    /* renamed from: a */
    private void m24464a(C2686iZ iZVar) {
        throw new C6039afL();
    }

    /* renamed from: a */
    private void m24465a(Asset tCVar) {
        throw new C6039afL();
    }

    /* renamed from: a */
    private void m24466a(I18NString i18NString) {
        throw new C6039afL();
    }

    /* renamed from: b */
    private void m24468b(C3438ra raVar) {
        throw new C6039afL();
    }

    /* renamed from: b */
    private void m24469b(Asset tCVar) {
        throw new C6039afL();
    }

    /* renamed from: bc */
    private I18NString m24471bc() {
        return ((AvatarAppearanceType) getType()).mo5543by();
    }

    /* renamed from: bd */
    private boolean m24472bd() {
        return ((AvatarAppearanceType) getType()).mo5530bA();
    }

    /* renamed from: be */
    private boolean m24473be() {
        return ((AvatarAppearanceType) getType()).mo5531bC();
    }

    /* renamed from: bf */
    private String m24474bf() {
        return ((AvatarAppearanceType) getType()).getPrefix();
    }

    /* renamed from: bg */
    private Asset m24475bg() {
        return ((AvatarAppearanceType) getType()).mo5532bF();
    }

    /* renamed from: bh */
    private Asset m24476bh() {
        return ((AvatarAppearanceType) getType()).mo5533bH();
    }

    /* renamed from: bi */
    private Asset m24477bi() {
        return ((AvatarAppearanceType) getType()).mo5534bJ();
    }

    /* renamed from: bj */
    private C3438ra m24478bj() {
        return ((AvatarAppearanceType) getType()).mo5535bL();
    }

    /* renamed from: bk */
    private C3438ra m24479bk() {
        return ((AvatarAppearanceType) getType()).mo5537bP();
    }

    /* renamed from: bl */
    private C3438ra m24480bl() {
        return ((AvatarAppearanceType) getType()).mo5539bT();
    }

    /* renamed from: bm */
    private C3438ra m24481bm() {
        return ((AvatarAppearanceType) getType()).mo5541bX();
    }

    /* renamed from: bn */
    private C3438ra m24482bn() {
        return ((AvatarAppearanceType) getType()).mo5551cb();
    }

    /* renamed from: bo */
    private C3438ra m24483bo() {
        return ((AvatarAppearanceType) getType()).mo5553cf();
    }

    /* renamed from: bp */
    private C3438ra m24484bp() {
        return ((AvatarAppearanceType) getType()).mo5555cj();
    }

    /* renamed from: bq */
    private C2686iZ m24485bq() {
        return ((AvatarAppearanceType) getType()).mo5557cn();
    }

    /* renamed from: br */
    private C3438ra m24486br() {
        return ((AvatarAppearanceType) getType()).mo5559cr();
    }

    /* renamed from: bs */
    private C3438ra m24487bs() {
        return ((AvatarAppearanceType) getType()).mo5561cv();
    }

    /* renamed from: bt */
    private C3438ra m24488bt() {
        return ((AvatarAppearanceType) getType()).mo5563cz();
    }

    /* renamed from: bu */
    private C3438ra m24489bu() {
        return ((AvatarAppearanceType) getType()).mo5546cD();
    }

    /* renamed from: bv */
    private float m24490bv() {
        return ((AvatarAppearanceType) getType()).mo5548cH();
    }

    /* renamed from: bv */
    private void m24491bv(Asset tCVar) {
        bFf().mo5608dq().mo3197f(gkb, tCVar);
    }

    /* renamed from: bw */
    private Asset m24492bw() {
        return ((AvatarAppearanceType) getType()).mo5549cJ();
    }

    @C0064Am(aul = "3b6c9d576cf19a92d1fe7e956a867fd2", aum = 0)
    @C5566aOg
    @C2499fr
    /* renamed from: bw */
    private void m24493bw(Asset tCVar) {
        throw new aWi(new aCE(this, gkJ, new Object[]{tCVar}));
    }

    /* renamed from: c */
    private void m24494c(C3438ra raVar) {
        throw new C6039afL();
    }

    /* renamed from: c */
    private void m24495c(Asset tCVar) {
        throw new C6039afL();
    }

    private TextureScheme coA() {
        return (TextureScheme) bFf().mo5608dq().mo3214p(gjV);
    }

    private TextureGrid coB() {
        return (TextureGrid) bFf().mo5608dq().mo3214p(gjX);
    }

    private ColorWrapper coC() {
        return (ColorWrapper) bFf().mo5608dq().mo3214p(gjZ);
    }

    private Asset coD() {
        return (Asset) bFf().mo5608dq().mo3214p(gkb);
    }

    private TextureGrid coE() {
        return (TextureGrid) bFf().mo5608dq().mo3214p(gkd);
    }

    private TextureGrid coF() {
        return (TextureGrid) bFf().mo5608dq().mo3214p(gkf);
    }

    private TextureGrid coG() {
        return (TextureGrid) bFf().mo5608dq().mo3214p(gkh);
    }

    private float coH() {
        return bFf().mo5608dq().mo3211m(gkj);
    }

    private float coI() {
        return bFf().mo5608dq().mo3211m(gkl);
    }

    private float coJ() {
        return bFf().mo5608dq().mo3211m(gkn);
    }

    private C1556Wo coz() {
        return (C1556Wo) bFf().mo5608dq().mo3214p(gjT);
    }

    /* renamed from: d */
    private void m24497d(C3438ra raVar) {
        throw new C6039afL();
    }

    /* renamed from: d */
    private void m24498d(Asset tCVar) {
        throw new C6039afL();
    }

    /* renamed from: e */
    private void m24499e(TextureScheme ark) {
        bFf().mo5608dq().mo3197f(gjV, ark);
    }

    /* renamed from: e */
    private void m24500e(C3438ra raVar) {
        throw new C6039afL();
    }

    @C0064Am(aul = "6723370af69be941bfaddc3ceb74b7eb", aum = 0)
    @C5566aOg
    @C2499fr
    /* renamed from: f */
    private void m24501f(TextureScheme ark) {
        throw new aWi(new aCE(this, gku, new Object[]{ark}));
    }

    /* renamed from: f */
    private void m24502f(C3438ra raVar) {
        throw new C6039afL();
    }

    /* renamed from: f */
    private void m24503f(String str) {
        throw new C6039afL();
    }

    /* renamed from: g */
    private void m24504g(C3438ra raVar) {
        throw new C6039afL();
    }

    /* renamed from: h */
    private void m24505h(ColorWrapper apf) {
        bFf().mo5608dq().mo3197f(gjZ, apf);
    }

    /* renamed from: h */
    private void m24506h(C3438ra raVar) {
        throw new C6039afL();
    }

    /* renamed from: h */
    private void m24507h(boolean z) {
        throw new C6039afL();
    }

    /* renamed from: i */
    private void m24508i(TextureGrid uk) {
        bFf().mo5608dq().mo3197f(gjX, uk);
    }

    @C0064Am(aul = "cc0f6c1810a853118c88022efd7ee9af", aum = 0)
    @C5566aOg
    @C2499fr
    /* renamed from: i */
    private void m24509i(ColorWrapper apf) {
        throw new aWi(new aCE(this, gkA, new Object[]{apf}));
    }

    /* renamed from: i */
    private void m24510i(C3438ra raVar) {
        throw new C6039afL();
    }

    /* renamed from: i */
    private void m24511i(boolean z) {
        throw new C6039afL();
    }

    /* renamed from: j */
    private void m24512j(TextureGrid uk) {
        bFf().mo5608dq().mo3197f(gkd, uk);
    }

    /* renamed from: j */
    private void m24513j(C3438ra raVar) {
        throw new C6039afL();
    }

    /* renamed from: k */
    private void m24514k(TextureGrid uk) {
        bFf().mo5608dq().mo3197f(gkf, uk);
    }

    /* renamed from: k */
    private void m24515k(C3438ra raVar) {
        throw new C6039afL();
    }

    /* renamed from: kd */
    private void m24517kd(float f) {
        bFf().mo5608dq().mo3150a(gkj, f);
    }

    /* renamed from: ke */
    private void m24518ke(float f) {
        bFf().mo5608dq().mo3150a(gkl, f);
    }

    /* renamed from: kf */
    private void m24519kf(float f) {
        bFf().mo5608dq().mo3150a(gkn, f);
    }

    @C0064Am(aul = "ac4ba8e137e4f63bd12742eefd4d6148", aum = 0)
    @C5566aOg
    @C2499fr
    /* renamed from: kg */
    private void m24520kg(float f) {
        throw new aWi(new aCE(this, gkw, new Object[]{new Float(f)}));
    }

    @C0064Am(aul = "b05cc598fcb974e533adf007ccc7d520", aum = 0)
    @C5566aOg
    @C2499fr
    /* renamed from: ki */
    private void m24521ki(float f) {
        throw new aWi(new aCE(this, gkB, new Object[]{new Float(f)}));
    }

    @C0064Am(aul = "b9821a84e746f3113bd4e645321cdb87", aum = 0)
    @C5566aOg
    @C2499fr
    /* renamed from: kk */
    private void m24522kk(float f) {
        throw new aWi(new aCE(this, gkP, new Object[]{new Float(f)}));
    }

    /* renamed from: l */
    private void m24523l(TextureGrid uk) {
        bFf().mo5608dq().mo3197f(gkh, uk);
    }

    /* renamed from: l */
    private void m24524l(C3438ra raVar) {
        throw new C6039afL();
    }

    @C0064Am(aul = "b6566974f65a38a51181c7ddf0ae79c2", aum = 0)
    @C5566aOg
    @C2499fr
    /* renamed from: m */
    private void m24525m(TextureGrid uk) {
        throw new aWi(new aCE(this, gky, new Object[]{uk}));
    }

    @C0064Am(aul = "e3dc6a4a7bc59ddacc25a80e72eb4c0f", aum = 0)
    @C5566aOg
    /* renamed from: o */
    private void m24526o(AvatarAppearanceType t) {
        throw new aWi(new aCE(this, gkp, new Object[]{t}));
    }

    @C0064Am(aul = "ec6d40d914c087351e1df534bdc3752a", aum = 0)
    @C5566aOg
    @C2499fr
    /* renamed from: o */
    private void m24527o(TextureGrid uk) {
        throw new aWi(new aCE(this, gkD, new Object[]{uk}));
    }

    @C5566aOg
    /* renamed from: p */
    private void m24528p(AvatarAppearanceType t) {
        switch (bFf().mo6893i(gkp)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, gkp, new Object[]{t}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, gkp, new Object[]{t}));
                break;
        }
        m24526o(t);
    }

    @C0064Am(aul = "dfeaedd9d4b2c27d0eefd3417fb15f8c", aum = 0)
    @C5566aOg
    @C2499fr
    /* renamed from: q */
    private void m24529q(TextureGrid uk) {
        throw new aWi(new aCE(this, gkF, new Object[]{uk}));
    }

    /* renamed from: s */
    private void m24532s(float f) {
        throw new C6039afL();
    }

    @C0064Am(aul = "7696120de29067ce857a1920acf04136", aum = 0)
    @C5566aOg
    @C2499fr
    /* renamed from: s */
    private void m24533s(TextureGrid uk) {
        throw new aWi(new aCE(this, gkH, new Object[]{uk}));
    }

    @C0064Am(aul = "77a546db8375cbbd7ffa9f6d8c5eb4f6", aum = 0)
    @C5566aOg
    /* renamed from: y */
    private <T> T m24535y(List<T> list) {
        throw new aWi(new aCE(this, gko, new Object[]{list}));
    }

    @C5566aOg
    /* renamed from: z */
    private <T> T m24536z(List<T> list) {
        switch (bFf().mo6893i(gko)) {
            case 0:
                return null;
            case 2:
                return bFf().mo5606d(new aCE(this, gko, new Object[]{list}));
            case 3:
                bFf().mo5606d(new aCE(this, gko, new Object[]{list}));
                break;
        }
        return m24535y(list);
    }

    /* renamed from: S */
    public void mo10S() {
        super.mo10S();
    }

    /* renamed from: T */
    public C6494any mo11T() {
        return ___iScriptClass;
    }

    /* renamed from: U */
    public C2491fm[] mo12U() {
        return _m_methods;
    }

    /* renamed from: W */
    public Object mo13W() {
        return new aEC(this);
    }

    /* renamed from: a */
    public Object mo14a(C0495Gr gr) {
        Object[] args = gr.getArgs();
        switch (gr.mo2417hq() - TaikodomObject._m_methodCount) {
            case 0:
                m24467b((C2961mJ) args[0]);
                return null;
            case 1:
                return m24535y((List) args[0]);
            case 2:
                m24526o((AvatarAppearanceType) args[0]);
                return null;
            case 3:
                return m24534x((AvatarSector) args[0]);
            case 4:
                return coK();
            case 5:
                m24463a((AvatarSector) args[0], (BodyPiece) args[1]);
                return null;
            case 6:
                return coL();
            case 7:
                m24501f((TextureScheme) args[0]);
                return null;
            case 8:
                return new Float(coM());
            case 9:
                m24520kg(((Float) args[0]).floatValue());
                return null;
            case 10:
                return coN();
            case 11:
                m24525m((TextureGrid) args[0]);
                return null;
            case 12:
                return coO();
            case 13:
                return aGj();
            case 14:
                m24509i((ColorWrapper) args[0]);
                return null;
            case 15:
                return new Float(aOC());
            case 16:
                m24521ki(((Float) args[0]).floatValue());
                return null;
            case 17:
                return coP();
            case 18:
                m24527o((TextureGrid) args[0]);
                return null;
            case 19:
                return coQ();
            case 20:
                m24529q((TextureGrid) args[0]);
                return null;
            case 21:
                return coR();
            case 22:
                m24533s((TextureGrid) args[0]);
                return null;
            case 23:
                return coS();
            case 24:
                m24493bw((Asset) args[0]);
                return null;
            case 25:
                return coT();
            case 26:
                return aFN();
            case 27:
                return aFP();
            case 28:
                return aFH();
            case 29:
                return new Boolean(coU());
            case 30:
                return coV();
            case 31:
                return coW();
            case 32:
                return m24516kd();
            case 33:
                return new Float(coX());
            case 34:
                m24522kk(((Float) args[0]).floatValue());
                return null;
            case 35:
                return m24470bD();
            case 36:
                return coY();
            case 37:
                return m24496cI();
            case 38:
                return new Float(aOB());
            case 39:
                m24530qU();
                return null;
            case 40:
                return m24531qW();
            default:
                return super.mo14a(gr);
        }
    }

    /* renamed from: a */
    public void mo15a(Collection collection, C0495Gr gr) {
        super.mo15a(collection, gr);
    }

    @ClientOnly
    public Collection<AvatarSector> aFI() {
        switch (bFf().mo6893i(cGn)) {
            case 0:
                return null;
            case 2:
                return (Collection) bFf().mo5606d(new aCE(this, cGn, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, cGn, new Object[0]));
                break;
        }
        return aFH();
    }

    @ClientOnly
    public AvatarSector aFO() {
        switch (bFf().mo6893i(cGr)) {
            case 0:
                return null;
            case 2:
                return (AvatarSector) bFf().mo5606d(new aCE(this, cGr, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, cGr, new Object[0]));
                break;
        }
        return aFN();
    }

    @ClientOnly
    public AvatarSector aFQ() {
        switch (bFf().mo6893i(cGt)) {
            case 0:
                return null;
            case 2:
                return (AvatarSector) bFf().mo5606d(new aCE(this, cGt, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, cGt, new Object[0]));
                break;
        }
        return aFP();
    }

    @ClientOnly
    public Rectangle2D.Float aGk() {
        switch (bFf().mo6893i(cGP)) {
            case 0:
                return null;
            case 2:
                return (Rectangle2D.Float) bFf().mo5606d(new aCE(this, cGP, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, cGP, new Object[0]));
                break;
        }
        return aGj();
    }

    public float aOD() {
        switch (bFf().mo6893i(cSP)) {
            case 0:
                return 0.0f;
            case 2:
                return ((Float) bFf().mo5606d(new aCE(this, cSP, new Object[0]))).floatValue();
            case 3:
                bFf().mo5606d(new aCE(this, cSP, new Object[0]));
                break;
        }
        return aOC();
    }

    @C5566aOg
    @C2499fr
    /* renamed from: b */
    public void mo15257b(AvatarSector nw, BodyPiece qb) {
        switch (bFf().mo6893i(gks)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, gks, new Object[]{nw, qb}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, gks, new Object[]{nw, qb}));
                break;
        }
        m24463a(nw, qb);
    }

    public Asset bdT() {
        switch (bFf().mo6893i(gkQ)) {
            case 0:
                return null;
            case 2:
                return (Asset) bFf().mo5606d(new aCE(this, gkQ, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, gkQ, new Object[0]));
                break;
        }
        return coY();
    }

    public Collection<BodyPiece> bdU() {
        switch (bFf().mo6893i(gkr)) {
            case 0:
                return null;
            case 2:
                return (Collection) bFf().mo5606d(new aCE(this, gkr, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, gkr, new Object[0]));
                break;
        }
        return coK();
    }

    @ClientOnly
    public AvatarSector bdV() {
        switch (bFf().mo6893i(gkK)) {
            case 0:
                return null;
            case 2:
                return (AvatarSector) bFf().mo5606d(new aCE(this, gkK, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, gkK, new Object[0]));
                break;
        }
        return coT();
    }

    public Asset bdW() {
        switch (bFf().mo6893i(gkI)) {
            case 0:
                return null;
            case 2:
                return (Asset) bFf().mo5606d(new aCE(this, gkI, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, gkI, new Object[0]));
                break;
        }
        return coS();
    }

    public ColorWrapper bdX() {
        switch (bFf().mo6893i(gkz)) {
            case 0:
                return null;
            case 2:
                return (ColorWrapper) bFf().mo5606d(new aCE(this, gkz, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, gkz, new Object[0]));
                break;
        }
        return coO();
    }

    public float bdY() {
        switch (bFf().mo6893i(gkO)) {
            case 0:
                return 0.0f;
            case 2:
                return ((Float) bFf().mo5606d(new aCE(this, gkO, new Object[0]))).floatValue();
            case 3:
                bFf().mo5606d(new aCE(this, gkO, new Object[0]));
                break;
        }
        return coX();
    }

    public TextureGrid bdZ() {
        switch (bFf().mo6893i(gkx)) {
            case 0:
                return null;
            case 2:
                return (TextureGrid) bFf().mo5606d(new aCE(this, gkx, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, gkx, new Object[0]));
                break;
        }
        return coN();
    }

    public Asset bea() {
        switch (bFf().mo6893i(gkN)) {
            case 0:
                return null;
            case 2:
                return (Asset) bFf().mo5606d(new aCE(this, gkN, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, gkN, new Object[0]));
                break;
        }
        return coW();
    }

    public Asset beb() {
        switch (bFf().mo6893i(gkM)) {
            case 0:
                return null;
            case 2:
                return (Asset) bFf().mo5606d(new aCE(this, gkM, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, gkM, new Object[0]));
                break;
        }
        return coV();
    }

    public float bec() {
        switch (bFf().mo6893i(gkv)) {
            case 0:
                return 0.0f;
            case 2:
                return ((Float) bFf().mo5606d(new aCE(this, gkv, new Object[0]))).floatValue();
            case 3:
                bFf().mo5606d(new aCE(this, gkv, new Object[0]));
                break;
        }
        return coM();
    }

    public TextureScheme bed() {
        switch (bFf().mo6893i(gkt)) {
            case 0:
                return null;
            case 2:
                return (TextureScheme) bFf().mo5606d(new aCE(this, gkt, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, gkt, new Object[0]));
                break;
        }
        return coL();
    }

    public boolean bee() {
        switch (bFf().mo6893i(gkL)) {
            case 0:
                return false;
            case 2:
                return ((Boolean) bFf().mo5606d(new aCE(this, gkL, new Object[0]))).booleanValue();
            case 3:
                bFf().mo5606d(new aCE(this, gkL, new Object[0]));
                break;
        }
        return coU();
    }

    public TextureGrid bef() {
        switch (bFf().mo6893i(gkC)) {
            case 0:
                return null;
            case 2:
                return (TextureGrid) bFf().mo5606d(new aCE(this, gkC, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, gkC, new Object[0]));
                break;
        }
        return coP();
    }

    public TextureGrid beg() {
        switch (bFf().mo6893i(gkE)) {
            case 0:
                return null;
            case 2:
                return (TextureGrid) bFf().mo5606d(new aCE(this, gkE, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, gkE, new Object[0]));
                break;
        }
        return coQ();
    }

    public TextureGrid beh() {
        switch (bFf().mo6893i(gkG)) {
            case 0:
                return null;
            case 2:
                return (TextureGrid) bFf().mo5606d(new aCE(this, gkG, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, gkG, new Object[0]));
                break;
        }
        return coR();
    }

    @C5566aOg
    @C2499fr
    /* renamed from: bx */
    public void mo15258bx(Asset tCVar) {
        switch (bFf().mo6893i(gkJ)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, gkJ, new Object[]{tCVar}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, gkJ, new Object[]{tCVar}));
                break;
        }
        m24493bw(tCVar);
    }

    /* access modifiers changed from: protected */
    /* renamed from: c */
    public void mo8337c(C2961mJ mJVar) {
        switch (bFf().mo6893i(f5017x6a821b1)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f5017x6a821b1, new Object[]{mJVar}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f5017x6a821b1, new Object[]{mJVar}));
                break;
        }
        m24467b(mJVar);
    }

    /* renamed from: c */
    public C5663aRz[] mo25c() {
        return _m_fields;
    }

    /* renamed from: cJ */
    public Asset mo7725cJ() {
        switch (bFf().mo6893i(f5059gm)) {
            case 0:
                return null;
            case 2:
                return (Asset) bFf().mo5606d(new aCE(this, f5059gm, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f5059gm, new Object[0]));
                break;
        }
        return m24496cI();
    }

    @C5566aOg
    @C2499fr
    /* renamed from: g */
    public void mo15259g(TextureScheme ark) {
        switch (bFf().mo6893i(gku)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, gku, new Object[]{ark}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, gku, new Object[]{ark}));
                break;
        }
        m24501f(ark);
    }

    public float getHeight() {
        switch (bFf().mo6893i(cSO)) {
            case 0:
                return 0.0f;
            case 2:
                return ((Float) bFf().mo5606d(new aCE(this, cSO, new Object[0]))).floatValue();
            case 3:
                bFf().mo5606d(new aCE(this, cSO, new Object[0]));
                break;
        }
        return aOB();
    }

    public String getPrefix() {
        switch (bFf().mo6893i(f5058ff)) {
            case 0:
                return null;
            case 2:
                return (String) bFf().mo5606d(new aCE(this, f5058ff, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f5058ff, new Object[0]));
                break;
        }
        return m24470bD();
    }

    public <T> T getType() {
        switch (bFf().mo6893i(_f_getType_0020_0028_0029Ljava_002flang_002fObject_003b)) {
            case 0:
                return null;
            case 2:
                return bFf().mo5606d(new aCE(this, _f_getType_0020_0028_0029Ljava_002flang_002fObject_003b, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, _f_getType_0020_0028_0029Ljava_002flang_002fObject_003b, new Object[0]));
                break;
        }
        return m24531qW();
    }

    @C5566aOg
    @C2499fr
    /* renamed from: j */
    public void mo15260j(ColorWrapper apf) {
        switch (bFf().mo6893i(gkA)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, gkA, new Object[]{apf}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, gkA, new Object[]{apf}));
                break;
        }
        m24509i(apf);
    }

    /* renamed from: ke */
    public I18NString mo15261ke() {
        switch (bFf().mo6893i(f5060zT)) {
            case 0:
                return null;
            case 2:
                return (I18NString) bFf().mo5606d(new aCE(this, f5060zT, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f5060zT, new Object[0]));
                break;
        }
        return m24516kd();
    }

    @C5566aOg
    @C2499fr
    /* renamed from: kh */
    public void mo15262kh(float f) {
        switch (bFf().mo6893i(gkw)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, gkw, new Object[]{new Float(f)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, gkw, new Object[]{new Float(f)}));
                break;
        }
        m24520kg(f);
    }

    @C5566aOg
    @C2499fr
    /* renamed from: kj */
    public void mo15263kj(float f) {
        switch (bFf().mo6893i(gkB)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, gkB, new Object[]{new Float(f)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, gkB, new Object[]{new Float(f)}));
                break;
        }
        m24521ki(f);
    }

    @C5566aOg
    @C2499fr
    /* renamed from: kl */
    public void mo15264kl(float f) {
        switch (bFf().mo6893i(gkP)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, gkP, new Object[]{new Float(f)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, gkP, new Object[]{new Float(f)}));
                break;
        }
        m24522kk(f);
    }

    /* renamed from: n */
    public BodyPiece mo7728n(AvatarSector nw) {
        switch (bFf().mo6893i(gkq)) {
            case 0:
                return null;
            case 2:
                return (BodyPiece) bFf().mo5606d(new aCE(this, gkq, new Object[]{nw}));
            case 3:
                bFf().mo5606d(new aCE(this, gkq, new Object[]{nw}));
                break;
        }
        return m24534x(nw);
    }

    @C5566aOg
    @C2499fr
    /* renamed from: n */
    public void mo15266n(TextureGrid uk) {
        switch (bFf().mo6893i(gky)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, gky, new Object[]{uk}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, gky, new Object[]{uk}));
                break;
        }
        m24525m(uk);
    }

    @C5566aOg
    @C2499fr
    /* renamed from: p */
    public void mo15267p(TextureGrid uk) {
        switch (bFf().mo6893i(gkD)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, gkD, new Object[]{uk}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, gkD, new Object[]{uk}));
                break;
        }
        m24527o(uk);
    }

    /* renamed from: qV */
    public void mo15268qV() {
        switch (bFf().mo6893i(f5016Lm)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f5016Lm, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f5016Lm, new Object[0]));
                break;
        }
        m24530qU();
    }

    @C5566aOg
    @C2499fr
    /* renamed from: r */
    public void mo15269r(TextureGrid uk) {
        switch (bFf().mo6893i(gkF)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, gkF, new Object[]{uk}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, gkF, new Object[]{uk}));
                break;
        }
        m24529q(uk);
    }

    @C5566aOg
    @C2499fr
    /* renamed from: t */
    public void mo15270t(TextureGrid uk) {
        switch (bFf().mo6893i(gkH)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, gkH, new Object[]{uk}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, gkH, new Object[]{uk}));
                break;
        }
        m24533s(uk);
    }

    /* renamed from: n */
    public void mo15265n(AvatarAppearanceType t) {
        super.mo967a((C2961mJ) t);
        m24528p(t);
    }

    @C0064Am(aul = "4d26878dccaf66a276f8a441d324d03a", aum = 0)
    /* renamed from: b */
    private void m24467b(C2961mJ mJVar) {
        if (mJVar != getType() && (mJVar instanceof AvatarAppearanceType)) {
            super.mo8337c(mJVar);
            m24528p((AvatarAppearanceType) mJVar);
        }
    }

    @C0064Am(aul = "058c37822b0db3ef7acef7b35d193add", aum = 0)
    /* renamed from: x */
    private BodyPiece m24534x(AvatarSector nw) {
        return (BodyPiece) coz().get(nw);
    }

    @C0064Am(aul = "a5c244d0f309c6c99434968a5cbe28fd", aum = 0)
    private Collection<BodyPiece> coK() {
        return coz().values();
    }

    @C0064Am(aul = "5c1a9579fb93ca231146de44740f37b3", aum = 0)
    private TextureScheme coL() {
        return coA();
    }

    @C0064Am(aul = "9f62dd28a633a58f63bfb771b60df46f", aum = 0)
    private float coM() {
        return coI();
    }

    @C0064Am(aul = "f9d25d4a255569394972616fd7166258", aum = 0)
    private TextureGrid coN() {
        return coB();
    }

    @C0064Am(aul = "ed90f0e6be6bdc41c09f02e6d60a1bb2", aum = 0)
    private ColorWrapper coO() {
        return coC();
    }

    @C0064Am(aul = "dbad2f64c85b18d0830962e90834c081", aum = 0)
    @ClientOnly
    private Rectangle2D.Float aGj() {
        return ala().aLa().aGk();
    }

    @C0064Am(aul = "b5d071aaffd1a4677405ce56dae66a4e", aum = 0)
    private float aOC() {
        return coH();
    }

    @C0064Am(aul = "6db0da67f4145feca8acc9f582b9a5b5", aum = 0)
    private TextureGrid coP() {
        return coE();
    }

    @C0064Am(aul = "ce5672936b8a8afa65cef467614174f4", aum = 0)
    private TextureGrid coQ() {
        return coF();
    }

    @C0064Am(aul = "8168cd790c762048a1209a317294f0c2", aum = 0)
    private TextureGrid coR() {
        return coG();
    }

    @C0064Am(aul = "2047e4d5efce23b984744da9485884e6", aum = 0)
    private Asset coS() {
        return coD();
    }

    @C0064Am(aul = "2a5d14b345e225671f0e35b91b20f359", aum = 0)
    @ClientOnly
    private AvatarSector coT() {
        return ala().aLa().aFM();
    }

    @C0064Am(aul = "098254157edd989a5198988820b392c9", aum = 0)
    @ClientOnly
    private AvatarSector aFN() {
        return ala().aLa().aFO();
    }

    @C0064Am(aul = "f6d148bf5945a1dc30569eb3ad6c172c", aum = 0)
    @ClientOnly
    private AvatarSector aFP() {
        return ala().aLa().aFQ();
    }

    @C0064Am(aul = "b359e709f676082800de88aced46f93e", aum = 0)
    @ClientOnly
    private Collection<AvatarSector> aFH() {
        return ala().aLa().aFI();
    }

    @C0064Am(aul = "cd48552cd3f989161a69d225807c8407", aum = 0)
    private boolean coU() {
        return ((AvatarAppearanceType) getType()).mo5531bC();
    }

    @C0064Am(aul = "c721a146b3079e5646ebb067253ec07c", aum = 0)
    private Asset coV() {
        return ((AvatarAppearanceType) getType()).mo5533bH();
    }

    @C0064Am(aul = "4c82c5e3943014a30dd3c9c0c58affce", aum = 0)
    private Asset coW() {
        return ((AvatarAppearanceType) getType()).mo5532bF();
    }

    @C0064Am(aul = "36f0122f39ec2dfd82feb77569361312", aum = 0)
    /* renamed from: kd */
    private I18NString m24516kd() {
        return ((AvatarAppearanceType) getType()).mo5543by();
    }

    @C0064Am(aul = "33565b0b47b6f478edad0e40616b1722", aum = 0)
    private float coX() {
        return coJ();
    }

    @C0064Am(aul = "d5e25794a341814ff1c32b2f81f45b0f", aum = 0)
    /* renamed from: bD */
    private String m24470bD() {
        return ((AvatarAppearanceType) getType()).getPrefix();
    }

    @C0064Am(aul = "7231351ec03c58b60c35fac7aedde7dc", aum = 0)
    private Asset coY() {
        return ((AvatarAppearanceType) getType()).mo5534bJ();
    }

    @C0064Am(aul = "c2a6a3227b39f9842eee9a650df84b62", aum = 0)
    /* renamed from: cI */
    private Asset m24496cI() {
        return ((AvatarAppearanceType) getType()).mo5549cJ();
    }

    @C0064Am(aul = "57ebf99f6fcc46f6956ca511f31d7331", aum = 0)
    private float aOB() {
        return ((AvatarAppearanceType) getType()).mo5548cH() * aOD();
    }

    @C0064Am(aul = "7077ad82a3e052fbdd03931612fb1086", aum = 0)
    /* renamed from: qU */
    private void m24530qU() {
        push();
        bdW().push();
        bed().push();
        beb().push();
        if (bdZ() != null) {
            bdZ().push();
        }
        bdX().push();
        bef().push();
        if (beh() != null) {
            beh().push();
        }
        if (beg() != null) {
            beg().push();
        }
        for (BodyPiece next : bdU()) {
            next.push();
            next.bpk().push();
        }
    }

    @C0064Am(aul = "64991a9fad27fefe17675c801f9ec956", aum = 0)
    /* renamed from: qW */
    private <T> T m24531qW() {
        return super.getType();
    }
}
