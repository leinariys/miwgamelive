package game.script.avatar;

import game.network.message.externalizable.aCE;
import game.network.message.serializable.C2686iZ;
import game.network.message.serializable.C3438ra;
import game.script.avatar.body.AvatarAppearanceType;
import game.script.avatar.body.BodyPieceBank;
import game.script.avatar.body.TextureScheme;
import game.script.avatar.cloth.Cloth;
import game.script.avatar.cloth.ClothCategory;
import logic.baa.*;
import logic.data.mbean.C3439rb;
import logic.data.mbean.C3687tm;
import logic.res.code.C5640aRc;
import logic.res.code.C5663aRz;
import logic.res.code.C6494any;
import logic.res.html.C2491fm;
import p001a.*;

import java.awt.geom.Rectangle2D;
import java.util.*;

@C5511aMd
@C6485anp
/* renamed from: a.Dq */
/* compiled from: a */
public class AvatarManager extends aDJ implements C0468GU, C1616Xf {
    public static final int _m_fieldCount = 0;
    public static final C5663aRz[] _m_fields = null;
    public static final int _m_methodCount = 0;
    public static final C2491fm[] _m_methods = null;
    /* renamed from: bL */
    public static final C5663aRz f440bL = null;
    /* renamed from: bM */
    public static final C5663aRz f441bM = null;
    /* renamed from: bN */
    public static final C2491fm f442bN = null;
    /* renamed from: bO */
    public static final C2491fm f443bO = null;
    /* renamed from: bP */
    public static final C2491fm f444bP = null;
    /* renamed from: bQ */
    public static final C2491fm f445bQ = null;
    public static final C5663aRz cFG = null;
    public static final C5663aRz cFH = null;
    public static final C5663aRz cFI = null;
    public static final C5663aRz cFJ = null;
    public static final C5663aRz cFK = null;
    public static final C5663aRz cFL = null;
    public static final C5663aRz cFM = null;
    public static final C5663aRz cFN = null;
    public static final C5663aRz cFO = null;
    public static final C5663aRz cFP = null;
    public static final C5663aRz cFQ = null;
    public static final C5663aRz cFR = null;
    public static final C5663aRz cFS = null;
    public static final C5663aRz cFT = null;
    public static final C5663aRz cFU = null;
    public static final C5663aRz cFV = null;
    public static final C5663aRz cFW = null;
    public static final C5663aRz cFX = null;
    public static final C2491fm cFZ = null;
    public static final C2491fm cGA = null;
    public static final C2491fm cGB = null;
    public static final C2491fm cGC = null;
    public static final C2491fm cGD = null;
    public static final C2491fm cGE = null;
    public static final C2491fm cGF = null;
    public static final C2491fm cGG = null;
    public static final C2491fm cGH = null;
    public static final C2491fm cGI = null;
    public static final C2491fm cGJ = null;
    public static final C2491fm cGK = null;
    public static final C2491fm cGL = null;
    public static final C2491fm cGM = null;
    public static final C2491fm cGN = null;
    public static final C2491fm cGO = null;
    public static final C2491fm cGP = null;
    public static final C2491fm cGa = null;
    public static final C2491fm cGb = null;
    public static final C2491fm cGc = null;
    public static final C2491fm cGd = null;
    public static final C2491fm cGe = null;
    public static final C2491fm cGf = null;
    public static final C2491fm cGg = null;
    public static final C2491fm cGh = null;
    public static final C2491fm cGi = null;
    public static final C2491fm cGj = null;
    public static final C2491fm cGk = null;
    public static final C2491fm cGl = null;
    public static final C2491fm cGm = null;
    public static final C2491fm cGn = null;
    public static final C2491fm cGo = null;
    public static final C2491fm cGp = null;
    public static final C2491fm cGq = null;
    public static final C2491fm cGr = null;
    public static final C2491fm cGs = null;
    public static final C2491fm cGt = null;
    public static final C2491fm cGu = null;
    public static final C2491fm cGv = null;
    public static final C2491fm cGw = null;
    public static final C2491fm cGx = null;
    public static final C2491fm cGy = null;
    public static final C2491fm cGz = null;
    public static final long serialVersionUID = 0;
    public static C6494any ___iScriptClass;
    @C0064Am(aul = "ecb1d41dac8f44be92714d30b0d0c3c5", aum = 18)

    /* renamed from: bK */
    private static UUID f439bK;
    @C0064Am(aul = "b5f460b5c29d59828d0a4115e1a8c1f5", aum = 0)
    private static C2686iZ<Cloth> bla;
    @C0064Am(aul = "9482426c1ce216dd024e7d9216af4977", aum = 1)
    private static C2686iZ<ClothCategory> blb;
    @C0064Am(aul = "3d2fc0d1abde58a8d4a5655a8ee8b5fc", aum = 2)
    private static ClothCategory blc;
    @C0064Am(aul = "6bc0af67f21dbc8c4ac14ae1609fe9bc", aum = 3)
    private static ClothCategory bld;
    @C0064Am(aul = "99e7352cb88cb1fb0682a48f840b9206", aum = 4)
    private static C3438ra<AvatarAppearanceType> ble;
    @C0064Am(aul = "b2fca0cb00158038a44772577a17336c", aum = 5)
    private static C2686iZ<AvatarSector> blf;
    @C0064Am(aul = "ca6f29f9a9a2b48f5b4f43ab6c34b300", aum = 6)
    private static C2686iZ<BodyPieceBank> blg;
    @C0064Am(aul = "5b3cfb08f8f60989a038628b799f7087", aum = 7)
    private static AvatarSector blh;
    @C0064Am(aul = "c5800d4f750c0113596a45b64dc62b2f", aum = 8)
    private static AvatarSector bli;
    @C0064Am(aul = "42a18519718617f3343ee6f269a7d2a1", aum = 9)
    private static AvatarSector blj;
    @C0064Am(aul = "21ec2ccd755cf4eef5ef1954d9767d85", aum = 10)
    private static String blk;
    @C0064Am(aul = "3a50bf464731c82afc464726131d1ec8", aum = 11)
    private static String bll;
    @C0064Am(aul = "adc04dfd846ec9ef260682fac4bd4e71", aum = 12)
    private static Cloth blm;
    @C0064Am(aul = "7f7e36998d501aab57f26594a0db4c61", aum = 13)
    private static ColorManager bln;
    @C0064Am(aul = "ea683526cd0249dec7eb49a22a63722f", aum = 14)
    private static float blo;
    @C0064Am(aul = "e0be4d2af9d0f3cd2c7767163601d9bb", aum = 15)
    private static float blp;
    @C0064Am(aul = "3a3ab552637e7abc7dfda36bb396c042", aum = 16)
    private static float blq;
    @C0064Am(aul = "ef29e0f9f75a8a3c577d0efacbf2236d", aum = 17)
    private static float blr;
    @C0064Am(aul = "5292b9c9a80ae92b26ec15933ad6fda6", aum = 19)
    private static String handle;

    static {
        m2589V();
    }

    @ClientOnly
    private transient Rectangle2D.Float cFY;

    public AvatarManager() {
        super((C5540aNg) null);
        super.mo10S();
    }

    public AvatarManager(C5540aNg ang) {
        super(ang);
    }

    /* renamed from: V */
    static void m2589V() {
        serialVersionUID = (long) 1;
        _m_fieldCount = aDJ._m_fieldCount + 20;
        _m_methodCount = aDJ._m_methodCount + 47;
        int i = aDJ._m_fieldCount;
        C5663aRz[] arzArr = new C5663aRz[(i + 20)];
        C5663aRz b = C5640aRc.m17844b(AvatarManager.class, "b5f460b5c29d59828d0a4115e1a8c1f5", i);
        cFG = b;
        arzArr[i] = b;
        int i2 = i + 1;
        C5663aRz b2 = C5640aRc.m17844b(AvatarManager.class, "9482426c1ce216dd024e7d9216af4977", i2);
        cFH = b2;
        arzArr[i2] = b2;
        int i3 = i2 + 1;
        C5663aRz b3 = C5640aRc.m17844b(AvatarManager.class, "3d2fc0d1abde58a8d4a5655a8ee8b5fc", i3);
        cFI = b3;
        arzArr[i3] = b3;
        int i4 = i3 + 1;
        C5663aRz b4 = C5640aRc.m17844b(AvatarManager.class, "6bc0af67f21dbc8c4ac14ae1609fe9bc", i4);
        cFJ = b4;
        arzArr[i4] = b4;
        int i5 = i4 + 1;
        C5663aRz b5 = C5640aRc.m17844b(AvatarManager.class, "99e7352cb88cb1fb0682a48f840b9206", i5);
        cFK = b5;
        arzArr[i5] = b5;
        int i6 = i5 + 1;
        C5663aRz b6 = C5640aRc.m17844b(AvatarManager.class, "b2fca0cb00158038a44772577a17336c", i6);
        cFL = b6;
        arzArr[i6] = b6;
        int i7 = i6 + 1;
        C5663aRz b7 = C5640aRc.m17844b(AvatarManager.class, "ca6f29f9a9a2b48f5b4f43ab6c34b300", i7);
        cFM = b7;
        arzArr[i7] = b7;
        int i8 = i7 + 1;
        C5663aRz b8 = C5640aRc.m17844b(AvatarManager.class, "5b3cfb08f8f60989a038628b799f7087", i8);
        cFN = b8;
        arzArr[i8] = b8;
        int i9 = i8 + 1;
        C5663aRz b9 = C5640aRc.m17844b(AvatarManager.class, "c5800d4f750c0113596a45b64dc62b2f", i9);
        cFO = b9;
        arzArr[i9] = b9;
        int i10 = i9 + 1;
        C5663aRz b10 = C5640aRc.m17844b(AvatarManager.class, "42a18519718617f3343ee6f269a7d2a1", i10);
        cFP = b10;
        arzArr[i10] = b10;
        int i11 = i10 + 1;
        C5663aRz b11 = C5640aRc.m17844b(AvatarManager.class, "21ec2ccd755cf4eef5ef1954d9767d85", i11);
        cFQ = b11;
        arzArr[i11] = b11;
        int i12 = i11 + 1;
        C5663aRz b12 = C5640aRc.m17844b(AvatarManager.class, "3a50bf464731c82afc464726131d1ec8", i12);
        cFR = b12;
        arzArr[i12] = b12;
        int i13 = i12 + 1;
        C5663aRz b13 = C5640aRc.m17844b(AvatarManager.class, "adc04dfd846ec9ef260682fac4bd4e71", i13);
        cFS = b13;
        arzArr[i13] = b13;
        int i14 = i13 + 1;
        C5663aRz b14 = C5640aRc.m17844b(AvatarManager.class, "7f7e36998d501aab57f26594a0db4c61", i14);
        cFT = b14;
        arzArr[i14] = b14;
        int i15 = i14 + 1;
        C5663aRz b15 = C5640aRc.m17844b(AvatarManager.class, "ea683526cd0249dec7eb49a22a63722f", i15);
        cFU = b15;
        arzArr[i15] = b15;
        int i16 = i15 + 1;
        C5663aRz b16 = C5640aRc.m17844b(AvatarManager.class, "e0be4d2af9d0f3cd2c7767163601d9bb", i16);
        cFV = b16;
        arzArr[i16] = b16;
        int i17 = i16 + 1;
        C5663aRz b17 = C5640aRc.m17844b(AvatarManager.class, "3a3ab552637e7abc7dfda36bb396c042", i17);
        cFW = b17;
        arzArr[i17] = b17;
        int i18 = i17 + 1;
        C5663aRz b18 = C5640aRc.m17844b(AvatarManager.class, "ef29e0f9f75a8a3c577d0efacbf2236d", i18);
        cFX = b18;
        arzArr[i18] = b18;
        int i19 = i18 + 1;
        C5663aRz b19 = C5640aRc.m17844b(AvatarManager.class, "ecb1d41dac8f44be92714d30b0d0c3c5", i19);
        f440bL = b19;
        arzArr[i19] = b19;
        int i20 = i19 + 1;
        C5663aRz b20 = C5640aRc.m17844b(AvatarManager.class, "5292b9c9a80ae92b26ec15933ad6fda6", i20);
        f441bM = b20;
        arzArr[i20] = b20;
        int i21 = i20 + 1;
        _m_fields = arzArr;
        C1634Xv.m11725a((Object[]) aDJ._m_fields, (Object[]) _m_fields);
        int i22 = aDJ._m_methodCount;
        C2491fm[] fmVarArr = new C2491fm[(i22 + 47)];
        C2491fm a = C4105zY.m41624a(AvatarManager.class, "ead4cee567badd55eebe8aa9c0162fb7", i22);
        f442bN = a;
        fmVarArr[i22] = a;
        int i23 = i22 + 1;
        C2491fm a2 = C4105zY.m41624a(AvatarManager.class, "bbd66bb78aab695984400cc4066267c1", i23);
        f443bO = a2;
        fmVarArr[i23] = a2;
        int i24 = i23 + 1;
        C2491fm a3 = C4105zY.m41624a(AvatarManager.class, "cae26e9f6a0651ca7796b0f1b7c56992", i24);
        cFZ = a3;
        fmVarArr[i24] = a3;
        int i25 = i24 + 1;
        C2491fm a4 = C4105zY.m41624a(AvatarManager.class, "f39467392dadc81dd8a9032be1f4efe6", i25);
        cGa = a4;
        fmVarArr[i25] = a4;
        int i26 = i25 + 1;
        C2491fm a5 = C4105zY.m41624a(AvatarManager.class, "e4f7785316d4bf936422235ee02aad3c", i26);
        cGb = a5;
        fmVarArr[i26] = a5;
        int i27 = i26 + 1;
        C2491fm a6 = C4105zY.m41624a(AvatarManager.class, "135575fd783d98e9d42f4bbc37795396", i27);
        cGc = a6;
        fmVarArr[i27] = a6;
        int i28 = i27 + 1;
        C2491fm a7 = C4105zY.m41624a(AvatarManager.class, "58ade3fe0cdadc85e4d14d7a5788cd75", i28);
        cGd = a7;
        fmVarArr[i28] = a7;
        int i29 = i28 + 1;
        C2491fm a8 = C4105zY.m41624a(AvatarManager.class, "97d53a1550f72f959696ede6ac2305f2", i29);
        cGe = a8;
        fmVarArr[i29] = a8;
        int i30 = i29 + 1;
        C2491fm a9 = C4105zY.m41624a(AvatarManager.class, "60356584ed9fdf273f58492fd52aefaf", i30);
        cGf = a9;
        fmVarArr[i30] = a9;
        int i31 = i30 + 1;
        C2491fm a10 = C4105zY.m41624a(AvatarManager.class, "73097e869ac644cca77625f474c46037", i31);
        cGg = a10;
        fmVarArr[i31] = a10;
        int i32 = i31 + 1;
        C2491fm a11 = C4105zY.m41624a(AvatarManager.class, "91f9f66a0c6b1ec2273f5de52c1c7422", i32);
        cGh = a11;
        fmVarArr[i32] = a11;
        int i33 = i32 + 1;
        C2491fm a12 = C4105zY.m41624a(AvatarManager.class, "15cdc4d66cd55e22c07e602843ced8f2", i33);
        cGi = a12;
        fmVarArr[i33] = a12;
        int i34 = i33 + 1;
        C2491fm a13 = C4105zY.m41624a(AvatarManager.class, "296c51300d24e844d54dce90c677cfab", i34);
        cGj = a13;
        fmVarArr[i34] = a13;
        int i35 = i34 + 1;
        C2491fm a14 = C4105zY.m41624a(AvatarManager.class, "27864f8ad328393cd6b381d49acd2630", i35);
        cGk = a14;
        fmVarArr[i35] = a14;
        int i36 = i35 + 1;
        C2491fm a15 = C4105zY.m41624a(AvatarManager.class, "48c26db56206a7d3d62bc286e7c83405", i36);
        cGl = a15;
        fmVarArr[i36] = a15;
        int i37 = i36 + 1;
        C2491fm a16 = C4105zY.m41624a(AvatarManager.class, "5fd6680d9cc34e6b09d181be91aff14d", i37);
        cGm = a16;
        fmVarArr[i37] = a16;
        int i38 = i37 + 1;
        C2491fm a17 = C4105zY.m41624a(AvatarManager.class, "f9a76e3dd4f69900724c75ea7db0e134", i38);
        cGn = a17;
        fmVarArr[i38] = a17;
        int i39 = i38 + 1;
        C2491fm a18 = C4105zY.m41624a(AvatarManager.class, "c4d67a1571fc94866f124749092389ff", i39);
        cGo = a18;
        fmVarArr[i39] = a18;
        int i40 = i39 + 1;
        C2491fm a19 = C4105zY.m41624a(AvatarManager.class, "f72ecce206d68c5112491daf75692865", i40);
        cGp = a19;
        fmVarArr[i40] = a19;
        int i41 = i40 + 1;
        C2491fm a20 = C4105zY.m41624a(AvatarManager.class, "b121a300f8fe2447427455ae7f6aa6f3", i41);
        cGq = a20;
        fmVarArr[i41] = a20;
        int i42 = i41 + 1;
        C2491fm a21 = C4105zY.m41624a(AvatarManager.class, "6f6150ed89261f9fb409043ae1c4e700", i42);
        cGr = a21;
        fmVarArr[i42] = a21;
        int i43 = i42 + 1;
        C2491fm a22 = C4105zY.m41624a(AvatarManager.class, "9e47c4ae519afaadc8f899279ced9e56", i43);
        cGs = a22;
        fmVarArr[i43] = a22;
        int i44 = i43 + 1;
        C2491fm a23 = C4105zY.m41624a(AvatarManager.class, "4c0146dbe0bce07df8eed10286c163d6", i44);
        cGt = a23;
        fmVarArr[i44] = a23;
        int i45 = i44 + 1;
        C2491fm a24 = C4105zY.m41624a(AvatarManager.class, "184f2e1cd2e3f164601ad0255c3a4bff", i45);
        cGu = a24;
        fmVarArr[i45] = a24;
        int i46 = i45 + 1;
        C2491fm a25 = C4105zY.m41624a(AvatarManager.class, "1fc0884f00601ca1fb4489ea020bd8e3", i46);
        cGv = a25;
        fmVarArr[i46] = a25;
        int i47 = i46 + 1;
        C2491fm a26 = C4105zY.m41624a(AvatarManager.class, "7588fe837c5fcad4664b50935db803a0", i47);
        cGw = a26;
        fmVarArr[i47] = a26;
        int i48 = i47 + 1;
        C2491fm a27 = C4105zY.m41624a(AvatarManager.class, "3f79b426da892fd2d7a2c8909ce01a2b", i48);
        cGx = a27;
        fmVarArr[i48] = a27;
        int i49 = i48 + 1;
        C2491fm a28 = C4105zY.m41624a(AvatarManager.class, "38782d8bafce7015c40ff74be22ea219", i49);
        cGy = a28;
        fmVarArr[i49] = a28;
        int i50 = i49 + 1;
        C2491fm a29 = C4105zY.m41624a(AvatarManager.class, "c3d60068efc600b23e6811378327ad11", i50);
        cGz = a29;
        fmVarArr[i50] = a29;
        int i51 = i50 + 1;
        C2491fm a30 = C4105zY.m41624a(AvatarManager.class, "743b89bfa692fa71c00ba9ceffe7569b", i51);
        cGA = a30;
        fmVarArr[i51] = a30;
        int i52 = i51 + 1;
        C2491fm a31 = C4105zY.m41624a(AvatarManager.class, "3b8cd07a505a7283c2871505b0ade523", i52);
        cGB = a31;
        fmVarArr[i52] = a31;
        int i53 = i52 + 1;
        C2491fm a32 = C4105zY.m41624a(AvatarManager.class, "7a6bcdb49e3d9672ef847bcf6a931925", i53);
        cGC = a32;
        fmVarArr[i53] = a32;
        int i54 = i53 + 1;
        C2491fm a33 = C4105zY.m41624a(AvatarManager.class, "0dbf1bed7adf03d2a831b154aaa6f2a4", i54);
        cGD = a33;
        fmVarArr[i54] = a33;
        int i55 = i54 + 1;
        C2491fm a34 = C4105zY.m41624a(AvatarManager.class, "32e275d46b7930eca367a85a310e6e5f", i55);
        cGE = a34;
        fmVarArr[i55] = a34;
        int i56 = i55 + 1;
        C2491fm a35 = C4105zY.m41624a(AvatarManager.class, "b5fcf4bc48fa32eb2ee2414d31b3f822", i56);
        cGF = a35;
        fmVarArr[i56] = a35;
        int i57 = i56 + 1;
        C2491fm a36 = C4105zY.m41624a(AvatarManager.class, "5bae5b9ac660df12ba1d585f81e5f0d0", i57);
        cGG = a36;
        fmVarArr[i57] = a36;
        int i58 = i57 + 1;
        C2491fm a37 = C4105zY.m41624a(AvatarManager.class, "106abe1d3fbe9f182866b8f21d936835", i58);
        cGH = a37;
        fmVarArr[i58] = a37;
        int i59 = i58 + 1;
        C2491fm a38 = C4105zY.m41624a(AvatarManager.class, "74375cf5274bae2d87f96c175b1bcc52", i59);
        cGI = a38;
        fmVarArr[i59] = a38;
        int i60 = i59 + 1;
        C2491fm a39 = C4105zY.m41624a(AvatarManager.class, "db9eacf1e1934064c1b871aa47234b07", i60);
        cGJ = a39;
        fmVarArr[i60] = a39;
        int i61 = i60 + 1;
        C2491fm a40 = C4105zY.m41624a(AvatarManager.class, "28a5cd4f5dce945142a1021c5e802e57", i61);
        cGK = a40;
        fmVarArr[i61] = a40;
        int i62 = i61 + 1;
        C2491fm a41 = C4105zY.m41624a(AvatarManager.class, "cd380d0e4309ccbde9a50b84b18a3efc", i62);
        cGL = a41;
        fmVarArr[i62] = a41;
        int i63 = i62 + 1;
        C2491fm a42 = C4105zY.m41624a(AvatarManager.class, "3a8925bd2c178361f0008aec5a7e0740", i63);
        cGM = a42;
        fmVarArr[i63] = a42;
        int i64 = i63 + 1;
        C2491fm a43 = C4105zY.m41624a(AvatarManager.class, "c382df9908fb812028d78849ac0b29a3", i64);
        cGN = a43;
        fmVarArr[i64] = a43;
        int i65 = i64 + 1;
        C2491fm a44 = C4105zY.m41624a(AvatarManager.class, "5a49c877545f263b35507e222020995d", i65);
        cGO = a44;
        fmVarArr[i65] = a44;
        int i66 = i65 + 1;
        C2491fm a45 = C4105zY.m41624a(AvatarManager.class, "352421d8f0e2f08fd00c390588d39645", i66);
        cGP = a45;
        fmVarArr[i66] = a45;
        int i67 = i66 + 1;
        C2491fm a46 = C4105zY.m41624a(AvatarManager.class, "0709cc2d0ef56c5b68e193748d8b4d4d", i67);
        f445bQ = a46;
        fmVarArr[i67] = a46;
        int i68 = i67 + 1;
        C2491fm a47 = C4105zY.m41624a(AvatarManager.class, "ef6d53587b5874cec2da7d9a3140d6ad", i68);
        f444bP = a47;
        fmVarArr[i68] = a47;
        int i69 = i68 + 1;
        _m_methods = fmVarArr;
        C1634Xv.m11725a((Object[]) aDJ._m_methods, (Object[]) _m_methods);
        ___iScriptClass = aUO.m18566a(AvatarManager.class, C3687tm.class, _m_fields, _m_methods);
    }

    /* renamed from: a */
    private void m2590a(ColorManager aVar) {
        bFf().mo5608dq().mo3197f(cFT, aVar);
    }

    /* renamed from: a */
    private void m2591a(AvatarSector nw) {
        bFf().mo5608dq().mo3197f(cFN, nw);
    }

    /* renamed from: a */
    private void m2594a(String str) {
        bFf().mo5608dq().mo3197f(f441bM, str);
    }

    /* renamed from: a */
    private void m2595a(UUID uuid) {
        bFf().mo5608dq().mo3197f(f440bL, uuid);
    }

    @C5566aOg
    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Color Manager")
    @C0064Am(aul = "c4d67a1571fc94866f124749092389ff", aum = 0)
    @C2499fr
    private ColorManager aFJ() {
        throw new aWi(new aCE(this, cGo, new Object[0]));
    }

    private C2686iZ aFh() {
        return (C2686iZ) bFf().mo5608dq().mo3214p(cFG);
    }

    private C2686iZ aFi() {
        return (C2686iZ) bFf().mo5608dq().mo3214p(cFH);
    }

    private ClothCategory aFj() {
        return (ClothCategory) bFf().mo5608dq().mo3214p(cFI);
    }

    private ClothCategory aFk() {
        return (ClothCategory) bFf().mo5608dq().mo3214p(cFJ);
    }

    private C3438ra aFl() {
        return (C3438ra) bFf().mo5608dq().mo3214p(cFK);
    }

    private C2686iZ aFm() {
        return (C2686iZ) bFf().mo5608dq().mo3214p(cFL);
    }

    private C2686iZ aFn() {
        return (C2686iZ) bFf().mo5608dq().mo3214p(cFM);
    }

    private AvatarSector aFo() {
        return (AvatarSector) bFf().mo5608dq().mo3214p(cFN);
    }

    private AvatarSector aFp() {
        return (AvatarSector) bFf().mo5608dq().mo3214p(cFO);
    }

    private AvatarSector aFq() {
        return (AvatarSector) bFf().mo5608dq().mo3214p(cFP);
    }

    private String aFr() {
        return (String) bFf().mo5608dq().mo3214p(cFQ);
    }

    private String aFs() {
        return (String) bFf().mo5608dq().mo3214p(cFR);
    }

    private Cloth aFt() {
        return (Cloth) bFf().mo5608dq().mo3214p(cFS);
    }

    private ColorManager aFu() {
        return (ColorManager) bFf().mo5608dq().mo3214p(cFT);
    }

    private float aFv() {
        return bFf().mo5608dq().mo3211m(cFU);
    }

    private float aFw() {
        return bFf().mo5608dq().mo3211m(cFV);
    }

    private float aFx() {
        return bFf().mo5608dq().mo3211m(cFW);
    }

    private float aFy() {
        return bFf().mo5608dq().mo3211m(cFX);
    }

    /* renamed from: ah */
    private void m2596ah(C3438ra raVar) {
        bFf().mo5608dq().mo3197f(cFK, raVar);
    }

    /* renamed from: an */
    private UUID m2597an() {
        return (UUID) bFf().mo5608dq().mo3214p(f440bL);
    }

    /* renamed from: ao */
    private String m2598ao() {
        return (String) bFf().mo5608dq().mo3214p(f441bM);
    }

    /* renamed from: b */
    private void m2601b(AvatarSector nw) {
        bFf().mo5608dq().mo3197f(cFO, nw);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Handle (Consistency checker)")
    @C0064Am(aul = "0709cc2d0ef56c5b68e193748d8b4d4d", aum = 0)
    @C5566aOg
    /* renamed from: b */
    private void m2602b(String str) {
        throw new aWi(new aCE(this, f445bQ, new Object[]{str}));
    }

    /* renamed from: c */
    private void m2604c(AvatarSector nw) {
        bFf().mo5608dq().mo3197f(cFP, nw);
    }

    /* renamed from: c */
    private void m2607c(ClothCategory dHVar) {
        bFf().mo5608dq().mo3197f(cFI, dHVar);
    }

    /* renamed from: c */
    private void m2608c(UUID uuid) {
        switch (bFf().mo6893i(f443bO)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f443bO, new Object[]{uuid}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f443bO, new Object[]{uuid}));
                break;
        }
        m2603b(uuid);
    }

    /* renamed from: d */
    private void m2610d(Cloth wj) {
        bFf().mo5608dq().mo3197f(cFS, wj);
    }

    /* renamed from: d */
    private void m2611d(ClothCategory dHVar) {
        bFf().mo5608dq().mo3197f(cFJ, dHVar);
    }

    /* renamed from: dV */
    private void m2612dV(String str) {
        bFf().mo5608dq().mo3197f(cFQ, str);
    }

    /* renamed from: dW */
    private void m2613dW(String str) {
        bFf().mo5608dq().mo3197f(cFR, str);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Resource Path (Mesh)")
    @C0064Am(aul = "743b89bfa692fa71c00ba9ceffe7569b", aum = 0)
    @C5566aOg
    /* renamed from: dX */
    private void m2614dX(String str) {
        throw new aWi(new aCE(this, cGA, new Object[]{str}));
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Resource Path (Texture)")
    @C0064Am(aul = "7a6bcdb49e3d9672ef847bcf6a931925", aum = 0)
    @C5566aOg
    /* renamed from: dZ */
    private void m2615dZ(String str) {
        throw new aWi(new aCE(this, cGC, new Object[]{str}));
    }

    /* renamed from: eI */
    private void m2618eI(float f) {
        bFf().mo5608dq().mo3150a(cFU, f);
    }

    /* renamed from: eJ */
    private void m2619eJ(float f) {
        bFf().mo5608dq().mo3150a(cFV, f);
    }

    /* renamed from: eK */
    private void m2620eK(float f) {
        bFf().mo5608dq().mo3150a(cFW, f);
    }

    /* renamed from: eL */
    private void m2621eL(float f) {
        bFf().mo5608dq().mo3150a(cFX, f);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Features Area (Pos X)")
    @C0064Am(aul = "5bae5b9ac660df12ba1d585f81e5f0d0", aum = 0)
    @C5566aOg
    /* renamed from: eM */
    private void m2622eM(float f) {
        throw new aWi(new aCE(this, cGG, new Object[]{new Float(f)}));
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Features Area (Pos Y)")
    @C0064Am(aul = "74375cf5274bae2d87f96c175b1bcc52", aum = 0)
    @C5566aOg
    /* renamed from: eO */
    private void m2623eO(float f) {
        throw new aWi(new aCE(this, cGI, new Object[]{new Float(f)}));
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Features Area (width)")
    @C0064Am(aul = "28a5cd4f5dce945142a1021c5e802e57", aum = 0)
    @C5566aOg
    /* renamed from: eQ */
    private void m2624eQ(float f) {
        throw new aWi(new aCE(this, cGK, new Object[]{new Float(f)}));
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Features Area (height)")
    @C0064Am(aul = "3a8925bd2c178361f0008aec5a7e0740", aum = 0)
    @C5566aOg
    /* renamed from: eS */
    private void m2625eS(float f) {
        throw new aWi(new aCE(this, cGM, new Object[]{new Float(f)}));
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Avatar Ocular Globes")
    @C0064Am(aul = "b121a300f8fe2447427455ae7f6aa6f3", aum = 0)
    @C5566aOg
    /* renamed from: h */
    private void m2629h(AvatarSector nw) {
        throw new aWi(new aCE(this, cGq, new Object[]{nw}));
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Worm suit")
    @C0064Am(aul = "32e275d46b7930eca367a85a310e6e5f", aum = 0)
    @C5566aOg
    /* renamed from: i */
    private void m2630i(Cloth wj) {
        throw new aWi(new aCE(this, cGE, new Object[]{wj}));
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Overtorso Category")
    @C0064Am(aul = "7588fe837c5fcad4664b50935db803a0", aum = 0)
    @C5566aOg
    /* renamed from: i */
    private void m2631i(ClothCategory dHVar) {
        throw new aWi(new aCE(this, cGw, new Object[]{dHVar}));
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Avatar Hair Sector")
    @C0064Am(aul = "9e47c4ae519afaadc8f899279ced9e56", aum = 0)
    @C5566aOg
    /* renamed from: j */
    private void m2632j(AvatarSector nw) {
        throw new aWi(new aCE(this, cGs, new Object[]{nw}));
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Second Skin Category")
    @C0064Am(aul = "38782d8bafce7015c40ff74be22ea219", aum = 0)
    @C5566aOg
    /* renamed from: k */
    private void m2633k(ClothCategory dHVar) {
        throw new aWi(new aCE(this, cGy, new Object[]{dHVar}));
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Avatar Facial Hair Sector")
    @C0064Am(aul = "184f2e1cd2e3f164601ad0255c3a4bff", aum = 0)
    @C5566aOg
    /* renamed from: l */
    private void m2635l(AvatarSector nw) {
        throw new aWi(new aCE(this, cGu, new Object[]{nw}));
    }

    /* renamed from: t */
    private void m2637t(C2686iZ iZVar) {
        bFf().mo5608dq().mo3197f(cFG, iZVar);
    }

    /* renamed from: u */
    private void m2638u(C2686iZ iZVar) {
        bFf().mo5608dq().mo3197f(cFH, iZVar);
    }

    /* renamed from: v */
    private void m2639v(C2686iZ iZVar) {
        bFf().mo5608dq().mo3197f(cFL, iZVar);
    }

    /* renamed from: w */
    private void m2640w(C2686iZ iZVar) {
        bFf().mo5608dq().mo3197f(cFM, iZVar);
    }

    /* renamed from: T */
    public C6494any mo11T() {
        return ___iScriptClass;
    }

    /* renamed from: U */
    public C2491fm[] mo12U() {
        return _m_methods;
    }

    /* renamed from: W */
    public Object mo13W() {
        return new C3687tm(this);
    }

    /* renamed from: a */
    public Object mo14a(C0495Gr gr) {
        Object[] args = gr.getArgs();
        switch (gr.mo2417hq() - aDJ._m_methodCount) {
            case 0:
                return m2599ap();
            case 1:
                m2603b((UUID) args[0]);
                return null;
            case 2:
                m2616e((Cloth) args[0]);
                return null;
            case 3:
                m2627g((Cloth) args[0]);
                return null;
            case 4:
                return aFz();
            case 5:
                m2617e((ClothCategory) args[0]);
                return null;
            case 6:
                m2628g((ClothCategory) args[0]);
                return null;
            case 7:
                return aFB();
            case 8:
                m2593a((BodyPieceBank) args[0]);
                return null;
            case 9:
                m2606c((BodyPieceBank) args[0]);
                return null;
            case 10:
                return aFD();
            case 11:
                m2592a((AvatarAppearanceType) args[0]);
                return null;
            case 12:
                m2605c((AvatarAppearanceType) args[0]);
                return null;
            case 13:
                return aFF();
            case 14:
                m2609d((AvatarSector) args[0]);
                return null;
            case 15:
                m2626f((AvatarSector) args[0]);
                return null;
            case 16:
                return aFH();
            case 17:
                return aFJ();
            case 18:
                return aFL();
            case 19:
                m2629h((AvatarSector) args[0]);
                return null;
            case 20:
                return aFN();
            case 21:
                m2632j((AvatarSector) args[0]);
                return null;
            case 22:
                return aFP();
            case 23:
                m2635l((AvatarSector) args[0]);
                return null;
            case 24:
                return aFR();
            case 25:
                m2631i((ClothCategory) args[0]);
                return null;
            case 26:
                return aFT();
            case 27:
                m2633k((ClothCategory) args[0]);
                return null;
            case 28:
                return aFV();
            case 29:
                m2614dX((String) args[0]);
                return null;
            case 30:
                return aFX();
            case 31:
                m2615dZ((String) args[0]);
                return null;
            case 32:
                return aFZ();
            case 33:
                m2630i((Cloth) args[0]);
                return null;
            case 34:
                return new Float(aGb());
            case 35:
                m2622eM(((Float) args[0]).floatValue());
                return null;
            case 36:
                return new Float(aGd());
            case 37:
                m2623eO(((Float) args[0]).floatValue());
                return null;
            case 38:
                return new Float(aGf());
            case 39:
                m2624eQ(((Float) args[0]).floatValue());
                return null;
            case 40:
                return new Float(aGh());
            case 41:
                m2625eS(((Float) args[0]).floatValue());
                return null;
            case 42:
                return new Boolean(m2634k((Cloth) args[0]));
            case 43:
                return new Boolean(m2636m((Cloth) args[0]));
            case 44:
                return aGj();
            case 45:
                m2602b((String) args[0]);
                return null;
            case 46:
                return m2600ar();
            default:
                return super.mo14a(gr);
        }
    }

    /* renamed from: a */
    public void mo15a(Collection collection, C0495Gr gr) {
        super.mo15a(collection, gr);
    }

    @C3248pc(aYR = C3248pc.C3250b.ITERATOR, displayName = "Cloth list")
    public Set<Cloth> aFA() {
        switch (bFf().mo6893i(cGb)) {
            case 0:
                return null;
            case 2:
                return (Set) bFf().mo5606d(new aCE(this, cGb, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, cGb, new Object[0]));
                break;
        }
        return aFz();
    }

    @C3248pc(aYR = C3248pc.C3250b.ITERATOR, displayName = "Cloth Categories")
    public Set<ClothCategory> aFC() {
        switch (bFf().mo6893i(cGe)) {
            case 0:
                return null;
            case 2:
                return (Set) bFf().mo5606d(new aCE(this, cGe, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, cGe, new Object[0]));
                break;
        }
        return aFB();
    }

    @C3248pc(aYR = C3248pc.C3250b.ITERATOR, displayName = "Bodypiece Banks")
    public Set<BodyPieceBank> aFE() {
        switch (bFf().mo6893i(cGh)) {
            case 0:
                return null;
            case 2:
                return (Set) bFf().mo5606d(new aCE(this, cGh, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, cGh, new Object[0]));
                break;
        }
        return aFD();
    }

    @C3248pc(aYR = C3248pc.C3250b.ITERATOR, displayName = "Avatar Appearances")
    public List<AvatarAppearanceType> aFG() {
        switch (bFf().mo6893i(cGk)) {
            case 0:
                return null;
            case 2:
                return (List) bFf().mo5606d(new aCE(this, cGk, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, cGk, new Object[0]));
                break;
        }
        return aFF();
    }

    @C3248pc(aYR = C3248pc.C3250b.ITERATOR, displayName = "Avatar Sectors")
    public Collection<AvatarSector> aFI() {
        switch (bFf().mo6893i(cGn)) {
            case 0:
                return null;
            case 2:
                return (Collection) bFf().mo5606d(new aCE(this, cGn, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, cGn, new Object[0]));
                break;
        }
        return aFH();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Color Manager")
    @C5566aOg
    @C2499fr
    public ColorManager aFK() {
        switch (bFf().mo6893i(cGo)) {
            case 0:
                return null;
            case 2:
                return (ColorManager) bFf().mo5606d(new aCE(this, cGo, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, cGo, new Object[0]));
                break;
        }
        return aFJ();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Avatar Ocular Globes")
    public AvatarSector aFM() {
        switch (bFf().mo6893i(cGp)) {
            case 0:
                return null;
            case 2:
                return (AvatarSector) bFf().mo5606d(new aCE(this, cGp, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, cGp, new Object[0]));
                break;
        }
        return aFL();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Avatar Hair Sector")
    public AvatarSector aFO() {
        switch (bFf().mo6893i(cGr)) {
            case 0:
                return null;
            case 2:
                return (AvatarSector) bFf().mo5606d(new aCE(this, cGr, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, cGr, new Object[0]));
                break;
        }
        return aFN();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Avatar Facial Hair Sector")
    public AvatarSector aFQ() {
        switch (bFf().mo6893i(cGt)) {
            case 0:
                return null;
            case 2:
                return (AvatarSector) bFf().mo5606d(new aCE(this, cGt, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, cGt, new Object[0]));
                break;
        }
        return aFP();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Overtorso Category")
    public ClothCategory aFS() {
        switch (bFf().mo6893i(cGv)) {
            case 0:
                return null;
            case 2:
                return (ClothCategory) bFf().mo5606d(new aCE(this, cGv, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, cGv, new Object[0]));
                break;
        }
        return aFR();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Second Skin Category")
    public ClothCategory aFU() {
        switch (bFf().mo6893i(cGx)) {
            case 0:
                return null;
            case 2:
                return (ClothCategory) bFf().mo5606d(new aCE(this, cGx, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, cGx, new Object[0]));
                break;
        }
        return aFT();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Resource Path (Mesh)")
    public String aFW() {
        switch (bFf().mo6893i(cGz)) {
            case 0:
                return null;
            case 2:
                return (String) bFf().mo5606d(new aCE(this, cGz, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, cGz, new Object[0]));
                break;
        }
        return aFV();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Resource Path (Texture)")
    public String aFY() {
        switch (bFf().mo6893i(cGB)) {
            case 0:
                return null;
            case 2:
                return (String) bFf().mo5606d(new aCE(this, cGB, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, cGB, new Object[0]));
                break;
        }
        return aFX();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Worm suit")
    public Cloth aGa() {
        switch (bFf().mo6893i(cGD)) {
            case 0:
                return null;
            case 2:
                return (Cloth) bFf().mo5606d(new aCE(this, cGD, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, cGD, new Object[0]));
                break;
        }
        return aFZ();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Features Area (Pos X)")
    public float aGc() {
        switch (bFf().mo6893i(cGF)) {
            case 0:
                return 0.0f;
            case 2:
                return ((Float) bFf().mo5606d(new aCE(this, cGF, new Object[0]))).floatValue();
            case 3:
                bFf().mo5606d(new aCE(this, cGF, new Object[0]));
                break;
        }
        return aGb();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Features Area (Pos Y)")
    public float aGe() {
        switch (bFf().mo6893i(cGH)) {
            case 0:
                return 0.0f;
            case 2:
                return ((Float) bFf().mo5606d(new aCE(this, cGH, new Object[0]))).floatValue();
            case 3:
                bFf().mo5606d(new aCE(this, cGH, new Object[0]));
                break;
        }
        return aGd();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Features Area (width)")
    public float aGg() {
        switch (bFf().mo6893i(cGJ)) {
            case 0:
                return 0.0f;
            case 2:
                return ((Float) bFf().mo5606d(new aCE(this, cGJ, new Object[0]))).floatValue();
            case 3:
                bFf().mo5606d(new aCE(this, cGJ, new Object[0]));
                break;
        }
        return aGf();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Features Area (height)")
    public float aGi() {
        switch (bFf().mo6893i(cGL)) {
            case 0:
                return 0.0f;
            case 2:
                return ((Float) bFf().mo5606d(new aCE(this, cGL, new Object[0]))).floatValue();
            case 3:
                bFf().mo5606d(new aCE(this, cGL, new Object[0]));
                break;
        }
        return aGh();
    }

    @ClientOnly
    public Rectangle2D.Float aGk() {
        switch (bFf().mo6893i(cGP)) {
            case 0:
                return null;
            case 2:
                return (Rectangle2D.Float) bFf().mo5606d(new aCE(this, cGP, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, cGP, new Object[0]));
                break;
        }
        return aGj();
    }

    /* renamed from: aq */
    public UUID mo18aq() {
        switch (bFf().mo6893i(f442bN)) {
            case 0:
                return null;
            case 2:
                return (UUID) bFf().mo5606d(new aCE(this, f442bN, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f442bN, new Object[0]));
                break;
        }
        return m2599ap();
    }

    @C3248pc(aYR = C3248pc.C3250b.ADDER, displayName = "Avatar Appearances")
    /* renamed from: b */
    public void mo1738b(AvatarAppearanceType t) {
        switch (bFf().mo6893i(cGi)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, cGi, new Object[]{t}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, cGi, new Object[]{t}));
                break;
        }
        m2592a(t);
    }

    @C3248pc(aYR = C3248pc.C3250b.ADDER, displayName = "Bodypiece Banks")
    /* renamed from: b */
    public void mo1739b(BodyPieceBank arp) {
        switch (bFf().mo6893i(cGf)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, cGf, new Object[]{arp}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, cGf, new Object[]{arp}));
                break;
        }
        m2593a(arp);
    }

    /* renamed from: c */
    public C5663aRz[] mo25c() {
        return _m_fields;
    }

    @C3248pc(aYR = C3248pc.C3250b.REMOVER, displayName = "Avatar Appearances")
    /* renamed from: d */
    public void mo1740d(AvatarAppearanceType t) {
        switch (bFf().mo6893i(cGj)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, cGj, new Object[]{t}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, cGj, new Object[]{t}));
                break;
        }
        m2605c(t);
    }

    @C3248pc(aYR = C3248pc.C3250b.REMOVER, displayName = "Bodypiece Banks")
    /* renamed from: d */
    public void mo1741d(BodyPieceBank arp) {
        switch (bFf().mo6893i(cGg)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, cGg, new Object[]{arp}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, cGg, new Object[]{arp}));
                break;
        }
        m2606c(arp);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Resource Path (Mesh)")
    @C5566aOg
    /* renamed from: dY */
    public void mo1742dY(String str) {
        switch (bFf().mo6893i(cGA)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, cGA, new Object[]{str}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, cGA, new Object[]{str}));
                break;
        }
        m2614dX(str);
    }

    @C3248pc(aYR = C3248pc.C3250b.ADDER, displayName = "Avatar Sectors")
    /* renamed from: e */
    public void mo1743e(AvatarSector nw) {
        switch (bFf().mo6893i(cGl)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, cGl, new Object[]{nw}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, cGl, new Object[]{nw}));
                break;
        }
        m2609d(nw);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Features Area (Pos X)")
    @C5566aOg
    /* renamed from: eN */
    public void mo1744eN(float f) {
        switch (bFf().mo6893i(cGG)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, cGG, new Object[]{new Float(f)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, cGG, new Object[]{new Float(f)}));
                break;
        }
        m2622eM(f);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Features Area (Pos Y)")
    @C5566aOg
    /* renamed from: eP */
    public void mo1745eP(float f) {
        switch (bFf().mo6893i(cGI)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, cGI, new Object[]{new Float(f)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, cGI, new Object[]{new Float(f)}));
                break;
        }
        m2623eO(f);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Features Area (width)")
    @C5566aOg
    /* renamed from: eR */
    public void mo1746eR(float f) {
        switch (bFf().mo6893i(cGK)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, cGK, new Object[]{new Float(f)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, cGK, new Object[]{new Float(f)}));
                break;
        }
        m2624eQ(f);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Features Area (height)")
    @C5566aOg
    /* renamed from: eT */
    public void mo1747eT(float f) {
        switch (bFf().mo6893i(cGM)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, cGM, new Object[]{new Float(f)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, cGM, new Object[]{new Float(f)}));
                break;
        }
        m2625eS(f);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Resource Path (Texture)")
    @C5566aOg
    /* renamed from: ea */
    public void mo1748ea(String str) {
        switch (bFf().mo6893i(cGC)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, cGC, new Object[]{str}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, cGC, new Object[]{str}));
                break;
        }
        m2615dZ(str);
    }

    @C3248pc(aYR = C3248pc.C3250b.ADDER, displayName = "Cloth list")
    /* renamed from: f */
    public void mo1749f(Cloth wj) {
        switch (bFf().mo6893i(cFZ)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, cFZ, new Object[]{wj}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, cFZ, new Object[]{wj}));
                break;
        }
        m2616e(wj);
    }

    @C3248pc(aYR = C3248pc.C3250b.ADDER, displayName = "Cloth Categories")
    /* renamed from: f */
    public void mo1750f(ClothCategory dHVar) {
        switch (bFf().mo6893i(cGc)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, cGc, new Object[]{dHVar}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, cGc, new Object[]{dHVar}));
                break;
        }
        m2617e(dHVar);
    }

    @C3248pc(aYR = C3248pc.C3250b.REMOVER, displayName = "Avatar Sectors")
    /* renamed from: g */
    public void mo1751g(AvatarSector nw) {
        switch (bFf().mo6893i(cGm)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, cGm, new Object[]{nw}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, cGm, new Object[]{nw}));
                break;
        }
        m2626f(nw);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Handle (Consistency checker)")
    public String getHandle() {
        switch (bFf().mo6893i(f444bP)) {
            case 0:
                return null;
            case 2:
                return (String) bFf().mo5606d(new aCE(this, f444bP, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f444bP, new Object[0]));
                break;
        }
        return m2600ar();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Handle (Consistency checker)")
    @C5566aOg
    public void setHandle(String str) {
        switch (bFf().mo6893i(f445bQ)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f445bQ, new Object[]{str}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f445bQ, new Object[]{str}));
                break;
        }
        m2602b(str);
    }

    @C3248pc(aYR = C3248pc.C3250b.REMOVER, displayName = "Cloth list")
    /* renamed from: h */
    public void mo1752h(Cloth wj) {
        switch (bFf().mo6893i(cGa)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, cGa, new Object[]{wj}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, cGa, new Object[]{wj}));
                break;
        }
        m2627g(wj);
    }

    @C3248pc(aYR = C3248pc.C3250b.REMOVER, displayName = "Cloth Categories")
    /* renamed from: h */
    public void mo1753h(ClothCategory dHVar) {
        switch (bFf().mo6893i(cGd)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, cGd, new Object[]{dHVar}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, cGd, new Object[]{dHVar}));
                break;
        }
        m2628g(dHVar);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Avatar Ocular Globes")
    @C5566aOg
    /* renamed from: i */
    public void mo1754i(AvatarSector nw) {
        switch (bFf().mo6893i(cGq)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, cGq, new Object[]{nw}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, cGq, new Object[]{nw}));
                break;
        }
        m2629h(nw);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Worm suit")
    @C5566aOg
    /* renamed from: j */
    public void mo1755j(Cloth wj) {
        switch (bFf().mo6893i(cGE)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, cGE, new Object[]{wj}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, cGE, new Object[]{wj}));
                break;
        }
        m2630i(wj);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Overtorso Category")
    @C5566aOg
    /* renamed from: j */
    public void mo1756j(ClothCategory dHVar) {
        switch (bFf().mo6893i(cGw)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, cGw, new Object[]{dHVar}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, cGw, new Object[]{dHVar}));
                break;
        }
        m2631i(dHVar);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Avatar Hair Sector")
    @C5566aOg
    /* renamed from: k */
    public void mo1757k(AvatarSector nw) {
        switch (bFf().mo6893i(cGs)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, cGs, new Object[]{nw}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, cGs, new Object[]{nw}));
                break;
        }
        m2632j(nw);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Second Skin Category")
    @C5566aOg
    /* renamed from: l */
    public void mo1758l(ClothCategory dHVar) {
        switch (bFf().mo6893i(cGy)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, cGy, new Object[]{dHVar}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, cGy, new Object[]{dHVar}));
                break;
        }
        m2633k(dHVar);
    }

    /* renamed from: l */
    public boolean mo1759l(Cloth wj) {
        switch (bFf().mo6893i(cGN)) {
            case 0:
                return false;
            case 2:
                return ((Boolean) bFf().mo5606d(new aCE(this, cGN, new Object[]{wj}))).booleanValue();
            case 3:
                bFf().mo5606d(new aCE(this, cGN, new Object[]{wj}));
                break;
        }
        return m2634k(wj);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Avatar Facial Hair Sector")
    @C5566aOg
    /* renamed from: m */
    public void mo1760m(AvatarSector nw) {
        switch (bFf().mo6893i(cGu)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, cGu, new Object[]{nw}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, cGu, new Object[]{nw}));
                break;
        }
        m2635l(nw);
    }

    /* renamed from: n */
    public boolean mo1761n(Cloth wj) {
        switch (bFf().mo6893i(cGO)) {
            case 0:
                return false;
            case 2:
                return ((Boolean) bFf().mo5606d(new aCE(this, cGO, new Object[]{wj}))).booleanValue();
            case 3:
                bFf().mo5606d(new aCE(this, cGO, new Object[]{wj}));
                break;
        }
        return m2636m(wj);
    }

    @C0064Am(aul = "ead4cee567badd55eebe8aa9c0162fb7", aum = 0)
    /* renamed from: ap */
    private UUID m2599ap() {
        return m2597an();
    }

    @C0064Am(aul = "bbd66bb78aab695984400cc4066267c1", aum = 0)
    /* renamed from: b */
    private void m2603b(UUID uuid) {
        m2595a(uuid);
    }

    /* renamed from: S */
    public void mo10S() {
        super.mo10S();
        m2595a(UUID.randomUUID());
    }

    @C3248pc(aYR = C3248pc.C3250b.ADDER, displayName = "Cloth list")
    @C0064Am(aul = "cae26e9f6a0651ca7796b0f1b7c56992", aum = 0)
    /* renamed from: e */
    private void m2616e(Cloth wj) {
        aFh().add(wj);
    }

    @C3248pc(aYR = C3248pc.C3250b.REMOVER, displayName = "Cloth list")
    @C0064Am(aul = "f39467392dadc81dd8a9032be1f4efe6", aum = 0)
    /* renamed from: g */
    private void m2627g(Cloth wj) {
        aFh().remove(wj);
    }

    @C3248pc(aYR = C3248pc.C3250b.ITERATOR, displayName = "Cloth list")
    @C0064Am(aul = "e4f7785316d4bf936422235ee02aad3c", aum = 0)
    private Set<Cloth> aFz() {
        return Collections.unmodifiableSet(aFh());
    }

    @C3248pc(aYR = C3248pc.C3250b.ADDER, displayName = "Cloth Categories")
    @C0064Am(aul = "135575fd783d98e9d42f4bbc37795396", aum = 0)
    /* renamed from: e */
    private void m2617e(ClothCategory dHVar) {
        aFi().add(dHVar);
    }

    @C3248pc(aYR = C3248pc.C3250b.REMOVER, displayName = "Cloth Categories")
    @C0064Am(aul = "58ade3fe0cdadc85e4d14d7a5788cd75", aum = 0)
    /* renamed from: g */
    private void m2628g(ClothCategory dHVar) {
        aFi().remove(dHVar);
    }

    @C3248pc(aYR = C3248pc.C3250b.ITERATOR, displayName = "Cloth Categories")
    @C0064Am(aul = "97d53a1550f72f959696ede6ac2305f2", aum = 0)
    private Set<ClothCategory> aFB() {
        return Collections.unmodifiableSet(aFi());
    }

    @C3248pc(aYR = C3248pc.C3250b.ADDER, displayName = "Bodypiece Banks")
    @C0064Am(aul = "60356584ed9fdf273f58492fd52aefaf", aum = 0)
    /* renamed from: a */
    private void m2593a(BodyPieceBank arp) {
        aFn().add(arp);
    }

    @C3248pc(aYR = C3248pc.C3250b.REMOVER, displayName = "Bodypiece Banks")
    @C0064Am(aul = "73097e869ac644cca77625f474c46037", aum = 0)
    /* renamed from: c */
    private void m2606c(BodyPieceBank arp) {
        aFn().remove(arp);
    }

    @C3248pc(aYR = C3248pc.C3250b.ITERATOR, displayName = "Bodypiece Banks")
    @C0064Am(aul = "91f9f66a0c6b1ec2273f5de52c1c7422", aum = 0)
    private Set<BodyPieceBank> aFD() {
        return Collections.unmodifiableSet(aFn());
    }

    @C3248pc(aYR = C3248pc.C3250b.ADDER, displayName = "Avatar Appearances")
    @C0064Am(aul = "15cdc4d66cd55e22c07e602843ced8f2", aum = 0)
    /* renamed from: a */
    private void m2592a(AvatarAppearanceType t) {
        aFl().add(t);
    }

    @C3248pc(aYR = C3248pc.C3250b.REMOVER, displayName = "Avatar Appearances")
    @C0064Am(aul = "296c51300d24e844d54dce90c677cfab", aum = 0)
    /* renamed from: c */
    private void m2605c(AvatarAppearanceType t) {
        aFl().remove(t);
    }

    @C3248pc(aYR = C3248pc.C3250b.ITERATOR, displayName = "Avatar Appearances")
    @C0064Am(aul = "27864f8ad328393cd6b381d49acd2630", aum = 0)
    private List<AvatarAppearanceType> aFF() {
        return Collections.unmodifiableList(aFl());
    }

    @C3248pc(aYR = C3248pc.C3250b.ADDER, displayName = "Avatar Sectors")
    @C0064Am(aul = "48c26db56206a7d3d62bc286e7c83405", aum = 0)
    /* renamed from: d */
    private void m2609d(AvatarSector nw) {
        aFm().add(nw);
    }

    @C3248pc(aYR = C3248pc.C3250b.REMOVER, displayName = "Avatar Sectors")
    @C0064Am(aul = "5fd6680d9cc34e6b09d181be91aff14d", aum = 0)
    /* renamed from: f */
    private void m2626f(AvatarSector nw) {
        aFm().remove(nw);
    }

    @C3248pc(aYR = C3248pc.C3250b.ITERATOR, displayName = "Avatar Sectors")
    @C0064Am(aul = "f9a76e3dd4f69900724c75ea7db0e134", aum = 0)
    private Collection<AvatarSector> aFH() {
        return Collections.unmodifiableCollection(aFm());
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Avatar Ocular Globes")
    @C0064Am(aul = "f72ecce206d68c5112491daf75692865", aum = 0)
    private AvatarSector aFL() {
        return aFo();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Avatar Hair Sector")
    @C0064Am(aul = "6f6150ed89261f9fb409043ae1c4e700", aum = 0)
    private AvatarSector aFN() {
        return aFp();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Avatar Facial Hair Sector")
    @C0064Am(aul = "4c0146dbe0bce07df8eed10286c163d6", aum = 0)
    private AvatarSector aFP() {
        return aFq();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Overtorso Category")
    @C0064Am(aul = "1fc0884f00601ca1fb4489ea020bd8e3", aum = 0)
    private ClothCategory aFR() {
        return aFj();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Second Skin Category")
    @C0064Am(aul = "3f79b426da892fd2d7a2c8909ce01a2b", aum = 0)
    private ClothCategory aFT() {
        return aFk();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Resource Path (Mesh)")
    @C0064Am(aul = "c3d60068efc600b23e6811378327ad11", aum = 0)
    private String aFV() {
        return aFr();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Resource Path (Texture)")
    @C0064Am(aul = "3b8cd07a505a7283c2871505b0ade523", aum = 0)
    private String aFX() {
        return aFs();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Worm suit")
    @C0064Am(aul = "0dbf1bed7adf03d2a831b154aaa6f2a4", aum = 0)
    private Cloth aFZ() {
        return aFt();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Features Area (Pos X)")
    @C0064Am(aul = "b5fcf4bc48fa32eb2ee2414d31b3f822", aum = 0)
    private float aGb() {
        return aFv();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Features Area (Pos Y)")
    @C0064Am(aul = "106abe1d3fbe9f182866b8f21d936835", aum = 0)
    private float aGd() {
        return aFw();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Features Area (width)")
    @C0064Am(aul = "db9eacf1e1934064c1b871aa47234b07", aum = 0)
    private float aGf() {
        return aFx();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Features Area (height)")
    @C0064Am(aul = "cd380d0e4309ccbde9a50b84b18a3efc", aum = 0)
    private float aGh() {
        return aFy();
    }

    @C0064Am(aul = "c382df9908fb812028d78849ac0b29a3", aum = 0)
    /* renamed from: k */
    private boolean m2634k(Cloth wj) {
        return wj.bEb().equals(aFS());
    }

    @C0064Am(aul = "5a49c877545f263b35507e222020995d", aum = 0)
    /* renamed from: m */
    private boolean m2636m(Cloth wj) {
        return wj.bEb().equals(aFU());
    }

    @C0064Am(aul = "352421d8f0e2f08fd00c390588d39645", aum = 0)
    @ClientOnly
    private Rectangle2D.Float aGj() {
        if (this.cFY == null) {
            this.cFY = new Rectangle2D.Float(aFv(), aFw(), aFx(), aFy());
        }
        return this.cFY;
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Handle (Consistency checker)")
    @C0064Am(aul = "ef6d53587b5874cec2da7d9a3140d6ad", aum = 0)
    /* renamed from: ar */
    private String m2600ar() {
        return m2598ao();
    }

    @C6485anp
    @C5511aMd
    /* renamed from: a.Dq$a */
    public static class ColorManager extends aDJ implements C0468GU, C1616Xf {

        /* renamed from: Lm */
        public static final C2491fm f446Lm = null;
        public static final C2491fm _f_toString_0020_0028_0029Ljava_002flang_002fString_003b = null;
        public static final int _m_fieldCount = 0;
        public static final C5663aRz[] _m_fields = null;
        public static final int _m_methodCount = 0;
        public static final C2491fm[] _m_methods = null;
        /* renamed from: bL */
        public static final C5663aRz f448bL = null;
        /* renamed from: bN */
        public static final C2491fm f449bN = null;
        /* renamed from: bO */
        public static final C2491fm f450bO = null;
        /* renamed from: bP */
        public static final C2491fm f451bP = null;
        public static final C5663aRz gZM = null;
        public static final C5663aRz gZN = null;
        public static final C5663aRz gZO = null;
        public static final C5663aRz gZP = null;
        public static final C2491fm gZQ = null;
        public static final C2491fm gZR = null;
        public static final C2491fm gZS = null;
        public static final C2491fm gZT = null;
        public static final C2491fm gZU = null;
        public static final C2491fm gZV = null;
        public static final C2491fm gZW = null;
        public static final C2491fm gZX = null;
        public static final C2491fm gZY = null;
        public static final C2491fm gZZ = null;
        public static final C2491fm haa = null;
        public static final C2491fm hab = null;
        public static final long serialVersionUID = 0;
        public static C6494any ___iScriptClass;
        @C0064Am(aul = "87a9be45160e115d8ffb3a4910db0999", aum = 0)
        private static C2686iZ<TextureGridType> aZn;
        @C0064Am(aul = "8cf46c08947b5eb1ffb54ccee60c9719", aum = 1)
        private static C2686iZ<TextureGrid> aZo;
        @C0064Am(aul = "6b39d2ddb8c921d060ccf637bc65b857", aum = 2)
        private static C2686iZ<ColorWrapper> aZp;
        @C0064Am(aul = "8982c6f60565212f2659bfdb26e1ad39", aum = 3)
        private static C2686iZ<TextureScheme> aZq;
        @C0064Am(aul = "a9d1dbe0a1a6b1765d26b7e3b54d5117", aum = 4)

        /* renamed from: bK */
        private static UUID f447bK;

        static {
            m2673V();
        }

        public ColorManager() {
            super((C5540aNg) null);
            super.mo10S();
        }

        public ColorManager(C5540aNg ang) {
            super(ang);
        }

        /* renamed from: V */
        static void m2673V() {
            serialVersionUID = (long) 1;
            _m_fieldCount = aDJ._m_fieldCount + 5;
            _m_methodCount = aDJ._m_methodCount + 17;
            int i = aDJ._m_fieldCount;
            C5663aRz[] arzArr = new C5663aRz[(i + 5)];
            C5663aRz b = C5640aRc.m17844b(ColorManager.class, "87a9be45160e115d8ffb3a4910db0999", i);
            gZM = b;
            arzArr[i] = b;
            int i2 = i + 1;
            C5663aRz b2 = C5640aRc.m17844b(ColorManager.class, "8cf46c08947b5eb1ffb54ccee60c9719", i2);
            gZN = b2;
            arzArr[i2] = b2;
            int i3 = i2 + 1;
            C5663aRz b3 = C5640aRc.m17844b(ColorManager.class, "6b39d2ddb8c921d060ccf637bc65b857", i3);
            gZO = b3;
            arzArr[i3] = b3;
            int i4 = i3 + 1;
            C5663aRz b4 = C5640aRc.m17844b(ColorManager.class, "8982c6f60565212f2659bfdb26e1ad39", i4);
            gZP = b4;
            arzArr[i4] = b4;
            int i5 = i4 + 1;
            C5663aRz b5 = C5640aRc.m17844b(ColorManager.class, "a9d1dbe0a1a6b1765d26b7e3b54d5117", i5);
            f448bL = b5;
            arzArr[i5] = b5;
            int i6 = i5 + 1;
            _m_fields = arzArr;
            C1634Xv.m11725a((Object[]) aDJ._m_fields, (Object[]) _m_fields);
            int i7 = aDJ._m_methodCount;
            C2491fm[] fmVarArr = new C2491fm[(i7 + 17)];
            C2491fm a = C4105zY.m41624a(ColorManager.class, "0f9cedf0cf6e8bc08dc56c4952da8f66", i7);
            f449bN = a;
            fmVarArr[i7] = a;
            int i8 = i7 + 1;
            C2491fm a2 = C4105zY.m41624a(ColorManager.class, "7f928eb0772ccb14f6f3e5e2ce7006a1", i8);
            f450bO = a2;
            fmVarArr[i8] = a2;
            int i9 = i8 + 1;
            C2491fm a3 = C4105zY.m41624a(ColorManager.class, "77acdc9ff65197fe7810f856340da5cf", i9);
            f451bP = a3;
            fmVarArr[i9] = a3;
            int i10 = i9 + 1;
            C2491fm a4 = C4105zY.m41624a(ColorManager.class, "b2781b36479a530e9f3f1a8d4e439b88", i10);
            _f_toString_0020_0028_0029Ljava_002flang_002fString_003b = a4;
            fmVarArr[i10] = a4;
            int i11 = i10 + 1;
            C2491fm a5 = C4105zY.m41624a(ColorManager.class, "f06f87edc4aebe7e4c20780e3b1f48c4", i11);
            gZQ = a5;
            fmVarArr[i11] = a5;
            int i12 = i11 + 1;
            C2491fm a6 = C4105zY.m41624a(ColorManager.class, "2e58ef233615120fa6750ccb53c924ef", i12);
            gZR = a6;
            fmVarArr[i12] = a6;
            int i13 = i12 + 1;
            C2491fm a7 = C4105zY.m41624a(ColorManager.class, "2454f641bfd2eb3218c822c881c90124", i13);
            gZS = a7;
            fmVarArr[i13] = a7;
            int i14 = i13 + 1;
            C2491fm a8 = C4105zY.m41624a(ColorManager.class, "832a46c019c82617db194ee5a00d3ade", i14);
            gZT = a8;
            fmVarArr[i14] = a8;
            int i15 = i14 + 1;
            C2491fm a9 = C4105zY.m41624a(ColorManager.class, "4efc206218b7c8568d01cfd2dfb2984a", i15);
            gZU = a9;
            fmVarArr[i15] = a9;
            int i16 = i15 + 1;
            C2491fm a10 = C4105zY.m41624a(ColorManager.class, "503c80d2f28ff6a0f3821779ed77aba5", i16);
            gZV = a10;
            fmVarArr[i16] = a10;
            int i17 = i16 + 1;
            C2491fm a11 = C4105zY.m41624a(ColorManager.class, "da522325e15e52e1f93f3dcf4d57faf8", i17);
            gZW = a11;
            fmVarArr[i17] = a11;
            int i18 = i17 + 1;
            C2491fm a12 = C4105zY.m41624a(ColorManager.class, "366ed7a967d145dc7ceab960427821e3", i18);
            gZX = a12;
            fmVarArr[i18] = a12;
            int i19 = i18 + 1;
            C2491fm a13 = C4105zY.m41624a(ColorManager.class, "3e646ce8d9aeef59687bc0b71837c393", i19);
            gZY = a13;
            fmVarArr[i19] = a13;
            int i20 = i19 + 1;
            C2491fm a14 = C4105zY.m41624a(ColorManager.class, "630171a35124af0c2fe333d42079a8db", i20);
            gZZ = a14;
            fmVarArr[i20] = a14;
            int i21 = i20 + 1;
            C2491fm a15 = C4105zY.m41624a(ColorManager.class, "de325555e2fcf5feea227246c7c4a749", i21);
            haa = a15;
            fmVarArr[i21] = a15;
            int i22 = i21 + 1;
            C2491fm a16 = C4105zY.m41624a(ColorManager.class, "94a865021c5a978ad7556c6d08558b46", i22);
            hab = a16;
            fmVarArr[i22] = a16;
            int i23 = i22 + 1;
            C2491fm a17 = C4105zY.m41624a(ColorManager.class, "f433a52efd878c6c66096784e0185c11", i23);
            f446Lm = a17;
            fmVarArr[i23] = a17;
            int i24 = i23 + 1;
            _m_methods = fmVarArr;
            C1634Xv.m11725a((Object[]) aDJ._m_methods, (Object[]) _m_methods);
            ___iScriptClass = aUO.m18566a(ColorManager.class, C3439rb.class, _m_fields, _m_methods);
        }

        /* renamed from: Y */
        private void m2674Y(C2686iZ iZVar) {
            bFf().mo5608dq().mo3197f(gZM, iZVar);
        }

        /* renamed from: Z */
        private void m2675Z(C2686iZ iZVar) {
            bFf().mo5608dq().mo3197f(gZN, iZVar);
        }

        /* renamed from: a */
        private void m2676a(UUID uuid) {
            bFf().mo5608dq().mo3197f(f448bL, uuid);
        }

        /* renamed from: aa */
        private void m2677aa(C2686iZ iZVar) {
            bFf().mo5608dq().mo3197f(gZO, iZVar);
        }

        /* renamed from: ab */
        private void m2678ab(C2686iZ iZVar) {
            bFf().mo5608dq().mo3197f(gZP, iZVar);
        }

        /* renamed from: an */
        private UUID m2679an() {
            return (UUID) bFf().mo5608dq().mo3214p(f448bL);
        }

        /* renamed from: c */
        private void m2684c(UUID uuid) {
            switch (bFf().mo6893i(f450bO)) {
                case 0:
                    return;
                case 2:
                    bFf().mo5606d(new aCE(this, f450bO, new Object[]{uuid}));
                    return;
                case 3:
                    bFf().mo5606d(new aCE(this, f450bO, new Object[]{uuid}));
                    break;
            }
            m2683b(uuid);
        }

        private C2686iZ cFW() {
            return (C2686iZ) bFf().mo5608dq().mo3214p(gZM);
        }

        private C2686iZ cFX() {
            return (C2686iZ) bFf().mo5608dq().mo3214p(gZN);
        }

        private C2686iZ cFY() {
            return (C2686iZ) bFf().mo5608dq().mo3214p(gZO);
        }

        private C2686iZ cFZ() {
            return (C2686iZ) bFf().mo5608dq().mo3214p(gZP);
        }

        /* renamed from: T */
        public C6494any mo11T() {
            return ___iScriptClass;
        }

        /* renamed from: U */
        public C2491fm[] mo12U() {
            return _m_methods;
        }

        /* renamed from: W */
        public Object mo13W() {
            return new C3439rb(this);
        }

        /* renamed from: a */
        public Object mo14a(C0495Gr gr) {
            Object[] args = gr.getArgs();
            switch (gr.mo2417hq() - aDJ._m_methodCount) {
                case 0:
                    return m2680ap();
                case 1:
                    m2683b((UUID) args[0]);
                    return null;
                case 2:
                    return m2681ar();
                case 3:
                    return m2682au();
                case 4:
                    m2685d((TextureGridType) args[0]);
                    return null;
                case 5:
                    m2686f((TextureGridType) args[0]);
                    return null;
                case 6:
                    return cGa();
                case 7:
                    m2692u((TextureGrid) args[0]);
                    return null;
                case 8:
                    m2693w((TextureGrid) args[0]);
                    return null;
                case 9:
                    return cGc();
                case 10:
                    m2689k((ColorWrapper) args[0]);
                    return null;
                case 11:
                    m2690m((ColorWrapper) args[0]);
                    return null;
                case 12:
                    return cGe();
                case 13:
                    m2687h((TextureScheme) args[0]);
                    return null;
                case 14:
                    m2688j((TextureScheme) args[0]);
                    return null;
                case 15:
                    return cGg();
                case 16:
                    m2691qU();
                    return null;
                default:
                    return super.mo14a(gr);
            }
        }

        /* renamed from: a */
        public void mo15a(Collection collection, C0495Gr gr) {
            super.mo15a(collection, gr);
        }

        /* renamed from: aq */
        public UUID mo18aq() {
            switch (bFf().mo6893i(f449bN)) {
                case 0:
                    return null;
                case 2:
                    return (UUID) bFf().mo5606d(new aCE(this, f449bN, new Object[0]));
                case 3:
                    bFf().mo5606d(new aCE(this, f449bN, new Object[0]));
                    break;
            }
            return m2680ap();
        }

        /* renamed from: c */
        public C5663aRz[] mo25c() {
            return _m_fields;
        }

        @C3248pc(aYR = C3248pc.C3250b.ITERATOR, displayName = "Texture Grid Bank")
        public Set<TextureGridType> cGb() {
            switch (bFf().mo6893i(gZS)) {
                case 0:
                    return null;
                case 2:
                    return (Set) bFf().mo5606d(new aCE(this, gZS, new Object[0]));
                case 3:
                    bFf().mo5606d(new aCE(this, gZS, new Object[0]));
                    break;
            }
            return cGa();
        }

        @C3248pc(aYR = C3248pc.C3250b.ITERATOR, displayName = "Texture Grid Cell Bank")
        public Set<TextureGrid> cGd() {
            switch (bFf().mo6893i(gZV)) {
                case 0:
                    return null;
                case 2:
                    return (Set) bFf().mo5606d(new aCE(this, gZV, new Object[0]));
                case 3:
                    bFf().mo5606d(new aCE(this, gZV, new Object[0]));
                    break;
            }
            return cGc();
        }

        @C3248pc(aYR = C3248pc.C3250b.ITERATOR, displayName = "Avatar Color Bank")
        public Set<ColorWrapper> cGf() {
            switch (bFf().mo6893i(gZY)) {
                case 0:
                    return null;
                case 2:
                    return (Set) bFf().mo5606d(new aCE(this, gZY, new Object[0]));
                case 3:
                    bFf().mo5606d(new aCE(this, gZY, new Object[0]));
                    break;
            }
            return cGe();
        }

        @C3248pc(aYR = C3248pc.C3250b.ITERATOR, displayName = "Skin Color Bank")
        public Set<TextureScheme> cGh() {
            switch (bFf().mo6893i(hab)) {
                case 0:
                    return null;
                case 2:
                    return (Set) bFf().mo5606d(new aCE(this, hab, new Object[0]));
                case 3:
                    bFf().mo5606d(new aCE(this, hab, new Object[0]));
                    break;
            }
            return cGg();
        }

        @C3248pc(aYR = C3248pc.C3250b.ADDER, displayName = "Texture Grid Bank")
        /* renamed from: e */
        public void mo1767e(TextureGridType fn) {
            switch (bFf().mo6893i(gZQ)) {
                case 0:
                    return;
                case 2:
                    bFf().mo5606d(new aCE(this, gZQ, new Object[]{fn}));
                    return;
                case 3:
                    bFf().mo5606d(new aCE(this, gZQ, new Object[]{fn}));
                    break;
            }
            m2685d(fn);
        }

        @C3248pc(aYR = C3248pc.C3250b.REMOVER, displayName = "Texture Grid Bank")
        /* renamed from: g */
        public void mo1768g(TextureGridType fn) {
            switch (bFf().mo6893i(gZR)) {
                case 0:
                    return;
                case 2:
                    bFf().mo5606d(new aCE(this, gZR, new Object[]{fn}));
                    return;
                case 3:
                    bFf().mo5606d(new aCE(this, gZR, new Object[]{fn}));
                    break;
            }
            m2686f(fn);
        }

        public String getHandle() {
            switch (bFf().mo6893i(f451bP)) {
                case 0:
                    return null;
                case 2:
                    return (String) bFf().mo5606d(new aCE(this, f451bP, new Object[0]));
                case 3:
                    bFf().mo5606d(new aCE(this, f451bP, new Object[0]));
                    break;
            }
            return m2681ar();
        }

        @C3248pc(aYR = C3248pc.C3250b.ADDER, displayName = "Skin Color Bank")
        /* renamed from: i */
        public void mo1769i(TextureScheme ark) {
            switch (bFf().mo6893i(gZZ)) {
                case 0:
                    return;
                case 2:
                    bFf().mo5606d(new aCE(this, gZZ, new Object[]{ark}));
                    return;
                case 3:
                    bFf().mo5606d(new aCE(this, gZZ, new Object[]{ark}));
                    break;
            }
            m2687h(ark);
        }

        @C3248pc(aYR = C3248pc.C3250b.REMOVER, displayName = "Skin Color Bank")
        /* renamed from: k */
        public void mo1770k(TextureScheme ark) {
            switch (bFf().mo6893i(haa)) {
                case 0:
                    return;
                case 2:
                    bFf().mo5606d(new aCE(this, haa, new Object[]{ark}));
                    return;
                case 3:
                    bFf().mo5606d(new aCE(this, haa, new Object[]{ark}));
                    break;
            }
            m2688j(ark);
        }

        @C3248pc(aYR = C3248pc.C3250b.ADDER, displayName = "Avatar Color Bank")
        /* renamed from: l */
        public void mo1771l(ColorWrapper apf) {
            switch (bFf().mo6893i(gZW)) {
                case 0:
                    return;
                case 2:
                    bFf().mo5606d(new aCE(this, gZW, new Object[]{apf}));
                    return;
                case 3:
                    bFf().mo5606d(new aCE(this, gZW, new Object[]{apf}));
                    break;
            }
            m2689k(apf);
        }

        @C3248pc(aYR = C3248pc.C3250b.REMOVER, displayName = "Avatar Color Bank")
        /* renamed from: n */
        public void mo1772n(ColorWrapper apf) {
            switch (bFf().mo6893i(gZX)) {
                case 0:
                    return;
                case 2:
                    bFf().mo5606d(new aCE(this, gZX, new Object[]{apf}));
                    return;
                case 3:
                    bFf().mo5606d(new aCE(this, gZX, new Object[]{apf}));
                    break;
            }
            m2690m(apf);
        }

        /* renamed from: qV */
        public void mo1773qV() {
            switch (bFf().mo6893i(f446Lm)) {
                case 0:
                    return;
                case 2:
                    bFf().mo5606d(new aCE(this, f446Lm, new Object[0]));
                    return;
                case 3:
                    bFf().mo5606d(new aCE(this, f446Lm, new Object[0]));
                    break;
            }
            m2691qU();
        }

        public String toString() {
            switch (bFf().mo6893i(_f_toString_0020_0028_0029Ljava_002flang_002fString_003b)) {
                case 0:
                    return null;
                case 2:
                    return (String) bFf().mo5606d(new aCE(this, _f_toString_0020_0028_0029Ljava_002flang_002fString_003b, new Object[0]));
                case 3:
                    bFf().mo5606d(new aCE(this, _f_toString_0020_0028_0029Ljava_002flang_002fString_003b, new Object[0]));
                    break;
            }
            return m2682au();
        }

        @C3248pc(aYR = C3248pc.C3250b.ADDER, displayName = "Texture Grid Cell Bank")
        /* renamed from: v */
        public void mo1774v(TextureGrid uk) {
            switch (bFf().mo6893i(gZT)) {
                case 0:
                    return;
                case 2:
                    bFf().mo5606d(new aCE(this, gZT, new Object[]{uk}));
                    return;
                case 3:
                    bFf().mo5606d(new aCE(this, gZT, new Object[]{uk}));
                    break;
            }
            m2692u(uk);
        }

        @C3248pc(aYR = C3248pc.C3250b.REMOVER, displayName = "Texture Grid Cell Bank")
        /* renamed from: x */
        public void mo1775x(TextureGrid uk) {
            switch (bFf().mo6893i(gZU)) {
                case 0:
                    return;
                case 2:
                    bFf().mo5606d(new aCE(this, gZU, new Object[]{uk}));
                    return;
                case 3:
                    bFf().mo5606d(new aCE(this, gZU, new Object[]{uk}));
                    break;
            }
            m2693w(uk);
        }

        /* renamed from: S */
        public void mo10S() {
            super.mo10S();
        }

        @C0064Am(aul = "0f9cedf0cf6e8bc08dc56c4952da8f66", aum = 0)
        /* renamed from: ap */
        private UUID m2680ap() {
            return m2679an();
        }

        @C0064Am(aul = "7f928eb0772ccb14f6f3e5e2ce7006a1", aum = 0)
        /* renamed from: b */
        private void m2683b(UUID uuid) {
            m2676a(uuid);
        }

        @C0064Am(aul = "77acdc9ff65197fe7810f856340da5cf", aum = 0)
        /* renamed from: ar */
        private String m2681ar() {
            return "color_manager";
        }

        @C0064Am(aul = "b2781b36479a530e9f3f1a8d4e439b88", aum = 0)
        /* renamed from: au */
        private String m2682au() {
            return "Color Manager";
        }

        @C3248pc(aYR = C3248pc.C3250b.ADDER, displayName = "Texture Grid Bank")
        @C0064Am(aul = "f06f87edc4aebe7e4c20780e3b1f48c4", aum = 0)
        /* renamed from: d */
        private void m2685d(TextureGridType fn) {
            cFW().add(fn);
        }

        @C3248pc(aYR = C3248pc.C3250b.REMOVER, displayName = "Texture Grid Bank")
        @C0064Am(aul = "2e58ef233615120fa6750ccb53c924ef", aum = 0)
        /* renamed from: f */
        private void m2686f(TextureGridType fn) {
            cFW().remove(fn);
        }

        @C3248pc(aYR = C3248pc.C3250b.ITERATOR, displayName = "Texture Grid Bank")
        @C0064Am(aul = "2454f641bfd2eb3218c822c881c90124", aum = 0)
        private Set<TextureGridType> cGa() {
            return Collections.unmodifiableSet(cFW());
        }

        @C3248pc(aYR = C3248pc.C3250b.ADDER, displayName = "Texture Grid Cell Bank")
        @C0064Am(aul = "832a46c019c82617db194ee5a00d3ade", aum = 0)
        /* renamed from: u */
        private void m2692u(TextureGrid uk) {
            cFX().add(uk);
        }

        @C3248pc(aYR = C3248pc.C3250b.REMOVER, displayName = "Texture Grid Cell Bank")
        @C0064Am(aul = "4efc206218b7c8568d01cfd2dfb2984a", aum = 0)
        /* renamed from: w */
        private void m2693w(TextureGrid uk) {
            cFX().remove(uk);
        }

        @C3248pc(aYR = C3248pc.C3250b.ITERATOR, displayName = "Texture Grid Cell Bank")
        @C0064Am(aul = "503c80d2f28ff6a0f3821779ed77aba5", aum = 0)
        private Set<TextureGrid> cGc() {
            return Collections.unmodifiableSet(cFX());
        }

        @C3248pc(aYR = C3248pc.C3250b.ADDER, displayName = "Avatar Color Bank")
        @C0064Am(aul = "da522325e15e52e1f93f3dcf4d57faf8", aum = 0)
        /* renamed from: k */
        private void m2689k(ColorWrapper apf) {
            cFY().add(apf);
        }

        @C3248pc(aYR = C3248pc.C3250b.REMOVER, displayName = "Avatar Color Bank")
        @C0064Am(aul = "366ed7a967d145dc7ceab960427821e3", aum = 0)
        /* renamed from: m */
        private void m2690m(ColorWrapper apf) {
            cFY().remove(apf);
        }

        @C3248pc(aYR = C3248pc.C3250b.ITERATOR, displayName = "Avatar Color Bank")
        @C0064Am(aul = "3e646ce8d9aeef59687bc0b71837c393", aum = 0)
        private Set<ColorWrapper> cGe() {
            return Collections.unmodifiableSet(cFY());
        }

        @C3248pc(aYR = C3248pc.C3250b.ADDER, displayName = "Skin Color Bank")
        @C0064Am(aul = "630171a35124af0c2fe333d42079a8db", aum = 0)
        /* renamed from: h */
        private void m2687h(TextureScheme ark) {
            cFZ().add(ark);
        }

        @C3248pc(aYR = C3248pc.C3250b.REMOVER, displayName = "Skin Color Bank")
        @C0064Am(aul = "de325555e2fcf5feea227246c7c4a749", aum = 0)
        /* renamed from: j */
        private void m2688j(TextureScheme ark) {
            cFZ().remove(ark);
        }

        @C3248pc(aYR = C3248pc.C3250b.ITERATOR, displayName = "Skin Color Bank")
        @C0064Am(aul = "94a865021c5a978ad7556c6d08558b46", aum = 0)
        private Set<TextureScheme> cGg() {
            return Collections.unmodifiableSet(cFZ());
        }

        @C0064Am(aul = "f433a52efd878c6c66096784e0185c11", aum = 0)
        /* renamed from: qU */
        private void m2691qU() {
            for (TextureGridType push : cFW()) {
                push.push();
            }
            for (TextureGrid qV : cFX()) {
                qV.mo5846qV();
            }
            for (ColorWrapper push2 : cFY()) {
                push2.push();
            }
            for (TextureScheme qV2 : cFZ()) {
                qV2.mo15856qV();
            }
        }
    }
}
