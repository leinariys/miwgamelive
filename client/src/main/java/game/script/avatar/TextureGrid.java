package game.script.avatar;

import game.network.message.externalizable.aCE;
import game.script.resource.Asset;
import logic.baa.*;
import logic.data.mbean.aRY;
import logic.res.code.C5640aRc;
import logic.res.code.C5663aRz;
import logic.res.code.C6494any;
import logic.res.html.C2491fm;
import p001a.*;
import taikodom.infra.script.I18NString;
import taikodom.render.textures.BaseTexture;

import java.awt.*;
import java.awt.geom.Rectangle2D;
import java.util.Collection;
import java.util.UUID;

@C2712iu(mo19786Bt = BaseTextureGridType.class)
@C5511aMd
@C6485anp
/* renamed from: a.UK */
/* compiled from: a */
public class TextureGrid extends TaikodomObject implements C0468GU, C1616Xf {

    /* renamed from: Lm */
    public static final C2491fm f1736Lm = null;
    public static final C2491fm _f_getType_0020_0028_0029Ljava_002flang_002fObject_003b = null;
    public static final C2491fm _f_toString_0020_0028_0029Ljava_002flang_002fString_003b = null;
    public static final int _m_fieldCount = 0;
    public static final C5663aRz[] _m_fields = null;
    public static final int _m_methodCount = 0;
    public static final C2491fm[] _m_methods = null;
    /* renamed from: bL */
    public static final C5663aRz f1738bL = null;
    /* renamed from: bM */
    public static final C5663aRz f1739bM = null;
    /* renamed from: bN */
    public static final C2491fm f1740bN = null;
    /* renamed from: bO */
    public static final C2491fm f1741bO = null;
    /* renamed from: bP */
    public static final C2491fm f1742bP = null;
    /* renamed from: bQ */
    public static final C2491fm f1743bQ = null;
    public static final C5663aRz cWw = null;
    public static final C5663aRz cWx = null;
    public static final C5663aRz cWy = null;
    public static final C5663aRz eqd = null;
    public static final C5663aRz eqe = null;
    public static final C2491fm eqg = null;
    public static final C2491fm eqh = null;
    public static final C2491fm eqi = null;
    public static final C2491fm eqj = null;
    public static final C2491fm eqk = null;
    public static final C2491fm eql = null;
    public static final C2491fm eqm = null;
    public static final C2491fm eqn = null;
    public static final C2491fm eqo = null;
    public static final C2491fm eqp = null;
    public static final long serialVersionUID = 0;
    /* renamed from: zQ */
    public static final C5663aRz f1745zQ = null;
    /* renamed from: zT */
    public static final C2491fm f1746zT = null;
    /* renamed from: zU */
    public static final C2491fm f1747zU = null;
    public static C6494any ___iScriptClass;
    @C0064Am(aul = "a34424902e908c90f78c98327fd15440", aum = 6)

    /* renamed from: bK */
    private static UUID f1737bK;
    @C0064Am(aul = "365a3d37aad89cc1aaf58813e64f96f7", aum = 0)
    private static Asset cWv;
    @C0064Am(aul = "aff7431de523ff98115938296898c946", aum = 1)
    private static int cellHeight;
    @C0064Am(aul = "7a98d9d9280225ad151020d999d21855", aum = 2)
    private static int cellWidth;
    @C0064Am(aul = "8892f5cc5d4e08c1c3db89f777ad1921", aum = 4)
    private static ColorWrapper djz;
    @C0064Am(aul = "41c55c9af3001e4b5a2ad561e37a1318", aum = 3)
    private static int eqc;
    @C0064Am(aul = "93fdfa23c59f03d64616271fdb0b1501", aum = 7)
    private static String handle;
    @C0064Am(aul = "ba558fa005378334dbe5cda8f2c1d8ee", aum = 5)

    /* renamed from: zP */
    private static I18NString f1744zP;

    static {
        m10213V();
    }

    @ClientOnly
    private transient Rectangle2D.Float eqf;

    public TextureGrid() {
        super((C5540aNg) null);
        super.mo10S();
    }

    public TextureGrid(TextureGridType fn) {
        super((C5540aNg) null);
        super._m_script_init(fn);
    }

    public TextureGrid(C5540aNg ang) {
        super(ang);
    }

    /* renamed from: V */
    static void m10213V() {
        serialVersionUID = (long) 1;
        _m_fieldCount = TaikodomObject._m_fieldCount + 8;
        _m_methodCount = TaikodomObject._m_methodCount + 19;
        int i = TaikodomObject._m_fieldCount;
        C5663aRz[] arzArr = new C5663aRz[(i + 8)];
        C5663aRz b = C5640aRc.m17844b(TextureGrid.class, "365a3d37aad89cc1aaf58813e64f96f7", i);
        cWw = b;
        arzArr[i] = b;
        int i2 = i + 1;
        C5663aRz b2 = C5640aRc.m17844b(TextureGrid.class, "aff7431de523ff98115938296898c946", i2);
        cWx = b2;
        arzArr[i2] = b2;
        int i3 = i2 + 1;
        C5663aRz b3 = C5640aRc.m17844b(TextureGrid.class, "7a98d9d9280225ad151020d999d21855", i3);
        cWy = b3;
        arzArr[i3] = b3;
        int i4 = i3 + 1;
        C5663aRz b4 = C5640aRc.m17844b(TextureGrid.class, "41c55c9af3001e4b5a2ad561e37a1318", i4);
        eqd = b4;
        arzArr[i4] = b4;
        int i5 = i4 + 1;
        C5663aRz b5 = C5640aRc.m17844b(TextureGrid.class, "8892f5cc5d4e08c1c3db89f777ad1921", i5);
        eqe = b5;
        arzArr[i5] = b5;
        int i6 = i5 + 1;
        C5663aRz b6 = C5640aRc.m17844b(TextureGrid.class, "ba558fa005378334dbe5cda8f2c1d8ee", i6);
        f1745zQ = b6;
        arzArr[i6] = b6;
        int i7 = i6 + 1;
        C5663aRz b7 = C5640aRc.m17844b(TextureGrid.class, "a34424902e908c90f78c98327fd15440", i7);
        f1738bL = b7;
        arzArr[i7] = b7;
        int i8 = i7 + 1;
        C5663aRz b8 = C5640aRc.m17844b(TextureGrid.class, "93fdfa23c59f03d64616271fdb0b1501", i8);
        f1739bM = b8;
        arzArr[i8] = b8;
        int i9 = i8 + 1;
        _m_fields = arzArr;
        C1634Xv.m11725a((Object[]) TaikodomObject._m_fields, (Object[]) _m_fields);
        int i10 = TaikodomObject._m_methodCount;
        C2491fm[] fmVarArr = new C2491fm[(i10 + 19)];
        C2491fm a = C4105zY.m41624a(TextureGrid.class, "9aa82b896be50b5946365152aa981ee3", i10);
        f1740bN = a;
        fmVarArr[i10] = a;
        int i11 = i10 + 1;
        C2491fm a2 = C4105zY.m41624a(TextureGrid.class, "36194bde27eb768897f82c589eb7b896", i11);
        f1741bO = a2;
        fmVarArr[i11] = a2;
        int i12 = i11 + 1;
        C2491fm a3 = C4105zY.m41624a(TextureGrid.class, "c77f7de897a00de26b399d80fedcea4a", i12);
        _f_toString_0020_0028_0029Ljava_002flang_002fString_003b = a3;
        fmVarArr[i12] = a3;
        int i13 = i12 + 1;
        C2491fm a4 = C4105zY.m41624a(TextureGrid.class, "4c49b5af53078885a0a80084c14986ad", i13);
        eqg = a4;
        fmVarArr[i13] = a4;
        int i14 = i13 + 1;
        C2491fm a5 = C4105zY.m41624a(TextureGrid.class, "8f7c0a2a67a44c1ff5cd3d7c7833db63", i14);
        eqh = a5;
        fmVarArr[i14] = a5;
        int i15 = i14 + 1;
        C2491fm a6 = C4105zY.m41624a(TextureGrid.class, "edd57c69e73187ab954ab468660162b9", i15);
        eqi = a6;
        fmVarArr[i15] = a6;
        int i16 = i15 + 1;
        C2491fm a7 = C4105zY.m41624a(TextureGrid.class, "3b5365368c415598d9cae56d613dc6bb", i16);
        eqj = a7;
        fmVarArr[i16] = a7;
        int i17 = i16 + 1;
        C2491fm a8 = C4105zY.m41624a(TextureGrid.class, "7faf0ef5c3b4cbca46de78b517989eb9", i17);
        eqk = a8;
        fmVarArr[i17] = a8;
        int i18 = i17 + 1;
        C2491fm a9 = C4105zY.m41624a(TextureGrid.class, "562894800c71b01216b3c789115f8feb", i18);
        eql = a9;
        fmVarArr[i18] = a9;
        int i19 = i18 + 1;
        C2491fm a10 = C4105zY.m41624a(TextureGrid.class, "0ae529b10ec3dd32170b48e53e1513b6", i19);
        eqm = a10;
        fmVarArr[i19] = a10;
        int i20 = i19 + 1;
        C2491fm a11 = C4105zY.m41624a(TextureGrid.class, "26404afbc2b1bd510e37e2d164750048", i20);
        eqn = a11;
        fmVarArr[i20] = a11;
        int i21 = i20 + 1;
        C2491fm a12 = C4105zY.m41624a(TextureGrid.class, "14b2ff87ba72a2aad7920a47d10267d0", i21);
        eqo = a12;
        fmVarArr[i21] = a12;
        int i22 = i21 + 1;
        C2491fm a13 = C4105zY.m41624a(TextureGrid.class, "6c9e0dc062e2af61f393527295eb9d0d", i22);
        eqp = a13;
        fmVarArr[i22] = a13;
        int i23 = i22 + 1;
        C2491fm a14 = C4105zY.m41624a(TextureGrid.class, "cc5b0c50a29f78728823916b0e91ce15", i23);
        f1746zT = a14;
        fmVarArr[i23] = a14;
        int i24 = i23 + 1;
        C2491fm a15 = C4105zY.m41624a(TextureGrid.class, "ea4451b9c846406f66479dcbb732a949", i24);
        f1747zU = a15;
        fmVarArr[i24] = a15;
        int i25 = i24 + 1;
        C2491fm a16 = C4105zY.m41624a(TextureGrid.class, "006d28d5074249cd5cb9d55e64af9522", i25);
        f1743bQ = a16;
        fmVarArr[i25] = a16;
        int i26 = i25 + 1;
        C2491fm a17 = C4105zY.m41624a(TextureGrid.class, "097abec20f81694a027042101119e7bd", i26);
        f1742bP = a17;
        fmVarArr[i26] = a17;
        int i27 = i26 + 1;
        C2491fm a18 = C4105zY.m41624a(TextureGrid.class, "18807ef5818fbfc0bbfd9d574536a95e", i27);
        f1736Lm = a18;
        fmVarArr[i27] = a18;
        int i28 = i27 + 1;
        C2491fm a19 = C4105zY.m41624a(TextureGrid.class, "97caca6a5d945baea852a14d1cc8eb83", i28);
        _f_getType_0020_0028_0029Ljava_002flang_002fObject_003b = a19;
        fmVarArr[i28] = a19;
        int i29 = i28 + 1;
        _m_methods = fmVarArr;
        C1634Xv.m11725a((Object[]) TaikodomObject._m_methods, (Object[]) _m_methods);
        ___iScriptClass = aUO.m18566a(TextureGrid.class, aRY.class, _m_fields, _m_methods);
    }

    /* renamed from: a */
    private void m10214a(String str) {
        bFf().mo5608dq().mo3197f(f1739bM, str);
    }

    /* renamed from: a */
    private void m10215a(UUID uuid) {
        bFf().mo5608dq().mo3197f(f1738bL, uuid);
    }

    private Asset aPJ() {
        return ((TextureGridType) getType()).aPN();
    }

    private int aPK() {
        return ((TextureGridType) getType()).aPP();
    }

    private int aPL() {
        return ((TextureGridType) getType()).mo2117gw();
    }

    /* renamed from: an */
    private UUID m10216an() {
        return (UUID) bFf().mo5608dq().mo3214p(f1738bL);
    }

    /* renamed from: an */
    private void m10217an(Asset tCVar) {
        throw new C6039afL();
    }

    /* renamed from: ao */
    private String m10218ao() {
        return (String) bFf().mo5608dq().mo3214p(f1739bM);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Grid definition")
    @C0064Am(aul = "8f7c0a2a67a44c1ff5cd3d7c7833db63", aum = 0)
    @C5566aOg
    /* renamed from: b */
    private void m10222b(TextureGridType fn) {
        throw new aWi(new aCE(this, eqh, new Object[]{fn}));
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Handle (Consistency checker)")
    @C0064Am(aul = "006d28d5074249cd5cb9d55e64af9522", aum = 0)
    @C5566aOg
    /* renamed from: b */
    private void m10223b(String str) {
        throw new aWi(new aCE(this, f1743bQ, new Object[]{str}));
    }

    private int bye() {
        return bFf().mo5608dq().mo3212n(eqd);
    }

    private ColorWrapper byf() {
        return (ColorWrapper) bFf().mo5608dq().mo3214p(eqe);
    }

    /* renamed from: c */
    private void m10225c(UUID uuid) {
        switch (bFf().mo6893i(f1741bO)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f1741bO, new Object[]{uuid}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f1741bO, new Object[]{uuid}));
                break;
        }
        m10224b(uuid);
    }

    /* renamed from: d */
    private void m10226d(I18NString i18NString) {
        bFf().mo5608dq().mo3197f(f1745zQ, i18NString);
    }

    /* renamed from: e */
    private void m10227e(ColorWrapper apf) {
        bFf().mo5608dq().mo3197f(eqe, apf);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Name (GUI feedback)")
    @C0064Am(aul = "ea4451b9c846406f66479dcbb732a949", aum = 0)
    @C5566aOg
    /* renamed from: e */
    private void m10228e(I18NString i18NString) {
        throw new aWi(new aCE(this, f1747zU, new Object[]{i18NString}));
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Color (GUI feedback)")
    @C0064Am(aul = "6c9e0dc062e2af61f393527295eb9d0d", aum = 0)
    @C5566aOg
    /* renamed from: f */
    private void m10229f(ColorWrapper apf) {
        throw new aWi(new aCE(this, eqp, new Object[]{apf}));
    }

    /* renamed from: hk */
    private void m10230hk(int i) {
        throw new C6039afL();
    }

    /* renamed from: hl */
    private void m10231hl(int i) {
        throw new C6039afL();
    }

    /* renamed from: kb */
    private I18NString m10232kb() {
        return (I18NString) bFf().mo5608dq().mo3214p(f1745zQ);
    }

    /* renamed from: oe */
    private void m10234oe(int i) {
        bFf().mo5608dq().mo3183b(eqd, i);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Index")
    @C0064Am(aul = "3b5365368c415598d9cae56d613dc6bb", aum = 0)
    @C5566aOg
    /* renamed from: of */
    private void m10235of(int i) {
        throw new aWi(new aCE(this, eqj, new Object[]{new Integer(i)}));
    }

    @C0064Am(aul = "97caca6a5d945baea852a14d1cc8eb83", aum = 0)
    /* renamed from: qW */
    private Object m10237qW() {
        return byk();
    }

    /* renamed from: T */
    public C6494any mo11T() {
        return ___iScriptClass;
    }

    /* renamed from: U */
    public C2491fm[] mo12U() {
        return _m_methods;
    }

    /* renamed from: W */
    public Object mo13W() {
        return new aRY(this);
    }

    /* renamed from: a */
    public Object mo14a(C0495Gr gr) {
        Object[] args = gr.getArgs();
        switch (gr.mo2417hq() - TaikodomObject._m_methodCount) {
            case 0:
                return m10219ap();
            case 1:
                m10224b((UUID) args[0]);
                return null;
            case 2:
                return m10221au();
            case 3:
                return byg();
            case 4:
                m10222b((TextureGridType) args[0]);
                return null;
            case 5:
                return new Integer(byi());
            case 6:
                m10235of(((Integer) args[0]).intValue());
                return null;
            case 7:
                return byj();
            case 8:
                return byl();
            case 9:
                return bym();
            case 10:
                return byo();
            case 11:
                return byq();
            case 12:
                m10229f((ColorWrapper) args[0]);
                return null;
            case 13:
                return m10233kd();
            case 14:
                m10228e((I18NString) args[0]);
                return null;
            case 15:
                m10223b((String) args[0]);
                return null;
            case 16:
                return m10220ar();
            case 17:
                m10236qU();
                return null;
            case 18:
                return m10237qW();
            default:
                return super.mo14a(gr);
        }
    }

    /* renamed from: a */
    public void mo15a(Collection collection, C0495Gr gr) {
        super.mo15a(collection, gr);
    }

    /* renamed from: aq */
    public UUID mo18aq() {
        switch (bFf().mo6893i(f1740bN)) {
            case 0:
                return null;
            case 2:
                return (UUID) bFf().mo5606d(new aCE(this, f1740bN, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f1740bN, new Object[0]));
                break;
        }
        return m10219ap();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Grid definition")
    public TextureGridType byh() {
        switch (bFf().mo6893i(eqg)) {
            case 0:
                return null;
            case 2:
                return (TextureGridType) bFf().mo5606d(new aCE(this, eqg, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, eqg, new Object[0]));
                break;
        }
        return byg();
    }

    /* access modifiers changed from: protected */
    public TextureGridType byk() {
        switch (bFf().mo6893i(eqk)) {
            case 0:
                return null;
            case 2:
                return (TextureGridType) bFf().mo5606d(new aCE(this, eqk, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, eqk, new Object[0]));
                break;
        }
        return byj();
    }

    @ClientOnly
    public Color byn() {
        switch (bFf().mo6893i(eqm)) {
            case 0:
                return null;
            case 2:
                return (Color) bFf().mo5606d(new aCE(this, eqm, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, eqm, new Object[0]));
                break;
        }
        return bym();
    }

    @ClientOnly
    public Rectangle2D.Float byp() {
        switch (bFf().mo6893i(eqn)) {
            case 0:
                return null;
            case 2:
                return (Rectangle2D.Float) bFf().mo5606d(new aCE(this, eqn, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, eqn, new Object[0]));
                break;
        }
        return byo();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Color (GUI feedback)")
    public ColorWrapper byr() {
        switch (bFf().mo6893i(eqo)) {
            case 0:
                return null;
            case 2:
                return (ColorWrapper) bFf().mo5606d(new aCE(this, eqo, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, eqo, new Object[0]));
                break;
        }
        return byq();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Grid definition")
    @C5566aOg
    /* renamed from: c */
    public void mo5840c(TextureGridType fn) {
        switch (bFf().mo6893i(eqh)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, eqh, new Object[]{fn}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, eqh, new Object[]{fn}));
                break;
        }
        m10222b(fn);
    }

    /* renamed from: c */
    public C5663aRz[] mo25c() {
        return _m_fields;
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Name (GUI feedback)")
    @C5566aOg
    /* renamed from: f */
    public void mo5841f(I18NString i18NString) {
        switch (bFf().mo6893i(f1747zU)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f1747zU, new Object[]{i18NString}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f1747zU, new Object[]{i18NString}));
                break;
        }
        m10228e(i18NString);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Color (GUI feedback)")
    @C5566aOg
    /* renamed from: g */
    public void mo5842g(ColorWrapper apf) {
        switch (bFf().mo6893i(eqp)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, eqp, new Object[]{apf}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, eqp, new Object[]{apf}));
                break;
        }
        m10229f(apf);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Handle (Consistency checker)")
    public String getHandle() {
        switch (bFf().mo6893i(f1742bP)) {
            case 0:
                return null;
            case 2:
                return (String) bFf().mo5606d(new aCE(this, f1742bP, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f1742bP, new Object[0]));
                break;
        }
        return m10220ar();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Handle (Consistency checker)")
    @C5566aOg
    public void setHandle(String str) {
        switch (bFf().mo6893i(f1743bQ)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f1743bQ, new Object[]{str}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f1743bQ, new Object[]{str}));
                break;
        }
        m10223b(str);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Index")
    public int getIndex() {
        switch (bFf().mo6893i(eqi)) {
            case 0:
                return 0;
            case 2:
                return ((Integer) bFf().mo5606d(new aCE(this, eqi, new Object[0]))).intValue();
            case 3:
                bFf().mo5606d(new aCE(this, eqi, new Object[0]));
                break;
        }
        return byi();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Index")
    @C5566aOg
    public void setIndex(int i) {
        switch (bFf().mo6893i(eqj)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, eqj, new Object[]{new Integer(i)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, eqj, new Object[]{new Integer(i)}));
                break;
        }
        m10235of(i);
    }

    @ClientOnly
    public BaseTexture getTexture() {
        switch (bFf().mo6893i(eql)) {
            case 0:
                return null;
            case 2:
                return (BaseTexture) bFf().mo5606d(new aCE(this, eql, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, eql, new Object[0]));
                break;
        }
        return byl();
    }

    /* access modifiers changed from: protected */
    public /* bridge */ /* synthetic */ Object getType() {
        switch (bFf().mo6893i(_f_getType_0020_0028_0029Ljava_002flang_002fObject_003b)) {
            case 0:
                return null;
            case 2:
                return bFf().mo5606d(new aCE(this, _f_getType_0020_0028_0029Ljava_002flang_002fObject_003b, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, _f_getType_0020_0028_0029Ljava_002flang_002fObject_003b, new Object[0]));
                break;
        }
        return m10237qW();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Name (GUI feedback)")
    /* renamed from: ke */
    public I18NString mo5845ke() {
        switch (bFf().mo6893i(f1746zT)) {
            case 0:
                return null;
            case 2:
                return (I18NString) bFf().mo5606d(new aCE(this, f1746zT, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f1746zT, new Object[0]));
                break;
        }
        return m10233kd();
    }

    /* renamed from: qV */
    public void mo5846qV() {
        switch (bFf().mo6893i(f1736Lm)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f1736Lm, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f1736Lm, new Object[0]));
                break;
        }
        m10236qU();
    }

    public String toString() {
        switch (bFf().mo6893i(_f_toString_0020_0028_0029Ljava_002flang_002fString_003b)) {
            case 0:
                return null;
            case 2:
                return (String) bFf().mo5606d(new aCE(this, _f_toString_0020_0028_0029Ljava_002flang_002fString_003b, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, _f_toString_0020_0028_0029Ljava_002flang_002fString_003b, new Object[0]));
                break;
        }
        return m10221au();
    }

    @C0064Am(aul = "9aa82b896be50b5946365152aa981ee3", aum = 0)
    /* renamed from: ap */
    private UUID m10219ap() {
        return m10216an();
    }

    @C0064Am(aul = "36194bde27eb768897f82c589eb7b896", aum = 0)
    /* renamed from: b */
    private void m10224b(UUID uuid) {
        m10215a(uuid);
    }

    /* renamed from: S */
    public void mo10S() {
        super.mo10S();
        m10215a(UUID.randomUUID());
    }

    @C0064Am(aul = "c77f7de897a00de26b399d80fedcea4a", aum = 0)
    /* renamed from: au */
    private String m10221au() {
        if (m10232kb() == null) {
            return null;
        }
        return m10232kb().get();
    }

    /* renamed from: a */
    public void mo5834a(TextureGridType fn) {
        super.mo967a((C2961mJ) fn);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Grid definition")
    @C0064Am(aul = "4c49b5af53078885a0a80084c14986ad", aum = 0)
    private TextureGridType byg() {
        return byk();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Index")
    @C0064Am(aul = "edd57c69e73187ab954ab468660162b9", aum = 0)
    private int byi() {
        return bye();
    }

    @C0064Am(aul = "7faf0ef5c3b4cbca46de78b517989eb9", aum = 0)
    private TextureGridType byj() {
        return (TextureGridType) super.getType();
    }

    @C0064Am(aul = "562894800c71b01216b3c789115f8feb", aum = 0)
    @ClientOnly
    private BaseTexture byl() {
        if (byk() == null) {
            return null;
        }
        return byk().mo13238b(ald().ale());
    }

    @C0064Am(aul = "0ae529b10ec3dd32170b48e53e1513b6", aum = 0)
    @ClientOnly
    private Color bym() {
        return byf() == null ? Color.WHITE : byf().getColor();
    }

    @C0064Am(aul = "26404afbc2b1bd510e37e2d164750048", aum = 0)
    @ClientOnly
    private Rectangle2D.Float byo() {
        if (this.eqf == null) {
            BaseTexture texture = getTexture();
            if (texture == null) {
                return new Rectangle2D.Float(0.0f, 0.0f, 0.0f, 0.0f);
            }
            int sizeX = texture.getSizeX();
            int sizeY = texture.getSizeY();
            if (sizeX <= 0 || sizeY <= 0) {
                BaseTextureGridType.logger.debug("Width or height is zero for this texture");
                return new Rectangle2D.Float(0.0f, 0.0f, 0.0f, 0.0f);
            }
            int gw = sizeX / byk().mo2117gw();
            int aPP = sizeY / byk().aPP();
            if (gw <= 0 || aPP <= 0) {
                BaseTextureGridType.logger.debug("Columns or rows counts allowed for this cell size is zero");
                return new Rectangle2D.Float(0.0f, 0.0f, 0.0f, 0.0f);
            }
            float gw2 = ((float) byk().mo2117gw()) / ((float) sizeX);
            float aPP2 = ((float) byk().aPP()) / ((float) sizeY);
            float min = ((float) Math.min(bye() % gw, gw - 1)) * gw2;
            float min2 = ((float) Math.min(bye() / gw, aPP - 1)) * aPP2;
            this.eqf = new Rectangle2D.Float(min, min2, gw2 + min, aPP2 + min2);
        }
        return this.eqf;
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Color (GUI feedback)")
    @C0064Am(aul = "14b2ff87ba72a2aad7920a47d10267d0", aum = 0)
    private ColorWrapper byq() {
        return byf();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Name (GUI feedback)")
    @C0064Am(aul = "cc5b0c50a29f78728823916b0e91ce15", aum = 0)
    /* renamed from: kd */
    private I18NString m10233kd() {
        return m10232kb();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Handle (Consistency checker)")
    @C0064Am(aul = "097abec20f81694a027042101119e7bd", aum = 0)
    /* renamed from: ar */
    private String m10220ar() {
        return m10218ao();
    }

    @C0064Am(aul = "18807ef5818fbfc0bbfd9d574536a95e", aum = 0)
    /* renamed from: qU */
    private void m10236qU() {
        push();
        TextureGridType byh = byh();
        if (byh != null) {
            byh.push();
        }
        Asset aPN = byh.aPN();
        if (aPN != null) {
            aPN.push();
        }
        if (byf() != null) {
            byf().push();
        }
    }
}
