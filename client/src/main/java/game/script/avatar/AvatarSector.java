package game.script.avatar;

import game.network.message.externalizable.aCE;
import logic.baa.*;
import logic.data.mbean.C1427Uu;
import logic.res.code.C5640aRc;
import logic.res.code.C5663aRz;
import logic.res.code.C6494any;
import logic.res.html.C2491fm;
import p001a.*;
import taikodom.infra.script.I18NString;

import java.awt.geom.Rectangle2D;
import java.util.Collection;
import java.util.UUID;

@C5511aMd
@C6485anp
/* renamed from: a.Nw */
/* compiled from: a */
public class AvatarSector extends aDJ implements C0468GU, C1616Xf {
    public static final C2491fm _f_toString_0020_0028_0029Ljava_002flang_002fString_003b = null;
    public static final int _m_fieldCount = 0;
    public static final C5663aRz[] _m_fields = null;
    public static final int _m_methodCount = 0;
    public static final C2491fm[] _m_methods = null;
    /* renamed from: bL */
    public static final C5663aRz f1261bL = null;
    /* renamed from: bN */
    public static final C2491fm f1262bN = null;
    /* renamed from: bO */
    public static final C2491fm f1263bO = null;
    /* renamed from: bP */
    public static final C2491fm f1264bP = null;
    /* renamed from: bQ */
    public static final C2491fm f1265bQ = null;
    public static final C2491fm cSO = null;
    public static final int dDV = -1;
    public static final C5663aRz dDX = null;
    public static final C5663aRz dDZ = null;
    public static final C5663aRz dEa = null;
    public static final C5663aRz dEb = null;
    public static final C5663aRz dEc = null;
    public static final C5663aRz dEd = null;
    public static final C2491fm dEe = null;
    public static final C2491fm dEf = null;
    public static final C2491fm dEg = null;
    public static final C2491fm dEh = null;
    public static final C2491fm dEi = null;
    public static final C2491fm dEj = null;
    public static final C2491fm dEk = null;
    public static final C2491fm dEl = null;
    public static final C2491fm dEm = null;
    public static final C2491fm dEn = null;
    public static final C2491fm dEo = null;
    public static final C2491fm dEp = null;
    /* renamed from: eq */
    public static final C5663aRz f1266eq = null;
    /* renamed from: ff */
    public static final C2491fm f1267ff = null;
    /* renamed from: fg */
    public static final C2491fm f1268fg = null;
    public static final long serialVersionUID = 0;
    /* renamed from: zQ */
    public static final C5663aRz f1272zQ = null;
    /* renamed from: zS */
    public static final C5663aRz f1274zS = null;
    /* renamed from: zT */
    public static final C2491fm f1275zT = null;
    /* renamed from: zU */
    public static final C2491fm f1276zU = null;
    public static C6494any ___iScriptClass = null;
    @C0064Am(aul = "554d6e0ebc834500640e1f7f25b48eea", aum = 9)

    /* renamed from: bK */
    private static UUID f1260bK = null;
    @C0064Am(aul = "0ad2c70a92c2cfaa11af4b9acbc5f4bc", aum = 1)
    private static int dDW;
    @C0064Am(aul = "417c962573555cfad04188436e28051d", aum = 2)
    private static C0955a dDY;
    @C0064Am(aul = "88bef3c63ef4bab40337f0d91501c882", aum = 7)
    private static float height;
    @C0064Am(aul = "3a7311ce0a409c83d7d957d895bc2dd9", aum = 3)
    private static String prefix;
    @C0064Am(aul = "d3904c2986f2ea2b500a2b0c7cac4f60", aum = 6)
    private static float width;
    @C0064Am(aul = "366d73b2ac1d672c443eb111b0792f7a", aum = 4)

    /* renamed from: x */
    private static float f1269x;
    @C0064Am(aul = "3da8dfcfee6194b359c3867d99d45b8c", aum = 5)

    /* renamed from: y */
    private static float f1270y;
    @C0064Am(aul = "8dcb7c28dca806f724f5889d02e1574d", aum = 0)

    /* renamed from: zP */
    private static I18NString f1271zP;
    @C0064Am(aul = "3ae35b882b9bbcc11b5a11892719a5cc", aum = 8)

    /* renamed from: zR */
    private static String f1273zR;

    static {
        m7746V();
    }

    public AvatarSector() {
        super((C5540aNg) null);
        super.mo10S();
    }

    public AvatarSector(C5540aNg ang) {
        super(ang);
    }

    /* renamed from: V */
    static void m7746V() {
        serialVersionUID = (long) 1;
        _m_fieldCount = aDJ._m_fieldCount + 10;
        _m_methodCount = aDJ._m_methodCount + 22;
        int i = aDJ._m_fieldCount;
        C5663aRz[] arzArr = new C5663aRz[(i + 10)];
        C5663aRz b = C5640aRc.m17844b(AvatarSector.class, "8dcb7c28dca806f724f5889d02e1574d", i);
        f1272zQ = b;
        arzArr[i] = b;
        int i2 = i + 1;
        C5663aRz b2 = C5640aRc.m17844b(AvatarSector.class, "0ad2c70a92c2cfaa11af4b9acbc5f4bc", i2);
        dDX = b2;
        arzArr[i2] = b2;
        int i3 = i2 + 1;
        C5663aRz b3 = C5640aRc.m17844b(AvatarSector.class, "417c962573555cfad04188436e28051d", i3);
        dDZ = b3;
        arzArr[i3] = b3;
        int i4 = i3 + 1;
        C5663aRz b4 = C5640aRc.m17844b(AvatarSector.class, "3a7311ce0a409c83d7d957d895bc2dd9", i4);
        f1266eq = b4;
        arzArr[i4] = b4;
        int i5 = i4 + 1;
        C5663aRz b5 = C5640aRc.m17844b(AvatarSector.class, "366d73b2ac1d672c443eb111b0792f7a", i5);
        dEa = b5;
        arzArr[i5] = b5;
        int i6 = i5 + 1;
        C5663aRz b6 = C5640aRc.m17844b(AvatarSector.class, "3da8dfcfee6194b359c3867d99d45b8c", i6);
        dEb = b6;
        arzArr[i6] = b6;
        int i7 = i6 + 1;
        C5663aRz b7 = C5640aRc.m17844b(AvatarSector.class, "d3904c2986f2ea2b500a2b0c7cac4f60", i7);
        dEc = b7;
        arzArr[i7] = b7;
        int i8 = i7 + 1;
        C5663aRz b8 = C5640aRc.m17844b(AvatarSector.class, "88bef3c63ef4bab40337f0d91501c882", i8);
        dEd = b8;
        arzArr[i8] = b8;
        int i9 = i8 + 1;
        C5663aRz b9 = C5640aRc.m17844b(AvatarSector.class, "3ae35b882b9bbcc11b5a11892719a5cc", i9);
        f1274zS = b9;
        arzArr[i9] = b9;
        int i10 = i9 + 1;
        C5663aRz b10 = C5640aRc.m17844b(AvatarSector.class, "554d6e0ebc834500640e1f7f25b48eea", i10);
        f1261bL = b10;
        arzArr[i10] = b10;
        int i11 = i10 + 1;
        _m_fields = arzArr;
        C1634Xv.m11725a((Object[]) aDJ._m_fields, (Object[]) _m_fields);
        int i12 = aDJ._m_methodCount;
        C2491fm[] fmVarArr = new C2491fm[(i12 + 22)];
        C2491fm a = C4105zY.m41624a(AvatarSector.class, "943fab63343092e51e2ccf3c4e6cf218", i12);
        f1262bN = a;
        fmVarArr[i12] = a;
        int i13 = i12 + 1;
        C2491fm a2 = C4105zY.m41624a(AvatarSector.class, "a85e799abd7f81f6132565b6e02d130f", i13);
        f1263bO = a2;
        fmVarArr[i13] = a2;
        int i14 = i13 + 1;
        C2491fm a3 = C4105zY.m41624a(AvatarSector.class, "958737e5c9d6fb349f4352754d1cbd53", i14);
        _f_toString_0020_0028_0029Ljava_002flang_002fString_003b = a3;
        fmVarArr[i14] = a3;
        int i15 = i14 + 1;
        C2491fm a4 = C4105zY.m41624a(AvatarSector.class, "a8eb26fabb49188f8f76219c2faea3cf", i15);
        f1275zT = a4;
        fmVarArr[i15] = a4;
        int i16 = i15 + 1;
        C2491fm a5 = C4105zY.m41624a(AvatarSector.class, "9e76e875dd2b63635a3bdc6cb51ee8e7", i16);
        f1276zU = a5;
        fmVarArr[i16] = a5;
        int i17 = i16 + 1;
        C2491fm a6 = C4105zY.m41624a(AvatarSector.class, "8794d2945c770367afbfbc8c857b14ff", i17);
        dEe = a6;
        fmVarArr[i17] = a6;
        int i18 = i17 + 1;
        C2491fm a7 = C4105zY.m41624a(AvatarSector.class, "55581681ad83b2934bada36217c59e17", i18);
        dEf = a7;
        fmVarArr[i18] = a7;
        int i19 = i18 + 1;
        C2491fm a8 = C4105zY.m41624a(AvatarSector.class, "6d43d424e1d281b11622ef9dcf3f4b8d", i19);
        dEg = a8;
        fmVarArr[i19] = a8;
        int i20 = i19 + 1;
        C2491fm a9 = C4105zY.m41624a(AvatarSector.class, "9d9fb4e15279b8e138b1033f3b1bbdb3", i20);
        dEh = a9;
        fmVarArr[i20] = a9;
        int i21 = i20 + 1;
        C2491fm a10 = C4105zY.m41624a(AvatarSector.class, "3b4fe4d73f7a3f5f61302c919063fcb0", i21);
        dEi = a10;
        fmVarArr[i21] = a10;
        int i22 = i21 + 1;
        C2491fm a11 = C4105zY.m41624a(AvatarSector.class, "0a1590aeff2d52823747609b6a2cfb62", i22);
        dEj = a11;
        fmVarArr[i22] = a11;
        int i23 = i22 + 1;
        C2491fm a12 = C4105zY.m41624a(AvatarSector.class, "6233ac6ffabeba69102224a3b3bde834", i23);
        dEk = a12;
        fmVarArr[i23] = a12;
        int i24 = i23 + 1;
        C2491fm a13 = C4105zY.m41624a(AvatarSector.class, "ea1a9b0cef7897702d22e078a5564868", i24);
        dEl = a13;
        fmVarArr[i24] = a13;
        int i25 = i24 + 1;
        C2491fm a14 = C4105zY.m41624a(AvatarSector.class, "684af9480fdcdc6248c9084dbd16bcd6", i25);
        dEm = a14;
        fmVarArr[i25] = a14;
        int i26 = i25 + 1;
        C2491fm a15 = C4105zY.m41624a(AvatarSector.class, "fe1b0d9242d846125f469e0e9516d939", i26);
        dEn = a15;
        fmVarArr[i26] = a15;
        int i27 = i26 + 1;
        C2491fm a16 = C4105zY.m41624a(AvatarSector.class, "977ceda02677b0edc5857c94289ebc04", i27);
        cSO = a16;
        fmVarArr[i27] = a16;
        int i28 = i27 + 1;
        C2491fm a17 = C4105zY.m41624a(AvatarSector.class, "2e6d033c26600c3398c82f4919ad9d0c", i28);
        dEo = a17;
        fmVarArr[i28] = a17;
        int i29 = i28 + 1;
        C2491fm a18 = C4105zY.m41624a(AvatarSector.class, "7582bb9b07cda194be9d418023ecaa4b", i29);
        f1267ff = a18;
        fmVarArr[i29] = a18;
        int i30 = i29 + 1;
        C2491fm a19 = C4105zY.m41624a(AvatarSector.class, "8566381557a4d5a6db1b1314348c9ff9", i30);
        f1268fg = a19;
        fmVarArr[i30] = a19;
        int i31 = i30 + 1;
        C2491fm a20 = C4105zY.m41624a(AvatarSector.class, "1141936aaa8d3fdb117a36c77d9594a5", i31);
        dEp = a20;
        fmVarArr[i31] = a20;
        int i32 = i31 + 1;
        C2491fm a21 = C4105zY.m41624a(AvatarSector.class, "73d95d3153513df83d18877b01412f36", i32);
        f1264bP = a21;
        fmVarArr[i32] = a21;
        int i33 = i32 + 1;
        C2491fm a22 = C4105zY.m41624a(AvatarSector.class, "c2099a6334924e28be705f67334ad9bd", i33);
        f1265bQ = a22;
        fmVarArr[i33] = a22;
        int i34 = i33 + 1;
        _m_methods = fmVarArr;
        C1634Xv.m11725a((Object[]) aDJ._m_methods, (Object[]) _m_methods);
        ___iScriptClass = aUO.m18566a(AvatarSector.class, C1427Uu.class, _m_fields, _m_methods);
    }

    /* renamed from: S */
    private void m7745S(String str) {
        bFf().mo5608dq().mo3197f(f1274zS, str);
    }

    /* renamed from: a */
    private void m7747a(UUID uuid) {
        bFf().mo5608dq().mo3197f(f1261bL, uuid);
    }

    /* renamed from: an */
    private UUID m7748an() {
        return (UUID) bFf().mo5608dq().mo3214p(f1261bL);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, aYS = "Consistency", displayName = "Handle (Consistency checker)")
    @C0064Am(aul = "c2099a6334924e28be705f67334ad9bd", aum = 0)
    @C5566aOg
    /* renamed from: b */
    private void m7752b(String str) {
        throw new aWi(new aCE(this, f1265bQ, new Object[]{str}));
    }

    /* renamed from: bf */
    private String m7755bf() {
        return (String) bFf().mo5608dq().mo3214p(f1266eq);
    }

    private int biO() {
        return bFf().mo5608dq().mo3212n(dDX);
    }

    private C0955a biP() {
        return (C0955a) bFf().mo5608dq().mo3214p(dDZ);
    }

    private float biQ() {
        return bFf().mo5608dq().mo3211m(dEa);
    }

    private float biR() {
        return bFf().mo5608dq().mo3211m(dEb);
    }

    private float biS() {
        return bFf().mo5608dq().mo3211m(dEc);
    }

    private float biT() {
        return bFf().mo5608dq().mo3211m(dEd);
    }

    /* renamed from: c */
    private void m7756c(C0955a aVar) {
        bFf().mo5608dq().mo3197f(dDZ, aVar);
    }

    /* renamed from: c */
    private void m7757c(UUID uuid) {
        switch (bFf().mo6893i(f1263bO)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f1263bO, new Object[]{uuid}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f1263bO, new Object[]{uuid}));
                break;
        }
        m7753b(uuid);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, aYS = "Texture Section", displayName = "Texture to lookup")
    @C0064Am(aul = "9d9fb4e15279b8e138b1033f3b1bbdb3", aum = 0)
    @C5566aOg
    /* renamed from: d */
    private void m7758d(C0955a aVar) {
        throw new aWi(new aCE(this, dEh, new Object[]{aVar}));
    }

    /* renamed from: d */
    private void m7759d(I18NString i18NString) {
        bFf().mo5608dq().mo3197f(f1272zQ, i18NString);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Avatar Sector Name")
    @C0064Am(aul = "9e76e875dd2b63635a3bdc6cb51ee8e7", aum = 0)
    @C5566aOg
    /* renamed from: e */
    private void m7760e(I18NString i18NString) {
        throw new aWi(new aCE(this, f1276zU, new Object[]{i18NString}));
    }

    /* renamed from: f */
    private void m7761f(String str) {
        bFf().mo5608dq().mo3197f(f1266eq, str);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, aYS = "Geometry Section", displayName = "Prefix")
    @C0064Am(aul = "8566381557a4d5a6db1b1314348c9ff9", aum = 0)
    @C5566aOg
    /* renamed from: g */
    private void m7762g(String str) {
        throw new aWi(new aCE(this, f1268fg, new Object[]{str}));
    }

    /* renamed from: gn */
    private void m7763gn(float f) {
        bFf().mo5608dq().mo3150a(dEa, f);
    }

    /* renamed from: go */
    private void m7764go(float f) {
        bFf().mo5608dq().mo3150a(dEb, f);
    }

    /* renamed from: gp */
    private void m7765gp(float f) {
        bFf().mo5608dq().mo3150a(dEc, f);
    }

    /* renamed from: gq */
    private void m7766gq(float f) {
        bFf().mo5608dq().mo3150a(dEd, f);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, aYS = "Texture Section", displayName = "Texture Area offset (%) (x)")
    @C0064Am(aul = "0a1590aeff2d52823747609b6a2cfb62", aum = 0)
    @C5566aOg
    /* renamed from: gr */
    private void m7767gr(float f) {
        throw new aWi(new aCE(this, dEj, new Object[]{new Float(f)}));
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, aYS = "Texture Section", displayName = "Texture Area offset (%) (y)")
    @C0064Am(aul = "ea1a9b0cef7897702d22e078a5564868", aum = 0)
    @C5566aOg
    /* renamed from: gs */
    private void m7768gs(float f) {
        throw new aWi(new aCE(this, dEl, new Object[]{new Float(f)}));
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, aYS = "Texture Section", displayName = "Texture Area dimension (%) (width)")
    @C0064Am(aul = "fe1b0d9242d846125f469e0e9516d939", aum = 0)
    @C5566aOg
    /* renamed from: gt */
    private void m7769gt(float f) {
        throw new aWi(new aCE(this, dEn, new Object[]{new Float(f)}));
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, aYS = "Texture Section", displayName = "Texture Area dimension (%) (height)")
    @C0064Am(aul = "2e6d033c26600c3398c82f4919ad9d0c", aum = 0)
    @C5566aOg
    /* renamed from: gu */
    private void m7770gu(float f) {
        throw new aWi(new aCE(this, dEo, new Object[]{new Float(f)}));
    }

    /* renamed from: kb */
    private I18NString m7771kb() {
        return (I18NString) bFf().mo5608dq().mo3214p(f1272zQ);
    }

    /* renamed from: kc */
    private String m7772kc() {
        return (String) bFf().mo5608dq().mo3214p(f1274zS);
    }

    /* renamed from: lU */
    private void m7774lU(int i) {
        bFf().mo5608dq().mo3183b(dDX, i);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, aYS = "Geometry Section", displayName = "Mesh Sector Index (-1 for none)")
    @C0064Am(aul = "55581681ad83b2934bada36217c59e17", aum = 0)
    @C5566aOg
    /* renamed from: lV */
    private void m7775lV(int i) {
        throw new aWi(new aCE(this, dEf, new Object[]{new Integer(i)}));
    }

    /* renamed from: T */
    public C6494any mo11T() {
        return ___iScriptClass;
    }

    /* renamed from: U */
    public C2491fm[] mo12U() {
        return _m_methods;
    }

    /* renamed from: W */
    public Object mo13W() {
        return new C1427Uu(this);
    }

    /* renamed from: a */
    public Object mo14a(C0495Gr gr) {
        Object[] args = gr.getArgs();
        switch (gr.mo2417hq() - aDJ._m_methodCount) {
            case 0:
                return m7749ap();
            case 1:
                m7753b((UUID) args[0]);
                return null;
            case 2:
                return m7751au();
            case 3:
                return m7773kd();
            case 4:
                m7760e((I18NString) args[0]);
                return null;
            case 5:
                return new Integer(biU());
            case 6:
                m7775lV(((Integer) args[0]).intValue());
                return null;
            case 7:
                return biW();
            case 8:
                m7758d((C0955a) args[0]);
                return null;
            case 9:
                return new Float(biY());
            case 10:
                m7767gr(((Float) args[0]).floatValue());
                return null;
            case 11:
                return new Float(biZ());
            case 12:
                m7768gs(((Float) args[0]).floatValue());
                return null;
            case 13:
                return new Float(bja());
            case 14:
                m7769gt(((Float) args[0]).floatValue());
                return null;
            case 15:
                return new Float(aOB());
            case 16:
                m7770gu(((Float) args[0]).floatValue());
                return null;
            case 17:
                return m7754bD();
            case 18:
                m7762g((String) args[0]);
                return null;
            case 19:
                return bjb();
            case 20:
                return m7750ar();
            case 21:
                m7752b((String) args[0]);
                return null;
            default:
                return super.mo14a(gr);
        }
    }

    /* renamed from: a */
    public void mo15a(Collection collection, C0495Gr gr) {
        super.mo15a(collection, gr);
    }

    /* renamed from: aq */
    public UUID mo18aq() {
        switch (bFf().mo6893i(f1262bN)) {
            case 0:
                return null;
            case 2:
                return (UUID) bFf().mo5606d(new aCE(this, f1262bN, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f1262bN, new Object[0]));
                break;
        }
        return m7749ap();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Mesh Sector Index (-1 for none)")
    public int biV() {
        switch (bFf().mo6893i(dEe)) {
            case 0:
                return 0;
            case 2:
                return ((Integer) bFf().mo5606d(new aCE(this, dEe, new Object[0]))).intValue();
            case 3:
                bFf().mo5606d(new aCE(this, dEe, new Object[0]));
                break;
        }
        return biU();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, aYS = "Texture Section", displayName = "Texture to lookup")
    public C0955a biX() {
        switch (bFf().mo6893i(dEg)) {
            case 0:
                return null;
            case 2:
                return (C0955a) bFf().mo5606d(new aCE(this, dEg, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, dEg, new Object[0]));
                break;
        }
        return biW();
    }

    @ClientOnly
    public Rectangle2D.Float bjc() {
        switch (bFf().mo6893i(dEp)) {
            case 0:
                return null;
            case 2:
                return (Rectangle2D.Float) bFf().mo5606d(new aCE(this, dEp, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, dEp, new Object[0]));
                break;
        }
        return bjb();
    }

    /* renamed from: c */
    public C5663aRz[] mo25c() {
        return _m_fields;
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, aYS = "Texture Section", displayName = "Texture to lookup")
    @C5566aOg
    /* renamed from: e */
    public void mo4291e(C0955a aVar) {
        switch (bFf().mo6893i(dEh)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, dEh, new Object[]{aVar}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, dEh, new Object[]{aVar}));
                break;
        }
        m7758d(aVar);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Avatar Sector Name")
    @C5566aOg
    /* renamed from: f */
    public void mo4292f(I18NString i18NString) {
        switch (bFf().mo6893i(f1276zU)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f1276zU, new Object[]{i18NString}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f1276zU, new Object[]{i18NString}));
                break;
        }
        m7760e(i18NString);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, aYS = "Consistency", displayName = "Handle (Consistency checker)")
    public String getHandle() {
        switch (bFf().mo6893i(f1264bP)) {
            case 0:
                return null;
            case 2:
                return (String) bFf().mo5606d(new aCE(this, f1264bP, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f1264bP, new Object[0]));
                break;
        }
        return m7750ar();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, aYS = "Consistency", displayName = "Handle (Consistency checker)")
    @C5566aOg
    public void setHandle(String str) {
        switch (bFf().mo6893i(f1265bQ)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f1265bQ, new Object[]{str}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f1265bQ, new Object[]{str}));
                break;
        }
        m7752b(str);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, aYS = "Texture Section", displayName = "Texture Area dimension (%) (height)")
    public float getHeight() {
        switch (bFf().mo6893i(cSO)) {
            case 0:
                return 0.0f;
            case 2:
                return ((Float) bFf().mo5606d(new aCE(this, cSO, new Object[0]))).floatValue();
            case 3:
                bFf().mo5606d(new aCE(this, cSO, new Object[0]));
                break;
        }
        return aOB();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, aYS = "Texture Section", displayName = "Texture Area dimension (%) (height)")
    @C5566aOg
    public void setHeight(float f) {
        switch (bFf().mo6893i(dEo)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, dEo, new Object[]{new Float(f)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, dEo, new Object[]{new Float(f)}));
                break;
        }
        m7770gu(f);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, aYS = "Geometry Section", displayName = "Prefix")
    public String getPrefix() {
        switch (bFf().mo6893i(f1267ff)) {
            case 0:
                return null;
            case 2:
                return (String) bFf().mo5606d(new aCE(this, f1267ff, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f1267ff, new Object[0]));
                break;
        }
        return m7754bD();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, aYS = "Geometry Section", displayName = "Prefix")
    @C5566aOg
    public void setPrefix(String str) {
        switch (bFf().mo6893i(f1268fg)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f1268fg, new Object[]{str}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f1268fg, new Object[]{str}));
                break;
        }
        m7762g(str);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, aYS = "Texture Section", displayName = "Texture Area dimension (%) (width)")
    public float getWidth() {
        switch (bFf().mo6893i(dEm)) {
            case 0:
                return 0.0f;
            case 2:
                return ((Float) bFf().mo5606d(new aCE(this, dEm, new Object[0]))).floatValue();
            case 3:
                bFf().mo5606d(new aCE(this, dEm, new Object[0]));
                break;
        }
        return bja();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, aYS = "Texture Section", displayName = "Texture Area dimension (%) (width)")
    @C5566aOg
    public void setWidth(float f) {
        switch (bFf().mo6893i(dEn)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, dEn, new Object[]{new Float(f)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, dEn, new Object[]{new Float(f)}));
                break;
        }
        m7769gt(f);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, aYS = "Texture Section", displayName = "Texture Area offset (%) (x)")
    public float getX() {
        switch (bFf().mo6893i(dEi)) {
            case 0:
                return 0.0f;
            case 2:
                return ((Float) bFf().mo5606d(new aCE(this, dEi, new Object[0]))).floatValue();
            case 3:
                bFf().mo5606d(new aCE(this, dEi, new Object[0]));
                break;
        }
        return biY();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, aYS = "Texture Section", displayName = "Texture Area offset (%) (x)")
    @C5566aOg
    public void setX(float f) {
        switch (bFf().mo6893i(dEj)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, dEj, new Object[]{new Float(f)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, dEj, new Object[]{new Float(f)}));
                break;
        }
        m7767gr(f);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, aYS = "Texture Section", displayName = "Texture Area offset (%) (y)")
    public float getY() {
        switch (bFf().mo6893i(dEk)) {
            case 0:
                return 0.0f;
            case 2:
                return ((Float) bFf().mo5606d(new aCE(this, dEk, new Object[0]))).floatValue();
            case 3:
                bFf().mo5606d(new aCE(this, dEk, new Object[0]));
                break;
        }
        return biZ();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, aYS = "Texture Section", displayName = "Texture Area offset (%) (y)")
    @C5566aOg
    public void setY(float f) {
        switch (bFf().mo6893i(dEl)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, dEl, new Object[]{new Float(f)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, dEl, new Object[]{new Float(f)}));
                break;
        }
        m7768gs(f);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Avatar Sector Name")
    /* renamed from: ke */
    public I18NString mo4298ke() {
        switch (bFf().mo6893i(f1275zT)) {
            case 0:
                return null;
            case 2:
                return (I18NString) bFf().mo5606d(new aCE(this, f1275zT, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f1275zT, new Object[0]));
                break;
        }
        return m7773kd();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, aYS = "Geometry Section", displayName = "Mesh Sector Index (-1 for none)")
    @C5566aOg
    /* renamed from: lW */
    public void mo4299lW(int i) {
        switch (bFf().mo6893i(dEf)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, dEf, new Object[]{new Integer(i)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, dEf, new Object[]{new Integer(i)}));
                break;
        }
        m7775lV(i);
    }

    public String toString() {
        switch (bFf().mo6893i(_f_toString_0020_0028_0029Ljava_002flang_002fString_003b)) {
            case 0:
                return null;
            case 2:
                return (String) bFf().mo5606d(new aCE(this, _f_toString_0020_0028_0029Ljava_002flang_002fString_003b, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, _f_toString_0020_0028_0029Ljava_002flang_002fString_003b, new Object[0]));
                break;
        }
        return m7751au();
    }

    @C0064Am(aul = "943fab63343092e51e2ccf3c4e6cf218", aum = 0)
    /* renamed from: ap */
    private UUID m7749ap() {
        return m7748an();
    }

    @C0064Am(aul = "a85e799abd7f81f6132565b6e02d130f", aum = 0)
    /* renamed from: b */
    private void m7753b(UUID uuid) {
        m7747a(uuid);
    }

    /* renamed from: S */
    public void mo10S() {
        super.mo10S();
        m7756c(C0955a.HEAD);
        m7747a(UUID.randomUUID());
    }

    @C0064Am(aul = "958737e5c9d6fb349f4352754d1cbd53", aum = 0)
    /* renamed from: au */
    private String m7751au() {
        return "[" + biP().toString() + "] - " + biO() + " - " + m7771kb().get();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Avatar Sector Name")
    @C0064Am(aul = "a8eb26fabb49188f8f76219c2faea3cf", aum = 0)
    /* renamed from: kd */
    private I18NString m7773kd() {
        return m7771kb();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Mesh Sector Index (-1 for none)")
    @C0064Am(aul = "8794d2945c770367afbfbc8c857b14ff", aum = 0)
    private int biU() {
        return biO();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, aYS = "Texture Section", displayName = "Texture to lookup")
    @C0064Am(aul = "6d43d424e1d281b11622ef9dcf3f4b8d", aum = 0)
    private C0955a biW() {
        return biP();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, aYS = "Texture Section", displayName = "Texture Area offset (%) (x)")
    @C0064Am(aul = "3b4fe4d73f7a3f5f61302c919063fcb0", aum = 0)
    private float biY() {
        return biQ();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, aYS = "Texture Section", displayName = "Texture Area offset (%) (y)")
    @C0064Am(aul = "6233ac6ffabeba69102224a3b3bde834", aum = 0)
    private float biZ() {
        return biR();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, aYS = "Texture Section", displayName = "Texture Area dimension (%) (width)")
    @C0064Am(aul = "684af9480fdcdc6248c9084dbd16bcd6", aum = 0)
    private float bja() {
        return biS();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, aYS = "Texture Section", displayName = "Texture Area dimension (%) (height)")
    @C0064Am(aul = "977ceda02677b0edc5857c94289ebc04", aum = 0)
    private float aOB() {
        return biT();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, aYS = "Geometry Section", displayName = "Prefix")
    @C0064Am(aul = "7582bb9b07cda194be9d418023ecaa4b", aum = 0)
    /* renamed from: bD */
    private String m7754bD() {
        return m7755bf();
    }

    @C0064Am(aul = "1141936aaa8d3fdb117a36c77d9594a5", aum = 0)
    @ClientOnly
    private Rectangle2D.Float bjb() {
        return new Rectangle2D.Float(biQ(), biR(), biS(), biT());
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, aYS = "Consistency", displayName = "Handle (Consistency checker)")
    @C0064Am(aul = "73d95d3153513df83d18877b01412f36", aum = 0)
    /* renamed from: ar */
    private String m7750ar() {
        return m7772kc();
    }

    /* renamed from: a.Nw$a */
    public enum C0955a {
        HEAD("_head"),
        BODY("_body"),
        HAIR("_hair"),
        OTHER("_other");

        private final String suffix;

        private C0955a(String str) {
            this.suffix = str;
        }

        public String getSuffix() {
            return this.suffix;
        }
    }
}
