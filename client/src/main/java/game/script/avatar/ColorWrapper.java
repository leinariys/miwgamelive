package game.script.avatar;

import game.network.message.externalizable.aCE;
import logic.baa.*;
import logic.data.mbean.axJ;
import logic.res.code.C5640aRc;
import logic.res.code.C5663aRz;
import logic.res.code.C6494any;
import logic.res.html.C2491fm;
import p001a.*;
import taikodom.infra.script.I18NString;

import java.awt.*;
import java.util.Collection;
import java.util.UUID;

@C5511aMd
@C6485anp
/* renamed from: a.apf  reason: case insensitive filesystem */
/* compiled from: a */
public class ColorWrapper extends aDJ implements C0468GU, C1616Xf {
    public static final C2491fm _f_toString_0020_0028_0029Ljava_002flang_002fString_003b = null;
    public static final int _m_fieldCount = 0;
    public static final C5663aRz[] _m_fields = null;
    public static final int _m_methodCount = 0;
    public static final C2491fm[] _m_methods = null;
    /* renamed from: bL */
    public static final C5663aRz f5125bL = null;
    /* renamed from: bN */
    public static final C2491fm f5126bN = null;
    /* renamed from: bO */
    public static final C2491fm f5127bO = null;
    /* renamed from: bP */
    public static final C2491fm f5128bP = null;
    /* renamed from: bQ */
    public static final C2491fm f5129bQ = null;
    public static final C5663aRz eqe = null;
    public static final C2491fm glA = null;
    public static final C2491fm glB = null;
    public static final C2491fm gls = null;
    public static final C2491fm glt = null;
    public static final C2491fm glv = null;
    public static final C2491fm glw = null;
    public static final C2491fm glx = null;
    public static final C2491fm gly = null;
    public static final C2491fm glz = null;
    public static final long serialVersionUID = 0;
    /* renamed from: zQ */
    public static final C5663aRz f5131zQ = null;
    /* renamed from: zS */
    public static final C5663aRz f5133zS = null;
    /* renamed from: zT */
    public static final C2491fm f5134zT = null;
    /* renamed from: zU */
    public static final C2491fm f5135zU = null;
    public static C6494any ___iScriptClass;
    @C0064Am(aul = "98f321e8ffced1aa00e7de9d81d23afe", aum = 3)

    /* renamed from: bK */
    private static UUID f5124bK;
    @C0064Am(aul = "1371daa548ebd59d76a1b6476fbe0788", aum = 1)
    private static int glr;
    @C0064Am(aul = "caf04646fc5316f2a29d44cd3ec7b237", aum = 0)

    /* renamed from: zP */
    private static I18NString f5130zP;
    @C0064Am(aul = "4536d12629045849ca7811d0645daabe", aum = 2)

    /* renamed from: zR */
    private static String f5132zR;

    static {
        m24958V();
    }

    public ColorWrapper() {
        super((C5540aNg) null);
        super.mo10S();
    }

    public ColorWrapper(C5540aNg ang) {
        super(ang);
    }

    /* renamed from: V */
    static void m24958V() {
        serialVersionUID = (long) 1;
        _m_fieldCount = aDJ._m_fieldCount + 4;
        _m_methodCount = aDJ._m_methodCount + 16;
        int i = aDJ._m_fieldCount;
        C5663aRz[] arzArr = new C5663aRz[(i + 4)];
        C5663aRz b = C5640aRc.m17844b(ColorWrapper.class, "caf04646fc5316f2a29d44cd3ec7b237", i);
        f5131zQ = b;
        arzArr[i] = b;
        int i2 = i + 1;
        C5663aRz b2 = C5640aRc.m17844b(ColorWrapper.class, "1371daa548ebd59d76a1b6476fbe0788", i2);
        eqe = b2;
        arzArr[i2] = b2;
        int i3 = i2 + 1;
        C5663aRz b3 = C5640aRc.m17844b(ColorWrapper.class, "4536d12629045849ca7811d0645daabe", i3);
        f5133zS = b3;
        arzArr[i3] = b3;
        int i4 = i3 + 1;
        C5663aRz b4 = C5640aRc.m17844b(ColorWrapper.class, "98f321e8ffced1aa00e7de9d81d23afe", i4);
        f5125bL = b4;
        arzArr[i4] = b4;
        int i5 = i4 + 1;
        _m_fields = arzArr;
        C1634Xv.m11725a((Object[]) aDJ._m_fields, (Object[]) _m_fields);
        int i6 = aDJ._m_methodCount;
        C2491fm[] fmVarArr = new C2491fm[(i6 + 16)];
        C2491fm a = C4105zY.m41624a(ColorWrapper.class, "ce1d74a58f95b492d8c4fd33ea847fa1", i6);
        f5126bN = a;
        fmVarArr[i6] = a;
        int i7 = i6 + 1;
        C2491fm a2 = C4105zY.m41624a(ColorWrapper.class, "a8312642b68c3806994a7e0eae6e4a67", i7);
        f5127bO = a2;
        fmVarArr[i7] = a2;
        int i8 = i7 + 1;
        C2491fm a3 = C4105zY.m41624a(ColorWrapper.class, "b5407628d166b1d5e974d2322996079a", i8);
        _f_toString_0020_0028_0029Ljava_002flang_002fString_003b = a3;
        fmVarArr[i8] = a3;
        int i9 = i8 + 1;
        C2491fm a4 = C4105zY.m41624a(ColorWrapper.class, "409c83117a20424d1a8ad0d1cc5eadd4", i9);
        f5134zT = a4;
        fmVarArr[i9] = a4;
        int i10 = i9 + 1;
        C2491fm a5 = C4105zY.m41624a(ColorWrapper.class, "33c36b580da4a9f26d4d79e49fc91021", i10);
        f5135zU = a5;
        fmVarArr[i10] = a5;
        int i11 = i10 + 1;
        C2491fm a6 = C4105zY.m41624a(ColorWrapper.class, "b18b12aa7ca64ab5589a59d6350f4b96", i11);
        gls = a6;
        fmVarArr[i11] = a6;
        int i12 = i11 + 1;
        C2491fm a7 = C4105zY.m41624a(ColorWrapper.class, "7041df0cc2c0005da66d86a28fc41c56", i12);
        glt = a7;
        fmVarArr[i12] = a7;
        int i13 = i12 + 1;
        C2491fm a8 = C4105zY.m41624a(ColorWrapper.class, "979c3fccc12f534faffe6aad69131254", i13);
        glv = a8;
        fmVarArr[i13] = a8;
        int i14 = i13 + 1;
        C2491fm a9 = C4105zY.m41624a(ColorWrapper.class, "4b28c2475bfc5e87a91551b23ca16a34", i14);
        glw = a9;
        fmVarArr[i14] = a9;
        int i15 = i14 + 1;
        C2491fm a10 = C4105zY.m41624a(ColorWrapper.class, "4216ae7d7f255e6984b571b1f8ed8db6", i15);
        glx = a10;
        fmVarArr[i15] = a10;
        int i16 = i15 + 1;
        C2491fm a11 = C4105zY.m41624a(ColorWrapper.class, "678766e029564535ec96aadf1c48ba12", i16);
        gly = a11;
        fmVarArr[i16] = a11;
        int i17 = i16 + 1;
        C2491fm a12 = C4105zY.m41624a(ColorWrapper.class, "87718bdfb280e18540cf6ccfb39a25a8", i17);
        glz = a12;
        fmVarArr[i17] = a12;
        int i18 = i17 + 1;
        C2491fm a13 = C4105zY.m41624a(ColorWrapper.class, "aa6c3f0da052388e3912af7a86c4b839", i18);
        glA = a13;
        fmVarArr[i18] = a13;
        int i19 = i18 + 1;
        C2491fm a14 = C4105zY.m41624a(ColorWrapper.class, "f546bca58cb6178aff2027763f6317ba", i19);
        glB = a14;
        fmVarArr[i19] = a14;
        int i20 = i19 + 1;
        C2491fm a15 = C4105zY.m41624a(ColorWrapper.class, "d3a8119d2506cd03bf887680c1ef9a62", i20);
        f5128bP = a15;
        fmVarArr[i20] = a15;
        int i21 = i20 + 1;
        C2491fm a16 = C4105zY.m41624a(ColorWrapper.class, "085b32e2e1fe2f3764aded89d7a1ff54", i21);
        f5129bQ = a16;
        fmVarArr[i21] = a16;
        int i22 = i21 + 1;
        _m_methods = fmVarArr;
        C1634Xv.m11725a((Object[]) aDJ._m_methods, (Object[]) _m_methods);
        ___iScriptClass = aUO.m18566a(ColorWrapper.class, axJ.class, _m_fields, _m_methods);
    }

    /* renamed from: S */
    private void m24957S(String str) {
        bFf().mo5608dq().mo3197f(f5133zS, str);
    }

    /* renamed from: a */
    private void m24959a(UUID uuid) {
        bFf().mo5608dq().mo3197f(f5125bL, uuid);
    }

    /* renamed from: an */
    private UUID m24960an() {
        return (UUID) bFf().mo5608dq().mo3214p(f5125bL);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, aYS = "Consistency", displayName = "Handle (Consistency checker)")
    @C0064Am(aul = "085b32e2e1fe2f3764aded89d7a1ff54", aum = 0)
    @C5566aOg
    /* renamed from: b */
    private void m24964b(String str) {
        throw new aWi(new aCE(this, f5129bQ, new Object[]{str}));
    }

    /* renamed from: c */
    private void m24966c(UUID uuid) {
        switch (bFf().mo6893i(f5127bO)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f5127bO, new Object[]{uuid}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f5127bO, new Object[]{uuid}));
                break;
        }
        m24965b(uuid);
    }

    private int cph() {
        return bFf().mo5608dq().mo3212n(eqe);
    }

    /* renamed from: d */
    private void m24967d(I18NString i18NString) {
        bFf().mo5608dq().mo3197f(f5131zQ, i18NString);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Name")
    @C0064Am(aul = "33c36b580da4a9f26d4d79e49fc91021", aum = 0)
    @C5566aOg
    /* renamed from: e */
    private void m24968e(I18NString i18NString) {
        throw new aWi(new aCE(this, f5135zU, new Object[]{i18NString}));
    }

    /* renamed from: kb */
    private I18NString m24969kb() {
        return (I18NString) bFf().mo5608dq().mo3214p(f5131zQ);
    }

    /* renamed from: kc */
    private String m24970kc() {
        return (String) bFf().mo5608dq().mo3214p(f5133zS);
    }

    /* renamed from: tm */
    private void m24972tm(int i) {
        bFf().mo5608dq().mo3183b(eqe, i);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Color Component 1 (Red)")
    @C0064Am(aul = "7041df0cc2c0005da66d86a28fc41c56", aum = 0)
    @C5566aOg
    /* renamed from: tn */
    private void m24973tn(int i) {
        throw new aWi(new aCE(this, glt, new Object[]{new Integer(i)}));
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Color Component 2 (Green)")
    @C0064Am(aul = "4b28c2475bfc5e87a91551b23ca16a34", aum = 0)
    @C5566aOg
    /* renamed from: to */
    private void m24974to(int i) {
        throw new aWi(new aCE(this, glw, new Object[]{new Integer(i)}));
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Color Component 3 (Blue)")
    @C0064Am(aul = "678766e029564535ec96aadf1c48ba12", aum = 0)
    @C5566aOg
    /* renamed from: tp */
    private void m24975tp(int i) {
        throw new aWi(new aCE(this, gly, new Object[]{new Integer(i)}));
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Color Component 4 (Alpha)")
    @C0064Am(aul = "aa6c3f0da052388e3912af7a86c4b839", aum = 0)
    @C5566aOg
    /* renamed from: tq */
    private void m24976tq(int i) {
        throw new aWi(new aCE(this, glA, new Object[]{new Integer(i)}));
    }

    /* renamed from: T */
    public C6494any mo11T() {
        return ___iScriptClass;
    }

    /* renamed from: U */
    public C2491fm[] mo12U() {
        return _m_methods;
    }

    /* renamed from: W */
    public Object mo13W() {
        return new axJ(this);
    }

    /* renamed from: a */
    public Object mo14a(C0495Gr gr) {
        Object[] args = gr.getArgs();
        switch (gr.mo2417hq() - aDJ._m_methodCount) {
            case 0:
                return m24961ap();
            case 1:
                m24965b((UUID) args[0]);
                return null;
            case 2:
                return m24963au();
            case 3:
                return m24971kd();
            case 4:
                m24968e((I18NString) args[0]);
                return null;
            case 5:
                return new Integer(cpi());
            case 6:
                m24973tn(((Integer) args[0]).intValue());
                return null;
            case 7:
                return new Integer(cpj());
            case 8:
                m24974to(((Integer) args[0]).intValue());
                return null;
            case 9:
                return new Integer(cpk());
            case 10:
                m24975tp(((Integer) args[0]).intValue());
                return null;
            case 11:
                return new Integer(cpl());
            case 12:
                m24976tq(((Integer) args[0]).intValue());
                return null;
            case 13:
                return cpm();
            case 14:
                return m24962ar();
            case 15:
                m24964b((String) args[0]);
                return null;
            default:
                return super.mo14a(gr);
        }
    }

    /* renamed from: a */
    public void mo15a(Collection collection, C0495Gr gr) {
        super.mo15a(collection, gr);
    }

    /* renamed from: aq */
    public UUID mo18aq() {
        switch (bFf().mo6893i(f5126bN)) {
            case 0:
                return null;
            case 2:
                return (UUID) bFf().mo5606d(new aCE(this, f5126bN, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f5126bN, new Object[0]));
                break;
        }
        return m24961ap();
    }

    /* renamed from: c */
    public C5663aRz[] mo25c() {
        return _m_fields;
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Name")
    @C5566aOg
    /* renamed from: f */
    public void mo15446f(I18NString i18NString) {
        switch (bFf().mo6893i(f5135zU)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f5135zU, new Object[]{i18NString}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f5135zU, new Object[]{i18NString}));
                break;
        }
        m24968e(i18NString);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, aYS = "Color", displayName = "Color Component 4 (Alpha)")
    public int getAlpha() {
        switch (bFf().mo6893i(glz)) {
            case 0:
                return 0;
            case 2:
                return ((Integer) bFf().mo5606d(new aCE(this, glz, new Object[0]))).intValue();
            case 3:
                bFf().mo5606d(new aCE(this, glz, new Object[0]));
                break;
        }
        return cpl();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Color Component 4 (Alpha)")
    @C5566aOg
    public void setAlpha(int i) {
        switch (bFf().mo6893i(glA)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, glA, new Object[]{new Integer(i)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, glA, new Object[]{new Integer(i)}));
                break;
        }
        m24976tq(i);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, aYS = "Color", displayName = "Color Component 3 (Blue)")
    public int getBlue() {
        switch (bFf().mo6893i(glx)) {
            case 0:
                return 0;
            case 2:
                return ((Integer) bFf().mo5606d(new aCE(this, glx, new Object[0]))).intValue();
            case 3:
                bFf().mo5606d(new aCE(this, glx, new Object[0]));
                break;
        }
        return cpk();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Color Component 3 (Blue)")
    @C5566aOg
    public void setBlue(int i) {
        switch (bFf().mo6893i(gly)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, gly, new Object[]{new Integer(i)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, gly, new Object[]{new Integer(i)}));
                break;
        }
        m24975tp(i);
    }

    @ClientOnly
    public Color getColor() {
        switch (bFf().mo6893i(glB)) {
            case 0:
                return null;
            case 2:
                return (Color) bFf().mo5606d(new aCE(this, glB, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, glB, new Object[0]));
                break;
        }
        return cpm();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, aYS = "Color", displayName = "Color Component 2 (Green)")
    public int getGreen() {
        switch (bFf().mo6893i(glv)) {
            case 0:
                return 0;
            case 2:
                return ((Integer) bFf().mo5606d(new aCE(this, glv, new Object[0]))).intValue();
            case 3:
                bFf().mo5606d(new aCE(this, glv, new Object[0]));
                break;
        }
        return cpj();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Color Component 2 (Green)")
    @C5566aOg
    public void setGreen(int i) {
        switch (bFf().mo6893i(glw)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, glw, new Object[]{new Integer(i)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, glw, new Object[]{new Integer(i)}));
                break;
        }
        m24974to(i);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, aYS = "Consistency", displayName = "Handle (Consistency checker)")
    public String getHandle() {
        switch (bFf().mo6893i(f5128bP)) {
            case 0:
                return null;
            case 2:
                return (String) bFf().mo5606d(new aCE(this, f5128bP, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f5128bP, new Object[0]));
                break;
        }
        return m24962ar();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, aYS = "Consistency", displayName = "Handle (Consistency checker)")
    @C5566aOg
    public void setHandle(String str) {
        switch (bFf().mo6893i(f5129bQ)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f5129bQ, new Object[]{str}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f5129bQ, new Object[]{str}));
                break;
        }
        m24964b(str);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, aYS = "Color", displayName = "Color Component 1 (Red)")
    public int getRed() {
        switch (bFf().mo6893i(gls)) {
            case 0:
                return 0;
            case 2:
                return ((Integer) bFf().mo5606d(new aCE(this, gls, new Object[0]))).intValue();
            case 3:
                bFf().mo5606d(new aCE(this, gls, new Object[0]));
                break;
        }
        return cpi();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Color Component 1 (Red)")
    @C5566aOg
    public void setRed(int i) {
        switch (bFf().mo6893i(glt)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, glt, new Object[]{new Integer(i)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, glt, new Object[]{new Integer(i)}));
                break;
        }
        m24973tn(i);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Name")
    /* renamed from: ke */
    public I18NString mo15452ke() {
        switch (bFf().mo6893i(f5134zT)) {
            case 0:
                return null;
            case 2:
                return (I18NString) bFf().mo5606d(new aCE(this, f5134zT, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f5134zT, new Object[0]));
                break;
        }
        return m24971kd();
    }

    public String toString() {
        switch (bFf().mo6893i(_f_toString_0020_0028_0029Ljava_002flang_002fString_003b)) {
            case 0:
                return null;
            case 2:
                return (String) bFf().mo5606d(new aCE(this, _f_toString_0020_0028_0029Ljava_002flang_002fString_003b, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, _f_toString_0020_0028_0029Ljava_002flang_002fString_003b, new Object[0]));
                break;
        }
        return m24963au();
    }

    @C0064Am(aul = "ce1d74a58f95b492d8c4fd33ea847fa1", aum = 0)
    /* renamed from: ap */
    private UUID m24961ap() {
        return m24960an();
    }

    @C0064Am(aul = "a8312642b68c3806994a7e0eae6e4a67", aum = 0)
    /* renamed from: b */
    private void m24965b(UUID uuid) {
        m24959a(uuid);
    }

    /* renamed from: S */
    public void mo10S() {
        super.mo10S();
        m24959a(UUID.randomUUID());
    }

    @C0064Am(aul = "b5407628d166b1d5e974d2322996079a", aum = 0)
    /* renamed from: au */
    private String m24963au() {
        if (m24969kb() == null) {
            return null;
        }
        return String.valueOf(mo15452ke().get()) + "[" + getRed() + ", " + getGreen() + ", " + getBlue() + ", " + getAlpha() + "] - ";
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Name")
    @C0064Am(aul = "409c83117a20424d1a8ad0d1cc5eadd4", aum = 0)
    /* renamed from: kd */
    private I18NString m24971kd() {
        return m24969kb();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, aYS = "Color", displayName = "Color Component 1 (Red)")
    @C0064Am(aul = "b18b12aa7ca64ab5589a59d6350f4b96", aum = 0)
    private int cpi() {
        return (cph() >> 24) & 255;
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, aYS = "Color", displayName = "Color Component 2 (Green)")
    @C0064Am(aul = "979c3fccc12f534faffe6aad69131254", aum = 0)
    private int cpj() {
        return (cph() >> 16) & 255;
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, aYS = "Color", displayName = "Color Component 3 (Blue)")
    @C0064Am(aul = "4216ae7d7f255e6984b571b1f8ed8db6", aum = 0)
    private int cpk() {
        return (cph() >> 8) & 255;
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, aYS = "Color", displayName = "Color Component 4 (Alpha)")
    @C0064Am(aul = "87718bdfb280e18540cf6ccfb39a25a8", aum = 0)
    private int cpl() {
        return cph() & 255;
    }

    @C0064Am(aul = "f546bca58cb6178aff2027763f6317ba", aum = 0)
    @ClientOnly
    private Color cpm() {
        return new Color(getRed(), getGreen(), getBlue(), getAlpha());
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, aYS = "Consistency", displayName = "Handle (Consistency checker)")
    @C0064Am(aul = "d3a8119d2506cd03bf887680c1ef9a62", aum = 0)
    /* renamed from: ar */
    private String m24962ar() {
        return m24970kc();
    }
}
