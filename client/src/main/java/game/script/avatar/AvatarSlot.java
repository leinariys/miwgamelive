package game.script.avatar;

import game.network.message.externalizable.aCE;
import game.script.item.Item;
import game.script.item.ItemLocation;
import logic.baa.C1616Xf;
import logic.baa.C1634Xv;
import logic.baa.C5511aMd;
import logic.baa.C6485anp;
import logic.data.mbean.C0105BK;
import logic.res.code.C5640aRc;
import logic.res.code.C5663aRz;
import logic.res.code.C6494any;
import logic.res.html.C2491fm;
import p001a.*;

import java.util.Collection;

@C5511aMd
@C6485anp
/* renamed from: a.auK  reason: case insensitive filesystem */
/* compiled from: a */
public class AvatarSlot extends ItemLocation implements C1616Xf {

    /* renamed from: RA */
    public static final C2491fm f5368RA = null;

    /* renamed from: RD */
    public static final C2491fm f5369RD = null;
    public static final int _m_fieldCount = 0;
    public static final C5663aRz[] _m_fields = null;
    public static final int _m_methodCount = 0;
    public static final C2491fm[] _m_methods = null;
    public static final C5663aRz gEU = null;
    public static final C5663aRz gEV = null;
    /* renamed from: ku */
    public static final C2491fm f5370ku = null;
    public static final long serialVersionUID = 0;
    public static C6494any ___iScriptClass;
    @C0064Am(aul = "532b9b7fc3d7a3104b81c8d2d0426f93", aum = 0)
    private static String cqe;
    @C0064Am(aul = "cba6a0d79fe58cff9c4a052845906102", aum = 1)
    private static int cqf;

    static {
        m26223V();
    }

    public AvatarSlot() {
        super((C5540aNg) null);
        super.mo10S();
    }

    public AvatarSlot(C5540aNg ang) {
        super(ang);
    }

    /* renamed from: V */
    static void m26223V() {
        serialVersionUID = (long) 1;
        _m_fieldCount = ItemLocation._m_fieldCount + 2;
        _m_methodCount = ItemLocation._m_methodCount + 3;
        int i = ItemLocation._m_fieldCount;
        C5663aRz[] arzArr = new C5663aRz[(i + 2)];
        C5663aRz b = C5640aRc.m17844b(AvatarSlot.class, "532b9b7fc3d7a3104b81c8d2d0426f93", i);
        gEU = b;
        arzArr[i] = b;
        int i2 = i + 1;
        C5663aRz b2 = C5640aRc.m17844b(AvatarSlot.class, "cba6a0d79fe58cff9c4a052845906102", i2);
        gEV = b2;
        arzArr[i2] = b2;
        int i3 = i2 + 1;
        _m_fields = arzArr;
        C1634Xv.m11725a((Object[]) ItemLocation._m_fields, (Object[]) _m_fields);
        int i4 = ItemLocation._m_methodCount;
        C2491fm[] fmVarArr = new C2491fm[(i4 + 3)];
        C2491fm a = C4105zY.m41624a(AvatarSlot.class, "aa2be5b4d5d893b0d3ae453af40f47c0", i4);
        f5368RA = a;
        fmVarArr[i4] = a;
        int i5 = i4 + 1;
        C2491fm a2 = C4105zY.m41624a(AvatarSlot.class, "326817309018ba5800af1bcf1a9d18bc", i5);
        f5369RD = a2;
        fmVarArr[i5] = a2;
        int i6 = i5 + 1;
        C2491fm a3 = C4105zY.m41624a(AvatarSlot.class, "6559b14a0166ac096b6cca23b172aa69", i6);
        f5370ku = a3;
        fmVarArr[i6] = a3;
        int i7 = i6 + 1;
        _m_methods = fmVarArr;
        C1634Xv.m11725a((Object[]) ItemLocation._m_methods, (Object[]) _m_methods);
        ___iScriptClass = aUO.m18566a(AvatarSlot.class, C0105BK.class, _m_fields, _m_methods);
    }

    private String cyh() {
        return (String) bFf().mo5608dq().mo3214p(gEU);
    }

    private int cyi() {
        return bFf().mo5608dq().mo3212n(gEV);
    }

    /* renamed from: kg */
    private void m26226kg(String str) {
        bFf().mo5608dq().mo3197f(gEU, str);
    }

    /* renamed from: uJ */
    private void m26228uJ(int i) {
        bFf().mo5608dq().mo3183b(gEV, i);
    }

    /* renamed from: T */
    public C6494any mo11T() {
        return ___iScriptClass;
    }

    /* renamed from: U */
    public C2491fm[] mo12U() {
        return _m_methods;
    }

    /* renamed from: W */
    public Object mo13W() {
        return new C0105BK(this);
    }

    /* renamed from: a */
    public Object mo14a(C0495Gr gr) {
        Object[] args = gr.getArgs();
        switch (gr.mo2417hq() - ItemLocation._m_methodCount) {
            case 0:
                m26224a((Item) args[0], (ItemLocation) args[1]);
                return null;
            case 1:
                m26227m((Item) args[0]);
                return null;
            case 2:
                m26225g((Item) args[0]);
                return null;
            default:
                return super.mo14a(gr);
        }
    }

    /* renamed from: a */
    public void mo15a(Collection collection, C0495Gr gr) {
        super.mo15a(collection, gr);
    }

    /* access modifiers changed from: protected */
    /* renamed from: b */
    public void mo1887b(Item auq, ItemLocation aag) {
        switch (bFf().mo6893i(f5368RA)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f5368RA, new Object[]{auq, aag}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f5368RA, new Object[]{auq, aag}));
                break;
        }
        m26224a(auq, aag);
    }

    /* renamed from: c */
    public C5663aRz[] mo25c() {
        return _m_fields;
    }

    /* access modifiers changed from: protected */
    /* renamed from: h */
    public void mo1889h(Item auq) {
        switch (bFf().mo6893i(f5370ku)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f5370ku, new Object[]{auq}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f5370ku, new Object[]{auq}));
                break;
        }
        m26225g(auq);
    }

    /* access modifiers changed from: protected */
    /* renamed from: n */
    public void mo1890n(Item auq) {
        switch (bFf().mo6893i(f5369RD)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f5369RD, new Object[]{auq}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f5369RD, new Object[]{auq}));
                break;
        }
        m26227m(auq);
    }

    /* renamed from: S */
    public void mo10S() {
        super.mo10S();
    }

    @C0064Am(aul = "aa2be5b4d5d893b0d3ae453af40f47c0", aum = 0)
    /* renamed from: a */
    private void m26224a(Item auq, ItemLocation aag) {
    }

    @C0064Am(aul = "326817309018ba5800af1bcf1a9d18bc", aum = 0)
    /* renamed from: m */
    private void m26227m(Item auq) {
    }

    @C0064Am(aul = "6559b14a0166ac096b6cca23b172aa69", aum = 0)
    /* renamed from: g */
    private void m26225g(Item auq) {
    }
}
