package game.script.avatar.cloth;

import game.network.message.externalizable.aCE;
import logic.baa.*;
import logic.data.mbean.C4134zy;
import logic.res.code.C5640aRc;
import logic.res.code.C5663aRz;
import logic.res.code.C6494any;
import logic.res.html.C2491fm;
import p001a.*;
import taikodom.infra.script.I18NString;

import java.util.Collection;
import java.util.Set;
import java.util.UUID;

@C5511aMd
@C6485anp
/* renamed from: a.dH */
/* compiled from: a */
public class ClothCategory extends aDJ implements C0468GU, C1616Xf {
    public static final C2491fm _f_toString_0020_0028_0029Ljava_002flang_002fString_003b = null;
    public static final int _m_fieldCount = 0;
    public static final C5663aRz[] _m_fields = null;
    public static final int _m_methodCount = 0;
    public static final C2491fm[] _m_methods = null;
    /* renamed from: bL */
    public static final C5663aRz f6446bL = null;
    /* renamed from: bN */
    public static final C2491fm f6447bN = null;
    /* renamed from: bO */
    public static final C2491fm f6448bO = null;
    /* renamed from: bP */
    public static final C2491fm f6449bP = null;
    /* renamed from: bQ */
    public static final C2491fm f6450bQ = null;
    public static final long serialVersionUID = 0;
    /* renamed from: zQ */
    public static final C5663aRz f6452zQ = null;
    /* renamed from: zS */
    public static final C5663aRz f6454zS = null;
    /* renamed from: zT */
    public static final C2491fm f6455zT = null;
    /* renamed from: zU */
    public static final C2491fm f6456zU = null;
    /* renamed from: zV */
    public static final C2491fm f6457zV = null;
    public static C6494any ___iScriptClass;
    @C0064Am(aul = "8fc9387dda10d94ba30e1726099636d4", aum = 2)

    /* renamed from: bK */
    private static UUID f6445bK;
    @C0064Am(aul = "ff123354afa3896656b487747ad1c92c", aum = 0)

    /* renamed from: zP */
    private static I18NString f6451zP;
    @C0064Am(aul = "d911675436c735ecb77e1cca9b118dff", aum = 1)

    /* renamed from: zR */
    private static String f6453zR;

    static {
        m28751V();
    }

    public ClothCategory() {
        super((C5540aNg) null);
        super.mo10S();
    }

    public ClothCategory(C5540aNg ang) {
        super(ang);
    }

    /* renamed from: V */
    static void m28751V() {
        serialVersionUID = (long) 1;
        _m_fieldCount = aDJ._m_fieldCount + 3;
        _m_methodCount = aDJ._m_methodCount + 8;
        int i = aDJ._m_fieldCount;
        C5663aRz[] arzArr = new C5663aRz[(i + 3)];
        C5663aRz b = C5640aRc.m17844b(ClothCategory.class, "ff123354afa3896656b487747ad1c92c", i);
        f6452zQ = b;
        arzArr[i] = b;
        int i2 = i + 1;
        C5663aRz b2 = C5640aRc.m17844b(ClothCategory.class, "d911675436c735ecb77e1cca9b118dff", i2);
        f6454zS = b2;
        arzArr[i2] = b2;
        int i3 = i2 + 1;
        C5663aRz b3 = C5640aRc.m17844b(ClothCategory.class, "8fc9387dda10d94ba30e1726099636d4", i3);
        f6446bL = b3;
        arzArr[i3] = b3;
        int i4 = i3 + 1;
        _m_fields = arzArr;
        C1634Xv.m11725a((Object[]) aDJ._m_fields, (Object[]) _m_fields);
        int i5 = aDJ._m_methodCount;
        C2491fm[] fmVarArr = new C2491fm[(i5 + 8)];
        C2491fm a = C4105zY.m41624a(ClothCategory.class, "7c2d25220a722566fddf560a9607154c", i5);
        _f_toString_0020_0028_0029Ljava_002flang_002fString_003b = a;
        fmVarArr[i5] = a;
        int i6 = i5 + 1;
        C2491fm a2 = C4105zY.m41624a(ClothCategory.class, "68dcebe16adb9c14af87ca9746fc6eb1", i6);
        f6447bN = a2;
        fmVarArr[i6] = a2;
        int i7 = i6 + 1;
        C2491fm a3 = C4105zY.m41624a(ClothCategory.class, "87c7f4bf338e2a3868fbfdd29d109403", i7);
        f6448bO = a3;
        fmVarArr[i7] = a3;
        int i8 = i7 + 1;
        C2491fm a4 = C4105zY.m41624a(ClothCategory.class, "fa60b054db38c52b7868e5ea2a11357e", i8);
        f6455zT = a4;
        fmVarArr[i8] = a4;
        int i9 = i8 + 1;
        C2491fm a5 = C4105zY.m41624a(ClothCategory.class, "ada67641f596bd1b437455c26d9507e7", i9);
        f6456zU = a5;
        fmVarArr[i9] = a5;
        int i10 = i9 + 1;
        C2491fm a6 = C4105zY.m41624a(ClothCategory.class, "12c9367497bc59e0cca80fc95abaeed8", i10);
        f6457zV = a6;
        fmVarArr[i10] = a6;
        int i11 = i10 + 1;
        C2491fm a7 = C4105zY.m41624a(ClothCategory.class, "53c8b259e1c6264d33d94a5679bc1670", i11);
        f6449bP = a7;
        fmVarArr[i11] = a7;
        int i12 = i11 + 1;
        C2491fm a8 = C4105zY.m41624a(ClothCategory.class, "83da93e4608339e84d54e481aff25585", i12);
        f6450bQ = a8;
        fmVarArr[i12] = a8;
        int i13 = i12 + 1;
        _m_methods = fmVarArr;
        C1634Xv.m11725a((Object[]) aDJ._m_methods, (Object[]) _m_methods);
        ___iScriptClass = aUO.m18566a(ClothCategory.class, C4134zy.class, _m_fields, _m_methods);
    }

    /* renamed from: a */
    public static boolean m28754a(Set<ClothCategory> set, Set<ClothCategory> set2) {
        for (ClothCategory contains : set2) {
            if (set.contains(contains)) {
                return true;
            }
        }
        return false;
    }

    /* renamed from: S */
    private void m28750S(String str) {
        bFf().mo5608dq().mo3197f(f6454zS, str);
    }

    /* renamed from: a */
    private void m28752a(UUID uuid) {
        bFf().mo5608dq().mo3197f(f6446bL, uuid);
    }

    /* renamed from: an */
    private UUID m28755an() {
        return (UUID) bFf().mo5608dq().mo3214p(f6446bL);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, aYS = "Consistency", displayName = "Handle (Consistency checker)")
    @C0064Am(aul = "83da93e4608339e84d54e481aff25585", aum = 0)
    @C5566aOg
    /* renamed from: b */
    private void m28759b(String str) {
        throw new aWi(new aCE(this, f6450bQ, new Object[]{str}));
    }

    /* renamed from: c */
    private void m28761c(UUID uuid) {
        switch (bFf().mo6893i(f6448bO)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f6448bO, new Object[]{uuid}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f6448bO, new Object[]{uuid}));
                break;
        }
        m28760b(uuid);
    }

    /* renamed from: d */
    private void m28762d(I18NString i18NString) {
        bFf().mo5608dq().mo3197f(f6452zQ, i18NString);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Cloth Category Name")
    @C0064Am(aul = "ada67641f596bd1b437455c26d9507e7", aum = 0)
    @C5566aOg
    /* renamed from: e */
    private void m28763e(I18NString i18NString) {
        throw new aWi(new aCE(this, f6456zU, new Object[]{i18NString}));
    }

    /* renamed from: kb */
    private I18NString m28764kb() {
        return (I18NString) bFf().mo5608dq().mo3214p(f6452zQ);
    }

    /* renamed from: kc */
    private String m28765kc() {
        return (String) bFf().mo5608dq().mo3214p(f6454zS);
    }

    /* renamed from: T */
    public C6494any mo11T() {
        return ___iScriptClass;
    }

    /* renamed from: U */
    public C2491fm[] mo12U() {
        return _m_methods;
    }

    /* renamed from: W */
    public Object mo13W() {
        return new C4134zy(this);
    }

    /* renamed from: a */
    public Object mo14a(C0495Gr gr) {
        Object[] args = gr.getArgs();
        switch (gr.mo2417hq() - aDJ._m_methodCount) {
            case 0:
                return m28758au();
            case 1:
                return m28756ap();
            case 2:
                m28760b((UUID) args[0]);
                return null;
            case 3:
                return m28766kd();
            case 4:
                m28763e((I18NString) args[0]);
                return null;
            case 5:
                return new Boolean(m28753a((ClothCategory) args[0]));
            case 6:
                return m28757ar();
            case 7:
                m28759b((String) args[0]);
                return null;
            default:
                return super.mo14a(gr);
        }
    }

    /* renamed from: a */
    public void mo15a(Collection collection, C0495Gr gr) {
        super.mo15a(collection, gr);
    }

    /* renamed from: aq */
    public UUID mo18aq() {
        switch (bFf().mo6893i(f6447bN)) {
            case 0:
                return null;
            case 2:
                return (UUID) bFf().mo5606d(new aCE(this, f6447bN, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f6447bN, new Object[0]));
                break;
        }
        return m28756ap();
    }

    /* renamed from: b */
    public boolean mo15442b(ClothCategory dHVar) {
        switch (bFf().mo6893i(f6457zV)) {
            case 0:
                return false;
            case 2:
                return ((Boolean) bFf().mo5606d(new aCE(this, f6457zV, new Object[]{dHVar}))).booleanValue();
            case 3:
                bFf().mo5606d(new aCE(this, f6457zV, new Object[]{dHVar}));
                break;
        }
        return m28753a(dHVar);
    }

    /* renamed from: c */
    public C5663aRz[] mo25c() {
        return _m_fields;
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Cloth Category Name")
    @C5566aOg
    /* renamed from: f */
    public void mo17732f(I18NString i18NString) {
        switch (bFf().mo6893i(f6456zU)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f6456zU, new Object[]{i18NString}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f6456zU, new Object[]{i18NString}));
                break;
        }
        m28763e(i18NString);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, aYS = "Consistency", displayName = "Handle (Consistency checker)")
    public String getHandle() {
        switch (bFf().mo6893i(f6449bP)) {
            case 0:
                return null;
            case 2:
                return (String) bFf().mo5606d(new aCE(this, f6449bP, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f6449bP, new Object[0]));
                break;
        }
        return m28757ar();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, aYS = "Consistency", displayName = "Handle (Consistency checker)")
    @C5566aOg
    public void setHandle(String str) {
        switch (bFf().mo6893i(f6450bQ)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f6450bQ, new Object[]{str}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f6450bQ, new Object[]{str}));
                break;
        }
        m28759b(str);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Cloth Category Name")
    /* renamed from: ke */
    public I18NString mo17733ke() {
        switch (bFf().mo6893i(f6455zT)) {
            case 0:
                return null;
            case 2:
                return (I18NString) bFf().mo5606d(new aCE(this, f6455zT, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f6455zT, new Object[0]));
                break;
        }
        return m28766kd();
    }

    public String toString() {
        switch (bFf().mo6893i(_f_toString_0020_0028_0029Ljava_002flang_002fString_003b)) {
            case 0:
                return null;
            case 2:
                return (String) bFf().mo5606d(new aCE(this, _f_toString_0020_0028_0029Ljava_002flang_002fString_003b, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, _f_toString_0020_0028_0029Ljava_002flang_002fString_003b, new Object[0]));
                break;
        }
        return m28758au();
    }

    @C0064Am(aul = "7c2d25220a722566fddf560a9607154c", aum = 0)
    /* renamed from: au */
    private String m28758au() {
        if (m28764kb() != null) {
            return m28764kb().get();
        }
        return super.toString();
    }

    @C0064Am(aul = "68dcebe16adb9c14af87ca9746fc6eb1", aum = 0)
    /* renamed from: ap */
    private UUID m28756ap() {
        return m28755an();
    }

    @C0064Am(aul = "87c7f4bf338e2a3868fbfdd29d109403", aum = 0)
    /* renamed from: b */
    private void m28760b(UUID uuid) {
        m28752a(uuid);
    }

    /* renamed from: S */
    public void mo10S() {
        super.mo10S();
        m28752a(UUID.randomUUID());
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Cloth Category Name")
    @C0064Am(aul = "fa60b054db38c52b7868e5ea2a11357e", aum = 0)
    /* renamed from: kd */
    private I18NString m28766kd() {
        return m28764kb();
    }

    @C0064Am(aul = "12c9367497bc59e0cca80fc95abaeed8", aum = 0)
    /* renamed from: a */
    private boolean m28753a(ClothCategory dHVar) {
        return equals(dHVar);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, aYS = "Consistency", displayName = "Handle (Consistency checker)")
    @C0064Am(aul = "53c8b259e1c6264d33d94a5679bc1670", aum = 0)
    /* renamed from: ar */
    private String m28757ar() {
        return m28765kc();
    }
}
