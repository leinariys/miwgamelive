package game.script.avatar.cloth;

import game.network.message.externalizable.aCE;
import game.network.message.serializable.C2686iZ;
import logic.baa.C1616Xf;
import logic.baa.C1634Xv;
import logic.baa.C5511aMd;
import logic.baa.C6485anp;
import logic.data.mbean.C0322EN;
import logic.res.code.C5640aRc;
import logic.res.code.C5663aRz;
import logic.res.code.C6494any;
import logic.res.html.C2491fm;
import p001a.*;

import java.util.Collection;
import java.util.Collections;
import java.util.Set;

@C5511aMd
@C6485anp
/* renamed from: a.ape  reason: case insensitive filesystem */
/* compiled from: a */
public class CompoundCategory extends ClothCategory implements C1616Xf {
    public static final int _m_fieldCount = 0;
    public static final C5663aRz[] _m_fields = null;
    public static final int _m_methodCount = 0;
    public static final C2491fm[] _m_methods = null;
    public static final C5663aRz gln = null;
    public static final C2491fm glo = null;
    public static final C2491fm glp = null;
    public static final C2491fm glq = null;
    public static final long serialVersionUID = 0;
    /* renamed from: zV */
    public static final C2491fm f5123zV = null;
    public static C6494any ___iScriptClass;
    @C0064Am(aul = "74065acf36a47ca51d64b00b55a341a0", aum = 0)
    private static C2686iZ<ClothCategory> cTU;

    static {
        m24942V();
    }

    public CompoundCategory() {
        super((C5540aNg) null);
        super.mo10S();
    }

    public CompoundCategory(C5540aNg ang) {
        super(ang);
    }

    /* renamed from: V */
    static void m24942V() {
        serialVersionUID = (long) 1;
        _m_fieldCount = ClothCategory._m_fieldCount + 1;
        _m_methodCount = ClothCategory._m_methodCount + 4;
        int i = ClothCategory._m_fieldCount;
        C5663aRz[] arzArr = new C5663aRz[(i + 1)];
        C5663aRz b = C5640aRc.m17844b(CompoundCategory.class, "74065acf36a47ca51d64b00b55a341a0", i);
        gln = b;
        arzArr[i] = b;
        int i2 = i + 1;
        _m_fields = arzArr;
        C1634Xv.m11725a((Object[]) ClothCategory._m_fields, (Object[]) _m_fields);
        int i3 = ClothCategory._m_methodCount;
        C2491fm[] fmVarArr = new C2491fm[(i3 + 4)];
        C2491fm a = C4105zY.m41624a(CompoundCategory.class, "537fa52e067f0c9b66cd3f451b3ca835", i3);
        glo = a;
        fmVarArr[i3] = a;
        int i4 = i3 + 1;
        C2491fm a2 = C4105zY.m41624a(CompoundCategory.class, "a7c6fb92f57d0d51def5ce9d5f3d4fed", i4);
        glp = a2;
        fmVarArr[i4] = a2;
        int i5 = i4 + 1;
        C2491fm a3 = C4105zY.m41624a(CompoundCategory.class, "e9bc6b8bfc0871521904dd43b645dac9", i5);
        glq = a3;
        fmVarArr[i5] = a3;
        int i6 = i5 + 1;
        C2491fm a4 = C4105zY.m41624a(CompoundCategory.class, "b251b0e6d504332649db87d23e9cb189", i6);
        f5123zV = a4;
        fmVarArr[i6] = a4;
        int i7 = i6 + 1;
        _m_methods = fmVarArr;
        C1634Xv.m11725a((Object[]) ClothCategory._m_methods, (Object[]) _m_methods);
        ___iScriptClass = aUO.m18566a(CompoundCategory.class, C0322EN.class, _m_fields, _m_methods);
    }

    /* renamed from: W */
    private void m24943W(C2686iZ iZVar) {
        bFf().mo5608dq().mo3197f(gln, iZVar);
    }

    private C2686iZ cpe() {
        return (C2686iZ) bFf().mo5608dq().mo3214p(gln);
    }

    /* renamed from: T */
    public C6494any mo11T() {
        return ___iScriptClass;
    }

    /* renamed from: U */
    public C2491fm[] mo12U() {
        return _m_methods;
    }

    /* renamed from: W */
    public Object mo13W() {
        return new C0322EN(this);
    }

    /* renamed from: a */
    public Object mo14a(C0495Gr gr) {
        Object[] args = gr.getArgs();
        switch (gr.mo2417hq() - ClothCategory._m_methodCount) {
            case 0:
                m24945p((ClothCategory) args[0]);
                return null;
            case 1:
                m24946r((ClothCategory) args[0]);
                return null;
            case 2:
                return cpf();
            case 3:
                return new Boolean(m24944a((ClothCategory) args[0]));
            default:
                return super.mo14a(gr);
        }
    }

    /* renamed from: a */
    public void mo15a(Collection collection, C0495Gr gr) {
        super.mo15a(collection, gr);
    }

    /* renamed from: b */
    public boolean mo15442b(ClothCategory dHVar) {
        switch (bFf().mo6893i(f5123zV)) {
            case 0:
                return false;
            case 2:
                return ((Boolean) bFf().mo5606d(new aCE(this, f5123zV, new Object[]{dHVar}))).booleanValue();
            case 3:
                bFf().mo5606d(new aCE(this, f5123zV, new Object[]{dHVar}));
                break;
        }
        return m24944a(dHVar);
    }

    /* renamed from: c */
    public C5663aRz[] mo25c() {
        return _m_fields;
    }

    @C3248pc(aYR = C3248pc.C3250b.ITERATOR, displayName = "Sub categories")
    public Set<ClothCategory> cpg() {
        switch (bFf().mo6893i(glq)) {
            case 0:
                return null;
            case 2:
                return (Set) bFf().mo5606d(new aCE(this, glq, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, glq, new Object[0]));
                break;
        }
        return cpf();
    }

    @C3248pc(aYR = C3248pc.C3250b.ADDER, displayName = "Sub categories")
    /* renamed from: q */
    public void mo15444q(ClothCategory dHVar) {
        switch (bFf().mo6893i(glo)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, glo, new Object[]{dHVar}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, glo, new Object[]{dHVar}));
                break;
        }
        m24945p(dHVar);
    }

    @C3248pc(aYR = C3248pc.C3250b.REMOVER, displayName = "Sub categories")
    /* renamed from: s */
    public void mo15445s(ClothCategory dHVar) {
        switch (bFf().mo6893i(glp)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, glp, new Object[]{dHVar}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, glp, new Object[]{dHVar}));
                break;
        }
        m24946r(dHVar);
    }

    /* renamed from: S */
    public void mo10S() {
        super.mo10S();
    }

    @C3248pc(aYR = C3248pc.C3250b.ADDER, displayName = "Sub categories")
    @C0064Am(aul = "537fa52e067f0c9b66cd3f451b3ca835", aum = 0)
    /* renamed from: p */
    private void m24945p(ClothCategory dHVar) {
        cpe().add(dHVar);
    }

    @C3248pc(aYR = C3248pc.C3250b.REMOVER, displayName = "Sub categories")
    @C0064Am(aul = "a7c6fb92f57d0d51def5ce9d5f3d4fed", aum = 0)
    /* renamed from: r */
    private void m24946r(ClothCategory dHVar) {
        cpe().remove(dHVar);
    }

    @C3248pc(aYR = C3248pc.C3250b.ITERATOR, displayName = "Sub categories")
    @C0064Am(aul = "e9bc6b8bfc0871521904dd43b645dac9", aum = 0)
    private Set<ClothCategory> cpf() {
        return Collections.unmodifiableSet(cpe());
    }

    @C0064Am(aul = "b251b0e6d504332649db87d23e9cb189", aum = 0)
    /* renamed from: a */
    private boolean m24944a(ClothCategory dHVar) {
        if (dHVar instanceof CompoundCategory) {
            return !equals(dHVar) || !m28754a((Set<ClothCategory>) cpe(), (Set<ClothCategory>) ((CompoundCategory) dHVar).cpe());
        }
        return super.mo15442b(dHVar);
    }
}
