package game.script.avatar.cloth;

import game.network.message.externalizable.aCE;
import game.network.message.serializable.C2686iZ;
import game.script.avatar.AvatarSector;
import game.script.avatar.body.AvatarAppearanceType;
import game.script.avatar.body.BodyPiece;
import game.script.resource.Asset;
import logic.baa.*;
import logic.data.mbean.aVZ;
import logic.res.code.C5640aRc;
import logic.res.code.C5663aRz;
import logic.res.code.C6494any;
import logic.res.html.C2491fm;
import p001a.*;
import taikodom.infra.script.I18NString;

import java.io.File;
import java.util.Collection;
import java.util.Collections;
import java.util.Set;
import java.util.UUID;

@C5511aMd
@C6485anp
/* renamed from: a.WJ */
/* compiled from: a */
public class Cloth extends TaikodomObject implements C0468GU, C1616Xf {

    /* renamed from: KP */
    public static final C5663aRz f1992KP = null;

    /* renamed from: NY */
    public static final C5663aRz f1993NY = null;

    /* renamed from: Ob */
    public static final C2491fm f1994Ob = null;

    /* renamed from: Oc */
    public static final C2491fm f1995Oc = null;
    public static final C2491fm _f_toString_0020_0028_0029Ljava_002flang_002fString_003b = null;
    public static final int _m_fieldCount = 0;
    public static final C5663aRz[] _m_fields = null;
    public static final int _m_methodCount = 0;
    public static final C2491fm[] _m_methods = null;
    /* renamed from: bL */
    public static final C5663aRz f1997bL = null;
    /* renamed from: bN */
    public static final C2491fm f1998bN = null;
    /* renamed from: bO */
    public static final C2491fm f1999bO = null;
    /* renamed from: bP */
    public static final C2491fm f2000bP = null;
    /* renamed from: bQ */
    public static final C2491fm f2001bQ = null;
    public static final C2491fm bxj = null;
    public static final C2491fm bxk = null;
    /* renamed from: en */
    public static final C5663aRz f2002en = null;
    /* renamed from: eq */
    public static final C5663aRz f2003eq = null;
    public static final C5663aRz eyB = null;
    public static final C5663aRz eyD = null;
    public static final C5663aRz eyF = null;
    public static final C5663aRz eyH = null;
    public static final C5663aRz eyJ = null;
    public static final C5663aRz eyL = null;
    public static final C2491fm eyM = null;
    public static final C2491fm eyN = null;
    public static final C2491fm eyO = null;
    public static final C2491fm eyP = null;
    public static final C2491fm eyQ = null;
    public static final C2491fm eyR = null;
    public static final C2491fm eyS = null;
    public static final C2491fm eyT = null;
    public static final C2491fm eyU = null;
    public static final C2491fm eyV = null;
    public static final C2491fm eyW = null;
    public static final C2491fm eyX = null;
    public static final C2491fm eyY = null;
    public static final C2491fm eyZ = null;
    public static final C5663aRz eyu = null;
    public static final C5663aRz eyw = null;
    public static final C5663aRz eyz = null;
    public static final C2491fm eza = null;
    public static final C2491fm ezb = null;
    public static final C2491fm ezc = null;
    public static final C2491fm ezd = null;
    public static final C2491fm eze = null;
    public static final C2491fm ezf = null;
    public static final C2491fm ezg = null;
    public static final C2491fm ezh = null;
    public static final C2491fm ezi = null;
    public static final C2491fm ezj = null;
    public static final C2491fm ezk = null;
    /* renamed from: ff */
    public static final C2491fm f2004ff = null;
    /* renamed from: fg */
    public static final C2491fm f2005fg = null;
    public static final long serialVersionUID = 0;
    /* renamed from: zQ */
    public static final C5663aRz f2008zQ = null;
    /* renamed from: zS */
    public static final C5663aRz f2010zS = null;
    /* renamed from: zT */
    public static final C2491fm f2011zT = null;
    private static final String eys = "/";
    private static final String eyt = ".dds";
    public static C6494any ___iScriptClass = null;
    @C0064Am(aul = "b879629d78b19e04bac558c5fc3674c1", aum = 15)

    /* renamed from: bK */
    private static UUID f1996bK = null;
    @C0064Am(aul = "f0eadea20242a60dff1dc618257b7d13", aum = 7)
    private static C2686iZ<BodyPiece> eyA = null;
    @C0064Am(aul = "b50decd8f8501ed93375808e10e72a78", aum = 8)
    private static C2686iZ<AvatarSector> eyC = null;
    @C0064Am(aul = "4a1a423f8dda865de494757b9d203396", aum = 10)
    private static Asset eyE = null;
    @C0064Am(aul = "83a0ffc53895b1192314647e7537be5a", aum = 11)
    private static long eyG = 0;
    @C0064Am(aul = "5c2407469bcde71ad9126a3092135262", aum = 12)
    private static long eyI = 0;
    @C0064Am(aul = "6f1ad25bf470496c8487ff319755c8e9", aum = 13)
    private static boolean eyK = false;
    @C0064Am(aul = "db6688104aa265f1f110b4b6d2ef56f9", aum = 4)
    private static String eyv;
    @C0064Am(aul = "e0b4bee99fbf7f9f154fb299102a1018", aum = 5)
    private static ClothCategory eyx;
    @C0064Am(aul = "b845cbb8e21f53585ba403fb0bc4a25c", aum = 6)
    private static C2686iZ<AvatarAppearanceType> eyy;
    @C0064Am(aul = "eac100e0d005443a694979dd7da02238", aum = 9)

    /* renamed from: jn */
    private static Asset f2006jn;
    @C0064Am(aul = "0d38d90a8624474ef6c161237e482bf4", aum = 3)
    private static String prefix;
    @C0064Am(aul = "0def779b891abdc3d7f8fd4867c455ee", aum = 2)
    private static int priority;
    @C0064Am(aul = "0c6887e0d8f826324f9c64ddddbbf6ab", aum = 1)
    private static boolean restricted;
    @C0064Am(aul = "6d2297712022956c641a9d65702b5aaa", aum = 0)

    /* renamed from: zP */
    private static I18NString f2007zP;
    @C0064Am(aul = "728cdf7edfb33bcac271ae2aa7dcbffa", aum = 14)

    /* renamed from: zR */
    private static String f2009zR;

    static {
        m11076V();
    }

    public Cloth() {
        super((C5540aNg) null);
        super.mo10S();
    }

    public Cloth(C5540aNg ang) {
        super(ang);
    }

    /* renamed from: V */
    static void m11076V() {
        serialVersionUID = (long) 1;
        _m_fieldCount = TaikodomObject._m_fieldCount + 16;
        _m_methodCount = TaikodomObject._m_methodCount + 37;
        int i = TaikodomObject._m_fieldCount;
        C5663aRz[] arzArr = new C5663aRz[(i + 16)];
        C5663aRz b = C5640aRc.m17844b(Cloth.class, "6d2297712022956c641a9d65702b5aaa", i);
        f2008zQ = b;
        arzArr[i] = b;
        int i2 = i + 1;
        C5663aRz b2 = C5640aRc.m17844b(Cloth.class, "0c6887e0d8f826324f9c64ddddbbf6ab", i2);
        f2002en = b2;
        arzArr[i2] = b2;
        int i3 = i2 + 1;
        C5663aRz b3 = C5640aRc.m17844b(Cloth.class, "0def779b891abdc3d7f8fd4867c455ee", i3);
        eyu = b3;
        arzArr[i3] = b3;
        int i4 = i3 + 1;
        C5663aRz b4 = C5640aRc.m17844b(Cloth.class, "0d38d90a8624474ef6c161237e482bf4", i4);
        f2003eq = b4;
        arzArr[i4] = b4;
        int i5 = i4 + 1;
        C5663aRz b5 = C5640aRc.m17844b(Cloth.class, "db6688104aa265f1f110b4b6d2ef56f9", i5);
        eyw = b5;
        arzArr[i5] = b5;
        int i6 = i5 + 1;
        C5663aRz b6 = C5640aRc.m17844b(Cloth.class, "e0b4bee99fbf7f9f154fb299102a1018", i6);
        f1992KP = b6;
        arzArr[i6] = b6;
        int i7 = i6 + 1;
        C5663aRz b7 = C5640aRc.m17844b(Cloth.class, "b845cbb8e21f53585ba403fb0bc4a25c", i7);
        eyz = b7;
        arzArr[i7] = b7;
        int i8 = i7 + 1;
        C5663aRz b8 = C5640aRc.m17844b(Cloth.class, "f0eadea20242a60dff1dc618257b7d13", i8);
        eyB = b8;
        arzArr[i8] = b8;
        int i9 = i8 + 1;
        C5663aRz b9 = C5640aRc.m17844b(Cloth.class, "b50decd8f8501ed93375808e10e72a78", i9);
        eyD = b9;
        arzArr[i9] = b9;
        int i10 = i9 + 1;
        C5663aRz b10 = C5640aRc.m17844b(Cloth.class, "eac100e0d005443a694979dd7da02238", i10);
        f1993NY = b10;
        arzArr[i10] = b10;
        int i11 = i10 + 1;
        C5663aRz b11 = C5640aRc.m17844b(Cloth.class, "4a1a423f8dda865de494757b9d203396", i11);
        eyF = b11;
        arzArr[i11] = b11;
        int i12 = i11 + 1;
        C5663aRz b12 = C5640aRc.m17844b(Cloth.class, "83a0ffc53895b1192314647e7537be5a", i12);
        eyH = b12;
        arzArr[i12] = b12;
        int i13 = i12 + 1;
        C5663aRz b13 = C5640aRc.m17844b(Cloth.class, "5c2407469bcde71ad9126a3092135262", i13);
        eyJ = b13;
        arzArr[i13] = b13;
        int i14 = i13 + 1;
        C5663aRz b14 = C5640aRc.m17844b(Cloth.class, "6f1ad25bf470496c8487ff319755c8e9", i14);
        eyL = b14;
        arzArr[i14] = b14;
        int i15 = i14 + 1;
        C5663aRz b15 = C5640aRc.m17844b(Cloth.class, "728cdf7edfb33bcac271ae2aa7dcbffa", i15);
        f2010zS = b15;
        arzArr[i15] = b15;
        int i16 = i15 + 1;
        C5663aRz b16 = C5640aRc.m17844b(Cloth.class, "b879629d78b19e04bac558c5fc3674c1", i16);
        f1997bL = b16;
        arzArr[i16] = b16;
        int i17 = i16 + 1;
        _m_fields = arzArr;
        C1634Xv.m11725a((Object[]) TaikodomObject._m_fields, (Object[]) _m_fields);
        int i18 = TaikodomObject._m_methodCount;
        C2491fm[] fmVarArr = new C2491fm[(i18 + 37)];
        C2491fm a = C4105zY.m41624a(Cloth.class, "5253ca2264a737282d03b73a8b16844c", i18);
        f1998bN = a;
        fmVarArr[i18] = a;
        int i19 = i18 + 1;
        C2491fm a2 = C4105zY.m41624a(Cloth.class, "6afc989f9511a5087d4f1677bbb368d7", i19);
        f1999bO = a2;
        fmVarArr[i19] = a2;
        int i20 = i19 + 1;
        C2491fm a3 = C4105zY.m41624a(Cloth.class, "6cbb652b01f9f8ad03ebf8bd8c34c543", i20);
        _f_toString_0020_0028_0029Ljava_002flang_002fString_003b = a3;
        fmVarArr[i20] = a3;
        int i21 = i20 + 1;
        C2491fm a4 = C4105zY.m41624a(Cloth.class, "1a410368a8e7df38ba6a2bfe88b31d29", i21);
        eyM = a4;
        fmVarArr[i21] = a4;
        int i22 = i21 + 1;
        C2491fm a5 = C4105zY.m41624a(Cloth.class, "5186179060443c7b0c505e57809d84a3", i22);
        f2011zT = a5;
        fmVarArr[i22] = a5;
        int i23 = i22 + 1;
        C2491fm a6 = C4105zY.m41624a(Cloth.class, "4fde5738b7a34027f079ecd37ecb396f", i23);
        eyN = a6;
        fmVarArr[i23] = a6;
        int i24 = i23 + 1;
        C2491fm a7 = C4105zY.m41624a(Cloth.class, "23b1852c459a4d722f07d3c36adc34e0", i24);
        eyO = a7;
        fmVarArr[i24] = a7;
        int i25 = i24 + 1;
        C2491fm a8 = C4105zY.m41624a(Cloth.class, "7584819e8d82fef4044ae4dd2d5aeaf9", i25);
        f2005fg = a8;
        fmVarArr[i25] = a8;
        int i26 = i25 + 1;
        C2491fm a9 = C4105zY.m41624a(Cloth.class, "6c70715a6a6c595dbedd2094f8514f27", i26);
        f2004ff = a9;
        fmVarArr[i26] = a9;
        int i27 = i26 + 1;
        C2491fm a10 = C4105zY.m41624a(Cloth.class, "ce442a25ec01c0bc076a2f298735ec72", i27);
        eyP = a10;
        fmVarArr[i27] = a10;
        int i28 = i27 + 1;
        C2491fm a11 = C4105zY.m41624a(Cloth.class, "cbe9f2c3e079ecb5356d0c1152d07625", i28);
        eyQ = a11;
        fmVarArr[i28] = a11;
        int i29 = i28 + 1;
        C2491fm a12 = C4105zY.m41624a(Cloth.class, "e3f6a97c3e0a2694bfd0db494037e2dd", i29);
        bxk = a12;
        fmVarArr[i29] = a12;
        int i30 = i29 + 1;
        C2491fm a13 = C4105zY.m41624a(Cloth.class, "7490003b55264f1934bca216d18ca1ab", i30);
        bxj = a13;
        fmVarArr[i30] = a13;
        int i31 = i30 + 1;
        C2491fm a14 = C4105zY.m41624a(Cloth.class, "9193138c4bbde854ab4aa81103f947c2", i31);
        eyR = a14;
        fmVarArr[i31] = a14;
        int i32 = i31 + 1;
        C2491fm a15 = C4105zY.m41624a(Cloth.class, "1ddcb12a19b8f6d62b57ca9e025f984c", i32);
        eyS = a15;
        fmVarArr[i32] = a15;
        int i33 = i32 + 1;
        C2491fm a16 = C4105zY.m41624a(Cloth.class, "8f7751bad308fe21b169c26f7b0be92c", i33);
        eyT = a16;
        fmVarArr[i33] = a16;
        int i34 = i33 + 1;
        C2491fm a17 = C4105zY.m41624a(Cloth.class, "597d750b2fcefb1610be06a6e2c9458e", i34);
        eyU = a17;
        fmVarArr[i34] = a17;
        int i35 = i34 + 1;
        C2491fm a18 = C4105zY.m41624a(Cloth.class, "acbcfb978d64265b1d6886e45fe9ef81", i35);
        eyV = a18;
        fmVarArr[i35] = a18;
        int i36 = i35 + 1;
        C2491fm a19 = C4105zY.m41624a(Cloth.class, "7debc66dbe64011fdcd9420851407ba6", i36);
        eyW = a19;
        fmVarArr[i36] = a19;
        int i37 = i36 + 1;
        C2491fm a20 = C4105zY.m41624a(Cloth.class, "e854659066d116957e60a3ca53c9f17c", i37);
        f1995Oc = a20;
        fmVarArr[i37] = a20;
        int i38 = i37 + 1;
        C2491fm a21 = C4105zY.m41624a(Cloth.class, "2fa7c7f832b3990115aec77ef1024c67", i38);
        f1994Ob = a21;
        fmVarArr[i38] = a21;
        int i39 = i38 + 1;
        C2491fm a22 = C4105zY.m41624a(Cloth.class, "e1362c2ae34b313a639a123e0af7a229", i39);
        eyX = a22;
        fmVarArr[i39] = a22;
        int i40 = i39 + 1;
        C2491fm a23 = C4105zY.m41624a(Cloth.class, "b53242e69555638229ad897a03a65558", i40);
        eyY = a23;
        fmVarArr[i40] = a23;
        int i41 = i40 + 1;
        C2491fm a24 = C4105zY.m41624a(Cloth.class, "044b6f592a678cf07f361c0bacca1e12", i41);
        eyZ = a24;
        fmVarArr[i41] = a24;
        int i42 = i41 + 1;
        C2491fm a25 = C4105zY.m41624a(Cloth.class, "66dec2cc51c087fd4d744d1b8d815e8c", i42);
        eza = a25;
        fmVarArr[i42] = a25;
        int i43 = i42 + 1;
        C2491fm a26 = C4105zY.m41624a(Cloth.class, "c2505b9bc10bf40b41d78f53140f25dd", i43);
        ezb = a26;
        fmVarArr[i43] = a26;
        int i44 = i43 + 1;
        C2491fm a27 = C4105zY.m41624a(Cloth.class, "a8b36f6ccadc27c6332153b9375c7b8b", i44);
        ezc = a27;
        fmVarArr[i44] = a27;
        int i45 = i44 + 1;
        C2491fm a28 = C4105zY.m41624a(Cloth.class, "3ea1c73bc278dcf8360961f898063498", i45);
        ezd = a28;
        fmVarArr[i45] = a28;
        int i46 = i45 + 1;
        C2491fm a29 = C4105zY.m41624a(Cloth.class, "c7d1a55a61b4545d05658dfc34623d2d", i46);
        eze = a29;
        fmVarArr[i46] = a29;
        int i47 = i46 + 1;
        C2491fm a30 = C4105zY.m41624a(Cloth.class, "5ced61d4ef6f0082fc8152afddc60e90", i47);
        ezf = a30;
        fmVarArr[i47] = a30;
        int i48 = i47 + 1;
        C2491fm a31 = C4105zY.m41624a(Cloth.class, "56649b414759114eb4ef24eb22a33967", i48);
        ezg = a31;
        fmVarArr[i48] = a31;
        int i49 = i48 + 1;
        C2491fm a32 = C4105zY.m41624a(Cloth.class, "8f13cce651b76601867cbd1f5ceac864", i49);
        ezh = a32;
        fmVarArr[i49] = a32;
        int i50 = i49 + 1;
        C2491fm a33 = C4105zY.m41624a(Cloth.class, "7e5b021243759c85bf3214bd4b10fd68", i50);
        ezi = a33;
        fmVarArr[i50] = a33;
        int i51 = i50 + 1;
        C2491fm a34 = C4105zY.m41624a(Cloth.class, "e03a23a3580b92cee24d7eaa2a2a8b18", i51);
        ezj = a34;
        fmVarArr[i51] = a34;
        int i52 = i51 + 1;
        C2491fm a35 = C4105zY.m41624a(Cloth.class, "13a38b3a1b1e75422082c2050807cbcb", i52);
        ezk = a35;
        fmVarArr[i52] = a35;
        int i53 = i52 + 1;
        C2491fm a36 = C4105zY.m41624a(Cloth.class, "fad96c3d214d3f18fbfc2369930db5c1", i53);
        f2000bP = a36;
        fmVarArr[i53] = a36;
        int i54 = i53 + 1;
        C2491fm a37 = C4105zY.m41624a(Cloth.class, "ecec45336740ffc00933290c65b7ef7e", i54);
        f2001bQ = a37;
        fmVarArr[i54] = a37;
        int i55 = i54 + 1;
        _m_methods = fmVarArr;
        C1634Xv.m11725a((Object[]) TaikodomObject._m_methods, (Object[]) _m_methods);
        ___iScriptClass = aUO.m18566a(Cloth.class, aVZ.class, _m_fields, _m_methods);
    }

    /* renamed from: K */
    private void m11072K(C2686iZ iZVar) {
        bFf().mo5608dq().mo3197f(eyz, iZVar);
    }

    /* renamed from: L */
    private void m11073L(C2686iZ iZVar) {
        bFf().mo5608dq().mo3197f(eyB, iZVar);
    }

    /* renamed from: M */
    private void m11074M(C2686iZ iZVar) {
        bFf().mo5608dq().mo3197f(eyD, iZVar);
    }

    /* renamed from: S */
    private void m11075S(String str) {
        bFf().mo5608dq().mo3197f(f2010zS, str);
    }

    /* renamed from: a */
    private void m11078a(UUID uuid) {
        bFf().mo5608dq().mo3197f(f1997bL, uuid);
    }

    /* renamed from: aY */
    private void m11079aY(Asset tCVar) {
        bFf().mo5608dq().mo3197f(eyF, tCVar);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Material asset")
    @C0064Am(aul = "e1362c2ae34b313a639a123e0af7a229", aum = 0)
    @C5566aOg
    /* renamed from: aZ */
    private void m11080aZ(Asset tCVar) {
        throw new aWi(new aCE(this, eyX, new Object[]{tCVar}));
    }

    /* renamed from: an */
    private UUID m11081an() {
        return (UUID) bFf().mo5608dq().mo3214p(f1997bL);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, aYS = "Consistency", displayName = "Handle (Consistency checker)")
    @C0064Am(aul = "ecec45336740ffc00933290c65b7ef7e", aum = 0)
    @C5566aOg
    /* renamed from: b */
    private void m11085b(String str) {
        throw new aWi(new aCE(this, f2001bQ, new Object[]{str}));
    }

    private int bDJ() {
        return bFf().mo5608dq().mo3212n(eyu);
    }

    private String bDK() {
        return (String) bFf().mo5608dq().mo3214p(eyw);
    }

    private ClothCategory bDL() {
        return (ClothCategory) bFf().mo5608dq().mo3214p(f1992KP);
    }

    private C2686iZ bDM() {
        return (C2686iZ) bFf().mo5608dq().mo3214p(eyz);
    }

    private C2686iZ bDN() {
        return (C2686iZ) bFf().mo5608dq().mo3214p(eyB);
    }

    private C2686iZ bDO() {
        return (C2686iZ) bFf().mo5608dq().mo3214p(eyD);
    }

    private Asset bDP() {
        return (Asset) bFf().mo5608dq().mo3214p(eyF);
    }

    private long bDQ() {
        return bFf().mo5608dq().mo3213o(eyH);
    }

    private long bDR() {
        return bFf().mo5608dq().mo3213o(eyJ);
    }

    private boolean bDS() {
        return bFf().mo5608dq().mo3201h(eyL);
    }

    /* renamed from: bd */
    private boolean m11088bd() {
        return bFf().mo5608dq().mo3201h(f2002en);
    }

    /* renamed from: bf */
    private String m11089bf() {
        return (String) bFf().mo5608dq().mo3214p(f2003eq);
    }

    /* renamed from: c */
    private void m11090c(UUID uuid) {
        switch (bFf().mo6893i(f1999bO)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f1999bO, new Object[]{uuid}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f1999bO, new Object[]{uuid}));
                break;
        }
        m11086b(uuid);
    }

    /* renamed from: d */
    private void m11091d(I18NString i18NString) {
        bFf().mo5608dq().mo3197f(f2008zQ, i18NString);
    }

    /* renamed from: dD */
    private void m11092dD(boolean z) {
        bFf().mo5608dq().mo3153a(eyL, z);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Price is Combined")
    @C0064Am(aul = "8f7751bad308fe21b169c26f7b0be92c", aum = 0)
    @C5566aOg
    /* renamed from: dE */
    private void m11093dE(boolean z) {
        throw new aWi(new aCE(this, eyT, new Object[]{new Boolean(z)}));
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Price in Hoplons")
    @C0064Am(aul = "e3f6a97c3e0a2694bfd0db494037e2dd", aum = 0)
    @C5566aOg
    /* renamed from: dh */
    private void m11094dh(long j) {
        throw new aWi(new aCE(this, bxk, new Object[]{new Long(j)}));
    }

    /* renamed from: f */
    private void m11095f(String str) {
        bFf().mo5608dq().mo3197f(f2003eq, str);
    }

    /* renamed from: fT */
    private void m11096fT(long j) {
        bFf().mo5608dq().mo3184b(eyH, j);
    }

    /* renamed from: fU */
    private void m11097fU(long j) {
        bFf().mo5608dq().mo3184b(eyJ, j);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Price in Taels")
    @C0064Am(aul = "9193138c4bbde854ab4aa81103f947c2", aum = 0)
    @C5566aOg
    /* renamed from: fV */
    private void m11098fV(long j) {
        throw new aWi(new aCE(this, eyR, new Object[]{new Long(j)}));
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Prefix (for mesh resource resolution)")
    @C0064Am(aul = "7584819e8d82fef4044ae4dd2d5aeaf9", aum = 0)
    @C5566aOg
    /* renamed from: g */
    private void m11099g(String str) {
        throw new aWi(new aCE(this, f2005fg, new Object[]{str}));
    }

    /* renamed from: h */
    private void m11101h(boolean z) {
        bFf().mo5608dq().mo3153a(f2002en, z);
    }

    /* renamed from: ht */
    private void m11102ht(String str) {
        bFf().mo5608dq().mo3197f(eyw, str);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Prefix (for texture resource resolution)")
    @C0064Am(aul = "ce442a25ec01c0bc076a2f298735ec72", aum = 0)
    @C5566aOg
    /* renamed from: hu */
    private void m11103hu(String str) {
        throw new aWi(new aCE(this, eyP, new Object[]{str}));
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Cloth Name")
    @C0064Am(aul = "1a410368a8e7df38ba6a2bfe88b31d29", aum = 0)
    @C5566aOg
    /* renamed from: jB */
    private void m11106jB(I18NString i18NString) {
        throw new aWi(new aCE(this, eyM, new Object[]{i18NString}));
    }

    /* renamed from: kb */
    private I18NString m11107kb() {
        return (I18NString) bFf().mo5608dq().mo3214p(f2008zQ);
    }

    /* renamed from: kc */
    private String m11108kc() {
        return (String) bFf().mo5608dq().mo3214p(f2010zS);
    }

    /* renamed from: m */
    private void m11110m(ClothCategory dHVar) {
        bFf().mo5608dq().mo3197f(f1992KP, dHVar);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Cloth Category")
    @C0064Am(aul = "acbcfb978d64265b1d6886e45fe9ef81", aum = 0)
    @C5566aOg
    /* renamed from: n */
    private void m11111n(ClothCategory dHVar) {
        throw new aWi(new aCE(this, eyV, new Object[]{dHVar}));
    }

    /* renamed from: oY */
    private void m11112oY(int i) {
        bFf().mo5608dq().mo3183b(eyu, i);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Priority")
    @C0064Am(aul = "4fde5738b7a34027f079ecd37ecb396f", aum = 0)
    @C5566aOg
    /* renamed from: oZ */
    private void m11113oZ(int i) {
        throw new aWi(new aCE(this, eyN, new Object[]{new Integer(i)}));
    }

    /* renamed from: sG */
    private Asset m11114sG() {
        return (Asset) bFf().mo5608dq().mo3214p(f1993NY);
    }

    /* renamed from: t */
    private void m11117t(Asset tCVar) {
        bFf().mo5608dq().mo3197f(f1993NY, tCVar);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Icon resource")
    @C0064Am(aul = "e854659066d116957e60a3ca53c9f17c", aum = 0)
    @C5566aOg
    /* renamed from: u */
    private void m11118u(Asset tCVar) {
        throw new aWi(new aCE(this, f1995Oc, new Object[]{tCVar}));
    }

    /* renamed from: D */
    public boolean mo6504D(Cloth wj) {
        switch (bFf().mo6893i(ezi)) {
            case 0:
                return false;
            case 2:
                return ((Boolean) bFf().mo5606d(new aCE(this, ezi, new Object[]{wj}))).booleanValue();
            case 3:
                bFf().mo5606d(new aCE(this, ezi, new Object[]{wj}));
                break;
        }
        return m11069C(wj);
    }

    @C3248pc(aYR = C3248pc.C3250b.ADDER, displayName = "Component pieces for this cloth")
    /* renamed from: J */
    public void mo6505J(BodyPiece qb) {
        switch (bFf().mo6893i(ezf)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, ezf, new Object[]{qb}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, ezf, new Object[]{qb}));
                break;
        }
        m11070I(qb);
    }

    @C3248pc(aYR = C3248pc.C3250b.REMOVER, displayName = "Component pieces for this cloth")
    /* renamed from: L */
    public void mo6506L(BodyPiece qb) {
        switch (bFf().mo6893i(ezg)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, ezg, new Object[]{qb}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, ezg, new Object[]{qb}));
                break;
        }
        m11071K(qb);
    }

    /* renamed from: T */
    public C6494any mo11T() {
        return ___iScriptClass;
    }

    /* renamed from: U */
    public C2491fm[] mo12U() {
        return _m_methods;
    }

    /* renamed from: W */
    public Object mo13W() {
        return new aVZ(this);
    }

    /* renamed from: a */
    public Object mo14a(C0495Gr gr) {
        Object[] args = gr.getArgs();
        switch (gr.mo2417hq() - TaikodomObject._m_methodCount) {
            case 0:
                return m11082ap();
            case 1:
                m11086b((UUID) args[0]);
                return null;
            case 2:
                return m11084au();
            case 3:
                m11106jB((I18NString) args[0]);
                return null;
            case 4:
                return m11109kd();
            case 5:
                m11113oZ(((Integer) args[0]).intValue());
                return null;
            case 6:
                return new Integer(bDT());
            case 7:
                m11099g((String) args[0]);
                return null;
            case 8:
                return m11087bD();
            case 9:
                m11103hu((String) args[0]);
                return null;
            case 10:
                return bDU();
            case 11:
                m11094dh(((Long) args[0]).longValue());
                return null;
            case 12:
                return new Long(ajT());
            case 13:
                m11098fV(((Long) args[0]).longValue());
                return null;
            case 14:
                return new Long(bDW());
            case 15:
                m11093dE(((Boolean) args[0]).booleanValue());
                return null;
            case 16:
                return new Boolean(bDY());
            case 17:
                m11111n((ClothCategory) args[0]);
                return null;
            case 18:
                return bEa();
            case 19:
                m11118u((Asset) args[0]);
                return null;
            case 20:
                return m11115sJ();
            case 21:
                m11080aZ((Asset) args[0]);
                return null;
            case 22:
                return bEc();
            case 23:
                m11100h((AvatarAppearanceType) args[0]);
                return null;
            case 24:
                m11105j((AvatarAppearanceType) args[0]);
                return null;
            case 25:
                return bEe();
            case 26:
                m11116t((AvatarSector) args[0]);
                return null;
            case 27:
                m11119v((AvatarSector) args[0]);
                return null;
            case 28:
                return bEg();
            case 29:
                m11070I((BodyPiece) args[0]);
                return null;
            case 30:
                m11071K((BodyPiece) args[0]);
                return null;
            case 31:
                return bEi();
            case 32:
                return new Boolean(m11069C((Cloth) args[0]));
            case 33:
                return m11104hw((String) args[0]);
            case 34:
                return m11077a((AvatarSector.C0955a) args[0], (C3845vi.C3847b) args[1], (String) args[2]);
            case 35:
                return m11083ar();
            case 36:
                m11085b((String) args[0]);
                return null;
            default:
                return super.mo14a(gr);
        }
    }

    /* renamed from: a */
    public void mo15a(Collection collection, C0495Gr gr) {
        super.mo15a(collection, gr);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Price in Hoplons")
    public long ajU() {
        switch (bFf().mo6893i(bxj)) {
            case 0:
                return 0;
            case 2:
                return ((Long) bFf().mo5606d(new aCE(this, bxj, new Object[0]))).longValue();
            case 3:
                bFf().mo5606d(new aCE(this, bxj, new Object[0]));
                break;
        }
        return ajT();
    }

    /* renamed from: aq */
    public UUID mo18aq() {
        switch (bFf().mo6893i(f1998bN)) {
            case 0:
                return null;
            case 2:
                return (UUID) bFf().mo5606d(new aCE(this, f1998bN, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f1998bN, new Object[0]));
                break;
        }
        return m11082ap();
    }

    @ClientOnly
    /* renamed from: b */
    public String mo6508b(AvatarSector.C0955a aVar, C3845vi.C3847b bVar, String str) {
        switch (bFf().mo6893i(ezk)) {
            case 0:
                return null;
            case 2:
                return (String) bFf().mo5606d(new aCE(this, ezk, new Object[]{aVar, bVar, str}));
            case 3:
                bFf().mo5606d(new aCE(this, ezk, new Object[]{aVar, bVar, str}));
                break;
        }
        return m11077a(aVar, bVar, str);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Prefix (for texture resource resolution)")
    public String bDV() {
        switch (bFf().mo6893i(eyQ)) {
            case 0:
                return null;
            case 2:
                return (String) bFf().mo5606d(new aCE(this, eyQ, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, eyQ, new Object[0]));
                break;
        }
        return bDU();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Price in Taels")
    public long bDX() {
        switch (bFf().mo6893i(eyS)) {
            case 0:
                return 0;
            case 2:
                return ((Long) bFf().mo5606d(new aCE(this, eyS, new Object[0]))).longValue();
            case 3:
                bFf().mo5606d(new aCE(this, eyS, new Object[0]));
                break;
        }
        return bDW();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Price is Combined")
    public boolean bDZ() {
        switch (bFf().mo6893i(eyU)) {
            case 0:
                return false;
            case 2:
                return ((Boolean) bFf().mo5606d(new aCE(this, eyU, new Object[0]))).booleanValue();
            case 3:
                bFf().mo5606d(new aCE(this, eyU, new Object[0]));
                break;
        }
        return bDY();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Cloth Category")
    public ClothCategory bEb() {
        switch (bFf().mo6893i(eyW)) {
            case 0:
                return null;
            case 2:
                return (ClothCategory) bFf().mo5606d(new aCE(this, eyW, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, eyW, new Object[0]));
                break;
        }
        return bEa();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Material asset")
    public Asset bEd() {
        switch (bFf().mo6893i(eyY)) {
            case 0:
                return null;
            case 2:
                return (Asset) bFf().mo5606d(new aCE(this, eyY, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, eyY, new Object[0]));
                break;
        }
        return bEc();
    }

    @C3248pc(aYR = C3248pc.C3250b.ITERATOR, displayName = "Forbidden Appearances")
    public Set<AvatarAppearanceType> bEf() {
        switch (bFf().mo6893i(ezb)) {
            case 0:
                return null;
            case 2:
                return (Set) bFf().mo5606d(new aCE(this, ezb, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, ezb, new Object[0]));
                break;
        }
        return bEe();
    }

    @C3248pc(aYR = C3248pc.C3250b.ITERATOR, displayName = "Sectors hidden")
    public Set<AvatarSector> bEh() {
        switch (bFf().mo6893i(eze)) {
            case 0:
                return null;
            case 2:
                return (Set) bFf().mo5606d(new aCE(this, eze, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, eze, new Object[0]));
                break;
        }
        return bEg();
    }

    @C3248pc(aYR = C3248pc.C3250b.ITERATOR, displayName = "Component pieces for this cloth")
    public Set<BodyPiece> bEj() {
        switch (bFf().mo6893i(ezh)) {
            case 0:
                return null;
            case 2:
                return (Set) bFf().mo5606d(new aCE(this, ezh, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, ezh, new Object[0]));
                break;
        }
        return bEi();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Material asset")
    @C5566aOg
    /* renamed from: ba */
    public void mo6517ba(Asset tCVar) {
        switch (bFf().mo6893i(eyX)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, eyX, new Object[]{tCVar}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, eyX, new Object[]{tCVar}));
                break;
        }
        m11080aZ(tCVar);
    }

    /* renamed from: c */
    public C5663aRz[] mo25c() {
        return _m_fields;
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Price is Combined")
    @C5566aOg
    /* renamed from: dF */
    public void mo6518dF(boolean z) {
        switch (bFf().mo6893i(eyT)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, eyT, new Object[]{new Boolean(z)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, eyT, new Object[]{new Boolean(z)}));
                break;
        }
        m11093dE(z);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Price in Hoplons")
    @C5566aOg
    /* renamed from: di */
    public void mo6519di(long j) {
        switch (bFf().mo6893i(bxk)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, bxk, new Object[]{new Long(j)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, bxk, new Object[]{new Long(j)}));
                break;
        }
        m11094dh(j);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Price in Taels")
    @C5566aOg
    /* renamed from: fW */
    public void mo6520fW(long j) {
        switch (bFf().mo6893i(eyR)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, eyR, new Object[]{new Long(j)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, eyR, new Object[]{new Long(j)}));
                break;
        }
        m11098fV(j);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, aYS = "Consistency", displayName = "Handle (Consistency checker)")
    public String getHandle() {
        switch (bFf().mo6893i(f2000bP)) {
            case 0:
                return null;
            case 2:
                return (String) bFf().mo5606d(new aCE(this, f2000bP, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f2000bP, new Object[0]));
                break;
        }
        return m11083ar();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, aYS = "Consistency", displayName = "Handle (Consistency checker)")
    @C5566aOg
    public void setHandle(String str) {
        switch (bFf().mo6893i(f2001bQ)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f2001bQ, new Object[]{str}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f2001bQ, new Object[]{str}));
                break;
        }
        m11085b(str);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Prefix (for mesh resource resolution)")
    public String getPrefix() {
        switch (bFf().mo6893i(f2004ff)) {
            case 0:
                return null;
            case 2:
                return (String) bFf().mo5606d(new aCE(this, f2004ff, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f2004ff, new Object[0]));
                break;
        }
        return m11087bD();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Prefix (for mesh resource resolution)")
    @C5566aOg
    public void setPrefix(String str) {
        switch (bFf().mo6893i(f2005fg)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f2005fg, new Object[]{str}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f2005fg, new Object[]{str}));
                break;
        }
        m11099g(str);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Priority")
    public int getPriority() {
        switch (bFf().mo6893i(eyO)) {
            case 0:
                return 0;
            case 2:
                return ((Integer) bFf().mo5606d(new aCE(this, eyO, new Object[0]))).intValue();
            case 3:
                bFf().mo5606d(new aCE(this, eyO, new Object[0]));
                break;
        }
        return bDT();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Priority")
    @C5566aOg
    public void setPriority(int i) {
        switch (bFf().mo6893i(eyN)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, eyN, new Object[]{new Integer(i)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, eyN, new Object[]{new Integer(i)}));
                break;
        }
        m11113oZ(i);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Prefix (for texture resource resolution)")
    @C5566aOg
    /* renamed from: hv */
    public void mo6523hv(String str) {
        switch (bFf().mo6893i(eyP)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, eyP, new Object[]{str}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, eyP, new Object[]{str}));
                break;
        }
        m11103hu(str);
    }

    @ClientOnly
    /* renamed from: hx */
    public String mo6524hx(String str) {
        switch (bFf().mo6893i(ezj)) {
            case 0:
                return null;
            case 2:
                return (String) bFf().mo5606d(new aCE(this, ezj, new Object[]{str}));
            case 3:
                bFf().mo5606d(new aCE(this, ezj, new Object[]{str}));
                break;
        }
        return m11104hw(str);
    }

    @C3248pc(aYR = C3248pc.C3250b.ADDER, displayName = "Forbidden Appearances")
    /* renamed from: i */
    public void mo6525i(AvatarAppearanceType t) {
        switch (bFf().mo6893i(eyZ)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, eyZ, new Object[]{t}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, eyZ, new Object[]{t}));
                break;
        }
        m11100h(t);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Cloth Name")
    @C5566aOg
    /* renamed from: jC */
    public void mo6526jC(I18NString i18NString) {
        switch (bFf().mo6893i(eyM)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, eyM, new Object[]{i18NString}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, eyM, new Object[]{i18NString}));
                break;
        }
        m11106jB(i18NString);
    }

    @C3248pc(aYR = C3248pc.C3250b.REMOVER, displayName = "Forbidden Appearances")
    /* renamed from: k */
    public void mo6527k(AvatarAppearanceType t) {
        switch (bFf().mo6893i(eza)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, eza, new Object[]{t}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, eza, new Object[]{t}));
                break;
        }
        m11105j(t);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Cloth Name")
    /* renamed from: ke */
    public I18NString mo6528ke() {
        switch (bFf().mo6893i(f2011zT)) {
            case 0:
                return null;
            case 2:
                return (I18NString) bFf().mo5606d(new aCE(this, f2011zT, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f2011zT, new Object[0]));
                break;
        }
        return m11109kd();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Cloth Category")
    @C5566aOg
    /* renamed from: o */
    public void mo6529o(ClothCategory dHVar) {
        switch (bFf().mo6893i(eyV)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, eyV, new Object[]{dHVar}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, eyV, new Object[]{dHVar}));
                break;
        }
        m11111n(dHVar);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Icon resource")
    /* renamed from: sK */
    public Asset mo6530sK() {
        switch (bFf().mo6893i(f1994Ob)) {
            case 0:
                return null;
            case 2:
                return (Asset) bFf().mo5606d(new aCE(this, f1994Ob, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f1994Ob, new Object[0]));
                break;
        }
        return m11115sJ();
    }

    public String toString() {
        switch (bFf().mo6893i(_f_toString_0020_0028_0029Ljava_002flang_002fString_003b)) {
            case 0:
                return null;
            case 2:
                return (String) bFf().mo5606d(new aCE(this, _f_toString_0020_0028_0029Ljava_002flang_002fString_003b, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, _f_toString_0020_0028_0029Ljava_002flang_002fString_003b, new Object[0]));
                break;
        }
        return m11084au();
    }

    @C3248pc(aYR = C3248pc.C3250b.ADDER, displayName = "Sectors hidden")
    /* renamed from: u */
    public void mo6534u(AvatarSector nw) {
        switch (bFf().mo6893i(ezc)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, ezc, new Object[]{nw}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, ezc, new Object[]{nw}));
                break;
        }
        m11116t(nw);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Icon resource")
    @C5566aOg
    /* renamed from: v */
    public void mo6535v(Asset tCVar) {
        switch (bFf().mo6893i(f1995Oc)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f1995Oc, new Object[]{tCVar}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f1995Oc, new Object[]{tCVar}));
                break;
        }
        m11118u(tCVar);
    }

    @C3248pc(aYR = C3248pc.C3250b.REMOVER, displayName = "Sectors hidden")
    /* renamed from: w */
    public void mo6536w(AvatarSector nw) {
        switch (bFf().mo6893i(ezd)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, ezd, new Object[]{nw}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, ezd, new Object[]{nw}));
                break;
        }
        m11119v(nw);
    }

    @C0064Am(aul = "5253ca2264a737282d03b73a8b16844c", aum = 0)
    /* renamed from: ap */
    private UUID m11082ap() {
        return m11081an();
    }

    @C0064Am(aul = "6afc989f9511a5087d4f1677bbb368d7", aum = 0)
    /* renamed from: b */
    private void m11086b(UUID uuid) {
        m11078a(uuid);
    }

    /* renamed from: S */
    public void mo10S() {
        super.mo10S();
        m11078a(UUID.randomUUID());
    }

    @C0064Am(aul = "6cbb652b01f9f8ad03ebf8bd8c34c543", aum = 0)
    /* renamed from: au */
    private String m11084au() {
        if (m11107kb() == null) {
            return null;
        }
        return m11107kb().get();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Cloth Name")
    @C0064Am(aul = "5186179060443c7b0c505e57809d84a3", aum = 0)
    /* renamed from: kd */
    private I18NString m11109kd() {
        return m11107kb();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Priority")
    @C0064Am(aul = "23b1852c459a4d722f07d3c36adc34e0", aum = 0)
    private int bDT() {
        return bDJ();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Prefix (for mesh resource resolution)")
    @C0064Am(aul = "6c70715a6a6c595dbedd2094f8514f27", aum = 0)
    /* renamed from: bD */
    private String m11087bD() {
        return m11089bf();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Prefix (for texture resource resolution)")
    @C0064Am(aul = "cbe9f2c3e079ecb5356d0c1152d07625", aum = 0)
    private String bDU() {
        return bDK();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Price in Hoplons")
    @C0064Am(aul = "7490003b55264f1934bca216d18ca1ab", aum = 0)
    private long ajT() {
        return bDQ();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Price in Taels")
    @C0064Am(aul = "1ddcb12a19b8f6d62b57ca9e025f984c", aum = 0)
    private long bDW() {
        return bDR();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Price is Combined")
    @C0064Am(aul = "597d750b2fcefb1610be06a6e2c9458e", aum = 0)
    private boolean bDY() {
        return bDS();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Cloth Category")
    @C0064Am(aul = "7debc66dbe64011fdcd9420851407ba6", aum = 0)
    private ClothCategory bEa() {
        return bDL();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Icon resource")
    @C0064Am(aul = "2fa7c7f832b3990115aec77ef1024c67", aum = 0)
    /* renamed from: sJ */
    private Asset m11115sJ() {
        return m11114sG();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Material asset")
    @C0064Am(aul = "b53242e69555638229ad897a03a65558", aum = 0)
    private Asset bEc() {
        return bDP();
    }

    @C3248pc(aYR = C3248pc.C3250b.ADDER, displayName = "Forbidden Appearances")
    @C0064Am(aul = "044b6f592a678cf07f361c0bacca1e12", aum = 0)
    /* renamed from: h */
    private void m11100h(AvatarAppearanceType t) {
        bDM().add(t);
    }

    @C3248pc(aYR = C3248pc.C3250b.REMOVER, displayName = "Forbidden Appearances")
    @C0064Am(aul = "66dec2cc51c087fd4d744d1b8d815e8c", aum = 0)
    /* renamed from: j */
    private void m11105j(AvatarAppearanceType t) {
        bDM().remove(t);
    }

    @C3248pc(aYR = C3248pc.C3250b.ITERATOR, displayName = "Forbidden Appearances")
    @C0064Am(aul = "c2505b9bc10bf40b41d78f53140f25dd", aum = 0)
    private Set<AvatarAppearanceType> bEe() {
        return Collections.unmodifiableSet(bDM());
    }

    @C3248pc(aYR = C3248pc.C3250b.ADDER, displayName = "Sectors hidden")
    @C0064Am(aul = "a8b36f6ccadc27c6332153b9375c7b8b", aum = 0)
    /* renamed from: t */
    private void m11116t(AvatarSector nw) {
        bDO().add(nw);
    }

    @C3248pc(aYR = C3248pc.C3250b.REMOVER, displayName = "Sectors hidden")
    @C0064Am(aul = "3ea1c73bc278dcf8360961f898063498", aum = 0)
    /* renamed from: v */
    private void m11119v(AvatarSector nw) {
        bDO().remove(nw);
    }

    @C3248pc(aYR = C3248pc.C3250b.ITERATOR, displayName = "Sectors hidden")
    @C0064Am(aul = "c7d1a55a61b4545d05658dfc34623d2d", aum = 0)
    private Set<AvatarSector> bEg() {
        return Collections.unmodifiableSet(bDO());
    }

    @C3248pc(aYR = C3248pc.C3250b.ADDER, displayName = "Component pieces for this cloth")
    @C0064Am(aul = "5ced61d4ef6f0082fc8152afddc60e90", aum = 0)
    /* renamed from: I */
    private void m11070I(BodyPiece qb) {
        bDN().add(qb);
    }

    @C3248pc(aYR = C3248pc.C3250b.REMOVER, displayName = "Component pieces for this cloth")
    @C0064Am(aul = "56649b414759114eb4ef24eb22a33967", aum = 0)
    /* renamed from: K */
    private void m11071K(BodyPiece qb) {
        bDN().remove(qb);
    }

    @C3248pc(aYR = C3248pc.C3250b.ITERATOR, displayName = "Component pieces for this cloth")
    @C0064Am(aul = "8f13cce651b76601867cbd1f5ceac864", aum = 0)
    private Set<BodyPiece> bEi() {
        return Collections.unmodifiableSet(bDN());
    }

    @C0064Am(aul = "7e5b021243759c85bf3214bd4b10fd68", aum = 0)
    /* renamed from: C */
    private boolean m11069C(Cloth wj) {
        if (!wj.equals(this) && bEb().mo15442b(wj.bEb())) {
            return true;
        }
        return false;
    }

    @C0064Am(aul = "e03a23a3580b92cee24d7eaa2a2a8b18", aum = 0)
    @ClientOnly
    /* renamed from: hw */
    private String m11104hw(String str) {
        if (bEj().isEmpty()) {
            return null;
        }
        String str2 = String.valueOf(ala().aLa().aFW()) + str + "/" + m11089bf() + ".GR2";
        if (new File(ala().ald().aVX().getPath(), str2).exists()) {
            return str2;
        }
        return null;
    }

    @C0064Am(aul = "13a38b3a1b1e75422082c2050807cbcb", aum = 0)
    @ClientOnly
    /* renamed from: a */
    private String m11077a(AvatarSector.C0955a aVar, C3845vi.C3847b bVar, String str) {
        String path = ala().ald().aVX().getPath();
        String aFY = ala().aLa().aFY();
        String str2 = String.valueOf(aFY) + str + "/" + bDK() + aVar.getSuffix() + bVar.getSuffix() + eyt;
        if (new File(path, str2).exists()) {
            return str2;
        }
        String str3 = String.valueOf(aFY) + bDK() + "/" + bDK() + aVar.getSuffix() + bVar.getSuffix() + eyt;
        if (!new File(path, str3).exists()) {
            return null;
        }
        return str3;
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, aYS = "Consistency", displayName = "Handle (Consistency checker)")
    @C0064Am(aul = "fad96c3d214d3f18fbfc2369930db5c1", aum = 0)
    /* renamed from: ar */
    private String m11083ar() {
        return m11108kc();
    }
}
