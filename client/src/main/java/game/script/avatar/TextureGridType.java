package game.script.avatar;

import game.network.message.externalizable.aCE;
import game.script.resource.Asset;
import logic.baa.*;
import logic.data.mbean.aHF;
import logic.res.code.C5640aRc;
import logic.res.code.C5663aRz;
import logic.res.code.C6494any;
import logic.res.html.C2491fm;
import p001a.*;

import java.util.Collection;

@C0566Hp
@C5511aMd
@C6485anp
/* renamed from: a.FN */
/* compiled from: a */
public class TextureGridType extends BaseTextureGridType implements C1616Xf {
    public static final int _m_fieldCount = 0;
    public static final C5663aRz[] _m_fields = null;
    public static final int _m_methodCount = 0;
    public static final C2491fm[] _m_methods = null;
    public static final C2491fm cWA = null;
    public static final C2491fm cWB = null;
    public static final C2491fm cWC = null;
    public static final C2491fm cWD = null;
    public static final C2491fm cWE = null;
    public static final C2491fm cWF = null;
    public static final C5663aRz cWw = null;
    public static final C5663aRz cWx = null;
    public static final C5663aRz cWy = null;
    public static final C2491fm cWz = null;
    /* renamed from: dN */
    public static final C2491fm f565dN = null;
    public static final long serialVersionUID = 0;
    public static C6494any ___iScriptClass;
    @C0064Am(aul = "209d3b239352351a88bbff5a430c292f", aum = 0)
    private static Asset cWv;
    @C0064Am(aul = "ecbdeaa5d5eb27a7a244be4c83031a7a", aum = 1)
    private static int cellHeight;
    @C0064Am(aul = "a8346aa54e7fd09e546f42dfd6f359ff", aum = 2)
    private static int cellWidth;

    static {
        m3168V();
    }

    public TextureGridType() {
        super((C5540aNg) null);
        super.mo10S();
    }

    public TextureGridType(C5540aNg ang) {
        super(ang);
    }

    /* renamed from: V */
    static void m3168V() {
        serialVersionUID = (long) 1;
        _m_fieldCount = BaseTextureGridType._m_fieldCount + 3;
        _m_methodCount = BaseTextureGridType._m_methodCount + 8;
        int i = BaseTextureGridType._m_fieldCount;
        C5663aRz[] arzArr = new C5663aRz[(i + 3)];
        C5663aRz b = C5640aRc.m17844b(TextureGridType.class, "209d3b239352351a88bbff5a430c292f", i);
        cWw = b;
        arzArr[i] = b;
        int i2 = i + 1;
        C5663aRz b2 = C5640aRc.m17844b(TextureGridType.class, "ecbdeaa5d5eb27a7a244be4c83031a7a", i2);
        cWx = b2;
        arzArr[i2] = b2;
        int i3 = i2 + 1;
        C5663aRz b3 = C5640aRc.m17844b(TextureGridType.class, "a8346aa54e7fd09e546f42dfd6f359ff", i3);
        cWy = b3;
        arzArr[i3] = b3;
        int i4 = i3 + 1;
        _m_fields = arzArr;
        C1634Xv.m11725a((Object[]) BaseTextureGridType._m_fields, (Object[]) _m_fields);
        int i5 = BaseTextureGridType._m_methodCount;
        C2491fm[] fmVarArr = new C2491fm[(i5 + 8)];
        C2491fm a = C4105zY.m41624a(TextureGridType.class, "bbd9e3bf30ca75e7f65da447f5fa992a", i5);
        cWz = a;
        fmVarArr[i5] = a;
        int i6 = i5 + 1;
        C2491fm a2 = C4105zY.m41624a(TextureGridType.class, "0b097d4b8916544f7018254779fa49bd", i6);
        cWA = a2;
        fmVarArr[i6] = a2;
        int i7 = i6 + 1;
        C2491fm a3 = C4105zY.m41624a(TextureGridType.class, "03b13ccdc1bb8db3ff8307e5f82649ba", i7);
        cWB = a3;
        fmVarArr[i7] = a3;
        int i8 = i7 + 1;
        C2491fm a4 = C4105zY.m41624a(TextureGridType.class, "14b2d83254f380e73a0fa4215f131dc3", i8);
        cWC = a4;
        fmVarArr[i8] = a4;
        int i9 = i8 + 1;
        C2491fm a5 = C4105zY.m41624a(TextureGridType.class, "f2ba2f16d2fa1b3fb2bf7d1a9de70a72", i9);
        cWD = a5;
        fmVarArr[i9] = a5;
        int i10 = i9 + 1;
        C2491fm a6 = C4105zY.m41624a(TextureGridType.class, "1507e39368f190bb53f55512066f7bd7", i10);
        cWE = a6;
        fmVarArr[i10] = a6;
        int i11 = i10 + 1;
        C2491fm a7 = C4105zY.m41624a(TextureGridType.class, "53c87615e45835e956ec4c4f3c09464f", i11);
        cWF = a7;
        fmVarArr[i11] = a7;
        int i12 = i11 + 1;
        C2491fm a8 = C4105zY.m41624a(TextureGridType.class, "1fd6c02934cf6a91a8852dfdfc805ca8", i12);
        f565dN = a8;
        fmVarArr[i12] = a8;
        int i13 = i12 + 1;
        _m_methods = fmVarArr;
        C1634Xv.m11725a((Object[]) BaseTextureGridType._m_methods, (Object[]) _m_methods);
        ___iScriptClass = aUO.m18566a(TextureGridType.class, aHF.class, _m_fields, _m_methods);
    }

    private Asset aPJ() {
        return (Asset) bFf().mo5608dq().mo3214p(cWw);
    }

    private int aPK() {
        return bFf().mo5608dq().mo3212n(cWx);
    }

    private int aPL() {
        return bFf().mo5608dq().mo3212n(cWy);
    }

    /* renamed from: an */
    private void m3170an(Asset tCVar) {
        bFf().mo5608dq().mo3197f(cWw, tCVar);
    }

    /* renamed from: hk */
    private void m3172hk(int i) {
        bFf().mo5608dq().mo3183b(cWx, i);
    }

    /* renamed from: hl */
    private void m3173hl(int i) {
        bFf().mo5608dq().mo3183b(cWy, i);
    }

    /* renamed from: T */
    public C6494any mo11T() {
        return ___iScriptClass;
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Cell Width")
    /* renamed from: U */
    public void mo2112U(int i) {
        switch (bFf().mo6893i(cWE)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, cWE, new Object[]{new Integer(i)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, cWE, new Object[]{new Integer(i)}));
                break;
        }
        m3175ho(i);
    }

    /* renamed from: U */
    public C2491fm[] mo12U() {
        return _m_methods;
    }

    /* renamed from: W */
    public Object mo13W() {
        return new aHF(this);
    }

    /* renamed from: a */
    public Object mo14a(C0495Gr gr) {
        Object[] args = gr.getArgs();
        switch (gr.mo2417hq() - BaseTextureGridType._m_methodCount) {
            case 0:
                return aPM();
            case 1:
                m3171ao((Asset) args[0]);
                return null;
            case 2:
                return new Integer(aPO());
            case 3:
                m3174hm(((Integer) args[0]).intValue());
                return null;
            case 4:
                return new Integer(aPQ());
            case 5:
                m3175ho(((Integer) args[0]).intValue());
                return null;
            case 6:
                return aPR();
            case 7:
                return m3169aT();
            default:
                return super.mo14a(gr);
        }
    }

    /* renamed from: a */
    public void mo15a(Collection collection, C0495Gr gr) {
        super.mo15a(collection, gr);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Source Texture")
    public Asset aPN() {
        switch (bFf().mo6893i(cWz)) {
            case 0:
                return null;
            case 2:
                return (Asset) bFf().mo5606d(new aCE(this, cWz, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, cWz, new Object[0]));
                break;
        }
        return aPM();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Cell Height")
    public int aPP() {
        switch (bFf().mo6893i(cWB)) {
            case 0:
                return 0;
            case 2:
                return ((Integer) bFf().mo5606d(new aCE(this, cWB, new Object[0]))).intValue();
            case 3:
                bFf().mo5606d(new aCE(this, cWB, new Object[0]));
                break;
        }
        return aPO();
    }

    public TextureGrid aPS() {
        switch (bFf().mo6893i(cWF)) {
            case 0:
                return null;
            case 2:
                return (TextureGrid) bFf().mo5606d(new aCE(this, cWF, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, cWF, new Object[0]));
                break;
        }
        return aPR();
    }

    /* access modifiers changed from: protected */
    /* renamed from: aU */
    public <T extends aDJ> T mo745aU() {
        switch (bFf().mo6893i(f565dN)) {
            case 0:
                return null;
            case 2:
                return (aDJ) bFf().mo5606d(new aCE(this, f565dN, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f565dN, new Object[0]));
                break;
        }
        return m3169aT();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Source Texture")
    /* renamed from: ap */
    public void mo2116ap(Asset tCVar) {
        switch (bFf().mo6893i(cWA)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, cWA, new Object[]{tCVar}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, cWA, new Object[]{tCVar}));
                break;
        }
        m3171ao(tCVar);
    }

    /* renamed from: c */
    public C5663aRz[] mo25c() {
        return _m_fields;
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Cell Width")
    /* renamed from: gw */
    public int mo2117gw() {
        switch (bFf().mo6893i(cWD)) {
            case 0:
                return 0;
            case 2:
                return ((Integer) bFf().mo5606d(new aCE(this, cWD, new Object[0]))).intValue();
            case 3:
                bFf().mo5606d(new aCE(this, cWD, new Object[0]));
                break;
        }
        return aPQ();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Cell Height")
    /* renamed from: hn */
    public void mo2118hn(int i) {
        switch (bFf().mo6893i(cWC)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, cWC, new Object[]{new Integer(i)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, cWC, new Object[]{new Integer(i)}));
                break;
        }
        m3174hm(i);
    }

    /* renamed from: S */
    public void mo10S() {
        super.mo10S();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Source Texture")
    @C0064Am(aul = "bbd9e3bf30ca75e7f65da447f5fa992a", aum = 0)
    private Asset aPM() {
        return aPJ();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Source Texture")
    @C0064Am(aul = "0b097d4b8916544f7018254779fa49bd", aum = 0)
    /* renamed from: ao */
    private void m3171ao(Asset tCVar) {
        m3170an(tCVar);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Cell Height")
    @C0064Am(aul = "03b13ccdc1bb8db3ff8307e5f82649ba", aum = 0)
    private int aPO() {
        return aPK();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Cell Height")
    @C0064Am(aul = "14b2d83254f380e73a0fa4215f131dc3", aum = 0)
    /* renamed from: hm */
    private void m3174hm(int i) {
        m3172hk(i);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Cell Width")
    @C0064Am(aul = "f2ba2f16d2fa1b3fb2bf7d1a9de70a72", aum = 0)
    private int aPQ() {
        return aPL();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Cell Width")
    @C0064Am(aul = "1507e39368f190bb53f55512066f7bd7", aum = 0)
    /* renamed from: ho */
    private void m3175ho(int i) {
        m3173hl(i);
    }

    @C0064Am(aul = "53c87615e45835e956ec4c4f3c09464f", aum = 0)
    private TextureGrid aPR() {
        return (TextureGrid) mo745aU();
    }

    @C0064Am(aul = "1fd6c02934cf6a91a8852dfdfc805ca8", aum = 0)
    /* renamed from: aT */
    private <T extends aDJ> T m3169aT() {
        T t = (TextureGrid) bFf().mo6865M(TextureGrid.class);
        t.mo5834a(this);
        return t;
    }
}
