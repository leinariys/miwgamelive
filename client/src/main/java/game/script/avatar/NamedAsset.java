package game.script.avatar;

import game.network.message.externalizable.aCE;
import game.script.resource.Asset;
import logic.baa.C1616Xf;
import logic.baa.C1634Xv;
import logic.baa.C5511aMd;
import logic.baa.C6485anp;
import logic.data.mbean.C6889avd;
import logic.res.code.C5640aRc;
import logic.res.code.C5663aRz;
import logic.res.code.C6494any;
import logic.res.html.C2491fm;
import p001a.*;
import taikodom.infra.script.I18NString;

import java.util.Collection;

@C5511aMd
@C6485anp
/* renamed from: a.vb */
/* compiled from: a */
public class NamedAsset extends Asset implements C1616Xf {
    public static final C2491fm _f_toString_0020_0028_0029Ljava_002flang_002fString_003b = null;
    public static final int _m_fieldCount = 0;
    public static final C5663aRz[] _m_fields = null;
    public static final int _m_methodCount = 0;
    public static final C2491fm[] _m_methods = null;
    public static final long serialVersionUID = 0;
    /* renamed from: zQ */
    public static final C5663aRz f9418zQ = null;
    /* renamed from: zT */
    public static final C2491fm f9419zT = null;
    /* renamed from: zU */
    public static final C2491fm f9420zU = null;
    public static C6494any ___iScriptClass;
    @C0064Am(aul = "a86cdf1e535291728bb7b8a94428020a", aum = 0)

    /* renamed from: zP */
    private static I18NString f9417zP;

    static {
        m40294V();
    }

    public NamedAsset() {
        super((C5540aNg) null);
        super.mo10S();
    }

    public NamedAsset(C5540aNg ang) {
        super(ang);
    }

    /* renamed from: V */
    static void m40294V() {
        serialVersionUID = (long) 1;
        _m_fieldCount = Asset._m_fieldCount + 1;
        _m_methodCount = Asset._m_methodCount + 3;
        int i = Asset._m_fieldCount;
        C5663aRz[] arzArr = new C5663aRz[(i + 1)];
        C5663aRz b = C5640aRc.m17844b(NamedAsset.class, "a86cdf1e535291728bb7b8a94428020a", i);
        f9418zQ = b;
        arzArr[i] = b;
        int i2 = i + 1;
        _m_fields = arzArr;
        C1634Xv.m11725a((Object[]) Asset._m_fields, (Object[]) _m_fields);
        int i3 = Asset._m_methodCount;
        C2491fm[] fmVarArr = new C2491fm[(i3 + 3)];
        C2491fm a = C4105zY.m41624a(NamedAsset.class, "ac4ac9a203dd1d30c6b6f00897ee2d2a", i3);
        _f_toString_0020_0028_0029Ljava_002flang_002fString_003b = a;
        fmVarArr[i3] = a;
        int i4 = i3 + 1;
        C2491fm a2 = C4105zY.m41624a(NamedAsset.class, "a6bc8a29f7a60729b02e3f63246953db", i4);
        f9419zT = a2;
        fmVarArr[i4] = a2;
        int i5 = i4 + 1;
        C2491fm a3 = C4105zY.m41624a(NamedAsset.class, "26c34b460a39bb7407d22c759cfc9a7f", i5);
        f9420zU = a3;
        fmVarArr[i5] = a3;
        int i6 = i5 + 1;
        _m_methods = fmVarArr;
        C1634Xv.m11725a((Object[]) Asset._m_methods, (Object[]) _m_methods);
        ___iScriptClass = aUO.m18566a(NamedAsset.class, C6889avd.class, _m_fields, _m_methods);
    }

    /* renamed from: d */
    private void m40296d(I18NString i18NString) {
        bFf().mo5608dq().mo3197f(f9418zQ, i18NString);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Asset Name")
    @C0064Am(aul = "26c34b460a39bb7407d22c759cfc9a7f", aum = 0)
    @C5566aOg
    /* renamed from: e */
    private void m40297e(I18NString i18NString) {
        throw new aWi(new aCE(this, f9420zU, new Object[]{i18NString}));
    }

    /* renamed from: kb */
    private I18NString m40298kb() {
        return (I18NString) bFf().mo5608dq().mo3214p(f9418zQ);
    }

    /* renamed from: T */
    public C6494any mo11T() {
        return ___iScriptClass;
    }

    /* renamed from: U */
    public C2491fm[] mo12U() {
        return _m_methods;
    }

    /* renamed from: W */
    public Object mo13W() {
        return new C6889avd(this);
    }

    /* renamed from: a */
    public Object mo14a(C0495Gr gr) {
        Object[] args = gr.getArgs();
        switch (gr.mo2417hq() - Asset._m_methodCount) {
            case 0:
                return m40295au();
            case 1:
                return m40299kd();
            case 2:
                m40297e((I18NString) args[0]);
                return null;
            default:
                return super.mo14a(gr);
        }
    }

    /* renamed from: a */
    public void mo15a(Collection collection, C0495Gr gr) {
        super.mo15a(collection, gr);
    }

    /* renamed from: c */
    public C5663aRz[] mo25c() {
        return _m_fields;
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Asset Name")
    @C5566aOg
    /* renamed from: f */
    public void mo22609f(I18NString i18NString) {
        switch (bFf().mo6893i(f9420zU)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f9420zU, new Object[]{i18NString}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f9420zU, new Object[]{i18NString}));
                break;
        }
        m40297e(i18NString);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Asset Name")
    /* renamed from: ke */
    public I18NString mo22610ke() {
        switch (bFf().mo6893i(f9419zT)) {
            case 0:
                return null;
            case 2:
                return (I18NString) bFf().mo5606d(new aCE(this, f9419zT, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f9419zT, new Object[0]));
                break;
        }
        return m40299kd();
    }

    public String toString() {
        switch (bFf().mo6893i(_f_toString_0020_0028_0029Ljava_002flang_002fString_003b)) {
            case 0:
                return null;
            case 2:
                return (String) bFf().mo5606d(new aCE(this, _f_toString_0020_0028_0029Ljava_002flang_002fString_003b, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, _f_toString_0020_0028_0029Ljava_002flang_002fString_003b, new Object[0]));
                break;
        }
        return m40295au();
    }

    /* renamed from: S */
    public void mo10S() {
        super.mo10S();
    }

    @C0064Am(aul = "ac4ac9a203dd1d30c6b6f00897ee2d2a", aum = 0)
    /* renamed from: au */
    private String m40295au() {
        return mo22610ke().get();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Asset Name")
    @C0064Am(aul = "a6bc8a29f7a60729b02e3f63246953db", aum = 0)
    /* renamed from: kd */
    private I18NString m40299kd() {
        return m40298kb();
    }
}
