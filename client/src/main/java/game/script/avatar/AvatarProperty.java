package game.script.avatar;

import game.network.message.externalizable.aCE;
import logic.baa.*;
import logic.data.mbean.C2424fK;
import logic.res.code.C5640aRc;
import logic.res.code.C5663aRz;
import logic.res.code.C6494any;
import logic.res.html.C2491fm;
import p001a.*;
import taikodom.infra.script.I18NString;

import java.util.Collection;

@C5511aMd
@C6485anp
/* renamed from: a.aMc  reason: case insensitive filesystem */
/* compiled from: a */
public abstract class AvatarProperty extends aDJ implements C1616Xf {
    public static final C2491fm _f_toString_0020_0028_0029Ljava_002flang_002fString_003b = null;
    public static final int _m_fieldCount = 0;
    public static final C5663aRz[] _m_fields = null;
    public static final int _m_methodCount = 0;
    public static final C2491fm[] _m_methods = null;
    /* renamed from: en */
    public static final C5663aRz f3379en = null;
    /* renamed from: fc */
    public static final C2491fm f3380fc = null;
    public static final C2491fm iol = null;
    public static final long serialVersionUID = 0;
    /* renamed from: zQ */
    public static final C5663aRz f3382zQ = null;
    /* renamed from: zT */
    public static final C2491fm f3383zT = null;
    /* renamed from: zU */
    public static final C2491fm f3384zU = null;
    public static C6494any ___iScriptClass;
    @C0064Am(aul = "43d5c0aa4637767161a6ab20c346489b", aum = 1)
    private static boolean restricted;
    @C0064Am(aul = "c99635b6da5d5135d4f4ec11107e70aa", aum = 0)

    /* renamed from: zP */
    private static I18NString f3381zP;

    static {
        m16404V();
    }

    public AvatarProperty() {
        super((C5540aNg) null);
        super.mo10S();
    }

    public AvatarProperty(C5540aNg ang) {
        super(ang);
    }

    /* renamed from: V */
    static void m16404V() {
        serialVersionUID = (long) 1;
        _m_fieldCount = aDJ._m_fieldCount + 2;
        _m_methodCount = aDJ._m_methodCount + 5;
        int i = aDJ._m_fieldCount;
        C5663aRz[] arzArr = new C5663aRz[(i + 2)];
        C5663aRz b = C5640aRc.m17844b(AvatarProperty.class, "c99635b6da5d5135d4f4ec11107e70aa", i);
        f3382zQ = b;
        arzArr[i] = b;
        int i2 = i + 1;
        C5663aRz b2 = C5640aRc.m17844b(AvatarProperty.class, "43d5c0aa4637767161a6ab20c346489b", i2);
        f3379en = b2;
        arzArr[i2] = b2;
        int i3 = i2 + 1;
        _m_fields = arzArr;
        C1634Xv.m11725a((Object[]) aDJ._m_fields, (Object[]) _m_fields);
        int i4 = aDJ._m_methodCount;
        C2491fm[] fmVarArr = new C2491fm[(i4 + 5)];
        C2491fm a = C4105zY.m41624a(AvatarProperty.class, "2a8ca8067981f4b989a273c342b54d0d", i4);
        _f_toString_0020_0028_0029Ljava_002flang_002fString_003b = a;
        fmVarArr[i4] = a;
        int i5 = i4 + 1;
        C2491fm a2 = C4105zY.m41624a(AvatarProperty.class, "4b3fa3efa77b5b6c9b2e79370e246e1e", i5);
        f3383zT = a2;
        fmVarArr[i5] = a2;
        int i6 = i5 + 1;
        C2491fm a3 = C4105zY.m41624a(AvatarProperty.class, "884e41ccc6f033b1eecfb7a91027dd66", i6);
        f3384zU = a3;
        fmVarArr[i6] = a3;
        int i7 = i6 + 1;
        C2491fm a4 = C4105zY.m41624a(AvatarProperty.class, "1874d791891438ff020ad3df09a7ed01", i7);
        iol = a4;
        fmVarArr[i7] = a4;
        int i8 = i7 + 1;
        C2491fm a5 = C4105zY.m41624a(AvatarProperty.class, "4899e37c7aa3e01a05fa3147ffa07ca9", i8);
        f3380fc = a5;
        fmVarArr[i8] = a5;
        int i9 = i8 + 1;
        _m_methods = fmVarArr;
        C1634Xv.m11725a((Object[]) aDJ._m_methods, (Object[]) _m_methods);
        ___iScriptClass = aUO.m18566a(AvatarProperty.class, C2424fK.class, _m_fields, _m_methods);
    }

    /* renamed from: bd */
    private boolean m16406bd() {
        return bFf().mo5608dq().mo3201h(f3379en);
    }

    /* renamed from: d */
    private void m16407d(I18NString i18NString) {
        bFf().mo5608dq().mo3197f(f3382zQ, i18NString);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Avatar Property Name")
    @C0064Am(aul = "884e41ccc6f033b1eecfb7a91027dd66", aum = 0)
    @C5566aOg
    /* renamed from: e */
    private void m16408e(I18NString i18NString) {
        throw new aWi(new aCE(this, f3384zU, new Object[]{i18NString}));
    }

    /* renamed from: h */
    private void m16409h(boolean z) {
        bFf().mo5608dq().mo3153a(f3379en, z);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Avatar Property Restricted ?")
    @C0064Am(aul = "4899e37c7aa3e01a05fa3147ffa07ca9", aum = 0)
    @C5566aOg
    /* renamed from: j */
    private void m16410j(boolean z) {
        throw new aWi(new aCE(this, f3380fc, new Object[]{new Boolean(z)}));
    }

    /* renamed from: kb */
    private I18NString m16411kb() {
        return (I18NString) bFf().mo5608dq().mo3214p(f3382zQ);
    }

    /* renamed from: T */
    public C6494any mo11T() {
        return ___iScriptClass;
    }

    /* renamed from: U */
    public C2491fm[] mo12U() {
        return _m_methods;
    }

    /* renamed from: W */
    public Object mo13W() {
        return new C2424fK(this);
    }

    /* renamed from: a */
    public Object mo14a(C0495Gr gr) {
        Object[] args = gr.getArgs();
        switch (gr.mo2417hq() - aDJ._m_methodCount) {
            case 0:
                return m16405au();
            case 1:
                return m16412kd();
            case 2:
                m16408e((I18NString) args[0]);
                return null;
            case 3:
                return new Boolean(diO());
            case 4:
                m16410j(((Boolean) args[0]).booleanValue());
                return null;
            default:
                return super.mo14a(gr);
        }
    }

    /* renamed from: a */
    public void mo15a(Collection collection, C0495Gr gr) {
        super.mo15a(collection, gr);
    }

    /* renamed from: c */
    public C5663aRz[] mo25c() {
        return _m_fields;
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Avatar Property Name")
    @C5566aOg
    /* renamed from: f */
    public void mo10068f(I18NString i18NString) {
        switch (bFf().mo6893i(f3384zU)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f3384zU, new Object[]{i18NString}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f3384zU, new Object[]{i18NString}));
                break;
        }
        m16408e(i18NString);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Avatar Property Restricted ?")
    public boolean isRestricted() {
        switch (bFf().mo6893i(iol)) {
            case 0:
                return false;
            case 2:
                return ((Boolean) bFf().mo5606d(new aCE(this, iol, new Object[0]))).booleanValue();
            case 3:
                bFf().mo5606d(new aCE(this, iol, new Object[0]));
                break;
        }
        return diO();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Avatar Property Restricted ?")
    @C5566aOg
    public void setRestricted(boolean z) {
        switch (bFf().mo6893i(f3380fc)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f3380fc, new Object[]{new Boolean(z)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f3380fc, new Object[]{new Boolean(z)}));
                break;
        }
        m16410j(z);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Avatar Property Name")
    /* renamed from: ke */
    public I18NString mo10070ke() {
        switch (bFf().mo6893i(f3383zT)) {
            case 0:
                return null;
            case 2:
                return (I18NString) bFf().mo5606d(new aCE(this, f3383zT, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f3383zT, new Object[0]));
                break;
        }
        return m16412kd();
    }

    public String toString() {
        switch (bFf().mo6893i(_f_toString_0020_0028_0029Ljava_002flang_002fString_003b)) {
            case 0:
                return null;
            case 2:
                return (String) bFf().mo5606d(new aCE(this, _f_toString_0020_0028_0029Ljava_002flang_002fString_003b, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, _f_toString_0020_0028_0029Ljava_002flang_002fString_003b, new Object[0]));
                break;
        }
        return m16405au();
    }

    /* renamed from: S */
    public void mo10S() {
        super.mo10S();
    }

    @C0064Am(aul = "2a8ca8067981f4b989a273c342b54d0d", aum = 0)
    /* renamed from: au */
    private String m16405au() {
        return mo10070ke().get();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Avatar Property Name")
    @C0064Am(aul = "4b3fa3efa77b5b6c9b2e79370e246e1e", aum = 0)
    /* renamed from: kd */
    private I18NString m16412kd() {
        return m16411kb();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Avatar Property Restricted ?")
    @C0064Am(aul = "1874d791891438ff020ad3df09a7ed01", aum = 0)
    private boolean diO() {
        return m16406bd();
    }
}
