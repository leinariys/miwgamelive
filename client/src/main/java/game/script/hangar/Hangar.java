package game.script.hangar;

import game.network.message.externalizable.aCE;
import game.network.message.serializable.C3438ra;
import game.script.player.Player;
import game.script.ship.Ship;
import game.script.ship.Station;
import logic.baa.C1616Xf;
import logic.baa.C1634Xv;
import logic.baa.C5511aMd;
import logic.baa.C6485anp;
import logic.data.mbean.C4103zW;
import logic.res.code.C5640aRc;
import logic.res.code.C5663aRz;
import logic.res.code.C6494any;
import logic.res.html.C2491fm;
import p001a.*;

import java.util.Collection;

@C5511aMd
@C6485anp
/* renamed from: a.kR */
/* compiled from: a */
public class Hangar extends TaikodomObject implements C1616Xf {

    /* renamed from: Lm */
    public static final C2491fm f8420Lm = null;
    public static final C2491fm _f_dispose_0020_0028_0029V = null;
    public static final C2491fm _f_onResurrect_0020_0028_0029V = null;
    public static final C2491fm _f_toString_0020_0028_0029Ljava_002flang_002fString_003b = null;
    public static final int _m_fieldCount = 0;
    public static final C5663aRz[] _m_fields = null;
    public static final int _m_methodCount = 0;
    public static final C2491fm[] _m_methods = null;
    public static final int avE = 2;
    public static final C5663aRz avF = null;
    public static final C5663aRz avH = null;
    public static final C5663aRz avJ = null;
    public static final C2491fm avK = null;
    public static final C2491fm avL = null;
    public static final C2491fm avM = null;
    public static final C2491fm avN = null;
    public static final C2491fm avO = null;
    public static final C2491fm avP = null;
    public static final C2491fm avQ = null;
    public static final C2491fm avR = null;
    public static final C2491fm avS = null;
    public static final C2491fm avT = null;
    /* renamed from: gQ */
    public static final C2491fm f8421gQ = null;
    /* renamed from: kX */
    public static final C5663aRz f8423kX = null;
    /* renamed from: lm */
    public static final C2491fm f8424lm = null;
    public static final long serialVersionUID = 0;
    public static C6494any ___iScriptClass = null;
    @C0064Am(aul = "39ac92121912c026efd5241bf57b77be", aum = 2)
    private static C3438ra<Bay> avG;
    @C0064Am(aul = "3aa5bd42d6008518d1fa5bfb6c06a417", aum = 3)
    private static int avI;
    @C0064Am(aul = "d415aa0a380f1256fb64f2f749397637", aum = 1)

    /* renamed from: iH */
    private static Station f8422iH;
    @C0064Am(aul = "9a04dbceebe9e09f587200d31ab410fd", aum = 0)

    /* renamed from: nj */
    private static Player f8425nj;

    static {
        m34394V();
    }

    public Hangar() {
        super((C5540aNg) null);
        super.mo10S();
    }

    public Hangar(C5540aNg ang) {
        super(ang);
    }

    public Hangar(Player aku, Station bf) {
        super((C5540aNg) null);
        super._m_script_init(aku, bf);
    }

    /* renamed from: V */
    static void m34394V() {
        serialVersionUID = (long) 1;
        _m_fieldCount = TaikodomObject._m_fieldCount + 4;
        _m_methodCount = TaikodomObject._m_methodCount + 16;
        int i = TaikodomObject._m_fieldCount;
        C5663aRz[] arzArr = new C5663aRz[(i + 4)];
        C5663aRz b = C5640aRc.m17844b(Hangar.class, "9a04dbceebe9e09f587200d31ab410fd", i);
        avF = b;
        arzArr[i] = b;
        int i2 = i + 1;
        C5663aRz b2 = C5640aRc.m17844b(Hangar.class, "d415aa0a380f1256fb64f2f749397637", i2);
        f8423kX = b2;
        arzArr[i2] = b2;
        int i3 = i2 + 1;
        C5663aRz b3 = C5640aRc.m17844b(Hangar.class, "39ac92121912c026efd5241bf57b77be", i3);
        avH = b3;
        arzArr[i3] = b3;
        int i4 = i3 + 1;
        C5663aRz b4 = C5640aRc.m17844b(Hangar.class, "3aa5bd42d6008518d1fa5bfb6c06a417", i4);
        avJ = b4;
        arzArr[i4] = b4;
        int i5 = i4 + 1;
        _m_fields = arzArr;
        C1634Xv.m11725a((Object[]) TaikodomObject._m_fields, (Object[]) _m_fields);
        int i6 = TaikodomObject._m_methodCount;
        C2491fm[] fmVarArr = new C2491fm[(i6 + 16)];
        C2491fm a = C4105zY.m41624a(Hangar.class, "715e7b2a7c40b390b6aa57e709642e87", i6);
        _f_toString_0020_0028_0029Ljava_002flang_002fString_003b = a;
        fmVarArr[i6] = a;
        int i7 = i6 + 1;
        C2491fm a2 = C4105zY.m41624a(Hangar.class, "dfecefdbb9b5aca980a8ef49fbab7117", i7);
        avK = a2;
        fmVarArr[i7] = a2;
        int i8 = i7 + 1;
        C2491fm a3 = C4105zY.m41624a(Hangar.class, "0c31ae5d3e299aac8dfe734cb8b8d73c", i8);
        avL = a3;
        fmVarArr[i8] = a3;
        int i9 = i8 + 1;
        C2491fm a4 = C4105zY.m41624a(Hangar.class, "ba72e28a15325e9a2c8186700fd29202", i9);
        f8424lm = a4;
        fmVarArr[i9] = a4;
        int i10 = i9 + 1;
        C2491fm a5 = C4105zY.m41624a(Hangar.class, "8801712b35f0120c0a406ba8e56752cd", i10);
        avM = a5;
        fmVarArr[i10] = a5;
        int i11 = i10 + 1;
        C2491fm a6 = C4105zY.m41624a(Hangar.class, "18140773bdbec6cbc093fc0bf1078014", i11);
        f8420Lm = a6;
        fmVarArr[i11] = a6;
        int i12 = i11 + 1;
        C2491fm a7 = C4105zY.m41624a(Hangar.class, "0bee707e1b2164cd59980c0a25ca6047", i12);
        _f_dispose_0020_0028_0029V = a7;
        fmVarArr[i12] = a7;
        int i13 = i12 + 1;
        C2491fm a8 = C4105zY.m41624a(Hangar.class, "18491765741920587210733940152c93", i13);
        avN = a8;
        fmVarArr[i13] = a8;
        int i14 = i13 + 1;
        C2491fm a9 = C4105zY.m41624a(Hangar.class, "17ce86be065309e69689e9f9ec48d126", i14);
        avO = a9;
        fmVarArr[i14] = a9;
        int i15 = i14 + 1;
        C2491fm a10 = C4105zY.m41624a(Hangar.class, "e2a4c886adda54c8efdd201a11d8ca57", i15);
        avP = a10;
        fmVarArr[i15] = a10;
        int i16 = i15 + 1;
        C2491fm a11 = C4105zY.m41624a(Hangar.class, "1859c885f9c2798c2a46f7e1e12ac061", i16);
        avQ = a11;
        fmVarArr[i16] = a11;
        int i17 = i16 + 1;
        C2491fm a12 = C4105zY.m41624a(Hangar.class, "1ac5e97c2b903b4bdd790f4f3c4f82b0", i17);
        avR = a12;
        fmVarArr[i17] = a12;
        int i18 = i17 + 1;
        C2491fm a13 = C4105zY.m41624a(Hangar.class, "ae49871a252d749eba744167e4e33aeb", i18);
        avS = a13;
        fmVarArr[i18] = a13;
        int i19 = i18 + 1;
        C2491fm a14 = C4105zY.m41624a(Hangar.class, "6cbc4e16cb86c0e4be145d38b2a537ad", i19);
        _f_onResurrect_0020_0028_0029V = a14;
        fmVarArr[i19] = a14;
        int i20 = i19 + 1;
        C2491fm a15 = C4105zY.m41624a(Hangar.class, "8c0753260fb64c1a5b02e5543df0fa82", i20);
        f8421gQ = a15;
        fmVarArr[i20] = a15;
        int i21 = i20 + 1;
        C2491fm a16 = C4105zY.m41624a(Hangar.class, "f0862dae3642c7f8467769c32cd6e016", i21);
        avT = a16;
        fmVarArr[i21] = a16;
        int i22 = i21 + 1;
        _m_methods = fmVarArr;
        C1634Xv.m11725a((Object[]) TaikodomObject._m_methods, (Object[]) _m_methods);
        ___iScriptClass = aUO.m18566a(Hangar.class, C4103zW.class, _m_fields, _m_methods);
    }

    /* renamed from: KQ */
    private Player m34385KQ() {
        return (Player) bFf().mo5608dq().mo3214p(avF);
    }

    /* renamed from: KR */
    private C3438ra m34386KR() {
        return (C3438ra) bFf().mo5608dq().mo3214p(avH);
    }

    /* renamed from: KS */
    private int m34387KS() {
        return bFf().mo5608dq().mo3212n(avJ);
    }

    @C0064Am(aul = "f0862dae3642c7f8467769c32cd6e016", aum = 0)
    @C5566aOg
    /* renamed from: Ld */
    private void m34393Ld() {
        throw new aWi(new aCE(this, avT, new Object[0]));
    }

    /* renamed from: a */
    private void m34395a(Station bf) {
        bFf().mo5608dq().mo3197f(f8423kX, bf);
    }

    /* renamed from: cG */
    private void m34399cG(int i) {
        bFf().mo5608dq().mo3183b(avJ, i);
    }

    @C0064Am(aul = "e2a4c886adda54c8efdd201a11d8ca57", aum = 0)
    @C5566aOg
    /* renamed from: cH */
    private void m34400cH(int i) {
        throw new aWi(new aCE(this, avP, new Object[]{new Integer(i)}));
    }

    @C0064Am(aul = "8c0753260fb64c1a5b02e5543df0fa82", aum = 0)
    @C5566aOg
    /* renamed from: dd */
    private void m34401dd() {
        throw new aWi(new aCE(this, f8421gQ, new Object[0]));
    }

    /* renamed from: eF */
    private Station m34402eF() {
        return (Station) bFf().mo5608dq().mo3214p(f8423kX);
    }

    @C0064Am(aul = "0bee707e1b2164cd59980c0a25ca6047", aum = 0)
    @C5566aOg
    /* renamed from: fg */
    private void m34404fg() {
        throw new aWi(new aCE(this, _f_dispose_0020_0028_0029V, new Object[0]));
    }

    @C0064Am(aul = "0c31ae5d3e299aac8dfe734cb8b8d73c", aum = 0)
    @C5566aOg
    /* renamed from: g */
    private void m34405g(Station bf) {
        throw new aWi(new aCE(this, avL, new Object[]{bf}));
    }

    @C5566aOg
    /* renamed from: h */
    private void m34406h(Station bf) {
        switch (bFf().mo6893i(avL)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, avL, new Object[]{bf}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, avL, new Object[]{bf}));
                break;
        }
        m34405g(bf);
    }

    @C0064Am(aul = "18140773bdbec6cbc093fc0bf1078014", aum = 0)
    @C5566aOg
    @C2499fr
    /* renamed from: qU */
    private void m34407qU() {
        throw new aWi(new aCE(this, f8420Lm, new Object[0]));
    }

    /* renamed from: u */
    private void m34408u(Player aku) {
        bFf().mo5608dq().mo3197f(avF, aku);
    }

    @C0064Am(aul = "17ce86be065309e69689e9f9ec48d126", aum = 0)
    @C5566aOg
    /* renamed from: v */
    private void m34409v(Player aku) {
        throw new aWi(new aCE(this, avO, new Object[]{aku}));
    }

    @C5566aOg
    /* renamed from: w */
    private void m34410w(Player aku) {
        switch (bFf().mo6893i(avO)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, avO, new Object[]{aku}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, avO, new Object[]{aku}));
                break;
        }
        m34409v(aku);
    }

    /* renamed from: w */
    private void m34411w(C3438ra raVar) {
        bFf().mo5608dq().mo3197f(avH, raVar);
    }

    /* renamed from: KU */
    public C3438ra<Bay> mo20056KU() {
        switch (bFf().mo6893i(avK)) {
            case 0:
                return null;
            case 2:
                return (C3438ra) bFf().mo5606d(new aCE(this, avK, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, avK, new Object[0]));
                break;
        }
        return m34388KT();
    }

    /* renamed from: KW */
    public Bay mo20057KW() {
        switch (bFf().mo6893i(avM)) {
            case 0:
                return null;
            case 2:
                return (Bay) bFf().mo5606d(new aCE(this, avM, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, avM, new Object[0]));
                break;
        }
        return m34389KV();
    }

    /* renamed from: KY */
    public Player mo20058KY() {
        switch (bFf().mo6893i(avN)) {
            case 0:
                return null;
            case 2:
                return (Player) bFf().mo5606d(new aCE(this, avN, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, avN, new Object[0]));
                break;
        }
        return m34390KX();
    }

    /* renamed from: La */
    public boolean mo20059La() {
        switch (bFf().mo6893i(avR)) {
            case 0:
                return false;
            case 2:
                return ((Boolean) bFf().mo5606d(new aCE(this, avR, new Object[0]))).booleanValue();
            case 3:
                bFf().mo5606d(new aCE(this, avR, new Object[0]));
                break;
        }
        return m34391KZ();
    }

    /* renamed from: Lc */
    public int mo20060Lc() {
        switch (bFf().mo6893i(avS)) {
            case 0:
                return 0;
            case 2:
                return ((Integer) bFf().mo5606d(new aCE(this, avS, new Object[0]))).intValue();
            case 3:
                bFf().mo5606d(new aCE(this, avS, new Object[0]));
                break;
        }
        return m34392Lb();
    }

    @C5566aOg
    /* renamed from: Le */
    public void mo20061Le() {
        switch (bFf().mo6893i(avT)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, avT, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, avT, new Object[0]));
                break;
        }
        m34393Ld();
    }

    /* renamed from: S */
    public void mo10S() {
        super.mo10S();
    }

    /* renamed from: T */
    public C6494any mo11T() {
        return ___iScriptClass;
    }

    /* renamed from: U */
    public C2491fm[] mo12U() {
        return _m_methods;
    }

    /* renamed from: W */
    public Object mo13W() {
        return new C4103zW(this);
    }

    /* renamed from: a */
    public Object mo14a(C0495Gr gr) {
        Object[] args = gr.getArgs();
        switch (gr.mo2417hq() - TaikodomObject._m_methodCount) {
            case 0:
                return m34398au();
            case 1:
                return m34388KT();
            case 2:
                m34405g((Station) args[0]);
                return null;
            case 3:
                return m34403eS();
            case 4:
                return m34389KV();
            case 5:
                m34407qU();
                return null;
            case 6:
                m34404fg();
                return null;
            case 7:
                return m34390KX();
            case 8:
                m34409v((Player) args[0]);
                return null;
            case 9:
                m34400cH(((Integer) args[0]).intValue());
                return null;
            case 10:
                m34396a((Bay) args[0]);
                return null;
            case 11:
                return new Boolean(m34391KZ());
            case 12:
                return new Integer(m34392Lb());
            case 13:
                m34397aG();
                return null;
            case 14:
                m34401dd();
                return null;
            case 15:
                m34393Ld();
                return null;
            default:
                return super.mo14a(gr);
        }
    }

    /* renamed from: a */
    public void mo15a(Collection collection, C0495Gr gr) {
        super.mo15a(collection, gr);
    }

    /* renamed from: aH */
    public void mo70aH() {
        switch (bFf().mo6893i(_f_onResurrect_0020_0028_0029V)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, _f_onResurrect_0020_0028_0029V, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, _f_onResurrect_0020_0028_0029V, new Object[0]));
                break;
        }
        m34397aG();
    }

    /* renamed from: b */
    public void mo20063b(Bay ajm) {
        switch (bFf().mo6893i(avQ)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, avQ, new Object[]{ajm}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, avQ, new Object[]{ajm}));
                break;
        }
        m34396a(ajm);
    }

    /* renamed from: c */
    public C5663aRz[] mo25c() {
        return _m_fields;
    }

    @C5566aOg
    /* renamed from: cI */
    public void mo20064cI(int i) {
        switch (bFf().mo6893i(avP)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, avP, new Object[]{new Integer(i)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, avP, new Object[]{new Integer(i)}));
                break;
        }
        m34400cH(i);
    }

    @C5566aOg
    /* renamed from: de */
    public void mo20065de() {
        switch (bFf().mo6893i(f8421gQ)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f8421gQ, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f8421gQ, new Object[0]));
                break;
        }
        m34401dd();
    }

    @C5566aOg
    public void dispose() {
        switch (bFf().mo6893i(_f_dispose_0020_0028_0029V)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, _f_dispose_0020_0028_0029V, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, _f_dispose_0020_0028_0029V, new Object[0]));
                break;
        }
        m34404fg();
    }

    /* renamed from: eT */
    public Station mo20066eT() {
        switch (bFf().mo6893i(f8424lm)) {
            case 0:
                return null;
            case 2:
                return (Station) bFf().mo5606d(new aCE(this, f8424lm, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f8424lm, new Object[0]));
                break;
        }
        return m34403eS();
    }

    @C5566aOg
    @C2499fr
    /* renamed from: qV */
    public void mo20067qV() {
        switch (bFf().mo6893i(f8420Lm)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f8420Lm, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f8420Lm, new Object[0]));
                break;
        }
        m34407qU();
    }

    public String toString() {
        switch (bFf().mo6893i(_f_toString_0020_0028_0029Ljava_002flang_002fString_003b)) {
            case 0:
                return null;
            case 2:
                return (String) bFf().mo5606d(new aCE(this, _f_toString_0020_0028_0029Ljava_002flang_002fString_003b, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, _f_toString_0020_0028_0029Ljava_002flang_002fString_003b, new Object[0]));
                break;
        }
        return m34398au();
    }

    /* renamed from: a */
    public void mo20062a(Player aku, Station bf) {
        super.mo10S();
        m34399cG(2);
        m34410w(aku);
        m34406h(bf);
        int i = 0;
        while (true) {
            int i2 = i;
            if (i2 < m34387KS()) {
                Bay ajm = (Bay) bFf().mo6865M(Bay.class);
                ajm.mo9628f(this);
                m34386KR().add(ajm);
                i = i2 + 1;
            } else {
                return;
            }
        }
    }

    @C0064Am(aul = "715e7b2a7c40b390b6aa57e709642e87", aum = 0)
    /* renamed from: au */
    private String m34398au() {
        String str = "Hangar: [";
        int i = 0;
        while (true) {
            int i2 = i;
            if (i2 >= m34386KR().size()) {
                return String.valueOf(str) + "]";
            }
            str = String.valueOf(str) + ((Bay) m34386KR().get(i2)).toString() + ",";
            i = i2 + 1;
        }
    }

    @C0064Am(aul = "dfecefdbb9b5aca980a8ef49fbab7117", aum = 0)
    /* renamed from: KT */
    private C3438ra<Bay> m34388KT() {
        return m34386KR();
    }

    @C0064Am(aul = "ba72e28a15325e9a2c8186700fd29202", aum = 0)
    /* renamed from: eS */
    private Station m34403eS() {
        return m34402eF();
    }

    @C0064Am(aul = "8801712b35f0120c0a406ba8e56752cd", aum = 0)
    /* renamed from: KV */
    private Bay m34389KV() {
        String str;
        for (Bay ajm : mo20056KU()) {
            Ship al = ajm.mo9625al();
            if (al != null) {
                if (al.isDead()) {
                    mo8360mc("Removing Dead Ship from Bay!!");
                    mo20056KU().remove(al);
                }
            }
            return ajm;
        }
        StringBuilder sb = new StringBuilder("No empty bays available for player ");
        if (m34385KQ() == null) {
            str = "null";
        } else {
            str = String.valueOf(m34385KQ().getName()) + " - " + m34385KQ().bFY();
        }
        throw new IllegalStateException(sb.append(str).append(" at Station ").append(m34402eF() == null ? "null" : String.valueOf(m34402eF().getHandle()) + " - " + m34402eF().bFY()).toString());
    }

    @C0064Am(aul = "18491765741920587210733940152c93", aum = 0)
    /* renamed from: KX */
    private Player m34390KX() {
        return m34385KQ();
    }

    @C0064Am(aul = "1859c885f9c2798c2a46f7e1e12ac061", aum = 0)
    /* renamed from: a */
    private void m34396a(Bay ajm) {
        if (m34386KR().size() > m34387KS()) {
            m34386KR().remove(ajm);
        }
    }

    @C0064Am(aul = "1ac5e97c2b903b4bdd790f4f3c4f82b0", aum = 0)
    /* renamed from: KZ */
    private boolean m34391KZ() {
        for (Bay al : mo20056KU()) {
            if (al.mo9625al() == null) {
                return true;
            }
        }
        return false;
    }

    @C0064Am(aul = "ae49871a252d749eba744167e4e33aeb", aum = 0)
    /* renamed from: Lb */
    private int m34392Lb() {
        int i = 0;
        for (Bay al : mo20056KU()) {
            if (al.mo9625al() == null) {
                i++;
            }
        }
        return i;
    }

    @C0064Am(aul = "6cbc4e16cb86c0e4be145d38b2a537ad", aum = 0)
    /* renamed from: aG */
    private void m34397aG() {
        if (m34385KQ() == null || m34385KQ().isDisposed()) {
            m34402eF().mo640d(this);
            dispose();
            System.out.println("DISPOSED " + bFY());
        }
        super.mo70aH();
    }
}
