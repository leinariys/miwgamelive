package game.script.hangar;

import game.network.message.externalizable.C1749Zq;
import game.network.message.externalizable.aCE;
import game.script.item.Item;
import game.script.item.ItemLocation;
import game.script.item.ShipItem;
import game.script.nls.NLSItem;
import game.script.nls.NLSManager;
import game.script.nls.NLSProgression;
import game.script.ship.Ship;
import logic.aaa.C1506WA;
import logic.baa.C1616Xf;
import logic.baa.C1634Xv;
import logic.baa.C5511aMd;
import logic.baa.C6485anp;
import logic.data.mbean.C0483Gg;
import logic.res.code.C5640aRc;
import logic.res.code.C5663aRz;
import logic.res.code.C6494any;
import logic.res.html.C2491fm;
import p001a.*;

import java.util.Collection;

@C5511aMd
@C6485anp
/* renamed from: a.aJm  reason: case insensitive filesystem */
/* compiled from: a */
public class Bay extends ItemLocation implements C1616Xf {

    /* renamed from: Lm */
    public static final C2491fm f3226Lm = null;

    /* renamed from: RA */
    public static final C2491fm f3227RA = null;

    /* renamed from: RD */
    public static final C2491fm f3228RD = null;
    public static final C2491fm _f_dispose_0020_0028_0029V = null;
    /* renamed from: _f_getShip_0020_0028_0029Ltaikodom_002fgame_002fscript_002fship_002fShip_003b */
    public static final C2491fm f3229x851d656b = null;
    public static final C2491fm _f_toString_0020_0028_0029Ljava_002flang_002fString_003b = null;
    public static final int _m_fieldCount = 0;
    public static final C5663aRz[] _m_fields = null;
    public static final int _m_methodCount = 0;
    public static final C2491fm[] _m_methods = null;
    public static final C5663aRz ifL = null;
    public static final C2491fm ifM = null;
    public static final C2491fm ifN = null;
    public static final C2491fm ifO = null;
    /* renamed from: ku */
    public static final C2491fm f3230ku = null;
    public static final long serialVersionUID = 0;
    public static C6494any ___iScriptClass;
    @C0064Am(aul = "02a74869b051fc2d921f55e243cfd23d", aum = 0)
    private static Hangar cXJ;

    static {
        m15842V();
    }

    public Bay() {
        super((C5540aNg) null);
        super.mo10S();
    }

    public Bay(C5540aNg ang) {
        super(ang);
    }

    public Bay(Hangar kRVar) {
        super((C5540aNg) null);
        super._m_script_init(kRVar);
    }

    /* renamed from: V */
    static void m15842V() {
        serialVersionUID = (long) 1;
        _m_fieldCount = ItemLocation._m_fieldCount + 1;
        _m_methodCount = ItemLocation._m_methodCount + 10;
        int i = ItemLocation._m_fieldCount;
        C5663aRz[] arzArr = new C5663aRz[(i + 1)];
        C5663aRz b = C5640aRc.m17844b(Bay.class, "02a74869b051fc2d921f55e243cfd23d", i);
        ifL = b;
        arzArr[i] = b;
        int i2 = i + 1;
        _m_fields = arzArr;
        C1634Xv.m11725a((Object[]) ItemLocation._m_fields, (Object[]) _m_fields);
        int i3 = ItemLocation._m_methodCount;
        C2491fm[] fmVarArr = new C2491fm[(i3 + 10)];
        C2491fm a = C4105zY.m41624a(Bay.class, "beaea82aa3d05c901c0c2777a6ba5ba1", i3);
        _f_dispose_0020_0028_0029V = a;
        fmVarArr[i3] = a;
        int i4 = i3 + 1;
        C2491fm a2 = C4105zY.m41624a(Bay.class, "5bac014233aa3b3d387e82407f4c46aa", i4);
        ifM = a2;
        fmVarArr[i4] = a2;
        int i5 = i4 + 1;
        C2491fm a3 = C4105zY.m41624a(Bay.class, "4f1554988b03e61e24c6425199b48539", i5);
        ifN = a3;
        fmVarArr[i5] = a3;
        int i6 = i5 + 1;
        C2491fm a4 = C4105zY.m41624a(Bay.class, "de3274bfbec56436f805bcc46c5fe30f", i6);
        f3229x851d656b = a4;
        fmVarArr[i6] = a4;
        int i7 = i6 + 1;
        C2491fm a5 = C4105zY.m41624a(Bay.class, "63b421e7569b91c4fa4031755553b4a5", i7);
        ifO = a5;
        fmVarArr[i7] = a5;
        int i8 = i7 + 1;
        C2491fm a6 = C4105zY.m41624a(Bay.class, "850a146f8cbfecb8a5c47736d6df6d9c", i8);
        _f_toString_0020_0028_0029Ljava_002flang_002fString_003b = a6;
        fmVarArr[i8] = a6;
        int i9 = i8 + 1;
        C2491fm a7 = C4105zY.m41624a(Bay.class, "f63985c694a004a3ca36b5cab5b7bf9c", i9);
        f3226Lm = a7;
        fmVarArr[i9] = a7;
        int i10 = i9 + 1;
        C2491fm a8 = C4105zY.m41624a(Bay.class, "bb235a7f759a8eae2c98df8c58c72dfb", i10);
        f3228RD = a8;
        fmVarArr[i10] = a8;
        int i11 = i10 + 1;
        C2491fm a9 = C4105zY.m41624a(Bay.class, "fb8108e29e18ac30a5535d8dc1942aa3", i11);
        f3227RA = a9;
        fmVarArr[i11] = a9;
        int i12 = i11 + 1;
        C2491fm a10 = C4105zY.m41624a(Bay.class, "950b42b389af6d5abeb89fcc68bfe08c", i12);
        f3230ku = a10;
        fmVarArr[i12] = a10;
        int i13 = i12 + 1;
        _m_methods = fmVarArr;
        C1634Xv.m11725a((Object[]) ItemLocation._m_methods, (Object[]) _m_methods);
        ___iScriptClass = aUO.m18566a(Bay.class, C0483Gg.class, _m_fields, _m_methods);
    }

    @C0064Am(aul = "63b421e7569b91c4fa4031755553b4a5", aum = 0)
    @C5566aOg
    @C2499fr
    /* renamed from: ar */
    private boolean m15844ar(Ship fAVar) {
        throw new aWi(new aCE(this, ifO, new Object[]{fAVar}));
    }

    private Hangar dfD() {
        return (Hangar) bFf().mo5608dq().mo3214p(ifL);
    }

    /* renamed from: e */
    private void m15846e(Hangar kRVar) {
        bFf().mo5608dq().mo3197f(ifL, kRVar);
    }

    /* renamed from: S */
    public void mo10S() {
        super.mo10S();
    }

    /* renamed from: T */
    public C6494any mo11T() {
        return ___iScriptClass;
    }

    /* renamed from: U */
    public C2491fm[] mo12U() {
        return _m_methods;
    }

    /* renamed from: W */
    public Object mo13W() {
        return new C0483Gg(this);
    }

    /* renamed from: a */
    public Object mo14a(C0495Gr gr) {
        Object[] args = gr.getArgs();
        switch (gr.mo2417hq() - ItemLocation._m_methodCount) {
            case 0:
                m15847fg();
                return null;
            case 1:
                return dfE();
            case 2:
                m15849g((Hangar) args[0]);
                return null;
            case 3:
                return m15841Mo();
            case 4:
                return new Boolean(m15844ar((Ship) args[0]));
            case 5:
                return m15845au();
            case 6:
                m15851qU();
                return null;
            case 7:
                m15850m((Item) args[0]);
                return null;
            case 8:
                m15843a((Item) args[0], (ItemLocation) args[1]);
                return null;
            case 9:
                m15848g((Item) args[0]);
                return null;
            default:
                return super.mo14a(gr);
        }
    }

    /* renamed from: a */
    public void mo15a(Collection collection, C0495Gr gr) {
        super.mo15a(collection, gr);
    }

    /* renamed from: al */
    public Ship mo9625al() {
        switch (bFf().mo6893i(f3229x851d656b)) {
            case 0:
                return null;
            case 2:
                return (Ship) bFf().mo5606d(new aCE(this, f3229x851d656b, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f3229x851d656b, new Object[0]));
                break;
        }
        return m15841Mo();
    }

    @C5566aOg
    @C2499fr
    /* renamed from: as */
    public boolean mo9626as(Ship fAVar) {
        switch (bFf().mo6893i(ifO)) {
            case 0:
                return false;
            case 2:
                return ((Boolean) bFf().mo5606d(new aCE(this, ifO, new Object[]{fAVar}))).booleanValue();
            case 3:
                bFf().mo5606d(new aCE(this, ifO, new Object[]{fAVar}));
                break;
        }
        return m15844ar(fAVar);
    }

    /* access modifiers changed from: protected */
    /* renamed from: b */
    public void mo1887b(Item auq, ItemLocation aag) {
        switch (bFf().mo6893i(f3227RA)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f3227RA, new Object[]{auq, aag}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f3227RA, new Object[]{auq, aag}));
                break;
        }
        m15843a(auq, aag);
    }

    /* renamed from: c */
    public C5663aRz[] mo25c() {
        return _m_fields;
    }

    public Hangar dfF() {
        switch (bFf().mo6893i(ifM)) {
            case 0:
                return null;
            case 2:
                return (Hangar) bFf().mo5606d(new aCE(this, ifM, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, ifM, new Object[0]));
                break;
        }
        return dfE();
    }

    public void dispose() {
        switch (bFf().mo6893i(_f_dispose_0020_0028_0029V)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, _f_dispose_0020_0028_0029V, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, _f_dispose_0020_0028_0029V, new Object[0]));
                break;
        }
        m15847fg();
    }

    /* access modifiers changed from: protected */
    /* renamed from: h */
    public void mo1889h(Item auq) {
        switch (bFf().mo6893i(f3230ku)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f3230ku, new Object[]{auq}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f3230ku, new Object[]{auq}));
                break;
        }
        m15848g(auq);
    }

    /* renamed from: h */
    public void mo9629h(Hangar kRVar) {
        switch (bFf().mo6893i(ifN)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, ifN, new Object[]{kRVar}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, ifN, new Object[]{kRVar}));
                break;
        }
        m15849g(kRVar);
    }

    /* access modifiers changed from: protected */
    /* renamed from: n */
    public void mo1890n(Item auq) {
        switch (bFf().mo6893i(f3228RD)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f3228RD, new Object[]{auq}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f3228RD, new Object[]{auq}));
                break;
        }
        m15850m(auq);
    }

    /* renamed from: qV */
    public void mo4433qV() {
        switch (bFf().mo6893i(f3226Lm)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f3226Lm, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f3226Lm, new Object[0]));
                break;
        }
        m15851qU();
    }

    public String toString() {
        switch (bFf().mo6893i(_f_toString_0020_0028_0029Ljava_002flang_002fString_003b)) {
            case 0:
                return null;
            case 2:
                return (String) bFf().mo5606d(new aCE(this, _f_toString_0020_0028_0029Ljava_002flang_002fString_003b, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, _f_toString_0020_0028_0029Ljava_002flang_002fString_003b, new Object[0]));
                break;
        }
        return m15845au();
    }

    /* renamed from: f */
    public void mo9628f(Hangar kRVar) {
        super.mo10S();
        m15846e((Hangar) null);
        m15846e(kRVar);
    }

    @C0064Am(aul = "beaea82aa3d05c901c0c2777a6ba5ba1", aum = 0)
    /* renamed from: fg */
    private void m15847fg() {
        super.dispose();
        m15846e((Hangar) null);
    }

    @C0064Am(aul = "5bac014233aa3b3d387e82407f4c46aa", aum = 0)
    private Hangar dfE() {
        return dfD();
    }

    @C0064Am(aul = "4f1554988b03e61e24c6425199b48539", aum = 0)
    /* renamed from: g */
    private void m15849g(Hangar kRVar) {
        m15846e(kRVar);
    }

    @C0064Am(aul = "de3274bfbec56436f805bcc46c5fe30f", aum = 0)
    /* renamed from: Mo */
    private Ship m15841Mo() {
        if (isEmpty()) {
            return null;
        }
        return ((ShipItem) cIB()).mo11127al();
    }

    @C0064Am(aul = "850a146f8cbfecb8a5c47736d6df6d9c", aum = 0)
    /* renamed from: au */
    private String m15845au() {
        if (mo9625al() == null) {
            return "empty bay";
        }
        return "bay with " + mo9625al().agH().getHandle();
    }

    @C0064Am(aul = "f63985c694a004a3ca36b5cab5b7bf9c", aum = 0)
    /* renamed from: qU */
    private void m15851qU() {
        super.mo4433qV();
        push();
        if (mo9625al() != null) {
            mo9625al().mo656qV();
        }
    }

    @C0064Am(aul = "bb235a7f759a8eae2c98df8c58c72dfb", aum = 0)
    /* renamed from: m */
    private void m15850m(Item auq) {
        dfD().mo20063b(this);
        dfD().mo20058KY().mo14419f((C1506WA) new C1749Zq(false, auq));
    }

    @C0064Am(aul = "fb8108e29e18ac30a5535d8dc1942aa3", aum = 0)
    /* renamed from: a */
    private void m15843a(Item auq, ItemLocation aag) {
        dfD().mo20058KY().mo14419f((C1506WA) new C1749Zq(true, auq));
    }

    @C0064Am(aul = "950b42b389af6d5abeb89fcc68bfe08c", aum = 0)
    /* renamed from: g */
    private void m15848g(Item auq) {
        if (!auq.mo16383bW(dfD().mo20058KY())) {
            throw new C2293dh(((NLSProgression) ala().aIY().mo6310c(NLSManager.C1472a.PROGRESSION)).cyg());
        }
        if (!((auq instanceof ShipItem) && mo9625al() == null)) {
            throw new C2293dh(((NLSItem) ala().aIY().mo6310c(NLSManager.C1472a.ITEM)).bto());
        }
    }
}
