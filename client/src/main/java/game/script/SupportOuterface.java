package game.script;

import com.hoplon.taikodom.common.TaikodomVersion;
import game.network.message.externalizable.aCE;
import game.script.support.TicketManager;
import logic.baa.C1616Xf;
import logic.baa.C1634Xv;
import logic.baa.C5511aMd;
import logic.baa.C6485anp;
import logic.data.mbean.C5231aBj;
import logic.res.code.C5640aRc;
import logic.res.code.C5663aRz;
import logic.res.code.C6494any;
import logic.res.html.C2491fm;
import p001a.*;

import java.util.Collection;

@C5511aMd
@C6485anp
/* renamed from: a.aLx  reason: case insensitive filesystem */
/* compiled from: a */
public class SupportOuterface extends TaikodomObject implements C1616Xf {
    public static final int _m_fieldCount = 0;
    public static final C5663aRz[] _m_fields = null;
    public static final int _m_methodCount = 0;
    public static final C2491fm[] _m_methods = null;
    public static final C5663aRz ilT = null;
    public static final C5663aRz ilU = null;
    public static final C2491fm ilV = null;
    public static final C2491fm ilW = null;
    public static final C2491fm ilX = null;
    public static final C2491fm ilY = null;
    public static final long serialVersionUID = 0;
    public static C6494any ___iScriptClass;
    @C0064Am(aul = "cdce878edb8e24896d76139c09c7d708", aum = 0)
    private static int hgV;
    @C0064Am(aul = "4f5f8799b81126e8ab153c77e18e5869", aum = 1)
    @C5566aOg
    private static TicketManager hgW;

    static {
        m16293V();
    }

    public SupportOuterface() {
        super((C5540aNg) null);
        super.mo10S();
    }

    public SupportOuterface(C5540aNg ang) {
        super(ang);
    }

    /* renamed from: V */
    static void m16293V() {
        serialVersionUID = (long) 1;
        _m_fieldCount = TaikodomObject._m_fieldCount + 2;
        _m_methodCount = TaikodomObject._m_methodCount + 4;
        int i = TaikodomObject._m_fieldCount;
        C5663aRz[] arzArr = new C5663aRz[(i + 2)];
        C5663aRz b = C5640aRc.m17844b(SupportOuterface.class, "cdce878edb8e24896d76139c09c7d708", i);
        ilT = b;
        arzArr[i] = b;
        int i2 = i + 1;
        C5663aRz b2 = C5640aRc.m17844b(SupportOuterface.class, "4f5f8799b81126e8ab153c77e18e5869", i2);
        ilU = b2;
        arzArr[i2] = b2;
        int i3 = i2 + 1;
        _m_fields = arzArr;
        C1634Xv.m11725a((Object[]) TaikodomObject._m_fields, (Object[]) _m_fields);
        int i4 = TaikodomObject._m_methodCount;
        C2491fm[] fmVarArr = new C2491fm[(i4 + 4)];
        C2491fm a = C4105zY.m41624a(SupportOuterface.class, "2e097ea424c01e662257d82d505be824", i4);
        ilV = a;
        fmVarArr[i4] = a;
        int i5 = i4 + 1;
        C2491fm a2 = C4105zY.m41624a(SupportOuterface.class, "be50e1d595e6ee5644adc2cbeb227f0f", i5);
        ilW = a2;
        fmVarArr[i5] = a2;
        int i6 = i5 + 1;
        C2491fm a3 = C4105zY.m41624a(SupportOuterface.class, "4b71528178fdcda4b18086c5efa7e7c9", i6);
        ilX = a3;
        fmVarArr[i6] = a3;
        int i7 = i6 + 1;
        C2491fm a4 = C4105zY.m41624a(SupportOuterface.class, "ab6b28d9a658090c1707d7361a136c49", i7);
        ilY = a4;
        fmVarArr[i7] = a4;
        int i8 = i7 + 1;
        _m_methods = fmVarArr;
        C1634Xv.m11725a((Object[]) TaikodomObject._m_methods, (Object[]) _m_methods);
        ___iScriptClass = aUO.m18566a(SupportOuterface.class, C5231aBj.class, _m_fields, _m_methods);
    }

    /* renamed from: a */
    private void m16294a(TicketManager ayl) {
        bFf().mo5608dq().mo3197f(ilU, ayl);
    }

    private int dhZ() {
        return bFf().mo5608dq().mo3212n(ilT);
    }

    private TicketManager dia() {
        return (TicketManager) bFf().mo5608dq().mo3214p(ilU);
    }

    @C0064Am(aul = "be50e1d595e6ee5644adc2cbeb227f0f", aum = 0)
    @C5566aOg
    @C2499fr
    private void did() {
        throw new aWi(new aCE(this, ilW, new Object[0]));
    }

    /* renamed from: yE */
    private void m16295yE(int i) {
        bFf().mo5608dq().mo3183b(ilT, i);
    }

    /* renamed from: T */
    public C6494any mo11T() {
        return ___iScriptClass;
    }

    /* renamed from: U */
    public C2491fm[] mo12U() {
        return _m_methods;
    }

    /* renamed from: W */
    public Object mo13W() {
        return new C5231aBj(this);
    }

    /* renamed from: a */
    public Object mo14a(C0495Gr gr) {
        gr.getArgs();
        switch (gr.mo2417hq() - TaikodomObject._m_methodCount) {
            case 0:
                return new Integer(dib());
            case 1:
                did();
                return null;
            case 2:
                return dif();
            case 3:
                return dih();
            default:
                return super.mo14a(gr);
        }
    }

    /* renamed from: a */
    public void mo15a(Collection collection, C0495Gr gr) {
        super.mo15a(collection, gr);
    }

    /* renamed from: c */
    public C5663aRz[] mo25c() {
        return _m_fields;
    }

    public int dic() {
        switch (bFf().mo6893i(ilV)) {
            case 0:
                return 0;
            case 2:
                return ((Integer) bFf().mo5606d(new aCE(this, ilV, new Object[0]))).intValue();
            case 3:
                bFf().mo5606d(new aCE(this, ilV, new Object[0]));
                break;
        }
        return dib();
    }

    @C5566aOg
    @C2499fr
    public void die() {
        switch (bFf().mo6893i(ilW)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, ilW, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, ilW, new Object[0]));
                break;
        }
        did();
    }

    public TicketManager dig() {
        switch (bFf().mo6893i(ilX)) {
            case 0:
                return null;
            case 2:
                return (TicketManager) bFf().mo5606d(new aCE(this, ilX, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, ilX, new Object[0]));
                break;
        }
        return dif();
    }

    public String dii() {
        switch (bFf().mo6893i(ilY)) {
            case 0:
                return null;
            case 2:
                return (String) bFf().mo5606d(new aCE(this, ilY, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, ilY, new Object[0]));
                break;
        }
        return dih();
    }

    /* renamed from: S */
    public void mo10S() {
        super.mo10S();
        m16295yE(0);
    }

    @C0064Am(aul = "2e097ea424c01e662257d82d505be824", aum = 0)
    private int dib() {
        int dhZ = dhZ();
        m16295yE(dhZ + 1);
        return dhZ;
    }

    @C0064Am(aul = "4b71528178fdcda4b18086c5efa7e7c9", aum = 0)
    private TicketManager dif() {
        if (dia() == null) {
            die();
        }
        return dia();
    }

    @C0064Am(aul = "ab6b28d9a658090c1707d7361a136c49", aum = 0)
    private String dih() {
        return TaikodomVersion.VERSION;
    }
}
