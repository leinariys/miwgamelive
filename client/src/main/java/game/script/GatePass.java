package game.script;

import game.network.message.externalizable.aCE;
import game.script.space.Gate;
import logic.baa.*;
import logic.data.mbean.C5692aTc;
import logic.res.code.C5640aRc;
import logic.res.code.C5663aRz;
import logic.res.code.C6494any;
import logic.res.html.C2491fm;
import p001a.*;

import java.util.Collection;

@C5511aMd
@C6485anp
/* renamed from: a.aDB */
/* compiled from: a */
public class GatePass extends aDJ implements C1616Xf {
    public static final int _m_fieldCount = 0;
    public static final C5663aRz[] _m_fields = null;
    public static final int _m_methodCount = 0;
    public static final C2491fm[] _m_methods = null;
    public static final C5663aRz aDA = null;
    public static final C2491fm aDB = null;
    public static final C2491fm aDC = null;
    public static final C5663aRz hCt = null;
    public static final C2491fm hCu = null;
    public static final C2491fm hCv = null;
    public static final long serialVersionUID = 0;
    public static C6494any ___iScriptClass;
    @C0064Am(aul = "de30589c38e412e8ebc0c3ea2f6e4b43", aum = 1)

    /* renamed from: BN */
    private static Gate f2512BN;
    @C0064Am(aul = "1f3ba3c8a84902652f63e903d91e0027", aum = 0)
    private static boolean hCs;

    static {
        m13413V();
    }

    public GatePass() {
        super((C5540aNg) null);
        super.mo10S();
    }

    public GatePass(C5540aNg ang) {
        super(ang);
    }

    public GatePass(Gate fFVar) {
        super((C5540aNg) null);
        super._m_script_init(fFVar);
    }

    public GatePass(Gate fFVar, boolean z) {
        super((C5540aNg) null);
        super._m_script_init(fFVar, z);
    }

    /* renamed from: V */
    static void m13413V() {
        serialVersionUID = (long) 1;
        _m_fieldCount = aDJ._m_fieldCount + 2;
        _m_methodCount = aDJ._m_methodCount + 4;
        int i = aDJ._m_fieldCount;
        C5663aRz[] arzArr = new C5663aRz[(i + 2)];
        C5663aRz b = C5640aRc.m17844b(GatePass.class, "1f3ba3c8a84902652f63e903d91e0027", i);
        hCt = b;
        arzArr[i] = b;
        int i2 = i + 1;
        C5663aRz b2 = C5640aRc.m17844b(GatePass.class, "de30589c38e412e8ebc0c3ea2f6e4b43", i2);
        aDA = b2;
        arzArr[i2] = b2;
        int i3 = i2 + 1;
        _m_fields = arzArr;
        C1634Xv.m11725a((Object[]) aDJ._m_fields, (Object[]) _m_fields);
        int i4 = aDJ._m_methodCount;
        C2491fm[] fmVarArr = new C2491fm[(i4 + 4)];
        C2491fm a = C4105zY.m41624a(GatePass.class, "9dcfdeee42fa4e86930d8928a34decd2", i4);
        hCu = a;
        fmVarArr[i4] = a;
        int i5 = i4 + 1;
        C2491fm a2 = C4105zY.m41624a(GatePass.class, "32694836298a86ea077c7bb6be58861d", i5);
        hCv = a2;
        fmVarArr[i5] = a2;
        int i6 = i5 + 1;
        C2491fm a3 = C4105zY.m41624a(GatePass.class, "a541ce41f67727513e0ca7eb921a5b49", i6);
        aDC = a3;
        fmVarArr[i6] = a3;
        int i7 = i6 + 1;
        C2491fm a4 = C4105zY.m41624a(GatePass.class, "c1544a0808cf11a53951f77c7009c7fa", i7);
        aDB = a4;
        fmVarArr[i7] = a4;
        int i8 = i7 + 1;
        _m_methods = fmVarArr;
        C1634Xv.m11725a((Object[]) aDJ._m_methods, (Object[]) _m_methods);
        ___iScriptClass = aUO.m18566a(GatePass.class, C5692aTc.class, _m_fields, _m_methods);
    }

    /* renamed from: NA */
    private Gate m13411NA() {
        return (Gate) bFf().mo5608dq().mo3214p(aDA);
    }

    private boolean cVt() {
        return bFf().mo5608dq().mo3201h(hCt);
    }

    /* renamed from: d */
    private void m13414d(Gate fFVar) {
        bFf().mo5608dq().mo3197f(aDA, fFVar);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Gate")
    @C0064Am(aul = "c1544a0808cf11a53951f77c7009c7fa", aum = 0)
    @C5566aOg
    /* renamed from: e */
    private void m13415e(Gate fFVar) {
        throw new aWi(new aCE(this, aDB, new Object[]{fFVar}));
    }

    /* renamed from: iZ */
    private void m13416iZ(boolean z) {
        bFf().mo5608dq().mo3153a(hCt, z);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Free Pass")
    @C0064Am(aul = "32694836298a86ea077c7bb6be58861d", aum = 0)
    @C5566aOg
    /* renamed from: ja */
    private void m13417ja(boolean z) {
        throw new aWi(new aCE(this, hCv, new Object[]{new Boolean(z)}));
    }

    /* renamed from: T */
    public C6494any mo11T() {
        return ___iScriptClass;
    }

    /* renamed from: U */
    public C2491fm[] mo12U() {
        return _m_methods;
    }

    /* renamed from: W */
    public Object mo13W() {
        return new C5692aTc(this);
    }

    /* renamed from: a */
    public Object mo14a(C0495Gr gr) {
        Object[] args = gr.getArgs();
        switch (gr.mo2417hq() - aDJ._m_methodCount) {
            case 0:
                return new Boolean(cVu());
            case 1:
                m13417ja(((Boolean) args[0]).booleanValue());
                return null;
            case 2:
                return m13412NB();
            case 3:
                m13415e((Gate) args[0]);
                return null;
            default:
                return super.mo14a(gr);
        }
    }

    /* renamed from: a */
    public void mo15a(Collection collection, C0495Gr gr) {
        super.mo15a(collection, gr);
    }

    /* renamed from: c */
    public C5663aRz[] mo25c() {
        return _m_fields;
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Free Pass")
    public boolean cVv() {
        switch (bFf().mo6893i(hCu)) {
            case 0:
                return false;
            case 2:
                return ((Boolean) bFf().mo5606d(new aCE(this, hCu, new Object[0]))).booleanValue();
            case 3:
                bFf().mo5606d(new aCE(this, hCu, new Object[0]));
                break;
        }
        return cVu();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Gate")
    @C5566aOg
    /* renamed from: f */
    public void mo8291f(Gate fFVar) {
        switch (bFf().mo6893i(aDB)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, aDB, new Object[]{fFVar}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, aDB, new Object[]{fFVar}));
                break;
        }
        m13415e(fFVar);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Free Pass")
    @C5566aOg
    /* renamed from: jb */
    public void mo8292jb(boolean z) {
        switch (bFf().mo6893i(hCv)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, hCv, new Object[]{new Boolean(z)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, hCv, new Object[]{new Boolean(z)}));
                break;
        }
        m13417ja(z);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Gate")
    /* renamed from: lb */
    public Gate mo8293lb() {
        switch (bFf().mo6893i(aDC)) {
            case 0:
                return null;
            case 2:
                return (Gate) bFf().mo5606d(new aCE(this, aDC, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, aDC, new Object[0]));
                break;
        }
        return m13412NB();
    }

    /* renamed from: S */
    public void mo10S() {
        super.mo10S();
        m13414d((Gate) null);
        m13416iZ(false);
    }

    /* renamed from: p */
    public void mo8294p(Gate fFVar) {
        mo8289a(fFVar, false);
    }

    /* renamed from: a */
    public void mo8289a(Gate fFVar, boolean z) {
        super.mo10S();
        m13414d(fFVar);
        m13416iZ(z);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Free Pass")
    @C0064Am(aul = "9dcfdeee42fa4e86930d8928a34decd2", aum = 0)
    private boolean cVu() {
        return cVt();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Gate")
    @C0064Am(aul = "a541ce41f67727513e0ca7eb921a5b49", aum = 0)
    /* renamed from: NB */
    private Gate m13412NB() {
        return m13411NA();
    }
}
