package game.script.mail;

import game.network.message.externalizable.aCE;
import game.network.message.serializable.C3438ra;
import game.script.TaikodomObject;
import logic.baa.C1616Xf;
import logic.baa.C1634Xv;
import logic.baa.C5511aMd;
import logic.baa.C6485anp;
import logic.data.mbean.C6350alK;
import logic.res.code.C5640aRc;
import logic.res.code.C5663aRz;
import logic.res.code.C6494any;
import logic.res.html.C2491fm;
import p001a.*;

import java.util.Collection;
import java.util.List;

@C5511aMd
@C6485anp
/* renamed from: a.aFG */
/* compiled from: a */
public class MailBox extends TaikodomObject implements C1616Xf {

    /* renamed from: Km */
    public static final C2491fm f2789Km = null;

    /* renamed from: Pf */
    public static final C2491fm f2790Pf = null;
    public static final C2491fm _f_toString_0020_0028_0029Ljava_002flang_002fString_003b = null;
    public static final int _m_fieldCount = 0;
    public static final C5663aRz[] _m_fields = null;
    public static final int _m_methodCount = 0;
    public static final C2491fm[] _m_methods = null;
    public static final C5663aRz hJM = null;
    public static final C2491fm hJN = null;
    public static final C2491fm hJO = null;
    public static final C2491fm hJP = null;
    public static final long serialVersionUID = 0;
    /* renamed from: zQ */
    public static final C5663aRz f2791zQ = null;
    public static C6494any ___iScriptClass;
    @C0064Am(aul = "4147251704b043c048c5952ed0391a63", aum = 1)
    private static C3438ra<MailMessage> fXH;
    @C0064Am(aul = "9ea7fa4f58ed44d4118de35bfa16a5fb", aum = 0)
    private static String name;

    static {
        m14522V();
    }

    public MailBox() {
        super((C5540aNg) null);
        super.mo10S();
    }

    public MailBox(C5540aNg ang) {
        super(ang);
    }

    public MailBox(String str) {
        super((C5540aNg) null);
        super._m_script_init(str);
    }

    /* renamed from: V */
    static void m14522V() {
        serialVersionUID = (long) 1;
        _m_fieldCount = TaikodomObject._m_fieldCount + 2;
        _m_methodCount = TaikodomObject._m_methodCount + 6;
        int i = TaikodomObject._m_fieldCount;
        C5663aRz[] arzArr = new C5663aRz[(i + 2)];
        C5663aRz b = C5640aRc.m17844b(MailBox.class, "9ea7fa4f58ed44d4118de35bfa16a5fb", i);
        f2791zQ = b;
        arzArr[i] = b;
        int i2 = i + 1;
        C5663aRz b2 = C5640aRc.m17844b(MailBox.class, "4147251704b043c048c5952ed0391a63", i2);
        hJM = b2;
        arzArr[i2] = b2;
        int i3 = i2 + 1;
        _m_fields = arzArr;
        C1634Xv.m11725a((Object[]) TaikodomObject._m_fields, (Object[]) _m_fields);
        int i4 = TaikodomObject._m_methodCount;
        C2491fm[] fmVarArr = new C2491fm[(i4 + 6)];
        C2491fm a = C4105zY.m41624a(MailBox.class, "9885935c0d1e977c47bd277d70c88386", i4);
        hJN = a;
        fmVarArr[i4] = a;
        int i5 = i4 + 1;
        C2491fm a2 = C4105zY.m41624a(MailBox.class, "bb7b75e47bb3f8f3ade62b365a646907", i5);
        f2790Pf = a2;
        fmVarArr[i5] = a2;
        int i6 = i5 + 1;
        C2491fm a3 = C4105zY.m41624a(MailBox.class, "f5f43bc72437a2d82d7ca321462158d4", i6);
        hJO = a3;
        fmVarArr[i6] = a3;
        int i7 = i6 + 1;
        C2491fm a4 = C4105zY.m41624a(MailBox.class, "57599a2911aabc658e051beb68599ec6", i7);
        hJP = a4;
        fmVarArr[i7] = a4;
        int i8 = i7 + 1;
        C2491fm a5 = C4105zY.m41624a(MailBox.class, "bae9a0dc7bed3d3fa778878aac213352", i8);
        f2789Km = a5;
        fmVarArr[i8] = a5;
        int i9 = i8 + 1;
        C2491fm a6 = C4105zY.m41624a(MailBox.class, "b88337f4bb2eda7c3e40ab2ed9e8b206", i9);
        _f_toString_0020_0028_0029Ljava_002flang_002fString_003b = a6;
        fmVarArr[i9] = a6;
        int i10 = i9 + 1;
        _m_methods = fmVarArr;
        C1634Xv.m11725a((Object[]) TaikodomObject._m_methods, (Object[]) _m_methods);
        ___iScriptClass = aUO.m18566a(MailBox.class, C6350alK.class, _m_fields, _m_methods);
    }

    /* renamed from: ap */
    private void m14523ap(String str) {
        bFf().mo5608dq().mo3197f(f2791zQ, str);
    }

    private C3438ra cZf() {
        return (C3438ra) bFf().mo5608dq().mo3214p(hJM);
    }

    /* renamed from: cu */
    private void m14525cu(C3438ra raVar) {
        bFf().mo5608dq().mo3197f(hJM, raVar);
    }

    @C0064Am(aul = "57599a2911aabc658e051beb68599ec6", aum = 0)
    @C5566aOg
    @C2499fr
    /* renamed from: g */
    private void m14527g(MailMessage asf) {
        throw new aWi(new aCE(this, hJP, new Object[]{asf}));
    }

    /* renamed from: uU */
    private String m14528uU() {
        return (String) bFf().mo5608dq().mo3214p(f2791zQ);
    }

    /* renamed from: S */
    public void mo10S() {
        super.mo10S();
    }

    /* renamed from: T */
    public C6494any mo11T() {
        return ___iScriptClass;
    }

    /* renamed from: U */
    public C2491fm[] mo12U() {
        return _m_methods;
    }

    /* renamed from: W */
    public Object mo13W() {
        return new C6350alK(this);
    }

    /* renamed from: a */
    public Object mo14a(C0495Gr gr) {
        Object[] args = gr.getArgs();
        switch (gr.mo2417hq() - TaikodomObject._m_methodCount) {
            case 0:
                return cZg();
            case 1:
                return m14529uV();
            case 2:
                m14526e((MailMessage) args[0]);
                return null;
            case 3:
                m14527g((MailMessage) args[0]);
                return null;
            case 4:
                return new Boolean(m14530v(args[0]));
            case 5:
                return m14524au();
            default:
                return super.mo14a(gr);
        }
    }

    /* renamed from: a */
    public void mo15a(Collection collection, C0495Gr gr) {
        super.mo15a(collection, gr);
    }

    /* renamed from: c */
    public C5663aRz[] mo25c() {
        return _m_fields;
    }

    public List<MailMessage> cZh() {
        switch (bFf().mo6893i(hJN)) {
            case 0:
                return null;
            case 2:
                return (List) bFf().mo5606d(new aCE(this, hJN, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, hJN, new Object[0]));
                break;
        }
        return cZg();
    }

    public boolean equals(Object obj) {
        switch (bFf().mo6893i(f2789Km)) {
            case 0:
                return false;
            case 2:
                return ((Boolean) bFf().mo5606d(new aCE(this, f2789Km, new Object[]{obj}))).booleanValue();
            case 3:
                bFf().mo5606d(new aCE(this, f2789Km, new Object[]{obj}));
                break;
        }
        return m14530v(obj);
    }

    /* renamed from: f */
    public void mo8771f(MailMessage asf) {
        switch (bFf().mo6893i(hJO)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, hJO, new Object[]{asf}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, hJO, new Object[]{asf}));
                break;
        }
        m14526e(asf);
    }

    public String getName() {
        switch (bFf().mo6893i(f2790Pf)) {
            case 0:
                return null;
            case 2:
                return (String) bFf().mo5606d(new aCE(this, f2790Pf, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f2790Pf, new Object[0]));
                break;
        }
        return m14529uV();
    }

    @C5566aOg
    @C2499fr
    /* renamed from: h */
    public void mo8773h(MailMessage asf) {
        switch (bFf().mo6893i(hJP)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, hJP, new Object[]{asf}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, hJP, new Object[]{asf}));
                break;
        }
        m14527g(asf);
    }

    public String toString() {
        switch (bFf().mo6893i(_f_toString_0020_0028_0029Ljava_002flang_002fString_003b)) {
            case 0:
                return null;
            case 2:
                return (String) bFf().mo5606d(new aCE(this, _f_toString_0020_0028_0029Ljava_002flang_002fString_003b, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, _f_toString_0020_0028_0029Ljava_002flang_002fString_003b, new Object[0]));
                break;
        }
        return m14524au();
    }

    /* renamed from: lo */
    public void mo8774lo(String str) {
        super.mo10S();
        m14523ap(str);
    }

    @C0064Am(aul = "9885935c0d1e977c47bd277d70c88386", aum = 0)
    private List<MailMessage> cZg() {
        return cZf();
    }

    @C0064Am(aul = "bb7b75e47bb3f8f3ade62b365a646907", aum = 0)
    /* renamed from: uV */
    private String m14529uV() {
        return m14528uU();
    }

    @C0064Am(aul = "f5f43bc72437a2d82d7ca321462158d4", aum = 0)
    /* renamed from: e */
    private void m14526e(MailMessage asf) {
        cZf().add(asf);
    }

    @C0064Am(aul = "bae9a0dc7bed3d3fa778878aac213352", aum = 0)
    /* renamed from: v */
    private boolean m14530v(Object obj) {
        String str;
        if (obj == null) {
            return false;
        }
        if (obj instanceof MailBox) {
            str = ((MailBox) obj).getName();
        } else {
            str = obj instanceof String ? (String) obj : null;
        }
        if (str != null) {
            return m14528uU().equals(str);
        }
        return false;
    }

    @C0064Am(aul = "b88337f4bb2eda7c3e40ab2ed9e8b206", aum = 0)
    /* renamed from: au */
    private String m14524au() {
        return m14528uU();
    }
}
