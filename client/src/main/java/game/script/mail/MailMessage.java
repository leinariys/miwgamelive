package game.script.mail;

import game.network.message.externalizable.aCE;
import game.network.message.serializable.C3438ra;
import game.script.TaikodomObject;
import game.script.player.Player;
import logic.baa.C1616Xf;
import logic.baa.C1634Xv;
import logic.baa.C5511aMd;
import logic.baa.C6485anp;
import logic.data.mbean.C4123zn;
import logic.res.code.C5640aRc;
import logic.res.code.C5663aRz;
import logic.res.code.C6494any;
import logic.res.html.C2491fm;
import p001a.*;

import java.text.SimpleDateFormat;
import java.util.Collection;
import java.util.Date;
import java.util.List;

@C5511aMd
@C6485anp
/* renamed from: a.asf  reason: case insensitive filesystem */
/* compiled from: a */
public class MailMessage extends TaikodomObject implements C1616Xf {

    /* renamed from: NA */
    public static final C2491fm f5285NA = null;

    /* renamed from: Nj */
    public static final C5663aRz f5286Nj = null;

    /* renamed from: Nz */
    public static final C2491fm f5287Nz = null;
    public static final C2491fm _f_toString_0020_0028_0029Ljava_002flang_002fString_003b = null;
    public static final int _m_fieldCount = 0;
    public static final C5663aRz[] _m_fields = null;
    public static final int _m_methodCount = 0;
    public static final C2491fm[] _m_methods = null;
    public static final C5663aRz dnb = null;
    public static final C5663aRz gub = null;
    public static final C5663aRz guc = null;
    public static final C5663aRz gud = null;
    public static final C5663aRz gue = null;
    public static final C2491fm guf = null;
    public static final C2491fm gug = null;
    public static final C2491fm guh = null;
    public static final C2491fm gui = null;
    public static final C2491fm guj = null;
    public static final C2491fm guk = null;
    public static final C2491fm gul = null;
    public static final C2491fm gum = null;
    public static final C2491fm gun = null;
    public static final C2491fm guo = null;
    public static final long serialVersionUID = 0;
    private static final SimpleDateFormat dBH = new SimpleDateFormat("yyyy-MM-dd HH:mm");
    public static C6494any ___iScriptClass;
    @C0064Am(aul = "b085c43051e439c3d8978a652e3a0587", aum = 3)
    private static Player auN;
    @C0064Am(aul = "6131dd2b8bb8997e340dcc7c08156e38", aum = 1)
    private static boolean bZi;
    @C0064Am(aul = "5cefd57857f4ca598667972969f0bb5d", aum = 2)
    private static C3438ra<Player> bZj;
    @C0064Am(aul = "060f5ab2a7671c95a709646c1801e6b6", aum = 5)
    private static String bZk;
    @C0064Am(aul = "bb2088c01b39a05d1e8ef230223f0f0a", aum = 0)
    private static Date date;
    @C0064Am(aul = "25a9e5bfd883735b74c563cd64a920e8", aum = 4)
    private static String subject;

    static {
        m25722V();
    }

    public MailMessage() {
        super((C5540aNg) null);
        super.mo10S();
    }

    public MailMessage(C5540aNg ang) {
        super(ang);
    }

    public MailMessage(Player aku, String str, String str2) {
        super((C5540aNg) null);
        super._m_script_init(aku, str, str2);
    }

    /* renamed from: V */
    static void m25722V() {
        serialVersionUID = (long) 1;
        _m_fieldCount = TaikodomObject._m_fieldCount + 6;
        _m_methodCount = TaikodomObject._m_methodCount + 13;
        int i = TaikodomObject._m_fieldCount;
        C5663aRz[] arzArr = new C5663aRz[(i + 6)];
        C5663aRz b = C5640aRc.m17844b(MailMessage.class, "bb2088c01b39a05d1e8ef230223f0f0a", i);
        gub = b;
        arzArr[i] = b;
        int i2 = i + 1;
        C5663aRz b2 = C5640aRc.m17844b(MailMessage.class, "6131dd2b8bb8997e340dcc7c08156e38", i2);
        guc = b2;
        arzArr[i2] = b2;
        int i3 = i2 + 1;
        C5663aRz b3 = C5640aRc.m17844b(MailMessage.class, "5cefd57857f4ca598667972969f0bb5d", i3);
        gud = b3;
        arzArr[i3] = b3;
        int i4 = i3 + 1;
        C5663aRz b4 = C5640aRc.m17844b(MailMessage.class, "b085c43051e439c3d8978a652e3a0587", i4);
        dnb = b4;
        arzArr[i4] = b4;
        int i5 = i4 + 1;
        C5663aRz b5 = C5640aRc.m17844b(MailMessage.class, "25a9e5bfd883735b74c563cd64a920e8", i5);
        f5286Nj = b5;
        arzArr[i5] = b5;
        int i6 = i5 + 1;
        C5663aRz b6 = C5640aRc.m17844b(MailMessage.class, "060f5ab2a7671c95a709646c1801e6b6", i6);
        gue = b6;
        arzArr[i6] = b6;
        int i7 = i6 + 1;
        _m_fields = arzArr;
        C1634Xv.m11725a((Object[]) TaikodomObject._m_fields, (Object[]) _m_fields);
        int i8 = TaikodomObject._m_methodCount;
        C2491fm[] fmVarArr = new C2491fm[(i8 + 13)];
        C2491fm a = C4105zY.m41624a(MailMessage.class, "69833980928b89193dfca2d1c6a4e0ea", i8);
        guf = a;
        fmVarArr[i8] = a;
        int i9 = i8 + 1;
        C2491fm a2 = C4105zY.m41624a(MailMessage.class, "7acfdab88702ab774b3bc1c5872158a8", i9);
        gug = a2;
        fmVarArr[i9] = a2;
        int i10 = i9 + 1;
        C2491fm a3 = C4105zY.m41624a(MailMessage.class, "8c30309035b39a4d559ca2472adbd913", i10);
        f5287Nz = a3;
        fmVarArr[i10] = a3;
        int i11 = i10 + 1;
        C2491fm a4 = C4105zY.m41624a(MailMessage.class, "cf19d7ddf90be805f3a543fd6279884f", i11);
        f5285NA = a4;
        fmVarArr[i11] = a4;
        int i12 = i11 + 1;
        C2491fm a5 = C4105zY.m41624a(MailMessage.class, "338bee9471578b1d242c2923c954e605", i12);
        guh = a5;
        fmVarArr[i12] = a5;
        int i13 = i12 + 1;
        C2491fm a6 = C4105zY.m41624a(MailMessage.class, "fa4bca0ca6dca33e1cbe0474a5824b4e", i13);
        gui = a6;
        fmVarArr[i13] = a6;
        int i14 = i13 + 1;
        C2491fm a7 = C4105zY.m41624a(MailMessage.class, "27557764dd3fb642aa43df725d66d5e3", i14);
        guj = a7;
        fmVarArr[i14] = a7;
        int i15 = i14 + 1;
        C2491fm a8 = C4105zY.m41624a(MailMessage.class, "e94b88d6c4270a94adbc8fb2cb393eb8", i15);
        guk = a8;
        fmVarArr[i15] = a8;
        int i16 = i15 + 1;
        C2491fm a9 = C4105zY.m41624a(MailMessage.class, "b21b60a3573a1bc1bcd6890af2f401c2", i16);
        gul = a9;
        fmVarArr[i16] = a9;
        int i17 = i16 + 1;
        C2491fm a10 = C4105zY.m41624a(MailMessage.class, "b8df1535f842a4d1dbbf1c02d391b2ca", i17);
        gum = a10;
        fmVarArr[i17] = a10;
        int i18 = i17 + 1;
        C2491fm a11 = C4105zY.m41624a(MailMessage.class, "b2f91828433af1505a1581c263f41456", i18);
        gun = a11;
        fmVarArr[i18] = a11;
        int i19 = i18 + 1;
        C2491fm a12 = C4105zY.m41624a(MailMessage.class, "b3e0d44bd3a07fc82d67189806350aab", i19);
        guo = a12;
        fmVarArr[i19] = a12;
        int i20 = i19 + 1;
        C2491fm a13 = C4105zY.m41624a(MailMessage.class, "65d50eb0e6d5fbe0beaf500602454e2b", i20);
        _f_toString_0020_0028_0029Ljava_002flang_002fString_003b = a13;
        fmVarArr[i20] = a13;
        int i21 = i20 + 1;
        _m_methods = fmVarArr;
        C1634Xv.m11725a((Object[]) TaikodomObject._m_methods, (Object[]) _m_methods);
        ___iScriptClass = aUO.m18566a(MailMessage.class, C4123zn.class, _m_fields, _m_methods);
    }

    /* renamed from: ah */
    private void m25723ah(String str) {
        bFf().mo5608dq().mo3197f(f5286Nj, str);
    }

    /* renamed from: bO */
    private void m25726bO(Player aku) {
        bFf().mo5608dq().mo3197f(dnb, aku);
    }

    /* renamed from: c */
    private void m25728c(Date date2) {
        bFf().mo5608dq().mo3197f(gub, date2);
    }

    /* renamed from: ci */
    private void m25729ci(C3438ra raVar) {
        bFf().mo5608dq().mo3197f(gud, raVar);
    }

    private Date ctF() {
        return (Date) bFf().mo5608dq().mo3214p(gub);
    }

    private boolean ctG() {
        return bFf().mo5608dq().mo3201h(guc);
    }

    private C3438ra ctH() {
        return (C3438ra) bFf().mo5608dq().mo3214p(gud);
    }

    private Player ctI() {
        return (Player) bFf().mo5608dq().mo3214p(dnb);
    }

    private String ctJ() {
        return (String) bFf().mo5608dq().mo3214p(gue);
    }

    /* renamed from: gt */
    private void m25730gt(boolean z) {
        bFf().mo5608dq().mo3153a(guc, z);
    }

    @C0064Am(aul = "b3e0d44bd3a07fc82d67189806350aab", aum = 0)
    @C5566aOg
    @C2499fr
    /* renamed from: gu */
    private void m25731gu(boolean z) {
        throw new aWi(new aCE(this, guo, new Object[]{new Boolean(z)}));
    }

    /* renamed from: jP */
    private void m25732jP(String str) {
        bFf().mo5608dq().mo3197f(gue, str);
    }

    /* renamed from: sa */
    private String m25734sa() {
        return (String) bFf().mo5608dq().mo3214p(f5286Nj);
    }

    /* renamed from: B */
    public void mo15961B(List<Player> list) {
        switch (bFf().mo6893i(gug)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, gug, new Object[]{list}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, gug, new Object[]{list}));
                break;
        }
        m25721A(list);
    }

    /* renamed from: KJ */
    public Player mo15962KJ() {
        switch (bFf().mo6893i(guj)) {
            case 0:
                return null;
            case 2:
                return (Player) bFf().mo5606d(new aCE(this, guj, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, guj, new Object[0]));
                break;
        }
        return ctM();
    }

    /* renamed from: S */
    public void mo10S() {
        super.mo10S();
    }

    /* renamed from: T */
    public C6494any mo11T() {
        return ___iScriptClass;
    }

    /* renamed from: U */
    public C2491fm[] mo12U() {
        return _m_methods;
    }

    /* renamed from: W */
    public Object mo13W() {
        return new C4123zn(this);
    }

    /* renamed from: a */
    public Object mo14a(C0495Gr gr) {
        Object[] args = gr.getArgs();
        switch (gr.mo2417hq() - TaikodomObject._m_methodCount) {
            case 0:
                m25727bP((Player) args[0]);
                return null;
            case 1:
                m25721A((List) args[0]);
                return null;
            case 2:
                return m25735si();
            case 3:
                m25724ak((String) args[0]);
                return null;
            case 4:
                return ctK();
            case 5:
                m25733jQ((String) args[0]);
                return null;
            case 6:
                return ctM();
            case 7:
                return ctN();
            case 8:
                return ctP();
            case 9:
                return ctQ();
            case 10:
                return new Boolean(ctS());
            case 11:
                m25731gu(((Boolean) args[0]).booleanValue());
                return null;
            case 12:
                return m25725au();
            default:
                return super.mo14a(gr);
        }
    }

    /* renamed from: a */
    public void mo15a(Collection collection, C0495Gr gr) {
        super.mo15a(collection, gr);
    }

    /* renamed from: bQ */
    public void mo15964bQ(Player aku) {
        switch (bFf().mo6893i(guf)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, guf, new Object[]{aku}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, guf, new Object[]{aku}));
                break;
        }
        m25727bP(aku);
    }

    /* renamed from: c */
    public C5663aRz[] mo25c() {
        return _m_fields;
    }

    public String ctL() {
        switch (bFf().mo6893i(guh)) {
            case 0:
                return null;
            case 2:
                return (String) bFf().mo5606d(new aCE(this, guh, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, guh, new Object[0]));
                break;
        }
        return ctK();
    }

    public List<Player> ctO() {
        switch (bFf().mo6893i(guk)) {
            case 0:
                return null;
            case 2:
                return (List) bFf().mo5606d(new aCE(this, guk, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, guk, new Object[0]));
                break;
        }
        return ctN();
    }

    public String ctR() {
        switch (bFf().mo6893i(gum)) {
            case 0:
                return null;
            case 2:
                return (String) bFf().mo5606d(new aCE(this, gum, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, gum, new Object[0]));
                break;
        }
        return ctQ();
    }

    public Date getDate() {
        switch (bFf().mo6893i(gul)) {
            case 0:
                return null;
            case 2:
                return (Date) bFf().mo5606d(new aCE(this, gul, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, gul, new Object[0]));
                break;
        }
        return ctP();
    }

    public String getSubject() {
        switch (bFf().mo6893i(f5287Nz)) {
            case 0:
                return null;
            case 2:
                return (String) bFf().mo5606d(new aCE(this, f5287Nz, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f5287Nz, new Object[0]));
                break;
        }
        return m25735si();
    }

    public void setSubject(String str) {
        switch (bFf().mo6893i(f5285NA)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f5285NA, new Object[]{str}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f5285NA, new Object[]{str}));
                break;
        }
        m25724ak(str);
    }

    @C5566aOg
    @C2499fr
    /* renamed from: gv */
    public void mo15970gv(boolean z) {
        switch (bFf().mo6893i(guo)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, guo, new Object[]{new Boolean(z)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, guo, new Object[]{new Boolean(z)}));
                break;
        }
        m25731gu(z);
    }

    public boolean isRead() {
        switch (bFf().mo6893i(gun)) {
            case 0:
                return false;
            case 2:
                return ((Boolean) bFf().mo5606d(new aCE(this, gun, new Object[0]))).booleanValue();
            case 3:
                bFf().mo5606d(new aCE(this, gun, new Object[0]));
                break;
        }
        return ctS();
    }

    public void setBody(String str) {
        switch (bFf().mo6893i(gui)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, gui, new Object[]{str}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, gui, new Object[]{str}));
                break;
        }
        m25733jQ(str);
    }

    public String toString() {
        switch (bFf().mo6893i(_f_toString_0020_0028_0029Ljava_002flang_002fString_003b)) {
            case 0:
                return null;
            case 2:
                return (String) bFf().mo5606d(new aCE(this, _f_toString_0020_0028_0029Ljava_002flang_002fString_003b, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, _f_toString_0020_0028_0029Ljava_002flang_002fString_003b, new Object[0]));
                break;
        }
        return m25725au();
    }

    /* renamed from: a */
    public void mo15963a(Player aku, String str, String str2) {
        super.mo10S();
        m25728c(new Date(System.currentTimeMillis()));
        m25726bO(aku);
        m25723ah(str);
        m25732jP(str2);
    }

    @C0064Am(aul = "69833980928b89193dfca2d1c6a4e0ea", aum = 0)
    /* renamed from: bP */
    private void m25727bP(Player aku) {
        ctH().add(aku);
    }

    @C0064Am(aul = "7acfdab88702ab774b3bc1c5872158a8", aum = 0)
    /* renamed from: A */
    private void m25721A(List<Player> list) {
        for (Player add : list) {
            ctH().add(add);
        }
    }

    @C0064Am(aul = "8c30309035b39a4d559ca2472adbd913", aum = 0)
    /* renamed from: si */
    private String m25735si() {
        return m25734sa();
    }

    @C0064Am(aul = "cf19d7ddf90be805f3a543fd6279884f", aum = 0)
    /* renamed from: ak */
    private void m25724ak(String str) {
        m25723ah(str);
    }

    @C0064Am(aul = "338bee9471578b1d242c2923c954e605", aum = 0)
    private String ctK() {
        return ctJ();
    }

    @C0064Am(aul = "fa4bca0ca6dca33e1cbe0474a5824b4e", aum = 0)
    /* renamed from: jQ */
    private void m25733jQ(String str) {
        m25732jP(str);
    }

    @C0064Am(aul = "27557764dd3fb642aa43df725d66d5e3", aum = 0)
    private Player ctM() {
        return ctI();
    }

    @C0064Am(aul = "e94b88d6c4270a94adbc8fb2cb393eb8", aum = 0)
    private List<Player> ctN() {
        return ctH();
    }

    @C0064Am(aul = "b21b60a3573a1bc1bcd6890af2f401c2", aum = 0)
    private Date ctP() {
        return ctF();
    }

    @C0064Am(aul = "b8df1535f842a4d1dbbf1c02d391b2ca", aum = 0)
    private String ctQ() {
        return dBH.format(ctF());
    }

    @C0064Am(aul = "b2f91828433af1505a1581c263f41456", aum = 0)
    private boolean ctS() {
        return ctG();
    }

    @C0064Am(aul = "65d50eb0e6d5fbe0beaf500602454e2b", aum = 0)
    /* renamed from: au */
    private String m25725au() {
        return m25734sa();
    }
}
