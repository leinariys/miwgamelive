package game.script;

import game.network.message.externalizable.aCE;
import game.script.login.UserConnection;
import game.script.player.Player;
import game.script.simulation.Space;
import gnu.trove.THashSet;
import logic.baa.C1616Xf;
import logic.baa.C1634Xv;
import logic.baa.C5511aMd;
import logic.baa.C6485anp;
import logic.data.mbean.C3697tu;
import logic.res.code.C5663aRz;
import logic.res.code.C6494any;
import logic.res.html.C2491fm;
import p001a.*;

import java.util.Collection;
import java.util.Set;

@C5511aMd
@C6485anp
/* renamed from: a.aTp  reason: case insensitive filesystem */
/* compiled from: a */
public class ClientUtils extends TaikodomObject implements C1616Xf {
    public static final int _m_fieldCount = 0;
    public static final C5663aRz[] _m_fields = null;
    public static final int _m_methodCount = 0;
    public static final C2491fm[] _m_methods = null;
    public static final C2491fm ePd = null;
    public static final C2491fm hlG = null;
    public static final C2491fm hzh = null;
    public static final C2491fm iOE = null;
    public static final C2491fm iOF = null;
    public static final C2491fm iOG = null;
    public static final C2491fm iOH = null;
    public static final C2491fm iOI = null;
    public static final C2491fm iOJ = null;
    public static final C2491fm iOK = null;
    public static final C2491fm iOL = null;
    public static final C2491fm iOM = null;
    public static final C2491fm iON = null;
    public static final C2491fm iOO = null;
    public static final C2491fm iOP = null;
    /* renamed from: pp */
    public static final C2491fm f3835pp = null;
    public static final long serialVersionUID = 0;
    public static C6494any ___iScriptClass;

    static {
        m18404V();
    }

    @ClientOnly
    private UserConnection dzV;
    @ClientOnly
    private Space iOA;
    private transient boolean iOB;
    @ClientOnly
    private long iOC;
    @ClientOnly
    private Set<Actor> iOD;
    @ClientOnly
    private C2127cJ iOz;

    public ClientUtils() {
        super((C5540aNg) null);
        super.mo10S();
    }

    public ClientUtils(C5540aNg ang) {
        super(ang);
    }

    /* renamed from: V */
    static void m18404V() {
        serialVersionUID = (long) 1;
        _m_fieldCount = TaikodomObject._m_fieldCount + 0;
        _m_methodCount = TaikodomObject._m_methodCount + 16;
        _m_fields = new C5663aRz[(TaikodomObject._m_fieldCount + 0)];
        C1634Xv.m11725a((Object[]) TaikodomObject._m_fields, (Object[]) _m_fields);
        int i = TaikodomObject._m_methodCount;
        C2491fm[] fmVarArr = new C2491fm[(i + 16)];
        C2491fm a = C4105zY.m41624a(ClientUtils.class, "60b186a9c830df5e2effe588f2470a84", i);
        hzh = a;
        fmVarArr[i] = a;
        int i2 = i + 1;
        C2491fm a2 = C4105zY.m41624a(ClientUtils.class, "e8941b284cdd05518a0ffcc7a7a340a1", i2);
        iOE = a2;
        fmVarArr[i2] = a2;
        int i3 = i2 + 1;
        C2491fm a3 = C4105zY.m41624a(ClientUtils.class, "a1dba59928aaa04b6d65db0dde16e4a8", i3);
        iOF = a3;
        fmVarArr[i3] = a3;
        int i4 = i3 + 1;
        C2491fm a4 = C4105zY.m41624a(ClientUtils.class, "a161deb75e5f22c5472c7c6b05d83684", i4);
        iOG = a4;
        fmVarArr[i4] = a4;
        int i5 = i4 + 1;
        C2491fm a5 = C4105zY.m41624a(ClientUtils.class, "e6d30f1ed2b1e02f918506870eab9344", i5);
        iOH = a5;
        fmVarArr[i5] = a5;
        int i6 = i5 + 1;
        C2491fm a6 = C4105zY.m41624a(ClientUtils.class, "8fbd17dcfa72a52953e33373a665753b", i6);
        iOI = a6;
        fmVarArr[i6] = a6;
        int i7 = i6 + 1;
        C2491fm a7 = C4105zY.m41624a(ClientUtils.class, "426999d3597fd0c2782477ef59296a19", i7);
        iOJ = a7;
        fmVarArr[i7] = a7;
        int i8 = i7 + 1;
        C2491fm a8 = C4105zY.m41624a(ClientUtils.class, "823e3ffc1812fd3301866b2d77955180", i8);
        f3835pp = a8;
        fmVarArr[i8] = a8;
        int i9 = i8 + 1;
        C2491fm a9 = C4105zY.m41624a(ClientUtils.class, "05a1e9216d9ab2ca90f8a743e17ef897", i9);
        iOK = a9;
        fmVarArr[i9] = a9;
        int i10 = i9 + 1;
        C2491fm a10 = C4105zY.m41624a(ClientUtils.class, "5f56661558e0adf1b6ed83576d56a532", i10);
        iOL = a10;
        fmVarArr[i10] = a10;
        int i11 = i10 + 1;
        C2491fm a11 = C4105zY.m41624a(ClientUtils.class, "874642c4f2a63553b6ed97cbf7bea932", i11);
        hlG = a11;
        fmVarArr[i11] = a11;
        int i12 = i11 + 1;
        C2491fm a12 = C4105zY.m41624a(ClientUtils.class, "a10e9a49e225fb84aae086deece9194f", i12);
        iOM = a12;
        fmVarArr[i12] = a12;
        int i13 = i12 + 1;
        C2491fm a13 = C4105zY.m41624a(ClientUtils.class, "7e7df553dcd05aa8b382e62a1a56721f", i13);
        iON = a13;
        fmVarArr[i13] = a13;
        int i14 = i13 + 1;
        C2491fm a14 = C4105zY.m41624a(ClientUtils.class, "cf3c1d676025a042fd8a47ae61b71b89", i14);
        iOO = a14;
        fmVarArr[i14] = a14;
        int i15 = i14 + 1;
        C2491fm a15 = C4105zY.m41624a(ClientUtils.class, "e06cdfb53e93518c193bac2e74f5d115", i15);
        iOP = a15;
        fmVarArr[i15] = a15;
        int i16 = i15 + 1;
        C2491fm a16 = C4105zY.m41624a(ClientUtils.class, "bf116c7824c414a82f569f7c7341510f", i16);
        ePd = a16;
        fmVarArr[i16] = a16;
        int i17 = i16 + 1;
        _m_methods = fmVarArr;
        C1634Xv.m11725a((Object[]) TaikodomObject._m_methods, (Object[]) _m_methods);
        ___iScriptClass = aUO.m18566a(ClientUtils.class, C3697tu.class, _m_fields, _m_methods);
    }

    @C0064Am(aul = "bf116c7824c414a82f569f7c7341510f", aum = 0)
    @C5566aOg
    @C2499fr
    private void bIV() {
        throw new aWi(new aCE(this, ePd, new Object[0]));
    }

    @C0064Am(aul = "a161deb75e5f22c5472c7c6b05d83684", aum = 0)
    @C5566aOg
    @C2499fr
    /* renamed from: cG */
    private void m18406cG(Player aku) {
        throw new aWi(new aCE(this, iOG, new Object[]{aku}));
    }

    @C5566aOg
    @C2499fr
    /* renamed from: cH */
    private void m18407cH(Player aku) {
        switch (bFf().mo6893i(iOG)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, iOG, new Object[]{aku}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, iOG, new Object[]{aku}));
                break;
        }
        m18406cG(aku);
    }

    @C0064Am(aul = "e8941b284cdd05518a0ffcc7a7a340a1", aum = 0)
    @C5566aOg
    @C2499fr
    private UserConnection dvG() {
        throw new aWi(new aCE(this, iOE, new Object[0]));
    }

    @C5566aOg
    @C2499fr
    private UserConnection dvH() {
        switch (bFf().mo6893i(iOE)) {
            case 0:
                return null;
            case 2:
                return (UserConnection) bFf().mo5606d(new aCE(this, iOE, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, iOE, new Object[0]));
                break;
        }
        return dvG();
    }

    /* renamed from: T */
    public C6494any mo11T() {
        return ___iScriptClass;
    }

    /* renamed from: U */
    public C2491fm[] mo12U() {
        return _m_methods;
    }

    /* renamed from: W */
    public Object mo13W() {
        return new C3697tu(this);
    }

    /* renamed from: a */
    public Object mo14a(C0495Gr gr) {
        Object[] args = gr.getArgs();
        switch (gr.mo2417hq() - TaikodomObject._m_methodCount) {
            case 0:
                return cSU();
            case 1:
                return dvG();
            case 2:
                m18405cF((Player) args[0]);
                return null;
            case 3:
                m18406cG((Player) args[0]);
                return null;
            case 4:
                m18408cv((Actor) args[0]);
                return null;
            case 5:
                dvI();
                return null;
            case 6:
                return new Long(dvK());
            case 7:
                m18410gY();
                return null;
            case 8:
                dvL();
                return null;
            case 9:
                return dvN();
            case 10:
                cMf();
                return null;
            case 11:
                return new Long(dvP());
            case 12:
                m18411lY(((Long) args[0]).longValue());
                return null;
            case 13:
                return dvR();
            case 14:
                m18409cx((Actor) args[0]);
                return null;
            case 15:
                bIV();
                return null;
            default:
                return super.mo14a(gr);
        }
    }

    /* renamed from: a */
    public void mo15a(Collection collection, C0495Gr gr) {
        super.mo15a(collection, gr);
    }

    @C5566aOg
    @C2499fr
    public void bIW() {
        switch (bFf().mo6893i(ePd)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, ePd, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, ePd, new Object[0]));
                break;
        }
        bIV();
    }

    public long bvj() {
        switch (bFf().mo6893i(iOJ)) {
            case 0:
                return 0;
            case 2:
                return ((Long) bFf().mo5606d(new aCE(this, iOJ, new Object[0]))).longValue();
            case 3:
                bFf().mo5606d(new aCE(this, iOJ, new Object[0]));
                break;
        }
        return dvK();
    }

    /* renamed from: c */
    public C5663aRz[] mo25c() {
        return _m_fields;
    }

    @ClientOnly
    public void cMg() {
        switch (bFf().mo6893i(hlG)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, hlG, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, hlG, new Object[0]));
                break;
        }
        cMf();
    }

    public UserConnection cSV() {
        switch (bFf().mo6893i(hzh)) {
            case 0:
                return null;
            case 2:
                return (UserConnection) bFf().mo5606d(new aCE(this, hzh, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, hzh, new Object[0]));
                break;
        }
        return cSU();
    }

    @ClientOnly
    /* renamed from: cd */
    public void mo11534cd(Player aku) {
        switch (bFf().mo6893i(iOF)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, iOF, new Object[]{aku}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, iOF, new Object[]{aku}));
                break;
        }
        m18405cF(aku);
    }

    @ClientOnly
    /* renamed from: cw */
    public void mo11535cw(Actor cr) {
        switch (bFf().mo6893i(iOH)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, iOH, new Object[]{cr}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, iOH, new Object[]{cr}));
                break;
        }
        m18408cv(cr);
    }

    /* renamed from: cy */
    public void mo11536cy(Actor cr) {
        switch (bFf().mo6893i(iOP)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, iOP, new Object[]{cr}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, iOP, new Object[]{cr}));
                break;
        }
        m18409cx(cr);
    }

    public void dvJ() {
        switch (bFf().mo6893i(iOI)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, iOI, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, iOI, new Object[0]));
                break;
        }
        dvI();
    }

    public void dvM() {
        switch (bFf().mo6893i(iOK)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, iOK, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, iOK, new Object[0]));
                break;
        }
        dvL();
    }

    public Space dvO() {
        switch (bFf().mo6893i(iOL)) {
            case 0:
                return null;
            case 2:
                return (Space) bFf().mo5606d(new aCE(this, iOL, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, iOL, new Object[0]));
                break;
        }
        return dvN();
    }

    public long dvQ() {
        switch (bFf().mo6893i(iOM)) {
            case 0:
                return 0;
            case 2:
                return ((Long) bFf().mo5606d(new aCE(this, iOM, new Object[0]))).longValue();
            case 3:
                bFf().mo5606d(new aCE(this, iOM, new Object[0]));
                break;
        }
        return dvP();
    }

    public Set<Actor> dvS() {
        switch (bFf().mo6893i(iOO)) {
            case 0:
                return null;
            case 2:
                return (Set) bFf().mo5606d(new aCE(this, iOO, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, iOO, new Object[0]));
                break;
        }
        return dvR();
    }

    @ClientOnly
    /* renamed from: gZ */
    public void mo11542gZ() {
        switch (bFf().mo6893i(f3835pp)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f3835pp, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f3835pp, new Object[0]));
                break;
        }
        m18410gY();
    }

    /* renamed from: lZ */
    public void mo11543lZ(long j) {
        switch (bFf().mo6893i(iON)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, iON, new Object[]{new Long(j)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, iON, new Object[]{new Long(j)}));
                break;
        }
        m18411lY(j);
    }

    /* renamed from: S */
    public void mo10S() {
        super.mo10S();
        this.iOB = false;
    }

    @C0064Am(aul = "60b186a9c830df5e2effe588f2470a84", aum = 0)
    private UserConnection cSU() {
        if (this.dzV == null) {
            this.dzV = dvH();
        }
        return this.dzV;
    }

    @C0064Am(aul = "a1dba59928aaa04b6d65db0dde16e4a8", aum = 0)
    @ClientOnly
    /* renamed from: cF */
    private void m18405cF(Player aku) {
        m18407cH(aku);
    }

    @C0064Am(aul = "e6d30f1ed2b1e02f918506870eab9344", aum = 0)
    @ClientOnly
    /* renamed from: cv */
    private void m18408cv(Actor cr) {
        Space Zc = cr.mo965Zc();
        if (Zc != this.iOA) {
            this.iOA = Zc;
            this.iOA.init();
        }
    }

    @C0064Am(aul = "8fbd17dcfa72a52953e33373a665753b", aum = 0)
    private void dvI() {
        this.iOC = bFf().mo6866PM().getSynchronizerTime().currentTimeMillis();
    }

    @C0064Am(aul = "426999d3597fd0c2782477ef59296a19", aum = 0)
    private long dvK() {
        if (this.iOA == null || !this.iOA.cKF() || this.iOB || this.iOA.cKn() == null) {
            return SingletonCurrentTimeMilliImpl.currentTimeMillis();
        }
        return this.iOA.cKn().bvj();
    }

    @C0064Am(aul = "823e3ffc1812fd3301866b2d77955180", aum = 0)
    @ClientOnly
    /* renamed from: gY */
    private void m18410gY() {
        if (this.iOD == null) {
            this.iOD = new THashSet();
        }
        if (this.iOA != null && this.iOA.cKF()) {
            if (!this.iOB) {
                this.iOA.mo1915jB(-1);
                this.iOA.cKt();
            }
            for (Actor next : this.iOD) {
                try {
                    if (next.cLZ()) {
                        next.mo991b(0, true);
                    }
                } catch (Exception e) {
                    mo8354g("err:", (Throwable) e);
                }
            }
            this.iOD.clear();
        }
        if (this.iOz != null) {
            this.iOz.mo17546gZ();
        }
        if (bGX() && this.iOA != null) {
            ald().alb().cMZ();
        }
    }

    @C0064Am(aul = "05a1e9216d9ab2ca90f8a743e17ef897", aum = 0)
    private void dvL() {
        if (this.iOA != null) {
            try {
                this.iOA.stop();
            } catch (Exception e) {
                mo8358lY(e.getMessage());
            }
            this.iOA = null;
        }
    }

    @C0064Am(aul = "5f56661558e0adf1b6ed83576d56a532", aum = 0)
    private Space dvN() {
        return this.iOA;
    }

    @C0064Am(aul = "874642c4f2a63553b6ed97cbf7bea932", aum = 0)
    @ClientOnly
    private void cMf() {
    }

    @C0064Am(aul = "a10e9a49e225fb84aae086deece9194f", aum = 0)
    private long dvP() {
        return this.iOC;
    }

    @C0064Am(aul = "7e7df553dcd05aa8b382e62a1a56721f", aum = 0)
    /* renamed from: lY */
    private void m18411lY(long j) {
        this.iOC = j;
    }

    @C0064Am(aul = "cf3c1d676025a042fd8a47ae61b71b89", aum = 0)
    private Set<Actor> dvR() {
        return this.iOD;
    }

    @C0064Am(aul = "e06cdfb53e93518c193bac2e74f5d115", aum = 0)
    /* renamed from: cx */
    private void m18409cx(Actor cr) {
        if (cr != null && bGX()) {
            this.iOD.add(cr);
        }
    }
}
