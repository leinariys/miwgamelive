package game.script.contract;

import game.network.manager.C6546aoy;
import game.network.message.externalizable.aCE;
import game.network.message.serializable.C2686iZ;
import game.network.message.serializable.C5553aNt;
import game.script.contract.types.BountyBaseInfo;
import game.script.contract.types.ContractTemplate;
import game.script.contract.types.TransportBaseInfo;
import game.script.item.BagItem;
import game.script.item.Item;
import game.script.item.ItemLocation;
import game.script.player.Player;
import game.script.ship.Station;
import logic.baa.*;
import logic.data.link.C3161oY;
import logic.data.mbean.C6183ahz;
import logic.data.mbean.ayI;
import logic.res.code.C5640aRc;
import logic.res.code.C5663aRz;
import logic.res.code.C6494any;
import logic.res.html.C2491fm;
import p001a.*;

import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.Collection;
import java.util.Collections;
import java.util.Set;
import java.util.UUID;

@C5511aMd
@C6485anp
/* renamed from: a.hC */
/* compiled from: a */
public class ContractBoard extends TaikodomObject implements C0468GU, C1616Xf {
    public static final C2491fm _f_onResurrect_0020_0028_0029V = null;
    public static final int _m_fieldCount = 0;
    public static final C5663aRz[] _m_fields = null;
    public static final int _m_methodCount = 0;
    public static final C2491fm[] _m_methods = null;
    public static final C5663aRz aJw = null;
    /* renamed from: bL */
    public static final C5663aRz f7844bL = null;
    /* renamed from: bN */
    public static final C2491fm f7845bN = null;
    /* renamed from: bO */
    public static final C2491fm f7846bO = null;
    /* renamed from: bP */
    public static final C2491fm f7847bP = null;
    public static final C5663aRz hdW = null;
    public static final C5663aRz hdX = null;
    public static final C5663aRz hdY = null;
    public static final C2491fm hdZ = null;
    public static final C2491fm hea = null;
    public static final C2491fm heb = null;
    public static final C2491fm hec = null;
    public static final C2491fm hed = null;
    public static final C2491fm hee = null;
    public static final C2491fm hef = null;
    public static final C2491fm heg = null;
    public static final C2491fm heh = null;
    public static final C2491fm hei = null;
    public static final C2491fm hej = null;
    public static final C2491fm hek = null;
    public static final C2491fm hel = null;
    public static final C2491fm hem = null;
    public static final C2491fm hen = null;
    public static final long serialVersionUID = 0;
    public static C6494any ___iScriptClass;
    @C0064Am(aul = "e9bcc88ea0fb8094b5757f8f27d5caac", aum = 2)

    /* renamed from: bK */
    private static UUID f7843bK;
    private static /* synthetic */ int[] dng;
    @C0064Am(aul = "5e1f73fe93eded9d7186593a0cb1a579", aum = 0)
    private static C2686iZ<ContractTemplate> fKX;
    @C0064Am(aul = "b5152398d3bb8599850a69c1b96d1324", aum = 1)
    private static PlayerDisposedListener fKY;
    @C0064Am(aul = "7588aae05e7a0a0be5703079873a505e", aum = 3)
    private static TransportBaseInfo fKZ;
    @C0064Am(aul = "c9510742837d54e1d4b83032ebd77f00", aum = 4)
    private static BountyBaseInfo fLa;

    static {
        m32471V();
    }

    public ContractBoard() {
        super((C5540aNg) null);
        super.mo10S();
    }

    public ContractBoard(C5540aNg ang) {
        super(ang);
    }

    /* renamed from: V */
    static void m32471V() {
        serialVersionUID = (long) 1;
        _m_fieldCount = TaikodomObject._m_fieldCount + 5;
        _m_methodCount = TaikodomObject._m_methodCount + 19;
        int i = TaikodomObject._m_fieldCount;
        C5663aRz[] arzArr = new C5663aRz[(i + 5)];
        C5663aRz b = C5640aRc.m17844b(ContractBoard.class, "5e1f73fe93eded9d7186593a0cb1a579", i);
        hdW = b;
        arzArr[i] = b;
        int i2 = i + 1;
        C5663aRz b2 = C5640aRc.m17844b(ContractBoard.class, "b5152398d3bb8599850a69c1b96d1324", i2);
        aJw = b2;
        arzArr[i2] = b2;
        int i3 = i2 + 1;
        C5663aRz b3 = C5640aRc.m17844b(ContractBoard.class, "e9bcc88ea0fb8094b5757f8f27d5caac", i3);
        f7844bL = b3;
        arzArr[i3] = b3;
        int i4 = i3 + 1;
        C5663aRz b4 = C5640aRc.m17844b(ContractBoard.class, "7588aae05e7a0a0be5703079873a505e", i4);
        hdX = b4;
        arzArr[i4] = b4;
        int i5 = i4 + 1;
        C5663aRz b5 = C5640aRc.m17844b(ContractBoard.class, "c9510742837d54e1d4b83032ebd77f00", i5);
        hdY = b5;
        arzArr[i5] = b5;
        int i6 = i5 + 1;
        _m_fields = arzArr;
        C1634Xv.m11725a((Object[]) TaikodomObject._m_fields, (Object[]) _m_fields);
        int i7 = TaikodomObject._m_methodCount;
        C2491fm[] fmVarArr = new C2491fm[(i7 + 19)];
        C2491fm a = C4105zY.m41624a(ContractBoard.class, "ea59b188fcaff132bd1108fb4c893602", i7);
        f7845bN = a;
        fmVarArr[i7] = a;
        int i8 = i7 + 1;
        C2491fm a2 = C4105zY.m41624a(ContractBoard.class, "398059fa55866aedc2f440ab449605bf", i8);
        f7846bO = a2;
        fmVarArr[i8] = a2;
        int i9 = i8 + 1;
        C2491fm a3 = C4105zY.m41624a(ContractBoard.class, "2fc738af9159138db2ac49a48138cb3b", i9);
        f7847bP = a3;
        fmVarArr[i9] = a3;
        int i10 = i9 + 1;
        C2491fm a4 = C4105zY.m41624a(ContractBoard.class, "57592db789470f9c027b25a70ce97e57", i10);
        hdZ = a4;
        fmVarArr[i10] = a4;
        int i11 = i10 + 1;
        C2491fm a5 = C4105zY.m41624a(ContractBoard.class, "bd434f6d1f8dac5054ecd471e147814f", i11);
        hea = a5;
        fmVarArr[i11] = a5;
        int i12 = i11 + 1;
        C2491fm a6 = C4105zY.m41624a(ContractBoard.class, "0bc527e523095c0c6a32d6a234910925", i12);
        heb = a6;
        fmVarArr[i12] = a6;
        int i13 = i12 + 1;
        C2491fm a7 = C4105zY.m41624a(ContractBoard.class, "0371f3b58f7ec0c855273e4a71f7aa93", i13);
        hec = a7;
        fmVarArr[i13] = a7;
        int i14 = i13 + 1;
        C2491fm a8 = C4105zY.m41624a(ContractBoard.class, "b5eaf70089ca72fad1a48ecb53a695e6", i14);
        hed = a8;
        fmVarArr[i14] = a8;
        int i15 = i14 + 1;
        C2491fm a9 = C4105zY.m41624a(ContractBoard.class, "f7ca4d385d8f40f396a00adab8b7455c", i15);
        hee = a9;
        fmVarArr[i15] = a9;
        int i16 = i15 + 1;
        C2491fm a10 = C4105zY.m41624a(ContractBoard.class, "5552ea60ccdf25d5ebdc3cc792a0576a", i16);
        hef = a10;
        fmVarArr[i16] = a10;
        int i17 = i16 + 1;
        C2491fm a11 = C4105zY.m41624a(ContractBoard.class, "b6891fb5e1988de65fa2fea80ce9fb9c", i17);
        heg = a11;
        fmVarArr[i17] = a11;
        int i18 = i17 + 1;
        C2491fm a12 = C4105zY.m41624a(ContractBoard.class, "4c80e70fc31748e379c8edce8c76c7b9", i18);
        heh = a12;
        fmVarArr[i18] = a12;
        int i19 = i18 + 1;
        C2491fm a13 = C4105zY.m41624a(ContractBoard.class, "07b84346ab264632c9e93d18f8906c3f", i19);
        hei = a13;
        fmVarArr[i19] = a13;
        int i20 = i19 + 1;
        C2491fm a14 = C4105zY.m41624a(ContractBoard.class, "80cc6df3747a5c5e3da3e663523b36d7", i20);
        hej = a14;
        fmVarArr[i20] = a14;
        int i21 = i20 + 1;
        C2491fm a15 = C4105zY.m41624a(ContractBoard.class, "f3711ee82ba4c197eb3bb891bc4e5cf1", i21);
        hek = a15;
        fmVarArr[i21] = a15;
        int i22 = i21 + 1;
        C2491fm a16 = C4105zY.m41624a(ContractBoard.class, "e5b835acdf49f07649bf7b213df5833b", i22);
        hel = a16;
        fmVarArr[i22] = a16;
        int i23 = i22 + 1;
        C2491fm a17 = C4105zY.m41624a(ContractBoard.class, "b3455161d5f5f48d208c21b77e8254dd", i23);
        hem = a17;
        fmVarArr[i23] = a17;
        int i24 = i23 + 1;
        C2491fm a18 = C4105zY.m41624a(ContractBoard.class, "acb4c065134b0ad81b1a3527042c027d", i24);
        hen = a18;
        fmVarArr[i24] = a18;
        int i25 = i24 + 1;
        C2491fm a19 = C4105zY.m41624a(ContractBoard.class, "9949fcd2619dea6e957a1b128d494936", i25);
        _f_onResurrect_0020_0028_0029V = a19;
        fmVarArr[i25] = a19;
        int i26 = i25 + 1;
        _m_methods = fmVarArr;
        C1634Xv.m11725a((Object[]) TaikodomObject._m_methods, (Object[]) _m_methods);
        ___iScriptClass = aUO.m18566a(ContractBoard.class, C6183ahz.class, _m_fields, _m_methods);
    }

    static /* synthetic */ int[] baP() {
        int[] iArr = dng;
        if (iArr == null) {
            iArr = new int[ContractTemplate.C0104a.values().length];
            try {
                iArr[ContractTemplate.C0104a.AVAILABLE.ordinal()] = 1;
            } catch (NoSuchFieldError e) {
            }
            try {
                iArr[ContractTemplate.C0104a.DEAD.ordinal()] = 3;
            } catch (NoSuchFieldError e2) {
            }
            try {
                iArr[ContractTemplate.C0104a.TAKEN.ordinal()] = 2;
            } catch (NoSuchFieldError e3) {
            }
            dng = iArr;
        }
        return iArr;
    }

    /* renamed from: a */
    private void m32472a(BountyBaseInfo ane) {
        bFf().mo5608dq().mo3197f(hdY, ane);
    }

    @C0064Am(aul = "4c80e70fc31748e379c8edce8c76c7b9", aum = 0)
    @C5566aOg
    @C2499fr
    /* renamed from: a */
    private void m32473a(C5553aNt ant) {
        throw new aWi(new aCE(this, heh, new Object[]{ant}));
    }

    /* renamed from: a */
    private void m32475a(TransportBaseInfo gcVar) {
        bFf().mo5608dq().mo3197f(hdX, gcVar);
    }

    /* renamed from: a */
    private void m32476a(PlayerDisposedListener bVar) {
        bFf().mo5608dq().mo3197f(aJw, bVar);
    }

    /* renamed from: a */
    private void m32477a(UUID uuid) {
        bFf().mo5608dq().mo3197f(f7844bL, uuid);
    }

    /* renamed from: ac */
    private void m32479ac(C2686iZ iZVar) {
        bFf().mo5608dq().mo3197f(hdW, iZVar);
    }

    /* renamed from: an */
    private UUID m32480an() {
        return (UUID) bFf().mo5608dq().mo3214p(f7844bL);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "BountyContract")
    @C0064Am(aul = "f7ca4d385d8f40f396a00adab8b7455c", aum = 0)
    @C5566aOg
    /* renamed from: b */
    private void m32483b(BountyBaseInfo ane) {
        throw new aWi(new aCE(this, hee, new Object[]{ane}));
    }

    /* renamed from: b */
    private void m32484b(Item auq, ItemLocation aag, Station bf, C1493Vz vz) {
        switch (bFf().mo6893i(hei)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, hei, new Object[]{auq, aag, bf, vz}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, hei, new Object[]{auq, aag, bf, vz}));
                break;
        }
        m32474a(auq, aag, bf, vz);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "TransportContract")
    @C0064Am(aul = "0371f3b58f7ec0c855273e4a71f7aa93", aum = 0)
    @C5566aOg
    /* renamed from: b */
    private void m32485b(TransportBaseInfo gcVar) {
        throw new aWi(new aCE(this, hec, new Object[]{gcVar}));
    }

    /* renamed from: c */
    private void m32487c(UUID uuid) {
        switch (bFf().mo6893i(f7846bO)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f7846bO, new Object[]{uuid}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f7846bO, new Object[]{uuid}));
                break;
        }
        m32486b(uuid);
    }

    /* access modifiers changed from: private */
    public C2686iZ cHP() {
        return (C2686iZ) bFf().mo5608dq().mo3214p(hdW);
    }

    private PlayerDisposedListener cHQ() {
        return (PlayerDisposedListener) bFf().mo5608dq().mo3214p(aJw);
    }

    private TransportBaseInfo cHR() {
        return (TransportBaseInfo) bFf().mo5608dq().mo3214p(hdX);
    }

    private BountyBaseInfo cHS() {
        return (BountyBaseInfo) bFf().mo5608dq().mo3214p(hdY);
    }

    /* renamed from: cf */
    private boolean m32489cf(Player aku) {
        switch (bFf().mo6893i(hen)) {
            case 0:
                return false;
            case 2:
                return ((Boolean) bFf().mo5606d(new aCE(this, hen, new Object[]{aku}))).booleanValue();
            case 3:
                bFf().mo5606d(new aCE(this, hen, new Object[]{aku}));
                break;
        }
        return m32488ce(aku);
    }

    @C0064Am(aul = "5552ea60ccdf25d5ebdc3cc792a0576a", aum = 0)
    @C5566aOg
    @C2499fr
    /* renamed from: f */
    private boolean m32491f(ContractTemplate bj) {
        throw new aWi(new aCE(this, hef, new Object[]{bj}));
    }

    @C0064Am(aul = "b6891fb5e1988de65fa2fea80ce9fb9c", aum = 0)
    @C5566aOg
    @C2499fr
    /* renamed from: h */
    private boolean m32492h(ContractTemplate bj) {
        throw new aWi(new aCE(this, heg, new Object[]{bj}));
    }

    @C0064Am(aul = "f3711ee82ba4c197eb3bb891bc4e5cf1", aum = 0)
    @C5566aOg
    /* renamed from: l */
    private void m32494l(ContractTemplate bj) {
        throw new aWi(new aCE(this, hek, new Object[]{bj}));
    }

    @C0064Am(aul = "e5b835acdf49f07649bf7b213df5833b", aum = 0)
    @C5566aOg
    /* renamed from: n */
    private void m32495n(ContractTemplate bj) {
        throw new aWi(new aCE(this, hel, new Object[]{bj}));
    }

    @C0064Am(aul = "b3455161d5f5f48d208c21b77e8254dd", aum = 0)
    @C5566aOg
    /* renamed from: p */
    private boolean m32496p(ContractTemplate bj) {
        throw new aWi(new aCE(this, hem, new Object[]{bj}));
    }

    /* renamed from: T */
    public C6494any mo11T() {
        return ___iScriptClass;
    }

    /* renamed from: U */
    public C2491fm[] mo12U() {
        return _m_methods;
    }

    /* renamed from: W */
    public Object mo13W() {
        return new C6183ahz(this);
    }

    /* renamed from: a */
    public Object mo14a(C0495Gr gr) {
        Object[] args = gr.getArgs();
        switch (gr.mo2417hq() - TaikodomObject._m_methodCount) {
            case 0:
                return m32481ap();
            case 1:
                m32486b((UUID) args[0]);
                return null;
            case 2:
                return m32482ar();
            case 3:
                return cHT();
            case 4:
                return new Integer(cHV());
            case 5:
                return cHX();
            case 6:
                m32485b((TransportBaseInfo) args[0]);
                return null;
            case 7:
                return cHZ();
            case 8:
                m32483b((BountyBaseInfo) args[0]);
                return null;
            case 9:
                return new Boolean(m32491f((ContractTemplate) args[0]));
            case 10:
                return new Boolean(m32492h((ContractTemplate) args[0]));
            case 11:
                m32473a((C5553aNt) args[0]);
                return null;
            case 12:
                m32474a((Item) args[0], (ItemLocation) args[1], (Station) args[2], (C1493Vz) args[3]);
                return null;
            case 13:
                return new Long(m32493j((ContractTemplate) args[0]));
            case 14:
                m32494l((ContractTemplate) args[0]);
                return null;
            case 15:
                m32495n((ContractTemplate) args[0]);
                return null;
            case 16:
                return new Boolean(m32496p((ContractTemplate) args[0]));
            case 17:
                return new Boolean(m32488ce((Player) args[0]));
            case 18:
                m32478aG();
                return null;
            default:
                return super.mo14a(gr);
        }
    }

    /* renamed from: a */
    public void mo15a(Collection collection, C0495Gr gr) {
        super.mo15a(collection, gr);
    }

    /* renamed from: aH */
    public void mo70aH() {
        switch (bFf().mo6893i(_f_onResurrect_0020_0028_0029V)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, _f_onResurrect_0020_0028_0029V, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, _f_onResurrect_0020_0028_0029V, new Object[0]));
                break;
        }
        m32478aG();
    }

    /* renamed from: aq */
    public UUID mo18aq() {
        switch (bFf().mo6893i(f7845bN)) {
            case 0:
                return null;
            case 2:
                return (UUID) bFf().mo5606d(new aCE(this, f7845bN, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f7845bN, new Object[0]));
                break;
        }
        return m32481ap();
    }

    @C5566aOg
    @C2499fr
    /* renamed from: b */
    public void mo19170b(C5553aNt ant) {
        switch (bFf().mo6893i(heh)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, heh, new Object[]{ant}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, heh, new Object[]{ant}));
                break;
        }
        m32473a(ant);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "BountyContract")
    @C5566aOg
    /* renamed from: c */
    public void mo19171c(BountyBaseInfo ane) {
        switch (bFf().mo6893i(hee)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, hee, new Object[]{ane}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, hee, new Object[]{ane}));
                break;
        }
        m32483b(ane);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "TransportContract")
    @C5566aOg
    /* renamed from: c */
    public void mo19172c(TransportBaseInfo gcVar) {
        switch (bFf().mo6893i(hec)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, hec, new Object[]{gcVar}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, hec, new Object[]{gcVar}));
                break;
        }
        m32485b(gcVar);
    }

    /* renamed from: c */
    public C5663aRz[] mo25c() {
        return _m_fields;
    }

    public Set<ContractTemplate> cHU() {
        switch (bFf().mo6893i(hdZ)) {
            case 0:
                return null;
            case 2:
                return (Set) bFf().mo5606d(new aCE(this, hdZ, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, hdZ, new Object[0]));
                break;
        }
        return cHT();
    }

    public int cHW() {
        switch (bFf().mo6893i(hea)) {
            case 0:
                return 0;
            case 2:
                return ((Integer) bFf().mo5606d(new aCE(this, hea, new Object[0]))).intValue();
            case 3:
                bFf().mo5606d(new aCE(this, hea, new Object[0]));
                break;
        }
        return cHV();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "TransportContract")
    public TransportBaseInfo cHY() {
        switch (bFf().mo6893i(heb)) {
            case 0:
                return null;
            case 2:
                return (TransportBaseInfo) bFf().mo5606d(new aCE(this, heb, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, heb, new Object[0]));
                break;
        }
        return cHX();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "BountyContract")
    public BountyBaseInfo cIa() {
        switch (bFf().mo6893i(hed)) {
            case 0:
                return null;
            case 2:
                return (BountyBaseInfo) bFf().mo5606d(new aCE(this, hed, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, hed, new Object[0]));
                break;
        }
        return cHZ();
    }

    @C5566aOg
    @C2499fr
    /* renamed from: g */
    public boolean mo19177g(ContractTemplate bj) {
        switch (bFf().mo6893i(hef)) {
            case 0:
                return false;
            case 2:
                return ((Boolean) bFf().mo5606d(new aCE(this, hef, new Object[]{bj}))).booleanValue();
            case 3:
                bFf().mo5606d(new aCE(this, hef, new Object[]{bj}));
                break;
        }
        return m32491f(bj);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Handle")
    public String getHandle() {
        switch (bFf().mo6893i(f7847bP)) {
            case 0:
                return null;
            case 2:
                return (String) bFf().mo5606d(new aCE(this, f7847bP, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f7847bP, new Object[0]));
                break;
        }
        return m32482ar();
    }

    @C5566aOg
    @C2499fr
    /* renamed from: i */
    public boolean mo19178i(ContractTemplate bj) {
        switch (bFf().mo6893i(heg)) {
            case 0:
                return false;
            case 2:
                return ((Boolean) bFf().mo5606d(new aCE(this, heg, new Object[]{bj}))).booleanValue();
            case 3:
                bFf().mo5606d(new aCE(this, heg, new Object[]{bj}));
                break;
        }
        return m32492h(bj);
    }

    /* renamed from: k */
    public long mo19179k(ContractTemplate bj) {
        switch (bFf().mo6893i(hej)) {
            case 0:
                return 0;
            case 2:
                return ((Long) bFf().mo5606d(new aCE(this, hej, new Object[]{bj}))).longValue();
            case 3:
                bFf().mo5606d(new aCE(this, hej, new Object[]{bj}));
                break;
        }
        return m32493j(bj);
    }

    @C5566aOg
    /* renamed from: m */
    public void mo19180m(ContractTemplate bj) {
        switch (bFf().mo6893i(hek)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, hek, new Object[]{bj}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, hek, new Object[]{bj}));
                break;
        }
        m32494l(bj);
    }

    @C5566aOg
    /* renamed from: o */
    public void mo19181o(ContractTemplate bj) {
        switch (bFf().mo6893i(hel)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, hel, new Object[]{bj}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, hel, new Object[]{bj}));
                break;
        }
        m32495n(bj);
    }

    @C5566aOg
    /* renamed from: q */
    public boolean mo19182q(ContractTemplate bj) {
        switch (bFf().mo6893i(hem)) {
            case 0:
                return false;
            case 2:
                return ((Boolean) bFf().mo5606d(new aCE(this, hem, new Object[]{bj}))).booleanValue();
            case 3:
                bFf().mo5606d(new aCE(this, hem, new Object[]{bj}));
                break;
        }
        return m32496p(bj);
    }

    @C0064Am(aul = "ea59b188fcaff132bd1108fb4c893602", aum = 0)
    /* renamed from: ap */
    private UUID m32481ap() {
        return m32480an();
    }

    @C0064Am(aul = "398059fa55866aedc2f440ab449605bf", aum = 0)
    /* renamed from: b */
    private void m32486b(UUID uuid) {
        m32477a(uuid);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Handle")
    @C0064Am(aul = "2fc738af9159138db2ac49a48138cb3b", aum = 0)
    /* renamed from: ar */
    private String m32482ar() {
        return "contract_board";
    }

    /* renamed from: S */
    public void mo10S() {
        super.mo10S();
        PlayerDisposedListener bVar = (PlayerDisposedListener) bFf().mo6865M(PlayerDisposedListener.class);
        bVar.mo19183e(this);
        m32476a(bVar);
        m32477a(UUID.randomUUID());
    }

    @C0064Am(aul = "57592db789470f9c027b25a70ce97e57", aum = 0)
    private Set<ContractTemplate> cHT() {
        return Collections.unmodifiableSet(cHP());
    }

    @C0064Am(aul = "bd434f6d1f8dac5054ecd471e147814f", aum = 0)
    private int cHV() {
        return cHP().size();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "TransportContract")
    @C0064Am(aul = "0bc527e523095c0c6a32d6a234910925", aum = 0)
    private TransportBaseInfo cHX() {
        return cHR();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "BountyContract")
    @C0064Am(aul = "b5eaf70089ca72fad1a48ecb53a695e6", aum = 0)
    private BountyBaseInfo cHZ() {
        return cHS();
    }

    @C0064Am(aul = "07b84346ab264632c9e93d18f8906c3f", aum = 0)
    /* renamed from: a */
    private void m32474a(Item auq, ItemLocation aag, Station bf, C1493Vz vz) {
        if (auq instanceof BagItem) {
            mo8358lY("Trying to add a bagItem to a transport bag. Player " + aPA());
            throw new C2579c(C2580d.TRANSPORT_INCONSISTENT_BAG);
        } else if (!C6432amo.m24003a(auq, (C1493Vz) aag)) {
            if (!C6432amo.m24002a(auq, bf)) {
                mo8358lY("Trying to add an item from a different station in the bag. Player " + aPA());
                throw new C2579c(C2580d.TRANSPORT_INCONSISTENT_BAG);
            } else if (vz == null || !vz.aRz().contains(auq)) {
                aag.mo7615N(auq);
            } else {
                mo8358lY("Trying to add an item to the bag and the reward. Player " + aPA());
                throw new C2579c(C2580d.TRANSPORT_INCONSISTENT_BAG);
            }
        }
    }

    @C0064Am(aul = "80cc6df3747a5c5e3da3e663523b36d7", aum = 0)
    /* renamed from: j */
    private long m32493j(ContractTemplate bj) {
        if (cHP().contains(bj)) {
            return (bj.getCreationTime() + bj.aAE()) - cVr();
        }
        return -1;
    }

    /* JADX WARNING: Removed duplicated region for block: B:4:0x0010  */
    @p001a.C0064Am(aul = "acb4c065134b0ad81b1a3527042c027d", aum = 0)
    /* renamed from: ce */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private boolean m32488ce(Player r4) {
        /*
            r3 = this;
            a.iZ r0 = r3.cHP()
            java.util.Iterator r1 = r0.iterator()
        L_0x0008:
            boolean r0 = r1.hasNext()
            if (r0 != 0) goto L_0x0010
            r0 = 0
        L_0x000f:
            return r0
        L_0x0010:
            java.lang.Object r0 = r1.next()
            a.BJ r0 = (p001a.C0103BJ) r0
            a.akU r2 = r0.aAw()
            if (r2 == r4) goto L_0x0022
            a.akU r0 = r0.aAC()
            if (r0 != r4) goto L_0x0008
        L_0x0022:
            r0 = 1
            goto L_0x000f
        */
        throw new UnsupportedOperationException("Method not decompiled: p001a.C2576hC.m32488ce(a.akU):boolean");
    }

    @C0064Am(aul = "9949fcd2619dea6e957a1b128d494936", aum = 0)
    /* renamed from: aG */
    private void m32478aG() {
        for (ContractTemplate m : cHP()) {
            mo19180m(m);
        }
        if (cHQ() == null) {
            PlayerDisposedListener bVar = (PlayerDisposedListener) bFf().mo6865M(PlayerDisposedListener.class);
            bVar.mo19183e(this);
            m32476a(bVar);
        }
        super.mo70aH();
    }

    /* renamed from: a.hC$a */
    public enum C2577a {
        TRANSPORT,
        BOUNTY,
        EXTRACTION,
        MISSION
    }

    /* renamed from: a.hC$d */
    /* compiled from: a */
    public enum C2580d {
        TRANSPORT_INCONSISTENT_BAG,
        INCONSISTENT_DATA,
        INSUFFICIENT_MONEY,
        BAD_BOUNTY_PREY
    }

    @C6546aoy
    /* renamed from: a.hC$c */
    /* compiled from: a */
    public static class C2579c extends C5953add {

        private C2580d icZ;

        public C2579c(C2580d dVar) {
            this.icZ = dVar;
        }

        public C2580d dfd() {
            return this.icZ;
        }

        private void writeObject(ObjectOutputStream objectOutputStream) {
            objectOutputStream.writeObject(this.icZ);
        }

        private void readObject(ObjectInputStream objectInputStream) {
            this.icZ = (C2580d) objectInputStream.readObject();
        }
    }

    @C6485anp
    @C5511aMd
    /* renamed from: a.hC$b */
    /* compiled from: a */
    public class PlayerDisposedListener extends aDJ implements C1616Xf, C3161oY.C3162a {
        /* renamed from: _f_onObjectDisposed_0020_0028Ltaikodom_002finfra_002fscript_002fScriptObject_003b_0029V */
        public static final C2491fm f7853x13860637 = null;
        public static final int _m_fieldCount = 0;
        public static final C5663aRz[] _m_fields = null;
        public static final int _m_methodCount = 0;
        public static final C2491fm[] _m_methods = null;
        /* renamed from: aT */
        public static final C5663aRz f7854aT = null;
        public static final long serialVersionUID = 0;
        public static C6494any ___iScriptClass;
        @C0064Am(aul = "7c94755a4666a4d75d5fd4c2f113875a", aum = 0)
        static /* synthetic */ ContractBoard dIG;

        static {
            m32515V();
        }

        public PlayerDisposedListener() {
            super((C5540aNg) null);
            super.mo10S();
        }

        public PlayerDisposedListener(C5540aNg ang) {
            super(ang);
        }

        PlayerDisposedListener(ContractBoard hCVar) {
            super((C5540aNg) null);
            super._m_script_init(hCVar);
        }

        /* renamed from: V */
        static void m32515V() {
            serialVersionUID = (long) 1;
            _m_fieldCount = aDJ._m_fieldCount + 1;
            _m_methodCount = aDJ._m_methodCount + 1;
            int i = aDJ._m_fieldCount;
            C5663aRz[] arzArr = new C5663aRz[(i + 1)];
            C5663aRz b = C5640aRc.m17844b(PlayerDisposedListener.class, "7c94755a4666a4d75d5fd4c2f113875a", i);
            f7854aT = b;
            arzArr[i] = b;
            int i2 = i + 1;
            _m_fields = arzArr;
            C1634Xv.m11725a((Object[]) aDJ._m_fields, (Object[]) _m_fields);
            int i3 = aDJ._m_methodCount;
            C2491fm[] fmVarArr = new C2491fm[(i3 + 1)];
            C2491fm a = C4105zY.m41624a(PlayerDisposedListener.class, "cc2145a275270942e1f5db0f9575fcb4", i3);
            f7853x13860637 = a;
            fmVarArr[i3] = a;
            int i4 = i3 + 1;
            _m_methods = fmVarArr;
            C1634Xv.m11725a((Object[]) aDJ._m_methods, (Object[]) _m_methods);
            ___iScriptClass = aUO.m18566a(PlayerDisposedListener.class, ayI.class, _m_fields, _m_methods);
        }

        @C0064Am(aul = "cc2145a275270942e1f5db0f9575fcb4", aum = 0)
        @C5566aOg
        /* renamed from: b */
        private void m32516b(aDJ adj) {
            throw new aWi(new aCE(this, f7853x13860637, new Object[]{adj}));
        }

        private ContractBoard bkU() {
            return (ContractBoard) bFf().mo5608dq().mo3214p(f7854aT);
        }

        /* renamed from: d */
        private void m32517d(ContractBoard hCVar) {
            bFf().mo5608dq().mo3197f(f7854aT, hCVar);
        }

        /* renamed from: S */
        public void mo10S() {
            super.mo10S();
        }

        /* renamed from: T */
        public C6494any mo11T() {
            return ___iScriptClass;
        }

        /* renamed from: U */
        public C2491fm[] mo12U() {
            return _m_methods;
        }

        /* renamed from: W */
        public Object mo13W() {
            return new ayI(this);
        }

        /* renamed from: a */
        public Object mo14a(C0495Gr gr) {
            Object[] args = gr.getArgs();
            switch (gr.mo2417hq() - aDJ._m_methodCount) {
                case 0:
                    m32516b((aDJ) args[0]);
                    return null;
                default:
                    return super.mo14a(gr);
            }
        }

        /* renamed from: a */
        public void mo15a(Collection collection, C0495Gr gr) {
            super.mo15a(collection, gr);
        }

        @C5566aOg
        /* renamed from: c */
        public void mo98c(aDJ adj) {
            switch (bFf().mo6893i(f7853x13860637)) {
                case 0:
                    return;
                case 2:
                    bFf().mo5606d(new aCE(this, f7853x13860637, new Object[]{adj}));
                    return;
                case 3:
                    bFf().mo5606d(new aCE(this, f7853x13860637, new Object[]{adj}));
                    break;
            }
            m32516b(adj);
        }

        /* renamed from: c */
        public C5663aRz[] mo25c() {
            return _m_fields;
        }

        /* access modifiers changed from: package-private */
        /* renamed from: e */
        public void mo19183e(ContractBoard hCVar) {
            m32517d(hCVar);
            super.mo10S();
        }
    }
}
