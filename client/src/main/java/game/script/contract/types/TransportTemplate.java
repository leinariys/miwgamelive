package game.script.contract.types;

import game.network.message.externalizable.aCE;
import game.script.item.BagItem;
import game.script.mission.Mission;
import game.script.ship.Station;
import logic.baa.C1616Xf;
import logic.baa.C1634Xv;
import logic.baa.C5511aMd;
import logic.baa.C6485anp;
import logic.data.mbean.aKO;
import logic.res.code.C5640aRc;
import logic.res.code.C5663aRz;
import logic.res.code.C6494any;
import logic.res.html.C2491fm;
import p001a.*;
import taikodom.infra.script.I18NString;

import java.util.Collection;

@C5511aMd
@C6485anp
/* renamed from: a.JW */
/* compiled from: a */
public class TransportTemplate extends ContractTemplate implements C1616Xf, C6927awP {

    /* renamed from: MN */
    public static final C2491fm f789MN = null;

    /* renamed from: QE */
    public static final C2491fm f790QE = null;
    public static final int _m_fieldCount = 0;
    public static final C5663aRz[] _m_fields = null;
    public static final int _m_methodCount = 0;
    public static final C2491fm[] _m_methods = null;
    public static final C2491fm cpD = null;
    public static final C2491fm cpK = null;
    public static final C5663aRz dna = null;
    public static final C5663aRz dnb = null;
    public static final C5663aRz dnd = null;
    public static final C5663aRz dnf = null;
    public static final C2491fm dnh = null;
    public static final C2491fm dni = null;
    public static final C2491fm dnj = null;
    public static final C2491fm dnk = null;
    public static final C2491fm dnl = null;
    public static final C2491fm dnm = null;
    public static final C2491fm dnn = null;
    public static final C2491fm dno = null;
    public static final C2491fm dnp = null;
    public static final long serialVersionUID = 0;
    /* renamed from: xF */
    public static final C2491fm f791xF = null;
    public static C6494any ___iScriptClass;
    @C0064Am(aul = "0b183978d3b13f5930367cc6c1092775", aum = 1)
    private static Station ajx;
    @C0064Am(aul = "7cf39c06273adc66e890d1c954796755", aum = 0)
    private static Station ajy;
    @C0064Am(aul = "a7f384ce550f4529a0a880bdaee02e2a", aum = 2)
    private static BagItem dnc;
    @C0064Am(aul = "6adc64975954fb722a303835fff1b00f", aum = 3)
    private static TransportContract<TransportTemplate> dne;
    private static /* synthetic */ int[] dng;

    static {
        m5805V();
    }

    public TransportTemplate() {
        super((C5540aNg) null);
        super.mo10S();
    }

    public TransportTemplate(C5540aNg ang) {
        super(ang);
    }

    /* renamed from: V */
    static void m5805V() {
        serialVersionUID = (long) 1;
        _m_fieldCount = ContractTemplate._m_fieldCount + 4;
        _m_methodCount = ContractTemplate._m_methodCount + 14;
        int i = ContractTemplate._m_fieldCount;
        C5663aRz[] arzArr = new C5663aRz[(i + 4)];
        C5663aRz b = C5640aRc.m17844b(TransportTemplate.class, "7cf39c06273adc66e890d1c954796755", i);
        dna = b;
        arzArr[i] = b;
        int i2 = i + 1;
        C5663aRz b2 = C5640aRc.m17844b(TransportTemplate.class, "0b183978d3b13f5930367cc6c1092775", i2);
        dnb = b2;
        arzArr[i2] = b2;
        int i3 = i2 + 1;
        C5663aRz b3 = C5640aRc.m17844b(TransportTemplate.class, "a7f384ce550f4529a0a880bdaee02e2a", i3);
        dnd = b3;
        arzArr[i3] = b3;
        int i4 = i3 + 1;
        C5663aRz b4 = C5640aRc.m17844b(TransportTemplate.class, "6adc64975954fb722a303835fff1b00f", i4);
        dnf = b4;
        arzArr[i4] = b4;
        int i5 = i4 + 1;
        _m_fields = arzArr;
        C1634Xv.m11725a((Object[]) ContractTemplate._m_fields, (Object[]) _m_fields);
        int i6 = ContractTemplate._m_methodCount;
        C2491fm[] fmVarArr = new C2491fm[(i6 + 14)];
        C2491fm a = C4105zY.m41624a(TransportTemplate.class, "0be2c0f5c1a021ee173b76da7cdf83d8", i6);
        cpK = a;
        fmVarArr[i6] = a;
        int i7 = i6 + 1;
        C2491fm a2 = C4105zY.m41624a(TransportTemplate.class, "ffa326610795c2dab2974492495eef71", i7);
        dnh = a2;
        fmVarArr[i7] = a2;
        int i8 = i7 + 1;
        C2491fm a3 = C4105zY.m41624a(TransportTemplate.class, "95f8b373c76237827a0222307ab1e538", i8);
        dni = a3;
        fmVarArr[i8] = a3;
        int i9 = i8 + 1;
        C2491fm a4 = C4105zY.m41624a(TransportTemplate.class, "3f929abad10c9f231fbfef88725c650b", i9);
        dnj = a4;
        fmVarArr[i9] = a4;
        int i10 = i9 + 1;
        C2491fm a5 = C4105zY.m41624a(TransportTemplate.class, "4255c9b2fbca81b216e9469dfdb00c4e", i10);
        dnk = a5;
        fmVarArr[i10] = a5;
        int i11 = i10 + 1;
        C2491fm a6 = C4105zY.m41624a(TransportTemplate.class, "47bf6ffdbcd816a0ce569899302881ce", i11);
        dnl = a6;
        fmVarArr[i11] = a6;
        int i12 = i11 + 1;
        C2491fm a7 = C4105zY.m41624a(TransportTemplate.class, "e25c1b28f51987ca7e2093018eee3b40", i12);
        dnm = a7;
        fmVarArr[i12] = a7;
        int i13 = i12 + 1;
        C2491fm a8 = C4105zY.m41624a(TransportTemplate.class, "37dcd3df7a1081d69a13a5f68d574240", i13);
        dnn = a8;
        fmVarArr[i13] = a8;
        int i14 = i13 + 1;
        C2491fm a9 = C4105zY.m41624a(TransportTemplate.class, "fedba96a5f328c8bfb3ed740eee885b2", i14);
        f791xF = a9;
        fmVarArr[i14] = a9;
        int i15 = i14 + 1;
        C2491fm a10 = C4105zY.m41624a(TransportTemplate.class, "1ee9024bfc64a3e7b094639027fc81b8", i15);
        cpD = a10;
        fmVarArr[i15] = a10;
        int i16 = i15 + 1;
        C2491fm a11 = C4105zY.m41624a(TransportTemplate.class, "3213b1a773ac0986fb89e942eb1b11ac", i16);
        f789MN = a11;
        fmVarArr[i16] = a11;
        int i17 = i16 + 1;
        C2491fm a12 = C4105zY.m41624a(TransportTemplate.class, "3e3756ce7ef3fde57bccd130cb1af562", i17);
        f790QE = a12;
        fmVarArr[i17] = a12;
        int i18 = i17 + 1;
        C2491fm a13 = C4105zY.m41624a(TransportTemplate.class, "3c78b8d61410afdae82cbbbddfc0ee80", i18);
        dno = a13;
        fmVarArr[i18] = a13;
        int i19 = i18 + 1;
        C2491fm a14 = C4105zY.m41624a(TransportTemplate.class, "9139e15bdf6955cc57b4af852b4d0a92", i19);
        dnp = a14;
        fmVarArr[i19] = a14;
        int i20 = i19 + 1;
        _m_methods = fmVarArr;
        C1634Xv.m11725a((Object[]) ContractTemplate._m_methods, (Object[]) _m_methods);
        ___iScriptClass = aUO.m18566a(TransportTemplate.class, aKO.class, _m_fields, _m_methods);
    }

    static /* synthetic */ int[] baP() {
        int[] iArr = dng;
        if (iArr == null) {
            iArr = new int[ContractTemplate.C0104a.values().length];
            try {
                iArr[ContractTemplate.C0104a.AVAILABLE.ordinal()] = 1;
            } catch (NoSuchFieldError e) {
            }
            try {
                iArr[ContractTemplate.C0104a.DEAD.ordinal()] = 3;
            } catch (NoSuchFieldError e2) {
            }
            try {
                iArr[ContractTemplate.C0104a.TAKEN.ordinal()] = 2;
            } catch (NoSuchFieldError e3) {
            }
            dng = iArr;
        }
        return iArr;
    }

    /* renamed from: C */
    private void m5801C(Station bf) {
        bFf().mo5608dq().mo3197f(dna, bf);
    }

    /* renamed from: D */
    private void m5802D(Station bf) {
        bFf().mo5608dq().mo3197f(dnb, bf);
    }

    @C0064Am(aul = "47bf6ffdbcd816a0ce569899302881ce", aum = 0)
    @C5566aOg
    /* renamed from: E */
    private void m5803E(Station bf) {
        throw new aWi(new aCE(this, dnl, new Object[]{bf}));
    }

    @C0064Am(aul = "37dcd3df7a1081d69a13a5f68d574240", aum = 0)
    @C5566aOg
    /* renamed from: F */
    private void m5804F(Station bf) {
        throw new aWi(new aCE(this, dnn, new Object[]{bf}));
    }

    /* renamed from: a */
    private void m5806a(TransportContract and) {
        bFf().mo5608dq().mo3197f(dnf, and);
    }

    @C0064Am(aul = "1ee9024bfc64a3e7b094639027fc81b8", aum = 0)
    @C5566aOg
    private void aAn() {
        throw new aWi(new aCE(this, cpD, new Object[0]));
    }

    private Station baC() {
        return (Station) bFf().mo5608dq().mo3214p(dna);
    }

    private Station baD() {
        return (Station) bFf().mo5608dq().mo3214p(dnb);
    }

    private BagItem baE() {
        return (BagItem) bFf().mo5608dq().mo3214p(dnd);
    }

    private TransportContract baF() {
        return (TransportContract) bFf().mo5608dq().mo3214p(dnf);
    }

    /* renamed from: c */
    private void m5807c(BagItem at) {
        bFf().mo5608dq().mo3197f(dnd, at);
    }

    @C0064Am(aul = "3f929abad10c9f231fbfef88725c650b", aum = 0)
    @C5566aOg
    /* renamed from: d */
    private void m5808d(BagItem at) {
        throw new aWi(new aCE(this, dnj, new Object[]{at}));
    }

    @C0064Am(aul = "fedba96a5f328c8bfb3ed740eee885b2", aum = 0)
    @C5566aOg
    /* renamed from: iR */
    private Mission m5809iR() {
        throw new aWi(new aCE(this, f791xF, new Object[0]));
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "From")
    /* renamed from: Fl */
    public Station mo3130Fl() {
        switch (bFf().mo6893i(dnk)) {
            case 0:
                return null;
            case 2:
                return (Station) bFf().mo5606d(new aCE(this, dnk, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, dnk, new Object[0]));
                break;
        }
        return baJ();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "To")
    /* renamed from: Fm */
    public Station mo3131Fm() {
        switch (bFf().mo6893i(dnm)) {
            case 0:
                return null;
            case 2:
                return (Station) bFf().mo5606d(new aCE(this, dnm, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, dnm, new Object[0]));
                break;
        }
        return baK();
    }

    /* renamed from: Fo */
    public C1493Vz mo3132Fo() {
        switch (bFf().mo6893i(dni)) {
            case 0:
                return null;
            case 2:
                return (C1493Vz) bFf().mo5606d(new aCE(this, dni, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, dni, new Object[0]));
                break;
        }
        return baI();
    }

    /* renamed from: T */
    public C6494any mo11T() {
        return ___iScriptClass;
    }

    /* renamed from: U */
    public C2491fm[] mo12U() {
        return _m_methods;
    }

    /* renamed from: W */
    public Object mo13W() {
        return new aKO(this);
    }

    /* renamed from: a */
    public Object mo14a(C0495Gr gr) {
        Object[] args = gr.getArgs();
        switch (gr.mo2417hq() - ContractTemplate._m_methodCount) {
            case 0:
                return aAt();
            case 1:
                return baG();
            case 2:
                return baI();
            case 3:
                m5808d((BagItem) args[0]);
                return null;
            case 4:
                return baJ();
            case 5:
                m5803E((Station) args[0]);
                return null;
            case 6:
                return baK();
            case 7:
                m5804F((Station) args[0]);
                return null;
            case 8:
                return m5809iR();
            case 9:
                aAn();
                return null;
            case 10:
                return m5810rO();
            case 11:
                return m5811vV();
            case 12:
                return baL();
            case 13:
                return baN();
            default:
                return super.mo14a(gr);
        }
    }

    /* renamed from: a */
    public void mo15a(Collection collection, C0495Gr gr) {
        super.mo15a(collection, gr);
    }

    @C5566aOg
    public void aAo() {
        switch (bFf().mo6893i(cpD)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, cpD, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, cpD, new Object[0]));
                break;
        }
        aAn();
    }

    public ContractAdapter aAu() {
        switch (bFf().mo6893i(cpK)) {
            case 0:
                return null;
            case 2:
                return (ContractAdapter) bFf().mo5606d(new aCE(this, cpK, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, cpK, new Object[0]));
                break;
        }
        return aAt();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Container")
    public BagItem baH() {
        switch (bFf().mo6893i(dnh)) {
            case 0:
                return null;
            case 2:
                return (BagItem) bFf().mo5606d(new aCE(this, dnh, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, dnh, new Object[0]));
                break;
        }
        return baG();
    }

    public String baM() {
        switch (bFf().mo6893i(dno)) {
            case 0:
                return null;
            case 2:
                return (String) bFf().mo5606d(new aCE(this, dno, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, dno, new Object[0]));
                break;
        }
        return baL();
    }

    public String baO() {
        switch (bFf().mo6893i(dnp)) {
            case 0:
                return null;
            case 2:
                return (String) bFf().mo5606d(new aCE(this, dnp, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, dnp, new Object[0]));
                break;
        }
        return baN();
    }

    /* renamed from: c */
    public C5663aRz[] mo25c() {
        return _m_fields;
    }

    @C5566aOg
    /* renamed from: e */
    public void mo3136e(BagItem at) {
        switch (bFf().mo6893i(dnj)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, dnj, new Object[]{at}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, dnj, new Object[]{at}));
                break;
        }
        m5808d(at);
    }

    @C5566aOg
    /* renamed from: e */
    public void mo3137e(Station bf) {
        switch (bFf().mo6893i(dnl)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, dnl, new Object[]{bf}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, dnl, new Object[]{bf}));
                break;
        }
        m5803E(bf);
    }

    @C5566aOg
    /* renamed from: f */
    public void mo3138f(Station bf) {
        switch (bFf().mo6893i(dnn)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, dnn, new Object[]{bf}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, dnn, new Object[]{bf}));
                break;
        }
        m5804F(bf);
    }

    @C5566aOg
    /* renamed from: iS */
    public Mission mo1716iS() {
        switch (bFf().mo6893i(f791xF)) {
            case 0:
                return null;
            case 2:
                return (Mission) bFf().mo5606d(new aCE(this, f791xF, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f791xF, new Object[0]));
                break;
        }
        return m5809iR();
    }

    /* renamed from: rP */
    public I18NString mo711rP() {
        switch (bFf().mo6893i(f789MN)) {
            case 0:
                return null;
            case 2:
                return (I18NString) bFf().mo5606d(new aCE(this, f789MN, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f789MN, new Object[0]));
                break;
        }
        return m5810rO();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Description")
    /* renamed from: vW */
    public I18NString mo713vW() {
        switch (bFf().mo6893i(f790QE)) {
            case 0:
                return null;
            case 2:
                return (I18NString) bFf().mo5606d(new aCE(this, f790QE, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f790QE, new Object[0]));
                break;
        }
        return m5811vV();
    }

    /* renamed from: S */
    public void mo10S() {
        super.mo10S();
    }

    @C0064Am(aul = "0be2c0f5c1a021ee173b76da7cdf83d8", aum = 0)
    private ContractAdapter aAt() {
        return baF();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Container")
    @C0064Am(aul = "ffa326610795c2dab2974492495eef71", aum = 0)
    private BagItem baG() {
        return baE();
    }

    @C0064Am(aul = "95f8b373c76237827a0222307ab1e538", aum = 0)
    private C1493Vz baI() {
        return baE().aoG();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "From")
    @C0064Am(aul = "4255c9b2fbca81b216e9469dfdb00c4e", aum = 0)
    private Station baJ() {
        return baD();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "To")
    @C0064Am(aul = "e25c1b28f51987ca7e2093018eee3b40", aum = 0)
    private Station baK() {
        return baC();
    }

    @C0064Am(aul = "3213b1a773ac0986fb89e942eb1b11ac", aum = 0)
    /* renamed from: rO */
    private I18NString m5810rO() {
        return new I18NString(String.valueOf(super.mo711rP().get()) + ": " + baC().getName());
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Description")
    @C0064Am(aul = "3e3756ce7ef3fde57bccd130cb1af562", aum = 0)
    /* renamed from: vV */
    private I18NString m5811vV() {
        return new I18NString(aAj().mo22207vW().get().replaceAll("\\{0\\}", aAw().getName()).replaceAll("\\{1\\}", mo3130Fl().getName()).replaceAll("\\{2\\}", mo3131Fm().getName()).replaceAll("\\{3\\}", Float.toString(baH().getVolume())).replaceAll("\\{4\\}", Long.toString(aAA())).replaceAll("\\{5\\}", Long.toString(aAy().aqL())));
    }

    @C0064Am(aul = "3c78b8d61410afdae82cbbbddfc0ee80", aum = 0)
    private String baL() {
        return null;
    }

    @C0064Am(aul = "9139e15bdf6955cc57b4af852b4d0a92", aum = 0)
    private String baN() {
        return null;
    }
}
