package game.script.contract.types;

import game.network.message.externalizable.aCE;
import game.network.message.serializable.C5857abl;
import game.script.contract.ContractReward;
import game.script.player.Player;
import game.script.ship.Station;
import logic.baa.C1616Xf;
import logic.baa.C1634Xv;
import logic.baa.C5511aMd;
import logic.baa.C6485anp;
import logic.data.mbean.C4100zT;
import logic.res.code.C5640aRc;
import logic.res.code.C5663aRz;
import logic.res.code.C6494any;
import logic.res.html.C2491fm;
import p001a.*;
import taikodom.infra.script.I18NString;

import java.util.Collection;
import java.util.Collections;
import java.util.List;

@C5511aMd
@C6485anp
/* renamed from: a.BJ */
/* compiled from: a */
public abstract class ContractTemplate extends TaikodomObject implements C1616Xf, C1647YG, C4045yZ {

    /* renamed from: Cq */
    public static final C2491fm f186Cq = null;

    /* renamed from: Dl */
    public static final C2491fm f187Dl = null;

    /* renamed from: MN */
    public static final C2491fm f188MN = null;
    /* renamed from: QE */
    public static final C2491fm f190QE = null;
    public static final C2491fm _f_dispose_0020_0028_0029V = null;
    public static final int _m_fieldCount = 0;
    public static final C5663aRz[] _m_fields = null;
    public static final int _m_methodCount = 0;
    public static final C2491fm[] _m_methods = null;
    /* renamed from: bP */
    public static final C2491fm f191bP = null;
    public static final C5663aRz bid = null;
    public static final C2491fm cpA = null;
    public static final C2491fm cpB = null;
    public static final C2491fm cpC = null;
    public static final C2491fm cpD = null;
    public static final C2491fm cpE = null;
    public static final C2491fm cpF = null;
    public static final C2491fm cpG = null;
    public static final C2491fm cpH = null;
    public static final C2491fm cpI = null;
    public static final C2491fm cpJ = null;
    public static final C2491fm cpK = null;
    public static final C2491fm cpL = null;
    public static final C2491fm cpM = null;
    public static final C2491fm cpN = null;
    public static final C2491fm cpO = null;
    public static final C2491fm cpP = null;
    public static final C2491fm cpQ = null;
    public static final C2491fm cpR = null;
    public static final C2491fm cpS = null;
    public static final C2491fm cpT = null;
    public static final C2491fm cpU = null;
    public static final C2491fm cpV = null;
    public static final C2491fm cpW = null;
    public static final C2491fm cpX = null;
    public static final C2491fm cpY = null;
    public static final C2491fm cpZ = null;
    public static final C5663aRz cpm = null;
    public static final C5663aRz cpn = null;
    public static final C5663aRz cpo = null;
    public static final C5663aRz cpp = null;
    public static final C5663aRz cpq = null;
    public static final C5663aRz cpr = null;
    public static final C5663aRz cps = null;
    public static final C2491fm cpt = null;
    public static final C2491fm cpu = null;
    public static final C2491fm cpv = null;
    public static final C2491fm cpw = null;
    public static final C2491fm cpx = null;
    public static final C2491fm cpy = null;
    public static final C2491fm cpz = null;
    public static final C2491fm cqa = null;
    public static final C2491fm cqb = null;
    public static final C2491fm cqc = null;
    public static final C2491fm cqd = null;
    /* renamed from: la */
    public static final C5663aRz f192la = null;
    /* renamed from: lo */
    public static final C2491fm f193lo = null;
    public static final long serialVersionUID = 0;
    public static C6494any ___iScriptClass;
    @C0064Am(aul = "6d977181ef0cc11282d050b011fd44b9", aum = 7)

    /* renamed from: Q */
    private static long f189Q;
    @C0064Am(aul = "400fe96dc79c9406848cf021f68c4f5e", aum = 0)
    private static ContractBaseInfo cbY;
    @C0064Am(aul = "d78341e00307e19a768b76c0438d0686", aum = 1)
    private static C0104a cbZ;
    @C0064Am(aul = "5af2bb598c01386cee1d449bafa937e9", aum = 2)
    private static Station cca;
    @C0064Am(aul = "bb32faef6ee9eda4ecf4090e71b75650", aum = 3)
    private static ContractReward ccb;
    @C0064Am(aul = "adde3baeccb855786c6162eaf197599d", aum = 4)
    private static long ccc;
    @C0064Am(aul = "ec4dfc420a31021b3df90f35433c519f", aum = 5)
    private static Player ccd;
    @C0064Am(aul = "6a91467aaec640d7534650310199ef82", aum = 6)
    private static Player cce;
    @C0064Am(aul = "2950f3f97b67a891c2166fa9c93d9a6a", aum = 8)
    private static long ccf;

    static {
        m1077V();
    }

    public ContractTemplate() {
        super((C5540aNg) null);
        super.mo10S();
    }

    public ContractTemplate(C5540aNg ang) {
        super(ang);
    }

    /* renamed from: V */
    static void m1077V() {
        serialVersionUID = (long) 1;
        _m_fieldCount = TaikodomObject._m_fieldCount + 9;
        _m_methodCount = TaikodomObject._m_methodCount + 44;
        int i = TaikodomObject._m_fieldCount;
        C5663aRz[] arzArr = new C5663aRz[(i + 9)];
        C5663aRz b = C5640aRc.m17844b(ContractTemplate.class, "400fe96dc79c9406848cf021f68c4f5e", i);
        cpm = b;
        arzArr[i] = b;
        int i2 = i + 1;
        C5663aRz b2 = C5640aRc.m17844b(ContractTemplate.class, "d78341e00307e19a768b76c0438d0686", i2);
        bid = b2;
        arzArr[i2] = b2;
        int i3 = i2 + 1;
        C5663aRz b3 = C5640aRc.m17844b(ContractTemplate.class, "5af2bb598c01386cee1d449bafa937e9", i3);
        cpn = b3;
        arzArr[i3] = b3;
        int i4 = i3 + 1;
        C5663aRz b4 = C5640aRc.m17844b(ContractTemplate.class, "bb32faef6ee9eda4ecf4090e71b75650", i4);
        cpo = b4;
        arzArr[i4] = b4;
        int i5 = i4 + 1;
        C5663aRz b5 = C5640aRc.m17844b(ContractTemplate.class, "adde3baeccb855786c6162eaf197599d", i5);
        cpp = b5;
        arzArr[i5] = b5;
        int i6 = i5 + 1;
        C5663aRz b6 = C5640aRc.m17844b(ContractTemplate.class, "ec4dfc420a31021b3df90f35433c519f", i6);
        cpq = b6;
        arzArr[i6] = b6;
        int i7 = i6 + 1;
        C5663aRz b7 = C5640aRc.m17844b(ContractTemplate.class, "6a91467aaec640d7534650310199ef82", i7);
        cpr = b7;
        arzArr[i7] = b7;
        int i8 = i7 + 1;
        C5663aRz b8 = C5640aRc.m17844b(ContractTemplate.class, "6d977181ef0cc11282d050b011fd44b9", i8);
        f192la = b8;
        arzArr[i8] = b8;
        int i9 = i8 + 1;
        C5663aRz b9 = C5640aRc.m17844b(ContractTemplate.class, "2950f3f97b67a891c2166fa9c93d9a6a", i9);
        cps = b9;
        arzArr[i9] = b9;
        int i10 = i9 + 1;
        _m_fields = arzArr;
        C1634Xv.m11725a((Object[]) TaikodomObject._m_fields, (Object[]) _m_fields);
        int i11 = TaikodomObject._m_methodCount;
        C2491fm[] fmVarArr = new C2491fm[(i11 + 44)];
        C2491fm a = C4105zY.m41624a(ContractTemplate.class, "7527ec2a9fc02fa9e1ee6dd47e08c5bb", i11);
        cpt = a;
        fmVarArr[i11] = a;
        int i12 = i11 + 1;
        C2491fm a2 = C4105zY.m41624a(ContractTemplate.class, "88c24a737f98fcb0f3f00dc89690a9dc", i12);
        cpu = a2;
        fmVarArr[i12] = a2;
        int i13 = i12 + 1;
        C2491fm a3 = C4105zY.m41624a(ContractTemplate.class, "e481f56a4134aef51265ea683af1d9df", i13);
        cpv = a3;
        fmVarArr[i13] = a3;
        int i14 = i13 + 1;
        C2491fm a4 = C4105zY.m41624a(ContractTemplate.class, "baa6d0c171b1c06938632488dfc7b37f", i14);
        cpw = a4;
        fmVarArr[i14] = a4;
        int i15 = i14 + 1;
        C2491fm a5 = C4105zY.m41624a(ContractTemplate.class, "8886a9dd0193eeb3898b2ef64e22200d", i15);
        cpx = a5;
        fmVarArr[i15] = a5;
        int i16 = i15 + 1;
        C2491fm a6 = C4105zY.m41624a(ContractTemplate.class, "b6f30dcb97e000369f4373904eb0ae5b", i16);
        cpy = a6;
        fmVarArr[i16] = a6;
        int i17 = i16 + 1;
        C2491fm a7 = C4105zY.m41624a(ContractTemplate.class, "7ac514a43a8bead36c05e66d4c4fc78c", i17);
        cpz = a7;
        fmVarArr[i17] = a7;
        int i18 = i17 + 1;
        C2491fm a8 = C4105zY.m41624a(ContractTemplate.class, "526a394210508285a5c85a59dd62f5d0", i18);
        cpA = a8;
        fmVarArr[i18] = a8;
        int i19 = i18 + 1;
        C2491fm a9 = C4105zY.m41624a(ContractTemplate.class, "0e46558407061cc93d60616416de27d2", i19);
        cpB = a9;
        fmVarArr[i19] = a9;
        int i20 = i19 + 1;
        C2491fm a10 = C4105zY.m41624a(ContractTemplate.class, "2aee1f0e6451f0d47725e58289a8a323", i20);
        cpC = a10;
        fmVarArr[i20] = a10;
        int i21 = i20 + 1;
        C2491fm a11 = C4105zY.m41624a(ContractTemplate.class, "25efb4c6125b0fb58128b4604b5b113a", i21);
        cpD = a11;
        fmVarArr[i21] = a11;
        int i22 = i21 + 1;
        C2491fm a12 = C4105zY.m41624a(ContractTemplate.class, "cc852a89be3850e2f4eaad6082e0e186", i22);
        cpE = a12;
        fmVarArr[i22] = a12;
        int i23 = i22 + 1;
        C2491fm a13 = C4105zY.m41624a(ContractTemplate.class, "409f41ddd5d4cf2eba6305868f9c61d1", i23);
        _f_dispose_0020_0028_0029V = a13;
        fmVarArr[i23] = a13;
        int i24 = i23 + 1;
        C2491fm a14 = C4105zY.m41624a(ContractTemplate.class, "3769d74ae7d30fe0880c131dfc48a533", i24);
        cpF = a14;
        fmVarArr[i24] = a14;
        int i25 = i24 + 1;
        C2491fm a15 = C4105zY.m41624a(ContractTemplate.class, "bb735dfa74d7e50d488b716742222d82", i25);
        cpG = a15;
        fmVarArr[i25] = a15;
        int i26 = i25 + 1;
        C2491fm a16 = C4105zY.m41624a(ContractTemplate.class, "b40b6bbdc88c2c57f4afb1557c1b4de2", i26);
        f193lo = a16;
        fmVarArr[i26] = a16;
        int i27 = i26 + 1;
        C2491fm a17 = C4105zY.m41624a(ContractTemplate.class, "d4c990937ac6011e99019ccf52630cb6", i27);
        cpH = a17;
        fmVarArr[i27] = a17;
        int i28 = i27 + 1;
        C2491fm a18 = C4105zY.m41624a(ContractTemplate.class, "0d09f2e05272819d313dba08fb6b5d4d", i28);
        cpI = a18;
        fmVarArr[i28] = a18;
        int i29 = i28 + 1;
        C2491fm a19 = C4105zY.m41624a(ContractTemplate.class, "de837df2c37b4cf7bcc87ff87d5fe3b7", i29);
        cpJ = a19;
        fmVarArr[i29] = a19;
        int i30 = i29 + 1;
        C2491fm a20 = C4105zY.m41624a(ContractTemplate.class, "38189ab027ada32bd105616d9fc020dd", i30);
        cpK = a20;
        fmVarArr[i30] = a20;
        int i31 = i30 + 1;
        C2491fm a21 = C4105zY.m41624a(ContractTemplate.class, "46508f62e6eea4c840d50ddfeaea7bf9", i31);
        cpL = a21;
        fmVarArr[i31] = a21;
        int i32 = i31 + 1;
        C2491fm a22 = C4105zY.m41624a(ContractTemplate.class, "a502dfd284efe3fa66f83ea4196b5ea1", i32);
        cpM = a22;
        fmVarArr[i32] = a22;
        int i33 = i32 + 1;
        C2491fm a23 = C4105zY.m41624a(ContractTemplate.class, "05a961bf43fe1bbd87789bef06e3ce2d", i33);
        cpN = a23;
        fmVarArr[i33] = a23;
        int i34 = i33 + 1;
        C2491fm a24 = C4105zY.m41624a(ContractTemplate.class, "6f1511f3fb6763216eab55e3d3fc68f5", i34);
        cpO = a24;
        fmVarArr[i34] = a24;
        int i35 = i34 + 1;
        C2491fm a25 = C4105zY.m41624a(ContractTemplate.class, "f27d14c437d920c623394f478d0312ab", i35);
        cpP = a25;
        fmVarArr[i35] = a25;
        int i36 = i35 + 1;
        C2491fm a26 = C4105zY.m41624a(ContractTemplate.class, "770a0cb28ab3ed46296906a492f30772", i36);
        cpQ = a26;
        fmVarArr[i36] = a26;
        int i37 = i36 + 1;
        C2491fm a27 = C4105zY.m41624a(ContractTemplate.class, "db92300222e38dd9b3bb622f257a12c2", i37);
        f190QE = a27;
        fmVarArr[i37] = a27;
        int i38 = i37 + 1;
        C2491fm a28 = C4105zY.m41624a(ContractTemplate.class, "219da64b6818a7ba9fa9e8d2fcb9a8ca", i38);
        cpR = a28;
        fmVarArr[i38] = a28;
        int i39 = i38 + 1;
        C2491fm a29 = C4105zY.m41624a(ContractTemplate.class, "2a267a29753b486f205d1a8382b5963a", i39);
        cpS = a29;
        fmVarArr[i39] = a29;
        int i40 = i39 + 1;
        C2491fm a30 = C4105zY.m41624a(ContractTemplate.class, "905a23095cf8ca9e970ed31958d4fecf", i40);
        cpT = a30;
        fmVarArr[i40] = a30;
        int i41 = i40 + 1;
        C2491fm a31 = C4105zY.m41624a(ContractTemplate.class, "ba9c55254c7da909d3cd11bb0ac2e8f3", i41);
        f191bP = a31;
        fmVarArr[i41] = a31;
        int i42 = i41 + 1;
        C2491fm a32 = C4105zY.m41624a(ContractTemplate.class, "4bb253d4cddaac6fe2a172eedd1aa51e", i42);
        cpU = a32;
        fmVarArr[i42] = a32;
        int i43 = i42 + 1;
        C2491fm a33 = C4105zY.m41624a(ContractTemplate.class, "6f7a9dfd6dacdeba5eff544b8bf45cec", i43);
        cpV = a33;
        fmVarArr[i43] = a33;
        int i44 = i43 + 1;
        C2491fm a34 = C4105zY.m41624a(ContractTemplate.class, "a99df59b58f0a0a3f6af7a1c9cef0378", i44);
        cpW = a34;
        fmVarArr[i44] = a34;
        int i45 = i44 + 1;
        C2491fm a35 = C4105zY.m41624a(ContractTemplate.class, "a94df52c8ce905df56bdb194e96ac1cb", i45);
        cpX = a35;
        fmVarArr[i45] = a35;
        int i46 = i45 + 1;
        C2491fm a36 = C4105zY.m41624a(ContractTemplate.class, "95733ab904e822e517ffe2b0261a75c4", i46);
        cpY = a36;
        fmVarArr[i46] = a36;
        int i47 = i46 + 1;
        C2491fm a37 = C4105zY.m41624a(ContractTemplate.class, "ff3c99adaa04452e68524ef51914a31d", i47);
        f188MN = a37;
        fmVarArr[i47] = a37;
        int i48 = i47 + 1;
        C2491fm a38 = C4105zY.m41624a(ContractTemplate.class, "0c9e901be62c7f0a9bac371bf5a4ba4c", i48);
        f187Dl = a38;
        fmVarArr[i48] = a38;
        int i49 = i48 + 1;
        C2491fm a39 = C4105zY.m41624a(ContractTemplate.class, "0503dd7bdf0f584729bbf1f261a24e35", i49);
        cpZ = a39;
        fmVarArr[i49] = a39;
        int i50 = i49 + 1;
        C2491fm a40 = C4105zY.m41624a(ContractTemplate.class, "ac54d8d18ff0c8ed7273a18c3186fdb3", i50);
        cqa = a40;
        fmVarArr[i50] = a40;
        int i51 = i50 + 1;
        C2491fm a41 = C4105zY.m41624a(ContractTemplate.class, "b5d9db0932f23bc9c072bdb6723d81fd", i51);
        cqb = a41;
        fmVarArr[i51] = a41;
        int i52 = i51 + 1;
        C2491fm a42 = C4105zY.m41624a(ContractTemplate.class, "8b488218a786f7bf76720cf482d5aa5d", i52);
        f186Cq = a42;
        fmVarArr[i52] = a42;
        int i53 = i52 + 1;
        C2491fm a43 = C4105zY.m41624a(ContractTemplate.class, "e599828ca35ca460728c41ee1d357271", i53);
        cqc = a43;
        fmVarArr[i53] = a43;
        int i54 = i53 + 1;
        C2491fm a44 = C4105zY.m41624a(ContractTemplate.class, "e789b5e317f2a725a7c0f56a0a4245f7", i54);
        cqd = a44;
        fmVarArr[i54] = a44;
        int i55 = i54 + 1;
        _m_methods = fmVarArr;
        C1634Xv.m11725a((Object[]) TaikodomObject._m_methods, (Object[]) _m_methods);
        ___iScriptClass = aUO.m18566a(ContractTemplate.class, C4100zT.class, _m_fields, _m_methods);
    }

    /* renamed from: a */
    private void m1078a(C0104a aVar) {
        bFf().mo5608dq().mo3197f(bid, aVar);
    }

    /* renamed from: a */
    private void m1079a(ContractReward os) {
        bFf().mo5608dq().mo3197f(cpo, os);
    }

    /* renamed from: a */
    private void m1081a(ContractBaseInfo tEVar) {
        bFf().mo5608dq().mo3197f(cpm, tEVar);
    }

    @C5566aOg
    /* renamed from: aA */
    private void m1082aA(Player aku) {
        switch (bFf().mo6893i(cpC)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, cpC, new Object[]{aku}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, cpC, new Object[]{aku}));
                break;
        }
        m1090az(aku);
    }

    @C0064Am(aul = "0503dd7bdf0f584729bbf1f261a24e35", aum = 0)
    @C5566aOg
    private void aAN() {
        throw new aWi(new aCE(this, cpZ, new Object[0]));
    }

    @C0064Am(aul = "e789b5e317f2a725a7c0f56a0a4245f7", aum = 0)
    private C5857abl aAS() {
        return aAy();
    }

    private ContractBaseInfo aAa() {
        return (ContractBaseInfo) bFf().mo5608dq().mo3214p(cpm);
    }

    private C0104a aAb() {
        return (C0104a) bFf().mo5608dq().mo3214p(bid);
    }

    private Station aAc() {
        return (Station) bFf().mo5608dq().mo3214p(cpn);
    }

    private ContractReward aAd() {
        return (ContractReward) bFf().mo5608dq().mo3214p(cpo);
    }

    private long aAe() {
        return bFf().mo5608dq().mo3213o(cpp);
    }

    private Player aAf() {
        return (Player) bFf().mo5608dq().mo3214p(cpq);
    }

    private Player aAg() {
        return (Player) bFf().mo5608dq().mo3214p(cpr);
    }

    private long aAh() {
        return bFf().mo5608dq().mo3213o(cps);
    }

    @C0064Am(aul = "0e46558407061cc93d60616416de27d2", aum = 0)
    @C5566aOg
    private void aAl() {
        throw new aWi(new aCE(this, cpB, new Object[0]));
    }

    @C0064Am(aul = "25efb4c6125b0fb58128b4604b5b113a", aum = 0)
    @C5566aOg
    private void aAn() {
        throw new aWi(new aCE(this, cpD, new Object[0]));
    }

    @C0064Am(aul = "38189ab027ada32bd105616d9fc020dd", aum = 0)
    private ContractAdapter aAt() {
        throw new aWi(new aCE(this, cpK, new Object[0]));
    }

    @C0064Am(aul = "cc852a89be3850e2f4eaad6082e0e186", aum = 0)
    @C5566aOg
    /* renamed from: aB */
    private boolean m1083aB(Player aku) {
        throw new aWi(new aCE(this, cpE, new Object[]{aku}));
    }

    /* renamed from: at */
    private void m1085at(Player aku) {
        bFf().mo5608dq().mo3197f(cpq, aku);
    }

    /* renamed from: au */
    private void m1086au(Player aku) {
        bFf().mo5608dq().mo3197f(cpr, aku);
    }

    @C0064Am(aul = "7ac514a43a8bead36c05e66d4c4fc78c", aum = 0)
    @C5566aOg
    /* renamed from: av */
    private void m1087av(Player aku) {
        throw new aWi(new aCE(this, cpz, new Object[]{aku}));
    }

    @C0064Am(aul = "526a394210508285a5c85a59dd62f5d0", aum = 0)
    @C5566aOg
    /* renamed from: ax */
    private void m1088ax(Player aku) {
        throw new aWi(new aCE(this, cpA, new Object[]{aku}));
    }

    @C5566aOg
    /* renamed from: ay */
    private void m1089ay(Player aku) {
        switch (bFf().mo6893i(cpA)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, cpA, new Object[]{aku}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, cpA, new Object[]{aku}));
                break;
        }
        m1088ax(aku);
    }

    @C0064Am(aul = "2aee1f0e6451f0d47725e58289a8a323", aum = 0)
    @C5566aOg
    /* renamed from: az */
    private void m1090az(Player aku) {
        throw new aWi(new aCE(this, cpC, new Object[]{aku}));
    }

    @C0064Am(aul = "de837df2c37b4cf7bcc87ff87d5fe3b7", aum = 0)
    @C5566aOg
    /* renamed from: b */
    private void m1091b(C0104a aVar) {
        throw new aWi(new aCE(this, cpJ, new Object[]{aVar}));
    }

    /* renamed from: eH */
    private long m1093eH() {
        return bFf().mo5608dq().mo3213o(f192la);
    }

    /* renamed from: ec */
    private void m1095ec(long j) {
        bFf().mo5608dq().mo3184b(cpp, j);
    }

    /* renamed from: ed */
    private void m1096ed(long j) {
        bFf().mo5608dq().mo3184b(cps, j);
    }

    @C0064Am(aul = "d4c990937ac6011e99019ccf52630cb6", aum = 0)
    @C5566aOg
    /* renamed from: eh */
    private void m1099eh(long j) {
        throw new aWi(new aCE(this, cpH, new Object[]{new Long(j)}));
    }

    @C0064Am(aul = "770a0cb28ab3ed46296906a492f30772", aum = 0)
    @C5566aOg
    /* renamed from: ej */
    private void m1100ej(long j) {
        throw new aWi(new aCE(this, cpQ, new Object[]{new Long(j)}));
    }

    /* renamed from: m */
    private void m1104m(long j) {
        bFf().mo5608dq().mo3184b(f192la, j);
    }

    /* renamed from: w */
    private void m1107w(Station bf) {
        bFf().mo5608dq().mo3197f(cpn, bf);
    }

    @C0064Am(aul = "bb735dfa74d7e50d488b716742222d82", aum = 0)
    @C5566aOg
    /* renamed from: x */
    private void m1108x(Station bf) {
        throw new aWi(new aCE(this, cpG, new Object[]{bf}));
    }

    /* renamed from: T */
    public C6494any mo11T() {
        return ___iScriptClass;
    }

    /* renamed from: U */
    public C2491fm[] mo12U() {
        return _m_methods;
    }

    /* renamed from: W */
    public Object mo13W() {
        return new C4100zT(this);
    }

    /* renamed from: a */
    public Object mo14a(C0495Gr gr) {
        Object[] args = gr.getArgs();
        switch (gr.mo2417hq() - TaikodomObject._m_methodCount) {
            case 0:
                return aAi();
            case 1:
                m1092b((ContractBaseInfo) args[0]);
                return null;
            case 2:
                m1097ee(((Long) args[0]).longValue());
                return null;
            case 3:
                m1098ef(((Long) args[0]).longValue());
                return null;
            case 4:
                m1080a((C5857abl) args[0]);
                return null;
            case 5:
                return new Boolean(aAk());
            case 6:
                m1087av((Player) args[0]);
                return null;
            case 7:
                m1088ax((Player) args[0]);
                return null;
            case 8:
                aAl();
                return null;
            case 9:
                m1090az((Player) args[0]);
                return null;
            case 10:
                aAn();
                return null;
            case 11:
                return new Boolean(m1083aB((Player) args[0]));
            case 12:
                m1101fg();
                return null;
            case 13:
                return aAp();
            case 14:
                m1108x((Station) args[0]);
                return null;
            case 15:
                return new Long(m1094eW());
            case 16:
                m1099eh(((Long) args[0]).longValue());
                return null;
            case 17:
                return aAr();
            case 18:
                m1091b((C0104a) args[0]);
                return null;
            case 19:
                return aAt();
            case 20:
                return aAv();
            case 21:
                return aAx();
            case 22:
                return new Long(aAz());
            case 23:
                return aAB();
            case 24:
                return new Long(aAD());
            case 25:
                m1100ej(((Long) args[0]).longValue());
                return null;
            case 26:
                return m1106vV();
            case 27:
                return new Float(aAF());
            case 28:
                return aAG();
            case 29:
                return aAH();
            case 30:
                return m1084ar();
            case 31:
                return aAI();
            case 32:
                return aAJ();
            case 33:
                return new Long(aAK());
            case 34:
                return aAL();
            case 35:
                return aAM();
            case 36:
                return m1105rO();
            case 37:
                return new Boolean(m1102lY());
            case 38:
                aAN();
                return null;
            case 39:
                return aAP();
            case 40:
                return new Integer(aAQ());
            case 41:
                return new Integer(m1103ls());
            case 42:
                return new Boolean(aAR());
            case 43:
                return aAS();
            default:
                return super.mo14a(gr);
        }
    }

    /* renamed from: a */
    public void mo15a(Collection collection, C0495Gr gr) {
        super.mo15a(collection, gr);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "SafeGuard")
    public long aAA() {
        switch (bFf().mo6893i(cpN)) {
            case 0:
                return 0;
            case 2:
                return ((Long) bFf().mo5606d(new aCE(this, cpN, new Object[0]))).longValue();
            case 3:
                bFf().mo5606d(new aCE(this, cpN, new Object[0]));
                break;
        }
        return aAz();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Taker")
    public Player aAC() {
        switch (bFf().mo6893i(cpO)) {
            case 0:
                return null;
            case 2:
                return (Player) bFf().mo5606d(new aCE(this, cpO, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, cpO, new Object[0]));
                break;
        }
        return aAB();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "TimeToLive")
    public long aAE() {
        switch (bFf().mo6893i(cpP)) {
            case 0:
                return 0;
            case 2:
                return ((Long) bFf().mo5606d(new aCE(this, cpP, new Object[0]))).longValue();
            case 3:
                bFf().mo5606d(new aCE(this, cpP, new Object[0]));
                break;
        }
        return aAD();
    }

    @C5566aOg
    public void aAO() {
        switch (bFf().mo6893i(cpZ)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, cpZ, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, cpZ, new Object[0]));
                break;
        }
        aAN();
    }

    public /* bridge */ /* synthetic */ C5857abl aAT() {
        switch (bFf().mo6893i(cqd)) {
            case 0:
                return null;
            case 2:
                return (C5857abl) bFf().mo5606d(new aCE(this, cqd, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, cqd, new Object[0]));
                break;
        }
        return aAS();
    }

    public ContractBaseInfo aAj() {
        switch (bFf().mo6893i(cpt)) {
            case 0:
                return null;
            case 2:
                return (ContractBaseInfo) bFf().mo5606d(new aCE(this, cpt, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, cpt, new Object[0]));
                break;
        }
        return aAi();
    }

    @C5566aOg
    public void aAm() {
        switch (bFf().mo6893i(cpB)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, cpB, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, cpB, new Object[0]));
                break;
        }
        aAl();
    }

    @C5566aOg
    public void aAo() {
        switch (bFf().mo6893i(cpD)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, cpD, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, cpD, new Object[0]));
                break;
        }
        aAn();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "BoardLocation")
    public Station aAq() {
        switch (bFf().mo6893i(cpF)) {
            case 0:
                return null;
            case 2:
                return (Station) bFf().mo5606d(new aCE(this, cpF, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, cpF, new Object[0]));
                break;
        }
        return aAp();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "State")
    public C0104a aAs() {
        switch (bFf().mo6893i(cpI)) {
            case 0:
                return null;
            case 2:
                return (C0104a) bFf().mo5606d(new aCE(this, cpI, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, cpI, new Object[0]));
                break;
        }
        return aAr();
    }

    public ContractAdapter aAu() {
        switch (bFf().mo6893i(cpK)) {
            case 0:
                return null;
            case 2:
                return (ContractAdapter) bFf().mo5606d(new aCE(this, cpK, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, cpK, new Object[0]));
                break;
        }
        return aAt();
    }

    public Player aAw() {
        switch (bFf().mo6893i(cpL)) {
            case 0:
                return null;
            case 2:
                return (Player) bFf().mo5606d(new aCE(this, cpL, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, cpL, new Object[0]));
                break;
        }
        return aAv();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Reward")
    public final ContractReward aAy() {
        switch (bFf().mo6893i(cpM)) {
            case 0:
                return null;
            case 2:
                return (ContractReward) bFf().mo5606d(new aCE(this, cpM, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, cpM, new Object[0]));
                break;
        }
        return aAx();
    }

    @C5566aOg
    /* renamed from: aC */
    public boolean mo687aC(Player aku) {
        switch (bFf().mo6893i(cpE)) {
            case 0:
                return false;
            case 2:
                return ((Boolean) bFf().mo5606d(new aCE(this, cpE, new Object[]{aku}))).booleanValue();
            case 3:
                bFf().mo5606d(new aCE(this, cpE, new Object[]{aku}));
                break;
        }
        return m1083aB(aku);
    }

    public I18NString aqH() {
        switch (bFf().mo6893i(cpY)) {
            case 0:
                return null;
            case 2:
                return (I18NString) bFf().mo5606d(new aCE(this, cpY, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, cpY, new Object[0]));
                break;
        }
        return aAM();
    }

    public I18NString aqI() {
        switch (bFf().mo6893i(cpX)) {
            case 0:
                return null;
            case 2:
                return (I18NString) bFf().mo5606d(new aCE(this, cpX, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, cpX, new Object[0]));
                break;
        }
        return aAL();
    }

    public boolean aqJ() {
        switch (bFf().mo6893i(cpy)) {
            case 0:
                return false;
            case 2:
                return ((Boolean) bFf().mo5606d(new aCE(this, cpy, new Object[0]))).booleanValue();
            case 3:
                bFf().mo5606d(new aCE(this, cpy, new Object[0]));
                break;
        }
        return aAk();
    }

    public float aqK() {
        switch (bFf().mo6893i(cpR)) {
            case 0:
                return 0.0f;
            case 2:
                return ((Float) bFf().mo5606d(new aCE(this, cpR, new Object[0]))).floatValue();
            case 3:
                bFf().mo5606d(new aCE(this, cpR, new Object[0]));
                break;
        }
        return aAF();
    }

    public long aqL() {
        switch (bFf().mo6893i(cpW)) {
            case 0:
                return 0;
            case 2:
                return ((Long) bFf().mo5606d(new aCE(this, cpW, new Object[0]))).longValue();
            case 3:
                bFf().mo5606d(new aCE(this, cpW, new Object[0]));
                break;
        }
        return aAK();
    }

    public List<ItemTypeTableItem> aqM() {
        switch (bFf().mo6893i(cpU)) {
            case 0:
                return null;
            case 2:
                return (List) bFf().mo5606d(new aCE(this, cpU, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, cpU, new Object[0]));
                break;
        }
        return aAI();
    }

    public List<ItemTypeTableItem> aqN() {
        switch (bFf().mo6893i(cpV)) {
            case 0:
                return null;
            case 2:
                return (List) bFf().mo5606d(new aCE(this, cpV, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, cpV, new Object[0]));
                break;
        }
        return aAJ();
    }

    public String aqO() {
        switch (bFf().mo6893i(cpT)) {
            case 0:
                return null;
            case 2:
                return (String) bFf().mo5606d(new aCE(this, cpT, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, cpT, new Object[0]));
                break;
        }
        return aAH();
    }

    public String aqP() {
        switch (bFf().mo6893i(cpS)) {
            case 0:
                return null;
            case 2:
                return (String) bFf().mo5606d(new aCE(this, cpS, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, cpS, new Object[0]));
                break;
        }
        return aAG();
    }

    public C4045yZ.C4046a aqQ() {
        switch (bFf().mo6893i(cqa)) {
            case 0:
                return null;
            case 2:
                return (C4045yZ.C4046a) bFf().mo5606d(new aCE(this, cqa, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, cqa, new Object[0]));
                break;
        }
        return aAP();
    }

    public int aqR() {
        switch (bFf().mo6893i(cqb)) {
            case 0:
                return 0;
            case 2:
                return ((Integer) bFf().mo5606d(new aCE(this, cqb, new Object[0]))).intValue();
            case 3:
                bFf().mo5606d(new aCE(this, cqb, new Object[0]));
                break;
        }
        return aAQ();
    }

    public boolean aqS() {
        switch (bFf().mo6893i(cqc)) {
            case 0:
                return false;
            case 2:
                return ((Boolean) bFf().mo5606d(new aCE(this, cqc, new Object[0]))).booleanValue();
            case 3:
                bFf().mo5606d(new aCE(this, cqc, new Object[0]));
                break;
        }
        return aAR();
    }

    @C5566aOg
    /* renamed from: aw */
    public void mo700aw(Player aku) {
        switch (bFf().mo6893i(cpz)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, cpz, new Object[]{aku}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, cpz, new Object[]{aku}));
                break;
        }
        m1087av(aku);
    }

    /* renamed from: b */
    public final void mo701b(C5857abl abl) {
        switch (bFf().mo6893i(cpx)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, cpx, new Object[]{abl}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, cpx, new Object[]{abl}));
                break;
        }
        m1080a(abl);
    }

    @C5566aOg
    /* renamed from: c */
    public void mo702c(C0104a aVar) {
        switch (bFf().mo6893i(cpJ)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, cpJ, new Object[]{aVar}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, cpJ, new Object[]{aVar}));
                break;
        }
        m1091b(aVar);
    }

    /* renamed from: c */
    public void mo703c(ContractBaseInfo tEVar) {
        switch (bFf().mo6893i(cpu)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, cpu, new Object[]{tEVar}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, cpu, new Object[]{tEVar}));
                break;
        }
        m1092b(tEVar);
    }

    /* renamed from: c */
    public C5663aRz[] mo25c() {
        return _m_fields;
    }

    public void dispose() {
        switch (bFf().mo6893i(_f_dispose_0020_0028_0029V)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, _f_dispose_0020_0028_0029V, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, _f_dispose_0020_0028_0029V, new Object[0]));
                break;
        }
        m1101fg();
    }

    /* renamed from: eg */
    public void mo704eg(long j) {
        switch (bFf().mo6893i(cpw)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, cpw, new Object[]{new Long(j)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, cpw, new Object[]{new Long(j)}));
                break;
        }
        m1098ef(j);
    }

    @C5566aOg
    /* renamed from: ei */
    public void mo705ei(long j) {
        switch (bFf().mo6893i(cpH)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, cpH, new Object[]{new Long(j)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, cpH, new Object[]{new Long(j)}));
                break;
        }
        m1099eh(j);
    }

    @C5566aOg
    /* renamed from: ek */
    public void mo706ek(long j) {
        switch (bFf().mo6893i(cpQ)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, cpQ, new Object[]{new Long(j)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, cpQ, new Object[]{new Long(j)}));
                break;
        }
        m1100ej(j);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "CreationTime")
    public long getCreationTime() {
        switch (bFf().mo6893i(f193lo)) {
            case 0:
                return 0;
            case 2:
                return ((Long) bFf().mo5606d(new aCE(this, f193lo, new Object[0]))).longValue();
            case 3:
                bFf().mo5606d(new aCE(this, f193lo, new Object[0]));
                break;
        }
        return m1094eW();
    }

    public String getHandle() {
        switch (bFf().mo6893i(f191bP)) {
            case 0:
                return null;
            case 2:
                return (String) bFf().mo5606d(new aCE(this, f191bP, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f191bP, new Object[0]));
                break;
        }
        return m1084ar();
    }

    /* renamed from: lZ */
    public boolean mo709lZ() {
        switch (bFf().mo6893i(f187Dl)) {
            case 0:
                return false;
            case 2:
                return ((Boolean) bFf().mo5606d(new aCE(this, f187Dl, new Object[0]))).booleanValue();
            case 3:
                bFf().mo5606d(new aCE(this, f187Dl, new Object[0]));
                break;
        }
        return m1102lY();
    }

    /* renamed from: lt */
    public int mo710lt() {
        switch (bFf().mo6893i(f186Cq)) {
            case 0:
                return 0;
            case 2:
                return ((Integer) bFf().mo5606d(new aCE(this, f186Cq, new Object[0]))).intValue();
            case 3:
                bFf().mo5606d(new aCE(this, f186Cq, new Object[0]));
                break;
        }
        return m1103ls();
    }

    /* renamed from: rP */
    public I18NString mo711rP() {
        switch (bFf().mo6893i(f188MN)) {
            case 0:
                return null;
            case 2:
                return (I18NString) bFf().mo5606d(new aCE(this, f188MN, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f188MN, new Object[0]));
                break;
        }
        return m1105rO();
    }

    public void setTimeToLive(long j) {
        switch (bFf().mo6893i(cpv)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, cpv, new Object[]{new Long(j)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, cpv, new Object[]{new Long(j)}));
                break;
        }
        m1097ee(j);
    }

    /* renamed from: vW */
    public I18NString mo713vW() {
        switch (bFf().mo6893i(f190QE)) {
            case 0:
                return null;
            case 2:
                return (I18NString) bFf().mo5606d(new aCE(this, f190QE, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f190QE, new Object[0]));
                break;
        }
        return m1106vV();
    }

    @C5566aOg
    /* renamed from: y */
    public void mo714y(Station bf) {
        switch (bFf().mo6893i(cpG)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, cpG, new Object[]{bf}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, cpG, new Object[]{bf}));
                break;
        }
        m1108x(bf);
    }

    /* renamed from: S */
    public void mo10S() {
        super.mo10S();
    }

    @C0064Am(aul = "7527ec2a9fc02fa9e1ee6dd47e08c5bb", aum = 0)
    private ContractBaseInfo aAi() {
        return aAa();
    }

    @C0064Am(aul = "88c24a737f98fcb0f3f00dc89690a9dc", aum = 0)
    /* renamed from: b */
    private void m1092b(ContractBaseInfo tEVar) {
        m1081a(tEVar);
    }

    @C0064Am(aul = "e481f56a4134aef51265ea683af1d9df", aum = 0)
    /* renamed from: ee */
    private void m1097ee(long j) {
        m1096ed(j);
    }

    @C0064Am(aul = "baa6d0c171b1c06938632488dfc7b37f", aum = 0)
    /* renamed from: ef */
    private void m1098ef(long j) {
        m1095ec(j);
    }

    @C0064Am(aul = "8886a9dd0193eeb3898b2ef64e22200d", aum = 0)
    /* renamed from: a */
    private void m1080a(C5857abl abl) {
        if (abl instanceof ContractReward) {
            m1079a((ContractReward) abl);
        }
    }

    @C0064Am(aul = "b6f30dcb97e000369f4373904eb0ae5b", aum = 0)
    private boolean aAk() {
        return false;
    }

    @C0064Am(aul = "409f41ddd5d4cf2eba6305868f9c61d1", aum = 0)
    /* renamed from: fg */
    private void m1101fg() {
        aAd().dispose();
        super.dispose();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "BoardLocation")
    @C0064Am(aul = "3769d74ae7d30fe0880c131dfc48a533", aum = 0)
    private Station aAp() {
        return aAc();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "CreationTime")
    @C0064Am(aul = "b40b6bbdc88c2c57f4afb1557c1b4de2", aum = 0)
    /* renamed from: eW */
    private long m1094eW() {
        return m1093eH();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "State")
    @C0064Am(aul = "0d09f2e05272819d313dba08fb6b5d4d", aum = 0)
    private C0104a aAr() {
        return aAb();
    }

    @C0064Am(aul = "46508f62e6eea4c840d50ddfeaea7bf9", aum = 0)
    private Player aAv() {
        return aAf();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Reward")
    @C0064Am(aul = "a502dfd284efe3fa66f83ea4196b5ea1", aum = 0)
    private ContractReward aAx() {
        return aAd();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "SafeGuard")
    @C0064Am(aul = "05a961bf43fe1bbd87789bef06e3ce2d", aum = 0)
    private long aAz() {
        return aAe();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Taker")
    @C0064Am(aul = "6f1511f3fb6763216eab55e3d3fc68f5", aum = 0)
    private Player aAB() {
        return aAg();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "TimeToLive")
    @C0064Am(aul = "f27d14c437d920c623394f478d0312ab", aum = 0)
    private long aAD() {
        return aAh();
    }

    @C0064Am(aul = "db92300222e38dd9b3bb622f257a12c2", aum = 0)
    /* renamed from: vV */
    private I18NString m1106vV() {
        return aAa().mo22207vW();
    }

    @C0064Am(aul = "219da64b6818a7ba9fa9e8d2fcb9a8ca", aum = 0)
    private float aAF() {
        return 0.0f;
    }

    @C0064Am(aul = "2a267a29753b486f205d1a8382b5963a", aum = 0)
    private String aAG() {
        return aAj().getHandle();
    }

    @C0064Am(aul = "905a23095cf8ca9e970ed31958d4fecf", aum = 0)
    private String aAH() {
        return aAw().getName();
    }

    @C0064Am(aul = "ba9c55254c7da909d3cd11bb0ac2e8f3", aum = 0)
    /* renamed from: ar */
    private String m1084ar() {
        return aAa().getHandle();
    }

    @C0064Am(aul = "4bb253d4cddaac6fe2a172eedd1aa51e", aum = 0)
    private List<ItemTypeTableItem> aAI() {
        return Collections.EMPTY_LIST;
    }

    @C0064Am(aul = "6f7a9dfd6dacdeba5eff544b8bf45cec", aum = 0)
    private List<ItemTypeTableItem> aAJ() {
        return Collections.EMPTY_LIST;
    }

    @C0064Am(aul = "a99df59b58f0a0a3f6af7a1c9cef0378", aum = 0)
    private long aAK() {
        return 0;
    }

    @C0064Am(aul = "a94df52c8ce905df56bdb194e96ac1cb", aum = 0)
    private I18NString aAL() {
        return null;
    }

    @C0064Am(aul = "95733ab904e822e517ffe2b0261a75c4", aum = 0)
    private I18NString aAM() {
        return null;
    }

    @C0064Am(aul = "ff3c99adaa04452e68524ef51914a31d", aum = 0)
    /* renamed from: rO */
    private I18NString m1105rO() {
        return aAa().mo22205rP();
    }

    @C0064Am(aul = "0c9e901be62c7f0a9bac371bf5a4ba4c", aum = 0)
    /* renamed from: lY */
    private boolean m1102lY() {
        return false;
    }

    @C0064Am(aul = "ac54d8d18ff0c8ed7273a18c3186fdb3", aum = 0)
    private C4045yZ.C4046a aAP() {
        return null;
    }

    @C0064Am(aul = "b5d9db0932f23bc9c072bdb6723d81fd", aum = 0)
    private int aAQ() {
        return 0;
    }

    @C0064Am(aul = "8b488218a786f7bf76720cf482d5aa5d", aum = 0)
    /* renamed from: ls */
    private int m1103ls() {
        return 0;
    }

    @C0064Am(aul = "e599828ca35ca460728c41ee1d357271", aum = 0)
    private boolean aAR() {
        return false;
    }

    /* renamed from: a.BJ$a */
    public enum C0104a {
        AVAILABLE,
        TAKEN,
        DEAD
    }
}
