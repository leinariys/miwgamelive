package game.script.contract.types;

import game.network.message.externalizable.aCE;
import game.script.Character;
import game.script.item.ItemType;
import game.script.mission.Mission;
import game.script.mission.MissionTrigger;
import game.script.ship.Ship;
import game.script.ship.Station;
import game.script.space.Gate;
import game.script.space.Loot;
import game.script.space.LootType;
import game.script.space.Node;
import logic.baa.C1616Xf;
import logic.baa.C1634Xv;
import logic.baa.C5511aMd;
import logic.baa.C6485anp;
import logic.data.mbean.C5978aeC;
import logic.res.code.C5663aRz;
import logic.res.code.C6494any;
import logic.res.html.C2491fm;
import logic.sql.C5878acG;
import logic.sql.aMH;
import p001a.*;

import java.util.Collection;

@C5511aMd
@C6485anp
/* renamed from: a.vf */
/* compiled from: a */
public abstract class ContractAdapter<T extends ContractTemplate> extends Mission<T> implements C1616Xf {
    public static final int _m_fieldCount = 0;
    public static final C5663aRz[] _m_fields = null;
    public static final int _m_methodCount = 0;
    public static final C2491fm[] _m_methods = null;
    public static final C2491fm aLR = null;
    public static final C2491fm aLS = null;
    public static final C2491fm aLT = null;
    public static final C2491fm aLU = null;
    public static final C2491fm aMe = null;
    public static final C2491fm beF = null;
    public static final C2491fm beG = null;
    public static final C2491fm beH = null;
    public static final C2491fm byA = null;
    public static final C2491fm byB = null;
    public static final C2491fm byC = null;
    public static final C2491fm byD = null;
    public static final C2491fm byE = null;
    public static final C2491fm byF = null;
    public static final C2491fm byG = null;
    public static final C2491fm byH = null;
    public static final C2491fm byI = null;
    public static final C2491fm byJ = null;
    public static final C2491fm byK = null;
    public static final C2491fm byL = null;
    public static final C2491fm byM = null;
    public static final C2491fm byN = null;
    public static final C2491fm byO = null;
    public static final C2491fm byP = null;
    public static final C2491fm byQ = null;
    public static final C2491fm byR = null;
    public static final C2491fm byS = null;
    public static final C2491fm byz = null;
    private static final long serialVersionUID = 2541273569738879888L;
    public static C6494any ___iScriptClass = null;

    static {
        m40357V();
    }

    public ContractAdapter() {
        super((C5540aNg) null);
        super.mo10S();
    }

    public ContractAdapter(C5540aNg ang) {
        super(ang);
    }

    /* renamed from: V */
    static void m40357V() {
        _m_fieldCount = Mission._m_fieldCount + 0;
        _m_methodCount = Mission._m_methodCount + 28;
        _m_fields = new C5663aRz[(Mission._m_fieldCount + 0)];
        C1634Xv.m11725a((Object[]) Mission._m_fields, (Object[]) _m_fields);
        int i = Mission._m_methodCount;
        C2491fm[] fmVarArr = new C2491fm[(i + 28)];
        C2491fm a = C4105zY.m41624a(ContractAdapter.class, "c3926ff5f1690f9335f4d61848ebdf10", i);
        byz = a;
        fmVarArr[i] = a;
        int i2 = i + 1;
        C2491fm a2 = C4105zY.m41624a(ContractAdapter.class, "66c41b8f90f6bd0705eacb3a184dd606", i2);
        byA = a2;
        fmVarArr[i2] = a2;
        int i3 = i2 + 1;
        C2491fm a3 = C4105zY.m41624a(ContractAdapter.class, "c447a9b887639c938f54eb374337f99b", i3);
        beG = a3;
        fmVarArr[i3] = a3;
        int i4 = i3 + 1;
        C2491fm a4 = C4105zY.m41624a(ContractAdapter.class, "48c912a1d48d4b046872b87091bac2b6", i4);
        byB = a4;
        fmVarArr[i4] = a4;
        int i5 = i4 + 1;
        C2491fm a5 = C4105zY.m41624a(ContractAdapter.class, "4c914a8381814d19f3d0cdb7564e3235", i5);
        byC = a5;
        fmVarArr[i5] = a5;
        int i6 = i5 + 1;
        C2491fm a6 = C4105zY.m41624a(ContractAdapter.class, "576e56ecd56e810e93bfec060978b9a7", i6);
        byD = a6;
        fmVarArr[i6] = a6;
        int i7 = i6 + 1;
        C2491fm a7 = C4105zY.m41624a(ContractAdapter.class, "156e7efe1be14253b25026c378c33edb", i7);
        byE = a7;
        fmVarArr[i7] = a7;
        int i8 = i7 + 1;
        C2491fm a8 = C4105zY.m41624a(ContractAdapter.class, "a4d610addd13066f4e1951040a03df59", i8);
        byF = a8;
        fmVarArr[i8] = a8;
        int i9 = i8 + 1;
        C2491fm a9 = C4105zY.m41624a(ContractAdapter.class, "e8f1c2d8638888607d159bbdad270875", i9);
        byG = a9;
        fmVarArr[i9] = a9;
        int i10 = i9 + 1;
        C2491fm a10 = C4105zY.m41624a(ContractAdapter.class, "11c32a01536fd0b49dbacf542cdfe50f", i10);
        byH = a10;
        fmVarArr[i10] = a10;
        int i11 = i10 + 1;
        C2491fm a11 = C4105zY.m41624a(ContractAdapter.class, "3ab24e039a371700dbac72ff5b4993bd", i11);
        aLU = a11;
        fmVarArr[i11] = a11;
        int i12 = i11 + 1;
        C2491fm a12 = C4105zY.m41624a(ContractAdapter.class, "a2522aaa5dbd269f9177c329cb5bc4e9", i12);
        aLT = a12;
        fmVarArr[i12] = a12;
        int i13 = i12 + 1;
        C2491fm a13 = C4105zY.m41624a(ContractAdapter.class, "f80bc4c4e467c864937fb68e99014852", i13);
        byI = a13;
        fmVarArr[i13] = a13;
        int i14 = i13 + 1;
        C2491fm a14 = C4105zY.m41624a(ContractAdapter.class, "aaac1693e9c8283274cd5309efdd1297", i14);
        byJ = a14;
        fmVarArr[i14] = a14;
        int i15 = i14 + 1;
        C2491fm a15 = C4105zY.m41624a(ContractAdapter.class, "bd4bb6475bc31195ddb5d2ad2910065f", i15);
        byK = a15;
        fmVarArr[i15] = a15;
        int i16 = i15 + 1;
        C2491fm a16 = C4105zY.m41624a(ContractAdapter.class, "28ab8c69973e88b848cd39e43ff5d5c6", i16);
        byL = a16;
        fmVarArr[i16] = a16;
        int i17 = i16 + 1;
        C2491fm a17 = C4105zY.m41624a(ContractAdapter.class, "35f2539ab152d311639a925d938658f6", i17);
        byM = a17;
        fmVarArr[i17] = a17;
        int i18 = i17 + 1;
        C2491fm a18 = C4105zY.m41624a(ContractAdapter.class, "88901245626d708e9ca786024b4e988b", i18);
        beH = a18;
        fmVarArr[i18] = a18;
        int i19 = i18 + 1;
        C2491fm a19 = C4105zY.m41624a(ContractAdapter.class, "b275537547c6e4f6da9220607ffc45b3", i19);
        byN = a19;
        fmVarArr[i19] = a19;
        int i20 = i19 + 1;
        C2491fm a20 = C4105zY.m41624a(ContractAdapter.class, "618f725c1149129f36f307ef53f54aea", i20);
        byO = a20;
        fmVarArr[i20] = a20;
        int i21 = i20 + 1;
        C2491fm a21 = C4105zY.m41624a(ContractAdapter.class, "2bc0d56b723e895e520bf98a6bd6bb20", i21);
        byP = a21;
        fmVarArr[i21] = a21;
        int i22 = i21 + 1;
        C2491fm a22 = C4105zY.m41624a(ContractAdapter.class, "13e68eaf3c34f4e66abce4240baf5e54", i22);
        byQ = a22;
        fmVarArr[i22] = a22;
        int i23 = i22 + 1;
        C2491fm a23 = C4105zY.m41624a(ContractAdapter.class, "f5c09953838990dc8126123ccda85183", i23);
        aMe = a23;
        fmVarArr[i23] = a23;
        int i24 = i23 + 1;
        C2491fm a24 = C4105zY.m41624a(ContractAdapter.class, "dfdfd592e6283f77749c794f89c08b4e", i24);
        aLS = a24;
        fmVarArr[i24] = a24;
        int i25 = i24 + 1;
        C2491fm a25 = C4105zY.m41624a(ContractAdapter.class, "909fc3369197b10e58049b556cbaa09a", i25);
        aLR = a25;
        fmVarArr[i25] = a25;
        int i26 = i25 + 1;
        C2491fm a26 = C4105zY.m41624a(ContractAdapter.class, "034149a458b3e5011c2747aa17b5b77a", i26);
        beF = a26;
        fmVarArr[i26] = a26;
        int i27 = i26 + 1;
        C2491fm a27 = C4105zY.m41624a(ContractAdapter.class, "cb52fcc103e3e00db6be1d5b8574ad11", i27);
        byR = a27;
        fmVarArr[i27] = a27;
        int i28 = i27 + 1;
        C2491fm a28 = C4105zY.m41624a(ContractAdapter.class, "386c45b29b246e23284797e09d0a94e5", i28);
        byS = a28;
        fmVarArr[i28] = a28;
        int i29 = i28 + 1;
        _m_methods = fmVarArr;
        C1634Xv.m11725a((Object[]) Mission._m_methods, (Object[]) _m_methods);
        ___iScriptClass = aUO.m18566a(ContractAdapter.class, C5978aeC.class, _m_fields, _m_methods);
    }

    /* renamed from: RK */
    public void mo65RK() {
        switch (bFf().mo6893i(aLT)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, aLT, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, aLT, new Object[0]));
                break;
        }
        m40355RJ();
    }

    /* renamed from: RM */
    public void mo66RM() {
        switch (bFf().mo6893i(aLU)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, aLU, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, aLU, new Object[0]));
                break;
        }
        m40356RL();
    }

    /* renamed from: T */
    public C6494any mo11T() {
        return ___iScriptClass;
    }

    /* renamed from: U */
    public C2491fm[] mo12U() {
        return _m_methods;
    }

    /* renamed from: W */
    public Object mo13W() {
        return new C5978aeC(this);
    }

    /* renamed from: a */
    public Object mo14a(C0495Gr gr) {
        Object[] args = gr.getArgs();
        switch (gr.mo2417hq() - Mission._m_methodCount) {
            case 0:
                akF();
                return null;
            case 1:
                akH();
                return null;
            case 2:
                m40364a((String) args[0], (Ship) args[1], (Ship) args[2]);
                return null;
            case 3:
                m40361a((String) args[0], (Station) args[1]);
                return null;
            case 4:
                m40362a((String) args[0], (Character) args[1]);
                return null;
            case 5:
                m40366b((Loot) args[0]);
                return null;
            case 6:
                m40360a((Character) args[0], (Loot) args[1]);
                return null;
            case 7:
                m40363a((String) args[0], (Ship) args[1]);
                return null;
            case 8:
                m40368c((String) args[0], (Ship) args[1]);
                return null;
            case 9:
                m40377s((String) args[0], (String) args[1]);
                return null;
            case 10:
                m40356RL();
                return null;
            case 11:
                m40355RJ();
                return null;
            case 12:
                m40379w((Ship) args[0]);
                return null;
            case 13:
                m40375p((Station) args[0]);
                return null;
            case 14:
                m40376r((Station) args[0]);
                return null;
            case 15:
                m40370d((Node) args[0]);
                return null;
            case 16:
                m40373k((Gate) args[0]);
                return null;
            case 17:
                m40374n((Ship) args[0]);
                return null;
            case 18:
                m40378t((Station) args[0]);
                return null;
            case 19:
                m40359a((MissionTrigger) args[0]);
                return null;
            case 20:
                akJ();
                return null;
            case 21:
                m40369cq((String) args[0]);
                return null;
            case 22:
                m40372i((Ship) args[0]);
                return null;
            case 23:
                m40371g((Ship) args[0]);
                return null;
            case 24:
                m40365aV((String) args[0]);
                return null;
            case 25:
                return new Boolean(m40367bk((String) args[0]));
            case 26:
                m40358a((LootType) args[0], (ItemType) args[1], ((Integer) args[2]).intValue());
                return null;
            case 27:
                akL();
                return null;
            default:
                return super.mo14a(gr);
        }
    }

    /* renamed from: a */
    public void mo15a(Collection collection, C0495Gr gr) {
        super.mo15a(collection, gr);
    }

    /* renamed from: aW */
    public void mo73aW(String str) {
        switch (bFf().mo6893i(aLR)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, aLR, new Object[]{str}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, aLR, new Object[]{str}));
                break;
        }
        m40365aV(str);
    }

    /* access modifiers changed from: protected */
    public void akG() {
        switch (bFf().mo6893i(byz)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, byz, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, byz, new Object[0]));
                break;
        }
        akF();
    }

    /* access modifiers changed from: protected */
    public void akI() {
        switch (bFf().mo6893i(byA)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, byA, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, byA, new Object[0]));
                break;
        }
        akH();
    }

    public void akK() {
        switch (bFf().mo6893i(byP)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, byP, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, byP, new Object[0]));
                break;
        }
        akJ();
    }

    public void akM() {
        switch (bFf().mo6893i(byS)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, byS, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, byS, new Object[0]));
                break;
        }
        akL();
    }

    /* renamed from: b */
    public void mo86b(LootType ahc, ItemType jCVar, int i) {
        switch (bFf().mo6893i(byR)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, byR, new Object[]{ahc, jCVar, new Integer(i)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, byR, new Object[]{ahc, jCVar, new Integer(i)}));
                break;
        }
        m40358a(ahc, jCVar, i);
    }

    /* renamed from: b */
    public void mo87b(MissionTrigger aus) {
        switch (bFf().mo6893i(byO)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, byO, new Object[]{aus}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, byO, new Object[]{aus}));
                break;
        }
        m40359a(aus);
    }

    /* renamed from: b */
    public void mo88b(Character acx, Loot ael) {
        switch (bFf().mo6893i(byE)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, byE, new Object[]{acx, ael}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, byE, new Object[]{acx, ael}));
                break;
        }
        m40360a(acx, ael);
    }

    /* renamed from: b */
    public void mo91b(String str, Station bf) {
        switch (bFf().mo6893i(byB)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, byB, new Object[]{str, bf}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, byB, new Object[]{str, bf}));
                break;
        }
        m40361a(str, bf);
    }

    /* renamed from: b */
    public void mo93b(String str, Character acx) {
        switch (bFf().mo6893i(byC)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, byC, new Object[]{str, acx}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, byC, new Object[]{str, acx}));
                break;
        }
        m40362a(str, acx);
    }

    /* renamed from: b */
    public void mo94b(String str, Ship fAVar) {
        switch (bFf().mo6893i(byF)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, byF, new Object[]{str, fAVar}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, byF, new Object[]{str, fAVar}));
                break;
        }
        m40363a(str, fAVar);
    }

    /* renamed from: b */
    public void mo95b(String str, Ship fAVar, Ship fAVar2) {
        switch (bFf().mo6893i(beG)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, beG, new Object[]{str, fAVar, fAVar2}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, beG, new Object[]{str, fAVar, fAVar2}));
                break;
        }
        m40364a(str, fAVar, fAVar2);
    }

    /* renamed from: bl */
    public boolean mo97bl(String str) {
        switch (bFf().mo6893i(beF)) {
            case 0:
                return false;
            case 2:
                return ((Boolean) bFf().mo5606d(new aCE(this, beF, new Object[]{str}))).booleanValue();
            case 3:
                bFf().mo5606d(new aCE(this, beF, new Object[]{str}));
                break;
        }
        return m40367bk(str);
    }

    /* renamed from: c */
    public void mo99c(Loot ael) {
        switch (bFf().mo6893i(byD)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, byD, new Object[]{ael}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, byD, new Object[]{ael}));
                break;
        }
        m40366b(ael);
    }

    /* renamed from: c */
    public C5663aRz[] mo25c() {
        return _m_fields;
    }

    /* renamed from: cr */
    public void mo115cr(String str) {
        switch (bFf().mo6893i(byQ)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, byQ, new Object[]{str}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, byQ, new Object[]{str}));
                break;
        }
        m40369cq(str);
    }

    /* renamed from: d */
    public void mo119d(String str, Ship fAVar) {
        switch (bFf().mo6893i(byG)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, byG, new Object[]{str, fAVar}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, byG, new Object[]{str, fAVar}));
                break;
        }
        m40368c(str, fAVar);
    }

    /* renamed from: e */
    public void mo121e(Node rPVar) {
        switch (bFf().mo6893i(byL)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, byL, new Object[]{rPVar}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, byL, new Object[]{rPVar}));
                break;
        }
        m40370d(rPVar);
    }

    /* renamed from: h */
    public void mo125h(Ship fAVar) {
        switch (bFf().mo6893i(aLS)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, aLS, new Object[]{fAVar}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, aLS, new Object[]{fAVar}));
                break;
        }
        m40371g(fAVar);
    }

    /* renamed from: j */
    public void mo130j(Ship fAVar) {
        switch (bFf().mo6893i(aMe)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, aMe, new Object[]{fAVar}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, aMe, new Object[]{fAVar}));
                break;
        }
        m40372i(fAVar);
    }

    /* renamed from: l */
    public void mo131l(Gate fFVar) {
        switch (bFf().mo6893i(byM)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, byM, new Object[]{fFVar}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, byM, new Object[]{fFVar}));
                break;
        }
        m40373k(fFVar);
    }

    /* renamed from: o */
    public void mo132o(Ship fAVar) {
        switch (bFf().mo6893i(beH)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, beH, new Object[]{fAVar}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, beH, new Object[]{fAVar}));
                break;
        }
        m40374n(fAVar);
    }

    /* renamed from: q */
    public void mo133q(Station bf) {
        switch (bFf().mo6893i(byJ)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, byJ, new Object[]{bf}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, byJ, new Object[]{bf}));
                break;
        }
        m40375p(bf);
    }

    /* renamed from: s */
    public void mo134s(Station bf) {
        switch (bFf().mo6893i(byK)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, byK, new Object[]{bf}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, byK, new Object[]{bf}));
                break;
        }
        m40376r(bf);
    }

    /* renamed from: t */
    public void mo135t(String str, String str2) {
        switch (bFf().mo6893i(byH)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, byH, new Object[]{str, str2}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, byH, new Object[]{str, str2}));
                break;
        }
        m40377s(str, str2);
    }

    /* renamed from: u */
    public void mo136u(Station bf) {
        switch (bFf().mo6893i(byN)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, byN, new Object[]{bf}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, byN, new Object[]{bf}));
                break;
        }
        m40378t(bf);
    }

    /* renamed from: x */
    public void mo138x(Ship fAVar) {
        switch (bFf().mo6893i(byI)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, byI, new Object[]{fAVar}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, byI, new Object[]{fAVar}));
                break;
        }
        m40379w(fAVar);
    }

    /* renamed from: S */
    public void mo10S() {
        super.mo10S();
    }

    @C0064Am(aul = "c3926ff5f1690f9335f4d61848ebdf10", aum = 0)
    private void akF() {
        ((ContractTemplate) cdp()).aAo();
        fail();
    }

    @C0064Am(aul = "66c41b8f90f6bd0705eacb3a184dd606", aum = 0)
    private void akH() {
        ((ContractTemplate) cdp()).aAm();
        cdk();
        if (cdp() instanceof TransportTemplate) {
            mo2066a((C5878acG) new aMH(((ContractTemplate) cdp()).aAC(), ((ContractTemplate) cdp()).aAw().getName(), ((ContractTemplate) cdp()).mo711rP().get(), ((ContractTemplate) cdp()).aAy().aqL(), ((ContractTemplate) cdp()).aAA(), ((ContractTemplate) cdp()).aAy().aRB(), ((ContractTemplate) cdp()).aAq().getName(), ((TransportTemplate) cdp()).baH().aoG().aRB(), ((TransportTemplate) cdp()).mo3130Fl().getName(), ((TransportTemplate) cdp()).mo3131Fm().getName(), (String) null));
        } else if (cdp() instanceof BountyTemplate) {
            mo2066a((C5878acG) new aMH(((ContractTemplate) cdp()).aAC(), ((ContractTemplate) cdp()).aAw().getName(), ((ContractTemplate) cdp()).mo711rP().get(), ((ContractTemplate) cdp()).aAy().aqL(), ((ContractTemplate) cdp()).aAA(), ((ContractTemplate) cdp()).aAy().aRB(), ((ContractTemplate) cdp()).aAq().getName(), 0.0f, (String) null, (String) null, ((BountyTemplate) cdp()).mo9371KG().getName()));
        }
    }

    @C0064Am(aul = "c447a9b887639c938f54eb374337f99b", aum = 0)
    /* renamed from: a */
    private void m40364a(String str, Ship fAVar, Ship fAVar2) {
    }

    @C0064Am(aul = "48c912a1d48d4b046872b87091bac2b6", aum = 0)
    /* renamed from: a */
    private void m40361a(String str, Station bf) {
    }

    @C0064Am(aul = "4c914a8381814d19f3d0cdb7564e3235", aum = 0)
    /* renamed from: a */
    private void m40362a(String str, Character acx) {
    }

    @C0064Am(aul = "576e56ecd56e810e93bfec060978b9a7", aum = 0)
    /* renamed from: b */
    private void m40366b(Loot ael) {
    }

    @C0064Am(aul = "156e7efe1be14253b25026c378c33edb", aum = 0)
    /* renamed from: a */
    private void m40360a(Character acx, Loot ael) {
    }

    @C0064Am(aul = "a4d610addd13066f4e1951040a03df59", aum = 0)
    /* renamed from: a */
    private void m40363a(String str, Ship fAVar) {
    }

    @C0064Am(aul = "e8f1c2d8638888607d159bbdad270875", aum = 0)
    /* renamed from: c */
    private void m40368c(String str, Ship fAVar) {
    }

    @C0064Am(aul = "11c32a01536fd0b49dbacf542cdfe50f", aum = 0)
    /* renamed from: s */
    private void m40377s(String str, String str2) {
    }

    @C0064Am(aul = "3ab24e039a371700dbac72ff5b4993bd", aum = 0)
    /* renamed from: RL */
    private void m40356RL() {
    }

    @C0064Am(aul = "a2522aaa5dbd269f9177c329cb5bc4e9", aum = 0)
    /* renamed from: RJ */
    private void m40355RJ() {
    }

    @C0064Am(aul = "f80bc4c4e467c864937fb68e99014852", aum = 0)
    /* renamed from: w */
    private void m40379w(Ship fAVar) {
    }

    @C0064Am(aul = "aaac1693e9c8283274cd5309efdd1297", aum = 0)
    /* renamed from: p */
    private void m40375p(Station bf) {
    }

    @C0064Am(aul = "bd4bb6475bc31195ddb5d2ad2910065f", aum = 0)
    /* renamed from: r */
    private void m40376r(Station bf) {
    }

    @C0064Am(aul = "28ab8c69973e88b848cd39e43ff5d5c6", aum = 0)
    /* renamed from: d */
    private void m40370d(Node rPVar) {
    }

    @C0064Am(aul = "35f2539ab152d311639a925d938658f6", aum = 0)
    /* renamed from: k */
    private void m40373k(Gate fFVar) {
    }

    @C0064Am(aul = "88901245626d708e9ca786024b4e988b", aum = 0)
    /* renamed from: n */
    private void m40374n(Ship fAVar) {
    }

    @C0064Am(aul = "b275537547c6e4f6da9220607ffc45b3", aum = 0)
    /* renamed from: t */
    private void m40378t(Station bf) {
    }

    @C0064Am(aul = "618f725c1149129f36f307ef53f54aea", aum = 0)
    /* renamed from: a */
    private void m40359a(MissionTrigger aus) {
    }

    @C0064Am(aul = "2bc0d56b723e895e520bf98a6bd6bb20", aum = 0)
    private void akJ() {
    }

    @C0064Am(aul = "13e68eaf3c34f4e66abce4240baf5e54", aum = 0)
    /* renamed from: cq */
    private void m40369cq(String str) {
    }

    @C0064Am(aul = "f5c09953838990dc8126123ccda85183", aum = 0)
    /* renamed from: i */
    private void m40372i(Ship fAVar) {
    }

    @C0064Am(aul = "dfdfd592e6283f77749c794f89c08b4e", aum = 0)
    /* renamed from: g */
    private void m40371g(Ship fAVar) {
    }

    @C0064Am(aul = "909fc3369197b10e58049b556cbaa09a", aum = 0)
    /* renamed from: aV */
    private void m40365aV(String str) {
    }

    @C0064Am(aul = "034149a458b3e5011c2747aa17b5b77a", aum = 0)
    /* renamed from: bk */
    private boolean m40367bk(String str) {
        return false;
    }

    @C0064Am(aul = "cb52fcc103e3e00db6be1d5b8574ad11", aum = 0)
    /* renamed from: a */
    private void m40358a(LootType ahc, ItemType jCVar, int i) {
    }

    @C0064Am(aul = "386c45b29b246e23284797e09d0a94e5", aum = 0)
    private void akL() {
    }
}
