package game.script.contract.types;

import game.network.message.externalizable.aCE;
import game.script.Actor;
import game.script.item.Item;
import game.script.item.ItemLocation;
import game.script.ship.Ship;
import logic.baa.C1616Xf;
import logic.baa.C1634Xv;
import logic.baa.C5511aMd;
import logic.baa.C6485anp;
import logic.data.mbean.C0793LU;
import logic.res.code.C5663aRz;
import logic.res.code.C6494any;
import logic.res.html.C2491fm;
import p001a.*;

import java.util.Collection;

@C5511aMd
@C6485anp
/* renamed from: a.nS */
/* compiled from: a */
public class BountyContract<T extends BountyTemplate> extends ContractAdapter<T> implements C1616Xf {
    public static final int _m_fieldCount = 0;
    public static final C5663aRz[] _m_fields = null;
    public static final int _m_methodCount = 0;
    public static final C2491fm[] _m_methods = null;
    public static final C2491fm aLR = null;
    public static final C2491fm aLS = null;
    public static final C2491fm aLT = null;
    public static final C2491fm aLU = null;
    public static final C2491fm aLV = null;
    public static final C2491fm aLW = null;
    public static final C2491fm aLX = null;
    public static final C2491fm aLY = null;
    private static final String aLQ = "bounty_mission_timer";
    private static final long serialVersionUID = -6932759948606148695L;
    public static C6494any ___iScriptClass = null;

    static {
        m36082V();
    }

    public BountyContract() {
        super((C5540aNg) null);
        super.mo10S();
    }

    public BountyContract(C5540aNg ang) {
        super(ang);
    }

    /* renamed from: V */
    static void m36082V() {
        _m_fieldCount = ContractAdapter._m_fieldCount + 0;
        _m_methodCount = ContractAdapter._m_methodCount + 8;
        _m_fields = new C5663aRz[(ContractAdapter._m_fieldCount + 0)];
        C1634Xv.m11725a((Object[]) ContractAdapter._m_fields, (Object[]) _m_fields);
        int i = ContractAdapter._m_methodCount;
        C2491fm[] fmVarArr = new C2491fm[(i + 8)];
        C2491fm a = C4105zY.m41624a(BountyContract.class, "2392c09a436eb61855544460c050ee33", i);
        aLR = a;
        fmVarArr[i] = a;
        int i2 = i + 1;
        C2491fm a2 = C4105zY.m41624a(BountyContract.class, "bb054b8283bab8a04c6e55c33d8c5447", i2);
        aLS = a2;
        fmVarArr[i2] = a2;
        int i3 = i2 + 1;
        C2491fm a3 = C4105zY.m41624a(BountyContract.class, "c8b246f5242e3087d07374b9e8f2ed2f", i3);
        aLT = a3;
        fmVarArr[i3] = a3;
        int i4 = i3 + 1;
        C2491fm a4 = C4105zY.m41624a(BountyContract.class, "72dfb2ec3117a29d6f70cd5029421600", i4);
        aLU = a4;
        fmVarArr[i4] = a4;
        int i5 = i4 + 1;
        C2491fm a5 = C4105zY.m41624a(BountyContract.class, "7aaff0b78463d67e30c8f2bdd1f0a2a6", i5);
        aLV = a5;
        fmVarArr[i5] = a5;
        int i6 = i5 + 1;
        C2491fm a6 = C4105zY.m41624a(BountyContract.class, "ff3b45bbcf43a62d556814812962953f", i6);
        aLW = a6;
        fmVarArr[i6] = a6;
        int i7 = i6 + 1;
        C2491fm a7 = C4105zY.m41624a(BountyContract.class, "34dc90d3fa1798eb23e8011569c970ba", i7);
        aLX = a7;
        fmVarArr[i7] = a7;
        int i8 = i7 + 1;
        C2491fm a8 = C4105zY.m41624a(BountyContract.class, "0a83c8156606cb10e5856504f037c6d3", i8);
        aLY = a8;
        fmVarArr[i8] = a8;
        int i9 = i8 + 1;
        _m_methods = fmVarArr;
        C1634Xv.m11725a((Object[]) ContractAdapter._m_methods, (Object[]) _m_methods);
        ___iScriptClass = aUO.m18566a(BountyContract.class, C0793LU.class, _m_fields, _m_methods);
    }

    @C0064Am(aul = "c8b246f5242e3087d07374b9e8f2ed2f", aum = 0)
    @C5566aOg
    /* renamed from: RJ */
    private void m36079RJ() {
        throw new aWi(new aCE(this, aLT, new Object[0]));
    }

    @C0064Am(aul = "2392c09a436eb61855544460c050ee33", aum = 0)
    @C5566aOg
    /* renamed from: aV */
    private void m36085aV(String str) {
        throw new aWi(new aCE(this, aLR, new Object[]{str}));
    }

    @C5566aOg
    /* renamed from: RK */
    public void mo65RK() {
        switch (bFf().mo6893i(aLT)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, aLT, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, aLT, new Object[0]));
                break;
        }
        m36079RJ();
    }

    /* renamed from: RM */
    public void mo66RM() {
        switch (bFf().mo6893i(aLU)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, aLU, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, aLU, new Object[0]));
                break;
        }
        m36080RL();
    }

    /* renamed from: RO */
    public void mo67RO() {
        switch (bFf().mo6893i(aLX)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, aLX, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, aLX, new Object[0]));
                break;
        }
        m36081RN();
    }

    /* renamed from: T */
    public C6494any mo11T() {
        return ___iScriptClass;
    }

    /* renamed from: U */
    public C2491fm[] mo12U() {
        return _m_methods;
    }

    /* renamed from: W */
    public Object mo13W() {
        return new C0793LU(this);
    }

    /* renamed from: a */
    public Object mo14a(C0495Gr gr) {
        Object[] args = gr.getArgs();
        switch (gr.mo2417hq() - ContractAdapter._m_methodCount) {
            case 0:
                m36085aV((String) args[0]);
                return null;
            case 1:
                m36087g((Ship) args[0]);
                return null;
            case 2:
                m36079RJ();
                return null;
            case 3:
                m36080RL();
                return null;
            case 4:
                m36084a((Item) args[0], (ItemLocation) args[1], (ItemLocation) args[2]);
                return null;
            case 5:
                m36086c((Item) args[0], (ItemLocation) args[1]);
                return null;
            case 6:
                m36081RN();
                return null;
            case 7:
                m36083a((Actor) args[0], (C5260aCm) args[1]);
                return null;
            default:
                return super.mo14a(gr);
        }
    }

    /* renamed from: a */
    public void mo15a(Collection collection, C0495Gr gr) {
        super.mo15a(collection, gr);
    }

    @C5566aOg
    /* renamed from: aW */
    public void mo73aW(String str) {
        switch (bFf().mo6893i(aLR)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, aLR, new Object[]{str}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, aLR, new Object[]{str}));
                break;
        }
        m36085aV(str);
    }

    /* renamed from: b */
    public void mo85b(Actor cr, C5260aCm acm) {
        switch (bFf().mo6893i(aLY)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, aLY, new Object[]{cr, acm}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, aLY, new Object[]{cr, acm}));
                break;
        }
        m36083a(cr, acm);
    }

    /* renamed from: b */
    public void mo90b(Item auq, ItemLocation aag, ItemLocation aag2) {
        switch (bFf().mo6893i(aLV)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, aLV, new Object[]{auq, aag, aag2}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, aLV, new Object[]{auq, aag, aag2}));
                break;
        }
        m36084a(auq, aag, aag2);
    }

    /* renamed from: c */
    public C5663aRz[] mo25c() {
        return _m_fields;
    }

    /* renamed from: d */
    public void mo117d(Item auq, ItemLocation aag) {
        switch (bFf().mo6893i(aLW)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, aLW, new Object[]{auq, aag}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, aLW, new Object[]{auq, aag}));
                break;
        }
        m36086c(auq, aag);
    }

    /* renamed from: h */
    public void mo125h(Ship fAVar) {
        switch (bFf().mo6893i(aLS)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, aLS, new Object[]{fAVar}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, aLS, new Object[]{fAVar}));
                break;
        }
        m36087g(fAVar);
    }

    /* renamed from: S */
    public void mo10S() {
        super.mo10S();
    }

    @C0064Am(aul = "bb054b8283bab8a04c6e55c33d8c5447", aum = 0)
    /* renamed from: g */
    private void m36087g(Ship fAVar) {
        super.mo125h(fAVar);
        if (fAVar.agj() == ((BountyTemplate) cdp()).mo9371KG()) {
            akI();
        }
    }

    @C0064Am(aul = "72dfb2ec3117a29d6f70cd5029421600", aum = 0)
    /* renamed from: RL */
    private void m36080RL() {
        super.mo66RM();
        akG();
    }

    @C0064Am(aul = "7aaff0b78463d67e30c8f2bdd1f0a2a6", aum = 0)
    /* renamed from: a */
    private void m36084a(Item auq, ItemLocation aag, ItemLocation aag2) {
    }

    @C0064Am(aul = "ff3b45bbcf43a62d556814812962953f", aum = 0)
    /* renamed from: c */
    private void m36086c(Item auq, ItemLocation aag) {
    }

    @C0064Am(aul = "34dc90d3fa1798eb23e8011569c970ba", aum = 0)
    /* renamed from: RN */
    private void m36081RN() {
    }

    @C0064Am(aul = "0a83c8156606cb10e5856504f037c6d3", aum = 0)
    /* renamed from: a */
    private void m36083a(Actor cr, C5260aCm acm) {
    }
}
