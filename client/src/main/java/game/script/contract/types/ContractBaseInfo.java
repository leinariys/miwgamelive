package game.script.contract.types;

import game.network.message.externalizable.aCE;
import logic.baa.*;
import logic.data.mbean.C2035bN;
import logic.res.code.C5640aRc;
import logic.res.code.C5663aRz;
import logic.res.code.C6494any;
import logic.res.html.C2491fm;
import p001a.*;
import taikodom.infra.script.I18NString;

import java.util.Collection;
import java.util.UUID;

@C5511aMd
@C6485anp
/* renamed from: a.tE */
/* compiled from: a */
public abstract class ContractBaseInfo extends TaikodomObject implements C0468GU, C1616Xf {

    /* renamed from: MN */
    public static final C2491fm f9216MN = null;

    /* renamed from: QA */
    public static final C5663aRz f9217QA = null;

    /* renamed from: QD */
    public static final C2491fm f9218QD = null;

    /* renamed from: QE */
    public static final C2491fm f9219QE = null;

    /* renamed from: QF */
    public static final C2491fm f9220QF = null;

    /* renamed from: Qz */
    public static final C5663aRz f9221Qz = null;
    public static final int _m_fieldCount = 0;
    public static final C5663aRz[] _m_fields = null;
    public static final int _m_methodCount = 0;
    public static final C2491fm[] _m_methods = null;
    /* renamed from: bL */
    public static final C5663aRz f9223bL = null;
    /* renamed from: bM */
    public static final C5663aRz f9224bM = null;
    /* renamed from: bN */
    public static final C2491fm f9225bN = null;
    /* renamed from: bO */
    public static final C2491fm f9226bO = null;
    /* renamed from: bP */
    public static final C2491fm f9227bP = null;
    /* renamed from: bQ */
    public static final C2491fm f9228bQ = null;
    public static final C5663aRz bnx = null;
    public static final C2491fm bny = null;
    public static final C2491fm bnz = null;
    public static final long serialVersionUID = 0;
    public static C6494any ___iScriptClass;
    @C0064Am(aul = "c112259417f715acd277ec1562419c9c", aum = 4)

    /* renamed from: bK */
    private static UUID f9222bK;
    @C0064Am(aul = "2bdaa38ec32c32c3f23add4bf45e95f2", aum = 0)
    private static String handle;
    @C0064Am(aul = "935faa1f3eeac8b25f86198cbbc1ed0b", aum = 2)

    /* renamed from: nh */
    private static I18NString f9229nh;
    @C0064Am(aul = "106cc3eb6d91518302404372239bd752", aum = 1)

    /* renamed from: ni */
    private static I18NString f9230ni;
    @C0064Am(aul = "0a277957c0f7dae11bc560e66ad94ba1", aum = 3)

    /* renamed from: pi */
    private static long f9231pi;

    static {
        m39531V();
    }

    public ContractBaseInfo() {
        super((C5540aNg) null);
        super.mo10S();
    }

    public ContractBaseInfo(C5540aNg ang) {
        super(ang);
    }

    /* renamed from: V */
    static void m39531V() {
        serialVersionUID = (long) 1;
        _m_fieldCount = TaikodomObject._m_fieldCount + 5;
        _m_methodCount = TaikodomObject._m_methodCount + 10;
        int i = TaikodomObject._m_fieldCount;
        C5663aRz[] arzArr = new C5663aRz[(i + 5)];
        C5663aRz b = C5640aRc.m17844b(ContractBaseInfo.class, "2bdaa38ec32c32c3f23add4bf45e95f2", i);
        f9224bM = b;
        arzArr[i] = b;
        int i2 = i + 1;
        C5663aRz b2 = C5640aRc.m17844b(ContractBaseInfo.class, "106cc3eb6d91518302404372239bd752", i2);
        f9221Qz = b2;
        arzArr[i2] = b2;
        int i3 = i2 + 1;
        C5663aRz b3 = C5640aRc.m17844b(ContractBaseInfo.class, "935faa1f3eeac8b25f86198cbbc1ed0b", i3);
        f9217QA = b3;
        arzArr[i3] = b3;
        int i4 = i3 + 1;
        C5663aRz b4 = C5640aRc.m17844b(ContractBaseInfo.class, "0a277957c0f7dae11bc560e66ad94ba1", i4);
        bnx = b4;
        arzArr[i4] = b4;
        int i5 = i4 + 1;
        C5663aRz b5 = C5640aRc.m17844b(ContractBaseInfo.class, "c112259417f715acd277ec1562419c9c", i5);
        f9223bL = b5;
        arzArr[i5] = b5;
        int i6 = i5 + 1;
        _m_fields = arzArr;
        C1634Xv.m11725a((Object[]) TaikodomObject._m_fields, (Object[]) _m_fields);
        int i7 = TaikodomObject._m_methodCount;
        C2491fm[] fmVarArr = new C2491fm[(i7 + 10)];
        C2491fm a = C4105zY.m41624a(ContractBaseInfo.class, "3db859387120742ba9e5331e04643de3", i7);
        f9225bN = a;
        fmVarArr[i7] = a;
        int i8 = i7 + 1;
        C2491fm a2 = C4105zY.m41624a(ContractBaseInfo.class, "59460652276add063368d2c15b42d49f", i8);
        f9226bO = a2;
        fmVarArr[i8] = a2;
        int i9 = i8 + 1;
        C2491fm a3 = C4105zY.m41624a(ContractBaseInfo.class, "f17ca1ae87c133e26cc998b177b1aafd", i9);
        f9219QE = a3;
        fmVarArr[i9] = a3;
        int i10 = i9 + 1;
        C2491fm a4 = C4105zY.m41624a(ContractBaseInfo.class, "02e72af8ab63e5574f1050f23bcd6d07", i10);
        f9220QF = a4;
        fmVarArr[i10] = a4;
        int i11 = i10 + 1;
        C2491fm a5 = C4105zY.m41624a(ContractBaseInfo.class, "5bebafb193098889494b50c0cb1c5d47", i11);
        f9216MN = a5;
        fmVarArr[i11] = a5;
        int i12 = i11 + 1;
        C2491fm a6 = C4105zY.m41624a(ContractBaseInfo.class, "fc473dc88d9b60f62be9229683392c01", i12);
        f9218QD = a6;
        fmVarArr[i12] = a6;
        int i13 = i12 + 1;
        C2491fm a7 = C4105zY.m41624a(ContractBaseInfo.class, "86688df9850d3fed2a05355c6e89f91d", i13);
        f9227bP = a7;
        fmVarArr[i13] = a7;
        int i14 = i13 + 1;
        C2491fm a8 = C4105zY.m41624a(ContractBaseInfo.class, "7ba2b8dec40b99f17459f2970fd05ff3", i14);
        f9228bQ = a8;
        fmVarArr[i14] = a8;
        int i15 = i14 + 1;
        C2491fm a9 = C4105zY.m41624a(ContractBaseInfo.class, "42d617b057cc9dee37fb4f5d5f291ccb", i15);
        bny = a9;
        fmVarArr[i15] = a9;
        int i16 = i15 + 1;
        C2491fm a10 = C4105zY.m41624a(ContractBaseInfo.class, "f531412c43a64d6998bf97fd572c705f", i16);
        bnz = a10;
        fmVarArr[i16] = a10;
        int i17 = i16 + 1;
        _m_methods = fmVarArr;
        C1634Xv.m11725a((Object[]) TaikodomObject._m_methods, (Object[]) _m_methods);
        ___iScriptClass = aUO.m18566a(ContractBaseInfo.class, C2035bN.class, _m_fields, _m_methods);
    }

    /* renamed from: a */
    private void m39532a(String str) {
        bFf().mo5608dq().mo3197f(f9224bM, str);
    }

    /* renamed from: a */
    private void m39533a(UUID uuid) {
        bFf().mo5608dq().mo3197f(f9223bL, uuid);
    }

    private long aeF() {
        return bFf().mo5608dq().mo3213o(bnx);
    }

    /* renamed from: an */
    private UUID m39534an() {
        return (UUID) bFf().mo5608dq().mo3214p(f9223bL);
    }

    /* renamed from: ao */
    private String m39535ao() {
        return (String) bFf().mo5608dq().mo3214p(f9224bM);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Handle")
    @C0064Am(aul = "7ba2b8dec40b99f17459f2970fd05ff3", aum = 0)
    @C5566aOg
    /* renamed from: b */
    private void m39538b(String str) {
        throw new aWi(new aCE(this, f9228bQ, new Object[]{str}));
    }

    /* renamed from: bq */
    private void m39540bq(I18NString i18NString) {
        bFf().mo5608dq().mo3197f(f9221Qz, i18NString);
    }

    /* renamed from: br */
    private void m39541br(I18NString i18NString) {
        bFf().mo5608dq().mo3197f(f9217QA, i18NString);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Title")
    @C0064Am(aul = "fc473dc88d9b60f62be9229683392c01", aum = 0)
    @C5566aOg
    /* renamed from: bs */
    private void m39542bs(I18NString i18NString) {
        throw new aWi(new aCE(this, f9218QD, new Object[]{i18NString}));
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Description")
    @C0064Am(aul = "02e72af8ab63e5574f1050f23bcd6d07", aum = 0)
    @C5566aOg
    /* renamed from: bu */
    private void m39543bu(I18NString i18NString) {
        throw new aWi(new aCE(this, f9220QF, new Object[]{i18NString}));
    }

    /* renamed from: c */
    private void m39544c(UUID uuid) {
        switch (bFf().mo6893i(f9226bO)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f9226bO, new Object[]{uuid}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f9226bO, new Object[]{uuid}));
                break;
        }
        m39539b(uuid);
    }

    /* renamed from: cS */
    private void m39545cS(long j) {
        bFf().mo5608dq().mo3184b(bnx, j);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Fee")
    @C0064Am(aul = "f531412c43a64d6998bf97fd572c705f", aum = 0)
    @C5566aOg
    /* renamed from: cT */
    private void m39546cT(long j) {
        throw new aWi(new aCE(this, bnz, new Object[]{new Long(j)}));
    }

    /* renamed from: vS */
    private I18NString m39548vS() {
        return (I18NString) bFf().mo5608dq().mo3214p(f9221Qz);
    }

    /* renamed from: vT */
    private I18NString m39549vT() {
        return (I18NString) bFf().mo5608dq().mo3214p(f9217QA);
    }

    /* renamed from: T */
    public C6494any mo11T() {
        return ___iScriptClass;
    }

    /* renamed from: U */
    public C2491fm[] mo12U() {
        return _m_methods;
    }

    /* renamed from: W */
    public Object mo13W() {
        return new C2035bN(this);
    }

    /* renamed from: a */
    public Object mo14a(C0495Gr gr) {
        Object[] args = gr.getArgs();
        switch (gr.mo2417hq() - TaikodomObject._m_methodCount) {
            case 0:
                return m39536ap();
            case 1:
                m39539b((UUID) args[0]);
                return null;
            case 2:
                return m39550vV();
            case 3:
                m39543bu((I18NString) args[0]);
                return null;
            case 4:
                return m39547rO();
            case 5:
                m39542bs((I18NString) args[0]);
                return null;
            case 6:
                return m39537ar();
            case 7:
                m39538b((String) args[0]);
                return null;
            case 8:
                return new Long(aeG());
            case 9:
                m39546cT(((Long) args[0]).longValue());
                return null;
            default:
                return super.mo14a(gr);
        }
    }

    /* renamed from: a */
    public void mo15a(Collection collection, C0495Gr gr) {
        super.mo15a(collection, gr);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Fee")
    public long aeH() {
        switch (bFf().mo6893i(bny)) {
            case 0:
                return 0;
            case 2:
                return ((Long) bFf().mo5606d(new aCE(this, bny, new Object[0]))).longValue();
            case 3:
                bFf().mo5606d(new aCE(this, bny, new Object[0]));
                break;
        }
        return aeG();
    }

    /* renamed from: aq */
    public UUID mo18aq() {
        switch (bFf().mo6893i(f9225bN)) {
            case 0:
                return null;
            case 2:
                return (UUID) bFf().mo5606d(new aCE(this, f9225bN, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f9225bN, new Object[0]));
                break;
        }
        return m39536ap();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Title")
    @C5566aOg
    /* renamed from: bt */
    public void mo22202bt(I18NString i18NString) {
        switch (bFf().mo6893i(f9218QD)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f9218QD, new Object[]{i18NString}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f9218QD, new Object[]{i18NString}));
                break;
        }
        m39542bs(i18NString);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Description")
    @C5566aOg
    /* renamed from: bv */
    public void mo22203bv(I18NString i18NString) {
        switch (bFf().mo6893i(f9220QF)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f9220QF, new Object[]{i18NString}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f9220QF, new Object[]{i18NString}));
                break;
        }
        m39543bu(i18NString);
    }

    /* renamed from: c */
    public C5663aRz[] mo25c() {
        return _m_fields;
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Fee")
    @C5566aOg
    /* renamed from: cU */
    public void mo22204cU(long j) {
        switch (bFf().mo6893i(bnz)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, bnz, new Object[]{new Long(j)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, bnz, new Object[]{new Long(j)}));
                break;
        }
        m39546cT(j);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Handle")
    public String getHandle() {
        switch (bFf().mo6893i(f9227bP)) {
            case 0:
                return null;
            case 2:
                return (String) bFf().mo5606d(new aCE(this, f9227bP, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f9227bP, new Object[0]));
                break;
        }
        return m39537ar();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Handle")
    @C5566aOg
    public void setHandle(String str) {
        switch (bFf().mo6893i(f9228bQ)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f9228bQ, new Object[]{str}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f9228bQ, new Object[]{str}));
                break;
        }
        m39538b(str);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Title")
    /* renamed from: rP */
    public I18NString mo22205rP() {
        switch (bFf().mo6893i(f9216MN)) {
            case 0:
                return null;
            case 2:
                return (I18NString) bFf().mo5606d(new aCE(this, f9216MN, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f9216MN, new Object[0]));
                break;
        }
        return m39547rO();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Description")
    /* renamed from: vW */
    public I18NString mo22207vW() {
        switch (bFf().mo6893i(f9219QE)) {
            case 0:
                return null;
            case 2:
                return (I18NString) bFf().mo5606d(new aCE(this, f9219QE, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f9219QE, new Object[0]));
                break;
        }
        return m39550vV();
    }

    @C0064Am(aul = "3db859387120742ba9e5331e04643de3", aum = 0)
    /* renamed from: ap */
    private UUID m39536ap() {
        return m39534an();
    }

    @C0064Am(aul = "59460652276add063368d2c15b42d49f", aum = 0)
    /* renamed from: b */
    private void m39539b(UUID uuid) {
        m39533a(uuid);
    }

    /* renamed from: S */
    public void mo10S() {
        super.mo10S();
        m39533a(UUID.randomUUID());
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Description")
    @C0064Am(aul = "f17ca1ae87c133e26cc998b177b1aafd", aum = 0)
    /* renamed from: vV */
    private I18NString m39550vV() {
        return m39549vT();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Title")
    @C0064Am(aul = "5bebafb193098889494b50c0cb1c5d47", aum = 0)
    /* renamed from: rO */
    private I18NString m39547rO() {
        return m39548vS();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Handle")
    @C0064Am(aul = "86688df9850d3fed2a05355c6e89f91d", aum = 0)
    /* renamed from: ar */
    private String m39537ar() {
        return m39535ao();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Fee")
    @C0064Am(aul = "42d617b057cc9dee37fb4f5d5f291ccb", aum = 0)
    private long aeG() {
        return aeF();
    }
}
