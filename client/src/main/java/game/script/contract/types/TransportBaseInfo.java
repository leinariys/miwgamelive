package game.script.contract.types;

import game.network.message.externalizable.aCE;
import game.script.item.BagItemType;
import logic.baa.C1616Xf;
import logic.baa.C1634Xv;
import logic.baa.C5511aMd;
import logic.baa.C6485anp;
import logic.data.mbean.C6279ajr;
import logic.res.code.C5640aRc;
import logic.res.code.C5663aRz;
import logic.res.code.C6494any;
import logic.res.html.C2491fm;
import p001a.*;

import java.util.Collection;

@C5511aMd
@C6485anp
/* renamed from: a.gc */
/* compiled from: a */
public class TransportBaseInfo extends ContractBaseInfo implements C1616Xf {
    /* renamed from: OL */
    public static final C5663aRz f7677OL = null;
    /* renamed from: ON */
    public static final C5663aRz f7679ON = null;
    /* renamed from: OO */
    public static final C2491fm f7680OO = null;
    /* renamed from: OP */
    public static final C2491fm f7681OP = null;
    /* renamed from: OQ */
    public static final C2491fm f7682OQ = null;
    /* renamed from: OS */
    public static final C2491fm f7683OS = null;
    public static final int _m_fieldCount = 0;
    public static final C5663aRz[] _m_fields = null;
    public static final int _m_methodCount = 0;
    public static final C2491fm[] _m_methods = null;
    public static final long serialVersionUID = 0;
    public static C6494any ___iScriptClass;
    @C0064Am(aul = "f0cb93307cbe8b9a327a9766a0e14c05", aum = 0)

    /* renamed from: OK */
    private static BagItemType f7676OK;
    @C0064Am(aul = "882af63fc5686fa77dc04c017e36d9e9", aum = 1)

    /* renamed from: OM */
    private static long f7678OM;

    static {
        m32109V();
    }

    public TransportBaseInfo() {
        super((C5540aNg) null);
        super.mo10S();
    }

    public TransportBaseInfo(C5540aNg ang) {
        super(ang);
    }

    /* renamed from: V */
    static void m32109V() {
        serialVersionUID = (long) 1;
        _m_fieldCount = ContractBaseInfo._m_fieldCount + 2;
        _m_methodCount = ContractBaseInfo._m_methodCount + 4;
        int i = ContractBaseInfo._m_fieldCount;
        C5663aRz[] arzArr = new C5663aRz[(i + 2)];
        C5663aRz b = C5640aRc.m17844b(TransportBaseInfo.class, "f0cb93307cbe8b9a327a9766a0e14c05", i);
        f7677OL = b;
        arzArr[i] = b;
        int i2 = i + 1;
        C5663aRz b2 = C5640aRc.m17844b(TransportBaseInfo.class, "882af63fc5686fa77dc04c017e36d9e9", i2);
        f7679ON = b2;
        arzArr[i2] = b2;
        int i3 = i2 + 1;
        _m_fields = arzArr;
        C1634Xv.m11725a((Object[]) ContractBaseInfo._m_fields, (Object[]) _m_fields);
        int i4 = ContractBaseInfo._m_methodCount;
        C2491fm[] fmVarArr = new C2491fm[(i4 + 4)];
        C2491fm a = C4105zY.m41624a(TransportBaseInfo.class, "7f422f8227cba3680159d4c0e420501a", i4);
        f7680OO = a;
        fmVarArr[i4] = a;
        int i5 = i4 + 1;
        C2491fm a2 = C4105zY.m41624a(TransportBaseInfo.class, "a3945d06d002ac8b25c24f473ce29005", i5);
        f7681OP = a2;
        fmVarArr[i5] = a2;
        int i6 = i5 + 1;
        C2491fm a3 = C4105zY.m41624a(TransportBaseInfo.class, "e6311a7feca937d0514b60732bd855c7", i6);
        f7682OQ = a3;
        fmVarArr[i6] = a3;
        int i7 = i6 + 1;
        C2491fm a4 = C4105zY.m41624a(TransportBaseInfo.class, "d1a4357f09d0a12a256d50542e628d5a", i7);
        f7683OS = a4;
        fmVarArr[i7] = a4;
        int i8 = i7 + 1;
        _m_methods = fmVarArr;
        C1634Xv.m11725a((Object[]) ContractBaseInfo._m_methods, (Object[]) _m_methods);
        ___iScriptClass = aUO.m18566a(TransportBaseInfo.class, C6279ajr.class, _m_fields, _m_methods);
    }

    /* renamed from: a */
    private void m32110a(BagItemType ato) {
        bFf().mo5608dq().mo3197f(f7677OL, ato);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "BagType")
    @C0064Am(aul = "7f422f8227cba3680159d4c0e420501a", aum = 0)
    @C5566aOg
    /* renamed from: b */
    private void m32111b(BagItemType ato) {
        throw new aWi(new aCE(this, f7680OO, new Object[]{ato}));
    }

    /* renamed from: bd */
    private void m32112bd(long j) {
        bFf().mo5608dq().mo3184b(f7679ON, j);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "ExpiringTime (seconds)")
    @C0064Am(aul = "e6311a7feca937d0514b60732bd855c7", aum = 0)
    @C5566aOg
    /* renamed from: be */
    private void m32113be(long j) {
        throw new aWi(new aCE(this, f7682OQ, new Object[]{new Long(j)}));
    }

    /* renamed from: uJ */
    private BagItemType m32114uJ() {
        return (BagItemType) bFf().mo5608dq().mo3214p(f7677OL);
    }

    /* renamed from: uK */
    private long m32115uK() {
        return bFf().mo5608dq().mo3213o(f7679ON);
    }

    /* renamed from: T */
    public C6494any mo11T() {
        return ___iScriptClass;
    }

    /* renamed from: U */
    public C2491fm[] mo12U() {
        return _m_methods;
    }

    /* renamed from: W */
    public Object mo13W() {
        return new C6279ajr(this);
    }

    /* renamed from: a */
    public Object mo14a(C0495Gr gr) {
        Object[] args = gr.getArgs();
        switch (gr.mo2417hq() - ContractBaseInfo._m_methodCount) {
            case 0:
                m32111b((BagItemType) args[0]);
                return null;
            case 1:
                return m32116uL();
            case 2:
                m32113be(((Long) args[0]).longValue());
                return null;
            case 3:
                return new Long(m32117uN());
            default:
                return super.mo14a(gr);
        }
    }

    /* renamed from: a */
    public void mo15a(Collection collection, C0495Gr gr) {
        super.mo15a(collection, gr);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "ExpiringTime (seconds)")
    @C5566aOg
    /* renamed from: bf */
    public void mo19035bf(long j) {
        switch (bFf().mo6893i(f7682OQ)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f7682OQ, new Object[]{new Long(j)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f7682OQ, new Object[]{new Long(j)}));
                break;
        }
        m32113be(j);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "BagType")
    @C5566aOg
    /* renamed from: c */
    public void mo19036c(BagItemType ato) {
        switch (bFf().mo6893i(f7680OO)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f7680OO, new Object[]{ato}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f7680OO, new Object[]{ato}));
                break;
        }
        m32111b(ato);
    }

    /* renamed from: c */
    public C5663aRz[] mo25c() {
        return _m_fields;
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "BagType")
    /* renamed from: uM */
    public BagItemType mo19037uM() {
        switch (bFf().mo6893i(f7681OP)) {
            case 0:
                return null;
            case 2:
                return (BagItemType) bFf().mo5606d(new aCE(this, f7681OP, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f7681OP, new Object[0]));
                break;
        }
        return m32116uL();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "ExpiringTime (seconds)")
    /* renamed from: uO */
    public long mo19038uO() {
        switch (bFf().mo6893i(f7683OS)) {
            case 0:
                return 0;
            case 2:
                return ((Long) bFf().mo5606d(new aCE(this, f7683OS, new Object[0]))).longValue();
            case 3:
                bFf().mo5606d(new aCE(this, f7683OS, new Object[0]));
                break;
        }
        return m32117uN();
    }

    /* renamed from: S */
    public void mo10S() {
        super.mo10S();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "BagType")
    @C0064Am(aul = "a3945d06d002ac8b25c24f473ce29005", aum = 0)
    /* renamed from: uL */
    private BagItemType m32116uL() {
        return m32114uJ();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "ExpiringTime (seconds)")
    @C0064Am(aul = "d1a4357f09d0a12a256d50542e628d5a", aum = 0)
    /* renamed from: uN */
    private long m32117uN() {
        return m32115uK();
    }
}
