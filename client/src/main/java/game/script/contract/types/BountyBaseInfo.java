package game.script.contract.types;

import game.network.message.externalizable.aCE;
import logic.baa.C1616Xf;
import logic.baa.C1634Xv;
import logic.baa.C5511aMd;
import logic.baa.C6485anp;
import logic.data.mbean.C1548Wi;
import logic.res.code.C5640aRc;
import logic.res.code.C5663aRz;
import logic.res.code.C6494any;
import logic.res.html.C2491fm;
import p001a.*;

import java.util.Collection;

@C5511aMd
@C6485anp
/* renamed from: a.aNe  reason: case insensitive filesystem */
/* compiled from: a */
public class BountyBaseInfo extends ContractBaseInfo implements C1616Xf {

    /* renamed from: OQ */
    public static final C2491fm f3434OQ = null;

    /* renamed from: OS */
    public static final C2491fm f3435OS = null;
    public static final int _m_fieldCount = 0;
    public static final C5663aRz[] _m_fields = null;
    public static final int _m_methodCount = 0;
    public static final C2491fm[] _m_methods = null;
    public static final C5663aRz irr = null;
    public static final long serialVersionUID = 0;
    public static C6494any ___iScriptClass;
    @C0064Am(aul = "31aa37666fc7db14caeb1030f2438a07", aum = 0)
    private static long exe;

    static {
        m16738V();
    }

    public BountyBaseInfo() {
        super((C5540aNg) null);
        super.mo10S();
    }

    public BountyBaseInfo(C5540aNg ang) {
        super(ang);
    }

    /* renamed from: V */
    static void m16738V() {
        serialVersionUID = (long) 1;
        _m_fieldCount = ContractBaseInfo._m_fieldCount + 1;
        _m_methodCount = ContractBaseInfo._m_methodCount + 2;
        int i = ContractBaseInfo._m_fieldCount;
        C5663aRz[] arzArr = new C5663aRz[(i + 1)];
        C5663aRz b = C5640aRc.m17844b(BountyBaseInfo.class, "31aa37666fc7db14caeb1030f2438a07", i);
        irr = b;
        arzArr[i] = b;
        int i2 = i + 1;
        _m_fields = arzArr;
        C1634Xv.m11725a((Object[]) ContractBaseInfo._m_fields, (Object[]) _m_fields);
        int i3 = ContractBaseInfo._m_methodCount;
        C2491fm[] fmVarArr = new C2491fm[(i3 + 2)];
        C2491fm a = C4105zY.m41624a(BountyBaseInfo.class, "acc59ab5df12db34ce01ebf794d59ab8", i3);
        f3434OQ = a;
        fmVarArr[i3] = a;
        int i4 = i3 + 1;
        C2491fm a2 = C4105zY.m41624a(BountyBaseInfo.class, "c12f3ad6a88c856346379231e19d0dff", i4);
        f3435OS = a2;
        fmVarArr[i4] = a2;
        int i5 = i4 + 1;
        _m_methods = fmVarArr;
        C1634Xv.m11725a((Object[]) ContractBaseInfo._m_methods, (Object[]) _m_methods);
        ___iScriptClass = aUO.m18566a(BountyBaseInfo.class, C1548Wi.class, _m_fields, _m_methods);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "ExpiringTime (seconds)")
    @C0064Am(aul = "acc59ab5df12db34ce01ebf794d59ab8", aum = 0)
    @C5566aOg
    /* renamed from: be */
    private void m16739be(long j) {
        throw new aWi(new aCE(this, f3434OQ, new Object[]{new Long(j)}));
    }

    private long djK() {
        return bFf().mo5608dq().mo3213o(irr);
    }

    /* renamed from: li */
    private void m16740li(long j) {
        bFf().mo5608dq().mo3184b(irr, j);
    }

    /* renamed from: T */
    public C6494any mo11T() {
        return ___iScriptClass;
    }

    /* renamed from: U */
    public C2491fm[] mo12U() {
        return _m_methods;
    }

    /* renamed from: W */
    public Object mo13W() {
        return new C1548Wi(this);
    }

    /* renamed from: a */
    public Object mo14a(C0495Gr gr) {
        Object[] args = gr.getArgs();
        switch (gr.mo2417hq() - ContractBaseInfo._m_methodCount) {
            case 0:
                m16739be(((Long) args[0]).longValue());
                return null;
            case 1:
                return new Long(m16741uN());
            default:
                return super.mo14a(gr);
        }
    }

    /* renamed from: a */
    public void mo15a(Collection collection, C0495Gr gr) {
        super.mo15a(collection, gr);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "ExpiringTime (seconds)")
    @C5566aOg
    /* renamed from: bf */
    public void mo10500bf(long j) {
        switch (bFf().mo6893i(f3434OQ)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f3434OQ, new Object[]{new Long(j)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f3434OQ, new Object[]{new Long(j)}));
                break;
        }
        m16739be(j);
    }

    /* renamed from: c */
    public C5663aRz[] mo25c() {
        return _m_fields;
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "ExpiringTime (seconds)")
    /* renamed from: uO */
    public long mo10501uO() {
        switch (bFf().mo6893i(f3435OS)) {
            case 0:
                return 0;
            case 2:
                return ((Long) bFf().mo5606d(new aCE(this, f3435OS, new Object[0]))).longValue();
            case 3:
                bFf().mo5606d(new aCE(this, f3435OS, new Object[0]));
                break;
        }
        return m16741uN();
    }

    /* renamed from: S */
    public void mo10S() {
        super.mo10S();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "ExpiringTime (seconds)")
    @C0064Am(aul = "c12f3ad6a88c856346379231e19d0dff", aum = 0)
    /* renamed from: uN */
    private long m16741uN() {
        return djK();
    }
}
