package game.script.contract.types;

import game.network.message.externalizable.aCE;
import game.script.Actor;
import game.script.item.Item;
import game.script.item.ItemLocation;
import game.script.ship.Station;
import logic.baa.C1616Xf;
import logic.baa.C1634Xv;
import logic.baa.C5511aMd;
import logic.baa.C6485anp;
import logic.data.mbean.C5200aAe;
import logic.res.code.C5663aRz;
import logic.res.code.C6494any;
import logic.res.html.C2491fm;
import p001a.*;

import java.util.Collection;

@C5511aMd
@C6485anp
/* renamed from: a.anD  reason: case insensitive filesystem */
/* compiled from: a */
public class TransportContract<T extends TransportTemplate> extends ContractAdapter<T> implements C1616Xf {
    public static final int _m_fieldCount = 0;
    public static final C5663aRz[] _m_fields = null;
    public static final int _m_methodCount = 0;
    public static final C2491fm[] _m_methods = null;
    public static final C2491fm aLR = null;
    public static final C2491fm aLT = null;
    public static final C2491fm aLU = null;
    public static final C2491fm aLV = null;
    public static final C2491fm aLW = null;
    public static final C2491fm aLX = null;
    public static final C2491fm aLY = null;
    public static final C2491fm byJ = null;
    private static final String gek = "transport_mission_timer";
    private static final long serialVersionUID = -5872444861942547452L;
    public static C6494any ___iScriptClass = null;

    static {
        m24041V();
    }

    public TransportContract() {
        super((C5540aNg) null);
        super.mo10S();
    }

    public TransportContract(C5540aNg ang) {
        super(ang);
    }

    /* renamed from: V */
    static void m24041V() {
        _m_fieldCount = ContractAdapter._m_fieldCount + 0;
        _m_methodCount = ContractAdapter._m_methodCount + 8;
        _m_fields = new C5663aRz[(ContractAdapter._m_fieldCount + 0)];
        C1634Xv.m11725a((Object[]) ContractAdapter._m_fields, (Object[]) _m_fields);
        int i = ContractAdapter._m_methodCount;
        C2491fm[] fmVarArr = new C2491fm[(i + 8)];
        C2491fm a = C4105zY.m41624a(TransportContract.class, "cb2892bc11d17b6cc5cc8618dcde7a70", i);
        byJ = a;
        fmVarArr[i] = a;
        int i2 = i + 1;
        C2491fm a2 = C4105zY.m41624a(TransportContract.class, "78979ee2c930e26597ede5e2831d8e4d", i2);
        aLR = a2;
        fmVarArr[i2] = a2;
        int i3 = i2 + 1;
        C2491fm a3 = C4105zY.m41624a(TransportContract.class, "5ce1c8f1d24b0d6f7950a5c35a3c1b4b", i3);
        aLT = a3;
        fmVarArr[i3] = a3;
        int i4 = i3 + 1;
        C2491fm a4 = C4105zY.m41624a(TransportContract.class, "0377d3d5b31bfec374800111d260f24f", i4);
        aLU = a4;
        fmVarArr[i4] = a4;
        int i5 = i4 + 1;
        C2491fm a5 = C4105zY.m41624a(TransportContract.class, "3820d63a43c88934a5b7e03548e430ca", i5);
        aLV = a5;
        fmVarArr[i5] = a5;
        int i6 = i5 + 1;
        C2491fm a6 = C4105zY.m41624a(TransportContract.class, "2715e64acc1e4a8f0fb94e4ae031ca2f", i6);
        aLW = a6;
        fmVarArr[i6] = a6;
        int i7 = i6 + 1;
        C2491fm a7 = C4105zY.m41624a(TransportContract.class, "edbfbee0747d96befcb828e130d31d2d", i7);
        aLX = a7;
        fmVarArr[i7] = a7;
        int i8 = i7 + 1;
        C2491fm a8 = C4105zY.m41624a(TransportContract.class, "5c0eb0c925b24b6919943a09661e3409", i8);
        aLY = a8;
        fmVarArr[i8] = a8;
        int i9 = i8 + 1;
        _m_methods = fmVarArr;
        C1634Xv.m11725a((Object[]) ContractAdapter._m_methods, (Object[]) _m_methods);
        ___iScriptClass = aUO.m18566a(TransportContract.class, C5200aAe.class, _m_fields, _m_methods);
    }

    @C0064Am(aul = "5ce1c8f1d24b0d6f7950a5c35a3c1b4b", aum = 0)
    @C5566aOg
    /* renamed from: RJ */
    private void m24038RJ() {
        throw new aWi(new aCE(this, aLT, new Object[0]));
    }

    @C0064Am(aul = "78979ee2c930e26597ede5e2831d8e4d", aum = 0)
    @C5566aOg
    /* renamed from: aV */
    private void m24044aV(String str) {
        throw new aWi(new aCE(this, aLR, new Object[]{str}));
    }

    @C0064Am(aul = "cb2892bc11d17b6cc5cc8618dcde7a70", aum = 0)
    @C5566aOg
    /* renamed from: p */
    private void m24046p(Station bf) {
        throw new aWi(new aCE(this, byJ, new Object[]{bf}));
    }

    @C5566aOg
    /* renamed from: RK */
    public void mo65RK() {
        switch (bFf().mo6893i(aLT)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, aLT, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, aLT, new Object[0]));
                break;
        }
        m24038RJ();
    }

    /* renamed from: RM */
    public void mo66RM() {
        switch (bFf().mo6893i(aLU)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, aLU, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, aLU, new Object[0]));
                break;
        }
        m24039RL();
    }

    /* renamed from: RO */
    public void mo67RO() {
        switch (bFf().mo6893i(aLX)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, aLX, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, aLX, new Object[0]));
                break;
        }
        m24040RN();
    }

    /* renamed from: T */
    public C6494any mo11T() {
        return ___iScriptClass;
    }

    /* renamed from: U */
    public C2491fm[] mo12U() {
        return _m_methods;
    }

    /* renamed from: W */
    public Object mo13W() {
        return new C5200aAe(this);
    }

    /* renamed from: a */
    public Object mo14a(C0495Gr gr) {
        Object[] args = gr.getArgs();
        switch (gr.mo2417hq() - ContractAdapter._m_methodCount) {
            case 0:
                m24046p((Station) args[0]);
                return null;
            case 1:
                m24044aV((String) args[0]);
                return null;
            case 2:
                m24038RJ();
                return null;
            case 3:
                m24039RL();
                return null;
            case 4:
                m24043a((Item) args[0], (ItemLocation) args[1], (ItemLocation) args[2]);
                return null;
            case 5:
                m24045c((Item) args[0], (ItemLocation) args[1]);
                return null;
            case 6:
                m24040RN();
                return null;
            case 7:
                m24042a((Actor) args[0], (C5260aCm) args[1]);
                return null;
            default:
                return super.mo14a(gr);
        }
    }

    /* renamed from: a */
    public void mo15a(Collection collection, C0495Gr gr) {
        super.mo15a(collection, gr);
    }

    @C5566aOg
    /* renamed from: aW */
    public void mo73aW(String str) {
        switch (bFf().mo6893i(aLR)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, aLR, new Object[]{str}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, aLR, new Object[]{str}));
                break;
        }
        m24044aV(str);
    }

    /* renamed from: b */
    public void mo85b(Actor cr, C5260aCm acm) {
        switch (bFf().mo6893i(aLY)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, aLY, new Object[]{cr, acm}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, aLY, new Object[]{cr, acm}));
                break;
        }
        m24042a(cr, acm);
    }

    /* renamed from: b */
    public void mo90b(Item auq, ItemLocation aag, ItemLocation aag2) {
        switch (bFf().mo6893i(aLV)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, aLV, new Object[]{auq, aag, aag2}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, aLV, new Object[]{auq, aag, aag2}));
                break;
        }
        m24043a(auq, aag, aag2);
    }

    /* renamed from: c */
    public C5663aRz[] mo25c() {
        return _m_fields;
    }

    /* renamed from: d */
    public void mo117d(Item auq, ItemLocation aag) {
        switch (bFf().mo6893i(aLW)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, aLW, new Object[]{auq, aag}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, aLW, new Object[]{auq, aag}));
                break;
        }
        m24045c(auq, aag);
    }

    @C5566aOg
    /* renamed from: q */
    public void mo133q(Station bf) {
        switch (bFf().mo6893i(byJ)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, byJ, new Object[]{bf}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, byJ, new Object[]{bf}));
                break;
        }
        m24046p(bf);
    }

    /* renamed from: S */
    public void mo10S() {
        super.mo10S();
    }

    @C0064Am(aul = "0377d3d5b31bfec374800111d260f24f", aum = 0)
    /* renamed from: RL */
    private void m24039RL() {
        super.mo66RM();
        akG();
    }

    @C0064Am(aul = "3820d63a43c88934a5b7e03548e430ca", aum = 0)
    /* renamed from: a */
    private void m24043a(Item auq, ItemLocation aag, ItemLocation aag2) {
    }

    @C0064Am(aul = "2715e64acc1e4a8f0fb94e4ae031ca2f", aum = 0)
    /* renamed from: c */
    private void m24045c(Item auq, ItemLocation aag) {
    }

    @C0064Am(aul = "edbfbee0747d96befcb828e130d31d2d", aum = 0)
    /* renamed from: RN */
    private void m24040RN() {
    }

    @C0064Am(aul = "5c0eb0c925b24b6919943a09661e3409", aum = 0)
    /* renamed from: a */
    private void m24042a(Actor cr, C5260aCm acm) {
    }
}
