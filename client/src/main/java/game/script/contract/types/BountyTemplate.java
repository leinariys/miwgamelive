package game.script.contract.types;

import game.network.message.externalizable.aCE;
import game.script.mission.Mission;
import game.script.player.Player;
import logic.baa.C1616Xf;
import logic.baa.C1634Xv;
import logic.baa.C5511aMd;
import logic.baa.C6485anp;
import logic.data.mbean.C2501ft;
import logic.res.code.C5640aRc;
import logic.res.code.C5663aRz;
import logic.res.code.C6494any;
import logic.res.html.C2491fm;
import p001a.*;
import taikodom.infra.script.I18NString;

import java.util.Collection;

@C5511aMd
@C6485anp
/* renamed from: a.aIf  reason: case insensitive filesystem */
/* compiled from: a */
public class BountyTemplate extends ContractTemplate implements C1616Xf, C2795kE {
    /* renamed from: MN */
    public static final C2491fm f3090MN = null;
    /* renamed from: QE */
    public static final C2491fm f3091QE = null;
    public static final int _m_fieldCount = 0;
    public static final C5663aRz[] _m_fields = null;
    public static final int _m_methodCount = 0;
    public static final C2491fm[] _m_methods = null;
    public static final C2491fm cpD = null;
    public static final C2491fm cpE = null;
    public static final C2491fm cpK = null;
    public static final C5663aRz dnf = null;
    public static final C2491fm dno = null;
    public static final C2491fm dnp = null;
    public static final C5663aRz iaT = null;
    public static final C2491fm iaU = null;
    public static final C2491fm iaV = null;
    public static final long serialVersionUID = 0;
    /* renamed from: xF */
    public static final C2491fm f3092xF = null;
    public static C6494any ___iScriptClass;
    @C0064Am(aul = "c71686bfe1dc212f340973dc9ca5aecc", aum = 0)

    /* renamed from: Kd */
    private static Player f3088Kd;
    @C0064Am(aul = "78b3916a948e8ec1b487dd4f0f14745c", aum = 1)

    /* renamed from: Ke */
    private static BountyContract<BountyTemplate> f3089Ke;
    private static /* synthetic */ int[] dng;

    static {
        m15432V();
    }

    public BountyTemplate() {
        super((C5540aNg) null);
        super.mo10S();
    }

    public BountyTemplate(C5540aNg ang) {
        super(ang);
    }

    /* renamed from: V */
    static void m15432V() {
        serialVersionUID = (long) 1;
        _m_fieldCount = ContractTemplate._m_fieldCount + 2;
        _m_methodCount = ContractTemplate._m_methodCount + 10;
        int i = ContractTemplate._m_fieldCount;
        C5663aRz[] arzArr = new C5663aRz[(i + 2)];
        C5663aRz b = C5640aRc.m17844b(BountyTemplate.class, "c71686bfe1dc212f340973dc9ca5aecc", i);
        iaT = b;
        arzArr[i] = b;
        int i2 = i + 1;
        C5663aRz b2 = C5640aRc.m17844b(BountyTemplate.class, "78b3916a948e8ec1b487dd4f0f14745c", i2);
        dnf = b2;
        arzArr[i2] = b2;
        int i3 = i2 + 1;
        _m_fields = arzArr;
        C1634Xv.m11725a((Object[]) ContractTemplate._m_fields, (Object[]) _m_fields);
        int i4 = ContractTemplate._m_methodCount;
        C2491fm[] fmVarArr = new C2491fm[(i4 + 10)];
        C2491fm a = C4105zY.m41624a(BountyTemplate.class, "4406ac7aa3ebb8d5b781127ea31e541e", i4);
        iaU = a;
        fmVarArr[i4] = a;
        int i5 = i4 + 1;
        C2491fm a2 = C4105zY.m41624a(BountyTemplate.class, "14a2282555c4a1669a4dcaf45a62c405", i5);
        iaV = a2;
        fmVarArr[i5] = a2;
        int i6 = i5 + 1;
        C2491fm a3 = C4105zY.m41624a(BountyTemplate.class, "2306334d0d96488066b13cc61baa1679", i6);
        cpK = a3;
        fmVarArr[i6] = a3;
        int i7 = i6 + 1;
        C2491fm a4 = C4105zY.m41624a(BountyTemplate.class, "e274bbdf952d2f30aace8aa023776e7d", i7);
        f3092xF = a4;
        fmVarArr[i7] = a4;
        int i8 = i7 + 1;
        C2491fm a5 = C4105zY.m41624a(BountyTemplate.class, "53b44682e4d59575bb8d736e5b06fb16", i8);
        cpD = a5;
        fmVarArr[i8] = a5;
        int i9 = i8 + 1;
        C2491fm a6 = C4105zY.m41624a(BountyTemplate.class, "bf5a3a952164705265a594cdba3c333e", i9);
        f3090MN = a6;
        fmVarArr[i9] = a6;
        int i10 = i9 + 1;
        C2491fm a7 = C4105zY.m41624a(BountyTemplate.class, "f3b9bb710bc847186115333766f958ff", i10);
        f3091QE = a7;
        fmVarArr[i10] = a7;
        int i11 = i10 + 1;
        C2491fm a8 = C4105zY.m41624a(BountyTemplate.class, "d2a376f07668c2e855099b801acd5b1b", i11);
        dno = a8;
        fmVarArr[i11] = a8;
        int i12 = i11 + 1;
        C2491fm a9 = C4105zY.m41624a(BountyTemplate.class, "f6280e502901f835ed19f01f5d6f4aa1", i12);
        dnp = a9;
        fmVarArr[i12] = a9;
        int i13 = i12 + 1;
        C2491fm a10 = C4105zY.m41624a(BountyTemplate.class, "606147fe1afeacbd0f3dedebe9530cbe", i13);
        cpE = a10;
        fmVarArr[i13] = a10;
        int i14 = i13 + 1;
        _m_methods = fmVarArr;
        C1634Xv.m11725a((Object[]) ContractTemplate._m_methods, (Object[]) _m_methods);
        ___iScriptClass = aUO.m18566a(BountyTemplate.class, C2501ft.class, _m_fields, _m_methods);
    }

    static /* synthetic */ int[] baP() {
        int[] iArr = dng;
        if (iArr == null) {
            iArr = new int[ContractTemplate.C0104a.values().length];
            try {
                iArr[ContractTemplate.C0104a.AVAILABLE.ordinal()] = 1;
            } catch (NoSuchFieldError e) {
            }
            try {
                iArr[ContractTemplate.C0104a.DEAD.ordinal()] = 3;
            } catch (NoSuchFieldError e2) {
            }
            try {
                iArr[ContractTemplate.C0104a.TAKEN.ordinal()] = 2;
            } catch (NoSuchFieldError e3) {
            }
            dng = iArr;
        }
        return iArr;
    }

    /* renamed from: a */
    private void m15433a(BountyContract nSVar) {
        bFf().mo5608dq().mo3197f(dnf, nSVar);
    }

    @C0064Am(aul = "53b44682e4d59575bb8d736e5b06fb16", aum = 0)
    @C5566aOg
    private void aAn() {
        throw new aWi(new aCE(this, cpD, new Object[0]));
    }

    /* renamed from: cw */
    private void m15435cw(Player aku) {
        bFf().mo5608dq().mo3197f(iaT, aku);
    }

    private Player dee() {
        return (Player) bFf().mo5608dq().mo3214p(iaT);
    }

    private BountyContract def() {
        return (BountyContract) bFf().mo5608dq().mo3214p(dnf);
    }

    @C0064Am(aul = "e274bbdf952d2f30aace8aa023776e7d", aum = 0)
    @C5566aOg
    /* renamed from: iR */
    private Mission m15437iR() {
        throw new aWi(new aCE(this, f3092xF, new Object[0]));
    }

    /* renamed from: KG */
    public Player mo9371KG() {
        switch (bFf().mo6893i(iaV)) {
            case 0:
                return null;
            case 2:
                return (Player) bFf().mo5606d(new aCE(this, iaV, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, iaV, new Object[0]));
                break;
        }
        return deg();
    }

    /* renamed from: T */
    public C6494any mo11T() {
        return ___iScriptClass;
    }

    /* renamed from: U */
    public C2491fm[] mo12U() {
        return _m_methods;
    }

    /* renamed from: W */
    public Object mo13W() {
        return new C2501ft(this);
    }

    /* renamed from: a */
    public Object mo14a(C0495Gr gr) {
        Object[] args = gr.getArgs();
        switch (gr.mo2417hq() - ContractTemplate._m_methodCount) {
            case 0:
                m15436cx((Player) args[0]);
                return null;
            case 1:
                return deg();
            case 2:
                return aAt();
            case 3:
                return m15437iR();
            case 4:
                aAn();
                return null;
            case 5:
                return m15438rO();
            case 6:
                return m15439vV();
            case 7:
                return baL();
            case 8:
                return baN();
            case 9:
                return new Boolean(m15434aB((Player) args[0]));
            default:
                return super.mo14a(gr);
        }
    }

    /* renamed from: a */
    public void mo15a(Collection collection, C0495Gr gr) {
        super.mo15a(collection, gr);
    }

    @C5566aOg
    public void aAo() {
        switch (bFf().mo6893i(cpD)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, cpD, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, cpD, new Object[0]));
                break;
        }
        aAn();
    }

    public ContractAdapter aAu() {
        switch (bFf().mo6893i(cpK)) {
            case 0:
                return null;
            case 2:
                return (ContractAdapter) bFf().mo5606d(new aCE(this, cpK, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, cpK, new Object[0]));
                break;
        }
        return aAt();
    }

    /* renamed from: aC */
    public boolean mo687aC(Player aku) {
        switch (bFf().mo6893i(cpE)) {
            case 0:
                return false;
            case 2:
                return ((Boolean) bFf().mo5606d(new aCE(this, cpE, new Object[]{aku}))).booleanValue();
            case 3:
                bFf().mo5606d(new aCE(this, cpE, new Object[]{aku}));
                break;
        }
        return m15434aB(aku);
    }

    public String baM() {
        switch (bFf().mo6893i(dno)) {
            case 0:
                return null;
            case 2:
                return (String) bFf().mo5606d(new aCE(this, dno, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, dno, new Object[0]));
                break;
        }
        return baL();
    }

    public String baO() {
        switch (bFf().mo6893i(dnp)) {
            case 0:
                return null;
            case 2:
                return (String) bFf().mo5606d(new aCE(this, dnp, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, dnp, new Object[0]));
                break;
        }
        return baN();
    }

    /* renamed from: c */
    public C5663aRz[] mo25c() {
        return _m_fields;
    }

    /* renamed from: cy */
    public void mo9374cy(Player aku) {
        switch (bFf().mo6893i(iaU)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, iaU, new Object[]{aku}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, iaU, new Object[]{aku}));
                break;
        }
        m15436cx(aku);
    }

    @C5566aOg
    /* renamed from: iS */
    public Mission mo1716iS() {
        switch (bFf().mo6893i(f3092xF)) {
            case 0:
                return null;
            case 2:
                return (Mission) bFf().mo5606d(new aCE(this, f3092xF, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f3092xF, new Object[0]));
                break;
        }
        return m15437iR();
    }

    /* renamed from: rP */
    public I18NString mo711rP() {
        switch (bFf().mo6893i(f3090MN)) {
            case 0:
                return null;
            case 2:
                return (I18NString) bFf().mo5606d(new aCE(this, f3090MN, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f3090MN, new Object[0]));
                break;
        }
        return m15438rO();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Description")
    /* renamed from: vW */
    public I18NString mo713vW() {
        switch (bFf().mo6893i(f3091QE)) {
            case 0:
                return null;
            case 2:
                return (I18NString) bFf().mo5606d(new aCE(this, f3091QE, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f3091QE, new Object[0]));
                break;
        }
        return m15439vV();
    }

    @C0064Am(aul = "4406ac7aa3ebb8d5b781127ea31e541e", aum = 0)
    /* renamed from: cx */
    private void m15436cx(Player aku) {
        m15435cw(aku);
    }

    @C0064Am(aul = "14a2282555c4a1669a4dcaf45a62c405", aum = 0)
    private Player deg() {
        return dee();
    }

    /* renamed from: S */
    public void mo10S() {
        super.mo10S();
    }

    @C0064Am(aul = "2306334d0d96488066b13cc61baa1679", aum = 0)
    private ContractAdapter aAt() {
        return def();
    }

    @C0064Am(aul = "bf5a3a952164705265a594cdba3c333e", aum = 0)
    /* renamed from: rO */
    private I18NString m15438rO() {
        return new I18NString(String.valueOf(super.mo711rP().get()) + ": " + dee().getName());
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Description")
    @C0064Am(aul = "f3b9bb710bc847186115333766f958ff", aum = 0)
    /* renamed from: vV */
    private I18NString m15439vV() {
        return new I18NString(aAj().mo22207vW().get().replaceAll("\\{0\\}", aAw().getName()).replaceAll("\\{1\\}", Long.toString(aAA())).replaceAll("\\{2\\}", Long.toString(aAy().aqL())).replaceAll("\\{3\\}", mo9371KG().getName()));
    }

    @C0064Am(aul = "d2a376f07668c2e855099b801acd5b1b", aum = 0)
    private String baL() {
        return null;
    }

    @C0064Am(aul = "f6280e502901f835ed19f01f5d6f4aa1", aum = 0)
    private String baN() {
        return null;
    }

    @C0064Am(aul = "606147fe1afeacbd0f3dedebe9530cbe", aum = 0)
    /* renamed from: aB */
    private boolean m15434aB(Player aku) {
        if (aku == mo9371KG()) {
            return false;
        }
        return super.mo687aC(aku);
    }
}
