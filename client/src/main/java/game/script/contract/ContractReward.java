package game.script.contract;

import game.network.message.externalizable.aCE;
import game.network.message.serializable.C5857abl;
import game.script.item.Item;
import game.script.item.ItemLocation;
import logic.baa.C1616Xf;
import logic.baa.C1634Xv;
import logic.baa.C5511aMd;
import logic.baa.C6485anp;
import logic.data.mbean.C3226pU;
import logic.res.code.C5640aRc;
import logic.res.code.C5663aRz;
import logic.res.code.C6494any;
import logic.res.html.C2491fm;
import p001a.*;

import java.util.Collection;

@C5511aMd
@C6485anp
/* renamed from: a.OS */
/* compiled from: a */
public class ContractReward extends ItemLocation implements C1616Xf, C5857abl {
    /* renamed from: RA */
    public static final C2491fm f1316RA = null;
    /* renamed from: RD */
    public static final C2491fm f1317RD = null;
    public static final int _m_fieldCount = 0;
    public static final C5663aRz[] _m_fields = null;
    public static final int _m_methodCount = 0;
    public static final C2491fm[] _m_methods = null;
    public static final C2491fm cpW = null;
    public static final C5663aRz dPh = null;
    public static final C2491fm dPi = null;
    /* renamed from: ku */
    public static final C2491fm f1318ku = null;
    public static final long serialVersionUID = 0;
    public static C6494any ___iScriptClass;
    @C0064Am(aul = "79fdc5cebbd2f89ce70c02e4b976eb5e", aum = 0)

    /* renamed from: E */
    private static long f1315E;

    static {
        m8066V();
    }

    public ContractReward() {
        super((C5540aNg) null);
        super.mo10S();
    }

    public ContractReward(C5540aNg ang) {
        super(ang);
    }

    public ContractReward(C5857abl abl) {
        super((C5540aNg) null);
        super._m_script_init(abl);
    }

    /* renamed from: V */
    static void m8066V() {
        serialVersionUID = (long) 1;
        _m_fieldCount = ItemLocation._m_fieldCount + 1;
        _m_methodCount = ItemLocation._m_methodCount + 5;
        int i = ItemLocation._m_fieldCount;
        C5663aRz[] arzArr = new C5663aRz[(i + 1)];
        C5663aRz b = C5640aRc.m17844b(ContractReward.class, "79fdc5cebbd2f89ce70c02e4b976eb5e", i);
        dPh = b;
        arzArr[i] = b;
        int i2 = i + 1;
        _m_fields = arzArr;
        C1634Xv.m11725a((Object[]) ItemLocation._m_fields, (Object[]) _m_fields);
        int i3 = ItemLocation._m_methodCount;
        C2491fm[] fmVarArr = new C2491fm[(i3 + 5)];
        C2491fm a = C4105zY.m41624a(ContractReward.class, "25d879e682ac9138ba9f2bc511fcc618", i3);
        cpW = a;
        fmVarArr[i3] = a;
        int i4 = i3 + 1;
        C2491fm a2 = C4105zY.m41624a(ContractReward.class, "50645620b1d9f0713606b61c6acb77e8", i4);
        dPi = a2;
        fmVarArr[i4] = a2;
        int i5 = i4 + 1;
        C2491fm a3 = C4105zY.m41624a(ContractReward.class, "b7c22e154fccab6d5b88dddaa1779a62", i5);
        f1316RA = a3;
        fmVarArr[i5] = a3;
        int i6 = i5 + 1;
        C2491fm a4 = C4105zY.m41624a(ContractReward.class, "6531db3237315a7fb4e451a65687e98e", i6);
        f1317RD = a4;
        fmVarArr[i6] = a4;
        int i7 = i6 + 1;
        C2491fm a5 = C4105zY.m41624a(ContractReward.class, "9978c052ab25c3359d64e0fe55a9087e", i7);
        f1318ku = a5;
        fmVarArr[i7] = a5;
        int i8 = i7 + 1;
        _m_methods = fmVarArr;
        C1634Xv.m11725a((Object[]) ItemLocation._m_methods, (Object[]) _m_methods);
        ___iScriptClass = aUO.m18566a(ContractReward.class, C3226pU.class, _m_fields, _m_methods);
    }

    private long bnb() {
        return bFf().mo5608dq().mo3213o(dPh);
    }

    /* renamed from: fo */
    private void m8068fo(long j) {
        bFf().mo5608dq().mo3184b(dPh, j);
    }

    @C0064Am(aul = "50645620b1d9f0713606b61c6acb77e8", aum = 0)
    @C5566aOg
    /* renamed from: fp */
    private void m8069fp(long j) {
        throw new aWi(new aCE(this, dPi, new Object[]{new Long(j)}));
    }

    /* renamed from: T */
    public C6494any mo11T() {
        return ___iScriptClass;
    }

    /* renamed from: U */
    public C2491fm[] mo12U() {
        return _m_methods;
    }

    /* renamed from: W */
    public Object mo13W() {
        return new C3226pU(this);
    }

    /* renamed from: a */
    public Object mo14a(C0495Gr gr) {
        Object[] args = gr.getArgs();
        switch (gr.mo2417hq() - ItemLocation._m_methodCount) {
            case 0:
                return new Long(aAK());
            case 1:
                m8069fp(((Long) args[0]).longValue());
                return null;
            case 2:
                m8067a((Item) args[0], (ItemLocation) args[1]);
                return null;
            case 3:
                m8071m((Item) args[0]);
                return null;
            case 4:
                m8070g((Item) args[0]);
                return null;
            default:
                return super.mo14a(gr);
        }
    }

    /* renamed from: a */
    public void mo15a(Collection collection, C0495Gr gr) {
        super.mo15a(collection, gr);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "MoneyReward")
    public long aqL() {
        switch (bFf().mo6893i(cpW)) {
            case 0:
                return 0;
            case 2:
                return ((Long) bFf().mo5606d(new aCE(this, cpW, new Object[0]))).longValue();
            case 3:
                bFf().mo5606d(new aCE(this, cpW, new Object[0]));
                break;
        }
        return aAK();
    }

    /* access modifiers changed from: protected */
    /* renamed from: b */
    public void mo1887b(Item auq, ItemLocation aag) {
        switch (bFf().mo6893i(f1316RA)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f1316RA, new Object[]{auq, aag}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f1316RA, new Object[]{auq, aag}));
                break;
        }
        m8067a(auq, aag);
    }

    /* renamed from: c */
    public C5663aRz[] mo25c() {
        return _m_fields;
    }

    @C5566aOg
    /* renamed from: fq */
    public void mo4437fq(long j) {
        switch (bFf().mo6893i(dPi)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, dPi, new Object[]{new Long(j)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, dPi, new Object[]{new Long(j)}));
                break;
        }
        m8069fp(j);
    }

    /* access modifiers changed from: protected */
    /* renamed from: h */
    public void mo1889h(Item auq) {
        switch (bFf().mo6893i(f1318ku)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f1318ku, new Object[]{auq}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f1318ku, new Object[]{auq}));
                break;
        }
        m8070g(auq);
    }

    /* access modifiers changed from: protected */
    /* renamed from: n */
    public void mo1890n(Item auq) {
        switch (bFf().mo6893i(f1317RD)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f1317RD, new Object[]{auq}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f1317RD, new Object[]{auq}));
                break;
        }
        m8071m(auq);
    }

    /* renamed from: S */
    public void mo10S() {
        super.mo10S();
    }

    @C5566aOg
    /* renamed from: c */
    public void mo4436c(C5857abl abl) {
        super.mo10S();
        m8068fo(abl.aqL());
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "MoneyReward")
    @C0064Am(aul = "25d879e682ac9138ba9f2bc511fcc618", aum = 0)
    private long aAK() {
        return bnb();
    }

    @C0064Am(aul = "b7c22e154fccab6d5b88dddaa1779a62", aum = 0)
    /* renamed from: a */
    private void m8067a(Item auq, ItemLocation aag) {
    }

    @C0064Am(aul = "6531db3237315a7fb4e451a65687e98e", aum = 0)
    /* renamed from: m */
    private void m8071m(Item auq) {
    }

    @C0064Am(aul = "9978c052ab25c3359d64e0fe55a9087e", aum = 0)
    /* renamed from: g */
    private void m8070g(Item auq) {
    }
}
