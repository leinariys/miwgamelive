package game.script;

import com.hoplon.geometry.Vec3f;
import game.network.message.externalizable.aCE;
import logic.baa.*;
import logic.data.link.C3161oY;
import logic.data.link.C3396rD;
import logic.data.mbean.C6157ahZ;
import logic.res.code.C5640aRc;
import logic.res.code.C5663aRz;
import logic.res.code.C6494any;
import logic.res.html.C2491fm;
import p001a.*;

import java.util.Collection;

@C5511aMd
@C6485anp
/* renamed from: a.uI */
/* compiled from: a */
public class AttachedTrigger extends Trigger implements C1616Xf, C3161oY.C3162a, C3396rD.C3397a, C3396rD.C3398b {

    /* renamed from: CB */
    public static final C2491fm f9340CB = null;
    public static final C2491fm _f_dispose_0020_0028_0029V = null;
    /* renamed from: _f_onObjectDisposed_0020_0028Ltaikodom_002finfra_002fscript_002fScriptObject_003b_0029V */
    public static final C2491fm f9341x13860637 = null;
    public static final int _m_fieldCount = 0;
    public static final C5663aRz[] _m_fields = null;
    public static final int _m_methodCount = 0;
    public static final C2491fm[] _m_methods = null;
    public static final C2491fm bwA = null;
    public static final C2491fm bwB = null;
    public static final C5663aRz bwz = null;

    public static C6494any ___iScriptClass = null;
    @C0064Am(aul = "0ec532cff956541cc44b1c68368cd2a5", aum = 0)
    private static Vec3f bwy = null;

    static {
        m39916V();
    }

    public AttachedTrigger() {
        super((C5540aNg) null);
        super.mo10S();
    }

    public AttachedTrigger(Actor cr, Vec3f vec3f) {
        super((C5540aNg) null);
        super._m_script_init(cr, vec3f);
    }

    public AttachedTrigger(C5540aNg ang) {
        super(ang);
    }

    /* renamed from: V */
    static void m39916V() {
        _m_fieldCount = Trigger._m_fieldCount + 1;
        _m_methodCount = Trigger._m_methodCount + 5;
        int i = Trigger._m_fieldCount;
        C5663aRz[] arzArr = new C5663aRz[(i + 1)];
        C5663aRz b = C5640aRc.m17844b(AttachedTrigger.class, "0ec532cff956541cc44b1c68368cd2a5", i);
        bwz = b;
        arzArr[i] = b;
        int i2 = i + 1;
        _m_fields = arzArr;
        C1634Xv.m11725a((Object[]) Trigger._m_fields, (Object[]) _m_fields);
        int i3 = Trigger._m_methodCount;
        C2491fm[] fmVarArr = new C2491fm[(i3 + 5)];
        C2491fm a = C4105zY.m41624a(AttachedTrigger.class, "e047b1abdcb03eda4c3b5011b030cdee", i3);
        bwA = a;
        fmVarArr[i3] = a;
        int i4 = i3 + 1;
        C2491fm a2 = C4105zY.m41624a(AttachedTrigger.class, "46efe769788df192f74e36954b001ebb", i4);
        f9340CB = a2;
        fmVarArr[i4] = a2;
        int i5 = i4 + 1;
        C2491fm a3 = C4105zY.m41624a(AttachedTrigger.class, "294913b380bf6b4c3d7d7f2000efa804", i5);
        bwB = a3;
        fmVarArr[i5] = a3;
        int i6 = i5 + 1;
        C2491fm a4 = C4105zY.m41624a(AttachedTrigger.class, "1ff8c2a9c52cd06859838d71638eec10", i6);
        f9341x13860637 = a4;
        fmVarArr[i6] = a4;
        int i7 = i6 + 1;
        C2491fm a5 = C4105zY.m41624a(AttachedTrigger.class, "f824bca7b17bae00a164c19b140e5886", i7);
        _f_dispose_0020_0028_0029V = a5;
        fmVarArr[i7] = a5;
        int i8 = i7 + 1;
        _m_methods = fmVarArr;
        C1634Xv.m11725a((Object[]) Trigger._m_methods, (Object[]) _m_methods);
        ___iScriptClass = aUO.m18566a(AttachedTrigger.class, C6157ahZ.class, _m_fields, _m_methods);
    }

    /* renamed from: F */
    private void m39915F(Vec3f vec3f) {
        bFf().mo5608dq().mo3197f(bwz, vec3f);
    }

    private Vec3f ajy() {
        return (Vec3f) bFf().mo5608dq().mo3214p(bwz);
    }

    /* renamed from: S */
    public void mo10S() {
        super.mo10S();
    }

    /* renamed from: T */
    public C6494any mo11T() {
        return ___iScriptClass;
    }

    /* renamed from: U */
    public C2491fm[] mo12U() {
        return _m_methods;
    }

    /* renamed from: W */
    public Object mo13W() {
        return new C6157ahZ(this);
    }

    /* renamed from: a */
    public Object mo14a(C0495Gr gr) {
        Object[] args = gr.getArgs();
        switch (gr.mo2417hq() - Trigger._m_methodCount) {
            case 0:
                return ajz();
            case 1:
                m39920l((Actor) args[0]);
                return null;
            case 2:
                m39917ag((Actor) args[0]);
                return null;
            case 3:
                m39918b((aDJ) args[0]);
                return null;
            case 4:
                m39919fg();
                return null;
            default:
                return super.mo14a(gr);
        }
    }

    /* renamed from: a */
    public void mo15a(Collection collection, C0495Gr gr) {
        super.mo15a(collection, gr);
    }

    /* renamed from: ah */
    public void mo21556ah(Actor cr) {
        switch (bFf().mo6893i(bwB)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, bwB, new Object[]{cr}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, bwB, new Object[]{cr}));
                break;
        }
        m39917ag(cr);
    }

    public Vec3f ajA() {
        switch (bFf().mo6893i(bwA)) {
            case 0:
                return null;
            case 2:
                return (Vec3f) bFf().mo5606d(new aCE(this, bwA, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, bwA, new Object[0]));
                break;
        }
        return ajz();
    }

    /* renamed from: c */
    public void mo98c(aDJ adj) {
        switch (bFf().mo6893i(f9341x13860637)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f9341x13860637, new Object[]{adj}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f9341x13860637, new Object[]{adj}));
                break;
        }
        m39918b(adj);
    }

    /* renamed from: c */
    public C5663aRz[] mo25c() {
        return _m_fields;
    }

    public void dispose() {
        switch (bFf().mo6893i(_f_dispose_0020_0028_0029V)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, _f_dispose_0020_0028_0029V, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, _f_dispose_0020_0028_0029V, new Object[0]));
                break;
        }
        m39919fg();
    }

    /* renamed from: m */
    public void mo18227m(Actor cr) {
        switch (bFf().mo6893i(f9340CB)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f9340CB, new Object[]{cr}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f9340CB, new Object[]{cr}));
                break;
        }
        m39920l(cr);
    }

    /* renamed from: a */
    public void mo22342a(Actor cr, Vec3f vec3f) {
        super.mo10S();
        m39915F(vec3f);
        setPosition(cr.cLl().mo9531q(cr.getOrientation().mo15209aT(vec3f)));
        mo1003bD(cr);
        cr.mo8348d(C3396rD.C3398b.class, this);
        cr.mo8348d(C3396rD.C3397a.class, this);
        cr.mo8348d(C3161oY.C3162a.class, this);
    }

    @C0064Am(aul = "e047b1abdcb03eda4c3b5011b030cdee", aum = 0)
    private Vec3f ajz() {
        return ajy();
    }

    @C0064Am(aul = "46efe769788df192f74e36954b001ebb", aum = 0)
    /* renamed from: l */
    private void m39920l(Actor cr) {
        if (cr == cKZ() && bae()) {
            mo1099zx();
        }
    }

    @C0064Am(aul = "294913b380bf6b4c3d7d7f2000efa804", aum = 0)
    /* renamed from: ag */
    private void m39917ag(Actor cr) {
        if (cr == cKZ() && !bae()) {
            mo993b(cKZ().mo960Nc());
        }
    }

    @C0064Am(aul = "1ff8c2a9c52cd06859838d71638eec10", aum = 0)
    /* renamed from: b */
    private void m39918b(aDJ adj) {
        if (adj == cKZ()) {
            dispose();
        }
    }

    @C0064Am(aul = "f824bca7b17bae00a164c19b140e5886", aum = 0)
    /* renamed from: fg */
    private void m39919fg() {
        if (bae()) {
            mo1099zx();
        }
        if (mo960Nc() != null) {
            mo1099zx();
        }
        super.dispose();
    }
}
