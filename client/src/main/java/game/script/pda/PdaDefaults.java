package game.script.pda;

import game.network.message.externalizable.aCE;
import game.network.message.serializable.C3438ra;
import game.script.TaikodomObject;
import logic.baa.*;
import logic.data.mbean.C2809kQ;
import logic.res.code.C5640aRc;
import logic.res.code.C5663aRz;
import logic.res.code.C6494any;
import logic.res.html.C2491fm;
import p001a.*;
import taikodom.infra.script.I18NString;

import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.UUID;

@C5511aMd
@C6485anp
/* renamed from: a.aBI */
/* compiled from: a */
public class PdaDefaults extends TaikodomObject implements C0468GU, C1616Xf {
    public static final int _m_fieldCount = 0;
    public static final C5663aRz[] _m_fields = null;
    public static final int _m_methodCount = 0;
    public static final C2491fm[] _m_methods = null;
    /* renamed from: bL */
    public static final C5663aRz f2405bL = null;
    /* renamed from: bN */
    public static final C2491fm f2406bN = null;
    /* renamed from: bO */
    public static final C2491fm f2407bO = null;
    /* renamed from: bP */
    public static final C2491fm f2408bP = null;
    public static final C5663aRz hhC = null;
    public static final C5663aRz hhD = null;
    public static final C2491fm hhE = null;
    public static final C2491fm hhF = null;
    public static final C2491fm hhG = null;
    public static final C2491fm hhH = null;
    public static final C2491fm hhI = null;
    public static final long serialVersionUID = 0;
    public static C6494any ___iScriptClass;
    @C0064Am(aul = "cd06878c5853948c2feae62976ec7849", aum = 0)
    private static DatabaseCategory avy;
    @C0064Am(aul = "678cd82bf1ae576aff46f2dd4d4b0eee", aum = 1)
    private static C3438ra<TaikopediaEntry> avz;
    @C0064Am(aul = "c2916df2e8e61288f71665a2deabca02", aum = 2)

    /* renamed from: bK */
    private static UUID f2404bK;

    static {
        m12758V();
    }

    public PdaDefaults() {
        super((C5540aNg) null);
        super.mo10S();
    }

    public PdaDefaults(C5540aNg ang) {
        super(ang);
    }

    /* renamed from: V */
    static void m12758V() {
        serialVersionUID = (long) 1;
        _m_fieldCount = TaikodomObject._m_fieldCount + 3;
        _m_methodCount = TaikodomObject._m_methodCount + 8;
        int i = TaikodomObject._m_fieldCount;
        C5663aRz[] arzArr = new C5663aRz[(i + 3)];
        C5663aRz b = C5640aRc.m17844b(PdaDefaults.class, "cd06878c5853948c2feae62976ec7849", i);
        hhC = b;
        arzArr[i] = b;
        int i2 = i + 1;
        C5663aRz b2 = C5640aRc.m17844b(PdaDefaults.class, "678cd82bf1ae576aff46f2dd4d4b0eee", i2);
        hhD = b2;
        arzArr[i2] = b2;
        int i3 = i2 + 1;
        C5663aRz b3 = C5640aRc.m17844b(PdaDefaults.class, "c2916df2e8e61288f71665a2deabca02", i3);
        f2405bL = b3;
        arzArr[i3] = b3;
        int i4 = i3 + 1;
        _m_fields = arzArr;
        C1634Xv.m11725a((Object[]) TaikodomObject._m_fields, (Object[]) _m_fields);
        int i5 = TaikodomObject._m_methodCount;
        C2491fm[] fmVarArr = new C2491fm[(i5 + 8)];
        C2491fm a = C4105zY.m41624a(PdaDefaults.class, "4873c838b8e48bc8e0c52b32a07365c0", i5);
        f2406bN = a;
        fmVarArr[i5] = a;
        int i6 = i5 + 1;
        C2491fm a2 = C4105zY.m41624a(PdaDefaults.class, "55da6bb15c666b6220b350fbcc6869c4", i6);
        f2407bO = a2;
        fmVarArr[i6] = a2;
        int i7 = i6 + 1;
        C2491fm a3 = C4105zY.m41624a(PdaDefaults.class, "a722462643506e9376d71411bd62b6a5", i7);
        f2408bP = a3;
        fmVarArr[i7] = a3;
        int i8 = i7 + 1;
        C2491fm a4 = C4105zY.m41624a(PdaDefaults.class, "2b39ff0c9d5b87e4d8f8848cd3f88324", i8);
        hhE = a4;
        fmVarArr[i8] = a4;
        int i9 = i8 + 1;
        C2491fm a5 = C4105zY.m41624a(PdaDefaults.class, "fba33484d80ee533afb57023693b3436", i9);
        hhF = a5;
        fmVarArr[i9] = a5;
        int i10 = i9 + 1;
        C2491fm a6 = C4105zY.m41624a(PdaDefaults.class, "3e7f4d6b0750748b6e337c6d5623ac2c", i10);
        hhG = a6;
        fmVarArr[i10] = a6;
        int i11 = i10 + 1;
        C2491fm a7 = C4105zY.m41624a(PdaDefaults.class, "fced1deea0d6ac31fc656e97afca1f3c", i11);
        hhH = a7;
        fmVarArr[i11] = a7;
        int i12 = i11 + 1;
        C2491fm a8 = C4105zY.m41624a(PdaDefaults.class, "6bbe43163af1eaec98875e4d51c57615", i12);
        hhI = a8;
        fmVarArr[i12] = a8;
        int i13 = i12 + 1;
        _m_methods = fmVarArr;
        C1634Xv.m11725a((Object[]) TaikodomObject._m_methods, (Object[]) _m_methods);
        ___iScriptClass = aUO.m18566a(PdaDefaults.class, C2809kQ.class, _m_fields, _m_methods);
    }

    /* renamed from: a */
    private void m12759a(UUID uuid) {
        bFf().mo5608dq().mo3197f(f2405bL, uuid);
    }

    /* renamed from: an */
    private UUID m12760an() {
        return (UUID) bFf().mo5608dq().mo3214p(f2405bL);
    }

    /* renamed from: c */
    private void m12764c(UUID uuid) {
        switch (bFf().mo6893i(f2407bO)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f2407bO, new Object[]{uuid}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f2407bO, new Object[]{uuid}));
                break;
        }
        m12763b(uuid);
    }

    private DatabaseCategory cJs() {
        return (DatabaseCategory) bFf().mo5608dq().mo3214p(hhC);
    }

    private C3438ra cJt() {
        return (C3438ra) bFf().mo5608dq().mo3214p(hhD);
    }

    /* renamed from: cn */
    private void m12765cn(C3438ra raVar) {
        bFf().mo5608dq().mo3197f(hhD, raVar);
    }

    @C3248pc(aYR = C3248pc.C3250b.ADDER, displayName = "Default Taikopedia Entries")
    @C0064Am(aul = "3e7f4d6b0750748b6e337c6d5623ac2c", aum = 0)
    @C5566aOg
    /* renamed from: g */
    private void m12766g(TaikopediaEntry aiz) {
        throw new aWi(new aCE(this, hhG, new Object[]{aiz}));
    }

    /* renamed from: h */
    private void m12767h(DatabaseCategory aik) {
        bFf().mo5608dq().mo3197f(hhC, aik);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Default category")
    @C0064Am(aul = "fba33484d80ee533afb57023693b3436", aum = 0)
    @C5566aOg
    /* renamed from: i */
    private void m12768i(DatabaseCategory aik) {
        throw new aWi(new aCE(this, hhF, new Object[]{aik}));
    }

    @C3248pc(aYR = C3248pc.C3250b.REMOVER, displayName = "Default Taikopedia Entries")
    @C0064Am(aul = "fced1deea0d6ac31fc656e97afca1f3c", aum = 0)
    @C5566aOg
    /* renamed from: i */
    private void m12769i(TaikopediaEntry aiz) {
        throw new aWi(new aCE(this, hhH, new Object[]{aiz}));
    }

    /* renamed from: T */
    public C6494any mo11T() {
        return ___iScriptClass;
    }

    /* renamed from: U */
    public C2491fm[] mo12U() {
        return _m_methods;
    }

    /* renamed from: W */
    public Object mo13W() {
        return new C2809kQ(this);
    }

    /* renamed from: a */
    public Object mo14a(C0495Gr gr) {
        Object[] args = gr.getArgs();
        switch (gr.mo2417hq() - TaikodomObject._m_methodCount) {
            case 0:
                return m12761ap();
            case 1:
                m12763b((UUID) args[0]);
                return null;
            case 2:
                return m12762ar();
            case 3:
                return cJu();
            case 4:
                m12768i((DatabaseCategory) args[0]);
                return null;
            case 5:
                m12766g((TaikopediaEntry) args[0]);
                return null;
            case 6:
                m12769i((TaikopediaEntry) args[0]);
                return null;
            case 7:
                return cJw();
            default:
                return super.mo14a(gr);
        }
    }

    /* renamed from: a */
    public void mo15a(Collection collection, C0495Gr gr) {
        super.mo15a(collection, gr);
    }

    /* renamed from: aq */
    public UUID mo18aq() {
        switch (bFf().mo6893i(f2406bN)) {
            case 0:
                return null;
            case 2:
                return (UUID) bFf().mo5606d(new aCE(this, f2406bN, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f2406bN, new Object[0]));
                break;
        }
        return m12761ap();
    }

    /* renamed from: c */
    public C5663aRz[] mo25c() {
        return _m_fields;
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Default category")
    public DatabaseCategory cJv() {
        switch (bFf().mo6893i(hhE)) {
            case 0:
                return null;
            case 2:
                return (DatabaseCategory) bFf().mo5606d(new aCE(this, hhE, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, hhE, new Object[0]));
                break;
        }
        return cJu();
    }

    @C3248pc(aYR = C3248pc.C3250b.ITERATOR, displayName = "Default Taikopedia Entries")
    public List<TaikopediaEntry> cJx() {
        switch (bFf().mo6893i(hhI)) {
            case 0:
                return null;
            case 2:
                return (List) bFf().mo5606d(new aCE(this, hhI, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, hhI, new Object[0]));
                break;
        }
        return cJw();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Handle")
    public String getHandle() {
        switch (bFf().mo6893i(f2408bP)) {
            case 0:
                return null;
            case 2:
                return (String) bFf().mo5606d(new aCE(this, f2408bP, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f2408bP, new Object[0]));
                break;
        }
        return m12762ar();
    }

    @C3248pc(aYR = C3248pc.C3250b.ADDER, displayName = "Default Taikopedia Entries")
    @C5566aOg
    /* renamed from: h */
    public void mo7837h(TaikopediaEntry aiz) {
        switch (bFf().mo6893i(hhG)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, hhG, new Object[]{aiz}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, hhG, new Object[]{aiz}));
                break;
        }
        m12766g(aiz);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Default category")
    @C5566aOg
    /* renamed from: j */
    public void mo7838j(DatabaseCategory aik) {
        switch (bFf().mo6893i(hhF)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, hhF, new Object[]{aik}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, hhF, new Object[]{aik}));
                break;
        }
        m12768i(aik);
    }

    @C3248pc(aYR = C3248pc.C3250b.REMOVER, displayName = "Default Taikopedia Entries")
    @C5566aOg
    /* renamed from: j */
    public void mo7839j(TaikopediaEntry aiz) {
        switch (bFf().mo6893i(hhH)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, hhH, new Object[]{aiz}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, hhH, new Object[]{aiz}));
                break;
        }
        m12769i(aiz);
    }

    @C0064Am(aul = "4873c838b8e48bc8e0c52b32a07365c0", aum = 0)
    /* renamed from: ap */
    private UUID m12761ap() {
        return m12760an();
    }

    @C0064Am(aul = "55da6bb15c666b6220b350fbcc6869c4", aum = 0)
    /* renamed from: b */
    private void m12763b(UUID uuid) {
        m12759a(uuid);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Handle")
    @C0064Am(aul = "a722462643506e9376d71411bd62b6a5", aum = 0)
    /* renamed from: ar */
    private String m12762ar() {
        return "pda_defaults";
    }

    /* renamed from: S */
    public void mo10S() {
        super.mo10S();
        DatabaseCategory aik = (DatabaseCategory) bFf().mo6865M(DatabaseCategory.class);
        aik.mo10S();
        m12767h(aik);
        cJs().mo13665bt(new I18NString("Uncategorized"));
        cJs().mo13667bv(new I18NString("Uncategorized pda entry."));
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Default category")
    @C0064Am(aul = "2b39ff0c9d5b87e4d8f8848cd3f88324", aum = 0)
    private DatabaseCategory cJu() {
        return cJs();
    }

    @C3248pc(aYR = C3248pc.C3250b.ITERATOR, displayName = "Default Taikopedia Entries")
    @C0064Am(aul = "6bbe43163af1eaec98875e4d51c57615", aum = 0)
    private List<TaikopediaEntry> cJw() {
        return Collections.unmodifiableList(cJt());
    }
}
