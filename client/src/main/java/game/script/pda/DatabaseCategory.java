package game.script.pda;

import game.network.message.externalizable.aCE;
import game.script.TaikodomObject;
import game.script.resource.Asset;
import logic.baa.*;
import logic.data.mbean.C0126BZ;
import logic.res.code.C5640aRc;
import logic.res.code.C5663aRz;
import logic.res.code.C6494any;
import logic.res.html.C2491fm;
import p001a.*;
import taikodom.infra.script.I18NString;

import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.UUID;

@C5511aMd
@C6485anp
/* renamed from: a.aiK  reason: case insensitive filesystem */
/* compiled from: a */
public class DatabaseCategory extends TaikodomObject implements C0468GU, C1616Xf, C4068yr {

    /* renamed from: MN */
    public static final C2491fm f4605MN = null;

    /* renamed from: QA */
    public static final C5663aRz f4606QA = null;

    /* renamed from: QD */
    public static final C2491fm f4607QD = null;

    /* renamed from: QE */
    public static final C2491fm f4608QE = null;

    /* renamed from: QF */
    public static final C2491fm f4609QF = null;

    /* renamed from: Qz */
    public static final C5663aRz f4610Qz = null;
    public static final int _m_fieldCount = 0;
    public static final C5663aRz[] _m_fields = null;
    public static final int _m_methodCount = 0;
    public static final C2491fm[] _m_methods = null;
    public static final C2491fm aMj = null;
    public static final C2491fm aMk = null;
    public static final C2491fm aMn = null;
    public static final C2491fm aMp = null;
    public static final C2491fm aMr = null;
    public static final C2491fm aMu = null;
    /* renamed from: bL */
    public static final C5663aRz f4612bL = null;
    /* renamed from: bM */
    public static final C5663aRz f4613bM = null;
    /* renamed from: bN */
    public static final C2491fm f4614bN = null;
    /* renamed from: bO */
    public static final C2491fm f4615bO = null;
    /* renamed from: bP */
    public static final C2491fm f4616bP = null;
    /* renamed from: bQ */
    public static final C2491fm f4617bQ = null;
    public static final C5663aRz fOA = null;
    public static final C5663aRz fOB = null;
    public static final C2491fm fOC = null;
    public static final C2491fm fOD = null;
    public static final C2491fm fOE = null;
    public static final C2491fm fOF = null;
    public static final long serialVersionUID = 0;
    public static C6494any ___iScriptClass;
    @C0064Am(aul = "2beec937081f09bdcfe49ff2e9051966", aum = 5)

    /* renamed from: bK */
    private static UUID f4611bK;
    @C0064Am(aul = "c8724ebfc99f7a89210a1c79fb6ae20a", aum = 3)
    private static Asset cuI;
    @C0064Am(aul = "fc70f2c512117ed815c3f3cd86434294", aum = 4)
    private static boolean cuJ;
    @C0064Am(aul = "88d8a5c18c85135447261c492ac82ad2", aum = 0)
    private static String handle;
    @C0064Am(aul = "03fa34162e8f90d4035dc238cec23504", aum = 2)

    /* renamed from: nh */
    private static I18NString f4618nh;
    @C0064Am(aul = "6778fe618cfd90c9603e8e3911136a24", aum = 1)

    /* renamed from: ni */
    private static I18NString f4619ni;

    static {
        m22490V();
    }

    public DatabaseCategory() {
        super((C5540aNg) null);
        super.mo10S();
    }

    public DatabaseCategory(C5540aNg ang) {
        super(ang);
    }

    /* renamed from: V */
    static void m22490V() {
        serialVersionUID = (long) 1;
        _m_fieldCount = TaikodomObject._m_fieldCount + 6;
        _m_methodCount = TaikodomObject._m_methodCount + 18;
        int i = TaikodomObject._m_fieldCount;
        C5663aRz[] arzArr = new C5663aRz[(i + 6)];
        C5663aRz b = C5640aRc.m17844b(DatabaseCategory.class, "88d8a5c18c85135447261c492ac82ad2", i);
        f4613bM = b;
        arzArr[i] = b;
        int i2 = i + 1;
        C5663aRz b2 = C5640aRc.m17844b(DatabaseCategory.class, "6778fe618cfd90c9603e8e3911136a24", i2);
        f4610Qz = b2;
        arzArr[i2] = b2;
        int i3 = i2 + 1;
        C5663aRz b3 = C5640aRc.m17844b(DatabaseCategory.class, "03fa34162e8f90d4035dc238cec23504", i3);
        f4606QA = b3;
        arzArr[i3] = b3;
        int i4 = i3 + 1;
        C5663aRz b4 = C5640aRc.m17844b(DatabaseCategory.class, "c8724ebfc99f7a89210a1c79fb6ae20a", i4);
        fOA = b4;
        arzArr[i4] = b4;
        int i5 = i4 + 1;
        C5663aRz b5 = C5640aRc.m17844b(DatabaseCategory.class, "fc70f2c512117ed815c3f3cd86434294", i5);
        fOB = b5;
        arzArr[i5] = b5;
        int i6 = i5 + 1;
        C5663aRz b6 = C5640aRc.m17844b(DatabaseCategory.class, "2beec937081f09bdcfe49ff2e9051966", i6);
        f4612bL = b6;
        arzArr[i6] = b6;
        int i7 = i6 + 1;
        _m_fields = arzArr;
        C1634Xv.m11725a((Object[]) TaikodomObject._m_fields, (Object[]) _m_fields);
        int i8 = TaikodomObject._m_methodCount;
        C2491fm[] fmVarArr = new C2491fm[(i8 + 18)];
        C2491fm a = C4105zY.m41624a(DatabaseCategory.class, "77886174d40f1edc34f0b2e2b56562d5", i8);
        f4614bN = a;
        fmVarArr[i8] = a;
        int i9 = i8 + 1;
        C2491fm a2 = C4105zY.m41624a(DatabaseCategory.class, "2c6f41926f69dcbcdb3a369eb7da2a0d", i9);
        f4615bO = a2;
        fmVarArr[i9] = a2;
        int i10 = i9 + 1;
        C2491fm a3 = C4105zY.m41624a(DatabaseCategory.class, "2517d9654396a442af679ec42e037f8c", i10);
        f4616bP = a3;
        fmVarArr[i10] = a3;
        int i11 = i10 + 1;
        C2491fm a4 = C4105zY.m41624a(DatabaseCategory.class, "32e30db5f076dbcba3adf0c83b41adf8", i11);
        f4617bQ = a4;
        fmVarArr[i11] = a4;
        int i12 = i11 + 1;
        C2491fm a5 = C4105zY.m41624a(DatabaseCategory.class, "457b289504505bce826959359731eb66", i12);
        f4605MN = a5;
        fmVarArr[i12] = a5;
        int i13 = i12 + 1;
        C2491fm a6 = C4105zY.m41624a(DatabaseCategory.class, "9a2d9e4872b6cacc62c72ea1e963de6e", i13);
        f4607QD = a6;
        fmVarArr[i13] = a6;
        int i14 = i13 + 1;
        C2491fm a7 = C4105zY.m41624a(DatabaseCategory.class, "b7a0da546172b88c52e8607d202c9dcd", i14);
        f4608QE = a7;
        fmVarArr[i14] = a7;
        int i15 = i14 + 1;
        C2491fm a8 = C4105zY.m41624a(DatabaseCategory.class, "0cef2a3ad2c5e19eef998d1aee7b8a65", i15);
        f4609QF = a8;
        fmVarArr[i15] = a8;
        int i16 = i15 + 1;
        C2491fm a9 = C4105zY.m41624a(DatabaseCategory.class, "01fabc48360ac3f5c6f742efe8f6b20e", i16);
        aMj = a9;
        fmVarArr[i16] = a9;
        int i17 = i16 + 1;
        C2491fm a10 = C4105zY.m41624a(DatabaseCategory.class, "ccbcfea6e22ff14439ecd9449a50b1f7", i17);
        aMn = a10;
        fmVarArr[i17] = a10;
        int i18 = i17 + 1;
        C2491fm a11 = C4105zY.m41624a(DatabaseCategory.class, "dd8b22b67fad10f20aecb1b9f909d966", i18);
        fOC = a11;
        fmVarArr[i18] = a11;
        int i19 = i18 + 1;
        C2491fm a12 = C4105zY.m41624a(DatabaseCategory.class, "11108c83054368b259aed764c7048f06", i19);
        fOD = a12;
        fmVarArr[i19] = a12;
        int i20 = i19 + 1;
        C2491fm a13 = C4105zY.m41624a(DatabaseCategory.class, "2ebd1c8454640f9bd35861769cedd2b4", i20);
        aMp = a13;
        fmVarArr[i20] = a13;
        int i21 = i20 + 1;
        C2491fm a14 = C4105zY.m41624a(DatabaseCategory.class, "58cb6362f824aa8acbf19d9abc5339b4", i21);
        aMr = a14;
        fmVarArr[i21] = a14;
        int i22 = i21 + 1;
        C2491fm a15 = C4105zY.m41624a(DatabaseCategory.class, "d58560897ac6273dcecc20559851e651", i22);
        aMk = a15;
        fmVarArr[i22] = a15;
        int i23 = i22 + 1;
        C2491fm a16 = C4105zY.m41624a(DatabaseCategory.class, "23dabc0fd9b8a250a853ee8ff41a8f88", i23);
        aMu = a16;
        fmVarArr[i23] = a16;
        int i24 = i23 + 1;
        C2491fm a17 = C4105zY.m41624a(DatabaseCategory.class, "8cbbf2a12536288843fe0cfd960aa92e", i24);
        fOE = a17;
        fmVarArr[i24] = a17;
        int i25 = i24 + 1;
        C2491fm a18 = C4105zY.m41624a(DatabaseCategory.class, "334579a4c1e97c7f3e86d1cc5986a250", i25);
        fOF = a18;
        fmVarArr[i25] = a18;
        int i26 = i25 + 1;
        _m_methods = fmVarArr;
        C1634Xv.m11725a((Object[]) TaikodomObject._m_methods, (Object[]) _m_methods);
        ___iScriptClass = aUO.m18566a(DatabaseCategory.class, C0126BZ.class, _m_fields, _m_methods);
    }

    /* renamed from: a */
    private void m22491a(String str) {
        bFf().mo5608dq().mo3197f(f4613bM, str);
    }

    /* renamed from: a */
    private void m22492a(UUID uuid) {
        bFf().mo5608dq().mo3197f(f4612bL, uuid);
    }

    /* renamed from: an */
    private UUID m22493an() {
        return (UUID) bFf().mo5608dq().mo3214p(f4612bL);
    }

    /* renamed from: ao */
    private String m22494ao() {
        return (String) bFf().mo5608dq().mo3214p(f4613bM);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Handle")
    @C0064Am(aul = "32e30db5f076dbcba3adf0c83b41adf8", aum = 0)
    @C5566aOg
    /* renamed from: b */
    private void m22497b(String str) {
        throw new aWi(new aCE(this, f4617bQ, new Object[]{str}));
    }

    /* renamed from: bq */
    private void m22499bq(I18NString i18NString) {
        bFf().mo5608dq().mo3197f(f4610Qz, i18NString);
    }

    /* renamed from: br */
    private void m22500br(I18NString i18NString) {
        bFf().mo5608dq().mo3197f(f4606QA, i18NString);
    }

    /* renamed from: bs */
    private void m22501bs(Asset tCVar) {
        bFf().mo5608dq().mo3197f(fOA, tCVar);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Title")
    @C0064Am(aul = "9a2d9e4872b6cacc62c72ea1e963de6e", aum = 0)
    @C5566aOg
    /* renamed from: bs */
    private void m22502bs(I18NString i18NString) {
        throw new aWi(new aCE(this, f4607QD, new Object[]{i18NString}));
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "PDA Icon")
    @C0064Am(aul = "11108c83054368b259aed764c7048f06", aum = 0)
    @C5566aOg
    /* renamed from: bt */
    private void m22503bt(Asset tCVar) {
        throw new aWi(new aCE(this, fOD, new Object[]{tCVar}));
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Description")
    @C0064Am(aul = "0cef2a3ad2c5e19eef998d1aee7b8a65", aum = 0)
    @C5566aOg
    /* renamed from: bu */
    private void m22504bu(I18NString i18NString) {
        throw new aWi(new aCE(this, f4609QF, new Object[]{i18NString}));
    }

    /* renamed from: c */
    private void m22505c(UUID uuid) {
        switch (bFf().mo6893i(f4615bO)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f4615bO, new Object[]{uuid}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f4615bO, new Object[]{uuid}));
                break;
        }
        m22498b(uuid);
    }

    private Asset ccw() {
        return (Asset) bFf().mo5608dq().mo3214p(fOA);
    }

    private boolean ccx() {
        return bFf().mo5608dq().mo3201h(fOB);
    }

    /* renamed from: eW */
    private void m22506eW(boolean z) {
        bFf().mo5608dq().mo3153a(fOB, z);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Deactivate")
    @C0064Am(aul = "334579a4c1e97c7f3e86d1cc5986a250", aum = 0)
    @C5566aOg
    /* renamed from: eX */
    private void m22507eX(boolean z) {
        throw new aWi(new aCE(this, fOF, new Object[]{new Boolean(z)}));
    }

    /* renamed from: vS */
    private I18NString m22509vS() {
        return (I18NString) bFf().mo5608dq().mo3214p(f4610Qz);
    }

    /* renamed from: vT */
    private I18NString m22510vT() {
        return (I18NString) bFf().mo5608dq().mo3214p(f4606QA);
    }

    /* renamed from: RU */
    public I18NString mo182RU() {
        switch (bFf().mo6893i(aMj)) {
            case 0:
                return null;
            case 2:
                return (I18NString) bFf().mo5606d(new aCE(this, aMj, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, aMj, new Object[0]));
                break;
        }
        return m22484RT();
    }

    /* renamed from: RW */
    public List<TaikopediaEntry> mo183RW() {
        switch (bFf().mo6893i(aMk)) {
            case 0:
                return null;
            case 2:
                return (List) bFf().mo5606d(new aCE(this, aMk, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, aMk, new Object[0]));
                break;
        }
        return m22485RV();
    }

    /* renamed from: RY */
    public DatabaseCategory mo184RY() {
        switch (bFf().mo6893i(aMn)) {
            case 0:
                return null;
            case 2:
                return (DatabaseCategory) bFf().mo5606d(new aCE(this, aMn, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, aMn, new Object[0]));
                break;
        }
        return m22486RX();
    }

    /* renamed from: Sa */
    public String mo185Sa() {
        switch (bFf().mo6893i(aMp)) {
            case 0:
                return null;
            case 2:
                return (String) bFf().mo5606d(new aCE(this, aMp, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, aMp, new Object[0]));
                break;
        }
        return m22487RZ();
    }

    /* renamed from: Se */
    public Asset mo187Se() {
        switch (bFf().mo6893i(aMr)) {
            case 0:
                return null;
            case 2:
                return (Asset) bFf().mo5606d(new aCE(this, aMr, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, aMr, new Object[0]));
                break;
        }
        return m22488Sd();
    }

    /* renamed from: Sg */
    public float mo188Sg() {
        switch (bFf().mo6893i(aMu)) {
            case 0:
                return 0.0f;
            case 2:
                return ((Float) bFf().mo5606d(new aCE(this, aMu, new Object[0]))).floatValue();
            case 3:
                bFf().mo5606d(new aCE(this, aMu, new Object[0]));
                break;
        }
        return m22489Sf();
    }

    /* renamed from: T */
    public C6494any mo11T() {
        return ___iScriptClass;
    }

    /* renamed from: U */
    public C2491fm[] mo12U() {
        return _m_methods;
    }

    /* renamed from: W */
    public Object mo13W() {
        return new C0126BZ(this);
    }

    /* renamed from: a */
    public Object mo14a(C0495Gr gr) {
        Object[] args = gr.getArgs();
        switch (gr.mo2417hq() - TaikodomObject._m_methodCount) {
            case 0:
                return m22495ap();
            case 1:
                m22498b((UUID) args[0]);
                return null;
            case 2:
                return m22496ar();
            case 3:
                m22497b((String) args[0]);
                return null;
            case 4:
                return m22508rO();
            case 5:
                m22502bs((I18NString) args[0]);
                return null;
            case 6:
                return m22511vV();
            case 7:
                m22504bu((I18NString) args[0]);
                return null;
            case 8:
                return m22484RT();
            case 9:
                return m22486RX();
            case 10:
                return ccy();
            case 11:
                m22503bt((Asset) args[0]);
                return null;
            case 12:
                return m22487RZ();
            case 13:
                return m22488Sd();
            case 14:
                return m22485RV();
            case 15:
                return new Float(m22489Sf());
            case 16:
                return new Boolean(ccA());
            case 17:
                m22507eX(((Boolean) args[0]).booleanValue());
                return null;
            default:
                return super.mo14a(gr);
        }
    }

    /* renamed from: a */
    public void mo15a(Collection collection, C0495Gr gr) {
        super.mo15a(collection, gr);
    }

    /* renamed from: aq */
    public UUID mo18aq() {
        switch (bFf().mo6893i(f4614bN)) {
            case 0:
                return null;
            case 2:
                return (UUID) bFf().mo5606d(new aCE(this, f4614bN, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f4614bN, new Object[0]));
                break;
        }
        return m22495ap();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Title")
    @C5566aOg
    /* renamed from: bt */
    public void mo13665bt(I18NString i18NString) {
        switch (bFf().mo6893i(f4607QD)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f4607QD, new Object[]{i18NString}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f4607QD, new Object[]{i18NString}));
                break;
        }
        m22502bs(i18NString);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "PDA Icon")
    @C5566aOg
    /* renamed from: bu */
    public void mo13666bu(Asset tCVar) {
        switch (bFf().mo6893i(fOD)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, fOD, new Object[]{tCVar}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, fOD, new Object[]{tCVar}));
                break;
        }
        m22503bt(tCVar);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Description")
    @C5566aOg
    /* renamed from: bv */
    public void mo13667bv(I18NString i18NString) {
        switch (bFf().mo6893i(f4609QF)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f4609QF, new Object[]{i18NString}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f4609QF, new Object[]{i18NString}));
                break;
        }
        m22504bu(i18NString);
    }

    /* renamed from: c */
    public C5663aRz[] mo25c() {
        return _m_fields;
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Deactivate")
    public boolean ccB() {
        switch (bFf().mo6893i(fOE)) {
            case 0:
                return false;
            case 2:
                return ((Boolean) bFf().mo5606d(new aCE(this, fOE, new Object[0]))).booleanValue();
            case 3:
                bFf().mo5606d(new aCE(this, fOE, new Object[0]));
                break;
        }
        return ccA();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "PDA Icon")
    public Asset ccz() {
        switch (bFf().mo6893i(fOC)) {
            case 0:
                return null;
            case 2:
                return (Asset) bFf().mo5606d(new aCE(this, fOC, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, fOC, new Object[0]));
                break;
        }
        return ccy();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Deactivate")
    @C5566aOg
    /* renamed from: eY */
    public void mo13670eY(boolean z) {
        switch (bFf().mo6893i(fOF)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, fOF, new Object[]{new Boolean(z)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, fOF, new Object[]{new Boolean(z)}));
                break;
        }
        m22507eX(z);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Handle")
    public String getHandle() {
        switch (bFf().mo6893i(f4616bP)) {
            case 0:
                return null;
            case 2:
                return (String) bFf().mo5606d(new aCE(this, f4616bP, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f4616bP, new Object[0]));
                break;
        }
        return m22496ar();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Handle")
    @C5566aOg
    public void setHandle(String str) {
        switch (bFf().mo6893i(f4617bQ)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f4617bQ, new Object[]{str}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f4617bQ, new Object[]{str}));
                break;
        }
        m22497b(str);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Title")
    /* renamed from: rP */
    public I18NString mo195rP() {
        switch (bFf().mo6893i(f4605MN)) {
            case 0:
                return null;
            case 2:
                return (I18NString) bFf().mo5606d(new aCE(this, f4605MN, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f4605MN, new Object[0]));
                break;
        }
        return m22508rO();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Description")
    /* renamed from: vW */
    public I18NString mo13672vW() {
        switch (bFf().mo6893i(f4608QE)) {
            case 0:
                return null;
            case 2:
                return (I18NString) bFf().mo5606d(new aCE(this, f4608QE, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f4608QE, new Object[0]));
                break;
        }
        return m22511vV();
    }

    @C0064Am(aul = "77886174d40f1edc34f0b2e2b56562d5", aum = 0)
    /* renamed from: ap */
    private UUID m22495ap() {
        return m22493an();
    }

    @C0064Am(aul = "2c6f41926f69dcbcdb3a369eb7da2a0d", aum = 0)
    /* renamed from: b */
    private void m22498b(UUID uuid) {
        m22492a(uuid);
    }

    /* renamed from: S */
    public void mo10S() {
        super.mo10S();
        m22492a(UUID.randomUUID());
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Handle")
    @C0064Am(aul = "2517d9654396a442af679ec42e037f8c", aum = 0)
    /* renamed from: ar */
    private String m22496ar() {
        return m22494ao();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Title")
    @C0064Am(aul = "457b289504505bce826959359731eb66", aum = 0)
    /* renamed from: rO */
    private I18NString m22508rO() {
        return m22509vS();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Description")
    @C0064Am(aul = "b7a0da546172b88c52e8607d202c9dcd", aum = 0)
    /* renamed from: vV */
    private I18NString m22511vV() {
        return m22510vT();
    }

    @C0064Am(aul = "01fabc48360ac3f5c6f742efe8f6b20e", aum = 0)
    /* renamed from: RT */
    private I18NString m22484RT() {
        return mo13672vW();
    }

    @C0064Am(aul = "ccbcfea6e22ff14439ecd9449a50b1f7", aum = 0)
    /* renamed from: RX */
    private DatabaseCategory m22486RX() {
        return null;
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "PDA Icon")
    @C0064Am(aul = "dd8b22b67fad10f20aecb1b9f909d966", aum = 0)
    private Asset ccy() {
        return ccw();
    }

    @C0064Am(aul = "2ebd1c8454640f9bd35861769cedd2b4", aum = 0)
    /* renamed from: RZ */
    private String m22487RZ() {
        if (ccw() == null) {
            return null;
        }
        return ccw().getHandle();
    }

    @C0064Am(aul = "58cb6362f824aa8acbf19d9abc5339b4", aum = 0)
    /* renamed from: Sd */
    private Asset m22488Sd() {
        return null;
    }

    @C0064Am(aul = "d58560897ac6273dcecc20559851e651", aum = 0)
    /* renamed from: RV */
    private List<TaikopediaEntry> m22485RV() {
        return Collections.emptyList();
    }

    @C0064Am(aul = "23dabc0fd9b8a250a853ee8ff41a8f88", aum = 0)
    /* renamed from: Sf */
    private float m22489Sf() {
        return 0.0f;
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Deactivate")
    @C0064Am(aul = "8cbbf2a12536288843fe0cfd960aa92e", aum = 0)
    private boolean ccA() {
        return ccx();
    }
}
