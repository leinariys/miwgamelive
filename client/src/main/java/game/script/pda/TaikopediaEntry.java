package game.script.pda;

import game.network.message.externalizable.aCE;
import logic.baa.*;
import logic.data.mbean.C6612aqM;
import logic.res.code.C5640aRc;
import logic.res.code.C5663aRz;
import logic.res.code.C6494any;
import logic.res.html.C2491fm;
import p001a.*;
import taikodom.infra.script.I18NString;

import java.util.Collection;
import java.util.UUID;

@C5511aMd
@C6485anp
/* renamed from: a.aiZ  reason: case insensitive filesystem */
/* compiled from: a */
public class TaikopediaEntry extends aDJ implements C0468GU, C1616Xf, C4033yO {

    /* renamed from: MN */
    public static final C2491fm f4641MN = null;

    /* renamed from: QA */
    public static final C5663aRz f4642QA = null;

    /* renamed from: QD */
    public static final C2491fm f4643QD = null;

    /* renamed from: QE */
    public static final C2491fm f4644QE = null;

    /* renamed from: QF */
    public static final C2491fm f4645QF = null;

    /* renamed from: Qz */
    public static final C5663aRz f4646Qz = null;
    public static final int _m_fieldCount = 0;
    public static final C5663aRz[] _m_fields = null;
    public static final int _m_methodCount = 0;
    public static final C2491fm[] _m_methods = null;
    public static final C2491fm aMj = null;
    /* renamed from: bL */
    public static final C5663aRz f4648bL = null;
    /* renamed from: bM */
    public static final C5663aRz f4649bM = null;
    /* renamed from: bN */
    public static final C2491fm f4650bN = null;
    /* renamed from: bO */
    public static final C2491fm f4651bO = null;
    /* renamed from: bP */
    public static final C2491fm f4652bP = null;
    /* renamed from: bQ */
    public static final C2491fm f4653bQ = null;
    public static final long serialVersionUID = 0;
    public static C6494any ___iScriptClass;
    @C0064Am(aul = "8bac975bf95a6f90d68bec4b0b3989b7", aum = 3)

    /* renamed from: bK */
    private static UUID f4647bK;
    @C0064Am(aul = "8c54a988dadf2d2e52cf4368f3e1db70", aum = 0)
    private static String handle;
    @C0064Am(aul = "f906d758323f6e6b27cce4bc685b2a98", aum = 2)

    /* renamed from: nh */
    private static I18NString f4654nh;
    @C0064Am(aul = "f6f5f4bfad64c72ce6a865acd7b342f6", aum = 1)

    /* renamed from: ni */
    private static I18NString f4655ni;

    static {
        m22585V();
    }

    public TaikopediaEntry() {
        super((C5540aNg) null);
        super.mo10S();
    }

    public TaikopediaEntry(C5540aNg ang) {
        super(ang);
    }

    /* renamed from: V */
    static void m22585V() {
        serialVersionUID = (long) 1;
        _m_fieldCount = aDJ._m_fieldCount + 4;
        _m_methodCount = aDJ._m_methodCount + 9;
        int i = aDJ._m_fieldCount;
        C5663aRz[] arzArr = new C5663aRz[(i + 4)];
        C5663aRz b = C5640aRc.m17844b(TaikopediaEntry.class, "8c54a988dadf2d2e52cf4368f3e1db70", i);
        f4649bM = b;
        arzArr[i] = b;
        int i2 = i + 1;
        C5663aRz b2 = C5640aRc.m17844b(TaikopediaEntry.class, "f6f5f4bfad64c72ce6a865acd7b342f6", i2);
        f4646Qz = b2;
        arzArr[i2] = b2;
        int i3 = i2 + 1;
        C5663aRz b3 = C5640aRc.m17844b(TaikopediaEntry.class, "f906d758323f6e6b27cce4bc685b2a98", i3);
        f4642QA = b3;
        arzArr[i3] = b3;
        int i4 = i3 + 1;
        C5663aRz b4 = C5640aRc.m17844b(TaikopediaEntry.class, "8bac975bf95a6f90d68bec4b0b3989b7", i4);
        f4648bL = b4;
        arzArr[i4] = b4;
        int i5 = i4 + 1;
        _m_fields = arzArr;
        C1634Xv.m11725a((Object[]) aDJ._m_fields, (Object[]) _m_fields);
        int i6 = aDJ._m_methodCount;
        C2491fm[] fmVarArr = new C2491fm[(i6 + 9)];
        C2491fm a = C4105zY.m41624a(TaikopediaEntry.class, "51a53d559a5675d11c23e3aac0c52be1", i6);
        f4650bN = a;
        fmVarArr[i6] = a;
        int i7 = i6 + 1;
        C2491fm a2 = C4105zY.m41624a(TaikopediaEntry.class, "9e2a1ecce9972ca388baed7e95b71429", i7);
        f4651bO = a2;
        fmVarArr[i7] = a2;
        int i8 = i7 + 1;
        C2491fm a3 = C4105zY.m41624a(TaikopediaEntry.class, "13f0152805a291762599b908bca3ded4", i8);
        f4652bP = a3;
        fmVarArr[i8] = a3;
        int i9 = i8 + 1;
        C2491fm a4 = C4105zY.m41624a(TaikopediaEntry.class, "2cdae95840f979d14595117f4ec31e39", i9);
        f4653bQ = a4;
        fmVarArr[i9] = a4;
        int i10 = i9 + 1;
        C2491fm a5 = C4105zY.m41624a(TaikopediaEntry.class, "a29a9c58d9369491aa501fbcefd705a4", i10);
        f4641MN = a5;
        fmVarArr[i10] = a5;
        int i11 = i10 + 1;
        C2491fm a6 = C4105zY.m41624a(TaikopediaEntry.class, "4a2a10af5f168103e21402d697c3ecff", i11);
        f4643QD = a6;
        fmVarArr[i11] = a6;
        int i12 = i11 + 1;
        C2491fm a7 = C4105zY.m41624a(TaikopediaEntry.class, "fab0e7c3c895e8663a28858c9c3910bc", i12);
        f4644QE = a7;
        fmVarArr[i12] = a7;
        int i13 = i12 + 1;
        C2491fm a8 = C4105zY.m41624a(TaikopediaEntry.class, "e130ad826d4659bc8a1e06588f74f240", i13);
        f4645QF = a8;
        fmVarArr[i13] = a8;
        int i14 = i13 + 1;
        C2491fm a9 = C4105zY.m41624a(TaikopediaEntry.class, "e64c3a83cb704e0886f679631990b4f7", i14);
        aMj = a9;
        fmVarArr[i14] = a9;
        int i15 = i14 + 1;
        _m_methods = fmVarArr;
        C1634Xv.m11725a((Object[]) aDJ._m_methods, (Object[]) _m_methods);
        ___iScriptClass = aUO.m18566a(TaikopediaEntry.class, C6612aqM.class, _m_fields, _m_methods);
    }

    /* renamed from: a */
    private void m22586a(String str) {
        bFf().mo5608dq().mo3197f(f4649bM, str);
    }

    /* renamed from: a */
    private void m22587a(UUID uuid) {
        bFf().mo5608dq().mo3197f(f4648bL, uuid);
    }

    /* renamed from: an */
    private UUID m22588an() {
        return (UUID) bFf().mo5608dq().mo3214p(f4648bL);
    }

    /* renamed from: ao */
    private String m22589ao() {
        return (String) bFf().mo5608dq().mo3214p(f4649bM);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Handle")
    @C0064Am(aul = "2cdae95840f979d14595117f4ec31e39", aum = 0)
    @C5566aOg
    /* renamed from: b */
    private void m22592b(String str) {
        throw new aWi(new aCE(this, f4653bQ, new Object[]{str}));
    }

    /* renamed from: bq */
    private void m22594bq(I18NString i18NString) {
        bFf().mo5608dq().mo3197f(f4646Qz, i18NString);
    }

    /* renamed from: br */
    private void m22595br(I18NString i18NString) {
        bFf().mo5608dq().mo3197f(f4642QA, i18NString);
    }

    /* renamed from: c */
    private void m22598c(UUID uuid) {
        switch (bFf().mo6893i(f4651bO)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f4651bO, new Object[]{uuid}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f4651bO, new Object[]{uuid}));
                break;
        }
        m22593b(uuid);
    }

    /* renamed from: vS */
    private I18NString m22600vS() {
        return (I18NString) bFf().mo5608dq().mo3214p(f4646Qz);
    }

    /* renamed from: vT */
    private I18NString m22601vT() {
        return (I18NString) bFf().mo5608dq().mo3214p(f4642QA);
    }

    /* renamed from: RU */
    public I18NString mo182RU() {
        switch (bFf().mo6893i(aMj)) {
            case 0:
                return null;
            case 2:
                return (I18NString) bFf().mo5606d(new aCE(this, aMj, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, aMj, new Object[0]));
                break;
        }
        return m22584RT();
    }

    /* renamed from: T */
    public C6494any mo11T() {
        return ___iScriptClass;
    }

    /* renamed from: U */
    public C2491fm[] mo12U() {
        return _m_methods;
    }

    /* renamed from: W */
    public Object mo13W() {
        return new C6612aqM(this);
    }

    /* renamed from: a */
    public Object mo14a(C0495Gr gr) {
        Object[] args = gr.getArgs();
        switch (gr.mo2417hq() - aDJ._m_methodCount) {
            case 0:
                return m22590ap();
            case 1:
                m22593b((UUID) args[0]);
                return null;
            case 2:
                return m22591ar();
            case 3:
                m22592b((String) args[0]);
                return null;
            case 4:
                return m22599rO();
            case 5:
                m22596bs((I18NString) args[0]);
                return null;
            case 6:
                return m22602vV();
            case 7:
                m22597bu((I18NString) args[0]);
                return null;
            case 8:
                return m22584RT();
            default:
                return super.mo14a(gr);
        }
    }

    /* renamed from: a */
    public void mo15a(Collection collection, C0495Gr gr) {
        super.mo15a(collection, gr);
    }

    /* renamed from: aq */
    public UUID mo18aq() {
        switch (bFf().mo6893i(f4650bN)) {
            case 0:
                return null;
            case 2:
                return (UUID) bFf().mo5606d(new aCE(this, f4650bN, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f4650bN, new Object[0]));
                break;
        }
        return m22590ap();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Title")
    /* renamed from: bt */
    public void mo13759bt(I18NString i18NString) {
        switch (bFf().mo6893i(f4643QD)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f4643QD, new Object[]{i18NString}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f4643QD, new Object[]{i18NString}));
                break;
        }
        m22596bs(i18NString);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Description")
    /* renamed from: bv */
    public void mo13760bv(I18NString i18NString) {
        switch (bFf().mo6893i(f4645QF)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f4645QF, new Object[]{i18NString}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f4645QF, new Object[]{i18NString}));
                break;
        }
        m22597bu(i18NString);
    }

    /* renamed from: c */
    public C5663aRz[] mo25c() {
        return _m_fields;
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Handle")
    public String getHandle() {
        switch (bFf().mo6893i(f4652bP)) {
            case 0:
                return null;
            case 2:
                return (String) bFf().mo5606d(new aCE(this, f4652bP, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f4652bP, new Object[0]));
                break;
        }
        return m22591ar();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Handle")
    @C5566aOg
    public void setHandle(String str) {
        switch (bFf().mo6893i(f4653bQ)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f4653bQ, new Object[]{str}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f4653bQ, new Object[]{str}));
                break;
        }
        m22592b(str);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Title")
    /* renamed from: rP */
    public I18NString mo195rP() {
        switch (bFf().mo6893i(f4641MN)) {
            case 0:
                return null;
            case 2:
                return (I18NString) bFf().mo5606d(new aCE(this, f4641MN, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f4641MN, new Object[0]));
                break;
        }
        return m22599rO();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Description")
    /* renamed from: vW */
    public I18NString mo13762vW() {
        switch (bFf().mo6893i(f4644QE)) {
            case 0:
                return null;
            case 2:
                return (I18NString) bFf().mo5606d(new aCE(this, f4644QE, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f4644QE, new Object[0]));
                break;
        }
        return m22602vV();
    }

    @C0064Am(aul = "51a53d559a5675d11c23e3aac0c52be1", aum = 0)
    /* renamed from: ap */
    private UUID m22590ap() {
        return m22588an();
    }

    @C0064Am(aul = "9e2a1ecce9972ca388baed7e95b71429", aum = 0)
    /* renamed from: b */
    private void m22593b(UUID uuid) {
        m22587a(uuid);
    }

    /* renamed from: S */
    public void mo10S() {
        super.mo10S();
        m22587a(UUID.randomUUID());
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Handle")
    @C0064Am(aul = "13f0152805a291762599b908bca3ded4", aum = 0)
    /* renamed from: ar */
    private String m22591ar() {
        return m22589ao();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Title")
    @C0064Am(aul = "a29a9c58d9369491aa501fbcefd705a4", aum = 0)
    /* renamed from: rO */
    private I18NString m22599rO() {
        return m22600vS();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Title")
    @C0064Am(aul = "4a2a10af5f168103e21402d697c3ecff", aum = 0)
    /* renamed from: bs */
    private void m22596bs(I18NString i18NString) {
        m22594bq(i18NString);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Description")
    @C0064Am(aul = "fab0e7c3c895e8663a28858c9c3910bc", aum = 0)
    /* renamed from: vV */
    private I18NString m22602vV() {
        return m22601vT();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Description")
    @C0064Am(aul = "e130ad826d4659bc8a1e06588f74f240", aum = 0)
    /* renamed from: bu */
    private void m22597bu(I18NString i18NString) {
        m22595br(i18NString);
    }

    @C0064Am(aul = "e64c3a83cb704e0886f679631990b4f7", aum = 0)
    /* renamed from: RT */
    private I18NString m22584RT() {
        return mo13762vW();
    }
}
