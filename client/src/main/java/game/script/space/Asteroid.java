package game.script.space;

import com.hoplon.geometry.Vec3f;
import game.CollisionFXSet;
import game.geometry.Vec3d;
import game.network.message.externalizable.C1042PH;
import game.network.message.externalizable.aCE;
import game.network.message.serializable.C3438ra;
import game.script.Actor;
import game.script.itemgen.ItemGenTable;
import game.script.player.Player;
import game.script.resource.Asset;
import game.script.ship.Hull;
import game.script.ship.Shield;
import game.script.simulation.Space;
import game.script.spacezone.AsteroidZoneCategory;
import logic.aaa.C2235dB;
import logic.baa.*;
import logic.bbb.C4029yK;
import logic.data.link.C2530gR;
import logic.data.link.C2911li;
import logic.data.mbean.C5363aGl;
import logic.res.LoaderTrail;
import logic.res.code.C5640aRc;
import logic.res.code.C5663aRz;
import logic.res.code.C6494any;
import logic.res.html.C2491fm;
import logic.sql.C1790aE;
import logic.sql.C5878acG;
import logic.thred.C6339akz;
import p001a.*;
import taikodom.infra.script.I18NString;

import java.util.Collection;
import java.util.Iterator;
import java.util.Set;

@C5829abJ("1.0.0")
@C6485anp
@C2712iu(mo19785Bs = {C6251ajP.class, C0405Fe.class}, mo19786Bt = BaseAsteroidType.class)
@C5511aMd
/* renamed from: a.aEg  reason: case insensitive filesystem */
/* compiled from: a */
public class Asteroid extends Actor implements C1616Xf, aDA, C2530gR.C2531a {
    /* renamed from: LR */
    public static final C5663aRz f2746LR = null;
    /* renamed from: Lm */
    public static final C2491fm f2747Lm = null;
    /* renamed from: Pf */
    public static final C2491fm f2748Pf = null;
    /* renamed from: VW */
    public static final C5663aRz f2750VW = null;
    /* renamed from: Wc */
    public static final C5663aRz f2752Wc = null;
    /* renamed from: We */
    public static final C5663aRz f2754We = null;
    /* renamed from: Wk */
    public static final C2491fm f2755Wk = null;
    /* renamed from: Wl */
    public static final C2491fm f2756Wl = null;
    /* renamed from: Wm */
    public static final C2491fm f2757Wm = null;
    /* renamed from: Wu */
    public static final C2491fm f2758Wu = null;
    public static final C2491fm _f_dispose_0020_0028_0029V = null;
    public static final C2491fm _f_getType_0020_0028_0029Ljava_002flang_002fObject_003b = null;
    public static final C2491fm _f_onResurrect_0020_0028_0029V = null;
    public static final C2491fm _f_toString_0020_0028_0029Ljava_002flang_002fString_003b = null;
    public static final int _m_fieldCount = 0;
    public static final C5663aRz[] _m_fields = null;
    public static final int _m_methodCount = 0;
    public static final C2491fm[] _m_methods = null;
    public static final C5663aRz bnF = null;
    public static final C5663aRz bnN = null;
    public static final C5663aRz bod = null;
    public static final C2491fm bqK = null;
    public static final C2491fm bqM = null;
    public static final C5663aRz cKB = null;
    public static final C5663aRz fmC = null;
    public static final C2491fm hWI = null;
    public static final C2491fm hWJ = null;
    public static final C2491fm hWK = null;
    public static final C2491fm hWL = null;
    public static final C2491fm hWM = null;
    public static final C2491fm hWN = null;
    public static final C2491fm hWO = null;
    /* renamed from: uU */
    public static final C5663aRz f2760uU = null;
    /* renamed from: uX */
    public static final C2491fm f2761uX = null;
    /* renamed from: uY */
    public static final C2491fm f2762uY = null;
    /* renamed from: vc */
    public static final C2491fm f2763vc = null;
    /* renamed from: ve */
    public static final C2491fm f2764ve = null;
    /* renamed from: vi */
    public static final C2491fm f2765vi = null;
    /* renamed from: vm */
    public static final C2491fm f2766vm = null;
    /* renamed from: vn */
    public static final C2491fm f2767vn = null;
    /* renamed from: vo */
    public static final C2491fm f2768vo = null;
    /* renamed from: vs */
    public static final C2491fm f2769vs = null;
    /* renamed from: zQ */
    public static final C5663aRz f2771zQ = null;

    public static C6494any ___iScriptClass = null;
    @C0064Am(aul = "fd5eb56145f6fdd734f5fe2edc69af28", aum = 6)

    /* renamed from: LQ */
    private static Asset f2745LQ = null;
    @C0064Am(aul = "386f8c2aec570f4b144e7bcf6e189aa4", aum = 9)
    @C0803Ld

    /* renamed from: VV */
    private static Hull f2749VV = null;
    @C0064Am(aul = "6e1e5289241a3cb8bb9adb95efc4d271", aum = 1)

    /* renamed from: Wb */
    private static Asset f2751Wb = null;
    @C0064Am(aul = "4ab96e3a77f4141ae910d50fd07db76f", aum = 2)

    /* renamed from: Wd */
    private static Asset f2753Wd = null;
    @C0064Am(aul = "14f2056516293f98f34b1f1bb7a7e9e2", aum = 3)
    private static C3438ra<ItemGenTable> ayZ = null;
    @C0064Am(aul = "ff06521294be3eeaa6ddc4a1441274ff", aum = 0)
    @C5454aJy("Collisions FX")
    private static CollisionFXSet bnE = null;
    @C0064Am(aul = "d81325dc52e6d162b8e3a6cd145f4229", aum = 7)
    private static Asset boc = null;
    @C0064Am(aul = "25863e8583c60570c2ba57ec8919328c", aum = 10)
    private static AsteroidZoneCategory hPq = null;
    @C0064Am(aul = "e61a7266c1c05a5e74de0070d535295b", aum = 4)
    private static AsteroidLootType hhb = null;
    @C0064Am(aul = "af9194eb00f825764b0fe8f86c5405f9", aum = 8)

    /* renamed from: uT */
    private static String f2759uT;
    @C0064Am(aul = "3e481a4db7a93f2c52538a091ae46145", aum = 5)

    /* renamed from: zP */
    private static I18NString f2770zP;

    static {
        m14268V();
    }

    @ClientOnly
    private float radius;

    public Asteroid() {
        super((C5540aNg) null);
        super.mo10S();
    }

    public Asteroid(C5540aNg ang) {
        super(ang);
    }

    public Asteroid(AsteroidType aqn) {
        super((C5540aNg) null);
        super._m_script_init(aqn);
    }

    /* renamed from: V */
    static void m14268V() {
        _m_fieldCount = Actor._m_fieldCount + 11;
        _m_methodCount = Actor._m_methodCount + 28;
        int i = Actor._m_fieldCount;
        C5663aRz[] arzArr = new C5663aRz[(i + 11)];
        C5663aRz b = C5640aRc.m17844b(Asteroid.class, "ff06521294be3eeaa6ddc4a1441274ff", i);
        bnF = b;
        arzArr[i] = b;
        int i2 = i + 1;
        C5663aRz b2 = C5640aRc.m17844b(Asteroid.class, "6e1e5289241a3cb8bb9adb95efc4d271", i2);
        f2752Wc = b2;
        arzArr[i2] = b2;
        int i3 = i2 + 1;
        C5663aRz b3 = C5640aRc.m17844b(Asteroid.class, "4ab96e3a77f4141ae910d50fd07db76f", i3);
        f2754We = b3;
        arzArr[i3] = b3;
        int i4 = i3 + 1;
        C5663aRz b4 = C5640aRc.m17844b(Asteroid.class, "14f2056516293f98f34b1f1bb7a7e9e2", i4);
        fmC = b4;
        arzArr[i4] = b4;
        int i5 = i4 + 1;
        C5663aRz b5 = C5640aRc.m17844b(Asteroid.class, "e61a7266c1c05a5e74de0070d535295b", i5);
        bnN = b5;
        arzArr[i5] = b5;
        int i6 = i5 + 1;
        C5663aRz b6 = C5640aRc.m17844b(Asteroid.class, "3e481a4db7a93f2c52538a091ae46145", i6);
        f2771zQ = b6;
        arzArr[i6] = b6;
        int i7 = i6 + 1;
        C5663aRz b7 = C5640aRc.m17844b(Asteroid.class, "fd5eb56145f6fdd734f5fe2edc69af28", i7);
        f2746LR = b7;
        arzArr[i7] = b7;
        int i8 = i7 + 1;
        C5663aRz b8 = C5640aRc.m17844b(Asteroid.class, "d81325dc52e6d162b8e3a6cd145f4229", i8);
        bod = b8;
        arzArr[i8] = b8;
        int i9 = i8 + 1;
        C5663aRz b9 = C5640aRc.m17844b(Asteroid.class, "af9194eb00f825764b0fe8f86c5405f9", i9);
        f2760uU = b9;
        arzArr[i9] = b9;
        int i10 = i9 + 1;
        C5663aRz b10 = C5640aRc.m17844b(Asteroid.class, "386f8c2aec570f4b144e7bcf6e189aa4", i10);
        f2750VW = b10;
        arzArr[i10] = b10;
        int i11 = i10 + 1;
        C5663aRz b11 = C5640aRc.m17844b(Asteroid.class, "25863e8583c60570c2ba57ec8919328c", i11);
        cKB = b11;
        arzArr[i11] = b11;
        int i12 = i11 + 1;
        _m_fields = arzArr;
        C1634Xv.m11725a((Object[]) Actor._m_fields, (Object[]) _m_fields);
        int i13 = Actor._m_methodCount;
        C2491fm[] fmVarArr = new C2491fm[(i13 + 28)];
        C2491fm a = C4105zY.m41624a(Asteroid.class, "9e121de164258ed3b3a18ee9a1446336", i13);
        hWI = a;
        fmVarArr[i13] = a;
        int i14 = i13 + 1;
        C2491fm a2 = C4105zY.m41624a(Asteroid.class, "330a2777b6dc23ab2458eaf122585587", i14);
        hWJ = a2;
        fmVarArr[i14] = a2;
        int i15 = i14 + 1;
        C2491fm a3 = C4105zY.m41624a(Asteroid.class, "42a2a46c17f722cb57ba10f6e66f333a", i15);
        hWK = a3;
        fmVarArr[i15] = a3;
        int i16 = i15 + 1;
        C2491fm a4 = C4105zY.m41624a(Asteroid.class, "c5e3296852ea4292a9c184404634387b", i16);
        f2761uX = a4;
        fmVarArr[i16] = a4;
        int i17 = i16 + 1;
        C2491fm a5 = C4105zY.m41624a(Asteroid.class, "281bd96c1b16d4906234772e6f43f092", i17);
        f2766vm = a5;
        fmVarArr[i17] = a5;
        int i18 = i17 + 1;
        C2491fm a6 = C4105zY.m41624a(Asteroid.class, "a39365ff4501b5d801b8bd44318e6ba1", i18);
        f2769vs = a6;
        fmVarArr[i18] = a6;
        int i19 = i18 + 1;
        C2491fm a7 = C4105zY.m41624a(Asteroid.class, "74d7b19c8377ab7e89763b725fd9acf7", i19);
        f2767vn = a7;
        fmVarArr[i19] = a7;
        int i20 = i19 + 1;
        C2491fm a8 = C4105zY.m41624a(Asteroid.class, "7706c3f1429f7c3cbaa495d73c29e115", i20);
        _f_dispose_0020_0028_0029V = a8;
        fmVarArr[i20] = a8;
        int i21 = i20 + 1;
        C2491fm a9 = C4105zY.m41624a(Asteroid.class, "ba9bea24b5e4a9a57d31f55962568152", i21);
        f2756Wl = a9;
        fmVarArr[i21] = a9;
        int i22 = i21 + 1;
        C2491fm a10 = C4105zY.m41624a(Asteroid.class, "ca27c472a90389b7634359e09159f843", i22);
        f2748Pf = a10;
        fmVarArr[i22] = a10;
        int i23 = i22 + 1;
        C2491fm a11 = C4105zY.m41624a(Asteroid.class, "69f6930e639e4108bb9cfa7a37e1da73", i23);
        f2763vc = a11;
        fmVarArr[i23] = a11;
        int i24 = i23 + 1;
        C2491fm a12 = C4105zY.m41624a(Asteroid.class, "525755989eea97b96bb430b3ade9d6fe", i24);
        f2764ve = a12;
        fmVarArr[i24] = a12;
        int i25 = i24 + 1;
        C2491fm a13 = C4105zY.m41624a(Asteroid.class, "9dbcb73a5609bdad241ba407fc1f30ce", i25);
        f2768vo = a13;
        fmVarArr[i25] = a13;
        int i26 = i25 + 1;
        C2491fm a14 = C4105zY.m41624a(Asteroid.class, "95edff291851c87176d2073e246a911e", i26);
        f2757Wm = a14;
        fmVarArr[i26] = a14;
        int i27 = i26 + 1;
        C2491fm a15 = C4105zY.m41624a(Asteroid.class, "eea1adf2ea44992279e5c8a60f4ccb84", i27);
        f2765vi = a15;
        fmVarArr[i27] = a15;
        int i28 = i27 + 1;
        C2491fm a16 = C4105zY.m41624a(Asteroid.class, "0cae12ee1040e2445452be1de02d667f", i28);
        hWL = a16;
        fmVarArr[i28] = a16;
        int i29 = i28 + 1;
        C2491fm a17 = C4105zY.m41624a(Asteroid.class, "2ec32509e103119c46c7ec6a3d555be1", i29);
        hWM = a17;
        fmVarArr[i29] = a17;
        int i30 = i29 + 1;
        C2491fm a18 = C4105zY.m41624a(Asteroid.class, "e4a2e013d8da1a7221dd368e858e1f52", i30);
        hWN = a18;
        fmVarArr[i30] = a18;
        int i31 = i30 + 1;
        C2491fm a19 = C4105zY.m41624a(Asteroid.class, "4d8a3e6553f9a9f29bdf2c83d66b9002", i31);
        bqK = a19;
        fmVarArr[i31] = a19;
        int i32 = i31 + 1;
        C2491fm a20 = C4105zY.m41624a(Asteroid.class, "c35d291a506b5ce8d88da2fd4c97c528", i32);
        f2755Wk = a20;
        fmVarArr[i32] = a20;
        int i33 = i32 + 1;
        C2491fm a21 = C4105zY.m41624a(Asteroid.class, "00ee121d4c426efb4ffeca329b1d0127", i33);
        _f_onResurrect_0020_0028_0029V = a21;
        fmVarArr[i33] = a21;
        int i34 = i33 + 1;
        C2491fm a22 = C4105zY.m41624a(Asteroid.class, "6ebd46d395df670f195d88528148e184", i34);
        f2747Lm = a22;
        fmVarArr[i34] = a22;
        int i35 = i34 + 1;
        C2491fm a23 = C4105zY.m41624a(Asteroid.class, "2c65fd8d4bb7aa2ae162a297c0b2421c", i35);
        hWO = a23;
        fmVarArr[i35] = a23;
        int i36 = i35 + 1;
        C2491fm a24 = C4105zY.m41624a(Asteroid.class, "756196234cf2e983d36b3c2ffff3e44a", i36);
        f2758Wu = a24;
        fmVarArr[i36] = a24;
        int i37 = i36 + 1;
        C2491fm a25 = C4105zY.m41624a(Asteroid.class, "90b208b0619f3d00cc061e6535dc3b0b", i37);
        _f_toString_0020_0028_0029Ljava_002flang_002fString_003b = a25;
        fmVarArr[i37] = a25;
        int i38 = i37 + 1;
        C2491fm a26 = C4105zY.m41624a(Asteroid.class, "992a3d65500ee310adea377f4d91c33c", i38);
        f2762uY = a26;
        fmVarArr[i38] = a26;
        int i39 = i38 + 1;
        C2491fm a27 = C4105zY.m41624a(Asteroid.class, "d9734845106eb827a9c01c93f30273fc", i39);
        bqM = a27;
        fmVarArr[i39] = a27;
        int i40 = i39 + 1;
        C2491fm a28 = C4105zY.m41624a(Asteroid.class, "b77926ef4a9c46ac02ba4779d8ed433f", i40);
        _f_getType_0020_0028_0029Ljava_002flang_002fObject_003b = a28;
        fmVarArr[i40] = a28;
        int i41 = i40 + 1;
        _m_methods = fmVarArr;
        C1634Xv.m11725a((Object[]) Actor._m_methods, (Object[]) _m_methods);
        ___iScriptClass = aUO.m18566a(Asteroid.class, C5363aGl.class, _m_fields, _m_methods);
    }

    /* renamed from: A */
    private void m14266A(Asset tCVar) {
        throw new C6039afL();
    }

    /* renamed from: H */
    private void m14267H(String str) {
        throw new C6039afL();
    }

    /* renamed from: W */
    private void m14269W(Asset tCVar) {
        throw new C6039afL();
    }

    @C0064Am(aul = "2c65fd8d4bb7aa2ae162a297c0b2421c", aum = 0)
    @C5566aOg
    /* renamed from: a */
    private Set<Loot> m14270a(Actor cr, Vec3d ajr) {
        throw new aWi(new aCE(this, hWO, new Object[]{cr, ajr}));
    }

    @C4034yP
    @C5566aOg
    @C0064Am(aul = "281bd96c1b16d4906234772e6f43f092", aum = 0)
    @C2499fr
    /* renamed from: a */
    private void m14272a(Actor cr, C5260aCm acm, Vec3f vec3f, Vec3f vec3f2) {
        throw new aWi(new aCE(this, f2766vm, new Object[]{cr, acm, vec3f, vec3f2}));
    }

    /* renamed from: a */
    private void m14275a(Hull jRVar) {
        bFf().mo5608dq().mo3197f(f2750VW, jRVar);
    }

    /* renamed from: a */
    private void m14276a(CollisionFXSet yaVar) {
        throw new C6039afL();
    }

    private CollisionFXSet aeJ() {
        return ((AsteroidType) getType()).bkb();
    }

    private Asset aeX() {
        return ((AsteroidType) getType()).bkx();
    }

    @C5566aOg
    /* renamed from: b */
    private Set<Loot> m14279b(Actor cr, Vec3d ajr) {
        switch (bFf().mo6893i(hWO)) {
            case 0:
                return null;
            case 2:
                return (Set) bFf().mo5606d(new aCE(this, hWO, new Object[]{cr, ajr}));
            case 3:
                bFf().mo5606d(new aCE(this, hWO, new Object[]{cr, ajr}));
                break;
        }
        return m14270a(cr, ajr);
    }

    @C4034yP
    @ClientOnly
    @C2499fr
    /* renamed from: b */
    private void m14280b(Vec3d ajr, Node rPVar, StellarSystem jj, Set<Loot> set) {
        switch (bFf().mo6893i(bqK)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, bqK, new Object[]{ajr, rPVar, jj, set}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, bqK, new Object[]{ajr, rPVar, jj, set}));
                break;
        }
        m14274a(ajr, rPVar, jj, set);
    }

    /* renamed from: b */
    private void m14281b(AsteroidLootType agm) {
        throw new C6039afL();
    }

    /* renamed from: bL */
    private void m14283bL(C3438ra raVar) {
        throw new C6039afL();
    }

    private C3438ra bTz() {
        return ((AsteroidType) getType()).bTU();
    }

    @C0064Am(aul = "9e121de164258ed3b3a18ee9a1446336", aum = 0)
    @C6580apg
    /* renamed from: cj */
    private void m14288cj(Actor cr) {
        bFf().mo5600a((Class<? extends C4062yl>) C2911li.C2912a.class, (C0495Gr) new aCE(this, hWI, new Object[]{cr}));
    }

    @C4034yP
    /* renamed from: d */
    private void m14289d(Vec3d ajr, Node rPVar, StellarSystem jj, Set<Loot> set) {
        switch (bFf().mo6893i(hWN)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, hWN, new Object[]{ajr, rPVar, jj, set}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, hWN, new Object[]{ajr, rPVar, jj, set}));
                break;
        }
        m14286c(ajr, rPVar, jj, set);
    }

    @C4034yP
    @C2499fr
    @ClientOnly
    /* renamed from: d */
    private void m14290d(Vec3f vec3f, Node rPVar, StellarSystem jj, String str) {
        switch (bFf().mo6893i(hWM)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, hWM, new Object[]{vec3f, rPVar, jj, str}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, hWM, new Object[]{vec3f, rPVar, jj, str}));
                break;
        }
        m14287c(vec3f, rPVar, jj, str);
    }

    /* renamed from: d */
    private void m14291d(I18NString i18NString) {
        throw new C6039afL();
    }

    private AsteroidLootType ddb() {
        return ((AsteroidType) getType()).dpL();
    }

    private AsteroidZoneCategory ddc() {
        return (AsteroidZoneCategory) bFf().mo5608dq().mo3214p(cKB);
    }

    @C4034yP
    @C5566aOg
    @C0064Am(aul = "756196234cf2e983d36b3c2ffff3e44a", aum = 0)
    @C2499fr(mo18857qh = 2)
    /* renamed from: e */
    private void m14292e(Actor cr, C5260aCm acm, Vec3f vec3f, Vec3f vec3f2) {
        throw new aWi(new aCE(this, f2758Wu, new Object[]{cr, acm, vec3f, vec3f2}));
    }

    @C0064Am(aul = "7706c3f1429f7c3cbaa495d73c29e115", aum = 0)
    @C5566aOg
    @C2499fr
    /* renamed from: fg */
    private void m14293fg() {
        throw new aWi(new aCE(this, _f_dispose_0020_0028_0029V, new Object[0]));
    }

    /* renamed from: h */
    private void m14294h(AsteroidZoneCategory nWVar) {
        bFf().mo5608dq().mo3197f(cKB, nWVar);
    }

    @C0064Am(aul = "42a2a46c17f722cb57ba10f6e66f333a", aum = 0)
    @C5566aOg
    /* renamed from: i */
    private void m14295i(AsteroidZoneCategory nWVar) {
        throw new aWi(new aCE(this, hWK, new Object[]{nWVar}));
    }

    /* renamed from: ij */
    private String m14296ij() {
        return ((AsteroidType) getType()).mo11088iu();
    }

    /* renamed from: kb */
    private I18NString m14301kb() {
        return ((AsteroidType) getType()).mo11089ke();
    }

    @C0064Am(aul = "6ebd46d395df670f195d88528148e184", aum = 0)
    @C5566aOg
    @C2499fr
    /* renamed from: qU */
    private void m14302qU() {
        throw new aWi(new aCE(this, f2747Lm, new Object[0]));
    }

    @C0064Am(aul = "b77926ef4a9c46ac02ba4779d8ed433f", aum = 0)
    /* renamed from: qW */
    private Object m14303qW() {
        return ddg();
    }

    /* renamed from: r */
    private void m14304r(Asset tCVar) {
        throw new C6039afL();
    }

    /* renamed from: r */
    private void m14305r(Collection collection, C0495Gr gr) {
        Iterator it = collection.iterator();
        while (it.hasNext()) {
            ((C2911li.C2912a) it.next()).mo17467a(this, (Actor) gr.getArgs()[0]);
        }
    }

    /* renamed from: rh */
    private Asset m14306rh() {
        return ((AsteroidType) getType()).mo3521Nu();
    }

    /* renamed from: z */
    private void m14308z(Asset tCVar) {
        throw new C6039afL();
    }

    /* renamed from: zi */
    private Hull m14309zi() {
        return (Hull) bFf().mo5608dq().mo3214p(f2750VW);
    }

    /* renamed from: zl */
    private Asset m14310zl() {
        return ((AsteroidType) getType()).bkh();
    }

    /* renamed from: zm */
    private Asset m14311zm() {
        return ((AsteroidType) getType()).bkj();
    }

    /* renamed from: Fe */
    public void mo957Fe() {
        switch (bFf().mo6893i(bqM)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, bqM, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, bqM, new Object[0]));
                break;
        }
        ahp();
    }

    /* renamed from: S */
    public void mo10S() {
        super.mo10S();
    }

    /* renamed from: T */
    public C6494any mo11T() {
        return ___iScriptClass;
    }

    /* renamed from: U */
    public C2491fm[] mo12U() {
        return _m_methods;
    }

    /* renamed from: W */
    public Object mo13W() {
        return new C5363aGl(this);
    }

    /* renamed from: a */
    public Object mo14a(C0495Gr gr) {
        Object[] args = gr.getArgs();
        switch (gr.mo2417hq() - Actor._m_methodCount) {
            case 0:
                m14288cj((Actor) args[0]);
                return null;
            case 1:
                return ddd();
            case 2:
                m14295i((AsteroidZoneCategory) args[0]);
                return null;
            case 3:
                m14273a((Actor) args[0], (Vec3f) args[1], (Vec3f) args[2], ((Float) args[3]).floatValue(), (C6339akz) args[4]);
                return null;
            case 4:
                m14272a((Actor) args[0], (C5260aCm) args[1], (Vec3f) args[2], (Vec3f) args[3]);
                return null;
            case 5:
                m14271a((Actor.C0200h) args[0]);
                return null;
            case 6:
                m14285c((Actor) args[0], (C5260aCm) args[1], (Vec3f) args[2], (Vec3f) args[3]);
                return null;
            case 7:
                m14293fg();
                return null;
            case 8:
                return m14312zs();
            case 9:
                return m14307uV();
            case 10:
                return m14297io();
            case 11:
                return m14298iq();
            case 12:
                return m14300iz();
            case 13:
                return m14313zu();
            case 14:
                return m14299it();
            case 15:
                return ddf();
            case 16:
                m14287c((Vec3f) args[0], (Node) args[1], (StellarSystem) args[2], (String) args[3]);
                return null;
            case 17:
                m14286c((Vec3d) args[0], (Node) args[1], (StellarSystem) args[2], (Set<Loot>) (Set) args[3]);
                return null;
            case 18:
                m14274a((Vec3d) args[0], (Node) args[1], (StellarSystem) args[2], (Set<Loot>) (Set) args[3]);
                return null;
            case 19:
                m14282b((Hull) args[0], (C5260aCm) args[1], (Actor) args[2]);
                return null;
            case 20:
                m14277aG();
                return null;
            case 21:
                m14302qU();
                return null;
            case 22:
                return m14270a((Actor) args[0], (Vec3d) args[1]);
            case 23:
                m14292e((Actor) args[0], (C5260aCm) args[1], (Vec3f) args[2], (Vec3f) args[3]);
                return null;
            case 24:
                return m14278au();
            case 25:
                return m14284c((Space) args[0], ((Long) args[1]).longValue());
            case 26:
                ahp();
                return null;
            case 27:
                return m14303qW();
            default:
                return super.mo14a(gr);
        }
    }

    /* renamed from: a */
    public void mo8536a(Hull jRVar, C5260aCm acm, Actor cr) {
        switch (bFf().mo6893i(f2755Wk)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f2755Wk, new Object[]{jRVar, acm, cr}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f2755Wk, new Object[]{jRVar, acm, cr}));
                break;
        }
        m14282b(jRVar, acm, cr);
    }

    /* renamed from: a */
    public void mo15a(Collection collection, C0495Gr gr) {
        switch (gr.mo2417hq() - Actor._m_methodCount) {
            case 0:
                m14305r(collection, gr);
                return;
            default:
                super.mo15a(collection, gr);
                return;
        }
    }

    /* renamed from: aH */
    public void mo70aH() {
        switch (bFf().mo6893i(_f_onResurrect_0020_0028_0029V)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, _f_onResurrect_0020_0028_0029V, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, _f_onResurrect_0020_0028_0029V, new Object[0]));
                break;
        }
        m14277aG();
    }

    /* renamed from: b */
    public void mo620b(Actor.C0200h hVar) {
        switch (bFf().mo6893i(f2769vs)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f2769vs, new Object[]{hVar}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f2769vs, new Object[]{hVar}));
                break;
        }
        m14271a(hVar);
    }

    @C4034yP
    @C5566aOg
    @C2499fr
    /* renamed from: b */
    public void mo621b(Actor cr, C5260aCm acm, Vec3f vec3f, Vec3f vec3f2) {
        switch (bFf().mo6893i(f2766vm)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f2766vm, new Object[]{cr, acm, vec3f, vec3f2}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f2766vm, new Object[]{cr, acm, vec3f, vec3f2}));
                break;
        }
        m14272a(cr, acm, vec3f, vec3f2);
    }

    /* renamed from: b */
    public void mo992b(Actor cr, Vec3f vec3f, Vec3f vec3f2, float f, C6339akz akz) {
        switch (bFf().mo6893i(f2761uX)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f2761uX, new Object[]{cr, vec3f, vec3f2, new Float(f), akz}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f2761uX, new Object[]{cr, vec3f, vec3f2, new Float(f), akz}));
                break;
        }
        m14273a(cr, vec3f, vec3f2, f, akz);
    }

    /* renamed from: c */
    public C5663aRz[] mo25c() {
        return _m_fields;
    }

    /* access modifiers changed from: protected */
    @C6580apg
    /* renamed from: ck */
    public void mo8649ck(Actor cr) {
        switch (bFf().mo6893i(hWI)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, hWI, new Object[]{cr}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, hWI, new Object[]{cr}));
                break;
        }
        m14288cj(cr);
    }

    /* access modifiers changed from: protected */
    @C2198cg
    @C1253SX
    /* renamed from: d */
    public C0520HN mo635d(Space ea, long j) {
        switch (bFf().mo6893i(f2762uY)) {
            case 0:
                return null;
            case 2:
                return (C0520HN) bFf().mo5606d(new aCE(this, f2762uY, new Object[]{ea, new Long(j)}));
            case 3:
                bFf().mo5606d(new aCE(this, f2762uY, new Object[]{ea, new Long(j)}));
                break;
        }
        return m14284c(ea, j);
    }

    /* renamed from: d */
    public void mo636d(Actor cr, C5260aCm acm, Vec3f vec3f, Vec3f vec3f2) {
        switch (bFf().mo6893i(f2767vn)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f2767vn, new Object[]{cr, acm, vec3f, vec3f2}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f2767vn, new Object[]{cr, acm, vec3f, vec3f2}));
                break;
        }
        m14285c(cr, acm, vec3f, vec3f2);
    }

    public AsteroidZoneCategory dde() {
        switch (bFf().mo6893i(hWJ)) {
            case 0:
                return null;
            case 2:
                return (AsteroidZoneCategory) bFf().mo5606d(new aCE(this, hWJ, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, hWJ, new Object[0]));
                break;
        }
        return ddd();
    }

    public AsteroidType ddg() {
        switch (bFf().mo6893i(hWL)) {
            case 0:
                return null;
            case 2:
                return (AsteroidType) bFf().mo5606d(new aCE(this, hWL, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, hWL, new Object[0]));
                break;
        }
        return ddf();
    }

    @C5566aOg
    @C2499fr
    public void dispose() {
        switch (bFf().mo6893i(_f_dispose_0020_0028_0029V)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, _f_dispose_0020_0028_0029V, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, _f_dispose_0020_0028_0029V, new Object[0]));
                break;
        }
        m14293fg();
    }

    @C4034yP
    @C5566aOg
    @C2499fr(mo18857qh = 2)
    /* renamed from: f */
    public void mo1064f(Actor cr, C5260aCm acm, Vec3f vec3f, Vec3f vec3f2) {
        switch (bFf().mo6893i(f2758Wu)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f2758Wu, new Object[]{cr, acm, vec3f, vec3f2}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f2758Wu, new Object[]{cr, acm, vec3f, vec3f2}));
                break;
        }
        m14292e(cr, acm, vec3f, vec3f2);
    }

    public String getName() {
        switch (bFf().mo6893i(f2748Pf)) {
            case 0:
                return null;
            case 2:
                return (String) bFf().mo5606d(new aCE(this, f2748Pf, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f2748Pf, new Object[0]));
                break;
        }
        return m14307uV();
    }

    public /* bridge */ /* synthetic */ Object getType() {
        switch (bFf().mo6893i(_f_getType_0020_0028_0029Ljava_002flang_002fObject_003b)) {
            case 0:
                return null;
            case 2:
                return bFf().mo5606d(new aCE(this, _f_getType_0020_0028_0029Ljava_002flang_002fObject_003b, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, _f_getType_0020_0028_0029Ljava_002flang_002fObject_003b, new Object[0]));
                break;
        }
        return m14303qW();
    }

    /* renamed from: iA */
    public String mo647iA() {
        switch (bFf().mo6893i(f2768vo)) {
            case 0:
                return null;
            case 2:
                return (String) bFf().mo5606d(new aCE(this, f2768vo, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f2768vo, new Object[0]));
                break;
        }
        return m14300iz();
    }

    /* renamed from: ip */
    public String mo648ip() {
        switch (bFf().mo6893i(f2763vc)) {
            case 0:
                return null;
            case 2:
                return (String) bFf().mo5606d(new aCE(this, f2763vc, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f2763vc, new Object[0]));
                break;
        }
        return m14297io();
    }

    /* renamed from: ir */
    public String mo649ir() {
        switch (bFf().mo6893i(f2764ve)) {
            case 0:
                return null;
            case 2:
                return (String) bFf().mo5606d(new aCE(this, f2764ve, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f2764ve, new Object[0]));
                break;
        }
        return m14298iq();
    }

    /* renamed from: iu */
    public String mo651iu() {
        switch (bFf().mo6893i(f2765vi)) {
            case 0:
                return null;
            case 2:
                return (String) bFf().mo5606d(new aCE(this, f2765vi, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f2765vi, new Object[0]));
                break;
        }
        return m14299it();
    }

    @C5566aOg
    /* renamed from: j */
    public void mo8652j(AsteroidZoneCategory nWVar) {
        switch (bFf().mo6893i(hWK)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, hWK, new Object[]{nWVar}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, hWK, new Object[]{nWVar}));
                break;
        }
        m14295i(nWVar);
    }

    @C5566aOg
    @C2499fr
    /* renamed from: qV */
    public void mo656qV() {
        switch (bFf().mo6893i(f2747Lm)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f2747Lm, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f2747Lm, new Object[0]));
                break;
        }
        m14302qU();
    }

    public String toString() {
        switch (bFf().mo6893i(_f_toString_0020_0028_0029Ljava_002flang_002fString_003b)) {
            case 0:
                return null;
            case 2:
                return (String) bFf().mo5606d(new aCE(this, _f_toString_0020_0028_0029Ljava_002flang_002fString_003b, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, _f_toString_0020_0028_0029Ljava_002flang_002fString_003b, new Object[0]));
                break;
        }
        return m14278au();
    }

    /* renamed from: zt */
    public Hull mo8287zt() {
        switch (bFf().mo6893i(f2756Wl)) {
            case 0:
                return null;
            case 2:
                return (Hull) bFf().mo5606d(new aCE(this, f2756Wl, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f2756Wl, new Object[0]));
                break;
        }
        return m14312zs();
    }

    /* renamed from: zv */
    public Shield mo8288zv() {
        switch (bFf().mo6893i(f2757Wm)) {
            case 0:
                return null;
            case 2:
                return (Shield) bFf().mo5606d(new aCE(this, f2757Wm, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f2757Wm, new Object[0]));
                break;
        }
        return m14313zu();
    }

    /* renamed from: o */
    public void mo8653o(AsteroidType aqn) {
        super.mo967a((C2961mJ) aqn);
        this.radius = -1.0f;
        setStatic(true);
        if (bGZ()) {
            mo8287zt().mo8348d(C2530gR.C2531a.class, this);
        }
    }

    @C0064Am(aul = "330a2777b6dc23ab2458eaf122585587", aum = 0)
    private AsteroidZoneCategory ddd() {
        return ddc();
    }

    @C0064Am(aul = "c5e3296852ea4292a9c184404634387b", aum = 0)
    /* renamed from: a */
    private void m14273a(Actor cr, Vec3f vec3f, Vec3f vec3f2, float f, C6339akz akz) {
        if (bGX()) {
            ald().getEventManager().mo13975h(new C1042PH(this, cr, vec3f, vec3f2, f, akz));
        }
    }

    @C0064Am(aul = "a39365ff4501b5d801b8bd44318e6ba1", aum = 0)
    /* renamed from: a */
    private void m14271a(Actor.C0200h hVar) {
        if (m14306rh() == null) {
            throw new IllegalStateException("ShapedObject of class '" + getClass().getName() + "' does not have a RenderAsset");
        }
        String file = m14306rh().getFile();
        if (!bGX() || !(ald().getLoaderTrail() instanceof LoaderTrail)) {
            hVar.mo1103c(C2606hU.m32703b(file, false));
        } else {
            ((LoaderTrail) ald().getLoaderTrail()).mo19098a(file, new C1795a(hVar), getName(), false);
        }
    }

    @C0064Am(aul = "74d7b19c8377ab7e89763b725fd9acf7", aum = 0)
    /* renamed from: c */
    private void m14285c(Actor cr, C5260aCm acm, Vec3f vec3f, Vec3f vec3f2) {
    }

    @C0064Am(aul = "ba9bea24b5e4a9a57d31f55962568152", aum = 0)
    /* renamed from: zs */
    private Hull m14312zs() {
        return m14309zi();
    }

    @C0064Am(aul = "ca27c472a90389b7634359e09159f843", aum = 0)
    /* renamed from: uV */
    private String m14307uV() {
        return m14301kb().get();
    }

    @C0064Am(aul = "69f6930e639e4108bb9cfa7a37e1da73", aum = 0)
    /* renamed from: io */
    private String m14297io() {
        return m14306rh().getHandle();
    }

    @C0064Am(aul = "525755989eea97b96bb430b3ade9d6fe", aum = 0)
    /* renamed from: iq */
    private String m14298iq() {
        return m14306rh().getFile();
    }

    @C0064Am(aul = "9dbcb73a5609bdad241ba407fc1f30ce", aum = 0)
    /* renamed from: iz */
    private String m14300iz() {
        return null;
    }

    @C0064Am(aul = "95edff291851c87176d2073e246a911e", aum = 0)
    /* renamed from: zu */
    private Shield m14313zu() {
        return null;
    }

    @C0064Am(aul = "eea1adf2ea44992279e5c8a60f4ccb84", aum = 0)
    /* renamed from: it */
    private String m14299it() {
        return m14296ij();
    }

    @C0064Am(aul = "0cae12ee1040e2445452be1de02d667f", aum = 0)
    private AsteroidType ddf() {
        return (AsteroidType) super.getType();
    }

    @C4034yP
    @ClientOnly
    @C0064Am(aul = "2ec32509e103119c46c7ec6a3d555be1", aum = 0)
    @C2499fr
    /* renamed from: c */
    private void m14287c(Vec3f vec3f, Node rPVar, StellarSystem jj, String str) {
    }

    @C4034yP
    @C0064Am(aul = "e4a2e013d8da1a7221dd368e858e1f52", aum = 0)
    /* renamed from: c */
    private void m14286c(Vec3d ajr, Node rPVar, StellarSystem jj, Set<Loot> set) {
        m14280b(ajr, rPVar, jj, set);
    }

    @C4034yP
    @ClientOnly
    @C0064Am(aul = "4d8a3e6553f9a9f29bdf2c83d66b9002", aum = 0)
    @C2499fr
    /* renamed from: a */
    private void m14274a(Vec3d ajr, Node rPVar, StellarSystem jj, Set<Loot> set) {
        if (mo1000b(rPVar, jj) && ddg() != null) {
            new C3257pi().mo21209a(m14310zl(), ajr, C3257pi.C3261d.MID_RANGE);
            new C3257pi().mo21210a(m14311zm(), ajr, C3257pi.C3261d.MID_RANGE, this.radius);
        }
        if (set != null) {
            for (Loot b : set) {
                b.mo993b(mo960Nc());
            }
        }
    }

    @C0064Am(aul = "c35d291a506b5ce8d88da2fd4c97c528", aum = 0)
    /* renamed from: b */
    private void m14282b(Hull jRVar, C5260aCm acm, Actor cr) {
        if (bae()) {
            m14289d(getPosition(), azW(), mo960Nc(), m14279b(cr, getPosition()));
        }
        if (mo960Nc() != null) {
            mo1099zx();
        }
        if ((cr instanceof Pawn) && (((Pawn) cr).mo2998hb() instanceof PlayerController) && ((PlayerController) ((Pawn) cr).mo2998hb()).mo22135dL() != null) {
            Player dL = ((PlayerController) ((Pawn) cr).mo2998hb()).mo22135dL();
            dL.mo14348am("destroy.asteroid", "");
            mo2066a((C5878acG) new C1790aE(dL, ddg().getHandle()));
        }
        mo8649ck(cr);
    }

    @C0064Am(aul = "00ee121d4c426efb4ffeca329b1d0127", aum = 0)
    /* renamed from: aG */
    private void m14277aG() {
        super.mo70aH();
    }

    @C0064Am(aul = "90b208b0619f3d00cc061e6535dc3b0b", aum = 0)
    /* renamed from: au */
    private String m14278au() {
        return "Asteroid: [" + getName() + "]";
    }

    @C0064Am(aul = "992a3d65500ee310adea377f4d91c33c", aum = 0)
    @C2198cg
    @C1253SX
    /* renamed from: c */
    private C0520HN m14284c(Space ea, long j) {
        aHO aho = new aHO(ea, this, mo958IL());
        aho.mo2449a(ea.cKn().mo5725a(j, (C2235dB) aho, 13));
        return aho;
    }

    @C0064Am(aul = "d9734845106eb827a9c01c93f30273fc", aum = 0)
    private void ahp() {
        super.mo957Fe();
        this.radius = (float) this.hks.getAabbLSLenght();
    }

    /* renamed from: a.aEg$a */
    class C1795a implements C2601hQ {
        private final /* synthetic */ Actor.C0200h eyn;

        C1795a(Actor.C0200h hVar) {
            this.eyn = hVar;
        }

        /* renamed from: b */
        public void mo663b(C4029yK yKVar) {
            this.eyn.mo1103c(yKVar);
        }
    }
}
