package game.script.space;

import game.network.message.externalizable.aCE;
import logic.baa.*;
import logic.data.mbean.axZ;
import logic.res.code.C5663aRz;
import logic.res.code.C6494any;
import logic.res.html.C2491fm;
import p001a.*;

import java.util.Collection;

@C0566Hp
@C5511aMd
@C6485anp
/* renamed from: a.agm  reason: case insensitive filesystem */
/* compiled from: a */
public class AsteroidLootType extends LootType implements C1616Xf {
    public static final int _m_fieldCount = 0;
    public static final C5663aRz[] _m_fields = null;
    public static final int _m_methodCount = 0;
    public static final C2491fm[] _m_methods = null;
    /* renamed from: dN */
    public static final C2491fm f4531dN = null;
    public static final C2491fm fxP = null;
    public static final long serialVersionUID = 0;
    public static C6494any ___iScriptClass;

    static {
        m22110V();
    }

    public AsteroidLootType() {
        super((C5540aNg) null);
        super.mo10S();
    }

    public AsteroidLootType(C5540aNg ang) {
        super(ang);
    }

    /* renamed from: V */
    static void m22110V() {
        serialVersionUID = (long) 1;
        _m_fieldCount = LootType._m_fieldCount + 0;
        _m_methodCount = LootType._m_methodCount + 2;
        _m_fields = new C5663aRz[(LootType._m_fieldCount + 0)];
        C1634Xv.m11725a((Object[]) LootType._m_fields, (Object[]) _m_fields);
        int i = LootType._m_methodCount;
        C2491fm[] fmVarArr = new C2491fm[(i + 2)];
        C2491fm a = C4105zY.m41624a(AsteroidLootType.class, "5dd9565a8332af10a5f2ac216a10f28f", i);
        fxP = a;
        fmVarArr[i] = a;
        int i2 = i + 1;
        C2491fm a2 = C4105zY.m41624a(AsteroidLootType.class, "ccc925cffc1b0788b982add506d07a22", i2);
        f4531dN = a2;
        fmVarArr[i2] = a2;
        int i3 = i2 + 1;
        _m_methods = fmVarArr;
        C1634Xv.m11725a((Object[]) LootType._m_methods, (Object[]) _m_methods);
        ___iScriptClass = aUO.m18566a(AsteroidLootType.class, axZ.class, _m_fields, _m_methods);
    }

    /* renamed from: T */
    public C6494any mo11T() {
        return ___iScriptClass;
    }

    /* renamed from: U */
    public C2491fm[] mo12U() {
        return _m_methods;
    }

    /* renamed from: W */
    public Object mo13W() {
        return new axZ(this);
    }

    /* renamed from: a */
    public Object mo14a(C0495Gr gr) {
        gr.getArgs();
        switch (gr.mo2417hq() - LootType._m_methodCount) {
            case 0:
                return bWC();
            case 1:
                return m22111aT();
            default:
                return super.mo14a(gr);
        }
    }

    /* renamed from: a */
    public void mo15a(Collection collection, C0495Gr gr) {
        super.mo15a(collection, gr);
    }

    /* access modifiers changed from: protected */
    /* renamed from: aU */
    public <T extends aDJ> T mo745aU() {
        switch (bFf().mo6893i(f4531dN)) {
            case 0:
                return null;
            case 2:
                return (aDJ) bFf().mo5606d(new aCE(this, f4531dN, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f4531dN, new Object[0]));
                break;
        }
        return m22111aT();
    }

    public AsteroidLoot bWD() {
        switch (bFf().mo6893i(fxP)) {
            case 0:
                return null;
            case 2:
                return (AsteroidLoot) bFf().mo5606d(new aCE(this, fxP, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, fxP, new Object[0]));
                break;
        }
        return bWC();
    }

    /* renamed from: c */
    public C5663aRz[] mo25c() {
        return _m_fields;
    }

    /* renamed from: S */
    public void mo10S() {
        super.mo10S();
    }

    @C0064Am(aul = "5dd9565a8332af10a5f2ac216a10f28f", aum = 0)
    private AsteroidLoot bWC() {
        return (AsteroidLoot) mo745aU();
    }

    @C0064Am(aul = "ccc925cffc1b0788b982add506d07a22", aum = 0)
    /* renamed from: aT */
    private <T extends aDJ> T m22111aT() {
        T t = (AsteroidLoot) bFf().mo6865M(AsteroidLoot.class);
        t.mo20019a(this);
        return t;
    }
}
