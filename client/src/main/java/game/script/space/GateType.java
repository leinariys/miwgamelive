package game.script.space;

import game.network.message.externalizable.aCE;
import game.script.resource.Asset;
import game.script.template.BaseTaikodomContent;
import logic.baa.*;
import logic.data.mbean.C5849abd;
import logic.res.code.C5640aRc;
import logic.res.code.C5663aRz;
import logic.res.code.C6494any;
import logic.res.html.C2491fm;
import p001a.*;

import java.util.Collection;

@C0566Hp
@C5511aMd
@C6485anp
/* renamed from: a.mx */
/* compiled from: a */
public class GateType extends BaseTaikodomContent implements C1616Xf, C6251ajP {
    /* renamed from: LR */
    public static final C5663aRz f8673LR = null;
    /* renamed from: LU */
    public static final C5663aRz f8675LU = null;
    public static final int _m_fieldCount = 0;
    public static final C5663aRz[] _m_fields = null;
    public static final int _m_methodCount = 0;
    public static final C2491fm[] _m_methods = null;
    public static final C2491fm aDk = null;
    public static final C2491fm aDl = null;
    public static final C2491fm aDm = null;
    public static final C2491fm aDn = null;
    public static final C2491fm aDo = null;
    /* renamed from: dN */
    public static final C2491fm f8676dN = null;
    public static final long serialVersionUID = 0;
    /* renamed from: uU */
    public static final C5663aRz f8678uU = null;
    /* renamed from: vi */
    public static final C2491fm f8679vi = null;
    /* renamed from: vj */
    public static final C2491fm f8680vj = null;
    public static C6494any ___iScriptClass;
    @C0064Am(aul = "43ed39b9f009f17da0068f17fe210a89", aum = 0)

    /* renamed from: LQ */
    private static Asset f8672LQ;
    @C0064Am(aul = "05aa6be57989d3cc73985def10081019", aum = 2)

    /* renamed from: LS */
    private static Asset f8674LS;
    @C0064Am(aul = "82817f8954acf7674915cb47ff173dc6", aum = 1)

    /* renamed from: uT */
    private static String f8677uT;

    static {
        m35886V();
    }

    public GateType() {
        super((C5540aNg) null);
        super.mo10S();
    }

    public GateType(C5540aNg ang) {
        super(ang);
    }

    /* renamed from: V */
    static void m35886V() {
        serialVersionUID = (long) 1;
        _m_fieldCount = BaseTaikodomContent._m_fieldCount + 3;
        _m_methodCount = BaseTaikodomContent._m_methodCount + 8;
        int i = BaseTaikodomContent._m_fieldCount;
        C5663aRz[] arzArr = new C5663aRz[(i + 3)];
        C5663aRz b = C5640aRc.m17844b(GateType.class, "43ed39b9f009f17da0068f17fe210a89", i);
        f8673LR = b;
        arzArr[i] = b;
        int i2 = i + 1;
        C5663aRz b2 = C5640aRc.m17844b(GateType.class, "82817f8954acf7674915cb47ff173dc6", i2);
        f8678uU = b2;
        arzArr[i2] = b2;
        int i3 = i2 + 1;
        C5663aRz b3 = C5640aRc.m17844b(GateType.class, "05aa6be57989d3cc73985def10081019", i3);
        f8675LU = b3;
        arzArr[i3] = b3;
        int i4 = i3 + 1;
        _m_fields = arzArr;
        C1634Xv.m11725a((Object[]) BaseTaikodomContent._m_fields, (Object[]) _m_fields);
        int i5 = BaseTaikodomContent._m_methodCount;
        C2491fm[] fmVarArr = new C2491fm[(i5 + 8)];
        C2491fm a = C4105zY.m41624a(GateType.class, "7efc634a30c5336ca6eee31f3970923d", i5);
        aDk = a;
        fmVarArr[i5] = a;
        int i6 = i5 + 1;
        C2491fm a2 = C4105zY.m41624a(GateType.class, "e7a7182fa8bc2683d24ecabac78773c1", i6);
        aDl = a2;
        fmVarArr[i6] = a2;
        int i7 = i6 + 1;
        C2491fm a3 = C4105zY.m41624a(GateType.class, "a0f71bebf22322d4945b19b13073e28c", i7);
        f8679vi = a3;
        fmVarArr[i7] = a3;
        int i8 = i7 + 1;
        C2491fm a4 = C4105zY.m41624a(GateType.class, "c7cdfcba764c7ef337a5534cab9b225a", i8);
        f8680vj = a4;
        fmVarArr[i8] = a4;
        int i9 = i8 + 1;
        C2491fm a5 = C4105zY.m41624a(GateType.class, "eebfe98bb960e22fca8dcf69894a2845", i9);
        aDm = a5;
        fmVarArr[i9] = a5;
        int i10 = i9 + 1;
        C2491fm a6 = C4105zY.m41624a(GateType.class, "5a83757ae19e5dffd616e8b15417217a", i10);
        aDn = a6;
        fmVarArr[i10] = a6;
        int i11 = i10 + 1;
        C2491fm a7 = C4105zY.m41624a(GateType.class, "0dc8aec1f5f37e27f1a34a984efdb37a", i11);
        aDo = a7;
        fmVarArr[i11] = a7;
        int i12 = i11 + 1;
        C2491fm a8 = C4105zY.m41624a(GateType.class, "3a423bd99da041d8224074c2ff4dc978", i12);
        f8676dN = a8;
        fmVarArr[i12] = a8;
        int i13 = i12 + 1;
        _m_methods = fmVarArr;
        C1634Xv.m11725a((Object[]) BaseTaikodomContent._m_methods, (Object[]) _m_methods);
        ___iScriptClass = aUO.m18566a(GateType.class, C5849abd.class, _m_fields, _m_methods);
    }

    /* renamed from: H */
    private void m35880H(String str) {
        bFf().mo5608dq().mo3197f(f8678uU, str);
    }

    /* renamed from: ij */
    private String m35888ij() {
        return (String) bFf().mo5608dq().mo3214p(f8678uU);
    }

    /* renamed from: r */
    private void m35890r(Asset tCVar) {
        bFf().mo5608dq().mo3197f(f8673LR, tCVar);
    }

    /* renamed from: rh */
    private Asset m35891rh() {
        return (Asset) bFf().mo5608dq().mo3214p(f8673LR);
    }

    /* renamed from: ri */
    private Asset m35892ri() {
        return (Asset) bFf().mo5608dq().mo3214p(f8675LU);
    }

    /* renamed from: s */
    private void m35893s(Asset tCVar) {
        bFf().mo5608dq().mo3197f(f8675LU, tCVar);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Render Asset")
    /* renamed from: I */
    public void mo20627I(Asset tCVar) {
        switch (bFf().mo6893i(aDl)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, aDl, new Object[]{tCVar}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, aDl, new Object[]{tCVar}));
                break;
        }
        m35879H(tCVar);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Interact Asset")
    /* renamed from: K */
    public void mo20628K(Asset tCVar) {
        switch (bFf().mo6893i(aDn)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, aDn, new Object[]{tCVar}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, aDn, new Object[]{tCVar}));
                break;
        }
        m35881J(tCVar);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Surface Material")
    /* renamed from: N */
    public void mo20629N(String str) {
        switch (bFf().mo6893i(f8680vj)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f8680vj, new Object[]{str}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f8680vj, new Object[]{str}));
                break;
        }
        m35882M(str);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Render Asset")
    /* renamed from: Nu */
    public Asset mo3521Nu() {
        switch (bFf().mo6893i(aDk)) {
            case 0:
                return null;
            case 2:
                return (Asset) bFf().mo5606d(new aCE(this, aDk, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, aDk, new Object[0]));
                break;
        }
        return m35883Nt();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Interact Asset")
    /* renamed from: Nw */
    public Asset mo20630Nw() {
        switch (bFf().mo6893i(aDm)) {
            case 0:
                return null;
            case 2:
                return (Asset) bFf().mo5606d(new aCE(this, aDm, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, aDm, new Object[0]));
                break;
        }
        return m35884Nv();
    }

    /* renamed from: Ny */
    public Gate mo20631Ny() {
        switch (bFf().mo6893i(aDo)) {
            case 0:
                return null;
            case 2:
                return (Gate) bFf().mo5606d(new aCE(this, aDo, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, aDo, new Object[0]));
                break;
        }
        return m35885Nx();
    }

    /* renamed from: T */
    public C6494any mo11T() {
        return ___iScriptClass;
    }

    /* renamed from: U */
    public C2491fm[] mo12U() {
        return _m_methods;
    }

    /* renamed from: W */
    public Object mo13W() {
        return new C5849abd(this);
    }

    /* renamed from: a */
    public Object mo14a(C0495Gr gr) {
        Object[] args = gr.getArgs();
        switch (gr.mo2417hq() - BaseTaikodomContent._m_methodCount) {
            case 0:
                return m35883Nt();
            case 1:
                m35879H((Asset) args[0]);
                return null;
            case 2:
                return m35889it();
            case 3:
                m35882M((String) args[0]);
                return null;
            case 4:
                return m35884Nv();
            case 5:
                m35881J((Asset) args[0]);
                return null;
            case 6:
                return m35885Nx();
            case 7:
                return m35887aT();
            default:
                return super.mo14a(gr);
        }
    }

    /* renamed from: a */
    public void mo15a(Collection collection, C0495Gr gr) {
        super.mo15a(collection, gr);
    }

    /* access modifiers changed from: protected */
    /* renamed from: aU */
    public <T extends aDJ> T mo745aU() {
        switch (bFf().mo6893i(f8676dN)) {
            case 0:
                return null;
            case 2:
                return (aDJ) bFf().mo5606d(new aCE(this, f8676dN, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f8676dN, new Object[0]));
                break;
        }
        return m35887aT();
    }

    /* renamed from: c */
    public C5663aRz[] mo25c() {
        return _m_fields;
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Surface Material")
    /* renamed from: iu */
    public String mo20632iu() {
        switch (bFf().mo6893i(f8679vi)) {
            case 0:
                return null;
            case 2:
                return (String) bFf().mo5606d(new aCE(this, f8679vi, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f8679vi, new Object[0]));
                break;
        }
        return m35889it();
    }

    /* renamed from: S */
    public void mo10S() {
        super.mo10S();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Render Asset")
    @C0064Am(aul = "7efc634a30c5336ca6eee31f3970923d", aum = 0)
    /* renamed from: Nt */
    private Asset m35883Nt() {
        return m35891rh();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Render Asset")
    @C0064Am(aul = "e7a7182fa8bc2683d24ecabac78773c1", aum = 0)
    /* renamed from: H */
    private void m35879H(Asset tCVar) {
        m35890r(tCVar);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Surface Material")
    @C0064Am(aul = "a0f71bebf22322d4945b19b13073e28c", aum = 0)
    /* renamed from: it */
    private String m35889it() {
        return m35888ij();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Surface Material")
    @C0064Am(aul = "c7cdfcba764c7ef337a5534cab9b225a", aum = 0)
    /* renamed from: M */
    private void m35882M(String str) {
        m35880H(str);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Interact Asset")
    @C0064Am(aul = "eebfe98bb960e22fca8dcf69894a2845", aum = 0)
    /* renamed from: Nv */
    private Asset m35884Nv() {
        return m35892ri();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Interact Asset")
    @C0064Am(aul = "5a83757ae19e5dffd616e8b15417217a", aum = 0)
    /* renamed from: J */
    private void m35881J(Asset tCVar) {
        m35893s(tCVar);
    }

    @C0064Am(aul = "0dc8aec1f5f37e27f1a34a984efdb37a", aum = 0)
    /* renamed from: Nx */
    private Gate m35885Nx() {
        return (Gate) mo745aU();
    }

    @C0064Am(aul = "3a423bd99da041d8224074c2ff4dc978", aum = 0)
    /* renamed from: aT */
    private <T extends aDJ> T m35887aT() {
        T t = (Gate) bFf().mo6865M(Gate.class);
        t.mo18374a(this);
        return t;
    }
}
