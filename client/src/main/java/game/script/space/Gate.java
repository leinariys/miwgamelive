package game.script.space;

import com.hoplon.geometry.Vec3f;
import game.network.message.externalizable.aCE;
import game.network.message.serializable.C3438ra;
import game.script.Actor;
import game.script.Character;
import game.script.citizenship.inprovements.NodeAccessImprovementType;
import game.script.faction.Faction;
import game.script.resource.Asset;
import game.script.simulation.Space;
import game.script.template.BaseTaikodomContent;
import logic.aaa.C2235dB;
import logic.baa.*;
import logic.bbb.C2820ka;
import logic.bbb.C4029yK;
import logic.data.mbean.C6999aym;
import logic.res.LoaderTrail;
import logic.res.code.C5640aRc;
import logic.res.code.C5663aRz;
import logic.res.code.C6494any;
import logic.res.html.C2491fm;
import org.mozilla1.javascript.ScriptRuntime;
import p001a.*;
import taikodom.geom.Orientation;
import taikodom.infra.script.I18NString;

import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.UUID;

@C5829abJ("1.11.1")
@C6485anp
@C2712iu(mo19785Bs = {C6251ajP.class}, mo19786Bt = BaseTaikodomContent.class)
@C5511aMd
/* renamed from: a.fF */
/* compiled from: a */
public class Gate extends Actor implements C0286Dh, C0468GU, C1616Xf {

    /* renamed from: Do */
    public static final C2491fm f7191Do = null;
    /* renamed from: LR */
    public static final C5663aRz f7194LR = null;
    /* renamed from: LU */
    public static final C5663aRz f7196LU = null;
    /* renamed from: LX */
    public static final C5663aRz f7198LX = null;
    /* renamed from: LZ */
    public static final C5663aRz f7200LZ = null;
    /* renamed from: Lm */
    public static final C2491fm f7201Lm = null;
    /* renamed from: MA */
    public static final C2491fm f7202MA = null;
    /* renamed from: MB */
    public static final C2491fm f7203MB = null;
    /* renamed from: MC */
    public static final C2491fm f7204MC = null;
    /* renamed from: MD */
    public static final C2491fm f7205MD = null;
    /* renamed from: ME */
    public static final C2491fm f7206ME = null;
    /* renamed from: MF */
    public static final C2491fm f7207MF = null;
    /* renamed from: MG */
    public static final C2491fm f7208MG = null;
    /* renamed from: MH */
    public static final C2491fm f7209MH = null;
    /* renamed from: MI */
    public static final C2491fm f7210MI = null;
    /* renamed from: MJ */
    public static final C2491fm f7211MJ = null;
    /* renamed from: MK */
    public static final C2491fm f7212MK = null;
    /* renamed from: ML */
    public static final C2491fm f7213ML = null;
    /* renamed from: MM */
    public static final C2491fm f7214MM = null;
    /* renamed from: MN */
    public static final C2491fm f7215MN = null;
    /* renamed from: MO */
    public static final C2491fm f7216MO = null;
    /* renamed from: MP */
    public static final C2491fm f7217MP = null;
    /* renamed from: Mb */
    public static final C5663aRz f7219Mb = null;
    /* renamed from: Md */
    public static final C5663aRz f7221Md = null;
    /* renamed from: Mf */
    public static final C5663aRz f7223Mf = null;
    /* renamed from: Mh */
    public static final C5663aRz f7225Mh = null;
    /* renamed from: Mj */
    public static final C5663aRz f7227Mj = null;
    /* renamed from: Ml */
    public static final C5663aRz f7229Ml = null;
    /* renamed from: Mn */
    public static final C5663aRz f7231Mn = null;
    /* renamed from: Mo */
    public static final C2491fm f7232Mo = null;
    /* renamed from: Mp */
    public static final C2491fm f7233Mp = null;
    /* renamed from: Mq */
    public static final C2491fm f7234Mq = null;
    /* renamed from: Mr */
    public static final C2491fm f7235Mr = null;
    /* renamed from: Ms */
    public static final C2491fm f7236Ms = null;
    /* renamed from: Mt */
    public static final C2491fm f7237Mt = null;
    /* renamed from: Mu */
    public static final C2491fm f7238Mu = null;
    /* renamed from: Mv */
    public static final C2491fm f7239Mv = null;
    /* renamed from: Mw */
    public static final C2491fm f7240Mw = null;
    /* renamed from: Mx */
    public static final C2491fm f7241Mx = null;
    /* renamed from: My */
    public static final C2491fm f7242My = null;
    /* renamed from: Mz */
    public static final C2491fm f7243Mz = null;
    public static final C2491fm _f_dispose_0020_0028_0029V = null;
    public static final C5663aRz _f_enabled = null;
    public static final C2491fm _f_getType_0020_0028_0029Ljava_002flang_002fObject_003b = null;
    public static final C2491fm _f_isEnabled_0020_0028_0029Z = null;
    public static final C2491fm _f_onResurrect_0020_0028_0029V = null;
    public static final C2491fm _f_toString_0020_0028_0029Ljava_002flang_002fString_003b = null;
    public static final int _m_fieldCount = 0;
    public static final C5663aRz[] _m_fields = null;
    public static final int _m_methodCount = 0;
    public static final C2491fm[] _m_methods = null;
    /* renamed from: bL */
    public static final C5663aRz f7245bL = null;
    /* renamed from: bM */
    public static final C5663aRz f7246bM = null;
    /* renamed from: bN */
    public static final C2491fm f7247bN = null;
    /* renamed from: bO */
    public static final C2491fm f7248bO = null;
    /* renamed from: bP */
    public static final C2491fm f7249bP = null;
    /* renamed from: bQ */
    public static final C2491fm f7250bQ = null;
    public static final long serialVersionUID = 0;
    /* renamed from: uU */
    public static final C5663aRz f7252uU = null;
    /* renamed from: uY */
    public static final C2491fm f7253uY = null;
    /* renamed from: vc */
    public static final C2491fm f7254vc = null;
    /* renamed from: ve */
    public static final C2491fm f7255ve = null;
    /* renamed from: vi */
    public static final C2491fm f7256vi = null;
    /* renamed from: vm */
    public static final C2491fm f7257vm = null;
    /* renamed from: vn */
    public static final C2491fm f7258vn = null;
    /* renamed from: vo */
    public static final C2491fm f7259vo = null;
    /* renamed from: vs */
    public static final C2491fm f7260vs = null;
    /* renamed from: LO */
    private static final float f7192LO = 500.0f;
    public static C6494any ___iScriptClass;
    @C0064Am(aul = "abfe4f08f3aa28624f5970c33c20ada1", aum = 0)

    /* renamed from: LQ */
    private static Asset f7193LQ;
    @C0064Am(aul = "e822b3abcad76ee91ae71f33c0815b12", aum = 2)

    /* renamed from: LS */
    private static Asset f7195LS;
    @C0064Am(aul = "4e08340910f06aa733c9988180e1f9a2", aum = 4)

    /* renamed from: LW */
    private static Vec3f f7197LW;
    @C0064Am(aul = "748f6806390d3351009a691ad621438a", aum = 5)

    /* renamed from: LY */
    private static Orientation f7199LY;
    @C0064Am(aul = "1c0a2ab8be539dafb757352d3c98c659", aum = 6)

    /* renamed from: Ma */
    private static Gate f7218Ma;
    @C0064Am(aul = "85a311e04ce9e68659136360140118c5", aum = 8)

    /* renamed from: Mc */
    private static boolean f7220Mc;
    @C0064Am(aul = "c6c5bb1f506656fc0d0eba27a9576569", aum = 9)

    /* renamed from: Me */
    private static C3438ra<Faction> f7222Me;
    @C0064Am(aul = "0023050a5a04d2142394df69ba1f25bf", aum = 10)

    /* renamed from: Mg */
    private static boolean f7224Mg;
    @C0064Am(aul = "e59a3225f39fe6c5d8653d8aa4b54c9e", aum = 11)

    /* renamed from: Mi */
    private static long f7226Mi;
    @C0064Am(aul = "275403758d5773cb7c8f4c0aa9908568", aum = 12)

    /* renamed from: Mk */
    private static SpaceCategory f7228Mk;
    @C0064Am(aul = "4ee5863af94f35c38e1f5088733b2fb8", aum = 13)

    /* renamed from: Mm */
    private static NodeAccessImprovementType f7230Mm;
    @C0064Am(aul = "2b8329b40b87a8e571e0b900ccee2bb0", aum = 14)

    /* renamed from: bK */
    private static UUID f7244bK;
    @C0064Am(aul = "add2e690b422885b5c3aa701b6f4236e", aum = 7)
    private static boolean enabled;
    @C0064Am(aul = "fce16e781cf744f461f512e45e553a71", aum = 3)
    private static String handle;
    @C0064Am(aul = "c89a04f8fe7860bcbc978f68a583bf07", aum = 1)

    /* renamed from: uT */
    private static String f7251uT;

    static {
        m30564V();
    }

    /* renamed from: LV */
    private transient GateCollider f7261LV;

    public Gate() {
        super((C5540aNg) null);
        super.mo10S();
    }

    public Gate(C5540aNg ang) {
        super(ang);
    }

    public Gate(GateType mxVar) {
        super((C5540aNg) null);
        super._m_script_init(mxVar);
    }

    /* renamed from: V */
    static void m30564V() {
        serialVersionUID = (long) 1;
        _m_fieldCount = Actor._m_fieldCount + 15;
        _m_methodCount = Actor._m_methodCount + 47;
        int i = Actor._m_fieldCount;
        C5663aRz[] arzArr = new C5663aRz[(i + 15)];
        C5663aRz b = C5640aRc.m17844b(Gate.class, "abfe4f08f3aa28624f5970c33c20ada1", i);
        f7194LR = b;
        arzArr[i] = b;
        int i2 = i + 1;
        C5663aRz b2 = C5640aRc.m17844b(Gate.class, "c89a04f8fe7860bcbc978f68a583bf07", i2);
        f7252uU = b2;
        arzArr[i2] = b2;
        int i3 = i2 + 1;
        C5663aRz b3 = C5640aRc.m17844b(Gate.class, "e822b3abcad76ee91ae71f33c0815b12", i3);
        f7196LU = b3;
        arzArr[i3] = b3;
        int i4 = i3 + 1;
        C5663aRz b4 = C5640aRc.m17844b(Gate.class, "fce16e781cf744f461f512e45e553a71", i4);
        f7246bM = b4;
        arzArr[i4] = b4;
        int i5 = i4 + 1;
        C5663aRz b5 = C5640aRc.m17844b(Gate.class, "4e08340910f06aa733c9988180e1f9a2", i5);
        f7198LX = b5;
        arzArr[i5] = b5;
        int i6 = i5 + 1;
        C5663aRz b6 = C5640aRc.m17844b(Gate.class, "748f6806390d3351009a691ad621438a", i6);
        f7200LZ = b6;
        arzArr[i6] = b6;
        int i7 = i6 + 1;
        C5663aRz b7 = C5640aRc.m17844b(Gate.class, "1c0a2ab8be539dafb757352d3c98c659", i7);
        f7219Mb = b7;
        arzArr[i7] = b7;
        int i8 = i7 + 1;
        C5663aRz b8 = C5640aRc.m17844b(Gate.class, "add2e690b422885b5c3aa701b6f4236e", i8);
        _f_enabled = b8;
        arzArr[i8] = b8;
        int i9 = i8 + 1;
        C5663aRz b9 = C5640aRc.m17844b(Gate.class, "85a311e04ce9e68659136360140118c5", i9);
        f7221Md = b9;
        arzArr[i9] = b9;
        int i10 = i9 + 1;
        C5663aRz b10 = C5640aRc.m17844b(Gate.class, "c6c5bb1f506656fc0d0eba27a9576569", i10);
        f7223Mf = b10;
        arzArr[i10] = b10;
        int i11 = i10 + 1;
        C5663aRz b11 = C5640aRc.m17844b(Gate.class, "0023050a5a04d2142394df69ba1f25bf", i11);
        f7225Mh = b11;
        arzArr[i11] = b11;
        int i12 = i11 + 1;
        C5663aRz b12 = C5640aRc.m17844b(Gate.class, "e59a3225f39fe6c5d8653d8aa4b54c9e", i12);
        f7227Mj = b12;
        arzArr[i12] = b12;
        int i13 = i12 + 1;
        C5663aRz b13 = C5640aRc.m17844b(Gate.class, "275403758d5773cb7c8f4c0aa9908568", i13);
        f7229Ml = b13;
        arzArr[i13] = b13;
        int i14 = i13 + 1;
        C5663aRz b14 = C5640aRc.m17844b(Gate.class, "4ee5863af94f35c38e1f5088733b2fb8", i14);
        f7231Mn = b14;
        arzArr[i14] = b14;
        int i15 = i14 + 1;
        C5663aRz b15 = C5640aRc.m17844b(Gate.class, "2b8329b40b87a8e571e0b900ccee2bb0", i15);
        f7245bL = b15;
        arzArr[i15] = b15;
        int i16 = i15 + 1;
        _m_fields = arzArr;
        C1634Xv.m11725a((Object[]) Actor._m_fields, (Object[]) _m_fields);
        int i17 = Actor._m_methodCount;
        C2491fm[] fmVarArr = new C2491fm[(i17 + 47)];
        C2491fm a = C4105zY.m41624a(Gate.class, "95021e1f61935539edef92327a1744d2", i17);
        f7247bN = a;
        fmVarArr[i17] = a;
        int i18 = i17 + 1;
        C2491fm a2 = C4105zY.m41624a(Gate.class, "718436d7543995a2492e3c35eda6cd40", i18);
        f7248bO = a2;
        fmVarArr[i18] = a2;
        int i19 = i18 + 1;
        C2491fm a3 = C4105zY.m41624a(Gate.class, "987fed7e58100080019de714e821f0f2", i19);
        f7249bP = a3;
        fmVarArr[i19] = a3;
        int i20 = i19 + 1;
        C2491fm a4 = C4105zY.m41624a(Gate.class, "6cc49b72ec953f56c6c140a756763261", i20);
        f7250bQ = a4;
        fmVarArr[i20] = a4;
        int i21 = i20 + 1;
        C2491fm a5 = C4105zY.m41624a(Gate.class, "8e92c53466682f4c0aa5886278212dc3", i21);
        f7232Mo = a5;
        fmVarArr[i21] = a5;
        int i22 = i21 + 1;
        C2491fm a6 = C4105zY.m41624a(Gate.class, "b96bcf2ae86cd9258520e40c0973d17a", i22);
        f7233Mp = a6;
        fmVarArr[i22] = a6;
        int i23 = i22 + 1;
        C2491fm a7 = C4105zY.m41624a(Gate.class, "5554b2aee88367f0baf137ea661d2f9f", i23);
        f7234Mq = a7;
        fmVarArr[i23] = a7;
        int i24 = i23 + 1;
        C2491fm a8 = C4105zY.m41624a(Gate.class, "95de259d5d10808574ae3704f5f59600", i24);
        _f_toString_0020_0028_0029Ljava_002flang_002fString_003b = a8;
        fmVarArr[i24] = a8;
        int i25 = i24 + 1;
        C2491fm a9 = C4105zY.m41624a(Gate.class, "b2852b9d48ec7fe1ef72ba3a231b8b23", i25);
        _f_dispose_0020_0028_0029V = a9;
        fmVarArr[i25] = a9;
        int i26 = i25 + 1;
        C2491fm a10 = C4105zY.m41624a(Gate.class, "95722f46e929c52deaa51add6525d75c", i26);
        f7235Mr = a10;
        fmVarArr[i26] = a10;
        int i27 = i26 + 1;
        C2491fm a11 = C4105zY.m41624a(Gate.class, "093fc2cf21a2fa3db9b6781c07279a7a", i27);
        f7236Ms = a11;
        fmVarArr[i27] = a11;
        int i28 = i27 + 1;
        C2491fm a12 = C4105zY.m41624a(Gate.class, "c9b1e8ab7304bdbe02015d22107a541e", i28);
        _f_isEnabled_0020_0028_0029Z = a12;
        fmVarArr[i28] = a12;
        int i29 = i28 + 1;
        C2491fm a13 = C4105zY.m41624a(Gate.class, "74137cc00e9243b746a3ea04bd4f6fbb", i29);
        f7237Mt = a13;
        fmVarArr[i29] = a13;
        int i30 = i29 + 1;
        C2491fm a14 = C4105zY.m41624a(Gate.class, "6d081beee1c36139c9e81933941bb95f", i30);
        f7238Mu = a14;
        fmVarArr[i30] = a14;
        int i31 = i30 + 1;
        C2491fm a15 = C4105zY.m41624a(Gate.class, "6bbdd4ca98aae723fa29d20b48b03bed", i31);
        f7239Mv = a15;
        fmVarArr[i31] = a15;
        int i32 = i31 + 1;
        C2491fm a16 = C4105zY.m41624a(Gate.class, "17d903b1bef5ca6c4053de1f30fe4a19", i32);
        f7240Mw = a16;
        fmVarArr[i32] = a16;
        int i33 = i32 + 1;
        C2491fm a17 = C4105zY.m41624a(Gate.class, "6557bc072b111d6a50f6475ebbafd2af", i33);
        f7241Mx = a17;
        fmVarArr[i33] = a17;
        int i34 = i33 + 1;
        C2491fm a18 = C4105zY.m41624a(Gate.class, "7f2f55c65bf7fd5e5b2050036dc8dd7b", i34);
        f7242My = a18;
        fmVarArr[i34] = a18;
        int i35 = i34 + 1;
        C2491fm a19 = C4105zY.m41624a(Gate.class, "47557c779eb311e47b0ae9a572e13fcb", i35);
        f7243Mz = a19;
        fmVarArr[i35] = a19;
        int i36 = i35 + 1;
        C2491fm a20 = C4105zY.m41624a(Gate.class, "ad6848d35117c81c734bcfa0bff09548", i36);
        f7202MA = a20;
        fmVarArr[i36] = a20;
        int i37 = i36 + 1;
        C2491fm a21 = C4105zY.m41624a(Gate.class, "fbce524e060d8976ba183bd24e6f702c", i37);
        f7203MB = a21;
        fmVarArr[i37] = a21;
        int i38 = i37 + 1;
        C2491fm a22 = C4105zY.m41624a(Gate.class, "fe602635d457c88f849b89efe39bc93c", i38);
        f7204MC = a22;
        fmVarArr[i38] = a22;
        int i39 = i38 + 1;
        C2491fm a23 = C4105zY.m41624a(Gate.class, "0058b3833f0e15328b53f3d7d8c91c84", i39);
        f7205MD = a23;
        fmVarArr[i39] = a23;
        int i40 = i39 + 1;
        C2491fm a24 = C4105zY.m41624a(Gate.class, "57754e0e119798c7f6d6a5c62645713c", i40);
        f7206ME = a24;
        fmVarArr[i40] = a24;
        int i41 = i40 + 1;
        C2491fm a25 = C4105zY.m41624a(Gate.class, "d6081c6a1d83e73445d1bf59c34015fc", i41);
        f7207MF = a25;
        fmVarArr[i41] = a25;
        int i42 = i41 + 1;
        C2491fm a26 = C4105zY.m41624a(Gate.class, "6f130d46c9502870e1dd010a12e62bba", i42);
        f7208MG = a26;
        fmVarArr[i42] = a26;
        int i43 = i42 + 1;
        C2491fm a27 = C4105zY.m41624a(Gate.class, "1c594ff67b7d609291311362316562fe", i43);
        f7209MH = a27;
        fmVarArr[i43] = a27;
        int i44 = i43 + 1;
        C2491fm a28 = C4105zY.m41624a(Gate.class, "2b7e98c419c3f49da2bb32768269343e", i44);
        f7210MI = a28;
        fmVarArr[i44] = a28;
        int i45 = i44 + 1;
        C2491fm a29 = C4105zY.m41624a(Gate.class, "401ca638335c32ae4016bd0a5b830edd", i45);
        f7211MJ = a29;
        fmVarArr[i45] = a29;
        int i46 = i45 + 1;
        C2491fm a30 = C4105zY.m41624a(Gate.class, "9743dc5d9e3aa9eec9b4ebd504f8c20c", i46);
        f7212MK = a30;
        fmVarArr[i46] = a30;
        int i47 = i46 + 1;
        C2491fm a31 = C4105zY.m41624a(Gate.class, "65c6d8209e1f4ef9a67379aa0c08a623", i47);
        f7254vc = a31;
        fmVarArr[i47] = a31;
        int i48 = i47 + 1;
        C2491fm a32 = C4105zY.m41624a(Gate.class, "22d2cf5e01213e69135edcd86cc002a3", i48);
        f7255ve = a32;
        fmVarArr[i48] = a32;
        int i49 = i48 + 1;
        C2491fm a33 = C4105zY.m41624a(Gate.class, "186599e4febc249dabf4e70990507142", i49);
        f7258vn = a33;
        fmVarArr[i49] = a33;
        int i50 = i49 + 1;
        C2491fm a34 = C4105zY.m41624a(Gate.class, "56f50cd4415fe55462ab7251aad1f305", i50);
        f7259vo = a34;
        fmVarArr[i50] = a34;
        int i51 = i50 + 1;
        C2491fm a35 = C4105zY.m41624a(Gate.class, "5c8381416775c4f83df62d507bb92645", i51);
        f7257vm = a35;
        fmVarArr[i51] = a35;
        int i52 = i51 + 1;
        C2491fm a36 = C4105zY.m41624a(Gate.class, "dfe8534f0ea114a6b105fe8d6f6d87bf", i52);
        _f_onResurrect_0020_0028_0029V = a36;
        fmVarArr[i52] = a36;
        int i53 = i52 + 1;
        C2491fm a37 = C4105zY.m41624a(Gate.class, "7e8cc9fda3fa0c56d4887193ebf82101", i53);
        f7256vi = a37;
        fmVarArr[i53] = a37;
        int i54 = i53 + 1;
        C2491fm a38 = C4105zY.m41624a(Gate.class, "7a9c70a39615260e7e79329cf2564c0b", i54);
        f7260vs = a38;
        fmVarArr[i54] = a38;
        int i55 = i54 + 1;
        C2491fm a39 = C4105zY.m41624a(Gate.class, "0fe422403f227cacc44d186ddd3a0a7a", i55);
        f7213ML = a39;
        fmVarArr[i55] = a39;
        int i56 = i55 + 1;
        C2491fm a40 = C4105zY.m41624a(Gate.class, "fa213a2454ce01f45b29b7d10e7ffb3b", i56);
        f7201Lm = a40;
        fmVarArr[i56] = a40;
        int i57 = i56 + 1;
        C2491fm a41 = C4105zY.m41624a(Gate.class, "655d42161231b62d9662712226866498", i57);
        f7191Do = a41;
        fmVarArr[i57] = a41;
        int i58 = i57 + 1;
        C2491fm a42 = C4105zY.m41624a(Gate.class, "502ff61512a6fbd140ca399734cc4745", i58);
        f7253uY = a42;
        fmVarArr[i58] = a42;
        int i59 = i58 + 1;
        C2491fm a43 = C4105zY.m41624a(Gate.class, "95d38126d20f90155b9df72a77ddde70", i59);
        f7214MM = a43;
        fmVarArr[i59] = a43;
        int i60 = i59 + 1;
        C2491fm a44 = C4105zY.m41624a(Gate.class, "5fb1ba01f69204bb3dc5a9d1ed36e9b6", i60);
        f7215MN = a44;
        fmVarArr[i60] = a44;
        int i61 = i60 + 1;
        C2491fm a45 = C4105zY.m41624a(Gate.class, "a2576947a6fc7a126e893c75cb759a1c", i61);
        f7216MO = a45;
        fmVarArr[i61] = a45;
        int i62 = i61 + 1;
        C2491fm a46 = C4105zY.m41624a(Gate.class, "1e113de81375975db442c6605ea71e25", i62);
        f7217MP = a46;
        fmVarArr[i62] = a46;
        int i63 = i62 + 1;
        C2491fm a47 = C4105zY.m41624a(Gate.class, "60c809966584397c56728e9438b8d148", i63);
        _f_getType_0020_0028_0029Ljava_002flang_002fObject_003b = a47;
        fmVarArr[i63] = a47;
        int i64 = i63 + 1;
        _m_methods = fmVarArr;
        C1634Xv.m11725a((Object[]) Actor._m_methods, (Object[]) _m_methods);
        ___iScriptClass = aUO.m18566a(Gate.class, C6999aym.class, _m_fields, _m_methods);
    }

    /* renamed from: C */
    private void m30556C(long j) {
        bFf().mo5608dq().mo3184b(f7227Mj, j);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Toll")
    @C0064Am(aul = "093fc2cf21a2fa3db9b6781c07279a7a", aum = 0)
    @C5566aOg
    /* renamed from: D */
    private void m30557D(long j) {
        throw new aWi(new aCE(this, f7236Ms, new Object[]{new Long(j)}));
    }

    /* renamed from: H */
    private void m30558H(String str) {
        throw new C6039afL();
    }

    /* renamed from: J */
    private void m30559J(boolean z) {
        bFf().mo5608dq().mo3153a(_f_enabled, z);
    }

    /* renamed from: K */
    private void m30560K(boolean z) {
        bFf().mo5608dq().mo3153a(f7221Md, z);
    }

    /* renamed from: L */
    private void m30561L(boolean z) {
        bFf().mo5608dq().mo3153a(f7225Mh, z);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Enabled")
    @C0064Am(aul = "74137cc00e9243b746a3ea04bd4f6fbb", aum = 0)
    @C5566aOg
    /* renamed from: M */
    private void m30562M(boolean z) {
        throw new aWi(new aCE(this, f7237Mt, new Object[]{new Boolean(z)}));
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Needs Pass")
    @C0064Am(aul = "6bbdd4ca98aae723fa29d20b48b03bed", aum = 0)
    @C5566aOg
    /* renamed from: N */
    private void m30563N(boolean z) {
        throw new aWi(new aCE(this, f7239Mv, new Object[]{new Boolean(z)}));
    }

    /* renamed from: a */
    private void m30566a(SpaceCategory ak) {
        bFf().mo5608dq().mo3197f(f7229Ml, ak);
    }

    /* renamed from: a */
    private void m30571a(NodeAccessImprovementType oc) {
        bFf().mo5608dq().mo3197f(f7231Mn, oc);
    }

    @C0064Am(aul = "ad6848d35117c81c734bcfa0bff09548", aum = 0)
    @C5566aOg
    /* renamed from: a */
    private void m30573a(Character acx) {
        throw new aWi(new aCE(this, f7202MA, new Object[]{acx}));
    }

    /* renamed from: a */
    private void m30574a(Gate fFVar) {
        bFf().mo5608dq().mo3197f(f7219Mb, fFVar);
    }

    /* renamed from: a */
    private void m30575a(String str) {
        bFf().mo5608dq().mo3197f(f7246bM, str);
    }

    /* renamed from: a */
    private void m30576a(UUID uuid) {
        bFf().mo5608dq().mo3197f(f7245bL, uuid);
    }

    /* renamed from: a */
    private void m30577a(Orientation orientation) {
        bFf().mo5608dq().mo3197f(f7200LZ, orientation);
    }

    /* renamed from: an */
    private UUID m30579an() {
        return (UUID) bFf().mo5608dq().mo3214p(f7245bL);
    }

    /* renamed from: ao */
    private String m30580ao() {
        return (String) bFf().mo5608dq().mo3214p(f7246bM);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Space Category")
    @C0064Am(aul = "1e113de81375975db442c6605ea71e25", aum = 0)
    @C5566aOg
    /* renamed from: b */
    private void m30584b(SpaceCategory ak) {
        throw new aWi(new aCE(this, f7217MP, new Object[]{ak}));
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Node Access Improvement")
    @C0064Am(aul = "401ca638335c32ae4016bd0a5b830edd", aum = 0)
    @C5566aOg
    /* renamed from: b */
    private void m30585b(NodeAccessImprovementType oc) {
        throw new aWi(new aCE(this, f7211MJ, new Object[]{oc}));
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Destiny")
    @C0064Am(aul = "fe602635d457c88f849b89efe39bc93c", aum = 0)
    @C5566aOg
    /* renamed from: b */
    private void m30586b(Gate fFVar) {
        throw new aWi(new aCE(this, f7204MC, new Object[]{fFVar}));
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Spawn Orientation")
    @C0064Am(aul = "57754e0e119798c7f6d6a5c62645713c", aum = 0)
    @C5566aOg
    /* renamed from: b */
    private void m30590b(Orientation orientation) {
        throw new aWi(new aCE(this, f7206ME, new Object[]{orientation}));
    }

    @C4034yP
    @C5566aOg
    @C0064Am(aul = "1c594ff67b7d609291311362316562fe", aum = 0)
    @C2499fr(mo18857qh = 2)
    /* renamed from: c */
    private C0286Dh.C0287a m30591c(Character acx) {
        throw new aWi(new aCE(this, f7209MH, new Object[]{acx}));
    }

    /* renamed from: c */
    private void m30595c(UUID uuid) {
        switch (bFf().mo6893i(f7248bO)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f7248bO, new Object[]{uuid}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f7248bO, new Object[]{uuid}));
                break;
        }
        m30589b(uuid);
    }

    @C0064Am(aul = "9743dc5d9e3aa9eec9b4ebd504f8c20c", aum = 0)
    @C5566aOg
    /* renamed from: e */
    private C0286Dh.C0287a m30596e(Character acx) {
        throw new aWi(new aCE(this, f7212MK, new Object[]{acx}));
    }

    @C5566aOg
    /* renamed from: f */
    private C0286Dh.C0287a m30597f(Character acx) {
        switch (bFf().mo6893i(f7212MK)) {
            case 0:
                return null;
            case 2:
                return (C0286Dh.C0287a) bFf().mo5606d(new aCE(this, f7212MK, new Object[]{acx}));
            case 3:
                bFf().mo5606d(new aCE(this, f7212MK, new Object[]{acx}));
                break;
        }
        return m30596e(acx);
    }

    /* renamed from: i */
    private void m30599i(Vec3f vec3f) {
        bFf().mo5608dq().mo3197f(f7198LX, vec3f);
    }

    /* renamed from: ij */
    private String m30600ij() {
        return ((GateType) getType()).mo20632iu();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Spawn Relative Position")
    @C0064Am(aul = "6f130d46c9502870e1dd010a12e62bba", aum = 0)
    @C5566aOg
    /* renamed from: j */
    private void m30605j(Vec3f vec3f) {
        throw new aWi(new aCE(this, f7208MG, new Object[]{vec3f}));
    }

    /* renamed from: q */
    private void m30606q(C3438ra raVar) {
        bFf().mo5608dq().mo3197f(f7223Mf, raVar);
    }

    @C0064Am(aul = "fa213a2454ce01f45b29b7d10e7ffb3b", aum = 0)
    @C5566aOg
    @C2499fr
    /* renamed from: qU */
    private void m30607qU() {
        throw new aWi(new aCE(this, f7201Lm, new Object[0]));
    }

    @C0064Am(aul = "60c809966584397c56728e9438b8d148", aum = 0)
    /* renamed from: qW */
    private Object m30608qW() {
        return mo18392ru();
    }

    /* renamed from: r */
    private void m30609r(Asset tCVar) {
        throw new C6039afL();
    }

    /* renamed from: rD */
    private void m30612rD() {
        switch (bFf().mo6893i(f7243Mz)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f7243Mz, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f7243Mz, new Object[0]));
                break;
        }
        m30611rC();
    }

    /* renamed from: rN */
    private C4029yK m30618rN() {
        switch (bFf().mo6893i(f7213ML)) {
            case 0:
                return null;
            case 2:
                return (C4029yK) bFf().mo5606d(new aCE(this, f7213ML, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f7213ML, new Object[0]));
                break;
        }
        return m30617rM();
    }

    /* renamed from: rh */
    private Asset m30621rh() {
        return ((GateType) getType()).mo3521Nu();
    }

    /* renamed from: ri */
    private Asset m30622ri() {
        return ((GateType) getType()).mo20630Nw();
    }

    /* renamed from: rj */
    private Vec3f m30623rj() {
        return (Vec3f) bFf().mo5608dq().mo3214p(f7198LX);
    }

    /* renamed from: rk */
    private Orientation m30624rk() {
        return (Orientation) bFf().mo5608dq().mo3214p(f7200LZ);
    }

    /* renamed from: rl */
    private Gate m30625rl() {
        return (Gate) bFf().mo5608dq().mo3214p(f7219Mb);
    }

    /* renamed from: rm */
    private boolean m30626rm() {
        return bFf().mo5608dq().mo3201h(_f_enabled);
    }

    /* renamed from: rn */
    private boolean m30627rn() {
        return bFf().mo5608dq().mo3201h(f7221Md);
    }

    /* renamed from: ro */
    private C3438ra m30628ro() {
        return (C3438ra) bFf().mo5608dq().mo3214p(f7223Mf);
    }

    /* renamed from: rp */
    private boolean m30629rp() {
        return bFf().mo5608dq().mo3201h(f7225Mh);
    }

    /* renamed from: rq */
    private long m30630rq() {
        return bFf().mo5608dq().mo3213o(f7227Mj);
    }

    /* renamed from: rr */
    private SpaceCategory m30631rr() {
        return (SpaceCategory) bFf().mo5608dq().mo3214p(f7229Ml);
    }

    /* renamed from: rs */
    private NodeAccessImprovementType m30632rs() {
        return (NodeAccessImprovementType) bFf().mo5608dq().mo3214p(f7231Mn);
    }

    /* renamed from: s */
    private void m30637s(Asset tCVar) {
        throw new C6039afL();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Toll")
    @C5566aOg
    /* renamed from: E */
    public void mo18372E(long j) {
        switch (bFf().mo6893i(f7236Ms)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f7236Ms, new Object[]{new Long(j)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f7236Ms, new Object[]{new Long(j)}));
                break;
        }
        m30557D(j);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Needs Pass")
    @C5566aOg
    /* renamed from: O */
    public void mo18373O(boolean z) {
        switch (bFf().mo6893i(f7239Mv)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f7239Mv, new Object[]{new Boolean(z)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f7239Mv, new Object[]{new Boolean(z)}));
                break;
        }
        m30563N(z);
    }

    /* renamed from: T */
    public C6494any mo11T() {
        return ___iScriptClass;
    }

    /* renamed from: U */
    public C2491fm[] mo12U() {
        return _m_methods;
    }

    /* renamed from: W */
    public Object mo13W() {
        return new C6999aym(this);
    }

    /* renamed from: Y */
    public boolean mo599Y(float f) {
        switch (bFf().mo6893i(f7214MM)) {
            case 0:
                return false;
            case 2:
                return ((Boolean) bFf().mo5606d(new aCE(this, f7214MM, new Object[]{new Float(f)}))).booleanValue();
            case 3:
                bFf().mo5606d(new aCE(this, f7214MM, new Object[]{new Float(f)}));
                break;
        }
        return m30565X(f);
    }

    /* renamed from: a */
    public Object mo14a(C0495Gr gr) {
        Object[] args = gr.getArgs();
        switch (gr.mo2417hq() - Actor._m_methodCount) {
            case 0:
                return m30581ap();
            case 1:
                m30589b((UUID) args[0]);
                return null;
            case 2:
                return m30582ar();
            case 3:
                m30588b((String) args[0]);
                return null;
            case 4:
                return m30633rt();
            case 5:
                m30587b((GateType) args[0]);
                return null;
            case 6:
                m30570a((StellarSystem) args[0]);
                return null;
            case 7:
                return m30583au();
            case 8:
                m30598fg();
                return null;
            case 9:
                return new Long(m30634rv());
            case 10:
                m30557D(((Long) args[0]).longValue());
                return null;
            case 11:
                return new Boolean(m30635rx());
            case 12:
                m30562M(((Boolean) args[0]).booleanValue());
                return null;
            case 13:
                return new Boolean(m30636ry());
            case 14:
                m30563N(((Boolean) args[0]).booleanValue());
                return null;
            case 15:
                m30572a((Faction) args[0]);
                return null;
            case 16:
                m30594c((Faction) args[0]);
                return null;
            case 17:
                return m30610rA();
            case 18:
                m30611rC();
                return null;
            case 19:
                m30573a((Character) args[0]);
                return null;
            case 20:
                return m30613rE();
            case 21:
                m30586b((Gate) args[0]);
                return null;
            case 22:
                return m30614rG();
            case 23:
                m30590b((Orientation) args[0]);
                return null;
            case 24:
                return m30615rI();
            case 25:
                m30605j((Vec3f) args[0]);
                return null;
            case 26:
                return m30591c((Character) args[0]);
            case 27:
                return m30616rK();
            case 28:
                m30585b((NodeAccessImprovementType) args[0]);
                return null;
            case 29:
                return m30596e((Character) args[0]);
            case 30:
                return m30601io();
            case 31:
                return m30602iq();
            case 32:
                m30593c((Actor) args[0], (C5260aCm) args[1], (Vec3f) args[2], (Vec3f) args[3]);
                return null;
            case 33:
                return m30604iz();
            case 34:
                m30568a((Actor) args[0], (C5260aCm) args[1], (Vec3f) args[2], (Vec3f) args[3]);
                return null;
            case 35:
                m30578aG();
                return null;
            case 36:
                return m30603it();
            case 37:
                m30567a((Actor.C0200h) args[0]);
                return null;
            case 38:
                return m30617rM();
            case 39:
                m30607qU();
                return null;
            case 40:
                m30569a((C0665JT) args[0]);
                return null;
            case 41:
                return m30592c((Space) args[0], ((Long) args[1]).longValue());
            case 42:
                return new Boolean(m30565X(((Float) args[0]).floatValue()));
            case 43:
                return m30619rO();
            case 44:
                return m30620rQ();
            case 45:
                m30584b((SpaceCategory) args[0]);
                return null;
            case 46:
                return m30608qW();
            default:
                return super.mo14a(gr);
        }
    }

    /* renamed from: a */
    public void mo15a(Collection collection, C0495Gr gr) {
        super.mo15a(collection, gr);
    }

    /* renamed from: aH */
    public void mo70aH() {
        switch (bFf().mo6893i(_f_onResurrect_0020_0028_0029V)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, _f_onResurrect_0020_0028_0029V, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, _f_onResurrect_0020_0028_0029V, new Object[0]));
                break;
        }
        m30578aG();
    }

    /* renamed from: aq */
    public UUID mo18aq() {
        switch (bFf().mo6893i(f7247bN)) {
            case 0:
                return null;
            case 2:
                return (UUID) bFf().mo5606d(new aCE(this, f7247bN, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f7247bN, new Object[0]));
                break;
        }
        return m30581ap();
    }

    /* renamed from: b */
    public void mo620b(Actor.C0200h hVar) {
        switch (bFf().mo6893i(f7260vs)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f7260vs, new Object[]{hVar}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f7260vs, new Object[]{hVar}));
                break;
        }
        m30567a(hVar);
    }

    /* renamed from: b */
    public void mo621b(Actor cr, C5260aCm acm, Vec3f vec3f, Vec3f vec3f2) {
        switch (bFf().mo6893i(f7257vm)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f7257vm, new Object[]{cr, acm, vec3f, vec3f2}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f7257vm, new Object[]{cr, acm, vec3f, vec3f2}));
                break;
        }
        m30568a(cr, acm, vec3f, vec3f2);
    }

    /* renamed from: b */
    public void mo24b(C0665JT jt) {
        switch (bFf().mo6893i(f7191Do)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f7191Do, new Object[]{jt}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f7191Do, new Object[]{jt}));
                break;
        }
        m30569a(jt);
    }

    /* renamed from: b */
    public void mo993b(StellarSystem jj) {
        switch (bFf().mo6893i(f7234Mq)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f7234Mq, new Object[]{jj}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f7234Mq, new Object[]{jj}));
                break;
        }
        m30570a(jj);
    }

    @C3248pc(aYR = C3248pc.C3250b.ADDER, displayName = "Banned Factions")
    /* renamed from: b */
    public void mo18375b(Faction xm) {
        switch (bFf().mo6893i(f7240Mw)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f7240Mw, new Object[]{xm}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f7240Mw, new Object[]{xm}));
                break;
        }
        m30572a(xm);
    }

    @C5566aOg
    /* renamed from: b */
    public void mo18376b(Character acx) {
        switch (bFf().mo6893i(f7202MA)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f7202MA, new Object[]{acx}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f7202MA, new Object[]{acx}));
                break;
        }
        m30573a(acx);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Space Category")
    @C5566aOg
    /* renamed from: c */
    public void mo18377c(SpaceCategory ak) {
        switch (bFf().mo6893i(f7217MP)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f7217MP, new Object[]{ak}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f7217MP, new Object[]{ak}));
                break;
        }
        m30584b(ak);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Node Access Improvement")
    @C5566aOg
    /* renamed from: c */
    public void mo18378c(NodeAccessImprovementType oc) {
        switch (bFf().mo6893i(f7211MJ)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f7211MJ, new Object[]{oc}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f7211MJ, new Object[]{oc}));
                break;
        }
        m30585b(oc);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Destiny")
    @C5566aOg
    /* renamed from: c */
    public void mo18379c(Gate fFVar) {
        switch (bFf().mo6893i(f7204MC)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f7204MC, new Object[]{fFVar}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f7204MC, new Object[]{fFVar}));
                break;
        }
        m30586b(fFVar);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Type")
    /* renamed from: c */
    public void mo18380c(GateType mxVar) {
        switch (bFf().mo6893i(f7233Mp)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f7233Mp, new Object[]{mxVar}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f7233Mp, new Object[]{mxVar}));
                break;
        }
        m30587b(mxVar);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Spawn Orientation")
    @C5566aOg
    /* renamed from: c */
    public void mo18381c(Orientation orientation) {
        switch (bFf().mo6893i(f7206ME)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f7206ME, new Object[]{orientation}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f7206ME, new Object[]{orientation}));
                break;
        }
        m30590b(orientation);
    }

    /* renamed from: c */
    public C5663aRz[] mo25c() {
        return _m_fields;
    }

    @C4034yP
    @C5566aOg
    @C2499fr(mo18857qh = 2)
    /* renamed from: d */
    public C0286Dh.C0287a mo634d(Character acx) {
        switch (bFf().mo6893i(f7209MH)) {
            case 0:
                return null;
            case 2:
                return (C0286Dh.C0287a) bFf().mo5606d(new aCE(this, f7209MH, new Object[]{acx}));
            case 3:
                bFf().mo5606d(new aCE(this, f7209MH, new Object[]{acx}));
                break;
        }
        return m30591c(acx);
    }

    /* access modifiers changed from: protected */
    @C2198cg
    @C1253SX
    /* renamed from: d */
    public C0520HN mo635d(Space ea, long j) {
        switch (bFf().mo6893i(f7253uY)) {
            case 0:
                return null;
            case 2:
                return (C0520HN) bFf().mo5606d(new aCE(this, f7253uY, new Object[]{ea, new Long(j)}));
            case 3:
                bFf().mo5606d(new aCE(this, f7253uY, new Object[]{ea, new Long(j)}));
                break;
        }
        return m30592c(ea, j);
    }

    /* renamed from: d */
    public void mo636d(Actor cr, C5260aCm acm, Vec3f vec3f, Vec3f vec3f2) {
        switch (bFf().mo6893i(f7258vn)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f7258vn, new Object[]{cr, acm, vec3f, vec3f2}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f7258vn, new Object[]{cr, acm, vec3f, vec3f2}));
                break;
        }
        m30593c(cr, acm, vec3f, vec3f2);
    }

    @C3248pc(aYR = C3248pc.C3250b.REMOVER, displayName = "Banned Factions")
    /* renamed from: d */
    public void mo18382d(Faction xm) {
        switch (bFf().mo6893i(f7241Mx)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f7241Mx, new Object[]{xm}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f7241Mx, new Object[]{xm}));
                break;
        }
        m30594c(xm);
    }

    public void dispose() {
        switch (bFf().mo6893i(_f_dispose_0020_0028_0029V)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, _f_dispose_0020_0028_0029V, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, _f_dispose_0020_0028_0029V, new Object[0]));
                break;
        }
        m30598fg();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Handle")
    public String getHandle() {
        switch (bFf().mo6893i(f7249bP)) {
            case 0:
                return null;
            case 2:
                return (String) bFf().mo5606d(new aCE(this, f7249bP, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f7249bP, new Object[0]));
                break;
        }
        return m30582ar();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Handle")
    public void setHandle(String str) {
        switch (bFf().mo6893i(f7250bQ)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f7250bQ, new Object[]{str}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f7250bQ, new Object[]{str}));
                break;
        }
        m30588b(str);
    }

    public /* bridge */ /* synthetic */ Object getType() {
        switch (bFf().mo6893i(_f_getType_0020_0028_0029Ljava_002flang_002fObject_003b)) {
            case 0:
                return null;
            case 2:
                return bFf().mo5606d(new aCE(this, _f_getType_0020_0028_0029Ljava_002flang_002fObject_003b, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, _f_getType_0020_0028_0029Ljava_002flang_002fObject_003b, new Object[0]));
                break;
        }
        return m30608qW();
    }

    /* renamed from: iA */
    public String mo647iA() {
        switch (bFf().mo6893i(f7259vo)) {
            case 0:
                return null;
            case 2:
                return (String) bFf().mo5606d(new aCE(this, f7259vo, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f7259vo, new Object[0]));
                break;
        }
        return m30604iz();
    }

    /* renamed from: ip */
    public String mo648ip() {
        switch (bFf().mo6893i(f7254vc)) {
            case 0:
                return null;
            case 2:
                return (String) bFf().mo5606d(new aCE(this, f7254vc, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f7254vc, new Object[0]));
                break;
        }
        return m30601io();
    }

    /* renamed from: ir */
    public String mo649ir() {
        switch (bFf().mo6893i(f7255ve)) {
            case 0:
                return null;
            case 2:
                return (String) bFf().mo5606d(new aCE(this, f7255ve, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f7255ve, new Object[0]));
                break;
        }
        return m30602iq();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Enabled")
    public boolean isEnabled() {
        switch (bFf().mo6893i(_f_isEnabled_0020_0028_0029Z)) {
            case 0:
                return false;
            case 2:
                return ((Boolean) bFf().mo5606d(new aCE(this, _f_isEnabled_0020_0028_0029Z, new Object[0]))).booleanValue();
            case 3:
                bFf().mo5606d(new aCE(this, _f_isEnabled_0020_0028_0029Z, new Object[0]));
                break;
        }
        return m30635rx();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Enabled")
    @C5566aOg
    public void setEnabled(boolean z) {
        switch (bFf().mo6893i(f7237Mt)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f7237Mt, new Object[]{new Boolean(z)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f7237Mt, new Object[]{new Boolean(z)}));
                break;
        }
        m30562M(z);
    }

    /* renamed from: iu */
    public String mo651iu() {
        switch (bFf().mo6893i(f7256vi)) {
            case 0:
                return null;
            case 2:
                return (String) bFf().mo5606d(new aCE(this, f7256vi, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f7256vi, new Object[0]));
                break;
        }
        return m30603it();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Spawn Relative Position")
    @C5566aOg
    /* renamed from: k */
    public void mo18384k(Vec3f vec3f) {
        switch (bFf().mo6893i(f7208MG)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f7208MG, new Object[]{vec3f}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f7208MG, new Object[]{vec3f}));
                break;
        }
        m30605j(vec3f);
    }

    @C5566aOg
    @C2499fr
    /* renamed from: qV */
    public void mo656qV() {
        switch (bFf().mo6893i(f7201Lm)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f7201Lm, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f7201Lm, new Object[0]));
                break;
        }
        m30607qU();
    }

    @C3248pc(aYR = C3248pc.C3250b.ITERATOR, displayName = "Banned Factions")
    /* renamed from: rB */
    public List<Faction> mo18385rB() {
        switch (bFf().mo6893i(f7242My)) {
            case 0:
                return null;
            case 2:
                return (List) bFf().mo5606d(new aCE(this, f7242My, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f7242My, new Object[0]));
                break;
        }
        return m30610rA();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Destiny")
    /* renamed from: rF */
    public Gate mo18386rF() {
        switch (bFf().mo6893i(f7203MB)) {
            case 0:
                return null;
            case 2:
                return (Gate) bFf().mo5606d(new aCE(this, f7203MB, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f7203MB, new Object[0]));
                break;
        }
        return m30613rE();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Spawn Orientation")
    /* renamed from: rH */
    public Orientation mo18387rH() {
        switch (bFf().mo6893i(f7205MD)) {
            case 0:
                return null;
            case 2:
                return (Orientation) bFf().mo5606d(new aCE(this, f7205MD, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f7205MD, new Object[0]));
                break;
        }
        return m30614rG();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Spawn Relative Position")
    /* renamed from: rJ */
    public Vec3f mo18388rJ() {
        switch (bFf().mo6893i(f7207MF)) {
            case 0:
                return null;
            case 2:
                return (Vec3f) bFf().mo5606d(new aCE(this, f7207MF, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f7207MF, new Object[0]));
                break;
        }
        return m30615rI();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Node Access Improvement")
    /* renamed from: rL */
    public NodeAccessImprovementType mo18389rL() {
        switch (bFf().mo6893i(f7210MI)) {
            case 0:
                return null;
            case 2:
                return (NodeAccessImprovementType) bFf().mo5606d(new aCE(this, f7210MI, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f7210MI, new Object[0]));
                break;
        }
        return m30616rK();
    }

    /* renamed from: rP */
    public I18NString mo18390rP() {
        switch (bFf().mo6893i(f7215MN)) {
            case 0:
                return null;
            case 2:
                return (I18NString) bFf().mo5606d(new aCE(this, f7215MN, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f7215MN, new Object[0]));
                break;
        }
        return m30619rO();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Space Category")
    /* renamed from: rR */
    public SpaceCategory mo18391rR() {
        switch (bFf().mo6893i(f7216MO)) {
            case 0:
                return null;
            case 2:
                return (SpaceCategory) bFf().mo5606d(new aCE(this, f7216MO, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f7216MO, new Object[0]));
                break;
        }
        return m30620rQ();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Type")
    /* renamed from: ru */
    public GateType mo18392ru() {
        switch (bFf().mo6893i(f7232Mo)) {
            case 0:
                return null;
            case 2:
                return (GateType) bFf().mo5606d(new aCE(this, f7232Mo, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f7232Mo, new Object[0]));
                break;
        }
        return m30633rt();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Toll")
    /* renamed from: rw */
    public long mo18393rw() {
        switch (bFf().mo6893i(f7235Mr)) {
            case 0:
                return 0;
            case 2:
                return ((Long) bFf().mo5606d(new aCE(this, f7235Mr, new Object[0]))).longValue();
            case 3:
                bFf().mo5606d(new aCE(this, f7235Mr, new Object[0]));
                break;
        }
        return m30634rv();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Needs Pass")
    /* renamed from: rz */
    public boolean mo18394rz() {
        switch (bFf().mo6893i(f7238Mu)) {
            case 0:
                return false;
            case 2:
                return ((Boolean) bFf().mo5606d(new aCE(this, f7238Mu, new Object[0]))).booleanValue();
            case 3:
                bFf().mo5606d(new aCE(this, f7238Mu, new Object[0]));
                break;
        }
        return m30636ry();
    }

    public String toString() {
        switch (bFf().mo6893i(_f_toString_0020_0028_0029Ljava_002flang_002fString_003b)) {
            case 0:
                return null;
            case 2:
                return (String) bFf().mo5606d(new aCE(this, _f_toString_0020_0028_0029Ljava_002flang_002fString_003b, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, _f_toString_0020_0028_0029Ljava_002flang_002fString_003b, new Object[0]));
                break;
        }
        return m30583au();
    }

    @C0064Am(aul = "95021e1f61935539edef92327a1744d2", aum = 0)
    /* renamed from: ap */
    private UUID m30581ap() {
        return m30579an();
    }

    @C0064Am(aul = "718436d7543995a2492e3c35eda6cd40", aum = 0)
    /* renamed from: b */
    private void m30589b(UUID uuid) {
        m30576a(uuid);
    }

    /* access modifiers changed from: protected */
    /* renamed from: S */
    public void mo10S() {
        super.mo10S();
        m30599i(new Vec3f(0.0f, 0.0f, 0.0f));
        m30577a(new Orientation(ScriptRuntime.NaN, ScriptRuntime.NaN, ScriptRuntime.NaN));
        m30576a(UUID.randomUUID());
    }

    /* renamed from: a */
    public void mo18374a(GateType mxVar) {
        super.mo967a((C2961mJ) mxVar);
        m30599i(new Vec3f(0.0f, 0.0f, 0.0f));
        m30577a(new Orientation(ScriptRuntime.NaN, ScriptRuntime.NaN, ScriptRuntime.NaN));
        setStatic(true);
        setHandle(String.valueOf(mo18392ru().getHandle()) + "_" + cWm());
        m30576a(UUID.randomUUID());
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Handle")
    @C0064Am(aul = "987fed7e58100080019de714e821f0f2", aum = 0)
    /* renamed from: ar */
    private String m30582ar() {
        return m30580ao();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Handle")
    @C0064Am(aul = "6cc49b72ec953f56c6c140a756763261", aum = 0)
    /* renamed from: b */
    private void m30588b(String str) {
        m30575a(str);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Type")
    @C0064Am(aul = "8e92c53466682f4c0aa5886278212dc3", aum = 0)
    /* renamed from: rt */
    private GateType m30633rt() {
        return (GateType) super.getType();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Type")
    @C0064Am(aul = "b96bcf2ae86cd9258520e40c0973d17a", aum = 0)
    /* renamed from: b */
    private void m30587b(GateType mxVar) {
        if (mo18392ru() != mxVar) {
            boolean bae = bae();
            if (bae) {
                mo1099zx();
            }
            super.mo8337c(mxVar);
            if (bae) {
                cMC();
            }
        }
    }

    @C0064Am(aul = "5554b2aee88367f0baf137ea661d2f9f", aum = 0)
    /* renamed from: a */
    private void m30570a(StellarSystem jj) {
        super.mo993b(jj);
        if (m30629rp() && mo960Nc() != null) {
            mo8359ma("The later not spawned gate " + this + " is now spawned, so its collider");
        }
        m30561L(false);
    }

    @C0064Am(aul = "95de259d5d10808574ae3704f5f59600", aum = 0)
    /* renamed from: au */
    private String m30583au() {
        return "Gate: [" + getName() + "]";
    }

    @C0064Am(aul = "b2852b9d48ec7fe1ef72ba3a231b8b23", aum = 0)
    /* renamed from: fg */
    private void m30598fg() {
        super.dispose();
        if (this.f7261LV != null) {
            this.f7261LV.dispose();
        }
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Toll")
    @C0064Am(aul = "95722f46e929c52deaa51add6525d75c", aum = 0)
    /* renamed from: rv */
    private long m30634rv() {
        return m30630rq();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Enabled")
    @C0064Am(aul = "c9b1e8ab7304bdbe02015d22107a541e", aum = 0)
    /* renamed from: rx */
    private boolean m30635rx() {
        return m30626rm();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Needs Pass")
    @C0064Am(aul = "6d081beee1c36139c9e81933941bb95f", aum = 0)
    /* renamed from: ry */
    private boolean m30636ry() {
        return m30627rn();
    }

    @C3248pc(aYR = C3248pc.C3250b.ADDER, displayName = "Banned Factions")
    @C0064Am(aul = "17d903b1bef5ca6c4053de1f30fe4a19", aum = 0)
    /* renamed from: a */
    private void m30572a(Faction xm) {
        m30628ro().add(xm);
    }

    @C3248pc(aYR = C3248pc.C3250b.REMOVER, displayName = "Banned Factions")
    @C0064Am(aul = "6557bc072b111d6a50f6475ebbafd2af", aum = 0)
    /* renamed from: c */
    private void m30594c(Faction xm) {
        m30628ro().remove(xm);
    }

    @C3248pc(aYR = C3248pc.C3250b.ITERATOR, displayName = "Banned Factions")
    @C0064Am(aul = "7f2f55c65bf7fd5e5b2050036dc8dd7b", aum = 0)
    /* renamed from: rA */
    private List<Faction> m30610rA() {
        return Collections.unmodifiableList(m30628ro());
    }

    @C0064Am(aul = "47557c779eb311e47b0ae9a572e13fcb", aum = 0)
    /* renamed from: rC */
    private void m30611rC() {
        C4029yK rN = m30618rN();
        if (rN != null) {
            GateCollider mGVar = (GateCollider) bFf().mo6865M(GateCollider.class);
            mGVar.mo10S();
            this.f7261LV = mGVar;
            this.f7261LV.mo999b(rN);
            this.f7261LV.mo20458f(this);
            this.f7261LV.mo988aj(getPosition());
            this.f7261LV.mo1086l(getOrientation());
            this.f7261LV.mo993b(mo960Nc());
            if (mo960Nc() == null) {
                mo8359ma("Not spawned gate " + this + " so collider wont spawn");
            }
        }
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Destiny")
    @C0064Am(aul = "fbce524e060d8976ba183bd24e6f702c", aum = 0)
    /* renamed from: rE */
    private Gate m30613rE() {
        return m30625rl();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Spawn Orientation")
    @C0064Am(aul = "0058b3833f0e15328b53f3d7d8c91c84", aum = 0)
    /* renamed from: rG */
    private Orientation m30614rG() {
        return m30624rk();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Spawn Relative Position")
    @C0064Am(aul = "d6081c6a1d83e73445d1bf59c34015fc", aum = 0)
    /* renamed from: rI */
    private Vec3f m30615rI() {
        return m30623rj();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Node Access Improvement")
    @C0064Am(aul = "2b7e98c419c3f49da2bb32768269343e", aum = 0)
    /* renamed from: rK */
    private NodeAccessImprovementType m30616rK() {
        return m30632rs();
    }

    @C0064Am(aul = "65c6d8209e1f4ef9a67379aa0c08a623", aum = 0)
    /* renamed from: io */
    private String m30601io() {
        return m30621rh().getHandle();
    }

    @C0064Am(aul = "22d2cf5e01213e69135edcd86cc002a3", aum = 0)
    /* renamed from: iq */
    private String m30602iq() {
        return m30621rh().getFile();
    }

    @C0064Am(aul = "186599e4febc249dabf4e70990507142", aum = 0)
    /* renamed from: c */
    private void m30593c(Actor cr, C5260aCm acm, Vec3f vec3f, Vec3f vec3f2) {
    }

    @C0064Am(aul = "56f50cd4415fe55462ab7251aad1f305", aum = 0)
    /* renamed from: iz */
    private String m30604iz() {
        return ala().aIW().avo();
    }

    @C0064Am(aul = "5c8381416775c4f83df62d507bb92645", aum = 0)
    /* renamed from: a */
    private void m30568a(Actor cr, C5260aCm acm, Vec3f vec3f, Vec3f vec3f2) {
    }

    @C0064Am(aul = "dfe8534f0ea114a6b105fe8d6f6d87bf", aum = 0)
    /* renamed from: aG */
    private void m30578aG() {
        super.mo70aH();
        if (m30580ao() == null) {
            setHandle(String.valueOf(mo18392ru().getHandle()) + "_" + cWm());
        }
    }

    @C0064Am(aul = "7e8cc9fda3fa0c56d4887193ebf82101", aum = 0)
    /* renamed from: it */
    private String m30603it() {
        return m30600ij();
    }

    @C0064Am(aul = "7a9c70a39615260e7e79329cf2564c0b", aum = 0)
    /* renamed from: a */
    private void m30567a(Actor.C0200h hVar) {
        if (m30621rh() == null) {
            throw new IllegalStateException("ShapedObject of class '" + getClass().getName() + "' does not have a RenderAsset");
        }
        String file = m30621rh().getFile();
        if (!bGX() || !(ald().getLoaderTrail() instanceof LoaderTrail)) {
            hVar.mo1103c(C2606hU.m32703b(file, false));
        } else {
            ((LoaderTrail) ald().getLoaderTrail()).mo19098a(file, new C2419a(hVar), getName(), false);
        }
    }

    @C0064Am(aul = "0fe422403f227cacc44d186ddd3a0a7a", aum = 0)
    /* renamed from: rM */
    private C4029yK m30617rM() {
        if (m30622ri() == null) {
            return null;
        }
        if (this.f7261LV == null || this.f7261LV.mo958IL() == null) {
            return new C2820ka(m30622ri().getFile());
        }
        return this.f7261LV.mo958IL();
    }

    @C0064Am(aul = "655d42161231b62d9662712226866498", aum = 0)
    /* renamed from: a */
    private void m30569a(C0665JT jt) {
        super.mo24b(jt);
        if (jt.getVersion().equals("1.10.1")) {
            super.mo1066f((I18NString) jt.get("i18name"));
        } else if (jt.mo3117j(1, 10, 1)) {
            super.setName(super.getName());
        }
    }

    @C0064Am(aul = "502ff61512a6fbd140ca399734cc4745", aum = 0)
    @C2198cg
    @C1253SX
    /* renamed from: c */
    private C0520HN m30592c(Space ea, long j) {
        aBA aba = new aBA(ea, this, mo958IL());
        aba.mo2449a(ea.cKn().mo5725a(j, (C2235dB) aba, 13));
        if (bGY() && ala().aLQ().des()) {
            m30612rD();
        }
        return aba;
    }

    @C0064Am(aul = "95d38126d20f90155b9df72a77ddde70", aum = 0)
    /* renamed from: X */
    private boolean m30565X(float f) {
        return f <= PlayerController.m39266c((C0286Dh) this);
    }

    @C0064Am(aul = "5fb1ba01f69204bb3dc5a9d1ed36e9b6", aum = 0)
    /* renamed from: rO */
    private I18NString m30619rO() {
        return new I18NString(getName());
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Space Category")
    @C0064Am(aul = "a2576947a6fc7a126e893c75cb759a1c", aum = 0)
    /* renamed from: rQ */
    private SpaceCategory m30620rQ() {
        return m30631rr();
    }

    /* renamed from: a.fF$a */
    class C2419a implements C2601hQ {
        private final /* synthetic */ Actor.C0200h eyn;

        C2419a(Actor.C0200h hVar) {
            this.eyn = hVar;
        }

        /* renamed from: b */
        public void mo663b(C4029yK yKVar) {
            this.eyn.mo1103c(yKVar);
        }
    }
}
