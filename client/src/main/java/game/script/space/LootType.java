package game.script.space;

import game.network.message.externalizable.aCE;
import game.script.resource.Asset;
import game.script.template.BaseTaikodomContent;
import logic.baa.*;
import logic.data.mbean.aPX;
import logic.res.code.C5640aRc;
import logic.res.code.C5663aRz;
import logic.res.code.C6494any;
import logic.res.html.C2491fm;
import p001a.*;

import java.util.Collection;

@C0566Hp
@C5511aMd
@C6485anp
/* renamed from: a.aHC */
/* compiled from: a */
public class LootType extends BaseTaikodomContent implements C1616Xf, C6251ajP {
    /* renamed from: LR */
    public static final C5663aRz f2914LR = null;
    public static final int _m_fieldCount = 0;
    public static final C5663aRz[] _m_fields = null;
    public static final int _m_methodCount = 0;
    public static final C2491fm[] _m_methods = null;
    public static final C2491fm aDk = null;
    public static final C2491fm aDl = null;
    public static final C2491fm cXN = null;
    public static final C2491fm cXO = null;
    /* renamed from: dN */
    public static final C2491fm f2915dN = null;
    public static final C2491fm hYd = null;
    /* renamed from: kZ */
    public static final C5663aRz f2916kZ = null;
    public static final long serialVersionUID = 0;
    public static C6494any ___iScriptClass;
    @C0064Am(aul = "8e859bd97252d67222c4d4c19513c847", aum = 0)

    /* renamed from: LQ */
    private static Asset f2913LQ;
    @C0064Am(aul = "fb3f41f93b85bfc44422e650991c1e5b", aum = 1)
    private static float lifeTime;

    static {
        m15120V();
    }

    public LootType() {
        super((C5540aNg) null);
        super.mo10S();
    }

    public LootType(C5540aNg ang) {
        super(ang);
    }

    /* renamed from: V */
    static void m15120V() {
        serialVersionUID = (long) 1;
        _m_fieldCount = BaseTaikodomContent._m_fieldCount + 2;
        _m_methodCount = BaseTaikodomContent._m_methodCount + 6;
        int i = BaseTaikodomContent._m_fieldCount;
        C5663aRz[] arzArr = new C5663aRz[(i + 2)];
        C5663aRz b = C5640aRc.m17844b(LootType.class, "8e859bd97252d67222c4d4c19513c847", i);
        f2914LR = b;
        arzArr[i] = b;
        int i2 = i + 1;
        C5663aRz b2 = C5640aRc.m17844b(LootType.class, "fb3f41f93b85bfc44422e650991c1e5b", i2);
        f2916kZ = b2;
        arzArr[i2] = b2;
        int i3 = i2 + 1;
        _m_fields = arzArr;
        C1634Xv.m11725a((Object[]) BaseTaikodomContent._m_fields, (Object[]) _m_fields);
        int i4 = BaseTaikodomContent._m_methodCount;
        C2491fm[] fmVarArr = new C2491fm[(i4 + 6)];
        C2491fm a = C4105zY.m41624a(LootType.class, "430d463c7512a5c8094155968569149c", i4);
        aDk = a;
        fmVarArr[i4] = a;
        int i5 = i4 + 1;
        C2491fm a2 = C4105zY.m41624a(LootType.class, "0bf4b44bc49dedbdb37da2d2c90677e7", i5);
        aDl = a2;
        fmVarArr[i5] = a2;
        int i6 = i5 + 1;
        C2491fm a3 = C4105zY.m41624a(LootType.class, "128766753d8888aba9a4c7b9417cf9e6", i6);
        cXN = a3;
        fmVarArr[i6] = a3;
        int i7 = i6 + 1;
        C2491fm a4 = C4105zY.m41624a(LootType.class, "b5de1913f79b520198b9e214268ce448", i7);
        cXO = a4;
        fmVarArr[i7] = a4;
        int i8 = i7 + 1;
        C2491fm a5 = C4105zY.m41624a(LootType.class, "d7fad2f4a62c831a70a1da9db2adbbcb", i8);
        hYd = a5;
        fmVarArr[i8] = a5;
        int i9 = i8 + 1;
        C2491fm a6 = C4105zY.m41624a(LootType.class, "90578ea5d27847cf14caa4719277e6dd", i9);
        f2915dN = a6;
        fmVarArr[i9] = a6;
        int i10 = i9 + 1;
        _m_methods = fmVarArr;
        C1634Xv.m11725a((Object[]) BaseTaikodomContent._m_methods, (Object[]) _m_methods);
        ___iScriptClass = aUO.m18566a(LootType.class, aPX.class, _m_fields, _m_methods);
    }

    private float aQG() {
        return bFf().mo5608dq().mo3211m(f2916kZ);
    }

    /* renamed from: fm */
    private void m15122fm(float f) {
        bFf().mo5608dq().mo3150a(f2916kZ, f);
    }

    /* renamed from: r */
    private void m15124r(Asset tCVar) {
        bFf().mo5608dq().mo3197f(f2914LR, tCVar);
    }

    /* renamed from: rh */
    private Asset m15125rh() {
        return (Asset) bFf().mo5608dq().mo3214p(f2914LR);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Render Asset")
    /* renamed from: I */
    public void mo9167I(Asset tCVar) {
        switch (bFf().mo6893i(aDl)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, aDl, new Object[]{tCVar}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, aDl, new Object[]{tCVar}));
                break;
        }
        m15118H(tCVar);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Render Asset")
    /* renamed from: Nu */
    public Asset mo3521Nu() {
        switch (bFf().mo6893i(aDk)) {
            case 0:
                return null;
            case 2:
                return (Asset) bFf().mo5606d(new aCE(this, aDk, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, aDk, new Object[0]));
                break;
        }
        return m15119Nt();
    }

    /* renamed from: T */
    public C6494any mo11T() {
        return ___iScriptClass;
    }

    /* renamed from: U */
    public C2491fm[] mo12U() {
        return _m_methods;
    }

    /* renamed from: W */
    public Object mo13W() {
        return new aPX(this);
    }

    /* renamed from: a */
    public Object mo14a(C0495Gr gr) {
        Object[] args = gr.getArgs();
        switch (gr.mo2417hq() - BaseTaikodomContent._m_methodCount) {
            case 0:
                return m15119Nt();
            case 1:
                m15118H((Asset) args[0]);
                return null;
            case 2:
                return new Float(aQH());
            case 3:
                m15123fn(((Float) args[0]).floatValue());
                return null;
            case 4:
                return ddD();
            case 5:
                return m15121aT();
            default:
                return super.mo14a(gr);
        }
    }

    /* renamed from: a */
    public void mo15a(Collection collection, C0495Gr gr) {
        super.mo15a(collection, gr);
    }

    /* access modifiers changed from: protected */
    /* renamed from: aU */
    public <T extends aDJ> T mo745aU() {
        switch (bFf().mo6893i(f2915dN)) {
            case 0:
                return null;
            case 2:
                return (aDJ) bFf().mo5606d(new aCE(this, f2915dN, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f2915dN, new Object[0]));
                break;
        }
        return m15121aT();
    }

    /* renamed from: c */
    public C5663aRz[] mo25c() {
        return _m_fields;
    }

    public Loot ddE() {
        switch (bFf().mo6893i(hYd)) {
            case 0:
                return null;
            case 2:
                return (Loot) bFf().mo5606d(new aCE(this, hYd, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, hYd, new Object[0]));
                break;
        }
        return ddD();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Lifetime (s)")
    public float getLifeTime() {
        switch (bFf().mo6893i(cXN)) {
            case 0:
                return 0.0f;
            case 2:
                return ((Float) bFf().mo5606d(new aCE(this, cXN, new Object[0]))).floatValue();
            case 3:
                bFf().mo5606d(new aCE(this, cXN, new Object[0]));
                break;
        }
        return aQH();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Lifetime (s)")
    public void setLifeTime(float f) {
        switch (bFf().mo6893i(cXO)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, cXO, new Object[]{new Float(f)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, cXO, new Object[]{new Float(f)}));
                break;
        }
        m15123fn(f);
    }

    /* renamed from: S */
    public void mo10S() {
        super.mo10S();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Render Asset")
    @C0064Am(aul = "430d463c7512a5c8094155968569149c", aum = 0)
    /* renamed from: Nt */
    private Asset m15119Nt() {
        return m15125rh();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Render Asset")
    @C0064Am(aul = "0bf4b44bc49dedbdb37da2d2c90677e7", aum = 0)
    /* renamed from: H */
    private void m15118H(Asset tCVar) {
        m15124r(tCVar);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Lifetime (s)")
    @C0064Am(aul = "128766753d8888aba9a4c7b9417cf9e6", aum = 0)
    private float aQH() {
        return aQG();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Lifetime (s)")
    @C0064Am(aul = "b5de1913f79b520198b9e214268ce448", aum = 0)
    /* renamed from: fn */
    private void m15123fn(float f) {
        m15122fm(f);
    }

    @C0064Am(aul = "d7fad2f4a62c831a70a1da9db2adbbcb", aum = 0)
    private Loot ddD() {
        return (Loot) mo745aU();
    }

    @C0064Am(aul = "90578ea5d27847cf14caa4719277e6dd", aum = 0)
    /* renamed from: aT */
    private <T extends aDJ> T m15121aT() {
        T t = (Loot) bFf().mo6865M(Loot.class);
        t.mo8585h(this);
        return t;
    }
}
