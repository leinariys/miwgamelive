package game.script.space;

import com.hoplon.geometry.Vec3f;
import game.network.message.externalizable.aCE;
import game.script.Actor;
import game.script.resource.Asset;
import game.script.simulation.Space;
import game.script.template.BaseTaikodomContent;
import logic.aaa.C2235dB;
import logic.baa.*;
import logic.bbb.C4029yK;
import logic.data.mbean.C5462aKg;
import logic.res.LoaderTrail;
import logic.res.code.C5640aRc;
import logic.res.code.C5663aRz;
import logic.res.code.C6494any;
import logic.res.html.C2491fm;
import p001a.*;

import java.util.Collection;
import java.util.UUID;

@C2712iu(mo19785Bs = {C6251ajP.class}, mo19786Bt = BaseTaikodomContent.class)
@C5511aMd
@C6485anp
/* renamed from: a.WE */
/* compiled from: a */
public class Advertise extends Actor implements C0468GU, C1616Xf {
    /* renamed from: LR */
    public static final C5663aRz f1971LR = null;
    /* renamed from: Lm */
    public static final C2491fm f1972Lm = null;
    public static final C2491fm _f_getType_0020_0028_0029Ljava_002flang_002fObject_003b = null;
    public static final C2491fm _f_onResurrect_0020_0028_0029V = null;
    public static final C2491fm _f_toString_0020_0028_0029Ljava_002flang_002fString_003b = null;
    public static final int _m_fieldCount = 0;
    public static final C5663aRz[] _m_fields = null;
    public static final int _m_methodCount = 0;
    public static final C2491fm[] _m_methods = null;
    /* renamed from: bL */
    public static final C5663aRz f1974bL = null;
    /* renamed from: bM */
    public static final C5663aRz f1975bM = null;
    /* renamed from: bN */
    public static final C2491fm f1976bN = null;
    /* renamed from: bO */
    public static final C2491fm f1977bO = null;
    /* renamed from: bP */
    public static final C2491fm f1978bP = null;
    /* renamed from: bQ */
    public static final C2491fm f1979bQ = null;
    public static final C2491fm bqO = null;
    public static final C2491fm fUU = null;
    public static final long serialVersionUID = 0;
    /* renamed from: uU */
    public static final C5663aRz f1981uU = null;
    /* renamed from: uY */
    public static final C2491fm f1982uY = null;
    /* renamed from: vc */
    public static final C2491fm f1983vc = null;
    /* renamed from: ve */
    public static final C2491fm f1984ve = null;
    /* renamed from: vi */
    public static final C2491fm f1985vi = null;
    /* renamed from: vm */
    public static final C2491fm f1986vm = null;
    /* renamed from: vn */
    public static final C2491fm f1987vn = null;
    /* renamed from: vo */
    public static final C2491fm f1988vo = null;
    /* renamed from: vs */
    public static final C2491fm f1989vs = null;
    public static C6494any ___iScriptClass;
    @C0064Am(aul = "aaaa2dde525f44e37096e7fda5ca38d1", aum = 0)

    /* renamed from: LQ */
    private static Asset f1970LQ;
    @C0064Am(aul = "bda968c59da3c100d271331928a41e31", aum = 3)

    /* renamed from: bK */
    private static UUID f1973bK;
    @C0064Am(aul = "014f7e94b8363e9f4715843e94b40c27", aum = 2)
    private static String handle;
    @C0064Am(aul = "062cc085d5518d3491fd5f005120b3c5", aum = 1)

    /* renamed from: uT */
    private static String f1980uT;

    static {
        m11019V();
    }

    public Advertise() {
        super((C5540aNg) null);
        super.mo10S();
    }

    public Advertise(AdvertiseType kp) {
        super((C5540aNg) null);
        super._m_script_init(kp);
    }

    public Advertise(C5540aNg ang) {
        super(ang);
    }

    /* renamed from: V */
    static void m11019V() {
        serialVersionUID = (long) 1;
        _m_fieldCount = Actor._m_fieldCount + 4;
        _m_methodCount = Actor._m_methodCount + 18;
        int i = Actor._m_fieldCount;
        C5663aRz[] arzArr = new C5663aRz[(i + 4)];
        C5663aRz b = C5640aRc.m17844b(Advertise.class, "aaaa2dde525f44e37096e7fda5ca38d1", i);
        f1971LR = b;
        arzArr[i] = b;
        int i2 = i + 1;
        C5663aRz b2 = C5640aRc.m17844b(Advertise.class, "062cc085d5518d3491fd5f005120b3c5", i2);
        f1981uU = b2;
        arzArr[i2] = b2;
        int i3 = i2 + 1;
        C5663aRz b3 = C5640aRc.m17844b(Advertise.class, "014f7e94b8363e9f4715843e94b40c27", i3);
        f1975bM = b3;
        arzArr[i3] = b3;
        int i4 = i3 + 1;
        C5663aRz b4 = C5640aRc.m17844b(Advertise.class, "bda968c59da3c100d271331928a41e31", i4);
        f1974bL = b4;
        arzArr[i4] = b4;
        int i5 = i4 + 1;
        _m_fields = arzArr;
        C1634Xv.m11725a((Object[]) Actor._m_fields, (Object[]) _m_fields);
        int i6 = Actor._m_methodCount;
        C2491fm[] fmVarArr = new C2491fm[(i6 + 18)];
        C2491fm a = C4105zY.m41624a(Advertise.class, "61e2b50f0fb03d5ec90a236b36453df7", i6);
        f1976bN = a;
        fmVarArr[i6] = a;
        int i7 = i6 + 1;
        C2491fm a2 = C4105zY.m41624a(Advertise.class, "ae9e170b5ac8022eee801a5c1adc73dd", i7);
        f1977bO = a2;
        fmVarArr[i7] = a2;
        int i8 = i7 + 1;
        C2491fm a3 = C4105zY.m41624a(Advertise.class, "3c9955396f9a89737da6a4ee0c30feda", i8);
        bqO = a3;
        fmVarArr[i8] = a3;
        int i9 = i8 + 1;
        C2491fm a4 = C4105zY.m41624a(Advertise.class, "8ab806d0a0e3817e8826c8a0dbba8b76", i9);
        _f_toString_0020_0028_0029Ljava_002flang_002fString_003b = a4;
        fmVarArr[i9] = a4;
        int i10 = i9 + 1;
        C2491fm a5 = C4105zY.m41624a(Advertise.class, "a72755eeeaa28318b954c01cd1e8a175", i10);
        f1978bP = a5;
        fmVarArr[i10] = a5;
        int i11 = i10 + 1;
        C2491fm a6 = C4105zY.m41624a(Advertise.class, "6fc0e956bb1f51782d8b0c3a9661916f", i11);
        f1979bQ = a6;
        fmVarArr[i11] = a6;
        int i12 = i11 + 1;
        C2491fm a7 = C4105zY.m41624a(Advertise.class, "383cd3b5a30b864ecab0181447d5e70c", i12);
        fUU = a7;
        fmVarArr[i12] = a7;
        int i13 = i12 + 1;
        C2491fm a8 = C4105zY.m41624a(Advertise.class, "35b4b0b222ca235af7208bd4e7132576", i13);
        f1988vo = a8;
        fmVarArr[i13] = a8;
        int i14 = i13 + 1;
        C2491fm a9 = C4105zY.m41624a(Advertise.class, "385bfbdd4697bd04a1d37d65af89e67e", i14);
        f1987vn = a9;
        fmVarArr[i14] = a9;
        int i15 = i14 + 1;
        C2491fm a10 = C4105zY.m41624a(Advertise.class, "fc8cdd0cbe14b461fc9e063b80362d86", i15);
        f1983vc = a10;
        fmVarArr[i15] = a10;
        int i16 = i15 + 1;
        C2491fm a11 = C4105zY.m41624a(Advertise.class, "ba794c09dfb43eaa8cacdbf40f73ace0", i16);
        f1984ve = a11;
        fmVarArr[i16] = a11;
        int i17 = i16 + 1;
        C2491fm a12 = C4105zY.m41624a(Advertise.class, "97e6e947dd01a8f90e8be18a8515b9fa", i17);
        f1986vm = a12;
        fmVarArr[i17] = a12;
        int i18 = i17 + 1;
        C2491fm a13 = C4105zY.m41624a(Advertise.class, "c5b389a755629032725ed20990504da3", i18);
        _f_onResurrect_0020_0028_0029V = a13;
        fmVarArr[i18] = a13;
        int i19 = i18 + 1;
        C2491fm a14 = C4105zY.m41624a(Advertise.class, "2f547034353bae3890be2be443418c3e", i19);
        f1985vi = a14;
        fmVarArr[i19] = a14;
        int i20 = i19 + 1;
        C2491fm a15 = C4105zY.m41624a(Advertise.class, "04fb037c156853b2906e2f5ca54959b2", i20);
        f1989vs = a15;
        fmVarArr[i20] = a15;
        int i21 = i20 + 1;
        C2491fm a16 = C4105zY.m41624a(Advertise.class, "18ca953e01fecd5525642300b8e8fc47", i21);
        f1972Lm = a16;
        fmVarArr[i21] = a16;
        int i22 = i21 + 1;
        C2491fm a17 = C4105zY.m41624a(Advertise.class, "01f54d3aa9764cd19d26499644ac30b0", i22);
        f1982uY = a17;
        fmVarArr[i22] = a17;
        int i23 = i22 + 1;
        C2491fm a18 = C4105zY.m41624a(Advertise.class, "851c8f25d9ddb3e4107761f82e1f92dd", i23);
        _f_getType_0020_0028_0029Ljava_002flang_002fObject_003b = a18;
        fmVarArr[i23] = a18;
        int i24 = i23 + 1;
        _m_methods = fmVarArr;
        C1634Xv.m11725a((Object[]) Actor._m_methods, (Object[]) _m_methods);
        ___iScriptClass = aUO.m18566a(Advertise.class, C5462aKg.class, _m_fields, _m_methods);
    }

    /* renamed from: H */
    private void m11018H(String str) {
        throw new C6039afL();
    }

    /* renamed from: a */
    private void m11022a(String str) {
        bFf().mo5608dq().mo3197f(f1975bM, str);
    }

    /* renamed from: a */
    private void m11023a(UUID uuid) {
        bFf().mo5608dq().mo3197f(f1974bL, uuid);
    }

    /* renamed from: an */
    private UUID m11025an() {
        return (UUID) bFf().mo5608dq().mo3214p(f1974bL);
    }

    /* renamed from: ao */
    private String m11026ao() {
        return (String) bFf().mo5608dq().mo3214p(f1975bM);
    }

    /* renamed from: c */
    private void m11034c(UUID uuid) {
        switch (bFf().mo6893i(f1977bO)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f1977bO, new Object[]{uuid}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f1977bO, new Object[]{uuid}));
                break;
        }
        m11031b(uuid);
    }

    /* renamed from: ij */
    private String m11035ij() {
        return ((AdvertiseType) getType()).mo3523iu();
    }

    @C0064Am(aul = "18ca953e01fecd5525642300b8e8fc47", aum = 0)
    @C5566aOg
    @C2499fr
    /* renamed from: qU */
    private void m11040qU() {
        throw new aWi(new aCE(this, f1972Lm, new Object[0]));
    }

    @C0064Am(aul = "851c8f25d9ddb3e4107761f82e1f92dd", aum = 0)
    /* renamed from: qW */
    private Object m11041qW() {
        return chF();
    }

    /* renamed from: r */
    private void m11042r(Asset tCVar) {
        throw new C6039afL();
    }

    /* renamed from: rh */
    private Asset m11043rh() {
        return ((AdvertiseType) getType()).mo3521Nu();
    }

    /* renamed from: S */
    public void mo10S() {
        super.mo10S();
    }

    /* renamed from: T */
    public C6494any mo11T() {
        return ___iScriptClass;
    }

    /* renamed from: U */
    public C2491fm[] mo12U() {
        return _m_methods;
    }

    /* renamed from: W */
    public Object mo13W() {
        return new C5462aKg(this);
    }

    /* renamed from: a */
    public Object mo14a(C0495Gr gr) {
        Object[] args = gr.getArgs();
        switch (gr.mo2417hq() - Actor._m_methodCount) {
            case 0:
                return m11027ap();
            case 1:
                m11031b((UUID) args[0]);
                return null;
            case 2:
                ahs();
                return null;
            case 3:
                return m11029au();
            case 4:
                return m11028ar();
            case 5:
                m11030b((String) args[0]);
                return null;
            case 6:
                return chE();
            case 7:
                return m11039iz();
            case 8:
                m11033c((Actor) args[0], (C5260aCm) args[1], (Vec3f) args[2], (Vec3f) args[3]);
                return null;
            case 9:
                return m11036io();
            case 10:
                return m11037iq();
            case 11:
                m11021a((Actor) args[0], (C5260aCm) args[1], (Vec3f) args[2], (Vec3f) args[3]);
                return null;
            case 12:
                m11024aG();
                return null;
            case 13:
                return m11038it();
            case 14:
                m11020a((Actor.C0200h) args[0]);
                return null;
            case 15:
                m11040qU();
                return null;
            case 16:
                return m11032c((Space) args[0], ((Long) args[1]).longValue());
            case 17:
                return m11041qW();
            default:
                return super.mo14a(gr);
        }
    }

    /* renamed from: a */
    public void mo15a(Collection collection, C0495Gr gr) {
        super.mo15a(collection, gr);
    }

    /* renamed from: aH */
    public void mo70aH() {
        switch (bFf().mo6893i(_f_onResurrect_0020_0028_0029V)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, _f_onResurrect_0020_0028_0029V, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, _f_onResurrect_0020_0028_0029V, new Object[0]));
                break;
        }
        m11024aG();
    }

    /* access modifiers changed from: protected */
    public void aht() {
        switch (bFf().mo6893i(bqO)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, bqO, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, bqO, new Object[0]));
                break;
        }
        ahs();
    }

    /* renamed from: aq */
    public UUID mo18aq() {
        switch (bFf().mo6893i(f1976bN)) {
            case 0:
                return null;
            case 2:
                return (UUID) bFf().mo5606d(new aCE(this, f1976bN, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f1976bN, new Object[0]));
                break;
        }
        return m11027ap();
    }

    /* renamed from: b */
    public void mo620b(Actor.C0200h hVar) {
        switch (bFf().mo6893i(f1989vs)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f1989vs, new Object[]{hVar}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f1989vs, new Object[]{hVar}));
                break;
        }
        m11020a(hVar);
    }

    /* renamed from: b */
    public void mo621b(Actor cr, C5260aCm acm, Vec3f vec3f, Vec3f vec3f2) {
        switch (bFf().mo6893i(f1986vm)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f1986vm, new Object[]{cr, acm, vec3f, vec3f2}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f1986vm, new Object[]{cr, acm, vec3f, vec3f2}));
                break;
        }
        m11021a(cr, acm, vec3f, vec3f2);
    }

    /* renamed from: c */
    public C5663aRz[] mo25c() {
        return _m_fields;
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Type")
    public AdvertiseType chF() {
        switch (bFf().mo6893i(fUU)) {
            case 0:
                return null;
            case 2:
                return (AdvertiseType) bFf().mo5606d(new aCE(this, fUU, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, fUU, new Object[0]));
                break;
        }
        return chE();
    }

    /* access modifiers changed from: protected */
    @C2198cg
    @C1253SX
    /* renamed from: d */
    public C0520HN mo635d(Space ea, long j) {
        switch (bFf().mo6893i(f1982uY)) {
            case 0:
                return null;
            case 2:
                return (C0520HN) bFf().mo5606d(new aCE(this, f1982uY, new Object[]{ea, new Long(j)}));
            case 3:
                bFf().mo5606d(new aCE(this, f1982uY, new Object[]{ea, new Long(j)}));
                break;
        }
        return m11032c(ea, j);
    }

    /* renamed from: d */
    public void mo636d(Actor cr, C5260aCm acm, Vec3f vec3f, Vec3f vec3f2) {
        switch (bFf().mo6893i(f1987vn)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f1987vn, new Object[]{cr, acm, vec3f, vec3f2}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f1987vn, new Object[]{cr, acm, vec3f, vec3f2}));
                break;
        }
        m11033c(cr, acm, vec3f, vec3f2);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Handle")
    public String getHandle() {
        switch (bFf().mo6893i(f1978bP)) {
            case 0:
                return null;
            case 2:
                return (String) bFf().mo5606d(new aCE(this, f1978bP, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f1978bP, new Object[0]));
                break;
        }
        return m11028ar();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Handle")
    public void setHandle(String str) {
        switch (bFf().mo6893i(f1979bQ)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f1979bQ, new Object[]{str}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f1979bQ, new Object[]{str}));
                break;
        }
        m11030b(str);
    }

    public /* bridge */ /* synthetic */ Object getType() {
        switch (bFf().mo6893i(_f_getType_0020_0028_0029Ljava_002flang_002fObject_003b)) {
            case 0:
                return null;
            case 2:
                return bFf().mo5606d(new aCE(this, _f_getType_0020_0028_0029Ljava_002flang_002fObject_003b, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, _f_getType_0020_0028_0029Ljava_002flang_002fObject_003b, new Object[0]));
                break;
        }
        return m11041qW();
    }

    /* renamed from: iA */
    public String mo647iA() {
        switch (bFf().mo6893i(f1988vo)) {
            case 0:
                return null;
            case 2:
                return (String) bFf().mo5606d(new aCE(this, f1988vo, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f1988vo, new Object[0]));
                break;
        }
        return m11039iz();
    }

    /* renamed from: ip */
    public String mo648ip() {
        switch (bFf().mo6893i(f1983vc)) {
            case 0:
                return null;
            case 2:
                return (String) bFf().mo5606d(new aCE(this, f1983vc, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f1983vc, new Object[0]));
                break;
        }
        return m11036io();
    }

    /* renamed from: ir */
    public String mo649ir() {
        switch (bFf().mo6893i(f1984ve)) {
            case 0:
                return null;
            case 2:
                return (String) bFf().mo5606d(new aCE(this, f1984ve, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f1984ve, new Object[0]));
                break;
        }
        return m11037iq();
    }

    /* renamed from: iu */
    public String mo651iu() {
        switch (bFf().mo6893i(f1985vi)) {
            case 0:
                return null;
            case 2:
                return (String) bFf().mo5606d(new aCE(this, f1985vi, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f1985vi, new Object[0]));
                break;
        }
        return m11038it();
    }

    @C5566aOg
    @C2499fr
    /* renamed from: qV */
    public void mo656qV() {
        switch (bFf().mo6893i(f1972Lm)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f1972Lm, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f1972Lm, new Object[0]));
                break;
        }
        m11040qU();
    }

    public String toString() {
        switch (bFf().mo6893i(_f_toString_0020_0028_0029Ljava_002flang_002fString_003b)) {
            case 0:
                return null;
            case 2:
                return (String) bFf().mo5606d(new aCE(this, _f_toString_0020_0028_0029Ljava_002flang_002fString_003b, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, _f_toString_0020_0028_0029Ljava_002flang_002fString_003b, new Object[0]));
                break;
        }
        return m11029au();
    }

    @C0064Am(aul = "61e2b50f0fb03d5ec90a236b36453df7", aum = 0)
    /* renamed from: ap */
    private UUID m11027ap() {
        return m11025an();
    }

    @C0064Am(aul = "ae9e170b5ac8022eee801a5c1adc73dd", aum = 0)
    /* renamed from: b */
    private void m11031b(UUID uuid) {
        m11023a(uuid);
    }

    /* renamed from: g */
    public void mo6497g(AdvertiseType kp) {
        super.mo967a((C2961mJ) kp);
        setStatic(true);
        setHandle(String.valueOf(chF().getHandle()) + "_" + cWm());
        m11023a(UUID.randomUUID());
    }

    @C0064Am(aul = "3c9955396f9a89737da6a4ee0c30feda", aum = 0)
    private void ahs() {
        super.aht();
        PlayerController alb = ald().alb();
        if (alb != null) {
            alb.cNo().mo4712k(this.hks);
        }
    }

    @C0064Am(aul = "8ab806d0a0e3817e8826c8a0dbba8b76", aum = 0)
    /* renamed from: au */
    private String m11029au() {
        return "Advertise: [" + m11026ao() + "]";
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Handle")
    @C0064Am(aul = "a72755eeeaa28318b954c01cd1e8a175", aum = 0)
    /* renamed from: ar */
    private String m11028ar() {
        return m11026ao();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Handle")
    @C0064Am(aul = "6fc0e956bb1f51782d8b0c3a9661916f", aum = 0)
    /* renamed from: b */
    private void m11030b(String str) {
        m11022a(str);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Type")
    @C0064Am(aul = "383cd3b5a30b864ecab0181447d5e70c", aum = 0)
    private AdvertiseType chE() {
        return (AdvertiseType) super.getType();
    }

    @C0064Am(aul = "35b4b0b222ca235af7208bd4e7132576", aum = 0)
    /* renamed from: iz */
    private String m11039iz() {
        return null;
    }

    @C0064Am(aul = "385bfbdd4697bd04a1d37d65af89e67e", aum = 0)
    /* renamed from: c */
    private void m11033c(Actor cr, C5260aCm acm, Vec3f vec3f, Vec3f vec3f2) {
    }

    @C0064Am(aul = "fc8cdd0cbe14b461fc9e063b80362d86", aum = 0)
    /* renamed from: io */
    private String m11036io() {
        return m11043rh().getHandle();
    }

    @C0064Am(aul = "ba794c09dfb43eaa8cacdbf40f73ace0", aum = 0)
    /* renamed from: iq */
    private String m11037iq() {
        return m11043rh().getFile();
    }

    @C0064Am(aul = "97e6e947dd01a8f90e8be18a8515b9fa", aum = 0)
    /* renamed from: a */
    private void m11021a(Actor cr, C5260aCm acm, Vec3f vec3f, Vec3f vec3f2) {
    }

    @C0064Am(aul = "c5b389a755629032725ed20990504da3", aum = 0)
    /* renamed from: aG */
    private void m11024aG() {
        super.mo70aH();
        if (m11026ao() == null) {
            setHandle(String.valueOf(chF().getHandle()) + "_" + cWm());
        }
    }

    @C0064Am(aul = "2f547034353bae3890be2be443418c3e", aum = 0)
    /* renamed from: it */
    private String m11038it() {
        return m11035ij();
    }

    @C0064Am(aul = "04fb037c156853b2906e2f5ca54959b2", aum = 0)
    /* renamed from: a */
    private void m11020a(Actor.C0200h hVar) {
        if (m11043rh() == null) {
            throw new IllegalStateException("ShapedObject of class '" + getClass().getName() + "' does not have a RenderAsset");
        }
        String file = m11043rh().getFile();
        if (!bGX() || !(ald().getLoaderTrail() instanceof LoaderTrail)) {
            hVar.mo1103c(C2606hU.m32703b(file, false));
        } else {
            ((LoaderTrail) ald().getLoaderTrail()).mo19098a(file, new C1511a(hVar), getName(), false);
        }
    }

    @C0064Am(aul = "01f54d3aa9764cd19d26499644ac30b0", aum = 0)
    @C2198cg
    @C1253SX
    /* renamed from: c */
    private C0520HN m11032c(Space ea, long j) {
        aBA aba = new aBA(ea, this, mo958IL());
        aba.mo2449a(ea.cKn().mo5725a(j, (C2235dB) aba, 13));
        return aba;
    }

    /* renamed from: a.WE$a */
    class C1511a implements C2601hQ {
        private final /* synthetic */ Actor.C0200h eyn;

        C1511a(Actor.C0200h hVar) {
            this.eyn = hVar;
        }

        /* renamed from: b */
        public void mo663b(C4029yK yKVar) {
            this.eyn.mo1103c(yKVar);
        }
    }
}
