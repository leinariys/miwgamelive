package game.script.space;

import game.network.message.externalizable.aCE;
import game.network.message.serializable.C3438ra;
import game.script.TaikodomObject;
import game.script.pda.DatabaseCategory;
import game.script.pda.TaikopediaEntry;
import game.script.resource.Asset;
import logic.baa.*;
import logic.data.mbean.C0685Jl;
import logic.res.code.C5640aRc;
import logic.res.code.C5663aRz;
import logic.res.code.C6494any;
import logic.res.html.C2491fm;
import p001a.*;
import taikodom.infra.script.I18NString;

import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.UUID;

@C5511aMd
@C6485anp
/* renamed from: a.AK */
/* compiled from: a */
public class SpaceCategory extends TaikodomObject implements C0468GU, C1616Xf, C4068yr {

    /* renamed from: MN */
    public static final C2491fm f20MN = null;

    /* renamed from: NY */
    public static final C5663aRz f21NY = null;

    /* renamed from: Ob */
    public static final C2491fm f22Ob = null;

    /* renamed from: Oc */
    public static final C2491fm f23Oc = null;

    /* renamed from: QA */
    public static final C5663aRz f24QA = null;

    /* renamed from: QD */
    public static final C2491fm f25QD = null;

    /* renamed from: QE */
    public static final C2491fm f26QE = null;

    /* renamed from: QF */
    public static final C2491fm f27QF = null;

    /* renamed from: Qz */
    public static final C5663aRz f28Qz = null;
    public static final int _m_fieldCount = 0;
    public static final C5663aRz[] _m_fields = null;
    public static final int _m_methodCount = 0;
    public static final C2491fm[] _m_methods = null;
    public static final C5663aRz aMg = null;
    public static final C5663aRz aMh = null;
    public static final C5663aRz aMi = null;
    public static final C2491fm aMj = null;
    public static final C2491fm aMk = null;
    public static final C2491fm aMl = null;
    public static final C2491fm aMm = null;
    public static final C2491fm aMn = null;
    public static final C2491fm aMo = null;
    public static final C2491fm aMp = null;
    public static final C2491fm aMq = null;
    public static final C2491fm aMr = null;
    public static final C2491fm aMs = null;
    public static final C2491fm aMu = null;
    public static final C2491fm aMv = null;
    public static final C5663aRz awd = null;
    /* renamed from: bL */
    public static final C5663aRz f30bL = null;
    /* renamed from: bM */
    public static final C5663aRz f31bM = null;
    /* renamed from: bN */
    public static final C2491fm f32bN = null;
    /* renamed from: bO */
    public static final C2491fm f33bO = null;
    /* renamed from: bP */
    public static final C2491fm f34bP = null;
    /* renamed from: bQ */
    public static final C2491fm f35bQ = null;
    public static final long serialVersionUID = 0;
    public static C6494any ___iScriptClass;
    @C0064Am(aul = "849f84e963f13cd4449e6b6253c29f21", aum = 8)

    /* renamed from: bK */
    private static UUID f29bK;
    @C0064Am(aul = "5e2360d977b81e67459fe7f482ea3565", aum = 0)
    private static String handle;
    @C0064Am(aul = "074ca0ade414977a7f3924d537b99e29", aum = 5)

    /* renamed from: jS */
    private static DatabaseCategory f36jS;
    @C0064Am(aul = "aab8c078d2f3ecda6ffc682d45f88d90", aum = 6)

    /* renamed from: jT */
    private static C3438ra<TaikopediaEntry> f37jT;
    @C0064Am(aul = "19bc996008a99c4b84ff603a3c77afdd", aum = 2)

    /* renamed from: jU */
    private static Asset f38jU;
    @C0064Am(aul = "2f52a0ca238bc1f2ed09aa8dd9ef41e4", aum = 7)

    /* renamed from: jV */
    private static float f39jV;
    @C0064Am(aul = "c2e0b57f4c6cde4ecd0fda031aa61a6e", aum = 1)

    /* renamed from: jn */
    private static Asset f40jn;
    @C0064Am(aul = "e91f863fedbb076805aab2c8b55aab3b", aum = 4)

    /* renamed from: nh */
    private static I18NString f41nh;
    @C0064Am(aul = "a0b9a5d017d3acc6854c4f16e1356181", aum = 3)

    /* renamed from: ni */
    private static I18NString f42ni;

    static {
        m239V();
    }

    public SpaceCategory() {
        super((C5540aNg) null);
        super.mo10S();
    }

    public SpaceCategory(C5540aNg ang) {
        super(ang);
    }

    /* renamed from: V */
    static void m239V() {
        serialVersionUID = (long) 1;
        _m_fieldCount = TaikodomObject._m_fieldCount + 9;
        _m_methodCount = TaikodomObject._m_methodCount + 22;
        int i = TaikodomObject._m_fieldCount;
        C5663aRz[] arzArr = new C5663aRz[(i + 9)];
        C5663aRz b = C5640aRc.m17844b(SpaceCategory.class, "5e2360d977b81e67459fe7f482ea3565", i);
        f31bM = b;
        arzArr[i] = b;
        int i2 = i + 1;
        C5663aRz b2 = C5640aRc.m17844b(SpaceCategory.class, "c2e0b57f4c6cde4ecd0fda031aa61a6e", i2);
        f21NY = b2;
        arzArr[i2] = b2;
        int i3 = i2 + 1;
        C5663aRz b3 = C5640aRc.m17844b(SpaceCategory.class, "19bc996008a99c4b84ff603a3c77afdd", i3);
        aMg = b3;
        arzArr[i3] = b3;
        int i4 = i3 + 1;
        C5663aRz b4 = C5640aRc.m17844b(SpaceCategory.class, "a0b9a5d017d3acc6854c4f16e1356181", i4);
        f28Qz = b4;
        arzArr[i4] = b4;
        int i5 = i4 + 1;
        C5663aRz b5 = C5640aRc.m17844b(SpaceCategory.class, "e91f863fedbb076805aab2c8b55aab3b", i5);
        f24QA = b5;
        arzArr[i5] = b5;
        int i6 = i5 + 1;
        C5663aRz b6 = C5640aRc.m17844b(SpaceCategory.class, "074ca0ade414977a7f3924d537b99e29", i6);
        aMh = b6;
        arzArr[i6] = b6;
        int i7 = i6 + 1;
        C5663aRz b7 = C5640aRc.m17844b(SpaceCategory.class, "aab8c078d2f3ecda6ffc682d45f88d90", i7);
        awd = b7;
        arzArr[i7] = b7;
        int i8 = i7 + 1;
        C5663aRz b8 = C5640aRc.m17844b(SpaceCategory.class, "2f52a0ca238bc1f2ed09aa8dd9ef41e4", i8);
        aMi = b8;
        arzArr[i8] = b8;
        int i9 = i8 + 1;
        C5663aRz b9 = C5640aRc.m17844b(SpaceCategory.class, "849f84e963f13cd4449e6b6253c29f21", i9);
        f30bL = b9;
        arzArr[i9] = b9;
        int i10 = i9 + 1;
        _m_fields = arzArr;
        C1634Xv.m11725a((Object[]) TaikodomObject._m_fields, (Object[]) _m_fields);
        int i11 = TaikodomObject._m_methodCount;
        C2491fm[] fmVarArr = new C2491fm[(i11 + 22)];
        C2491fm a = C4105zY.m41624a(SpaceCategory.class, "c14613765bf3905bf144669d7b16b59e", i11);
        f32bN = a;
        fmVarArr[i11] = a;
        int i12 = i11 + 1;
        C2491fm a2 = C4105zY.m41624a(SpaceCategory.class, "7fef79873d602f0cc9e62912151f9901", i12);
        f33bO = a2;
        fmVarArr[i12] = a2;
        int i13 = i12 + 1;
        C2491fm a3 = C4105zY.m41624a(SpaceCategory.class, "7cb151c1ce30aa0a0b133e519d9fc074", i13);
        f34bP = a3;
        fmVarArr[i13] = a3;
        int i14 = i13 + 1;
        C2491fm a4 = C4105zY.m41624a(SpaceCategory.class, "63448a9bd3b95cd39724c438494b7e94", i14);
        f35bQ = a4;
        fmVarArr[i14] = a4;
        int i15 = i14 + 1;
        C2491fm a5 = C4105zY.m41624a(SpaceCategory.class, "82e7a16066c5047dd0e1362bad04523d", i15);
        f20MN = a5;
        fmVarArr[i15] = a5;
        int i16 = i15 + 1;
        C2491fm a6 = C4105zY.m41624a(SpaceCategory.class, "c65634957fbd0f221a0f9c3477fc2fca", i16);
        f25QD = a6;
        fmVarArr[i16] = a6;
        int i17 = i16 + 1;
        C2491fm a7 = C4105zY.m41624a(SpaceCategory.class, "dfd9dbf6651a09589b8d0a37549b1dbe", i17);
        f26QE = a7;
        fmVarArr[i17] = a7;
        int i18 = i17 + 1;
        C2491fm a8 = C4105zY.m41624a(SpaceCategory.class, "ea97ab53a7bd9fa3afd53898a8f14ee9", i18);
        f27QF = a8;
        fmVarArr[i18] = a8;
        int i19 = i18 + 1;
        C2491fm a9 = C4105zY.m41624a(SpaceCategory.class, "425ef90c69258f72ca4ea7366c0080db", i19);
        aMj = a9;
        fmVarArr[i19] = a9;
        int i20 = i19 + 1;
        C2491fm a10 = C4105zY.m41624a(SpaceCategory.class, "775ef975443498db307d78135b655975", i20);
        aMk = a10;
        fmVarArr[i20] = a10;
        int i21 = i20 + 1;
        C2491fm a11 = C4105zY.m41624a(SpaceCategory.class, "8d987c027e712567bd01f32b87fbfe6a", i21);
        aMl = a11;
        fmVarArr[i21] = a11;
        int i22 = i21 + 1;
        C2491fm a12 = C4105zY.m41624a(SpaceCategory.class, "1530ac4c51d0e21cb6acca68e63932aa", i22);
        aMm = a12;
        fmVarArr[i22] = a12;
        int i23 = i22 + 1;
        C2491fm a13 = C4105zY.m41624a(SpaceCategory.class, "6399628dda26cb2737f06954e3ddfa03", i23);
        aMn = a13;
        fmVarArr[i23] = a13;
        int i24 = i23 + 1;
        C2491fm a14 = C4105zY.m41624a(SpaceCategory.class, "ac1ce6d2f72f1dab14985a71591fe69a", i24);
        aMo = a14;
        fmVarArr[i24] = a14;
        int i25 = i24 + 1;
        C2491fm a15 = C4105zY.m41624a(SpaceCategory.class, "cdbee85a3d9083ffae7cc47ffbb9a3dc", i25);
        aMp = a15;
        fmVarArr[i25] = a15;
        int i26 = i25 + 1;
        C2491fm a16 = C4105zY.m41624a(SpaceCategory.class, "493756c64a84efd631574599e853005e", i26);
        f23Oc = a16;
        fmVarArr[i26] = a16;
        int i27 = i26 + 1;
        C2491fm a17 = C4105zY.m41624a(SpaceCategory.class, "f68016d14c0cc36005d162d9465b950a", i27);
        f22Ob = a17;
        fmVarArr[i27] = a17;
        int i28 = i27 + 1;
        C2491fm a18 = C4105zY.m41624a(SpaceCategory.class, "cab042c557e15ed782fe8dfb777e20c2", i28);
        aMq = a18;
        fmVarArr[i28] = a18;
        int i29 = i28 + 1;
        C2491fm a19 = C4105zY.m41624a(SpaceCategory.class, "9391ecf2e6ffaf8c18531a08d6d5e139", i29);
        aMr = a19;
        fmVarArr[i29] = a19;
        int i30 = i29 + 1;
        C2491fm a20 = C4105zY.m41624a(SpaceCategory.class, "81e0edd887fab9d54241e9b55c8742c1", i30);
        aMs = a20;
        fmVarArr[i30] = a20;
        int i31 = i30 + 1;
        C2491fm a21 = C4105zY.m41624a(SpaceCategory.class, "830e7d4e3a952be2363b7e5ab41d3e00", i31);
        aMu = a21;
        fmVarArr[i31] = a21;
        int i32 = i31 + 1;
        C2491fm a22 = C4105zY.m41624a(SpaceCategory.class, "01adacaed41e05acac08d99099e4929d", i32);
        aMv = a22;
        fmVarArr[i32] = a22;
        int i33 = i32 + 1;
        _m_methods = fmVarArr;
        C1634Xv.m11725a((Object[]) TaikodomObject._m_methods, (Object[]) _m_methods);
        ___iScriptClass = aUO.m18566a(SpaceCategory.class, C0685Jl.class, _m_fields, _m_methods);
    }

    /* renamed from: L */
    private void m226L(Asset tCVar) {
        bFf().mo5608dq().mo3197f(aMg, tCVar);
    }

    /* renamed from: Ll */
    private C3438ra m227Ll() {
        return (C3438ra) bFf().mo5608dq().mo3214p(awd);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Pda Render")
    @C0064Am(aul = "81e0edd887fab9d54241e9b55c8742c1", aum = 0)
    @C5566aOg
    /* renamed from: M */
    private void m228M(Asset tCVar) {
        throw new aWi(new aCE(this, aMs, new Object[]{tCVar}));
    }

    /* renamed from: RQ */
    private Asset m229RQ() {
        return (Asset) bFf().mo5608dq().mo3214p(aMg);
    }

    /* renamed from: RR */
    private DatabaseCategory m230RR() {
        return (DatabaseCategory) bFf().mo5608dq().mo3214p(aMh);
    }

    /* renamed from: RS */
    private float m231RS() {
        return bFf().mo5608dq().mo3211m(aMi);
    }

    /* renamed from: a */
    private void m240a(DatabaseCategory aik) {
        bFf().mo5608dq().mo3197f(aMh, aik);
    }

    /* renamed from: a */
    private void m242a(String str) {
        bFf().mo5608dq().mo3197f(f31bM, str);
    }

    /* renamed from: a */
    private void m243a(UUID uuid) {
        bFf().mo5608dq().mo3197f(f30bL, uuid);
    }

    /* renamed from: an */
    private UUID m244an() {
        return (UUID) bFf().mo5608dq().mo3214p(f30bL);
    }

    /* renamed from: ao */
    private String m245ao() {
        return (String) bFf().mo5608dq().mo3214p(f31bM);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Database Category")
    @C0064Am(aul = "ac1ce6d2f72f1dab14985a71591fe69a", aum = 0)
    @C5566aOg
    /* renamed from: b */
    private void m248b(DatabaseCategory aik) {
        throw new aWi(new aCE(this, aMo, new Object[]{aik}));
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Handle")
    @C0064Am(aul = "63448a9bd3b95cd39724c438494b7e94", aum = 0)
    @C5566aOg
    /* renamed from: b */
    private void m249b(String str) {
        throw new aWi(new aCE(this, f35bQ, new Object[]{str}));
    }

    /* renamed from: bq */
    private void m251bq(I18NString i18NString) {
        bFf().mo5608dq().mo3197f(f28Qz, i18NString);
    }

    /* renamed from: br */
    private void m252br(I18NString i18NString) {
        bFf().mo5608dq().mo3197f(f24QA, i18NString);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Title")
    @C0064Am(aul = "c65634957fbd0f221a0f9c3477fc2fca", aum = 0)
    @C5566aOg
    /* renamed from: bs */
    private void m253bs(I18NString i18NString) {
        throw new aWi(new aCE(this, f25QD, new Object[]{i18NString}));
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Description")
    @C0064Am(aul = "ea97ab53a7bd9fa3afd53898a8f14ee9", aum = 0)
    @C5566aOg
    /* renamed from: bu */
    private void m254bu(I18NString i18NString) {
        throw new aWi(new aCE(this, f27QF, new Object[]{i18NString}));
    }

    /* renamed from: c */
    private void m256c(UUID uuid) {
        switch (bFf().mo6893i(f33bO)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f33bO, new Object[]{uuid}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f33bO, new Object[]{uuid}));
                break;
        }
        m250b(uuid);
    }

    /* renamed from: cx */
    private void m257cx(float f) {
        bFf().mo5608dq().mo3150a(aMi, f);
    }

    /* renamed from: sG */
    private Asset m260sG() {
        return (Asset) bFf().mo5608dq().mo3214p(f21NY);
    }

    /* renamed from: t */
    private void m262t(Asset tCVar) {
        bFf().mo5608dq().mo3197f(f21NY, tCVar);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Icon")
    @C0064Am(aul = "493756c64a84efd631574599e853005e", aum = 0)
    @C5566aOg
    /* renamed from: u */
    private void m263u(Asset tCVar) {
        throw new aWi(new aCE(this, f23Oc, new Object[]{tCVar}));
    }

    /* renamed from: vS */
    private I18NString m264vS() {
        return (I18NString) bFf().mo5608dq().mo3214p(f28Qz);
    }

    /* renamed from: vT */
    private I18NString m265vT() {
        return (I18NString) bFf().mo5608dq().mo3214p(f24QA);
    }

    /* renamed from: x */
    private void m267x(C3438ra raVar) {
        bFf().mo5608dq().mo3197f(awd, raVar);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Pda Render")
    @C5566aOg
    /* renamed from: N */
    public void mo181N(Asset tCVar) {
        switch (bFf().mo6893i(aMs)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, aMs, new Object[]{tCVar}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, aMs, new Object[]{tCVar}));
                break;
        }
        m228M(tCVar);
    }

    /* renamed from: RU */
    public I18NString mo182RU() {
        switch (bFf().mo6893i(aMj)) {
            case 0:
                return null;
            case 2:
                return (I18NString) bFf().mo5606d(new aCE(this, aMj, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, aMj, new Object[0]));
                break;
        }
        return m232RT();
    }

    @C3248pc(aYR = C3248pc.C3250b.ITERATOR, displayName = "Taikopedia Entries")
    /* renamed from: RW */
    public List<TaikopediaEntry> mo183RW() {
        switch (bFf().mo6893i(aMk)) {
            case 0:
                return null;
            case 2:
                return (List) bFf().mo5606d(new aCE(this, aMk, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, aMk, new Object[0]));
                break;
        }
        return m233RV();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Database Category")
    /* renamed from: RY */
    public DatabaseCategory mo184RY() {
        switch (bFf().mo6893i(aMn)) {
            case 0:
                return null;
            case 2:
                return (DatabaseCategory) bFf().mo5606d(new aCE(this, aMn, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, aMn, new Object[0]));
                break;
        }
        return m234RX();
    }

    /* renamed from: Sa */
    public String mo185Sa() {
        switch (bFf().mo6893i(aMp)) {
            case 0:
                return null;
            case 2:
                return (String) bFf().mo5606d(new aCE(this, aMp, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, aMp, new Object[0]));
                break;
        }
        return m235RZ();
    }

    /* renamed from: Sc */
    public String mo186Sc() {
        switch (bFf().mo6893i(aMq)) {
            case 0:
                return null;
            case 2:
                return (String) bFf().mo5606d(new aCE(this, aMq, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, aMq, new Object[0]));
                break;
        }
        return m236Sb();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Pda Render")
    /* renamed from: Se */
    public Asset mo187Se() {
        switch (bFf().mo6893i(aMr)) {
            case 0:
                return null;
            case 2:
                return (Asset) bFf().mo5606d(new aCE(this, aMr, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, aMr, new Object[0]));
                break;
        }
        return m237Sd();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Pda MP Multiplier")
    /* renamed from: Sg */
    public float mo188Sg() {
        switch (bFf().mo6893i(aMu)) {
            case 0:
                return 0.0f;
            case 2:
                return ((Float) bFf().mo5606d(new aCE(this, aMu, new Object[0]))).floatValue();
            case 3:
                bFf().mo5606d(new aCE(this, aMu, new Object[0]));
                break;
        }
        return m238Sf();
    }

    /* renamed from: T */
    public C6494any mo11T() {
        return ___iScriptClass;
    }

    /* renamed from: U */
    public C2491fm[] mo12U() {
        return _m_methods;
    }

    /* renamed from: W */
    public Object mo13W() {
        return new C0685Jl(this);
    }

    /* renamed from: a */
    public Object mo14a(C0495Gr gr) {
        Object[] args = gr.getArgs();
        switch (gr.mo2417hq() - TaikodomObject._m_methodCount) {
            case 0:
                return m246ap();
            case 1:
                m250b((UUID) args[0]);
                return null;
            case 2:
                return m247ar();
            case 3:
                m249b((String) args[0]);
                return null;
            case 4:
                return m259rO();
            case 5:
                m253bs((I18NString) args[0]);
                return null;
            case 6:
                return m266vV();
            case 7:
                m254bu((I18NString) args[0]);
                return null;
            case 8:
                return m232RT();
            case 9:
                return m233RV();
            case 10:
                m241a((TaikopediaEntry) args[0]);
                return null;
            case 11:
                m255c((TaikopediaEntry) args[0]);
                return null;
            case 12:
                return m234RX();
            case 13:
                m248b((DatabaseCategory) args[0]);
                return null;
            case 14:
                return m235RZ();
            case 15:
                m263u((Asset) args[0]);
                return null;
            case 16:
                return m261sJ();
            case 17:
                return m236Sb();
            case 18:
                return m237Sd();
            case 19:
                m228M((Asset) args[0]);
                return null;
            case 20:
                return new Float(m238Sf());
            case 21:
                m258cy(((Float) args[0]).floatValue());
                return null;
            default:
                return super.mo14a(gr);
        }
    }

    /* renamed from: a */
    public void mo15a(Collection collection, C0495Gr gr) {
        super.mo15a(collection, gr);
    }

    /* renamed from: aq */
    public UUID mo18aq() {
        switch (bFf().mo6893i(f32bN)) {
            case 0:
                return null;
            case 2:
                return (UUID) bFf().mo5606d(new aCE(this, f32bN, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f32bN, new Object[0]));
                break;
        }
        return m246ap();
    }

    @C3248pc(aYR = C3248pc.C3250b.ADDER, displayName = "Taikopedia Entries")
    /* renamed from: b */
    public void mo189b(TaikopediaEntry aiz) {
        switch (bFf().mo6893i(aMl)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, aMl, new Object[]{aiz}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, aMl, new Object[]{aiz}));
                break;
        }
        m241a(aiz);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Title")
    @C5566aOg
    /* renamed from: bt */
    public void mo190bt(I18NString i18NString) {
        switch (bFf().mo6893i(f25QD)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f25QD, new Object[]{i18NString}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f25QD, new Object[]{i18NString}));
                break;
        }
        m253bs(i18NString);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Description")
    @C5566aOg
    /* renamed from: bv */
    public void mo191bv(I18NString i18NString) {
        switch (bFf().mo6893i(f27QF)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f27QF, new Object[]{i18NString}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f27QF, new Object[]{i18NString}));
                break;
        }
        m254bu(i18NString);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Database Category")
    @C5566aOg
    /* renamed from: c */
    public void mo192c(DatabaseCategory aik) {
        switch (bFf().mo6893i(aMo)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, aMo, new Object[]{aik}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, aMo, new Object[]{aik}));
                break;
        }
        m248b(aik);
    }

    /* renamed from: c */
    public C5663aRz[] mo25c() {
        return _m_fields;
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Pda MP Multiplier")
    /* renamed from: cz */
    public void mo193cz(float f) {
        switch (bFf().mo6893i(aMv)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, aMv, new Object[]{new Float(f)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, aMv, new Object[]{new Float(f)}));
                break;
        }
        m258cy(f);
    }

    @C3248pc(aYR = C3248pc.C3250b.REMOVER, displayName = "Taikopedia Entries")
    /* renamed from: d */
    public void mo194d(TaikopediaEntry aiz) {
        switch (bFf().mo6893i(aMm)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, aMm, new Object[]{aiz}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, aMm, new Object[]{aiz}));
                break;
        }
        m255c(aiz);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Handle")
    public String getHandle() {
        switch (bFf().mo6893i(f34bP)) {
            case 0:
                return null;
            case 2:
                return (String) bFf().mo5606d(new aCE(this, f34bP, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f34bP, new Object[0]));
                break;
        }
        return m247ar();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Handle")
    @C5566aOg
    public void setHandle(String str) {
        switch (bFf().mo6893i(f35bQ)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f35bQ, new Object[]{str}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f35bQ, new Object[]{str}));
                break;
        }
        m249b(str);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Title")
    /* renamed from: rP */
    public I18NString mo195rP() {
        switch (bFf().mo6893i(f20MN)) {
            case 0:
                return null;
            case 2:
                return (I18NString) bFf().mo5606d(new aCE(this, f20MN, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f20MN, new Object[0]));
                break;
        }
        return m259rO();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Icon")
    /* renamed from: sK */
    public Asset mo196sK() {
        switch (bFf().mo6893i(f22Ob)) {
            case 0:
                return null;
            case 2:
                return (Asset) bFf().mo5606d(new aCE(this, f22Ob, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f22Ob, new Object[0]));
                break;
        }
        return m261sJ();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Icon")
    @C5566aOg
    /* renamed from: v */
    public void mo198v(Asset tCVar) {
        switch (bFf().mo6893i(f23Oc)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f23Oc, new Object[]{tCVar}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f23Oc, new Object[]{tCVar}));
                break;
        }
        m263u(tCVar);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Description")
    /* renamed from: vW */
    public I18NString mo199vW() {
        switch (bFf().mo6893i(f26QE)) {
            case 0:
                return null;
            case 2:
                return (I18NString) bFf().mo5606d(new aCE(this, f26QE, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f26QE, new Object[0]));
                break;
        }
        return m266vV();
    }

    @C0064Am(aul = "c14613765bf3905bf144669d7b16b59e", aum = 0)
    /* renamed from: ap */
    private UUID m246ap() {
        return m244an();
    }

    @C0064Am(aul = "7fef79873d602f0cc9e62912151f9901", aum = 0)
    /* renamed from: b */
    private void m250b(UUID uuid) {
        m243a(uuid);
    }

    /* renamed from: S */
    public void mo10S() {
        super.mo10S();
        m243a(UUID.randomUUID());
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Handle")
    @C0064Am(aul = "7cb151c1ce30aa0a0b133e519d9fc074", aum = 0)
    /* renamed from: ar */
    private String m247ar() {
        return m245ao();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Title")
    @C0064Am(aul = "82e7a16066c5047dd0e1362bad04523d", aum = 0)
    /* renamed from: rO */
    private I18NString m259rO() {
        return m264vS();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Description")
    @C0064Am(aul = "dfd9dbf6651a09589b8d0a37549b1dbe", aum = 0)
    /* renamed from: vV */
    private I18NString m266vV() {
        return m265vT();
    }

    @C0064Am(aul = "425ef90c69258f72ca4ea7366c0080db", aum = 0)
    /* renamed from: RT */
    private I18NString m232RT() {
        return mo199vW();
    }

    @C3248pc(aYR = C3248pc.C3250b.ITERATOR, displayName = "Taikopedia Entries")
    @C0064Am(aul = "775ef975443498db307d78135b655975", aum = 0)
    /* renamed from: RV */
    private List<TaikopediaEntry> m233RV() {
        return Collections.unmodifiableList(m227Ll());
    }

    @C3248pc(aYR = C3248pc.C3250b.ADDER, displayName = "Taikopedia Entries")
    @C0064Am(aul = "8d987c027e712567bd01f32b87fbfe6a", aum = 0)
    /* renamed from: a */
    private void m241a(TaikopediaEntry aiz) {
        m227Ll().add(aiz);
    }

    @C3248pc(aYR = C3248pc.C3250b.REMOVER, displayName = "Taikopedia Entries")
    @C0064Am(aul = "1530ac4c51d0e21cb6acca68e63932aa", aum = 0)
    /* renamed from: c */
    private void m255c(TaikopediaEntry aiz) {
        m227Ll().remove(aiz);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Database Category")
    @C0064Am(aul = "6399628dda26cb2737f06954e3ddfa03", aum = 0)
    /* renamed from: RX */
    private DatabaseCategory m234RX() {
        return m230RR();
    }

    @C0064Am(aul = "cdbee85a3d9083ffae7cc47ffbb9a3dc", aum = 0)
    /* renamed from: RZ */
    private String m235RZ() {
        if (m260sG() != null) {
            return m260sG().getHandle();
        }
        return null;
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Icon")
    @C0064Am(aul = "f68016d14c0cc36005d162d9465b950a", aum = 0)
    /* renamed from: sJ */
    private Asset m261sJ() {
        return m260sG();
    }

    @C0064Am(aul = "cab042c557e15ed782fe8dfb777e20c2", aum = 0)
    /* renamed from: Sb */
    private String m236Sb() {
        return m229RQ().getHandle();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Pda Render")
    @C0064Am(aul = "9391ecf2e6ffaf8c18531a08d6d5e139", aum = 0)
    /* renamed from: Sd */
    private Asset m237Sd() {
        return m229RQ();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Pda MP Multiplier")
    @C0064Am(aul = "830e7d4e3a952be2363b7e5ab41d3e00", aum = 0)
    /* renamed from: Sf */
    private float m238Sf() {
        return m231RS();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Pda MP Multiplier")
    @C0064Am(aul = "01adacaed41e05acac08d99099e4929d", aum = 0)
    /* renamed from: cy */
    private void m258cy(float f) {
        m257cx(f);
    }
}
