package game.script.space;

import game.network.message.externalizable.aCE;
import game.script.resource.Asset;
import game.script.template.BaseTaikodomContent;
import logic.baa.*;
import logic.data.mbean.C0398FX;
import logic.res.code.C5640aRc;
import logic.res.code.C5663aRz;
import logic.res.code.C6494any;
import logic.res.html.C2491fm;
import p001a.*;

import java.util.Collection;

@C0566Hp
@C5511aMd
@C6485anp
/* renamed from: a.KP */
/* compiled from: a */
public class AdvertiseType extends BaseTaikodomContent implements C1616Xf, C6251ajP {
    /* renamed from: LR */
    public static final C5663aRz f951LR = null;
    public static final int _m_fieldCount = 0;
    public static final C5663aRz[] _m_fields = null;
    public static final int _m_methodCount = 0;
    public static final C2491fm[] _m_methods = null;
    public static final C2491fm aDk = null;
    public static final C2491fm aDl = null;
    /* renamed from: dN */
    public static final C2491fm f952dN = null;
    public static final C2491fm dqT = null;
    public static final long serialVersionUID = 0;
    /* renamed from: uU */
    public static final C5663aRz f954uU = null;
    /* renamed from: vi */
    public static final C2491fm f955vi = null;
    /* renamed from: vj */
    public static final C2491fm f956vj = null;
    public static C6494any ___iScriptClass;
    @C0064Am(aul = "eca500ef55c4bb72835e001cc1d8bbdc", aum = 0)

    /* renamed from: LQ */
    private static Asset f950LQ;
    @C0064Am(aul = "563fa5c9833dae1507f8e182549b54d9", aum = 1)

    /* renamed from: uT */
    private static String f953uT;

    static {
        m6325V();
    }

    public AdvertiseType() {
        super((C5540aNg) null);
        super.mo10S();
    }

    public AdvertiseType(C5540aNg ang) {
        super(ang);
    }

    /* renamed from: V */
    static void m6325V() {
        serialVersionUID = (long) 1;
        _m_fieldCount = BaseTaikodomContent._m_fieldCount + 2;
        _m_methodCount = BaseTaikodomContent._m_methodCount + 6;
        int i = BaseTaikodomContent._m_fieldCount;
        C5663aRz[] arzArr = new C5663aRz[(i + 2)];
        C5663aRz b = C5640aRc.m17844b(AdvertiseType.class, "eca500ef55c4bb72835e001cc1d8bbdc", i);
        f951LR = b;
        arzArr[i] = b;
        int i2 = i + 1;
        C5663aRz b2 = C5640aRc.m17844b(AdvertiseType.class, "563fa5c9833dae1507f8e182549b54d9", i2);
        f954uU = b2;
        arzArr[i2] = b2;
        int i3 = i2 + 1;
        _m_fields = arzArr;
        C1634Xv.m11725a((Object[]) BaseTaikodomContent._m_fields, (Object[]) _m_fields);
        int i4 = BaseTaikodomContent._m_methodCount;
        C2491fm[] fmVarArr = new C2491fm[(i4 + 6)];
        C2491fm a = C4105zY.m41624a(AdvertiseType.class, "ac763623b5cfaf6129de3dfcf2990907", i4);
        aDk = a;
        fmVarArr[i4] = a;
        int i5 = i4 + 1;
        C2491fm a2 = C4105zY.m41624a(AdvertiseType.class, "618cc3e9085ef441719cb6f3b1867764", i5);
        aDl = a2;
        fmVarArr[i5] = a2;
        int i6 = i5 + 1;
        C2491fm a3 = C4105zY.m41624a(AdvertiseType.class, "dfa663f2ce8bf87af8db229c605ce80c", i6);
        f955vi = a3;
        fmVarArr[i6] = a3;
        int i7 = i6 + 1;
        C2491fm a4 = C4105zY.m41624a(AdvertiseType.class, "af5635bd24cd52929c3d1a2698640451", i7);
        f956vj = a4;
        fmVarArr[i7] = a4;
        int i8 = i7 + 1;
        C2491fm a5 = C4105zY.m41624a(AdvertiseType.class, "be9cdf4410771acfb3d3bfcb1e936d61", i8);
        dqT = a5;
        fmVarArr[i8] = a5;
        int i9 = i8 + 1;
        C2491fm a6 = C4105zY.m41624a(AdvertiseType.class, "99deb3a71e4afd5f79da10c528a283dd", i9);
        f952dN = a6;
        fmVarArr[i9] = a6;
        int i10 = i9 + 1;
        _m_methods = fmVarArr;
        C1634Xv.m11725a((Object[]) BaseTaikodomContent._m_methods, (Object[]) _m_methods);
        ___iScriptClass = aUO.m18566a(AdvertiseType.class, C0398FX.class, _m_fields, _m_methods);
    }

    /* renamed from: H */
    private void m6322H(String str) {
        bFf().mo5608dq().mo3197f(f954uU, str);
    }

    /* renamed from: ij */
    private String m6327ij() {
        return (String) bFf().mo5608dq().mo3214p(f954uU);
    }

    /* renamed from: r */
    private void m6329r(Asset tCVar) {
        bFf().mo5608dq().mo3197f(f951LR, tCVar);
    }

    /* renamed from: rh */
    private Asset m6330rh() {
        return (Asset) bFf().mo5608dq().mo3214p(f951LR);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Render Asset")
    /* renamed from: I */
    public void mo3519I(Asset tCVar) {
        switch (bFf().mo6893i(aDl)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, aDl, new Object[]{tCVar}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, aDl, new Object[]{tCVar}));
                break;
        }
        m6321H(tCVar);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Surface Material")
    /* renamed from: N */
    public void mo3520N(String str) {
        switch (bFf().mo6893i(f956vj)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f956vj, new Object[]{str}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f956vj, new Object[]{str}));
                break;
        }
        m6323M(str);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Render Asset")
    /* renamed from: Nu */
    public Asset mo3521Nu() {
        switch (bFf().mo6893i(aDk)) {
            case 0:
                return null;
            case 2:
                return (Asset) bFf().mo5606d(new aCE(this, aDk, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, aDk, new Object[0]));
                break;
        }
        return m6324Nt();
    }

    /* renamed from: T */
    public C6494any mo11T() {
        return ___iScriptClass;
    }

    /* renamed from: U */
    public C2491fm[] mo12U() {
        return _m_methods;
    }

    /* renamed from: W */
    public Object mo13W() {
        return new C0398FX(this);
    }

    /* renamed from: a */
    public Object mo14a(C0495Gr gr) {
        Object[] args = gr.getArgs();
        switch (gr.mo2417hq() - BaseTaikodomContent._m_methodCount) {
            case 0:
                return m6324Nt();
            case 1:
                m6321H((Asset) args[0]);
                return null;
            case 2:
                return m6328it();
            case 3:
                m6323M((String) args[0]);
                return null;
            case 4:
                return bdi();
            case 5:
                return m6326aT();
            default:
                return super.mo14a(gr);
        }
    }

    /* renamed from: a */
    public void mo15a(Collection collection, C0495Gr gr) {
        super.mo15a(collection, gr);
    }

    /* access modifiers changed from: protected */
    /* renamed from: aU */
    public <T extends aDJ> T mo745aU() {
        switch (bFf().mo6893i(f952dN)) {
            case 0:
                return null;
            case 2:
                return (aDJ) bFf().mo5606d(new aCE(this, f952dN, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f952dN, new Object[0]));
                break;
        }
        return m6326aT();
    }

    public Advertise bdj() {
        switch (bFf().mo6893i(dqT)) {
            case 0:
                return null;
            case 2:
                return (Advertise) bFf().mo5606d(new aCE(this, dqT, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, dqT, new Object[0]));
                break;
        }
        return bdi();
    }

    /* renamed from: c */
    public C5663aRz[] mo25c() {
        return _m_fields;
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Surface Material")
    /* renamed from: iu */
    public String mo3523iu() {
        switch (bFf().mo6893i(f955vi)) {
            case 0:
                return null;
            case 2:
                return (String) bFf().mo5606d(new aCE(this, f955vi, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f955vi, new Object[0]));
                break;
        }
        return m6328it();
    }

    /* renamed from: S */
    public void mo10S() {
        super.mo10S();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Render Asset")
    @C0064Am(aul = "ac763623b5cfaf6129de3dfcf2990907", aum = 0)
    /* renamed from: Nt */
    private Asset m6324Nt() {
        return m6330rh();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Render Asset")
    @C0064Am(aul = "618cc3e9085ef441719cb6f3b1867764", aum = 0)
    /* renamed from: H */
    private void m6321H(Asset tCVar) {
        m6329r(tCVar);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Surface Material")
    @C0064Am(aul = "dfa663f2ce8bf87af8db229c605ce80c", aum = 0)
    /* renamed from: it */
    private String m6328it() {
        return m6327ij();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Surface Material")
    @C0064Am(aul = "af5635bd24cd52929c3d1a2698640451", aum = 0)
    /* renamed from: M */
    private void m6323M(String str) {
        m6322H(str);
    }

    @C0064Am(aul = "be9cdf4410771acfb3d3bfcb1e936d61", aum = 0)
    private Advertise bdi() {
        return (Advertise) mo745aU();
    }

    @C0064Am(aul = "99deb3a71e4afd5f79da10c528a283dd", aum = 0)
    /* renamed from: aT */
    private <T extends aDJ> T m6326aT() {
        T t = (Advertise) bFf().mo6865M(Advertise.class);
        t.mo6497g(this);
        return t;
    }
}
