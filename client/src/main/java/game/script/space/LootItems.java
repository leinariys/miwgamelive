package game.script.space;

import game.network.message.externalizable.aCE;
import game.script.Character;
import game.script.item.Item;
import game.script.item.ItemLocation;
import game.script.nls.NLSLoot;
import game.script.nls.NLSManager;
import logic.baa.C1616Xf;
import logic.baa.C1634Xv;
import logic.baa.C5511aMd;
import logic.baa.C6485anp;
import logic.data.mbean.C5984aeI;
import logic.res.code.C5640aRc;
import logic.res.code.C5663aRz;
import logic.res.code.C6494any;
import logic.res.html.C2491fm;
import p001a.*;

import java.util.Collection;

@C5511aMd
@C6485anp
/* renamed from: a.EZ */
/* compiled from: a */
public class LootItems extends ItemLocation implements C1616Xf {

    /* renamed from: RA */
    public static final C2491fm f492RA = null;

    /* renamed from: RD */
    public static final C2491fm f493RD = null;
    public static final C2491fm _f_dispose_0020_0028_0029V = null;
    public static final int _m_fieldCount = 0;
    public static final C5663aRz[] _m_fields = null;
    public static final int _m_methodCount = 0;
    public static final C2491fm[] _m_methods = null;
    public static final C5663aRz cUH = null;
    public static final C2491fm cUI = null;
    /* renamed from: ku */
    public static final C2491fm f494ku = null;
    public static final long serialVersionUID = 0;
    public static C6494any ___iScriptClass;
    @C0064Am(aul = "0de317cc0a8e6b2af50160eeb2def196", aum = 0)
    private static Loot aZM;

    static {
        m2884V();
    }

    public LootItems() {
        super((C5540aNg) null);
        super.mo10S();
    }

    public LootItems(Loot ael) {
        super((C5540aNg) null);
        super._m_script_init(ael);
    }

    public LootItems(C5540aNg ang) {
        super(ang);
    }

    /* renamed from: V */
    static void m2884V() {
        serialVersionUID = (long) 1;
        _m_fieldCount = ItemLocation._m_fieldCount + 1;
        _m_methodCount = ItemLocation._m_methodCount + 5;
        int i = ItemLocation._m_fieldCount;
        C5663aRz[] arzArr = new C5663aRz[(i + 1)];
        C5663aRz b = C5640aRc.m17844b(LootItems.class, "0de317cc0a8e6b2af50160eeb2def196", i);
        cUH = b;
        arzArr[i] = b;
        int i2 = i + 1;
        _m_fields = arzArr;
        C1634Xv.m11725a((Object[]) ItemLocation._m_fields, (Object[]) _m_fields);
        int i3 = ItemLocation._m_methodCount;
        C2491fm[] fmVarArr = new C2491fm[(i3 + 5)];
        C2491fm a = C4105zY.m41624a(LootItems.class, "b873baa194e6cde6f3a2446ab374d954", i3);
        cUI = a;
        fmVarArr[i3] = a;
        int i4 = i3 + 1;
        C2491fm a2 = C4105zY.m41624a(LootItems.class, "3e70c2af33f43b3462647189f91e7a2f", i4);
        f492RA = a2;
        fmVarArr[i4] = a2;
        int i5 = i4 + 1;
        C2491fm a3 = C4105zY.m41624a(LootItems.class, "7da36d1e59b710d2424296a9830c42e0", i5);
        f493RD = a3;
        fmVarArr[i5] = a3;
        int i6 = i5 + 1;
        C2491fm a4 = C4105zY.m41624a(LootItems.class, "fa4e6f7ba61af5caf42f1739882cdc7c", i6);
        f494ku = a4;
        fmVarArr[i6] = a4;
        int i7 = i6 + 1;
        C2491fm a5 = C4105zY.m41624a(LootItems.class, "ce609f20915ddc8b7e123a1aa9d0e167", i7);
        _f_dispose_0020_0028_0029V = a5;
        fmVarArr[i7] = a5;
        int i8 = i7 + 1;
        _m_methods = fmVarArr;
        C1634Xv.m11725a((Object[]) ItemLocation._m_methods, (Object[]) _m_methods);
        ___iScriptClass = aUO.m18566a(LootItems.class, C5984aeI.class, _m_fields, _m_methods);
    }

    private Loot aPh() {
        return (Loot) bFf().mo5608dq().mo3214p(cUH);
    }

    /* renamed from: d */
    private void m2886d(Loot ael) {
        bFf().mo5608dq().mo3197f(cUH, ael);
    }

    /* renamed from: S */
    public void mo10S() {
        super.mo10S();
    }

    /* renamed from: T */
    public C6494any mo11T() {
        return ___iScriptClass;
    }

    /* renamed from: U */
    public C2491fm[] mo12U() {
        return _m_methods;
    }

    /* renamed from: W */
    public Object mo13W() {
        return new C5984aeI(this);
    }

    /* renamed from: a */
    public Object mo14a(C0495Gr gr) {
        Object[] args = gr.getArgs();
        switch (gr.mo2417hq() - ItemLocation._m_methodCount) {
            case 0:
                return aPi();
            case 1:
                m2885a((Item) args[0], (ItemLocation) args[1]);
                return null;
            case 2:
                m2889m((Item) args[0]);
                return null;
            case 3:
                m2888g((Item) args[0]);
                return null;
            case 4:
                m2887fg();
                return null;
            default:
                return super.mo14a(gr);
        }
    }

    /* renamed from: a */
    public void mo15a(Collection collection, C0495Gr gr) {
        super.mo15a(collection, gr);
    }

    public Loot aPj() {
        switch (bFf().mo6893i(cUI)) {
            case 0:
                return null;
            case 2:
                return (Loot) bFf().mo5606d(new aCE(this, cUI, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, cUI, new Object[0]));
                break;
        }
        return aPi();
    }

    /* access modifiers changed from: protected */
    /* renamed from: b */
    public void mo1887b(Item auq, ItemLocation aag) {
        switch (bFf().mo6893i(f492RA)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f492RA, new Object[]{auq, aag}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f492RA, new Object[]{auq, aag}));
                break;
        }
        m2885a(auq, aag);
    }

    /* renamed from: c */
    public C5663aRz[] mo25c() {
        return _m_fields;
    }

    public void dispose() {
        switch (bFf().mo6893i(_f_dispose_0020_0028_0029V)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, _f_dispose_0020_0028_0029V, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, _f_dispose_0020_0028_0029V, new Object[0]));
                break;
        }
        m2887fg();
    }

    /* access modifiers changed from: protected */
    /* renamed from: h */
    public void mo1889h(Item auq) {
        switch (bFf().mo6893i(f494ku)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f494ku, new Object[]{auq}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f494ku, new Object[]{auq}));
                break;
        }
        m2888g(auq);
    }

    /* access modifiers changed from: protected */
    /* renamed from: n */
    public void mo1890n(Item auq) {
        switch (bFf().mo6893i(f493RD)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f493RD, new Object[]{auq}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f493RD, new Object[]{auq}));
                break;
        }
        m2889m(auq);
    }

    /* renamed from: e */
    public void mo1888e(Loot ael) {
        super.mo10S();
        m2886d(ael);
    }

    @C0064Am(aul = "b873baa194e6cde6f3a2446ab374d954", aum = 0)
    private Loot aPi() {
        return aPh();
    }

    @C0064Am(aul = "3e70c2af33f43b3462647189f91e7a2f", aum = 0)
    /* renamed from: a */
    private void m2885a(Item auq, ItemLocation aag) {
    }

    @C0064Am(aul = "7da36d1e59b710d2424296a9830c42e0", aum = 0)
    /* renamed from: m */
    private void m2889m(Item auq) {
    }

    @C0064Am(aul = "fa4e6f7ba61af5caf42f1739882cdc7c", aum = 0)
    /* renamed from: g */
    private void m2888g(Item auq) {
        if (auq.bNh() != null && !auq.bNh().equals(this)) {
            throw new C2293dh(((NLSLoot) ala().aIY().mo6310c(NLSManager.C1472a.LOOT)).dDl());
        }
    }

    @C0064Am(aul = "ce609f20915ddc8b7e123a1aa9d0e167", aum = 0)
    /* renamed from: fg */
    private void m2887fg() {
        super.dispose();
        Character dtO = aPj().dtO();
        if (dtO != null && dtO.mo12638Rp() != null) {
            dtO.mo12638Rp().mo18212b(this);
        }
    }
}
