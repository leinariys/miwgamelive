package game.script.space;

import com.hoplon.geometry.Vec3f;
import game.network.message.externalizable.aCE;
import game.script.Actor;
import game.script.player.Player;
import game.script.ship.Ship;
import game.script.simulation.Space;
import logic.aaa.C2235dB;
import logic.baa.C1616Xf;
import logic.baa.C1634Xv;
import logic.baa.C5511aMd;
import logic.baa.C6485anp;
import logic.bbb.C6348alI;
import logic.data.mbean.C6373alh;
import logic.res.code.C5640aRc;
import logic.res.code.C5663aRz;
import logic.res.code.C6494any;
import logic.res.html.C2491fm;
import logic.thred.C6339akz;
import p001a.*;

import java.util.Collection;

@C6485anp
@C3122oB(mo20937Uq = C3122oB.C3123a.NOT_REPLICATED)
@C1020Ot(drz = C1020Ot.C1021a.NOT_PERSISTED)
@C5511aMd
@C0909NL
/* renamed from: a.mG */
/* compiled from: a */
public class GateCollider extends Actor implements C1616Xf {
    public static final C2491fm _f_onResurrect_0020_0028_0029V = null;
    public static final int _m_fieldCount = 0;
    public static final C5663aRz[] _m_fields = null;
    public static final int _m_methodCount = 0;
    public static final C2491fm[] _m_methods = null;
    public static final C5663aRz aDA = null;
    public static final C2491fm aDB = null;
    public static final C2491fm aDC = null;
    public static final long serialVersionUID = 0;
    /* renamed from: uX */
    public static final C2491fm f8631uX = null;
    /* renamed from: uY */
    public static final C2491fm f8632uY = null;
    /* renamed from: vc */
    public static final C2491fm f8633vc = null;
    /* renamed from: ve */
    public static final C2491fm f8634ve = null;
    /* renamed from: vi */
    public static final C2491fm f8635vi = null;
    /* renamed from: vm */
    public static final C2491fm f8636vm = null;
    /* renamed from: vn */
    public static final C2491fm f8637vn = null;
    /* renamed from: vo */
    public static final C2491fm f8638vo = null;
    /* renamed from: vs */
    public static final C2491fm f8639vs = null;
    public static C6494any ___iScriptClass;
    @C0064Am(aul = "94ca1ba68f958a162481498c4533beeb", aum = 0)

    /* renamed from: BN */
    private static Gate f8630BN;

    static {
        m35439V();
    }

    public GateCollider() {
        super((C5540aNg) null);
        super.mo10S();
    }

    public GateCollider(C5540aNg ang) {
        super(ang);
    }

    /* renamed from: V */
    static void m35439V() {
        serialVersionUID = (long) 1;
        _m_fieldCount = Actor._m_fieldCount + 1;
        _m_methodCount = Actor._m_methodCount + 12;
        int i = Actor._m_fieldCount;
        C5663aRz[] arzArr = new C5663aRz[(i + 1)];
        C5663aRz b = C5640aRc.m17844b(GateCollider.class, "94ca1ba68f958a162481498c4533beeb", i);
        aDA = b;
        arzArr[i] = b;
        int i2 = i + 1;
        _m_fields = arzArr;
        C1634Xv.m11725a((Object[]) Actor._m_fields, (Object[]) _m_fields);
        int i3 = Actor._m_methodCount;
        C2491fm[] fmVarArr = new C2491fm[(i3 + 12)];
        C2491fm a = C4105zY.m41624a(GateCollider.class, "2e739fa9bde3f3615277802c174fba07", i3);
        f8633vc = a;
        fmVarArr[i3] = a;
        int i4 = i3 + 1;
        C2491fm a2 = C4105zY.m41624a(GateCollider.class, "93a2601435fafe7cb9c464a0b9f2fdc8", i4);
        f8634ve = a2;
        fmVarArr[i4] = a2;
        int i5 = i4 + 1;
        C2491fm a3 = C4105zY.m41624a(GateCollider.class, "7b73fa3c39d629f37c6ca6fdc33ed6af", i5);
        f8635vi = a3;
        fmVarArr[i5] = a3;
        int i6 = i5 + 1;
        C2491fm a4 = C4105zY.m41624a(GateCollider.class, "fa946046b8f3ec59978322fa5ef89492", i6);
        f8631uX = a4;
        fmVarArr[i6] = a4;
        int i7 = i6 + 1;
        C2491fm a5 = C4105zY.m41624a(GateCollider.class, "eee100a379999253466239ce9f7ee4c6", i7);
        aDB = a5;
        fmVarArr[i7] = a5;
        int i8 = i7 + 1;
        C2491fm a6 = C4105zY.m41624a(GateCollider.class, "aa32815067d180b5488a83152591fcbd", i8);
        f8639vs = a6;
        fmVarArr[i8] = a6;
        int i9 = i8 + 1;
        C2491fm a7 = C4105zY.m41624a(GateCollider.class, "693081cb3fb1af868ab64e3d82cdc93d", i9);
        f8632uY = a7;
        fmVarArr[i9] = a7;
        int i10 = i9 + 1;
        C2491fm a8 = C4105zY.m41624a(GateCollider.class, "6db4dde04fc1d395bdfb0a5b663b1681", i10);
        f8636vm = a8;
        fmVarArr[i10] = a8;
        int i11 = i10 + 1;
        C2491fm a9 = C4105zY.m41624a(GateCollider.class, "8a12c63d7deb3c8018887be7cf889e49", i11);
        f8637vn = a9;
        fmVarArr[i11] = a9;
        int i12 = i11 + 1;
        C2491fm a10 = C4105zY.m41624a(GateCollider.class, "3a175c011cf8ac60e1c0e4d3fdd4def7", i12);
        f8638vo = a10;
        fmVarArr[i12] = a10;
        int i13 = i12 + 1;
        C2491fm a11 = C4105zY.m41624a(GateCollider.class, "4e2a34618253da3aef559311aabecb14", i13);
        _f_onResurrect_0020_0028_0029V = a11;
        fmVarArr[i13] = a11;
        int i14 = i13 + 1;
        C2491fm a12 = C4105zY.m41624a(GateCollider.class, "e095b91e1a1c9864be1a58274f0973f3", i14);
        aDC = a12;
        fmVarArr[i14] = a12;
        int i15 = i14 + 1;
        _m_methods = fmVarArr;
        C1634Xv.m11725a((Object[]) Actor._m_methods, (Object[]) _m_methods);
        ___iScriptClass = aUO.m18566a(GateCollider.class, C6373alh.class, _m_fields, _m_methods);
    }

    /* renamed from: NA */
    private Gate m35437NA() {
        return (Gate) bFf().mo5608dq().mo3214p(aDA);
    }

    /* renamed from: d */
    private void m35446d(Gate fFVar) {
        bFf().mo5608dq().mo3197f(aDA, fFVar);
    }

    /* renamed from: T */
    public C6494any mo11T() {
        return ___iScriptClass;
    }

    /* renamed from: U */
    public C2491fm[] mo12U() {
        return _m_methods;
    }

    /* renamed from: W */
    public Object mo13W() {
        return new C6373alh(this);
    }

    /* renamed from: a */
    public Object mo14a(C0495Gr gr) {
        Object[] args = gr.getArgs();
        switch (gr.mo2417hq() - Actor._m_methodCount) {
            case 0:
                return m35448io();
            case 1:
                return m35449iq();
            case 2:
                return m35450it();
            case 3:
                m35442a((Actor) args[0], (Vec3f) args[1], (Vec3f) args[2], ((Float) args[3]).floatValue(), (C6339akz) args[4]);
                return null;
            case 4:
                m35447e((Gate) args[0]);
                return null;
            case 5:
                m35440a((Actor.C0200h) args[0]);
                return null;
            case 6:
                return m35444c((Space) args[0], ((Long) args[1]).longValue());
            case 7:
                m35441a((Actor) args[0], (C5260aCm) args[1], (Vec3f) args[2], (Vec3f) args[3]);
                return null;
            case 8:
                m35445c((Actor) args[0], (C5260aCm) args[1], (Vec3f) args[2], (Vec3f) args[3]);
                return null;
            case 9:
                return m35451iz();
            case 10:
                m35443aG();
                return null;
            case 11:
                return m35438NB();
            default:
                return super.mo14a(gr);
        }
    }

    /* renamed from: a */
    public void mo15a(Collection collection, C0495Gr gr) {
        super.mo15a(collection, gr);
    }

    /* renamed from: aH */
    public void mo70aH() {
        switch (bFf().mo6893i(_f_onResurrect_0020_0028_0029V)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, _f_onResurrect_0020_0028_0029V, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, _f_onResurrect_0020_0028_0029V, new Object[0]));
                break;
        }
        m35443aG();
    }

    /* renamed from: b */
    public void mo620b(Actor.C0200h hVar) {
        switch (bFf().mo6893i(f8639vs)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f8639vs, new Object[]{hVar}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f8639vs, new Object[]{hVar}));
                break;
        }
        m35440a(hVar);
    }

    /* renamed from: b */
    public void mo621b(Actor cr, C5260aCm acm, Vec3f vec3f, Vec3f vec3f2) {
        switch (bFf().mo6893i(f8636vm)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f8636vm, new Object[]{cr, acm, vec3f, vec3f2}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f8636vm, new Object[]{cr, acm, vec3f, vec3f2}));
                break;
        }
        m35441a(cr, acm, vec3f, vec3f2);
    }

    /* renamed from: b */
    public void mo992b(Actor cr, Vec3f vec3f, Vec3f vec3f2, float f, C6339akz akz) {
        switch (bFf().mo6893i(f8631uX)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f8631uX, new Object[]{cr, vec3f, vec3f2, new Float(f), akz}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f8631uX, new Object[]{cr, vec3f, vec3f2, new Float(f), akz}));
                break;
        }
        m35442a(cr, vec3f, vec3f2, f, akz);
    }

    /* renamed from: c */
    public C5663aRz[] mo25c() {
        return _m_fields;
    }

    /* access modifiers changed from: protected */
    @C2198cg
    @C1253SX
    /* renamed from: d */
    public C0520HN mo635d(Space ea, long j) {
        switch (bFf().mo6893i(f8632uY)) {
            case 0:
                return null;
            case 2:
                return (C0520HN) bFf().mo5606d(new aCE(this, f8632uY, new Object[]{ea, new Long(j)}));
            case 3:
                bFf().mo5606d(new aCE(this, f8632uY, new Object[]{ea, new Long(j)}));
                break;
        }
        return m35444c(ea, j);
    }

    /* renamed from: d */
    public void mo636d(Actor cr, C5260aCm acm, Vec3f vec3f, Vec3f vec3f2) {
        switch (bFf().mo6893i(f8637vn)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f8637vn, new Object[]{cr, acm, vec3f, vec3f2}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f8637vn, new Object[]{cr, acm, vec3f, vec3f2}));
                break;
        }
        m35445c(cr, acm, vec3f, vec3f2);
    }

    /* renamed from: f */
    public void mo20458f(Gate fFVar) {
        switch (bFf().mo6893i(aDB)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, aDB, new Object[]{fFVar}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, aDB, new Object[]{fFVar}));
                break;
        }
        m35447e(fFVar);
    }

    /* renamed from: iA */
    public String mo647iA() {
        switch (bFf().mo6893i(f8638vo)) {
            case 0:
                return null;
            case 2:
                return (String) bFf().mo5606d(new aCE(this, f8638vo, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f8638vo, new Object[0]));
                break;
        }
        return m35451iz();
    }

    /* renamed from: ip */
    public String mo648ip() {
        switch (bFf().mo6893i(f8633vc)) {
            case 0:
                return null;
            case 2:
                return (String) bFf().mo5606d(new aCE(this, f8633vc, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f8633vc, new Object[0]));
                break;
        }
        return m35448io();
    }

    /* renamed from: ir */
    public String mo649ir() {
        switch (bFf().mo6893i(f8634ve)) {
            case 0:
                return null;
            case 2:
                return (String) bFf().mo5606d(new aCE(this, f8634ve, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f8634ve, new Object[0]));
                break;
        }
        return m35449iq();
    }

    /* renamed from: iu */
    public String mo651iu() {
        switch (bFf().mo6893i(f8635vi)) {
            case 0:
                return null;
            case 2:
                return (String) bFf().mo5606d(new aCE(this, f8635vi, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f8635vi, new Object[0]));
                break;
        }
        return m35450it();
    }

    /* renamed from: lb */
    public Gate mo20459lb() {
        switch (bFf().mo6893i(aDC)) {
            case 0:
                return null;
            case 2:
                return (Gate) bFf().mo5606d(new aCE(this, aDC, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, aDC, new Object[0]));
                break;
        }
        return m35438NB();
    }

    /* renamed from: S */
    public void mo10S() {
        super.mo10S();
    }

    @C0064Am(aul = "2e739fa9bde3f3615277802c174fba07", aum = 0)
    /* renamed from: io */
    private String m35448io() {
        return null;
    }

    @C0064Am(aul = "93a2601435fafe7cb9c464a0b9f2fdc8", aum = 0)
    /* renamed from: iq */
    private String m35449iq() {
        return null;
    }

    @C0064Am(aul = "7b73fa3c39d629f37c6ca6fdc33ed6af", aum = 0)
    /* renamed from: it */
    private String m35450it() {
        return null;
    }

    @C0064Am(aul = "fa946046b8f3ec59978322fa5ef89492", aum = 0)
    /* renamed from: a */
    private void m35442a(Actor cr, Vec3f vec3f, Vec3f vec3f2, float f, C6339akz akz) {
        super.mo992b(cr, vec3f, vec3f2, f, akz);
        if (ala().aLQ().des() && (cr instanceof Ship)) {
            Ship fAVar = (Ship) cr;
            if (fAVar.ahb()) {
                fAVar.mo18312da(cVr());
            }
            if ((fAVar.agj() instanceof Player) && !fAVar.ahb()) {
                fAVar.mo18302b((C0286Dh) m35437NA());
            }
        }
    }

    @C0064Am(aul = "eee100a379999253466239ce9f7ee4c6", aum = 0)
    /* renamed from: e */
    private void m35447e(Gate fFVar) {
        m35446d(fFVar);
    }

    @C0064Am(aul = "aa32815067d180b5488a83152591fcbd", aum = 0)
    /* renamed from: a */
    private void m35440a(Actor.C0200h hVar) {
        hVar.mo1103c(new C6348alI(1.0f));
    }

    @C0064Am(aul = "693081cb3fb1af868ab64e3d82cdc93d", aum = 0)
    @C2198cg
    @C1253SX
    /* renamed from: c */
    private C0520HN m35444c(Space ea, long j) {
        C1627Xp xp = new C1627Xp(ea, this, mo958IL());
        xp.mo2449a(ea.cKn().mo5725a(j, (C2235dB) xp, 13));
        return xp;
    }

    @C0064Am(aul = "6db4dde04fc1d395bdfb0a5b663b1681", aum = 0)
    /* renamed from: a */
    private void m35441a(Actor cr, C5260aCm acm, Vec3f vec3f, Vec3f vec3f2) {
    }

    @C0064Am(aul = "8a12c63d7deb3c8018887be7cf889e49", aum = 0)
    /* renamed from: c */
    private void m35445c(Actor cr, C5260aCm acm, Vec3f vec3f, Vec3f vec3f2) {
    }

    @C0064Am(aul = "3a175c011cf8ac60e1c0e4d3fdd4def7", aum = 0)
    /* renamed from: iz */
    private String m35451iz() {
        return null;
    }

    @C0064Am(aul = "4e2a34618253da3aef559311aabecb14", aum = 0)
    /* renamed from: aG */
    private void m35443aG() {
        super.mo70aH();
        dispose();
    }

    @C0064Am(aul = "e095b91e1a1c9864be1a58274f0973f3", aum = 0)
    /* renamed from: NB */
    private Gate m35438NB() {
        return m35437NA();
    }
}
