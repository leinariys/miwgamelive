package game.script.space;

import game.CollisionFXSet;
import game.network.message.externalizable.aCE;
import game.network.message.serializable.C3438ra;
import game.script.itemgen.ItemGenTable;
import game.script.resource.Asset;
import game.script.ship.HullType;
import logic.baa.*;
import logic.data.link.C2911li;
import logic.data.mbean.C5232aBk;
import logic.res.code.C5640aRc;
import logic.res.code.C5663aRz;
import logic.res.code.C6494any;
import logic.res.html.C2491fm;
import p001a.*;
import taikodom.infra.script.I18NString;

import java.util.Collection;
import java.util.Collections;
import java.util.List;

@C0566Hp
@C5511aMd
@C6485anp
/* renamed from: a.aQn  reason: case insensitive filesystem */
/* compiled from: a */
public class AsteroidType extends BaseAsteroidType implements C0405Fe, C1616Xf, C6251ajP {
    /* renamed from: LR */
    public static final C5663aRz f3653LR = null;
    /* renamed from: VW */
    public static final C5663aRz f3654VW = null;
    /* renamed from: Wc */
    public static final C5663aRz f3656Wc = null;
    /* renamed from: We */
    public static final C5663aRz f3658We = null;
    public static final int _m_fieldCount = 0;
    public static final C5663aRz[] _m_fields = null;
    public static final int _m_methodCount = 0;
    public static final C2491fm[] _m_methods = null;
    public static final C2491fm aDR = null;
    public static final C2491fm aDk = null;
    public static final C2491fm aDl = null;
    public static final C2491fm aTr = null;
    public static final C5663aRz bnF = null;
    public static final C5663aRz bnN = null;
    public static final C5663aRz bod = null;
    public static final C2491fm dGK = null;
    public static final C2491fm dGL = null;
    public static final C2491fm dGQ = null;
    public static final C2491fm dGR = null;
    public static final C2491fm dGS = null;
    public static final C2491fm dGT = null;
    public static final C2491fm dHh = null;
    public static final C2491fm dHi = null;
    public static final C2491fm dHz = null;
    /* renamed from: dN */
    public static final C2491fm f3659dN = null;
    public static final C2491fm eLi = null;
    public static final C5663aRz fmC = null;
    public static final C2491fm fmV = null;
    public static final C2491fm fmW = null;
    public static final C2491fm fmX = null;
    public static final C2491fm fnb = null;
    public static final C2491fm iEs = null;
    public static final C2491fm iEt = null;
    public static final C2491fm iEu = null;
    public static final long serialVersionUID = 0;
    /* renamed from: uU */
    public static final C5663aRz f3661uU = null;
    /* renamed from: vi */
    public static final C2491fm f3662vi = null;
    /* renamed from: vj */
    public static final C2491fm f3663vj = null;
    /* renamed from: zQ */
    public static final C5663aRz f3665zQ = null;
    /* renamed from: zT */
    public static final C2491fm f3666zT = null;
    /* renamed from: zU */
    public static final C2491fm f3667zU = null;
    public static C6494any ___iScriptClass;
    @C0064Am(aul = "d7624973e208c02dc7bb51cd5177d30d", aum = 6)

    /* renamed from: LQ */
    private static Asset f3652LQ;
    @C0064Am(aul = "c379172a9f6945e852e2548776a7bec4", aum = 1)

    /* renamed from: Wb */
    private static Asset f3655Wb;
    @C0064Am(aul = "d40bc9383dda8e6a43c80e0d2fa443ed", aum = 2)

    /* renamed from: Wd */
    private static Asset f3657Wd;
    @C0064Am(aul = "441efaf17b25894a2f45a7d5015dc694", aum = 3)
    private static C3438ra<ItemGenTable> ayZ;
    @C0064Am(aul = "914f8c7ac5f00bbde3fda4f2d6eb6a6f", aum = 9)
    private static HullType bhn;
    @C0064Am(aul = "fda858dfb99f6fc724fa2c37cb8028ac", aum = 0)
    private static CollisionFXSet bnE;
    @C0064Am(aul = "a30f7cc66b3106cb5837e081cbb28b43", aum = 7)
    private static Asset boc;
    @C0064Am(aul = "c8e5ddc60dbe8f48ae4a8a7f30c644b8", aum = 4)
    private static AsteroidLootType hhb;
    @C0064Am(aul = "425fb267c8b706d9b97fe565cd4febac", aum = 8)

    /* renamed from: uT */
    private static String f3660uT;
    @C0064Am(aul = "c808e77f1ae8def9baaaa5e9775ad919", aum = 5)

    /* renamed from: zP */
    private static I18NString f3664zP;

    static {
        m17651V();
    }

    public AsteroidType() {
        super((C5540aNg) null);
        super.mo10S();
    }

    public AsteroidType(C5540aNg ang) {
        super(ang);
    }

    /* renamed from: V */
    static void m17651V() {
        serialVersionUID = (long) 1;
        _m_fieldCount = BaseAsteroidType._m_fieldCount + 10;
        _m_methodCount = BaseAsteroidType._m_methodCount + 26;
        int i = BaseAsteroidType._m_fieldCount;
        C5663aRz[] arzArr = new C5663aRz[(i + 10)];
        C5663aRz b = C5640aRc.m17844b(AsteroidType.class, "fda858dfb99f6fc724fa2c37cb8028ac", i);
        bnF = b;
        arzArr[i] = b;
        int i2 = i + 1;
        C5663aRz b2 = C5640aRc.m17844b(AsteroidType.class, "c379172a9f6945e852e2548776a7bec4", i2);
        f3656Wc = b2;
        arzArr[i2] = b2;
        int i3 = i2 + 1;
        C5663aRz b3 = C5640aRc.m17844b(AsteroidType.class, "d40bc9383dda8e6a43c80e0d2fa443ed", i3);
        f3658We = b3;
        arzArr[i3] = b3;
        int i4 = i3 + 1;
        C5663aRz b4 = C5640aRc.m17844b(AsteroidType.class, "441efaf17b25894a2f45a7d5015dc694", i4);
        fmC = b4;
        arzArr[i4] = b4;
        int i5 = i4 + 1;
        C5663aRz b5 = C5640aRc.m17844b(AsteroidType.class, "c8e5ddc60dbe8f48ae4a8a7f30c644b8", i5);
        bnN = b5;
        arzArr[i5] = b5;
        int i6 = i5 + 1;
        C5663aRz b6 = C5640aRc.m17844b(AsteroidType.class, "c808e77f1ae8def9baaaa5e9775ad919", i6);
        f3665zQ = b6;
        arzArr[i6] = b6;
        int i7 = i6 + 1;
        C5663aRz b7 = C5640aRc.m17844b(AsteroidType.class, "d7624973e208c02dc7bb51cd5177d30d", i7);
        f3653LR = b7;
        arzArr[i7] = b7;
        int i8 = i7 + 1;
        C5663aRz b8 = C5640aRc.m17844b(AsteroidType.class, "a30f7cc66b3106cb5837e081cbb28b43", i8);
        bod = b8;
        arzArr[i8] = b8;
        int i9 = i8 + 1;
        C5663aRz b9 = C5640aRc.m17844b(AsteroidType.class, "425fb267c8b706d9b97fe565cd4febac", i9);
        f3661uU = b9;
        arzArr[i9] = b9;
        int i10 = i9 + 1;
        C5663aRz b10 = C5640aRc.m17844b(AsteroidType.class, "914f8c7ac5f00bbde3fda4f2d6eb6a6f", i10);
        f3654VW = b10;
        arzArr[i10] = b10;
        int i11 = i10 + 1;
        _m_fields = arzArr;
        C1634Xv.m11725a((Object[]) BaseAsteroidType._m_fields, (Object[]) _m_fields);
        int i12 = BaseAsteroidType._m_methodCount;
        C2491fm[] fmVarArr = new C2491fm[(i12 + 26)];
        C2491fm a = C4105zY.m41624a(AsteroidType.class, "85e652a11a4c125ba739bf3c084714b0", i12);
        dGK = a;
        fmVarArr[i12] = a;
        int i13 = i12 + 1;
        C2491fm a2 = C4105zY.m41624a(AsteroidType.class, "61f7abd8a5fb014e223fd0042338b7ea", i13);
        dGL = a2;
        fmVarArr[i13] = a2;
        int i14 = i13 + 1;
        C2491fm a3 = C4105zY.m41624a(AsteroidType.class, "ee98451bced64a9a8a5c42d3a9d927a4", i14);
        dGQ = a3;
        fmVarArr[i14] = a3;
        int i15 = i14 + 1;
        C2491fm a4 = C4105zY.m41624a(AsteroidType.class, "0c0165e3ee42120d6bf21d514254fabe", i15);
        dGR = a4;
        fmVarArr[i15] = a4;
        int i16 = i15 + 1;
        C2491fm a5 = C4105zY.m41624a(AsteroidType.class, "0ce86c6cfa09f08253bc2fbef9208130", i16);
        dGS = a5;
        fmVarArr[i16] = a5;
        int i17 = i16 + 1;
        C2491fm a6 = C4105zY.m41624a(AsteroidType.class, "98842c18c4217611faa9dd6780365ff3", i17);
        dGT = a6;
        fmVarArr[i17] = a6;
        int i18 = i17 + 1;
        C2491fm a7 = C4105zY.m41624a(AsteroidType.class, "de4c065cd21449b58337be6b106f5a4b", i18);
        eLi = a7;
        fmVarArr[i18] = a7;
        int i19 = i18 + 1;
        C2491fm a8 = C4105zY.m41624a(AsteroidType.class, "a6be5ce4b8460c86a7ff2c9e4d8e97bb", i19);
        fmV = a8;
        fmVarArr[i19] = a8;
        int i20 = i19 + 1;
        C2491fm a9 = C4105zY.m41624a(AsteroidType.class, "8ef535f408ab07627f9b5edd0bcdf633", i20);
        fmW = a9;
        fmVarArr[i20] = a9;
        int i21 = i20 + 1;
        C2491fm a10 = C4105zY.m41624a(AsteroidType.class, "51a6e7625ded3076f505f5049ad69f55", i21);
        fmX = a10;
        fmVarArr[i21] = a10;
        int i22 = i21 + 1;
        C2491fm a11 = C4105zY.m41624a(AsteroidType.class, "49a70e15c29168d938cf51574aaec082", i22);
        iEs = a11;
        fmVarArr[i22] = a11;
        int i23 = i22 + 1;
        C2491fm a12 = C4105zY.m41624a(AsteroidType.class, "36a647e9d11cadd463f4f3062e1f72a5", i23);
        iEt = a12;
        fmVarArr[i23] = a12;
        int i24 = i23 + 1;
        C2491fm a13 = C4105zY.m41624a(AsteroidType.class, "793e2cc553450bad5ca44b3db59311e6", i24);
        f3666zT = a13;
        fmVarArr[i24] = a13;
        int i25 = i24 + 1;
        C2491fm a14 = C4105zY.m41624a(AsteroidType.class, "f1825403ebba4c11f25f51515add6345", i25);
        f3667zU = a14;
        fmVarArr[i25] = a14;
        int i26 = i25 + 1;
        C2491fm a15 = C4105zY.m41624a(AsteroidType.class, "7ea500f1b935909437dde0c3aca280ab", i26);
        aDk = a15;
        fmVarArr[i26] = a15;
        int i27 = i26 + 1;
        C2491fm a16 = C4105zY.m41624a(AsteroidType.class, "9eb20e445493016a8d4688c3062d0740", i27);
        aDl = a16;
        fmVarArr[i27] = a16;
        int i28 = i27 + 1;
        C2491fm a17 = C4105zY.m41624a(AsteroidType.class, "5d8ab88da957149718e1ad7079e182d1", i28);
        dHh = a17;
        fmVarArr[i28] = a17;
        int i29 = i28 + 1;
        C2491fm a18 = C4105zY.m41624a(AsteroidType.class, "f0d97cb80d57788cfe46657392b3cba2", i29);
        dHi = a18;
        fmVarArr[i29] = a18;
        int i30 = i29 + 1;
        C2491fm a19 = C4105zY.m41624a(AsteroidType.class, "018d42e429eb6926536e42ed975cdd82", i30);
        f3662vi = a19;
        fmVarArr[i30] = a19;
        int i31 = i30 + 1;
        C2491fm a20 = C4105zY.m41624a(AsteroidType.class, "19f5a22c1c36dd034ac4dea467ee9cf0", i31);
        f3663vj = a20;
        fmVarArr[i31] = a20;
        int i32 = i31 + 1;
        C2491fm a21 = C4105zY.m41624a(AsteroidType.class, "f39558ae89265b522183437df669ccb3", i32);
        aTr = a21;
        fmVarArr[i32] = a21;
        int i33 = i32 + 1;
        C2491fm a22 = C4105zY.m41624a(AsteroidType.class, "8020e4664db053aea52a85a00f60712d", i33);
        dHz = a22;
        fmVarArr[i33] = a22;
        int i34 = i33 + 1;
        C2491fm a23 = C4105zY.m41624a(AsteroidType.class, "b6f548f1ec2f6b1878dabcd4d96c9105", i34);
        aDR = a23;
        fmVarArr[i34] = a23;
        int i35 = i34 + 1;
        C2491fm a24 = C4105zY.m41624a(AsteroidType.class, "61744a73f0798b5a867497745877f49d", i35);
        iEu = a24;
        fmVarArr[i35] = a24;
        int i36 = i35 + 1;
        C2491fm a25 = C4105zY.m41624a(AsteroidType.class, "baedb08a8e08632d790bb85b7f9246cf", i36);
        f3659dN = a25;
        fmVarArr[i36] = a25;
        int i37 = i36 + 1;
        C2491fm a26 = C4105zY.m41624a(AsteroidType.class, "b3c661a2dd0d399bce6cc49d04fbbfc2", i37);
        fnb = a26;
        fmVarArr[i37] = a26;
        int i38 = i37 + 1;
        _m_methods = fmVarArr;
        C1634Xv.m11725a((Object[]) BaseAsteroidType._m_methods, (Object[]) _m_methods);
        ___iScriptClass = aUO.m18566a(AsteroidType.class, C5232aBk.class, _m_fields, _m_methods);
    }

    /* renamed from: A */
    private void m17646A(Asset tCVar) {
        bFf().mo5608dq().mo3197f(f3658We, tCVar);
    }

    /* renamed from: H */
    private void m17648H(String str) {
        bFf().mo5608dq().mo3197f(f3661uU, str);
    }

    /* renamed from: W */
    private void m17653W(Asset tCVar) {
        bFf().mo5608dq().mo3197f(bod, tCVar);
    }

    /* renamed from: a */
    private void m17655a(CollisionFXSet yaVar) {
        bFf().mo5608dq().mo3197f(bnF, yaVar);
    }

    private CollisionFXSet aeJ() {
        return (CollisionFXSet) bFf().mo5608dq().mo3214p(bnF);
    }

    private Asset aeX() {
        return (Asset) bFf().mo5608dq().mo3214p(bod);
    }

    /* renamed from: b */
    private void m17660b(AsteroidLootType agm) {
        bFf().mo5608dq().mo3197f(bnN, agm);
    }

    /* renamed from: bL */
    private void m17661bL(C3438ra raVar) {
        bFf().mo5608dq().mo3197f(fmC, raVar);
    }

    private C3438ra bTz() {
        return (C3438ra) bFf().mo5608dq().mo3214p(fmC);
    }

    @C0064Am(aul = "b3c661a2dd0d399bce6cc49d04fbbfc2", aum = 0)
    private List bUb() {
        return bTU();
    }

    private HullType bjZ() {
        return (HullType) bFf().mo5608dq().mo3214p(f3654VW);
    }

    /* renamed from: d */
    private void m17664d(I18NString i18NString) {
        bFf().mo5608dq().mo3197f(f3665zQ, i18NString);
    }

    private AsteroidLootType ddb() {
        return (AsteroidLootType) bFf().mo5608dq().mo3214p(bnN);
    }

    /* renamed from: f */
    private void m17667f(HullType wHVar) {
        bFf().mo5608dq().mo3197f(f3654VW, wHVar);
    }

    /* renamed from: ij */
    private String m17670ij() {
        return (String) bFf().mo5608dq().mo3214p(f3661uU);
    }

    /* renamed from: kb */
    private I18NString m17672kb() {
        return (I18NString) bFf().mo5608dq().mo3214p(f3665zQ);
    }

    /* renamed from: r */
    private void m17674r(Asset tCVar) {
        bFf().mo5608dq().mo3197f(f3653LR, tCVar);
    }

    /* renamed from: rh */
    private Asset m17675rh() {
        return (Asset) bFf().mo5608dq().mo3214p(f3653LR);
    }

    /* renamed from: z */
    private void m17676z(Asset tCVar) {
        bFf().mo5608dq().mo3197f(f3656Wc, tCVar);
    }

    /* renamed from: zl */
    private Asset m17677zl() {
        return (Asset) bFf().mo5608dq().mo3214p(f3656Wc);
    }

    /* renamed from: zm */
    private Asset m17678zm() {
        return (Asset) bFf().mo5608dq().mo3214p(f3658We);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Render Asset")
    /* renamed from: I */
    public void mo11069I(Asset tCVar) {
        switch (bFf().mo6893i(aDl)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, aDl, new Object[]{tCVar}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, aDl, new Object[]{tCVar}));
                break;
        }
        m17647H(tCVar);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Surface Material")
    /* renamed from: N */
    public void mo11070N(String str) {
        switch (bFf().mo6893i(f3663vj)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f3663vj, new Object[]{str}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f3663vj, new Object[]{str}));
                break;
        }
        m17649M(str);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Render Asset")
    /* renamed from: Nu */
    public Asset mo3521Nu() {
        switch (bFf().mo6893i(aDk)) {
            case 0:
                return null;
            case 2:
                return (Asset) bFf().mo5606d(new aCE(this, aDk, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, aDk, new Object[0]));
                break;
        }
        return m17650Nt();
    }

    /* renamed from: T */
    public C6494any mo11T() {
        return ___iScriptClass;
    }

    /* renamed from: U */
    public C2491fm[] mo12U() {
        return _m_methods;
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Hull")
    /* renamed from: VT */
    public HullType mo11071VT() {
        switch (bFf().mo6893i(aTr)) {
            case 0:
                return null;
            case 2:
                return (HullType) bFf().mo5606d(new aCE(this, aTr, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, aTr, new Object[0]));
                break;
        }
        return m17652VS();
    }

    /* renamed from: W */
    public Object mo13W() {
        return new C5232aBk(this);
    }

    /* renamed from: a */
    public Object mo14a(C0495Gr gr) {
        Object[] args = gr.getArgs();
        switch (gr.mo2417hq() - BaseAsteroidType._m_methodCount) {
            case 0:
                return bka();
            case 1:
                m17668f((CollisionFXSet) args[0]);
                return null;
            case 2:
                return bkg();
            case 3:
                m17656aR((Asset) args[0]);
                return null;
            case 4:
                return bki();
            case 5:
                m17658aT((Asset) args[0]);
                return null;
            case 6:
                m17654a((ItemGenTable) args[0]);
                return null;
            case 7:
                m17663c((ItemGenTable) args[0]);
                return null;
            case 8:
                return bTT();
            case 9:
                return bTV();
            case 10:
                return dpK();
            case 11:
                m17662c((AsteroidLootType) args[0]);
                return null;
            case 12:
                return m17673kd();
            case 13:
                m17666e((I18NString) args[0]);
                return null;
            case 14:
                return m17650Nt();
            case 15:
                m17647H((Asset) args[0]);
                return null;
            case 16:
                return bkw();
            case 17:
                m17659aV((Asset) args[0]);
                return null;
            case 18:
                return m17671it();
            case 19:
                m17649M((String) args[0]);
                return null;
            case 20:
                return m17652VS();
            case 21:
                m17669g((HullType) args[0]);
                return null;
            case 22:
                m17665e((aDJ) args[0]);
                return null;
            case 23:
                return dpM();
            case 24:
                return m17657aT();
            case 25:
                return bUb();
            default:
                return super.mo14a(gr);
        }
    }

    /* renamed from: a */
    public void mo15a(Collection collection, C0495Gr gr) {
        super.mo15a(collection, gr);
    }

    public /* bridge */ /* synthetic */ List aPl() {
        switch (bFf().mo6893i(fnb)) {
            case 0:
                return null;
            case 2:
                return (List) bFf().mo5606d(new aCE(this, fnb, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, fnb, new Object[0]));
                break;
        }
        return bUb();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Explosion G F X")
    /* renamed from: aS */
    public void mo11072aS(Asset tCVar) {
        switch (bFf().mo6893i(dGR)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, dGR, new Object[]{tCVar}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, dGR, new Object[]{tCVar}));
                break;
        }
        m17656aR(tCVar);
    }

    /* access modifiers changed from: protected */
    /* renamed from: aU */
    public <T extends aDJ> T mo745aU() {
        switch (bFf().mo6893i(f3659dN)) {
            case 0:
                return null;
            case 2:
                return (aDJ) bFf().mo5606d(new aCE(this, f3659dN, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f3659dN, new Object[0]));
                break;
        }
        return m17657aT();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Explosion S F X")
    /* renamed from: aU */
    public void mo11073aU(Asset tCVar) {
        switch (bFf().mo6893i(dGT)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, dGT, new Object[]{tCVar}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, dGT, new Object[]{tCVar}));
                break;
        }
        m17658aT(tCVar);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Physical Asset")
    /* renamed from: aW */
    public void mo11074aW(Asset tCVar) {
        switch (bFf().mo6893i(dHi)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, dHi, new Object[]{tCVar}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, dHi, new Object[]{tCVar}));
                break;
        }
        m17659aV(tCVar);
    }

    @C3248pc(aYR = C3248pc.C3250b.ADDER, displayName = "Item Gen Tables")
    /* renamed from: b */
    public void mo7248b(ItemGenTable oqVar) {
        switch (bFf().mo6893i(eLi)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, eLi, new Object[]{oqVar}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, eLi, new Object[]{oqVar}));
                break;
        }
        m17654a(oqVar);
    }

    public C3438ra<ItemGenTable> bTU() {
        switch (bFf().mo6893i(fmW)) {
            case 0:
                return null;
            case 2:
                return (C3438ra) bFf().mo5606d(new aCE(this, fmW, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, fmW, new Object[0]));
                break;
        }
        return bTT();
    }

    @C3248pc(aYR = C3248pc.C3250b.ITERATOR, displayName = "Item Gen Tables")
    public List<ItemGenTable> bTW() {
        switch (bFf().mo6893i(fmX)) {
            case 0:
                return null;
            case 2:
                return (List) bFf().mo5606d(new aCE(this, fmX, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, fmX, new Object[0]));
                break;
        }
        return bTV();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Collisions FX")
    public CollisionFXSet bkb() {
        switch (bFf().mo6893i(dGK)) {
            case 0:
                return null;
            case 2:
                return (CollisionFXSet) bFf().mo5606d(new aCE(this, dGK, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, dGK, new Object[0]));
                break;
        }
        return bka();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Explosion G F X")
    public Asset bkh() {
        switch (bFf().mo6893i(dGQ)) {
            case 0:
                return null;
            case 2:
                return (Asset) bFf().mo5606d(new aCE(this, dGQ, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, dGQ, new Object[0]));
                break;
        }
        return bkg();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Explosion S F X")
    public Asset bkj() {
        switch (bFf().mo6893i(dGS)) {
            case 0:
                return null;
            case 2:
                return (Asset) bFf().mo5606d(new aCE(this, dGS, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, dGS, new Object[0]));
                break;
        }
        return bki();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Physical Asset")
    public Asset bkx() {
        switch (bFf().mo6893i(dHh)) {
            case 0:
                return null;
            case 2:
                return (Asset) bFf().mo5606d(new aCE(this, dHh, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, dHh, new Object[0]));
                break;
        }
        return bkw();
    }

    /* renamed from: c */
    public C5663aRz[] mo25c() {
        return _m_fields;
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Loot Type")
    /* renamed from: d */
    public void mo11081d(AsteroidLootType agm) {
        switch (bFf().mo6893i(iEt)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, iEt, new Object[]{agm}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, iEt, new Object[]{agm}));
                break;
        }
        m17662c(agm);
    }

    @C3248pc(aYR = C3248pc.C3250b.REMOVER, displayName = "Item Gen Tables")
    /* renamed from: d */
    public void mo11082d(ItemGenTable oqVar) {
        switch (bFf().mo6893i(fmV)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, fmV, new Object[]{oqVar}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, fmV, new Object[]{oqVar}));
                break;
        }
        m17663c(oqVar);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Loot Type")
    public AsteroidLootType dpL() {
        switch (bFf().mo6893i(iEs)) {
            case 0:
                return null;
            case 2:
                return (AsteroidLootType) bFf().mo5606d(new aCE(this, iEs, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, iEs, new Object[0]));
                break;
        }
        return dpK();
    }

    public Asteroid dpN() {
        switch (bFf().mo6893i(iEu)) {
            case 0:
                return null;
            case 2:
                return (Asteroid) bFf().mo5606d(new aCE(this, iEu, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, iEu, new Object[0]));
                break;
        }
        return dpM();
    }

    /* renamed from: f */
    public void mo2631f(aDJ adj) {
        switch (bFf().mo6893i(aDR)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, aDR, new Object[]{adj}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, aDR, new Object[]{adj}));
                break;
        }
        m17665e(adj);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Name")
    /* renamed from: f */
    public void mo11085f(I18NString i18NString) {
        switch (bFf().mo6893i(f3667zU)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f3667zU, new Object[]{i18NString}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f3667zU, new Object[]{i18NString}));
                break;
        }
        m17666e(i18NString);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Collisions FX")
    /* renamed from: g */
    public void mo11086g(CollisionFXSet yaVar) {
        switch (bFf().mo6893i(dGL)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, dGL, new Object[]{yaVar}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, dGL, new Object[]{yaVar}));
                break;
        }
        m17668f(yaVar);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Hull")
    /* renamed from: h */
    public void mo11087h(HullType wHVar) {
        switch (bFf().mo6893i(dHz)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, dHz, new Object[]{wHVar}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, dHz, new Object[]{wHVar}));
                break;
        }
        m17669g(wHVar);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Surface Material")
    /* renamed from: iu */
    public String mo11088iu() {
        switch (bFf().mo6893i(f3662vi)) {
            case 0:
                return null;
            case 2:
                return (String) bFf().mo5606d(new aCE(this, f3662vi, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f3662vi, new Object[0]));
                break;
        }
        return m17671it();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Name")
    /* renamed from: ke */
    public I18NString mo11089ke() {
        switch (bFf().mo6893i(f3666zT)) {
            case 0:
                return null;
            case 2:
                return (I18NString) bFf().mo5606d(new aCE(this, f3666zT, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f3666zT, new Object[0]));
                break;
        }
        return m17673kd();
    }

    /* renamed from: S */
    public void mo10S() {
        super.mo10S();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Collisions FX")
    @C0064Am(aul = "85e652a11a4c125ba739bf3c084714b0", aum = 0)
    private CollisionFXSet bka() {
        return aeJ();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Collisions FX")
    @C0064Am(aul = "61f7abd8a5fb014e223fd0042338b7ea", aum = 0)
    /* renamed from: f */
    private void m17668f(CollisionFXSet yaVar) {
        m17655a(yaVar);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Explosion G F X")
    @C0064Am(aul = "ee98451bced64a9a8a5c42d3a9d927a4", aum = 0)
    private Asset bkg() {
        return m17677zl();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Explosion G F X")
    @C0064Am(aul = "0c0165e3ee42120d6bf21d514254fabe", aum = 0)
    /* renamed from: aR */
    private void m17656aR(Asset tCVar) {
        m17676z(tCVar);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Explosion S F X")
    @C0064Am(aul = "0ce86c6cfa09f08253bc2fbef9208130", aum = 0)
    private Asset bki() {
        return m17678zm();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Explosion S F X")
    @C0064Am(aul = "98842c18c4217611faa9dd6780365ff3", aum = 0)
    /* renamed from: aT */
    private void m17658aT(Asset tCVar) {
        m17646A(tCVar);
    }

    @C3248pc(aYR = C3248pc.C3250b.ADDER, displayName = "Item Gen Tables")
    @C0064Am(aul = "de4c065cd21449b58337be6b106f5a4b", aum = 0)
    /* renamed from: a */
    private void m17654a(ItemGenTable oqVar) {
        bTz().add(oqVar);
    }

    @C3248pc(aYR = C3248pc.C3250b.REMOVER, displayName = "Item Gen Tables")
    @C0064Am(aul = "a6be5ce4b8460c86a7ff2c9e4d8e97bb", aum = 0)
    /* renamed from: c */
    private void m17663c(ItemGenTable oqVar) {
        bTz().remove(oqVar);
    }

    @C0064Am(aul = "8ef535f408ab07627f9b5edd0bcdf633", aum = 0)
    private C3438ra<ItemGenTable> bTT() {
        return bTz();
    }

    @C3248pc(aYR = C3248pc.C3250b.ITERATOR, displayName = "Item Gen Tables")
    @C0064Am(aul = "51a6e7625ded3076f505f5049ad69f55", aum = 0)
    private List<ItemGenTable> bTV() {
        return Collections.unmodifiableList(bTz());
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Loot Type")
    @C0064Am(aul = "49a70e15c29168d938cf51574aaec082", aum = 0)
    private AsteroidLootType dpK() {
        return ddb();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Loot Type")
    @C0064Am(aul = "36a647e9d11cadd463f4f3062e1f72a5", aum = 0)
    /* renamed from: c */
    private void m17662c(AsteroidLootType agm) {
        m17660b(agm);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Name")
    @C0064Am(aul = "793e2cc553450bad5ca44b3db59311e6", aum = 0)
    /* renamed from: kd */
    private I18NString m17673kd() {
        return m17672kb();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Name")
    @C0064Am(aul = "f1825403ebba4c11f25f51515add6345", aum = 0)
    /* renamed from: e */
    private void m17666e(I18NString i18NString) {
        m17664d(i18NString);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Render Asset")
    @C0064Am(aul = "7ea500f1b935909437dde0c3aca280ab", aum = 0)
    /* renamed from: Nt */
    private Asset m17650Nt() {
        return m17675rh();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Render Asset")
    @C0064Am(aul = "9eb20e445493016a8d4688c3062d0740", aum = 0)
    /* renamed from: H */
    private void m17647H(Asset tCVar) {
        m17674r(tCVar);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Physical Asset")
    @C0064Am(aul = "5d8ab88da957149718e1ad7079e182d1", aum = 0)
    private Asset bkw() {
        return aeX();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Physical Asset")
    @C0064Am(aul = "f0d97cb80d57788cfe46657392b3cba2", aum = 0)
    /* renamed from: aV */
    private void m17659aV(Asset tCVar) {
        m17653W(tCVar);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Surface Material")
    @C0064Am(aul = "018d42e429eb6926536e42ed975cdd82", aum = 0)
    /* renamed from: it */
    private String m17671it() {
        return m17670ij();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Surface Material")
    @C0064Am(aul = "19f5a22c1c36dd034ac4dea467ee9cf0", aum = 0)
    /* renamed from: M */
    private void m17649M(String str) {
        m17648H(str);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Hull")
    @C0064Am(aul = "f39558ae89265b522183437df669ccb3", aum = 0)
    /* renamed from: VS */
    private HullType m17652VS() {
        return bjZ();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Hull")
    @C0064Am(aul = "8020e4664db053aea52a85a00f60712d", aum = 0)
    /* renamed from: g */
    private void m17669g(HullType wHVar) {
        m17667f(wHVar);
    }

    @C0064Am(aul = "b6f548f1ec2f6b1878dabcd4d96c9105", aum = 0)
    /* renamed from: e */
    private void m17665e(aDJ adj) {
        super.mo2631f(adj);
        if (adj instanceof Asteroid) {
            C1616Xf xf = adj;
            if (bjZ() != null) {
                xf.mo6765g(C2911li.aMH, bjZ().mo7459NK());
            }
        }
    }

    @C0064Am(aul = "61744a73f0798b5a867497745877f49d", aum = 0)
    private Asteroid dpM() {
        return (Asteroid) mo745aU();
    }

    @C0064Am(aul = "baedb08a8e08632d790bb85b7f9246cf", aum = 0)
    /* renamed from: aT */
    private <T extends aDJ> T m17657aT() {
        T t = (Asteroid) bFf().mo6865M(Asteroid.class);
        t.mo8653o(this);
        return t;
    }
}
