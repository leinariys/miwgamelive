package game.script.space;

import com.hoplon.geometry.Vec3f;
import game.network.message.externalizable.aCE;
import game.script.Actor;
import game.script.Character;
import logic.baa.C1616Xf;
import logic.baa.C1634Xv;
import logic.baa.C5511aMd;
import logic.baa.C6485anp;
import logic.bbb.C6348alI;
import logic.data.mbean.C6634aqi;
import logic.res.code.C5663aRz;
import logic.res.code.C6494any;
import logic.res.html.C2491fm;
import p001a.*;

import java.util.Collection;

@C5511aMd
@C6485anp
@ClientOnly
/* renamed from: a.aAJ */
/* compiled from: a */
public class NodeAnchor extends Actor implements C0286Dh, C1616Xf {

    /* renamed from: MH */
    public static final C2491fm f2304MH = null;

    /* renamed from: MM */
    public static final C2491fm f2305MM = null;
    public static final int _m_fieldCount = 0;
    public static final C5663aRz[] _m_fields = null;
    public static final int _m_methodCount = 0;
    public static final C2491fm[] _m_methods = null;
    public static final long serialVersionUID = 0;
    /* renamed from: vc */
    public static final C2491fm f2306vc = null;
    /* renamed from: ve */
    public static final C2491fm f2307ve = null;
    /* renamed from: vi */
    public static final C2491fm f2308vi = null;
    /* renamed from: vm */
    public static final C2491fm f2309vm = null;
    /* renamed from: vn */
    public static final C2491fm f2310vn = null;
    /* renamed from: vo */
    public static final C2491fm f2311vo = null;
    /* renamed from: vs */
    public static final C2491fm f2312vs = null;
    public static C6494any ___iScriptClass;

    static {
        m12513V();
    }

    public NodeAnchor() {
        super((C5540aNg) null);
        super.mo10S();
    }

    public NodeAnchor(C5540aNg ang) {
        super(ang);
    }

    /* renamed from: V */
    static void m12513V() {
        serialVersionUID = (long) 1;
        _m_fieldCount = Actor._m_fieldCount + 0;
        _m_methodCount = Actor._m_methodCount + 9;
        _m_fields = new C5663aRz[(Actor._m_fieldCount + 0)];
        C1634Xv.m11725a((Object[]) Actor._m_fields, (Object[]) _m_fields);
        int i = Actor._m_methodCount;
        C2491fm[] fmVarArr = new C2491fm[(i + 9)];
        C2491fm a = C4105zY.m41624a(NodeAnchor.class, "fddac1dde04d036fb62c863106705325", i);
        f2309vm = a;
        fmVarArr[i] = a;
        int i2 = i + 1;
        C2491fm a2 = C4105zY.m41624a(NodeAnchor.class, "2c18c07ea18135a4b7d6fab8ab3618c3", i2);
        f2312vs = a2;
        fmVarArr[i2] = a2;
        int i3 = i2 + 1;
        C2491fm a3 = C4105zY.m41624a(NodeAnchor.class, "a384af3c25f8dcf20882f1f28f5a9da6", i3);
        f2310vn = a3;
        fmVarArr[i3] = a3;
        int i4 = i3 + 1;
        C2491fm a4 = C4105zY.m41624a(NodeAnchor.class, "e93eec1397a9af5289538f454b8ca8c2", i4);
        f2306vc = a4;
        fmVarArr[i4] = a4;
        int i5 = i4 + 1;
        C2491fm a5 = C4105zY.m41624a(NodeAnchor.class, "b8ac9e1470f6dfecbf33ae5059ca562e", i5);
        f2307ve = a5;
        fmVarArr[i5] = a5;
        int i6 = i5 + 1;
        C2491fm a6 = C4105zY.m41624a(NodeAnchor.class, "d3c4e1190c980905c269c26932ca584d", i6);
        f2311vo = a6;
        fmVarArr[i6] = a6;
        int i7 = i6 + 1;
        C2491fm a7 = C4105zY.m41624a(NodeAnchor.class, "aa10de8ed4405309c2df7733a8643bb1", i7);
        f2308vi = a7;
        fmVarArr[i7] = a7;
        int i8 = i7 + 1;
        C2491fm a8 = C4105zY.m41624a(NodeAnchor.class, "ba7738a2c4102b461b5605222556e5fc", i8);
        f2304MH = a8;
        fmVarArr[i8] = a8;
        int i9 = i8 + 1;
        C2491fm a9 = C4105zY.m41624a(NodeAnchor.class, "41b96896cd5c47ff3826af3e5361819f", i9);
        f2305MM = a9;
        fmVarArr[i9] = a9;
        int i10 = i9 + 1;
        _m_methods = fmVarArr;
        C1634Xv.m11725a((Object[]) Actor._m_methods, (Object[]) _m_methods);
        ___iScriptClass = aUO.m18566a(NodeAnchor.class, C6634aqi.class, _m_fields, _m_methods);
    }

    /* renamed from: T */
    public C6494any mo11T() {
        return ___iScriptClass;
    }

    /* renamed from: U */
    public C2491fm[] mo12U() {
        return _m_methods;
    }

    /* renamed from: W */
    public Object mo13W() {
        return new C6634aqi(this);
    }

    /* renamed from: Y */
    public boolean mo599Y(float f) {
        switch (bFf().mo6893i(f2305MM)) {
            case 0:
                return false;
            case 2:
                return ((Boolean) bFf().mo5606d(new aCE(this, f2305MM, new Object[]{new Float(f)}))).booleanValue();
            case 3:
                bFf().mo5606d(new aCE(this, f2305MM, new Object[]{new Float(f)}));
                break;
        }
        return m12514X(f);
    }

    /* renamed from: a */
    public Object mo14a(C0495Gr gr) {
        Object[] args = gr.getArgs();
        switch (gr.mo2417hq() - Actor._m_methodCount) {
            case 0:
                m12516a((Actor) args[0], (C5260aCm) args[1], (Vec3f) args[2], (Vec3f) args[3]);
                return null;
            case 1:
                m12515a((Actor.C0200h) args[0]);
                return null;
            case 2:
                m12518c((Actor) args[0], (C5260aCm) args[1], (Vec3f) args[2], (Vec3f) args[3]);
                return null;
            case 3:
                return m12519io();
            case 4:
                return m12520iq();
            case 5:
                return m12522iz();
            case 6:
                return m12521it();
            case 7:
                return m12517c((Character) args[0]);
            case 8:
                return new Boolean(m12514X(((Float) args[0]).floatValue()));
            default:
                return super.mo14a(gr);
        }
    }

    /* renamed from: a */
    public void mo15a(Collection collection, C0495Gr gr) {
        super.mo15a(collection, gr);
    }

    /* renamed from: b */
    public void mo620b(Actor.C0200h hVar) {
        switch (bFf().mo6893i(f2312vs)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f2312vs, new Object[]{hVar}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f2312vs, new Object[]{hVar}));
                break;
        }
        m12515a(hVar);
    }

    /* renamed from: b */
    public void mo621b(Actor cr, C5260aCm acm, Vec3f vec3f, Vec3f vec3f2) {
        switch (bFf().mo6893i(f2309vm)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f2309vm, new Object[]{cr, acm, vec3f, vec3f2}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f2309vm, new Object[]{cr, acm, vec3f, vec3f2}));
                break;
        }
        m12516a(cr, acm, vec3f, vec3f2);
    }

    /* renamed from: c */
    public C5663aRz[] mo25c() {
        return _m_fields;
    }

    /* renamed from: d */
    public C0286Dh.C0287a mo634d(Character acx) {
        switch (bFf().mo6893i(f2304MH)) {
            case 0:
                return null;
            case 2:
                return (C0286Dh.C0287a) bFf().mo5606d(new aCE(this, f2304MH, new Object[]{acx}));
            case 3:
                bFf().mo5606d(new aCE(this, f2304MH, new Object[]{acx}));
                break;
        }
        return m12517c(acx);
    }

    /* renamed from: d */
    public void mo636d(Actor cr, C5260aCm acm, Vec3f vec3f, Vec3f vec3f2) {
        switch (bFf().mo6893i(f2310vn)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f2310vn, new Object[]{cr, acm, vec3f, vec3f2}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f2310vn, new Object[]{cr, acm, vec3f, vec3f2}));
                break;
        }
        m12518c(cr, acm, vec3f, vec3f2);
    }

    /* renamed from: iA */
    public String mo647iA() {
        switch (bFf().mo6893i(f2311vo)) {
            case 0:
                return null;
            case 2:
                return (String) bFf().mo5606d(new aCE(this, f2311vo, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f2311vo, new Object[0]));
                break;
        }
        return m12522iz();
    }

    /* renamed from: ip */
    public String mo648ip() {
        switch (bFf().mo6893i(f2306vc)) {
            case 0:
                return null;
            case 2:
                return (String) bFf().mo5606d(new aCE(this, f2306vc, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f2306vc, new Object[0]));
                break;
        }
        return m12519io();
    }

    /* renamed from: ir */
    public String mo649ir() {
        switch (bFf().mo6893i(f2307ve)) {
            case 0:
                return null;
            case 2:
                return (String) bFf().mo5606d(new aCE(this, f2307ve, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f2307ve, new Object[0]));
                break;
        }
        return m12520iq();
    }

    /* renamed from: iu */
    public String mo651iu() {
        switch (bFf().mo6893i(f2308vi)) {
            case 0:
                return null;
            case 2:
                return (String) bFf().mo5606d(new aCE(this, f2308vi, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f2308vi, new Object[0]));
                break;
        }
        return m12521it();
    }

    /* renamed from: S */
    public void mo10S() {
        super.mo10S();
    }

    @C0064Am(aul = "fddac1dde04d036fb62c863106705325", aum = 0)
    /* renamed from: a */
    private void m12516a(Actor cr, C5260aCm acm, Vec3f vec3f, Vec3f vec3f2) {
    }

    @C0064Am(aul = "2c18c07ea18135a4b7d6fab8ab3618c3", aum = 0)
    /* renamed from: a */
    private void m12515a(Actor.C0200h hVar) {
        hVar.mo1103c(new C6348alI(1.0f));
    }

    @C0064Am(aul = "a384af3c25f8dcf20882f1f28f5a9da6", aum = 0)
    /* renamed from: c */
    private void m12518c(Actor cr, C5260aCm acm, Vec3f vec3f, Vec3f vec3f2) {
    }

    @C0064Am(aul = "e93eec1397a9af5289538f454b8ca8c2", aum = 0)
    /* renamed from: io */
    private String m12519io() {
        return "dummy_group";
    }

    @C0064Am(aul = "b8ac9e1470f6dfecbf33ae5059ca562e", aum = 0)
    /* renamed from: iq */
    private String m12520iq() {
        return "data/dummy_data.pro";
    }

    @C0064Am(aul = "d3c4e1190c980905c269c26932ca584d", aum = 0)
    /* renamed from: iz */
    private String m12522iz() {
        return ala().aIW().avE();
    }

    @C0064Am(aul = "aa10de8ed4405309c2df7733a8643bb1", aum = 0)
    /* renamed from: it */
    private String m12521it() {
        return null;
    }

    @C0064Am(aul = "ba7738a2c4102b461b5605222556e5fc", aum = 0)
    /* renamed from: c */
    private C0286Dh.C0287a m12517c(Character acx) {
        return C0286Dh.C0287a.OK;
    }

    @C0064Am(aul = "41b96896cd5c47ff3826af3e5361819f", aum = 0)
    /* renamed from: X */
    private boolean m12514X(float f) {
        return false;
    }
}
