package game.script.space;

import com.hoplon.geometry.Vec3f;
import game.geometry.Vec3d;
import game.network.message.externalizable.aCE;
import game.network.message.serializable.C1556Wo;
import game.network.message.serializable.C2686iZ;
import game.network.message.serializable.C3438ra;
import game.script.Actor;
import game.script.TaikodomObject;
import game.script.citizenship.inprovements.NodeAccessImprovementType;
import game.script.hazardarea.HazardArea.HazardArea;
import game.script.hazardarea.HazardArea.HazardAreaType;
import game.script.pda.DatabaseCategory;
import game.script.pda.TaikopediaEntry;
import game.script.player.Player;
import game.script.resource.Asset;
import game.script.ship.*;
import game.script.simulation.Space;
import game.script.spacezone.*;
import logic.baa.*;
import logic.bbb.aDR;
import logic.data.mbean.C0721KN;
import logic.data.mbean.C0883Mo;
import logic.res.code.C5640aRc;
import logic.res.code.C5663aRz;
import logic.res.code.C6494any;
import logic.res.html.C2491fm;
import p001a.*;
import taikodom.infra.script.I18NString;

import java.io.PrintWriter;
import java.util.*;

@C5829abJ("1.1.1")
@C6485anp
@C5511aMd
/* renamed from: a.rP */
/* compiled from: a */
public class Node extends TaikodomObject implements C0468GU, C1616Xf, aOW, C6222aim, C4068yr {

    /* renamed from: Do */
    public static final C2491fm f8982Do = null;
    /* renamed from: LR */
    public static final C5663aRz f8984LR = null;
    /* renamed from: MI */
    public static final C2491fm f8985MI = null;
    /* renamed from: MJ */
    public static final C2491fm f8986MJ = null;
    /* renamed from: MN */
    public static final C2491fm f8987MN = null;
    /* renamed from: Mn */
    public static final C5663aRz f8989Mn = null;
    /* renamed from: NY */
    public static final C5663aRz f8990NY = null;
    /* renamed from: Ob */
    public static final C2491fm f8991Ob = null;
    /* renamed from: Oc */
    public static final C2491fm f8992Oc = null;
    /* renamed from: QA */
    public static final C5663aRz f8993QA = null;
    /* renamed from: QE */
    public static final C2491fm f8994QE = null;
    /* renamed from: QF */
    public static final C2491fm f8995QF = null;
    /* renamed from: _f_getOffloaders_0020_0028_0029Ljava_002futil_002fCollection_003b */
    public static final C2491fm f8996x99c43db3 = null;
    public static final C2491fm _f_push_0020_0028_0029V = null;
    public static final C2491fm _f_setRadius_0020_0028F_0029V = null;
    public static final C2491fm _f_toString_0020_0028_0029Ljava_002flang_002fString_003b = null;
    public static final int _m_fieldCount = 0;
    public static final C5663aRz[] _m_fields = null;
    public static final int _m_methodCount = 0;
    public static final C2491fm[] _m_methods = null;
    public static final C5663aRz aBo = null;
    public static final C2491fm aBs = null;
    public static final C2491fm aBt = null;
    public static final C2491fm aDk = null;
    public static final C2491fm aDl = null;
    public static final C2491fm aKm = null;
    public static final C5663aRz aMg = null;
    public static final C5663aRz aMh = null;
    public static final C5663aRz aMi = null;
    public static final C2491fm aMj = null;
    public static final C2491fm aMk = null;
    public static final C2491fm aMl = null;
    public static final C2491fm aMn = null;
    public static final C2491fm aMo = null;
    public static final C2491fm aMp = null;
    public static final C2491fm aMq = null;
    public static final C2491fm aMr = null;
    public static final C2491fm aMs = null;
    public static final C2491fm aMu = null;
    public static final C2491fm aMv = null;
    public static final C5663aRz awd = null;
    public static final C5663aRz awh = null;
    /* renamed from: bL */
    public static final C5663aRz f8998bL = null;
    /* renamed from: bM */
    public static final C5663aRz f8999bM = null;
    /* renamed from: bN */
    public static final C2491fm f9000bN = null;
    /* renamed from: bO */
    public static final C2491fm f9001bO = null;
    /* renamed from: bP */
    public static final C2491fm f9002bP = null;
    /* renamed from: bQ */
    public static final C2491fm f9003bQ = null;
    public static final C2491fm bcA = null;
    public static final C2491fm bcB = null;
    public static final C2491fm bcC = null;
    public static final C2491fm bcD = null;
    public static final C2491fm bcE = null;
    public static final C2491fm bcF = null;
    public static final C2491fm bcG = null;
    public static final C2491fm bcH = null;
    public static final C2491fm bcI = null;
    public static final C2491fm bcJ = null;
    public static final C2491fm bcK = null;
    public static final C2491fm bcL = null;
    public static final C2491fm bcM = null;
    public static final C2491fm bcN = null;
    public static final C2491fm bcO = null;
    public static final C2491fm bcP = null;
    public static final C2491fm bcQ = null;
    public static final C2491fm bcR = null;
    public static final C2491fm bcS = null;
    public static final C2491fm bcT = null;
    public static final C2491fm bcU = null;
    public static final C2491fm bcV = null;
    public static final C2491fm bcW = null;
    public static final C2491fm bcX = null;
    public static final C2491fm bcY = null;
    public static final C2491fm bcZ = null;
    public static final C5663aRz bcr = null;
    public static final C5663aRz bct = null;
    public static final C5663aRz bcu = null;
    public static final C5663aRz bcv = null;
    public static final C5663aRz bcx = null;
    public static final C5663aRz bcz = null;
    public static final C2491fm bdA = null;
    public static final C2491fm bdB = null;
    public static final C2491fm bdC = null;
    public static final C2491fm bdD = null;
    public static final C2491fm bdE = null;
    public static final C2491fm bdF = null;
    public static final C2491fm bdG = null;
    public static final C2491fm bdH = null;
    public static final C2491fm bdI = null;
    public static final C2491fm bdJ = null;
    public static final C2491fm bdK = null;
    public static final C2491fm bdL = null;
    public static final C2491fm bdM = null;
    public static final C2491fm bdN = null;
    public static final C2491fm bdO = null;
    public static final C2491fm bdP = null;
    public static final C2491fm bdQ = null;
    public static final C2491fm bdR = null;
    public static final C2491fm bdS = null;
    public static final C2491fm bda = null;
    public static final C2491fm bdb = null;
    public static final C2491fm bdc = null;
    public static final C2491fm bdd = null;
    public static final C2491fm bde = null;
    public static final C2491fm bdf = null;
    public static final C2491fm bdg = null;
    public static final C2491fm bdh = null;
    public static final C2491fm bdi = null;
    public static final C2491fm bdj = null;
    public static final C2491fm bdk = null;
    public static final C2491fm bdl = null;
    public static final C2491fm bdm = null;
    public static final C2491fm bdn = null;
    public static final C2491fm bdo = null;
    public static final C2491fm bdp = null;
    public static final C2491fm bdq = null;
    public static final C2491fm bdr = null;
    public static final C2491fm bds = null;
    public static final C2491fm bdt = null;
    public static final C2491fm bdu = null;
    public static final C2491fm bdv = null;
    public static final C2491fm bdw = null;
    public static final C2491fm bdx = null;
    public static final C2491fm bdy = null;
    public static final C2491fm bdz = null;
    public static final long serialVersionUID = 0;
    /* renamed from: vc */
    public static final C2491fm f9012vc = null;
    /* renamed from: ve */
    public static final C2491fm f9013ve = null;
    /* renamed from: vp */
    public static final C2491fm f9014vp = null;
    /* renamed from: zQ */
    public static final C5663aRz f9016zQ = null;
    /* renamed from: zT */
    public static final C2491fm f9017zT = null;
    /* renamed from: zU */
    public static final C2491fm f9018zU = null;
    public static C6494any ___iScriptClass;
    @C0064Am(aul = "71eb97b9014a8df581887168abf21476", aum = 3)

    /* renamed from: LQ */
    private static Asset f8983LQ;
    @C0064Am(aul = "566519299ab0912e69d41e5b0f7f1d5e", aum = 11)

    /* renamed from: Mm */
    private static NodeAccessImprovementType f8988Mm;
    @C0064Am(aul = "ce7817e3357c3d341d2ec5f5bb7ddd74", aum = 18)

    /* renamed from: bK */
    private static UUID f8997bK;
    @C0064Am(aul = "a672013f6598ad8423bf0f91ec091f79", aum = 2)
    private static float bcq;
    @C0064Am(aul = "d604552d5a410766eed30ee6e1d14a9a", aum = 6)
    private static boolean bcs;
    @C0064Am(aul = "b406685a072cf114e051149f4bf865bc", aum = 12)
    private static C1556Wo<C3423a, NodeObjectHolder> bcw;
    @C0064Am(aul = "cebedec59cb54b7d50b3f16d003f49d5", aum = 17)
    private static boolean bcy;
    @C0064Am(aul = "d5ce9d4447f6174ee09cbca95104b906", aum = 0)
    private static String handle;
    @C0064Am(aul = "bcf450d1d903002ba9c691e6bd5c3b22", aum = 14)

    /* renamed from: jS */
    private static DatabaseCategory f9004jS;
    @C0064Am(aul = "a2e790ab1af38ca4928c33cfc52b644b", aum = 13)

    /* renamed from: jT */
    private static C3438ra<TaikopediaEntry> f9005jT;
    @C0064Am(aul = "63e2c56e516900a3385ac684402f8745", aum = 5)

    /* renamed from: jU */
    private static Asset f9006jU;
    @C0064Am(aul = "076a3aab6f8a4f1da2c293683e8089f7", aum = 16)

    /* renamed from: jV */
    private static float f9007jV;
    @C0064Am(aul = "dac22c8ad486d6ea76a334cfaa0997d2", aum = 4)

    /* renamed from: jn */
    private static Asset f9008jn;
    @C0064Am(aul = "a8fe27e9559caab9b64bed9338473bc0", aum = 15)

    /* renamed from: nh */
    private static I18NString f9009nh;
    @C0064Am(aul = "2283e2439084f9cb2fe60c15ec4ca468", aum = 7)
    private static Vec3d position;
    @C0064Am(aul = "93319c2c0276eca4b73a11769d895aa9", aum = 9)

    /* renamed from: rI */
    private static StellarSystem f9010rI;
    @C0064Am(aul = "36804dfe16fa1a1187577cec45839991", aum = 10)

    /* renamed from: rL */
    private static Space f9011rL;
    @C0064Am(aul = "d80ce90825228b373b9a88ffe328038a", aum = 8)
    private static float radius;
    @C0064Am(aul = "5c22a3a88c762c070fd38bab16d42265", aum = 1)

    /* renamed from: zP */
    private static I18NString f9015zP;

    static {
        m38115V();
    }

    public Node() {
        super((C5540aNg) null);
        super.mo10S();
    }

    public Node(C5540aNg ang) {
        super(ang);
    }

    /* renamed from: V */
    static void m38115V() {
        serialVersionUID = (long) 1;
        _m_fieldCount = TaikodomObject._m_fieldCount + 19;
        _m_methodCount = TaikodomObject._m_methodCount + 108;
        int i = TaikodomObject._m_fieldCount;
        C5663aRz[] arzArr = new C5663aRz[(i + 19)];
        C5663aRz b = C5640aRc.m17844b(Node.class, "d5ce9d4447f6174ee09cbca95104b906", i);
        f8999bM = b;
        arzArr[i] = b;
        int i2 = i + 1;
        C5663aRz b2 = C5640aRc.m17844b(Node.class, "5c22a3a88c762c070fd38bab16d42265", i2);
        f9016zQ = b2;
        arzArr[i2] = b2;
        int i3 = i2 + 1;
        C5663aRz b3 = C5640aRc.m17844b(Node.class, "a672013f6598ad8423bf0f91ec091f79", i3);
        bcr = b3;
        arzArr[i3] = b3;
        int i4 = i3 + 1;
        C5663aRz b4 = C5640aRc.m17844b(Node.class, "71eb97b9014a8df581887168abf21476", i4);
        f8984LR = b4;
        arzArr[i4] = b4;
        int i5 = i4 + 1;
        C5663aRz b5 = C5640aRc.m17844b(Node.class, "dac22c8ad486d6ea76a334cfaa0997d2", i5);
        f8990NY = b5;
        arzArr[i5] = b5;
        int i6 = i5 + 1;
        C5663aRz b6 = C5640aRc.m17844b(Node.class, "63e2c56e516900a3385ac684402f8745", i6);
        aMg = b6;
        arzArr[i6] = b6;
        int i7 = i6 + 1;
        C5663aRz b7 = C5640aRc.m17844b(Node.class, "d604552d5a410766eed30ee6e1d14a9a", i7);
        bct = b7;
        arzArr[i7] = b7;
        int i8 = i7 + 1;
        C5663aRz b8 = C5640aRc.m17844b(Node.class, "2283e2439084f9cb2fe60c15ec4ca468", i8);
        bcu = b8;
        arzArr[i8] = b8;
        int i9 = i8 + 1;
        C5663aRz b9 = C5640aRc.m17844b(Node.class, "d80ce90825228b373b9a88ffe328038a", i9);
        awh = b9;
        arzArr[i9] = b9;
        int i10 = i9 + 1;
        C5663aRz b10 = C5640aRc.m17844b(Node.class, "93319c2c0276eca4b73a11769d895aa9", i10);
        aBo = b10;
        arzArr[i10] = b10;
        int i11 = i10 + 1;
        C5663aRz b11 = C5640aRc.m17844b(Node.class, "36804dfe16fa1a1187577cec45839991", i11);
        bcv = b11;
        arzArr[i11] = b11;
        int i12 = i11 + 1;
        C5663aRz b12 = C5640aRc.m17844b(Node.class, "566519299ab0912e69d41e5b0f7f1d5e", i12);
        f8989Mn = b12;
        arzArr[i12] = b12;
        int i13 = i12 + 1;
        C5663aRz b13 = C5640aRc.m17844b(Node.class, "b406685a072cf114e051149f4bf865bc", i13);
        bcx = b13;
        arzArr[i13] = b13;
        int i14 = i13 + 1;
        C5663aRz b14 = C5640aRc.m17844b(Node.class, "a2e790ab1af38ca4928c33cfc52b644b", i14);
        awd = b14;
        arzArr[i14] = b14;
        int i15 = i14 + 1;
        C5663aRz b15 = C5640aRc.m17844b(Node.class, "bcf450d1d903002ba9c691e6bd5c3b22", i15);
        aMh = b15;
        arzArr[i15] = b15;
        int i16 = i15 + 1;
        C5663aRz b16 = C5640aRc.m17844b(Node.class, "a8fe27e9559caab9b64bed9338473bc0", i16);
        f8993QA = b16;
        arzArr[i16] = b16;
        int i17 = i16 + 1;
        C5663aRz b17 = C5640aRc.m17844b(Node.class, "076a3aab6f8a4f1da2c293683e8089f7", i17);
        aMi = b17;
        arzArr[i17] = b17;
        int i18 = i17 + 1;
        C5663aRz b18 = C5640aRc.m17844b(Node.class, "cebedec59cb54b7d50b3f16d003f49d5", i18);
        bcz = b18;
        arzArr[i18] = b18;
        int i19 = i18 + 1;
        C5663aRz b19 = C5640aRc.m17844b(Node.class, "ce7817e3357c3d341d2ec5f5bb7ddd74", i19);
        f8998bL = b19;
        arzArr[i19] = b19;
        int i20 = i19 + 1;
        _m_fields = arzArr;
        C1634Xv.m11725a((Object[]) TaikodomObject._m_fields, (Object[]) _m_fields);
        int i21 = TaikodomObject._m_methodCount;
        C2491fm[] fmVarArr = new C2491fm[(i21 + 108)];
        C2491fm a = C4105zY.m41624a(Node.class, "beb7c98ae1d4ce9f7f68944bd2ea0ed2", i21);
        _f_toString_0020_0028_0029Ljava_002flang_002fString_003b = a;
        fmVarArr[i21] = a;
        int i22 = i21 + 1;
        C2491fm a2 = C4105zY.m41624a(Node.class, "49aaba70febe04515599534a3150b753", i22);
        f9000bN = a2;
        fmVarArr[i22] = a2;
        int i23 = i22 + 1;
        C2491fm a3 = C4105zY.m41624a(Node.class, "c1b293ea6180f3f0e313773b0ef99138", i23);
        f9001bO = a3;
        fmVarArr[i23] = a3;
        int i24 = i23 + 1;
        C2491fm a4 = C4105zY.m41624a(Node.class, "b55b6f1c88718de06b19748969da961a", i24);
        bcA = a4;
        fmVarArr[i24] = a4;
        int i25 = i24 + 1;
        C2491fm a5 = C4105zY.m41624a(Node.class, "e1bc6f99eb4a4245d4e065afaade7784", i25);
        bcB = a5;
        fmVarArr[i25] = a5;
        int i26 = i25 + 1;
        C2491fm a6 = C4105zY.m41624a(Node.class, "5cf4b44b19a84321a6a91ad33a8663ea", i26);
        f9017zT = a6;
        fmVarArr[i26] = a6;
        int i27 = i26 + 1;
        C2491fm a7 = C4105zY.m41624a(Node.class, "a34fcec5323da6a76094d298490fc31f", i27);
        f9018zU = a7;
        fmVarArr[i27] = a7;
        int i28 = i27 + 1;
        C2491fm a8 = C4105zY.m41624a(Node.class, "6dc39158b529be7a7aa75aec63e2ecbb", i28);
        bcC = a8;
        fmVarArr[i28] = a8;
        int i29 = i28 + 1;
        C2491fm a9 = C4105zY.m41624a(Node.class, "6de69324a0c80fdc352a6aa54b9bf612", i29);
        f9014vp = a9;
        fmVarArr[i29] = a9;
        int i30 = i29 + 1;
        C2491fm a10 = C4105zY.m41624a(Node.class, "32fb451d447df6a3c89dec4daf9be65a", i30);
        bcD = a10;
        fmVarArr[i30] = a10;
        int i31 = i30 + 1;
        C2491fm a11 = C4105zY.m41624a(Node.class, "51b660f02163632d67a62bc885597f88", i31);
        bcE = a11;
        fmVarArr[i31] = a11;
        int i32 = i31 + 1;
        C2491fm a12 = C4105zY.m41624a(Node.class, "f9adc4d8634153e1683b84ebb6ee1446", i32);
        bcF = a12;
        fmVarArr[i32] = a12;
        int i33 = i32 + 1;
        C2491fm a13 = C4105zY.m41624a(Node.class, "bfbc0dbda8ff0e04718863e2af22680a", i33);
        _f_setRadius_0020_0028F_0029V = a13;
        fmVarArr[i33] = a13;
        int i34 = i33 + 1;
        C2491fm a14 = C4105zY.m41624a(Node.class, "d566ea95fa3c490a1556d1ec6d0a3624", i34);
        f9002bP = a14;
        fmVarArr[i34] = a14;
        int i35 = i34 + 1;
        C2491fm a15 = C4105zY.m41624a(Node.class, "7fa20b783cf3d52be98b9b94addc6b37", i35);
        f9003bQ = a15;
        fmVarArr[i35] = a15;
        int i36 = i35 + 1;
        C2491fm a16 = C4105zY.m41624a(Node.class, "1f105cf56ae2f09b7862c7fe77317eb2", i36);
        bcG = a16;
        fmVarArr[i36] = a16;
        int i37 = i36 + 1;
        C2491fm a17 = C4105zY.m41624a(Node.class, "f7f59082dcef16b7fe5887e493722938", i37);
        bcH = a17;
        fmVarArr[i37] = a17;
        int i38 = i37 + 1;
        C2491fm a18 = C4105zY.m41624a(Node.class, "00391d22175761374ba74f034e0e31d7", i38);
        aDk = a18;
        fmVarArr[i38] = a18;
        int i39 = i38 + 1;
        C2491fm a19 = C4105zY.m41624a(Node.class, "323b86bbee351bf1099edeef465e8230", i39);
        aDl = a19;
        fmVarArr[i39] = a19;
        int i40 = i39 + 1;
        C2491fm a20 = C4105zY.m41624a(Node.class, "db2c9e809aab9107972215494692eafe", i40);
        aMl = a20;
        fmVarArr[i40] = a20;
        int i41 = i40 + 1;
        C2491fm a21 = C4105zY.m41624a(Node.class, "0c7a2495bf5936c8362860212a057da6", i41);
        bcI = a21;
        fmVarArr[i41] = a21;
        int i42 = i41 + 1;
        C2491fm a22 = C4105zY.m41624a(Node.class, "ef9dca2ef077e67850b64cbc6e013b4d", i42);
        aMk = a22;
        fmVarArr[i42] = a22;
        int i43 = i42 + 1;
        C2491fm a23 = C4105zY.m41624a(Node.class, "295c157479cee5ebdaf7a906991883a3", i43);
        bcJ = a23;
        fmVarArr[i43] = a23;
        int i44 = i43 + 1;
        C2491fm a24 = C4105zY.m41624a(Node.class, "9014d3f07ea769c47aab271ad7582c03", i44);
        bcK = a24;
        fmVarArr[i44] = a24;
        int i45 = i44 + 1;
        C2491fm a25 = C4105zY.m41624a(Node.class, "4a455a6393c9d764cd5f030141f6c0b7", i45);
        f9012vc = a25;
        fmVarArr[i45] = a25;
        int i46 = i45 + 1;
        C2491fm a26 = C4105zY.m41624a(Node.class, "c3e569dc6807df86c5de8bf45e0ee8c0", i46);
        f9013ve = a26;
        fmVarArr[i46] = a26;
        int i47 = i46 + 1;
        C2491fm a27 = C4105zY.m41624a(Node.class, "b78d3b9a3eafac1b4d673ab5b5311f63", i47);
        aBs = a27;
        fmVarArr[i47] = a27;
        int i48 = i47 + 1;
        C2491fm a28 = C4105zY.m41624a(Node.class, "5699c6e04aa4c4942acdb7af2f6497f1", i48);
        aBt = a28;
        fmVarArr[i48] = a28;
        int i49 = i48 + 1;
        C2491fm a29 = C4105zY.m41624a(Node.class, "34b0f28d123e231475a50c2da9ee261c", i49);
        bcL = a29;
        fmVarArr[i49] = a29;
        int i50 = i49 + 1;
        C2491fm a30 = C4105zY.m41624a(Node.class, "a2a38306665ce77bb79a2fab72dd87f0", i50);
        _f_push_0020_0028_0029V = a30;
        fmVarArr[i50] = a30;
        int i51 = i50 + 1;
        C2491fm a31 = C4105zY.m41624a(Node.class, "eb86e73c31a1e0102ab5d541c8c992e5", i51);
        bcM = a31;
        fmVarArr[i51] = a31;
        int i52 = i51 + 1;
        C2491fm a32 = C4105zY.m41624a(Node.class, "2f031a6b75dfd3d1032dbc77324fca50", i52);
        bcN = a32;
        fmVarArr[i52] = a32;
        int i53 = i52 + 1;
        C2491fm a33 = C4105zY.m41624a(Node.class, "856e41fe20fba5eff07bec39df805d51", i53);
        bcO = a33;
        fmVarArr[i53] = a33;
        int i54 = i53 + 1;
        C2491fm a34 = C4105zY.m41624a(Node.class, "3b52e92ca8ccf6cc7b390176b9f29921", i54);
        bcP = a34;
        fmVarArr[i54] = a34;
        int i55 = i54 + 1;
        C2491fm a35 = C4105zY.m41624a(Node.class, "2c3da941856e4db8911135f5ef5da31c", i55);
        bcQ = a35;
        fmVarArr[i55] = a35;
        int i56 = i55 + 1;
        C2491fm a36 = C4105zY.m41624a(Node.class, "98b673002e76cee67f6e558168c74a52", i56);
        bcR = a36;
        fmVarArr[i56] = a36;
        int i57 = i56 + 1;
        C2491fm a37 = C4105zY.m41624a(Node.class, "1a903e74e6b02b27f070e46cc195c777", i57);
        bcS = a37;
        fmVarArr[i57] = a37;
        int i58 = i57 + 1;
        C2491fm a38 = C4105zY.m41624a(Node.class, "92cc0987c1d89d44c79f8bc55e307da9", i58);
        bcT = a38;
        fmVarArr[i58] = a38;
        int i59 = i58 + 1;
        C2491fm a39 = C4105zY.m41624a(Node.class, "d0c946d8d5741da9823c8904ba43284b", i59);
        bcU = a39;
        fmVarArr[i59] = a39;
        int i60 = i59 + 1;
        C2491fm a40 = C4105zY.m41624a(Node.class, "a39d43e4844dcd360dd51f63e94ef4ef", i60);
        bcV = a40;
        fmVarArr[i60] = a40;
        int i61 = i60 + 1;
        C2491fm a41 = C4105zY.m41624a(Node.class, "b73b41573d803841fe9715829ed6cbee", i61);
        bcW = a41;
        fmVarArr[i61] = a41;
        int i62 = i61 + 1;
        C2491fm a42 = C4105zY.m41624a(Node.class, "4bf32c0e92df10127598ebe463b851f8", i62);
        bcX = a42;
        fmVarArr[i62] = a42;
        int i63 = i62 + 1;
        C2491fm a43 = C4105zY.m41624a(Node.class, "d22a58e2070b57d140711436d84ec936", i63);
        bcY = a43;
        fmVarArr[i63] = a43;
        int i64 = i63 + 1;
        C2491fm a44 = C4105zY.m41624a(Node.class, "5dd3e9dba23dafe2d8f15d622716bdd9", i64);
        bcZ = a44;
        fmVarArr[i64] = a44;
        int i65 = i64 + 1;
        C2491fm a45 = C4105zY.m41624a(Node.class, "36b1b59f4c63d8ad38697271536b9d18", i65);
        bda = a45;
        fmVarArr[i65] = a45;
        int i66 = i65 + 1;
        C2491fm a46 = C4105zY.m41624a(Node.class, "bcea2db79dfe3fcba6e9dbd754630280", i66);
        bdb = a46;
        fmVarArr[i66] = a46;
        int i67 = i66 + 1;
        C2491fm a47 = C4105zY.m41624a(Node.class, "41f187992c09ec6ace0dfa6e77d82404", i67);
        bdc = a47;
        fmVarArr[i67] = a47;
        int i68 = i67 + 1;
        C2491fm a48 = C4105zY.m41624a(Node.class, "c7efeb4af91bb603bd6ceb4b8c3bd2b4", i68);
        bdd = a48;
        fmVarArr[i68] = a48;
        int i69 = i68 + 1;
        C2491fm a49 = C4105zY.m41624a(Node.class, "466b724996ffdfa5c090f07649401fdd", i69);
        bde = a49;
        fmVarArr[i69] = a49;
        int i70 = i69 + 1;
        C2491fm a50 = C4105zY.m41624a(Node.class, "860a7aea2adde64d5987448b263ec5b0", i70);
        bdf = a50;
        fmVarArr[i70] = a50;
        int i71 = i70 + 1;
        C2491fm a51 = C4105zY.m41624a(Node.class, "ccf86ff924609364a8950624b194f390", i71);
        bdg = a51;
        fmVarArr[i71] = a51;
        int i72 = i71 + 1;
        C2491fm a52 = C4105zY.m41624a(Node.class, "0dbe7f73565919b6324ef4f75c66ae63", i72);
        bdh = a52;
        fmVarArr[i72] = a52;
        int i73 = i72 + 1;
        C2491fm a53 = C4105zY.m41624a(Node.class, "6b7b9f3ffb1f811bcdae15488d42558e", i73);
        bdi = a53;
        fmVarArr[i73] = a53;
        int i74 = i73 + 1;
        C2491fm a54 = C4105zY.m41624a(Node.class, "681bdb3d17ec978cc0496612b68d238f", i74);
        bdj = a54;
        fmVarArr[i74] = a54;
        int i75 = i74 + 1;
        C2491fm a55 = C4105zY.m41624a(Node.class, "4a8b92453e0ab838b9c9e978ffe66d9e", i75);
        bdk = a55;
        fmVarArr[i75] = a55;
        int i76 = i75 + 1;
        C2491fm a56 = C4105zY.m41624a(Node.class, "1c5a01b70704c8b522d65df35cf4bf3e", i76);
        bdl = a56;
        fmVarArr[i76] = a56;
        int i77 = i76 + 1;
        C2491fm a57 = C4105zY.m41624a(Node.class, "634809e454d0e1c0773625a5ca30250a", i77);
        bdm = a57;
        fmVarArr[i77] = a57;
        int i78 = i77 + 1;
        C2491fm a58 = C4105zY.m41624a(Node.class, "e0af9832a9c655e2efb92715f38cb288", i78);
        bdn = a58;
        fmVarArr[i78] = a58;
        int i79 = i78 + 1;
        C2491fm a59 = C4105zY.m41624a(Node.class, "85cd82768cf68638a2d19b0707a70bd0", i79);
        bdo = a59;
        fmVarArr[i79] = a59;
        int i80 = i79 + 1;
        C2491fm a60 = C4105zY.m41624a(Node.class, "ea4dffb141783bb6dbb78bc81e1bfcaa", i80);
        bdp = a60;
        fmVarArr[i80] = a60;
        int i81 = i80 + 1;
        C2491fm a61 = C4105zY.m41624a(Node.class, "24b3a8756e4657a6417ad5cf2c0b6349", i81);
        bdq = a61;
        fmVarArr[i81] = a61;
        int i82 = i81 + 1;
        C2491fm a62 = C4105zY.m41624a(Node.class, "4005589ac7ede9049f9990597a4f054e", i82);
        bdr = a62;
        fmVarArr[i82] = a62;
        int i83 = i82 + 1;
        C2491fm a63 = C4105zY.m41624a(Node.class, "7bf27ad95d5a77a68e08f2b362bdd82d", i83);
        bds = a63;
        fmVarArr[i83] = a63;
        int i84 = i83 + 1;
        C2491fm a64 = C4105zY.m41624a(Node.class, "bb6bbf3043ba9af97e15f876ebee2fd8", i84);
        bdt = a64;
        fmVarArr[i84] = a64;
        int i85 = i84 + 1;
        C2491fm a65 = C4105zY.m41624a(Node.class, "74fc84b9b54a82bc91ba6fef764599b7", i85);
        bdu = a65;
        fmVarArr[i85] = a65;
        int i86 = i85 + 1;
        C2491fm a66 = C4105zY.m41624a(Node.class, "68f043864abe8457efe1fd62dd8cfda5", i86);
        bdv = a66;
        fmVarArr[i86] = a66;
        int i87 = i86 + 1;
        C2491fm a67 = C4105zY.m41624a(Node.class, "3c987ba8abefcf14b665aead5769192b", i87);
        bdw = a67;
        fmVarArr[i87] = a67;
        int i88 = i87 + 1;
        C2491fm a68 = C4105zY.m41624a(Node.class, "bb3ab4d0a9eb873e583eaea406f6f96d", i88);
        bdx = a68;
        fmVarArr[i88] = a68;
        int i89 = i88 + 1;
        C2491fm a69 = C4105zY.m41624a(Node.class, "444cb850416a39e2b1c428222d85b0d8", i89);
        bdy = a69;
        fmVarArr[i89] = a69;
        int i90 = i89 + 1;
        C2491fm a70 = C4105zY.m41624a(Node.class, "7abec1643117c96a8f4a77890d9cb3f5", i90);
        bdz = a70;
        fmVarArr[i90] = a70;
        int i91 = i90 + 1;
        C2491fm a71 = C4105zY.m41624a(Node.class, "44f29c55c31461629ee4096e7b283f42", i91);
        bdA = a71;
        fmVarArr[i91] = a71;
        int i92 = i91 + 1;
        C2491fm a72 = C4105zY.m41624a(Node.class, "1a31df3950388377fa35e6e3a71e3e6c", i92);
        bdB = a72;
        fmVarArr[i92] = a72;
        int i93 = i92 + 1;
        C2491fm a73 = C4105zY.m41624a(Node.class, "73f1503a36ef881d1aba584d25103c28", i93);
        bdC = a73;
        fmVarArr[i93] = a73;
        int i94 = i93 + 1;
        C2491fm a74 = C4105zY.m41624a(Node.class, "a793364feff1bbe4f1899a9f231aa118", i94);
        bdD = a74;
        fmVarArr[i94] = a74;
        int i95 = i94 + 1;
        C2491fm a75 = C4105zY.m41624a(Node.class, "57ca3a4c2b1038ea568540ec26ed8e70", i95);
        bdE = a75;
        fmVarArr[i95] = a75;
        int i96 = i95 + 1;
        C2491fm a76 = C4105zY.m41624a(Node.class, "feb10b4839d82f12fb7ee3bc998d72c9", i96);
        bdF = a76;
        fmVarArr[i96] = a76;
        int i97 = i96 + 1;
        C2491fm a77 = C4105zY.m41624a(Node.class, "0cd29c45d9d1975f011c97dfc9a2fca1", i97);
        bdG = a77;
        fmVarArr[i97] = a77;
        int i98 = i97 + 1;
        C2491fm a78 = C4105zY.m41624a(Node.class, "94db72c4efaac4cc2b5b030fb2eba06f", i98);
        bdH = a78;
        fmVarArr[i98] = a78;
        int i99 = i98 + 1;
        C2491fm a79 = C4105zY.m41624a(Node.class, "771465e315dab965b85b4ddffbf72e04", i99);
        bdI = a79;
        fmVarArr[i99] = a79;
        int i100 = i99 + 1;
        C2491fm a80 = C4105zY.m41624a(Node.class, "10f8b72481cf288552c8aece4f3c9511", i100);
        bdJ = a80;
        fmVarArr[i100] = a80;
        int i101 = i100 + 1;
        C2491fm a81 = C4105zY.m41624a(Node.class, "91a53f6f539b7ac00abc38d4f736fcb7", i101);
        bdK = a81;
        fmVarArr[i101] = a81;
        int i102 = i101 + 1;
        C2491fm a82 = C4105zY.m41624a(Node.class, "ef640dd59e4eb38cdf550c56a9351097", i102);
        bdL = a82;
        fmVarArr[i102] = a82;
        int i103 = i102 + 1;
        C2491fm a83 = C4105zY.m41624a(Node.class, "7a8ada200201b0940383091b3e8d2816", i103);
        bdM = a83;
        fmVarArr[i103] = a83;
        int i104 = i103 + 1;
        C2491fm a84 = C4105zY.m41624a(Node.class, "5e94b911ebbd295b066f88e04f1fb228", i104);
        bdN = a84;
        fmVarArr[i104] = a84;
        int i105 = i104 + 1;
        C2491fm a85 = C4105zY.m41624a(Node.class, "b816acf8eb32a29314546341860256e0", i105);
        bdO = a85;
        fmVarArr[i105] = a85;
        int i106 = i105 + 1;
        C2491fm a86 = C4105zY.m41624a(Node.class, "4fa4f274a32e626ff5d2a47de02b6475", i106);
        bdP = a86;
        fmVarArr[i106] = a86;
        int i107 = i106 + 1;
        C2491fm a87 = C4105zY.m41624a(Node.class, "a76c077d133199b86b9b5eabc0404ef3", i107);
        bdQ = a87;
        fmVarArr[i107] = a87;
        int i108 = i107 + 1;
        C2491fm a88 = C4105zY.m41624a(Node.class, "953f46f481f877060c995eeca54bacdd", i108);
        f8985MI = a88;
        fmVarArr[i108] = a88;
        int i109 = i108 + 1;
        C2491fm a89 = C4105zY.m41624a(Node.class, "3595770773bd7ace45a4246da28ff60a", i109);
        f8986MJ = a89;
        fmVarArr[i109] = a89;
        int i110 = i109 + 1;
        C2491fm a90 = C4105zY.m41624a(Node.class, "c7a624a20f522baa793e53521d76fe63", i110);
        f8996x99c43db3 = a90;
        fmVarArr[i110] = a90;
        int i111 = i110 + 1;
        C2491fm a91 = C4105zY.m41624a(Node.class, "97207fcec542795127f2b9431d799dcf", i111);
        aMn = a91;
        fmVarArr[i111] = a91;
        int i112 = i111 + 1;
        C2491fm a92 = C4105zY.m41624a(Node.class, "acf42ca6dd87dd2d5967989262452a44", i112);
        aMo = a92;
        fmVarArr[i112] = a92;
        int i113 = i112 + 1;
        C2491fm a93 = C4105zY.m41624a(Node.class, "90ae4da7d354afae84bec589f493d05a", i113);
        f8994QE = a93;
        fmVarArr[i113] = a93;
        int i114 = i113 + 1;
        C2491fm a94 = C4105zY.m41624a(Node.class, "9194d312c1699d8049c0248115389ef6", i114);
        f8995QF = a94;
        fmVarArr[i114] = a94;
        int i115 = i114 + 1;
        C2491fm a95 = C4105zY.m41624a(Node.class, "21b19515111da01a2275dd78331cdf67", i115);
        aMj = a95;
        fmVarArr[i115] = a95;
        int i116 = i115 + 1;
        C2491fm a96 = C4105zY.m41624a(Node.class, "13c8a07f63f9307f1739d549ec8f5445", i116);
        f8987MN = a96;
        fmVarArr[i116] = a96;
        int i117 = i116 + 1;
        C2491fm a97 = C4105zY.m41624a(Node.class, "7f8be7916bf9e46f242a43cdc919d178", i117);
        aMp = a97;
        fmVarArr[i117] = a97;
        int i118 = i117 + 1;
        C2491fm a98 = C4105zY.m41624a(Node.class, "e42032548bac53113e4375085cc0a0b0", i118);
        f8991Ob = a98;
        fmVarArr[i118] = a98;
        int i119 = i118 + 1;
        C2491fm a99 = C4105zY.m41624a(Node.class, "4601c38c28c44d4f5a70415e746735b6", i119);
        f8992Oc = a99;
        fmVarArr[i119] = a99;
        int i120 = i119 + 1;
        C2491fm a100 = C4105zY.m41624a(Node.class, "5f5329deb31f1d62d527dd92a204b38e", i120);
        aMq = a100;
        fmVarArr[i120] = a100;
        int i121 = i120 + 1;
        C2491fm a101 = C4105zY.m41624a(Node.class, "0dd90ef01faddd4d58de07a65895ac7d", i121);
        aMr = a101;
        fmVarArr[i121] = a101;
        int i122 = i121 + 1;
        C2491fm a102 = C4105zY.m41624a(Node.class, "398fae9b139268384cdab99e0212c4a5", i122);
        aMs = a102;
        fmVarArr[i122] = a102;
        int i123 = i122 + 1;
        C2491fm a103 = C4105zY.m41624a(Node.class, "48944d1a05a2cf188fd53390ba611478", i123);
        f8982Do = a103;
        fmVarArr[i123] = a103;
        int i124 = i123 + 1;
        C2491fm a104 = C4105zY.m41624a(Node.class, "592828907173d971d8256a29da7ed21c", i124);
        bdR = a104;
        fmVarArr[i124] = a104;
        int i125 = i124 + 1;
        C2491fm a105 = C4105zY.m41624a(Node.class, "397af32d0b640cdd0518892fcab62674", i125);
        aMu = a105;
        fmVarArr[i125] = a105;
        int i126 = i125 + 1;
        C2491fm a106 = C4105zY.m41624a(Node.class, "61276fe8865a567219dc8fdb2fc237a6", i126);
        aMv = a106;
        fmVarArr[i126] = a106;
        int i127 = i126 + 1;
        C2491fm a107 = C4105zY.m41624a(Node.class, "5a53be96b2ad0adba2ee6652fe9dd316", i127);
        aKm = a107;
        fmVarArr[i127] = a107;
        int i128 = i127 + 1;
        C2491fm a108 = C4105zY.m41624a(Node.class, "36d0f02372f7d78e7492e127cfcb44f0", i128);
        bdS = a108;
        fmVarArr[i128] = a108;
        int i129 = i128 + 1;
        _m_methods = fmVarArr;
        C1634Xv.m11725a((Object[]) TaikodomObject._m_methods, (Object[]) _m_methods);
        ___iScriptClass = aUO.m18566a(Node.class, C0883Mo.class, _m_fields, _m_methods);
    }

    @C0064Am(aul = "a39d43e4844dcd360dd51f63e94ef4ef", aum = 0)
    @C5566aOg
    /* renamed from: K */
    private void m38094K(Actor cr) {
        throw new aWi(new aCE(this, bcV, new Object[]{cr}));
    }

    /* renamed from: L */
    private void m38095L(Asset tCVar) {
        bFf().mo5608dq().mo3197f(aMg, tCVar);
    }

    /* renamed from: Ll */
    private C3438ra m38096Ll() {
        return (C3438ra) bFf().mo5608dq().mo3214p(awd);
    }

    /* renamed from: Ln */
    private float m38097Ln() {
        return bFf().mo5608dq().mo3211m(awh);
    }

    @C0064Am(aul = "36b1b59f4c63d8ad38697271536b9d18", aum = 0)
    @C5566aOg
    /* renamed from: M */
    private void m38098M(Actor cr) {
        throw new aWi(new aCE(this, bda, new Object[]{cr}));
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Pda Render")
    @C0064Am(aul = "398fae9b139268384cdab99e0212c4a5", aum = 0)
    @C5566aOg
    /* renamed from: M */
    private void m38099M(Asset tCVar) {
        throw new aWi(new aCE(this, aMs, new Object[]{tCVar}));
    }

    /* renamed from: MX */
    private StellarSystem m38100MX() {
        return (StellarSystem) bFf().mo5608dq().mo3214p(aBo);
    }

    @C0064Am(aul = "5a53be96b2ad0adba2ee6652fe9dd316", aum = 0)
    @C5566aOg
    /* renamed from: QR */
    private void m38103QR() {
        throw new aWi(new aCE(this, aKm, new Object[0]));
    }

    /* renamed from: RQ */
    private Asset m38104RQ() {
        return (Asset) bFf().mo5608dq().mo3214p(aMg);
    }

    /* renamed from: RR */
    private DatabaseCategory m38105RR() {
        return (DatabaseCategory) bFf().mo5608dq().mo3214p(aMh);
    }

    /* renamed from: RS */
    private float m38106RS() {
        return bFf().mo5608dq().mo3211m(aMi);
    }

    /* renamed from: YV */
    private float m38116YV() {
        return bFf().mo5608dq().mo3211m(bcr);
    }

    /* renamed from: YW */
    private boolean m38117YW() {
        return bFf().mo5608dq().mo3201h(bct);
    }

    /* renamed from: YX */
    private Vec3d m38118YX() {
        return (Vec3d) bFf().mo5608dq().mo3214p(bcu);
    }

    /* renamed from: YY */
    private Space m38119YY() {
        return (Space) bFf().mo5608dq().mo3214p(bcv);
    }

    /* renamed from: YZ */
    private C1556Wo m38120YZ() {
        return (C1556Wo) bFf().mo5608dq().mo3214p(bcx);
    }

    /* renamed from: ZC */
    private Set<Gate> m38122ZC() {
        switch (bFf().mo6893i(bdh)) {
            case 0:
                return null;
            case 2:
                return (Set) bFf().mo5606d(new aCE(this, bdh, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, bdh, new Object[0]));
                break;
        }
        return m38121ZB();
    }

    /* renamed from: ZG */
    private Set<Scenery> m38125ZG() {
        switch (bFf().mo6893i(bdm)) {
            case 0:
                return null;
            case 2:
                return (Set) bFf().mo5606d(new aCE(this, bdm, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, bdm, new Object[0]));
                break;
        }
        return m38124ZF();
    }

    /* renamed from: ZK */
    private Set<HazardArea> m38128ZK() {
        switch (bFf().mo6893i(bdr)) {
            case 0:
                return null;
            case 2:
                return (Set) bFf().mo5606d(new aCE(this, bdr, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, bdr, new Object[0]));
                break;
        }
        return m38127ZJ();
    }

    /* renamed from: ZO */
    private Set<AsteroidZone> m38131ZO() {
        switch (bFf().mo6893i(bdw)) {
            case 0:
                return null;
            case 2:
                return (Set) bFf().mo5606d(new aCE(this, bdw, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, bdw, new Object[0]));
                break;
        }
        return m38130ZN();
    }

    /* renamed from: ZS */
    private Set<NPCZone> m38134ZS() {
        switch (bFf().mo6893i(bdB)) {
            case 0:
                return null;
            case 2:
                return (Set) bFf().mo5606d(new aCE(this, bdB, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, bdB, new Object[0]));
                break;
        }
        return m38133ZR();
    }

    /* renamed from: ZW */
    private Set<ScriptableZone> m38137ZW() {
        switch (bFf().mo6893i(bdG)) {
            case 0:
                return null;
            case 2:
                return (Set) bFf().mo5606d(new aCE(this, bdG, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, bdG, new Object[0]));
                break;
        }
        return m38136ZV();
    }

    /* renamed from: Za */
    private boolean m38140Za() {
        return bFf().mo5608dq().mo3201h(bcz);
    }

    /* renamed from: Zo */
    private List<Actor> m38149Zo() {
        switch (bFf().mo6893i(bcM)) {
            case 0:
                return null;
            case 2:
                return (List) bFf().mo5606d(new aCE(this, bcM, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, bcM, new Object[0]));
                break;
        }
        return m38148Zn();
    }

    /* renamed from: Zs */
    private Set<Actor> m38152Zs() {
        switch (bFf().mo6893i(bcT)) {
            case 0:
                return null;
            case 2:
                return (Set) bFf().mo5606d(new aCE(this, bcT, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, bcT, new Object[0]));
                break;
        }
        return m38151Zr();
    }

    @Deprecated
    /* renamed from: Zu */
    private Set<Actor> m38154Zu() {
        switch (bFf().mo6893i(bcU)) {
            case 0:
                return null;
            case 2:
                return (Set) bFf().mo5606d(new aCE(this, bcU, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, bcU, new Object[0]));
                break;
        }
        return m38153Zt();
    }

    /* renamed from: Zy */
    private Set<Station> m38157Zy() {
        switch (bFf().mo6893i(bdc)) {
            case 0:
                return null;
            case 2:
                return (Set) bFf().mo5606d(new aCE(this, bdc, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, bdc, new Object[0]));
                break;
        }
        return m38156Zx();
    }

    /* renamed from: a */
    private void m38159a(Space ea) {
        bFf().mo5608dq().mo3197f(bcv, ea);
    }

    @C0064Am(aul = "a76c077d133199b86b9b5eabc0404ef3", aum = 0)
    @C5566aOg
    /* renamed from: a */
    private void m38161a(StellarSystem jj, Set<Actor> set) {
        throw new aWi(new aCE(this, bdQ, new Object[]{jj, set}));
    }

    @C3248pc(aYR = C3248pc.C3250b.ALTERNATE_ADDER, displayName = "Node Objects: Advertise")
    @C0064Am(aul = "ef640dd59e4eb38cdf550c56a9351097", aum = 0)
    @C5566aOg
    /* renamed from: a */
    private void m38162a(AdvertiseType kp) {
        throw new aWi(new aCE(this, bdL, new Object[]{kp}));
    }

    @C0064Am(aul = "b73b41573d803841fe9715829ed6cbee", aum = 0)
    @C5566aOg
    /* renamed from: a */
    private void m38163a(StationType mx) {
        throw new aWi(new aCE(this, bcW, new Object[]{mx}));
    }

    /* renamed from: a */
    private void m38164a(NodeAccessImprovementType oc) {
        bFf().mo5608dq().mo3197f(f8989Mn, oc);
    }

    @C3248pc(aYR = C3248pc.C3250b.ADDER, displayName = "Node Objects: Scenery")
    @C0064Am(aul = "e0af9832a9c655e2efb92715f38cb288", aum = 0)
    @C5566aOg
    /* renamed from: a */
    private void m38165a(Scenery un) {
        throw new aWi(new aCE(this, bdn, new Object[]{un}));
    }

    @C3248pc(aYR = C3248pc.C3250b.ADDER, displayName = "Node Objects: Advertise")
    @C0064Am(aul = "7a8ada200201b0940383091b3e8d2816", aum = 0)
    @C5566aOg
    /* renamed from: a */
    private void m38166a(Advertise we) {
        throw new aWi(new aCE(this, bdM, new Object[]{we}));
    }

    @C3248pc(aYR = C3248pc.C3250b.ADDER, displayName = "Node Objects: ScriptableZones")
    @C0064Am(aul = "94db72c4efaac4cc2b5b030fb2eba06f", aum = 0)
    @C5566aOg
    /* renamed from: a */
    private void m38167a(ScriptableZone zz) {
        throw new aWi(new aCE(this, bdH, new Object[]{zz}));
    }

    @C3248pc(aYR = C3248pc.C3250b.ALTERNATE_ADDER, displayName = "Node Objects: AsteroidZones")
    @C0064Am(aul = "bb3ab4d0a9eb873e583eaea406f6f96d", aum = 0)
    @C5566aOg
    /* renamed from: a */
    private void m38168a(AsteroidZoneType asq) {
        throw new aWi(new aCE(this, bdx, new Object[]{asq}));
    }

    /* renamed from: a */
    private void m38169a(DatabaseCategory aik) {
        bFf().mo5608dq().mo3197f(aMh, aik);
    }

    @C3248pc(aYR = C3248pc.C3250b.ADDER, displayName = "Taikopedia Entries")
    @C0064Am(aul = "db2c9e809aab9107972215494692eafe", aum = 0)
    @C5566aOg
    /* renamed from: a */
    private void m38170a(TaikopediaEntry aiz) {
        throw new aWi(new aCE(this, aMl, new Object[]{aiz}));
    }

    @C4034yP
    @C5566aOg
    @C0064Am(aul = "3b52e92ca8ccf6cc7b390176b9f29921", aum = 0)
    @C2499fr
    /* renamed from: a */
    private void m38171a(Player aku, String str, PlayerController tVar) {
        throw new aWi(new aCE(this, bcP, new Object[]{aku, str, tVar}));
    }

    @C0064Am(aul = "5dd3e9dba23dafe2d8f15d622716bdd9", aum = 0)
    @C5566aOg
    /* renamed from: a */
    private void m38172a(SceneryType atc) {
        throw new aWi(new aCE(this, bcZ, new Object[]{atc}));
    }

    @C3248pc(aYR = C3248pc.C3250b.ADDER, displayName = "Node Objects: AsteroidZones")
    @C0064Am(aul = "444cb850416a39e2b1c428222d85b0d8", aum = 0)
    @C5566aOg
    /* renamed from: a */
    private void m38173a(AsteroidZone btVar) {
        throw new aWi(new aCE(this, bdy, new Object[]{btVar}));
    }

    @C0064Am(aul = "36d0f02372f7d78e7492e127cfcb44f0", aum = 0)
    @C5566aOg
    /* renamed from: a */
    private void m38174a(PrintWriter printWriter) {
        throw new aWi(new aCE(this, bdS, new Object[]{printWriter}));
    }

    /* renamed from: a */
    private void m38175a(String str) {
        bFf().mo5608dq().mo3197f(f8999bM, str);
    }

    /* renamed from: a */
    private void m38176a(UUID uuid) {
        bFf().mo5608dq().mo3197f(f8998bL, uuid);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Radius")
    @C0064Am(aul = "bfbc0dbda8ff0e04718863e2af22680a", aum = 0)
    @C5566aOg
    /* renamed from: aA */
    private void m38177aA(float f) {
        throw new aWi(new aCE(this, _f_setRadius_0020_0028F_0029V, new Object[]{new Float(f)}));
    }

    /* renamed from: aS */
    private void m38178aS(boolean z) {
        bFf().mo5608dq().mo3153a(bct, z);
    }

    /* renamed from: aT */
    private void m38179aT(boolean z) {
        bFf().mo5608dq().mo3153a(bcz, z);
    }

    /* renamed from: aU */
    private void m38180aU(float f) {
        bFf().mo5608dq().mo3150a(awh, f);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "PVP Allowed")
    @C0064Am(aul = "51b660f02163632d67a62bc885597f88", aum = 0)
    @C5566aOg
    /* renamed from: aU */
    private void m38181aU(boolean z) {
        throw new aWi(new aCE(this, bcE, new Object[]{new Boolean(z)}));
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Block Item Disposal")
    @C0064Am(aul = "9014d3f07ea769c47aab271ad7582c03", aum = 0)
    @C5566aOg
    /* renamed from: aW */
    private void m38182aW(boolean z) {
        throw new aWi(new aCE(this, bcK, new Object[]{new Boolean(z)}));
    }

    private Set<Advertise> aaa() {
        switch (bFf().mo6893i(bdK)) {
            case 0:
                return null;
            case 2:
                return (Set) bFf().mo5606d(new aCE(this, bdK, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, bdK, new Object[0]));
                break;
        }
        return m38139ZZ();
    }

    /* renamed from: an */
    private UUID m38183an() {
        return (UUID) bFf().mo5608dq().mo3214p(f8998bL);
    }

    /* renamed from: ao */
    private String m38184ao() {
        return (String) bFf().mo5608dq().mo3214p(f8999bM);
    }

    @C3248pc(aYR = C3248pc.C3250b.ALTERNATE_ADDER, displayName = "Node Objects: NPCZones")
    @C0064Am(aul = "73f1503a36ef881d1aba584d25103c28", aum = 0)
    @C5566aOg
    /* renamed from: b */
    private void m38189b(NPCZoneType hu) {
        throw new aWi(new aCE(this, bdC, new Object[]{hu}));
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Node Access Improvement")
    @C0064Am(aul = "3595770773bd7ace45a4246da28ff60a", aum = 0)
    @C5566aOg
    /* renamed from: b */
    private void m38190b(NodeAccessImprovementType oc) {
        throw new aWi(new aCE(this, f8986MJ, new Object[]{oc}));
    }

    @C3248pc(aYR = C3248pc.C3250b.ADDER, displayName = "Node Objects: Scenery")
    @C5566aOg
    /* renamed from: b */
    private void m38191b(Scenery un) {
        switch (bFf().mo6893i(bdn)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, bdn, new Object[]{un}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, bdn, new Object[]{un}));
                break;
        }
        m38165a(un);
    }

    @C3248pc(aYR = C3248pc.C3250b.ADDER, displayName = "Node Objects: Advertise")
    @C5566aOg
    /* renamed from: b */
    private void m38192b(Advertise we) {
        switch (bFf().mo6893i(bdM)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, bdM, new Object[]{we}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, bdM, new Object[]{we}));
                break;
        }
        m38166a(we);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Database Category")
    @C0064Am(aul = "acf42ca6dd87dd2d5967989262452a44", aum = 0)
    @C5566aOg
    /* renamed from: b */
    private void m38193b(DatabaseCategory aik) {
        throw new aWi(new aCE(this, aMo, new Object[]{aik}));
    }

    @C4034yP
    @C5566aOg
    @C2499fr
    /* renamed from: b */
    private void m38194b(Player aku, String str, PlayerController tVar) {
        switch (bFf().mo6893i(bcP)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, bcP, new Object[]{aku, str, tVar}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, bcP, new Object[]{aku, str, tVar}));
                break;
        }
        m38171a(aku, str, tVar);
    }

    @C0064Am(aul = "4bf32c0e92df10127598ebe463b851f8", aum = 0)
    @C5566aOg
    /* renamed from: b */
    private void m38195b(HazardAreaType ass) {
        throw new aWi(new aCE(this, bcX, new Object[]{ass}));
    }

    @C3248pc(aYR = C3248pc.C3250b.ADDER, displayName = "Node Objects: AsteroidZones")
    @C5566aOg
    /* renamed from: b */
    private void m38196b(AsteroidZone btVar) {
        switch (bFf().mo6893i(bdy)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, bdy, new Object[]{btVar}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, bdy, new Object[]{btVar}));
                break;
        }
        m38173a(btVar);
    }

    @C3248pc(aYR = C3248pc.C3250b.ADDER, displayName = "Node Objects: HazardArea")
    @C0064Am(aul = "7bf27ad95d5a77a68e08f2b362bdd82d", aum = 0)
    @C5566aOg
    /* renamed from: b */
    private void m38197b(HazardArea pbVar) {
        throw new aWi(new aCE(this, bds, new Object[]{pbVar}));
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Handle")
    @C0064Am(aul = "7fa20b783cf3d52be98b9b94addc6b37", aum = 0)
    @C5566aOg
    /* renamed from: b */
    private void m38198b(String str) {
        throw new aWi(new aCE(this, f9003bQ, new Object[]{str}));
    }

    /* renamed from: br */
    private void m38200br(I18NString i18NString) {
        bFf().mo5608dq().mo3197f(f8993QA, i18NString);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Description")
    @C0064Am(aul = "9194d312c1699d8049c0248115389ef6", aum = 0)
    @C5566aOg
    /* renamed from: bu */
    private void m38201bu(I18NString i18NString) {
        throw new aWi(new aCE(this, f8995QF, new Object[]{i18NString}));
    }

    /* renamed from: c */
    private void m38202c(StellarSystem jj) {
        bFf().mo5608dq().mo3197f(aBo, jj);
    }

    @C3248pc(aYR = C3248pc.C3250b.ALTERNATE_ADDER, displayName = "Node Objects: Station")
    @C0064Am(aul = "466b724996ffdfa5c090f07649401fdd", aum = 0)
    @C5566aOg
    /* renamed from: c */
    private void m38203c(StationType mx) {
        throw new aWi(new aCE(this, bde, new Object[]{mx}));
    }

    @C3248pc(aYR = C3248pc.C3250b.REMOVER, displayName = "Node Objects: Scenery")
    @C0064Am(aul = "ea4dffb141783bb6dbb78bc81e1bfcaa", aum = 0)
    @C5566aOg
    /* renamed from: c */
    private void m38204c(Scenery un) {
        throw new aWi(new aCE(this, bdp, new Object[]{un}));
    }

    @C3248pc(aYR = C3248pc.C3250b.REMOVER, displayName = "Node Objects: Advertise")
    @C0064Am(aul = "5e94b911ebbd295b066f88e04f1fb228", aum = 0)
    @C5566aOg
    /* renamed from: c */
    private void m38205c(Advertise we) {
        throw new aWi(new aCE(this, bdN, new Object[]{we}));
    }

    @C3248pc(aYR = C3248pc.C3250b.REMOVER, displayName = "Node Objects: ScriptableZones")
    @C0064Am(aul = "771465e315dab965b85b4ddffbf72e04", aum = 0)
    @C5566aOg
    /* renamed from: c */
    private void m38206c(ScriptableZone zz) {
        throw new aWi(new aCE(this, bdI, new Object[]{zz}));
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Position")
    @C0064Am(aul = "6de69324a0c80fdc352a6aa54b9bf612", aum = 0)
    @C5566aOg
    /* renamed from: c */
    private void m38207c(Vec3d ajr) {
        throw new aWi(new aCE(this, f9014vp, new Object[]{ajr}));
    }

    @C4034yP
    @C5566aOg
    @C0064Am(aul = "2f031a6b75dfd3d1032dbc77324fca50", aum = 0)
    @C2499fr
    /* renamed from: c */
    private void m38208c(Player aku, String str) {
        throw new aWi(new aCE(this, bcN, new Object[]{aku, str}));
    }

    @C3248pc(aYR = C3248pc.C3250b.ALTERNATE_ADDER, displayName = "Node Objects: Scenery")
    @C0064Am(aul = "85cd82768cf68638a2d19b0707a70bd0", aum = 0)
    @C5566aOg
    /* renamed from: c */
    private void m38209c(SceneryType atc) {
        throw new aWi(new aCE(this, bdo, new Object[]{atc}));
    }

    @C3248pc(aYR = C3248pc.C3250b.REMOVER, displayName = "Node Objects: AsteroidZones")
    @C0064Am(aul = "7abec1643117c96a8f4a77890d9cb3f5", aum = 0)
    @C5566aOg
    /* renamed from: c */
    private void m38210c(AsteroidZone btVar) {
        throw new aWi(new aCE(this, bdz, new Object[]{btVar}));
    }

    @C3248pc(aYR = C3248pc.C3250b.ADDER, displayName = "Node Objects: HazardArea")
    @C5566aOg
    /* renamed from: c */
    private void m38211c(HazardArea pbVar) {
        switch (bFf().mo6893i(bds)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, bds, new Object[]{pbVar}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, bds, new Object[]{pbVar}));
                break;
        }
        m38197b(pbVar);
    }

    /* renamed from: c */
    private void m38212c(UUID uuid) {
        switch (bFf().mo6893i(f9001bO)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f9001bO, new Object[]{uuid}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f9001bO, new Object[]{uuid}));
                break;
        }
        m38199b(uuid);
    }

    /* renamed from: cx */
    private void m38213cx(float f) {
        bFf().mo5608dq().mo3150a(aMi, f);
    }

    @C3248pc(aYR = C3248pc.C3250b.ALTERNATE_ADDER, displayName = "Node Objects: HazardArea")
    @C0064Am(aul = "bb6bbf3043ba9af97e15f876ebee2fd8", aum = 0)
    @C5566aOg
    /* renamed from: d */
    private void m38216d(HazardAreaType ass) {
        throw new aWi(new aCE(this, bdt, new Object[]{ass}));
    }

    @C3248pc(aYR = C3248pc.C3250b.ADDER, displayName = "Node Objects: NPCZones")
    @C0064Am(aul = "a793364feff1bbe4f1899a9f231aa118", aum = 0)
    @C5566aOg
    /* renamed from: d */
    private void m38217d(NPCZone fwVar) {
        throw new aWi(new aCE(this, bdD, new Object[]{fwVar}));
    }

    @C0064Am(aul = "d22a58e2070b57d140711436d84ec936", aum = 0)
    @C5566aOg
    /* renamed from: d */
    private void m38218d(GateType mxVar) {
        throw new aWi(new aCE(this, bcY, new Object[]{mxVar}));
    }

    @C3248pc(aYR = C3248pc.C3250b.REMOVER, displayName = "Node Objects: HazardArea")
    @C0064Am(aul = "74fc84b9b54a82bc91ba6fef764599b7", aum = 0)
    @C5566aOg
    /* renamed from: d */
    private void m38219d(HazardArea pbVar) {
        throw new aWi(new aCE(this, bdu, new Object[]{pbVar}));
    }

    /* renamed from: d */
    private void m38220d(I18NString i18NString) {
        bFf().mo5608dq().mo3197f(f9016zQ, i18NString);
    }

    /* renamed from: dc */
    private void m38221dc(float f) {
        bFf().mo5608dq().mo3150a(bcr, f);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Z Rotation")
    @C0064Am(aul = "f7f59082dcef16b7fe5887e493722938", aum = 0)
    @C5566aOg
    /* renamed from: dd */
    private void m38222dd(float f) {
        throw new aWi(new aCE(this, bcH, new Object[]{new Float(f)}));
    }

    @C3248pc(aYR = C3248pc.C3250b.REMOVER, displayName = "Taikopedia Entries")
    @C0064Am(aul = "0c7a2495bf5936c8362860212a057da6", aum = 0)
    @C5566aOg
    /* renamed from: e */
    private void m38223e(TaikopediaEntry aiz) {
        throw new aWi(new aCE(this, bcI, new Object[]{aiz}));
    }

    @C3248pc(aYR = C3248pc.C3250b.ADDER, displayName = "Node Objects: NPCZones")
    @C5566aOg
    /* renamed from: e */
    private void m38225e(NPCZone fwVar) {
        switch (bFf().mo6893i(bdD)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, bdD, new Object[]{fwVar}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, bdD, new Object[]{fwVar}));
                break;
        }
        m38217d(fwVar);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Name")
    @C0064Am(aul = "a34fcec5323da6a76094d298490fc31f", aum = 0)
    @C5566aOg
    /* renamed from: e */
    private void m38226e(I18NString i18NString) {
        throw new aWi(new aCE(this, f9018zU, new Object[]{i18NString}));
    }

    @C0064Am(aul = "4fa4f274a32e626ff5d2a47de02b6475", aum = 0)
    @C5566aOg
    /* renamed from: f */
    private Set<Actor> m38227f(StellarSystem jj) {
        throw new aWi(new aCE(this, bdP, new Object[]{jj}));
    }

    @C4034yP
    @C2499fr
    @C1253SX
    /* renamed from: f */
    private void m38228f(Player aku, String str) {
        switch (bFf().mo6893i(bcO)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, bcO, new Object[]{aku, str}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, bcO, new Object[]{aku, str}));
                break;
        }
        m38224e(aku, str);
    }

    @C3248pc(aYR = C3248pc.C3250b.REMOVER, displayName = "Node Objects: NPCZones")
    @C0064Am(aul = "57ca3a4c2b1038ea568540ec26ed8e70", aum = 0)
    @C5566aOg
    /* renamed from: f */
    private void m38229f(NPCZone fwVar) {
        throw new aWi(new aCE(this, bdE, new Object[]{fwVar}));
    }

    @C3248pc(aYR = C3248pc.C3250b.ALTERNATE_ADDER, displayName = "Node Objects: Gate")
    @C0064Am(aul = "681bdb3d17ec978cc0496612b68d238f", aum = 0)
    @C5566aOg
    /* renamed from: f */
    private void m38230f(GateType mxVar) {
        throw new aWi(new aCE(this, bdj, new Object[]{mxVar}));
    }

    @C3248pc(aYR = C3248pc.C3250b.ADDER, displayName = "Node Objects: Gate")
    @C0064Am(aul = "6b7b9f3ffb1f811bcdae15488d42558e", aum = 0)
    @C5566aOg
    /* renamed from: g */
    private void m38231g(Gate fFVar) {
        throw new aWi(new aCE(this, bdi, new Object[]{fFVar}));
    }

    @C3248pc(aYR = C3248pc.C3250b.ADDER, displayName = "Node Objects: Gate")
    @C5566aOg
    /* renamed from: h */
    private void m38232h(Gate fFVar) {
        switch (bFf().mo6893i(bdi)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, bdi, new Object[]{fFVar}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, bdi, new Object[]{fFVar}));
                break;
        }
        m38231g(fFVar);
    }

    @C3248pc(aYR = C3248pc.C3250b.REMOVER, displayName = "Node Objects: Gate")
    @C0064Am(aul = "4a8b92453e0ab838b9c9e978ffe66d9e", aum = 0)
    @C5566aOg
    /* renamed from: i */
    private void m38233i(Gate fFVar) {
        throw new aWi(new aCE(this, bdk, new Object[]{fFVar}));
    }

    @C3248pc(aYR = C3248pc.C3250b.ADDER, displayName = "Node Objects: Station")
    @C0064Am(aul = "c7efeb4af91bb603bd6ceb4b8c3bd2b4", aum = 0)
    @C5566aOg
    /* renamed from: j */
    private void m38236j(Station bf) {
        throw new aWi(new aCE(this, bdd, new Object[]{bf}));
    }

    @C3248pc(aYR = C3248pc.C3250b.ADDER, displayName = "Node Objects: Station")
    @C5566aOg
    /* renamed from: k */
    private void m38237k(Station bf) {
        switch (bFf().mo6893i(bdd)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, bdd, new Object[]{bf}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, bdd, new Object[]{bf}));
                break;
        }
        m38236j(bf);
    }

    /* renamed from: kb */
    private I18NString m38238kb() {
        return (I18NString) bFf().mo5608dq().mo3214p(f9016zQ);
    }

    @C3248pc(aYR = C3248pc.C3250b.REMOVER, displayName = "Node Objects: Station")
    @C0064Am(aul = "860a7aea2adde64d5987448b263ec5b0", aum = 0)
    @C5566aOg
    /* renamed from: l */
    private void m38240l(Station bf) {
        throw new aWi(new aCE(this, bdf, new Object[]{bf}));
    }

    /* renamed from: p */
    private void m38241p(C1556Wo wo) {
        bFf().mo5608dq().mo3197f(bcx, wo);
    }

    /* renamed from: r */
    private void m38242r(Vec3d ajr) {
        bFf().mo5608dq().mo3197f(bcu, ajr);
    }

    /* renamed from: r */
    private void m38243r(Asset tCVar) {
        bFf().mo5608dq().mo3197f(f8984LR, tCVar);
    }

    /* renamed from: rh */
    private Asset m38246rh() {
        return (Asset) bFf().mo5608dq().mo3214p(f8984LR);
    }

    /* renamed from: rs */
    private NodeAccessImprovementType m38247rs() {
        return (NodeAccessImprovementType) bFf().mo5608dq().mo3214p(f8989Mn);
    }

    /* renamed from: sG */
    private Asset m38248sG() {
        return (Asset) bFf().mo5608dq().mo3214p(f8990NY);
    }

    /* renamed from: t */
    private void m38250t(Asset tCVar) {
        bFf().mo5608dq().mo3197f(f8990NY, tCVar);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Icon")
    @C0064Am(aul = "4601c38c28c44d4f5a70415e746735b6", aum = 0)
    @C5566aOg
    /* renamed from: u */
    private void m38251u(Asset tCVar) {
        throw new aWi(new aCE(this, f8992Oc, new Object[]{tCVar}));
    }

    /* renamed from: vT */
    private I18NString m38252vT() {
        return (I18NString) bFf().mo5608dq().mo3214p(f8993QA);
    }

    /* renamed from: x */
    private void m38254x(C3438ra raVar) {
        bFf().mo5608dq().mo3197f(awd, raVar);
    }

    /* renamed from: H */
    public boolean mo21600H(Actor cr) {
        switch (bFf().mo6893i(bcQ)) {
            case 0:
                return false;
            case 2:
                return ((Boolean) bFf().mo5606d(new aCE(this, bcQ, new Object[]{cr}))).booleanValue();
            case 3:
                bFf().mo5606d(new aCE(this, bcQ, new Object[]{cr}));
                break;
        }
        return m38090G(cr);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Render Asset")
    /* renamed from: I */
    public void mo21601I(Asset tCVar) {
        switch (bFf().mo6893i(aDl)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, aDl, new Object[]{tCVar}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, aDl, new Object[]{tCVar}));
                break;
        }
        m38092H(tCVar);
    }

    /* renamed from: J */
    public boolean mo21602J(Actor cr) {
        switch (bFf().mo6893i(bcR)) {
            case 0:
                return false;
            case 2:
                return ((Boolean) bFf().mo5606d(new aCE(this, bcR, new Object[]{cr}))).booleanValue();
            case 3:
                bFf().mo5606d(new aCE(this, bcR, new Object[]{cr}));
                break;
        }
        return m38093I(cr);
    }

    /* access modifiers changed from: protected */
    @C5566aOg
    /* renamed from: L */
    public void mo21603L(Actor cr) {
        switch (bFf().mo6893i(bcV)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, bcV, new Object[]{cr}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, bcV, new Object[]{cr}));
                break;
        }
        m38094K(cr);
    }

    @C5566aOg
    /* renamed from: N */
    public void mo21604N(Actor cr) {
        switch (bFf().mo6893i(bda)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, bda, new Object[]{cr}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, bda, new Object[]{cr}));
                break;
        }
        m38098M(cr);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Pda Render")
    @C5566aOg
    /* renamed from: N */
    public void mo21605N(Asset tCVar) {
        switch (bFf().mo6893i(aMs)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, aMs, new Object[]{tCVar}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, aMs, new Object[]{tCVar}));
                break;
        }
        m38099M(tCVar);
    }

    @aFW("Stellar System")
    /* renamed from: Nc */
    public StellarSystem mo21606Nc() {
        switch (bFf().mo6893i(aBs)) {
            case 0:
                return null;
            case 2:
                return (StellarSystem) bFf().mo5606d(new aCE(this, aBs, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, aBs, new Object[0]));
                break;
        }
        return m38101Nb();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Render Asset")
    /* renamed from: Nu */
    public Asset mo21607Nu() {
        switch (bFf().mo6893i(aDk)) {
            case 0:
                return null;
            case 2:
                return (Asset) bFf().mo5606d(new aCE(this, aDk, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, aDk, new Object[0]));
                break;
        }
        return m38102Nt();
    }

    @C5566aOg
    /* renamed from: QS */
    public void mo21608QS() {
        switch (bFf().mo6893i(aKm)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, aKm, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, aKm, new Object[0]));
                break;
        }
        m38103QR();
    }

    /* renamed from: RU */
    public I18NString mo182RU() {
        switch (bFf().mo6893i(aMj)) {
            case 0:
                return null;
            case 2:
                return (I18NString) bFf().mo5606d(new aCE(this, aMj, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, aMj, new Object[0]));
                break;
        }
        return m38107RT();
    }

    @C3248pc(aYR = C3248pc.C3250b.ITERATOR, displayName = "Taikopedia Entries")
    /* renamed from: RW */
    public List<TaikopediaEntry> mo183RW() {
        switch (bFf().mo6893i(aMk)) {
            case 0:
                return null;
            case 2:
                return (List) bFf().mo5606d(new aCE(this, aMk, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, aMk, new Object[0]));
                break;
        }
        return m38108RV();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Database Category")
    /* renamed from: RY */
    public DatabaseCategory mo184RY() {
        switch (bFf().mo6893i(aMn)) {
            case 0:
                return null;
            case 2:
                return (DatabaseCategory) bFf().mo5606d(new aCE(this, aMn, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, aMn, new Object[0]));
                break;
        }
        return m38109RX();
    }

    /* renamed from: Sa */
    public String mo185Sa() {
        switch (bFf().mo6893i(aMp)) {
            case 0:
                return null;
            case 2:
                return (String) bFf().mo5606d(new aCE(this, aMp, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, aMp, new Object[0]));
                break;
        }
        return m38110RZ();
    }

    /* renamed from: Sc */
    public String mo21609Sc() {
        switch (bFf().mo6893i(aMq)) {
            case 0:
                return null;
            case 2:
                return (String) bFf().mo5606d(new aCE(this, aMq, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, aMq, new Object[0]));
                break;
        }
        return m38111Sb();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Pda Render")
    /* renamed from: Se */
    public Asset mo187Se() {
        switch (bFf().mo6893i(aMr)) {
            case 0:
                return null;
            case 2:
                return (Asset) bFf().mo5606d(new aCE(this, aMr, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, aMr, new Object[0]));
                break;
        }
        return m38112Sd();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Pda MP Multiplier")
    /* renamed from: Sg */
    public float mo188Sg() {
        switch (bFf().mo6893i(aMu)) {
            case 0:
                return 0.0f;
            case 2:
                return ((Float) bFf().mo5606d(new aCE(this, aMu, new Object[0]))).floatValue();
            case 3:
                bFf().mo5606d(new aCE(this, aMu, new Object[0]));
                break;
        }
        return m38113Sf();
    }

    /* renamed from: T */
    public C6494any mo11T() {
        return ___iScriptClass;
    }

    /* renamed from: U */
    public C2491fm[] mo12U() {
        return _m_methods;
    }

    /* renamed from: W */
    public Object mo13W() {
        return new C0883Mo(this);
    }

    @aFW(cZD = true, value = "Gates")
    /* renamed from: W */
    public Set<aDJ> mo21610W(Player aku) {
        switch (bFf().mo6893i(bdR)) {
            case 0:
                return null;
            case 2:
                return (Set) bFf().mo5606d(new aCE(this, bdR, new Object[]{aku}));
            case 3:
                bFf().mo5606d(new aCE(this, bdR, new Object[]{aku}));
                break;
        }
        return m38114V(aku);
    }

    @C3248pc(aYR = C3248pc.C3250b.ITERATOR, displayName = "Node Objects: Station")
    /* renamed from: ZA */
    public Set<Station> mo21611ZA() {
        switch (bFf().mo6893i(bdg)) {
            case 0:
                return null;
            case 2:
                return (Set) bFf().mo5606d(new aCE(this, bdg, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, bdg, new Object[0]));
                break;
        }
        return m38158Zz();
    }

    @C3248pc(aYR = C3248pc.C3250b.ITERATOR, displayName = "Node Objects: Gate")
    /* renamed from: ZE */
    public Set<Gate> mo21612ZE() {
        switch (bFf().mo6893i(bdl)) {
            case 0:
                return null;
            case 2:
                return (Set) bFf().mo5606d(new aCE(this, bdl, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, bdl, new Object[0]));
                break;
        }
        return m38123ZD();
    }

    @C3248pc(aYR = C3248pc.C3250b.ITERATOR, displayName = "Node Objects: Scenery")
    /* renamed from: ZI */
    public Set<Scenery> mo21613ZI() {
        switch (bFf().mo6893i(bdq)) {
            case 0:
                return null;
            case 2:
                return (Set) bFf().mo5606d(new aCE(this, bdq, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, bdq, new Object[0]));
                break;
        }
        return m38126ZH();
    }

    @C3248pc(aYR = C3248pc.C3250b.ITERATOR, displayName = "Node Objects: HazardArea")
    /* renamed from: ZM */
    public Set<HazardArea> mo21614ZM() {
        switch (bFf().mo6893i(bdv)) {
            case 0:
                return null;
            case 2:
                return (Set) bFf().mo5606d(new aCE(this, bdv, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, bdv, new Object[0]));
                break;
        }
        return m38129ZL();
    }

    @C3248pc(aYR = C3248pc.C3250b.ITERATOR, displayName = "Node Objects: AsteroidZones")
    /* renamed from: ZQ */
    public Set<AsteroidZone> mo21615ZQ() {
        switch (bFf().mo6893i(bdA)) {
            case 0:
                return null;
            case 2:
                return (Set) bFf().mo5606d(new aCE(this, bdA, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, bdA, new Object[0]));
                break;
        }
        return m38132ZP();
    }

    @C3248pc(aYR = C3248pc.C3250b.ITERATOR, displayName = "Node Objects: NPCZones")
    /* renamed from: ZU */
    public Set<NPCZone> mo21616ZU() {
        switch (bFf().mo6893i(bdF)) {
            case 0:
                return null;
            case 2:
                return (Set) bFf().mo5606d(new aCE(this, bdF, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, bdF, new Object[0]));
                break;
        }
        return m38135ZT();
    }

    @C3248pc(aYR = C3248pc.C3250b.ITERATOR, displayName = "Node Objects: ScriptableZones")
    /* renamed from: ZY */
    public Set<ScriptableZone> mo21617ZY() {
        switch (bFf().mo6893i(bdJ)) {
            case 0:
                return null;
            case 2:
                return (Set) bFf().mo5606d(new aCE(this, bdJ, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, bdJ, new Object[0]));
                break;
        }
        return m38138ZX();
    }

    /* renamed from: Zc */
    public Space mo21618Zc() {
        switch (bFf().mo6893i(bcA)) {
            case 0:
                return null;
            case 2:
                return (Space) bFf().mo5606d(new aCE(this, bcA, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, bcA, new Object[0]));
                break;
        }
        return m38141Zb();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "PVP Allowed")
    /* renamed from: Zf */
    public boolean mo21619Zf() {
        switch (bFf().mo6893i(bcD)) {
            case 0:
                return false;
            case 2:
                return ((Boolean) bFf().mo5606d(new aCE(this, bcD, new Object[0]))).booleanValue();
            case 3:
                bFf().mo5606d(new aCE(this, bcD, new Object[0]));
                break;
        }
        return m38143Ze();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Z Rotation")
    /* renamed from: Zi */
    public float mo21620Zi() {
        switch (bFf().mo6893i(bcG)) {
            case 0:
                return 0.0f;
            case 2:
                return ((Float) bFf().mo5606d(new aCE(this, bcG, new Object[0]))).floatValue();
            case 3:
                bFf().mo5606d(new aCE(this, bcG, new Object[0]));
                break;
        }
        return m38145Zh();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Block Item Disposal")
    /* renamed from: Zk */
    public boolean mo21621Zk() {
        switch (bFf().mo6893i(bcJ)) {
            case 0:
                return false;
            case 2:
                return ((Boolean) bFf().mo5606d(new aCE(this, bcJ, new Object[0]))).booleanValue();
            case 3:
                bFf().mo5606d(new aCE(this, bcJ, new Object[0]));
                break;
        }
        return m38146Zj();
    }

    /* renamed from: Zm */
    public Vec3d mo21622Zm() {
        switch (bFf().mo6893i(bcL)) {
            case 0:
                return null;
            case 2:
                return (Vec3d) bFf().mo5606d(new aCE(this, bcL, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, bcL, new Object[0]));
                break;
        }
        return m38147Zl();
    }

    /* renamed from: Zq */
    public Set<Actor> mo21623Zq() {
        switch (bFf().mo6893i(bcS)) {
            case 0:
                return null;
            case 2:
                return (Set) bFf().mo5606d(new aCE(this, bcS, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, bcS, new Object[0]));
                break;
        }
        return m38150Zp();
    }

    @C3248pc(aYR = C3248pc.C3250b.ITERATOR, displayName = "Node Objects: Static")
    @Deprecated
    /* renamed from: Zw */
    public Set<Actor> mo21624Zw() {
        switch (bFf().mo6893i(bdb)) {
            case 0:
                return null;
            case 2:
                return (Set) bFf().mo5606d(new aCE(this, bdb, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, bdb, new Object[0]));
                break;
        }
        return m38155Zv();
    }

    /* renamed from: a */
    public Object mo14a(C0495Gr gr) {
        Object[] args = gr.getArgs();
        switch (gr.mo2417hq() - TaikodomObject._m_methodCount) {
            case 0:
                return m38187au();
            case 1:
                return m38185ap();
            case 2:
                m38199b((UUID) args[0]);
                return null;
            case 3:
                return m38141Zb();
            case 4:
                m38188b((Space) args[0]);
                return null;
            case 5:
                return m38239kd();
            case 6:
                m38226e((I18NString) args[0]);
                return null;
            case 7:
                return m38142Zd();
            case 8:
                m38207c((Vec3d) args[0]);
                return null;
            case 9:
                return new Boolean(m38143Ze());
            case 10:
                m38181aU(((Boolean) args[0]).booleanValue());
                return null;
            case 11:
                return new Float(m38144Zg());
            case 12:
                m38177aA(((Float) args[0]).floatValue());
                return null;
            case 13:
                return m38186ar();
            case 14:
                m38198b((String) args[0]);
                return null;
            case 15:
                return new Float(m38145Zh());
            case 16:
                m38222dd(((Float) args[0]).floatValue());
                return null;
            case 17:
                return m38102Nt();
            case 18:
                m38092H((Asset) args[0]);
                return null;
            case 19:
                m38170a((TaikopediaEntry) args[0]);
                return null;
            case 20:
                m38223e((TaikopediaEntry) args[0]);
                return null;
            case 21:
                return m38108RV();
            case 22:
                return new Boolean(m38146Zj());
            case 23:
                m38182aW(((Boolean) args[0]).booleanValue());
                return null;
            case 24:
                return m38234io();
            case 25:
                return m38235iq();
            case 26:
                return m38101Nb();
            case 27:
                m38215d((StellarSystem) args[0]);
                return null;
            case 28:
                return m38147Zl();
            case 29:
                m38091Gd();
                return null;
            case 30:
                return m38148Zn();
            case 31:
                m38208c((Player) args[0], (String) args[1]);
                return null;
            case 32:
                m38224e((Player) args[0], (String) args[1]);
                return null;
            case 33:
                m38171a((Player) args[0], (String) args[1], (PlayerController) args[2]);
                return null;
            case 34:
                return new Boolean(m38090G((Actor) args[0]));
            case 35:
                return new Boolean(m38093I((Actor) args[0]));
            case 36:
                return m38150Zp();
            case 37:
                return m38151Zr();
            case 38:
                return m38153Zt();
            case 39:
                m38094K((Actor) args[0]);
                return null;
            case 40:
                m38163a((StationType) args[0]);
                return null;
            case 41:
                m38195b((HazardAreaType) args[0]);
                return null;
            case 42:
                m38218d((GateType) args[0]);
                return null;
            case 43:
                m38172a((SceneryType) args[0]);
                return null;
            case 44:
                m38098M((Actor) args[0]);
                return null;
            case 45:
                return m38155Zv();
            case 46:
                return m38156Zx();
            case 47:
                m38236j((Station) args[0]);
                return null;
            case 48:
                m38203c((StationType) args[0]);
                return null;
            case 49:
                m38240l((Station) args[0]);
                return null;
            case 50:
                return m38158Zz();
            case 51:
                return m38121ZB();
            case 52:
                m38231g((Gate) args[0]);
                return null;
            case 53:
                m38230f((GateType) args[0]);
                return null;
            case 54:
                m38233i((Gate) args[0]);
                return null;
            case 55:
                return m38123ZD();
            case 56:
                return m38124ZF();
            case 57:
                m38165a((Scenery) args[0]);
                return null;
            case 58:
                m38209c((SceneryType) args[0]);
                return null;
            case 59:
                m38204c((Scenery) args[0]);
                return null;
            case 60:
                return m38126ZH();
            case 61:
                return m38127ZJ();
            case 62:
                m38197b((HazardArea) args[0]);
                return null;
            case 63:
                m38216d((HazardAreaType) args[0]);
                return null;
            case 64:
                m38219d((HazardArea) args[0]);
                return null;
            case 65:
                return m38129ZL();
            case 66:
                return m38130ZN();
            case 67:
                m38168a((AsteroidZoneType) args[0]);
                return null;
            case 68:
                m38173a((AsteroidZone) args[0]);
                return null;
            case 69:
                m38210c((AsteroidZone) args[0]);
                return null;
            case 70:
                return m38132ZP();
            case 71:
                return m38133ZR();
            case 72:
                m38189b((NPCZoneType) args[0]);
                return null;
            case 73:
                m38217d((NPCZone) args[0]);
                return null;
            case 74:
                m38229f((NPCZone) args[0]);
                return null;
            case 75:
                return m38135ZT();
            case 76:
                return m38136ZV();
            case 77:
                m38167a((ScriptableZone) args[0]);
                return null;
            case 78:
                m38206c((ScriptableZone) args[0]);
                return null;
            case 79:
                return m38138ZX();
            case 80:
                return m38139ZZ();
            case 81:
                m38162a((AdvertiseType) args[0]);
                return null;
            case 82:
                m38166a((Advertise) args[0]);
                return null;
            case 83:
                m38205c((Advertise) args[0]);
                return null;
            case 84:
                return aab();
            case 85:
                return m38227f((StellarSystem) args[0]);
            case 86:
                m38161a((StellarSystem) args[0], (Set<Actor>) (Set) args[1]);
                return null;
            case 87:
                return m38244rK();
            case 88:
                m38190b((NodeAccessImprovementType) args[0]);
                return null;
            case 89:
                return aad();
            case 90:
                return m38109RX();
            case 91:
                m38193b((DatabaseCategory) args[0]);
                return null;
            case 92:
                return m38253vV();
            case 93:
                m38201bu((I18NString) args[0]);
                return null;
            case 94:
                return m38107RT();
            case 95:
                return m38245rO();
            case 96:
                return m38110RZ();
            case 97:
                return m38249sJ();
            case 98:
                m38251u((Asset) args[0]);
                return null;
            case 99:
                return m38111Sb();
            case 100:
                return m38112Sd();
            case 101:
                m38099M((Asset) args[0]);
                return null;
            case 102:
                m38160a((C0665JT) args[0]);
                return null;
            case 103:
                return m38114V((Player) args[0]);
            case 104:
                return new Float(m38113Sf());
            case 105:
                m38214cy(((Float) args[0]).floatValue());
                return null;
            case 106:
                m38103QR();
                return null;
            case 107:
                m38174a((PrintWriter) args[0]);
                return null;
            default:
                return super.mo14a(gr);
        }
    }

    /* renamed from: a */
    public void mo15a(Collection collection, C0495Gr gr) {
        super.mo15a(collection, gr);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "PVP Allowed")
    @C5566aOg
    /* renamed from: aV */
    public void mo21625aV(boolean z) {
        switch (bFf().mo6893i(bcE)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, bcE, new Object[]{new Boolean(z)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, bcE, new Object[]{new Boolean(z)}));
                break;
        }
        m38181aU(z);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Block Item Disposal")
    @C5566aOg
    /* renamed from: aX */
    public void mo21626aX(boolean z) {
        switch (bFf().mo6893i(bcK)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, bcK, new Object[]{new Boolean(z)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, bcK, new Object[]{new Boolean(z)}));
                break;
        }
        m38182aW(z);
    }

    @C3248pc(aYR = C3248pc.C3250b.ITERATOR, displayName = "Node Objects: Advertise")
    public Set<Advertise> aac() {
        switch (bFf().mo6893i(bdO)) {
            case 0:
                return null;
            case 2:
                return (Set) bFf().mo5606d(new aCE(this, bdO, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, bdO, new Object[0]));
                break;
        }
        return aab();
    }

    public Collection<aDR> aae() {
        switch (bFf().mo6893i(f8996x99c43db3)) {
            case 0:
                return null;
            case 2:
                return (Collection) bFf().mo5606d(new aCE(this, f8996x99c43db3, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f8996x99c43db3, new Object[0]));
                break;
        }
        return aad();
    }

    /* renamed from: aq */
    public UUID mo18aq() {
        switch (bFf().mo6893i(f9000bN)) {
            case 0:
                return null;
            case 2:
                return (UUID) bFf().mo5606d(new aCE(this, f9000bN, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f9000bN, new Object[0]));
                break;
        }
        return m38185ap();
    }

    /* renamed from: b */
    public void mo24b(C0665JT jt) {
        switch (bFf().mo6893i(f8982Do)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f8982Do, new Object[]{jt}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f8982Do, new Object[]{jt}));
                break;
        }
        m38160a(jt);
    }

    /* access modifiers changed from: package-private */
    @C5566aOg
    /* renamed from: b */
    public void mo21628b(StellarSystem jj, Set<Actor> set) {
        switch (bFf().mo6893i(bdQ)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, bdQ, new Object[]{jj, set}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, bdQ, new Object[]{jj, set}));
                break;
        }
        m38161a(jj, set);
    }

    @C3248pc(aYR = C3248pc.C3250b.ALTERNATE_ADDER, displayName = "Node Objects: Advertise")
    @C5566aOg
    /* renamed from: b */
    public void mo21629b(AdvertiseType kp) {
        switch (bFf().mo6893i(bdL)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, bdL, new Object[]{kp}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, bdL, new Object[]{kp}));
                break;
        }
        m38162a(kp);
    }

    @C5566aOg
    /* renamed from: b */
    public void mo21630b(StationType mx) {
        switch (bFf().mo6893i(bcW)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, bcW, new Object[]{mx}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, bcW, new Object[]{mx}));
                break;
        }
        m38163a(mx);
    }

    @C3248pc(aYR = C3248pc.C3250b.ADDER, displayName = "Node Objects: ScriptableZones")
    @C5566aOg
    /* renamed from: b */
    public void mo21631b(ScriptableZone zz) {
        switch (bFf().mo6893i(bdH)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, bdH, new Object[]{zz}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, bdH, new Object[]{zz}));
                break;
        }
        m38167a(zz);
    }

    @C3248pc(aYR = C3248pc.C3250b.ALTERNATE_ADDER, displayName = "Node Objects: AsteroidZones")
    @C5566aOg
    /* renamed from: b */
    public void mo21632b(AsteroidZoneType asq) {
        switch (bFf().mo6893i(bdx)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, bdx, new Object[]{asq}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, bdx, new Object[]{asq}));
                break;
        }
        m38168a(asq);
    }

    @C3248pc(aYR = C3248pc.C3250b.ADDER, displayName = "Taikopedia Entries")
    @C5566aOg
    /* renamed from: b */
    public void mo21633b(TaikopediaEntry aiz) {
        switch (bFf().mo6893i(aMl)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, aMl, new Object[]{aiz}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, aMl, new Object[]{aiz}));
                break;
        }
        m38170a(aiz);
    }

    @C5566aOg
    /* renamed from: b */
    public void mo21634b(SceneryType atc) {
        switch (bFf().mo6893i(bcZ)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, bcZ, new Object[]{atc}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, bcZ, new Object[]{atc}));
                break;
        }
        m38172a(atc);
    }

    @C5566aOg
    /* renamed from: b */
    public void mo21635b(PrintWriter printWriter) {
        switch (bFf().mo6893i(bdS)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, bdS, new Object[]{printWriter}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, bdS, new Object[]{printWriter}));
                break;
        }
        m38174a(printWriter);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Description")
    @C5566aOg
    /* renamed from: bv */
    public void mo21636bv(I18NString i18NString) {
        switch (bFf().mo6893i(f8995QF)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f8995QF, new Object[]{i18NString}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f8995QF, new Object[]{i18NString}));
                break;
        }
        m38201bu(i18NString);
    }

    /* renamed from: c */
    public void mo21637c(Space ea) {
        switch (bFf().mo6893i(bcB)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, bcB, new Object[]{ea}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, bcB, new Object[]{ea}));
                break;
        }
        m38188b(ea);
    }

    @C3248pc(aYR = C3248pc.C3250b.ALTERNATE_ADDER, displayName = "Node Objects: NPCZones")
    @C5566aOg
    /* renamed from: c */
    public void mo21638c(NPCZoneType hu) {
        switch (bFf().mo6893i(bdC)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, bdC, new Object[]{hu}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, bdC, new Object[]{hu}));
                break;
        }
        m38189b(hu);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Node Access Improvement")
    @C5566aOg
    /* renamed from: c */
    public void mo21639c(NodeAccessImprovementType oc) {
        switch (bFf().mo6893i(f8986MJ)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f8986MJ, new Object[]{oc}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f8986MJ, new Object[]{oc}));
                break;
        }
        m38190b(oc);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Database Category")
    @C5566aOg
    /* renamed from: c */
    public void mo21640c(DatabaseCategory aik) {
        switch (bFf().mo6893i(aMo)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, aMo, new Object[]{aik}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, aMo, new Object[]{aik}));
                break;
        }
        m38193b(aik);
    }

    @C5566aOg
    /* renamed from: c */
    public void mo21641c(HazardAreaType ass) {
        switch (bFf().mo6893i(bcX)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, bcX, new Object[]{ass}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, bcX, new Object[]{ass}));
                break;
        }
        m38195b(ass);
    }

    /* renamed from: c */
    public C5663aRz[] mo25c() {
        return _m_fields;
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Pda MP Multiplier")
    /* renamed from: cz */
    public void mo21642cz(float f) {
        switch (bFf().mo6893i(aMv)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, aMv, new Object[]{new Float(f)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, aMv, new Object[]{new Float(f)}));
                break;
        }
        m38214cy(f);
    }

    @C3248pc(aYR = C3248pc.C3250b.ALTERNATE_ADDER, displayName = "Node Objects: Station")
    @C5566aOg
    /* renamed from: d */
    public void mo21643d(StationType mx) {
        switch (bFf().mo6893i(bde)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, bde, new Object[]{mx}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, bde, new Object[]{mx}));
                break;
        }
        m38203c(mx);
    }

    @C3248pc(aYR = C3248pc.C3250b.REMOVER, displayName = "Node Objects: Scenery")
    @C5566aOg
    /* renamed from: d */
    public void mo21644d(Scenery un) {
        switch (bFf().mo6893i(bdp)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, bdp, new Object[]{un}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, bdp, new Object[]{un}));
                break;
        }
        m38204c(un);
    }

    @C3248pc(aYR = C3248pc.C3250b.REMOVER, displayName = "Node Objects: Advertise")
    @C5566aOg
    /* renamed from: d */
    public void mo21645d(Advertise we) {
        switch (bFf().mo6893i(bdN)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, bdN, new Object[]{we}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, bdN, new Object[]{we}));
                break;
        }
        m38205c(we);
    }

    @C3248pc(aYR = C3248pc.C3250b.REMOVER, displayName = "Node Objects: ScriptableZones")
    @C5566aOg
    /* renamed from: d */
    public void mo21646d(ScriptableZone zz) {
        switch (bFf().mo6893i(bdI)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, bdI, new Object[]{zz}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, bdI, new Object[]{zz}));
                break;
        }
        m38206c(zz);
    }

    @C4034yP
    @C5566aOg
    @C2499fr
    /* renamed from: d */
    public void mo21647d(Player aku, String str) {
        switch (bFf().mo6893i(bcN)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, bcN, new Object[]{aku, str}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, bcN, new Object[]{aku, str}));
                break;
        }
        m38208c(aku, str);
    }

    @C3248pc(aYR = C3248pc.C3250b.ALTERNATE_ADDER, displayName = "Node Objects: Scenery")
    @C5566aOg
    /* renamed from: d */
    public void mo21648d(SceneryType atc) {
        switch (bFf().mo6893i(bdo)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, bdo, new Object[]{atc}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, bdo, new Object[]{atc}));
                break;
        }
        m38209c(atc);
    }

    @C3248pc(aYR = C3248pc.C3250b.REMOVER, displayName = "Node Objects: AsteroidZones")
    @C5566aOg
    /* renamed from: d */
    public void mo21649d(AsteroidZone btVar) {
        switch (bFf().mo6893i(bdz)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, bdz, new Object[]{btVar}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, bdz, new Object[]{btVar}));
                break;
        }
        m38210c(btVar);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Z Rotation")
    @C5566aOg
    /* renamed from: de */
    public void mo21650de(float f) {
        switch (bFf().mo6893i(bcH)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, bcH, new Object[]{new Float(f)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, bcH, new Object[]{new Float(f)}));
                break;
        }
        m38222dd(f);
    }

    /* renamed from: e */
    public void mo21651e(StellarSystem jj) {
        switch (bFf().mo6893i(aBt)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, aBt, new Object[]{jj}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, aBt, new Object[]{jj}));
                break;
        }
        m38215d(jj);
    }

    @C3248pc(aYR = C3248pc.C3250b.ALTERNATE_ADDER, displayName = "Node Objects: HazardArea")
    @C5566aOg
    /* renamed from: e */
    public void mo21652e(HazardAreaType ass) {
        switch (bFf().mo6893i(bdt)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, bdt, new Object[]{ass}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, bdt, new Object[]{ass}));
                break;
        }
        m38216d(ass);
    }

    @C5566aOg
    /* renamed from: e */
    public void mo21653e(GateType mxVar) {
        switch (bFf().mo6893i(bcY)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, bcY, new Object[]{mxVar}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, bcY, new Object[]{mxVar}));
                break;
        }
        m38218d(mxVar);
    }

    @C3248pc(aYR = C3248pc.C3250b.REMOVER, displayName = "Node Objects: HazardArea")
    @C5566aOg
    /* renamed from: e */
    public void mo21654e(HazardArea pbVar) {
        switch (bFf().mo6893i(bdu)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, bdu, new Object[]{pbVar}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, bdu, new Object[]{pbVar}));
                break;
        }
        m38219d(pbVar);
    }

    @C3248pc(aYR = C3248pc.C3250b.REMOVER, displayName = "Taikopedia Entries")
    @C5566aOg
    /* renamed from: f */
    public void mo21655f(TaikopediaEntry aiz) {
        switch (bFf().mo6893i(bcI)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, bcI, new Object[]{aiz}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, bcI, new Object[]{aiz}));
                break;
        }
        m38223e(aiz);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Name")
    @C5566aOg
    /* renamed from: f */
    public void mo21656f(I18NString i18NString) {
        switch (bFf().mo6893i(f9018zU)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f9018zU, new Object[]{i18NString}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f9018zU, new Object[]{i18NString}));
                break;
        }
        m38226e(i18NString);
    }

    /* access modifiers changed from: package-private */
    @C5566aOg
    /* renamed from: g */
    public Set<Actor> mo21657g(StellarSystem jj) {
        switch (bFf().mo6893i(bdP)) {
            case 0:
                return null;
            case 2:
                return (Set) bFf().mo5606d(new aCE(this, bdP, new Object[]{jj}));
            case 3:
                bFf().mo5606d(new aCE(this, bdP, new Object[]{jj}));
                break;
        }
        return m38227f(jj);
    }

    @C3248pc(aYR = C3248pc.C3250b.REMOVER, displayName = "Node Objects: NPCZones")
    @C5566aOg
    /* renamed from: g */
    public void mo21658g(NPCZone fwVar) {
        switch (bFf().mo6893i(bdE)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, bdE, new Object[]{fwVar}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, bdE, new Object[]{fwVar}));
                break;
        }
        m38229f(fwVar);
    }

    @C3248pc(aYR = C3248pc.C3250b.ALTERNATE_ADDER, displayName = "Node Objects: Gate")
    @C5566aOg
    /* renamed from: g */
    public void mo21659g(GateType mxVar) {
        switch (bFf().mo6893i(bdj)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, bdj, new Object[]{mxVar}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, bdj, new Object[]{mxVar}));
                break;
        }
        m38230f(mxVar);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Handle")
    public String getHandle() {
        switch (bFf().mo6893i(f9002bP)) {
            case 0:
                return null;
            case 2:
                return (String) bFf().mo5606d(new aCE(this, f9002bP, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f9002bP, new Object[0]));
                break;
        }
        return m38186ar();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Handle")
    @C5566aOg
    public void setHandle(String str) {
        switch (bFf().mo6893i(f9003bQ)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f9003bQ, new Object[]{str}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f9003bQ, new Object[]{str}));
                break;
        }
        m38198b(str);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Position")
    public Vec3d getPosition() {
        switch (bFf().mo6893i(bcC)) {
            case 0:
                return null;
            case 2:
                return (Vec3d) bFf().mo5606d(new aCE(this, bcC, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, bcC, new Object[0]));
                break;
        }
        return m38142Zd();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Position")
    @C5566aOg
    public void setPosition(Vec3d ajr) {
        switch (bFf().mo6893i(f9014vp)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f9014vp, new Object[]{ajr}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f9014vp, new Object[]{ajr}));
                break;
        }
        m38207c(ajr);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Radius")
    public float getRadius() {
        switch (bFf().mo6893i(bcF)) {
            case 0:
                return 0.0f;
            case 2:
                return ((Float) bFf().mo5606d(new aCE(this, bcF, new Object[0]))).floatValue();
            case 3:
                bFf().mo5606d(new aCE(this, bcF, new Object[0]));
                break;
        }
        return m38144Zg();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Radius")
    @C5566aOg
    public void setRadius(float f) {
        switch (bFf().mo6893i(_f_setRadius_0020_0028F_0029V)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, _f_setRadius_0020_0028F_0029V, new Object[]{new Float(f)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, _f_setRadius_0020_0028F_0029V, new Object[]{new Float(f)}));
                break;
        }
        m38177aA(f);
    }

    /* renamed from: ip */
    public String mo21662ip() {
        switch (bFf().mo6893i(f9012vc)) {
            case 0:
                return null;
            case 2:
                return (String) bFf().mo5606d(new aCE(this, f9012vc, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f9012vc, new Object[0]));
                break;
        }
        return m38234io();
    }

    /* renamed from: ir */
    public String mo21663ir() {
        switch (bFf().mo6893i(f9013ve)) {
            case 0:
                return null;
            case 2:
                return (String) bFf().mo5606d(new aCE(this, f9013ve, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f9013ve, new Object[0]));
                break;
        }
        return m38235iq();
    }

    @C3248pc(aYR = C3248pc.C3250b.REMOVER, displayName = "Node Objects: Gate")
    @C5566aOg
    /* renamed from: j */
    public void mo21664j(Gate fFVar) {
        switch (bFf().mo6893i(bdk)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, bdk, new Object[]{fFVar}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, bdk, new Object[]{fFVar}));
                break;
        }
        m38233i(fFVar);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Name")
    /* renamed from: ke */
    public I18NString mo21665ke() {
        switch (bFf().mo6893i(f9017zT)) {
            case 0:
                return null;
            case 2:
                return (I18NString) bFf().mo5606d(new aCE(this, f9017zT, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f9017zT, new Object[0]));
                break;
        }
        return m38239kd();
    }

    @C3248pc(aYR = C3248pc.C3250b.REMOVER, displayName = "Node Objects: Station")
    @C5566aOg
    /* renamed from: m */
    public void mo21666m(Station bf) {
        switch (bFf().mo6893i(bdf)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, bdf, new Object[]{bf}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, bdf, new Object[]{bf}));
                break;
        }
        m38240l(bf);
    }

    public void push() {
        switch (bFf().mo6893i(_f_push_0020_0028_0029V)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, _f_push_0020_0028_0029V, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, _f_push_0020_0028_0029V, new Object[0]));
                break;
        }
        m38091Gd();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Node Access Improvement")
    /* renamed from: rL */
    public NodeAccessImprovementType mo21667rL() {
        switch (bFf().mo6893i(f8985MI)) {
            case 0:
                return null;
            case 2:
                return (NodeAccessImprovementType) bFf().mo5606d(new aCE(this, f8985MI, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f8985MI, new Object[0]));
                break;
        }
        return m38244rK();
    }

    /* renamed from: rP */
    public I18NString mo195rP() {
        switch (bFf().mo6893i(f8987MN)) {
            case 0:
                return null;
            case 2:
                return (I18NString) bFf().mo5606d(new aCE(this, f8987MN, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f8987MN, new Object[0]));
                break;
        }
        return m38245rO();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Icon")
    /* renamed from: sK */
    public Asset mo21668sK() {
        switch (bFf().mo6893i(f8991Ob)) {
            case 0:
                return null;
            case 2:
                return (Asset) bFf().mo5606d(new aCE(this, f8991Ob, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f8991Ob, new Object[0]));
                break;
        }
        return m38249sJ();
    }

    public String toString() {
        switch (bFf().mo6893i(_f_toString_0020_0028_0029Ljava_002flang_002fString_003b)) {
            case 0:
                return null;
            case 2:
                return (String) bFf().mo5606d(new aCE(this, _f_toString_0020_0028_0029Ljava_002flang_002fString_003b, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, _f_toString_0020_0028_0029Ljava_002flang_002fString_003b, new Object[0]));
                break;
        }
        return m38187au();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Icon")
    @C5566aOg
    /* renamed from: v */
    public void mo21672v(Asset tCVar) {
        switch (bFf().mo6893i(f8992Oc)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f8992Oc, new Object[]{tCVar}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f8992Oc, new Object[]{tCVar}));
                break;
        }
        m38251u(tCVar);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Description")
    /* renamed from: vW */
    public I18NString mo21673vW() {
        switch (bFf().mo6893i(f8994QE)) {
            case 0:
                return null;
            case 2:
                return (I18NString) bFf().mo5606d(new aCE(this, f8994QE, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f8994QE, new Object[0]));
                break;
        }
        return m38253vV();
    }

    @C0064Am(aul = "beb7c98ae1d4ce9f7f68944bd2ea0ed2", aum = 0)
    /* renamed from: au */
    private String m38187au() {
        return "Node: [" + m38184ao() + "]";
    }

    @C0064Am(aul = "49aaba70febe04515599534a3150b753", aum = 0)
    /* renamed from: ap */
    private UUID m38185ap() {
        return m38183an();
    }

    @C0064Am(aul = "c1b293ea6180f3f0e313773b0ef99138", aum = 0)
    /* renamed from: b */
    private void m38199b(UUID uuid) {
        m38176a(uuid);
    }

    /* renamed from: S */
    public void mo10S() {
        super.mo10S();
        for (C3423a put : C3423a.values()) {
            C1556Wo YZ = m38120YZ();
            NodeObjectHolder bVar = (NodeObjectHolder) bFf().mo6865M(NodeObjectHolder.class);
            bVar.mo21674a((NodeObjectHolder) null);
            YZ.put(put, bVar);
        }
        m38176a(UUID.randomUUID());
    }

    @C0064Am(aul = "b55b6f1c88718de06b19748969da961a", aum = 0)
    /* renamed from: Zb */
    private Space m38141Zb() {
        if (m38119YY() == null) {
            Space ea = (Space) bFf().mo6865M(Space.class);
            ea.mo1918nz(mo21665ke());
            m38159a(ea);
        }
        return m38119YY();
    }

    @C0064Am(aul = "e1bc6f99eb4a4245d4e065afaade7784", aum = 0)
    /* renamed from: b */
    private void m38188b(Space ea) {
        m38159a(ea);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Name")
    @C0064Am(aul = "5cf4b44b19a84321a6a91ad33a8663ea", aum = 0)
    /* renamed from: kd */
    private I18NString m38239kd() {
        return m38238kb();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Position")
    @C0064Am(aul = "6dc39158b529be7a7aa75aec63e2ecbb", aum = 0)
    /* renamed from: Zd */
    private Vec3d m38142Zd() {
        return m38118YX();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "PVP Allowed")
    @C0064Am(aul = "32fb451d447df6a3c89dec4daf9be65a", aum = 0)
    /* renamed from: Ze */
    private boolean m38143Ze() {
        return m38117YW();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Radius")
    @C0064Am(aul = "f9adc4d8634153e1683b84ebb6ee1446", aum = 0)
    /* renamed from: Zg */
    private float m38144Zg() {
        return m38097Ln();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Handle")
    @C0064Am(aul = "d566ea95fa3c490a1556d1ec6d0a3624", aum = 0)
    /* renamed from: ar */
    private String m38186ar() {
        return m38184ao();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Z Rotation")
    @C0064Am(aul = "1f105cf56ae2f09b7862c7fe77317eb2", aum = 0)
    /* renamed from: Zh */
    private float m38145Zh() {
        return m38116YV();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Render Asset")
    @C0064Am(aul = "00391d22175761374ba74f034e0e31d7", aum = 0)
    /* renamed from: Nt */
    private Asset m38102Nt() {
        return m38246rh();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Render Asset")
    @C0064Am(aul = "323b86bbee351bf1099edeef465e8230", aum = 0)
    /* renamed from: H */
    private void m38092H(Asset tCVar) {
        m38243r(tCVar);
    }

    @C3248pc(aYR = C3248pc.C3250b.ITERATOR, displayName = "Taikopedia Entries")
    @C0064Am(aul = "ef9dca2ef077e67850b64cbc6e013b4d", aum = 0)
    /* renamed from: RV */
    private List<TaikopediaEntry> m38108RV() {
        return Collections.unmodifiableList(m38096Ll());
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Block Item Disposal")
    @C0064Am(aul = "295c157479cee5ebdaf7a906991883a3", aum = 0)
    /* renamed from: Zj */
    private boolean m38146Zj() {
        return m38140Za();
    }

    @C0064Am(aul = "4a455a6393c9d764cd5f030141f6c0b7", aum = 0)
    /* renamed from: io */
    private String m38234io() {
        if (m38246rh() != null) {
            return m38246rh().getHandle();
        }
        return null;
    }

    @C0064Am(aul = "c3e569dc6807df86c5de8bf45e0ee8c0", aum = 0)
    /* renamed from: iq */
    private String m38235iq() {
        if (m38246rh() != null) {
            return m38246rh().getFile();
        }
        return null;
    }

    @aFW("Stellar System")
    @C0064Am(aul = "b78d3b9a3eafac1b4d673ab5b5311f63", aum = 0)
    /* renamed from: Nb */
    private StellarSystem m38101Nb() {
        return m38100MX();
    }

    @C0064Am(aul = "5699c6e04aa4c4942acdb7af2f6497f1", aum = 0)
    /* renamed from: d */
    private void m38215d(StellarSystem jj) {
        m38202c(jj);
    }

    @C0064Am(aul = "34b0f28d123e231475a50c2da9ee261c", aum = 0)
    /* renamed from: Zl */
    private Vec3d m38147Zl() {
        if (m38100MX() == null) {
            return new Vec3d(m38118YX());
        }
        return new Vec3d(m38118YX().x + ((double) m38100MX().mo3248gL().x), m38118YX().y + ((double) m38100MX().mo3248gL().y), m38118YX().z + ((double) m38100MX().mo3248gL().z));
    }

    @C0064Am(aul = "a2a38306665ce77bb79a2fab72dd87f0", aum = 0)
    /* renamed from: Gd */
    private void m38091Gd() {
        super.push();
    }

    @C0064Am(aul = "eb86e73c31a1e0102ab5d541c8c992e5", aum = 0)
    /* renamed from: Zn */
    private List<Actor> m38148Zn() {
        List<Actor> c = m38119YY().mo1900c(getPosition(), getRadius());
        Iterator<Actor> it = c.iterator();
        while (it.hasNext()) {
            if (m38152Zs().contains(it.next())) {
                it.remove();
            }
        }
        return c;
    }

    @C4034yP
    @C0064Am(aul = "856e41fe20fba5eff07bec39df805d51", aum = 0)
    @C2499fr
    @C1253SX
    /* renamed from: e */
    private void m38224e(Player aku, String str) {
        for (Actor next : m38149Zo()) {
            if (next instanceof Ship) {
                Controller hb = ((Ship) next).mo2998hb();
                if (hb == null) {
                    System.err.println("ERROR! Can not send a message the ship's controller is null");
                } else if (hb instanceof PlayerController) {
                    m38194b(aku, str, (PlayerController) hb);
                }
            }
        }
    }

    @C0064Am(aul = "2c3da941856e4db8911135f5ef5da31c", aum = 0)
    /* renamed from: G */
    private boolean m38090G(Actor cr) {
        return getPosition().mo9486aC(cr.getPosition()) < getRadius() * getRadius();
    }

    @C0064Am(aul = "98b673002e76cee67f6e558168c74a52", aum = 0)
    /* renamed from: I */
    private boolean m38093I(Actor cr) {
        Vec3f vec3f = null;
        if (cr instanceof Station) {
            vec3f = ((Station) cr).azP().mo4109rJ();
        } else if (cr instanceof Gate) {
            vec3f = ((Gate) cr).mo18388rJ();
        }
        if (vec3f == null) {
            return true;
        }
        if (getPosition().mo9486aC(cr.getPosition().mo9531q(vec3f)) < getRadius() * getRadius()) {
            return true;
        }
        return false;
    }

    @C0064Am(aul = "1a903e74e6b02b27f070e46cc195c777", aum = 0)
    /* renamed from: Zp */
    private Set<Actor> m38150Zp() {
        HashSet hashSet = new HashSet();
        for (NodeObjectHolder dnp : m38120YZ().values()) {
            hashSet.addAll(dnp.dnp());
        }
        return hashSet;
    }

    @C0064Am(aul = "92cc0987c1d89d44c79f8bc55e307da9", aum = 0)
    /* renamed from: Zr */
    private Set<Actor> m38151Zr() {
        HashSet hashSet = new HashSet();
        hashSet.addAll(mo21611ZA());
        hashSet.addAll(mo21612ZE());
        hashSet.addAll(mo21614ZM());
        hashSet.addAll(mo21613ZI());
        return hashSet;
    }

    @C0064Am(aul = "d0c946d8d5741da9823c8904ba43284b", aum = 0)
    @Deprecated
    /* renamed from: Zt */
    private Set<Actor> m38153Zt() {
        NodeObjectHolder bVar = (NodeObjectHolder) m38120YZ().get(C3423a.STATIC);
        if (bVar == null) {
            return Collections.EMPTY_SET;
        }
        return bVar.dnp();
    }

    @C3248pc(aYR = C3248pc.C3250b.ITERATOR, displayName = "Node Objects: Static")
    @C0064Am(aul = "bcea2db79dfe3fcba6e9dbd754630280", aum = 0)
    @Deprecated
    /* renamed from: Zv */
    private Set<Actor> m38155Zv() {
        return Collections.unmodifiableSet(m38152Zs());
    }

    @C0064Am(aul = "41f187992c09ec6ace0dfa6e77d82404", aum = 0)
    /* renamed from: Zx */
    private Set<Station> m38156Zx() {
        NodeObjectHolder bVar = (NodeObjectHolder) m38120YZ().get(C3423a.STATION);
        if (bVar == null) {
            bVar = (NodeObjectHolder) bFf().mo6865M(NodeObjectHolder.class);
            bVar.mo21674a((NodeObjectHolder) null);
            m38120YZ().put(C3423a.STATION, bVar);
        }
        return bVar.dnp();
    }

    @C3248pc(aYR = C3248pc.C3250b.ITERATOR, displayName = "Node Objects: Station")
    @C0064Am(aul = "ccf86ff924609364a8950624b194f390", aum = 0)
    /* renamed from: Zz */
    private Set<Station> m38158Zz() {
        return Collections.unmodifiableSet(m38157Zy());
    }

    @C0064Am(aul = "0dbe7f73565919b6324ef4f75c66ae63", aum = 0)
    /* renamed from: ZB */
    private Set<Gate> m38121ZB() {
        NodeObjectHolder bVar = (NodeObjectHolder) m38120YZ().get(C3423a.GATE);
        if (bVar == null) {
            bVar = (NodeObjectHolder) bFf().mo6865M(NodeObjectHolder.class);
            bVar.mo21674a((NodeObjectHolder) null);
            m38120YZ().put(C3423a.GATE, bVar);
        }
        return bVar.dnp();
    }

    @C3248pc(aYR = C3248pc.C3250b.ITERATOR, displayName = "Node Objects: Gate")
    @C0064Am(aul = "1c5a01b70704c8b522d65df35cf4bf3e", aum = 0)
    /* renamed from: ZD */
    private Set<Gate> m38123ZD() {
        return Collections.unmodifiableSet(m38122ZC());
    }

    @C0064Am(aul = "634809e454d0e1c0773625a5ca30250a", aum = 0)
    /* renamed from: ZF */
    private Set<Scenery> m38124ZF() {
        NodeObjectHolder bVar = (NodeObjectHolder) m38120YZ().get(C3423a.SCENERY);
        if (bVar == null) {
            bVar = (NodeObjectHolder) bFf().mo6865M(NodeObjectHolder.class);
            bVar.mo21674a((NodeObjectHolder) null);
            m38120YZ().put(C3423a.SCENERY, bVar);
        }
        return bVar.dnp();
    }

    @C3248pc(aYR = C3248pc.C3250b.ITERATOR, displayName = "Node Objects: Scenery")
    @C0064Am(aul = "24b3a8756e4657a6417ad5cf2c0b6349", aum = 0)
    /* renamed from: ZH */
    private Set<Scenery> m38126ZH() {
        return Collections.unmodifiableSet(m38125ZG());
    }

    @C0064Am(aul = "4005589ac7ede9049f9990597a4f054e", aum = 0)
    /* renamed from: ZJ */
    private Set<HazardArea> m38127ZJ() {
        NodeObjectHolder bVar = (NodeObjectHolder) m38120YZ().get(C3423a.HAZARDAREA);
        if (bVar == null) {
            bVar = (NodeObjectHolder) bFf().mo6865M(NodeObjectHolder.class);
            bVar.mo21674a((NodeObjectHolder) null);
            m38120YZ().put(C3423a.HAZARDAREA, bVar);
        }
        return bVar.dnp();
    }

    @C3248pc(aYR = C3248pc.C3250b.ITERATOR, displayName = "Node Objects: HazardArea")
    @C0064Am(aul = "68f043864abe8457efe1fd62dd8cfda5", aum = 0)
    /* renamed from: ZL */
    private Set<HazardArea> m38129ZL() {
        return Collections.unmodifiableSet(m38128ZK());
    }

    @C0064Am(aul = "3c987ba8abefcf14b665aead5769192b", aum = 0)
    /* renamed from: ZN */
    private Set<AsteroidZone> m38130ZN() {
        return ((NodeObjectHolder) m38120YZ().get(C3423a.ASTEROIDZONE)).dnp();
    }

    @C3248pc(aYR = C3248pc.C3250b.ITERATOR, displayName = "Node Objects: AsteroidZones")
    @C0064Am(aul = "44f29c55c31461629ee4096e7b283f42", aum = 0)
    /* renamed from: ZP */
    private Set<AsteroidZone> m38132ZP() {
        return Collections.unmodifiableSet(m38131ZO());
    }

    @C0064Am(aul = "1a31df3950388377fa35e6e3a71e3e6c", aum = 0)
    /* renamed from: ZR */
    private Set<NPCZone> m38133ZR() {
        return ((NodeObjectHolder) m38120YZ().get(C3423a.NPCZONE)).dnp();
    }

    @C3248pc(aYR = C3248pc.C3250b.ITERATOR, displayName = "Node Objects: NPCZones")
    @C0064Am(aul = "feb10b4839d82f12fb7ee3bc998d72c9", aum = 0)
    /* renamed from: ZT */
    private Set<NPCZone> m38135ZT() {
        return Collections.unmodifiableSet(m38134ZS());
    }

    @C0064Am(aul = "0cd29c45d9d1975f011c97dfc9a2fca1", aum = 0)
    /* renamed from: ZV */
    private Set<ScriptableZone> m38136ZV() {
        return ((NodeObjectHolder) m38120YZ().get(C3423a.SCRIPTABLEZONE)).dnp();
    }

    @C3248pc(aYR = C3248pc.C3250b.ITERATOR, displayName = "Node Objects: ScriptableZones")
    @C0064Am(aul = "10f8b72481cf288552c8aece4f3c9511", aum = 0)
    /* renamed from: ZX */
    private Set<ScriptableZone> m38138ZX() {
        return Collections.unmodifiableSet(m38137ZW());
    }

    @C0064Am(aul = "91a53f6f539b7ac00abc38d4f736fcb7", aum = 0)
    /* renamed from: ZZ */
    private Set<Advertise> m38139ZZ() {
        return ((NodeObjectHolder) m38120YZ().get(C3423a.ADVERTISE)).dnp();
    }

    @C3248pc(aYR = C3248pc.C3250b.ITERATOR, displayName = "Node Objects: Advertise")
    @C0064Am(aul = "b816acf8eb32a29314546341860256e0", aum = 0)
    private Set<Advertise> aab() {
        return Collections.unmodifiableSet(aaa());
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Node Access Improvement")
    @C0064Am(aul = "953f46f481f877060c995eeca54bacdd", aum = 0)
    /* renamed from: rK */
    private NodeAccessImprovementType m38244rK() {
        return m38247rs();
    }

    @C0064Am(aul = "c7a624a20f522baa793e53521d76fe63", aum = 0)
    private Collection<aDR> aad() {
        return m38119YY().aae();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Database Category")
    @C0064Am(aul = "97207fcec542795127f2b9431d799dcf", aum = 0)
    /* renamed from: RX */
    private DatabaseCategory m38109RX() {
        return m38105RR();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Description")
    @C0064Am(aul = "90ae4da7d354afae84bec589f493d05a", aum = 0)
    /* renamed from: vV */
    private I18NString m38253vV() {
        return m38252vT();
    }

    @C0064Am(aul = "21b19515111da01a2275dd78331cdf67", aum = 0)
    /* renamed from: RT */
    private I18NString m38107RT() {
        return mo21673vW();
    }

    @C0064Am(aul = "13c8a07f63f9307f1739d549ec8f5445", aum = 0)
    /* renamed from: rO */
    private I18NString m38245rO() {
        return mo21665ke();
    }

    @C0064Am(aul = "7f8be7916bf9e46f242a43cdc919d178", aum = 0)
    /* renamed from: RZ */
    private String m38110RZ() {
        if (m38248sG() != null) {
            return m38248sG().getHandle();
        }
        return null;
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Icon")
    @C0064Am(aul = "e42032548bac53113e4375085cc0a0b0", aum = 0)
    /* renamed from: sJ */
    private Asset m38249sJ() {
        return m38248sG();
    }

    @C0064Am(aul = "5f5329deb31f1d62d527dd92a204b38e", aum = 0)
    /* renamed from: Sb */
    private String m38111Sb() {
        return m38104RQ().getHandle();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Pda Render")
    @C0064Am(aul = "0dd90ef01faddd4d58de07a65895ac7d", aum = 0)
    /* renamed from: Sd */
    private Asset m38112Sd() {
        return m38104RQ();
    }

    @C0064Am(aul = "48944d1a05a2cf188fd53390ba611478", aum = 0)
    /* renamed from: a */
    private void m38160a(C0665JT jt) {
        if (jt.mo3117j(1, 1, 1)) {
            C1556Wo YZ = m38120YZ();
            C3423a aVar = C3423a.SCRIPTABLEZONE;
            NodeObjectHolder bVar = (NodeObjectHolder) bFf().mo6865M(NodeObjectHolder.class);
            bVar.mo21674a((NodeObjectHolder) null);
            YZ.put(aVar, bVar);
        }
    }

    @aFW(cZD = true, value = "Gates")
    @C0064Am(aul = "592828907173d971d8256a29da7ed21c", aum = 0)
    /* renamed from: V */
    private Set<aDJ> m38114V(Player aku) {
        return aku.dyl().mo11984g(this);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Pda MP Multiplier")
    @C0064Am(aul = "397af32d0b640cdd0518892fcab62674", aum = 0)
    /* renamed from: Sf */
    private float m38113Sf() {
        return m38106RS();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Pda MP Multiplier")
    @C0064Am(aul = "61276fe8865a567219dc8fdb2fc237a6", aum = 0)
    /* renamed from: cy */
    private void m38214cy(float f) {
        m38213cx(f);
    }

    /* renamed from: a.rP$a */
    enum C3423a {
        STATIC,
        ADVERTISE,
        NPCZONE,
        ASTEROIDZONE,
        SCRIPTABLEZONE,
        STATION,
        GATE,
        SCENERY,
        HAZARDAREA
    }

    @C6485anp
    @C5511aMd
    /* renamed from: a.rP$b */
    /* compiled from: a */
    public static class NodeObjectHolder<T extends Actor> extends aDJ implements C0468GU, C1616Xf {
        public static final int _m_fieldCount = 0;
        public static final C5663aRz[] _m_fields = null;
        public static final int _m_methodCount = 0;
        public static final C2491fm[] _m_methods = null;
        /* renamed from: bL */
        public static final C5663aRz f9020bL = null;
        /* renamed from: bM */
        public static final C5663aRz f9021bM = null;
        /* renamed from: bN */
        public static final C2491fm f9022bN = null;
        /* renamed from: bO */
        public static final C2491fm f9023bO = null;
        /* renamed from: bP */
        public static final C2491fm f9024bP = null;
        /* renamed from: bQ */
        public static final C2491fm f9025bQ = null;
        public static final C5663aRz iyD = null;
        public static final C2491fm iyE = null;
        public static final C2491fm iyF = null;
        public static final C2491fm iyG = null;
        public static final long serialVersionUID = 0;
        public static C6494any ___iScriptClass;
        @C0064Am(aul = "9cb48776ad4fac3eebc7234995150e33", aum = 0)

        /* renamed from: bK */
        private static UUID f9019bK;
        @C0064Am(aul = "00693877b5ccf0b9c43b215825c0fc1b", aum = 2)
        private static C2686iZ<T> dqO;
        @C0064Am(aul = "f0cd3c6860f38db18cd147abfbfaac2b", aum = 1)
        private static String handle;

        static {
            m38340V();
        }

        private NodeObjectHolder() {
            super((C5540aNg) null);
            super.mo10S();
        }

        public NodeObjectHolder(C5540aNg ang) {
            super(ang);
        }

        /* synthetic */ NodeObjectHolder(NodeObjectHolder bVar) {
            super((C5540aNg) null);
            super._m_script_init(bVar);
        }

        /* renamed from: V */
        static void m38340V() {
            serialVersionUID = (long) 1;
            _m_fieldCount = aDJ._m_fieldCount + 3;
            _m_methodCount = aDJ._m_methodCount + 7;
            int i = aDJ._m_fieldCount;
            C5663aRz[] arzArr = new C5663aRz[(i + 3)];
            C5663aRz b = C5640aRc.m17844b(NodeObjectHolder.class, "9cb48776ad4fac3eebc7234995150e33", i);
            f9020bL = b;
            arzArr[i] = b;
            int i2 = i + 1;
            C5663aRz b2 = C5640aRc.m17844b(NodeObjectHolder.class, "f0cd3c6860f38db18cd147abfbfaac2b", i2);
            f9021bM = b2;
            arzArr[i2] = b2;
            int i3 = i2 + 1;
            C5663aRz b3 = C5640aRc.m17844b(NodeObjectHolder.class, "00693877b5ccf0b9c43b215825c0fc1b", i3);
            iyD = b3;
            arzArr[i3] = b3;
            int i4 = i3 + 1;
            _m_fields = arzArr;
            C1634Xv.m11725a((Object[]) aDJ._m_fields, (Object[]) _m_fields);
            int i5 = aDJ._m_methodCount;
            C2491fm[] fmVarArr = new C2491fm[(i5 + 7)];
            C2491fm a = C4105zY.m41624a(NodeObjectHolder.class, "fe407fdd23e62829c605d680f627f464", i5);
            f9023bO = a;
            fmVarArr[i5] = a;
            int i6 = i5 + 1;
            C2491fm a2 = C4105zY.m41624a(NodeObjectHolder.class, "605f130c1d1e3187df627a648a7c825a", i6);
            iyE = a2;
            fmVarArr[i6] = a2;
            int i7 = i6 + 1;
            C2491fm a3 = C4105zY.m41624a(NodeObjectHolder.class, "731dfffe74766082c86d211a6cb7db16", i7);
            iyF = a3;
            fmVarArr[i7] = a3;
            int i8 = i7 + 1;
            C2491fm a4 = C4105zY.m41624a(NodeObjectHolder.class, "ce5ca9e97fc6abed98009fcbd6f7a5eb", i8);
            iyG = a4;
            fmVarArr[i8] = a4;
            int i9 = i8 + 1;
            C2491fm a5 = C4105zY.m41624a(NodeObjectHolder.class, "9bc262b79b54529dccd48c18fc175aa5", i9);
            f9025bQ = a5;
            fmVarArr[i9] = a5;
            int i10 = i9 + 1;
            C2491fm a6 = C4105zY.m41624a(NodeObjectHolder.class, "c19ef4b05bdbaf5140e1c5ee680d1026", i10);
            f9024bP = a6;
            fmVarArr[i10] = a6;
            int i11 = i10 + 1;
            C2491fm a7 = C4105zY.m41624a(NodeObjectHolder.class, "17910f6a87dda06a320e5562f246ca28", i11);
            f9022bN = a7;
            fmVarArr[i11] = a7;
            int i12 = i11 + 1;
            _m_methods = fmVarArr;
            C1634Xv.m11725a((Object[]) aDJ._m_methods, (Object[]) _m_methods);
            ___iScriptClass = aUO.m18566a(NodeObjectHolder.class, C0721KN.class, _m_fields, _m_methods);
        }

        /* renamed from: a */
        private void m38341a(String str) {
            bFf().mo5608dq().mo3197f(f9021bM, str);
        }

        /* renamed from: a */
        private void m38342a(UUID uuid) {
            bFf().mo5608dq().mo3197f(f9020bL, uuid);
        }

        /* renamed from: an */
        private UUID m38343an() {
            return (UUID) bFf().mo5608dq().mo3214p(f9020bL);
        }

        /* renamed from: ao */
        private String m38344ao() {
            return (String) bFf().mo5608dq().mo3214p(f9021bM);
        }

        /* renamed from: ap */
        private void m38346ap(C2686iZ iZVar) {
            bFf().mo5608dq().mo3197f(iyD, iZVar);
        }

        /* renamed from: c */
        private void m38350c(UUID uuid) {
            switch (bFf().mo6893i(f9023bO)) {
                case 0:
                    return;
                case 2:
                    bFf().mo5606d(new aCE(this, f9023bO, new Object[]{uuid}));
                    return;
                case 3:
                    bFf().mo5606d(new aCE(this, f9023bO, new Object[]{uuid}));
                    break;
            }
            m38349b(uuid);
        }

        private C2686iZ dnn() {
            return (C2686iZ) bFf().mo5608dq().mo3214p(iyD);
        }

        /* renamed from: T */
        public C6494any mo11T() {
            return ___iScriptClass;
        }

        /* renamed from: U */
        public C2491fm[] mo12U() {
            return _m_methods;
        }

        /* renamed from: W */
        public Object mo13W() {
            return new C0721KN(this);
        }

        /* renamed from: a */
        public Object mo14a(C0495Gr gr) {
            Object[] args = gr.getArgs();
            switch (gr.mo2417hq() - aDJ._m_methodCount) {
                case 0:
                    m38349b((UUID) args[0]);
                    return null;
                case 1:
                    return new Boolean(m38351cr((Actor) args[0]));
                case 2:
                    return new Boolean(m38352ct((Actor) args[0]));
                case 3:
                    return dno();
                case 4:
                    m38348b((String) args[0]);
                    return null;
                case 5:
                    return m38347ar();
                case 6:
                    return m38345ap();
                default:
                    return super.mo14a(gr);
            }
        }

        /* renamed from: a */
        public void mo15a(Collection collection, C0495Gr gr) {
            super.mo15a(collection, gr);
        }

        /* renamed from: aq */
        public UUID mo18aq() {
            switch (bFf().mo6893i(f9022bN)) {
                case 0:
                    return null;
                case 2:
                    return (UUID) bFf().mo5606d(new aCE(this, f9022bN, new Object[0]));
                case 3:
                    bFf().mo5606d(new aCE(this, f9022bN, new Object[0]));
                    break;
            }
            return m38345ap();
        }

        /* renamed from: c */
        public C5663aRz[] mo25c() {
            return _m_fields;
        }

        /* access modifiers changed from: package-private */
        /* renamed from: cs */
        public boolean mo21675cs(T t) {
            switch (bFf().mo6893i(iyE)) {
                case 0:
                    return false;
                case 2:
                    return ((Boolean) bFf().mo5606d(new aCE(this, iyE, new Object[]{t}))).booleanValue();
                case 3:
                    bFf().mo5606d(new aCE(this, iyE, new Object[]{t}));
                    break;
            }
            return m38351cr(t);
        }

        /* access modifiers changed from: package-private */
        /* renamed from: cu */
        public boolean mo21676cu(T t) {
            switch (bFf().mo6893i(iyF)) {
                case 0:
                    return false;
                case 2:
                    return ((Boolean) bFf().mo5606d(new aCE(this, iyF, new Object[]{t}))).booleanValue();
                case 3:
                    bFf().mo5606d(new aCE(this, iyF, new Object[]{t}));
                    break;
            }
            return m38352ct(t);
        }

        /* access modifiers changed from: package-private */
        public C2686iZ<T> dnp() {
            switch (bFf().mo6893i(iyG)) {
                case 0:
                    return null;
                case 2:
                    return (C2686iZ) bFf().mo5606d(new aCE(this, iyG, new Object[0]));
                case 3:
                    bFf().mo5606d(new aCE(this, iyG, new Object[0]));
                    break;
            }
            return dno();
        }

        public String getHandle() {
            switch (bFf().mo6893i(f9024bP)) {
                case 0:
                    return null;
                case 2:
                    return (String) bFf().mo5606d(new aCE(this, f9024bP, new Object[0]));
                case 3:
                    bFf().mo5606d(new aCE(this, f9024bP, new Object[0]));
                    break;
            }
            return m38347ar();
        }

        public void setHandle(String str) {
            switch (bFf().mo6893i(f9025bQ)) {
                case 0:
                    return;
                case 2:
                    bFf().mo5606d(new aCE(this, f9025bQ, new Object[]{str}));
                    return;
                case 3:
                    bFf().mo5606d(new aCE(this, f9025bQ, new Object[]{str}));
                    break;
            }
            m38348b(str);
        }

        /* renamed from: S */
        private void m38339S() {
            super.mo10S();
            m38342a(UUID.randomUUID());
            m38341a("NodeObjectHolder" + cWm());
        }

        /* access modifiers changed from: package-private */
        /* renamed from: a */
        public /* synthetic */ void mo21674a(NodeObjectHolder bVar) {
            m38339S();
        }

        @C0064Am(aul = "fe407fdd23e62829c605d680f627f464", aum = 0)
        /* renamed from: b */
        private void m38349b(UUID uuid) {
            m38342a(uuid);
        }

        @C0064Am(aul = "605f130c1d1e3187df627a648a7c825a", aum = 0)
        /* renamed from: cr */
        private boolean m38351cr(T t) {
            return dnn().add(t);
        }

        @C0064Am(aul = "731dfffe74766082c86d211a6cb7db16", aum = 0)
        /* renamed from: ct */
        private boolean m38352ct(T t) {
            return dnn().remove(t);
        }

        @C0064Am(aul = "ce5ca9e97fc6abed98009fcbd6f7a5eb", aum = 0)
        private C2686iZ<T> dno() {
            return dnn();
        }

        @C0064Am(aul = "9bc262b79b54529dccd48c18fc175aa5", aum = 0)
        /* renamed from: b */
        private void m38348b(String str) {
            m38341a(str);
        }

        @C0064Am(aul = "c19ef4b05bdbaf5140e1c5ee680d1026", aum = 0)
        /* renamed from: ar */
        private String m38347ar() {
            return m38344ao();
        }

        @C0064Am(aul = "17910f6a87dda06a320e5562f246ca28", aum = 0)
        /* renamed from: ap */
        private UUID m38345ap() {
            return m38343an();
        }
    }
}
