package game.script.space;

import game.network.message.externalizable.aCE;
import game.script.nls.NLSLoot;
import game.script.nls.NLSManager;
import logic.baa.C1616Xf;
import logic.baa.C1634Xv;
import logic.baa.C5511aMd;
import logic.baa.C6485anp;
import logic.data.mbean.C6779atX;
import logic.res.code.C5640aRc;
import logic.res.code.C5663aRz;
import logic.res.code.C6494any;
import logic.res.html.C2491fm;
import p001a.*;

import java.util.Collection;

@C2712iu(mo19786Bt = LootType.class)
@C5511aMd
@C6485anp
/* renamed from: a.kC */
/* compiled from: a */
public class AsteroidLoot extends Loot implements C1616Xf {

    /* renamed from: Pf */
    public static final C2491fm f8407Pf = null;
    public static final int _m_fieldCount = 0;
    public static final C5663aRz[] _m_fields = null;
    public static final int _m_methodCount = 0;
    public static final C2491fm[] _m_methods = null;
    public static final C5663aRz auI = null;
    public static final C2491fm auJ = null;
    public static final long serialVersionUID = 0;
    public static C6494any ___iScriptClass;
    @C0064Am(aul = "27d1d3b99b9c8940f4aea56b35383b89", aum = 0)
    private static AsteroidType auH;

    static {
        m34300V();
    }

    public AsteroidLoot() {
        super((C5540aNg) null);
        super.mo10S();
    }

    public AsteroidLoot(C5540aNg ang) {
        super(ang);
    }

    public AsteroidLoot(AsteroidLootType agm) {
        super((C5540aNg) null);
        super._m_script_init(agm);
    }

    /* renamed from: V */
    static void m34300V() {
        serialVersionUID = (long) 1;
        _m_fieldCount = Loot._m_fieldCount + 1;
        _m_methodCount = Loot._m_methodCount + 2;
        int i = Loot._m_fieldCount;
        C5663aRz[] arzArr = new C5663aRz[(i + 1)];
        C5663aRz b = C5640aRc.m17844b(AsteroidLoot.class, "27d1d3b99b9c8940f4aea56b35383b89", i);
        auI = b;
        arzArr[i] = b;
        int i2 = i + 1;
        _m_fields = arzArr;
        C1634Xv.m11725a((Object[]) Loot._m_fields, (Object[]) _m_fields);
        int i3 = Loot._m_methodCount;
        C2491fm[] fmVarArr = new C2491fm[(i3 + 2)];
        C2491fm a = C4105zY.m41624a(AsteroidLoot.class, "99e9e471e2bd3e84b34fdb9fef889a43", i3);
        auJ = a;
        fmVarArr[i3] = a;
        int i4 = i3 + 1;
        C2491fm a2 = C4105zY.m41624a(AsteroidLoot.class, "03499bb5b45b75b0333bb32494c95bbd", i4);
        f8407Pf = a2;
        fmVarArr[i4] = a2;
        int i5 = i4 + 1;
        _m_methods = fmVarArr;
        C1634Xv.m11725a((Object[]) Loot._m_methods, (Object[]) _m_methods);
        ___iScriptClass = aUO.m18566a(AsteroidLoot.class, C6779atX.class, _m_fields, _m_methods);
    }

    /* renamed from: KF */
    private AsteroidType m34299KF() {
        return (AsteroidType) bFf().mo5608dq().mo3214p(auI);
    }

    /* renamed from: a */
    private void m34301a(AsteroidType aqn) {
        bFf().mo5608dq().mo3197f(auI, aqn);
    }

    /* renamed from: S */
    public void mo10S() {
        super.mo10S();
    }

    /* renamed from: T */
    public C6494any mo11T() {
        return ___iScriptClass;
    }

    /* renamed from: U */
    public C2491fm[] mo12U() {
        return _m_methods;
    }

    /* renamed from: W */
    public Object mo13W() {
        return new C6779atX(this);
    }

    /* renamed from: a */
    public Object mo14a(C0495Gr gr) {
        Object[] args = gr.getArgs();
        switch (gr.mo2417hq() - Loot._m_methodCount) {
            case 0:
                m34302b((AsteroidType) args[0]);
                return null;
            case 1:
                return m34303uV();
            default:
                return super.mo14a(gr);
        }
    }

    /* renamed from: a */
    public void mo15a(Collection collection, C0495Gr gr) {
        super.mo15a(collection, gr);
    }

    /* renamed from: c */
    public void mo20020c(AsteroidType aqn) {
        switch (bFf().mo6893i(auJ)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, auJ, new Object[]{aqn}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, auJ, new Object[]{aqn}));
                break;
        }
        m34302b(aqn);
    }

    /* renamed from: c */
    public C5663aRz[] mo25c() {
        return _m_fields;
    }

    public String getName() {
        switch (bFf().mo6893i(f8407Pf)) {
            case 0:
                return null;
            case 2:
                return (String) bFf().mo5606d(new aCE(this, f8407Pf, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f8407Pf, new Object[0]));
                break;
        }
        return m34303uV();
    }

    /* renamed from: a */
    public void mo20019a(AsteroidLootType agm) {
        super.mo8585h(agm);
    }

    @C0064Am(aul = "99e9e471e2bd3e84b34fdb9fef889a43", aum = 0)
    /* renamed from: b */
    private void m34302b(AsteroidType aqn) {
        m34301a(aqn);
    }

    @C0064Am(aul = "03499bb5b45b75b0333bb32494c95bbd", aum = 0)
    /* renamed from: uV */
    private String m34303uV() {
        NLSLoot avt = (NLSLoot) ala().aIY().mo6310c(NLSManager.C1472a.LOOT);
        Object[] objArr = new Object[0];
        if (m34299KF() != null) {
            objArr = new Object[]{m34299KF().mo11089ke().get()};
        }
        return C5956adg.format(avt.dDj().get(), objArr);
    }
}
