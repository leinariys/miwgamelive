package game.script.space;

import com.hoplon.geometry.Vec3f;
import game.geometry.Vec3d;
import game.network.message.externalizable.aCE;
import game.network.message.serializable.C2686iZ;
import game.network.message.serializable.C3438ra;
import game.script.Actor;
import game.script.TaikodomObject;
import game.script.pda.DatabaseCategory;
import game.script.pda.TaikopediaEntry;
import game.script.player.Player;
import game.script.resource.Asset;
import game.script.ship.Ship;
import game.script.spacezone.AsteroidZone;
import game.script.spacezone.NPCZone;
import logic.baa.*;
import logic.data.mbean.aIR;
import logic.res.code.C5640aRc;
import logic.res.code.C5663aRz;
import logic.res.code.C6494any;
import logic.res.html.C2491fm;
import p001a.*;
import taikodom.infra.script.I18NString;

import java.util.*;

@C5829abJ("1.0.1")
@C5511aMd
@C6485anp
/* renamed from: a.Jj */
/* compiled from: a */
public class StellarSystem extends TaikodomObject implements C0468GU, C0943Nq, C1616Xf, C4068yr {

    /* renamed from: Lm */
    public static final C2491fm f829Lm = null;

    /* renamed from: MN */
    public static final C2491fm f830MN = null;

    /* renamed from: NY */
    public static final C5663aRz f831NY = null;

    /* renamed from: Ob */
    public static final C2491fm f832Ob = null;

    /* renamed from: Oc */
    public static final C2491fm f833Oc = null;

    /* renamed from: QA */
    public static final C5663aRz f834QA = null;

    /* renamed from: QE */
    public static final C2491fm f835QE = null;

    /* renamed from: QF */
    public static final C2491fm f836QF = null;
    public static final C2491fm _f_toString_0020_0028_0029Ljava_002flang_002fString_003b = null;
    public static final int _m_fieldCount = 0;
    public static final C5663aRz[] _m_fields = null;
    public static final int _m_methodCount = 0;
    public static final C2491fm[] _m_methods = null;
    public static final C2491fm aKm = null;
    public static final C5663aRz aMg = null;
    public static final C5663aRz aMh = null;
    public static final C5663aRz aMi = null;
    public static final C2491fm aMj = null;
    public static final C2491fm aMk = null;
    public static final C2491fm aMl = null;
    public static final C2491fm aMn = null;
    public static final C2491fm aMo = null;
    public static final C2491fm aMp = null;
    public static final C2491fm aMq = null;
    public static final C2491fm aMr = null;
    public static final C2491fm aMs = null;
    public static final C2491fm aMu = null;
    public static final C2491fm aMv = null;
    public static final C5663aRz awd = null;
    /* renamed from: bL */
    public static final C5663aRz f838bL = null;
    /* renamed from: bM */
    public static final C5663aRz f839bM = null;
    /* renamed from: bN */
    public static final C2491fm f840bN = null;
    /* renamed from: bO */
    public static final C2491fm f841bO = null;
    /* renamed from: bP */
    public static final C2491fm f842bP = null;
    /* renamed from: bQ */
    public static final C2491fm f843bQ = null;
    public static final C2491fm bcI = null;
    public static final C5663aRz bcu = null;
    public static final C2491fm coL = null;
    public static final C5663aRz diT = null;
    public static final C5663aRz diV = null;
    public static final C2491fm diW = null;
    public static final C2491fm diX = null;
    public static final C2491fm diY = null;
    public static final C2491fm diZ = null;
    public static final C2491fm dja = null;
    public static final C2491fm djb = null;
    public static final C2491fm djc = null;
    public static final C2491fm djd = null;
    public static final C2491fm dje = null;
    public static final C2491fm djf = null;
    public static final C2491fm djg = null;
    public static final C2491fm djh = null;
    public static final C2491fm dji = null;
    public static final C2491fm djj = null;
    public static final C2491fm djk = null;
    public static final long serialVersionUID = 0;
    /* renamed from: zQ */
    public static final C5663aRz f851zQ = null;
    /* renamed from: zT */
    public static final C2491fm f852zT = null;
    /* renamed from: zU */
    public static final C2491fm f853zU = null;
    public static C6494any ___iScriptClass;
    @C0064Am(aul = "4d0a7d05dbfdd4329aae562c57f8777f", aum = 11)

    /* renamed from: bK */
    private static UUID f837bK;
    @C0064Am(aul = "6185197333bcd516dd9a325e05f1fabe", aum = 0)
    @C5566aOg
    private static C2686iZ<Actor> diS;
    @C0064Am(aul = "1ae4ad5225aefe91cb50aeacaba4bfc4", aum = 8)
    private static C3438ra<Node> diU;
    @C0064Am(aul = "3b021bb7d61c4c6d886b8fa9448a43c1", aum = 7)
    private static String handle;
    @C0064Am(aul = "c1b3de45147a9764f07138f9e02bf550", aum = 2)

    /* renamed from: jS */
    private static DatabaseCategory f844jS;
    @C0064Am(aul = "6567c00e58a87d4c27abae20c5438c47", aum = 3)

    /* renamed from: jT */
    private static C3438ra<TaikopediaEntry> f845jT;
    @C0064Am(aul = "6f2d82f4152f7e69c7282fdf04048aac", aum = 6)

    /* renamed from: jU */
    private static Asset f846jU;
    @C0064Am(aul = "d116df0429b121c3465bf41e174a1c0e", aum = 10)

    /* renamed from: jV */
    private static float f847jV;
    @C0064Am(aul = "9b72275a2aa520fbb9a9cf37ec811b52", aum = 5)

    /* renamed from: jn */
    private static Asset f848jn;
    @C0064Am(aul = "0308192860363d23896020a9e6622160", aum = 4)

    /* renamed from: nh */
    private static I18NString f849nh;
    @C0064Am(aul = "f6a359289d0cfef6958f30d326386526", aum = 9)
    private static Vec3f position;
    @C0064Am(aul = "cf0653ea8764e7e7daa5bc49454b1d76", aum = 1)

    /* renamed from: zP */
    private static I18NString f850zP;

    static {
        m5927V();
    }

    public StellarSystem() {
        super((C5540aNg) null);
        super.mo10S();
    }

    public StellarSystem(C5540aNg ang) {
        super(ang);
    }

    /* renamed from: V */
    static void m5927V() {
        serialVersionUID = (long) 1;
        _m_fieldCount = TaikodomObject._m_fieldCount + 12;
        _m_methodCount = TaikodomObject._m_methodCount + 42;
        int i = TaikodomObject._m_fieldCount;
        C5663aRz[] arzArr = new C5663aRz[(i + 12)];
        C5663aRz b = C5640aRc.m17844b(StellarSystem.class, "6185197333bcd516dd9a325e05f1fabe", i);
        diT = b;
        arzArr[i] = b;
        int i2 = i + 1;
        C5663aRz b2 = C5640aRc.m17844b(StellarSystem.class, "cf0653ea8764e7e7daa5bc49454b1d76", i2);
        f851zQ = b2;
        arzArr[i2] = b2;
        int i3 = i2 + 1;
        C5663aRz b3 = C5640aRc.m17844b(StellarSystem.class, "c1b3de45147a9764f07138f9e02bf550", i3);
        aMh = b3;
        arzArr[i3] = b3;
        int i4 = i3 + 1;
        C5663aRz b4 = C5640aRc.m17844b(StellarSystem.class, "6567c00e58a87d4c27abae20c5438c47", i4);
        awd = b4;
        arzArr[i4] = b4;
        int i5 = i4 + 1;
        C5663aRz b5 = C5640aRc.m17844b(StellarSystem.class, "0308192860363d23896020a9e6622160", i5);
        f834QA = b5;
        arzArr[i5] = b5;
        int i6 = i5 + 1;
        C5663aRz b6 = C5640aRc.m17844b(StellarSystem.class, "9b72275a2aa520fbb9a9cf37ec811b52", i6);
        f831NY = b6;
        arzArr[i6] = b6;
        int i7 = i6 + 1;
        C5663aRz b7 = C5640aRc.m17844b(StellarSystem.class, "6f2d82f4152f7e69c7282fdf04048aac", i7);
        aMg = b7;
        arzArr[i7] = b7;
        int i8 = i7 + 1;
        C5663aRz b8 = C5640aRc.m17844b(StellarSystem.class, "3b021bb7d61c4c6d886b8fa9448a43c1", i8);
        f839bM = b8;
        arzArr[i8] = b8;
        int i9 = i8 + 1;
        C5663aRz b9 = C5640aRc.m17844b(StellarSystem.class, "1ae4ad5225aefe91cb50aeacaba4bfc4", i9);
        diV = b9;
        arzArr[i9] = b9;
        int i10 = i9 + 1;
        C5663aRz b10 = C5640aRc.m17844b(StellarSystem.class, "f6a359289d0cfef6958f30d326386526", i10);
        bcu = b10;
        arzArr[i10] = b10;
        int i11 = i10 + 1;
        C5663aRz b11 = C5640aRc.m17844b(StellarSystem.class, "d116df0429b121c3465bf41e174a1c0e", i11);
        aMi = b11;
        arzArr[i11] = b11;
        int i12 = i11 + 1;
        C5663aRz b12 = C5640aRc.m17844b(StellarSystem.class, "4d0a7d05dbfdd4329aae562c57f8777f", i12);
        f838bL = b12;
        arzArr[i12] = b12;
        int i13 = i12 + 1;
        _m_fields = arzArr;
        C1634Xv.m11725a((Object[]) TaikodomObject._m_fields, (Object[]) _m_fields);
        int i14 = TaikodomObject._m_methodCount;
        C2491fm[] fmVarArr = new C2491fm[(i14 + 42)];
        C2491fm a = C4105zY.m41624a(StellarSystem.class, "4dff90fecf20662a8d84fe65fa82d570", i14);
        _f_toString_0020_0028_0029Ljava_002flang_002fString_003b = a;
        fmVarArr[i14] = a;
        int i15 = i14 + 1;
        C2491fm a2 = C4105zY.m41624a(StellarSystem.class, "911a3fdba2a7137f522fb00e0de35d7c", i15);
        f852zT = a2;
        fmVarArr[i15] = a2;
        int i16 = i15 + 1;
        C2491fm a3 = C4105zY.m41624a(StellarSystem.class, "816d7fe3cd89bbf610724bf59fc36d2d", i16);
        f853zU = a3;
        fmVarArr[i16] = a3;
        int i17 = i16 + 1;
        C2491fm a4 = C4105zY.m41624a(StellarSystem.class, "91bf721792def4642a5541776a92200b", i17);
        f840bN = a4;
        fmVarArr[i17] = a4;
        int i18 = i17 + 1;
        C2491fm a5 = C4105zY.m41624a(StellarSystem.class, "342d04e1615d83c44c203aeadb29276e", i18);
        f841bO = a5;
        fmVarArr[i18] = a5;
        int i19 = i18 + 1;
        C2491fm a6 = C4105zY.m41624a(StellarSystem.class, "2799f320b4cb6a253573f381646fbcf3", i19);
        diW = a6;
        fmVarArr[i19] = a6;
        int i20 = i19 + 1;
        C2491fm a7 = C4105zY.m41624a(StellarSystem.class, "f5af94e5903927322cb9c134cfe2b413", i20);
        f842bP = a7;
        fmVarArr[i20] = a7;
        int i21 = i20 + 1;
        C2491fm a8 = C4105zY.m41624a(StellarSystem.class, "5034205ec7e0365a7e7cdae052faeaa4", i21);
        f843bQ = a8;
        fmVarArr[i21] = a8;
        int i22 = i21 + 1;
        C2491fm a9 = C4105zY.m41624a(StellarSystem.class, "276928fc575a22dec11dc03a5b7fdee7", i22);
        diX = a9;
        fmVarArr[i22] = a9;
        int i23 = i22 + 1;
        C2491fm a10 = C4105zY.m41624a(StellarSystem.class, "957d72f8f6bb97e44f975272033036fc", i23);
        diY = a10;
        fmVarArr[i23] = a10;
        int i24 = i23 + 1;
        C2491fm a11 = C4105zY.m41624a(StellarSystem.class, "b223f208d1b891e7300ecaee9f7aeb28", i24);
        diZ = a11;
        fmVarArr[i24] = a11;
        int i25 = i24 + 1;
        C2491fm a12 = C4105zY.m41624a(StellarSystem.class, "34c3bb9ef17b294a80e626c4239c008d", i25);
        dja = a12;
        fmVarArr[i25] = a12;
        int i26 = i25 + 1;
        C2491fm a13 = C4105zY.m41624a(StellarSystem.class, "32560f5e8c203e5a62533db433e2761f", i26);
        djb = a13;
        fmVarArr[i26] = a13;
        int i27 = i26 + 1;
        C2491fm a14 = C4105zY.m41624a(StellarSystem.class, "788bdc9c05e066348c32810596cdbba2", i27);
        djc = a14;
        fmVarArr[i27] = a14;
        int i28 = i27 + 1;
        C2491fm a15 = C4105zY.m41624a(StellarSystem.class, "6e47e2adbf506decf52de0cdd7104e7c", i28);
        djd = a15;
        fmVarArr[i28] = a15;
        int i29 = i28 + 1;
        C2491fm a16 = C4105zY.m41624a(StellarSystem.class, "791a66250bd3dd3bc0348523f744194a", i29);
        dje = a16;
        fmVarArr[i29] = a16;
        int i30 = i29 + 1;
        C2491fm a17 = C4105zY.m41624a(StellarSystem.class, "cf3d95070cfba2a571d24e49d1d35c64", i30);
        djf = a17;
        fmVarArr[i30] = a17;
        int i31 = i30 + 1;
        C2491fm a18 = C4105zY.m41624a(StellarSystem.class, "c7a2e344ad06541a13ec7b636567f788", i31);
        djg = a18;
        fmVarArr[i31] = a18;
        int i32 = i31 + 1;
        C2491fm a19 = C4105zY.m41624a(StellarSystem.class, "bc9ac4c56faf1a93847170008741848e", i32);
        coL = a19;
        fmVarArr[i32] = a19;
        int i33 = i32 + 1;
        C2491fm a20 = C4105zY.m41624a(StellarSystem.class, "2805db9b81d256cc86e8550f5ee95069", i33);
        f829Lm = a20;
        fmVarArr[i33] = a20;
        int i34 = i33 + 1;
        C2491fm a21 = C4105zY.m41624a(StellarSystem.class, "8f0aaf469367dfeb8e00a5e71db0fee1", i34);
        djh = a21;
        fmVarArr[i34] = a21;
        int i35 = i34 + 1;
        C2491fm a22 = C4105zY.m41624a(StellarSystem.class, "71ae156f97a9be8f56f33234d862a25f", i35);
        dji = a22;
        fmVarArr[i35] = a22;
        int i36 = i35 + 1;
        C2491fm a23 = C4105zY.m41624a(StellarSystem.class, "e7ff9e5b429c17991687a8f9a010d09c", i36);
        aMn = a23;
        fmVarArr[i36] = a23;
        int i37 = i36 + 1;
        C2491fm a24 = C4105zY.m41624a(StellarSystem.class, "7b0522d6d764493dc1ca985021e1b2c6", i37);
        aMo = a24;
        fmVarArr[i37] = a24;
        int i38 = i37 + 1;
        C2491fm a25 = C4105zY.m41624a(StellarSystem.class, "19764efd4948cbf8872a2a8d87c5ebc6", i38);
        aMl = a25;
        fmVarArr[i38] = a25;
        int i39 = i38 + 1;
        C2491fm a26 = C4105zY.m41624a(StellarSystem.class, "201498b78595670f15caea85a8550322", i39);
        bcI = a26;
        fmVarArr[i39] = a26;
        int i40 = i39 + 1;
        C2491fm a27 = C4105zY.m41624a(StellarSystem.class, "3cf0f493b2ce8bbc57e6e6a3f8b31d1b", i40);
        aMk = a27;
        fmVarArr[i40] = a27;
        int i41 = i40 + 1;
        C2491fm a28 = C4105zY.m41624a(StellarSystem.class, "a4b4eda8ff26a5f2feeb02ea5aabf72a", i41);
        f835QE = a28;
        fmVarArr[i41] = a28;
        int i42 = i41 + 1;
        C2491fm a29 = C4105zY.m41624a(StellarSystem.class, "efd70f27b5c6c115d9ba36db0844ddb8", i42);
        f836QF = a29;
        fmVarArr[i42] = a29;
        int i43 = i42 + 1;
        C2491fm a30 = C4105zY.m41624a(StellarSystem.class, "cb94d4d1c41b7c2e9a10622cd26c8b39", i43);
        aMj = a30;
        fmVarArr[i43] = a30;
        int i44 = i43 + 1;
        C2491fm a31 = C4105zY.m41624a(StellarSystem.class, "a516e47cddcf7350fcd6f294146965bb", i44);
        f830MN = a31;
        fmVarArr[i44] = a31;
        int i45 = i44 + 1;
        C2491fm a32 = C4105zY.m41624a(StellarSystem.class, "54a482b755b571a257081c8059f46f4c", i45);
        aMp = a32;
        fmVarArr[i45] = a32;
        int i46 = i45 + 1;
        C2491fm a33 = C4105zY.m41624a(StellarSystem.class, "775c5dd61bb3b3a6eaeebf8f1f26fdfc", i46);
        f832Ob = a33;
        fmVarArr[i46] = a33;
        int i47 = i46 + 1;
        C2491fm a34 = C4105zY.m41624a(StellarSystem.class, "b271e36e3453248c3e2e62d9488763a5", i47);
        f833Oc = a34;
        fmVarArr[i47] = a34;
        int i48 = i47 + 1;
        C2491fm a35 = C4105zY.m41624a(StellarSystem.class, "7a84feb20a54ab937598c965213cc3b8", i48);
        aMq = a35;
        fmVarArr[i48] = a35;
        int i49 = i48 + 1;
        C2491fm a36 = C4105zY.m41624a(StellarSystem.class, "b56dd0a7d435c799b022e901c4a361a9", i49);
        aMr = a36;
        fmVarArr[i49] = a36;
        int i50 = i49 + 1;
        C2491fm a37 = C4105zY.m41624a(StellarSystem.class, "461c2f7299477df0846f51254b6ce352", i50);
        aMs = a37;
        fmVarArr[i50] = a37;
        int i51 = i50 + 1;
        C2491fm a38 = C4105zY.m41624a(StellarSystem.class, "b2f74b445c8f47ab8cf04df9bcf0a2ae", i51);
        djj = a38;
        fmVarArr[i51] = a38;
        int i52 = i51 + 1;
        C2491fm a39 = C4105zY.m41624a(StellarSystem.class, "a4ae5f55f281bd5a903acc3a32784ad3", i52);
        aMu = a39;
        fmVarArr[i52] = a39;
        int i53 = i52 + 1;
        C2491fm a40 = C4105zY.m41624a(StellarSystem.class, "05d8feb683e29395eb2127030f66e420", i53);
        aMv = a40;
        fmVarArr[i53] = a40;
        int i54 = i53 + 1;
        C2491fm a41 = C4105zY.m41624a(StellarSystem.class, "0184325992daeaf031609cdece19d724", i54);
        djk = a41;
        fmVarArr[i54] = a41;
        int i55 = i54 + 1;
        C2491fm a42 = C4105zY.m41624a(StellarSystem.class, "f1c33d5a37b7478f8b82f5e68e11daff", i55);
        aKm = a42;
        fmVarArr[i55] = a42;
        int i56 = i55 + 1;
        _m_methods = fmVarArr;
        C1634Xv.m11725a((Object[]) TaikodomObject._m_methods, (Object[]) _m_methods);
        ___iScriptClass = aUO.m18566a(StellarSystem.class, aIR.class, _m_fields, _m_methods);
    }

    /* renamed from: a */
    public static void m5932a(C6859auz auz) {
        if ("".equals(auz.getVersion())) {
            Iterator it = ((Collection) auz.get("stellarSystemObjects")).iterator();
            while (it.hasNext()) {
                C6859auz auz2 = (C6859auz) it.next();
                if (NPCZone.class.getName().equals(auz2.bav()) || AsteroidZone.class.getName().equals(auz2.bav())) {
                    it.remove();
                }
            }
        }
    }

    /* renamed from: H */
    private void m5911H(C2686iZ iZVar) {
        bFf().mo5608dq().mo3197f(diT, iZVar);
    }

    /* renamed from: L */
    private void m5912L(Asset tCVar) {
        bFf().mo5608dq().mo3197f(aMg, tCVar);
    }

    /* renamed from: Ll */
    private C3438ra m5913Ll() {
        return (C3438ra) bFf().mo5608dq().mo3214p(awd);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Pda Render")
    @C0064Am(aul = "461c2f7299477df0846f51254b6ce352", aum = 0)
    @C5566aOg
    /* renamed from: M */
    private void m5914M(Asset tCVar) {
        throw new aWi(new aCE(this, aMs, new Object[]{tCVar}));
    }

    @C0064Am(aul = "f1c33d5a37b7478f8b82f5e68e11daff", aum = 0)
    @C5566aOg
    /* renamed from: QR */
    private void m5915QR() {
        throw new aWi(new aCE(this, aKm, new Object[0]));
    }

    /* renamed from: RQ */
    private Asset m5916RQ() {
        return (Asset) bFf().mo5608dq().mo3214p(aMg);
    }

    /* renamed from: RR */
    private DatabaseCategory m5917RR() {
        return (DatabaseCategory) bFf().mo5608dq().mo3214p(aMh);
    }

    /* renamed from: RS */
    private float m5918RS() {
        return bFf().mo5608dq().mo3211m(aMi);
    }

    /* renamed from: U */
    private void m5926U(Vec3f vec3f) {
        bFf().mo5608dq().mo3197f(bcu, vec3f);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Position")
    @C0064Am(aul = "32560f5e8c203e5a62533db433e2761f", aum = 0)
    @C5566aOg
    /* renamed from: V */
    private void m5928V(Vec3f vec3f) {
        throw new aWi(new aCE(this, djb, new Object[]{vec3f}));
    }

    /* renamed from: a */
    private void m5929a(DatabaseCategory aik) {
        bFf().mo5608dq().mo3197f(aMh, aik);
    }

    @C3248pc(aYR = C3248pc.C3250b.ADDER, displayName = "Taikopedia Entries")
    @C0064Am(aul = "19764efd4948cbf8872a2a8d87c5ebc6", aum = 0)
    @C5566aOg
    /* renamed from: a */
    private void m5930a(TaikopediaEntry aiz) {
        throw new aWi(new aCE(this, aMl, new Object[]{aiz}));
    }

    /* renamed from: a */
    private void m5933a(String str) {
        bFf().mo5608dq().mo3197f(f839bM, str);
    }

    /* renamed from: a */
    private void m5934a(UUID uuid) {
        bFf().mo5608dq().mo3197f(f838bL, uuid);
    }

    private C2686iZ aXF() {
        return (C2686iZ) bFf().mo5608dq().mo3214p(diT);
    }

    private C3438ra aXG() {
        return (C3438ra) bFf().mo5608dq().mo3214p(diV);
    }

    private Vec3f aXH() {
        return (Vec3f) bFf().mo5608dq().mo3214p(bcu);
    }

    /* renamed from: an */
    private UUID m5936an() {
        return (UUID) bFf().mo5608dq().mo3214p(f838bL);
    }

    /* renamed from: ao */
    private String m5938ao() {
        return (String) bFf().mo5608dq().mo3214p(f839bM);
    }

    /* renamed from: ap */
    private Node m5939ap(Actor cr) {
        switch (bFf().mo6893i(djg)) {
            case 0:
                return null;
            case 2:
                return (Node) bFf().mo5606d(new aCE(this, djg, new Object[]{cr}));
            case 3:
                bFf().mo5606d(new aCE(this, djg, new Object[]{cr}));
                break;
        }
        return m5937ao(cr);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Database Category")
    @C0064Am(aul = "7b0522d6d764493dc1ca985021e1b2c6", aum = 0)
    @C5566aOg
    /* renamed from: b */
    private void m5944b(DatabaseCategory aik) {
        throw new aWi(new aCE(this, aMo, new Object[]{aik}));
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Handle")
    @C0064Am(aul = "5034205ec7e0365a7e7cdae052faeaa4", aum = 0)
    @C5566aOg
    /* renamed from: b */
    private void m5945b(String str) {
        throw new aWi(new aCE(this, f843bQ, new Object[]{str}));
    }

    /* renamed from: bc */
    private void m5947bc(C3438ra raVar) {
        bFf().mo5608dq().mo3197f(diV, raVar);
    }

    /* renamed from: br */
    private void m5948br(I18NString i18NString) {
        bFf().mo5608dq().mo3197f(f834QA, i18NString);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Description")
    @C0064Am(aul = "efd70f27b5c6c115d9ba36db0844ddb8", aum = 0)
    @C5566aOg
    /* renamed from: bu */
    private void m5949bu(I18NString i18NString) {
        throw new aWi(new aCE(this, f836QF, new Object[]{i18NString}));
    }

    /* renamed from: c */
    private void m5950c(UUID uuid) {
        switch (bFf().mo6893i(f841bO)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f841bO, new Object[]{uuid}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f841bO, new Object[]{uuid}));
                break;
        }
        m5946b(uuid);
    }

    /* renamed from: cx */
    private void m5951cx(float f) {
        bFf().mo5608dq().mo3150a(aMi, f);
    }

    /* renamed from: d */
    private void m5953d(I18NString i18NString) {
        bFf().mo5608dq().mo3197f(f851zQ, i18NString);
    }

    @C3248pc(aYR = C3248pc.C3250b.REMOVER, displayName = "Taikopedia Entries")
    @C0064Am(aul = "201498b78595670f15caea85a8550322", aum = 0)
    @C5566aOg
    /* renamed from: e */
    private void m5954e(TaikopediaEntry aiz) {
        throw new aWi(new aCE(this, bcI, new Object[]{aiz}));
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Name")
    @C0064Am(aul = "816d7fe3cd89bbf610724bf59fc36d2d", aum = 0)
    @C5566aOg
    /* renamed from: e */
    private void m5955e(I18NString i18NString) {
        throw new aWi(new aCE(this, f853zU, new Object[]{i18NString}));
    }

    @C3248pc(aYR = C3248pc.C3250b.ADDER, displayName = "Nodes")
    @C0064Am(aul = "276928fc575a22dec11dc03a5b7fdee7", aum = 0)
    @C5566aOg
    /* renamed from: f */
    private void m5956f(Node rPVar) {
        throw new aWi(new aCE(this, diX, new Object[]{rPVar}));
    }

    @C3248pc(aYR = C3248pc.C3250b.REMOVER, displayName = "Nodes")
    /* renamed from: i */
    private Set<Actor> m5958i(Node rPVar) {
        switch (bFf().mo6893i(diZ)) {
            case 0:
                return null;
            case 2:
                return (Set) bFf().mo5606d(new aCE(this, diZ, new Object[]{rPVar}));
            case 3:
                bFf().mo5606d(new aCE(this, diZ, new Object[]{rPVar}));
                break;
        }
        return m5957h(rPVar);
    }

    /* renamed from: kb */
    private I18NString m5959kb() {
        return (I18NString) bFf().mo5608dq().mo3214p(f851zQ);
    }

    @C0064Am(aul = "2805db9b81d256cc86e8550f5ee95069", aum = 0)
    @C5566aOg
    /* renamed from: qU */
    private void m5961qU() {
        throw new aWi(new aCE(this, f829Lm, new Object[0]));
    }

    /* renamed from: sG */
    private Asset m5963sG() {
        return (Asset) bFf().mo5608dq().mo3214p(f831NY);
    }

    /* renamed from: t */
    private void m5965t(Asset tCVar) {
        bFf().mo5608dq().mo3197f(f831NY, tCVar);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "[Asset] Icon")
    @C0064Am(aul = "b271e36e3453248c3e2e62d9488763a5", aum = 0)
    @C5566aOg
    /* renamed from: u */
    private void m5966u(Asset tCVar) {
        throw new aWi(new aCE(this, f833Oc, new Object[]{tCVar}));
    }

    /* renamed from: vT */
    private I18NString m5967vT() {
        return (I18NString) bFf().mo5608dq().mo3214p(f834QA);
    }

    /* renamed from: x */
    private void m5969x(C3438ra raVar) {
        bFf().mo5608dq().mo3197f(awd, raVar);
    }

    /* renamed from: C */
    public Node mo3229C(Vec3d ajr) {
        switch (bFf().mo6893i(dje)) {
            case 0:
                return null;
            case 2:
                return (Node) bFf().mo5606d(new aCE(this, dje, new Object[]{ajr}));
            case 3:
                bFf().mo5606d(new aCE(this, dje, new Object[]{ajr}));
                break;
        }
        return m5909B(ajr);
    }

    /* renamed from: E */
    public Node mo3230E(Vec3d ajr) {
        switch (bFf().mo6893i(djf)) {
            case 0:
                return null;
            case 2:
                return (Node) bFf().mo5606d(new aCE(this, djf, new Object[]{ajr}));
            case 3:
                bFf().mo5606d(new aCE(this, djf, new Object[]{ajr}));
                break;
        }
        return m5910D(ajr);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Pda Render")
    @C5566aOg
    /* renamed from: N */
    public void mo3231N(Asset tCVar) {
        switch (bFf().mo6893i(aMs)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, aMs, new Object[]{tCVar}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, aMs, new Object[]{tCVar}));
                break;
        }
        m5914M(tCVar);
    }

    @C5566aOg
    /* renamed from: QS */
    public void mo3232QS() {
        switch (bFf().mo6893i(aKm)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, aKm, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, aKm, new Object[0]));
                break;
        }
        m5915QR();
    }

    /* renamed from: RU */
    public I18NString mo182RU() {
        switch (bFf().mo6893i(aMj)) {
            case 0:
                return null;
            case 2:
                return (I18NString) bFf().mo5606d(new aCE(this, aMj, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, aMj, new Object[0]));
                break;
        }
        return m5919RT();
    }

    @C3248pc(aYR = C3248pc.C3250b.ITERATOR, displayName = "Taikopedia Entries")
    /* renamed from: RW */
    public List<TaikopediaEntry> mo183RW() {
        switch (bFf().mo6893i(aMk)) {
            case 0:
                return null;
            case 2:
                return (List) bFf().mo5606d(new aCE(this, aMk, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, aMk, new Object[0]));
                break;
        }
        return m5920RV();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Database Category")
    /* renamed from: RY */
    public DatabaseCategory mo184RY() {
        switch (bFf().mo6893i(aMn)) {
            case 0:
                return null;
            case 2:
                return (DatabaseCategory) bFf().mo5606d(new aCE(this, aMn, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, aMn, new Object[0]));
                break;
        }
        return m5921RX();
    }

    /* renamed from: Sa */
    public String mo185Sa() {
        switch (bFf().mo6893i(aMp)) {
            case 0:
                return null;
            case 2:
                return (String) bFf().mo5606d(new aCE(this, aMp, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, aMp, new Object[0]));
                break;
        }
        return m5922RZ();
    }

    /* renamed from: Sc */
    public String mo3233Sc() {
        switch (bFf().mo6893i(aMq)) {
            case 0:
                return null;
            case 2:
                return (String) bFf().mo5606d(new aCE(this, aMq, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, aMq, new Object[0]));
                break;
        }
        return m5923Sb();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Pda Render")
    /* renamed from: Se */
    public Asset mo187Se() {
        switch (bFf().mo6893i(aMr)) {
            case 0:
                return null;
            case 2:
                return (Asset) bFf().mo5606d(new aCE(this, aMr, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, aMr, new Object[0]));
                break;
        }
        return m5924Sd();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Pda MP Multiplier")
    /* renamed from: Sg */
    public float mo188Sg() {
        switch (bFf().mo6893i(aMu)) {
            case 0:
                return 0.0f;
            case 2:
                return ((Float) bFf().mo5606d(new aCE(this, aMu, new Object[0]))).floatValue();
            case 3:
                bFf().mo5606d(new aCE(this, aMu, new Object[0]));
                break;
        }
        return m5925Sf();
    }

    /* renamed from: T */
    public C6494any mo11T() {
        return ___iScriptClass;
    }

    /* renamed from: U */
    public C2491fm[] mo12U() {
        return _m_methods;
    }

    /* renamed from: W */
    public Object mo13W() {
        return new aIR(this);
    }

    /* renamed from: a */
    public Object mo14a(C0495Gr gr) {
        Object[] args = gr.getArgs();
        switch (gr.mo2417hq() - TaikodomObject._m_methodCount) {
            case 0:
                return m5942au();
            case 1:
                return m5960kd();
            case 2:
                m5955e((I18NString) args[0]);
                return null;
            case 3:
                return m5940ap();
            case 4:
                m5946b((UUID) args[0]);
                return null;
            case 5:
                return aXI();
            case 6:
                return m5941ar();
            case 7:
                m5945b((String) args[0]);
                return null;
            case 8:
                m5956f((Node) args[0]);
                return null;
            case 9:
                return aXK();
            case 10:
                return m5957h((Node) args[0]);
            case 11:
                return aXL();
            case 12:
                m5928V((Vec3f) args[0]);
                return null;
            case 13:
                return aXM();
            case 14:
                return aXO();
            case 15:
                return m5909B((Vec3d) args[0]);
            case 16:
                return m5910D((Vec3d) args[0]);
            case 17:
                return m5937ao((Actor) args[0]);
            case 18:
                m5931a((C6417amZ) args[0]);
                return null;
            case 19:
                m5961qU();
                return null;
            case 20:
                aXQ();
                return null;
            case 21:
                return aXS();
            case 22:
                return m5921RX();
            case 23:
                m5944b((DatabaseCategory) args[0]);
                return null;
            case 24:
                m5930a((TaikopediaEntry) args[0]);
                return null;
            case 25:
                m5954e((TaikopediaEntry) args[0]);
                return null;
            case 26:
                return m5920RV();
            case 27:
                return m5968vV();
            case 28:
                m5949bu((I18NString) args[0]);
                return null;
            case 29:
                return m5919RT();
            case 30:
                return m5962rO();
            case 31:
                return m5922RZ();
            case 32:
                return m5964sJ();
            case 33:
                m5966u((Asset) args[0]);
                return null;
            case 34:
                return m5923Sb();
            case 35:
                return m5924Sd();
            case 36:
                m5914M((Asset) args[0]);
                return null;
            case 37:
                return m5935aW((Player) args[0]);
            case 38:
                return new Float(m5925Sf());
            case 39:
                m5952cy(((Float) args[0]).floatValue());
                return null;
            case 40:
                return m5943b((Vec3d) args[0], ((Float) args[1]).floatValue());
            case 41:
                m5915QR();
                return null;
            default:
                return super.mo14a(gr);
        }
    }

    /* renamed from: a */
    public void mo15a(Collection collection, C0495Gr gr) {
        super.mo15a(collection, gr);
    }

    @aFW(cZD = true, value = "Nodes")
    /* renamed from: aX */
    public Set<aDJ> mo3234aX(Player aku) {
        switch (bFf().mo6893i(djj)) {
            case 0:
                return null;
            case 2:
                return (Set) bFf().mo5606d(new aCE(this, djj, new Object[]{aku}));
            case 3:
                bFf().mo5606d(new aCE(this, djj, new Object[]{aku}));
                break;
        }
        return m5935aW(aku);
    }

    public Set<Actor> aXJ() {
        switch (bFf().mo6893i(diW)) {
            case 0:
                return null;
            case 2:
                return (Set) bFf().mo5606d(new aCE(this, diW, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, diW, new Object[0]));
                break;
        }
        return aXI();
    }

    @C3248pc(aYR = C3248pc.C3250b.ITERATOR, displayName = "StellarSystemObjects")
    public Set<Actor> aXN() {
        switch (bFf().mo6893i(djc)) {
            case 0:
                return null;
            case 2:
                return (Set) bFf().mo5606d(new aCE(this, djc, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, djc, new Object[0]));
                break;
        }
        return aXM();
    }

    public Set<Actor> aXP() {
        switch (bFf().mo6893i(djd)) {
            case 0:
                return null;
            case 2:
                return (Set) bFf().mo5606d(new aCE(this, djd, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, djd, new Object[0]));
                break;
        }
        return aXO();
    }

    public void aXR() {
        switch (bFf().mo6893i(djh)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, djh, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, djh, new Object[0]));
                break;
        }
        aXQ();
    }

    public StellarSystem aXT() {
        switch (bFf().mo6893i(dji)) {
            case 0:
                return null;
            case 2:
                return (StellarSystem) bFf().mo5606d(new aCE(this, dji, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, dji, new Object[0]));
                break;
        }
        return aXS();
    }

    /* renamed from: aq */
    public UUID mo18aq() {
        switch (bFf().mo6893i(f840bN)) {
            case 0:
                return null;
            case 2:
                return (UUID) bFf().mo5606d(new aCE(this, f840bN, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f840bN, new Object[0]));
                break;
        }
        return m5940ap();
    }

    @C3248pc(aYR = C3248pc.C3250b.ADDER, displayName = "Taikopedia Entries")
    @C5566aOg
    /* renamed from: b */
    public void mo3240b(TaikopediaEntry aiz) {
        switch (bFf().mo6893i(aMl)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, aMl, new Object[]{aiz}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, aMl, new Object[]{aiz}));
                break;
        }
        m5930a(aiz);
    }

    /* renamed from: b */
    public void mo625b(C6417amZ amz) {
        switch (bFf().mo6893i(coL)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, coL, new Object[]{amz}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, coL, new Object[]{amz}));
                break;
        }
        m5931a(amz);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Description")
    @C5566aOg
    /* renamed from: bv */
    public void mo3241bv(I18NString i18NString) {
        switch (bFf().mo6893i(f836QF)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f836QF, new Object[]{i18NString}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f836QF, new Object[]{i18NString}));
                break;
        }
        m5949bu(i18NString);
    }

    /* renamed from: c */
    public List<Actor> mo3242c(Vec3d ajr, float f) {
        switch (bFf().mo6893i(djk)) {
            case 0:
                return null;
            case 2:
                return (List) bFf().mo5606d(new aCE(this, djk, new Object[]{ajr, new Float(f)}));
            case 3:
                bFf().mo5606d(new aCE(this, djk, new Object[]{ajr, new Float(f)}));
                break;
        }
        return m5943b(ajr, f);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Database Category")
    @C5566aOg
    /* renamed from: c */
    public void mo3243c(DatabaseCategory aik) {
        switch (bFf().mo6893i(aMo)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, aMo, new Object[]{aik}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, aMo, new Object[]{aik}));
                break;
        }
        m5944b(aik);
    }

    /* renamed from: c */
    public C5663aRz[] mo25c() {
        return _m_fields;
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Pda MP Multiplier")
    /* renamed from: cz */
    public void mo3244cz(float f) {
        switch (bFf().mo6893i(aMv)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, aMv, new Object[]{new Float(f)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, aMv, new Object[]{new Float(f)}));
                break;
        }
        m5952cy(f);
    }

    @C3248pc(aYR = C3248pc.C3250b.REMOVER, displayName = "Taikopedia Entries")
    @C5566aOg
    /* renamed from: f */
    public void mo3245f(TaikopediaEntry aiz) {
        switch (bFf().mo6893i(bcI)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, bcI, new Object[]{aiz}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, bcI, new Object[]{aiz}));
                break;
        }
        m5954e(aiz);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Name")
    @C5566aOg
    /* renamed from: f */
    public void mo3246f(I18NString i18NString) {
        switch (bFf().mo6893i(f853zU)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f853zU, new Object[]{i18NString}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f853zU, new Object[]{i18NString}));
                break;
        }
        m5955e(i18NString);
    }

    @C3248pc(aYR = C3248pc.C3250b.ADDER, displayName = "Nodes")
    @C5566aOg
    /* renamed from: g */
    public void mo3247g(Node rPVar) {
        switch (bFf().mo6893i(diX)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, diX, new Object[]{rPVar}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, diX, new Object[]{rPVar}));
                break;
        }
        m5956f(rPVar);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Position")
    /* renamed from: gL */
    public Vec3f mo3248gL() {
        switch (bFf().mo6893i(dja)) {
            case 0:
                return null;
            case 2:
                return (Vec3f) bFf().mo5606d(new aCE(this, dja, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, dja, new Object[0]));
                break;
        }
        return aXL();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Handle")
    public String getHandle() {
        switch (bFf().mo6893i(f842bP)) {
            case 0:
                return null;
            case 2:
                return (String) bFf().mo5606d(new aCE(this, f842bP, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f842bP, new Object[0]));
                break;
        }
        return m5941ar();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Handle")
    @C5566aOg
    public void setHandle(String str) {
        switch (bFf().mo6893i(f843bQ)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f843bQ, new Object[]{str}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f843bQ, new Object[]{str}));
                break;
        }
        m5945b(str);
    }

    @C3248pc(aYR = C3248pc.C3250b.ITERATOR, displayName = "Nodes")
    public List<Node> getNodes() {
        switch (bFf().mo6893i(diY)) {
            case 0:
                return null;
            case 2:
                return (List) bFf().mo5606d(new aCE(this, diY, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, diY, new Object[0]));
                break;
        }
        return aXK();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Name")
    /* renamed from: ke */
    public I18NString mo3250ke() {
        switch (bFf().mo6893i(f852zT)) {
            case 0:
                return null;
            case 2:
                return (I18NString) bFf().mo5606d(new aCE(this, f852zT, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f852zT, new Object[0]));
                break;
        }
        return m5960kd();
    }

    @C5566aOg
    /* renamed from: qV */
    public void mo3251qV() {
        switch (bFf().mo6893i(f829Lm)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f829Lm, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f829Lm, new Object[0]));
                break;
        }
        m5961qU();
    }

    /* renamed from: rP */
    public I18NString mo195rP() {
        switch (bFf().mo6893i(f830MN)) {
            case 0:
                return null;
            case 2:
                return (I18NString) bFf().mo5606d(new aCE(this, f830MN, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f830MN, new Object[0]));
                break;
        }
        return m5962rO();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "[Asset] Icon")
    /* renamed from: sK */
    public Asset mo3252sK() {
        switch (bFf().mo6893i(f832Ob)) {
            case 0:
                return null;
            case 2:
                return (Asset) bFf().mo5606d(new aCE(this, f832Ob, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f832Ob, new Object[0]));
                break;
        }
        return m5964sJ();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Position")
    @C5566aOg
    public void setPosition(Vec3f vec3f) {
        switch (bFf().mo6893i(djb)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, djb, new Object[]{vec3f}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, djb, new Object[]{vec3f}));
                break;
        }
        m5928V(vec3f);
    }

    public String toString() {
        switch (bFf().mo6893i(_f_toString_0020_0028_0029Ljava_002flang_002fString_003b)) {
            case 0:
                return null;
            case 2:
                return (String) bFf().mo5606d(new aCE(this, _f_toString_0020_0028_0029Ljava_002flang_002fString_003b, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, _f_toString_0020_0028_0029Ljava_002flang_002fString_003b, new Object[0]));
                break;
        }
        return m5942au();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "[Asset] Icon")
    @C5566aOg
    /* renamed from: v */
    public void mo3255v(Asset tCVar) {
        switch (bFf().mo6893i(f833Oc)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f833Oc, new Object[]{tCVar}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f833Oc, new Object[]{tCVar}));
                break;
        }
        m5966u(tCVar);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Description")
    /* renamed from: vW */
    public I18NString mo3256vW() {
        switch (bFf().mo6893i(f835QE)) {
            case 0:
                return null;
            case 2:
                return (I18NString) bFf().mo5606d(new aCE(this, f835QE, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f835QE, new Object[0]));
                break;
        }
        return m5968vV();
    }

    @C0064Am(aul = "4dff90fecf20662a8d84fe65fa82d570", aum = 0)
    /* renamed from: au */
    private String m5942au() {
        return "[" + m5938ao() + "]";
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Name")
    @C0064Am(aul = "911a3fdba2a7137f522fb00e0de35d7c", aum = 0)
    /* renamed from: kd */
    private I18NString m5960kd() {
        return m5959kb();
    }

    @C0064Am(aul = "91bf721792def4642a5541776a92200b", aum = 0)
    /* renamed from: ap */
    private UUID m5940ap() {
        return m5936an();
    }

    @C0064Am(aul = "342d04e1615d83c44c203aeadb29276e", aum = 0)
    /* renamed from: b */
    private void m5946b(UUID uuid) {
        m5934a(uuid);
    }

    /* renamed from: S */
    public void mo10S() {
        super.mo10S();
        mo3246f(new I18NString("unamed"));
        m5934a(UUID.randomUUID());
    }

    @C0064Am(aul = "2799f320b4cb6a253573f381646fbcf3", aum = 0)
    private Set<Actor> aXI() {
        HashSet hashSet = new HashSet();
        for (Node Zc : getNodes()) {
            hashSet.addAll(Zc.mo21618Zc().cKD());
        }
        return hashSet;
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Handle")
    @C0064Am(aul = "f5af94e5903927322cb9c134cfe2b413", aum = 0)
    /* renamed from: ar */
    private String m5941ar() {
        return m5938ao();
    }

    @C3248pc(aYR = C3248pc.C3250b.ITERATOR, displayName = "Nodes")
    @C0064Am(aul = "957d72f8f6bb97e44f975272033036fc", aum = 0)
    private List<Node> aXK() {
        return Collections.unmodifiableList(aXG());
    }

    @C3248pc(aYR = C3248pc.C3250b.REMOVER, displayName = "Nodes")
    @C0064Am(aul = "b223f208d1b891e7300ecaee9f7aeb28", aum = 0)
    /* renamed from: h */
    private Set<Actor> m5957h(Node rPVar) {
        Set<Actor> g = rPVar.mo21657g(this);
        aXG().remove(rPVar);
        return g;
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Position")
    @C0064Am(aul = "34c3bb9ef17b294a80e626c4239c008d", aum = 0)
    private Vec3f aXL() {
        return aXH();
    }

    @C3248pc(aYR = C3248pc.C3250b.ITERATOR, displayName = "StellarSystemObjects")
    @C0064Am(aul = "788bdc9c05e066348c32810596cdbba2", aum = 0)
    private Set<Actor> aXM() {
        return Collections.unmodifiableSet(aXJ());
    }

    @C0064Am(aul = "6e47e2adbf506decf52de0cdd7104e7c", aum = 0)
    private Set<Actor> aXO() {
        return Collections.unmodifiableSet(aXF());
    }

    @C0064Am(aul = "791a66250bd3dd3bc0348523f744194a", aum = 0)
    /* renamed from: B */
    private Node m5909B(Vec3d ajr) {
        Node rPVar = null;
        if (ajr != null && aXG().size() >= 1) {
            rPVar = (Node) aXG().get(0);
            for (int i = 1; i < aXG().size(); i++) {
                Node rPVar2 = (Node) aXG().get(i);
                if (rPVar.getPosition().mo9486aC(ajr) > rPVar2.getPosition().mo9486aC(ajr)) {
                    rPVar = rPVar2;
                }
            }
            if (rPVar == null) {
                System.err.println("************ NO NODES HAVE BEEN FOUND ***************");
            }
        }
        return rPVar;
    }

    @C0064Am(aul = "cf3d95070cfba2a571d24e49d1d35c64", aum = 0)
    /* renamed from: D */
    private Node m5910D(Vec3d ajr) {
        if (ajr == null || aXG().size() < 1) {
            return null;
        }
        Node rPVar = (Node) aXG().get(0);
        float aC = rPVar.getPosition().mo9486aC(ajr);
        Node rPVar2 = rPVar;
        for (int i = 1; i < aXG().size(); i++) {
            Node rPVar3 = (Node) aXG().get(i);
            float aC2 = rPVar2.getPosition().mo9486aC(ajr);
            float aC3 = rPVar3.getPosition().mo9486aC(ajr);
            if (aC2 > aC3) {
                aC = aC3;
                rPVar2 = rPVar3;
            }
        }
        float radius = rPVar2.getRadius();
        if (aC > radius * radius) {
            rPVar2 = null;
        }
        return rPVar2;
    }

    @C0064Am(aul = "c7a2e344ad06541a13ec7b636567f788", aum = 0)
    /* renamed from: ao */
    private Node m5937ao(Actor cr) {
        Node C = mo3229C(cr.cLl());
        if (C == null) {
            return null;
        }
        if (C.mo21600H(cr)) {
            return C;
        }
        return null;
    }

    @C0064Am(aul = "bc9ac4c56faf1a93847170008741848e", aum = 0)
    /* renamed from: a */
    private void m5931a(C6417amZ amz) {
        if (amz.ape() && amz.aph() != null) {
            HashSet hashSet = new HashSet(aXJ());
            int i = 0;
            int i2 = 0;
            for (C0665JT baw : (Set) amz.aph().get("stellarSystemObjects")) {
                Actor cr = (Actor) baw.baw();
                if (!hashSet.contains(cr)) {
                    i2++;
                    if (!(cr instanceof Asteroid) && (cr instanceof Ship) && (((Ship) cr).mo2998hb() instanceof PlayerController)) {
                        aXJ().add(cr);
                        hashSet.add(cr);
                        i++;
                    }
                }
            }
            mo8359ma("StellarSystem merge added: " + i + " not added: " + (i2 - i) + " total: " + i2);
        }
    }

    @C0064Am(aul = "8f0aaf469367dfeb8e00a5e71db0fee1", aum = 0)
    private void aXQ() {
    }

    @C0064Am(aul = "71ae156f97a9be8f56f33234d862a25f", aum = 0)
    private StellarSystem aXS() {
        return this;
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Database Category")
    @C0064Am(aul = "e7ff9e5b429c17991687a8f9a010d09c", aum = 0)
    /* renamed from: RX */
    private DatabaseCategory m5921RX() {
        return m5917RR();
    }

    @C3248pc(aYR = C3248pc.C3250b.ITERATOR, displayName = "Taikopedia Entries")
    @C0064Am(aul = "3cf0f493b2ce8bbc57e6e6a3f8b31d1b", aum = 0)
    /* renamed from: RV */
    private List<TaikopediaEntry> m5920RV() {
        return Collections.unmodifiableList(m5913Ll());
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Description")
    @C0064Am(aul = "a4b4eda8ff26a5f2feeb02ea5aabf72a", aum = 0)
    /* renamed from: vV */
    private I18NString m5968vV() {
        return m5967vT();
    }

    @C0064Am(aul = "cb94d4d1c41b7c2e9a10622cd26c8b39", aum = 0)
    /* renamed from: RT */
    private I18NString m5919RT() {
        return mo3256vW();
    }

    @C0064Am(aul = "a516e47cddcf7350fcd6f294146965bb", aum = 0)
    /* renamed from: rO */
    private I18NString m5962rO() {
        return mo3250ke();
    }

    @C0064Am(aul = "54a482b755b571a257081c8059f46f4c", aum = 0)
    /* renamed from: RZ */
    private String m5922RZ() {
        if (m5963sG() != null) {
            return m5963sG().getHandle();
        }
        return null;
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "[Asset] Icon")
    @C0064Am(aul = "775c5dd61bb3b3a6eaeebf8f1f26fdfc", aum = 0)
    /* renamed from: sJ */
    private Asset m5964sJ() {
        return m5963sG();
    }

    @C0064Am(aul = "7a84feb20a54ab937598c965213cc3b8", aum = 0)
    /* renamed from: Sb */
    private String m5923Sb() {
        return m5916RQ().getHandle();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Pda Render")
    @C0064Am(aul = "b56dd0a7d435c799b022e901c4a361a9", aum = 0)
    /* renamed from: Sd */
    private Asset m5924Sd() {
        return m5916RQ();
    }

    @aFW(cZD = true, value = "Nodes")
    @C0064Am(aul = "b2f74b445c8f47ab8cf04df9bcf0a2ae", aum = 0)
    /* renamed from: aW */
    private Set<aDJ> m5935aW(Player aku) {
        return aku.dyl().mo11984g(this);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Pda MP Multiplier")
    @C0064Am(aul = "a4ae5f55f281bd5a903acc3a32784ad3", aum = 0)
    /* renamed from: Sf */
    private float m5925Sf() {
        return m5918RS();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Pda MP Multiplier")
    @C0064Am(aul = "05d8feb683e29395eb2127030f66e420", aum = 0)
    /* renamed from: cy */
    private void m5952cy(float f) {
        m5951cx(f);
    }

    @C0064Am(aul = "0184325992daeaf031609cdece19d724", aum = 0)
    /* renamed from: b */
    private List<Actor> m5943b(Vec3d ajr, float f) {
        ArrayList arrayList = new ArrayList();
        for (Node Zc : aXG()) {
            arrayList.addAll(Zc.mo21618Zc().mo1900c(ajr, f));
        }
        return arrayList;
    }
}
