package game.script.space;

import com.hoplon.geometry.Color;
import com.hoplon.geometry.Vec3f;
import game.network.message.externalizable.aCE;
import game.script.Actor;
import game.script.Character;
import game.script.item.Item;
import game.script.mission.NPCEventDispatcher;
import game.script.nls.NLSLoot;
import game.script.nls.NLSManager;
import game.script.player.Player;
import game.script.resource.Asset;
import game.script.ship.Ship;
import game.script.simulation.Space;
import game.script.template.BaseTaikodomContent;
import logic.aaa.C2235dB;
import logic.baa.*;
import logic.bbb.C6348alI;
import logic.data.mbean.C6379aln;
import logic.res.code.C5640aRc;
import logic.res.code.C5663aRz;
import logic.res.code.C6494any;
import logic.res.html.C2491fm;
import p001a.*;

import java.util.Collection;
import java.util.Comparator;
import java.util.Set;

@C2712iu(mo19785Bs = {C6251ajP.class}, mo19786Bt = BaseTaikodomContent.class)
@C5511aMd
@C6485anp
/* renamed from: a.aEL */
/* compiled from: a */
public class Loot extends Actor implements C0286Dh, C1616Xf {
    /* renamed from: LR */
    public static final C5663aRz f2670LR = null;
    /* renamed from: Lm */
    public static final C2491fm f2671Lm = null;
    /* renamed from: MH */
    public static final C2491fm f2672MH = null;
    /* renamed from: MM */
    public static final C2491fm f2673MM = null;
    /* renamed from: Mq */
    public static final C2491fm f2674Mq = null;
    /* renamed from: Pf */
    public static final C2491fm f2675Pf = null;
    /* renamed from: Wp */
    public static final C2491fm f2676Wp = null;
    public static final C2491fm _f_dispose_0020_0028_0029V = null;
    public static final C2491fm _f_getType_0020_0028_0029Ljava_002flang_002fObject_003b = null;
    public static final C2491fm _f_hasHollowField_0020_0028_0029Z = null;
    public static final C2491fm _f_onResurrect_0020_0028_0029V = null;
    public static final C2491fm _f_step_0020_0028F_0029V = null;
    public static final C2491fm _f_tick_0020_0028F_0029V = null;
    public static final int _m_fieldCount = 0;
    public static final C5663aRz[] _m_fields = null;
    public static final int _m_methodCount = 0;
    public static final C2491fm[] _m_methods = null;
    public static final C2491fm aKn = null;
    public static final C2491fm brY = null;
    public static final C2491fm clC = null;
    public static final C2491fm iKA = null;
    public static final C2491fm iKB = null;
    public static final C2491fm iKC = null;
    public static final C2491fm iKD = null;
    public static final C2491fm iKE = null;
    public static final C2491fm iKF = null;
    public static final C2491fm iKG = null;
    public static final C2491fm iKH = null;
    public static final C2491fm iKI = null;
    public static final C2491fm iKJ = null;
    public static final C2491fm iKK = null;
    public static final C2491fm iKL = null;
    public static final C2491fm iKM = null;
    public static final C2491fm iKN = null;
    public static final C2491fm iKO = null;
    public static final C2491fm iKP = null;
    public static final C2491fm iKQ = null;
    public static final C2491fm iKR = null;
    public static final C2491fm iKS = null;
    public static final C2491fm iKT = null;
    public static final C5663aRz iKm = null;
    public static final C5663aRz iKn = null;
    public static final C5663aRz iKo = null;
    public static final C5663aRz iKp = null;
    public static final C5663aRz iKq = null;
    public static final C5663aRz iKu = null;
    public static final C5663aRz iKv = null;
    public static final C5663aRz iKw = null;
    public static final C2491fm iKx = null;
    public static final C2491fm iKy = null;
    public static final C2491fm iKz = null;
    /* renamed from: kZ */
    public static final C5663aRz f2677kZ = null;
    public static final long serialVersionUID = 0;
    /* renamed from: uY */
    public static final C2491fm f2678uY = null;
    /* renamed from: vc */
    public static final C2491fm f2679vc = null;
    /* renamed from: ve */
    public static final C2491fm f2680ve = null;
    /* renamed from: vi */
    public static final C2491fm f2681vi = null;
    /* renamed from: vm */
    public static final C2491fm f2682vm = null;
    /* renamed from: vn */
    public static final C2491fm f2683vn = null;
    /* renamed from: vo */
    public static final C2491fm f2684vo = null;
    /* renamed from: vs */
    public static final C2491fm f2685vs = null;
    public static C6494any ___iScriptClass;
    @C0064Am(aul = "793471a42b73d3099e5d3771c994a16e", aum = 0)

    /* renamed from: LQ */
    private static Asset f2669LQ;
    @C0064Am(aul = "1c8828e793d8ae9330b665d11a45a425", aum = 2)
    private static LootItems fWo;
    @C0064Am(aul = "ca5362f757f7120227f0ecb98e095379", aum = 3)
    private static Character fWp;
    @C0064Am(aul = "99e36d4281877e977b9d6f7595437347", aum = 4)
    private static Character fWq;
    @C0064Am(aul = "f97ca1937e047ae711d77cfb75d321f8", aum = 5)
    private static String fWr;
    @C0064Am(aul = "13bf1ce58d371c7aed686b0c5c0b2508", aum = 6)
    @C1020Ot(drz = C1020Ot.C1021a.NOT_PERSISTED)
    private static boolean fWs;
    @C0064Am(aul = "052ae4c1d0ccf5b495de4dc2efdd6dee", aum = 7)
    private static NPCEventDispatcher fWt;
    @C0064Am(aul = "05caf179c0d8e59c73283b65142a8db9", aum = 8)
    private static boolean fWu;
    @C0064Am(aul = "11d55e4abecf6233a39ee379aae10b56", aum = 9)
    private static Character fWv;
    @C0064Am(aul = "03e01d0addf8a7dcf7ba2cd81f3f8e78", aum = 1)
    @C5454aJy("Lifetime (s)")
    private static float lifeTime;

    static {
        m14065V();
    }

    private transient float elapsedTime;
    @ClientOnly
    private transient boolean iKr;
    @ClientOnly
    private float iKs;
    @ClientOnly
    private float iKt;

    public Loot() {
        super((C5540aNg) null);
        super.mo10S();
    }

    public Loot(LootType ahc) {
        super((C5540aNg) null);
        super._m_script_init(ahc);
    }

    public Loot(C5540aNg ang) {
        super(ang);
    }

    /* renamed from: V */
    static void m14065V() {
        serialVersionUID = (long) 1;
        _m_fieldCount = Actor._m_fieldCount + 10;
        _m_methodCount = Actor._m_methodCount + 46;
        int i = Actor._m_fieldCount;
        C5663aRz[] arzArr = new C5663aRz[(i + 10)];
        C5663aRz b = C5640aRc.m17844b(Loot.class, "793471a42b73d3099e5d3771c994a16e", i);
        f2670LR = b;
        arzArr[i] = b;
        int i2 = i + 1;
        C5663aRz b2 = C5640aRc.m17844b(Loot.class, "03e01d0addf8a7dcf7ba2cd81f3f8e78", i2);
        f2677kZ = b2;
        arzArr[i2] = b2;
        int i3 = i2 + 1;
        C5663aRz b3 = C5640aRc.m17844b(Loot.class, "1c8828e793d8ae9330b665d11a45a425", i3);
        iKm = b3;
        arzArr[i3] = b3;
        int i4 = i3 + 1;
        C5663aRz b4 = C5640aRc.m17844b(Loot.class, "ca5362f757f7120227f0ecb98e095379", i4);
        iKn = b4;
        arzArr[i4] = b4;
        int i5 = i4 + 1;
        C5663aRz b5 = C5640aRc.m17844b(Loot.class, "99e36d4281877e977b9d6f7595437347", i5);
        iKo = b5;
        arzArr[i5] = b5;
        int i6 = i5 + 1;
        C5663aRz b6 = C5640aRc.m17844b(Loot.class, "f97ca1937e047ae711d77cfb75d321f8", i6);
        iKp = b6;
        arzArr[i6] = b6;
        int i7 = i6 + 1;
        C5663aRz b7 = C5640aRc.m17844b(Loot.class, "13bf1ce58d371c7aed686b0c5c0b2508", i7);
        iKq = b7;
        arzArr[i7] = b7;
        int i8 = i7 + 1;
        C5663aRz b8 = C5640aRc.m17844b(Loot.class, "052ae4c1d0ccf5b495de4dc2efdd6dee", i8);
        iKu = b8;
        arzArr[i8] = b8;
        int i9 = i8 + 1;
        C5663aRz b9 = C5640aRc.m17844b(Loot.class, "05caf179c0d8e59c73283b65142a8db9", i9);
        iKv = b9;
        arzArr[i9] = b9;
        int i10 = i9 + 1;
        C5663aRz b10 = C5640aRc.m17844b(Loot.class, "11d55e4abecf6233a39ee379aae10b56", i10);
        iKw = b10;
        arzArr[i10] = b10;
        int i11 = i10 + 1;
        _m_fields = arzArr;
        C1634Xv.m11725a((Object[]) Actor._m_fields, (Object[]) _m_fields);
        int i12 = Actor._m_methodCount;
        C2491fm[] fmVarArr = new C2491fm[(i12 + 46)];
        C2491fm a = C4105zY.m41624a(Loot.class, "5bc3cace1538866e5bf2f39b9213940f", i12);
        iKx = a;
        fmVarArr[i12] = a;
        int i13 = i12 + 1;
        C2491fm a2 = C4105zY.m41624a(Loot.class, "ded744e27c1f9d2e27b708c8796a21e5", i13);
        iKy = a2;
        fmVarArr[i13] = a2;
        int i14 = i13 + 1;
        C2491fm a3 = C4105zY.m41624a(Loot.class, "68db3d019708fa3537b2af2290847d85", i14);
        iKz = a3;
        fmVarArr[i14] = a3;
        int i15 = i14 + 1;
        C2491fm a4 = C4105zY.m41624a(Loot.class, "e5223563ebd96cf36dff492733e253a6", i15);
        f2674Mq = a4;
        fmVarArr[i15] = a4;
        int i16 = i15 + 1;
        C2491fm a5 = C4105zY.m41624a(Loot.class, "9d8f15badaa1365c31128691a634bf60", i16);
        f2676Wp = a5;
        fmVarArr[i16] = a5;
        int i17 = i16 + 1;
        C2491fm a6 = C4105zY.m41624a(Loot.class, "0c66e9839451688affe81236ad80f67a", i17);
        f2675Pf = a6;
        fmVarArr[i17] = a6;
        int i18 = i17 + 1;
        C2491fm a7 = C4105zY.m41624a(Loot.class, "1722d11162b61ad464ddaab15cda4928", i18);
        iKA = a7;
        fmVarArr[i18] = a7;
        int i19 = i18 + 1;
        C2491fm a8 = C4105zY.m41624a(Loot.class, "9ef42ed5578e8ec51e240cf41983095a", i19);
        f2672MH = a8;
        fmVarArr[i19] = a8;
        int i20 = i19 + 1;
        C2491fm a9 = C4105zY.m41624a(Loot.class, "864da50bda8095f37d05ffab966a7d71", i20);
        iKB = a9;
        fmVarArr[i20] = a9;
        int i21 = i20 + 1;
        C2491fm a10 = C4105zY.m41624a(Loot.class, "c4db5a059db90626f7f60dd46fc75d41", i21);
        iKC = a10;
        fmVarArr[i21] = a10;
        int i22 = i21 + 1;
        C2491fm a11 = C4105zY.m41624a(Loot.class, "0f69f2be0f9518b696389bfd154bcb13", i22);
        iKD = a11;
        fmVarArr[i22] = a11;
        int i23 = i22 + 1;
        C2491fm a12 = C4105zY.m41624a(Loot.class, "ff43f9ec526df758118733efc85316a8", i23);
        f2683vn = a12;
        fmVarArr[i23] = a12;
        int i24 = i23 + 1;
        C2491fm a13 = C4105zY.m41624a(Loot.class, "9af4af4c118aa01ff7b8137bf0648b01", i24);
        f2679vc = a13;
        fmVarArr[i24] = a13;
        int i25 = i24 + 1;
        C2491fm a14 = C4105zY.m41624a(Loot.class, "a1549f96eb3c153f8eb0b256ab3d0968", i25);
        f2680ve = a14;
        fmVarArr[i25] = a14;
        int i26 = i25 + 1;
        C2491fm a15 = C4105zY.m41624a(Loot.class, "9d9aa475481664f5c280ddd178aa10db", i26);
        iKE = a15;
        fmVarArr[i26] = a15;
        int i27 = i26 + 1;
        C2491fm a16 = C4105zY.m41624a(Loot.class, "02005983a3eafc2d66433b54b09dc31d", i27);
        iKF = a16;
        fmVarArr[i27] = a16;
        int i28 = i27 + 1;
        C2491fm a17 = C4105zY.m41624a(Loot.class, "d75d0b91e472e04033b8a4c37e9b2f2c", i28);
        iKG = a17;
        fmVarArr[i28] = a17;
        int i29 = i28 + 1;
        C2491fm a18 = C4105zY.m41624a(Loot.class, "0c78472610adadbc6b676c54fb34c84d", i29);
        iKH = a18;
        fmVarArr[i29] = a18;
        int i30 = i29 + 1;
        C2491fm a19 = C4105zY.m41624a(Loot.class, "33d00225fd0af1e06fd2650dc7ad30d0", i30);
        _f_tick_0020_0028F_0029V = a19;
        fmVarArr[i30] = a19;
        int i31 = i30 + 1;
        C2491fm a20 = C4105zY.m41624a(Loot.class, "37a7c53d5f0f5e18938a2bd86cb0746b", i31);
        f2678uY = a20;
        fmVarArr[i31] = a20;
        int i32 = i31 + 1;
        C2491fm a21 = C4105zY.m41624a(Loot.class, "e8221368bd7ce4eb61f3eb8b78415362", i32);
        _f_step_0020_0028F_0029V = a21;
        fmVarArr[i32] = a21;
        int i33 = i32 + 1;
        C2491fm a22 = C4105zY.m41624a(Loot.class, "164f3afbbfe68655bf09871a2790db02", i33);
        iKI = a22;
        fmVarArr[i33] = a22;
        int i34 = i33 + 1;
        C2491fm a23 = C4105zY.m41624a(Loot.class, "0abf84650b575097278cb7f2f7507360", i34);
        iKJ = a23;
        fmVarArr[i34] = a23;
        int i35 = i34 + 1;
        C2491fm a24 = C4105zY.m41624a(Loot.class, "694c142573f60a2ed0b98e58e609f145", i35);
        clC = a24;
        fmVarArr[i35] = a24;
        int i36 = i35 + 1;
        C2491fm a25 = C4105zY.m41624a(Loot.class, "b7666ff3e821a5ae29675dfa331a768f", i36);
        iKK = a25;
        fmVarArr[i36] = a25;
        int i37 = i36 + 1;
        C2491fm a26 = C4105zY.m41624a(Loot.class, "091ca5e8d0878765c0d5ba702380f1ba", i37);
        iKL = a26;
        fmVarArr[i37] = a26;
        int i38 = i37 + 1;
        C2491fm a27 = C4105zY.m41624a(Loot.class, "5736f871346fd5696ee719145d2dddca", i38);
        iKM = a27;
        fmVarArr[i38] = a27;
        int i39 = i38 + 1;
        C2491fm a28 = C4105zY.m41624a(Loot.class, "2a68f10a13e61d8ff41d19a4730ba8f7", i39);
        brY = a28;
        fmVarArr[i39] = a28;
        int i40 = i39 + 1;
        C2491fm a29 = C4105zY.m41624a(Loot.class, "859edaba82b6c8b3bbdd82e93b0ff06f", i40);
        _f_dispose_0020_0028_0029V = a29;
        fmVarArr[i40] = a29;
        int i41 = i40 + 1;
        C2491fm a30 = C4105zY.m41624a(Loot.class, "c63f74698ad96cf269781512d44f5e49", i41);
        iKN = a30;
        fmVarArr[i41] = a30;
        int i42 = i41 + 1;
        C2491fm a31 = C4105zY.m41624a(Loot.class, "1d9b7771faa261ccc20967142907233e", i42);
        iKO = a31;
        fmVarArr[i42] = a31;
        int i43 = i42 + 1;
        C2491fm a32 = C4105zY.m41624a(Loot.class, "ff701f58430f08ede790bd776e3b44f0", i43);
        f2684vo = a32;
        fmVarArr[i43] = a32;
        int i44 = i43 + 1;
        C2491fm a33 = C4105zY.m41624a(Loot.class, "f853283e1fdf93a3630d76e8415b7799", i44);
        iKP = a33;
        fmVarArr[i44] = a33;
        int i45 = i44 + 1;
        C2491fm a34 = C4105zY.m41624a(Loot.class, "f906e5a54a151f4088c16956ad50f4fb", i45);
        f2682vm = a34;
        fmVarArr[i45] = a34;
        int i46 = i45 + 1;
        C2491fm a35 = C4105zY.m41624a(Loot.class, "1ec14773ccb4258fc419d142b6876a2c", i46);
        f2681vi = a35;
        fmVarArr[i46] = a35;
        int i47 = i46 + 1;
        C2491fm a36 = C4105zY.m41624a(Loot.class, "4b41cd2258947ab4c743591a3980d7a1", i47);
        f2671Lm = a36;
        fmVarArr[i47] = a36;
        int i48 = i47 + 1;
        C2491fm a37 = C4105zY.m41624a(Loot.class, "577f29a3c1ecba34204b38e4d08588a8", i48);
        _f_onResurrect_0020_0028_0029V = a37;
        fmVarArr[i48] = a37;
        int i49 = i48 + 1;
        C2491fm a38 = C4105zY.m41624a(Loot.class, "d1caf04e8e58c55befcedb22ebce4d22", i49);
        iKQ = a38;
        fmVarArr[i49] = a38;
        int i50 = i49 + 1;
        C2491fm a39 = C4105zY.m41624a(Loot.class, "8b1658c86a45566ae299c4487a1abea3", i50);
        f2673MM = a39;
        fmVarArr[i50] = a39;
        int i51 = i50 + 1;
        C2491fm a40 = C4105zY.m41624a(Loot.class, "458c2df80bd539fc42b21a3448cbfc49", i51);
        iKR = a40;
        fmVarArr[i51] = a40;
        int i52 = i51 + 1;
        C2491fm a41 = C4105zY.m41624a(Loot.class, "e351880bbe2145cdc0bd5ca2e54557ba", i52);
        f2685vs = a41;
        fmVarArr[i52] = a41;
        int i53 = i52 + 1;
        C2491fm a42 = C4105zY.m41624a(Loot.class, "b932fabb358827d125e5ca856446d2b6", i53);
        iKS = a42;
        fmVarArr[i53] = a42;
        int i54 = i53 + 1;
        C2491fm a43 = C4105zY.m41624a(Loot.class, "751610937e83ca0dc71321c800164bae", i54);
        iKT = a43;
        fmVarArr[i54] = a43;
        int i55 = i54 + 1;
        C2491fm a44 = C4105zY.m41624a(Loot.class, "8f86e5b5beca63468a3df08532713343", i55);
        _f_hasHollowField_0020_0028_0029Z = a44;
        fmVarArr[i55] = a44;
        int i56 = i55 + 1;
        C2491fm a45 = C4105zY.m41624a(Loot.class, "43c989a3854e577203b686397b778710", i56);
        aKn = a45;
        fmVarArr[i56] = a45;
        int i57 = i56 + 1;
        C2491fm a46 = C4105zY.m41624a(Loot.class, "531275ef677be817d0a19a3d00e90969", i57);
        _f_getType_0020_0028_0029Ljava_002flang_002fObject_003b = a46;
        fmVarArr[i57] = a46;
        int i58 = i57 + 1;
        _m_methods = fmVarArr;
        C1634Xv.m11725a((Object[]) Actor._m_methods, (Object[]) _m_methods);
        ___iScriptClass = aUO.m18566a(Loot.class, C6379aln.class, _m_fields, _m_methods);
    }

    @C0064Am(aul = "43c989a3854e577203b686397b778710", aum = 0)
    @C5566aOg
    @C2499fr
    /* renamed from: QT */
    private void m14062QT() {
        throw new aWi(new aCE(this, aKn, new Object[0]));
    }

    /* renamed from: a */
    private void m14067a(NPCEventDispatcher bp) {
        bFf().mo5608dq().mo3197f(iKu, bp);
    }

    @C0064Am(aul = "0f69f2be0f9518b696389bfd154bcb13", aum = 0)
    @C5566aOg
    @C2499fr
    /* renamed from: a */
    private void m14071a(Character acx, Item auq) {
        throw new aWi(new aCE(this, iKD, new Object[]{acx, auq}));
    }

    private float aQG() {
        return ((LootType) getType()).getLifeTime();
    }

    /* renamed from: ab */
    private void m14073ab(Character acx) {
        bFf().mo5608dq().mo3197f(iKn, acx);
    }

    /* renamed from: ac */
    private void m14074ac(Character acx) {
        bFf().mo5608dq().mo3197f(iKo, acx);
    }

    /* renamed from: ad */
    private void m14075ad(Character acx) {
        bFf().mo5608dq().mo3197f(iKw, acx);
    }

    /* renamed from: af */
    private C0286Dh.C0287a m14077af(Character acx) {
        switch (bFf().mo6893i(iKA)) {
            case 0:
                return null;
            case 2:
                return (C0286Dh.C0287a) bFf().mo5606d(new aCE(this, iKA, new Object[]{acx}));
            case 3:
                bFf().mo5606d(new aCE(this, iKA, new Object[]{acx}));
                break;
        }
        return m14076ae(acx);
    }

    @C0064Am(aul = "864da50bda8095f37d05ffab966a7d71", aum = 0)
    @C5566aOg
    /* renamed from: ag */
    private C0286Dh.C0287a m14078ag(Character acx) {
        throw new aWi(new aCE(this, iKB, new Object[]{acx}));
    }

    @C0064Am(aul = "c4db5a059db90626f7f60dd46fc75d41", aum = 0)
    @C5566aOg
    @C2499fr
    /* renamed from: ai */
    private boolean m14079ai(Character acx) {
        throw new aWi(new aCE(this, iKC, new Object[]{acx}));
    }

    @C0064Am(aul = "0c78472610adadbc6b676c54fb34c84d", aum = 0)
    @C5566aOg
    /* renamed from: ak */
    private void m14080ak(Character acx) {
        throw new aWi(new aCE(this, iKH, new Object[]{acx}));
    }

    /* renamed from: an */
    private boolean m14082an(Character acx) {
        switch (bFf().mo6893i(iKJ)) {
            case 0:
                return false;
            case 2:
                return ((Boolean) bFf().mo5606d(new aCE(this, iKJ, new Object[]{acx}))).booleanValue();
            case 3:
                bFf().mo5606d(new aCE(this, iKJ, new Object[]{acx}));
                break;
        }
        return m14081am(acx);
    }

    /* renamed from: ap */
    private boolean m14084ap(Character acx) {
        switch (bFf().mo6893i(iKR)) {
            case 0:
                return false;
            case 2:
                return ((Boolean) bFf().mo5606d(new aCE(this, iKR, new Object[]{acx}))).booleanValue();
            case 3:
                bFf().mo5606d(new aCE(this, iKR, new Object[]{acx}));
                break;
        }
        return m14083ao(acx);
    }

    @C0064Am(aul = "751610937e83ca0dc71321c800164bae", aum = 0)
    @C5566aOg
    /* renamed from: aq */
    private void m14085aq(Character acx) {
        throw new aWi(new aCE(this, iKT, new Object[]{acx}));
    }

    @C0064Am(aul = "9ef42ed5578e8ec51e240cf41983095a", aum = 0)
    @C5566aOg
    /* renamed from: c */
    private C0286Dh.C0287a m14088c(Character acx) {
        throw new aWi(new aCE(this, f2672MH, new Object[]{acx}));
    }

    /* renamed from: c */
    private void m14091c(LootItems ez) {
        bFf().mo5608dq().mo3197f(iKm, ez);
    }

    @C4034yP
    @C5566aOg
    @C0064Am(aul = "164f3afbbfe68655bf09871a2790db02", aum = 0)
    @C2499fr
    private void dtD() {
        throw new aWi(new aCE(this, iKI, new Object[0]));
    }

    @C4034yP
    @C5566aOg
    @C2499fr
    private void dtE() {
        switch (bFf().mo6893i(iKI)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, iKI, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, iKI, new Object[0]));
                break;
        }
        dtD();
    }

    @C4034yP
    @C5566aOg
    @C0064Am(aul = "b7666ff3e821a5ae29675dfa331a768f", aum = 0)
    @C2499fr
    private void dtF() {
        throw new aWi(new aCE(this, iKK, new Object[0]));
    }

    @C4034yP
    @C5566aOg
    @C0064Am(aul = "091ca5e8d0878765c0d5ba702380f1ba", aum = 0)
    @C2499fr
    private void dtH() {
        throw new aWi(new aCE(this, iKL, new Object[0]));
    }

    private LootItems dto() {
        return (LootItems) bFf().mo5608dq().mo3214p(iKm);
    }

    private Character dtp() {
        return (Character) bFf().mo5608dq().mo3214p(iKn);
    }

    private Character dtq() {
        return (Character) bFf().mo5608dq().mo3214p(iKo);
    }

    private String dtr() {
        return (String) bFf().mo5608dq().mo3214p(iKp);
    }

    private boolean dts() {
        return bFf().mo5608dq().mo3201h(iKq);
    }

    private NPCEventDispatcher dtt() {
        return (NPCEventDispatcher) bFf().mo5608dq().mo3214p(iKu);
    }

    private boolean dtu() {
        return bFf().mo5608dq().mo3201h(iKv);
    }

    private Character dtv() {
        return (Character) bFf().mo5608dq().mo3214p(iKw);
    }

    @C0064Am(aul = "ded744e27c1f9d2e27b708c8796a21e5", aum = 0)
    @C5566aOg
    private void dtw() {
        throw new aWi(new aCE(this, iKy, new Object[0]));
    }

    /* renamed from: fm */
    private void m14093fm(float f) {
        throw new C6039afL();
    }

    /* renamed from: kd */
    private void m14098kd(boolean z) {
        bFf().mo5608dq().mo3153a(iKq, z);
    }

    /* renamed from: ke */
    private void m14099ke(boolean z) {
        bFf().mo5608dq().mo3153a(iKv, z);
    }

    /* renamed from: nz */
    private void m14103nz(String str) {
        bFf().mo5608dq().mo3197f(iKp, str);
    }

    @C0064Am(aul = "02005983a3eafc2d66433b54b09dc31d", aum = 0)
    @C5566aOg
    /* renamed from: o */
    private void m14104o(Set<Item> set) {
        throw new aWi(new aCE(this, iKF, new Object[]{set}));
    }

    @C0064Am(aul = "4b41cd2258947ab4c743591a3980d7a1", aum = 0)
    @C5566aOg
    @C2499fr
    /* renamed from: qU */
    private void m14105qU() {
        throw new aWi(new aCE(this, f2671Lm, new Object[0]));
    }

    @C0064Am(aul = "531275ef677be817d0a19a3d00e90969", aum = 0)
    /* renamed from: qW */
    private Object m14106qW() {
        return dty();
    }

    /* renamed from: r */
    private void m14107r(Asset tCVar) {
        throw new C6039afL();
    }

    /* renamed from: rh */
    private Asset m14108rh() {
        return ((LootType) getType()).mo3521Nu();
    }

    @C5566aOg
    @C2499fr
    /* renamed from: QU */
    public void mo8569QU() {
        switch (bFf().mo6893i(aKn)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, aKn, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, aKn, new Object[0]));
                break;
        }
        m14062QT();
    }

    @ClientOnly
    /* renamed from: QW */
    public boolean mo961QW() {
        switch (bFf().mo6893i(_f_hasHollowField_0020_0028_0029Z)) {
            case 0:
                return false;
            case 2:
                return ((Boolean) bFf().mo5606d(new aCE(this, _f_hasHollowField_0020_0028_0029Z, new Object[0]))).booleanValue();
            case 3:
                bFf().mo5606d(new aCE(this, _f_hasHollowField_0020_0028_0029Z, new Object[0]));
                break;
        }
        return m14063QV();
    }

    /* renamed from: S */
    public void mo10S() {
        super.mo10S();
    }

    /* renamed from: T */
    public C6494any mo11T() {
        return ___iScriptClass;
    }

    /* renamed from: U */
    public C2491fm[] mo12U() {
        return _m_methods;
    }

    /* renamed from: V */
    public void mo4709V(float f) {
        switch (bFf().mo6893i(_f_tick_0020_0028F_0029V)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, _f_tick_0020_0028F_0029V, new Object[]{new Float(f)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, _f_tick_0020_0028F_0029V, new Object[]{new Float(f)}));
                break;
        }
        m14064U(f);
    }

    /* renamed from: W */
    public Object mo13W() {
        return new C6379aln(this);
    }

    /* renamed from: Y */
    public boolean mo599Y(float f) {
        switch (bFf().mo6893i(f2673MM)) {
            case 0:
                return false;
            case 2:
                return ((Boolean) bFf().mo5606d(new aCE(this, f2673MM, new Object[]{new Float(f)}))).booleanValue();
            case 3:
                bFf().mo5606d(new aCE(this, f2673MM, new Object[]{new Float(f)}));
                break;
        }
        return m14066X(f);
    }

    /* renamed from: a */
    public Object mo14a(C0495Gr gr) {
        Object[] args = gr.getArgs();
        switch (gr.mo2417hq() - Actor._m_methodCount) {
            case 0:
                m14100kf(((Boolean) args[0]).booleanValue());
                return null;
            case 1:
                dtw();
                return null;
            case 2:
                return dtx();
            case 3:
                m14070a((StellarSystem) args[0]);
                return null;
            case 4:
                m14110zw();
                return null;
            case 5:
                return m14109uV();
            case 6:
                return m14076ae((Character) args[0]);
            case 7:
                return m14088c((Character) args[0]);
            case 8:
                return m14078ag((Character) args[0]);
            case 9:
                return new Boolean(m14079ai((Character) args[0]));
            case 10:
                m14071a((Character) args[0], (Item) args[1]);
                return null;
            case 11:
                m14090c((Actor) args[0], (C5260aCm) args[1], (Vec3f) args[2], (Vec3f) args[3]);
                return null;
            case 12:
                return m14094io();
            case 13:
                return m14095iq();
            case 14:
                return dtz();
            case 15:
                m14104o((Set) args[0]);
                return null;
            case 16:
                return dtB();
            case 17:
                m14080ak((Character) args[0]);
                return null;
            case 18:
                m14064U(((Float) args[0]).floatValue());
                return null;
            case 19:
                return m14089c((Space) args[0], ((Long) args[1]).longValue());
            case 20:
                m14086ay(((Float) args[0]).floatValue());
                return null;
            case 21:
                dtD();
                return null;
            case 22:
                return new Boolean(m14081am((Character) args[0]));
            case 23:
                return new Boolean(ayG());
            case 24:
                dtF();
                return null;
            case 25:
                dtH();
                return null;
            case 26:
                m14101kh(((Boolean) args[0]).booleanValue());
                return null;
            case 27:
                return ail();
            case 28:
                m14092fg();
                return null;
            case 29:
                return dtJ();
            case 30:
                m14102nA((String) args[0]);
                return null;
            case 31:
                return m14097iz();
            case 32:
                m14087b((NPCEventDispatcher) args[0]);
                return null;
            case 33:
                m14069a((Actor) args[0], (C5260aCm) args[1], (Vec3f) args[2], (Vec3f) args[3]);
                return null;
            case 34:
                return m14096it();
            case 35:
                m14105qU();
                return null;
            case 36:
                m14072aG();
                return null;
            case 37:
                dtL();
                return null;
            case 38:
                return new Boolean(m14066X(((Float) args[0]).floatValue()));
            case 39:
                return new Boolean(m14083ao((Character) args[0]));
            case 40:
                m14068a((Actor.C0200h) args[0]);
                return null;
            case 41:
                return dtN();
            case 42:
                m14085aq((Character) args[0]);
                return null;
            case 43:
                return new Boolean(m14063QV());
            case 44:
                m14062QT();
                return null;
            case 45:
                return m14106qW();
            default:
                return super.mo14a(gr);
        }
    }

    /* renamed from: a */
    public void mo15a(Collection collection, C0495Gr gr) {
        super.mo15a(collection, gr);
    }

    /* renamed from: aH */
    public void mo70aH() {
        switch (bFf().mo6893i(_f_onResurrect_0020_0028_0029V)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, _f_onResurrect_0020_0028_0029V, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, _f_onResurrect_0020_0028_0029V, new Object[0]));
                break;
        }
        m14072aG();
    }

    @C5566aOg
    /* renamed from: ah */
    public C0286Dh.C0287a mo8570ah(Character acx) {
        switch (bFf().mo6893i(iKB)) {
            case 0:
                return null;
            case 2:
                return (C0286Dh.C0287a) bFf().mo5606d(new aCE(this, iKB, new Object[]{acx}));
            case 3:
                bFf().mo5606d(new aCE(this, iKB, new Object[]{acx}));
                break;
        }
        return m14078ag(acx);
    }

    @C5566aOg
    @C2499fr
    /* renamed from: aj */
    public boolean mo8571aj(Character acx) {
        switch (bFf().mo6893i(iKC)) {
            case 0:
                return false;
            case 2:
                return ((Boolean) bFf().mo5606d(new aCE(this, iKC, new Object[]{acx}))).booleanValue();
            case 3:
                bFf().mo5606d(new aCE(this, iKC, new Object[]{acx}));
                break;
        }
        return m14079ai(acx);
    }

    @C5566aOg
    /* renamed from: al */
    public void mo8572al(Character acx) {
        switch (bFf().mo6893i(iKH)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, iKH, new Object[]{acx}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, iKH, new Object[]{acx}));
                break;
        }
        m14080ak(acx);
    }

    @C5566aOg
    /* renamed from: ar */
    public void mo8573ar(Character acx) {
        switch (bFf().mo6893i(iKT)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, iKT, new Object[]{acx}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, iKT, new Object[]{acx}));
                break;
        }
        m14085aq(acx);
    }

    /* renamed from: b */
    public void mo620b(Actor.C0200h hVar) {
        switch (bFf().mo6893i(f2685vs)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f2685vs, new Object[]{hVar}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f2685vs, new Object[]{hVar}));
                break;
        }
        m14068a(hVar);
    }

    /* renamed from: b */
    public void mo621b(Actor cr, C5260aCm acm, Vec3f vec3f, Vec3f vec3f2) {
        switch (bFf().mo6893i(f2682vm)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f2682vm, new Object[]{cr, acm, vec3f, vec3f2}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f2682vm, new Object[]{cr, acm, vec3f, vec3f2}));
                break;
        }
        m14069a(cr, acm, vec3f, vec3f2);
    }

    /* renamed from: b */
    public void mo993b(StellarSystem jj) {
        switch (bFf().mo6893i(f2674Mq)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f2674Mq, new Object[]{jj}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f2674Mq, new Object[]{jj}));
                break;
        }
        m14070a(jj);
    }

    @C5566aOg
    @C2499fr
    /* renamed from: b */
    public void mo8574b(Character acx, Item auq) {
        switch (bFf().mo6893i(iKD)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, iKD, new Object[]{acx, auq}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, iKD, new Object[]{acx, auq}));
                break;
        }
        m14071a(acx, auq);
    }

    /* renamed from: c */
    public void mo8575c(NPCEventDispatcher bp) {
        switch (bFf().mo6893i(iKP)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, iKP, new Object[]{bp}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, iKP, new Object[]{bp}));
                break;
        }
        m14087b(bp);
    }

    /* renamed from: c */
    public C5663aRz[] mo25c() {
        return _m_fields;
    }

    @C5566aOg
    public void cua() {
        switch (bFf().mo6893i(iKy)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, iKy, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, iKy, new Object[0]));
                break;
        }
        dtw();
    }

    @C5566aOg
    /* renamed from: d */
    public C0286Dh.C0287a mo634d(Character acx) {
        switch (bFf().mo6893i(f2672MH)) {
            case 0:
                return null;
            case 2:
                return (C0286Dh.C0287a) bFf().mo5606d(new aCE(this, f2672MH, new Object[]{acx}));
            case 3:
                bFf().mo5606d(new aCE(this, f2672MH, new Object[]{acx}));
                break;
        }
        return m14088c(acx);
    }

    /* access modifiers changed from: protected */
    /* renamed from: d */
    public C0520HN mo635d(Space ea, long j) {
        switch (bFf().mo6893i(f2678uY)) {
            case 0:
                return null;
            case 2:
                return (C0520HN) bFf().mo5606d(new aCE(this, f2678uY, new Object[]{ea, new Long(j)}));
            case 3:
                bFf().mo5606d(new aCE(this, f2678uY, new Object[]{ea, new Long(j)}));
                break;
        }
        return m14089c(ea, j);
    }

    /* renamed from: d */
    public void mo636d(Actor cr, C5260aCm acm, Vec3f vec3f, Vec3f vec3f2) {
        switch (bFf().mo6893i(f2683vn)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f2683vn, new Object[]{cr, acm, vec3f, vec3f2}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f2683vn, new Object[]{cr, acm, vec3f, vec3f2}));
                break;
        }
        m14090c(cr, acm, vec3f, vec3f2);
    }

    public void dispose() {
        switch (bFf().mo6893i(_f_dispose_0020_0028_0029V)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, _f_dispose_0020_0028_0029V, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, _f_dispose_0020_0028_0029V, new Object[0]));
                break;
        }
        m14092fg();
    }

    public LootItems dtA() {
        switch (bFf().mo6893i(iKE)) {
            case 0:
                return null;
            case 2:
                return (LootItems) bFf().mo5606d(new aCE(this, iKE, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, iKE, new Object[0]));
                break;
        }
        return dtz();
    }

    public Character dtC() {
        switch (bFf().mo6893i(iKG)) {
            case 0:
                return null;
            case 2:
                return (Character) bFf().mo5606d(new aCE(this, iKG, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, iKG, new Object[0]));
                break;
        }
        return dtB();
    }

    @C4034yP
    @C5566aOg
    @C2499fr
    public void dtG() {
        switch (bFf().mo6893i(iKK)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, iKK, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, iKK, new Object[0]));
                break;
        }
        dtF();
    }

    @C4034yP
    @C5566aOg
    @C2499fr
    public void dtI() {
        switch (bFf().mo6893i(iKL)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, iKL, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, iKL, new Object[0]));
                break;
        }
        dtH();
    }

    public String dtK() {
        switch (bFf().mo6893i(iKN)) {
            case 0:
                return null;
            case 2:
                return (String) bFf().mo5606d(new aCE(this, iKN, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, iKN, new Object[0]));
                break;
        }
        return dtJ();
    }

    public void dtM() {
        switch (bFf().mo6893i(iKQ)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, iKQ, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, iKQ, new Object[0]));
                break;
        }
        dtL();
    }

    public Character dtO() {
        switch (bFf().mo6893i(iKS)) {
            case 0:
                return null;
            case 2:
                return (Character) bFf().mo5606d(new aCE(this, iKS, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, iKS, new Object[0]));
                break;
        }
        return dtN();
    }

    public LootType dty() {
        switch (bFf().mo6893i(iKz)) {
            case 0:
                return null;
            case 2:
                return (LootType) bFf().mo5606d(new aCE(this, iKz, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, iKz, new Object[0]));
                break;
        }
        return dtx();
    }

    @ClientOnly
    public Color getColor() {
        switch (bFf().mo6893i(brY)) {
            case 0:
                return null;
            case 2:
                return (Color) bFf().mo5606d(new aCE(this, brY, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, brY, new Object[0]));
                break;
        }
        return ail();
    }

    public String getName() {
        switch (bFf().mo6893i(f2675Pf)) {
            case 0:
                return null;
            case 2:
                return (String) bFf().mo5606d(new aCE(this, f2675Pf, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f2675Pf, new Object[0]));
                break;
        }
        return m14109uV();
    }

    public /* bridge */ /* synthetic */ Object getType() {
        switch (bFf().mo6893i(_f_getType_0020_0028_0029Ljava_002flang_002fObject_003b)) {
            case 0:
                return null;
            case 2:
                return bFf().mo5606d(new aCE(this, _f_getType_0020_0028_0029Ljava_002flang_002fObject_003b, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, _f_getType_0020_0028_0029Ljava_002flang_002fObject_003b, new Object[0]));
                break;
        }
        return m14106qW();
    }

    /* renamed from: iA */
    public String mo647iA() {
        switch (bFf().mo6893i(f2684vo)) {
            case 0:
                return null;
            case 2:
                return (String) bFf().mo5606d(new aCE(this, f2684vo, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f2684vo, new Object[0]));
                break;
        }
        return m14097iz();
    }

    /* renamed from: ip */
    public String mo648ip() {
        switch (bFf().mo6893i(f2679vc)) {
            case 0:
                return null;
            case 2:
                return (String) bFf().mo5606d(new aCE(this, f2679vc, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f2679vc, new Object[0]));
                break;
        }
        return m14094io();
    }

    /* renamed from: ir */
    public String mo649ir() {
        switch (bFf().mo6893i(f2680ve)) {
            case 0:
                return null;
            case 2:
                return (String) bFf().mo5606d(new aCE(this, f2680ve, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f2680ve, new Object[0]));
                break;
        }
        return m14095iq();
    }

    public boolean isLocked() {
        switch (bFf().mo6893i(clC)) {
            case 0:
                return false;
            case 2:
                return ((Boolean) bFf().mo5606d(new aCE(this, clC, new Object[0]))).booleanValue();
            case 3:
                bFf().mo5606d(new aCE(this, clC, new Object[0]));
                break;
        }
        return ayG();
    }

    /* renamed from: iu */
    public String mo651iu() {
        switch (bFf().mo6893i(f2681vi)) {
            case 0:
                return null;
            case 2:
                return (String) bFf().mo5606d(new aCE(this, f2681vi, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f2681vi, new Object[0]));
                break;
        }
        return m14096it();
    }

    /* renamed from: kg */
    public void mo8587kg(boolean z) {
        switch (bFf().mo6893i(iKx)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, iKx, new Object[]{new Boolean(z)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, iKx, new Object[]{new Boolean(z)}));
                break;
        }
        m14100kf(z);
    }

    @ClientOnly
    /* renamed from: ki */
    public void mo8588ki(boolean z) {
        switch (bFf().mo6893i(iKM)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, iKM, new Object[]{new Boolean(z)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, iKM, new Object[]{new Boolean(z)}));
                break;
        }
        m14101kh(z);
    }

    /* renamed from: nB */
    public void mo8589nB(String str) {
        switch (bFf().mo6893i(iKO)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, iKO, new Object[]{str}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, iKO, new Object[]{str}));
                break;
        }
        m14102nA(str);
    }

    @C5566aOg
    /* renamed from: p */
    public void mo8590p(Set<Item> set) {
        switch (bFf().mo6893i(iKF)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, iKF, new Object[]{set}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, iKF, new Object[]{set}));
                break;
        }
        m14104o(set);
    }

    @C5566aOg
    @C2499fr
    /* renamed from: qV */
    public void mo656qV() {
        switch (bFf().mo6893i(f2671Lm)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f2671Lm, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f2671Lm, new Object[0]));
                break;
        }
        m14105qU();
    }

    public void step(float f) {
        switch (bFf().mo6893i(_f_step_0020_0028F_0029V)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, _f_step_0020_0028F_0029V, new Object[]{new Float(f)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, _f_step_0020_0028F_0029V, new Object[]{new Float(f)}));
                break;
        }
        m14086ay(f);
    }

    /* renamed from: zx */
    public void mo1099zx() {
        switch (bFf().mo6893i(f2676Wp)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f2676Wp, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f2676Wp, new Object[0]));
                break;
        }
        m14110zw();
    }

    /* renamed from: h */
    public void mo8585h(LootType ahc) {
        super.mo967a((C2961mJ) ahc);
        setStatic(true);
        this.elapsedTime = 0.0f;
        LootItems ez = (LootItems) bFf().mo6865M(LootItems.class);
        ez.mo1888e(this);
        m14091c(ez);
    }

    @C0064Am(aul = "5bc3cace1538866e5bf2f39b9213940f", aum = 0)
    /* renamed from: kf */
    private void m14100kf(boolean z) {
        m14099ke(z);
    }

    @C0064Am(aul = "68db3d019708fa3537b2af2290847d85", aum = 0)
    private LootType dtx() {
        return (LootType) super.getType();
    }

    @C0064Am(aul = "e5223563ebd96cf36dff492733e253a6", aum = 0)
    /* renamed from: a */
    private void m14070a(StellarSystem jj) {
        super.mo993b(jj);
        if (bHa()) {
            m14098kd(false);
        } else {
            this.iKr = false;
            this.iKs = aQG() * 0.5f;
        }
        this.iKt = aQG();
        mo8361ms(1.0f);
    }

    @C0064Am(aul = "9d8f15badaa1365c31128691a634bf60", aum = 0)
    /* renamed from: zw */
    private void m14110zw() {
        super.mo1099zx();
    }

    @C0064Am(aul = "0c66e9839451688affe81236ad80f67a", aum = 0)
    /* renamed from: uV */
    private String m14109uV() {
        NLSLoot avt = (NLSLoot) ala().aIY().mo6310c(NLSManager.C1472a.LOOT);
        if (dtq() != null) {
            return C5956adg.format(avt.dCZ().get(), dtq().getName());
        } else if (dtu()) {
            return C5956adg.format(avt.dDb().get(), dtK());
        } else {
            return C5956adg.format(avt.dDd().get(), dtK());
        }
    }

    @C0064Am(aul = "1722d11162b61ad464ddaab15cda4928", aum = 0)
    /* renamed from: ae */
    private C0286Dh.C0287a m14076ae(Character acx) {
        if (dts()) {
            return C0286Dh.C0287a.ERROR_OBJECT_IN_USE;
        }
        if (acx.bQx() == null) {
            mo6317hy("The causer trying to interact to the loot '" + getName() + "' (" + acx.getName() + ") doesn't have an active ship");
            return C0286Dh.C0287a.ERROR_UNKNOWN;
        } else if (!m14082an(acx)) {
            return C0286Dh.C0287a.ERROR_CANT_GET_LOOT;
        } else {
            return C0286Dh.C0287a.OK;
        }
    }

    @C0064Am(aul = "ff43f9ec526df758118733efc85316a8", aum = 0)
    /* renamed from: c */
    private void m14090c(Actor cr, C5260aCm acm, Vec3f vec3f, Vec3f vec3f2) {
    }

    @C0064Am(aul = "9af4af4c118aa01ff7b8137bf0648b01", aum = 0)
    /* renamed from: io */
    private String m14094io() {
        return m14108rh().getHandle();
    }

    @C0064Am(aul = "a1549f96eb3c153f8eb0b256ab3d0968", aum = 0)
    /* renamed from: iq */
    private String m14095iq() {
        return m14108rh().getFile();
    }

    @C0064Am(aul = "9d9aa475481664f5c280ddd178aa10db", aum = 0)
    private LootItems dtz() {
        return dto();
    }

    @C0064Am(aul = "d75d0b91e472e04033b8a4c37e9b2f2c", aum = 0)
    private Character dtB() {
        return dtq();
    }

    @C0064Am(aul = "33d00225fd0af1e06fd2650dc7ad30d0", aum = 0)
    /* renamed from: U */
    private void m14064U(float f) {
        if (bae()) {
            super.mo4709V(f);
            this.elapsedTime += f;
            if (bGZ()) {
                if (!dts()) {
                    if (dto().isEmpty()) {
                        mo1099zx();
                        if (dtt() != null) {
                            dtt().mo732u(dtv());
                            return;
                        }
                        return;
                    } else if (this.elapsedTime > this.iKt) {
                        mo1099zx();
                        if (dtt() != null) {
                            dtt().mo732u((Character) null);
                            return;
                        }
                        return;
                    }
                }
            } else if (!this.iKr && this.elapsedTime > this.iKs) {
                float f2 = 1.0f - (this.elapsedTime / this.iKt);
                if (f2 >= 0.0f) {
                    mo1087lt(f2 + 0.3f);
                }
            }
            mo8361ms(1.0f);
        }
    }

    @C0064Am(aul = "37a7c53d5f0f5e18938a2bd86cb0746b", aum = 0)
    /* renamed from: c */
    private C0520HN m14089c(Space ea, long j) {
        aHO aho = new aHO(ea, this, mo958IL());
        aNW a = ea.cKn().mo5725a(j, (C2235dB) aho, 15);
        a.mo3842lz(1000);
        aho.mo2449a(a);
        return aho;
    }

    @C0064Am(aul = "e8221368bd7ce4eb61f3eb8b78415362", aum = 0)
    /* renamed from: ay */
    private void m14086ay(float f) {
        super.step(f);
        if (bGY() && dtq() != null && !m14084ap(dtq()) && dts()) {
            dtE();
        }
    }

    @C0064Am(aul = "0abf84650b575097278cb7f2f7507360", aum = 0)
    /* renamed from: am */
    private boolean m14081am(Character acx) {
        if (dtq() == null) {
            return true;
        }
        if (acx == dtq()) {
            return true;
        }
        if (!(acx instanceof Player)) {
            return false;
        }
        Player aku = (Player) dtq();
        return aku.dxk() && aku.dxi().mo2426aV((Player) acx);
    }

    @C0064Am(aul = "694c142573f60a2ed0b98e58e609f145", aum = 0)
    private boolean ayG() {
        return dts();
    }

    @C0064Am(aul = "5736f871346fd5696ee719145d2dddca", aum = 0)
    @ClientOnly
    /* renamed from: kh */
    private void m14101kh(boolean z) {
        this.iKr = z;
    }

    @C0064Am(aul = "2a68f10a13e61d8ff41d19a4730ba8f7", aum = 0)
    @ClientOnly
    private Color ail() {
        if (dtC() == null || dtC() != getPlayer()) {
            return super.getColor();
        }
        return ala().aIW().avc();
    }

    @C0064Am(aul = "859edaba82b6c8b3bbdd82e93b0ff06f", aum = 0)
    /* renamed from: fg */
    private void m14092fg() {
        dto().dispose();
        mo8572al((Character) null);
        mo8573ar((Character) null);
        super.dispose();
    }

    @C0064Am(aul = "c63f74698ad96cf269781512d44f5e49", aum = 0)
    private String dtJ() {
        return dtr();
    }

    @C0064Am(aul = "1d9b7771faa261ccc20967142907233e", aum = 0)
    /* renamed from: nA */
    private void m14102nA(String str) {
        m14103nz(str);
    }

    @C0064Am(aul = "ff701f58430f08ede790bd776e3b44f0", aum = 0)
    /* renamed from: iz */
    private String m14097iz() {
        return ala().aIW().avq();
    }

    @C0064Am(aul = "f853283e1fdf93a3630d76e8415b7799", aum = 0)
    /* renamed from: b */
    private void m14087b(NPCEventDispatcher bp) {
        m14067a(bp);
    }

    @C0064Am(aul = "f906e5a54a151f4088c16956ad50f4fb", aum = 0)
    /* renamed from: a */
    private void m14069a(Actor cr, C5260aCm acm, Vec3f vec3f, Vec3f vec3f2) {
    }

    @C0064Am(aul = "1ec14773ccb4258fc419d142b6876a2c", aum = 0)
    /* renamed from: it */
    private String m14096it() {
        return null;
    }

    @C0064Am(aul = "577f29a3c1ecba34204b38e4d08588a8", aum = 0)
    /* renamed from: aG */
    private void m14072aG() {
        super.mo70aH();
        mo1099zx();
        if (mo960Nc() != null) {
            mo1099zx();
        }
        dispose();
    }

    @C0064Am(aul = "d1caf04e8e58c55befcedb22ebce4d22", aum = 0)
    private void dtL() {
        dtG();
    }

    @C0064Am(aul = "8b1658c86a45566ae299c4487a1abea3", aum = 0)
    /* renamed from: X */
    private boolean m14066X(float f) {
        return f <= PlayerController.m39266c((C0286Dh) this);
    }

    @C0064Am(aul = "458c2df80bd539fc42b21a3448cbfc49", aum = 0)
    /* renamed from: ao */
    private boolean m14083ao(Character acx) {
        if (!bHb()) {
            mo8352e("The transactional server (still) doesnt know actor's position, so Loot.inRage will return false for safety. stack dump for debugging", (Throwable) new Exception());
            return false;
        }
        Ship bQx = acx.bQx();
        if (bQx == null || bQx.mo1013bx((Actor) this) > PlayerController.m39266c((C0286Dh) this)) {
            return false;
        }
        return true;
    }

    @C0064Am(aul = "e351880bbe2145cdc0bd5ca2e54557ba", aum = 0)
    /* renamed from: a */
    private void m14068a(Actor.C0200h hVar) {
        C6348alI ali = new C6348alI(2.0f);
        ali.setTransparent(true);
        hVar.mo1103c(ali);
    }

    @C0064Am(aul = "b932fabb358827d125e5ca856446d2b6", aum = 0)
    private Character dtN() {
        return dtp();
    }

    @C0064Am(aul = "8f86e5b5beca63468a3df08532713343", aum = 0)
    @ClientOnly
    /* renamed from: QV */
    private boolean m14063QV() {
        if (super.mo961QW()) {
            return true;
        }
        if (dtA() != null && (C1298TD.m9830t(dtA()).bFT() || dtA().mo961QW())) {
            return true;
        }
        if (dtC() == null || !C1298TD.m9830t(dtC()).bFT()) {
            return false;
        }
        return true;
    }

    /* renamed from: a.aEL$a */
    class C1792a implements Comparator<Item> {
        C1792a() {
        }

        /* renamed from: a */
        public int compare(Item auq, Item auq2) {
            long HG = auq.bAP().mo19868HG();
            long HG2 = auq2.bAP().mo19868HG();
            if (HG > HG2) {
                return 1;
            }
            if (HG < HG2) {
                return -1;
            }
            if (auq.mo8317Ej() <= auq2.mo8317Ej()) {
                return -1;
            }
            return 1;
        }
    }
}
