package game.script.space;

import game.network.message.externalizable.aCE;
import game.script.itemgen.ItemGenTable;
import game.script.template.BaseTaikodomContent;
import logic.baa.*;
import logic.data.mbean.C6665arN;
import logic.res.code.C5663aRz;
import logic.res.code.C6494any;
import logic.res.html.C2491fm;
import p001a.*;

import java.util.Collection;

@C5829abJ("1.0.0")
@C5511aMd
@C6485anp
/* renamed from: a.Yi */
/* compiled from: a */
public abstract class BaseAsteroidType extends BaseTaikodomContent implements C1616Xf, aOW {

    /* renamed from: Do */
    public static final C2491fm f2188Do = null;
    public static final int _m_fieldCount = 0;
    public static final C5663aRz[] _m_fields = null;
    public static final int _m_methodCount = 0;
    public static final C2491fm[] _m_methods = null;
    public static final C2491fm eLi = null;
    public static final long serialVersionUID = 0;
    public static C6494any ___iScriptClass;

    static {
        m11921V();
    }

    public BaseAsteroidType() {
        super((C5540aNg) null);
        super.mo10S();
    }

    public BaseAsteroidType(C5540aNg ang) {
        super(ang);
    }

    /* renamed from: V */
    static void m11921V() {
        serialVersionUID = (long) 1;
        _m_fieldCount = BaseTaikodomContent._m_fieldCount + 0;
        _m_methodCount = BaseTaikodomContent._m_methodCount + 2;
        _m_fields = new C5663aRz[(BaseTaikodomContent._m_fieldCount + 0)];
        C1634Xv.m11725a((Object[]) BaseTaikodomContent._m_fields, (Object[]) _m_fields);
        int i = BaseTaikodomContent._m_methodCount;
        C2491fm[] fmVarArr = new C2491fm[(i + 2)];
        C2491fm a = C4105zY.m41624a(BaseAsteroidType.class, "e5bf38d1aff64ff679b6a125b3da93b4", i);
        f2188Do = a;
        fmVarArr[i] = a;
        int i2 = i + 1;
        C2491fm a2 = C4105zY.m41624a(BaseAsteroidType.class, "80a96f8ce57d64ea212daa2446098a5e", i2);
        eLi = a2;
        fmVarArr[i2] = a2;
        int i3 = i2 + 1;
        _m_methods = fmVarArr;
        C1634Xv.m11725a((Object[]) BaseTaikodomContent._m_methods, (Object[]) _m_methods);
        ___iScriptClass = aUO.m18566a(BaseAsteroidType.class, C6665arN.class, _m_fields, _m_methods);
    }

    @C0064Am(aul = "80a96f8ce57d64ea212daa2446098a5e", aum = 0)
    /* renamed from: a */
    private void m11923a(ItemGenTable oqVar) {
        throw new aWi(new aCE(this, eLi, new Object[]{oqVar}));
    }

    /* renamed from: T */
    public C6494any mo11T() {
        return ___iScriptClass;
    }

    /* renamed from: U */
    public C2491fm[] mo12U() {
        return _m_methods;
    }

    /* renamed from: W */
    public Object mo13W() {
        return new C6665arN(this);
    }

    /* renamed from: a */
    public Object mo14a(C0495Gr gr) {
        Object[] args = gr.getArgs();
        switch (gr.mo2417hq() - BaseTaikodomContent._m_methodCount) {
            case 0:
                m11922a((C0665JT) args[0]);
                return null;
            case 1:
                m11923a((ItemGenTable) args[0]);
                return null;
            default:
                return super.mo14a(gr);
        }
    }

    /* renamed from: a */
    public void mo15a(Collection collection, C0495Gr gr) {
        super.mo15a(collection, gr);
    }

    /* renamed from: b */
    public void mo24b(C0665JT jt) {
        switch (bFf().mo6893i(f2188Do)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f2188Do, new Object[]{jt}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f2188Do, new Object[]{jt}));
                break;
        }
        m11922a(jt);
    }

    /* renamed from: b */
    public void mo7248b(ItemGenTable oqVar) {
        switch (bFf().mo6893i(eLi)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, eLi, new Object[]{oqVar}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, eLi, new Object[]{oqVar}));
                break;
        }
        m11923a(oqVar);
    }

    /* renamed from: c */
    public C5663aRz[] mo25c() {
        return _m_fields;
    }

    /* renamed from: S */
    public void mo10S() {
        super.mo10S();
    }

    @C0064Am(aul = "e5bf38d1aff64ff679b6a125b3da93b4", aum = 0)
    /* renamed from: a */
    private void m11922a(C0665JT jt) {
        C0665JT jt2;
        if ("".equals(jt.getVersion()) && (jt2 = (C0665JT) jt.get("itemGenTable")) != null) {
            mo7248b((ItemGenTable) jt2.baw());
        }
    }
}
