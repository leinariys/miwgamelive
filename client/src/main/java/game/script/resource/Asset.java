package game.script.resource;

import game.network.message.externalizable.aCE;
import game.script.TaikodomObject;
import logic.baa.*;
import logic.data.mbean.C2306dp;
import logic.res.code.C5640aRc;
import logic.res.code.C5663aRz;
import logic.res.code.C6494any;
import logic.res.html.C2491fm;
import p001a.*;

import java.util.Collection;
import java.util.UUID;

@C5511aMd
@C6485anp
/* renamed from: a.tC */
/* compiled from: a */
public class Asset extends TaikodomObject implements C0468GU, C1616Xf, C5990aeO {
    public static final C2491fm _f_toString_0020_0028_0029Ljava_002flang_002fString_003b = null;
    public static final int _m_fieldCount = 0;
    public static final C5663aRz[] _m_fields = null;
    public static final int _m_methodCount = 0;
    public static final C2491fm[] _m_methods = null;
    /* renamed from: bL */
    public static final C5663aRz f9210bL = null;
    /* renamed from: bM */
    public static final C5663aRz f9211bM = null;
    /* renamed from: bN */
    public static final C2491fm f9212bN = null;
    /* renamed from: bO */
    public static final C2491fm f9213bO = null;
    /* renamed from: bP */
    public static final C2491fm f9214bP = null;
    /* renamed from: bQ */
    public static final C2491fm f9215bQ = null;
    public static final C5663aRz bns = null;
    public static final C2491fm bnt = null;
    public static final C2491fm bnu = null;
    public static final long serialVersionUID = 0;
    public static C6494any ___iScriptClass;
    @C0064Am(aul = "52b09501f97d6963c27394a6a834afd8", aum = 0)

    /* renamed from: bK */
    private static UUID f9209bK;
    @C0064Am(aul = "44fbbf9a20076b409d8dad802551abc8", aum = 2)
    private static String file;
    @C0064Am(aul = "d327065d22c44b426792549a808a385d", aum = 1)
    private static String handle;

    static {
        m39480V();
    }

    public Asset() {
        super((C5540aNg) null);
        super.mo10S();
    }

    public Asset(C5540aNg ang) {
        super(ang);
    }

    public Asset(String str, String str2) {
        super((C5540aNg) null);
        super._m_script_init(str, str2);
    }

    /* renamed from: V */
    static void m39480V() {
        serialVersionUID = (long) 1;
        _m_fieldCount = TaikodomObject._m_fieldCount + 3;
        _m_methodCount = TaikodomObject._m_methodCount + 7;
        int i = TaikodomObject._m_fieldCount;
        C5663aRz[] arzArr = new C5663aRz[(i + 3)];
        C5663aRz b = C5640aRc.m17844b(Asset.class, "52b09501f97d6963c27394a6a834afd8", i);
        f9210bL = b;
        arzArr[i] = b;
        int i2 = i + 1;
        C5663aRz b2 = C5640aRc.m17844b(Asset.class, "d327065d22c44b426792549a808a385d", i2);
        f9211bM = b2;
        arzArr[i2] = b2;
        int i3 = i2 + 1;
        C5663aRz b3 = C5640aRc.m17844b(Asset.class, "44fbbf9a20076b409d8dad802551abc8", i3);
        bns = b3;
        arzArr[i3] = b3;
        int i4 = i3 + 1;
        _m_fields = arzArr;
        C1634Xv.m11725a((Object[]) TaikodomObject._m_fields, (Object[]) _m_fields);
        int i5 = TaikodomObject._m_methodCount;
        C2491fm[] fmVarArr = new C2491fm[(i5 + 7)];
        C2491fm a = C4105zY.m41624a(Asset.class, "004e87d56aaa5b11824d9965e878032a", i5);
        f9212bN = a;
        fmVarArr[i5] = a;
        int i6 = i5 + 1;
        C2491fm a2 = C4105zY.m41624a(Asset.class, "57a533e4d65cca7ea6ce2d1ae00d9040", i6);
        f9213bO = a2;
        fmVarArr[i6] = a2;
        int i7 = i6 + 1;
        C2491fm a3 = C4105zY.m41624a(Asset.class, "8a2b8dc1bd6506363abfcd2be0424078", i7);
        _f_toString_0020_0028_0029Ljava_002flang_002fString_003b = a3;
        fmVarArr[i7] = a3;
        int i8 = i7 + 1;
        C2491fm a4 = C4105zY.m41624a(Asset.class, "c115c921749c59dfb5ffcf7df0a21f90", i8);
        f9214bP = a4;
        fmVarArr[i8] = a4;
        int i9 = i8 + 1;
        C2491fm a5 = C4105zY.m41624a(Asset.class, "c23bf9d1dae5c895334c608e620005ed", i9);
        f9215bQ = a5;
        fmVarArr[i9] = a5;
        int i10 = i9 + 1;
        C2491fm a6 = C4105zY.m41624a(Asset.class, "00bf845046bf39eae374aba493de0570", i10);
        bnt = a6;
        fmVarArr[i10] = a6;
        int i11 = i10 + 1;
        C2491fm a7 = C4105zY.m41624a(Asset.class, "efa7694a58413cb8434bbfb088bb177e", i11);
        bnu = a7;
        fmVarArr[i11] = a7;
        int i12 = i11 + 1;
        _m_methods = fmVarArr;
        C1634Xv.m11725a((Object[]) TaikodomObject._m_methods, (Object[]) _m_methods);
        ___iScriptClass = aUO.m18566a(Asset.class, C2306dp.class, _m_fields, _m_methods);
    }

    /* renamed from: a */
    private void m39481a(String str) {
        bFf().mo5608dq().mo3197f(f9211bM, str);
    }

    /* renamed from: a */
    private void m39482a(UUID uuid) {
        bFf().mo5608dq().mo3197f(f9210bL, uuid);
    }

    private String adT() {
        return (String) bFf().mo5608dq().mo3214p(bns);
    }

    /* renamed from: an */
    private UUID m39483an() {
        return (UUID) bFf().mo5608dq().mo3214p(f9210bL);
    }

    /* renamed from: ao */
    private String m39484ao() {
        return (String) bFf().mo5608dq().mo3214p(f9211bM);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Asset Handle")
    @C0064Am(aul = "c23bf9d1dae5c895334c608e620005ed", aum = 0)
    @C5566aOg
    /* renamed from: b */
    private void m39488b(String str) {
        throw new aWi(new aCE(this, f9215bQ, new Object[]{str}));
    }

    /* renamed from: bR */
    private void m39490bR(String str) {
        bFf().mo5608dq().mo3197f(bns, str);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Asset File")
    @C0064Am(aul = "efa7694a58413cb8434bbfb088bb177e", aum = 0)
    @C5566aOg
    /* renamed from: bS */
    private void m39491bS(String str) {
        throw new aWi(new aCE(this, bnu, new Object[]{str}));
    }

    /* renamed from: c */
    private void m39492c(UUID uuid) {
        switch (bFf().mo6893i(f9213bO)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f9213bO, new Object[]{uuid}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f9213bO, new Object[]{uuid}));
                break;
        }
        m39489b(uuid);
    }

    /* renamed from: T */
    public C6494any mo11T() {
        return ___iScriptClass;
    }

    /* renamed from: U */
    public C2491fm[] mo12U() {
        return _m_methods;
    }

    /* renamed from: W */
    public Object mo13W() {
        return new C2306dp(this);
    }

    /* renamed from: a */
    public Object mo14a(C0495Gr gr) {
        Object[] args = gr.getArgs();
        switch (gr.mo2417hq() - TaikodomObject._m_methodCount) {
            case 0:
                return m39485ap();
            case 1:
                m39489b((UUID) args[0]);
                return null;
            case 2:
                return m39487au();
            case 3:
                return m39486ar();
            case 4:
                m39488b((String) args[0]);
                return null;
            case 5:
                return adU();
            case 6:
                m39491bS((String) args[0]);
                return null;
            default:
                return super.mo14a(gr);
        }
    }

    /* renamed from: a */
    public void mo15a(Collection collection, C0495Gr gr) {
        super.mo15a(collection, gr);
    }

    /* renamed from: aq */
    public UUID mo18aq() {
        switch (bFf().mo6893i(f9212bN)) {
            case 0:
                return null;
            case 2:
                return (UUID) bFf().mo5606d(new aCE(this, f9212bN, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f9212bN, new Object[0]));
                break;
        }
        return m39485ap();
    }

    /* renamed from: c */
    public C5663aRz[] mo25c() {
        return _m_fields;
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Asset File")
    public String getFile() {
        switch (bFf().mo6893i(bnt)) {
            case 0:
                return null;
            case 2:
                return (String) bFf().mo5606d(new aCE(this, bnt, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, bnt, new Object[0]));
                break;
        }
        return adU();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Asset File")
    @C5566aOg
    public void setFile(String str) {
        switch (bFf().mo6893i(bnu)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, bnu, new Object[]{str}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, bnu, new Object[]{str}));
                break;
        }
        m39491bS(str);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Asset Handle")
    public String getHandle() {
        switch (bFf().mo6893i(f9214bP)) {
            case 0:
                return null;
            case 2:
                return (String) bFf().mo5606d(new aCE(this, f9214bP, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f9214bP, new Object[0]));
                break;
        }
        return m39486ar();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Asset Handle")
    @C5566aOg
    public void setHandle(String str) {
        switch (bFf().mo6893i(f9215bQ)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f9215bQ, new Object[]{str}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f9215bQ, new Object[]{str}));
                break;
        }
        m39488b(str);
    }

    public String toString() {
        switch (bFf().mo6893i(_f_toString_0020_0028_0029Ljava_002flang_002fString_003b)) {
            case 0:
                return null;
            case 2:
                return (String) bFf().mo5606d(new aCE(this, _f_toString_0020_0028_0029Ljava_002flang_002fString_003b, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, _f_toString_0020_0028_0029Ljava_002flang_002fString_003b, new Object[0]));
                break;
        }
        return m39487au();
    }

    @C0064Am(aul = "004e87d56aaa5b11824d9965e878032a", aum = 0)
    /* renamed from: ap */
    private UUID m39485ap() {
        return m39483an();
    }

    @C0064Am(aul = "57a533e4d65cca7ea6ce2d1ae00d9040", aum = 0)
    /* renamed from: b */
    private void m39489b(UUID uuid) {
        m39482a(uuid);
    }

    /* renamed from: S */
    public void mo10S() {
        super.mo10S();
        m39482a(UUID.randomUUID());
    }

    /* renamed from: p */
    public void mo22198p(String str, String str2) {
        super.mo10S();
        m39481a(str);
        m39490bR(str2);
    }

    @C0064Am(aul = "8a2b8dc1bd6506363abfcd2be0424078", aum = 0)
    /* renamed from: au */
    private String m39487au() {
        return "[" + m39484ao() + "]";
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Asset Handle")
    @C0064Am(aul = "c115c921749c59dfb5ffcf7df0a21f90", aum = 0)
    /* renamed from: ar */
    private String m39486ar() {
        return m39484ao();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Asset File")
    @C0064Am(aul = "00bf845046bf39eae374aba493de0570", aum = 0)
    private String adU() {
        return adT();
    }
}
