package game.script.resource;

import game.network.message.externalizable.aCE;
import game.network.message.serializable.C3438ra;
import game.script.TaikodomObject;
import logic.baa.*;
import logic.data.mbean.C0416Fn;
import logic.res.code.C5640aRc;
import logic.res.code.C5663aRz;
import logic.res.code.C6494any;
import logic.res.html.C2491fm;
import p001a.*;

import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.UUID;

@C5511aMd
@C6485anp
/* renamed from: a.NE */
/* compiled from: a */
public class AssetGroup extends TaikodomObject implements C0468GU, C1616Xf {

    /* renamed from: Pf */
    public static final C2491fm f1193Pf = null;

    /* renamed from: Pg */
    public static final C2491fm f1194Pg = null;
    public static final C2491fm _f_toString_0020_0028_0029Ljava_002flang_002fString_003b = null;
    public static final int _m_fieldCount = 0;
    public static final C5663aRz[] _m_fields = null;
    public static final int _m_methodCount = 0;
    public static final C2491fm[] _m_methods = null;
    /* renamed from: bL */
    public static final C5663aRz f1196bL = null;
    /* renamed from: bM */
    public static final C5663aRz f1197bM = null;
    /* renamed from: bN */
    public static final C2491fm f1198bN = null;
    /* renamed from: bO */
    public static final C2491fm f1199bO = null;
    /* renamed from: bP */
    public static final C2491fm f1200bP = null;
    /* renamed from: bQ */
    public static final C2491fm f1201bQ = null;
    public static final C5663aRz cKY = null;
    public static final C5663aRz dGm = null;
    public static final C2491fm dGn = null;
    public static final C2491fm dGo = null;
    public static final C2491fm dGp = null;
    public static final C2491fm dGq = null;
    public static final C2491fm dGr = null;
    public static final C2491fm dGs = null;
    public static final long serialVersionUID = 0;
    /* renamed from: zQ */
    public static final C5663aRz f1202zQ = null;
    public static C6494any ___iScriptClass;
    @C0064Am(aul = "7f2a3be91958ccd25c78ac2750b67035", aum = 0)

    /* renamed from: bK */
    private static UUID f1195bK;
    @C0064Am(aul = "520da7ec80080d3a3e4d53f3db473021", aum = 3)
    private static C3438ra<AssetGroup> cVk;
    @C0064Am(aul = "961a3a87323548e06014478127a5b3b8", aum = 4)
    private static C3438ra<Asset> cVl;
    @C0064Am(aul = "a58137ac894ed105dfb6a8a1d8cfa333", aum = 1)
    private static String handle;
    @C0064Am(aul = "1c0c1c9ded84e8399fc26482c176337b", aum = 2)
    private static String name;

    static {
        m7237V();
    }

    public AssetGroup() {
        super((C5540aNg) null);
        super.mo10S();
    }

    public AssetGroup(C5540aNg ang) {
        super(ang);
    }

    /* renamed from: V */
    static void m7237V() {
        serialVersionUID = (long) 1;
        _m_fieldCount = TaikodomObject._m_fieldCount + 5;
        _m_methodCount = TaikodomObject._m_methodCount + 13;
        int i = TaikodomObject._m_fieldCount;
        C5663aRz[] arzArr = new C5663aRz[(i + 5)];
        C5663aRz b = C5640aRc.m17844b(AssetGroup.class, "7f2a3be91958ccd25c78ac2750b67035", i);
        f1196bL = b;
        arzArr[i] = b;
        int i2 = i + 1;
        C5663aRz b2 = C5640aRc.m17844b(AssetGroup.class, "a58137ac894ed105dfb6a8a1d8cfa333", i2);
        f1197bM = b2;
        arzArr[i2] = b2;
        int i3 = i2 + 1;
        C5663aRz b3 = C5640aRc.m17844b(AssetGroup.class, "1c0c1c9ded84e8399fc26482c176337b", i3);
        f1202zQ = b3;
        arzArr[i3] = b3;
        int i4 = i3 + 1;
        C5663aRz b4 = C5640aRc.m17844b(AssetGroup.class, "520da7ec80080d3a3e4d53f3db473021", i4);
        dGm = b4;
        arzArr[i4] = b4;
        int i5 = i4 + 1;
        C5663aRz b5 = C5640aRc.m17844b(AssetGroup.class, "961a3a87323548e06014478127a5b3b8", i5);
        cKY = b5;
        arzArr[i5] = b5;
        int i6 = i5 + 1;
        _m_fields = arzArr;
        C1634Xv.m11725a((Object[]) TaikodomObject._m_fields, (Object[]) _m_fields);
        int i7 = TaikodomObject._m_methodCount;
        C2491fm[] fmVarArr = new C2491fm[(i7 + 13)];
        C2491fm a = C4105zY.m41624a(AssetGroup.class, "621d0cb3e83d50f5a098366ecb7ec70e", i7);
        _f_toString_0020_0028_0029Ljava_002flang_002fString_003b = a;
        fmVarArr[i7] = a;
        int i8 = i7 + 1;
        C2491fm a2 = C4105zY.m41624a(AssetGroup.class, "4e5e2b1903b5715f202f8e42e3e7a5a0", i8);
        f1198bN = a2;
        fmVarArr[i8] = a2;
        int i9 = i8 + 1;
        C2491fm a3 = C4105zY.m41624a(AssetGroup.class, "5b4175218eb4b8f38ded744dcd422396", i9);
        f1199bO = a3;
        fmVarArr[i9] = a3;
        int i10 = i9 + 1;
        C2491fm a4 = C4105zY.m41624a(AssetGroup.class, "62d3b3f666f9d897ce7ca34efe81856c", i10);
        f1200bP = a4;
        fmVarArr[i10] = a4;
        int i11 = i10 + 1;
        C2491fm a5 = C4105zY.m41624a(AssetGroup.class, "95f5d22dd6a60fc0895b6564b2bee7a1", i11);
        f1201bQ = a5;
        fmVarArr[i11] = a5;
        int i12 = i11 + 1;
        C2491fm a6 = C4105zY.m41624a(AssetGroup.class, "66287db6279f02115e88980d5ff830aa", i12);
        dGn = a6;
        fmVarArr[i12] = a6;
        int i13 = i12 + 1;
        C2491fm a7 = C4105zY.m41624a(AssetGroup.class, "4a7a572e99bf9498c09c06c56da9a7a1", i13);
        dGo = a7;
        fmVarArr[i13] = a7;
        int i14 = i13 + 1;
        C2491fm a8 = C4105zY.m41624a(AssetGroup.class, "b793704dcfcd34fe04d7d9b553922150", i14);
        dGp = a8;
        fmVarArr[i14] = a8;
        int i15 = i14 + 1;
        C2491fm a9 = C4105zY.m41624a(AssetGroup.class, "61df5c2c2268bba78996a869aa108ff5", i15);
        dGq = a9;
        fmVarArr[i15] = a9;
        int i16 = i15 + 1;
        C2491fm a10 = C4105zY.m41624a(AssetGroup.class, "6f52fdf4c8c355a15b5f6548ec139a9f", i16);
        dGr = a10;
        fmVarArr[i16] = a10;
        int i17 = i16 + 1;
        C2491fm a11 = C4105zY.m41624a(AssetGroup.class, "f0065e4c4d2c03403b17931e53e23bc4", i17);
        dGs = a11;
        fmVarArr[i17] = a11;
        int i18 = i17 + 1;
        C2491fm a12 = C4105zY.m41624a(AssetGroup.class, "cefa1b7d902e29be0664d0fbec2769fe", i18);
        f1193Pf = a12;
        fmVarArr[i18] = a12;
        int i19 = i18 + 1;
        C2491fm a13 = C4105zY.m41624a(AssetGroup.class, "8c5358bbabd0e0fdb0543cc7a35b042f", i19);
        f1194Pg = a13;
        fmVarArr[i19] = a13;
        int i20 = i19 + 1;
        _m_methods = fmVarArr;
        C1634Xv.m11725a((Object[]) TaikodomObject._m_methods, (Object[]) _m_methods);
        ___iScriptClass = aUO.m18566a(AssetGroup.class, C0416Fn.class, _m_fields, _m_methods);
    }

    /* renamed from: a */
    private void m7238a(String str) {
        bFf().mo5608dq().mo3197f(f1197bM, str);
    }

    /* renamed from: a */
    private void m7239a(UUID uuid) {
        bFf().mo5608dq().mo3197f(f1196bL, uuid);
    }

    @C3248pc(aYR = C3248pc.C3250b.ADDER, displayName = "Assets")
    @C0064Am(aul = "4a7a572e99bf9498c09c06c56da9a7a1", aum = 0)
    @C5566aOg
    /* renamed from: aF */
    private void m7240aF(Asset tCVar) {
        throw new aWi(new aCE(this, dGo, new Object[]{tCVar}));
    }

    @C3248pc(aYR = C3248pc.C3250b.REMOVER, displayName = "Assets")
    @C0064Am(aul = "b793704dcfcd34fe04d7d9b553922150", aum = 0)
    @C5566aOg
    /* renamed from: aH */
    private void m7241aH(Asset tCVar) {
        throw new aWi(new aCE(this, dGp, new Object[]{tCVar}));
    }

    /* renamed from: an */
    private UUID m7242an() {
        return (UUID) bFf().mo5608dq().mo3214p(f1196bL);
    }

    /* renamed from: ao */
    private String m7243ao() {
        return (String) bFf().mo5608dq().mo3214p(f1197bM);
    }

    /* renamed from: ap */
    private void m7245ap(String str) {
        bFf().mo5608dq().mo3197f(f1202zQ, str);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Name")
    @C0064Am(aul = "8c5358bbabd0e0fdb0543cc7a35b042f", aum = 0)
    @C5566aOg
    /* renamed from: aq */
    private void m7246aq(String str) {
        throw new aWi(new aCE(this, f1194Pg, new Object[]{str}));
    }

    @C3248pc(aYR = C3248pc.C3250b.ADDER, displayName = "Asset Subgroups")
    @C0064Am(aul = "6f52fdf4c8c355a15b5f6548ec139a9f", aum = 0)
    @C5566aOg
    /* renamed from: b */
    private void m7249b(AssetGroup ne) {
        throw new aWi(new aCE(this, dGr, new Object[]{ne}));
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Handle")
    @C0064Am(aul = "95f5d22dd6a60fc0895b6564b2bee7a1", aum = 0)
    @C5566aOg
    /* renamed from: b */
    private void m7250b(String str) {
        throw new aWi(new aCE(this, f1201bQ, new Object[]{str}));
    }

    private C3438ra bjJ() {
        return (C3438ra) bFf().mo5608dq().mo3214p(dGm);
    }

    private C3438ra bjK() {
        return (C3438ra) bFf().mo5608dq().mo3214p(cKY);
    }

    /* renamed from: bk */
    private void m7252bk(C3438ra raVar) {
        bFf().mo5608dq().mo3197f(dGm, raVar);
    }

    /* renamed from: bl */
    private void m7253bl(C3438ra raVar) {
        bFf().mo5608dq().mo3197f(cKY, raVar);
    }

    /* renamed from: c */
    private void m7254c(UUID uuid) {
        switch (bFf().mo6893i(f1199bO)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f1199bO, new Object[]{uuid}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f1199bO, new Object[]{uuid}));
                break;
        }
        m7251b(uuid);
    }

    @C3248pc(aYR = C3248pc.C3250b.REMOVER, displayName = "Asset Subgroups")
    @C0064Am(aul = "f0065e4c4d2c03403b17931e53e23bc4", aum = 0)
    @C5566aOg
    /* renamed from: d */
    private void m7255d(AssetGroup ne) {
        throw new aWi(new aCE(this, dGs, new Object[]{ne}));
    }

    /* renamed from: uU */
    private String m7256uU() {
        return (String) bFf().mo5608dq().mo3214p(f1202zQ);
    }

    /* renamed from: T */
    public C6494any mo11T() {
        return ___iScriptClass;
    }

    /* renamed from: U */
    public C2491fm[] mo12U() {
        return _m_methods;
    }

    /* renamed from: W */
    public Object mo13W() {
        return new C0416Fn(this);
    }

    /* renamed from: a */
    public Object mo14a(C0495Gr gr) {
        Object[] args = gr.getArgs();
        switch (gr.mo2417hq() - TaikodomObject._m_methodCount) {
            case 0:
                return m7248au();
            case 1:
                return m7244ap();
            case 2:
                m7251b((UUID) args[0]);
                return null;
            case 3:
                return m7247ar();
            case 4:
                m7250b((String) args[0]);
                return null;
            case 5:
                return bjL();
            case 6:
                m7240aF((Asset) args[0]);
                return null;
            case 7:
                m7241aH((Asset) args[0]);
                return null;
            case 8:
                return bjN();
            case 9:
                m7249b((AssetGroup) args[0]);
                return null;
            case 10:
                m7255d((AssetGroup) args[0]);
                return null;
            case 11:
                return m7257uV();
            case 12:
                m7246aq((String) args[0]);
                return null;
            default:
                return super.mo14a(gr);
        }
    }

    /* renamed from: a */
    public void mo15a(Collection collection, C0495Gr gr) {
        super.mo15a(collection, gr);
    }

    @C3248pc(aYR = C3248pc.C3250b.ADDER, displayName = "Assets")
    @C5566aOg
    /* renamed from: aG */
    public void mo4118aG(Asset tCVar) {
        switch (bFf().mo6893i(dGo)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, dGo, new Object[]{tCVar}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, dGo, new Object[]{tCVar}));
                break;
        }
        m7240aF(tCVar);
    }

    @C3248pc(aYR = C3248pc.C3250b.REMOVER, displayName = "Assets")
    @C5566aOg
    /* renamed from: aI */
    public void mo4119aI(Asset tCVar) {
        switch (bFf().mo6893i(dGp)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, dGp, new Object[]{tCVar}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, dGp, new Object[]{tCVar}));
                break;
        }
        m7241aH(tCVar);
    }

    /* renamed from: aq */
    public UUID mo18aq() {
        switch (bFf().mo6893i(f1198bN)) {
            case 0:
                return null;
            case 2:
                return (UUID) bFf().mo5606d(new aCE(this, f1198bN, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f1198bN, new Object[0]));
                break;
        }
        return m7244ap();
    }

    @C3248pc(aYR = C3248pc.C3250b.ITERATOR, displayName = "Assets")
    public List<Asset> bjM() {
        switch (bFf().mo6893i(dGn)) {
            case 0:
                return null;
            case 2:
                return (List) bFf().mo5606d(new aCE(this, dGn, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, dGn, new Object[0]));
                break;
        }
        return bjL();
    }

    @C3248pc(aYR = C3248pc.C3250b.ITERATOR, displayName = "Asset Subgroups")
    public List<AssetGroup> bjO() {
        switch (bFf().mo6893i(dGq)) {
            case 0:
                return null;
            case 2:
                return (List) bFf().mo5606d(new aCE(this, dGq, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, dGq, new Object[0]));
                break;
        }
        return bjN();
    }

    @C3248pc(aYR = C3248pc.C3250b.ADDER, displayName = "Asset Subgroups")
    @C5566aOg
    /* renamed from: c */
    public void mo4122c(AssetGroup ne) {
        switch (bFf().mo6893i(dGr)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, dGr, new Object[]{ne}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, dGr, new Object[]{ne}));
                break;
        }
        m7249b(ne);
    }

    /* renamed from: c */
    public C5663aRz[] mo25c() {
        return _m_fields;
    }

    @C3248pc(aYR = C3248pc.C3250b.REMOVER, displayName = "Asset Subgroups")
    @C5566aOg
    /* renamed from: e */
    public void mo4123e(AssetGroup ne) {
        switch (bFf().mo6893i(dGs)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, dGs, new Object[]{ne}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, dGs, new Object[]{ne}));
                break;
        }
        m7255d(ne);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Handle")
    public String getHandle() {
        switch (bFf().mo6893i(f1200bP)) {
            case 0:
                return null;
            case 2:
                return (String) bFf().mo5606d(new aCE(this, f1200bP, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f1200bP, new Object[0]));
                break;
        }
        return m7247ar();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Handle")
    @C5566aOg
    public void setHandle(String str) {
        switch (bFf().mo6893i(f1201bQ)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f1201bQ, new Object[]{str}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f1201bQ, new Object[]{str}));
                break;
        }
        m7250b(str);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Name")
    public String getName() {
        switch (bFf().mo6893i(f1193Pf)) {
            case 0:
                return null;
            case 2:
                return (String) bFf().mo5606d(new aCE(this, f1193Pf, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f1193Pf, new Object[0]));
                break;
        }
        return m7257uV();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Name")
    @C5566aOg
    public void setName(String str) {
        switch (bFf().mo6893i(f1194Pg)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f1194Pg, new Object[]{str}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f1194Pg, new Object[]{str}));
                break;
        }
        m7246aq(str);
    }

    public String toString() {
        switch (bFf().mo6893i(_f_toString_0020_0028_0029Ljava_002flang_002fString_003b)) {
            case 0:
                return null;
            case 2:
                return (String) bFf().mo5606d(new aCE(this, _f_toString_0020_0028_0029Ljava_002flang_002fString_003b, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, _f_toString_0020_0028_0029Ljava_002flang_002fString_003b, new Object[0]));
                break;
        }
        return m7248au();
    }

    @C0064Am(aul = "621d0cb3e83d50f5a098366ecb7ec70e", aum = 0)
    /* renamed from: au */
    private String m7248au() {
        return m7256uU() == null ? "<null>" : "[" + m7256uU() + "]";
    }

    @C0064Am(aul = "4e5e2b1903b5715f202f8e42e3e7a5a0", aum = 0)
    /* renamed from: ap */
    private UUID m7244ap() {
        return m7242an();
    }

    @C0064Am(aul = "5b4175218eb4b8f38ded744dcd422396", aum = 0)
    /* renamed from: b */
    private void m7251b(UUID uuid) {
        m7239a(uuid);
    }

    /* renamed from: S */
    public void mo10S() {
        super.mo10S();
        m7239a(UUID.randomUUID());
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Handle")
    @C0064Am(aul = "62d3b3f666f9d897ce7ca34efe81856c", aum = 0)
    /* renamed from: ar */
    private String m7247ar() {
        return m7243ao();
    }

    @C3248pc(aYR = C3248pc.C3250b.ITERATOR, displayName = "Assets")
    @C0064Am(aul = "66287db6279f02115e88980d5ff830aa", aum = 0)
    private List<Asset> bjL() {
        return Collections.unmodifiableList(bjK());
    }

    @C3248pc(aYR = C3248pc.C3250b.ITERATOR, displayName = "Asset Subgroups")
    @C0064Am(aul = "61df5c2c2268bba78996a869aa108ff5", aum = 0)
    private List<AssetGroup> bjN() {
        return Collections.unmodifiableList(bjJ());
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Name")
    @C0064Am(aul = "cefa1b7d902e29be0664d0fbec2769fe", aum = 0)
    /* renamed from: uV */
    private String m7257uV() {
        return m7256uU();
    }
}
