package game.script.player;

import game.network.message.externalizable.aCE;
import game.network.message.serializable.C2686iZ;
import logic.baa.*;
import logic.data.mbean.C6670arS;
import logic.res.code.C5640aRc;
import logic.res.code.C5663aRz;
import logic.res.code.C6494any;
import logic.res.html.C2491fm;
import p001a.*;

import java.util.Collection;

@C5511aMd
@C6485anp
/* renamed from: a.Dt */
/* compiled from: a */
public class DataSetContainer<T> extends aDJ implements C1616Xf {
    public static final int _m_fieldCount = 0;
    public static final C5663aRz[] _m_fields = null;
    public static final int _m_methodCount = 0;
    public static final C2491fm[] _m_methods = null;
    public static final C5663aRz cHb = null;
    public static final C2491fm cHc = null;
    public static final C2491fm cbk = null;
    public static final C2491fm cbl = null;
    public static final long serialVersionUID = 0;
    public static C6494any ___iScriptClass;
    @C0064Am(aul = "3d1533e171e28812f840e0dc4ca94641", aum = 0)
    private static C2686iZ<T> cHa;

    static {
        m2715V();
    }

    public DataSetContainer() {
        super((C5540aNg) null);
        super.mo10S();
    }

    public DataSetContainer(C5540aNg ang) {
        super(ang);
    }

    /* renamed from: V */
    static void m2715V() {
        serialVersionUID = (long) 1;
        _m_fieldCount = aDJ._m_fieldCount + 1;
        _m_methodCount = aDJ._m_methodCount + 3;
        int i = aDJ._m_fieldCount;
        C5663aRz[] arzArr = new C5663aRz[(i + 1)];
        C5663aRz b = C5640aRc.m17844b(DataSetContainer.class, "3d1533e171e28812f840e0dc4ca94641", i);
        cHb = b;
        arzArr[i] = b;
        int i2 = i + 1;
        _m_fields = arzArr;
        C1634Xv.m11725a((Object[]) aDJ._m_fields, (Object[]) _m_fields);
        int i3 = aDJ._m_methodCount;
        C2491fm[] fmVarArr = new C2491fm[(i3 + 3)];
        C2491fm a = C4105zY.m41624a(DataSetContainer.class, "0a4adc1259d154ba8ad232848e1a093d", i3);
        cbk = a;
        fmVarArr[i3] = a;
        int i4 = i3 + 1;
        C2491fm a2 = C4105zY.m41624a(DataSetContainer.class, "f10396bf1640828ac21be2358f32753c", i4);
        cbl = a2;
        fmVarArr[i4] = a2;
        int i5 = i4 + 1;
        C2491fm a3 = C4105zY.m41624a(DataSetContainer.class, "e80a4153cc250859b9beabc8dec716df", i5);
        cHc = a3;
        fmVarArr[i5] = a3;
        int i6 = i5 + 1;
        _m_methods = fmVarArr;
        C1634Xv.m11725a((Object[]) aDJ._m_methods, (Object[]) _m_methods);
        ___iScriptClass = aUO.m18566a(DataSetContainer.class, C6670arS.class, _m_fields, _m_methods);
    }

    private C2686iZ aGl() {
        return (C2686iZ) bFf().mo5608dq().mo3214p(cHb);
    }

    /* renamed from: x */
    private void m2716x(C2686iZ iZVar) {
        bFf().mo5608dq().mo3197f(cHb, iZVar);
    }

    /* renamed from: T */
    public C6494any mo11T() {
        return ___iScriptClass;
    }

    /* renamed from: U */
    public C2491fm[] mo12U() {
        return _m_methods;
    }

    /* renamed from: W */
    public Object mo13W() {
        return new C6670arS(this);
    }

    /* renamed from: a */
    public Object mo14a(C0495Gr gr) {
        Object[] args = gr.getArgs();
        switch (gr.mo2417hq() - aDJ._m_methodCount) {
            case 0:
                m2713I(args[0]);
                return null;
            case 1:
                return new Boolean(m2714J(args[0]));
            case 2:
                return aGm();
            default:
                return super.mo14a(gr);
        }
    }

    /* renamed from: a */
    public void mo15a(Collection collection, C0495Gr gr) {
        super.mo15a(collection, gr);
    }

    public C2686iZ<T> aGn() {
        switch (bFf().mo6893i(cHc)) {
            case 0:
                return null;
            case 2:
                return (C2686iZ) bFf().mo5606d(new aCE(this, cHc, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, cHc, new Object[0]));
                break;
        }
        return aGm();
    }

    public void add(T t) {
        switch (bFf().mo6893i(cbk)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, cbk, new Object[]{t}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, cbk, new Object[]{t}));
                break;
        }
        m2713I(t);
    }

    /* renamed from: c */
    public C5663aRz[] mo25c() {
        return _m_fields;
    }

    public boolean remove(T t) {
        switch (bFf().mo6893i(cbl)) {
            case 0:
                return false;
            case 2:
                return ((Boolean) bFf().mo5606d(new aCE(this, cbl, new Object[]{t}))).booleanValue();
            case 3:
                bFf().mo5606d(new aCE(this, cbl, new Object[]{t}));
                break;
        }
        return m2714J(t);
    }

    /* renamed from: S */
    public void mo10S() {
        super.mo10S();
    }

    @C0064Am(aul = "0a4adc1259d154ba8ad232848e1a093d", aum = 0)
    /* renamed from: I */
    private void m2713I(T t) {
        aGl().add(t);
    }

    @C0064Am(aul = "f10396bf1640828ac21be2358f32753c", aum = 0)
    /* renamed from: J */
    private boolean m2714J(T t) {
        return aGl().remove(t);
    }

    @C0064Am(aul = "e80a4153cc250859b9beabc8dec716df", aum = 0)
    private C2686iZ<T> aGm() {
        return aGl();
    }
}
