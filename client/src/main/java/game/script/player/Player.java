package game.script.player;

import game.engine.IEngineGame;
import game.network.message.Blocking;
import game.network.message.externalizable.*;
import game.network.message.serializable.C1556Wo;
import game.network.message.serializable.C3438ra;
import game.script.Character;
import game.script.*;
import game.script.associates.Associates;
import game.script.bank.BankAccount;
import game.script.citizenship.CitizenshipControl;
import game.script.citizenship.CitizenshipType;
import game.script.cloning.CloningInfo;
import game.script.corporation.Corporation;
import game.script.corporation.CorporationCreationStatus;
import game.script.faction.Faction;
import game.script.hangar.Hangar;
import game.script.item.Item;
import game.script.item.ItemLocation;
import game.script.item.ShipItem;
import game.script.item.WeaponType;
import game.script.login.UserConnection;
import game.script.mission.MissionTemplate;
import game.script.npc.NPC;
import game.script.pda.DatabaseCategory;
import game.script.pda.TaikopediaEntry;
import game.script.progress.OniLogger;
import game.script.progression.CharacterProgression;
import game.script.progression.ProgressionCareer;
import game.script.resource.Asset;
import game.script.ship.*;
import game.script.space.Loot;
import game.script.storage.Storage;
import game.script.trade.Trade;
import logic.aaa.C1506WA;
import logic.baa.*;
import logic.bbb.aDR;
import logic.data.mbean.C5490aLi;
import logic.data.mbean.C6163ahf;
import logic.data.mbean.C6300akM;
import logic.res.code.C5640aRc;
import logic.res.code.C5663aRz;
import logic.res.code.C6494any;
import logic.res.html.C2491fm;
import logic.thred.LogPrinter;
import org.apache.commons.logging.Log;
import p001a.*;
import taikodom.infra.script.I18NString;

import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;

@C5829abJ("1.9.2")
@C6968axh
@C6485anp
@C5511aMd
/* renamed from: a.akU  reason: case insensitive filesystem */
/* compiled from: a */
public class Player extends Character implements C1165RF, C1616Xf, C6625aqZ {

    /* renamed from: Lm */
    public static final C2491fm f4777Lm = null;

    /* renamed from: Pf */
    public static final C2491fm f4778Pf = null;

    /* renamed from: Pg */
    public static final C2491fm f4779Pg = null;
    public static final C2491fm _f_dispose_0020_0028_0029V = null;
    /* renamed from: _f_getGameIO_0020_0028_0029Ltaikodom_002finfra_002fclient_002fIGameIO_003b */
    public static final C2491fm f4780x3cb2b465 = null;
    public static final C2491fm _f_hasHollowField_0020_0028_0029Z = null;
    public static final C2491fm _f_toString_0020_0028_0029Ljava_002flang_002fString_003b = null;
    public static final int _m_fieldCount = 0;
    public static final C5663aRz[] _m_fields = null;
    public static final int _m_methodCount = 0;
    public static final C2491fm[] _m_methods = null;
    public static final C2491fm aJA = null;
    public static final C2491fm aJB = null;
    public static final C2491fm aJz = null;
    public static final C2491fm aKm = null;
    public static final C5663aRz avF = null;
    public static final C2491fm bpJ = null;
    public static final C2491fm brK = null;
    public static final C5663aRz cUH = null;
    public static final C5663aRz cfD = null;
    public static final C5663aRz cnU = null;
    public static final C2491fm cov = null;
    public static final C2491fm cow = null;
    public static final C2491fm dXe = null;
    public static final C5663aRz fHY = null;
    public static final C2491fm fIk = null;
    public static final C2491fm fRW = null;
    public static final C2491fm fax = null;
    public static final C2491fm fay = null;
    public static final C2491fm fgc = null;
    public static final C2491fm fgf = null;
    public static final C2491fm fgi = null;
    public static final C2491fm fgv = null;
    public static final C2491fm fgw = null;
    public static final C5663aRz fkz = null;
    /* renamed from: gQ */
    public static final C2491fm f4781gQ = null;
    public static final C2491fm hEG = null;
    public static final C2491fm hEI = null;
    public static final C2491fm hmy = null;
    public static final C5663aRz hyN = null;
    public static final C2491fm hzp = null;
    public static final C2491fm hzq = null;
    public static final C2491fm iKQ = null;
    public static final C2491fm iTA = null;
    public static final C2491fm iTB = null;
    public static final C2491fm iTC = null;
    public static final C2491fm iTD = null;
    public static final C2491fm iTE = null;
    public static final C2491fm iTF = null;
    public static final C2491fm iTG = null;
    public static final C2491fm iTH = null;
    public static final C2491fm iTI = null;
    public static final C2491fm iTJ = null;
    public static final C2491fm iTK = null;
    public static final C2491fm iTL = null;
    public static final C2491fm iTM = null;
    public static final C2491fm iTN = null;
    public static final C2491fm iTO = null;
    public static final C2491fm iTP = null;
    public static final C2491fm iTQ = null;
    public static final C2491fm iTR = null;
    public static final C2491fm iTS = null;
    public static final C2491fm iTT = null;
    public static final C2491fm iTU = null;
    public static final C2491fm iTV = null;
    public static final C2491fm iTW = null;
    public static final C2491fm iTX = null;
    public static final C2491fm iTY = null;
    public static final C2491fm iTZ = null;
    public static final C5663aRz iTb = null;
    public static final C5663aRz iTc = null;
    public static final C5663aRz iTd = null;
    public static final C5663aRz iTe = null;
    public static final C5663aRz iTf = null;
    public static final C5663aRz iTg = null;
    public static final C5663aRz iTh = null;
    public static final C5663aRz iTi = null;
    public static final C5663aRz iTj = null;
    public static final C5663aRz iTk = null;
    public static final C5663aRz iTl = null;
    public static final C5663aRz iTm = null;
    public static final C5663aRz iTn = null;
    public static final C5663aRz iTo = null;
    public static final C5663aRz iTp = null;
    public static final C5663aRz iTq = null;
    public static final C5663aRz iTr = null;
    public static final C5663aRz iTs = null;
    public static final C5663aRz iTt = null;
    public static final C5663aRz iTu = null;
    public static final C5663aRz iTv = null;
    public static final C5663aRz iTw = null;
    public static final C2491fm iTx = null;
    public static final C2491fm iTy = null;
    public static final C2491fm iTz = null;
    public static final C2491fm iUA = null;
    public static final C2491fm iUB = null;
    public static final C2491fm iUC = null;
    public static final C2491fm iUD = null;
    public static final C2491fm iUE = null;
    public static final C2491fm iUF = null;
    public static final C2491fm iUG = null;
    public static final C2491fm iUH = null;
    public static final C2491fm iUI = null;
    public static final C2491fm iUJ = null;
    public static final C2491fm iUK = null;
    public static final C2491fm iUL = null;
    public static final C2491fm iUM = null;
    public static final C2491fm iUN = null;
    public static final C2491fm iUO = null;
    public static final C2491fm iUP = null;
    public static final C2491fm iUQ = null;
    public static final C2491fm iUR = null;
    public static final C2491fm iUS = null;
    public static final C2491fm iUT = null;
    public static final C2491fm iUU = null;
    public static final C2491fm iUV = null;
    public static final C2491fm iUW = null;
    public static final C2491fm iUX = null;
    public static final C2491fm iUY = null;
    public static final C2491fm iUZ = null;
    public static final C2491fm iUa = null;
    public static final C2491fm iUb = null;
    public static final C2491fm iUc = null;
    public static final C2491fm iUd = null;
    public static final C2491fm iUe = null;
    public static final C2491fm iUf = null;
    public static final C2491fm iUg = null;
    public static final C2491fm iUh = null;
    public static final C2491fm iUi = null;
    public static final C2491fm iUj = null;
    public static final C2491fm iUk = null;
    public static final C2491fm iUl = null;
    public static final C2491fm iUm = null;
    public static final C2491fm iUn = null;
    public static final C2491fm iUo = null;
    public static final C2491fm iUp = null;
    public static final C2491fm iUq = null;
    public static final C2491fm iUr = null;
    public static final C2491fm iUs = null;
    public static final C2491fm iUt = null;
    public static final C2491fm iUu = null;
    public static final C2491fm iUv = null;
    public static final C2491fm iUw = null;
    public static final C2491fm iUx = null;
    public static final C2491fm iUy = null;
    public static final C2491fm iUz = null;
    public static final C2491fm iVA = null;
    public static final C2491fm iVB = null;
    public static final C2491fm iVC = null;
    public static final C2491fm iVD = null;
    public static final C2491fm iVE = null;
    public static final C2491fm iVF = null;
    public static final C2491fm iVG = null;
    public static final C2491fm iVH = null;
    public static final C2491fm iVI = null;
    public static final C2491fm iVJ = null;
    public static final C2491fm iVa = null;
    public static final C2491fm iVb = null;
    public static final C2491fm iVc = null;
    public static final C2491fm iVd = null;
    public static final C2491fm iVe = null;
    public static final C2491fm iVf = null;
    public static final C2491fm iVg = null;
    public static final C2491fm iVh = null;
    public static final C2491fm iVi = null;
    public static final C2491fm iVj = null;
    public static final C2491fm iVk = null;
    public static final C2491fm iVl = null;
    public static final C2491fm iVm = null;
    public static final C2491fm iVn = null;
    public static final C2491fm iVo = null;
    public static final C2491fm iVp = null;
    public static final C2491fm iVq = null;
    public static final C2491fm iVr = null;
    public static final C2491fm iVs = null;
    public static final C2491fm iVt = null;
    public static final C2491fm iVu = null;
    public static final C2491fm iVv = null;
    public static final C2491fm iVw = null;
    public static final C2491fm iVx = null;
    public static final C2491fm iVy = null;
    public static final C2491fm iVz = null;
    /* renamed from: zQ */
    public static final C5663aRz f4782zQ = null;

    public static C6494any ___iScriptClass = null;
    @C0064Am(aul = "13368b165449e9c6d8f1f6727e30f64c", aum = 18)
    @C1020Ot(drz = C1020Ot.C1021a.NOT_PERSISTED)
    private static Loot aZM = null;
    @C0064Am(aul = "40f6b440ed8030a8500d96cbf336a13c", aum = 11)
    private static Faction axU = null;
    @C0064Am(aul = "4ad52749613f6450903d2cfca97346e9", aum = 13)
    private static Corporation daW = null;
    @C0064Am(aul = "e87a453a33a85425a1afdf8b80074c34", aum = 15)
    private static PlayerAssistant dqK = null;
    @C0064Am(aul = "f5b6b0d81cbeed2e304e69d30fe44982", aum = 1)
    @C1020Ot(drz = C1020Ot.C1021a.NOT_PERSISTED)
    private static Party fGD = null;
    @C0064Am(aul = "7017b525483e6c7b86822c20bebe4e94", aum = 3)
    private static Associates fGE = null;
    @C0064Am(aul = "20977a88ff1b726cbc09e2a7ffefe9ad", aum = 4)
    private static CloningInfo fGF = null;
    @C0064Am(aul = "f90deaea7b1294df6525dba3988f2b42", aum = 5)
    private static C3438ra<Hangar> fGG = null;
    @C0064Am(aul = "7f294f174be37e35d0544aaa730636a3", aum = 6)
    private static C3438ra<Storage> fGH = null;
    @C0064Am(aul = "491845629c2b24a6daefd66706914bfa", aum = 7)
    private static C3438ra<Item> fGI = null;
    @C0064Am(aul = "51d444fcad3f7346fb01067e656563bc", aum = 8)
    private static C3438ra<Item> fGJ = null;
    @C0064Am(aul = "882e8a22dcc482a3febcbe0b1c79ced9", aum = 9)
    private static BankAccount fGK = null;
    @C0064Am(aul = "02fcf107f7bc8cb9d554ba98292e7d96", aum = 12)
    @C1020Ot(drz = C1020Ot.C1021a.NOT_PERSISTED)
    private static CorporationCreationStatus fGL = null;
    @C0064Am(aul = "ff554d34da5b684f15d3ba8305764084", aum = 14)
    private static OniLogger fGM = null;
    @C0064Am(aul = "e328f1b223761858bc3d46e1027be2eb", aum = 16)
    private static CitizenshipControl fGN = null;
    @C0064Am(aul = "9b2b13a4b9331d42b34f2af602be9a1b", aum = 17)
    private static PlayerSocialController fGO = null;
    @C0064Am(aul = "a52c8c62cc9b48b5fc8c7f0d01f57537", aum = 19)
    @C1020Ot(drz = C1020Ot.C1021a.NOT_PERSISTED)
    private static boolean fGP = false;
    @C0064Am(aul = "9ab9a431813c0c09cbb75f702c54c3da", aum = 20)
    @C1020Ot(drz = C1020Ot.C1021a.NOT_PERSISTED)
    private static boolean fGQ = false;
    @C0064Am(aul = "c678beee15b52c3525a5418d54540ff8", aum = 21)
    private static C1556Wo<C2546ge, BlockStatus> fGR = null;
    @C0064Am(aul = "f38d2534324fc95ee524365c71fc0562", aum = 22)
    private static PlayerSessionLog fGS = null;
    @C0064Am(aul = "3df9a8e07008aa0ad899b1d4afbfe5ef", aum = 23)
    private static ItemsDurabilityTask fGT = null;
    @C0064Am(aul = "26d6d1d2708e3516de2975cf414b12f8", aum = 24)
    private static int fGU = 0;
    @C0064Am(aul = "8194074b67788acd9dc58d3979106079", aum = 25)
    private static boolean fGV = false;
    @C0064Am(aul = "3d4ec3a25a6ce867f6a5b94b6d31daa5", aum = 26)
    private static aMU fGW = null;
    @C0064Am(aul = "143bb2050c02054f901643245ac4207d", aum = 27)
    @C1020Ot(drz = C1020Ot.C1021a.NOT_PERSISTED)
    private static PlayerTimeoutTask fGX = null;
    @C0064Am(aul = "a278dfeeb89b3acc6e1ae9f01ad4f0f4", aum = 28)
    @C1020Ot(drz = C1020Ot.C1021a.NOT_PERSISTED)
    private static long fGY = 0;
    @C0064Am(aul = "7b3b4b8da105949946b4d7c8ac134621", aum = 29)
    @C1020Ot(drz = C1020Ot.C1021a.NOT_PERSISTED)
    private static long fGZ = 0;
    @C0064Am(aul = "9885ab4fb8801859db567bd6d03fdfd1", aum = 10)
    private static User far = null;
    @C0064Am(aul = "0a190893e78ff75583cd47fc4db0d014", aum = 2)
    @C1020Ot(drz = C1020Ot.C1021a.NOT_PERSISTED)
    private static Trade fky = null;
    private static Log logger = LogPrinter.setClass(Player.class);
    @C0064Am(aul = "7ee960480a9c85fdf18811f679441960", aum = 0)
    private static String name = null;

    static {
        m23238V();
    }

    public Player() {
        super((C5540aNg) null);
        super.mo10S();
    }

    public Player(User adk) {
        super((C5540aNg) null);
        super._m_script_init(adk);
    }

    public Player(C5540aNg ang) {
        super(ang);
    }

    public Player(String str, User adk) {
        super((C5540aNg) null);
        super._m_script_init(str, adk);
    }

    /* renamed from: V */
    static void m23238V() {
        _m_fieldCount = Character._m_fieldCount + 30;
        _m_methodCount = Character._m_methodCount + 149;
        int i = Character._m_fieldCount;
        C5663aRz[] arzArr = new C5663aRz[(i + 30)];
        C5663aRz b = C5640aRc.m17844b(Player.class, "7ee960480a9c85fdf18811f679441960", i);
        f4782zQ = b;
        arzArr[i] = b;
        int i2 = i + 1;
        C5663aRz b2 = C5640aRc.m17844b(Player.class, "f5b6b0d81cbeed2e304e69d30fe44982", i2);
        cfD = b2;
        arzArr[i2] = b2;
        int i3 = i2 + 1;
        C5663aRz b3 = C5640aRc.m17844b(Player.class, "0a190893e78ff75583cd47fc4db0d014", i3);
        fkz = b3;
        arzArr[i3] = b3;
        int i4 = i3 + 1;
        C5663aRz b4 = C5640aRc.m17844b(Player.class, "7017b525483e6c7b86822c20bebe4e94", i4);
        iTb = b4;
        arzArr[i4] = b4;
        int i5 = i4 + 1;
        C5663aRz b5 = C5640aRc.m17844b(Player.class, "20977a88ff1b726cbc09e2a7ffefe9ad", i5);
        iTc = b5;
        arzArr[i5] = b5;
        int i6 = i5 + 1;
        C5663aRz b6 = C5640aRc.m17844b(Player.class, "f90deaea7b1294df6525dba3988f2b42", i6);
        iTd = b6;
        arzArr[i6] = b6;
        int i7 = i6 + 1;
        C5663aRz b7 = C5640aRc.m17844b(Player.class, "7f294f174be37e35d0544aaa730636a3", i7);
        iTe = b7;
        arzArr[i7] = b7;
        int i8 = i7 + 1;
        C5663aRz b8 = C5640aRc.m17844b(Player.class, "491845629c2b24a6daefd66706914bfa", i8);
        iTf = b8;
        arzArr[i8] = b8;
        int i9 = i8 + 1;
        C5663aRz b9 = C5640aRc.m17844b(Player.class, "51d444fcad3f7346fb01067e656563bc", i9);
        iTg = b9;
        arzArr[i9] = b9;
        int i10 = i9 + 1;
        C5663aRz b10 = C5640aRc.m17844b(Player.class, "882e8a22dcc482a3febcbe0b1c79ced9", i10);
        iTh = b10;
        arzArr[i10] = b10;
        int i11 = i10 + 1;
        C5663aRz b11 = C5640aRc.m17844b(Player.class, "9885ab4fb8801859db567bd6d03fdfd1", i11);
        avF = b11;
        arzArr[i11] = b11;
        int i12 = i11 + 1;
        C5663aRz b12 = C5640aRc.m17844b(Player.class, "40f6b440ed8030a8500d96cbf336a13c", i12);
        cnU = b12;
        arzArr[i12] = b12;
        int i13 = i12 + 1;
        C5663aRz b13 = C5640aRc.m17844b(Player.class, "02fcf107f7bc8cb9d554ba98292e7d96", i13);
        iTi = b13;
        arzArr[i13] = b13;
        int i14 = i13 + 1;
        C5663aRz b14 = C5640aRc.m17844b(Player.class, "4ad52749613f6450903d2cfca97346e9", i14);
        fHY = b14;
        arzArr[i14] = b14;
        int i15 = i14 + 1;
        C5663aRz b15 = C5640aRc.m17844b(Player.class, "ff554d34da5b684f15d3ba8305764084", i15);
        iTj = b15;
        arzArr[i15] = b15;
        int i16 = i15 + 1;
        C5663aRz b16 = C5640aRc.m17844b(Player.class, "e87a453a33a85425a1afdf8b80074c34", i16);
        iTk = b16;
        arzArr[i16] = b16;
        int i17 = i16 + 1;
        C5663aRz b17 = C5640aRc.m17844b(Player.class, "e328f1b223761858bc3d46e1027be2eb", i17);
        iTl = b17;
        arzArr[i17] = b17;
        int i18 = i17 + 1;
        C5663aRz b18 = C5640aRc.m17844b(Player.class, "9b2b13a4b9331d42b34f2af602be9a1b", i18);
        iTm = b18;
        arzArr[i18] = b18;
        int i19 = i18 + 1;
        C5663aRz b19 = C5640aRc.m17844b(Player.class, "13368b165449e9c6d8f1f6727e30f64c", i19);
        cUH = b19;
        arzArr[i19] = b19;
        int i20 = i19 + 1;
        C5663aRz b20 = C5640aRc.m17844b(Player.class, "a52c8c62cc9b48b5fc8c7f0d01f57537", i20);
        iTn = b20;
        arzArr[i20] = b20;
        int i21 = i20 + 1;
        C5663aRz b21 = C5640aRc.m17844b(Player.class, "9ab9a431813c0c09cbb75f702c54c3da", i21);
        iTo = b21;
        arzArr[i21] = b21;
        int i22 = i21 + 1;
        C5663aRz b22 = C5640aRc.m17844b(Player.class, "c678beee15b52c3525a5418d54540ff8", i22);
        iTp = b22;
        arzArr[i22] = b22;
        int i23 = i22 + 1;
        C5663aRz b23 = C5640aRc.m17844b(Player.class, "f38d2534324fc95ee524365c71fc0562", i23);
        iTq = b23;
        arzArr[i23] = b23;
        int i24 = i23 + 1;
        C5663aRz b24 = C5640aRc.m17844b(Player.class, "3df9a8e07008aa0ad899b1d4afbfe5ef", i24);
        iTr = b24;
        arzArr[i24] = b24;
        int i25 = i24 + 1;
        C5663aRz b25 = C5640aRc.m17844b(Player.class, "26d6d1d2708e3516de2975cf414b12f8", i25);
        hyN = b25;
        arzArr[i25] = b25;
        int i26 = i25 + 1;
        C5663aRz b26 = C5640aRc.m17844b(Player.class, "8194074b67788acd9dc58d3979106079", i26);
        iTs = b26;
        arzArr[i26] = b26;
        int i27 = i26 + 1;
        C5663aRz b27 = C5640aRc.m17844b(Player.class, "3d4ec3a25a6ce867f6a5b94b6d31daa5", i27);
        iTt = b27;
        arzArr[i27] = b27;
        int i28 = i27 + 1;
        C5663aRz b28 = C5640aRc.m17844b(Player.class, "143bb2050c02054f901643245ac4207d", i28);
        iTu = b28;
        arzArr[i28] = b28;
        int i29 = i28 + 1;
        C5663aRz b29 = C5640aRc.m17844b(Player.class, "a278dfeeb89b3acc6e1ae9f01ad4f0f4", i29);
        iTv = b29;
        arzArr[i29] = b29;
        int i30 = i29 + 1;
        C5663aRz b30 = C5640aRc.m17844b(Player.class, "7b3b4b8da105949946b4d7c8ac134621", i30);
        iTw = b30;
        arzArr[i30] = b30;
        int i31 = i30 + 1;
        _m_fields = arzArr;
        C1634Xv.m11725a((Object[]) Character._m_fields, (Object[]) _m_fields);
        int i32 = Character._m_methodCount;
        C2491fm[] fmVarArr = new C2491fm[(i32 + 149)];
        C2491fm a = C4105zY.m41624a(Player.class, "b62f409e3afdeacf5473f0ccacb876c5", i32);
        iTx = a;
        fmVarArr[i32] = a;
        int i33 = i32 + 1;
        C2491fm a2 = C4105zY.m41624a(Player.class, "2d7b1b1a24a6633edd2f094872ee3d5c", i33);
        iTy = a2;
        fmVarArr[i33] = a2;
        int i34 = i33 + 1;
        C2491fm a3 = C4105zY.m41624a(Player.class, "ecea673557ce93622b47014da38f1146", i34);
        iTz = a3;
        fmVarArr[i34] = a3;
        int i35 = i34 + 1;
        C2491fm a4 = C4105zY.m41624a(Player.class, "4cc604b3f6dfeef7fb268a9b5f656921", i35);
        _f_toString_0020_0028_0029Ljava_002flang_002fString_003b = a4;
        fmVarArr[i35] = a4;
        int i36 = i35 + 1;
        C2491fm a5 = C4105zY.m41624a(Player.class, "6dcf927a66bf5f2898054356cd4d72fa", i36);
        f4779Pg = a5;
        fmVarArr[i36] = a5;
        int i37 = i36 + 1;
        C2491fm a6 = C4105zY.m41624a(Player.class, "99045c3a4c230ace897e28b6969fef2b", i37);
        iTA = a6;
        fmVarArr[i37] = a6;
        int i38 = i37 + 1;
        C2491fm a7 = C4105zY.m41624a(Player.class, "22c8fc4f05eee7c0dc3830966b37f8ac", i38);
        f4778Pf = a7;
        fmVarArr[i38] = a7;
        int i39 = i38 + 1;
        C2491fm a8 = C4105zY.m41624a(Player.class, "f8479bbf784bd91416a1cf7e68645eeb", i39);
        fgi = a8;
        fmVarArr[i39] = a8;
        int i40 = i39 + 1;
        C2491fm a9 = C4105zY.m41624a(Player.class, "47352a03f94c1cb5fd151900f7cc1935", i40);
        brK = a9;
        fmVarArr[i40] = a9;
        int i41 = i40 + 1;
        C2491fm a10 = C4105zY.m41624a(Player.class, "249d605dabee47972271c4a002a981d4", i41);
        iTB = a10;
        fmVarArr[i41] = a10;
        int i42 = i41 + 1;
        C2491fm a11 = C4105zY.m41624a(Player.class, "99cb66af10822474af796fa61d2c75c9", i42);
        iTC = a11;
        fmVarArr[i42] = a11;
        int i43 = i42 + 1;
        C2491fm a12 = C4105zY.m41624a(Player.class, "574ea48fdfe8524d25e2eafb8025f2f7", i43);
        _f_dispose_0020_0028_0029V = a12;
        fmVarArr[i43] = a12;
        int i44 = i43 + 1;
        C2491fm a13 = C4105zY.m41624a(Player.class, "e42d0add3afa6b59be75fb5f254f41de", i44);
        f4777Lm = a13;
        fmVarArr[i44] = a13;
        int i45 = i44 + 1;
        C2491fm a14 = C4105zY.m41624a(Player.class, "8f2705392e2fff0a2b6384a97087a224", i45);
        _f_hasHollowField_0020_0028_0029Z = a14;
        fmVarArr[i45] = a14;
        int i46 = i45 + 1;
        C2491fm a15 = C4105zY.m41624a(Player.class, "c738dc6439d8de92313b111aaceb6922", i46);
        f4780x3cb2b465 = a15;
        fmVarArr[i46] = a15;
        int i47 = i46 + 1;
        C2491fm a16 = C4105zY.m41624a(Player.class, "d31036a4cab65e40ed5b52945ad563c2", i47);
        iTD = a16;
        fmVarArr[i47] = a16;
        int i48 = i47 + 1;
        C2491fm a17 = C4105zY.m41624a(Player.class, "21a00cadd94251b19b13bb6e9a94e8da", i48);
        fgf = a17;
        fmVarArr[i48] = a17;
        int i49 = i48 + 1;
        C2491fm a18 = C4105zY.m41624a(Player.class, "561230076450586a51b0522158d72582", i49);
        iTE = a18;
        fmVarArr[i49] = a18;
        int i50 = i49 + 1;
        C2491fm a19 = C4105zY.m41624a(Player.class, "70b47e95b58bb6b023e96ea91df99b1a", i50);
        iTF = a19;
        fmVarArr[i50] = a19;
        int i51 = i50 + 1;
        C2491fm a20 = C4105zY.m41624a(Player.class, "ef8ef535643a5d53d1a167d295b88a3a", i51);
        iTG = a20;
        fmVarArr[i51] = a20;
        int i52 = i51 + 1;
        C2491fm a21 = C4105zY.m41624a(Player.class, "defe36efb4d9194f1a79f434ddddb32b", i52);
        iTH = a21;
        fmVarArr[i52] = a21;
        int i53 = i52 + 1;
        C2491fm a22 = C4105zY.m41624a(Player.class, "2527e93c0bfa824108eac936889b2016", i53);
        iTI = a22;
        fmVarArr[i53] = a22;
        int i54 = i53 + 1;
        C2491fm a23 = C4105zY.m41624a(Player.class, "1426d3b6453e191c74e2804c2267d620", i54);
        iTJ = a23;
        fmVarArr[i54] = a23;
        int i55 = i54 + 1;
        C2491fm a24 = C4105zY.m41624a(Player.class, "69d273feac0dd2021408b5380fc6c331", i55);
        aJz = a24;
        fmVarArr[i55] = a24;
        int i56 = i55 + 1;
        C2491fm a25 = C4105zY.m41624a(Player.class, "c4708d73260e36e6e3032fd66866a537", i56);
        iTK = a25;
        fmVarArr[i56] = a25;
        int i57 = i56 + 1;
        C2491fm a26 = C4105zY.m41624a(Player.class, "402345e58acbd923cf7018d0f1a9b5ea", i57);
        aJA = a26;
        fmVarArr[i57] = a26;
        int i58 = i57 + 1;
        C2491fm a27 = C4105zY.m41624a(Player.class, "14164b90348f90351603d7decbe87bc2", i58);
        aJB = a27;
        fmVarArr[i58] = a27;
        int i59 = i58 + 1;
        C2491fm a28 = C4105zY.m41624a(Player.class, "d258cc41464f6b5c12da960ff0bfb31c", i59);
        iTL = a28;
        fmVarArr[i59] = a28;
        int i60 = i59 + 1;
        C2491fm a29 = C4105zY.m41624a(Player.class, "b326f8dc98862c9d4fb518d0e16482d6", i60);
        fax = a29;
        fmVarArr[i60] = a29;
        int i61 = i60 + 1;
        C2491fm a30 = C4105zY.m41624a(Player.class, "335c39e072ba6429577c2d29be599c4a", i61);
        fay = a30;
        fmVarArr[i61] = a30;
        int i62 = i61 + 1;
        C2491fm a31 = C4105zY.m41624a(Player.class, "585b5d6a426208043e4e881315336dcd", i62);
        iTM = a31;
        fmVarArr[i62] = a31;
        int i63 = i62 + 1;
        C2491fm a32 = C4105zY.m41624a(Player.class, "892c26ab019ea2735273abe22c40ec31", i63);
        iTN = a32;
        fmVarArr[i63] = a32;
        int i64 = i63 + 1;
        C2491fm a33 = C4105zY.m41624a(Player.class, "b08540ae478009791a8e926fe76a591d", i64);
        iTO = a33;
        fmVarArr[i64] = a33;
        int i65 = i64 + 1;
        C2491fm a34 = C4105zY.m41624a(Player.class, "b7c39903be1b756ee2b4fc0024b87dbe", i65);
        iTP = a34;
        fmVarArr[i65] = a34;
        int i66 = i65 + 1;
        C2491fm a35 = C4105zY.m41624a(Player.class, "04510a4ded24c210f482670a5992e8c7", i66);
        iTQ = a35;
        fmVarArr[i66] = a35;
        int i67 = i66 + 1;
        C2491fm a36 = C4105zY.m41624a(Player.class, "7ea5b9c5790aade849e7701fb24d78a4", i67);
        iTR = a36;
        fmVarArr[i67] = a36;
        int i68 = i67 + 1;
        C2491fm a37 = C4105zY.m41624a(Player.class, "085f66f34be042275e3894f8ebdd0275", i68);
        iTS = a37;
        fmVarArr[i68] = a37;
        int i69 = i68 + 1;
        C2491fm a38 = C4105zY.m41624a(Player.class, "f49b110a06a8ea3b8b5679e337d82e4b", i69);
        iTT = a38;
        fmVarArr[i69] = a38;
        int i70 = i69 + 1;
        C2491fm a39 = C4105zY.m41624a(Player.class, "44a412d752b2206d4979716cdf7363cd", i70);
        iTU = a39;
        fmVarArr[i70] = a39;
        int i71 = i70 + 1;
        C2491fm a40 = C4105zY.m41624a(Player.class, "4a97ed51ba79ec323da27c45dca9999f", i71);
        iTV = a40;
        fmVarArr[i71] = a40;
        int i72 = i71 + 1;
        C2491fm a41 = C4105zY.m41624a(Player.class, "a48e2cec190c8e942bba130dcf31a870", i72);
        iTW = a41;
        fmVarArr[i72] = a41;
        int i73 = i72 + 1;
        C2491fm a42 = C4105zY.m41624a(Player.class, "50f7fb26b08a041b4be620027c1a4345", i73);
        iTX = a42;
        fmVarArr[i73] = a42;
        int i74 = i73 + 1;
        C2491fm a43 = C4105zY.m41624a(Player.class, "29ce6fcf990cc81677ce5e426a96aca6", i74);
        iTY = a43;
        fmVarArr[i74] = a43;
        int i75 = i74 + 1;
        C2491fm a44 = C4105zY.m41624a(Player.class, "accbcfccafbcc19e2ac52d78649c76e2", i75);
        iTZ = a44;
        fmVarArr[i75] = a44;
        int i76 = i75 + 1;
        C2491fm a45 = C4105zY.m41624a(Player.class, "d635eb6038cd879afe4d0deb07f50acc", i76);
        iUa = a45;
        fmVarArr[i76] = a45;
        int i77 = i76 + 1;
        C2491fm a46 = C4105zY.m41624a(Player.class, "48e5f8ce0bbcc5fd3b8e4df389ddad48", i77);
        iUb = a46;
        fmVarArr[i77] = a46;
        int i78 = i77 + 1;
        C2491fm a47 = C4105zY.m41624a(Player.class, "a3990da4eaef461d38d6a6200501e37d", i78);
        iUc = a47;
        fmVarArr[i78] = a47;
        int i79 = i78 + 1;
        C2491fm a48 = C4105zY.m41624a(Player.class, "98db9516eff5154a819898e0807df75f", i79);
        iUd = a48;
        fmVarArr[i79] = a48;
        int i80 = i79 + 1;
        C2491fm a49 = C4105zY.m41624a(Player.class, "b06b99f3d3faac2ed13c0e44300b5813", i80);
        iUe = a49;
        fmVarArr[i80] = a49;
        int i81 = i80 + 1;
        C2491fm a50 = C4105zY.m41624a(Player.class, "32a1d23248de230824f1dc22c10a24a9", i81);
        iUf = a50;
        fmVarArr[i81] = a50;
        int i82 = i81 + 1;
        C2491fm a51 = C4105zY.m41624a(Player.class, "fd0e1e4a268c8ca0ac3c696e5cb7db73", i82);
        iUg = a51;
        fmVarArr[i82] = a51;
        int i83 = i82 + 1;
        C2491fm a52 = C4105zY.m41624a(Player.class, "2163ab49aa59ab1ddb4c56e38497c37b", i83);
        iUh = a52;
        fmVarArr[i83] = a52;
        int i84 = i83 + 1;
        C2491fm a53 = C4105zY.m41624a(Player.class, "ef20d5134e02d2101cf106efb116ecb4", i84);
        iUi = a53;
        fmVarArr[i84] = a53;
        int i85 = i84 + 1;
        C2491fm a54 = C4105zY.m41624a(Player.class, "5671b8b1afc5cb69f193368027d34f68", i85);
        iUj = a54;
        fmVarArr[i85] = a54;
        int i86 = i85 + 1;
        C2491fm a55 = C4105zY.m41624a(Player.class, "efbb472fbcb132ac558cf84b76abfa84", i86);
        iUk = a55;
        fmVarArr[i86] = a55;
        int i87 = i86 + 1;
        C2491fm a56 = C4105zY.m41624a(Player.class, "cd3f413922cb6eb57e1b3fdcac53fb95", i87);
        iUl = a56;
        fmVarArr[i87] = a56;
        int i88 = i87 + 1;
        C2491fm a57 = C4105zY.m41624a(Player.class, "9b94c38b0e328f6bea07d6fc8e90f0a9", i88);
        iUm = a57;
        fmVarArr[i88] = a57;
        int i89 = i88 + 1;
        C2491fm a58 = C4105zY.m41624a(Player.class, "eafd350310b64eb637633f1c59796b0c", i89);
        iUn = a58;
        fmVarArr[i89] = a58;
        int i90 = i89 + 1;
        C2491fm a59 = C4105zY.m41624a(Player.class, "a0e604370aff7297d3e9f8725f8f2ffe", i90);
        iUo = a59;
        fmVarArr[i90] = a59;
        int i91 = i90 + 1;
        C2491fm a60 = C4105zY.m41624a(Player.class, "d8b45d472800adbae7f32fe01c89f277", i91);
        iUp = a60;
        fmVarArr[i91] = a60;
        int i92 = i91 + 1;
        C2491fm a61 = C4105zY.m41624a(Player.class, "38ad1a09ea85ff6882ae408a55d24321", i92);
        iUq = a61;
        fmVarArr[i92] = a61;
        int i93 = i92 + 1;
        C2491fm a62 = C4105zY.m41624a(Player.class, "bf2030b242719fa4d660ec4df91009b8", i93);
        iUr = a62;
        fmVarArr[i93] = a62;
        int i94 = i93 + 1;
        C2491fm a63 = C4105zY.m41624a(Player.class, "bd67b9a749a5b384357304e4c6e44e3a", i94);
        iUs = a63;
        fmVarArr[i94] = a63;
        int i95 = i94 + 1;
        C2491fm a64 = C4105zY.m41624a(Player.class, "a2f0126a4c4a4e66720e532028b902ae", i95);
        iUt = a64;
        fmVarArr[i95] = a64;
        int i96 = i95 + 1;
        C2491fm a65 = C4105zY.m41624a(Player.class, "8b63b5a2727bf9d54d8f40e7f45c9168", i96);
        iUu = a65;
        fmVarArr[i96] = a65;
        int i97 = i96 + 1;
        C2491fm a66 = C4105zY.m41624a(Player.class, "60a343a3904c432d06e6b5277b210106", i97);
        iUv = a66;
        fmVarArr[i97] = a66;
        int i98 = i97 + 1;
        C2491fm a67 = C4105zY.m41624a(Player.class, "e380dd565a8d15aa701fe530f8d3a2bc", i98);
        iUw = a67;
        fmVarArr[i98] = a67;
        int i99 = i98 + 1;
        C2491fm a68 = C4105zY.m41624a(Player.class, "ea3066ce03b542b30c6421056ecd54d1", i99);
        iUx = a68;
        fmVarArr[i99] = a68;
        int i100 = i99 + 1;
        C2491fm a69 = C4105zY.m41624a(Player.class, "e9193297baf984046002c401bebee49b", i100);
        iUy = a69;
        fmVarArr[i100] = a69;
        int i101 = i100 + 1;
        C2491fm a70 = C4105zY.m41624a(Player.class, "11fb44696b28ea3078f1e0ab614da94a", i101);
        iUz = a70;
        fmVarArr[i101] = a70;
        int i102 = i101 + 1;
        C2491fm a71 = C4105zY.m41624a(Player.class, "b551ffb5048a5fc49558bcb7ebc7298b", i102);
        iUA = a71;
        fmVarArr[i102] = a71;
        int i103 = i102 + 1;
        C2491fm a72 = C4105zY.m41624a(Player.class, "c17a74fc0a9c5ea56d21025b1c5a81c0", i103);
        iUB = a72;
        fmVarArr[i103] = a72;
        int i104 = i103 + 1;
        C2491fm a73 = C4105zY.m41624a(Player.class, "12a9ca0c0c6023ab1f8046de27630e14", i104);
        fIk = a73;
        fmVarArr[i104] = a73;
        int i105 = i104 + 1;
        C2491fm a74 = C4105zY.m41624a(Player.class, "164087923a56c000d47917810a50c754", i105);
        iUC = a74;
        fmVarArr[i105] = a74;
        int i106 = i105 + 1;
        C2491fm a75 = C4105zY.m41624a(Player.class, "cb168cec98d48307f96c864914b67c07", i106);
        iUD = a75;
        fmVarArr[i106] = a75;
        int i107 = i106 + 1;
        C2491fm a76 = C4105zY.m41624a(Player.class, "49f74267f3d30318dd4739227dd71a48", i107);
        iUE = a76;
        fmVarArr[i107] = a76;
        int i108 = i107 + 1;
        C2491fm a77 = C4105zY.m41624a(Player.class, "678deec37501da696766e67f9c924d91", i108);
        iUF = a77;
        fmVarArr[i108] = a77;
        int i109 = i108 + 1;
        C2491fm a78 = C4105zY.m41624a(Player.class, "84ee63320f0f8a77c04e291ac1da8b62", i109);
        iUG = a78;
        fmVarArr[i109] = a78;
        int i110 = i109 + 1;
        C2491fm a79 = C4105zY.m41624a(Player.class, "96a5caf4e2b6d095fd95844c42488843", i110);
        iUH = a79;
        fmVarArr[i110] = a79;
        int i111 = i110 + 1;
        C2491fm a80 = C4105zY.m41624a(Player.class, "53311962d7988ca667aec72ea09012a2", i111);
        iUI = a80;
        fmVarArr[i111] = a80;
        int i112 = i111 + 1;
        C2491fm a81 = C4105zY.m41624a(Player.class, "87f27bcff03899d215b50845aef3ad27", i112);
        iUJ = a81;
        fmVarArr[i112] = a81;
        int i113 = i112 + 1;
        C2491fm a82 = C4105zY.m41624a(Player.class, "3aac2c4c131b908046447392380859ee", i113);
        iUK = a82;
        fmVarArr[i113] = a82;
        int i114 = i113 + 1;
        C2491fm a83 = C4105zY.m41624a(Player.class, "9c970fcff9d0575486336ae7419fa17b", i114);
        iKQ = a83;
        fmVarArr[i114] = a83;
        int i115 = i114 + 1;
        C2491fm a84 = C4105zY.m41624a(Player.class, "f6162c37d89c5d063c4a8160240f48a3", i115);
        iUL = a84;
        fmVarArr[i115] = a84;
        int i116 = i115 + 1;
        C2491fm a85 = C4105zY.m41624a(Player.class, "22c2d259fc05d999bbe5b2cbdfd7611b", i116);
        iUM = a85;
        fmVarArr[i116] = a85;
        int i117 = i116 + 1;
        C2491fm a86 = C4105zY.m41624a(Player.class, "62ad2ffbf3f4b4d293a41c3755c5bbd4", i117);
        iUN = a86;
        fmVarArr[i117] = a86;
        int i118 = i117 + 1;
        C2491fm a87 = C4105zY.m41624a(Player.class, "911316255671b48134e9e130f9ae4a1a", i118);
        iUO = a87;
        fmVarArr[i118] = a87;
        int i119 = i118 + 1;
        C2491fm a88 = C4105zY.m41624a(Player.class, "8782f08bc40de1dd5dc1d09cb817f34a", i119);
        iUP = a88;
        fmVarArr[i119] = a88;
        int i120 = i119 + 1;
        C2491fm a89 = C4105zY.m41624a(Player.class, "144a21ac6a5c6e69bfcbc71a341e4c5f", i120);
        fRW = a89;
        fmVarArr[i120] = a89;
        int i121 = i120 + 1;
        C2491fm a90 = C4105zY.m41624a(Player.class, "a7083cc470d58bda6cf368852649bf0b", i121);
        iUQ = a90;
        fmVarArr[i121] = a90;
        int i122 = i121 + 1;
        C2491fm a91 = C4105zY.m41624a(Player.class, "9f4b75cf9de240edaf64c8b2f87b1f0b", i122);
        iUR = a91;
        fmVarArr[i122] = a91;
        int i123 = i122 + 1;
        C2491fm a92 = C4105zY.m41624a(Player.class, "65d03f9c33b8984c6674aaa01afb20be", i123);
        iUS = a92;
        fmVarArr[i123] = a92;
        int i124 = i123 + 1;
        C2491fm a93 = C4105zY.m41624a(Player.class, "7ef7cba3e063b28b563e99db368909ae", i124);
        iUT = a93;
        fmVarArr[i124] = a93;
        int i125 = i124 + 1;
        C2491fm a94 = C4105zY.m41624a(Player.class, "9e9c404d141f635b6aa4c3f40a06c85c", i125);
        iUU = a94;
        fmVarArr[i125] = a94;
        int i126 = i125 + 1;
        C2491fm a95 = C4105zY.m41624a(Player.class, "c1f2c56c5ad1f0900e9c9110aa027ee3", i126);
        iUV = a95;
        fmVarArr[i126] = a95;
        int i127 = i126 + 1;
        C2491fm a96 = C4105zY.m41624a(Player.class, "633d62f02bbdf6f885b418c9e3c5a408", i127);
        iUW = a96;
        fmVarArr[i127] = a96;
        int i128 = i127 + 1;
        C2491fm a97 = C4105zY.m41624a(Player.class, "8ca6dd39b126b4cf8be5e7a149ab1616", i128);
        cov = a97;
        fmVarArr[i128] = a97;
        int i129 = i128 + 1;
        C2491fm a98 = C4105zY.m41624a(Player.class, "27be2cfe2f4b1247e9ba60a14b9d0160", i129);
        cow = a98;
        fmVarArr[i129] = a98;
        int i130 = i129 + 1;
        C2491fm a99 = C4105zY.m41624a(Player.class, "e62c968bfba2dca7a1a1a29425b327ec", i130);
        iUX = a99;
        fmVarArr[i130] = a99;
        int i131 = i130 + 1;
        C2491fm a100 = C4105zY.m41624a(Player.class, "fc9c69c1876ebb33a5735814d39efacf", i131);
        iUY = a100;
        fmVarArr[i131] = a100;
        int i132 = i131 + 1;
        C2491fm a101 = C4105zY.m41624a(Player.class, "153aaeea90845dd8672328c130903a99", i132);
        iUZ = a101;
        fmVarArr[i132] = a101;
        int i133 = i132 + 1;
        C2491fm a102 = C4105zY.m41624a(Player.class, "6b0a7428f86b4ba31d194c2e2ccb48bd", i133);
        iVa = a102;
        fmVarArr[i133] = a102;
        int i134 = i133 + 1;
        C2491fm a103 = C4105zY.m41624a(Player.class, "0aab5b0678e6e2e4da404a93ccd2e0d6", i134);
        iVb = a103;
        fmVarArr[i134] = a103;
        int i135 = i134 + 1;
        C2491fm a104 = C4105zY.m41624a(Player.class, "5481934baf9ecb5a7a17586e9bcf0f9a", i135);
        iVc = a104;
        fmVarArr[i135] = a104;
        int i136 = i135 + 1;
        C2491fm a105 = C4105zY.m41624a(Player.class, "1db63204a9d240436b1c268a998b096b", i136);
        iVd = a105;
        fmVarArr[i136] = a105;
        int i137 = i136 + 1;
        C2491fm a106 = C4105zY.m41624a(Player.class, "c8a29cef39a3857055a3bd30654ff3ef", i137);
        iVe = a106;
        fmVarArr[i137] = a106;
        int i138 = i137 + 1;
        C2491fm a107 = C4105zY.m41624a(Player.class, "1cebf3cff56a7d3353f4d2be029a3c2b", i138);
        iVf = a107;
        fmVarArr[i138] = a107;
        int i139 = i138 + 1;
        C2491fm a108 = C4105zY.m41624a(Player.class, "33929c54e785d38d817d1de94aa1736b", i139);
        iVg = a108;
        fmVarArr[i139] = a108;
        int i140 = i139 + 1;
        C2491fm a109 = C4105zY.m41624a(Player.class, "b62c7f05cb2e2ccd726220b60dee7d20", i140);
        iVh = a109;
        fmVarArr[i140] = a109;
        int i141 = i140 + 1;
        C2491fm a110 = C4105zY.m41624a(Player.class, "2dff80ca4081a19c3e11cefdd6991f29", i141);
        iVi = a110;
        fmVarArr[i141] = a110;
        int i142 = i141 + 1;
        C2491fm a111 = C4105zY.m41624a(Player.class, "86fcd2e80667d6b50e838f545c87ecdc", i142);
        iVj = a111;
        fmVarArr[i142] = a111;
        int i143 = i142 + 1;
        C2491fm a112 = C4105zY.m41624a(Player.class, "a9594230cdf62e928a778f18c8f7b532", i143);
        hzq = a112;
        fmVarArr[i143] = a112;
        int i144 = i143 + 1;
        C2491fm a113 = C4105zY.m41624a(Player.class, "60f272f62950070e102980ef5b56e45b", i144);
        hzp = a113;
        fmVarArr[i144] = a113;
        int i145 = i144 + 1;
        C2491fm a114 = C4105zY.m41624a(Player.class, "25dab5d40f9d7e7dad6d9e7e64ce2cf4", i145);
        iVk = a114;
        fmVarArr[i145] = a114;
        int i146 = i145 + 1;
        C2491fm a115 = C4105zY.m41624a(Player.class, "de80ff8c1636d4a1e4369e042f03618f", i146);
        hmy = a115;
        fmVarArr[i146] = a115;
        int i147 = i146 + 1;
        C2491fm a116 = C4105zY.m41624a(Player.class, "87410b4e4ef90e30610a3846cd4991d1", i147);
        iVl = a116;
        fmVarArr[i147] = a116;
        int i148 = i147 + 1;
        C2491fm a117 = C4105zY.m41624a(Player.class, "a3b67813216eb1c84d1c83fdd2f77f35", i148);
        iVm = a117;
        fmVarArr[i148] = a117;
        int i149 = i148 + 1;
        C2491fm a118 = C4105zY.m41624a(Player.class, "b4ff6a106632247ff7aa765caf7ed385", i149);
        iVn = a118;
        fmVarArr[i149] = a118;
        int i150 = i149 + 1;
        C2491fm a119 = C4105zY.m41624a(Player.class, "91f1aff3b0bdb1c1d4be98b663baf086", i150);
        iVo = a119;
        fmVarArr[i150] = a119;
        int i151 = i150 + 1;
        C2491fm a120 = C4105zY.m41624a(Player.class, "8e000364eccfb71c48897640d6f450b2", i151);
        hEG = a120;
        fmVarArr[i151] = a120;
        int i152 = i151 + 1;
        C2491fm a121 = C4105zY.m41624a(Player.class, "1496d83b4b076fb3969c09a95369988e", i152);
        iVp = a121;
        fmVarArr[i152] = a121;
        int i153 = i152 + 1;
        C2491fm a122 = C4105zY.m41624a(Player.class, "63a60b566914169e23ed73f201791f32", i153);
        iVq = a122;
        fmVarArr[i153] = a122;
        int i154 = i153 + 1;
        C2491fm a123 = C4105zY.m41624a(Player.class, "0cfd56c1d932ba97d6fe0c6ea14f48b9", i154);
        iVr = a123;
        fmVarArr[i154] = a123;
        int i155 = i154 + 1;
        C2491fm a124 = C4105zY.m41624a(Player.class, "1a750dc248eb6542bad489e76da56ac1", i155);
        iVs = a124;
        fmVarArr[i155] = a124;
        int i156 = i155 + 1;
        C2491fm a125 = C4105zY.m41624a(Player.class, "c288144cc08d22a6f39a3c0cf68bc2e1", i156);
        dXe = a125;
        fmVarArr[i156] = a125;
        int i157 = i156 + 1;
        C2491fm a126 = C4105zY.m41624a(Player.class, "0787b6f9f0af82c562f2703be8aa831f", i157);
        iVt = a126;
        fmVarArr[i157] = a126;
        int i158 = i157 + 1;
        C2491fm a127 = C4105zY.m41624a(Player.class, "aee51d477b4c2772c35ef30003ac8398", i158);
        iVu = a127;
        fmVarArr[i158] = a127;
        int i159 = i158 + 1;
        C2491fm a128 = C4105zY.m41624a(Player.class, "339693a48e2031b8eec8d16c4acc2fd1", i159);
        iVv = a128;
        fmVarArr[i159] = a128;
        int i160 = i159 + 1;
        C2491fm a129 = C4105zY.m41624a(Player.class, "c88e624761ec4f0bd4c5c59ac41890f8", i160);
        iVw = a129;
        fmVarArr[i160] = a129;
        int i161 = i160 + 1;
        C2491fm a130 = C4105zY.m41624a(Player.class, "10e62dc5445e80a9c8bf6b52a7a46815", i161);
        iVx = a130;
        fmVarArr[i161] = a130;
        int i162 = i161 + 1;
        C2491fm a131 = C4105zY.m41624a(Player.class, "c9bd2f43ec0d338b1875deae21e0c004", i162);
        iVy = a131;
        fmVarArr[i162] = a131;
        int i163 = i162 + 1;
        C2491fm a132 = C4105zY.m41624a(Player.class, "7dc13c03ca631a9ca69d63216fab4a2d", i163);
        iVz = a132;
        fmVarArr[i163] = a132;
        int i164 = i163 + 1;
        C2491fm a133 = C4105zY.m41624a(Player.class, "1ff89df79b3184f331b8b2587deaa728", i164);
        hEI = a133;
        fmVarArr[i164] = a133;
        int i165 = i164 + 1;
        C2491fm a134 = C4105zY.m41624a(Player.class, "c1c37bea538eaf01b0f6b0d5f47d7df1", i165);
        iVA = a134;
        fmVarArr[i165] = a134;
        int i166 = i165 + 1;
        C2491fm a135 = C4105zY.m41624a(Player.class, "42106b0fc96518a3696adfad00152cf6", i166);
        iVB = a135;
        fmVarArr[i166] = a135;
        int i167 = i166 + 1;
        C2491fm a136 = C4105zY.m41624a(Player.class, "dc3075a5c1f9b2b58306b3d3eaba3772", i167);
        fgv = a136;
        fmVarArr[i167] = a136;
        int i168 = i167 + 1;
        C2491fm a137 = C4105zY.m41624a(Player.class, "08d315b0eb477254b57438862d4e5d74", i168);
        fgc = a137;
        fmVarArr[i168] = a137;
        int i169 = i168 + 1;
        C2491fm a138 = C4105zY.m41624a(Player.class, "4c2b1b0331971eb851b56e93a79a250b", i169);
        iVC = a138;
        fmVarArr[i169] = a138;
        int i170 = i169 + 1;
        C2491fm a139 = C4105zY.m41624a(Player.class, "7d51bbb46ccb703039458cf7425effb0", i170);
        iVD = a139;
        fmVarArr[i170] = a139;
        int i171 = i170 + 1;
        C2491fm a140 = C4105zY.m41624a(Player.class, "b29cb843b176b7514431d2fdb27b20b8", i171);
        iVE = a140;
        fmVarArr[i171] = a140;
        int i172 = i171 + 1;
        C2491fm a141 = C4105zY.m41624a(Player.class, "764604f1ac03c518760b21941ec184ee", i172);
        iVF = a141;
        fmVarArr[i172] = a141;
        int i173 = i172 + 1;
        C2491fm a142 = C4105zY.m41624a(Player.class, "7939d73685be02a09673cc1c5d7bc44e", i173);
        iVG = a142;
        fmVarArr[i173] = a142;
        int i174 = i173 + 1;
        C2491fm a143 = C4105zY.m41624a(Player.class, "fb03df933ee8f051bf8fda4bdcd07089", i174);
        iVH = a143;
        fmVarArr[i174] = a143;
        int i175 = i174 + 1;
        C2491fm a144 = C4105zY.m41624a(Player.class, "0f2146f4c259b5253d4819747f57687e", i175);
        fgw = a144;
        fmVarArr[i175] = a144;
        int i176 = i175 + 1;
        C2491fm a145 = C4105zY.m41624a(Player.class, "399c3f5613ee6733f1f10a3fa0fb6d06", i176);
        f4781gQ = a145;
        fmVarArr[i176] = a145;
        int i177 = i176 + 1;
        C2491fm a146 = C4105zY.m41624a(Player.class, "3e40058470818a26fdcd85cc3a7701d9", i177);
        aKm = a146;
        fmVarArr[i177] = a146;
        int i178 = i177 + 1;
        C2491fm a147 = C4105zY.m41624a(Player.class, "5a78b6deb36afe45976db5ce586d4091", i178);
        iVI = a147;
        fmVarArr[i178] = a147;
        int i179 = i178 + 1;
        C2491fm a148 = C4105zY.m41624a(Player.class, "63c41518edcd51ca2fa365795d049e09", i179);
        iVJ = a148;
        fmVarArr[i179] = a148;
        int i180 = i179 + 1;
        C2491fm a149 = C4105zY.m41624a(Player.class, "0bcf41537eebbd1d6a321e865ac2c1a2", i180);
        bpJ = a149;
        fmVarArr[i180] = a149;
        int i181 = i180 + 1;
        _m_methods = fmVarArr;
        C1634Xv.m11725a((Object[]) Character._m_methods, (Object[]) _m_methods);
        ___iScriptClass = aUO.m18566a(Player.class, C6163ahf.class, _m_fields, _m_methods);
    }

    @C0064Am(aul = "08d315b0eb477254b57438862d4e5d74", aum = 0)
    @C5566aOg
    @C2499fr
    /* renamed from: O */
    private void m23232O(Ship fAVar) {
        throw new aWi(new aCE(this, fgc, new Object[]{fAVar}));
    }

    @C0064Am(aul = "3e40058470818a26fdcd85cc3a7701d9", aum = 0)
    @C5566aOg
    /* renamed from: QR */
    private void m23233QR() {
        throw new aWi(new aCE(this, aKm, new Object[0]));
    }

    @C5566aOg
    @C0064Am(aul = "bd67b9a749a5b384357304e4c6e44e3a", aum = 0)
    @C2499fr
    /* renamed from: S */
    private boolean m23236S(List<Player> list) {
        throw new aWi(new aCE(this, iUs, new Object[]{list}));
    }

    @C0064Am(aul = "65d03f9c33b8984c6674aaa01afb20be", aum = 0)
    @C5566aOg
    /* renamed from: U */
    private ItemLocation m23237U(Item auq) {
        throw new aWi(new aCE(this, iUS, new Object[]{auq}));
    }

    @C0064Am(aul = "339693a48e2031b8eec8d16c4acc2fd1", aum = 0)
    @C5566aOg
    /* renamed from: W */
    private void m23239W(Item auq) {
        throw new aWi(new aCE(this, iVv, new Object[]{auq}));
    }

    @C0064Am(aul = "10e62dc5445e80a9c8bf6b52a7a46815", aum = 0)
    @C5566aOg
    /* renamed from: Y */
    private void m23240Y(Item auq) {
        throw new aWi(new aCE(this, iVx, new Object[]{auq}));
    }

    @C5566aOg
    /* renamed from: Z */
    private void m23241Z(Item auq) {
        switch (bFf().mo6893i(iVx)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, iVx, new Object[]{auq}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, iVx, new Object[]{auq}));
                break;
        }
        m23240Y(auq);
    }

    @C0064Am(aul = "63a60b566914169e23ed73f201791f32", aum = 0)
    @C5566aOg
    @C2499fr
    /* renamed from: a */
    private void m23242a(Station bf, ShipType ng, WeaponType apt, MissionTemplate avh) {
        throw new aWi(new aCE(this, iVq, new Object[]{bf, ng, apt, avh}));
    }

    /* renamed from: a */
    private void m23244a(CitizenshipControl qo) {
        bFf().mo5608dq().mo3197f(iTl, qo);
    }

    /* renamed from: a */
    private void m23245a(CloningInfo ti) {
        bFf().mo5608dq().mo3197f(iTc, ti);
    }

    /* renamed from: a */
    private void m23246a(Trade td) {
        bFf().mo5608dq().mo3197f(fkz, td);
    }

    @C4034yP
    @C5566aOg
    @C0064Am(aul = "70b47e95b58bb6b023e96ea91df99b1a", aum = 0)
    @C2499fr
    /* renamed from: a */
    private void m23247a(C1368Tv.C1369a aVar, String str) {
        throw new aWi(new aCE(this, iTF, new Object[]{aVar, str}));
    }

    /* renamed from: a */
    private void m23248a(OniLogger aex) {
        bFf().mo5608dq().mo3197f(iTj, aex);
    }

    /* renamed from: a */
    private void m23249a(PlayerAssistant avw) {
        bFf().mo5608dq().mo3197f(iTk, avw);
    }

    /* renamed from: a */
    private void m23250a(ItemsDurabilityTask aVar) {
        bFf().mo5608dq().mo3197f(iTr, aVar);
    }

    /* renamed from: a */
    private void m23251a(PlayerTimeoutTask bVar) {
        bFf().mo5608dq().mo3197f(iTu, bVar);
    }

    @C4034yP
    @C5566aOg
    @C0064Am(aul = "d635eb6038cd879afe4d0deb07f50acc", aum = 0)
    @C2499fr
    /* renamed from: a */
    private void m23252a(Player aku, C1191Rb.C1192a aVar, boolean z) {
        throw new aWi(new aCE(this, iUa, new Object[]{aku, aVar, new Boolean(z)}));
    }

    /* renamed from: a */
    private void m23253a(CorporationCreationStatus baVar) {
        bFf().mo5608dq().mo3197f(iTi, baVar);
    }

    @C0064Am(aul = "e62c968bfba2dca7a1a1a29425b327ec", aum = 0)
    @C5566aOg
    /* renamed from: a */
    private void m23254a(C2546ge geVar, int i) {
        throw new aWi(new aCE(this, iUX, new Object[]{geVar, new Integer(i)}));
    }

    /* renamed from: a */
    private void m23255a(PlayerSocialController qhVar) {
        bFf().mo5608dq().mo3197f(iTm, qhVar);
    }

    @C0064Am(aul = "1496d83b4b076fb3969c09a95369988e", aum = 0)
    @C5566aOg
    @C2499fr
    /* renamed from: a */
    private void m23256a(ProgressionCareer sbVar, int i) {
        throw new aWi(new aCE(this, iVp, new Object[]{sbVar, new Integer(i)}));
    }

    @C0064Am(aul = "96a5caf4e2b6d095fd95844c42488843", aum = 0)
    @C5566aOg
    @C2499fr
    /* renamed from: aE */
    private Hangar m23260aE(Station bf) {
        throw new aWi(new aCE(this, iUH, new Object[]{bf}));
    }

    @C5566aOg
    @C2499fr
    /* renamed from: aF */
    private Hangar m23261aF(Station bf) {
        switch (bFf().mo6893i(iUH)) {
            case 0:
                return null;
            case 2:
                return (Hangar) bFf().mo5606d(new aCE(this, iUH, new Object[]{bf}));
            case 3:
                bFf().mo5606d(new aCE(this, iUH, new Object[]{bf}));
                break;
        }
        return m23260aE(bf);
    }

    @C0064Am(aul = "0aab5b0678e6e2e4da404a93ccd2e0d6", aum = 0)
    @C5566aOg
    @C2499fr
    /* renamed from: aG */
    private void m23262aG(Station bf) {
        throw new aWi(new aCE(this, iVb, new Object[]{bf}));
    }

    private Loot aPh() {
        return (Loot) bFf().mo5608dq().mo3214p(cUH);
    }

    @C0064Am(aul = "0bcf41537eebbd1d6a321e865ac2c1a2", aum = 0)
    private Controller afJ() {
        return dxc();
    }

    /* renamed from: ap */
    private void m23263ap(String str) {
        bFf().mo5608dq().mo3197f(f4782zQ, str);
    }

    @C0064Am(aul = "6dcf927a66bf5f2898054356cd4d72fa", aum = 0)
    @C5566aOg
    @C2499fr
    /* renamed from: aq */
    private void m23264aq(String str) {
        throw new aWi(new aCE(this, f4779Pg, new Object[]{str}));
    }

    /* renamed from: as */
    private void m23265as(C1556Wo wo) {
        bFf().mo5608dq().mo3197f(iTp, wo);
    }

    private Faction azq() {
        return (Faction) bFf().mo5608dq().mo3214p(cnU);
    }

    @C0064Am(aul = "b06b99f3d3faac2ed13c0e44300b5813", aum = 0)
    @C5566aOg
    /* renamed from: b */
    private void m23269b(Trade td) {
        throw new aWi(new aCE(this, iUe, new Object[]{td}));
    }

    @C4034yP
    @C5566aOg
    @C2499fr
    /* renamed from: b */
    private void m23270b(Player aku, C1191Rb.C1192a aVar, boolean z) {
        switch (bFf().mo6893i(iUa)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, iUa, new Object[]{aku, aVar, new Boolean(z)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, iUa, new Object[]{aku, aVar, new Boolean(z)}));
                break;
        }
        m23252a(aku, aVar, z);
    }

    @C0064Am(aul = "b551ffb5048a5fc49558bcb7ebc7298b", aum = 0)
    @C5566aOg
    /* renamed from: b */
    private void m23271b(CorporationCreationStatus baVar) {
        throw new aWi(new aCE(this, iUA, new Object[]{baVar}));
    }

    @C0064Am(aul = "2dff80ca4081a19c3e11cefdd6991f29", aum = 0)
    @C5566aOg
    /* renamed from: b */
    private void m23273b(Object obj, Collection<C6950awp> collection) {
        throw new aWi(new aCE(this, iVi, new Object[]{obj, collection}));
    }

    @C0401Fa
    /* renamed from: b */
    private void m23274b(Collection<C6950awp> collection, aDR adr) {
        switch (bFf().mo6893i(iVj)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, iVj, new Object[]{collection, adr}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, iVj, new Object[]{collection, adr}));
                break;
        }
        m23257a(collection, adr);
    }

    /* renamed from: b */
    private void m23275b(List<C5820abA> list, Station bf, Ship fAVar) {
        switch (bFf().mo6893i(iUU)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, iUU, new Object[]{list, bf, fAVar}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, iUU, new Object[]{list, bf, fAVar}));
                break;
        }
        m23258a(list, bf, fAVar);
    }

    private User bOn() {
        return (User) bFf().mo5608dq().mo3214p(avF);
    }

    @C0064Am(aul = "f8479bbf784bd91416a1cf7e68645eeb", aum = 0)
    @C5566aOg
    private I18NString bQD() {
        throw new aWi(new aCE(this, fgi, new Object[0]));
    }

    @C0064Am(aul = "dc3075a5c1f9b2b58306b3d3eaba3772", aum = 0)
    @C5566aOg
    private CharacterProgression bQJ() {
        throw new aWi(new aCE(this, fgv, new Object[0]));
    }

    @C0064Am(aul = "0f2146f4c259b5253d4819747f57687e", aum = 0)
    @C5566aOg
    private void bQL() {
        throw new aWi(new aCE(this, fgw, new Object[0]));
    }

    private Trade bRA() {
        return (Trade) bFf().mo5608dq().mo3214p(fkz);
    }

    private Corporation bXM() {
        return (Corporation) bFf().mo5608dq().mo3214p(fHY);
    }

    @C4034yP
    @C5566aOg
    @C0064Am(aul = "ef8ef535643a5d53d1a167d295b88a3a", aum = 0)
    @C2499fr
    /* renamed from: be */
    private void m23277be(String str, String str2) {
        throw new aWi(new aCE(this, iTG, new Object[]{str, str2}));
    }

    /* renamed from: c */
    private void m23279c(Party gt) {
        bFf().mo5608dq().mo3197f(cfD, gt);
    }

    @C5566aOg
    /* renamed from: c */
    private void m23280c(Trade td) {
        switch (bFf().mo6893i(iUe)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, iUe, new Object[]{td}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, iUe, new Object[]{td}));
                break;
        }
        m23269b(td);
    }

    /* renamed from: c */
    private void m23281c(Associates adq) {
        bFf().mo5608dq().mo3197f(iTb, adq);
    }

    /* renamed from: c */
    private void m23282c(PlayerSocialController qhVar) {
        switch (bFf().mo6893i(iTx)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, iTx, new Object[]{qhVar}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, iTx, new Object[]{qhVar}));
                break;
        }
        m23272b(qhVar);
    }

    @C0064Am(aul = "60a343a3904c432d06e6b5277b210106", aum = 0)
    @C5566aOg
    @C2499fr
    /* renamed from: c */
    private void m23283c(String str, Asset tCVar) {
        throw new aWi(new aCE(this, iUv, new Object[]{str, tCVar}));
    }

    @C0064Am(aul = "cb168cec98d48307f96c864914b67c07", aum = 0)
    @C5566aOg
    @C2499fr
    /* renamed from: c */
    private void m23284c(boolean z, boolean z2) {
        throw new aWi(new aCE(this, iUD, new Object[]{new Boolean(z), new Boolean(z2)}));
    }

    /* renamed from: cB */
    private void m23285cB(C3438ra raVar) {
        bFf().mo5608dq().mo3197f(iTd, raVar);
    }

    /* renamed from: cC */
    private void m23286cC(C3438ra raVar) {
        bFf().mo5608dq().mo3197f(iTe, raVar);
    }

    /* renamed from: cD */
    private void m23287cD(C3438ra raVar) {
        bFf().mo5608dq().mo3197f(iTf, raVar);
    }

    /* renamed from: cE */
    private void m23288cE(C3438ra raVar) {
        bFf().mo5608dq().mo3197f(iTg, raVar);
    }

    @C0064Am(aul = "defe36efb4d9194f1a79f434ddddb32b", aum = 0)
    @C5566aOg
    /* renamed from: cI */
    private void m23289cI(Player aku) {
        throw new aWi(new aCE(this, iTH, new Object[]{aku}));
    }

    @C0064Am(aul = "b08540ae478009791a8e926fe76a591d", aum = 0)
    @C5566aOg
    /* renamed from: cK */
    private boolean m23290cK(Player aku) {
        throw new aWi(new aCE(this, iTO, new Object[]{aku}));
    }

    @C0064Am(aul = "4a97ed51ba79ec323da27c45dca9999f", aum = 0)
    @C5566aOg
    /* renamed from: cO */
    private void m23292cO(Player aku) {
        throw new aWi(new aCE(this, iTV, new Object[]{aku}));
    }

    @C0064Am(aul = "a48e2cec190c8e942bba130dcf31a870", aum = 0)
    @C5566aOg
    /* renamed from: cQ */
    private void m23293cQ(Player aku) {
        throw new aWi(new aCE(this, iTW, new Object[]{aku}));
    }

    @C4034yP
    @C5566aOg
    @C0064Am(aul = "29ce6fcf990cc81677ce5e426a96aca6", aum = 0)
    @C2499fr
    /* renamed from: cS */
    private void m23294cS(Player aku) {
        throw new aWi(new aCE(this, iTY, new Object[]{aku}));
    }

    private int cSF() {
        return bFf().mo5608dq().mo3212n(hyN);
    }

    @C4034yP
    @C5566aOg
    @C2499fr
    /* renamed from: cT */
    private void m23295cT(Player aku) {
        switch (bFf().mo6893i(iTY)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, iTY, new Object[]{aku}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, iTY, new Object[]{aku}));
                break;
        }
        m23294cS(aku);
    }

    @C4034yP
    @C5566aOg
    @C0064Am(aul = "accbcfccafbcc19e2ac52d78649c76e2", aum = 0)
    @C2499fr
    /* renamed from: cU */
    private void m23296cU(Player aku) {
        throw new aWi(new aCE(this, iTZ, new Object[]{aku}));
    }

    @C4034yP
    @C5566aOg
    @C2499fr
    /* renamed from: cV */
    private void m23297cV(Player aku) {
        switch (bFf().mo6893i(iTZ)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, iTZ, new Object[]{aku}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, iTZ, new Object[]{aku}));
                break;
        }
        m23296cU(aku);
    }

    @C4034yP
    @C5566aOg
    @C0064Am(aul = "eafd350310b64eb637633f1c59796b0c", aum = 0)
    @C2499fr
    /* renamed from: cW */
    private void m23298cW(Player aku) {
        throw new aWi(new aCE(this, iUn, new Object[]{aku}));
    }

    @C4034yP
    @C5566aOg
    @C0064Am(aul = "d8b45d472800adbae7f32fe01c89f277", aum = 0)
    @C2499fr
    /* renamed from: cX */
    private void m23299cX(Player aku) {
        throw new aWi(new aCE(this, iUp, new Object[]{aku}));
    }

    @C0064Am(aul = "b7c39903be1b756ee2b4fc0024b87dbe", aum = 0)
    @C5566aOg
    /* renamed from: d */
    private void m23303d(Party gt) {
        throw new aWi(new aCE(this, iTP, new Object[]{gt}));
    }

    @C0064Am(aul = "32a1d23248de230824f1dc22c10a24a9", aum = 0)
    @C5566aOg
    /* renamed from: d */
    private void m23304d(Trade td) {
        throw new aWi(new aCE(this, iUf, new Object[]{td}));
    }

    @C0064Am(aul = "efbb472fbcb132ac558cf84b76abfa84", aum = 0)
    @C5566aOg
    /* renamed from: d */
    private void m23305d(Associates adq) {
        throw new aWi(new aCE(this, iUk, new Object[]{adq}));
    }

    /* renamed from: d */
    private void m23306d(Loot ael) {
        bFf().mo5608dq().mo3197f(cUH, ael);
    }

    /* renamed from: d */
    private void m23307d(aMU amu) {
        bFf().mo5608dq().mo3197f(iTt, amu);
    }

    @C0064Am(aul = "153aaeea90845dd8672328c130903a99", aum = 0)
    @C5566aOg
    /* renamed from: d */
    private void m23308d(C2546ge geVar) {
        throw new aWi(new aCE(this, iUZ, new Object[]{geVar}));
    }

    @C0064Am(aul = "399c3f5613ee6733f1f10a3fa0fb6d06", aum = 0)
    @C5566aOg
    /* renamed from: dd */
    private void m23312dd() {
        throw new aWi(new aCE(this, f4781gQ, new Object[0]));
    }

    @C4034yP
    @C5566aOg
    @C0064Am(aul = "9c970fcff9d0575486336ae7419fa17b", aum = 0)
    @C2499fr
    private void dtL() {
        throw new aWi(new aCE(this, iKQ, new Object[0]));
    }

    private C3438ra dwA() {
        return (C3438ra) bFf().mo5608dq().mo3214p(iTe);
    }

    /* access modifiers changed from: private */
    public C3438ra dwB() {
        return (C3438ra) bFf().mo5608dq().mo3214p(iTf);
    }

    private C3438ra dwC() {
        return (C3438ra) bFf().mo5608dq().mo3214p(iTg);
    }

    private BankAccount dwD() {
        return (BankAccount) bFf().mo5608dq().mo3214p(iTh);
    }

    private CorporationCreationStatus dwE() {
        return (CorporationCreationStatus) bFf().mo5608dq().mo3214p(iTi);
    }

    private OniLogger dwF() {
        return (OniLogger) bFf().mo5608dq().mo3214p(iTj);
    }

    private PlayerAssistant dwG() {
        return (PlayerAssistant) bFf().mo5608dq().mo3214p(iTk);
    }

    private CitizenshipControl dwH() {
        return (CitizenshipControl) bFf().mo5608dq().mo3214p(iTl);
    }

    private PlayerSocialController dwI() {
        return (PlayerSocialController) bFf().mo5608dq().mo3214p(iTm);
    }

    private boolean dwJ() {
        return bFf().mo5608dq().mo3201h(iTn);
    }

    private boolean dwK() {
        return bFf().mo5608dq().mo3201h(iTo);
    }

    private C1556Wo dwL() {
        return (C1556Wo) bFf().mo5608dq().mo3214p(iTp);
    }

    private PlayerSessionLog dwM() {
        return (PlayerSessionLog) bFf().mo5608dq().mo3214p(iTq);
    }

    private ItemsDurabilityTask dwN() {
        return (ItemsDurabilityTask) bFf().mo5608dq().mo3214p(iTr);
    }

    private boolean dwO() {
        return bFf().mo5608dq().mo3201h(iTs);
    }

    private aMU dwP() {
        return (aMU) bFf().mo5608dq().mo3214p(iTt);
    }

    private PlayerTimeoutTask dwQ() {
        return (PlayerTimeoutTask) bFf().mo5608dq().mo3214p(iTu);
    }

    private long dwR() {
        return bFf().mo5608dq().mo3213o(iTv);
    }

    private long dwS() {
        return bFf().mo5608dq().mo3213o(iTw);
    }

    @C0064Am(aul = "99cb66af10822474af796fa61d2c75c9", aum = 0)
    @C5566aOg
    @C2499fr
    private void dwX() {
        throw new aWi(new aCE(this, iTC, new Object[0]));
    }

    @C0064Am(aul = "d31036a4cab65e40ed5b52945ad563c2", aum = 0)
    @C5566aOg
    @C2499fr
    private boolean dwZ() {
        throw new aWi(new aCE(this, iTD, new Object[0]));
    }

    private Party dww() {
        return (Party) bFf().mo5608dq().mo3214p(cfD);
    }

    private Associates dwx() {
        return (Associates) bFf().mo5608dq().mo3214p(iTb);
    }

    private CloningInfo dwy() {
        return (CloningInfo) bFf().mo5608dq().mo3214p(iTc);
    }

    private C3438ra dwz() {
        return (C3438ra) bFf().mo5608dq().mo3214p(iTd);
    }

    @C0064Am(aul = "87f27bcff03899d215b50845aef3ad27", aum = 0)
    @C5566aOg
    private void dxH() {
        throw new aWi(new aCE(this, iUJ, new Object[0]));
    }

    @C0064Am(aul = "3aac2c4c131b908046447392380859ee", aum = 0)
    @C5566aOg
    private void dxJ() {
        throw new aWi(new aCE(this, iUK, new Object[0]));
    }

    @C5566aOg
    private void dxK() {
        switch (bFf().mo6893i(iUK)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, iUK, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, iUK, new Object[0]));
                break;
        }
        dxJ();
    }

    @C0064Am(aul = "f6162c37d89c5d063c4a8160240f48a3", aum = 0)
    @C5566aOg
    private void dxL() {
        throw new aWi(new aCE(this, iUL, new Object[0]));
    }

    @C0064Am(aul = "22c2d259fc05d999bbe5b2cbdfd7611b", aum = 0)
    @C5566aOg
    private void dxN() {
        throw new aWi(new aCE(this, iUM, new Object[0]));
    }

    @C5566aOg
    private void dxO() {
        switch (bFf().mo6893i(iUM)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, iUM, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, iUM, new Object[0]));
                break;
        }
        dxN();
    }

    @C0064Am(aul = "9f4b75cf9de240edaf64c8b2f87b1f0b", aum = 0)
    @C5566aOg
    @C2499fr
    private Storage dxT() {
        throw new aWi(new aCE(this, iUR, new Object[0]));
    }

    @C5566aOg
    @C0064Am(aul = "7ef7cba3e063b28b563e99db368909ae", aum = 0)
    @C2499fr
    private List<C5820abA> dxV() {
        throw new aWi(new aCE(this, iUT, new Object[0]));
    }

    @C4034yP
    @C5566aOg
    @C0064Am(aul = "fd0e1e4a268c8ca0ac3c696e5cb7db73", aum = 0)
    @C2499fr
    private void dxn() {
        throw new aWi(new aCE(this, iUg, new Object[0]));
    }

    @C4034yP
    @C5566aOg
    @C0064Am(aul = "2163ab49aa59ab1ddb4c56e38497c37b", aum = 0)
    @C2499fr
    private void dxp() {
        throw new aWi(new aCE(this, iUh, new Object[0]));
    }

    @C0064Am(aul = "ef20d5134e02d2101cf106efb116ecb4", aum = 0)
    @C5566aOg
    private void dxr() {
        throw new aWi(new aCE(this, iUi, new Object[0]));
    }

    @C0064Am(aul = "e380dd565a8d15aa701fe530f8d3a2bc", aum = 0)
    @C5566aOg
    private void dxv() {
        throw new aWi(new aCE(this, iUw, new Object[0]));
    }

    @C5566aOg
    private void dxw() {
        switch (bFf().mo6893i(iUw)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, iUw, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, iUw, new Object[0]));
                break;
        }
        dxv();
    }

    @C4034yP
    @C5566aOg
    @C0064Am(aul = "e9193297baf984046002c401bebee49b", aum = 0)
    @C2499fr
    private void dxz() {
        throw new aWi(new aCE(this, iUy, new Object[0]));
    }

    @C4034yP
    @C5566aOg
    @C0064Am(aul = "764604f1ac03c518760b21941ec184ee", aum = 0)
    @C2499fr
    private void dyB() {
        throw new aWi(new aCE(this, iVF, new Object[0]));
    }

    @C0064Am(aul = "5a78b6deb36afe45976db5ce586d4091", aum = 0)
    @C5566aOg
    private void dyG() {
        throw new aWi(new aCE(this, iVI, new Object[0]));
    }

    @C0064Am(aul = "63c41518edcd51ca2fa365795d049e09", aum = 0)
    @C5566aOg
    @C2499fr
    private void dyI() {
        throw new aWi(new aCE(this, iVJ, new Object[0]));
    }

    @C0064Am(aul = "25dab5d40f9d7e7dad6d9e7e64ce2cf4", aum = 0)
    @C5566aOg
    private void dyg() {
        throw new aWi(new aCE(this, iVk, new Object[0]));
    }

    @C0064Am(aul = "1a750dc248eb6542bad489e76da56ac1", aum = 0)
    @C5566aOg
    @C2499fr
    private void dym() {
        throw new aWi(new aCE(this, iVs, new Object[0]));
    }

    @C5566aOg
    @C2499fr
    private void dyn() {
        switch (bFf().mo6893i(iVs)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, iVs, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, iVs, new Object[0]));
                break;
        }
        dym();
    }

    @C0064Am(aul = "0787b6f9f0af82c562f2703be8aa831f", aum = 0)
    @C5566aOg
    private void dyo() {
        throw new aWi(new aCE(this, iVt, new Object[0]));
    }

    /* access modifiers changed from: private */
    @C5566aOg
    public void dyp() {
        switch (bFf().mo6893i(iVt)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, iVt, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, iVt, new Object[0]));
                break;
        }
        dyo();
    }

    @C0064Am(aul = "aee51d477b4c2772c35ef30003ac8398", aum = 0)
    @C5566aOg
    private List<Item> dyq() {
        throw new aWi(new aCE(this, iVu, new Object[0]));
    }

    @C4034yP
    @C5566aOg
    @C0064Am(aul = "c88e624761ec4f0bd4c5c59ac41890f8", aum = 0)
    @C2499fr
    private void dys() {
        throw new aWi(new aCE(this, iVw, new Object[0]));
    }

    private void dyw() {
        switch (bFf().mo6893i(iVB)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, iVB, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, iVB, new Object[0]));
                break;
        }
        dyv();
    }

    @C0064Am(aul = "4c2b1b0331971eb851b56e93a79a250b", aum = 0)
    @C5566aOg
    @C2499fr
    private void dyx() {
        throw new aWi(new aCE(this, iVC, new Object[0]));
    }

    /* renamed from: e */
    private void m23313e(PlayerSessionLog al) {
        bFf().mo5608dq().mo3197f(iTq, al);
    }

    @C5566aOg
    /* renamed from: e */
    private void m23314e(Trade td) {
        switch (bFf().mo6893i(iUf)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, iUf, new Object[]{td}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, iUf, new Object[]{td}));
                break;
        }
        m23304d(td);
    }

    @C5566aOg
    /* renamed from: e */
    private void m23316e(Associates adq) {
        switch (bFf().mo6893i(iUk)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, iUk, new Object[]{adq}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, iUk, new Object[]{adq}));
                break;
        }
        m23305d(adq);
    }

    @C0064Am(aul = "c1c37bea538eaf01b0f6b0d5f47d7df1", aum = 0)
    @C5566aOg
    @C2499fr
    /* renamed from: e */
    private void m23317e(aMU amu) {
        throw new aWi(new aCE(this, iVA, new Object[]{amu}));
    }

    /* renamed from: e */
    private void m23318e(BankAccount ado) {
        bFf().mo5608dq().mo3197f(iTh, ado);
    }

    /* renamed from: e */
    private void m23319e(String str, User adk) {
        switch (bFf().mo6893i(iTz)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, iTz, new Object[]{str, adk}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, iTz, new Object[]{str, adk}));
                break;
        }
        m23309d(str, adk);
    }

    @C0064Am(aul = "44a412d752b2206d4979716cdf7363cd", aum = 0)
    @C5566aOg
    /* renamed from: f */
    private void m23320f(Party gt) {
        throw new aWi(new aCE(this, iTU, new Object[]{gt}));
    }

    @C0064Am(aul = "633d62f02bbdf6f885b418c9e3c5a408", aum = 0)
    @C5566aOg
    /* renamed from: f */
    private void m23321f(Loot ael) {
        throw new aWi(new aCE(this, iUW, new Object[]{ael}));
    }

    @C0064Am(aul = "574ea48fdfe8524d25e2eafb8025f2f7", aum = 0)
    @C5566aOg
    /* renamed from: fg */
    private void m23323fg() {
        throw new aWi(new aCE(this, _f_dispose_0020_0028_0029V, new Object[0]));
    }

    @C5566aOg
    /* renamed from: g */
    private void m23324g(Party gt) {
        switch (bFf().mo6893i(iTU)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, iTU, new Object[]{gt}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, iTU, new Object[]{gt}));
                break;
        }
        m23320f(gt);
    }

    /* renamed from: g */
    private void m23325g(BankAccount ado) {
        switch (bFf().mo6893i(iTK)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, iTK, new Object[]{ado}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, iTK, new Object[]{ado}));
                break;
        }
        m23322f(ado);
    }

    @C4034yP
    @C5566aOg
    @C0064Am(aul = "50f7fb26b08a041b4be620027c1a4345", aum = 0)
    @C2499fr
    /* renamed from: g */
    private void m23326g(Player aku, boolean z) {
        throw new aWi(new aCE(this, iTX, new Object[]{aku, new Boolean(z)}));
    }

    @C0064Am(aul = "48e5f8ce0bbcc5fd3b8e4df389ddad48", aum = 0)
    @C5566aOg
    /* renamed from: i */
    private void m23327i(Player aku, boolean z) {
        throw new aWi(new aCE(this, iUb, new Object[]{aku, new Boolean(z)}));
    }

    /* renamed from: k */
    private void m23330k(Corporation aPVar) {
        bFf().mo5608dq().mo3197f(fHY, aPVar);
    }

    /* renamed from: kk */
    private void m23333kk(boolean z) {
        bFf().mo5608dq().mo3153a(iTn, z);
    }

    /* renamed from: kl */
    private void m23334kl(boolean z) {
        bFf().mo5608dq().mo3153a(iTo, z);
    }

    /* renamed from: km */
    private void m23335km(boolean z) {
        bFf().mo5608dq().mo3153a(iTs, z);
    }

    @C4034yP
    @C5566aOg
    @C0064Am(aul = "f49b110a06a8ea3b8b5679e337d82e4b", aum = 0)
    @C2499fr
    /* renamed from: kn */
    private void m23336kn(boolean z) {
        throw new aWi(new aCE(this, iTT, new Object[]{new Boolean(z)}));
    }

    @C0064Am(aul = "9b94c38b0e328f6bea07d6fc8e90f0a9", aum = 0)
    @C5566aOg
    /* renamed from: kp */
    private void m23337kp(boolean z) {
        throw new aWi(new aCE(this, iUm, new Object[]{new Boolean(z)}));
    }

    @C5566aOg
    /* renamed from: kq */
    private void m23338kq(boolean z) {
        switch (bFf().mo6893i(iUm)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, iUm, new Object[]{new Boolean(z)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, iUm, new Object[]{new Boolean(z)}));
                break;
        }
        m23337kp(z);
    }

    @C4034yP
    @C5566aOg
    @C0064Am(aul = "33929c54e785d38d817d1de94aa1736b", aum = 0)
    @C2499fr
    /* renamed from: kr */
    private void m23339kr(boolean z) {
        throw new aWi(new aCE(this, iVg, new Object[]{new Boolean(z)}));
    }

    @C0064Am(aul = "b29cb843b176b7514431d2fdb27b20b8", aum = 0)
    @C5566aOg
    /* renamed from: kt */
    private void m23340kt(boolean z) {
        throw new aWi(new aCE(this, iVE, new Object[]{new Boolean(z)}));
    }

    /* renamed from: l */
    private void m23341l(Faction xm) {
        bFf().mo5608dq().mo3197f(cnU, xm);
    }

    @C4034yP
    @C2499fr
    @C1253SX
    /* renamed from: l */
    private void m23342l(Player aku, boolean z) {
        switch (bFf().mo6893i(iUc)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, iUc, new Object[]{aku, new Boolean(z)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, iUc, new Object[]{aku, new Boolean(z)}));
                break;
        }
        m23331k(aku, z);
    }

    @C0064Am(aul = "27be2cfe2f4b1247e9ba60a14b9d0160", aum = 0)
    @C5566aOg
    @C2499fr
    /* renamed from: m */
    private void m23343m(Faction xm) {
        throw new aWi(new aCE(this, cow, new Object[]{xm}));
    }

    @C4034yP
    @C5566aOg
    @C0064Am(aul = "5671b8b1afc5cb69f193368027d34f68", aum = 0)
    @C2499fr
    /* renamed from: m */
    private void m23344m(Player aku, boolean z) {
        throw new aWi(new aCE(this, iUj, new Object[]{aku, new Boolean(z)}));
    }

    /* renamed from: mb */
    private void m23346mb(long j) {
        bFf().mo5608dq().mo3184b(iTv, j);
    }

    /* renamed from: mc */
    private void m23347mc(long j) {
        bFf().mo5608dq().mo3184b(iTw, j);
    }

    @C0064Am(aul = "c9bd2f43ec0d338b1875deae21e0c004", aum = 0)
    @C5566aOg
    @C2499fr
    /* renamed from: md */
    private boolean m23348md(long j) {
        throw new aWi(new aCE(this, iVy, new Object[]{new Long(j)}));
    }

    @C0064Am(aul = "99045c3a4c230ace897e28b6969fef2b", aum = 0)
    @C5566aOg
    /* renamed from: nH */
    private void m23349nH(String str) {
        throw new aWi(new aCE(this, iTA, new Object[]{str}));
    }

    @C5566aOg
    /* renamed from: nI */
    private void m23350nI(String str) {
        switch (bFf().mo6893i(iTA)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, iTA, new Object[]{str}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, iTA, new Object[]{str}));
                break;
        }
        m23349nH(str);
    }

    @C4034yP
    @C5566aOg
    @C0064Am(aul = "561230076450586a51b0522158d72582", aum = 0)
    @C2499fr
    /* renamed from: nJ */
    private void m23351nJ(String str) {
        throw new aWi(new aCE(this, iTE, new Object[]{str}));
    }

    @C0064Am(aul = "164087923a56c000d47917810a50c754", aum = 0)
    @C5566aOg
    /* renamed from: o */
    private void m23354o(Corporation aPVar) {
        throw new aWi(new aCE(this, iUC, new Object[]{aPVar}));
    }

    @C4034yP
    @C5566aOg
    @C0064Am(aul = "a0e604370aff7297d3e9f8725f8f2ffe", aum = 0)
    @C2499fr
    /* renamed from: o */
    private void m23355o(Player aku, boolean z) {
        throw new aWi(new aCE(this, iUo, new Object[]{aku, new Boolean(z)}));
    }

    @C0064Am(aul = "49f74267f3d30318dd4739227dd71a48", aum = 0)
    @C5566aOg
    /* renamed from: q */
    private void m23356q(Corporation aPVar) {
        throw new aWi(new aCE(this, iUE, new Object[]{aPVar}));
    }

    @C4034yP
    @C5566aOg
    @C0064Am(aul = "a2f0126a4c4a4e66720e532028b902ae", aum = 0)
    @C2499fr
    /* renamed from: q */
    private void m23357q(Player aku, boolean z) {
        throw new aWi(new aCE(this, iUt, new Object[]{aku, new Boolean(z)}));
    }

    @C5566aOg
    @C0064Am(aul = "e42d0add3afa6b59be75fb5f254f41de", aum = 0)
    @C2499fr
    @Deprecated
    /* renamed from: qU */
    private void m23358qU() {
        throw new aWi(new aCE(this, f4777Lm, new Object[0]));
    }

    @C5566aOg
    /* renamed from: r */
    private void m23359r(Corporation aPVar) {
        switch (bFf().mo6893i(iUE)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, iUE, new Object[]{aPVar}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, iUE, new Object[]{aPVar}));
                break;
        }
        m23356q(aPVar);
    }

    @C0064Am(aul = "8b63b5a2727bf9d54d8f40e7f45c9168", aum = 0)
    @C5566aOg
    @C2499fr
    /* renamed from: s */
    private void m23360s(Player aku, boolean z) {
        throw new aWi(new aCE(this, iUu, new Object[]{aku, new Boolean(z)}));
    }

    /* renamed from: t */
    private void m23361t(User adk) {
        bFf().mo5608dq().mo3197f(avF, adk);
    }

    /* renamed from: uU */
    private String m23362uU() {
        return (String) bFf().mo5608dq().mo3214p(f4782zQ);
    }

    /* renamed from: xw */
    private void m23365xw(int i) {
        bFf().mo5608dq().mo3183b(hyN, i);
    }

    @C0064Am(aul = "60f272f62950070e102980ef5b56e45b", aum = 0)
    @C5566aOg
    /* renamed from: xx */
    private void m23366xx(int i) {
        throw new aWi(new aCE(this, hzp, new Object[]{new Integer(i)}));
    }

    @C0401Fa
    /* renamed from: A */
    public void mo14338A(String str, Object obj) {
        switch (bFf().mo6893i(iVm)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, iVm, new Object[]{str, obj}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, iVm, new Object[]{str, obj}));
                break;
        }
        m23367z(str, obj);
    }

    @C5566aOg
    @C2499fr
    /* renamed from: P */
    public void mo12634P(Ship fAVar) {
        switch (bFf().mo6893i(fgc)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, fgc, new Object[]{fAVar}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, fgc, new Object[]{fAVar}));
                break;
        }
        m23232O(fAVar);
    }

    @C5566aOg
    /* renamed from: QS */
    public void mo14339QS() {
        switch (bFf().mo6893i(aKm)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, aKm, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, aKm, new Object[0]));
                break;
        }
        m23233QR();
    }

    @ClientOnly
    /* renamed from: QW */
    public boolean mo961QW() {
        switch (bFf().mo6893i(_f_hasHollowField_0020_0028_0029Z)) {
            case 0:
                return false;
            case 2:
                return ((Boolean) bFf().mo5606d(new aCE(this, _f_hasHollowField_0020_0028_0029Z, new Object[0]))).booleanValue();
            case 3:
                bFf().mo5606d(new aCE(this, _f_hasHollowField_0020_0028_0029Z, new Object[0]));
                break;
        }
        return m23234QV();
    }

    /* renamed from: Qw */
    public BankAccount mo5156Qw() {
        switch (bFf().mo6893i(aJz)) {
            case 0:
                return null;
            case 2:
                return (BankAccount) bFf().mo5606d(new aCE(this, aJz, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, aJz, new Object[0]));
                break;
        }
        return m23235Qv();
    }

    /* renamed from: T */
    public C6494any mo11T() {
        return ___iScriptClass;
    }

    @C5566aOg
    @C2499fr
    /* renamed from: T */
    public boolean mo14340T(List<Player> list) {
        switch (bFf().mo6893i(iUs)) {
            case 0:
                return false;
            case 2:
                return ((Boolean) bFf().mo5606d(new aCE(this, iUs, new Object[]{list}))).booleanValue();
            case 3:
                bFf().mo5606d(new aCE(this, iUs, new Object[]{list}));
                break;
        }
        return m23236S(list);
    }

    /* renamed from: U */
    public C2491fm[] mo12U() {
        return _m_methods;
    }

    @C5566aOg
    /* renamed from: V */
    public ItemLocation mo14341V(Item auq) {
        switch (bFf().mo6893i(iUS)) {
            case 0:
                return null;
            case 2:
                return (ItemLocation) bFf().mo5606d(new aCE(this, iUS, new Object[]{auq}));
            case 3:
                bFf().mo5606d(new aCE(this, iUS, new Object[]{auq}));
                break;
        }
        return m23237U(auq);
    }

    /* renamed from: W */
    public Object mo13W() {
        return new C6163ahf(this);
    }

    @C4034yP
    @C5566aOg
    @C2499fr
    /* renamed from: X */
    public void mo14342X(Player aku) {
        switch (bFf().mo6893i(iUn)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, iUn, new Object[]{aku}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, iUn, new Object[]{aku}));
                break;
        }
        m23298cW(aku);
    }

    @C5566aOg
    /* renamed from: X */
    public void mo14343X(Item auq) {
        switch (bFf().mo6893i(iVv)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, iVv, new Object[]{auq}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, iVv, new Object[]{auq}));
                break;
        }
        m23239W(auq);
    }

    /* renamed from: a */
    public Object mo14a(C0495Gr gr) {
        Object[] args = gr.getArgs();
        switch (gr.mo2417hq() - Character._m_methodCount) {
            case 0:
                m23272b((PlayerSocialController) args[0]);
                return null;
            case 1:
                return dwT();
            case 2:
                m23309d((String) args[0], (User) args[1]);
                return null;
            case 3:
                return m23266au();
            case 4:
                m23264aq((String) args[0]);
                return null;
            case 5:
                m23349nH((String) args[0]);
                return null;
            case 6:
                return m23363uV();
            case 7:
                return bQD();
            case 8:
                return ahP();
            case 9:
                return new Boolean(dwV());
            case 10:
                dwX();
                return null;
            case 11:
                m23323fg();
                return null;
            case 12:
                m23358qU();
                return null;
            case 13:
                return new Boolean(m23234QV());
            case 14:
                return aMu();
            case 15:
                return new Boolean(dwZ());
            case 16:
                return new Boolean(bQA());
            case 17:
                m23351nJ((String) args[0]);
                return null;
            case 18:
                m23247a((C1368Tv.C1369a) args[0], (String) args[1]);
                return null;
            case 19:
                m23277be((String) args[0], (String) args[1]);
                return null;
            case 20:
                m23289cI((Player) args[0]);
                return null;
            case 21:
                return dxb();
            case 22:
                m23332k((PlayerController) args[0]);
                return null;
            case 23:
                return m23235Qv();
            case 24:
                m23322f((BankAccount) args[0]);
                return null;
            case 25:
                m23301cu(((Long) args[0]).longValue());
                return null;
            case 26:
                m23302cw(((Long) args[0]).longValue());
                return null;
            case 27:
                return dxd();
            case 28:
                return bOw();
            case 29:
                m23364v((User) args[0]);
                return null;
            case 30:
                return dxf();
            case 31:
                m23345m((Storage) args[0]);
                return null;
            case 32:
                return new Boolean(m23290cK((Player) args[0]));
            case 33:
                m23303d((Party) args[0]);
                return null;
            case 34:
                return dxh();
            case 35:
                return new Boolean(dxj());
            case 36:
                return new Boolean(m23291cM((Player) args[0]));
            case 37:
                m23336kn(((Boolean) args[0]).booleanValue());
                return null;
            case 38:
                m23320f((Party) args[0]);
                return null;
            case 39:
                m23292cO((Player) args[0]);
                return null;
            case 40:
                m23293cQ((Player) args[0]);
                return null;
            case 41:
                m23326g((Player) args[0], ((Boolean) args[1]).booleanValue());
                return null;
            case 42:
                m23294cS((Player) args[0]);
                return null;
            case 43:
                m23296cU((Player) args[0]);
                return null;
            case 44:
                m23252a((Player) args[0], (C1191Rb.C1192a) args[1], ((Boolean) args[2]).booleanValue());
                return null;
            case 45:
                m23327i((Player) args[0], ((Boolean) args[1]).booleanValue());
                return null;
            case 46:
                m23331k((Player) args[0], ((Boolean) args[1]).booleanValue());
                return null;
            case 47:
                return new Boolean(dxl());
            case 48:
                m23269b((Trade) args[0]);
                return null;
            case 49:
                m23304d((Trade) args[0]);
                return null;
            case 50:
                dxn();
                return null;
            case 51:
                dxp();
                return null;
            case 52:
                dxr();
                return null;
            case 53:
                m23344m((Player) args[0], ((Boolean) args[1]).booleanValue());
                return null;
            case 54:
                m23305d((Associates) args[0]);
                return null;
            case 55:
                return dxt();
            case 56:
                m23337kp(((Boolean) args[0]).booleanValue());
                return null;
            case 57:
                m23298cW((Player) args[0]);
                return null;
            case 58:
                m23355o((Player) args[0], ((Boolean) args[1]).booleanValue());
                return null;
            case 59:
                m23299cX((Player) args[0]);
                return null;
            case 60:
                return new Boolean(m23300cZ((Player) args[0]));
            case 61:
                return new Boolean(m23352nK((String) args[0]));
            case 62:
                return new Boolean(m23236S((List) args[0]));
            case 63:
                m23357q((Player) args[0], ((Boolean) args[1]).booleanValue());
                return null;
            case 64:
                m23360s((Player) args[0], ((Boolean) args[1]).booleanValue());
                return null;
            case 65:
                m23283c((String) args[0], (Asset) args[1]);
                return null;
            case 66:
                dxv();
                return null;
            case 67:
                return dxx();
            case 68:
                dxz();
                return null;
            case 69:
                return dxB();
            case 70:
                m23271b((CorporationCreationStatus) args[0]);
                return null;
            case 71:
                return new Boolean(dxD());
            case 72:
                return bYc();
            case 73:
                m23354o((Corporation) args[0]);
                return null;
            case 74:
                m23284c(((Boolean) args[0]).booleanValue(), ((Boolean) args[1]).booleanValue());
                return null;
            case 75:
                m23356q((Corporation) args[0]);
                return null;
            case 76:
                return dxF();
            case 77:
                return m23259aC((Station) args[0]);
            case 78:
                return m23260aE((Station) args[0]);
            case 79:
                m23328i((Hangar) args[0]);
                return null;
            case 80:
                dxH();
                return null;
            case 81:
                dxJ();
                return null;
            case 82:
                dtL();
                return null;
            case 83:
                dxL();
                return null;
            case 84:
                dxN();
                return null;
            case 85:
                m23267ax((Ship) args[0]);
                return null;
            case 86:
                m23268az((Ship) args[0]);
                return null;
            case 87:
                dxP();
                return null;
            case 88:
                return cff();
            case 89:
                return dxR();
            case 90:
                return dxT();
            case 91:
                return m23237U((Item) args[0]);
            case 92:
                return dxV();
            case 93:
                m23258a((List<C5820abA>) (List) args[0], (Station) args[1], (Ship) args[2]);
                return null;
            case 94:
                return dxX();
            case 95:
                m23321f((Loot) args[0]);
                return null;
            case 96:
                return azM();
            case 97:
                m23343m((Faction) args[0]);
                return null;
            case 98:
                m23254a((C2546ge) args[0], ((Integer) args[1]).intValue());
                return null;
            case 99:
                return new Boolean(m23276b((C2546ge) args[0]));
            case 100:
                m23308d((C2546ge) args[0]);
                return null;
            case 101:
                return dxY();
            case 102:
                m23262aG((Station) args[0]);
                return null;
            case 103:
                return dya();
            case 104:
                return new Boolean(dyc());
            case 105:
                m23353nM((String) args[0]);
                return null;
            case 106:
                return new Boolean(dye());
            case 107:
                m23339kr(((Boolean) args[0]).booleanValue());
                return null;
            case 108:
                m23278bf((String) args[0], (String) args[1]);
                return null;
            case 109:
                m23273b(args[0], (Collection<C6950awp>) (Collection) args[1]);
                return null;
            case 110:
                m23257a((Collection<C6950awp>) (Collection) args[0], (aDR) args[1]);
                return null;
            case 111:
                return new Integer(cTd());
            case 112:
                m23366xx(((Integer) args[0]).intValue());
                return null;
            case 113:
                dyg();
                return null;
            case 114:
                return new Long(cMJ());
            case 115:
                return dyi();
            case 116:
                m23367z((String) args[0], args[1]);
                return null;
            case 117:
                m23315e((C1506WA) args[0]);
                return null;
            case 118:
                return dyk();
            case 119:
                return cXj();
            case 120:
                m23256a((ProgressionCareer) args[0], ((Integer) args[1]).intValue());
                return null;
            case 121:
                m23242a((Station) args[0], (ShipType) args[1], (WeaponType) args[2], (MissionTemplate) args[3]);
                return null;
            case 122:
                m23243a((Station) args[0], (ShipType) args[1], (List<WeaponType>) (List) args[2], (MissionTemplate) args[3]);
                return null;
            case 123:
                dym();
                return null;
            case 124:
                return new Long(m23329j((CitizenshipType) args[0]));
            case 125:
                dyo();
                return null;
            case 126:
                return dyq();
            case 127:
                m23239W((Item) args[0]);
                return null;
            case 128:
                dys();
                return null;
            case 129:
                m23240Y((Item) args[0]);
                return null;
            case 130:
                return new Boolean(m23348md(((Long) args[0]).longValue()));
            case 131:
                return new Boolean(dyu());
            case 132:
                return cXl();
            case 133:
                m23317e((aMU) args[0]);
                return null;
            case 134:
                dyv();
                return null;
            case 135:
                return bQJ();
            case 136:
                m23232O((Ship) args[0]);
                return null;
            case 137:
                dyx();
                return null;
            case 138:
                return new Boolean(dyz());
            case 139:
                m23340kt(((Boolean) args[0]).booleanValue());
                return null;
            case 140:
                dyB();
                return null;
            case 141:
                return new Boolean(dyD());
            case 142:
                dyE();
                return null;
            case 143:
                bQL();
                return null;
            case 144:
                m23312dd();
                return null;
            case 145:
                m23233QR();
                return null;
            case 146:
                dyG();
                return null;
            case 147:
                dyI();
                return null;
            case 148:
                return afJ();
            default:
                return super.mo14a(gr);
        }
    }

    @C5566aOg
    /* renamed from: a */
    public void mo14344a(Object obj, Collection<C6950awp> collection) {
        switch (bFf().mo6893i(iVi)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, iVi, new Object[]{obj, collection}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, iVi, new Object[]{obj, collection}));
                break;
        }
        m23273b(obj, collection);
    }

    /* renamed from: a */
    public void mo15a(Collection collection, C0495Gr gr) {
        super.mo15a(collection, gr);
    }

    /* renamed from: aA */
    public void mo14345aA(Ship fAVar) {
        switch (bFf().mo6893i(iUO)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, iUO, new Object[]{fAVar}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, iUO, new Object[]{fAVar}));
                break;
        }
        m23268az(fAVar);
    }

    /* renamed from: aD */
    public Hangar mo14346aD(Station bf) {
        switch (bFf().mo6893i(iUG)) {
            case 0:
                return null;
            case 2:
                return (Hangar) bFf().mo5606d(new aCE(this, iUG, new Object[]{bf}));
            case 3:
                bFf().mo5606d(new aCE(this, iUG, new Object[]{bf}));
                break;
        }
        return m23259aC(bf);
    }

    @C5566aOg
    @C2499fr
    /* renamed from: aH */
    public void mo14347aH(Station bf) {
        switch (bFf().mo6893i(iVb)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, iVb, new Object[]{bf}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, iVb, new Object[]{bf}));
                break;
        }
        m23262aG(bf);
    }

    @ClientOnly
    public String ahQ() {
        switch (bFf().mo6893i(brK)) {
            case 0:
                return null;
            case 2:
                return (String) bFf().mo5606d(new aCE(this, brK, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, brK, new Object[0]));
                break;
        }
        return ahP();
    }

    public IEngineGame ald() {
        switch (bFf().mo6893i(f4780x3cb2b465)) {
            case 0:
                return null;
            case 2:
                return (IEngineGame) bFf().mo5606d(new aCE(this, f4780x3cb2b465, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f4780x3cb2b465, new Object[0]));
                break;
        }
        return aMu();
    }

    /* renamed from: am */
    public void mo14348am(String str, String str2) {
        switch (bFf().mo6893i(iVh)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, iVh, new Object[]{str, str2}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, iVh, new Object[]{str, str2}));
                break;
        }
        m23278bf(str, str2);
    }

    @C4034yP
    @C5566aOg
    @C2499fr
    /* renamed from: ax */
    public void mo14349ax(String str, String str2) {
        switch (bFf().mo6893i(iTG)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, iTG, new Object[]{str, str2}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, iTG, new Object[]{str, str2}));
                break;
        }
        m23277be(str, str2);
    }

    /* renamed from: ay */
    public void mo14350ay(Ship fAVar) {
        switch (bFf().mo6893i(iUN)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, iUN, new Object[]{fAVar}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, iUN, new Object[]{fAVar}));
                break;
        }
        m23267ax(fAVar);
    }

    public Faction azN() {
        switch (bFf().mo6893i(cov)) {
            case 0:
                return null;
            case 2:
                return (Faction) bFf().mo5606d(new aCE(this, cov, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, cov, new Object[0]));
                break;
        }
        return azM();
    }

    @C5566aOg
    @C2499fr
    /* renamed from: b */
    public void mo14351b(Station bf, ShipType ng, WeaponType apt, MissionTemplate avh) {
        switch (bFf().mo6893i(iVq)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, iVq, new Object[]{bf, ng, apt, avh}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, iVq, new Object[]{bf, ng, apt, avh}));
                break;
        }
        m23242a(bf, ng, apt, avh);
    }

    /* renamed from: b */
    public void mo14352b(Station bf, ShipType ng, List<WeaponType> list, MissionTemplate avh) {
        switch (bFf().mo6893i(iVr)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, iVr, new Object[]{bf, ng, list, avh}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, iVr, new Object[]{bf, ng, list, avh}));
                break;
        }
        m23243a(bf, ng, list, avh);
    }

    @C4034yP
    @C5566aOg
    @C2499fr
    /* renamed from: b */
    public void mo14353b(C1368Tv.C1369a aVar, String str) {
        switch (bFf().mo6893i(iTF)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, iTF, new Object[]{aVar, str}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, iTF, new Object[]{aVar, str}));
                break;
        }
        m23247a(aVar, str);
    }

    @C5566aOg
    /* renamed from: b */
    public void mo14354b(C2546ge geVar, int i) {
        switch (bFf().mo6893i(iUX)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, iUX, new Object[]{geVar, new Integer(i)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, iUX, new Object[]{geVar, new Integer(i)}));
                break;
        }
        m23254a(geVar, i);
    }

    @C5566aOg
    @C2499fr
    /* renamed from: b */
    public void mo14355b(ProgressionCareer sbVar, int i) {
        switch (bFf().mo6893i(iVp)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, iVp, new Object[]{sbVar, new Integer(i)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, iVp, new Object[]{sbVar, new Integer(i)}));
                break;
        }
        m23256a(sbVar, i);
    }

    /* access modifiers changed from: protected */
    public Loot bOl() {
        switch (bFf().mo6893i(iUV)) {
            case 0:
                return null;
            case 2:
                return (Loot) bFf().mo5606d(new aCE(this, iUV, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, iUV, new Object[0]));
                break;
        }
        return dxX();
    }

    public User bOx() {
        switch (bFf().mo6893i(fax)) {
            case 0:
                return null;
            case 2:
                return (User) bFf().mo5606d(new aCE(this, fax, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, fax, new Object[0]));
                break;
        }
        return bOw();
    }

    public boolean bQB() {
        switch (bFf().mo6893i(fgf)) {
            case 0:
                return false;
            case 2:
                return ((Boolean) bFf().mo5606d(new aCE(this, fgf, new Object[0]))).booleanValue();
            case 3:
                bFf().mo5606d(new aCE(this, fgf, new Object[0]));
                break;
        }
        return bQA();
    }

    @C5566aOg
    public I18NString bQE() {
        switch (bFf().mo6893i(fgi)) {
            case 0:
                return null;
            case 2:
                return (I18NString) bFf().mo5606d(new aCE(this, fgi, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, fgi, new Object[0]));
                break;
        }
        return bQD();
    }

    @C5566aOg
    public CharacterProgression bQK() {
        switch (bFf().mo6893i(fgv)) {
            case 0:
                return null;
            case 2:
                return (CharacterProgression) bFf().mo5606d(new aCE(this, fgv, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, fgv, new Object[0]));
                break;
        }
        return bQJ();
    }

    @C5566aOg
    public void bQM() {
        switch (bFf().mo6893i(fgw)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, fgw, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, fgw, new Object[0]));
                break;
        }
        bQL();
    }

    public Corporation bYd() {
        switch (bFf().mo6893i(fIk)) {
            case 0:
                return null;
            case 2:
                return (Corporation) bFf().mo5606d(new aCE(this, fIk, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, fIk, new Object[0]));
                break;
        }
        return bYc();
    }

    @C5566aOg
    /* renamed from: c */
    public void mo14359c(CorporationCreationStatus baVar) {
        switch (bFf().mo6893i(iUA)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, iUA, new Object[]{baVar}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, iUA, new Object[]{baVar}));
                break;
        }
        m23271b(baVar);
    }

    /* renamed from: c */
    public boolean mo14361c(C2546ge geVar) {
        switch (bFf().mo6893i(iUY)) {
            case 0:
                return false;
            case 2:
                return ((Boolean) bFf().mo5606d(new aCE(this, iUY, new Object[]{geVar}))).booleanValue();
            case 3:
                bFf().mo5606d(new aCE(this, iUY, new Object[]{geVar}));
                break;
        }
        return m23276b(geVar);
    }

    /* renamed from: c */
    public C5663aRz[] mo25c() {
        return _m_fields;
    }

    @C5566aOg
    /* renamed from: cJ */
    public void mo14362cJ(Player aku) {
        switch (bFf().mo6893i(iTH)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, iTH, new Object[]{aku}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, iTH, new Object[]{aku}));
                break;
        }
        m23289cI(aku);
    }

    @C5566aOg
    /* renamed from: cL */
    public boolean mo14363cL(Player aku) {
        switch (bFf().mo6893i(iTO)) {
            case 0:
                return false;
            case 2:
                return ((Boolean) bFf().mo5606d(new aCE(this, iTO, new Object[]{aku}))).booleanValue();
            case 3:
                bFf().mo5606d(new aCE(this, iTO, new Object[]{aku}));
                break;
        }
        return m23290cK(aku);
    }

    /* renamed from: cN */
    public boolean mo14364cN(Player aku) {
        switch (bFf().mo6893i(iTS)) {
            case 0:
                return false;
            case 2:
                return ((Boolean) bFf().mo5606d(new aCE(this, iTS, new Object[]{aku}))).booleanValue();
            case 3:
                bFf().mo5606d(new aCE(this, iTS, new Object[]{aku}));
                break;
        }
        return m23291cM(aku);
    }

    @C5566aOg
    /* renamed from: cP */
    public void mo14365cP(Player aku) {
        switch (bFf().mo6893i(iTV)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, iTV, new Object[]{aku}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, iTV, new Object[]{aku}));
                break;
        }
        m23292cO(aku);
    }

    @C5566aOg
    /* renamed from: cR */
    public void mo14366cR(Player aku) {
        switch (bFf().mo6893i(iTW)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, iTW, new Object[]{aku}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, iTW, new Object[]{aku}));
                break;
        }
        m23293cQ(aku);
    }

    public int cTe() {
        switch (bFf().mo6893i(hzq)) {
            case 0:
                return 0;
            case 2:
                return ((Integer) bFf().mo5606d(new aCE(this, hzq, new Object[0]))).intValue();
            case 3:
                bFf().mo5606d(new aCE(this, hzq, new Object[0]));
                break;
        }
        return cTd();
    }

    public CitizenshipControl cXk() {
        switch (bFf().mo6893i(hEG)) {
            case 0:
                return null;
            case 2:
                return (CitizenshipControl) bFf().mo5606d(new aCE(this, hEG, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, hEG, new Object[0]));
                break;
        }
        return cXj();
    }

    public aMU cXm() {
        switch (bFf().mo6893i(hEI)) {
            case 0:
                return null;
            case 2:
                return (aMU) bFf().mo5606d(new aCE(this, hEI, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, hEI, new Object[0]));
                break;
        }
        return cXl();
    }

    @C4034yP
    @C5566aOg
    @C2499fr
    /* renamed from: cY */
    public void mo14370cY(Player aku) {
        switch (bFf().mo6893i(iUp)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, iUp, new Object[]{aku}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, iUp, new Object[]{aku}));
                break;
        }
        m23299cX(aku);
    }

    public String cfg() {
        switch (bFf().mo6893i(fRW)) {
            case 0:
                return null;
            case 2:
                return (String) bFf().mo5606d(new aCE(this, fRW, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, fRW, new Object[0]));
                break;
        }
        return cff();
    }

    public long crP() {
        switch (bFf().mo6893i(hmy)) {
            case 0:
                return 0;
            case 2:
                return ((Long) bFf().mo5606d(new aCE(this, hmy, new Object[0]))).longValue();
            case 3:
                bFf().mo5606d(new aCE(this, hmy, new Object[0]));
                break;
        }
        return cMJ();
    }

    /* renamed from: cv */
    public void mo5157cv(long j) {
        switch (bFf().mo6893i(aJA)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, aJA, new Object[]{new Long(j)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, aJA, new Object[]{new Long(j)}));
                break;
        }
        m23301cu(j);
    }

    /* renamed from: cx */
    public void mo5158cx(long j) {
        switch (bFf().mo6893i(aJB)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, aJB, new Object[]{new Long(j)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, aJB, new Object[]{new Long(j)}));
                break;
        }
        m23302cw(j);
    }

    @C5566aOg
    @C2499fr
    /* renamed from: d */
    public void mo14373d(String str, Asset tCVar) {
        switch (bFf().mo6893i(iUv)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, iUv, new Object[]{str, tCVar}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, iUv, new Object[]{str, tCVar}));
                break;
        }
        m23283c(str, tCVar);
    }

    @C5566aOg
    @C2499fr
    /* renamed from: d */
    public void mo14374d(boolean z, boolean z2) {
        switch (bFf().mo6893i(iUD)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, iUD, new Object[]{new Boolean(z), new Boolean(z2)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, iUD, new Object[]{new Boolean(z), new Boolean(z2)}));
                break;
        }
        m23284c(z, z2);
    }

    /* renamed from: da */
    public boolean mo14375da(Player aku) {
        switch (bFf().mo6893i(iUq)) {
            case 0:
                return false;
            case 2:
                return ((Boolean) bFf().mo5606d(new aCE(this, iUq, new Object[]{aku}))).booleanValue();
            case 3:
                bFf().mo5606d(new aCE(this, iUq, new Object[]{aku}));
                break;
        }
        return m23300cZ(aku);
    }

    @C5566aOg
    /* renamed from: de */
    public void mo12653de() {
        switch (bFf().mo6893i(f4781gQ)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f4781gQ, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f4781gQ, new Object[0]));
                break;
        }
        m23312dd();
    }

    @C5566aOg
    public void dispose() {
        switch (bFf().mo6893i(_f_dispose_0020_0028_0029V)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, _f_dispose_0020_0028_0029V, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, _f_dispose_0020_0028_0029V, new Object[0]));
                break;
        }
        m23323fg();
    }

    @C4034yP
    @C5566aOg
    @C2499fr
    public void dtM() {
        switch (bFf().mo6893i(iKQ)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, iKQ, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, iKQ, new Object[0]));
                break;
        }
        dtL();
    }

    public PlayerSocialController dwU() {
        switch (bFf().mo6893i(iTy)) {
            case 0:
                return null;
            case 2:
                return (PlayerSocialController) bFf().mo5606d(new aCE(this, iTy, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, iTy, new Object[0]));
                break;
        }
        return dwT();
    }

    public boolean dwW() {
        switch (bFf().mo6893i(iTB)) {
            case 0:
                return false;
            case 2:
                return ((Boolean) bFf().mo5606d(new aCE(this, iTB, new Object[0]))).booleanValue();
            case 3:
                bFf().mo5606d(new aCE(this, iTB, new Object[0]));
                break;
        }
        return dwV();
    }

    @C5566aOg
    @C2499fr
    public void dwY() {
        switch (bFf().mo6893i(iTC)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, iTC, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, iTC, new Object[0]));
                break;
        }
        dwX();
    }

    @C4034yP
    @C5566aOg
    @C2499fr
    public void dxA() {
        switch (bFf().mo6893i(iUy)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, iUy, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, iUy, new Object[0]));
                break;
        }
        dxz();
    }

    public CorporationCreationStatus dxC() {
        switch (bFf().mo6893i(iUz)) {
            case 0:
                return null;
            case 2:
                return (CorporationCreationStatus) bFf().mo5606d(new aCE(this, iUz, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, iUz, new Object[0]));
                break;
        }
        return dxB();
    }

    public boolean dxE() {
        switch (bFf().mo6893i(iUB)) {
            case 0:
                return false;
            case 2:
                return ((Boolean) bFf().mo5606d(new aCE(this, iUB, new Object[0]))).booleanValue();
            case 3:
                bFf().mo5606d(new aCE(this, iUB, new Object[0]));
                break;
        }
        return dxD();
    }

    public C3438ra<Hangar> dxG() {
        switch (bFf().mo6893i(iUF)) {
            case 0:
                return null;
            case 2:
                return (C3438ra) bFf().mo5606d(new aCE(this, iUF, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, iUF, new Object[0]));
                break;
        }
        return dxF();
    }

    @C5566aOg
    public void dxI() {
        switch (bFf().mo6893i(iUJ)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, iUJ, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, iUJ, new Object[0]));
                break;
        }
        dxH();
    }

    @C5566aOg
    public void dxM() {
        switch (bFf().mo6893i(iUL)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, iUL, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, iUL, new Object[0]));
                break;
        }
        dxL();
    }

    public void dxQ() {
        switch (bFf().mo6893i(iUP)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, iUP, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, iUP, new Object[0]));
                break;
        }
        dxP();
    }

    public PlayerSessionLog dxS() {
        switch (bFf().mo6893i(iUQ)) {
            case 0:
                return null;
            case 2:
                return (PlayerSessionLog) bFf().mo5606d(new aCE(this, iUQ, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, iUQ, new Object[0]));
                break;
        }
        return dxR();
    }

    @C5566aOg
    @C2499fr
    public Storage dxU() {
        switch (bFf().mo6893i(iUR)) {
            case 0:
                return null;
            case 2:
                return (Storage) bFf().mo5606d(new aCE(this, iUR, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, iUR, new Object[0]));
                break;
        }
        return dxT();
    }

    @C5566aOg
    @C2499fr
    public List<C5820abA> dxW() {
        switch (bFf().mo6893i(iUT)) {
            case 0:
                return null;
            case 2:
                return (List) bFf().mo5606d(new aCE(this, iUT, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, iUT, new Object[0]));
                break;
        }
        return dxV();
    }

    public CloningInfo dxZ() {
        switch (bFf().mo6893i(iVa)) {
            case 0:
                return null;
            case 2:
                return (CloningInfo) bFf().mo5606d(new aCE(this, iVa, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, iVa, new Object[0]));
                break;
        }
        return dxY();
    }

    @C5566aOg
    @C2499fr
    public boolean dxa() {
        switch (bFf().mo6893i(iTD)) {
            case 0:
                return false;
            case 2:
                return ((Boolean) bFf().mo5606d(new aCE(this, iTD, new Object[0]))).booleanValue();
            case 3:
                bFf().mo5606d(new aCE(this, iTD, new Object[0]));
                break;
        }
        return dwZ();
    }

    public PlayerController dxc() {
        switch (bFf().mo6893i(iTI)) {
            case 0:
                return null;
            case 2:
                return (PlayerController) bFf().mo5606d(new aCE(this, iTI, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, iTI, new Object[0]));
                break;
        }
        return dxb();
    }

    public Station dxe() {
        switch (bFf().mo6893i(iTL)) {
            case 0:
                return null;
            case 2:
                return (Station) bFf().mo5606d(new aCE(this, iTL, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, iTL, new Object[0]));
                break;
        }
        return dxd();
    }

    public List<Storage> dxg() {
        switch (bFf().mo6893i(iTM)) {
            case 0:
                return null;
            case 2:
                return (List) bFf().mo5606d(new aCE(this, iTM, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, iTM, new Object[0]));
                break;
        }
        return dxf();
    }

    public Party dxi() {
        switch (bFf().mo6893i(iTQ)) {
            case 0:
                return null;
            case 2:
                return (Party) bFf().mo5606d(new aCE(this, iTQ, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, iTQ, new Object[0]));
                break;
        }
        return dxh();
    }

    public boolean dxk() {
        switch (bFf().mo6893i(iTR)) {
            case 0:
                return false;
            case 2:
                return ((Boolean) bFf().mo5606d(new aCE(this, iTR, new Object[0]))).booleanValue();
            case 3:
                bFf().mo5606d(new aCE(this, iTR, new Object[0]));
                break;
        }
        return dxj();
    }

    public boolean dxm() {
        switch (bFf().mo6893i(iUd)) {
            case 0:
                return false;
            case 2:
                return ((Boolean) bFf().mo5606d(new aCE(this, iUd, new Object[0]))).booleanValue();
            case 3:
                bFf().mo5606d(new aCE(this, iUd, new Object[0]));
                break;
        }
        return dxl();
    }

    @C4034yP
    @C5566aOg
    @C2499fr
    public void dxo() {
        switch (bFf().mo6893i(iUg)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, iUg, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, iUg, new Object[0]));
                break;
        }
        dxn();
    }

    @C4034yP
    @C5566aOg
    @C2499fr
    public void dxq() {
        switch (bFf().mo6893i(iUh)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, iUh, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, iUh, new Object[0]));
                break;
        }
        dxp();
    }

    @C5566aOg
    public void dxs() {
        switch (bFf().mo6893i(iUi)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, iUi, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, iUi, new Object[0]));
                break;
        }
        dxr();
    }

    public Associates dxu() {
        switch (bFf().mo6893i(iUl)) {
            case 0:
                return null;
            case 2:
                return (Associates) bFf().mo5606d(new aCE(this, iUl, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, iUl, new Object[0]));
                break;
        }
        return dxt();
    }

    public PlayerView dxy() {
        switch (bFf().mo6893i(iUx)) {
            case 0:
                return null;
            case 2:
                return (PlayerView) bFf().mo5606d(new aCE(this, iUx, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, iUx, new Object[0]));
                break;
        }
        return dxx();
    }

    public boolean dyA() {
        switch (bFf().mo6893i(iVD)) {
            case 0:
                return false;
            case 2:
                return ((Boolean) bFf().mo5606d(new aCE(this, iVD, new Object[0]))).booleanValue();
            case 3:
                bFf().mo5606d(new aCE(this, iVD, new Object[0]));
                break;
        }
        return dyz();
    }

    @C4034yP
    @C5566aOg
    @C2499fr
    public void dyC() {
        switch (bFf().mo6893i(iVF)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, iVF, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, iVF, new Object[0]));
                break;
        }
        dyB();
    }

    public void dyF() {
        switch (bFf().mo6893i(iVH)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, iVH, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, iVH, new Object[0]));
                break;
        }
        dyE();
    }

    @C5566aOg
    public void dyH() {
        switch (bFf().mo6893i(iVI)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, iVI, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, iVI, new Object[0]));
                break;
        }
        dyG();
    }

    @C5566aOg
    @C2499fr
    public void dyJ() {
        switch (bFf().mo6893i(iVJ)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, iVJ, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, iVJ, new Object[0]));
                break;
        }
        dyI();
    }

    public OniLogger dyb() {
        switch (bFf().mo6893i(iVc)) {
            case 0:
                return null;
            case 2:
                return (OniLogger) bFf().mo5606d(new aCE(this, iVc, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, iVc, new Object[0]));
                break;
        }
        return dya();
    }

    public boolean dyd() {
        switch (bFf().mo6893i(iVd)) {
            case 0:
                return false;
            case 2:
                return ((Boolean) bFf().mo5606d(new aCE(this, iVd, new Object[0]))).booleanValue();
            case 3:
                bFf().mo5606d(new aCE(this, iVd, new Object[0]));
                break;
        }
        return dyc();
    }

    public boolean dyf() {
        switch (bFf().mo6893i(iVf)) {
            case 0:
                return false;
            case 2:
                return ((Boolean) bFf().mo5606d(new aCE(this, iVf, new Object[0]))).booleanValue();
            case 3:
                bFf().mo5606d(new aCE(this, iVf, new Object[0]));
                break;
        }
        return dye();
    }

    @C5566aOg
    public void dyh() {
        switch (bFf().mo6893i(iVk)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, iVk, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, iVk, new Object[0]));
                break;
        }
        dyg();
    }

    /* access modifiers changed from: protected */
    public Player dyj() {
        switch (bFf().mo6893i(iVl)) {
            case 0:
                return null;
            case 2:
                return (Player) bFf().mo5606d(new aCE(this, iVl, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, iVl, new Object[0]));
                break;
        }
        return dyi();
    }

    public PlayerAssistant dyl() {
        switch (bFf().mo6893i(iVo)) {
            case 0:
                return null;
            case 2:
                return (PlayerAssistant) bFf().mo5606d(new aCE(this, iVo, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, iVo, new Object[0]));
                break;
        }
        return dyk();
    }

    @C5566aOg
    public List<Item> dyr() {
        switch (bFf().mo6893i(iVu)) {
            case 0:
                return null;
            case 2:
                return (List) bFf().mo5606d(new aCE(this, iVu, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, iVu, new Object[0]));
                break;
        }
        return dyq();
    }

    @C4034yP
    @C5566aOg
    @C2499fr
    public void dyt() {
        switch (bFf().mo6893i(iVw)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, iVw, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, iVw, new Object[0]));
                break;
        }
        dys();
    }

    @C5566aOg
    @C2499fr
    public void dyy() {
        switch (bFf().mo6893i(iVC)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, iVC, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, iVC, new Object[0]));
                break;
        }
        dyx();
    }

    /* access modifiers changed from: protected */
    @C5566aOg
    /* renamed from: e */
    public void mo14417e(Party gt) {
        switch (bFf().mo6893i(iTP)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, iTP, new Object[]{gt}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, iTP, new Object[]{gt}));
                break;
        }
        m23303d(gt);
    }

    @C5566aOg
    /* renamed from: e */
    public void mo14418e(C2546ge geVar) {
        switch (bFf().mo6893i(iUZ)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, iUZ, new Object[]{geVar}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, iUZ, new Object[]{geVar}));
                break;
        }
        m23308d(geVar);
    }

    @C0401Fa
    /* renamed from: f */
    public void mo14419f(C1506WA wa) {
        switch (bFf().mo6893i(iVn)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, iVn, new Object[]{wa}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, iVn, new Object[]{wa}));
                break;
        }
        m23315e(wa);
    }

    @C5566aOg
    @C2499fr
    /* renamed from: f */
    public void mo14420f(aMU amu) {
        switch (bFf().mo6893i(iVA)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, iVA, new Object[]{amu}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, iVA, new Object[]{amu}));
                break;
        }
        m23317e(amu);
    }

    @C5566aOg
    /* renamed from: g */
    public void mo14421g(Loot ael) {
        switch (bFf().mo6893i(iUW)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, iUW, new Object[]{ael}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, iUW, new Object[]{ael}));
                break;
        }
        m23321f(ael);
    }

    public String getName() {
        switch (bFf().mo6893i(f4778Pf)) {
            case 0:
                return null;
            case 2:
                return (String) bFf().mo5606d(new aCE(this, f4778Pf, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f4778Pf, new Object[0]));
                break;
        }
        return m23363uV();
    }

    @C5566aOg
    @C2499fr
    public void setName(String str) {
        switch (bFf().mo6893i(f4779Pg)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f4779Pg, new Object[]{str}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f4779Pg, new Object[]{str}));
                break;
        }
        m23264aq(str);
    }

    @C4034yP
    @C5566aOg
    @C2499fr
    /* renamed from: h */
    public void mo14422h(Player aku, boolean z) {
        switch (bFf().mo6893i(iTX)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, iTX, new Object[]{aku, new Boolean(z)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, iTX, new Object[]{aku, new Boolean(z)}));
                break;
        }
        m23326g(aku, z);
    }

    /* renamed from: hb */
    public /* bridge */ /* synthetic */ Controller mo12657hb() {
        switch (bFf().mo6893i(bpJ)) {
            case 0:
                return null;
            case 2:
                return (Controller) bFf().mo5606d(new aCE(this, bpJ, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, bpJ, new Object[0]));
                break;
        }
        return afJ();
    }

    public boolean isReady() {
        switch (bFf().mo6893i(iVG)) {
            case 0:
                return false;
            case 2:
                return ((Boolean) bFf().mo5606d(new aCE(this, iVG, new Object[0]))).booleanValue();
            case 3:
                bFf().mo5606d(new aCE(this, iVG, new Object[0]));
                break;
        }
        return dyD();
    }

    public boolean isUnnamed() {
        switch (bFf().mo6893i(iVz)) {
            case 0:
                return false;
            case 2:
                return ((Boolean) bFf().mo5606d(new aCE(this, iVz, new Object[0]))).booleanValue();
            case 3:
                bFf().mo5606d(new aCE(this, iVz, new Object[0]));
                break;
        }
        return dyu();
    }

    @C5566aOg
    /* renamed from: j */
    public void mo14425j(Player aku, boolean z) {
        switch (bFf().mo6893i(iUb)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, iUb, new Object[]{aku, new Boolean(z)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, iUb, new Object[]{aku, new Boolean(z)}));
                break;
        }
        m23327i(aku, z);
    }

    /* renamed from: j */
    public void mo14426j(Hangar kRVar) {
        switch (bFf().mo6893i(iUI)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, iUI, new Object[]{kRVar}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, iUI, new Object[]{kRVar}));
                break;
        }
        m23328i(kRVar);
    }

    /* renamed from: k */
    public long mo14427k(CitizenshipType adl) {
        switch (bFf().mo6893i(dXe)) {
            case 0:
                return 0;
            case 2:
                return ((Long) bFf().mo5606d(new aCE(this, dXe, new Object[]{adl}))).longValue();
            case 3:
                bFf().mo5606d(new aCE(this, dXe, new Object[]{adl}));
                break;
        }
        return m23329j(adl);
    }

    @C4034yP
    @C5566aOg
    @C2499fr
    /* renamed from: ko */
    public void mo14428ko(boolean z) {
        switch (bFf().mo6893i(iTT)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, iTT, new Object[]{new Boolean(z)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, iTT, new Object[]{new Boolean(z)}));
                break;
        }
        m23336kn(z);
    }

    @C4034yP
    @C5566aOg
    @C2499fr
    /* renamed from: ks */
    public void mo14429ks(boolean z) {
        switch (bFf().mo6893i(iVg)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, iVg, new Object[]{new Boolean(z)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, iVg, new Object[]{new Boolean(z)}));
                break;
        }
        m23339kr(z);
    }

    /* renamed from: l */
    public void mo14430l(PlayerController tVar) {
        switch (bFf().mo6893i(iTJ)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, iTJ, new Object[]{tVar}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, iTJ, new Object[]{tVar}));
                break;
        }
        m23332k(tVar);
    }

    public void logError(String str) {
        switch (bFf().mo6893i(iVe)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, iVe, new Object[]{str}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, iVe, new Object[]{str}));
                break;
        }
        m23353nM(str);
    }

    @C5566aOg
    @C2499fr
    /* renamed from: me */
    public boolean mo14432me(long j) {
        switch (bFf().mo6893i(iVy)) {
            case 0:
                return false;
            case 2:
                return ((Boolean) bFf().mo5606d(new aCE(this, iVy, new Object[]{new Long(j)}))).booleanValue();
            case 3:
                bFf().mo5606d(new aCE(this, iVy, new Object[]{new Long(j)}));
                break;
        }
        return m23348md(j);
    }

    @C5566aOg
    @C2499fr
    /* renamed from: n */
    public void mo11676n(Faction xm) {
        switch (bFf().mo6893i(cow)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, cow, new Object[]{xm}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, cow, new Object[]{xm}));
                break;
        }
        m23343m(xm);
    }

    @C4034yP
    @C5566aOg
    @C2499fr
    /* renamed from: n */
    public void mo14433n(Player aku, boolean z) {
        switch (bFf().mo6893i(iUj)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, iUj, new Object[]{aku, new Boolean(z)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, iUj, new Object[]{aku, new Boolean(z)}));
                break;
        }
        m23344m(aku, z);
    }

    /* renamed from: n */
    public void mo14434n(Storage qzVar) {
        switch (bFf().mo6893i(iTN)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, iTN, new Object[]{qzVar}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, iTN, new Object[]{qzVar}));
                break;
        }
        m23345m(qzVar);
    }

    @C4034yP
    @C5566aOg
    @C2499fr
    /* renamed from: nF */
    public void mo14435nF(String str) {
        switch (bFf().mo6893i(iTE)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, iTE, new Object[]{str}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, iTE, new Object[]{str}));
                break;
        }
        m23351nJ(str);
    }

    /* renamed from: nL */
    public boolean mo14436nL(String str) {
        switch (bFf().mo6893i(iUr)) {
            case 0:
                return false;
            case 2:
                return ((Boolean) bFf().mo5606d(new aCE(this, iUr, new Object[]{str}))).booleanValue();
            case 3:
                bFf().mo5606d(new aCE(this, iUr, new Object[]{str}));
                break;
        }
        return m23352nK(str);
    }

    @C5566aOg
    /* renamed from: p */
    public void mo14437p(Corporation aPVar) {
        switch (bFf().mo6893i(iUC)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, iUC, new Object[]{aPVar}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, iUC, new Object[]{aPVar}));
                break;
        }
        m23354o(aPVar);
    }

    @C4034yP
    @C5566aOg
    @C2499fr
    /* renamed from: p */
    public void mo14438p(Player aku, boolean z) {
        switch (bFf().mo6893i(iUo)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, iUo, new Object[]{aku, new Boolean(z)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, iUo, new Object[]{aku, new Boolean(z)}));
                break;
        }
        m23355o(aku, z);
    }

    @C5566aOg
    @C2499fr
    @Deprecated
    /* renamed from: qV */
    public void mo12661qV() {
        switch (bFf().mo6893i(f4777Lm)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f4777Lm, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f4777Lm, new Object[0]));
                break;
        }
        m23358qU();
    }

    @C4034yP
    @C5566aOg
    @C2499fr
    /* renamed from: r */
    public void mo14439r(Player aku, boolean z) {
        switch (bFf().mo6893i(iUt)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, iUt, new Object[]{aku, new Boolean(z)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, iUt, new Object[]{aku, new Boolean(z)}));
                break;
        }
        m23357q(aku, z);
    }

    @C5566aOg
    public void setLoading(boolean z) {
        switch (bFf().mo6893i(iVE)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, iVE, new Object[]{new Boolean(z)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, iVE, new Object[]{new Boolean(z)}));
                break;
        }
        m23340kt(z);
    }

    @C5566aOg
    @C2499fr
    /* renamed from: t */
    public void mo14442t(Player aku, boolean z) {
        switch (bFf().mo6893i(iUu)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, iUu, new Object[]{aku, new Boolean(z)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, iUu, new Object[]{aku, new Boolean(z)}));
                break;
        }
        m23360s(aku, z);
    }

    public String toString() {
        switch (bFf().mo6893i(_f_toString_0020_0028_0029Ljava_002flang_002fString_003b)) {
            case 0:
                return null;
            case 2:
                return (String) bFf().mo5606d(new aCE(this, _f_toString_0020_0028_0029Ljava_002flang_002fString_003b, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, _f_toString_0020_0028_0029Ljava_002flang_002fString_003b, new Object[0]));
                break;
        }
        return m23266au();
    }

    /* renamed from: w */
    public void mo14444w(User adk) {
        switch (bFf().mo6893i(fay)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, fay, new Object[]{adk}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, fay, new Object[]{adk}));
                break;
        }
        m23364v(adk);
    }

    @C5566aOg
    /* renamed from: xy */
    public void mo14445xy(int i) {
        switch (bFf().mo6893i(hzp)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, hzp, new Object[]{new Integer(i)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, hzp, new Object[]{new Integer(i)}));
                break;
        }
        m23366xx(i);
    }

    /* access modifiers changed from: protected */
    /* renamed from: S */
    public void mo10S() {
        super.mo10S();
    }

    @C0064Am(aul = "b62f409e3afdeacf5473f0ccacb876c5", aum = 0)
    /* renamed from: b */
    private void m23272b(PlayerSocialController qhVar) {
        m23255a(qhVar);
    }

    @C0064Am(aul = "2d7b1b1a24a6633edd2f094872ee3d5c", aum = 0)
    private PlayerSocialController dwT() {
        if (dwI() == null && bHa()) {
            PlayerSocialController qhVar = (PlayerSocialController) bFf().mo6865M(PlayerSocialController.class);
            qhVar.mo10S();
            m23255a(qhVar);
        }
        return dwI();
    }

    /* renamed from: u */
    public void mo14443u(User adk) {
        super.mo10S();
        m23335km(true);
        m23319e("Player" + bFf().getObjectId().getId(), adk);
    }

    /* renamed from: c */
    public void mo14360c(String str, User adk) {
        super.mo10S();
        if (!adk.cEb()) {
            C3004mr.m35861a(str, ala());
        }
        m23335km(false);
        m23319e(str, adk);
    }

    @C0064Am(aul = "ecea673557ce93622b47014da38f1146", aum = 0)
    /* renamed from: d */
    private void m23309d(String str, User adk) {
        mo14444w(adk);
        m23350nI(str);
        BankAccount ado = (BankAccount) bFf().mo6865M(BankAccount.class);
        ado.mo10S();
        m23325g(ado);
        PlayerController tVar = (PlayerController) bFf().mo6865M(PlayerController.class);
        tVar.mo10S();
        mo12649c((Controller) tVar);
        dxc().mo22156p(this);
        PlayerSocialController qhVar = (PlayerSocialController) bFf().mo6865M(PlayerSocialController.class);
        qhVar.mo10S();
        m23282c(qhVar);
        dwU().mo21453p(this);
        LDScriptingController lYVar = (LDScriptingController) bFf().mo6865M(LDScriptingController.class);
        lYVar.mo10S();
        mo12654e(lYVar);
        bQG().mo20251Y(this);
        Associates adq = (Associates) bFf().mo6865M(Associates.class);
        adq.mo8506b(this);
        m23316e(adq);
        OniLogger aex = (OniLogger) bFf().mo6865M(OniLogger.class);
        aex.mo8699b(this);
        m23248a(aex);
        PlayerAssistant avw = (PlayerAssistant) bFf().mo6865M(PlayerAssistant.class);
        avw.mo11962b(this);
        m23249a(avw);
        PlayerSessionLog al = (PlayerSessionLog) bFf().mo6865M(PlayerSessionLog.class);
        al.mo201b(this);
        m23313e(al);
        m23307d(aMU.CREATED_PLAYER);
        ItemsDurabilityTask aVar = (ItemsDurabilityTask) bFf().mo6865M(ItemsDurabilityTask.class);
        aVar.mo14446a(this, (aDJ) this);
        m23250a(aVar);
    }

    @C0064Am(aul = "4cc604b3f6dfeef7fb268a9b5f656921", aum = 0)
    /* renamed from: au */
    private String m23266au() {
        StringBuffer stringBuffer = new StringBuffer();
        stringBuffer.append("Player: [");
        stringBuffer.append(getName());
        stringBuffer.append("]");
        return stringBuffer.toString();
    }

    @C0064Am(aul = "22c8fc4f05eee7c0dc3830966b37f8ac", aum = 0)
    /* renamed from: uV */
    private String m23363uV() {
        return m23362uU();
    }

    @C0064Am(aul = "47352a03f94c1cb5fd151900f7cc1935", aum = 0)
    @ClientOnly
    private String ahP() {
        return m23362uU();
    }

    @C0064Am(aul = "249d605dabee47972271c4a002a981d4", aum = 0)
    private boolean dwV() {
        return dxS().cfy() == 1;
    }

    @C0064Am(aul = "8f2705392e2fff0a2b6384a97087a224", aum = 0)
    @ClientOnly
    /* renamed from: QV */
    private boolean m23234QV() {
        Station bf;
        if (super.mo961QW()) {
            return true;
        }
        if ((bhE() instanceof Station) && (bf = (Station) bhE()) != null) {
            if (C1298TD.m9830t(bf).bFT()) {
                return true;
            }
            if (C1298TD.m9830t(bf.mo960Nc()).bFT()) {
                return true;
            }
            if (C1298TD.m9830t(bf.mo965Zc()).bFT()) {
                return true;
            }
            for (Hangar t : dxG()) {
                if (C1298TD.m9830t(t).bFT()) {
                    return true;
                }
            }
        }
        if (dyb() != null && C1298TD.m9830t(dyb()).bFT()) {
            return true;
        }
        if (dyl() != null && C1298TD.m9830t(dyl()).bFT()) {
            return true;
        }
        if (cXk() != null && C1298TD.m9830t(cXk()).bFT()) {
            return true;
        }
        if (dxS() != null && C1298TD.m9830t(dxS()).bFT()) {
            return true;
        }
        if (dxu() != null && C1298TD.m9830t(dxu()).bFT()) {
            return true;
        }
        if (mo5156Qw() != null && C1298TD.m9830t(mo5156Qw()).bFT()) {
            return true;
        }
        if (dxZ() == null || !C1298TD.m9830t(dxZ()).bFT()) {
            return false;
        }
        return true;
    }

    @C0064Am(aul = "c738dc6439d8de92313b111aaceb6922", aum = 0)
    private IEngineGame aMu() {
        return ala().ald();
    }

    @C0064Am(aul = "21a00cadd94251b19b13bb6e9a94e8da", aum = 0)
    private boolean bQA() {
        return bhE() instanceof Station;
    }

    @C0064Am(aul = "2527e93c0bfa824108eac936889b2016", aum = 0)
    private PlayerController dxb() {
        return (PlayerController) super.mo12657hb();
    }

    @C0064Am(aul = "1426d3b6453e191c74e2804c2267d620", aum = 0)
    /* renamed from: k */
    private void m23332k(PlayerController tVar) {
        mo12649c((Controller) tVar);
    }

    @C0064Am(aul = "69d273feac0dd2021408b5380fc6c331", aum = 0)
    /* renamed from: Qv */
    private BankAccount m23235Qv() {
        return dwD();
    }

    @C0064Am(aul = "c4708d73260e36e6e3032fd66866a537", aum = 0)
    /* renamed from: f */
    private void m23322f(BankAccount ado) {
        m23318e(ado);
    }

    @C0064Am(aul = "402345e58acbd923cf7018d0f1a9b5ea", aum = 0)
    /* renamed from: cu */
    private void m23301cu(long j) {
        dwM().mo211hF(j);
    }

    @C0064Am(aul = "14164b90348f90351603d7decbe87bc2", aum = 0)
    /* renamed from: cw */
    private void m23302cw(long j) {
        dwM().mo212hH(j);
    }

    @C0064Am(aul = "d258cc41464f6b5c12da960ff0bfb31c", aum = 0)
    private Station dxd() {
        return dwy().djk();
    }

    @C0064Am(aul = "b326f8dc98862c9d4fb518d0e16482d6", aum = 0)
    private User bOw() {
        return bOn();
    }

    @C0064Am(aul = "335c39e072ba6429577c2d29be599c4a", aum = 0)
    /* renamed from: v */
    private void m23364v(User adk) {
        m23361t(adk);
    }

    @C0064Am(aul = "585b5d6a426208043e4e881315336dcd", aum = 0)
    private List<Storage> dxf() {
        return dwA();
    }

    @C0064Am(aul = "892c26ab019ea2735273abe22c40ec31", aum = 0)
    /* renamed from: m */
    private void m23345m(Storage qzVar) {
        dwA().add(qzVar);
    }

    @C0064Am(aul = "04510a4ded24c210f482670a5992e8c7", aum = 0)
    private Party dxh() {
        return dww();
    }

    @C0064Am(aul = "7ea5b9c5790aade849e7701fb24d78a4", aum = 0)
    private boolean dxj() {
        return dww() != null && !dww().isDisposed();
    }

    @C0064Am(aul = "085f66f34be042275e3894f8ebdd0275", aum = 0)
    /* renamed from: cM */
    private boolean m23291cM(Player aku) {
        if (!dxk()) {
            return false;
        }
        return dww().aQS().contains(aku);
    }

    @C4034yP
    @C0064Am(aul = "a3990da4eaef461d38d6a6200501e37d", aum = 0)
    @C2499fr
    @C1253SX
    /* renamed from: k */
    private void m23331k(Player aku, boolean z) {
        Actor bhE = bhE();
        if (bhE instanceof Station) {
            if (aku.bhE() != bhE) {
                m23270b(aku, C1191Rb.C1192a.NOT_IN_SAME_NODE, z);
                return;
            }
        } else if (bhE instanceof Ship) {
            if (!(aku.bhE() instanceof Ship)) {
                m23270b(aku, C1191Rb.C1192a.NOT_IN_SAME_NODE, z);
                return;
            }
            Ship fAVar = (Ship) bhE;
            Ship fAVar2 = (Ship) aku.bhE();
            if (fAVar.azW() != fAVar2.azW()) {
                m23270b(aku, C1191Rb.C1192a.NOT_IN_SAME_NODE, z);
                return;
            }
            float xD = ala().aJe().mo19005xD();
            if (xD == 0.0f) {
                xD = 1000.0f;
            }
            if (fAVar.mo1011bv((Actor) fAVar2) > xD) {
                m23270b(aku, C1191Rb.C1192a.PLAYER_TOO_FAR, z);
                return;
            }
        }
        if (z) {
            m23295cT(aku);
        } else {
            m23297cV(aku);
        }
    }

    @C0064Am(aul = "98db9516eff5154a819898e0807df75f", aum = 0)
    private boolean dxl() {
        return bRA() != null;
    }

    @C0064Am(aul = "cd3f413922cb6eb57e1b3fdcac53fb95", aum = 0)
    private Associates dxt() {
        return dwx();
    }

    @C0064Am(aul = "38ad1a09ea85ff6882ae408a55d24321", aum = 0)
    /* renamed from: cZ */
    private boolean m23300cZ(Player aku) {
        return dxu().mo8514ct(aku);
    }

    @C0064Am(aul = "bf2030b242719fa4d660ec4df91009b8", aum = 0)
    /* renamed from: nK */
    private boolean m23352nK(String str) {
        return dxu().mo8517lW(str);
    }

    @C0064Am(aul = "ea3066ce03b542b30c6421056ecd54d1", aum = 0)
    private PlayerView dxx() {
        return (PlayerView) bQO();
    }

    @C0064Am(aul = "11fb44696b28ea3078f1e0ab614da94a", aum = 0)
    private CorporationCreationStatus dxB() {
        return dwE();
    }

    @C0064Am(aul = "c17a74fc0a9c5ea56d21025b1c5a81c0", aum = 0)
    private boolean dxD() {
        return bXM() != null;
    }

    @C0064Am(aul = "12a9ca0c0c6023ab1f8046de27630e14", aum = 0)
    private Corporation bYc() {
        return bXM();
    }

    @C0064Am(aul = "678deec37501da696766e67f9c924d91", aum = 0)
    private C3438ra<Hangar> dxF() {
        return dwz();
    }

    @C0064Am(aul = "84ee63320f0f8a77c04e291ac1da8b62", aum = 0)
    /* renamed from: aC */
    private Hangar m23259aC(Station bf) {
        if (bf == null) {
            logger.error("**** PLAYER IS TRYING TO GET HANGAR AT NULL STATION", new Exception());
        }
        for (Hangar kRVar : dxG()) {
            if (kRVar.mo20066eT() == bf) {
                return kRVar;
            }
        }
        return m23261aF(bf);
    }

    @C0064Am(aul = "53311962d7988ca667aec72ea09012a2", aum = 0)
    /* renamed from: i */
    private void m23328i(Hangar kRVar) {
        dwz().add(kRVar);
    }

    @C0064Am(aul = "62ad2ffbf3f4b4d293a41c3755c5bbd4", aum = 0)
    /* renamed from: ax */
    private void m23267ax(Ship fAVar) {
        Character agj = fAVar.agj();
        if (agj instanceof Player) {
            dwM().cfk();
        } else if (agj instanceof NPC) {
            dwM().cfm();
        } else {
            throw new IllegalArgumentException();
        }
    }

    @C0064Am(aul = "911316255671b48134e9e130f9ae4a1a", aum = 0)
    /* renamed from: az */
    private void m23268az(Ship fAVar) {
        Character agj = fAVar.agj();
        if (agj instanceof Player) {
            dwM().cfo();
        } else if (agj instanceof NPC) {
            dwM().cfq();
        } else {
            mo8358lY("Trying to log death of player '" + getName() + " (" + cWm() + ")' by unknown killer pilot: " + (agj == null ? "null" : agj.bFY()) + ". Is killer ship disposed? " + fAVar.isDisposed());
        }
    }

    @C0064Am(aul = "8782f08bc40de1dd5dc1d09cb817f34a", aum = 0)
    private void dxP() {
        dwM().aAm();
    }

    @C0064Am(aul = "144a21ac6a5c6e69bfcbc71a341e4c5f", aum = 0)
    private String cff() {
        return dwM().cfg();
    }

    @C0064Am(aul = "a7083cc470d58bda6cf368852649bf0b", aum = 0)
    private PlayerSessionLog dxR() {
        return dwM();
    }

    @C0064Am(aul = "9e9c404d141f635b6aa4c3f40a06c85c", aum = 0)
    /* renamed from: a */
    private void m23258a(List<C5820abA> list, Station bf, Ship fAVar) {
        Iterator<Item> iterator = fAVar.afI().getIterator();
        while (iterator.hasNext()) {
            Item next = iterator.next();
            list.add(new C5820abA(bf, fAVar, next));
            if (next instanceof ShipItem) {
                m23275b(list, bf, ((ShipItem) next).mo11127al());
            }
        }
        for (ShipStructure bVG : fAVar.agt()) {
            for (ShipSector iterator2 : bVG.bVG()) {
                Iterator<Item> iterator3 = iterator2.getIterator();
                while (iterator3.hasNext()) {
                    Item next2 = iterator3.next();
                    list.add(new C5820abA(bf, fAVar, next2));
                    if (next2 instanceof ShipItem) {
                        m23275b(list, bf, ((ShipItem) next2).mo11127al());
                    }
                }
            }
        }
    }

    @C0064Am(aul = "c1f2c56c5ad1f0900e9c9110aa027ee3", aum = 0)
    private Loot dxX() {
        return aPh();
    }

    @C0064Am(aul = "8ca6dd39b126b4cf8be5e7a149ab1616", aum = 0)
    private Faction azM() {
        if (azq() == null) {
            return ala().aJe().mo19011xf();
        }
        return azq();
    }

    @C0064Am(aul = "fc9c69c1876ebb33a5735814d39efacf", aum = 0)
    /* renamed from: b */
    private boolean m23276b(C2546ge geVar) {
        BlockStatus ahu = (BlockStatus) dwL().get(geVar);
        return ahu != null && ahu.isEnabled();
    }

    @C0064Am(aul = "6b0a7428f86b4ba31d194c2e2ccb48bd", aum = 0)
    private CloningInfo dxY() {
        return dwy();
    }

    @C0064Am(aul = "5481934baf9ecb5a7a17586e9bcf0f9a", aum = 0)
    private OniLogger dya() {
        return dwF();
    }

    @C0064Am(aul = "1db63204a9d240436b1c268a998b096b", aum = 0)
    private boolean dyc() {
        if (ala().aJO().contains(bOx()) && bOx().cSQ().contains(this)) {
            return true;
        }
        return false;
    }

    @C0064Am(aul = "c8a29cef39a3857055a3bd30654ff3ef", aum = 0)
    /* renamed from: nM */
    private void m23353nM(String str) {
        super.mo8358lY(str);
    }

    @C0064Am(aul = "1cebf3cff56a7d3353f4d2be029a3c2b", aum = 0)
    private boolean dye() {
        return dwJ();
    }

    @C0064Am(aul = "b62c7f05cb2e2ccd726220b60dee7d20", aum = 0)
    /* renamed from: bf */
    private void m23278bf(String str, String str2) {
        mo14419f((C1506WA) new C6018aeq(str, str2));
    }

    @C0401Fa
    @C0064Am(aul = "86fcd2e80667d6b50e838f545c87ecdc", aum = 0)
    /* renamed from: a */
    private void m23257a(Collection<C6950awp> collection, aDR adr) {
        Actor ha;
        mo8366w(adr);
        try {
            for (C6950awp next : collection) {
                if ((next instanceof C4031yM) && (ha = ((C4031yM) next).mo23123ha()) != null && !adr.mo8406s(ha)) {
                    ha.mo656qV();
                }
            }
        } finally {
            cVm();
        }
    }

    @C0064Am(aul = "a9594230cdf62e928a778f18c8f7b532", aum = 0)
    private int cTd() {
        return cSF();
    }

    @C0064Am(aul = "de80ff8c1636d4a1e4369e042f03618f", aum = 0)
    private long cMJ() {
        if (bQx() == null || bQx().ahO() == null) {
            return 0;
        }
        return bQx().crP();
    }

    @C0064Am(aul = "87410b4e4ef90e30610a3846cd4991d1", aum = 0)
    private Player dyi() {
        return this;
    }

    @C0401Fa
    @C0064Am(aul = "a3b67813216eb1c84d1c83fdd2f77f35", aum = 0)
    /* renamed from: z */
    private void m23367z(String str, Object obj) {
        if (bGX() && getPlayer() == this) {
            ald().getEventManager().mo13972d(str, obj);
        } else if (!bGX() || getPlayer() == this) {
            if (bGY()) {
                mo6317hy("trying to post a client event from offload node, not implemented yet!!!!!!");
                return;
            }
            try {
                if (dyj().dxa() && dyj().bOx() != null && dyj().bOx().cSV() != null && dyj().bOx().cSV().bgt() != null) {
                    UserConnection cSV = dyj().bOx().cSV();
                    C5694aTe ate = new C5694aTe(str, obj);
                    aWq dDA = aWq.dDA();
                    if (dDA != null) {
                        aDR f = bFf().mo6866PM().bGJ().mo6442f(cSV.bgt());
                        if (f != null) {
                            dDA.mo12048a(f, (Blocking) ate);
                            return;
                        }
                        return;
                    }
                    bFf().mo6866PM().bGU().mo15549b(cSV.bgt(), (Blocking) ate);
                }
            } catch (Exception e) {
                logger.error("Processing event: " + obj, e);
            }
        } else if (bGX() && C3582se.class.getName().endsWith(".Infra")) {
            logger.error("Trying to post a client event for " + this + " on " + getPlayer() + " - event: " + obj + " / " + str, new IllegalAccessException("Illegal call from " + bFY()));
        }
    }

    @C0401Fa
    @C0064Am(aul = "b4ff6a106632247ff7aa765caf7ed385", aum = 0)
    /* renamed from: e */
    private void m23315e(C1506WA wa) {
        mo14338A("", wa);
    }

    @C0064Am(aul = "91f1aff3b0bdb1c1d4be98b663baf086", aum = 0)
    private PlayerAssistant dyk() {
        return dwG();
    }

    @C0064Am(aul = "8e000364eccfb71c48897640d6f450b2", aum = 0)
    private CitizenshipControl cXj() {
        if (dwH() == null) {
            dyn();
        }
        return dwH();
    }

    @C0064Am(aul = "0cfd56c1d932ba97d6fe0c6ea14f48b9", aum = 0)
    /* renamed from: a */
    private void m23243a(Station bf, ShipType ng, List<WeaponType> list, MissionTemplate avh) {
        mo14347aH(bf);
        if (avh != null) {
            try {
                bQG().mo20278e(avh);
            } catch (C3243pZ e) {
                logger.warn("Problems while preparing the starting mission for player " + getName(), e);
            }
        }
        if (ng != null) {
            try {
                ala().mo1437b(this, ng, list);
            } catch (C2293dh e2) {
                logger.warn("Problems while preparing the initial ship for player " + getName(), e2);
            }
        }
    }

    @C0064Am(aul = "c288144cc08d22a6f39a3c0cf68bc2e1", aum = 0)
    /* renamed from: j */
    private long m23329j(CitizenshipType adl) {
        return cXk().mo4985k(adl);
    }

    @C0064Am(aul = "7dc13c03ca631a9ca69d63216fab4a2d", aum = 0)
    private boolean dyu() {
        return dwO();
    }

    @C0064Am(aul = "1ff89df79b3184f331b8b2587deaa728", aum = 0)
    private aMU cXl() {
        return dwP();
    }

    @C0064Am(aul = "42106b0fc96518a3696adfad00152cf6", aum = 0)
    private void dyv() {
        HashSet hashSet = new HashSet();
        for (C4033yO next : dwG().dBQ().keySet()) {
            if (next instanceof DatabaseCategory) {
                hashSet.add((DatabaseCategory) next);
            }
        }
        for (DatabaseCategory next2 : ala().aLC()) {
            if (!hashSet.contains(next2)) {
                dwG().mo11981e((C4068yr) next2);
            }
        }
        HashSet hashSet2 = new HashSet();
        for (C4033yO next3 : dwG().dBS().keySet()) {
            if (next3 instanceof DatabaseCategory) {
                hashSet2.add(next3);
            }
        }
        for (TaikopediaEntry next4 : ala().aJe().mo19020xx().cJx()) {
            if (!hashSet2.contains(next4)) {
                dwG().mo11985l(next4);
            }
        }
    }

    @C0064Am(aul = "7d51bbb46ccb703039458cf7425effb0", aum = 0)
    private boolean dyz() {
        return dwK();
    }

    @C0064Am(aul = "7939d73685be02a09673cc1c5d7bc44e", aum = 0)
    private boolean dyD() {
        return (!dwK() && cVr() - dwS() > 5000) || cVr() - dwR() > 60000;
    }

    @C0064Am(aul = "fb03df933ee8f051bf8fda4bdcd07089", aum = 0)
    private void dyE() {
        push();
        if (bQI() != null) {
            bQI().mo2034qV();
        }
    }

    @C6485anp
    @C5511aMd
    /* renamed from: a.akU$b */
    /* compiled from: a */
    public class PlayerTimeoutTask extends TaskletImpl implements C1616Xf {

        /* renamed from: JD */
        public static final C2491fm f4785JD = null;
        public static final int _m_fieldCount = 0;
        public static final C5663aRz[] _m_fields = null;
        public static final int _m_methodCount = 0;
        public static final C2491fm[] _m_methods = null;
        /* renamed from: aT */
        public static final C5663aRz f4786aT = null;
        public static final long serialVersionUID = 0;
        public static C6494any ___iScriptClass;
        @C0064Am(aul = "98899f8c5e44ac29d6c017bc5667c3b9", aum = 0)
        static /* synthetic */ Player fUq;

        static {
            m23450V();
        }

        public PlayerTimeoutTask() {
            super((C5540aNg) null);
            super.mo10S();
        }

        public PlayerTimeoutTask(C5540aNg ang) {
            super(ang);
        }

        public PlayerTimeoutTask(Player aku, aDJ adj) {
            super((C5540aNg) null);
            super._m_script_init(aku, adj);
        }

        /* renamed from: V */
        static void m23450V() {
            serialVersionUID = (long) 1;
            _m_fieldCount = TaskletImpl._m_fieldCount + 1;
            _m_methodCount = TaskletImpl._m_methodCount + 1;
            int i = TaskletImpl._m_fieldCount;
            C5663aRz[] arzArr = new C5663aRz[(i + 1)];
            C5663aRz b = C5640aRc.m17844b(PlayerTimeoutTask.class, "98899f8c5e44ac29d6c017bc5667c3b9", i);
            f4786aT = b;
            arzArr[i] = b;
            int i2 = i + 1;
            _m_fields = arzArr;
            C1634Xv.m11725a((Object[]) TaskletImpl._m_fields, (Object[]) _m_fields);
            int i3 = TaskletImpl._m_methodCount;
            C2491fm[] fmVarArr = new C2491fm[(i3 + 1)];
            C2491fm a = C4105zY.m41624a(PlayerTimeoutTask.class, "dca0f8c9f5fab0af945def06a55496d9", i3);
            f4785JD = a;
            fmVarArr[i3] = a;
            int i4 = i3 + 1;
            _m_methods = fmVarArr;
            C1634Xv.m11725a((Object[]) TaskletImpl._m_methods, (Object[]) _m_methods);
            ___iScriptClass = aUO.m18566a(PlayerTimeoutTask.class, C6300akM.class, _m_fields, _m_methods);
        }

        /* renamed from: bB */
        private void m23451bB(Player aku) {
            bFf().mo5608dq().mo3197f(f4786aT, aku);
        }

        private Player chB() {
            return (Player) bFf().mo5608dq().mo3214p(f4786aT);
        }

        /* renamed from: S */
        public void mo10S() {
            super.mo10S();
        }

        /* renamed from: T */
        public C6494any mo11T() {
            return ___iScriptClass;
        }

        /* renamed from: U */
        public C2491fm[] mo12U() {
            return _m_methods;
        }

        /* renamed from: W */
        public Object mo13W() {
            return new C6300akM(this);
        }

        /* renamed from: a */
        public Object mo14a(C0495Gr gr) {
            gr.getArgs();
            switch (gr.mo2417hq() - TaskletImpl._m_methodCount) {
                case 0:
                    m23452pi();
                    return null;
                default:
                    return super.mo14a(gr);
            }
        }

        /* renamed from: a */
        public void mo15a(Collection collection, C0495Gr gr) {
            super.mo15a(collection, gr);
        }

        /* renamed from: c */
        public C5663aRz[] mo25c() {
            return _m_fields;
        }

        /* access modifiers changed from: protected */
        /* renamed from: pj */
        public void mo667pj() {
            switch (bFf().mo6893i(f4785JD)) {
                case 0:
                    return;
                case 2:
                    bFf().mo5606d(new aCE(this, f4785JD, new Object[0]));
                    return;
                case 3:
                    bFf().mo5606d(new aCE(this, f4785JD, new Object[0]));
                    break;
            }
            m23452pi();
        }

        /* renamed from: a */
        public void mo14448a(Player aku, aDJ adj) {
            m23451bB(aku);
            super.mo4706l(adj);
        }

        @C0064Am(aul = "dca0f8c9f5fab0af945def06a55496d9", aum = 0)
        /* renamed from: pi */
        private void m23452pi() {
            chB().dxM();
        }
    }

    @C6485anp
    @C5511aMd
    /* renamed from: a.akU$a */
    public class ItemsDurabilityTask extends TaskletImpl implements C1616Xf {

        /* renamed from: JD */
        public static final C2491fm f4783JD = null;
        public static final int _m_fieldCount = 0;
        public static final C5663aRz[] _m_fields = null;
        public static final int _m_methodCount = 0;
        public static final C2491fm[] _m_methods = null;
        /* renamed from: aT */
        public static final C5663aRz f4784aT = null;
        public static final C2491fm fUO = null;
        private static final long ONE_HOUR = 3600000;
        private static final long ONE_SECOND = 1000;

        public static C6494any ___iScriptClass = null;
        @C0064Am(aul = "377e92473e9c72f724eb8712fd9795e0", aum = 0)
        static /* synthetic */ Player fUq = null;

        static {
            m23438V();
        }

        public ItemsDurabilityTask() {
            super((C5540aNg) null);
            super.mo10S();
        }

        public ItemsDurabilityTask(C5540aNg ang) {
            super(ang);
        }

        public ItemsDurabilityTask(Player aku, aDJ adj) {
            super((C5540aNg) null);
            super._m_script_init(aku, adj);
        }

        /* renamed from: V */
        static void m23438V() {
            _m_fieldCount = TaskletImpl._m_fieldCount + 1;
            _m_methodCount = TaskletImpl._m_methodCount + 2;
            int i = TaskletImpl._m_fieldCount;
            C5663aRz[] arzArr = new C5663aRz[(i + 1)];
            C5663aRz b = C5640aRc.m17844b(ItemsDurabilityTask.class, "377e92473e9c72f724eb8712fd9795e0", i);
            f4784aT = b;
            arzArr[i] = b;
            int i2 = i + 1;
            _m_fields = arzArr;
            C1634Xv.m11725a((Object[]) TaskletImpl._m_fields, (Object[]) _m_fields);
            int i3 = TaskletImpl._m_methodCount;
            C2491fm[] fmVarArr = new C2491fm[(i3 + 2)];
            C2491fm a = C4105zY.m41624a(ItemsDurabilityTask.class, "7c61c7a8d769fa40ea3cdda1201bcfe4", i3);
            f4783JD = a;
            fmVarArr[i3] = a;
            int i4 = i3 + 1;
            C2491fm a2 = C4105zY.m41624a(ItemsDurabilityTask.class, "6a964b0b329e189f194237d688fb8e18", i4);
            fUO = a2;
            fmVarArr[i4] = a2;
            int i5 = i4 + 1;
            _m_methods = fmVarArr;
            C1634Xv.m11725a((Object[]) TaskletImpl._m_methods, (Object[]) _m_methods);
            ___iScriptClass = aUO.m18566a(ItemsDurabilityTask.class, C5490aLi.class, _m_fields, _m_methods);
        }

        /* renamed from: bB */
        private void m23439bB(Player aku) {
            bFf().mo5608dq().mo3197f(f4784aT, aku);
        }

        private Player chB() {
            return (Player) bFf().mo5608dq().mo3214p(f4784aT);
        }

        /* renamed from: S */
        public void mo10S() {
            super.mo10S();
        }

        /* renamed from: T */
        public C6494any mo11T() {
            return ___iScriptClass;
        }

        /* renamed from: U */
        public C2491fm[] mo12U() {
            return _m_methods;
        }

        /* renamed from: W */
        public Object mo13W() {
            return new C5490aLi(this);
        }

        /* renamed from: a */
        public Object mo14a(C0495Gr gr) {
            gr.getArgs();
            switch (gr.mo2417hq() - TaskletImpl._m_methodCount) {
                case 0:
                    m23440pi();
                    return null;
                case 1:
                    chC();
                    return null;
                default:
                    return super.mo14a(gr);
            }
        }

        /* renamed from: a */
        public void mo15a(Collection collection, C0495Gr gr) {
            super.mo15a(collection, gr);
        }

        /* renamed from: c */
        public C5663aRz[] mo25c() {
            return _m_fields;
        }

        public void chD() {
            switch (bFf().mo6893i(fUO)) {
                case 0:
                    return;
                case 2:
                    bFf().mo5606d(new aCE(this, fUO, new Object[0]));
                    return;
                case 3:
                    bFf().mo5606d(new aCE(this, fUO, new Object[0]));
                    break;
            }
            chC();
        }

        /* access modifiers changed from: protected */
        /* renamed from: pj */
        public void mo667pj() {
            switch (bFf().mo6893i(f4783JD)) {
                case 0:
                    return;
                case 2:
                    bFf().mo5606d(new aCE(this, f4783JD, new Object[0]));
                    return;
                case 3:
                    bFf().mo5606d(new aCE(this, f4783JD, new Object[0]));
                    break;
            }
            m23440pi();
        }

        /* renamed from: a */
        public void mo14446a(Player aku, aDJ adj) {
            m23439bB(aku);
            super.mo4706l(adj);
        }

        @C0064Am(aul = "7c61c7a8d769fa40ea3cdda1201bcfe4", aum = 0)
        /* renamed from: pi */
        private void m23440pi() {
            chB().dyp();
        }

        @C0064Am(aul = "6a964b0b329e189f194237d688fb8e18", aum = 0)
        private void chC() {
            chB().dyp();
            if (!chB().dwB().isEmpty() && !isActive()) {
                long ciT = ala().aJe().mo19016xp().ciT();
                if (ciT <= 0) {
                    ciT = 43200;
                }
                mo4704hk((float) ciT);
            }
        }
    }
}
