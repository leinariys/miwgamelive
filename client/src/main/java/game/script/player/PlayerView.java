package game.script.player;

import game.network.message.externalizable.aCE;
import game.network.message.serializable.C3438ra;
import game.script.Character;
import game.script.CharacterView;
import game.script.citizenship.CitizenshipType;
import game.script.corporation.CorporationLogo;
import logic.baa.C1616Xf;
import logic.baa.C1634Xv;
import logic.baa.C5511aMd;
import logic.baa.C6485anp;
import logic.data.mbean.C5584aOy;
import logic.res.code.C5640aRc;
import logic.res.code.C5663aRz;
import logic.res.code.C6494any;
import logic.res.html.C2491fm;
import p001a.*;

import java.util.Collection;

@C5511aMd
@C6485anp
/* renamed from: a.Xs */
/* compiled from: a */
public class PlayerView extends CharacterView implements C1616Xf {

    /* renamed from: Cx */
    public static final C2491fm f2162Cx = null;

    /* renamed from: Lm */
    public static final C2491fm f2163Lm = null;
    public static final C2491fm _f_dispose_0020_0028_0029V = null;
    public static final int _m_fieldCount = 0;
    public static final C5663aRz[] _m_fields = null;
    public static final int _m_methodCount = 0;
    public static final C2491fm[] _m_methods = null;
    public static final C2491fm avN = null;
    public static final C2491fm cPr = null;
    public static final C2491fm cPs = null;
    public static final C5663aRz dWS = null;
    public static final C2491fm dWX = null;
    public static final C5663aRz eIQ = null;
    public static final C5663aRz eIS = null;
    public static final C2491fm eIT = null;
    public static final C2491fm eIU = null;
    public static final C2491fm eIV = null;
    public static final C2491fm eIW = null;
    public static final C2491fm eIX = null;
    public static final C2491fm eIY = null;
    /* renamed from: gQ */
    public static final C2491fm f2164gQ = null;
    public static final long serialVersionUID = 0;
    public static C6494any ___iScriptClass;
    @C0064Am(aul = "7144716bfc84f175704d2bf894762369", aum = 2)
    private static C3438ra<CitizenshipType> dWR;
    @C0064Am(aul = "be47b6cd60bda7684c638e76b43410cb", aum = 0)
    private static String eIP;
    @C0064Am(aul = "580706d67f2f5a71422523872175824e", aum = 1)
    private static CorporationLogo eIR;

    static {
        m11692V();
    }

    public PlayerView() {
        super((C5540aNg) null);
        super.mo10S();
    }

    public PlayerView(C5540aNg ang) {
        super(ang);
    }

    public PlayerView(Player aku) {
        super((C5540aNg) null);
        super._m_script_init(aku);
    }

    /* renamed from: V */
    static void m11692V() {
        serialVersionUID = (long) 1;
        _m_fieldCount = CharacterView._m_fieldCount + 3;
        _m_methodCount = CharacterView._m_methodCount + 14;
        int i = CharacterView._m_fieldCount;
        C5663aRz[] arzArr = new C5663aRz[(i + 3)];
        C5663aRz b = C5640aRc.m17844b(PlayerView.class, "be47b6cd60bda7684c638e76b43410cb", i);
        eIQ = b;
        arzArr[i] = b;
        int i2 = i + 1;
        C5663aRz b2 = C5640aRc.m17844b(PlayerView.class, "580706d67f2f5a71422523872175824e", i2);
        eIS = b2;
        arzArr[i2] = b2;
        int i3 = i2 + 1;
        C5663aRz b3 = C5640aRc.m17844b(PlayerView.class, "7144716bfc84f175704d2bf894762369", i3);
        dWS = b3;
        arzArr[i3] = b3;
        int i4 = i3 + 1;
        _m_fields = arzArr;
        C1634Xv.m11725a((Object[]) CharacterView._m_fields, (Object[]) _m_fields);
        int i5 = CharacterView._m_methodCount;
        C2491fm[] fmVarArr = new C2491fm[(i5 + 14)];
        C2491fm a = C4105zY.m41624a(PlayerView.class, "c42f660f96318e2d55715f277da3f14e", i5);
        eIT = a;
        fmVarArr[i5] = a;
        int i6 = i5 + 1;
        C2491fm a2 = C4105zY.m41624a(PlayerView.class, "8425d4e0ef7c8e1080267067263ef7d3", i6);
        eIU = a2;
        fmVarArr[i6] = a2;
        int i7 = i6 + 1;
        C2491fm a3 = C4105zY.m41624a(PlayerView.class, "5233c49d14b63d0426e820668ee43f1a", i7);
        eIV = a3;
        fmVarArr[i7] = a3;
        int i8 = i7 + 1;
        C2491fm a4 = C4105zY.m41624a(PlayerView.class, "6e77764b1bcd6bafed669b6c2a46634e", i8);
        eIW = a4;
        fmVarArr[i8] = a4;
        int i9 = i8 + 1;
        C2491fm a5 = C4105zY.m41624a(PlayerView.class, "d3e8b3e6a8226caa8b38574f4613ddb3", i9);
        dWX = a5;
        fmVarArr[i9] = a5;
        int i10 = i9 + 1;
        C2491fm a6 = C4105zY.m41624a(PlayerView.class, "ae006af9f4fa7b41fa41fa834f30af07", i10);
        cPr = a6;
        fmVarArr[i10] = a6;
        int i11 = i10 + 1;
        C2491fm a7 = C4105zY.m41624a(PlayerView.class, "919cc8327c4fa118d8a7cd5a1610e4da", i11);
        cPs = a7;
        fmVarArr[i11] = a7;
        int i12 = i11 + 1;
        C2491fm a8 = C4105zY.m41624a(PlayerView.class, "86687d2163be6b74727cbdd4200a5967", i12);
        eIX = a8;
        fmVarArr[i12] = a8;
        int i13 = i12 + 1;
        C2491fm a9 = C4105zY.m41624a(PlayerView.class, "6ed59b3bd5ee181dc6b41f619938d0d9", i13);
        eIY = a9;
        fmVarArr[i13] = a9;
        int i14 = i13 + 1;
        C2491fm a10 = C4105zY.m41624a(PlayerView.class, "c279fcc6dec6745313b9eb1927a0efb1", i14);
        avN = a10;
        fmVarArr[i14] = a10;
        int i15 = i14 + 1;
        C2491fm a11 = C4105zY.m41624a(PlayerView.class, "ed361740329f54ec3edc3f5a8ebb9151", i15);
        _f_dispose_0020_0028_0029V = a11;
        fmVarArr[i15] = a11;
        int i16 = i15 + 1;
        C2491fm a12 = C4105zY.m41624a(PlayerView.class, "3a3f53fb8f62b4308c38f1e2a4eee89e", i16);
        f2163Lm = a12;
        fmVarArr[i16] = a12;
        int i17 = i16 + 1;
        C2491fm a13 = C4105zY.m41624a(PlayerView.class, "5053265444cf31a4f91e9452928fdc1c", i17);
        f2164gQ = a13;
        fmVarArr[i17] = a13;
        int i18 = i17 + 1;
        C2491fm a14 = C4105zY.m41624a(PlayerView.class, "4e47af9cd554bdb7f64b1967f98d8abf", i18);
        f2162Cx = a14;
        fmVarArr[i18] = a14;
        int i19 = i18 + 1;
        _m_methods = fmVarArr;
        C1634Xv.m11725a((Object[]) CharacterView._m_methods, (Object[]) _m_methods);
        ___iScriptClass = aUO.m18566a(PlayerView.class, C5584aOy.class, _m_fields, _m_methods);
    }

    /* renamed from: b */
    private void m11693b(CorporationLogo ec) {
        bFf().mo5608dq().mo3197f(eIS, ec);
    }

    @C0064Am(aul = "ae006af9f4fa7b41fa41fa834f30af07", aum = 0)
    @C5566aOg
    /* renamed from: b */
    private void m11694b(CitizenshipType adl) {
        throw new aWi(new aCE(this, cPr, new Object[]{adl}));
    }

    private String bFF() {
        return (String) bFf().mo5608dq().mo3214p(eIQ);
    }

    private CorporationLogo bFG() {
        return (CorporationLogo) bFf().mo5608dq().mo3214p(eIS);
    }

    @C0064Am(aul = "86687d2163be6b74727cbdd4200a5967", aum = 0)
    @C5566aOg
    private void bFL() {
        throw new aWi(new aCE(this, eIX, new Object[0]));
    }

    @C0064Am(aul = "6ed59b3bd5ee181dc6b41f619938d0d9", aum = 0)
    @C5566aOg
    private void bFN() {
        throw new aWi(new aCE(this, eIY, new Object[0]));
    }

    /* renamed from: bo */
    private void m11695bo(C3438ra raVar) {
        bFf().mo5608dq().mo3197f(dWS, raVar);
    }

    private C3438ra bqt() {
        return (C3438ra) bFf().mo5608dq().mo3214p(dWS);
    }

    @C0064Am(aul = "6e77764b1bcd6bafed669b6c2a46634e", aum = 0)
    @C5566aOg
    /* renamed from: c */
    private void m11696c(CorporationLogo ec) {
        throw new aWi(new aCE(this, eIW, new Object[]{ec}));
    }

    @C0064Am(aul = "919cc8327c4fa118d8a7cd5a1610e4da", aum = 0)
    @C5566aOg
    /* renamed from: d */
    private void m11697d(CitizenshipType adl) {
        throw new aWi(new aCE(this, cPs, new Object[]{adl}));
    }

    @C0064Am(aul = "5053265444cf31a4f91e9452928fdc1c", aum = 0)
    @C5566aOg
    /* renamed from: dd */
    private void m11698dd() {
        throw new aWi(new aCE(this, f2164gQ, new Object[0]));
    }

    /* renamed from: hA */
    private void m11700hA(String str) {
        bFf().mo5608dq().mo3197f(eIQ, str);
    }

    @C0064Am(aul = "8425d4e0ef7c8e1080267067263ef7d3", aum = 0)
    @C5566aOg
    /* renamed from: hB */
    private void m11701hB(String str) {
        throw new aWi(new aCE(this, eIU, new Object[]{str}));
    }

    @C0064Am(aul = "4e47af9cd554bdb7f64b1967f98d8abf", aum = 0)
    /* renamed from: lz */
    private Character m11702lz() {
        return mo6843KY();
    }

    /* renamed from: KY */
    public Player mo6843KY() {
        switch (bFf().mo6893i(avN)) {
            case 0:
                return null;
            case 2:
                return (Player) bFf().mo5606d(new aCE(this, avN, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, avN, new Object[0]));
                break;
        }
        return m11691KX();
    }

    /* renamed from: S */
    public void mo10S() {
        super.mo10S();
    }

    /* renamed from: T */
    public C6494any mo11T() {
        return ___iScriptClass;
    }

    /* renamed from: U */
    public C2491fm[] mo12U() {
        return _m_methods;
    }

    /* renamed from: W */
    public Object mo13W() {
        return new C5584aOy(this);
    }

    /* renamed from: a */
    public Object mo14a(C0495Gr gr) {
        Object[] args = gr.getArgs();
        switch (gr.mo2417hq() - CharacterView._m_methodCount) {
            case 0:
                return bFH();
            case 1:
                m11701hB((String) args[0]);
                return null;
            case 2:
                return bFJ();
            case 3:
                m11696c((CorporationLogo) args[0]);
                return null;
            case 4:
                return bqw();
            case 5:
                m11694b((CitizenshipType) args[0]);
                return null;
            case 6:
                m11697d((CitizenshipType) args[0]);
                return null;
            case 7:
                bFL();
                return null;
            case 8:
                bFN();
                return null;
            case 9:
                return m11691KX();
            case 10:
                m11699fg();
                return null;
            case 11:
                m11703qU();
                return null;
            case 12:
                m11698dd();
                return null;
            case 13:
                return m11702lz();
            default:
                return super.mo14a(gr);
        }
    }

    /* renamed from: a */
    public void mo15a(Collection collection, C0495Gr gr) {
        super.mo15a(collection, gr);
    }

    public String bFI() {
        switch (bFf().mo6893i(eIT)) {
            case 0:
                return null;
            case 2:
                return (String) bFf().mo5606d(new aCE(this, eIT, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, eIT, new Object[0]));
                break;
        }
        return bFH();
    }

    public CorporationLogo bFK() {
        switch (bFf().mo6893i(eIV)) {
            case 0:
                return null;
            case 2:
                return (CorporationLogo) bFf().mo5606d(new aCE(this, eIV, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, eIV, new Object[0]));
                break;
        }
        return bFJ();
    }

    @C5566aOg
    public void bFM() {
        switch (bFf().mo6893i(eIX)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, eIX, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, eIX, new Object[0]));
                break;
        }
        bFL();
    }

    public C3438ra<CitizenshipType> bqx() {
        switch (bFf().mo6893i(dWX)) {
            case 0:
                return null;
            case 2:
                return (C3438ra) bFf().mo5606d(new aCE(this, dWX, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, dWX, new Object[0]));
                break;
        }
        return bqw();
    }

    @C5566aOg
    /* renamed from: c */
    public void mo6849c(CitizenshipType adl) {
        switch (bFf().mo6893i(cPr)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, cPr, new Object[]{adl}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, cPr, new Object[]{adl}));
                break;
        }
        m11694b(adl);
    }

    /* renamed from: c */
    public C5663aRz[] mo25c() {
        return _m_fields;
    }

    @C5566aOg
    /* renamed from: d */
    public void mo6850d(CorporationLogo ec) {
        switch (bFf().mo6893i(eIW)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, eIW, new Object[]{ec}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, eIW, new Object[]{ec}));
                break;
        }
        m11696c(ec);
    }

    @C5566aOg
    /* renamed from: de */
    public void mo6851de() {
        switch (bFf().mo6893i(f2164gQ)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f2164gQ, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f2164gQ, new Object[0]));
                break;
        }
        m11698dd();
    }

    public void dispose() {
        switch (bFf().mo6893i(_f_dispose_0020_0028_0029V)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, _f_dispose_0020_0028_0029V, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, _f_dispose_0020_0028_0029V, new Object[0]));
                break;
        }
        m11699fg();
    }

    @C5566aOg
    /* renamed from: e */
    public void mo6852e(CitizenshipType adl) {
        switch (bFf().mo6893i(cPs)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, cPs, new Object[]{adl}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, cPs, new Object[]{adl}));
                break;
        }
        m11697d(adl);
    }

    @C5566aOg
    /* renamed from: hC */
    public void mo6853hC(String str) {
        switch (bFf().mo6893i(eIU)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, eIU, new Object[]{str}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, eIU, new Object[]{str}));
                break;
        }
        m11701hB(str);
    }

    /* renamed from: lA */
    public /* bridge */ /* synthetic */ Character mo6854lA() {
        switch (bFf().mo6893i(f2162Cx)) {
            case 0:
                return null;
            case 2:
                return (Character) bFf().mo5606d(new aCE(this, f2162Cx, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f2162Cx, new Object[0]));
                break;
        }
        return m11702lz();
    }

    /* renamed from: qV */
    public void mo6855qV() {
        switch (bFf().mo6893i(f2163Lm)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f2163Lm, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f2163Lm, new Object[0]));
                break;
        }
        m11703qU();
    }

    @C5566aOg
    public void refresh() {
        switch (bFf().mo6893i(eIY)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, eIY, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, eIY, new Object[0]));
                break;
        }
        bFN();
    }

    /* renamed from: b */
    public void mo6844b(Player aku) {
        super.mo14234h(aku);
    }

    @C0064Am(aul = "c42f660f96318e2d55715f277da3f14e", aum = 0)
    private String bFH() {
        return bFF();
    }

    @C0064Am(aul = "5233c49d14b63d0426e820668ee43f1a", aum = 0)
    private CorporationLogo bFJ() {
        return bFG();
    }

    @C0064Am(aul = "d3e8b3e6a8226caa8b38574f4613ddb3", aum = 0)
    private C3438ra<CitizenshipType> bqw() {
        return bqt();
    }

    @C0064Am(aul = "c279fcc6dec6745313b9eb1927a0efb1", aum = 0)
    /* renamed from: KX */
    private Player m11691KX() {
        return (Player) super.mo6854lA();
    }

    @C0064Am(aul = "ed361740329f54ec3edc3f5a8ebb9151", aum = 0)
    /* renamed from: fg */
    private void m11699fg() {
        bFM();
        m11693b((CorporationLogo) null);
        super.dispose();
    }

    @C0064Am(aul = "3a3f53fb8f62b4308c38f1e2a4eee89e", aum = 0)
    /* renamed from: qU */
    private void m11703qU() {
        super.mo6855qV();
        if (bFG() != null) {
            bFG().push();
        }
    }
}
