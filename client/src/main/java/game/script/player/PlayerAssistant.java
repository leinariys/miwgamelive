package game.script.player;

import game.network.message.externalizable.aCE;
import game.network.message.serializable.C1556Wo;
import game.script.Actor;
import game.script.TaikodomObject;
import game.script.item.Item;
import game.script.item.ItemLocation;
import game.script.item.ItemType;
import game.script.newmarket.BuyOrder;
import game.script.newmarket.BuyOrderInfo;
import game.script.newmarket.ListContainer;
import game.script.newmarket.SellOrderInfo;
import game.script.newmarket.store.Store;
import game.script.pda.TaikopediaEntry;
import game.script.ship.Station;
import game.script.space.Node;
import game.script.space.StellarSystem;
import logic.aaa.C1506WA;
import logic.aaa.C1549Wj;
import logic.baa.*;
import logic.data.mbean.C3773ur;
import logic.res.code.C5640aRc;
import logic.res.code.C5663aRz;
import logic.res.code.C6494any;
import logic.res.html.C2491fm;
import p001a.*;

import java.util.*;

@C5829abJ("1.0.0")
@C5511aMd
@C6485anp
/* renamed from: a.aVw  reason: case insensitive filesystem */
/* compiled from: a */
public class PlayerAssistant extends TaikodomObject implements C1616Xf, aOW {

    /* renamed from: Do */
    public static final C2491fm f3985Do = null;
    public static final C2491fm _f_dispose_0020_0028_0029V = null;
    public static final int _m_fieldCount = 0;
    public static final C5663aRz[] _m_fields = null;
    public static final int _m_methodCount = 0;
    public static final C2491fm[] _m_methods = null;
    public static final C2491fm bdg = null;
    public static final C2491fm bkL = null;
    public static final C5663aRz dNK = null;
    public static final C5663aRz dNL = null;
    public static final C2491fm dOc = null;
    /* renamed from: gQ */
    public static final C2491fm f3987gQ = null;
    /* renamed from: hz */
    public static final C5663aRz f3988hz = null;
    public static final C5663aRz iZA = null;
    public static final C5663aRz iZB = null;
    public static final C5663aRz iZC = null;
    public static final C5663aRz iZD = null;
    public static final C5663aRz iZE = null;
    public static final C2491fm iZF = null;
    public static final C2491fm iZG = null;
    public static final C2491fm iZH = null;
    public static final C2491fm iZI = null;
    public static final C2491fm iZJ = null;
    public static final C2491fm iZK = null;
    public static final C2491fm iZL = null;
    public static final C2491fm iZM = null;
    public static final C2491fm iZN = null;
    public static final C2491fm iZO = null;
    public static final C2491fm iZP = null;
    public static final C2491fm iZQ = null;
    public static final C2491fm iZR = null;
    public static final C2491fm iZS = null;
    public static final C2491fm iZT = null;
    public static final C2491fm iZU = null;
    public static final C2491fm iZV = null;
    public static final C2491fm iZW = null;
    public static final C2491fm iZX = null;
    public static final C2491fm iZY = null;
    public static final C2491fm iZZ = null;
    public static final C2491fm jaa = null;
    public static final C2491fm jab = null;
    public static final C2491fm jac = null;
    public static final C2491fm jad = null;
    public static final C2491fm jae = null;
    public static final C2491fm jaf = null;
    public static final C2491fm jag = null;
    public static final long serialVersionUID = 0;
    private static final long iZz = 30000;
    public static C6494any ___iScriptClass = null;
    @C0064Am(aul = "f314b1b3499f5192b9bbf259f9c55438", aum = 0)

    /* renamed from: P */
    private static Player f3986P = null;
    @C0064Am(aul = "01256b69480846d5c33b681858b37511", aum = 1)
    private static C1556Wo<Station, ListContainer<BuyOrderInfo>> bvA = null;
    @C0064Am(aul = "99db07ec2284575e307d6777a6829837", aum = 2)
    private static C1556Wo<Station, ListContainer<SellOrderInfo>> bvB = null;
    @C0064Am(aul = "3316fa910e6e7cc2b06ee696b6e4b0e2", aum = 3)
    private static long bvC = 0;
    @C0064Am(aul = "2d60fc7c4a7f6b40fc30c302281badef", aum = 4)
    private static long bvD = 0;
    @C0064Am(aul = "3f1172dafcc9e68ff48441ceeea3cd25", aum = 5)
    private static C1556Wo<C4033yO, Boolean> bvE = null;
    @C0064Am(aul = "6ffea845e9f52beabfa83020e15f49d3", aum = 6)
    private static C1556Wo<C4033yO, Boolean> bvF = null;
    @C0064Am(aul = "4d8623eab9c871fbcc04de55fb9cccb3", aum = 7)
    private static C1556Wo<C4068yr, DataSetContainer<aDJ>> bvG = null;

    static {
        m19155V();
    }

    public PlayerAssistant() {
        super((C5540aNg) null);
        super.mo10S();
    }

    public PlayerAssistant(C5540aNg ang) {
        super(ang);
    }

    public PlayerAssistant(Player aku) {
        super((C5540aNg) null);
        super._m_script_init(aku);
    }

    /* renamed from: V */
    static void m19155V() {
        serialVersionUID = (long) 1;
        _m_fieldCount = TaikodomObject._m_fieldCount + 8;
        _m_methodCount = TaikodomObject._m_methodCount + 34;
        int i = TaikodomObject._m_fieldCount;
        C5663aRz[] arzArr = new C5663aRz[(i + 8)];
        C5663aRz b = C5640aRc.m17844b(PlayerAssistant.class, "f314b1b3499f5192b9bbf259f9c55438", i);
        f3988hz = b;
        arzArr[i] = b;
        int i2 = i + 1;
        C5663aRz b2 = C5640aRc.m17844b(PlayerAssistant.class, "01256b69480846d5c33b681858b37511", i2);
        dNK = b2;
        arzArr[i2] = b2;
        int i3 = i2 + 1;
        C5663aRz b3 = C5640aRc.m17844b(PlayerAssistant.class, "99db07ec2284575e307d6777a6829837", i3);
        dNL = b3;
        arzArr[i3] = b3;
        int i4 = i3 + 1;
        C5663aRz b4 = C5640aRc.m17844b(PlayerAssistant.class, "3316fa910e6e7cc2b06ee696b6e4b0e2", i4);
        iZA = b4;
        arzArr[i4] = b4;
        int i5 = i4 + 1;
        C5663aRz b5 = C5640aRc.m17844b(PlayerAssistant.class, "2d60fc7c4a7f6b40fc30c302281badef", i5);
        iZB = b5;
        arzArr[i5] = b5;
        int i6 = i5 + 1;
        C5663aRz b6 = C5640aRc.m17844b(PlayerAssistant.class, "3f1172dafcc9e68ff48441ceeea3cd25", i6);
        iZC = b6;
        arzArr[i6] = b6;
        int i7 = i6 + 1;
        C5663aRz b7 = C5640aRc.m17844b(PlayerAssistant.class, "6ffea845e9f52beabfa83020e15f49d3", i7);
        iZD = b7;
        arzArr[i7] = b7;
        int i8 = i7 + 1;
        C5663aRz b8 = C5640aRc.m17844b(PlayerAssistant.class, "4d8623eab9c871fbcc04de55fb9cccb3", i8);
        iZE = b8;
        arzArr[i8] = b8;
        int i9 = i8 + 1;
        _m_fields = arzArr;
        C1634Xv.m11725a((Object[]) TaikodomObject._m_fields, (Object[]) _m_fields);
        int i10 = TaikodomObject._m_methodCount;
        C2491fm[] fmVarArr = new C2491fm[(i10 + 34)];
        C2491fm a = C4105zY.m41624a(PlayerAssistant.class, "f0d785fc3063e414a0f4cfadcc09c653", i10);
        iZF = a;
        fmVarArr[i10] = a;
        int i11 = i10 + 1;
        C2491fm a2 = C4105zY.m41624a(PlayerAssistant.class, "69ede12cb4232bcbf556f6c2c41f8a8e", i11);
        iZG = a2;
        fmVarArr[i11] = a2;
        int i12 = i11 + 1;
        C2491fm a3 = C4105zY.m41624a(PlayerAssistant.class, "164688c4e1d1dad7795b2d45b83f9e62", i12);
        iZH = a3;
        fmVarArr[i12] = a3;
        int i13 = i12 + 1;
        C2491fm a4 = C4105zY.m41624a(PlayerAssistant.class, "68d154bfc39afe778bf01740154da130", i13);
        iZI = a4;
        fmVarArr[i13] = a4;
        int i14 = i13 + 1;
        C2491fm a5 = C4105zY.m41624a(PlayerAssistant.class, "8d7c8ad67bab586f2920ef25b1f5c436", i14);
        iZJ = a5;
        fmVarArr[i14] = a5;
        int i15 = i14 + 1;
        C2491fm a6 = C4105zY.m41624a(PlayerAssistant.class, "7cf3a4e60423a7e7602e404ea1bb0191", i15);
        bdg = a6;
        fmVarArr[i15] = a6;
        int i16 = i15 + 1;
        C2491fm a7 = C4105zY.m41624a(PlayerAssistant.class, "6fea6ec4341422cf02ac5a11c74721e0", i16);
        iZK = a7;
        fmVarArr[i16] = a7;
        int i17 = i16 + 1;
        C2491fm a8 = C4105zY.m41624a(PlayerAssistant.class, "c2f240f4a86e6324cf8a97c1c5261b58", i17);
        iZL = a8;
        fmVarArr[i17] = a8;
        int i18 = i17 + 1;
        C2491fm a9 = C4105zY.m41624a(PlayerAssistant.class, "f76b3368e3e6456beb3af69a5c40e4ac", i18);
        iZM = a9;
        fmVarArr[i18] = a9;
        int i19 = i18 + 1;
        C2491fm a10 = C4105zY.m41624a(PlayerAssistant.class, "266de33d89b993f9b40d9e3395c32322", i19);
        iZN = a10;
        fmVarArr[i19] = a10;
        int i20 = i19 + 1;
        C2491fm a11 = C4105zY.m41624a(PlayerAssistant.class, "40a8757a971d148e22287c7fe5c6a4f0", i20);
        iZO = a11;
        fmVarArr[i20] = a11;
        int i21 = i20 + 1;
        C2491fm a12 = C4105zY.m41624a(PlayerAssistant.class, "79e36b7f4abe8d3ac2a305e199affd3c", i21);
        iZP = a12;
        fmVarArr[i21] = a12;
        int i22 = i21 + 1;
        C2491fm a13 = C4105zY.m41624a(PlayerAssistant.class, "97823e273c1156ca35a75d1d51b36933", i22);
        iZQ = a13;
        fmVarArr[i22] = a13;
        int i23 = i22 + 1;
        C2491fm a14 = C4105zY.m41624a(PlayerAssistant.class, "f42198c181b12452aed7e773697f7608", i23);
        iZR = a14;
        fmVarArr[i23] = a14;
        int i24 = i23 + 1;
        C2491fm a15 = C4105zY.m41624a(PlayerAssistant.class, "f75ad8919d37368a04c95abee9767fe6", i24);
        dOc = a15;
        fmVarArr[i24] = a15;
        int i25 = i24 + 1;
        C2491fm a16 = C4105zY.m41624a(PlayerAssistant.class, "5565850c72082490eaf08b9a10d0931c", i25);
        iZS = a16;
        fmVarArr[i25] = a16;
        int i26 = i25 + 1;
        C2491fm a17 = C4105zY.m41624a(PlayerAssistant.class, "f8d71539dac5f9718fda24e3d2d3e994", i26);
        iZT = a17;
        fmVarArr[i26] = a17;
        int i27 = i26 + 1;
        C2491fm a18 = C4105zY.m41624a(PlayerAssistant.class, "a9e1b882a93ea8ccfbdabd2d9f4e4e14", i27);
        iZU = a18;
        fmVarArr[i27] = a18;
        int i28 = i27 + 1;
        C2491fm a19 = C4105zY.m41624a(PlayerAssistant.class, "f8affe945c7cefbe422abcfa2f0d49cd", i28);
        iZV = a19;
        fmVarArr[i28] = a19;
        int i29 = i28 + 1;
        C2491fm a20 = C4105zY.m41624a(PlayerAssistant.class, "cfc901748016fb01acda059e7b7d0386", i29);
        iZW = a20;
        fmVarArr[i29] = a20;
        int i30 = i29 + 1;
        C2491fm a21 = C4105zY.m41624a(PlayerAssistant.class, "4a7060d1728962fa91e216830611ca17", i30);
        iZX = a21;
        fmVarArr[i30] = a21;
        int i31 = i30 + 1;
        C2491fm a22 = C4105zY.m41624a(PlayerAssistant.class, "a204ef042f9b33d263794ca6314aec20", i31);
        iZY = a22;
        fmVarArr[i31] = a22;
        int i32 = i31 + 1;
        C2491fm a23 = C4105zY.m41624a(PlayerAssistant.class, "4d361b6d964567fd9bdf923aa71e10bd", i32);
        iZZ = a23;
        fmVarArr[i32] = a23;
        int i33 = i32 + 1;
        C2491fm a24 = C4105zY.m41624a(PlayerAssistant.class, "0f1fd1516c5469850d774f2c2bb6df68", i33);
        _f_dispose_0020_0028_0029V = a24;
        fmVarArr[i33] = a24;
        int i34 = i33 + 1;
        C2491fm a25 = C4105zY.m41624a(PlayerAssistant.class, "ab5e2f190d5cd1b61def92120e2f0a85", i34);
        jaa = a25;
        fmVarArr[i34] = a25;
        int i35 = i34 + 1;
        C2491fm a26 = C4105zY.m41624a(PlayerAssistant.class, "f1fff14222d9a323ac53f9fda535b9f6", i35);
        jab = a26;
        fmVarArr[i35] = a26;
        int i36 = i35 + 1;
        C2491fm a27 = C4105zY.m41624a(PlayerAssistant.class, "5ca8091258ea47984b79caaa264cc4bd", i36);
        f3985Do = a27;
        fmVarArr[i36] = a27;
        int i37 = i36 + 1;
        C2491fm a28 = C4105zY.m41624a(PlayerAssistant.class, "4942f83e9a4138cc824231d195dc1aa0", i37);
        jac = a28;
        fmVarArr[i37] = a28;
        int i38 = i37 + 1;
        C2491fm a29 = C4105zY.m41624a(PlayerAssistant.class, "afbb19c83502f032d2f57b4cc08da420", i38);
        jad = a29;
        fmVarArr[i38] = a29;
        int i39 = i38 + 1;
        C2491fm a30 = C4105zY.m41624a(PlayerAssistant.class, "71b1cb08153b563343bc610896629c76", i39);
        jae = a30;
        fmVarArr[i39] = a30;
        int i40 = i39 + 1;
        C2491fm a31 = C4105zY.m41624a(PlayerAssistant.class, "f01b8b059625245d85e55eecbbbc3fd0", i40);
        jaf = a31;
        fmVarArr[i40] = a31;
        int i41 = i40 + 1;
        C2491fm a32 = C4105zY.m41624a(PlayerAssistant.class, "23c579a03dc09e65f3b411b51f150bc0", i41);
        jag = a32;
        fmVarArr[i41] = a32;
        int i42 = i41 + 1;
        C2491fm a33 = C4105zY.m41624a(PlayerAssistant.class, "dbd524d4e9155c89fa1ec8ae876626d0", i42);
        bkL = a33;
        fmVarArr[i42] = a33;
        int i43 = i42 + 1;
        C2491fm a34 = C4105zY.m41624a(PlayerAssistant.class, "9bb5def8da5d975b6488cf35715976f5", i43);
        f3987gQ = a34;
        fmVarArr[i43] = a34;
        int i44 = i43 + 1;
        _m_methods = fmVarArr;
        C1634Xv.m11725a((Object[]) TaikodomObject._m_methods, (Object[]) _m_methods);
        ___iScriptClass = aUO.m18566a(PlayerAssistant.class, C3773ur.class, _m_fields, _m_methods);
    }

    /* renamed from: K */
    private void m19152K(C1556Wo wo) {
        bFf().mo5608dq().mo3197f(dNK, wo);
    }

    /* renamed from: L */
    private void m19153L(C1556Wo wo) {
        bFf().mo5608dq().mo3197f(dNL, wo);
    }

    @C5566aOg
    @C0064Am(aul = "cfc901748016fb01acda059e7b7d0386", aum = 0)
    @C2499fr
    /* renamed from: U */
    private void m19154U(List<C4068yr> list) {
        throw new aWi(new aCE(this, iZW, new Object[]{list}));
    }

    @C4034yP
    @C5566aOg
    @C0064Am(aul = "4a7060d1728962fa91e216830611ca17", aum = 0)
    @C2499fr
    /* renamed from: W */
    private void m19156W(List<C4068yr> list) {
        throw new aWi(new aCE(this, iZX, new Object[]{list}));
    }

    /* renamed from: a */
    private void m19159a(Player aku) {
        bFf().mo5608dq().mo3197f(f3988hz, aku);
    }

    @C4034yP
    @C5566aOg
    @C0064Am(aul = "71b1cb08153b563343bc610896629c76", aum = 0)
    @C2499fr
    /* renamed from: a */
    private void m19160a(C4033yO yOVar, boolean z) {
        throw new aWi(new aCE(this, jae, new Object[]{yOVar, new Boolean(z)}));
    }

    @C4034yP
    @C5566aOg
    @C0064Am(aul = "ab5e2f190d5cd1b61def92120e2f0a85", aum = 0)
    @C2499fr
    /* renamed from: a */
    private void m19161a(C4068yr yrVar, aDJ adj) {
        throw new aWi(new aCE(this, jaa, new Object[]{yrVar, adj}));
    }

    @C0064Am(aul = "a204ef042f9b33d263794ca6314aec20", aum = 0)
    @C5566aOg
    /* renamed from: a */
    private boolean m19163a(C4068yr yrVar, boolean z) {
        throw new aWi(new aCE(this, iZY, new Object[]{yrVar, new Boolean(z)}));
    }

    @C0064Am(aul = "69ede12cb4232bcbf556f6c2c41f8a8e", aum = 0)
    @C5566aOg
    @C2499fr
    /* renamed from: aI */
    private boolean m19164aI(Station bf) {
        throw new aWi(new aCE(this, iZG, new Object[]{bf}));
    }

    @C0064Am(aul = "68d154bfc39afe778bf01740154da130", aum = 0)
    @C5566aOg
    @C2499fr
    /* renamed from: aK */
    private boolean m19165aK(Station bf) {
        throw new aWi(new aCE(this, iZI, new Object[]{bf}));
    }

    /* renamed from: au */
    private void m19166au(C1556Wo wo) {
        bFf().mo5608dq().mo3197f(iZC, wo);
    }

    /* renamed from: av */
    private void m19167av(C1556Wo wo) {
        bFf().mo5608dq().mo3197f(iZD, wo);
    }

    /* renamed from: aw */
    private void m19168aw(C1556Wo wo) {
        bFf().mo5608dq().mo3197f(iZE, wo);
    }

    @C0064Am(aul = "6fea6ec4341422cf02ac5a11c74721e0", aum = 0)
    @C5566aOg
    @C2499fr
    /* renamed from: b */
    private SellOrderInfo m19169b(Item auq, int i, long j, long j2) {
        throw new aWi(new aCE(this, iZK, new Object[]{auq, new Integer(i), new Long(j), new Long(j2)}));
    }

    /* renamed from: b */
    private boolean m19170b(ItemLocation aag, Station bf) {
        switch (bFf().mo6893i(iZU)) {
            case 0:
                return false;
            case 2:
                return ((Boolean) bFf().mo5606d(new aCE(this, iZU, new Object[]{aag, bf}))).booleanValue();
            case 3:
                bFf().mo5606d(new aCE(this, iZU, new Object[]{aag, bf}));
                break;
        }
        return m19162a(aag, bf);
    }

    @C0064Am(aul = "f76b3368e3e6456beb3af69a5c40e4ac", aum = 0)
    @C5566aOg
    @C2499fr
    /* renamed from: b */
    private boolean m19171b(BuyOrderInfo sfVar, Item auq) {
        throw new aWi(new aCE(this, iZM, new Object[]{sfVar, auq}));
    }

    @C0064Am(aul = "266de33d89b993f9b40d9e3395c32322", aum = 0)
    @C5566aOg
    @C2499fr
    /* renamed from: b */
    private boolean m19172b(SellOrderInfo xTVar, int i, C0314EF ef) {
        throw new aWi(new aCE(this, iZN, new Object[]{xTVar, new Integer(i), ef}));
    }

    @C5566aOg
    /* renamed from: b */
    private boolean m19173b(C4068yr yrVar, boolean z) {
        switch (bFf().mo6893i(iZY)) {
            case 0:
                return false;
            case 2:
                return ((Boolean) bFf().mo5606d(new aCE(this, iZY, new Object[]{yrVar, new Boolean(z)}))).booleanValue();
            case 3:
                bFf().mo5606d(new aCE(this, iZY, new Object[]{yrVar, new Boolean(z)}));
                break;
        }
        return m19163a(yrVar, z);
    }

    private C1556Wo bmv() {
        return (C1556Wo) bFf().mo5608dq().mo3214p(dNK);
    }

    private C1556Wo bmw() {
        return (C1556Wo) bFf().mo5608dq().mo3214p(dNL);
    }

    @C4034yP
    @C5566aOg
    @C0064Am(aul = "f01b8b059625245d85e55eecbbbc3fd0", aum = 0)
    @C2499fr
    /* renamed from: c */
    private void m19174c(C4033yO yOVar, boolean z) {
        throw new aWi(new aCE(this, jaf, new Object[]{yOVar, new Boolean(z)}));
    }

    @C0064Am(aul = "79e36b7f4abe8d3ac2a305e199affd3c", aum = 0)
    @C5566aOg
    @C2499fr
    /* renamed from: c */
    private boolean m19175c(SellOrderInfo xTVar) {
        throw new aWi(new aCE(this, iZP, new Object[]{xTVar}));
    }

    @C4034yP
    @C5566aOg
    @C0064Am(aul = "40a8757a971d148e22287c7fe5c6a4f0", aum = 0)
    @C2499fr
    /* renamed from: d */
    private void m19176d(BuyOrderInfo sfVar) {
        throw new aWi(new aCE(this, iZO, new Object[]{sfVar}));
    }

    @C4034yP
    @C5566aOg
    @C0064Am(aul = "f8affe945c7cefbe422abcfa2f0d49cd", aum = 0)
    @C2499fr
    /* renamed from: d */
    private void m19177d(C4068yr yrVar) {
        throw new aWi(new aCE(this, iZV, new Object[]{yrVar}));
    }

    private C1556Wo dBA() {
        return (C1556Wo) bFf().mo5608dq().mo3214p(iZE);
    }

    @C5566aOg
    @C2950m(timeout = 3000)
    @C0064Am(aul = "f0d785fc3063e414a0f4cfadcc09c653", aum = 0)
    @C2499fr
    private boolean dBB() {
        throw new aWi(new aCE(this, iZF, new Object[0]));
    }

    @C5566aOg
    @C2950m(timeout = 3000)
    @C0064Am(aul = "164688c4e1d1dad7795b2d45b83f9e62", aum = 0)
    @C2499fr
    private boolean dBD() {
        throw new aWi(new aCE(this, iZH, new Object[0]));
    }

    @C5566aOg
    @C0064Am(aul = "5565850c72082490eaf08b9a10d0931c", aum = 0)
    @C2499fr
    private List<ItemType> dBL() {
        throw new aWi(new aCE(this, iZS, new Object[0]));
    }

    @C0064Am(aul = "f8d71539dac5f9718fda24e3d2d3e994", aum = 0)
    @C5566aOg
    private boolean dBN() {
        throw new aWi(new aCE(this, iZT, new Object[0]));
    }

    private long dBw() {
        return bFf().mo5608dq().mo3213o(iZA);
    }

    private long dBx() {
        return bFf().mo5608dq().mo3213o(iZB);
    }

    private C1556Wo dBy() {
        return (C1556Wo) bFf().mo5608dq().mo3214p(iZC);
    }

    private C1556Wo dBz() {
        return (C1556Wo) bFf().mo5608dq().mo3214p(iZD);
    }

    /* renamed from: dG */
    private Player m19178dG() {
        return (Player) bFf().mo5608dq().mo3214p(f3988hz);
    }

    @C0064Am(aul = "9bb5def8da5d975b6488cf35715976f5", aum = 0)
    @C5566aOg
    /* renamed from: dd */
    private void m19179dd() {
        throw new aWi(new aCE(this, f3987gQ, new Object[0]));
    }

    @C0064Am(aul = "c2f240f4a86e6324cf8a97c1c5261b58", aum = 0)
    @C5566aOg
    @C2499fr
    /* renamed from: e */
    private BuyOrderInfo m19180e(C0847MM mm, ItemType jCVar, int i, int i2, long j, long j2) {
        throw new aWi(new aCE(this, iZL, new Object[]{mm, jCVar, new Integer(i), new Integer(i2), new Long(j), new Long(j2)}));
    }

    @C0064Am(aul = "f75ad8919d37368a04c95abee9767fe6", aum = 0)
    @C5566aOg
    @C2499fr
    /* renamed from: fj */
    private long m19183fj(long j) {
        throw new aWi(new aCE(this, dOc, new Object[]{new Long(j)}));
    }

    /* renamed from: iK */
    private boolean m19184iK() {
        switch (bFf().mo6893i(jag)) {
            case 0:
                return false;
            case 2:
                return ((Boolean) bFf().mo5606d(new aCE(this, jag, new Object[0]))).booleanValue();
            case 3:
                bFf().mo5606d(new aCE(this, jag, new Object[0]));
                break;
        }
        return dBT();
    }

    /* renamed from: iL */
    private boolean m19185iL() {
        switch (bFf().mo6893i(bkL)) {
            case 0:
                return false;
            case 2:
                return ((Boolean) bFf().mo5606d(new aCE(this, bkL, new Object[0]))).booleanValue();
            case 3:
                bFf().mo5606d(new aCE(this, bkL, new Object[0]));
                break;
        }
        return adv();
    }

    /* renamed from: mm */
    private void m19187mm(long j) {
        bFf().mo5608dq().mo3184b(iZA, j);
    }

    /* renamed from: mn */
    private void m19188mn(long j) {
        bFf().mo5608dq().mo3184b(iZB, j);
    }

    /* renamed from: S */
    public void mo10S() {
        super.mo10S();
    }

    /* renamed from: T */
    public C6494any mo11T() {
        return ___iScriptClass;
    }

    /* renamed from: U */
    public C2491fm[] mo12U() {
        return _m_methods;
    }

    @C5566aOg
    @C2499fr
    /* renamed from: V */
    public void mo11957V(List<C4068yr> list) {
        switch (bFf().mo6893i(iZW)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, iZW, new Object[]{list}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, iZW, new Object[]{list}));
                break;
        }
        m19154U(list);
    }

    /* renamed from: W */
    public Object mo13W() {
        return new C3773ur(this);
    }

    @C4034yP
    @C5566aOg
    @C2499fr
    /* renamed from: X */
    public void mo11958X(List<C4068yr> list) {
        switch (bFf().mo6893i(iZX)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, iZX, new Object[]{list}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, iZX, new Object[]{list}));
                break;
        }
        m19156W(list);
    }

    /* renamed from: ZA */
    public Set<Station> mo11959ZA() {
        switch (bFf().mo6893i(bdg)) {
            case 0:
                return null;
            case 2:
                return (Set) bFf().mo5606d(new aCE(this, bdg, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, bdg, new Object[0]));
                break;
        }
        return m19157Zz();
    }

    /* renamed from: a */
    public Object mo14a(C0495Gr gr) {
        Object[] args = gr.getArgs();
        switch (gr.mo2417hq() - TaikodomObject._m_methodCount) {
            case 0:
                return new Boolean(dBB());
            case 1:
                return new Boolean(m19164aI((Station) args[0]));
            case 2:
                return new Boolean(dBD());
            case 3:
                return new Boolean(m19165aK((Station) args[0]));
            case 4:
                return dBF();
            case 5:
                return m19157Zz();
            case 6:
                return m19169b((Item) args[0], ((Integer) args[1]).intValue(), ((Long) args[2]).longValue(), ((Long) args[3]).longValue());
            case 7:
                return m19180e((C0847MM) args[0], (ItemType) args[1], ((Integer) args[2]).intValue(), ((Integer) args[3]).intValue(), ((Long) args[4]).longValue(), ((Long) args[5]).longValue());
            case 8:
                return new Boolean(m19171b((BuyOrderInfo) args[0], (Item) args[1]));
            case 9:
                return new Boolean(m19172b((SellOrderInfo) args[0], ((Integer) args[1]).intValue(), (C0314EF) args[2]));
            case 10:
                m19176d((BuyOrderInfo) args[0]);
                return null;
            case 11:
                return new Boolean(m19175c((SellOrderInfo) args[0]));
            case 12:
                return dBH();
            case 13:
                return dBJ();
            case 14:
                return new Long(m19183fj(((Long) args[0]).longValue()));
            case 15:
                return dBL();
            case 16:
                return new Boolean(dBN());
            case 17:
                return new Boolean(m19162a((ItemLocation) args[0], (Station) args[1]));
            case 18:
                m19177d((C4068yr) args[0]);
                return null;
            case 19:
                m19154U((List) args[0]);
                return null;
            case 20:
                m19156W((List) args[0]);
                return null;
            case 21:
                return new Boolean(m19163a((C4068yr) args[0], ((Boolean) args[1]).booleanValue()));
            case 22:
                m19186k((TaikopediaEntry) args[0]);
                return null;
            case 23:
                m19182fg();
                return null;
            case 24:
                m19161a((C4068yr) args[0], (aDJ) args[1]);
                return null;
            case 25:
                return m19181f((C4068yr) args[0]);
            case 26:
                m19158a((C0665JT) args[0]);
                return null;
            case 27:
                return dBP();
            case 28:
                return dBR();
            case 29:
                m19160a((C4033yO) args[0], ((Boolean) args[1]).booleanValue());
                return null;
            case 30:
                m19174c((C4033yO) args[0], ((Boolean) args[1]).booleanValue());
                return null;
            case 31:
                return new Boolean(dBT());
            case 32:
                return new Boolean(adv());
            case 33:
                m19179dd();
                return null;
            default:
                return super.mo14a(gr);
        }
    }

    /* renamed from: a */
    public void mo15a(Collection collection, C0495Gr gr) {
        super.mo15a(collection, gr);
    }

    @C5566aOg
    @C2499fr
    /* renamed from: aJ */
    public boolean mo11960aJ(Station bf) {
        switch (bFf().mo6893i(iZG)) {
            case 0:
                return false;
            case 2:
                return ((Boolean) bFf().mo5606d(new aCE(this, iZG, new Object[]{bf}))).booleanValue();
            case 3:
                bFf().mo5606d(new aCE(this, iZG, new Object[]{bf}));
                break;
        }
        return m19164aI(bf);
    }

    @C5566aOg
    @C2499fr
    /* renamed from: aL */
    public boolean mo11961aL(Station bf) {
        switch (bFf().mo6893i(iZI)) {
            case 0:
                return false;
            case 2:
                return ((Boolean) bFf().mo5606d(new aCE(this, iZI, new Object[]{bf}))).booleanValue();
            case 3:
                bFf().mo5606d(new aCE(this, iZI, new Object[]{bf}));
                break;
        }
        return m19165aK(bf);
    }

    /* renamed from: b */
    public void mo24b(C0665JT jt) {
        switch (bFf().mo6893i(f3985Do)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f3985Do, new Object[]{jt}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f3985Do, new Object[]{jt}));
                break;
        }
        m19158a(jt);
    }

    @C4034yP
    @C5566aOg
    @C2499fr
    /* renamed from: b */
    public void mo11963b(C4033yO yOVar, boolean z) {
        switch (bFf().mo6893i(jae)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, jae, new Object[]{yOVar, new Boolean(z)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, jae, new Object[]{yOVar, new Boolean(z)}));
                break;
        }
        m19160a(yOVar, z);
    }

    @C4034yP
    @C5566aOg
    @C2499fr
    /* renamed from: b */
    public void mo11964b(C4068yr yrVar, aDJ adj) {
        switch (bFf().mo6893i(jaa)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, jaa, new Object[]{yrVar, adj}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, jaa, new Object[]{yrVar, adj}));
                break;
        }
        m19161a(yrVar, adj);
    }

    @C5566aOg
    @C2499fr
    /* renamed from: c */
    public SellOrderInfo mo11965c(Item auq, int i, long j, long j2) {
        switch (bFf().mo6893i(iZK)) {
            case 0:
                return null;
            case 2:
                return (SellOrderInfo) bFf().mo5606d(new aCE(this, iZK, new Object[]{auq, new Integer(i), new Long(j), new Long(j2)}));
            case 3:
                bFf().mo5606d(new aCE(this, iZK, new Object[]{auq, new Integer(i), new Long(j), new Long(j2)}));
                break;
        }
        return m19169b(auq, i, j, j2);
    }

    @C5566aOg
    @C2499fr
    /* renamed from: c */
    public boolean mo11966c(BuyOrderInfo sfVar, Item auq) {
        switch (bFf().mo6893i(iZM)) {
            case 0:
                return false;
            case 2:
                return ((Boolean) bFf().mo5606d(new aCE(this, iZM, new Object[]{sfVar, auq}))).booleanValue();
            case 3:
                bFf().mo5606d(new aCE(this, iZM, new Object[]{sfVar, auq}));
                break;
        }
        return m19171b(sfVar, auq);
    }

    @C5566aOg
    @C2499fr
    /* renamed from: c */
    public boolean mo11967c(SellOrderInfo xTVar, int i, C0314EF ef) {
        switch (bFf().mo6893i(iZN)) {
            case 0:
                return false;
            case 2:
                return ((Boolean) bFf().mo5606d(new aCE(this, iZN, new Object[]{xTVar, new Integer(i), ef}))).booleanValue();
            case 3:
                bFf().mo5606d(new aCE(this, iZN, new Object[]{xTVar, new Integer(i), ef}));
                break;
        }
        return m19172b(xTVar, i, ef);
    }

    /* renamed from: c */
    public C5663aRz[] mo25c() {
        return _m_fields;
    }

    @C4034yP
    @C5566aOg
    @C2499fr
    /* renamed from: d */
    public void mo11968d(C4033yO yOVar, boolean z) {
        switch (bFf().mo6893i(jaf)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, jaf, new Object[]{yOVar, new Boolean(z)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, jaf, new Object[]{yOVar, new Boolean(z)}));
                break;
        }
        m19174c(yOVar, z);
    }

    @C5566aOg
    @C2499fr
    /* renamed from: d */
    public boolean mo11969d(SellOrderInfo xTVar) {
        switch (bFf().mo6893i(iZP)) {
            case 0:
                return false;
            case 2:
                return ((Boolean) bFf().mo5606d(new aCE(this, iZP, new Object[]{xTVar}))).booleanValue();
            case 3:
                bFf().mo5606d(new aCE(this, iZP, new Object[]{xTVar}));
                break;
        }
        return m19175c(xTVar);
    }

    @C5566aOg
    @C2499fr
    @C2950m(timeout = 3000)
    public boolean dBC() {
        switch (bFf().mo6893i(iZF)) {
            case 0:
                return false;
            case 2:
                return ((Boolean) bFf().mo5606d(new aCE(this, iZF, new Object[0]))).booleanValue();
            case 3:
                bFf().mo5606d(new aCE(this, iZF, new Object[0]));
                break;
        }
        return dBB();
    }

    @C5566aOg
    @C2499fr
    @C2950m(timeout = 3000)
    public boolean dBE() {
        switch (bFf().mo6893i(iZH)) {
            case 0:
                return false;
            case 2:
                return ((Boolean) bFf().mo5606d(new aCE(this, iZH, new Object[0]))).booleanValue();
            case 3:
                bFf().mo5606d(new aCE(this, iZH, new Object[0]));
                break;
        }
        return dBD();
    }

    public Set<Station> dBG() {
        switch (bFf().mo6893i(iZJ)) {
            case 0:
                return null;
            case 2:
                return (Set) bFf().mo5606d(new aCE(this, iZJ, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, iZJ, new Object[0]));
                break;
        }
        return dBF();
    }

    public C1556Wo<Station, ListContainer<BuyOrderInfo>> dBI() {
        switch (bFf().mo6893i(iZQ)) {
            case 0:
                return null;
            case 2:
                return (C1556Wo) bFf().mo5606d(new aCE(this, iZQ, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, iZQ, new Object[0]));
                break;
        }
        return dBH();
    }

    public C1556Wo<Station, ListContainer<SellOrderInfo>> dBK() {
        switch (bFf().mo6893i(iZR)) {
            case 0:
                return null;
            case 2:
                return (C1556Wo) bFf().mo5606d(new aCE(this, iZR, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, iZR, new Object[0]));
                break;
        }
        return dBJ();
    }

    @C5566aOg
    @C2499fr
    public List<ItemType> dBM() {
        switch (bFf().mo6893i(iZS)) {
            case 0:
                return null;
            case 2:
                return (List) bFf().mo5606d(new aCE(this, iZS, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, iZS, new Object[0]));
                break;
        }
        return dBL();
    }

    @C5566aOg
    public boolean dBO() {
        switch (bFf().mo6893i(iZT)) {
            case 0:
                return false;
            case 2:
                return ((Boolean) bFf().mo5606d(new aCE(this, iZT, new Object[0]))).booleanValue();
            case 3:
                bFf().mo5606d(new aCE(this, iZT, new Object[0]));
                break;
        }
        return dBN();
    }

    public Map<C4033yO, Boolean> dBQ() {
        switch (bFf().mo6893i(jac)) {
            case 0:
                return null;
            case 2:
                return (Map) bFf().mo5606d(new aCE(this, jac, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, jac, new Object[0]));
                break;
        }
        return dBP();
    }

    public Map<C4033yO, Boolean> dBS() {
        switch (bFf().mo6893i(jad)) {
            case 0:
                return null;
            case 2:
                return (Map) bFf().mo5606d(new aCE(this, jad, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, jad, new Object[0]));
                break;
        }
        return dBR();
    }

    @C5566aOg
    /* renamed from: de */
    public void mo11979de() {
        switch (bFf().mo6893i(f3987gQ)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f3987gQ, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f3987gQ, new Object[0]));
                break;
        }
        m19179dd();
    }

    public void dispose() {
        switch (bFf().mo6893i(_f_dispose_0020_0028_0029V)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, _f_dispose_0020_0028_0029V, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, _f_dispose_0020_0028_0029V, new Object[0]));
                break;
        }
        m19182fg();
    }

    @C4034yP
    @C5566aOg
    @C2499fr
    /* renamed from: e */
    public void mo11980e(BuyOrderInfo sfVar) {
        switch (bFf().mo6893i(iZO)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, iZO, new Object[]{sfVar}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, iZO, new Object[]{sfVar}));
                break;
        }
        m19176d(sfVar);
    }

    @C4034yP
    @C5566aOg
    @C2499fr
    /* renamed from: e */
    public void mo11981e(C4068yr yrVar) {
        switch (bFf().mo6893i(iZV)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, iZV, new Object[]{yrVar}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, iZV, new Object[]{yrVar}));
                break;
        }
        m19177d(yrVar);
    }

    @C5566aOg
    @C2499fr
    /* renamed from: f */
    public BuyOrderInfo mo11982f(C0847MM mm, ItemType jCVar, int i, int i2, long j, long j2) {
        switch (bFf().mo6893i(iZL)) {
            case 0:
                return null;
            case 2:
                return (BuyOrderInfo) bFf().mo5606d(new aCE(this, iZL, new Object[]{mm, jCVar, new Integer(i), new Integer(i2), new Long(j), new Long(j2)}));
            case 3:
                bFf().mo5606d(new aCE(this, iZL, new Object[]{mm, jCVar, new Integer(i), new Integer(i2), new Long(j), new Long(j2)}));
                break;
        }
        return m19180e(mm, jCVar, i, i2, j, j2);
    }

    @C5566aOg
    @C2499fr
    /* renamed from: fk */
    public long mo11983fk(long j) {
        switch (bFf().mo6893i(dOc)) {
            case 0:
                return 0;
            case 2:
                return ((Long) bFf().mo5606d(new aCE(this, dOc, new Object[]{new Long(j)}))).longValue();
            case 3:
                bFf().mo5606d(new aCE(this, dOc, new Object[]{new Long(j)}));
                break;
        }
        return m19183fj(j);
    }

    /* renamed from: g */
    public Set<aDJ> mo11984g(C4068yr yrVar) {
        switch (bFf().mo6893i(jab)) {
            case 0:
                return null;
            case 2:
                return (Set) bFf().mo5606d(new aCE(this, jab, new Object[]{yrVar}));
            case 3:
                bFf().mo5606d(new aCE(this, jab, new Object[]{yrVar}));
                break;
        }
        return m19181f(yrVar);
    }

    /* renamed from: l */
    public void mo11985l(TaikopediaEntry aiz) {
        switch (bFf().mo6893i(iZZ)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, iZZ, new Object[]{aiz}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, iZZ, new Object[]{aiz}));
                break;
        }
        m19186k(aiz);
    }

    /* renamed from: b */
    public void mo11962b(Player aku) {
        super.mo10S();
        m19159a(aku);
    }

    @C0064Am(aul = "8d7c8ad67bab586f2920ef25b1f5c436", aum = 0)
    private Set<Station> dBF() {
        if (m19178dG() == null) {
            return Collections.EMPTY_SET;
        }
        HashSet hashSet = new HashSet();
        for (Node Zw : m19178dG().bhE().azW().mo21606Nc().getNodes()) {
            for (Actor next : Zw.mo21624Zw()) {
                if (next instanceof Station) {
                    hashSet.add((Station) next);
                }
            }
        }
        return hashSet;
    }

    @C0064Am(aul = "7cf3a4e60423a7e7602e404ea1bb0191", aum = 0)
    /* renamed from: Zz */
    private Set<Station> m19157Zz() {
        if (m19178dG() == null) {
            return Collections.EMPTY_SET;
        }
        HashSet hashSet = new HashSet();
        ArrayList arrayList = new ArrayList();
        for (Node Nc : m19178dG().dyb().cXK()) {
            StellarSystem Nc2 = Nc.mo21606Nc();
            if (!arrayList.contains(Nc2)) {
                arrayList.add(Nc2);
                for (Node Zw : Nc2.getNodes()) {
                    for (Actor next : Zw.mo21624Zw()) {
                        if (next instanceof Station) {
                            hashSet.add((Station) next);
                        }
                    }
                }
            }
        }
        if (!m19184iK()) {
            Station bZC = ala().aJe().mo19006xF().bZC();
            Station bZE = ala().aJe().mo19006xF().bZE();
            hashSet.remove(bZC);
            hashSet.remove(bZE);
        }
        return hashSet;
    }

    @C0064Am(aul = "97823e273c1156ca35a75d1d51b36933", aum = 0)
    private C1556Wo<Station, ListContainer<BuyOrderInfo>> dBH() {
        return bmv();
    }

    @C0064Am(aul = "f42198c181b12452aed7e773697f7608", aum = 0)
    private C1556Wo<Station, ListContainer<SellOrderInfo>> dBJ() {
        return bmw();
    }

    @C0064Am(aul = "a9e1b882a93ea8ccfbdabd2d9f4e4e14", aum = 0)
    /* renamed from: a */
    private boolean m19162a(ItemLocation aag, Station bf) {
        Iterator<Item> iterator = aag.getIterator();
        Store azJ = bf.azJ();
        boolean z = false;
        while (iterator.hasNext()) {
            Item next = iterator.next();
            BuyOrder b = azJ.mo19301b(m19178dG(), next.bAP(), next.mo302Iq());
            if (b != null) {
                BuyOrderInfo sfVar = (BuyOrderInfo) b.mo9447fo();
                sfVar.push();
                ((ListContainer) bmv().get(bf)).add(sfVar);
            }
            z = true;
        }
        return z;
    }

    @C0064Am(aul = "4d361b6d964567fd9bdf923aa71e10bd", aum = 0)
    /* renamed from: k */
    private void m19186k(TaikopediaEntry aiz) {
        if (!dBz().containsKey(aiz)) {
            dBz().put(aiz, true);
            m19178dG().mo14419f((C1506WA) new C1549Wj(aiz));
        }
    }

    @C0064Am(aul = "0f1fd1516c5469850d774f2c2bb6df68", aum = 0)
    /* renamed from: fg */
    private void m19182fg() {
        m19159a((Player) null);
        bmv().clear();
        bmw().clear();
        dBy().clear();
        dBz().clear();
        super.dispose();
    }

    @C0064Am(aul = "f1fff14222d9a323ac53f9fda535b9f6", aum = 0)
    /* renamed from: f */
    private Set<aDJ> m19181f(C4068yr yrVar) {
        DataSetContainer dt = (DataSetContainer) dBA().get(yrVar);
        if (dt != null) {
            return Collections.unmodifiableSet(dt.aGn());
        }
        return null;
    }

    @C0064Am(aul = "5ca8091258ea47984b79caaa264cc4bd", aum = 0)
    /* renamed from: a */
    private void m19158a(C0665JT jt) {
        if (jt.mo3117j(1, 0, 0)) {
            bmv().clear();
            bmw().clear();
        }
    }

    @C0064Am(aul = "4942f83e9a4138cc824231d195dc1aa0", aum = 0)
    private Map<C4033yO, Boolean> dBP() {
        return Collections.unmodifiableMap(dBy());
    }

    @C0064Am(aul = "afbb19c83502f032d2f57b4cc08da420", aum = 0)
    private Map<C4033yO, Boolean> dBR() {
        return Collections.unmodifiableMap(dBz());
    }

    @C0064Am(aul = "23c579a03dc09e65f3b411b51f150bc0", aum = 0)
    private boolean dBT() {
        if (m19185iL() || aMU.FINISHED_NURSERY.equals(m19178dG().cXm())) {
            return false;
        }
        return true;
    }

    @C0064Am(aul = "dbd524d4e9155c89fa1ec8ae876626d0", aum = 0)
    private boolean adv() {
        return ala().aLS().mo22273iL();
    }
}
