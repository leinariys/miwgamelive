package game.script.player;

import game.network.message.externalizable.aCE;
import game.network.message.serializable.C1556Wo;
import game.script.TaikodomObject;
import logic.baa.C1616Xf;
import logic.baa.C1634Xv;
import logic.baa.C5511aMd;
import logic.baa.C6485anp;
import logic.data.mbean.C1252SW;
import logic.res.code.C5640aRc;
import logic.res.code.C5663aRz;
import logic.res.code.C6494any;
import logic.res.html.C2491fm;
import p001a.*;

import java.util.Collection;

@C5511aMd
@C6485anp
/* renamed from: a.aMR */
/* compiled from: a */
public class PlayerReservedNames extends TaikodomObject implements C1616Xf {
    public static final int _m_fieldCount = 0;
    public static final C5663aRz[] _m_fields = null;
    public static final int _m_methodCount = 0;
    public static final C2491fm[] _m_methods = null;
    public static final C5663aRz cZo = null;
    public static final C2491fm cZp = null;
    public static final C2491fm iqh = null;
    public static final C2491fm iqi = null;
    public static final long serialVersionUID = 0;
    public static C6494any ___iScriptClass;
    @C0064Am(aul = "5541f3d7f23ba0d6e9e214ae01ad6f19", aum = 0)
    private static C1556Wo<String, String> cZn;

    static {
        m16374V();
    }

    public PlayerReservedNames() {
        super((C5540aNg) null);
        super.mo10S();
    }

    public PlayerReservedNames(C5540aNg ang) {
        super(ang);
    }

    /* renamed from: V */
    static void m16374V() {
        serialVersionUID = (long) 1;
        _m_fieldCount = TaikodomObject._m_fieldCount + 1;
        _m_methodCount = TaikodomObject._m_methodCount + 3;
        int i = TaikodomObject._m_fieldCount;
        C5663aRz[] arzArr = new C5663aRz[(i + 1)];
        C5663aRz b = C5640aRc.m17844b(PlayerReservedNames.class, "5541f3d7f23ba0d6e9e214ae01ad6f19", i);
        cZo = b;
        arzArr[i] = b;
        int i2 = i + 1;
        _m_fields = arzArr;
        C1634Xv.m11725a((Object[]) TaikodomObject._m_fields, (Object[]) _m_fields);
        int i3 = TaikodomObject._m_methodCount;
        C2491fm[] fmVarArr = new C2491fm[(i3 + 3)];
        C2491fm a = C4105zY.m41624a(PlayerReservedNames.class, "194e51785e78389e120d4d4581c31bb3", i3);
        cZp = a;
        fmVarArr[i3] = a;
        int i4 = i3 + 1;
        C2491fm a2 = C4105zY.m41624a(PlayerReservedNames.class, "5840ac7f9b362cf7474232c2aec1a2ec", i4);
        iqh = a2;
        fmVarArr[i4] = a2;
        int i5 = i4 + 1;
        C2491fm a3 = C4105zY.m41624a(PlayerReservedNames.class, "862219ac11fc91d9d7f5aae88cc4c44c", i5);
        iqi = a3;
        fmVarArr[i5] = a3;
        int i6 = i5 + 1;
        _m_methods = fmVarArr;
        C1634Xv.m11725a((Object[]) TaikodomObject._m_methods, (Object[]) _m_methods);
        ___iScriptClass = aUO.m18566a(PlayerReservedNames.class, C1252SW.class, _m_fields, _m_methods);
    }

    /* renamed from: D */
    private void m16373D(C1556Wo wo) {
        bFf().mo5608dq().mo3197f(cZo, wo);
    }

    private C1556Wo aRj() {
        return (C1556Wo) bFf().mo5608dq().mo3214p(cZo);
    }

    /* renamed from: T */
    public C6494any mo11T() {
        return ___iScriptClass;
    }

    /* renamed from: U */
    public C2491fm[] mo12U() {
        return _m_methods;
    }

    /* renamed from: W */
    public Object mo13W() {
        return new C1252SW(this);
    }

    /* renamed from: a */
    public Object mo14a(C0495Gr gr) {
        Object[] args = gr.getArgs();
        switch (gr.mo2417hq() - TaikodomObject._m_methodCount) {
            case 0:
                return aRk();
            case 1:
                return new Boolean(m16375a((String) args[0], (User) args[1]));
            case 2:
                return new Boolean(m16376aW((String) args[0], (String) args[1]));
            default:
                return super.mo14a(gr);
        }
    }

    /* renamed from: a */
    public void mo15a(Collection collection, C0495Gr gr) {
        super.mo15a(collection, gr);
    }

    public C1556Wo<String, String> aRl() {
        switch (bFf().mo6893i(cZp)) {
            case 0:
                return null;
            case 2:
                return (C1556Wo) bFf().mo5606d(new aCE(this, cZp, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, cZp, new Object[0]));
                break;
        }
        return aRk();
    }

    /* renamed from: aX */
    public boolean mo10047aX(String str, String str2) {
        switch (bFf().mo6893i(iqi)) {
            case 0:
                return false;
            case 2:
                return ((Boolean) bFf().mo5606d(new aCE(this, iqi, new Object[]{str, str2}))).booleanValue();
            case 3:
                bFf().mo5606d(new aCE(this, iqi, new Object[]{str, str2}));
                break;
        }
        return m16376aW(str, str2);
    }

    /* renamed from: b */
    public boolean mo10048b(String str, User adk) {
        switch (bFf().mo6893i(iqh)) {
            case 0:
                return false;
            case 2:
                return ((Boolean) bFf().mo5606d(new aCE(this, iqh, new Object[]{str, adk}))).booleanValue();
            case 3:
                bFf().mo5606d(new aCE(this, iqh, new Object[]{str, adk}));
                break;
        }
        return m16375a(str, adk);
    }

    /* renamed from: c */
    public C5663aRz[] mo25c() {
        return _m_fields;
    }

    /* renamed from: S */
    public void mo10S() {
        super.mo10S();
    }

    @C0064Am(aul = "194e51785e78389e120d4d4581c31bb3", aum = 0)
    private C1556Wo<String, String> aRk() {
        return aRj();
    }

    @C0064Am(aul = "5840ac7f9b362cf7474232c2aec1a2ec", aum = 0)
    /* renamed from: a */
    private boolean m16375a(String str, User adk) {
        if (adk == null) {
            return true;
        }
        String str2 = (String) aRj().get(str);
        if (str2 == null) {
            return true;
        }
        if (adk.getName().equals(str2)) {
            return true;
        }
        return false;
    }

    @C0064Am(aul = "862219ac11fc91d9d7f5aae88cc4c44c", aum = 0)
    /* renamed from: aW */
    private boolean m16376aW(String str, String str2) {
        if (str2 == null || str == null || str2.length() == 0 || str.length() == 0 || aRj().get(str) != null) {
            return false;
        }
        aRj().put(str, str2);
        return true;
    }
}
