package game.script.player;

import game.network.channel.client.ClientConnectImpl;
import game.network.message.externalizable.aCE;
import game.network.message.serializable.C3438ra;
import game.script.TaikodomObject;
import game.script.bank.BankAccount;
import game.script.billing.Billing;
import game.script.login.UserConnection;
import logic.baa.*;
import logic.bbb.aDR;
import logic.data.link.C4078z;
import logic.data.mbean.C5229aBh;
import logic.res.code.C5640aRc;
import logic.res.code.C5663aRz;
import logic.res.code.C6494any;
import logic.res.html.C2491fm;
import logic.sql.C2697ik;
import logic.sql.C3544sP;
import p001a.*;

import java.io.File;
import java.util.Collection;

@C5829abJ("1.0.0")
@C6485anp
@C2499fr(mo18855qf = {"72b31763fe0c6097fd1ec34914576237"})
@C5511aMd
/* renamed from: a.aDk  reason: case insensitive filesystem */
/* compiled from: a */
public class User extends TaikodomObject implements C1165RF, C1616Xf, aOW {

    /* renamed from: Do */
    public static final C2491fm f2575Do = null;

    /* renamed from: Pf */
    public static final C2491fm f2576Pf = null;
    public static final C2491fm _f_isDeleted_0020_0028_0029Z = null;
    public static final C2491fm _f_onResurrect_0020_0028_0029V = null;
    public static final C2491fm _f_toString_0020_0028_0029Ljava_002flang_002fString_003b = null;
    public static final int _m_fieldCount = 0;
    public static final C5663aRz[] _m_fields = null;
    public static final int _m_methodCount = 0;
    public static final C2491fm[] _m_methods = null;
    public static final C2491fm aJA = null;
    public static final C2491fm aJB = null;
    public static final C2491fm aJz = null;
    public static final C2491fm aKm = null;
    public static final C5663aRz cnX = null;
    public static final C5663aRz eVZ = null;
    public static final C2491fm eWG = null;
    public static final C2491fm eWH = null;
    public static final C2491fm fav = null;
    public static final C2491fm gVP = null;
    public static final C5663aRz hyA = null;
    public static final C5663aRz hyB = null;
    public static final C5663aRz hyC = null;
    public static final C5663aRz hyD = null;
    public static final C5663aRz hyE = null;
    public static final C5663aRz hyF = null;
    public static final C5663aRz hyG = null;
    public static final C5663aRz hyH = null;
    public static final C5663aRz hyI = null;
    public static final C5663aRz hyJ = null;
    public static final C5663aRz hyK = null;
    public static final C5663aRz hyL = null;
    public static final C5663aRz hyM = null;
    public static final C5663aRz hyN = null;
    public static final C5663aRz hyO = null;
    public static final C2491fm hyP = null;
    public static final C2491fm hyQ = null;
    public static final C2491fm hyR = null;
    public static final C2491fm hyS = null;
    public static final C2491fm hyT = null;
    public static final C2491fm hyU = null;
    public static final C2491fm hyV = null;
    public static final C2491fm hyW = null;
    public static final C2491fm hyX = null;
    public static final C2491fm hyY = null;
    public static final C2491fm hyZ = null;
    public static final int hyw = 6;
    public static final C5663aRz hyx = null;
    public static final C5663aRz hyy = null;
    public static final C5663aRz hyz = null;
    public static final C2491fm hzA = null;
    public static final C2491fm hzB = null;
    public static final C2491fm hzC = null;
    public static final C2491fm hzD = null;
    public static final C2491fm hzE = null;
    public static final C2491fm hzF = null;
    public static final C2491fm hzG = null;
    public static final C2491fm hzH = null;
    public static final C2491fm hzI = null;
    public static final C2491fm hzJ = null;
    public static final C2491fm hza = null;
    public static final C2491fm hzb = null;
    public static final C2491fm hzc = null;
    public static final C2491fm hzd = null;
    public static final C2491fm hze = null;
    public static final C2491fm hzf = null;
    public static final C2491fm hzg = null;
    public static final C2491fm hzh = null;
    public static final C2491fm hzi = null;
    public static final C2491fm hzj = null;
    public static final C2491fm hzk = null;
    public static final C2491fm hzl = null;
    public static final C2491fm hzm = null;
    public static final C2491fm hzn = null;
    public static final C2491fm hzo = null;
    public static final C2491fm hzp = null;
    public static final C2491fm hzq = null;
    public static final C2491fm hzr = null;
    public static final C2491fm hzs = null;
    public static final C2491fm hzt = null;
    public static final C2491fm hzu = null;
    public static final C2491fm hzv = null;
    public static final C2491fm hzw = null;
    public static final C2491fm hzx = null;
    public static final C2491fm hzy = null;
    public static final C2491fm hzz = null;
    public static final long serialVersionUID = 0;
    private static final String hyv = "none of your business";
    public static C6494any ___iScriptClass = null;
    @C0064Am(aul = "3ce1115aad0c5f63e1110d8688b23274", aum = 6)
    private static C3438ra<Player> cYy = null;
    @C0064Am(aul = "a9c1459387a752cb732d8c4f91be05aa", aum = 8)
    private static boolean eVY = false;
    @C0064Am(aul = "6b882cb0636b30b1278398ea6222a893", aum = 18)
    private static int fGU = 0;
    @C0064Am(aul = "92eef0bb4ca256bf07033afe0977f334", aum = 17)
    private static Billing fpQ = null;
    @C0064Am(aul = "6ab6d277e86d2c195da2e5a74f117e11", aum = 1)
    private static String fullName = null;
    @C0064Am(aul = "e7d649d4761a2bb38d923fe770538715", aum = 0)
    private static BankAccount hgb = null;
    @C0064Am(aul = "776351cb3f22be39cb86d9987863d453", aum = 3)
    private static String hgc = null;
    @C0064Am(aul = "3990f9fcad3ecff8b5fe3f7b08212f7d", aum = 4)
    private static String hgd = null;
    @C0064Am(aul = "5944d67a5b7e2d0155d7871bd20c6b4d", aum = 7)
    private static boolean hge = false;
    @C0064Am(aul = "4a2d3b5a9beefa9e632d68981047863f", aum = 9)
    private static boolean hgf = false;
    @C0064Am(aul = "62e8613885da8ac811e46ad1ebe7d63b", aum = 10)
    private static boolean hgg = false;
    @C0064Am(aul = "c50de7b42a9365d1d1119d3f4c9b88d3", aum = 11)
    private static long hgh = 0;
    @C0064Am(aul = "b2a596f78acc7073a7a9775a13358a15", aum = 12)
    private static String hgi = null;
    @C0064Am(aul = "e75dcbcd13e997dab763e1b827371cea", aum = 13)
    @C5566aOg
    private static C6283ajv hgj = null;
    @C0064Am(aul = "3871935cfa2dee14c2d6b7d8845cc57c", aum = 14)
    private static String hgk = null;
    @C0064Am(aul = "e9641e137742e6191472a2f1c0297002", aum = 15)
    private static String hgl = null;
    @C0064Am(aul = "a1e115cc6fa83796cb051ff38c9d7686", aum = 16)
    private static UserConnection hgm = null;
    @C0064Am(aul = "1d72bf8844bc9971603b0435cafc7330", aum = 19)
    private static long hgn = 0;
    @C0064Am(aul = "bc6c473d24bb1cf2e71051ce8302192b", aum = 5)
    private static String password;
    @C0064Am(aul = "b5a2169f073420f1d8f004d07091c071", aum = 2)
    private static String username;

    static {
        m13752V();
    }

    public User() {
        super((C5540aNg) null);
        super.mo10S();
    }

    public User(C5540aNg ang) {
        super(ang);
    }

    /* renamed from: V */
    static void m13752V() {
        serialVersionUID = (long) 1;
        _m_fieldCount = TaikodomObject._m_fieldCount + 20;
        _m_methodCount = TaikodomObject._m_methodCount + 60;
        int i = TaikodomObject._m_fieldCount;
        C5663aRz[] arzArr = new C5663aRz[(i + 20)];
        C5663aRz b = C5640aRc.m17844b(User.class, "e7d649d4761a2bb38d923fe770538715", i);
        hyx = b;
        arzArr[i] = b;
        int i2 = i + 1;
        C5663aRz b2 = C5640aRc.m17844b(User.class, "6ab6d277e86d2c195da2e5a74f117e11", i2);
        hyy = b2;
        arzArr[i2] = b2;
        int i3 = i2 + 1;
        C5663aRz b3 = C5640aRc.m17844b(User.class, "b5a2169f073420f1d8f004d07091c071", i3);
        hyz = b3;
        arzArr[i3] = b3;
        int i4 = i3 + 1;
        C5663aRz b4 = C5640aRc.m17844b(User.class, "776351cb3f22be39cb86d9987863d453", i4);
        hyA = b4;
        arzArr[i4] = b4;
        int i5 = i4 + 1;
        C5663aRz b5 = C5640aRc.m17844b(User.class, "3990f9fcad3ecff8b5fe3f7b08212f7d", i5);
        hyB = b5;
        arzArr[i5] = b5;
        int i6 = i5 + 1;
        C5663aRz b6 = C5640aRc.m17844b(User.class, "bc6c473d24bb1cf2e71051ce8302192b", i6);
        hyC = b6;
        arzArr[i6] = b6;
        int i7 = i6 + 1;
        C5663aRz b7 = C5640aRc.m17844b(User.class, "3ce1115aad0c5f63e1110d8688b23274", i7);
        cnX = b7;
        arzArr[i7] = b7;
        int i8 = i7 + 1;
        C5663aRz b8 = C5640aRc.m17844b(User.class, "5944d67a5b7e2d0155d7871bd20c6b4d", i8);
        hyD = b8;
        arzArr[i8] = b8;
        int i9 = i8 + 1;
        C5663aRz b9 = C5640aRc.m17844b(User.class, "a9c1459387a752cb732d8c4f91be05aa", i9);
        eVZ = b9;
        arzArr[i9] = b9;
        int i10 = i9 + 1;
        C5663aRz b10 = C5640aRc.m17844b(User.class, "4a2d3b5a9beefa9e632d68981047863f", i10);
        hyE = b10;
        arzArr[i10] = b10;
        int i11 = i10 + 1;
        C5663aRz b11 = C5640aRc.m17844b(User.class, "62e8613885da8ac811e46ad1ebe7d63b", i11);
        hyF = b11;
        arzArr[i11] = b11;
        int i12 = i11 + 1;
        C5663aRz b12 = C5640aRc.m17844b(User.class, "c50de7b42a9365d1d1119d3f4c9b88d3", i12);
        hyG = b12;
        arzArr[i12] = b12;
        int i13 = i12 + 1;
        C5663aRz b13 = C5640aRc.m17844b(User.class, "b2a596f78acc7073a7a9775a13358a15", i13);
        hyH = b13;
        arzArr[i13] = b13;
        int i14 = i13 + 1;
        C5663aRz b14 = C5640aRc.m17844b(User.class, "e75dcbcd13e997dab763e1b827371cea", i14);
        hyI = b14;
        arzArr[i14] = b14;
        int i15 = i14 + 1;
        C5663aRz b15 = C5640aRc.m17844b(User.class, "3871935cfa2dee14c2d6b7d8845cc57c", i15);
        hyJ = b15;
        arzArr[i15] = b15;
        int i16 = i15 + 1;
        C5663aRz b16 = C5640aRc.m17844b(User.class, "e9641e137742e6191472a2f1c0297002", i16);
        hyK = b16;
        arzArr[i16] = b16;
        int i17 = i16 + 1;
        C5663aRz b17 = C5640aRc.m17844b(User.class, "a1e115cc6fa83796cb051ff38c9d7686", i17);
        hyL = b17;
        arzArr[i17] = b17;
        int i18 = i17 + 1;
        C5663aRz b18 = C5640aRc.m17844b(User.class, "92eef0bb4ca256bf07033afe0977f334", i18);
        hyM = b18;
        arzArr[i18] = b18;
        int i19 = i18 + 1;
        C5663aRz b19 = C5640aRc.m17844b(User.class, "6b882cb0636b30b1278398ea6222a893", i19);
        hyN = b19;
        arzArr[i19] = b19;
        int i20 = i19 + 1;
        C5663aRz b20 = C5640aRc.m17844b(User.class, "1d72bf8844bc9971603b0435cafc7330", i20);
        hyO = b20;
        arzArr[i20] = b20;
        int i21 = i20 + 1;
        _m_fields = arzArr;
        C1634Xv.m11725a((Object[]) TaikodomObject._m_fields, (Object[]) _m_fields);
        int i22 = TaikodomObject._m_methodCount;
        C2491fm[] fmVarArr = new C2491fm[(i22 + 60)];
        C2491fm a = C4105zY.m41624a(User.class, C4078z.f9620bZ, i22);
        hyP = a;
        fmVarArr[i22] = a;
        int i23 = i22 + 1;
        C2491fm a2 = C4105zY.m41624a(User.class, "d84557b5bfebe927c6749591ea08da9f", i23);
        hyQ = a2;
        fmVarArr[i23] = a2;
        int i24 = i23 + 1;
        C2491fm a3 = C4105zY.m41624a(User.class, "298b6fab19efba960d3d0f77b6816f1e", i24);
        hyR = a3;
        fmVarArr[i24] = a3;
        int i25 = i24 + 1;
        C2491fm a4 = C4105zY.m41624a(User.class, "6bf8a0df3f4f5a45e3ba96cf02cb4eef", i25);
        hyS = a4;
        fmVarArr[i25] = a4;
        int i26 = i25 + 1;
        C2491fm a5 = C4105zY.m41624a(User.class, "796a257c3182154b5eb8e67f3324d502", i26);
        hyT = a5;
        fmVarArr[i26] = a5;
        int i27 = i26 + 1;
        C2491fm a6 = C4105zY.m41624a(User.class, "a9b7ecad121239228e156784daaf20eb", i27);
        hyU = a6;
        fmVarArr[i27] = a6;
        int i28 = i27 + 1;
        C2491fm a7 = C4105zY.m41624a(User.class, "a13e8d74f643f8a3c67e322fa30a5a8d", i28);
        hyV = a7;
        fmVarArr[i28] = a7;
        int i29 = i28 + 1;
        C2491fm a8 = C4105zY.m41624a(User.class, "885d56b255a55efe097789cd2709b48f", i29);
        _f_toString_0020_0028_0029Ljava_002flang_002fString_003b = a8;
        fmVarArr[i29] = a8;
        int i30 = i29 + 1;
        C2491fm a9 = C4105zY.m41624a(User.class, "e81a49fc21fd716f204a9f23ee5abceb", i30);
        hyW = a9;
        fmVarArr[i30] = a9;
        int i31 = i30 + 1;
        C2491fm a10 = C4105zY.m41624a(User.class, "82df6c36e75958eff133e4c81e035e01", i31);
        hyX = a10;
        fmVarArr[i31] = a10;
        int i32 = i31 + 1;
        C2491fm a11 = C4105zY.m41624a(User.class, "02ecefb64f645195627673ffc6446a59", i32);
        hyY = a11;
        fmVarArr[i32] = a11;
        int i33 = i32 + 1;
        C2491fm a12 = C4105zY.m41624a(User.class, "1b2cf2b10c15dd73a052621d4d5a2a6f", i33);
        hyZ = a12;
        fmVarArr[i33] = a12;
        int i34 = i33 + 1;
        C2491fm a13 = C4105zY.m41624a(User.class, "c699272ed725d05aaadafe2e80dd8c86", i34);
        hza = a13;
        fmVarArr[i34] = a13;
        int i35 = i34 + 1;
        C2491fm a14 = C4105zY.m41624a(User.class, "8d72fa9c9644514463c3333717ec7658", i35);
        hzb = a14;
        fmVarArr[i35] = a14;
        int i36 = i35 + 1;
        C2491fm a15 = C4105zY.m41624a(User.class, "b3f9c70e2059ea9dd954170bde12145e", i36);
        hzc = a15;
        fmVarArr[i36] = a15;
        int i37 = i36 + 1;
        C2491fm a16 = C4105zY.m41624a(User.class, "7983acbc9fe1f3e043097c4561f1a909", i37);
        hzd = a16;
        fmVarArr[i37] = a16;
        int i38 = i37 + 1;
        C2491fm a17 = C4105zY.m41624a(User.class, "a874950881ac65f1b397151cc89419d3", i38);
        hze = a17;
        fmVarArr[i38] = a17;
        int i39 = i38 + 1;
        C2491fm a18 = C4105zY.m41624a(User.class, "4839dd357c5e3fcd6a8eeacbd8d06855", i39);
        hzf = a18;
        fmVarArr[i39] = a18;
        int i40 = i39 + 1;
        C2491fm a19 = C4105zY.m41624a(User.class, "48d8740d7ae4b236f22039f5f7fe86b2", i40);
        hzg = a19;
        fmVarArr[i40] = a19;
        int i41 = i40 + 1;
        C2491fm a20 = C4105zY.m41624a(User.class, "4d9378bfa3e6bec6e1e8d3d2be33d9ad", i41);
        eWG = a20;
        fmVarArr[i41] = a20;
        int i42 = i41 + 1;
        C2491fm a21 = C4105zY.m41624a(User.class, "d205e3019df456a2294389e2a084db69", i42);
        eWH = a21;
        fmVarArr[i42] = a21;
        int i43 = i42 + 1;
        C2491fm a22 = C4105zY.m41624a(User.class, "fe73cc6aed80f9742424b4d54e645ed6", i43);
        hzh = a22;
        fmVarArr[i43] = a22;
        int i44 = i43 + 1;
        C2491fm a23 = C4105zY.m41624a(User.class, "1a212fb8394410313dc001670c54a4c4", i44);
        hzi = a23;
        fmVarArr[i44] = a23;
        int i45 = i44 + 1;
        C2491fm a24 = C4105zY.m41624a(User.class, "7554f7b5fedc89ae796e8bc396466156", i45);
        hzj = a24;
        fmVarArr[i45] = a24;
        int i46 = i45 + 1;
        C2491fm a25 = C4105zY.m41624a(User.class, "a3b4d92ae9142309201ce62cff5c7b10", i46);
        hzk = a25;
        fmVarArr[i46] = a25;
        int i47 = i46 + 1;
        C2491fm a26 = C4105zY.m41624a(User.class, "ff273613bbca5cdd65023019386e3782", i47);
        hzl = a26;
        fmVarArr[i47] = a26;
        int i48 = i47 + 1;
        C2491fm a27 = C4105zY.m41624a(User.class, "b5840c5b1aa6efe9dd04bb10666439aa", i48);
        _f_onResurrect_0020_0028_0029V = a27;
        fmVarArr[i48] = a27;
        int i49 = i48 + 1;
        C2491fm a28 = C4105zY.m41624a(User.class, "9c6f21692302ee8342336f14d68b009a", i49);
        gVP = a28;
        fmVarArr[i49] = a28;
        int i50 = i49 + 1;
        C2491fm a29 = C4105zY.m41624a(User.class, "a293ebb6458ada435157646faa19d6ca", i50);
        hzm = a29;
        fmVarArr[i50] = a29;
        int i51 = i50 + 1;
        C2491fm a30 = C4105zY.m41624a(User.class, "f9fd68fe1f380bd4025820c9217dcc39", i51);
        hzn = a30;
        fmVarArr[i51] = a30;
        int i52 = i51 + 1;
        C2491fm a31 = C4105zY.m41624a(User.class, "d9182253edc00cb6d54444d5500e4d75", i52);
        hzo = a31;
        fmVarArr[i52] = a31;
        int i53 = i52 + 1;
        C2491fm a32 = C4105zY.m41624a(User.class, "08c223254fd0f4c7a10a2f156d6f8e00", i53);
        hzp = a32;
        fmVarArr[i53] = a32;
        int i54 = i53 + 1;
        C2491fm a33 = C4105zY.m41624a(User.class, "dff3b702621f7ff82fdbfa6efb42599a", i54);
        hzq = a33;
        fmVarArr[i54] = a33;
        int i55 = i54 + 1;
        C2491fm a34 = C4105zY.m41624a(User.class, "0e615feb4d5d60cfa15c56d63e4eb80b", i55);
        f2575Do = a34;
        fmVarArr[i55] = a34;
        int i56 = i55 + 1;
        C2491fm a35 = C4105zY.m41624a(User.class, "204873e53a3d8054851286f52fe188e4", i56);
        fav = a35;
        fmVarArr[i56] = a35;
        int i57 = i56 + 1;
        C2491fm a36 = C4105zY.m41624a(User.class, "e129e3a1ee1c30e8114a5152847c03d0", i57);
        hzr = a36;
        fmVarArr[i57] = a36;
        int i58 = i57 + 1;
        C2491fm a37 = C4105zY.m41624a(User.class, "131fbb0aabd3dbebf57e5ae947369d3d", i58);
        hzs = a37;
        fmVarArr[i58] = a37;
        int i59 = i58 + 1;
        C2491fm a38 = C4105zY.m41624a(User.class, "c5a090251f8779e544d2e14b0b96ee7f", i59);
        hzt = a38;
        fmVarArr[i59] = a38;
        int i60 = i59 + 1;
        C2491fm a39 = C4105zY.m41624a(User.class, "b80ed300c8b6b7a52bfd17728e7b0bf0", i60);
        hzu = a39;
        fmVarArr[i60] = a39;
        int i61 = i60 + 1;
        C2491fm a40 = C4105zY.m41624a(User.class, "9ddb19b592e8b7afd1ff76ae92a4de8e", i61);
        hzv = a40;
        fmVarArr[i61] = a40;
        int i62 = i61 + 1;
        C2491fm a41 = C4105zY.m41624a(User.class, "4a642250e8d5bf2e6c8e0687c2436e53", i62);
        hzw = a41;
        fmVarArr[i62] = a41;
        int i63 = i62 + 1;
        C2491fm a42 = C4105zY.m41624a(User.class, "16bfeaa0b0169685bdf329f20224a107", i63);
        hzx = a42;
        fmVarArr[i63] = a42;
        int i64 = i63 + 1;
        C2491fm a43 = C4105zY.m41624a(User.class, "1342f04e8a1c7b6b5b7085b6013d8b9f", i64);
        hzy = a43;
        fmVarArr[i64] = a43;
        int i65 = i64 + 1;
        C2491fm a44 = C4105zY.m41624a(User.class, "5578e9fbfb2c66c3829f80df08a310d4", i65);
        hzz = a44;
        fmVarArr[i65] = a44;
        int i66 = i65 + 1;
        C2491fm a45 = C4105zY.m41624a(User.class, "7057dcb772fc98a01460ac84b1a85033", i66);
        hzA = a45;
        fmVarArr[i66] = a45;
        int i67 = i66 + 1;
        C2491fm a46 = C4105zY.m41624a(User.class, "f36594e47a26b6ffe06ed1c6fc16d6a0", i67);
        _f_isDeleted_0020_0028_0029Z = a46;
        fmVarArr[i67] = a46;
        int i68 = i67 + 1;
        C2491fm a47 = C4105zY.m41624a(User.class, "1f3c98be219175c9f1514399f2f7c610", i68);
        hzB = a47;
        fmVarArr[i68] = a47;
        int i69 = i68 + 1;
        C2491fm a48 = C4105zY.m41624a(User.class, "fe0950d7b7b950a00d59e045b57b0d70", i69);
        aJz = a48;
        fmVarArr[i69] = a48;
        int i70 = i69 + 1;
        C2491fm a49 = C4105zY.m41624a(User.class, "dca6753cbe681c772a2ac785667e652a", i70);
        f2576Pf = a49;
        fmVarArr[i70] = a49;
        int i71 = i70 + 1;
        C2491fm a50 = C4105zY.m41624a(User.class, "704d9af17c1100ea0e653d4f6d2a819a", i71);
        aJA = a50;
        fmVarArr[i71] = a50;
        int i72 = i71 + 1;
        C2491fm a51 = C4105zY.m41624a(User.class, "46d956aebbe659f747e54e051a022704", i72);
        aJB = a51;
        fmVarArr[i72] = a51;
        int i73 = i72 + 1;
        C2491fm a52 = C4105zY.m41624a(User.class, "946b0af37e96849aa5ed0e89913ed301", i73);
        hzC = a52;
        fmVarArr[i73] = a52;
        int i74 = i73 + 1;
        C2491fm a53 = C4105zY.m41624a(User.class, "b413f96072787ae56a562912e4c5da11", i74);
        hzD = a53;
        fmVarArr[i74] = a53;
        int i75 = i74 + 1;
        C2491fm a54 = C4105zY.m41624a(User.class, "8f84b6a65e0e143a759acaa66acb8acd", i75);
        hzE = a54;
        fmVarArr[i75] = a54;
        int i76 = i75 + 1;
        C2491fm a55 = C4105zY.m41624a(User.class, "ee7b343c6e48972df381df52c4a041b6", i76);
        hzF = a55;
        fmVarArr[i76] = a55;
        int i77 = i76 + 1;
        C2491fm a56 = C4105zY.m41624a(User.class, "b5cce2c4d3c446e72f8e494c78d80b88", i77);
        hzG = a56;
        fmVarArr[i77] = a56;
        int i78 = i77 + 1;
        C2491fm a57 = C4105zY.m41624a(User.class, "524d0e007db1db4dd073a4e899da3767", i78);
        hzH = a57;
        fmVarArr[i78] = a57;
        int i79 = i78 + 1;
        C2491fm a58 = C4105zY.m41624a(User.class, "1bf959882c87839a1af3ffe8e7bb04a9", i79);
        hzI = a58;
        fmVarArr[i79] = a58;
        int i80 = i79 + 1;
        C2491fm a59 = C4105zY.m41624a(User.class, "1f269c10fa8115d93984d1dd1fbe3306", i80);
        hzJ = a59;
        fmVarArr[i80] = a59;
        int i81 = i80 + 1;
        C2491fm a60 = C4105zY.m41624a(User.class, "52acff50eac255776387973c274b4c3d", i81);
        aKm = a60;
        fmVarArr[i81] = a60;
        int i82 = i81 + 1;
        _m_methods = fmVarArr;
        C1634Xv.m11725a((Object[]) TaikodomObject._m_methods, (Object[]) _m_methods);
        ___iScriptClass = aUO.m18566a(User.class, C5229aBh.class, _m_fields, _m_methods);
    }

    public static String cTt() {
        String a = C5673aSj.m18227a("http://www.taikodom.com.br/end_user_license_in_game_v", (String) null, new File("data/web-cache/"));
        return a == null ? "" : a;
    }

    public static String cTu() {
        String a = C5673aSj.m18227a("http://www.taikodom.com.br/terms_of_use_in_game_v", (String) null, new File("data/web-cache/"));
        return a == null ? "" : a;
    }

    /* renamed from: D */
    private void m13749D(User adk) {
        switch (bFf().mo6893i(hzs)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, hzs, new Object[]{adk}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, hzs, new Object[]{adk}));
                break;
        }
        m13748C(adk);
    }

    @C0064Am(aul = "52acff50eac255776387973c274b4c3d", aum = 0)
    @C5566aOg
    /* renamed from: QR */
    private void m13750QR() {
        throw new aWi(new aCE(this, aKm, new Object[0]));
    }

    /* renamed from: a */
    private void m13754a(Billing ace) {
        bFf().mo5608dq().mo3197f(hyM, ace);
    }

    /* renamed from: a */
    private void m13755a(C6283ajv ajv) {
        bFf().mo5608dq().mo3197f(hyI, ajv);
    }

    @C0064Am(aul = "b3f9c70e2059ea9dd954170bde12145e", aum = 0)
    @C5566aOg
    /* renamed from: aM */
    private void m13758aM(String str, String str2) {
        throw new aWi(new aCE(this, hzc, new Object[]{str, str2}));
    }

    private C3438ra aQN() {
        return (C3438ra) bFf().mo5608dq().mo3214p(cnX);
    }

    /* renamed from: aZ */
    private void m13759aZ(C3438ra raVar) {
        bFf().mo5608dq().mo3197f(cnX, raVar);
    }

    /* renamed from: b */
    private void m13762b(BankAccount ado) {
        bFf().mo5608dq().mo3197f(hyx, ado);
    }

    @C0064Am(aul = "d9182253edc00cb6d54444d5500e4d75", aum = 0)
    @C5566aOg
    /* renamed from: b */
    private void m13763b(C6283ajv ajv) {
        throw new aWi(new aCE(this, hzo, new Object[]{ajv}));
    }

    private boolean bLJ() {
        return bFf().mo5608dq().mo3201h(eVZ);
    }

    private C6283ajv cSA() {
        return (C6283ajv) bFf().mo5608dq().mo3214p(hyI);
    }

    private String cSB() {
        return (String) bFf().mo5608dq().mo3214p(hyJ);
    }

    private String cSC() {
        return (String) bFf().mo5608dq().mo3214p(hyK);
    }

    private UserConnection cSD() {
        return (UserConnection) bFf().mo5608dq().mo3214p(hyL);
    }

    private Billing cSE() {
        return (Billing) bFf().mo5608dq().mo3214p(hyM);
    }

    private int cSF() {
        return bFf().mo5608dq().mo3212n(hyN);
    }

    private long cSG() {
        return bFf().mo5608dq().mo3213o(hyO);
    }

    @C0064Am(aul = "a9b7ecad121239228e156784daaf20eb", aum = 0)
    @C5566aOg
    @C2499fr
    private Player cSJ() {
        throw new aWi(new aCE(this, hyU, new Object[0]));
    }

    private BankAccount cSp() {
        return (BankAccount) bFf().mo5608dq().mo3214p(hyx);
    }

    private String cSq() {
        return (String) bFf().mo5608dq().mo3214p(hyy);
    }

    private String cSr() {
        return (String) bFf().mo5608dq().mo3214p(hyz);
    }

    private String cSs() {
        return (String) bFf().mo5608dq().mo3214p(hyA);
    }

    private String cSt() {
        return (String) bFf().mo5608dq().mo3214p(hyB);
    }

    private String cSu() {
        return (String) bFf().mo5608dq().mo3214p(hyC);
    }

    private boolean cSv() {
        return bFf().mo5608dq().mo3201h(hyD);
    }

    private boolean cSw() {
        return bFf().mo5608dq().mo3201h(hyE);
    }

    private boolean cSx() {
        return bFf().mo5608dq().mo3201h(hyF);
    }

    private long cSy() {
        return bFf().mo5608dq().mo3213o(hyG);
    }

    private String cSz() {
        return (String) bFf().mo5608dq().mo3214p(hyH);
    }

    @C0064Am(aul = "f9fd68fe1f380bd4025820c9217dcc39", aum = 0)
    @C5566aOg
    @C2499fr
    private C6283ajv cTb() {
        throw new aWi(new aCE(this, hzn, new Object[0]));
    }

    @C0064Am(aul = "a874950881ac65f1b397151cc89419d3", aum = 0)
    @C5566aOg
    @C2499fr
    /* renamed from: cg */
    private void m13765cg(Player aku) {
        throw new aWi(new aCE(this, hze, new Object[]{aku}));
    }

    /* renamed from: d */
    private void m13768d(BankAccount ado) {
        switch (bFf().mo6893i(hzD)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, hzD, new Object[]{ado}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, hzD, new Object[]{ado}));
                break;
        }
        m13764c(ado);
    }

    /* renamed from: dQ */
    private void m13769dQ(boolean z) {
        bFf().mo5608dq().mo3153a(eVZ, z);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Activated")
    @C0064Am(aul = "d205e3019df456a2294389e2a084db69", aum = 0)
    @C5566aOg
    /* renamed from: dR */
    private void m13770dR(boolean z) {
        throw new aWi(new aCE(this, eWH, new Object[]{new Boolean(z)}));
    }

    /* renamed from: iF */
    private void m13771iF(boolean z) {
        bFf().mo5608dq().mo3153a(hyD, z);
    }

    /* renamed from: iG */
    private void m13772iG(boolean z) {
        bFf().mo5608dq().mo3153a(hyE, z);
    }

    /* renamed from: iH */
    private void m13773iH(boolean z) {
        bFf().mo5608dq().mo3153a(hyF, z);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Idqd")
    @C0064Am(aul = "48d8740d7ae4b236f22039f5f7fe86b2", aum = 0)
    @C5566aOg
    /* renamed from: iI */
    private void m13774iI(boolean z) {
        throw new aWi(new aCE(this, hzg, new Object[]{new Boolean(z)}));
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Banned")
    @C0064Am(aul = "7554f7b5fedc89ae796e8bc396466156", aum = 0)
    @C5566aOg
    /* renamed from: iK */
    private void m13775iK(boolean z) {
        throw new aWi(new aCE(this, hzj, new Object[]{new Boolean(z)}));
    }

    @C0064Am(aul = "1f3c98be219175c9f1514399f2f7c610", aum = 0)
    @C5566aOg
    /* renamed from: iM */
    private void m13776iM(boolean z) {
        throw new aWi(new aCE(this, hzB, new Object[]{new Boolean(z)}));
    }

    /* renamed from: j */
    private void m13777j(UserConnection apk) {
        bFf().mo5608dq().mo3197f(hyL, apk);
    }

    /* renamed from: kn */
    private void m13778kn(long j) {
        bFf().mo5608dq().mo3184b(hyG, j);
    }

    /* renamed from: ko */
    private void m13779ko(long j) {
        bFf().mo5608dq().mo3184b(hyO, j);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "BannedUntil")
    @C0064Am(aul = "16bfeaa0b0169685bdf329f20224a107", aum = 0)
    @C5566aOg
    /* renamed from: kp */
    private void m13780kp(long j) {
        throw new aWi(new aCE(this, hzx, new Object[]{new Long(j)}));
    }

    @C0064Am(aul = "1f269c10fa8115d93984d1dd1fbe3306", aum = 0)
    @C5566aOg
    /* renamed from: kr */
    private void m13781kr(long j) {
        throw new aWi(new aCE(this, hzJ, new Object[]{new Long(j)}));
    }

    /* renamed from: lA */
    private void m13782lA(String str) {
        bFf().mo5608dq().mo3197f(hyA, str);
    }

    /* renamed from: lB */
    private void m13783lB(String str) {
        bFf().mo5608dq().mo3197f(hyB, str);
    }

    /* renamed from: lC */
    private void m13784lC(String str) {
        bFf().mo5608dq().mo3197f(hyC, str);
    }

    /* renamed from: lD */
    private void m13785lD(String str) {
        bFf().mo5608dq().mo3197f(hyH, str);
    }

    /* renamed from: lE */
    private void m13786lE(String str) {
        bFf().mo5608dq().mo3197f(hyJ, str);
    }

    /* renamed from: lF */
    private void m13787lF(String str) {
        bFf().mo5608dq().mo3197f(hyK, str);
    }

    @C0064Am(aul = "d84557b5bfebe927c6749591ea08da9f", aum = 0)
    @C5566aOg
    @C0909NL
    /* renamed from: lG */
    private boolean m13788lG(String str) {
        throw new aWi(new aCE(this, hyQ, new Object[]{str}));
    }

    @C0064Am(aul = "796a257c3182154b5eb8e67f3324d502", aum = 0)
    @C5566aOg
    @C2499fr
    /* renamed from: lI */
    private Player m13789lI(String str) {
        throw new aWi(new aCE(this, hyT, new Object[]{str}));
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Name")
    @C0064Am(aul = "82df6c36e75958eff133e4c81e035e01", aum = 0)
    @C5566aOg
    /* renamed from: lK */
    private void m13790lK(String str) {
        throw new aWi(new aCE(this, hyX, new Object[]{str}));
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Username")
    @C0064Am(aul = "1b2cf2b10c15dd73a052621d4d5a2a6f", aum = 0)
    @C5566aOg
    /* renamed from: lM */
    private void m13791lM(String str) {
        throw new aWi(new aCE(this, hyZ, new Object[]{str}));
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Password")
    @C0064Am(aul = "8d72fa9c9644514463c3333717ec7658", aum = 0)
    @C5566aOg
    /* renamed from: lN */
    private void m13792lN(String str) {
        throw new aWi(new aCE(this, hzb, new Object[]{str}));
    }

    @C4034yP
    @C5566aOg
    @C0064Am(aul = "7057dcb772fc98a01460ac84b1a85033", aum = 0)
    @C2499fr
    /* renamed from: lO */
    private void m13793lO(String str) {
        throw new aWi(new aCE(this, hzA, new Object[]{str}));
    }

    @C4034yP
    @C5566aOg
    @C0064Am(aul = "8f84b6a65e0e143a759acaa66acb8acd", aum = 0)
    @C2499fr
    /* renamed from: lQ */
    private void m13794lQ(String str) {
        throw new aWi(new aCE(this, hzE, new Object[]{str}));
    }

    @C4034yP
    @C5566aOg
    @C0064Am(aul = "ee7b343c6e48972df381df52c4a041b6", aum = 0)
    @C2499fr
    /* renamed from: lS */
    private void m13795lS(String str) {
        throw new aWi(new aCE(this, hzF, new Object[]{str}));
    }

    /* renamed from: ly */
    private void m13796ly(String str) {
        bFf().mo5608dq().mo3197f(hyy, str);
    }

    /* renamed from: lz */
    private void m13797lz(String str) {
        bFf().mo5608dq().mo3197f(hyz, str);
    }

    /* renamed from: xw */
    private void m13801xw(int i) {
        bFf().mo5608dq().mo3183b(hyN, i);
    }

    @C0064Am(aul = "08c223254fd0f4c7a10a2f156d6f8e00", aum = 0)
    @C5566aOg
    /* renamed from: xx */
    private void m13802xx(int i) {
        throw new aWi(new aCE(this, hzp, new Object[]{new Integer(i)}));
    }

    @C5566aOg
    /* renamed from: QS */
    public void mo8438QS() {
        switch (bFf().mo6893i(aKm)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, aKm, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, aKm, new Object[0]));
                break;
        }
        m13750QR();
    }

    /* renamed from: Qw */
    public BankAccount mo5156Qw() {
        switch (bFf().mo6893i(aJz)) {
            case 0:
                return null;
            case 2:
                return (BankAccount) bFf().mo5606d(new aCE(this, aJz, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, aJz, new Object[0]));
                break;
        }
        return m13751Qv();
    }

    /* renamed from: T */
    public C6494any mo11T() {
        return ___iScriptClass;
    }

    /* renamed from: U */
    public C2491fm[] mo12U() {
        return _m_methods;
    }

    /* renamed from: W */
    public Object mo13W() {
        return new C5229aBh(this);
    }

    /* renamed from: a */
    public Object mo14a(C0495Gr gr) {
        Object[] args = gr.getArgs();
        switch (gr.mo2417hq() - TaikodomObject._m_methodCount) {
            case 0:
                return new Boolean(m13798q((C1722ZT) args[0]));
            case 1:
                return new Boolean(m13788lG((String) args[0]));
            case 2:
                return new Boolean(m13757aK((String) args[0], (String) args[1]));
            case 3:
                return new Boolean(cSH());
            case 4:
                return m13789lI((String) args[0]);
            case 5:
                return cSJ();
            case 6:
                return new Boolean(cSL());
            case 7:
                return m13760au();
            case 8:
                return cSN();
            case 9:
                m13790lK((String) args[0]);
                return null;
            case 10:
                return cSO();
            case 11:
                m13791lM((String) args[0]);
                return null;
            case 12:
                return cSP();
            case 13:
                m13792lN((String) args[0]);
                return null;
            case 14:
                m13758aM((String) args[0], (String) args[1]);
                return null;
            case 15:
                return cSR();
            case 16:
                m13765cg((Player) args[0]);
                return null;
            case 17:
                return new Boolean(cSS());
            case 18:
                m13774iI(((Boolean) args[0]).booleanValue());
                return null;
            case 19:
                return new Boolean(bMp());
            case 20:
                m13770dR(((Boolean) args[0]).booleanValue());
                return null;
            case 21:
                return cSU();
            case 22:
                return new Boolean(cSW());
            case 23:
                m13775iK(((Boolean) args[0]).booleanValue());
                return null;
            case 24:
                return m13799t((aDR) args[0]);
            case 25:
                cSY();
                return null;
            case 26:
                m13756aG();
                return null;
            case 27:
                return new Boolean(cEa());
            case 28:
                return new Boolean(cTa());
            case 29:
                return cTb();
            case 30:
                m13763b((C6283ajv) args[0]);
                return null;
            case 31:
                m13802xx(((Integer) args[0]).intValue());
                return null;
            case 32:
                return new Integer(cTd());
            case 33:
                m13753a((C0665JT) args[0]);
                return null;
            case 34:
                return new Boolean(bOs());
            case 35:
                return cTf();
            case 36:
                m13748C((User) args[0]);
                return null;
            case 37:
                m13761b((Billing) args[0]);
                return null;
            case 38:
                cTh();
                return null;
            case 39:
                return cTj();
            case 40:
                return new Long(cTl());
            case 41:
                m13780kp(((Long) args[0]).longValue());
                return null;
            case 42:
                return new Boolean(cTn());
            case 43:
                return cTo();
            case 44:
                m13793lO((String) args[0]);
                return null;
            case 45:
                return new Boolean(cTq());
            case 46:
                m13776iM(((Boolean) args[0]).booleanValue());
                return null;
            case 47:
                return m13751Qv();
            case 48:
                return m13800uV();
            case 49:
                m13766cu(((Long) args[0]).longValue());
                return null;
            case 50:
                m13767cw(((Long) args[0]).longValue());
                return null;
            case 51:
                return cTr();
            case 52:
                m13764c((BankAccount) args[0]);
                return null;
            case 53:
                m13794lQ((String) args[0]);
                return null;
            case 54:
                m13795lS((String) args[0]);
                return null;
            case 55:
                return cTv();
            case 56:
                return cTx();
            case 57:
                return new Long(cTz());
            case 58:
                m13781kr(((Long) args[0]).longValue());
                return null;
            case 59:
                m13750QR();
                return null;
            default:
                return super.mo14a(gr);
        }
    }

    /* renamed from: a */
    public void mo15a(Collection collection, C0495Gr gr) {
        super.mo15a(collection, gr);
    }

    /* renamed from: aH */
    public void mo70aH() {
        switch (bFf().mo6893i(_f_onResurrect_0020_0028_0029V)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, _f_onResurrect_0020_0028_0029V, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, _f_onResurrect_0020_0028_0029V, new Object[0]));
                break;
        }
        m13756aG();
    }

    /* renamed from: aL */
    public boolean mo8439aL(String str, String str2) {
        switch (bFf().mo6893i(hyR)) {
            case 0:
                return false;
            case 2:
                return ((Boolean) bFf().mo5606d(new aCE(this, hyR, new Object[]{str, str2}))).booleanValue();
            case 3:
                bFf().mo5606d(new aCE(this, hyR, new Object[]{str, str2}));
                break;
        }
        return m13757aK(str, str2);
    }

    @C5566aOg
    /* renamed from: aN */
    public void mo8440aN(String str, String str2) {
        switch (bFf().mo6893i(hzc)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, hzc, new Object[]{str, str2}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, hzc, new Object[]{str, str2}));
                break;
        }
        m13758aM(str, str2);
    }

    /* renamed from: b */
    public void mo24b(C0665JT jt) {
        switch (bFf().mo6893i(f2575Do)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f2575Do, new Object[]{jt}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f2575Do, new Object[]{jt}));
                break;
        }
        m13753a(jt);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Activated")
    public boolean bMq() {
        switch (bFf().mo6893i(eWG)) {
            case 0:
                return false;
            case 2:
                return ((Boolean) bFf().mo5606d(new aCE(this, eWG, new Object[0]))).booleanValue();
            case 3:
                bFf().mo5606d(new aCE(this, eWG, new Object[0]));
                break;
        }
        return bMp();
    }

    public boolean bOt() {
        switch (bFf().mo6893i(fav)) {
            case 0:
                return false;
            case 2:
                return ((Boolean) bFf().mo5606d(new aCE(this, fav, new Object[0]))).booleanValue();
            case 3:
                bFf().mo5606d(new aCE(this, fav, new Object[0]));
                break;
        }
        return bOs();
    }

    /* renamed from: c */
    public void mo8443c(Billing ace) {
        switch (bFf().mo6893i(hzt)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, hzt, new Object[]{ace}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, hzt, new Object[]{ace}));
                break;
        }
        m13761b(ace);
    }

    @C5566aOg
    /* renamed from: c */
    public void mo8444c(C6283ajv ajv) {
        switch (bFf().mo6893i(hzo)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, hzo, new Object[]{ajv}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, hzo, new Object[]{ajv}));
                break;
        }
        m13763b(ajv);
    }

    /* renamed from: c */
    public C5663aRz[] mo25c() {
        return _m_fields;
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "GameMaster")
    public boolean cEb() {
        switch (bFf().mo6893i(gVP)) {
            case 0:
                return false;
            case 2:
                return ((Boolean) bFf().mo5606d(new aCE(this, gVP, new Object[0]))).booleanValue();
            case 3:
                bFf().mo5606d(new aCE(this, gVP, new Object[0]));
                break;
        }
        return cEa();
    }

    @C5566aOg
    /* renamed from: cS */
    public void mo8446cS(boolean z) {
        switch (bFf().mo6893i(hzB)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, hzB, new Object[]{new Boolean(z)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, hzB, new Object[]{new Boolean(z)}));
                break;
        }
        m13776iM(z);
    }

    public boolean cSI() {
        switch (bFf().mo6893i(hyS)) {
            case 0:
                return false;
            case 2:
                return ((Boolean) bFf().mo5606d(new aCE(this, hyS, new Object[0]))).booleanValue();
            case 3:
                bFf().mo5606d(new aCE(this, hyS, new Object[0]));
                break;
        }
        return cSH();
    }

    @C5566aOg
    @C2499fr
    public Player cSK() {
        switch (bFf().mo6893i(hyU)) {
            case 0:
                return null;
            case 2:
                return (Player) bFf().mo5606d(new aCE(this, hyU, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, hyU, new Object[0]));
                break;
        }
        return cSJ();
    }

    public boolean cSM() {
        switch (bFf().mo6893i(hyV)) {
            case 0:
                return false;
            case 2:
                return ((Boolean) bFf().mo5606d(new aCE(this, hyV, new Object[0]))).booleanValue();
            case 3:
                bFf().mo5606d(new aCE(this, hyV, new Object[0]));
                break;
        }
        return cSL();
    }

    public C3438ra<Player> cSQ() {
        switch (bFf().mo6893i(hza)) {
            case 0:
                return null;
            case 2:
                return (C3438ra) bFf().mo5606d(new aCE(this, hza, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, hza, new Object[0]));
                break;
        }
        return cSP();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Idqd")
    public boolean cST() {
        switch (bFf().mo6893i(hzf)) {
            case 0:
                return false;
            case 2:
                return ((Boolean) bFf().mo5606d(new aCE(this, hzf, new Object[0]))).booleanValue();
            case 3:
                bFf().mo5606d(new aCE(this, hzf, new Object[0]));
                break;
        }
        return cSS();
    }

    public UserConnection cSV() {
        switch (bFf().mo6893i(hzh)) {
            case 0:
                return null;
            case 2:
                return (UserConnection) bFf().mo5606d(new aCE(this, hzh, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, hzh, new Object[0]));
                break;
        }
        return cSU();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Banned")
    public boolean cSX() {
        switch (bFf().mo6893i(hzi)) {
            case 0:
                return false;
            case 2:
                return ((Boolean) bFf().mo5606d(new aCE(this, hzi, new Object[0]))).booleanValue();
            case 3:
                bFf().mo5606d(new aCE(this, hzi, new Object[0]));
                break;
        }
        return cSW();
    }

    public void cSZ() {
        switch (bFf().mo6893i(hzl)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, hzl, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, hzl, new Object[0]));
                break;
        }
        cSY();
    }

    public long cTA() {
        switch (bFf().mo6893i(hzI)) {
            case 0:
                return 0;
            case 2:
                return ((Long) bFf().mo5606d(new aCE(this, hzI, new Object[0]))).longValue();
            case 3:
                bFf().mo5606d(new aCE(this, hzI, new Object[0]));
                break;
        }
        return cTz();
    }

    @C5566aOg
    @C2499fr
    public C6283ajv cTc() {
        switch (bFf().mo6893i(hzn)) {
            case 0:
                return null;
            case 2:
                return (C6283ajv) bFf().mo5606d(new aCE(this, hzn, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, hzn, new Object[0]));
                break;
        }
        return cTb();
    }

    public int cTe() {
        switch (bFf().mo6893i(hzq)) {
            case 0:
                return 0;
            case 2:
                return ((Integer) bFf().mo5606d(new aCE(this, hzq, new Object[0]))).intValue();
            case 3:
                bFf().mo5606d(new aCE(this, hzq, new Object[0]));
                break;
        }
        return cTd();
    }

    public Billing cTg() {
        switch (bFf().mo6893i(hzr)) {
            case 0:
                return null;
            case 2:
                return (Billing) bFf().mo5606d(new aCE(this, hzr, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, hzr, new Object[0]));
                break;
        }
        return cTf();
    }

    public void cTi() {
        switch (bFf().mo6893i(hzu)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, hzu, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, hzu, new Object[0]));
                break;
        }
        cTh();
    }

    public String cTk() {
        switch (bFf().mo6893i(hzv)) {
            case 0:
                return null;
            case 2:
                return (String) bFf().mo5606d(new aCE(this, hzv, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, hzv, new Object[0]));
                break;
        }
        return cTj();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "BannedUntil")
    public long cTm() {
        switch (bFf().mo6893i(hzw)) {
            case 0:
                return 0;
            case 2:
                return ((Long) bFf().mo5606d(new aCE(this, hzw, new Object[0]))).longValue();
            case 3:
                bFf().mo5606d(new aCE(this, hzw, new Object[0]));
                break;
        }
        return cTl();
    }

    public String cTp() {
        switch (bFf().mo6893i(hzz)) {
            case 0:
                return null;
            case 2:
                return (String) bFf().mo5606d(new aCE(this, hzz, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, hzz, new Object[0]));
                break;
        }
        return cTo();
    }

    public BankAccount cTs() {
        switch (bFf().mo6893i(hzC)) {
            case 0:
                return null;
            case 2:
                return (BankAccount) bFf().mo5606d(new aCE(this, hzC, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, hzC, new Object[0]));
                break;
        }
        return cTr();
    }

    public String cTw() {
        switch (bFf().mo6893i(hzG)) {
            case 0:
                return null;
            case 2:
                return (String) bFf().mo5606d(new aCE(this, hzG, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, hzG, new Object[0]));
                break;
        }
        return cTv();
    }

    public String cTy() {
        switch (bFf().mo6893i(hzH)) {
            case 0:
                return null;
            case 2:
                return (String) bFf().mo5606d(new aCE(this, hzH, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, hzH, new Object[0]));
                break;
        }
        return cTx();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Tester")
    public boolean cen() {
        switch (bFf().mo6893i(hzm)) {
            case 0:
                return false;
            case 2:
                return ((Boolean) bFf().mo5606d(new aCE(this, hzm, new Object[0]))).booleanValue();
            case 3:
                bFf().mo5606d(new aCE(this, hzm, new Object[0]));
                break;
        }
        return cTa();
    }

    @C5566aOg
    @C2499fr
    /* renamed from: ch */
    public void mo8467ch(Player aku) {
        switch (bFf().mo6893i(hze)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, hze, new Object[]{aku}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, hze, new Object[]{aku}));
                break;
        }
        m13765cg(aku);
    }

    /* renamed from: cv */
    public void mo5157cv(long j) {
        switch (bFf().mo6893i(aJA)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, aJA, new Object[]{new Long(j)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, aJA, new Object[]{new Long(j)}));
                break;
        }
        m13766cu(j);
    }

    /* renamed from: cx */
    public void mo5158cx(long j) {
        switch (bFf().mo6893i(aJB)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, aJB, new Object[]{new Long(j)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, aJB, new Object[]{new Long(j)}));
                break;
        }
        m13767cw(j);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Activated")
    @C5566aOg
    /* renamed from: dS */
    public void mo8468dS(boolean z) {
        switch (bFf().mo6893i(eWH)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, eWH, new Object[]{new Boolean(z)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, eWH, new Object[]{new Boolean(z)}));
                break;
        }
        m13770dR(z);
    }

    /* renamed from: du */
    public boolean mo8351du() {
        switch (bFf().mo6893i(_f_isDeleted_0020_0028_0029Z)) {
            case 0:
                return true;
            case 2:
                return ((Boolean) bFf().mo5606d(new aCE(this, _f_isDeleted_0020_0028_0029Z, new Object[0]))).booleanValue();
            case 3:
                bFf().mo5606d(new aCE(this, _f_isDeleted_0020_0028_0029Z, new Object[0]));
                break;
        }
        return cTq();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Name")
    public String getFullName() {
        switch (bFf().mo6893i(hyW)) {
            case 0:
                return null;
            case 2:
                return (String) bFf().mo5606d(new aCE(this, hyW, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, hyW, new Object[0]));
                break;
        }
        return cSN();
    }

    public String getName() {
        switch (bFf().mo6893i(f2576Pf)) {
            case 0:
                return null;
            case 2:
                return (String) bFf().mo5606d(new aCE(this, f2576Pf, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f2576Pf, new Object[0]));
                break;
        }
        return m13800uV();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Password")
    public String getPassword() {
        switch (bFf().mo6893i(hzd)) {
            case 0:
                return null;
            case 2:
                return (String) bFf().mo5606d(new aCE(this, hzd, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, hzd, new Object[0]));
                break;
        }
        return cSR();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Password")
    @C5566aOg
    public void setPassword(String str) {
        switch (bFf().mo6893i(hzb)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, hzb, new Object[]{str}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, hzb, new Object[]{str}));
                break;
        }
        m13792lN(str);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Username")
    public String getUsername() {
        switch (bFf().mo6893i(hyY)) {
            case 0:
                return null;
            case 2:
                return (String) bFf().mo5606d(new aCE(this, hyY, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, hyY, new Object[0]));
                break;
        }
        return cSO();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Username")
    @C5566aOg
    public void setUsername(String str) {
        switch (bFf().mo6893i(hyZ)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, hyZ, new Object[]{str}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, hyZ, new Object[]{str}));
                break;
        }
        m13791lM(str);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Idqd")
    @C5566aOg
    /* renamed from: iJ */
    public void mo8472iJ(boolean z) {
        switch (bFf().mo6893i(hzg)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, hzg, new Object[]{new Boolean(z)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, hzg, new Object[]{new Boolean(z)}));
                break;
        }
        m13774iI(z);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Banned")
    @C5566aOg
    /* renamed from: iL */
    public void mo8473iL(boolean z) {
        switch (bFf().mo6893i(hzj)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, hzj, new Object[]{new Boolean(z)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, hzj, new Object[]{new Boolean(z)}));
                break;
        }
        m13775iK(z);
    }

    public boolean isSuspended() {
        switch (bFf().mo6893i(hzy)) {
            case 0:
                return false;
            case 2:
                return ((Boolean) bFf().mo5606d(new aCE(this, hzy, new Object[0]))).booleanValue();
            case 3:
                bFf().mo5606d(new aCE(this, hzy, new Object[0]));
                break;
        }
        return cTn();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "BannedUntil")
    @C5566aOg
    /* renamed from: kq */
    public void mo8475kq(long j) {
        switch (bFf().mo6893i(hzx)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, hzx, new Object[]{new Long(j)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, hzx, new Object[]{new Long(j)}));
                break;
        }
        m13780kp(j);
    }

    @C5566aOg
    /* renamed from: ks */
    public void mo8476ks(long j) {
        switch (bFf().mo6893i(hzJ)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, hzJ, new Object[]{new Long(j)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, hzJ, new Object[]{new Long(j)}));
                break;
        }
        m13781kr(j);
    }

    @C5566aOg
    @C0909NL
    /* renamed from: lH */
    public boolean mo8477lH(String str) {
        switch (bFf().mo6893i(hyQ)) {
            case 0:
                return false;
            case 2:
                return ((Boolean) bFf().mo5606d(new aCE(this, hyQ, new Object[]{str}))).booleanValue();
            case 3:
                bFf().mo5606d(new aCE(this, hyQ, new Object[]{str}));
                break;
        }
        return m13788lG(str);
    }

    @C5566aOg
    @C2499fr
    /* renamed from: lJ */
    public Player mo8478lJ(String str) {
        switch (bFf().mo6893i(hyT)) {
            case 0:
                return null;
            case 2:
                return (Player) bFf().mo5606d(new aCE(this, hyT, new Object[]{str}));
            case 3:
                bFf().mo5606d(new aCE(this, hyT, new Object[]{str}));
                break;
        }
        return m13789lI(str);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Name")
    @C5566aOg
    /* renamed from: lL */
    public void mo8479lL(String str) {
        switch (bFf().mo6893i(hyX)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, hyX, new Object[]{str}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, hyX, new Object[]{str}));
                break;
        }
        m13790lK(str);
    }

    @C4034yP
    @C5566aOg
    @C2499fr
    /* renamed from: lP */
    public void mo8480lP(String str) {
        switch (bFf().mo6893i(hzA)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, hzA, new Object[]{str}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, hzA, new Object[]{str}));
                break;
        }
        m13793lO(str);
    }

    @C4034yP
    @C5566aOg
    @C2499fr
    /* renamed from: lR */
    public void mo8481lR(String str) {
        switch (bFf().mo6893i(hzE)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, hzE, new Object[]{str}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, hzE, new Object[]{str}));
                break;
        }
        m13794lQ(str);
    }

    @C4034yP
    @C5566aOg
    @C2499fr
    /* renamed from: lT */
    public void mo8482lT(String str) {
        switch (bFf().mo6893i(hzF)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, hzF, new Object[]{str}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, hzF, new Object[]{str}));
                break;
        }
        m13795lS(str);
    }

    @C5472aKq
    /* renamed from: r */
    public boolean mo8483r(C1722ZT<UserConnection> zt) {
        switch (bFf().mo6893i(hyP)) {
            case 0:
                return false;
            case 2:
                return ((Boolean) bFf().mo5606d(new aCE(this, hyP, new Object[]{zt}))).booleanValue();
            case 3:
                bFf().mo5606d(new aCE(this, hyP, new Object[]{zt}));
                break;
        }
        return m13798q(zt);
    }

    public String toString() {
        switch (bFf().mo6893i(_f_toString_0020_0028_0029Ljava_002flang_002fString_003b)) {
            case 0:
                return null;
            case 2:
                return (String) bFf().mo5606d(new aCE(this, _f_toString_0020_0028_0029Ljava_002flang_002fString_003b, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, _f_toString_0020_0028_0029Ljava_002flang_002fString_003b, new Object[0]));
                break;
        }
        return m13760au();
    }

    /* renamed from: u */
    public UserConnection mo8486u(aDR adr) {
        switch (bFf().mo6893i(hzk)) {
            case 0:
                return null;
            case 2:
                return (UserConnection) bFf().mo5606d(new aCE(this, hzk, new Object[]{adr}));
            case 3:
                bFf().mo5606d(new aCE(this, hzk, new Object[]{adr}));
                break;
        }
        return m13799t(adr);
    }

    @C5566aOg
    /* renamed from: xy */
    public void mo8487xy(int i) {
        switch (bFf().mo6893i(hzp)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, hzp, new Object[]{new Integer(i)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, hzp, new Object[]{new Integer(i)}));
                break;
        }
        m13802xx(i);
    }

    @C0064Am(aul = "72b31763fe0c6097fd1ec34914576237", aum = 0)
    @C5472aKq
    /* renamed from: q */
    private boolean m13798q(C1722ZT<UserConnection> zt) {
        if (zt == null) {
            mo8358lY("Null replication context. Ignoring replication rule");
            return false;
        } else if (zt.bFq().getUser() == this || zt.bFq().getUser().cEb()) {
            return true;
        } else {
            if (isDebugEnabled()) {
                mo8360mc("#############################################");
                mo8360mc(" User " + zt.bFq().getUser().getFullName() + " didn't pass the replication rule. Missed username: " + getFullName() + " (Check bug on replication or Hack)");
                mo8360mc("#############################################");
            }
            return false;
        }
    }

    /* renamed from: S */
    public void mo10S() {
        super.mo10S();
        m13755a(C6283ajv.PLAYER);
        m13749D(this);
        BankAccount ado = (BankAccount) bFf().mo6865M(BankAccount.class);
        ado.mo12834gR(0);
        m13768d(ado);
    }

    @C0064Am(aul = "298b6fab19efba960d3d0f77b6816f1e", aum = 0)
    /* renamed from: aK */
    private boolean m13757aK(String str, String str2) {
        return false;
    }

    @C0064Am(aul = "6bf8a0df3f4f5a45e3ba96cf02cb4eef", aum = 0)
    private boolean cSH() {
        return true;
    }

    @C0064Am(aul = "a13e8d74f643f8a3c67e322fa30a5a8d", aum = 0)
    private boolean cSL() {
        return aQN().size() < 6;
    }

    @C0064Am(aul = "885d56b255a55efe097789cd2709b48f", aum = 0)
    /* renamed from: au */
    private String m13760au() {
        return getUsername();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Name")
    @C0064Am(aul = "e81a49fc21fd716f204a9f23ee5abceb", aum = 0)
    private String cSN() {
        return cSq();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Username")
    @C0064Am(aul = "02ecefb64f645195627673ffc6446a59", aum = 0)
    private String cSO() {
        return cSr();
    }

    @C0064Am(aul = "c699272ed725d05aaadafe2e80dd8c86", aum = 0)
    private C3438ra<Player> cSP() {
        return aQN();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Password")
    @C0064Am(aul = "7983acbc9fe1f3e043097c4561f1a909", aum = 0)
    private String cSR() {
        return hyv;
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Idqd")
    @C0064Am(aul = "4839dd357c5e3fcd6a8eeacbd8d06855", aum = 0)
    private boolean cSS() {
        return cSv();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Activated")
    @C0064Am(aul = "4d9378bfa3e6bec6e1e8d3d2be33d9ad", aum = 0)
    private boolean bMp() {
        return bLJ();
    }

    @C0064Am(aul = "fe73cc6aed80f9742424b4d54e645ed6", aum = 0)
    private UserConnection cSU() {
        return cSD();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Banned")
    @C0064Am(aul = "1a212fb8394410313dc001670c54a4c4", aum = 0)
    private boolean cSW() {
        return cSx();
    }

    @C0064Am(aul = "a3b4d92ae9142309201ce62cff5c7b10", aum = 0)
    /* renamed from: t */
    private UserConnection m13799t(aDR adr) {
        UserConnection apk = (UserConnection) bFf().mo6865M(UserConnection.class);
        apk.mo15381a(this, adr);
        m13777j(apk);
        adr.mo8399d(cSD().bFf());
        ala().aMC().mo12161g(cSD());
        ala().aJu().mo7853z(this);
        mo2071b(new C3544sP(this));
        return cSD();
    }

    @C0064Am(aul = "ff273613bbca5cdd65023019386e3782", aum = 0)
    private void cSY() {
        ala().aJu().mo7842b(this, cSD().bgt());
        mo2071b(new C2697ik(this));
        UserConnection cSD = cSD();
        m13777j((UserConnection) null);
        cSD.dispose();
    }

    @C0064Am(aul = "b5840c5b1aa6efe9dd04bb10666439aa", aum = 0)
    /* renamed from: aG */
    private void m13756aG() {
        super.mo70aH();
        m13777j((UserConnection) null);
        if (cSp() == null) {
            BankAccount ado = (BankAccount) bFf().mo6865M(BankAccount.class);
            ado.mo12834gR(0);
            m13768d(ado);
        }
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "GameMaster")
    @C0064Am(aul = "9c6f21692302ee8342336f14d68b009a", aum = 0)
    private boolean cEa() {
        return cSA().ceo();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Tester")
    @C0064Am(aul = "a293ebb6458ada435157646faa19d6ca", aum = 0)
    private boolean cTa() {
        return cSA().cen();
    }

    @C0064Am(aul = "dff3b702621f7ff82fdbfa6efb42599a", aum = 0)
    private int cTd() {
        return cSF();
    }

    @C0064Am(aul = "0e615feb4d5d60cfa15c56d63e4eb80b", aum = 0)
    /* renamed from: a */
    private void m13753a(C0665JT jt) {
        if (jt.mo3117j(1, 0, 0)) {
            int intValue = ((Integer) jt.get("adminLevel")).intValue();
            if (!((Boolean) jt.get("gameMaster")).booleanValue()) {
                m13755a(C6283ajv.PLAYER);
            } else if (intValue == 0) {
                m13755a(C6283ajv.ADMIN);
            } else if (intValue == 1) {
                m13755a(C6283ajv.GM);
            } else if (intValue == 2) {
                m13755a(C6283ajv.PLAYER);
            }
        }
    }

    @C0064Am(aul = "204873e53a3d8054851286f52fe188e4", aum = 0)
    private boolean bOs() {
        return cTg().bOt();
    }

    @C0064Am(aul = "e129e3a1ee1c30e8114a5152847c03d0", aum = 0)
    private Billing cTf() {
        if (cSE() == null) {
            m13749D(this);
        }
        return cSE();
    }

    @C0064Am(aul = "131fbb0aabd3dbebf57e5ae947369d3d", aum = 0)
    /* renamed from: C */
    private void m13748C(User adk) {
        Billing ace = (Billing) bFf().mo6865M(Billing.class);
        ace.mo12676u(adk);
        mo8443c(ace);
    }

    @C0064Am(aul = "c5a090251f8779e544d2e14b0b96ee7f", aum = 0)
    /* renamed from: b */
    private void m13761b(Billing ace) {
        m13754a(ace);
    }

    @C0064Am(aul = "b80ed300c8b6b7a52bfd17728e7b0bf0", aum = 0)
    private void cTh() {
        cTg().bOz();
    }

    @C0064Am(aul = "9ddb19b592e8b7afd1ff76ae92a4de8e", aum = 0)
    private String cTj() {
        try {
            if (cSV() != null) {
                return ((ClientConnectImpl) cSV().bFs().bgt()).getInetAddress().getHostAddress();
            }
        } catch (Exception e) {
        }
        return null;
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "BannedUntil")
    @C0064Am(aul = "4a642250e8d5bf2e6c8e0687c2436e53", aum = 0)
    private long cTl() {
        return cSy();
    }

    @C0064Am(aul = "1342f04e8a1c7b6b5b7085b6013d8b9f", aum = 0)
    private boolean cTn() {
        boolean z = cTm() > cVr();
        if (!z && cSx()) {
            mo8473iL(false);
            mo8475kq(0);
        }
        return z;
    }

    @C0064Am(aul = "5578e9fbfb2c66c3829f80df08a310d4", aum = 0)
    private String cTo() {
        return cSz();
    }

    @C0064Am(aul = "f36594e47a26b6ffe06ed1c6fc16d6a0", aum = 0)
    private boolean cTq() {
        return cSw();
    }

    @C0064Am(aul = "fe0950d7b7b950a00d59e045b57b0d70", aum = 0)
    /* renamed from: Qv */
    private BankAccount m13751Qv() {
        return cSp();
    }

    @C0064Am(aul = "dca6753cbe681c772a2ac785667e652a", aum = 0)
    /* renamed from: uV */
    private String m13800uV() {
        return cSr();
    }

    @C0064Am(aul = "704d9af17c1100ea0e653d4f6d2a819a", aum = 0)
    /* renamed from: cu */
    private void m13766cu(long j) {
    }

    @C0064Am(aul = "46d956aebbe659f747e54e051a022704", aum = 0)
    /* renamed from: cw */
    private void m13767cw(long j) {
    }

    @C0064Am(aul = "946b0af37e96849aa5ed0e89913ed301", aum = 0)
    private BankAccount cTr() {
        return cSp();
    }

    @C0064Am(aul = "b413f96072787ae56a562912e4c5da11", aum = 0)
    /* renamed from: c */
    private void m13764c(BankAccount ado) {
        m13762b(ado);
    }

    @C0064Am(aul = "b5cce2c4d3c446e72f8e494c78d80b88", aum = 0)
    private String cTv() {
        return cSB();
    }

    @C0064Am(aul = "524d0e007db1db4dd073a4e899da3767", aum = 0)
    private String cTx() {
        return cSC();
    }

    @C0064Am(aul = "1bf959882c87839a1af3ffe8e7bb04a9", aum = 0)
    private long cTz() {
        return cSG();
    }
}
