package game.script.player;

import game.network.message.externalizable.aCE;
import game.script.TaikodomObject;
import logic.baa.C1616Xf;
import logic.baa.C1634Xv;
import logic.baa.C5511aMd;
import logic.baa.C6485anp;
import logic.data.mbean.C3364qj;
import logic.res.code.C5663aRz;
import logic.res.code.C6494any;
import logic.res.html.C2491fm;
import p001a.*;

import java.util.Collection;

@C5511aMd
@C6485anp
/* renamed from: a.aLa  reason: case insensitive filesystem */
/* compiled from: a */
public class PlayerFinder extends TaikodomObject implements C1616Xf {
    public static final int _m_fieldCount = 0;
    public static final C5663aRz[] _m_fields = null;
    public static final int _m_methodCount = 0;
    public static final C2491fm[] _m_methods = null;
    public static final C2491fm ijX = null;
    public static final long serialVersionUID = 0;
    public static C6494any ___iScriptClass;

    static {
        m16206V();
    }

    public PlayerFinder() {
        super((C5540aNg) null);
        super.mo10S();
    }

    public PlayerFinder(C5540aNg ang) {
        super(ang);
    }

    /* renamed from: V */
    static void m16206V() {
        serialVersionUID = (long) 1;
        _m_fieldCount = TaikodomObject._m_fieldCount + 0;
        _m_methodCount = TaikodomObject._m_methodCount + 1;
        _m_fields = new C5663aRz[(TaikodomObject._m_fieldCount + 0)];
        C1634Xv.m11725a((Object[]) TaikodomObject._m_fields, (Object[]) _m_fields);
        int i = TaikodomObject._m_methodCount;
        C2491fm[] fmVarArr = new C2491fm[(i + 1)];
        C2491fm a = C4105zY.m41624a(PlayerFinder.class, "e36b1a4b8f15edc735a9d4a9252db4ce", i);
        ijX = a;
        fmVarArr[i] = a;
        int i2 = i + 1;
        _m_methods = fmVarArr;
        C1634Xv.m11725a((Object[]) TaikodomObject._m_methods, (Object[]) _m_methods);
        ___iScriptClass = aUO.m18566a(PlayerFinder.class, C3364qj.class, _m_fields, _m_methods);
    }

    @C0064Am(aul = "e36b1a4b8f15edc735a9d4a9252db4ce", aum = 0)
    @C5566aOg
    @C2499fr
    /* renamed from: nc */
    private Player m16207nc(String str) {
        throw new aWi(new aCE(this, ijX, new Object[]{str}));
    }

    /* renamed from: T */
    public C6494any mo11T() {
        return ___iScriptClass;
    }

    /* renamed from: U */
    public C2491fm[] mo12U() {
        return _m_methods;
    }

    /* renamed from: W */
    public Object mo13W() {
        return new C3364qj(this);
    }

    /* renamed from: a */
    public Object mo14a(C0495Gr gr) {
        Object[] args = gr.getArgs();
        switch (gr.mo2417hq() - TaikodomObject._m_methodCount) {
            case 0:
                return m16207nc((String) args[0]);
            default:
                return super.mo14a(gr);
        }
    }

    /* renamed from: a */
    public void mo15a(Collection collection, C0495Gr gr) {
        super.mo15a(collection, gr);
    }

    /* renamed from: c */
    public C5663aRz[] mo25c() {
        return _m_fields;
    }

    @C5566aOg
    @C2499fr
    /* renamed from: nd */
    public Player mo9908nd(String str) {
        switch (bFf().mo6893i(ijX)) {
            case 0:
                return null;
            case 2:
                return (Player) bFf().mo5606d(new aCE(this, ijX, new Object[]{str}));
            case 3:
                bFf().mo5606d(new aCE(this, ijX, new Object[]{str}));
                break;
        }
        return m16207nc(str);
    }

    /* renamed from: S */
    public void mo10S() {
        super.mo10S();
    }
}
