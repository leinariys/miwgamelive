package game.script.player;

import game.network.message.externalizable.aCE;
import game.script.Actor;
import game.script.TaikodomObject;
import game.script.ship.Ship;
import game.script.ship.Station;
import game.script.space.Node;
import logic.baa.*;
import logic.data.mbean.C2788k;
import logic.data.mbean.aPN;
import logic.res.code.C5640aRc;
import logic.res.code.C5663aRz;
import logic.res.code.C6494any;
import logic.res.html.C2491fm;
import logic.sql.C5878acG;
import logic.sql.C6444anA;
import logic.sql.C6974axn;
import p001a.*;

import java.text.SimpleDateFormat;
import java.util.Collection;

@C5511aMd
@C6485anp
/* renamed from: a.AL */
/* compiled from: a */
public class PlayerSessionLog extends TaikodomObject implements C1616Xf {
    public static final C2491fm _f_dispose_0020_0028_0029V = null;
    public static final int _m_fieldCount = 0;
    public static final C5663aRz[] _m_fields = null;
    public static final int _m_methodCount = 0;
    public static final C2491fm[] _m_methods = null;
    public static final C2491fm cpB = null;
    public static final C5663aRz fRJ = null;
    public static final C5663aRz fRK = null;
    public static final C5663aRz fRL = null;
    public static final C5663aRz fRM = null;
    public static final C5663aRz fRN = null;
    public static final C5663aRz fRO = null;
    public static final C5663aRz fRP = null;
    public static final C5663aRz fRQ = null;
    public static final C5663aRz fRR = null;
    public static final C5663aRz fRS = null;
    public static final C5663aRz fRT = null;
    public static final C5663aRz fRU = null;
    public static final C5663aRz fRV = null;
    public static final C2491fm fRW = null;
    public static final C2491fm fRX = null;
    public static final C2491fm fRY = null;
    public static final C2491fm fRZ = null;
    public static final C2491fm fSa = null;
    public static final C2491fm fSb = null;
    public static final C2491fm fSc = null;
    public static final C2491fm fSd = null;
    public static final C2491fm fSe = null;
    public static final C2491fm fSf = null;
    public static final C2491fm fSg = null;
    public static final C2491fm fSh = null;
    public static final C2491fm fSi = null;
    public static final C2491fm fSj = null;
    /* renamed from: hz */
    public static final C5663aRz f58hz = null;
    /* renamed from: la */
    public static final C5663aRz f59la = null;
    public static final long serialVersionUID = 0;
    private static final SimpleDateFormat fRI = new SimpleDateFormat("dd.MM.yyyy HH:mm");
    public static C6494any ___iScriptClass;
    @C0064Am(aul = "10eb8abe3d131f6489f41aeba3dbff62", aum = 0)

    /* renamed from: P */
    private static Player f43P;
    @C0064Am(aul = "078359c7c36cbc43f3424f2ab2a10e40", aum = 1)

    /* renamed from: Q */
    private static long f44Q;
    @C0064Am(aul = "98d34c3024bdcc3112c716b8c0d2c74d", aum = 2)

    /* renamed from: R */
    private static long f45R;
    @C0064Am(aul = "231e38476cd5e2193c79c7e0c5bffa39", aum = 3)

    /* renamed from: S */
    private static int f46S;
    @C0064Am(aul = "1700fc6c5409860f4b7a157c20154ff0", aum = 4)

    /* renamed from: T */
    private static long f47T;
    @C0064Am(aul = "bf805df432c9b97ebbf3c8f1cb04ab21", aum = 5)

    /* renamed from: U */
    private static int f48U;
    @C0064Am(aul = "8070018378a974d05fff3029a32d08a4", aum = 6)

    /* renamed from: V */
    private static int f49V;
    @C0064Am(aul = "67b78b5daa2a29947c5c3ade4754d9ae", aum = 7)

    /* renamed from: W */
    private static int f50W;
    @C0064Am(aul = "14c8b890f33b0a69651e10d240099f46", aum = 8)

    /* renamed from: X */
    private static int f51X;
    @C0064Am(aul = "274447d1ff8414421a2e661ba4c7795e", aum = 9)

    /* renamed from: aa */
    private static long f52aa;
    @C0064Am(aul = "331b503223d16d5fb6f0816b461e5e17", aum = 10)

    /* renamed from: ab */
    private static long f53ab;
    @C0064Am(aul = "571c7cc918767fe9e52c4ab5373514d8", aum = 11)

    /* renamed from: ac */
    private static long f54ac;
    @C0064Am(aul = "e49865ae4d21199d6678fac3320551c2", aum = 12)

    /* renamed from: ad */
    private static long f55ad;
    @C0064Am(aul = "71135dadc3eaa2e32455c904cdb3122a", aum = 13)

    /* renamed from: ae */
    private static long f56ae;
    @C0064Am(aul = "76ef260b46db3c2307e6ede355511bcf", aum = 14)

    /* renamed from: af */
    private static PlayerSession f57af;

    static {
        m294V();
    }

    public PlayerSessionLog() {
        super((C5540aNg) null);
        super.mo10S();
    }

    public PlayerSessionLog(C5540aNg ang) {
        super(ang);
    }

    PlayerSessionLog(Player aku) {
        super((C5540aNg) null);
        super._m_script_init(aku);
    }

    /* renamed from: V */
    static void m294V() {
        serialVersionUID = (long) 1;
        _m_fieldCount = TaikodomObject._m_fieldCount + 15;
        _m_methodCount = TaikodomObject._m_methodCount + 16;
        int i = TaikodomObject._m_fieldCount;
        C5663aRz[] arzArr = new C5663aRz[(i + 15)];
        C5663aRz b = C5640aRc.m17844b(PlayerSessionLog.class, "10eb8abe3d131f6489f41aeba3dbff62", i);
        f58hz = b;
        arzArr[i] = b;
        int i2 = i + 1;
        C5663aRz b2 = C5640aRc.m17844b(PlayerSessionLog.class, "078359c7c36cbc43f3424f2ab2a10e40", i2);
        f59la = b2;
        arzArr[i2] = b2;
        int i3 = i2 + 1;
        C5663aRz b3 = C5640aRc.m17844b(PlayerSessionLog.class, "98d34c3024bdcc3112c716b8c0d2c74d", i3);
        fRJ = b3;
        arzArr[i3] = b3;
        int i4 = i3 + 1;
        C5663aRz b4 = C5640aRc.m17844b(PlayerSessionLog.class, "231e38476cd5e2193c79c7e0c5bffa39", i4);
        fRK = b4;
        arzArr[i4] = b4;
        int i5 = i4 + 1;
        C5663aRz b5 = C5640aRc.m17844b(PlayerSessionLog.class, "1700fc6c5409860f4b7a157c20154ff0", i5);
        fRL = b5;
        arzArr[i5] = b5;
        int i6 = i5 + 1;
        C5663aRz b6 = C5640aRc.m17844b(PlayerSessionLog.class, "bf805df432c9b97ebbf3c8f1cb04ab21", i6);
        fRM = b6;
        arzArr[i6] = b6;
        int i7 = i6 + 1;
        C5663aRz b7 = C5640aRc.m17844b(PlayerSessionLog.class, "8070018378a974d05fff3029a32d08a4", i7);
        fRN = b7;
        arzArr[i7] = b7;
        int i8 = i7 + 1;
        C5663aRz b8 = C5640aRc.m17844b(PlayerSessionLog.class, "67b78b5daa2a29947c5c3ade4754d9ae", i8);
        fRO = b8;
        arzArr[i8] = b8;
        int i9 = i8 + 1;
        C5663aRz b9 = C5640aRc.m17844b(PlayerSessionLog.class, "14c8b890f33b0a69651e10d240099f46", i9);
        fRP = b9;
        arzArr[i9] = b9;
        int i10 = i9 + 1;
        C5663aRz b10 = C5640aRc.m17844b(PlayerSessionLog.class, "274447d1ff8414421a2e661ba4c7795e", i10);
        fRQ = b10;
        arzArr[i10] = b10;
        int i11 = i10 + 1;
        C5663aRz b11 = C5640aRc.m17844b(PlayerSessionLog.class, "331b503223d16d5fb6f0816b461e5e17", i11);
        fRR = b11;
        arzArr[i11] = b11;
        int i12 = i11 + 1;
        C5663aRz b12 = C5640aRc.m17844b(PlayerSessionLog.class, "571c7cc918767fe9e52c4ab5373514d8", i12);
        fRS = b12;
        arzArr[i12] = b12;
        int i13 = i12 + 1;
        C5663aRz b13 = C5640aRc.m17844b(PlayerSessionLog.class, "e49865ae4d21199d6678fac3320551c2", i13);
        fRT = b13;
        arzArr[i13] = b13;
        int i14 = i13 + 1;
        C5663aRz b14 = C5640aRc.m17844b(PlayerSessionLog.class, "71135dadc3eaa2e32455c904cdb3122a", i14);
        fRU = b14;
        arzArr[i14] = b14;
        int i15 = i14 + 1;
        C5663aRz b15 = C5640aRc.m17844b(PlayerSessionLog.class, "76ef260b46db3c2307e6ede355511bcf", i15);
        fRV = b15;
        arzArr[i15] = b15;
        int i16 = i15 + 1;
        _m_fields = arzArr;
        C1634Xv.m11725a((Object[]) TaikodomObject._m_fields, (Object[]) _m_fields);
        int i17 = TaikodomObject._m_methodCount;
        C2491fm[] fmVarArr = new C2491fm[(i17 + 16)];
        C2491fm a = C4105zY.m41624a(PlayerSessionLog.class, "b2d2fe39533fa70f90a63e99db6d1d54", i17);
        _f_dispose_0020_0028_0029V = a;
        fmVarArr[i17] = a;
        int i18 = i17 + 1;
        C2491fm a2 = C4105zY.m41624a(PlayerSessionLog.class, "2cb697660180c5edbd0a397a115ca796", i18);
        fRW = a2;
        fmVarArr[i18] = a2;
        int i19 = i18 + 1;
        C2491fm a3 = C4105zY.m41624a(PlayerSessionLog.class, "790be2f5aa411c19f7fbda1f79ba4be3", i19);
        fRX = a3;
        fmVarArr[i19] = a3;
        int i20 = i19 + 1;
        C2491fm a4 = C4105zY.m41624a(PlayerSessionLog.class, "d071bbb37f12355a75bf7b4f9c8418cf", i20);
        fRY = a4;
        fmVarArr[i20] = a4;
        int i21 = i20 + 1;
        C2491fm a5 = C4105zY.m41624a(PlayerSessionLog.class, "0f8b2ea2e6dc589fcb41776ad6b1d2d0", i21);
        fRZ = a5;
        fmVarArr[i21] = a5;
        int i22 = i21 + 1;
        C2491fm a6 = C4105zY.m41624a(PlayerSessionLog.class, "e5b07686b747249bff8aab7551799025", i22);
        fSa = a6;
        fmVarArr[i22] = a6;
        int i23 = i22 + 1;
        C2491fm a7 = C4105zY.m41624a(PlayerSessionLog.class, "eb7b8cfe3d547b3add6ec13065d7aa10", i23);
        fSb = a7;
        fmVarArr[i23] = a7;
        int i24 = i23 + 1;
        C2491fm a8 = C4105zY.m41624a(PlayerSessionLog.class, "7ae8440596b55e131a351bccdb2aa7d9", i24);
        fSc = a8;
        fmVarArr[i24] = a8;
        int i25 = i24 + 1;
        C2491fm a9 = C4105zY.m41624a(PlayerSessionLog.class, "a511e2ab6321b85143f72fe2671d798b", i25);
        fSd = a9;
        fmVarArr[i25] = a9;
        int i26 = i25 + 1;
        C2491fm a10 = C4105zY.m41624a(PlayerSessionLog.class, "944f627d53f091694c9ebb412132538e", i26);
        fSe = a10;
        fmVarArr[i26] = a10;
        int i27 = i26 + 1;
        C2491fm a11 = C4105zY.m41624a(PlayerSessionLog.class, "59f3ca43b7e7d57da8bb8964592d38b8", i27);
        fSf = a11;
        fmVarArr[i27] = a11;
        int i28 = i27 + 1;
        C2491fm a12 = C4105zY.m41624a(PlayerSessionLog.class, "090ed252e728a0b4bcf5077a5b3b258b", i28);
        cpB = a12;
        fmVarArr[i28] = a12;
        int i29 = i28 + 1;
        C2491fm a13 = C4105zY.m41624a(PlayerSessionLog.class, "18c0d045eecf150c068c06ff241f401e", i29);
        fSg = a13;
        fmVarArr[i29] = a13;
        int i30 = i29 + 1;
        C2491fm a14 = C4105zY.m41624a(PlayerSessionLog.class, "3cb1c571e2176e4ee55eda59b68bef88", i30);
        fSh = a14;
        fmVarArr[i30] = a14;
        int i31 = i30 + 1;
        C2491fm a15 = C4105zY.m41624a(PlayerSessionLog.class, "6cf063d856bf587a200961da6aaea622", i31);
        fSi = a15;
        fmVarArr[i31] = a15;
        int i32 = i31 + 1;
        C2491fm a16 = C4105zY.m41624a(PlayerSessionLog.class, "129a6dad0141fbd80f965a112ee79907", i32);
        fSj = a16;
        fmVarArr[i32] = a16;
        int i33 = i32 + 1;
        _m_methods = fmVarArr;
        C1634Xv.m11725a((Object[]) TaikodomObject._m_methods, (Object[]) _m_methods);
        ___iScriptClass = aUO.m18566a(PlayerSessionLog.class, C2788k.class, _m_fields, _m_methods);
    }

    /* renamed from: a */
    private void m295a(PlayerSession aVar) {
        bFf().mo5608dq().mo3197f(fRV, aVar);
    }

    /* renamed from: a */
    private void m297a(Player aku) {
        bFf().mo5608dq().mo3197f(f58hz, aku);
    }

    @C0064Am(aul = "090ed252e728a0b4bcf5077a5b3b258b", aum = 0)
    @C5566aOg
    private void aAl() {
        throw new aWi(new aCE(this, cpB, new Object[0]));
    }

    @C0064Am(aul = "7ae8440596b55e131a351bccdb2aa7d9", aum = 0)
    @C5566aOg
    /* renamed from: c */
    private void m299c(C0024b bVar) {
        throw new aWi(new aCE(this, fSc, new Object[]{bVar}));
    }

    private long ceS() {
        return bFf().mo5608dq().mo3213o(fRJ);
    }

    /* access modifiers changed from: private */
    public int ceT() {
        return bFf().mo5608dq().mo3212n(fRK);
    }

    private long ceU() {
        return bFf().mo5608dq().mo3213o(fRL);
    }

    private int ceV() {
        return bFf().mo5608dq().mo3212n(fRM);
    }

    private int ceW() {
        return bFf().mo5608dq().mo3212n(fRN);
    }

    private int ceX() {
        return bFf().mo5608dq().mo3212n(fRO);
    }

    private int ceY() {
        return bFf().mo5608dq().mo3212n(fRP);
    }

    private long ceZ() {
        return bFf().mo5608dq().mo3213o(fRQ);
    }

    private long cfa() {
        return bFf().mo5608dq().mo3213o(fRR);
    }

    private long cfb() {
        return bFf().mo5608dq().mo3213o(fRS);
    }

    private long cfc() {
        return bFf().mo5608dq().mo3213o(fRT);
    }

    private long cfd() {
        return bFf().mo5608dq().mo3213o(fRU);
    }

    private PlayerSession cfe() {
        return (PlayerSession) bFf().mo5608dq().mo3214p(fRV);
    }

    @C0064Am(aul = "790be2f5aa411c19f7fbda1f79ba4be3", aum = 0)
    @C5566aOg
    private void cfh() {
        throw new aWi(new aCE(this, fRX, new Object[0]));
    }

    @C0064Am(aul = "d071bbb37f12355a75bf7b4f9c8418cf", aum = 0)
    @C5566aOg
    private void cfj() {
        throw new aWi(new aCE(this, fRY, new Object[0]));
    }

    @C0064Am(aul = "0f8b2ea2e6dc589fcb41776ad6b1d2d0", aum = 0)
    @C5566aOg
    private void cfl() {
        throw new aWi(new aCE(this, fRZ, new Object[0]));
    }

    @C0064Am(aul = "e5b07686b747249bff8aab7551799025", aum = 0)
    @C5566aOg
    private void cfn() {
        throw new aWi(new aCE(this, fSa, new Object[0]));
    }

    @C0064Am(aul = "eb7b8cfe3d547b3add6ec13065d7aa10", aum = 0)
    @C5566aOg
    private void cfp() {
        throw new aWi(new aCE(this, fSb, new Object[0]));
    }

    @C4034yP
    @C5566aOg
    @C0064Am(aul = "a511e2ab6321b85143f72fe2671d798b", aum = 0)
    @C2499fr
    private void cfr() {
        throw new aWi(new aCE(this, fSd, new Object[0]));
    }

    private String cfu() {
        switch (bFf().mo6893i(fSh)) {
            case 0:
                return null;
            case 2:
                return (String) bFf().mo5606d(new aCE(this, fSh, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, fSh, new Object[0]));
                break;
        }
        return cft();
    }

    private String cfw() {
        switch (bFf().mo6893i(fSi)) {
            case 0:
                return null;
            case 2:
                return (String) bFf().mo5606d(new aCE(this, fSi, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, fSi, new Object[0]));
                break;
        }
        return cfv();
    }

    /* access modifiers changed from: private */
    /* renamed from: dG */
    public Player m301dG() {
        return (Player) bFf().mo5608dq().mo3214p(f58hz);
    }

    /* renamed from: eH */
    private long m302eH() {
        return bFf().mo5608dq().mo3213o(f59la);
    }

    /* renamed from: hA */
    private void m304hA(long j) {
        bFf().mo5608dq().mo3184b(fRR, j);
    }

    /* renamed from: hB */
    private void m305hB(long j) {
        bFf().mo5608dq().mo3184b(fRS, j);
    }

    /* renamed from: hC */
    private void m306hC(long j) {
        bFf().mo5608dq().mo3184b(fRT, j);
    }

    /* renamed from: hD */
    private void m307hD(long j) {
        bFf().mo5608dq().mo3184b(fRU, j);
    }

    @C0064Am(aul = "944f627d53f091694c9ebb412132538e", aum = 0)
    @C5566aOg
    /* renamed from: hE */
    private void m308hE(long j) {
        throw new aWi(new aCE(this, fSe, new Object[]{new Long(j)}));
    }

    @C0064Am(aul = "59f3ca43b7e7d57da8bb8964592d38b8", aum = 0)
    @C5566aOg
    /* renamed from: hG */
    private void m309hG(long j) {
        throw new aWi(new aCE(this, fSf, new Object[]{new Long(j)}));
    }

    /* renamed from: hJ */
    private String m311hJ(long j) {
        switch (bFf().mo6893i(fSg)) {
            case 0:
                return null;
            case 2:
                return (String) bFf().mo5606d(new aCE(this, fSg, new Object[]{new Long(j)}));
            case 3:
                bFf().mo5606d(new aCE(this, fSg, new Object[]{new Long(j)}));
                break;
        }
        return m310hI(j);
    }

    /* renamed from: hx */
    private void m312hx(long j) {
        bFf().mo5608dq().mo3184b(fRJ, j);
    }

    /* renamed from: hy */
    private void m313hy(long j) {
        bFf().mo5608dq().mo3184b(fRL, j);
    }

    /* renamed from: hz */
    private void m314hz(long j) {
        bFf().mo5608dq().mo3184b(fRQ, j);
    }

    /* renamed from: m */
    private void m315m(long j) {
        bFf().mo5608dq().mo3184b(f59la, j);
    }

    /* access modifiers changed from: private */
    /* renamed from: rB */
    public void m316rB(int i) {
        bFf().mo5608dq().mo3183b(fRK, i);
    }

    /* renamed from: rC */
    private void m317rC(int i) {
        bFf().mo5608dq().mo3183b(fRM, i);
    }

    /* renamed from: rD */
    private void m318rD(int i) {
        bFf().mo5608dq().mo3183b(fRN, i);
    }

    /* renamed from: rE */
    private void m319rE(int i) {
        bFf().mo5608dq().mo3183b(fRO, i);
    }

    /* renamed from: rF */
    private void m320rF(int i) {
        bFf().mo5608dq().mo3183b(fRP, i);
    }

    /* renamed from: S */
    public void mo10S() {
        super.mo10S();
    }

    /* renamed from: T */
    public C6494any mo11T() {
        return ___iScriptClass;
    }

    /* renamed from: U */
    public C2491fm[] mo12U() {
        return _m_methods;
    }

    /* renamed from: W */
    public Object mo13W() {
        return new C2788k(this);
    }

    /* renamed from: a */
    public Object mo14a(C0495Gr gr) {
        Object[] args = gr.getArgs();
        switch (gr.mo2417hq() - TaikodomObject._m_methodCount) {
            case 0:
                m303fg();
                return null;
            case 1:
                return cff();
            case 2:
                cfh();
                return null;
            case 3:
                cfj();
                return null;
            case 4:
                cfl();
                return null;
            case 5:
                cfn();
                return null;
            case 6:
                cfp();
                return null;
            case 7:
                m299c((C0024b) args[0]);
                return null;
            case 8:
                cfr();
                return null;
            case 9:
                m308hE(((Long) args[0]).longValue());
                return null;
            case 10:
                m309hG(((Long) args[0]).longValue());
                return null;
            case 11:
                aAl();
                return null;
            case 12:
                return m310hI(((Long) args[0]).longValue());
            case 13:
                return cft();
            case 14:
                return cfv();
            case 15:
                return new Integer(cfx());
            default:
                return super.mo14a(gr);
        }
    }

    /* renamed from: a */
    public void mo15a(Collection collection, C0495Gr gr) {
        super.mo15a(collection, gr);
    }

    /* access modifiers changed from: package-private */
    @C5566aOg
    public void aAm() {
        switch (bFf().mo6893i(cpB)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, cpB, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, cpB, new Object[0]));
                break;
        }
        aAl();
    }

    /* renamed from: c */
    public C5663aRz[] mo25c() {
        return _m_fields;
    }

    /* access modifiers changed from: package-private */
    public String cfg() {
        switch (bFf().mo6893i(fRW)) {
            case 0:
                return null;
            case 2:
                return (String) bFf().mo5606d(new aCE(this, fRW, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, fRW, new Object[0]));
                break;
        }
        return cff();
    }

    /* access modifiers changed from: package-private */
    @C5566aOg
    public void cfi() {
        switch (bFf().mo6893i(fRX)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, fRX, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, fRX, new Object[0]));
                break;
        }
        cfh();
    }

    /* access modifiers changed from: package-private */
    @C5566aOg
    public void cfk() {
        switch (bFf().mo6893i(fRY)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, fRY, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, fRY, new Object[0]));
                break;
        }
        cfj();
    }

    /* access modifiers changed from: package-private */
    @C5566aOg
    public void cfm() {
        switch (bFf().mo6893i(fRZ)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, fRZ, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, fRZ, new Object[0]));
                break;
        }
        cfl();
    }

    /* access modifiers changed from: package-private */
    @C5566aOg
    public void cfo() {
        switch (bFf().mo6893i(fSa)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, fSa, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, fSa, new Object[0]));
                break;
        }
        cfn();
    }

    /* access modifiers changed from: package-private */
    @C5566aOg
    public void cfq() {
        switch (bFf().mo6893i(fSb)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, fSb, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, fSb, new Object[0]));
                break;
        }
        cfp();
    }

    @C4034yP
    @C5566aOg
    @C2499fr
    public void cfs() {
        switch (bFf().mo6893i(fSd)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, fSd, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, fSd, new Object[0]));
                break;
        }
        cfr();
    }

    public int cfy() {
        switch (bFf().mo6893i(fSj)) {
            case 0:
                return 0;
            case 2:
                return ((Integer) bFf().mo5606d(new aCE(this, fSj, new Object[0]))).intValue();
            case 3:
                bFf().mo5606d(new aCE(this, fSj, new Object[0]));
                break;
        }
        return cfx();
    }

    @C5566aOg
    /* renamed from: d */
    public void mo210d(C0024b bVar) {
        switch (bFf().mo6893i(fSc)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, fSc, new Object[]{bVar}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, fSc, new Object[]{bVar}));
                break;
        }
        m299c(bVar);
    }

    public void dispose() {
        switch (bFf().mo6893i(_f_dispose_0020_0028_0029V)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, _f_dispose_0020_0028_0029V, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, _f_dispose_0020_0028_0029V, new Object[0]));
                break;
        }
        m303fg();
    }

    /* access modifiers changed from: package-private */
    @C5566aOg
    /* renamed from: hF */
    public void mo211hF(long j) {
        switch (bFf().mo6893i(fSe)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, fSe, new Object[]{new Long(j)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, fSe, new Object[]{new Long(j)}));
                break;
        }
        m308hE(j);
    }

    /* access modifiers changed from: package-private */
    @C5566aOg
    /* renamed from: hH */
    public void mo212hH(long j) {
        switch (bFf().mo6893i(fSf)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, fSf, new Object[]{new Long(j)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, fSf, new Object[]{new Long(j)}));
                break;
        }
        m309hG(j);
    }

    /* access modifiers changed from: package-private */
    /* renamed from: b */
    public void mo201b(Player aku) {
        super.mo10S();
        m297a(aku);
        m312hx(-1);
        mo2066a((C5878acG) new C6444anA(aku, aku.bOx().getUsername()));
    }

    @C0064Am(aul = "b2d2fe39533fa70f90a63e99db6d1d54", aum = 0)
    /* renamed from: fg */
    private void m303fg() {
        if (cfe() != null) {
            mo210d(C0024b.SERVER_CRASH);
        }
        m312hx(cVr());
        mo2066a((C5878acG) new C6974axn(m301dG(), ceT(), m302eH(), ceS(), ceV(), ceW(), ceX(), ceY(), ceZ(), cfa(), cfb(), cfc(), cfd()));
        super.dispose();
    }

    @C0064Am(aul = "2cb697660180c5edbd0a397a115ca796", aum = 0)
    private String cff() {
        if (cfe() == null) {
            return m311hJ(ceU());
        }
        return m311hJ((cVr() - cfe().axc()) + ceU());
    }

    @C0064Am(aul = "18c0d045eecf150c068c06ff241f401e", aum = 0)
    /* renamed from: hI */
    private String m310hI(long j) {
        long j2 = j / 1000;
        long j3 = j2 % 60;
        int floor = (int) Math.floor((double) ((j2 % 3600) / 60));
        StringBuffer stringBuffer = new StringBuffer();
        stringBuffer.append((int) Math.floor((double) (j2 / 3600)));
        stringBuffer.append("h ");
        stringBuffer.append(floor < 10 ? "0" : "");
        stringBuffer.append(floor);
        stringBuffer.append("min ");
        stringBuffer.append(j3 < 10 ? "0" : "");
        stringBuffer.append(j3);
        stringBuffer.append("s");
        return stringBuffer.toString();
    }

    @C0064Am(aul = "3cb1c571e2176e4ee55eda59b68bef88", aum = 0)
    private String cft() {
        Actor bhE = m301dG().bhE();
        if (bhE instanceof Ship) {
            return "<<Undocked>>";
        }
        if (bhE instanceof Station) {
            return ((Station) bhE).getName();
        }
        return "<<Unknown location (" + (bhE == null ? "null" : bhE.getClass().getSimpleName()) + ")>>";
    }

    @C0064Am(aul = "6cf063d856bf587a200961da6aaea622", aum = 0)
    private String cfv() {
        Actor bhE = m301dG().bhE();
        if (bhE == null) {
            return "<<Null location>>";
        }
        Node cLb = bhE.cLb();
        return cLb == null ? "<<Null node>>" : cLb.mo21665ke().get();
    }

    @C0064Am(aul = "129a6dad0141fbd80f965a112ee79907", aum = 0)
    private int cfx() {
        return ceT();
    }

    /* renamed from: a.AL$b */
    /* compiled from: a */
    public enum C0024b {
        PLAYER_LOGOUT,
        PLAYER_KICKED,
        SERVER_SHUTDOWN,
        SERVER_CRASH
    }

    @C6485anp
    @C5511aMd
    /* renamed from: a.AL$a */
    public class PlayerSession extends aDJ implements C1616Xf {
        public static final C2491fm _f_start_0020_0028_0029V = null;
        public static final int _m_fieldCount = 0;
        public static final C5663aRz[] _m_fields = null;
        public static final int _m_methodCount = 0;
        public static final C2491fm[] _m_methods = null;
        /* renamed from: aT */
        public static final C5663aRz f60aT = null;
        public static final C5663aRz ciS = null;
        public static final C5663aRz ciU = null;
        public static final C5663aRz ciW = null;
        public static final C5663aRz ciY = null;
        public static final C5663aRz ciZ = null;
        public static final C2491fm cjC = null;
        public static final C2491fm cjD = null;
        public static final C2491fm cjE = null;
        public static final C2491fm cjF = null;
        public static final C2491fm cjG = null;
        public static final C2491fm cjH = null;
        public static final C2491fm cjI = null;
        public static final C2491fm cjJ = null;
        public static final C2491fm cjK = null;
        public static final C2491fm cjL = null;
        public static final C2491fm cjM = null;
        public static final C2491fm cjN = null;
        public static final C2491fm cjO = null;
        public static final C2491fm cjP = null;
        public static final C2491fm cjQ = null;
        public static final C2491fm cjR = null;
        public static final C2491fm cjS = null;
        public static final C2491fm cjT = null;
        public static final C2491fm cjU = null;
        public static final C2491fm cjV = null;
        public static final C2491fm cjW = null;
        public static final C2491fm cjX = null;
        public static final C2491fm cjY = null;
        public static final C2491fm cjZ = null;
        public static final C5663aRz cjb = null;
        public static final C5663aRz cjd = null;
        public static final C5663aRz cjf = null;
        public static final C5663aRz cjh = null;
        public static final C5663aRz cjj = null;
        public static final C5663aRz cjl = null;
        public static final C5663aRz cjn = null;
        public static final C5663aRz cjp = null;
        public static final C5663aRz cjr = null;
        public static final C5663aRz cjt = null;
        public static final C5663aRz cjv = null;
        public static final C5663aRz cjx = null;
        public static final C5663aRz cjz = null;
        public static final C2491fm cka = null;
        public static final C2491fm ckb = null;
        public static final C2491fm ckc = null;
        public static final C2491fm ckd = null;
        public static final C2491fm cke = null;
        public static final long serialVersionUID = 0;
        public static C6494any ___iScriptClass;
        @C0064Am(aul = "6311a457ece10e9b611eb9438b6d3ecb", aum = 18)
        static /* synthetic */ PlayerSessionLog cjA;
        @C0064Am(aul = "4df3892d223a798b104e5ead07a9dfce", aum = 4)
        private static long bGr;
        @C0064Am(aul = "c6971e975aacdfd7fd0de6d423547c8e", aum = 0)
        private static long ciR;
        @C0064Am(aul = "f56a62614dacdeb6e1de9ce2501b1ef8", aum = 1)
        private static long ciT;
        @C0064Am(aul = "d4d9cf7386682977148cc972556bebde", aum = 2)
        private static long ciV;
        @C0064Am(aul = "ab641fdf3ad6ad9118efda6c8f52d96b", aum = 3)
        private static long ciX;
        private static /* synthetic */ int[] cjB;
        @C0064Am(aul = "13b949ca6f82402f128036564efdfd84", aum = 5)
        private static long cja;
        @C0064Am(aul = "acf33eac0d5dc4b3522c449015230ebe", aum = 6)
        private static int cjc;
        @C0064Am(aul = "4892722a529146a797f85a569ac5b776", aum = 7)
        private static int cje;
        @C0064Am(aul = "2821782398f477b7142c235499e06ab7", aum = 8)
        private static int cjg;
        @C0064Am(aul = "a392e3eec02029fe79750fac6b27cb62", aum = 9)
        private static long cji;
        @C0064Am(aul = "c0692e7d3e6e8984fe93f978e7bca0c1", aum = 10)
        private static int cjk;
        @C0064Am(aul = "ea8709b1b686944b20b0c96b869bbcef", aum = 11)
        private static int cjm;
        @C0064Am(aul = "84b16e59d20787120a07076d46876104", aum = 12)
        private static int cjo;
        @C0064Am(aul = "2803c6a63e9b928e4513d9853d0755c0", aum = 13)
        private static int cjq;
        @C0064Am(aul = "71b42f2df66ae7c5f92918673951b236", aum = 14)
        private static int cjs;
        @C0064Am(aul = "fad1d623f7d1f43c7bba25d74d01a184", aum = 15)
        private static int cju;
        @C0064Am(aul = "427c33798805f698400ddf668bf2a98c", aum = 16)
        private static int cjw;
        @C0064Am(aul = "16df34ff59514c8ad0475a1a302c3f97", aum = 17)
        private static int cjy;

        static {
            m332V();
        }

        public PlayerSession() {
            super((C5540aNg) null);
            super.mo10S();
        }

        PlayerSession(PlayerSessionLog al) {
            super((C5540aNg) null);
            super._m_script_init(al);
        }

        public PlayerSession(C5540aNg ang) {
            super(ang);
        }

        /* renamed from: V */
        static void m332V() {
            serialVersionUID = (long) 1;
            _m_fieldCount = aDJ._m_fieldCount + 19;
            _m_methodCount = aDJ._m_methodCount + 30;
            int i = aDJ._m_fieldCount;
            C5663aRz[] arzArr = new C5663aRz[(i + 19)];
            C5663aRz b = C5640aRc.m17844b(PlayerSession.class, "c6971e975aacdfd7fd0de6d423547c8e", i);
            ciS = b;
            arzArr[i] = b;
            int i2 = i + 1;
            C5663aRz b2 = C5640aRc.m17844b(PlayerSession.class, "f56a62614dacdeb6e1de9ce2501b1ef8", i2);
            ciU = b2;
            arzArr[i2] = b2;
            int i3 = i2 + 1;
            C5663aRz b3 = C5640aRc.m17844b(PlayerSession.class, "d4d9cf7386682977148cc972556bebde", i3);
            ciW = b3;
            arzArr[i3] = b3;
            int i4 = i3 + 1;
            C5663aRz b4 = C5640aRc.m17844b(PlayerSession.class, "ab641fdf3ad6ad9118efda6c8f52d96b", i4);
            ciY = b4;
            arzArr[i4] = b4;
            int i5 = i4 + 1;
            C5663aRz b5 = C5640aRc.m17844b(PlayerSession.class, "4df3892d223a798b104e5ead07a9dfce", i5);
            ciZ = b5;
            arzArr[i5] = b5;
            int i6 = i5 + 1;
            C5663aRz b6 = C5640aRc.m17844b(PlayerSession.class, "13b949ca6f82402f128036564efdfd84", i6);
            cjb = b6;
            arzArr[i6] = b6;
            int i7 = i6 + 1;
            C5663aRz b7 = C5640aRc.m17844b(PlayerSession.class, "acf33eac0d5dc4b3522c449015230ebe", i7);
            cjd = b7;
            arzArr[i7] = b7;
            int i8 = i7 + 1;
            C5663aRz b8 = C5640aRc.m17844b(PlayerSession.class, "4892722a529146a797f85a569ac5b776", i8);
            cjf = b8;
            arzArr[i8] = b8;
            int i9 = i8 + 1;
            C5663aRz b9 = C5640aRc.m17844b(PlayerSession.class, "2821782398f477b7142c235499e06ab7", i9);
            cjh = b9;
            arzArr[i9] = b9;
            int i10 = i9 + 1;
            C5663aRz b10 = C5640aRc.m17844b(PlayerSession.class, "a392e3eec02029fe79750fac6b27cb62", i10);
            cjj = b10;
            arzArr[i10] = b10;
            int i11 = i10 + 1;
            C5663aRz b11 = C5640aRc.m17844b(PlayerSession.class, "c0692e7d3e6e8984fe93f978e7bca0c1", i11);
            cjl = b11;
            arzArr[i11] = b11;
            int i12 = i11 + 1;
            C5663aRz b12 = C5640aRc.m17844b(PlayerSession.class, "ea8709b1b686944b20b0c96b869bbcef", i12);
            cjn = b12;
            arzArr[i12] = b12;
            int i13 = i12 + 1;
            C5663aRz b13 = C5640aRc.m17844b(PlayerSession.class, "84b16e59d20787120a07076d46876104", i13);
            cjp = b13;
            arzArr[i13] = b13;
            int i14 = i13 + 1;
            C5663aRz b14 = C5640aRc.m17844b(PlayerSession.class, "2803c6a63e9b928e4513d9853d0755c0", i14);
            cjr = b14;
            arzArr[i14] = b14;
            int i15 = i14 + 1;
            C5663aRz b15 = C5640aRc.m17844b(PlayerSession.class, "71b42f2df66ae7c5f92918673951b236", i15);
            cjt = b15;
            arzArr[i15] = b15;
            int i16 = i15 + 1;
            C5663aRz b16 = C5640aRc.m17844b(PlayerSession.class, "fad1d623f7d1f43c7bba25d74d01a184", i16);
            cjv = b16;
            arzArr[i16] = b16;
            int i17 = i16 + 1;
            C5663aRz b17 = C5640aRc.m17844b(PlayerSession.class, "427c33798805f698400ddf668bf2a98c", i17);
            cjx = b17;
            arzArr[i17] = b17;
            int i18 = i17 + 1;
            C5663aRz b18 = C5640aRc.m17844b(PlayerSession.class, "16df34ff59514c8ad0475a1a302c3f97", i18);
            cjz = b18;
            arzArr[i18] = b18;
            int i19 = i18 + 1;
            C5663aRz b19 = C5640aRc.m17844b(PlayerSession.class, "6311a457ece10e9b611eb9438b6d3ecb", i19);
            f60aT = b19;
            arzArr[i19] = b19;
            int i20 = i19 + 1;
            _m_fields = arzArr;
            C1634Xv.m11725a((Object[]) aDJ._m_fields, (Object[]) _m_fields);
            int i21 = aDJ._m_methodCount;
            C2491fm[] fmVarArr = new C2491fm[(i21 + 30)];
            C2491fm a = C4105zY.m41624a(PlayerSession.class, "6611273e5b7e1808257093426e2d26ed", i21);
            cjC = a;
            fmVarArr[i21] = a;
            int i22 = i21 + 1;
            C2491fm a2 = C4105zY.m41624a(PlayerSession.class, "dfc31de79e6e67d0a897872c41afce57", i22);
            cjD = a2;
            fmVarArr[i22] = a2;
            int i23 = i22 + 1;
            C2491fm a3 = C4105zY.m41624a(PlayerSession.class, "0221fba8812a3b73a01073a3a8ba4e0d", i23);
            cjE = a3;
            fmVarArr[i23] = a3;
            int i24 = i23 + 1;
            C2491fm a4 = C4105zY.m41624a(PlayerSession.class, "8af3846dcb6164821863266696af625c", i24);
            cjF = a4;
            fmVarArr[i24] = a4;
            int i25 = i24 + 1;
            C2491fm a5 = C4105zY.m41624a(PlayerSession.class, "e06bab71ba627fc6b327e0424497dbef", i25);
            cjG = a5;
            fmVarArr[i25] = a5;
            int i26 = i25 + 1;
            C2491fm a6 = C4105zY.m41624a(PlayerSession.class, "c28297c0fb95faeaf3777c90375b84d0", i26);
            cjH = a6;
            fmVarArr[i26] = a6;
            int i27 = i26 + 1;
            C2491fm a7 = C4105zY.m41624a(PlayerSession.class, "09ee877ebae22ad24358ee867238548b", i27);
            cjI = a7;
            fmVarArr[i27] = a7;
            int i28 = i27 + 1;
            C2491fm a8 = C4105zY.m41624a(PlayerSession.class, "8fda70e49f1575db180a65a5d98567fd", i28);
            cjJ = a8;
            fmVarArr[i28] = a8;
            int i29 = i28 + 1;
            C2491fm a9 = C4105zY.m41624a(PlayerSession.class, "6434c788b1205f4af71e405d67126a99", i29);
            cjK = a9;
            fmVarArr[i29] = a9;
            int i30 = i29 + 1;
            C2491fm a10 = C4105zY.m41624a(PlayerSession.class, "ea2b89d49310dbdb4aba13053bafc3c5", i30);
            cjL = a10;
            fmVarArr[i30] = a10;
            int i31 = i30 + 1;
            C2491fm a11 = C4105zY.m41624a(PlayerSession.class, "5056f38d7ba3a8e2142fba51cd9481b4", i31);
            cjM = a11;
            fmVarArr[i31] = a11;
            int i32 = i31 + 1;
            C2491fm a12 = C4105zY.m41624a(PlayerSession.class, "c6c5d680598ca72118ac8ee2717e2b53", i32);
            cjN = a12;
            fmVarArr[i32] = a12;
            int i33 = i32 + 1;
            C2491fm a13 = C4105zY.m41624a(PlayerSession.class, "dd1b2217bdcc4a3b0189cfd9fcd0aabf", i33);
            cjO = a13;
            fmVarArr[i33] = a13;
            int i34 = i33 + 1;
            C2491fm a14 = C4105zY.m41624a(PlayerSession.class, "89bfe8d64832e46e3ff18112e7bcd2f6", i34);
            cjP = a14;
            fmVarArr[i34] = a14;
            int i35 = i34 + 1;
            C2491fm a15 = C4105zY.m41624a(PlayerSession.class, "726bf5a8c9ea447d7a7870a9605060ba", i35);
            cjQ = a15;
            fmVarArr[i35] = a15;
            int i36 = i35 + 1;
            C2491fm a16 = C4105zY.m41624a(PlayerSession.class, "57bc9915a8a682dbf2ce26c7ffc36cb6", i36);
            cjR = a16;
            fmVarArr[i36] = a16;
            int i37 = i36 + 1;
            C2491fm a17 = C4105zY.m41624a(PlayerSession.class, "8d94ab0db661fc2ea5365d1da1591cd6", i37);
            cjS = a17;
            fmVarArr[i37] = a17;
            int i38 = i37 + 1;
            C2491fm a18 = C4105zY.m41624a(PlayerSession.class, "f9f5c0fe9b691cc1ccdeb036605d2c05", i38);
            cjT = a18;
            fmVarArr[i38] = a18;
            int i39 = i38 + 1;
            C2491fm a19 = C4105zY.m41624a(PlayerSession.class, "deccb5551d5be49f0c3b03cbf8bf8a44", i39);
            cjU = a19;
            fmVarArr[i39] = a19;
            int i40 = i39 + 1;
            C2491fm a20 = C4105zY.m41624a(PlayerSession.class, "0f8fccbbaa2f699ecc1b1ec4700cd648", i40);
            cjV = a20;
            fmVarArr[i40] = a20;
            int i41 = i40 + 1;
            C2491fm a21 = C4105zY.m41624a(PlayerSession.class, "f8483abcae9347aa437bd116c114fa71", i41);
            cjW = a21;
            fmVarArr[i41] = a21;
            int i42 = i41 + 1;
            C2491fm a22 = C4105zY.m41624a(PlayerSession.class, "d352eb2842c64d31fbe7e90d20503b1c", i42);
            cjX = a22;
            fmVarArr[i42] = a22;
            int i43 = i42 + 1;
            C2491fm a23 = C4105zY.m41624a(PlayerSession.class, "4f5c930010aee840e0f7ecc4b2dc6f24", i43);
            cjY = a23;
            fmVarArr[i43] = a23;
            int i44 = i43 + 1;
            C2491fm a24 = C4105zY.m41624a(PlayerSession.class, "9afa26292f337d71176a2395c02c9f16", i44);
            cjZ = a24;
            fmVarArr[i44] = a24;
            int i45 = i44 + 1;
            C2491fm a25 = C4105zY.m41624a(PlayerSession.class, "c895361196cac0be2e62a88abd5002a4", i45);
            _f_start_0020_0028_0029V = a25;
            fmVarArr[i45] = a25;
            int i46 = i45 + 1;
            C2491fm a26 = C4105zY.m41624a(PlayerSession.class, "7cd2ea869d1814c0dc2948cf5bcdbc14", i46);
            cka = a26;
            fmVarArr[i46] = a26;
            int i47 = i46 + 1;
            C2491fm a27 = C4105zY.m41624a(PlayerSession.class, "dd86c2325b70ec007dcb80267bc8eaa2", i47);
            ckb = a27;
            fmVarArr[i47] = a27;
            int i48 = i47 + 1;
            C2491fm a28 = C4105zY.m41624a(PlayerSession.class, "70ad4b93a1e595cab8fc9d2d1c678761", i48);
            ckc = a28;
            fmVarArr[i48] = a28;
            int i49 = i48 + 1;
            C2491fm a29 = C4105zY.m41624a(PlayerSession.class, "5f28c8e0260a6f217d63cc087ae41df3", i49);
            ckd = a29;
            fmVarArr[i49] = a29;
            int i50 = i49 + 1;
            C2491fm a30 = C4105zY.m41624a(PlayerSession.class, "33a94021240b39c2d3bd00b83c68c65f", i50);
            cke = a30;
            fmVarArr[i50] = a30;
            int i51 = i50 + 1;
            _m_methods = fmVarArr;
            C1634Xv.m11725a((Object[]) aDJ._m_methods, (Object[]) _m_methods);
            ___iScriptClass = aUO.m18566a(PlayerSession.class, aPN.class, _m_fields, _m_methods);
        }

        static /* synthetic */ int[] axR() {
            int[] iArr = cjB;
            if (iArr == null) {
                iArr = new int[C0024b.values().length];
                try {
                    iArr[C0024b.PLAYER_KICKED.ordinal()] = 2;
                } catch (NoSuchFieldError e) {
                }
                try {
                    iArr[C0024b.PLAYER_LOGOUT.ordinal()] = 1;
                } catch (NoSuchFieldError e2) {
                }
                try {
                    iArr[C0024b.SERVER_CRASH.ordinal()] = 4;
                } catch (NoSuchFieldError e3) {
                }
                try {
                    iArr[C0024b.SERVER_SHUTDOWN.ordinal()] = 3;
                } catch (NoSuchFieldError e4) {
                }
                cjB = iArr;
            }
            return iArr;
        }

        /* renamed from: a */
        private void m334a(PlayerSessionLog al) {
            bFf().mo5608dq().mo3197f(f60aT, al);
        }

        private long awA() {
            return bFf().mo5608dq().mo3213o(ciS);
        }

        private long awB() {
            return bFf().mo5608dq().mo3213o(ciU);
        }

        private long awC() {
            return bFf().mo5608dq().mo3213o(ciW);
        }

        private long awD() {
            return bFf().mo5608dq().mo3213o(ciY);
        }

        private long awE() {
            return bFf().mo5608dq().mo3213o(ciZ);
        }

        private long awF() {
            return bFf().mo5608dq().mo3213o(cjb);
        }

        private int awG() {
            return bFf().mo5608dq().mo3212n(cjd);
        }

        private int awH() {
            return bFf().mo5608dq().mo3212n(cjf);
        }

        private int awI() {
            return bFf().mo5608dq().mo3212n(cjh);
        }

        private long awJ() {
            return bFf().mo5608dq().mo3213o(cjj);
        }

        private int awK() {
            return bFf().mo5608dq().mo3212n(cjl);
        }

        private int awL() {
            return bFf().mo5608dq().mo3212n(cjn);
        }

        private int awM() {
            return bFf().mo5608dq().mo3212n(cjp);
        }

        private int awN() {
            return bFf().mo5608dq().mo3212n(cjr);
        }

        private int awO() {
            return bFf().mo5608dq().mo3212n(cjt);
        }

        private int awP() {
            return bFf().mo5608dq().mo3212n(cjv);
        }

        private int awQ() {
            return bFf().mo5608dq().mo3212n(cjx);
        }

        private int awR() {
            return bFf().mo5608dq().mo3212n(cjz);
        }

        private PlayerSessionLog awS() {
            return (PlayerSessionLog) bFf().mo5608dq().mo3214p(f60aT);
        }

        /* renamed from: dL */
        private void m335dL(long j) {
            bFf().mo5608dq().mo3184b(ciS, j);
        }

        /* renamed from: dM */
        private void m336dM(long j) {
            bFf().mo5608dq().mo3184b(ciU, j);
        }

        /* renamed from: dN */
        private void m337dN(long j) {
            bFf().mo5608dq().mo3184b(ciW, j);
        }

        /* renamed from: dO */
        private void m338dO(long j) {
            bFf().mo5608dq().mo3184b(ciY, j);
        }

        /* renamed from: dP */
        private void m339dP(long j) {
            bFf().mo5608dq().mo3184b(ciZ, j);
        }

        /* renamed from: dQ */
        private void m340dQ(long j) {
            bFf().mo5608dq().mo3184b(cjb, j);
        }

        /* renamed from: dR */
        private void m341dR(long j) {
            bFf().mo5608dq().mo3184b(cjj, j);
        }

        /* renamed from: gc */
        private void m345gc(int i) {
            bFf().mo5608dq().mo3183b(cjd, i);
        }

        /* renamed from: gd */
        private void m346gd(int i) {
            bFf().mo5608dq().mo3183b(cjf, i);
        }

        /* renamed from: ge */
        private void m347ge(int i) {
            bFf().mo5608dq().mo3183b(cjh, i);
        }

        /* renamed from: gf */
        private void m348gf(int i) {
            bFf().mo5608dq().mo3183b(cjl, i);
        }

        /* renamed from: gg */
        private void m349gg(int i) {
            bFf().mo5608dq().mo3183b(cjn, i);
        }

        /* renamed from: gh */
        private void m350gh(int i) {
            bFf().mo5608dq().mo3183b(cjp, i);
        }

        /* renamed from: gi */
        private void m351gi(int i) {
            bFf().mo5608dq().mo3183b(cjr, i);
        }

        /* renamed from: gj */
        private void m352gj(int i) {
            bFf().mo5608dq().mo3183b(cjt, i);
        }

        /* renamed from: gk */
        private void m353gk(int i) {
            bFf().mo5608dq().mo3183b(cjv, i);
        }

        /* renamed from: gl */
        private void m354gl(int i) {
            bFf().mo5608dq().mo3183b(cjx, i);
        }

        /* renamed from: gm */
        private void m355gm(int i) {
            bFf().mo5608dq().mo3183b(cjz, i);
        }

        /* renamed from: S */
        public void mo10S() {
            super.mo10S();
        }

        /* renamed from: T */
        public C6494any mo11T() {
            return ___iScriptClass;
        }

        /* renamed from: U */
        public C2491fm[] mo12U() {
            return _m_methods;
        }

        /* renamed from: W */
        public Object mo13W() {
            return new aPN(this);
        }

        /* renamed from: a */
        public Object mo14a(C0495Gr gr) {
            Object[] args = gr.getArgs();
            switch (gr.mo2417hq() - aDJ._m_methodCount) {
                case 0:
                    awT();
                    return null;
                case 1:
                    awV();
                    return null;
                case 2:
                    awX();
                    return null;
                case 3:
                    awZ();
                    return null;
                case 4:
                    m342dS(((Long) args[0]).longValue());
                    return null;
                case 5:
                    return new Long(axb());
                case 6:
                    return new Long(axd());
                case 7:
                    return new Integer(axf());
                case 8:
                    return new Integer(axh());
                case 9:
                    return new Integer(axj());
                case 10:
                    return new Integer(axl());
                case 11:
                    return new Long(axn());
                case 12:
                    return new Long(axp());
                case 13:
                    return new Long(axr());
                case 14:
                    m343dU(((Long) args[0]).longValue());
                    return null;
                case 15:
                    m344dW(((Long) args[0]).longValue());
                    return null;
                case 16:
                    axt();
                    return null;
                case 17:
                    return new Long(axv());
                case 18:
                    return new Long(axx());
                case 19:
                    return new Integer(axz());
                case 20:
                    return new Integer(axB());
                case 21:
                    return new Integer(axD());
                case 22:
                    return new Integer(axF());
                case 23:
                    return new Integer(axH());
                case 24:
                    m356qN();
                    return null;
                case 25:
                    m333a((C0024b) args[0]);
                    return null;
                case 26:
                    return new Integer(axJ());
                case 27:
                    return new Integer(axL());
                case 28:
                    return new Integer(axN());
                case 29:
                    return new Integer(axP());
                default:
                    return super.mo14a(gr);
            }
        }

        /* renamed from: a */
        public void mo15a(Collection collection, C0495Gr gr) {
            super.mo15a(collection, gr);
        }

        public void awU() {
            switch (bFf().mo6893i(cjC)) {
                case 0:
                    return;
                case 2:
                    bFf().mo5606d(new aCE(this, cjC, new Object[0]));
                    return;
                case 3:
                    bFf().mo5606d(new aCE(this, cjC, new Object[0]));
                    break;
            }
            awT();
        }

        public void awW() {
            switch (bFf().mo6893i(cjD)) {
                case 0:
                    return;
                case 2:
                    bFf().mo5606d(new aCE(this, cjD, new Object[0]));
                    return;
                case 3:
                    bFf().mo5606d(new aCE(this, cjD, new Object[0]));
                    break;
            }
            awV();
        }

        public void awY() {
            switch (bFf().mo6893i(cjE)) {
                case 0:
                    return;
                case 2:
                    bFf().mo5606d(new aCE(this, cjE, new Object[0]));
                    return;
                case 3:
                    bFf().mo5606d(new aCE(this, cjE, new Object[0]));
                    break;
            }
            awX();
        }

        /* access modifiers changed from: package-private */
        public int axA() {
            switch (bFf().mo6893i(cjV)) {
                case 0:
                    return 0;
                case 2:
                    return ((Integer) bFf().mo5606d(new aCE(this, cjV, new Object[0]))).intValue();
                case 3:
                    bFf().mo5606d(new aCE(this, cjV, new Object[0]));
                    break;
            }
            return axz();
        }

        /* access modifiers changed from: package-private */
        public int axC() {
            switch (bFf().mo6893i(cjW)) {
                case 0:
                    return 0;
                case 2:
                    return ((Integer) bFf().mo5606d(new aCE(this, cjW, new Object[0]))).intValue();
                case 3:
                    bFf().mo5606d(new aCE(this, cjW, new Object[0]));
                    break;
            }
            return axB();
        }

        /* access modifiers changed from: package-private */
        public int axE() {
            switch (bFf().mo6893i(cjX)) {
                case 0:
                    return 0;
                case 2:
                    return ((Integer) bFf().mo5606d(new aCE(this, cjX, new Object[0]))).intValue();
                case 3:
                    bFf().mo5606d(new aCE(this, cjX, new Object[0]));
                    break;
            }
            return axD();
        }

        /* access modifiers changed from: package-private */
        public int axG() {
            switch (bFf().mo6893i(cjY)) {
                case 0:
                    return 0;
                case 2:
                    return ((Integer) bFf().mo5606d(new aCE(this, cjY, new Object[0]))).intValue();
                case 3:
                    bFf().mo5606d(new aCE(this, cjY, new Object[0]));
                    break;
            }
            return axF();
        }

        /* access modifiers changed from: package-private */
        public int axI() {
            switch (bFf().mo6893i(cjZ)) {
                case 0:
                    return 0;
                case 2:
                    return ((Integer) bFf().mo5606d(new aCE(this, cjZ, new Object[0]))).intValue();
                case 3:
                    bFf().mo5606d(new aCE(this, cjZ, new Object[0]));
                    break;
            }
            return axH();
        }

        public int axK() {
            switch (bFf().mo6893i(ckb)) {
                case 0:
                    return 0;
                case 2:
                    return ((Integer) bFf().mo5606d(new aCE(this, ckb, new Object[0]))).intValue();
                case 3:
                    bFf().mo5606d(new aCE(this, ckb, new Object[0]));
                    break;
            }
            return axJ();
        }

        public int axM() {
            switch (bFf().mo6893i(ckc)) {
                case 0:
                    return 0;
                case 2:
                    return ((Integer) bFf().mo5606d(new aCE(this, ckc, new Object[0]))).intValue();
                case 3:
                    bFf().mo5606d(new aCE(this, ckc, new Object[0]));
                    break;
            }
            return axL();
        }

        public int axO() {
            switch (bFf().mo6893i(ckd)) {
                case 0:
                    return 0;
                case 2:
                    return ((Integer) bFf().mo5606d(new aCE(this, ckd, new Object[0]))).intValue();
                case 3:
                    bFf().mo5606d(new aCE(this, ckd, new Object[0]));
                    break;
            }
            return axN();
        }

        public int axQ() {
            switch (bFf().mo6893i(cke)) {
                case 0:
                    return 0;
                case 2:
                    return ((Integer) bFf().mo5606d(new aCE(this, cke, new Object[0]))).intValue();
                case 3:
                    bFf().mo5606d(new aCE(this, cke, new Object[0]));
                    break;
            }
            return axP();
        }

        public void axa() {
            switch (bFf().mo6893i(cjF)) {
                case 0:
                    return;
                case 2:
                    bFf().mo5606d(new aCE(this, cjF, new Object[0]));
                    return;
                case 3:
                    bFf().mo5606d(new aCE(this, cjF, new Object[0]));
                    break;
            }
            awZ();
        }

        public long axc() {
            switch (bFf().mo6893i(cjH)) {
                case 0:
                    return 0;
                case 2:
                    return ((Long) bFf().mo5606d(new aCE(this, cjH, new Object[0]))).longValue();
                case 3:
                    bFf().mo5606d(new aCE(this, cjH, new Object[0]));
                    break;
            }
            return axb();
        }

        public long axe() {
            switch (bFf().mo6893i(cjI)) {
                case 0:
                    return 0;
                case 2:
                    return ((Long) bFf().mo5606d(new aCE(this, cjI, new Object[0]))).longValue();
                case 3:
                    bFf().mo5606d(new aCE(this, cjI, new Object[0]));
                    break;
            }
            return axd();
        }

        public int axg() {
            switch (bFf().mo6893i(cjJ)) {
                case 0:
                    return 0;
                case 2:
                    return ((Integer) bFf().mo5606d(new aCE(this, cjJ, new Object[0]))).intValue();
                case 3:
                    bFf().mo5606d(new aCE(this, cjJ, new Object[0]));
                    break;
            }
            return axf();
        }

        public int axi() {
            switch (bFf().mo6893i(cjK)) {
                case 0:
                    return 0;
                case 2:
                    return ((Integer) bFf().mo5606d(new aCE(this, cjK, new Object[0]))).intValue();
                case 3:
                    bFf().mo5606d(new aCE(this, cjK, new Object[0]));
                    break;
            }
            return axh();
        }

        public int axk() {
            switch (bFf().mo6893i(cjL)) {
                case 0:
                    return 0;
                case 2:
                    return ((Integer) bFf().mo5606d(new aCE(this, cjL, new Object[0]))).intValue();
                case 3:
                    bFf().mo5606d(new aCE(this, cjL, new Object[0]));
                    break;
            }
            return axj();
        }

        public int axm() {
            switch (bFf().mo6893i(cjM)) {
                case 0:
                    return 0;
                case 2:
                    return ((Integer) bFf().mo5606d(new aCE(this, cjM, new Object[0]))).intValue();
                case 3:
                    bFf().mo5606d(new aCE(this, cjM, new Object[0]));
                    break;
            }
            return axl();
        }

        public long axo() {
            switch (bFf().mo6893i(cjN)) {
                case 0:
                    return 0;
                case 2:
                    return ((Long) bFf().mo5606d(new aCE(this, cjN, new Object[0]))).longValue();
                case 3:
                    bFf().mo5606d(new aCE(this, cjN, new Object[0]));
                    break;
            }
            return axn();
        }

        public long axq() {
            switch (bFf().mo6893i(cjO)) {
                case 0:
                    return 0;
                case 2:
                    return ((Long) bFf().mo5606d(new aCE(this, cjO, new Object[0]))).longValue();
                case 3:
                    bFf().mo5606d(new aCE(this, cjO, new Object[0]));
                    break;
            }
            return axp();
        }

        /* access modifiers changed from: package-private */
        public long axs() {
            switch (bFf().mo6893i(cjP)) {
                case 0:
                    return 0;
                case 2:
                    return ((Long) bFf().mo5606d(new aCE(this, cjP, new Object[0]))).longValue();
                case 3:
                    bFf().mo5606d(new aCE(this, cjP, new Object[0]));
                    break;
            }
            return axr();
        }

        /* access modifiers changed from: package-private */
        public void axu() {
            switch (bFf().mo6893i(cjS)) {
                case 0:
                    return;
                case 2:
                    bFf().mo5606d(new aCE(this, cjS, new Object[0]));
                    return;
                case 3:
                    bFf().mo5606d(new aCE(this, cjS, new Object[0]));
                    break;
            }
            axt();
        }

        /* access modifiers changed from: package-private */
        public long axw() {
            switch (bFf().mo6893i(cjT)) {
                case 0:
                    return 0;
                case 2:
                    return ((Long) bFf().mo5606d(new aCE(this, cjT, new Object[0]))).longValue();
                case 3:
                    bFf().mo5606d(new aCE(this, cjT, new Object[0]));
                    break;
            }
            return axv();
        }

        /* access modifiers changed from: package-private */
        public long axy() {
            switch (bFf().mo6893i(cjU)) {
                case 0:
                    return 0;
                case 2:
                    return ((Long) bFf().mo5606d(new aCE(this, cjU, new Object[0]))).longValue();
                case 3:
                    bFf().mo5606d(new aCE(this, cjU, new Object[0]));
                    break;
            }
            return axx();
        }

        /* access modifiers changed from: package-private */
        /* renamed from: b */
        public void mo238b(C0024b bVar) {
            switch (bFf().mo6893i(cka)) {
                case 0:
                    return;
                case 2:
                    bFf().mo5606d(new aCE(this, cka, new Object[]{bVar}));
                    return;
                case 3:
                    bFf().mo5606d(new aCE(this, cka, new Object[]{bVar}));
                    break;
            }
            m333a(bVar);
        }

        /* renamed from: c */
        public C5663aRz[] mo25c() {
            return _m_fields;
        }

        /* renamed from: dT */
        public void mo240dT(long j) {
            switch (bFf().mo6893i(cjG)) {
                case 0:
                    return;
                case 2:
                    bFf().mo5606d(new aCE(this, cjG, new Object[]{new Long(j)}));
                    return;
                case 3:
                    bFf().mo5606d(new aCE(this, cjG, new Object[]{new Long(j)}));
                    break;
            }
            m342dS(j);
        }

        /* access modifiers changed from: package-private */
        /* renamed from: dV */
        public void mo241dV(long j) {
            switch (bFf().mo6893i(cjQ)) {
                case 0:
                    return;
                case 2:
                    bFf().mo5606d(new aCE(this, cjQ, new Object[]{new Long(j)}));
                    return;
                case 3:
                    bFf().mo5606d(new aCE(this, cjQ, new Object[]{new Long(j)}));
                    break;
            }
            m343dU(j);
        }

        /* access modifiers changed from: package-private */
        /* renamed from: dX */
        public void mo242dX(long j) {
            switch (bFf().mo6893i(cjR)) {
                case 0:
                    return;
                case 2:
                    bFf().mo5606d(new aCE(this, cjR, new Object[]{new Long(j)}));
                    return;
                case 3:
                    bFf().mo5606d(new aCE(this, cjR, new Object[]{new Long(j)}));
                    break;
            }
            m344dW(j);
        }

        /* access modifiers changed from: package-private */
        public void start() {
            switch (bFf().mo6893i(_f_start_0020_0028_0029V)) {
                case 0:
                    return;
                case 2:
                    bFf().mo5606d(new aCE(this, _f_start_0020_0028_0029V, new Object[0]));
                    return;
                case 3:
                    bFf().mo5606d(new aCE(this, _f_start_0020_0028_0029V, new Object[0]));
                    break;
            }
            m356qN();
        }

        /* access modifiers changed from: package-private */
        /* renamed from: b */
        public void mo239b(PlayerSessionLog al) {
            m334a(al);
            super.mo10S();
            al.m316rB(al.ceT() + 1);
            m341dR(cVr());
        }

        @C0064Am(aul = "6611273e5b7e1808257093426e2d26ed", aum = 0)
        private void awT() {
            m348gf(awK() + 1);
        }

        @C0064Am(aul = "dfc31de79e6e67d0a897872c41afce57", aum = 0)
        private void awV() {
            m349gg(awL() + 1);
        }

        @C0064Am(aul = "0221fba8812a3b73a01073a3a8ba4e0d", aum = 0)
        private void awX() {
            m350gh(awM() + 1);
        }

        @C0064Am(aul = "8af3846dcb6164821863266696af625c", aum = 0)
        private void awZ() {
            m351gi(awN() + 1);
        }

        @C0064Am(aul = "e06bab71ba627fc6b327e0424497dbef", aum = 0)
        /* renamed from: dS */
        private void m342dS(long j) {
            m341dR(j);
        }

        @C0064Am(aul = "c28297c0fb95faeaf3777c90375b84d0", aum = 0)
        private long axb() {
            return awA();
        }

        @C0064Am(aul = "09ee877ebae22ad24358ee867238548b", aum = 0)
        private long axd() {
            return awB();
        }

        @C0064Am(aul = "8fda70e49f1575db180a65a5d98567fd", aum = 0)
        private int axf() {
            return awK();
        }

        @C0064Am(aul = "6434c788b1205f4af71e405d67126a99", aum = 0)
        private int axh() {
            return awL();
        }

        @C0064Am(aul = "ea2b89d49310dbdb4aba13053bafc3c5", aum = 0)
        private int axj() {
            return awM();
        }

        @C0064Am(aul = "5056f38d7ba3a8e2142fba51cd9481b4", aum = 0)
        private int axl() {
            return awN();
        }

        @C0064Am(aul = "c6c5d680598ca72118ac8ee2717e2b53", aum = 0)
        private long axn() {
            return awC();
        }

        @C0064Am(aul = "dd1b2217bdcc4a3b0189cfd9fcd0aabf", aum = 0)
        private long axp() {
            return awD();
        }

        @C0064Am(aul = "89bfe8d64832e46e3ff18112e7bcd2f6", aum = 0)
        private long axr() {
            return awB() - awA();
        }

        @C0064Am(aul = "726bf5a8c9ea447d7a7870a9605060ba", aum = 0)
        /* renamed from: dU */
        private void m343dU(long j) {
            m339dP(awE() + j);
            m345gc(awG() + 1);
        }

        @C0064Am(aul = "57bc9915a8a682dbf2ce26c7ffc36cb6", aum = 0)
        /* renamed from: dW */
        private void m344dW(long j) {
            m340dQ(awF() + j);
            m346gd(awH() + 1);
        }

        @C0064Am(aul = "8d94ab0db661fc2ea5365d1da1591cd6", aum = 0)
        private void axt() {
            m347ge(awI() + 1);
        }

        @C0064Am(aul = "f9f5c0fe9b691cc1ccdeb036605d2c05", aum = 0)
        private long axv() {
            return awE();
        }

        @C0064Am(aul = "deccb5551d5be49f0c3b03cbf8bf8a44", aum = 0)
        private long axx() {
            return awF();
        }

        @C0064Am(aul = "0f8fccbbaa2f699ecc1b1ec4700cd648", aum = 0)
        private int axz() {
            return awG();
        }

        @C0064Am(aul = "f8483abcae9347aa437bd116c114fa71", aum = 0)
        private int axB() {
            return awH();
        }

        @C0064Am(aul = "d352eb2842c64d31fbe7e90d20503b1c", aum = 0)
        private int axD() {
            return awI();
        }

        @C0064Am(aul = "4f5c930010aee840e0f7ecc4b2dc6f24", aum = 0)
        private int axF() {
            if (awG() == 0) {
                return 0;
            }
            return (int) (awE() / ((long) awG()));
        }

        @C0064Am(aul = "9afa26292f337d71176a2395c02c9f16", aum = 0)
        private int axH() {
            if (awH() == 0) {
                return 0;
            }
            return (int) (awF() / ((long) awH()));
        }

        @C0064Am(aul = "c895361196cac0be2e62a88abd5002a4", aum = 0)
        /* renamed from: qN */
        private void m356qN() {
            m335dL(cVr());
            m337dN(awS().m301dG().mo5156Qw().bSs());
            m352gj(awS().m301dG().dxu().cUP().size());
            m353gk(awS().m301dG().dxu().cUR().size());
        }

        @C0064Am(aul = "7cd2ea869d1814c0dc2948cf5bcdbc14", aum = 0)
        /* renamed from: a */
        private void m333a(C0024b bVar) {
            switch (axR()[bVar.ordinal()]) {
                case 1:
                    m336dM(cVr());
                    break;
                case 2:
                    m336dM(cVr());
                    break;
                case 3:
                    m336dM(cVr());
                    break;
                case 4:
                    m336dM(awJ());
                    break;
                default:
                    m336dM(cVr());
                    break;
            }
            m338dO(awS().m301dG().mo5156Qw().bSs());
            m354gl(awS().m301dG().dxu().cUP().size());
            m355gm(awS().m301dG().dxu().cUR().size());
        }

        @C0064Am(aul = "dd86c2325b70ec007dcb80267bc8eaa2", aum = 0)
        private int axJ() {
            return awR();
        }

        @C0064Am(aul = "70ad4b93a1e595cab8fc9d2d1c678761", aum = 0)
        private int axL() {
            return awQ();
        }

        @C0064Am(aul = "5f28c8e0260a6f217d63cc087ae41df3", aum = 0)
        private int axN() {
            return awO();
        }

        @C0064Am(aul = "33a94021240b39c2d3bd00b83c68c65f", aum = 0)
        private int axP() {
            return awP();
        }
    }
}
