package game.script.player;

import game.network.message.externalizable.aCE;
import logic.baa.*;
import logic.data.mbean.C2369eY;
import logic.res.code.C5640aRc;
import logic.res.code.C5663aRz;
import logic.res.code.C6494any;
import logic.res.html.C2491fm;
import p001a.*;

import java.util.Collection;

@C5511aMd
@C6485anp
/* renamed from: a.ajb  reason: case insensitive filesystem */
/* compiled from: a */
public class LDParameter extends aDJ implements C1616Xf {

    /* renamed from: CR */
    public static final C5663aRz f4706CR = null;

    /* renamed from: CS */
    public static final C5663aRz f4707CS = null;

    /* renamed from: CU */
    public static final C2491fm f4708CU = null;

    /* renamed from: CW */
    public static final C2491fm f4709CW = null;

    /* renamed from: CX */
    public static final C2491fm f4710CX = null;
    public static final int _m_fieldCount = 0;
    public static final C5663aRz[] _m_fields = null;
    public static final int _m_methodCount = 0;
    public static final C2491fm[] _m_methods = null;
    public static final long serialVersionUID = 0;
    public static C6494any ___iScriptClass;
    @C0064Am(aul = "db222ad0fe751a8b53ad587db204d462", aum = 0)
    private static String key;
    @C0064Am(aul = "254f6cbedcb5c489658116d979e2f86c", aum = 1)
    private static String value;

    static {
        m22999V();
    }

    public LDParameter() {
        super((C5540aNg) null);
        super.mo10S();
    }

    public LDParameter(C5540aNg ang) {
        super(ang);
    }

    public LDParameter(String str, String str2) {
        super((C5540aNg) null);
        super._m_script_init(str, str2);
    }

    /* renamed from: V */
    static void m22999V() {
        serialVersionUID = (long) 1;
        _m_fieldCount = aDJ._m_fieldCount + 2;
        _m_methodCount = aDJ._m_methodCount + 3;
        int i = aDJ._m_fieldCount;
        C5663aRz[] arzArr = new C5663aRz[(i + 2)];
        C5663aRz b = C5640aRc.m17844b(LDParameter.class, "db222ad0fe751a8b53ad587db204d462", i);
        f4706CR = b;
        arzArr[i] = b;
        int i2 = i + 1;
        C5663aRz b2 = C5640aRc.m17844b(LDParameter.class, "254f6cbedcb5c489658116d979e2f86c", i2);
        f4707CS = b2;
        arzArr[i2] = b2;
        int i3 = i2 + 1;
        _m_fields = arzArr;
        C1634Xv.m11725a((Object[]) aDJ._m_fields, (Object[]) _m_fields);
        int i4 = aDJ._m_methodCount;
        C2491fm[] fmVarArr = new C2491fm[(i4 + 3)];
        C2491fm a = C4105zY.m41624a(LDParameter.class, "5593c1e8854aaae9b48e6f73c19f6c36", i4);
        f4708CU = a;
        fmVarArr[i4] = a;
        int i5 = i4 + 1;
        C2491fm a2 = C4105zY.m41624a(LDParameter.class, "6e279d3350968ac41d6cf0ecd6d74bad", i5);
        f4709CW = a2;
        fmVarArr[i5] = a2;
        int i6 = i5 + 1;
        C2491fm a3 = C4105zY.m41624a(LDParameter.class, "a507e945a8cfe014d960589e64a472cb", i6);
        f4710CX = a3;
        fmVarArr[i6] = a3;
        int i7 = i6 + 1;
        _m_methods = fmVarArr;
        C1634Xv.m11725a((Object[]) aDJ._m_methods, (Object[]) _m_methods);
        ___iScriptClass = aUO.m18566a(LDParameter.class, C2369eY.class, _m_fields, _m_methods);
    }

    /* renamed from: U */
    private void m22998U(String str) {
        bFf().mo5608dq().mo3197f(f4706CR, str);
    }

    /* renamed from: V */
    private void m23000V(String str) {
        bFf().mo5608dq().mo3197f(f4707CS, str);
    }

    /* renamed from: lM */
    private String m23002lM() {
        return (String) bFf().mo5608dq().mo3214p(f4706CR);
    }

    /* renamed from: lN */
    private String m23003lN() {
        return (String) bFf().mo5608dq().mo3214p(f4707CS);
    }

    /* renamed from: S */
    public void mo10S() {
        super.mo10S();
    }

    /* renamed from: T */
    public C6494any mo11T() {
        return ___iScriptClass;
    }

    /* renamed from: U */
    public C2491fm[] mo12U() {
        return _m_methods;
    }

    /* renamed from: W */
    public Object mo13W() {
        return new C2369eY(this);
    }

    /* renamed from: a */
    public Object mo14a(C0495Gr gr) {
        Object[] args = gr.getArgs();
        switch (gr.mo2417hq() - aDJ._m_methodCount) {
            case 0:
                return m23004lO();
            case 1:
                return m23005lP();
            case 2:
                m23001X((String) args[0]);
                return null;
            default:
                return super.mo14a(gr);
        }
    }

    /* renamed from: a */
    public void mo15a(Collection collection, C0495Gr gr) {
        super.mo15a(collection, gr);
    }

    /* renamed from: c */
    public C5663aRz[] mo25c() {
        return _m_fields;
    }

    public String getKey() {
        switch (bFf().mo6893i(f4708CU)) {
            case 0:
                return null;
            case 2:
                return (String) bFf().mo5606d(new aCE(this, f4708CU, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f4708CU, new Object[0]));
                break;
        }
        return m23004lO();
    }

    public String getValue() {
        switch (bFf().mo6893i(f4709CW)) {
            case 0:
                return null;
            case 2:
                return (String) bFf().mo5606d(new aCE(this, f4709CW, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f4709CW, new Object[0]));
                break;
        }
        return m23005lP();
    }

    public void setValue(String str) {
        switch (bFf().mo6893i(f4710CX)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f4710CX, new Object[]{str}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f4710CX, new Object[]{str}));
                break;
        }
        m23001X(str);
    }

    /* renamed from: p */
    public void mo14157p(String str, String str2) {
        super.mo10S();
        m22998U(str);
        m23000V(str2);
    }

    @C0064Am(aul = "5593c1e8854aaae9b48e6f73c19f6c36", aum = 0)
    /* renamed from: lO */
    private String m23004lO() {
        return m23002lM();
    }

    @C0064Am(aul = "6e279d3350968ac41d6cf0ecd6d74bad", aum = 0)
    /* renamed from: lP */
    private String m23005lP() {
        return m23003lN();
    }

    @C0064Am(aul = "a507e945a8cfe014d960589e64a472cb", aum = 0)
    /* renamed from: X */
    private void m23001X(String str) {
        m23000V(str);
    }
}
