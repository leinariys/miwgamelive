package game.script.associates;

import game.network.message.externalizable.aCE;
import game.script.player.Player;
import logic.baa.*;
import logic.data.mbean.C6919awH;
import logic.res.code.C5640aRc;
import logic.res.code.C5663aRz;
import logic.res.code.C6494any;
import logic.res.html.C2491fm;
import p001a.*;

import java.util.Collection;

@C5511aMd
@C6485anp
/* renamed from: a.FF */
/* compiled from: a */
public class Associate extends aDJ implements C1616Xf {
    public static final C2491fm _f_dispose_0020_0028_0029V = null;
    public static final C2491fm _f_toString_0020_0028_0029Ljava_002flang_002fString_003b = null;
    public static final int _m_fieldCount = 0;
    public static final C5663aRz[] _m_fields = null;
    public static final int _m_methodCount = 0;
    public static final C2491fm[] _m_methods = null;
    public static final C5663aRz cVX = null;
    public static final C2491fm cVY = null;
    public static final C2491fm cVZ = null;
    /* renamed from: hF */
    public static final C2491fm f537hF = null;
    /* renamed from: hz */
    public static final C5663aRz f538hz = null;
    public static final long serialVersionUID = 0;
    public static C6494any ___iScriptClass;
    @C0064Am(aul = "5c598b651bbc49e06534dfca80559e2c", aum = 0)

    /* renamed from: P */
    private static Player f536P;
    @C0064Am(aul = "f6960bc365489e1f15642c580dc51135", aum = 1)
    @C1020Ot(drz = C1020Ot.C1021a.NOT_PERSISTED)
    private static boolean cVW;

    static {
        m3142V();
    }

    public Associate() {
        super((C5540aNg) null);
        super.mo10S();
    }

    public Associate(C5540aNg ang) {
        super(ang);
    }

    public Associate(Player aku) {
        super((C5540aNg) null);
        super._m_script_init(aku);
    }

    /* renamed from: V */
    static void m3142V() {
        serialVersionUID = (long) 1;
        _m_fieldCount = aDJ._m_fieldCount + 2;
        _m_methodCount = aDJ._m_methodCount + 5;
        int i = aDJ._m_fieldCount;
        C5663aRz[] arzArr = new C5663aRz[(i + 2)];
        C5663aRz b = C5640aRc.m17844b(Associate.class, "5c598b651bbc49e06534dfca80559e2c", i);
        f538hz = b;
        arzArr[i] = b;
        int i2 = i + 1;
        C5663aRz b2 = C5640aRc.m17844b(Associate.class, "f6960bc365489e1f15642c580dc51135", i2);
        cVX = b2;
        arzArr[i2] = b2;
        int i3 = i2 + 1;
        _m_fields = arzArr;
        C1634Xv.m11725a((Object[]) aDJ._m_fields, (Object[]) _m_fields);
        int i4 = aDJ._m_methodCount;
        C2491fm[] fmVarArr = new C2491fm[(i4 + 5)];
        C2491fm a = C4105zY.m41624a(Associate.class, "c5b5060548deddf7835eb3078ae1f6dc", i4);
        _f_toString_0020_0028_0029Ljava_002flang_002fString_003b = a;
        fmVarArr[i4] = a;
        int i5 = i4 + 1;
        C2491fm a2 = C4105zY.m41624a(Associate.class, "8d1ffdf52c4541a599484f20100ec92f", i5);
        cVY = a2;
        fmVarArr[i5] = a2;
        int i6 = i5 + 1;
        C2491fm a3 = C4105zY.m41624a(Associate.class, "34df47228c0218223adb0e74837bb1d6", i6);
        f537hF = a3;
        fmVarArr[i6] = a3;
        int i7 = i6 + 1;
        C2491fm a4 = C4105zY.m41624a(Associate.class, "17ea06e09b71d25e15b38dbf9a7532df", i7);
        cVZ = a4;
        fmVarArr[i7] = a4;
        int i8 = i7 + 1;
        C2491fm a5 = C4105zY.m41624a(Associate.class, "08f1f8a71b3e8edbb284027679331d7f", i8);
        _f_dispose_0020_0028_0029V = a5;
        fmVarArr[i8] = a5;
        int i9 = i8 + 1;
        _m_methods = fmVarArr;
        C1634Xv.m11725a((Object[]) aDJ._m_methods, (Object[]) _m_methods);
        ___iScriptClass = aUO.m18566a(Associate.class, C6919awH.class, _m_fields, _m_methods);
    }

    /* renamed from: a */
    private void m3143a(Player aku) {
        bFf().mo5608dq().mo3197f(f538hz, aku);
    }

    private boolean aPG() {
        return bFf().mo5608dq().mo3201h(cVX);
    }

    /* renamed from: cH */
    private void m3145cH(boolean z) {
        bFf().mo5608dq().mo3153a(cVX, z);
    }

    @C0064Am(aul = "8d1ffdf52c4541a599484f20100ec92f", aum = 0)
    @C5566aOg
    /* renamed from: cI */
    private void m3146cI(boolean z) {
        throw new aWi(new aCE(this, cVY, new Object[]{new Boolean(z)}));
    }

    /* renamed from: dG */
    private Player m3147dG() {
        return (Player) bFf().mo5608dq().mo3214p(f538hz);
    }

    /* renamed from: S */
    public void mo10S() {
        super.mo10S();
    }

    /* renamed from: T */
    public C6494any mo11T() {
        return ___iScriptClass;
    }

    /* renamed from: U */
    public C2491fm[] mo12U() {
        return _m_methods;
    }

    /* renamed from: W */
    public Object mo13W() {
        return new C6919awH(this);
    }

    /* renamed from: a */
    public Object mo14a(C0495Gr gr) {
        Object[] args = gr.getArgs();
        switch (gr.mo2417hq() - aDJ._m_methodCount) {
            case 0:
                return m3144au();
            case 1:
                m3146cI(((Boolean) args[0]).booleanValue());
                return null;
            case 2:
                return m3148dK();
            case 3:
                return new Boolean(aPH());
            case 4:
                m3149fg();
                return null;
            default:
                return super.mo14a(gr);
        }
    }

    /* renamed from: a */
    public void mo15a(Collection collection, C0495Gr gr) {
        super.mo15a(collection, gr);
    }

    public boolean aPI() {
        switch (bFf().mo6893i(cVZ)) {
            case 0:
                return false;
            case 2:
                return ((Boolean) bFf().mo5606d(new aCE(this, cVZ, new Object[0]))).booleanValue();
            case 3:
                bFf().mo5606d(new aCE(this, cVZ, new Object[0]));
                break;
        }
        return aPH();
    }

    /* renamed from: c */
    public C5663aRz[] mo25c() {
        return _m_fields;
    }

    /* access modifiers changed from: package-private */
    @C5566aOg
    /* renamed from: cJ */
    public void mo2076cJ(boolean z) {
        switch (bFf().mo6893i(cVY)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, cVY, new Object[]{new Boolean(z)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, cVY, new Object[]{new Boolean(z)}));
                break;
        }
        m3146cI(z);
    }

    /* renamed from: dL */
    public Player mo2077dL() {
        switch (bFf().mo6893i(f537hF)) {
            case 0:
                return null;
            case 2:
                return (Player) bFf().mo5606d(new aCE(this, f537hF, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f537hF, new Object[0]));
                break;
        }
        return m3148dK();
    }

    public void dispose() {
        switch (bFf().mo6893i(_f_dispose_0020_0028_0029V)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, _f_dispose_0020_0028_0029V, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, _f_dispose_0020_0028_0029V, new Object[0]));
                break;
        }
        m3149fg();
    }

    public String toString() {
        switch (bFf().mo6893i(_f_toString_0020_0028_0029Ljava_002flang_002fString_003b)) {
            case 0:
                return null;
            case 2:
                return (String) bFf().mo5606d(new aCE(this, _f_toString_0020_0028_0029Ljava_002flang_002fString_003b, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, _f_toString_0020_0028_0029Ljava_002flang_002fString_003b, new Object[0]));
                break;
        }
        return m3144au();
    }

    @C0064Am(aul = "c5b5060548deddf7835eb3078ae1f6dc", aum = 0)
    /* renamed from: au */
    private String m3144au() {
        return "[" + m3147dG() + "]: " + (aPG() ? "online" : "offline");
    }

    /* renamed from: b */
    public void mo2075b(Player aku) {
        super.mo10S();
        m3143a(aku);
        m3145cH(true);
    }

    @C0064Am(aul = "34df47228c0218223adb0e74837bb1d6", aum = 0)
    /* renamed from: dK */
    private Player m3148dK() {
        return m3147dG();
    }

    @C0064Am(aul = "17ea06e09b71d25e15b38dbf9a7532df", aum = 0)
    private boolean aPH() {
        return aPG();
    }

    @C0064Am(aul = "08f1f8a71b3e8edbb284027679331d7f", aum = 0)
    /* renamed from: fg */
    private void m3149fg() {
        m3143a((Player) null);
        super.dispose();
    }
}
