package game.script.associates;

import game.network.message.externalizable.aCE;
import game.network.message.serializable.C2686iZ;
import game.script.player.Player;
import logic.baa.*;
import logic.data.link.C3161oY;
import logic.data.mbean.C1217Rx;
import logic.res.code.C5640aRc;
import logic.res.code.C5663aRz;
import logic.res.code.C6494any;
import logic.res.html.C2491fm;
import p001a.*;

import java.util.Collection;
import java.util.Set;

@C5511aMd
@C6485anp
/* renamed from: a.aDq  reason: case insensitive filesystem */
/* compiled from: a */
public class Associates extends aDJ implements C1616Xf, C3161oY.C3162a {
    public static final C2491fm _f_dispose_0020_0028_0029V = null;
    /* renamed from: _f_onObjectDisposed_0020_0028Ltaikodom_002finfra_002fscript_002fScriptObject_003b_0029V */
    public static final C2491fm f2592x13860637 = null;
    public static final int _m_fieldCount = 0;
    public static final C5663aRz[] _m_fields = null;
    public static final int _m_methodCount = 0;
    public static final C2491fm[] _m_methods = null;
    /* renamed from: gQ */
    public static final C2491fm f2593gQ = null;
    public static final C5663aRz hBg = null;
    public static final C5663aRz hBh = null;
    public static final C2491fm hBi = null;
    public static final C2491fm hBj = null;
    public static final C2491fm hBk = null;
    public static final C2491fm hBl = null;
    public static final C2491fm hBm = null;
    public static final C2491fm hBn = null;
    public static final C2491fm hBo = null;
    public static final C2491fm hBp = null;
    public static final C2491fm hBq = null;
    public static final C2491fm hBr = null;
    public static final C2491fm hBs = null;
    public static final C2491fm hBt = null;
    /* renamed from: hz */
    public static final C5663aRz f2594hz = null;
    public static final long serialVersionUID = 0;
    public static C6494any ___iScriptClass;
    @C0064Am(aul = "8f10827e9c11e4919c4335efb0a90e93", aum = 0)

    /* renamed from: P */
    private static Player f2591P;
    @C0064Am(aul = "ea9ad70c7777c88ffdc403c593cfbf69", aum = 1)
    private static C2686iZ<Associate> eaq;
    @C0064Am(aul = "86602a25798d75959cbd224d397e3cf4", aum = 2)
    private static C2686iZ<Player> ear;

    static {
        m13847V();
    }

    public Associates() {
        super((C5540aNg) null);
        super.mo10S();
    }

    public Associates(C5540aNg ang) {
        super(ang);
    }

    public Associates(Player aku) {
        super((C5540aNg) null);
        super._m_script_init(aku);
    }

    /* renamed from: V */
    static void m13847V() {
        serialVersionUID = (long) 1;
        _m_fieldCount = aDJ._m_fieldCount + 3;
        _m_methodCount = aDJ._m_methodCount + 15;
        int i = aDJ._m_fieldCount;
        C5663aRz[] arzArr = new C5663aRz[(i + 3)];
        C5663aRz b = C5640aRc.m17844b(Associates.class, "8f10827e9c11e4919c4335efb0a90e93", i);
        f2594hz = b;
        arzArr[i] = b;
        int i2 = i + 1;
        C5663aRz b2 = C5640aRc.m17844b(Associates.class, "ea9ad70c7777c88ffdc403c593cfbf69", i2);
        hBg = b2;
        arzArr[i2] = b2;
        int i3 = i2 + 1;
        C5663aRz b3 = C5640aRc.m17844b(Associates.class, "86602a25798d75959cbd224d397e3cf4", i3);
        hBh = b3;
        arzArr[i3] = b3;
        int i4 = i3 + 1;
        _m_fields = arzArr;
        C1634Xv.m11725a((Object[]) aDJ._m_fields, (Object[]) _m_fields);
        int i5 = aDJ._m_methodCount;
        C2491fm[] fmVarArr = new C2491fm[(i5 + 15)];
        C2491fm a = C4105zY.m41624a(Associates.class, "17a293fa1467c95b78996f949efe4127", i5);
        hBi = a;
        fmVarArr[i5] = a;
        int i6 = i5 + 1;
        C2491fm a2 = C4105zY.m41624a(Associates.class, "573cdff28abacadd2e3353b330f9f467", i6);
        hBj = a2;
        fmVarArr[i6] = a2;
        int i7 = i6 + 1;
        C2491fm a3 = C4105zY.m41624a(Associates.class, "8e950890651af802c07653ed40d03b16", i7);
        hBk = a3;
        fmVarArr[i7] = a3;
        int i8 = i7 + 1;
        C2491fm a4 = C4105zY.m41624a(Associates.class, "55ed2d74fd6d854aac16d74b55bbfe0f", i8);
        hBl = a4;
        fmVarArr[i8] = a4;
        int i9 = i8 + 1;
        C2491fm a5 = C4105zY.m41624a(Associates.class, "8070db4875bc8704b407851c400f2f25", i9);
        hBm = a5;
        fmVarArr[i9] = a5;
        int i10 = i9 + 1;
        C2491fm a6 = C4105zY.m41624a(Associates.class, "9480c9c3aa7a8f277df5cca0759e9a6c", i10);
        hBn = a6;
        fmVarArr[i10] = a6;
        int i11 = i10 + 1;
        C2491fm a7 = C4105zY.m41624a(Associates.class, "03d35726627722971ab7a1729780ceff", i11);
        hBo = a7;
        fmVarArr[i11] = a7;
        int i12 = i11 + 1;
        C2491fm a8 = C4105zY.m41624a(Associates.class, "eb75590a610860bff819ce01f48c9f61", i12);
        hBp = a8;
        fmVarArr[i12] = a8;
        int i13 = i12 + 1;
        C2491fm a9 = C4105zY.m41624a(Associates.class, "46cfe27ba34b71e379bb2ad1e16149b5", i13);
        hBq = a9;
        fmVarArr[i13] = a9;
        int i14 = i13 + 1;
        C2491fm a10 = C4105zY.m41624a(Associates.class, "650548e5a31d9f9de52c0a031e44efea", i14);
        hBr = a10;
        fmVarArr[i14] = a10;
        int i15 = i14 + 1;
        C2491fm a11 = C4105zY.m41624a(Associates.class, "35524fe6482a2127854a5bb11e231576", i15);
        hBs = a11;
        fmVarArr[i15] = a11;
        int i16 = i15 + 1;
        C2491fm a12 = C4105zY.m41624a(Associates.class, "06c22265b47d15370e9f53b8ac45dc3b", i16);
        _f_dispose_0020_0028_0029V = a12;
        fmVarArr[i16] = a12;
        int i17 = i16 + 1;
        C2491fm a13 = C4105zY.m41624a(Associates.class, "a3578a81b4fbd37eb9a1a588de62b114", i17);
        f2592x13860637 = a13;
        fmVarArr[i17] = a13;
        int i18 = i17 + 1;
        C2491fm a14 = C4105zY.m41624a(Associates.class, "bd00dc623f452df56b2ffc2c1c66ddc3", i18);
        f2593gQ = a14;
        fmVarArr[i18] = a14;
        int i19 = i18 + 1;
        C2491fm a15 = C4105zY.m41624a(Associates.class, "012b9ab5f37900b48d0d6a8cc81b8ece", i19);
        hBt = a15;
        fmVarArr[i19] = a15;
        int i20 = i19 + 1;
        _m_methods = fmVarArr;
        C1634Xv.m11725a((Object[]) aDJ._m_methods, (Object[]) _m_methods);
        ___iScriptClass = aUO.m18566a(Associates.class, C1217Rx.class, _m_fields, _m_methods);
    }

    @C0064Am(aul = "012b9ab5f37900b48d0d6a8cc81b8ece", aum = 0)
    @C5566aOg
    /* renamed from: a */
    private void m13848a(Associate ff) {
        throw new aWi(new aCE(this, hBt, new Object[]{ff}));
    }

    /* renamed from: a */
    private void m13849a(Player aku) {
        bFf().mo5608dq().mo3197f(f2594hz, aku);
    }

    /* renamed from: ah */
    private void m13850ah(C2686iZ iZVar) {
        bFf().mo5608dq().mo3197f(hBg, iZVar);
    }

    /* renamed from: ai */
    private void m13851ai(C2686iZ iZVar) {
        bFf().mo5608dq().mo3197f(hBh, iZVar);
    }

    @C5566aOg
    /* renamed from: b */
    private void m13852b(Associate ff) {
        switch (bFf().mo6893i(hBt)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, hBt, new Object[]{ff}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, hBt, new Object[]{ff}));
                break;
        }
        m13848a(ff);
    }

    private C2686iZ cUM() {
        return (C2686iZ) bFf().mo5608dq().mo3214p(hBg);
    }

    private C2686iZ cUN() {
        return (C2686iZ) bFf().mo5608dq().mo3214p(hBh);
    }

    @C0064Am(aul = "8070db4875bc8704b407851c400f2f25", aum = 0)
    @C5566aOg
    /* renamed from: ci */
    private boolean m13854ci(Player aku) {
        throw new aWi(new aCE(this, hBm, new Object[]{aku}));
    }

    @C0064Am(aul = "9480c9c3aa7a8f277df5cca0759e9a6c", aum = 0)
    @C5566aOg
    /* renamed from: ck */
    private boolean m13855ck(Player aku) {
        throw new aWi(new aCE(this, hBn, new Object[]{aku}));
    }

    @C0064Am(aul = "eb75590a610860bff819ce01f48c9f61", aum = 0)
    @C5566aOg
    /* renamed from: co */
    private boolean m13857co(Player aku) {
        throw new aWi(new aCE(this, hBp, new Object[]{aku}));
    }

    @C0064Am(aul = "46cfe27ba34b71e379bb2ad1e16149b5", aum = 0)
    @C5566aOg
    /* renamed from: cq */
    private boolean m13858cq(Player aku) {
        throw new aWi(new aCE(this, hBq, new Object[]{aku}));
    }

    /* renamed from: dG */
    private Player m13860dG() {
        return (Player) bFf().mo5608dq().mo3214p(f2594hz);
    }

    @C0064Am(aul = "bd00dc623f452df56b2ffc2c1c66ddc3", aum = 0)
    @C5566aOg
    /* renamed from: dd */
    private void m13861dd() {
        throw new aWi(new aCE(this, f2593gQ, new Object[0]));
    }

    @C0064Am(aul = "55ed2d74fd6d854aac16d74b55bbfe0f", aum = 0)
    @C5566aOg
    /* renamed from: e */
    private void m13862e(Player aku, boolean z) {
        throw new aWi(new aCE(this, hBl, new Object[]{aku, new Boolean(z)}));
    }

    @C5566aOg
    /* renamed from: f */
    private void m13863f(Player aku, boolean z) {
        switch (bFf().mo6893i(hBl)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, hBl, new Object[]{aku, new Boolean(z)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, hBl, new Object[]{aku, new Boolean(z)}));
                break;
        }
        m13862e(aku, z);
    }

    @C0064Am(aul = "8e950890651af802c07653ed40d03b16", aum = 0)
    @C5566aOg
    /* renamed from: iW */
    private void m13865iW(boolean z) {
        throw new aWi(new aCE(this, hBk, new Object[]{new Boolean(z)}));
    }

    /* renamed from: S */
    public void mo10S() {
        super.mo10S();
    }

    /* renamed from: T */
    public C6494any mo11T() {
        return ___iScriptClass;
    }

    /* renamed from: U */
    public C2491fm[] mo12U() {
        return _m_methods;
    }

    /* renamed from: W */
    public Object mo13W() {
        return new C1217Rx(this);
    }

    /* renamed from: a */
    public Object mo14a(C0495Gr gr) {
        Object[] args = gr.getArgs();
        switch (gr.mo2417hq() - aDJ._m_methodCount) {
            case 0:
                return cUO();
            case 1:
                return cUQ();
            case 2:
                m13865iW(((Boolean) args[0]).booleanValue());
                return null;
            case 3:
                m13862e((Player) args[0], ((Boolean) args[1]).booleanValue());
                return null;
            case 4:
                return new Boolean(m13854ci((Player) args[0]));
            case 5:
                return new Boolean(m13855ck((Player) args[0]));
            case 6:
                return new Boolean(m13856cm((Player) args[0]));
            case 7:
                return new Boolean(m13857co((Player) args[0]));
            case 8:
                return new Boolean(m13858cq((Player) args[0]));
            case 9:
                return new Boolean(m13859cs((Player) args[0]));
            case 10:
                return new Boolean(m13866lV((String) args[0]));
            case 11:
                m13864fg();
                return null;
            case 12:
                m13853b((aDJ) args[0]);
                return null;
            case 13:
                m13861dd();
                return null;
            case 14:
                m13848a((Associate) args[0]);
                return null;
            default:
                return super.mo14a(gr);
        }
    }

    /* renamed from: a */
    public void mo15a(Collection collection, C0495Gr gr) {
        super.mo15a(collection, gr);
    }

    /* renamed from: c */
    public void mo98c(aDJ adj) {
        switch (bFf().mo6893i(f2592x13860637)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f2592x13860637, new Object[]{adj}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f2592x13860637, new Object[]{adj}));
                break;
        }
        m13853b(adj);
    }

    /* renamed from: c */
    public C5663aRz[] mo25c() {
        return _m_fields;
    }

    public Set<Associate> cUP() {
        switch (bFf().mo6893i(hBi)) {
            case 0:
                return null;
            case 2:
                return (Set) bFf().mo5606d(new aCE(this, hBi, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, hBi, new Object[0]));
                break;
        }
        return cUO();
    }

    public Set<Player> cUR() {
        switch (bFf().mo6893i(hBj)) {
            case 0:
                return null;
            case 2:
                return (Set) bFf().mo5606d(new aCE(this, hBj, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, hBj, new Object[0]));
                break;
        }
        return cUQ();
    }

    @C5566aOg
    /* renamed from: cj */
    public boolean mo8509cj(Player aku) {
        switch (bFf().mo6893i(hBm)) {
            case 0:
                return false;
            case 2:
                return ((Boolean) bFf().mo5606d(new aCE(this, hBm, new Object[]{aku}))).booleanValue();
            case 3:
                bFf().mo5606d(new aCE(this, hBm, new Object[]{aku}));
                break;
        }
        return m13854ci(aku);
    }

    @C5566aOg
    /* renamed from: cl */
    public boolean mo8510cl(Player aku) {
        switch (bFf().mo6893i(hBn)) {
            case 0:
                return false;
            case 2:
                return ((Boolean) bFf().mo5606d(new aCE(this, hBn, new Object[]{aku}))).booleanValue();
            case 3:
                bFf().mo5606d(new aCE(this, hBn, new Object[]{aku}));
                break;
        }
        return m13855ck(aku);
    }

    /* renamed from: cn */
    public boolean mo8511cn(Player aku) {
        switch (bFf().mo6893i(hBo)) {
            case 0:
                return false;
            case 2:
                return ((Boolean) bFf().mo5606d(new aCE(this, hBo, new Object[]{aku}))).booleanValue();
            case 3:
                bFf().mo5606d(new aCE(this, hBo, new Object[]{aku}));
                break;
        }
        return m13856cm(aku);
    }

    @C5566aOg
    /* renamed from: cp */
    public boolean mo8512cp(Player aku) {
        switch (bFf().mo6893i(hBp)) {
            case 0:
                return false;
            case 2:
                return ((Boolean) bFf().mo5606d(new aCE(this, hBp, new Object[]{aku}))).booleanValue();
            case 3:
                bFf().mo5606d(new aCE(this, hBp, new Object[]{aku}));
                break;
        }
        return m13857co(aku);
    }

    @C5566aOg
    /* renamed from: cr */
    public boolean mo8513cr(Player aku) {
        switch (bFf().mo6893i(hBq)) {
            case 0:
                return false;
            case 2:
                return ((Boolean) bFf().mo5606d(new aCE(this, hBq, new Object[]{aku}))).booleanValue();
            case 3:
                bFf().mo5606d(new aCE(this, hBq, new Object[]{aku}));
                break;
        }
        return m13858cq(aku);
    }

    /* renamed from: ct */
    public boolean mo8514ct(Player aku) {
        switch (bFf().mo6893i(hBr)) {
            case 0:
                return false;
            case 2:
                return ((Boolean) bFf().mo5606d(new aCE(this, hBr, new Object[]{aku}))).booleanValue();
            case 3:
                bFf().mo5606d(new aCE(this, hBr, new Object[]{aku}));
                break;
        }
        return m13859cs(aku);
    }

    @C5566aOg
    /* renamed from: de */
    public void mo8515de() {
        switch (bFf().mo6893i(f2593gQ)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f2593gQ, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f2593gQ, new Object[0]));
                break;
        }
        m13861dd();
    }

    public void dispose() {
        switch (bFf().mo6893i(_f_dispose_0020_0028_0029V)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, _f_dispose_0020_0028_0029V, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, _f_dispose_0020_0028_0029V, new Object[0]));
                break;
        }
        m13864fg();
    }

    @C5566aOg
    /* renamed from: iX */
    public void mo8516iX(boolean z) {
        switch (bFf().mo6893i(hBk)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, hBk, new Object[]{new Boolean(z)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, hBk, new Object[]{new Boolean(z)}));
                break;
        }
        m13865iW(z);
    }

    /* renamed from: lW */
    public boolean mo8517lW(String str) {
        switch (bFf().mo6893i(hBs)) {
            case 0:
                return false;
            case 2:
                return ((Boolean) bFf().mo5606d(new aCE(this, hBs, new Object[]{str}))).booleanValue();
            case 3:
                bFf().mo5606d(new aCE(this, hBs, new Object[]{str}));
                break;
        }
        return m13866lV(str);
    }

    /* renamed from: b */
    public void mo8506b(Player aku) {
        super.mo10S();
        m13849a(aku);
    }

    @C0064Am(aul = "17a293fa1467c95b78996f949efe4127", aum = 0)
    private Set<Associate> cUO() {
        return cUM();
    }

    @C0064Am(aul = "573cdff28abacadd2e3353b330f9f467", aum = 0)
    private Set<Player> cUQ() {
        return cUN();
    }

    @C0064Am(aul = "03d35726627722971ab7a1729780ceff", aum = 0)
    /* renamed from: cm */
    private boolean m13856cm(Player aku) {
        for (Associate dL : cUM()) {
            if (dL.mo2077dL() == aku) {
                return true;
            }
        }
        return false;
    }

    @C0064Am(aul = "650548e5a31d9f9de52c0a031e44efea", aum = 0)
    /* renamed from: cs */
    private boolean m13859cs(Player aku) {
        return cUN().contains(aku);
    }

    @C0064Am(aul = "35524fe6482a2127854a5bb11e231576", aum = 0)
    /* renamed from: lV */
    private boolean m13866lV(String str) {
        for (Player name : cUN()) {
            if (name.getName().equals(str)) {
                return true;
            }
        }
        return false;
    }

    @C0064Am(aul = "06c22265b47d15370e9f53b8ac45dc3b", aum = 0)
    /* renamed from: fg */
    private void m13864fg() {
        for (Associate dispose : cUM()) {
            dispose.dispose();
        }
        cUM().clear();
        cUN().clear();
        m13849a((Player) null);
    }

    @C0064Am(aul = "a3578a81b4fbd37eb9a1a588de62b114", aum = 0)
    /* renamed from: b */
    private void m13853b(aDJ adj) {
        mo8510cl((Player) adj);
    }
}
