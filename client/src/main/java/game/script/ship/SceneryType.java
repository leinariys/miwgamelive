package game.script.ship;

import game.network.message.externalizable.aCE;
import game.script.resource.Asset;
import game.script.template.BaseTaikodomContent;
import logic.baa.*;
import logic.data.mbean.C6090agK;
import logic.res.code.C5640aRc;
import logic.res.code.C5663aRz;
import logic.res.code.C6494any;
import logic.res.html.C2491fm;
import p001a.*;

import java.util.Collection;

@C0566Hp
@C5511aMd
@C6485anp
/* renamed from: a.atc  reason: case insensitive filesystem */
/* compiled from: a */
public class SceneryType extends BaseTaikodomContent implements C1616Xf, C6251ajP {
    /* renamed from: LR */
    public static final C5663aRz f5322LR = null;
    public static final int _m_fieldCount = 0;
    public static final C5663aRz[] _m_fields = null;
    public static final int _m_methodCount = 0;
    public static final C2491fm[] _m_methods = null;
    public static final C2491fm aDk = null;
    public static final C2491fm aDl = null;
    /* renamed from: dN */
    public static final C2491fm f5323dN = null;
    public static final C5663aRz enM = null;
    public static final C2491fm gxQ = null;
    public static final C2491fm gxR = null;
    public static final C2491fm gxS = null;
    public static final long serialVersionUID = 0;
    /* renamed from: uU */
    public static final C5663aRz f5325uU = null;
    /* renamed from: vi */
    public static final C2491fm f5326vi = null;
    /* renamed from: vj */
    public static final C2491fm f5327vj = null;
    public static C6494any ___iScriptClass;
    @C0064Am(aul = "92b2d994518d21e78d6f74a5c31668ee", aum = 0)

    /* renamed from: LQ */
    private static Asset f5321LQ;
    @C0064Am(aul = "dff5886b4db1f90e951e73ff3cc31213", aum = 2)
    private static boolean enL;
    @C0064Am(aul = "db6a3e4e34adedadf6cbd7841919199e", aum = 1)

    /* renamed from: uT */
    private static String f5324uT;

    static {
        m25915V();
    }

    public SceneryType() {
        super((C5540aNg) null);
        super.mo10S();
    }

    public SceneryType(C5540aNg ang) {
        super(ang);
    }

    /* renamed from: V */
    static void m25915V() {
        serialVersionUID = (long) 1;
        _m_fieldCount = BaseTaikodomContent._m_fieldCount + 3;
        _m_methodCount = BaseTaikodomContent._m_methodCount + 8;
        int i = BaseTaikodomContent._m_fieldCount;
        C5663aRz[] arzArr = new C5663aRz[(i + 3)];
        C5663aRz b = C5640aRc.m17844b(SceneryType.class, "92b2d994518d21e78d6f74a5c31668ee", i);
        f5322LR = b;
        arzArr[i] = b;
        int i2 = i + 1;
        C5663aRz b2 = C5640aRc.m17844b(SceneryType.class, "db6a3e4e34adedadf6cbd7841919199e", i2);
        f5325uU = b2;
        arzArr[i2] = b2;
        int i3 = i2 + 1;
        C5663aRz b3 = C5640aRc.m17844b(SceneryType.class, "dff5886b4db1f90e951e73ff3cc31213", i3);
        enM = b3;
        arzArr[i3] = b3;
        int i4 = i3 + 1;
        _m_fields = arzArr;
        C1634Xv.m11725a((Object[]) BaseTaikodomContent._m_fields, (Object[]) _m_fields);
        int i5 = BaseTaikodomContent._m_methodCount;
        C2491fm[] fmVarArr = new C2491fm[(i5 + 8)];
        C2491fm a = C4105zY.m41624a(SceneryType.class, "7d3ffc17c550a58d84e80f7469d08691", i5);
        aDk = a;
        fmVarArr[i5] = a;
        int i6 = i5 + 1;
        C2491fm a2 = C4105zY.m41624a(SceneryType.class, "63ee92eae8bec9682bd07f2781f953f0", i6);
        aDl = a2;
        fmVarArr[i6] = a2;
        int i7 = i6 + 1;
        C2491fm a3 = C4105zY.m41624a(SceneryType.class, "1723151ce2355c499ac293dded5f2496", i7);
        f5326vi = a3;
        fmVarArr[i7] = a3;
        int i8 = i7 + 1;
        C2491fm a4 = C4105zY.m41624a(SceneryType.class, "42da12d978704145115dc90d10bd48c6", i8);
        f5327vj = a4;
        fmVarArr[i8] = a4;
        int i9 = i8 + 1;
        C2491fm a5 = C4105zY.m41624a(SceneryType.class, "02dd96e9d60d8a4a3b40821192566617", i9);
        gxQ = a5;
        fmVarArr[i9] = a5;
        int i10 = i9 + 1;
        C2491fm a6 = C4105zY.m41624a(SceneryType.class, "085d40756b9cc84b46d1c4f2170376e5", i10);
        gxR = a6;
        fmVarArr[i10] = a6;
        int i11 = i10 + 1;
        C2491fm a7 = C4105zY.m41624a(SceneryType.class, "820adbc751969cc34b7fa5ff1d05a912", i11);
        gxS = a7;
        fmVarArr[i11] = a7;
        int i12 = i11 + 1;
        C2491fm a8 = C4105zY.m41624a(SceneryType.class, "38937bb9367bb2960f262b48cf74f86a", i12);
        f5323dN = a8;
        fmVarArr[i12] = a8;
        int i13 = i12 + 1;
        _m_methods = fmVarArr;
        C1634Xv.m11725a((Object[]) BaseTaikodomContent._m_methods, (Object[]) _m_methods);
        ___iScriptClass = aUO.m18566a(SceneryType.class, C6090agK.class, _m_fields, _m_methods);
    }

    /* renamed from: H */
    private void m25912H(String str) {
        bFf().mo5608dq().mo3197f(f5325uU, str);
    }

    private boolean bxE() {
        return bFf().mo5608dq().mo3201h(enM);
    }

    /* renamed from: dw */
    private void m25917dw(boolean z) {
        bFf().mo5608dq().mo3153a(enM, z);
    }

    /* renamed from: ij */
    private String m25919ij() {
        return (String) bFf().mo5608dq().mo3214p(f5325uU);
    }

    /* renamed from: r */
    private void m25921r(Asset tCVar) {
        bFf().mo5608dq().mo3197f(f5322LR, tCVar);
    }

    /* renamed from: rh */
    private Asset m25922rh() {
        return (Asset) bFf().mo5608dq().mo3214p(f5322LR);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Render Asset")
    /* renamed from: I */
    public void mo16098I(Asset tCVar) {
        switch (bFf().mo6893i(aDl)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, aDl, new Object[]{tCVar}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, aDl, new Object[]{tCVar}));
                break;
        }
        m25911H(tCVar);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Surface Material")
    /* renamed from: N */
    public void mo16099N(String str) {
        switch (bFf().mo6893i(f5327vj)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f5327vj, new Object[]{str}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f5327vj, new Object[]{str}));
                break;
        }
        m25913M(str);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Render Asset")
    /* renamed from: Nu */
    public Asset mo3521Nu() {
        switch (bFf().mo6893i(aDk)) {
            case 0:
                return null;
            case 2:
                return (Asset) bFf().mo5606d(new aCE(this, aDk, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, aDk, new Object[0]));
                break;
        }
        return m25914Nt();
    }

    /* renamed from: T */
    public C6494any mo11T() {
        return ___iScriptClass;
    }

    /* renamed from: U */
    public C2491fm[] mo12U() {
        return _m_methods;
    }

    /* renamed from: W */
    public Object mo13W() {
        return new C6090agK(this);
    }

    /* renamed from: a */
    public Object mo14a(C0495Gr gr) {
        Object[] args = gr.getArgs();
        switch (gr.mo2417hq() - BaseTaikodomContent._m_methodCount) {
            case 0:
                return m25914Nt();
            case 1:
                m25911H((Asset) args[0]);
                return null;
            case 2:
                return m25920it();
            case 3:
                m25913M((String) args[0]);
                return null;
            case 4:
                return new Boolean(cuV());
            case 5:
                m25918gw(((Boolean) args[0]).booleanValue());
                return null;
            case 6:
                return cuX();
            case 7:
                return m25916aT();
            default:
                return super.mo14a(gr);
        }
    }

    /* renamed from: a */
    public void mo15a(Collection collection, C0495Gr gr) {
        super.mo15a(collection, gr);
    }

    /* access modifiers changed from: protected */
    /* renamed from: aU */
    public <T extends aDJ> T mo745aU() {
        switch (bFf().mo6893i(f5323dN)) {
            case 0:
                return null;
            case 2:
                return (aDJ) bFf().mo5606d(new aCE(this, f5323dN, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f5323dN, new Object[0]));
                break;
        }
        return m25916aT();
    }

    /* renamed from: c */
    public C5663aRz[] mo25c() {
        return _m_fields;
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Use Concave")
    public boolean cuW() {
        switch (bFf().mo6893i(gxQ)) {
            case 0:
                return false;
            case 2:
                return ((Boolean) bFf().mo5606d(new aCE(this, gxQ, new Object[0]))).booleanValue();
            case 3:
                bFf().mo5606d(new aCE(this, gxQ, new Object[0]));
                break;
        }
        return cuV();
    }

    public Scenery cuY() {
        switch (bFf().mo6893i(gxS)) {
            case 0:
                return null;
            case 2:
                return (Scenery) bFf().mo5606d(new aCE(this, gxS, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, gxS, new Object[0]));
                break;
        }
        return cuX();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Use Concave")
    /* renamed from: gx */
    public void mo16102gx(boolean z) {
        switch (bFf().mo6893i(gxR)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, gxR, new Object[]{new Boolean(z)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, gxR, new Object[]{new Boolean(z)}));
                break;
        }
        m25918gw(z);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Surface Material")
    /* renamed from: iu */
    public String mo16103iu() {
        switch (bFf().mo6893i(f5326vi)) {
            case 0:
                return null;
            case 2:
                return (String) bFf().mo5606d(new aCE(this, f5326vi, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f5326vi, new Object[0]));
                break;
        }
        return m25920it();
    }

    /* renamed from: S */
    public void mo10S() {
        super.mo10S();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Render Asset")
    @C0064Am(aul = "7d3ffc17c550a58d84e80f7469d08691", aum = 0)
    /* renamed from: Nt */
    private Asset m25914Nt() {
        return m25922rh();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Render Asset")
    @C0064Am(aul = "63ee92eae8bec9682bd07f2781f953f0", aum = 0)
    /* renamed from: H */
    private void m25911H(Asset tCVar) {
        m25921r(tCVar);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Surface Material")
    @C0064Am(aul = "1723151ce2355c499ac293dded5f2496", aum = 0)
    /* renamed from: it */
    private String m25920it() {
        return m25919ij();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Surface Material")
    @C0064Am(aul = "42da12d978704145115dc90d10bd48c6", aum = 0)
    /* renamed from: M */
    private void m25913M(String str) {
        m25912H(str);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Use Concave")
    @C0064Am(aul = "02dd96e9d60d8a4a3b40821192566617", aum = 0)
    private boolean cuV() {
        return bxE();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Use Concave")
    @C0064Am(aul = "085d40756b9cc84b46d1c4f2170376e5", aum = 0)
    /* renamed from: gw */
    private void m25918gw(boolean z) {
        m25917dw(z);
    }

    @C0064Am(aul = "820adbc751969cc34b7fa5ff1d05a912", aum = 0)
    private Scenery cuX() {
        return (Scenery) mo745aU();
    }

    @C0064Am(aul = "38937bb9367bb2960f262b48cf74f86a", aum = 0)
    /* renamed from: aT */
    private <T extends aDJ> T m25916aT() {
        T t = (Scenery) bFf().mo6865M(Scenery.class);
        t.mo5912i(this);
        return t;
    }
}
