package game.script.ship;

import game.network.message.externalizable.aCE;
import game.network.message.serializable.C1556Wo;
import logic.baa.*;
import logic.data.mbean.C1703ZD;
import logic.res.code.C5640aRc;
import logic.res.code.C5663aRz;
import logic.res.code.C6494any;
import logic.res.html.C2491fm;
import p001a.*;

import java.util.*;

@C5511aMd
@C6485anp
/* renamed from: a.axR */
/* compiled from: a */
public class OutpostUpgrade extends aDJ implements C0468GU, C1616Xf {
    public static final C2491fm _f_onResurrect_0020_0028_0029V = null;
    public static final C2491fm _f_toString_0020_0028_0029Ljava_002flang_002fString_003b = null;
    public static final int _m_fieldCount = 0;
    public static final C5663aRz[] _m_fields = null;
    public static final int _m_methodCount = 0;
    public static final C2491fm[] _m_methods = null;
    public static final C5663aRz ayJ = null;
    public static final C2491fm ayM = null;
    /* renamed from: bL */
    public static final C5663aRz f5561bL = null;
    /* renamed from: bM */
    public static final C5663aRz f5562bM = null;
    /* renamed from: bN */
    public static final C2491fm f5563bN = null;
    /* renamed from: bO */
    public static final C2491fm f5564bO = null;
    /* renamed from: bP */
    public static final C2491fm f5565bP = null;
    /* renamed from: bQ */
    public static final C2491fm f5566bQ = null;
    public static final C5663aRz gQK = null;
    public static final C5663aRz gQL = null;
    public static final C5663aRz gQM = null;
    public static final C5663aRz gQN = null;
    public static final C2491fm gQO = null;
    public static final C2491fm gQP = null;
    public static final C2491fm gQQ = null;
    public static final C2491fm gQR = null;
    public static final C2491fm gQS = null;
    public static final C2491fm gQT = null;
    public static final C2491fm gQU = null;
    public static final C2491fm gQV = null;
    public static final C2491fm gQW = null;
    public static final long serialVersionUID = 0;
    public static C6494any ___iScriptClass;
    @C0064Am(aul = "cd12be44a0655c4a1f94cbc097075e5b", aum = 0)
    private static C2611hZ ayI;
    @C0064Am(aul = "7f329854002a45f7b9ff7ba5a5ee9997", aum = 4)

    /* renamed from: bK */
    private static UUID f5560bK;
    @C0064Am(aul = "60b679d9dd57a10ea51d4c1bdfa0f780", aum = 1)
    private static long ePJ;
    @C0064Am(aul = "35da54b98d9e4c8aa2527acb2acc3de7", aum = 2)
    private static float ePK;
    @C0064Am(aul = "a3aae3a8929388506c034ebbc6aaf45b", aum = 3)
    private static int ePL;
    @C0064Am(aul = "4b51721b2c3934bf2f8cc345047fe820", aum = 6)
    private static C1556Wo<C2348eJ, Integer> ePM;
    @C0064Am(aul = "1ca5015d50838fc3f535f8a7d484ab19", aum = 5)
    private static String handle;

    static {
        m27047V();
    }

    public OutpostUpgrade() {
        super((C5540aNg) null);
        super.mo10S();
    }

    public OutpostUpgrade(C5540aNg ang) {
        super(ang);
    }

    public OutpostUpgrade(C2611hZ hZVar) {
        super((C5540aNg) null);
        super._m_script_init(hZVar);
    }

    /* renamed from: V */
    static void m27047V() {
        serialVersionUID = (long) 1;
        _m_fieldCount = aDJ._m_fieldCount + 7;
        _m_methodCount = aDJ._m_methodCount + 16;
        int i = aDJ._m_fieldCount;
        C5663aRz[] arzArr = new C5663aRz[(i + 7)];
        C5663aRz b = C5640aRc.m17844b(OutpostUpgrade.class, "cd12be44a0655c4a1f94cbc097075e5b", i);
        ayJ = b;
        arzArr[i] = b;
        int i2 = i + 1;
        C5663aRz b2 = C5640aRc.m17844b(OutpostUpgrade.class, "60b679d9dd57a10ea51d4c1bdfa0f780", i2);
        gQK = b2;
        arzArr[i2] = b2;
        int i3 = i2 + 1;
        C5663aRz b3 = C5640aRc.m17844b(OutpostUpgrade.class, "35da54b98d9e4c8aa2527acb2acc3de7", i3);
        gQL = b3;
        arzArr[i3] = b3;
        int i4 = i3 + 1;
        C5663aRz b4 = C5640aRc.m17844b(OutpostUpgrade.class, "a3aae3a8929388506c034ebbc6aaf45b", i4);
        gQM = b4;
        arzArr[i4] = b4;
        int i5 = i4 + 1;
        C5663aRz b5 = C5640aRc.m17844b(OutpostUpgrade.class, "7f329854002a45f7b9ff7ba5a5ee9997", i5);
        f5561bL = b5;
        arzArr[i5] = b5;
        int i6 = i5 + 1;
        C5663aRz b6 = C5640aRc.m17844b(OutpostUpgrade.class, "1ca5015d50838fc3f535f8a7d484ab19", i6);
        f5562bM = b6;
        arzArr[i6] = b6;
        int i7 = i6 + 1;
        C5663aRz b7 = C5640aRc.m17844b(OutpostUpgrade.class, "4b51721b2c3934bf2f8cc345047fe820", i7);
        gQN = b7;
        arzArr[i7] = b7;
        int i8 = i7 + 1;
        _m_fields = arzArr;
        C1634Xv.m11725a((Object[]) aDJ._m_fields, (Object[]) _m_fields);
        int i9 = aDJ._m_methodCount;
        C2491fm[] fmVarArr = new C2491fm[(i9 + 16)];
        C2491fm a = C4105zY.m41624a(OutpostUpgrade.class, "fe0626b3df588147ada55578587e6022", i9);
        _f_toString_0020_0028_0029Ljava_002flang_002fString_003b = a;
        fmVarArr[i9] = a;
        int i10 = i9 + 1;
        C2491fm a2 = C4105zY.m41624a(OutpostUpgrade.class, "73b2cec08bb5f3084ae74d89ae3e80b7", i10);
        f5563bN = a2;
        fmVarArr[i10] = a2;
        int i11 = i10 + 1;
        C2491fm a3 = C4105zY.m41624a(OutpostUpgrade.class, "477100fce20c3e7f17f0331b9a2358e1", i11);
        f5564bO = a3;
        fmVarArr[i11] = a3;
        int i12 = i11 + 1;
        C2491fm a4 = C4105zY.m41624a(OutpostUpgrade.class, "9f8aa0fa7cb5fc189e799a089e82587a", i12);
        f5565bP = a4;
        fmVarArr[i12] = a4;
        int i13 = i12 + 1;
        C2491fm a5 = C4105zY.m41624a(OutpostUpgrade.class, "ab2f19043ef1395d218a2f3b6ad74158", i13);
        f5566bQ = a5;
        fmVarArr[i13] = a5;
        int i14 = i13 + 1;
        C2491fm a6 = C4105zY.m41624a(OutpostUpgrade.class, "5c0bcfd5485dfb0691f5c16b7c6ad8d1", i14);
        ayM = a6;
        fmVarArr[i14] = a6;
        int i15 = i14 + 1;
        C2491fm a7 = C4105zY.m41624a(OutpostUpgrade.class, "5a38f2e73b8c7c6baf77fd81f5efa347", i15);
        _f_onResurrect_0020_0028_0029V = a7;
        fmVarArr[i15] = a7;
        int i16 = i15 + 1;
        C2491fm a8 = C4105zY.m41624a(OutpostUpgrade.class, "2b5e65fa26f9f956ef0e9f26e12f0c0c", i16);
        gQO = a8;
        fmVarArr[i16] = a8;
        int i17 = i16 + 1;
        C2491fm a9 = C4105zY.m41624a(OutpostUpgrade.class, "9dc571728750328371d912b57b8751c8", i17);
        gQP = a9;
        fmVarArr[i17] = a9;
        int i18 = i17 + 1;
        C2491fm a10 = C4105zY.m41624a(OutpostUpgrade.class, "03428dda94afff722356925ff03c58a1", i18);
        gQQ = a10;
        fmVarArr[i18] = a10;
        int i19 = i18 + 1;
        C2491fm a11 = C4105zY.m41624a(OutpostUpgrade.class, "93b2b0ae8a60641ca732ceaa597d2f2e", i19);
        gQR = a11;
        fmVarArr[i19] = a11;
        int i20 = i19 + 1;
        C2491fm a12 = C4105zY.m41624a(OutpostUpgrade.class, "03972f06b32ac47569acc246f6f2b0d5", i20);
        gQS = a12;
        fmVarArr[i20] = a12;
        int i21 = i20 + 1;
        C2491fm a13 = C4105zY.m41624a(OutpostUpgrade.class, "e21abca1cf3ab91d8c5dbbbb766b34b7", i21);
        gQT = a13;
        fmVarArr[i21] = a13;
        int i22 = i21 + 1;
        C2491fm a14 = C4105zY.m41624a(OutpostUpgrade.class, "7abc8092ea5c6ea0087459f97b378e87", i22);
        gQU = a14;
        fmVarArr[i22] = a14;
        int i23 = i22 + 1;
        C2491fm a15 = C4105zY.m41624a(OutpostUpgrade.class, "a2790e23296bdb8ed2fb82d8fac064cc", i23);
        gQV = a15;
        fmVarArr[i23] = a15;
        int i24 = i23 + 1;
        C2491fm a16 = C4105zY.m41624a(OutpostUpgrade.class, "eb7fdc420690897983320d36261d4e8f", i24);
        gQW = a16;
        fmVarArr[i24] = a16;
        int i25 = i24 + 1;
        _m_methods = fmVarArr;
        C1634Xv.m11725a((Object[]) aDJ._m_methods, (Object[]) _m_methods);
        ___iScriptClass = aUO.m18566a(OutpostUpgrade.class, C1703ZD.class, _m_fields, _m_methods);
    }

    @C5566aOg
    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Slot numbers (absolute for the level) (NOT the diff from the previous level)")
    @C0064Am(aul = "03972f06b32ac47569acc246f6f2b0d5", aum = 0)
    /* renamed from: F */
    private void m27044F(Map<C2348eJ, Integer> map) {
        throw new aWi(new aCE(this, gQS, new Object[]{map}));
    }

    /* renamed from: LY */
    private C2611hZ m27045LY() {
        return (C2611hZ) bFf().mo5608dq().mo3214p(ayJ);
    }

    /* renamed from: a */
    private void m27048a(C2611hZ hZVar) {
        bFf().mo5608dq().mo3197f(ayJ, hZVar);
    }

    /* renamed from: a */
    private void m27049a(String str) {
        bFf().mo5608dq().mo3197f(f5562bM, str);
    }

    /* renamed from: a */
    private void m27050a(UUID uuid) {
        bFf().mo5608dq().mo3197f(f5561bL, uuid);
    }

    /* renamed from: ac */
    private void m27052ac(C1556Wo wo) {
        bFf().mo5608dq().mo3197f(gQN, wo);
    }

    /* renamed from: an */
    private UUID m27053an() {
        return (UUID) bFf().mo5608dq().mo3214p(f5561bL);
    }

    /* renamed from: ao */
    private String m27054ao() {
        return (String) bFf().mo5608dq().mo3214p(f5562bM);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Handle")
    @C0064Am(aul = "ab2f19043ef1395d218a2f3b6ad74158", aum = 0)
    @C5566aOg
    /* renamed from: b */
    private void m27058b(String str) {
        throw new aWi(new aCE(this, f5566bQ, new Object[]{str}));
    }

    /* renamed from: c */
    private void m27060c(UUID uuid) {
        switch (bFf().mo6893i(f5564bO)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f5564bO, new Object[]{uuid}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f5564bO, new Object[]{uuid}));
                break;
        }
        m27059b(uuid);
    }

    private long cCW() {
        return bFf().mo5608dq().mo3213o(gQK);
    }

    private float cCX() {
        return bFf().mo5608dq().mo3211m(gQL);
    }

    private int cCY() {
        return bFf().mo5608dq().mo3212n(gQM);
    }

    private C1556Wo cCZ() {
        return (C1556Wo) bFf().mo5608dq().mo3214p(gQN);
    }

    private void cDb() {
        switch (bFf().mo6893i(gQO)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, gQO, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, gQO, new Object[0]));
                break;
        }
        cDa();
    }

    /* renamed from: je */
    private void m27061je(long j) {
        bFf().mo5608dq().mo3184b(gQK, j);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Upgrade cost")
    @C0064Am(aul = "7abc8092ea5c6ea0087459f97b378e87", aum = 0)
    @C5566aOg
    /* renamed from: jf */
    private void m27062jf(long j) {
        throw new aWi(new aCE(this, gQU, new Object[]{new Long(j)}));
    }

    /* renamed from: kV */
    private void m27063kV(float f) {
        bFf().mo5608dq().mo3150a(gQL, f);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Absolute Max Storage Capacity")
    @C0064Am(aul = "03428dda94afff722356925ff03c58a1", aum = 0)
    @C5566aOg
    /* renamed from: kW */
    private void m27064kW(float f) {
        throw new aWi(new aCE(this, gQQ, new Object[]{new Float(f)}));
    }

    /* renamed from: vV */
    private void m27065vV(int i) {
        bFf().mo5608dq().mo3183b(gQM, i);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Upgrade time to complete (minutes)")
    @C0064Am(aul = "eb7fdc420690897983320d36261d4e8f", aum = 0)
    @C5566aOg
    /* renamed from: vW */
    private void m27066vW(int i) {
        throw new aWi(new aCE(this, gQW, new Object[]{new Integer(i)}));
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Slot numbers (absolute for the level) (NOT the diff from the previous level)")
    @C5566aOg
    /* renamed from: G */
    public void mo16751G(Map<C2348eJ, Integer> map) {
        switch (bFf().mo6893i(gQS)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, gQS, new Object[]{map}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, gQS, new Object[]{map}));
                break;
        }
        m27044F(map);
    }

    /* renamed from: Mb */
    public C2611hZ mo16752Mb() {
        switch (bFf().mo6893i(ayM)) {
            case 0:
                return null;
            case 2:
                return (C2611hZ) bFf().mo5606d(new aCE(this, ayM, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, ayM, new Object[0]));
                break;
        }
        return m27046Ma();
    }

    /* renamed from: S */
    public void mo10S() {
        super.mo10S();
    }

    /* renamed from: T */
    public C6494any mo11T() {
        return ___iScriptClass;
    }

    /* renamed from: U */
    public C2491fm[] mo12U() {
        return _m_methods;
    }

    /* renamed from: W */
    public Object mo13W() {
        return new C1703ZD(this);
    }

    /* renamed from: a */
    public Object mo14a(C0495Gr gr) {
        Object[] args = gr.getArgs();
        switch (gr.mo2417hq() - aDJ._m_methodCount) {
            case 0:
                return m27057au();
            case 1:
                return m27055ap();
            case 2:
                m27059b((UUID) args[0]);
                return null;
            case 3:
                return m27056ar();
            case 4:
                m27058b((String) args[0]);
                return null;
            case 5:
                return m27046Ma();
            case 6:
                m27051aG();
                return null;
            case 7:
                cDa();
                return null;
            case 8:
                return new Float(cDc());
            case 9:
                m27064kW(((Float) args[0]).floatValue());
                return null;
            case 10:
                return cDe();
            case 11:
                m27044F((Map) args[0]);
                return null;
            case 12:
                return new Long(cDg());
            case 13:
                m27062jf(((Long) args[0]).longValue());
                return null;
            case 14:
                return new Integer(cDi());
            case 15:
                m27066vW(((Integer) args[0]).intValue());
                return null;
            default:
                return super.mo14a(gr);
        }
    }

    /* renamed from: a */
    public void mo15a(Collection collection, C0495Gr gr) {
        super.mo15a(collection, gr);
    }

    /* renamed from: aH */
    public void mo70aH() {
        switch (bFf().mo6893i(_f_onResurrect_0020_0028_0029V)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, _f_onResurrect_0020_0028_0029V, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, _f_onResurrect_0020_0028_0029V, new Object[0]));
                break;
        }
        m27051aG();
    }

    /* renamed from: aq */
    public UUID mo18aq() {
        switch (bFf().mo6893i(f5563bN)) {
            case 0:
                return null;
            case 2:
                return (UUID) bFf().mo5606d(new aCE(this, f5563bN, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f5563bN, new Object[0]));
                break;
        }
        return m27055ap();
    }

    /* renamed from: c */
    public C5663aRz[] mo25c() {
        return _m_fields;
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Absolute Max Storage Capacity")
    public float cDd() {
        switch (bFf().mo6893i(gQP)) {
            case 0:
                return 0.0f;
            case 2:
                return ((Float) bFf().mo5606d(new aCE(this, gQP, new Object[0]))).floatValue();
            case 3:
                bFf().mo5606d(new aCE(this, gQP, new Object[0]));
                break;
        }
        return cDc();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Slot numbers (absolute for the level) (NOT the diff from the previous level)")
    public Map<C2348eJ, Integer> cDf() {
        switch (bFf().mo6893i(gQR)) {
            case 0:
                return null;
            case 2:
                return (Map) bFf().mo5606d(new aCE(this, gQR, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, gQR, new Object[0]));
                break;
        }
        return cDe();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Upgrade cost")
    public long cDh() {
        switch (bFf().mo6893i(gQT)) {
            case 0:
                return 0;
            case 2:
                return ((Long) bFf().mo5606d(new aCE(this, gQT, new Object[0]))).longValue();
            case 3:
                bFf().mo5606d(new aCE(this, gQT, new Object[0]));
                break;
        }
        return cDg();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Upgrade time to complete (minutes)")
    public int cDj() {
        switch (bFf().mo6893i(gQV)) {
            case 0:
                return 0;
            case 2:
                return ((Integer) bFf().mo5606d(new aCE(this, gQV, new Object[0]))).intValue();
            case 3:
                bFf().mo5606d(new aCE(this, gQV, new Object[0]));
                break;
        }
        return cDi();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Handle")
    public String getHandle() {
        switch (bFf().mo6893i(f5565bP)) {
            case 0:
                return null;
            case 2:
                return (String) bFf().mo5606d(new aCE(this, f5565bP, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f5565bP, new Object[0]));
                break;
        }
        return m27056ar();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Handle")
    @C5566aOg
    public void setHandle(String str) {
        switch (bFf().mo6893i(f5566bQ)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f5566bQ, new Object[]{str}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f5566bQ, new Object[]{str}));
                break;
        }
        m27058b(str);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Upgrade cost")
    @C5566aOg
    /* renamed from: jg */
    public void mo16757jg(long j) {
        switch (bFf().mo6893i(gQU)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, gQU, new Object[]{new Long(j)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, gQU, new Object[]{new Long(j)}));
                break;
        }
        m27062jf(j);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Absolute Max Storage Capacity")
    @C5566aOg
    /* renamed from: kX */
    public void mo16759kX(float f) {
        switch (bFf().mo6893i(gQQ)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, gQQ, new Object[]{new Float(f)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, gQQ, new Object[]{new Float(f)}));
                break;
        }
        m27064kW(f);
    }

    public String toString() {
        switch (bFf().mo6893i(_f_toString_0020_0028_0029Ljava_002flang_002fString_003b)) {
            case 0:
                return null;
            case 2:
                return (String) bFf().mo5606d(new aCE(this, _f_toString_0020_0028_0029Ljava_002flang_002fString_003b, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, _f_toString_0020_0028_0029Ljava_002flang_002fString_003b, new Object[0]));
                break;
        }
        return m27057au();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Upgrade time to complete (minutes)")
    @C5566aOg
    /* renamed from: vX */
    public void mo16761vX(int i) {
        switch (bFf().mo6893i(gQW)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, gQW, new Object[]{new Integer(i)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, gQW, new Object[]{new Integer(i)}));
                break;
        }
        m27066vW(i);
    }

    @C0064Am(aul = "fe0626b3df588147ada55578587e6022", aum = 0)
    /* renamed from: au */
    private String m27057au() {
        return "Upgrade for " + m27045LY().name();
    }

    @C0064Am(aul = "73b2cec08bb5f3084ae74d89ae3e80b7", aum = 0)
    /* renamed from: ap */
    private UUID m27055ap() {
        return m27053an();
    }

    @C0064Am(aul = "477100fce20c3e7f17f0331b9a2358e1", aum = 0)
    /* renamed from: b */
    private void m27059b(UUID uuid) {
        m27050a(uuid);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Handle")
    @C0064Am(aul = "9f8aa0fa7cb5fc189e799a089e82587a", aum = 0)
    /* renamed from: ar */
    private String m27056ar() {
        return m27054ao();
    }

    /* renamed from: k */
    public void mo16758k(C2611hZ hZVar) {
        super.mo10S();
        m27048a(hZVar);
        cDb();
        m27050a(UUID.randomUUID());
    }

    @C0064Am(aul = "5c0bcfd5485dfb0691f5c16b7c6ad8d1", aum = 0)
    /* renamed from: Ma */
    private C2611hZ m27046Ma() {
        return m27045LY();
    }

    @C0064Am(aul = "5a38f2e73b8c7c6baf77fd81f5efa347", aum = 0)
    /* renamed from: aG */
    private void m27051aG() {
        cDb();
        super.mo70aH();
    }

    @C0064Am(aul = "2b5e65fa26f9f956ef0e9f26e12f0c0c", aum = 0)
    private void cDa() {
        for (C2348eJ eJVar : C2348eJ.values()) {
            if (!cCZ().containsKey(eJVar)) {
                cCZ().put(eJVar, 0);
            }
        }
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Absolute Max Storage Capacity")
    @C0064Am(aul = "9dc571728750328371d912b57b8751c8", aum = 0)
    private float cDc() {
        return cCX();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Slot numbers (absolute for the level) (NOT the diff from the previous level)")
    @C0064Am(aul = "93b2b0ae8a60641ca732ceaa597d2f2e", aum = 0)
    private Map<C2348eJ, Integer> cDe() {
        TreeMap treeMap = new TreeMap();
        treeMap.putAll(cCZ());
        return Collections.unmodifiableMap(treeMap);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Upgrade cost")
    @C0064Am(aul = "e21abca1cf3ab91d8c5dbbbb766b34b7", aum = 0)
    private long cDg() {
        return cCW();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Upgrade time to complete (minutes)")
    @C0064Am(aul = "a2790e23296bdb8ed2fb82d8fac064cc", aum = 0)
    private int cDi() {
        return cCY();
    }
}
