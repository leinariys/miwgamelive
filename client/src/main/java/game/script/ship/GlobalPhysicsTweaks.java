package game.script.ship;

import game.network.message.externalizable.aCE;
import game.script.TaikodomObject;
import logic.baa.*;
import logic.data.mbean.C6231aiv;
import logic.res.code.C5640aRc;
import logic.res.code.C5663aRz;
import logic.res.code.C6494any;
import logic.res.html.C2491fm;
import p001a.*;

import java.util.Collection;
import java.util.UUID;

@C5511aMd
@C6485anp
/* renamed from: a.zd */
/* compiled from: a */
public class GlobalPhysicsTweaks extends TaikodomObject implements C0468GU, C1616Xf {
    public static final int _m_fieldCount = 0;
    public static final C5663aRz[] _m_fields = null;
    public static final int _m_methodCount = 0;
    public static final C2491fm[] _m_methods = null;
    public static final C2491fm aEJ = null;
    /* renamed from: bL */
    public static final C5663aRz f9701bL = null;
    /* renamed from: bN */
    public static final C2491fm f9702bN = null;
    public static final C2491fm bNA = null;
    public static final C2491fm bNB = null;
    public static final C2491fm bNC = null;
    public static final C2491fm bND = null;
    public static final C5663aRz bNl = null;
    public static final C5663aRz bNn = null;
    public static final C5663aRz bNp = null;
    public static final C5663aRz bNr = null;
    public static final C5663aRz bNt = null;
    public static final C2491fm bNu = null;
    public static final C2491fm bNv = null;
    public static final C2491fm bNw = null;
    public static final C2491fm bNx = null;
    public static final C2491fm bNy = null;
    public static final C2491fm bNz = null;
    /* renamed from: bO */
    public static final C2491fm f9703bO = null;
    /* renamed from: bP */
    public static final C2491fm f9704bP = null;
    public static final long serialVersionUID = 0;
    public static C6494any ___iScriptClass;
    @C0064Am(aul = "c33b28cb6d8d939b74b5812fd34cf254", aum = 5)

    /* renamed from: bK */
    private static UUID f9700bK;
    @C0064Am(aul = "f8200fe7fd723fd8880427e457bf1949", aum = 0)
    private static float bNk;
    @C0064Am(aul = "7dc90cf4765c48184f60f6ca921798b5", aum = 1)
    private static float bNm;
    @C0064Am(aul = "86069e5214c4dbe2aa3a0e7939f11ff1", aum = 2)
    private static float bNo;
    @C0064Am(aul = "9cc22c2adda756250b93a3b750b1ef5f", aum = 3)
    private static float bNq;
    @C0064Am(aul = "7a7ea9643cc90afb2ac05cc30db83b3b", aum = 4)
    private static float bNs;

    static {
        m41663V();
    }

    public GlobalPhysicsTweaks() {
        super((C5540aNg) null);
        super.mo10S();
    }

    public GlobalPhysicsTweaks(C5540aNg ang) {
        super(ang);
    }

    /* renamed from: V */
    static void m41663V() {
        serialVersionUID = (long) 1;
        _m_fieldCount = TaikodomObject._m_fieldCount + 6;
        _m_methodCount = TaikodomObject._m_methodCount + 14;
        int i = TaikodomObject._m_fieldCount;
        C5663aRz[] arzArr = new C5663aRz[(i + 6)];
        C5663aRz b = C5640aRc.m17844b(GlobalPhysicsTweaks.class, "f8200fe7fd723fd8880427e457bf1949", i);
        bNl = b;
        arzArr[i] = b;
        int i2 = i + 1;
        C5663aRz b2 = C5640aRc.m17844b(GlobalPhysicsTweaks.class, "7dc90cf4765c48184f60f6ca921798b5", i2);
        bNn = b2;
        arzArr[i2] = b2;
        int i3 = i2 + 1;
        C5663aRz b3 = C5640aRc.m17844b(GlobalPhysicsTweaks.class, "86069e5214c4dbe2aa3a0e7939f11ff1", i3);
        bNp = b3;
        arzArr[i3] = b3;
        int i4 = i3 + 1;
        C5663aRz b4 = C5640aRc.m17844b(GlobalPhysicsTweaks.class, "9cc22c2adda756250b93a3b750b1ef5f", i4);
        bNr = b4;
        arzArr[i4] = b4;
        int i5 = i4 + 1;
        C5663aRz b5 = C5640aRc.m17844b(GlobalPhysicsTweaks.class, "7a7ea9643cc90afb2ac05cc30db83b3b", i5);
        bNt = b5;
        arzArr[i5] = b5;
        int i6 = i5 + 1;
        C5663aRz b6 = C5640aRc.m17844b(GlobalPhysicsTweaks.class, "c33b28cb6d8d939b74b5812fd34cf254", i6);
        f9701bL = b6;
        arzArr[i6] = b6;
        int i7 = i6 + 1;
        _m_fields = arzArr;
        C1634Xv.m11725a((Object[]) TaikodomObject._m_fields, (Object[]) _m_fields);
        int i8 = TaikodomObject._m_methodCount;
        C2491fm[] fmVarArr = new C2491fm[(i8 + 14)];
        C2491fm a = C4105zY.m41624a(GlobalPhysicsTweaks.class, "64b38a5a66a15590df58fa0f81f65b2c", i8);
        f9702bN = a;
        fmVarArr[i8] = a;
        int i9 = i8 + 1;
        C2491fm a2 = C4105zY.m41624a(GlobalPhysicsTweaks.class, "140e2a549feb4d554e5341fc9d6dcb6b", i9);
        f9703bO = a2;
        fmVarArr[i9] = a2;
        int i10 = i9 + 1;
        C2491fm a3 = C4105zY.m41624a(GlobalPhysicsTweaks.class, "dc3449c0b5ee041fddd8a499a5e51663", i10);
        f9704bP = a3;
        fmVarArr[i10] = a3;
        int i11 = i10 + 1;
        C2491fm a4 = C4105zY.m41624a(GlobalPhysicsTweaks.class, "80379b26b7548da564fe4720c7d24c42", i11);
        aEJ = a4;
        fmVarArr[i11] = a4;
        int i12 = i11 + 1;
        C2491fm a5 = C4105zY.m41624a(GlobalPhysicsTweaks.class, "fd9d4ea5a1bc128989c5b2f7324dc988", i12);
        bNu = a5;
        fmVarArr[i12] = a5;
        int i13 = i12 + 1;
        C2491fm a6 = C4105zY.m41624a(GlobalPhysicsTweaks.class, "9d7a5afc18f422e93c9389c4d1fed6ae", i13);
        bNv = a6;
        fmVarArr[i13] = a6;
        int i14 = i13 + 1;
        C2491fm a7 = C4105zY.m41624a(GlobalPhysicsTweaks.class, "5201f0fec65dca68310fcdad6ac567ac", i14);
        bNw = a7;
        fmVarArr[i14] = a7;
        int i15 = i14 + 1;
        C2491fm a8 = C4105zY.m41624a(GlobalPhysicsTweaks.class, "9f3688d699c0a907c74fb3b9d8bd474c", i15);
        bNx = a8;
        fmVarArr[i15] = a8;
        int i16 = i15 + 1;
        C2491fm a9 = C4105zY.m41624a(GlobalPhysicsTweaks.class, "1f5f39e0d1d127b07e3bc15f3b6247dd", i16);
        bNy = a9;
        fmVarArr[i16] = a9;
        int i17 = i16 + 1;
        C2491fm a10 = C4105zY.m41624a(GlobalPhysicsTweaks.class, "6cee0c973d1b7988839f09c324abacef", i17);
        bNz = a10;
        fmVarArr[i17] = a10;
        int i18 = i17 + 1;
        C2491fm a11 = C4105zY.m41624a(GlobalPhysicsTweaks.class, "e3b05f348b29cb34bbb2f1d47c5ab702", i18);
        bNA = a11;
        fmVarArr[i18] = a11;
        int i19 = i18 + 1;
        C2491fm a12 = C4105zY.m41624a(GlobalPhysicsTweaks.class, "c5620388218492421358756f41719c5b", i19);
        bNB = a12;
        fmVarArr[i19] = a12;
        int i20 = i19 + 1;
        C2491fm a13 = C4105zY.m41624a(GlobalPhysicsTweaks.class, "f4b2a546f4a52084b56b457b923999a6", i20);
        bNC = a13;
        fmVarArr[i20] = a13;
        int i21 = i20 + 1;
        C2491fm a14 = C4105zY.m41624a(GlobalPhysicsTweaks.class, "43692f138a6fa3fc6f48606e1f7469db", i21);
        bND = a14;
        fmVarArr[i21] = a14;
        int i22 = i21 + 1;
        _m_methods = fmVarArr;
        C1634Xv.m11725a((Object[]) TaikodomObject._m_methods, (Object[]) _m_methods);
        ___iScriptClass = aUO.m18566a(GlobalPhysicsTweaks.class, C6231aiv.class, _m_fields, _m_methods);
    }

    @C3248pc(aYR = C3248pc.C3250b.ACTION, displayName = "_Load Defaults_")
    @C0064Am(aul = "80379b26b7548da564fe4720c7d24c42", aum = 0)
    @C5566aOg
    /* renamed from: Og */
    private void m41662Og() {
        throw new aWi(new aCE(this, aEJ, new Object[0]));
    }

    /* renamed from: a */
    private void m41664a(UUID uuid) {
        bFf().mo5608dq().mo3197f(f9701bL, uuid);
    }

    /* renamed from: an */
    private UUID m41665an() {
        return (UUID) bFf().mo5608dq().mo3214p(f9701bL);
    }

    private float aqX() {
        return bFf().mo5608dq().mo3211m(bNl);
    }

    private float aqY() {
        return bFf().mo5608dq().mo3211m(bNn);
    }

    private float aqZ() {
        return bFf().mo5608dq().mo3211m(bNp);
    }

    private float ara() {
        return bFf().mo5608dq().mo3211m(bNr);
    }

    private float arb() {
        return bFf().mo5608dq().mo3211m(bNt);
    }

    /* renamed from: c */
    private void m41669c(UUID uuid) {
        switch (bFf().mo6893i(f9703bO)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f9703bO, new Object[]{uuid}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f9703bO, new Object[]{uuid}));
                break;
        }
        m41668b(uuid);
    }

    /* renamed from: ef */
    private void m41670ef(float f) {
        bFf().mo5608dq().mo3150a(bNl, f);
    }

    /* renamed from: eg */
    private void m41671eg(float f) {
        bFf().mo5608dq().mo3150a(bNn, f);
    }

    /* renamed from: eh */
    private void m41672eh(float f) {
        bFf().mo5608dq().mo3150a(bNp, f);
    }

    /* renamed from: ei */
    private void m41673ei(float f) {
        bFf().mo5608dq().mo3150a(bNr, f);
    }

    /* renamed from: ej */
    private void m41674ej(float f) {
        bFf().mo5608dq().mo3150a(bNt, f);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Global Pitch Correction Multiplier")
    @C0064Am(aul = "fd9d4ea5a1bc128989c5b2f7324dc988", aum = 0)
    @C5566aOg
    /* renamed from: ek */
    private void m41675ek(float f) {
        throw new aWi(new aCE(this, bNu, new Object[]{new Float(f)}));
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Global Roll Correction Multiplier")
    @C0064Am(aul = "9d7a5afc18f422e93c9389c4d1fed6ae", aum = 0)
    @C5566aOg
    /* renamed from: em */
    private void m41676em(float f) {
        throw new aWi(new aCE(this, bNv, new Object[]{new Float(f)}));
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Drift Correction, Standard")
    @C0064Am(aul = "5201f0fec65dca68310fcdad6ac567ac", aum = 0)
    @C5566aOg
    /* renamed from: eo */
    private void m41677eo(float f) {
        throw new aWi(new aCE(this, bNw, new Object[]{new Float(f)}));
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Drift Correction, Inertial")
    @C0064Am(aul = "9f3688d699c0a907c74fb3b9d8bd474c", aum = 0)
    @C5566aOg
    /* renamed from: eq */
    private void m41678eq(float f) {
        throw new aWi(new aCE(this, bNx, new Object[]{new Float(f)}));
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Aim Delay Multiplier")
    @C0064Am(aul = "1f5f39e0d1d127b07e3bc15f3b6247dd", aum = 0)
    @C5566aOg
    /* renamed from: es */
    private void m41679es(float f) {
        throw new aWi(new aCE(this, bNy, new Object[]{new Float(f)}));
    }

    @C3248pc(aYR = C3248pc.C3250b.ACTION, displayName = "_Load Defaults_")
    @C5566aOg
    /* renamed from: Oh */
    public void mo23359Oh() {
        switch (bFf().mo6893i(aEJ)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, aEJ, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, aEJ, new Object[0]));
                break;
        }
        m41662Og();
    }

    /* renamed from: T */
    public C6494any mo11T() {
        return ___iScriptClass;
    }

    /* renamed from: U */
    public C2491fm[] mo12U() {
        return _m_methods;
    }

    /* renamed from: W */
    public Object mo13W() {
        return new C6231aiv(this);
    }

    /* renamed from: a */
    public Object mo14a(C0495Gr gr) {
        Object[] args = gr.getArgs();
        switch (gr.mo2417hq() - TaikodomObject._m_methodCount) {
            case 0:
                return m41666ap();
            case 1:
                m41668b((UUID) args[0]);
                return null;
            case 2:
                return m41667ar();
            case 3:
                m41662Og();
                return null;
            case 4:
                m41675ek(((Float) args[0]).floatValue());
                return null;
            case 5:
                m41676em(((Float) args[0]).floatValue());
                return null;
            case 6:
                m41677eo(((Float) args[0]).floatValue());
                return null;
            case 7:
                m41678eq(((Float) args[0]).floatValue());
                return null;
            case 8:
                m41679es(((Float) args[0]).floatValue());
                return null;
            case 9:
                return new Float(arc());
            case 10:
                return new Float(are());
            case 11:
                return new Float(arg());
            case 12:
                return new Float(ari());
            case 13:
                return new Float(ark());
            default:
                return super.mo14a(gr);
        }
    }

    /* renamed from: a */
    public void mo15a(Collection collection, C0495Gr gr) {
        super.mo15a(collection, gr);
    }

    /* renamed from: aq */
    public UUID mo18aq() {
        switch (bFf().mo6893i(f9702bN)) {
            case 0:
                return null;
            case 2:
                return (UUID) bFf().mo5606d(new aCE(this, f9702bN, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f9702bN, new Object[0]));
                break;
        }
        return m41666ap();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Global Pitch Correction Multiplier")
    public float ard() {
        switch (bFf().mo6893i(bNz)) {
            case 0:
                return 0.0f;
            case 2:
                return ((Float) bFf().mo5606d(new aCE(this, bNz, new Object[0]))).floatValue();
            case 3:
                bFf().mo5606d(new aCE(this, bNz, new Object[0]));
                break;
        }
        return arc();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Global Roll Correction Multiplier")
    public float arf() {
        switch (bFf().mo6893i(bNA)) {
            case 0:
                return 0.0f;
            case 2:
                return ((Float) bFf().mo5606d(new aCE(this, bNA, new Object[0]))).floatValue();
            case 3:
                bFf().mo5606d(new aCE(this, bNA, new Object[0]));
                break;
        }
        return are();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Drift Correction, Standard")
    public float arh() {
        switch (bFf().mo6893i(bNB)) {
            case 0:
                return 0.0f;
            case 2:
                return ((Float) bFf().mo5606d(new aCE(this, bNB, new Object[0]))).floatValue();
            case 3:
                bFf().mo5606d(new aCE(this, bNB, new Object[0]));
                break;
        }
        return arg();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Drift Correction, Inertial")
    public float arj() {
        switch (bFf().mo6893i(bNC)) {
            case 0:
                return 0.0f;
            case 2:
                return ((Float) bFf().mo5606d(new aCE(this, bNC, new Object[0]))).floatValue();
            case 3:
                bFf().mo5606d(new aCE(this, bNC, new Object[0]));
                break;
        }
        return ari();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Aim Delay Multiplier")
    public float arl() {
        switch (bFf().mo6893i(bND)) {
            case 0:
                return 0.0f;
            case 2:
                return ((Float) bFf().mo5606d(new aCE(this, bND, new Object[0]))).floatValue();
            case 3:
                bFf().mo5606d(new aCE(this, bND, new Object[0]));
                break;
        }
        return ark();
    }

    /* renamed from: c */
    public C5663aRz[] mo25c() {
        return _m_fields;
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Global Pitch Correction Multiplier")
    @C5566aOg
    /* renamed from: el */
    public void mo23365el(float f) {
        switch (bFf().mo6893i(bNu)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, bNu, new Object[]{new Float(f)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, bNu, new Object[]{new Float(f)}));
                break;
        }
        m41675ek(f);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Global Roll Correction Multiplier")
    @C5566aOg
    /* renamed from: en */
    public void mo23366en(float f) {
        switch (bFf().mo6893i(bNv)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, bNv, new Object[]{new Float(f)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, bNv, new Object[]{new Float(f)}));
                break;
        }
        m41676em(f);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Drift Correction, Standard")
    @C5566aOg
    /* renamed from: ep */
    public void mo23367ep(float f) {
        switch (bFf().mo6893i(bNw)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, bNw, new Object[]{new Float(f)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, bNw, new Object[]{new Float(f)}));
                break;
        }
        m41677eo(f);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Drift Correction, Inertial")
    @C5566aOg
    /* renamed from: er */
    public void mo23368er(float f) {
        switch (bFf().mo6893i(bNx)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, bNx, new Object[]{new Float(f)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, bNx, new Object[]{new Float(f)}));
                break;
        }
        m41678eq(f);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Aim Delay Multiplier")
    @C5566aOg
    /* renamed from: et */
    public void mo23369et(float f) {
        switch (bFf().mo6893i(bNy)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, bNy, new Object[]{new Float(f)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, bNy, new Object[]{new Float(f)}));
                break;
        }
        m41679es(f);
    }

    public String getHandle() {
        switch (bFf().mo6893i(f9704bP)) {
            case 0:
                return null;
            case 2:
                return (String) bFf().mo5606d(new aCE(this, f9704bP, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f9704bP, new Object[0]));
                break;
        }
        return m41667ar();
    }

    @C0064Am(aul = "64b38a5a66a15590df58fa0f81f65b2c", aum = 0)
    /* renamed from: ap */
    private UUID m41666ap() {
        return m41665an();
    }

    @C0064Am(aul = "140e2a549feb4d554e5341fc9d6dcb6b", aum = 0)
    /* renamed from: b */
    private void m41668b(UUID uuid) {
        m41664a(uuid);
    }

    @C0064Am(aul = "dc3449c0b5ee041fddd8a499a5e51663", aum = 0)
    /* renamed from: ar */
    private String m41667ar() {
        return "global_physics_tweaks";
    }

    /* renamed from: S */
    public void mo10S() {
        super.mo10S();
        m41670ef(1.5f);
        m41671eg(0.33f);
        m41672eh(10.0f);
        m41673ei(1.5f);
        m41674ej(0.5f);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Global Pitch Correction Multiplier")
    @C0064Am(aul = "6cee0c973d1b7988839f09c324abacef", aum = 0)
    private float arc() {
        return aqX();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Global Roll Correction Multiplier")
    @C0064Am(aul = "e3b05f348b29cb34bbb2f1d47c5ab702", aum = 0)
    private float are() {
        return aqY();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Drift Correction, Standard")
    @C0064Am(aul = "c5620388218492421358756f41719c5b", aum = 0)
    private float arg() {
        return aqZ();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Drift Correction, Inertial")
    @C0064Am(aul = "f4b2a546f4a52084b56b457b923999a6", aum = 0)
    private float ari() {
        return ara();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Aim Delay Multiplier")
    @C0064Am(aul = "43692f138a6fa3fc6f48606e1f7469db", aum = 0)
    private float ark() {
        return arb();
    }
}
