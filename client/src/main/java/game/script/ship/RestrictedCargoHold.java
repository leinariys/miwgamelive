package game.script.ship;

import game.network.message.externalizable.aCE;
import game.network.message.serializable.C2686iZ;
import game.script.item.Item;
import game.script.item.ItemTypeCategory;
import game.script.nls.NLSItem;
import game.script.nls.NLSManager;
import logic.baa.C1616Xf;
import logic.baa.C1634Xv;
import logic.baa.C5511aMd;
import logic.baa.C6485anp;
import logic.data.mbean.C5831abL;
import logic.res.code.C5640aRc;
import logic.res.code.C5663aRz;
import logic.res.code.C6494any;
import logic.res.html.C2491fm;
import p001a.*;

import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

@C2712iu
@C5511aMd
@C6485anp
/* renamed from: a.aH */
/* compiled from: a */
public class RestrictedCargoHold extends CargoHold implements C1616Xf {
    public static final int _m_fieldCount = 0;
    public static final C5663aRz[] _m_fields = null;
    public static final int _m_methodCount = 0;
    public static final C2491fm[] _m_methods = null;
    /* renamed from: kt */
    public static final C5663aRz f2902kt = null;
    /* renamed from: ku */
    public static final C2491fm f2903ku = null;
    /* renamed from: kv */
    public static final C2491fm f2904kv = null;
    /* renamed from: kw */
    public static final C2491fm f2905kw = null;
    /* renamed from: kx */
    public static final C2491fm f2906kx = null;
    /* renamed from: ky */
    public static final C2491fm f2907ky = null;
    /* renamed from: kz */
    public static final C2491fm f2908kz = null;
    public static final long serialVersionUID = 0;
    public static C6494any ___iScriptClass;
    @C0064Am(aul = "9146c0d1e88f27867eae4a6adc1087b7", aum = 0)

    /* renamed from: ks */
    private static C2686iZ<ItemTypeCategory> f2901ks;

    static {
        m15089V();
    }

    public RestrictedCargoHold() {
        super((C5540aNg) null);
        super.mo10S();
    }

    public RestrictedCargoHold(C5540aNg ang) {
        super(ang);
    }

    public RestrictedCargoHold(CargoHoldType arb) {
        super((C5540aNg) null);
        super.mo9150a(arb);
    }

    /* renamed from: V */
    static void m15089V() {
        serialVersionUID = (long) 1;
        _m_fieldCount = CargoHold._m_fieldCount + 1;
        _m_methodCount = CargoHold._m_methodCount + 6;
        int i = CargoHold._m_fieldCount;
        C5663aRz[] arzArr = new C5663aRz[(i + 1)];
        C5663aRz b = C5640aRc.m17844b(RestrictedCargoHold.class, "9146c0d1e88f27867eae4a6adc1087b7", i);
        f2902kt = b;
        arzArr[i] = b;
        int i2 = i + 1;
        _m_fields = arzArr;
        C1634Xv.m11725a((Object[]) CargoHold._m_fields, (Object[]) _m_fields);
        int i3 = CargoHold._m_methodCount;
        C2491fm[] fmVarArr = new C2491fm[(i3 + 6)];
        C2491fm a = C4105zY.m41624a(RestrictedCargoHold.class, "a088db603f017c3d5ca4a66125389c01", i3);
        f2903ku = a;
        fmVarArr[i3] = a;
        int i4 = i3 + 1;
        C2491fm a2 = C4105zY.m41624a(RestrictedCargoHold.class, "0d042503d0cd8984c7f709011fb7963d", i4);
        f2904kv = a2;
        fmVarArr[i4] = a2;
        int i5 = i4 + 1;
        C2491fm a3 = C4105zY.m41624a(RestrictedCargoHold.class, "722227324e093fdc125d3cc8ca8c5f61", i5);
        f2905kw = a3;
        fmVarArr[i5] = a3;
        int i6 = i5 + 1;
        C2491fm a4 = C4105zY.m41624a(RestrictedCargoHold.class, "50f2d1c60475bfe09bf9ec3bf78eac6e", i6);
        f2906kx = a4;
        fmVarArr[i6] = a4;
        int i7 = i6 + 1;
        C2491fm a5 = C4105zY.m41624a(RestrictedCargoHold.class, "a79dfbacab914248d3aaaef89b17f047", i7);
        f2907ky = a5;
        fmVarArr[i7] = a5;
        int i8 = i7 + 1;
        C2491fm a6 = C4105zY.m41624a(RestrictedCargoHold.class, "7144d2fdb63d922884a73422f75a197a", i8);
        f2908kz = a6;
        fmVarArr[i8] = a6;
        int i9 = i8 + 1;
        _m_methods = fmVarArr;
        C1634Xv.m11725a((Object[]) CargoHold._m_methods, (Object[]) _m_methods);
        ___iScriptClass = aUO.m18566a(RestrictedCargoHold.class, C5831abL.class, _m_fields, _m_methods);
    }

    /* renamed from: c */
    private void m15091c(C2686iZ iZVar) {
        bFf().mo5608dq().mo3197f(f2902kt, iZVar);
    }

    /* renamed from: ev */
    private C2686iZ m15093ev() {
        return (C2686iZ) bFf().mo5608dq().mo3214p(f2902kt);
    }

    /* renamed from: S */
    public void mo10S() {
        super.mo10S();
    }

    /* renamed from: T */
    public C6494any mo11T() {
        return ___iScriptClass;
    }

    /* renamed from: U */
    public C2491fm[] mo12U() {
        return _m_methods;
    }

    /* renamed from: W */
    public Object mo13W() {
        return new C5831abL(this);
    }

    /* renamed from: a */
    public Object mo14a(C0495Gr gr) {
        Object[] args = gr.getArgs();
        switch (gr.mo2417hq() - CargoHold._m_methodCount) {
            case 0:
                m15097g((Item) args[0]);
                return null;
            case 1:
                return new Boolean(m15090a((ItemTypeCategory) args[0]));
            case 2:
                m15092d((C2686iZ) args[0]);
                return null;
            case 3:
                m15096f((C2686iZ) args[0]);
                return null;
            case 4:
                m15094ew();
                return null;
            case 5:
                return m15095ey();
            default:
                return super.mo14a(gr);
        }
    }

    /* renamed from: a */
    public void mo15a(Collection collection, C0495Gr gr) {
        super.mo15a(collection, gr);
    }

    /* renamed from: b */
    public boolean mo9151b(ItemTypeCategory aai) {
        switch (bFf().mo6893i(f2904kv)) {
            case 0:
                return false;
            case 2:
                return ((Boolean) bFf().mo5606d(new aCE(this, f2904kv, new Object[]{aai}))).booleanValue();
            case 3:
                bFf().mo5606d(new aCE(this, f2904kv, new Object[]{aai}));
                break;
        }
        return m15090a(aai);
    }

    /* renamed from: c */
    public C5663aRz[] mo25c() {
        return _m_fields;
    }

    /* renamed from: e */
    public void mo9152e(C2686iZ<ItemTypeCategory> iZVar) {
        switch (bFf().mo6893i(f2905kw)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f2905kw, new Object[]{iZVar}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f2905kw, new Object[]{iZVar}));
                break;
        }
        m15092d(iZVar);
    }

    /* renamed from: ex */
    public void mo9153ex() {
        switch (bFf().mo6893i(f2907ky)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f2907ky, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f2907ky, new Object[0]));
                break;
        }
        m15094ew();
    }

    /* renamed from: ez */
    public Set<Item> mo9154ez() {
        switch (bFf().mo6893i(f2908kz)) {
            case 0:
                return null;
            case 2:
                return (Set) bFf().mo5606d(new aCE(this, f2908kz, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f2908kz, new Object[0]));
                break;
        }
        return m15095ey();
    }

    /* renamed from: g */
    public void mo9155g(C2686iZ<ItemTypeCategory> iZVar) {
        switch (bFf().mo6893i(f2906kx)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f2906kx, new Object[]{iZVar}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f2906kx, new Object[]{iZVar}));
                break;
        }
        m15096f(iZVar);
    }

    /* access modifiers changed from: protected */
    /* renamed from: h */
    public void mo1889h(Item auq) {
        switch (bFf().mo6893i(f2903ku)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f2903ku, new Object[]{auq}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f2903ku, new Object[]{auq}));
                break;
        }
        m15097g(auq);
    }

    /* renamed from: a */
    public void mo9150a(CargoHoldType arb) {
        super.mo9150a(arb);
    }

    @C0064Am(aul = "a088db603f017c3d5ca4a66125389c01", aum = 0)
    /* renamed from: g */
    private void m15097g(Item auq) {
        super.mo1889h(auq);
        if (!mo9151b(auq.bAP().mo19866HC())) {
            throw new C2293dh(((NLSItem) ala().aIY().mo6310c(NLSManager.C1472a.ITEM)).bto());
        }
    }

    @C0064Am(aul = "0d042503d0cd8984c7f709011fb7963d", aum = 0)
    /* renamed from: a */
    private boolean m15090a(ItemTypeCategory aai) {
        if (m15093ev().contains(aai)) {
            return true;
        }
        if (aai.bJS() != null) {
            return mo9151b(aai.bJS());
        }
        return false;
    }

    @C0064Am(aul = "722227324e093fdc125d3cc8ca8c5f61", aum = 0)
    /* renamed from: d */
    private void m15092d(C2686iZ<ItemTypeCategory> iZVar) {
        m15093ev().addAll(iZVar);
    }

    @C0064Am(aul = "50f2d1c60475bfe09bf9ec3bf78eac6e", aum = 0)
    /* renamed from: f */
    private void m15096f(C2686iZ<ItemTypeCategory> iZVar) {
        m15093ev().removeAll(iZVar);
    }

    @C0064Am(aul = "a79dfbacab914248d3aaaef89b17f047", aum = 0)
    /* renamed from: ew */
    private void m15094ew() {
        m15093ev().clear();
    }

    @C0064Am(aul = "7144d2fdb63d922884a73422f75a197a", aum = 0)
    /* renamed from: ey */
    private Set<Item> m15095ey() {
        HashSet hashSet = new HashSet();
        Iterator<Item> iterator = getIterator();
        while (iterator.hasNext()) {
            Item next = iterator.next();
            if (!mo9151b(next.bAP().mo19866HC())) {
                hashSet.add(next);
            }
        }
        return hashSet;
    }
}
