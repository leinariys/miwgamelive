package game.script.ship.strike;

import game.network.message.externalizable.aCE;
import game.network.message.serializable.C1556Wo;
import game.network.message.serializable.C3438ra;
import game.script.damage.DamageType;
import game.script.item.ItemTypeCategory;
import game.script.resource.Asset;
import logic.baa.*;
import logic.data.mbean.aSK;
import logic.res.code.C5640aRc;
import logic.res.code.C5663aRz;
import logic.res.code.C6494any;
import logic.res.html.C2491fm;
import p001a.*;

import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Map;

@C0566Hp
@C5511aMd
@C6485anp
/* renamed from: a.aDD */
/* compiled from: a */
public class StrikeType extends BaseStrikeType implements C1616Xf {
    public static final int _m_fieldCount = 0;
    public static final C5663aRz[] _m_fields = null;
    public static final int _m_methodCount = 0;
    public static final C2491fm[] _m_methods = null;
    public static final C5663aRz aAe = null;
    public static final C5663aRz aAg = null;
    public static final C2491fm aAh = null;
    public static final C2491fm aAi = null;
    public static final C2491fm aAj = null;
    public static final C2491fm aAk = null;
    public static final C2491fm aAl = null;
    public static final C2491fm aQB = null;
    public static final C5663aRz atP = null;
    public static final C5663aRz atQ = null;
    public static final C5663aRz atS = null;
    public static final C5663aRz atT = null;
    public static final C2491fm bbS = null;
    public static final C2491fm bbT = null;
    public static final C2491fm bbU = null;
    public static final C2491fm bbV = null;
    public static final C5663aRz bjK = null;
    /* renamed from: dN */
    public static final C2491fm f2514dN = null;
    public static final C2491fm eUN = null;
    public static final C2491fm hCA = null;
    public static final C2491fm hCw = null;
    public static final C2491fm hCx = null;
    public static final C2491fm hCy = null;
    public static final C2491fm hCz = null;
    public static final long serialVersionUID = 0;
    /* renamed from: yg */
    public static final C2491fm f2515yg = null;
    public static C6494any ___iScriptClass;
    @C0064Am(aul = "f8c4e8e400173930c1f36301b0b4fe9d", aum = 2)
    private static float aAc;
    @C0064Am(aul = "da77ecf6effd7f485efdc9bd9433fc1c", aum = 5)
    private static Asset aAd;
    @C0064Am(aul = "8fb0592a0b14741aadc85066f1f90dc1", aum = 6)
    private static Asset aAf;
    @C0064Am(aul = "1ce980ab167280182de4b846d1769b62", aum = 0)
    private static C1556Wo<DamageType, C3892wO> atO;
    @C0064Am(aul = "3b2dac10830d68f2f148bcc9e3869739", aum = 3)
    private static float bbL;
    @C0064Am(aul = "09df90e679eccf6f82fe8403a4ecc5d6", aum = 1)
    private static float bbM;
    @C0064Am(aul = "5a744d37d9d6a0fadb5091086430c36a", aum = 4)
    private static C3438ra<ItemTypeCategory> bjJ;

    static {
        m13442V();
    }

    public StrikeType() {
        super((C5540aNg) null);
        super.mo10S();
    }

    public StrikeType(C5540aNg ang) {
        super(ang);
    }

    /* renamed from: V */
    static void m13442V() {
        serialVersionUID = (long) 1;
        _m_fieldCount = BaseStrikeType._m_fieldCount + 7;
        _m_methodCount = BaseStrikeType._m_methodCount + 18;
        int i = BaseStrikeType._m_fieldCount;
        C5663aRz[] arzArr = new C5663aRz[(i + 7)];
        C5663aRz b = C5640aRc.m17844b(StrikeType.class, "1ce980ab167280182de4b846d1769b62", i);
        atP = b;
        arzArr[i] = b;
        int i2 = i + 1;
        C5663aRz b2 = C5640aRc.m17844b(StrikeType.class, "09df90e679eccf6f82fe8403a4ecc5d6", i2);
        atQ = b2;
        arzArr[i2] = b2;
        int i3 = i2 + 1;
        C5663aRz b3 = C5640aRc.m17844b(StrikeType.class, "f8c4e8e400173930c1f36301b0b4fe9d", i3);
        atS = b3;
        arzArr[i3] = b3;
        int i4 = i3 + 1;
        C5663aRz b4 = C5640aRc.m17844b(StrikeType.class, "3b2dac10830d68f2f148bcc9e3869739", i4);
        atT = b4;
        arzArr[i4] = b4;
        int i5 = i4 + 1;
        C5663aRz b5 = C5640aRc.m17844b(StrikeType.class, "5a744d37d9d6a0fadb5091086430c36a", i5);
        bjK = b5;
        arzArr[i5] = b5;
        int i6 = i5 + 1;
        C5663aRz b6 = C5640aRc.m17844b(StrikeType.class, "da77ecf6effd7f485efdc9bd9433fc1c", i6);
        aAe = b6;
        arzArr[i6] = b6;
        int i7 = i6 + 1;
        C5663aRz b7 = C5640aRc.m17844b(StrikeType.class, "8fb0592a0b14741aadc85066f1f90dc1", i7);
        aAg = b7;
        arzArr[i7] = b7;
        int i8 = i7 + 1;
        _m_fields = arzArr;
        C1634Xv.m11725a((Object[]) BaseStrikeType._m_fields, (Object[]) _m_fields);
        int i9 = BaseStrikeType._m_methodCount;
        C2491fm[] fmVarArr = new C2491fm[(i9 + 18)];
        C2491fm a = C4105zY.m41624a(StrikeType.class, "2a65f39c663a20d0e254acfce1c9b725", i9);
        aQB = a;
        fmVarArr[i9] = a;
        int i10 = i9 + 1;
        C2491fm a2 = C4105zY.m41624a(StrikeType.class, "ef1d224aacefd74e308f6cc5f2dcf3e2", i10);
        eUN = a2;
        fmVarArr[i10] = a2;
        int i11 = i10 + 1;
        C2491fm a3 = C4105zY.m41624a(StrikeType.class, "9a498aec9029fb5c29afc8d03304269f", i11);
        bbU = a3;
        fmVarArr[i11] = a3;
        int i12 = i11 + 1;
        C2491fm a4 = C4105zY.m41624a(StrikeType.class, "42a97a98572de207c77473ca2ef7595a", i12);
        bbV = a4;
        fmVarArr[i12] = a4;
        int i13 = i12 + 1;
        C2491fm a5 = C4105zY.m41624a(StrikeType.class, "19ab9fad17c47bcfc5ebfe502cf65294", i13);
        f2515yg = a5;
        fmVarArr[i13] = a5;
        int i14 = i13 + 1;
        C2491fm a6 = C4105zY.m41624a(StrikeType.class, "d9cb07c9ab087b571e440331eba8697c", i14);
        aAh = a6;
        fmVarArr[i14] = a6;
        int i15 = i14 + 1;
        C2491fm a7 = C4105zY.m41624a(StrikeType.class, "3a76c29457fc71dd8e580a1a019b59e2", i15);
        bbS = a7;
        fmVarArr[i15] = a7;
        int i16 = i15 + 1;
        C2491fm a8 = C4105zY.m41624a(StrikeType.class, "96b1deb0b0922246f56fcd209373436f", i16);
        bbT = a8;
        fmVarArr[i16] = a8;
        int i17 = i16 + 1;
        C2491fm a9 = C4105zY.m41624a(StrikeType.class, "172fc1ada0b80d203d27d1cccd2e879a", i17);
        hCw = a9;
        fmVarArr[i17] = a9;
        int i18 = i17 + 1;
        C2491fm a10 = C4105zY.m41624a(StrikeType.class, "1d93409f880c3276a0957b4172decd9d", i18);
        hCx = a10;
        fmVarArr[i18] = a10;
        int i19 = i18 + 1;
        C2491fm a11 = C4105zY.m41624a(StrikeType.class, "a748a3133d22f476415c535bd39f9d03", i19);
        hCy = a11;
        fmVarArr[i19] = a11;
        int i20 = i19 + 1;
        C2491fm a12 = C4105zY.m41624a(StrikeType.class, "4e08bfa91ae73055cbce1db49419822c", i20);
        hCz = a12;
        fmVarArr[i20] = a12;
        int i21 = i20 + 1;
        C2491fm a13 = C4105zY.m41624a(StrikeType.class, "ac89f41695733129c519d61b0018b9bc", i21);
        aAi = a13;
        fmVarArr[i21] = a13;
        int i22 = i21 + 1;
        C2491fm a14 = C4105zY.m41624a(StrikeType.class, "861b08e61a5dfb2e2acba003309f5ed1", i22);
        aAj = a14;
        fmVarArr[i22] = a14;
        int i23 = i22 + 1;
        C2491fm a15 = C4105zY.m41624a(StrikeType.class, "b3aee0d05c1c19ba5addbdb8e867193d", i23);
        aAk = a15;
        fmVarArr[i23] = a15;
        int i24 = i23 + 1;
        C2491fm a16 = C4105zY.m41624a(StrikeType.class, "bfa040d2462326b9699d46d2ba0acf94", i24);
        aAl = a16;
        fmVarArr[i24] = a16;
        int i25 = i24 + 1;
        C2491fm a17 = C4105zY.m41624a(StrikeType.class, "c2538d86190fb8bc8307ba6b8e6ae38b", i25);
        hCA = a17;
        fmVarArr[i25] = a17;
        int i26 = i25 + 1;
        C2491fm a18 = C4105zY.m41624a(StrikeType.class, "692b796d18668165a74e63a55839ba6e", i26);
        f2514dN = a18;
        fmVarArr[i26] = a18;
        int i27 = i26 + 1;
        _m_methods = fmVarArr;
        C1634Xv.m11725a((Object[]) BaseStrikeType._m_methods, (Object[]) _m_methods);
        ___iScriptClass = aUO.m18566a(StrikeType.class, aSK.class, _m_fields, _m_methods);
    }

    /* renamed from: B */
    private void m13430B(Asset tCVar) {
        bFf().mo5608dq().mo3197f(aAe, tCVar);
    }

    /* renamed from: C */
    private void m13431C(Asset tCVar) {
        bFf().mo5608dq().mo3197f(aAg, tCVar);
    }

    /* renamed from: J */
    private void m13434J(C3438ra raVar) {
        bFf().mo5608dq().mo3197f(bjK, raVar);
    }

    /* renamed from: Km */
    private C1556Wo m13435Km() {
        return (C1556Wo) bFf().mo5608dq().mo3214p(atP);
    }

    /* renamed from: Mu */
    private float m13436Mu() {
        return bFf().mo5608dq().mo3211m(atS);
    }

    /* renamed from: Mv */
    private Asset m13437Mv() {
        return (Asset) bFf().mo5608dq().mo3214p(aAe);
    }

    /* renamed from: Mw */
    private Asset m13438Mw() {
        return (Asset) bFf().mo5608dq().mo3214p(aAg);
    }

    /* renamed from: Yx */
    private float m13445Yx() {
        return bFf().mo5608dq().mo3211m(atT);
    }

    /* renamed from: Yy */
    private float m13446Yy() {
        return bFf().mo5608dq().mo3211m(atQ);
    }

    private C3438ra acx() {
        return (C3438ra) bFf().mo5608dq().mo3214p(bjK);
    }

    /* renamed from: ba */
    private void m13448ba(float f) {
        bFf().mo5608dq().mo3150a(atS, f);
    }

    /* renamed from: cW */
    private void m13450cW(float f) {
        bFf().mo5608dq().mo3150a(atT, f);
    }

    /* renamed from: cX */
    private void m13451cX(float f) {
        bFf().mo5608dq().mo3150a(atQ, f);
    }

    /* renamed from: i */
    private void m13454i(C1556Wo wo) {
        bFf().mo5608dq().mo3197f(atP, wo);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Sfx Start")
    /* renamed from: E */
    public void mo8295E(Asset tCVar) {
        switch (bFf().mo6893i(aAj)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, aAj, new Object[]{tCVar}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, aAj, new Object[]{tCVar}));
                break;
        }
        m13432D(tCVar);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Sfx Stop")
    /* renamed from: G */
    public void mo8296G(Asset tCVar) {
        switch (bFf().mo6893i(aAl)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, aAl, new Object[]{tCVar}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, aAl, new Object[]{tCVar}));
                break;
        }
        m13433F(tCVar);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Sfx Stop")
    /* renamed from: MA */
    public Asset mo8297MA() {
        switch (bFf().mo6893i(aAk)) {
            case 0:
                return null;
            case 2:
                return (Asset) bFf().mo5606d(new aCE(this, aAk, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, aAk, new Object[0]));
                break;
        }
        return m13440Mz();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Sfx Start")
    /* renamed from: My */
    public Asset mo8298My() {
        switch (bFf().mo6893i(aAi)) {
            case 0:
                return null;
            case 2:
                return (Asset) bFf().mo5606d(new aCE(this, aAi, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, aAi, new Object[0]));
                break;
        }
        return m13439Mx();
    }

    /* renamed from: T */
    public C6494any mo11T() {
        return ___iScriptClass;
    }

    /* renamed from: U */
    public C2491fm[] mo12U() {
        return _m_methods;
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Damages")
    /* renamed from: UD */
    public C1556Wo<DamageType, C3892wO> mo8299UD() {
        switch (bFf().mo6893i(aQB)) {
            case 0:
                return null;
            case 2:
                return (C1556Wo) bFf().mo5606d(new aCE(this, aQB, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, aQB, new Object[0]));
                break;
        }
        return m13441UC();
    }

    /* renamed from: W */
    public Object mo13W() {
        return new aSK(this);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Warmup")
    /* renamed from: YE */
    public float mo8300YE() {
        switch (bFf().mo6893i(bbS)) {
            case 0:
                return 0.0f;
            case 2:
                return ((Float) bFf().mo5606d(new aCE(this, bbS, new Object[0]))).floatValue();
            case 3:
                bFf().mo5606d(new aCE(this, bbS, new Object[0]));
                break;
        }
        return m13443YD();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Cooldown")
    /* renamed from: YG */
    public float mo8301YG() {
        switch (bFf().mo6893i(bbU)) {
            case 0:
                return 0.0f;
            case 2:
                return ((Float) bFf().mo5606d(new aCE(this, bbU, new Object[0]))).floatValue();
            case 3:
                bFf().mo5606d(new aCE(this, bbU, new Object[0]));
                break;
        }
        return m13444YF();
    }

    /* renamed from: a */
    public Object mo14a(C0495Gr gr) {
        Object[] args = gr.getArgs();
        switch (gr.mo2417hq() - BaseStrikeType._m_methodCount) {
            case 0:
                return m13441UC();
            case 1:
                m13458u((Map) args[0]);
                return null;
            case 2:
                return new Float(m13444YF());
            case 3:
                m13453da(((Float) args[0]).floatValue());
                return null;
            case 4:
                return new Float(m13455js());
            case 5:
                m13449bb(((Float) args[0]).floatValue());
                return null;
            case 6:
                return new Float(m13443YD());
            case 7:
                m13452cY(((Float) args[0]).floatValue());
                return null;
            case 8:
                m13456q((ItemTypeCategory) args[0]);
                return null;
            case 9:
                m13457s((ItemTypeCategory) args[0]);
                return null;
            case 10:
                return cVw();
            case 11:
                return cVy();
            case 12:
                return m13439Mx();
            case 13:
                m13432D((Asset) args[0]);
                return null;
            case 14:
                return m13440Mz();
            case 15:
                m13433F((Asset) args[0]);
                return null;
            case 16:
                return cVA();
            case 17:
                return m13447aT();
            default:
                return super.mo14a(gr);
        }
    }

    /* renamed from: a */
    public void mo15a(Collection collection, C0495Gr gr) {
        super.mo15a(collection, gr);
    }

    /* access modifiers changed from: protected */
    /* renamed from: aU */
    public <T extends aDJ> T mo745aU() {
        switch (bFf().mo6893i(f2514dN)) {
            case 0:
                return null;
            case 2:
                return (aDJ) bFf().mo5606d(new aCE(this, f2514dN, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f2514dN, new Object[0]));
                break;
        }
        return m13447aT();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Duration")
    /* renamed from: bc */
    public void mo8302bc(float f) {
        switch (bFf().mo6893i(aAh)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, aAh, new Object[]{new Float(f)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, aAh, new Object[]{new Float(f)}));
                break;
        }
        m13449bb(f);
    }

    /* renamed from: c */
    public C5663aRz[] mo25c() {
        return _m_fields;
    }

    public Strike cVB() {
        switch (bFf().mo6893i(hCA)) {
            case 0:
                return null;
            case 2:
                return (Strike) bFf().mo5606d(new aCE(this, hCA, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, hCA, new Object[0]));
                break;
        }
        return cVA();
    }

    public C3438ra<ItemTypeCategory> cVx() {
        switch (bFf().mo6893i(hCy)) {
            case 0:
                return null;
            case 2:
                return (C3438ra) bFf().mo5606d(new aCE(this, hCy, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, hCy, new Object[0]));
                break;
        }
        return cVw();
    }

    @C3248pc(aYR = C3248pc.C3250b.ITERATOR, displayName = "Types To Buff")
    public List<ItemTypeCategory> cVz() {
        switch (bFf().mo6893i(hCz)) {
            case 0:
                return null;
            case 2:
                return (List) bFf().mo5606d(new aCE(this, hCz, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, hCz, new Object[0]));
                break;
        }
        return cVy();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Warmup")
    /* renamed from: cZ */
    public void mo8306cZ(float f) {
        switch (bFf().mo6893i(bbT)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, bbT, new Object[]{new Float(f)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, bbT, new Object[]{new Float(f)}));
                break;
        }
        m13452cY(f);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Cooldown")
    /* renamed from: db */
    public void mo8307db(float f) {
        switch (bFf().mo6893i(bbV)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, bbV, new Object[]{new Float(f)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, bbV, new Object[]{new Float(f)}));
                break;
        }
        m13453da(f);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Duration")
    /* renamed from: jt */
    public float mo8308jt() {
        switch (bFf().mo6893i(f2515yg)) {
            case 0:
                return 0.0f;
            case 2:
                return ((Float) bFf().mo5606d(new aCE(this, f2515yg, new Object[0]))).floatValue();
            case 3:
                bFf().mo5606d(new aCE(this, f2515yg, new Object[0]));
                break;
        }
        return m13455js();
    }

    @C3248pc(aYR = C3248pc.C3250b.ADDER, displayName = "Types To Buff")
    /* renamed from: r */
    public void mo8309r(ItemTypeCategory aai) {
        switch (bFf().mo6893i(hCw)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, hCw, new Object[]{aai}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, hCw, new Object[]{aai}));
                break;
        }
        m13456q(aai);
    }

    @C3248pc(aYR = C3248pc.C3250b.REMOVER, displayName = "Types To Buff")
    /* renamed from: t */
    public void mo8310t(ItemTypeCategory aai) {
        switch (bFf().mo6893i(hCx)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, hCx, new Object[]{aai}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, hCx, new Object[]{aai}));
                break;
        }
        m13457s(aai);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Damages")
    /* renamed from: v */
    public void mo8311v(Map<DamageType, C3892wO> map) {
        switch (bFf().mo6893i(eUN)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, eUN, new Object[]{map}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, eUN, new Object[]{map}));
                break;
        }
        m13458u(map);
    }

    /* renamed from: S */
    public void mo10S() {
        super.mo10S();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Damages")
    @C0064Am(aul = "2a65f39c663a20d0e254acfce1c9b725", aum = 0)
    /* renamed from: UC */
    private C1556Wo<DamageType, C3892wO> m13441UC() {
        return m13435Km();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Damages")
    @C0064Am(aul = "ef1d224aacefd74e308f6cc5f2dcf3e2", aum = 0)
    /* renamed from: u */
    private void m13458u(Map<DamageType, C3892wO> map) {
        m13435Km().clear();
        m13435Km().putAll(map);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Cooldown")
    @C0064Am(aul = "9a498aec9029fb5c29afc8d03304269f", aum = 0)
    /* renamed from: YF */
    private float m13444YF() {
        return m13446Yy();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Cooldown")
    @C0064Am(aul = "42a97a98572de207c77473ca2ef7595a", aum = 0)
    /* renamed from: da */
    private void m13453da(float f) {
        m13451cX(f);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Duration")
    @C0064Am(aul = "19ab9fad17c47bcfc5ebfe502cf65294", aum = 0)
    /* renamed from: js */
    private float m13455js() {
        return m13436Mu();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Duration")
    @C0064Am(aul = "d9cb07c9ab087b571e440331eba8697c", aum = 0)
    /* renamed from: bb */
    private void m13449bb(float f) {
        m13448ba(f);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Warmup")
    @C0064Am(aul = "3a76c29457fc71dd8e580a1a019b59e2", aum = 0)
    /* renamed from: YD */
    private float m13443YD() {
        return m13445Yx();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Warmup")
    @C0064Am(aul = "96b1deb0b0922246f56fcd209373436f", aum = 0)
    /* renamed from: cY */
    private void m13452cY(float f) {
        m13450cW(f);
    }

    @C3248pc(aYR = C3248pc.C3250b.ADDER, displayName = "Types To Buff")
    @C0064Am(aul = "172fc1ada0b80d203d27d1cccd2e879a", aum = 0)
    /* renamed from: q */
    private void m13456q(ItemTypeCategory aai) {
        acx().add(aai);
    }

    @C3248pc(aYR = C3248pc.C3250b.REMOVER, displayName = "Types To Buff")
    @C0064Am(aul = "1d93409f880c3276a0957b4172decd9d", aum = 0)
    /* renamed from: s */
    private void m13457s(ItemTypeCategory aai) {
        acx().remove(aai);
    }

    @C0064Am(aul = "a748a3133d22f476415c535bd39f9d03", aum = 0)
    private C3438ra<ItemTypeCategory> cVw() {
        return acx();
    }

    @C3248pc(aYR = C3248pc.C3250b.ITERATOR, displayName = "Types To Buff")
    @C0064Am(aul = "4e08bfa91ae73055cbce1db49419822c", aum = 0)
    private List<ItemTypeCategory> cVy() {
        return Collections.unmodifiableList(acx());
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Sfx Start")
    @C0064Am(aul = "ac89f41695733129c519d61b0018b9bc", aum = 0)
    /* renamed from: Mx */
    private Asset m13439Mx() {
        return m13437Mv();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Sfx Start")
    @C0064Am(aul = "861b08e61a5dfb2e2acba003309f5ed1", aum = 0)
    /* renamed from: D */
    private void m13432D(Asset tCVar) {
        m13430B(tCVar);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Sfx Stop")
    @C0064Am(aul = "b3aee0d05c1c19ba5addbdb8e867193d", aum = 0)
    /* renamed from: Mz */
    private Asset m13440Mz() {
        return m13438Mw();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Sfx Stop")
    @C0064Am(aul = "bfa040d2462326b9699d46d2ba0acf94", aum = 0)
    /* renamed from: F */
    private void m13433F(Asset tCVar) {
        m13431C(tCVar);
    }

    @C0064Am(aul = "c2538d86190fb8bc8307ba6b8e6ae38b", aum = 0)
    private Strike cVA() {
        return (Strike) mo745aU();
    }

    @C0064Am(aul = "692b796d18668165a74e63a55839ba6e", aum = 0)
    /* renamed from: aT */
    private <T extends aDJ> T m13447aT() {
        T t = (Strike) bFf().mo6865M(Strike.class);
        t.mo17797d(this);
        return t;
    }
}
