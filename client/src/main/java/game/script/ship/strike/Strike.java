package game.script.ship.strike;

import game.geometry.Vec3d;
import game.network.message.externalizable.aCE;
import game.network.message.serializable.C1556Wo;
import game.network.message.serializable.C3438ra;
import game.script.TaikodomObject;
import game.script.damage.DamageType;
import game.script.item.ItemTypeCategory;
import game.script.item.ProjectileWeapon;
import game.script.item.Weapon;
import game.script.item.WeaponAdapter;
import game.script.nls.NLSManager;
import game.script.nls.NLSProgression;
import game.script.player.Player;
import game.script.resource.Asset;
import game.script.ship.Ship;
import game.script.util.AdapterLikedList;
import logic.aaa.C0284Df;
import logic.aaa.C1506WA;
import logic.aaa.C3853vo;
import logic.baa.*;
import logic.data.mbean.C3124oC;
import logic.data.mbean.C5304aEe;
import logic.data.mbean.aQM;
import logic.render.IEngineGraphics;
import logic.res.code.C5640aRc;
import logic.res.code.C5663aRz;
import logic.res.code.C6494any;
import logic.res.html.C2491fm;
import org.mozilla1.javascript.ScriptRuntime;
import p001a.*;

import java.util.ArrayList;
import java.util.Collection;

@C6485anp
@C2712iu(mo19786Bt = BaseStrikeType.class)
@C5511aMd
/* renamed from: a.dQ */
/* compiled from: a */
public class Strike extends TaikodomObject implements C6480ank<StrikeAdapter>, C6872avM, C6872avM {

    /* renamed from: Lm */
    public static final C2491fm f6506Lm = null;
    /* renamed from: _f_getShip_0020_0028_0029Ltaikodom_002fgame_002fscript_002fship_002fShip_003b */
    public static final C2491fm f6507x851d656b = null;
    public static final C2491fm _f_tick_0020_0028F_0029V = null;
    public static final int _m_fieldCount = 0;
    public static final C5663aRz[] _m_fields = null;
    public static final int _m_methodCount = 0;
    public static final C2491fm[] _m_methods = null;
    public static final C5663aRz aAe = null;
    public static final C5663aRz aAg = null;
    public static final C5663aRz aOP = null;
    public static final C5663aRz aOx = null;
    public static final C2491fm aPe = null;
    public static final C2491fm aPf = null;
    public static final C2491fm aPi = null;
    public static final C2491fm aPj = null;
    public static final C2491fm aPk = null;
    public static final C2491fm aPl = null;
    public static final C2491fm aPm = null;
    public static final C5663aRz atP = null;
    public static final C5663aRz atQ = null;
    public static final C5663aRz atS = null;
    public static final C5663aRz atT = null;
    public static final C2491fm atY = null;
    public static final C2491fm bbS = null;
    public static final C2491fm bbU = null;
    public static final C5663aRz bid = null;
    public static final C5663aRz bjK = null;
    public static final C5663aRz bjM = null;
    public static final C5663aRz bjO = null;
    public static final C5663aRz bjQ = null;
    public static final C5663aRz bjS = null;
    public static final C5663aRz bjU = null;
    public static final C2491fm bjV = null;
    public static final C2491fm bjW = null;
    public static final C2491fm bjX = null;
    public static final C2491fm bjY = null;
    public static final C2491fm bjZ = null;
    public static final C2491fm bka = null;
    public static final C2491fm bkb = null;
    public static final C2491fm bkc = null;
    public static final C2491fm bkd = null;
    public static final C2491fm bke = null;
    public static final C2491fm bkf = null;
    public static final C2491fm bkg = null;
    public static final C2491fm bkh = null;
    public static final C2491fm bki = null;
    public static final C2491fm bkj = null;
    public static final C2491fm bkk = null;
    public static final C2491fm bkl = null;
    public static final long serialVersionUID = 0;
    /* renamed from: yg */
    public static final C2491fm f6508yg = null;
    public static C6494any ___iScriptClass;
    @C0064Am(aul = "d890572a0d690e09c66440decce19a4c", aum = 2)
    private static float aAc;
    @C0064Am(aul = "a0e118f4e7653af684d062209b02f77e", aum = 5)
    private static Asset aAd;
    @C0064Am(aul = "fd29ace52b0921d5a3ddcc91f6a4544c", aum = 6)
    private static Asset aAf;
    @C0064Am(aul = "1fa35492d6965c0c59c699018ae5e75a", aum = 9)
    private static Ship aOO;
    @C0064Am(aul = "0bc9a7fbf92ecbd4c58b8d2d4e9b7238", aum = 7)
    private static AdapterLikedList<StrikeAdapter> aOw;
    @C0064Am(aul = "66408fd0dd394619a7c0cfd1f0404a0b", aum = 8)
    private static C6809auB.C1996a aZI;
    @C0064Am(aul = "ea073621be37a10db45389941486509d", aum = 0)
    private static C1556Wo<DamageType, C3892wO> atO;
    @C0064Am(aul = "8f414302657beb7392d41f8997c4276f", aum = 3)
    private static float bbL;
    @C0064Am(aul = "920a2662b013ebfaa849780fc31cc68a", aum = 1)
    private static float bbM;
    @C0064Am(aul = "e0852059eac2da97a229fb094f91dba0", aum = 4)
    private static C3438ra<ItemTypeCategory> bjJ;
    @C0064Am(aul = "ff946a5ee065d3cb6991916c9fe9ba5b", aum = 10)
    private static C1556Wo<Weapon, StrikeDamageAdapter> bjL;
    @C0064Am(aul = "c73523937749c48d1545bcbf0340882f", aum = 11)
    private static C1556Wo<DamageType, C3892wO> bjN;
    @C0064Am(aul = "ac0191e23cb237bbfe0b81121c29b2a3", aum = 12)
    private static float bjP;
    @C0064Am(aul = "c338f0526df1b6a9a515b43c9d77a45d", aum = 13)
    private static float bjR;
    @C0064Am(aul = "3ffc7c22d3f5fe159abdb3fcda18390f", aum = 14)
    private static float bjT;

    static {
        m28861V();
    }

    public Strike() {
        super((C5540aNg) null);
        super.mo10S();
    }

    public Strike(StrikeType add) {
        super((C5540aNg) null);
        super._m_script_init(add);
    }

    public Strike(C5540aNg ang) {
        super(ang);
    }

    /* renamed from: V */
    static void m28861V() {
        serialVersionUID = (long) 1;
        _m_fieldCount = TaikodomObject._m_fieldCount + 15;
        _m_methodCount = TaikodomObject._m_methodCount + 31;
        int i = TaikodomObject._m_fieldCount;
        C5663aRz[] arzArr = new C5663aRz[(i + 15)];
        C5663aRz b = C5640aRc.m17844b(Strike.class, "ea073621be37a10db45389941486509d", i);
        atP = b;
        arzArr[i] = b;
        int i2 = i + 1;
        C5663aRz b2 = C5640aRc.m17844b(Strike.class, "920a2662b013ebfaa849780fc31cc68a", i2);
        atQ = b2;
        arzArr[i2] = b2;
        int i3 = i2 + 1;
        C5663aRz b3 = C5640aRc.m17844b(Strike.class, "d890572a0d690e09c66440decce19a4c", i3);
        atS = b3;
        arzArr[i3] = b3;
        int i4 = i3 + 1;
        C5663aRz b4 = C5640aRc.m17844b(Strike.class, "8f414302657beb7392d41f8997c4276f", i4);
        atT = b4;
        arzArr[i4] = b4;
        int i5 = i4 + 1;
        C5663aRz b5 = C5640aRc.m17844b(Strike.class, "e0852059eac2da97a229fb094f91dba0", i5);
        bjK = b5;
        arzArr[i5] = b5;
        int i6 = i5 + 1;
        C5663aRz b6 = C5640aRc.m17844b(Strike.class, "a0e118f4e7653af684d062209b02f77e", i6);
        aAe = b6;
        arzArr[i6] = b6;
        int i7 = i6 + 1;
        C5663aRz b7 = C5640aRc.m17844b(Strike.class, "fd29ace52b0921d5a3ddcc91f6a4544c", i7);
        aAg = b7;
        arzArr[i7] = b7;
        int i8 = i7 + 1;
        C5663aRz b8 = C5640aRc.m17844b(Strike.class, "0bc9a7fbf92ecbd4c58b8d2d4e9b7238", i8);
        aOx = b8;
        arzArr[i8] = b8;
        int i9 = i8 + 1;
        C5663aRz b9 = C5640aRc.m17844b(Strike.class, "66408fd0dd394619a7c0cfd1f0404a0b", i9);
        bid = b9;
        arzArr[i9] = b9;
        int i10 = i9 + 1;
        C5663aRz b10 = C5640aRc.m17844b(Strike.class, "1fa35492d6965c0c59c699018ae5e75a", i10);
        aOP = b10;
        arzArr[i10] = b10;
        int i11 = i10 + 1;
        C5663aRz b11 = C5640aRc.m17844b(Strike.class, "ff946a5ee065d3cb6991916c9fe9ba5b", i11);
        bjM = b11;
        arzArr[i11] = b11;
        int i12 = i11 + 1;
        C5663aRz b12 = C5640aRc.m17844b(Strike.class, "c73523937749c48d1545bcbf0340882f", i12);
        bjO = b12;
        arzArr[i12] = b12;
        int i13 = i12 + 1;
        C5663aRz b13 = C5640aRc.m17844b(Strike.class, "ac0191e23cb237bbfe0b81121c29b2a3", i13);
        bjQ = b13;
        arzArr[i13] = b13;
        int i14 = i13 + 1;
        C5663aRz b14 = C5640aRc.m17844b(Strike.class, "c338f0526df1b6a9a515b43c9d77a45d", i14);
        bjS = b14;
        arzArr[i14] = b14;
        int i15 = i14 + 1;
        C5663aRz b15 = C5640aRc.m17844b(Strike.class, "3ffc7c22d3f5fe159abdb3fcda18390f", i15);
        bjU = b15;
        arzArr[i15] = b15;
        int i16 = i15 + 1;
        _m_fields = arzArr;
        C1634Xv.m11725a((Object[]) TaikodomObject._m_fields, (Object[]) _m_fields);
        int i17 = TaikodomObject._m_methodCount;
        C2491fm[] fmVarArr = new C2491fm[(i17 + 31)];
        C2491fm a = C4105zY.m41624a(Strike.class, "d678a73a951ddafc6642a53dffddf923", i17);
        aPf = a;
        fmVarArr[i17] = a;
        int i18 = i17 + 1;
        C2491fm a2 = C4105zY.m41624a(Strike.class, "0e89fa6f20d8d40ca750b1a0de293f8a", i18);
        aPe = a2;
        fmVarArr[i18] = a2;
        int i19 = i18 + 1;
        C2491fm a3 = C4105zY.m41624a(Strike.class, "6bf246effe78e464a347df736540f96d", i19);
        bjV = a3;
        fmVarArr[i19] = a3;
        int i20 = i19 + 1;
        C2491fm a4 = C4105zY.m41624a(Strike.class, "76a22bded5d6ce9d0c5610efbaf27f18", i20);
        bjW = a4;
        fmVarArr[i20] = a4;
        int i21 = i20 + 1;
        C2491fm a5 = C4105zY.m41624a(Strike.class, "9db8336ff4dd5a34015d956652dd3ca3", i21);
        bjX = a5;
        fmVarArr[i21] = a5;
        int i22 = i21 + 1;
        C2491fm a6 = C4105zY.m41624a(Strike.class, "d9c22512eed9d06be1b10cf782b57e59", i22);
        atY = a6;
        fmVarArr[i22] = a6;
        int i23 = i22 + 1;
        C2491fm a7 = C4105zY.m41624a(Strike.class, "48fd18b9016fb70ece99a40657295f2b", i23);
        bjY = a7;
        fmVarArr[i23] = a7;
        int i24 = i23 + 1;
        C2491fm a8 = C4105zY.m41624a(Strike.class, "173976c9dff3f80b58dbeeb4e27ff359", i24);
        _f_tick_0020_0028F_0029V = a8;
        fmVarArr[i24] = a8;
        int i25 = i24 + 1;
        C2491fm a9 = C4105zY.m41624a(Strike.class, "7307d4ee87672e7755425db14a82739e", i25);
        bjZ = a9;
        fmVarArr[i25] = a9;
        int i26 = i25 + 1;
        C2491fm a10 = C4105zY.m41624a(Strike.class, "f186656998d131c67ddad9f474d57b64", i26);
        bka = a10;
        fmVarArr[i26] = a10;
        int i27 = i26 + 1;
        C2491fm a11 = C4105zY.m41624a(Strike.class, "704d6fbc2ef4b4947118f7abdd4c41ed", i27);
        bkb = a11;
        fmVarArr[i27] = a11;
        int i28 = i27 + 1;
        C2491fm a12 = C4105zY.m41624a(Strike.class, "3788ea50ecf4138a2ebfb8ea62233a11", i28);
        bkc = a12;
        fmVarArr[i28] = a12;
        int i29 = i28 + 1;
        C2491fm a13 = C4105zY.m41624a(Strike.class, "ed25375886d9ca5817f82ffce0dd93a4", i29);
        bkd = a13;
        fmVarArr[i29] = a13;
        int i30 = i29 + 1;
        C2491fm a14 = C4105zY.m41624a(Strike.class, "2eb234bbe4d4f5887e52ba556c41cb1a", i30);
        bke = a14;
        fmVarArr[i30] = a14;
        int i31 = i30 + 1;
        C2491fm a15 = C4105zY.m41624a(Strike.class, "681968417b425299bad40bb2726f7610", i31);
        bkf = a15;
        fmVarArr[i31] = a15;
        int i32 = i31 + 1;
        C2491fm a16 = C4105zY.m41624a(Strike.class, "3d9426ce2369a0cff4dbddf75736b7f9", i32);
        bkg = a16;
        fmVarArr[i32] = a16;
        int i33 = i32 + 1;
        C2491fm a17 = C4105zY.m41624a(Strike.class, "0c2aa4ca1d2459052d8f8acefe1cb261", i33);
        bkh = a17;
        fmVarArr[i33] = a17;
        int i34 = i33 + 1;
        C2491fm a18 = C4105zY.m41624a(Strike.class, "a33aab892b11484bc34e17e701980026", i34);
        bki = a18;
        fmVarArr[i34] = a18;
        int i35 = i34 + 1;
        C2491fm a19 = C4105zY.m41624a(Strike.class, "220e04a6cc27308fcc188bfe78b1fd60", i35);
        bkj = a19;
        fmVarArr[i35] = a19;
        int i36 = i35 + 1;
        C2491fm a20 = C4105zY.m41624a(Strike.class, "49a027356df63ba3e77ebb0df014be1d", i36);
        bkk = a20;
        fmVarArr[i36] = a20;
        int i37 = i36 + 1;
        C2491fm a21 = C4105zY.m41624a(Strike.class, "d4c4a3560a09ec85be09f91e32c71d98", i37);
        bkl = a21;
        fmVarArr[i37] = a21;
        int i38 = i37 + 1;
        C2491fm a22 = C4105zY.m41624a(Strike.class, "0a37947bf2a11268b154e81ca950c1db", i38);
        f6507x851d656b = a22;
        fmVarArr[i38] = a22;
        int i39 = i38 + 1;
        C2491fm a23 = C4105zY.m41624a(Strike.class, "4028eea6ecec13bbbad30c3b90aa2095", i39);
        aPi = a23;
        fmVarArr[i39] = a23;
        int i40 = i39 + 1;
        C2491fm a24 = C4105zY.m41624a(Strike.class, "8036a6358f5146e551f87a1818c2e5b8", i40);
        bbU = a24;
        fmVarArr[i40] = a24;
        int i41 = i40 + 1;
        C2491fm a25 = C4105zY.m41624a(Strike.class, "3b77ea7931aa67170db204b5daf7ff9a", i41);
        f6508yg = a25;
        fmVarArr[i41] = a25;
        int i42 = i41 + 1;
        C2491fm a26 = C4105zY.m41624a(Strike.class, "b062db490617d3ecfbe4e6a2ace2593d", i42);
        bbS = a26;
        fmVarArr[i42] = a26;
        int i43 = i42 + 1;
        C2491fm a27 = C4105zY.m41624a(Strike.class, "9d629df285f3fe8e4a8614a475846e66", i43);
        f6506Lm = a27;
        fmVarArr[i43] = a27;
        int i44 = i43 + 1;
        C2491fm a28 = C4105zY.m41624a(Strike.class, "23d7fa3763d59763be8c6508f46f8172", i44);
        aPl = a28;
        fmVarArr[i44] = a28;
        int i45 = i44 + 1;
        C2491fm a29 = C4105zY.m41624a(Strike.class, "435e294a7eb17d34f0355d8836fbb880", i45);
        aPj = a29;
        fmVarArr[i45] = a29;
        int i46 = i45 + 1;
        C2491fm a30 = C4105zY.m41624a(Strike.class, "a3b8e0ab443598dba2b936de41d5f07e", i46);
        aPk = a30;
        fmVarArr[i46] = a30;
        int i47 = i46 + 1;
        C2491fm a31 = C4105zY.m41624a(Strike.class, "fc02248e7b6b09b8fffe5ea05a708a9b", i47);
        aPm = a31;
        fmVarArr[i47] = a31;
        int i48 = i47 + 1;
        _m_methods = fmVarArr;
        C1634Xv.m11725a((Object[]) TaikodomObject._m_methods, (Object[]) _m_methods);
        ___iScriptClass = aUO.m18566a(Strike.class, aQM.class, _m_fields, _m_methods);
    }

    /* renamed from: B */
    private void m28842B(Asset tCVar) {
        throw new C6039afL();
    }

    /* renamed from: C */
    private void m28843C(Asset tCVar) {
        throw new C6039afL();
    }

    /* renamed from: J */
    private void m28844J(C3438ra raVar) {
        throw new C6039afL();
    }

    /* renamed from: Km */
    private C1556Wo m28845Km() {
        return ((StrikeType) getType()).mo8299UD();
    }

    /* renamed from: Mu */
    private float m28847Mu() {
        return ((StrikeType) getType()).mo8308jt();
    }

    /* renamed from: Mv */
    private Asset m28848Mv() {
        return ((StrikeType) getType()).mo8298My();
    }

    /* renamed from: Mw */
    private Asset m28849Mw() {
        return ((StrikeType) getType()).mo8297MA();
    }

    /* renamed from: TH */
    private void m28851TH() {
        switch (bFf().mo6893i(aPe)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, aPe, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, aPe, new Object[0]));
                break;
        }
        m28850TG();
    }

    /* renamed from: TP */
    private void m28856TP() {
        switch (bFf().mo6893i(aPl)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, aPl, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, aPl, new Object[0]));
                break;
        }
        m28855TO();
    }

    /* renamed from: Te */
    private AdapterLikedList m28858Te() {
        return (AdapterLikedList) bFf().mo5608dq().mo3214p(aOx);
    }

    /* renamed from: Tn */
    private Ship m28859Tn() {
        return (Ship) bFf().mo5608dq().mo3214p(aOP);
    }

    /* renamed from: Yx */
    private float m28864Yx() {
        return ((StrikeType) getType()).mo8300YE();
    }

    /* renamed from: Yy */
    private float m28865Yy() {
        return ((StrikeType) getType()).mo8301YG();
    }

    /* renamed from: a */
    private void m28867a(C6809auB.C1996a aVar) {
        bFf().mo5608dq().mo3197f(bid, aVar);
    }

    /* renamed from: a */
    private void m28868a(AdapterLikedList zAVar) {
        bFf().mo5608dq().mo3197f(aOx, zAVar);
    }

    private C1556Wo acA() {
        return (C1556Wo) bFf().mo5608dq().mo3214p(bjO);
    }

    private float acB() {
        return bFf().mo5608dq().mo3211m(bjQ);
    }

    private float acC() {
        return bFf().mo5608dq().mo3211m(bjS);
    }

    private float acD() {
        return bFf().mo5608dq().mo3211m(bjU);
    }

    @C4034yP
    @C5566aOg
    @C0064Am(aul = "f186656998d131c67ddad9f474d57b64", aum = 0)
    @C2499fr
    private void acK() {
        throw new aWi(new aCE(this, bka, new Object[0]));
    }

    @C4034yP
    @C2499fr
    @ClientOnly
    private void acM() {
        switch (bFf().mo6893i(bkb)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, bkb, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, bkb, new Object[0]));
                break;
        }
        acL();
    }

    @C4034yP
    @C2499fr
    @ClientOnly
    private void acO() {
        switch (bFf().mo6893i(bkc)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, bkc, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, bkc, new Object[0]));
                break;
        }
        acN();
    }

    @C4034yP
    @C5566aOg
    @C0064Am(aul = "ed25375886d9ca5817f82ffce0dd93a4", aum = 0)
    @C2499fr
    private void acP() {
        throw new aWi(new aCE(this, bkd, new Object[0]));
    }

    private void acR() {
        switch (bFf().mo6893i(bke)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, bke, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, bke, new Object[0]));
                break;
        }
        acQ();
    }

    private void acT() {
        switch (bFf().mo6893i(bkf)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, bkf, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, bkf, new Object[0]));
                break;
        }
        acS();
    }

    private void acV() {
        switch (bFf().mo6893i(bkg)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, bkg, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, bkg, new Object[0]));
                break;
        }
        acU();
    }

    private void acX() {
        switch (bFf().mo6893i(bki)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, bki, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, bki, new Object[0]));
                break;
        }
        acW();
    }

    private C3438ra acx() {
        return ((StrikeType) getType()).cVx();
    }

    private C6809auB.C1996a acy() {
        return (C6809auB.C1996a) bFf().mo5608dq().mo3214p(bid);
    }

    private C1556Wo acz() {
        return (C1556Wo) bFf().mo5608dq().mo3214p(bjM);
    }

    /* renamed from: ba */
    private void m28871ba(float f) {
        throw new C6039afL();
    }

    /* renamed from: c */
    private void m28872c(Weapon adv) {
        switch (bFf().mo6893i(bkh)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, bkh, new Object[]{adv}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, bkh, new Object[]{adv}));
                break;
        }
        m28869b(adv);
    }

    /* renamed from: cW */
    private void m28873cW(float f) {
        throw new C6039afL();
    }

    /* renamed from: cX */
    private void m28874cX(float f) {
        throw new C6039afL();
    }

    /* renamed from: di */
    private void m28877di(float f) {
        bFf().mo5608dq().mo3150a(bjQ, f);
    }

    /* renamed from: dj */
    private void m28878dj(float f) {
        bFf().mo5608dq().mo3150a(bjS, f);
    }

    /* renamed from: dk */
    private void m28879dk(float f) {
        bFf().mo5608dq().mo3150a(bjU, f);
    }

    /* renamed from: e */
    private void m28881e(Weapon adv) {
        switch (bFf().mo6893i(bkj)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, bkj, new Object[]{adv}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, bkj, new Object[]{adv}));
                break;
        }
        m28875d(adv);
    }

    /* access modifiers changed from: private */
    /* renamed from: f */
    public C3892wO m28882f(DamageType fr) {
        switch (bFf().mo6893i(atY)) {
            case 0:
                return null;
            case 2:
                return (C3892wO) bFf().mo5606d(new aCE(this, atY, new Object[]{fr}));
            case 3:
                bFf().mo5606d(new aCE(this, atY, new Object[]{fr}));
                break;
        }
        return m28880e(fr);
    }

    /* renamed from: i */
    private void m28883i(C1556Wo wo) {
        throw new C6039afL();
    }

    /* renamed from: k */
    private void m28885k(Ship fAVar) {
        bFf().mo5608dq().mo3197f(aOP, fAVar);
    }

    /* renamed from: q */
    private void m28888q(C1556Wo wo) {
        bFf().mo5608dq().mo3197f(bjM, wo);
    }

    /* renamed from: r */
    private void m28890r(C1556Wo wo) {
        bFf().mo5608dq().mo3197f(bjO, wo);
    }

    /* renamed from: S */
    public void mo10S() {
        super.mo10S();
    }

    /* renamed from: T */
    public C6494any mo11T() {
        return ___iScriptClass;
    }

    /* renamed from: TJ */
    public AdapterLikedList<StrikeAdapter> mo5353TJ() {
        switch (bFf().mo6893i(aPf)) {
            case 0:
                return null;
            case 2:
                return (AdapterLikedList) bFf().mo5606d(new aCE(this, aPf, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, aPf, new Object[0]));
                break;
        }
        return m28852TI();
    }

    /* renamed from: TL */
    public void mo12905TL() {
        switch (bFf().mo6893i(aPj)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, aPj, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, aPj, new Object[0]));
                break;
        }
        m28853TK();
    }

    /* renamed from: TN */
    public void mo12906TN() {
        switch (bFf().mo6893i(aPk)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, aPk, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, aPk, new Object[0]));
                break;
        }
        m28854TM();
    }

    /* renamed from: TR */
    public void mo17786TR() {
        switch (bFf().mo6893i(aPm)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, aPm, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, aPm, new Object[0]));
                break;
        }
        m28857TQ();
    }

    /* renamed from: U */
    public C2491fm[] mo12U() {
        return _m_methods;
    }

    /* renamed from: V */
    public void mo4709V(float f) {
        switch (bFf().mo6893i(_f_tick_0020_0028F_0029V)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, _f_tick_0020_0028F_0029V, new Object[]{new Float(f)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, _f_tick_0020_0028F_0029V, new Object[]{new Float(f)}));
                break;
        }
        m28860U(f);
    }

    /* renamed from: W */
    public Object mo13W() {
        return new aQM(this);
    }

    /* renamed from: YE */
    public float mo17787YE() {
        switch (bFf().mo6893i(bbS)) {
            case 0:
                return 0.0f;
            case 2:
                return ((Float) bFf().mo5606d(new aCE(this, bbS, new Object[0]))).floatValue();
            case 3:
                bFf().mo5606d(new aCE(this, bbS, new Object[0]));
                break;
        }
        return m28862YD();
    }

    /* renamed from: YG */
    public float mo17788YG() {
        switch (bFf().mo6893i(bbU)) {
            case 0:
                return 0.0f;
            case 2:
                return ((Float) bFf().mo5606d(new aCE(this, bbU, new Object[0]))).floatValue();
            case 3:
                bFf().mo5606d(new aCE(this, bbU, new Object[0]));
                break;
        }
        return m28863YF();
    }

    /* renamed from: a */
    public Object mo14a(C0495Gr gr) {
        Object[] args = gr.getArgs();
        switch (gr.mo2417hq() - TaikodomObject._m_methodCount) {
            case 0:
                return m28852TI();
            case 1:
                m28850TG();
                return null;
            case 2:
                return new Float(acE());
            case 3:
                return new Float(acG());
            case 4:
                return new Float(acI());
            case 5:
                return m28880e((DamageType) args[0]);
            case 6:
                return m28887m((DamageType) args[0]);
            case 7:
                m28860U(((Float) args[0]).floatValue());
                return null;
            case 8:
                return new Boolean(m28870b((C6809auB.C1996a) args[0]));
            case 9:
                acK();
                return null;
            case 10:
                acL();
                return null;
            case 11:
                acN();
                return null;
            case 12:
                acP();
                return null;
            case 13:
                acQ();
                return null;
            case 14:
                acS();
                return null;
            case 15:
                acU();
                return null;
            case 16:
                m28869b((Weapon) args[0]);
                return null;
            case 17:
                acW();
                return null;
            case 18:
                m28875d((Weapon) args[0]);
                return null;
            case 19:
                return acY();
            case 20:
                m28876d((C6809auB.C1996a) args[0]);
                return null;
            case 21:
                return m28846Mo();
            case 22:
                m28886l((Ship) args[0]);
                return null;
            case 23:
                return new Float(m28863YF());
            case 24:
                return new Float(m28884js());
            case 25:
                return new Float(m28862YD());
            case 26:
                m28889qU();
                return null;
            case 27:
                m28855TO();
                return null;
            case 28:
                m28853TK();
                return null;
            case 29:
                m28854TM();
                return null;
            case 30:
                m28857TQ();
                return null;
            default:
                return super.mo14a(gr);
        }
    }

    /* renamed from: a */
    public void mo15a(Collection collection, C0495Gr gr) {
        super.mo15a(collection, gr);
    }

    @C4034yP
    @C5566aOg
    @C2499fr
    public void abort() {
        switch (bFf().mo6893i(bkd)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, bkd, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, bkd, new Object[0]));
                break;
        }
        acP();
    }

    public float acF() {
        switch (bFf().mo6893i(bjV)) {
            case 0:
                return 0.0f;
            case 2:
                return ((Float) bFf().mo5606d(new aCE(this, bjV, new Object[0]))).floatValue();
            case 3:
                bFf().mo5606d(new aCE(this, bjV, new Object[0]));
                break;
        }
        return acE();
    }

    public float acH() {
        switch (bFf().mo6893i(bjW)) {
            case 0:
                return 0.0f;
            case 2:
                return ((Float) bFf().mo5606d(new aCE(this, bjW, new Object[0]))).floatValue();
            case 3:
                bFf().mo5606d(new aCE(this, bjW, new Object[0]));
                break;
        }
        return acG();
    }

    public float acJ() {
        switch (bFf().mo6893i(bjX)) {
            case 0:
                return 0.0f;
            case 2:
                return ((Float) bFf().mo5606d(new aCE(this, bjX, new Object[0]))).floatValue();
            case 3:
                bFf().mo5606d(new aCE(this, bjX, new Object[0]));
                break;
        }
        return acI();
    }

    public C6809auB.C1996a acZ() {
        switch (bFf().mo6893i(bkk)) {
            case 0:
                return null;
            case 2:
                return (C6809auB.C1996a) bFf().mo5606d(new aCE(this, bkk, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, bkk, new Object[0]));
                break;
        }
        return acY();
    }

    @C4034yP
    @C5566aOg
    @C2499fr
    public void activate() {
        switch (bFf().mo6893i(bka)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, bka, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, bka, new Object[0]));
                break;
        }
        acK();
    }

    /* renamed from: al */
    public Ship mo17795al() {
        switch (bFf().mo6893i(f6507x851d656b)) {
            case 0:
                return null;
            case 2:
                return (Ship) bFf().mo5606d(new aCE(this, f6507x851d656b, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f6507x851d656b, new Object[0]));
                break;
        }
        return m28846Mo();
    }

    /* renamed from: c */
    public boolean mo17796c(C6809auB.C1996a aVar) {
        switch (bFf().mo6893i(bjZ)) {
            case 0:
                return false;
            case 2:
                return ((Boolean) bFf().mo5606d(new aCE(this, bjZ, new Object[]{aVar}))).booleanValue();
            case 3:
                bFf().mo5606d(new aCE(this, bjZ, new Object[]{aVar}));
                break;
        }
        return m28870b(aVar);
    }

    /* renamed from: c */
    public C5663aRz[] mo25c() {
        return _m_fields;
    }

    /* renamed from: e */
    public void mo17798e(C6809auB.C1996a aVar) {
        switch (bFf().mo6893i(bkl)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, bkl, new Object[]{aVar}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, bkl, new Object[]{aVar}));
                break;
        }
        m28876d(aVar);
    }

    /* renamed from: jt */
    public float mo17799jt() {
        switch (bFf().mo6893i(f6508yg)) {
            case 0:
                return 0.0f;
            case 2:
                return ((Float) bFf().mo5606d(new aCE(this, f6508yg, new Object[0]))).floatValue();
            case 3:
                bFf().mo5606d(new aCE(this, f6508yg, new Object[0]));
                break;
        }
        return m28884js();
    }

    /* renamed from: m */
    public void mo17800m(Ship fAVar) {
        switch (bFf().mo6893i(aPi)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, aPi, new Object[]{fAVar}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, aPi, new Object[]{fAVar}));
                break;
        }
        m28886l(fAVar);
    }

    /* renamed from: n */
    public C3892wO mo17801n(DamageType fr) {
        switch (bFf().mo6893i(bjY)) {
            case 0:
                return null;
            case 2:
                return (C3892wO) bFf().mo5606d(new aCE(this, bjY, new Object[]{fr}));
            case 3:
                bFf().mo5606d(new aCE(this, bjY, new Object[]{fr}));
                break;
        }
        return m28887m(fr);
    }

    /* renamed from: qV */
    public void mo17802qV() {
        switch (bFf().mo6893i(f6506Lm)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f6506Lm, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f6506Lm, new Object[0]));
                break;
        }
        m28889qU();
    }

    /* renamed from: d */
    public void mo17797d(StrikeType add) {
        super.mo967a((C2961mJ) add);
        m28851TH();
    }

    @C0064Am(aul = "d678a73a951ddafc6642a53dffddf923", aum = 0)
    /* renamed from: TI */
    private AdapterLikedList<StrikeAdapter> m28852TI() {
        if (m28858Te() == null) {
            m28851TH();
        }
        return m28858Te();
    }

    @C0064Am(aul = "0e89fa6f20d8d40ca750b1a0de293f8a", aum = 0)
    /* renamed from: TG */
    private void m28850TG() {
        if (m28858Te() == null) {
            AdapterLikedList zAVar = (AdapterLikedList) bFf().mo6865M(AdapterLikedList.class);
            BaseStrikeAdapter bVar = (BaseStrikeAdapter) bFf().mo6865M(BaseStrikeAdapter.class);
            bVar.mo17804b(this);
            zAVar.mo23260c(bVar);
            m28868a(zAVar);
            m28858Te().mo23259b((C6872avM) this);
            m28856TP();
        }
    }

    @C0064Am(aul = "6bf246effe78e464a347df736540f96d", aum = 0)
    private float acE() {
        return m28865Yy();
    }

    @C0064Am(aul = "76a22bded5d6ce9d0c5610efbaf27f18", aum = 0)
    private float acG() {
        return m28864Yx();
    }

    @C0064Am(aul = "9db8336ff4dd5a34015d956652dd3ca3", aum = 0)
    private float acI() {
        return m28847Mu();
    }

    @C0064Am(aul = "d9c22512eed9d06be1b10cf782b57e59", aum = 0)
    /* renamed from: e */
    private C3892wO m28880e(DamageType fr) {
        return (C3892wO) m28845Km().get(fr);
    }

    @C0064Am(aul = "48fd18b9016fb70ece99a40657295f2b", aum = 0)
    /* renamed from: m */
    private C3892wO m28887m(DamageType fr) {
        return (C3892wO) acA().get(fr);
    }

    @C0064Am(aul = "173976c9dff3f80b58dbeeb4e27ff359", aum = 0)
    /* renamed from: U */
    private void m28860U(float f) {
        if (mo17796c(C6809auB.C1996a.COOLDOWN)) {
            acT();
        } else if (mo17796c(C6809auB.C1996a.ACTIVE)) {
            acR();
        }
    }

    @C0064Am(aul = "7307d4ee87672e7755425db14a82739e", aum = 0)
    /* renamed from: b */
    private boolean m28870b(C6809auB.C1996a aVar) {
        return acy() == aVar;
    }

    @C4034yP
    @ClientOnly
    @C0064Am(aul = "704d6fbc2ef4b4947118f7abdd4c41ed", aum = 0)
    @C2499fr
    private void acL() {
        IEngineGraphics ale = ald().getEngineGraphics();
        if (ale != null && m28848Mv() != null && ale.aed() > 0.0f) {
            new C3257pi().mo21213a(m28848Mv(), new Vec3d(ScriptRuntime.NaN, ScriptRuntime.NaN, ScriptRuntime.NaN), mo17795al().cHg(), C3257pi.C3261d.NEAR_ENOUGH);
        }
    }

    @C4034yP
    @ClientOnly
    @C0064Am(aul = "3788ea50ecf4138a2ebfb8ea62233a11", aum = 0)
    @C2499fr
    private void acN() {
        IEngineGraphics ale = ald().getEngineGraphics();
        if (ale != null && m28849Mw() != null && ale.aed() > 0.0f) {
            new C3257pi().mo21213a(m28849Mw(), new Vec3d(ScriptRuntime.NaN, ScriptRuntime.NaN, ScriptRuntime.NaN), mo17795al().cHg(), C3257pi.C3261d.NEAR_ENOUGH);
        }
    }

    @C0064Am(aul = "2eb234bbe4d4f5887e52ba556c41cb1a", aum = 0)
    private void acQ() {
        acX();
        mo17798e(C6809auB.C1996a.COOLDOWN);
        mo8361ms(mo5353TJ().ase().mo7995YG());
        if (m28859Tn().agj() instanceof Player) {
            ((Player) m28859Tn().agj()).mo14419f((C1506WA) new C3853vo());
            ((Player) m28859Tn().agj()).mo14419f((C1506WA) new C0284Df(((NLSProgression) ala().aIY().mo6310c(NLSManager.C1472a.PROGRESSION)).cya(), false, new Object[0]));
        }
        acO();
    }

    @C0064Am(aul = "681968417b425299bad40bb2726f7610", aum = 0)
    private void acS() {
        mo17798e(C6809auB.C1996a.DEACTIVE);
    }

    @C0064Am(aul = "3d9426ce2369a0cff4dbddf75736b7f9", aum = 0)
    private void acU() {
        for (Weapon next : mo17795al().agL()) {
            if (acx().contains(next.aqz().mo19866HC())) {
                m28872c(next);
            }
        }
    }

    @C0064Am(aul = "0c2aa4ca1d2459052d8f8acefe1cb261", aum = 0)
    /* renamed from: b */
    private void m28869b(Weapon adv) {
        StrikeDamageAdapter aVar = (StrikeDamageAdapter) bFf().mo6865M(StrikeDamageAdapter.class);
        aVar.mo17803a(this, adv);
        adv.mo5353TJ().mo23261e(aVar);
        acz().put(adv, aVar);
    }

    @C0064Am(aul = "a33aab892b11484bc34e17e701980026", aum = 0)
    private void acW() {
        ArrayList<Weapon> arrayList = new ArrayList<>();
        for (Weapon adv : acz().keySet()) {
            if (acx().contains(adv.aqz().mo19866HC())) {
                m28881e(adv);
                arrayList.add(adv);
            }
        }
        for (Weapon remove : arrayList) {
            acz().remove(remove);
        }
    }

    @C0064Am(aul = "220e04a6cc27308fcc188bfe78b1fd60", aum = 0)
    /* renamed from: d */
    private void m28875d(Weapon adv) {
        adv.mo5353TJ().mo23262g((StrikeDamageAdapter) acz().get(adv));
    }

    @C0064Am(aul = "49a027356df63ba3e77ebb0df014be1d", aum = 0)
    private C6809auB.C1996a acY() {
        return acy();
    }

    @C0064Am(aul = "d4c4a3560a09ec85be09f91e32c71d98", aum = 0)
    /* renamed from: d */
    private void m28876d(C6809auB.C1996a aVar) {
        m28867a(aVar);
    }

    @C0064Am(aul = "0a37947bf2a11268b154e81ca950c1db", aum = 0)
    /* renamed from: Mo */
    private Ship m28846Mo() {
        return m28859Tn();
    }

    @C0064Am(aul = "4028eea6ecec13bbbad30c3b90aa2095", aum = 0)
    /* renamed from: l */
    private void m28886l(Ship fAVar) {
        m28885k(fAVar);
    }

    @C0064Am(aul = "8036a6358f5146e551f87a1818c2e5b8", aum = 0)
    /* renamed from: YF */
    private float m28863YF() {
        return acB();
    }

    @C0064Am(aul = "3b77ea7931aa67170db204b5daf7ff9a", aum = 0)
    /* renamed from: js */
    private float m28884js() {
        return acC();
    }

    @C0064Am(aul = "b062db490617d3ecfbe4e6a2ace2593d", aum = 0)
    /* renamed from: YD */
    private float m28862YD() {
        return acD();
    }

    @C0064Am(aul = "9d629df285f3fe8e4a8614a475846e66", aum = 0)
    /* renamed from: qU */
    private void m28889qU() {
        push();
    }

    @C0064Am(aul = "23d7fa3763d59763be8c6508f46f8172", aum = 0)
    /* renamed from: TO */
    private void m28855TO() {
        m28877di(((StrikeAdapter) m28858Te().ase()).mo7995YG());
        m28878dj(((StrikeAdapter) m28858Te().ase()).mo7996jt());
        m28879dk(((StrikeAdapter) m28858Te().ase()).mo7994YE());
        for (DamageType next : ala().aLs()) {
            acA().put(next, ((StrikeAdapter) m28858Te().ase()).mo7997t(next));
        }
    }

    @C0064Am(aul = "435e294a7eb17d34f0355d8836fbb880", aum = 0)
    /* renamed from: TK */
    private void m28853TK() {
        m28856TP();
    }

    @C0064Am(aul = "a3b8e0ab443598dba2b936de41d5f07e", aum = 0)
    /* renamed from: TM */
    private void m28854TM() {
        m28856TP();
    }

    @C0064Am(aul = "fc02248e7b6b09b8fffe5ea05a708a9b", aum = 0)
    /* renamed from: TQ */
    private void m28857TQ() {
        if (m28858Te() == null) {
            mo8358lY(String.valueOf(bFY()) + " with NULL adapterList");
            return;
        }
        m28858Te().mo23259b((C6872avM) this);
        m28856TP();
    }

    @C6485anp
    @C5511aMd
    /* renamed from: a.dQ$a */
    public class StrikeDamageAdapter extends WeaponAdapter implements C1616Xf {

        /* renamed from: AG */
        public static final C5663aRz f6509AG = null;
        /* renamed from: AI */
        public static final C2491fm f6511AI = null;
        public static final int _m_fieldCount = 0;
        public static final C5663aRz[] _m_fields = null;
        public static final int _m_methodCount = 0;
        public static final C2491fm[] _m_methods = null;
        /* renamed from: aT */
        public static final C5663aRz f6512aT = null;
        public static final long serialVersionUID = 0;
        public static C6494any ___iScriptClass;
        @C0064Am(aul = "a3cf0a6afb75136526f746f62fd69487", aum = 1)

        /* renamed from: AH */
        static /* synthetic */ Strike f6510AH;
        @C0064Am(aul = "c649767e5eaeb21311f3f9a1bd0a6ca9", aum = 0)

        /* renamed from: oX */
        private static Weapon f6513oX;

        static {
            m28913V();
        }

        public StrikeDamageAdapter() {
            super((C5540aNg) null);
            super.mo10S();
        }

        public StrikeDamageAdapter(C5540aNg ang) {
            super(ang);
        }

        public StrikeDamageAdapter(Strike dQVar, Weapon adv) {
            super((C5540aNg) null);
            super._m_script_init(dQVar, adv);
        }

        /* renamed from: V */
        static void m28913V() {
            serialVersionUID = (long) 1;
            _m_fieldCount = WeaponAdapter._m_fieldCount + 2;
            _m_methodCount = WeaponAdapter._m_methodCount + 1;
            int i = WeaponAdapter._m_fieldCount;
            C5663aRz[] arzArr = new C5663aRz[(i + 2)];
            C5663aRz b = C5640aRc.m17844b(StrikeDamageAdapter.class, "c649767e5eaeb21311f3f9a1bd0a6ca9", i);
            f6509AG = b;
            arzArr[i] = b;
            int i2 = i + 1;
            C5663aRz b2 = C5640aRc.m17844b(StrikeDamageAdapter.class, "a3cf0a6afb75136526f746f62fd69487", i2);
            f6512aT = b2;
            arzArr[i2] = b2;
            int i3 = i2 + 1;
            _m_fields = arzArr;
            C1634Xv.m11725a((Object[]) WeaponAdapter._m_fields, (Object[]) _m_fields);
            int i4 = WeaponAdapter._m_methodCount;
            C2491fm[] fmVarArr = new C2491fm[(i4 + 1)];
            C2491fm a = C4105zY.m41624a(StrikeDamageAdapter.class, "04de8d6dbdfa61a2a8900d9185566f35", i4);
            f6511AI = a;
            fmVarArr[i4] = a;
            int i5 = i4 + 1;
            _m_methods = fmVarArr;
            C1634Xv.m11725a((Object[]) WeaponAdapter._m_methods, (Object[]) _m_methods);
            ___iScriptClass = aUO.m18566a(StrikeDamageAdapter.class, C5304aEe.class, _m_fields, _m_methods);
        }

        /* renamed from: a */
        private void m28914a(Weapon adv) {
            bFf().mo5608dq().mo3197f(f6509AG, adv);
        }

        /* renamed from: a */
        private void m28915a(Strike dQVar) {
            bFf().mo5608dq().mo3197f(f6512aT, dQVar);
        }

        /* renamed from: kk */
        private Weapon m28917kk() {
            return (Weapon) bFf().mo5608dq().mo3214p(f6509AG);
        }

        /* renamed from: kl */
        private Strike m28918kl() {
            return (Strike) bFf().mo5608dq().mo3214p(f6512aT);
        }

        /* renamed from: S */
        public void mo10S() {
            super.mo10S();
        }

        /* renamed from: T */
        public C6494any mo11T() {
            return ___iScriptClass;
        }

        /* renamed from: U */
        public C2491fm[] mo12U() {
            return _m_methods;
        }

        /* renamed from: W */
        public Object mo13W() {
            return new C5304aEe(this);
        }

        /* renamed from: a */
        public Object mo14a(C0495Gr gr) {
            Object[] args = gr.getArgs();
            switch (gr.mo2417hq() - WeaponAdapter._m_methodCount) {
                case 0:
                    return new Float(m28916c((DamageType) args[0]));
                default:
                    return super.mo14a(gr);
            }
        }

        /* renamed from: a */
        public void mo15a(Collection collection, C0495Gr gr) {
            super.mo15a(collection, gr);
        }

        /* renamed from: c */
        public C5663aRz[] mo25c() {
            return _m_fields;
        }

        /* renamed from: d */
        public float mo5375d(DamageType fr) {
            switch (bFf().mo6893i(f6511AI)) {
                case 0:
                    return 0.0f;
                case 2:
                    return ((Float) bFf().mo5606d(new aCE(this, f6511AI, new Object[]{fr}))).floatValue();
                case 3:
                    bFf().mo5606d(new aCE(this, f6511AI, new Object[]{fr}));
                    break;
            }
            return m28916c(fr);
        }

        /* renamed from: a */
        public void mo17803a(Strike dQVar, Weapon adv) {
            m28915a(dQVar);
            super.mo10S();
            m28914a(adv);
        }

        @C0064Am(aul = "04de8d6dbdfa61a2a8900d9185566f35", aum = 0)
        /* renamed from: c */
        private float m28916c(DamageType fr) {
            if (m28917kk() instanceof ProjectileWeapon) {
                return m28918kl().mo17801n(fr).mo22753v(((WeaponAdapter) ((WeaponAdapter) mo16070IE()).cwL()).mo5375d(fr), super.mo5375d(fr));
            }
            return m28918kl().mo17801n(fr).mo22753v(((WeaponAdapter) mo16070IE()).mo5375d(fr), super.mo5375d(fr));
        }
    }

    @C6485anp
    @C5511aMd
    /* renamed from: a.dQ$b */
    /* compiled from: a */
    public class BaseStrikeAdapter extends StrikeAdapter implements C1616Xf {
        public static final int _m_fieldCount = 0;
        public static final C5663aRz[] _m_fields = null;
        public static final int _m_methodCount = 0;
        public static final C2491fm[] _m_methods = null;
        /* renamed from: aT */
        public static final C5663aRz f6515aT = null;
        public static final C2491fm bbS = null;
        public static final C2491fm bbU = null;
        public static final C2491fm ccE = null;
        public static final long serialVersionUID = 0;
        /* renamed from: yg */
        public static final C2491fm f6516yg = null;
        public static C6494any ___iScriptClass;
        @C0064Am(aul = "00b87c38dad8e00ba773428328604779", aum = 0)

        /* renamed from: AH */
        static /* synthetic */ Strike f6514AH;

        static {
            m28928V();
        }

        public BaseStrikeAdapter() {
            super((C5540aNg) null);
            super.mo10S();
        }

        public BaseStrikeAdapter(C5540aNg ang) {
            super(ang);
        }

        public BaseStrikeAdapter(Strike dQVar) {
            super((C5540aNg) null);
            super._m_script_init(dQVar);
        }

        /* renamed from: V */
        static void m28928V() {
            serialVersionUID = (long) 1;
            _m_fieldCount = StrikeAdapter._m_fieldCount + 1;
            _m_methodCount = StrikeAdapter._m_methodCount + 4;
            int i = StrikeAdapter._m_fieldCount;
            C5663aRz[] arzArr = new C5663aRz[(i + 1)];
            C5663aRz b = C5640aRc.m17844b(BaseStrikeAdapter.class, "00b87c38dad8e00ba773428328604779", i);
            f6515aT = b;
            arzArr[i] = b;
            int i2 = i + 1;
            _m_fields = arzArr;
            C1634Xv.m11725a((Object[]) StrikeAdapter._m_fields, (Object[]) _m_fields);
            int i3 = StrikeAdapter._m_methodCount;
            C2491fm[] fmVarArr = new C2491fm[(i3 + 4)];
            C2491fm a = C4105zY.m41624a(BaseStrikeAdapter.class, "c08ead12a6236b7c3c2ddd2615fdc8b1", i3);
            bbU = a;
            fmVarArr[i3] = a;
            int i4 = i3 + 1;
            C2491fm a2 = C4105zY.m41624a(BaseStrikeAdapter.class, "7699616a6e16e453846e9bb54c8df68e", i4);
            f6516yg = a2;
            fmVarArr[i4] = a2;
            int i5 = i4 + 1;
            C2491fm a3 = C4105zY.m41624a(BaseStrikeAdapter.class, "f5e3d2233be9730c898f0481a5ff7de6", i5);
            bbS = a3;
            fmVarArr[i5] = a3;
            int i6 = i5 + 1;
            C2491fm a4 = C4105zY.m41624a(BaseStrikeAdapter.class, "f25d85b796ba46955227d9f39b6ae7b3", i6);
            ccE = a4;
            fmVarArr[i6] = a4;
            int i7 = i6 + 1;
            _m_methods = fmVarArr;
            C1634Xv.m11725a((Object[]) StrikeAdapter._m_methods, (Object[]) _m_methods);
            ___iScriptClass = aUO.m18566a(BaseStrikeAdapter.class, C3124oC.class, _m_fields, _m_methods);
        }

        /* renamed from: a */
        private void m28931a(Strike dQVar) {
            bFf().mo5608dq().mo3197f(f6515aT, dQVar);
        }

        /* renamed from: kl */
        private Strike m28933kl() {
            return (Strike) bFf().mo5608dq().mo3214p(f6515aT);
        }

        /* renamed from: S */
        public void mo10S() {
            super.mo10S();
        }

        /* renamed from: T */
        public C6494any mo11T() {
            return ___iScriptClass;
        }

        /* renamed from: U */
        public C2491fm[] mo12U() {
            return _m_methods;
        }

        /* renamed from: W */
        public Object mo13W() {
            return new C3124oC(this);
        }

        /* renamed from: YE */
        public float mo7994YE() {
            switch (bFf().mo6893i(bbS)) {
                case 0:
                    return 0.0f;
                case 2:
                    return ((Float) bFf().mo5606d(new aCE(this, bbS, new Object[0]))).floatValue();
                case 3:
                    bFf().mo5606d(new aCE(this, bbS, new Object[0]));
                    break;
            }
            return m28929YD();
        }

        /* renamed from: YG */
        public float mo7995YG() {
            switch (bFf().mo6893i(bbU)) {
                case 0:
                    return 0.0f;
                case 2:
                    return ((Float) bFf().mo5606d(new aCE(this, bbU, new Object[0]))).floatValue();
                case 3:
                    bFf().mo5606d(new aCE(this, bbU, new Object[0]));
                    break;
            }
            return m28930YF();
        }

        /* renamed from: a */
        public Object mo14a(C0495Gr gr) {
            Object[] args = gr.getArgs();
            switch (gr.mo2417hq() - StrikeAdapter._m_methodCount) {
                case 0:
                    return new Float(m28930YF());
                case 1:
                    return new Float(m28932js());
                case 2:
                    return new Float(m28929YD());
                case 3:
                    return m28934s((DamageType) args[0]);
                default:
                    return super.mo14a(gr);
            }
        }

        /* renamed from: a */
        public void mo15a(Collection collection, C0495Gr gr) {
            super.mo15a(collection, gr);
        }

        /* renamed from: c */
        public C5663aRz[] mo25c() {
            return _m_fields;
        }

        /* renamed from: jt */
        public float mo7996jt() {
            switch (bFf().mo6893i(f6516yg)) {
                case 0:
                    return 0.0f;
                case 2:
                    return ((Float) bFf().mo5606d(new aCE(this, f6516yg, new Object[0]))).floatValue();
                case 3:
                    bFf().mo5606d(new aCE(this, f6516yg, new Object[0]));
                    break;
            }
            return m28932js();
        }

        /* renamed from: t */
        public C3892wO mo7997t(DamageType fr) {
            switch (bFf().mo6893i(ccE)) {
                case 0:
                    return null;
                case 2:
                    return (C3892wO) bFf().mo5606d(new aCE(this, ccE, new Object[]{fr}));
                case 3:
                    bFf().mo5606d(new aCE(this, ccE, new Object[]{fr}));
                    break;
            }
            return m28934s(fr);
        }

        /* renamed from: b */
        public void mo17804b(Strike dQVar) {
            m28931a(dQVar);
            super.mo10S();
        }

        @C0064Am(aul = "c08ead12a6236b7c3c2ddd2615fdc8b1", aum = 0)
        /* renamed from: YF */
        private float m28930YF() {
            return m28933kl().acF();
        }

        @C0064Am(aul = "7699616a6e16e453846e9bb54c8df68e", aum = 0)
        /* renamed from: js */
        private float m28932js() {
            return m28933kl().acJ();
        }

        @C0064Am(aul = "f5e3d2233be9730c898f0481a5ff7de6", aum = 0)
        /* renamed from: YD */
        private float m28929YD() {
            return m28933kl().acH();
        }

        @C0064Am(aul = "f25d85b796ba46955227d9f39b6ae7b3", aum = 0)
        /* renamed from: s */
        private C3892wO m28934s(DamageType fr) {
            return m28933kl().m28882f(fr);
        }
    }
}
