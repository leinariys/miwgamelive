package game.script.ship.strike;

import game.network.message.externalizable.aCE;
import game.network.message.serializable.C1556Wo;
import game.script.damage.DamageType;
import logic.baa.*;
import logic.data.mbean.C0106BL;
import logic.res.code.C5640aRc;
import logic.res.code.C5663aRz;
import logic.res.code.C6494any;
import logic.res.html.C2491fm;
import p001a.*;

import java.util.Collection;
import java.util.UUID;

@C5511aMd
@C6485anp
/* renamed from: a.akm  reason: case insensitive filesystem */
/* compiled from: a */
public abstract class BaseStrikeType extends C2961mJ implements C0468GU, C1616Xf {
    /* renamed from: _f_getTaikodom_0020_0028_0029Ltaikodom_002fgame_002fscript_002fTaikodom_003b */
    public static final C2491fm f4801xa236a942 = null;
    public static final int _m_fieldCount = 0;
    public static final C5663aRz[] _m_fields = null;
    public static final int _m_methodCount = 0;
    public static final C2491fm[] _m_methods = null;
    public static final C2491fm aQB = null;
    /* renamed from: bL */
    public static final C5663aRz f4803bL = null;
    /* renamed from: bM */
    public static final C5663aRz f4804bM = null;
    /* renamed from: bN */
    public static final C2491fm f4805bN = null;
    /* renamed from: bO */
    public static final C2491fm f4806bO = null;
    /* renamed from: bP */
    public static final C2491fm f4807bP = null;
    /* renamed from: bQ */
    public static final C2491fm f4808bQ = null;
    public static final long serialVersionUID = 0;
    public static C6494any ___iScriptClass;
    @C0064Am(aul = "d84e89d3f7277d1eac4c21c7838b2d60", aum = 0)

    /* renamed from: bK */
    private static UUID f4802bK;
    @C0064Am(aul = "8f9bb3ad8d7c85af52aa79ff38c8776e", aum = 1)
    private static String handle;

    static {
        m23540V();
    }

    public BaseStrikeType() {
        super((C5540aNg) null);
        super.mo10S();
    }

    public BaseStrikeType(C5540aNg ang) {
        super(ang);
    }

    /* renamed from: V */
    static void m23540V() {
        serialVersionUID = (long) 1;
        _m_fieldCount = C2961mJ._m_fieldCount + 2;
        _m_methodCount = C2961mJ._m_methodCount + 6;
        int i = C2961mJ._m_fieldCount;
        C5663aRz[] arzArr = new C5663aRz[(i + 2)];
        C5663aRz b = C5640aRc.m17844b(BaseStrikeType.class, "d84e89d3f7277d1eac4c21c7838b2d60", i);
        f4803bL = b;
        arzArr[i] = b;
        int i2 = i + 1;
        C5663aRz b2 = C5640aRc.m17844b(BaseStrikeType.class, "8f9bb3ad8d7c85af52aa79ff38c8776e", i2);
        f4804bM = b2;
        arzArr[i2] = b2;
        int i3 = i2 + 1;
        _m_fields = arzArr;
        C1634Xv.m11725a((Object[]) C2961mJ._m_fields, (Object[]) _m_fields);
        int i4 = C2961mJ._m_methodCount;
        C2491fm[] fmVarArr = new C2491fm[(i4 + 6)];
        C2491fm a = C4105zY.m41624a(BaseStrikeType.class, "cc8cfc182ac4414c775bec77d9218ea0", i4);
        f4805bN = a;
        fmVarArr[i4] = a;
        int i5 = i4 + 1;
        C2491fm a2 = C4105zY.m41624a(BaseStrikeType.class, "ea50a7b4e29d04fee0a1d2c5093a337c", i5);
        f4806bO = a2;
        fmVarArr[i5] = a2;
        int i6 = i5 + 1;
        C2491fm a3 = C4105zY.m41624a(BaseStrikeType.class, "8e4289c50cb9995a9c2540282d374126", i6);
        f4807bP = a3;
        fmVarArr[i6] = a3;
        int i7 = i6 + 1;
        C2491fm a4 = C4105zY.m41624a(BaseStrikeType.class, "9644d60bf7690a2670cd6ccd936b6127", i7);
        f4808bQ = a4;
        fmVarArr[i7] = a4;
        int i8 = i7 + 1;
        C2491fm a5 = C4105zY.m41624a(BaseStrikeType.class, "9073be6ca0574c00a0e13776601f9fed", i8);
        f4801xa236a942 = a5;
        fmVarArr[i8] = a5;
        int i9 = i8 + 1;
        C2491fm a6 = C4105zY.m41624a(BaseStrikeType.class, "62587f2a0a9718a3a077426079c10280", i9);
        aQB = a6;
        fmVarArr[i9] = a6;
        int i10 = i9 + 1;
        _m_methods = fmVarArr;
        C1634Xv.m11725a((Object[]) C2961mJ._m_methods, (Object[]) _m_methods);
        ___iScriptClass = aUO.m18566a(BaseStrikeType.class, C0106BL.class, _m_fields, _m_methods);
    }

    @C0064Am(aul = "62587f2a0a9718a3a077426079c10280", aum = 0)
    /* renamed from: UC */
    private C1556Wo<DamageType, C3892wO> m23539UC() {
        throw new aWi(new aCE(this, aQB, new Object[0]));
    }

    /* renamed from: a */
    private void m23541a(String str) {
        bFf().mo5608dq().mo3197f(f4804bM, str);
    }

    /* renamed from: a */
    private void m23542a(UUID uuid) {
        bFf().mo5608dq().mo3197f(f4803bL, uuid);
    }

    /* renamed from: an */
    private UUID m23543an() {
        return (UUID) bFf().mo5608dq().mo3214p(f4803bL);
    }

    /* renamed from: ao */
    private String m23544ao() {
        return (String) bFf().mo5608dq().mo3214p(f4804bM);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Handle")
    @C0064Am(aul = "9644d60bf7690a2670cd6ccd936b6127", aum = 0)
    @C5566aOg
    /* renamed from: b */
    private void m23547b(String str) {
        throw new aWi(new aCE(this, f4808bQ, new Object[]{str}));
    }

    /* renamed from: c */
    private void m23549c(UUID uuid) {
        switch (bFf().mo6893i(f4806bO)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f4806bO, new Object[]{uuid}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f4806bO, new Object[]{uuid}));
                break;
        }
        m23548b(uuid);
    }

    /* renamed from: T */
    public C6494any mo11T() {
        return ___iScriptClass;
    }

    /* renamed from: U */
    public C2491fm[] mo12U() {
        return _m_methods;
    }

    /* renamed from: UD */
    public C1556Wo<DamageType, C3892wO> mo8299UD() {
        switch (bFf().mo6893i(aQB)) {
            case 0:
                return null;
            case 2:
                return (C1556Wo) bFf().mo5606d(new aCE(this, aQB, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, aQB, new Object[0]));
                break;
        }
        return m23539UC();
    }

    /* renamed from: W */
    public Object mo13W() {
        return new C0106BL(this);
    }

    /* renamed from: a */
    public Object mo14a(C0495Gr gr) {
        Object[] args = gr.getArgs();
        switch (gr.mo2417hq() - C2961mJ._m_methodCount) {
            case 0:
                return m23545ap();
            case 1:
                m23548b((UUID) args[0]);
                return null;
            case 2:
                return m23546ar();
            case 3:
                m23547b((String) args[0]);
                return null;
            case 4:
                return aPD();
            case 5:
                return m23539UC();
            default:
                return super.mo14a(gr);
        }
    }

    /* renamed from: a */
    public void mo15a(Collection collection, C0495Gr gr) {
        super.mo15a(collection, gr);
    }

    public Taikodom ala() {
        switch (bFf().mo6893i(f4801xa236a942)) {
            case 0:
                return null;
            case 2:
                return (Taikodom) bFf().mo5606d(new aCE(this, f4801xa236a942, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f4801xa236a942, new Object[0]));
                break;
        }
        return aPD();
    }

    /* renamed from: aq */
    public UUID mo18aq() {
        switch (bFf().mo6893i(f4805bN)) {
            case 0:
                return null;
            case 2:
                return (UUID) bFf().mo5606d(new aCE(this, f4805bN, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f4805bN, new Object[0]));
                break;
        }
        return m23545ap();
    }

    /* renamed from: c */
    public C5663aRz[] mo25c() {
        return _m_fields;
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Handle")
    public String getHandle() {
        switch (bFf().mo6893i(f4807bP)) {
            case 0:
                return null;
            case 2:
                return (String) bFf().mo5606d(new aCE(this, f4807bP, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f4807bP, new Object[0]));
                break;
        }
        return m23546ar();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Handle")
    @C5566aOg
    public void setHandle(String str) {
        switch (bFf().mo6893i(f4808bQ)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f4808bQ, new Object[]{str}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f4808bQ, new Object[]{str}));
                break;
        }
        m23547b(str);
    }

    @C0064Am(aul = "cc8cfc182ac4414c775bec77d9218ea0", aum = 0)
    /* renamed from: ap */
    private UUID m23545ap() {
        return m23543an();
    }

    @C0064Am(aul = "ea50a7b4e29d04fee0a1d2c5093a337c", aum = 0)
    /* renamed from: b */
    private void m23548b(UUID uuid) {
        m23542a(uuid);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Handle")
    @C0064Am(aul = "8e4289c50cb9995a9c2540282d374126", aum = 0)
    /* renamed from: ar */
    private String m23546ar() {
        return m23544ao();
    }

    /* renamed from: S */
    public void mo10S() {
        super.mo10S();
        C1556Wo<DamageType, C3892wO> UD = mo8299UD();
        for (DamageType put : ala().aLs()) {
            UD.put(put, new C3892wO());
        }
        m23542a(UUID.randomUUID());
    }

    @C0064Am(aul = "9073be6ca0574c00a0e13776601f9fed", aum = 0)
    private Taikodom aPD() {
        return (Taikodom) bFf().mo6866PM().bGz().bFV();
    }
}
