package game.script.ship.strike;

import game.network.message.externalizable.aCE;
import game.script.damage.DamageType;
import game.script.util.GameObjectAdapter;
import logic.baa.C1616Xf;
import logic.baa.C1634Xv;
import logic.baa.C5511aMd;
import logic.baa.C6485anp;
import logic.data.mbean.C6821auN;
import logic.res.code.C5663aRz;
import logic.res.code.C6494any;
import logic.res.html.C2491fm;
import p001a.*;

import java.util.Collection;

@C5511aMd
@C6485anp
/* renamed from: a.aCA */
/* compiled from: a */
public class StrikeAdapter extends GameObjectAdapter<StrikeAdapter> implements C1616Xf {
    public static final int _m_fieldCount = 0;
    public static final C5663aRz[] _m_fields = null;
    public static final int _m_methodCount = 0;
    public static final C2491fm[] _m_methods = null;
    public static final C2491fm bbS = null;
    public static final C2491fm bbU = null;
    public static final C2491fm ccE = null;
    public static final long serialVersionUID = 0;
    /* renamed from: yg */
    public static final C2491fm f2456yg = null;
    public static C6494any ___iScriptClass;

    static {
        m13123V();
    }

    public StrikeAdapter() {
        super((C5540aNg) null);
        super.mo10S();
    }

    public StrikeAdapter(C5540aNg ang) {
        super(ang);
    }

    /* renamed from: V */
    static void m13123V() {
        serialVersionUID = (long) 1;
        _m_fieldCount = GameObjectAdapter._m_fieldCount + 0;
        _m_methodCount = GameObjectAdapter._m_methodCount + 4;
        _m_fields = new C5663aRz[(GameObjectAdapter._m_fieldCount + 0)];
        C1634Xv.m11725a((Object[]) GameObjectAdapter._m_fields, (Object[]) _m_fields);
        int i = GameObjectAdapter._m_methodCount;
        C2491fm[] fmVarArr = new C2491fm[(i + 4)];
        C2491fm a = C4105zY.m41624a(StrikeAdapter.class, "56a2b869c9ab1fc01ba45dccee4c31da", i);
        f2456yg = a;
        fmVarArr[i] = a;
        int i2 = i + 1;
        C2491fm a2 = C4105zY.m41624a(StrikeAdapter.class, "e5da8bb8146a1df8d0acfd026cbf9a20", i2);
        bbU = a2;
        fmVarArr[i2] = a2;
        int i3 = i2 + 1;
        C2491fm a3 = C4105zY.m41624a(StrikeAdapter.class, "d4c33056423db53d30d1e02adc871929", i3);
        bbS = a3;
        fmVarArr[i3] = a3;
        int i4 = i3 + 1;
        C2491fm a4 = C4105zY.m41624a(StrikeAdapter.class, "009172a5f5b9048d63b436669c3209f3", i4);
        ccE = a4;
        fmVarArr[i4] = a4;
        int i5 = i4 + 1;
        _m_methods = fmVarArr;
        C1634Xv.m11725a((Object[]) GameObjectAdapter._m_methods, (Object[]) _m_methods);
        ___iScriptClass = aUO.m18566a(StrikeAdapter.class, C6821auN.class, _m_fields, _m_methods);
    }

    /* renamed from: T */
    public C6494any mo11T() {
        return ___iScriptClass;
    }

    /* renamed from: U */
    public C2491fm[] mo12U() {
        return _m_methods;
    }

    /* renamed from: W */
    public Object mo13W() {
        return new C6821auN(this);
    }

    /* renamed from: YE */
    public float mo7994YE() {
        switch (bFf().mo6893i(bbS)) {
            case 0:
                return 0.0f;
            case 2:
                return ((Float) bFf().mo5606d(new aCE(this, bbS, new Object[0]))).floatValue();
            case 3:
                bFf().mo5606d(new aCE(this, bbS, new Object[0]));
                break;
        }
        return m13124YD();
    }

    /* renamed from: YG */
    public float mo7995YG() {
        switch (bFf().mo6893i(bbU)) {
            case 0:
                return 0.0f;
            case 2:
                return ((Float) bFf().mo5606d(new aCE(this, bbU, new Object[0]))).floatValue();
            case 3:
                bFf().mo5606d(new aCE(this, bbU, new Object[0]));
                break;
        }
        return m13125YF();
    }

    /* renamed from: a */
    public Object mo14a(C0495Gr gr) {
        Object[] args = gr.getArgs();
        switch (gr.mo2417hq() - GameObjectAdapter._m_methodCount) {
            case 0:
                return new Float(m13126js());
            case 1:
                return new Float(m13125YF());
            case 2:
                return new Float(m13124YD());
            case 3:
                return m13127s((DamageType) args[0]);
            default:
                return super.mo14a(gr);
        }
    }

    /* renamed from: a */
    public void mo15a(Collection collection, C0495Gr gr) {
        super.mo15a(collection, gr);
    }

    /* renamed from: c */
    public C5663aRz[] mo25c() {
        return _m_fields;
    }

    /* renamed from: jt */
    public float mo7996jt() {
        switch (bFf().mo6893i(f2456yg)) {
            case 0:
                return 0.0f;
            case 2:
                return ((Float) bFf().mo5606d(new aCE(this, f2456yg, new Object[0]))).floatValue();
            case 3:
                bFf().mo5606d(new aCE(this, f2456yg, new Object[0]));
                break;
        }
        return m13126js();
    }

    /* renamed from: t */
    public C3892wO mo7997t(DamageType fr) {
        switch (bFf().mo6893i(ccE)) {
            case 0:
                return null;
            case 2:
                return (C3892wO) bFf().mo5606d(new aCE(this, ccE, new Object[]{fr}));
            case 3:
                bFf().mo5606d(new aCE(this, ccE, new Object[]{fr}));
                break;
        }
        return m13127s(fr);
    }

    /* renamed from: S */
    public void mo10S() {
        super.mo10S();
    }

    @C0064Am(aul = "56a2b869c9ab1fc01ba45dccee4c31da", aum = 0)
    /* renamed from: js */
    private float m13126js() {
        return ((StrikeAdapter) mo16071IG()).mo7996jt();
    }

    @C0064Am(aul = "e5da8bb8146a1df8d0acfd026cbf9a20", aum = 0)
    /* renamed from: YF */
    private float m13125YF() {
        return ((StrikeAdapter) mo16071IG()).mo7995YG();
    }

    @C0064Am(aul = "d4c33056423db53d30d1e02adc871929", aum = 0)
    /* renamed from: YD */
    private float m13124YD() {
        return ((StrikeAdapter) mo16071IG()).mo7994YE();
    }

    @C0064Am(aul = "009172a5f5b9048d63b436669c3209f3", aum = 0)
    /* renamed from: s */
    private C3892wO m13127s(DamageType fr) {
        return ((StrikeAdapter) mo16071IG()).mo7997t(fr);
    }
}
