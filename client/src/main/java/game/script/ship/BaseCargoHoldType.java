package game.script.ship;

import game.network.message.externalizable.aCE;
import game.script.template.BaseTaikodomContent;
import logic.baa.*;
import logic.data.mbean.C2789kA;
import logic.res.code.C5663aRz;
import logic.res.code.C6494any;
import logic.res.html.C2491fm;
import p001a.*;

import java.util.Collection;

@C5511aMd
@C6485anp
/* renamed from: a.pW */
/* compiled from: a */
public abstract class BaseCargoHoldType extends BaseTaikodomContent implements C1616Xf, aOW {

    /* renamed from: Do */
    public static final C2491fm f8828Do = null;
    public static final int _m_fieldCount = 0;
    public static final C5663aRz[] _m_fields = null;
    public static final int _m_methodCount = 0;
    public static final C2491fm[] _m_methods = null;
    public static final long serialVersionUID = 0;
    public static C6494any ___iScriptClass;

    static {
        m37157V();
    }

    public BaseCargoHoldType() {
        super((C5540aNg) null);
        super.mo10S();
    }

    public BaseCargoHoldType(C5540aNg ang) {
        super(ang);
    }

    /* renamed from: V */
    static void m37157V() {
        serialVersionUID = (long) 1;
        _m_fieldCount = BaseTaikodomContent._m_fieldCount + 0;
        _m_methodCount = BaseTaikodomContent._m_methodCount + 1;
        _m_fields = new C5663aRz[(BaseTaikodomContent._m_fieldCount + 0)];
        C1634Xv.m11725a((Object[]) BaseTaikodomContent._m_fields, (Object[]) _m_fields);
        int i = BaseTaikodomContent._m_methodCount;
        C2491fm[] fmVarArr = new C2491fm[(i + 1)];
        C2491fm a = C4105zY.m41624a(BaseCargoHoldType.class, "f7245067e454555e7ef3fd65acd1c6e0", i);
        f8828Do = a;
        fmVarArr[i] = a;
        int i2 = i + 1;
        _m_methods = fmVarArr;
        C1634Xv.m11725a((Object[]) BaseTaikodomContent._m_methods, (Object[]) _m_methods);
        ___iScriptClass = aUO.m18566a(BaseCargoHoldType.class, C2789kA.class, _m_fields, _m_methods);
    }

    /* renamed from: T */
    public C6494any mo11T() {
        return ___iScriptClass;
    }

    /* renamed from: U */
    public C2491fm[] mo12U() {
        return _m_methods;
    }

    /* renamed from: W */
    public Object mo13W() {
        return new C2789kA(this);
    }

    /* renamed from: a */
    public Object mo14a(C0495Gr gr) {
        Object[] args = gr.getArgs();
        switch (gr.mo2417hq() - BaseTaikodomContent._m_methodCount) {
            case 0:
                m37158a((C0665JT) args[0]);
                return null;
            default:
                return super.mo14a(gr);
        }
    }

    /* renamed from: a */
    public void mo15a(Collection collection, C0495Gr gr) {
        super.mo15a(collection, gr);
    }

    /* renamed from: b */
    public void mo24b(C0665JT jt) {
        switch (bFf().mo6893i(f8828Do)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f8828Do, new Object[]{jt}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f8828Do, new Object[]{jt}));
                break;
        }
        m37158a(jt);
    }

    /* renamed from: c */
    public C5663aRz[] mo25c() {
        return _m_fields;
    }

    /* renamed from: S */
    public void mo10S() {
        super.mo10S();
    }

    @C0064Am(aul = "f7245067e454555e7ef3fd65acd1c6e0", aum = 0)
    /* renamed from: a */
    private void m37158a(C0665JT jt) {
        jt.mo3117j(2, 0, 0);
    }
}
