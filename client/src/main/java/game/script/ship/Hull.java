package game.script.ship;

import game.network.message.externalizable.C2814kV;
import game.network.message.externalizable.C6556apI;
import game.network.message.externalizable.aCE;
import game.network.message.serializable.C1556Wo;
import game.network.message.serializable.C2686iZ;
import game.script.Actor;
import game.script.TaikodomObject;
import game.script.damage.DamageType;
import game.script.player.Player;
import game.script.util.AdapterLikedList;
import game.script.util.GameObjectAdapter;
import logic.aaa.C1506WA;
import logic.baa.*;
import logic.data.link.C2530gR;
import logic.data.mbean.C0184CJ;
import logic.data.mbean.C5776aaI;
import logic.res.code.C5640aRc;
import logic.res.code.C5663aRz;
import logic.res.code.C6494any;
import logic.res.html.C2491fm;
import p001a.*;

import java.util.Collection;
import java.util.Iterator;

@C5829abJ("2.1.0")
@C6485anp
@C2712iu(mo19786Bt = BaseHullType.class, version = "2.1.0")
@C5511aMd
/* renamed from: a.jR */
/* compiled from: a */
public class Hull extends TaikodomObject implements C1616Xf, aOW, C6480ank, C6872avM {

    /* renamed from: Do */
    public static final C2491fm f8303Do = null;

    /* renamed from: Lm */
    public static final C2491fm f8304Lm = null;
    /* renamed from: _f_getShip_0020_0028_0029Ltaikodom_002fgame_002fscript_002fship_002fShip_003b */
    public static final C2491fm f8305x851d656b = null;
    public static final C2491fm _f_onResurrect_0020_0028_0029V = null;
    public static final int _m_fieldCount = 0;
    public static final C5663aRz[] _m_fields = null;
    public static final int _m_methodCount = 0;
    public static final C2491fm[] _m_methods = null;
    public static final C5663aRz aOB = null;
    public static final C5663aRz aOH = null;
    public static final C5663aRz aOJ = null;
    public static final C5663aRz aOP = null;
    public static final C2491fm aOR = null;
    public static final C2491fm aOU = null;
    public static final C2491fm aOV = null;
    public static final C2491fm aOX = null;
    public static final C5663aRz aOp = null;
    public static final C5663aRz aOv = null;
    public static final C5663aRz aOx = null;
    public static final C2491fm aPc = null;
    public static final C2491fm aPe = null;
    public static final C2491fm aPf = null;
    public static final C2491fm aPg = null;
    public static final C2491fm aPh = null;
    public static final C2491fm aPi = null;
    public static final C2491fm aPj = null;
    public static final C2491fm aPk = null;
    public static final C2491fm aPl = null;
    public static final C2491fm aPm = null;
    public static final C5663aRz bEC = null;
    public static final C2491fm clc = null;
    public static final C2491fm cld = null;
    public static final C2491fm cle = null;
    public static final C2491fm clf = null;
    public static final C2491fm clg = null;
    /* renamed from: qa */
    public static final C2491fm f8306qa = null;
    /* renamed from: qb */
    public static final C2491fm f8307qb = null;
    public static final long serialVersionUID = 0;
    /* renamed from: uI */
    public static final C2491fm f8308uI = null;
    public static C6494any ___iScriptClass;
    @C0064Am(aul = "69fee834371829196a17321b92abe328", aum = 4)
    private static float aOA;
    @C0064Am(aul = "674c314385d57bed677ef90b3d1719eb", aum = 5)
    private static C1556Wo<DamageType, Float> aOG;
    @C0064Am(aul = "d36a5e54dcc50c1b2e3ae4dcaa8b4587", aum = 6)
    @C1020Ot(mo4553Ur = 5000, drz = C1020Ot.C1021a.TIME_BASED)
    @C5256aCi
    private static float aOI;
    @C0064Am(aul = "b9294ace7cc3d0badeaee4070f91a436", aum = 7)
    private static Ship aOO;
    @C0064Am(aul = "a9cc0e2cbdc56a1b581816ca22a675d2", aum = 2)
    private static float aOo;
    @C0064Am(aul = "58244a993453be22a790683bf5431ac3", aum = 0)
    private static C1556Wo<DamageType, C1649YI> aOu;
    @C0064Am(aul = "5b1be121c46f0a0e1b56d0ee66a068d5", aum = 3)
    @C5566aOg
    private static AdapterLikedList<HullAdapter> aOw;
    @C0064Am(aul = "e97448476ae488e5379338afff0a2839", aum = 1)
    private static C2686iZ<DamageType> bEB;

    static {
        m33912V();
    }

    public Hull() {
        super((C5540aNg) null);
        super.mo10S();
    }

    public Hull(C5540aNg ang) {
        super(ang);
    }

    public Hull(HullType wHVar) {
        super((C5540aNg) null);
        super._m_script_init(wHVar);
    }

    /* renamed from: V */
    static void m33912V() {
        serialVersionUID = (long) 1;
        _m_fieldCount = TaikodomObject._m_fieldCount + 8;
        _m_methodCount = TaikodomObject._m_methodCount + 26;
        int i = TaikodomObject._m_fieldCount;
        C5663aRz[] arzArr = new C5663aRz[(i + 8)];
        C5663aRz b = C5640aRc.m17844b(Hull.class, "58244a993453be22a790683bf5431ac3", i);
        aOv = b;
        arzArr[i] = b;
        int i2 = i + 1;
        C5663aRz b2 = C5640aRc.m17844b(Hull.class, "e97448476ae488e5379338afff0a2839", i2);
        bEC = b2;
        arzArr[i2] = b2;
        int i3 = i2 + 1;
        C5663aRz b3 = C5640aRc.m17844b(Hull.class, "a9cc0e2cbdc56a1b581816ca22a675d2", i3);
        aOp = b3;
        arzArr[i3] = b3;
        int i4 = i3 + 1;
        C5663aRz b4 = C5640aRc.m17844b(Hull.class, "5b1be121c46f0a0e1b56d0ee66a068d5", i4);
        aOx = b4;
        arzArr[i4] = b4;
        int i5 = i4 + 1;
        C5663aRz b5 = C5640aRc.m17844b(Hull.class, "69fee834371829196a17321b92abe328", i5);
        aOB = b5;
        arzArr[i5] = b5;
        int i6 = i5 + 1;
        C5663aRz b6 = C5640aRc.m17844b(Hull.class, "674c314385d57bed677ef90b3d1719eb", i6);
        aOH = b6;
        arzArr[i6] = b6;
        int i7 = i6 + 1;
        C5663aRz b7 = C5640aRc.m17844b(Hull.class, "d36a5e54dcc50c1b2e3ae4dcaa8b4587", i7);
        aOJ = b7;
        arzArr[i7] = b7;
        int i8 = i7 + 1;
        C5663aRz b8 = C5640aRc.m17844b(Hull.class, "b9294ace7cc3d0badeaee4070f91a436", i8);
        aOP = b8;
        arzArr[i8] = b8;
        int i9 = i8 + 1;
        _m_fields = arzArr;
        C1634Xv.m11725a((Object[]) TaikodomObject._m_fields, (Object[]) _m_fields);
        int i10 = TaikodomObject._m_methodCount;
        C2491fm[] fmVarArr = new C2491fm[(i10 + 26)];
        C2491fm a = C4105zY.m41624a(Hull.class, "c0a951e2805f8bc7ab9c7f6d8312e748", i10);
        aOV = a;
        fmVarArr[i10] = a;
        int i11 = i10 + 1;
        C2491fm a2 = C4105zY.m41624a(Hull.class, "23eba5388f1054f9f3185908f9526889", i11);
        aOX = a2;
        fmVarArr[i11] = a2;
        int i12 = i11 + 1;
        C2491fm a3 = C4105zY.m41624a(Hull.class, "48be41fbb87a1435e791a81edd3b6aaf", i12);
        f8306qa = a3;
        fmVarArr[i12] = a3;
        int i13 = i12 + 1;
        C2491fm a4 = C4105zY.m41624a(Hull.class, "7fab1261d522b5a4329ebe1cd0b6df02", i13);
        f8308uI = a4;
        fmVarArr[i13] = a4;
        int i14 = i13 + 1;
        C2491fm a5 = C4105zY.m41624a(Hull.class, "bff97b47f8b0aeafa1d8468ae31ae052", i14);
        f8307qb = a5;
        fmVarArr[i14] = a5;
        int i15 = i14 + 1;
        C2491fm a6 = C4105zY.m41624a(Hull.class, "39cc5a628caa105f93d707a6264cefb5", i15);
        aOU = a6;
        fmVarArr[i15] = a6;
        int i16 = i15 + 1;
        C2491fm a7 = C4105zY.m41624a(Hull.class, "28f14e57e64a260ee16f94e626bce549", i16);
        aOR = a7;
        fmVarArr[i16] = a7;
        int i17 = i16 + 1;
        C2491fm a8 = C4105zY.m41624a(Hull.class, "b391f599292a4dfc0c079f87acd76f9a", i17);
        clc = a8;
        fmVarArr[i17] = a8;
        int i18 = i17 + 1;
        C2491fm a9 = C4105zY.m41624a(Hull.class, "0d236ee223e51fd730422bc205a85663", i18);
        cld = a9;
        fmVarArr[i18] = a9;
        int i19 = i18 + 1;
        C2491fm a10 = C4105zY.m41624a(Hull.class, "658d93d5322b63e94d99f6c0113e0854", i19);
        aPc = a10;
        fmVarArr[i19] = a10;
        int i20 = i19 + 1;
        C2491fm a11 = C4105zY.m41624a(Hull.class, "b30ba72f9c1e4e4fa968eba14a7086ff", i20);
        cle = a11;
        fmVarArr[i20] = a11;
        int i21 = i20 + 1;
        C2491fm a12 = C4105zY.m41624a(Hull.class, "904bae872fd1efae3111e754f99a41ce", i21);
        f8304Lm = a12;
        fmVarArr[i21] = a12;
        int i22 = i21 + 1;
        C2491fm a13 = C4105zY.m41624a(Hull.class, "1ef6fc2db35f432bac1864bb38b29c3e", i22);
        aPe = a13;
        fmVarArr[i22] = a13;
        int i23 = i22 + 1;
        C2491fm a14 = C4105zY.m41624a(Hull.class, "b00fd3a391a9e79bf2e083ea68aadd4c", i23);
        aPf = a14;
        fmVarArr[i23] = a14;
        int i24 = i23 + 1;
        C2491fm a15 = C4105zY.m41624a(Hull.class, "ed21a02473a397ffdc373944023dc6cb", i24);
        _f_onResurrect_0020_0028_0029V = a15;
        fmVarArr[i24] = a15;
        int i25 = i24 + 1;
        C2491fm a16 = C4105zY.m41624a(Hull.class, "8f5ecbb34955d59623af823d9722e66e", i25);
        clf = a16;
        fmVarArr[i25] = a16;
        int i26 = i25 + 1;
        C2491fm a17 = C4105zY.m41624a(Hull.class, "43280f9f5643cc3afedd33f8a51eb8fc", i26);
        f8303Do = a17;
        fmVarArr[i26] = a17;
        int i27 = i26 + 1;
        C2491fm a18 = C4105zY.m41624a(Hull.class, "4aafadfdf7bcc9a31b0216daf324f61e", i27);
        aPg = a18;
        fmVarArr[i27] = a18;
        int i28 = i27 + 1;
        C2491fm a19 = C4105zY.m41624a(Hull.class, "7e4731a8c80e067b9fe4e7600a0bcd6c", i28);
        aPh = a19;
        fmVarArr[i28] = a19;
        int i29 = i28 + 1;
        C2491fm a20 = C4105zY.m41624a(Hull.class, "97e74a43630de9aff4d952d26f4d53e9", i29);
        clg = a20;
        fmVarArr[i29] = a20;
        int i30 = i29 + 1;
        C2491fm a21 = C4105zY.m41624a(Hull.class, "08e0596735a3db11d20360151d885d69", i30);
        aPi = a21;
        fmVarArr[i30] = a21;
        int i31 = i30 + 1;
        C2491fm a22 = C4105zY.m41624a(Hull.class, "8405bd9b186160ce1471565a4bb8a771", i31);
        f8305x851d656b = a22;
        fmVarArr[i31] = a22;
        int i32 = i31 + 1;
        C2491fm a23 = C4105zY.m41624a(Hull.class, "d6e5461d340898ecd81413ee678f0094", i32);
        aPj = a23;
        fmVarArr[i32] = a23;
        int i33 = i32 + 1;
        C2491fm a24 = C4105zY.m41624a(Hull.class, "963720e7242990c5230412f794ab67f7", i33);
        aPk = a24;
        fmVarArr[i33] = a24;
        int i34 = i33 + 1;
        C2491fm a25 = C4105zY.m41624a(Hull.class, "fe982c5349f12946a844c7998d695d0d", i34);
        aPl = a25;
        fmVarArr[i34] = a25;
        int i35 = i34 + 1;
        C2491fm a26 = C4105zY.m41624a(Hull.class, "365ab8103350fd5739faeb04d125a0d1", i35);
        aPm = a26;
        fmVarArr[i35] = a26;
        int i36 = i35 + 1;
        _m_methods = fmVarArr;
        C1634Xv.m11725a((Object[]) TaikodomObject._m_methods, (Object[]) _m_methods);
        ___iScriptClass = aUO.m18566a(Hull.class, C5776aaI.class, _m_fields, _m_methods);
    }

    @C0064Am(aul = "1ef6fc2db35f432bac1864bb38b29c3e", aum = 0)
    @C5566aOg
    /* renamed from: TG */
    private void m33895TG() {
        throw new aWi(new aCE(this, aPe, new Object[0]));
    }

    @C5566aOg
    /* renamed from: TH */
    private void m33896TH() {
        switch (bFf().mo6893i(aPe)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, aPe, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, aPe, new Object[0]));
                break;
        }
        m33895TG();
    }

    /* renamed from: TP */
    private void m33901TP() {
        switch (bFf().mo6893i(aPl)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, aPl, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, aPl, new Object[0]));
                break;
        }
        m33900TO();
    }

    /* renamed from: Ta */
    private float m33903Ta() {
        return ((HullType) getType()).mo22727hh();
    }

    /* renamed from: Td */
    private C1556Wo m33904Td() {
        return ((HullType) getType()).ana();
    }

    /* renamed from: Te */
    private AdapterLikedList m33905Te() {
        return (AdapterLikedList) bFf().mo5608dq().mo3214p(aOx);
    }

    /* renamed from: Tg */
    private float m33906Tg() {
        return bFf().mo5608dq().mo3211m(aOB);
    }

    /* renamed from: Tj */
    private C1556Wo m33907Tj() {
        return (C1556Wo) bFf().mo5608dq().mo3214p(aOH);
    }

    /* renamed from: Tk */
    private float m33908Tk() {
        return bFf().mo5608dq().mo3211m(aOJ);
    }

    /* renamed from: Tn */
    private Ship m33909Tn() {
        return (Ship) bFf().mo5608dq().mo3214p(aOP);
    }

    @C0064Am(aul = "b30ba72f9c1e4e4fa968eba14a7086ff", aum = 0)
    @C6580apg
    /* renamed from: a */
    private void m33918a(C5260aCm acm, Actor cr) {
        bFf().mo5600a((Class<? extends C4062yl>) C2530gR.C2531a.class, (C0495Gr) new aCE(this, cle, new Object[]{acm, cr}));
    }

    @C0064Am(aul = "0d236ee223e51fd730422bc205a85663", aum = 0)
    @C5566aOg
    /* renamed from: a */
    private void m33919a(C5260aCm acm, DamageType fr, float f, Actor cr) {
        throw new aWi(new aCE(this, cld, new Object[]{acm, fr, new Float(f), cr}));
    }

    /* renamed from: a */
    private void m33920a(AdapterLikedList zAVar) {
        bFf().mo5608dq().mo3197f(aOx, zAVar);
    }

    private C2686iZ amY() {
        return ((HullType) getType()).anc();
    }

    /* renamed from: b */
    private void m33922b(C5260aCm acm, float f, Actor cr) {
        switch (bFf().mo6893i(clc)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, clc, new Object[]{acm, new Float(f), cr}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, clc, new Object[]{acm, new Float(f), cr}));
                break;
        }
        m33917a(acm, f, cr);
    }

    @C6580apg
    /* renamed from: b */
    private void m33923b(C5260aCm acm, Actor cr) {
        switch (bFf().mo6893i(cle)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, cle, new Object[]{acm, cr}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, cle, new Object[]{acm, cr}));
                break;
        }
        m33918a(acm, cr);
    }

    @C0064Am(aul = "658d93d5322b63e94d99f6c0113e0854", aum = 0)
    @C5566aOg
    /* renamed from: c */
    private float m33924c(DamageType fr, float f) {
        throw new aWi(new aCE(this, aPc, new Object[]{fr, new Float(f)}));
    }

    /* renamed from: cC */
    private void m33925cC(float f) {
        throw new C6039afL();
    }

    /* renamed from: cF */
    private void m33926cF(float f) {
        bFf().mo5608dq().mo3150a(aOB, f);
    }

    /* renamed from: cI */
    private void m33927cI(float f) {
        bFf().mo5608dq().mo3150a(aOJ, f);
    }

    /* renamed from: cK */
    private void m33929cK(float f) {
        switch (bFf().mo6893i(aOX)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, aOX, new Object[]{new Float(f)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, aOX, new Object[]{new Float(f)}));
                break;
        }
        m33928cJ(f);
    }

    /* renamed from: e */
    private void m33931e(Collection collection, C0495Gr gr) {
        Iterator it = collection.iterator();
        while (it.hasNext()) {
            Object[] args = gr.getArgs();
            ((C2530gR.C2531a) it.next()).mo8536a(this, (C5260aCm) args[0], (Actor) args[1]);
        }
    }

    /* renamed from: k */
    private void m33934k(Ship fAVar) {
        bFf().mo5608dq().mo3197f(aOP, fAVar);
    }

    @C0064Am(aul = "08e0596735a3db11d20360151d885d69", aum = 0)
    @C5566aOg
    /* renamed from: l */
    private void m33935l(Ship fAVar) {
        throw new aWi(new aCE(this, aPi, new Object[]{fAVar}));
    }

    /* renamed from: m */
    private void m33936m(C1556Wo wo) {
        throw new C6039afL();
    }

    /* renamed from: n */
    private void m33937n(C1556Wo wo) {
        bFf().mo5608dq().mo3197f(aOH, wo);
    }

    /* renamed from: n */
    private void m33938n(C2686iZ iZVar) {
        throw new C6039afL();
    }

    @C0064Am(aul = "904bae872fd1efae3111e754f99a41ce", aum = 0)
    @C5566aOg
    @C2499fr
    /* renamed from: qU */
    private void m33939qU() {
        throw new aWi(new aCE(this, f8304Lm, new Object[0]));
    }

    /* renamed from: S */
    public void mo10S() {
        super.mo10S();
    }

    /* renamed from: T */
    public C6494any mo11T() {
        return ___iScriptClass;
    }

    /* renamed from: TJ */
    public AdapterLikedList<HullAdapter> mo5353TJ() {
        switch (bFf().mo6893i(aPf)) {
            case 0:
                return null;
            case 2:
                return (AdapterLikedList) bFf().mo5606d(new aCE(this, aPf, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, aPf, new Object[0]));
                break;
        }
        return m33897TI();
    }

    /* renamed from: TL */
    public void mo12905TL() {
        switch (bFf().mo6893i(aPj)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, aPj, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, aPj, new Object[0]));
                break;
        }
        m33898TK();
    }

    /* renamed from: TN */
    public void mo12906TN() {
        switch (bFf().mo6893i(aPk)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, aPk, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, aPk, new Object[0]));
                break;
        }
        m33899TM();
    }

    /* renamed from: TR */
    public void mo19921TR() {
        switch (bFf().mo6893i(aPm)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, aPm, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, aPm, new Object[0]));
                break;
        }
        m33902TQ();
    }

    /* renamed from: Tv */
    public float mo19922Tv() {
        switch (bFf().mo6893i(aOU)) {
            case 0:
                return 0.0f;
            case 2:
                return ((Float) bFf().mo5606d(new aCE(this, aOU, new Object[0]))).floatValue();
            case 3:
                bFf().mo5606d(new aCE(this, aOU, new Object[0]));
                break;
        }
        return m33910Tu();
    }

    /* renamed from: Tx */
    public float mo19923Tx() {
        switch (bFf().mo6893i(aOV)) {
            case 0:
                return 0.0f;
            case 2:
                return ((Float) bFf().mo5606d(new aCE(this, aOV, new Object[0]))).floatValue();
            case 3:
                bFf().mo5606d(new aCE(this, aOV, new Object[0]));
                break;
        }
        return m33911Tw();
    }

    /* renamed from: U */
    public C2491fm[] mo12U() {
        return _m_methods;
    }

    /* renamed from: W */
    public Object mo13W() {
        return new C5776aaI(this);
    }

    /* renamed from: a */
    public Object mo14a(C0495Gr gr) {
        Object[] args = gr.getArgs();
        switch (gr.mo2417hq() - TaikodomObject._m_methodCount) {
            case 0:
                return new Float(m33911Tw());
            case 1:
                m33928cJ(((Float) args[0]).floatValue());
                return null;
            case 2:
                return new Float(m33914a((DamageType) args[0], ((Float) args[1]).floatValue()));
            case 3:
                return new Float(m33913a((DamageType) args[0]));
            case 4:
                return new Float(m33933hg());
            case 5:
                return new Float(m33910Tu());
            case 6:
                m33915a(((Float) args[0]).floatValue(), (Actor) args[1]);
                return null;
            case 7:
                m33917a((C5260aCm) args[0], ((Float) args[1]).floatValue(), (Actor) args[2]);
                return null;
            case 8:
                m33919a((C5260aCm) args[0], (DamageType) args[1], ((Float) args[2]).floatValue(), (Actor) args[3]);
                return null;
            case 9:
                return new Float(m33924c((DamageType) args[0], ((Float) args[1]).floatValue()));
            case 10:
                m33918a((C5260aCm) args[0], (Actor) args[1]);
                return null;
            case 11:
                m33939qU();
                return null;
            case 12:
                m33895TG();
                return null;
            case 13:
                return m33897TI();
            case 14:
                m33921aG();
                return null;
            case 15:
                return ayo();
            case 16:
                m33916a((C0665JT) args[0]);
                return null;
            case 17:
                return new Float(m33930e((DamageType) args[0], ((Float) args[1]).floatValue()));
            case 18:
                return new Float(m33932g((DamageType) args[0]));
            case 19:
                return new Boolean(m33940u((DamageType) args[0]));
            case 20:
                m33935l((Ship) args[0]);
                return null;
            case 21:
                return m33894Mo();
            case 22:
                m33898TK();
                return null;
            case 23:
                m33899TM();
                return null;
            case 24:
                m33900TO();
                return null;
            case 25:
                m33902TQ();
                return null;
            default:
                return super.mo14a(gr);
        }
    }

    /* renamed from: a */
    public void mo15a(Collection collection, C0495Gr gr) {
        switch (gr.mo2417hq() - TaikodomObject._m_methodCount) {
            case 10:
                m33931e(collection, gr);
                return;
            default:
                super.mo15a(collection, gr);
                return;
        }
    }

    /* renamed from: aH */
    public void mo70aH() {
        switch (bFf().mo6893i(_f_onResurrect_0020_0028_0029V)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, _f_onResurrect_0020_0028_0029V, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, _f_onResurrect_0020_0028_0029V, new Object[0]));
                break;
        }
        m33921aG();
    }

    /* renamed from: al */
    public Ship mo19925al() {
        switch (bFf().mo6893i(f8305x851d656b)) {
            case 0:
                return null;
            case 2:
                return (Ship) bFf().mo5606d(new aCE(this, f8305x851d656b, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f8305x851d656b, new Object[0]));
                break;
        }
        return m33894Mo();
    }

    public HullType ayp() {
        switch (bFf().mo6893i(clf)) {
            case 0:
                return null;
            case 2:
                return (HullType) bFf().mo5606d(new aCE(this, clf, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, clf, new Object[0]));
                break;
        }
        return ayo();
    }

    /* renamed from: b */
    public float mo19927b(DamageType fr) {
        switch (bFf().mo6893i(f8308uI)) {
            case 0:
                return 0.0f;
            case 2:
                return ((Float) bFf().mo5606d(new aCE(this, f8308uI, new Object[]{fr}))).floatValue();
            case 3:
                bFf().mo5606d(new aCE(this, f8308uI, new Object[]{fr}));
                break;
        }
        return m33913a(fr);
    }

    /* renamed from: b */
    public float mo19928b(DamageType fr, float f) {
        switch (bFf().mo6893i(f8306qa)) {
            case 0:
                return 0.0f;
            case 2:
                return ((Float) bFf().mo5606d(new aCE(this, f8306qa, new Object[]{fr, new Float(f)}))).floatValue();
            case 3:
                bFf().mo5606d(new aCE(this, f8306qa, new Object[]{fr, new Float(f)}));
                break;
        }
        return m33914a(fr, f);
    }

    /* renamed from: b */
    public void mo19929b(float f, Actor cr) {
        switch (bFf().mo6893i(aOR)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, aOR, new Object[]{new Float(f), cr}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, aOR, new Object[]{new Float(f), cr}));
                break;
        }
        m33915a(f, cr);
    }

    /* renamed from: b */
    public void mo24b(C0665JT jt) {
        switch (bFf().mo6893i(f8303Do)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f8303Do, new Object[]{jt}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f8303Do, new Object[]{jt}));
                break;
        }
        m33916a(jt);
    }

    @C5566aOg
    /* renamed from: b */
    public void mo19930b(C5260aCm acm, DamageType fr, float f, Actor cr) {
        switch (bFf().mo6893i(cld)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, cld, new Object[]{acm, fr, new Float(f), cr}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, cld, new Object[]{acm, fr, new Float(f), cr}));
                break;
        }
        m33919a(acm, fr, f, cr);
    }

    /* renamed from: c */
    public C5663aRz[] mo25c() {
        return _m_fields;
    }

    @C5566aOg
    /* renamed from: d */
    public float mo19931d(DamageType fr, float f) {
        switch (bFf().mo6893i(aPc)) {
            case 0:
                return 0.0f;
            case 2:
                return ((Float) bFf().mo5606d(new aCE(this, aPc, new Object[]{fr, new Float(f)}))).floatValue();
            case 3:
                bFf().mo5606d(new aCE(this, aPc, new Object[]{fr, new Float(f)}));
                break;
        }
        return m33924c(fr, f);
    }

    /* renamed from: f */
    public float mo19932f(DamageType fr, float f) {
        switch (bFf().mo6893i(aPg)) {
            case 0:
                return 0.0f;
            case 2:
                return ((Float) bFf().mo5606d(new aCE(this, aPg, new Object[]{fr, new Float(f)}))).floatValue();
            case 3:
                bFf().mo5606d(new aCE(this, aPg, new Object[]{fr, new Float(f)}));
                break;
        }
        return m33930e(fr, f);
    }

    /* renamed from: h */
    public float mo19933h(DamageType fr) {
        switch (bFf().mo6893i(aPh)) {
            case 0:
                return 0.0f;
            case 2:
                return ((Float) bFf().mo5606d(new aCE(this, aPh, new Object[]{fr}))).floatValue();
            case 3:
                bFf().mo5606d(new aCE(this, aPh, new Object[]{fr}));
                break;
        }
        return m33932g(fr);
    }

    /* renamed from: hh */
    public float mo19934hh() {
        switch (bFf().mo6893i(f8307qb)) {
            case 0:
                return 0.0f;
            case 2:
                return ((Float) bFf().mo5606d(new aCE(this, f8307qb, new Object[0]))).floatValue();
            case 3:
                bFf().mo5606d(new aCE(this, f8307qb, new Object[0]));
                break;
        }
        return m33933hg();
    }

    @C5566aOg
    /* renamed from: m */
    public void mo19935m(Ship fAVar) {
        switch (bFf().mo6893i(aPi)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, aPi, new Object[]{fAVar}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, aPi, new Object[]{fAVar}));
                break;
        }
        m33935l(fAVar);
    }

    @C5566aOg
    @C2499fr
    /* renamed from: qV */
    public void mo19936qV() {
        switch (bFf().mo6893i(f8304Lm)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f8304Lm, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f8304Lm, new Object[0]));
                break;
        }
        m33939qU();
    }

    /* renamed from: v */
    public boolean mo19937v(DamageType fr) {
        switch (bFf().mo6893i(clg)) {
            case 0:
                return false;
            case 2:
                return ((Boolean) bFf().mo5606d(new aCE(this, clg, new Object[]{fr}))).booleanValue();
            case 3:
                bFf().mo5606d(new aCE(this, clg, new Object[]{fr}));
                break;
        }
        return m33940u(fr);
    }

    /* renamed from: a */
    public void mo19924a(HullType wHVar) {
        super.mo967a((C2961mJ) wHVar);
        m33927cI(m33903Ta());
        m33896TH();
    }

    @C0064Am(aul = "c0a951e2805f8bc7ab9c7f6d8312e748", aum = 0)
    /* renamed from: Tw */
    private float m33911Tw() {
        return m33908Tk();
    }

    @C0064Am(aul = "23eba5388f1054f9f3185908f9526889", aum = 0)
    /* renamed from: cJ */
    private void m33928cJ(float f) {
        if (f > mo19934hh()) {
            m33927cI(mo19934hh());
            return;
        }
        if (f <= 0.0f) {
            f = 0.0f;
        }
        m33927cI(f);
    }

    @C0064Am(aul = "48be41fbb87a1435e791a81edd3b6aaf", aum = 0)
    /* renamed from: a */
    private float m33914a(DamageType fr, float f) {
        return ((HullAdapter) m33905Te().ase()).mo9877b(fr, f);
    }

    @C0064Am(aul = "7fab1261d522b5a4329ebe1cd0b6df02", aum = 0)
    /* renamed from: a */
    private float m33913a(DamageType fr) {
        return ((Float) m33907Tj().get(fr)).floatValue();
    }

    @C0064Am(aul = "bff97b47f8b0aeafa1d8468ae31ae052", aum = 0)
    /* renamed from: hg */
    private float m33933hg() {
        return m33906Tg();
    }

    @C0064Am(aul = "39cc5a628caa105f93d707a6264cefb5", aum = 0)
    /* renamed from: Tu */
    private float m33910Tu() {
        return m33903Ta();
    }

    @C0064Am(aul = "28f14e57e64a260ee16f94e626bce549", aum = 0)
    /* renamed from: a */
    private void m33915a(float f, Actor cr) {
        m33929cK(mo19923Tx() + f);
        if (m33909Tn() != null && (m33909Tn().agj() instanceof Player)) {
            ((Player) m33909Tn().agj()).mo14419f((C1506WA) new C6556apI(f, cr));
        }
        if (((double) mo19923Tx()) <= 1.0E-5d) {
            m33923b((C5260aCm) null, cr);
        }
    }

    @C0064Am(aul = "b391f599292a4dfc0c079f87acd76f9a", aum = 0)
    /* renamed from: a */
    private void m33917a(C5260aCm acm, float f, Actor cr) {
        if (f < 0.0f) {
            mo8358lY("Hull damage points must be positive (dmg " + f + ")");
            return;
        }
        if (f >= mo19923Tx()) {
            m33929cK(0.0f);
            m33923b(acm, cr);
        } else {
            m33929cK(mo19923Tx() - f);
        }
        if (m33909Tn() != null && (m33909Tn().agj() instanceof Player)) {
            ((Player) m33909Tn().agj()).mo14419f((C1506WA) new C2814kV(f, C2814kV.C2815a.HULL, cr));
        }
    }

    @C0064Am(aul = "b00fd3a391a9e79bf2e083ea68aadd4c", aum = 0)
    /* renamed from: TI */
    private AdapterLikedList<HullAdapter> m33897TI() {
        if (m33905Te() == null && bHa()) {
            m33896TH();
        }
        return m33905Te();
    }

    @C0064Am(aul = "ed21a02473a397ffdc373944023dc6cb", aum = 0)
    /* renamed from: aG */
    private void m33921aG() {
        super.mo70aH();
        m33896TH();
    }

    @C0064Am(aul = "8f5ecbb34955d59623af823d9722e66e", aum = 0)
    private HullType ayo() {
        return (HullType) getType();
    }

    @C0064Am(aul = "43280f9f5643cc3afedd33f8a51eb8fc", aum = 0)
    /* renamed from: a */
    private void m33916a(C0665JT jt) {
        if (jt.mo3117j(2, 0, 0)) {
            try {
                Object obj = jt.get("healthPoints");
                if (obj instanceof Integer) {
                    m33927cI(((Integer) obj).floatValue());
                } else if ((obj instanceof Float) && Math.abs(((double) ((Float) obj).floatValue()) - 1.0E-6d) < 1.0d) {
                    if (jt.mo3113eE("type") != null) {
                        m33927cI(((Float) jt.mo3113eE("type").get("maxHealthPoints")).floatValue());
                    } else {
                        System.err.println("Hull onLoadVersion: hull type is null !");
                    }
                }
            } catch (NullPointerException e) {
                System.err.println("Hull : onLoadVersion " + e);
            }
        }
    }

    @C0064Am(aul = "4aafadfdf7bcc9a31b0216daf324f61e", aum = 0)
    /* renamed from: e */
    private float m33930e(DamageType fr, float f) {
        C1649YI yi = (C1649YI) m33904Td().get(fr);
        if (yi != null) {
            return yi.mo6928oI(f);
        }
        return 0.0f;
    }

    @C0064Am(aul = "7e4731a8c80e067b9fe4e7600a0bcd6c", aum = 0)
    /* renamed from: g */
    private float m33932g(DamageType fr) {
        C1649YI yi = (C1649YI) m33904Td().get(fr);
        if (yi == null || yi.getValue() <= 0.0f) {
            return 0.0f;
        }
        return yi.getValue();
    }

    @C0064Am(aul = "97e74a43630de9aff4d952d26f4d53e9", aum = 0)
    /* renamed from: u */
    private boolean m33940u(DamageType fr) {
        return amY().contains(fr);
    }

    @C0064Am(aul = "8405bd9b186160ce1471565a4bb8a771", aum = 0)
    /* renamed from: Mo */
    private Ship m33894Mo() {
        return m33909Tn();
    }

    @C0064Am(aul = "d6e5461d340898ecd81413ee678f0094", aum = 0)
    /* renamed from: TK */
    private void m33898TK() {
        m33901TP();
    }

    @C0064Am(aul = "963720e7242990c5230412f794ab67f7", aum = 0)
    /* renamed from: TM */
    private void m33899TM() {
        m33901TP();
    }

    @C0064Am(aul = "fe982c5349f12946a844c7998d695d0d", aum = 0)
    /* renamed from: TO */
    private void m33900TO() {
        m33926cF(((HullAdapter) m33905Te().ase()).mo9878hh());
        for (DamageType next : ala().aLs()) {
            m33907Tj().put(next, Float.valueOf(((HullAdapter) m33905Te().ase()).mo9876b(next)));
        }
    }

    @C0064Am(aul = "365ab8103350fd5739faeb04d125a0d1", aum = 0)
    /* renamed from: TQ */
    private void m33902TQ() {
        if (m33905Te() == null) {
            mo8358lY(String.valueOf(bFY()) + " with NULL adapterList");
            return;
        }
        m33905Te().mo23259b((C6872avM) this);
        m33901TP();
    }

    @C6485anp
    @C5511aMd
    /* renamed from: a.jR$a */
    public class BaseAdapter extends HullAdapter implements C1616Xf {

        /* renamed from: PF */
        public static final C2491fm f8309PF = null;
        public static final int _m_fieldCount = 0;
        public static final C5663aRz[] _m_fields = null;
        public static final int _m_methodCount = 0;
        public static final C2491fm[] _m_methods = null;
        /* renamed from: aT */
        public static final C5663aRz f8310aT = null;
        public static final C2491fm apC = null;
        public static final C2491fm apD = null;
        public static final C2491fm apE = null;
        public static final C2491fm apF = null;
        public static final C2491fm apG = null;
        /* renamed from: qa */
        public static final C2491fm f8311qa = null;
        /* renamed from: qb */
        public static final C2491fm f8312qb = null;
        public static final long serialVersionUID = 0;
        /* renamed from: uI */
        public static final C2491fm f8313uI = null;
        public static C6494any ___iScriptClass;
        @C0064Am(aul = "243b936efffd0f90ee37193247386280", aum = 0)
        static /* synthetic */ Hull apB;

        static {
            m33974V();
        }

        public BaseAdapter() {
            super((C5540aNg) null);
            super.mo10S();
        }

        public BaseAdapter(C5540aNg ang) {
            super(ang);
        }

        public BaseAdapter(Hull jRVar) {
            super((C5540aNg) null);
            super._m_script_init(jRVar);
        }

        /* renamed from: V */
        static void m33974V() {
            serialVersionUID = (long) 1;
            _m_fieldCount = HullAdapter._m_fieldCount + 1;
            _m_methodCount = HullAdapter._m_methodCount + 9;
            int i = HullAdapter._m_fieldCount;
            C5663aRz[] arzArr = new C5663aRz[(i + 1)];
            C5663aRz b = C5640aRc.m17844b(BaseAdapter.class, "243b936efffd0f90ee37193247386280", i);
            f8310aT = b;
            arzArr[i] = b;
            int i2 = i + 1;
            _m_fields = arzArr;
            C1634Xv.m11725a((Object[]) HullAdapter._m_fields, (Object[]) _m_fields);
            int i3 = HullAdapter._m_methodCount;
            C2491fm[] fmVarArr = new C2491fm[(i3 + 9)];
            C2491fm a = C4105zY.m41624a(BaseAdapter.class, "1ff36007f8651a15fd99401daeb02e12", i3);
            apC = a;
            fmVarArr[i3] = a;
            int i4 = i3 + 1;
            C2491fm a2 = C4105zY.m41624a(BaseAdapter.class, "26b3557368382433202a0d4a8e3b28ba", i4);
            apD = a2;
            fmVarArr[i4] = a2;
            int i5 = i4 + 1;
            C2491fm a3 = C4105zY.m41624a(BaseAdapter.class, "ac7b583e8bbf5b4beee41ab10f47d39d", i5);
            apE = a3;
            fmVarArr[i5] = a3;
            int i6 = i5 + 1;
            C2491fm a4 = C4105zY.m41624a(BaseAdapter.class, "ffdc128409d8b8d0b6a2d8baa24773cc", i6);
            f8311qa = a4;
            fmVarArr[i6] = a4;
            int i7 = i6 + 1;
            C2491fm a5 = C4105zY.m41624a(BaseAdapter.class, "05ef09ee007d8613f6eb785f8c67d949", i7);
            f8313uI = a5;
            fmVarArr[i7] = a5;
            int i8 = i7 + 1;
            C2491fm a6 = C4105zY.m41624a(BaseAdapter.class, "83ac06639461ee1e5f05dda168613bf1", i8);
            f8312qb = a6;
            fmVarArr[i8] = a6;
            int i9 = i8 + 1;
            C2491fm a7 = C4105zY.m41624a(BaseAdapter.class, "fc7ee9254b998d3690c62e0315038bab", i9);
            apF = a7;
            fmVarArr[i9] = a7;
            int i10 = i9 + 1;
            C2491fm a8 = C4105zY.m41624a(BaseAdapter.class, "3058d9f6ba13e4ee622bf1fa198a0d22", i10);
            f8309PF = a8;
            fmVarArr[i10] = a8;
            int i11 = i10 + 1;
            C2491fm a9 = C4105zY.m41624a(BaseAdapter.class, "6b5e752eb139ef5fa38d268d357db2df", i11);
            apG = a9;
            fmVarArr[i11] = a9;
            int i12 = i11 + 1;
            _m_methods = fmVarArr;
            C1634Xv.m11725a((Object[]) HullAdapter._m_methods, (Object[]) _m_methods);
            ___iScriptClass = aUO.m18566a(BaseAdapter.class, C0184CJ.class, _m_fields, _m_methods);
        }

        @C0064Am(aul = "fc7ee9254b998d3690c62e0315038bab", aum = 0)
        /* renamed from: ID */
        private GameObjectAdapter m33970ID() {
            return mo19939IC();
        }

        @C0064Am(aul = "6b5e752eb139ef5fa38d268d357db2df", aum = 0)
        /* renamed from: IF */
        private GameObjectAdapter m33971IF() {
            return mo19938IA();
        }

        /* renamed from: Iy */
        private Hull m33972Iy() {
            return (Hull) bFf().mo5608dq().mo3214p(f8310aT);
        }

        @C0064Am(aul = "3058d9f6ba13e4ee622bf1fa198a0d22", aum = 0)
        /* renamed from: a */
        private void m33978a(GameObjectAdapter ato, GameObjectAdapter ato2, GameObjectAdapter ato3) {
            mo19940b((HullAdapter) ato, (HullAdapter) ato2, (HullAdapter) ato3);
        }

        /* renamed from: b */
        private void m33979b(Hull jRVar) {
            bFf().mo5608dq().mo3197f(f8310aT, jRVar);
        }

        /* renamed from: IA */
        public HullAdapter mo19938IA() {
            switch (bFf().mo6893i(apD)) {
                case 0:
                    return null;
                case 2:
                    return (HullAdapter) bFf().mo5606d(new aCE(this, apD, new Object[0]));
                case 3:
                    bFf().mo5606d(new aCE(this, apD, new Object[0]));
                    break;
            }
            return m33973Iz();
        }

        /* renamed from: IC */
        public HullAdapter mo19939IC() {
            switch (bFf().mo6893i(apE)) {
                case 0:
                    return null;
                case 2:
                    return (HullAdapter) bFf().mo5606d(new aCE(this, apE, new Object[0]));
                case 3:
                    bFf().mo5606d(new aCE(this, apE, new Object[0]));
                    break;
            }
            return m33969IB();
        }

        /* renamed from: IE */
        public /* bridge */ /* synthetic */ GameObjectAdapter mo16070IE() {
            switch (bFf().mo6893i(apF)) {
                case 0:
                    return null;
                case 2:
                    return (GameObjectAdapter) bFf().mo5606d(new aCE(this, apF, new Object[0]));
                case 3:
                    bFf().mo5606d(new aCE(this, apF, new Object[0]));
                    break;
            }
            return m33970ID();
        }

        /* renamed from: IG */
        public /* bridge */ /* synthetic */ GameObjectAdapter mo16071IG() {
            switch (bFf().mo6893i(apG)) {
                case 0:
                    return null;
                case 2:
                    return (GameObjectAdapter) bFf().mo5606d(new aCE(this, apG, new Object[0]));
                case 3:
                    bFf().mo5606d(new aCE(this, apG, new Object[0]));
                    break;
            }
            return m33971IF();
        }

        /* renamed from: S */
        public void mo10S() {
            super.mo10S();
        }

        /* renamed from: T */
        public C6494any mo11T() {
            return ___iScriptClass;
        }

        /* renamed from: U */
        public C2491fm[] mo12U() {
            return _m_methods;
        }

        /* renamed from: W */
        public Object mo13W() {
            return new C0184CJ(this);
        }

        /* renamed from: a */
        public Object mo14a(C0495Gr gr) {
            Object[] args = gr.getArgs();
            switch (gr.mo2417hq() - HullAdapter._m_methodCount) {
                case 0:
                    m33977a((HullAdapter) args[0], (HullAdapter) args[1], (HullAdapter) args[2]);
                    return null;
                case 1:
                    return m33973Iz();
                case 2:
                    return m33969IB();
                case 3:
                    return new Float(m33976a((DamageType) args[0], ((Float) args[1]).floatValue()));
                case 4:
                    return new Float(m33975a((DamageType) args[0]));
                case 5:
                    return new Float(m33980hg());
                case 6:
                    return m33970ID();
                case 7:
                    m33978a((GameObjectAdapter) args[0], (GameObjectAdapter) args[1], (GameObjectAdapter) args[2]);
                    return null;
                case 8:
                    return m33971IF();
                default:
                    return super.mo14a(gr);
            }
        }

        /* renamed from: a */
        public void mo15a(Collection collection, C0495Gr gr) {
            super.mo15a(collection, gr);
        }

        /* renamed from: b */
        public float mo9876b(DamageType fr) {
            switch (bFf().mo6893i(f8313uI)) {
                case 0:
                    return 0.0f;
                case 2:
                    return ((Float) bFf().mo5606d(new aCE(this, f8313uI, new Object[]{fr}))).floatValue();
                case 3:
                    bFf().mo5606d(new aCE(this, f8313uI, new Object[]{fr}));
                    break;
            }
            return m33975a(fr);
        }

        /* renamed from: b */
        public float mo9877b(DamageType fr, float f) {
            switch (bFf().mo6893i(f8311qa)) {
                case 0:
                    return 0.0f;
                case 2:
                    return ((Float) bFf().mo5606d(new aCE(this, f8311qa, new Object[]{fr, new Float(f)}))).floatValue();
                case 3:
                    bFf().mo5606d(new aCE(this, f8311qa, new Object[]{fr, new Float(f)}));
                    break;
            }
            return m33976a(fr, f);
        }

        /* renamed from: b */
        public void mo19940b(HullAdapter alm, HullAdapter alm2, HullAdapter alm3) {
            switch (bFf().mo6893i(apC)) {
                case 0:
                    return;
                case 2:
                    bFf().mo5606d(new aCE(this, apC, new Object[]{alm, alm2, alm3}));
                    return;
                case 3:
                    bFf().mo5606d(new aCE(this, apC, new Object[]{alm, alm2, alm3}));
                    break;
            }
            m33977a(alm, alm2, alm3);
        }

        /* renamed from: b */
        public /* bridge */ /* synthetic */ void mo12950b(GameObjectAdapter ato, GameObjectAdapter ato2, GameObjectAdapter ato3) {
            switch (bFf().mo6893i(f8309PF)) {
                case 0:
                    return;
                case 2:
                    bFf().mo5606d(new aCE(this, f8309PF, new Object[]{ato, ato2, ato3}));
                    return;
                case 3:
                    bFf().mo5606d(new aCE(this, f8309PF, new Object[]{ato, ato2, ato3}));
                    break;
            }
            m33978a(ato, ato2, ato3);
        }

        /* renamed from: c */
        public C5663aRz[] mo25c() {
            return _m_fields;
        }

        /* renamed from: hh */
        public float mo9878hh() {
            switch (bFf().mo6893i(f8312qb)) {
                case 0:
                    return 0.0f;
                case 2:
                    return ((Float) bFf().mo5606d(new aCE(this, f8312qb, new Object[0]))).floatValue();
                case 3:
                    bFf().mo5606d(new aCE(this, f8312qb, new Object[0]));
                    break;
            }
            return m33980hg();
        }

        /* renamed from: c */
        public void mo19941c(Hull jRVar) {
            m33979b(jRVar);
            super.mo10S();
        }

        @C0064Am(aul = "1ff36007f8651a15fd99401daeb02e12", aum = 0)
        /* renamed from: a */
        private void m33977a(HullAdapter alm, HullAdapter alm2, HullAdapter alm3) {
            if (alm != null) {
                throw new IllegalArgumentException();
            } else if (alm3 != this) {
                throw new IllegalArgumentException();
            } else {
                super.mo12950b(alm, alm2, alm3);
            }
        }

        @C0064Am(aul = "26b3557368382433202a0d4a8e3b28ba", aum = 0)
        /* renamed from: Iz */
        private HullAdapter m33973Iz() {
            return null;
        }

        @C0064Am(aul = "ac7b583e8bbf5b4beee41ab10f47d39d", aum = 0)
        /* renamed from: IB */
        private HullAdapter m33969IB() {
            return null;
        }

        @C0064Am(aul = "ffdc128409d8b8d0b6a2d8baa24773cc", aum = 0)
        /* renamed from: a */
        private float m33976a(DamageType fr, float f) {
            return m33972Iy().mo19932f(fr, f);
        }

        @C0064Am(aul = "05ef09ee007d8613f6eb785f8c67d949", aum = 0)
        /* renamed from: a */
        private float m33975a(DamageType fr) {
            return m33972Iy().mo19933h(fr);
        }

        @C0064Am(aul = "83ac06639461ee1e5f05dda168613bf1", aum = 0)
        /* renamed from: hg */
        private float m33980hg() {
            return m33972Iy().mo19922Tv();
        }
    }
}
