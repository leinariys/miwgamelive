package game.script.ship;

import game.network.message.externalizable.aCE;
import game.script.util.GameObjectAdapter;
import logic.baa.*;
import logic.data.mbean.C6557apJ;
import logic.res.code.C5663aRz;
import logic.res.code.C6494any;
import logic.res.html.C2491fm;
import p001a.*;

import java.util.Collection;

@C5511aMd
@C6485anp
/* renamed from: a.TU */
/* compiled from: a */
public class ShipAdapter extends GameObjectAdapter<ShipAdapter> implements C1616Xf {
    public static final int _m_fieldCount = 0;
    public static final C5663aRz[] _m_fields = null;
    public static final int _m_methodCount = 0;
    public static final C2491fm[] _m_methods = null;
    public static final C2491fm aTe = null;
    public static final C2491fm aTf = null;
    public static final C2491fm aTg = null;
    public static final C2491fm aTh = null;
    public static final C2491fm bpZ = null;
    public static final long serialVersionUID = 0;
    public static C6494any ___iScriptClass;

    static {
        m9963V();
    }

    public ShipAdapter() {
        super((C5540aNg) null);
        super.mo10S();
    }

    public ShipAdapter(C5540aNg ang) {
        super(ang);
    }

    public ShipAdapter(C2961mJ mJVar) {
        super((C5540aNg) null);
        super.mo967a(mJVar);
    }

    /* renamed from: V */
    static void m9963V() {
        serialVersionUID = (long) 1;
        _m_fieldCount = GameObjectAdapter._m_fieldCount + 0;
        _m_methodCount = GameObjectAdapter._m_methodCount + 5;
        _m_fields = new C5663aRz[(GameObjectAdapter._m_fieldCount + 0)];
        C1634Xv.m11725a((Object[]) GameObjectAdapter._m_fields, (Object[]) _m_fields);
        int i = GameObjectAdapter._m_methodCount;
        C2491fm[] fmVarArr = new C2491fm[(i + 5)];
        C2491fm a = C4105zY.m41624a(ShipAdapter.class, "8f0aa72b676b79a9156675d4d08f17f3", i);
        aTe = a;
        fmVarArr[i] = a;
        int i2 = i + 1;
        C2491fm a2 = C4105zY.m41624a(ShipAdapter.class, "f9735f1e810119005179834e41a7a35b", i2);
        aTf = a2;
        fmVarArr[i2] = a2;
        int i3 = i2 + 1;
        C2491fm a3 = C4105zY.m41624a(ShipAdapter.class, "fc4da8ea29ca88de645ee3d52f55d6f7", i3);
        aTg = a3;
        fmVarArr[i3] = a3;
        int i4 = i3 + 1;
        C2491fm a4 = C4105zY.m41624a(ShipAdapter.class, "f69370db54fa182284a0587599355cb8", i4);
        aTh = a4;
        fmVarArr[i4] = a4;
        int i5 = i4 + 1;
        C2491fm a5 = C4105zY.m41624a(ShipAdapter.class, "05f6365e0b625dbda26042c7bccbe7bf", i5);
        bpZ = a5;
        fmVarArr[i5] = a5;
        int i6 = i5 + 1;
        _m_methods = fmVarArr;
        C1634Xv.m11725a((Object[]) GameObjectAdapter._m_methods, (Object[]) _m_methods);
        ___iScriptClass = aUO.m18566a(ShipAdapter.class, C6557apJ.class, _m_fields, _m_methods);
    }

    /* renamed from: T */
    public C6494any mo11T() {
        return ___iScriptClass;
    }

    /* renamed from: U */
    public C2491fm[] mo12U() {
        return _m_methods;
    }

    /* renamed from: VF */
    public float mo5665VF() {
        switch (bFf().mo6893i(aTe)) {
            case 0:
                return 0.0f;
            case 2:
                return ((Float) bFf().mo5606d(new aCE(this, aTe, new Object[0]))).floatValue();
            case 3:
                bFf().mo5606d(new aCE(this, aTe, new Object[0]));
                break;
        }
        return m9964VE();
    }

    /* renamed from: VH */
    public float mo5666VH() {
        switch (bFf().mo6893i(aTf)) {
            case 0:
                return 0.0f;
            case 2:
                return ((Float) bFf().mo5606d(new aCE(this, aTf, new Object[0]))).floatValue();
            case 3:
                bFf().mo5606d(new aCE(this, aTf, new Object[0]));
                break;
        }
        return m9965VG();
    }

    /* renamed from: W */
    public Object mo13W() {
        return new C6557apJ(this);
    }

    /* renamed from: a */
    public Object mo14a(C0495Gr gr) {
        gr.getArgs();
        switch (gr.mo2417hq() - GameObjectAdapter._m_methodCount) {
            case 0:
                return new Float(m9964VE());
            case 1:
                return new Float(m9965VG());
            case 2:
                return new Float(m9966VI());
            case 3:
                return new Float(m9967VJ());
            case 4:
                return new Float(ago());
            default:
                return super.mo14a(gr);
        }
    }

    /* renamed from: a */
    public void mo15a(Collection collection, C0495Gr gr) {
        super.mo15a(collection, gr);
    }

    public float agp() {
        switch (bFf().mo6893i(bpZ)) {
            case 0:
                return 0.0f;
            case 2:
                return ((Float) bFf().mo5606d(new aCE(this, bpZ, new Object[0]))).floatValue();
            case 3:
                bFf().mo5606d(new aCE(this, bpZ, new Object[0]));
                break;
        }
        return ago();
    }

    /* renamed from: c */
    public C5663aRz[] mo25c() {
        return _m_fields;
    }

    /* renamed from: ra */
    public float mo5668ra() {
        switch (bFf().mo6893i(aTh)) {
            case 0:
                return 0.0f;
            case 2:
                return ((Float) bFf().mo5606d(new aCE(this, aTh, new Object[0]))).floatValue();
            case 3:
                bFf().mo5606d(new aCE(this, aTh, new Object[0]));
                break;
        }
        return m9967VJ();
    }

    /* renamed from: rb */
    public float mo5669rb() {
        switch (bFf().mo6893i(aTg)) {
            case 0:
                return 0.0f;
            case 2:
                return ((Float) bFf().mo5606d(new aCE(this, aTg, new Object[0]))).floatValue();
            case 3:
                bFf().mo5606d(new aCE(this, aTg, new Object[0]));
                break;
        }
        return m9966VI();
    }

    /* renamed from: a */
    public void mo967a(C2961mJ mJVar) {
        super.mo967a(mJVar);
    }

    /* renamed from: S */
    public void mo10S() {
        super.mo10S();
    }

    @C0064Am(aul = "8f0aa72b676b79a9156675d4d08f17f3", aum = 0)
    /* renamed from: VE */
    private float m9964VE() {
        return ((ShipAdapter) mo16071IG()).mo5665VF();
    }

    @C0064Am(aul = "f9735f1e810119005179834e41a7a35b", aum = 0)
    /* renamed from: VG */
    private float m9965VG() {
        return ((ShipAdapter) mo16071IG()).mo5666VH();
    }

    @C0064Am(aul = "fc4da8ea29ca88de645ee3d52f55d6f7", aum = 0)
    /* renamed from: VI */
    private float m9966VI() {
        return ((ShipAdapter) mo16071IG()).mo5669rb();
    }

    @C0064Am(aul = "f69370db54fa182284a0587599355cb8", aum = 0)
    /* renamed from: VJ */
    private float m9967VJ() {
        return ((ShipAdapter) mo16071IG()).mo5668ra();
    }

    @C0064Am(aul = "05f6365e0b625dbda26042c7bccbe7bf", aum = 0)
    private float ago() {
        return ((ShipAdapter) mo16071IG()).agp();
    }
}
