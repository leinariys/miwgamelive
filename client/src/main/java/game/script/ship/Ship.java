package game.script.ship;

import com.hoplon.geometry.Color;
import com.hoplon.geometry.Vec3f;
import game.CollisionFXSet;
import game.geometry.Vec3d;
import game.network.message.externalizable.*;
import game.network.message.serializable.C2686iZ;
import game.network.message.serializable.C3438ra;
import game.script.Actor;
import game.script.Character;
import game.script.Pawn;
import game.script.TaikodomObject;
import game.script.ai.npc.StationaryAIController;
import game.script.bank.Bank;
import game.script.corporation.CorporationLogo;
import game.script.damage.DamageType;
import game.script.faction.Faction;
import game.script.item.*;
import game.script.login.UserConnection;
import game.script.missile.MissileWeapon;
import game.script.mission.Mission;
import game.script.mission.MissionTrigger;
import game.script.nls.NLSManager;
import game.script.nls.NLSWindowAlert;
import game.script.npc.NPC;
import game.script.resource.Asset;
import game.script.simulation.Space;
import game.script.space.*;
import game.script.util.AdapterLikedList;
import game.script.util.GameObjectAdapter;
import logic.aaa.*;
import logic.baa.*;
import logic.bbb.C4029yK;
import logic.data.link.C0471GX;
import logic.data.link.C2530gR;
import logic.data.link.C3396rD;
import logic.data.link.C3981xi;
import logic.data.mbean.C1392UP;
import logic.data.mbean.C3430rU;
import logic.data.mbean.aMX;
import logic.res.ILoaderTrail;
import logic.res.KeyCode;
import logic.res.LoaderTrail;
import logic.res.code.C5640aRc;
import logic.res.code.C5663aRz;
import logic.res.code.C6494any;
import logic.res.html.C2491fm;
import logic.res.sound.C0907NJ;
import logic.sql.C1363Tr;
import logic.sql.C2059bi;
import logic.sql.C5878acG;
import logic.thred.C6339akz;
import logic.thred.LogPrinter;
import org.apache.commons.logging.Log;
import org.mozilla1.javascript.ScriptRuntime;
import p001a.*;
import taikodom.infra.script.I18NString;
import taikodom.render.scene.SPitchedSet;
import taikodom.render.scene.Scene;
import taikodom.render.scene.SceneObject;

import javax.vecmath.Vector3f;
import java.util.*;

@C5829abJ("1.2.3")
@aEZ("Basic")
@C2712iu(mo19786Bt = BaseShipType.class, version = "1.1.2")
@C5511aMd
@C6485anp
/* renamed from: a.fA */
/* compiled from: a */
public class Ship extends Pawn implements C6661arJ, aDA, C2530gR.C2531a, C2508fz, C1762a, C5359aGh, C0471GX.C0472a, C3396rD.C3397a, aOW, C6480ank<ShipAdapter>, C6809auB, C6200aiQ<Hull>, C6872avM, C4072yv, C4072yv {

    /* renamed from: CB */
    public static final C2491fm f7116CB = null;

    /* renamed from: Cq */
    public static final C2491fm f7117Cq = null;

    /* renamed from: Do */
    public static final C2491fm f7118Do = null;
    /* renamed from: LR */
    public static final C5663aRz f7120LR = null;
    /* renamed from: Lj */
    public static final C2491fm f7121Lj = null;
    /* renamed from: Lm */
    public static final C2491fm f7122Lm = null;
    /* renamed from: Mq */
    public static final C2491fm f7123Mq = null;
    /* renamed from: Pf */
    public static final C2491fm f7124Pf = null;
    /* renamed from: VB */
    public static final C5663aRz f7125VB = null;
    /* renamed from: VW */
    public static final C5663aRz f7127VW = null;
    /* renamed from: Wc */
    public static final C5663aRz f7131Wc = null;
    /* renamed from: We */
    public static final C5663aRz f7133We = null;
    /* renamed from: Wk */
    public static final C2491fm f7134Wk = null;
    /* renamed from: Wl */
    public static final C2491fm f7135Wl = null;
    /* renamed from: Wm */
    public static final C2491fm f7136Wm = null;
    /* renamed from: Wp */
    public static final C2491fm f7137Wp = null;
    /* renamed from: Wu */
    public static final C2491fm f7138Wu = null;
    public static final C2491fm _f_dispose_0020_0028_0029V = null;
    public static final C2491fm _f_getType_0020_0028_0029Ljava_002flang_002fObject_003b = null;
    public static final C2491fm _f_hasHollowField_0020_0028_0029Z = null;
    public static final C2491fm _f_step_0020_0028F_0029V = null;
    public static final C2491fm _f_toString_0020_0028_0029Ljava_002flang_002fString_003b = null;
    public static final int _m_fieldCount = 0;
    public static final C5663aRz[] _m_fields = null;
    public static final int _m_methodCount = 0;
    public static final C2491fm[] _m_methods = null;
    public static final C2491fm aKm = null;
    public static final C5663aRz aMh = null;
    public static final C5663aRz aMi = null;
    public static final C5663aRz aOx = null;
    public static final C2491fm aPe = null;
    public static final C2491fm aPf = null;
    public static final C2491fm aPj = null;
    public static final C2491fm aPk = null;
    public static final C2491fm aPl = null;
    public static final C2491fm aPm = null;
    public static final C2491fm aRm = null;
    public static final C2491fm aTe = null;
    public static final C2491fm aTf = null;
    public static final C2491fm aTg = null;
    public static final C2491fm aTh = null;
    public static final C2491fm aTk = null;
    public static final C5663aRz awd = null;
    public static final C2491fm bbS = null;
    public static final C2491fm bbU = null;
    public static final C5663aRz bid = null;
    public static final C2491fm bka = null;
    public static final C2491fm bkd = null;
    public static final C2491fm bkk = null;
    public static final C5663aRz bnD = null;
    public static final C5663aRz bnF = null;
    public static final C5663aRz bnH = null;
    public static final C5663aRz bnJ = null;
    public static final C5663aRz bnL = null;
    public static final C5663aRz bnN = null;
    public static final C5663aRz bnO = null;
    public static final C5663aRz bnP = null;
    public static final C5663aRz bnR = null;
    public static final C5663aRz bnT = null;
    public static final C5663aRz bnU = null;
    public static final C5663aRz bnV = null;
    public static final C5663aRz bnX = null;
    public static final C5663aRz bnZ = null;
    public static final C5663aRz boD = null;
    public static final C5663aRz boF = null;
    public static final C5663aRz boI = null;
    public static final C5663aRz boK = null;
    public static final C5663aRz boM = null;
    public static final C5663aRz boO = null;
    public static final C5663aRz boP = null;
    public static final C5663aRz boS = null;
    public static final C5663aRz boU = null;
    public static final C5663aRz boW = null;
    public static final C5663aRz bob = null;
    public static final C5663aRz bod = null;
    public static final C5663aRz bof = null;
    public static final C5663aRz boi = null;
    public static final C5663aRz bok = null;
    public static final C5663aRz bom = null;
    public static final C5663aRz boo = null;
    public static final C5663aRz boq = null;
    public static final C5663aRz bos = null;
    public static final C5663aRz bot = null;
    public static final C5663aRz bou = null;
    public static final C5663aRz bow = null;
    public static final C5663aRz boz = null;
    public static final C2491fm bpA = null;
    public static final C2491fm bpB = null;
    public static final C2491fm bpC = null;
    public static final C2491fm bpD = null;
    public static final C2491fm bpE = null;
    public static final C2491fm bpF = null;
    public static final C2491fm bpG = null;
    public static final C2491fm bpH = null;
    public static final C2491fm bpI = null;
    public static final C2491fm bpJ = null;
    public static final C2491fm bpK = null;
    public static final C2491fm bpL = null;
    public static final C2491fm bpM = null;
    public static final C2491fm bpN = null;
    public static final C2491fm bpO = null;
    public static final C2491fm bpP = null;
    public static final C2491fm bpQ = null;
    public static final C2491fm bpR = null;
    public static final C2491fm bpS = null;
    public static final C2491fm bpT = null;
    public static final C2491fm bpU = null;
    public static final C2491fm bpV = null;
    public static final C2491fm bpW = null;
    public static final C2491fm bpX = null;
    public static final C2491fm bpY = null;
    public static final C2491fm bpZ = null;
    public static final C5663aRz bpb = null;
    public static final C5663aRz bpd = null;
    public static final C5663aRz bph = null;
    public static final C5663aRz bpj = null;
    public static final C5663aRz bpl = null;
    public static final C5663aRz bpn = null;
    public static final C5663aRz bpq = null;
    public static final C2491fm bpr = null;
    public static final C2491fm bps = null;
    public static final C2491fm bpt = null;
    public static final C2491fm bpu = null;
    public static final C2491fm bpv = null;
    public static final C2491fm bpw = null;
    public static final C2491fm bpx = null;
    public static final C2491fm bpy = null;
    public static final C2491fm bpz = null;
    public static final C2491fm bqA = null;
    public static final C2491fm bqB = null;
    public static final C2491fm bqC = null;
    public static final C2491fm bqD = null;
    public static final C2491fm bqE = null;
    public static final C2491fm bqF = null;
    public static final C2491fm bqG = null;
    public static final C2491fm bqH = null;
    public static final C2491fm bqI = null;
    public static final C2491fm bqJ = null;
    public static final C2491fm bqK = null;
    public static final C2491fm bqL = null;
    public static final C2491fm bqM = null;
    public static final C2491fm bqN = null;
    public static final C2491fm bqO = null;
    public static final C2491fm bqP = null;
    public static final C2491fm bqQ = null;
    public static final C2491fm bqR = null;
    public static final C2491fm bqS = null;
    public static final C2491fm bqT = null;
    public static final C2491fm bqU = null;
    public static final C2491fm bqV = null;
    public static final C2491fm bqW = null;
    public static final C2491fm bqX = null;
    public static final C2491fm bqY = null;
    public static final C2491fm bqZ = null;
    public static final C2491fm bqa = null;
    public static final C2491fm bqb = null;
    public static final C2491fm bqc = null;
    public static final C2491fm bqd = null;
    public static final C2491fm bqe = null;
    public static final C2491fm bqf = null;
    public static final C2491fm bqg = null;
    public static final C2491fm bqh = null;
    public static final C2491fm bqi = null;
    public static final C2491fm bqj = null;
    public static final C2491fm bqk = null;
    public static final C2491fm bql = null;
    public static final C2491fm bqm = null;
    public static final C2491fm bqn = null;
    public static final C2491fm bqo = null;
    public static final C2491fm bqp = null;
    public static final C2491fm bqq = null;
    public static final C2491fm bqr = null;
    public static final C2491fm bqs = null;
    public static final C2491fm bqt = null;
    public static final C2491fm bqu = null;
    public static final C2491fm bqv = null;
    public static final C2491fm bqw = null;
    public static final C2491fm bqx = null;
    public static final C2491fm bqy = null;
    public static final C2491fm bqz = null;
    public static final C2491fm brA = null;
    public static final C2491fm brB = null;
    public static final C2491fm brC = null;
    public static final C2491fm brD = null;
    public static final C2491fm brE = null;
    public static final C2491fm brF = null;
    public static final C2491fm brG = null;
    public static final C2491fm brH = null;
    public static final C2491fm brI = null;
    public static final C2491fm brJ = null;
    public static final C2491fm brK = null;
    public static final C2491fm brL = null;
    public static final C2491fm brM = null;
    public static final C2491fm brN = null;
    public static final C2491fm brO = null;
    public static final C2491fm brP = null;
    public static final C2491fm brQ = null;
    public static final C2491fm brR = null;
    public static final C2491fm brS = null;
    public static final C2491fm brT = null;
    public static final C2491fm brU = null;
    public static final C2491fm brV = null;
    public static final C2491fm brW = null;
    public static final C2491fm brX = null;
    public static final C2491fm brY = null;
    public static final C2491fm brZ = null;
    public static final C2491fm bra = null;
    public static final C2491fm brb = null;
    public static final C2491fm brc = null;
    public static final C2491fm brd = null;
    public static final C2491fm bre = null;
    public static final C2491fm brf = null;
    public static final C2491fm brg = null;
    public static final C2491fm brh = null;
    public static final C2491fm bri = null;
    public static final C2491fm brj = null;
    public static final C2491fm brk = null;
    public static final C2491fm brl = null;
    public static final C2491fm brm = null;
    public static final C2491fm brn = null;
    public static final C2491fm bro = null;
    public static final C2491fm brp = null;
    public static final C2491fm brq = null;
    public static final C2491fm brr = null;
    public static final C2491fm brs = null;
    public static final C2491fm brt = null;
    public static final C2491fm bru = null;
    public static final C2491fm brv = null;
    public static final C2491fm brw = null;
    public static final C2491fm brx = null;
    public static final C2491fm bry = null;
    public static final C2491fm brz = null;
    public static final C2491fm bsa = null;
    public static final C2491fm bsb = null;
    public static final C2491fm bsc = null;
    public static final C2491fm bsd = null;
    public static final C2491fm bse = null;
    public static final C2491fm bsf = null;
    public static final C2491fm bsg = null;
    public static final C2491fm bsh = null;
    public static final C2491fm bsi = null;
    public static final C2491fm bsj = null;
    public static final C2491fm bsk = null;
    public static final C2491fm bsl = null;
    public static final C2491fm bsm = null;
    public static final C2491fm bsn = null;
    public static final C2491fm bso = null;
    public static final C2491fm bsp = null;
    public static final C2491fm bsq = null;
    public static final long serialVersionUID = 0;
    /* renamed from: uU */
    public static final C5663aRz f7145uU = null;
    /* renamed from: uX */
    public static final C2491fm f7146uX = null;
    /* renamed from: uY */
    public static final C2491fm f7147uY = null;
    /* renamed from: uZ */
    public static final C2491fm f7148uZ = null;
    /* renamed from: vc */
    public static final C2491fm f7149vc = null;
    /* renamed from: ve */
    public static final C2491fm f7150ve = null;
    /* renamed from: vi */
    public static final C2491fm f7151vi = null;
    /* renamed from: vm */
    public static final C2491fm f7152vm = null;
    /* renamed from: vn */
    public static final C2491fm f7153vn = null;
    /* renamed from: vo */
    public static final C2491fm f7154vo = null;
    /* renamed from: vq */
    public static final C2491fm f7155vq = null;
    /* renamed from: vr */
    public static final C2491fm f7156vr = null;
    /* renamed from: vs */
    public static final C2491fm f7157vs = null;
    /* renamed from: yg */
    public static final C2491fm f7158yg = null;
    /* renamed from: zr */
    public static final C2491fm f7159zr = null;
    private static final long bnA = 60000;
    private static final long bnB = 5000;
    private static final Log logger = LogPrinter.setClass(Ship.class);
    public static C6494any ___iScriptClass = null;
    @C0064Am(aul = "d65771ff3efabbe9926ff6d0008a8282", aum = 22)

    /* renamed from: LQ */
    private static Asset f7119LQ = null;
    @C0064Am(aul = "ce62fd24fad44a0c6c2bd67edb82fd22", aum = 36)
    @C0803Ld

    /* renamed from: VV */
    private static Hull f7126VV = null;
    @C0064Am(aul = "540b2fa017eb685368f950fc172fd658", aum = 30)

    /* renamed from: WL */
    private static float f7128WL = 0.7f;
    @C0064Am(aul = "76a2e596d427a588c043ad45d40b00ac", aum = 31)

    /* renamed from: WM */
    private static float f7129WM = 0.25f;
    @C0064Am(aul = "5908121079e3b58a89a70b76f3e8c976", aum = 6)

    /* renamed from: Wb */
    private static Asset f7130Wb = null;
    @C0064Am(aul = "de8d98cfda4e4684609496bf31c5984d", aum = 7)

    /* renamed from: Wd */
    private static Asset f7132Wd = null;
    @C0064Am(aul = "15f0692836c7328dd4fd758c7ac6fb58", aum = 34)
    @C5566aOg
    private static AdapterLikedList<ShipAdapter> aOw = null;
    @C0064Am(aul = "2d1b3508f6de8b3fb6bc8b7f77f1bc82", aum = 43)
    private static C6809auB.C1996a aZI = null;
    @C0064Am(aul = "4f6ed4c9369bb645b8d4d8fb201084d2", aum = 44)
    @C1020Ot(drz = C1020Ot.C1021a.NOT_PERSISTED)
    @C5256aCi
    private static Actor ams = null;
    @C0064Am(aul = "c0bf54703b959d66b2da7d00a53168c7", aum = 14)
    private static float bhk = 0.0f;
    @C0064Am(aul = "d6f2f05364d791966adfd5a66466d990", aum = 10)
    private static float bhl = 0.0f;
    @C0064Am(aul = "41ca32e6775ce7680bf4af9269151bb5", aum = 0)
    private static CargoHoldType bnC;
    @C0064Am(aul = "408f6518272376f783fcf481298532d7", aum = 3)
    @C5454aJy("Collisions FX")
    private static CollisionFXSet bnE;
    @C0064Am(aul = "f338f47e91e6988f5ea6eaaf9d0a2013", aum = 4)
    private static CruiseSpeedType bnG;
    @C0064Am(aul = "f5a599db78dcbc71b7443510492c74aa", aum = 5)
    private static Asset bnI;
    @C0064Am(aul = "6ca62339cef27a0e7dfe28855201c3e9", aum = 8)
    @C5454aJy("Long description")
    private static I18NString bnK;
    @C0064Am(aul = "59f03f8d501ae96eb33d3189aceaf769", aum = 9)
    private static LootType bnM;
    @C0064Am(aul = "bc2836484f3b82dbccc5e35292d0021e", aum = 12)
    private static float bnQ;
    @C0064Am(aul = "8e759831b6b819a453787c2c334bfc98", aum = 13)
    private static float bnS;
    @C0064Am(aul = "4e786aee15d241bf6f5e178492e073a9", aum = 16)
    private static float bnW;
    @C0064Am(aul = "703113558d172f6e6df6a8f27bc7a87f", aum = 17)
    private static float bnY;
    @C0064Am(aul = "6307cbd895f869dbf6b3c6c2941678f1", aum = 37)
    private static CargoHold boC;
    @C0064Am(aul = "c79cf2d18597b17dafdce6345a115078", aum = 38)
    private static C3438ra<ShipStructure> boE;
    @C0064Am(aul = "67a214c3f2bb3da8ce4cd82c57fa1d32", aum = 39)
    private static Character boH;
    @C0064Am(aul = "b6f3e2426c4db92cb5c518db4dde10f4", aum = 40)
    private static C3438ra<Component> boJ;
    @C0064Am(aul = "ff062fb3e7385f02b8dc783cc72aa5dd", aum = 41)
    private static ShipItem boL;
    @C0064Am(aul = "f94501d21485a328fc44f085e8f959ce", aum = 42)
    private static long boN;
    @C0064Am(aul = "5eaa252d59d9bdd4ae9bdf5049e6aebd", aum = 45)
    private static C3438ra<Turret> boR;
    @C0064Am(aul = "f7973441f80aa15f5856ac20a967e2e2", aum = 46)
    @C5256aCi
    private static Weapon boT;
    @C0064Am(aul = "7729e8215e41642c9f9c1cf2db254825", aum = 47)
    @C5256aCi
    private static Weapon boV;
    @C0064Am(aul = "f57039722771c290261270fc11ea215a", aum = 18)
    @C5454aJy("Orbital Camera Distance\\n(Default is Camera Distance)")
    private static float boa;
    @C0064Am(aul = "95a88713e9f15ed67f247d88fdeabe24", aum = 19)
    private static Asset boc;
    @C0064Am(aul = "78d648ae28cc6e326c3d7766c839c604", aum = 20)
    @C5454aJy("Camera Y Position on Screen\\n(Default 0.0f)")
    private static float boe;
    @C0064Am(aul = "77920c33a6f29cb25c6827ec067906b7", aum = 21)
    private static float bog;
    @C0064Am(aul = "fe70592ffdee3463b6fc2a6ec4a2c282", aum = 23)
    private static float boh = 0.0f;
    @C0064Am(aul = "436986ecb8ccee6ec5ede89815f949ed", aum = 24)
    private static C3438ra<ShipStructureType> boj;
    @C0064Am(aul = "5e8a70067774850a0ab59335dcf38c43", aum = 26)
    private static C3438ra<C3315qE> bol;
    @C0064Am(aul = "cda67b981bd6b81eddc11b146d278d4f", aum = 27)
    private static Vec3d bon;
    @C0064Am(aul = "eaad92915612d97ae95413006a61691c", aum = 28)
    private static Vec3d bop;
    @C0064Am(aul = "f9e840447561fabf74bee25bb8054b85", aum = 29)
    private static Vec3d bor;
    @C0064Am(aul = "cf25afcf2d302c59983c9b7d46d6dd04", aum = 33)
    @C5454aJy("Explosion delay (secs)")
    private static float bov;
    @C0064Am(aul = "8559899c23517ad1c07a27c42c27859c", aum = 35)
    private static CruiseSpeed boy;
    @C0064Am(aul = "f9350cce5941de23e828745965eb122e", aum = 48)
    @C1020Ot(drz = C1020Ot.C1021a.NOT_PERSISTED)
    private static long bpa;
    @C0064Am(aul = "955302f0b2669d8f3fc357b44eb4d71b", aum = 49)
    @C1020Ot(drz = C1020Ot.C1021a.NOT_PERSISTED)
    private static DeathInformation bpc;
    @C0064Am(aul = "95f9f1daff345868b5c0f0a6e0e9ce34", aum = 50)
    private static float bpg;
    @C0064Am(aul = "2283a964f9c6533a51ded6e38d40a0e4", aum = 51)
    private static float bpi;
    @C0064Am(aul = "a36555ac5f2f582ce9600a99ade722ea", aum = 52)
    private static float bpk;
    @C0064Am(aul = "04de02b87e1ef104769337bf29e98626", aum = 53)
    private static float bpm;
    @C0064Am(aul = "2f73c026196fec0b941468ccfd34fa6b", aum = 54)
    private static float bpo;
    @C0064Am(aul = "60b44e37464206ba7cb7aa01c04aacb8", aum = 15)

    /* renamed from: dW */
    private static float f7139dW;
    @C0064Am(aul = "1394e5857a668e774a91cbcffd431fb2", aum = 11)

    /* renamed from: dX */
    private static float f7140dX;
    @C0064Am(aul = "c3d92a8da7ad2013bb2a64cb2f6ec4e8", aum = 1)

    /* renamed from: jS */
    private static C6194aiK f7141jS;
    @C0064Am(aul = "2c9737909a77f97389e30aaed1101b65", aum = 2)

    /* renamed from: jT */
    private static C3438ra<C6209aiZ> f7142jT;
    @C0064Am(aul = "6bdc7d72f8bbf3f16b2963456b47fc8e", aum = 32)

    /* renamed from: jV */
    private static float f7143jV;
    @C0064Am(aul = "09d79fe35c3692b7feeeae637bcac0c8", aum = 25)

    /* renamed from: uT */
    private static String f7144uT;

    static {
        m30209V();
    }

    @ClientOnly
    private transient C1255SZ ajg;
    @ClientOnly
    private transient SPitchedSet boA;
    private transient boolean boB;
    @ClientOnly
    private transient boolean boG;
    @ClientOnly
    private Actor boQ;
    @ClientOnly
    private C6853aut boX;
    @ClientOnly
    private C6853aut boY;
    @ClientOnly
    private C6853aut boZ;
    private transient float box;
    @ClientOnly
    private C2279dZ bpe;
    @C1020Ot(drz = C1020Ot.C1021a.NOT_PERSISTED)
    @ClientOnly
    private C3869w bpf;
    @ClientOnly
    private float radius;

    public Ship() {
        super((C5540aNg) null);
        super.mo10S();
    }

    public Ship(ShipType ng) {
        super((C5540aNg) null);
        super._m_script_init(ng);
    }

    public Ship(C5540aNg ang) {
        super(ang);
    }

    /* renamed from: V */
    static void m30209V() {
        serialVersionUID = (long) 1;
        _m_fieldCount = Pawn._m_fieldCount + 55;
        _m_methodCount = Pawn._m_methodCount + KeyCode.csI;
        int i = Pawn._m_fieldCount;
        C5663aRz[] arzArr = new C5663aRz[(i + 55)];
        C5663aRz b = C5640aRc.m17844b(Ship.class, "41ca32e6775ce7680bf4af9269151bb5", i);
        bnD = b;
        arzArr[i] = b;
        int i2 = i + 1;
        C5663aRz b2 = C5640aRc.m17844b(Ship.class, "c3d92a8da7ad2013bb2a64cb2f6ec4e8", i2);
        aMh = b2;
        arzArr[i2] = b2;
        int i3 = i2 + 1;
        C5663aRz b3 = C5640aRc.m17844b(Ship.class, "2c9737909a77f97389e30aaed1101b65", i3);
        awd = b3;
        arzArr[i3] = b3;
        int i4 = i3 + 1;
        C5663aRz b4 = C5640aRc.m17844b(Ship.class, "408f6518272376f783fcf481298532d7", i4);
        bnF = b4;
        arzArr[i4] = b4;
        int i5 = i4 + 1;
        C5663aRz b5 = C5640aRc.m17844b(Ship.class, "f338f47e91e6988f5ea6eaaf9d0a2013", i5);
        bnH = b5;
        arzArr[i5] = b5;
        int i6 = i5 + 1;
        C5663aRz b6 = C5640aRc.m17844b(Ship.class, "f5a599db78dcbc71b7443510492c74aa", i6);
        bnJ = b6;
        arzArr[i6] = b6;
        int i7 = i6 + 1;
        C5663aRz b7 = C5640aRc.m17844b(Ship.class, "5908121079e3b58a89a70b76f3e8c976", i7);
        f7131Wc = b7;
        arzArr[i7] = b7;
        int i8 = i7 + 1;
        C5663aRz b8 = C5640aRc.m17844b(Ship.class, "de8d98cfda4e4684609496bf31c5984d", i8);
        f7133We = b8;
        arzArr[i8] = b8;
        int i9 = i8 + 1;
        C5663aRz b9 = C5640aRc.m17844b(Ship.class, "6ca62339cef27a0e7dfe28855201c3e9", i9);
        bnL = b9;
        arzArr[i9] = b9;
        int i10 = i9 + 1;
        C5663aRz b10 = C5640aRc.m17844b(Ship.class, "59f03f8d501ae96eb33d3189aceaf769", i10);
        bnN = b10;
        arzArr[i10] = b10;
        int i11 = i10 + 1;
        C5663aRz b11 = C5640aRc.m17844b(Ship.class, "d6f2f05364d791966adfd5a66466d990", i11);
        bnO = b11;
        arzArr[i11] = b11;
        int i12 = i11 + 1;
        C5663aRz b12 = C5640aRc.m17844b(Ship.class, "1394e5857a668e774a91cbcffd431fb2", i12);
        bnP = b12;
        arzArr[i12] = b12;
        int i13 = i12 + 1;
        C5663aRz b13 = C5640aRc.m17844b(Ship.class, "bc2836484f3b82dbccc5e35292d0021e", i13);
        bnR = b13;
        arzArr[i13] = b13;
        int i14 = i13 + 1;
        C5663aRz b14 = C5640aRc.m17844b(Ship.class, "8e759831b6b819a453787c2c334bfc98", i14);
        bnT = b14;
        arzArr[i14] = b14;
        int i15 = i14 + 1;
        C5663aRz b15 = C5640aRc.m17844b(Ship.class, "c0bf54703b959d66b2da7d00a53168c7", i15);
        bnU = b15;
        arzArr[i15] = b15;
        int i16 = i15 + 1;
        C5663aRz b16 = C5640aRc.m17844b(Ship.class, "60b44e37464206ba7cb7aa01c04aacb8", i16);
        bnV = b16;
        arzArr[i16] = b16;
        int i17 = i16 + 1;
        C5663aRz b17 = C5640aRc.m17844b(Ship.class, "4e786aee15d241bf6f5e178492e073a9", i17);
        bnX = b17;
        arzArr[i17] = b17;
        int i18 = i17 + 1;
        C5663aRz b18 = C5640aRc.m17844b(Ship.class, "703113558d172f6e6df6a8f27bc7a87f", i18);
        bnZ = b18;
        arzArr[i18] = b18;
        int i19 = i18 + 1;
        C5663aRz b19 = C5640aRc.m17844b(Ship.class, "f57039722771c290261270fc11ea215a", i19);
        bob = b19;
        arzArr[i19] = b19;
        int i20 = i19 + 1;
        C5663aRz b20 = C5640aRc.m17844b(Ship.class, "95a88713e9f15ed67f247d88fdeabe24", i20);
        bod = b20;
        arzArr[i20] = b20;
        int i21 = i20 + 1;
        C5663aRz b21 = C5640aRc.m17844b(Ship.class, "78d648ae28cc6e326c3d7766c839c604", i21);
        bof = b21;
        arzArr[i21] = b21;
        int i22 = i21 + 1;
        C5663aRz b22 = C5640aRc.m17844b(Ship.class, "77920c33a6f29cb25c6827ec067906b7", i22);
        f7125VB = b22;
        arzArr[i22] = b22;
        int i23 = i22 + 1;
        C5663aRz b23 = C5640aRc.m17844b(Ship.class, "d65771ff3efabbe9926ff6d0008a8282", i23);
        f7120LR = b23;
        arzArr[i23] = b23;
        int i24 = i23 + 1;
        C5663aRz b24 = C5640aRc.m17844b(Ship.class, "fe70592ffdee3463b6fc2a6ec4a2c282", i24);
        boi = b24;
        arzArr[i24] = b24;
        int i25 = i24 + 1;
        C5663aRz b25 = C5640aRc.m17844b(Ship.class, "436986ecb8ccee6ec5ede89815f949ed", i25);
        bok = b25;
        arzArr[i25] = b25;
        int i26 = i25 + 1;
        C5663aRz b26 = C5640aRc.m17844b(Ship.class, "09d79fe35c3692b7feeeae637bcac0c8", i26);
        f7145uU = b26;
        arzArr[i26] = b26;
        int i27 = i26 + 1;
        C5663aRz b27 = C5640aRc.m17844b(Ship.class, "5e8a70067774850a0ab59335dcf38c43", i27);
        bom = b27;
        arzArr[i27] = b27;
        int i28 = i27 + 1;
        C5663aRz b28 = C5640aRc.m17844b(Ship.class, "cda67b981bd6b81eddc11b146d278d4f", i28);
        boo = b28;
        arzArr[i28] = b28;
        int i29 = i28 + 1;
        C5663aRz b29 = C5640aRc.m17844b(Ship.class, "eaad92915612d97ae95413006a61691c", i29);
        boq = b29;
        arzArr[i29] = b29;
        int i30 = i29 + 1;
        C5663aRz b30 = C5640aRc.m17844b(Ship.class, "f9e840447561fabf74bee25bb8054b85", i30);
        bos = b30;
        arzArr[i30] = b30;
        int i31 = i30 + 1;
        C5663aRz b31 = C5640aRc.m17844b(Ship.class, "540b2fa017eb685368f950fc172fd658", i31);
        bot = b31;
        arzArr[i31] = b31;
        int i32 = i31 + 1;
        C5663aRz b32 = C5640aRc.m17844b(Ship.class, "76a2e596d427a588c043ad45d40b00ac", i32);
        bou = b32;
        arzArr[i32] = b32;
        int i33 = i32 + 1;
        C5663aRz b33 = C5640aRc.m17844b(Ship.class, "6bdc7d72f8bbf3f16b2963456b47fc8e", i33);
        aMi = b33;
        arzArr[i33] = b33;
        int i34 = i33 + 1;
        C5663aRz b34 = C5640aRc.m17844b(Ship.class, "cf25afcf2d302c59983c9b7d46d6dd04", i34);
        bow = b34;
        arzArr[i34] = b34;
        int i35 = i34 + 1;
        C5663aRz b35 = C5640aRc.m17844b(Ship.class, "15f0692836c7328dd4fd758c7ac6fb58", i35);
        aOx = b35;
        arzArr[i35] = b35;
        int i36 = i35 + 1;
        C5663aRz b36 = C5640aRc.m17844b(Ship.class, "8559899c23517ad1c07a27c42c27859c", i36);
        boz = b36;
        arzArr[i36] = b36;
        int i37 = i36 + 1;
        C5663aRz b37 = C5640aRc.m17844b(Ship.class, "ce62fd24fad44a0c6c2bd67edb82fd22", i37);
        f7127VW = b37;
        arzArr[i37] = b37;
        int i38 = i37 + 1;
        C5663aRz b38 = C5640aRc.m17844b(Ship.class, "6307cbd895f869dbf6b3c6c2941678f1", i38);
        boD = b38;
        arzArr[i38] = b38;
        int i39 = i38 + 1;
        C5663aRz b39 = C5640aRc.m17844b(Ship.class, "c79cf2d18597b17dafdce6345a115078", i39);
        boF = b39;
        arzArr[i39] = b39;
        int i40 = i39 + 1;
        C5663aRz b40 = C5640aRc.m17844b(Ship.class, "67a214c3f2bb3da8ce4cd82c57fa1d32", i40);
        boI = b40;
        arzArr[i40] = b40;
        int i41 = i40 + 1;
        C5663aRz b41 = C5640aRc.m17844b(Ship.class, "b6f3e2426c4db92cb5c518db4dde10f4", i41);
        boK = b41;
        arzArr[i41] = b41;
        int i42 = i41 + 1;
        C5663aRz b42 = C5640aRc.m17844b(Ship.class, "ff062fb3e7385f02b8dc783cc72aa5dd", i42);
        boM = b42;
        arzArr[i42] = b42;
        int i43 = i42 + 1;
        C5663aRz b43 = C5640aRc.m17844b(Ship.class, "f94501d21485a328fc44f085e8f959ce", i43);
        boO = b43;
        arzArr[i43] = b43;
        int i44 = i43 + 1;
        C5663aRz b44 = C5640aRc.m17844b(Ship.class, "2d1b3508f6de8b3fb6bc8b7f77f1bc82", i44);
        bid = b44;
        arzArr[i44] = b44;
        int i45 = i44 + 1;
        C5663aRz b45 = C5640aRc.m17844b(Ship.class, "4f6ed4c9369bb645b8d4d8fb201084d2", i45);
        boP = b45;
        arzArr[i45] = b45;
        int i46 = i45 + 1;
        C5663aRz b46 = C5640aRc.m17844b(Ship.class, "5eaa252d59d9bdd4ae9bdf5049e6aebd", i46);
        boS = b46;
        arzArr[i46] = b46;
        int i47 = i46 + 1;
        C5663aRz b47 = C5640aRc.m17844b(Ship.class, "f7973441f80aa15f5856ac20a967e2e2", i47);
        boU = b47;
        arzArr[i47] = b47;
        int i48 = i47 + 1;
        C5663aRz b48 = C5640aRc.m17844b(Ship.class, "7729e8215e41642c9f9c1cf2db254825", i48);
        boW = b48;
        arzArr[i48] = b48;
        int i49 = i48 + 1;
        C5663aRz b49 = C5640aRc.m17844b(Ship.class, "f9350cce5941de23e828745965eb122e", i49);
        bpb = b49;
        arzArr[i49] = b49;
        int i50 = i49 + 1;
        C5663aRz b50 = C5640aRc.m17844b(Ship.class, "955302f0b2669d8f3fc357b44eb4d71b", i50);
        bpd = b50;
        arzArr[i50] = b50;
        int i51 = i50 + 1;
        C5663aRz b51 = C5640aRc.m17844b(Ship.class, "95f9f1daff345868b5c0f0a6e0e9ce34", i51);
        bph = b51;
        arzArr[i51] = b51;
        int i52 = i51 + 1;
        C5663aRz b52 = C5640aRc.m17844b(Ship.class, "2283a964f9c6533a51ded6e38d40a0e4", i52);
        bpj = b52;
        arzArr[i52] = b52;
        int i53 = i52 + 1;
        C5663aRz b53 = C5640aRc.m17844b(Ship.class, "a36555ac5f2f582ce9600a99ade722ea", i53);
        bpl = b53;
        arzArr[i53] = b53;
        int i54 = i53 + 1;
        C5663aRz b54 = C5640aRc.m17844b(Ship.class, "04de02b87e1ef104769337bf29e98626", i54);
        bpn = b54;
        arzArr[i54] = b54;
        int i55 = i54 + 1;
        C5663aRz b55 = C5640aRc.m17844b(Ship.class, "2f73c026196fec0b941468ccfd34fa6b", i55);
        bpq = b55;
        arzArr[i55] = b55;
        int i56 = i55 + 1;
        _m_fields = arzArr;
        C1634Xv.m11725a((Object[]) Pawn._m_fields, (Object[]) _m_fields);
        int i57 = Pawn._m_methodCount;
        C2491fm[] fmVarArr = new C2491fm[(i57 + KeyCode.csI)];
        C2491fm a = C4105zY.m41624a(Ship.class, "694a61d732bfd92e4cf044ade7563b28", i57);
        bkd = a;
        fmVarArr[i57] = a;
        int i58 = i57 + 1;
        C2491fm a2 = C4105zY.m41624a(Ship.class, "147a1277d4bfc60af0d5f7d4b834c583", i58);
        bpr = a2;
        fmVarArr[i58] = a2;
        int i59 = i58 + 1;
        C2491fm a3 = C4105zY.m41624a(Ship.class, "42ba94691fd3856bd32dffab0a906a1f", i59);
        bka = a3;
        fmVarArr[i59] = a3;
        int i60 = i59 + 1;
        C2491fm a4 = C4105zY.m41624a(Ship.class, "6188c6747357ea4b691cb799af7b86e3", i60);
        bps = a4;
        fmVarArr[i60] = a4;
        int i61 = i60 + 1;
        C2491fm a5 = C4105zY.m41624a(Ship.class, "27b82832b7d0f91fb23b78ad6b12e9ec", i61);
        bpt = a5;
        fmVarArr[i61] = a5;
        int i62 = i61 + 1;
        C2491fm a6 = C4105zY.m41624a(Ship.class, "b9ac07c9fbe2d57b863b08578fee36c8", i62);
        bpu = a6;
        fmVarArr[i62] = a6;
        int i63 = i62 + 1;
        C2491fm a7 = C4105zY.m41624a(Ship.class, "46b08a3ffa759a8ba3bbfa27a3f9d4d9", i63);
        bpv = a7;
        fmVarArr[i63] = a7;
        int i64 = i63 + 1;
        C2491fm a8 = C4105zY.m41624a(Ship.class, "967a2a2a2d128c04bdcfa8daa8f0cead", i64);
        bpw = a8;
        fmVarArr[i64] = a8;
        int i65 = i64 + 1;
        C2491fm a9 = C4105zY.m41624a(Ship.class, "8367e7183549ef9ef678b19dfedc493d", i65);
        bpx = a9;
        fmVarArr[i65] = a9;
        int i66 = i65 + 1;
        C2491fm a10 = C4105zY.m41624a(Ship.class, "ab38f02cea8ea026f18621dffd23ed2e", i66);
        bpy = a10;
        fmVarArr[i66] = a10;
        int i67 = i66 + 1;
        C2491fm a11 = C4105zY.m41624a(Ship.class, C3981xi.bgb, i67);
        aRm = a11;
        fmVarArr[i67] = a11;
        int i68 = i67 + 1;
        C2491fm a12 = C4105zY.m41624a(Ship.class, "509421a788c0d62bf9d20d8e178c8b27", i68);
        f7152vm = a12;
        fmVarArr[i68] = a12;
        int i69 = i68 + 1;
        C2491fm a13 = C4105zY.m41624a(Ship.class, "605164dad796f14c92f162af56da1335", i69);
        bpz = a13;
        fmVarArr[i69] = a13;
        int i70 = i69 + 1;
        C2491fm a14 = C4105zY.m41624a(Ship.class, "0b2594f3bc5acba031e769203ca41dc4", i70);
        bpA = a14;
        fmVarArr[i70] = a14;
        int i71 = i70 + 1;
        C2491fm a15 = C4105zY.m41624a(Ship.class, "83b89b2893fc7ad3bbd5299cfbc08cf3", i71);
        bpB = a15;
        fmVarArr[i71] = a15;
        int i72 = i71 + 1;
        C2491fm a16 = C4105zY.m41624a(Ship.class, "1a1f04ff74c14f26d83088aebd9b6b24", i72);
        aPe = a16;
        fmVarArr[i72] = a16;
        int i73 = i72 + 1;
        C2491fm a17 = C4105zY.m41624a(Ship.class, "8109f47716990ad6cc559649f3da481f", i73);
        aPl = a17;
        fmVarArr[i73] = a17;
        int i74 = i73 + 1;
        C2491fm a18 = C4105zY.m41624a(Ship.class, "192d108ce9ac199d56c8f044e253a95e", i74);
        f7157vs = a18;
        fmVarArr[i74] = a18;
        int i75 = i74 + 1;
        C2491fm a19 = C4105zY.m41624a(Ship.class, "cc3e45febbf5feb8cdbf3357051a7c46", i75);
        bpC = a19;
        fmVarArr[i75] = a19;
        int i76 = i75 + 1;
        C2491fm a20 = C4105zY.m41624a(Ship.class, "347ee5d227d27aa29c2c1f370355faed", i76);
        bpD = a20;
        fmVarArr[i76] = a20;
        int i77 = i76 + 1;
        C2491fm a21 = C4105zY.m41624a(Ship.class, "1f2e1038490feb3bdd4ddb831a0d27da", i77);
        f7147uY = a21;
        fmVarArr[i77] = a21;
        int i78 = i77 + 1;
        C2491fm a22 = C4105zY.m41624a(Ship.class, "34989062d48bbb4773dd25db8c41ead7", i78);
        bpE = a22;
        fmVarArr[i78] = a22;
        int i79 = i78 + 1;
        C2491fm a23 = C4105zY.m41624a(Ship.class, "b7e318a2771ec66628dd50ee9410c688", i79);
        bpF = a23;
        fmVarArr[i79] = a23;
        int i80 = i79 + 1;
        C2491fm a24 = C4105zY.m41624a(Ship.class, "9d0d2b502467b41ea987d3170f13d8c0", i80);
        f7153vn = a24;
        fmVarArr[i80] = a24;
        int i81 = i80 + 1;
        C2491fm a25 = C4105zY.m41624a(Ship.class, "c38b1e10b2794240396f6cff959d1d6a", i81);
        bpG = a25;
        fmVarArr[i81] = a25;
        int i82 = i81 + 1;
        C2491fm a26 = C4105zY.m41624a(Ship.class, "811b78f29daf44ae2534669f66e7bba9", i82);
        bpH = a26;
        fmVarArr[i82] = a26;
        int i83 = i82 + 1;
        C2491fm a27 = C4105zY.m41624a(Ship.class, "3662e634d7e2bd9bc23f623489dea0ec", i83);
        _f_dispose_0020_0028_0029V = a27;
        fmVarArr[i83] = a27;
        int i84 = i83 + 1;
        C2491fm a28 = C4105zY.m41624a(Ship.class, "51f404e7c6d89d312578ff37827865d2", i84);
        aPf = a28;
        fmVarArr[i84] = a28;
        int i85 = i84 + 1;
        C2491fm a29 = C4105zY.m41624a(Ship.class, "c1bb90bce63a7c4d3e337fe3e896e576", i85);
        bpI = a29;
        fmVarArr[i85] = a29;
        int i86 = i85 + 1;
        C2491fm a30 = C4105zY.m41624a(Ship.class, "4291fb73c5822f71b664106271041a7c", i86);
        bpJ = a30;
        fmVarArr[i86] = a30;
        int i87 = i86 + 1;
        C2491fm a31 = C4105zY.m41624a(Ship.class, "a401a453214ae257f7c751dedc8a01d3", i87);
        bbU = a31;
        fmVarArr[i87] = a31;
        int i88 = i87 + 1;
        C2491fm a32 = C4105zY.m41624a(Ship.class, "709f96c845755ad928fc5301838afaad", i88);
        bpK = a32;
        fmVarArr[i88] = a32;
        int i89 = i88 + 1;
        C2491fm a33 = C4105zY.m41624a(Ship.class, "f7bb10bed848c230f8726d4d943b2358", i89);
        bpL = a33;
        fmVarArr[i89] = a33;
        int i90 = i89 + 1;
        C2491fm a34 = C4105zY.m41624a(Ship.class, "92bc47b94baf638e7ce1ea47693bf045", i90);
        bpM = a34;
        fmVarArr[i90] = a34;
        int i91 = i90 + 1;
        C2491fm a35 = C4105zY.m41624a(Ship.class, "471482215affb6008987432e94a975ad", i91);
        bpN = a35;
        fmVarArr[i91] = a35;
        int i92 = i91 + 1;
        C2491fm a36 = C4105zY.m41624a(Ship.class, "61910bbe36f198c9e0998dc172a4ec87", i92);
        bpO = a36;
        fmVarArr[i92] = a36;
        int i93 = i92 + 1;
        C2491fm a37 = C4105zY.m41624a(Ship.class, "929fcd9af0dc7f624226cd94317938b4", i93);
        bpP = a37;
        fmVarArr[i93] = a37;
        int i94 = i93 + 1;
        C2491fm a38 = C4105zY.m41624a(Ship.class, "13461c231e510a786ef4e77986cf886f", i94);
        f7158yg = a38;
        fmVarArr[i94] = a38;
        int i95 = i94 + 1;
        C2491fm a39 = C4105zY.m41624a(Ship.class, "a8fb832a14995407252e62d086fa3760", i95);
        bpQ = a39;
        fmVarArr[i95] = a39;
        int i96 = i95 + 1;
        C2491fm a40 = C4105zY.m41624a(Ship.class, "d783f5a614eb7f526844d8698e9d6808", i96);
        f7135Wl = a40;
        fmVarArr[i96] = a40;
        int i97 = i96 + 1;
        C2491fm a41 = C4105zY.m41624a(Ship.class, "ee7effea2745205d535048623ec7f04d", i97);
        aTe = a41;
        fmVarArr[i97] = a41;
        int i98 = i97 + 1;
        C2491fm a42 = C4105zY.m41624a(Ship.class, "f7d9ac177fdd45962ee11383bc4e9318", i98);
        aTf = a42;
        fmVarArr[i98] = a42;
        int i99 = i98 + 1;
        C2491fm a43 = C4105zY.m41624a(Ship.class, "7ff404dec002c10c5ce0e0b7c898a1e0", i99);
        bpR = a43;
        fmVarArr[i99] = a43;
        int i100 = i99 + 1;
        C2491fm a44 = C4105zY.m41624a(Ship.class, "e107c60c246e5e87e76cbe9114769b5a", i100);
        bpS = a44;
        fmVarArr[i100] = a44;
        int i101 = i100 + 1;
        C2491fm a45 = C4105zY.m41624a(Ship.class, "d97ac40a6c21bd6433ba304d7c870a73", i101);
        bpT = a45;
        fmVarArr[i101] = a45;
        int i102 = i101 + 1;
        C2491fm a46 = C4105zY.m41624a(Ship.class, "b756d2d88d5acdd054c450b6d3c76bbd", i102);
        bpU = a46;
        fmVarArr[i102] = a46;
        int i103 = i102 + 1;
        C2491fm a47 = C4105zY.m41624a(Ship.class, "6f9573c0e92bef054db1d90546754f33", i103);
        aTg = a47;
        fmVarArr[i103] = a47;
        int i104 = i103 + 1;
        C2491fm a48 = C4105zY.m41624a(Ship.class, "05c2de41e21476f460c07580c2c4c01c", i104);
        aTh = a48;
        fmVarArr[i104] = a48;
        int i105 = i104 + 1;
        C2491fm a49 = C4105zY.m41624a(Ship.class, "3b19e838b5a53d7715b611d220ee058e", i105);
        bpV = a49;
        fmVarArr[i105] = a49;
        int i106 = i105 + 1;
        C2491fm a50 = C4105zY.m41624a(Ship.class, "d23c45038a915bd772fe4e48950bdf8a", i106);
        bpW = a50;
        fmVarArr[i106] = a50;
        int i107 = i106 + 1;
        C2491fm a51 = C4105zY.m41624a(Ship.class, "947c57c19204dec1a2b100509f0745b6", i107);
        bpX = a51;
        fmVarArr[i107] = a51;
        int i108 = i107 + 1;
        C2491fm a52 = C4105zY.m41624a(Ship.class, "5d0993e086fd1481b49a22e91f5f3fc8", i108);
        bpY = a52;
        fmVarArr[i108] = a52;
        int i109 = i108 + 1;
        C2491fm a53 = C4105zY.m41624a(Ship.class, "0f2280ced41e37e936940aef477fa128", i109);
        bpZ = a53;
        fmVarArr[i109] = a53;
        int i110 = i109 + 1;
        C2491fm a54 = C4105zY.m41624a(Ship.class, "714af79623d733cd15883830bb6ad929", i110);
        f7149vc = a54;
        fmVarArr[i110] = a54;
        int i111 = i110 + 1;
        C2491fm a55 = C4105zY.m41624a(Ship.class, "b0cad9e851282f9199a43bb4e733947d", i111);
        f7150ve = a55;
        fmVarArr[i111] = a55;
        int i112 = i111 + 1;
        C2491fm a56 = C4105zY.m41624a(Ship.class, "969f26f8005f0e2a035158760f063795", i112);
        f7154vo = a56;
        fmVarArr[i112] = a56;
        int i113 = i112 + 1;
        C2491fm a57 = C4105zY.m41624a(Ship.class, "40d7ee933bc57b7648c45d9222e2f4d4", i113);
        f7136Wm = a57;
        fmVarArr[i113] = a57;
        int i114 = i113 + 1;
        C2491fm a58 = C4105zY.m41624a(Ship.class, "2a2d688065c50a493c0f3d57f9dac277", i114);
        bqa = a58;
        fmVarArr[i114] = a58;
        int i115 = i114 + 1;
        C2491fm a59 = C4105zY.m41624a(Ship.class, "af67e17f054b596251b42f538f2afb64", i115);
        bqb = a59;
        fmVarArr[i115] = a59;
        int i116 = i115 + 1;
        C2491fm a60 = C4105zY.m41624a(Ship.class, "11c6bb7122e86401f3008257740d7de8", i116);
        bqc = a60;
        fmVarArr[i116] = a60;
        int i117 = i116 + 1;
        C2491fm a61 = C4105zY.m41624a(Ship.class, "cc91ef92aee4fd4d8255cf608d70ff29", i117);
        bqd = a61;
        fmVarArr[i117] = a61;
        int i118 = i117 + 1;
        C2491fm a62 = C4105zY.m41624a(Ship.class, "a06c15239329ee538e3052df836aec81", i118);
        bqe = a62;
        fmVarArr[i118] = a62;
        int i119 = i118 + 1;
        C2491fm a63 = C4105zY.m41624a(Ship.class, "b62640136c117bb30ffa4945a9afb240", i119);
        bkk = a63;
        fmVarArr[i119] = a63;
        int i120 = i119 + 1;
        C2491fm a64 = C4105zY.m41624a(Ship.class, "4d6cd5a2445d0eb2a93ce931f99f0d39", i120);
        bqf = a64;
        fmVarArr[i120] = a64;
        int i121 = i120 + 1;
        C2491fm a65 = C4105zY.m41624a(Ship.class, "2b7f9efeeb2f9e88c18050b9339e1acb", i121);
        f7151vi = a65;
        fmVarArr[i121] = a65;
        int i122 = i121 + 1;
        C2491fm a66 = C4105zY.m41624a(Ship.class, "89cbaf46b58f1be9e0d94122fbec7313", i122);
        bqg = a66;
        fmVarArr[i122] = a66;
        int i123 = i122 + 1;
        C2491fm a67 = C4105zY.m41624a(Ship.class, "fc436d2830185ec4079feb381a1b9083", i123);
        bqh = a67;
        fmVarArr[i123] = a67;
        int i124 = i123 + 1;
        C2491fm a68 = C4105zY.m41624a(Ship.class, "896175fa229632d8d92b3a58b2568745", i124);
        bqi = a68;
        fmVarArr[i124] = a68;
        int i125 = i124 + 1;
        C2491fm a69 = C4105zY.m41624a(Ship.class, "fb71d87bd17fc6cc77df7230b0c38028", i125);
        bqj = a69;
        fmVarArr[i125] = a69;
        int i126 = i125 + 1;
        C2491fm a70 = C4105zY.m41624a(Ship.class, "da200beda84e8e888d295dcedbddb9d7", i126);
        bqk = a70;
        fmVarArr[i126] = a70;
        int i127 = i126 + 1;
        C2491fm a71 = C4105zY.m41624a(Ship.class, "ceb7fee37ddc7d49a45e91b1c6824cd5", i127);
        bbS = a71;
        fmVarArr[i127] = a71;
        int i128 = i127 + 1;
        C2491fm a72 = C4105zY.m41624a(Ship.class, "1354f41b5ae79bc3b64d8435d7cb68fc", i128);
        bql = a72;
        fmVarArr[i128] = a72;
        int i129 = i128 + 1;
        C2491fm a73 = C4105zY.m41624a(Ship.class, "0d0d49c2e76bfa3d6fc2bec0e63e6d96", i129);
        bqm = a73;
        fmVarArr[i129] = a73;
        int i130 = i129 + 1;
        C2491fm a74 = C4105zY.m41624a(Ship.class, "ed74e1594bf493d7972378246eb30608", i130);
        bqn = a74;
        fmVarArr[i130] = a74;
        int i131 = i130 + 1;
        C2491fm a75 = C4105zY.m41624a(Ship.class, "7b26bc74ab22ccfad7f0d8ac70673d1d", i131);
        bqo = a75;
        fmVarArr[i131] = a75;
        int i132 = i131 + 1;
        C2491fm a76 = C4105zY.m41624a(Ship.class, "9a372301d7626e6cf21ce9d2744a410d", i132);
        bqp = a76;
        fmVarArr[i132] = a76;
        int i133 = i132 + 1;
        C2491fm a77 = C4105zY.m41624a(Ship.class, "9738ed635515eaabd6b88486b1132133", i133);
        bqq = a77;
        fmVarArr[i133] = a77;
        int i134 = i133 + 1;
        C2491fm a78 = C4105zY.m41624a(Ship.class, "839b981b79653d74d62ce8f6c1665bcf", i134);
        bqr = a78;
        fmVarArr[i134] = a78;
        int i135 = i134 + 1;
        C2491fm a79 = C4105zY.m41624a(Ship.class, "f63875537e50642668c25fcaaced83e7", i135);
        bqs = a79;
        fmVarArr[i135] = a79;
        int i136 = i135 + 1;
        C2491fm a80 = C4105zY.m41624a(Ship.class, "7d74f60cb5ffd4bf24f9bc5700698a8f", i136);
        bqt = a80;
        fmVarArr[i136] = a80;
        int i137 = i136 + 1;
        C2491fm a81 = C4105zY.m41624a(Ship.class, "90e1e1684603f138db2655c9186b8d63", i137);
        bqu = a81;
        fmVarArr[i137] = a81;
        int i138 = i137 + 1;
        C2491fm a82 = C4105zY.m41624a(Ship.class, "bd3d5187110bb88d7fbb12a5fe9871fa", i138);
        bqv = a82;
        fmVarArr[i138] = a82;
        int i139 = i138 + 1;
        C2491fm a83 = C4105zY.m41624a(Ship.class, "42b181bb65a527169083a909646f34c9", i139);
        bqw = a83;
        fmVarArr[i139] = a83;
        int i140 = i139 + 1;
        C2491fm a84 = C4105zY.m41624a(Ship.class, "81cab29d4f08710bbb4b300201a60017", i140);
        bqx = a84;
        fmVarArr[i140] = a84;
        int i141 = i140 + 1;
        C2491fm a85 = C4105zY.m41624a(Ship.class, "765eb35e001ae453344cf84c59266ff9", i141);
        bqy = a85;
        fmVarArr[i141] = a85;
        int i142 = i141 + 1;
        C2491fm a86 = C4105zY.m41624a(Ship.class, "db1de72988be33e13d3d4c0e23071c0e", i142);
        bqz = a86;
        fmVarArr[i142] = a86;
        int i143 = i142 + 1;
        C2491fm a87 = C4105zY.m41624a(Ship.class, "8515dceed6fec1b1db59419eb38399f6", i143);
        bqA = a87;
        fmVarArr[i143] = a87;
        int i144 = i143 + 1;
        C2491fm a88 = C4105zY.m41624a(Ship.class, "6d799dc572ff4efa279d24e2c0821059", i144);
        bqB = a88;
        fmVarArr[i144] = a88;
        int i145 = i144 + 1;
        C2491fm a89 = C4105zY.m41624a(Ship.class, "acb917dd9c1688e5e2ac2a94bb0a3c42", i145);
        bqC = a89;
        fmVarArr[i145] = a89;
        int i146 = i145 + 1;
        C2491fm a90 = C4105zY.m41624a(Ship.class, "e2d3dfa707b568403d092ca46c5fccb1", i146);
        bqD = a90;
        fmVarArr[i146] = a90;
        int i147 = i146 + 1;
        C2491fm a91 = C4105zY.m41624a(Ship.class, "42afde475128130bdd22a21f5df24067", i147);
        bqE = a91;
        fmVarArr[i147] = a91;
        int i148 = i147 + 1;
        C2491fm a92 = C4105zY.m41624a(Ship.class, "28133fdae892b8b2336f63a0900d4c7f", i148);
        f7146uX = a92;
        fmVarArr[i148] = a92;
        int i149 = i148 + 1;
        C2491fm a93 = C4105zY.m41624a(Ship.class, "0aa40f18d38f3e5abe68d79ec23a1e57", i149);
        bqF = a93;
        fmVarArr[i149] = a93;
        int i150 = i149 + 1;
        C2491fm a94 = C4105zY.m41624a(Ship.class, "bea28b66997d47a230b6c824d219e56a", i150);
        bqG = a94;
        fmVarArr[i150] = a94;
        int i151 = i150 + 1;
        C2491fm a95 = C4105zY.m41624a(Ship.class, "da357158998b6d201ae957543e334333", i151);
        bqH = a95;
        fmVarArr[i151] = a95;
        int i152 = i151 + 1;
        C2491fm a96 = C4105zY.m41624a(Ship.class, "49bdca2786c8edf8819274a0171854bb", i152);
        bqI = a96;
        fmVarArr[i152] = a96;
        int i153 = i152 + 1;
        C2491fm a97 = C4105zY.m41624a(Ship.class, "8ef4f7dca16306dbc958c1a6889d1768", i153);
        f7121Lj = a97;
        fmVarArr[i153] = a97;
        int i154 = i153 + 1;
        C2491fm a98 = C4105zY.m41624a(Ship.class, "875b28b87fff3ab69db17c478a3f57ca", i154);
        bqJ = a98;
        fmVarArr[i154] = a98;
        int i155 = i154 + 1;
        C2491fm a99 = C4105zY.m41624a(Ship.class, "670fabbecd7023a644e569ab66c686a5", i155);
        bqK = a99;
        fmVarArr[i155] = a99;
        int i156 = i155 + 1;
        C2491fm a100 = C4105zY.m41624a(Ship.class, "92f49336f6d25254c7988b65363d0ae2", i156);
        f7134Wk = a100;
        fmVarArr[i156] = a100;
        int i157 = i156 + 1;
        C2491fm a101 = C4105zY.m41624a(Ship.class, "8211af2b6d0265eaae7ee115352e17c0", i157);
        bqL = a101;
        fmVarArr[i157] = a101;
        int i158 = i157 + 1;
        C2491fm a102 = C4105zY.m41624a(Ship.class, "5df9dd3c795856e579470eadc56e72cd", i158);
        f7118Do = a102;
        fmVarArr[i158] = a102;
        int i159 = i158 + 1;
        C2491fm a103 = C4105zY.m41624a(Ship.class, "33dbff76f10f6a89ddda928890ac3152", i159);
        bqM = a103;
        fmVarArr[i159] = a103;
        int i160 = i159 + 1;
        C2491fm a104 = C4105zY.m41624a(Ship.class, "8a6cb07ccb1fd2e583e35b12a19ec2e5", i160);
        f7156vr = a104;
        fmVarArr[i160] = a104;
        int i161 = i160 + 1;
        C2491fm a105 = C4105zY.m41624a(Ship.class, "a1a2139bd11090adc0522ee23f4a74e7", i161);
        bqN = a105;
        fmVarArr[i161] = a105;
        int i162 = i161 + 1;
        C2491fm a106 = C4105zY.m41624a(Ship.class, "536919a01efd0c5712a3c0e6929e688b", i162);
        bqO = a106;
        fmVarArr[i162] = a106;
        int i163 = i162 + 1;
        C2491fm a107 = C4105zY.m41624a(Ship.class, "36dc437d1c4a3798a35cedaa199f0088", i163);
        f7116CB = a107;
        fmVarArr[i163] = a107;
        int i164 = i163 + 1;
        C2491fm a108 = C4105zY.m41624a(Ship.class, "9e486d4f9295a03707270a63faa1f514", i164);
        bqP = a108;
        fmVarArr[i164] = a108;
        int i165 = i164 + 1;
        C2491fm a109 = C4105zY.m41624a(Ship.class, "9ef08f55c36d2fa219a02bf4a92a8e39", i165);
        bqQ = a109;
        fmVarArr[i165] = a109;
        int i166 = i165 + 1;
        C2491fm a110 = C4105zY.m41624a(Ship.class, "2f4aa5e6fd66624b1830dbb245d4c5de", i166);
        f7122Lm = a110;
        fmVarArr[i166] = a110;
        int i167 = i166 + 1;
        C2491fm a111 = C4105zY.m41624a(Ship.class, "a174e96a7ef5faa46590e861c5e0c344", i167);
        bqR = a111;
        fmVarArr[i167] = a111;
        int i168 = i167 + 1;
        C2491fm a112 = C4105zY.m41624a(Ship.class, "c30b3be54f5d9039eefd4e74454e7721", i168);
        bqS = a112;
        fmVarArr[i168] = a112;
        int i169 = i168 + 1;
        C2491fm a113 = C4105zY.m41624a(Ship.class, "0e8c108fb7b35aa2197ccfa3da3e4c90", i169);
        bqT = a113;
        fmVarArr[i169] = a113;
        int i170 = i169 + 1;
        C2491fm a114 = C4105zY.m41624a(Ship.class, "00b3ada660914d80a7dcc16503bc5ff7", i170);
        bqU = a114;
        fmVarArr[i170] = a114;
        int i171 = i170 + 1;
        C2491fm a115 = C4105zY.m41624a(Ship.class, "5c6b59bd06afc4d40e923175f2f39346", i171);
        bqV = a115;
        fmVarArr[i171] = a115;
        int i172 = i171 + 1;
        C2491fm a116 = C4105zY.m41624a(Ship.class, "65a4d987e756f537044318fdd0e8c1db", i172);
        bqW = a116;
        fmVarArr[i172] = a116;
        int i173 = i172 + 1;
        C2491fm a117 = C4105zY.m41624a(Ship.class, "330064503b43a8806b416e145c223d2e", i173);
        bqX = a117;
        fmVarArr[i173] = a117;
        int i174 = i173 + 1;
        C2491fm a118 = C4105zY.m41624a(Ship.class, "6c7a58f824cb8663953ec85d67305ba2", i174);
        bqY = a118;
        fmVarArr[i174] = a118;
        int i175 = i174 + 1;
        C2491fm a119 = C4105zY.m41624a(Ship.class, "1417d72719cf07e0306fa5b260d85d68", i175);
        bqZ = a119;
        fmVarArr[i175] = a119;
        int i176 = i175 + 1;
        C2491fm a120 = C4105zY.m41624a(Ship.class, "4e1d3227d6e63d494c3f0b5521bc6a18", i176);
        bra = a120;
        fmVarArr[i176] = a120;
        int i177 = i176 + 1;
        C2491fm a121 = C4105zY.m41624a(Ship.class, "839eb722d120ef84b0060d0d83f6c1de", i177);
        brb = a121;
        fmVarArr[i177] = a121;
        int i178 = i177 + 1;
        C2491fm a122 = C4105zY.m41624a(Ship.class, "15676101e12d6b0220d6bfc616679471", i178);
        brc = a122;
        fmVarArr[i178] = a122;
        int i179 = i178 + 1;
        C2491fm a123 = C4105zY.m41624a(Ship.class, "81c8132e6b364ac2eb725f009d4b8a0c", i179);
        brd = a123;
        fmVarArr[i179] = a123;
        int i180 = i179 + 1;
        C2491fm a124 = C4105zY.m41624a(Ship.class, "d00e363bf9634e100f5cf46f87314d3c", i180);
        bre = a124;
        fmVarArr[i180] = a124;
        int i181 = i180 + 1;
        C2491fm a125 = C4105zY.m41624a(Ship.class, "c4cbf98aad48fa51da90822b75324e12", i181);
        brf = a125;
        fmVarArr[i181] = a125;
        int i182 = i181 + 1;
        C2491fm a126 = C4105zY.m41624a(Ship.class, "3c0a00c10abe0dfe870ddf61e24c4113", i182);
        brg = a126;
        fmVarArr[i182] = a126;
        int i183 = i182 + 1;
        C2491fm a127 = C4105zY.m41624a(Ship.class, "eaa7892d4368e1dc7f94b8359d69bb37", i183);
        brh = a127;
        fmVarArr[i183] = a127;
        int i184 = i183 + 1;
        C2491fm a128 = C4105zY.m41624a(Ship.class, "bcffe353e34b6e18c46db26d441e36f8", i184);
        bri = a128;
        fmVarArr[i184] = a128;
        int i185 = i184 + 1;
        C2491fm a129 = C4105zY.m41624a(Ship.class, "acea22c22dd3eb16c0f523f288b37ad5", i185);
        brj = a129;
        fmVarArr[i185] = a129;
        int i186 = i185 + 1;
        C2491fm a130 = C4105zY.m41624a(Ship.class, "b7869876dcef485bceacb1feea925195", i186);
        brk = a130;
        fmVarArr[i186] = a130;
        int i187 = i186 + 1;
        C2491fm a131 = C4105zY.m41624a(Ship.class, "1ef70b074203a6d2ddd91d9079e4c388", i187);
        brl = a131;
        fmVarArr[i187] = a131;
        int i188 = i187 + 1;
        C2491fm a132 = C4105zY.m41624a(Ship.class, "05e58d90f297509ed6464a706c7fecb9", i188);
        brm = a132;
        fmVarArr[i188] = a132;
        int i189 = i188 + 1;
        C2491fm a133 = C4105zY.m41624a(Ship.class, "855d8f4fbdb9ab07d8a579f8d43e655e", i189);
        brn = a133;
        fmVarArr[i189] = a133;
        int i190 = i189 + 1;
        C2491fm a134 = C4105zY.m41624a(Ship.class, "64cdd80fef1ce2f72256968ecfad9f6f", i190);
        bro = a134;
        fmVarArr[i190] = a134;
        int i191 = i190 + 1;
        C2491fm a135 = C4105zY.m41624a(Ship.class, "3bd5385f10bf00485cbc2feda1e63f40", i191);
        brp = a135;
        fmVarArr[i191] = a135;
        int i192 = i191 + 1;
        C2491fm a136 = C4105zY.m41624a(Ship.class, "675a4db910d0b97d9aa4825f437de2ca", i192);
        brq = a136;
        fmVarArr[i192] = a136;
        int i193 = i192 + 1;
        C2491fm a137 = C4105zY.m41624a(Ship.class, "59932d65cfbe8ee6f4a1ec38de74ffe9", i193);
        brr = a137;
        fmVarArr[i193] = a137;
        int i194 = i193 + 1;
        C2491fm a138 = C4105zY.m41624a(Ship.class, "5a9977285474fec34b013c32cc396fff", i194);
        brs = a138;
        fmVarArr[i194] = a138;
        int i195 = i194 + 1;
        C2491fm a139 = C4105zY.m41624a(Ship.class, "f8b48fe8237fc3f985f7afc60c6e17da", i195);
        brt = a139;
        fmVarArr[i195] = a139;
        int i196 = i195 + 1;
        C2491fm a140 = C4105zY.m41624a(Ship.class, "a9eb996862832f06b36a866340a6bf5c", i196);
        bru = a140;
        fmVarArr[i196] = a140;
        int i197 = i196 + 1;
        C2491fm a141 = C4105zY.m41624a(Ship.class, "c96d0f6f8648f6a6b2f3eddc9f5a0818", i197);
        brv = a141;
        fmVarArr[i197] = a141;
        int i198 = i197 + 1;
        C2491fm a142 = C4105zY.m41624a(Ship.class, "553d50399bce64694d9d7506a6332f64", i198);
        brw = a142;
        fmVarArr[i198] = a142;
        int i199 = i198 + 1;
        C2491fm a143 = C4105zY.m41624a(Ship.class, "54a6e0f54c5c0349210c1ce387a02781", i199);
        brx = a143;
        fmVarArr[i199] = a143;
        int i200 = i199 + 1;
        C2491fm a144 = C4105zY.m41624a(Ship.class, "01ee1a74f27364abac5b45ecd0573ab8", i200);
        bry = a144;
        fmVarArr[i200] = a144;
        int i201 = i200 + 1;
        C2491fm a145 = C4105zY.m41624a(Ship.class, "2420de6252fefdedc9076a4015478058", i201);
        brz = a145;
        fmVarArr[i201] = a145;
        int i202 = i201 + 1;
        C2491fm a146 = C4105zY.m41624a(Ship.class, "48f39407a2535b41496d9abdeeb85bd0", i202);
        brA = a146;
        fmVarArr[i202] = a146;
        int i203 = i202 + 1;
        C2491fm a147 = C4105zY.m41624a(Ship.class, "2c57f6cd3b7e4fb532ffdf36d808b0d6", i203);
        brB = a147;
        fmVarArr[i203] = a147;
        int i204 = i203 + 1;
        C2491fm a148 = C4105zY.m41624a(Ship.class, "162cb02e26d1833f86295b3289a6ca51", i204);
        brC = a148;
        fmVarArr[i204] = a148;
        int i205 = i204 + 1;
        C2491fm a149 = C4105zY.m41624a(Ship.class, "25fa3c45085f69f3ccb6fa04c697b3bb", i205);
        brD = a149;
        fmVarArr[i205] = a149;
        int i206 = i205 + 1;
        C2491fm a150 = C4105zY.m41624a(Ship.class, "51d1c27eb7f1b6e921caa8caafb09c47", i206);
        brE = a150;
        fmVarArr[i206] = a150;
        int i207 = i206 + 1;
        C2491fm a151 = C4105zY.m41624a(Ship.class, "b2fe0517d329b81acfd4426787363709", i207);
        f7138Wu = a151;
        fmVarArr[i207] = a151;
        int i208 = i207 + 1;
        C2491fm a152 = C4105zY.m41624a(Ship.class, "3ab60ad19ff4babc1f196e12f142bb82", i208);
        f7159zr = a152;
        fmVarArr[i208] = a152;
        int i209 = i208 + 1;
        C2491fm a153 = C4105zY.m41624a(Ship.class, "a8212dc32a2087561a7c6f706a3faa43", i209);
        brF = a153;
        fmVarArr[i209] = a153;
        int i210 = i209 + 1;
        C2491fm a154 = C4105zY.m41624a(Ship.class, "ac6f920ea1c049d8111e6f2334f0cbb5", i210);
        brG = a154;
        fmVarArr[i210] = a154;
        int i211 = i210 + 1;
        C2491fm a155 = C4105zY.m41624a(Ship.class, "c7ab68b8b1a99d2bcf560b5344b4df88", i211);
        brH = a155;
        fmVarArr[i211] = a155;
        int i212 = i211 + 1;
        C2491fm a156 = C4105zY.m41624a(Ship.class, "5a814fd9c940212e3c0074e94d053386", i212);
        brI = a156;
        fmVarArr[i212] = a156;
        int i213 = i212 + 1;
        C2491fm a157 = C4105zY.m41624a(Ship.class, "2dd969d9d72ddd1bacec1576a7bbac77", i213);
        brJ = a157;
        fmVarArr[i213] = a157;
        int i214 = i213 + 1;
        C2491fm a158 = C4105zY.m41624a(Ship.class, "57529e3beb3c57d464f0347f4ae2ac02", i214);
        _f_toString_0020_0028_0029Ljava_002flang_002fString_003b = a158;
        fmVarArr[i214] = a158;
        int i215 = i214 + 1;
        C2491fm a159 = C4105zY.m41624a(Ship.class, "9a7f88396bbc53950ba66c3ae612eb26", i215);
        f7124Pf = a159;
        fmVarArr[i215] = a159;
        int i216 = i215 + 1;
        C2491fm a160 = C4105zY.m41624a(Ship.class, "01a68764554f3f30b2819df231094e70", i216);
        brK = a160;
        fmVarArr[i216] = a160;
        int i217 = i216 + 1;
        C2491fm a161 = C4105zY.m41624a(Ship.class, "9b114245599d7934cf060e1de340dd33", i217);
        f7137Wp = a161;
        fmVarArr[i217] = a161;
        int i218 = i217 + 1;
        C2491fm a162 = C4105zY.m41624a(Ship.class, "af53e2cb7374264f0de1e2b03b096e2b", i218);
        brL = a162;
        fmVarArr[i218] = a162;
        int i219 = i218 + 1;
        C2491fm a163 = C4105zY.m41624a(Ship.class, "3c71e0a68e8d6c7dc0585c747937e7f8", i219);
        brM = a163;
        fmVarArr[i219] = a163;
        int i220 = i219 + 1;
        C2491fm a164 = C4105zY.m41624a(Ship.class, "6fc80ecd955c9138f148c66fe332bf22", i220);
        brN = a164;
        fmVarArr[i220] = a164;
        int i221 = i220 + 1;
        C2491fm a165 = C4105zY.m41624a(Ship.class, "313e1edd9b5fd7c8cdc2b222d0e29c9f", i221);
        brO = a165;
        fmVarArr[i221] = a165;
        int i222 = i221 + 1;
        C2491fm a166 = C4105zY.m41624a(Ship.class, "e464827c4fe236e7fb3af8bf2b8819cb", i222);
        brP = a166;
        fmVarArr[i222] = a166;
        int i223 = i222 + 1;
        C2491fm a167 = C4105zY.m41624a(Ship.class, "0dbc80bbf2acd369766e5bbb66bd665c", i223);
        brQ = a167;
        fmVarArr[i223] = a167;
        int i224 = i223 + 1;
        C2491fm a168 = C4105zY.m41624a(Ship.class, "0e979c149719c88bfdd76839abdf2c0f", i224);
        _f_step_0020_0028F_0029V = a168;
        fmVarArr[i224] = a168;
        int i225 = i224 + 1;
        C2491fm a169 = C4105zY.m41624a(Ship.class, "8ca5318e5ae843db76d66327499ff3c2", i225);
        brR = a169;
        fmVarArr[i225] = a169;
        int i226 = i225 + 1;
        C2491fm a170 = C4105zY.m41624a(Ship.class, "ed403b5753551efcb497b91db441de56", i226);
        brS = a170;
        fmVarArr[i226] = a170;
        int i227 = i226 + 1;
        C2491fm a171 = C4105zY.m41624a(Ship.class, "6b2d199ffd6e859dedd86b09eb78fb76", i227);
        brT = a171;
        fmVarArr[i227] = a171;
        int i228 = i227 + 1;
        C2491fm a172 = C4105zY.m41624a(Ship.class, "abb5889c6537a1d2344ee6b6a8d476b9", i228);
        brU = a172;
        fmVarArr[i228] = a172;
        int i229 = i228 + 1;
        C2491fm a173 = C4105zY.m41624a(Ship.class, "c4b03c35decd795c61b7726923db52c0", i229);
        brV = a173;
        fmVarArr[i229] = a173;
        int i230 = i229 + 1;
        C2491fm a174 = C4105zY.m41624a(Ship.class, "8d5ace663bdb63497cf2baa14f830003", i230);
        brW = a174;
        fmVarArr[i230] = a174;
        int i231 = i230 + 1;
        C2491fm a175 = C4105zY.m41624a(Ship.class, "543bf43b03d0462651709fb8ad1923ab", i231);
        brX = a175;
        fmVarArr[i231] = a175;
        int i232 = i231 + 1;
        C2491fm a176 = C4105zY.m41624a(Ship.class, "b68fb6f3a9b9c12d49e43a5401e4539a", i232);
        brY = a176;
        fmVarArr[i232] = a176;
        int i233 = i232 + 1;
        C2491fm a177 = C4105zY.m41624a(Ship.class, "7247fc85b29a6c240a9cd6d5747f84d8", i233);
        brZ = a177;
        fmVarArr[i233] = a177;
        int i234 = i233 + 1;
        C2491fm a178 = C4105zY.m41624a(Ship.class, "e6f313929ab5a2615ccdfbfc21bf5981", i234);
        f7148uZ = a178;
        fmVarArr[i234] = a178;
        int i235 = i234 + 1;
        C2491fm a179 = C4105zY.m41624a(Ship.class, "dce44b2c05a65eeaa0af6d7e759c5f88", i235);
        bsa = a179;
        fmVarArr[i235] = a179;
        int i236 = i235 + 1;
        C2491fm a180 = C4105zY.m41624a(Ship.class, "4e9f16a575b009ade68dfdc639ea70a0", i236);
        bsb = a180;
        fmVarArr[i236] = a180;
        int i237 = i236 + 1;
        C2491fm a181 = C4105zY.m41624a(Ship.class, "9e1cc4ef8b8ac09b5eb077bcf16e01d7", i237);
        bsc = a181;
        fmVarArr[i237] = a181;
        int i238 = i237 + 1;
        C2491fm a182 = C4105zY.m41624a(Ship.class, "8af786a6b9a4a856470ce286adcd4ee0", i238);
        f7155vq = a182;
        fmVarArr[i238] = a182;
        int i239 = i238 + 1;
        C2491fm a183 = C4105zY.m41624a(Ship.class, "9e84b53f177da0f650752adb5bc1c111", i239);
        bsd = a183;
        fmVarArr[i239] = a183;
        int i240 = i239 + 1;
        C2491fm a184 = C4105zY.m41624a(Ship.class, "c651b6fd35ef96a254cce817c845ce84", i240);
        bse = a184;
        fmVarArr[i240] = a184;
        int i241 = i240 + 1;
        C2491fm a185 = C4105zY.m41624a(Ship.class, "67a7761e9a618bfe62170fe8929cc5d8", i241);
        bsf = a185;
        fmVarArr[i241] = a185;
        int i242 = i241 + 1;
        C2491fm a186 = C4105zY.m41624a(Ship.class, "b084088d06fd3e92e38b080f9b6c4bb1", i242);
        bsg = a186;
        fmVarArr[i242] = a186;
        int i243 = i242 + 1;
        C2491fm a187 = C4105zY.m41624a(Ship.class, "43ddd657d1c023c15ff4f96ca884fec8", i243);
        bsh = a187;
        fmVarArr[i243] = a187;
        int i244 = i243 + 1;
        C2491fm a188 = C4105zY.m41624a(Ship.class, "24e29867e9121d0624db1aeaf1baa585", i244);
        bsi = a188;
        fmVarArr[i244] = a188;
        int i245 = i244 + 1;
        C2491fm a189 = C4105zY.m41624a(Ship.class, "867062449b60a3c404abb94188ab2b45", i245);
        bsj = a189;
        fmVarArr[i245] = a189;
        int i246 = i245 + 1;
        C2491fm a190 = C4105zY.m41624a(Ship.class, "efcec86d49a9541df17dd55843bf2bc5", i246);
        bsk = a190;
        fmVarArr[i246] = a190;
        int i247 = i246 + 1;
        C2491fm a191 = C4105zY.m41624a(Ship.class, "8f9bfac49b69f44f112fa5380fed96e3", i247);
        bsl = a191;
        fmVarArr[i247] = a191;
        int i248 = i247 + 1;
        C2491fm a192 = C4105zY.m41624a(Ship.class, "84bdd3b5baf042671ce78ec3f5bce502", i248);
        f7117Cq = a192;
        fmVarArr[i248] = a192;
        int i249 = i248 + 1;
        C2491fm a193 = C4105zY.m41624a(Ship.class, "3b287cced1156cbe9cec74c2a89b9258", i249);
        bsm = a193;
        fmVarArr[i249] = a193;
        int i250 = i249 + 1;
        C2491fm a194 = C4105zY.m41624a(Ship.class, "5fdb3dd2bfb9fdf2237e3f30764d5e46", i250);
        bsn = a194;
        fmVarArr[i250] = a194;
        int i251 = i250 + 1;
        C2491fm a195 = C4105zY.m41624a(Ship.class, "a8946d8e9307d18ac80a4e16c4350f61", i251);
        f7123Mq = a195;
        fmVarArr[i251] = a195;
        int i252 = i251 + 1;
        C2491fm a196 = C4105zY.m41624a(Ship.class, "147b10fbecd15f7d4ccb3847216f0995", i252);
        aPj = a196;
        fmVarArr[i252] = a196;
        int i253 = i252 + 1;
        C2491fm a197 = C4105zY.m41624a(Ship.class, "9b0a9cc7c269ae7102ece0a24408d1ad", i253);
        aPk = a197;
        fmVarArr[i253] = a197;
        int i254 = i253 + 1;
        C2491fm a198 = C4105zY.m41624a(Ship.class, "c6d46e7972fb57996b07101fb885adcf", i254);
        aPm = a198;
        fmVarArr[i254] = a198;
        int i255 = i254 + 1;
        C2491fm a199 = C4105zY.m41624a(Ship.class, "c65bb0ca978162e1008ca7796f29b245", i255);
        _f_hasHollowField_0020_0028_0029Z = a199;
        fmVarArr[i255] = a199;
        int i256 = i255 + 1;
        C2491fm a200 = C4105zY.m41624a(Ship.class, "7c37e19a88e3b0a2b46a3d97c60b6baf", i256);
        aKm = a200;
        fmVarArr[i256] = a200;
        int i257 = i256 + 1;
        C2491fm a201 = C4105zY.m41624a(Ship.class, "8fd9bcb3f0695eaf90fa50fcc85b8bca", i257);
        bso = a201;
        fmVarArr[i257] = a201;
        int i258 = i257 + 1;
        C2491fm a202 = C4105zY.m41624a(Ship.class, "a3dfe25df88b353ef24f0789e2e25af4", i258);
        bsp = a202;
        fmVarArr[i258] = a202;
        int i259 = i258 + 1;
        C2491fm a203 = C4105zY.m41624a(Ship.class, "e944895ff099fc77423ca3f8e8eb9b36", i259);
        aTk = a203;
        fmVarArr[i259] = a203;
        int i260 = i259 + 1;
        C2491fm a204 = C4105zY.m41624a(Ship.class, "22176be6f3721f93444105dd4281ee11", i260);
        _f_getType_0020_0028_0029Ljava_002flang_002fObject_003b = a204;
        fmVarArr[i260] = a204;
        int i261 = i260 + 1;
        C2491fm a205 = C4105zY.m41624a(Ship.class, "52cd82d99e92a5f9a5356ca4b5ac16f3", i261);
        bsq = a205;
        fmVarArr[i261] = a205;
        int i262 = i261 + 1;
        _m_methods = fmVarArr;
        C1634Xv.m11725a((Object[]) Pawn._m_methods, (Object[]) _m_methods);
        ___iScriptClass = aUO.m18566a(Ship.class, aMX.class, _m_fields, _m_methods);
    }

    /* renamed from: A */
    private void m30181A(Asset tCVar) {
        throw new C6039afL();
    }

    /* renamed from: C */
    private Shield m30184C(Vec3f vec3f) {
        switch (bFf().mo6893i(bqa)) {
            case 0:
                return null;
            case 2:
                return (Shield) bFf().mo5606d(new aCE(this, bqa, new Object[]{vec3f}));
            case 3:
                bFf().mo5606d(new aCE(this, bqa, new Object[]{vec3f}));
                break;
        }
        return m30183B(vec3f);
    }

    /* renamed from: H */
    private void m30186H(String str) {
        throw new C6039afL();
    }

    /* renamed from: K */
    private void m30187K(C3438ra raVar) {
        throw new C6039afL();
    }

    /* renamed from: L */
    private void m30188L(C3438ra raVar) {
        throw new C6039afL();
    }

    /* renamed from: Ll */
    private C3438ra m30189Ll() {
        return ((ShipType) getType()).bgF();
    }

    /* renamed from: M */
    private void m30190M(C3438ra raVar) {
        bFf().mo5608dq().mo3197f(boF, raVar);
    }

    /* renamed from: N */
    private void m30191N(C3438ra raVar) {
        bFf().mo5608dq().mo3197f(boK, raVar);
    }

    /* renamed from: O */
    private void m30192O(C3438ra raVar) {
        bFf().mo5608dq().mo3197f(boS, raVar);
    }

    @C0064Am(aul = "7c37e19a88e3b0a2b46a3d97c60b6baf", aum = 0)
    @C5566aOg
    /* renamed from: QR */
    private void m30193QR() {
        throw new aWi(new aCE(this, aKm, new Object[0]));
    }

    /* renamed from: R */
    private void m30195R(Actor cr) {
        bFf().mo5608dq().mo3197f(boP, cr);
    }

    /* renamed from: RR */
    private C6194aiK m30196RR() {
        return ((ShipType) getType()).mo184RY();
    }

    /* renamed from: RS */
    private float m30197RS() {
        return ((ShipType) getType()).mo188Sg();
    }

    @C0064Am(aul = "1a1f04ff74c14f26d83088aebd9b6b24", aum = 0)
    @C5566aOg
    /* renamed from: TG */
    private void m30199TG() {
        throw new aWi(new aCE(this, aPe, new Object[0]));
    }

    @C5566aOg
    /* renamed from: TH */
    private void m30200TH() {
        switch (bFf().mo6893i(aPe)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, aPe, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, aPe, new Object[0]));
                break;
        }
        m30199TG();
    }

    @C0064Am(aul = "51f404e7c6d89d312578ff37827865d2", aum = 0)
    @C5566aOg
    /* renamed from: TI */
    private AdapterLikedList<ShipAdapter> m30201TI() {
        throw new aWi(new aCE(this, aPf, new Object[0]));
    }

    /* renamed from: TP */
    private void m30205TP() {
        switch (bFf().mo6893i(aPl)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, aPl, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, aPl, new Object[0]));
                break;
        }
        m30204TO();
    }

    /* renamed from: Te */
    private AdapterLikedList m30207Te() {
        return (AdapterLikedList) bFf().mo5608dq().mo3214p(aOx);
    }

    @C0064Am(aul = "34989062d48bbb4773dd25db8c41ead7", aum = 0)
    @C6580apg
    /* renamed from: U */
    private void m30208U(Actor cr) {
        bFf().mo5600a((Class<? extends C4062yl>) C3981xi.C3982a.class, (C0495Gr) new aCE(this, bpE, new Object[]{cr}));
    }

    /* renamed from: V */
    private void m30210V(Asset tCVar) {
        throw new C6039afL();
    }

    @C0064Am(aul = "e944895ff099fc77423ca3f8e8eb9b36", aum = 0)
    /* renamed from: VK */
    private List m30215VK() {
        return agt();
    }

    @C4034yP
    @C5566aOg
    @C0064Am(aul = "bcffe353e34b6e18c46db26d441e36f8", aum = 0)
    @C2499fr(mo18855qf = {"b81f2b0b73838e82b8bf1f30b4fc2f96"})
    /* renamed from: W */
    private void m30216W(Actor cr) {
        throw new aWi(new aCE(this, bri, new Object[]{cr}));
    }

    /* renamed from: W */
    private void m30217W(Asset tCVar) {
        throw new C6039afL();
    }

    @C0064Am(aul = "a9eb996862832f06b36a866340a6bf5c", aum = 0)
    @C5566aOg
    /* renamed from: X */
    private void m30218X(Asset tCVar) {
        throw new aWi(new aCE(this, bru, new Object[]{tCVar}));
    }

    @C5566aOg
    @C0064Am(aul = "01ee1a74f27364abac5b45ecd0573ab8", aum = 0)
    @C2499fr
    /* renamed from: a */
    private Set<Loot> m30224a(Actor cr, Vec3d ajr, boolean z) {
        throw new aWi(new aCE(this, bry, new Object[]{cr, ajr, new Boolean(z)}));
    }

    @C0064Am(aul = "eaa7892d4368e1dc7f94b8359d69bb37", aum = 0)
    @C5566aOg
    @C2499fr
    /* renamed from: a */
    private void m30226a(int i, Component abl) {
        throw new aWi(new aCE(this, brh, new Object[]{new Integer(i), abl}));
    }

    @C0064Am(aul = "3ab60ad19ff4babc1f196e12f142bb82", aum = 0)
    @C5566aOg
    /* renamed from: a */
    private void m30229a(Actor cr, float f) {
        throw new aWi(new aCE(this, f7159zr, new Object[]{cr, new Float(f)}));
    }

    @C4034yP
    @C5566aOg
    @C0064Am(aul = "509421a788c0d62bf9d20d8e178c8b27", aum = 0)
    @C2499fr
    /* renamed from: a */
    private void m30230a(Actor cr, C5260aCm acm, Vec3f vec3f, Vec3f vec3f2) {
        throw new aWi(new aCE(this, f7152vm, new Object[]{cr, acm, vec3f, vec3f2}));
    }

    @C4034yP
    @C5566aOg
    @C0064Am(aul = "67a7761e9a618bfe62170fe8929cc5d8", aum = 0)
    @C2499fr(mo18857qh = 2)
    /* renamed from: a */
    private void m30233a(C0286Dh dh, float f) {
        throw new aWi(new aCE(this, bsf, new Object[]{dh, new Float(f)}));
    }

    /* renamed from: a */
    private void m30236a(CruiseSpeedType nf) {
        throw new C6039afL();
    }

    @C0064Am(aul = "605164dad796f14c92f162af56da1335", aum = 0)
    @C6580apg
    /* renamed from: a */
    private void m30238a(Component abl) {
        bFf().mo5600a((Class<? extends C4062yl>) C3981xi.C3984c.class, (C0495Gr) new aCE(this, bpz, new Object[]{abl}));
    }

    /* renamed from: a */
    private void m30240a(LootType ahc) {
        throw new C6039afL();
    }

    /* renamed from: a */
    private void m30242a(ShipItem arq) {
        bFf().mo5608dq().mo3197f(boM, arq);
    }

    /* renamed from: a */
    private void m30245a(C6194aiK aik) {
        throw new C6039afL();
    }

    /* renamed from: a */
    private void m30246a(C6809auB.C1996a aVar) {
        bFf().mo5608dq().mo3197f(bid, aVar);
    }

    /* renamed from: a */
    private void m30248a(DeathInformation aVar) {
        bFf().mo5608dq().mo3197f(bpd, aVar);
    }

    /* renamed from: a */
    private void m30249a(Hull jRVar) {
        bFf().mo5608dq().mo3197f(f7127VW, jRVar);
    }

    @C0064Am(aul = "c96d0f6f8648f6a6b2f3eddc9f5a0818", aum = 0)
    @C5566aOg
    /* renamed from: a */
    private void m30251a(Asset tCVar, Vec3d ajr) {
        throw new aWi(new aCE(this, brv, new Object[]{tCVar, ajr}));
    }

    /* renamed from: a */
    private void m30252a(CargoHold wIVar) {
        bFf().mo5608dq().mo3197f(boD, wIVar);
    }

    /* renamed from: a */
    private void m30253a(CollisionFXSet yaVar) {
        throw new C6039afL();
    }

    /* renamed from: a */
    private void m30254a(AdapterLikedList zAVar) {
        bFf().mo5608dq().mo3197f(aOx, zAVar);
    }

    @C4034yP
    @C5566aOg
    @C0064Am(aul = "b7869876dcef485bceacb1feea925195", aum = 0)
    @C2499fr(mo18857qh = 2)
    /* renamed from: aa */
    private void m30257aa(Actor cr) {
        throw new aWi(new aCE(this, brk, new Object[]{cr}));
    }

    @C4034yP
    @C5566aOg
    @C2499fr(mo18857qh = 2)
    /* renamed from: ab */
    private void m30258ab(Actor cr) {
        switch (bFf().mo6893i(brk)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, brk, new Object[]{cr}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, brk, new Object[]{cr}));
                break;
        }
        m30257aa(cr);
    }

    @C4034yP
    @C5566aOg
    @C0064Am(aul = "42ba94691fd3856bd32dffab0a906a1f", aum = 0)
    @C2499fr
    private void acK() {
        throw new aWi(new aCE(this, bka, new Object[0]));
    }

    @C4034yP
    @C5566aOg
    @C0064Am(aul = "694a61d732bfd92e4cf044ade7563b28", aum = 0)
    @C2499fr
    private void acP() {
        throw new aWi(new aCE(this, bkd, new Object[0]));
    }

    private C6809auB.C1996a acy() {
        return (C6809auB.C1996a) bFf().mo5608dq().mo3214p(bid);
    }

    private CargoHoldType aeI() {
        return ((ShipType) getType()).mo4156Wb();
    }

    private CollisionFXSet aeJ() {
        return ((ShipType) getType()).bkb();
    }

    private CruiseSpeedType aeK() {
        return ((ShipType) getType()).bkd();
    }

    private Asset aeL() {
        return ((ShipType) getType()).bkf();
    }

    private I18NString aeM() {
        return ((ShipType) getType()).mo4158Wn();
    }

    private LootType aeN() {
        return ((ShipType) getType()).bkl();
    }

    private float aeO() {
        return ((ShipType) getType()).mo4152VF();
    }

    private float aeP() {
        return ((ShipType) getType()).mo4153VH();
    }

    private float aeQ() {
        return ((ShipType) getType()).bkn();
    }

    private float aeR() {
        return ((ShipType) getType()).bkp();
    }

    private float aeS() {
        return ((ShipType) getType()).mo4218rb();
    }

    private float aeT() {
        return ((ShipType) getType()).mo4217ra();
    }

    private float aeU() {
        return ((ShipType) getType()).bkr();
    }

    private float aeV() {
        return ((ShipType) getType()).bkt();
    }

    private float aeW() {
        return ((ShipType) getType()).bkv();
    }

    private Asset aeX() {
        return ((ShipType) getType()).bkx();
    }

    private float aeY() {
        return ((ShipType) getType()).bkz();
    }

    private float aeZ() {
        return ((ShipType) getType()).agp();
    }

    /* renamed from: af */
    private void m30262af(Actor cr) {
        switch (bFf().mo6893i(brm)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, brm, new Object[]{cr}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, brm, new Object[]{cr}));
                break;
        }
        m30261ae(cr);
    }

    private float afA() {
        return bFf().mo5608dq().mo3211m(bpq);
    }

    @C4034yP
    @C5566aOg
    @C0064Am(aul = "147a1277d4bfc60af0d5f7d4b834c583", aum = 0)
    @C2499fr
    private void afB() {
        throw new aWi(new aCE(this, bpr, new Object[0]));
    }

    private float afa() {
        return ((ShipType) getType()).air();
    }

    private C3438ra afb() {
        return ((ShipType) getType()).agt();
    }

    private C3438ra afc() {
        return ((ShipType) getType()).mo4157Wl();
    }

    private Vec3d afd() {
        return ((ShipType) getType()).ait();
    }

    private Vec3d afe() {
        return ((ShipType) getType()).aiv();
    }

    private Vec3d aff() {
        return ((ShipType) getType()).aix();
    }

    private float afg() {
        return ((ShipType) getType()).mo4145Ag();
    }

    private float afh() {
        return ((ShipType) getType()).mo4144Ae();
    }

    private float afi() {
        return ((ShipType) getType()).bkF();
    }

    private CruiseSpeed afj() {
        return (CruiseSpeed) bFf().mo5608dq().mo3214p(boz);
    }

    private CargoHold afk() {
        return (CargoHold) bFf().mo5608dq().mo3214p(boD);
    }

    private C3438ra afl() {
        return (C3438ra) bFf().mo5608dq().mo3214p(boF);
    }

    private Character afm() {
        return (Character) bFf().mo5608dq().mo3214p(boI);
    }

    private C3438ra afn() {
        return (C3438ra) bFf().mo5608dq().mo3214p(boK);
    }

    private ShipItem afo() {
        return (ShipItem) bFf().mo5608dq().mo3214p(boM);
    }

    private long afp() {
        return bFf().mo5608dq().mo3213o(boO);
    }

    private Actor afq() {
        return (Actor) bFf().mo5608dq().mo3214p(boP);
    }

    private C3438ra afr() {
        return (C3438ra) bFf().mo5608dq().mo3214p(boS);
    }

    private Weapon afs() {
        return (Weapon) bFf().mo5608dq().mo3214p(boU);
    }

    private Weapon aft() {
        return (Weapon) bFf().mo5608dq().mo3214p(boW);
    }

    private long afu() {
        return bFf().mo5608dq().mo3213o(bpb);
    }

    private DeathInformation afv() {
        return (DeathInformation) bFf().mo5608dq().mo3214p(bpd);
    }

    private float afw() {
        return bFf().mo5608dq().mo3211m(bph);
    }

    private float afx() {
        return bFf().mo5608dq().mo3211m(bpj);
    }

    private float afy() {
        return bFf().mo5608dq().mo3211m(bpl);
    }

    private float afz() {
        return bFf().mo5608dq().mo3211m(bpn);
    }

    private void agN() {
        switch (bFf().mo6893i(bqp)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, bqp, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, bqp, new Object[0]));
                break;
        }
        agM();
    }

    /* access modifiers changed from: private */
    public float agP() {
        switch (bFf().mo6893i(bqq)) {
            case 0:
                return 0.0f;
            case 2:
                return ((Float) bFf().mo5606d(new aCE(this, bqq, new Object[0]))).floatValue();
            case 3:
                bFf().mo5606d(new aCE(this, bqq, new Object[0]));
                break;
        }
        return agO();
    }

    /* access modifiers changed from: private */
    public float agT() {
        switch (bFf().mo6893i(bqs)) {
            case 0:
                return 0.0f;
            case 2:
                return ((Float) bFf().mo5606d(new aCE(this, bqs, new Object[0]))).floatValue();
            case 3:
                bFf().mo5606d(new aCE(this, bqs, new Object[0]));
                break;
        }
        return agS();
    }

    /* access modifiers changed from: private */
    public float agV() {
        switch (bFf().mo6893i(bqt)) {
            case 0:
                return 0.0f;
            case 2:
                return ((Float) bFf().mo5606d(new aCE(this, bqt, new Object[0]))).floatValue();
            case 3:
                bFf().mo5606d(new aCE(this, bqt, new Object[0]));
                break;
        }
        return agU();
    }

    /* access modifiers changed from: private */
    public float agX() {
        switch (bFf().mo6893i(bqu)) {
            case 0:
                return 0.0f;
            case 2:
                return ((Float) bFf().mo5606d(new aCE(this, bqu, new Object[0]))).floatValue();
            case 3:
                bFf().mo5606d(new aCE(this, bqu, new Object[0]));
                break;
        }
        return agW();
    }

    @C0064Am(aul = "947c57c19204dec1a2b100509f0745b6", aum = 0)
    @C5566aOg
    @C2499fr
    private String agk() {
        throw new aWi(new aCE(this, bpX, new Object[0]));
    }

    private Ship ahA() {
        switch (bFf().mo6893i(bro)) {
            case 0:
                return null;
            case 2:
                return (Ship) bFf().mo5606d(new aCE(this, bro, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, bro, new Object[0]));
                break;
        }
        return ahz();
    }

    @C4034yP
    @C5566aOg
    @C0064Am(aul = "48f39407a2535b41496d9abdeeb85bd0", aum = 0)
    @C2499fr(mo18855qf = {"b81f2b0b73838e82b8bf1f30b4fc2f96"})
    @Deprecated
    private void ahD() {
        throw new aWi(new aCE(this, brA, new Object[0]));
    }

    @C4034yP
    @C5566aOg
    @C0064Am(aul = "2c57f6cd3b7e4fb532ffdf36d808b0d6", aum = 0)
    @C2499fr(mo18855qf = {"b81f2b0b73838e82b8bf1f30b4fc2f96"})
    @Deprecated
    private void ahF() {
        throw new aWi(new aCE(this, brB, new Object[0]));
    }

    @C4034yP
    @C5566aOg
    @C0064Am(aul = "25fa3c45085f69f3ccb6fa04c697b3bb", aum = 0)
    @C2499fr(mo18855qf = {"b81f2b0b73838e82b8bf1f30b4fc2f96"})
    @Deprecated
    private void ahH() {
        throw new aWi(new aCE(this, brD, new Object[0]));
    }

    @C4034yP
    @C5566aOg
    @C0064Am(aul = "51d1c27eb7f1b6e921caa8caafb09c47", aum = 0)
    @C2499fr(mo18855qf = {"b81f2b0b73838e82b8bf1f30b4fc2f96"})
    @Deprecated
    private void ahJ() {
        throw new aWi(new aCE(this, brE, new Object[0]));
    }

    @C0064Am(aul = "3c71e0a68e8d6c7dc0585c747937e7f8", aum = 0)
    @C5566aOg
    private void ahR() {
        throw new aWi(new aCE(this, brM, new Object[0]));
    }

    private void ahY() {
        switch (bFf().mo6893i(brP)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, brP, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, brP, new Object[0]));
                break;
        }
        ahX();
    }

    private boolean ahk() {
        switch (bFf().mo6893i(bqC)) {
            case 0:
                return false;
            case 2:
                return ((Boolean) bFf().mo5606d(new aCE(this, bqC, new Object[0]))).booleanValue();
            case 3:
                bFf().mo5606d(new aCE(this, bqC, new Object[0]));
                break;
        }
        return ahj();
    }

    private void ahm() {
        switch (bFf().mo6893i(bqE)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, bqE, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, bqE, new Object[0]));
                break;
        }
        ahl();
    }

    private void ahr() {
        switch (bFf().mo6893i(bqN)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, bqN, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, bqN, new Object[0]));
                break;
        }
        ahq();
    }

    @C0064Am(aul = "0e8c108fb7b35aa2197ccfa3da3e4c90", aum = 0)
    @C5566aOg
    private void ahw() {
        throw new aWi(new aCE(this, bqT, new Object[0]));
    }

    @ClientOnly
    private void ahy() {
        switch (bFf().mo6893i(brn)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, brn, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, brn, new Object[0]));
                break;
        }
        ahx();
    }

    @C0064Am(aul = "52cd82d99e92a5f9a5356ca4b5ac16f3", aum = 0)
    private C0520HN aiC() {
        return ahO();
    }

    @C4034yP
    @C5566aOg
    @C0064Am(aul = "dce44b2c05a65eeaa0af6d7e759c5f88", aum = 0)
    @C2499fr
    private void aim() {
        throw new aWi(new aCE(this, bsa, new Object[0]));
    }

    @C5566aOg
    @C2499fr
    /* renamed from: b */
    private void m30265b(int i, Component abl) {
        switch (bFf().mo6893i(brh)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, brh, new Object[]{new Integer(i), abl}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, brh, new Object[]{new Integer(i), abl}));
                break;
        }
        m30226a(i, abl);
    }

    @C4034yP
    @C5566aOg
    @C2499fr(mo18857qh = 2)
    /* renamed from: b */
    private void m30266b(C0286Dh dh, float f) {
        switch (bFf().mo6893i(bsf)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, bsf, new Object[]{dh, new Float(f)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, bsf, new Object[]{dh, new Float(f)}));
                break;
        }
        m30233a(dh, f);
    }

    /* renamed from: b */
    private void m30267b(ShipSector or) {
        switch (bFf().mo6893i(bpG)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, bpG, new Object[]{or}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, bpG, new Object[]{or}));
                break;
        }
        m30237a(or);
    }

    @C6580apg
    /* renamed from: b */
    private void m30268b(Component abl) {
        switch (bFf().mo6893i(bpz)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, bpz, new Object[]{abl}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, bpz, new Object[]{abl}));
                break;
        }
        m30238a(abl);
    }

    @C0064Am(aul = "a3dfe25df88b353ef24f0789e2e25af4", aum = 0)
    /* renamed from: b */
    private void m30269b(aDJ adj, C5663aRz arz, Object obj) {
        mo18303b((Hull) adj, arz, obj);
    }

    @C4034yP
    @ClientOnly
    @C2499fr
    /* renamed from: b */
    private void m30270b(Vec3d ajr, Node rPVar, StellarSystem jj, Set<Loot> set) {
        switch (bFf().mo6893i(bqK)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, bqK, new Object[]{ajr, rPVar, jj, set}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, bqK, new Object[]{ajr, rPVar, jj, set}));
                break;
        }
        m30241a(ajr, rPVar, jj, set);
    }

    /* renamed from: b */
    private void m30271b(CargoHoldType arb) {
        throw new C6039afL();
    }

    @ClientOnly
    /* renamed from: b */
    private void m30272b(Weapon adv, C3904wY wYVar) {
        switch (bFf().mo6893i(bps)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, bps, new Object[]{adv, wYVar}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, bps, new Object[]{adv, wYVar}));
                break;
        }
        m30243a(adv, wYVar);
    }

    /* renamed from: b */
    private void m30273b(ShipStructure afk) {
        switch (bFf().mo6893i(bpH)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, bpH, new Object[]{afk}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, bpH, new Object[]{afk}));
                break;
        }
        m30244a(afk);
    }

    /* renamed from: b */
    private void m30274b(CruiseSpeed fkVar) {
        bFf().mo5608dq().mo3197f(boz, fkVar);
    }

    /* renamed from: b */
    private void m30276b(Collection collection, C0495Gr gr) {
        Iterator it = collection.iterator();
        while (it.hasNext()) {
            ((C3981xi.C3984c) it.next()).mo3933d(this, (Component) gr.getArgs()[0]);
        }
    }

    /* renamed from: b */
    private boolean m30278b(Controller jx, Actor cr) {
        switch (bFf().mo6893i(bqR)) {
            case 0:
                return false;
            case 2:
                return ((Boolean) bFf().mo5606d(new aCE(this, bqR, new Object[]{jx, cr}))).booleanValue();
            case 3:
                bFf().mo5606d(new aCE(this, bqR, new Object[]{jx, cr}));
                break;
        }
        return m30256a(jx, cr);
    }

    @C0064Am(aul = "9d0d2b502467b41ea987d3170f13d8c0", aum = 0)
    @C5566aOg
    /* renamed from: c */
    private void m30283c(Actor cr, C5260aCm acm, Vec3f vec3f, Vec3f vec3f2) {
        throw new aWi(new aCE(this, f7153vn, new Object[]{cr, acm, vec3f, vec3f2}));
    }

    @C0064Am(aul = "0b2594f3bc5acba031e769203ca41dc4", aum = 0)
    @C6580apg
    /* renamed from: c */
    private void m30284c(Component abl) {
        bFf().mo5600a((Class<? extends C4062yl>) C3981xi.C3983b.class, (C0495Gr) new aCE(this, bpA, new Object[]{abl}));
    }

    @C4034yP
    @C5566aOg
    @C0064Am(aul = "27b82832b7d0f91fb23b78ad6b12e9ec", aum = 0)
    @C2499fr
    /* renamed from: c */
    private void m30287c(Weapon adv, C3904wY wYVar) {
        throw new aWi(new aCE(this, bpt, new Object[]{adv, wYVar}));
    }

    @C0064Am(aul = "3bd5385f10bf00485cbc2feda1e63f40", aum = 0)
    @C5566aOg
    /* renamed from: c */
    private void m30288c(PlayerController tVar) {
        throw new aWi(new aCE(this, brp, new Object[]{tVar}));
    }

    /* renamed from: c */
    private void m30290c(Collection collection, C0495Gr gr) {
        Iterator it = collection.iterator();
        while (it.hasNext()) {
            ((C3981xi.C3983b) it.next()).mo3926b(this, (Component) gr.getArgs()[0]);
        }
    }

    /* renamed from: c */
    private void m30291c(Iterator<Item> it) {
        switch (bFf().mo6893i(brt)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, brt, new Object[]{it}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, brt, new Object[]{it}));
                break;
        }
        m30277b(it);
    }

    /* renamed from: cV */
    private void m30293cV(long j) {
        bFf().mo5608dq().mo3184b(boO, j);
    }

    /* renamed from: cW */
    private void m30294cW(long j) {
        bFf().mo5608dq().mo3184b(bpb, j);
    }

    @C0064Am(aul = "cc3e45febbf5feb8cdbf3357051a7c46", aum = 0)
    @C5566aOg
    /* renamed from: cX */
    private void m30295cX(long j) {
        throw new aWi(new aCE(this, bpC, new Object[]{new Long(j)}));
    }

    @C0064Am(aul = "b7e318a2771ec66628dd50ee9410c688", aum = 0)
    @C5566aOg
    /* renamed from: cZ */
    private void m30296cZ(long j) {
        throw new aWi(new aCE(this, bpF, new Object[]{new Long(j)}));
    }

    /* renamed from: cx */
    private void m30297cx(float f) {
        throw new C6039afL();
    }

    @C6580apg
    /* renamed from: d */
    private void m30300d(Component abl) {
        switch (bFf().mo6893i(bpA)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, bpA, new Object[]{abl}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, bpA, new Object[]{abl}));
                break;
        }
        m30284c(abl);
    }

    @C4034yP
    @C5566aOg
    @C2499fr
    /* renamed from: d */
    private void m30301d(Weapon adv, C3904wY wYVar) {
        switch (bFf().mo6893i(bpt)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, bpt, new Object[]{adv, wYVar}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, bpt, new Object[]{adv, wYVar}));
                break;
        }
        m30287c(adv, wYVar);
    }

    @C5566aOg
    /* renamed from: d */
    private void m30302d(PlayerController tVar) {
        switch (bFf().mo6893i(brp)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, brp, new Object[]{tVar}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, brp, new Object[]{tVar}));
                break;
        }
        m30288c(tVar);
    }

    /* renamed from: d */
    private void m30303d(Collection collection, C0495Gr gr) {
        Iterator it = collection.iterator();
        while (it.hasNext()) {
            ((C3981xi.C3982a) it.next()).mo7563b(this, (Actor) gr.getArgs()[0]);
        }
    }

    /* renamed from: dA */
    private void m30305dA(float f) {
        throw new C6039afL();
    }

    /* renamed from: dB */
    private void m30306dB(float f) {
        throw new C6039afL();
    }

    /* renamed from: dC */
    private void m30307dC(float f) {
        throw new C6039afL();
    }

    /* renamed from: dD */
    private void m30308dD(float f) {
        throw new C6039afL();
    }

    /* renamed from: dE */
    private void m30309dE(float f) {
        throw new C6039afL();
    }

    /* renamed from: dF */
    private void m30310dF(float f) {
        throw new C6039afL();
    }

    /* renamed from: dG */
    private void m30311dG(float f) {
        throw new C6039afL();
    }

    /* renamed from: dH */
    private void m30312dH(float f) {
        throw new C6039afL();
    }

    /* renamed from: dI */
    private void m30313dI(float f) {
        bFf().mo5608dq().mo3150a(bph, f);
    }

    /* renamed from: dJ */
    private void m30314dJ(float f) {
        bFf().mo5608dq().mo3150a(bpj, f);
    }

    /* renamed from: dK */
    private void m30315dK(float f) {
        bFf().mo5608dq().mo3150a(bpl, f);
    }

    /* renamed from: dL */
    private void m30316dL(float f) {
        bFf().mo5608dq().mo3150a(bpn, f);
    }

    /* renamed from: dM */
    private void m30317dM(float f) {
        bFf().mo5608dq().mo3150a(bpq, f);
    }

    @C4034yP
    @C5566aOg
    @C0064Am(aul = "a8212dc32a2087561a7c6f706a3faa43", aum = 0)
    @C2499fr
    /* renamed from: db */
    private void m30318db(long j) {
        throw new aWi(new aCE(this, brF, new Object[]{new Long(j)}));
    }

    @C4034yP
    @C5566aOg
    @C2499fr
    /* renamed from: dc */
    private void m30319dc(long j) {
        switch (bFf().mo6893i(brF)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, brF, new Object[]{new Long(j)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, brF, new Object[]{new Long(j)}));
                break;
        }
        m30318db(j);
    }

    /* renamed from: dt */
    private void m30321dt(float f) {
        throw new C6039afL();
    }

    /* renamed from: du */
    private void m30322du(float f) {
        throw new C6039afL();
    }

    /* renamed from: dv */
    private void m30323dv(float f) {
        throw new C6039afL();
    }

    /* renamed from: dw */
    private void m30324dw(float f) {
        throw new C6039afL();
    }

    /* renamed from: dx */
    private void m30325dx(float f) {
        throw new C6039afL();
    }

    /* renamed from: dy */
    private void m30326dy(float f) {
        throw new C6039afL();
    }

    /* renamed from: dz */
    private void m30327dz(float f) {
        throw new C6039afL();
    }

    @C4034yP
    @C5566aOg
    @C0064Am(aul = "b2fe0517d329b81acfd4426787363709", aum = 0)
    @C2499fr(mo18857qh = 2)
    /* renamed from: e */
    private void m30328e(Actor cr, C5260aCm acm, Vec3f vec3f, Vec3f vec3f2) {
        throw new aWi(new aCE(this, f7138Wu, new Object[]{cr, acm, vec3f, vec3f2}));
    }

    @C0064Am(aul = "0aa40f18d38f3e5abe68d79ec23a1e57", aum = 0)
    @C5566aOg
    /* renamed from: e */
    private void m30330e(Component abl) {
        throw new aWi(new aCE(this, bqF, new Object[]{abl}));
    }

    /* renamed from: eF */
    private void m30332eF(I18NString i18NString) {
        throw new C6039afL();
    }

    /* renamed from: eL */
    private boolean m30334eL(int i) {
        switch (bFf().mo6893i(bqv)) {
            case 0:
                return false;
            case 2:
                return ((Boolean) bFf().mo5606d(new aCE(this, bqv, new Object[]{new Integer(i)}))).booleanValue();
            case 3:
                bFf().mo5606d(new aCE(this, bqv, new Object[]{new Integer(i)}));
                break;
        }
        return m30333eK(i);
    }

    /* renamed from: f */
    private void m30336f(Weapon adv) {
        bFf().mo5608dq().mo3197f(boU, adv);
    }

    /* renamed from: f */
    private void m30337f(Weapon adv, C3904wY wYVar) {
        switch (bFf().mo6893i(bpu)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, bpu, new Object[]{adv, wYVar}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, bpu, new Object[]{adv, wYVar}));
                break;
        }
        m30331e(adv, wYVar);
    }

    @C0064Am(aul = "3662e634d7e2bd9bc23f623489dea0ec", aum = 0)
    @C5566aOg
    @C2499fr
    /* renamed from: fg */
    private void m30339fg() {
        throw new aWi(new aCE(this, _f_dispose_0020_0028_0029V, new Object[0]));
    }

    @C0064Am(aul = "bea28b66997d47a230b6c824d219e56a", aum = 0)
    @C5566aOg
    /* renamed from: g */
    private void m30340g(Component abl) {
        throw new aWi(new aCE(this, bqG, new Object[]{abl}));
    }

    /* renamed from: g */
    private void m30341g(Weapon adv) {
        bFf().mo5608dq().mo3197f(boW, adv);
    }

    @ClientOnly
    /* renamed from: g */
    private void m30343g(SceneObject sceneObject) {
        switch (bFf().mo6893i(brU)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, brU, new Object[]{sceneObject}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, brU, new Object[]{sceneObject}));
                break;
        }
        m30338f(sceneObject);
    }

    @C0064Am(aul = "967a2a2a2d128c04bdcfa8daa8f0cead", aum = 0)
    @C5566aOg
    /* renamed from: h */
    private void m30344h(Weapon adv) {
        throw new aWi(new aCE(this, bpw, new Object[]{adv}));
    }

    /* renamed from: h */
    private void m30345h(Weapon adv, C3904wY wYVar) {
        switch (bFf().mo6893i(bpv)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, bpv, new Object[]{adv, wYVar}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, bpv, new Object[]{adv, wYVar}));
                break;
        }
        m30342g(adv, wYVar);
    }

    @C0064Am(aul = "9e486d4f9295a03707270a63faa1f514", aum = 0)
    @C5566aOg
    /* renamed from: i */
    private void m30348i(Component abl) {
        throw new aWi(new aCE(this, bqP, new Object[]{abl}));
    }

    /* renamed from: i */
    private void m30349i(Character acx) {
        bFf().mo5608dq().mo3197f(boI, acx);
    }

    /* renamed from: ij */
    private String m30351ij() {
        return ((ShipType) getType()).mo4216iu();
    }

    @C5566aOg
    /* renamed from: j */
    private void m30356j(Component abl) {
        switch (bFf().mo6893i(bqP)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, bqP, new Object[]{abl}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, bqP, new Object[]{abl}));
                break;
        }
        m30348i(abl);
    }

    @C4034yP
    @C5566aOg
    @C0064Am(aul = "00b3ada660914d80a7dcc16503bc5ff7", aum = 0)
    @C2499fr
    @Deprecated
    /* renamed from: j */
    private void m30358j(Weapon adv) {
        throw new aWi(new aCE(this, bqU, new Object[]{adv}));
    }

    @C0064Am(aul = "c30b3be54f5d9039eefd4e74454e7721", aum = 0)
    @C5566aOg
    /* renamed from: k */
    private void m30361k(Component abl) {
        throw new aWi(new aCE(this, bqS, new Object[]{abl}));
    }

    @C5566aOg
    /* renamed from: l */
    private void m30363l(Component abl) {
        switch (bFf().mo6893i(bqS)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, bqS, new Object[]{abl}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, bqS, new Object[]{abl}));
                break;
        }
        m30361k(abl);
    }

    @C4034yP
    @C5566aOg
    @C0064Am(aul = "5c6b59bd06afc4d40e923175f2f39346", aum = 0)
    @C2499fr
    @Deprecated
    /* renamed from: l */
    private void m30364l(Weapon adv) {
        throw new aWi(new aCE(this, bqV, new Object[]{adv}));
    }

    @C4034yP
    @C5566aOg
    @C0064Am(aul = "65a4d987e756f537044318fdd0e8c1db", aum = 0)
    @C2499fr(mo18855qf = {"b81f2b0b73838e82b8bf1f30b4fc2f96"})
    /* renamed from: l */
    private void m30365l(C3904wY wYVar) {
        throw new aWi(new aCE(this, bqW, new Object[]{wYVar}));
    }

    @C4034yP
    @C5566aOg
    @C2499fr(mo18855qf = {"b81f2b0b73838e82b8bf1f30b4fc2f96"})
    /* renamed from: m */
    private void m30367m(C3904wY wYVar) {
        switch (bFf().mo6893i(bqW)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, bqW, new Object[]{wYVar}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, bqW, new Object[]{wYVar}));
                break;
        }
        m30365l(wYVar);
    }

    @ClientOnly
    /* renamed from: o */
    private void m30370o(C3904wY wYVar) {
        switch (bFf().mo6893i(bqX)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, bqX, new Object[]{wYVar}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, bqX, new Object[]{wYVar}));
                break;
        }
        m30369n(wYVar);
    }

    @C4034yP
    @C5566aOg
    @C0064Am(aul = "6c7a58f824cb8663953ec85d67305ba2", aum = 0)
    @C2499fr(mo18857qh = 2)
    /* renamed from: p */
    private void m30373p(C3904wY wYVar) {
        throw new aWi(new aCE(this, bqY, new Object[]{wYVar}));
    }

    @C4034yP
    @C5566aOg
    @C2499fr(mo18857qh = 2)
    /* renamed from: q */
    private void m30375q(C3904wY wYVar) {
        switch (bFf().mo6893i(bqY)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, bqY, new Object[]{wYVar}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, bqY, new Object[]{wYVar}));
                break;
        }
        m30373p(wYVar);
    }

    @C0064Am(aul = "2f4aa5e6fd66624b1830dbb245d4c5de", aum = 0)
    @C5566aOg
    @C2499fr
    /* renamed from: qU */
    private void m30376qU() {
        throw new aWi(new aCE(this, f7122Lm, new Object[0]));
    }

    @C0064Am(aul = "22176be6f3721f93444105dd4281ee11", aum = 0)
    /* renamed from: qW */
    private Object m30377qW() {
        return agH();
    }

    /* renamed from: r */
    private void m30379r(Asset tCVar) {
        throw new C6039afL();
    }

    @C4034yP
    @C5566aOg
    @C0064Am(aul = "1417d72719cf07e0306fa5b260d85d68", aum = 0)
    @C2499fr(mo18855qf = {"b81f2b0b73838e82b8bf1f30b4fc2f96"})
    /* renamed from: r */
    private void m30380r(C3904wY wYVar) {
        throw new aWi(new aCE(this, bqZ, new Object[]{wYVar}));
    }

    /* renamed from: rh */
    private Asset m30381rh() {
        return ((ShipType) getType()).mo4151Nu();
    }

    /* renamed from: s */
    private void m30383s(Vec3d ajr) {
        throw new C6039afL();
    }

    @C4034yP
    @C5566aOg
    @C2499fr(mo18855qf = {"b81f2b0b73838e82b8bf1f30b4fc2f96"})
    /* renamed from: s */
    private void m30384s(C3904wY wYVar) {
        switch (bFf().mo6893i(bqZ)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, bqZ, new Object[]{wYVar}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, bqZ, new Object[]{wYVar}));
                break;
        }
        m30380r(wYVar);
    }

    /* renamed from: t */
    private void m30385t(Vec3d ajr) {
        throw new C6039afL();
    }

    /* renamed from: u */
    private void m30387u(Vec3d ajr) {
        throw new C6039afL();
    }

    @ClientOnly
    /* renamed from: u */
    private void m30388u(C3904wY wYVar) {
        switch (bFf().mo6893i(bra)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, bra, new Object[]{wYVar}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, bra, new Object[]{wYVar}));
                break;
        }
        m30386t(wYVar);
    }

    @C4034yP
    @C5566aOg
    @C0064Am(aul = "839eb722d120ef84b0060d0d83f6c1de", aum = 0)
    @C2499fr(mo18857qh = 2)
    /* renamed from: v */
    private void m30390v(C3904wY wYVar) {
        throw new aWi(new aCE(this, brb, new Object[]{wYVar}));
    }

    @C4034yP
    @C5566aOg
    @C2499fr(mo18857qh = 2)
    /* renamed from: w */
    private void m30391w(C3904wY wYVar) {
        switch (bFf().mo6893i(brb)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, brb, new Object[]{wYVar}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, brb, new Object[]{wYVar}));
                break;
        }
        m30390v(wYVar);
    }

    /* renamed from: x */
    private void m30392x(C3438ra raVar) {
        throw new C6039afL();
    }

    /* renamed from: z */
    private void m30394z(Asset tCVar) {
        throw new C6039afL();
    }

    /* renamed from: zi */
    private Hull m30396zi() {
        return (Hull) bFf().mo5608dq().mo3214p(f7127VW);
    }

    /* renamed from: zl */
    private Asset m30397zl() {
        return ((ShipType) getType()).bkh();
    }

    /* renamed from: zm */
    private Asset m30398zm() {
        return ((ShipType) getType()).bkj();
    }

    @ClientOnly
    /* renamed from: A */
    public void mo18250A(C3904wY wYVar) {
        switch (bFf().mo6893i(brC)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, brC, new Object[]{wYVar}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, brC, new Object[]{wYVar}));
                break;
        }
        m30395z(wYVar);
    }

    /* renamed from: Ae */
    public float mo18251Ae() {
        switch (bFf().mo6893i(bsk)) {
            case 0:
                return 0.0f;
            case 2:
                return ((Float) bFf().mo5606d(new aCE(this, bsk, new Object[0]))).floatValue();
            case 3:
                bFf().mo5606d(new aCE(this, bsk, new Object[0]));
                break;
        }
        return aiz();
    }

    /* renamed from: Ag */
    public float mo18252Ag() {
        switch (bFf().mo6893i(bsj)) {
            case 0:
                return 0.0f;
            case 2:
                return ((Float) bFf().mo5606d(new aCE(this, bsj, new Object[0]))).floatValue();
            case 3:
                bFf().mo5606d(new aCE(this, bsj, new Object[0]));
                break;
        }
        return aiy();
    }

    /* renamed from: C */
    public ShipSector mo18253C(C3904wY wYVar) {
        switch (bFf().mo6893i(bsn)) {
            case 0:
                return null;
            case 2:
                return (ShipSector) bFf().mo5606d(new aCE(this, bsn, new Object[]{wYVar}));
            case 3:
                bFf().mo5606d(new aCE(this, bsn, new Object[]{wYVar}));
                break;
        }
        return m30182B(wYVar);
    }

    @C1253SX
    /* renamed from: E */
    public void mo18254E(Vec3f vec3f) {
        switch (bFf().mo6893i(bsb)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, bsb, new Object[]{vec3f}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, bsb, new Object[]{vec3f}));
                break;
        }
        m30185D(vec3f);
    }

    @ClientOnly
    /* renamed from: Fe */
    public void mo957Fe() {
        switch (bFf().mo6893i(bqM)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, bqM, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, bqM, new Object[0]));
                break;
        }
        ahp();
    }

    @C5566aOg
    /* renamed from: QS */
    public void mo18255QS() {
        switch (bFf().mo6893i(aKm)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, aKm, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, aKm, new Object[0]));
                break;
        }
        m30193QR();
    }

    @ClientOnly
    /* renamed from: QW */
    public boolean mo961QW() {
        switch (bFf().mo6893i(_f_hasHollowField_0020_0028_0029Z)) {
            case 0:
                return false;
            case 2:
                return ((Boolean) bFf().mo5606d(new aCE(this, _f_hasHollowField_0020_0028_0029Z, new Object[0]))).booleanValue();
            case 3:
                bFf().mo5606d(new aCE(this, _f_hasHollowField_0020_0028_0029Z, new Object[0]));
                break;
        }
        return m30194QV();
    }

    /* renamed from: T */
    public C6494any mo11T() {
        return ___iScriptClass;
    }

    /* renamed from: T */
    public boolean mo18256T(Actor cr) {
        switch (bFf().mo6893i(bpy)) {
            case 0:
                return false;
            case 2:
                return ((Boolean) bFf().mo5606d(new aCE(this, bpy, new Object[]{cr}))).booleanValue();
            case 3:
                bFf().mo5606d(new aCE(this, bpy, new Object[]{cr}));
                break;
        }
        return m30198S(cr);
    }

    @C5566aOg
    /* renamed from: TJ */
    public AdapterLikedList<ShipAdapter> mo5353TJ() {
        switch (bFf().mo6893i(aPf)) {
            case 0:
                return null;
            case 2:
                return (AdapterLikedList) bFf().mo5606d(new aCE(this, aPf, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, aPf, new Object[0]));
                break;
        }
        return m30201TI();
    }

    /* renamed from: TL */
    public void mo12905TL() {
        switch (bFf().mo6893i(aPj)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, aPj, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, aPj, new Object[0]));
                break;
        }
        m30202TK();
    }

    /* renamed from: TN */
    public void mo12906TN() {
        switch (bFf().mo6893i(aPk)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, aPk, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, aPk, new Object[0]));
                break;
        }
        m30203TM();
    }

    /* renamed from: TR */
    public void mo18257TR() {
        switch (bFf().mo6893i(aPm)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, aPm, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, aPm, new Object[0]));
                break;
        }
        m30206TQ();
    }

    /* renamed from: U */
    public C2491fm[] mo12U() {
        return _m_methods;
    }

    /* access modifiers changed from: protected */
    @C6580apg
    /* renamed from: V */
    public void mo18258V(Actor cr) {
        switch (bFf().mo6893i(bpE)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, bpE, new Object[]{cr}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, bpE, new Object[]{cr}));
                break;
        }
        m30208U(cr);
    }

    /* renamed from: VF */
    public float mo962VF() {
        switch (bFf().mo6893i(aTe)) {
            case 0:
                return 0.0f;
            case 2:
                return ((Float) bFf().mo5606d(new aCE(this, aTe, new Object[0]))).floatValue();
            case 3:
                bFf().mo5606d(new aCE(this, aTe, new Object[0]));
                break;
        }
        return m30211VE();
    }

    /* renamed from: VH */
    public float mo963VH() {
        switch (bFf().mo6893i(aTf)) {
            case 0:
                return 0.0f;
            case 2:
                return ((Float) bFf().mo5606d(new aCE(this, aTf, new Object[0]))).floatValue();
            case 3:
                bFf().mo5606d(new aCE(this, aTf, new Object[0]));
                break;
        }
        return m30212VG();
    }

    /* renamed from: VL */
    public /* bridge */ /* synthetic */ List mo9103VL() {
        switch (bFf().mo6893i(aTk)) {
            case 0:
                return null;
            case 2:
                return (List) bFf().mo5606d(new aCE(this, aTk, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, aTk, new Object[0]));
                break;
        }
        return m30215VK();
    }

    /* renamed from: W */
    public Object mo13W() {
        return new aMX(this);
    }

    @C4034yP
    @C5566aOg
    @C2499fr(mo18855qf = {"b81f2b0b73838e82b8bf1f30b4fc2f96"})
    /* renamed from: X */
    public void mo2967X(Actor cr) {
        switch (bFf().mo6893i(bri)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, bri, new Object[]{cr}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, bri, new Object[]{cr}));
                break;
        }
        m30216W(cr);
    }

    @C5566aOg
    /* renamed from: Y */
    public void mo18259Y(Asset tCVar) {
        switch (bFf().mo6893i(bru)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, bru, new Object[]{tCVar}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, bru, new Object[]{tCVar}));
                break;
        }
        m30218X(tCVar);
    }

    /* renamed from: YE */
    public float mo16294YE() {
        switch (bFf().mo6893i(bbS)) {
            case 0:
                return 0.0f;
            case 2:
                return ((Float) bFf().mo5606d(new aCE(this, bbS, new Object[0]))).floatValue();
            case 3:
                bFf().mo5606d(new aCE(this, bbS, new Object[0]));
                break;
        }
        return m30220YD();
    }

    /* renamed from: YG */
    public float mo16295YG() {
        switch (bFf().mo6893i(bbU)) {
            case 0:
                return 0.0f;
            case 2:
                return ((Float) bFf().mo5606d(new aCE(this, bbU, new Object[0]))).floatValue();
            case 3:
                bFf().mo5606d(new aCE(this, bbU, new Object[0]));
                break;
        }
        return m30221YF();
    }

    @C4034yP
    @C2499fr
    @ClientOnly
    /* renamed from: Z */
    public void mo18260Z(Actor cr) {
        switch (bFf().mo6893i(brj)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, brj, new Object[]{cr}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, brj, new Object[]{cr}));
                break;
        }
        m30219Y(cr);
    }

    /* renamed from: a */
    public Object mo14a(C0495Gr gr) {
        Object[] args = gr.getArgs();
        switch (gr.mo2417hq() - Pawn._m_methodCount) {
            case 0:
                acP();
                return null;
            case 1:
                afB();
                return null;
            case 2:
                acK();
                return null;
            case 3:
                m30243a((Weapon) args[0], (C3904wY) args[1]);
                return null;
            case 4:
                m30287c((Weapon) args[0], (C3904wY) args[1]);
                return null;
            case 5:
                m30331e((Weapon) args[0], (C3904wY) args[1]);
                return null;
            case 6:
                m30342g((Weapon) args[0], (C3904wY) args[1]);
                return null;
            case 7:
                m30344h((Weapon) args[0]);
                return null;
            case 8:
                afD();
                return null;
            case 9:
                return new Boolean(m30198S((Actor) args[0]));
            case 10:
                return new Boolean(m30292c((C1722ZT<UserConnection>) (C1722ZT) args[0]));
            case 11:
                m30230a((Actor) args[0], (C5260aCm) args[1], (Vec3f) args[2], (Vec3f) args[3]);
                return null;
            case 12:
                m30238a((Component) args[0]);
                return null;
            case 13:
                m30284c((Component) args[0]);
                return null;
            case 14:
                afF();
                return null;
            case 15:
                m30199TG();
                return null;
            case 16:
                m30204TO();
                return null;
            case 17:
                m30228a((Actor.C0200h) args[0]);
                return null;
            case 18:
                m30295cX(((Long) args[0]).longValue());
                return null;
            case 19:
                return m30299d((Space) args[0]);
            case 20:
                return m30282c((Space) args[0], ((Long) args[1]).longValue());
            case 21:
                m30208U((Actor) args[0]);
                return null;
            case 22:
                m30296cZ(((Long) args[0]).longValue());
                return null;
            case 23:
                m30283c((Actor) args[0], (C5260aCm) args[1], (Vec3f) args[2], (Vec3f) args[3]);
                return null;
            case 24:
                m30237a((ShipSector) args[0]);
                return null;
            case 25:
                m30244a((ShipStructure) args[0]);
                return null;
            case 26:
                m30339fg();
                return null;
            case 27:
                return m30201TI();
            case 28:
                return afH();
            case 29:
                return afJ();
            case 30:
                return new Float(m30221YF());
            case 31:
                return afK();
            case 32:
                return afM();
            case 33:
                return new Boolean(afO());
            case 34:
                return afQ();
            case 35:
                return afS();
            case 36:
                return afU();
            case 37:
                return new Float(m30360js());
            case 38:
                return afW();
            case 39:
                return m30399zs();
            case 40:
                return new Float(m30211VE());
            case 41:
                return new Float(m30212VG());
            case 42:
                return new Float(afY());
            case 43:
                return new Float(aga());
            case 44:
                return new Float(agc());
            case 45:
                return new Float(age());
            case 46:
                return new Float(m30213VI());
            case 47:
                return new Float(m30214VJ());
            case 48:
                return agg();
            case 49:
                return agi();
            case 50:
                return agk();
            case 51:
                return agm();
            case 52:
                return new Float(ago());
            case 53:
                return m30352io();
            case 54:
                return m30353iq();
            case 55:
                return m30355iz();
            case 56:
                return m30400zu();
            case 57:
                return m30183B((Vec3f) args[0]);
            case 58:
                return agq();
            case 59:
                return ags();
            case 60:
                return agu();
            case 61:
                return agw();
            case 62:
                return acY();
            case 63:
                return new Float(agy());
            case 64:
                return m30354it();
            case 65:
                return agA();
            case 66:
                return new Float(agC());
            case 67:
                return agE();
            case 68:
                return agG();
            case 69:
                return agI();
            case 70:
                return new Float(m30220YD());
            case 71:
                return m30298d((C3904wY) args[0]);
            case 72:
                return m30335f((C3904wY) args[0]);
            case 73:
                return agK();
            case 74:
                return new Boolean(m30347h((C3904wY) args[0]));
            case 75:
                agM();
                return null;
            case 76:
                return new Float(agO());
            case 77:
                return new Float(agQ());
            case 78:
                return new Float(agS());
            case 79:
                return new Float(agU());
            case 80:
                return new Float(agW());
            case 81:
                return new Boolean(m30333eK(((Integer) args[0]).intValue()));
            case 82:
                return new Boolean(agY());
            case 83:
                return new Boolean(aha());
            case 84:
                return new Boolean(ahc());
            case 85:
                return new Boolean(ahe());
            case 86:
                return new Boolean(ahg());
            case 87:
                return new Boolean(ahi());
            case 88:
                return new Boolean(ahj());
            case 89:
                return new Boolean(m30359j((C3904wY) args[0]));
            case 90:
                ahl();
                return null;
            case 91:
                m30231a((Actor) args[0], (Vec3f) args[1], (Vec3f) args[2], ((Float) args[3]).floatValue(), (C6339akz) args[4]);
                return null;
            case 92:
                m30330e((Component) args[0]);
                return null;
            case 93:
                m30340g((Component) args[0]);
                return null;
            case 94:
                return ahn();
            case 95:
                m30239a((C5260aCm) args[0], (Pawn) args[1]);
                return null;
            case 96:
                m30247a((Pawn) args[0], (Actor) args[1]);
                return null;
            case 97:
                m30286c((Vec3d) args[0], (Node) args[1], (StellarSystem) args[2]);
                return null;
            case 98:
                m30241a((Vec3d) args[0], (Node) args[1], (StellarSystem) args[2], (Set<Loot>) (Set) args[3]);
                return null;
            case 99:
                m30275b((Hull) args[0], (C5260aCm) args[1], (Actor) args[2]);
                return null;
            case 100:
                m30285c((C5260aCm) args[0], (Pawn) args[1]);
                return null;
            case 101:
                m30234a((C0665JT) args[0]);
                return null;
            case 102:
                ahp();
                return null;
            case 103:
                m30350iB();
                return null;
            case 104:
                ahq();
                return null;
            case 105:
                ahs();
                return null;
            case 106:
                m30362l((Actor) args[0]);
                return null;
            case 107:
                m30348i((Component) args[0]);
                return null;
            case 108:
                ahu();
                return null;
            case 109:
                m30376qU();
                return null;
            case 110:
                return new Boolean(m30256a((Controller) args[0], (Actor) args[1]));
            case 111:
                m30361k((Component) args[0]);
                return null;
            case 112:
                ahw();
                return null;
            case 113:
                m30358j((Weapon) args[0]);
                return null;
            case 114:
                m30364l((Weapon) args[0]);
                return null;
            case 115:
                m30365l((C3904wY) args[0]);
                return null;
            case 116:
                m30369n((C3904wY) args[0]);
                return null;
            case 117:
                m30373p((C3904wY) args[0]);
                return null;
            case 118:
                m30380r((C3904wY) args[0]);
                return null;
            case 119:
                m30386t((C3904wY) args[0]);
                return null;
            case 120:
                m30390v((C3904wY) args[0]);
                return null;
            case 121:
                m30368n((Weapon) args[0]);
                return null;
            case 122:
                m30372p((Weapon) args[0]);
                return null;
            case 123:
                m30255a((SPitchedSet) args[0]);
                return null;
            case 124:
                m30357j((Character) args[0]);
                return null;
            case 125:
                m30280bs(((Boolean) args[0]).booleanValue());
                return null;
            case 126:
                m30226a(((Integer) args[0]).intValue(), (Component) args[1]);
                return null;
            case 127:
                m30216W((Actor) args[0]);
                return null;
            case 128:
                m30219Y((Actor) args[0]);
                return null;
            case 129:
                m30257aa((Actor) args[0]);
                return null;
            case 130:
                m30260ac((Actor) args[0]);
                return null;
            case 131:
                m30261ae((Actor) args[0]);
                return null;
            case 132:
                ahx();
                return null;
            case 133:
                return ahz();
            case 134:
                m30288c((PlayerController) args[0]);
                return null;
            case 135:
                ahB();
                return null;
            case 136:
                m30329e((Space) args[0], ((Long) args[1]).longValue());
                return null;
            case 137:
                m30279bZ((String) args[0]);
                return null;
            case 138:
                m30277b((Iterator<Item>) (Iterator) args[0]);
                return null;
            case 139:
                m30218X((Asset) args[0]);
                return null;
            case 140:
                m30251a((Asset) args[0], (Vec3d) args[1]);
                return null;
            case 141:
                m30222Z((Asset) args[0]);
                return null;
            case 142:
                m30289c((Asset) args[0], (Vec3d) args[1]);
                return null;
            case 143:
                return m30224a((Actor) args[0], (Vec3d) args[1], ((Boolean) args[2]).booleanValue());
            case 144:
                m30393x((C3904wY) args[0]);
                return null;
            case 145:
                ahD();
                return null;
            case 146:
                ahF();
                return null;
            case 147:
                m30395z((C3904wY) args[0]);
                return null;
            case 148:
                ahH();
                return null;
            case 149:
                ahJ();
                return null;
            case 150:
                m30328e((Actor) args[0], (C5260aCm) args[1], (Vec3f) args[2], (Vec3f) args[3]);
                return null;
            case 151:
                m30229a((Actor) args[0], ((Float) args[1]).floatValue());
                return null;
            case 152:
                m30318db(((Long) args[0]).longValue());
                return null;
            case 153:
                m30320dd(((Long) args[0]).longValue());
                return null;
            case 154:
                return new Boolean(ahL());
            case 155:
                m30281bu(((Boolean) args[0]).booleanValue());
                return null;
            case 156:
                return ahN();
            case 157:
                return m30263au();
            case 158:
                return m30389uV();
            case 159:
                return ahP();
            case 160:
                m30401zw();
                return null;
            case 161:
                m30259ab((Asset) args[0]);
                return null;
            case 162:
                ahR();
                return null;
            case 163:
                ahT();
                return null;
            case 164:
                ahV();
                return null;
            case 165:
                ahX();
                return null;
            case 166:
                ahZ();
                return null;
            case 167:
                m30264ay(((Float) args[0]).floatValue());
                return null;
            case 168:
                aib();
                return null;
            case 169:
                aid();
                return null;
            case 170:
                aif();
                return null;
            case 171:
                m30338f((SceneObject) args[0]);
                return null;
            case 172:
                aih();
                return null;
            case 173:
                aij();
                return null;
            case 174:
                m30346h((SceneObject) args[0]);
                return null;
            case 175:
                return ail();
            case 176:
                m30250a((Hull) args[0], (C5663aRz) args[1], args[2]);
                return null;
            case 177:
                m30227a(((Long) args[0]).longValue(), ((Boolean) args[1]).booleanValue());
                return null;
            case 178:
                aim();
                return null;
            case 179:
                m30185D((Vec3f) args[0]);
                return null;
            case 180:
                return aio();
            case 181:
                m30225a(((Integer) args[0]).intValue(), ((Long) args[1]).longValue(), (Collection<C6625aqZ>) (Collection) args[2], (C6705asB) args[3]);
                return null;
            case 182:
                return new Float(aiq());
            case 183:
                m30232a((C0286Dh) args[0]);
                return null;
            case 184:
                m30233a((C0286Dh) args[0], ((Float) args[1]).floatValue());
                return null;
            case 185:
                return ais();
            case KeyCode.cso:
                return aiu();
            case 187:
                return aiw();
            case 188:
                return new Float(aiy());
            case 189:
                return new Float(aiz());
            case 190:
                return aiA();
            case 191:
                return new Integer(m30366ls());
            case 192:
                return m30223a((SectorCategory) args[0]);
            case 193:
                return m30182B((C3904wY) args[0]);
            case 194:
                m30235a((StellarSystem) args[0]);
                return null;
            case 195:
                m30202TK();
                return null;
            case 196:
                m30203TM();
                return null;
            case 197:
                m30206TQ();
                return null;
            case 198:
                return new Boolean(m30194QV());
            case 199:
                m30193QR();
                return null;
            case 200:
                return new Boolean(m30304d((C5663aRz) args[0]));
            case 201:
                m30269b((aDJ) args[0], (C5663aRz) args[1], args[2]);
                return null;
            case 202:
                return m30215VK();
            case KeyCode.csG:
                return m30377qW();
            case KeyCode.csH:
                return aiC();
            default:
                return super.mo14a(gr);
        }
    }

    /* renamed from: a */
    public /* bridge */ /* synthetic */ void mo1143a(aDJ adj, C5663aRz arz, Object obj) {
        switch (bFf().mo6893i(bsp)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, bsp, new Object[]{adj, arz, obj}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, bsp, new Object[]{adj, arz, obj}));
                break;
        }
        m30269b(adj, arz, obj);
    }

    /* renamed from: a */
    public void mo8536a(Hull jRVar, C5260aCm acm, Actor cr) {
        switch (bFf().mo6893i(f7134Wk)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f7134Wk, new Object[]{jRVar, acm, cr}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f7134Wk, new Object[]{jRVar, acm, cr}));
                break;
        }
        m30275b(jRVar, acm, cr);
    }

    /* renamed from: a */
    public void mo15a(Collection collection, C0495Gr gr) {
        switch (gr.mo2417hq() - Pawn._m_methodCount) {
            case 12:
                m30276b(collection, gr);
                return;
            case 13:
                m30290c(collection, gr);
                return;
            case 21:
                m30303d(collection, gr);
                return;
            default:
                super.mo15a(collection, gr);
                return;
        }
    }

    @C4034yP
    @C2499fr
    @ClientOnly
    /* renamed from: aa */
    public void mo18261aa(Asset tCVar) {
        switch (bFf().mo6893i(brw)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, brw, new Object[]{tCVar}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, brw, new Object[]{tCVar}));
                break;
        }
        m30222Z(tCVar);
    }

    @C4034yP
    @C5566aOg
    @C2499fr
    public void abort() {
        switch (bFf().mo6893i(bkd)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, bkd, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, bkd, new Object[0]));
                break;
        }
        acP();
    }

    @C4034yP
    @C2499fr
    @ClientOnly
    /* renamed from: ac */
    public void mo18262ac(Asset tCVar) {
        switch (bFf().mo6893i(brL)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, brL, new Object[]{tCVar}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, brL, new Object[]{tCVar}));
                break;
        }
        m30259ab(tCVar);
    }

    public C6809auB.C1996a acZ() {
        switch (bFf().mo6893i(bkk)) {
            case 0:
                return null;
            case 2:
                return (C6809auB.C1996a) bFf().mo5606d(new aCE(this, bkk, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, bkk, new Object[0]));
                break;
        }
        return acY();
    }

    @C4034yP
    @C5566aOg
    @C2499fr
    public void activate() {
        switch (bFf().mo6893i(bka)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, bka, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, bka, new Object[0]));
                break;
        }
        acK();
    }

    /* renamed from: ad */
    public void mo18263ad(Actor cr) {
        switch (bFf().mo6893i(brl)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, brl, new Object[]{cr}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, brl, new Object[]{cr}));
                break;
        }
        m30260ac(cr);
    }

    @C4034yP
    @C5566aOg
    @C2499fr
    public void afC() {
        switch (bFf().mo6893i(bpr)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, bpr, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, bpr, new Object[0]));
                break;
        }
        afB();
    }

    public void afE() {
        switch (bFf().mo6893i(bpx)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, bpx, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, bpx, new Object[0]));
                break;
        }
        afD();
    }

    public void afG() {
        switch (bFf().mo6893i(bpB)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, bpB, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, bpB, new Object[0]));
                break;
        }
        afF();
    }

    public CargoHold afI() {
        switch (bFf().mo6893i(bpI)) {
            case 0:
                return null;
            case 2:
                return (CargoHold) bFf().mo5606d(new aCE(this, bpI, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, bpI, new Object[0]));
                break;
        }
        return afH();
    }

    public CruiseSpeed afL() {
        switch (bFf().mo6893i(bpK)) {
            case 0:
                return null;
            case 2:
                return (CruiseSpeed) bFf().mo5606d(new aCE(this, bpK, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, bpK, new Object[0]));
                break;
        }
        return afK();
    }

    public C6853aut afN() {
        switch (bFf().mo6893i(bpL)) {
            case 0:
                return null;
            case 2:
                return (C6853aut) bFf().mo5606d(new aCE(this, bpL, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, bpL, new Object[0]));
                break;
        }
        return afM();
    }

    @ClientOnly
    public boolean afP() {
        switch (bFf().mo6893i(bpM)) {
            case 0:
                return false;
            case 2:
                return ((Boolean) bFf().mo5606d(new aCE(this, bpM, new Object[0]))).booleanValue();
            case 3:
                bFf().mo5606d(new aCE(this, bpM, new Object[0]));
                break;
        }
        return afO();
    }

    public Weapon afR() {
        switch (bFf().mo6893i(bpN)) {
            case 0:
                return null;
            case 2:
                return (Weapon) bFf().mo5606d(new aCE(this, bpN, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, bpN, new Object[0]));
                break;
        }
        return afQ();
    }

    public Weapon afT() {
        switch (bFf().mo6893i(bpO)) {
            case 0:
                return null;
            case 2:
                return (Weapon) bFf().mo5606d(new aCE(this, bpO, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, bpO, new Object[0]));
                break;
        }
        return afS();
    }

    public Vec3f afV() {
        switch (bFf().mo6893i(bpP)) {
            case 0:
                return null;
            case 2:
                return (Vec3f) bFf().mo5606d(new aCE(this, bpP, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, bpP, new Object[0]));
                break;
        }
        return afU();
    }

    @ClientOnly
    public SPitchedSet afX() {
        switch (bFf().mo6893i(bpQ)) {
            case 0:
                return null;
            case 2:
                return (SPitchedSet) bFf().mo5606d(new aCE(this, bpQ, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, bpQ, new Object[0]));
                break;
        }
        return afW();
    }

    public float afZ() {
        switch (bFf().mo6893i(bpR)) {
            case 0:
                return 0.0f;
            case 2:
                return ((Float) bFf().mo5606d(new aCE(this, bpR, new Object[0]))).floatValue();
            case 3:
                bFf().mo5606d(new aCE(this, bpR, new Object[0]));
                break;
        }
        return afY();
    }

    public Actor agB() {
        switch (bFf().mo6893i(bqg)) {
            case 0:
                return null;
            case 2:
                return (Actor) bFf().mo5606d(new aCE(this, bqg, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, bqg, new Object[0]));
                break;
        }
        return agA();
    }

    public float agD() {
        switch (bFf().mo6893i(bqh)) {
            case 0:
                return 0.0f;
            case 2:
                return ((Float) bFf().mo5606d(new aCE(this, bqh, new Object[0]))).floatValue();
            case 3:
                bFf().mo5606d(new aCE(this, bqh, new Object[0]));
                break;
        }
        return agC();
    }

    public C3438ra<Turret> agF() {
        switch (bFf().mo6893i(bqi)) {
            case 0:
                return null;
            case 2:
                return (C3438ra) bFf().mo5606d(new aCE(this, bqi, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, bqi, new Object[0]));
                break;
        }
        return agE();
    }

    public ShipType agH() {
        switch (bFf().mo6893i(bqj)) {
            case 0:
                return null;
            case 2:
                return (ShipType) bFf().mo5606d(new aCE(this, bqj, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, bqj, new Object[0]));
                break;
        }
        return agG();
    }

    public String agJ() {
        switch (bFf().mo6893i(bqk)) {
            case 0:
                return null;
            case 2:
                return (String) bFf().mo5606d(new aCE(this, bqk, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, bqk, new Object[0]));
                break;
        }
        return agI();
    }

    public List<Weapon> agL() {
        switch (bFf().mo6893i(bqn)) {
            case 0:
                return null;
            case 2:
                return (List) bFf().mo5606d(new aCE(this, bqn, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, bqn, new Object[0]));
                break;
        }
        return agK();
    }

    public float agR() {
        switch (bFf().mo6893i(bqr)) {
            case 0:
                return 0.0f;
            case 2:
                return ((Float) bFf().mo5606d(new aCE(this, bqr, new Object[0]))).floatValue();
            case 3:
                bFf().mo5606d(new aCE(this, bqr, new Object[0]));
                break;
        }
        return agQ();
    }

    public boolean agZ() {
        switch (bFf().mo6893i(bqw)) {
            case 0:
                return false;
            case 2:
                return ((Boolean) bFf().mo5606d(new aCE(this, bqw, new Object[0]))).booleanValue();
            case 3:
                bFf().mo5606d(new aCE(this, bqw, new Object[0]));
                break;
        }
        return agY();
    }

    public float agb() {
        switch (bFf().mo6893i(bpS)) {
            case 0:
                return 0.0f;
            case 2:
                return ((Float) bFf().mo5606d(new aCE(this, bpS, new Object[0]))).floatValue();
            case 3:
                bFf().mo5606d(new aCE(this, bpS, new Object[0]));
                break;
        }
        return aga();
    }

    public float agd() {
        switch (bFf().mo6893i(bpT)) {
            case 0:
                return 0.0f;
            case 2:
                return ((Float) bFf().mo5606d(new aCE(this, bpT, new Object[0]))).floatValue();
            case 3:
                bFf().mo5606d(new aCE(this, bpT, new Object[0]));
                break;
        }
        return agc();
    }

    public float agf() {
        switch (bFf().mo6893i(bpU)) {
            case 0:
                return 0.0f;
            case 2:
                return ((Float) bFf().mo5606d(new aCE(this, bpU, new Object[0]))).floatValue();
            case 3:
                bFf().mo5606d(new aCE(this, bpU, new Object[0]));
                break;
        }
        return age();
    }

    public C6853aut agh() {
        switch (bFf().mo6893i(bpV)) {
            case 0:
                return null;
            case 2:
                return (C6853aut) bFf().mo5606d(new aCE(this, bpV, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, bpV, new Object[0]));
                break;
        }
        return agg();
    }

    public Character agj() {
        switch (bFf().mo6893i(bpW)) {
            case 0:
                return null;
            case 2:
                return (Character) bFf().mo5606d(new aCE(this, bpW, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, bpW, new Object[0]));
                break;
        }
        return agi();
    }

    @C5566aOg
    @C2499fr
    public String agl() {
        switch (bFf().mo6893i(bpX)) {
            case 0:
                return null;
            case 2:
                return (String) bFf().mo5606d(new aCE(this, bpX, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, bpX, new Object[0]));
                break;
        }
        return agk();
    }

    public C3438ra<Component> agn() {
        switch (bFf().mo6893i(bpY)) {
            case 0:
                return null;
            case 2:
                return (C3438ra) bFf().mo5606d(new aCE(this, bpY, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, bpY, new Object[0]));
                break;
        }
        return agm();
    }

    public float agp() {
        switch (bFf().mo6893i(bpZ)) {
            case 0:
                return 0.0f;
            case 2:
                return ((Float) bFf().mo5606d(new aCE(this, bpZ, new Object[0]))).floatValue();
            case 3:
                bFf().mo5606d(new aCE(this, bpZ, new Object[0]));
                break;
        }
        return ago();
    }

    public ShipItem agr() {
        switch (bFf().mo6893i(bqb)) {
            case 0:
                return null;
            case 2:
                return (ShipItem) bFf().mo5606d(new aCE(this, bqb, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, bqb, new Object[0]));
                break;
        }
        return agq();
    }

    public C3438ra<ShipStructure> agt() {
        switch (bFf().mo6893i(bqc)) {
            case 0:
                return null;
            case 2:
                return (C3438ra) bFf().mo5606d(new aCE(this, bqc, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, bqc, new Object[0]));
                break;
        }
        return ags();
    }

    public Pawn agv() {
        switch (bFf().mo6893i(bqd)) {
            case 0:
                return null;
            case 2:
                return (Pawn) bFf().mo5606d(new aCE(this, bqd, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, bqd, new Object[0]));
                break;
        }
        return agu();
    }

    public Vec3f agx() {
        switch (bFf().mo6893i(bqe)) {
            case 0:
                return null;
            case 2:
                return (Vec3f) bFf().mo5606d(new aCE(this, bqe, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, bqe, new Object[0]));
                break;
        }
        return agw();
    }

    public float agz() {
        switch (bFf().mo6893i(bqf)) {
            case 0:
                return 0.0f;
            case 2:
                return ((Float) bFf().mo5606d(new aCE(this, bqf, new Object[0]))).floatValue();
            case 3:
                bFf().mo5606d(new aCE(this, bqf, new Object[0]));
                break;
        }
        return agy();
    }

    public void ahC() {
        switch (bFf().mo6893i(brq)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, brq, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, brq, new Object[0]));
                break;
        }
        ahB();
    }

    @C4034yP
    @C5566aOg
    @C2499fr(mo18855qf = {"b81f2b0b73838e82b8bf1f30b4fc2f96"})
    @Deprecated
    public void ahE() {
        switch (bFf().mo6893i(brA)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, brA, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, brA, new Object[0]));
                break;
        }
        ahD();
    }

    @C4034yP
    @C5566aOg
    @C2499fr(mo18855qf = {"b81f2b0b73838e82b8bf1f30b4fc2f96"})
    @Deprecated
    public void ahG() {
        switch (bFf().mo6893i(brB)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, brB, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, brB, new Object[0]));
                break;
        }
        ahF();
    }

    @C4034yP
    @C5566aOg
    @C2499fr(mo18855qf = {"b81f2b0b73838e82b8bf1f30b4fc2f96"})
    @Deprecated
    public void ahI() {
        switch (bFf().mo6893i(brD)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, brD, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, brD, new Object[0]));
                break;
        }
        ahH();
    }

    @C4034yP
    @C5566aOg
    @C2499fr(mo18855qf = {"b81f2b0b73838e82b8bf1f30b4fc2f96"})
    @Deprecated
    public void ahK() {
        switch (bFf().mo6893i(brE)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, brE, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, brE, new Object[0]));
                break;
        }
        ahJ();
    }

    /* access modifiers changed from: protected */
    public boolean ahM() {
        switch (bFf().mo6893i(brH)) {
            case 0:
                return false;
            case 2:
                return ((Boolean) bFf().mo5606d(new aCE(this, brH, new Object[0]))).booleanValue();
            case 3:
                bFf().mo5606d(new aCE(this, brH, new Object[0]));
                break;
        }
        return ahL();
    }

    public C6901avp ahO() {
        switch (bFf().mo6893i(brJ)) {
            case 0:
                return null;
            case 2:
                return (C6901avp) bFf().mo5606d(new aCE(this, brJ, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, brJ, new Object[0]));
                break;
        }
        return ahN();
    }

    public String ahQ() {
        switch (bFf().mo6893i(brK)) {
            case 0:
                return null;
            case 2:
                return (String) bFf().mo5606d(new aCE(this, brK, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, brK, new Object[0]));
                break;
        }
        return ahP();
    }

    @C5566aOg
    public void ahS() {
        switch (bFf().mo6893i(brM)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, brM, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, brM, new Object[0]));
                break;
        }
        ahR();
    }

    public void ahU() {
        switch (bFf().mo6893i(brN)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, brN, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, brN, new Object[0]));
                break;
        }
        ahT();
    }

    public void ahW() {
        switch (bFf().mo6893i(brO)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, brO, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, brO, new Object[0]));
                break;
        }
        ahV();
    }

    public boolean ahb() {
        switch (bFf().mo6893i(bqx)) {
            case 0:
                return false;
            case 2:
                return ((Boolean) bFf().mo5606d(new aCE(this, bqx, new Object[0]))).booleanValue();
            case 3:
                bFf().mo5606d(new aCE(this, bqx, new Object[0]));
                break;
        }
        return aha();
    }

    public boolean ahd() {
        switch (bFf().mo6893i(bqy)) {
            case 0:
                return false;
            case 2:
                return ((Boolean) bFf().mo5606d(new aCE(this, bqy, new Object[0]))).booleanValue();
            case 3:
                bFf().mo5606d(new aCE(this, bqy, new Object[0]));
                break;
        }
        return ahc();
    }

    public boolean ahf() {
        switch (bFf().mo6893i(bqz)) {
            case 0:
                return false;
            case 2:
                return ((Boolean) bFf().mo5606d(new aCE(this, bqz, new Object[0]))).booleanValue();
            case 3:
                bFf().mo5606d(new aCE(this, bqz, new Object[0]));
                break;
        }
        return ahe();
    }

    public boolean ahh() {
        switch (bFf().mo6893i(bqA)) {
            case 0:
                return false;
            case 2:
                return ((Boolean) bFf().mo5606d(new aCE(this, bqA, new Object[0]))).booleanValue();
            case 3:
                bFf().mo5606d(new aCE(this, bqA, new Object[0]));
                break;
        }
        return ahg();
    }

    @ClientOnly
    public C2279dZ aho() {
        switch (bFf().mo6893i(bqH)) {
            case 0:
                return null;
            case 2:
                return (C2279dZ) bFf().mo5606d(new aCE(this, bqH, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, bqH, new Object[0]));
                break;
        }
        return ahn();
    }

    /* access modifiers changed from: protected */
    public void aht() {
        switch (bFf().mo6893i(bqO)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, bqO, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, bqO, new Object[0]));
                break;
        }
        ahs();
    }

    public void ahv() {
        switch (bFf().mo6893i(bqQ)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, bqQ, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, bqQ, new Object[0]));
                break;
        }
        ahu();
    }

    public List<Item> aiB() {
        switch (bFf().mo6893i(bsl)) {
            case 0:
                return null;
            case 2:
                return (List) bFf().mo5606d(new aCE(this, bsl, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, bsl, new Object[0]));
                break;
        }
        return aiA();
    }

    public /* bridge */ /* synthetic */ C0520HN aiD() {
        switch (bFf().mo6893i(bsq)) {
            case 0:
                return null;
            case 2:
                return (C0520HN) bFf().mo5606d(new aCE(this, bsq, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, bsq, new Object[0]));
                break;
        }
        return aiC();
    }

    public void aia() {
        switch (bFf().mo6893i(brQ)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, brQ, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, brQ, new Object[0]));
                break;
        }
        ahZ();
    }

    public void aic() {
        switch (bFf().mo6893i(brR)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, brR, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, brR, new Object[0]));
                break;
        }
        aib();
    }

    public void aie() {
        switch (bFf().mo6893i(brS)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, brS, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, brS, new Object[0]));
                break;
        }
        aid();
    }

    @ClientOnly
    public void aig() {
        switch (bFf().mo6893i(brT)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, brT, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, brT, new Object[0]));
                break;
        }
        aif();
    }

    @ClientOnly
    public void aii() {
        switch (bFf().mo6893i(brV)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, brV, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, brV, new Object[0]));
                break;
        }
        aih();
    }

    @ClientOnly
    public void aik() {
        switch (bFf().mo6893i(brW)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, brW, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, brW, new Object[0]));
                break;
        }
        aij();
    }

    @C4034yP
    @C5566aOg
    @C2499fr
    public void ain() {
        switch (bFf().mo6893i(bsa)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, bsa, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, bsa, new Object[0]));
                break;
        }
        aim();
    }

    @C1253SX
    public Vec3f aip() {
        switch (bFf().mo6893i(bsc)) {
            case 0:
                return null;
            case 2:
                return (Vec3f) bFf().mo5606d(new aCE(this, bsc, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, bsc, new Object[0]));
                break;
        }
        return aio();
    }

    public float air() {
        switch (bFf().mo6893i(bsd)) {
            case 0:
                return 0.0f;
            case 2:
                return ((Float) bFf().mo5606d(new aCE(this, bsd, new Object[0]))).floatValue();
            case 3:
                bFf().mo5606d(new aCE(this, bsd, new Object[0]));
                break;
        }
        return aiq();
    }

    public Vec3d ait() {
        switch (bFf().mo6893i(bsg)) {
            case 0:
                return null;
            case 2:
                return (Vec3d) bFf().mo5606d(new aCE(this, bsg, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, bsg, new Object[0]));
                break;
        }
        return ais();
    }

    public Vec3d aiv() {
        switch (bFf().mo6893i(bsh)) {
            case 0:
                return null;
            case 2:
                return (Vec3d) bFf().mo5606d(new aCE(this, bsh, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, bsh, new Object[0]));
                break;
        }
        return aiu();
    }

    public Vec3d aix() {
        switch (bFf().mo6893i(bsi)) {
            case 0:
                return null;
            case 2:
                return (Vec3d) bFf().mo5606d(new aCE(this, bsi, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, bsi, new Object[0]));
                break;
        }
        return aiw();
    }

    /* renamed from: b */
    public ShipSector mo18300b(SectorCategory fMVar) {
        switch (bFf().mo6893i(bsm)) {
            case 0:
                return null;
            case 2:
                return (ShipSector) bFf().mo5606d(new aCE(this, bsm, new Object[]{fMVar}));
            case 3:
                bFf().mo5606d(new aCE(this, bsm, new Object[]{fMVar}));
                break;
        }
        return m30223a(fMVar);
    }

    @C5566aOg
    @C2499fr
    /* renamed from: b */
    public Set<Loot> mo18301b(Actor cr, Vec3d ajr, boolean z) {
        switch (bFf().mo6893i(bry)) {
            case 0:
                return null;
            case 2:
                return (Set) bFf().mo5606d(new aCE(this, bry, new Object[]{cr, ajr, new Boolean(z)}));
            case 3:
                bFf().mo5606d(new aCE(this, bry, new Object[]{cr, ajr, new Boolean(z)}));
                break;
        }
        return m30224a(cr, ajr, z);
    }

    @C2198cg
    /* renamed from: b */
    public void mo990b(int i, long j, Collection<C6625aqZ> collection, C6705asB asb) {
        switch (bFf().mo6893i(f7155vq)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f7155vq, new Object[]{new Integer(i), new Long(j), collection, asb}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f7155vq, new Object[]{new Integer(i), new Long(j), collection, asb}));
                break;
        }
        m30225a(i, j, collection, asb);
    }

    /* renamed from: b */
    public void mo991b(long j, boolean z) {
        switch (bFf().mo6893i(f7148uZ)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f7148uZ, new Object[]{new Long(j), new Boolean(z)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f7148uZ, new Object[]{new Long(j), new Boolean(z)}));
                break;
        }
        m30227a(j, z);
    }

    /* renamed from: b */
    public void mo620b(Actor.C0200h hVar) {
        switch (bFf().mo6893i(f7157vs)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f7157vs, new Object[]{hVar}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f7157vs, new Object[]{hVar}));
                break;
        }
        m30228a(hVar);
    }

    @C5566aOg
    /* renamed from: b */
    public void mo16605b(Actor cr, float f) {
        switch (bFf().mo6893i(f7159zr)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f7159zr, new Object[]{cr, new Float(f)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f7159zr, new Object[]{cr, new Float(f)}));
                break;
        }
        m30229a(cr, f);
    }

    @C4034yP
    @C5566aOg
    @C2499fr
    /* renamed from: b */
    public void mo621b(Actor cr, C5260aCm acm, Vec3f vec3f, Vec3f vec3f2) {
        switch (bFf().mo6893i(f7152vm)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f7152vm, new Object[]{cr, acm, vec3f, vec3f2}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f7152vm, new Object[]{cr, acm, vec3f, vec3f2}));
                break;
        }
        m30230a(cr, acm, vec3f, vec3f2);
    }

    /* renamed from: b */
    public void mo992b(Actor cr, Vec3f vec3f, Vec3f vec3f2, float f, C6339akz akz) {
        switch (bFf().mo6893i(f7146uX)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f7146uX, new Object[]{cr, vec3f, vec3f2, new Float(f), akz}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f7146uX, new Object[]{cr, vec3f, vec3f2, new Float(f), akz}));
                break;
        }
        m30231a(cr, vec3f, vec3f2, f, akz);
    }

    @C4034yP
    @C2499fr
    @C1253SX
    /* renamed from: b */
    public void mo18302b(C0286Dh dh) {
        switch (bFf().mo6893i(bse)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, bse, new Object[]{dh}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, bse, new Object[]{dh}));
                break;
        }
        m30232a(dh);
    }

    /* renamed from: b */
    public void mo24b(C0665JT jt) {
        switch (bFf().mo6893i(f7118Do)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f7118Do, new Object[]{jt}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f7118Do, new Object[]{jt}));
                break;
        }
        m30234a(jt);
    }

    /* renamed from: b */
    public void mo993b(StellarSystem jj) {
        switch (bFf().mo6893i(f7123Mq)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f7123Mq, new Object[]{jj}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f7123Mq, new Object[]{jj}));
                break;
        }
        m30235a(jj);
    }

    /* access modifiers changed from: protected */
    /* renamed from: b */
    public void mo16606b(C5260aCm acm, Pawn avi) {
        switch (bFf().mo6893i(bqI)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, bqI, new Object[]{acm, avi}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, bqI, new Object[]{acm, avi}));
                break;
        }
        m30239a(acm, avi);
    }

    /* renamed from: b */
    public void mo2353b(Pawn avi, Actor cr) {
        switch (bFf().mo6893i(f7121Lj)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f7121Lj, new Object[]{avi, cr}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f7121Lj, new Object[]{avi, cr}));
                break;
        }
        m30247a(avi, cr);
    }

    /* renamed from: b */
    public void mo18303b(Hull jRVar, C5663aRz arz, Object obj) {
        switch (bFf().mo6893i(brZ)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, brZ, new Object[]{jRVar, arz, obj}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, brZ, new Object[]{jRVar, arz, obj}));
                break;
        }
        m30250a(jRVar, arz, obj);
    }

    @C5566aOg
    /* renamed from: b */
    public void mo18304b(Asset tCVar, Vec3d ajr) {
        switch (bFf().mo6893i(brv)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, brv, new Object[]{tCVar, ajr}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, brv, new Object[]{tCVar, ajr}));
                break;
        }
        m30251a(tCVar, ajr);
    }

    @ClientOnly
    /* renamed from: b */
    public void mo18305b(SPitchedSet sPitchedSet) {
        switch (bFf().mo6893i(bre)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, bre, new Object[]{sPitchedSet}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, bre, new Object[]{sPitchedSet}));
                break;
        }
        m30255a(sPitchedSet);
    }

    @ClientOnly
    /* renamed from: bt */
    public void mo18306bt(boolean z) {
        switch (bFf().mo6893i(brg)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, brg, new Object[]{new Boolean(z)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, brg, new Object[]{new Boolean(z)}));
                break;
        }
        m30280bs(z);
    }

    @C4034yP
    @C2499fr
    @C1253SX
    /* renamed from: bv */
    public void mo18307bv(boolean z) {
        switch (bFf().mo6893i(brI)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, brI, new Object[]{new Boolean(z)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, brI, new Object[]{new Boolean(z)}));
                break;
        }
        m30281bu(z);
    }

    /* renamed from: c */
    public C5663aRz[] mo25c() {
        return _m_fields;
    }

    @C5566aOg
    /* renamed from: cY */
    public void mo18308cY(long j) {
        switch (bFf().mo6893i(bpC)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, bpC, new Object[]{new Long(j)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, bpC, new Object[]{new Long(j)}));
                break;
        }
        m30295cX(j);
    }

    /* access modifiers changed from: protected */
    /* renamed from: ca */
    public void mo1055ca(String str) {
        switch (bFf().mo6893i(brs)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, brs, new Object[]{str}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, brs, new Object[]{str}));
                break;
        }
        m30279bZ(str);
    }

    /* access modifiers changed from: protected */
    @C2198cg
    /* renamed from: d */
    public C0520HN mo635d(Space ea, long j) {
        switch (bFf().mo6893i(f7147uY)) {
            case 0:
                return null;
            case 2:
                return (C0520HN) bFf().mo5606d(new aCE(this, f7147uY, new Object[]{ea, new Long(j)}));
            case 3:
                bFf().mo5606d(new aCE(this, f7147uY, new Object[]{ea, new Long(j)}));
                break;
        }
        return m30282c(ea, j);
    }

    @C5566aOg
    /* renamed from: d */
    public void mo636d(Actor cr, C5260aCm acm, Vec3f vec3f, Vec3f vec3f2) {
        switch (bFf().mo6893i(f7153vn)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f7153vn, new Object[]{cr, acm, vec3f, vec3f2}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f7153vn, new Object[]{cr, acm, vec3f, vec3f2}));
                break;
        }
        m30283c(cr, acm, vec3f, vec3f2);
    }

    /* access modifiers changed from: protected */
    /* renamed from: d */
    public void mo16612d(C5260aCm acm, Pawn avi) {
        switch (bFf().mo6893i(bqL)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, bqL, new Object[]{acm, avi}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, bqL, new Object[]{acm, avi}));
                break;
        }
        m30285c(acm, avi);
    }

    @C4034yP
    /* renamed from: d */
    public void mo18309d(Vec3d ajr, Node rPVar, StellarSystem jj) {
        switch (bFf().mo6893i(bqJ)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, bqJ, new Object[]{ajr, rPVar, jj}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, bqJ, new Object[]{ajr, rPVar, jj}));
                break;
        }
        m30286c(ajr, rPVar, jj);
    }

    @C4034yP
    @C2499fr
    @ClientOnly
    /* renamed from: d */
    public void mo18310d(Asset tCVar, Vec3d ajr) {
        switch (bFf().mo6893i(brx)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, brx, new Object[]{tCVar, ajr}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, brx, new Object[]{tCVar, ajr}));
                break;
        }
        m30289c(tCVar, ajr);
    }

    @C5472aKq
    /* renamed from: d */
    public boolean mo18311d(C1722ZT<UserConnection> zt) {
        switch (bFf().mo6893i(aRm)) {
            case 0:
                return false;
            case 2:
                return ((Boolean) bFf().mo5606d(new aCE(this, aRm, new Object[]{zt}))).booleanValue();
            case 3:
                bFf().mo5606d(new aCE(this, aRm, new Object[]{zt}));
                break;
        }
        return m30292c(zt);
    }

    @C5566aOg
    /* renamed from: da */
    public void mo18312da(long j) {
        switch (bFf().mo6893i(bpF)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, bpF, new Object[]{new Long(j)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, bpF, new Object[]{new Long(j)}));
                break;
        }
        m30296cZ(j);
    }

    /* renamed from: de */
    public void mo7599de(long j) {
        switch (bFf().mo6893i(brG)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, brG, new Object[]{new Long(j)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, brG, new Object[]{new Long(j)}));
                break;
        }
        m30320dd(j);
    }

    @C5566aOg
    @C2499fr
    public void dispose() {
        switch (bFf().mo6893i(_f_dispose_0020_0028_0029V)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, _f_dispose_0020_0028_0029V, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, _f_dispose_0020_0028_0029V, new Object[0]));
                break;
        }
        m30339fg();
    }

    /* renamed from: e */
    public Weapon mo18313e(C3904wY wYVar) {
        switch (bFf().mo6893i(bql)) {
            case 0:
                return null;
            case 2:
                return (Weapon) bFf().mo5606d(new aCE(this, bql, new Object[]{wYVar}));
            case 3:
                bFf().mo5606d(new aCE(this, bql, new Object[]{wYVar}));
                break;
        }
        return m30298d(wYVar);
    }

    /* access modifiers changed from: protected */
    /* renamed from: e */
    public C6901avp mo18314e(Space ea) {
        switch (bFf().mo6893i(bpD)) {
            case 0:
                return null;
            case 2:
                return (C6901avp) bFf().mo5606d(new aCE(this, bpD, new Object[]{ea}));
            case 3:
                bFf().mo5606d(new aCE(this, bpD, new Object[]{ea}));
                break;
        }
        return m30299d(ea);
    }

    /* renamed from: e */
    public boolean mo18315e(C5663aRz arz) {
        switch (bFf().mo6893i(bso)) {
            case 0:
                return false;
            case 2:
                return ((Boolean) bFf().mo5606d(new aCE(this, bso, new Object[]{arz}))).booleanValue();
            case 3:
                bFf().mo5606d(new aCE(this, bso, new Object[]{arz}));
                break;
        }
        return m30304d(arz);
    }

    @C4034yP
    @C5566aOg
    @C2499fr(mo18857qh = 2)
    /* renamed from: f */
    public void mo1064f(Actor cr, C5260aCm acm, Vec3f vec3f, Vec3f vec3f2) {
        switch (bFf().mo6893i(f7138Wu)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f7138Wu, new Object[]{cr, acm, vec3f, vec3f2}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f7138Wu, new Object[]{cr, acm, vec3f, vec3f2}));
                break;
        }
        m30328e(cr, acm, vec3f, vec3f2);
    }

    @C1253SX
    /* renamed from: f */
    public void mo1065f(Space ea, long j) {
        switch (bFf().mo6893i(brr)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, brr, new Object[]{ea, new Long(j)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, brr, new Object[]{ea, new Long(j)}));
                break;
        }
        m30329e(ea, j);
    }

    @C5566aOg
    /* renamed from: f */
    public void mo18316f(Component abl) {
        switch (bFf().mo6893i(bqF)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, bqF, new Object[]{abl}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, bqF, new Object[]{abl}));
                break;
        }
        m30330e(abl);
    }

    /* renamed from: g */
    public List<Component> mo18317g(C3904wY wYVar) {
        switch (bFf().mo6893i(bqm)) {
            case 0:
                return null;
            case 2:
                return (List) bFf().mo5606d(new aCE(this, bqm, new Object[]{wYVar}));
            case 3:
                bFf().mo5606d(new aCE(this, bqm, new Object[]{wYVar}));
                break;
        }
        return m30335f(wYVar);
    }

    @ClientOnly
    public Color getColor() {
        switch (bFf().mo6893i(brY)) {
            case 0:
                return null;
            case 2:
                return (Color) bFf().mo5606d(new aCE(this, brY, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, brY, new Object[0]));
                break;
        }
        return ail();
    }

    public String getName() {
        switch (bFf().mo6893i(f7124Pf)) {
            case 0:
                return null;
            case 2:
                return (String) bFf().mo5606d(new aCE(this, f7124Pf, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f7124Pf, new Object[0]));
                break;
        }
        return m30389uV();
    }

    public /* bridge */ /* synthetic */ Object getType() {
        switch (bFf().mo6893i(_f_getType_0020_0028_0029Ljava_002flang_002fObject_003b)) {
            case 0:
                return null;
            case 2:
                return bFf().mo5606d(new aCE(this, _f_getType_0020_0028_0029Ljava_002flang_002fObject_003b, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, _f_getType_0020_0028_0029Ljava_002flang_002fObject_003b, new Object[0]));
                break;
        }
        return m30377qW();
    }

    @C5566aOg
    /* renamed from: h */
    public void mo18319h(Component abl) {
        switch (bFf().mo6893i(bqG)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, bqG, new Object[]{abl}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, bqG, new Object[]{abl}));
                break;
        }
        m30340g(abl);
    }

    /* renamed from: hb */
    public Controller mo2998hb() {
        switch (bFf().mo6893i(bpJ)) {
            case 0:
                return null;
            case 2:
                return (Controller) bFf().mo5606d(new aCE(this, bpJ, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, bpJ, new Object[0]));
                break;
        }
        return afJ();
    }

    @C5566aOg
    /* renamed from: i */
    public void mo18320i(Weapon adv) {
        switch (bFf().mo6893i(bpw)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, bpw, new Object[]{adv}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, bpw, new Object[]{adv}));
                break;
        }
        m30344h(adv);
    }

    @ClientOnly
    /* renamed from: i */
    public void mo18321i(SceneObject sceneObject) {
        switch (bFf().mo6893i(brX)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, brX, new Object[]{sceneObject}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, brX, new Object[]{sceneObject}));
                break;
        }
        m30346h(sceneObject);
    }

    /* renamed from: i */
    public boolean mo18322i(C3904wY wYVar) {
        switch (bFf().mo6893i(bqo)) {
            case 0:
                return false;
            case 2:
                return ((Boolean) bFf().mo5606d(new aCE(this, bqo, new Object[]{wYVar}))).booleanValue();
            case 3:
                bFf().mo5606d(new aCE(this, bqo, new Object[]{wYVar}));
                break;
        }
        return m30347h(wYVar);
    }

    /* renamed from: iA */
    public String mo647iA() {
        switch (bFf().mo6893i(f7154vo)) {
            case 0:
                return null;
            case 2:
                return (String) bFf().mo5606d(new aCE(this, f7154vo, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f7154vo, new Object[0]));
                break;
        }
        return m30355iz();
    }

    @ClientOnly
    /* renamed from: iC */
    public void mo1076iC() {
        switch (bFf().mo6893i(f7156vr)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f7156vr, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f7156vr, new Object[0]));
                break;
        }
        m30350iB();
    }

    /* renamed from: ip */
    public String mo648ip() {
        switch (bFf().mo6893i(f7149vc)) {
            case 0:
                return null;
            case 2:
                return (String) bFf().mo5606d(new aCE(this, f7149vc, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f7149vc, new Object[0]));
                break;
        }
        return m30352io();
    }

    /* renamed from: ir */
    public String mo649ir() {
        switch (bFf().mo6893i(f7150ve)) {
            case 0:
                return null;
            case 2:
                return (String) bFf().mo5606d(new aCE(this, f7150ve, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f7150ve, new Object[0]));
                break;
        }
        return m30353iq();
    }

    public boolean isEmpty() {
        switch (bFf().mo6893i(bqB)) {
            case 0:
                return false;
            case 2:
                return ((Boolean) bFf().mo5606d(new aCE(this, bqB, new Object[0]))).booleanValue();
            case 3:
                bFf().mo5606d(new aCE(this, bqB, new Object[0]));
                break;
        }
        return ahi();
    }

    /* renamed from: iu */
    public String mo651iu() {
        switch (bFf().mo6893i(f7151vi)) {
            case 0:
                return null;
            case 2:
                return (String) bFf().mo5606d(new aCE(this, f7151vi, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f7151vi, new Object[0]));
                break;
        }
        return m30354it();
    }

    /* renamed from: jt */
    public float mo16301jt() {
        switch (bFf().mo6893i(f7158yg)) {
            case 0:
                return 0.0f;
            case 2:
                return ((Float) bFf().mo5606d(new aCE(this, f7158yg, new Object[0]))).floatValue();
            case 3:
                bFf().mo5606d(new aCE(this, f7158yg, new Object[0]));
                break;
        }
        return m30360js();
    }

    /* renamed from: k */
    public void mo18324k(Character acx) {
        switch (bFf().mo6893i(brf)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, brf, new Object[]{acx}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, brf, new Object[]{acx}));
                break;
        }
        m30357j(acx);
    }

    @C4034yP
    @C5566aOg
    @C2499fr
    @Deprecated
    /* renamed from: k */
    public void mo9105k(Weapon adv) {
        switch (bFf().mo6893i(bqU)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, bqU, new Object[]{adv}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, bqU, new Object[]{adv}));
                break;
        }
        m30358j(adv);
    }

    /* renamed from: k */
    public boolean mo18325k(C3904wY wYVar) {
        switch (bFf().mo6893i(bqD)) {
            case 0:
                return false;
            case 2:
                return ((Boolean) bFf().mo5606d(new aCE(this, bqD, new Object[]{wYVar}))).booleanValue();
            case 3:
                bFf().mo5606d(new aCE(this, bqD, new Object[]{wYVar}));
                break;
        }
        return m30359j(wYVar);
    }

    /* renamed from: lt */
    public int mo18326lt() {
        switch (bFf().mo6893i(f7117Cq)) {
            case 0:
                return 0;
            case 2:
                return ((Integer) bFf().mo5606d(new aCE(this, f7117Cq, new Object[0]))).intValue();
            case 3:
                bFf().mo5606d(new aCE(this, f7117Cq, new Object[0]));
                break;
        }
        return m30366ls();
    }

    /* renamed from: m */
    public void mo18227m(Actor cr) {
        switch (bFf().mo6893i(f7116CB)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f7116CB, new Object[]{cr}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f7116CB, new Object[]{cr}));
                break;
        }
        m30362l(cr);
    }

    @C4034yP
    @C5566aOg
    @C2499fr
    @Deprecated
    /* renamed from: m */
    public void mo18327m(Weapon adv) {
        switch (bFf().mo6893i(bqV)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, bqV, new Object[]{adv}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, bqV, new Object[]{adv}));
                break;
        }
        m30364l(adv);
    }

    /* renamed from: o */
    public void mo18328o(Weapon adv) {
        switch (bFf().mo6893i(brc)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, brc, new Object[]{adv}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, brc, new Object[]{adv}));
                break;
        }
        m30368n(adv);
    }

    /* renamed from: q */
    public void mo18329q(Weapon adv) {
        switch (bFf().mo6893i(brd)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, brd, new Object[]{adv}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, brd, new Object[]{adv}));
                break;
        }
        m30372p(adv);
    }

    @C5566aOg
    @C2499fr
    /* renamed from: qV */
    public void mo656qV() {
        switch (bFf().mo6893i(f7122Lm)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f7122Lm, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f7122Lm, new Object[0]));
                break;
        }
        m30376qU();
    }

    /* renamed from: ra */
    public float mo1091ra() {
        switch (bFf().mo6893i(aTh)) {
            case 0:
                return 0.0f;
            case 2:
                return ((Float) bFf().mo5606d(new aCE(this, aTh, new Object[0]))).floatValue();
            case 3:
                bFf().mo5606d(new aCE(this, aTh, new Object[0]));
                break;
        }
        return m30214VJ();
    }

    /* renamed from: rb */
    public float mo1092rb() {
        switch (bFf().mo6893i(aTg)) {
            case 0:
                return 0.0f;
            case 2:
                return ((Float) bFf().mo5606d(new aCE(this, aTg, new Object[0]))).floatValue();
            case 3:
                bFf().mo5606d(new aCE(this, aTg, new Object[0]));
                break;
        }
        return m30213VI();
    }

    @C5566aOg
    public void repair() {
        switch (bFf().mo6893i(bqT)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, bqT, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, bqT, new Object[0]));
                break;
        }
        ahw();
    }

    public void step(float f) {
        switch (bFf().mo6893i(_f_step_0020_0028F_0029V)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, _f_step_0020_0028F_0029V, new Object[]{new Float(f)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, _f_step_0020_0028F_0029V, new Object[]{new Float(f)}));
                break;
        }
        m30264ay(f);
    }

    public String toString() {
        switch (bFf().mo6893i(_f_toString_0020_0028_0029Ljava_002flang_002fString_003b)) {
            case 0:
                return null;
            case 2:
                return (String) bFf().mo5606d(new aCE(this, _f_toString_0020_0028_0029Ljava_002flang_002fString_003b, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, _f_toString_0020_0028_0029Ljava_002flang_002fString_003b, new Object[0]));
                break;
        }
        return m30263au();
    }

    @ClientOnly
    /* renamed from: y */
    public void mo18331y(C3904wY wYVar) {
        switch (bFf().mo6893i(brz)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, brz, new Object[]{wYVar}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, brz, new Object[]{wYVar}));
                break;
        }
        m30393x(wYVar);
    }

    /* renamed from: zt */
    public Hull mo8287zt() {
        switch (bFf().mo6893i(f7135Wl)) {
            case 0:
                return null;
            case 2:
                return (Hull) bFf().mo5606d(new aCE(this, f7135Wl, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f7135Wl, new Object[0]));
                break;
        }
        return m30399zs();
    }

    /* renamed from: zv */
    public Shield mo8288zv() {
        switch (bFf().mo6893i(f7136Wm)) {
            case 0:
                return null;
            case 2:
                return (Shield) bFf().mo5606d(new aCE(this, f7136Wm, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f7136Wm, new Object[0]));
                break;
        }
        return m30400zu();
    }

    /* renamed from: zx */
    public void mo1099zx() {
        switch (bFf().mo6893i(f7137Wp)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f7137Wp, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f7137Wp, new Object[0]));
                break;
        }
        m30401zw();
    }

    /* access modifiers changed from: protected */
    /* renamed from: S */
    public void mo10S() {
        super.mo10S();
        this.radius = -1.0f;
        this.boG = false;
    }

    /* renamed from: h */
    public void mo18318h(ShipType ng) {
        super.mo967a((C2961mJ) ng);
        this.radius = -1.0f;
        this.boG = false;
        if (aeI() != null) {
            CargoHold wIVar = (CargoHold) bFf().mo6865M(CargoHold.class);
            wIVar.mo9150a(aeI());
            m30252a(wIVar);
            afk().mo22734m(this);
        }
        for (ShipStructureType i : afb()) {
            ShipStructure afk = (ShipStructure) bFf().mo6865M(ShipStructure.class);
            afk.mo13244i(i);
            afk.mo13245m(this);
            afl().add(afk);
        }
        ShipItem arq = (ShipItem) bFf().mo6865M(ShipItem.class);
        arq.mo11126a((ItemType) ng, this);
        m30242a(arq);
        Iterator<E> it = ng.mo4157Wl().iterator();
        while (true) {
            if (!it.hasNext()) {
                break;
            }
            C3315qE qEVar = (C3315qE) it.next();
            C6853aut bc = agH().mo21127bc(C5791aaX.eXw + qEVar.mo21301Xx());
            if (bc == null) {
                mo8358lY(" Consistency -> " + agH().mo19891ke() + ": no bone found for " + C5791aaX.eXw + qEVar.mo21301Xx());
                break;
            }
            C6544aow Xv = qEVar.mo21300Xv();
            if (Xv == null) {
                mo8358lY(" Consistency -> null TurretType for slot turret_" + qEVar.mo21301Xx());
                break;
            }
            Turret clU = Xv.clU();
            clU.mo2995f(bc);
            clU.mo2996h((Pawn) this);
            afr().add(clU);
        }
        ahr();
        if (bHa()) {
            mo8287zt().mo8348d(C2530gR.C2531a.class, this);
        }
        if (agH().bkd() != null) {
            m30274b(agH().bkd().bjY());
            afj().mo18825au(this);
        }
        m30200TH();
        if (mo8288zv() != null) {
            mo8288zv().mo19092m(this);
        }
        if (mo8287zt() != null) {
            m30396zi().mo19935m(this);
            DamageType ek = ala().mo1499ek("dmg_non_shield");
            if (ek == null) {
                mo8358lY("HULL NEEDS TO BE INVULNERABLE TO SHIELD DAMAGE! SHIELD DAMAGE TYPE NOT FOUND");
                return;
            }
            HullType ayp = mo8287zt().ayp();
            if (ayp != null) {
                C2686iZ<DamageType> anc = ayp.anc();
                if (anc != null) {
                    anc.add(ek);
                } else {
                    mo8358lY("InvulnerabilityList null: we are doomed!" + ayp.bFY());
                }
            } else {
                mo8358lY("Hull type null: we are doomed!" + bFY());
            }
        }
    }

    @C0064Am(aul = "6188c6747357ea4b691cb799af7b86e3", aum = 0)
    @ClientOnly
    /* renamed from: a */
    private void m30243a(Weapon adv, C3904wY wYVar) {
        m30337f(adv, wYVar);
    }

    @C0064Am(aul = "b9ac07c9fbe2d57b863b08578fee36c8", aum = 0)
    /* renamed from: e */
    private void m30331e(Weapon adv, C3904wY wYVar) {
        if (adv == null || agL().contains(adv)) {
            if (adv == null) {
                Iterator<Weapon> it = agL().iterator();
                while (true) {
                    if (!it.hasNext()) {
                        break;
                    }
                    Weapon next = it.next();
                    if (next.mo7860sM() == wYVar) {
                        adv = next;
                        break;
                    }
                }
            } else {
                if (adv.cUB()) {
                    mo8358lY("Setting active weapon a firing weapon!");
                    adv.bai();
                }
                if (bGZ()) {
                    adv.mo12913c((C6661arJ) this);
                }
            }
            C6308akU aku = null;
            if (agj() instanceof C6308akU) {
                aku = (C6308akU) agj();
            }
            if (wYVar == C3904wY.CANNON) {
                if (afs() != null && afs().cUB()) {
                    afs().bai();
                }
                m30336f(adv);
                if (!(aku == null || adv == null)) {
                    aku.mo14419f((C1506WA) new C2208cm(adv));
                }
            }
            if (wYVar == C3904wY.LAUNCHER) {
                if (aft() != null && aft().cUB()) {
                    aft().bai();
                }
                m30341g(adv);
                if (aku != null && adv != null) {
                    aku.mo14419f((C1506WA) new C2028bG(adv));
                }
            }
        }
    }

    @C0064Am(aul = "46b08a3ffa759a8ba3bbfa27a3f9d4d9", aum = 0)
    /* renamed from: g */
    private void m30342g(Weapon adv, C3904wY wYVar) {
        if (bGX()) {
            m30272b(adv, wYVar);
        }
        m30301d(adv, wYVar);
    }

    @C0064Am(aul = "8367e7183549ef9ef678b19dfedc493d", aum = 0)
    private void afD() {
        this.boB = !this.boB;
    }

    @C0064Am(aul = "ab38f02cea8ea026f18621dffd23ed2e", aum = 0)
    /* renamed from: S */
    private boolean m30198S(Actor cr) {
        if (cr == null) {
            return true;
        }
        if (!cr.cMb() && ((cr instanceof Station) || (cr instanceof Gate) || (cr instanceof MissionTrigger))) {
            return true;
        }
        if ((agj() instanceof C6308akU) && (cr instanceof Ship) && (((Ship) cr).agj() instanceof C6308akU)) {
            C6308akU aku = (C6308akU) ((Ship) cr).agj();
            Party dxi = ((C6308akU) agj()).dxi();
            if (dxi != null && dxi.mo2426aV(aku)) {
                return true;
            }
        }
        return mo1011bv(cr) <= agp();
    }

    @C0064Am(aul = "b81f2b0b73838e82b8bf1f30b4fc2f96", aum = 0)
    @C5472aKq
    /* renamed from: c */
    private boolean m30292c(C1722ZT<UserConnection> zt) {
        if (zt == null) {
            mo8358lY("Null replication context. Ignoring replication rule");
            return false;
        } else if (bGZ()) {
            if (zt.bFq().mo15388dL() == afm()) {
                return true;
            }
            return false;
        } else if (zt.bFq() == null || zt.bFq().mo15388dL() != afm()) {
            return false;
        } else {
            return true;
        }
    }

    @C0064Am(aul = "83b89b2893fc7ad3bbd5299cfbc08cf3", aum = 0)
    private void afF() {
        if (bGX()) {
            super.afG();
            if (this.hks != null && ahO() != null && !isDead()) {
                float length = ahO().mo2571qZ().length() / ahA().agH().mo4217ra();
                if (this.ajg != null && !Float.isInfinite(length) && !Float.isNaN(length)) {
                    this.ajg.mo5423nT(Math.min(length, 1.0f));
                }
                if (this.boA != null) {
                    this.boA.setPosition(this.hks.getPosition());
                    if (length > 1.0f) {
                        length = ((length - 1.0f) * 0.1f) + 1.0f;
                    }
                    this.boA.setControllerValue(Math.abs(length));
                }
            }
        }
    }

    @C0064Am(aul = "8109f47716990ad6cc559649f3da481f", aum = 0)
    /* renamed from: TO */
    private void m30204TO() {
        m30313dI(mo5353TJ().ase().mo5665VF());
        m30314dJ(mo5353TJ().ase().mo5666VH());
        m30315dK(mo5353TJ().ase().mo5669rb());
        m30316dL(mo5353TJ().ase().mo5668ra());
        m30317dM(mo5353TJ().ase().agp());
    }

    @C0064Am(aul = "192d108ce9ac199d56c8f044e253a95e", aum = 0)
    /* renamed from: a */
    private void m30228a(Actor.C0200h hVar) {
        if (m30381rh() == null) {
            throw new IllegalStateException("ShapedObject of class '" + getClass().getName() + "' does not have a RenderAsset");
        }
        String file = m30381rh().getFile();
        if (!bGX() || !(ald().getLoaderTrail() instanceof LoaderTrail) || equals(getPlayer().bQx())) {
            hVar.mo1103c(C2606hU.m32703b(file, false));
        } else {
            ((LoaderTrail) ald().getLoaderTrail()).mo19098a(file, new C2413c(hVar), getName(), false);
        }
    }

    @C0064Am(aul = "347ee5d227d27aa29c2c1f370355faed", aum = 0)
    /* renamed from: d */
    private C6901avp m30299d(Space ea) {
        return new C6901avp(ea, this, mo958IL());
    }

    @C0064Am(aul = "1f2e1038490feb3bdd4ddb831a0d27da", aum = 0)
    @C2198cg
    /* renamed from: c */
    private C0520HN m30282c(Space ea, long j) {
        C6308akU aku;
        if (j < afu()) {
            j = afu();
        }
        C6901avp e = mo18314e(ea);
        if (mo2998hb() instanceof StationaryAIController) {
            e.mo2449a(ea.cKn().mo5725a(j, (C2235dB) e, 15));
            e.setStatic(true);
        } else {
            try {
                e.mo2449a(ea.cKn().mo5725a(j, (C2235dB) e, 11));
            } catch (RuntimeException e2) {
                e2.printStackTrace();
                if (e == null) {
                    mo8358lY("Solid is null on create solid for " + getName() + " " + bFY());
                }
                if (ea == null) {
                    mo8358lY("Space is null on create solid for " + getName() + " " + bFY());
                }
                if (ea != null && ea.cKn() == null) {
                    mo8358lY("Simulator is null on " + ea.bFY() + " on create solid for " + getName() + " " + bFY());
                }
                throw new IllegalStateException("Invalid state of ship " + getName() + " " + bFY());
            }
        }
        if (bGY() && (agj() instanceof C6308akU) && (aku = (C6308akU) agj()) != null && aku.dxa()) {
            if (e.aSk() != null) {
                e.aSk().dispose();
                e.mo2544a((C3387qy<C0461GN>) null);
            }
            e.mo2544a(ea.cKn().mo13314a((C0461GN) e));
        }
        if (bGX() && ald().mo4089dL() == agj()) {
            C6308akU aku2 = (C6308akU) agj();
            aku2.dxc().mo22141fx(true);
            e.mo2544a(ea.cKn().mo13314a((C0461GN) e));
            aku2.mo14419f((C1506WA) new aRX(e));
        }
        return e;
    }

    @C0064Am(aul = "c38b1e10b2794240396f6cff959d1d6a", aum = 0)
    /* renamed from: a */
    private void m30237a(ShipSector or) {
        or.dispose();
    }

    @C0064Am(aul = "811b78f29daf44ae2534669f66e7bba9", aum = 0)
    /* renamed from: a */
    private void m30244a(ShipStructure afk) {
        Iterator it = afk.bVG().iterator();
        while (it.hasNext()) {
            it.remove();
            m30267b((ShipSector) it.next());
        }
        afk.mo13246zv().dispose();
        afk.dispose();
    }

    @C0064Am(aul = "c1bb90bce63a7c4d3e337fe3e896e576", aum = 0)
    private CargoHold afH() {
        return afk();
    }

    @C0064Am(aul = "4291fb73c5822f71b664106271041a7c", aum = 0)
    private Controller afJ() {
        if (afm() == null) {
            return null;
        }
        return afm().mo12657hb();
    }

    @C0064Am(aul = "a401a453214ae257f7c751dedc8a01d3", aum = 0)
    /* renamed from: YF */
    private float m30221YF() {
        return 0.0f;
    }

    @C0064Am(aul = "709f96c845755ad928fc5301838afaad", aum = 0)
    private CruiseSpeed afK() {
        return afj();
    }

    @C0064Am(aul = "f7bb10bed848c230f8726d4d943b2358", aum = 0)
    private C6853aut afM() {
        if (this.boB) {
            return this.boX;
        }
        return this.boZ;
    }

    @C0064Am(aul = "92bc47b94baf638e7ce1ea47693bf045", aum = 0)
    @ClientOnly
    private boolean afO() {
        return !this.boB;
    }

    @C0064Am(aul = "471482215affb6008987432e94a975ad", aum = 0)
    private Weapon afQ() {
        return mo18313e(C3904wY.CANNON);
    }

    @C0064Am(aul = "61910bbe36f198c9e0998dc172a4ec87", aum = 0)
    private Weapon afS() {
        return mo18313e(C3904wY.LAUNCHER);
    }

    @C0064Am(aul = "929fcd9af0dc7f624226cd94317938b4", aum = 0)
    private Vec3f afU() {
        float max = Math.max(0.0f, (-mo1090qZ().z) / 10.0f) + 10.0f;
        if (mo958IL() != null) {
            max -= mo958IL().getBoundingBox().dqP().z;
        }
        return new Vec3f(0.0f, 0.0f, -max);
    }

    @C0064Am(aul = "13461c231e510a786ef4e77986cf886f", aum = 0)
    /* renamed from: js */
    private float m30360js() {
        return 0.0f;
    }

    @C0064Am(aul = "a8fb832a14995407252e62d086fa3760", aum = 0)
    @ClientOnly
    private SPitchedSet afW() {
        return this.boA;
    }

    @C0064Am(aul = "d783f5a614eb7f526844d8698e9d6808", aum = 0)
    /* renamed from: zs */
    private Hull m30399zs() {
        return m30396zi();
    }

    @C0064Am(aul = "ee7effea2745205d535048623ec7f04d", aum = 0)
    /* renamed from: VE */
    private float m30211VE() {
        if (agZ()) {
            return afj().mo18815VF();
        }
        if (!isDisposed()) {
            return afw();
        }
        return 0.0f;
    }

    @C0064Am(aul = "f7d9ac177fdd45962ee11383bc4e9318", aum = 0)
    /* renamed from: VG */
    private float m30212VG() {
        if (agZ()) {
            return afj().mo18816VH();
        }
        if (!isDisposed()) {
            return afx();
        }
        return 0.0f;
    }

    @C0064Am(aul = "7ff404dec002c10c5ce0e0b7c898a1e0", aum = 0)
    private float afY() {
        if (isDisposed()) {
            return 0.0f;
        }
        float bkn = agH().bkn();
        if (bkn == 0.0f) {
            return ala().aJg().ard();
        }
        return bkn;
    }

    @C0064Am(aul = "e107c60c246e5e87e76cbe9114769b5a", aum = 0)
    private float aga() {
        if (isDisposed()) {
            return 0.0f;
        }
        float bkp = agH().bkp();
        if (bkp == 0.0f) {
            return ala().aJg().arf();
        }
        return bkp;
    }

    @C0064Am(aul = "d97ac40a6c21bd6433ba304d7c870a73", aum = 0)
    private float agc() {
        if (isDisposed()) {
            return 0.0f;
        }
        float bkr = agH().bkr();
        if (bkr > 0.0f) {
            return bkr;
        }
        return ala().aJg().arh();
    }

    @C0064Am(aul = "b756d2d88d5acdd054c450b6d3c76bbd", aum = 0)
    private float age() {
        if (isDisposed()) {
            return 0.0f;
        }
        float bkt = agH().bkt();
        if (bkt != 0.0f) {
            return bkt;
        }
        return ala().aJg().arj();
    }

    @C0064Am(aul = "6f9573c0e92bef054db1d90546754f33", aum = 0)
    /* renamed from: VI */
    private float m30213VI() {
        if (isDisposed()) {
            return 0.0f;
        }
        if (agZ()) {
            return afj().mo18836rb();
        }
        return afy();
    }

    @C0064Am(aul = "05c2de41e21476f460c07580c2c4c01c", aum = 0)
    /* renamed from: VJ */
    private float m30214VJ() {
        if (isDisposed()) {
            return 0.0f;
        }
        if (agZ()) {
            return afj().mo18835ra();
        }
        return afz();
    }

    @C0064Am(aul = "3b19e838b5a53d7715b611d220ee058e", aum = 0)
    private C6853aut agg() {
        return this.boY;
    }

    @C0064Am(aul = "d23c45038a915bd772fe4e48950bdf8a", aum = 0)
    private Character agi() {
        return afm();
    }

    @C0064Am(aul = "5d0993e086fd1481b49a22e91f5f3fc8", aum = 0)
    private C3438ra<Component> agm() {
        return afn();
    }

    @C0064Am(aul = "0f2280ced41e37e936940aef477fa128", aum = 0)
    private float ago() {
        return afA() + 100000.0f;
    }

    @C0064Am(aul = "714af79623d733cd15883830bb6ad929", aum = 0)
    /* renamed from: io */
    private String m30352io() {
        return m30381rh().getHandle();
    }

    @C0064Am(aul = "b0cad9e851282f9199a43bb4e733947d", aum = 0)
    /* renamed from: iq */
    private String m30353iq() {
        return m30381rh().getFile();
    }

    @C0064Am(aul = "969f26f8005f0e2a035158760f063795", aum = 0)
    /* renamed from: iz */
    private String m30355iz() {
        if (agj() instanceof C6308akU) {
            return ala().aIW().avA();
        }
        if (!(agj() instanceof NPC)) {
            return null;
        }
        if (isStatic()) {
            return ala().aIW().avm();
        }
        if (((NPC) agj()).dzG()) {
            return ala().aIW().avy();
        }
        return ala().aIW().avw();
    }

    @C0064Am(aul = "40d7ee933bc57b7648c45d9222e2f4d4", aum = 0)
    /* renamed from: zu */
    private Shield m30400zu() {
        if (agt().size() == 0) {
            return null;
        }
        return ((ShipStructure) agt().get(0)).mo13246zv();
    }

    @C0064Am(aul = "2a2d688065c50a493c0f3d57f9dac277", aum = 0)
    /* renamed from: B */
    private Shield m30183B(Vec3f vec3f) {
        return ((ShipStructure) agt().get(0)).mo13246zv();
    }

    @C0064Am(aul = "af67e17f054b596251b42f538f2afb64", aum = 0)
    private ShipItem agq() {
        return afo();
    }

    @C0064Am(aul = "11c6bb7122e86401f3008257740d7de8", aum = 0)
    private C3438ra<ShipStructure> ags() {
        return afl();
    }

    @C0064Am(aul = "cc91ef92aee4fd4d8255cf608d70ff29", aum = 0)
    private Pawn agu() {
        return this;
    }

    @C0064Am(aul = "a06c15239329ee538e3052df836aec81", aum = 0)
    private Vec3f agw() {
        if (afN() != null) {
            return afN().mo16402gL();
        }
        return afV();
    }

    @C0064Am(aul = "b62640136c117bb30ffa4945a9afb240", aum = 0)
    private C6809auB.C1996a acY() {
        return acy();
    }

    @C0064Am(aul = "4d6cd5a2445d0eb2a93ce931f99f0d39", aum = 0)
    private float agy() {
        float cVr = ((float) (cVr() - afp())) / 1000.0f;
        if (acy() == C6809auB.C1996a.ACTIVE) {
            return Math.max((mo16301jt() + mo16294YE()) - cVr, 0.0f);
        }
        if (acy() == C6809auB.C1996a.COOLDOWN) {
            return Math.max(((mo16301jt() + mo16294YE()) + mo16295YG()) - cVr, 0.0f);
        }
        if (acy() == C6809auB.C1996a.WARMUP) {
            return Math.max(mo16294YE() - cVr, 0.0f);
        }
        return 0.0f;
    }

    @C0064Am(aul = "2b7f9efeeb2f9e88c18050b9339e1acb", aum = 0)
    /* renamed from: it */
    private String m30354it() {
        return m30351ij();
    }

    @C0064Am(aul = "89cbaf46b58f1be9e0d94122fbec7313", aum = 0)
    private Actor agA() {
        if (!bGX() || !getPlayer().equals(afm())) {
            return afq();
        }
        return this.boQ;
    }

    @C0064Am(aul = "fc436d2830185ec4079feb381a1b9083", aum = 0)
    private float agC() {
        return (float) (((double) 2.5f) * 0.017453292519943295d);
    }

    @C0064Am(aul = "896175fa229632d8d92b3a58b2568745", aum = 0)
    private C3438ra<Turret> agE() {
        return afr();
    }

    @C0064Am(aul = "fb71d87bd17fc6cc77df7230b0c38028", aum = 0)
    private ShipType agG() {
        return (ShipType) super.getType();
    }

    @C0064Am(aul = "da200beda84e8e888d295dcedbddb9d7", aum = 0)
    private String agI() {
        return Long.toString(cWm());
    }

    @C0064Am(aul = "ceb7fee37ddc7d49a45e91b1c6824cd5", aum = 0)
    /* renamed from: YD */
    private float m30220YD() {
        return 0.0f;
    }

    @C0064Am(aul = "1354f41b5ae79bc3b64d8435d7cb68fc", aum = 0)
    /* renamed from: d */
    private Weapon m30298d(C3904wY wYVar) {
        if (wYVar == C3904wY.CANNON) {
            return afs();
        }
        if (wYVar == C3904wY.LAUNCHER) {
            return aft();
        }
        return null;
    }

    @C0064Am(aul = "0d0d49c2e76bfa3d6fc2bec0e63e6d96", aum = 0)
    /* renamed from: f */
    private List<Component> m30335f(C3904wY wYVar) {
        ArrayList arrayList = new ArrayList();
        for (ShipStructure bVG : agt()) {
            for (ShipSector iterator : bVG.bVG()) {
                Iterator<Item> iterator2 = iterator.getIterator();
                while (iterator2.hasNext()) {
                    Item next = iterator2.next();
                    if ((next instanceof Component) && ((Component) next).mo7860sM() == wYVar) {
                        arrayList.add((Component) next);
                    }
                }
            }
        }
        return arrayList;
    }

    @C0064Am(aul = "ed74e1594bf493d7972378246eb30608", aum = 0)
    private List<Weapon> agK() {
        ArrayList arrayList = new ArrayList();
        for (ShipStructure bVG : agt()) {
            for (ShipSector iterator : bVG.bVG()) {
                Iterator<Item> iterator2 = iterator.getIterator();
                while (iterator2.hasNext()) {
                    Item next = iterator2.next();
                    if (next instanceof Weapon) {
                        arrayList.add((Weapon) next);
                    }
                }
            }
        }
        return arrayList;
    }

    @C0064Am(aul = "7b26bc74ab22ccfad7f0d8ac70673d1d", aum = 0)
    /* renamed from: h */
    private boolean m30347h(C3904wY wYVar) {
        if (mo18313e(wYVar) == null || mo18313e(wYVar).aqh() <= 0) {
            return false;
        }
        if (bGX()) {
            if (wYVar == C3904wY.CANNON) {
                ald().getEventManager().mo13975h(new C6018aeq("shot.primary", mo18313e(wYVar).aqz().getHandle()));
            }
            if (wYVar == C3904wY.LAUNCHER) {
                ald().getEventManager().mo13975h(new C6018aeq("shot.secondary", mo18313e(wYVar).aqz().getHandle()));
            }
        }
        return true;
    }

    @C0064Am(aul = "9a372301d7626e6cf21ce9d2744a410d", aum = 0)
    private void agM() {
        if (!C5877acF.RUNNING_CLIENT_BOT || agH() != null) {
            if (this.boX == null) {
                this.boX = agH().mo21127bc(C5791aaX.eXt);
            }
            if (this.boZ == null) {
                this.boZ = agH().mo21127bc(C5791aaX.eXu);
            }
            if (this.boY == null) {
                this.boY = agH().mo21127bc(C5791aaX.eXv);
            }
        }
    }

    @C0064Am(aul = "9738ed635515eaabd6b88486b1132133", aum = 0)
    private float agO() {
        return aeO();
    }

    @C0064Am(aul = "839b981b79653d74d62ce8f6c1665bcf", aum = 0)
    private float agQ() {
        return aeP();
    }

    @C0064Am(aul = "f63875537e50642668c25fcaaced83e7", aum = 0)
    private float agS() {
        return aeS();
    }

    @C0064Am(aul = "7d74f60cb5ffd4bf24f9bc5700698a8f", aum = 0)
    private float agU() {
        return aeT();
    }

    @C0064Am(aul = "90e1e1684603f138db2655c9186b8d63", aum = 0)
    private float agW() {
        return agH().agp();
    }

    @C0064Am(aul = "bd3d5187110bb88d7fbb12a5fe9871fa", aum = 0)
    /* renamed from: eK */
    private boolean m30333eK(int i) {
        return true;
    }

    @C0064Am(aul = "42b181bb65a527169083a909646f34c9", aum = 0)
    private boolean agY() {
        return afj() != null && afj().acZ() == C6809auB.C1996a.ACTIVE;
    }

    @C0064Am(aul = "81cab29d4f08710bbb4b300201a60017", aum = 0)
    private boolean aha() {
        if (afj() == null) {
            return false;
        }
        if (afj().acZ() == C6809auB.C1996a.ACTIVE || afj().acZ() == C6809auB.C1996a.WARMUP) {
            return true;
        }
        return false;
    }

    @C0064Am(aul = "765eb35e001ae453344cf84c59266ff9", aum = 0)
    private boolean ahc() {
        return afj() != null && afj().acZ() == C6809auB.C1996a.COOLDOWN;
    }

    @C0064Am(aul = "db1de72988be33e13d3d4c0e23071c0e", aum = 0)
    private boolean ahe() {
        return afj() != null && afj().acZ() == C6809auB.C1996a.DEACTIVE;
    }

    @C0064Am(aul = "8515dceed6fec1b1db59419eb38399f6", aum = 0)
    private boolean ahg() {
        return afj() != null && afj().acZ() == C6809auB.C1996a.WARMUP;
    }

    @C0064Am(aul = "6d799dc572ff4efa279d24e2c0821059", aum = 0)
    private boolean ahi() {
        return afI().isEmpty() && !ahk();
    }

    @C0064Am(aul = "acb917dd9c1688e5e2ac2a94bb0a3c42", aum = 0)
    private boolean ahj() {
        for (ShipStructure bVG : afl()) {
            Iterator<E> it = bVG.bVG().iterator();
            while (true) {
                if (it.hasNext()) {
                    if (((ShipSector) it.next()).getIterator().hasNext()) {
                        return true;
                    }
                }
            }
        }
        return false;
    }

    @C0064Am(aul = "e2d3dfa707b568403d092ca46c5fccb1", aum = 0)
    /* renamed from: j */
    private boolean m30359j(C3904wY wYVar) {
        return mo18313e(wYVar) != null && mo18313e(wYVar).cUB();
    }

    @C0064Am(aul = "42afde475128130bdd22a21f5df24067", aum = 0)
    private void ahl() {
        if (afj() == null && agH().bkd() != null) {
            m30274b(agH().bkd().bjY());
        }
    }

    @C0064Am(aul = "28133fdae892b8b2336f63a0900d4c7f", aum = 0)
    /* renamed from: a */
    private void m30231a(Actor cr, Vec3f vec3f, Vec3f vec3f2, float f, C6339akz akz) {
        super.mo992b(cr, vec3f, vec3f2, f, akz);
        if (!(cr instanceof Shot) && agZ()) {
            mo7599de(cVr());
        }
        if (cr == null) {
            System.err.println("OnCollide: other is null!!");
            return;
        }
        if (ala().aLS().adu()) {
            HashMap hashMap = new HashMap();
            Iterator<DamageType> it = ala().aLs().iterator();
            hashMap.put(it.next(), Float.valueOf(50.0f));
            hashMap.put(it.next(), Float.valueOf(50.0f));
            mo1064f(cr, new C5260aCm((Map<DamageType, Float>) hashMap, "particle", (Actor) this), vec3f2, vec3f);
        }
        if (!bGX()) {
            return;
        }
        if (!(cr instanceof Ship) || ((Ship) cr).mo8317Ej() <= mo8317Ej()) {
            ald().getEventManager().mo13975h(new C1042PH(this, cr, vec3f, vec3f2, f, akz));
        }
    }

    @C0064Am(aul = "da357158998b6d201ae957543e334333", aum = 0)
    @ClientOnly
    private C2279dZ ahn() {
        if (this.bpe == null) {
            this.bpe = new C2279dZ();
        }
        return this.bpe;
    }

    @C0064Am(aul = "49bdca2786c8edf8819274a0171854bb", aum = 0)
    /* renamed from: a */
    private void m30239a(C5260aCm acm, Pawn avi) {
        String str;
        LDScriptingController bQG = agj().bQG();
        super.mo16606b(acm, avi);
        bQG.mo20288q(avi);
        if (agj() instanceof C6308akU) {
            C6308akU aku = (C6308akU) agj();
            if (avi instanceof Ship) {
                aku.mo14345aA((Ship) avi);
            }
            String str2 = "";
            if (!(avi instanceof Ship) || !(((Ship) avi).agj() instanceof C6308akU)) {
                str = "";
            } else {
                String username = ((C6308akU) ((Ship) avi).agj()).bOx().getUsername();
                str = ((C6308akU) ((Ship) avi).agj()).getName();
                str2 = username;
            }
            mo2066a((C5878acG) new C2059bi(aku, str2, str));
        }
    }

    @C0064Am(aul = "8ef4f7dca16306dbc958c1a6889d1768", aum = 0)
    /* renamed from: a */
    private void m30247a(Pawn avi, Actor cr) {
        if (avi == agB()) {
            mo18260Z((Actor) null);
        }
    }

    @C4034yP
    @C0064Am(aul = "875b28b87fff3ab69db17c478a3f57ca", aum = 0)
    /* renamed from: c */
    private void m30286c(Vec3d ajr, Node rPVar, StellarSystem jj) {
        m30270b(ajr, rPVar, jj, (Set<Loot>) null);
    }

    @C4034yP
    @ClientOnly
    @C0064Am(aul = "670fabbecd7023a644e569ab66c686a5", aum = 0)
    @C2499fr
    /* renamed from: a */
    private void m30241a(Vec3d ajr, Node rPVar, StellarSystem jj, Set<Loot> set) {
        if (mo1000b(rPVar, jj) && agH() != null) {
            new C3257pi().mo21209a(m30397zl(), ajr, C3257pi.C3261d.FAR_AWAY);
            new C3257pi().mo21210a(m30398zm(), ajr, C3257pi.C3261d.MID_RANGE, this.radius);
        }
        if (set != null) {
            for (Loot b : set) {
                b.mo993b(mo960Nc());
            }
        }
    }

    @C0064Am(aul = "92f49336f6d25254c7988b65363d0ae2", aum = 0)
    /* renamed from: b */
    private void m30275b(Hull jRVar, C5260aCm acm, Actor cr) {
        Pawn avi;
        if (jRVar == m30396zi()) {
            if (afR() != null) {
                afR().bai();
            }
            if (afT() != null) {
                afT().bai();
            }
            if (mo2998hb() != null && (mo2998hb() instanceof PlayerController)) {
                ((PlayerController) mo2998hb()).mo22076bT(cr);
            }
            mo16613ge(true);
            if (cr instanceof Pawn) {
                mo16606b(acm, (Pawn) cr);
            }
            if (cr instanceof Pawn) {
                avi = (Pawn) cr;
                Controller hb = avi.mo2998hb();
                if (hb instanceof PlayerController) {
                    m30302d((PlayerController) hb);
                    m30270b(getPosition(), azW(), mo960Nc(), mo18301b((Actor) avi, getPosition(), true));
                    mo16608bc(cr);
                    mo1099zx();
                }
            }
            avi = null;
            m30270b(getPosition(), azW(), mo960Nc(), mo18301b((Actor) avi, getPosition(), true));
            mo16608bc(cr);
            mo1099zx();
        }
    }

    @C0064Am(aul = "8211af2b6d0265eaae7ee115352e17c0", aum = 0)
    /* renamed from: c */
    private void m30285c(C5260aCm acm, Pawn avi) {
        float f;
        Character agj = agj();
        agj.bQG().mo20292s(avi);
        if (mo2998hb() != null) {
            mo2998hb().mo3289av(avi);
        }
        ala().aJu().mo7844b(agj, avi, acm.cPJ());
        if (avi instanceof Ship) {
            Ship fAVar = (Ship) avi;
            Character agj2 = fAVar.agj();
            if (agj2 instanceof NPC) {
                NPC auf = (NPC) agj2;
                if (auf.bTC() != null) {
                    f = auf.bTC().mo19055vh();
                } else {
                    f = 0.0f;
                }
                agj().mo12659ly().mo20722Rp().mo18214b(fAVar, f);
            }
            if (agj() instanceof C6308akU) {
                C6308akU aku = (C6308akU) agj();
                aku.mo14350ay((Ship) avi);
                if (agj2 instanceof NPC) {
                    long bTY = ((NPC) agj2).mo11654Fs().bTY();
                    if (bTY > 0) {
                        ala().aIS().mo12602b((C6308akU) agj, bTY, Bank.C1861a.CREEP_KILL_REWARD, agj2.getName(), 0);
                    } else if (bTY < 0) {
                        ala().aIS().mo12608d((C6308akU) agj, -bTY, Bank.C1861a.CREEP_KILL_FINE, agj2.getName(), 0);
                    }
                }
                ((C6308akU) agj()).mo14348am("destroy.ship", agH() != null ? agH().getHandle() : "");
                String str = "";
                String str2 = "";
                if (agj2 instanceof C6308akU) {
                    str = ((C6308akU) fAVar.agj()).bOx().getUsername();
                    str2 = ((C6308akU) fAVar.agj()).getName();
                }
                mo2066a((C5878acG) new C1363Tr(aku, str, str2));
            }
        }
    }

    @C0064Am(aul = "5df9dd3c795856e579470eadc56e72cd", aum = 0)
    /* renamed from: a */
    private void m30234a(C0665JT jt) {
    }

    @C0064Am(aul = "33dbff76f10f6a89ddda928890ac3152", aum = 0)
    @ClientOnly
    private void ahp() {
        super.mo957Fe();
        ahy();
        this.radius = (float) this.hks.getAabbLSLenght();
    }

    @C0064Am(aul = "8a6cb07ccb1fd2e583e35b12a19ec2e5", aum = 0)
    @ClientOnly
    /* renamed from: iB */
    private void m30350iB() {
    }

    @C0064Am(aul = "a1a2139bd11090adc0522ee23f4a74e7", aum = 0)
    private void ahq() {
        for (Turret add : afr()) {
            cMG().add(add);
        }
    }

    @C0064Am(aul = "536919a01efd0c5712a3c0e6929e688b", aum = 0)
    private void ahs() {
        CorporationLogo bFK;
        super.aht();
        if (ahA() == C5916acs.getSingolton().mo4089dL().bQx()) {
            C5916acs.getSingolton().alb().mo22154m((Pawn) ahA());
        }
        if (this.hks != null) {
            Character agj = ahA().agj();
            if ((agj instanceof C6308akU) && (bFK = ((C6308akU) agj).dxy().bFK()) != null) {
                C2403eu.m30125a(bFK.getHandle(), ahA().agH().mo4151Nu().getHandle(), this.hks);
                return;
            }
            return;
        }
        logger.error("Missing asset for our ship " + toString());
    }

    @C0064Am(aul = "36dc437d1c4a3798a35cedaa199f0088", aum = 0)
    /* renamed from: l */
    private void m30362l(Actor cr) {
        if (cr == agB()) {
            mo18260Z((Actor) null);
        }
    }

    @C0064Am(aul = "9ef08f55c36d2fa219a02bf4a92a8e39", aum = 0)
    private void ahu() {
        if (agj() != null && (agj() instanceof C6308akU)) {
            ((C6308akU) agj()).mo14338A(agJ(), new C3504rh(acy()));
        }
    }

    @C0064Am(aul = "a174e96a7ef5faa46590e861c5e0c344", aum = 0)
    /* renamed from: a */
    private boolean m30256a(Controller jx, Actor cr) {
        if ((mo2998hb() instanceof PlayerController) && (jx instanceof PlayerController)) {
            if (!azW().mo21619Zf()) {
                ((PlayerController) jx).mo22135dL().mo14419f((C1506WA) new C0284Df(((NLSWindowAlert) ala().aIY().mo6310c(NLSManager.C1472a.WINDOWALERT)).dbw(), false, false, true, new Object[0]));
                return true;
            }
            C6308akU aku = (C6308akU) agj();
            C6308akU aku2 = (C6308akU) ((Ship) cr).agj();
            if (aku.dxk() && aku.dxi().equals(aku2.dxi())) {
                ((PlayerController) jx).mo22135dL().mo14419f((C1506WA) new C0284Df(((NLSWindowAlert) ala().aIY().mo6310c(NLSManager.C1472a.WINDOWALERT)).dbk(), false, false, true, new Object[0]));
            }
        }
        return false;
    }

    @C0064Am(aul = "330064503b43a8806b416e145c223d2e", aum = 0)
    @ClientOnly
    /* renamed from: n */
    private void m30369n(C3904wY wYVar) {
        Weapon e = mo18313e(wYVar);
        if (e != null) {
            e.mo12933cc(agB());
            e.cDL();
        }
    }

    @C0064Am(aul = "4e1d3227d6e63d494c3f0b5521bc6a18", aum = 0)
    @ClientOnly
    /* renamed from: t */
    private void m30386t(C3904wY wYVar) {
        Weapon e = mo18313e(wYVar);
        if (e != null) {
            e.bai();
        }
    }

    @C0064Am(aul = "15676101e12d6b0220d6bfc616679471", aum = 0)
    /* renamed from: n */
    private void m30368n(Weapon adv) {
        m30345h(adv, C3904wY.CANNON);
    }

    @C0064Am(aul = "81c8132e6b364ac2eb725f009d4b8a0c", aum = 0)
    /* renamed from: p */
    private void m30372p(Weapon adv) {
        m30345h(adv, C3904wY.LAUNCHER);
    }

    @C0064Am(aul = "d00e363bf9634e100f5cf46f87314d3c", aum = 0)
    @ClientOnly
    /* renamed from: a */
    private void m30255a(SPitchedSet sPitchedSet) {
        this.boA = sPitchedSet;
        this.boA.setGain(C5916acs.getSingolton().getEngineGraphics().aeb());
    }

    @C0064Am(aul = "c4cbf98aad48fa51da90822b75324e12", aum = 0)
    /* renamed from: j */
    private void m30357j(Character acx) {
        m30349i(acx);
    }

    @C0064Am(aul = "3c0a00c10abe0dfe870ddf61e24c4113", aum = 0)
    @ClientOnly
    /* renamed from: bs */
    private void m30280bs(boolean z) {
        this.boG = z;
    }

    @C4034yP
    @ClientOnly
    @C0064Am(aul = "acea22c22dd3eb16c0f523f288b37ad5", aum = 0)
    @C2499fr
    /* renamed from: Y */
    private void m30219Y(Actor cr) {
        mo2967X(cr);
        m30262af(cr);
    }

    @C0064Am(aul = "1ef70b074203a6d2ddd91d9079e4c388", aum = 0)
    /* renamed from: ac */
    private void m30260ac(Actor cr) {
        m30258ab(cr);
        m30195R(cr);
    }

    @C0064Am(aul = "05e58d90f297509ed6464a706c7fecb9", aum = 0)
    /* renamed from: ae */
    private void m30261ae(Actor cr) {
        boolean z = cr != null && cr.azW() == azW();
        if (bGX() && getPlayer().equals(afm())) {
            if (this.boQ != null) {
                this.boQ.mo8353f(C0471GX.C0472a.class, this);
                this.boQ.mo8353f(C3396rD.C3397a.class, this);
            }
            if (z) {
                cr.mo8327b(C0471GX.C0472a.class, this);
                cr.mo8327b(C3396rD.C3397a.class, this);
                this.boQ = cr;
            } else {
                this.boQ = null;
            }
            if (agj() instanceof C6308akU) {
                ((C6308akU) agj()).mo14419f((C1506WA) new C3551sV(cr));
            }
        }
        if (z) {
            m30195R(cr);
        } else {
            m30195R((Actor) null);
        }
        if (mo18313e(C3904wY.CANNON) != null) {
            mo18313e(C3904wY.CANNON).mo12933cc(afq());
        }
        if (mo18313e(C3904wY.LAUNCHER) != null) {
            mo18313e(C3904wY.LAUNCHER).mo12933cc(afq());
            if (mo18313e(C3904wY.LAUNCHER) instanceof MissileWeapon) {
                ((MissileWeapon) mo18313e(C3904wY.LAUNCHER)).mo830bR(false);
            }
        }
    }

    @C0064Am(aul = "855d8f4fbdb9ab07d8a579f8d43e655e", aum = 0)
    @ClientOnly
    private void ahx() {
        this.ajg = new C1255SZ();
        this.ajg.mo5422kc(this.boG);
        this.ajg.mo5419a(ahA(), this.hks);
    }

    @C0064Am(aul = "64cdd80fef1ce2f72256968ecfad9f6f", aum = 0)
    private Ship ahz() {
        return this;
    }

    @C0064Am(aul = "675a4db910d0b97d9aa4825f437de2ca", aum = 0)
    private void ahB() {
        super.ahC();
        if (bHa()) {
            for (Turret jq : afr()) {
                if (jq.isAlive()) {
                    jq.mo993b(mo960Nc());
                }
            }
        }
    }

    @C0064Am(aul = "59932d65cfbe8ee6f4a1ec38de74ffe9", aum = 0)
    @C1253SX
    /* renamed from: e */
    private void m30329e(Space ea, long j) {
        super.mo1065f(ea, j);
        if (bGX()) {
            for (ShipStructure bVG : agt()) {
                for (ShipSector iterator : bVG.bVG()) {
                    Iterator<Item> iterator2 = iterator.getIterator();
                    while (iterator2.hasNext()) {
                        Item next = iterator2.next();
                        if (next instanceof Weapon) {
                            ((Weapon) next).cTX();
                        }
                    }
                }
            }
        }
        agN();
    }

    @C0064Am(aul = "5a9977285474fec34b013c32cc396fff", aum = 0)
    /* renamed from: bZ */
    private void m30279bZ(String str) {
        super.mo1055ca(str);
        ILoaderTrail alg = ald().getLoaderTrail();
        for (ShipStructure bVG : agt()) {
            for (ShipSector iterator : bVG.bVG()) {
                m30291c(iterator.getIterator());
            }
        }
        if (agH().bkh() != null) {
            alg.mo4995a(agH().bkh().getFile(), (String) null, (Scene) null, (C0907NJ) null, "Preloading ship FX");
        }
        if (agH().bkj() != null) {
            alg.mo4995a(agH().bkj().getFile(), (String) null, (Scene) null, (C0907NJ) null, "Preloading ship FX");
        }
    }

    @C0064Am(aul = "f8b48fe8237fc3f985f7afc60c6e17da", aum = 0)
    /* renamed from: b */
    private void m30277b(Iterator<Item> it) {
        ILoaderTrail alg = ald().getLoaderTrail();
        while (it.hasNext()) {
            Item next = it.next();
            if (next instanceof Weapon) {
                Weapon adv = (Weapon) next;
                WeaponType aqz = adv.aqz();
                if (aqz.doR() != null) {
                    alg.mo4995a(aqz.doR().getFile(), (String) null, (Scene) null, (C0907NJ) null, "Preloading weapons FX");
                }
                if (aqz.doX() != null) {
                    alg.mo4995a(aqz.doX().getFile(), (String) null, (Scene) null, (C0907NJ) null, "Preloading weapons FX");
                }
                if (aqz.doZ() != null) {
                    alg.mo4995a(aqz.doZ().getFile(), (String) null, (Scene) null, (C0907NJ) null, "Preloading weapons FX");
                }
                if (aqz.doN() != null) {
                    alg.mo4995a(aqz.doN().getFile(), (String) null, (Scene) null, (C0907NJ) null, "Preloading weapons FX");
                }
                if (adv instanceof EnergyWeapon) {
                    alg.mo4995a(((EnergyWeapon) adv).aqx(), (String) null, (Scene) null, (C0907NJ) null, "Preloading weapons FX");
                    super.mo1055ca(adv.aqr());
                } else if (adv instanceof ProjectileWeapon) {
                    ProjectileWeapon sg = (ProjectileWeapon) adv;
                    if (sg.bup() != null) {
                        alg.mo4995a(sg.bup().bAJ().mo3521Nu().getFile(), (String) null, (Scene) null, (C0907NJ) null, "Preloading weapons FX");
                        super.mo1055ca(adv.aqr());
                    }
                }
            }
        }
    }

    @C4034yP
    @ClientOnly
    @C0064Am(aul = "553d50399bce64694d9d7506a6332f64", aum = 0)
    @C2499fr
    /* renamed from: Z */
    private void m30222Z(Asset tCVar) {
        if (!C1298TD.m9830t(this).bFT()) {
            mo18310d(tCVar, new Vec3d(ScriptRuntime.NaN, ScriptRuntime.NaN, ScriptRuntime.NaN));
        }
    }

    @C4034yP
    @ClientOnly
    @C0064Am(aul = "54a6e0f54c5c0349210c1ce387a02781", aum = 0)
    @C2499fr
    /* renamed from: c */
    private void m30289c(Asset tCVar, Vec3d ajr) {
        if (bGX()) {
            Ship bQx = getPlayer().bQx();
            if (tCVar != null && ajr != null && bQx != null && bQx.mo1000b(azW(), mo960Nc())) {
                new C3257pi().mo21213a(tCVar, ajr, cHg(), C3257pi.C3261d.NEAR_ENOUGH);
            }
        }
    }

    @C0064Am(aul = "2420de6252fefdedc9076a4015478058", aum = 0)
    @ClientOnly
    /* renamed from: x */
    private void m30393x(C3904wY wYVar) {
        if (mo18313e(wYVar) != null && mo18322i(wYVar) && !mo18325k(wYVar)) {
            mo18313e(wYVar).mo12933cc(agB());
            if (bGY()) {
                m30375q(wYVar);
                return;
            }
            m30370o(wYVar);
            m30367m(wYVar);
        }
    }

    @C0064Am(aul = "162cb02e26d1833f86295b3289a6ca51", aum = 0)
    @ClientOnly
    /* renamed from: z */
    private void m30395z(C3904wY wYVar) {
        if (mo18313e(wYVar) == null) {
            return;
        }
        if (bGY()) {
            m30391w(wYVar);
            return;
        }
        m30388u(wYVar);
        m30384s(wYVar);
    }

    @C0064Am(aul = "ac6f920ea1c049d8111e6f2334f0cbb5", aum = 0)
    /* renamed from: dd */
    private void m30320dd(long j) {
        m30319dc(j);
        mo964W(0.0f);
    }

    @C0064Am(aul = "c7ab68b8b1a99d2bcf560b5344b4df88", aum = 0)
    private boolean ahL() {
        return afj() == null || !(afj().acZ() == C6809auB.C1996a.ACTIVE || afj().acZ() == C6809auB.C1996a.WARMUP);
    }

    @C4034yP
    @C0064Am(aul = "5a814fd9c940212e3c0074e94d053386", aum = 0)
    @C2499fr
    @C1253SX
    /* renamed from: bu */
    private void m30281bu(boolean z) {
        if (!z || ahM()) {
            ahO().mo16635ec(z);
            if (ahO() != null && ahO().aQX() != null && (ahO().aQX() instanceof C5917act)) {
                ((C5917act) ahO().aQX()).mo12722ec(z);
            }
        }
    }

    @C0064Am(aul = "2dd969d9d72ddd1bacec1576a7bbac77", aum = 0)
    private C6901avp ahN() {
        return (C6901avp) super.aiD();
    }

    @C0064Am(aul = "57529e3beb3c57d464f0347f4ae2ac02", aum = 0)
    /* renamed from: au */
    private String m30263au() {
        return "Ship: [" + getName() + "]";
    }

    @C0064Am(aul = "9a7f88396bbc53950ba66c3ae612eb26", aum = 0)
    /* renamed from: uV */
    private String m30389uV() {
        if (afm() != null) {
            return afm().getName();
        }
        return agH().mo19891ke().get();
    }

    @C0064Am(aul = "01a68764554f3f30b2819df231094e70", aum = 0)
    private String ahP() {
        if (afm() != null) {
            return afm().ahQ();
        }
        return agH().mo19891ke().get();
    }

    @C0064Am(aul = "9b114245599d7934cf060e1de340dd33", aum = 0)
    /* renamed from: zw */
    private void m30401zw() {
        super.mo1099zx();
        if (bHa()) {
            for (Turret zx : afr()) {
                zx.mo1099zx();
            }
            mo18312da(cVr());
            mo2967X((Actor) null);
        }
        if (bHb()) {
            for (C3904wY wYVar : C3904wY.values()) {
                if (mo18313e(wYVar) != null) {
                    mo18313e(wYVar).bai();
                }
            }
            for (ShipStructure bVG : agt()) {
                for (ShipSector iterator : bVG.bVG()) {
                    Iterator<Item> iterator2 = iterator.getIterator();
                    while (iterator2.hasNext()) {
                        Item next = iterator2.next();
                        if (next instanceof Weapon) {
                            ((Weapon) next).cTX();
                        }
                    }
                }
            }
        }
    }

    @C4034yP
    @ClientOnly
    @C0064Am(aul = "af53e2cb7374264f0de1e2b03b096e2b", aum = 0)
    @C2499fr
    /* renamed from: ab */
    private void m30259ab(Asset tCVar) {
    }

    @C0064Am(aul = "6fc80ecd955c9138f148c66fe332bf22", aum = 0)
    private void ahT() {
        super.ahU();
        if (bGX()) {
            for (Turret ahU : ahA().agF()) {
                ahU.ahU();
            }
            if (this.ajg != null) {
                this.ajg.destroy();
                this.ajg = null;
            }
        }
    }

    @C0064Am(aul = "313e1edd9b5fd7c8cdc2b222d0e29c9f", aum = 0)
    private void ahV() {
        ahY();
        super.ahW();
    }

    @C0064Am(aul = "e464827c4fe236e7fb3af8bf2b8819cb", aum = 0)
    private void ahX() {
        if (bGX() && isDead() && mo2998hb() != null && (mo2998hb() instanceof PlayerController) && C5916acs.getSingolton().mo4089dL() == afm()) {
            ((PlayerController) mo2998hb()).cOc();
        }
    }

    @C0064Am(aul = "0dbc80bbf2acd369766e5bbb66bd665c", aum = 0)
    private void ahZ() {
        ahY();
        super.aia();
    }

    @C0064Am(aul = "0e979c149719c88bfdd76839abdf2c0f", aum = 0)
    /* renamed from: ay */
    private void m30264ay(float f) {
        super.step(f);
        this.box += f;
        if (this.box >= 0.005f) {
            this.box = 0.0f;
            if (afs() != null) {
                afs().step(f);
            }
            if (aft() != null) {
                aft().step(f);
            }
        }
        if (bGX() && !isStatic() && getPlayer().bQx() != this && getPlayer().bQx() != null && getPlayer().bQx().azW() != azW() && azW() != null && getPlayer().bQx().azW() != null) {
            mo6317hy("Removing non static actor " + this + " from client simulation. Reason: diferent node.");
            ahW();
        }
    }

    @C0064Am(aul = "8ca5318e5ae843db76d66327499ff3c2", aum = 0)
    private void aib() {
        super.aic();
        if (afr() != null) {
            for (Turret aic : afr()) {
                aic.aic();
            }
        }
    }

    @C0064Am(aul = "ed403b5753551efcb497b91db441de56", aum = 0)
    private void aid() {
        super.aie();
        if (afr() != null) {
            for (Turret aie : afr()) {
                aie.aie();
            }
        }
    }

    @C0064Am(aul = "6b2d199ffd6e859dedd86b09eb78fb76", aum = 0)
    @ClientOnly
    private void aif() {
        m30396zi().mo8320a(C2530gR.clX, (C6200aiQ<?>) this);
        if (this.bpf != null) {
            C3869w wVar = this.bpf;
            this.bpf = new C3869w(cHg(), wVar);
            wVar.dispose();
            return;
        }
        this.bpf = new C3869w(cHg(), this);
    }

    @C0064Am(aul = "abb5889c6537a1d2344ee6b6a8d476b9", aum = 0)
    @ClientOnly
    /* renamed from: f */
    private void m30338f(SceneObject sceneObject) {
        m30396zi().mo8320a(C2530gR.clX, (C6200aiQ<?>) this);
        if (this.bpf != null) {
            C3869w wVar = this.bpf;
            this.bpf = new C3869w(sceneObject, wVar);
            wVar.dispose();
            return;
        }
        this.bpf = new C3869w(sceneObject, this);
    }

    @C0064Am(aul = "c4b03c35decd795c61b7726923db52c0", aum = 0)
    @ClientOnly
    private void aih() {
        if (this.bpf == null) {
            mo8358lY("Bleed emitter not spawned");
            return;
        }
        float Tx = 1.0f - (mo8287zt().mo19923Tx() / mo8287zt().mo19934hh());
        if (Tx > 0.4f) {
            if (this.bpf.mo22693ah()) {
                this.bpf.mo22689ad();
            }
            if (Tx > 0.85f && this.bpf.mo22694ai()) {
                this.bpf.mo22691af();
            } else if (Tx < 0.85f && !this.bpf.mo22694ai()) {
                this.bpf.mo22692ag();
            }
            this.bpf.mo22702h(Tx);
            return;
        }
        if (!this.bpf.mo22693ah()) {
            this.bpf.mo22690ae();
        }
        if (!this.bpf.mo22694ai()) {
            this.bpf.mo22692ag();
        }
    }

    @C0064Am(aul = "8d5ace663bdb63497cf2baa14f830003", aum = 0)
    @ClientOnly
    private void aij() {
        if (this.bpf != null) {
            this.bpf.mo22690ae();
            this.bpf.mo22692ag();
        }
    }

    @C0064Am(aul = "543bf43b03d0462651709fb8ad1923ab", aum = 0)
    @ClientOnly
    /* renamed from: h */
    private void m30346h(SceneObject sceneObject) {
    }

    @C0064Am(aul = "b68fb6f3a9b9c12d49e43a5401e4539a", aum = 0)
    @ClientOnly
    private Color ail() {
        boolean z;
        boolean z2 = false;
        Character agj = agj();
        if (!(agj instanceof NPC)) {
            C6308akU aku = (C6308akU) agj;
            if (aku == null) {
                return ala().aIW().auQ();
            }
            if (getPlayer().mo12639T(aku) == Faction.C1624a.ENEMY) {
                return ala().aIW().ava();
            }
            Node azW = azW();
            if (azW != null && azW.mo21619Zf()) {
                return ala().aIW().avk();
            }
            if (aku.dxk() && aku.dxi().aQS().contains(getPlayer())) {
                return ala().aIW().auY();
            }
        } else if (!isStatic()) {
            if (getPlayer().mo12639T(agj) == Faction.C1624a.ENEMY) {
                z = true;
            } else {
                z = false;
            }
            Mission cWL = getPlayer().bQG().cWL();
            if (cWL != null) {
                z2 = cWL.mo137u((NPC) agj);
            }
            if (z) {
                if (z2) {
                    return ala().aIW().auS();
                }
                return ala().aIW().auO();
            } else if (z2) {
                return ala().aIW().auU();
            } else {
                return ala().aIW().auQ();
            }
        }
        return super.getColor();
    }

    @C0064Am(aul = "7247fc85b29a6c240a9cd6d5747f84d8", aum = 0)
    /* renamed from: a */
    private void m30250a(Hull jRVar, C5663aRz arz, Object obj) {
        if (jRVar == m30396zi()) {
        }
    }

    @C0064Am(aul = "e6f313929ab5a2615ccdfbfc21bf5981", aum = 0)
    /* renamed from: a */
    private void m30227a(long j, boolean z) {
        super.mo991b(j, z);
        for (Turret b : agF()) {
            b.mo991b(j, z);
        }
    }

    @C0064Am(aul = "4e9f16a575b009ade68dfdc639ea70a0", aum = 0)
    @C1253SX
    /* renamed from: D */
    private void m30185D(Vec3f vec3f) {
        if (ahO() != null) {
            ahO().mo16629E(vec3f);
        }
    }

    @C0064Am(aul = "9e1cc4ef8b8ac09b5eb077bcf16e01d7", aum = 0)
    @C1253SX
    private Vec3f aio() {
        if (ahO() == null) {
            return new Vec3f(0.0f, 0.0f, -1.0f);
        }
        return new Vec3f((Vector3f) ahO().czd());
    }

    @C0064Am(aul = "8af786a6b9a4a856470ce286adcd4ee0", aum = 0)
    @C2198cg
    /* renamed from: a */
    private void m30225a(int i, long j, Collection<C6625aqZ> collection, C6705asB asb) {
        if (cLd() != null) {
            asb.mo13024a(collection, (C6950awp) new C6729asZ(i, this, cLd().mo2472kQ(), j));
        }
    }

    @C0064Am(aul = "9e84b53f177da0f650752adb5bc1c111", aum = 0)
    private float aiq() {
        if (afa() <= 0.0f) {
            return ala().aJg().arl();
        }
        return afa();
    }

    @C4034yP
    @C0064Am(aul = "c651b6fd35ef96a254cce817c845ce84", aum = 0)
    @C2499fr
    @C1253SX
    /* renamed from: a */
    private void m30232a(C0286Dh dh) {
        if (dh instanceof Actor) {
            m30266b(dh, mo1013bx((Actor) dh));
        } else {
            m30266b(dh, getPosition().mo9486aC(dh.getPosition()));
        }
    }

    @C0064Am(aul = "b084088d06fd3e92e38b080f9b6c4bb1", aum = 0)
    private Vec3d ais() {
        return afd();
    }

    @C0064Am(aul = "43ddd657d1c023c15ff4f96ca884fec8", aum = 0)
    private Vec3d aiu() {
        return afe();
    }

    @C0064Am(aul = "24e29867e9121d0624db1aeaf1baa585", aum = 0)
    private Vec3d aiw() {
        return aff();
    }

    @C0064Am(aul = "867062449b60a3c404abb94188ab2b45", aum = 0)
    private float aiy() {
        return afg();
    }

    @C0064Am(aul = "efcec86d49a9541df17dd55843bf2bc5", aum = 0)
    private float aiz() {
        return afh();
    }

    @C0064Am(aul = "8f9bfac49b69f44f112fa5380fed96e3", aum = 0)
    private List<Item> aiA() {
        Clip bup;
        ArrayList arrayList = new ArrayList();
        for (ShipStructure bVG : agt()) {
            for (ShipSector iterator : bVG.bVG()) {
                Iterator<Item> iterator2 = iterator.getIterator();
                while (iterator2.hasNext()) {
                    Item next = iterator2.next();
                    if ((next instanceof ProjectileWeapon) && (bup = ((ProjectileWeapon) next).bup()) != null) {
                        arrayList.add(bup);
                    }
                    arrayList.add(next);
                }
            }
        }
        return arrayList;
    }

    @C0064Am(aul = "84bdd3b5baf042671ce78ec3f5bce502", aum = 0)
    /* renamed from: ls */
    private int m30366ls() {
        if (agj() == null) {
            return 0;
        }
        return agj().mo12658lt();
    }

    @C0064Am(aul = "3b287cced1156cbe9cec74c2a89b9258", aum = 0)
    /* renamed from: a */
    private ShipSector m30223a(SectorCategory fMVar) {
        for (ShipStructure bVG : agt()) {
            Iterator it = bVG.bVG().iterator();
            while (true) {
                if (it.hasNext()) {
                    ShipSector or = (ShipSector) it.next();
                    if (or.bmW() == fMVar) {
                        return or;
                    }
                }
            }
        }
        return null;
    }

    @C0064Am(aul = "5fdb3dd2bfb9fdf2237e3f30764d5e46", aum = 0)
    /* renamed from: B */
    private ShipSector m30182B(C3904wY wYVar) {
        for (ShipStructure bVG : agt()) {
            Iterator it = bVG.bVG().iterator();
            while (true) {
                if (it.hasNext()) {
                    ShipSector or = (ShipSector) it.next();
                    if (or.bmW() != null && or.bmW().mo18441sM() == wYVar) {
                        return or;
                    }
                }
            }
        }
        return null;
    }

    @C0064Am(aul = "a8946d8e9307d18ac80a4e16c4350f61", aum = 0)
    /* renamed from: a */
    private void m30235a(StellarSystem jj) {
        if (afm() instanceof C6308akU) {
            ((C6308akU) afm()).setLoading(true);
        }
        super.mo993b(jj);
    }

    @C0064Am(aul = "147b10fbecd15f7d4ccb3847216f0995", aum = 0)
    /* renamed from: TK */
    private void m30202TK() {
        m30205TP();
    }

    @C0064Am(aul = "9b0a9cc7c269ae7102ece0a24408d1ad", aum = 0)
    /* renamed from: TM */
    private void m30203TM() {
        m30205TP();
    }

    @C0064Am(aul = "c6d46e7972fb57996b07101fb885adcf", aum = 0)
    /* renamed from: TQ */
    private void m30206TQ() {
        if (m30207Te() == null) {
            logger.error(String.valueOf(bFY()) + " with NULL adapterList");
            return;
        }
        m30207Te().mo23259b((C6872avM) this);
        m30205TP();
    }

    /* JADX WARNING: Removed duplicated region for block: B:106:0x00f3 A[LOOP:1: B:53:0x00f3->B:106:0x00f3, LOOP_END, SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:70:0x0149  */
    /* JADX WARNING: Removed duplicated region for block: B:85:0x0191  */
    @p001a.C0064Am(aul = "c65bb0ca978162e1008ca7796f29b245", aum = 0)
    @ClientOnly
    /* renamed from: QV */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private boolean m30194QV() {
        /*
            r7 = this;
            r2 = 0
            r1 = 1
            boolean r0 = super.mo961QW()
            if (r0 == 0) goto L_0x000a
            r0 = r1
        L_0x0009:
            return r0
        L_0x000a:
            a.jR r0 = r7.m30396zi()
            if (r0 == 0) goto L_0x002c
            a.jR r0 = r7.m30396zi()
            a.TD r0 = p001a.C1298TD.m9830t(r0)
            boolean r0 = r0.bFT()
            if (r0 == 0) goto L_0x0020
            r0 = r1
            goto L_0x0009
        L_0x0020:
            a.jR r0 = r7.m30396zi()
            boolean r0 = r0.mo961QW()
            if (r0 == 0) goto L_0x002c
            r0 = r1
            goto L_0x0009
        L_0x002c:
            a.acX r0 = r7.afm()
            if (r0 == 0) goto L_0x004c
            a.acX r0 = r7.afm()
            a.TD r0 = p001a.C1298TD.m9830t(r0)
            boolean r0 = r0.bFT()
            if (r0 != 0) goto L_0x004a
            a.acX r0 = r7.afm()
            boolean r0 = r0.mo961QW()
            if (r0 == 0) goto L_0x004c
        L_0x004a:
            r0 = r1
            goto L_0x0009
        L_0x004c:
            a.wY[] r3 = p001a.C3904wY.values()
            int r4 = r3.length
            r0 = r2
        L_0x0052:
            if (r0 < r4) goto L_0x006a
            a.NG r0 = r7.agH()
            if (r0 == 0) goto L_0x0090
            a.NG r0 = r7.agH()
            a.TD r0 = p001a.C1298TD.m9830t(r0)
            boolean r0 = r0.bFT()
            if (r0 == 0) goto L_0x0090
            r0 = r1
            goto L_0x0009
        L_0x006a:
            r5 = r3[r0]
            a.adv r6 = r7.mo18313e((p001a.C3904wY) r5)
            if (r6 == 0) goto L_0x008d
            a.adv r6 = r7.mo18313e((p001a.C3904wY) r5)
            a.TD r6 = p001a.C1298TD.m9830t(r6)
            boolean r6 = r6.bFT()
            if (r6 != 0) goto L_0x008a
            a.adv r5 = r7.mo18313e((p001a.C3904wY) r5)
            boolean r5 = r5.mo961QW()
            if (r5 == 0) goto L_0x008d
        L_0x008a:
            r0 = r1
            goto L_0x0009
        L_0x008d:
            int r0 = r0 + 1
            goto L_0x0052
        L_0x0090:
            a.Jx r0 = r7.mo2998hb()
            boolean r0 = r0 instanceof p001a.C0699Jy
            if (r0 == 0) goto L_0x00a9
            a.Jx r0 = r7.mo2998hb()
            a.TD r0 = p001a.C1298TD.m9830t(r0)
            boolean r0 = r0.bFT()
            if (r0 == 0) goto L_0x00a9
            r0 = r1
            goto L_0x0009
        L_0x00a9:
            a.wI r0 = r7.afI()
            if (r0 == 0) goto L_0x00ca
            a.wI r0 = r7.afI()
            a.TD r0 = p001a.C1298TD.m9830t(r0)
            boolean r0 = r0.bFT()
            if (r0 != 0) goto L_0x00c7
            a.wI r0 = r7.afk()
            boolean r0 = r0.mo961QW()
            if (r0 == 0) goto L_0x00ca
        L_0x00c7:
            r0 = r1
            goto L_0x0009
        L_0x00ca:
            a.aRQ r0 = r7.afo()
            if (r0 == 0) goto L_0x00eb
            a.aRQ r0 = r7.afo()
            a.TD r0 = p001a.C1298TD.m9830t(r0)
            boolean r0 = r0.bFT()
            if (r0 != 0) goto L_0x00e8
            a.aRQ r0 = r7.afo()
            boolean r0 = r0.mo961QW()
            if (r0 == 0) goto L_0x00eb
        L_0x00e8:
            r0 = r1
            goto L_0x0009
        L_0x00eb:
            a.ra r0 = r7.agt()
            java.util.Iterator r3 = r0.iterator()
        L_0x00f3:
            boolean r0 = r3.hasNext()
            if (r0 != 0) goto L_0x011a
            a.gj r0 = r7.mo8288zv()
            if (r0 == 0) goto L_0x0162
            a.gj r0 = r7.mo8288zv()
            a.TD r0 = p001a.C1298TD.m9830t(r0)
            boolean r0 = r0.bFT()
            if (r0 != 0) goto L_0x0117
            a.gj r0 = r7.mo8288zv()
            boolean r0 = r0.mo961QW()
            if (r0 == 0) goto L_0x0162
        L_0x0117:
            r0 = r1
            goto L_0x0009
        L_0x011a:
            java.lang.Object r0 = r3.next()
            a.afK r0 = (p001a.C6038afK) r0
            a.TD r4 = p001a.C1298TD.m9830t(r0)
            boolean r4 = r4.bFT()
            if (r4 != 0) goto L_0x0138
            a.gj r4 = r0.mo13246zv()
            a.TD r4 = p001a.C1298TD.m9830t(r4)
            boolean r4 = r4.bFT()
            if (r4 == 0) goto L_0x013b
        L_0x0138:
            r0 = r1
            goto L_0x0009
        L_0x013b:
            a.ra r0 = r0.bVG()
            java.util.Iterator r4 = r0.iterator()
        L_0x0143:
            boolean r0 = r4.hasNext()
            if (r0 == 0) goto L_0x00f3
            java.lang.Object r0 = r4.next()
            a.OR r0 = (p001a.C0981OR) r0
            a.TD r5 = p001a.C1298TD.m9830t(r0)
            boolean r5 = r5.bFT()
            if (r5 != 0) goto L_0x015f
            boolean r0 = r0.mo961QW()
            if (r0 == 0) goto L_0x0143
        L_0x015f:
            r0 = r1
            goto L_0x0009
        L_0x0162:
            a.ra r0 = r7.afr()
            java.util.Iterator r3 = r0.iterator()
        L_0x016a:
            boolean r0 = r3.hasNext()
            if (r0 != 0) goto L_0x0191
            a.fk r0 = r7.afj()
            if (r0 == 0) goto L_0x01aa
            a.fk r0 = r7.afj()
            a.TD r0 = p001a.C1298TD.m9830t(r0)
            boolean r0 = r0.bFT()
            if (r0 != 0) goto L_0x018e
            a.fk r0 = r7.afj()
            boolean r0 = r0.mo961QW()
            if (r0 == 0) goto L_0x01aa
        L_0x018e:
            r0 = r1
            goto L_0x0009
        L_0x0191:
            java.lang.Object r0 = r3.next()
            a.JQ r0 = (p001a.C0652JQ) r0
            a.TD r4 = p001a.C1298TD.m9830t(r0)
            boolean r4 = r4.bFT()
            if (r4 != 0) goto L_0x01a7
            boolean r0 = r0.mo961QW()
            if (r0 == 0) goto L_0x016a
        L_0x01a7:
            r0 = r1
            goto L_0x0009
        L_0x01aa:
            a.dq r0 = r7.cyP()
            if (r0 == 0) goto L_0x01c1
            a.dq r0 = r7.cyP()
            a.TD r0 = p001a.C1298TD.m9830t(r0)
            boolean r0 = r0.bFT()
            if (r0 == 0) goto L_0x01c1
            r0 = r1
            goto L_0x0009
        L_0x01c1:
            r0 = r2
            goto L_0x0009
        */
        throw new UnsupportedOperationException("Method not decompiled: p001a.C2410fA.m30194QV():boolean");
    }

    @C0064Am(aul = "8fd9bcb3f0695eaf90fa50fcc85b8bca", aum = 0)
    /* renamed from: d */
    private boolean m30304d(C5663aRz arz) {
        if ((arz == C3981xi.etw || arz == C3981xi.etx) && bGX() && agj() == getPlayer()) {
            return false;
        }
        return true;
    }

    @C6485anp
    @C5511aMd
    /* renamed from: a.fA$b */
    /* compiled from: a */
    public class BaseAdapter extends ShipAdapter implements C1616Xf {
        /* renamed from: PF */
        public static final C2491fm f7169PF = null;
        public static final int _m_fieldCount = 0;
        public static final C5663aRz[] _m_fields = null;
        public static final int _m_methodCount = 0;
        public static final C2491fm[] _m_methods = null;
        /* renamed from: aT */
        public static final C5663aRz f7170aT = null;
        public static final C2491fm aTe = null;
        public static final C2491fm aTf = null;
        public static final C2491fm aTg = null;
        public static final C2491fm aTh = null;
        public static final C2491fm apF = null;
        public static final C2491fm apG = null;
        public static final C2491fm bpZ = null;
        public static final C2491fm exs = null;
        public static final C2491fm ext = null;
        public static final C2491fm exu = null;
        public static final long serialVersionUID = 0;
        public static C6494any ___iScriptClass;
        @C0064Am(aul = "765ba710f57a0bcfe2685870fbfc988e", aum = 0)

        /* renamed from: Lq */
        static /* synthetic */ Ship f7168Lq;

        static {
            m30519V();
        }

        public BaseAdapter() {
            super((C5540aNg) null);
            super.mo10S();
        }

        public BaseAdapter(C5540aNg ang) {
            super(ang);
        }

        public BaseAdapter(Ship fAVar) {
            super((C5540aNg) null);
            super._m_script_init(fAVar);
        }

        /* renamed from: V */
        static void m30519V() {
            serialVersionUID = (long) 1;
            _m_fieldCount = ShipAdapter._m_fieldCount + 1;
            _m_methodCount = ShipAdapter._m_methodCount + 11;
            int i = ShipAdapter._m_fieldCount;
            C5663aRz[] arzArr = new C5663aRz[(i + 1)];
            C5663aRz b = C5640aRc.m17844b(BaseAdapter.class, "765ba710f57a0bcfe2685870fbfc988e", i);
            f7170aT = b;
            arzArr[i] = b;
            int i2 = i + 1;
            _m_fields = arzArr;
            C1634Xv.m11725a((Object[]) ShipAdapter._m_fields, (Object[]) _m_fields);
            int i3 = ShipAdapter._m_methodCount;
            C2491fm[] fmVarArr = new C2491fm[(i3 + 11)];
            C2491fm a = C4105zY.m41624a(BaseAdapter.class, "df8e8d2be39996c894c64819b82c808c", i3);
            exs = a;
            fmVarArr[i3] = a;
            int i4 = i3 + 1;
            C2491fm a2 = C4105zY.m41624a(BaseAdapter.class, "b123753bfa9d67cb4d3120080041a93b", i4);
            aTe = a2;
            fmVarArr[i4] = a2;
            int i5 = i4 + 1;
            C2491fm a3 = C4105zY.m41624a(BaseAdapter.class, "119b733074d3feaef619737fab9cc887", i5);
            aTf = a3;
            fmVarArr[i5] = a3;
            int i6 = i5 + 1;
            C2491fm a4 = C4105zY.m41624a(BaseAdapter.class, "f8970e4872218acafea00f2842e72aa1", i6);
            aTg = a4;
            fmVarArr[i6] = a4;
            int i7 = i6 + 1;
            C2491fm a5 = C4105zY.m41624a(BaseAdapter.class, "775f67640f06b7ed95919ce0febf6819", i7);
            aTh = a5;
            fmVarArr[i7] = a5;
            int i8 = i7 + 1;
            C2491fm a6 = C4105zY.m41624a(BaseAdapter.class, "dc15d41fb79f312becd3bb6b9946c7c0", i8);
            bpZ = a6;
            fmVarArr[i8] = a6;
            int i9 = i8 + 1;
            C2491fm a7 = C4105zY.m41624a(BaseAdapter.class, "94c6815b122316845e099bf826961ed4", i9);
            ext = a7;
            fmVarArr[i9] = a7;
            int i10 = i9 + 1;
            C2491fm a8 = C4105zY.m41624a(BaseAdapter.class, "3724e15673822830726c10a0a5c29030", i10);
            exu = a8;
            fmVarArr[i10] = a8;
            int i11 = i10 + 1;
            C2491fm a9 = C4105zY.m41624a(BaseAdapter.class, "018af73d1f813fcdfd221c55c04f4175", i11);
            apF = a9;
            fmVarArr[i11] = a9;
            int i12 = i11 + 1;
            C2491fm a10 = C4105zY.m41624a(BaseAdapter.class, "25326df36af5f81e7af6e673ceb3ad4a", i12);
            f7169PF = a10;
            fmVarArr[i12] = a10;
            int i13 = i12 + 1;
            C2491fm a11 = C4105zY.m41624a(BaseAdapter.class, "b3e934702f7e6287c69cc673dea74037", i13);
            apG = a11;
            fmVarArr[i13] = a11;
            int i14 = i13 + 1;
            _m_methods = fmVarArr;
            C1634Xv.m11725a((Object[]) ShipAdapter._m_methods, (Object[]) _m_methods);
            ___iScriptClass = aUO.m18566a(BaseAdapter.class, C1392UP.class, _m_fields, _m_methods);
        }

        @C0064Am(aul = "018af73d1f813fcdfd221c55c04f4175", aum = 0)
        /* renamed from: ID */
        private GameObjectAdapter m30517ID() {
            return bDq();
        }

        @C0064Am(aul = "b3e934702f7e6287c69cc673dea74037", aum = 0)
        /* renamed from: IF */
        private GameObjectAdapter m30518IF() {
            return bDs();
        }

        @C0064Am(aul = "25326df36af5f81e7af6e673ceb3ad4a", aum = 0)
        /* renamed from: a */
        private void m30525a(GameObjectAdapter ato, GameObjectAdapter ato2, GameObjectAdapter ato3) {
            mo18336b((ShipAdapter) ato, (ShipAdapter) ato2, (ShipAdapter) ato3);
        }

        /* renamed from: d */
        private void m30526d(Ship fAVar) {
            bFf().mo5608dq().mo3197f(f7170aT, fAVar);
        }

        /* renamed from: rd */
        private Ship m30527rd() {
            return (Ship) bFf().mo5608dq().mo3214p(f7170aT);
        }

        /* renamed from: IE */
        public /* bridge */ /* synthetic */ GameObjectAdapter mo16070IE() {
            switch (bFf().mo6893i(apF)) {
                case 0:
                    return null;
                case 2:
                    return (GameObjectAdapter) bFf().mo5606d(new aCE(this, apF, new Object[0]));
                case 3:
                    bFf().mo5606d(new aCE(this, apF, new Object[0]));
                    break;
            }
            return m30517ID();
        }

        /* renamed from: IG */
        public /* bridge */ /* synthetic */ GameObjectAdapter mo16071IG() {
            switch (bFf().mo6893i(apG)) {
                case 0:
                    return null;
                case 2:
                    return (GameObjectAdapter) bFf().mo5606d(new aCE(this, apG, new Object[0]));
                case 3:
                    bFf().mo5606d(new aCE(this, apG, new Object[0]));
                    break;
            }
            return m30518IF();
        }

        /* renamed from: S */
        public void mo10S() {
            super.mo10S();
        }

        /* renamed from: T */
        public C6494any mo11T() {
            return ___iScriptClass;
        }

        /* renamed from: U */
        public C2491fm[] mo12U() {
            return _m_methods;
        }

        /* renamed from: VF */
        public float mo5665VF() {
            switch (bFf().mo6893i(aTe)) {
                case 0:
                    return 0.0f;
                case 2:
                    return ((Float) bFf().mo5606d(new aCE(this, aTe, new Object[0]))).floatValue();
                case 3:
                    bFf().mo5606d(new aCE(this, aTe, new Object[0]));
                    break;
            }
            return m30520VE();
        }

        /* renamed from: VH */
        public float mo5666VH() {
            switch (bFf().mo6893i(aTf)) {
                case 0:
                    return 0.0f;
                case 2:
                    return ((Float) bFf().mo5606d(new aCE(this, aTf, new Object[0]))).floatValue();
                case 3:
                    bFf().mo5606d(new aCE(this, aTf, new Object[0]));
                    break;
            }
            return m30521VG();
        }

        /* renamed from: W */
        public Object mo13W() {
            return new C1392UP(this);
        }

        /* renamed from: a */
        public Object mo14a(C0495Gr gr) {
            Object[] args = gr.getArgs();
            switch (gr.mo2417hq() - ShipAdapter._m_methodCount) {
                case 0:
                    return bDp();
                case 1:
                    return new Float(m30520VE());
                case 2:
                    return new Float(m30521VG());
                case 3:
                    return new Float(m30522VI());
                case 4:
                    return new Float(m30523VJ());
                case 5:
                    return new Float(ago());
                case 6:
                    m30524a((ShipAdapter) args[0], (ShipAdapter) args[1], (ShipAdapter) args[2]);
                    return null;
                case 7:
                    return bDr();
                case 8:
                    return m30517ID();
                case 9:
                    m30525a((GameObjectAdapter) args[0], (GameObjectAdapter) args[1], (GameObjectAdapter) args[2]);
                    return null;
                case 10:
                    return m30518IF();
                default:
                    return super.mo14a(gr);
            }
        }

        /* renamed from: a */
        public void mo15a(Collection collection, C0495Gr gr) {
            super.mo15a(collection, gr);
        }

        public float agp() {
            switch (bFf().mo6893i(bpZ)) {
                case 0:
                    return 0.0f;
                case 2:
                    return ((Float) bFf().mo5606d(new aCE(this, bpZ, new Object[0]))).floatValue();
                case 3:
                    bFf().mo5606d(new aCE(this, bpZ, new Object[0]));
                    break;
            }
            return ago();
        }

        /* renamed from: b */
        public void mo18336b(ShipAdapter tu, ShipAdapter tu2, ShipAdapter tu3) {
            switch (bFf().mo6893i(ext)) {
                case 0:
                    return;
                case 2:
                    bFf().mo5606d(new aCE(this, ext, new Object[]{tu, tu2, tu3}));
                    return;
                case 3:
                    bFf().mo5606d(new aCE(this, ext, new Object[]{tu, tu2, tu3}));
                    break;
            }
            m30524a(tu, tu2, tu3);
        }

        /* renamed from: b */
        public /* bridge */ /* synthetic */ void mo12950b(GameObjectAdapter ato, GameObjectAdapter ato2, GameObjectAdapter ato3) {
            switch (bFf().mo6893i(f7169PF)) {
                case 0:
                    return;
                case 2:
                    bFf().mo5606d(new aCE(this, f7169PF, new Object[]{ato, ato2, ato3}));
                    return;
                case 3:
                    bFf().mo5606d(new aCE(this, f7169PF, new Object[]{ato, ato2, ato3}));
                    break;
            }
            m30525a(ato, ato2, ato3);
        }

        public ShipAdapter bDq() {
            switch (bFf().mo6893i(exs)) {
                case 0:
                    return null;
                case 2:
                    return (ShipAdapter) bFf().mo5606d(new aCE(this, exs, new Object[0]));
                case 3:
                    bFf().mo5606d(new aCE(this, exs, new Object[0]));
                    break;
            }
            return bDp();
        }

        public ShipAdapter bDs() {
            switch (bFf().mo6893i(exu)) {
                case 0:
                    return null;
                case 2:
                    return (ShipAdapter) bFf().mo5606d(new aCE(this, exu, new Object[0]));
                case 3:
                    bFf().mo5606d(new aCE(this, exu, new Object[0]));
                    break;
            }
            return bDr();
        }

        /* renamed from: c */
        public C5663aRz[] mo25c() {
            return _m_fields;
        }

        /* renamed from: ra */
        public float mo5668ra() {
            switch (bFf().mo6893i(aTh)) {
                case 0:
                    return 0.0f;
                case 2:
                    return ((Float) bFf().mo5606d(new aCE(this, aTh, new Object[0]))).floatValue();
                case 3:
                    bFf().mo5606d(new aCE(this, aTh, new Object[0]));
                    break;
            }
            return m30523VJ();
        }

        /* renamed from: rb */
        public float mo5669rb() {
            switch (bFf().mo6893i(aTg)) {
                case 0:
                    return 0.0f;
                case 2:
                    return ((Float) bFf().mo5606d(new aCE(this, aTg, new Object[0]))).floatValue();
                case 3:
                    bFf().mo5606d(new aCE(this, aTg, new Object[0]));
                    break;
            }
            return m30522VI();
        }

        /* renamed from: L */
        public void mo18335L(Ship fAVar) {
            m30526d(fAVar);
            super.mo10S();
        }

        @C0064Am(aul = "df8e8d2be39996c894c64819b82c808c", aum = 0)
        private ShipAdapter bDp() {
            return null;
        }

        @C0064Am(aul = "b123753bfa9d67cb4d3120080041a93b", aum = 0)
        /* renamed from: VE */
        private float m30520VE() {
            return m30527rd().agP();
        }

        @C0064Am(aul = "119b733074d3feaef619737fab9cc887", aum = 0)
        /* renamed from: VG */
        private float m30521VG() {
            return m30527rd().agR();
        }

        @C0064Am(aul = "f8970e4872218acafea00f2842e72aa1", aum = 0)
        /* renamed from: VI */
        private float m30522VI() {
            return m30527rd().agT();
        }

        @C0064Am(aul = "775f67640f06b7ed95919ce0febf6819", aum = 0)
        /* renamed from: VJ */
        private float m30523VJ() {
            return m30527rd().agV();
        }

        @C0064Am(aul = "dc15d41fb79f312becd3bb6b9946c7c0", aum = 0)
        private float ago() {
            return m30527rd().agX();
        }

        @C0064Am(aul = "94c6815b122316845e099bf826961ed4", aum = 0)
        /* renamed from: a */
        private void m30524a(ShipAdapter tu, ShipAdapter tu2, ShipAdapter tu3) {
            if (tu != null) {
                throw new IllegalArgumentException("base can't have a previous");
            } else if (tu3 != this) {
                throw new IllegalArgumentException("base can't have another base");
            } else {
                super.mo12950b(tu, tu2, tu3);
            }
        }

        @C0064Am(aul = "3724e15673822830726c10a0a5c29030", aum = 0)
        private ShipAdapter bDr() {
            return null;
        }
    }

    /* renamed from: a.fA$c */
    /* compiled from: a */
    class C2413c implements C2601hQ {
        private final /* synthetic */ Actor.C0200h eyn;

        C2413c(Actor.C0200h hVar) {
            this.eyn = hVar;
        }

        /* renamed from: b */
        public void mo663b(C4029yK yKVar) {
            this.eyn.mo1103c(yKVar);
        }
    }

    @C6485anp
    @C5511aMd
    /* renamed from: a.fA$a */
    public class DeathInformation extends TaikodomObject implements C1616Xf {
        /* renamed from: Lp */
        public static final C5663aRz f7161Lp = null;
        /* renamed from: Lr */
        public static final C2491fm f7163Lr = null;
        public static final int _m_fieldCount = 0;
        public static final C5663aRz[] _m_fields = null;
        public static final int _m_methodCount = 0;
        public static final C2491fm[] _m_methods = null;
        /* renamed from: aT */
        public static final C5663aRz f7164aT = null;
        public static final long serialVersionUID = 0;
        /* renamed from: uK */
        public static final C5663aRz f7166uK = null;
        /* renamed from: uW */
        public static final C2491fm f7167uW = null;
        public static C6494any ___iScriptClass;
        @C0064Am(aul = "f75599f30da5d42cef2f9f33fd4f137c", aum = 2)

        /* renamed from: Lq */
        static /* synthetic */ Ship f7162Lq;
        @C0064Am(aul = "a0daeb3f3a4cda30cfbcbe95ea5a17cb", aum = 0)

        /* renamed from: Lo */
        private static Actor f7160Lo;
        @C0064Am(aul = "3bf8de174ed58fb9dc58043b52b94a46", aum = 1)

        /* renamed from: uJ */
        private static C5260aCm f7165uJ;

        static {
            m30498V();
        }

        public DeathInformation() {
            super((C5540aNg) null);
            super.mo10S();
        }

        public DeathInformation(C5540aNg ang) {
            super(ang);
        }

        public DeathInformation(Ship fAVar, Actor cr, C5260aCm acm) {
            super((C5540aNg) null);
            super._m_script_init(fAVar, cr, acm);
        }

        /* renamed from: V */
        static void m30498V() {
            serialVersionUID = (long) 1;
            _m_fieldCount = TaikodomObject._m_fieldCount + 3;
            _m_methodCount = TaikodomObject._m_methodCount + 2;
            int i = TaikodomObject._m_fieldCount;
            C5663aRz[] arzArr = new C5663aRz[(i + 3)];
            C5663aRz b = C5640aRc.m17844b(DeathInformation.class, "a0daeb3f3a4cda30cfbcbe95ea5a17cb", i);
            f7161Lp = b;
            arzArr[i] = b;
            int i2 = i + 1;
            C5663aRz b2 = C5640aRc.m17844b(DeathInformation.class, "3bf8de174ed58fb9dc58043b52b94a46", i2);
            f7166uK = b2;
            arzArr[i2] = b2;
            int i3 = i2 + 1;
            C5663aRz b3 = C5640aRc.m17844b(DeathInformation.class, "f75599f30da5d42cef2f9f33fd4f137c", i3);
            f7164aT = b3;
            arzArr[i3] = b3;
            int i4 = i3 + 1;
            _m_fields = arzArr;
            C1634Xv.m11725a((Object[]) TaikodomObject._m_fields, (Object[]) _m_fields);
            int i5 = TaikodomObject._m_methodCount;
            C2491fm[] fmVarArr = new C2491fm[(i5 + 2)];
            C2491fm a = C4105zY.m41624a(DeathInformation.class, "9acd9717111bcb1e8e5b9d520f7d564d", i5);
            f7163Lr = a;
            fmVarArr[i5] = a;
            int i6 = i5 + 1;
            C2491fm a2 = C4105zY.m41624a(DeathInformation.class, "658e2a82d04e42c10f611f564085171a", i6);
            f7167uW = a2;
            fmVarArr[i6] = a2;
            int i7 = i6 + 1;
            _m_methods = fmVarArr;
            C1634Xv.m11725a((Object[]) TaikodomObject._m_methods, (Object[]) _m_methods);
            ___iScriptClass = aUO.m18566a(DeathInformation.class, C3430rU.class, _m_fields, _m_methods);
        }

        /* renamed from: a */
        private void m30499a(C5260aCm acm) {
            bFf().mo5608dq().mo3197f(f7166uK, acm);
        }

        /* renamed from: d */
        private void m30500d(Ship fAVar) {
            bFf().mo5608dq().mo3197f(f7164aT, fAVar);
        }

        /* renamed from: id */
        private C5260aCm m30501id() {
            return (C5260aCm) bFf().mo5608dq().mo3214p(f7166uK);
        }

        /* renamed from: rc */
        private Actor m30503rc() {
            return (Actor) bFf().mo5608dq().mo3214p(f7161Lp);
        }

        /* renamed from: rd */
        private Ship m30504rd() {
            return (Ship) bFf().mo5608dq().mo3214p(f7164aT);
        }

        /* renamed from: v */
        private void m30506v(Actor cr) {
            bFf().mo5608dq().mo3197f(f7161Lp, cr);
        }

        /* renamed from: S */
        public void mo10S() {
            super.mo10S();
        }

        /* renamed from: T */
        public C6494any mo11T() {
            return ___iScriptClass;
        }

        /* renamed from: U */
        public C2491fm[] mo12U() {
            return _m_methods;
        }

        /* renamed from: W */
        public Object mo13W() {
            return new C3430rU(this);
        }

        /* renamed from: a */
        public Object mo14a(C0495Gr gr) {
            gr.getArgs();
            switch (gr.mo2417hq() - TaikodomObject._m_methodCount) {
                case 0:
                    return m30505re();
                case 1:
                    return m30502ik();
                default:
                    return super.mo14a(gr);
            }
        }

        /* renamed from: a */
        public void mo15a(Collection collection, C0495Gr gr) {
            super.mo15a(collection, gr);
        }

        /* renamed from: c */
        public C5663aRz[] mo25c() {
            return _m_fields;
        }

        /* renamed from: il */
        public C5260aCm mo18333il() {
            switch (bFf().mo6893i(f7167uW)) {
                case 0:
                    return null;
                case 2:
                    return (C5260aCm) bFf().mo5606d(new aCE(this, f7167uW, new Object[0]));
                case 3:
                    bFf().mo5606d(new aCE(this, f7167uW, new Object[0]));
                    break;
            }
            return m30502ik();
        }

        /* renamed from: rf */
        public Actor mo18334rf() {
            switch (bFf().mo6893i(f7163Lr)) {
                case 0:
                    return null;
                case 2:
                    return (Actor) bFf().mo5606d(new aCE(this, f7163Lr, new Object[0]));
                case 3:
                    bFf().mo5606d(new aCE(this, f7163Lr, new Object[0]));
                    break;
            }
            return m30505re();
        }

        /* renamed from: a */
        public void mo18332a(Ship fAVar, Actor cr, C5260aCm acm) {
            m30500d(fAVar);
            super.mo10S();
            m30506v(cr);
            m30499a(acm);
        }

        @C0064Am(aul = "9acd9717111bcb1e8e5b9d520f7d564d", aum = 0)
        /* renamed from: re */
        private Actor m30505re() {
            return m30503rc();
        }

        @C0064Am(aul = "658e2a82d04e42c10f611f564085171a", aum = 0)
        /* renamed from: ik */
        private C5260aCm m30502ik() {
            return m30501id();
        }
    }
}
