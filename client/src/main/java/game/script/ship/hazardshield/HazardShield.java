package game.script.ship.hazardshield;

import com.hoplon.geometry.Color;
import game.network.message.externalizable.aCE;
import game.network.message.serializable.C2686iZ;
import game.script.TaikodomObject;
import game.script.damage.DamageType;
import game.script.resource.Asset;
import game.script.ship.HullAdapter;
import game.script.ship.ShieldAdapter;
import game.script.ship.Ship;
import game.script.template.BaseTaikodomContent;
import game.script.util.AdapterLikedList;
import logic.WrapRunnable;
import logic.baa.*;
import logic.data.mbean.*;
import logic.res.ILoaderTrail;
import logic.res.code.C5640aRc;
import logic.res.code.C5663aRz;
import logic.res.code.C6494any;
import logic.res.html.C2491fm;
import p001a.*;
import taikodom.render.textures.Material;

import java.util.Collection;

@C6485anp
@C2712iu(mo19786Bt = BaseTaikodomContent.class)
@C5511aMd
/* renamed from: a.ca */
/* compiled from: a */
public class HazardShield extends TaikodomObject implements C1616Xf, C6872avM {

    /* renamed from: Lm */
    public static final C2491fm f6167Lm = null;

    /* renamed from: Wp */
    public static final C2491fm f6168Wp = null;
    public static final C2491fm _f_dispose_0020_0028_0029V = null;
    public static final int _m_fieldCount = 0;
    public static final C5663aRz[] _m_fields = null;
    public static final int _m_methodCount = 0;
    public static final C2491fm[] _m_methods = null;
    public static final C5663aRz aAe = null;
    public static final C5663aRz aAg = null;
    public static final C5663aRz aOx = null;
    public static final C2491fm aPe = null;
    public static final C2491fm aPf = null;
    public static final C2491fm aPj = null;
    public static final C2491fm aPk = null;
    public static final C2491fm aPl = null;
    public static final C2491fm aPm = null;
    public static final C5663aRz atQ = null;
    public static final C5663aRz atT = null;
    public static final C2491fm axb = null;
    public static final C5663aRz bbI = null;
    public static final C5663aRz bbK = null;
    public static final C2491fm bbQ = null;
    public static final C2491fm bbS = null;
    public static final C2491fm bbU = null;
    public static final C5663aRz bid = null;
    public static final C2491fm bjZ = null;
    public static final C2491fm bka = null;
    public static final C2491fm bkd = null;
    public static final C2491fm bkk = null;
    public static final C2491fm bkl = null;
    public static final C5663aRz hxZ = null;
    public static final C5663aRz hya = null;
    public static final C5663aRz hyb = null;
    public static final C5663aRz hyc = null;
    public static final C5663aRz hyd = null;
    public static final C5663aRz hye = null;
    public static final C2491fm hyf = null;
    public static final C2491fm hyg = null;
    public static final C2491fm hyh = null;
    public static final C2491fm hyi = null;
    public static final C2491fm hyj = null;
    public static final C2491fm hyk = null;
    public static final C2491fm hyl = null;
    public static final C2491fm hym = null;
    public static final C2491fm hyn = null;
    public static final long serialVersionUID = 0;
    public static C6494any ___iScriptClass;
    @C0064Am(aul = "90a2b58056892d219d57c5c224940bdb", aum = 4)
    private static Asset aAd;
    @C0064Am(aul = "e577891f92dbdcaf69639f88fd0c48de", aum = 5)
    private static Asset aAf;
    @C0064Am(aul = "ca8c0481d90f705cade9483c49ab8e08", aum = 12)
    @C5566aOg
    private static AdapterLikedList<HazardShieldAdapter> aOw;
    @C0064Am(aul = "a5e9ae4063ad294bb14fa038e2ca7f67", aum = 11)
    private static C6809auB.C1996a aZI;
    @C0064Am(aul = "7280aeff281ebeb301d0e0311e7f4f26", aum = 0)
    private static C3892wO bbH;
    @C0064Am(aul = "6fa66f9269616690963811a1e73d66c2", aum = 1)
    private static C2686iZ<DamageType> bbJ;
    @C0064Am(aul = "d2c13266ed9998911c6b7b760cb47dda", aum = 2)
    private static float bbL;
    @C0064Am(aul = "61ba70f03952b1cd187eb56a5285d5b2", aum = 3)
    private static float bbM;
    @C0064Am(aul = "3ba06f6cc30f912a27c93cf3e2243860", aum = 6)
    private static ShieldBlock hcb;
    @C0064Am(aul = "58efc9890ba52903898dbe81c45faef2", aum = 7)
    private static HullBlock hcc;
    @C0064Am(aul = "e871cad428abe047b53d1432da4a7b05", aum = 8)
    private static Ship hcd;
    @C0064Am(aul = "882943d28f875ccb887ac4006692ee59", aum = 9)
    private static InvulnerabilityEnterer hce;
    @C0064Am(aul = "4bcc3090a10c9a85fe9b11e62b06b1bc", aum = 10)
    private static InvulnerabilityExiter hcf;
    @C0064Am(aul = "c20ee6a4f0275a2e65623ed6408a37b8", aum = 13)
    private static C3892wO hcg;

    static {
        m28434V();
    }

    /* access modifiers changed from: private */
    @ClientOnly
    public C3738uP fye;
    @ClientOnly
    private WrapRunnable hky;

    public HazardShield() {
        super((C5540aNg) null);
        super.mo10S();
    }

    public HazardShield(C5540aNg ang) {
        super(ang);
    }

    public HazardShield(HazardShieldType rGVar) {
        super((C5540aNg) null);
        super._m_script_init(rGVar);
    }

    /* renamed from: V */
    static void m28434V() {
        serialVersionUID = (long) 1;
        _m_fieldCount = TaikodomObject._m_fieldCount + 14;
        _m_methodCount = TaikodomObject._m_methodCount + 27;
        int i = TaikodomObject._m_fieldCount;
        C5663aRz[] arzArr = new C5663aRz[(i + 14)];
        C5663aRz b = C5640aRc.m17844b(HazardShield.class, "7280aeff281ebeb301d0e0311e7f4f26", i);
        bbI = b;
        arzArr[i] = b;
        int i2 = i + 1;
        C5663aRz b2 = C5640aRc.m17844b(HazardShield.class, "6fa66f9269616690963811a1e73d66c2", i2);
        bbK = b2;
        arzArr[i2] = b2;
        int i3 = i2 + 1;
        C5663aRz b3 = C5640aRc.m17844b(HazardShield.class, "d2c13266ed9998911c6b7b760cb47dda", i3);
        atT = b3;
        arzArr[i3] = b3;
        int i4 = i3 + 1;
        C5663aRz b4 = C5640aRc.m17844b(HazardShield.class, "61ba70f03952b1cd187eb56a5285d5b2", i4);
        atQ = b4;
        arzArr[i4] = b4;
        int i5 = i4 + 1;
        C5663aRz b5 = C5640aRc.m17844b(HazardShield.class, "90a2b58056892d219d57c5c224940bdb", i5);
        aAe = b5;
        arzArr[i5] = b5;
        int i6 = i5 + 1;
        C5663aRz b6 = C5640aRc.m17844b(HazardShield.class, "e577891f92dbdcaf69639f88fd0c48de", i6);
        aAg = b6;
        arzArr[i6] = b6;
        int i7 = i6 + 1;
        C5663aRz b7 = C5640aRc.m17844b(HazardShield.class, "3ba06f6cc30f912a27c93cf3e2243860", i7);
        hxZ = b7;
        arzArr[i7] = b7;
        int i8 = i7 + 1;
        C5663aRz b8 = C5640aRc.m17844b(HazardShield.class, "58efc9890ba52903898dbe81c45faef2", i8);
        hya = b8;
        arzArr[i8] = b8;
        int i9 = i8 + 1;
        C5663aRz b9 = C5640aRc.m17844b(HazardShield.class, "e871cad428abe047b53d1432da4a7b05", i9);
        hyb = b9;
        arzArr[i9] = b9;
        int i10 = i9 + 1;
        C5663aRz b10 = C5640aRc.m17844b(HazardShield.class, "882943d28f875ccb887ac4006692ee59", i10);
        hyc = b10;
        arzArr[i10] = b10;
        int i11 = i10 + 1;
        C5663aRz b11 = C5640aRc.m17844b(HazardShield.class, "4bcc3090a10c9a85fe9b11e62b06b1bc", i11);
        hyd = b11;
        arzArr[i11] = b11;
        int i12 = i11 + 1;
        C5663aRz b12 = C5640aRc.m17844b(HazardShield.class, "a5e9ae4063ad294bb14fa038e2ca7f67", i12);
        bid = b12;
        arzArr[i12] = b12;
        int i13 = i12 + 1;
        C5663aRz b13 = C5640aRc.m17844b(HazardShield.class, "ca8c0481d90f705cade9483c49ab8e08", i13);
        aOx = b13;
        arzArr[i13] = b13;
        int i14 = i13 + 1;
        C5663aRz b14 = C5640aRc.m17844b(HazardShield.class, "c20ee6a4f0275a2e65623ed6408a37b8", i14);
        hye = b14;
        arzArr[i14] = b14;
        int i15 = i14 + 1;
        _m_fields = arzArr;
        C1634Xv.m11725a((Object[]) TaikodomObject._m_fields, (Object[]) _m_fields);
        int i16 = TaikodomObject._m_methodCount;
        C2491fm[] fmVarArr = new C2491fm[(i16 + 27)];
        C2491fm a = C4105zY.m41624a(HazardShield.class, "eaf6d1cddefa58975e4be79449049e51", i16);
        axb = a;
        fmVarArr[i16] = a;
        int i17 = i16 + 1;
        C2491fm a2 = C4105zY.m41624a(HazardShield.class, "5b71483b212329439101b5e4dc80f72a", i17);
        hyf = a2;
        fmVarArr[i17] = a2;
        int i18 = i17 + 1;
        C2491fm a3 = C4105zY.m41624a(HazardShield.class, "2d456e5fdecc01b0fa21666fce08bcc6", i18);
        hyg = a3;
        fmVarArr[i18] = a3;
        int i19 = i18 + 1;
        C2491fm a4 = C4105zY.m41624a(HazardShield.class, "08c6061eb25f51592827525d772fd7b7", i19);
        hyh = a4;
        fmVarArr[i19] = a4;
        int i20 = i19 + 1;
        C2491fm a5 = C4105zY.m41624a(HazardShield.class, "38a3f44815b457d50a66b637a2cf4010", i20);
        hyi = a5;
        fmVarArr[i20] = a5;
        int i21 = i20 + 1;
        C2491fm a6 = C4105zY.m41624a(HazardShield.class, "458b69bbe20b52197f9fad4e3a8b6551", i21);
        hyj = a6;
        fmVarArr[i21] = a6;
        int i22 = i21 + 1;
        C2491fm a7 = C4105zY.m41624a(HazardShield.class, "2349f9669640818a77475e628a24d6e5", i22);
        hyk = a7;
        fmVarArr[i22] = a7;
        int i23 = i22 + 1;
        C2491fm a8 = C4105zY.m41624a(HazardShield.class, "5138099b8f7c1a1a5787918862258fdd", i23);
        bka = a8;
        fmVarArr[i23] = a8;
        int i24 = i23 + 1;
        C2491fm a9 = C4105zY.m41624a(HazardShield.class, "d568cb12be279575bfba010dbf3da690", i24);
        f6168Wp = a9;
        fmVarArr[i24] = a9;
        int i25 = i24 + 1;
        C2491fm a10 = C4105zY.m41624a(HazardShield.class, "e1eeb7ae9178b34965d9bd03602b4302", i25);
        bkd = a10;
        fmVarArr[i25] = a10;
        int i26 = i25 + 1;
        C2491fm a11 = C4105zY.m41624a(HazardShield.class, "af981eea2d67a39d6594cddf842930b9", i26);
        _f_dispose_0020_0028_0029V = a11;
        fmVarArr[i26] = a11;
        int i27 = i26 + 1;
        C2491fm a12 = C4105zY.m41624a(HazardShield.class, "3d227eb3c757d338e5721f2da4a3fe31", i27);
        f6167Lm = a12;
        fmVarArr[i27] = a12;
        int i28 = i27 + 1;
        C2491fm a13 = C4105zY.m41624a(HazardShield.class, "0dc2b342bf949c173f605cf11becf283", i28);
        bkk = a13;
        fmVarArr[i28] = a13;
        int i29 = i28 + 1;
        C2491fm a14 = C4105zY.m41624a(HazardShield.class, "c24ab6e98adb46743d2304e0446561e8", i29);
        bkl = a14;
        fmVarArr[i29] = a14;
        int i30 = i29 + 1;
        C2491fm a15 = C4105zY.m41624a(HazardShield.class, "b55be488adaad66948df46dbc9b09eda", i30);
        bjZ = a15;
        fmVarArr[i30] = a15;
        int i31 = i30 + 1;
        C2491fm a16 = C4105zY.m41624a(HazardShield.class, "d9b24cc26557303db2c78a0ed4c21c7f", i31);
        bbQ = a16;
        fmVarArr[i31] = a16;
        int i32 = i31 + 1;
        C2491fm a17 = C4105zY.m41624a(HazardShield.class, "dc1ab31248e933a29a88295e19c5eb37", i32);
        hyl = a17;
        fmVarArr[i32] = a17;
        int i33 = i32 + 1;
        C2491fm a18 = C4105zY.m41624a(HazardShield.class, "8a10eacada1cd06f3221955018d37627", i33);
        bbS = a18;
        fmVarArr[i33] = a18;
        int i34 = i33 + 1;
        C2491fm a19 = C4105zY.m41624a(HazardShield.class, "bb1aca9f9f50c73f36b7041c9446ca7a", i34);
        bbU = a19;
        fmVarArr[i34] = a19;
        int i35 = i34 + 1;
        C2491fm a20 = C4105zY.m41624a(HazardShield.class, "d1fb469e256943308d51d81be1c64ca8", i35);
        hym = a20;
        fmVarArr[i35] = a20;
        int i36 = i35 + 1;
        C2491fm a21 = C4105zY.m41624a(HazardShield.class, "d3e9ccb0095cf59ee720b14d387f4f28", i36);
        hyn = a21;
        fmVarArr[i36] = a21;
        int i37 = i36 + 1;
        C2491fm a22 = C4105zY.m41624a(HazardShield.class, "7c1d7a0269d12d2d290026b32285077c", i37);
        aPf = a22;
        fmVarArr[i37] = a22;
        int i38 = i37 + 1;
        C2491fm a23 = C4105zY.m41624a(HazardShield.class, "541efc5311bb8e74c64eb08b2c6d4059", i38);
        aPe = a23;
        fmVarArr[i38] = a23;
        int i39 = i38 + 1;
        C2491fm a24 = C4105zY.m41624a(HazardShield.class, "4cd91aee477295b4e2b6796052e0dd50", i39);
        aPj = a24;
        fmVarArr[i39] = a24;
        int i40 = i39 + 1;
        C2491fm a25 = C4105zY.m41624a(HazardShield.class, "69f3518c539ea4cd1e4824cc6a6b305d", i40);
        aPk = a25;
        fmVarArr[i40] = a25;
        int i41 = i40 + 1;
        C2491fm a26 = C4105zY.m41624a(HazardShield.class, "755e46377fcc63d059017afedd1010e0", i41);
        aPl = a26;
        fmVarArr[i41] = a26;
        int i42 = i41 + 1;
        C2491fm a27 = C4105zY.m41624a(HazardShield.class, "16e37124c2da57ae7e9978cf1b7fdd92", i42);
        aPm = a27;
        fmVarArr[i42] = a27;
        int i43 = i42 + 1;
        _m_methods = fmVarArr;
        C1634Xv.m11725a((Object[]) TaikodomObject._m_methods, (Object[]) _m_methods);
        ___iScriptClass = aUO.m18566a(HazardShield.class, azW.class, _m_fields, _m_methods);
    }

    /* renamed from: B */
    private void m28420B(Asset tCVar) {
        throw new C6039afL();
    }

    /* renamed from: C */
    private void m28421C(Asset tCVar) {
        throw new C6039afL();
    }

    /* renamed from: Mv */
    private Asset m28423Mv() {
        return ((HazardShieldType) getType()).mo21562My();
    }

    /* renamed from: Mw */
    private Asset m28424Mw() {
        return ((HazardShieldType) getType()).mo21561MA();
    }

    /* renamed from: TH */
    private void m28426TH() {
        switch (bFf().mo6893i(aPe)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, aPe, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, aPe, new Object[0]));
                break;
        }
        m28425TG();
    }

    /* renamed from: TP */
    private void m28431TP() {
        switch (bFf().mo6893i(aPl)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, aPl, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, aPl, new Object[0]));
                break;
        }
        m28430TO();
    }

    /* renamed from: Te */
    private AdapterLikedList m28433Te() {
        return (AdapterLikedList) bFf().mo5608dq().mo3214p(aOx);
    }

    /* renamed from: Yv */
    private C3892wO m28437Yv() {
        return ((HazardShieldType) getType()).mo21560LG();
    }

    /* renamed from: Yw */
    private C2686iZ m28438Yw() {
        return ((HazardShieldType) getType()).mo21563YA();
    }

    /* renamed from: Yx */
    private float m28439Yx() {
        return ((HazardShieldType) getType()).mo21565YE();
    }

    /* renamed from: Yy */
    private float m28440Yy() {
        return ((HazardShieldType) getType()).mo21566YG();
    }

    /* renamed from: a */
    private void m28442a(C6809auB.C1996a aVar) {
        bFf().mo5608dq().mo3197f(bid, aVar);
    }

    /* renamed from: a */
    private void m28443a(ShieldBlock aVar) {
        bFf().mo5608dq().mo3197f(hxZ, aVar);
    }

    /* renamed from: a */
    private void m28444a(InvulnerabilityExiter eVar) {
        bFf().mo5608dq().mo3197f(hyd, eVar);
    }

    /* renamed from: a */
    private void m28445a(InvulnerabilityEnterer fVar) {
        bFf().mo5608dq().mo3197f(hyc, fVar);
    }

    /* renamed from: a */
    private void m28446a(HullBlock gVar) {
        bFf().mo5608dq().mo3197f(hya, gVar);
    }

    /* renamed from: a */
    private void m28448a(AdapterLikedList zAVar) {
        bFf().mo5608dq().mo3197f(aOx, zAVar);
    }

    @C4034yP
    @C5566aOg
    @C0064Am(aul = "5138099b8f7c1a1a5787918862258fdd", aum = 0)
    @C2499fr
    private void acK() {
        throw new aWi(new aCE(this, bka, new Object[0]));
    }

    @C4034yP
    @C5566aOg
    @C0064Am(aul = "e1eeb7ae9178b34965d9bd03602b4302", aum = 0)
    @C2499fr
    private void acP() {
        throw new aWi(new aCE(this, bkd, new Object[0]));
    }

    private C6809auB.C1996a acy() {
        return (C6809auB.C1996a) bFf().mo5608dq().mo3214p(bid);
    }

    /* renamed from: ai */
    private void m28449ai(C3892wO wOVar) {
        bFf().mo5608dq().mo3197f(hye, wOVar);
    }

    /* renamed from: am */
    private void m28450am(Ship fAVar) {
        bFf().mo5608dq().mo3197f(hyb, fAVar);
    }

    private ShieldBlock cRT() {
        return (ShieldBlock) bFf().mo5608dq().mo3214p(hxZ);
    }

    private HullBlock cRU() {
        return (HullBlock) bFf().mo5608dq().mo3214p(hya);
    }

    private Ship cRV() {
        return (Ship) bFf().mo5608dq().mo3214p(hyb);
    }

    private InvulnerabilityEnterer cRW() {
        return (InvulnerabilityEnterer) bFf().mo5608dq().mo3214p(hyc);
    }

    private InvulnerabilityExiter cRX() {
        return (InvulnerabilityExiter) bFf().mo5608dq().mo3214p(hyd);
    }

    private C3892wO cRY() {
        return (C3892wO) bFf().mo5608dq().mo3214p(hye);
    }

    @C0064Am(aul = "5b71483b212329439101b5e4dc80f72a", aum = 0)
    @C5566aOg
    private void cRZ() {
        throw new aWi(new aCE(this, hyf, new Object[0]));
    }

    @C0064Am(aul = "2d456e5fdecc01b0fa21666fce08bcc6", aum = 0)
    @C5566aOg
    private void cSb() {
        throw new aWi(new aCE(this, hyg, new Object[0]));
    }

    @C0064Am(aul = "08c6061eb25f51592827525d772fd7b7", aum = 0)
    @C5566aOg
    private void cSd() {
        throw new aWi(new aCE(this, hyh, new Object[0]));
    }

    @C4034yP
    @C2499fr
    @ClientOnly
    private void cSg() {
        switch (bFf().mo6893i(hyi)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, hyi, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, hyi, new Object[0]));
                break;
        }
        cSf();
    }

    @C4034yP
    @C2499fr
    @ClientOnly
    private void cSi() {
        switch (bFf().mo6893i(hyj)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, hyj, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, hyj, new Object[0]));
                break;
        }
        cSh();
    }

    /* renamed from: cW */
    private void m28453cW(float f) {
        throw new C6039afL();
    }

    /* renamed from: cX */
    private void m28454cX(float f) {
        throw new C6039afL();
    }

    /* renamed from: m */
    private void m28458m(C2686iZ iZVar) {
        throw new C6039afL();
    }

    /* renamed from: x */
    private void m28460x(C3892wO wOVar) {
        throw new C6039afL();
    }

    /* renamed from: LG */
    public C3892wO mo17625LG() {
        switch (bFf().mo6893i(axb)) {
            case 0:
                return null;
            case 2:
                return (C3892wO) bFf().mo5606d(new aCE(this, axb, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, axb, new Object[0]));
                break;
        }
        return m28422LF();
    }

    /* renamed from: S */
    public void mo10S() {
        super.mo10S();
    }

    /* renamed from: T */
    public C6494any mo11T() {
        return ___iScriptClass;
    }

    /* renamed from: TJ */
    public AdapterLikedList<HazardShieldAdapter> mo17626TJ() {
        switch (bFf().mo6893i(aPf)) {
            case 0:
                return null;
            case 2:
                return (AdapterLikedList) bFf().mo5606d(new aCE(this, aPf, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, aPf, new Object[0]));
                break;
        }
        return m28427TI();
    }

    /* renamed from: TL */
    public void mo12905TL() {
        switch (bFf().mo6893i(aPj)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, aPj, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, aPj, new Object[0]));
                break;
        }
        m28428TK();
    }

    /* renamed from: TN */
    public void mo12906TN() {
        switch (bFf().mo6893i(aPk)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, aPk, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, aPk, new Object[0]));
                break;
        }
        m28429TM();
    }

    /* renamed from: TR */
    public void mo17627TR() {
        switch (bFf().mo6893i(aPm)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, aPm, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, aPm, new Object[0]));
                break;
        }
        m28432TQ();
    }

    /* renamed from: U */
    public C2491fm[] mo12U() {
        return _m_methods;
    }

    /* renamed from: W */
    public Object mo13W() {
        return new azW(this);
    }

    /* renamed from: YA */
    public C2686iZ<DamageType> mo17628YA() {
        switch (bFf().mo6893i(bbQ)) {
            case 0:
                return null;
            case 2:
                return (C2686iZ) bFf().mo5606d(new aCE(this, bbQ, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, bbQ, new Object[0]));
                break;
        }
        return m28441Yz();
    }

    /* renamed from: YE */
    public float mo17629YE() {
        switch (bFf().mo6893i(bbS)) {
            case 0:
                return 0.0f;
            case 2:
                return ((Float) bFf().mo5606d(new aCE(this, bbS, new Object[0]))).floatValue();
            case 3:
                bFf().mo5606d(new aCE(this, bbS, new Object[0]));
                break;
        }
        return m28435YD();
    }

    /* renamed from: YG */
    public float mo17630YG() {
        switch (bFf().mo6893i(bbU)) {
            case 0:
                return 0.0f;
            case 2:
                return ((Float) bFf().mo5606d(new aCE(this, bbU, new Object[0]))).floatValue();
            case 3:
                bFf().mo5606d(new aCE(this, bbU, new Object[0]));
                break;
        }
        return m28436YF();
    }

    /* renamed from: a */
    public Object mo14a(C0495Gr gr) {
        Object[] args = gr.getArgs();
        switch (gr.mo2417hq() - TaikodomObject._m_methodCount) {
            case 0:
                return m28422LF();
            case 1:
                cRZ();
                return null;
            case 2:
                cSb();
                return null;
            case 3:
                cSd();
                return null;
            case 4:
                cSf();
                return null;
            case 5:
                cSh();
                return null;
            case 6:
                return new Boolean(cSj());
            case 7:
                acK();
                return null;
            case 8:
                m28461zw();
                return null;
            case 9:
                acP();
                return null;
            case 10:
                m28457fg();
                return null;
            case 11:
                m28459qU();
                return null;
            case 12:
                return acY();
            case 13:
                m28456d((C6809auB.C1996a) args[0]);
                return null;
            case 14:
                return new Boolean(m28452b((C6809auB.C1996a) args[0]));
            case 15:
                return m28441Yz();
            case 16:
                return cSl();
            case 17:
                return new Float(m28435YD());
            case 18:
                return new Float(m28436YF());
            case 19:
                return cSn();
            case 20:
                m28451an((Ship) args[0]);
                return null;
            case 21:
                return m28427TI();
            case 22:
                m28425TG();
                return null;
            case 23:
                m28428TK();
                return null;
            case 24:
                m28429TM();
                return null;
            case 25:
                m28430TO();
                return null;
            case 26:
                m28432TQ();
                return null;
            default:
                return super.mo14a(gr);
        }
    }

    /* renamed from: a */
    public void mo15a(Collection collection, C0495Gr gr) {
        super.mo15a(collection, gr);
    }

    @C4034yP
    @C5566aOg
    @C2499fr
    public void abort() {
        switch (bFf().mo6893i(bkd)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, bkd, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, bkd, new Object[0]));
                break;
        }
        acP();
    }

    public C6809auB.C1996a acZ() {
        switch (bFf().mo6893i(bkk)) {
            case 0:
                return null;
            case 2:
                return (C6809auB.C1996a) bFf().mo5606d(new aCE(this, bkk, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, bkk, new Object[0]));
                break;
        }
        return acY();
    }

    @C4034yP
    @C5566aOg
    @C2499fr
    public void activate() {
        switch (bFf().mo6893i(bka)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, bka, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, bka, new Object[0]));
                break;
        }
        acK();
    }

    /* renamed from: ao */
    public void mo17634ao(Ship fAVar) {
        switch (bFf().mo6893i(hyn)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, hyn, new Object[]{fAVar}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, hyn, new Object[]{fAVar}));
                break;
        }
        m28451an(fAVar);
    }

    /* renamed from: c */
    public boolean mo17635c(C6809auB.C1996a aVar) {
        switch (bFf().mo6893i(bjZ)) {
            case 0:
                return false;
            case 2:
                return ((Boolean) bFf().mo5606d(new aCE(this, bjZ, new Object[]{aVar}))).booleanValue();
            case 3:
                bFf().mo5606d(new aCE(this, bjZ, new Object[]{aVar}));
                break;
        }
        return m28452b(aVar);
    }

    /* renamed from: c */
    public C5663aRz[] mo25c() {
        return _m_fields;
    }

    @C5566aOg
    public void cSa() {
        switch (bFf().mo6893i(hyf)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, hyf, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, hyf, new Object[0]));
                break;
        }
        cRZ();
    }

    @C5566aOg
    public void cSc() {
        switch (bFf().mo6893i(hyg)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, hyg, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, hyg, new Object[0]));
                break;
        }
        cSb();
    }

    @C5566aOg
    public void cSe() {
        switch (bFf().mo6893i(hyh)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, hyh, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, hyh, new Object[0]));
                break;
        }
        cSd();
    }

    public boolean cSk() {
        switch (bFf().mo6893i(hyk)) {
            case 0:
                return false;
            case 2:
                return ((Boolean) bFf().mo5606d(new aCE(this, hyk, new Object[0]))).booleanValue();
            case 3:
                bFf().mo5606d(new aCE(this, hyk, new Object[0]));
                break;
        }
        return cSj();
    }

    public C3892wO cSm() {
        switch (bFf().mo6893i(hyl)) {
            case 0:
                return null;
            case 2:
                return (C3892wO) bFf().mo5606d(new aCE(this, hyl, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, hyl, new Object[0]));
                break;
        }
        return cSl();
    }

    public Ship cSo() {
        switch (bFf().mo6893i(hym)) {
            case 0:
                return null;
            case 2:
                return (Ship) bFf().mo5606d(new aCE(this, hym, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, hym, new Object[0]));
                break;
        }
        return cSn();
    }

    public void dispose() {
        switch (bFf().mo6893i(_f_dispose_0020_0028_0029V)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, _f_dispose_0020_0028_0029V, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, _f_dispose_0020_0028_0029V, new Object[0]));
                break;
        }
        m28457fg();
    }

    /* renamed from: e */
    public void mo17643e(C6809auB.C1996a aVar) {
        switch (bFf().mo6893i(bkl)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, bkl, new Object[]{aVar}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, bkl, new Object[]{aVar}));
                break;
        }
        m28456d(aVar);
    }

    /* renamed from: qV */
    public void mo17644qV() {
        switch (bFf().mo6893i(f6167Lm)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f6167Lm, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f6167Lm, new Object[0]));
                break;
        }
        m28459qU();
    }

    /* renamed from: zx */
    public void mo17645zx() {
        switch (bFf().mo6893i(f6168Wp)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f6168Wp, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f6168Wp, new Object[0]));
                break;
        }
        m28461zw();
    }

    @C0064Am(aul = "eaf6d1cddefa58975e4be79449049e51", aum = 0)
    /* renamed from: LF */
    private C3892wO m28422LF() {
        return cRY();
    }

    /* renamed from: d */
    public void mo17642d(HazardShieldType rGVar) {
        super.mo967a((C2961mJ) rGVar);
        mo17643e(C6809auB.C1996a.DEACTIVE);
        m28426TH();
        InvulnerabilityEnterer fVar = (InvulnerabilityEnterer) bFf().mo6865M(InvulnerabilityEnterer.class);
        fVar.mo17652a(this, (aDJ) this);
        m28445a(fVar);
        InvulnerabilityExiter eVar = (InvulnerabilityExiter) bFf().mo6865M(InvulnerabilityExiter.class);
        eVar.mo17650a(this, (aDJ) this);
        m28444a(eVar);
        ShieldBlock aVar = (ShieldBlock) bFf().mo6865M(ShieldBlock.class);
        aVar.mo17646b(this);
        m28443a(aVar);
        HullBlock gVar = (HullBlock) bFf().mo6865M(HullBlock.class);
        gVar.mo17654b(this);
        m28446a(gVar);
    }

    @C4034yP
    @ClientOnly
    @C0064Am(aul = "38a3f44815b457d50a66b637a2cf4010", aum = 0)
    @C2499fr
    private void cSf() {
        if (this.fye != null) {
            this.fye.mo22375XB();
            this.fye.mo22374XA();
            this.fye = null;
        }
        this.fye = new C3738uP(cRV().cHg(), cRV().cHg());
        ILoaderTrail alg = C5916acs.getSingolton().getLoaderTrail();
        alg.mo4999as("data/shaders/fx_passes.pro");
        Material material = (Material) alg.getAsset("extrude_noise0_material");
        if (material == null) {
            this.fye.mo22375XB();
            this.fye = null;
            return;
        }
        this.fye.mo22378a(material);
        this.fye.mo22380c(new Color(0.0f, 0.0f, 0.0f, 0.0f));
        this.hky = new C2188c();
        C5916acs.getSingolton().alf().addTask("spawnInvulnerabilityFGXTask", this.hky, 50);
    }

    @C4034yP
    @ClientOnly
    @C0064Am(aul = "458b69bbe20b52197f9fad4e3a8b6551", aum = 0)
    @C2499fr
    private void cSh() {
        if (this.hky != null) {
            this.hky.cancel();
            this.hky = null;
        }
        if (this.fye != null) {
            this.hky = new C2189d();
            cRV().mo18261aa(m28424Mw());
            C5916acs.getSingolton().alf().addTask("spawnInvulnerabilityFGXTask", this.hky, 50);
        }
    }

    @C0064Am(aul = "2349f9669640818a77475e628a24d6e5", aum = 0)
    private boolean cSj() {
        return mo17635c(C6809auB.C1996a.ACTIVE);
    }

    @C0064Am(aul = "d568cb12be279575bfba010dbf3da690", aum = 0)
    /* renamed from: zw */
    private void m28461zw() {
        if (mo17635c(C6809auB.C1996a.WARMUP)) {
            abort();
        } else if (mo17635c(C6809auB.C1996a.ACTIVE)) {
            cSa();
        }
    }

    @C0064Am(aul = "af981eea2d67a39d6594cddf842930b9", aum = 0)
    /* renamed from: fg */
    private void m28457fg() {
        super.dispose();
        m28450am((Ship) null);
        if (cRT() != null) {
            cRT().dispose();
            m28443a((ShieldBlock) null);
        }
        if (cRU() != null) {
            cRU().dispose();
            m28446a((HullBlock) null);
        }
        if (cRW() != null) {
            cRW().dispose();
            m28445a((InvulnerabilityEnterer) null);
        }
        if (cRX() != null) {
            cRX().dispose();
            m28444a((InvulnerabilityExiter) null);
        }
    }

    @C0064Am(aul = "3d227eb3c757d338e5721f2da4a3fe31", aum = 0)
    /* renamed from: qU */
    private void m28459qU() {
        push();
    }

    @C0064Am(aul = "0dc2b342bf949c173f605cf11becf283", aum = 0)
    private C6809auB.C1996a acY() {
        return acy();
    }

    @C0064Am(aul = "c24ab6e98adb46743d2304e0446561e8", aum = 0)
    /* renamed from: d */
    private void m28456d(C6809auB.C1996a aVar) {
        m28442a(aVar);
    }

    @C0064Am(aul = "b55be488adaad66948df46dbc9b09eda", aum = 0)
    /* renamed from: b */
    private boolean m28452b(C6809auB.C1996a aVar) {
        return acy() == aVar;
    }

    @C0064Am(aul = "d9b24cc26557303db2c78a0ed4c21c7f", aum = 0)
    /* renamed from: Yz */
    private C2686iZ<DamageType> m28441Yz() {
        return m28438Yw();
    }

    @C0064Am(aul = "dc1ab31248e933a29a88295e19c5eb37", aum = 0)
    private C3892wO cSl() {
        return m28437Yv();
    }

    @C0064Am(aul = "8a10eacada1cd06f3221955018d37627", aum = 0)
    /* renamed from: YD */
    private float m28435YD() {
        return m28439Yx();
    }

    @C0064Am(aul = "bb1aca9f9f50c73f36b7041c9446ca7a", aum = 0)
    /* renamed from: YF */
    private float m28436YF() {
        return m28440Yy();
    }

    @C0064Am(aul = "d1fb469e256943308d51d81be1c64ca8", aum = 0)
    private Ship cSn() {
        return cRV();
    }

    @C0064Am(aul = "d3e9ccb0095cf59ee720b14d387f4f28", aum = 0)
    /* renamed from: an */
    private void m28451an(Ship fAVar) {
        m28450am(fAVar);
    }

    @C0064Am(aul = "7c1d7a0269d12d2d290026b32285077c", aum = 0)
    /* renamed from: TI */
    private AdapterLikedList<HazardShieldAdapter> m28427TI() {
        if (m28433Te() == null) {
            m28426TH();
        }
        return m28433Te();
    }

    @C0064Am(aul = "541efc5311bb8e74c64eb08b2c6d4059", aum = 0)
    /* renamed from: TG */
    private void m28425TG() {
        if (m28433Te() == null) {
            AdapterLikedList zAVar = (AdapterLikedList) bFf().mo6865M(AdapterLikedList.class);
            BaseReconModeAdapter bVar = (BaseReconModeAdapter) bFf().mo6865M(BaseReconModeAdapter.class);
            bVar.mo17647b(this);
            zAVar.mo23260c(bVar);
            m28448a(zAVar);
            m28433Te().mo23259b((C6872avM) this);
            m28431TP();
        }
    }

    @C0064Am(aul = "4cd91aee477295b4e2b6796052e0dd50", aum = 0)
    /* renamed from: TK */
    private void m28428TK() {
        m28431TP();
    }

    @C0064Am(aul = "69f3518c539ea4cd1e4824cc6a6b305d", aum = 0)
    /* renamed from: TM */
    private void m28429TM() {
        m28431TP();
    }

    @C0064Am(aul = "755e46377fcc63d059017afedd1010e0", aum = 0)
    /* renamed from: TO */
    private void m28430TO() {
        m28449ai(((HazardShieldAdapter) m28433Te().ase()).mo7993LG());
    }

    @C0064Am(aul = "16e37124c2da57ae7e9978cf1b7fdd92", aum = 0)
    /* renamed from: TQ */
    private void m28432TQ() {
        if (m28433Te() == null) {
            mo8358lY(String.valueOf(bFY()) + " with NULL adapterList");
            return;
        }
        m28433Te().mo23259b((C6872avM) this);
        m28431TP();
    }

    @C6485anp
    @C5511aMd
    /* renamed from: a.ca$b */
    /* compiled from: a */
    public class BaseReconModeAdapter extends HazardShieldAdapter implements C1616Xf {
        public static final int _m_fieldCount = 0;
        public static final C5663aRz[] _m_fields = null;
        public static final int _m_methodCount = 0;
        public static final C2491fm[] _m_methods = null;
        /* renamed from: aT */
        public static final C5663aRz f6173aT = null;
        public static final C2491fm axb = null;
        public static final long serialVersionUID = 0;
        public static C6494any ___iScriptClass;
        @C0064Am(aul = "9676201a3691aa0869b6cb0d1a350e37", aum = 0)

        /* renamed from: pZ */
        static /* synthetic */ HazardShield f6174pZ;

        static {
            m28499V();
        }

        public BaseReconModeAdapter() {
            super((C5540aNg) null);
            super.mo10S();
        }

        public BaseReconModeAdapter(C5540aNg ang) {
            super(ang);
        }

        public BaseReconModeAdapter(HazardShield caVar) {
            super((C5540aNg) null);
            super._m_script_init(caVar);
        }

        /* renamed from: V */
        static void m28499V() {
            serialVersionUID = (long) 1;
            _m_fieldCount = HazardShieldAdapter._m_fieldCount + 1;
            _m_methodCount = HazardShieldAdapter._m_methodCount + 1;
            int i = HazardShieldAdapter._m_fieldCount;
            C5663aRz[] arzArr = new C5663aRz[(i + 1)];
            C5663aRz b = C5640aRc.m17844b(BaseReconModeAdapter.class, "9676201a3691aa0869b6cb0d1a350e37", i);
            f6173aT = b;
            arzArr[i] = b;
            int i2 = i + 1;
            _m_fields = arzArr;
            C1634Xv.m11725a((Object[]) HazardShieldAdapter._m_fields, (Object[]) _m_fields);
            int i3 = HazardShieldAdapter._m_methodCount;
            C2491fm[] fmVarArr = new C2491fm[(i3 + 1)];
            C2491fm a = C4105zY.m41624a(BaseReconModeAdapter.class, "946dd61567c1c6b72b24bf1cd04f4483", i3);
            axb = a;
            fmVarArr[i3] = a;
            int i4 = i3 + 1;
            _m_methods = fmVarArr;
            C1634Xv.m11725a((Object[]) HazardShieldAdapter._m_methods, (Object[]) _m_methods);
            ___iScriptClass = aUO.m18566a(BaseReconModeAdapter.class, C6102agW.class, _m_fields, _m_methods);
        }

        /* renamed from: a */
        private void m28500a(HazardShield caVar) {
            bFf().mo5608dq().mo3197f(f6173aT, caVar);
        }

        /* renamed from: hf */
        private HazardShield m28501hf() {
            return (HazardShield) bFf().mo5608dq().mo3214p(f6173aT);
        }

        /* renamed from: LG */
        public C3892wO mo7993LG() {
            switch (bFf().mo6893i(axb)) {
                case 0:
                    return null;
                case 2:
                    return (C3892wO) bFf().mo5606d(new aCE(this, axb, new Object[0]));
                case 3:
                    bFf().mo5606d(new aCE(this, axb, new Object[0]));
                    break;
            }
            return m28498LF();
        }

        /* renamed from: S */
        public void mo10S() {
            super.mo10S();
        }

        /* renamed from: T */
        public C6494any mo11T() {
            return ___iScriptClass;
        }

        /* renamed from: U */
        public C2491fm[] mo12U() {
            return _m_methods;
        }

        /* renamed from: W */
        public Object mo13W() {
            return new C6102agW(this);
        }

        /* renamed from: a */
        public Object mo14a(C0495Gr gr) {
            gr.getArgs();
            switch (gr.mo2417hq() - HazardShieldAdapter._m_methodCount) {
                case 0:
                    return m28498LF();
                default:
                    return super.mo14a(gr);
            }
        }

        /* renamed from: a */
        public void mo15a(Collection collection, C0495Gr gr) {
            super.mo15a(collection, gr);
        }

        /* renamed from: c */
        public C5663aRz[] mo25c() {
            return _m_fields;
        }

        /* renamed from: b */
        public void mo17647b(HazardShield caVar) {
            m28500a(caVar);
            super.mo10S();
        }

        @C0064Am(aul = "946dd61567c1c6b72b24bf1cd04f4483", aum = 0)
        /* renamed from: LF */
        private C3892wO m28498LF() {
            return m28501hf().cSm();
        }
    }

    @C6485anp
    @C5511aMd
    /* renamed from: a.ca$g */
    /* compiled from: a */
    public class HullBlock extends HullAdapter implements C1616Xf {
        public static final int _m_fieldCount = 0;
        public static final C5663aRz[] _m_fields = null;
        public static final int _m_methodCount = 0;
        public static final C2491fm[] _m_methods = null;
        /* renamed from: aT */
        public static final C5663aRz f6183aT = null;
        /* renamed from: qa */
        public static final C2491fm f6185qa = null;
        public static final long serialVersionUID = 0;
        public static C6494any ___iScriptClass;
        @C0064Am(aul = "bc9b01df3566b36da539180373bcafff", aum = 0)

        /* renamed from: pZ */
        static /* synthetic */ HazardShield f6184pZ;

        static {
            m28537V();
        }

        public HullBlock() {
            super((C5540aNg) null);
            super.mo10S();
        }

        public HullBlock(C5540aNg ang) {
            super(ang);
        }

        public HullBlock(HazardShield caVar) {
            super((C5540aNg) null);
            super._m_script_init(caVar);
        }

        /* renamed from: V */
        static void m28537V() {
            serialVersionUID = (long) 1;
            _m_fieldCount = HullAdapter._m_fieldCount + 1;
            _m_methodCount = HullAdapter._m_methodCount + 1;
            int i = HullAdapter._m_fieldCount;
            C5663aRz[] arzArr = new C5663aRz[(i + 1)];
            C5663aRz b = C5640aRc.m17844b(HullBlock.class, "bc9b01df3566b36da539180373bcafff", i);
            f6183aT = b;
            arzArr[i] = b;
            int i2 = i + 1;
            _m_fields = arzArr;
            C1634Xv.m11725a((Object[]) HullAdapter._m_fields, (Object[]) _m_fields);
            int i3 = HullAdapter._m_methodCount;
            C2491fm[] fmVarArr = new C2491fm[(i3 + 1)];
            C2491fm a = C4105zY.m41624a(HullBlock.class, "47a16757238b5a1d24924bd234bcda55", i3);
            f6185qa = a;
            fmVarArr[i3] = a;
            int i4 = i3 + 1;
            _m_methods = fmVarArr;
            C1634Xv.m11725a((Object[]) HullAdapter._m_methods, (Object[]) _m_methods);
            ___iScriptClass = aUO.m18566a(HullBlock.class, C3520rt.class, _m_fields, _m_methods);
        }

        /* renamed from: a */
        private void m28539a(HazardShield caVar) {
            bFf().mo5608dq().mo3197f(f6183aT, caVar);
        }

        /* renamed from: hf */
        private HazardShield m28540hf() {
            return (HazardShield) bFf().mo5608dq().mo3214p(f6183aT);
        }

        /* renamed from: S */
        public void mo10S() {
            super.mo10S();
        }

        /* renamed from: T */
        public C6494any mo11T() {
            return ___iScriptClass;
        }

        /* renamed from: U */
        public C2491fm[] mo12U() {
            return _m_methods;
        }

        /* renamed from: W */
        public Object mo13W() {
            return new C3520rt(this);
        }

        /* renamed from: a */
        public Object mo14a(C0495Gr gr) {
            Object[] args = gr.getArgs();
            switch (gr.mo2417hq() - HullAdapter._m_methodCount) {
                case 0:
                    return new Float(m28538a((DamageType) args[0], ((Float) args[1]).floatValue()));
                default:
                    return super.mo14a(gr);
            }
        }

        /* renamed from: a */
        public void mo15a(Collection collection, C0495Gr gr) {
            super.mo15a(collection, gr);
        }

        /* renamed from: b */
        public float mo9877b(DamageType fr, float f) {
            switch (bFf().mo6893i(f6185qa)) {
                case 0:
                    return 0.0f;
                case 2:
                    return ((Float) bFf().mo5606d(new aCE(this, f6185qa, new Object[]{fr, new Float(f)}))).floatValue();
                case 3:
                    bFf().mo5606d(new aCE(this, f6185qa, new Object[]{fr, new Float(f)}));
                    break;
            }
            return m28538a(fr, f);
        }

        /* renamed from: c */
        public C5663aRz[] mo25c() {
            return _m_fields;
        }

        /* renamed from: b */
        public void mo17654b(HazardShield caVar) {
            m28539a(caVar);
            super.mo10S();
        }

        @C0064Am(aul = "47a16757238b5a1d24924bd234bcda55", aum = 0)
        /* renamed from: a */
        private float m28538a(DamageType fr, float f) {
            if (m28540hf().mo17628YA().contains(fr)) {
                return f;
            }
            return super.mo9877b(fr, f);
        }
    }

    @C6485anp
    @C5511aMd
    /* renamed from: a.ca$a */
    public class ShieldBlock extends ShieldAdapter implements C1616Xf {
        public static final int _m_fieldCount = 0;
        public static final C5663aRz[] _m_fields = null;
        public static final int _m_methodCount = 0;
        public static final C2491fm[] _m_methods = null;
        /* renamed from: aT */
        public static final C5663aRz f6169aT = null;
        /* renamed from: qa */
        public static final C2491fm f6171qa = null;
        /* renamed from: qb */
        public static final C2491fm f6172qb = null;
        public static final long serialVersionUID = 0;
        public static C6494any ___iScriptClass;
        @C0064Am(aul = "ccce454763e3c3787beb87d03712989b", aum = 0)

        /* renamed from: pZ */
        static /* synthetic */ HazardShield f6170pZ;

        static {
            m28483V();
        }

        public ShieldBlock() {
            super((C5540aNg) null);
            super.mo10S();
        }

        public ShieldBlock(C5540aNg ang) {
            super(ang);
        }

        public ShieldBlock(HazardShield caVar) {
            super((C5540aNg) null);
            super._m_script_init(caVar);
        }

        /* renamed from: V */
        static void m28483V() {
            serialVersionUID = (long) 1;
            _m_fieldCount = ShieldAdapter._m_fieldCount + 1;
            _m_methodCount = ShieldAdapter._m_methodCount + 2;
            int i = ShieldAdapter._m_fieldCount;
            C5663aRz[] arzArr = new C5663aRz[(i + 1)];
            C5663aRz b = C5640aRc.m17844b(ShieldBlock.class, "ccce454763e3c3787beb87d03712989b", i);
            f6169aT = b;
            arzArr[i] = b;
            int i2 = i + 1;
            _m_fields = arzArr;
            C1634Xv.m11725a((Object[]) ShieldAdapter._m_fields, (Object[]) _m_fields);
            int i3 = ShieldAdapter._m_methodCount;
            C2491fm[] fmVarArr = new C2491fm[(i3 + 2)];
            C2491fm a = C4105zY.m41624a(ShieldBlock.class, "cafc0856466bc8f72202aa547589672d", i3);
            f6171qa = a;
            fmVarArr[i3] = a;
            int i4 = i3 + 1;
            C2491fm a2 = C4105zY.m41624a(ShieldBlock.class, "3cec15cbaffa082b09f0cb27568ca97c", i4);
            f6172qb = a2;
            fmVarArr[i4] = a2;
            int i5 = i4 + 1;
            _m_methods = fmVarArr;
            C1634Xv.m11725a((Object[]) ShieldAdapter._m_methods, (Object[]) _m_methods);
            ___iScriptClass = aUO.m18566a(ShieldBlock.class, C5787aaT.class, _m_fields, _m_methods);
        }

        /* renamed from: a */
        private void m28485a(HazardShield caVar) {
            bFf().mo5608dq().mo3197f(f6169aT, caVar);
        }

        /* renamed from: hf */
        private HazardShield m28486hf() {
            return (HazardShield) bFf().mo5608dq().mo3214p(f6169aT);
        }

        /* renamed from: S */
        public void mo10S() {
            super.mo10S();
        }

        /* renamed from: T */
        public C6494any mo11T() {
            return ___iScriptClass;
        }

        /* renamed from: U */
        public C2491fm[] mo12U() {
            return _m_methods;
        }

        /* renamed from: W */
        public Object mo13W() {
            return new C5787aaT(this);
        }

        /* renamed from: a */
        public Object mo14a(C0495Gr gr) {
            Object[] args = gr.getArgs();
            switch (gr.mo2417hq() - ShieldAdapter._m_methodCount) {
                case 0:
                    return new Float(m28484a((DamageType) args[0], ((Float) args[1]).floatValue()));
                case 1:
                    return new Float(m28487hg());
                default:
                    return super.mo14a(gr);
            }
        }

        /* renamed from: a */
        public void mo15a(Collection collection, C0495Gr gr) {
            super.mo15a(collection, gr);
        }

        /* renamed from: b */
        public float mo3759b(DamageType fr, float f) {
            switch (bFf().mo6893i(f6171qa)) {
                case 0:
                    return 0.0f;
                case 2:
                    return ((Float) bFf().mo5606d(new aCE(this, f6171qa, new Object[]{fr, new Float(f)}))).floatValue();
                case 3:
                    bFf().mo5606d(new aCE(this, f6171qa, new Object[]{fr, new Float(f)}));
                    break;
            }
            return m28484a(fr, f);
        }

        /* renamed from: c */
        public C5663aRz[] mo25c() {
            return _m_fields;
        }

        /* renamed from: hh */
        public float mo3761hh() {
            switch (bFf().mo6893i(f6172qb)) {
                case 0:
                    return 0.0f;
                case 2:
                    return ((Float) bFf().mo5606d(new aCE(this, f6172qb, new Object[0]))).floatValue();
                case 3:
                    bFf().mo5606d(new aCE(this, f6172qb, new Object[0]));
                    break;
            }
            return m28487hg();
        }

        /* renamed from: b */
        public void mo17646b(HazardShield caVar) {
            m28485a(caVar);
            super.mo10S();
        }

        @C0064Am(aul = "cafc0856466bc8f72202aa547589672d", aum = 0)
        /* renamed from: a */
        private float m28484a(DamageType fr, float f) {
            if (m28486hf().mo17628YA().contains(fr)) {
                return f;
            }
            return super.mo3759b(fr, f);
        }

        @C0064Am(aul = "3cec15cbaffa082b09f0cb27568ca97c", aum = 0)
        /* renamed from: hg */
        private float m28487hg() {
            return m28486hf().mo17625LG().mo22753v(((ShieldAdapter) mo16070IE()).mo3761hh(), super.mo3761hh());
        }
    }

    @C6485anp
    @C5511aMd
            /* renamed from: a.ca$f */
            /* compiled from: a */
    class InvulnerabilityEnterer extends TaskletImpl implements C1616Xf {

        /* renamed from: JD */
        public static final C2491fm f6180JD = null;
        public static final int _m_fieldCount = 0;
        public static final C5663aRz[] _m_fields = null;
        public static final int _m_methodCount = 0;
        public static final C2491fm[] _m_methods = null;
        /* renamed from: aT */
        public static final C5663aRz f6181aT = null;
        public static final C2491fm fns = null;
        public static final long serialVersionUID = 0;
        public static C6494any ___iScriptClass;
        @C0064Am(aul = "0565fbe1f5d85616231f1497d2ca0a1e", aum = 0)

        /* renamed from: pZ */
        static /* synthetic */ HazardShield f6182pZ;

        static {
            m28524V();
        }

        public InvulnerabilityEnterer() {
            super((C5540aNg) null);
            super.mo10S();
        }

        public InvulnerabilityEnterer(C5540aNg ang) {
            super(ang);
        }

        public InvulnerabilityEnterer(HazardShield caVar, aDJ adj) {
            super((C5540aNg) null);
            super._m_script_init(caVar, adj);
        }

        /* renamed from: V */
        static void m28524V() {
            serialVersionUID = (long) 1;
            _m_fieldCount = TaskletImpl._m_fieldCount + 1;
            _m_methodCount = TaskletImpl._m_methodCount + 2;
            int i = TaskletImpl._m_fieldCount;
            C5663aRz[] arzArr = new C5663aRz[(i + 1)];
            C5663aRz b = C5640aRc.m17844b(InvulnerabilityEnterer.class, "0565fbe1f5d85616231f1497d2ca0a1e", i);
            f6181aT = b;
            arzArr[i] = b;
            int i2 = i + 1;
            _m_fields = arzArr;
            C1634Xv.m11725a((Object[]) TaskletImpl._m_fields, (Object[]) _m_fields);
            int i3 = TaskletImpl._m_methodCount;
            C2491fm[] fmVarArr = new C2491fm[(i3 + 2)];
            C2491fm a = C4105zY.m41624a(InvulnerabilityEnterer.class, "6681979301efdcfe4988d9e1fb15b539", i3);
            f6180JD = a;
            fmVarArr[i3] = a;
            int i4 = i3 + 1;
            C2491fm a2 = C4105zY.m41624a(InvulnerabilityEnterer.class, "622abb2e7ee70ba3e1f0e3ee677b970d", i4);
            fns = a2;
            fmVarArr[i4] = a2;
            int i5 = i4 + 1;
            _m_methods = fmVarArr;
            C1634Xv.m11725a((Object[]) TaskletImpl._m_methods, (Object[]) _m_methods);
            ___iScriptClass = aUO.m18566a(InvulnerabilityEnterer.class, C5662aRy.class, _m_fields, _m_methods);
        }

        /* renamed from: a */
        private void m28525a(HazardShield caVar) {
            bFf().mo5608dq().mo3197f(f6181aT, caVar);
        }

        /* renamed from: hf */
        private HazardShield m28526hf() {
            return (HazardShield) bFf().mo5608dq().mo3214p(f6181aT);
        }

        /* renamed from: S */
        public void mo10S() {
            super.mo10S();
        }

        /* renamed from: T */
        public C6494any mo11T() {
            return ___iScriptClass;
        }

        /* renamed from: U */
        public C2491fm[] mo12U() {
            return _m_methods;
        }

        /* renamed from: W */
        public Object mo13W() {
            return new C5662aRy(this);
        }

        /* renamed from: a */
        public Object mo14a(C0495Gr gr) {
            gr.getArgs();
            switch (gr.mo2417hq() - TaskletImpl._m_methodCount) {
                case 0:
                    m28527pi();
                    return null;
                case 1:
                    bUg();
                    return null;
                default:
                    return super.mo14a(gr);
            }
        }

        /* renamed from: a */
        public void mo15a(Collection collection, C0495Gr gr) {
            super.mo15a(collection, gr);
        }

        /* access modifiers changed from: package-private */
        public void bUh() {
            switch (bFf().mo6893i(fns)) {
                case 0:
                    return;
                case 2:
                    bFf().mo5606d(new aCE(this, fns, new Object[0]));
                    return;
                case 3:
                    bFf().mo5606d(new aCE(this, fns, new Object[0]));
                    break;
            }
            bUg();
        }

        /* renamed from: c */
        public C5663aRz[] mo25c() {
            return _m_fields;
        }

        /* access modifiers changed from: protected */
        /* renamed from: pj */
        public void mo667pj() {
            switch (bFf().mo6893i(f6180JD)) {
                case 0:
                    return;
                case 2:
                    bFf().mo5606d(new aCE(this, f6180JD, new Object[0]));
                    return;
                case 3:
                    bFf().mo5606d(new aCE(this, f6180JD, new Object[0]));
                    break;
            }
            m28527pi();
        }

        /* renamed from: a */
        public void mo17652a(HazardShield caVar, aDJ adj) {
            m28525a(caVar);
            super.mo4706l(adj);
        }

        @C0064Am(aul = "6681979301efdcfe4988d9e1fb15b539", aum = 0)
        /* renamed from: pi */
        private void m28527pi() {
            m28526hf().cSe();
        }

        @C0064Am(aul = "622abb2e7ee70ba3e1f0e3ee677b970d", aum = 0)
        private void bUg() {
            if (m28526hf().mo17629YE() > 0.0f) {
                mo4702d(m28526hf().mo17629YE(), 1);
            } else {
                m28526hf().cSe();
            }
        }
    }

    @C6485anp
    @C5511aMd
    /* renamed from: a.ca$e */
    /* compiled from: a */
    public class InvulnerabilityExiter extends TaskletImpl implements C1616Xf {

        /* renamed from: JD */
        public static final C2491fm f6177JD = null;
        public static final int _m_fieldCount = 0;
        public static final C5663aRz[] _m_fields = null;
        public static final int _m_methodCount = 0;
        public static final C2491fm[] _m_methods = null;
        /* renamed from: aT */
        public static final C5663aRz f6178aT = null;
        public static final C2491fm dvf = null;
        public static final long serialVersionUID = 0;
        public static C6494any ___iScriptClass;
        @C0064Am(aul = "aa86ddd3f5d6fec0666735a2c656c56a", aum = 0)

        /* renamed from: pZ */
        static /* synthetic */ HazardShield f6179pZ;

        static {
            m28511V();
        }

        public InvulnerabilityExiter() {
            super((C5540aNg) null);
            super.mo10S();
        }

        public InvulnerabilityExiter(C5540aNg ang) {
            super(ang);
        }

        public InvulnerabilityExiter(HazardShield caVar, aDJ adj) {
            super((C5540aNg) null);
            super._m_script_init(caVar, adj);
        }

        /* renamed from: V */
        static void m28511V() {
            serialVersionUID = (long) 1;
            _m_fieldCount = TaskletImpl._m_fieldCount + 1;
            _m_methodCount = TaskletImpl._m_methodCount + 2;
            int i = TaskletImpl._m_fieldCount;
            C5663aRz[] arzArr = new C5663aRz[(i + 1)];
            C5663aRz b = C5640aRc.m17844b(InvulnerabilityExiter.class, "aa86ddd3f5d6fec0666735a2c656c56a", i);
            f6178aT = b;
            arzArr[i] = b;
            int i2 = i + 1;
            _m_fields = arzArr;
            C1634Xv.m11725a((Object[]) TaskletImpl._m_fields, (Object[]) _m_fields);
            int i3 = TaskletImpl._m_methodCount;
            C2491fm[] fmVarArr = new C2491fm[(i3 + 2)];
            C2491fm a = C4105zY.m41624a(InvulnerabilityExiter.class, "71cb607c47d3429f3a08d52f7d92b38d", i3);
            f6177JD = a;
            fmVarArr[i3] = a;
            int i4 = i3 + 1;
            C2491fm a2 = C4105zY.m41624a(InvulnerabilityExiter.class, "05d0cfc10ec3d6a07e0cb78ef39bc679", i4);
            dvf = a2;
            fmVarArr[i4] = a2;
            int i5 = i4 + 1;
            _m_methods = fmVarArr;
            C1634Xv.m11725a((Object[]) TaskletImpl._m_methods, (Object[]) _m_methods);
            ___iScriptClass = aUO.m18566a(InvulnerabilityExiter.class, C3673ta.class, _m_fields, _m_methods);
        }

        /* renamed from: a */
        private void m28512a(HazardShield caVar) {
            bFf().mo5608dq().mo3197f(f6178aT, caVar);
        }

        /* renamed from: hf */
        private HazardShield m28513hf() {
            return (HazardShield) bFf().mo5608dq().mo3214p(f6178aT);
        }

        /* renamed from: S */
        public void mo10S() {
            super.mo10S();
        }

        /* renamed from: T */
        public C6494any mo11T() {
            return ___iScriptClass;
        }

        /* renamed from: U */
        public C2491fm[] mo12U() {
            return _m_methods;
        }

        /* renamed from: W */
        public Object mo13W() {
            return new C3673ta(this);
        }

        /* renamed from: a */
        public Object mo14a(C0495Gr gr) {
            gr.getArgs();
            switch (gr.mo2417hq() - TaskletImpl._m_methodCount) {
                case 0:
                    m28514pi();
                    return null;
                case 1:
                    bfJ();
                    return null;
                default:
                    return super.mo14a(gr);
            }
        }

        /* renamed from: a */
        public void mo15a(Collection collection, C0495Gr gr) {
            super.mo15a(collection, gr);
        }

        /* access modifiers changed from: package-private */
        public void bfK() {
            switch (bFf().mo6893i(dvf)) {
                case 0:
                    return;
                case 2:
                    bFf().mo5606d(new aCE(this, dvf, new Object[0]));
                    return;
                case 3:
                    bFf().mo5606d(new aCE(this, dvf, new Object[0]));
                    break;
            }
            bfJ();
        }

        /* renamed from: c */
        public C5663aRz[] mo25c() {
            return _m_fields;
        }

        /* access modifiers changed from: protected */
        /* renamed from: pj */
        public void mo667pj() {
            switch (bFf().mo6893i(f6177JD)) {
                case 0:
                    return;
                case 2:
                    bFf().mo5606d(new aCE(this, f6177JD, new Object[0]));
                    return;
                case 3:
                    bFf().mo5606d(new aCE(this, f6177JD, new Object[0]));
                    break;
            }
            m28514pi();
        }

        /* renamed from: a */
        public void mo17650a(HazardShield caVar, aDJ adj) {
            m28512a(caVar);
            super.mo4706l(adj);
        }

        @C0064Am(aul = "71cb607c47d3429f3a08d52f7d92b38d", aum = 0)
        /* renamed from: pi */
        private void m28514pi() {
            m28513hf().cSc();
        }

        @C0064Am(aul = "05d0cfc10ec3d6a07e0cb78ef39bc679", aum = 0)
        private void bfJ() {
            if (m28513hf().mo17630YG() > 0.0f) {
                mo4702d(m28513hf().mo17630YG(), 1);
                m28513hf().mo17643e(C6809auB.C1996a.COOLDOWN);
                return;
            }
            m28513hf().cSc();
        }
    }

    /* renamed from: a.ca$c */
    /* compiled from: a */
    class C2188c extends WrapRunnable {
        private float currentTime = 0.0f;

        C2188c() {
        }

        public void run() {
            this.currentTime += 0.05f;
            if (this.currentTime > 1.0f) {
                this.currentTime = 1.0f;
                cancel();
            }
            if (HazardShield.this.fye != null) {
                HazardShield.this.fye.mo22380c(new Color(this.currentTime, this.currentTime, this.currentTime, this.currentTime));
            }
        }
    }

    /* renamed from: a.ca$d */
    /* compiled from: a */
    class C2189d extends WrapRunnable {
        private float currentTime = 1.0f;

        C2189d() {
        }

        public void run() {
            this.currentTime -= 0.05f;
            if (this.currentTime < 0.0f) {
                this.currentTime = 0.0f;
                cancel();
            }
            if (HazardShield.this.fye != null) {
                HazardShield.this.fye.mo22380c(new Color(this.currentTime, this.currentTime, this.currentTime, this.currentTime));
            }
        }

        public void cancel() {
            super.cancel();
            if (HazardShield.this.fye != null) {
                HazardShield.this.fye.mo22375XB();
                HazardShield.this.fye.mo22374XA();
                HazardShield.this.fye = null;
            }
        }
    }
}
