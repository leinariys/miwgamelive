package game.script.ship.hazardshield;

import game.network.message.externalizable.aCE;
import game.network.message.serializable.C2686iZ;
import game.script.damage.DamageType;
import game.script.resource.Asset;
import game.script.template.BaseTaikodomContent;
import logic.baa.*;
import logic.data.mbean.C6820auM;
import logic.res.code.C5640aRc;
import logic.res.code.C5663aRz;
import logic.res.code.C6494any;
import logic.res.html.C2491fm;
import p001a.*;

import java.util.Collection;
import java.util.Collections;
import java.util.Set;

@C0566Hp
@C5511aMd
@C6485anp
/* renamed from: a.rG */
/* compiled from: a */
public class HazardShieldType extends BaseTaikodomContent implements C1616Xf {
    public static final int _m_fieldCount = 0;
    public static final C5663aRz[] _m_fields = null;
    public static final int _m_methodCount = 0;
    public static final C2491fm[] _m_methods = null;
    public static final C5663aRz aAe = null;
    public static final C5663aRz aAg = null;
    public static final C2491fm aAi = null;
    public static final C2491fm aAj = null;
    public static final C2491fm aAk = null;
    public static final C2491fm aAl = null;
    public static final C5663aRz atQ = null;
    public static final C5663aRz atT = null;
    public static final C2491fm axb = null;
    public static final C5663aRz bbI = null;
    public static final C5663aRz bbK = null;
    public static final C2491fm bbN = null;
    public static final C2491fm bbO = null;
    public static final C2491fm bbP = null;
    public static final C2491fm bbQ = null;
    public static final C2491fm bbR = null;
    public static final C2491fm bbS = null;
    public static final C2491fm bbT = null;
    public static final C2491fm bbU = null;
    public static final C2491fm bbV = null;
    public static final C2491fm bbW = null;
    /* renamed from: dN */
    public static final C2491fm f8975dN = null;
    public static final long serialVersionUID = 0;
    public static C6494any ___iScriptClass;
    @C0064Am(aul = "fb14eaefa814fbebc3be0cb3edb43828", aum = 4)
    private static Asset aAd;
    @C0064Am(aul = "3210178b90c2c5a32e7fc72acb807547", aum = 5)
    private static Asset aAf;
    @C0064Am(aul = "0bd84fcaa6e27fff66e26a498a301b74", aum = 0)
    private static C3892wO bbH;
    @C0064Am(aul = "443026e338dc1b3e39c53c3a0fa9355d", aum = 1)
    private static C2686iZ<DamageType> bbJ;
    @C0064Am(aul = "756c430d1b7d44aaadc5c7b45ada849f", aum = 2)
    private static float bbL;
    @C0064Am(aul = "f6b6a8ef3f822342644e2432b9f83038", aum = 3)
    private static float bbM;

    static {
        m37979V();
    }

    public HazardShieldType() {
        super((C5540aNg) null);
        super.mo10S();
    }

    public HazardShieldType(C5540aNg ang) {
        super(ang);
    }

    /* renamed from: V */
    static void m37979V() {
        serialVersionUID = (long) 1;
        _m_fieldCount = BaseTaikodomContent._m_fieldCount + 6;
        _m_methodCount = BaseTaikodomContent._m_methodCount + 16;
        int i = BaseTaikodomContent._m_fieldCount;
        C5663aRz[] arzArr = new C5663aRz[(i + 6)];
        C5663aRz b = C5640aRc.m17844b(HazardShieldType.class, "0bd84fcaa6e27fff66e26a498a301b74", i);
        bbI = b;
        arzArr[i] = b;
        int i2 = i + 1;
        C5663aRz b2 = C5640aRc.m17844b(HazardShieldType.class, "443026e338dc1b3e39c53c3a0fa9355d", i2);
        bbK = b2;
        arzArr[i2] = b2;
        int i3 = i2 + 1;
        C5663aRz b3 = C5640aRc.m17844b(HazardShieldType.class, "756c430d1b7d44aaadc5c7b45ada849f", i3);
        atT = b3;
        arzArr[i3] = b3;
        int i4 = i3 + 1;
        C5663aRz b4 = C5640aRc.m17844b(HazardShieldType.class, "f6b6a8ef3f822342644e2432b9f83038", i4);
        atQ = b4;
        arzArr[i4] = b4;
        int i5 = i4 + 1;
        C5663aRz b5 = C5640aRc.m17844b(HazardShieldType.class, "fb14eaefa814fbebc3be0cb3edb43828", i5);
        aAe = b5;
        arzArr[i5] = b5;
        int i6 = i5 + 1;
        C5663aRz b6 = C5640aRc.m17844b(HazardShieldType.class, "3210178b90c2c5a32e7fc72acb807547", i6);
        aAg = b6;
        arzArr[i6] = b6;
        int i7 = i6 + 1;
        _m_fields = arzArr;
        C1634Xv.m11725a((Object[]) BaseTaikodomContent._m_fields, (Object[]) _m_fields);
        int i8 = BaseTaikodomContent._m_methodCount;
        C2491fm[] fmVarArr = new C2491fm[(i8 + 16)];
        C2491fm a = C4105zY.m41624a(HazardShieldType.class, "4f875cabff3071a86cb1012ef829ac0c", i8);
        axb = a;
        fmVarArr[i8] = a;
        int i9 = i8 + 1;
        C2491fm a2 = C4105zY.m41624a(HazardShieldType.class, "5cb62fb4ae6d659e9bc56822f3b8ed0a", i9);
        bbN = a2;
        fmVarArr[i9] = a2;
        int i10 = i9 + 1;
        C2491fm a3 = C4105zY.m41624a(HazardShieldType.class, "5bef0dc2c37a384961b61c2ff15a5011", i10);
        bbO = a3;
        fmVarArr[i10] = a3;
        int i11 = i10 + 1;
        C2491fm a4 = C4105zY.m41624a(HazardShieldType.class, "5ecf0e3003017b37f2ebaea9ae64b756", i11);
        bbP = a4;
        fmVarArr[i11] = a4;
        int i12 = i11 + 1;
        C2491fm a5 = C4105zY.m41624a(HazardShieldType.class, "4e3fb13bac9707d72b0676d5ad9a3bbe", i12);
        bbQ = a5;
        fmVarArr[i12] = a5;
        int i13 = i12 + 1;
        C2491fm a6 = C4105zY.m41624a(HazardShieldType.class, "9c9b148dfab262c8f408a36deeb42546", i13);
        bbR = a6;
        fmVarArr[i13] = a6;
        int i14 = i13 + 1;
        C2491fm a7 = C4105zY.m41624a(HazardShieldType.class, "4c0d473f4001ab464efe936774167ed5", i14);
        bbS = a7;
        fmVarArr[i14] = a7;
        int i15 = i14 + 1;
        C2491fm a8 = C4105zY.m41624a(HazardShieldType.class, "9a3739e661175b1fdb1425d2f10d697c", i15);
        bbT = a8;
        fmVarArr[i15] = a8;
        int i16 = i15 + 1;
        C2491fm a9 = C4105zY.m41624a(HazardShieldType.class, "ed8ac6205e01e0b3f02dcb61c1905db9", i16);
        bbU = a9;
        fmVarArr[i16] = a9;
        int i17 = i16 + 1;
        C2491fm a10 = C4105zY.m41624a(HazardShieldType.class, "1a42c1bb6e8c347987bf10a9c400500e", i17);
        bbV = a10;
        fmVarArr[i17] = a10;
        int i18 = i17 + 1;
        C2491fm a11 = C4105zY.m41624a(HazardShieldType.class, "43a1e021809fa716a290f212bba2e4ac", i18);
        aAi = a11;
        fmVarArr[i18] = a11;
        int i19 = i18 + 1;
        C2491fm a12 = C4105zY.m41624a(HazardShieldType.class, "25dd3618e79d5aec7634ccb025a4d96b", i19);
        aAj = a12;
        fmVarArr[i19] = a12;
        int i20 = i19 + 1;
        C2491fm a13 = C4105zY.m41624a(HazardShieldType.class, "65f43d55e6c074e0d2e8583f4f450b8f", i20);
        aAk = a13;
        fmVarArr[i20] = a13;
        int i21 = i20 + 1;
        C2491fm a14 = C4105zY.m41624a(HazardShieldType.class, "43eac4a59f5445334c50f72eb558c4e7", i21);
        aAl = a14;
        fmVarArr[i21] = a14;
        int i22 = i21 + 1;
        C2491fm a15 = C4105zY.m41624a(HazardShieldType.class, "45b6166de361ebc2166ff73507ba9ee4", i22);
        bbW = a15;
        fmVarArr[i22] = a15;
        int i23 = i22 + 1;
        C2491fm a16 = C4105zY.m41624a(HazardShieldType.class, "0b5e280d395b5ad8cd9b6cde41a8edf6", i23);
        f8975dN = a16;
        fmVarArr[i23] = a16;
        int i24 = i23 + 1;
        _m_methods = fmVarArr;
        C1634Xv.m11725a((Object[]) BaseTaikodomContent._m_methods, (Object[]) _m_methods);
        ___iScriptClass = aUO.m18566a(HazardShieldType.class, C6820auM.class, _m_fields, _m_methods);
    }

    /* renamed from: B */
    private void m37970B(Asset tCVar) {
        bFf().mo5608dq().mo3197f(aAe, tCVar);
    }

    /* renamed from: C */
    private void m37971C(Asset tCVar) {
        bFf().mo5608dq().mo3197f(aAg, tCVar);
    }

    /* renamed from: Mv */
    private Asset m37975Mv() {
        return (Asset) bFf().mo5608dq().mo3214p(aAe);
    }

    /* renamed from: Mw */
    private Asset m37976Mw() {
        return (Asset) bFf().mo5608dq().mo3214p(aAg);
    }

    /* renamed from: Yv */
    private C3892wO m37984Yv() {
        return (C3892wO) bFf().mo5608dq().mo3214p(bbI);
    }

    /* renamed from: Yw */
    private C2686iZ m37985Yw() {
        return (C2686iZ) bFf().mo5608dq().mo3214p(bbK);
    }

    /* renamed from: Yx */
    private float m37986Yx() {
        return bFf().mo5608dq().mo3211m(atT);
    }

    /* renamed from: Yy */
    private float m37987Yy() {
        return bFf().mo5608dq().mo3211m(atQ);
    }

    /* renamed from: cW */
    private void m37990cW(float f) {
        bFf().mo5608dq().mo3150a(atT, f);
    }

    /* renamed from: cX */
    private void m37991cX(float f) {
        bFf().mo5608dq().mo3150a(atQ, f);
    }

    /* renamed from: m */
    private void m37996m(C2686iZ iZVar) {
        bFf().mo5608dq().mo3197f(bbK, iZVar);
    }

    /* renamed from: x */
    private void m37997x(C3892wO wOVar) {
        bFf().mo5608dq().mo3197f(bbI, wOVar);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Sfx Start")
    /* renamed from: E */
    public void mo21558E(Asset tCVar) {
        switch (bFf().mo6893i(aAj)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, aAj, new Object[]{tCVar}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, aAj, new Object[]{tCVar}));
                break;
        }
        m37972D(tCVar);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Sfx Stop")
    /* renamed from: G */
    public void mo21559G(Asset tCVar) {
        switch (bFf().mo6893i(aAl)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, aAl, new Object[]{tCVar}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, aAl, new Object[]{tCVar}));
                break;
        }
        m37973F(tCVar);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Shield Life Reduction")
    /* renamed from: LG */
    public C3892wO mo21560LG() {
        switch (bFf().mo6893i(axb)) {
            case 0:
                return null;
            case 2:
                return (C3892wO) bFf().mo5606d(new aCE(this, axb, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, axb, new Object[0]));
                break;
        }
        return m37974LF();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Sfx Stop")
    /* renamed from: MA */
    public Asset mo21561MA() {
        switch (bFf().mo6893i(aAk)) {
            case 0:
                return null;
            case 2:
                return (Asset) bFf().mo5606d(new aCE(this, aAk, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, aAk, new Object[0]));
                break;
        }
        return m37978Mz();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Sfx Start")
    /* renamed from: My */
    public Asset mo21562My() {
        switch (bFf().mo6893i(aAi)) {
            case 0:
                return null;
            case 2:
                return (Asset) bFf().mo5606d(new aCE(this, aAi, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, aAi, new Object[0]));
                break;
        }
        return m37977Mx();
    }

    /* renamed from: T */
    public C6494any mo11T() {
        return ___iScriptClass;
    }

    /* renamed from: U */
    public C2491fm[] mo12U() {
        return _m_methods;
    }

    /* renamed from: W */
    public Object mo13W() {
        return new C6820auM(this);
    }

    /* renamed from: YA */
    public C2686iZ<DamageType> mo21563YA() {
        switch (bFf().mo6893i(bbQ)) {
            case 0:
                return null;
            case 2:
                return (C2686iZ) bFf().mo5606d(new aCE(this, bbQ, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, bbQ, new Object[0]));
                break;
        }
        return m37988Yz();
    }

    @C3248pc(aYR = C3248pc.C3250b.ITERATOR, displayName = "Resistance Types")
    /* renamed from: YC */
    public Set<DamageType> mo21564YC() {
        switch (bFf().mo6893i(bbR)) {
            case 0:
                return null;
            case 2:
                return (Set) bFf().mo5606d(new aCE(this, bbR, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, bbR, new Object[0]));
                break;
        }
        return m37980YB();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Warmup")
    /* renamed from: YE */
    public float mo21565YE() {
        switch (bFf().mo6893i(bbS)) {
            case 0:
                return 0.0f;
            case 2:
                return ((Float) bFf().mo5606d(new aCE(this, bbS, new Object[0]))).floatValue();
            case 3:
                bFf().mo5606d(new aCE(this, bbS, new Object[0]));
                break;
        }
        return m37981YD();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Cooldown")
    /* renamed from: YG */
    public float mo21566YG() {
        switch (bFf().mo6893i(bbU)) {
            case 0:
                return 0.0f;
            case 2:
                return ((Float) bFf().mo5606d(new aCE(this, bbU, new Object[0]))).floatValue();
            case 3:
                bFf().mo5606d(new aCE(this, bbU, new Object[0]));
                break;
        }
        return m37982YF();
    }

    /* renamed from: YI */
    public HazardShield mo21567YI() {
        switch (bFf().mo6893i(bbW)) {
            case 0:
                return null;
            case 2:
                return (HazardShield) bFf().mo5606d(new aCE(this, bbW, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, bbW, new Object[0]));
                break;
        }
        return m37983YH();
    }

    /* renamed from: a */
    public Object mo14a(C0495Gr gr) {
        Object[] args = gr.getArgs();
        switch (gr.mo2417hq() - BaseTaikodomContent._m_methodCount) {
            case 0:
                return m37974LF();
            case 1:
                m37998y((C3892wO) args[0]);
                return null;
            case 2:
                m37994i((DamageType) args[0]);
                return null;
            case 3:
                m37995k((DamageType) args[0]);
                return null;
            case 4:
                return m37988Yz();
            case 5:
                return m37980YB();
            case 6:
                return new Float(m37981YD());
            case 7:
                m37992cY(((Float) args[0]).floatValue());
                return null;
            case 8:
                return new Float(m37982YF());
            case 9:
                m37993da(((Float) args[0]).floatValue());
                return null;
            case 10:
                return m37977Mx();
            case 11:
                m37972D((Asset) args[0]);
                return null;
            case 12:
                return m37978Mz();
            case 13:
                m37973F((Asset) args[0]);
                return null;
            case 14:
                return m37983YH();
            case 15:
                return m37989aT();
            default:
                return super.mo14a(gr);
        }
    }

    /* renamed from: a */
    public void mo15a(Collection collection, C0495Gr gr) {
        super.mo15a(collection, gr);
    }

    /* access modifiers changed from: protected */
    /* renamed from: aU */
    public <T extends aDJ> T mo745aU() {
        switch (bFf().mo6893i(f8975dN)) {
            case 0:
                return null;
            case 2:
                return (aDJ) bFf().mo5606d(new aCE(this, f8975dN, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f8975dN, new Object[0]));
                break;
        }
        return m37989aT();
    }

    /* renamed from: c */
    public C5663aRz[] mo25c() {
        return _m_fields;
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Warmup")
    /* renamed from: cZ */
    public void mo21568cZ(float f) {
        switch (bFf().mo6893i(bbT)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, bbT, new Object[]{new Float(f)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, bbT, new Object[]{new Float(f)}));
                break;
        }
        m37992cY(f);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Cooldown")
    /* renamed from: db */
    public void mo21569db(float f) {
        switch (bFf().mo6893i(bbV)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, bbV, new Object[]{new Float(f)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, bbV, new Object[]{new Float(f)}));
                break;
        }
        m37993da(f);
    }

    @C3248pc(aYR = C3248pc.C3250b.ADDER, displayName = "Resistance Types")
    /* renamed from: j */
    public void mo21570j(DamageType fr) {
        switch (bFf().mo6893i(bbO)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, bbO, new Object[]{fr}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, bbO, new Object[]{fr}));
                break;
        }
        m37994i(fr);
    }

    @C3248pc(aYR = C3248pc.C3250b.REMOVER, displayName = "Resistance Types")
    /* renamed from: l */
    public void mo21571l(DamageType fr) {
        switch (bFf().mo6893i(bbP)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, bbP, new Object[]{fr}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, bbP, new Object[]{fr}));
                break;
        }
        m37995k(fr);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Shield Life Reduction")
    /* renamed from: z */
    public void mo21572z(C3892wO wOVar) {
        switch (bFf().mo6893i(bbN)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, bbN, new Object[]{wOVar}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, bbN, new Object[]{wOVar}));
                break;
        }
        m37998y(wOVar);
    }

    /* renamed from: S */
    public void mo10S() {
        super.mo10S();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Shield Life Reduction")
    @C0064Am(aul = "4f875cabff3071a86cb1012ef829ac0c", aum = 0)
    /* renamed from: LF */
    private C3892wO m37974LF() {
        return m37984Yv();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Shield Life Reduction")
    @C0064Am(aul = "5cb62fb4ae6d659e9bc56822f3b8ed0a", aum = 0)
    /* renamed from: y */
    private void m37998y(C3892wO wOVar) {
        m37997x(wOVar);
    }

    @C3248pc(aYR = C3248pc.C3250b.ADDER, displayName = "Resistance Types")
    @C0064Am(aul = "5bef0dc2c37a384961b61c2ff15a5011", aum = 0)
    /* renamed from: i */
    private void m37994i(DamageType fr) {
        m37985Yw().add(fr);
    }

    @C3248pc(aYR = C3248pc.C3250b.REMOVER, displayName = "Resistance Types")
    @C0064Am(aul = "5ecf0e3003017b37f2ebaea9ae64b756", aum = 0)
    /* renamed from: k */
    private void m37995k(DamageType fr) {
        m37985Yw().remove(fr);
    }

    @C0064Am(aul = "4e3fb13bac9707d72b0676d5ad9a3bbe", aum = 0)
    /* renamed from: Yz */
    private C2686iZ<DamageType> m37988Yz() {
        return m37985Yw();
    }

    @C3248pc(aYR = C3248pc.C3250b.ITERATOR, displayName = "Resistance Types")
    @C0064Am(aul = "9c9b148dfab262c8f408a36deeb42546", aum = 0)
    /* renamed from: YB */
    private Set<DamageType> m37980YB() {
        return Collections.unmodifiableSet(m37985Yw());
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Warmup")
    @C0064Am(aul = "4c0d473f4001ab464efe936774167ed5", aum = 0)
    /* renamed from: YD */
    private float m37981YD() {
        return m37986Yx();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Warmup")
    @C0064Am(aul = "9a3739e661175b1fdb1425d2f10d697c", aum = 0)
    /* renamed from: cY */
    private void m37992cY(float f) {
        m37990cW(f);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Cooldown")
    @C0064Am(aul = "ed8ac6205e01e0b3f02dcb61c1905db9", aum = 0)
    /* renamed from: YF */
    private float m37982YF() {
        return m37987Yy();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Cooldown")
    @C0064Am(aul = "1a42c1bb6e8c347987bf10a9c400500e", aum = 0)
    /* renamed from: da */
    private void m37993da(float f) {
        m37991cX(f);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Sfx Start")
    @C0064Am(aul = "43a1e021809fa716a290f212bba2e4ac", aum = 0)
    /* renamed from: Mx */
    private Asset m37977Mx() {
        return m37975Mv();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Sfx Start")
    @C0064Am(aul = "25dd3618e79d5aec7634ccb025a4d96b", aum = 0)
    /* renamed from: D */
    private void m37972D(Asset tCVar) {
        m37970B(tCVar);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Sfx Stop")
    @C0064Am(aul = "65f43d55e6c074e0d2e8583f4f450b8f", aum = 0)
    /* renamed from: Mz */
    private Asset m37978Mz() {
        return m37976Mw();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Sfx Stop")
    @C0064Am(aul = "43eac4a59f5445334c50f72eb558c4e7", aum = 0)
    /* renamed from: F */
    private void m37973F(Asset tCVar) {
        m37971C(tCVar);
    }

    @C0064Am(aul = "45b6166de361ebc2166ff73507ba9ee4", aum = 0)
    /* renamed from: YH */
    private HazardShield m37983YH() {
        return (HazardShield) mo745aU();
    }

    @C0064Am(aul = "0b5e280d395b5ad8cd9b6cde41a8edf6", aum = 0)
    /* renamed from: aT */
    private <T extends aDJ> T m37989aT() {
        T t = (HazardShield) bFf().mo6865M(HazardShield.class);
        t.mo17642d(this);
        return t;
    }
}
