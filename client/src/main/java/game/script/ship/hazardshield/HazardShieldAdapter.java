package game.script.ship.hazardshield;

import game.network.message.externalizable.aCE;
import game.script.util.GameObjectAdapter;
import logic.baa.C1616Xf;
import logic.baa.C1634Xv;
import logic.baa.C5511aMd;
import logic.baa.C6485anp;
import logic.data.mbean.C2090bu;
import logic.res.code.C5663aRz;
import logic.res.code.C6494any;
import logic.res.html.C2491fm;
import p001a.*;

import java.util.Collection;

@C5511aMd
@C6485anp
/* renamed from: a.aBw  reason: case insensitive filesystem */
/* compiled from: a */
public class HazardShieldAdapter extends GameObjectAdapter<HazardShieldAdapter> implements C1616Xf {
    public static final int _m_fieldCount = 0;
    public static final C5663aRz[] _m_fields = null;
    public static final int _m_methodCount = 0;
    public static final C2491fm[] _m_methods = null;
    public static final C2491fm axb = null;
    public static final long serialVersionUID = 0;
    public static C6494any ___iScriptClass;

    static {
        m13107V();
    }

    public HazardShieldAdapter() {
        super((C5540aNg) null);
        super.mo10S();
    }

    public HazardShieldAdapter(C5540aNg ang) {
        super(ang);
    }

    /* renamed from: V */
    static void m13107V() {
        serialVersionUID = (long) 1;
        _m_fieldCount = GameObjectAdapter._m_fieldCount + 0;
        _m_methodCount = GameObjectAdapter._m_methodCount + 1;
        _m_fields = new C5663aRz[(GameObjectAdapter._m_fieldCount + 0)];
        C1634Xv.m11725a((Object[]) GameObjectAdapter._m_fields, (Object[]) _m_fields);
        int i = GameObjectAdapter._m_methodCount;
        C2491fm[] fmVarArr = new C2491fm[(i + 1)];
        C2491fm a = C4105zY.m41624a(HazardShieldAdapter.class, "f23df4c0539a5e62ea16a72ba25cd786", i);
        axb = a;
        fmVarArr[i] = a;
        int i2 = i + 1;
        _m_methods = fmVarArr;
        C1634Xv.m11725a((Object[]) GameObjectAdapter._m_methods, (Object[]) _m_methods);
        ___iScriptClass = aUO.m18566a(HazardShieldAdapter.class, C2090bu.class, _m_fields, _m_methods);
    }

    /* renamed from: LG */
    public C3892wO mo7993LG() {
        switch (bFf().mo6893i(axb)) {
            case 0:
                return null;
            case 2:
                return (C3892wO) bFf().mo5606d(new aCE(this, axb, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, axb, new Object[0]));
                break;
        }
        return m13106LF();
    }

    /* renamed from: T */
    public C6494any mo11T() {
        return ___iScriptClass;
    }

    /* renamed from: U */
    public C2491fm[] mo12U() {
        return _m_methods;
    }

    /* renamed from: W */
    public Object mo13W() {
        return new C2090bu(this);
    }

    /* renamed from: a */
    public Object mo14a(C0495Gr gr) {
        gr.getArgs();
        switch (gr.mo2417hq() - GameObjectAdapter._m_methodCount) {
            case 0:
                return m13106LF();
            default:
                return super.mo14a(gr);
        }
    }

    /* renamed from: a */
    public void mo15a(Collection collection, C0495Gr gr) {
        super.mo15a(collection, gr);
    }

    /* renamed from: c */
    public C5663aRz[] mo25c() {
        return _m_fields;
    }

    /* renamed from: S */
    public void mo10S() {
        super.mo10S();
    }

    @C0064Am(aul = "f23df4c0539a5e62ea16a72ba25cd786", aum = 0)
    /* renamed from: LF */
    private C3892wO m13106LF() {
        return ((HazardShieldAdapter) mo16071IG()).mo7993LG();
    }
}
