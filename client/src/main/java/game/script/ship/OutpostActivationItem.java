package game.script.ship;

import game.network.message.externalizable.aCE;
import game.script.item.ItemType;
import logic.baa.*;
import logic.data.mbean.C0189CO;
import logic.res.code.C5640aRc;
import logic.res.code.C5663aRz;
import logic.res.code.C6494any;
import logic.res.html.C2491fm;
import p001a.*;

import java.util.Collection;
import java.util.UUID;

@C5511aMd
@C6485anp
/* renamed from: a.uB */
/* compiled from: a */
public class OutpostActivationItem extends aDJ implements C0468GU, C1616Xf {
    public static final int _m_fieldCount = 0;
    public static final C5663aRz[] _m_fields = null;
    public static final int _m_methodCount = 0;
    public static final C2491fm[] _m_methods = null;
    /* renamed from: bL */
    public static final C5663aRz f9296bL = null;
    /* renamed from: bM */
    public static final C5663aRz f9297bM = null;
    /* renamed from: bN */
    public static final C2491fm f9298bN = null;
    /* renamed from: bO */
    public static final C2491fm f9299bO = null;
    /* renamed from: bP */
    public static final C2491fm f9300bP = null;
    /* renamed from: bQ */
    public static final C2491fm f9301bQ = null;
    public static final C5663aRz bwf = null;
    public static final C2491fm bwg = null;
    public static final C2491fm bwh = null;
    /* renamed from: cA */
    public static final C2491fm f9302cA = null;
    /* renamed from: cB */
    public static final C2491fm f9303cB = null;
    /* renamed from: cC */
    public static final C2491fm f9304cC = null;
    /* renamed from: cD */
    public static final C2491fm f9305cD = null;
    /* renamed from: cx */
    public static final C5663aRz f9307cx = null;
    /* renamed from: cz */
    public static final C5663aRz f9309cz = null;
    public static final long serialVersionUID = 0;
    public static C6494any ___iScriptClass;
    @C0064Am(aul = "61a1073ff25526df2ec9b9898997e336", aum = 3)

    /* renamed from: bK */
    private static UUID f9295bK;
    @C0064Am(aul = "a89f5c826390d138056528a9c0d7f2fe", aum = 2)
    private static int current;
    @C0064Am(aul = "a473db16ef0d0371a14a6096ec2397e5", aum = 0)

    /* renamed from: cw */
    private static ItemType f9306cw;
    @C0064Am(aul = "685a59f14517cbc2efa65d64d813e96a", aum = 1)

    /* renamed from: cy */
    private static int f9308cy;
    @C0064Am(aul = "3673f82d3767643e64c926171d8aaf7f", aum = 4)
    private static String handle;

    static {
        m39824V();
    }

    public OutpostActivationItem() {
        super((C5540aNg) null);
        super.mo10S();
    }

    public OutpostActivationItem(C5540aNg ang) {
        super(ang);
    }

    /* renamed from: V */
    static void m39824V() {
        serialVersionUID = (long) 1;
        _m_fieldCount = aDJ._m_fieldCount + 5;
        _m_methodCount = aDJ._m_methodCount + 10;
        int i = aDJ._m_fieldCount;
        C5663aRz[] arzArr = new C5663aRz[(i + 5)];
        C5663aRz b = C5640aRc.m17844b(OutpostActivationItem.class, "a473db16ef0d0371a14a6096ec2397e5", i);
        f9307cx = b;
        arzArr[i] = b;
        int i2 = i + 1;
        C5663aRz b2 = C5640aRc.m17844b(OutpostActivationItem.class, "685a59f14517cbc2efa65d64d813e96a", i2);
        f9309cz = b2;
        arzArr[i2] = b2;
        int i3 = i2 + 1;
        C5663aRz b3 = C5640aRc.m17844b(OutpostActivationItem.class, "a89f5c826390d138056528a9c0d7f2fe", i3);
        bwf = b3;
        arzArr[i3] = b3;
        int i4 = i3 + 1;
        C5663aRz b4 = C5640aRc.m17844b(OutpostActivationItem.class, "61a1073ff25526df2ec9b9898997e336", i4);
        f9296bL = b4;
        arzArr[i4] = b4;
        int i5 = i4 + 1;
        C5663aRz b5 = C5640aRc.m17844b(OutpostActivationItem.class, "3673f82d3767643e64c926171d8aaf7f", i5);
        f9297bM = b5;
        arzArr[i5] = b5;
        int i6 = i5 + 1;
        _m_fields = arzArr;
        C1634Xv.m11725a((Object[]) aDJ._m_fields, (Object[]) _m_fields);
        int i7 = aDJ._m_methodCount;
        C2491fm[] fmVarArr = new C2491fm[(i7 + 10)];
        C2491fm a = C4105zY.m41624a(OutpostActivationItem.class, "1447c97a2990df872971a2c09ee45321", i7);
        f9298bN = a;
        fmVarArr[i7] = a;
        int i8 = i7 + 1;
        C2491fm a2 = C4105zY.m41624a(OutpostActivationItem.class, "6822626271972c77d92132d22593a053", i8);
        f9299bO = a2;
        fmVarArr[i8] = a2;
        int i9 = i8 + 1;
        C2491fm a3 = C4105zY.m41624a(OutpostActivationItem.class, "d202885ab61751ac572a6bff72aee0ea", i9);
        f9300bP = a3;
        fmVarArr[i9] = a3;
        int i10 = i9 + 1;
        C2491fm a4 = C4105zY.m41624a(OutpostActivationItem.class, "986c9477e99dc7aa6198070837f912d1", i10);
        f9301bQ = a4;
        fmVarArr[i10] = a4;
        int i11 = i10 + 1;
        C2491fm a5 = C4105zY.m41624a(OutpostActivationItem.class, "c942578dee64c935db83b9e41397de4d", i11);
        f9302cA = a5;
        fmVarArr[i11] = a5;
        int i12 = i11 + 1;
        C2491fm a6 = C4105zY.m41624a(OutpostActivationItem.class, "14e49f1927474b0aa9ea1d5a43b20bc8", i12);
        f9303cB = a6;
        fmVarArr[i12] = a6;
        int i13 = i12 + 1;
        C2491fm a7 = C4105zY.m41624a(OutpostActivationItem.class, "d9e9f6e203336b46433b10fb3d0f36b0", i13);
        f9304cC = a7;
        fmVarArr[i13] = a7;
        int i14 = i13 + 1;
        C2491fm a8 = C4105zY.m41624a(OutpostActivationItem.class, "167d439806360e70818521b3523257b8", i14);
        f9305cD = a8;
        fmVarArr[i14] = a8;
        int i15 = i14 + 1;
        C2491fm a9 = C4105zY.m41624a(OutpostActivationItem.class, "6d9a5c81401b7eae73989a1e72fa9086", i15);
        bwg = a9;
        fmVarArr[i15] = a9;
        int i16 = i15 + 1;
        C2491fm a10 = C4105zY.m41624a(OutpostActivationItem.class, "a5be97da25a4aa75bd23f9a2ded3ca5f", i16);
        bwh = a10;
        fmVarArr[i16] = a10;
        int i17 = i16 + 1;
        _m_methods = fmVarArr;
        C1634Xv.m11725a((Object[]) aDJ._m_methods, (Object[]) _m_methods);
        ___iScriptClass = aUO.m18566a(OutpostActivationItem.class, C0189CO.class, _m_fields, _m_methods);
    }

    /* renamed from: a */
    private void m39825a(ItemType jCVar) {
        bFf().mo5608dq().mo3197f(f9307cx, jCVar);
    }

    /* renamed from: a */
    private void m39826a(String str) {
        bFf().mo5608dq().mo3197f(f9297bM, str);
    }

    /* renamed from: a */
    private void m39827a(UUID uuid) {
        bFf().mo5608dq().mo3197f(f9296bL, uuid);
    }

    private int ajp() {
        return bFf().mo5608dq().mo3212n(bwf);
    }

    /* renamed from: an */
    private UUID m39828an() {
        return (UUID) bFf().mo5608dq().mo3214p(f9296bL);
    }

    /* renamed from: ao */
    private String m39829ao() {
        return (String) bFf().mo5608dq().mo3214p(f9297bM);
    }

    /* renamed from: av */
    private ItemType m39832av() {
        return (ItemType) bFf().mo5608dq().mo3214p(f9307cx);
    }

    /* renamed from: aw */
    private int m39833aw() {
        return bFf().mo5608dq().mo3212n(f9309cz);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Item Type")
    @C0064Am(aul = "167d439806360e70818521b3523257b8", aum = 0)
    @C5566aOg
    /* renamed from: b */
    private void m39836b(ItemType jCVar) {
        throw new aWi(new aCE(this, f9305cD, new Object[]{jCVar}));
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Handle")
    @C0064Am(aul = "986c9477e99dc7aa6198070837f912d1", aum = 0)
    @C5566aOg
    /* renamed from: b */
    private void m39837b(String str) {
        throw new aWi(new aCE(this, f9301bQ, new Object[]{str}));
    }

    /* renamed from: c */
    private void m39839c(UUID uuid) {
        switch (bFf().mo6893i(f9299bO)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f9299bO, new Object[]{uuid}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f9299bO, new Object[]{uuid}));
                break;
        }
        m39838b(uuid);
    }

    /* renamed from: eQ */
    private void m39840eQ(int i) {
        bFf().mo5608dq().mo3183b(bwf, i);
    }

    /* renamed from: f */
    private void m39842f(int i) {
        bFf().mo5608dq().mo3183b(f9309cz, i);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Amount")
    @C0064Am(aul = "14e49f1927474b0aa9ea1d5a43b20bc8", aum = 0)
    @C5566aOg
    /* renamed from: g */
    private void m39843g(int i) {
        throw new aWi(new aCE(this, f9303cB, new Object[]{new Integer(i)}));
    }

    /* renamed from: T */
    public C6494any mo11T() {
        return ___iScriptClass;
    }

    /* renamed from: U */
    public C2491fm[] mo12U() {
        return _m_methods;
    }

    /* renamed from: W */
    public Object mo13W() {
        return new C0189CO(this);
    }

    /* renamed from: a */
    public Object mo14a(C0495Gr gr) {
        Object[] args = gr.getArgs();
        switch (gr.mo2417hq() - aDJ._m_methodCount) {
            case 0:
                return m39830ap();
            case 1:
                m39838b((UUID) args[0]);
                return null;
            case 2:
                return m39831ar();
            case 3:
                m39837b((String) args[0]);
                return null;
            case 4:
                return new Integer(m39834ax());
            case 5:
                m39843g(((Integer) args[0]).intValue());
                return null;
            case 6:
                return m39835ay();
            case 7:
                m39836b((ItemType) args[0]);
                return null;
            case 8:
                return new Integer(ajq());
            case 9:
                m39841eR(((Integer) args[0]).intValue());
                return null;
            default:
                return super.mo14a(gr);
        }
    }

    /* renamed from: a */
    public void mo15a(Collection collection, C0495Gr gr) {
        super.mo15a(collection, gr);
    }

    public int ajr() {
        switch (bFf().mo6893i(bwg)) {
            case 0:
                return 0;
            case 2:
                return ((Integer) bFf().mo5606d(new aCE(this, bwg, new Object[0]))).intValue();
            case 3:
                bFf().mo5606d(new aCE(this, bwg, new Object[0]));
                break;
        }
        return ajq();
    }

    /* renamed from: aq */
    public UUID mo18aq() {
        switch (bFf().mo6893i(f9298bN)) {
            case 0:
                return null;
            case 2:
                return (UUID) bFf().mo5606d(new aCE(this, f9298bN, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f9298bN, new Object[0]));
                break;
        }
        return m39830ap();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Item Type")
    /* renamed from: az */
    public ItemType mo22322az() {
        switch (bFf().mo6893i(f9304cC)) {
            case 0:
                return null;
            case 2:
                return (ItemType) bFf().mo5606d(new aCE(this, f9304cC, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f9304cC, new Object[0]));
                break;
        }
        return m39835ay();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Item Type")
    @C5566aOg
    /* renamed from: c */
    public void mo22323c(ItemType jCVar) {
        switch (bFf().mo6893i(f9305cD)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f9305cD, new Object[]{jCVar}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f9305cD, new Object[]{jCVar}));
                break;
        }
        m39836b(jCVar);
    }

    /* renamed from: c */
    public C5663aRz[] mo25c() {
        return _m_fields;
    }

    /* renamed from: eS */
    public void mo22324eS(int i) {
        switch (bFf().mo6893i(bwh)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, bwh, new Object[]{new Integer(i)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, bwh, new Object[]{new Integer(i)}));
                break;
        }
        m39841eR(i);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Amount")
    public int getAmount() {
        switch (bFf().mo6893i(f9302cA)) {
            case 0:
                return 0;
            case 2:
                return ((Integer) bFf().mo5606d(new aCE(this, f9302cA, new Object[0]))).intValue();
            case 3:
                bFf().mo5606d(new aCE(this, f9302cA, new Object[0]));
                break;
        }
        return m39834ax();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Amount")
    @C5566aOg
    public void setAmount(int i) {
        switch (bFf().mo6893i(f9303cB)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f9303cB, new Object[]{new Integer(i)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f9303cB, new Object[]{new Integer(i)}));
                break;
        }
        m39843g(i);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Handle")
    public String getHandle() {
        switch (bFf().mo6893i(f9300bP)) {
            case 0:
                return null;
            case 2:
                return (String) bFf().mo5606d(new aCE(this, f9300bP, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f9300bP, new Object[0]));
                break;
        }
        return m39831ar();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Handle")
    @C5566aOg
    public void setHandle(String str) {
        switch (bFf().mo6893i(f9301bQ)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f9301bQ, new Object[]{str}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f9301bQ, new Object[]{str}));
                break;
        }
        m39837b(str);
    }

    @C0064Am(aul = "1447c97a2990df872971a2c09ee45321", aum = 0)
    /* renamed from: ap */
    private UUID m39830ap() {
        return m39828an();
    }

    @C0064Am(aul = "6822626271972c77d92132d22593a053", aum = 0)
    /* renamed from: b */
    private void m39838b(UUID uuid) {
        m39827a(uuid);
    }

    /* renamed from: S */
    public void mo10S() {
        super.mo10S();
        m39827a(UUID.randomUUID());
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Handle")
    @C0064Am(aul = "d202885ab61751ac572a6bff72aee0ea", aum = 0)
    /* renamed from: ar */
    private String m39831ar() {
        return m39829ao();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Amount")
    @C0064Am(aul = "c942578dee64c935db83b9e41397de4d", aum = 0)
    /* renamed from: ax */
    private int m39834ax() {
        return m39833aw();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Item Type")
    @C0064Am(aul = "d9e9f6e203336b46433b10fb3d0f36b0", aum = 0)
    /* renamed from: ay */
    private ItemType m39835ay() {
        return m39832av();
    }

    @C0064Am(aul = "6d9a5c81401b7eae73989a1e72fa9086", aum = 0)
    private int ajq() {
        return ajp();
    }

    @C0064Am(aul = "a5be97da25a4aa75bd23f9a2ded3ca5f", aum = 0)
    /* renamed from: eR */
    private void m39841eR(int i) {
        m39840eQ(i);
    }
}
