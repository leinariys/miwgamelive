package game.script.ship;

import game.network.manager.C6546aoy;
import game.network.message.externalizable.aCE;
import game.network.message.serializable.C1556Wo;
import game.network.message.serializable.C3438ra;
import game.script.Character;
import game.script.TaskletImpl;
import game.script.corporation.Corporation;
import game.script.item.Item;
import game.script.nls.NLSManager;
import game.script.nls.NLSOutpost;
import game.script.player.Player;
import game.script.storage.OutpostStorage;
import game.script.storage.Storage;
import logic.baa.*;
import logic.data.link.C6360alU;
import logic.data.mbean.C0556Hj;
import logic.data.mbean.C1486Vs;
import logic.res.code.C5640aRc;
import logic.res.code.C5663aRz;
import logic.res.code.C6494any;
import logic.res.html.C2491fm;
import logic.thred.LogPrinter;
import org.apache.commons.logging.Log;
import p001a.*;
import taikodom.infra.script.I18NString;

import java.io.ObjectInput;
import java.io.ObjectOutput;
import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;

@C6485anp
@C2712iu(mo19786Bt = BaseOutpostType.class)
@C5511aMd
/* renamed from: a.qZ */
/* compiled from: a */
public class Outpost extends Station implements C1616Xf {

    /* renamed from: KP */
    public static final C5663aRz f8935KP = null;

    /* renamed from: Lm */
    public static final C2491fm f8936Lm = null;

    /* renamed from: MH */
    public static final C2491fm f8937MH = null;

    /* renamed from: MN */
    public static final C2491fm f8938MN = null;

    /* renamed from: MT */
    public static final C5663aRz f8939MT = null;

    /* renamed from: Pf */
    public static final C2491fm f8940Pf = null;
    public static final C2491fm _f_isEnabled_0020_0028_0029Z = null;
    public static final C2491fm _f_toString_0020_0028_0029Ljava_002flang_002fString_003b = null;
    public static final int _m_fieldCount = 0;
    public static final C5663aRz[] _m_fields = null;
    public static final int _m_methodCount = 0;
    public static final C2491fm[] _m_methods = null;
    public static final C5663aRz axd = null;
    public static final C5663aRz ayJ = null;
    public static final C2491fm ayM = null;
    public static final C2491fm bqO = null;
    public static final C2491fm brK = null;
    public static final C2491fm coG = null;
    public static final C2491fm coI = null;
    public static final C2491fm coO = null;
    public static final C2491fm coS = null;
    public static final C2491fm dTf = null;
    public static final C2491fm dXQ = null;
    public static final C2491fm dXR = null;
    public static final C5663aRz dnO = null;
    public static final C5663aRz dsA = null;
    public static final C5663aRz dsB = null;
    public static final C5663aRz dsw = null;
    public static final C5663aRz dsx = null;
    public static final C5663aRz dsy = null;
    public static final C5663aRz dsz = null;
    public static final C5663aRz fHY = null;
    public static final C5663aRz fHZ = null;
    public static final C2491fm fIA = null;
    public static final C2491fm fIB = null;
    public static final C2491fm fIC = null;
    public static final C5663aRz fIa = null;
    public static final C5663aRz fIb = null;
    public static final C2491fm fIc = null;
    public static final C2491fm fId = null;
    public static final C2491fm fIe = null;
    public static final C2491fm fIf = null;
    public static final C2491fm fIg = null;
    public static final C2491fm fIh = null;
    public static final C2491fm fIi = null;
    public static final C2491fm fIj = null;
    public static final C2491fm fIk = null;
    public static final C2491fm fIl = null;
    public static final C2491fm fIm = null;
    public static final C2491fm fIn = null;
    public static final C2491fm fIo = null;
    public static final C2491fm fIp = null;
    public static final C2491fm fIq = null;
    public static final C2491fm fIr = null;
    public static final C2491fm fIs = null;
    public static final C2491fm fIt = null;
    public static final C2491fm fIu = null;
    public static final C2491fm fIv = null;
    public static final C2491fm fIw = null;
    public static final C2491fm fIx = null;
    public static final C2491fm fIy = null;
    public static final C2491fm fIz = null;
    public static final long serialVersionUID = 0;
    private static final Log logger = LogPrinter.setClass(Outpost.class);
    public static C6494any ___iScriptClass;
    @C0064Am(aul = "ad8562474e94ad972d48d4c89a2f2304", aum = 11)
    private static C2611hZ ayI;
    @C0064Am(aul = "b376464f7e1bc67da442e3c6561b977d", aum = 1)
    @C5454aJy("Activation - Reservation Length (in days)")
    private static int daP;
    @C0064Am(aul = "42449b86b3d3b379ca0590ce4b8ed8e5", aum = 2)
    @C5454aJy("Activation - Required material")
    private static C3438ra<OutpostActivationItem> daQ;
    @C0064Am(aul = "e424ca71d5a1bc836dd6dc4ab5cf34fb", aum = 3)
    private static aHK daR;
    @C0064Am(aul = "6b38e6bd970f6c5bec8c21e446fc2c23", aum = 4)
    private static I18NString daS;
    @C0064Am(aul = "4e5c58160b995cfd6471fdaebb670412", aum = 5)
    private static C1556Wo<C2611hZ, OutpostUpgrade> daT;
    @C0064Am(aul = "bfd5198866eddd8679e17d10b2f09baa", aum = 6)
    private static C1556Wo<C2611hZ, OutpostLevelInfo> daU;
    @C0064Am(aul = "e6a48ed6b705f7193ea0417435227c8a", aum = 7)
    private static C1556Wo<C0437GE, OutpostLevelFeature> daV;
    @C0064Am(aul = "f7a0e33d599083c2cb09ee1541e9fcc9", aum = 8)
    private static Corporation daW;
    @C0064Am(aul = "2223ed646040ece9599d191a14dc3b09", aum = 9)
    private static C3349b daX;
    @C0064Am(aul = "2e43a187cea8d996d16e1c55cf9dc195", aum = 10)
    private static OutpostStorage daY;
    @C0064Am(aul = "aa7b88d348d7a24f621aefbba600fa52", aum = 12)
    private static OutpostUpkeepPeriod daZ;
    @C0064Am(aul = "669fdadbce330350fd8bc021e2554107", aum = 0)
    @C5454aJy("Activation - Required fee")

    /* renamed from: db */
    private static long f8941db;
    @C0064Am(aul = "7ccaabf4093d42bf62a12fa7521039a6", aum = 13)
    private static OutpostUpgradeStatus dba;
    @C0064Am(aul = "c11643b333ba651219dd6fce65d44e11", aum = 14)
    private static boolean disabled;

    static {
        m37652V();
    }

    public Outpost() {
        super((C5540aNg) null);
        super.mo10S();
    }

    public Outpost(OutpostType kz) {
        super((C5540aNg) null);
        super._m_script_init(kz);
    }

    public Outpost(C5540aNg ang) {
        super(ang);
    }

    /* renamed from: V */
    static void m37652V() {
        serialVersionUID = (long) 1;
        _m_fieldCount = Station._m_fieldCount + 15;
        _m_methodCount = Station._m_methodCount + 43;
        int i = Station._m_fieldCount;
        C5663aRz[] arzArr = new C5663aRz[(i + 15)];
        C5663aRz b = C5640aRc.m17844b(Outpost.class, "669fdadbce330350fd8bc021e2554107", i);
        f8939MT = b;
        arzArr[i] = b;
        int i2 = i + 1;
        C5663aRz b2 = C5640aRc.m17844b(Outpost.class, "b376464f7e1bc67da442e3c6561b977d", i2);
        dsw = b2;
        arzArr[i2] = b2;
        int i3 = i2 + 1;
        C5663aRz b3 = C5640aRc.m17844b(Outpost.class, "42449b86b3d3b379ca0590ce4b8ed8e5", i3);
        dsx = b3;
        arzArr[i3] = b3;
        int i4 = i3 + 1;
        C5663aRz b4 = C5640aRc.m17844b(Outpost.class, "e424ca71d5a1bc836dd6dc4ab5cf34fb", i4);
        f8935KP = b4;
        arzArr[i4] = b4;
        int i5 = i4 + 1;
        C5663aRz b5 = C5640aRc.m17844b(Outpost.class, "6b38e6bd970f6c5bec8c21e446fc2c23", i5);
        dsy = b5;
        arzArr[i5] = b5;
        int i6 = i5 + 1;
        C5663aRz b6 = C5640aRc.m17844b(Outpost.class, "4e5c58160b995cfd6471fdaebb670412", i6);
        dsz = b6;
        arzArr[i6] = b6;
        int i7 = i6 + 1;
        C5663aRz b7 = C5640aRc.m17844b(Outpost.class, "bfd5198866eddd8679e17d10b2f09baa", i7);
        dsA = b7;
        arzArr[i7] = b7;
        int i8 = i7 + 1;
        C5663aRz b8 = C5640aRc.m17844b(Outpost.class, "e6a48ed6b705f7193ea0417435227c8a", i8);
        dsB = b8;
        arzArr[i8] = b8;
        int i9 = i8 + 1;
        C5663aRz b9 = C5640aRc.m17844b(Outpost.class, "f7a0e33d599083c2cb09ee1541e9fcc9", i9);
        fHY = b9;
        arzArr[i9] = b9;
        int i10 = i9 + 1;
        C5663aRz b10 = C5640aRc.m17844b(Outpost.class, "2223ed646040ece9599d191a14dc3b09", i10);
        axd = b10;
        arzArr[i10] = b10;
        int i11 = i10 + 1;
        C5663aRz b11 = C5640aRc.m17844b(Outpost.class, "2e43a187cea8d996d16e1c55cf9dc195", i11);
        fHZ = b11;
        arzArr[i11] = b11;
        int i12 = i11 + 1;
        C5663aRz b12 = C5640aRc.m17844b(Outpost.class, "ad8562474e94ad972d48d4c89a2f2304", i12);
        ayJ = b12;
        arzArr[i12] = b12;
        int i13 = i12 + 1;
        C5663aRz b13 = C5640aRc.m17844b(Outpost.class, "aa7b88d348d7a24f621aefbba600fa52", i13);
        fIa = b13;
        arzArr[i13] = b13;
        int i14 = i13 + 1;
        C5663aRz b14 = C5640aRc.m17844b(Outpost.class, "7ccaabf4093d42bf62a12fa7521039a6", i14);
        fIb = b14;
        arzArr[i14] = b14;
        int i15 = i14 + 1;
        C5663aRz b15 = C5640aRc.m17844b(Outpost.class, "c11643b333ba651219dd6fce65d44e11", i15);
        dnO = b15;
        arzArr[i15] = b15;
        int i16 = i15 + 1;
        _m_fields = arzArr;
        C1634Xv.m11725a((Object[]) Station._m_fields, (Object[]) _m_fields);
        int i17 = Station._m_methodCount;
        C2491fm[] fmVarArr = new C2491fm[(i17 + 43)];
        C2491fm a = C4105zY.m41624a(Outpost.class, "cfa77f245ee2beb773566a453024b087", i17);
        _f_toString_0020_0028_0029Ljava_002flang_002fString_003b = a;
        fmVarArr[i17] = a;
        int i18 = i17 + 1;
        C2491fm a2 = C4105zY.m41624a(Outpost.class, "5742743ca1026d1ab5ab1d0e515f24e3", i18);
        fIc = a2;
        fmVarArr[i18] = a2;
        int i19 = i18 + 1;
        C2491fm a3 = C4105zY.m41624a(Outpost.class, "5f9660ed8ee541d03eec3db8ea3ec49f", i19);
        fId = a3;
        fmVarArr[i19] = a3;
        int i20 = i19 + 1;
        C2491fm a4 = C4105zY.m41624a(Outpost.class, "a14fdbeb6b27cf7a7ab479b9ab2c35e7", i20);
        fIe = a4;
        fmVarArr[i20] = a4;
        int i21 = i20 + 1;
        C2491fm a5 = C4105zY.m41624a(Outpost.class, "4b0105d10c2ed3c5f78e25479d2db77f", i21);
        fIf = a5;
        fmVarArr[i21] = a5;
        int i22 = i21 + 1;
        C2491fm a6 = C4105zY.m41624a(Outpost.class, "3de09f2bcf9497dc08e44e8587138757", i22);
        fIg = a6;
        fmVarArr[i22] = a6;
        int i23 = i22 + 1;
        C2491fm a7 = C4105zY.m41624a(Outpost.class, "8ac7770eaae380e55ae8179e7e0ce56f", i23);
        fIh = a7;
        fmVarArr[i23] = a7;
        int i24 = i23 + 1;
        C2491fm a8 = C4105zY.m41624a(Outpost.class, "f565823b3290c1bcf01b7c993bbd69c7", i24);
        fIi = a8;
        fmVarArr[i24] = a8;
        int i25 = i24 + 1;
        C2491fm a9 = C4105zY.m41624a(Outpost.class, "5799fac638a7a6a32de2582a816d233e", i25);
        f8937MH = a9;
        fmVarArr[i25] = a9;
        int i26 = i25 + 1;
        C2491fm a10 = C4105zY.m41624a(Outpost.class, "19527edfcb21d27a9bd954741ad9c025", i26);
        coI = a10;
        fmVarArr[i26] = a10;
        int i27 = i26 + 1;
        C2491fm a11 = C4105zY.m41624a(Outpost.class, "de0ae164c5241026572dda0050df039a", i27);
        fIj = a11;
        fmVarArr[i27] = a11;
        int i28 = i27 + 1;
        C2491fm a12 = C4105zY.m41624a(Outpost.class, "fb3e9d2696d884225818eb1819482311", i28);
        f8940Pf = a12;
        fmVarArr[i28] = a12;
        int i29 = i28 + 1;
        C2491fm a13 = C4105zY.m41624a(Outpost.class, "81454be930ab6c325cbb06a1f8d5a118", i29);
        brK = a13;
        fmVarArr[i29] = a13;
        int i30 = i29 + 1;
        C2491fm a14 = C4105zY.m41624a(Outpost.class, "2c36149cab9a613e22e42e7fce723a32", i30);
        fIk = a14;
        fmVarArr[i30] = a14;
        int i31 = i30 + 1;
        C2491fm a15 = C4105zY.m41624a(Outpost.class, "9d8dbd4964973bf2c7fcf6dbc3670219", i31);
        fIl = a15;
        fmVarArr[i31] = a15;
        int i32 = i31 + 1;
        C2491fm a16 = C4105zY.m41624a(Outpost.class, "ee7bcefd115adced368e12a3b79dd4bf", i32);
        fIm = a16;
        fmVarArr[i32] = a16;
        int i33 = i32 + 1;
        C2491fm a17 = C4105zY.m41624a(Outpost.class, "bc1a52da2803589c26f0d7bb958ebf08", i33);
        ayM = a17;
        fmVarArr[i33] = a17;
        int i34 = i33 + 1;
        C2491fm a18 = C4105zY.m41624a(Outpost.class, "65533c3703e824631371103a4a558117", i34);
        fIn = a18;
        fmVarArr[i34] = a18;
        int i35 = i34 + 1;
        C2491fm a19 = C4105zY.m41624a(Outpost.class, "2ef82a979ce115e6f066b78b0b164799", i35);
        dXR = a19;
        fmVarArr[i35] = a19;
        int i36 = i35 + 1;
        C2491fm a20 = C4105zY.m41624a(Outpost.class, "7c4a09b93c5083a40a815fe376824427", i36);
        dXQ = a20;
        fmVarArr[i36] = a20;
        int i37 = i36 + 1;
        C2491fm a21 = C4105zY.m41624a(Outpost.class, "5cf5454a3bfc27fc07b82282d40a5c9e", i37);
        dTf = a21;
        fmVarArr[i37] = a21;
        int i38 = i37 + 1;
        C2491fm a22 = C4105zY.m41624a(Outpost.class, "5fc1930ccacc89498199d7d07b001a5d", i38);
        fIo = a22;
        fmVarArr[i38] = a22;
        int i39 = i38 + 1;
        C2491fm a23 = C4105zY.m41624a(Outpost.class, "d16df91eb900d2fca8209587fd8d153a", i39);
        fIp = a23;
        fmVarArr[i39] = a23;
        int i40 = i39 + 1;
        C2491fm a24 = C4105zY.m41624a(Outpost.class, "c9bd84147e16b019621ded0f7fa041db", i40);
        fIq = a24;
        fmVarArr[i40] = a24;
        int i41 = i40 + 1;
        C2491fm a25 = C4105zY.m41624a(Outpost.class, "46275d2f6eb473ad90658c21b813b40b", i41);
        fIr = a25;
        fmVarArr[i41] = a25;
        int i42 = i41 + 1;
        C2491fm a26 = C4105zY.m41624a(Outpost.class, "b600800b3a7320ad73043fdee1892b75", i42);
        fIs = a26;
        fmVarArr[i42] = a26;
        int i43 = i42 + 1;
        C2491fm a27 = C4105zY.m41624a(Outpost.class, "8b850b2e240cd535290b4c1d0a1de41f", i43);
        fIt = a27;
        fmVarArr[i43] = a27;
        int i44 = i43 + 1;
        C2491fm a28 = C4105zY.m41624a(Outpost.class, "884d758f046e2a0c7d14a7c7f96f9d74", i44);
        fIu = a28;
        fmVarArr[i44] = a28;
        int i45 = i44 + 1;
        C2491fm a29 = C4105zY.m41624a(Outpost.class, "c6cc9e406a0b599e94e6893038bb1725", i45);
        _f_isEnabled_0020_0028_0029Z = a29;
        fmVarArr[i45] = a29;
        int i46 = i45 + 1;
        C2491fm a30 = C4105zY.m41624a(Outpost.class, "c5a157f40e40d7451029dbc947709394", i46);
        fIv = a30;
        fmVarArr[i46] = a30;
        int i47 = i46 + 1;
        C2491fm a31 = C4105zY.m41624a(Outpost.class, "9c8b6eb61307c454ffa403a888610025", i47);
        fIw = a31;
        fmVarArr[i47] = a31;
        int i48 = i47 + 1;
        C2491fm a32 = C4105zY.m41624a(Outpost.class, "c232c67452fe4b36decf7b1440866b5e", i48);
        fIx = a32;
        fmVarArr[i48] = a32;
        int i49 = i48 + 1;
        C2491fm a33 = C4105zY.m41624a(Outpost.class, "a86bd6580c9f0bee6b41ed0da9ef61a2", i49);
        fIy = a33;
        fmVarArr[i49] = a33;
        int i50 = i49 + 1;
        C2491fm a34 = C4105zY.m41624a(Outpost.class, "68dd75b8978ce56dfd961c03bd69210f", i50);
        coO = a34;
        fmVarArr[i50] = a34;
        int i51 = i50 + 1;
        C2491fm a35 = C4105zY.m41624a(Outpost.class, "fae5300d3051e357d53b96b8129e4477", i51);
        fIz = a35;
        fmVarArr[i51] = a35;
        int i52 = i51 + 1;
        C2491fm a36 = C4105zY.m41624a(Outpost.class, "ccc6619f35c1ed3b35efb59c75d14c68", i52);
        fIA = a36;
        fmVarArr[i52] = a36;
        int i53 = i52 + 1;
        C2491fm a37 = C4105zY.m41624a(Outpost.class, "ab85fefb1ef6de6c7e023e6b759b6fbe", i53);
        fIB = a37;
        fmVarArr[i53] = a37;
        int i54 = i53 + 1;
        C2491fm a38 = C4105zY.m41624a(Outpost.class, "ce095920c30c12cc7351b3fd2aa2e57f", i54);
        bqO = a38;
        fmVarArr[i54] = a38;
        int i55 = i54 + 1;
        C2491fm a39 = C4105zY.m41624a(Outpost.class, "ec259eaded108ef16d3d6b591026e9bc", i55);
        fIC = a39;
        fmVarArr[i55] = a39;
        int i56 = i55 + 1;
        C2491fm a40 = C4105zY.m41624a(Outpost.class, "01fb911ba28f1303b59eee7bd4ea77a1", i56);
        f8938MN = a40;
        fmVarArr[i56] = a40;
        int i57 = i56 + 1;
        C2491fm a41 = C4105zY.m41624a(Outpost.class, "e13158e4343844439fe67b967222af99", i57);
        f8936Lm = a41;
        fmVarArr[i57] = a41;
        int i58 = i57 + 1;
        C2491fm a42 = C4105zY.m41624a(Outpost.class, "72419fcaa94c9b8d6f5a90f375eccbdf", i58);
        coS = a42;
        fmVarArr[i58] = a42;
        int i59 = i58 + 1;
        C2491fm a43 = C4105zY.m41624a(Outpost.class, "343d768319e44e2fb9731133425ff478", i59);
        coG = a43;
        fmVarArr[i59] = a43;
        int i60 = i59 + 1;
        _m_methods = fmVarArr;
        C1634Xv.m11725a((Object[]) Station._m_methods, (Object[]) _m_methods);
        ___iScriptClass = aUO.m18566a(Outpost.class, C0556Hj.class, _m_fields, _m_methods);
    }

    /* renamed from: E */
    private void m37646E(C1556Wo wo) {
        throw new C6039afL();
    }

    /* renamed from: F */
    private void m37647F(C1556Wo wo) {
        throw new C6039afL();
    }

    /* renamed from: G */
    private void m37648G(long j) {
        throw new C6039afL();
    }

    /* renamed from: G */
    private void m37649G(C1556Wo wo) {
        throw new C6039afL();
    }

    /* renamed from: LY */
    private C2611hZ m37650LY() {
        return (C2611hZ) bFf().mo5608dq().mo3214p(ayJ);
    }

    /* renamed from: a */
    private void m37653a(aHK ahk) {
        throw new C6039afL();
    }

    @C0064Am(aul = "c9bd84147e16b019621ded0f7fa041db", aum = 0)
    @C5566aOg
    /* renamed from: a */
    private void m37654a(OutpostUpgrade axr) {
        throw new aWi(new aCE(this, fIq, new Object[]{axr}));
    }

    /* renamed from: a */
    private void m37655a(C2611hZ hZVar) {
        bFf().mo5608dq().mo3197f(ayJ, hZVar);
    }

    /* renamed from: a */
    private void m37656a(C3349b bVar) {
        bFf().mo5608dq().mo3197f(axd, bVar);
    }

    /* renamed from: a */
    private void m37657a(OutpostUpgradeStatus dVar) {
        bFf().mo5608dq().mo3197f(fIb, dVar);
    }

    @C0064Am(aul = "19527edfcb21d27a9bd954741ad9c025", aum = 0)
    @C5566aOg
    @C2499fr
    /* renamed from: al */
    private Storage m37660al(Player aku) {
        throw new aWi(new aCE(this, coI, new Object[]{aku}));
    }

    @C0064Am(aul = "343d768319e44e2fb9731133425ff478", aum = 0)
    private StationType azO() {
        return bYh();
    }

    /* renamed from: b */
    private void m37663b(OutpostStorage io) {
        bFf().mo5608dq().mo3197f(fHZ, io);
    }

    /* renamed from: b */
    private void m37664b(OutpostUpkeepPeriod rc) {
        bFf().mo5608dq().mo3197f(fIa, rc);
    }

    /* access modifiers changed from: private */
    @C5566aOg
    /* renamed from: b */
    public void m37665b(OutpostUpgrade axr) {
        switch (bFf().mo6893i(fIq)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, fIq, new Object[]{axr}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, fIq, new Object[]{axr}));
                break;
        }
        m37654a(axr);
    }

    /* renamed from: bA */
    private boolean m37667bA(Player aku) {
        switch (bFf().mo6893i(fIz)) {
            case 0:
                return false;
            case 2:
                return ((Boolean) bFf().mo5606d(new aCE(this, fIz, new Object[]{aku}))).booleanValue();
            case 3:
                bFf().mo5606d(new aCE(this, fIz, new Object[]{aku}));
                break;
        }
        return m37670bz(aku);
    }

    private Corporation bXM() {
        return (Corporation) bFf().mo5608dq().mo3214p(fHY);
    }

    private C3349b bXN() {
        return (C3349b) bFf().mo5608dq().mo3214p(axd);
    }

    private OutpostStorage bXO() {
        return (OutpostStorage) bFf().mo5608dq().mo3214p(fHZ);
    }

    private OutpostUpkeepPeriod bXP() {
        return (OutpostUpkeepPeriod) bFf().mo5608dq().mo3214p(fIa);
    }

    private OutpostUpgradeStatus bXQ() {
        return (OutpostUpgradeStatus) bFf().mo5608dq().mo3214p(fIb);
    }

    private boolean bXR() {
        return bFf().mo5608dq().mo3201h(dnO);
    }

    @C0064Am(aul = "5742743ca1026d1ab5ab1d0e515f24e3", aum = 0)
    @C6580apg
    private void bXS() {
        bFf().mo5600a((Class<? extends C4062yl>) C6360alU.C1935a.class, (C0495Gr) new aCE(this, fIc, new Object[0]));
    }

    @C0064Am(aul = "3de09f2bcf9497dc08e44e8587138757", aum = 0)
    @C5566aOg
    private void bXW() {
        throw new aWi(new aCE(this, fIg, new Object[0]));
    }

    @C0064Am(aul = "8ac7770eaae380e55ae8179e7e0ce56f", aum = 0)
    @C5566aOg
    private void bXY() {
        throw new aWi(new aCE(this, fIh, new Object[0]));
    }

    @C0064Am(aul = "f565823b3290c1bcf01b7c993bbd69c7", aum = 0)
    @C5566aOg
    private void bYa() {
        throw new aWi(new aCE(this, fIi, new Object[0]));
    }

    @C0064Am(aul = "d16df91eb900d2fca8209587fd8d153a", aum = 0)
    @C5566aOg
    @C2499fr
    private void bYk() {
        throw new aWi(new aCE(this, fIp, new Object[0]));
    }

    @C0064Am(aul = "c5a157f40e40d7451029dbc947709394", aum = 0)
    @C5566aOg
    private void bYu() {
        throw new aWi(new aCE(this, fIv, new Object[0]));
    }

    @C0064Am(aul = "9c8b6eb61307c454ffa403a888610025", aum = 0)
    @C5566aOg
    private void bYv() {
        throw new aWi(new aCE(this, fIw, new Object[0]));
    }

    private Outpost bYx() {
        switch (bFf().mo6893i(fIB)) {
            case 0:
                return null;
            case 2:
                return (Outpost) bFf().mo5606d(new aCE(this, fIB, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, fIB, new Object[0]));
                break;
        }
        return bYw();
    }

    @C0064Am(aul = "ec259eaded108ef16d3d6b591026e9bc", aum = 0)
    @C5566aOg
    private void bYy() {
        throw new aWi(new aCE(this, fIC, new Object[0]));
    }

    @C5566aOg
    private void bYz() {
        switch (bFf().mo6893i(fIC)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, fIC, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, fIC, new Object[0]));
                break;
        }
        bYy();
    }

    private int bep() {
        return ((OutpostType) getType()).bex();
    }

    private C3438ra beq() {
        return ((OutpostType) getType()).bez();
    }

    private aHK ber() {
        return ((OutpostType) getType()).beD();
    }

    private I18NString bes() {
        return ((OutpostType) getType()).beF();
    }

    private C1556Wo bet() {
        return ((OutpostType) getType()).beH();
    }

    private C1556Wo beu() {
        return ((OutpostType) getType()).beJ();
    }

    private C1556Wo bev() {
        return ((OutpostType) getType()).beL();
    }

    /* renamed from: bf */
    private void m37668bf(C3438ra raVar) {
        throw new C6039afL();
    }

    @C0064Am(aul = "5fc1930ccacc89498199d7d07b001a5d", aum = 0)
    @C5566aOg
    @C2499fr
    /* renamed from: c */
    private void m37672c(Item[] auqArr) {
        throw new aWi(new aCE(this, fIo, new Object[]{auqArr}));
    }

    /* renamed from: eA */
    private void m37674eA(boolean z) {
        bFf().mo5608dq().mo3153a(dnO, z);
    }

    @C0064Am(aul = "c232c67452fe4b36decf7b1440866b5e", aum = 0)
    @C5566aOg
    /* renamed from: eB */
    private void m37675eB(boolean z) {
        throw new aWi(new aCE(this, fIx, new Object[]{new Boolean(z)}));
    }

    /* renamed from: f */
    private boolean m37676f(C0437GE ge) {
        switch (bFf().mo6893i(fIA)) {
            case 0:
                return false;
            case 2:
                return ((Boolean) bFf().mo5606d(new aCE(this, fIA, new Object[]{ge}))).booleanValue();
            case 3:
                bFf().mo5606d(new aCE(this, fIA, new Object[]{ge}));
                break;
        }
        return m37673e(ge);
    }

    /* renamed from: iC */
    private void m37678iC(I18NString i18NString) {
        throw new C6039afL();
    }

    /* renamed from: k */
    private void m37679k(Corporation aPVar) {
        bFf().mo5608dq().mo3197f(fHY, aPVar);
    }

    @C0064Am(aul = "4b0105d10c2ed3c5f78e25479d2db77f", aum = 0)
    @C5566aOg
    /* renamed from: l */
    private void m37680l(Corporation aPVar) {
        throw new aWi(new aCE(this, fIf, new Object[]{aPVar}));
    }

    /* renamed from: l */
    private void m37681l(Collection collection, C0495Gr gr) {
        Iterator it = collection.iterator();
        while (it.hasNext()) {
            ((C6360alU.C1935a) it.next()).mo14685g(this);
        }
    }

    /* renamed from: lw */
    private void m37682lw(int i) {
        throw new C6039afL();
    }

    @C0064Am(aul = "e13158e4343844439fe67b967222af99", aum = 0)
    @C5566aOg
    /* renamed from: qU */
    private void m37683qU() {
        throw new aWi(new aCE(this, f8936Lm, new Object[0]));
    }

    /* renamed from: rU */
    private long m37685rU() {
        return ((OutpostType) getType()).mo3587rZ();
    }

    /* renamed from: Mb */
    public C2611hZ mo21372Mb() {
        switch (bFf().mo6893i(ayM)) {
            case 0:
                return null;
            case 2:
                return (C2611hZ) bFf().mo5606d(new aCE(this, ayM, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, ayM, new Object[0]));
                break;
        }
        return m37651Ma();
    }

    /* renamed from: S */
    public void mo10S() {
        super.mo10S();
    }

    /* renamed from: T */
    public C6494any mo11T() {
        return ___iScriptClass;
    }

    /* renamed from: U */
    public C2491fm[] mo12U() {
        return _m_methods;
    }

    /* renamed from: W */
    public Object mo13W() {
        return new C0556Hj(this);
    }

    /* renamed from: a */
    public Object mo14a(C0495Gr gr) {
        Object[] args = gr.getArgs();
        switch (gr.mo2417hq() - Station._m_methodCount) {
            case 0:
                return m37662au();
            case 1:
                bXS();
                return null;
            case 2:
                return bXU();
            case 3:
                m37666b((C3349b) args[0]);
                return null;
            case 4:
                m37680l((Corporation) args[0]);
                return null;
            case 5:
                bXW();
                return null;
            case 6:
                bXY();
                return null;
            case 7:
                bYa();
                return null;
            case 8:
                return m37671c((Character) args[0]);
            case 9:
                return m37660al((Player) args[0]);
            case 10:
                return m37669bx((Player) args[0]);
            case 11:
                return m37687uV();
            case 12:
                return ahP();
            case 13:
                return bYc();
            case 14:
                return new Integer(bYe());
            case 15:
                return bYg();
            case 16:
                return m37651Ma();
            case 17:
                return bYi();
            case 18:
                return new Integer(bqY());
            case 19:
                return new Boolean(bqW());
            case 20:
                return new Boolean(boB());
            case 21:
                m37672c((Item[]) args[0]);
                return null;
            case 22:
                bYk();
                return null;
            case 23:
                m37654a((OutpostUpgrade) args[0]);
                return null;
            case 24:
                return bYm();
            case 25:
                return new Long(bYo());
            case 26:
                return new Long(bYq());
            case 27:
                return bYs();
            case 28:
                return new Boolean(m37686rx());
            case 29:
                bYu();
                return null;
            case 30:
                bYv();
                return null;
            case 31:
                m37675eB(((Boolean) args[0]).booleanValue());
                return null;
            case 32:
                m37677hr(((Long) args[0]).longValue());
                return null;
            case 33:
                return new Boolean(m37659a((C0437GE) args[0], (Player) args[1]));
            case 34:
                return new Boolean(m37670bz((Player) args[0]));
            case 35:
                return new Boolean(m37673e((C0437GE) args[0]));
            case 36:
                return bYw();
            case 37:
                ahs();
                return null;
            case 38:
                bYy();
                return null;
            case 39:
                return m37684rO();
            case 40:
                m37683qU();
                return null;
            case 41:
                m37661ap((Player) args[0]);
                return null;
            case 42:
                return azO();
            default:
                return super.mo14a(gr);
        }
    }

    /* renamed from: a */
    public void mo15a(Collection collection, C0495Gr gr) {
        switch (gr.mo2417hq() - Station._m_methodCount) {
            case 1:
                m37681l(collection, gr);
                return;
            default:
                super.mo15a(collection, gr);
                return;
        }
    }

    public String ahQ() {
        switch (bFf().mo6893i(brK)) {
            case 0:
                return null;
            case 2:
                return (String) bFf().mo5606d(new aCE(this, brK, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, brK, new Object[0]));
                break;
        }
        return ahP();
    }

    /* access modifiers changed from: protected */
    @ClientOnly
    public void aht() {
        switch (bFf().mo6893i(bqO)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, bqO, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, bqO, new Object[0]));
                break;
        }
        ahs();
    }

    @C5566aOg
    @C2499fr
    /* renamed from: am */
    public Storage mo602am(Player aku) {
        switch (bFf().mo6893i(coI)) {
            case 0:
                return null;
            case 2:
                return (Storage) bFf().mo5606d(new aCE(this, coI, new Object[]{aku}));
            case 3:
                bFf().mo5606d(new aCE(this, coI, new Object[]{aku}));
                break;
        }
        return m37660al(aku);
    }

    /* renamed from: aq */
    public void mo603aq(Player aku) {
        switch (bFf().mo6893i(coS)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, coS, new Object[]{aku}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, coS, new Object[]{aku}));
                break;
        }
        m37661ap(aku);
    }

    public /* bridge */ /* synthetic */ StationType azP() {
        switch (bFf().mo6893i(coG)) {
            case 0:
                return null;
            case 2:
                return (StationType) bFf().mo5606d(new aCE(this, coG, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, coG, new Object[0]));
                break;
        }
        return azO();
    }

    /* renamed from: b */
    public boolean mo628b(C0437GE ge, Player aku) {
        switch (bFf().mo6893i(coO)) {
            case 0:
                return false;
            case 2:
                return ((Boolean) bFf().mo5606d(new aCE(this, coO, new Object[]{ge, aku}))).booleanValue();
            case 3:
                bFf().mo5606d(new aCE(this, coO, new Object[]{ge, aku}));
                break;
        }
        return m37659a(ge, aku);
    }

    @C6580apg
    public void bXT() {
        switch (bFf().mo6893i(fIc)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, fIc, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, fIc, new Object[0]));
                break;
        }
        bXS();
    }

    public C3349b bXV() {
        switch (bFf().mo6893i(fId)) {
            case 0:
                return null;
            case 2:
                return (C3349b) bFf().mo5606d(new aCE(this, fId, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, fId, new Object[0]));
                break;
        }
        return bXU();
    }

    @C5566aOg
    public void bXX() {
        switch (bFf().mo6893i(fIg)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, fIg, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, fIg, new Object[0]));
                break;
        }
        bXW();
    }

    @C5566aOg
    public void bXZ() {
        switch (bFf().mo6893i(fIh)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, fIh, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, fIh, new Object[0]));
                break;
        }
        bXY();
    }

    @C5566aOg
    public void bYb() {
        switch (bFf().mo6893i(fIi)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, fIi, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, fIi, new Object[0]));
                break;
        }
        bYa();
    }

    public Corporation bYd() {
        switch (bFf().mo6893i(fIk)) {
            case 0:
                return null;
            case 2:
                return (Corporation) bFf().mo5606d(new aCE(this, fIk, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, fIk, new Object[0]));
                break;
        }
        return bYc();
    }

    public int bYf() {
        switch (bFf().mo6893i(fIl)) {
            case 0:
                return 0;
            case 2:
                return ((Integer) bFf().mo5606d(new aCE(this, fIl, new Object[0]))).intValue();
            case 3:
                bFf().mo5606d(new aCE(this, fIl, new Object[0]));
                break;
        }
        return bYe();
    }

    public OutpostType bYh() {
        switch (bFf().mo6893i(fIm)) {
            case 0:
                return null;
            case 2:
                return (OutpostType) bFf().mo5606d(new aCE(this, fIm, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, fIm, new Object[0]));
                break;
        }
        return bYg();
    }

    public OutpostUpgradeStatus bYj() {
        switch (bFf().mo6893i(fIn)) {
            case 0:
                return null;
            case 2:
                return (OutpostUpgradeStatus) bFf().mo5606d(new aCE(this, fIn, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, fIn, new Object[0]));
                break;
        }
        return bYi();
    }

    @C5566aOg
    @C2499fr
    public void bYl() {
        switch (bFf().mo6893i(fIp)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, fIp, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, fIp, new Object[0]));
                break;
        }
        bYk();
    }

    public String bYn() {
        switch (bFf().mo6893i(fIr)) {
            case 0:
                return null;
            case 2:
                return (String) bFf().mo5606d(new aCE(this, fIr, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, fIr, new Object[0]));
                break;
        }
        return bYm();
    }

    public long bYp() {
        switch (bFf().mo6893i(fIs)) {
            case 0:
                return 0;
            case 2:
                return ((Long) bFf().mo5606d(new aCE(this, fIs, new Object[0]))).longValue();
            case 3:
                bFf().mo5606d(new aCE(this, fIs, new Object[0]));
                break;
        }
        return bYo();
    }

    public long bYr() {
        switch (bFf().mo6893i(fIt)) {
            case 0:
                return 0;
            case 2:
                return ((Long) bFf().mo5606d(new aCE(this, fIt, new Object[0]))).longValue();
            case 3:
                bFf().mo5606d(new aCE(this, fIt, new Object[0]));
                break;
        }
        return bYq();
    }

    public Collection<OutpostLevelInfo.OutpostUpkeepItem> bYt() {
        switch (bFf().mo6893i(fIu)) {
            case 0:
                return null;
            case 2:
                return (Collection) bFf().mo5606d(new aCE(this, fIu, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, fIu, new Object[0]));
                break;
        }
        return bYs();
    }

    /* access modifiers changed from: protected */
    public boolean boC() {
        switch (bFf().mo6893i(dTf)) {
            case 0:
                return false;
            case 2:
                return ((Boolean) bFf().mo5606d(new aCE(this, dTf, new Object[0]))).booleanValue();
            case 3:
                bFf().mo5606d(new aCE(this, dTf, new Object[0]));
                break;
        }
        return boB();
    }

    public boolean bqX() {
        switch (bFf().mo6893i(dXQ)) {
            case 0:
                return false;
            case 2:
                return ((Boolean) bFf().mo5606d(new aCE(this, dXQ, new Object[0]))).booleanValue();
            case 3:
                bFf().mo5606d(new aCE(this, dXQ, new Object[0]));
                break;
        }
        return bqW();
    }

    public int bqZ() {
        switch (bFf().mo6893i(dXR)) {
            case 0:
                return 0;
            case 2:
                return ((Integer) bFf().mo5606d(new aCE(this, dXR, new Object[0]))).intValue();
            case 3:
                bFf().mo5606d(new aCE(this, dXR, new Object[0]));
                break;
        }
        return bqY();
    }

    /* renamed from: by */
    public OutpostStorage mo21390by(Player aku) {
        switch (bFf().mo6893i(fIj)) {
            case 0:
                return null;
            case 2:
                return (OutpostStorage) bFf().mo5606d(new aCE(this, fIj, new Object[]{aku}));
            case 3:
                bFf().mo5606d(new aCE(this, fIj, new Object[]{aku}));
                break;
        }
        return m37669bx(aku);
    }

    /* access modifiers changed from: protected */
    /* renamed from: c */
    public void mo21391c(C3349b bVar) {
        switch (bFf().mo6893i(fIe)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, fIe, new Object[]{bVar}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, fIe, new Object[]{bVar}));
                break;
        }
        m37666b(bVar);
    }

    /* renamed from: c */
    public C5663aRz[] mo25c() {
        return _m_fields;
    }

    /* renamed from: d */
    public C0286Dh.C0287a mo634d(Character acx) {
        switch (bFf().mo6893i(f8937MH)) {
            case 0:
                return null;
            case 2:
                return (C0286Dh.C0287a) bFf().mo5606d(new aCE(this, f8937MH, new Object[]{acx}));
            case 3:
                bFf().mo5606d(new aCE(this, f8937MH, new Object[]{acx}));
                break;
        }
        return m37671c(acx);
    }

    @C5566aOg
    @C2499fr
    /* renamed from: d */
    public void mo21393d(Item... auqArr) {
        switch (bFf().mo6893i(fIo)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, fIo, new Object[]{auqArr}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, fIo, new Object[]{auqArr}));
                break;
        }
        m37672c(auqArr);
    }

    /* access modifiers changed from: package-private */
    @C5566aOg
    public void disable() {
        switch (bFf().mo6893i(fIw)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, fIw, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, fIw, new Object[0]));
                break;
        }
        bYv();
    }

    /* access modifiers changed from: package-private */
    @C5566aOg
    /* renamed from: eC */
    public void mo21395eC(boolean z) {
        switch (bFf().mo6893i(fIx)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, fIx, new Object[]{new Boolean(z)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, fIx, new Object[]{new Boolean(z)}));
                break;
        }
        m37675eB(z);
    }

    /* access modifiers changed from: package-private */
    @C5566aOg
    public void enable() {
        switch (bFf().mo6893i(fIv)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, fIv, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, fIv, new Object[0]));
                break;
        }
        bYu();
    }

    public String getName() {
        switch (bFf().mo6893i(f8940Pf)) {
            case 0:
                return null;
            case 2:
                return (String) bFf().mo5606d(new aCE(this, f8940Pf, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f8940Pf, new Object[0]));
                break;
        }
        return m37687uV();
    }

    /* renamed from: hs */
    public void mo21397hs(long j) {
        switch (bFf().mo6893i(fIy)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, fIy, new Object[]{new Long(j)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, fIy, new Object[]{new Long(j)}));
                break;
        }
        m37677hr(j);
    }

    public boolean isEnabled() {
        switch (bFf().mo6893i(_f_isEnabled_0020_0028_0029Z)) {
            case 0:
                return false;
            case 2:
                return ((Boolean) bFf().mo5606d(new aCE(this, _f_isEnabled_0020_0028_0029Z, new Object[0]))).booleanValue();
            case 3:
                bFf().mo5606d(new aCE(this, _f_isEnabled_0020_0028_0029Z, new Object[0]));
                break;
        }
        return m37686rx();
    }

    @C5566aOg
    /* renamed from: m */
    public void mo21399m(Corporation aPVar) {
        switch (bFf().mo6893i(fIf)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, fIf, new Object[]{aPVar}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, fIf, new Object[]{aPVar}));
                break;
        }
        m37680l(aPVar);
    }

    @C5566aOg
    /* renamed from: qV */
    public void mo656qV() {
        switch (bFf().mo6893i(f8936Lm)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f8936Lm, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f8936Lm, new Object[0]));
                break;
        }
        m37683qU();
    }

    /* renamed from: rP */
    public I18NString mo195rP() {
        switch (bFf().mo6893i(f8938MN)) {
            case 0:
                return null;
            case 2:
                return (I18NString) bFf().mo5606d(new aCE(this, f8938MN, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f8938MN, new Object[0]));
                break;
        }
        return m37684rO();
    }

    public String toString() {
        switch (bFf().mo6893i(_f_toString_0020_0028_0029Ljava_002flang_002fString_003b)) {
            case 0:
                return null;
            case 2:
                return (String) bFf().mo5606d(new aCE(this, _f_toString_0020_0028_0029Ljava_002flang_002fString_003b, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, _f_toString_0020_0028_0029Ljava_002flang_002fString_003b, new Object[0]));
                break;
        }
        return m37662au();
    }

    @C0064Am(aul = "cfa77f245ee2beb773566a453024b087", aum = 0)
    /* renamed from: au */
    private String m37662au() {
        return "Outpost: [" + getName() + "]";
    }

    /* renamed from: d */
    public void mo21392d(OutpostType kz) {
        super.mo642e((StationType) kz);
        m37656a(C3349b.FOR_SALE);
        OutpostUpkeepPeriod rc = (OutpostUpkeepPeriod) bFf().mo6865M(OutpostUpkeepPeriod.class);
        rc.mo5257f(this);
        m37664b(rc);
        m37655a(C2611hZ.m32750Ac());
        OutpostStorage io = (OutpostStorage) bFf().mo6865M(OutpostStorage.class);
        io.mo2876f(this);
        m37663b(io);
        bXO().mo2877fA(bYh().mo5105e(m37650LY()).cDd());
        m37657a((OutpostUpgradeStatus) null);
        enable();
    }

    @C0064Am(aul = "5f9660ed8ee541d03eec3db8ea3ec49f", aum = 0)
    private C3349b bXU() {
        return bXN();
    }

    @C0064Am(aul = "a14fdbeb6b27cf7a7ab479b9ab2c35e7", aum = 0)
    /* renamed from: b */
    private void m37666b(C3349b bVar) {
        m37656a(bVar);
        bXT();
    }

    @C0064Am(aul = "5799fac638a7a6a32de2582a816d233e", aum = 0)
    /* renamed from: c */
    private C0286Dh.C0287a m37671c(Character acx) {
        if (!(acx instanceof Player)) {
            return C0286Dh.C0287a.ERROR_CORP_EXCLUSIVE;
        }
        if (bXN() != C3349b.FOR_SALE && ((Player) acx).bYd() != bXM()) {
            return C0286Dh.C0287a.ERROR_CORP_EXCLUSIVE;
        }
        try {
            return mo654o(acx);
        } catch (C6046afS e) {
            throw new C5475aKt();
        }
    }

    @C0064Am(aul = "de0ae164c5241026572dda0050df039a", aum = 0)
    /* renamed from: bx */
    private OutpostStorage m37669bx(Player aku) {
        if (bXN() == C3349b.SOLD && bXM() != null && aku.bYd() == bXM()) {
            return bXO();
        }
        return null;
    }

    @C0064Am(aul = "fb3e9d2696d884225818eb1819482311", aum = 0)
    /* renamed from: uV */
    private String m37687uV() {
        NLSOutpost aci = (NLSOutpost) ala().aIY().mo6310c(NLSManager.C1472a.OUTPOST);
        C3349b bXV = bXV();
        if (bXV == C3349b.FOR_SALE) {
            return aci.bPO().get();
        }
        if (bXV == C3349b.RESERVED) {
            return aci.bPM().get();
        }
        if (bXV == C3349b.SOLD) {
            return bXM().getName();
        }
        return super.getName();
    }

    @C0064Am(aul = "81454be930ab6c325cbb06a1f8d5a118", aum = 0)
    private String ahP() {
        return String.valueOf(bYh().beF().get()) + ": " + getName();
    }

    @C0064Am(aul = "2c36149cab9a613e22e42e7fce723a32", aum = 0)
    private Corporation bYc() {
        return bXM();
    }

    @C0064Am(aul = "9d8dbd4964973bf2c7fcf6dbc3670219", aum = 0)
    private int bYe() {
        int i = 0;
        C2611hZ[] values = C2611hZ.values();
        int i2 = 0;
        while (true) {
            int i3 = i;
            if (i2 >= values.length) {
                return i3;
            }
            C2611hZ hZVar = values[i2];
            i = bYh().mo5106g(hZVar).cEO() + i3;
            if (hZVar == mo21372Mb()) {
                return i;
            }
            i2++;
        }
    }

    @C0064Am(aul = "ee7bcefd115adced368e12a3b79dd4bf", aum = 0)
    private OutpostType bYg() {
        return (OutpostType) super.azP();
    }

    @C0064Am(aul = "bc1a52da2803589c26f0d7bb958ebf08", aum = 0)
    /* renamed from: Ma */
    private C2611hZ m37651Ma() {
        return m37650LY();
    }

    @C0064Am(aul = "65533c3703e824631371103a4a558117", aum = 0)
    private OutpostUpgradeStatus bYi() {
        return bXQ();
    }

    @C0064Am(aul = "2ef82a979ce115e6f066b78b0b164799", aum = 0)
    private int bqY() {
        return bXP().bqZ();
    }

    @C0064Am(aul = "7c4a09b93c5083a40a815fe376824427", aum = 0)
    private boolean bqW() {
        return bXP().bqX();
    }

    @C0064Am(aul = "5cf5454a3bfc27fc07b82282d40a5c9e", aum = 0)
    private boolean boB() {
        return false;
    }

    @C0064Am(aul = "46275d2f6eb473ad90658c21b813b40b", aum = 0)
    private String bYm() {
        return bYh().mo5106g(m37650LY()).cET().get();
    }

    @C0064Am(aul = "b600800b3a7320ad73043fdee1892b75", aum = 0)
    private long bYo() {
        if (m37650LY().mo19281Ab()) {
            return 0;
        }
        return bYh().mo5105e(m37650LY().mo19280Aa()).cDh();
    }

    @C0064Am(aul = "8b850b2e240cd535290b4c1d0a1de41f", aum = 0)
    private long bYq() {
        return bYh().mo5106g(m37650LY()).cEQ();
    }

    @C0064Am(aul = "884d758f046e2a0c7d14a7c7f96f9d74", aum = 0)
    private Collection<OutpostLevelInfo.OutpostUpkeepItem> bYs() {
        return Collections.unmodifiableCollection(bYh().mo5106g(m37650LY()).get());
    }

    @C0064Am(aul = "c6cc9e406a0b599e94e6893038bb1725", aum = 0)
    /* renamed from: rx */
    private boolean m37686rx() {
        return !bXR();
    }

    @C0064Am(aul = "a86bd6580c9f0bee6b41ed0da9ef61a2", aum = 0)
    /* renamed from: hr */
    private void m37677hr(long j) {
        bXP().mo5258fz(j);
    }

    @C0064Am(aul = "68dd75b8978ce56dfd961c03bd69210f", aum = 0)
    /* renamed from: a */
    private boolean m37659a(C0437GE ge, Player aku) {
        if (!m37667bA(aku)) {
            return false;
        }
        if (ge == C0437GE.CLONING) {
            if (azL() != null) {
                return m37676f(ge);
            }
            return false;
        } else if (ge != C0437GE.CORP_STORAGE || mo21390by(aku) == null) {
            return false;
        } else {
            return m37676f(ge);
        }
    }

    @C0064Am(aul = "fae5300d3051e357d53b96b8129e4477", aum = 0)
    /* renamed from: bz */
    private boolean m37670bz(Player aku) {
        Corporation bYd;
        if (aku.bhE() == this && (bYd = aku.bYd()) != null && bYd == bXM() && bYd.mo10707Qy().contains(this) && bXV() == C3349b.SOLD) {
            return true;
        }
        return false;
    }

    @C0064Am(aul = "ccc6619f35c1ed3b35efb59c75d14c68", aum = 0)
    /* renamed from: e */
    private boolean m37673e(C0437GE ge) {
        OutpostLevelFeature d = bYh().mo5104d(ge);
        if (!d.mo20446Md() && mo21372Mb().ordinal() >= d.mo20445Mb().ordinal()) {
            return true;
        }
        return false;
    }

    @C0064Am(aul = "ab85fefb1ef6de6c7e023e6b759b6fbe", aum = 0)
    private Outpost bYw() {
        return this;
    }

    @C0064Am(aul = "ce095920c30c12cc7351b3fd2aa2e57f", aum = 0)
    @ClientOnly
    private void ahs() {
        super.aht();
        if (this.hks != null) {
            Corporation bYd = bYx().bYd();
            if (bYd != null && bYx().bXV() == C3349b.SOLD) {
                C2403eu.m30125a(bYd.mo10706Qu().getHandle(), bYx().bYh().mo3521Nu().getHandle(), this.hks);
                return;
            }
            return;
        }
        logger.error("Missing asset for our outpost " + toString());
    }

    @C0064Am(aul = "01fb911ba28f1303b59eee7bd4ea77a1", aum = 0)
    /* renamed from: rO */
    private I18NString m37684rO() {
        return new I18NString(ahQ());
    }

    @C0064Am(aul = "72419fcaa94c9b8d6f5a90f375eccbdf", aum = 0)
    /* renamed from: ap */
    private void m37661ap(Player aku) {
        super.mo603aq(aku);
        if (bYd() != null) {
            bYd().push();
            if (bYd().mo10706Qu() != null) {
                bYd().mo10706Qu().push();
            }
        }
    }

    /* renamed from: a.qZ$a */
    public enum C3348a {
        MONEY_TRANSFER,
        NO_UPKEEP_ITEM
    }

    /* renamed from: a.qZ$b */
    /* compiled from: a */
    public enum C3349b {
        FOR_SALE,
        RESERVED,
        SOLD
    }

    @C6546aoy
    /* renamed from: a.qZ$c */
    /* compiled from: a */
    public static class C3350c extends C5953add {
        private C3348a gxC;
        private OutpostLevelInfo.OutpostUpkeepItem gxD;

        C3350c(C3348a aVar) {
            this(aVar, (OutpostLevelInfo.OutpostUpkeepItem) null);
        }

        C3350c(C3348a aVar, OutpostLevelInfo.OutpostUpkeepItem aVar2) {
            this.gxC = aVar;
            this.gxD = aVar2;
        }

        public C3348a cuS() {
            return this.gxC;
        }

        public OutpostLevelInfo.OutpostUpkeepItem cuT() {
            return this.gxD;
        }

        public void readExternal(ObjectInput objectInput) {
            this.gxC = C3348a.values()[objectInput.readInt()];
            this.gxD = (OutpostLevelInfo.OutpostUpkeepItem) objectInput.readObject();
        }

        public void writeExternal(ObjectOutput objectOutput) {
            objectOutput.writeInt(this.gxC.ordinal());
            objectOutput.writeObject(this.gxD);
        }
    }

    @C6485anp
    @C5511aMd
    /* renamed from: a.qZ$d */
    /* compiled from: a */
    public class OutpostUpgradeStatus extends TaskletImpl implements C1616Xf {

        /* renamed from: JD */
        public static final C2491fm f8942JD = null;
        public static final int _m_fieldCount = 0;
        public static final C5663aRz[] _m_fields = null;
        public static final int _m_methodCount = 0;
        public static final C2491fm[] _m_methods = null;
        /* renamed from: aT */
        public static final C5663aRz f8943aT = null;
        public static final C2491fm dTf = null;
        public static final C5663aRz dna = null;
        public static final C5663aRz iNO = null;
        public static final C5663aRz iNP = null;
        public static final C5663aRz iNQ = null;
        public static final C2491fm iNR = null;
        public static final C2491fm iNS = null;
        public static final C2491fm iNT = null;
        public static final C2491fm iNU = null;
        public static final long serialVersionUID = 0;
        public static C6494any ___iScriptClass;
        @C0064Am(aul = "fd2b6ec7ace154f6c723eba9b62a8fec", aum = 4)
        static /* synthetic */ Outpost esA;
        @C0064Am(aul = "65f91218137239a880edc1b4c9eebc79", aum = 0)
        private static OutpostUpgrade esx;
        @C0064Am(aul = "0334d5ffc40d8d20238dbbb8f5805cfc", aum = 1)
        private static float esy;
        @C0064Am(aul = "293d470ab2fd60432cc4783f252df625", aum = 2)
        private static float esz;
        @C0064Am(aul = "464dc2dcc451130806a1de1602ccfb54", aum = 3)
        private static boolean stopped;

        static {
            m37709V();
        }

        public OutpostUpgradeStatus() {
            super((C5540aNg) null);
            super.mo10S();
        }

        public OutpostUpgradeStatus(C5540aNg ang) {
            super(ang);
        }

        OutpostUpgradeStatus(Outpost qZVar, Outpost qZVar2, OutpostUpgrade axr) {
            super((C5540aNg) null);
            super._m_script_init(qZVar, qZVar2, axr);
        }

        /* renamed from: V */
        static void m37709V() {
            serialVersionUID = (long) 1;
            _m_fieldCount = TaskletImpl._m_fieldCount + 5;
            _m_methodCount = TaskletImpl._m_methodCount + 6;
            int i = TaskletImpl._m_fieldCount;
            C5663aRz[] arzArr = new C5663aRz[(i + 5)];
            C5663aRz b = C5640aRc.m17844b(OutpostUpgradeStatus.class, "65f91218137239a880edc1b4c9eebc79", i);
            dna = b;
            arzArr[i] = b;
            int i2 = i + 1;
            C5663aRz b2 = C5640aRc.m17844b(OutpostUpgradeStatus.class, "0334d5ffc40d8d20238dbbb8f5805cfc", i2);
            iNO = b2;
            arzArr[i2] = b2;
            int i3 = i2 + 1;
            C5663aRz b3 = C5640aRc.m17844b(OutpostUpgradeStatus.class, "293d470ab2fd60432cc4783f252df625", i3);
            iNP = b3;
            arzArr[i3] = b3;
            int i4 = i3 + 1;
            C5663aRz b4 = C5640aRc.m17844b(OutpostUpgradeStatus.class, "464dc2dcc451130806a1de1602ccfb54", i4);
            iNQ = b4;
            arzArr[i4] = b4;
            int i5 = i4 + 1;
            C5663aRz b5 = C5640aRc.m17844b(OutpostUpgradeStatus.class, "fd2b6ec7ace154f6c723eba9b62a8fec", i5);
            f8943aT = b5;
            arzArr[i5] = b5;
            int i6 = i5 + 1;
            _m_fields = arzArr;
            C1634Xv.m11725a((Object[]) TaskletImpl._m_fields, (Object[]) _m_fields);
            int i7 = TaskletImpl._m_methodCount;
            C2491fm[] fmVarArr = new C2491fm[(i7 + 6)];
            C2491fm a = C4105zY.m41624a(OutpostUpgradeStatus.class, "7f1e9c53fe912927a374d4d6f3500415", i7);
            iNR = a;
            fmVarArr[i7] = a;
            int i8 = i7 + 1;
            C2491fm a2 = C4105zY.m41624a(OutpostUpgradeStatus.class, "4d69ba782200849c377df30c09fda1f8", i8);
            iNS = a2;
            fmVarArr[i8] = a2;
            int i9 = i8 + 1;
            C2491fm a3 = C4105zY.m41624a(OutpostUpgradeStatus.class, "c96226eb1caaab6f2efd4be03c4b8464", i9);
            iNT = a3;
            fmVarArr[i9] = a3;
            int i10 = i9 + 1;
            C2491fm a4 = C4105zY.m41624a(OutpostUpgradeStatus.class, "72a70b178df9672551bced754491abf6", i10);
            iNU = a4;
            fmVarArr[i10] = a4;
            int i11 = i10 + 1;
            C2491fm a5 = C4105zY.m41624a(OutpostUpgradeStatus.class, "48913b8f823299a48187172cc885c002", i11);
            f8942JD = a5;
            fmVarArr[i11] = a5;
            int i12 = i11 + 1;
            C2491fm a6 = C4105zY.m41624a(OutpostUpgradeStatus.class, "7e748c14ed167baa0f3f84ae8be334ba", i12);
            dTf = a6;
            fmVarArr[i12] = a6;
            int i13 = i12 + 1;
            _m_methods = fmVarArr;
            C1634Xv.m11725a((Object[]) TaskletImpl._m_methods, (Object[]) _m_methods);
            ___iScriptClass = aUO.m18566a(OutpostUpgradeStatus.class, C1486Vs.class, _m_fields, _m_methods);
        }

        /* renamed from: c */
        private void m37710c(OutpostUpgrade axr) {
            bFf().mo5608dq().mo3197f(dna, axr);
        }

        private OutpostUpgrade dvl() {
            return (OutpostUpgrade) bFf().mo5608dq().mo3214p(dna);
        }

        private float dvm() {
            return bFf().mo5608dq().mo3211m(iNO);
        }

        private float dvn() {
            return bFf().mo5608dq().mo3211m(iNP);
        }

        private boolean dvo() {
            return bFf().mo5608dq().mo3201h(iNQ);
        }

        private Outpost dvp() {
            return (Outpost) bFf().mo5608dq().mo3214p(f8943aT);
        }

        private void dvt() {
            switch (bFf().mo6893i(iNS)) {
                case 0:
                    return;
                case 2:
                    bFf().mo5606d(new aCE(this, iNS, new Object[0]));
                    return;
                case 3:
                    bFf().mo5606d(new aCE(this, iNS, new Object[0]));
                    break;
            }
            dvs();
        }

        /* renamed from: i */
        private void m37711i(Outpost qZVar) {
            bFf().mo5608dq().mo3197f(f8943aT, qZVar);
        }

        /* renamed from: kj */
        private void m37712kj(boolean z) {
            bFf().mo5608dq().mo3153a(iNQ, z);
        }

        /* renamed from: oA */
        private void m37713oA(float f) {
            bFf().mo5608dq().mo3150a(iNO, f);
        }

        /* renamed from: oB */
        private void m37714oB(float f) {
            bFf().mo5608dq().mo3150a(iNP, f);
        }

        /* renamed from: S */
        public void mo10S() {
            super.mo10S();
        }

        /* renamed from: T */
        public C6494any mo11T() {
            return ___iScriptClass;
        }

        /* renamed from: U */
        public C2491fm[] mo12U() {
            return _m_methods;
        }

        /* renamed from: W */
        public Object mo13W() {
            return new C1486Vs(this);
        }

        /* renamed from: a */
        public Object mo14a(C0495Gr gr) {
            gr.getArgs();
            switch (gr.mo2417hq() - TaskletImpl._m_methodCount) {
                case 0:
                    dvq();
                    return null;
                case 1:
                    dvs();
                    return null;
                case 2:
                    return new Integer(dvu());
                case 3:
                    dvw();
                    return null;
                case 4:
                    m37715pi();
                    return null;
                case 5:
                    return new Boolean(boB());
                default:
                    return super.mo14a(gr);
            }
        }

        /* renamed from: a */
        public void mo15a(Collection collection, C0495Gr gr) {
            super.mo15a(collection, gr);
        }

        /* access modifiers changed from: protected */
        public boolean boC() {
            switch (bFf().mo6893i(dTf)) {
                case 0:
                    return false;
                case 2:
                    return ((Boolean) bFf().mo5606d(new aCE(this, dTf, new Object[0]))).booleanValue();
                case 3:
                    bFf().mo5606d(new aCE(this, dTf, new Object[0]));
                    break;
            }
            return boB();
        }

        /* renamed from: c */
        public C5663aRz[] mo25c() {
            return _m_fields;
        }

        /* access modifiers changed from: package-private */
        public void dvr() {
            switch (bFf().mo6893i(iNR)) {
                case 0:
                    return;
                case 2:
                    bFf().mo5606d(new aCE(this, iNR, new Object[0]));
                    return;
                case 3:
                    bFf().mo5606d(new aCE(this, iNR, new Object[0]));
                    break;
            }
            dvq();
        }

        public int dvv() {
            switch (bFf().mo6893i(iNT)) {
                case 0:
                    return 0;
                case 2:
                    return ((Integer) bFf().mo5606d(new aCE(this, iNT, new Object[0]))).intValue();
                case 3:
                    bFf().mo5606d(new aCE(this, iNT, new Object[0]));
                    break;
            }
            return dvu();
        }

        /* access modifiers changed from: package-private */
        public void dvx() {
            switch (bFf().mo6893i(iNU)) {
                case 0:
                    return;
                case 2:
                    bFf().mo5606d(new aCE(this, iNU, new Object[0]));
                    return;
                case 3:
                    bFf().mo5606d(new aCE(this, iNU, new Object[0]));
                    break;
            }
            dvw();
        }

        /* access modifiers changed from: protected */
        /* renamed from: pj */
        public void mo667pj() {
            switch (bFf().mo6893i(f8942JD)) {
                case 0:
                    return;
                case 2:
                    bFf().mo5606d(new aCE(this, f8942JD, new Object[0]));
                    return;
                case 3:
                    bFf().mo5606d(new aCE(this, f8942JD, new Object[0]));
                    break;
            }
            m37715pi();
        }

        /* access modifiers changed from: package-private */
        /* renamed from: a */
        public void mo21404a(Outpost qZVar, Outpost qZVar2, OutpostUpgrade axr) {
            m37711i(qZVar);
            super.mo4706l(qZVar2);
            m37710c(axr);
            m37713oA((float) axr.cDj());
            m37714oB((dvm() * 60.0f) / 100.0f);
        }

        @C0064Am(aul = "7f1e9c53fe912927a374d4d6f3500415", aum = 0)
        private void dvq() {
            mo4704hk(dvn());
            dvp().mo21395eC(true);
        }

        @C0064Am(aul = "4d69ba782200849c377df30c09fda1f8", aum = 0)
        private void dvs() {
            if (!dvo()) {
                m37713oA(dvm() - (dvn() / 60.0f));
                if (dvm() <= 0.0f) {
                    dvp().m37665b(dvl());
                    dvx();
                }
                dvp().mo21395eC(true);
            }
        }

        @C0064Am(aul = "c96226eb1caaab6f2efd4be03c4b8464", aum = 0)
        private int dvu() {
            return (int) Math.floor((double) ((1.0f - (dvm() / ((float) dvl().cDj()))) * 100.0f));
        }

        @C0064Am(aul = "72a70b178df9672551bced754491abf6", aum = 0)
        private void dvw() {
            m37712kj(true);
            stop();
        }

        @C0064Am(aul = "48913b8f823299a48187172cc885c002", aum = 0)
        /* renamed from: pi */
        private void m37715pi() {
            dvt();
        }

        @C0064Am(aul = "7e748c14ed167baa0f3f84ae8be334ba", aum = 0)
        private boolean boB() {
            return dvp().boC();
        }
    }
}
