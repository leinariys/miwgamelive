package game.script.ship;

import game.network.message.externalizable.aCE;
import game.network.message.serializable.C1556Wo;
import game.network.message.serializable.C3438ra;
import logic.baa.*;
import logic.data.mbean.C5503aLv;
import logic.res.code.C5640aRc;
import logic.res.code.C5663aRz;
import logic.res.code.C6494any;
import logic.res.html.C2491fm;
import p001a.*;
import taikodom.infra.script.I18NString;

import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Map;

@C0566Hp
@C5511aMd
@C6485anp
/* renamed from: a.KZ */
/* compiled from: a */
public class OutpostType extends BaseOutpostType implements C1616Xf {

    /* renamed from: KP */
    public static final C5663aRz f976KP = null;

    /* renamed from: MT */
    public static final C5663aRz f977MT = null;

    /* renamed from: MY */
    public static final C2491fm f978MY = null;

    /* renamed from: MZ */
    public static final C2491fm f979MZ = null;
    public static final int _m_fieldCount = 0;
    public static final C5663aRz[] _m_fields = null;
    public static final int _m_methodCount = 0;
    public static final C2491fm[] _m_methods = null;
    /* renamed from: dN */
    public static final C2491fm f980dN = null;
    public static final C5663aRz dsA = null;
    public static final C5663aRz dsB = null;
    public static final C2491fm dsC = null;
    public static final C2491fm dsD = null;
    public static final C2491fm dsE = null;
    public static final C2491fm dsF = null;
    public static final C2491fm dsG = null;
    public static final C2491fm dsH = null;
    public static final C2491fm dsI = null;
    public static final C2491fm dsJ = null;
    public static final C2491fm dsK = null;
    public static final C2491fm dsL = null;
    public static final C2491fm dsM = null;
    public static final C2491fm dsN = null;
    public static final C2491fm dsO = null;
    public static final C2491fm dsP = null;
    public static final C2491fm dsQ = null;
    public static final C2491fm dsR = null;
    public static final C2491fm dsS = null;
    public static final C2491fm dsT = null;
    public static final C2491fm dsU = null;
    public static final C2491fm dsV = null;
    public static final C5663aRz dsw = null;
    public static final C5663aRz dsx = null;
    public static final C5663aRz dsy = null;
    public static final C5663aRz dsz = null;
    public static final long serialVersionUID = 0;
    public static C6494any ___iScriptClass;
    @C0064Am(aul = "d08f040f2d41d02bb82429d8bde90d1a", aum = 1)
    private static int daP;
    @C0064Am(aul = "c586d70210fb7ba5d6d5187d6a19abab", aum = 2)
    private static C3438ra<OutpostActivationItem> daQ;
    @C0064Am(aul = "6085a82a432e3e355104d2291a8f76bd", aum = 3)
    private static aHK daR;
    @C0064Am(aul = "a3742f803b65fbd65f5cd4621edd2f84", aum = 4)
    private static I18NString daS;
    @C0064Am(aul = "3484167a43dc3d853d0bac73493a0f88", aum = 5)
    private static C1556Wo<C2611hZ, OutpostUpgrade> daT;
    @C0064Am(aul = "6beff6a45af17e43c2a2a3c419c4d171", aum = 6)
    private static C1556Wo<C2611hZ, OutpostLevelInfo> daU;
    @C0064Am(aul = "08db4f84a0e5e5e2ea70a0e425424813", aum = 7)
    private static C1556Wo<C0437GE, OutpostLevelFeature> daV;
    @C0064Am(aul = "0629af4cf51a6169fc3bd7726514acbc", aum = 0)

    /* renamed from: db */
    private static long f981db;

    static {
        m6450V();
    }

    public OutpostType() {
        super((C5540aNg) null);
        super.mo10S();
    }

    public OutpostType(C5540aNg ang) {
        super(ang);
    }

    /* renamed from: V */
    static void m6450V() {
        serialVersionUID = (long) 1;
        _m_fieldCount = BaseOutpostType._m_fieldCount + 8;
        _m_methodCount = BaseOutpostType._m_methodCount + 23;
        int i = BaseOutpostType._m_fieldCount;
        C5663aRz[] arzArr = new C5663aRz[(i + 8)];
        C5663aRz b = C5640aRc.m17844b(OutpostType.class, "0629af4cf51a6169fc3bd7726514acbc", i);
        f977MT = b;
        arzArr[i] = b;
        int i2 = i + 1;
        C5663aRz b2 = C5640aRc.m17844b(OutpostType.class, "d08f040f2d41d02bb82429d8bde90d1a", i2);
        dsw = b2;
        arzArr[i2] = b2;
        int i3 = i2 + 1;
        C5663aRz b3 = C5640aRc.m17844b(OutpostType.class, "c586d70210fb7ba5d6d5187d6a19abab", i3);
        dsx = b3;
        arzArr[i3] = b3;
        int i4 = i3 + 1;
        C5663aRz b4 = C5640aRc.m17844b(OutpostType.class, "6085a82a432e3e355104d2291a8f76bd", i4);
        f976KP = b4;
        arzArr[i4] = b4;
        int i5 = i4 + 1;
        C5663aRz b5 = C5640aRc.m17844b(OutpostType.class, "a3742f803b65fbd65f5cd4621edd2f84", i5);
        dsy = b5;
        arzArr[i5] = b5;
        int i6 = i5 + 1;
        C5663aRz b6 = C5640aRc.m17844b(OutpostType.class, "3484167a43dc3d853d0bac73493a0f88", i6);
        dsz = b6;
        arzArr[i6] = b6;
        int i7 = i6 + 1;
        C5663aRz b7 = C5640aRc.m17844b(OutpostType.class, "6beff6a45af17e43c2a2a3c419c4d171", i7);
        dsA = b7;
        arzArr[i7] = b7;
        int i8 = i7 + 1;
        C5663aRz b8 = C5640aRc.m17844b(OutpostType.class, "08db4f84a0e5e5e2ea70a0e425424813", i8);
        dsB = b8;
        arzArr[i8] = b8;
        int i9 = i8 + 1;
        _m_fields = arzArr;
        C1634Xv.m11725a((Object[]) BaseOutpostType._m_fields, (Object[]) _m_fields);
        int i10 = BaseOutpostType._m_methodCount;
        C2491fm[] fmVarArr = new C2491fm[(i10 + 23)];
        C2491fm a = C4105zY.m41624a(OutpostType.class, "307a7966209e5bf9e036f0b158add87e", i10);
        f978MY = a;
        fmVarArr[i10] = a;
        int i11 = i10 + 1;
        C2491fm a2 = C4105zY.m41624a(OutpostType.class, "94966f3f7866fd8be9b40d6e14d59e33", i11);
        f979MZ = a2;
        fmVarArr[i11] = a2;
        int i12 = i11 + 1;
        C2491fm a3 = C4105zY.m41624a(OutpostType.class, "6b4c4ef4b0c8859514e3b8c7ddd79fb9", i12);
        dsC = a3;
        fmVarArr[i12] = a3;
        int i13 = i12 + 1;
        C2491fm a4 = C4105zY.m41624a(OutpostType.class, "cf580ccd726c0ee60027ef063681676c", i13);
        dsD = a4;
        fmVarArr[i13] = a4;
        int i14 = i13 + 1;
        C2491fm a5 = C4105zY.m41624a(OutpostType.class, "308eada76657f97b94a92ac1c7d67165", i14);
        dsE = a5;
        fmVarArr[i14] = a5;
        int i15 = i14 + 1;
        C2491fm a6 = C4105zY.m41624a(OutpostType.class, "d0cb59d0529dd81b21ffb7297eb5a830", i15);
        dsF = a6;
        fmVarArr[i15] = a6;
        int i16 = i15 + 1;
        C2491fm a7 = C4105zY.m41624a(OutpostType.class, "f06519f1d8933d50d687a2d1d81a3524", i16);
        dsG = a7;
        fmVarArr[i16] = a7;
        int i17 = i16 + 1;
        C2491fm a8 = C4105zY.m41624a(OutpostType.class, "08dd67a194510d4e4768448e6384d9a1", i17);
        dsH = a8;
        fmVarArr[i17] = a8;
        int i18 = i17 + 1;
        C2491fm a9 = C4105zY.m41624a(OutpostType.class, "164b02234f03734d1e873720410490f4", i18);
        dsI = a9;
        fmVarArr[i18] = a9;
        int i19 = i18 + 1;
        C2491fm a10 = C4105zY.m41624a(OutpostType.class, "bd7d13c257c7e1d55b1f8f3043c01596", i19);
        dsJ = a10;
        fmVarArr[i19] = a10;
        int i20 = i19 + 1;
        C2491fm a11 = C4105zY.m41624a(OutpostType.class, "b429bfd944a33d289a619922f1b7f14d", i20);
        dsK = a11;
        fmVarArr[i20] = a11;
        int i21 = i20 + 1;
        C2491fm a12 = C4105zY.m41624a(OutpostType.class, "98d8499864405ffdbe0267400a6e8217", i21);
        dsL = a12;
        fmVarArr[i21] = a12;
        int i22 = i21 + 1;
        C2491fm a13 = C4105zY.m41624a(OutpostType.class, "ff25906a2570ff56a63c1f9a273b8f04", i22);
        dsM = a13;
        fmVarArr[i22] = a13;
        int i23 = i22 + 1;
        C2491fm a14 = C4105zY.m41624a(OutpostType.class, "c4209226d12e3a528efb1b443e95afcf", i23);
        dsN = a14;
        fmVarArr[i23] = a14;
        int i24 = i23 + 1;
        C2491fm a15 = C4105zY.m41624a(OutpostType.class, "8338cc2d4aeddc7ac9fabcf362033b99", i24);
        dsO = a15;
        fmVarArr[i24] = a15;
        int i25 = i24 + 1;
        C2491fm a16 = C4105zY.m41624a(OutpostType.class, "b625edb363a0f441c773986ce90e7899", i25);
        dsP = a16;
        fmVarArr[i25] = a16;
        int i26 = i25 + 1;
        C2491fm a17 = C4105zY.m41624a(OutpostType.class, "618d335c71f9eb10e2bf42cdaaec9a17", i26);
        dsQ = a17;
        fmVarArr[i26] = a17;
        int i27 = i26 + 1;
        C2491fm a18 = C4105zY.m41624a(OutpostType.class, "ec6c1c1b24ec26d9177582383b9559b8", i27);
        dsR = a18;
        fmVarArr[i27] = a18;
        int i28 = i27 + 1;
        C2491fm a19 = C4105zY.m41624a(OutpostType.class, "1cf6776033f22fae5dfd1748287f5ac1", i28);
        dsS = a19;
        fmVarArr[i28] = a19;
        int i29 = i28 + 1;
        C2491fm a20 = C4105zY.m41624a(OutpostType.class, "5997079ea22a44e935a5c6283e3d0632", i29);
        f980dN = a20;
        fmVarArr[i29] = a20;
        int i30 = i29 + 1;
        C2491fm a21 = C4105zY.m41624a(OutpostType.class, "666acde8712df0401c454ba524193c2f", i30);
        dsT = a21;
        fmVarArr[i30] = a21;
        int i31 = i30 + 1;
        C2491fm a22 = C4105zY.m41624a(OutpostType.class, "e62d1e3e2812c8e84f5584f41e8ddf06", i31);
        dsU = a22;
        fmVarArr[i31] = a22;
        int i32 = i31 + 1;
        C2491fm a23 = C4105zY.m41624a(OutpostType.class, "3c34c7455a19dd2454af239ec1878f22", i32);
        dsV = a23;
        fmVarArr[i32] = a23;
        int i33 = i32 + 1;
        _m_methods = fmVarArr;
        C1634Xv.m11725a((Object[]) BaseOutpostType._m_methods, (Object[]) _m_methods);
        ___iScriptClass = aUO.m18566a(OutpostType.class, C5503aLv.class, _m_fields, _m_methods);
    }

    /* renamed from: E */
    private void m6445E(C1556Wo wo) {
        bFf().mo5608dq().mo3197f(dsz, wo);
    }

    /* renamed from: F */
    private void m6446F(C1556Wo wo) {
        bFf().mo5608dq().mo3197f(dsA, wo);
    }

    /* renamed from: G */
    private void m6447G(long j) {
        bFf().mo5608dq().mo3184b(f977MT, j);
    }

    /* renamed from: G */
    private void m6448G(C1556Wo wo) {
        bFf().mo5608dq().mo3197f(dsB, wo);
    }

    /* renamed from: a */
    private void m6451a(aHK ahk) {
        bFf().mo5608dq().mo3197f(f976KP, ahk);
    }

    @C0064Am(aul = "666acde8712df0401c454ba524193c2f", aum = 0)
    private Map beO() {
        return beH();
    }

    @C0064Am(aul = "e62d1e3e2812c8e84f5584f41e8ddf06", aum = 0)
    private Map beQ() {
        return beL();
    }

    @C0064Am(aul = "3c34c7455a19dd2454af239ec1878f22", aum = 0)
    private Map beS() {
        return beJ();
    }

    private int bep() {
        return bFf().mo5608dq().mo3212n(dsw);
    }

    private C3438ra beq() {
        return (C3438ra) bFf().mo5608dq().mo3214p(dsx);
    }

    private aHK ber() {
        return (aHK) bFf().mo5608dq().mo3214p(f976KP);
    }

    private I18NString bes() {
        return (I18NString) bFf().mo5608dq().mo3214p(dsy);
    }

    private C1556Wo bet() {
        return (C1556Wo) bFf().mo5608dq().mo3214p(dsz);
    }

    private C1556Wo beu() {
        return (C1556Wo) bFf().mo5608dq().mo3214p(dsA);
    }

    private C1556Wo bev() {
        return (C1556Wo) bFf().mo5608dq().mo3214p(dsB);
    }

    /* renamed from: bf */
    private void m6455bf(C3438ra raVar) {
        bFf().mo5608dq().mo3197f(dsx, raVar);
    }

    /* renamed from: iC */
    private void m6457iC(I18NString i18NString) {
        bFf().mo5608dq().mo3197f(dsy, i18NString);
    }

    /* renamed from: lw */
    private void m6460lw(int i) {
        bFf().mo5608dq().mo3183b(dsw, i);
    }

    /* renamed from: rU */
    private long m6464rU() {
        return bFf().mo5608dq().mo3213o(f977MT);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Activation - Required fee")
    /* renamed from: K */
    public void mo3566K(long j) {
        switch (bFf().mo6893i(f979MZ)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f979MZ, new Object[]{new Long(j)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f979MZ, new Object[]{new Long(j)}));
                break;
        }
        m6449J(j);
    }

    /* renamed from: T */
    public C6494any mo11T() {
        return ___iScriptClass;
    }

    /* renamed from: U */
    public C2491fm[] mo12U() {
        return _m_methods;
    }

    /* renamed from: W */
    public Object mo13W() {
        return new C5503aLv(this);
    }

    /* renamed from: a */
    public Object mo14a(C0495Gr gr) {
        Object[] args = gr.getArgs();
        switch (gr.mo2417hq() - BaseOutpostType._m_methodCount) {
            case 0:
                return new Long(m6465rY());
            case 1:
                m6449J(((Long) args[0]).longValue());
                return null;
            case 2:
                return new Integer(bew());
            case 3:
                m6461lx(((Integer) args[0]).intValue());
                return null;
            case 4:
                m6452a((OutpostActivationItem) args[0]);
                return null;
            case 5:
                m6456c((OutpostActivationItem) args[0]);
                return null;
            case 6:
                return bey();
            case 7:
                return beA();
            case 8:
                return beC();
            case 9:
                m6454b((aHK) args[0]);
                return null;
            case 10:
                return beE();
            case 11:
                m6458iD((I18NString) args[0]);
                return null;
            case 12:
                return beG();
            case 13:
                m6459k((Map) args[0]);
                return null;
            case 14:
                return beI();
            case 15:
                m6462m((Map) args[0]);
                return null;
            case 16:
                return beK();
            case 17:
                m6463o((Map) args[0]);
                return null;
            case 18:
                return beM();
            case 19:
                return m6453aT();
            case 20:
                return beO();
            case 21:
                return beQ();
            case 22:
                return beS();
            default:
                return super.mo14a(gr);
        }
    }

    /* renamed from: a */
    public void mo15a(Collection collection, C0495Gr gr) {
        super.mo15a(collection, gr);
    }

    /* access modifiers changed from: protected */
    /* renamed from: aU */
    public <T extends aDJ> T mo745aU() {
        switch (bFf().mo6893i(f980dN)) {
            case 0:
                return null;
            case 2:
                return (aDJ) bFf().mo5606d(new aCE(this, f980dN, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f980dN, new Object[0]));
                break;
        }
        return m6453aT();
    }

    @C3248pc(aYR = C3248pc.C3250b.ADDER, displayName = "Activation - Required material")
    /* renamed from: b */
    public void mo3567b(OutpostActivationItem uBVar) {
        switch (bFf().mo6893i(dsE)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, dsE, new Object[]{uBVar}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, dsE, new Object[]{uBVar}));
                break;
        }
        m6452a(uBVar);
    }

    @C3248pc(aYR = C3248pc.C3250b.ITERATOR, displayName = "Activation - Required material")
    public List<OutpostActivationItem> beB() {
        switch (bFf().mo6893i(dsH)) {
            case 0:
                return null;
            case 2:
                return (List) bFf().mo5606d(new aCE(this, dsH, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, dsH, new Object[0]));
                break;
        }
        return beA();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Category")
    public aHK beD() {
        switch (bFf().mo6893i(dsI)) {
            case 0:
                return null;
            case 2:
                return (aHK) bFf().mo5606d(new aCE(this, dsI, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, dsI, new Object[0]));
                break;
        }
        return beC();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Type Name")
    public I18NString beF() {
        switch (bFf().mo6893i(dsK)) {
            case 0:
                return null;
            case 2:
                return (I18NString) bFf().mo5606d(new aCE(this, dsK, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, dsK, new Object[0]));
                break;
        }
        return beE();
    }

    public C1556Wo<C2611hZ, OutpostUpgrade> beH() {
        switch (bFf().mo6893i(dsM)) {
            case 0:
                return null;
            case 2:
                return (C1556Wo) bFf().mo5606d(new aCE(this, dsM, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, dsM, new Object[0]));
                break;
        }
        return beG();
    }

    public C1556Wo<C2611hZ, OutpostLevelInfo> beJ() {
        switch (bFf().mo6893i(dsO)) {
            case 0:
                return null;
            case 2:
                return (C1556Wo) bFf().mo5606d(new aCE(this, dsO, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, dsO, new Object[0]));
                break;
        }
        return beI();
    }

    public C1556Wo<C0437GE, OutpostLevelFeature> beL() {
        switch (bFf().mo6893i(dsQ)) {
            case 0:
                return null;
            case 2:
                return (C1556Wo) bFf().mo5606d(new aCE(this, dsQ, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, dsQ, new Object[0]));
                break;
        }
        return beK();
    }

    public Outpost beN() {
        switch (bFf().mo6893i(dsS)) {
            case 0:
                return null;
            case 2:
                return (Outpost) bFf().mo5606d(new aCE(this, dsS, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, dsS, new Object[0]));
                break;
        }
        return beM();
    }

    public /* bridge */ /* synthetic */ Map beP() {
        switch (bFf().mo6893i(dsT)) {
            case 0:
                return null;
            case 2:
                return (Map) bFf().mo5606d(new aCE(this, dsT, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, dsT, new Object[0]));
                break;
        }
        return beO();
    }

    public /* bridge */ /* synthetic */ Map beR() {
        switch (bFf().mo6893i(dsU)) {
            case 0:
                return null;
            case 2:
                return (Map) bFf().mo5606d(new aCE(this, dsU, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, dsU, new Object[0]));
                break;
        }
        return beQ();
    }

    public /* bridge */ /* synthetic */ Map beT() {
        switch (bFf().mo6893i(dsV)) {
            case 0:
                return null;
            case 2:
                return (Map) bFf().mo5606d(new aCE(this, dsV, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, dsV, new Object[0]));
                break;
        }
        return beS();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Activation - Reservation Length (in days)")
    public int bex() {
        switch (bFf().mo6893i(dsC)) {
            case 0:
                return 0;
            case 2:
                return ((Integer) bFf().mo5606d(new aCE(this, dsC, new Object[0]))).intValue();
            case 3:
                bFf().mo5606d(new aCE(this, dsC, new Object[0]));
                break;
        }
        return bew();
    }

    public C3438ra<OutpostActivationItem> bez() {
        switch (bFf().mo6893i(dsG)) {
            case 0:
                return null;
            case 2:
                return (C3438ra) bFf().mo5606d(new aCE(this, dsG, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, dsG, new Object[0]));
                break;
        }
        return bey();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Category")
    /* renamed from: c */
    public void mo3580c(aHK ahk) {
        switch (bFf().mo6893i(dsJ)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, dsJ, new Object[]{ahk}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, dsJ, new Object[]{ahk}));
                break;
        }
        m6454b(ahk);
    }

    /* renamed from: c */
    public C5663aRz[] mo25c() {
        return _m_fields;
    }

    @C3248pc(aYR = C3248pc.C3250b.REMOVER, displayName = "Activation - Required material")
    /* renamed from: d */
    public void mo3581d(OutpostActivationItem uBVar) {
        switch (bFf().mo6893i(dsF)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, dsF, new Object[]{uBVar}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, dsF, new Object[]{uBVar}));
                break;
        }
        m6456c(uBVar);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Type Name")
    /* renamed from: iE */
    public void mo3582iE(I18NString i18NString) {
        switch (bFf().mo6893i(dsL)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, dsL, new Object[]{i18NString}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, dsL, new Object[]{i18NString}));
                break;
        }
        m6458iD(i18NString);
    }

    /* renamed from: l */
    public void mo3583l(Map<C2611hZ, OutpostUpgrade> map) {
        switch (bFf().mo6893i(dsN)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, dsN, new Object[]{map}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, dsN, new Object[]{map}));
                break;
        }
        m6459k(map);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Activation - Reservation Length (in days)")
    /* renamed from: ly */
    public void mo3584ly(int i) {
        switch (bFf().mo6893i(dsD)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, dsD, new Object[]{new Integer(i)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, dsD, new Object[]{new Integer(i)}));
                break;
        }
        m6461lx(i);
    }

    /* renamed from: n */
    public void mo3585n(Map<C2611hZ, OutpostLevelInfo> map) {
        switch (bFf().mo6893i(dsP)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, dsP, new Object[]{map}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, dsP, new Object[]{map}));
                break;
        }
        m6462m(map);
    }

    /* renamed from: p */
    public void mo3586p(Map<C0437GE, OutpostLevelFeature> map) {
        switch (bFf().mo6893i(dsR)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, dsR, new Object[]{map}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, dsR, new Object[]{map}));
                break;
        }
        m6463o(map);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Activation - Required fee")
    /* renamed from: rZ */
    public long mo3587rZ() {
        switch (bFf().mo6893i(f978MY)) {
            case 0:
                return 0;
            case 2:
                return ((Long) bFf().mo5606d(new aCE(this, f978MY, new Object[0]))).longValue();
            case 3:
                bFf().mo5606d(new aCE(this, f978MY, new Object[0]));
                break;
        }
        return m6465rY();
    }

    /* renamed from: S */
    public void mo10S() {
        super.mo10S();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Activation - Required fee")
    @C0064Am(aul = "307a7966209e5bf9e036f0b158add87e", aum = 0)
    /* renamed from: rY */
    private long m6465rY() {
        return m6464rU();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Activation - Required fee")
    @C0064Am(aul = "94966f3f7866fd8be9b40d6e14d59e33", aum = 0)
    /* renamed from: J */
    private void m6449J(long j) {
        m6447G(j);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Activation - Reservation Length (in days)")
    @C0064Am(aul = "6b4c4ef4b0c8859514e3b8c7ddd79fb9", aum = 0)
    private int bew() {
        return bep();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Activation - Reservation Length (in days)")
    @C0064Am(aul = "cf580ccd726c0ee60027ef063681676c", aum = 0)
    /* renamed from: lx */
    private void m6461lx(int i) {
        m6460lw(i);
    }

    @C3248pc(aYR = C3248pc.C3250b.ADDER, displayName = "Activation - Required material")
    @C0064Am(aul = "308eada76657f97b94a92ac1c7d67165", aum = 0)
    /* renamed from: a */
    private void m6452a(OutpostActivationItem uBVar) {
        beq().add(uBVar);
    }

    @C3248pc(aYR = C3248pc.C3250b.REMOVER, displayName = "Activation - Required material")
    @C0064Am(aul = "d0cb59d0529dd81b21ffb7297eb5a830", aum = 0)
    /* renamed from: c */
    private void m6456c(OutpostActivationItem uBVar) {
        beq().remove(uBVar);
    }

    @C0064Am(aul = "f06519f1d8933d50d687a2d1d81a3524", aum = 0)
    private C3438ra<OutpostActivationItem> bey() {
        return beq();
    }

    @C3248pc(aYR = C3248pc.C3250b.ITERATOR, displayName = "Activation - Required material")
    @C0064Am(aul = "08dd67a194510d4e4768448e6384d9a1", aum = 0)
    private List<OutpostActivationItem> beA() {
        return Collections.unmodifiableList(beq());
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Category")
    @C0064Am(aul = "164b02234f03734d1e873720410490f4", aum = 0)
    private aHK beC() {
        return ber();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Category")
    @C0064Am(aul = "bd7d13c257c7e1d55b1f8f3043c01596", aum = 0)
    /* renamed from: b */
    private void m6454b(aHK ahk) {
        m6451a(ahk);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Type Name")
    @C0064Am(aul = "b429bfd944a33d289a619922f1b7f14d", aum = 0)
    private I18NString beE() {
        return bes();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Type Name")
    @C0064Am(aul = "98d8499864405ffdbe0267400a6e8217", aum = 0)
    /* renamed from: iD */
    private void m6458iD(I18NString i18NString) {
        m6457iC(i18NString);
    }

    @C0064Am(aul = "ff25906a2570ff56a63c1f9a273b8f04", aum = 0)
    private C1556Wo<C2611hZ, OutpostUpgrade> beG() {
        return bet();
    }

    @C0064Am(aul = "c4209226d12e3a528efb1b443e95afcf", aum = 0)
    /* renamed from: k */
    private void m6459k(Map<C2611hZ, OutpostUpgrade> map) {
        bet().clear();
        bet().putAll(map);
    }

    @C0064Am(aul = "8338cc2d4aeddc7ac9fabcf362033b99", aum = 0)
    private C1556Wo<C2611hZ, OutpostLevelInfo> beI() {
        return beu();
    }

    @C0064Am(aul = "b625edb363a0f441c773986ce90e7899", aum = 0)
    /* renamed from: m */
    private void m6462m(Map<C2611hZ, OutpostLevelInfo> map) {
        beu().clear();
        beu().putAll(map);
    }

    @C0064Am(aul = "618d335c71f9eb10e2bf42cdaaec9a17", aum = 0)
    private C1556Wo<C0437GE, OutpostLevelFeature> beK() {
        return bev();
    }

    @C0064Am(aul = "ec6c1c1b24ec26d9177582383b9559b8", aum = 0)
    /* renamed from: o */
    private void m6463o(Map<C0437GE, OutpostLevelFeature> map) {
        bev().clear();
        bev().putAll(map);
    }

    @C0064Am(aul = "1cf6776033f22fae5dfd1748287f5ac1", aum = 0)
    private Outpost beM() {
        return (Outpost) mo745aU();
    }

    @C0064Am(aul = "5997079ea22a44e935a5c6283e3d0632", aum = 0)
    /* renamed from: aT */
    private <T extends aDJ> T m6453aT() {
        T t = (Outpost) bFf().mo6865M(Outpost.class);
        t.mo21392d(this);
        return t;
    }
}
