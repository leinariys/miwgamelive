package game.script.ship;

import game.network.message.externalizable.C2814kV;
import game.network.message.externalizable.C6556apI;
import game.network.message.externalizable.aCE;
import game.network.message.serializable.C1556Wo;
import game.script.Actor;
import game.script.TaikodomObject;
import game.script.damage.DamageType;
import game.script.player.Player;
import game.script.util.AdapterLikedList;
import game.script.util.GameObjectAdapter;
import logic.aaa.C1506WA;
import logic.baa.*;
import logic.data.mbean.C6378alm;
import logic.data.mbean.C6698aru;
import logic.res.code.C5640aRc;
import logic.res.code.C5663aRz;
import logic.res.code.C6494any;
import logic.res.html.C2491fm;
import p001a.*;

import java.util.Collection;

@C5829abJ("2.1.0")
@C6485anp
@C2712iu(mo19786Bt = BaseShieldType.class, version = "2.1.0")
@C5511aMd
/* renamed from: a.gj */
/* compiled from: a */
public class Shield extends TaikodomObject implements C6480ank<ShieldAdapter>, logic.baa.aOW, C6872avM, C6872avM {

    /* renamed from: Do */
    public static final C2491fm f7733Do = null;

    /* renamed from: Lm */
    public static final C2491fm f7734Lm = null;
    /* renamed from: _f_getShip_0020_0028_0029Ltaikodom_002fgame_002fscript_002fship_002fShip_003b */
    public static final C2491fm f7735x851d656b = null;
    public static final C2491fm _f_onResurrect_0020_0028_0029V = null;
    public static final int _m_fieldCount = 0;
    public static final C5663aRz[] _m_fields = null;
    public static final int _m_methodCount = 0;
    public static final C2491fm[] _m_methods = null;
    public static final C5663aRz aOB = null;
    public static final C5663aRz aOD = null;
    public static final C5663aRz aOF = null;
    public static final C5663aRz aOH = null;
    public static final C5663aRz aOJ = null;
    public static final C5663aRz aOL = null;
    public static final C5663aRz aON = null;
    public static final C5663aRz aOP = null;
    public static final C2491fm aOQ = null;
    public static final C2491fm aOR = null;
    public static final C2491fm aOS = null;
    public static final C2491fm aOT = null;
    public static final C2491fm aOU = null;
    public static final C2491fm aOV = null;
    public static final C2491fm aOW = null;
    public static final C2491fm aOX = null;
    public static final C2491fm aOY = null;
    public static final C2491fm aOZ = null;
    public static final C5663aRz aOp = null;
    public static final C5663aRz aOr = null;
    public static final C5663aRz aOt = null;
    public static final C5663aRz aOv = null;
    public static final C5663aRz aOx = null;
    public static final C5663aRz aOz = null;
    public static final C2491fm aPa = null;
    public static final C2491fm aPb = null;
    public static final C2491fm aPc = null;
    public static final C2491fm aPd = null;
    public static final C2491fm aPe = null;
    public static final C2491fm aPf = null;
    public static final C2491fm aPg = null;
    public static final C2491fm aPh = null;
    public static final C2491fm aPi = null;
    public static final C2491fm aPj = null;
    public static final C2491fm aPk = null;
    public static final C2491fm aPl = null;
    public static final C2491fm aPm = null;
    /* renamed from: qa */
    public static final C2491fm f7736qa = null;
    /* renamed from: qb */
    public static final C2491fm f7737qb = null;
    public static final long serialVersionUID = 0;
    /* renamed from: uG */
    public static final C2491fm f7738uG = null;
    /* renamed from: uH */
    public static final C2491fm f7739uH = null;
    /* renamed from: uI */
    public static final C2491fm f7740uI = null;
    public static C6494any ___iScriptClass;
    @C0064Am(aul = "6c6fff801279feb764ab3031ca2b6d09", aum = 6)
    private static float aOA;
    @C0064Am(aul = "a7476bef8f13230be1e2fb9d7554e3e2", aum = 7)
    private static float aOC;
    @C0064Am(aul = "7dd53d465b767f8c29d8a3e261ad278b", aum = 8)
    private static float aOE;
    @C0064Am(aul = "571b767df644d582a1a42a872864665f", aum = 9)
    private static C1556Wo<DamageType, Float> aOG;
    @C0064Am(aul = "40063beff048219aa3b327f53c15120a", aum = 10)
    @C1020Ot(mo4553Ur = 5000, drz = C1020Ot.C1021a.TIME_BASED)
    @C5256aCi
    private static float aOI;
    @C0064Am(aul = "c3ddc4be019314404af79614e2557f19", aum = 11)
    @C1020Ot(drz = C1020Ot.C1021a.NOT_PERSISTED)
    @C5256aCi
    private static long aOK;
    @C0064Am(aul = "72ef3c36e162b1312c7c9c2e9777a928", aum = 12)
    @C1020Ot(mo4553Ur = 5000, drz = C1020Ot.C1021a.TIME_BASED)
    @C5256aCi
    private static boolean aOM;
    @C0064Am(aul = "1442365b0e76feef003992215b11db74", aum = 13)
    private static Ship aOO;
    @C0064Am(aul = "7c2a628bd5da8980c9ec74cc367dad6f", aum = 0)
    private static float aOo;
    @C0064Am(aul = "28ffe50c03e46febc54cebd70ee9e90b", aum = 1)
    private static float aOq;
    @C0064Am(aul = "8c1738a8ecd81022a1ada3a337ee12de", aum = 2)
    private static float aOs;
    @C0064Am(aul = "2015fc5e433cbaffbc574a03c735103e", aum = 3)
    private static C1556Wo<DamageType, C1649YI> aOu;
    @C0064Am(aul = "1086b68f599c4882fdf9b90b401e6ef0", aum = 4)
    @C5566aOg
    private static AdapterLikedList<ShieldAdapter> aOw;
    @C0064Am(aul = "18d4278273e2cbf6472ef6e8782d480b", aum = 5)
    @C5454aJy("Shield's HP / sec")
    private static int aOy;

    static {
        m32251V();
    }

    public Shield() {
        super((C5540aNg) null);
        super.mo10S();
    }

    public Shield(C5540aNg ang) {
        super(ang);
    }

    public Shield(ShieldType aph) {
        super((C5540aNg) null);
        super._m_script_init(aph);
    }

    /* renamed from: V */
    static void m32251V() {
        serialVersionUID = (long) 1;
        _m_fieldCount = TaikodomObject._m_fieldCount + 14;
        _m_methodCount = TaikodomObject._m_methodCount + 32;
        int i = TaikodomObject._m_fieldCount;
        C5663aRz[] arzArr = new C5663aRz[(i + 14)];
        C5663aRz b = C5640aRc.m17844b(Shield.class, "7c2a628bd5da8980c9ec74cc367dad6f", i);
        aOp = b;
        arzArr[i] = b;
        int i2 = i + 1;
        C5663aRz b2 = C5640aRc.m17844b(Shield.class, "28ffe50c03e46febc54cebd70ee9e90b", i2);
        aOr = b2;
        arzArr[i2] = b2;
        int i3 = i2 + 1;
        C5663aRz b3 = C5640aRc.m17844b(Shield.class, "8c1738a8ecd81022a1ada3a337ee12de", i3);
        aOt = b3;
        arzArr[i3] = b3;
        int i4 = i3 + 1;
        C5663aRz b4 = C5640aRc.m17844b(Shield.class, "2015fc5e433cbaffbc574a03c735103e", i4);
        aOv = b4;
        arzArr[i4] = b4;
        int i5 = i4 + 1;
        C5663aRz b5 = C5640aRc.m17844b(Shield.class, "1086b68f599c4882fdf9b90b401e6ef0", i5);
        aOx = b5;
        arzArr[i5] = b5;
        int i6 = i5 + 1;
        C5663aRz b6 = C5640aRc.m17844b(Shield.class, "18d4278273e2cbf6472ef6e8782d480b", i6);
        aOz = b6;
        arzArr[i6] = b6;
        int i7 = i6 + 1;
        C5663aRz b7 = C5640aRc.m17844b(Shield.class, "6c6fff801279feb764ab3031ca2b6d09", i7);
        aOB = b7;
        arzArr[i7] = b7;
        int i8 = i7 + 1;
        C5663aRz b8 = C5640aRc.m17844b(Shield.class, "a7476bef8f13230be1e2fb9d7554e3e2", i8);
        aOD = b8;
        arzArr[i8] = b8;
        int i9 = i8 + 1;
        C5663aRz b9 = C5640aRc.m17844b(Shield.class, "7dd53d465b767f8c29d8a3e261ad278b", i9);
        aOF = b9;
        arzArr[i9] = b9;
        int i10 = i9 + 1;
        C5663aRz b10 = C5640aRc.m17844b(Shield.class, "571b767df644d582a1a42a872864665f", i10);
        aOH = b10;
        arzArr[i10] = b10;
        int i11 = i10 + 1;
        C5663aRz b11 = C5640aRc.m17844b(Shield.class, "40063beff048219aa3b327f53c15120a", i11);
        aOJ = b11;
        arzArr[i11] = b11;
        int i12 = i11 + 1;
        C5663aRz b12 = C5640aRc.m17844b(Shield.class, "c3ddc4be019314404af79614e2557f19", i12);
        aOL = b12;
        arzArr[i12] = b12;
        int i13 = i12 + 1;
        C5663aRz b13 = C5640aRc.m17844b(Shield.class, "72ef3c36e162b1312c7c9c2e9777a928", i13);
        aON = b13;
        arzArr[i13] = b13;
        int i14 = i13 + 1;
        C5663aRz b14 = C5640aRc.m17844b(Shield.class, "1442365b0e76feef003992215b11db74", i14);
        aOP = b14;
        arzArr[i14] = b14;
        int i15 = i14 + 1;
        _m_fields = arzArr;
        C1634Xv.m11725a((Object[]) TaikodomObject._m_fields, (Object[]) _m_fields);
        int i16 = TaikodomObject._m_methodCount;
        C2491fm[] fmVarArr = new C2491fm[(i16 + 32)];
        C2491fm a = C4105zY.m41624a(Shield.class, "9279bc24e3b6fd9112180522a5519371", i16);
        f7737qb = a;
        fmVarArr[i16] = a;
        int i17 = i16 + 1;
        C2491fm a2 = C4105zY.m41624a(Shield.class, "160c395cc0cefa06e1fae5805972d0fc", i17);
        f7738uG = a2;
        fmVarArr[i17] = a2;
        int i18 = i17 + 1;
        C2491fm a3 = C4105zY.m41624a(Shield.class, "b542e0a3453e621c10961f71e9552336", i18);
        f7736qa = a3;
        fmVarArr[i18] = a3;
        int i19 = i18 + 1;
        C2491fm a4 = C4105zY.m41624a(Shield.class, "3ea8f57df1e6154182619b11acaf56f6", i19);
        f7740uI = a4;
        fmVarArr[i19] = a4;
        int i20 = i19 + 1;
        C2491fm a5 = C4105zY.m41624a(Shield.class, "6c8271e00e549896ef3478780796ecec", i20);
        aOQ = a5;
        fmVarArr[i20] = a5;
        int i21 = i20 + 1;
        C2491fm a6 = C4105zY.m41624a(Shield.class, "13b148823518a8b7573513ea1074f914", i21);
        f7739uH = a6;
        fmVarArr[i21] = a6;
        int i22 = i21 + 1;
        C2491fm a7 = C4105zY.m41624a(Shield.class, "d00163860f77959ae80055b7f73f399f", i22);
        aOR = a7;
        fmVarArr[i22] = a7;
        int i23 = i22 + 1;
        C2491fm a8 = C4105zY.m41624a(Shield.class, "0ea66aadbf268d0453ca7fbc9cbd152b", i23);
        aOS = a8;
        fmVarArr[i23] = a8;
        int i24 = i23 + 1;
        C2491fm a9 = C4105zY.m41624a(Shield.class, "a1b2898673330b1ac3e3c289fdf13768", i24);
        aOT = a9;
        fmVarArr[i24] = a9;
        int i25 = i24 + 1;
        C2491fm a10 = C4105zY.m41624a(Shield.class, "6ca4b0f4b88f399fc2944661ec8466fe", i25);
        aOU = a10;
        fmVarArr[i25] = a10;
        int i26 = i25 + 1;
        C2491fm a11 = C4105zY.m41624a(Shield.class, "ade4f380e6b131aa3d05aa465a1d8168", i26);
        aOV = a11;
        fmVarArr[i26] = a11;
        int i27 = i26 + 1;
        C2491fm a12 = C4105zY.m41624a(Shield.class, "bf17177f0848ba865f108f817b63a143", i27);
        aOW = a12;
        fmVarArr[i27] = a12;
        int i28 = i27 + 1;
        C2491fm a13 = C4105zY.m41624a(Shield.class, "f55841512d6d955e8c9c5715710fbe37", i28);
        aOX = a13;
        fmVarArr[i28] = a13;
        int i29 = i28 + 1;
        C2491fm a14 = C4105zY.m41624a(Shield.class, "017d6cb31e6b3dfe603605dc344a7c03", i29);
        aOY = a14;
        fmVarArr[i29] = a14;
        int i30 = i29 + 1;
        C2491fm a15 = C4105zY.m41624a(Shield.class, "7b5eef4d964c3c1329157a3dd0f5cec4", i30);
        aOZ = a15;
        fmVarArr[i30] = a15;
        int i31 = i30 + 1;
        C2491fm a16 = C4105zY.m41624a(Shield.class, "c5b906089d7858c68e609c0e23afa979", i31);
        aPa = a16;
        fmVarArr[i31] = a16;
        int i32 = i31 + 1;
        C2491fm a17 = C4105zY.m41624a(Shield.class, "6a41350bf4d0cb0d9a4ac72482cd249b", i32);
        aPb = a17;
        fmVarArr[i32] = a17;
        int i33 = i32 + 1;
        C2491fm a18 = C4105zY.m41624a(Shield.class, "a4106766e2c171a0b1ffb9be8f7ed30e", i33);
        aPc = a18;
        fmVarArr[i33] = a18;
        int i34 = i33 + 1;
        C2491fm a19 = C4105zY.m41624a(Shield.class, "2df22850abb65f0b5a48ee350f53a420", i34);
        f7734Lm = a19;
        fmVarArr[i34] = a19;
        int i35 = i34 + 1;
        C2491fm a20 = C4105zY.m41624a(Shield.class, "ae77ff793f24a69abf29e07c60a44efb", i35);
        aPd = a20;
        fmVarArr[i35] = a20;
        int i36 = i35 + 1;
        C2491fm a21 = C4105zY.m41624a(Shield.class, "625f6874b58f4bcd2bd7812301a7f24c", i36);
        _f_onResurrect_0020_0028_0029V = a21;
        fmVarArr[i36] = a21;
        int i37 = i36 + 1;
        C2491fm a22 = C4105zY.m41624a(Shield.class, "6fe6d62a44c340d61f8208e6b1476b39", i37);
        aPe = a22;
        fmVarArr[i37] = a22;
        int i38 = i37 + 1;
        C2491fm a23 = C4105zY.m41624a(Shield.class, "9c1323978760a45d3d3405197d646df2", i38);
        aPf = a23;
        fmVarArr[i38] = a23;
        int i39 = i38 + 1;
        C2491fm a24 = C4105zY.m41624a(Shield.class, "b20fdb1ccfdf987314db92c9749f5651", i39);
        f7733Do = a24;
        fmVarArr[i39] = a24;
        int i40 = i39 + 1;
        C2491fm a25 = C4105zY.m41624a(Shield.class, "c284f03aad2556cfb34cc819c54dbd22", i40);
        aPg = a25;
        fmVarArr[i40] = a25;
        int i41 = i40 + 1;
        C2491fm a26 = C4105zY.m41624a(Shield.class, "141bb739433be91d45210a42a431c8c8", i41);
        aPh = a26;
        fmVarArr[i41] = a26;
        int i42 = i41 + 1;
        C2491fm a27 = C4105zY.m41624a(Shield.class, "f3fd36712db059378bbccb29eafbaa3f", i42);
        aPi = a27;
        fmVarArr[i42] = a27;
        int i43 = i42 + 1;
        C2491fm a28 = C4105zY.m41624a(Shield.class, "1d67b8525fdcd637f04803033b4976de", i43);
        f7735x851d656b = a28;
        fmVarArr[i43] = a28;
        int i44 = i43 + 1;
        C2491fm a29 = C4105zY.m41624a(Shield.class, "bf36b07d2d915efa7f1514cafe1296c7", i44);
        aPj = a29;
        fmVarArr[i44] = a29;
        int i45 = i44 + 1;
        C2491fm a30 = C4105zY.m41624a(Shield.class, "18d4fae9c426209a2cb3b568534840d3", i45);
        aPk = a30;
        fmVarArr[i45] = a30;
        int i46 = i45 + 1;
        C2491fm a31 = C4105zY.m41624a(Shield.class, "b4e0034465e2b657f7aee08eecbd15ab", i46);
        aPl = a31;
        fmVarArr[i46] = a31;
        int i47 = i46 + 1;
        C2491fm a32 = C4105zY.m41624a(Shield.class, "4e2f542a284d610d8df4bcacde77ad19", i47);
        aPm = a32;
        fmVarArr[i47] = a32;
        int i48 = i47 + 1;
        _m_methods = fmVarArr;
        C1634Xv.m11725a((Object[]) TaikodomObject._m_methods, (Object[]) _m_methods);
        ___iScriptClass = aUO.m18566a(Shield.class, C6698aru.class, _m_fields, _m_methods);
    }

    @C0064Am(aul = "6fe6d62a44c340d61f8208e6b1476b39", aum = 0)
    @C5566aOg
    /* renamed from: TG */
    private void m32222TG() {
        throw new aWi(new aCE(this, aPe, new Object[0]));
    }

    @C5566aOg
    /* renamed from: TH */
    private void m32223TH() {
        switch (bFf().mo6893i(aPe)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, aPe, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, aPe, new Object[0]));
                break;
        }
        m32222TG();
    }

    /* renamed from: TP */
    private void m32228TP() {
        switch (bFf().mo6893i(aPl)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, aPl, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, aPl, new Object[0]));
                break;
        }
        m32227TO();
    }

    /* renamed from: Ta */
    private float m32230Ta() {
        return ((ShieldType) getType()).mo15373hh();
    }

    /* renamed from: Tb */
    private float m32231Tb() {
        return ((ShieldType) getType()).cpZ();
    }

    /* renamed from: Tc */
    private float m32232Tc() {
        return ((ShieldType) getType()).mo15374ic();
    }

    /* renamed from: Td */
    private C1556Wo m32233Td() {
        return ((ShieldType) getType()).ana();
    }

    /* renamed from: Te */
    private AdapterLikedList m32234Te() {
        return (AdapterLikedList) bFf().mo5608dq().mo3214p(aOx);
    }

    /* renamed from: Tf */
    private int m32235Tf() {
        return ((ShieldType) getType()).cqb();
    }

    /* renamed from: Tg */
    private float m32236Tg() {
        return bFf().mo5608dq().mo3211m(aOB);
    }

    /* renamed from: Th */
    private float m32237Th() {
        return bFf().mo5608dq().mo3211m(aOD);
    }

    /* renamed from: Ti */
    private float m32238Ti() {
        return bFf().mo5608dq().mo3211m(aOF);
    }

    /* renamed from: Tj */
    private C1556Wo m32239Tj() {
        return (C1556Wo) bFf().mo5608dq().mo3214p(aOH);
    }

    /* renamed from: Tk */
    private float m32240Tk() {
        return bFf().mo5608dq().mo3211m(aOJ);
    }

    /* renamed from: Tl */
    private long m32241Tl() {
        return bFf().mo5608dq().mo3213o(aOL);
    }

    /* renamed from: Tm */
    private boolean m32242Tm() {
        return bFf().mo5608dq().mo3201h(aON);
    }

    /* renamed from: Tn */
    private Ship m32243Tn() {
        return (Ship) bFf().mo5608dq().mo3214p(aOP);
    }

    @C4034yP
    @C5566aOg
    @C0064Am(aul = "bf17177f0848ba865f108f817b63a143", aum = 0)
    @C2499fr
    /* renamed from: Ty */
    private void m32249Ty() {
        throw new aWi(new aCE(this, aOW, new Object[0]));
    }

    @C4034yP
    @C5566aOg
    @C2499fr
    /* renamed from: Tz */
    private void m32250Tz() {
        switch (bFf().mo6893i(aOW)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, aOW, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, aOW, new Object[0]));
                break;
        }
        m32249Ty();
    }

    @C0064Am(aul = "6a41350bf4d0cb0d9a4ac72482cd249b", aum = 0)
    @C5566aOg
    /* renamed from: a */
    private float m32254a(DamageType fr, float f, Actor cr) {
        throw new aWi(new aCE(this, aPb, new Object[]{fr, new Float(f), cr}));
    }

    /* renamed from: a */
    private void m32257a(AdapterLikedList zAVar) {
        bFf().mo5608dq().mo3197f(aOx, zAVar);
    }

    /* renamed from: aN */
    private void m32259aN(boolean z) {
        bFf().mo5608dq().mo3153a(aON, z);
    }

    @C0064Am(aul = "a4106766e2c171a0b1ffb9be8f7ed30e", aum = 0)
    @C5566aOg
    /* renamed from: c */
    private float m32260c(DamageType fr, float f) {
        throw new aWi(new aCE(this, aPc, new Object[]{fr, new Float(f)}));
    }

    /* renamed from: cC */
    private void m32262cC(float f) {
        throw new C6039afL();
    }

    /* renamed from: cD */
    private void m32263cD(float f) {
        throw new C6039afL();
    }

    /* renamed from: cE */
    private void m32264cE(float f) {
        throw new C6039afL();
    }

    /* renamed from: cF */
    private void m32265cF(float f) {
        bFf().mo5608dq().mo3150a(aOB, f);
    }

    /* renamed from: cG */
    private void m32266cG(float f) {
        bFf().mo5608dq().mo3150a(aOD, f);
    }

    /* renamed from: cG */
    private void m32267cG(long j) {
        bFf().mo5608dq().mo3184b(aOL, j);
    }

    /* renamed from: cH */
    private void m32268cH(float f) {
        bFf().mo5608dq().mo3150a(aOF, f);
    }

    /* renamed from: cI */
    private void m32269cI(float f) {
        bFf().mo5608dq().mo3150a(aOJ, f);
    }

    /* renamed from: cK */
    private void m32271cK(float f) {
        switch (bFf().mo6893i(aOX)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, aOX, new Object[]{new Float(f)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, aOX, new Object[]{new Float(f)}));
                break;
        }
        m32270cJ(f);
    }

    /* renamed from: d */
    private void m32272d(float f, Actor cr) {
        switch (bFf().mo6893i(aPa)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, aPa, new Object[]{new Float(f), cr}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, aPa, new Object[]{new Float(f), cr}));
                break;
        }
        m32261c(f, cr);
    }

    /* renamed from: dw */
    private void m32273dw(int i) {
        throw new C6039afL();
    }

    /* renamed from: k */
    private void m32279k(Ship fAVar) {
        bFf().mo5608dq().mo3197f(aOP, fAVar);
    }

    @C0064Am(aul = "f3fd36712db059378bbccb29eafbaa3f", aum = 0)
    @C5566aOg
    /* renamed from: l */
    private void m32280l(Ship fAVar) {
        throw new aWi(new aCE(this, aPi, new Object[]{fAVar}));
    }

    /* renamed from: m */
    private void m32281m(C1556Wo wo) {
        throw new C6039afL();
    }

    /* renamed from: n */
    private void m32282n(C1556Wo wo) {
        bFf().mo5608dq().mo3197f(aOH, wo);
    }

    @C0064Am(aul = "2df22850abb65f0b5a48ee350f53a420", aum = 0)
    @C5566aOg
    @C2499fr
    /* renamed from: qU */
    private void m32283qU() {
        throw new aWi(new aCE(this, f7734Lm, new Object[0]));
    }

    /* renamed from: S */
    public void mo10S() {
        super.mo10S();
    }

    /* renamed from: T */
    public C6494any mo11T() {
        return ___iScriptClass;
    }

    /* renamed from: TB */
    public long mo19071TB() {
        switch (bFf().mo6893i(aOY)) {
            case 0:
                return 0;
            case 2:
                return ((Long) bFf().mo5606d(new aCE(this, aOY, new Object[0]))).longValue();
            case 3:
                bFf().mo5606d(new aCE(this, aOY, new Object[0]));
                break;
        }
        return m32219TA();
    }

    /* renamed from: TD */
    public boolean mo19072TD() {
        switch (bFf().mo6893i(aOZ)) {
            case 0:
                return false;
            case 2:
                return ((Boolean) bFf().mo5606d(new aCE(this, aOZ, new Object[0]))).booleanValue();
            case 3:
                bFf().mo5606d(new aCE(this, aOZ, new Object[0]));
                break;
        }
        return m32220TC();
    }

    /* renamed from: TF */
    public ShieldType mo19073TF() {
        switch (bFf().mo6893i(aPd)) {
            case 0:
                return null;
            case 2:
                return (ShieldType) bFf().mo5606d(new aCE(this, aPd, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, aPd, new Object[0]));
                break;
        }
        return m32221TE();
    }

    /* renamed from: TJ */
    public AdapterLikedList<ShieldAdapter> mo5353TJ() {
        switch (bFf().mo6893i(aPf)) {
            case 0:
                return null;
            case 2:
                return (AdapterLikedList) bFf().mo5606d(new aCE(this, aPf, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, aPf, new Object[0]));
                break;
        }
        return m32224TI();
    }

    /* renamed from: TL */
    public void mo12905TL() {
        switch (bFf().mo6893i(aPj)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, aPj, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, aPj, new Object[0]));
                break;
        }
        m32225TK();
    }

    /* renamed from: TN */
    public void mo12906TN() {
        switch (bFf().mo6893i(aPk)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, aPk, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, aPk, new Object[0]));
                break;
        }
        m32226TM();
    }

    /* renamed from: TR */
    public void mo19074TR() {
        switch (bFf().mo6893i(aPm)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, aPm, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, aPm, new Object[0]));
                break;
        }
        m32229TQ();
    }

    /* renamed from: Tp */
    public float mo19075Tp() {
        switch (bFf().mo6893i(aOQ)) {
            case 0:
                return 0.0f;
            case 2:
                return ((Float) bFf().mo5606d(new aCE(this, aOQ, new Object[0]))).floatValue();
            case 3:
                bFf().mo5606d(new aCE(this, aOQ, new Object[0]));
                break;
        }
        return m32244To();
    }

    /* renamed from: Tr */
    public int mo19076Tr() {
        switch (bFf().mo6893i(aOS)) {
            case 0:
                return 0;
            case 2:
                return ((Integer) bFf().mo5606d(new aCE(this, aOS, new Object[0]))).intValue();
            case 3:
                bFf().mo5606d(new aCE(this, aOS, new Object[0]));
                break;
        }
        return m32245Tq();
    }

    /* renamed from: Tt */
    public float mo19077Tt() {
        switch (bFf().mo6893i(aOT)) {
            case 0:
                return 0.0f;
            case 2:
                return ((Float) bFf().mo5606d(new aCE(this, aOT, new Object[0]))).floatValue();
            case 3:
                bFf().mo5606d(new aCE(this, aOT, new Object[0]));
                break;
        }
        return m32246Ts();
    }

    /* renamed from: Tv */
    public float mo19078Tv() {
        switch (bFf().mo6893i(aOU)) {
            case 0:
                return 0.0f;
            case 2:
                return ((Float) bFf().mo5606d(new aCE(this, aOU, new Object[0]))).floatValue();
            case 3:
                bFf().mo5606d(new aCE(this, aOU, new Object[0]));
                break;
        }
        return m32247Tu();
    }

    /* renamed from: Tx */
    public float mo19079Tx() {
        switch (bFf().mo6893i(aOV)) {
            case 0:
                return 0.0f;
            case 2:
                return ((Float) bFf().mo5606d(new aCE(this, aOV, new Object[0]))).floatValue();
            case 3:
                bFf().mo5606d(new aCE(this, aOV, new Object[0]));
                break;
        }
        return m32248Tw();
    }

    /* renamed from: U */
    public C2491fm[] mo12U() {
        return _m_methods;
    }

    /* renamed from: W */
    public Object mo13W() {
        return new C6698aru(this);
    }

    /* renamed from: a */
    public Object mo14a(C0495Gr gr) {
        Object[] args = gr.getArgs();
        switch (gr.mo2417hq() - TaikodomObject._m_methodCount) {
            case 0:
                return new Float(m32277hg());
            case 1:
                return new Float(m32276hZ());
            case 2:
                return new Float(m32253a((DamageType) args[0], ((Float) args[1]).floatValue()));
            case 3:
                return new Float(m32252a((DamageType) args[0]));
            case 4:
                return new Float(m32244To());
            case 5:
                return new Float(m32278ib());
            case 6:
                m32255a(((Float) args[0]).floatValue(), (Actor) args[1]);
                return null;
            case 7:
                return new Integer(m32245Tq());
            case 8:
                return new Float(m32246Ts());
            case 9:
                return new Float(m32247Tu());
            case 10:
                return new Float(m32248Tw());
            case 11:
                m32249Ty();
                return null;
            case 12:
                m32270cJ(((Float) args[0]).floatValue());
                return null;
            case 13:
                return new Long(m32219TA());
            case 14:
                return new Boolean(m32220TC());
            case 15:
                m32261c(((Float) args[0]).floatValue(), (Actor) args[1]);
                return null;
            case 16:
                return new Float(m32254a((DamageType) args[0], ((Float) args[1]).floatValue(), (Actor) args[2]));
            case 17:
                return new Float(m32260c((DamageType) args[0], ((Float) args[1]).floatValue()));
            case 18:
                m32283qU();
                return null;
            case 19:
                return m32221TE();
            case 20:
                m32258aG();
                return null;
            case 21:
                m32222TG();
                return null;
            case 22:
                return m32224TI();
            case 23:
                m32256a((C0665JT) args[0]);
                return null;
            case 24:
                return new Float(m32274e((DamageType) args[0], ((Float) args[1]).floatValue()));
            case 25:
                return new Float(m32275g((DamageType) args[0]));
            case 26:
                m32280l((Ship) args[0]);
                return null;
            case 27:
                return m32218Mo();
            case 28:
                m32225TK();
                return null;
            case 29:
                m32226TM();
                return null;
            case 30:
                m32227TO();
                return null;
            case 31:
                m32229TQ();
                return null;
            default:
                return super.mo14a(gr);
        }
    }

    /* renamed from: a */
    public void mo15a(Collection collection, C0495Gr gr) {
        super.mo15a(collection, gr);
    }

    /* renamed from: aH */
    public void mo70aH() {
        switch (bFf().mo6893i(_f_onResurrect_0020_0028_0029V)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, _f_onResurrect_0020_0028_0029V, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, _f_onResurrect_0020_0028_0029V, new Object[0]));
                break;
        }
        m32258aG();
    }

    /* renamed from: al */
    public Ship mo19081al() {
        switch (bFf().mo6893i(f7735x851d656b)) {
            case 0:
                return null;
            case 2:
                return (Ship) bFf().mo5606d(new aCE(this, f7735x851d656b, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f7735x851d656b, new Object[0]));
                break;
        }
        return m32218Mo();
    }

    /* renamed from: b */
    public float mo19082b(DamageType fr) {
        switch (bFf().mo6893i(f7740uI)) {
            case 0:
                return 0.0f;
            case 2:
                return ((Float) bFf().mo5606d(new aCE(this, f7740uI, new Object[]{fr}))).floatValue();
            case 3:
                bFf().mo5606d(new aCE(this, f7740uI, new Object[]{fr}));
                break;
        }
        return m32252a(fr);
    }

    /* renamed from: b */
    public float mo19083b(DamageType fr, float f) {
        switch (bFf().mo6893i(f7736qa)) {
            case 0:
                return 0.0f;
            case 2:
                return ((Float) bFf().mo5606d(new aCE(this, f7736qa, new Object[]{fr, new Float(f)}))).floatValue();
            case 3:
                bFf().mo5606d(new aCE(this, f7736qa, new Object[]{fr, new Float(f)}));
                break;
        }
        return m32253a(fr, f);
    }

    @C5566aOg
    /* renamed from: b */
    public float mo19084b(DamageType fr, float f, Actor cr) {
        switch (bFf().mo6893i(aPb)) {
            case 0:
                return 0.0f;
            case 2:
                return ((Float) bFf().mo5606d(new aCE(this, aPb, new Object[]{fr, new Float(f), cr}))).floatValue();
            case 3:
                bFf().mo5606d(new aCE(this, aPb, new Object[]{fr, new Float(f), cr}));
                break;
        }
        return m32254a(fr, f, cr);
    }

    /* renamed from: b */
    public void mo19085b(float f, Actor cr) {
        switch (bFf().mo6893i(aOR)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, aOR, new Object[]{new Float(f), cr}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, aOR, new Object[]{new Float(f), cr}));
                break;
        }
        m32255a(f, cr);
    }

    /* renamed from: b */
    public void mo24b(C0665JT jt) {
        switch (bFf().mo6893i(f7733Do)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f7733Do, new Object[]{jt}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f7733Do, new Object[]{jt}));
                break;
        }
        m32256a(jt);
    }

    /* renamed from: c */
    public C5663aRz[] mo25c() {
        return _m_fields;
    }

    @C5566aOg
    /* renamed from: d */
    public float mo19086d(DamageType fr, float f) {
        switch (bFf().mo6893i(aPc)) {
            case 0:
                return 0.0f;
            case 2:
                return ((Float) bFf().mo5606d(new aCE(this, aPc, new Object[]{fr, new Float(f)}))).floatValue();
            case 3:
                bFf().mo5606d(new aCE(this, aPc, new Object[]{fr, new Float(f)}));
                break;
        }
        return m32260c(fr, f);
    }

    /* renamed from: f */
    public float mo19087f(DamageType fr, float f) {
        switch (bFf().mo6893i(aPg)) {
            case 0:
                return 0.0f;
            case 2:
                return ((Float) bFf().mo5606d(new aCE(this, aPg, new Object[]{fr, new Float(f)}))).floatValue();
            case 3:
                bFf().mo5606d(new aCE(this, aPg, new Object[]{fr, new Float(f)}));
                break;
        }
        return m32274e(fr, f);
    }

    /* renamed from: h */
    public float mo19088h(DamageType fr) {
        switch (bFf().mo6893i(aPh)) {
            case 0:
                return 0.0f;
            case 2:
                return ((Float) bFf().mo5606d(new aCE(this, aPh, new Object[]{fr}))).floatValue();
            case 3:
                bFf().mo5606d(new aCE(this, aPh, new Object[]{fr}));
                break;
        }
        return m32275g(fr);
    }

    /* renamed from: hh */
    public float mo19089hh() {
        switch (bFf().mo6893i(f7737qb)) {
            case 0:
                return 0.0f;
            case 2:
                return ((Float) bFf().mo5606d(new aCE(this, f7737qb, new Object[0]))).floatValue();
            case 3:
                bFf().mo5606d(new aCE(this, f7737qb, new Object[0]));
                break;
        }
        return m32277hg();
    }

    /* renamed from: ia */
    public float mo19090ia() {
        switch (bFf().mo6893i(f7738uG)) {
            case 0:
                return 0.0f;
            case 2:
                return ((Float) bFf().mo5606d(new aCE(this, f7738uG, new Object[0]))).floatValue();
            case 3:
                bFf().mo5606d(new aCE(this, f7738uG, new Object[0]));
                break;
        }
        return m32276hZ();
    }

    /* renamed from: ic */
    public float mo19091ic() {
        switch (bFf().mo6893i(f7739uH)) {
            case 0:
                return 0.0f;
            case 2:
                return ((Float) bFf().mo5606d(new aCE(this, f7739uH, new Object[0]))).floatValue();
            case 3:
                bFf().mo5606d(new aCE(this, f7739uH, new Object[0]));
                break;
        }
        return m32278ib();
    }

    @C5566aOg
    /* renamed from: m */
    public void mo19092m(Ship fAVar) {
        switch (bFf().mo6893i(aPi)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, aPi, new Object[]{fAVar}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, aPi, new Object[]{fAVar}));
                break;
        }
        m32280l(fAVar);
    }

    @C5566aOg
    @C2499fr
    /* renamed from: qV */
    public void mo19093qV() {
        switch (bFf().mo6893i(f7734Lm)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f7734Lm, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f7734Lm, new Object[0]));
                break;
        }
        m32283qU();
    }

    /* renamed from: a */
    public void mo19080a(ShieldType aph) {
        super.mo967a((C2961mJ) aph);
        m32269cI(m32230Ta());
        m32223TH();
    }

    @C0064Am(aul = "9279bc24e3b6fd9112180522a5519371", aum = 0)
    /* renamed from: hg */
    private float m32277hg() {
        return m32236Tg();
    }

    @C0064Am(aul = "160c395cc0cefa06e1fae5805972d0fc", aum = 0)
    /* renamed from: hZ */
    private float m32276hZ() {
        return m32237Th();
    }

    @C0064Am(aul = "b542e0a3453e621c10961f71e9552336", aum = 0)
    /* renamed from: a */
    private float m32253a(DamageType fr, float f) {
        return mo5353TJ().ase().mo3759b(fr, f);
    }

    @C0064Am(aul = "3ea8f57df1e6154182619b11acaf56f6", aum = 0)
    /* renamed from: a */
    private float m32252a(DamageType fr) {
        return ((Float) m32239Tj().get(fr)).floatValue();
    }

    @C0064Am(aul = "6c8271e00e549896ef3478780796ecec", aum = 0)
    /* renamed from: To */
    private float m32244To() {
        return (float) m32235Tf();
    }

    @C0064Am(aul = "13b148823518a8b7573513ea1074f914", aum = 0)
    /* renamed from: ib */
    private float m32278ib() {
        return m32238Ti();
    }

    @C0064Am(aul = "d00163860f77959ae80055b7f73f399f", aum = 0)
    /* renamed from: a */
    private void m32255a(float f, Actor cr) {
        m32271cK(mo19079Tx() + f);
        if (m32243Tn() != null && (m32243Tn().agj() instanceof Player)) {
            ((Player) m32243Tn().agj()).mo14419f((C1506WA) new C6556apI(f, cr));
        }
    }

    @C0064Am(aul = "0ea66aadbf268d0453ca7fbc9cbd152b", aum = 0)
    /* renamed from: Tq */
    private int m32245Tq() {
        return m32235Tf();
    }

    @C0064Am(aul = "a1b2898673330b1ac3e3c289fdf13768", aum = 0)
    /* renamed from: Ts */
    private float m32246Ts() {
        return m32232Tc();
    }

    @C0064Am(aul = "6ca4b0f4b88f399fc2944661ec8466fe", aum = 0)
    /* renamed from: Tu */
    private float m32247Tu() {
        return m32230Ta();
    }

    @C0064Am(aul = "ade4f380e6b131aa3d05aa465a1d8168", aum = 0)
    /* renamed from: Tw */
    private float m32248Tw() {
        float f;
        long cVr = cVr();
        long Tl = m32241Tl();
        if (m32242Tm()) {
            long ic = (long) (mo19091ic() * 1000.0f);
            if (cVr - m32241Tl() < ic) {
                return 0.0f;
            }
            if (bGZ()) {
                m32259aN(false);
                m32267cG(ic + m32241Tl());
            } else {
                m32250Tz();
                Tl += ic;
            }
        }
        float Tk = m32240Tk();
        float hh = mo19089hh();
        if (Tk >= hh) {
            f = hh;
        } else if (Tl > 0 && Tl < cVr) {
            float min = Math.min(hh, ((float) Math.round((((float) (cVr - Tl)) / 1000.0f) * mo19090ia())) + Tk);
            if (!bGZ()) {
                return min;
            }
            m32269cI(min);
            m32267cG(cVr);
            return min;
        } else if (Tl < 0) {
            m32267cG(cVr);
            f = Tk;
        } else {
            f = Tk;
        }
        if (!bGZ()) {
            return f;
        }
        m32269cI(f);
        m32267cG(cVr);
        return f;
    }

    @C0064Am(aul = "f55841512d6d955e8c9c5715710fbe37", aum = 0)
    /* renamed from: cJ */
    private void m32270cJ(float f) {
        if (f < 1.0E-5f) {
            m32269cI(0.0f);
            m32259aN(true);
        } else {
            if (mo19072TD() && f > 1.0f) {
                m32259aN(false);
            }
            if (f > mo19089hh()) {
                m32269cI(mo19089hh());
            } else {
                m32269cI(f);
            }
        }
        m32267cG(cVr());
    }

    @C0064Am(aul = "017d6cb31e6b3dfe603605dc344a7c03", aum = 0)
    /* renamed from: TA */
    private long m32219TA() {
        return m32241Tl();
    }

    @C0064Am(aul = "7b5eef4d964c3c1329157a3dd0f5cec4", aum = 0)
    /* renamed from: TC */
    private boolean m32220TC() {
        return m32242Tm();
    }

    @C0064Am(aul = "c5b906089d7858c68e609c0e23afa979", aum = 0)
    /* renamed from: c */
    private void m32261c(float f, Actor cr) {
        if (f < 0.0f) {
            mo8358lY("Causing negative damage, it will heal instead of damaging!");
            throw new IllegalStateException("Shield damage points must be positive");
        }
        if (f - mo19079Tx() > 1.0E-5f) {
            m32271cK(0.0f);
        } else {
            m32271cK(mo19079Tx() - f);
        }
        if (m32243Tn() != null && (m32243Tn().agj() instanceof Player)) {
            ((Player) m32243Tn().agj()).mo14419f((C1506WA) new C2814kV(f, C2814kV.C2815a.SHIELD, cr));
        }
    }

    @C0064Am(aul = "ae77ff793f24a69abf29e07c60a44efb", aum = 0)
    /* renamed from: TE */
    private ShieldType m32221TE() {
        return (ShieldType) getType();
    }

    @C0064Am(aul = "625f6874b58f4bcd2bd7812301a7f24c", aum = 0)
    /* renamed from: aG */
    private void m32258aG() {
        if (m32241Tl() == 0) {
            m32267cG(cVr());
        }
        super.mo70aH();
        m32223TH();
    }

    @C0064Am(aul = "9c1323978760a45d3d3405197d646df2", aum = 0)
    /* renamed from: TI */
    private AdapterLikedList<ShieldAdapter> m32224TI() {
        if (m32234Te() == null && bHa()) {
            m32223TH();
        }
        return m32234Te();
    }

    @C0064Am(aul = "b20fdb1ccfdf987314db92c9749f5651", aum = 0)
    /* renamed from: a */
    private void m32256a(C0665JT jt) {
        if (jt.mo3117j(2, 0, 0)) {
            try {
                Object obj = jt.get("healthPoints");
                if (obj instanceof Integer) {
                    m32269cI(((Integer) obj).floatValue());
                } else if ((obj instanceof Float) && Math.abs(((double) ((Float) obj).floatValue()) - 1.0E-6d) < 1.0d) {
                    C0665JT eE = jt.mo3113eE("type");
                    if (eE != null) {
                        m32269cI(((Float) eE.get("maxHealthPoints")).floatValue());
                    } else {
                        System.err.println("Shield onLoadVersion : type is null!");
                    }
                }
            } catch (NullPointerException e) {
                System.err.println("Shield : onLoadVersion " + e);
            }
        }
    }

    @C0064Am(aul = "c284f03aad2556cfb34cc819c54dbd22", aum = 0)
    /* renamed from: e */
    private float m32274e(DamageType fr, float f) {
        C1649YI yi = (C1649YI) m32233Td().get(fr);
        if (yi != null) {
            return yi.mo6928oI(f);
        }
        return 0.0f;
    }

    @C0064Am(aul = "141bb739433be91d45210a42a431c8c8", aum = 0)
    /* renamed from: g */
    private float m32275g(DamageType fr) {
        C1649YI yi = (C1649YI) m32233Td().get(fr);
        if (yi == null || yi.getValue() <= 0.0f) {
            return 0.0f;
        }
        return yi.getValue();
    }

    @C0064Am(aul = "1d67b8525fdcd637f04803033b4976de", aum = 0)
    /* renamed from: Mo */
    private Ship m32218Mo() {
        return m32243Tn();
    }

    @C0064Am(aul = "bf36b07d2d915efa7f1514cafe1296c7", aum = 0)
    /* renamed from: TK */
    private void m32225TK() {
        m32228TP();
    }

    @C0064Am(aul = "18d4fae9c426209a2cb3b568534840d3", aum = 0)
    /* renamed from: TM */
    private void m32226TM() {
        m32228TP();
    }

    @C0064Am(aul = "b4e0034465e2b657f7aee08eecbd15ab", aum = 0)
    /* renamed from: TO */
    private void m32227TO() {
        m32265cF(((ShieldAdapter) m32234Te().ase()).mo3761hh());
        m32268cH(((ShieldAdapter) m32234Te().ase()).mo3763ic());
        m32266cG(((ShieldAdapter) m32234Te().ase()).mo3762ia());
        for (DamageType next : ala().aLs()) {
            m32239Tj().put(next, Float.valueOf(((ShieldAdapter) m32234Te().ase()).mo3758b(next)));
        }
    }

    @C0064Am(aul = "4e2f542a284d610d8df4bcacde77ad19", aum = 0)
    /* renamed from: TQ */
    private void m32229TQ() {
        if (m32234Te() == null) {
            mo8358lY(String.valueOf(bFY()) + " with NULL adapterList");
            return;
        }
        m32234Te().mo23259b((C6872avM) this);
        m32228TP();
    }

    @C6485anp
    @C5511aMd
    /* renamed from: a.gj$a */
    public class BaseAdapter extends ShieldAdapter implements C1616Xf {
        /* renamed from: PE */
        public static final C2491fm f7742PE = null;
        /* renamed from: PF */
        public static final C2491fm f7743PF = null;
        public static final int _m_fieldCount = 0;
        public static final C5663aRz[] _m_fields = null;
        public static final int _m_methodCount = 0;
        public static final C2491fm[] _m_methods = null;
        /* renamed from: aT */
        public static final C5663aRz f7744aT = null;
        /* renamed from: qa */
        public static final C2491fm f7745qa = null;
        /* renamed from: qb */
        public static final C2491fm f7746qb = null;
        public static final long serialVersionUID = 0;
        /* renamed from: uG */
        public static final C2491fm f7747uG = null;
        /* renamed from: uH */
        public static final C2491fm f7748uH = null;
        /* renamed from: uI */
        public static final C2491fm f7749uI = null;
        public static C6494any ___iScriptClass;
        @C0064Am(aul = "39591f7c70f4438e982e67c7a980b3e9", aum = 0)

        /* renamed from: PD */
        static /* synthetic */ Shield f7741PD;

        static {
            m32319V();
        }

        public BaseAdapter() {
            super((C5540aNg) null);
            super.mo10S();
        }

        public BaseAdapter(C5540aNg ang) {
            super(ang);
        }

        public BaseAdapter(Shield gjVar) {
            super((C5540aNg) null);
            super._m_script_init(gjVar);
        }

        /* renamed from: V */
        static void m32319V() {
            serialVersionUID = (long) 1;
            _m_fieldCount = ShieldAdapter._m_fieldCount + 1;
            _m_methodCount = ShieldAdapter._m_methodCount + 7;
            int i = ShieldAdapter._m_fieldCount;
            C5663aRz[] arzArr = new C5663aRz[(i + 1)];
            C5663aRz b = C5640aRc.m17844b(BaseAdapter.class, "39591f7c70f4438e982e67c7a980b3e9", i);
            f7744aT = b;
            arzArr[i] = b;
            int i2 = i + 1;
            _m_fields = arzArr;
            C1634Xv.m11725a((Object[]) ShieldAdapter._m_fields, (Object[]) _m_fields);
            int i3 = ShieldAdapter._m_methodCount;
            C2491fm[] fmVarArr = new C2491fm[(i3 + 7)];
            C2491fm a = C4105zY.m41624a(BaseAdapter.class, "b4911b6ec9a67027b8a2604926f940ec", i3);
            f7745qa = a;
            fmVarArr[i3] = a;
            int i4 = i3 + 1;
            C2491fm a2 = C4105zY.m41624a(BaseAdapter.class, "6b408fd13ead556b73a2e47345dc32a5", i4);
            f7749uI = a2;
            fmVarArr[i4] = a2;
            int i5 = i4 + 1;
            C2491fm a3 = C4105zY.m41624a(BaseAdapter.class, "20be67bcaf839e2e9df955700d16bda0", i5);
            f7746qb = a3;
            fmVarArr[i5] = a3;
            int i6 = i5 + 1;
            C2491fm a4 = C4105zY.m41624a(BaseAdapter.class, "fd662352230d1d4360d650a7fbc5c604", i6);
            f7748uH = a4;
            fmVarArr[i6] = a4;
            int i7 = i6 + 1;
            C2491fm a5 = C4105zY.m41624a(BaseAdapter.class, "1caa1f42e18f5a69315c3c3c449ca5aa", i7);
            f7747uG = a5;
            fmVarArr[i7] = a5;
            int i8 = i7 + 1;
            C2491fm a6 = C4105zY.m41624a(BaseAdapter.class, "7b7911fde907964c2c1be5f39876292b", i8);
            f7742PE = a6;
            fmVarArr[i8] = a6;
            int i9 = i8 + 1;
            C2491fm a7 = C4105zY.m41624a(BaseAdapter.class, "34072e64245b587fa53c68a0e0215071", i9);
            f7743PF = a7;
            fmVarArr[i9] = a7;
            int i10 = i9 + 1;
            _m_methods = fmVarArr;
            C1634Xv.m11725a((Object[]) ShieldAdapter._m_methods, (Object[]) _m_methods);
            ___iScriptClass = aUO.m18566a(BaseAdapter.class, C6378alm.class, _m_fields, _m_methods);
        }

        @C0064Am(aul = "34072e64245b587fa53c68a0e0215071", aum = 0)
        /* renamed from: a */
        private void m32322a(GameObjectAdapter ato, GameObjectAdapter ato2, GameObjectAdapter ato3) {
            mo19094b((ShieldAdapter) ato, (ShieldAdapter) ato2, (ShieldAdapter) ato3);
        }

        /* renamed from: a */
        private void m32324a(Shield gjVar) {
            bFf().mo5608dq().mo3197f(f7744aT, gjVar);
        }

        /* renamed from: vp */
        private Shield m32328vp() {
            return (Shield) bFf().mo5608dq().mo3214p(f7744aT);
        }

        /* renamed from: S */
        public void mo10S() {
            super.mo10S();
        }

        /* renamed from: T */
        public C6494any mo11T() {
            return ___iScriptClass;
        }

        /* renamed from: U */
        public C2491fm[] mo12U() {
            return _m_methods;
        }

        /* renamed from: W */
        public Object mo13W() {
            return new C6378alm(this);
        }

        /* renamed from: a */
        public Object mo14a(C0495Gr gr) {
            Object[] args = gr.getArgs();
            switch (gr.mo2417hq() - ShieldAdapter._m_methodCount) {
                case 0:
                    return new Float(m32321a((DamageType) args[0], ((Float) args[1]).floatValue()));
                case 1:
                    return new Float(m32320a((DamageType) args[0]));
                case 2:
                    return new Float(m32326hg());
                case 3:
                    return new Float(m32327ib());
                case 4:
                    return new Float(m32325hZ());
                case 5:
                    m32323a((ShieldAdapter) args[0], (ShieldAdapter) args[1], (ShieldAdapter) args[2]);
                    return null;
                case 6:
                    m32322a((GameObjectAdapter) args[0], (GameObjectAdapter) args[1], (GameObjectAdapter) args[2]);
                    return null;
                default:
                    return super.mo14a(gr);
            }
        }

        /* renamed from: a */
        public void mo15a(Collection collection, C0495Gr gr) {
            super.mo15a(collection, gr);
        }

        /* renamed from: b */
        public float mo3758b(DamageType fr) {
            switch (bFf().mo6893i(f7749uI)) {
                case 0:
                    return 0.0f;
                case 2:
                    return ((Float) bFf().mo5606d(new aCE(this, f7749uI, new Object[]{fr}))).floatValue();
                case 3:
                    bFf().mo5606d(new aCE(this, f7749uI, new Object[]{fr}));
                    break;
            }
            return m32320a(fr);
        }

        /* renamed from: b */
        public float mo3759b(DamageType fr, float f) {
            switch (bFf().mo6893i(f7745qa)) {
                case 0:
                    return 0.0f;
                case 2:
                    return ((Float) bFf().mo5606d(new aCE(this, f7745qa, new Object[]{fr, new Float(f)}))).floatValue();
                case 3:
                    bFf().mo5606d(new aCE(this, f7745qa, new Object[]{fr, new Float(f)}));
                    break;
            }
            return m32321a(fr, f);
        }

        /* renamed from: b */
        public /* bridge */ /* synthetic */ void mo12950b(GameObjectAdapter ato, GameObjectAdapter ato2, GameObjectAdapter ato3) {
            switch (bFf().mo6893i(f7743PF)) {
                case 0:
                    return;
                case 2:
                    bFf().mo5606d(new aCE(this, f7743PF, new Object[]{ato, ato2, ato3}));
                    return;
                case 3:
                    bFf().mo5606d(new aCE(this, f7743PF, new Object[]{ato, ato2, ato3}));
                    break;
            }
            m32322a(ato, ato2, ato3);
        }

        /* renamed from: b */
        public void mo19094b(ShieldAdapter cyVar, ShieldAdapter cyVar2, ShieldAdapter cyVar3) {
            switch (bFf().mo6893i(f7742PE)) {
                case 0:
                    return;
                case 2:
                    bFf().mo5606d(new aCE(this, f7742PE, new Object[]{cyVar, cyVar2, cyVar3}));
                    return;
                case 3:
                    bFf().mo5606d(new aCE(this, f7742PE, new Object[]{cyVar, cyVar2, cyVar3}));
                    break;
            }
            m32323a(cyVar, cyVar2, cyVar3);
        }

        /* renamed from: c */
        public C5663aRz[] mo25c() {
            return _m_fields;
        }

        /* renamed from: hh */
        public float mo3761hh() {
            switch (bFf().mo6893i(f7746qb)) {
                case 0:
                    return 0.0f;
                case 2:
                    return ((Float) bFf().mo5606d(new aCE(this, f7746qb, new Object[0]))).floatValue();
                case 3:
                    bFf().mo5606d(new aCE(this, f7746qb, new Object[0]));
                    break;
            }
            return m32326hg();
        }

        /* renamed from: ia */
        public float mo3762ia() {
            switch (bFf().mo6893i(f7747uG)) {
                case 0:
                    return 0.0f;
                case 2:
                    return ((Float) bFf().mo5606d(new aCE(this, f7747uG, new Object[0]))).floatValue();
                case 3:
                    bFf().mo5606d(new aCE(this, f7747uG, new Object[0]));
                    break;
            }
            return m32325hZ();
        }

        /* renamed from: ic */
        public float mo3763ic() {
            switch (bFf().mo6893i(f7748uH)) {
                case 0:
                    return 0.0f;
                case 2:
                    return ((Float) bFf().mo5606d(new aCE(this, f7748uH, new Object[0]))).floatValue();
                case 3:
                    bFf().mo5606d(new aCE(this, f7748uH, new Object[0]));
                    break;
            }
            return m32327ib();
        }

        /* renamed from: b */
        public void mo19095b(Shield gjVar) {
            m32324a(gjVar);
            super.mo10S();
        }

        @C0064Am(aul = "b4911b6ec9a67027b8a2604926f940ec", aum = 0)
        /* renamed from: a */
        private float m32321a(DamageType fr, float f) {
            return m32328vp().mo19087f(fr, f);
        }

        @C0064Am(aul = "6b408fd13ead556b73a2e47345dc32a5", aum = 0)
        /* renamed from: a */
        private float m32320a(DamageType fr) {
            return m32328vp().mo19088h(fr);
        }

        @C0064Am(aul = "20be67bcaf839e2e9df955700d16bda0", aum = 0)
        /* renamed from: hg */
        private float m32326hg() {
            return m32328vp().mo19078Tv();
        }

        @C0064Am(aul = "fd662352230d1d4360d650a7fbc5c604", aum = 0)
        /* renamed from: ib */
        private float m32327ib() {
            return m32328vp().mo19077Tt();
        }

        @C0064Am(aul = "1caa1f42e18f5a69315c3c3c449ca5aa", aum = 0)
        /* renamed from: hZ */
        private float m32325hZ() {
            return (float) m32328vp().mo19076Tr();
        }

        @C0064Am(aul = "7b7911fde907964c2c1be5f39876292b", aum = 0)
        /* renamed from: a */
        private void m32323a(ShieldAdapter cyVar, ShieldAdapter cyVar2, ShieldAdapter cyVar3) {
            if (cyVar != null) {
                throw new IllegalArgumentException("Expecting prev=null but found " + cyVar.bFY());
            } else if (cyVar3 != this) {
                throw new IllegalArgumentException("Expecting base = " + bFY() + " but received " + cyVar3.bFY());
            } else {
                super.mo12950b(cyVar, cyVar2, cyVar3);
            }
        }
    }
}
