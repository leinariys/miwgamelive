package game.script.ship;

import game.network.message.externalizable.aCE;
import game.network.message.serializable.C1556Wo;
import game.script.damage.DamageType;
import logic.baa.*;
import logic.data.mbean.C7017aze;
import logic.res.code.C5640aRc;
import logic.res.code.C5663aRz;
import logic.res.code.C6494any;
import logic.res.html.C2491fm;
import p001a.*;

import java.util.Collection;
import java.util.Map;

@C0566Hp
@C5829abJ("2.1.0")
@C6485anp
@C5511aMd
/* renamed from: a.apH  reason: case insensitive filesystem */
/* compiled from: a */
public class ShieldType extends BaseShieldType implements C1616Xf {
    public static final int _m_fieldCount = 0;
    public static final C5663aRz[] _m_fields = null;
    public static final int _m_methodCount = 0;
    public static final C2491fm[] _m_methods = null;
    public static final C5663aRz aOp = null;
    public static final C5663aRz aOr = null;
    public static final C5663aRz aOt = null;
    public static final C5663aRz aOv = null;
    public static final C5663aRz aOz = null;
    public static final C2491fm bED = null;
    public static final C2491fm bEE = null;
    public static final C2491fm bEJ = null;
    /* renamed from: dN */
    public static final C2491fm f5088dN = null;
    public static final C2491fm gnb = null;
    public static final C2491fm gnc = null;
    public static final C2491fm gnd = null;
    public static final C2491fm gne = null;
    public static final C2491fm gnf = null;
    public static final C2491fm gng = null;
    /* renamed from: qb */
    public static final C2491fm f5089qb = null;
    public static final long serialVersionUID = 0;
    /* renamed from: uH */
    public static final C2491fm f5090uH = null;
    public static C6494any ___iScriptClass;
    @C0064Am(aul = "23202c729d1b4f1a7800cedcf32abe60", aum = 0)
    private static float aOo;
    @C0064Am(aul = "adfa61408e5a15fc39c27ecc3b264f8f", aum = 1)
    private static float aOq;
    @C0064Am(aul = "6f859584baff3b559afe4d5900292f72", aum = 2)
    private static float aOs;
    @C0064Am(aul = "db40ad939d4389bfe3aef3610634854e", aum = 3)
    private static C1556Wo<DamageType, C1649YI> aOu;
    @C0064Am(aul = "5b94964da3f1701a06571b4b08d4b12e", aum = 4)
    private static int aOy;

    static {
        m24812V();
    }

    public ShieldType() {
        super((C5540aNg) null);
        super.mo10S();
    }

    public ShieldType(C5540aNg ang) {
        super(ang);
    }

    /* renamed from: V */
    static void m24812V() {
        serialVersionUID = (long) 1;
        _m_fieldCount = BaseShieldType._m_fieldCount + 5;
        _m_methodCount = BaseShieldType._m_methodCount + 12;
        int i = BaseShieldType._m_fieldCount;
        C5663aRz[] arzArr = new C5663aRz[(i + 5)];
        C5663aRz b = C5640aRc.m17844b(ShieldType.class, "23202c729d1b4f1a7800cedcf32abe60", i);
        aOp = b;
        arzArr[i] = b;
        int i2 = i + 1;
        C5663aRz b2 = C5640aRc.m17844b(ShieldType.class, "adfa61408e5a15fc39c27ecc3b264f8f", i2);
        aOr = b2;
        arzArr[i2] = b2;
        int i3 = i2 + 1;
        C5663aRz b3 = C5640aRc.m17844b(ShieldType.class, "6f859584baff3b559afe4d5900292f72", i3);
        aOt = b3;
        arzArr[i3] = b3;
        int i4 = i3 + 1;
        C5663aRz b4 = C5640aRc.m17844b(ShieldType.class, "db40ad939d4389bfe3aef3610634854e", i4);
        aOv = b4;
        arzArr[i4] = b4;
        int i5 = i4 + 1;
        C5663aRz b5 = C5640aRc.m17844b(ShieldType.class, "5b94964da3f1701a06571b4b08d4b12e", i5);
        aOz = b5;
        arzArr[i5] = b5;
        int i6 = i5 + 1;
        _m_fields = arzArr;
        C1634Xv.m11725a((Object[]) BaseShieldType._m_fields, (Object[]) _m_fields);
        int i7 = BaseShieldType._m_methodCount;
        C2491fm[] fmVarArr = new C2491fm[(i7 + 12)];
        C2491fm a = C4105zY.m41624a(ShieldType.class, "6fd667ec156ca8af8f1dedf393a370f9", i7);
        f5089qb = a;
        fmVarArr[i7] = a;
        int i8 = i7 + 1;
        C2491fm a2 = C4105zY.m41624a(ShieldType.class, "85316935c848df1a4e23715b56e38190", i8);
        bEJ = a2;
        fmVarArr[i8] = a2;
        int i9 = i8 + 1;
        C2491fm a3 = C4105zY.m41624a(ShieldType.class, "0e81f620cf476761cb4942e67620420b", i9);
        gnb = a3;
        fmVarArr[i9] = a3;
        int i10 = i9 + 1;
        C2491fm a4 = C4105zY.m41624a(ShieldType.class, "e13aebab921156efc2f66153b7c5eeac", i10);
        gnc = a4;
        fmVarArr[i10] = a4;
        int i11 = i10 + 1;
        C2491fm a5 = C4105zY.m41624a(ShieldType.class, "a1e5b5d63a904dfdf60377f0a3cad933", i11);
        f5090uH = a5;
        fmVarArr[i11] = a5;
        int i12 = i11 + 1;
        C2491fm a6 = C4105zY.m41624a(ShieldType.class, "b6191d015f077081a92c5a20c8ba3f31", i12);
        gnd = a6;
        fmVarArr[i12] = a6;
        int i13 = i12 + 1;
        C2491fm a7 = C4105zY.m41624a(ShieldType.class, "3357d15c664fa2aaa1513cb03d6e1744", i13);
        bED = a7;
        fmVarArr[i13] = a7;
        int i14 = i13 + 1;
        C2491fm a8 = C4105zY.m41624a(ShieldType.class, "778898eb7791f25fe9520e2918823029", i14);
        bEE = a8;
        fmVarArr[i14] = a8;
        int i15 = i14 + 1;
        C2491fm a9 = C4105zY.m41624a(ShieldType.class, "97fa2445c4c6bf1ec4e4ad94926c804d", i15);
        gne = a9;
        fmVarArr[i15] = a9;
        int i16 = i15 + 1;
        C2491fm a10 = C4105zY.m41624a(ShieldType.class, "694a482c44b58e99a6a715fb840ef693", i16);
        gnf = a10;
        fmVarArr[i16] = a10;
        int i17 = i16 + 1;
        C2491fm a11 = C4105zY.m41624a(ShieldType.class, "99ab357236b64792b24268bc8ddee88d", i17);
        gng = a11;
        fmVarArr[i17] = a11;
        int i18 = i17 + 1;
        C2491fm a12 = C4105zY.m41624a(ShieldType.class, "7518ee2f6c0595b04430a58c55bcfbd5", i18);
        f5088dN = a12;
        fmVarArr[i18] = a12;
        int i19 = i18 + 1;
        _m_methods = fmVarArr;
        C1634Xv.m11725a((Object[]) BaseShieldType._m_methods, (Object[]) _m_methods);
        ___iScriptClass = aUO.m18566a(ShieldType.class, C7017aze.class, _m_fields, _m_methods);
    }

    /* renamed from: Ta */
    private float m24807Ta() {
        return bFf().mo5608dq().mo3211m(aOp);
    }

    /* renamed from: Tb */
    private float m24808Tb() {
        return bFf().mo5608dq().mo3211m(aOr);
    }

    /* renamed from: Tc */
    private float m24809Tc() {
        return bFf().mo5608dq().mo3211m(aOt);
    }

    /* renamed from: Td */
    private C1556Wo m24810Td() {
        return (C1556Wo) bFf().mo5608dq().mo3214p(aOv);
    }

    /* renamed from: Tf */
    private int m24811Tf() {
        return bFf().mo5608dq().mo3212n(aOz);
    }

    /* renamed from: cC */
    private void m24815cC(float f) {
        bFf().mo5608dq().mo3150a(aOp, f);
    }

    /* renamed from: cD */
    private void m24816cD(float f) {
        bFf().mo5608dq().mo3150a(aOr, f);
    }

    /* renamed from: cE */
    private void m24817cE(float f) {
        bFf().mo5608dq().mo3150a(aOt, f);
    }

    /* renamed from: dw */
    private void m24819dw(int i) {
        bFf().mo5608dq().mo3183b(aOz, i);
    }

    /* renamed from: m */
    private void m24824m(C1556Wo wo) {
        bFf().mo5608dq().mo3197f(aOv, wo);
    }

    /* renamed from: T */
    public C6494any mo11T() {
        return ___iScriptClass;
    }

    /* renamed from: U */
    public C2491fm[] mo12U() {
        return _m_methods;
    }

    /* renamed from: W */
    public Object mo13W() {
        return new C7017aze(this);
    }

    /* renamed from: a */
    public Object mo14a(C0495Gr gr) {
        Object[] args = gr.getArgs();
        switch (gr.mo2417hq() - BaseShieldType._m_methodCount) {
            case 0:
                return new Float(m24820hg());
            case 1:
                m24818dW(((Float) args[0]).floatValue());
                return null;
            case 2:
                return new Float(cpY());
            case 3:
                m24822kn(((Float) args[0]).floatValue());
                return null;
            case 4:
                return new Float(m24821ib());
            case 5:
                m24823kp(((Float) args[0]).floatValue());
                return null;
            case 6:
                return amZ();
            case 7:
                m24814c((Map) args[0]);
                return null;
            case 8:
                return new Integer(cqa());
            case 9:
                m24825ty(((Integer) args[0]).intValue());
                return null;
            case 10:
                return cqc();
            case 11:
                return m24813aT();
            default:
                return super.mo14a(gr);
        }
    }

    /* renamed from: a */
    public void mo15a(Collection collection, C0495Gr gr) {
        super.mo15a(collection, gr);
    }

    /* access modifiers changed from: protected */
    /* renamed from: aU */
    public <T extends aDJ> T mo745aU() {
        switch (bFf().mo6893i(f5088dN)) {
            case 0:
                return null;
            case 2:
                return (aDJ) bFf().mo5606d(new aCE(this, f5088dN, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f5088dN, new Object[0]));
                break;
        }
        return m24813aT();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Damage Resistance")
    public C1556Wo<DamageType, C1649YI> ana() {
        switch (bFf().mo6893i(bED)) {
            case 0:
                return null;
            case 2:
                return (C1556Wo) bFf().mo5606d(new aCE(this, bED, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, bED, new Object[0]));
                break;
        }
        return amZ();
    }

    /* renamed from: c */
    public C5663aRz[] mo25c() {
        return _m_fields;
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Damage Reduction")
    public float cpZ() {
        switch (bFf().mo6893i(gnb)) {
            case 0:
                return 0.0f;
            case 2:
                return ((Float) bFf().mo5606d(new aCE(this, gnb, new Object[0]))).floatValue();
            case 3:
                bFf().mo5606d(new aCE(this, gnb, new Object[0]));
                break;
        }
        return cpY();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Shield's HP / sec")
    public int cqb() {
        switch (bFf().mo6893i(gne)) {
            case 0:
                return 0;
            case 2:
                return ((Integer) bFf().mo5606d(new aCE(this, gne, new Object[0]))).intValue();
            case 3:
                bFf().mo5606d(new aCE(this, gne, new Object[0]));
                break;
        }
        return cqa();
    }

    public Shield cqd() {
        switch (bFf().mo6893i(gng)) {
            case 0:
                return null;
            case 2:
                return (Shield) bFf().mo5606d(new aCE(this, gng, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, gng, new Object[0]));
                break;
        }
        return cqc();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Damage Resistance")
    /* renamed from: d */
    public void mo15371d(Map<DamageType, C1649YI> map) {
        switch (bFf().mo6893i(bEE)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, bEE, new Object[]{map}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, bEE, new Object[]{map}));
                break;
        }
        m24814c(map);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Max Health Points")
    /* renamed from: dX */
    public void mo15372dX(float f) {
        switch (bFf().mo6893i(bEJ)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, bEJ, new Object[]{new Float(f)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, bEJ, new Object[]{new Float(f)}));
                break;
        }
        m24818dW(f);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Max Health Points")
    /* renamed from: hh */
    public float mo15373hh() {
        switch (bFf().mo6893i(f5089qb)) {
            case 0:
                return 0.0f;
            case 2:
                return ((Float) bFf().mo5606d(new aCE(this, f5089qb, new Object[0]))).floatValue();
            case 3:
                bFf().mo5606d(new aCE(this, f5089qb, new Object[0]));
                break;
        }
        return m24820hg();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Recovery Time")
    /* renamed from: ic */
    public float mo15374ic() {
        switch (bFf().mo6893i(f5090uH)) {
            case 0:
                return 0.0f;
            case 2:
                return ((Float) bFf().mo5606d(new aCE(this, f5090uH, new Object[0]))).floatValue();
            case 3:
                bFf().mo5606d(new aCE(this, f5090uH, new Object[0]));
                break;
        }
        return m24821ib();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Damage Reduction")
    /* renamed from: ko */
    public void mo15375ko(float f) {
        switch (bFf().mo6893i(gnc)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, gnc, new Object[]{new Float(f)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, gnc, new Object[]{new Float(f)}));
                break;
        }
        m24822kn(f);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Recovery Time")
    /* renamed from: kq */
    public void mo15376kq(float f) {
        switch (bFf().mo6893i(gnd)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, gnd, new Object[]{new Float(f)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, gnd, new Object[]{new Float(f)}));
                break;
        }
        m24823kp(f);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Shield's HP / sec")
    /* renamed from: tz */
    public void mo15377tz(int i) {
        switch (bFf().mo6893i(gnf)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, gnf, new Object[]{new Integer(i)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, gnf, new Object[]{new Integer(i)}));
                break;
        }
        m24825ty(i);
    }

    /* renamed from: S */
    public void mo10S() {
        super.mo10S();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Max Health Points")
    @C0064Am(aul = "6fd667ec156ca8af8f1dedf393a370f9", aum = 0)
    /* renamed from: hg */
    private float m24820hg() {
        return m24807Ta();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Max Health Points")
    @C0064Am(aul = "85316935c848df1a4e23715b56e38190", aum = 0)
    /* renamed from: dW */
    private void m24818dW(float f) {
        m24815cC(f);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Damage Reduction")
    @C0064Am(aul = "0e81f620cf476761cb4942e67620420b", aum = 0)
    private float cpY() {
        return m24808Tb();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Damage Reduction")
    @C0064Am(aul = "e13aebab921156efc2f66153b7c5eeac", aum = 0)
    /* renamed from: kn */
    private void m24822kn(float f) {
        m24816cD(f);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Recovery Time")
    @C0064Am(aul = "a1e5b5d63a904dfdf60377f0a3cad933", aum = 0)
    /* renamed from: ib */
    private float m24821ib() {
        return m24809Tc();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Recovery Time")
    @C0064Am(aul = "b6191d015f077081a92c5a20c8ba3f31", aum = 0)
    /* renamed from: kp */
    private void m24823kp(float f) {
        m24817cE(f);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Damage Resistance")
    @C0064Am(aul = "3357d15c664fa2aaa1513cb03d6e1744", aum = 0)
    private C1556Wo<DamageType, C1649YI> amZ() {
        return m24810Td();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Damage Resistance")
    @C0064Am(aul = "778898eb7791f25fe9520e2918823029", aum = 0)
    /* renamed from: c */
    private void m24814c(Map<DamageType, C1649YI> map) {
        m24810Td().clear();
        m24810Td().putAll(map);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Shield's HP / sec")
    @C0064Am(aul = "97fa2445c4c6bf1ec4e4ad94926c804d", aum = 0)
    private int cqa() {
        return m24811Tf();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Shield's HP / sec")
    @C0064Am(aul = "694a482c44b58e99a6a715fb840ef693", aum = 0)
    /* renamed from: ty */
    private void m24825ty(int i) {
        m24819dw(i);
    }

    @C0064Am(aul = "99ab357236b64792b24268bc8ddee88d", aum = 0)
    private Shield cqc() {
        return (Shield) mo745aU();
    }

    @C0064Am(aul = "7518ee2f6c0595b04430a58c55bcfbd5", aum = 0)
    /* renamed from: aT */
    private <T extends aDJ> T m24813aT() {
        T t = (Shield) bFf().mo6865M(Shield.class);
        t.mo19080a(this);
        return t;
    }
}
