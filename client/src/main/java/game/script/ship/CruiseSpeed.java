package game.script.ship;

import game.network.message.externalizable.aCE;
import game.script.TaikodomObject;
import game.script.TaskletImpl;
import game.script.nls.NLSCruiseSpeed;
import game.script.nls.NLSManager;
import game.script.nls.NLSWindowAlert;
import game.script.player.Player;
import game.script.resource.Asset;
import game.script.template.BaseTaikodomContent;
import game.script.util.AdapterLikedList;
import game.script.util.GameObjectAdapter;
import logic.aaa.C0284Df;
import logic.aaa.C1506WA;
import logic.aaa.C3504rh;
import logic.baa.*;
import logic.data.mbean.C0062Ak;
import logic.data.mbean.C0973OK;
import logic.data.mbean.C6286ajy;
import logic.data.mbean.aCM;
import logic.res.code.C5640aRc;
import logic.res.code.C5663aRz;
import logic.res.code.C6494any;
import logic.res.html.C2491fm;
import p001a.*;

import java.util.Collection;

@C5829abJ("1.1.0")
@C6485anp
@C2712iu(mo19786Bt = BaseTaikodomContent.class, version = "1.1.0")
@C5511aMd
/* renamed from: a.fk */
/* compiled from: a */
public class CruiseSpeed extends TaikodomObject implements C6480ank<CruiseSpeedAdapter>, C6809auB, C6872avM, C6872avM {

    /* renamed from: Lm */
    public static final C2491fm f7437Lm = null;
    public static final C2491fm _f_dispose_0020_0028_0029V = null;
    /* renamed from: _f_getShip_0020_0028_0029Ltaikodom_002fgame_002fscript_002fship_002fShip_003b */
    public static final C2491fm f7438x851d656b = null;
    public static final int _m_fieldCount = 0;
    public static final C5663aRz[] _m_fields = null;
    public static final int _m_methodCount = 0;
    public static final C2491fm[] _m_methods = null;
    public static final C5663aRz aOP = null;
    public static final C5663aRz aOx = null;
    public static final C2491fm aPf = null;
    public static final C2491fm aPj = null;
    public static final C2491fm aPk = null;
    public static final C2491fm aPl = null;
    public static final C2491fm aPm = null;
    public static final C2491fm aTe = null;
    public static final C2491fm aTf = null;
    public static final C2491fm aTg = null;
    public static final C2491fm aTh = null;
    public static final C2491fm bFx = null;
    public static final C2491fm bFy = null;
    public static final C2491fm bbS = null;
    public static final C2491fm bbU = null;
    public static final C5663aRz bid = null;
    public static final C2491fm bka = null;
    public static final C2491fm bkd = null;
    public static final C2491fm bkk = null;
    public static final C2491fm bkl = null;
    public static final C5663aRz bnO = null;
    public static final C5663aRz bnP = null;
    public static final C5663aRz bnU = null;
    public static final C5663aRz bnV = null;
    public static final C2491fm bpF = null;
    public static final C2491fm bqQ = null;
    public static final C2491fm bqf = null;
    public static final C2491fm bqk = null;
    public static final C2491fm bqq = null;
    public static final C2491fm bqr = null;
    public static final C2491fm bqs = null;
    public static final C2491fm bqt = null;
    public static final C2491fm cRZ = null;
    public static final C5663aRz cwA = null;
    public static final C5663aRz cwC = null;
    public static final C5663aRz dGv = null;
    public static final C5663aRz dGx = null;
    public static final C5663aRz icD = null;
    public static final C5663aRz icE = null;
    public static final C5663aRz icF = null;
    public static final C5663aRz icG = null;
    public static final C5663aRz isQ = null;
    public static final C5663aRz isR = null;
    public static final C5663aRz isS = null;
    public static final C5663aRz isT = null;
    public static final C5663aRz isU = null;
    public static final C2491fm isV = null;
    public static final C2491fm isW = null;
    public static final C2491fm isX = null;
    public static final C2491fm isY = null;
    public static final C2491fm isZ = null;
    public static final C2491fm ita = null;
    public static final C2491fm itb = null;
    public static final C2491fm itc = null;
    public static final C2491fm itd = null;
    public static final C2491fm ite = null;
    public static final long serialVersionUID = 0;
    /* renamed from: yg */
    public static final C2491fm f7441yg = null;
    public static C6494any ___iScriptClass;
    @C0064Am(aul = "67e4250ce1c54b1b6c3e3288b736c014", aum = 9)
    private static Ship aOO;
    @C0064Am(aul = "a931f9b86484384308beeb65c81a94c5", aum = 8)
    @C5566aOg
    private static AdapterLikedList<CruiseSpeedAdapter> aOw;
    @C0064Am(aul = "f14842262b8609dc4b3e49b64a12a287", aum = 10)
    private static C6809auB.C1996a aZI;
    @C0064Am(aul = "4d6683c976867ec01158de5fbb6e90f5", aum = 6)
    private static Asset bIu;
    @C0064Am(aul = "63725efd38f3ce1ca5d7293ecf7a11f2", aum = 0)
    private static float bhk;
    @C0064Am(aul = "d147e074b9f94c92df736c31f1c47782", aum = 2)
    private static float bhl;
    @C0064Am(aul = "a915d5c11ac322c96038b12c63b3b0f6", aum = 4)
    private static float dGt;
    @C0064Am(aul = "087dc22806623789b3ac097158849077", aum = 5)
    private static float dGu;
    @C0064Am(aul = "3b39c4adb33740ec91e373e8d3daab03", aum = 7)
    private static Asset dGw;
    @C0064Am(aul = "18a726e0f96018399dedabd4342c8b1b", aum = 11)
    private static CruiseSpeedEnterer dOj;
    @C0064Am(aul = "8babc8e0d40d441f8bdbbdbe8736c58e", aum = 12)
    private static CruiseSpeedExiter dOk;
    @C0064Am(aul = "410a07f095a826d99c9ebad79bff1d03", aum = 13)
    private static float dOl;
    @C0064Am(aul = "cde325ca66891d7cec8abca06d4b3ce6", aum = 14)
    private static float dOm;
    @C0064Am(aul = "4ce36acb37c7587ae74fe699b9176371", aum = 15)
    private static float dOn;
    @C0064Am(aul = "1b3981b40b29780f471967de0a94e28f", aum = 16)
    private static float dOo;
    @C0064Am(aul = "e6c0e950048aa847547969094e873871", aum = 17)
    private static float dOp;
    @C0064Am(aul = "9aaaa0648e9ef89134f53486f06b4070", aum = 18)
    private static float dOq;
    @C0064Am(aul = "168a3cc04dfa5cfe5d35b5c8e7e34934", aum = 19)
    private static boolean dOr;
    @C0064Am(aul = "da77fcd00e651d6a6e873a34348c7790", aum = 1)

    /* renamed from: dW */
    private static float f7439dW;
    @C0064Am(aul = "2e2ccdfa86f57eff33c8d732623d9fe5", aum = 3)

    /* renamed from: dX */
    private static float f7440dX;

    static {
        m31307V();
    }

    private transient int hFe;

    public CruiseSpeed() {
        super((C5540aNg) null);
        super.mo10S();
    }

    public CruiseSpeed(CruiseSpeedType nf) {
        super((C5540aNg) null);
        super._m_script_init(nf);
    }

    public CruiseSpeed(C5540aNg ang) {
        super(ang);
    }

    /* renamed from: V */
    static void m31307V() {
        serialVersionUID = (long) 1;
        _m_fieldCount = TaikodomObject._m_fieldCount + 20;
        _m_methodCount = TaikodomObject._m_methodCount + 40;
        int i = TaikodomObject._m_fieldCount;
        C5663aRz[] arzArr = new C5663aRz[(i + 20)];
        C5663aRz b = C5640aRc.m17844b(CruiseSpeed.class, "63725efd38f3ce1ca5d7293ecf7a11f2", i);
        bnU = b;
        arzArr[i] = b;
        int i2 = i + 1;
        C5663aRz b2 = C5640aRc.m17844b(CruiseSpeed.class, "da77fcd00e651d6a6e873a34348c7790", i2);
        bnV = b2;
        arzArr[i2] = b2;
        int i3 = i2 + 1;
        C5663aRz b3 = C5640aRc.m17844b(CruiseSpeed.class, "d147e074b9f94c92df736c31f1c47782", i3);
        bnO = b3;
        arzArr[i3] = b3;
        int i4 = i3 + 1;
        C5663aRz b4 = C5640aRc.m17844b(CruiseSpeed.class, "2e2ccdfa86f57eff33c8d732623d9fe5", i4);
        bnP = b4;
        arzArr[i4] = b4;
        int i5 = i4 + 1;
        C5663aRz b5 = C5640aRc.m17844b(CruiseSpeed.class, "a915d5c11ac322c96038b12c63b3b0f6", i5);
        cwC = b5;
        arzArr[i5] = b5;
        int i6 = i5 + 1;
        C5663aRz b6 = C5640aRc.m17844b(CruiseSpeed.class, "087dc22806623789b3ac097158849077", i6);
        cwA = b6;
        arzArr[i6] = b6;
        int i7 = i6 + 1;
        C5663aRz b7 = C5640aRc.m17844b(CruiseSpeed.class, "4d6683c976867ec01158de5fbb6e90f5", i7);
        dGv = b7;
        arzArr[i7] = b7;
        int i8 = i7 + 1;
        C5663aRz b8 = C5640aRc.m17844b(CruiseSpeed.class, "3b39c4adb33740ec91e373e8d3daab03", i8);
        dGx = b8;
        arzArr[i8] = b8;
        int i9 = i8 + 1;
        C5663aRz b9 = C5640aRc.m17844b(CruiseSpeed.class, "a931f9b86484384308beeb65c81a94c5", i9);
        aOx = b9;
        arzArr[i9] = b9;
        int i10 = i9 + 1;
        C5663aRz b10 = C5640aRc.m17844b(CruiseSpeed.class, "67e4250ce1c54b1b6c3e3288b736c014", i10);
        aOP = b10;
        arzArr[i10] = b10;
        int i11 = i10 + 1;
        C5663aRz b11 = C5640aRc.m17844b(CruiseSpeed.class, "f14842262b8609dc4b3e49b64a12a287", i11);
        bid = b11;
        arzArr[i11] = b11;
        int i12 = i11 + 1;
        C5663aRz b12 = C5640aRc.m17844b(CruiseSpeed.class, "18a726e0f96018399dedabd4342c8b1b", i12);
        isQ = b12;
        arzArr[i12] = b12;
        int i13 = i12 + 1;
        C5663aRz b13 = C5640aRc.m17844b(CruiseSpeed.class, "8babc8e0d40d441f8bdbbdbe8736c58e", i13);
        isR = b13;
        arzArr[i13] = b13;
        int i14 = i13 + 1;
        C5663aRz b14 = C5640aRc.m17844b(CruiseSpeed.class, "410a07f095a826d99c9ebad79bff1d03", i14);
        icE = b14;
        arzArr[i14] = b14;
        int i15 = i14 + 1;
        C5663aRz b15 = C5640aRc.m17844b(CruiseSpeed.class, "cde325ca66891d7cec8abca06d4b3ce6", i15);
        icD = b15;
        arzArr[i15] = b15;
        int i16 = i15 + 1;
        C5663aRz b16 = C5640aRc.m17844b(CruiseSpeed.class, "4ce36acb37c7587ae74fe699b9176371", i16);
        icF = b16;
        arzArr[i16] = b16;
        int i17 = i16 + 1;
        C5663aRz b17 = C5640aRc.m17844b(CruiseSpeed.class, "1b3981b40b29780f471967de0a94e28f", i17);
        icG = b17;
        arzArr[i17] = b17;
        int i18 = i17 + 1;
        C5663aRz b18 = C5640aRc.m17844b(CruiseSpeed.class, "e6c0e950048aa847547969094e873871", i18);
        isS = b18;
        arzArr[i18] = b18;
        int i19 = i18 + 1;
        C5663aRz b19 = C5640aRc.m17844b(CruiseSpeed.class, "9aaaa0648e9ef89134f53486f06b4070", i19);
        isT = b19;
        arzArr[i19] = b19;
        int i20 = i19 + 1;
        C5663aRz b20 = C5640aRc.m17844b(CruiseSpeed.class, "168a3cc04dfa5cfe5d35b5c8e7e34934", i20);
        isU = b20;
        arzArr[i20] = b20;
        int i21 = i20 + 1;
        _m_fields = arzArr;
        C1634Xv.m11725a((Object[]) TaikodomObject._m_fields, (Object[]) _m_fields);
        int i22 = TaikodomObject._m_methodCount;
        C2491fm[] fmVarArr = new C2491fm[(i22 + 40)];
        C2491fm a = C4105zY.m41624a(CruiseSpeed.class, "5bf19ba770b0cc837ad7e6e53212e7b9", i22);
        bkl = a;
        fmVarArr[i22] = a;
        int i23 = i22 + 1;
        C2491fm a2 = C4105zY.m41624a(CruiseSpeed.class, "de259ef249f74a89f516c686b268bcad", i23);
        bqQ = a2;
        fmVarArr[i23] = a2;
        int i24 = i23 + 1;
        C2491fm a3 = C4105zY.m41624a(CruiseSpeed.class, "7f9b494a1bd979275e75869c24910726", i24);
        bqk = a3;
        fmVarArr[i24] = a3;
        int i25 = i24 + 1;
        C2491fm a4 = C4105zY.m41624a(CruiseSpeed.class, "8287b1c96c55eb78266b6a8cf2760c4f", i25);
        bbU = a4;
        fmVarArr[i25] = a4;
        int i26 = i25 + 1;
        C2491fm a5 = C4105zY.m41624a(CruiseSpeed.class, "fff7ba4d2b0bc4208deb365ec54d2bcb", i26);
        bbS = a5;
        fmVarArr[i26] = a5;
        int i27 = i26 + 1;
        C2491fm a6 = C4105zY.m41624a(CruiseSpeed.class, "b334e2c52864e2678bdaf26ec966fc14", i27);
        f7441yg = a6;
        fmVarArr[i27] = a6;
        int i28 = i27 + 1;
        C2491fm a7 = C4105zY.m41624a(CruiseSpeed.class, "ba264abf215cec608bd7c0a26e3771b2", i28);
        isV = a7;
        fmVarArr[i28] = a7;
        int i29 = i28 + 1;
        C2491fm a8 = C4105zY.m41624a(CruiseSpeed.class, "3579acffda735f5e6ea72e87f898cf95", i29);
        aPf = a8;
        fmVarArr[i29] = a8;
        int i30 = i29 + 1;
        C2491fm a9 = C4105zY.m41624a(CruiseSpeed.class, "17a6690b52a14360d45655f2cc9d5c2f", i30);
        bqt = a9;
        fmVarArr[i30] = a9;
        int i31 = i30 + 1;
        C2491fm a10 = C4105zY.m41624a(CruiseSpeed.class, "449de15d20a88f6725d5743646af440c", i31);
        bqs = a10;
        fmVarArr[i31] = a10;
        int i32 = i31 + 1;
        C2491fm a11 = C4105zY.m41624a(CruiseSpeed.class, "11065df130d965a14225bbc6e9d9bdd0", i32);
        bqr = a11;
        fmVarArr[i32] = a11;
        int i33 = i32 + 1;
        C2491fm a12 = C4105zY.m41624a(CruiseSpeed.class, "e68c5eca6ee59039032126c6ddef6e2e", i33);
        bqq = a12;
        fmVarArr[i33] = a12;
        int i34 = i33 + 1;
        C2491fm a13 = C4105zY.m41624a(CruiseSpeed.class, "bddbc22788283fc7f75de17fe441afe9", i34);
        isW = a13;
        fmVarArr[i34] = a13;
        int i35 = i34 + 1;
        C2491fm a14 = C4105zY.m41624a(CruiseSpeed.class, "afdfb8b83258dac18a075c3c8940be6a", i35);
        isX = a14;
        fmVarArr[i35] = a14;
        int i36 = i35 + 1;
        C2491fm a15 = C4105zY.m41624a(CruiseSpeed.class, "d570bc0b5cc79d36e45559161100e071", i36);
        isY = a15;
        fmVarArr[i36] = a15;
        int i37 = i36 + 1;
        C2491fm a16 = C4105zY.m41624a(CruiseSpeed.class, "4f3147766715e74f9ab4337cdca6cc42", i37);
        aTg = a16;
        fmVarArr[i37] = a16;
        int i38 = i37 + 1;
        C2491fm a17 = C4105zY.m41624a(CruiseSpeed.class, "1ba5eb9b8f77cd71ecb4e9cfbdfee71d", i38);
        aTh = a17;
        fmVarArr[i38] = a17;
        int i39 = i38 + 1;
        C2491fm a18 = C4105zY.m41624a(CruiseSpeed.class, "11c640c3bc7b178ae7a04dc23aacc5d7", i39);
        aTe = a18;
        fmVarArr[i39] = a18;
        int i40 = i39 + 1;
        C2491fm a19 = C4105zY.m41624a(CruiseSpeed.class, "ef5435d33773d60f369d75ab312c226e", i40);
        aTf = a19;
        fmVarArr[i40] = a19;
        int i41 = i40 + 1;
        C2491fm a20 = C4105zY.m41624a(CruiseSpeed.class, "572d4627e9fc1e08f7cbb30bb6ae9f11", i41);
        bFx = a20;
        fmVarArr[i41] = a20;
        int i42 = i41 + 1;
        C2491fm a21 = C4105zY.m41624a(CruiseSpeed.class, "bff5308ec2a82fcd947ac700a294fb17", i42);
        isZ = a21;
        fmVarArr[i42] = a21;
        int i43 = i42 + 1;
        C2491fm a22 = C4105zY.m41624a(CruiseSpeed.class, "9a9a0e047ade0ae8d5cc67811ce4e92c", i43);
        bFy = a22;
        fmVarArr[i43] = a22;
        int i44 = i43 + 1;
        C2491fm a23 = C4105zY.m41624a(CruiseSpeed.class, "618e0669ca251f571c0f3953200d46e8", i44);
        ita = a23;
        fmVarArr[i44] = a23;
        int i45 = i44 + 1;
        C2491fm a24 = C4105zY.m41624a(CruiseSpeed.class, "5a095f34ffe4e0ba33ce917c95095b9c", i45);
        cRZ = a24;
        fmVarArr[i45] = a24;
        int i46 = i45 + 1;
        C2491fm a25 = C4105zY.m41624a(CruiseSpeed.class, "96bc133e704aa2556af095381bfc7674", i46);
        f7437Lm = a25;
        fmVarArr[i46] = a25;
        int i47 = i46 + 1;
        C2491fm a26 = C4105zY.m41624a(CruiseSpeed.class, "a4bf55094c87aaab7a3f3523753f54a4", i47);
        bka = a26;
        fmVarArr[i47] = a26;
        int i48 = i47 + 1;
        C2491fm a27 = C4105zY.m41624a(CruiseSpeed.class, "0c0283a0a70b6856f1ad2f6327a36b3c", i48);
        itb = a27;
        fmVarArr[i48] = a27;
        int i49 = i48 + 1;
        C2491fm a28 = C4105zY.m41624a(CruiseSpeed.class, "8758293b88ba5b09f577b95d8b09c544", i49);
        itc = a28;
        fmVarArr[i49] = a28;
        int i50 = i49 + 1;
        C2491fm a29 = C4105zY.m41624a(CruiseSpeed.class, "4dbd63df46fba6d39d9eab1c99a59c8c", i50);
        bpF = a29;
        fmVarArr[i50] = a29;
        int i51 = i50 + 1;
        C2491fm a30 = C4105zY.m41624a(CruiseSpeed.class, "db219b64d12690d1f43866399cffe21a", i51);
        bkd = a30;
        fmVarArr[i51] = a30;
        int i52 = i51 + 1;
        C2491fm a31 = C4105zY.m41624a(CruiseSpeed.class, "1bdd3213cbadb9a250d3c40fb9d88e5b", i52);
        itd = a31;
        fmVarArr[i52] = a31;
        int i53 = i52 + 1;
        C2491fm a32 = C4105zY.m41624a(CruiseSpeed.class, "f3e1e51864c54d73b29989a897edbb94", i53);
        ite = a32;
        fmVarArr[i53] = a32;
        int i54 = i53 + 1;
        C2491fm a33 = C4105zY.m41624a(CruiseSpeed.class, "35051cfec3c4c051239a84522ce07b38", i54);
        bkk = a33;
        fmVarArr[i54] = a33;
        int i55 = i54 + 1;
        C2491fm a34 = C4105zY.m41624a(CruiseSpeed.class, "e8a5c4f7131d452118ea1a298ceb139c", i55);
        bqf = a34;
        fmVarArr[i55] = a34;
        int i56 = i55 + 1;
        C2491fm a35 = C4105zY.m41624a(CruiseSpeed.class, "3e04e3cc04be60f8cc548b3227a16e9a", i56);
        f7438x851d656b = a35;
        fmVarArr[i56] = a35;
        int i57 = i56 + 1;
        C2491fm a36 = C4105zY.m41624a(CruiseSpeed.class, "74628368efae2cdb0a38d9ec79f05ef7", i57);
        aPl = a36;
        fmVarArr[i57] = a36;
        int i58 = i57 + 1;
        C2491fm a37 = C4105zY.m41624a(CruiseSpeed.class, "3abab0f40ec40da0808995c0390ae1da", i58);
        aPj = a37;
        fmVarArr[i58] = a37;
        int i59 = i58 + 1;
        C2491fm a38 = C4105zY.m41624a(CruiseSpeed.class, "d8860b173ab3631be3885fc84386fad7", i59);
        aPk = a38;
        fmVarArr[i59] = a38;
        int i60 = i59 + 1;
        C2491fm a39 = C4105zY.m41624a(CruiseSpeed.class, "5d5c566e72f262c27f3ce04ba8cce1e1", i60);
        _f_dispose_0020_0028_0029V = a39;
        fmVarArr[i60] = a39;
        int i61 = i60 + 1;
        C2491fm a40 = C4105zY.m41624a(CruiseSpeed.class, "c958c6f3392ddd0638442ddabcbd212f", i61);
        aPm = a40;
        fmVarArr[i61] = a40;
        int i62 = i61 + 1;
        _m_methods = fmVarArr;
        C1634Xv.m11725a((Object[]) TaikodomObject._m_methods, (Object[]) _m_methods);
        ___iScriptClass = aUO.m18566a(CruiseSpeed.class, C0973OK.class, _m_fields, _m_methods);
    }

    /* renamed from: Te */
    private AdapterLikedList m31305Te() {
        return (AdapterLikedList) bFf().mo5608dq().mo3214p(aOx);
    }

    /* renamed from: Tn */
    private Ship m31306Tn() {
        return (Ship) bFf().mo5608dq().mo3214p(aOP);
    }

    /* renamed from: a */
    private void m31314a(C6809auB.C1996a aVar) {
        bFf().mo5608dq().mo3197f(bid, aVar);
    }

    /* renamed from: a */
    private void m31315a(CruiseSpeedExiter aVar) {
        bFf().mo5608dq().mo3197f(isR, aVar);
    }

    /* renamed from: a */
    private void m31316a(CruiseSpeedEnterer cVar) {
        bFf().mo5608dq().mo3197f(isQ, cVar);
    }

    /* renamed from: a */
    private void m31318a(AdapterLikedList zAVar) {
        bFf().mo5608dq().mo3197f(aOx, zAVar);
    }

    /* renamed from: aJ */
    private void m31319aJ(Asset tCVar) {
        throw new C6039afL();
    }

    /* renamed from: aK */
    private void m31320aK(Asset tCVar) {
        throw new C6039afL();
    }

    @C0064Am(aul = "a4bf55094c87aaab7a3f3523753f54a4", aum = 0)
    @C5566aOg
    private void acK() {
        throw new aWi(new aCE(this, bka, new Object[0]));
    }

    @C4034yP
    @C5566aOg
    @C0064Am(aul = "db219b64d12690d1f43866399cffe21a", aum = 0)
    @C2499fr
    private void acP() {
        throw new aWi(new aCE(this, bkd, new Object[0]));
    }

    private C6809auB.C1996a acy() {
        return (C6809auB.C1996a) bFf().mo5608dq().mo3214p(bid);
    }

    private float aeO() {
        return ((CruiseSpeedType) getType()).mo4127VF();
    }

    private float aeP() {
        return ((CruiseSpeedType) getType()).mo4128VH();
    }

    private float aeS() {
        return ((CruiseSpeedType) getType()).mo4143rb();
    }

    private float aeT() {
        return ((CruiseSpeedType) getType()).mo4142ra();
    }

    private float bjP() {
        return ((CruiseSpeedType) getType()).anr();
    }

    private float bjQ() {
        return ((CruiseSpeedType) getType()).ant();
    }

    private Asset bjR() {
        return ((CruiseSpeedType) getType()).bjU();
    }

    private Asset bjS() {
        return ((CruiseSpeedType) getType()).bjW();
    }

    /* renamed from: da */
    private void m31325da(long j) {
        switch (bFf().mo6893i(bpF)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, bpF, new Object[]{new Long(j)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, bpF, new Object[]{new Long(j)}));
                break;
        }
        m31322cZ(j);
    }

    private CruiseSpeedEnterer djO() {
        return (CruiseSpeedEnterer) bFf().mo5608dq().mo3214p(isQ);
    }

    private CruiseSpeedExiter djP() {
        return (CruiseSpeedExiter) bFf().mo5608dq().mo3214p(isR);
    }

    private float djQ() {
        return bFf().mo5608dq().mo3211m(icE);
    }

    private float djR() {
        return bFf().mo5608dq().mo3211m(icD);
    }

    private float djS() {
        return bFf().mo5608dq().mo3211m(icF);
    }

    private float djT() {
        return bFf().mo5608dq().mo3211m(icG);
    }

    private float djU() {
        return bFf().mo5608dq().mo3211m(isS);
    }

    private float djV() {
        return bFf().mo5608dq().mo3211m(isT);
    }

    private boolean djW() {
        return bFf().mo5608dq().mo3201h(isU);
    }

    @C0064Am(aul = "1bdd3213cbadb9a250d3c40fb9d88e5b", aum = 0)
    @C5566aOg
    private void dkd() {
        throw new aWi(new aCE(this, itd, new Object[0]));
    }

    /* access modifiers changed from: private */
    @C5566aOg
    public void dke() {
        switch (bFf().mo6893i(itd)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, itd, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, itd, new Object[0]));
                break;
        }
        dkd();
    }

    /* renamed from: dt */
    private void m31326dt(float f) {
        throw new C6039afL();
    }

    /* renamed from: du */
    private void m31327du(float f) {
        throw new C6039afL();
    }

    /* renamed from: dx */
    private void m31328dx(float f) {
        throw new C6039afL();
    }

    /* renamed from: dy */
    private void m31329dy(float f) {
        throw new C6039afL();
    }

    /* renamed from: gy */
    private void m31331gy(float f) {
        throw new C6039afL();
    }

    /* renamed from: gz */
    private void m31332gz(float f) {
        throw new C6039afL();
    }

    /* renamed from: jS */
    private void m31333jS(boolean z) {
        bFf().mo5608dq().mo3153a(isU, z);
    }

    @C0064Am(aul = "8758293b88ba5b09f577b95d8b09c544", aum = 0)
    @C5566aOg
    /* renamed from: k */
    private void m31335k(int i, long j) {
        throw new aWi(new aCE(this, itc, new Object[]{new Integer(i), new Long(j)}));
    }

    /* renamed from: k */
    private void m31336k(Ship fAVar) {
        bFf().mo5608dq().mo3197f(aOP, fAVar);
    }

    @C5566aOg
    /* renamed from: l */
    private void m31337l(int i, long j) {
        switch (bFf().mo6893i(itc)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, itc, new Object[]{new Integer(i), new Long(j)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, itc, new Object[]{new Integer(i), new Long(j)}));
                break;
        }
        m31335k(i, j);
    }

    @C0064Am(aul = "0c0283a0a70b6856f1ad2f6327a36b3c", aum = 0)
    @C5566aOg
    /* renamed from: ln */
    private void m31340ln(long j) {
        throw new aWi(new aCE(this, itb, new Object[]{new Long(j)}));
    }

    /* renamed from: nf */
    private void m31341nf(float f) {
        bFf().mo5608dq().mo3150a(icE, f);
    }

    /* renamed from: ng */
    private void m31342ng(float f) {
        bFf().mo5608dq().mo3150a(icD, f);
    }

    /* renamed from: nh */
    private void m31343nh(float f) {
        bFf().mo5608dq().mo3150a(icF, f);
    }

    /* renamed from: ni */
    private void m31344ni(float f) {
        bFf().mo5608dq().mo3150a(icG, f);
    }

    /* renamed from: nj */
    private void m31345nj(float f) {
        bFf().mo5608dq().mo3150a(isS, f);
    }

    /* renamed from: nk */
    private void m31346nk(float f) {
        bFf().mo5608dq().mo3150a(isT, f);
    }

    @C0064Am(aul = "96bc133e704aa2556af095381bfc7674", aum = 0)
    @C5566aOg
    @C2499fr
    /* renamed from: qU */
    private void m31347qU() {
        throw new aWi(new aCE(this, f7437Lm, new Object[0]));
    }

    @C0064Am(aul = "f3e1e51864c54d73b29989a897edbb94", aum = 0)
    @C5566aOg
    /* renamed from: yP */
    private void m31348yP(int i) {
        throw new aWi(new aCE(this, ite, new Object[]{new Integer(i)}));
    }

    /* access modifiers changed from: private */
    @C5566aOg
    /* renamed from: yQ */
    public void m31349yQ(int i) {
        switch (bFf().mo6893i(ite)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, ite, new Object[]{new Integer(i)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, ite, new Object[]{new Integer(i)}));
                break;
        }
        m31348yP(i);
    }

    /* renamed from: S */
    public void mo10S() {
        super.mo10S();
    }

    /* renamed from: T */
    public C6494any mo11T() {
        return ___iScriptClass;
    }

    /* renamed from: TJ */
    public AdapterLikedList<CruiseSpeedAdapter> mo5353TJ() {
        switch (bFf().mo6893i(aPf)) {
            case 0:
                return null;
            case 2:
                return (AdapterLikedList) bFf().mo5606d(new aCE(this, aPf, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, aPf, new Object[0]));
                break;
        }
        return m31300TI();
    }

    /* renamed from: TL */
    public void mo12905TL() {
        switch (bFf().mo6893i(aPj)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, aPj, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, aPj, new Object[0]));
                break;
        }
        m31301TK();
    }

    /* renamed from: TN */
    public void mo12906TN() {
        switch (bFf().mo6893i(aPk)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, aPk, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, aPk, new Object[0]));
                break;
        }
        m31302TM();
    }

    /* renamed from: TP */
    public void mo18813TP() {
        switch (bFf().mo6893i(aPl)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, aPl, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, aPl, new Object[0]));
                break;
        }
        m31303TO();
    }

    /* renamed from: TR */
    public void mo18814TR() {
        switch (bFf().mo6893i(aPm)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, aPm, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, aPm, new Object[0]));
                break;
        }
        m31304TQ();
    }

    /* renamed from: U */
    public C2491fm[] mo12U() {
        return _m_methods;
    }

    /* renamed from: VF */
    public float mo18815VF() {
        switch (bFf().mo6893i(aTe)) {
            case 0:
                return 0.0f;
            case 2:
                return ((Float) bFf().mo5606d(new aCE(this, aTe, new Object[0]))).floatValue();
            case 3:
                bFf().mo5606d(new aCE(this, aTe, new Object[0]));
                break;
        }
        return m31308VE();
    }

    /* renamed from: VH */
    public float mo18816VH() {
        switch (bFf().mo6893i(aTf)) {
            case 0:
                return 0.0f;
            case 2:
                return ((Float) bFf().mo5606d(new aCE(this, aTf, new Object[0]))).floatValue();
            case 3:
                bFf().mo5606d(new aCE(this, aTf, new Object[0]));
                break;
        }
        return m31309VG();
    }

    /* renamed from: W */
    public Object mo13W() {
        return new C0973OK(this);
    }

    /* renamed from: YE */
    public float mo16294YE() {
        switch (bFf().mo6893i(bbS)) {
            case 0:
                return 0.0f;
            case 2:
                return ((Float) bFf().mo5606d(new aCE(this, bbS, new Object[0]))).floatValue();
            case 3:
                bFf().mo5606d(new aCE(this, bbS, new Object[0]));
                break;
        }
        return m31312YD();
    }

    /* renamed from: YG */
    public float mo16295YG() {
        switch (bFf().mo6893i(bbU)) {
            case 0:
                return 0.0f;
            case 2:
                return ((Float) bFf().mo5606d(new aCE(this, bbU, new Object[0]))).floatValue();
            case 3:
                bFf().mo5606d(new aCE(this, bbU, new Object[0]));
                break;
        }
        return m31313YF();
    }

    /* renamed from: a */
    public Object mo14a(C0495Gr gr) {
        Object[] args = gr.getArgs();
        switch (gr.mo2417hq() - TaikodomObject._m_methodCount) {
            case 0:
                m31323d((C6809auB.C1996a) args[0]);
                return null;
            case 1:
                ahu();
                return null;
            case 2:
                return agI();
            case 3:
                return new Float(m31313YF());
            case 4:
                return new Float(m31312YD());
            case 5:
                return new Float(m31334js());
            case 6:
                m31321at((Ship) args[0]);
                return null;
            case 7:
                return m31300TI();
            case 8:
                return new Float(agU());
            case 9:
                return new Float(agS());
            case 10:
                return new Float(agQ());
            case 11:
                return new Float(agO());
            case 12:
                return new Float(djX());
            case 13:
                return new Float(djZ());
            case 14:
                return new Boolean(dkb());
            case 15:
                return new Float(m31310VI());
            case 16:
                return new Float(m31311VJ());
            case 17:
                return new Float(m31308VE());
            case 18:
                return new Float(m31309VG());
            case 19:
                return new Float(anq());
            case 20:
                return new Float(m31338lj(((Long) args[0]).longValue()));
            case 21:
                return new Float(ans());
            case 22:
                return new Float(m31339ll(((Long) args[0]).longValue()));
            case 23:
                return new Boolean(aNS());
            case 24:
                m31347qU();
                return null;
            case 25:
                acK();
                return null;
            case 26:
                m31340ln(((Long) args[0]).longValue());
                return null;
            case 27:
                m31335k(((Integer) args[0]).intValue(), ((Long) args[1]).longValue());
                return null;
            case 28:
                m31322cZ(((Long) args[0]).longValue());
                return null;
            case 29:
                acP();
                return null;
            case 30:
                dkd();
                return null;
            case 31:
                m31348yP(((Integer) args[0]).intValue());
                return null;
            case 32:
                return acY();
            case 33:
                return new Float(agy());
            case 34:
                return m31299Mo();
            case 35:
                m31303TO();
                return null;
            case 36:
                m31301TK();
                return null;
            case 37:
                m31302TM();
                return null;
            case 38:
                m31330fg();
                return null;
            case 39:
                m31304TQ();
                return null;
            default:
                return super.mo14a(gr);
        }
    }

    /* renamed from: a */
    public void mo15a(Collection collection, C0495Gr gr) {
        super.mo15a(collection, gr);
    }

    public boolean aNT() {
        switch (bFf().mo6893i(cRZ)) {
            case 0:
                return false;
            case 2:
                return ((Boolean) bFf().mo5606d(new aCE(this, cRZ, new Object[0]))).booleanValue();
            case 3:
                bFf().mo5606d(new aCE(this, cRZ, new Object[0]));
                break;
        }
        return aNS();
    }

    @C4034yP
    @C5566aOg
    @C2499fr
    public void abort() {
        switch (bFf().mo6893i(bkd)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, bkd, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, bkd, new Object[0]));
                break;
        }
        acP();
    }

    public C6809auB.C1996a acZ() {
        switch (bFf().mo6893i(bkk)) {
            case 0:
                return null;
            case 2:
                return (C6809auB.C1996a) bFf().mo5606d(new aCE(this, bkk, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, bkk, new Object[0]));
                break;
        }
        return acY();
    }

    @C5566aOg
    public void activate() {
        switch (bFf().mo6893i(bka)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, bka, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, bka, new Object[0]));
                break;
        }
        acK();
    }

    public String agJ() {
        switch (bFf().mo6893i(bqk)) {
            case 0:
                return null;
            case 2:
                return (String) bFf().mo5606d(new aCE(this, bqk, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, bqk, new Object[0]));
                break;
        }
        return agI();
    }

    public float agP() {
        switch (bFf().mo6893i(bqq)) {
            case 0:
                return 0.0f;
            case 2:
                return ((Float) bFf().mo5606d(new aCE(this, bqq, new Object[0]))).floatValue();
            case 3:
                bFf().mo5606d(new aCE(this, bqq, new Object[0]));
                break;
        }
        return agO();
    }

    public float agR() {
        switch (bFf().mo6893i(bqr)) {
            case 0:
                return 0.0f;
            case 2:
                return ((Float) bFf().mo5606d(new aCE(this, bqr, new Object[0]))).floatValue();
            case 3:
                bFf().mo5606d(new aCE(this, bqr, new Object[0]));
                break;
        }
        return agQ();
    }

    public float agT() {
        switch (bFf().mo6893i(bqs)) {
            case 0:
                return 0.0f;
            case 2:
                return ((Float) bFf().mo5606d(new aCE(this, bqs, new Object[0]))).floatValue();
            case 3:
                bFf().mo5606d(new aCE(this, bqs, new Object[0]));
                break;
        }
        return agS();
    }

    public float agV() {
        switch (bFf().mo6893i(bqt)) {
            case 0:
                return 0.0f;
            case 2:
                return ((Float) bFf().mo5606d(new aCE(this, bqt, new Object[0]))).floatValue();
            case 3:
                bFf().mo5606d(new aCE(this, bqt, new Object[0]));
                break;
        }
        return agU();
    }

    public float agz() {
        switch (bFf().mo6893i(bqf)) {
            case 0:
                return 0.0f;
            case 2:
                return ((Float) bFf().mo5606d(new aCE(this, bqf, new Object[0]))).floatValue();
            case 3:
                bFf().mo5606d(new aCE(this, bqf, new Object[0]));
                break;
        }
        return agy();
    }

    public void ahv() {
        switch (bFf().mo6893i(bqQ)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, bqQ, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, bqQ, new Object[0]));
                break;
        }
        ahu();
    }

    /* renamed from: al */
    public Ship mo18822al() {
        switch (bFf().mo6893i(f7438x851d656b)) {
            case 0:
                return null;
            case 2:
                return (Ship) bFf().mo5606d(new aCE(this, f7438x851d656b, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f7438x851d656b, new Object[0]));
                break;
        }
        return m31299Mo();
    }

    public float anr() {
        switch (bFf().mo6893i(bFx)) {
            case 0:
                return 0.0f;
            case 2:
                return ((Float) bFf().mo5606d(new aCE(this, bFx, new Object[0]))).floatValue();
            case 3:
                bFf().mo5606d(new aCE(this, bFx, new Object[0]));
                break;
        }
        return anq();
    }

    public float ant() {
        switch (bFf().mo6893i(bFy)) {
            case 0:
                return 0.0f;
            case 2:
                return ((Float) bFf().mo5606d(new aCE(this, bFy, new Object[0]))).floatValue();
            case 3:
                bFf().mo5606d(new aCE(this, bFy, new Object[0]));
                break;
        }
        return ans();
    }

    /* renamed from: au */
    public void mo18825au(Ship fAVar) {
        switch (bFf().mo6893i(isV)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, isV, new Object[]{fAVar}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, isV, new Object[]{fAVar}));
                break;
        }
        m31321at(fAVar);
    }

    /* renamed from: c */
    public C5663aRz[] mo25c() {
        return _m_fields;
    }

    public void dispose() {
        switch (bFf().mo6893i(_f_dispose_0020_0028_0029V)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, _f_dispose_0020_0028_0029V, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, _f_dispose_0020_0028_0029V, new Object[0]));
                break;
        }
        m31330fg();
    }

    public float djY() {
        switch (bFf().mo6893i(isW)) {
            case 0:
                return 0.0f;
            case 2:
                return ((Float) bFf().mo5606d(new aCE(this, isW, new Object[0]))).floatValue();
            case 3:
                bFf().mo5606d(new aCE(this, isW, new Object[0]));
                break;
        }
        return djX();
    }

    public float dka() {
        switch (bFf().mo6893i(isX)) {
            case 0:
                return 0.0f;
            case 2:
                return ((Float) bFf().mo5606d(new aCE(this, isX, new Object[0]))).floatValue();
            case 3:
                bFf().mo5606d(new aCE(this, isX, new Object[0]));
                break;
        }
        return djZ();
    }

    public boolean dkc() {
        switch (bFf().mo6893i(isY)) {
            case 0:
                return false;
            case 2:
                return ((Boolean) bFf().mo5606d(new aCE(this, isY, new Object[0]))).booleanValue();
            case 3:
                bFf().mo5606d(new aCE(this, isY, new Object[0]));
                break;
        }
        return dkb();
    }

    /* access modifiers changed from: protected */
    /* renamed from: e */
    public void mo18829e(C6809auB.C1996a aVar) {
        switch (bFf().mo6893i(bkl)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, bkl, new Object[]{aVar}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, bkl, new Object[]{aVar}));
                break;
        }
        m31323d(aVar);
    }

    /* renamed from: jt */
    public float mo16301jt() {
        switch (bFf().mo6893i(f7441yg)) {
            case 0:
                return 0.0f;
            case 2:
                return ((Float) bFf().mo5606d(new aCE(this, f7441yg, new Object[0]))).floatValue();
            case 3:
                bFf().mo5606d(new aCE(this, f7441yg, new Object[0]));
                break;
        }
        return m31334js();
    }

    /* renamed from: lk */
    public float mo18831lk(long j) {
        switch (bFf().mo6893i(isZ)) {
            case 0:
                return 0.0f;
            case 2:
                return ((Float) bFf().mo5606d(new aCE(this, isZ, new Object[]{new Long(j)}))).floatValue();
            case 3:
                bFf().mo5606d(new aCE(this, isZ, new Object[]{new Long(j)}));
                break;
        }
        return m31338lj(j);
    }

    /* renamed from: lm */
    public float mo18832lm(long j) {
        switch (bFf().mo6893i(ita)) {
            case 0:
                return 0.0f;
            case 2:
                return ((Float) bFf().mo5606d(new aCE(this, ita, new Object[]{new Long(j)}))).floatValue();
            case 3:
                bFf().mo5606d(new aCE(this, ita, new Object[]{new Long(j)}));
                break;
        }
        return m31339ll(j);
    }

    @C5566aOg
    /* renamed from: lo */
    public void mo18833lo(long j) {
        switch (bFf().mo6893i(itb)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, itb, new Object[]{new Long(j)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, itb, new Object[]{new Long(j)}));
                break;
        }
        m31340ln(j);
    }

    @C5566aOg
    @C2499fr
    /* renamed from: qV */
    public void mo18834qV() {
        switch (bFf().mo6893i(f7437Lm)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f7437Lm, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f7437Lm, new Object[0]));
                break;
        }
        m31347qU();
    }

    /* renamed from: ra */
    public float mo18835ra() {
        switch (bFf().mo6893i(aTh)) {
            case 0:
                return 0.0f;
            case 2:
                return ((Float) bFf().mo5606d(new aCE(this, aTh, new Object[0]))).floatValue();
            case 3:
                bFf().mo5606d(new aCE(this, aTh, new Object[0]));
                break;
        }
        return m31311VJ();
    }

    /* renamed from: rb */
    public float mo18836rb() {
        switch (bFf().mo6893i(aTg)) {
            case 0:
                return 0.0f;
            case 2:
                return ((Float) bFf().mo5606d(new aCE(this, aTg, new Object[0]))).floatValue();
            case 3:
                bFf().mo5606d(new aCE(this, aTg, new Object[0]));
                break;
        }
        return m31310VI();
    }

    /* renamed from: h */
    public void mo18830h(CruiseSpeedType nf) {
        super.mo967a((C2961mJ) nf);
        AdapterLikedList zAVar = (AdapterLikedList) bFf().mo6865M(AdapterLikedList.class);
        BaseAdapter bVar = (BaseAdapter) bFf().mo6865M(BaseAdapter.class);
        bVar.mo18840c(this);
        zAVar.mo23260c(bVar);
        m31318a(zAVar);
        mo18813TP();
        m31305Te().mo23259b((C6872avM) this);
        mo18829e(C6809auB.C1996a.DEACTIVE);
        CruiseSpeedEnterer cVar = (CruiseSpeedEnterer) bFf().mo6865M(CruiseSpeedEnterer.class);
        cVar.mo18843a(this, (aDJ) this);
        m31316a(cVar);
        CruiseSpeedExiter aVar = (CruiseSpeedExiter) bFf().mo6865M(CruiseSpeedExiter.class);
        aVar.mo18837a(this, (aDJ) this);
        m31315a(aVar);
    }

    @C0064Am(aul = "5bf19ba770b0cc837ad7e6e53212e7b9", aum = 0)
    /* renamed from: d */
    private void m31323d(C6809auB.C1996a aVar) {
        m31314a(aVar);
        ahv();
    }

    @C0064Am(aul = "de259ef249f74a89f516c686b268bcad", aum = 0)
    private void ahu() {
        if (m31306Tn() != null && (m31306Tn().agj() instanceof Player)) {
            ((Player) m31306Tn().agj()).mo14338A(agJ(), new C3504rh(acy()));
        }
    }

    @C0064Am(aul = "7f9b494a1bd979275e75869c24910726", aum = 0)
    private String agI() {
        return Long.toString(cWm());
    }

    @C0064Am(aul = "8287b1c96c55eb78266b6a8cf2760c4f", aum = 0)
    /* renamed from: YF */
    private float m31313YF() {
        return bjQ();
    }

    @C0064Am(aul = "fff7ba4d2b0bc4208deb365ec54d2bcb", aum = 0)
    /* renamed from: YD */
    private float m31312YD() {
        return bjP();
    }

    @C0064Am(aul = "b334e2c52864e2678bdaf26ec966fc14", aum = 0)
    /* renamed from: js */
    private float m31334js() {
        return 0.0f;
    }

    @C0064Am(aul = "ba264abf215cec608bd7c0a26e3771b2", aum = 0)
    /* renamed from: at */
    private void m31321at(Ship fAVar) {
        m31336k(fAVar);
    }

    @C0064Am(aul = "3579acffda735f5e6ea72e87f898cf95", aum = 0)
    /* renamed from: TI */
    private AdapterLikedList<CruiseSpeedAdapter> m31300TI() {
        return m31305Te();
    }

    @C0064Am(aul = "17a6690b52a14360d45655f2cc9d5c2f", aum = 0)
    private float agU() {
        return aeT();
    }

    @C0064Am(aul = "449de15d20a88f6725d5743646af440c", aum = 0)
    private float agS() {
        return aeS();
    }

    @C0064Am(aul = "11065df130d965a14225bbc6e9d9bdd0", aum = 0)
    private float agQ() {
        return aeP();
    }

    @C0064Am(aul = "e68c5eca6ee59039032126c6ddef6e2e", aum = 0)
    private float agO() {
        return aeO();
    }

    @C0064Am(aul = "bddbc22788283fc7f75de17fe441afe9", aum = 0)
    private float djX() {
        return bjP();
    }

    @C0064Am(aul = "afdfb8b83258dac18a075c3c8940be6a", aum = 0)
    private float djZ() {
        return bjQ();
    }

    @C0064Am(aul = "d570bc0b5cc79d36e45559161100e071", aum = 0)
    private boolean dkb() {
        return false;
    }

    @C0064Am(aul = "4f3147766715e74f9ab4337cdca6cc42", aum = 0)
    /* renamed from: VI */
    private float m31310VI() {
        return djQ();
    }

    @C0064Am(aul = "1ba5eb9b8f77cd71ecb4e9cfbdfee71d", aum = 0)
    /* renamed from: VJ */
    private float m31311VJ() {
        return djR();
    }

    @C0064Am(aul = "11c640c3bc7b178ae7a04dc23aacc5d7", aum = 0)
    /* renamed from: VE */
    private float m31308VE() {
        return djS();
    }

    @C0064Am(aul = "ef5435d33773d60f369d75ab312c226e", aum = 0)
    /* renamed from: VG */
    private float m31309VG() {
        return djT();
    }

    @C0064Am(aul = "572d4627e9fc1e08f7cbb30bb6ae9f11", aum = 0)
    private float anq() {
        return mo18831lk(0);
    }

    @C0064Am(aul = "bff5308ec2a82fcd947ac700a294fb17", aum = 0)
    /* renamed from: lj */
    private float m31338lj(long j) {
        if (j <= 0) {
            return djU();
        }
        float djU = djU() - (((float) ((super.cVr() - j) * 2)) / 1000.0f);
        if (djU >= 0.0f) {
            return djU;
        }
        return 0.0f;
    }

    @C0064Am(aul = "9a9a0e047ade0ae8d5cc67811ce4e92c", aum = 0)
    private float ans() {
        return mo18832lm(0);
    }

    @C0064Am(aul = "618e0669ca251f571c0f3953200d46e8", aum = 0)
    /* renamed from: ll */
    private float m31339ll(long j) {
        if (j <= 0) {
            return djV();
        }
        float djV = djV() - (((float) ((super.cVr() - j) * 2)) / 1000.0f);
        if (djV >= 0.0f) {
            return djV;
        }
        return 0.0f;
    }

    @C0064Am(aul = "5a095f34ffe4e0ba33ce917c95095b9c", aum = 0)
    private boolean aNS() {
        return djW();
    }

    @C0064Am(aul = "4dbd63df46fba6d39d9eab1c99a59c8c", aum = 0)
    /* renamed from: cZ */
    private void m31322cZ(long j) {
        djP().mo18838z(j);
        mo18829e(C6809auB.C1996a.COOLDOWN);
        if (m31306Tn().mo2998hb() != null) {
            m31306Tn().mo2998hb().mo3297xP();
        }
        m31306Tn().mo18259Y(bjS());
        m31306Tn().mo964W(0.0f);
        m31306Tn().ahS();
        if (m31306Tn().agj() instanceof Player) {
            ((Player) m31306Tn().agj()).mo14419f((C1506WA) new C0284Df(((NLSCruiseSpeed) ala().aIY().mo6310c(NLSManager.C1472a.CRUISESPEED)).dzU(), false, new Object[0]));
        }
    }

    @C0064Am(aul = "35051cfec3c4c051239a84522ce07b38", aum = 0)
    private C6809auB.C1996a acY() {
        return acy();
    }

    @C0064Am(aul = "e8a5c4f7131d452118ea1a298ceb139c", aum = 0)
    private float agy() {
        if (acy() == C6809auB.C1996a.ACTIVE) {
            return -1.0f;
        }
        if (acy() == C6809auB.C1996a.COOLDOWN) {
            return bjQ();
        }
        if (acy() == C6809auB.C1996a.WARMUP) {
            return bjP();
        }
        return 0.0f;
    }

    @C0064Am(aul = "3e04e3cc04be60f8cc548b3227a16e9a", aum = 0)
    /* renamed from: Mo */
    private Ship m31299Mo() {
        return m31306Tn();
    }

    @C0064Am(aul = "74628368efae2cdb0a38d9ec79f05ef7", aum = 0)
    /* renamed from: TO */
    private void m31303TO() {
        m31341nf(((CruiseSpeedAdapter) m31305Te().ase()).mo2002rb());
        m31342ng(((CruiseSpeedAdapter) m31305Te().ase()).mo2001ra());
        m31343nh(((CruiseSpeedAdapter) m31305Te().ase()).mo1996VF());
        m31344ni(((CruiseSpeedAdapter) m31305Te().ase()).mo1997VH());
        m31345nj(((CruiseSpeedAdapter) m31305Te().ase()).anr());
        m31346nk(((CruiseSpeedAdapter) m31305Te().ase()).ant());
        m31333jS(((CruiseSpeedAdapter) m31305Te().ase()).aNT());
    }

    @C0064Am(aul = "3abab0f40ec40da0808995c0390ae1da", aum = 0)
    /* renamed from: TK */
    private void m31301TK() {
        mo18813TP();
        if (aNT()) {
            if (m31306Tn().agj() instanceof Player) {
                ((Player) mo18822al().agj()).mo14419f((C1506WA) new C0284Df(((NLSWindowAlert) ala().aIY().mo6310c(NLSManager.C1472a.WINDOWALERT)).dci(), false, new Object[0]));
            }
            if (acy() == C6809auB.C1996a.ACTIVE) {
                m31325da(cVr());
            }
        }
    }

    @C0064Am(aul = "d8860b173ab3631be3885fc84386fad7", aum = 0)
    /* renamed from: TM */
    private void m31302TM() {
        mo18813TP();
    }

    @C0064Am(aul = "5d5c566e72f262c27f3ce04ba8cce1e1", aum = 0)
    /* renamed from: fg */
    private void m31330fg() {
        abort();
        djO().dispose();
        djP().dispose();
        m31336k((Ship) null);
        super.dispose();
    }

    @C0064Am(aul = "c958c6f3392ddd0638442ddabcbd212f", aum = 0)
    /* renamed from: TQ */
    private void m31304TQ() {
        if (m31305Te() == null) {
            mo8358lY(String.valueOf(bFY()) + " with NULL adapterList");
            return;
        }
        m31305Te().mo23259b((C6872avM) this);
        mo18813TP();
    }

    @C6485anp
    @C5511aMd
    /* renamed from: a.fk$b */
    /* compiled from: a */
    public class BaseAdapter extends CruiseSpeedAdapter implements C1616Xf {
        /* renamed from: PF */
        public static final C2491fm f7447PF = null;
        public static final C2491fm _f_toString_0020_0028_0029Ljava_002flang_002fString_003b = null;
        public static final int _m_fieldCount = 0;
        public static final C5663aRz[] _m_fields = null;
        public static final int _m_methodCount = 0;
        public static final C2491fm[] _m_methods = null;
        /* renamed from: aT */
        public static final C5663aRz f7448aT = null;
        public static final C2491fm aTe = null;
        public static final C2491fm aTf = null;
        public static final C2491fm aTg = null;
        public static final C2491fm aTh = null;
        public static final C2491fm apF = null;
        public static final C2491fm apG = null;
        public static final C2491fm bFx = null;
        public static final C2491fm bFy = null;
        public static final C2491fm cRZ = null;
        public static final C2491fm fXk = null;
        public static final C2491fm fXl = null;
        public static final C2491fm fXm = null;
        public static final long serialVersionUID = 0;
        public static C6494any ___iScriptClass;
        @C0064Am(aul = "8d578c907e9d4219f1a921ecca0f84bd", aum = 0)

        /* renamed from: JC */
        static /* synthetic */ CruiseSpeed f7446JC;

        static {
            m31394V();
        }

        public BaseAdapter() {
            super((C5540aNg) null);
            super.mo10S();
        }

        public BaseAdapter(C5540aNg ang) {
            super(ang);
        }

        public BaseAdapter(CruiseSpeed fkVar) {
            super((C5540aNg) null);
            super._m_script_init(fkVar);
        }

        /* renamed from: V */
        static void m31394V() {
            serialVersionUID = (long) 1;
            _m_fieldCount = CruiseSpeedAdapter._m_fieldCount + 1;
            _m_methodCount = CruiseSpeedAdapter._m_methodCount + 14;
            int i = CruiseSpeedAdapter._m_fieldCount;
            C5663aRz[] arzArr = new C5663aRz[(i + 1)];
            C5663aRz b = C5640aRc.m17844b(BaseAdapter.class, "8d578c907e9d4219f1a921ecca0f84bd", i);
            f7448aT = b;
            arzArr[i] = b;
            int i2 = i + 1;
            _m_fields = arzArr;
            C1634Xv.m11725a((Object[]) CruiseSpeedAdapter._m_fields, (Object[]) _m_fields);
            int i3 = CruiseSpeedAdapter._m_methodCount;
            C2491fm[] fmVarArr = new C2491fm[(i3 + 14)];
            C2491fm a = C4105zY.m41624a(BaseAdapter.class, "398a63ee453cf5b66b2b4231c1abd4f0", i3);
            aTe = a;
            fmVarArr[i3] = a;
            int i4 = i3 + 1;
            C2491fm a2 = C4105zY.m41624a(BaseAdapter.class, "6d1287ce2dba0ee2c6eaf5543bd0fb9f", i4);
            aTf = a2;
            fmVarArr[i4] = a2;
            int i5 = i4 + 1;
            C2491fm a3 = C4105zY.m41624a(BaseAdapter.class, "02d5b23223838678edcf652a4df47bca", i5);
            aTg = a3;
            fmVarArr[i5] = a3;
            int i6 = i5 + 1;
            C2491fm a4 = C4105zY.m41624a(BaseAdapter.class, "f9bc8995d703824da66924986fd0bc37", i6);
            aTh = a4;
            fmVarArr[i6] = a4;
            int i7 = i6 + 1;
            C2491fm a5 = C4105zY.m41624a(BaseAdapter.class, "b94b7e283265b4460db5637a493f9300", i7);
            bFx = a5;
            fmVarArr[i7] = a5;
            int i8 = i7 + 1;
            C2491fm a6 = C4105zY.m41624a(BaseAdapter.class, "c121e4e12f97ce8c60f99cb1f398a4e5", i8);
            bFy = a6;
            fmVarArr[i8] = a6;
            int i9 = i8 + 1;
            C2491fm a7 = C4105zY.m41624a(BaseAdapter.class, "b8e79261aa9e3124dc5dc08463424d1e", i9);
            cRZ = a7;
            fmVarArr[i9] = a7;
            int i10 = i9 + 1;
            C2491fm a8 = C4105zY.m41624a(BaseAdapter.class, "e0c9eb51505bd352fe4839f00a8a2094", i10);
            fXk = a8;
            fmVarArr[i10] = a8;
            int i11 = i10 + 1;
            C2491fm a9 = C4105zY.m41624a(BaseAdapter.class, "1c5ab68edb76917baccb26443fc473a7", i11);
            fXl = a9;
            fmVarArr[i11] = a9;
            int i12 = i11 + 1;
            C2491fm a10 = C4105zY.m41624a(BaseAdapter.class, "d32ba87e2ed06f0415e5a2a90998b5f2", i12);
            fXm = a10;
            fmVarArr[i12] = a10;
            int i13 = i12 + 1;
            C2491fm a11 = C4105zY.m41624a(BaseAdapter.class, "894d341aae2a64214bfcb0bcd903ee8e", i13);
            _f_toString_0020_0028_0029Ljava_002flang_002fString_003b = a11;
            fmVarArr[i13] = a11;
            int i14 = i13 + 1;
            C2491fm a12 = C4105zY.m41624a(BaseAdapter.class, "399fdcdbfde009fd69b94cedfca556ae", i14);
            apF = a12;
            fmVarArr[i14] = a12;
            int i15 = i14 + 1;
            C2491fm a13 = C4105zY.m41624a(BaseAdapter.class, "011de4437e92071e63288b9592e9c8f7", i15);
            f7447PF = a13;
            fmVarArr[i15] = a13;
            int i16 = i15 + 1;
            C2491fm a14 = C4105zY.m41624a(BaseAdapter.class, "f4dd48eb2045ef7812296bff7b014be2", i16);
            apG = a14;
            fmVarArr[i16] = a14;
            int i17 = i16 + 1;
            _m_methods = fmVarArr;
            C1634Xv.m11725a((Object[]) CruiseSpeedAdapter._m_methods, (Object[]) _m_methods);
            ___iScriptClass = aUO.m18566a(BaseAdapter.class, aCM.class, _m_fields, _m_methods);
        }

        @C0064Am(aul = "399fdcdbfde009fd69b94cedfca556ae", aum = 0)
        /* renamed from: ID */
        private GameObjectAdapter m31392ID() {
            return chW();
        }

        @C0064Am(aul = "f4dd48eb2045ef7812296bff7b014be2", aum = 0)
        /* renamed from: IF */
        private GameObjectAdapter m31393IF() {
            return chU();
        }

        @C0064Am(aul = "011de4437e92071e63288b9592e9c8f7", aum = 0)
        /* renamed from: a */
        private void m31400a(GameObjectAdapter ato, GameObjectAdapter ato2, GameObjectAdapter ato3) {
            mo18839b((CruiseSpeedAdapter) ato, (CruiseSpeedAdapter) ato2, (CruiseSpeedAdapter) ato3);
        }

        /* renamed from: a */
        private void m31401a(CruiseSpeed fkVar) {
            bFf().mo5608dq().mo3197f(f7448aT, fkVar);
        }

        /* renamed from: ph */
        private CruiseSpeed m31403ph() {
            return (CruiseSpeed) bFf().mo5608dq().mo3214p(f7448aT);
        }

        /* renamed from: IE */
        public /* bridge */ /* synthetic */ GameObjectAdapter mo16070IE() {
            switch (bFf().mo6893i(apF)) {
                case 0:
                    return null;
                case 2:
                    return (GameObjectAdapter) bFf().mo5606d(new aCE(this, apF, new Object[0]));
                case 3:
                    bFf().mo5606d(new aCE(this, apF, new Object[0]));
                    break;
            }
            return m31392ID();
        }

        /* renamed from: IG */
        public /* bridge */ /* synthetic */ GameObjectAdapter mo16071IG() {
            switch (bFf().mo6893i(apG)) {
                case 0:
                    return null;
                case 2:
                    return (GameObjectAdapter) bFf().mo5606d(new aCE(this, apG, new Object[0]));
                case 3:
                    bFf().mo5606d(new aCE(this, apG, new Object[0]));
                    break;
            }
            return m31393IF();
        }

        /* renamed from: S */
        public void mo10S() {
            super.mo10S();
        }

        /* renamed from: T */
        public C6494any mo11T() {
            return ___iScriptClass;
        }

        /* renamed from: U */
        public C2491fm[] mo12U() {
            return _m_methods;
        }

        /* renamed from: VF */
        public float mo1996VF() {
            switch (bFf().mo6893i(aTe)) {
                case 0:
                    return 0.0f;
                case 2:
                    return ((Float) bFf().mo5606d(new aCE(this, aTe, new Object[0]))).floatValue();
                case 3:
                    bFf().mo5606d(new aCE(this, aTe, new Object[0]));
                    break;
            }
            return m31395VE();
        }

        /* renamed from: VH */
        public float mo1997VH() {
            switch (bFf().mo6893i(aTf)) {
                case 0:
                    return 0.0f;
                case 2:
                    return ((Float) bFf().mo5606d(new aCE(this, aTf, new Object[0]))).floatValue();
                case 3:
                    bFf().mo5606d(new aCE(this, aTf, new Object[0]));
                    break;
            }
            return m31396VG();
        }

        /* renamed from: W */
        public Object mo13W() {
            return new aCM(this);
        }

        /* renamed from: a */
        public Object mo14a(C0495Gr gr) {
            Object[] args = gr.getArgs();
            switch (gr.mo2417hq() - CruiseSpeedAdapter._m_methodCount) {
                case 0:
                    return new Float(m31395VE());
                case 1:
                    return new Float(m31396VG());
                case 2:
                    return new Float(m31397VI());
                case 3:
                    return new Float(m31398VJ());
                case 4:
                    return new Float(anq());
                case 5:
                    return new Float(ans());
                case 6:
                    return new Boolean(aNS());
                case 7:
                    m31399a((CruiseSpeedAdapter) args[0], (CruiseSpeedAdapter) args[1], (CruiseSpeedAdapter) args[2]);
                    return null;
                case 8:
                    return chT();
                case 9:
                    return chV();
                case 10:
                    return m31402au();
                case 11:
                    return m31392ID();
                case 12:
                    m31400a((GameObjectAdapter) args[0], (GameObjectAdapter) args[1], (GameObjectAdapter) args[2]);
                    return null;
                case 13:
                    return m31393IF();
                default:
                    return super.mo14a(gr);
            }
        }

        /* renamed from: a */
        public void mo15a(Collection collection, C0495Gr gr) {
            super.mo15a(collection, gr);
        }

        public boolean aNT() {
            switch (bFf().mo6893i(cRZ)) {
                case 0:
                    return false;
                case 2:
                    return ((Boolean) bFf().mo5606d(new aCE(this, cRZ, new Object[0]))).booleanValue();
                case 3:
                    bFf().mo5606d(new aCE(this, cRZ, new Object[0]));
                    break;
            }
            return aNS();
        }

        public float anr() {
            switch (bFf().mo6893i(bFx)) {
                case 0:
                    return 0.0f;
                case 2:
                    return ((Float) bFf().mo5606d(new aCE(this, bFx, new Object[0]))).floatValue();
                case 3:
                    bFf().mo5606d(new aCE(this, bFx, new Object[0]));
                    break;
            }
            return anq();
        }

        public float ant() {
            switch (bFf().mo6893i(bFy)) {
                case 0:
                    return 0.0f;
                case 2:
                    return ((Float) bFf().mo5606d(new aCE(this, bFy, new Object[0]))).floatValue();
                case 3:
                    bFf().mo5606d(new aCE(this, bFy, new Object[0]));
                    break;
            }
            return ans();
        }

        /* renamed from: b */
        public void mo18839b(CruiseSpeedAdapter eq, CruiseSpeedAdapter eq2, CruiseSpeedAdapter eq3) {
            switch (bFf().mo6893i(fXk)) {
                case 0:
                    return;
                case 2:
                    bFf().mo5606d(new aCE(this, fXk, new Object[]{eq, eq2, eq3}));
                    return;
                case 3:
                    bFf().mo5606d(new aCE(this, fXk, new Object[]{eq, eq2, eq3}));
                    break;
            }
            m31399a(eq, eq2, eq3);
        }

        /* renamed from: b */
        public /* bridge */ /* synthetic */ void mo12950b(GameObjectAdapter ato, GameObjectAdapter ato2, GameObjectAdapter ato3) {
            switch (bFf().mo6893i(f7447PF)) {
                case 0:
                    return;
                case 2:
                    bFf().mo5606d(new aCE(this, f7447PF, new Object[]{ato, ato2, ato3}));
                    return;
                case 3:
                    bFf().mo5606d(new aCE(this, f7447PF, new Object[]{ato, ato2, ato3}));
                    break;
            }
            m31400a(ato, ato2, ato3);
        }

        /* renamed from: c */
        public C5663aRz[] mo25c() {
            return _m_fields;
        }

        public CruiseSpeedAdapter chU() {
            switch (bFf().mo6893i(fXl)) {
                case 0:
                    return null;
                case 2:
                    return (CruiseSpeedAdapter) bFf().mo5606d(new aCE(this, fXl, new Object[0]));
                case 3:
                    bFf().mo5606d(new aCE(this, fXl, new Object[0]));
                    break;
            }
            return chT();
        }

        public CruiseSpeedAdapter chW() {
            switch (bFf().mo6893i(fXm)) {
                case 0:
                    return null;
                case 2:
                    return (CruiseSpeedAdapter) bFf().mo5606d(new aCE(this, fXm, new Object[0]));
                case 3:
                    bFf().mo5606d(new aCE(this, fXm, new Object[0]));
                    break;
            }
            return chV();
        }

        /* renamed from: ra */
        public float mo2001ra() {
            switch (bFf().mo6893i(aTh)) {
                case 0:
                    return 0.0f;
                case 2:
                    return ((Float) bFf().mo5606d(new aCE(this, aTh, new Object[0]))).floatValue();
                case 3:
                    bFf().mo5606d(new aCE(this, aTh, new Object[0]));
                    break;
            }
            return m31398VJ();
        }

        /* renamed from: rb */
        public float mo2002rb() {
            switch (bFf().mo6893i(aTg)) {
                case 0:
                    return 0.0f;
                case 2:
                    return ((Float) bFf().mo5606d(new aCE(this, aTg, new Object[0]))).floatValue();
                case 3:
                    bFf().mo5606d(new aCE(this, aTg, new Object[0]));
                    break;
            }
            return m31397VI();
        }

        public String toString() {
            switch (bFf().mo6893i(_f_toString_0020_0028_0029Ljava_002flang_002fString_003b)) {
                case 0:
                    return null;
                case 2:
                    return (String) bFf().mo5606d(new aCE(this, _f_toString_0020_0028_0029Ljava_002flang_002fString_003b, new Object[0]));
                case 3:
                    bFf().mo5606d(new aCE(this, _f_toString_0020_0028_0029Ljava_002flang_002fString_003b, new Object[0]));
                    break;
            }
            return m31402au();
        }

        /* renamed from: c */
        public void mo18840c(CruiseSpeed fkVar) {
            m31401a(fkVar);
            super.mo10S();
        }

        @C0064Am(aul = "398a63ee453cf5b66b2b4231c1abd4f0", aum = 0)
        /* renamed from: VE */
        private float m31395VE() {
            return m31403ph().agP();
        }

        @C0064Am(aul = "6d1287ce2dba0ee2c6eaf5543bd0fb9f", aum = 0)
        /* renamed from: VG */
        private float m31396VG() {
            return m31403ph().agR();
        }

        @C0064Am(aul = "02d5b23223838678edcf652a4df47bca", aum = 0)
        /* renamed from: VI */
        private float m31397VI() {
            return m31403ph().agT();
        }

        @C0064Am(aul = "f9bc8995d703824da66924986fd0bc37", aum = 0)
        /* renamed from: VJ */
        private float m31398VJ() {
            return m31403ph().agV();
        }

        @C0064Am(aul = "b94b7e283265b4460db5637a493f9300", aum = 0)
        private float anq() {
            return m31403ph().djY();
        }

        @C0064Am(aul = "c121e4e12f97ce8c60f99cb1f398a4e5", aum = 0)
        private float ans() {
            return m31403ph().dka();
        }

        @C0064Am(aul = "b8e79261aa9e3124dc5dc08463424d1e", aum = 0)
        private boolean aNS() {
            return false;
        }

        @C0064Am(aul = "e0c9eb51505bd352fe4839f00a8a2094", aum = 0)
        /* renamed from: a */
        private void m31399a(CruiseSpeedAdapter eq, CruiseSpeedAdapter eq2, CruiseSpeedAdapter eq3) {
            if (eq != null) {
                throw new IllegalArgumentException();
            } else if (eq3 != this) {
                throw new IllegalArgumentException();
            } else {
                super.mo12950b(eq, eq2, eq3);
            }
        }

        @C0064Am(aul = "1c5ab68edb76917baccb26443fc473a7", aum = 0)
        private CruiseSpeedAdapter chT() {
            return null;
        }

        @C0064Am(aul = "d32ba87e2ed06f0415e5a2a90998b5f2", aum = 0)
        private CruiseSpeedAdapter chV() {
            return null;
        }

        @C0064Am(aul = "894d341aae2a64214bfcb0bcd903ee8e", aum = 0)
        /* renamed from: au */
        private String m31402au() {
            return "CruiseSpeed Internal BaseAdapter";
        }
    }

    @C6485anp
    @C5511aMd
    /* renamed from: a.fk$c */
    /* compiled from: a */
    public class CruiseSpeedEnterer extends TaskletImpl implements C1616Xf {
        /* renamed from: JD */
        public static final C2491fm f7450JD = null;
        public static final int _m_fieldCount = 0;
        public static final C5663aRz[] _m_fields = null;
        public static final int _m_methodCount = 0;
        public static final C2491fm[] _m_methods = null;
        /* renamed from: aT */
        public static final C5663aRz f7451aT = null;
        public static final C2491fm hFf = null;
        public static final long serialVersionUID = 0;
        public static C6494any ___iScriptClass;
        @C0064Am(aul = "c5c448f6ed4bb265cc621e7f071e6598", aum = 0)

        /* renamed from: JC */
        static /* synthetic */ CruiseSpeed f7449JC;

        static {
            m31420V();
        }

        private transient int hFe;

        public CruiseSpeedEnterer() {
            super((C5540aNg) null);
            super.mo10S();
        }

        public CruiseSpeedEnterer(C5540aNg ang) {
            super(ang);
        }

        public CruiseSpeedEnterer(CruiseSpeed fkVar, aDJ adj) {
            super((C5540aNg) null);
            super._m_script_init(fkVar, adj);
        }

        /* renamed from: V */
        static void m31420V() {
            serialVersionUID = (long) 1;
            _m_fieldCount = TaskletImpl._m_fieldCount + 1;
            _m_methodCount = TaskletImpl._m_methodCount + 2;
            int i = TaskletImpl._m_fieldCount;
            C5663aRz[] arzArr = new C5663aRz[(i + 1)];
            C5663aRz b = C5640aRc.m17844b(CruiseSpeedEnterer.class, "c5c448f6ed4bb265cc621e7f071e6598", i);
            f7451aT = b;
            arzArr[i] = b;
            int i2 = i + 1;
            _m_fields = arzArr;
            C1634Xv.m11725a((Object[]) TaskletImpl._m_fields, (Object[]) _m_fields);
            int i3 = TaskletImpl._m_methodCount;
            C2491fm[] fmVarArr = new C2491fm[(i3 + 2)];
            C2491fm a = C4105zY.m41624a(CruiseSpeedEnterer.class, "46e5ac32f51c24447882d96f875caae1", i3);
            f7450JD = a;
            fmVarArr[i3] = a;
            int i4 = i3 + 1;
            C2491fm a2 = C4105zY.m41624a(CruiseSpeedEnterer.class, "5e8b7e1e6ccd923aff097d07bb3178fa", i4);
            hFf = a2;
            fmVarArr[i4] = a2;
            int i5 = i4 + 1;
            _m_methods = fmVarArr;
            C1634Xv.m11725a((Object[]) TaskletImpl._m_methods, (Object[]) _m_methods);
            ___iScriptClass = aUO.m18566a(CruiseSpeedEnterer.class, C6286ajy.class, _m_fields, _m_methods);
        }

        /* renamed from: a */
        private void m31421a(CruiseSpeed fkVar) {
            bFf().mo5608dq().mo3197f(f7451aT, fkVar);
        }

        /* renamed from: ph */
        private CruiseSpeed m31423ph() {
            return (CruiseSpeed) bFf().mo5608dq().mo3214p(f7451aT);
        }

        /* renamed from: S */
        public void mo10S() {
            super.mo10S();
        }

        /* renamed from: T */
        public C6494any mo11T() {
            return ___iScriptClass;
        }

        /* renamed from: U */
        public C2491fm[] mo12U() {
            return _m_methods;
        }

        /* renamed from: W */
        public Object mo13W() {
            return new C6286ajy(this);
        }

        /* renamed from: a */
        public Object mo14a(C0495Gr gr) {
            Object[] args = gr.getArgs();
            switch (gr.mo2417hq() - TaskletImpl._m_methodCount) {
                case 0:
                    m31424pi();
                    return null;
                case 1:
                    m31422g(((Integer) args[0]).intValue(), ((Long) args[1]).longValue());
                    return null;
                default:
                    return super.mo14a(gr);
            }
        }

        /* renamed from: a */
        public void mo15a(Collection collection, C0495Gr gr) {
            super.mo15a(collection, gr);
        }

        /* renamed from: c */
        public C5663aRz[] mo25c() {
            return _m_fields;
        }

        /* access modifiers changed from: package-private */
        /* renamed from: h */
        public void mo18844h(int i, long j) {
            switch (bFf().mo6893i(hFf)) {
                case 0:
                    return;
                case 2:
                    bFf().mo5606d(new aCE(this, hFf, new Object[]{new Integer(i), new Long(j)}));
                    return;
                case 3:
                    bFf().mo5606d(new aCE(this, hFf, new Object[]{new Integer(i), new Long(j)}));
                    break;
            }
            m31422g(i, j);
        }

        /* access modifiers changed from: protected */
        /* renamed from: pj */
        public void mo667pj() {
            switch (bFf().mo6893i(f7450JD)) {
                case 0:
                    return;
                case 2:
                    bFf().mo5606d(new aCE(this, f7450JD, new Object[0]));
                    return;
                case 3:
                    bFf().mo5606d(new aCE(this, f7450JD, new Object[0]));
                    break;
            }
            m31424pi();
        }

        /* renamed from: a */
        public void mo18843a(CruiseSpeed fkVar, aDJ adj) {
            m31421a(fkVar);
            super.mo4706l(adj);
        }

        @C0064Am(aul = "46e5ac32f51c24447882d96f875caae1", aum = 0)
        /* renamed from: pi */
        private void m31424pi() {
            m31423ph().m31349yQ(this.hFe);
        }

        @C0064Am(aul = "5e8b7e1e6ccd923aff097d07bb3178fa", aum = 0)
        /* renamed from: g */
        private void m31422g(int i, long j) {
            float lk = m31423ph().mo18831lk(j);
            this.hFe = i;
            if (lk > 0.0f) {
                mo4702d(lk, 1);
            } else {
                m31423ph().m31349yQ(this.hFe);
            }
        }
    }

    @C6485anp
    @C5511aMd
    /* renamed from: a.fk$a */
    public class CruiseSpeedExiter extends TaskletImpl implements C1616Xf {
        /* renamed from: JD */
        public static final C2491fm f7443JD = null;
        /* renamed from: JE */
        public static final C2491fm f7444JE = null;
        public static final int _m_fieldCount = 0;
        public static final C5663aRz[] _m_fields = null;
        public static final int _m_methodCount = 0;
        public static final C2491fm[] _m_methods = null;
        /* renamed from: aT */
        public static final C5663aRz f7445aT = null;
        public static final long serialVersionUID = 0;
        public static C6494any ___iScriptClass;
        @C0064Am(aul = "8f4e94aaca64067ef7bc2ff1a46cd4a0", aum = 0)

        /* renamed from: JC */
        static /* synthetic */ CruiseSpeed f7442JC;

        static {
            m31377V();
        }

        public CruiseSpeedExiter() {
            super((C5540aNg) null);
            super.mo10S();
        }

        public CruiseSpeedExiter(C5540aNg ang) {
            super(ang);
        }

        public CruiseSpeedExiter(CruiseSpeed fkVar, aDJ adj) {
            super((C5540aNg) null);
            super._m_script_init(fkVar, adj);
        }

        /* renamed from: V */
        static void m31377V() {
            serialVersionUID = (long) 1;
            _m_fieldCount = TaskletImpl._m_fieldCount + 1;
            _m_methodCount = TaskletImpl._m_methodCount + 2;
            int i = TaskletImpl._m_fieldCount;
            C5663aRz[] arzArr = new C5663aRz[(i + 1)];
            C5663aRz b = C5640aRc.m17844b(CruiseSpeedExiter.class, "8f4e94aaca64067ef7bc2ff1a46cd4a0", i);
            f7445aT = b;
            arzArr[i] = b;
            int i2 = i + 1;
            _m_fields = arzArr;
            C1634Xv.m11725a((Object[]) TaskletImpl._m_fields, (Object[]) _m_fields);
            int i3 = TaskletImpl._m_methodCount;
            C2491fm[] fmVarArr = new C2491fm[(i3 + 2)];
            C2491fm a = C4105zY.m41624a(CruiseSpeedExiter.class, "80f4b0ab3c03001f3cb708a3fb3c7749", i3);
            f7443JD = a;
            fmVarArr[i3] = a;
            int i4 = i3 + 1;
            C2491fm a2 = C4105zY.m41624a(CruiseSpeedExiter.class, "3d4c82255d43edee2f3a9f556df8b9e2", i4);
            f7444JE = a2;
            fmVarArr[i4] = a2;
            int i5 = i4 + 1;
            _m_methods = fmVarArr;
            C1634Xv.m11725a((Object[]) TaskletImpl._m_methods, (Object[]) _m_methods);
            ___iScriptClass = aUO.m18566a(CruiseSpeedExiter.class, C0062Ak.class, _m_fields, _m_methods);
        }

        /* renamed from: a */
        private void m31378a(CruiseSpeed fkVar) {
            bFf().mo5608dq().mo3197f(f7445aT, fkVar);
        }

        /* renamed from: ph */
        private CruiseSpeed m31379ph() {
            return (CruiseSpeed) bFf().mo5608dq().mo3214p(f7445aT);
        }

        /* renamed from: S */
        public void mo10S() {
            super.mo10S();
        }

        /* renamed from: T */
        public C6494any mo11T() {
            return ___iScriptClass;
        }

        /* renamed from: U */
        public C2491fm[] mo12U() {
            return _m_methods;
        }

        /* renamed from: W */
        public Object mo13W() {
            return new C0062Ak(this);
        }

        /* renamed from: a */
        public Object mo14a(C0495Gr gr) {
            Object[] args = gr.getArgs();
            switch (gr.mo2417hq() - TaskletImpl._m_methodCount) {
                case 0:
                    m31380pi();
                    return null;
                case 1:
                    m31381y(((Long) args[0]).longValue());
                    return null;
                default:
                    return super.mo14a(gr);
            }
        }

        /* renamed from: a */
        public void mo15a(Collection collection, C0495Gr gr) {
            super.mo15a(collection, gr);
        }

        /* renamed from: c */
        public C5663aRz[] mo25c() {
            return _m_fields;
        }

        /* access modifiers changed from: protected */
        /* renamed from: pj */
        public void mo667pj() {
            switch (bFf().mo6893i(f7443JD)) {
                case 0:
                    return;
                case 2:
                    bFf().mo5606d(new aCE(this, f7443JD, new Object[0]));
                    return;
                case 3:
                    bFf().mo5606d(new aCE(this, f7443JD, new Object[0]));
                    break;
            }
            m31380pi();
        }

        /* access modifiers changed from: package-private */
        /* renamed from: z */
        public void mo18838z(long j) {
            switch (bFf().mo6893i(f7444JE)) {
                case 0:
                    return;
                case 2:
                    bFf().mo5606d(new aCE(this, f7444JE, new Object[]{new Long(j)}));
                    return;
                case 3:
                    bFf().mo5606d(new aCE(this, f7444JE, new Object[]{new Long(j)}));
                    break;
            }
            m31381y(j);
        }

        /* renamed from: a */
        public void mo18837a(CruiseSpeed fkVar, aDJ adj) {
            m31378a(fkVar);
            super.mo4706l(adj);
        }

        @C0064Am(aul = "80f4b0ab3c03001f3cb708a3fb3c7749", aum = 0)
        /* renamed from: pi */
        private void m31380pi() {
            m31379ph().dke();
        }

        @C0064Am(aul = "3d4c82255d43edee2f3a9f556df8b9e2", aum = 0)
        /* renamed from: y */
        private void m31381y(long j) {
            float lm = m31379ph().mo18832lm(j);
            if (lm > 0.0f) {
                mo4702d(lm, 1);
            } else {
                m31379ph().dke();
            }
        }
    }
}
