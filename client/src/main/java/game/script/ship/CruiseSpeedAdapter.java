package game.script.ship;

import game.network.message.externalizable.aCE;
import game.script.util.GameObjectAdapter;
import logic.baa.C1616Xf;
import logic.baa.C1634Xv;
import logic.baa.C5511aMd;
import logic.baa.C6485anp;
import logic.data.mbean.C3760ug;
import logic.res.code.C5663aRz;
import logic.res.code.C6494any;
import logic.res.html.C2491fm;
import p001a.*;

import java.util.Collection;

@C5511aMd
@C6485anp
/* renamed from: a.Eq */
/* compiled from: a */
public class CruiseSpeedAdapter extends GameObjectAdapter<CruiseSpeedAdapter> implements C1616Xf {
    public static final int _m_fieldCount = 0;
    public static final C5663aRz[] _m_fields = null;
    public static final int _m_methodCount = 0;
    public static final C2491fm[] _m_methods = null;
    public static final C2491fm aTe = null;
    public static final C2491fm aTf = null;
    public static final C2491fm aTg = null;
    public static final C2491fm aTh = null;
    public static final C2491fm bFx = null;
    public static final C2491fm bFy = null;
    public static final C2491fm cRZ = null;
    public static final long serialVersionUID = 0;
    public static C6494any ___iScriptClass;

    static {
        m3031V();
    }

    public CruiseSpeedAdapter() {
        super((C5540aNg) null);
        super.mo10S();
    }

    public CruiseSpeedAdapter(C5540aNg ang) {
        super(ang);
    }

    /* renamed from: V */
    static void m3031V() {
        serialVersionUID = (long) 1;
        _m_fieldCount = GameObjectAdapter._m_fieldCount + 0;
        _m_methodCount = GameObjectAdapter._m_methodCount + 7;
        _m_fields = new C5663aRz[(GameObjectAdapter._m_fieldCount + 0)];
        C1634Xv.m11725a((Object[]) GameObjectAdapter._m_fields, (Object[]) _m_fields);
        int i = GameObjectAdapter._m_methodCount;
        C2491fm[] fmVarArr = new C2491fm[(i + 7)];
        C2491fm a = C4105zY.m41624a(CruiseSpeedAdapter.class, "d841902e34945ef6fc035dfe38463d71", i);
        aTe = a;
        fmVarArr[i] = a;
        int i2 = i + 1;
        C2491fm a2 = C4105zY.m41624a(CruiseSpeedAdapter.class, "f27c56fd6edde2db293876e2f43c19dd", i2);
        aTf = a2;
        fmVarArr[i2] = a2;
        int i3 = i2 + 1;
        C2491fm a3 = C4105zY.m41624a(CruiseSpeedAdapter.class, "df609309b99b36dc20c22ea9d40d6762", i3);
        aTg = a3;
        fmVarArr[i3] = a3;
        int i4 = i3 + 1;
        C2491fm a4 = C4105zY.m41624a(CruiseSpeedAdapter.class, "33c5fd4594a9926cfe005a27bcbebf62", i4);
        aTh = a4;
        fmVarArr[i4] = a4;
        int i5 = i4 + 1;
        C2491fm a5 = C4105zY.m41624a(CruiseSpeedAdapter.class, "4d0cb2325f732f3e3ecce25175db994e", i5);
        bFx = a5;
        fmVarArr[i5] = a5;
        int i6 = i5 + 1;
        C2491fm a6 = C4105zY.m41624a(CruiseSpeedAdapter.class, "a5fedbb93c2cf21b69e691e1f0029eac", i6);
        bFy = a6;
        fmVarArr[i6] = a6;
        int i7 = i6 + 1;
        C2491fm a7 = C4105zY.m41624a(CruiseSpeedAdapter.class, "b398b7badf940669174301d55568e264", i7);
        cRZ = a7;
        fmVarArr[i7] = a7;
        int i8 = i7 + 1;
        _m_methods = fmVarArr;
        C1634Xv.m11725a((Object[]) GameObjectAdapter._m_methods, (Object[]) _m_methods);
        ___iScriptClass = aUO.m18566a(CruiseSpeedAdapter.class, C3760ug.class, _m_fields, _m_methods);
    }

    /* renamed from: T */
    public C6494any mo11T() {
        return ___iScriptClass;
    }

    /* renamed from: U */
    public C2491fm[] mo12U() {
        return _m_methods;
    }

    /* renamed from: VF */
    public float mo1996VF() {
        switch (bFf().mo6893i(aTe)) {
            case 0:
                return 0.0f;
            case 2:
                return ((Float) bFf().mo5606d(new aCE(this, aTe, new Object[0]))).floatValue();
            case 3:
                bFf().mo5606d(new aCE(this, aTe, new Object[0]));
                break;
        }
        return m3032VE();
    }

    /* renamed from: VH */
    public float mo1997VH() {
        switch (bFf().mo6893i(aTf)) {
            case 0:
                return 0.0f;
            case 2:
                return ((Float) bFf().mo5606d(new aCE(this, aTf, new Object[0]))).floatValue();
            case 3:
                bFf().mo5606d(new aCE(this, aTf, new Object[0]));
                break;
        }
        return m3033VG();
    }

    /* renamed from: W */
    public Object mo13W() {
        return new C3760ug(this);
    }

    /* renamed from: a */
    public Object mo14a(C0495Gr gr) {
        gr.getArgs();
        switch (gr.mo2417hq() - GameObjectAdapter._m_methodCount) {
            case 0:
                return new Float(m3032VE());
            case 1:
                return new Float(m3033VG());
            case 2:
                return new Float(m3034VI());
            case 3:
                return new Float(m3035VJ());
            case 4:
                return new Float(anq());
            case 5:
                return new Float(ans());
            case 6:
                return new Boolean(aNS());
            default:
                return super.mo14a(gr);
        }
    }

    /* renamed from: a */
    public void mo15a(Collection collection, C0495Gr gr) {
        super.mo15a(collection, gr);
    }

    public boolean aNT() {
        switch (bFf().mo6893i(cRZ)) {
            case 0:
                return false;
            case 2:
                return ((Boolean) bFf().mo5606d(new aCE(this, cRZ, new Object[0]))).booleanValue();
            case 3:
                bFf().mo5606d(new aCE(this, cRZ, new Object[0]));
                break;
        }
        return aNS();
    }

    public float anr() {
        switch (bFf().mo6893i(bFx)) {
            case 0:
                return 0.0f;
            case 2:
                return ((Float) bFf().mo5606d(new aCE(this, bFx, new Object[0]))).floatValue();
            case 3:
                bFf().mo5606d(new aCE(this, bFx, new Object[0]));
                break;
        }
        return anq();
    }

    public float ant() {
        switch (bFf().mo6893i(bFy)) {
            case 0:
                return 0.0f;
            case 2:
                return ((Float) bFf().mo5606d(new aCE(this, bFy, new Object[0]))).floatValue();
            case 3:
                bFf().mo5606d(new aCE(this, bFy, new Object[0]));
                break;
        }
        return ans();
    }

    /* renamed from: c */
    public C5663aRz[] mo25c() {
        return _m_fields;
    }

    /* renamed from: ra */
    public float mo2001ra() {
        switch (bFf().mo6893i(aTh)) {
            case 0:
                return 0.0f;
            case 2:
                return ((Float) bFf().mo5606d(new aCE(this, aTh, new Object[0]))).floatValue();
            case 3:
                bFf().mo5606d(new aCE(this, aTh, new Object[0]));
                break;
        }
        return m3035VJ();
    }

    /* renamed from: rb */
    public float mo2002rb() {
        switch (bFf().mo6893i(aTg)) {
            case 0:
                return 0.0f;
            case 2:
                return ((Float) bFf().mo5606d(new aCE(this, aTg, new Object[0]))).floatValue();
            case 3:
                bFf().mo5606d(new aCE(this, aTg, new Object[0]));
                break;
        }
        return m3034VI();
    }

    /* renamed from: S */
    public void mo10S() {
        super.mo10S();
    }

    @C0064Am(aul = "d841902e34945ef6fc035dfe38463d71", aum = 0)
    /* renamed from: VE */
    private float m3032VE() {
        return ((CruiseSpeedAdapter) mo16071IG()).mo1996VF();
    }

    @C0064Am(aul = "f27c56fd6edde2db293876e2f43c19dd", aum = 0)
    /* renamed from: VG */
    private float m3033VG() {
        return ((CruiseSpeedAdapter) mo16071IG()).mo1997VH();
    }

    @C0064Am(aul = "df609309b99b36dc20c22ea9d40d6762", aum = 0)
    /* renamed from: VI */
    private float m3034VI() {
        return ((CruiseSpeedAdapter) mo16071IG()).mo2002rb();
    }

    @C0064Am(aul = "33c5fd4594a9926cfe005a27bcbebf62", aum = 0)
    /* renamed from: VJ */
    private float m3035VJ() {
        return ((CruiseSpeedAdapter) mo16071IG()).mo2001ra();
    }

    @C0064Am(aul = "4d0cb2325f732f3e3ecce25175db994e", aum = 0)
    private float anq() {
        return ((CruiseSpeedAdapter) mo16071IG()).anr();
    }

    @C0064Am(aul = "a5fedbb93c2cf21b69e691e1f0029eac", aum = 0)
    private float ans() {
        return ((CruiseSpeedAdapter) mo16071IG()).ant();
    }

    @C0064Am(aul = "b398b7badf940669174301d55568e264", aum = 0)
    private boolean aNS() {
        return ((CruiseSpeedAdapter) mo16071IG()).aNT();
    }
}
