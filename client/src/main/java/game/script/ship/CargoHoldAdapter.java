package game.script.ship;

import game.network.message.externalizable.aCE;
import game.script.util.GameObjectAdapter;
import logic.baa.C1616Xf;
import logic.baa.C1634Xv;
import logic.baa.C5511aMd;
import logic.baa.C6485anp;
import logic.data.mbean.C6754asy;
import logic.res.code.C5663aRz;
import logic.res.code.C6494any;
import logic.res.html.C2491fm;
import p001a.*;

import java.util.Collection;

@C5511aMd
@C6485anp
/* renamed from: a.Sm */
/* compiled from: a */
public class CargoHoldAdapter extends GameObjectAdapter<CargoHoldAdapter> implements C1616Xf {

    /* renamed from: Rz */
    public static final C2491fm f1562Rz = null;
    public static final int _m_fieldCount = 0;
    public static final C5663aRz[] _m_fields = null;
    public static final int _m_methodCount = 0;
    public static final C2491fm[] _m_methods = null;
    public static final long serialVersionUID = 0;
    public static C6494any ___iScriptClass;

    static {
        m9577V();
    }

    public CargoHoldAdapter() {
        super((C5540aNg) null);
        super.mo10S();
    }

    public CargoHoldAdapter(C5540aNg ang) {
        super(ang);
    }

    /* renamed from: V */
    static void m9577V() {
        serialVersionUID = (long) 1;
        _m_fieldCount = GameObjectAdapter._m_fieldCount + 0;
        _m_methodCount = GameObjectAdapter._m_methodCount + 1;
        _m_fields = new C5663aRz[(GameObjectAdapter._m_fieldCount + 0)];
        C1634Xv.m11725a((Object[]) GameObjectAdapter._m_fields, (Object[]) _m_fields);
        int i = GameObjectAdapter._m_methodCount;
        C2491fm[] fmVarArr = new C2491fm[(i + 1)];
        C2491fm a = C4105zY.m41624a(CargoHoldAdapter.class, "939d1c9b796b8051d84231f8b907872e", i);
        f1562Rz = a;
        fmVarArr[i] = a;
        int i2 = i + 1;
        _m_methods = fmVarArr;
        C1634Xv.m11725a((Object[]) GameObjectAdapter._m_methods, (Object[]) _m_methods);
        ___iScriptClass = aUO.m18566a(CargoHoldAdapter.class, C6754asy.class, _m_fields, _m_methods);
    }

    /* renamed from: T */
    public C6494any mo11T() {
        return ___iScriptClass;
    }

    /* renamed from: U */
    public C2491fm[] mo12U() {
        return _m_methods;
    }

    /* renamed from: W */
    public Object mo13W() {
        return new C6754asy(this);
    }

    /* renamed from: a */
    public Object mo14a(C0495Gr gr) {
        gr.getArgs();
        switch (gr.mo2417hq() - GameObjectAdapter._m_methodCount) {
            case 0:
                return new Float(m9578wD());
            default:
                return super.mo14a(gr);
        }
    }

    /* renamed from: a */
    public void mo15a(Collection collection, C0495Gr gr) {
        super.mo15a(collection, gr);
    }

    /* renamed from: c */
    public C5663aRz[] mo25c() {
        return _m_fields;
    }

    /* renamed from: wE */
    public float mo1289wE() {
        switch (bFf().mo6893i(f1562Rz)) {
            case 0:
                return 0.0f;
            case 2:
                return ((Float) bFf().mo5606d(new aCE(this, f1562Rz, new Object[0]))).floatValue();
            case 3:
                bFf().mo5606d(new aCE(this, f1562Rz, new Object[0]));
                break;
        }
        return m9578wD();
    }

    /* renamed from: S */
    public void mo10S() {
        super.mo10S();
    }

    @C0064Am(aul = "939d1c9b796b8051d84231f8b907872e", aum = 0)
    /* renamed from: wD */
    private float m9578wD() {
        return ((CargoHoldAdapter) mo16071IG()).mo1289wE();
    }
}
