package game.script.ship;

import game.network.message.externalizable.aCE;
import game.script.TaikodomObject;
import logic.baa.*;
import logic.data.mbean.C2039bQ;
import logic.res.code.C5640aRc;
import logic.res.code.C5663aRz;
import logic.res.code.C6494any;
import logic.res.html.C2491fm;
import p001a.*;

import java.util.Collection;
import java.util.UUID;

@C5511aMd
@C6485anp
/* renamed from: a.aCU */
/* compiled from: a */
public class OutpostDefaults extends TaikodomObject implements C0468GU, C1616Xf {
    public static final int _m_fieldCount = 0;
    public static final C5663aRz[] _m_fields = null;
    public static final int _m_methodCount = 0;
    public static final C2491fm[] _m_methods = null;
    /* renamed from: bL */
    public static final C5663aRz f2479bL = null;
    /* renamed from: bN */
    public static final C2491fm f2480bN = null;
    /* renamed from: bO */
    public static final C2491fm f2481bO = null;
    /* renamed from: bP */
    public static final C2491fm f2482bP = null;
    public static final C5663aRz fIa = null;
    public static final C5663aRz hxu = null;
    public static final C2491fm hxv = null;
    public static final C2491fm hxw = null;
    public static final C2491fm hxx = null;
    public static final C2491fm hxy = null;
    public static final long serialVersionUID = 0;
    public static C6494any ___iScriptClass;
    @C0064Am(aul = "aef66997f99ff6a94b09da0942d19163", aum = 2)

    /* renamed from: bK */
    private static UUID f2478bK;
    @C0064Am(aul = "f9f32cddf3d417b24319e25cf89df229", aum = 0)

    /* renamed from: ps */
    private static int f2483ps;
    @C0064Am(aul = "6886861a3cfa527df93b116ae8b206d9", aum = 1)

    /* renamed from: pt */
    private static int f2484pt;

    static {
        m13277V();
    }

    public OutpostDefaults() {
        super((C5540aNg) null);
        super.mo10S();
    }

    public OutpostDefaults(C5540aNg ang) {
        super(ang);
    }

    /* renamed from: V */
    static void m13277V() {
        serialVersionUID = (long) 1;
        _m_fieldCount = TaikodomObject._m_fieldCount + 3;
        _m_methodCount = TaikodomObject._m_methodCount + 7;
        int i = TaikodomObject._m_fieldCount;
        C5663aRz[] arzArr = new C5663aRz[(i + 3)];
        C5663aRz b = C5640aRc.m17844b(OutpostDefaults.class, "f9f32cddf3d417b24319e25cf89df229", i);
        fIa = b;
        arzArr[i] = b;
        int i2 = i + 1;
        C5663aRz b2 = C5640aRc.m17844b(OutpostDefaults.class, "6886861a3cfa527df93b116ae8b206d9", i2);
        hxu = b2;
        arzArr[i2] = b2;
        int i3 = i2 + 1;
        C5663aRz b3 = C5640aRc.m17844b(OutpostDefaults.class, "aef66997f99ff6a94b09da0942d19163", i3);
        f2479bL = b3;
        arzArr[i3] = b3;
        int i4 = i3 + 1;
        _m_fields = arzArr;
        C1634Xv.m11725a((Object[]) TaikodomObject._m_fields, (Object[]) _m_fields);
        int i5 = TaikodomObject._m_methodCount;
        C2491fm[] fmVarArr = new C2491fm[(i5 + 7)];
        C2491fm a = C4105zY.m41624a(OutpostDefaults.class, "f38e0b594aec04ffb1bec25d6c356e30", i5);
        f2480bN = a;
        fmVarArr[i5] = a;
        int i6 = i5 + 1;
        C2491fm a2 = C4105zY.m41624a(OutpostDefaults.class, "9cb463d9adb85926a187d4673e3fcfa6", i6);
        f2481bO = a2;
        fmVarArr[i6] = a2;
        int i7 = i6 + 1;
        C2491fm a3 = C4105zY.m41624a(OutpostDefaults.class, "a73b72dc69274a31ab5b759c65f6768f", i7);
        f2482bP = a3;
        fmVarArr[i7] = a3;
        int i8 = i7 + 1;
        C2491fm a4 = C4105zY.m41624a(OutpostDefaults.class, "2c1d1e18aa4c290856c598db66fb8e70", i8);
        hxv = a4;
        fmVarArr[i8] = a4;
        int i9 = i8 + 1;
        C2491fm a5 = C4105zY.m41624a(OutpostDefaults.class, "ed1998405404e99e08f1e08c076f035b", i9);
        hxw = a5;
        fmVarArr[i9] = a5;
        int i10 = i9 + 1;
        C2491fm a6 = C4105zY.m41624a(OutpostDefaults.class, "08082743b7147dac37dc34868288a703", i10);
        hxx = a6;
        fmVarArr[i10] = a6;
        int i11 = i10 + 1;
        C2491fm a7 = C4105zY.m41624a(OutpostDefaults.class, "f9083f29bc65a916510a0304aee1251b", i11);
        hxy = a7;
        fmVarArr[i11] = a7;
        int i12 = i11 + 1;
        _m_methods = fmVarArr;
        C1634Xv.m11725a((Object[]) TaikodomObject._m_methods, (Object[]) _m_methods);
        ___iScriptClass = aUO.m18566a(OutpostDefaults.class, C2039bQ.class, _m_fields, _m_methods);
    }

    /* renamed from: a */
    private void m13278a(UUID uuid) {
        bFf().mo5608dq().mo3197f(f2479bL, uuid);
    }

    /* renamed from: an */
    private UUID m13279an() {
        return (UUID) bFf().mo5608dq().mo3214p(f2479bL);
    }

    /* renamed from: c */
    private void m13283c(UUID uuid) {
        switch (bFf().mo6893i(f2481bO)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f2481bO, new Object[]{uuid}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f2481bO, new Object[]{uuid}));
                break;
        }
        m13282b(uuid);
    }

    private int cRi() {
        return bFf().mo5608dq().mo3212n(fIa);
    }

    private int cRj() {
        return bFf().mo5608dq().mo3212n(hxu);
    }

    /* renamed from: xq */
    private void m13284xq(int i) {
        bFf().mo5608dq().mo3183b(fIa, i);
    }

    /* renamed from: xr */
    private void m13285xr(int i) {
        bFf().mo5608dq().mo3183b(hxu, i);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Upkeep period (days)")
    @C0064Am(aul = "ed1998405404e99e08f1e08c076f035b", aum = 0)
    @C5566aOg
    /* renamed from: xs */
    private void m13286xs(int i) {
        throw new aWi(new aCE(this, hxw, new Object[]{new Integer(i)}));
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Upkeep penalty period (days)\n(Time to settle the debts until outpost is lost)")
    @C0064Am(aul = "f9083f29bc65a916510a0304aee1251b", aum = 0)
    @C5566aOg
    /* renamed from: xu */
    private void m13287xu(int i) {
        throw new aWi(new aCE(this, hxy, new Object[]{new Integer(i)}));
    }

    /* renamed from: T */
    public C6494any mo11T() {
        return ___iScriptClass;
    }

    /* renamed from: U */
    public C2491fm[] mo12U() {
        return _m_methods;
    }

    /* renamed from: W */
    public Object mo13W() {
        return new C2039bQ(this);
    }

    /* renamed from: a */
    public Object mo14a(C0495Gr gr) {
        Object[] args = gr.getArgs();
        switch (gr.mo2417hq() - TaikodomObject._m_methodCount) {
            case 0:
                return m13280ap();
            case 1:
                m13282b((UUID) args[0]);
                return null;
            case 2:
                return m13281ar();
            case 3:
                return new Integer(cRk());
            case 4:
                m13286xs(((Integer) args[0]).intValue());
                return null;
            case 5:
                return new Integer(cRm());
            case 6:
                m13287xu(((Integer) args[0]).intValue());
                return null;
            default:
                return super.mo14a(gr);
        }
    }

    /* renamed from: a */
    public void mo15a(Collection collection, C0495Gr gr) {
        super.mo15a(collection, gr);
    }

    /* renamed from: aq */
    public UUID mo18aq() {
        switch (bFf().mo6893i(f2480bN)) {
            case 0:
                return null;
            case 2:
                return (UUID) bFf().mo5606d(new aCE(this, f2480bN, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f2480bN, new Object[0]));
                break;
        }
        return m13280ap();
    }

    /* renamed from: c */
    public C5663aRz[] mo25c() {
        return _m_fields;
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Upkeep period (days)")
    public int cRl() {
        switch (bFf().mo6893i(hxv)) {
            case 0:
                return 0;
            case 2:
                return ((Integer) bFf().mo5606d(new aCE(this, hxv, new Object[0]))).intValue();
            case 3:
                bFf().mo5606d(new aCE(this, hxv, new Object[0]));
                break;
        }
        return cRk();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Upkeep penalty period (days)\n(Time to settle the debts until outpost is lost)")
    public int cRn() {
        switch (bFf().mo6893i(hxx)) {
            case 0:
                return 0;
            case 2:
                return ((Integer) bFf().mo5606d(new aCE(this, hxx, new Object[0]))).intValue();
            case 3:
                bFf().mo5606d(new aCE(this, hxx, new Object[0]));
                break;
        }
        return cRm();
    }

    public String getHandle() {
        switch (bFf().mo6893i(f2482bP)) {
            case 0:
                return null;
            case 2:
                return (String) bFf().mo5606d(new aCE(this, f2482bP, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f2482bP, new Object[0]));
                break;
        }
        return m13281ar();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Upkeep period (days)")
    @C5566aOg
    /* renamed from: xt */
    public void mo8127xt(int i) {
        switch (bFf().mo6893i(hxw)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, hxw, new Object[]{new Integer(i)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, hxw, new Object[]{new Integer(i)}));
                break;
        }
        m13286xs(i);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Upkeep penalty period (days)\n(Time to settle the debts until outpost is lost)")
    @C5566aOg
    /* renamed from: xv */
    public void mo8128xv(int i) {
        switch (bFf().mo6893i(hxy)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, hxy, new Object[]{new Integer(i)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, hxy, new Object[]{new Integer(i)}));
                break;
        }
        m13287xu(i);
    }

    @C0064Am(aul = "f38e0b594aec04ffb1bec25d6c356e30", aum = 0)
    /* renamed from: ap */
    private UUID m13280ap() {
        return m13279an();
    }

    @C0064Am(aul = "9cb463d9adb85926a187d4673e3fcfa6", aum = 0)
    /* renamed from: b */
    private void m13282b(UUID uuid) {
        m13278a(uuid);
    }

    @C0064Am(aul = "a73b72dc69274a31ab5b759c65f6768f", aum = 0)
    /* renamed from: ar */
    private String m13281ar() {
        return "outpost_defaults";
    }

    /* renamed from: S */
    public void mo10S() {
        super.mo10S();
        m13278a(UUID.randomUUID());
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Upkeep period (days)")
    @C0064Am(aul = "2c1d1e18aa4c290856c598db66fb8e70", aum = 0)
    private int cRk() {
        return cRi();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Upkeep penalty period (days)\n(Time to settle the debts until outpost is lost)")
    @C0064Am(aul = "08082743b7147dac37dc34868288a703", aum = 0)
    private int cRm() {
        return cRj();
    }
}
