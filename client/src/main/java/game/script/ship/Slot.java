package game.script.ship;

import game.network.message.externalizable.aCE;
import game.script.Character;
import game.script.item.*;
import game.script.nls.NLSItem;
import game.script.nls.NLSManager;
import game.script.player.Player;
import game.script.resource.Asset;
import logic.baa.C1616Xf;
import logic.baa.C1634Xv;
import logic.baa.C5511aMd;
import logic.baa.C6485anp;
import logic.data.mbean.aHD;
import logic.res.code.C5640aRc;
import logic.res.code.C5663aRz;
import logic.res.code.C6494any;
import logic.res.html.C2491fm;
import p001a.*;

import java.util.Collection;

@C6485anp
@C5511aMd
@Deprecated
/* renamed from: a.lX */
/* compiled from: a */
public class Slot extends ItemLocation implements C1616Xf {

    /* renamed from: Ob */
    public static final C2491fm f8547Ob = null;

    /* renamed from: RA */
    public static final C2491fm f8548RA = null;

    /* renamed from: RD */
    public static final C2491fm f8549RD = null;
    public static final int _m_fieldCount = 0;
    public static final C5663aRz[] _m_fields = null;
    public static final int _m_methodCount = 0;
    public static final C2491fm[] _m_methods = null;
    public static final C2491fm dPa = null;
    public static final C5663aRz eeY = null;
    public static final C5663aRz efa = null;
    public static final C2491fm efb = null;
    public static final C2491fm efc = null;
    public static final C2491fm efd = null;
    public static final C2491fm efe = null;
    public static final C2491fm eff = null;
    /* renamed from: ku */
    public static final C2491fm f8551ku = null;
    public static final long serialVersionUID = 0;
    public static C6494any ___iScriptClass;
    private static SectorCategory cYq;
    private static C2893a cYr;
    private static int eeW;
    @C0064Am(aul = "cfcbfacd2516fc6909794e7bcefbec33", aum = 0)
    private static Item eeX;
    @C0064Am(aul = "0d199b08a4042430dcbfe611ed31cd9b", aum = 1)
    private static ShipSector eeZ;
    /* renamed from: jn */
    private static Asset f8550jn;

    static {
        m34934V();
    }

    public Slot() {
        super((C5540aNg) null);
        super.mo10S();
    }

    public Slot(C5540aNg ang) {
        super(ang);
    }

    /* renamed from: V */
    static void m34934V() {
        serialVersionUID = (long) 1;
        _m_fieldCount = ItemLocation._m_fieldCount + 2;
        _m_methodCount = ItemLocation._m_methodCount + 10;
        int i = ItemLocation._m_fieldCount;
        C5663aRz[] arzArr = new C5663aRz[(i + 2)];
        C5663aRz b = C5640aRc.m17844b(Slot.class, "cfcbfacd2516fc6909794e7bcefbec33", i);
        eeY = b;
        arzArr[i] = b;
        int i2 = i + 1;
        C5663aRz b2 = C5640aRc.m17844b(Slot.class, "0d199b08a4042430dcbfe611ed31cd9b", i2);
        efa = b2;
        arzArr[i2] = b2;
        int i3 = i2 + 1;
        _m_fields = arzArr;
        C1634Xv.m11725a((Object[]) ItemLocation._m_fields, (Object[]) _m_fields);
        int i4 = ItemLocation._m_methodCount;
        C2491fm[] fmVarArr = new C2491fm[(i4 + 10)];
        C2491fm a = C4105zY.m41624a(Slot.class, "2008c4ddae35cf8910eed5ac498b37d9", i4);
        dPa = a;
        fmVarArr[i4] = a;
        int i5 = i4 + 1;
        C2491fm a2 = C4105zY.m41624a(Slot.class, "cabf6b7f69f0596e42daf8cc05a863c2", i5);
        efb = a2;
        fmVarArr[i5] = a2;
        int i6 = i5 + 1;
        C2491fm a3 = C4105zY.m41624a(Slot.class, "5bc71a48e874b02a504b0b301b4598e9", i6);
        efc = a3;
        fmVarArr[i6] = a3;
        int i7 = i6 + 1;
        C2491fm a4 = C4105zY.m41624a(Slot.class, "38fd7371c7c8f57faf50f822f4af8620", i7);
        efd = a4;
        fmVarArr[i7] = a4;
        int i8 = i7 + 1;
        C2491fm a5 = C4105zY.m41624a(Slot.class, "3407063bcf41dc612e16cca75bd13712", i8);
        f8547Ob = a5;
        fmVarArr[i8] = a5;
        int i9 = i8 + 1;
        C2491fm a6 = C4105zY.m41624a(Slot.class, "c8dd13b1be28f83fef9cf551b801f768", i9);
        efe = a6;
        fmVarArr[i9] = a6;
        int i10 = i9 + 1;
        C2491fm a7 = C4105zY.m41624a(Slot.class, "c4a7a3931cab5278ddaece5f82bac518", i10);
        eff = a7;
        fmVarArr[i10] = a7;
        int i11 = i10 + 1;
        C2491fm a8 = C4105zY.m41624a(Slot.class, "0156e074852f3f8898f07b519375da5f", i11);
        f8548RA = a8;
        fmVarArr[i11] = a8;
        int i12 = i11 + 1;
        C2491fm a9 = C4105zY.m41624a(Slot.class, "a8cff132a75b7993b1087e8959f11826", i12);
        f8549RD = a9;
        fmVarArr[i12] = a9;
        int i13 = i12 + 1;
        C2491fm a10 = C4105zY.m41624a(Slot.class, "f72cc588bb3a0e70eb955af1294ab20d", i13);
        f8551ku = a10;
        fmVarArr[i13] = a10;
        int i14 = i13 + 1;
        _m_methods = fmVarArr;
        C1634Xv.m11725a((Object[]) ItemLocation._m_methods, (Object[]) _m_methods);
        ___iScriptClass = aUO.m18566a(Slot.class, aHD.class, _m_fields, _m_methods);
    }

    private Item btT() {
        return (Item) bFf().mo5608dq().mo3214p(eeY);
    }

    private ShipSector btU() {
        return (ShipSector) bFf().mo5608dq().mo3214p(efa);
    }

    /* renamed from: c */
    private void m34936c(ShipSector or) {
        bFf().mo5608dq().mo3197f(efa, or);
    }

    /* renamed from: y */
    private void m34941y(Item auq) {
        bFf().mo5608dq().mo3197f(eeY, auq);
    }

    /* renamed from: B */
    public boolean mo20242B(ItemType jCVar) {
        switch (bFf().mo6893i(dPa)) {
            case 0:
                return false;
            case 2:
                return ((Boolean) bFf().mo5606d(new aCE(this, dPa, new Object[]{jCVar}))).booleanValue();
            case 3:
                bFf().mo5606d(new aCE(this, dPa, new Object[]{jCVar}));
                break;
        }
        return m34933A(jCVar);
    }

    /* renamed from: T */
    public C6494any mo11T() {
        return ___iScriptClass;
    }

    /* renamed from: U */
    public C2491fm[] mo12U() {
        return _m_methods;
    }

    /* renamed from: W */
    public Object mo13W() {
        return new aHD(this);
    }

    /* renamed from: a */
    public Object mo14a(C0495Gr gr) {
        Object[] args = gr.getArgs();
        switch (gr.mo2417hq() - ItemLocation._m_methodCount) {
            case 0:
                return new Boolean(m34933A((ItemType) args[0]));
            case 1:
                return btV();
            case 2:
                m34937d((ShipSector) args[0]);
                return null;
            case 3:
                return btX();
            case 4:
                return m34940sJ();
            case 5:
                return new Integer(btZ());
            case 6:
                return bub();
            case 7:
                m34935a((Item) args[0], (ItemLocation) args[1]);
                return null;
            case 8:
                m34939m((Item) args[0]);
                return null;
            case 9:
                m34938g((Item) args[0]);
                return null;
            default:
                return super.mo14a(gr);
        }
    }

    /* renamed from: a */
    public void mo15a(Collection collection, C0495Gr gr) {
        super.mo15a(collection, gr);
    }

    /* access modifiers changed from: protected */
    /* renamed from: b */
    public void mo1887b(Item auq, ItemLocation aag) {
        switch (bFf().mo6893i(f8548RA)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f8548RA, new Object[]{auq, aag}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f8548RA, new Object[]{auq, aag}));
                break;
        }
        m34935a(auq, aag);
    }

    public ShipSector btW() {
        switch (bFf().mo6893i(efb)) {
            case 0:
                return null;
            case 2:
                return (ShipSector) bFf().mo5606d(new aCE(this, efb, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, efb, new Object[0]));
                break;
        }
        return btV();
    }

    public Item btY() {
        switch (bFf().mo6893i(efd)) {
            case 0:
                return null;
            case 2:
                return (Item) bFf().mo5606d(new aCE(this, efd, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, efd, new Object[0]));
                break;
        }
        return btX();
    }

    public int bua() {
        switch (bFf().mo6893i(efe)) {
            case 0:
                return 0;
            case 2:
                return ((Integer) bFf().mo5606d(new aCE(this, efe, new Object[0]))).intValue();
            case 3:
                bFf().mo5606d(new aCE(this, efe, new Object[0]));
                break;
        }
        return btZ();
    }

    public C2893a buc() {
        switch (bFf().mo6893i(eff)) {
            case 0:
                return null;
            case 2:
                return (C2893a) bFf().mo5606d(new aCE(this, eff, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, eff, new Object[0]));
                break;
        }
        return bub();
    }

    /* renamed from: c */
    public C5663aRz[] mo25c() {
        return _m_fields;
    }

    /* renamed from: e */
    public void mo20247e(ShipSector or) {
        switch (bFf().mo6893i(efc)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, efc, new Object[]{or}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, efc, new Object[]{or}));
                break;
        }
        m34937d(or);
    }

    /* access modifiers changed from: protected */
    /* renamed from: h */
    public void mo1889h(Item auq) {
        switch (bFf().mo6893i(f8551ku)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f8551ku, new Object[]{auq}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f8551ku, new Object[]{auq}));
                break;
        }
        m34938g(auq);
    }

    /* access modifiers changed from: protected */
    /* renamed from: n */
    public void mo1890n(Item auq) {
        switch (bFf().mo6893i(f8549RD)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f8549RD, new Object[]{auq}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f8549RD, new Object[]{auq}));
                break;
        }
        m34939m(auq);
    }

    /* renamed from: sK */
    public Asset mo20248sK() {
        switch (bFf().mo6893i(f8547Ob)) {
            case 0:
                return null;
            case 2:
                return (Asset) bFf().mo5606d(new aCE(this, f8547Ob, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f8547Ob, new Object[0]));
                break;
        }
        return m34940sJ();
    }

    /* renamed from: S */
    public void mo10S() {
        super.mo10S();
    }

    @C0064Am(aul = "2008c4ddae35cf8910eed5ac498b37d9", aum = 0)
    /* renamed from: A */
    private boolean m34933A(ItemType jCVar) {
        boolean z;
        boolean z2;
        if (!(jCVar instanceof ComponentType) || size() > 0) {
            return false;
        }
        ComponentType aef = (ComponentType) jCVar;
        boolean z3 = aef.cXV() == btW().bmQ().bmW();
        if (aef.cuH() == cYr) {
            z = true;
        } else {
            z = false;
        }
        Character agj = btW().bmS().mo13241al().agj();
        if (agj instanceof Player) {
            z2 = jCVar.mo22858m(agj);
        } else {
            z2 = true;
        }
        if (!z3 || !z || !z2) {
            return false;
        }
        return true;
    }

    @C0064Am(aul = "cabf6b7f69f0596e42daf8cc05a863c2", aum = 0)
    private ShipSector btV() {
        return btU();
    }

    @C0064Am(aul = "5bc71a48e874b02a504b0b301b4598e9", aum = 0)
    /* renamed from: d */
    private void m34937d(ShipSector or) {
        m34936c(or);
    }

    @C0064Am(aul = "38fd7371c7c8f57faf50f822f4af8620", aum = 0)
    private Item btX() {
        return btT();
    }

    @C0064Am(aul = "3407063bcf41dc612e16cca75bd13712", aum = 0)
    /* renamed from: sJ */
    private Asset m34940sJ() {
        return f8550jn;
    }

    @C0064Am(aul = "c8dd13b1be28f83fef9cf551b801f768", aum = 0)
    private int btZ() {
        return eeW;
    }

    @C0064Am(aul = "c4a7a3931cab5278ddaece5f82bac518", aum = 0)
    private C2893a bub() {
        return cYr;
    }

    @C0064Am(aul = "0156e074852f3f8898f07b519375da5f", aum = 0)
    /* renamed from: a */
    private void m34935a(Item auq, ItemLocation aag) {
        m34941y(auq);
        btW().bmS().mo13241al().mo18316f((Component) auq);
    }

    @C0064Am(aul = "a8cff132a75b7993b1087e8959f11826", aum = 0)
    /* renamed from: m */
    private void m34939m(Item auq) {
        if (btT() == auq) {
            btW().bmS().mo13241al().mo18319h((Component) auq);
            m34941y((Item) null);
        }
    }

    @C0064Am(aul = "f72cc588bb3a0e70eb955af1294ab20d", aum = 0)
    /* renamed from: g */
    private void m34938g(Item auq) {
        if (!mo20242B(auq.bAP())) {
            throw new C2293dh(((NLSItem) ala().aIY().mo6310c(NLSManager.C1472a.ITEM)).bto());
        }
    }

    /* renamed from: a.lX$a */
    public enum C2893a {
        SMALL,
        MEDIUM,
        LARGE
    }
}
