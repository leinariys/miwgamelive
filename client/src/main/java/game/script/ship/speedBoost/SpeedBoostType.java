package game.script.ship.speedBoost;

import game.network.message.externalizable.aCE;
import game.script.resource.Asset;
import game.script.template.BaseTaikodomContent;
import logic.baa.*;
import logic.data.mbean.C0836MC;
import logic.res.code.C5640aRc;
import logic.res.code.C5663aRz;
import logic.res.code.C6494any;
import logic.res.html.C2491fm;
import p001a.*;

import java.util.Collection;

@C0566Hp
@C5511aMd
@C6485anp
/* renamed from: a.lJ */
/* compiled from: a */
public class SpeedBoostType extends BaseTaikodomContent implements C1616Xf {

    /* renamed from: VE */
    public static final C2491fm f8505VE = null;

    /* renamed from: VF */
    public static final C2491fm f8506VF = null;

    /* renamed from: VG */
    public static final C2491fm f8507VG = null;

    /* renamed from: VH */
    public static final C2491fm f8508VH = null;

    /* renamed from: VI */
    public static final C2491fm f8509VI = null;

    /* renamed from: VJ */
    public static final C2491fm f8510VJ = null;

    /* renamed from: VK */
    public static final C2491fm f8511VK = null;

    /* renamed from: VL */
    public static final C2491fm f8512VL = null;
    /* renamed from: Vt */
    public static final C5663aRz f8514Vt = null;
    /* renamed from: Vv */
    public static final C5663aRz f8516Vv = null;
    /* renamed from: Vx */
    public static final C5663aRz f8518Vx = null;
    /* renamed from: Vz */
    public static final C5663aRz f8520Vz = null;
    public static final int _m_fieldCount = 0;
    public static final C5663aRz[] _m_fields = null;
    public static final int _m_methodCount = 0;
    public static final C2491fm[] _m_methods = null;
    public static final C5663aRz aAe = null;
    public static final C5663aRz aAg = null;
    public static final C2491fm aAh = null;
    public static final C2491fm aAi = null;
    public static final C2491fm aAj = null;
    public static final C2491fm aAk = null;
    public static final C2491fm aAl = null;
    public static final C2491fm aAm = null;
    public static final C5663aRz atS = null;
    /* renamed from: dN */
    public static final C2491fm f8521dN = null;
    public static final long serialVersionUID = 0;
    /* renamed from: yg */
    public static final C2491fm f8522yg = null;
    public static C6494any ___iScriptClass;
    @C0064Am(aul = "6691ab8f67242c411e58cf9c85b1e074", aum = 3)

    /* renamed from: Vs */
    private static C3892wO f8513Vs;
    @C0064Am(aul = "36e1cb14132dbfb24889b0ec2a8a29d3", aum = 4)

    /* renamed from: Vu */
    private static C3892wO f8515Vu;
    @C0064Am(aul = "6f09d398b7698d6278fbad1563d428ba", aum = 6)

    /* renamed from: Vw */
    private static C3892wO f8517Vw;
    @C0064Am(aul = "b7efd121f73916f4840bab0831461891", aum = 5)

    /* renamed from: Vy */
    private static C3892wO f8519Vy;
    @C0064Am(aul = "92c670f229fe208a2643f825f089a2e5", aum = 0)
    private static float aAc;
    @C0064Am(aul = "6835cac81de85fad250af753771a77d5", aum = 1)
    private static Asset aAd;
    @C0064Am(aul = "1dfa34af20261536a19a522c330ba75c", aum = 2)
    private static Asset aAf;

    static {
        m34790V();
    }

    public SpeedBoostType() {
        super((C5540aNg) null);
        super.mo10S();
    }

    public SpeedBoostType(C5540aNg ang) {
        super(ang);
    }

    /* renamed from: V */
    static void m34790V() {
        serialVersionUID = (long) 1;
        _m_fieldCount = BaseTaikodomContent._m_fieldCount + 7;
        _m_methodCount = BaseTaikodomContent._m_methodCount + 16;
        int i = BaseTaikodomContent._m_fieldCount;
        C5663aRz[] arzArr = new C5663aRz[(i + 7)];
        C5663aRz b = C5640aRc.m17844b(SpeedBoostType.class, "92c670f229fe208a2643f825f089a2e5", i);
        atS = b;
        arzArr[i] = b;
        int i2 = i + 1;
        C5663aRz b2 = C5640aRc.m17844b(SpeedBoostType.class, "6835cac81de85fad250af753771a77d5", i2);
        aAe = b2;
        arzArr[i2] = b2;
        int i3 = i2 + 1;
        C5663aRz b3 = C5640aRc.m17844b(SpeedBoostType.class, "1dfa34af20261536a19a522c330ba75c", i3);
        aAg = b3;
        arzArr[i3] = b3;
        int i4 = i3 + 1;
        C5663aRz b4 = C5640aRc.m17844b(SpeedBoostType.class, "6691ab8f67242c411e58cf9c85b1e074", i4);
        f8514Vt = b4;
        arzArr[i4] = b4;
        int i5 = i4 + 1;
        C5663aRz b5 = C5640aRc.m17844b(SpeedBoostType.class, "36e1cb14132dbfb24889b0ec2a8a29d3", i5);
        f8516Vv = b5;
        arzArr[i5] = b5;
        int i6 = i5 + 1;
        C5663aRz b6 = C5640aRc.m17844b(SpeedBoostType.class, "b7efd121f73916f4840bab0831461891", i6);
        f8520Vz = b6;
        arzArr[i6] = b6;
        int i7 = i6 + 1;
        C5663aRz b7 = C5640aRc.m17844b(SpeedBoostType.class, "6f09d398b7698d6278fbad1563d428ba", i7);
        f8518Vx = b7;
        arzArr[i7] = b7;
        int i8 = i7 + 1;
        _m_fields = arzArr;
        C1634Xv.m11725a((Object[]) BaseTaikodomContent._m_fields, (Object[]) _m_fields);
        int i9 = BaseTaikodomContent._m_methodCount;
        C2491fm[] fmVarArr = new C2491fm[(i9 + 16)];
        C2491fm a = C4105zY.m41624a(SpeedBoostType.class, "87812b524b6bdb18d64f6ffe046e3311", i9);
        f8522yg = a;
        fmVarArr[i9] = a;
        int i10 = i9 + 1;
        C2491fm a2 = C4105zY.m41624a(SpeedBoostType.class, "0957d1022bc8f7965bc006651be35b3f", i10);
        aAh = a2;
        fmVarArr[i10] = a2;
        int i11 = i10 + 1;
        C2491fm a3 = C4105zY.m41624a(SpeedBoostType.class, "b54f231defd48fe596aa483669b306cc", i11);
        aAi = a3;
        fmVarArr[i11] = a3;
        int i12 = i11 + 1;
        C2491fm a4 = C4105zY.m41624a(SpeedBoostType.class, "4ff51a1c3849a2bb0ff56144fe31ff90", i12);
        aAj = a4;
        fmVarArr[i12] = a4;
        int i13 = i12 + 1;
        C2491fm a5 = C4105zY.m41624a(SpeedBoostType.class, "a17863b9021ca4a29eaea7a6efcdf9e2", i13);
        aAk = a5;
        fmVarArr[i13] = a5;
        int i14 = i13 + 1;
        C2491fm a6 = C4105zY.m41624a(SpeedBoostType.class, "8e6e7a770d21fbf85a29aa0f6713cc7c", i14);
        aAl = a6;
        fmVarArr[i14] = a6;
        int i15 = i14 + 1;
        C2491fm a7 = C4105zY.m41624a(SpeedBoostType.class, "1d08d223c25683ae38bd28ebd0a77966", i15);
        f8505VE = a7;
        fmVarArr[i15] = a7;
        int i16 = i15 + 1;
        C2491fm a8 = C4105zY.m41624a(SpeedBoostType.class, "858659ebc5998a5eb4170e808b3bdc4e", i16);
        f8506VF = a8;
        fmVarArr[i16] = a8;
        int i17 = i16 + 1;
        C2491fm a9 = C4105zY.m41624a(SpeedBoostType.class, "70fa8ebd0270d076e4cb14499607f54f", i17);
        f8507VG = a9;
        fmVarArr[i17] = a9;
        int i18 = i17 + 1;
        C2491fm a10 = C4105zY.m41624a(SpeedBoostType.class, "70eb6d60c532a5596120803f2a08bdf8", i18);
        f8508VH = a10;
        fmVarArr[i18] = a10;
        int i19 = i18 + 1;
        C2491fm a11 = C4105zY.m41624a(SpeedBoostType.class, "409778f346fc98296d8c8310e04fe552", i19);
        f8511VK = a11;
        fmVarArr[i19] = a11;
        int i20 = i19 + 1;
        C2491fm a12 = C4105zY.m41624a(SpeedBoostType.class, "3e70f1751b9cc531e608808cc379b96f", i20);
        f8512VL = a12;
        fmVarArr[i20] = a12;
        int i21 = i20 + 1;
        C2491fm a13 = C4105zY.m41624a(SpeedBoostType.class, "e0ee49a3520b984a57905ff0c5c70520", i21);
        f8509VI = a13;
        fmVarArr[i21] = a13;
        int i22 = i21 + 1;
        C2491fm a14 = C4105zY.m41624a(SpeedBoostType.class, "df69de8f4fa9e81f0148195eb6b37689", i22);
        f8510VJ = a14;
        fmVarArr[i22] = a14;
        int i23 = i22 + 1;
        C2491fm a15 = C4105zY.m41624a(SpeedBoostType.class, "02be3d1116bcf0da4460bed32873861b", i23);
        aAm = a15;
        fmVarArr[i23] = a15;
        int i24 = i23 + 1;
        C2491fm a16 = C4105zY.m41624a(SpeedBoostType.class, "d87024159cf94bb83f0c2a7cab374cb6", i24);
        f8521dN = a16;
        fmVarArr[i24] = a16;
        int i25 = i24 + 1;
        _m_methods = fmVarArr;
        C1634Xv.m11725a((Object[]) BaseTaikodomContent._m_methods, (Object[]) _m_methods);
        ___iScriptClass = aUO.m18566a(SpeedBoostType.class, C0836MC.class, _m_fields, _m_methods);
    }

    /* renamed from: B */
    private void m34780B(Asset tCVar) {
        bFf().mo5608dq().mo3197f(aAe, tCVar);
    }

    /* renamed from: C */
    private void m34781C(Asset tCVar) {
        bFf().mo5608dq().mo3197f(aAg, tCVar);
    }

    /* renamed from: Mu */
    private float m34785Mu() {
        return bFf().mo5608dq().mo3211m(atS);
    }

    /* renamed from: Mv */
    private Asset m34786Mv() {
        return (Asset) bFf().mo5608dq().mo3214p(aAe);
    }

    /* renamed from: Mw */
    private Asset m34787Mw() {
        return (Asset) bFf().mo5608dq().mo3214p(aAg);
    }

    /* renamed from: ba */
    private void m34792ba(float f) {
        bFf().mo5608dq().mo3150a(atS, f);
    }

    /* renamed from: d */
    private void m34794d(C3892wO wOVar) {
        bFf().mo5608dq().mo3197f(f8514Vt, wOVar);
    }

    /* renamed from: e */
    private void m34795e(C3892wO wOVar) {
        bFf().mo5608dq().mo3197f(f8516Vv, wOVar);
    }

    /* renamed from: f */
    private void m34796f(C3892wO wOVar) {
        bFf().mo5608dq().mo3197f(f8518Vx, wOVar);
    }

    /* renamed from: g */
    private void m34797g(C3892wO wOVar) {
        bFf().mo5608dq().mo3197f(f8520Vz, wOVar);
    }

    /* renamed from: yE */
    private C3892wO m34803yE() {
        return (C3892wO) bFf().mo5608dq().mo3214p(f8514Vt);
    }

    /* renamed from: yF */
    private C3892wO m34804yF() {
        return (C3892wO) bFf().mo5608dq().mo3214p(f8516Vv);
    }

    /* renamed from: yG */
    private C3892wO m34805yG() {
        return (C3892wO) bFf().mo5608dq().mo3214p(f8518Vx);
    }

    /* renamed from: yH */
    private C3892wO m34806yH() {
        return (C3892wO) bFf().mo5608dq().mo3214p(f8520Vz);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Sfx Start")
    /* renamed from: E */
    public void mo20200E(Asset tCVar) {
        switch (bFf().mo6893i(aAj)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, aAj, new Object[]{tCVar}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, aAj, new Object[]{tCVar}));
                break;
        }
        m34782D(tCVar);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Sfx Stop")
    /* renamed from: G */
    public void mo20201G(Asset tCVar) {
        switch (bFf().mo6893i(aAl)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, aAl, new Object[]{tCVar}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, aAl, new Object[]{tCVar}));
                break;
        }
        m34783F(tCVar);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Sfx Stop")
    /* renamed from: MA */
    public Asset mo20202MA() {
        switch (bFf().mo6893i(aAk)) {
            case 0:
                return null;
            case 2:
                return (Asset) bFf().mo5606d(new aCE(this, aAk, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, aAk, new Object[0]));
                break;
        }
        return m34789Mz();
    }

    /* renamed from: MC */
    public SpeedBoost mo20203MC() {
        switch (bFf().mo6893i(aAm)) {
            case 0:
                return null;
            case 2:
                return (SpeedBoost) bFf().mo5606d(new aCE(this, aAm, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, aAm, new Object[0]));
                break;
        }
        return m34784MB();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Sfx Start")
    /* renamed from: My */
    public Asset mo20204My() {
        switch (bFf().mo6893i(aAi)) {
            case 0:
                return null;
            case 2:
                return (Asset) bFf().mo5606d(new aCE(this, aAi, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, aAi, new Object[0]));
                break;
        }
        return m34788Mx();
    }

    /* renamed from: T */
    public C6494any mo11T() {
        return ___iScriptClass;
    }

    /* renamed from: U */
    public C2491fm[] mo12U() {
        return _m_methods;
    }

    /* renamed from: W */
    public Object mo13W() {
        return new C0836MC(this);
    }

    /* renamed from: a */
    public Object mo14a(C0495Gr gr) {
        Object[] args = gr.getArgs();
        switch (gr.mo2417hq() - BaseTaikodomContent._m_methodCount) {
            case 0:
                return new Float(m34799js());
            case 1:
                m34793bb(((Float) args[0]).floatValue());
                return null;
            case 2:
                return m34788Mx();
            case 3:
                m34782D((Asset) args[0]);
                return null;
            case 4:
                return m34789Mz();
            case 5:
                m34783F((Asset) args[0]);
                return null;
            case 6:
                return m34807yK();
            case 7:
                m34798i((C3892wO) args[0]);
                return null;
            case 8:
                return m34808yM();
            case 9:
                m34800k((C3892wO) args[0]);
                return null;
            case 10:
                return m34810yQ();
            case 11:
                m34802o((C3892wO) args[0]);
                return null;
            case 12:
                return m34809yO();
            case 13:
                m34801m((C3892wO) args[0]);
                return null;
            case 14:
                return m34784MB();
            case 15:
                return m34791aT();
            default:
                return super.mo14a(gr);
        }
    }

    /* renamed from: a */
    public void mo15a(Collection collection, C0495Gr gr) {
        super.mo15a(collection, gr);
    }

    /* access modifiers changed from: protected */
    /* renamed from: aU */
    public <T extends aDJ> T mo745aU() {
        switch (bFf().mo6893i(f8521dN)) {
            case 0:
                return null;
            case 2:
                return (aDJ) bFf().mo5606d(new aCE(this, f8521dN, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f8521dN, new Object[0]));
                break;
        }
        return m34791aT();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Duration")
    /* renamed from: bc */
    public void mo20205bc(float f) {
        switch (bFf().mo6893i(aAh)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, aAh, new Object[]{new Float(f)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, aAh, new Object[]{new Float(f)}));
                break;
        }
        m34793bb(f);
    }

    /* renamed from: c */
    public C5663aRz[] mo25c() {
        return _m_fields;
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Linear Velocity")
    /* renamed from: j */
    public void mo20206j(C3892wO wOVar) {
        switch (bFf().mo6893i(f8506VF)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f8506VF, new Object[]{wOVar}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f8506VF, new Object[]{wOVar}));
                break;
        }
        m34798i(wOVar);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Duration")
    /* renamed from: jt */
    public float mo20207jt() {
        switch (bFf().mo6893i(f8522yg)) {
            case 0:
                return 0.0f;
            case 2:
                return ((Float) bFf().mo5606d(new aCE(this, f8522yg, new Object[0]))).floatValue();
            case 3:
                bFf().mo5606d(new aCE(this, f8522yg, new Object[0]));
                break;
        }
        return m34799js();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Linear Acceleration")
    /* renamed from: l */
    public void mo20208l(C3892wO wOVar) {
        switch (bFf().mo6893i(f8508VH)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f8508VH, new Object[]{wOVar}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f8508VH, new Object[]{wOVar}));
                break;
        }
        m34800k(wOVar);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Angular Velocity")
    /* renamed from: n */
    public void mo20209n(C3892wO wOVar) {
        switch (bFf().mo6893i(f8510VJ)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f8510VJ, new Object[]{wOVar}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f8510VJ, new Object[]{wOVar}));
                break;
        }
        m34801m(wOVar);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Angular Acceleration")
    /* renamed from: p */
    public void mo20210p(C3892wO wOVar) {
        switch (bFf().mo6893i(f8512VL)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f8512VL, new Object[]{wOVar}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f8512VL, new Object[]{wOVar}));
                break;
        }
        m34802o(wOVar);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Linear Velocity")
    /* renamed from: yL */
    public C3892wO mo20211yL() {
        switch (bFf().mo6893i(f8505VE)) {
            case 0:
                return null;
            case 2:
                return (C3892wO) bFf().mo5606d(new aCE(this, f8505VE, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f8505VE, new Object[0]));
                break;
        }
        return m34807yK();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Linear Acceleration")
    /* renamed from: yN */
    public C3892wO mo20212yN() {
        switch (bFf().mo6893i(f8507VG)) {
            case 0:
                return null;
            case 2:
                return (C3892wO) bFf().mo5606d(new aCE(this, f8507VG, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f8507VG, new Object[0]));
                break;
        }
        return m34808yM();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Angular Velocity")
    /* renamed from: yP */
    public C3892wO mo20213yP() {
        switch (bFf().mo6893i(f8509VI)) {
            case 0:
                return null;
            case 2:
                return (C3892wO) bFf().mo5606d(new aCE(this, f8509VI, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f8509VI, new Object[0]));
                break;
        }
        return m34809yO();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Angular Acceleration")
    /* renamed from: yR */
    public C3892wO mo20214yR() {
        switch (bFf().mo6893i(f8511VK)) {
            case 0:
                return null;
            case 2:
                return (C3892wO) bFf().mo5606d(new aCE(this, f8511VK, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f8511VK, new Object[0]));
                break;
        }
        return m34810yQ();
    }

    /* renamed from: S */
    public void mo10S() {
        super.mo10S();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Duration")
    @C0064Am(aul = "87812b524b6bdb18d64f6ffe046e3311", aum = 0)
    /* renamed from: js */
    private float m34799js() {
        return m34785Mu();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Duration")
    @C0064Am(aul = "0957d1022bc8f7965bc006651be35b3f", aum = 0)
    /* renamed from: bb */
    private void m34793bb(float f) {
        m34792ba(f);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Sfx Start")
    @C0064Am(aul = "b54f231defd48fe596aa483669b306cc", aum = 0)
    /* renamed from: Mx */
    private Asset m34788Mx() {
        return m34786Mv();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Sfx Start")
    @C0064Am(aul = "4ff51a1c3849a2bb0ff56144fe31ff90", aum = 0)
    /* renamed from: D */
    private void m34782D(Asset tCVar) {
        m34780B(tCVar);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Sfx Stop")
    @C0064Am(aul = "a17863b9021ca4a29eaea7a6efcdf9e2", aum = 0)
    /* renamed from: Mz */
    private Asset m34789Mz() {
        return m34787Mw();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Sfx Stop")
    @C0064Am(aul = "8e6e7a770d21fbf85a29aa0f6713cc7c", aum = 0)
    /* renamed from: F */
    private void m34783F(Asset tCVar) {
        m34781C(tCVar);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Linear Velocity")
    @C0064Am(aul = "1d08d223c25683ae38bd28ebd0a77966", aum = 0)
    /* renamed from: yK */
    private C3892wO m34807yK() {
        return m34803yE();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Linear Velocity")
    @C0064Am(aul = "858659ebc5998a5eb4170e808b3bdc4e", aum = 0)
    /* renamed from: i */
    private void m34798i(C3892wO wOVar) {
        m34794d(wOVar);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Linear Acceleration")
    @C0064Am(aul = "70fa8ebd0270d076e4cb14499607f54f", aum = 0)
    /* renamed from: yM */
    private C3892wO m34808yM() {
        return m34804yF();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Linear Acceleration")
    @C0064Am(aul = "70eb6d60c532a5596120803f2a08bdf8", aum = 0)
    /* renamed from: k */
    private void m34800k(C3892wO wOVar) {
        m34795e(wOVar);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Angular Acceleration")
    @C0064Am(aul = "409778f346fc98296d8c8310e04fe552", aum = 0)
    /* renamed from: yQ */
    private C3892wO m34810yQ() {
        return m34806yH();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Angular Acceleration")
    @C0064Am(aul = "3e70f1751b9cc531e608808cc379b96f", aum = 0)
    /* renamed from: o */
    private void m34802o(C3892wO wOVar) {
        m34797g(wOVar);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Angular Velocity")
    @C0064Am(aul = "e0ee49a3520b984a57905ff0c5c70520", aum = 0)
    /* renamed from: yO */
    private C3892wO m34809yO() {
        return m34805yG();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Angular Velocity")
    @C0064Am(aul = "df69de8f4fa9e81f0148195eb6b37689", aum = 0)
    /* renamed from: m */
    private void m34801m(C3892wO wOVar) {
        m34796f(wOVar);
    }

    @C0064Am(aul = "02be3d1116bcf0da4460bed32873861b", aum = 0)
    /* renamed from: MB */
    private SpeedBoost m34784MB() {
        return (SpeedBoost) mo745aU();
    }

    @C0064Am(aul = "d87024159cf94bb83f0c2a7cab374cb6", aum = 0)
    /* renamed from: aT */
    private <T extends aDJ> T m34791aT() {
        T t = (SpeedBoost) bFf().mo6865M(SpeedBoost.class);
        t.mo17889e(this);
        return t;
    }
}
