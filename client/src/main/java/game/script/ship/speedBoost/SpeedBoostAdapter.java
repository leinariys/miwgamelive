package game.script.ship.speedBoost;

import game.network.message.externalizable.aCE;
import game.script.util.GameObjectAdapter;
import logic.baa.C1616Xf;
import logic.baa.C1634Xv;
import logic.baa.C5511aMd;
import logic.baa.C6485anp;
import logic.data.mbean.C1601XR;
import logic.res.code.C5663aRz;
import logic.res.code.C6494any;
import logic.res.html.C2491fm;
import p001a.*;

import java.util.Collection;

@C5511aMd
@C6485anp
/* renamed from: a.aHz  reason: case insensitive filesystem */
/* compiled from: a */
public class SpeedBoostAdapter extends GameObjectAdapter<SpeedBoostAdapter> implements C1616Xf {
    public static final int _m_fieldCount = 0;
    public static final C5663aRz[] _m_fields = null;
    public static final int _m_methodCount = 0;
    public static final C2491fm[] _m_methods = null;
    public static final long serialVersionUID = 0;
    /* renamed from: yc */
    public static final C2491fm f3016yc = null;
    /* renamed from: yd */
    public static final C2491fm f3017yd = null;
    /* renamed from: ye */
    public static final C2491fm f3018ye = null;
    /* renamed from: yf */
    public static final C2491fm f3019yf = null;
    /* renamed from: yg */
    public static final C2491fm f3020yg = null;
    public static C6494any ___iScriptClass;

    static {
        m15329V();
    }

    public SpeedBoostAdapter() {
        super((C5540aNg) null);
        super.mo10S();
    }

    public SpeedBoostAdapter(C5540aNg ang) {
        super(ang);
    }

    /* renamed from: V */
    static void m15329V() {
        serialVersionUID = (long) 1;
        _m_fieldCount = GameObjectAdapter._m_fieldCount + 0;
        _m_methodCount = GameObjectAdapter._m_methodCount + 5;
        _m_fields = new C5663aRz[(GameObjectAdapter._m_fieldCount + 0)];
        C1634Xv.m11725a((Object[]) GameObjectAdapter._m_fields, (Object[]) _m_fields);
        int i = GameObjectAdapter._m_methodCount;
        C2491fm[] fmVarArr = new C2491fm[(i + 5)];
        C2491fm a = C4105zY.m41624a(SpeedBoostAdapter.class, "5672cd5a52927ccc5bf5f7b196dc9abf", i);
        f3016yc = a;
        fmVarArr[i] = a;
        int i2 = i + 1;
        C2491fm a2 = C4105zY.m41624a(SpeedBoostAdapter.class, "7cb128454d4701f2cc5653ab6bf88a71", i2);
        f3017yd = a2;
        fmVarArr[i2] = a2;
        int i3 = i2 + 1;
        C2491fm a3 = C4105zY.m41624a(SpeedBoostAdapter.class, "89fdc5bb40c2cf8c416f842277091115", i3);
        f3018ye = a3;
        fmVarArr[i3] = a3;
        int i4 = i3 + 1;
        C2491fm a4 = C4105zY.m41624a(SpeedBoostAdapter.class, "06fd3221a379462e716d59b66d1f71af", i4);
        f3019yf = a4;
        fmVarArr[i4] = a4;
        int i5 = i4 + 1;
        C2491fm a5 = C4105zY.m41624a(SpeedBoostAdapter.class, "db30ca239f5d959f05ca9f6cb643bba0", i5);
        f3020yg = a5;
        fmVarArr[i5] = a5;
        int i6 = i5 + 1;
        _m_methods = fmVarArr;
        C1634Xv.m11725a((Object[]) GameObjectAdapter._m_methods, (Object[]) _m_methods);
        ___iScriptClass = aUO.m18566a(SpeedBoostAdapter.class, C1601XR.class, _m_fields, _m_methods);
    }

    /* renamed from: T */
    public C6494any mo11T() {
        return ___iScriptClass;
    }

    /* renamed from: U */
    public C2491fm[] mo12U() {
        return _m_methods;
    }

    /* renamed from: W */
    public Object mo13W() {
        return new C1601XR(this);
    }

    /* renamed from: a */
    public Object mo14a(C0495Gr gr) {
        gr.getArgs();
        switch (gr.mo2417hq() - GameObjectAdapter._m_methodCount) {
            case 0:
                return m15330jk();
            case 1:
                return m15331jm();
            case 2:
                return m15332jo();
            case 3:
                return m15333jq();
            case 4:
                return new Float(m15334js());
            default:
                return super.mo14a(gr);
        }
    }

    /* renamed from: a */
    public void mo15a(Collection collection, C0495Gr gr) {
        super.mo15a(collection, gr);
    }

    /* renamed from: c */
    public C5663aRz[] mo25c() {
        return _m_fields;
    }

    /* renamed from: jl */
    public C3892wO mo9299jl() {
        switch (bFf().mo6893i(f3016yc)) {
            case 0:
                return null;
            case 2:
                return (C3892wO) bFf().mo5606d(new aCE(this, f3016yc, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f3016yc, new Object[0]));
                break;
        }
        return m15330jk();
    }

    /* renamed from: jn */
    public C3892wO mo9300jn() {
        switch (bFf().mo6893i(f3017yd)) {
            case 0:
                return null;
            case 2:
                return (C3892wO) bFf().mo5606d(new aCE(this, f3017yd, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f3017yd, new Object[0]));
                break;
        }
        return m15331jm();
    }

    /* renamed from: jp */
    public C3892wO mo9301jp() {
        switch (bFf().mo6893i(f3018ye)) {
            case 0:
                return null;
            case 2:
                return (C3892wO) bFf().mo5606d(new aCE(this, f3018ye, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f3018ye, new Object[0]));
                break;
        }
        return m15332jo();
    }

    /* renamed from: jr */
    public C3892wO mo9302jr() {
        switch (bFf().mo6893i(f3019yf)) {
            case 0:
                return null;
            case 2:
                return (C3892wO) bFf().mo5606d(new aCE(this, f3019yf, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f3019yf, new Object[0]));
                break;
        }
        return m15333jq();
    }

    /* renamed from: jt */
    public float mo9303jt() {
        switch (bFf().mo6893i(f3020yg)) {
            case 0:
                return 0.0f;
            case 2:
                return ((Float) bFf().mo5606d(new aCE(this, f3020yg, new Object[0]))).floatValue();
            case 3:
                bFf().mo5606d(new aCE(this, f3020yg, new Object[0]));
                break;
        }
        return m15334js();
    }

    /* renamed from: S */
    public void mo10S() {
        super.mo10S();
    }

    @C0064Am(aul = "5672cd5a52927ccc5bf5f7b196dc9abf", aum = 0)
    /* renamed from: jk */
    private C3892wO m15330jk() {
        return ((SpeedBoostAdapter) mo16071IG()).mo9299jl();
    }

    @C0064Am(aul = "7cb128454d4701f2cc5653ab6bf88a71", aum = 0)
    /* renamed from: jm */
    private C3892wO m15331jm() {
        return ((SpeedBoostAdapter) mo16071IG()).mo9300jn();
    }

    @C0064Am(aul = "89fdc5bb40c2cf8c416f842277091115", aum = 0)
    /* renamed from: jo */
    private C3892wO m15332jo() {
        return ((SpeedBoostAdapter) mo16071IG()).mo9301jp();
    }

    @C0064Am(aul = "06fd3221a379462e716d59b66d1f71af", aum = 0)
    /* renamed from: jq */
    private C3892wO m15333jq() {
        return ((SpeedBoostAdapter) mo16071IG()).mo9302jr();
    }

    @C0064Am(aul = "db30ca239f5d959f05ca9f6cb643bba0", aum = 0)
    /* renamed from: js */
    private float m15334js() {
        return ((SpeedBoostAdapter) mo16071IG()).mo9303jt();
    }
}
