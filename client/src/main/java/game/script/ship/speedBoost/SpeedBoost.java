package game.script.ship.speedBoost;

import game.network.message.externalizable.C5873acB;
import game.network.message.externalizable.aCE;
import game.script.TaikodomObject;
import game.script.resource.Asset;
import game.script.ship.Ship;
import game.script.ship.categories.Fighter;
import game.script.template.BaseTaikodomContent;
import game.script.util.AdapterLikedList;
import logic.aaa.C1506WA;
import logic.baa.*;
import logic.data.mbean.C0190CP;
import logic.data.mbean.C2595hK;
import logic.res.code.C5640aRc;
import logic.res.code.C5663aRz;
import logic.res.code.C6494any;
import logic.res.html.C2491fm;
import p001a.*;

import java.util.Collection;

@C6485anp
@C2712iu(mo19786Bt = BaseTaikodomContent.class)
@C5511aMd
/* renamed from: a.dc */
/* compiled from: a */
public class SpeedBoost extends TaikodomObject implements C6480ank<SpeedBoostAdapter>, C6872avM, C6872avM {

    /* renamed from: Lm */
    public static final C2491fm f6557Lm = null;
    /* renamed from: Vt */
    public static final C5663aRz f6559Vt = null;
    /* renamed from: Vv */
    public static final C5663aRz f6561Vv = null;
    /* renamed from: Vx */
    public static final C5663aRz f6563Vx = null;
    /* renamed from: Vz */
    public static final C5663aRz f6565Vz = null;
    public static final int _m_fieldCount = 0;
    public static final C5663aRz[] _m_fields = null;
    public static final int _m_methodCount = 0;
    public static final C2491fm[] _m_methods = null;
    public static final C5663aRz aAe = null;
    public static final C5663aRz aAg = null;
    public static final C5663aRz aOx = null;
    public static final C2491fm aPe = null;
    public static final C2491fm aPf = null;
    public static final C2491fm aPj = null;
    public static final C2491fm aPk = null;
    public static final C2491fm aPl = null;
    public static final C2491fm aPm = null;
    public static final C5663aRz atS = null;
    public static final C2491fm bFy = null;
    public static final C5663aRz bjS = null;
    public static final C2491fm bjX = null;
    public static final C2491fm bka = null;
    public static final C2491fm dDK = null;
    public static final C2491fm dDM = null;
    public static final C5663aRz icA = null;
    public static final C5663aRz icB = null;
    public static final C5663aRz icC = null;
    public static final C5663aRz icD = null;
    public static final C5663aRz icE = null;
    public static final C5663aRz icF = null;
    public static final C5663aRz icG = null;
    public static final C2491fm icH = null;
    public static final C2491fm icI = null;
    public static final C2491fm icJ = null;
    public static final C2491fm icK = null;
    public static final C2491fm icL = null;
    public static final C2491fm icM = null;
    public static final C2491fm icN = null;
    public static final C2491fm icO = null;
    public static final C2491fm icP = null;
    public static final C2491fm icQ = null;
    public static final C2491fm icR = null;
    public static final C2491fm icS = null;
    public static final C2491fm icT = null;
    public static final C2491fm icU = null;
    public static final C2491fm icV = null;
    public static final C2491fm icW = null;
    public static final C2491fm icX = null;
    public static final C2491fm icY = null;
    public static final C5663aRz icx = null;
    public static final C5663aRz icy = null;
    public static final C5663aRz icz = null;
    public static final long serialVersionUID = 0;
    /* renamed from: yg */
    public static final C2491fm f6566yg = null;
    public static C6494any ___iScriptClass;
    @C0064Am(aul = "c5481a5757300575a39f344d8e6424d2", aum = 3)

    /* renamed from: Vs */
    private static C3892wO f6558Vs;
    @C0064Am(aul = "7a197da26d390cc52864874ce81c27d1", aum = 4)

    /* renamed from: Vu */
    private static C3892wO f6560Vu;
    @C0064Am(aul = "991f2ce6602ae1e76d0a859198768e66", aum = 6)

    /* renamed from: Vw */
    private static C3892wO f6562Vw;
    @C0064Am(aul = "38192dd22a608dd174a0927455b08b17", aum = 5)

    /* renamed from: Vy */
    private static C3892wO f6564Vy;
    @C0064Am(aul = "40a95949b8fd149603eb2ebeda37b59d", aum = 0)
    private static float aAc;
    @C0064Am(aul = "c31b95be69859ca58b89f38b3fc951df", aum = 1)
    private static Asset aAd;
    @C0064Am(aul = "f456366e1fbed3f68777697d0187e42d", aum = 2)
    private static Asset aAf;
    @C0064Am(aul = "2ad9b60f498cde072627c0090a683f52", aum = 13)
    @C5566aOg
    private static AdapterLikedList<SpeedBoostAdapter> aOw;
    @C0064Am(aul = "d5faa07f87438c63f89d252f0b7e27b8", aum = 14)
    private static float bjR;
    @C0064Am(aul = "0c558e45d7eb65103301cc83497706da", aum = 7)
    private static Fighter cAq;
    @C3122oB(mo20937Uq = C3122oB.C3123a.NOT_REPLICATED)
    @C0064Am(aul = "d30afa60074c05f1ea8779a4893ff528", aum = 8)
    @C5256aCi
    private static C2286b cAr;
    @C3122oB(mo20937Uq = C3122oB.C3123a.NOT_REPLICATED)
    @C0064Am(aul = "5b95ca178cb140f7b3e6332a512b185e", aum = 9)
    @C5256aCi
    private static float cAs;
    @C3122oB(mo20937Uq = C3122oB.C3123a.NOT_REPLICATED)
    @C0064Am(aul = "7c564dddd7183fb74579a8c4ff87624b", aum = 10)
    @C5256aCi
    private static long cAt;
    @C3122oB(mo20937Uq = C3122oB.C3123a.NOT_REPLICATED)
    @C0064Am(aul = "fd6e363bcea1470b09789e7de2087770", aum = 11)
    @C5256aCi
    private static long cAu;
    @C3122oB(mo20937Uq = C3122oB.C3123a.NOT_REPLICATED)
    @C0064Am(aul = "ae16bc1c20af4ae3eb3b117da5694f4b", aum = 12)
    @C5256aCi
    private static int cAv;
    @C0064Am(aul = "ba6c37c89179ce356fd81c714cc8dcdf", aum = 15)
    private static C3892wO cAw;
    @C0064Am(aul = "968a3a24fd91de82e7bedbc81372a164", aum = 16)
    private static C3892wO cAx;
    @C0064Am(aul = "a8a70da906f6114eae3c330fe9deca84", aum = 17)
    private static C3892wO cAy;
    @C0064Am(aul = "aea7bdcc26ff3f48c0491bb15cb22783", aum = 18)
    private static C3892wO cAz;

    static {
        m29090V();
    }

    private transient int hFe;

    public SpeedBoost() {
        super((C5540aNg) null);
        super.mo10S();
    }

    public SpeedBoost(C5540aNg ang) {
        super(ang);
    }

    public SpeedBoost(SpeedBoostType lJVar) {
        super((C5540aNg) null);
        super._m_script_init(lJVar);
    }

    /* renamed from: V */
    static void m29090V() {
        serialVersionUID = (long) 1;
        _m_fieldCount = TaikodomObject._m_fieldCount + 19;
        _m_methodCount = TaikodomObject._m_methodCount + 31;
        int i = TaikodomObject._m_fieldCount;
        C5663aRz[] arzArr = new C5663aRz[(i + 19)];
        C5663aRz b = C5640aRc.m17844b(SpeedBoost.class, "40a95949b8fd149603eb2ebeda37b59d", i);
        atS = b;
        arzArr[i] = b;
        int i2 = i + 1;
        C5663aRz b2 = C5640aRc.m17844b(SpeedBoost.class, "c31b95be69859ca58b89f38b3fc951df", i2);
        aAe = b2;
        arzArr[i2] = b2;
        int i3 = i2 + 1;
        C5663aRz b3 = C5640aRc.m17844b(SpeedBoost.class, "f456366e1fbed3f68777697d0187e42d", i3);
        aAg = b3;
        arzArr[i3] = b3;
        int i4 = i3 + 1;
        C5663aRz b4 = C5640aRc.m17844b(SpeedBoost.class, "c5481a5757300575a39f344d8e6424d2", i4);
        f6559Vt = b4;
        arzArr[i4] = b4;
        int i5 = i4 + 1;
        C5663aRz b5 = C5640aRc.m17844b(SpeedBoost.class, "7a197da26d390cc52864874ce81c27d1", i5);
        f6561Vv = b5;
        arzArr[i5] = b5;
        int i6 = i5 + 1;
        C5663aRz b6 = C5640aRc.m17844b(SpeedBoost.class, "38192dd22a608dd174a0927455b08b17", i6);
        f6565Vz = b6;
        arzArr[i6] = b6;
        int i7 = i6 + 1;
        C5663aRz b7 = C5640aRc.m17844b(SpeedBoost.class, "991f2ce6602ae1e76d0a859198768e66", i7);
        f6563Vx = b7;
        arzArr[i7] = b7;
        int i8 = i7 + 1;
        C5663aRz b8 = C5640aRc.m17844b(SpeedBoost.class, "0c558e45d7eb65103301cc83497706da", i8);
        icx = b8;
        arzArr[i8] = b8;
        int i9 = i8 + 1;
        C5663aRz b9 = C5640aRc.m17844b(SpeedBoost.class, "d30afa60074c05f1ea8779a4893ff528", i9);
        icy = b9;
        arzArr[i9] = b9;
        int i10 = i9 + 1;
        C5663aRz b10 = C5640aRc.m17844b(SpeedBoost.class, "5b95ca178cb140f7b3e6332a512b185e", i10);
        icz = b10;
        arzArr[i10] = b10;
        int i11 = i10 + 1;
        C5663aRz b11 = C5640aRc.m17844b(SpeedBoost.class, "7c564dddd7183fb74579a8c4ff87624b", i11);
        icA = b11;
        arzArr[i11] = b11;
        int i12 = i11 + 1;
        C5663aRz b12 = C5640aRc.m17844b(SpeedBoost.class, "fd6e363bcea1470b09789e7de2087770", i12);
        icB = b12;
        arzArr[i12] = b12;
        int i13 = i12 + 1;
        C5663aRz b13 = C5640aRc.m17844b(SpeedBoost.class, "ae16bc1c20af4ae3eb3b117da5694f4b", i13);
        icC = b13;
        arzArr[i13] = b13;
        int i14 = i13 + 1;
        C5663aRz b14 = C5640aRc.m17844b(SpeedBoost.class, "2ad9b60f498cde072627c0090a683f52", i14);
        aOx = b14;
        arzArr[i14] = b14;
        int i15 = i14 + 1;
        C5663aRz b15 = C5640aRc.m17844b(SpeedBoost.class, "d5faa07f87438c63f89d252f0b7e27b8", i15);
        bjS = b15;
        arzArr[i15] = b15;
        int i16 = i15 + 1;
        C5663aRz b16 = C5640aRc.m17844b(SpeedBoost.class, "ba6c37c89179ce356fd81c714cc8dcdf", i16);
        icD = b16;
        arzArr[i16] = b16;
        int i17 = i16 + 1;
        C5663aRz b17 = C5640aRc.m17844b(SpeedBoost.class, "968a3a24fd91de82e7bedbc81372a164", i17);
        icE = b17;
        arzArr[i17] = b17;
        int i18 = i17 + 1;
        C5663aRz b18 = C5640aRc.m17844b(SpeedBoost.class, "a8a70da906f6114eae3c330fe9deca84", i18);
        icF = b18;
        arzArr[i18] = b18;
        int i19 = i18 + 1;
        C5663aRz b19 = C5640aRc.m17844b(SpeedBoost.class, "aea7bdcc26ff3f48c0491bb15cb22783", i19);
        icG = b19;
        arzArr[i19] = b19;
        int i20 = i19 + 1;
        _m_fields = arzArr;
        C1634Xv.m11725a((Object[]) TaikodomObject._m_fields, (Object[]) _m_fields);
        int i21 = TaikodomObject._m_methodCount;
        C2491fm[] fmVarArr = new C2491fm[(i21 + 31)];
        C2491fm a = C4105zY.m41624a(SpeedBoost.class, "170a5d12f624c7136a8c0ad3b644f76a", i21);
        bjX = a;
        fmVarArr[i21] = a;
        int i22 = i21 + 1;
        C2491fm a2 = C4105zY.m41624a(SpeedBoost.class, "5ae873b234e9de8a4c997b20fc437772", i22);
        icH = a2;
        fmVarArr[i22] = a2;
        int i23 = i22 + 1;
        C2491fm a3 = C4105zY.m41624a(SpeedBoost.class, "5e29ace893079f927f0e10a68e5bb562", i23);
        icI = a3;
        fmVarArr[i23] = a3;
        int i24 = i23 + 1;
        C2491fm a4 = C4105zY.m41624a(SpeedBoost.class, "b7580c3f3d6a2a9e91ce500658c41adc", i24);
        icJ = a4;
        fmVarArr[i24] = a4;
        int i25 = i24 + 1;
        C2491fm a5 = C4105zY.m41624a(SpeedBoost.class, "e8236174395c4d66c0810d746fdea2e6", i25);
        icK = a5;
        fmVarArr[i25] = a5;
        int i26 = i25 + 1;
        C2491fm a6 = C4105zY.m41624a(SpeedBoost.class, "074e057d6e85d267459cddb569722381", i26);
        icL = a6;
        fmVarArr[i26] = a6;
        int i27 = i26 + 1;
        C2491fm a7 = C4105zY.m41624a(SpeedBoost.class, "1e3631bd20929eed90ef233402f72fd9", i27);
        icM = a7;
        fmVarArr[i27] = a7;
        int i28 = i27 + 1;
        C2491fm a8 = C4105zY.m41624a(SpeedBoost.class, "7462ac54b691f41b44dd9349e79e56a3", i28);
        icN = a8;
        fmVarArr[i28] = a8;
        int i29 = i28 + 1;
        C2491fm a9 = C4105zY.m41624a(SpeedBoost.class, "43f7cdca42a2a42304c39be680781315", i29);
        icO = a9;
        fmVarArr[i29] = a9;
        int i30 = i29 + 1;
        C2491fm a10 = C4105zY.m41624a(SpeedBoost.class, "bb34fc6ecbf021b2888d3aaad61e1052", i30);
        icP = a10;
        fmVarArr[i30] = a10;
        int i31 = i30 + 1;
        C2491fm a11 = C4105zY.m41624a(SpeedBoost.class, "2bf87a43e9913d6e734191d63930243c", i31);
        icQ = a11;
        fmVarArr[i31] = a11;
        int i32 = i31 + 1;
        C2491fm a12 = C4105zY.m41624a(SpeedBoost.class, "2b621d0793a4a533c2998c9e1f43e22d", i32);
        aPf = a12;
        fmVarArr[i32] = a12;
        int i33 = i32 + 1;
        C2491fm a13 = C4105zY.m41624a(SpeedBoost.class, "6e05e61e3ac39613ffd7287c10d3009b", i33);
        aPe = a13;
        fmVarArr[i33] = a13;
        int i34 = i33 + 1;
        C2491fm a14 = C4105zY.m41624a(SpeedBoost.class, "9ab2d65fabc3b53aa765f47b48d2b43b", i34);
        icR = a14;
        fmVarArr[i34] = a14;
        int i35 = i34 + 1;
        C2491fm a15 = C4105zY.m41624a(SpeedBoost.class, "5cfe00bd3e150aad2b42b05b29d0bb4a", i35);
        icS = a15;
        fmVarArr[i35] = a15;
        int i36 = i35 + 1;
        C2491fm a16 = C4105zY.m41624a(SpeedBoost.class, "dcc5b63fb3203a1658ba32483b194304", i36);
        icT = a16;
        fmVarArr[i36] = a16;
        int i37 = i36 + 1;
        C2491fm a17 = C4105zY.m41624a(SpeedBoost.class, "c09cec99ca2d7b9deb2d2a45329bf184", i37);
        bFy = a17;
        fmVarArr[i37] = a17;
        int i38 = i37 + 1;
        C2491fm a18 = C4105zY.m41624a(SpeedBoost.class, "f47f2c363d72a7ec6aa4bcad6de1a2ca", i38);
        f6566yg = a18;
        fmVarArr[i38] = a18;
        int i39 = i38 + 1;
        C2491fm a19 = C4105zY.m41624a(SpeedBoost.class, "00c6b4397d0e555f0e12a1e6bafa08f7", i39);
        icU = a19;
        fmVarArr[i39] = a19;
        int i40 = i39 + 1;
        C2491fm a20 = C4105zY.m41624a(SpeedBoost.class, "aa3c31f15ca5461f8ad05c62c84419e6", i40);
        bka = a20;
        fmVarArr[i40] = a20;
        int i41 = i40 + 1;
        C2491fm a21 = C4105zY.m41624a(SpeedBoost.class, "f6d3df5db2f2ff766951e978ad3a5d73", i41);
        dDK = a21;
        fmVarArr[i41] = a21;
        int i42 = i41 + 1;
        C2491fm a22 = C4105zY.m41624a(SpeedBoost.class, "59503241f56d12962c337ca88357d50c", i42);
        dDM = a22;
        fmVarArr[i42] = a22;
        int i43 = i42 + 1;
        C2491fm a23 = C4105zY.m41624a(SpeedBoost.class, "df6487ad4c268f57a3654ec895e5783a", i43);
        icV = a23;
        fmVarArr[i43] = a23;
        int i44 = i43 + 1;
        C2491fm a24 = C4105zY.m41624a(SpeedBoost.class, "0d46f793198129ad410214dd06289283", i44);
        icW = a24;
        fmVarArr[i44] = a24;
        int i45 = i44 + 1;
        C2491fm a25 = C4105zY.m41624a(SpeedBoost.class, "7f4827acefc730b4b19077adc91b7409", i45);
        f6557Lm = a25;
        fmVarArr[i45] = a25;
        int i46 = i45 + 1;
        C2491fm a26 = C4105zY.m41624a(SpeedBoost.class, "e6c9c28582ce0c5453be9a77d9ed2350", i46);
        aPl = a26;
        fmVarArr[i46] = a26;
        int i47 = i46 + 1;
        C2491fm a27 = C4105zY.m41624a(SpeedBoost.class, "130c1404cbb07377bfa705d3a6d5471f", i47);
        aPj = a27;
        fmVarArr[i47] = a27;
        int i48 = i47 + 1;
        C2491fm a28 = C4105zY.m41624a(SpeedBoost.class, "6c4977c2f13e01e1eefb2fb8d4b33412", i48);
        aPk = a28;
        fmVarArr[i48] = a28;
        int i49 = i48 + 1;
        C2491fm a29 = C4105zY.m41624a(SpeedBoost.class, "a92a7d9595d016297e8ef0f356be38eb", i49);
        aPm = a29;
        fmVarArr[i49] = a29;
        int i50 = i49 + 1;
        C2491fm a30 = C4105zY.m41624a(SpeedBoost.class, "74369b31dec4cf997077c0a7decc6d81", i50);
        icX = a30;
        fmVarArr[i50] = a30;
        int i51 = i50 + 1;
        C2491fm a31 = C4105zY.m41624a(SpeedBoost.class, "3a72ba54846a0ae392fb7ae1b2c4187b", i51);
        icY = a31;
        fmVarArr[i51] = a31;
        int i52 = i51 + 1;
        _m_methods = fmVarArr;
        C1634Xv.m11725a((Object[]) TaikodomObject._m_methods, (Object[]) _m_methods);
        ___iScriptClass = aUO.m18566a(SpeedBoost.class, C0190CP.class, _m_fields, _m_methods);
    }

    /* renamed from: B */
    private void m29076B(Asset tCVar) {
        throw new C6039afL();
    }

    /* renamed from: C */
    private void m29077C(Asset tCVar) {
        throw new C6039afL();
    }

    /* renamed from: Mu */
    private float m29078Mu() {
        return ((SpeedBoostType) getType()).mo20207jt();
    }

    /* renamed from: Mv */
    private Asset m29079Mv() {
        return ((SpeedBoostType) getType()).mo20204My();
    }

    /* renamed from: Mw */
    private Asset m29080Mw() {
        return ((SpeedBoostType) getType()).mo20202MA();
    }

    /* renamed from: TH */
    private void m29082TH() {
        switch (bFf().mo6893i(aPe)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, aPe, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, aPe, new Object[0]));
                break;
        }
        m29081TG();
    }

    @C0064Am(aul = "2b621d0793a4a533c2998c9e1f43e22d", aum = 0)
    @C5566aOg
    /* renamed from: TI */
    private AdapterLikedList<SpeedBoostAdapter> m29083TI() {
        throw new aWi(new aCE(this, aPf, new Object[0]));
    }

    /* renamed from: TP */
    private void m29087TP() {
        switch (bFf().mo6893i(aPl)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, aPl, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, aPl, new Object[0]));
                break;
        }
        m29086TO();
    }

    /* renamed from: Te */
    private AdapterLikedList m29089Te() {
        return (AdapterLikedList) bFf().mo5608dq().mo3214p(aOx);
    }

    /* renamed from: a */
    private void m29091a(C2286b bVar) {
        bFf().mo5608dq().mo3197f(icy, bVar);
    }

    /* renamed from: a */
    private void m29092a(AdapterLikedList zAVar) {
        bFf().mo5608dq().mo3197f(aOx, zAVar);
    }

    private float acC() {
        return bFf().mo5608dq().mo3211m(bjS);
    }

    /* renamed from: aj */
    private void m29093aj(C3892wO wOVar) {
        bFf().mo5608dq().mo3197f(icD, wOVar);
    }

    /* renamed from: ak */
    private void m29094ak(C3892wO wOVar) {
        bFf().mo5608dq().mo3197f(icE, wOVar);
    }

    /* renamed from: al */
    private void m29095al(C3892wO wOVar) {
        bFf().mo5608dq().mo3197f(icF, wOVar);
    }

    /* renamed from: am */
    private void m29096am(C3892wO wOVar) {
        bFf().mo5608dq().mo3197f(icG, wOVar);
    }

    /* renamed from: ba */
    private void m29097ba(float f) {
        throw new C6039afL();
    }

    /* renamed from: c */
    private void m29098c(Fighter pFVar) {
        bFf().mo5608dq().mo3197f(icx, pFVar);
    }

    /* renamed from: d */
    private void m29100d(C3892wO wOVar) {
        throw new C6039afL();
    }

    private int deA() {
        return bFf().mo5608dq().mo3212n(icC);
    }

    private C3892wO deB() {
        return (C3892wO) bFf().mo5608dq().mo3214p(icD);
    }

    private C3892wO deC() {
        return (C3892wO) bFf().mo5608dq().mo3214p(icE);
    }

    private C3892wO deD() {
        return (C3892wO) bFf().mo5608dq().mo3214p(icF);
    }

    private C3892wO deE() {
        return (C3892wO) bFf().mo5608dq().mo3214p(icG);
    }

    private Fighter dev() {
        return (Fighter) bFf().mo5608dq().mo3214p(icx);
    }

    private C2286b dew() {
        return (C2286b) bFf().mo5608dq().mo3214p(icy);
    }

    private float dex() {
        return bFf().mo5608dq().mo3211m(icz);
    }

    private long dey() {
        return bFf().mo5608dq().mo3213o(icA);
    }

    private long dez() {
        return bFf().mo5608dq().mo3213o(icB);
    }

    /* renamed from: dj */
    private void m29101dj(float f) {
        bFf().mo5608dq().mo3150a(bjS, f);
    }

    /* renamed from: e */
    private void m29102e(C3892wO wOVar) {
        throw new C6039afL();
    }

    /* renamed from: f */
    private void m29103f(C3892wO wOVar) {
        throw new C6039afL();
    }

    /* renamed from: g */
    private void m29104g(C3892wO wOVar) {
        throw new C6039afL();
    }

    /* renamed from: kW */
    private void m29106kW(long j) {
        bFf().mo5608dq().mo3184b(icA, j);
    }

    /* renamed from: kX */
    private void m29107kX(long j) {
        bFf().mo5608dq().mo3184b(icB, j);
    }

    /* renamed from: mI */
    private void m29108mI(float f) {
        bFf().mo5608dq().mo3150a(icz, f);
    }

    /* renamed from: yE */
    private C3892wO m29114yE() {
        return ((SpeedBoostType) getType()).mo20211yL();
    }

    /* renamed from: yF */
    private C3892wO m29115yF() {
        return ((SpeedBoostType) getType()).mo20212yN();
    }

    /* renamed from: yG */
    private C3892wO m29116yG() {
        return ((SpeedBoostType) getType()).mo20213yP();
    }

    /* renamed from: yH */
    private C3892wO m29117yH() {
        return ((SpeedBoostType) getType()).mo20214yR();
    }

    /* renamed from: yl */
    private void m29118yl(int i) {
        bFf().mo5608dq().mo3183b(icC, i);
    }

    /* renamed from: S */
    public void mo10S() {
        super.mo10S();
    }

    /* renamed from: T */
    public C6494any mo11T() {
        return ___iScriptClass;
    }

    @C5566aOg
    /* renamed from: TJ */
    public AdapterLikedList<SpeedBoostAdapter> mo5353TJ() {
        switch (bFf().mo6893i(aPf)) {
            case 0:
                return null;
            case 2:
                return (AdapterLikedList) bFf().mo5606d(new aCE(this, aPf, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, aPf, new Object[0]));
                break;
        }
        return m29083TI();
    }

    /* renamed from: TL */
    public void mo12905TL() {
        switch (bFf().mo6893i(aPj)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, aPj, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, aPj, new Object[0]));
                break;
        }
        m29084TK();
    }

    /* renamed from: TN */
    public void mo12906TN() {
        switch (bFf().mo6893i(aPk)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, aPk, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, aPk, new Object[0]));
                break;
        }
        m29085TM();
    }

    /* renamed from: TR */
    public void mo17871TR() {
        switch (bFf().mo6893i(aPm)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, aPm, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, aPm, new Object[0]));
                break;
        }
        m29088TQ();
    }

    /* renamed from: U */
    public C2491fm[] mo12U() {
        return _m_methods;
    }

    /* renamed from: W */
    public Object mo13W() {
        return new C0190CP(this);
    }

    /* renamed from: a */
    public Object mo14a(C0495Gr gr) {
        Object[] args = gr.getArgs();
        switch (gr.mo2417hq() - TaikodomObject._m_methodCount) {
            case 0:
                return new Float(acI());
            case 1:
                return deF();
            case 2:
                return deH();
            case 3:
                return deJ();
            case 4:
                return deL();
            case 5:
                return new Float(m29109mJ(((Float) args[0]).floatValue()));
            case 6:
                return new Float(m29110mL(((Float) args[0]).floatValue()));
            case 7:
                return new Float(m29111mN(((Float) args[0]).floatValue()));
            case 8:
                return new Float(m29112mP(((Float) args[0]).floatValue()));
            case 9:
                return deN();
            case 10:
                m29099d((Fighter) args[0]);
                return null;
            case 11:
                return m29083TI();
            case 12:
                m29081TG();
                return null;
            case 13:
                deP();
                return null;
            case 14:
                deR();
                return null;
            case 15:
                m29119ym(((Integer) args[0]).intValue());
                return null;
            case 16:
                return new Float(ans());
            case 17:
                return new Float(m29105js());
            case 18:
                deT();
                return null;
            case 19:
                acK();
                return null;
            case 20:
                return new Boolean(biC());
            case 21:
                return biG();
            case 22:
                return new Float(deV());
            case 23:
                return new Float(deX());
            case 24:
                m29113qU();
                return null;
            case 25:
                m29086TO();
                return null;
            case 26:
                m29084TK();
                return null;
            case 27:
                m29085TM();
                return null;
            case 28:
                m29088TQ();
                return null;
            case 29:
                deZ();
                return null;
            case 30:
                dfb();
                return null;
            default:
                return super.mo14a(gr);
        }
    }

    /* renamed from: a */
    public void mo15a(Collection collection, C0495Gr gr) {
        super.mo15a(collection, gr);
    }

    public float acJ() {
        switch (bFf().mo6893i(bjX)) {
            case 0:
                return 0.0f;
            case 2:
                return ((Float) bFf().mo5606d(new aCE(this, bjX, new Object[0]))).floatValue();
            case 3:
                bFf().mo5606d(new aCE(this, bjX, new Object[0]));
                break;
        }
        return acI();
    }

    public void activate() {
        switch (bFf().mo6893i(bka)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, bka, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, bka, new Object[0]));
                break;
        }
        acK();
    }

    public float ant() {
        switch (bFf().mo6893i(bFy)) {
            case 0:
                return 0.0f;
            case 2:
                return ((Float) bFf().mo5606d(new aCE(this, bFy, new Object[0]))).floatValue();
            case 3:
                bFf().mo5606d(new aCE(this, bFy, new Object[0]));
                break;
        }
        return ans();
    }

    public boolean biD() {
        switch (bFf().mo6893i(dDK)) {
            case 0:
                return false;
            case 2:
                return ((Boolean) bFf().mo5606d(new aCE(this, dDK, new Object[0]))).booleanValue();
            case 3:
                bFf().mo5606d(new aCE(this, dDK, new Object[0]));
                break;
        }
        return biC();
    }

    public C2286b biH() {
        switch (bFf().mo6893i(dDM)) {
            case 0:
                return null;
            case 2:
                return (C2286b) bFf().mo5606d(new aCE(this, dDM, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, dDM, new Object[0]));
                break;
        }
        return biG();
    }

    /* renamed from: c */
    public C5663aRz[] mo25c() {
        return _m_fields;
    }

    public C3892wO deG() {
        switch (bFf().mo6893i(icH)) {
            case 0:
                return null;
            case 2:
                return (C3892wO) bFf().mo5606d(new aCE(this, icH, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, icH, new Object[0]));
                break;
        }
        return deF();
    }

    public C3892wO deI() {
        switch (bFf().mo6893i(icI)) {
            case 0:
                return null;
            case 2:
                return (C3892wO) bFf().mo5606d(new aCE(this, icI, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, icI, new Object[0]));
                break;
        }
        return deH();
    }

    public C3892wO deK() {
        switch (bFf().mo6893i(icJ)) {
            case 0:
                return null;
            case 2:
                return (C3892wO) bFf().mo5606d(new aCE(this, icJ, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, icJ, new Object[0]));
                break;
        }
        return deJ();
    }

    public C3892wO deM() {
        switch (bFf().mo6893i(icK)) {
            case 0:
                return null;
            case 2:
                return (C3892wO) bFf().mo5606d(new aCE(this, icK, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, icK, new Object[0]));
                break;
        }
        return deL();
    }

    public Ship deO() {
        switch (bFf().mo6893i(icP)) {
            case 0:
                return null;
            case 2:
                return (Ship) bFf().mo5606d(new aCE(this, icP, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, icP, new Object[0]));
                break;
        }
        return deN();
    }

    @C1253SX
    public void deQ() {
        switch (bFf().mo6893i(icR)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, icR, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, icR, new Object[0]));
                break;
        }
        deP();
    }

    @C1253SX
    public void deS() {
        switch (bFf().mo6893i(icS)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, icS, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, icS, new Object[0]));
                break;
        }
        deR();
    }

    public void deU() {
        switch (bFf().mo6893i(icU)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, icU, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, icU, new Object[0]));
                break;
        }
        deT();
    }

    public float deW() {
        switch (bFf().mo6893i(icV)) {
            case 0:
                return 0.0f;
            case 2:
                return ((Float) bFf().mo5606d(new aCE(this, icV, new Object[0]))).floatValue();
            case 3:
                bFf().mo5606d(new aCE(this, icV, new Object[0]));
                break;
        }
        return deV();
    }

    public float deY() {
        switch (bFf().mo6893i(icW)) {
            case 0:
                return 0.0f;
            case 2:
                return ((Float) bFf().mo5606d(new aCE(this, icW, new Object[0]))).floatValue();
            case 3:
                bFf().mo5606d(new aCE(this, icW, new Object[0]));
                break;
        }
        return deX();
    }

    @ClientOnly
    public void dfa() {
        switch (bFf().mo6893i(icX)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, icX, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, icX, new Object[0]));
                break;
        }
        deZ();
    }

    @ClientOnly
    public void dfc() {
        switch (bFf().mo6893i(icY)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, icY, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, icY, new Object[0]));
                break;
        }
        dfb();
    }

    /* renamed from: e */
    public void mo17890e(Fighter pFVar) {
        switch (bFf().mo6893i(icQ)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, icQ, new Object[]{pFVar}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, icQ, new Object[]{pFVar}));
                break;
        }
        m29099d(pFVar);
    }

    /* renamed from: jt */
    public float mo17891jt() {
        switch (bFf().mo6893i(f6566yg)) {
            case 0:
                return 0.0f;
            case 2:
                return ((Float) bFf().mo5606d(new aCE(this, f6566yg, new Object[0]))).floatValue();
            case 3:
                bFf().mo5606d(new aCE(this, f6566yg, new Object[0]));
                break;
        }
        return m29105js();
    }

    /* renamed from: mK */
    public float mo17892mK(float f) {
        switch (bFf().mo6893i(icL)) {
            case 0:
                return 0.0f;
            case 2:
                return ((Float) bFf().mo5606d(new aCE(this, icL, new Object[]{new Float(f)}))).floatValue();
            case 3:
                bFf().mo5606d(new aCE(this, icL, new Object[]{new Float(f)}));
                break;
        }
        return m29109mJ(f);
    }

    /* renamed from: mM */
    public float mo17893mM(float f) {
        switch (bFf().mo6893i(icM)) {
            case 0:
                return 0.0f;
            case 2:
                return ((Float) bFf().mo5606d(new aCE(this, icM, new Object[]{new Float(f)}))).floatValue();
            case 3:
                bFf().mo5606d(new aCE(this, icM, new Object[]{new Float(f)}));
                break;
        }
        return m29110mL(f);
    }

    /* renamed from: mO */
    public float mo17894mO(float f) {
        switch (bFf().mo6893i(icN)) {
            case 0:
                return 0.0f;
            case 2:
                return ((Float) bFf().mo5606d(new aCE(this, icN, new Object[]{new Float(f)}))).floatValue();
            case 3:
                bFf().mo5606d(new aCE(this, icN, new Object[]{new Float(f)}));
                break;
        }
        return m29111mN(f);
    }

    /* renamed from: mQ */
    public float mo17895mQ(float f) {
        switch (bFf().mo6893i(icO)) {
            case 0:
                return 0.0f;
            case 2:
                return ((Float) bFf().mo5606d(new aCE(this, icO, new Object[]{new Float(f)}))).floatValue();
            case 3:
                bFf().mo5606d(new aCE(this, icO, new Object[]{new Float(f)}));
                break;
        }
        return m29112mP(f);
    }

    /* renamed from: qV */
    public void mo17896qV() {
        switch (bFf().mo6893i(f6557Lm)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f6557Lm, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f6557Lm, new Object[0]));
                break;
        }
        m29113qU();
    }

    /* renamed from: yn */
    public void mo17897yn(int i) {
        switch (bFf().mo6893i(icT)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, icT, new Object[]{new Integer(i)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, icT, new Object[]{new Integer(i)}));
                break;
        }
        m29119ym(i);
    }

    /* renamed from: e */
    public void mo17889e(SpeedBoostType lJVar) {
        super.mo967a((C2961mJ) lJVar);
        m29091a(C2286b.FILLING);
        m29082TH();
    }

    @C0064Am(aul = "170a5d12f624c7136a8c0ad3b644f76a", aum = 0)
    private float acI() {
        return m29078Mu();
    }

    @C0064Am(aul = "5ae873b234e9de8a4c997b20fc437772", aum = 0)
    private C3892wO deF() {
        return m29114yE();
    }

    @C0064Am(aul = "5e29ace893079f927f0e10a68e5bb562", aum = 0)
    private C3892wO deH() {
        return m29115yF();
    }

    @C0064Am(aul = "b7580c3f3d6a2a9e91ce500658c41adc", aum = 0)
    private C3892wO deJ() {
        return m29116yG();
    }

    @C0064Am(aul = "e8236174395c4d66c0810d746fdea2e6", aum = 0)
    private C3892wO deL() {
        return m29117yH();
    }

    @C0064Am(aul = "074e057d6e85d267459cddb569722381", aum = 0)
    /* renamed from: mJ */
    private float m29109mJ(float f) {
        return deD().mo22753v(deO().agH().mo4152VF(), f);
    }

    @C0064Am(aul = "1e3631bd20929eed90ef233402f72fd9", aum = 0)
    /* renamed from: mL */
    private float m29110mL(float f) {
        return deE().mo22753v(deO().agH().mo4153VH(), f);
    }

    @C0064Am(aul = "7462ac54b691f41b44dd9349e79e56a3", aum = 0)
    /* renamed from: mN */
    private float m29111mN(float f) {
        return deC().mo22753v(deO().agH().mo4218rb(), deO().mo1092rb());
    }

    @C0064Am(aul = "43f7cdca42a2a42304c39be680781315", aum = 0)
    /* renamed from: mP */
    private float m29112mP(float f) {
        return deB().mo22753v(deO().agH().mo4217ra(), f);
    }

    @C0064Am(aul = "bb34fc6ecbf021b2888d3aaad61e1052", aum = 0)
    private Ship deN() {
        return dev();
    }

    @C0064Am(aul = "2bf87a43e9913d6e734191d63930243c", aum = 0)
    /* renamed from: d */
    private void m29099d(Fighter pFVar) {
        m29098c(pFVar);
    }

    @C0064Am(aul = "6e05e61e3ac39613ffd7287c10d3009b", aum = 0)
    /* renamed from: TG */
    private void m29081TG() {
        if (m29089Te() == null) {
            AdapterLikedList zAVar = (AdapterLikedList) bFf().mo6865M(AdapterLikedList.class);
            BaseSpeedBoostAdapter aVar = (BaseSpeedBoostAdapter) bFf().mo6865M(BaseSpeedBoostAdapter.class);
            aVar.mo17898b(this);
            zAVar.mo23260c(aVar);
            m29092a(zAVar);
            m29089Te().mo23259b((C6872avM) this);
            m29087TP();
        }
    }

    @C0064Am(aul = "9ab2d65fabc3b53aa765f47b48d2b43b", aum = 0)
    @C1253SX
    private void deP() {
        if (dev().biN().aXW()) {
            dev().mo18307bv(false);
        }
        if (bGX()) {
            getPlayer().mo14419f((C1506WA) new C5873acB(this));
        }
        dev().biN().mo19335R(true);
        dev().cyN();
        dev().mo2998hb().aYm();
        m29107kX(cVr());
    }

    @C0064Am(aul = "5cfe00bd3e150aad2b42b05b29d0bb4a", aum = 0)
    @C1253SX
    private void deR() {
        dev().biN().mo19335R(false);
        dev().cyN();
        dev().mo2998hb().aYm();
    }

    @C0064Am(aul = "dcc5b63fb3203a1658ba32483b194304", aum = 0)
    /* renamed from: ym */
    private void m29119ym(int i) {
        if (i == deA() && dew() == C2286b.CONSUMING) {
            deS();
            if (((float) (cVr() - dez())) / 1000.0f >= dex()) {
                m29091a(C2286b.COOLDOWN);
                if (bGX()) {
                    getPlayer().mo14419f((C1506WA) new C5873acB(this));
                }
                ((SpeedBoost) mo8362mt(ant())).deU();
                m29108mI(0.0f);
            }
        }
    }

    @C0064Am(aul = "c09cec99ca2d7b9deb2d2a45329bf184", aum = 0)
    private float ans() {
        return mo17891jt() * 2.0f;
    }

    @C0064Am(aul = "f47f2c363d72a7ec6aa4bcad6de1a2ca", aum = 0)
    /* renamed from: js */
    private float m29105js() {
        if (bGX()) {
            return acC();
        }
        return ((SpeedBoostAdapter) m29089Te().ase()).mo9303jt();
    }

    @C0064Am(aul = "00c6b4397d0e555f0e12a1e6bafa08f7", aum = 0)
    private void deT() {
        m29091a(C2286b.FILLING);
        m29106kW(cVr());
        if (bGX()) {
            getPlayer().mo14419f((C1506WA) new C5873acB(this));
        }
    }

    @C0064Am(aul = "aa3c31f15ca5461f8ad05c62c84419e6", aum = 0)
    private void acK() {
        if (dew() == C2286b.CONSUMING) {
            deS();
            m29091a(C2286b.FILLING);
            m29106kW(cVr());
            m29108mI(dex() - (((float) (dey() - dez())) / 1000.0f));
        } else if (dew() == C2286b.FILLING) {
            m29108mI(Math.min(mo17891jt(), (((float) (cVr() - dey())) / 1000.0f) + dex()));
            int i = this.hFe;
            this.hFe = i + 1;
            m29118yl(i);
            ((SpeedBoost) mo8362mt(dex())).mo17897yn(deA());
            m29091a(C2286b.CONSUMING);
            deQ();
        }
        if (bGX() && dew() != C2286b.COOLDOWN) {
            getPlayer().mo14419f((C1506WA) new C5873acB(this));
        }
    }

    @C0064Am(aul = "f6d3df5db2f2ff766951e978ad3a5d73", aum = 0)
    private boolean biC() {
        return Math.abs(mo17891jt() - Math.min(mo17891jt(), ((float) (cVr() - dey())) / 1000.0f)) < 1.0E-4f;
    }

    @C0064Am(aul = "59503241f56d12962c337ca88357d50c", aum = 0)
    private C2286b biG() {
        return dew();
    }

    @C0064Am(aul = "df6487ad4c268f57a3654ec895e5783a", aum = 0)
    private float deV() {
        long cVr = cVr();
        if (dew() != C2286b.CONSUMING) {
            return Math.min(mo17891jt(), (((float) (cVr - dey())) / 1000.0f) + dex());
        }
        return Math.max(0.0f, dex() - (((float) (cVr - dez())) / 1000.0f));
    }

    @C0064Am(aul = "0d46f793198129ad410214dd06289283", aum = 0)
    private float deX() {
        return deW() / mo17891jt();
    }

    @C0064Am(aul = "7f4827acefc730b4b19077adc91b7409", aum = 0)
    /* renamed from: qU */
    private void m29113qU() {
        push();
    }

    @C0064Am(aul = "e6c9c28582ce0c5453be9a77d9ed2350", aum = 0)
    /* renamed from: TO */
    private void m29086TO() {
        m29101dj(((SpeedBoostAdapter) m29089Te().ase()).mo9303jt());
        m29093aj(((SpeedBoostAdapter) m29089Te().ase()).mo9302jr());
        m29094ak(((SpeedBoostAdapter) m29089Te().ase()).mo9301jp());
        m29095al(((SpeedBoostAdapter) m29089Te().ase()).mo9299jl());
        m29096am(((SpeedBoostAdapter) m29089Te().ase()).mo9300jn());
    }

    @C0064Am(aul = "130c1404cbb07377bfa705d3a6d5471f", aum = 0)
    /* renamed from: TK */
    private void m29084TK() {
        m29087TP();
    }

    @C0064Am(aul = "6c4977c2f13e01e1eefb2fb8d4b33412", aum = 0)
    /* renamed from: TM */
    private void m29085TM() {
        m29087TP();
    }

    @C0064Am(aul = "a92a7d9595d016297e8ef0f356be38eb", aum = 0)
    /* renamed from: TQ */
    private void m29088TQ() {
        if (m29089Te() == null) {
            mo8358lY(String.valueOf(bFY()) + " with NULL adapterList");
            return;
        }
        m29089Te().mo23259b((C6872avM) this);
        m29087TP();
    }

    @C0064Am(aul = "74369b31dec4cf997077c0a7decc6d81", aum = 0)
    @ClientOnly
    private void deZ() {
        if (bGX() && dev() != null) {
            dev().mo18261aa(m29079Mv());
        }
    }

    @C0064Am(aul = "3a72ba54846a0ae392fb7ae1b2c4187b", aum = 0)
    @ClientOnly
    private void dfb() {
        if (bGX() && dev() != null) {
            dev().mo18261aa(m29080Mw());
        }
    }

    /* renamed from: a.dc$b */
    /* compiled from: a */
    public enum C2286b {
        FILLING,
        COOLDOWN,
        READY,
        CONSUMING
    }

    @C6485anp
    @C5511aMd
    /* renamed from: a.dc$a */
    public class BaseSpeedBoostAdapter extends SpeedBoostAdapter implements C1616Xf {
        public static final int _m_fieldCount = 0;
        public static final C5663aRz[] _m_fields = null;
        public static final int _m_methodCount = 0;
        public static final C2491fm[] _m_methods = null;
        /* renamed from: aT */
        public static final C5663aRz f6567aT = null;
        public static final long serialVersionUID = 0;
        /* renamed from: yc */
        public static final C2491fm f6569yc = null;
        /* renamed from: yd */
        public static final C2491fm f6570yd = null;
        /* renamed from: ye */
        public static final C2491fm f6571ye = null;
        /* renamed from: yf */
        public static final C2491fm f6572yf = null;
        /* renamed from: yg */
        public static final C2491fm f6573yg = null;
        public static C6494any ___iScriptClass;
        @C0064Am(aul = "2d18fef72437df79b036cf299af2c1f4", aum = 0)

        /* renamed from: yb */
        static /* synthetic */ SpeedBoost f6568yb;

        static {
            m29140V();
        }

        public BaseSpeedBoostAdapter() {
            super((C5540aNg) null);
            super.mo10S();
        }

        public BaseSpeedBoostAdapter(C5540aNg ang) {
            super(ang);
        }

        public BaseSpeedBoostAdapter(SpeedBoost dcVar) {
            super((C5540aNg) null);
            super._m_script_init(dcVar);
        }

        /* renamed from: V */
        static void m29140V() {
            serialVersionUID = (long) 1;
            _m_fieldCount = SpeedBoostAdapter._m_fieldCount + 1;
            _m_methodCount = SpeedBoostAdapter._m_methodCount + 5;
            int i = SpeedBoostAdapter._m_fieldCount;
            C5663aRz[] arzArr = new C5663aRz[(i + 1)];
            C5663aRz b = C5640aRc.m17844b(BaseSpeedBoostAdapter.class, "2d18fef72437df79b036cf299af2c1f4", i);
            f6567aT = b;
            arzArr[i] = b;
            int i2 = i + 1;
            _m_fields = arzArr;
            C1634Xv.m11725a((Object[]) SpeedBoostAdapter._m_fields, (Object[]) _m_fields);
            int i3 = SpeedBoostAdapter._m_methodCount;
            C2491fm[] fmVarArr = new C2491fm[(i3 + 5)];
            C2491fm a = C4105zY.m41624a(BaseSpeedBoostAdapter.class, "d46f42ef597b36e4fa34a7a2ced480e4", i3);
            f6569yc = a;
            fmVarArr[i3] = a;
            int i4 = i3 + 1;
            C2491fm a2 = C4105zY.m41624a(BaseSpeedBoostAdapter.class, "721ed93edf870d9589f3fa90dc44d9f1", i4);
            f6570yd = a2;
            fmVarArr[i4] = a2;
            int i5 = i4 + 1;
            C2491fm a3 = C4105zY.m41624a(BaseSpeedBoostAdapter.class, "a21bcbef2bd6997b48b2f9d620abebd6", i5);
            f6571ye = a3;
            fmVarArr[i5] = a3;
            int i6 = i5 + 1;
            C2491fm a4 = C4105zY.m41624a(BaseSpeedBoostAdapter.class, "25716157a59ee121e9ed75812a9fcfc7", i6);
            f6572yf = a4;
            fmVarArr[i6] = a4;
            int i7 = i6 + 1;
            C2491fm a5 = C4105zY.m41624a(BaseSpeedBoostAdapter.class, "02454b1a69389430aaa8d57335f19f3f", i7);
            f6573yg = a5;
            fmVarArr[i7] = a5;
            int i8 = i7 + 1;
            _m_methods = fmVarArr;
            C1634Xv.m11725a((Object[]) SpeedBoostAdapter._m_methods, (Object[]) _m_methods);
            ___iScriptClass = aUO.m18566a(BaseSpeedBoostAdapter.class, C2595hK.class, _m_fields, _m_methods);
        }

        /* renamed from: a */
        private void m29141a(SpeedBoost dcVar) {
            bFf().mo5608dq().mo3197f(f6567aT, dcVar);
        }

        /* renamed from: jj */
        private SpeedBoost m29142jj() {
            return (SpeedBoost) bFf().mo5608dq().mo3214p(f6567aT);
        }

        /* renamed from: S */
        public void mo10S() {
            super.mo10S();
        }

        /* renamed from: T */
        public C6494any mo11T() {
            return ___iScriptClass;
        }

        /* renamed from: U */
        public C2491fm[] mo12U() {
            return _m_methods;
        }

        /* renamed from: W */
        public Object mo13W() {
            return new C2595hK(this);
        }

        /* renamed from: a */
        public Object mo14a(C0495Gr gr) {
            gr.getArgs();
            switch (gr.mo2417hq() - SpeedBoostAdapter._m_methodCount) {
                case 0:
                    return m29143jk();
                case 1:
                    return m29144jm();
                case 2:
                    return m29145jo();
                case 3:
                    return m29146jq();
                case 4:
                    return new Float(m29147js());
                default:
                    return super.mo14a(gr);
            }
        }

        /* renamed from: a */
        public void mo15a(Collection collection, C0495Gr gr) {
            super.mo15a(collection, gr);
        }

        /* renamed from: c */
        public C5663aRz[] mo25c() {
            return _m_fields;
        }

        /* renamed from: jl */
        public C3892wO mo9299jl() {
            switch (bFf().mo6893i(f6569yc)) {
                case 0:
                    return null;
                case 2:
                    return (C3892wO) bFf().mo5606d(new aCE(this, f6569yc, new Object[0]));
                case 3:
                    bFf().mo5606d(new aCE(this, f6569yc, new Object[0]));
                    break;
            }
            return m29143jk();
        }

        /* renamed from: jn */
        public C3892wO mo9300jn() {
            switch (bFf().mo6893i(f6570yd)) {
                case 0:
                    return null;
                case 2:
                    return (C3892wO) bFf().mo5606d(new aCE(this, f6570yd, new Object[0]));
                case 3:
                    bFf().mo5606d(new aCE(this, f6570yd, new Object[0]));
                    break;
            }
            return m29144jm();
        }

        /* renamed from: jp */
        public C3892wO mo9301jp() {
            switch (bFf().mo6893i(f6571ye)) {
                case 0:
                    return null;
                case 2:
                    return (C3892wO) bFf().mo5606d(new aCE(this, f6571ye, new Object[0]));
                case 3:
                    bFf().mo5606d(new aCE(this, f6571ye, new Object[0]));
                    break;
            }
            return m29145jo();
        }

        /* renamed from: jr */
        public C3892wO mo9302jr() {
            switch (bFf().mo6893i(f6572yf)) {
                case 0:
                    return null;
                case 2:
                    return (C3892wO) bFf().mo5606d(new aCE(this, f6572yf, new Object[0]));
                case 3:
                    bFf().mo5606d(new aCE(this, f6572yf, new Object[0]));
                    break;
            }
            return m29146jq();
        }

        /* renamed from: jt */
        public float mo9303jt() {
            switch (bFf().mo6893i(f6573yg)) {
                case 0:
                    return 0.0f;
                case 2:
                    return ((Float) bFf().mo5606d(new aCE(this, f6573yg, new Object[0]))).floatValue();
                case 3:
                    bFf().mo5606d(new aCE(this, f6573yg, new Object[0]));
                    break;
            }
            return m29147js();
        }

        /* renamed from: b */
        public void mo17898b(SpeedBoost dcVar) {
            m29141a(dcVar);
            super.mo10S();
        }

        @C0064Am(aul = "d46f42ef597b36e4fa34a7a2ced480e4", aum = 0)
        /* renamed from: jk */
        private C3892wO m29143jk() {
            return m29142jj().deM();
        }

        @C0064Am(aul = "721ed93edf870d9589f3fa90dc44d9f1", aum = 0)
        /* renamed from: jm */
        private C3892wO m29144jm() {
            return m29142jj().deK();
        }

        @C0064Am(aul = "a21bcbef2bd6997b48b2f9d620abebd6", aum = 0)
        /* renamed from: jo */
        private C3892wO m29145jo() {
            return m29142jj().deI();
        }

        @C0064Am(aul = "25716157a59ee121e9ed75812a9fcfc7", aum = 0)
        /* renamed from: jq */
        private C3892wO m29146jq() {
            return m29142jj().deG();
        }

        @C0064Am(aul = "02454b1a69389430aaa8d57335f19f3f", aum = 0)
        /* renamed from: js */
        private float m29147js() {
            return m29142jj().acJ();
        }
    }
}
