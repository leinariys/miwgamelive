package game.script.ship;

import game.network.message.externalizable.aCE;
import game.script.TaikodomObject;
import game.script.ship.hazardshield.HazardShieldType;
import game.script.ship.speedBoost.SpeedBoostType;
import game.script.ship.strike.StrikeType;
import logic.baa.*;
import logic.data.mbean.C4064yn;
import logic.res.code.C5640aRc;
import logic.res.code.C5663aRz;
import logic.res.code.C6494any;
import logic.res.html.C2491fm;
import p001a.*;

import java.util.Collection;
import java.util.UUID;

@C5511aMd
@C6485anp
/* renamed from: a.aDv  reason: case insensitive filesystem */
/* compiled from: a */
public class ShipDefaults extends TaikodomObject implements C0468GU, C1616Xf {
    public static final int _m_fieldCount = 0;
    public static final C5663aRz[] _m_fields = null;
    public static final int _m_methodCount = 0;
    public static final C2491fm[] _m_methods = null;
    /* renamed from: bL */
    public static final C5663aRz f2600bL = null;
    /* renamed from: bM */
    public static final C5663aRz f2601bM = null;
    /* renamed from: bN */
    public static final C2491fm f2602bN = null;
    /* renamed from: bO */
    public static final C2491fm f2603bO = null;
    /* renamed from: bP */
    public static final C2491fm f2604bP = null;
    /* renamed from: bQ */
    public static final C2491fm f2605bQ = null;
    public static final C5663aRz hBC = null;
    public static final C5663aRz hBD = null;
    public static final C5663aRz hBE = null;
    public static final C5663aRz hBF = null;
    public static final C2491fm hBG = null;
    public static final C2491fm hBH = null;
    public static final C2491fm hBI = null;
    public static final C2491fm hBJ = null;
    public static final C2491fm hBK = null;
    public static final C2491fm hBL = null;
    public static final C2491fm hBM = null;
    public static final C2491fm hBN = null;
    public static final long serialVersionUID = 0;
    public static C6494any ___iScriptClass;
    @C0064Am(aul = "c4249039e11d803538bbed7793a31867", aum = 4)

    /* renamed from: bK */
    private static UUID f2599bK;
    @C0064Am(aul = "cf5ddaaab2633ef2d536bdf6ff626b1d", aum = 0)
    private static StrikeType bKc;
    @C0064Am(aul = "a58dbe1e49779ed5aeb2672df2ebba4a", aum = 1)
    private static HazardShieldType bKd;
    @C0064Am(aul = "54517cd593e908ae64e5c7792f69ce05", aum = 2)
    private static SpeedBoostType bKe;
    @C0064Am(aul = "327cb8f6806de9810688affc24a49bc8", aum = 3)
    private static CargoHoldType bKf;
    @C0064Am(aul = "afa653c86257c105a5f2afc419486782", aum = 5)
    private static String handle;

    static {
        m13893V();
    }

    public ShipDefaults() {
        super((C5540aNg) null);
        super.mo10S();
    }

    public ShipDefaults(C5540aNg ang) {
        super(ang);
    }

    /* renamed from: V */
    static void m13893V() {
        serialVersionUID = (long) 1;
        _m_fieldCount = TaikodomObject._m_fieldCount + 6;
        _m_methodCount = TaikodomObject._m_methodCount + 12;
        int i = TaikodomObject._m_fieldCount;
        C5663aRz[] arzArr = new C5663aRz[(i + 6)];
        C5663aRz b = C5640aRc.m17844b(ShipDefaults.class, "cf5ddaaab2633ef2d536bdf6ff626b1d", i);
        hBC = b;
        arzArr[i] = b;
        int i2 = i + 1;
        C5663aRz b2 = C5640aRc.m17844b(ShipDefaults.class, "a58dbe1e49779ed5aeb2672df2ebba4a", i2);
        hBD = b2;
        arzArr[i2] = b2;
        int i3 = i2 + 1;
        C5663aRz b3 = C5640aRc.m17844b(ShipDefaults.class, "54517cd593e908ae64e5c7792f69ce05", i3);
        hBE = b3;
        arzArr[i3] = b3;
        int i4 = i3 + 1;
        C5663aRz b4 = C5640aRc.m17844b(ShipDefaults.class, "327cb8f6806de9810688affc24a49bc8", i4);
        hBF = b4;
        arzArr[i4] = b4;
        int i5 = i4 + 1;
        C5663aRz b5 = C5640aRc.m17844b(ShipDefaults.class, "c4249039e11d803538bbed7793a31867", i5);
        f2600bL = b5;
        arzArr[i5] = b5;
        int i6 = i5 + 1;
        C5663aRz b6 = C5640aRc.m17844b(ShipDefaults.class, "afa653c86257c105a5f2afc419486782", i6);
        f2601bM = b6;
        arzArr[i6] = b6;
        int i7 = i6 + 1;
        _m_fields = arzArr;
        C1634Xv.m11725a((Object[]) TaikodomObject._m_fields, (Object[]) _m_fields);
        int i8 = TaikodomObject._m_methodCount;
        C2491fm[] fmVarArr = new C2491fm[(i8 + 12)];
        C2491fm a = C4105zY.m41624a(ShipDefaults.class, "91e1994b18182dfc80f7e8a03156538d", i8);
        f2602bN = a;
        fmVarArr[i8] = a;
        int i9 = i8 + 1;
        C2491fm a2 = C4105zY.m41624a(ShipDefaults.class, "ef994d463c6a7c9b2cb6a36c2c8a39fa", i9);
        f2603bO = a2;
        fmVarArr[i9] = a2;
        int i10 = i9 + 1;
        C2491fm a3 = C4105zY.m41624a(ShipDefaults.class, "fdb06ac3eb206603edcc4da984d22036", i10);
        f2604bP = a3;
        fmVarArr[i10] = a3;
        int i11 = i10 + 1;
        C2491fm a4 = C4105zY.m41624a(ShipDefaults.class, "4d04a40e0cc128105f1d0ac1e9554c9e", i11);
        f2605bQ = a4;
        fmVarArr[i11] = a4;
        int i12 = i11 + 1;
        C2491fm a5 = C4105zY.m41624a(ShipDefaults.class, "ce2a45dd359d577afa78b599b43dff7f", i12);
        hBG = a5;
        fmVarArr[i12] = a5;
        int i13 = i12 + 1;
        C2491fm a6 = C4105zY.m41624a(ShipDefaults.class, "b5c8fd5f484a349ad83744af89ed4ebd", i13);
        hBH = a6;
        fmVarArr[i13] = a6;
        int i14 = i13 + 1;
        C2491fm a7 = C4105zY.m41624a(ShipDefaults.class, "89a4ae64e6998add75cc5f69c87a4cb0", i14);
        hBI = a7;
        fmVarArr[i14] = a7;
        int i15 = i14 + 1;
        C2491fm a8 = C4105zY.m41624a(ShipDefaults.class, "4df0289aded6743a1ef9f51bb6d97aa0", i15);
        hBJ = a8;
        fmVarArr[i15] = a8;
        int i16 = i15 + 1;
        C2491fm a9 = C4105zY.m41624a(ShipDefaults.class, "ec8462de025620dc574426a133d2416f", i16);
        hBK = a9;
        fmVarArr[i16] = a9;
        int i17 = i16 + 1;
        C2491fm a10 = C4105zY.m41624a(ShipDefaults.class, "075d46fb2f752b8a9339bed459eb1a6c", i17);
        hBL = a10;
        fmVarArr[i17] = a10;
        int i18 = i17 + 1;
        C2491fm a11 = C4105zY.m41624a(ShipDefaults.class, "35e51e0c48381bfd2ec321b7f6a31537", i18);
        hBM = a11;
        fmVarArr[i18] = a11;
        int i19 = i18 + 1;
        C2491fm a12 = C4105zY.m41624a(ShipDefaults.class, "07c13dfbcbc8cb4381a96030add3be3f", i19);
        hBN = a12;
        fmVarArr[i19] = a12;
        int i20 = i19 + 1;
        _m_methods = fmVarArr;
        C1634Xv.m11725a((Object[]) TaikodomObject._m_methods, (Object[]) _m_methods);
        ___iScriptClass = aUO.m18566a(ShipDefaults.class, C4064yn.class, _m_fields, _m_methods);
    }

    /* renamed from: a */
    private void m13894a(String str) {
        bFf().mo5608dq().mo3197f(f2601bM, str);
    }

    /* renamed from: a */
    private void m13895a(UUID uuid) {
        bFf().mo5608dq().mo3197f(f2600bL, uuid);
    }

    /* renamed from: an */
    private UUID m13896an() {
        return (UUID) bFf().mo5608dq().mo3214p(f2600bL);
    }

    /* renamed from: ao */
    private String m13897ao() {
        return (String) bFf().mo5608dq().mo3214p(f2601bM);
    }

    /* renamed from: b */
    private void m13900b(SpeedBoostType lJVar) {
        bFf().mo5608dq().mo3197f(hBE, lJVar);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Handle")
    @C0064Am(aul = "4d04a40e0cc128105f1d0ac1e9554c9e", aum = 0)
    @C5566aOg
    /* renamed from: b */
    private void m13901b(String str) {
        throw new aWi(new aCE(this, f2605bQ, new Object[]{str}));
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Default Speed Boost")
    @C0064Am(aul = "075d46fb2f752b8a9339bed459eb1a6c", aum = 0)
    @C5566aOg
    /* renamed from: c */
    private void m13903c(SpeedBoostType lJVar) {
        throw new aWi(new aCE(this, hBL, new Object[]{lJVar}));
    }

    /* renamed from: c */
    private void m13904c(UUID uuid) {
        switch (bFf().mo6893i(f2603bO)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f2603bO, new Object[]{uuid}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f2603bO, new Object[]{uuid}));
                break;
        }
        m13902b(uuid);
    }

    private StrikeType cUS() {
        return (StrikeType) bFf().mo5608dq().mo3214p(hBC);
    }

    private HazardShieldType cUT() {
        return (HazardShieldType) bFf().mo5608dq().mo3214p(hBD);
    }

    private SpeedBoostType cUU() {
        return (SpeedBoostType) bFf().mo5608dq().mo3214p(hBE);
    }

    private CargoHoldType cUV() {
        return (CargoHoldType) bFf().mo5608dq().mo3214p(hBF);
    }

    /* renamed from: e */
    private void m13905e(StrikeType add) {
        bFf().mo5608dq().mo3197f(hBC, add);
    }

    /* renamed from: e */
    private void m13906e(HazardShieldType rGVar) {
        bFf().mo5608dq().mo3197f(hBD, rGVar);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Default Bomber Strike")
    @C0064Am(aul = "b5c8fd5f484a349ad83744af89ed4ebd", aum = 0)
    @C5566aOg
    /* renamed from: f */
    private void m13907f(StrikeType add) {
        throw new aWi(new aCE(this, hBH, new Object[]{add}));
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Default Hazard Shield")
    @C0064Am(aul = "4df0289aded6743a1ef9f51bb6d97aa0", aum = 0)
    @C5566aOg
    /* renamed from: f */
    private void m13908f(HazardShieldType rGVar) {
        throw new aWi(new aCE(this, hBJ, new Object[]{rGVar}));
    }

    /* renamed from: l */
    private void m13909l(CargoHoldType arb) {
        bFf().mo5608dq().mo3197f(hBF, arb);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Default Vault")
    @C0064Am(aul = "07c13dfbcbc8cb4381a96030add3be3f", aum = 0)
    @C5566aOg
    /* renamed from: m */
    private void m13910m(CargoHoldType arb) {
        throw new aWi(new aCE(this, hBN, new Object[]{arb}));
    }

    /* renamed from: T */
    public C6494any mo11T() {
        return ___iScriptClass;
    }

    /* renamed from: U */
    public C2491fm[] mo12U() {
        return _m_methods;
    }

    /* renamed from: W */
    public Object mo13W() {
        return new C4064yn(this);
    }

    /* renamed from: a */
    public Object mo14a(C0495Gr gr) {
        Object[] args = gr.getArgs();
        switch (gr.mo2417hq() - TaikodomObject._m_methodCount) {
            case 0:
                return m13898ap();
            case 1:
                m13902b((UUID) args[0]);
                return null;
            case 2:
                return m13899ar();
            case 3:
                m13901b((String) args[0]);
                return null;
            case 4:
                return cUW();
            case 5:
                m13907f((StrikeType) args[0]);
                return null;
            case 6:
                return cUY();
            case 7:
                m13908f((HazardShieldType) args[0]);
                return null;
            case 8:
                return cVa();
            case 9:
                m13903c((SpeedBoostType) args[0]);
                return null;
            case 10:
                return cVc();
            case 11:
                m13910m((CargoHoldType) args[0]);
                return null;
            default:
                return super.mo14a(gr);
        }
    }

    /* renamed from: a */
    public void mo15a(Collection collection, C0495Gr gr) {
        super.mo15a(collection, gr);
    }

    /* renamed from: aq */
    public UUID mo18aq() {
        switch (bFf().mo6893i(f2602bN)) {
            case 0:
                return null;
            case 2:
                return (UUID) bFf().mo5606d(new aCE(this, f2602bN, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f2602bN, new Object[0]));
                break;
        }
        return m13898ap();
    }

    /* renamed from: c */
    public C5663aRz[] mo25c() {
        return _m_fields;
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Default Bomber Strike")
    public StrikeType cUX() {
        switch (bFf().mo6893i(hBG)) {
            case 0:
                return null;
            case 2:
                return (StrikeType) bFf().mo5606d(new aCE(this, hBG, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, hBG, new Object[0]));
                break;
        }
        return cUW();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Default Hazard Shield")
    public HazardShieldType cUZ() {
        switch (bFf().mo6893i(hBI)) {
            case 0:
                return null;
            case 2:
                return (HazardShieldType) bFf().mo5606d(new aCE(this, hBI, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, hBI, new Object[0]));
                break;
        }
        return cUY();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Default Speed Boost")
    public SpeedBoostType cVb() {
        switch (bFf().mo6893i(hBK)) {
            case 0:
                return null;
            case 2:
                return (SpeedBoostType) bFf().mo5606d(new aCE(this, hBK, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, hBK, new Object[0]));
                break;
        }
        return cVa();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Default Vault")
    public CargoHoldType cVd() {
        switch (bFf().mo6893i(hBM)) {
            case 0:
                return null;
            case 2:
                return (CargoHoldType) bFf().mo5606d(new aCE(this, hBM, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, hBM, new Object[0]));
                break;
        }
        return cVc();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Default Speed Boost")
    @C5566aOg
    /* renamed from: d */
    public void mo8524d(SpeedBoostType lJVar) {
        switch (bFf().mo6893i(hBL)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, hBL, new Object[]{lJVar}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, hBL, new Object[]{lJVar}));
                break;
        }
        m13903c(lJVar);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Default Bomber Strike")
    @C5566aOg
    /* renamed from: g */
    public void mo8525g(StrikeType add) {
        switch (bFf().mo6893i(hBH)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, hBH, new Object[]{add}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, hBH, new Object[]{add}));
                break;
        }
        m13907f(add);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Default Hazard Shield")
    @C5566aOg
    /* renamed from: g */
    public void mo8526g(HazardShieldType rGVar) {
        switch (bFf().mo6893i(hBJ)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, hBJ, new Object[]{rGVar}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, hBJ, new Object[]{rGVar}));
                break;
        }
        m13908f(rGVar);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Handle")
    public String getHandle() {
        switch (bFf().mo6893i(f2604bP)) {
            case 0:
                return null;
            case 2:
                return (String) bFf().mo5606d(new aCE(this, f2604bP, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f2604bP, new Object[0]));
                break;
        }
        return m13899ar();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Handle")
    @C5566aOg
    public void setHandle(String str) {
        switch (bFf().mo6893i(f2605bQ)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f2605bQ, new Object[]{str}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f2605bQ, new Object[]{str}));
                break;
        }
        m13901b(str);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Default Vault")
    @C5566aOg
    /* renamed from: n */
    public void mo8527n(CargoHoldType arb) {
        switch (bFf().mo6893i(hBN)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, hBN, new Object[]{arb}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, hBN, new Object[]{arb}));
                break;
        }
        m13910m(arb);
    }

    @C0064Am(aul = "91e1994b18182dfc80f7e8a03156538d", aum = 0)
    /* renamed from: ap */
    private UUID m13898ap() {
        return m13896an();
    }

    @C0064Am(aul = "ef994d463c6a7c9b2cb6a36c2c8a39fa", aum = 0)
    /* renamed from: b */
    private void m13902b(UUID uuid) {
        m13895a(uuid);
    }

    /* renamed from: S */
    public void mo10S() {
        super.mo10S();
        StrikeType add = (StrikeType) bFf().mo6865M(StrikeType.class);
        add.mo10S();
        m13905e(add);
        HazardShieldType rGVar = (HazardShieldType) bFf().mo6865M(HazardShieldType.class);
        rGVar.mo10S();
        m13906e(rGVar);
        SpeedBoostType lJVar = (SpeedBoostType) bFf().mo6865M(SpeedBoostType.class);
        lJVar.mo10S();
        m13900b(lJVar);
        CargoHoldType arb = (CargoHoldType) bFf().mo6865M(CargoHoldType.class);
        arb.mo10S();
        m13909l(arb);
        m13895a(UUID.randomUUID());
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Handle")
    @C0064Am(aul = "fdb06ac3eb206603edcc4da984d22036", aum = 0)
    /* renamed from: ar */
    private String m13899ar() {
        return m13897ao();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Default Bomber Strike")
    @C0064Am(aul = "ce2a45dd359d577afa78b599b43dff7f", aum = 0)
    private StrikeType cUW() {
        return cUS();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Default Hazard Shield")
    @C0064Am(aul = "89a4ae64e6998add75cc5f69c87a4cb0", aum = 0)
    private HazardShieldType cUY() {
        return cUT();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Default Speed Boost")
    @C0064Am(aul = "ec8462de025620dc574426a133d2416f", aum = 0)
    private SpeedBoostType cVa() {
        return cUU();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Default Vault")
    @C0064Am(aul = "35e51e0c48381bfd2ec321b7f6a31537", aum = 0)
    private CargoHoldType cVc() {
        return cUV();
    }
}
