package game.script.ship;

import game.network.message.externalizable.aCE;
import game.script.TaikodomObject;
import game.script.resource.Asset;
import logic.baa.*;
import logic.data.mbean.aJH;
import logic.res.code.C5640aRc;
import logic.res.code.C5663aRz;
import logic.res.code.C6494any;
import logic.res.html.C2491fm;
import p001a.*;
import taikodom.infra.script.I18NString;

import java.util.Collection;
import java.util.UUID;

@C5511aMd
@C6485anp
/* renamed from: a.fM */
/* compiled from: a */
public class SectorCategory extends TaikodomObject implements C0468GU, C1616Xf {
    /* renamed from: NW */
    public static final C5663aRz f7327NW = null;
    /* renamed from: NY */
    public static final C5663aRz f7329NY = null;
    /* renamed from: NZ */
    public static final C2491fm f7330NZ = null;
    /* renamed from: Oa */
    public static final C2491fm f7331Oa = null;
    /* renamed from: Ob */
    public static final C2491fm f7332Ob = null;
    /* renamed from: Oc */
    public static final C2491fm f7333Oc = null;
    /* renamed from: Od */
    public static final C2491fm f7334Od = null;
    /* renamed from: Oe */
    public static final C2491fm f7335Oe = null;
    public static final C2491fm _f_toString_0020_0028_0029Ljava_002flang_002fString_003b = null;
    public static final C5663aRz _f_type = null;
    public static final int _m_fieldCount = 0;
    public static final C5663aRz[] _m_fields = null;
    public static final int _m_methodCount = 0;
    public static final C2491fm[] _m_methods = null;
    /* renamed from: bL */
    public static final C5663aRz f7337bL = null;
    /* renamed from: bM */
    public static final C5663aRz f7338bM = null;
    /* renamed from: bN */
    public static final C2491fm f7339bN = null;
    /* renamed from: bO */
    public static final C2491fm f7340bO = null;
    /* renamed from: bP */
    public static final C2491fm f7341bP = null;
    /* renamed from: bQ */
    public static final C2491fm f7342bQ = null;
    public static final long serialVersionUID = 0;
    /* renamed from: zQ */
    public static final C5663aRz f7345zQ = null;
    /* renamed from: zT */
    public static final C2491fm f7346zT = null;
    /* renamed from: zU */
    public static final C2491fm f7347zU = null;
    public static C6494any ___iScriptClass;
    @C0064Am(aul = "bc7aab9aae9ef176f2d53ee9ca63df8b", aum = 2)

    /* renamed from: NV */
    private static short f7326NV;
    @C0064Am(aul = "bc9969ba96df70dea505366862ec21ca", aum = 3)

    /* renamed from: NX */
    private static C3904wY f7328NX;
    @C0064Am(aul = "8fb81d109f82c6d72552ce095f73aa3c", aum = 5)

    /* renamed from: bK */
    private static UUID f7336bK;
    @C0064Am(aul = "5c9b14baf8aa1abf6bb169823016f72d", aum = 0)
    private static String handle;
    @C0064Am(aul = "7bdadfd27204534d01e9607002d1ed03", aum = 4)

    /* renamed from: jn */
    private static Asset f7343jn;
    @C0064Am(aul = "bb0306c596366d8bb38de2806e604268", aum = 1)

    /* renamed from: zP */
    private static I18NString f7344zP;

    static {
        m30790V();
    }

    public SectorCategory() {
        super((C5540aNg) null);
        super.mo10S();
    }

    public SectorCategory(C5540aNg ang) {
        super(ang);
    }

    /* renamed from: V */
    static void m30790V() {
        serialVersionUID = (long) 1;
        _m_fieldCount = TaikodomObject._m_fieldCount + 6;
        _m_methodCount = TaikodomObject._m_methodCount + 13;
        int i = TaikodomObject._m_fieldCount;
        C5663aRz[] arzArr = new C5663aRz[(i + 6)];
        C5663aRz b = C5640aRc.m17844b(SectorCategory.class, "5c9b14baf8aa1abf6bb169823016f72d", i);
        f7338bM = b;
        arzArr[i] = b;
        int i2 = i + 1;
        C5663aRz b2 = C5640aRc.m17844b(SectorCategory.class, "bb0306c596366d8bb38de2806e604268", i2);
        f7345zQ = b2;
        arzArr[i2] = b2;
        int i3 = i2 + 1;
        C5663aRz b3 = C5640aRc.m17844b(SectorCategory.class, "bc7aab9aae9ef176f2d53ee9ca63df8b", i3);
        f7327NW = b3;
        arzArr[i3] = b3;
        int i4 = i3 + 1;
        C5663aRz b4 = C5640aRc.m17844b(SectorCategory.class, "bc9969ba96df70dea505366862ec21ca", i4);
        _f_type = b4;
        arzArr[i4] = b4;
        int i5 = i4 + 1;
        C5663aRz b5 = C5640aRc.m17844b(SectorCategory.class, "7bdadfd27204534d01e9607002d1ed03", i5);
        f7329NY = b5;
        arzArr[i5] = b5;
        int i6 = i5 + 1;
        C5663aRz b6 = C5640aRc.m17844b(SectorCategory.class, "8fb81d109f82c6d72552ce095f73aa3c", i6);
        f7337bL = b6;
        arzArr[i6] = b6;
        int i7 = i6 + 1;
        _m_fields = arzArr;
        C1634Xv.m11725a((Object[]) TaikodomObject._m_fields, (Object[]) _m_fields);
        int i8 = TaikodomObject._m_methodCount;
        C2491fm[] fmVarArr = new C2491fm[(i8 + 13)];
        C2491fm a = C4105zY.m41624a(SectorCategory.class, "6536cf0a9829e102a91d340db006808c", i8);
        f7339bN = a;
        fmVarArr[i8] = a;
        int i9 = i8 + 1;
        C2491fm a2 = C4105zY.m41624a(SectorCategory.class, "bd07cb39025989a96daa0a400ec374b7", i9);
        f7340bO = a2;
        fmVarArr[i9] = a2;
        int i10 = i9 + 1;
        C2491fm a3 = C4105zY.m41624a(SectorCategory.class, "248249a806762cd86514f977c468fff1", i10);
        f7341bP = a3;
        fmVarArr[i10] = a3;
        int i11 = i10 + 1;
        C2491fm a4 = C4105zY.m41624a(SectorCategory.class, "273f6fd415f9314ecae6e7dc843c43cc", i11);
        f7342bQ = a4;
        fmVarArr[i11] = a4;
        int i12 = i11 + 1;
        C2491fm a5 = C4105zY.m41624a(SectorCategory.class, "fbbeafc3c101db6eb3e0b2ccd6132e67", i12);
        f7346zT = a5;
        fmVarArr[i12] = a5;
        int i13 = i12 + 1;
        C2491fm a6 = C4105zY.m41624a(SectorCategory.class, "fcad268a06b89311809988ac14fc81e3", i13);
        f7347zU = a6;
        fmVarArr[i13] = a6;
        int i14 = i13 + 1;
        C2491fm a7 = C4105zY.m41624a(SectorCategory.class, "f3a87d0eb2977b2876dbe6f3f2a54943", i14);
        f7330NZ = a7;
        fmVarArr[i14] = a7;
        int i15 = i14 + 1;
        C2491fm a8 = C4105zY.m41624a(SectorCategory.class, "9c90935f5fd9fa52337c254808986e55", i15);
        f7331Oa = a8;
        fmVarArr[i15] = a8;
        int i16 = i15 + 1;
        C2491fm a9 = C4105zY.m41624a(SectorCategory.class, "0e60ce38004db07e56f82b253fafc8c6", i16);
        f7332Ob = a9;
        fmVarArr[i16] = a9;
        int i17 = i16 + 1;
        C2491fm a10 = C4105zY.m41624a(SectorCategory.class, "39a7890aacb0cc83870b9532161b6e44", i17);
        f7333Oc = a10;
        fmVarArr[i17] = a10;
        int i18 = i17 + 1;
        C2491fm a11 = C4105zY.m41624a(SectorCategory.class, "e4ff3b02f7a94f29c6212d2ed9c91f23", i18);
        f7334Od = a11;
        fmVarArr[i18] = a11;
        int i19 = i18 + 1;
        C2491fm a12 = C4105zY.m41624a(SectorCategory.class, "2cf66db1e7fb275b0919083c6c4c91b2", i19);
        f7335Oe = a12;
        fmVarArr[i19] = a12;
        int i20 = i19 + 1;
        C2491fm a13 = C4105zY.m41624a(SectorCategory.class, "3f00770abeccb00654322121c7ef251a", i20);
        _f_toString_0020_0028_0029Ljava_002flang_002fString_003b = a13;
        fmVarArr[i20] = a13;
        int i21 = i20 + 1;
        _m_methods = fmVarArr;
        C1634Xv.m11725a((Object[]) TaikodomObject._m_methods, (Object[]) _m_methods);
        ___iScriptClass = aUO.m18566a(SectorCategory.class, aJH.class, _m_fields, _m_methods);
    }

    /* renamed from: a */
    private void m30791a(C3904wY wYVar) {
        bFf().mo5608dq().mo3197f(_f_type, wYVar);
    }

    /* renamed from: a */
    private void m30792a(String str) {
        bFf().mo5608dq().mo3197f(f7338bM, str);
    }

    /* renamed from: a */
    private void m30793a(UUID uuid) {
        bFf().mo5608dq().mo3197f(f7337bL, uuid);
    }

    /* renamed from: an */
    private UUID m30794an() {
        return (UUID) bFf().mo5608dq().mo3214p(f7337bL);
    }

    /* renamed from: ao */
    private String m30795ao() {
        return (String) bFf().mo5608dq().mo3214p(f7338bM);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Type")
    @C0064Am(aul = "2cf66db1e7fb275b0919083c6c4c91b2", aum = 0)
    @C5566aOg
    /* renamed from: b */
    private void m30799b(C3904wY wYVar) {
        throw new aWi(new aCE(this, f7335Oe, new Object[]{wYVar}));
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Handle")
    @C0064Am(aul = "273f6fd415f9314ecae6e7dc843c43cc", aum = 0)
    @C5566aOg
    /* renamed from: b */
    private void m30800b(String str) {
        throw new aWi(new aCE(this, f7342bQ, new Object[]{str}));
    }

    /* renamed from: b */
    private void m30802b(short s) {
        bFf().mo5608dq().mo3152a(f7327NW, s);
    }

    /* renamed from: c */
    private void m30803c(UUID uuid) {
        switch (bFf().mo6893i(f7340bO)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f7340bO, new Object[]{uuid}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f7340bO, new Object[]{uuid}));
                break;
        }
        m30801b(uuid);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Sector Category")
    @C0064Am(aul = "9c90935f5fd9fa52337c254808986e55", aum = 0)
    @C5566aOg
    /* renamed from: c */
    private void m30804c(short s) {
        throw new aWi(new aCE(this, f7331Oa, new Object[]{new Short(s)}));
    }

    /* renamed from: d */
    private void m30805d(I18NString i18NString) {
        bFf().mo5608dq().mo3197f(f7345zQ, i18NString);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Name")
    @C0064Am(aul = "fcad268a06b89311809988ac14fc81e3", aum = 0)
    @C5566aOg
    /* renamed from: e */
    private void m30806e(I18NString i18NString) {
        throw new aWi(new aCE(this, f7347zU, new Object[]{i18NString}));
    }

    /* renamed from: kb */
    private I18NString m30807kb() {
        return (I18NString) bFf().mo5608dq().mo3214p(f7345zQ);
    }

    /* renamed from: sE */
    private short m30809sE() {
        return bFf().mo5608dq().mo3215q(f7327NW);
    }

    /* renamed from: sF */
    private C3904wY m30810sF() {
        return (C3904wY) bFf().mo5608dq().mo3214p(_f_type);
    }

    /* renamed from: sG */
    private Asset m30811sG() {
        return (Asset) bFf().mo5608dq().mo3214p(f7329NY);
    }

    /* renamed from: t */
    private void m30815t(Asset tCVar) {
        bFf().mo5608dq().mo3197f(f7329NY, tCVar);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Icon")
    @C0064Am(aul = "39a7890aacb0cc83870b9532161b6e44", aum = 0)
    @C5566aOg
    /* renamed from: u */
    private void m30816u(Asset tCVar) {
        throw new aWi(new aCE(this, f7333Oc, new Object[]{tCVar}));
    }

    /* renamed from: T */
    public C6494any mo11T() {
        return ___iScriptClass;
    }

    /* renamed from: U */
    public C2491fm[] mo12U() {
        return _m_methods;
    }

    /* renamed from: W */
    public Object mo13W() {
        return new aJH(this);
    }

    /* renamed from: a */
    public Object mo14a(C0495Gr gr) {
        Object[] args = gr.getArgs();
        switch (gr.mo2417hq() - TaikodomObject._m_methodCount) {
            case 0:
                return m30796ap();
            case 1:
                m30801b((UUID) args[0]);
                return null;
            case 2:
                return m30797ar();
            case 3:
                m30800b((String) args[0]);
                return null;
            case 4:
                return m30808kd();
            case 5:
                m30806e((I18NString) args[0]);
                return null;
            case 6:
                return new Short(m30812sH());
            case 7:
                m30804c(((Short) args[0]).shortValue());
                return null;
            case 8:
                return m30813sJ();
            case 9:
                m30816u((Asset) args[0]);
                return null;
            case 10:
                return m30814sL();
            case 11:
                m30799b((C3904wY) args[0]);
                return null;
            case 12:
                return m30798au();
            default:
                return super.mo14a(gr);
        }
    }

    /* renamed from: a */
    public void mo15a(Collection collection, C0495Gr gr) {
        super.mo15a(collection, gr);
    }

    /* renamed from: aq */
    public UUID mo18aq() {
        switch (bFf().mo6893i(f7339bN)) {
            case 0:
                return null;
            case 2:
                return (UUID) bFf().mo5606d(new aCE(this, f7339bN, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f7339bN, new Object[0]));
                break;
        }
        return m30796ap();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Type")
    @C5566aOg
    /* renamed from: c */
    public void mo18435c(C3904wY wYVar) {
        switch (bFf().mo6893i(f7335Oe)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f7335Oe, new Object[]{wYVar}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f7335Oe, new Object[]{wYVar}));
                break;
        }
        m30799b(wYVar);
    }

    /* renamed from: c */
    public C5663aRz[] mo25c() {
        return _m_fields;
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Sector Category")
    @C5566aOg
    /* renamed from: d */
    public void mo18436d(short s) {
        switch (bFf().mo6893i(f7331Oa)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f7331Oa, new Object[]{new Short(s)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f7331Oa, new Object[]{new Short(s)}));
                break;
        }
        m30804c(s);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Name")
    @C5566aOg
    /* renamed from: f */
    public void mo18437f(I18NString i18NString) {
        switch (bFf().mo6893i(f7347zU)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f7347zU, new Object[]{i18NString}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f7347zU, new Object[]{i18NString}));
                break;
        }
        m30806e(i18NString);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Handle")
    public String getHandle() {
        switch (bFf().mo6893i(f7341bP)) {
            case 0:
                return null;
            case 2:
                return (String) bFf().mo5606d(new aCE(this, f7341bP, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f7341bP, new Object[0]));
                break;
        }
        return m30797ar();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Handle")
    @C5566aOg
    public void setHandle(String str) {
        switch (bFf().mo6893i(f7342bQ)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f7342bQ, new Object[]{str}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f7342bQ, new Object[]{str}));
                break;
        }
        m30800b(str);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Name")
    /* renamed from: ke */
    public I18NString mo18438ke() {
        switch (bFf().mo6893i(f7346zT)) {
            case 0:
                return null;
            case 2:
                return (I18NString) bFf().mo5606d(new aCE(this, f7346zT, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f7346zT, new Object[0]));
                break;
        }
        return m30808kd();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Sector Category")
    /* renamed from: sI */
    public short mo18439sI() {
        switch (bFf().mo6893i(f7330NZ)) {
            case 0:
                return 0;
            case 2:
                return ((Short) bFf().mo5606d(new aCE(this, f7330NZ, new Object[0]))).shortValue();
            case 3:
                bFf().mo5606d(new aCE(this, f7330NZ, new Object[0]));
                break;
        }
        return m30812sH();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Icon")
    /* renamed from: sK */
    public Asset mo18440sK() {
        switch (bFf().mo6893i(f7332Ob)) {
            case 0:
                return null;
            case 2:
                return (Asset) bFf().mo5606d(new aCE(this, f7332Ob, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f7332Ob, new Object[0]));
                break;
        }
        return m30813sJ();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Type")
    /* renamed from: sM */
    public C3904wY mo18441sM() {
        switch (bFf().mo6893i(f7334Od)) {
            case 0:
                return null;
            case 2:
                return (C3904wY) bFf().mo5606d(new aCE(this, f7334Od, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f7334Od, new Object[0]));
                break;
        }
        return m30814sL();
    }

    public String toString() {
        switch (bFf().mo6893i(_f_toString_0020_0028_0029Ljava_002flang_002fString_003b)) {
            case 0:
                return null;
            case 2:
                return (String) bFf().mo5606d(new aCE(this, _f_toString_0020_0028_0029Ljava_002flang_002fString_003b, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, _f_toString_0020_0028_0029Ljava_002flang_002fString_003b, new Object[0]));
                break;
        }
        return m30798au();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Icon")
    @C5566aOg
    /* renamed from: v */
    public void mo18443v(Asset tCVar) {
        switch (bFf().mo6893i(f7333Oc)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f7333Oc, new Object[]{tCVar}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f7333Oc, new Object[]{tCVar}));
                break;
        }
        m30816u(tCVar);
    }

    @C0064Am(aul = "6536cf0a9829e102a91d340db006808c", aum = 0)
    /* renamed from: ap */
    private UUID m30796ap() {
        return m30794an();
    }

    @C0064Am(aul = "bd07cb39025989a96daa0a400ec374b7", aum = 0)
    /* renamed from: b */
    private void m30801b(UUID uuid) {
        m30793a(uuid);
    }

    /* renamed from: S */
    public void mo10S() {
        super.mo10S();
        m30793a(UUID.randomUUID());
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Handle")
    @C0064Am(aul = "248249a806762cd86514f977c468fff1", aum = 0)
    /* renamed from: ar */
    private String m30797ar() {
        return m30795ao();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Name")
    @C0064Am(aul = "fbbeafc3c101db6eb3e0b2ccd6132e67", aum = 0)
    /* renamed from: kd */
    private I18NString m30808kd() {
        return m30807kb();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Sector Category")
    @C0064Am(aul = "f3a87d0eb2977b2876dbe6f3f2a54943", aum = 0)
    /* renamed from: sH */
    private short m30812sH() {
        return m30809sE();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Icon")
    @C0064Am(aul = "0e60ce38004db07e56f82b253fafc8c6", aum = 0)
    /* renamed from: sJ */
    private Asset m30813sJ() {
        return m30811sG();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Type")
    @C0064Am(aul = "e4ff3b02f7a94f29c6212d2ed9c91f23", aum = 0)
    /* renamed from: sL */
    private C3904wY m30814sL() {
        return m30810sF();
    }

    @C0064Am(aul = "3f00770abeccb00654322121c7ef251a", aum = 0)
    /* renamed from: au */
    private String m30798au() {
        return "[" + getHandle() + "] " + mo18438ke().get();
    }
}
