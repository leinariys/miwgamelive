package game.script.ship;

import game.network.message.externalizable.aCE;
import game.network.message.serializable.C3438ra;
import game.script.template.BaseTaikodomContent;
import logic.baa.*;
import logic.data.mbean.aEK;
import logic.res.code.C5640aRc;
import logic.res.code.C5663aRz;
import logic.res.code.C6494any;
import logic.res.html.C2491fm;
import p001a.*;

import java.util.Collection;

@C2712iu(mo19786Bt = BaseTaikodomContent.class)
@C5511aMd
@C6485anp
/* renamed from: a.afK  reason: case insensitive filesystem */
/* compiled from: a */
public class ShipStructure extends aDJ implements C1616Xf {

    /* renamed from: Wm */
    public static final C2491fm f4454Wm = null;
    /* renamed from: _f_getShip_0020_0028_0029Ltaikodom_002fgame_002fscript_002fship_002fShip_003b */
    public static final C2491fm f4455x851d656b = null;
    public static final int _m_fieldCount = 0;
    public static final C5663aRz[] _m_fields = null;
    public static final int _m_methodCount = 0;
    public static final C2491fm[] _m_methods = null;
    public static final C5663aRz aOP = null;
    public static final C2491fm aPi = null;
    public static final C5663aRz cJJ = null;
    public static final C5663aRz dmj = null;
    public static final C5663aRz fvt = null;
    public static final C2491fm fvu = null;
    public static final C2491fm fvv = null;
    public static final long serialVersionUID = 0;
    public static C6494any ___iScriptClass;
    @C0064Am(aul = "4d9837d7683bbc168b0a3aa29c9f8d98", aum = 3)
    private static Ship aOO;
    @C0064Am(aul = "22f001e3fd7a849fd6648314aa22edf6", aum = 0)
    private static C3438ra<ShipSectorType> cJI;
    @C0064Am(aul = "7bb6f81c91b1735f22e0c54932dfdd7a", aum = 2)
    @C0803Ld
    private static Shield dmi;
    @C0064Am(aul = "725de254639b84516756989dc53f4064", aum = 1)
    private static C3438ra<ShipSector> fvs;

    static {
        m21502V();
    }

    public ShipStructure() {
        super((C5540aNg) null);
        super.mo10S();
    }

    public ShipStructure(ShipStructureType aks) {
        super((C5540aNg) null);
        super._m_script_init(aks);
    }

    public ShipStructure(C5540aNg ang) {
        super(ang);
    }

    /* renamed from: V */
    static void m21502V() {
        serialVersionUID = (long) 1;
        _m_fieldCount = aDJ._m_fieldCount + 4;
        _m_methodCount = aDJ._m_methodCount + 5;
        int i = aDJ._m_fieldCount;
        C5663aRz[] arzArr = new C5663aRz[(i + 4)];
        C5663aRz b = C5640aRc.m17844b(ShipStructure.class, "22f001e3fd7a849fd6648314aa22edf6", i);
        cJJ = b;
        arzArr[i] = b;
        int i2 = i + 1;
        C5663aRz b2 = C5640aRc.m17844b(ShipStructure.class, "725de254639b84516756989dc53f4064", i2);
        fvt = b2;
        arzArr[i2] = b2;
        int i3 = i2 + 1;
        C5663aRz b3 = C5640aRc.m17844b(ShipStructure.class, "7bb6f81c91b1735f22e0c54932dfdd7a", i3);
        dmj = b3;
        arzArr[i3] = b3;
        int i4 = i3 + 1;
        C5663aRz b4 = C5640aRc.m17844b(ShipStructure.class, "4d9837d7683bbc168b0a3aa29c9f8d98", i4);
        aOP = b4;
        arzArr[i4] = b4;
        int i5 = i4 + 1;
        _m_fields = arzArr;
        C1634Xv.m11725a((Object[]) aDJ._m_fields, (Object[]) _m_fields);
        int i6 = aDJ._m_methodCount;
        C2491fm[] fmVarArr = new C2491fm[(i6 + 5)];
        C2491fm a = C4105zY.m41624a(ShipStructure.class, "4a70136f9431ee18c3c044c13912d896", i6);
        f4454Wm = a;
        fmVarArr[i6] = a;
        int i7 = i6 + 1;
        C2491fm a2 = C4105zY.m41624a(ShipStructure.class, "3fe7cd1d228324addead705124c834c8", i7);
        fvu = a2;
        fmVarArr[i7] = a2;
        int i8 = i7 + 1;
        C2491fm a3 = C4105zY.m41624a(ShipStructure.class, "ae397ce63b4feac0babbdce89f68f5b2", i8);
        f4455x851d656b = a3;
        fmVarArr[i8] = a3;
        int i9 = i8 + 1;
        C2491fm a4 = C4105zY.m41624a(ShipStructure.class, "5362d993773e7c201c2255b0067275f7", i9);
        aPi = a4;
        fmVarArr[i9] = a4;
        int i10 = i9 + 1;
        C2491fm a5 = C4105zY.m41624a(ShipStructure.class, "2312df12661238f36f33220c1a3ce24b", i10);
        fvv = a5;
        fmVarArr[i10] = a5;
        int i11 = i10 + 1;
        _m_methods = fmVarArr;
        C1634Xv.m11725a((Object[]) aDJ._m_methods, (Object[]) _m_methods);
        ___iScriptClass = aUO.m18566a(ShipStructure.class, aEK.class, _m_fields, _m_methods);
    }

    /* renamed from: Tn */
    private Ship m21501Tn() {
        return (Ship) bFf().mo5608dq().mo3214p(aOP);
    }

    private C3438ra aHC() {
        return ((ShipStructureType) getType()).bVG();
    }

    private Shield aZJ() {
        return (Shield) bFf().mo5608dq().mo3214p(dmj);
    }

    /* renamed from: az */
    private void m21503az(C3438ra raVar) {
        throw new C6039afL();
    }

    /* renamed from: bP */
    private void m21504bP(C3438ra raVar) {
        bFf().mo5608dq().mo3197f(fvt, raVar);
    }

    private C3438ra bVE() {
        return (C3438ra) bFf().mo5608dq().mo3214p(fvt);
    }

    /* renamed from: c */
    private void m21505c(Shield gjVar) {
        bFf().mo5608dq().mo3197f(dmj, gjVar);
    }

    /* renamed from: k */
    private void m21507k(Ship fAVar) {
        bFf().mo5608dq().mo3197f(aOP, fAVar);
    }

    /* renamed from: S */
    public void mo10S() {
        super.mo10S();
    }

    /* renamed from: T */
    public C6494any mo11T() {
        return ___iScriptClass;
    }

    /* renamed from: U */
    public C2491fm[] mo12U() {
        return _m_methods;
    }

    /* renamed from: W */
    public Object mo13W() {
        return new aEK(this);
    }

    /* renamed from: a */
    public Object mo14a(C0495Gr gr) {
        Object[] args = gr.getArgs();
        switch (gr.mo2417hq() - aDJ._m_methodCount) {
            case 0:
                return m21509zu();
            case 1:
                m21506d((Shield) args[0]);
                return null;
            case 2:
                return m21500Mo();
            case 3:
                m21508l((Ship) args[0]);
                return null;
            case 4:
                return bVF();
            default:
                return super.mo14a(gr);
        }
    }

    /* renamed from: a */
    public void mo15a(Collection collection, C0495Gr gr) {
        super.mo15a(collection, gr);
    }

    /* renamed from: al */
    public Ship mo13241al() {
        switch (bFf().mo6893i(f4455x851d656b)) {
            case 0:
                return null;
            case 2:
                return (Ship) bFf().mo5606d(new aCE(this, f4455x851d656b, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f4455x851d656b, new Object[0]));
                break;
        }
        return m21500Mo();
    }

    public C3438ra<ShipSector> bVG() {
        switch (bFf().mo6893i(fvv)) {
            case 0:
                return null;
            case 2:
                return (C3438ra) bFf().mo5606d(new aCE(this, fvv, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, fvv, new Object[0]));
                break;
        }
        return bVF();
    }

    /* renamed from: c */
    public C5663aRz[] mo25c() {
        return _m_fields;
    }

    /* renamed from: e */
    public void mo13243e(Shield gjVar) {
        switch (bFf().mo6893i(fvu)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, fvu, new Object[]{gjVar}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, fvu, new Object[]{gjVar}));
                break;
        }
        m21506d(gjVar);
    }

    /* renamed from: m */
    public void mo13245m(Ship fAVar) {
        switch (bFf().mo6893i(aPi)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, aPi, new Object[]{fAVar}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, aPi, new Object[]{fAVar}));
                break;
        }
        m21508l(fAVar);
    }

    /* renamed from: zv */
    public Shield mo13246zv() {
        switch (bFf().mo6893i(f4454Wm)) {
            case 0:
                return null;
            case 2:
                return (Shield) bFf().mo5606d(new aCE(this, f4454Wm, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f4454Wm, new Object[0]));
                break;
        }
        return m21509zu();
    }

    /* renamed from: i */
    public void mo13244i(ShipStructureType aks) {
        super.mo967a((C2961mJ) aks);
        for (ShipSectorType e : aHC()) {
            ShipSector or = (ShipSector) bFf().mo6865M(ShipSector.class);
            or.mo4430e(e);
            or.mo4431e(this);
            bVE().add(or);
        }
    }

    @C0064Am(aul = "4a70136f9431ee18c3c044c13912d896", aum = 0)
    /* renamed from: zu */
    private Shield m21509zu() {
        return aZJ();
    }

    @C0064Am(aul = "3fe7cd1d228324addead705124c834c8", aum = 0)
    /* renamed from: d */
    private void m21506d(Shield gjVar) {
        m21505c(gjVar);
    }

    @C0064Am(aul = "ae397ce63b4feac0babbdce89f68f5b2", aum = 0)
    /* renamed from: Mo */
    private Ship m21500Mo() {
        return m21501Tn();
    }

    @C0064Am(aul = "5362d993773e7c201c2255b0067275f7", aum = 0)
    /* renamed from: l */
    private void m21508l(Ship fAVar) {
        m21507k(fAVar);
    }

    @C0064Am(aul = "2312df12661238f36f33220c1a3ce24b", aum = 0)
    private C3438ra<ShipSector> bVF() {
        return bVE();
    }
}
