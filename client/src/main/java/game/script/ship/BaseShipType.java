package game.script.ship;

import game.network.message.externalizable.C6853aut;
import game.network.message.externalizable.aCE;
import game.network.message.serializable.C3438ra;
import game.script.damage.DamageType;
import game.script.item.ItemType;
import game.script.resource.Asset;
import game.script.ship.categories.*;
import logic.baa.*;
import logic.data.mbean.C6905avt;
import logic.res.code.C5663aRz;
import logic.res.code.C6494any;
import logic.res.html.C2491fm;
import p001a.*;
import taikodom.infra.script.I18NString;

import java.util.*;

@C5829abJ("1.2.0")
@C5511aMd
@C6485anp
/* renamed from: a.pG */
/* compiled from: a */
public abstract class BaseShipType extends ItemType implements C1616Xf, aOW, C4068yr {

    /* renamed from: Do */
    public static final C2491fm f8815Do = null;

    /* renamed from: MN */
    public static final C2491fm f8816MN = null;
    public static final int _m_fieldCount = 0;
    public static final C5663aRz[] _m_fields = null;
    public static final int _m_methodCount = 0;
    public static final C2491fm[] _m_methods = null;
    public static final C2491fm aDS = null;
    public static final C2491fm aDk = null;
    public static final C2491fm aMj = null;
    public static final C2491fm aMp = null;
    public static final C2491fm aMr = null;
    public static final C2491fm aTA = null;
    public static final C2491fm aTB = null;
    public static final C2491fm aTk = null;
    public static final C2491fm aTl = null;
    public static final C2491fm aTm = null;
    public static final C2491fm aTn = null;
    public static final C2491fm aTo = null;
    public static final C2491fm aTp = null;
    public static final C2491fm aTq = null;
    public static final C2491fm aTr = null;
    public static final C2491fm aTs = null;
    public static final C2491fm aTt = null;
    public static final C2491fm aTu = null;
    public static final C2491fm aTv = null;
    public static final C2491fm aTw = null;
    public static final C2491fm aTx = null;
    public static final C2491fm aTy = null;
    public static final C2491fm aTz = null;
    public static final long serialVersionUID = 0;
    public static C6494any ___iScriptClass;

    static {
        m37031V();
    }

    private transient List<C6853aut> aTi;
    private transient boolean aTj;

    public BaseShipType() {
        super((C5540aNg) null);
        super.mo10S();
    }

    public BaseShipType(C5540aNg ang) {
        super(ang);
    }

    /* renamed from: V */
    static void m37031V() {
        serialVersionUID = (long) 1;
        _m_fieldCount = ItemType._m_fieldCount + 0;
        _m_methodCount = ItemType._m_methodCount + 25;
        _m_fields = new C5663aRz[(ItemType._m_fieldCount + 0)];
        C1634Xv.m11725a((Object[]) ItemType._m_fields, (Object[]) _m_fields);
        int i = ItemType._m_methodCount;
        C2491fm[] fmVarArr = new C2491fm[(i + 25)];
        C2491fm a = C4105zY.m41624a(BaseShipType.class, "2f49a0f2b23ce320d6a336937d5e243e", i);
        aTk = a;
        fmVarArr[i] = a;
        int i2 = i + 1;
        C2491fm a2 = C4105zY.m41624a(BaseShipType.class, "3e3857a904a2bc4ed60ff73fdc4857b4", i2);
        aDk = a2;
        fmVarArr[i2] = a2;
        int i3 = i2 + 1;
        C2491fm a3 = C4105zY.m41624a(BaseShipType.class, "ec58ce8c17173a3d5da32f4d6b33cec5", i3);
        aTl = a3;
        fmVarArr[i3] = a3;
        int i4 = i3 + 1;
        C2491fm a4 = C4105zY.m41624a(BaseShipType.class, "b5d11f258f4ba9019225987455bedb38", i4);
        aTm = a4;
        fmVarArr[i4] = a4;
        int i5 = i4 + 1;
        C2491fm a5 = C4105zY.m41624a(BaseShipType.class, "ee2eed581e960653131bb1e7c0fde4ab", i5);
        aTn = a5;
        fmVarArr[i5] = a5;
        int i6 = i5 + 1;
        C2491fm a6 = C4105zY.m41624a(BaseShipType.class, "166dfdc822ddfcaa1b16f70acb4b0d27", i6);
        aTo = a6;
        fmVarArr[i6] = a6;
        int i7 = i6 + 1;
        C2491fm a7 = C4105zY.m41624a(BaseShipType.class, "479ffc7ed7f9f820d39e4cc5e145b456", i7);
        f8815Do = a7;
        fmVarArr[i7] = a7;
        int i8 = i7 + 1;
        C2491fm a8 = C4105zY.m41624a(BaseShipType.class, "b22c82422b0797f2b524187c967f91a6", i8);
        aTp = a8;
        fmVarArr[i8] = a8;
        int i9 = i8 + 1;
        C2491fm a9 = C4105zY.m41624a(BaseShipType.class, "c6c23c4d04591d0788c41560d0ec8054", i9);
        aDS = a9;
        fmVarArr[i9] = a9;
        int i10 = i9 + 1;
        C2491fm a10 = C4105zY.m41624a(BaseShipType.class, "8b234423b1d59540debb5d9c2512ac3d", i10);
        aTq = a10;
        fmVarArr[i10] = a10;
        int i11 = i10 + 1;
        C2491fm a11 = C4105zY.m41624a(BaseShipType.class, "e5e6265245e845e917545f4c7194dfb5", i11);
        aTr = a11;
        fmVarArr[i11] = a11;
        int i12 = i11 + 1;
        C2491fm a12 = C4105zY.m41624a(BaseShipType.class, "c39c7e33c8f092bca3d706d77eefd363", i12);
        aTs = a12;
        fmVarArr[i12] = a12;
        int i13 = i12 + 1;
        C2491fm a13 = C4105zY.m41624a(BaseShipType.class, "044dd60e42a8cb7857ffcf08152053f6", i13);
        aTt = a13;
        fmVarArr[i13] = a13;
        int i14 = i13 + 1;
        C2491fm a14 = C4105zY.m41624a(BaseShipType.class, "3431a02d462eeca1778e519ea5c6bffa", i14);
        aTu = a14;
        fmVarArr[i14] = a14;
        int i15 = i14 + 1;
        C2491fm a15 = C4105zY.m41624a(BaseShipType.class, "132977e4a955329585b8cec73f287213", i15);
        aTv = a15;
        fmVarArr[i15] = a15;
        int i16 = i15 + 1;
        C2491fm a16 = C4105zY.m41624a(BaseShipType.class, "c56fd8a97f683f9c20996730fa40f01f", i16);
        aTw = a16;
        fmVarArr[i16] = a16;
        int i17 = i16 + 1;
        C2491fm a17 = C4105zY.m41624a(BaseShipType.class, "4500406294d41cf81c994147681adb48", i17);
        aTx = a17;
        fmVarArr[i17] = a17;
        int i18 = i17 + 1;
        C2491fm a18 = C4105zY.m41624a(BaseShipType.class, "0278f71ce76c7cd947182f6df8013790", i18);
        aTy = a18;
        fmVarArr[i18] = a18;
        int i19 = i18 + 1;
        C2491fm a19 = C4105zY.m41624a(BaseShipType.class, "6aebe910f03e22cce20b9fed370bd0c1", i19);
        aTz = a19;
        fmVarArr[i19] = a19;
        int i20 = i19 + 1;
        C2491fm a20 = C4105zY.m41624a(BaseShipType.class, "0b5afbd3d36850c341b8ba8976f5d0b3", i20);
        aTA = a20;
        fmVarArr[i20] = a20;
        int i21 = i20 + 1;
        C2491fm a21 = C4105zY.m41624a(BaseShipType.class, "d9b4455745fe8e13469078d2641c5a41", i21);
        f8816MN = a21;
        fmVarArr[i21] = a21;
        int i22 = i21 + 1;
        C2491fm a22 = C4105zY.m41624a(BaseShipType.class, "bcd45f47067a77e1a82e4da44fc4fee0", i22);
        aMp = a22;
        fmVarArr[i22] = a22;
        int i23 = i22 + 1;
        C2491fm a23 = C4105zY.m41624a(BaseShipType.class, "2502b1b2385378b09d11d5effe8f2673", i23);
        aMr = a23;
        fmVarArr[i23] = a23;
        int i24 = i23 + 1;
        C2491fm a24 = C4105zY.m41624a(BaseShipType.class, "d6421ab338cab770932fbd85064d3492", i24);
        aTB = a24;
        fmVarArr[i24] = a24;
        int i25 = i24 + 1;
        C2491fm a25 = C4105zY.m41624a(BaseShipType.class, "b38560c8dcb4438171dedee0c4144efe", i25);
        aMj = a25;
        fmVarArr[i25] = a25;
        int i26 = i25 + 1;
        _m_methods = fmVarArr;
        C1634Xv.m11725a((Object[]) ItemType._m_methods, (Object[]) _m_methods);
        ___iScriptClass = aUO.m18566a(BaseShipType.class, C6905avt.class, _m_fields, _m_methods);
    }

    /* renamed from: a */
    public static void m37049a(C6859auz auz) {
        if ("".equals(auz.getVersion())) {
            try {
                String lowerCase = ((I18NString) auz.mo10092kf("itemTypeCategory").get("name")).get(I18NString.DEFAULT_LOCATION).toLowerCase();
                if ("battleship".equals(lowerCase)) {
                    auz.setClass(BattleCruiserType.class);
                } else if ("bomber".equals(lowerCase)) {
                    auz.setClass(BomberType.class);
                } else if ("explorer".equals(lowerCase)) {
                    auz.setClass(ExplorerType.class);
                } else if ("fighter".equals(lowerCase)) {
                    auz.setClass(FighterType.class);
                } else if ("freighter".equals(lowerCase)) {
                    auz.setClass(FreighterType.class);
                }
            } catch (NullPointerException e) {
                System.err.println(e);
            }
        }
        if (auz.mo3117j(1, 0, 0)) {
            try {
                if ("basic".equals(((I18NString) auz.mo10092kf("itemTypeCategory").get("name")).get(I18NString.DEFAULT_LOCATION).toLowerCase())) {
                    auz.setClass(ShipType.class);
                }
            } catch (NullPointerException e2) {
                System.err.println(e2);
            }
        }
    }

    @C0064Am(aul = "3e3857a904a2bc4ed60ff73fdc4857b4", aum = 0)
    /* renamed from: Nt */
    private Asset m37027Nt() {
        throw new aWi(new aCE(this, aDk, new Object[0]));
    }

    @C0064Am(aul = "2f49a0f2b23ce320d6a336937d5e243e", aum = 0)
    /* renamed from: VK */
    private List<ShipStructureType> m37032VK() {
        throw new aWi(new aCE(this, aTk, new Object[0]));
    }

    @C0064Am(aul = "e5e6265245e845e917545f4c7194dfb5", aum = 0)
    /* renamed from: VS */
    private HullType m37036VS() {
        throw new aWi(new aCE(this, aTr, new Object[0]));
    }

    /* renamed from: VX */
    private ShieldType m37039VX() {
        switch (bFf().mo6893i(aTt)) {
            case 0:
                return null;
            case 2:
                return (ShieldType) bFf().mo5606d(new aCE(this, aTt, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, aTt, new Object[0]));
                break;
        }
        return m37038VW();
    }

    @C0064Am(aul = "132977e4a955329585b8cec73f287213", aum = 0)
    /* renamed from: Wa */
    private CargoHoldType m37041Wa() {
        throw new aWi(new aCE(this, aTv, new Object[0]));
    }

    @C0064Am(aul = "0b5afbd3d36850c341b8ba8976f5d0b3", aum = 0)
    /* renamed from: Wk */
    private C3438ra<C3315qE> m37046Wk() {
        throw new aWi(new aCE(this, aTA, new Object[0]));
    }

    @C0064Am(aul = "d6421ab338cab770932fbd85064d3492", aum = 0)
    /* renamed from: Wm */
    private I18NString m37047Wm() {
        throw new aWi(new aCE(this, aTB, new Object[0]));
    }

    @C0064Am(aul = "b22c82422b0797f2b524187c967f91a6", aum = 0)
    /* renamed from: a */
    private void m37050a(C3315qE qEVar) {
        throw new aWi(new aCE(this, aTp, new Object[]{qEVar}));
    }

    /* renamed from: NK */
    public <T extends aDJ> T mo7459NK() {
        switch (bFf().mo6893i(aDS)) {
            case 0:
                return null;
            case 2:
                return (aDJ) bFf().mo5606d(new aCE(this, aDS, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, aDS, new Object[0]));
                break;
        }
        return m37026NJ();
    }

    /* renamed from: Nu */
    public Asset mo4151Nu() {
        switch (bFf().mo6893i(aDk)) {
            case 0:
                return null;
            case 2:
                return (Asset) bFf().mo5606d(new aCE(this, aDk, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, aDk, new Object[0]));
                break;
        }
        return m37027Nt();
    }

    /* renamed from: RU */
    public I18NString mo182RU() {
        switch (bFf().mo6893i(aMj)) {
            case 0:
                return null;
            case 2:
                return (I18NString) bFf().mo5606d(new aCE(this, aMj, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, aMj, new Object[0]));
                break;
        }
        return m37028RT();
    }

    /* renamed from: Sa */
    public String mo185Sa() {
        switch (bFf().mo6893i(aMp)) {
            case 0:
                return null;
            case 2:
                return (String) bFf().mo5606d(new aCE(this, aMp, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, aMp, new Object[0]));
                break;
        }
        return m37029RZ();
    }

    /* renamed from: Se */
    public Asset mo187Se() {
        switch (bFf().mo6893i(aMr)) {
            case 0:
                return null;
            case 2:
                return (Asset) bFf().mo5606d(new aCE(this, aMr, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, aMr, new Object[0]));
                break;
        }
        return m37030Sd();
    }

    /* renamed from: T */
    public C6494any mo11T() {
        return ___iScriptClass;
    }

    /* renamed from: U */
    public C2491fm[] mo12U() {
        return _m_methods;
    }

    /* renamed from: VL */
    public List<ShipStructureType> mo4154VL() {
        switch (bFf().mo6893i(aTk)) {
            case 0:
                return null;
            case 2:
                return (List) bFf().mo5606d(new aCE(this, aTk, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, aTk, new Object[0]));
                break;
        }
        return m37032VK();
    }

    /* renamed from: VN */
    public List<C6853aut> mo21118VN() {
        switch (bFf().mo6893i(aTm)) {
            case 0:
                return null;
            case 2:
                return (List) bFf().mo5606d(new aCE(this, aTm, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, aTm, new Object[0]));
                break;
        }
        return m37033VM();
    }

    /* renamed from: VP */
    public List<C6853aut> mo21119VP() {
        switch (bFf().mo6893i(aTn)) {
            case 0:
                return null;
            case 2:
                return (List) bFf().mo5606d(new aCE(this, aTn, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, aTn, new Object[0]));
                break;
        }
        return m37034VO();
    }

    @aFW("Armor")
    /* renamed from: VR */
    public float mo21120VR() {
        switch (bFf().mo6893i(aTq)) {
            case 0:
                return 0.0f;
            case 2:
                return ((Float) bFf().mo5606d(new aCE(this, aTq, new Object[0]))).floatValue();
            case 3:
                bFf().mo5606d(new aCE(this, aTq, new Object[0]));
                break;
        }
        return m37035VQ();
    }

    /* renamed from: VT */
    public HullType mo4155VT() {
        switch (bFf().mo6893i(aTr)) {
            case 0:
                return null;
            case 2:
                return (HullType) bFf().mo5606d(new aCE(this, aTr, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, aTr, new Object[0]));
                break;
        }
        return m37036VS();
    }

    @aFW("Shield")
    /* renamed from: VV */
    public float mo21121VV() {
        switch (bFf().mo6893i(aTs)) {
            case 0:
                return 0.0f;
            case 2:
                return ((Float) bFf().mo5606d(new aCE(this, aTs, new Object[0]))).floatValue();
            case 3:
                bFf().mo5606d(new aCE(this, aTs, new Object[0]));
                break;
        }
        return m37037VU();
    }

    @aFW("Cargo Hold capacity")
    /* renamed from: VZ */
    public float mo21122VZ() {
        switch (bFf().mo6893i(aTu)) {
            case 0:
                return 0.0f;
            case 2:
                return ((Float) bFf().mo5606d(new aCE(this, aTu, new Object[0]))).floatValue();
            case 3:
                bFf().mo5606d(new aCE(this, aTu, new Object[0]));
                break;
        }
        return m37040VY();
    }

    /* renamed from: W */
    public Object mo13W() {
        return new C6905avt(this);
    }

    /* renamed from: Wb */
    public CargoHoldType mo4156Wb() {
        switch (bFf().mo6893i(aTv)) {
            case 0:
                return null;
            case 2:
                return (CargoHoldType) bFf().mo5606d(new aCE(this, aTv, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, aTv, new Object[0]));
                break;
        }
        return m37041Wa();
    }

    @aFW("Hull")
    /* renamed from: Wd */
    public float mo21123Wd() {
        switch (bFf().mo6893i(aTw)) {
            case 0:
                return 0.0f;
            case 2:
                return ((Float) bFf().mo5606d(new aCE(this, aTw, new Object[0]))).floatValue();
            case 3:
                bFf().mo5606d(new aCE(this, aTw, new Object[0]));
                break;
        }
        return m37042Wc();
    }

    @aFW("Regeneration")
    /* renamed from: Wf */
    public int mo21124Wf() {
        switch (bFf().mo6893i(aTx)) {
            case 0:
                return 0;
            case 2:
                return ((Integer) bFf().mo5606d(new aCE(this, aTx, new Object[0]))).intValue();
            case 3:
                bFf().mo5606d(new aCE(this, aTx, new Object[0]));
                break;
        }
        return m37043We();
    }

    @aFW("Basic slots")
    /* renamed from: Wh */
    public List<ShipSectorType> mo21125Wh() {
        switch (bFf().mo6893i(aTy)) {
            case 0:
                return null;
            case 2:
                return (List) bFf().mo5606d(new aCE(this, aTy, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, aTy, new Object[0]));
                break;
        }
        return m37044Wg();
    }

    @aFW("Turrets")
    /* renamed from: Wj */
    public int mo21126Wj() {
        switch (bFf().mo6893i(aTz)) {
            case 0:
                return 0;
            case 2:
                return ((Integer) bFf().mo5606d(new aCE(this, aTz, new Object[0]))).intValue();
            case 3:
                bFf().mo5606d(new aCE(this, aTz, new Object[0]));
                break;
        }
        return m37045Wi();
    }

    /* renamed from: Wl */
    public C3438ra<C3315qE> mo4157Wl() {
        switch (bFf().mo6893i(aTA)) {
            case 0:
                return null;
            case 2:
                return (C3438ra) bFf().mo5606d(new aCE(this, aTA, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, aTA, new Object[0]));
                break;
        }
        return m37046Wk();
    }

    /* renamed from: Wn */
    public I18NString mo4158Wn() {
        switch (bFf().mo6893i(aTB)) {
            case 0:
                return null;
            case 2:
                return (I18NString) bFf().mo5606d(new aCE(this, aTB, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, aTB, new Object[0]));
                break;
        }
        return m37047Wm();
    }

    /* renamed from: a */
    public Object mo14a(C0495Gr gr) {
        Object[] args = gr.getArgs();
        switch (gr.mo2417hq() - ItemType._m_methodCount) {
            case 0:
                return m37032VK();
            case 1:
                return m37027Nt();
            case 2:
                return m37051bb((String) args[0]);
            case 3:
                return m37033VM();
            case 4:
                return m37034VO();
            case 5:
                m37052g((List) args[0]);
                return null;
            case 6:
                m37048a((C0665JT) args[0]);
                return null;
            case 7:
                m37050a((C3315qE) args[0]);
                return null;
            case 8:
                return m37026NJ();
            case 9:
                return new Float(m37035VQ());
            case 10:
                return m37036VS();
            case 11:
                return new Float(m37037VU());
            case 12:
                return m37038VW();
            case 13:
                return new Float(m37040VY());
            case 14:
                return m37041Wa();
            case 15:
                return new Float(m37042Wc());
            case 16:
                return new Integer(m37043We());
            case 17:
                return m37044Wg();
            case 18:
                return new Integer(m37045Wi());
            case 19:
                return m37046Wk();
            case 20:
                return m37053rO();
            case 21:
                return m37029RZ();
            case 22:
                return m37030Sd();
            case 23:
                return m37047Wm();
            case 24:
                return m37028RT();
            default:
                return super.mo14a(gr);
        }
    }

    /* renamed from: a */
    public void mo15a(Collection collection, C0495Gr gr) {
        super.mo15a(collection, gr);
    }

    /* renamed from: b */
    public void mo24b(C0665JT jt) {
        switch (bFf().mo6893i(f8815Do)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f8815Do, new Object[]{jt}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f8815Do, new Object[]{jt}));
                break;
        }
        m37048a(jt);
    }

    /* renamed from: b */
    public void mo4176b(C3315qE qEVar) {
        switch (bFf().mo6893i(aTp)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, aTp, new Object[]{qEVar}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, aTp, new Object[]{qEVar}));
                break;
        }
        m37050a(qEVar);
    }

    /* renamed from: bc */
    public C6853aut mo21127bc(String str) {
        switch (bFf().mo6893i(aTl)) {
            case 0:
                return null;
            case 2:
                return (C6853aut) bFf().mo5606d(new aCE(this, aTl, new Object[]{str}));
            case 3:
                bFf().mo5606d(new aCE(this, aTl, new Object[]{str}));
                break;
        }
        return m37051bb(str);
    }

    /* renamed from: c */
    public C5663aRz[] mo25c() {
        return _m_fields;
    }

    /* access modifiers changed from: protected */
    /* renamed from: h */
    public void mo21128h(List<C6853aut> list) {
        switch (bFf().mo6893i(aTo)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, aTo, new Object[]{list}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, aTo, new Object[]{list}));
                break;
        }
        m37052g(list);
    }

    /* renamed from: rP */
    public I18NString mo195rP() {
        switch (bFf().mo6893i(f8816MN)) {
            case 0:
                return null;
            case 2:
                return (I18NString) bFf().mo5606d(new aCE(this, f8816MN, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f8816MN, new Object[0]));
                break;
        }
        return m37053rO();
    }

    /* renamed from: S */
    public void mo10S() {
        super.mo10S();
        this.aTj = false;
    }

    /* JADX WARNING: Removed duplicated region for block: B:12:0x005d  */
    @p001a.C0064Am(aul = "ec58ce8c17173a3d5da32f4d6b33cec5", aum = 0)
    /* renamed from: bb */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private C6853aut m37051bb(java.lang.String r7) {
        /*
            r6 = this;
            r1 = 0
            java.util.List<a.aut> r0 = r6.aTi
            if (r0 != 0) goto L_0x001f
            boolean r0 = r6.aTj
            if (r0 == 0) goto L_0x000b
            r0 = r1
        L_0x000a:
            return r0
        L_0x000b:
            java.util.List r0 = r6.mo21118VN()
            r6.aTi = r0
            r0 = 1
            r6.aTj = r0
            java.util.List<a.aut> r0 = r6.aTi
            if (r0 != 0) goto L_0x001f
            java.lang.String r0 = "Problems reading bone file."
            r6.mo8358lY(r0)
            r0 = r1
            goto L_0x000a
        L_0x001f:
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            java.lang.String r2 = "bone_"
            r0.<init>(r2)
            java.lang.StringBuilder r0 = r0.append(r7)
            java.lang.String r2 = r0.toString()
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            java.lang.String r3 = "bone_"
            r0.<init>(r3)
            a.tC r3 = r6.mo4151Nu()
            java.lang.String r3 = r3.getHandle()
            java.lang.StringBuilder r0 = r0.append(r3)
            java.lang.String r3 = "_"
            java.lang.StringBuilder r0 = r0.append(r3)
            java.lang.StringBuilder r0 = r0.append(r7)
            java.lang.String r3 = r0.toString()
            java.util.List<a.aut> r0 = r6.aTi
            java.util.Iterator r4 = r0.iterator()
        L_0x0055:
            boolean r0 = r4.hasNext()
            if (r0 != 0) goto L_0x005d
            r0 = r1
            goto L_0x000a
        L_0x005d:
            java.lang.Object r0 = r4.next()
            a.aut r0 = (game.network.message.externalizable.C6853aut) r0
            java.lang.String r5 = r0.getName()
            boolean r5 = r5.equals(r7)
            if (r5 != 0) goto L_0x000a
            java.lang.String r5 = r0.getName()
            boolean r5 = r5.equals(r3)
            if (r5 != 0) goto L_0x000a
            java.lang.String r5 = r0.getName()
            boolean r5 = r5.equals(r2)
            if (r5 == 0) goto L_0x0055
            goto L_0x000a
        */
        throw new UnsupportedOperationException("Method not decompiled: p001a.C3212pG.m37051bb(java.lang.String):a.aut");
    }

    @C0064Am(aul = "b5d11f258f4ba9019225987455bedb38", aum = 0)
    /* renamed from: VM */
    private List<C6853aut> m37033VM() {
        if (this.aTi == null) {
            try {
                this.aTi = C3925wr.m40752cw(mo4151Nu().getHandle());
            } catch (C1547Wh e) {
                mo8358lY("Problems Reading Bone file '" + e.btt() + "'. Check the file");
                return null;
            }
        }
        return new ArrayList(this.aTi);
    }

    @C0064Am(aul = "ee2eed581e960653131bb1e7c0fde4ab", aum = 0)
    /* renamed from: VO */
    private List<C6853aut> m37034VO() {
        return this.aTi;
    }

    @C0064Am(aul = "166dfdc822ddfcaa1b16f70acb4b0d27", aum = 0)
    /* renamed from: g */
    private void m37052g(List<C6853aut> list) {
        this.aTi = list;
    }

    @C0064Am(aul = "479ffc7ed7f9f820d39e4cc5e145b456", aum = 0)
    /* renamed from: a */
    private void m37048a(C0665JT jt) {
        List<C0665JT> list;
        if (jt.mo3117j(1, 2, 3) && (list = (List) jt.get("turretTypes")) != null) {
            int i = 0;
            for (C0665JT baw : list) {
                C3315qE qEVar = (C3315qE) bFf().mo6865M(C3315qE.class);
                qEVar.mo10S();
                qEVar.mo21303ee(i);
                qEVar.mo21302c((C6544aow) baw.baw());
                mo4176b(qEVar);
                i++;
            }
        }
        if ("".equals(jt.getVersion())) {
            for (ShipStructureType next : mo4154VL()) {
                Iterator it = next.bVG().iterator();
                while (true) {
                    if (!it.hasNext()) {
                        break;
                    }
                    ShipSectorType aux = (ShipSectorType) it.next();
                    if ("sec_spe_special".equals(aux.getHandle())) {
                        next.bVG().remove(aux);
                        System.out.println("sector removed " + aux + " " + aux.getHandle() + " of " + mo19891ke());
                        break;
                    }
                }
            }
        }
    }

    @C0064Am(aul = "c6c23c4d04591d0788c41560d0ec8054", aum = 0)
    /* renamed from: NJ */
    private <T extends aDJ> T m37026NJ() {
        return ((Ship) super.mo7459NK()).agr();
    }

    @aFW("Armor")
    @C0064Am(aul = "8b234423b1d59540debb5d9c2512ac3d", aum = 0)
    /* renamed from: VQ */
    private float m37035VQ() {
        for (Map.Entry next : mo4155VT().ana().entrySet()) {
            if (((DamageType) next.getKey()).getHandle().equals("dmg_non_hull")) {
                return ((C1649YI) next.getValue()).getValue();
            }
        }
        return 0.0f;
    }

    @aFW("Shield")
    @C0064Am(aul = "c39c7e33c8f092bca3d706d77eefd363", aum = 0)
    /* renamed from: VU */
    private float m37037VU() {
        return m37039VX().mo15373hh();
    }

    @C0064Am(aul = "044dd60e42a8cb7857ffcf08152053f6", aum = 0)
    /* renamed from: VW */
    private ShieldType m37038VW() {
        return mo4154VL().get(0).mo9731VX();
    }

    @aFW("Cargo Hold capacity")
    @C0064Am(aul = "3431a02d462eeca1778e519ea5c6bffa", aum = 0)
    /* renamed from: VY */
    private float m37040VY() {
        return mo4156Wb().mo11172wE();
    }

    @aFW("Hull")
    @C0064Am(aul = "c56fd8a97f683f9c20996730fa40f01f", aum = 0)
    /* renamed from: Wc */
    private float m37042Wc() {
        return mo4155VT().mo22727hh();
    }

    @aFW("Regeneration")
    @C0064Am(aul = "4500406294d41cf81c994147681adb48", aum = 0)
    /* renamed from: We */
    private int m37043We() {
        return m37039VX().cqb();
    }

    @aFW("Basic slots")
    @C0064Am(aul = "0278f71ce76c7cd947182f6df8013790", aum = 0)
    /* renamed from: Wg */
    private List<ShipSectorType> m37044Wg() {
        ArrayList arrayList = new ArrayList();
        for (ShipStructureType bVG : mo4154VL()) {
            for (ShipSectorType add : bVG.bVG()) {
                arrayList.add(add);
            }
        }
        return arrayList;
    }

    @aFW("Turrets")
    @C0064Am(aul = "6aebe910f03e22cce20b9fed370bd0c1", aum = 0)
    /* renamed from: Wi */
    private int m37045Wi() {
        return mo4157Wl().size();
    }

    @C0064Am(aul = "d9b4455745fe8e13469078d2641c5a41", aum = 0)
    /* renamed from: rO */
    private I18NString m37053rO() {
        return mo19891ke();
    }

    @C0064Am(aul = "bcd45f47067a77e1a82e4da44fc4fee0", aum = 0)
    /* renamed from: RZ */
    private String m37029RZ() {
        if (mo12100sK() != null) {
            return mo12100sK().getHandle();
        }
        return null;
    }

    @C0064Am(aul = "2502b1b2385378b09d11d5effe8f2673", aum = 0)
    /* renamed from: Sd */
    private Asset m37030Sd() {
        return mo4151Nu();
    }

    @C0064Am(aul = "b38560c8dcb4438171dedee0c4144efe", aum = 0)
    /* renamed from: RT */
    private I18NString m37028RT() {
        return mo4158Wn();
    }
}
