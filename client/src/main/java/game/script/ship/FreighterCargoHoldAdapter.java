package game.script.ship;

import game.network.message.externalizable.aCE;
import logic.baa.C1616Xf;
import logic.baa.C1634Xv;
import logic.baa.C5511aMd;
import logic.baa.C6485anp;
import logic.data.mbean.C0812Lj;
import logic.res.code.C5640aRc;
import logic.res.code.C5663aRz;
import logic.res.code.C6494any;
import logic.res.html.C2491fm;
import p001a.*;

import java.util.Collection;

@C5511aMd
@Deprecated
@C6485anp
/* renamed from: a.DK */
/* compiled from: a */
public class FreighterCargoHoldAdapter extends CargoHoldAdapter implements C1616Xf {

    /* renamed from: Pv */
    public static final C5663aRz f392Pv = null;

    /* renamed from: Rz */
    public static final C2491fm f393Rz = null;
    public static final int _m_fieldCount = 0;
    public static final C5663aRz[] _m_fields = null;
    public static final int _m_methodCount = 0;
    public static final C2491fm[] _m_methods = null;
    public static final C2491fm cIC = null;
    public static final long serialVersionUID = 0;
    public static C6494any ___iScriptClass;
    @C0064Am(aul = "1d58a7d162f607c4523599d9df524b45", aum = 0)
    private static float cvd;

    static {
        m1974V();
    }

    public FreighterCargoHoldAdapter() {
        super((C5540aNg) null);
        super.mo10S();
    }

    public FreighterCargoHoldAdapter(C5540aNg ang) {
        super(ang);
    }

    /* renamed from: V */
    static void m1974V() {
        serialVersionUID = (long) 1;
        _m_fieldCount = CargoHoldAdapter._m_fieldCount + 1;
        _m_methodCount = CargoHoldAdapter._m_methodCount + 2;
        int i = CargoHoldAdapter._m_fieldCount;
        C5663aRz[] arzArr = new C5663aRz[(i + 1)];
        C5663aRz b = C5640aRc.m17844b(FreighterCargoHoldAdapter.class, "1d58a7d162f607c4523599d9df524b45", i);
        f392Pv = b;
        arzArr[i] = b;
        int i2 = i + 1;
        _m_fields = arzArr;
        C1634Xv.m11725a((Object[]) CargoHoldAdapter._m_fields, (Object[]) _m_fields);
        int i3 = CargoHoldAdapter._m_methodCount;
        C2491fm[] fmVarArr = new C2491fm[(i3 + 2)];
        C2491fm a = C4105zY.m41624a(FreighterCargoHoldAdapter.class, "ba92de46055183fb06e9a6c80833035d", i3);
        cIC = a;
        fmVarArr[i3] = a;
        int i4 = i3 + 1;
        C2491fm a2 = C4105zY.m41624a(FreighterCargoHoldAdapter.class, "16da20cd1d8d873eacee02ae3b038037", i4);
        f393Rz = a2;
        fmVarArr[i4] = a2;
        int i5 = i4 + 1;
        _m_methods = fmVarArr;
        C1634Xv.m11725a((Object[]) CargoHoldAdapter._m_methods, (Object[]) _m_methods);
        ___iScriptClass = aUO.m18566a(FreighterCargoHoldAdapter.class, C0812Lj.class, _m_fields, _m_methods);
    }

    private float aHj() {
        return bFf().mo5608dq().mo3211m(f392Pv);
    }

    /* renamed from: fc */
    private void m1975fc(float f) {
        bFf().mo5608dq().mo3150a(f392Pv, f);
    }

    /* renamed from: T */
    public C6494any mo11T() {
        return ___iScriptClass;
    }

    /* renamed from: U */
    public C2491fm[] mo12U() {
        return _m_methods;
    }

    /* renamed from: W */
    public Object mo13W() {
        return new C0812Lj(this);
    }

    /* renamed from: a */
    public Object mo14a(C0495Gr gr) {
        Object[] args = gr.getArgs();
        switch (gr.mo2417hq() - CargoHoldAdapter._m_methodCount) {
            case 0:
                m1976fd(((Float) args[0]).floatValue());
                return null;
            case 1:
                return new Float(m1977wD());
            default:
                return super.mo14a(gr);
        }
    }

    /* renamed from: a */
    public void mo15a(Collection collection, C0495Gr gr) {
        super.mo15a(collection, gr);
    }

    /* renamed from: c */
    public C5663aRz[] mo25c() {
        return _m_fields;
    }

    /* renamed from: fe */
    public void mo1288fe(float f) {
        switch (bFf().mo6893i(cIC)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, cIC, new Object[]{new Float(f)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, cIC, new Object[]{new Float(f)}));
                break;
        }
        m1976fd(f);
    }

    /* renamed from: wE */
    public float mo1289wE() {
        switch (bFf().mo6893i(f393Rz)) {
            case 0:
                return 0.0f;
            case 2:
                return ((Float) bFf().mo5606d(new aCE(this, f393Rz, new Object[0]))).floatValue();
            case 3:
                bFf().mo5606d(new aCE(this, f393Rz, new Object[0]));
                break;
        }
        return m1977wD();
    }

    /* renamed from: S */
    public void mo10S() {
        super.mo10S();
    }

    @C0064Am(aul = "ba92de46055183fb06e9a6c80833035d", aum = 0)
    /* renamed from: fd */
    private void m1976fd(float f) {
        m1975fc(f);
    }

    @C0064Am(aul = "16da20cd1d8d873eacee02ae3b038037", aum = 0)
    /* renamed from: wD */
    private float m1977wD() {
        return aHj();
    }
}
