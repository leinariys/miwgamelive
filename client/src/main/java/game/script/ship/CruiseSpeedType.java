package game.script.ship;

import game.network.message.externalizable.aCE;
import game.script.resource.Asset;
import game.script.template.BaseTaikodomContent;
import logic.baa.*;
import logic.data.mbean.C5467aKl;
import logic.res.code.C5640aRc;
import logic.res.code.C5663aRz;
import logic.res.code.C6494any;
import logic.res.html.C2491fm;
import p001a.*;

import java.util.Collection;

@C0566Hp
@C5829abJ("1.1.0")
@C6485anp
@C5511aMd
/* renamed from: a.NF */
/* compiled from: a */
public class CruiseSpeedType extends BaseTaikodomContent implements C1616Xf {
    public static final int _m_fieldCount = 0;
    public static final C5663aRz[] _m_fields = null;
    public static final int _m_methodCount = 0;
    public static final C2491fm[] _m_methods = null;
    public static final C2491fm aTe = null;
    public static final C2491fm aTf = null;
    public static final C2491fm aTg = null;
    public static final C2491fm aTh = null;
    public static final C2491fm bFx = null;
    public static final C2491fm bFy = null;
    public static final C5663aRz bnO = null;
    public static final C5663aRz bnP = null;
    public static final C5663aRz bnU = null;
    public static final C5663aRz bnV = null;
    public static final C5663aRz cwA = null;
    public static final C5663aRz cwC = null;
    public static final C2491fm dGA = null;
    public static final C2491fm dGB = null;
    public static final C2491fm dGC = null;
    public static final C2491fm dGD = null;
    public static final C2491fm dGE = null;
    public static final C2491fm dGF = null;
    public static final C2491fm dGG = null;
    public static final C2491fm dGH = null;
    public static final C2491fm dGI = null;
    public static final C5663aRz dGv = null;
    public static final C5663aRz dGx = null;
    public static final C2491fm dGy = null;
    public static final C2491fm dGz = null;
    /* renamed from: dN */
    public static final C2491fm f1203dN = null;
    public static final long serialVersionUID = 0;
    public static C6494any ___iScriptClass;
    @C0064Am(aul = "1fb7ebcd6a1ffdb4c6d93d7a9b8ad2d6", aum = 6)
    private static Asset bIu;
    @C0064Am(aul = "9c1368671cbe4d1a2930c900c7b800a5", aum = 0)
    private static float bhk;
    @C0064Am(aul = "473d7c2655278e3ec7c240e1cff91438", aum = 2)
    private static float bhl;
    @C0064Am(aul = "d1f2f62b1bfa89ba924b0076e2221ba9", aum = 4)
    private static float dGt;
    @C0064Am(aul = "3e49da931377a12ddc4330c4dbc6e46a", aum = 5)
    private static float dGu;
    @C0064Am(aul = "f7ae2006ce5840a7b4ea8519c5e8719d", aum = 7)
    private static Asset dGw;
    @C0064Am(aul = "3afdaafb44bd49243d26507d75d133f0", aum = 1)

    /* renamed from: dW */
    private static float f1204dW;
    @C0064Am(aul = "ac92b8d519161de5d11c422e54b62795", aum = 3)

    /* renamed from: dX */
    private static float f1205dX;

    static {
        m7270V();
    }

    public CruiseSpeedType() {
        super((C5540aNg) null);
        super.mo10S();
    }

    public CruiseSpeedType(C5540aNg ang) {
        super(ang);
    }

    /* renamed from: V */
    static void m7270V() {
        serialVersionUID = (long) 1;
        _m_fieldCount = BaseTaikodomContent._m_fieldCount + 8;
        _m_methodCount = BaseTaikodomContent._m_methodCount + 18;
        int i = BaseTaikodomContent._m_fieldCount;
        C5663aRz[] arzArr = new C5663aRz[(i + 8)];
        C5663aRz b = C5640aRc.m17844b(CruiseSpeedType.class, "9c1368671cbe4d1a2930c900c7b800a5", i);
        bnU = b;
        arzArr[i] = b;
        int i2 = i + 1;
        C5663aRz b2 = C5640aRc.m17844b(CruiseSpeedType.class, "3afdaafb44bd49243d26507d75d133f0", i2);
        bnV = b2;
        arzArr[i2] = b2;
        int i3 = i2 + 1;
        C5663aRz b3 = C5640aRc.m17844b(CruiseSpeedType.class, "473d7c2655278e3ec7c240e1cff91438", i3);
        bnO = b3;
        arzArr[i3] = b3;
        int i4 = i3 + 1;
        C5663aRz b4 = C5640aRc.m17844b(CruiseSpeedType.class, "ac92b8d519161de5d11c422e54b62795", i4);
        bnP = b4;
        arzArr[i4] = b4;
        int i5 = i4 + 1;
        C5663aRz b5 = C5640aRc.m17844b(CruiseSpeedType.class, "d1f2f62b1bfa89ba924b0076e2221ba9", i5);
        cwC = b5;
        arzArr[i5] = b5;
        int i6 = i5 + 1;
        C5663aRz b6 = C5640aRc.m17844b(CruiseSpeedType.class, "3e49da931377a12ddc4330c4dbc6e46a", i6);
        cwA = b6;
        arzArr[i6] = b6;
        int i7 = i6 + 1;
        C5663aRz b7 = C5640aRc.m17844b(CruiseSpeedType.class, "1fb7ebcd6a1ffdb4c6d93d7a9b8ad2d6", i7);
        dGv = b7;
        arzArr[i7] = b7;
        int i8 = i7 + 1;
        C5663aRz b8 = C5640aRc.m17844b(CruiseSpeedType.class, "f7ae2006ce5840a7b4ea8519c5e8719d", i8);
        dGx = b8;
        arzArr[i8] = b8;
        int i9 = i8 + 1;
        _m_fields = arzArr;
        C1634Xv.m11725a((Object[]) BaseTaikodomContent._m_fields, (Object[]) _m_fields);
        int i10 = BaseTaikodomContent._m_methodCount;
        C2491fm[] fmVarArr = new C2491fm[(i10 + 18)];
        C2491fm a = C4105zY.m41624a(CruiseSpeedType.class, "5eac9c8cc3e7525b65ce1640b085cb13", i10);
        aTg = a;
        fmVarArr[i10] = a;
        int i11 = i10 + 1;
        C2491fm a2 = C4105zY.m41624a(CruiseSpeedType.class, "dac5f54a0083c2afd0443fd3542a58a8", i11);
        dGy = a2;
        fmVarArr[i11] = a2;
        int i12 = i11 + 1;
        C2491fm a3 = C4105zY.m41624a(CruiseSpeedType.class, "a372337ccf37a78e2e2d8de1d32b3de0", i12);
        aTh = a3;
        fmVarArr[i12] = a3;
        int i13 = i12 + 1;
        C2491fm a4 = C4105zY.m41624a(CruiseSpeedType.class, "7d6201c65bfb9c1300aafec929449ba8", i13);
        dGz = a4;
        fmVarArr[i13] = a4;
        int i14 = i13 + 1;
        C2491fm a5 = C4105zY.m41624a(CruiseSpeedType.class, "198f08f1d28513b3229d5ec013d29f14", i14);
        aTe = a5;
        fmVarArr[i14] = a5;
        int i15 = i14 + 1;
        C2491fm a6 = C4105zY.m41624a(CruiseSpeedType.class, "ee22ce8ff0c861679103f83f95d9cd8a", i15);
        dGA = a6;
        fmVarArr[i15] = a6;
        int i16 = i15 + 1;
        C2491fm a7 = C4105zY.m41624a(CruiseSpeedType.class, "943de8792eb86287b6fce3cadf92e0f3", i16);
        aTf = a7;
        fmVarArr[i16] = a7;
        int i17 = i16 + 1;
        C2491fm a8 = C4105zY.m41624a(CruiseSpeedType.class, "de58b7a76a7c2abb2e198c751076ae44", i17);
        dGB = a8;
        fmVarArr[i17] = a8;
        int i18 = i17 + 1;
        C2491fm a9 = C4105zY.m41624a(CruiseSpeedType.class, "35fc2ddb508aed3c4bb6cc9735b4b673", i18);
        bFx = a9;
        fmVarArr[i18] = a9;
        int i19 = i18 + 1;
        C2491fm a10 = C4105zY.m41624a(CruiseSpeedType.class, "101e0d4fe95fcca91fbd070e8f90a7cf", i19);
        dGC = a10;
        fmVarArr[i19] = a10;
        int i20 = i19 + 1;
        C2491fm a11 = C4105zY.m41624a(CruiseSpeedType.class, "fa8aeeaa8715631b0bf2effe4b19cc3a", i20);
        bFy = a11;
        fmVarArr[i20] = a11;
        int i21 = i20 + 1;
        C2491fm a12 = C4105zY.m41624a(CruiseSpeedType.class, "2368464dc4d45250b034ad37d12abfb4", i21);
        dGD = a12;
        fmVarArr[i21] = a12;
        int i22 = i21 + 1;
        C2491fm a13 = C4105zY.m41624a(CruiseSpeedType.class, "946feaf25a18562e93b05fb9bdf125b0", i22);
        dGE = a13;
        fmVarArr[i22] = a13;
        int i23 = i22 + 1;
        C2491fm a14 = C4105zY.m41624a(CruiseSpeedType.class, "02682a9f448f55b2dc64fa9a8e82654e", i23);
        dGF = a14;
        fmVarArr[i23] = a14;
        int i24 = i23 + 1;
        C2491fm a15 = C4105zY.m41624a(CruiseSpeedType.class, "80349c14f91d98a741e5233246bbdcdb", i24);
        dGG = a15;
        fmVarArr[i24] = a15;
        int i25 = i24 + 1;
        C2491fm a16 = C4105zY.m41624a(CruiseSpeedType.class, "403d52fe10995a1d0e7c1ebb36c41e0e", i25);
        dGH = a16;
        fmVarArr[i25] = a16;
        int i26 = i25 + 1;
        C2491fm a17 = C4105zY.m41624a(CruiseSpeedType.class, "684f5cb1d9f3ffd041038f84df97673e", i26);
        dGI = a17;
        fmVarArr[i26] = a17;
        int i27 = i26 + 1;
        C2491fm a18 = C4105zY.m41624a(CruiseSpeedType.class, "753cd754348a2add5d8f1cbbf4c6c056", i27);
        f1203dN = a18;
        fmVarArr[i27] = a18;
        int i28 = i27 + 1;
        _m_methods = fmVarArr;
        C1634Xv.m11725a((Object[]) BaseTaikodomContent._m_methods, (Object[]) _m_methods);
        ___iScriptClass = aUO.m18566a(CruiseSpeedType.class, C5467aKl.class, _m_fields, _m_methods);
    }

    /* renamed from: aJ */
    private void m7275aJ(Asset tCVar) {
        bFf().mo5608dq().mo3197f(dGv, tCVar);
    }

    /* renamed from: aK */
    private void m7276aK(Asset tCVar) {
        bFf().mo5608dq().mo3197f(dGx, tCVar);
    }

    private float aeO() {
        return bFf().mo5608dq().mo3211m(bnO);
    }

    private float aeP() {
        return bFf().mo5608dq().mo3211m(bnP);
    }

    private float aeS() {
        return bFf().mo5608dq().mo3211m(bnU);
    }

    private float aeT() {
        return bFf().mo5608dq().mo3211m(bnV);
    }

    private float bjP() {
        return bFf().mo5608dq().mo3211m(cwC);
    }

    private float bjQ() {
        return bFf().mo5608dq().mo3211m(cwA);
    }

    private Asset bjR() {
        return (Asset) bFf().mo5608dq().mo3214p(dGv);
    }

    private Asset bjS() {
        return (Asset) bFf().mo5608dq().mo3214p(dGx);
    }

    /* renamed from: dt */
    private void m7280dt(float f) {
        bFf().mo5608dq().mo3150a(bnO, f);
    }

    /* renamed from: du */
    private void m7281du(float f) {
        bFf().mo5608dq().mo3150a(bnP, f);
    }

    /* renamed from: dx */
    private void m7282dx(float f) {
        bFf().mo5608dq().mo3150a(bnU, f);
    }

    /* renamed from: dy */
    private void m7283dy(float f) {
        bFf().mo5608dq().mo3150a(bnV, f);
    }

    /* renamed from: gy */
    private void m7290gy(float f) {
        bFf().mo5608dq().mo3150a(cwC, f);
    }

    /* renamed from: gz */
    private void m7291gz(float f) {
        bFf().mo5608dq().mo3150a(cwA, f);
    }

    /* renamed from: T */
    public C6494any mo11T() {
        return ___iScriptClass;
    }

    /* renamed from: U */
    public C2491fm[] mo12U() {
        return _m_methods;
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Max Angular Acceleration")
    /* renamed from: VF */
    public float mo4127VF() {
        switch (bFf().mo6893i(aTe)) {
            case 0:
                return 0.0f;
            case 2:
                return ((Float) bFf().mo5606d(new aCE(this, aTe, new Object[0]))).floatValue();
            case 3:
                bFf().mo5606d(new aCE(this, aTe, new Object[0]));
                break;
        }
        return m7271VE();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Max Angular Velocity")
    /* renamed from: VH */
    public float mo4128VH() {
        switch (bFf().mo6893i(aTf)) {
            case 0:
                return 0.0f;
            case 2:
                return ((Float) bFf().mo5606d(new aCE(this, aTf, new Object[0]))).floatValue();
            case 3:
                bFf().mo5606d(new aCE(this, aTf, new Object[0]));
                break;
        }
        return m7272VG();
    }

    /* renamed from: W */
    public Object mo13W() {
        return new C5467aKl(this);
    }

    /* renamed from: a */
    public Object mo14a(C0495Gr gr) {
        Object[] args = gr.getArgs();
        switch (gr.mo2417hq() - BaseTaikodomContent._m_methodCount) {
            case 0:
                return new Float(m7273VI());
            case 1:
                m7284gA(((Float) args[0]).floatValue());
                return null;
            case 2:
                return new Float(m7274VJ());
            case 3:
                m7285gB(((Float) args[0]).floatValue());
                return null;
            case 4:
                return new Float(m7271VE());
            case 5:
                m7286gC(((Float) args[0]).floatValue());
                return null;
            case 6:
                return new Float(m7272VG());
            case 7:
                m7287gD(((Float) args[0]).floatValue());
                return null;
            case 8:
                return new Float(anq());
            case 9:
                m7288gE(((Float) args[0]).floatValue());
                return null;
            case 10:
                return new Float(ans());
            case 11:
                m7289gG(((Float) args[0]).floatValue());
                return null;
            case 12:
                return bjT();
            case 13:
                m7277aL((Asset) args[0]);
                return null;
            case 14:
                return bjV();
            case 15:
                m7278aN((Asset) args[0]);
                return null;
            case 16:
                return bjX();
            case 17:
                return m7279aT();
            default:
                return super.mo14a(gr);
        }
    }

    /* renamed from: a */
    public void mo15a(Collection collection, C0495Gr gr) {
        super.mo15a(collection, gr);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Activate S F X")
    /* renamed from: aM */
    public void mo4129aM(Asset tCVar) {
        switch (bFf().mo6893i(dGF)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, dGF, new Object[]{tCVar}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, dGF, new Object[]{tCVar}));
                break;
        }
        m7277aL(tCVar);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Deactivate S F X")
    /* renamed from: aO */
    public void mo4130aO(Asset tCVar) {
        switch (bFf().mo6893i(dGH)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, dGH, new Object[]{tCVar}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, dGH, new Object[]{tCVar}));
                break;
        }
        m7278aN(tCVar);
    }

    /* access modifiers changed from: protected */
    /* renamed from: aU */
    public <T extends aDJ> T mo745aU() {
        switch (bFf().mo6893i(f1203dN)) {
            case 0:
                return null;
            case 2:
                return (aDJ) bFf().mo5606d(new aCE(this, f1203dN, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f1203dN, new Object[0]));
                break;
        }
        return m7279aT();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Warmup Time")
    public float anr() {
        switch (bFf().mo6893i(bFx)) {
            case 0:
                return 0.0f;
            case 2:
                return ((Float) bFf().mo5606d(new aCE(this, bFx, new Object[0]))).floatValue();
            case 3:
                bFf().mo5606d(new aCE(this, bFx, new Object[0]));
                break;
        }
        return anq();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Cooldown Time")
    public float ant() {
        switch (bFf().mo6893i(bFy)) {
            case 0:
                return 0.0f;
            case 2:
                return ((Float) bFf().mo5606d(new aCE(this, bFy, new Object[0]))).floatValue();
            case 3:
                bFf().mo5606d(new aCE(this, bFy, new Object[0]));
                break;
        }
        return ans();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Max Angular Velocity")
    /* renamed from: as */
    public void mo4133as(float f) {
        switch (bFf().mo6893i(dGB)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, dGB, new Object[]{new Float(f)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, dGB, new Object[]{new Float(f)}));
                break;
        }
        m7287gD(f);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Max Angular Acceleration")
    /* renamed from: at */
    public void mo4134at(float f) {
        switch (bFf().mo6893i(dGA)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, dGA, new Object[]{new Float(f)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, dGA, new Object[]{new Float(f)}));
                break;
        }
        m7286gC(f);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Max Linear Acceleration")
    /* renamed from: au */
    public void mo4135au(float f) {
        switch (bFf().mo6893i(dGy)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, dGy, new Object[]{new Float(f)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, dGy, new Object[]{new Float(f)}));
                break;
        }
        m7284gA(f);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Max Linear Velocity")
    /* renamed from: av */
    public void mo4136av(float f) {
        switch (bFf().mo6893i(dGz)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, dGz, new Object[]{new Float(f)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, dGz, new Object[]{new Float(f)}));
                break;
        }
        m7285gB(f);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Activate S F X")
    public Asset bjU() {
        switch (bFf().mo6893i(dGE)) {
            case 0:
                return null;
            case 2:
                return (Asset) bFf().mo5606d(new aCE(this, dGE, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, dGE, new Object[0]));
                break;
        }
        return bjT();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Deactivate S F X")
    public Asset bjW() {
        switch (bFf().mo6893i(dGG)) {
            case 0:
                return null;
            case 2:
                return (Asset) bFf().mo5606d(new aCE(this, dGG, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, dGG, new Object[0]));
                break;
        }
        return bjV();
    }

    public CruiseSpeed bjY() {
        switch (bFf().mo6893i(dGI)) {
            case 0:
                return null;
            case 2:
                return (CruiseSpeed) bFf().mo5606d(new aCE(this, dGI, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, dGI, new Object[0]));
                break;
        }
        return bjX();
    }

    /* renamed from: c */
    public C5663aRz[] mo25c() {
        return _m_fields;
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Warmup Time")
    /* renamed from: gF */
    public void mo4140gF(float f) {
        switch (bFf().mo6893i(dGC)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, dGC, new Object[]{new Float(f)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, dGC, new Object[]{new Float(f)}));
                break;
        }
        m7288gE(f);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Cooldown Time")
    /* renamed from: gH */
    public void mo4141gH(float f) {
        switch (bFf().mo6893i(dGD)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, dGD, new Object[]{new Float(f)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, dGD, new Object[]{new Float(f)}));
                break;
        }
        m7289gG(f);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Max Linear Velocity")
    /* renamed from: ra */
    public float mo4142ra() {
        switch (bFf().mo6893i(aTh)) {
            case 0:
                return 0.0f;
            case 2:
                return ((Float) bFf().mo5606d(new aCE(this, aTh, new Object[0]))).floatValue();
            case 3:
                bFf().mo5606d(new aCE(this, aTh, new Object[0]));
                break;
        }
        return m7274VJ();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Max Linear Acceleration")
    /* renamed from: rb */
    public float mo4143rb() {
        switch (bFf().mo6893i(aTg)) {
            case 0:
                return 0.0f;
            case 2:
                return ((Float) bFf().mo5606d(new aCE(this, aTg, new Object[0]))).floatValue();
            case 3:
                bFf().mo5606d(new aCE(this, aTg, new Object[0]));
                break;
        }
        return m7273VI();
    }

    /* renamed from: S */
    public void mo10S() {
        super.mo10S();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Max Linear Acceleration")
    @C0064Am(aul = "5eac9c8cc3e7525b65ce1640b085cb13", aum = 0)
    /* renamed from: VI */
    private float m7273VI() {
        return aeS();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Max Linear Acceleration")
    @C0064Am(aul = "dac5f54a0083c2afd0443fd3542a58a8", aum = 0)
    /* renamed from: gA */
    private void m7284gA(float f) {
        m7282dx(f);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Max Linear Velocity")
    @C0064Am(aul = "a372337ccf37a78e2e2d8de1d32b3de0", aum = 0)
    /* renamed from: VJ */
    private float m7274VJ() {
        return aeT();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Max Linear Velocity")
    @C0064Am(aul = "7d6201c65bfb9c1300aafec929449ba8", aum = 0)
    /* renamed from: gB */
    private void m7285gB(float f) {
        m7283dy(f);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Max Angular Acceleration")
    @C0064Am(aul = "198f08f1d28513b3229d5ec013d29f14", aum = 0)
    /* renamed from: VE */
    private float m7271VE() {
        return aeO();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Max Angular Acceleration")
    @C0064Am(aul = "ee22ce8ff0c861679103f83f95d9cd8a", aum = 0)
    /* renamed from: gC */
    private void m7286gC(float f) {
        m7280dt(f);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Max Angular Velocity")
    @C0064Am(aul = "943de8792eb86287b6fce3cadf92e0f3", aum = 0)
    /* renamed from: VG */
    private float m7272VG() {
        return aeP();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Max Angular Velocity")
    @C0064Am(aul = "de58b7a76a7c2abb2e198c751076ae44", aum = 0)
    /* renamed from: gD */
    private void m7287gD(float f) {
        m7281du(f);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Warmup Time")
    @C0064Am(aul = "35fc2ddb508aed3c4bb6cc9735b4b673", aum = 0)
    private float anq() {
        return bjP();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Warmup Time")
    @C0064Am(aul = "101e0d4fe95fcca91fbd070e8f90a7cf", aum = 0)
    /* renamed from: gE */
    private void m7288gE(float f) {
        m7290gy(f);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Cooldown Time")
    @C0064Am(aul = "fa8aeeaa8715631b0bf2effe4b19cc3a", aum = 0)
    private float ans() {
        return bjQ();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Cooldown Time")
    @C0064Am(aul = "2368464dc4d45250b034ad37d12abfb4", aum = 0)
    /* renamed from: gG */
    private void m7289gG(float f) {
        m7291gz(f);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Activate S F X")
    @C0064Am(aul = "946feaf25a18562e93b05fb9bdf125b0", aum = 0)
    private Asset bjT() {
        return bjR();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Activate S F X")
    @C0064Am(aul = "02682a9f448f55b2dc64fa9a8e82654e", aum = 0)
    /* renamed from: aL */
    private void m7277aL(Asset tCVar) {
        m7275aJ(tCVar);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Deactivate S F X")
    @C0064Am(aul = "80349c14f91d98a741e5233246bbdcdb", aum = 0)
    private Asset bjV() {
        return bjS();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Deactivate S F X")
    @C0064Am(aul = "403d52fe10995a1d0e7c1ebb36c41e0e", aum = 0)
    /* renamed from: aN */
    private void m7278aN(Asset tCVar) {
        m7276aK(tCVar);
    }

    @C0064Am(aul = "684f5cb1d9f3ffd041038f84df97673e", aum = 0)
    private CruiseSpeed bjX() {
        return (CruiseSpeed) mo745aU();
    }

    @C0064Am(aul = "753cd754348a2add5d8f1cbbf4c6c056", aum = 0)
    /* renamed from: aT */
    private <T extends aDJ> T m7279aT() {
        T t = (CruiseSpeed) bFf().mo6865M(CruiseSpeed.class);
        t.mo18830h(this);
        return t;
    }
}
