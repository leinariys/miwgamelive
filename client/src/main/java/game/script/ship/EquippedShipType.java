package game.script.ship;

import game.network.message.externalizable.aCE;
import game.network.message.serializable.C3438ra;
import game.script.TaikodomObject;
import game.script.item.AmplifierType;
import game.script.item.ClipType;
import game.script.item.WeaponType;
import logic.baa.*;
import logic.data.mbean.C2824ke;
import logic.res.code.C5640aRc;
import logic.res.code.C5663aRz;
import logic.res.code.C6494any;
import logic.res.html.C2491fm;
import p001a.*;

import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.UUID;

@C5511aMd
@C6485anp
/* renamed from: a.gf */
/* compiled from: a */
public class EquippedShipType extends TaikodomObject implements C0468GU, C1616Xf {
    /* renamed from: OW */
    public static final C5663aRz f7689OW = null;
    /* renamed from: OY */
    public static final C5663aRz f7691OY = null;
    /* renamed from: Pa */
    public static final C5663aRz f7693Pa = null;
    /* renamed from: Pc */
    public static final C5663aRz f7695Pc = null;
    /* renamed from: Pe */
    public static final C5663aRz f7697Pe = null;
    /* renamed from: Pf */
    public static final C2491fm f7698Pf = null;
    /* renamed from: Pg */
    public static final C2491fm f7699Pg = null;
    /* renamed from: Ph */
    public static final C2491fm f7700Ph = null;
    /* renamed from: Pi */
    public static final C2491fm f7701Pi = null;
    /* renamed from: Pj */
    public static final C2491fm f7702Pj = null;
    /* renamed from: Pk */
    public static final C2491fm f7703Pk = null;
    /* renamed from: Pl */
    public static final C2491fm f7704Pl = null;
    /* renamed from: Pm */
    public static final C2491fm f7705Pm = null;
    /* renamed from: Pn */
    public static final C2491fm f7706Pn = null;
    /* renamed from: Po */
    public static final C2491fm f7707Po = null;
    /* renamed from: Pp */
    public static final C2491fm f7708Pp = null;
    /* renamed from: Pq */
    public static final C2491fm f7709Pq = null;
    /* renamed from: Pr */
    public static final C2491fm f7710Pr = null;
    /* renamed from: Ps */
    public static final C2491fm f7711Ps = null;
    /* renamed from: Pt */
    public static final C2491fm f7712Pt = null;
    /* renamed from: _f_getShipType_0020_0028_0029Ltaikodom_002fgame_002fscript_002fship_002fShipType_003b */
    public static final C2491fm f7713xe514b437 = null;
    public static final C2491fm _f_toString_0020_0028_0029Ljava_002flang_002fString_003b = null;
    public static final int _m_fieldCount = 0;
    public static final C5663aRz[] _m_fields = null;
    public static final int _m_methodCount = 0;
    public static final C2491fm[] _m_methods = null;
    /* renamed from: bL */
    public static final C5663aRz f7715bL = null;
    /* renamed from: bM */
    public static final C5663aRz f7716bM = null;
    /* renamed from: bN */
    public static final C2491fm f7717bN = null;
    /* renamed from: bO */
    public static final C2491fm f7718bO = null;
    /* renamed from: bP */
    public static final C2491fm f7719bP = null;
    /* renamed from: bQ */
    public static final C2491fm f7720bQ = null;
    public static final long serialVersionUID = 0;
    /* renamed from: zQ */
    public static final C5663aRz f7721zQ = null;
    public static C6494any ___iScriptClass;
    @C0064Am(aul = "709479cb9121104b0edaa444efd36f5b", aum = 1)

    /* renamed from: OV */
    private static ShipType f7688OV;
    @C0064Am(aul = "8170cc2787e22207846ab7b60d74320f", aum = 2)

    /* renamed from: OX */
    private static C3438ra<WeaponType> f7690OX;
    @C0064Am(aul = "cd72dbbd0242d34a631da3b24eab191f", aum = 3)

    /* renamed from: OZ */
    private static C3438ra<AmplifierType> f7692OZ;
    @C0064Am(aul = "fa17f3291a0b83ff793820fb896414e6", aum = 4)

    /* renamed from: Pb */
    private static C3438ra<ClipType> f7694Pb;
    @C0064Am(aul = "c847d174a9c63c60c2c7e006a3490b77", aum = 5)

    /* renamed from: Pd */
    private static float f7696Pd;
    @C0064Am(aul = "6c589ceca2d4af163da9464c640f53cf", aum = 7)

    /* renamed from: bK */
    private static UUID f7714bK;
    @C0064Am(aul = "5ede074e8c7210b245cd5c28ea851748", aum = 0)
    private static String handle;
    @C0064Am(aul = "17b30b6acadb92ea46f145ddc0017a84", aum = 6)
    private static String name;

    static {
        m32132V();
    }

    public EquippedShipType() {
        super((C5540aNg) null);
        super.mo10S();
    }

    public EquippedShipType(C5540aNg ang) {
        super(ang);
    }

    /* renamed from: V */
    static void m32132V() {
        serialVersionUID = (long) 1;
        _m_fieldCount = TaikodomObject._m_fieldCount + 8;
        _m_methodCount = TaikodomObject._m_methodCount + 21;
        int i = TaikodomObject._m_fieldCount;
        C5663aRz[] arzArr = new C5663aRz[(i + 8)];
        C5663aRz b = C5640aRc.m17844b(EquippedShipType.class, "5ede074e8c7210b245cd5c28ea851748", i);
        f7716bM = b;
        arzArr[i] = b;
        int i2 = i + 1;
        C5663aRz b2 = C5640aRc.m17844b(EquippedShipType.class, "709479cb9121104b0edaa444efd36f5b", i2);
        f7689OW = b2;
        arzArr[i2] = b2;
        int i3 = i2 + 1;
        C5663aRz b3 = C5640aRc.m17844b(EquippedShipType.class, "8170cc2787e22207846ab7b60d74320f", i3);
        f7691OY = b3;
        arzArr[i3] = b3;
        int i4 = i3 + 1;
        C5663aRz b4 = C5640aRc.m17844b(EquippedShipType.class, "cd72dbbd0242d34a631da3b24eab191f", i4);
        f7693Pa = b4;
        arzArr[i4] = b4;
        int i5 = i4 + 1;
        C5663aRz b5 = C5640aRc.m17844b(EquippedShipType.class, "fa17f3291a0b83ff793820fb896414e6", i5);
        f7695Pc = b5;
        arzArr[i5] = b5;
        int i6 = i5 + 1;
        C5663aRz b6 = C5640aRc.m17844b(EquippedShipType.class, "c847d174a9c63c60c2c7e006a3490b77", i6);
        f7697Pe = b6;
        arzArr[i6] = b6;
        int i7 = i6 + 1;
        C5663aRz b7 = C5640aRc.m17844b(EquippedShipType.class, "17b30b6acadb92ea46f145ddc0017a84", i7);
        f7721zQ = b7;
        arzArr[i7] = b7;
        int i8 = i7 + 1;
        C5663aRz b8 = C5640aRc.m17844b(EquippedShipType.class, "6c589ceca2d4af163da9464c640f53cf", i8);
        f7715bL = b8;
        arzArr[i8] = b8;
        int i9 = i8 + 1;
        _m_fields = arzArr;
        C1634Xv.m11725a((Object[]) TaikodomObject._m_fields, (Object[]) _m_fields);
        int i10 = TaikodomObject._m_methodCount;
        C2491fm[] fmVarArr = new C2491fm[(i10 + 21)];
        C2491fm a = C4105zY.m41624a(EquippedShipType.class, "3c388f40c6e157c9de00de5ac6397b1a", i10);
        f7717bN = a;
        fmVarArr[i10] = a;
        int i11 = i10 + 1;
        C2491fm a2 = C4105zY.m41624a(EquippedShipType.class, "c829be2521cc8b0492f3c4da2805d4d7", i11);
        f7718bO = a2;
        fmVarArr[i11] = a2;
        int i12 = i11 + 1;
        C2491fm a3 = C4105zY.m41624a(EquippedShipType.class, "9af289a445ed2854ac14e61dd071c705", i12);
        f7719bP = a3;
        fmVarArr[i12] = a3;
        int i13 = i12 + 1;
        C2491fm a4 = C4105zY.m41624a(EquippedShipType.class, "838f9a2e540375f1253d2c71b7bbdfe1", i13);
        f7720bQ = a4;
        fmVarArr[i13] = a4;
        int i14 = i13 + 1;
        C2491fm a5 = C4105zY.m41624a(EquippedShipType.class, "e89882e0d81285abd7de0dcdceb2355b", i14);
        f7698Pf = a5;
        fmVarArr[i14] = a5;
        int i15 = i14 + 1;
        C2491fm a6 = C4105zY.m41624a(EquippedShipType.class, "aebe249e4ae508d73fa44e841b9f91ec", i15);
        f7699Pg = a6;
        fmVarArr[i15] = a6;
        int i16 = i15 + 1;
        C2491fm a7 = C4105zY.m41624a(EquippedShipType.class, "5ad1bc0b35c42553d87297a9259b59cd", i16);
        f7713xe514b437 = a7;
        fmVarArr[i16] = a7;
        int i17 = i16 + 1;
        C2491fm a8 = C4105zY.m41624a(EquippedShipType.class, "b50f729bf72b5039a513a2429af2e5cf", i17);
        f7700Ph = a8;
        fmVarArr[i17] = a8;
        int i18 = i17 + 1;
        C2491fm a9 = C4105zY.m41624a(EquippedShipType.class, "0226250947591937fe1b3906725ce32d", i18);
        f7701Pi = a9;
        fmVarArr[i18] = a9;
        int i19 = i18 + 1;
        C2491fm a10 = C4105zY.m41624a(EquippedShipType.class, "b186a3ee98eed33264669a224159754f", i19);
        f7702Pj = a10;
        fmVarArr[i19] = a10;
        int i20 = i19 + 1;
        C2491fm a11 = C4105zY.m41624a(EquippedShipType.class, "3ab8199a8dd063dcd47355373e9ad362", i20);
        f7703Pk = a11;
        fmVarArr[i20] = a11;
        int i21 = i20 + 1;
        C2491fm a12 = C4105zY.m41624a(EquippedShipType.class, "e4c2e8d5dce02c5ad665ae369ae53f39", i21);
        f7704Pl = a12;
        fmVarArr[i21] = a12;
        int i22 = i21 + 1;
        C2491fm a13 = C4105zY.m41624a(EquippedShipType.class, "a8508128004c5fdeb46e1ad43b581931", i22);
        f7705Pm = a13;
        fmVarArr[i22] = a13;
        int i23 = i22 + 1;
        C2491fm a14 = C4105zY.m41624a(EquippedShipType.class, "5d9e2d4b4ab10b3f7772ab2df84844e4", i23);
        f7706Pn = a14;
        fmVarArr[i23] = a14;
        int i24 = i23 + 1;
        C2491fm a15 = C4105zY.m41624a(EquippedShipType.class, "8ad4fd08ccacfa3ee1e36a72fea700c1", i24);
        f7707Po = a15;
        fmVarArr[i24] = a15;
        int i25 = i24 + 1;
        C2491fm a16 = C4105zY.m41624a(EquippedShipType.class, "609c779847226b734455056c996cc236", i25);
        f7708Pp = a16;
        fmVarArr[i25] = a16;
        int i26 = i25 + 1;
        C2491fm a17 = C4105zY.m41624a(EquippedShipType.class, "33dde43255e1b1e558a2b2e73ed78bdd", i26);
        f7709Pq = a17;
        fmVarArr[i26] = a17;
        int i27 = i26 + 1;
        C2491fm a18 = C4105zY.m41624a(EquippedShipType.class, "3216741e74ac77fdf99976f787e9bebd", i27);
        f7710Pr = a18;
        fmVarArr[i27] = a18;
        int i28 = i27 + 1;
        C2491fm a19 = C4105zY.m41624a(EquippedShipType.class, "3618ba9f69ecf74453ae2a19616c116b", i28);
        _f_toString_0020_0028_0029Ljava_002flang_002fString_003b = a19;
        fmVarArr[i28] = a19;
        int i29 = i28 + 1;
        C2491fm a20 = C4105zY.m41624a(EquippedShipType.class, "684e83ad55ea88d0a61d80508188db4d", i29);
        f7711Ps = a20;
        fmVarArr[i29] = a20;
        int i30 = i29 + 1;
        C2491fm a21 = C4105zY.m41624a(EquippedShipType.class, "ffbd0a27188d7710ddb9e14c3231da34", i30);
        f7712Pt = a21;
        fmVarArr[i30] = a21;
        int i31 = i30 + 1;
        _m_methods = fmVarArr;
        C1634Xv.m11725a((Object[]) TaikodomObject._m_methods, (Object[]) _m_methods);
        ___iScriptClass = aUO.m18566a(EquippedShipType.class, C2824ke.class, _m_fields, _m_methods);
    }

    /* renamed from: Z */
    private void m32133Z(float f) {
        bFf().mo5608dq().mo3150a(f7697Pe, f);
    }

    /* renamed from: a */
    private void m32134a(ShipType ng) {
        bFf().mo5608dq().mo3197f(f7689OW, ng);
    }

    /* renamed from: a */
    private void m32138a(String str) {
        bFf().mo5608dq().mo3197f(f7716bM, str);
    }

    /* renamed from: a */
    private void m32139a(UUID uuid) {
        bFf().mo5608dq().mo3197f(f7715bL, uuid);
    }

    /* renamed from: an */
    private UUID m32141an() {
        return (UUID) bFf().mo5608dq().mo3214p(f7715bL);
    }

    /* renamed from: ao */
    private String m32142ao() {
        return (String) bFf().mo5608dq().mo3214p(f7716bM);
    }

    /* renamed from: ap */
    private void m32144ap(String str) {
        bFf().mo5608dq().mo3197f(f7721zQ, str);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Name")
    @C0064Am(aul = "aebe249e4ae508d73fa44e841b9f91ec", aum = 0)
    @C5566aOg
    /* renamed from: aq */
    private void m32145aq(String str) {
        throw new aWi(new aCE(this, f7699Pg, new Object[]{str}));
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Handle")
    @C0064Am(aul = "838f9a2e540375f1253d2c71b7bbdfe1", aum = 0)
    @C5566aOg
    /* renamed from: b */
    private void m32149b(String str) {
        throw new aWi(new aCE(this, f7720bQ, new Object[]{str}));
    }

    /* renamed from: c */
    private void m32154c(UUID uuid) {
        switch (bFf().mo6893i(f7718bO)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f7718bO, new Object[]{uuid}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f7718bO, new Object[]{uuid}));
                break;
        }
        m32150b(uuid);
    }

    /* renamed from: r */
    private void m32155r(C3438ra raVar) {
        bFf().mo5608dq().mo3197f(f7691OY, raVar);
    }

    /* renamed from: s */
    private void m32156s(C3438ra raVar) {
        bFf().mo5608dq().mo3197f(f7693Pa, raVar);
    }

    /* renamed from: t */
    private void m32157t(C3438ra raVar) {
        bFf().mo5608dq().mo3197f(f7695Pc, raVar);
    }

    /* renamed from: uP */
    private ShipType m32158uP() {
        return (ShipType) bFf().mo5608dq().mo3214p(f7689OW);
    }

    /* renamed from: uQ */
    private C3438ra m32159uQ() {
        return (C3438ra) bFf().mo5608dq().mo3214p(f7691OY);
    }

    /* renamed from: uR */
    private C3438ra m32160uR() {
        return (C3438ra) bFf().mo5608dq().mo3214p(f7693Pa);
    }

    /* renamed from: uS */
    private C3438ra m32161uS() {
        return (C3438ra) bFf().mo5608dq().mo3214p(f7695Pc);
    }

    /* renamed from: uT */
    private float m32162uT() {
        return bFf().mo5608dq().mo3211m(f7697Pe);
    }

    /* renamed from: uU */
    private String m32163uU() {
        return (String) bFf().mo5608dq().mo3214p(f7721zQ);
    }

    @C0064Am(aul = "3216741e74ac77fdf99976f787e9bebd", aum = 0)
    @C5566aOg
    /* renamed from: ve */
    private Ship m32169ve() {
        throw new aWi(new aCE(this, f7710Pr, new Object[0]));
    }

    /* renamed from: T */
    public C6494any mo11T() {
        return ___iScriptClass;
    }

    /* renamed from: U */
    public C2491fm[] mo12U() {
        return _m_methods;
    }

    /* renamed from: W */
    public Object mo13W() {
        return new C2824ke(this);
    }

    /* renamed from: a */
    public Object mo14a(C0495Gr gr) {
        Object[] args = gr.getArgs();
        switch (gr.mo2417hq() - TaikodomObject._m_methodCount) {
            case 0:
                return m32143ap();
            case 1:
                m32150b((UUID) args[0]);
                return null;
            case 2:
                return m32146ar();
            case 3:
                m32149b((String) args[0]);
                return null;
            case 4:
                return m32164uV();
            case 5:
                m32145aq((String) args[0]);
                return null;
            case 6:
                return m32165uW();
            case 7:
                m32148b((ShipType) args[0]);
                return null;
            case 8:
                return m32166uY();
            case 9:
                m32135a((WeaponType) args[0]);
                return null;
            case 10:
                m32151c((WeaponType) args[0]);
                return null;
            case 11:
                return m32167va();
            case 12:
                m32137a((AmplifierType) args[0]);
                return null;
            case 13:
                m32153c((AmplifierType) args[0]);
                return null;
            case 14:
                return m32168vc();
            case 15:
                m32136a((ClipType) args[0]);
                return null;
            case 16:
                m32152c((ClipType) args[0]);
                return null;
            case 17:
                return m32169ve();
            case 18:
                return m32147au();
            case 19:
                return new Float(m32170vg());
            case 20:
                m32140aa(((Float) args[0]).floatValue());
                return null;
            default:
                return super.mo14a(gr);
        }
    }

    /* renamed from: a */
    public void mo15a(Collection collection, C0495Gr gr) {
        super.mo15a(collection, gr);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "MP Multiplier")
    /* renamed from: ab */
    public void mo19039ab(float f) {
        switch (bFf().mo6893i(f7712Pt)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f7712Pt, new Object[]{new Float(f)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f7712Pt, new Object[]{new Float(f)}));
                break;
        }
        m32140aa(f);
    }

    /* renamed from: aq */
    public UUID mo18aq() {
        switch (bFf().mo6893i(f7717bN)) {
            case 0:
                return null;
            case 2:
                return (UUID) bFf().mo5606d(new aCE(this, f7717bN, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f7717bN, new Object[0]));
                break;
        }
        return m32143ap();
    }

    @C3248pc(aYR = C3248pc.C3250b.ADDER, displayName = "WeaponTypes")
    /* renamed from: b */
    public void mo19040b(WeaponType apt) {
        switch (bFf().mo6893i(f7702Pj)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f7702Pj, new Object[]{apt}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f7702Pj, new Object[]{apt}));
                break;
        }
        m32135a(apt);
    }

    @C3248pc(aYR = C3248pc.C3250b.ADDER, displayName = "ClipTypes")
    /* renamed from: b */
    public void mo19041b(ClipType aqf) {
        switch (bFf().mo6893i(f7708Pp)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f7708Pp, new Object[]{aqf}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f7708Pp, new Object[]{aqf}));
                break;
        }
        m32136a(aqf);
    }

    @C3248pc(aYR = C3248pc.C3250b.ADDER, displayName = "AmplifierTypes")
    /* renamed from: b */
    public void mo19042b(AmplifierType iDVar) {
        switch (bFf().mo6893i(f7705Pm)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f7705Pm, new Object[]{iDVar}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f7705Pm, new Object[]{iDVar}));
                break;
        }
        m32137a(iDVar);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "ShipType")
    /* renamed from: c */
    public void mo19043c(ShipType ng) {
        switch (bFf().mo6893i(f7700Ph)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f7700Ph, new Object[]{ng}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f7700Ph, new Object[]{ng}));
                break;
        }
        m32148b(ng);
    }

    /* renamed from: c */
    public C5663aRz[] mo25c() {
        return _m_fields;
    }

    @C3248pc(aYR = C3248pc.C3250b.REMOVER, displayName = "WeaponTypes")
    /* renamed from: d */
    public void mo19044d(WeaponType apt) {
        switch (bFf().mo6893i(f7703Pk)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f7703Pk, new Object[]{apt}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f7703Pk, new Object[]{apt}));
                break;
        }
        m32151c(apt);
    }

    @C3248pc(aYR = C3248pc.C3250b.REMOVER, displayName = "ClipTypes")
    /* renamed from: d */
    public void mo19045d(ClipType aqf) {
        switch (bFf().mo6893i(f7709Pq)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f7709Pq, new Object[]{aqf}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f7709Pq, new Object[]{aqf}));
                break;
        }
        m32152c(aqf);
    }

    @C3248pc(aYR = C3248pc.C3250b.REMOVER, displayName = "AmplifierTypes")
    /* renamed from: d */
    public void mo19046d(AmplifierType iDVar) {
        switch (bFf().mo6893i(f7706Pn)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f7706Pn, new Object[]{iDVar}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f7706Pn, new Object[]{iDVar}));
                break;
        }
        m32153c(iDVar);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Handle")
    public String getHandle() {
        switch (bFf().mo6893i(f7719bP)) {
            case 0:
                return null;
            case 2:
                return (String) bFf().mo5606d(new aCE(this, f7719bP, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f7719bP, new Object[0]));
                break;
        }
        return m32146ar();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Handle")
    @C5566aOg
    public void setHandle(String str) {
        switch (bFf().mo6893i(f7720bQ)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f7720bQ, new Object[]{str}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f7720bQ, new Object[]{str}));
                break;
        }
        m32149b(str);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Name")
    public String getName() {
        switch (bFf().mo6893i(f7698Pf)) {
            case 0:
                return null;
            case 2:
                return (String) bFf().mo5606d(new aCE(this, f7698Pf, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f7698Pf, new Object[0]));
                break;
        }
        return m32164uV();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Name")
    @C5566aOg
    public void setName(String str) {
        switch (bFf().mo6893i(f7699Pg)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f7699Pg, new Object[]{str}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f7699Pg, new Object[]{str}));
                break;
        }
        m32145aq(str);
    }

    public String toString() {
        switch (bFf().mo6893i(_f_toString_0020_0028_0029Ljava_002flang_002fString_003b)) {
            case 0:
                return null;
            case 2:
                return (String) bFf().mo5606d(new aCE(this, _f_toString_0020_0028_0029Ljava_002flang_002fString_003b, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, _f_toString_0020_0028_0029Ljava_002flang_002fString_003b, new Object[0]));
                break;
        }
        return m32147au();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "ShipType")
    /* renamed from: uX */
    public ShipType mo19050uX() {
        switch (bFf().mo6893i(f7713xe514b437)) {
            case 0:
                return null;
            case 2:
                return (ShipType) bFf().mo5606d(new aCE(this, f7713xe514b437, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f7713xe514b437, new Object[0]));
                break;
        }
        return m32165uW();
    }

    @C3248pc(aYR = C3248pc.C3250b.ITERATOR, displayName = "WeaponTypes")
    /* renamed from: uZ */
    public List<WeaponType> mo19051uZ() {
        switch (bFf().mo6893i(f7701Pi)) {
            case 0:
                return null;
            case 2:
                return (List) bFf().mo5606d(new aCE(this, f7701Pi, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f7701Pi, new Object[0]));
                break;
        }
        return m32166uY();
    }

    @C3248pc(aYR = C3248pc.C3250b.ITERATOR, displayName = "AmplifierTypes")
    /* renamed from: vb */
    public List<AmplifierType> mo19052vb() {
        switch (bFf().mo6893i(f7704Pl)) {
            case 0:
                return null;
            case 2:
                return (List) bFf().mo5606d(new aCE(this, f7704Pl, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f7704Pl, new Object[0]));
                break;
        }
        return m32167va();
    }

    @C3248pc(aYR = C3248pc.C3250b.ITERATOR, displayName = "ClipTypes")
    /* renamed from: vd */
    public List<ClipType> mo19053vd() {
        switch (bFf().mo6893i(f7707Po)) {
            case 0:
                return null;
            case 2:
                return (List) bFf().mo5606d(new aCE(this, f7707Po, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f7707Po, new Object[0]));
                break;
        }
        return m32168vc();
    }

    @C5566aOg
    /* renamed from: vf */
    public Ship mo19054vf() {
        switch (bFf().mo6893i(f7710Pr)) {
            case 0:
                return null;
            case 2:
                return (Ship) bFf().mo5606d(new aCE(this, f7710Pr, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f7710Pr, new Object[0]));
                break;
        }
        return m32169ve();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "MP Multiplier")
    /* renamed from: vh */
    public float mo19055vh() {
        switch (bFf().mo6893i(f7711Ps)) {
            case 0:
                return 0.0f;
            case 2:
                return ((Float) bFf().mo5606d(new aCE(this, f7711Ps, new Object[0]))).floatValue();
            case 3:
                bFf().mo5606d(new aCE(this, f7711Ps, new Object[0]));
                break;
        }
        return m32170vg();
    }

    @C0064Am(aul = "3c388f40c6e157c9de00de5ac6397b1a", aum = 0)
    /* renamed from: ap */
    private UUID m32143ap() {
        return m32141an();
    }

    @C0064Am(aul = "c829be2521cc8b0492f3c4da2805d4d7", aum = 0)
    /* renamed from: b */
    private void m32150b(UUID uuid) {
        m32139a(uuid);
    }

    /* renamed from: S */
    public void mo10S() {
        super.mo10S();
        setHandle(C0468GU.dac);
        m32139a(UUID.randomUUID());
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Handle")
    @C0064Am(aul = "9af289a445ed2854ac14e61dd071c705", aum = 0)
    /* renamed from: ar */
    private String m32146ar() {
        return m32142ao();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Name")
    @C0064Am(aul = "e89882e0d81285abd7de0dcdceb2355b", aum = 0)
    /* renamed from: uV */
    private String m32164uV() {
        return m32163uU();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "ShipType")
    @C0064Am(aul = "5ad1bc0b35c42553d87297a9259b59cd", aum = 0)
    /* renamed from: uW */
    private ShipType m32165uW() {
        return m32158uP();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "ShipType")
    @C0064Am(aul = "b50f729bf72b5039a513a2429af2e5cf", aum = 0)
    /* renamed from: b */
    private void m32148b(ShipType ng) {
        m32134a(ng);
    }

    @C3248pc(aYR = C3248pc.C3250b.ITERATOR, displayName = "WeaponTypes")
    @C0064Am(aul = "0226250947591937fe1b3906725ce32d", aum = 0)
    /* renamed from: uY */
    private List<WeaponType> m32166uY() {
        return Collections.unmodifiableList(m32159uQ());
    }

    @C3248pc(aYR = C3248pc.C3250b.ADDER, displayName = "WeaponTypes")
    @C0064Am(aul = "b186a3ee98eed33264669a224159754f", aum = 0)
    /* renamed from: a */
    private void m32135a(WeaponType apt) {
        m32159uQ().add(apt);
    }

    @C3248pc(aYR = C3248pc.C3250b.REMOVER, displayName = "WeaponTypes")
    @C0064Am(aul = "3ab8199a8dd063dcd47355373e9ad362", aum = 0)
    /* renamed from: c */
    private void m32151c(WeaponType apt) {
        m32159uQ().remove(apt);
    }

    @C3248pc(aYR = C3248pc.C3250b.ITERATOR, displayName = "AmplifierTypes")
    @C0064Am(aul = "e4c2e8d5dce02c5ad665ae369ae53f39", aum = 0)
    /* renamed from: va */
    private List<AmplifierType> m32167va() {
        return Collections.unmodifiableList(m32160uR());
    }

    @C3248pc(aYR = C3248pc.C3250b.ADDER, displayName = "AmplifierTypes")
    @C0064Am(aul = "a8508128004c5fdeb46e1ad43b581931", aum = 0)
    /* renamed from: a */
    private void m32137a(AmplifierType iDVar) {
        m32160uR().add(iDVar);
    }

    @C3248pc(aYR = C3248pc.C3250b.REMOVER, displayName = "AmplifierTypes")
    @C0064Am(aul = "5d9e2d4b4ab10b3f7772ab2df84844e4", aum = 0)
    /* renamed from: c */
    private void m32153c(AmplifierType iDVar) {
        m32160uR().remove(iDVar);
    }

    @C3248pc(aYR = C3248pc.C3250b.ITERATOR, displayName = "ClipTypes")
    @C0064Am(aul = "8ad4fd08ccacfa3ee1e36a72fea700c1", aum = 0)
    /* renamed from: vc */
    private List<ClipType> m32168vc() {
        return Collections.unmodifiableList(m32161uS());
    }

    @C3248pc(aYR = C3248pc.C3250b.ADDER, displayName = "ClipTypes")
    @C0064Am(aul = "609c779847226b734455056c996cc236", aum = 0)
    /* renamed from: a */
    private void m32136a(ClipType aqf) {
        m32161uS().add(aqf);
    }

    @C3248pc(aYR = C3248pc.C3250b.REMOVER, displayName = "ClipTypes")
    @C0064Am(aul = "33dde43255e1b1e558a2b2e73ed78bdd", aum = 0)
    /* renamed from: c */
    private void m32152c(ClipType aqf) {
        m32161uS().remove(aqf);
    }

    @C0064Am(aul = "3618ba9f69ecf74453ae2a19616c116b", aum = 0)
    /* renamed from: au */
    private String m32147au() {
        return "[" + m32142ao() + "]: " + m32163uU();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "MP Multiplier")
    @C0064Am(aul = "684e83ad55ea88d0a61d80508188db4d", aum = 0)
    /* renamed from: vg */
    private float m32170vg() {
        return m32162uT();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "MP Multiplier")
    @C0064Am(aul = "ffbd0a27188d7710ddb9e14c3231da34", aum = 0)
    /* renamed from: aa */
    private void m32140aa(float f) {
        m32133Z(f);
    }
}
