package game.script.ship;

import game.network.message.externalizable.aCE;
import logic.baa.C1616Xf;
import logic.baa.C1634Xv;
import logic.baa.C5511aMd;
import logic.baa.C6485anp;
import logic.data.mbean.C6407amP;
import logic.res.code.C5663aRz;
import logic.res.code.C6494any;
import logic.res.html.C2491fm;
import p001a.*;

import java.util.*;

@C5511aMd
@C6485anp
/* renamed from: a.Qt */
/* compiled from: a */
public abstract class BaseOutpostType extends StationType implements C1616Xf {
    public static final C2491fm _f_onResurrect_0020_0028_0029V = null;
    public static final int _m_fieldCount = 0;
    public static final C5663aRz[] _m_fields = null;
    public static final int _m_methodCount = 0;
    public static final C2491fm[] _m_methods = null;
    public static final C2491fm dVQ = null;
    public static final C2491fm dVR = null;
    public static final C2491fm dVS = null;
    public static final C2491fm dVT = null;
    public static final C2491fm dVU = null;
    public static final C2491fm dVV = null;
    public static final C2491fm dVW = null;
    public static final C2491fm dVX = null;
    public static final C2491fm dVY = null;
    public static final C2491fm dsT = null;
    public static final C2491fm dsU = null;
    public static final C2491fm dsV = null;
    public static final long serialVersionUID = 0;
    public static C6494any ___iScriptClass;

    static {
        m9023V();
    }

    public BaseOutpostType() {
        super((C5540aNg) null);
        super.mo10S();
    }

    public BaseOutpostType(C5540aNg ang) {
        super(ang);
    }

    /* renamed from: V */
    static void m9023V() {
        serialVersionUID = (long) 1;
        _m_fieldCount = StationType._m_fieldCount + 0;
        _m_methodCount = StationType._m_methodCount + 13;
        _m_fields = new C5663aRz[(StationType._m_fieldCount + 0)];
        C1634Xv.m11725a((Object[]) StationType._m_fields, (Object[]) _m_fields);
        int i = StationType._m_methodCount;
        C2491fm[] fmVarArr = new C2491fm[(i + 13)];
        C2491fm a = C4105zY.m41624a(BaseOutpostType.class, "ae1c0e630a2a5e70f9b40b4a1a85cbc2", i);
        dsT = a;
        fmVarArr[i] = a;
        int i2 = i + 1;
        C2491fm a2 = C4105zY.m41624a(BaseOutpostType.class, "2b597e247cf5375bea332e7ee34dd9df", i2);
        dsV = a2;
        fmVarArr[i2] = a2;
        int i3 = i2 + 1;
        C2491fm a3 = C4105zY.m41624a(BaseOutpostType.class, "1d5b6421fb652e4574c65098fd48b75d", i3);
        dsU = a3;
        fmVarArr[i3] = a3;
        int i4 = i3 + 1;
        C2491fm a4 = C4105zY.m41624a(BaseOutpostType.class, "26328f6976fb7964ac78f089f4c52d35", i4);
        dVQ = a4;
        fmVarArr[i4] = a4;
        int i5 = i4 + 1;
        C2491fm a5 = C4105zY.m41624a(BaseOutpostType.class, "e36e9c13cae87b9873167d2bc8ff5166", i5);
        dVR = a5;
        fmVarArr[i5] = a5;
        int i6 = i5 + 1;
        C2491fm a6 = C4105zY.m41624a(BaseOutpostType.class, "1d9e67cc2f65332aa894a8dc54c05142", i6);
        dVS = a6;
        fmVarArr[i6] = a6;
        int i7 = i6 + 1;
        C2491fm a7 = C4105zY.m41624a(BaseOutpostType.class, "f224767b04e819c2fcebf7785ee89f5f", i7);
        dVT = a7;
        fmVarArr[i7] = a7;
        int i8 = i7 + 1;
        C2491fm a8 = C4105zY.m41624a(BaseOutpostType.class, "81f9f1df603a63236fb396fa53892ec0", i8);
        dVU = a8;
        fmVarArr[i8] = a8;
        int i9 = i8 + 1;
        C2491fm a9 = C4105zY.m41624a(BaseOutpostType.class, "b12eca881db135377d1c685622683dd1", i9);
        dVV = a9;
        fmVarArr[i9] = a9;
        int i10 = i9 + 1;
        C2491fm a10 = C4105zY.m41624a(BaseOutpostType.class, "b9964749dbbb6d4d1774fa76650ebfa5", i10);
        dVW = a10;
        fmVarArr[i10] = a10;
        int i11 = i10 + 1;
        C2491fm a11 = C4105zY.m41624a(BaseOutpostType.class, "ae2af499e1a32ba7bd08d99fc833f53a", i11);
        dVX = a11;
        fmVarArr[i11] = a11;
        int i12 = i11 + 1;
        C2491fm a12 = C4105zY.m41624a(BaseOutpostType.class, "e72e27a470b2435572a1ef43c865cc4f", i12);
        dVY = a12;
        fmVarArr[i12] = a12;
        int i13 = i12 + 1;
        C2491fm a13 = C4105zY.m41624a(BaseOutpostType.class, "64ba74314a6fd68a6138edb8768dcd55", i13);
        _f_onResurrect_0020_0028_0029V = a13;
        fmVarArr[i13] = a13;
        int i14 = i13 + 1;
        _m_methods = fmVarArr;
        C1634Xv.m11725a((Object[]) StationType._m_methods, (Object[]) _m_methods);
        ___iScriptClass = aUO.m18566a(BaseOutpostType.class, C6407amP.class, _m_fields, _m_methods);
    }

    @C0064Am(aul = "ae1c0e630a2a5e70f9b40b4a1a85cbc2", aum = 0)
    private Map<C2611hZ, OutpostUpgrade> beO() {
        throw new aWi(new aCE(this, dsT, new Object[0]));
    }

    @C0064Am(aul = "1d5b6421fb652e4574c65098fd48b75d", aum = 0)
    private Map<C0437GE, OutpostLevelFeature> beQ() {
        throw new aWi(new aCE(this, dsU, new Object[0]));
    }

    @C0064Am(aul = "2b597e247cf5375bea332e7ee34dd9df", aum = 0)
    private Map<C2611hZ, OutpostLevelInfo> beS() {
        throw new aWi(new aCE(this, dsV, new Object[0]));
    }

    private void bpK() {
        switch (bFf().mo6893i(dVT)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, dVT, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, dVT, new Object[0]));
                break;
        }
        bpJ();
    }

    private void bpM() {
        switch (bFf().mo6893i(dVU)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, dVU, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, dVU, new Object[0]));
                break;
        }
        bpL();
    }

    private void bpO() {
        switch (bFf().mo6893i(dVV)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, dVV, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, dVV, new Object[0]));
                break;
        }
        bpN();
    }

    /* renamed from: T */
    public C6494any mo11T() {
        return ___iScriptClass;
    }

    /* renamed from: U */
    public C2491fm[] mo12U() {
        return _m_methods;
    }

    /* renamed from: W */
    public Object mo13W() {
        return new C6407amP(this);
    }

    /* renamed from: a */
    public Object mo14a(C0495Gr gr) {
        Object[] args = gr.getArgs();
        switch (gr.mo2417hq() - StationType._m_methodCount) {
            case 0:
                return beO();
            case 1:
                return beS();
            case 2:
                return beQ();
            case 3:
                return bpD();
            case 4:
                return bpF();
            case 5:
                return bpH();
            case 6:
                bpJ();
                return null;
            case 7:
                bpL();
                return null;
            case 8:
                bpN();
                return null;
            case 9:
                return m9026d((C2611hZ) args[0]);
            case 10:
                return m9027f((C2611hZ) args[0]);
            case 11:
                return m9025c((C0437GE) args[0]);
            case 12:
                m9024aG();
                return null;
            default:
                return super.mo14a(gr);
        }
    }

    /* renamed from: a */
    public void mo15a(Collection collection, C0495Gr gr) {
        super.mo15a(collection, gr);
    }

    /* renamed from: aH */
    public void mo70aH() {
        switch (bFf().mo6893i(_f_onResurrect_0020_0028_0029V)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, _f_onResurrect_0020_0028_0029V, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, _f_onResurrect_0020_0028_0029V, new Object[0]));
                break;
        }
        m9024aG();
    }

    /* access modifiers changed from: package-private */
    public Map<C2611hZ, OutpostUpgrade> beP() {
        switch (bFf().mo6893i(dsT)) {
            case 0:
                return null;
            case 2:
                return (Map) bFf().mo5606d(new aCE(this, dsT, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, dsT, new Object[0]));
                break;
        }
        return beO();
    }

    /* access modifiers changed from: package-private */
    public Map<C0437GE, OutpostLevelFeature> beR() {
        switch (bFf().mo6893i(dsU)) {
            case 0:
                return null;
            case 2:
                return (Map) bFf().mo5606d(new aCE(this, dsU, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, dsU, new Object[0]));
                break;
        }
        return beQ();
    }

    /* access modifiers changed from: package-private */
    public Map<C2611hZ, OutpostLevelInfo> beT() {
        switch (bFf().mo6893i(dsV)) {
            case 0:
                return null;
            case 2:
                return (Map) bFf().mo5606d(new aCE(this, dsV, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, dsV, new Object[0]));
                break;
        }
        return beS();
    }

    @C3248pc(aYR = C3248pc.C3250b.ITERATOR, displayName = "Outpost Upgrades")
    public List<OutpostUpgrade> bpE() {
        switch (bFf().mo6893i(dVQ)) {
            case 0:
                return null;
            case 2:
                return (List) bFf().mo5606d(new aCE(this, dVQ, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, dVQ, new Object[0]));
                break;
        }
        return bpD();
    }

    @C3248pc(aYR = C3248pc.C3250b.ITERATOR, displayName = "Outpost Level Infos")
    public List<OutpostLevelInfo> bpG() {
        switch (bFf().mo6893i(dVR)) {
            case 0:
                return null;
            case 2:
                return (List) bFf().mo5606d(new aCE(this, dVR, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, dVR, new Object[0]));
                break;
        }
        return bpF();
    }

    @C3248pc(aYR = C3248pc.C3250b.ITERATOR, displayName = "Outpost Level Features")
    public List<OutpostLevelFeature> bpI() {
        switch (bFf().mo6893i(dVS)) {
            case 0:
                return null;
            case 2:
                return (List) bFf().mo5606d(new aCE(this, dVS, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, dVS, new Object[0]));
                break;
        }
        return bpH();
    }

    /* renamed from: c */
    public C5663aRz[] mo25c() {
        return _m_fields;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: d */
    public OutpostLevelFeature mo5104d(C0437GE ge) {
        switch (bFf().mo6893i(dVY)) {
            case 0:
                return null;
            case 2:
                return (OutpostLevelFeature) bFf().mo5606d(new aCE(this, dVY, new Object[]{ge}));
            case 3:
                bFf().mo5606d(new aCE(this, dVY, new Object[]{ge}));
                break;
        }
        return m9025c(ge);
    }

    /* access modifiers changed from: package-private */
    /* renamed from: e */
    public OutpostUpgrade mo5105e(C2611hZ hZVar) {
        switch (bFf().mo6893i(dVW)) {
            case 0:
                return null;
            case 2:
                return (OutpostUpgrade) bFf().mo5606d(new aCE(this, dVW, new Object[]{hZVar}));
            case 3:
                bFf().mo5606d(new aCE(this, dVW, new Object[]{hZVar}));
                break;
        }
        return m9026d(hZVar);
    }

    /* access modifiers changed from: package-private */
    /* renamed from: g */
    public OutpostLevelInfo mo5106g(C2611hZ hZVar) {
        switch (bFf().mo6893i(dVX)) {
            case 0:
                return null;
            case 2:
                return (OutpostLevelInfo) bFf().mo5606d(new aCE(this, dVX, new Object[]{hZVar}));
            case 3:
                bFf().mo5606d(new aCE(this, dVX, new Object[]{hZVar}));
                break;
        }
        return m9027f(hZVar);
    }

    /* renamed from: S */
    public void mo10S() {
        super.mo10S();
        bpK();
        bpM();
    }

    @C3248pc(aYR = C3248pc.C3250b.ITERATOR, displayName = "Outpost Upgrades")
    @C0064Am(aul = "26328f6976fb7964ac78f089f4c52d35", aum = 0)
    private List<OutpostUpgrade> bpD() {
        return Collections.unmodifiableList(new ArrayList(beP().values()));
    }

    @C3248pc(aYR = C3248pc.C3250b.ITERATOR, displayName = "Outpost Level Infos")
    @C0064Am(aul = "e36e9c13cae87b9873167d2bc8ff5166", aum = 0)
    private List<OutpostLevelInfo> bpF() {
        return Collections.unmodifiableList(new ArrayList(beT().values()));
    }

    @C3248pc(aYR = C3248pc.C3250b.ITERATOR, displayName = "Outpost Level Features")
    @C0064Am(aul = "1d9e67cc2f65332aa894a8dc54c05142", aum = 0)
    private List<OutpostLevelFeature> bpH() {
        return Collections.unmodifiableList(new ArrayList(beR().values()));
    }

    @C0064Am(aul = "f224767b04e819c2fcebf7785ee89f5f", aum = 0)
    private void bpJ() {
        for (C2611hZ hZVar : C2611hZ.values()) {
            if (!beP().containsKey(hZVar)) {
                OutpostUpgrade axr = (OutpostUpgrade) bFf().mo6865M(OutpostUpgrade.class);
                axr.mo16758k(hZVar);
                beP().put(hZVar, axr);
            }
        }
    }

    @C0064Am(aul = "81f9f1df603a63236fb396fa53892ec0", aum = 0)
    private void bpL() {
        for (C2611hZ hZVar : C2611hZ.values()) {
            if (!beT().containsKey(hZVar)) {
                OutpostLevelInfo an = (OutpostLevelInfo) bFf().mo6865M(OutpostLevelInfo.class);
                an.mo457k(hZVar);
                beT().put(hZVar, an);
            }
        }
    }

    @C0064Am(aul = "b12eca881db135377d1c685622683dd1", aum = 0)
    private void bpN() {
        for (C0437GE ge : C0437GE.values()) {
            if (!beR().containsKey(ge)) {
                OutpostLevelFeature lzVar = (OutpostLevelFeature) bFf().mo6865M(OutpostLevelFeature.class);
                lzVar.mo20448b(ge);
                beR().put(ge, lzVar);
            }
        }
    }

    @C0064Am(aul = "b9964749dbbb6d4d1774fa76650ebfa5", aum = 0)
    /* renamed from: d */
    private OutpostUpgrade m9026d(C2611hZ hZVar) {
        if (beP().containsKey(hZVar)) {
            return beP().get(hZVar);
        }
        throw new IllegalStateException("Outpost upgrade map does not contain key for level " + hZVar);
    }

    @C0064Am(aul = "ae2af499e1a32ba7bd08d99fc833f53a", aum = 0)
    /* renamed from: f */
    private OutpostLevelInfo m9027f(C2611hZ hZVar) {
        if (beT().containsKey(hZVar)) {
            return beT().get(hZVar);
        }
        throw new IllegalStateException("Outpost level map does not contain key for level " + hZVar);
    }

    @C0064Am(aul = "e72e27a470b2435572a1ef43c865cc4f", aum = 0)
    /* renamed from: c */
    private OutpostLevelFeature m9025c(C0437GE ge) {
        if (beR().containsKey(ge)) {
            return beR().get(ge);
        }
        throw new IllegalStateException("Outpost level map does not contain key for feature " + ge);
    }

    @C0064Am(aul = "64ba74314a6fd68a6138edb8768dcd55", aum = 0)
    /* renamed from: aG */
    private void m9024aG() {
        bpK();
        bpM();
        bpO();
        super.mo70aH();
    }
}
