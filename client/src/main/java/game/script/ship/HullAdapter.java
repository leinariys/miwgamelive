package game.script.ship;

import game.network.message.externalizable.aCE;
import game.script.damage.DamageType;
import game.script.util.GameObjectAdapter;
import logic.baa.C1616Xf;
import logic.baa.C1634Xv;
import logic.baa.C5511aMd;
import logic.baa.C6485anp;
import logic.data.mbean.aEW;
import logic.res.code.C5663aRz;
import logic.res.code.C6494any;
import logic.res.html.C2491fm;
import p001a.*;

import java.util.Collection;

@C5511aMd
@C6485anp
/* renamed from: a.aLM */
/* compiled from: a */
public class HullAdapter extends GameObjectAdapter<HullAdapter> implements C1616Xf {
    public static final int _m_fieldCount = 0;
    public static final C5663aRz[] _m_fields = null;
    public static final int _m_methodCount = 0;
    public static final C2491fm[] _m_methods = null;
    /* renamed from: qa */
    public static final C2491fm f3301qa = null;
    /* renamed from: qb */
    public static final C2491fm f3302qb = null;
    public static final long serialVersionUID = 0;
    /* renamed from: uI */
    public static final C2491fm f3303uI = null;
    public static C6494any ___iScriptClass;

    static {
        m16139V();
    }

    public HullAdapter() {
        super((C5540aNg) null);
        super.mo10S();
    }

    public HullAdapter(C5540aNg ang) {
        super(ang);
    }

    /* renamed from: V */
    static void m16139V() {
        serialVersionUID = (long) 1;
        _m_fieldCount = GameObjectAdapter._m_fieldCount + 0;
        _m_methodCount = GameObjectAdapter._m_methodCount + 3;
        _m_fields = new C5663aRz[(GameObjectAdapter._m_fieldCount + 0)];
        C1634Xv.m11725a((Object[]) GameObjectAdapter._m_fields, (Object[]) _m_fields);
        int i = GameObjectAdapter._m_methodCount;
        C2491fm[] fmVarArr = new C2491fm[(i + 3)];
        C2491fm a = C4105zY.m41624a(HullAdapter.class, "341e7f0c2fbf009a7a4d7b41d8924974", i);
        f3301qa = a;
        fmVarArr[i] = a;
        int i2 = i + 1;
        C2491fm a2 = C4105zY.m41624a(HullAdapter.class, "be81b7a6a5c5e7a50e53205372c51c98", i2);
        f3303uI = a2;
        fmVarArr[i2] = a2;
        int i3 = i2 + 1;
        C2491fm a3 = C4105zY.m41624a(HullAdapter.class, "006b7edfbdf9c63de70ccd41f0867914", i3);
        f3302qb = a3;
        fmVarArr[i3] = a3;
        int i4 = i3 + 1;
        _m_methods = fmVarArr;
        C1634Xv.m11725a((Object[]) GameObjectAdapter._m_methods, (Object[]) _m_methods);
        ___iScriptClass = aUO.m18566a(HullAdapter.class, aEW.class, _m_fields, _m_methods);
    }

    /* renamed from: T */
    public C6494any mo11T() {
        return ___iScriptClass;
    }

    /* renamed from: U */
    public C2491fm[] mo12U() {
        return _m_methods;
    }

    /* renamed from: W */
    public Object mo13W() {
        return new aEW(this);
    }

    /* renamed from: a */
    public Object mo14a(C0495Gr gr) {
        Object[] args = gr.getArgs();
        switch (gr.mo2417hq() - GameObjectAdapter._m_methodCount) {
            case 0:
                return new Float(m16141a((DamageType) args[0], ((Float) args[1]).floatValue()));
            case 1:
                return new Float(m16140a((DamageType) args[0]));
            case 2:
                return new Float(m16142hg());
            default:
                return super.mo14a(gr);
        }
    }

    /* renamed from: a */
    public void mo15a(Collection collection, C0495Gr gr) {
        super.mo15a(collection, gr);
    }

    /* renamed from: b */
    public float mo9876b(DamageType fr) {
        switch (bFf().mo6893i(f3303uI)) {
            case 0:
                return 0.0f;
            case 2:
                return ((Float) bFf().mo5606d(new aCE(this, f3303uI, new Object[]{fr}))).floatValue();
            case 3:
                bFf().mo5606d(new aCE(this, f3303uI, new Object[]{fr}));
                break;
        }
        return m16140a(fr);
    }

    /* renamed from: b */
    public float mo9877b(DamageType fr, float f) {
        switch (bFf().mo6893i(f3301qa)) {
            case 0:
                return 0.0f;
            case 2:
                return ((Float) bFf().mo5606d(new aCE(this, f3301qa, new Object[]{fr, new Float(f)}))).floatValue();
            case 3:
                bFf().mo5606d(new aCE(this, f3301qa, new Object[]{fr, new Float(f)}));
                break;
        }
        return m16141a(fr, f);
    }

    /* renamed from: c */
    public C5663aRz[] mo25c() {
        return _m_fields;
    }

    /* renamed from: hh */
    public float mo9878hh() {
        switch (bFf().mo6893i(f3302qb)) {
            case 0:
                return 0.0f;
            case 2:
                return ((Float) bFf().mo5606d(new aCE(this, f3302qb, new Object[0]))).floatValue();
            case 3:
                bFf().mo5606d(new aCE(this, f3302qb, new Object[0]));
                break;
        }
        return m16142hg();
    }

    /* renamed from: S */
    public void mo10S() {
        super.mo10S();
    }

    @C0064Am(aul = "341e7f0c2fbf009a7a4d7b41d8924974", aum = 0)
    /* renamed from: a */
    private float m16141a(DamageType fr, float f) {
        return ((HullAdapter) mo16071IG()).mo9877b(fr, f);
    }

    @C0064Am(aul = "be81b7a6a5c5e7a50e53205372c51c98", aum = 0)
    /* renamed from: a */
    private float m16140a(DamageType fr) {
        return ((HullAdapter) mo16071IG()).mo9876b(fr);
    }

    @C0064Am(aul = "006b7edfbdf9c63de70ccd41f0867914", aum = 0)
    /* renamed from: hg */
    private float m16142hg() {
        return ((HullAdapter) mo16071IG()).mo9878hh();
    }
}
