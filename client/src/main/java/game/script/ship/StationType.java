package game.script.ship;

import com.hoplon.geometry.Vec3f;
import game.network.message.externalizable.aCE;
import game.script.resource.Asset;
import game.script.template.BaseTaikodomContent;
import logic.baa.*;
import logic.data.mbean.aAR;
import logic.res.code.C5640aRc;
import logic.res.code.C5663aRz;
import logic.res.code.C6494any;
import logic.res.html.C2491fm;
import p001a.*;
import taikodom.geom.Orientation;

import java.util.Collection;

@C0566Hp
@C5829abJ("1.1.1")
@C6485anp
@C5511aMd
/* renamed from: a.Mx */
/* compiled from: a */
public class StationType extends BaseTaikodomContent implements C1616Xf, C6251ajP {
    /* renamed from: LR */
    public static final C5663aRz f1170LR = null;
    /* renamed from: LX */
    public static final C5663aRz f1172LX = null;
    /* renamed from: LZ */
    public static final C5663aRz f1174LZ = null;
    /* renamed from: MD */
    public static final C2491fm f1175MD = null;
    /* renamed from: ME */
    public static final C2491fm f1176ME = null;
    /* renamed from: MF */
    public static final C2491fm f1177MF = null;
    /* renamed from: MG */
    public static final C2491fm f1178MG = null;
    public static final int _m_fieldCount = 0;
    public static final C5663aRz[] _m_fields = null;
    public static final int _m_methodCount = 0;
    public static final C2491fm[] _m_methods = null;
    public static final C2491fm aDk = null;
    public static final C2491fm aDl = null;
    public static final C5663aRz cnR = null;
    public static final C2491fm dBI = null;
    public static final C2491fm dBJ = null;
    public static final C2491fm dBK = null;
    /* renamed from: dN */
    public static final C2491fm f1179dN = null;
    public static final long serialVersionUID = 0;
    /* renamed from: uU */
    public static final C5663aRz f1181uU = null;
    /* renamed from: vi */
    public static final C2491fm f1182vi = null;
    /* renamed from: vj */
    public static final C2491fm f1183vj = null;
    public static C6494any ___iScriptClass;
    @C0064Am(aul = "8f74e4651afd265f2de9304381be7cbf", aum = 0)

    /* renamed from: LQ */
    private static Asset f1169LQ;
    @C0064Am(aul = "88c4d9e23eceb6cac0271a5f641cec49", aum = 1)

    /* renamed from: LW */
    private static Vec3f f1171LW;
    @C0064Am(aul = "6578a6cbc97c6e85bfe897698403f662", aum = 2)

    /* renamed from: LY */
    private static Orientation f1173LY;
    @C0064Am(aul = "ffed31f536aded59d38f8b395a8e3cb3", aum = 3)
    private static Asset axR;
    @C0064Am(aul = "1b5a7383b96b77b4b924504ceca97831", aum = 4)

    /* renamed from: uT */
    private static String f1180uT;

    static {
        m7196V();
    }

    public StationType() {
        super((C5540aNg) null);
        super.mo10S();
    }

    public StationType(C5540aNg ang) {
        super(ang);
    }

    /* renamed from: V */
    static void m7196V() {
        serialVersionUID = (long) 1;
        _m_fieldCount = BaseTaikodomContent._m_fieldCount + 5;
        _m_methodCount = BaseTaikodomContent._m_methodCount + 12;
        int i = BaseTaikodomContent._m_fieldCount;
        C5663aRz[] arzArr = new C5663aRz[(i + 5)];
        C5663aRz b = C5640aRc.m17844b(StationType.class, "8f74e4651afd265f2de9304381be7cbf", i);
        f1170LR = b;
        arzArr[i] = b;
        int i2 = i + 1;
        C5663aRz b2 = C5640aRc.m17844b(StationType.class, "88c4d9e23eceb6cac0271a5f641cec49", i2);
        f1172LX = b2;
        arzArr[i2] = b2;
        int i3 = i2 + 1;
        C5663aRz b3 = C5640aRc.m17844b(StationType.class, "6578a6cbc97c6e85bfe897698403f662", i3);
        f1174LZ = b3;
        arzArr[i3] = b3;
        int i4 = i3 + 1;
        C5663aRz b4 = C5640aRc.m17844b(StationType.class, "ffed31f536aded59d38f8b395a8e3cb3", i4);
        cnR = b4;
        arzArr[i4] = b4;
        int i5 = i4 + 1;
        C5663aRz b5 = C5640aRc.m17844b(StationType.class, "1b5a7383b96b77b4b924504ceca97831", i5);
        f1181uU = b5;
        arzArr[i5] = b5;
        int i6 = i5 + 1;
        _m_fields = arzArr;
        C1634Xv.m11725a((Object[]) BaseTaikodomContent._m_fields, (Object[]) _m_fields);
        int i7 = BaseTaikodomContent._m_methodCount;
        C2491fm[] fmVarArr = new C2491fm[(i7 + 12)];
        C2491fm a = C4105zY.m41624a(StationType.class, "28b70e6808e5b556eb90a11b0f42b5b9", i7);
        aDk = a;
        fmVarArr[i7] = a;
        int i8 = i7 + 1;
        C2491fm a2 = C4105zY.m41624a(StationType.class, "340513d4ed05dccf5599dbb82a53338f", i8);
        aDl = a2;
        fmVarArr[i8] = a2;
        int i9 = i8 + 1;
        C2491fm a3 = C4105zY.m41624a(StationType.class, "40d5de0360844a8cf9a4cc5f1b995f71", i9);
        f1177MF = a3;
        fmVarArr[i9] = a3;
        int i10 = i9 + 1;
        C2491fm a4 = C4105zY.m41624a(StationType.class, "46c94f5957cdb0e28abf353ca6ffbc8f", i10);
        f1178MG = a4;
        fmVarArr[i10] = a4;
        int i11 = i10 + 1;
        C2491fm a5 = C4105zY.m41624a(StationType.class, "93aa2f08d4c1f3364d67650b232c4347", i11);
        f1175MD = a5;
        fmVarArr[i11] = a5;
        int i12 = i11 + 1;
        C2491fm a6 = C4105zY.m41624a(StationType.class, "32c89b45411f2df10f60a5c9b918c06d", i12);
        f1176ME = a6;
        fmVarArr[i12] = a6;
        int i13 = i12 + 1;
        C2491fm a7 = C4105zY.m41624a(StationType.class, "0002b3b4b07de01fab3ebb4b33889f11", i13);
        dBI = a7;
        fmVarArr[i13] = a7;
        int i14 = i13 + 1;
        C2491fm a8 = C4105zY.m41624a(StationType.class, "936d0e852403f6062a9a8c3ebc358235", i14);
        dBJ = a8;
        fmVarArr[i14] = a8;
        int i15 = i14 + 1;
        C2491fm a9 = C4105zY.m41624a(StationType.class, "c6dc4b282a663ea9d542f21964b77c66", i15);
        f1182vi = a9;
        fmVarArr[i15] = a9;
        int i16 = i15 + 1;
        C2491fm a10 = C4105zY.m41624a(StationType.class, "55aa5e132435f4b5936519e09c14d6f0", i16);
        f1183vj = a10;
        fmVarArr[i16] = a10;
        int i17 = i16 + 1;
        C2491fm a11 = C4105zY.m41624a(StationType.class, "8ae96dd8612452d388d6240757f48692", i17);
        dBK = a11;
        fmVarArr[i17] = a11;
        int i18 = i17 + 1;
        C2491fm a12 = C4105zY.m41624a(StationType.class, "21b203d3db3531d5717bbdb0e45102ba", i18);
        f1179dN = a12;
        fmVarArr[i18] = a12;
        int i19 = i18 + 1;
        _m_methods = fmVarArr;
        C1634Xv.m11725a((Object[]) BaseTaikodomContent._m_methods, (Object[]) _m_methods);
        ___iScriptClass = aUO.m18566a(StationType.class, aAR.class, _m_fields, _m_methods);
    }

    /* renamed from: H */
    private void m7193H(String str) {
        bFf().mo5608dq().mo3197f(f1181uU, str);
    }

    /* renamed from: a */
    private void m7197a(Orientation orientation) {
        bFf().mo5608dq().mo3197f(f1174LZ, orientation);
    }

    /* renamed from: ai */
    private void m7200ai(Asset tCVar) {
        bFf().mo5608dq().mo3197f(cnR, tCVar);
    }

    private Asset azn() {
        return (Asset) bFf().mo5608dq().mo3214p(cnR);
    }

    /* renamed from: i */
    private void m7202i(Vec3f vec3f) {
        bFf().mo5608dq().mo3197f(f1172LX, vec3f);
    }

    /* renamed from: ij */
    private String m7203ij() {
        return (String) bFf().mo5608dq().mo3214p(f1181uU);
    }

    /* renamed from: r */
    private void m7206r(Asset tCVar) {
        bFf().mo5608dq().mo3197f(f1170LR, tCVar);
    }

    /* renamed from: rh */
    private Asset m7209rh() {
        return (Asset) bFf().mo5608dq().mo3214p(f1170LR);
    }

    /* renamed from: rj */
    private Vec3f m7210rj() {
        return (Vec3f) bFf().mo5608dq().mo3214p(f1172LX);
    }

    /* renamed from: rk */
    private Orientation m7211rk() {
        return (Orientation) bFf().mo5608dq().mo3214p(f1174LZ);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Render Asset")
    /* renamed from: I */
    public void mo4100I(Asset tCVar) {
        switch (bFf().mo6893i(aDl)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, aDl, new Object[]{tCVar}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, aDl, new Object[]{tCVar}));
                break;
        }
        m7192H(tCVar);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Surface Material")
    /* renamed from: N */
    public void mo4101N(String str) {
        switch (bFf().mo6893i(f1183vj)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f1183vj, new Object[]{str}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f1183vj, new Object[]{str}));
                break;
        }
        m7194M(str);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Render Asset")
    /* renamed from: Nu */
    public Asset mo3521Nu() {
        switch (bFf().mo6893i(aDk)) {
            case 0:
                return null;
            case 2:
                return (Asset) bFf().mo5606d(new aCE(this, aDk, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, aDk, new Object[0]));
                break;
        }
        return m7195Nt();
    }

    /* renamed from: T */
    public C6494any mo11T() {
        return ___iScriptClass;
    }

    /* renamed from: U */
    public C2491fm[] mo12U() {
        return _m_methods;
    }

    /* renamed from: W */
    public Object mo13W() {
        return new aAR(this);
    }

    /* renamed from: a */
    public Object mo14a(C0495Gr gr) {
        Object[] args = gr.getArgs();
        switch (gr.mo2417hq() - BaseTaikodomContent._m_methodCount) {
            case 0:
                return m7195Nt();
            case 1:
                m7192H((Asset) args[0]);
                return null;
            case 2:
                return m7208rI();
            case 3:
                m7205j((Vec3f) args[0]);
                return null;
            case 4:
                return m7207rG();
            case 5:
                m7201b((Orientation) args[0]);
                return null;
            case 6:
                return bhi();
            case 7:
                m7198aD((Asset) args[0]);
                return null;
            case 8:
                return m7204it();
            case 9:
                m7194M((String) args[0]);
                return null;
            case 10:
                return bhk();
            case 11:
                return m7199aT();
            default:
                return super.mo14a(gr);
        }
    }

    /* renamed from: a */
    public void mo15a(Collection collection, C0495Gr gr) {
        super.mo15a(collection, gr);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Ambience S F X")
    /* renamed from: aE */
    public void mo4102aE(Asset tCVar) {
        switch (bFf().mo6893i(dBJ)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, dBJ, new Object[]{tCVar}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, dBJ, new Object[]{tCVar}));
                break;
        }
        m7198aD(tCVar);
    }

    /* access modifiers changed from: protected */
    /* renamed from: aU */
    public <T extends aDJ> T mo745aU() {
        switch (bFf().mo6893i(f1179dN)) {
            case 0:
                return null;
            case 2:
                return (aDJ) bFf().mo5606d(new aCE(this, f1179dN, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f1179dN, new Object[0]));
                break;
        }
        return m7199aT();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Ambience S F X")
    public Asset bhj() {
        switch (bFf().mo6893i(dBI)) {
            case 0:
                return null;
            case 2:
                return (Asset) bFf().mo5606d(new aCE(this, dBI, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, dBI, new Object[0]));
                break;
        }
        return bhi();
    }

    public Station bhl() {
        switch (bFf().mo6893i(dBK)) {
            case 0:
                return null;
            case 2:
                return (Station) bFf().mo5606d(new aCE(this, dBK, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, dBK, new Object[0]));
                break;
        }
        return bhk();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Spawn Orientation")
    /* renamed from: c */
    public void mo4105c(Orientation orientation) {
        switch (bFf().mo6893i(f1176ME)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f1176ME, new Object[]{orientation}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f1176ME, new Object[]{orientation}));
                break;
        }
        m7201b(orientation);
    }

    /* renamed from: c */
    public C5663aRz[] mo25c() {
        return _m_fields;
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Surface Material")
    /* renamed from: iu */
    public String mo4106iu() {
        switch (bFf().mo6893i(f1182vi)) {
            case 0:
                return null;
            case 2:
                return (String) bFf().mo5606d(new aCE(this, f1182vi, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f1182vi, new Object[0]));
                break;
        }
        return m7204it();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Spawn Relative Position")
    /* renamed from: k */
    public void mo4107k(Vec3f vec3f) {
        switch (bFf().mo6893i(f1178MG)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f1178MG, new Object[]{vec3f}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f1178MG, new Object[]{vec3f}));
                break;
        }
        m7205j(vec3f);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Spawn Orientation")
    /* renamed from: rH */
    public Orientation mo4108rH() {
        switch (bFf().mo6893i(f1175MD)) {
            case 0:
                return null;
            case 2:
                return (Orientation) bFf().mo5606d(new aCE(this, f1175MD, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f1175MD, new Object[0]));
                break;
        }
        return m7207rG();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Spawn Relative Position")
    /* renamed from: rJ */
    public Vec3f mo4109rJ() {
        switch (bFf().mo6893i(f1177MF)) {
            case 0:
                return null;
            case 2:
                return (Vec3f) bFf().mo5606d(new aCE(this, f1177MF, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f1177MF, new Object[0]));
                break;
        }
        return m7208rI();
    }

    /* renamed from: S */
    public void mo10S() {
        super.mo10S();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Render Asset")
    @C0064Am(aul = "28b70e6808e5b556eb90a11b0f42b5b9", aum = 0)
    /* renamed from: Nt */
    private Asset m7195Nt() {
        return m7209rh();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Render Asset")
    @C0064Am(aul = "340513d4ed05dccf5599dbb82a53338f", aum = 0)
    /* renamed from: H */
    private void m7192H(Asset tCVar) {
        m7206r(tCVar);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Spawn Relative Position")
    @C0064Am(aul = "40d5de0360844a8cf9a4cc5f1b995f71", aum = 0)
    /* renamed from: rI */
    private Vec3f m7208rI() {
        return m7210rj();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Spawn Relative Position")
    @C0064Am(aul = "46c94f5957cdb0e28abf353ca6ffbc8f", aum = 0)
    /* renamed from: j */
    private void m7205j(Vec3f vec3f) {
        m7202i(vec3f);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Spawn Orientation")
    @C0064Am(aul = "93aa2f08d4c1f3364d67650b232c4347", aum = 0)
    /* renamed from: rG */
    private Orientation m7207rG() {
        return m7211rk();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Spawn Orientation")
    @C0064Am(aul = "32c89b45411f2df10f60a5c9b918c06d", aum = 0)
    /* renamed from: b */
    private void m7201b(Orientation orientation) {
        m7197a(orientation);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Ambience S F X")
    @C0064Am(aul = "0002b3b4b07de01fab3ebb4b33889f11", aum = 0)
    private Asset bhi() {
        return azn();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Ambience S F X")
    @C0064Am(aul = "936d0e852403f6062a9a8c3ebc358235", aum = 0)
    /* renamed from: aD */
    private void m7198aD(Asset tCVar) {
        m7200ai(tCVar);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Surface Material")
    @C0064Am(aul = "c6dc4b282a663ea9d542f21964b77c66", aum = 0)
    /* renamed from: it */
    private String m7204it() {
        return m7203ij();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Surface Material")
    @C0064Am(aul = "55aa5e132435f4b5936519e09c14d6f0", aum = 0)
    /* renamed from: M */
    private void m7194M(String str) {
        m7193H(str);
    }

    @C0064Am(aul = "8ae96dd8612452d388d6240757f48692", aum = 0)
    private Station bhk() {
        return (Station) mo745aU();
    }

    @C0064Am(aul = "21b203d3db3531d5717bbdb0e45102ba", aum = 0)
    /* renamed from: aT */
    private <T extends aDJ> T m7199aT() {
        T t = (Station) bFf().mo6865M(Station.class);
        t.mo642e(this);
        return t;
    }
}
