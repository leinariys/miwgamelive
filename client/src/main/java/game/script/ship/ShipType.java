package game.script.ship;

import game.CollisionFXSet;
import game.geometry.Vec3d;
import game.network.message.externalizable.aCE;
import game.network.message.serializable.C3438ra;
import game.script.pda.DatabaseCategory;
import game.script.pda.TaikopediaEntry;
import game.script.resource.Asset;
import game.script.space.LootType;
import logic.baa.*;
import logic.data.link.C3981xi;
import logic.data.mbean.aLL;
import logic.res.code.C5640aRc;
import logic.res.code.C5663aRz;
import logic.res.code.C6494any;
import logic.res.html.C2491fm;
import p001a.*;
import taikodom.infra.script.I18NString;

import java.util.Collection;
import java.util.Collections;
import java.util.List;

@C0566Hp
@C5829abJ("1.1.2")
@C6485anp
@C5511aMd
/* renamed from: a.NG */
/* compiled from: a */
public class ShipType extends BaseShipType implements C1616Xf {
    /* renamed from: LR */
    public static final C5663aRz f1207LR = null;
    /* renamed from: Pr */
    public static final C2491fm f1208Pr = null;
    /* renamed from: VB */
    public static final C5663aRz f1209VB = null;
    /* renamed from: VW */
    public static final C5663aRz f1210VW = null;
    /* renamed from: Wc */
    public static final C5663aRz f1214Wc = null;
    /* renamed from: We */
    public static final C5663aRz f1216We = null;
    public static final int _m_fieldCount = 0;
    public static final C5663aRz[] _m_fields = null;
    public static final int _m_methodCount = 0;
    public static final C2491fm[] _m_methods = null;
    public static final C2491fm aDR = null;
    public static final C2491fm aDk = null;
    public static final C2491fm aDl = null;
    public static final C5663aRz aMh = null;
    public static final C5663aRz aMi = null;
    public static final C2491fm aMk = null;
    public static final C2491fm aMl = null;
    public static final C2491fm aMm = null;
    public static final C2491fm aMn = null;
    public static final C2491fm aMo = null;
    public static final C2491fm aMu = null;
    public static final C2491fm aMv = null;
    public static final C2491fm aTA = null;
    public static final C2491fm aTB = null;
    public static final C2491fm aTe = null;
    public static final C2491fm aTf = null;
    public static final C2491fm aTg = null;
    public static final C2491fm aTh = null;
    public static final C2491fm aTk = null;
    public static final C2491fm aTp = null;
    public static final C2491fm aTr = null;
    public static final C2491fm aTv = null;
    public static final C5663aRz awd = null;
    public static final C5663aRz bnD = null;
    public static final C5663aRz bnF = null;
    public static final C5663aRz bnH = null;
    public static final C5663aRz bnJ = null;
    public static final C5663aRz bnL = null;
    public static final C5663aRz bnN = null;
    public static final C5663aRz bnO = null;
    public static final C5663aRz bnP = null;
    public static final C5663aRz bnR = null;
    public static final C5663aRz bnT = null;
    public static final C5663aRz bnU = null;
    public static final C5663aRz bnV = null;
    public static final C5663aRz bnX = null;
    public static final C5663aRz bnZ = null;
    public static final C5663aRz bob = null;
    public static final C5663aRz bod = null;
    public static final C5663aRz bof = null;
    public static final C5663aRz boi = null;
    public static final C5663aRz bok = null;
    public static final C5663aRz bom = null;
    public static final C5663aRz boo = null;
    public static final C5663aRz boq = null;
    public static final C5663aRz bos = null;
    public static final C5663aRz bot = null;
    public static final C5663aRz bou = null;
    public static final C5663aRz bow = null;
    public static final C2491fm bpZ = null;
    public static final C2491fm bqc = null;
    public static final C2491fm bsd = null;
    public static final C2491fm bsg = null;
    public static final C2491fm bsh = null;
    public static final C2491fm bsi = null;
    public static final C2491fm bsj = null;
    public static final C2491fm bsk = null;
    public static final C2491fm dGA = null;
    public static final C2491fm dGB = null;
    public static final C2491fm dGJ = null;
    public static final C2491fm dGK = null;
    public static final C2491fm dGL = null;
    public static final C2491fm dGM = null;
    public static final C2491fm dGN = null;
    public static final C2491fm dGO = null;
    public static final C2491fm dGP = null;
    public static final C2491fm dGQ = null;
    public static final C2491fm dGR = null;
    public static final C2491fm dGS = null;
    public static final C2491fm dGT = null;
    public static final C2491fm dGU = null;
    public static final C2491fm dGV = null;
    public static final C2491fm dGW = null;
    public static final C2491fm dGX = null;
    public static final C2491fm dGY = null;
    public static final C2491fm dGZ = null;
    public static final C2491fm dGy = null;
    public static final C2491fm dGz = null;
    public static final C2491fm dHa = null;
    public static final C2491fm dHb = null;
    public static final C2491fm dHc = null;
    public static final C2491fm dHd = null;
    public static final C2491fm dHe = null;
    public static final C2491fm dHf = null;
    public static final C2491fm dHg = null;
    public static final C2491fm dHh = null;
    public static final C2491fm dHi = null;
    public static final C2491fm dHj = null;
    public static final C2491fm dHk = null;
    public static final C2491fm dHl = null;
    public static final C2491fm dHm = null;
    public static final C2491fm dHn = null;
    public static final C2491fm dHo = null;
    public static final C2491fm dHp = null;
    public static final C2491fm dHq = null;
    public static final C2491fm dHr = null;
    public static final C2491fm dHs = null;
    public static final C2491fm dHt = null;
    public static final C2491fm dHu = null;
    public static final C2491fm dHv = null;
    public static final C2491fm dHw = null;
    public static final C2491fm dHx = null;
    public static final C2491fm dHy = null;
    public static final C2491fm dHz = null;
    /* renamed from: dN */
    public static final C2491fm f1217dN = null;
    public static final C2491fm dyZ = null;
    public static final C2491fm dza = null;
    public static final long serialVersionUID = 0;
    /* renamed from: uU */
    public static final C5663aRz f1224uU = null;
    /* renamed from: vi */
    public static final C2491fm f1225vi = null;
    /* renamed from: vj */
    public static final C2491fm f1226vj = null;
    public static C6494any ___iScriptClass;
    @C0064Am(aul = "73b2db6e5bb6d3f94327d759092d198d", aum = 22)

    /* renamed from: LQ */
    private static Asset f1206LQ;
    @C0064Am(aul = "291267db6c5b29dcfeb359bb9e9f3d7e", aum = 30)

    /* renamed from: WL */
    private static float f1211WL;
    @C0064Am(aul = "08fa3e70d05931101c4b7051155c09fd", aum = 31)

    /* renamed from: WM */
    private static float f1212WM;
    @C0064Am(aul = "21d17085c2c9311a571da84fb593abef", aum = 6)

    /* renamed from: Wb */
    private static Asset f1213Wb;
    @C0064Am(aul = "3a169b6baacadee5e0e9322d4ff95243", aum = 7)

    /* renamed from: Wd */
    private static Asset f1215Wd;
    @C0064Am(aul = "48c4930db707d046cfbac8456038dd59", aum = 14)
    private static float bhk;
    @C0064Am(aul = "f0e011f6e0ed047cea6f241a8fd4c6d1", aum = 10)
    private static float bhl;
    @C0064Am(aul = "5c65ff8571e154539e3a702dafa5d3f5", aum = 34)
    private static HullType bhn;
    @C0064Am(aul = "368a3fbcb4e3b0782c1ea05057249238", aum = 0)
    private static CargoHoldType bnC;
    @C0064Am(aul = "479e144d222542d4cd699833a1703d41", aum = 3)
    private static CollisionFXSet bnE;
    @C0064Am(aul = "1eda3e3b844b14dbf9ab6ab2a782bd81", aum = 4)
    private static CruiseSpeedType bnG;
    @C0064Am(aul = "1ca50fd7afda7262fb75a522858bd914", aum = 5)
    private static Asset bnI;
    @C0064Am(aul = "204b98e6b0b6c691367c4125a2ca5d69", aum = 8)
    private static I18NString bnK;
    @C0064Am(aul = "b679ff8b5a15921b24cc5c2738dfbe15", aum = 9)
    private static LootType bnM;
    @C0064Am(aul = "b9bbe135cc895ad63f2776a9b4fec021", aum = 12)
    private static float bnQ;
    @C0064Am(aul = "6f09886ab27ed6def65f2ab7a12ab86a", aum = 13)
    private static float bnS;
    @C0064Am(aul = "928a4fe010233b298b1e0e97f9ef6917", aum = 16)
    private static float bnW;
    @C0064Am(aul = "ed3ef6b6fcd6db854fc4dd4a1ee71e2e", aum = 17)
    private static float bnY;
    @C0064Am(aul = "894e1aa092c042f297543fda0d831526", aum = 18)
    private static float boa;
    @C0064Am(aul = "3e3393327511358344ff4ca97d520acd", aum = 19)
    private static Asset boc;
    @C0064Am(aul = "02b94c71488473c83f12e123414932aa", aum = 20)
    private static float boe;
    @C0064Am(aul = "1a0c775e0f24e9208410e158f2733eac", aum = 21)
    private static float bog;
    @C0064Am(aul = "dfb56f2dcfa4d13a8a9054e290ca2a09", aum = 23)
    private static float boh;
    @C0064Am(aul = "460eae22844db2a80e0c9469d376aab4", aum = 24)
    private static C3438ra<ShipStructureType> boj;
    @C0064Am(aul = "6fb84320de6368554f4e2fb4852f05e7", aum = 26)
    private static C3438ra<C3315qE> bol;
    @C0064Am(aul = "088ddaf62e04789d9edae9c46d989697", aum = 27)
    private static Vec3d bon;
    @C0064Am(aul = "0c8577a46f1b6f59c4a78d7c0c1a0e39", aum = 28)
    private static Vec3d bop;
    @C0064Am(aul = "ebcd7769b9ffca827e3a466e0816f1d3", aum = 29)
    private static Vec3d bor;
    @C0064Am(aul = "cbdf91aa85cc3f67bdb5002b1d23df58", aum = 33)
    private static float bov;
    @C0064Am(aul = "a5c1b391d6327892172ac23fa23753bd", aum = 15)

    /* renamed from: dW */
    private static float f1218dW;
    @C0064Am(aul = "bfbbfbce0e341f232b6de4f7657b1299", aum = 11)

    /* renamed from: dX */
    private static float f1219dX;
    @C0064Am(aul = "822a91b5a407dbe9eef26ac76e93897a", aum = 1)

    /* renamed from: jS */
    private static DatabaseCategory f1220jS;
    @C0064Am(aul = "5155673a67c1317e271851e80810b375", aum = 2)

    /* renamed from: jT */
    private static C3438ra<TaikopediaEntry> f1221jT;
    @C0064Am(aul = "ce96933a3a33a11f2c6b22de7916e496", aum = 32)

    /* renamed from: jV */
    private static float f1222jV;
    @C0064Am(aul = "62182e99d40679055bfdd3c6c3b1a88e", aum = 25)

    /* renamed from: uT */
    private static String f1223uT;

    static {
        m7328V();
    }

    public ShipType() {
        super((C5540aNg) null);
        super.mo10S();
    }

    public ShipType(C5540aNg ang) {
        super(ang);
    }

    /* renamed from: V */
    static void m7328V() {
        serialVersionUID = (long) 1;
        _m_fieldCount = BaseShipType._m_fieldCount + 35;
        _m_methodCount = BaseShipType._m_methodCount + 81;
        int i = BaseShipType._m_fieldCount;
        C5663aRz[] arzArr = new C5663aRz[(i + 35)];
        C5663aRz b = C5640aRc.m17844b(ShipType.class, "368a3fbcb4e3b0782c1ea05057249238", i);
        bnD = b;
        arzArr[i] = b;
        int i2 = i + 1;
        C5663aRz b2 = C5640aRc.m17844b(ShipType.class, "822a91b5a407dbe9eef26ac76e93897a", i2);
        aMh = b2;
        arzArr[i2] = b2;
        int i3 = i2 + 1;
        C5663aRz b3 = C5640aRc.m17844b(ShipType.class, "5155673a67c1317e271851e80810b375", i3);
        awd = b3;
        arzArr[i3] = b3;
        int i4 = i3 + 1;
        C5663aRz b4 = C5640aRc.m17844b(ShipType.class, "479e144d222542d4cd699833a1703d41", i4);
        bnF = b4;
        arzArr[i4] = b4;
        int i5 = i4 + 1;
        C5663aRz b5 = C5640aRc.m17844b(ShipType.class, "1eda3e3b844b14dbf9ab6ab2a782bd81", i5);
        bnH = b5;
        arzArr[i5] = b5;
        int i6 = i5 + 1;
        C5663aRz b6 = C5640aRc.m17844b(ShipType.class, "1ca50fd7afda7262fb75a522858bd914", i6);
        bnJ = b6;
        arzArr[i6] = b6;
        int i7 = i6 + 1;
        C5663aRz b7 = C5640aRc.m17844b(ShipType.class, "21d17085c2c9311a571da84fb593abef", i7);
        f1214Wc = b7;
        arzArr[i7] = b7;
        int i8 = i7 + 1;
        C5663aRz b8 = C5640aRc.m17844b(ShipType.class, "3a169b6baacadee5e0e9322d4ff95243", i8);
        f1216We = b8;
        arzArr[i8] = b8;
        int i9 = i8 + 1;
        C5663aRz b9 = C5640aRc.m17844b(ShipType.class, "204b98e6b0b6c691367c4125a2ca5d69", i9);
        bnL = b9;
        arzArr[i9] = b9;
        int i10 = i9 + 1;
        C5663aRz b10 = C5640aRc.m17844b(ShipType.class, "b679ff8b5a15921b24cc5c2738dfbe15", i10);
        bnN = b10;
        arzArr[i10] = b10;
        int i11 = i10 + 1;
        C5663aRz b11 = C5640aRc.m17844b(ShipType.class, "f0e011f6e0ed047cea6f241a8fd4c6d1", i11);
        bnO = b11;
        arzArr[i11] = b11;
        int i12 = i11 + 1;
        C5663aRz b12 = C5640aRc.m17844b(ShipType.class, "bfbbfbce0e341f232b6de4f7657b1299", i12);
        bnP = b12;
        arzArr[i12] = b12;
        int i13 = i12 + 1;
        C5663aRz b13 = C5640aRc.m17844b(ShipType.class, "b9bbe135cc895ad63f2776a9b4fec021", i13);
        bnR = b13;
        arzArr[i13] = b13;
        int i14 = i13 + 1;
        C5663aRz b14 = C5640aRc.m17844b(ShipType.class, "6f09886ab27ed6def65f2ab7a12ab86a", i14);
        bnT = b14;
        arzArr[i14] = b14;
        int i15 = i14 + 1;
        C5663aRz b15 = C5640aRc.m17844b(ShipType.class, "48c4930db707d046cfbac8456038dd59", i15);
        bnU = b15;
        arzArr[i15] = b15;
        int i16 = i15 + 1;
        C5663aRz b16 = C5640aRc.m17844b(ShipType.class, "a5c1b391d6327892172ac23fa23753bd", i16);
        bnV = b16;
        arzArr[i16] = b16;
        int i17 = i16 + 1;
        C5663aRz b17 = C5640aRc.m17844b(ShipType.class, "928a4fe010233b298b1e0e97f9ef6917", i17);
        bnX = b17;
        arzArr[i17] = b17;
        int i18 = i17 + 1;
        C5663aRz b18 = C5640aRc.m17844b(ShipType.class, "ed3ef6b6fcd6db854fc4dd4a1ee71e2e", i18);
        bnZ = b18;
        arzArr[i18] = b18;
        int i19 = i18 + 1;
        C5663aRz b19 = C5640aRc.m17844b(ShipType.class, "894e1aa092c042f297543fda0d831526", i19);
        bob = b19;
        arzArr[i19] = b19;
        int i20 = i19 + 1;
        C5663aRz b20 = C5640aRc.m17844b(ShipType.class, "3e3393327511358344ff4ca97d520acd", i20);
        bod = b20;
        arzArr[i20] = b20;
        int i21 = i20 + 1;
        C5663aRz b21 = C5640aRc.m17844b(ShipType.class, "02b94c71488473c83f12e123414932aa", i21);
        bof = b21;
        arzArr[i21] = b21;
        int i22 = i21 + 1;
        C5663aRz b22 = C5640aRc.m17844b(ShipType.class, "1a0c775e0f24e9208410e158f2733eac", i22);
        f1209VB = b22;
        arzArr[i22] = b22;
        int i23 = i22 + 1;
        C5663aRz b23 = C5640aRc.m17844b(ShipType.class, "73b2db6e5bb6d3f94327d759092d198d", i23);
        f1207LR = b23;
        arzArr[i23] = b23;
        int i24 = i23 + 1;
        C5663aRz b24 = C5640aRc.m17844b(ShipType.class, "dfb56f2dcfa4d13a8a9054e290ca2a09", i24);
        boi = b24;
        arzArr[i24] = b24;
        int i25 = i24 + 1;
        C5663aRz b25 = C5640aRc.m17844b(ShipType.class, "460eae22844db2a80e0c9469d376aab4", i25);
        bok = b25;
        arzArr[i25] = b25;
        int i26 = i25 + 1;
        C5663aRz b26 = C5640aRc.m17844b(ShipType.class, "62182e99d40679055bfdd3c6c3b1a88e", i26);
        f1224uU = b26;
        arzArr[i26] = b26;
        int i27 = i26 + 1;
        C5663aRz b27 = C5640aRc.m17844b(ShipType.class, "6fb84320de6368554f4e2fb4852f05e7", i27);
        bom = b27;
        arzArr[i27] = b27;
        int i28 = i27 + 1;
        C5663aRz b28 = C5640aRc.m17844b(ShipType.class, "088ddaf62e04789d9edae9c46d989697", i28);
        boo = b28;
        arzArr[i28] = b28;
        int i29 = i28 + 1;
        C5663aRz b29 = C5640aRc.m17844b(ShipType.class, "0c8577a46f1b6f59c4a78d7c0c1a0e39", i29);
        boq = b29;
        arzArr[i29] = b29;
        int i30 = i29 + 1;
        C5663aRz b30 = C5640aRc.m17844b(ShipType.class, "ebcd7769b9ffca827e3a466e0816f1d3", i30);
        bos = b30;
        arzArr[i30] = b30;
        int i31 = i30 + 1;
        C5663aRz b31 = C5640aRc.m17844b(ShipType.class, "291267db6c5b29dcfeb359bb9e9f3d7e", i31);
        bot = b31;
        arzArr[i31] = b31;
        int i32 = i31 + 1;
        C5663aRz b32 = C5640aRc.m17844b(ShipType.class, "08fa3e70d05931101c4b7051155c09fd", i32);
        bou = b32;
        arzArr[i32] = b32;
        int i33 = i32 + 1;
        C5663aRz b33 = C5640aRc.m17844b(ShipType.class, "ce96933a3a33a11f2c6b22de7916e496", i33);
        aMi = b33;
        arzArr[i33] = b33;
        int i34 = i33 + 1;
        C5663aRz b34 = C5640aRc.m17844b(ShipType.class, "cbdf91aa85cc3f67bdb5002b1d23df58", i34);
        bow = b34;
        arzArr[i34] = b34;
        int i35 = i34 + 1;
        C5663aRz b35 = C5640aRc.m17844b(ShipType.class, "5c65ff8571e154539e3a702dafa5d3f5", i35);
        f1210VW = b35;
        arzArr[i35] = b35;
        int i36 = i35 + 1;
        _m_fields = arzArr;
        C1634Xv.m11725a((Object[]) BaseShipType._m_fields, (Object[]) _m_fields);
        int i37 = BaseShipType._m_methodCount;
        C2491fm[] fmVarArr = new C2491fm[(i37 + 81)];
        C2491fm a = C4105zY.m41624a(ShipType.class, "f0389975cf8b69598f10d7606266620d", i37);
        aTv = a;
        fmVarArr[i37] = a;
        int i38 = i37 + 1;
        C2491fm a2 = C4105zY.m41624a(ShipType.class, "43ca44b61070bb5777c42fa5b10ee276", i38);
        dGJ = a2;
        fmVarArr[i38] = a2;
        int i39 = i38 + 1;
        C2491fm a3 = C4105zY.m41624a(ShipType.class, "9ed8e7f438dfcc47f162ecffe34e03e4", i39);
        aMn = a3;
        fmVarArr[i39] = a3;
        int i40 = i39 + 1;
        C2491fm a4 = C4105zY.m41624a(ShipType.class, "8277a638ef70195ea30d72d9a880265c", i40);
        aMo = a4;
        fmVarArr[i40] = a4;
        int i41 = i40 + 1;
        C2491fm a5 = C4105zY.m41624a(ShipType.class, "dbc4db88322b79327aa485824ab6dca4", i41);
        aMl = a5;
        fmVarArr[i41] = a5;
        int i42 = i41 + 1;
        C2491fm a6 = C4105zY.m41624a(ShipType.class, "d1299d423a03caf57c5c2c8e9f2c9f82", i42);
        aMm = a6;
        fmVarArr[i42] = a6;
        int i43 = i42 + 1;
        C2491fm a7 = C4105zY.m41624a(ShipType.class, "31c8a497c01afb23949928879ee398b7", i43);
        dyZ = a7;
        fmVarArr[i43] = a7;
        int i44 = i43 + 1;
        C2491fm a8 = C4105zY.m41624a(ShipType.class, "645b4b49c764836f1ed0d477dd1d5b53", i44);
        dza = a8;
        fmVarArr[i44] = a8;
        int i45 = i44 + 1;
        C2491fm a9 = C4105zY.m41624a(ShipType.class, "56a8a5066f487b7e750862173c529aaf", i45);
        dGK = a9;
        fmVarArr[i45] = a9;
        int i46 = i45 + 1;
        C2491fm a10 = C4105zY.m41624a(ShipType.class, "3464ccdd97ec3423fccb4bacab63a5d5", i46);
        dGL = a10;
        fmVarArr[i46] = a10;
        int i47 = i46 + 1;
        C2491fm a11 = C4105zY.m41624a(ShipType.class, "73d87abe524f7aca907a08bbd5b0e005", i47);
        dGM = a11;
        fmVarArr[i47] = a11;
        int i48 = i47 + 1;
        C2491fm a12 = C4105zY.m41624a(ShipType.class, "53a4787a138062f1a868ea9318d4bab3", i48);
        dGN = a12;
        fmVarArr[i48] = a12;
        int i49 = i48 + 1;
        C2491fm a13 = C4105zY.m41624a(ShipType.class, "4b8c445336bf677a29cdafa3a85c27e9", i49);
        dGO = a13;
        fmVarArr[i49] = a13;
        int i50 = i49 + 1;
        C2491fm a14 = C4105zY.m41624a(ShipType.class, "db78838192b207cc91feb793a43295d1", i50);
        dGP = a14;
        fmVarArr[i50] = a14;
        int i51 = i50 + 1;
        C2491fm a15 = C4105zY.m41624a(ShipType.class, "2b441592015b3191c4b4cd2bed094f41", i51);
        dGQ = a15;
        fmVarArr[i51] = a15;
        int i52 = i51 + 1;
        C2491fm a16 = C4105zY.m41624a(ShipType.class, "4d77c01d6b22be5426e97eddbca21938", i52);
        dGR = a16;
        fmVarArr[i52] = a16;
        int i53 = i52 + 1;
        C2491fm a17 = C4105zY.m41624a(ShipType.class, "7a2358a2256ad029a05d5138233bbbce", i53);
        dGS = a17;
        fmVarArr[i53] = a17;
        int i54 = i53 + 1;
        C2491fm a18 = C4105zY.m41624a(ShipType.class, "2ef3044be89f514cc18198397b171c9b", i54);
        dGT = a18;
        fmVarArr[i54] = a18;
        int i55 = i54 + 1;
        C2491fm a19 = C4105zY.m41624a(ShipType.class, "4458be2ce8d4b2d271e178c3d312cedd", i55);
        aTB = a19;
        fmVarArr[i55] = a19;
        int i56 = i55 + 1;
        C2491fm a20 = C4105zY.m41624a(ShipType.class, "88db32192f03dee62a8fb80f188523d5", i56);
        dGU = a20;
        fmVarArr[i56] = a20;
        int i57 = i56 + 1;
        C2491fm a21 = C4105zY.m41624a(ShipType.class, "b5b406a23b76e3a2754fa35e6264292a", i57);
        dGV = a21;
        fmVarArr[i57] = a21;
        int i58 = i57 + 1;
        C2491fm a22 = C4105zY.m41624a(ShipType.class, "a3adf7f3371954b71fbfb4ed2da219e0", i58);
        dGW = a22;
        fmVarArr[i58] = a22;
        int i59 = i58 + 1;
        C2491fm a23 = C4105zY.m41624a(ShipType.class, "246ccf7700d6b8414732c67b1e50c06d", i59);
        aTe = a23;
        fmVarArr[i59] = a23;
        int i60 = i59 + 1;
        C2491fm a24 = C4105zY.m41624a(ShipType.class, "778fef8bf0ad09736407df80c9e5c460", i60);
        dGA = a24;
        fmVarArr[i60] = a24;
        int i61 = i60 + 1;
        C2491fm a25 = C4105zY.m41624a(ShipType.class, "f21ef6555808204171cb1936be8f514a", i61);
        aTf = a25;
        fmVarArr[i61] = a25;
        int i62 = i61 + 1;
        C2491fm a26 = C4105zY.m41624a(ShipType.class, "236c48b83fe029528f7f969b4ae1a85d", i62);
        dGB = a26;
        fmVarArr[i62] = a26;
        int i63 = i62 + 1;
        C2491fm a27 = C4105zY.m41624a(ShipType.class, "3799663b6caefa00f855095a2521f5de", i63);
        dGX = a27;
        fmVarArr[i63] = a27;
        int i64 = i63 + 1;
        C2491fm a28 = C4105zY.m41624a(ShipType.class, "42292b44e2e81e1d8d866002f976f55b", i64);
        dGY = a28;
        fmVarArr[i64] = a28;
        int i65 = i64 + 1;
        C2491fm a29 = C4105zY.m41624a(ShipType.class, "42a833098011d7179fe06c68d0fc1ce3", i65);
        dGZ = a29;
        fmVarArr[i65] = a29;
        int i66 = i65 + 1;
        C2491fm a30 = C4105zY.m41624a(ShipType.class, "4a8ef5e4fd44d5743898383436e44db3", i66);
        dHa = a30;
        fmVarArr[i66] = a30;
        int i67 = i66 + 1;
        C2491fm a31 = C4105zY.m41624a(ShipType.class, "8c8c9dbb1d463a9ecf223143004bfd1c", i67);
        aTg = a31;
        fmVarArr[i67] = a31;
        int i68 = i67 + 1;
        C2491fm a32 = C4105zY.m41624a(ShipType.class, "53a788c812ecc849519468a8f043e4d9", i68);
        dGy = a32;
        fmVarArr[i68] = a32;
        int i69 = i68 + 1;
        C2491fm a33 = C4105zY.m41624a(ShipType.class, "d0dc03f87a4f000e545a58606090ce77", i69);
        aTh = a33;
        fmVarArr[i69] = a33;
        int i70 = i69 + 1;
        C2491fm a34 = C4105zY.m41624a(ShipType.class, "8ecee81037747f896e87c1dc45baef10", i70);
        dGz = a34;
        fmVarArr[i70] = a34;
        int i71 = i70 + 1;
        C2491fm a35 = C4105zY.m41624a(ShipType.class, "b4a76c29ba78f7be48d76be20a44f024", i71);
        dHb = a35;
        fmVarArr[i71] = a35;
        int i72 = i71 + 1;
        C2491fm a36 = C4105zY.m41624a(ShipType.class, "8941105b2f462120174de38e58563231", i72);
        dHc = a36;
        fmVarArr[i72] = a36;
        int i73 = i72 + 1;
        C2491fm a37 = C4105zY.m41624a(ShipType.class, "dc6f8557c10070cb1a5a6e0180908133", i73);
        dHd = a37;
        fmVarArr[i73] = a37;
        int i74 = i73 + 1;
        C2491fm a38 = C4105zY.m41624a(ShipType.class, "0ffd9808b160d1caa2588a5df9b1bf02", i74);
        dHe = a38;
        fmVarArr[i74] = a38;
        int i75 = i74 + 1;
        C2491fm a39 = C4105zY.m41624a(ShipType.class, "1a42e887f2aed43b47bed84a6fc546c8", i75);
        dHf = a39;
        fmVarArr[i75] = a39;
        int i76 = i75 + 1;
        C2491fm a40 = C4105zY.m41624a(ShipType.class, "e70d70382675d2a7c08d5de513d2ad48", i76);
        dHg = a40;
        fmVarArr[i76] = a40;
        int i77 = i76 + 1;
        C2491fm a41 = C4105zY.m41624a(ShipType.class, "6b5864a42a4c93b962f15586e9057464", i77);
        dHh = a41;
        fmVarArr[i77] = a41;
        int i78 = i77 + 1;
        C2491fm a42 = C4105zY.m41624a(ShipType.class, "7ad090a5bee8c48c77f5147cff94eabf", i78);
        dHi = a42;
        fmVarArr[i78] = a42;
        int i79 = i78 + 1;
        C2491fm a43 = C4105zY.m41624a(ShipType.class, "5c736b27d64efde85ca8ba6d43f9db0f", i79);
        dHj = a43;
        fmVarArr[i79] = a43;
        int i80 = i79 + 1;
        C2491fm a44 = C4105zY.m41624a(ShipType.class, "dfee35c058a544961e5b0c87fed6f0cb", i80);
        dHk = a44;
        fmVarArr[i80] = a44;
        int i81 = i80 + 1;
        C2491fm a45 = C4105zY.m41624a(ShipType.class, "f413c26d09aaa13bc7a65127a71d3d2a", i81);
        bpZ = a45;
        fmVarArr[i81] = a45;
        int i82 = i81 + 1;
        C2491fm a46 = C4105zY.m41624a(ShipType.class, "8d290cc6493ccf8594dcb46c7803b110", i82);
        dHl = a46;
        fmVarArr[i82] = a46;
        int i83 = i82 + 1;
        C2491fm a47 = C4105zY.m41624a(ShipType.class, "e71a93ca9bfffef1861fb3e4af66955c", i83);
        aDk = a47;
        fmVarArr[i83] = a47;
        int i84 = i83 + 1;
        C2491fm a48 = C4105zY.m41624a(ShipType.class, "120d4439c6094aa504ec0df954b2b1a4", i84);
        aDl = a48;
        fmVarArr[i84] = a48;
        int i85 = i84 + 1;
        C2491fm a49 = C4105zY.m41624a(ShipType.class, "095f4fb6a725708da36790aa1e7efa52", i85);
        bsd = a49;
        fmVarArr[i85] = a49;
        int i86 = i85 + 1;
        C2491fm a50 = C4105zY.m41624a(ShipType.class, "2551e923ccbb5b1c7872ded84862451d", i86);
        dHm = a50;
        fmVarArr[i86] = a50;
        int i87 = i86 + 1;
        C2491fm a51 = C4105zY.m41624a(ShipType.class, "697d313f9c376f08fb835883e77c7772", i87);
        dHn = a51;
        fmVarArr[i87] = a51;
        int i88 = i87 + 1;
        C2491fm a52 = C4105zY.m41624a(ShipType.class, "4aa9a49c93b1dc31af2e126de2a2f2bd", i88);
        dHo = a52;
        fmVarArr[i88] = a52;
        int i89 = i88 + 1;
        C2491fm a53 = C4105zY.m41624a(ShipType.class, "f542bd7d3bfcdb737e678f755690ccbe", i89);
        bqc = a53;
        fmVarArr[i89] = a53;
        int i90 = i89 + 1;
        C2491fm a54 = C4105zY.m41624a(ShipType.class, "3b4bba15f401a4de2f7bacae386e8a52", i90);
        dHp = a54;
        fmVarArr[i90] = a54;
        int i91 = i90 + 1;
        C2491fm a55 = C4105zY.m41624a(ShipType.class, "c351b86b17ca9e8723918a5f04530d2b", i91);
        f1225vi = a55;
        fmVarArr[i91] = a55;
        int i92 = i91 + 1;
        C2491fm a56 = C4105zY.m41624a(ShipType.class, "fa93a90e9869ca4f913efa53e7729403", i92);
        f1226vj = a56;
        fmVarArr[i92] = a56;
        int i93 = i92 + 1;
        C2491fm a57 = C4105zY.m41624a(ShipType.class, "42d61c9730e28e2e7d79bc6fe61b8dd7", i93);
        aTp = a57;
        fmVarArr[i93] = a57;
        int i94 = i93 + 1;
        C2491fm a58 = C4105zY.m41624a(ShipType.class, "152abe0ef97224a476d96ceaf48378b6", i94);
        dHq = a58;
        fmVarArr[i94] = a58;
        int i95 = i94 + 1;
        C2491fm a59 = C4105zY.m41624a(ShipType.class, "847b033d5a6fd475c2c1bc40bf2dcb5c", i95);
        aTA = a59;
        fmVarArr[i95] = a59;
        int i96 = i95 + 1;
        C2491fm a60 = C4105zY.m41624a(ShipType.class, "da9d82442a96668f0b901847ca7d1ce7", i96);
        dHr = a60;
        fmVarArr[i96] = a60;
        int i97 = i96 + 1;
        C2491fm a61 = C4105zY.m41624a(ShipType.class, "806712038dea51e2d5536643bf7e5c5d", i97);
        bsg = a61;
        fmVarArr[i97] = a61;
        int i98 = i97 + 1;
        C2491fm a62 = C4105zY.m41624a(ShipType.class, "a1a4353fcc30eafc81277e142ffa2520", i98);
        dHs = a62;
        fmVarArr[i98] = a62;
        int i99 = i98 + 1;
        C2491fm a63 = C4105zY.m41624a(ShipType.class, "d6d65af14ae465fc69ddd1315159a5ff", i99);
        bsh = a63;
        fmVarArr[i99] = a63;
        int i100 = i99 + 1;
        C2491fm a64 = C4105zY.m41624a(ShipType.class, "1e6b4a311db9a593715e00f36b9da711", i100);
        dHt = a64;
        fmVarArr[i100] = a64;
        int i101 = i100 + 1;
        C2491fm a65 = C4105zY.m41624a(ShipType.class, "3f36e58c4d6d40329c5c6321fef9f7d7", i101);
        bsi = a65;
        fmVarArr[i101] = a65;
        int i102 = i101 + 1;
        C2491fm a66 = C4105zY.m41624a(ShipType.class, "7d48a12fc9b8642abcfac3a73a4f4bb5", i102);
        dHu = a66;
        fmVarArr[i102] = a66;
        int i103 = i102 + 1;
        C2491fm a67 = C4105zY.m41624a(ShipType.class, "afde9a01f27f79cec454cc0835441023", i103);
        bsj = a67;
        fmVarArr[i103] = a67;
        int i104 = i103 + 1;
        C2491fm a68 = C4105zY.m41624a(ShipType.class, "deb510e671ae93057260a21eb68759f8", i104);
        dHv = a68;
        fmVarArr[i104] = a68;
        int i105 = i104 + 1;
        C2491fm a69 = C4105zY.m41624a(ShipType.class, "bbf7256f2fa0758860a59c188dfea2ee", i105);
        bsk = a69;
        fmVarArr[i105] = a69;
        int i106 = i105 + 1;
        C2491fm a70 = C4105zY.m41624a(ShipType.class, "969868900e4fe6fce1b659fe179e5928", i106);
        dHw = a70;
        fmVarArr[i106] = a70;
        int i107 = i106 + 1;
        C2491fm a71 = C4105zY.m41624a(ShipType.class, "beac139a9218aa3b0862c0a5c4e01654", i107);
        aMu = a71;
        fmVarArr[i107] = a71;
        int i108 = i107 + 1;
        C2491fm a72 = C4105zY.m41624a(ShipType.class, "e381a5b945fdf24c738afc641f79d502", i108);
        aMv = a72;
        fmVarArr[i108] = a72;
        int i109 = i108 + 1;
        C2491fm a73 = C4105zY.m41624a(ShipType.class, "1fbb7df75187f702eeeab042caf6051e", i109);
        dHx = a73;
        fmVarArr[i109] = a73;
        int i110 = i109 + 1;
        C2491fm a74 = C4105zY.m41624a(ShipType.class, "00d252cd34b140b71a8fc25e94f932fa", i110);
        dHy = a74;
        fmVarArr[i110] = a74;
        int i111 = i110 + 1;
        C2491fm a75 = C4105zY.m41624a(ShipType.class, "b16f94772e73c74ce3377cc4d7ad1cab", i111);
        aTr = a75;
        fmVarArr[i111] = a75;
        int i112 = i111 + 1;
        C2491fm a76 = C4105zY.m41624a(ShipType.class, "e9c4b4f4cec7645d05a4f50a819f1b31", i112);
        dHz = a76;
        fmVarArr[i112] = a76;
        int i113 = i112 + 1;
        C2491fm a77 = C4105zY.m41624a(ShipType.class, "29ae8d849c97b7c74ae436ad86a127f8", i113);
        aDR = a77;
        fmVarArr[i113] = a77;
        int i114 = i113 + 1;
        C2491fm a78 = C4105zY.m41624a(ShipType.class, "7fa9adde1fd607cf9c5abae5e39f2551", i114);
        f1208Pr = a78;
        fmVarArr[i114] = a78;
        int i115 = i114 + 1;
        C2491fm a79 = C4105zY.m41624a(ShipType.class, "9366a8dc9823db9026993ba21ab47cd6", i115);
        f1217dN = a79;
        fmVarArr[i115] = a79;
        int i116 = i115 + 1;
        C2491fm a80 = C4105zY.m41624a(ShipType.class, "371350f7dbb5608a7f9fceb7c0478d3f", i116);
        aTk = a80;
        fmVarArr[i116] = a80;
        int i117 = i116 + 1;
        C2491fm a81 = C4105zY.m41624a(ShipType.class, "f5220ecac88f4ecf6561e4e551fb6ab0", i117);
        aMk = a81;
        fmVarArr[i117] = a81;
        int i118 = i117 + 1;
        _m_methods = fmVarArr;
        C1634Xv.m11725a((Object[]) BaseShipType._m_methods, (Object[]) _m_methods);
        ___iScriptClass = aUO.m18566a(ShipType.class, aLL.class, _m_fields, _m_methods);
    }

    /* renamed from: A */
    private void m7312A(Asset tCVar) {
        bFf().mo5608dq().mo3197f(f1216We, tCVar);
    }

    /* renamed from: H */
    private void m7315H(String str) {
        bFf().mo5608dq().mo3197f(f1224uU, str);
    }

    /* renamed from: K */
    private void m7317K(C3438ra raVar) {
        bFf().mo5608dq().mo3197f(bok, raVar);
    }

    /* renamed from: L */
    private void m7319L(C3438ra raVar) {
        bFf().mo5608dq().mo3197f(bom, raVar);
    }

    /* renamed from: Ll */
    private C3438ra m7320Ll() {
        return (C3438ra) bFf().mo5608dq().mo3214p(awd);
    }

    /* renamed from: RR */
    private DatabaseCategory m7323RR() {
        return (DatabaseCategory) bFf().mo5608dq().mo3214p(aMh);
    }

    /* renamed from: RS */
    private float m7324RS() {
        return bFf().mo5608dq().mo3211m(aMi);
    }

    @C0064Am(aul = "f5220ecac88f4ecf6561e4e551fb6ab0", aum = 0)
    /* renamed from: RV */
    private List m7325RV() {
        return bgF();
    }

    /* renamed from: V */
    private void m7329V(Asset tCVar) {
        bFf().mo5608dq().mo3197f(bnJ, tCVar);
    }

    @C0064Am(aul = "371350f7dbb5608a7f9fceb7c0478d3f", aum = 0)
    /* renamed from: VK */
    private List m7334VK() {
        return agt();
    }

    /* renamed from: W */
    private void m7336W(Asset tCVar) {
        bFf().mo5608dq().mo3197f(bod, tCVar);
    }

    /* renamed from: a */
    private void m7340a(CruiseSpeedType nf) {
        bFf().mo5608dq().mo3197f(bnH, nf);
    }

    /* renamed from: a */
    private void m7341a(LootType ahc) {
        bFf().mo5608dq().mo3197f(bnN, ahc);
    }

    /* renamed from: a */
    private void m7342a(DatabaseCategory aik) {
        bFf().mo5608dq().mo3197f(aMh, aik);
    }

    /* renamed from: a */
    private void m7345a(CollisionFXSet yaVar) {
        bFf().mo5608dq().mo3197f(bnF, yaVar);
    }

    private CargoHoldType aeI() {
        return (CargoHoldType) bFf().mo5608dq().mo3214p(bnD);
    }

    private CollisionFXSet aeJ() {
        return (CollisionFXSet) bFf().mo5608dq().mo3214p(bnF);
    }

    private CruiseSpeedType aeK() {
        return (CruiseSpeedType) bFf().mo5608dq().mo3214p(bnH);
    }

    private Asset aeL() {
        return (Asset) bFf().mo5608dq().mo3214p(bnJ);
    }

    private I18NString aeM() {
        return (I18NString) bFf().mo5608dq().mo3214p(bnL);
    }

    private LootType aeN() {
        return (LootType) bFf().mo5608dq().mo3214p(bnN);
    }

    private float aeO() {
        return bFf().mo5608dq().mo3211m(bnO);
    }

    private float aeP() {
        return bFf().mo5608dq().mo3211m(bnP);
    }

    private float aeQ() {
        return bFf().mo5608dq().mo3211m(bnR);
    }

    private float aeR() {
        return bFf().mo5608dq().mo3211m(bnT);
    }

    private float aeS() {
        return bFf().mo5608dq().mo3211m(bnU);
    }

    private float aeT() {
        return bFf().mo5608dq().mo3211m(bnV);
    }

    private float aeU() {
        return bFf().mo5608dq().mo3211m(bnX);
    }

    private float aeV() {
        return bFf().mo5608dq().mo3211m(bnZ);
    }

    private float aeW() {
        return bFf().mo5608dq().mo3211m(bob);
    }

    private Asset aeX() {
        return (Asset) bFf().mo5608dq().mo3214p(bod);
    }

    private float aeY() {
        return bFf().mo5608dq().mo3211m(bof);
    }

    private float aeZ() {
        return bFf().mo5608dq().mo3211m(f1209VB);
    }

    private float afa() {
        return bFf().mo5608dq().mo3211m(boi);
    }

    private C3438ra afb() {
        return (C3438ra) bFf().mo5608dq().mo3214p(bok);
    }

    private C3438ra afc() {
        return (C3438ra) bFf().mo5608dq().mo3214p(bom);
    }

    private Vec3d afd() {
        return (Vec3d) bFf().mo5608dq().mo3214p(boo);
    }

    private Vec3d afe() {
        return (Vec3d) bFf().mo5608dq().mo3214p(boq);
    }

    private Vec3d aff() {
        return (Vec3d) bFf().mo5608dq().mo3214p(bos);
    }

    private float afg() {
        return bFf().mo5608dq().mo3211m(bot);
    }

    private float afh() {
        return bFf().mo5608dq().mo3211m(bou);
    }

    private float afi() {
        return bFf().mo5608dq().mo3211m(bow);
    }

    /* renamed from: b */
    private void m7351b(CargoHoldType arb) {
        bFf().mo5608dq().mo3197f(bnD, arb);
    }

    private HullType bjZ() {
        return (HullType) bFf().mo5608dq().mo3214p(f1210VW);
    }

    /* renamed from: cx */
    private void m7355cx(float f) {
        bFf().mo5608dq().mo3150a(aMi, f);
    }

    /* renamed from: dA */
    private void m7357dA(float f) {
        bFf().mo5608dq().mo3150a(bnZ, f);
    }

    /* renamed from: dB */
    private void m7358dB(float f) {
        bFf().mo5608dq().mo3150a(bob, f);
    }

    /* renamed from: dC */
    private void m7359dC(float f) {
        bFf().mo5608dq().mo3150a(bof, f);
    }

    /* renamed from: dD */
    private void m7360dD(float f) {
        bFf().mo5608dq().mo3150a(f1209VB, f);
    }

    /* renamed from: dE */
    private void m7361dE(float f) {
        bFf().mo5608dq().mo3150a(boi, f);
    }

    /* renamed from: dF */
    private void m7362dF(float f) {
        bFf().mo5608dq().mo3150a(bot, f);
    }

    /* renamed from: dG */
    private void m7363dG(float f) {
        bFf().mo5608dq().mo3150a(bou, f);
    }

    /* renamed from: dH */
    private void m7364dH(float f) {
        bFf().mo5608dq().mo3150a(bow, f);
    }

    /* renamed from: dt */
    private void m7365dt(float f) {
        bFf().mo5608dq().mo3150a(bnO, f);
    }

    /* renamed from: du */
    private void m7366du(float f) {
        bFf().mo5608dq().mo3150a(bnP, f);
    }

    /* renamed from: dv */
    private void m7367dv(float f) {
        bFf().mo5608dq().mo3150a(bnR, f);
    }

    /* renamed from: dw */
    private void m7368dw(float f) {
        bFf().mo5608dq().mo3150a(bnT, f);
    }

    /* renamed from: dx */
    private void m7369dx(float f) {
        bFf().mo5608dq().mo3150a(bnU, f);
    }

    /* renamed from: dy */
    private void m7370dy(float f) {
        bFf().mo5608dq().mo3150a(bnV, f);
    }

    /* renamed from: dz */
    private void m7371dz(float f) {
        bFf().mo5608dq().mo3150a(bnX, f);
    }

    /* renamed from: eF */
    private void m7374eF(I18NString i18NString) {
        bFf().mo5608dq().mo3197f(bnL, i18NString);
    }

    /* renamed from: f */
    private void m7377f(HullType wHVar) {
        bFf().mo5608dq().mo3197f(f1210VW, wHVar);
    }

    /* renamed from: ij */
    private String m7398ij() {
        return (String) bFf().mo5608dq().mo3214p(f1224uU);
    }

    /* renamed from: r */
    private void m7400r(Asset tCVar) {
        bFf().mo5608dq().mo3197f(f1207LR, tCVar);
    }

    /* renamed from: rh */
    private Asset m7401rh() {
        return (Asset) bFf().mo5608dq().mo3214p(f1207LR);
    }

    /* renamed from: s */
    private void m7402s(Vec3d ajr) {
        bFf().mo5608dq().mo3197f(boo, ajr);
    }

    /* renamed from: t */
    private void m7403t(Vec3d ajr) {
        bFf().mo5608dq().mo3197f(boq, ajr);
    }

    /* renamed from: u */
    private void m7404u(Vec3d ajr) {
        bFf().mo5608dq().mo3197f(bos, ajr);
    }

    /* renamed from: x */
    private void m7406x(C3438ra raVar) {
        bFf().mo5608dq().mo3197f(awd, raVar);
    }

    /* renamed from: z */
    private void m7407z(Asset tCVar) {
        bFf().mo5608dq().mo3197f(f1214Wc, tCVar);
    }

    /* renamed from: zl */
    private Asset m7408zl() {
        return (Asset) bFf().mo5608dq().mo3214p(f1214Wc);
    }

    /* renamed from: zm */
    private Asset m7409zm() {
        return (Asset) bFf().mo5608dq().mo3214p(f1216We);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "YBottom Factor")
    /* renamed from: Ae */
    public float mo4144Ae() {
        switch (bFf().mo6893i(bsk)) {
            case 0:
                return 0.0f;
            case 2:
                return ((Float) bFf().mo5606d(new aCE(this, bsk, new Object[0]))).floatValue();
            case 3:
                bFf().mo5606d(new aCE(this, bsk, new Object[0]));
                break;
        }
        return aiz();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "YOffset Factor")
    /* renamed from: Ag */
    public float mo4145Ag() {
        switch (bFf().mo6893i(bsj)) {
            case 0:
                return 0.0f;
            case 2:
                return ((Float) bFf().mo5606d(new aCE(this, bsj, new Object[0]))).floatValue();
            case 3:
                bFf().mo5606d(new aCE(this, bsj, new Object[0]));
                break;
        }
        return aiy();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Camera Position Offset")
    /* renamed from: I */
    public void mo4146I(Vec3d ajr) {
        switch (bFf().mo6893i(dHs)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, dHs, new Object[]{ajr}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, dHs, new Object[]{ajr}));
                break;
        }
        m7313H(ajr);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Render Asset")
    /* renamed from: I */
    public void mo4147I(Asset tCVar) {
        switch (bFf().mo6893i(aDl)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, aDl, new Object[]{tCVar}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, aDl, new Object[]{tCVar}));
                break;
        }
        m7314H(tCVar);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Camera Look At Offset")
    /* renamed from: K */
    public void mo4148K(Vec3d ajr) {
        switch (bFf().mo6893i(dHt)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, dHt, new Object[]{ajr}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, dHt, new Object[]{ajr}));
                break;
        }
        m7316J(ajr);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Camera Sliding Offset")
    /* renamed from: M */
    public void mo4149M(Vec3d ajr) {
        switch (bFf().mo6893i(dHu)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, dHu, new Object[]{ajr}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, dHu, new Object[]{ajr}));
                break;
        }
        m7318L(ajr);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Surface Material")
    /* renamed from: N */
    public void mo4150N(String str) {
        switch (bFf().mo6893i(f1226vj)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f1226vj, new Object[]{str}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f1226vj, new Object[]{str}));
                break;
        }
        m7321M(str);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Render Asset")
    /* renamed from: Nu */
    public Asset mo4151Nu() {
        switch (bFf().mo6893i(aDk)) {
            case 0:
                return null;
            case 2:
                return (Asset) bFf().mo5606d(new aCE(this, aDk, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, aDk, new Object[0]));
                break;
        }
        return m7322Nt();
    }

    /* renamed from: RW */
    public /* bridge */ /* synthetic */ List mo183RW() {
        switch (bFf().mo6893i(aMk)) {
            case 0:
                return null;
            case 2:
                return (List) bFf().mo5606d(new aCE(this, aMk, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, aMk, new Object[0]));
                break;
        }
        return m7325RV();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Database Category")
    /* renamed from: RY */
    public DatabaseCategory mo184RY() {
        switch (bFf().mo6893i(aMn)) {
            case 0:
                return null;
            case 2:
                return (DatabaseCategory) bFf().mo5606d(new aCE(this, aMn, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, aMn, new Object[0]));
                break;
        }
        return m7326RX();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Pda M P Multiplier")
    /* renamed from: Sg */
    public float mo188Sg() {
        switch (bFf().mo6893i(aMu)) {
            case 0:
                return 0.0f;
            case 2:
                return ((Float) bFf().mo5606d(new aCE(this, aMu, new Object[0]))).floatValue();
            case 3:
                bFf().mo5606d(new aCE(this, aMu, new Object[0]));
                break;
        }
        return m7327Sf();
    }

    /* renamed from: T */
    public C6494any mo11T() {
        return ___iScriptClass;
    }

    /* renamed from: U */
    public C2491fm[] mo12U() {
        return _m_methods;
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Max Angular Acceleration")
    /* renamed from: VF */
    public float mo4152VF() {
        switch (bFf().mo6893i(aTe)) {
            case 0:
                return 0.0f;
            case 2:
                return ((Float) bFf().mo5606d(new aCE(this, aTe, new Object[0]))).floatValue();
            case 3:
                bFf().mo5606d(new aCE(this, aTe, new Object[0]));
                break;
        }
        return m7330VE();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Max Angular Velocity")
    /* renamed from: VH */
    public float mo4153VH() {
        switch (bFf().mo6893i(aTf)) {
            case 0:
                return 0.0f;
            case 2:
                return ((Float) bFf().mo5606d(new aCE(this, aTf, new Object[0]))).floatValue();
            case 3:
                bFf().mo5606d(new aCE(this, aTf, new Object[0]));
                break;
        }
        return m7331VG();
    }

    /* renamed from: VL */
    public /* bridge */ /* synthetic */ List mo4154VL() {
        switch (bFf().mo6893i(aTk)) {
            case 0:
                return null;
            case 2:
                return (List) bFf().mo5606d(new aCE(this, aTk, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, aTk, new Object[0]));
                break;
        }
        return m7334VK();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Hull")
    /* renamed from: VT */
    public HullType mo4155VT() {
        switch (bFf().mo6893i(aTr)) {
            case 0:
                return null;
            case 2:
                return (HullType) bFf().mo5606d(new aCE(this, aTr, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, aTr, new Object[0]));
                break;
        }
        return m7335VS();
    }

    /* renamed from: W */
    public Object mo13W() {
        return new aLL(this);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Cargo Hold")
    /* renamed from: Wb */
    public CargoHoldType mo4156Wb() {
        switch (bFf().mo6893i(aTv)) {
            case 0:
                return null;
            case 2:
                return (CargoHoldType) bFf().mo5606d(new aCE(this, aTv, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, aTv, new Object[0]));
                break;
        }
        return m7337Wa();
    }

    /* renamed from: Wl */
    public C3438ra<C3315qE> mo4157Wl() {
        switch (bFf().mo6893i(aTA)) {
            case 0:
                return null;
            case 2:
                return (C3438ra) bFf().mo5606d(new aCE(this, aTA, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, aTA, new Object[0]));
                break;
        }
        return m7338Wk();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Long description")
    /* renamed from: Wn */
    public I18NString mo4158Wn() {
        switch (bFf().mo6893i(aTB)) {
            case 0:
                return null;
            case 2:
                return (I18NString) bFf().mo5606d(new aCE(this, aTB, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, aTB, new Object[0]));
                break;
        }
        return m7339Wm();
    }

    /* renamed from: a */
    public Object mo14a(C0495Gr gr) {
        Object[] args = gr.getArgs();
        switch (gr.mo2417hq() - BaseShipType._m_methodCount) {
            case 0:
                return m7337Wa();
            case 1:
                m7395h((CargoHoldType) args[0]);
                return null;
            case 2:
                return m7326RX();
            case 3:
                m7352b((DatabaseCategory) args[0]);
                return null;
            case 4:
                m7343a((TaikopediaEntry) args[0]);
                return null;
            case 5:
                m7353c((TaikopediaEntry) args[0]);
                return null;
            case 6:
                return bgE();
            case 7:
                return bgG();
            case 8:
                return bka();
            case 9:
                m7378f((CollisionFXSet) args[0]);
                return null;
            case 10:
                return bkc();
            case 11:
                m7375f((CruiseSpeedType) args[0]);
                return null;
            case 12:
                return bke();
            case 13:
                m7346aP((Asset) args[0]);
                return null;
            case 14:
                return bkg();
            case 15:
                m7347aR((Asset) args[0]);
                return null;
            case 16:
                return bki();
            case 17:
                m7349aT((Asset) args[0]);
                return null;
            case 18:
                return m7339Wm();
            case 19:
                m7397iG((I18NString) args[0]);
                return null;
            case 20:
                return bkk();
            case 21:
                m7376f((LootType) args[0]);
                return null;
            case 22:
                return new Float(m7330VE());
            case 23:
                m7383gC(((Float) args[0]).floatValue());
                return null;
            case 24:
                return new Float(m7331VG());
            case 25:
                m7384gD(((Float) args[0]).floatValue());
                return null;
            case 26:
                return new Float(bkm());
            case 27:
                m7385gI(((Float) args[0]).floatValue());
                return null;
            case 28:
                return new Float(bko());
            case 29:
                m7386gK(((Float) args[0]).floatValue());
                return null;
            case 30:
                return new Float(m7332VI());
            case 31:
                m7381gA(((Float) args[0]).floatValue());
                return null;
            case 32:
                return new Float(m7333VJ());
            case 33:
                m7382gB(((Float) args[0]).floatValue());
                return null;
            case 34:
                return new Float(bkq());
            case 35:
                m7387gM(((Float) args[0]).floatValue());
                return null;
            case 36:
                return new Float(bks());
            case 37:
                m7388gO(((Float) args[0]).floatValue());
                return null;
            case 38:
                return new Float(bku());
            case 39:
                m7389gQ(((Float) args[0]).floatValue());
                return null;
            case 40:
                return bkw();
            case 41:
                m7350aV((Asset) args[0]);
                return null;
            case 42:
                return new Float(bky());
            case 43:
                m7390gS(((Float) args[0]).floatValue());
                return null;
            case 44:
                return new Float(ago());
            case 45:
                m7391gU(((Float) args[0]).floatValue());
                return null;
            case 46:
                return m7322Nt();
            case 47:
                m7314H((Asset) args[0]);
                return null;
            case 48:
                return new Float(aiq());
            case 49:
                m7392gW(((Float) args[0]).floatValue());
                return null;
            case 50:
                m7373e((ShipStructureType) args[0]);
                return null;
            case 51:
                m7379g((ShipStructureType) args[0]);
                return null;
            case 52:
                return ags();
            case 53:
                return bkA();
            case 54:
                return m7399it();
            case 55:
                m7321M((String) args[0]);
                return null;
            case 56:
                m7344a((C3315qE) args[0]);
                return null;
            case 57:
                m7354c((C3315qE) args[0]);
                return null;
            case 58:
                return m7338Wk();
            case 59:
                return bkC();
            case 60:
                return ais();
            case 61:
                m7313H((Vec3d) args[0]);
                return null;
            case 62:
                return aiu();
            case 63:
                m7316J((Vec3d) args[0]);
                return null;
            case 64:
                return aiw();
            case 65:
                m7318L((Vec3d) args[0]);
                return null;
            case 66:
                return new Float(aiy());
            case 67:
                m7393gY(((Float) args[0]).floatValue());
                return null;
            case 68:
                return new Float(aiz());
            case 69:
                m7394gZ(((Float) args[0]).floatValue());
                return null;
            case 70:
                return new Float(m7327Sf());
            case 71:
                m7356cy(((Float) args[0]).floatValue());
                return null;
            case 72:
                return new Float(bkE());
            case 73:
                m7396ha(((Float) args[0]).floatValue());
                return null;
            case 74:
                return m7335VS();
            case 75:
                m7380g((HullType) args[0]);
                return null;
            case 76:
                m7372e((aDJ) args[0]);
                return null;
            case 77:
                return m7405ve();
            case 78:
                return m7348aT();
            case 79:
                return m7334VK();
            case 80:
                return m7325RV();
            default:
                return super.mo14a(gr);
        }
    }

    /* renamed from: a */
    public void mo15a(Collection collection, C0495Gr gr) {
        super.mo15a(collection, gr);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "YBottom Factor")
    /* renamed from: aE */
    public void mo4159aE(float f) {
        switch (bFf().mo6893i(dHw)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, dHw, new Object[]{new Float(f)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, dHw, new Object[]{new Float(f)}));
                break;
        }
        m7394gZ(f);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "YOffset Factor")
    /* renamed from: aF */
    public void mo4160aF(float f) {
        switch (bFf().mo6893i(dHv)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, dHv, new Object[]{new Float(f)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, dHv, new Object[]{new Float(f)}));
                break;
        }
        m7393gY(f);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Engine S F X")
    /* renamed from: aQ */
    public void mo4161aQ(Asset tCVar) {
        switch (bFf().mo6893i(dGP)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, dGP, new Object[]{tCVar}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, dGP, new Object[]{tCVar}));
                break;
        }
        m7346aP(tCVar);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Explosion G F X")
    /* renamed from: aS */
    public void mo4162aS(Asset tCVar) {
        switch (bFf().mo6893i(dGR)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, dGR, new Object[]{tCVar}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, dGR, new Object[]{tCVar}));
                break;
        }
        m7347aR(tCVar);
    }

    /* access modifiers changed from: protected */
    /* renamed from: aU */
    public <T extends aDJ> T mo745aU() {
        switch (bFf().mo6893i(f1217dN)) {
            case 0:
                return null;
            case 2:
                return (aDJ) bFf().mo5606d(new aCE(this, f1217dN, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f1217dN, new Object[0]));
                break;
        }
        return m7348aT();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Explosion S F X")
    /* renamed from: aU */
    public void mo4163aU(Asset tCVar) {
        switch (bFf().mo6893i(dGT)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, dGT, new Object[]{tCVar}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, dGT, new Object[]{tCVar}));
                break;
        }
        m7349aT(tCVar);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Physical Asset")
    /* renamed from: aW */
    public void mo4164aW(Asset tCVar) {
        switch (bFf().mo6893i(dHi)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, dHi, new Object[]{tCVar}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, dHi, new Object[]{tCVar}));
                break;
        }
        m7350aV(tCVar);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Radar Range")
    public float agp() {
        switch (bFf().mo6893i(bpZ)) {
            case 0:
                return 0.0f;
            case 2:
                return ((Float) bFf().mo5606d(new aCE(this, bpZ, new Object[0]))).floatValue();
            case 3:
                bFf().mo5606d(new aCE(this, bpZ, new Object[0]));
                break;
        }
        return ago();
    }

    public C3438ra<ShipStructureType> agt() {
        switch (bFf().mo6893i(bqc)) {
            case 0:
                return null;
            case 2:
                return (C3438ra) bFf().mo5606d(new aCE(this, bqc, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, bqc, new Object[0]));
                break;
        }
        return ags();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Aim Delay Multiplier")
    public float air() {
        switch (bFf().mo6893i(bsd)) {
            case 0:
                return 0.0f;
            case 2:
                return ((Float) bFf().mo5606d(new aCE(this, bsd, new Object[0]))).floatValue();
            case 3:
                bFf().mo5606d(new aCE(this, bsd, new Object[0]));
                break;
        }
        return aiq();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Camera Position Offset")
    public Vec3d ait() {
        switch (bFf().mo6893i(bsg)) {
            case 0:
                return null;
            case 2:
                return (Vec3d) bFf().mo5606d(new aCE(this, bsg, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, bsg, new Object[0]));
                break;
        }
        return ais();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Camera Look At Offset")
    public Vec3d aiv() {
        switch (bFf().mo6893i(bsh)) {
            case 0:
                return null;
            case 2:
                return (Vec3d) bFf().mo5606d(new aCE(this, bsh, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, bsh, new Object[0]));
                break;
        }
        return aiu();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Camera Sliding Offset")
    public Vec3d aix() {
        switch (bFf().mo6893i(bsi)) {
            case 0:
                return null;
            case 2:
                return (Vec3d) bFf().mo5606d(new aCE(this, bsi, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, bsi, new Object[0]));
                break;
        }
        return aiw();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Max Angular Velocity")
    /* renamed from: as */
    public void mo4171as(float f) {
        switch (bFf().mo6893i(dGB)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, dGB, new Object[]{new Float(f)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, dGB, new Object[]{new Float(f)}));
                break;
        }
        m7384gD(f);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Max Angular Acceleration")
    /* renamed from: at */
    public void mo4172at(float f) {
        switch (bFf().mo6893i(dGA)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, dGA, new Object[]{new Float(f)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, dGA, new Object[]{new Float(f)}));
                break;
        }
        m7383gC(f);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Max Linear Acceleration")
    /* renamed from: au */
    public void mo4173au(float f) {
        switch (bFf().mo6893i(dGy)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, dGy, new Object[]{new Float(f)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, dGy, new Object[]{new Float(f)}));
                break;
        }
        m7381gA(f);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Max Linear Velocity")
    /* renamed from: av */
    public void mo4174av(float f) {
        switch (bFf().mo6893i(dGz)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, dGz, new Object[]{new Float(f)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, dGz, new Object[]{new Float(f)}));
                break;
        }
        m7382gB(f);
    }

    @C3248pc(aYR = C3248pc.C3250b.ADDER, displayName = "Taikopedia Entries")
    /* renamed from: b */
    public void mo4175b(TaikopediaEntry aiz) {
        switch (bFf().mo6893i(aMl)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, aMl, new Object[]{aiz}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, aMl, new Object[]{aiz}));
                break;
        }
        m7343a(aiz);
    }

    @C3248pc(aYR = C3248pc.C3250b.ADDER, displayName = "Turret Slots")
    /* renamed from: b */
    public void mo4176b(C3315qE qEVar) {
        switch (bFf().mo6893i(aTp)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, aTp, new Object[]{qEVar}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, aTp, new Object[]{qEVar}));
                break;
        }
        m7344a(qEVar);
    }

    public C3438ra<TaikopediaEntry> bgF() {
        switch (bFf().mo6893i(dyZ)) {
            case 0:
                return null;
            case 2:
                return (C3438ra) bFf().mo5606d(new aCE(this, dyZ, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, dyZ, new Object[0]));
                break;
        }
        return bgE();
    }

    @C3248pc(aYR = C3248pc.C3250b.ITERATOR, displayName = "Taikopedia Entries")
    public List<TaikopediaEntry> bgH() {
        switch (bFf().mo6893i(dza)) {
            case 0:
                return null;
            case 2:
                return (List) bFf().mo5606d(new aCE(this, dza, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, dza, new Object[0]));
                break;
        }
        return bgG();
    }

    @C3248pc(aYR = C3248pc.C3250b.ITERATOR, displayName = "Ship Structures")
    public List<ShipStructureType> bkB() {
        switch (bFf().mo6893i(dHp)) {
            case 0:
                return null;
            case 2:
                return (List) bFf().mo5606d(new aCE(this, dHp, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, dHp, new Object[0]));
                break;
        }
        return bkA();
    }

    @C3248pc(aYR = C3248pc.C3250b.ITERATOR, displayName = "Turret Slots")
    public List<C3315qE> bkD() {
        switch (bFf().mo6893i(dHr)) {
            case 0:
                return null;
            case 2:
                return (List) bFf().mo5606d(new aCE(this, dHr, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, dHr, new Object[0]));
                break;
        }
        return bkC();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Explosion delay (secs)")
    public float bkF() {
        switch (bFf().mo6893i(dHx)) {
            case 0:
                return 0.0f;
            case 2:
                return ((Float) bFf().mo5606d(new aCE(this, dHx, new Object[0]))).floatValue();
            case 3:
                bFf().mo5606d(new aCE(this, dHx, new Object[0]));
                break;
        }
        return bkE();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Collisions FX")
    public CollisionFXSet bkb() {
        switch (bFf().mo6893i(dGK)) {
            case 0:
                return null;
            case 2:
                return (CollisionFXSet) bFf().mo5606d(new aCE(this, dGK, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, dGK, new Object[0]));
                break;
        }
        return bka();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Cruise Speed")
    public CruiseSpeedType bkd() {
        switch (bFf().mo6893i(dGM)) {
            case 0:
                return null;
            case 2:
                return (CruiseSpeedType) bFf().mo5606d(new aCE(this, dGM, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, dGM, new Object[0]));
                break;
        }
        return bkc();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Engine S F X")
    public Asset bkf() {
        switch (bFf().mo6893i(dGO)) {
            case 0:
                return null;
            case 2:
                return (Asset) bFf().mo5606d(new aCE(this, dGO, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, dGO, new Object[0]));
                break;
        }
        return bke();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Explosion G F X")
    public Asset bkh() {
        switch (bFf().mo6893i(dGQ)) {
            case 0:
                return null;
            case 2:
                return (Asset) bFf().mo5606d(new aCE(this, dGQ, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, dGQ, new Object[0]));
                break;
        }
        return bkg();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Explosion S F X")
    public Asset bkj() {
        switch (bFf().mo6893i(dGS)) {
            case 0:
                return null;
            case 2:
                return (Asset) bFf().mo5606d(new aCE(this, dGS, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, dGS, new Object[0]));
                break;
        }
        return bki();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Loot Type")
    public LootType bkl() {
        switch (bFf().mo6893i(dGV)) {
            case 0:
                return null;
            case 2:
                return (LootType) bFf().mo5606d(new aCE(this, dGV, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, dGV, new Object[0]));
                break;
        }
        return bkk();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Max Angular Velocity Pitch Multiplier")
    public float bkn() {
        switch (bFf().mo6893i(dGX)) {
            case 0:
                return 0.0f;
            case 2:
                return ((Float) bFf().mo5606d(new aCE(this, dGX, new Object[0]))).floatValue();
            case 3:
                bFf().mo5606d(new aCE(this, dGX, new Object[0]));
                break;
        }
        return bkm();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Max Angular Velocity Roll Multiplier")
    public float bkp() {
        switch (bFf().mo6893i(dGZ)) {
            case 0:
                return 0.0f;
            case 2:
                return ((Float) bFf().mo5606d(new aCE(this, dGZ, new Object[0]))).floatValue();
            case 3:
                bFf().mo5606d(new aCE(this, dGZ, new Object[0]));
                break;
        }
        return bko();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Drifting Correction")
    public float bkr() {
        switch (bFf().mo6893i(dHb)) {
            case 0:
                return 0.0f;
            case 2:
                return ((Float) bFf().mo5606d(new aCE(this, dHb, new Object[0]))).floatValue();
            case 3:
                bFf().mo5606d(new aCE(this, dHb, new Object[0]));
                break;
        }
        return bkq();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Drift Inertial Correction")
    public float bkt() {
        switch (bFf().mo6893i(dHd)) {
            case 0:
                return 0.0f;
            case 2:
                return ((Float) bFf().mo5606d(new aCE(this, dHd, new Object[0]))).floatValue();
            case 3:
                bFf().mo5606d(new aCE(this, dHd, new Object[0]));
                break;
        }
        return bks();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Orbital Camera Distance\n(Default is Camera Distance)")
    public float bkv() {
        switch (bFf().mo6893i(dHf)) {
            case 0:
                return 0.0f;
            case 2:
                return ((Float) bFf().mo5606d(new aCE(this, dHf, new Object[0]))).floatValue();
            case 3:
                bFf().mo5606d(new aCE(this, dHf, new Object[0]));
                break;
        }
        return bku();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Physical Asset")
    public Asset bkx() {
        switch (bFf().mo6893i(dHh)) {
            case 0:
                return null;
            case 2:
                return (Asset) bFf().mo5606d(new aCE(this, dHh, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, dHh, new Object[0]));
                break;
        }
        return bkw();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Camera Y Position on Screen\n(Default 0.0f)")
    public float bkz() {
        switch (bFf().mo6893i(dHj)) {
            case 0:
                return 0.0f;
            case 2:
                return ((Float) bFf().mo5606d(new aCE(this, dHj, new Object[0]))).floatValue();
            case 3:
                bFf().mo5606d(new aCE(this, dHj, new Object[0]));
                break;
        }
        return bky();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Database Category")
    /* renamed from: c */
    public void mo4195c(DatabaseCategory aik) {
        switch (bFf().mo6893i(aMo)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, aMo, new Object[]{aik}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, aMo, new Object[]{aik}));
                break;
        }
        m7352b(aik);
    }

    /* renamed from: c */
    public C5663aRz[] mo25c() {
        return _m_fields;
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Pda M P Multiplier")
    /* renamed from: cz */
    public void mo4196cz(float f) {
        switch (bFf().mo6893i(aMv)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, aMv, new Object[]{new Float(f)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, aMv, new Object[]{new Float(f)}));
                break;
        }
        m7356cy(f);
    }

    @C3248pc(aYR = C3248pc.C3250b.REMOVER, displayName = "Taikopedia Entries")
    /* renamed from: d */
    public void mo4197d(TaikopediaEntry aiz) {
        switch (bFf().mo6893i(aMm)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, aMm, new Object[]{aiz}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, aMm, new Object[]{aiz}));
                break;
        }
        m7353c(aiz);
    }

    @C3248pc(aYR = C3248pc.C3250b.REMOVER, displayName = "Turret Slots")
    /* renamed from: d */
    public void mo4198d(C3315qE qEVar) {
        switch (bFf().mo6893i(dHq)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, dHq, new Object[]{qEVar}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, dHq, new Object[]{qEVar}));
                break;
        }
        m7354c(qEVar);
    }

    /* renamed from: f */
    public void mo2631f(aDJ adj) {
        switch (bFf().mo6893i(aDR)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, aDR, new Object[]{adj}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, aDR, new Object[]{adj}));
                break;
        }
        m7372e(adj);
    }

    @C3248pc(aYR = C3248pc.C3250b.ADDER, displayName = "Ship Structures")
    /* renamed from: f */
    public void mo4199f(ShipStructureType aks) {
        switch (bFf().mo6893i(dHn)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, dHn, new Object[]{aks}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, dHn, new Object[]{aks}));
                break;
        }
        m7373e(aks);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Cruise Speed")
    /* renamed from: g */
    public void mo4200g(CruiseSpeedType nf) {
        switch (bFf().mo6893i(dGN)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, dGN, new Object[]{nf}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, dGN, new Object[]{nf}));
                break;
        }
        m7375f(nf);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Loot Type")
    /* renamed from: g */
    public void mo4201g(LootType ahc) {
        switch (bFf().mo6893i(dGW)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, dGW, new Object[]{ahc}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, dGW, new Object[]{ahc}));
                break;
        }
        m7376f(ahc);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Collisions FX")
    /* renamed from: g */
    public void mo4202g(CollisionFXSet yaVar) {
        switch (bFf().mo6893i(dGL)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, dGL, new Object[]{yaVar}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, dGL, new Object[]{yaVar}));
                break;
        }
        m7378f(yaVar);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Max Angular Velocity Pitch Multiplier")
    /* renamed from: gJ */
    public void mo4203gJ(float f) {
        switch (bFf().mo6893i(dGY)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, dGY, new Object[]{new Float(f)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, dGY, new Object[]{new Float(f)}));
                break;
        }
        m7385gI(f);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Max Angular Velocity Roll Multiplier")
    /* renamed from: gL */
    public void mo4204gL(float f) {
        switch (bFf().mo6893i(dHa)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, dHa, new Object[]{new Float(f)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, dHa, new Object[]{new Float(f)}));
                break;
        }
        m7386gK(f);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Drifting Correction")
    /* renamed from: gN */
    public void mo4205gN(float f) {
        switch (bFf().mo6893i(dHc)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, dHc, new Object[]{new Float(f)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, dHc, new Object[]{new Float(f)}));
                break;
        }
        m7387gM(f);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Drift Inertial Correction")
    /* renamed from: gP */
    public void mo4206gP(float f) {
        switch (bFf().mo6893i(dHe)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, dHe, new Object[]{new Float(f)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, dHe, new Object[]{new Float(f)}));
                break;
        }
        m7388gO(f);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Orbital Camera Distance\n(Default is Camera Distance)")
    /* renamed from: gR */
    public void mo4207gR(float f) {
        switch (bFf().mo6893i(dHg)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, dHg, new Object[]{new Float(f)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, dHg, new Object[]{new Float(f)}));
                break;
        }
        m7389gQ(f);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Camera Y Position on Screen\n(Default 0.0f)")
    /* renamed from: gT */
    public void mo4208gT(float f) {
        switch (bFf().mo6893i(dHk)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, dHk, new Object[]{new Float(f)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, dHk, new Object[]{new Float(f)}));
                break;
        }
        m7390gS(f);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Radar Range")
    /* renamed from: gV */
    public void mo4209gV(float f) {
        switch (bFf().mo6893i(dHl)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, dHl, new Object[]{new Float(f)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, dHl, new Object[]{new Float(f)}));
                break;
        }
        m7391gU(f);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Aim Delay Multiplier")
    /* renamed from: gX */
    public void mo4210gX(float f) {
        switch (bFf().mo6893i(dHm)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, dHm, new Object[]{new Float(f)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, dHm, new Object[]{new Float(f)}));
                break;
        }
        m7392gW(f);
    }

    @C3248pc(aYR = C3248pc.C3250b.REMOVER, displayName = "Ship Structures")
    /* renamed from: h */
    public void mo4211h(ShipStructureType aks) {
        switch (bFf().mo6893i(dHo)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, dHo, new Object[]{aks}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, dHo, new Object[]{aks}));
                break;
        }
        m7379g(aks);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Hull")
    /* renamed from: h */
    public void mo4212h(HullType wHVar) {
        switch (bFf().mo6893i(dHz)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, dHz, new Object[]{wHVar}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, dHz, new Object[]{wHVar}));
                break;
        }
        m7380g(wHVar);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Explosion delay (secs)")
    /* renamed from: hb */
    public void mo4213hb(float f) {
        switch (bFf().mo6893i(dHy)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, dHy, new Object[]{new Float(f)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, dHy, new Object[]{new Float(f)}));
                break;
        }
        m7396ha(f);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Cargo Hold")
    /* renamed from: i */
    public void mo4214i(CargoHoldType arb) {
        switch (bFf().mo6893i(dGJ)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, dGJ, new Object[]{arb}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, dGJ, new Object[]{arb}));
                break;
        }
        m7395h(arb);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Long description")
    /* renamed from: iH */
    public void mo4215iH(I18NString i18NString) {
        switch (bFf().mo6893i(dGU)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, dGU, new Object[]{i18NString}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, dGU, new Object[]{i18NString}));
                break;
        }
        m7397iG(i18NString);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Surface Material")
    /* renamed from: iu */
    public String mo4216iu() {
        switch (bFf().mo6893i(f1225vi)) {
            case 0:
                return null;
            case 2:
                return (String) bFf().mo5606d(new aCE(this, f1225vi, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f1225vi, new Object[0]));
                break;
        }
        return m7399it();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Max Linear Velocity")
    /* renamed from: ra */
    public float mo4217ra() {
        switch (bFf().mo6893i(aTh)) {
            case 0:
                return 0.0f;
            case 2:
                return ((Float) bFf().mo5606d(new aCE(this, aTh, new Object[0]))).floatValue();
            case 3:
                bFf().mo5606d(new aCE(this, aTh, new Object[0]));
                break;
        }
        return m7333VJ();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Max Linear Acceleration")
    /* renamed from: rb */
    public float mo4218rb() {
        switch (bFf().mo6893i(aTg)) {
            case 0:
                return 0.0f;
            case 2:
                return ((Float) bFf().mo5606d(new aCE(this, aTg, new Object[0]))).floatValue();
            case 3:
                bFf().mo5606d(new aCE(this, aTg, new Object[0]));
                break;
        }
        return m7332VI();
    }

    /* renamed from: vf */
    public Ship mo4219vf() {
        switch (bFf().mo6893i(f1208Pr)) {
            case 0:
                return null;
            case 2:
                return (Ship) bFf().mo5606d(new aCE(this, f1208Pr, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f1208Pr, new Object[0]));
                break;
        }
        return m7405ve();
    }

    /* renamed from: S */
    public void mo10S() {
        super.mo10S();
        m7361dE(0.0f);
        m7362dF(0.7f);
        m7363dG(0.25f);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Cargo Hold")
    @C0064Am(aul = "f0389975cf8b69598f10d7606266620d", aum = 0)
    /* renamed from: Wa */
    private CargoHoldType m7337Wa() {
        return aeI();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Cargo Hold")
    @C0064Am(aul = "43ca44b61070bb5777c42fa5b10ee276", aum = 0)
    /* renamed from: h */
    private void m7395h(CargoHoldType arb) {
        m7351b(arb);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Database Category")
    @C0064Am(aul = "9ed8e7f438dfcc47f162ecffe34e03e4", aum = 0)
    /* renamed from: RX */
    private DatabaseCategory m7326RX() {
        return m7323RR();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Database Category")
    @C0064Am(aul = "8277a638ef70195ea30d72d9a880265c", aum = 0)
    /* renamed from: b */
    private void m7352b(DatabaseCategory aik) {
        m7342a(aik);
    }

    @C3248pc(aYR = C3248pc.C3250b.ADDER, displayName = "Taikopedia Entries")
    @C0064Am(aul = "dbc4db88322b79327aa485824ab6dca4", aum = 0)
    /* renamed from: a */
    private void m7343a(TaikopediaEntry aiz) {
        m7320Ll().add(aiz);
    }

    @C3248pc(aYR = C3248pc.C3250b.REMOVER, displayName = "Taikopedia Entries")
    @C0064Am(aul = "d1299d423a03caf57c5c2c8e9f2c9f82", aum = 0)
    /* renamed from: c */
    private void m7353c(TaikopediaEntry aiz) {
        m7320Ll().remove(aiz);
    }

    @C0064Am(aul = "31c8a497c01afb23949928879ee398b7", aum = 0)
    private C3438ra<TaikopediaEntry> bgE() {
        return m7320Ll();
    }

    @C3248pc(aYR = C3248pc.C3250b.ITERATOR, displayName = "Taikopedia Entries")
    @C0064Am(aul = "645b4b49c764836f1ed0d477dd1d5b53", aum = 0)
    private List<TaikopediaEntry> bgG() {
        return Collections.unmodifiableList(m7320Ll());
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Collisions FX")
    @C0064Am(aul = "56a8a5066f487b7e750862173c529aaf", aum = 0)
    private CollisionFXSet bka() {
        return aeJ();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Collisions FX")
    @C0064Am(aul = "3464ccdd97ec3423fccb4bacab63a5d5", aum = 0)
    /* renamed from: f */
    private void m7378f(CollisionFXSet yaVar) {
        m7345a(yaVar);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Cruise Speed")
    @C0064Am(aul = "73d87abe524f7aca907a08bbd5b0e005", aum = 0)
    private CruiseSpeedType bkc() {
        return aeK();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Cruise Speed")
    @C0064Am(aul = "53a4787a138062f1a868ea9318d4bab3", aum = 0)
    /* renamed from: f */
    private void m7375f(CruiseSpeedType nf) {
        m7340a(nf);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Engine S F X")
    @C0064Am(aul = "4b8c445336bf677a29cdafa3a85c27e9", aum = 0)
    private Asset bke() {
        return aeL();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Engine S F X")
    @C0064Am(aul = "db78838192b207cc91feb793a43295d1", aum = 0)
    /* renamed from: aP */
    private void m7346aP(Asset tCVar) {
        m7329V(tCVar);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Explosion G F X")
    @C0064Am(aul = "2b441592015b3191c4b4cd2bed094f41", aum = 0)
    private Asset bkg() {
        return m7408zl();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Explosion G F X")
    @C0064Am(aul = "4d77c01d6b22be5426e97eddbca21938", aum = 0)
    /* renamed from: aR */
    private void m7347aR(Asset tCVar) {
        m7407z(tCVar);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Explosion S F X")
    @C0064Am(aul = "7a2358a2256ad029a05d5138233bbbce", aum = 0)
    private Asset bki() {
        return m7409zm();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Explosion S F X")
    @C0064Am(aul = "2ef3044be89f514cc18198397b171c9b", aum = 0)
    /* renamed from: aT */
    private void m7349aT(Asset tCVar) {
        m7312A(tCVar);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Long description")
    @C0064Am(aul = "4458be2ce8d4b2d271e178c3d312cedd", aum = 0)
    /* renamed from: Wm */
    private I18NString m7339Wm() {
        return aeM();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Long description")
    @C0064Am(aul = "88db32192f03dee62a8fb80f188523d5", aum = 0)
    /* renamed from: iG */
    private void m7397iG(I18NString i18NString) {
        m7374eF(i18NString);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Loot Type")
    @C0064Am(aul = "b5b406a23b76e3a2754fa35e6264292a", aum = 0)
    private LootType bkk() {
        return aeN();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Loot Type")
    @C0064Am(aul = "a3adf7f3371954b71fbfb4ed2da219e0", aum = 0)
    /* renamed from: f */
    private void m7376f(LootType ahc) {
        m7341a(ahc);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Max Angular Acceleration")
    @C0064Am(aul = "246ccf7700d6b8414732c67b1e50c06d", aum = 0)
    /* renamed from: VE */
    private float m7330VE() {
        return aeO();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Max Angular Acceleration")
    @C0064Am(aul = "778fef8bf0ad09736407df80c9e5c460", aum = 0)
    /* renamed from: gC */
    private void m7383gC(float f) {
        m7365dt(f);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Max Angular Velocity")
    @C0064Am(aul = "f21ef6555808204171cb1936be8f514a", aum = 0)
    /* renamed from: VG */
    private float m7331VG() {
        return aeP();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Max Angular Velocity")
    @C0064Am(aul = "236c48b83fe029528f7f969b4ae1a85d", aum = 0)
    /* renamed from: gD */
    private void m7384gD(float f) {
        m7366du(f);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Max Angular Velocity Pitch Multiplier")
    @C0064Am(aul = "3799663b6caefa00f855095a2521f5de", aum = 0)
    private float bkm() {
        return aeQ();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Max Angular Velocity Pitch Multiplier")
    @C0064Am(aul = "42292b44e2e81e1d8d866002f976f55b", aum = 0)
    /* renamed from: gI */
    private void m7385gI(float f) {
        m7367dv(f);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Max Angular Velocity Roll Multiplier")
    @C0064Am(aul = "42a833098011d7179fe06c68d0fc1ce3", aum = 0)
    private float bko() {
        return aeR();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Max Angular Velocity Roll Multiplier")
    @C0064Am(aul = "4a8ef5e4fd44d5743898383436e44db3", aum = 0)
    /* renamed from: gK */
    private void m7386gK(float f) {
        m7368dw(f);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Max Linear Acceleration")
    @C0064Am(aul = "8c8c9dbb1d463a9ecf223143004bfd1c", aum = 0)
    /* renamed from: VI */
    private float m7332VI() {
        return aeS();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Max Linear Acceleration")
    @C0064Am(aul = "53a788c812ecc849519468a8f043e4d9", aum = 0)
    /* renamed from: gA */
    private void m7381gA(float f) {
        m7369dx(f);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Max Linear Velocity")
    @C0064Am(aul = "d0dc03f87a4f000e545a58606090ce77", aum = 0)
    /* renamed from: VJ */
    private float m7333VJ() {
        return aeT();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Max Linear Velocity")
    @C0064Am(aul = "8ecee81037747f896e87c1dc45baef10", aum = 0)
    /* renamed from: gB */
    private void m7382gB(float f) {
        m7370dy(f);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Drifting Correction")
    @C0064Am(aul = "b4a76c29ba78f7be48d76be20a44f024", aum = 0)
    private float bkq() {
        return aeU();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Drifting Correction")
    @C0064Am(aul = "8941105b2f462120174de38e58563231", aum = 0)
    /* renamed from: gM */
    private void m7387gM(float f) {
        m7371dz(f);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Drift Inertial Correction")
    @C0064Am(aul = "dc6f8557c10070cb1a5a6e0180908133", aum = 0)
    private float bks() {
        return aeV();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Drift Inertial Correction")
    @C0064Am(aul = "0ffd9808b160d1caa2588a5df9b1bf02", aum = 0)
    /* renamed from: gO */
    private void m7388gO(float f) {
        m7357dA(f);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Orbital Camera Distance\n(Default is Camera Distance)")
    @C0064Am(aul = "1a42e887f2aed43b47bed84a6fc546c8", aum = 0)
    private float bku() {
        return aeW();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Orbital Camera Distance\n(Default is Camera Distance)")
    @C0064Am(aul = "e70d70382675d2a7c08d5de513d2ad48", aum = 0)
    /* renamed from: gQ */
    private void m7389gQ(float f) {
        m7358dB(f);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Physical Asset")
    @C0064Am(aul = "6b5864a42a4c93b962f15586e9057464", aum = 0)
    private Asset bkw() {
        return aeX();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Physical Asset")
    @C0064Am(aul = "7ad090a5bee8c48c77f5147cff94eabf", aum = 0)
    /* renamed from: aV */
    private void m7350aV(Asset tCVar) {
        m7336W(tCVar);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Camera Y Position on Screen\n(Default 0.0f)")
    @C0064Am(aul = "5c736b27d64efde85ca8ba6d43f9db0f", aum = 0)
    private float bky() {
        return aeY();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Camera Y Position on Screen\n(Default 0.0f)")
    @C0064Am(aul = "dfee35c058a544961e5b0c87fed6f0cb", aum = 0)
    /* renamed from: gS */
    private void m7390gS(float f) {
        m7359dC(f);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Radar Range")
    @C0064Am(aul = "f413c26d09aaa13bc7a65127a71d3d2a", aum = 0)
    private float ago() {
        return aeZ();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Radar Range")
    @C0064Am(aul = "8d290cc6493ccf8594dcb46c7803b110", aum = 0)
    /* renamed from: gU */
    private void m7391gU(float f) {
        m7360dD(f);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Render Asset")
    @C0064Am(aul = "e71a93ca9bfffef1861fb3e4af66955c", aum = 0)
    /* renamed from: Nt */
    private Asset m7322Nt() {
        return m7401rh();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Render Asset")
    @C0064Am(aul = "120d4439c6094aa504ec0df954b2b1a4", aum = 0)
    /* renamed from: H */
    private void m7314H(Asset tCVar) {
        m7400r(tCVar);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Aim Delay Multiplier")
    @C0064Am(aul = "095f4fb6a725708da36790aa1e7efa52", aum = 0)
    private float aiq() {
        return afa();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Aim Delay Multiplier")
    @C0064Am(aul = "2551e923ccbb5b1c7872ded84862451d", aum = 0)
    /* renamed from: gW */
    private void m7392gW(float f) {
        m7361dE(f);
    }

    @C3248pc(aYR = C3248pc.C3250b.ADDER, displayName = "Ship Structures")
    @C0064Am(aul = "697d313f9c376f08fb835883e77c7772", aum = 0)
    /* renamed from: e */
    private void m7373e(ShipStructureType aks) {
        afb().add(aks);
    }

    @C3248pc(aYR = C3248pc.C3250b.REMOVER, displayName = "Ship Structures")
    @C0064Am(aul = "4aa9a49c93b1dc31af2e126de2a2f2bd", aum = 0)
    /* renamed from: g */
    private void m7379g(ShipStructureType aks) {
        afb().remove(aks);
    }

    @C0064Am(aul = "f542bd7d3bfcdb737e678f755690ccbe", aum = 0)
    private C3438ra<ShipStructureType> ags() {
        return afb();
    }

    @C3248pc(aYR = C3248pc.C3250b.ITERATOR, displayName = "Ship Structures")
    @C0064Am(aul = "3b4bba15f401a4de2f7bacae386e8a52", aum = 0)
    private List<ShipStructureType> bkA() {
        return Collections.unmodifiableList(afb());
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Surface Material")
    @C0064Am(aul = "c351b86b17ca9e8723918a5f04530d2b", aum = 0)
    /* renamed from: it */
    private String m7399it() {
        return m7398ij();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Surface Material")
    @C0064Am(aul = "fa93a90e9869ca4f913efa53e7729403", aum = 0)
    /* renamed from: M */
    private void m7321M(String str) {
        m7315H(str);
    }

    @C3248pc(aYR = C3248pc.C3250b.ADDER, displayName = "Turret Slots")
    @C0064Am(aul = "42d61c9730e28e2e7d79bc6fe61b8dd7", aum = 0)
    /* renamed from: a */
    private void m7344a(C3315qE qEVar) {
        afc().add(qEVar);
    }

    @C3248pc(aYR = C3248pc.C3250b.REMOVER, displayName = "Turret Slots")
    @C0064Am(aul = "152abe0ef97224a476d96ceaf48378b6", aum = 0)
    /* renamed from: c */
    private void m7354c(C3315qE qEVar) {
        afc().remove(qEVar);
    }

    @C0064Am(aul = "847b033d5a6fd475c2c1bc40bf2dcb5c", aum = 0)
    /* renamed from: Wk */
    private C3438ra<C3315qE> m7338Wk() {
        return afc();
    }

    @C3248pc(aYR = C3248pc.C3250b.ITERATOR, displayName = "Turret Slots")
    @C0064Am(aul = "da9d82442a96668f0b901847ca7d1ce7", aum = 0)
    private List<C3315qE> bkC() {
        return Collections.unmodifiableList(afc());
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Camera Position Offset")
    @C0064Am(aul = "806712038dea51e2d5536643bf7e5c5d", aum = 0)
    private Vec3d ais() {
        return afd();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Camera Position Offset")
    @C0064Am(aul = "a1a4353fcc30eafc81277e142ffa2520", aum = 0)
    /* renamed from: H */
    private void m7313H(Vec3d ajr) {
        m7402s(ajr);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Camera Look At Offset")
    @C0064Am(aul = "d6d65af14ae465fc69ddd1315159a5ff", aum = 0)
    private Vec3d aiu() {
        return afe();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Camera Look At Offset")
    @C0064Am(aul = "1e6b4a311db9a593715e00f36b9da711", aum = 0)
    /* renamed from: J */
    private void m7316J(Vec3d ajr) {
        m7403t(ajr);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Camera Sliding Offset")
    @C0064Am(aul = "3f36e58c4d6d40329c5c6321fef9f7d7", aum = 0)
    private Vec3d aiw() {
        return aff();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Camera Sliding Offset")
    @C0064Am(aul = "7d48a12fc9b8642abcfac3a73a4f4bb5", aum = 0)
    /* renamed from: L */
    private void m7318L(Vec3d ajr) {
        m7404u(ajr);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "YOffset Factor")
    @C0064Am(aul = "afde9a01f27f79cec454cc0835441023", aum = 0)
    private float aiy() {
        return afg();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "YOffset Factor")
    @C0064Am(aul = "deb510e671ae93057260a21eb68759f8", aum = 0)
    /* renamed from: gY */
    private void m7393gY(float f) {
        m7362dF(f);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "YBottom Factor")
    @C0064Am(aul = "bbf7256f2fa0758860a59c188dfea2ee", aum = 0)
    private float aiz() {
        return afh();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "YBottom Factor")
    @C0064Am(aul = "969868900e4fe6fce1b659fe179e5928", aum = 0)
    /* renamed from: gZ */
    private void m7394gZ(float f) {
        m7363dG(f);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Pda M P Multiplier")
    @C0064Am(aul = "beac139a9218aa3b0862c0a5c4e01654", aum = 0)
    /* renamed from: Sf */
    private float m7327Sf() {
        return m7324RS();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Pda M P Multiplier")
    @C0064Am(aul = "e381a5b945fdf24c738afc641f79d502", aum = 0)
    /* renamed from: cy */
    private void m7356cy(float f) {
        m7355cx(f);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Explosion delay (secs)")
    @C0064Am(aul = "1fbb7df75187f702eeeab042caf6051e", aum = 0)
    private float bkE() {
        return afi();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Explosion delay (secs)")
    @C0064Am(aul = "00d252cd34b140b71a8fc25e94f932fa", aum = 0)
    /* renamed from: ha */
    private void m7396ha(float f) {
        m7364dH(f);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Hull")
    @C0064Am(aul = "b16f94772e73c74ce3377cc4d7ad1cab", aum = 0)
    /* renamed from: VS */
    private HullType m7335VS() {
        return bjZ();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Hull")
    @C0064Am(aul = "e9c4b4f4cec7645d05a4f50a819f1b31", aum = 0)
    /* renamed from: g */
    private void m7380g(HullType wHVar) {
        m7377f(wHVar);
    }

    @C0064Am(aul = "29ae8d849c97b7c74ae436ad86a127f8", aum = 0)
    /* renamed from: e */
    private void m7372e(aDJ adj) {
        super.mo2631f(adj);
        if (adj instanceof Ship) {
            C1616Xf xf = adj;
            if (bjZ() != null) {
                xf.mo6765g(C3981xi.aMH, bjZ().mo7459NK());
            }
        }
    }

    @C0064Am(aul = "7fa9adde1fd607cf9c5abae5e39f2551", aum = 0)
    /* renamed from: ve */
    private Ship m7405ve() {
        return (Ship) mo745aU();
    }

    @C0064Am(aul = "9366a8dc9823db9026993ba21ab47cd6", aum = 0)
    /* renamed from: aT */
    private <T extends aDJ> T m7348aT() {
        T t = (Ship) bFf().mo6865M(Ship.class);
        t.mo18318h(this);
        return t;
    }
}
