package game.script.ship;

import game.network.message.externalizable.aCE;
import game.network.message.serializable.C1556Wo;
import game.network.message.serializable.C2686iZ;
import game.script.damage.DamageType;
import logic.baa.*;
import logic.data.mbean.C5571aOl;
import logic.res.code.C5640aRc;
import logic.res.code.C5663aRz;
import logic.res.code.C6494any;
import logic.res.html.C2491fm;
import p001a.*;

import java.util.Collection;
import java.util.Collections;
import java.util.Map;
import java.util.Set;

@C0566Hp
@C5829abJ("2.1.0")
@C6485anp
@C5511aMd
/* renamed from: a.wH */
/* compiled from: a */
public class HullType extends BaseHullType implements C1616Xf {
    public static final int _m_fieldCount = 0;
    public static final C5663aRz[] _m_fields = null;
    public static final int _m_methodCount = 0;
    public static final C2491fm[] _m_methods = null;
    public static final C5663aRz aOp = null;
    public static final C5663aRz aOv = null;
    public static final C5663aRz bEC = null;
    public static final C2491fm bED = null;
    public static final C2491fm bEE = null;
    public static final C2491fm bEF = null;
    public static final C2491fm bEG = null;
    public static final C2491fm bEH = null;
    public static final C2491fm bEI = null;
    public static final C2491fm bEJ = null;
    public static final C2491fm bEK = null;
    /* renamed from: dN */
    public static final C2491fm f9471dN = null;
    /* renamed from: qb */
    public static final C2491fm f9472qb = null;
    public static final long serialVersionUID = 0;
    public static C6494any ___iScriptClass;
    @C0064Am(aul = "5d23cfabe7bf1b78bd152f33901c76e9", aum = 2)
    private static float aOo;
    @C0064Am(aul = "a4cd3543c0569fdfd9d3ec0a32dc4d95", aum = 0)
    private static C1556Wo<DamageType, C1649YI> aOu;
    @C0064Am(aul = "61c35eef423ab410eb01524d1114c103", aum = 1)
    private static C2686iZ<DamageType> bEB;

    static {
        m40545V();
    }

    public HullType() {
        super((C5540aNg) null);
        super.mo10S();
    }

    public HullType(C5540aNg ang) {
        super(ang);
    }

    /* renamed from: V */
    static void m40545V() {
        serialVersionUID = (long) 1;
        _m_fieldCount = BaseHullType._m_fieldCount + 3;
        _m_methodCount = BaseHullType._m_methodCount + 10;
        int i = BaseHullType._m_fieldCount;
        C5663aRz[] arzArr = new C5663aRz[(i + 3)];
        C5663aRz b = C5640aRc.m17844b(HullType.class, "a4cd3543c0569fdfd9d3ec0a32dc4d95", i);
        aOv = b;
        arzArr[i] = b;
        int i2 = i + 1;
        C5663aRz b2 = C5640aRc.m17844b(HullType.class, "61c35eef423ab410eb01524d1114c103", i2);
        bEC = b2;
        arzArr[i2] = b2;
        int i3 = i2 + 1;
        C5663aRz b3 = C5640aRc.m17844b(HullType.class, "5d23cfabe7bf1b78bd152f33901c76e9", i3);
        aOp = b3;
        arzArr[i3] = b3;
        int i4 = i3 + 1;
        _m_fields = arzArr;
        C1634Xv.m11725a((Object[]) BaseHullType._m_fields, (Object[]) _m_fields);
        int i5 = BaseHullType._m_methodCount;
        C2491fm[] fmVarArr = new C2491fm[(i5 + 10)];
        C2491fm a = C4105zY.m41624a(HullType.class, "0d6c356f3574f824029f2455d17a17b7", i5);
        bED = a;
        fmVarArr[i5] = a;
        int i6 = i5 + 1;
        C2491fm a2 = C4105zY.m41624a(HullType.class, "014142a73353085ec5d0a8408ee77441", i6);
        bEE = a2;
        fmVarArr[i6] = a2;
        int i7 = i6 + 1;
        C2491fm a3 = C4105zY.m41624a(HullType.class, "db733be10fffac34d77aeab548aebe86", i7);
        bEF = a3;
        fmVarArr[i7] = a3;
        int i8 = i7 + 1;
        C2491fm a4 = C4105zY.m41624a(HullType.class, "bf4d6685ef65e06e3e237442e12784b9", i8);
        bEG = a4;
        fmVarArr[i8] = a4;
        int i9 = i8 + 1;
        C2491fm a5 = C4105zY.m41624a(HullType.class, "509e6ab8c0e1675998cdd1e8c3edbf1c", i9);
        bEH = a5;
        fmVarArr[i9] = a5;
        int i10 = i9 + 1;
        C2491fm a6 = C4105zY.m41624a(HullType.class, "0bcaf718352bd04142a85974f8f1cf39", i10);
        bEI = a6;
        fmVarArr[i10] = a6;
        int i11 = i10 + 1;
        C2491fm a7 = C4105zY.m41624a(HullType.class, "988972fce128a1714f6b7f1659313053", i11);
        f9472qb = a7;
        fmVarArr[i11] = a7;
        int i12 = i11 + 1;
        C2491fm a8 = C4105zY.m41624a(HullType.class, "558e21075d1f94cb7cd206fbb0a2cece", i12);
        bEJ = a8;
        fmVarArr[i12] = a8;
        int i13 = i12 + 1;
        C2491fm a9 = C4105zY.m41624a(HullType.class, "f5424fac58d2486d642b62a224c52664", i13);
        bEK = a9;
        fmVarArr[i13] = a9;
        int i14 = i13 + 1;
        C2491fm a10 = C4105zY.m41624a(HullType.class, "db6e78d09045ae9d3e02b93125aa845a", i14);
        f9471dN = a10;
        fmVarArr[i14] = a10;
        int i15 = i14 + 1;
        _m_methods = fmVarArr;
        C1634Xv.m11725a((Object[]) BaseHullType._m_methods, (Object[]) _m_methods);
        ___iScriptClass = aUO.m18566a(HullType.class, C5571aOl.class, _m_fields, _m_methods);
    }

    /* renamed from: Ta */
    private float m40543Ta() {
        return bFf().mo5608dq().mo3211m(aOp);
    }

    /* renamed from: Td */
    private C1556Wo m40544Td() {
        return (C1556Wo) bFf().mo5608dq().mo3214p(aOv);
    }

    private C2686iZ amY() {
        return (C2686iZ) bFf().mo5608dq().mo3214p(bEC);
    }

    /* renamed from: cC */
    private void m40548cC(float f) {
        bFf().mo5608dq().mo3150a(aOp, f);
    }

    /* renamed from: m */
    private void m40551m(C1556Wo wo) {
        bFf().mo5608dq().mo3197f(aOv, wo);
    }

    /* renamed from: n */
    private void m40552n(C2686iZ iZVar) {
        bFf().mo5608dq().mo3197f(bEC, iZVar);
    }

    /* renamed from: T */
    public C6494any mo11T() {
        return ___iScriptClass;
    }

    /* renamed from: U */
    public C2491fm[] mo12U() {
        return _m_methods;
    }

    /* renamed from: W */
    public Object mo13W() {
        return new C5571aOl(this);
    }

    /* renamed from: a */
    public Object mo14a(C0495Gr gr) {
        Object[] args = gr.getArgs();
        switch (gr.mo2417hq() - BaseHullType._m_methodCount) {
            case 0:
                return amZ();
            case 1:
                m40547c((Map) args[0]);
                return null;
            case 2:
                m40553o((DamageType) args[0]);
                return null;
            case 3:
                m40554q((DamageType) args[0]);
                return null;
            case 4:
                return anb();
            case 5:
                return and();
            case 6:
                return new Float(m40550hg());
            case 7:
                m40549dW(((Float) args[0]).floatValue());
                return null;
            case 8:
                return anf();
            case 9:
                return m40546aT();
            default:
                return super.mo14a(gr);
        }
    }

    /* renamed from: a */
    public void mo15a(Collection collection, C0495Gr gr) {
        super.mo15a(collection, gr);
    }

    /* access modifiers changed from: protected */
    /* renamed from: aU */
    public <T extends aDJ> T mo745aU() {
        switch (bFf().mo6893i(f9471dN)) {
            case 0:
                return null;
            case 2:
                return (aDJ) bFf().mo5606d(new aCE(this, f9471dN, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f9471dN, new Object[0]));
                break;
        }
        return m40546aT();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Damage Resistance")
    public C1556Wo<DamageType, C1649YI> ana() {
        switch (bFf().mo6893i(bED)) {
            case 0:
                return null;
            case 2:
                return (C1556Wo) bFf().mo5606d(new aCE(this, bED, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, bED, new Object[0]));
                break;
        }
        return amZ();
    }

    public C2686iZ<DamageType> anc() {
        switch (bFf().mo6893i(bEH)) {
            case 0:
                return null;
            case 2:
                return (C2686iZ) bFf().mo5606d(new aCE(this, bEH, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, bEH, new Object[0]));
                break;
        }
        return anb();
    }

    @C3248pc(aYR = C3248pc.C3250b.ITERATOR, displayName = "Invulnerability List")
    public Set<DamageType> ane() {
        switch (bFf().mo6893i(bEI)) {
            case 0:
                return null;
            case 2:
                return (Set) bFf().mo5606d(new aCE(this, bEI, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, bEI, new Object[0]));
                break;
        }
        return and();
    }

    public Hull ang() {
        switch (bFf().mo6893i(bEK)) {
            case 0:
                return null;
            case 2:
                return (Hull) bFf().mo5606d(new aCE(this, bEK, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, bEK, new Object[0]));
                break;
        }
        return anf();
    }

    /* renamed from: c */
    public C5663aRz[] mo25c() {
        return _m_fields;
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Damage Resistance")
    /* renamed from: d */
    public void mo22725d(Map<DamageType, C1649YI> map) {
        switch (bFf().mo6893i(bEE)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, bEE, new Object[]{map}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, bEE, new Object[]{map}));
                break;
        }
        m40547c(map);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Max Health Points")
    /* renamed from: dX */
    public void mo22726dX(float f) {
        switch (bFf().mo6893i(bEJ)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, bEJ, new Object[]{new Float(f)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, bEJ, new Object[]{new Float(f)}));
                break;
        }
        m40549dW(f);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Max Health Points")
    /* renamed from: hh */
    public float mo22727hh() {
        switch (bFf().mo6893i(f9472qb)) {
            case 0:
                return 0.0f;
            case 2:
                return ((Float) bFf().mo5606d(new aCE(this, f9472qb, new Object[0]))).floatValue();
            case 3:
                bFf().mo5606d(new aCE(this, f9472qb, new Object[0]));
                break;
        }
        return m40550hg();
    }

    @C3248pc(aYR = C3248pc.C3250b.ADDER, displayName = "Invulnerability List")
    /* renamed from: p */
    public void mo22728p(DamageType fr) {
        switch (bFf().mo6893i(bEF)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, bEF, new Object[]{fr}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, bEF, new Object[]{fr}));
                break;
        }
        m40553o(fr);
    }

    @C3248pc(aYR = C3248pc.C3250b.REMOVER, displayName = "Invulnerability List")
    /* renamed from: r */
    public void mo22729r(DamageType fr) {
        switch (bFf().mo6893i(bEG)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, bEG, new Object[]{fr}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, bEG, new Object[]{fr}));
                break;
        }
        m40554q(fr);
    }

    /* renamed from: S */
    public void mo10S() {
        super.mo10S();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Damage Resistance")
    @C0064Am(aul = "0d6c356f3574f824029f2455d17a17b7", aum = 0)
    private C1556Wo<DamageType, C1649YI> amZ() {
        return m40544Td();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Damage Resistance")
    @C0064Am(aul = "014142a73353085ec5d0a8408ee77441", aum = 0)
    /* renamed from: c */
    private void m40547c(Map<DamageType, C1649YI> map) {
        m40544Td().clear();
        m40544Td().putAll(map);
    }

    @C3248pc(aYR = C3248pc.C3250b.ADDER, displayName = "Invulnerability List")
    @C0064Am(aul = "db733be10fffac34d77aeab548aebe86", aum = 0)
    /* renamed from: o */
    private void m40553o(DamageType fr) {
        amY().add(fr);
    }

    @C3248pc(aYR = C3248pc.C3250b.REMOVER, displayName = "Invulnerability List")
    @C0064Am(aul = "bf4d6685ef65e06e3e237442e12784b9", aum = 0)
    /* renamed from: q */
    private void m40554q(DamageType fr) {
        amY().remove(fr);
    }

    @C0064Am(aul = "509e6ab8c0e1675998cdd1e8c3edbf1c", aum = 0)
    private C2686iZ<DamageType> anb() {
        return amY();
    }

    @C3248pc(aYR = C3248pc.C3250b.ITERATOR, displayName = "Invulnerability List")
    @C0064Am(aul = "0bcaf718352bd04142a85974f8f1cf39", aum = 0)
    private Set<DamageType> and() {
        return Collections.unmodifiableSet(amY());
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Max Health Points")
    @C0064Am(aul = "988972fce128a1714f6b7f1659313053", aum = 0)
    /* renamed from: hg */
    private float m40550hg() {
        return m40543Ta();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Max Health Points")
    @C0064Am(aul = "558e21075d1f94cb7cd206fbb0a2cece", aum = 0)
    /* renamed from: dW */
    private void m40549dW(float f) {
        m40548cC(f);
    }

    @C0064Am(aul = "f5424fac58d2486d642b62a224c52664", aum = 0)
    private Hull anf() {
        return (Hull) mo745aU();
    }

    @C0064Am(aul = "db6e78d09045ae9d3e02b93125aa845a", aum = 0)
    /* renamed from: aT */
    private <T extends aDJ> T m40546aT() {
        T t = (Hull) bFf().mo6865M(Hull.class);
        t.mo19924a(this);
        return t;
    }
}
