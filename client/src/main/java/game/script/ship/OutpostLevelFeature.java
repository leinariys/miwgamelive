package game.script.ship;

import game.network.message.externalizable.aCE;
import logic.baa.*;
import logic.data.mbean.C3748uV;
import logic.res.code.C5640aRc;
import logic.res.code.C5663aRz;
import logic.res.code.C6494any;
import logic.res.html.C2491fm;
import p001a.*;

import java.util.Collection;
import java.util.UUID;

@C5511aMd
@C6485anp
/* renamed from: a.lz */
/* compiled from: a */
public class OutpostLevelFeature extends aDJ implements C0468GU, C1616Xf {
    public static final C2491fm _f_toString_0020_0028_0029Ljava_002flang_002fString_003b = null;
    public static final int _m_fieldCount = 0;
    public static final C5663aRz[] _m_fields = null;
    public static final int _m_methodCount = 0;
    public static final C2491fm[] _m_methods = null;
    public static final C5663aRz ayH = null;
    public static final C5663aRz ayJ = null;
    public static final C5663aRz ayL = null;
    public static final C2491fm ayM = null;
    public static final C2491fm ayN = null;
    public static final C2491fm ayO = null;
    public static final C2491fm ayP = null;
    /* renamed from: bL */
    public static final C5663aRz f8621bL = null;
    /* renamed from: bM */
    public static final C5663aRz f8622bM = null;
    /* renamed from: bN */
    public static final C2491fm f8623bN = null;
    /* renamed from: bO */
    public static final C2491fm f8624bO = null;
    /* renamed from: bP */
    public static final C2491fm f8625bP = null;
    /* renamed from: bQ */
    public static final C2491fm f8626bQ = null;
    public static final long serialVersionUID = 0;
    public static C6494any ___iScriptClass;
    @C0064Am(aul = "93eeb4d0ca295ce4a7d5711054755131", aum = 0)
    private static C0437GE ayG;
    @C0064Am(aul = "238869e98fced996c4ca820d0f5a2e75", aum = 1)
    private static C2611hZ ayI;
    @C0064Am(aul = "6b4416087b12d503fea56c8cee93cca1", aum = 2)
    private static boolean ayK;
    @C0064Am(aul = "a808f1cf4d82fb3c3a63f2a8ab0ee9a5", aum = 3)

    /* renamed from: bK */
    private static UUID f8620bK;
    @C0064Am(aul = "f04cbf1ff185e4ba44d751254b2176af", aum = 4)
    private static String handle;

    static {
        m35376V();
    }

    public OutpostLevelFeature() {
        super((C5540aNg) null);
        super.mo10S();
    }

    OutpostLevelFeature(C0437GE ge) {
        super((C5540aNg) null);
        super._m_script_init(ge);
    }

    public OutpostLevelFeature(C5540aNg ang) {
        super(ang);
    }

    /* renamed from: V */
    static void m35376V() {
        serialVersionUID = (long) 1;
        _m_fieldCount = aDJ._m_fieldCount + 5;
        _m_methodCount = aDJ._m_methodCount + 9;
        int i = aDJ._m_fieldCount;
        C5663aRz[] arzArr = new C5663aRz[(i + 5)];
        C5663aRz b = C5640aRc.m17844b(OutpostLevelFeature.class, "93eeb4d0ca295ce4a7d5711054755131", i);
        ayH = b;
        arzArr[i] = b;
        int i2 = i + 1;
        C5663aRz b2 = C5640aRc.m17844b(OutpostLevelFeature.class, "238869e98fced996c4ca820d0f5a2e75", i2);
        ayJ = b2;
        arzArr[i2] = b2;
        int i3 = i2 + 1;
        C5663aRz b3 = C5640aRc.m17844b(OutpostLevelFeature.class, "6b4416087b12d503fea56c8cee93cca1", i3);
        ayL = b3;
        arzArr[i3] = b3;
        int i4 = i3 + 1;
        C5663aRz b4 = C5640aRc.m17844b(OutpostLevelFeature.class, "a808f1cf4d82fb3c3a63f2a8ab0ee9a5", i4);
        f8621bL = b4;
        arzArr[i4] = b4;
        int i5 = i4 + 1;
        C5663aRz b5 = C5640aRc.m17844b(OutpostLevelFeature.class, "f04cbf1ff185e4ba44d751254b2176af", i5);
        f8622bM = b5;
        arzArr[i5] = b5;
        int i6 = i5 + 1;
        _m_fields = arzArr;
        C1634Xv.m11725a((Object[]) aDJ._m_fields, (Object[]) _m_fields);
        int i7 = aDJ._m_methodCount;
        C2491fm[] fmVarArr = new C2491fm[(i7 + 9)];
        C2491fm a = C4105zY.m41624a(OutpostLevelFeature.class, "304f984519eb24ef3ec56814e20dc692", i7);
        f8623bN = a;
        fmVarArr[i7] = a;
        int i8 = i7 + 1;
        C2491fm a2 = C4105zY.m41624a(OutpostLevelFeature.class, "6bec0578e47b0deec235b4685fe03898", i8);
        f8624bO = a2;
        fmVarArr[i8] = a2;
        int i9 = i8 + 1;
        C2491fm a3 = C4105zY.m41624a(OutpostLevelFeature.class, "e6830986e108769a5b61cbace02a16cd", i9);
        f8625bP = a3;
        fmVarArr[i9] = a3;
        int i10 = i9 + 1;
        C2491fm a4 = C4105zY.m41624a(OutpostLevelFeature.class, "dab36b0287095fc7d34050caa9adeed8", i10);
        f8626bQ = a4;
        fmVarArr[i10] = a4;
        int i11 = i10 + 1;
        C2491fm a5 = C4105zY.m41624a(OutpostLevelFeature.class, "dc4c5c9037b0427f8d1370e15af1155b", i11);
        _f_toString_0020_0028_0029Ljava_002flang_002fString_003b = a5;
        fmVarArr[i11] = a5;
        int i12 = i11 + 1;
        C2491fm a6 = C4105zY.m41624a(OutpostLevelFeature.class, "b2a749256fa513c0d9d291670790d6a3", i12);
        ayM = a6;
        fmVarArr[i12] = a6;
        int i13 = i12 + 1;
        C2491fm a7 = C4105zY.m41624a(OutpostLevelFeature.class, "14bb7267e072218260d827172175f46a", i13);
        ayN = a7;
        fmVarArr[i13] = a7;
        int i14 = i13 + 1;
        C2491fm a8 = C4105zY.m41624a(OutpostLevelFeature.class, "650066797e511f1d5906715faaffb020", i14);
        ayO = a8;
        fmVarArr[i14] = a8;
        int i15 = i14 + 1;
        C2491fm a9 = C4105zY.m41624a(OutpostLevelFeature.class, "e686957111a3d3529621e74c58e972da", i15);
        ayP = a9;
        fmVarArr[i15] = a9;
        int i16 = i15 + 1;
        _m_methods = fmVarArr;
        C1634Xv.m11725a((Object[]) aDJ._m_methods, (Object[]) _m_methods);
        ___iScriptClass = aUO.m18566a(OutpostLevelFeature.class, C3748uV.class, _m_fields, _m_methods);
    }

    /* renamed from: LX */
    private C0437GE m35371LX() {
        return (C0437GE) bFf().mo5608dq().mo3214p(ayH);
    }

    /* renamed from: LY */
    private C2611hZ m35372LY() {
        return (C2611hZ) bFf().mo5608dq().mo3214p(ayJ);
    }

    /* renamed from: LZ */
    private boolean m35373LZ() {
        return bFf().mo5608dq().mo3201h(ayL);
    }

    /* renamed from: a */
    private void m35377a(C0437GE ge) {
        bFf().mo5608dq().mo3197f(ayH, ge);
    }

    /* renamed from: a */
    private void m35378a(C2611hZ hZVar) {
        bFf().mo5608dq().mo3197f(ayJ, hZVar);
    }

    /* renamed from: a */
    private void m35379a(String str) {
        bFf().mo5608dq().mo3197f(f8622bM, str);
    }

    /* renamed from: a */
    private void m35380a(UUID uuid) {
        bFf().mo5608dq().mo3197f(f8621bL, uuid);
    }

    /* renamed from: an */
    private UUID m35381an() {
        return (UUID) bFf().mo5608dq().mo3214p(f8621bL);
    }

    /* renamed from: ao */
    private String m35382ao() {
        return (String) bFf().mo5608dq().mo3214p(f8622bM);
    }

    /* renamed from: ax */
    private void m35386ax(boolean z) {
        bFf().mo5608dq().mo3153a(ayL, z);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Feature disabled for any Level")
    @C0064Am(aul = "e686957111a3d3529621e74c58e972da", aum = 0)
    @C5566aOg
    /* renamed from: ay */
    private void m35387ay(boolean z) {
        throw new aWi(new aCE(this, ayP, new Object[]{new Boolean(z)}));
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Required Level")
    @C0064Am(aul = "14bb7267e072218260d827172175f46a", aum = 0)
    @C5566aOg
    /* renamed from: b */
    private void m35388b(C2611hZ hZVar) {
        throw new aWi(new aCE(this, ayN, new Object[]{hZVar}));
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Handle")
    @C0064Am(aul = "dab36b0287095fc7d34050caa9adeed8", aum = 0)
    @C5566aOg
    /* renamed from: b */
    private void m35389b(String str) {
        throw new aWi(new aCE(this, f8626bQ, new Object[]{str}));
    }

    /* renamed from: c */
    private void m35391c(UUID uuid) {
        switch (bFf().mo6893i(f8624bO)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f8624bO, new Object[]{uuid}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f8624bO, new Object[]{uuid}));
                break;
        }
        m35390b(uuid);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Required Level")
    /* renamed from: Mb */
    public C2611hZ mo20445Mb() {
        switch (bFf().mo6893i(ayM)) {
            case 0:
                return null;
            case 2:
                return (C2611hZ) bFf().mo5606d(new aCE(this, ayM, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, ayM, new Object[0]));
                break;
        }
        return m35374Ma();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Feature disabled for any Level")
    /* renamed from: Md */
    public boolean mo20446Md() {
        switch (bFf().mo6893i(ayO)) {
            case 0:
                return false;
            case 2:
                return ((Boolean) bFf().mo5606d(new aCE(this, ayO, new Object[0]))).booleanValue();
            case 3:
                bFf().mo5606d(new aCE(this, ayO, new Object[0]));
                break;
        }
        return m35375Mc();
    }

    /* renamed from: S */
    public void mo10S() {
        super.mo10S();
    }

    /* renamed from: T */
    public C6494any mo11T() {
        return ___iScriptClass;
    }

    /* renamed from: U */
    public C2491fm[] mo12U() {
        return _m_methods;
    }

    /* renamed from: W */
    public Object mo13W() {
        return new C3748uV(this);
    }

    /* renamed from: a */
    public Object mo14a(C0495Gr gr) {
        Object[] args = gr.getArgs();
        switch (gr.mo2417hq() - aDJ._m_methodCount) {
            case 0:
                return m35383ap();
            case 1:
                m35390b((UUID) args[0]);
                return null;
            case 2:
                return m35384ar();
            case 3:
                m35389b((String) args[0]);
                return null;
            case 4:
                return m35385au();
            case 5:
                return m35374Ma();
            case 6:
                m35388b((C2611hZ) args[0]);
                return null;
            case 7:
                return new Boolean(m35375Mc());
            case 8:
                m35387ay(((Boolean) args[0]).booleanValue());
                return null;
            default:
                return super.mo14a(gr);
        }
    }

    /* renamed from: a */
    public void mo15a(Collection collection, C0495Gr gr) {
        super.mo15a(collection, gr);
    }

    /* renamed from: aq */
    public UUID mo18aq() {
        switch (bFf().mo6893i(f8623bN)) {
            case 0:
                return null;
            case 2:
                return (UUID) bFf().mo5606d(new aCE(this, f8623bN, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f8623bN, new Object[0]));
                break;
        }
        return m35383ap();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Feature disabled for any Level")
    @C5566aOg
    /* renamed from: az */
    public void mo20447az(boolean z) {
        switch (bFf().mo6893i(ayP)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, ayP, new Object[]{new Boolean(z)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, ayP, new Object[]{new Boolean(z)}));
                break;
        }
        m35387ay(z);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Required Level")
    @C5566aOg
    /* renamed from: c */
    public void mo20449c(C2611hZ hZVar) {
        switch (bFf().mo6893i(ayN)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, ayN, new Object[]{hZVar}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, ayN, new Object[]{hZVar}));
                break;
        }
        m35388b(hZVar);
    }

    /* renamed from: c */
    public C5663aRz[] mo25c() {
        return _m_fields;
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Handle")
    public String getHandle() {
        switch (bFf().mo6893i(f8625bP)) {
            case 0:
                return null;
            case 2:
                return (String) bFf().mo5606d(new aCE(this, f8625bP, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f8625bP, new Object[0]));
                break;
        }
        return m35384ar();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Handle")
    @C5566aOg
    public void setHandle(String str) {
        switch (bFf().mo6893i(f8626bQ)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f8626bQ, new Object[]{str}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f8626bQ, new Object[]{str}));
                break;
        }
        m35389b(str);
    }

    public String toString() {
        switch (bFf().mo6893i(_f_toString_0020_0028_0029Ljava_002flang_002fString_003b)) {
            case 0:
                return null;
            case 2:
                return (String) bFf().mo5606d(new aCE(this, _f_toString_0020_0028_0029Ljava_002flang_002fString_003b, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, _f_toString_0020_0028_0029Ljava_002flang_002fString_003b, new Object[0]));
                break;
        }
        return m35385au();
    }

    @C0064Am(aul = "304f984519eb24ef3ec56814e20dc692", aum = 0)
    /* renamed from: ap */
    private UUID m35383ap() {
        return m35381an();
    }

    @C0064Am(aul = "6bec0578e47b0deec235b4685fe03898", aum = 0)
    /* renamed from: b */
    private void m35390b(UUID uuid) {
        m35380a(uuid);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Handle")
    @C0064Am(aul = "e6830986e108769a5b61cbace02a16cd", aum = 0)
    /* renamed from: ar */
    private String m35384ar() {
        return m35382ao();
    }

    /* access modifiers changed from: package-private */
    /* renamed from: b */
    public void mo20448b(C0437GE ge) {
        super.mo10S();
        m35377a(ge);
        m35378a(C2611hZ.m32750Ac());
        m35380a(UUID.randomUUID());
    }

    @C0064Am(aul = "dc4c5c9037b0427f8d1370e15af1155b", aum = 0)
    /* renamed from: au */
    private String m35385au() {
        return "Required level to enable " + m35371LX();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Required Level")
    @C0064Am(aul = "b2a749256fa513c0d9d291670790d6a3", aum = 0)
    /* renamed from: Ma */
    private C2611hZ m35374Ma() {
        return m35372LY();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Feature disabled for any Level")
    @C0064Am(aul = "650066797e511f1d5906715faaffb020", aum = 0)
    /* renamed from: Mc */
    private boolean m35375Mc() {
        return m35373LZ();
    }
}
