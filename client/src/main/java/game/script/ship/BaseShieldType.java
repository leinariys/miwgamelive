package game.script.ship;

import game.network.message.externalizable.aCE;
import game.network.message.serializable.C1556Wo;
import game.script.damage.DamageType;
import game.script.template.BaseTaikodomContent;
import logic.baa.*;
import logic.data.mbean.C5868abw;
import logic.res.code.C5663aRz;
import logic.res.code.C6494any;
import logic.res.html.C2491fm;
import p001a.*;
import taikodom.infra.script.I18NString;

import java.util.Collection;

@C5511aMd
@C6485anp
/* renamed from: a.aGc  reason: case insensitive filesystem */
/* compiled from: a */
public abstract class BaseShieldType extends BaseTaikodomContent implements C1616Xf, aOW {

    /* renamed from: Do */
    public static final C2491fm f2874Do = null;
    public static final C2491fm _f_onResurrect_0020_0028_0029V = null;
    public static final int _m_fieldCount = 0;
    public static final C5663aRz[] _m_fields = null;
    public static final int _m_methodCount = 0;
    public static final C2491fm[] _m_methods = null;
    public static final C2491fm bED = null;
    public static final long serialVersionUID = 0;
    public static C6494any ___iScriptClass;

    static {
        m14913V();
    }

    public BaseShieldType() {
        super((C5540aNg) null);
        super.mo10S();
    }

    public BaseShieldType(C5540aNg ang) {
        super(ang);
    }

    /* renamed from: V */
    static void m14913V() {
        serialVersionUID = (long) 1;
        _m_fieldCount = BaseTaikodomContent._m_fieldCount + 0;
        _m_methodCount = BaseTaikodomContent._m_methodCount + 3;
        _m_fields = new C5663aRz[(BaseTaikodomContent._m_fieldCount + 0)];
        C1634Xv.m11725a((Object[]) BaseTaikodomContent._m_fields, (Object[]) _m_fields);
        int i = BaseTaikodomContent._m_methodCount;
        C2491fm[] fmVarArr = new C2491fm[(i + 3)];
        C2491fm a = C4105zY.m41624a(BaseShieldType.class, "7e44a76e6085f0a2ad3da7ae87f8ab62", i);
        bED = a;
        fmVarArr[i] = a;
        int i2 = i + 1;
        C2491fm a2 = C4105zY.m41624a(BaseShieldType.class, "62cc2fd0e1ce4b84e837986f7a8d1541", i2);
        _f_onResurrect_0020_0028_0029V = a2;
        fmVarArr[i2] = a2;
        int i3 = i2 + 1;
        C2491fm a3 = C4105zY.m41624a(BaseShieldType.class, "b409fbb1a1ccf6d948d473aeb57902b7", i3);
        f2874Do = a3;
        fmVarArr[i3] = a3;
        int i4 = i3 + 1;
        _m_methods = fmVarArr;
        C1634Xv.m11725a((Object[]) BaseTaikodomContent._m_methods, (Object[]) _m_methods);
        ___iScriptClass = aUO.m18566a(BaseShieldType.class, C5868abw.class, _m_fields, _m_methods);
    }

    @C0064Am(aul = "7e44a76e6085f0a2ad3da7ae87f8ab62", aum = 0)
    private C1556Wo<DamageType, C1649YI> amZ() {
        throw new aWi(new aCE(this, bED, new Object[0]));
    }

    /* renamed from: T */
    public C6494any mo11T() {
        return ___iScriptClass;
    }

    /* renamed from: U */
    public C2491fm[] mo12U() {
        return _m_methods;
    }

    /* renamed from: W */
    public Object mo13W() {
        return new C5868abw(this);
    }

    /* renamed from: a */
    public Object mo14a(C0495Gr gr) {
        Object[] args = gr.getArgs();
        switch (gr.mo2417hq() - BaseTaikodomContent._m_methodCount) {
            case 0:
                return amZ();
            case 1:
                m14915aG();
                return null;
            case 2:
                m14914a((C0665JT) args[0]);
                return null;
            default:
                return super.mo14a(gr);
        }
    }

    /* renamed from: a */
    public void mo15a(Collection collection, C0495Gr gr) {
        super.mo15a(collection, gr);
    }

    /* renamed from: aH */
    public void mo70aH() {
        switch (bFf().mo6893i(_f_onResurrect_0020_0028_0029V)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, _f_onResurrect_0020_0028_0029V, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, _f_onResurrect_0020_0028_0029V, new Object[0]));
                break;
        }
        m14915aG();
    }

    public C1556Wo<DamageType, C1649YI> ana() {
        switch (bFf().mo6893i(bED)) {
            case 0:
                return null;
            case 2:
                return (C1556Wo) bFf().mo5606d(new aCE(this, bED, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, bED, new Object[0]));
                break;
        }
        return amZ();
    }

    /* renamed from: b */
    public void mo24b(C0665JT jt) {
        switch (bFf().mo6893i(f2874Do)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f2874Do, new Object[]{jt}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f2874Do, new Object[]{jt}));
                break;
        }
        m14914a(jt);
    }

    /* renamed from: c */
    public C5663aRz[] mo25c() {
        return _m_fields;
    }

    /* renamed from: S */
    public void mo10S() {
        super.mo10S();
        C1556Wo<DamageType, C1649YI> ana = ana();
        for (DamageType put : ala().aLs()) {
            ana.put(put, new C1649YI(0.0f, C1649YI.C1650a.ABSOLUTE));
        }
    }

    @C0064Am(aul = "62cc2fd0e1ce4b84e837986f7a8d1541", aum = 0)
    /* renamed from: aG */
    private void m14915aG() {
        super.mo70aH();
        C1556Wo<DamageType, C1649YI> ana = ana();
        for (DamageType next : ala().aLs()) {
            if (!ana.containsKey(next)) {
                ana.put(next, new C1649YI(0.0f, C1649YI.C1650a.ABSOLUTE));
            }
        }
    }

    @C0064Am(aul = "b409fbb1a1ccf6d948d473aeb57902b7", aum = 0)
    /* renamed from: a */
    private void m14914a(C0665JT jt) {
        if (jt.mo3117j(2, 1, 0)) {
            try {
                DamageType ek = ala().mo1499ek("SHIELD");
                if (ek == null) {
                    ek = (DamageType) bFf().mo6865M(DamageType.class);
                    ek.mo10S();
                    I18NString i18NString = new I18NString();
                    i18NString.set("pt", "ESCUDO");
                    i18NString.set(I18NString.DEFAULT_LOCATION, "SHIELD");
                    ek.setHandle("SHIELD");
                    ek.mo2144gR(i18NString);
                    ala().mo1547x(ek);
                }
                DamageType fr = ek;
                DamageType ek2 = ala().mo1499ek("HULL");
                if (ek2 == null) {
                    ek2 = (DamageType) bFf().mo6865M(DamageType.class);
                    ek2.mo10S();
                    I18NString i18NString2 = new I18NString();
                    i18NString2.set("pt", "CASCO");
                    i18NString2.set(I18NString.DEFAULT_LOCATION, "HULL");
                    ek2.setHandle("HULL");
                    ek2.mo2144gR(i18NString2);
                    ala().mo1547x(ek2);
                }
                DamageType fr2 = ek2;
                if (jt.mo3117j(2, 0, 0)) {
                    ShieldType aph = (ShieldType) jt.baw();
                    aph.mo15372dX((float) ((Integer) jt.get("maxHealthPoints")).intValue());
                    aph.ana().put(fr, new C1649YI((float) ((Long) jt.get("damageReduction")).longValue(), C1649YI.C1650a.ABSOLUTE));
                    aph.ana().put(fr2, new C1649YI(100.0f, C1649YI.C1650a.PERCENT));
                    aph.mo15376kq((float) ((Long) jt.get("recoveryTime")).longValue());
                    return;
                }
                ShieldType aph2 = (ShieldType) jt.baw();
                aph2.ana().put(fr, new C1649YI(((Float) jt.get("damageReduction")).floatValue(), C1649YI.C1650a.ABSOLUTE));
                aph2.ana().put(fr2, new C1649YI(100.0f, C1649YI.C1650a.PERCENT));
            } catch (Exception e) {
                System.err.println("BaseShieldType : OnLoadVersion " + e);
            }
        }
    }
}
