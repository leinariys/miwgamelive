package game.script.ship;

import game.network.message.externalizable.aCE;
import game.network.message.serializable.C3438ra;
import game.script.item.ItemType;
import logic.baa.*;
import logic.data.mbean.C5628aQq;
import logic.data.mbean.C6829auV;
import logic.res.code.C5640aRc;
import logic.res.code.C5663aRz;
import logic.res.code.C6494any;
import logic.res.html.C2491fm;
import p001a.*;
import taikodom.infra.script.I18NString;

import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.UUID;

@C5511aMd
@C6485anp
/* renamed from: a.An */
/* compiled from: a */
public class OutpostLevelInfo extends aDJ implements C0468GU, C1616Xf {
    public static final C2491fm _f_toString_0020_0028_0029Ljava_002flang_002fString_003b = null;
    public static final int _m_fieldCount = 0;
    public static final C5663aRz[] _m_fields = null;
    public static final int _m_methodCount = 0;
    public static final C2491fm[] _m_methods = null;
    public static final C5663aRz ayJ = null;
    public static final C2491fm ayM = null;
    /* renamed from: bL */
    public static final C5663aRz f76bL = null;
    /* renamed from: bM */
    public static final C5663aRz f77bM = null;
    /* renamed from: bN */
    public static final C2491fm f78bN = null;
    /* renamed from: bO */
    public static final C2491fm f79bO = null;
    /* renamed from: bP */
    public static final C2491fm f80bP = null;
    /* renamed from: bQ */
    public static final C2491fm f81bQ = null;
    public static final C2491fm gXA = null;
    public static final C2491fm gXB = null;
    public static final C2491fm gXC = null;
    public static final C2491fm gXD = null;
    public static final C5663aRz gXo = null;
    public static final C5663aRz gXq = null;
    public static final C5663aRz gXs = null;
    public static final C5663aRz gXu = null;
    public static final C2491fm gXv = null;
    public static final C2491fm gXw = null;
    public static final C2491fm gXx = null;
    public static final C2491fm gXy = null;
    public static final C2491fm gXz = null;
    public static final long serialVersionUID = 0;
    public static C6494any ___iScriptClass;
    @C0064Am(aul = "0c07bb54e0ffe31a2652d8744c5f8019", aum = 2)
    private static C2611hZ ayI;
    @C0064Am(aul = "57dcdc0fcae66c64416d5a6e17e81c0f", aum = 0)

    /* renamed from: bK */
    private static UUID f75bK;
    @C0064Am(aul = "4f6ae553f985021dceb62ac1b0a4420a", aum = 3)
    private static long gXn;
    @C0064Am(aul = "a8958430d65a072b23573787fe75184b", aum = 4)
    private static C3438ra<OutpostUpkeepItem> gXp;
    @C0064Am(aul = "f1a613eb8b456439a14375d2faed0f44", aum = 5)
    private static int gXr;
    @C0064Am(aul = "6c69bc83b802929357c71a56eb557db4", aum = 6)
    private static I18NString gXt;
    @C0064Am(aul = "51827bceb13463214403d3b6063c8161", aum = 1)
    private static String handle;

    static {
        m476V();
    }

    public OutpostLevelInfo() {
        super((C5540aNg) null);
        super.mo10S();
    }

    public OutpostLevelInfo(C5540aNg ang) {
        super(ang);
    }

    public OutpostLevelInfo(C2611hZ hZVar) {
        super((C5540aNg) null);
        super._m_script_init(hZVar);
    }

    /* renamed from: V */
    static void m476V() {
        serialVersionUID = (long) 1;
        _m_fieldCount = aDJ._m_fieldCount + 7;
        _m_methodCount = aDJ._m_methodCount + 15;
        int i = aDJ._m_fieldCount;
        C5663aRz[] arzArr = new C5663aRz[(i + 7)];
        C5663aRz b = C5640aRc.m17844b(OutpostLevelInfo.class, "57dcdc0fcae66c64416d5a6e17e81c0f", i);
        f76bL = b;
        arzArr[i] = b;
        int i2 = i + 1;
        C5663aRz b2 = C5640aRc.m17844b(OutpostLevelInfo.class, "51827bceb13463214403d3b6063c8161", i2);
        f77bM = b2;
        arzArr[i2] = b2;
        int i3 = i2 + 1;
        C5663aRz b3 = C5640aRc.m17844b(OutpostLevelInfo.class, "0c07bb54e0ffe31a2652d8744c5f8019", i3);
        ayJ = b3;
        arzArr[i3] = b3;
        int i4 = i3 + 1;
        C5663aRz b4 = C5640aRc.m17844b(OutpostLevelInfo.class, "4f6ae553f985021dceb62ac1b0a4420a", i4);
        gXo = b4;
        arzArr[i4] = b4;
        int i5 = i4 + 1;
        C5663aRz b5 = C5640aRc.m17844b(OutpostLevelInfo.class, "a8958430d65a072b23573787fe75184b", i5);
        gXq = b5;
        arzArr[i5] = b5;
        int i6 = i5 + 1;
        C5663aRz b6 = C5640aRc.m17844b(OutpostLevelInfo.class, "f1a613eb8b456439a14375d2faed0f44", i6);
        gXs = b6;
        arzArr[i6] = b6;
        int i7 = i6 + 1;
        C5663aRz b7 = C5640aRc.m17844b(OutpostLevelInfo.class, "6c69bc83b802929357c71a56eb557db4", i7);
        gXu = b7;
        arzArr[i7] = b7;
        int i8 = i7 + 1;
        _m_fields = arzArr;
        C1634Xv.m11725a((Object[]) aDJ._m_fields, (Object[]) _m_fields);
        int i9 = aDJ._m_methodCount;
        C2491fm[] fmVarArr = new C2491fm[(i9 + 15)];
        C2491fm a = C4105zY.m41624a(OutpostLevelInfo.class, "4fad3c3e89a0676144feb2a0ea41f30d", i9);
        _f_toString_0020_0028_0029Ljava_002flang_002fString_003b = a;
        fmVarArr[i9] = a;
        int i10 = i9 + 1;
        C2491fm a2 = C4105zY.m41624a(OutpostLevelInfo.class, "69d9615c591271a3071c287302671747", i10);
        f78bN = a2;
        fmVarArr[i10] = a2;
        int i11 = i10 + 1;
        C2491fm a3 = C4105zY.m41624a(OutpostLevelInfo.class, "8c77495110eae88b13541ff5f6e35aa5", i11);
        f79bO = a3;
        fmVarArr[i11] = a3;
        int i12 = i11 + 1;
        C2491fm a4 = C4105zY.m41624a(OutpostLevelInfo.class, "4964d0bfb1034f9cb26b30bd419fe96d", i12);
        f80bP = a4;
        fmVarArr[i12] = a4;
        int i13 = i12 + 1;
        C2491fm a5 = C4105zY.m41624a(OutpostLevelInfo.class, "c3753e828f1e83228378e44e05b6cca3", i13);
        f81bQ = a5;
        fmVarArr[i13] = a5;
        int i14 = i13 + 1;
        C2491fm a6 = C4105zY.m41624a(OutpostLevelInfo.class, "d2b0c7376c2c70fade6a666ea9d7f80d", i14);
        ayM = a6;
        fmVarArr[i14] = a6;
        int i15 = i14 + 1;
        C2491fm a7 = C4105zY.m41624a(OutpostLevelInfo.class, "9cda6939528049fdeeb8fe536b6fffec", i15);
        gXv = a7;
        fmVarArr[i15] = a7;
        int i16 = i15 + 1;
        C2491fm a8 = C4105zY.m41624a(OutpostLevelInfo.class, "575480f263cb7643299395fd9e1e0deb", i16);
        gXw = a8;
        fmVarArr[i16] = a8;
        int i17 = i16 + 1;
        C2491fm a9 = C4105zY.m41624a(OutpostLevelInfo.class, "926c7241533412937b5d3a47d4eb63c2", i17);
        gXx = a9;
        fmVarArr[i17] = a9;
        int i18 = i17 + 1;
        C2491fm a10 = C4105zY.m41624a(OutpostLevelInfo.class, "a953d8dda215ca6bab4707f3fbb24687", i18);
        gXy = a10;
        fmVarArr[i18] = a10;
        int i19 = i18 + 1;
        C2491fm a11 = C4105zY.m41624a(OutpostLevelInfo.class, "d03df5a448dc71de2a2d1adcbff1638a", i19);
        gXz = a11;
        fmVarArr[i19] = a11;
        int i20 = i19 + 1;
        C2491fm a12 = C4105zY.m41624a(OutpostLevelInfo.class, "eec984d3e8761cd62b4bddd23cd5e5cb", i20);
        gXA = a12;
        fmVarArr[i20] = a12;
        int i21 = i20 + 1;
        C2491fm a13 = C4105zY.m41624a(OutpostLevelInfo.class, "15121ef64e5096d7eff41188b535ef0b", i21);
        gXB = a13;
        fmVarArr[i21] = a13;
        int i22 = i21 + 1;
        C2491fm a14 = C4105zY.m41624a(OutpostLevelInfo.class, "9e3fe9674e277e056aba7caecfb7b4b5", i22);
        gXC = a14;
        fmVarArr[i22] = a14;
        int i23 = i22 + 1;
        C2491fm a15 = C4105zY.m41624a(OutpostLevelInfo.class, "0f90196bff006797dc479477d0ff54db", i23);
        gXD = a15;
        fmVarArr[i23] = a15;
        int i24 = i23 + 1;
        _m_methods = fmVarArr;
        C1634Xv.m11725a((Object[]) aDJ._m_methods, (Object[]) _m_methods);
        ___iScriptClass = aUO.m18566a(OutpostLevelInfo.class, C5628aQq.class, _m_fields, _m_methods);
    }

    /* renamed from: LY */
    private C2611hZ m474LY() {
        return (C2611hZ) bFf().mo5608dq().mo3214p(ayJ);
    }

    @C3248pc(aYR = C3248pc.C3250b.ADDER, displayName = "Upkeep Items")
    @C0064Am(aul = "d03df5a448dc71de2a2d1adcbff1638a", aum = 0)
    @C5566aOg
    /* renamed from: a */
    private void m477a(OutpostUpkeepItem aVar) {
        throw new aWi(new aCE(this, gXz, new Object[]{aVar}));
    }

    /* renamed from: a */
    private void m478a(C2611hZ hZVar) {
        bFf().mo5608dq().mo3197f(ayJ, hZVar);
    }

    /* renamed from: a */
    private void m479a(String str) {
        bFf().mo5608dq().mo3197f(f77bM, str);
    }

    /* renamed from: a */
    private void m480a(UUID uuid) {
        bFf().mo5608dq().mo3197f(f76bL, uuid);
    }

    /* renamed from: an */
    private UUID m481an() {
        return (UUID) bFf().mo5608dq().mo3214p(f76bL);
    }

    /* renamed from: ao */
    private String m482ao() {
        return (String) bFf().mo5608dq().mo3214p(f77bM);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Handle")
    @C0064Am(aul = "c3753e828f1e83228378e44e05b6cca3", aum = 0)
    @C5566aOg
    /* renamed from: b */
    private void m486b(String str) {
        throw new aWi(new aCE(this, f81bQ, new Object[]{str}));
    }

    @C3248pc(aYR = C3248pc.C3250b.REMOVER, displayName = "Upkeep Items")
    @C0064Am(aul = "eec984d3e8761cd62b4bddd23cd5e5cb", aum = 0)
    @C5566aOg
    /* renamed from: c */
    private void m488c(OutpostUpkeepItem aVar) {
        throw new aWi(new aCE(this, gXA, new Object[]{aVar}));
    }

    /* renamed from: c */
    private void m489c(UUID uuid) {
        switch (bFf().mo6893i(f79bO)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f79bO, new Object[]{uuid}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f79bO, new Object[]{uuid}));
                break;
        }
        m487b(uuid);
    }

    private long cEJ() {
        return bFf().mo5608dq().mo3213o(gXo);
    }

    private C3438ra cEK() {
        return (C3438ra) bFf().mo5608dq().mo3214p(gXq);
    }

    private int cEL() {
        return bFf().mo5608dq().mo3212n(gXs);
    }

    private I18NString cEM() {
        return (I18NString) bFf().mo5608dq().mo3214p(gXu);
    }

    /* renamed from: cl */
    private void m490cl(C3438ra raVar) {
        bFf().mo5608dq().mo3197f(gXq, raVar);
    }

    /* renamed from: jm */
    private void m491jm(long j) {
        bFf().mo5608dq().mo3184b(gXo, j);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Upkeep Cost")
    @C0064Am(aul = "a953d8dda215ca6bab4707f3fbb24687", aum = 0)
    @C5566aOg
    /* renamed from: jn */
    private void m492jn(long j) {
        throw new aWi(new aCE(this, gXy, new Object[]{new Long(j)}));
    }

    /* renamed from: nt */
    private void m493nt(I18NString i18NString) {
        bFf().mo5608dq().mo3197f(gXu, i18NString);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Level name")
    @C0064Am(aul = "0f90196bff006797dc479477d0ff54db", aum = 0)
    @C5566aOg
    /* renamed from: nu */
    private void m494nu(I18NString i18NString) {
        throw new aWi(new aCE(this, gXD, new Object[]{i18NString}));
    }

    /* renamed from: ws */
    private void m495ws(int i) {
        bFf().mo5608dq().mo3183b(gXs, i);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Corp max members add")
    @C0064Am(aul = "575480f263cb7643299395fd9e1e0deb", aum = 0)
    @C5566aOg
    /* renamed from: wt */
    private void m496wt(int i) {
        throw new aWi(new aCE(this, gXw, new Object[]{new Integer(i)}));
    }

    /* renamed from: Mb */
    public C2611hZ mo449Mb() {
        switch (bFf().mo6893i(ayM)) {
            case 0:
                return null;
            case 2:
                return (C2611hZ) bFf().mo5606d(new aCE(this, ayM, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, ayM, new Object[0]));
                break;
        }
        return m475Ma();
    }

    /* renamed from: S */
    public void mo10S() {
        super.mo10S();
    }

    /* renamed from: T */
    public C6494any mo11T() {
        return ___iScriptClass;
    }

    /* renamed from: U */
    public C2491fm[] mo12U() {
        return _m_methods;
    }

    /* renamed from: W */
    public Object mo13W() {
        return new C5628aQq(this);
    }

    /* renamed from: a */
    public Object mo14a(C0495Gr gr) {
        Object[] args = gr.getArgs();
        switch (gr.mo2417hq() - aDJ._m_methodCount) {
            case 0:
                return m485au();
            case 1:
                return m483ap();
            case 2:
                m487b((UUID) args[0]);
                return null;
            case 3:
                return m484ar();
            case 4:
                m486b((String) args[0]);
                return null;
            case 5:
                return m475Ma();
            case 6:
                return new Integer(cEN());
            case 7:
                m496wt(((Integer) args[0]).intValue());
                return null;
            case 8:
                return new Long(cEP());
            case 9:
                m492jn(((Long) args[0]).longValue());
                return null;
            case 10:
                m477a((OutpostUpkeepItem) args[0]);
                return null;
            case 11:
                m488c((OutpostUpkeepItem) args[0]);
                return null;
            case 12:
                return cER();
            case 13:
                return cES();
            case 14:
                m494nu((I18NString) args[0]);
                return null;
            default:
                return super.mo14a(gr);
        }
    }

    /* renamed from: a */
    public void mo15a(Collection collection, C0495Gr gr) {
        super.mo15a(collection, gr);
    }

    /* renamed from: aq */
    public UUID mo18aq() {
        switch (bFf().mo6893i(f78bN)) {
            case 0:
                return null;
            case 2:
                return (UUID) bFf().mo5606d(new aCE(this, f78bN, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f78bN, new Object[0]));
                break;
        }
        return m483ap();
    }

    @C3248pc(aYR = C3248pc.C3250b.ADDER, displayName = "Upkeep Items")
    @C5566aOg
    /* renamed from: b */
    public void mo450b(OutpostUpkeepItem aVar) {
        switch (bFf().mo6893i(gXz)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, gXz, new Object[]{aVar}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, gXz, new Object[]{aVar}));
                break;
        }
        m477a(aVar);
    }

    /* renamed from: c */
    public C5663aRz[] mo25c() {
        return _m_fields;
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Corp max members add")
    public int cEO() {
        switch (bFf().mo6893i(gXv)) {
            case 0:
                return 0;
            case 2:
                return ((Integer) bFf().mo5606d(new aCE(this, gXv, new Object[0]))).intValue();
            case 3:
                bFf().mo5606d(new aCE(this, gXv, new Object[0]));
                break;
        }
        return cEN();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Upkeep Cost")
    public long cEQ() {
        switch (bFf().mo6893i(gXx)) {
            case 0:
                return 0;
            case 2:
                return ((Long) bFf().mo5606d(new aCE(this, gXx, new Object[0]))).longValue();
            case 3:
                bFf().mo5606d(new aCE(this, gXx, new Object[0]));
                break;
        }
        return cEP();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Level name")
    public I18NString cET() {
        switch (bFf().mo6893i(gXC)) {
            case 0:
                return null;
            case 2:
                return (I18NString) bFf().mo5606d(new aCE(this, gXC, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, gXC, new Object[0]));
                break;
        }
        return cES();
    }

    @C3248pc(aYR = C3248pc.C3250b.REMOVER, displayName = "Upkeep Items")
    @C5566aOg
    /* renamed from: d */
    public void mo454d(OutpostUpkeepItem aVar) {
        switch (bFf().mo6893i(gXA)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, gXA, new Object[]{aVar}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, gXA, new Object[]{aVar}));
                break;
        }
        m488c(aVar);
    }

    @C3248pc(aYR = C3248pc.C3250b.ITERATOR, displayName = "Upkeep Items")
    public List<OutpostUpkeepItem> get() {
        switch (bFf().mo6893i(gXB)) {
            case 0:
                return null;
            case 2:
                return (List) bFf().mo5606d(new aCE(this, gXB, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, gXB, new Object[0]));
                break;
        }
        return cER();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Handle")
    public String getHandle() {
        switch (bFf().mo6893i(f80bP)) {
            case 0:
                return null;
            case 2:
                return (String) bFf().mo5606d(new aCE(this, f80bP, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f80bP, new Object[0]));
                break;
        }
        return m484ar();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Handle")
    @C5566aOg
    public void setHandle(String str) {
        switch (bFf().mo6893i(f81bQ)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f81bQ, new Object[]{str}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f81bQ, new Object[]{str}));
                break;
        }
        m486b(str);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Upkeep Cost")
    @C5566aOg
    /* renamed from: jo */
    public void mo456jo(long j) {
        switch (bFf().mo6893i(gXy)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, gXy, new Object[]{new Long(j)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, gXy, new Object[]{new Long(j)}));
                break;
        }
        m492jn(j);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Level name")
    @C5566aOg
    /* renamed from: nv */
    public void mo458nv(I18NString i18NString) {
        switch (bFf().mo6893i(gXD)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, gXD, new Object[]{i18NString}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, gXD, new Object[]{i18NString}));
                break;
        }
        m494nu(i18NString);
    }

    public String toString() {
        switch (bFf().mo6893i(_f_toString_0020_0028_0029Ljava_002flang_002fString_003b)) {
            case 0:
                return null;
            case 2:
                return (String) bFf().mo5606d(new aCE(this, _f_toString_0020_0028_0029Ljava_002flang_002fString_003b, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, _f_toString_0020_0028_0029Ljava_002flang_002fString_003b, new Object[0]));
                break;
        }
        return m485au();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Corp max members add")
    @C5566aOg
    /* renamed from: wu */
    public void mo461wu(int i) {
        switch (bFf().mo6893i(gXw)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, gXw, new Object[]{new Integer(i)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, gXw, new Object[]{new Integer(i)}));
                break;
        }
        m496wt(i);
    }

    @C0064Am(aul = "4fad3c3e89a0676144feb2a0ea41f30d", aum = 0)
    /* renamed from: au */
    private String m485au() {
        return "Level info for " + m474LY().name();
    }

    /* renamed from: k */
    public void mo457k(C2611hZ hZVar) {
        super.mo10S();
        m478a(hZVar);
        m480a(UUID.randomUUID());
    }

    @C0064Am(aul = "69d9615c591271a3071c287302671747", aum = 0)
    /* renamed from: ap */
    private UUID m483ap() {
        return m481an();
    }

    @C0064Am(aul = "8c77495110eae88b13541ff5f6e35aa5", aum = 0)
    /* renamed from: b */
    private void m487b(UUID uuid) {
        m480a(uuid);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Handle")
    @C0064Am(aul = "4964d0bfb1034f9cb26b30bd419fe96d", aum = 0)
    /* renamed from: ar */
    private String m484ar() {
        return m482ao();
    }

    @C0064Am(aul = "d2b0c7376c2c70fade6a666ea9d7f80d", aum = 0)
    /* renamed from: Ma */
    private C2611hZ m475Ma() {
        return m474LY();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Corp max members add")
    @C0064Am(aul = "9cda6939528049fdeeb8fe536b6fffec", aum = 0)
    private int cEN() {
        return cEL();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Upkeep Cost")
    @C0064Am(aul = "926c7241533412937b5d3a47d4eb63c2", aum = 0)
    private long cEP() {
        return cEJ();
    }

    @C3248pc(aYR = C3248pc.C3250b.ITERATOR, displayName = "Upkeep Items")
    @C0064Am(aul = "15121ef64e5096d7eff41188b535ef0b", aum = 0)
    private List<OutpostUpkeepItem> cER() {
        return Collections.unmodifiableList(cEK());
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Level name")
    @C0064Am(aul = "9e3fe9674e277e056aba7caecfb7b4b5", aum = 0)
    private I18NString cES() {
        return cEM();
    }

    @C6485anp
    @C5511aMd
    /* renamed from: a.An$a */
    public class OutpostUpkeepItem extends aDJ implements C0468GU, C1616Xf {
        public static final C2491fm _f_toString_0020_0028_0029Ljava_002flang_002fString_003b = null;
        public static final int _m_fieldCount = 0;
        public static final C5663aRz[] _m_fields = null;
        public static final int _m_methodCount = 0;
        public static final C2491fm[] _m_methods = null;
        /* renamed from: aT */
        public static final C5663aRz f82aT = null;
        /* renamed from: bL */
        public static final C5663aRz f84bL = null;
        /* renamed from: bM */
        public static final C5663aRz f85bM = null;
        /* renamed from: bN */
        public static final C2491fm f86bN = null;
        /* renamed from: bO */
        public static final C2491fm f87bO = null;
        /* renamed from: bP */
        public static final C2491fm f88bP = null;
        /* renamed from: bQ */
        public static final C2491fm f89bQ = null;
        /* renamed from: cA */
        public static final C2491fm f90cA = null;
        /* renamed from: cB */
        public static final C2491fm f91cB = null;
        public static final C2491fm cfr = null;
        /* renamed from: cz */
        public static final C5663aRz f94cz = null;
        /* renamed from: kO */
        public static final C5663aRz f95kO = null;
        /* renamed from: lk */
        public static final C2491fm f96lk = null;
        public static final long serialVersionUID = 0;
        public static C6494any ___iScriptClass;
        @C0064Am(aul = "aaf3584f1aee02fc8a944e022f7da1c7", aum = 4)
        static /* synthetic */ OutpostLevelInfo cfq;
        @C0064Am(aul = "ad0e7b7673db304bf538c77e0df2fdd6", aum = 2)

        /* renamed from: bK */
        private static UUID f83bK;
        @C0064Am(aul = "62ad9c39cb3d49b814b6bfeb3a409e13", aum = 0)

        /* renamed from: cY */
        private static ItemType f92cY;
        @C0064Am(aul = "fe379ae0e7c9acb0db3d951ffca91aba", aum = 1)

        /* renamed from: cy */
        private static int f93cy;
        @C0064Am(aul = "14175d842ec5aadf1962158ca52b022d", aum = 3)
        private static String handle;

        static {
            m512V();
        }

        public OutpostUpkeepItem() {
            super((C5540aNg) null);
            super.mo10S();
        }

        public OutpostUpkeepItem(OutpostLevelInfo an) {
            super((C5540aNg) null);
            super._m_script_init(an);
        }

        public OutpostUpkeepItem(C5540aNg ang) {
            super(ang);
        }

        /* renamed from: V */
        static void m512V() {
            serialVersionUID = (long) 1;
            _m_fieldCount = aDJ._m_fieldCount + 5;
            _m_methodCount = aDJ._m_methodCount + 9;
            int i = aDJ._m_fieldCount;
            C5663aRz[] arzArr = new C5663aRz[(i + 5)];
            C5663aRz b = C5640aRc.m17844b(OutpostUpkeepItem.class, "62ad9c39cb3d49b814b6bfeb3a409e13", i);
            f95kO = b;
            arzArr[i] = b;
            int i2 = i + 1;
            C5663aRz b2 = C5640aRc.m17844b(OutpostUpkeepItem.class, "fe379ae0e7c9acb0db3d951ffca91aba", i2);
            f94cz = b2;
            arzArr[i2] = b2;
            int i3 = i2 + 1;
            C5663aRz b3 = C5640aRc.m17844b(OutpostUpkeepItem.class, "ad0e7b7673db304bf538c77e0df2fdd6", i3);
            f84bL = b3;
            arzArr[i3] = b3;
            int i4 = i3 + 1;
            C5663aRz b4 = C5640aRc.m17844b(OutpostUpkeepItem.class, "14175d842ec5aadf1962158ca52b022d", i4);
            f85bM = b4;
            arzArr[i4] = b4;
            int i5 = i4 + 1;
            C5663aRz b5 = C5640aRc.m17844b(OutpostUpkeepItem.class, "aaf3584f1aee02fc8a944e022f7da1c7", i5);
            f82aT = b5;
            arzArr[i5] = b5;
            int i6 = i5 + 1;
            _m_fields = arzArr;
            C1634Xv.m11725a((Object[]) aDJ._m_fields, (Object[]) _m_fields);
            int i7 = aDJ._m_methodCount;
            C2491fm[] fmVarArr = new C2491fm[(i7 + 9)];
            C2491fm a = C4105zY.m41624a(OutpostUpkeepItem.class, "6e7cf4435d680a0b6fa94c5d6f3ef727", i7);
            _f_toString_0020_0028_0029Ljava_002flang_002fString_003b = a;
            fmVarArr[i7] = a;
            int i8 = i7 + 1;
            C2491fm a2 = C4105zY.m41624a(OutpostUpkeepItem.class, "3fe326615b60412eea5ea28a06289141", i8);
            f86bN = a2;
            fmVarArr[i8] = a2;
            int i9 = i8 + 1;
            C2491fm a3 = C4105zY.m41624a(OutpostUpkeepItem.class, "91ac438c6ee29e6f14310cae10366150", i9);
            f87bO = a3;
            fmVarArr[i9] = a3;
            int i10 = i9 + 1;
            C2491fm a4 = C4105zY.m41624a(OutpostUpkeepItem.class, "af0691b51da5deb3899c63fd7a789903", i10);
            f88bP = a4;
            fmVarArr[i10] = a4;
            int i11 = i10 + 1;
            C2491fm a5 = C4105zY.m41624a(OutpostUpkeepItem.class, "6e41b613dbce77b500fac631758a50b8", i11);
            f89bQ = a5;
            fmVarArr[i11] = a5;
            int i12 = i11 + 1;
            C2491fm a6 = C4105zY.m41624a(OutpostUpkeepItem.class, "f0912c827ca195b567e2bc2ddfed8f9c", i12);
            f96lk = a6;
            fmVarArr[i12] = a6;
            int i13 = i12 + 1;
            C2491fm a7 = C4105zY.m41624a(OutpostUpkeepItem.class, "67540227c676ec62aec97117486073c6", i13);
            cfr = a7;
            fmVarArr[i13] = a7;
            int i14 = i13 + 1;
            C2491fm a8 = C4105zY.m41624a(OutpostUpkeepItem.class, "054a4824ef3b66eb76c4c016f4a9f2a6", i14);
            f90cA = a8;
            fmVarArr[i14] = a8;
            int i15 = i14 + 1;
            C2491fm a9 = C4105zY.m41624a(OutpostUpkeepItem.class, "6447bdb2375cda8ec86746db56bdda3a", i15);
            f91cB = a9;
            fmVarArr[i15] = a9;
            int i16 = i15 + 1;
            _m_methods = fmVarArr;
            C1634Xv.m11725a((Object[]) aDJ._m_methods, (Object[]) _m_methods);
            ___iScriptClass = aUO.m18566a(OutpostUpkeepItem.class, C6829auV.class, _m_fields, _m_methods);
        }

        /* renamed from: a */
        private void m513a(OutpostLevelInfo an) {
            bFf().mo5608dq().mo3197f(f82aT, an);
        }

        /* renamed from: a */
        private void m514a(String str) {
            bFf().mo5608dq().mo3197f(f85bM, str);
        }

        /* renamed from: a */
        private void m515a(UUID uuid) {
            bFf().mo5608dq().mo3197f(f84bL, uuid);
        }

        /* renamed from: an */
        private UUID m516an() {
            return (UUID) bFf().mo5608dq().mo3214p(f84bL);
        }

        /* renamed from: ao */
        private String m517ao() {
            return (String) bFf().mo5608dq().mo3214p(f85bM);
        }

        private OutpostLevelInfo aun() {
            return (OutpostLevelInfo) bFf().mo5608dq().mo3214p(f82aT);
        }

        /* renamed from: aw */
        private int m521aw() {
            return bFf().mo5608dq().mo3212n(f94cz);
        }

        @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Handle")
        @C0064Am(aul = "6e41b613dbce77b500fac631758a50b8", aum = 0)
        @C5566aOg
        /* renamed from: b */
        private void m523b(String str) {
            throw new aWi(new aCE(this, f89bQ, new Object[]{str}));
        }

        /* renamed from: c */
        private void m525c(UUID uuid) {
            switch (bFf().mo6893i(f87bO)) {
                case 0:
                    return;
                case 2:
                    bFf().mo5606d(new aCE(this, f87bO, new Object[]{uuid}));
                    return;
                case 3:
                    bFf().mo5606d(new aCE(this, f87bO, new Object[]{uuid}));
                    break;
            }
            m524b(uuid);
        }

        /* renamed from: eA */
        private ItemType m526eA() {
            return (ItemType) bFf().mo5608dq().mo3214p(f95kO);
        }

        /* renamed from: f */
        private void m528f(int i) {
            bFf().mo5608dq().mo3183b(f94cz, i);
        }

        /* renamed from: f */
        private void m529f(ItemType jCVar) {
            bFf().mo5608dq().mo3197f(f95kO, jCVar);
        }

        @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Amount")
        @C0064Am(aul = "6447bdb2375cda8ec86746db56bdda3a", aum = 0)
        @C5566aOg
        /* renamed from: g */
        private void m530g(int i) {
            throw new aWi(new aCE(this, f91cB, new Object[]{new Integer(i)}));
        }

        @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Item")
        @C0064Am(aul = "67540227c676ec62aec97117486073c6", aum = 0)
        @C5566aOg
        /* renamed from: o */
        private void m531o(ItemType jCVar) {
            throw new aWi(new aCE(this, cfr, new Object[]{jCVar}));
        }

        /* renamed from: S */
        public void mo10S() {
            super.mo10S();
        }

        /* renamed from: T */
        public C6494any mo11T() {
            return ___iScriptClass;
        }

        /* renamed from: U */
        public C2491fm[] mo12U() {
            return _m_methods;
        }

        /* renamed from: W */
        public Object mo13W() {
            return new C6829auV(this);
        }

        /* renamed from: a */
        public Object mo14a(C0495Gr gr) {
            Object[] args = gr.getArgs();
            switch (gr.mo2417hq() - aDJ._m_methodCount) {
                case 0:
                    return m520au();
                case 1:
                    return m518ap();
                case 2:
                    m524b((UUID) args[0]);
                    return null;
                case 3:
                    return m519ar();
                case 4:
                    m523b((String) args[0]);
                    return null;
                case 5:
                    return m527eO();
                case 6:
                    m531o((ItemType) args[0]);
                    return null;
                case 7:
                    return new Integer(m522ax());
                case 8:
                    m530g(((Integer) args[0]).intValue());
                    return null;
                default:
                    return super.mo14a(gr);
            }
        }

        /* renamed from: a */
        public void mo15a(Collection collection, C0495Gr gr) {
            super.mo15a(collection, gr);
        }

        /* renamed from: aq */
        public UUID mo18aq() {
            switch (bFf().mo6893i(f86bN)) {
                case 0:
                    return null;
                case 2:
                    return (UUID) bFf().mo5606d(new aCE(this, f86bN, new Object[0]));
                case 3:
                    bFf().mo5606d(new aCE(this, f86bN, new Object[0]));
                    break;
            }
            return m518ap();
        }

        /* renamed from: c */
        public C5663aRz[] mo25c() {
            return _m_fields;
        }

        @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Item")
        /* renamed from: eP */
        public ItemType mo463eP() {
            switch (bFf().mo6893i(f96lk)) {
                case 0:
                    return null;
                case 2:
                    return (ItemType) bFf().mo5606d(new aCE(this, f96lk, new Object[0]));
                case 3:
                    bFf().mo5606d(new aCE(this, f96lk, new Object[0]));
                    break;
            }
            return m527eO();
        }

        @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Amount")
        public int getAmount() {
            switch (bFf().mo6893i(f90cA)) {
                case 0:
                    return 0;
                case 2:
                    return ((Integer) bFf().mo5606d(new aCE(this, f90cA, new Object[0]))).intValue();
                case 3:
                    bFf().mo5606d(new aCE(this, f90cA, new Object[0]));
                    break;
            }
            return m522ax();
        }

        @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Amount")
        @C5566aOg
        public void setAmount(int i) {
            switch (bFf().mo6893i(f91cB)) {
                case 0:
                    return;
                case 2:
                    bFf().mo5606d(new aCE(this, f91cB, new Object[]{new Integer(i)}));
                    return;
                case 3:
                    bFf().mo5606d(new aCE(this, f91cB, new Object[]{new Integer(i)}));
                    break;
            }
            m530g(i);
        }

        @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Handle")
        public String getHandle() {
            switch (bFf().mo6893i(f88bP)) {
                case 0:
                    return null;
                case 2:
                    return (String) bFf().mo5606d(new aCE(this, f88bP, new Object[0]));
                case 3:
                    bFf().mo5606d(new aCE(this, f88bP, new Object[0]));
                    break;
            }
            return m519ar();
        }

        @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Handle")
        @C5566aOg
        public void setHandle(String str) {
            switch (bFf().mo6893i(f89bQ)) {
                case 0:
                    return;
                case 2:
                    bFf().mo5606d(new aCE(this, f89bQ, new Object[]{str}));
                    return;
                case 3:
                    bFf().mo5606d(new aCE(this, f89bQ, new Object[]{str}));
                    break;
            }
            m523b(str);
        }

        @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Item")
        @C5566aOg
        /* renamed from: p */
        public void mo465p(ItemType jCVar) {
            switch (bFf().mo6893i(cfr)) {
                case 0:
                    return;
                case 2:
                    bFf().mo5606d(new aCE(this, cfr, new Object[]{jCVar}));
                    return;
                case 3:
                    bFf().mo5606d(new aCE(this, cfr, new Object[]{jCVar}));
                    break;
            }
            m531o(jCVar);
        }

        public String toString() {
            switch (bFf().mo6893i(_f_toString_0020_0028_0029Ljava_002flang_002fString_003b)) {
                case 0:
                    return null;
                case 2:
                    return (String) bFf().mo5606d(new aCE(this, _f_toString_0020_0028_0029Ljava_002flang_002fString_003b, new Object[0]));
                case 3:
                    bFf().mo5606d(new aCE(this, _f_toString_0020_0028_0029Ljava_002flang_002fString_003b, new Object[0]));
                    break;
            }
            return m520au();
        }

        @C0064Am(aul = "6e7cf4435d680a0b6fa94c5d6f3ef727", aum = 0)
        /* renamed from: au */
        private String m520au() {
            return "[" + m526eA() + ", " + m521aw() + "]";
        }

        /* renamed from: b */
        public void mo462b(OutpostLevelInfo an) {
            m513a(an);
            super.mo10S();
            m515a(UUID.randomUUID());
        }

        @C0064Am(aul = "3fe326615b60412eea5ea28a06289141", aum = 0)
        /* renamed from: ap */
        private UUID m518ap() {
            return m516an();
        }

        @C0064Am(aul = "91ac438c6ee29e6f14310cae10366150", aum = 0)
        /* renamed from: b */
        private void m524b(UUID uuid) {
            m515a(uuid);
        }

        @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Handle")
        @C0064Am(aul = "af0691b51da5deb3899c63fd7a789903", aum = 0)
        /* renamed from: ar */
        private String m519ar() {
            return m517ao();
        }

        @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Item")
        @C0064Am(aul = "f0912c827ca195b567e2bc2ddfed8f9c", aum = 0)
        /* renamed from: eO */
        private ItemType m527eO() {
            return m526eA();
        }

        @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Amount")
        @C0064Am(aul = "054a4824ef3b66eb76c4c016f4a9f2a6", aum = 0)
        /* renamed from: ax */
        private int m522ax() {
            return m521aw();
        }
    }
}
