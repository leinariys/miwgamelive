package game.script.ship;

import com.hoplon.geometry.Vec3f;
import game.network.message.externalizable.aCE;
import game.network.message.serializable.C1556Wo;
import game.network.message.serializable.C2686iZ;
import game.network.message.serializable.C3438ra;
import game.script.Actor;
import game.script.Character;
import game.script.TaskletImpl;
import game.script.agent.Agent;
import game.script.agent.AgentType;
import game.script.cloning.CloningCenter;
import game.script.faction.Faction;
import game.script.hangar.Hangar;
import game.script.newmarket.store.Store;
import game.script.npc.NPC;
import game.script.pda.DatabaseCategory;
import game.script.pda.TaikopediaEntry;
import game.script.player.Player;
import game.script.resource.Asset;
import game.script.simulation.Space;
import game.script.space.Node;
import game.script.storage.Storage;
import game.script.template.BaseTaikodomContent;
import logic.aaa.C2235dB;
import logic.baa.*;
import logic.bbb.C4029yK;
import logic.bbb.aDR;
import logic.data.link.C3161oY;
import logic.data.mbean.C2947lx;
import logic.data.mbean.C3916wk;
import logic.res.LoaderTrail;
import logic.res.code.C5640aRc;
import logic.res.code.C5663aRz;
import logic.res.code.C6494any;
import logic.res.html.C2491fm;
import p001a.*;
import taikodom.geom.Orientation;
import taikodom.infra.script.I18NString;

import java.util.*;

@C5829abJ("1.1.1")
@C6485anp
@C2712iu(mo19785Bs = {C6251ajP.class}, mo19786Bt = BaseTaikodomContent.class, version = "1.1.1")
@C5511aMd
/* renamed from: a.BF */
/* compiled from: a */
public class Station extends Actor implements C0286Dh, C0468GU, C0943Nq, C1616Xf, aOW, C4068yr {

    /* renamed from: LR */
    public static final C5663aRz f130LR = null;
    /* renamed from: LX */
    public static final C5663aRz f132LX = null;
    /* renamed from: LZ */
    public static final C5663aRz f134LZ = null;
    /* renamed from: Lm */
    public static final C2491fm f135Lm = null;
    /* renamed from: MH */
    public static final C2491fm f136MH = null;
    /* renamed from: MM */
    public static final C2491fm f137MM = null;
    /* renamed from: MN */
    public static final C2491fm f138MN = null;
    /* renamed from: Mf */
    public static final C5663aRz f140Mf = null;
    /* renamed from: Mw */
    public static final C2491fm f141Mw = null;
    /* renamed from: Mx */
    public static final C2491fm f142Mx = null;
    /* renamed from: My */
    public static final C2491fm f143My = null;
    /* renamed from: NY */
    public static final C5663aRz f144NY = null;
    /* renamed from: Ob */
    public static final C2491fm f145Ob = null;
    /* renamed from: Oc */
    public static final C2491fm f146Oc = null;
    /* renamed from: QA */
    public static final C5663aRz f147QA = null;
    /* renamed from: QE */
    public static final C2491fm f148QE = null;
    /* renamed from: QF */
    public static final C2491fm f149QF = null;
    public static final C2491fm _f_getType_0020_0028_0029Ljava_002flang_002fObject_003b = null;
    public static final C2491fm _f_onResurrect_0020_0028_0029V = null;
    public static final C2491fm _f_toString_0020_0028_0029Ljava_002flang_002fString_003b = null;
    public static final int _m_fieldCount = 0;
    public static final C5663aRz[] _m_fields = null;
    public static final int _m_methodCount = 0;
    public static final C2491fm[] _m_methods = null;
    public static final C5663aRz aMh = null;
    public static final C5663aRz aMi = null;
    public static final C2491fm aMj = null;
    public static final C2491fm aMk = null;
    public static final C2491fm aMl = null;
    public static final C2491fm aMn = null;
    public static final C2491fm aMo = null;
    public static final C2491fm aMp = null;
    public static final C2491fm aMr = null;
    public static final C2491fm aMu = null;
    public static final C2491fm aMv = null;
    public static final C5663aRz awd = null;
    /* renamed from: bL */
    public static final C5663aRz f151bL = null;
    /* renamed from: bM */
    public static final C5663aRz f152bM = null;
    /* renamed from: bN */
    public static final C2491fm f153bN = null;
    /* renamed from: bO */
    public static final C2491fm f154bO = null;
    /* renamed from: bP */
    public static final C2491fm f155bP = null;
    /* renamed from: bQ */
    public static final C2491fm f156bQ = null;
    public static final C2491fm bcI = null;
    public static final C2491fm bcN = null;
    public static final C5663aRz cnR = null;
    public static final C5663aRz cnS = null;
    public static final C5663aRz cnT = null;
    public static final C5663aRz cnU = null;
    public static final C5663aRz cnV = null;
    public static final C5663aRz cnW = null;
    public static final C5663aRz cnX = null;
    public static final C5663aRz cnY = null;
    public static final C5663aRz cnZ = null;
    public static final C2491fm coA = null;
    public static final C2491fm coB = null;
    public static final C2491fm coC = null;
    public static final C2491fm coD = null;
    public static final C2491fm coE = null;
    public static final C2491fm coF = null;
    public static final C2491fm coG = null;
    public static final C2491fm coH = null;
    public static final C2491fm coI = null;
    public static final C2491fm coJ = null;
    public static final C2491fm coK = null;
    public static final C2491fm coL = null;
    public static final C2491fm coM = null;
    public static final C2491fm coN = null;
    public static final C2491fm coO = null;
    public static final C2491fm coP = null;
    public static final C2491fm coQ = null;
    public static final C2491fm coR = null;
    public static final C2491fm coS = null;
    public static final C2491fm coT = null;
    public static final C5663aRz coa = null;
    public static final C5663aRz cob = null;
    public static final C2491fm coc = null;
    public static final C2491fm cod = null;
    public static final C2491fm coe = null;
    public static final C2491fm cof = null;
    public static final C2491fm cog = null;
    public static final C2491fm coh = null;
    public static final C2491fm coi = null;
    public static final C2491fm coj = null;
    public static final C2491fm cok = null;
    public static final C2491fm col = null;
    /* renamed from: com  reason: collision with root package name */
    public static final C2491fm f10386com = null;
    public static final C2491fm con = null;
    public static final C2491fm coo = null;
    public static final C2491fm cop = null;
    public static final C2491fm coq = null;
    public static final C2491fm cor = null;
    public static final C2491fm cos = null;
    public static final C2491fm cot = null;
    public static final C2491fm cou = null;
    public static final C2491fm cov = null;
    public static final C2491fm cow = null;
    public static final C2491fm cox = null;
    public static final C2491fm coy = null;
    public static final C2491fm coz = null;
    public static final long serialVersionUID = 0;
    /* renamed from: uU */
    public static final C5663aRz f163uU = null;
    /* renamed from: uY */
    public static final C2491fm f164uY = null;
    /* renamed from: vc */
    public static final C2491fm f165vc = null;
    /* renamed from: ve */
    public static final C2491fm f166ve = null;
    /* renamed from: vi */
    public static final C2491fm f167vi = null;
    /* renamed from: vm */
    public static final C2491fm f168vm = null;
    /* renamed from: vn */
    public static final C2491fm f169vn = null;
    /* renamed from: vo */
    public static final C2491fm f170vo = null;
    /* renamed from: vs */
    public static final C2491fm f171vs = null;
    /* renamed from: LO */
    private static final float f128LO = 500.0f;
    public static C6494any ___iScriptClass;
    @C0064Am(aul = "5cf325f11a8256c9c637ecef1ab5cb34", aum = 0)

    /* renamed from: LQ */
    private static Asset f129LQ;
    @C0064Am(aul = "58e4d9aa51c579b19cdef083c80ad5de", aum = 1)

    /* renamed from: LW */
    private static Vec3f f131LW;
    @C0064Am(aul = "95ba83a607213f7863827868affd96ea", aum = 2)

    /* renamed from: LY */
    private static Orientation f133LY;
    @C0064Am(aul = "12c5c7c9f4d772e26612ad684a174b26", aum = 16)

    /* renamed from: Me */
    private static C3438ra<Faction> f139Me;
    @C0064Am(aul = "d727a303e7cdee6ceeba7f73e3e8e158", aum = 3)
    private static Asset axR;
    @C0064Am(aul = "9d7c361079ec854372fa4ef5b16f6319", aum = 7)
    private static Store axS;
    @C0064Am(aul = "f1b7687a150f7db7a3366e093bcf7e05", aum = 8)
    private static CloningCenter axT;
    @C0064Am(aul = "db7588f8db5d40238035e900b09275af", aum = 9)
    private static Faction axU;
    @C0064Am(aul = "3cb49860a2c28fd648493b989022d9dc", aum = 10)
    private static C2686iZ<Hangar> axV;
    @C0064Am(aul = "45bed0de13fb210ea8e03b1ba41365c2", aum = 11)
    private static C2686iZ<Storage> axW;
    @C0064Am(aul = "1f03c843afa88dcd5b33a3269e9f7a67", aum = 12)
    private static C2686iZ<Player> axX;
    @C0064Am(aul = "fc83d33a2146223e13258376afa4fc16", aum = 13)
    private static C3438ra<NPC> axY;
    @C0064Am(aul = "7a349fc8f317ede34c09f4f3ab0741eb", aum = 14)
    private static C3438ra<Agent> axZ;
    @C0064Am(aul = "3b498424d29b4073dc438ce096395dbc", aum = 15)
    private static C1556Wo<Agent, Integer> aya;
    @C0064Am(aul = "cf0259290db851f6d79ea712d71a9329", aum = 20)
    private static UndockTasklet ayb;
    @C0064Am(aul = "b43f15eebc7ed203b8cb4d65cf86322a", aum = 22)

    /* renamed from: bK */
    private static UUID f150bK;
    @C0064Am(aul = "1594952326e7602b876b1ae57346ed79", aum = 6)
    private static String handle;
    @C0064Am(aul = "78b68fc5f53af59d0c9d62c610ac27a0", aum = 19)

    /* renamed from: jS */
    private static DatabaseCategory f157jS;
    @C0064Am(aul = "841cc096ac605e2f71bcd3055db0447f", aum = 18)

    /* renamed from: jT */
    private static C3438ra<TaikopediaEntry> f158jT;
    @C0064Am(aul = "0f324e22ba416a6336fc7408951f4ca5", aum = 21)

    /* renamed from: jV */
    private static float f159jV;
    @C0064Am(aul = "ee367c1cff0afbf7fd82a312481e7ce8", aum = 5)

    /* renamed from: jn */
    private static Asset f160jn;
    @C0064Am(aul = "46c59775ff8eb1d867825694082df168", aum = 17)

    /* renamed from: nh */
    private static I18NString f161nh;
    @C0064Am(aul = "05a4eadd18f282b0b10187d507567fe2", aum = 4)

    /* renamed from: uT */
    private static String f162uT;

    static {
        m863V();
    }

    public Station() {
        super((C5540aNg) null);
        super.mo10S();
    }

    public Station(StationType mx) {
        super((C5540aNg) null);
        super._m_script_init(mx);
    }

    public Station(C5540aNg ang) {
        super(ang);
    }

    /* renamed from: V */
    static void m863V() {
        serialVersionUID = (long) 1;
        _m_fieldCount = Actor._m_fieldCount + 23;
        _m_methodCount = Actor._m_methodCount + 81;
        int i = Actor._m_fieldCount;
        C5663aRz[] arzArr = new C5663aRz[(i + 23)];
        C5663aRz b = C5640aRc.m17844b(Station.class, "5cf325f11a8256c9c637ecef1ab5cb34", i);
        f130LR = b;
        arzArr[i] = b;
        int i2 = i + 1;
        C5663aRz b2 = C5640aRc.m17844b(Station.class, "58e4d9aa51c579b19cdef083c80ad5de", i2);
        f132LX = b2;
        arzArr[i2] = b2;
        int i3 = i2 + 1;
        C5663aRz b3 = C5640aRc.m17844b(Station.class, "95ba83a607213f7863827868affd96ea", i3);
        f134LZ = b3;
        arzArr[i3] = b3;
        int i4 = i3 + 1;
        C5663aRz b4 = C5640aRc.m17844b(Station.class, "d727a303e7cdee6ceeba7f73e3e8e158", i4);
        cnR = b4;
        arzArr[i4] = b4;
        int i5 = i4 + 1;
        C5663aRz b5 = C5640aRc.m17844b(Station.class, "05a4eadd18f282b0b10187d507567fe2", i5);
        f163uU = b5;
        arzArr[i5] = b5;
        int i6 = i5 + 1;
        C5663aRz b6 = C5640aRc.m17844b(Station.class, "ee367c1cff0afbf7fd82a312481e7ce8", i6);
        f144NY = b6;
        arzArr[i6] = b6;
        int i7 = i6 + 1;
        C5663aRz b7 = C5640aRc.m17844b(Station.class, "1594952326e7602b876b1ae57346ed79", i7);
        f152bM = b7;
        arzArr[i7] = b7;
        int i8 = i7 + 1;
        C5663aRz b8 = C5640aRc.m17844b(Station.class, "9d7c361079ec854372fa4ef5b16f6319", i8);
        cnS = b8;
        arzArr[i8] = b8;
        int i9 = i8 + 1;
        C5663aRz b9 = C5640aRc.m17844b(Station.class, "f1b7687a150f7db7a3366e093bcf7e05", i9);
        cnT = b9;
        arzArr[i9] = b9;
        int i10 = i9 + 1;
        C5663aRz b10 = C5640aRc.m17844b(Station.class, "db7588f8db5d40238035e900b09275af", i10);
        cnU = b10;
        arzArr[i10] = b10;
        int i11 = i10 + 1;
        C5663aRz b11 = C5640aRc.m17844b(Station.class, "3cb49860a2c28fd648493b989022d9dc", i11);
        cnV = b11;
        arzArr[i11] = b11;
        int i12 = i11 + 1;
        C5663aRz b12 = C5640aRc.m17844b(Station.class, "45bed0de13fb210ea8e03b1ba41365c2", i12);
        cnW = b12;
        arzArr[i12] = b12;
        int i13 = i12 + 1;
        C5663aRz b13 = C5640aRc.m17844b(Station.class, "1f03c843afa88dcd5b33a3269e9f7a67", i13);
        cnX = b13;
        arzArr[i13] = b13;
        int i14 = i13 + 1;
        C5663aRz b14 = C5640aRc.m17844b(Station.class, "fc83d33a2146223e13258376afa4fc16", i14);
        cnY = b14;
        arzArr[i14] = b14;
        int i15 = i14 + 1;
        C5663aRz b15 = C5640aRc.m17844b(Station.class, "7a349fc8f317ede34c09f4f3ab0741eb", i15);
        cnZ = b15;
        arzArr[i15] = b15;
        int i16 = i15 + 1;
        C5663aRz b16 = C5640aRc.m17844b(Station.class, "3b498424d29b4073dc438ce096395dbc", i16);
        coa = b16;
        arzArr[i16] = b16;
        int i17 = i16 + 1;
        C5663aRz b17 = C5640aRc.m17844b(Station.class, "12c5c7c9f4d772e26612ad684a174b26", i17);
        f140Mf = b17;
        arzArr[i17] = b17;
        int i18 = i17 + 1;
        C5663aRz b18 = C5640aRc.m17844b(Station.class, "46c59775ff8eb1d867825694082df168", i18);
        f147QA = b18;
        arzArr[i18] = b18;
        int i19 = i18 + 1;
        C5663aRz b19 = C5640aRc.m17844b(Station.class, "841cc096ac605e2f71bcd3055db0447f", i19);
        awd = b19;
        arzArr[i19] = b19;
        int i20 = i19 + 1;
        C5663aRz b20 = C5640aRc.m17844b(Station.class, "78b68fc5f53af59d0c9d62c610ac27a0", i20);
        aMh = b20;
        arzArr[i20] = b20;
        int i21 = i20 + 1;
        C5663aRz b21 = C5640aRc.m17844b(Station.class, "cf0259290db851f6d79ea712d71a9329", i21);
        cob = b21;
        arzArr[i21] = b21;
        int i22 = i21 + 1;
        C5663aRz b22 = C5640aRc.m17844b(Station.class, "0f324e22ba416a6336fc7408951f4ca5", i22);
        aMi = b22;
        arzArr[i22] = b22;
        int i23 = i22 + 1;
        C5663aRz b23 = C5640aRc.m17844b(Station.class, "b43f15eebc7ed203b8cb4d65cf86322a", i23);
        f151bL = b23;
        arzArr[i23] = b23;
        int i24 = i23 + 1;
        _m_fields = arzArr;
        C1634Xv.m11725a((Object[]) Actor._m_fields, (Object[]) _m_fields);
        int i25 = Actor._m_methodCount;
        C2491fm[] fmVarArr = new C2491fm[(i25 + 81)];
        C2491fm a = C4105zY.m41624a(Station.class, "9871476ad57e9ff631f15d7dab2271bd", i25);
        f153bN = a;
        fmVarArr[i25] = a;
        int i26 = i25 + 1;
        C2491fm a2 = C4105zY.m41624a(Station.class, "56f561fffa1f5bc5a6d7ccd8c23ace45", i26);
        f154bO = a2;
        fmVarArr[i26] = a2;
        int i27 = i26 + 1;
        C2491fm a3 = C4105zY.m41624a(Station.class, "8aa057ccfe057a21464330f6810ca0f4", i27);
        _f_toString_0020_0028_0029Ljava_002flang_002fString_003b = a3;
        fmVarArr[i27] = a3;
        int i28 = i27 + 1;
        C2491fm a4 = C4105zY.m41624a(Station.class, "f0e8f4e77027f5d485ab1089e1cc8e15", i28);
        f155bP = a4;
        fmVarArr[i28] = a4;
        int i29 = i28 + 1;
        C2491fm a5 = C4105zY.m41624a(Station.class, "76bc37157cf7e0d422ab2f01ebf71e71", i29);
        f156bQ = a5;
        fmVarArr[i29] = a5;
        int i30 = i29 + 1;
        C2491fm a6 = C4105zY.m41624a(Station.class, "45f0b2d43c5d98b9f28f4a75993f3029", i30);
        coc = a6;
        fmVarArr[i30] = a6;
        int i31 = i30 + 1;
        C2491fm a7 = C4105zY.m41624a(Station.class, "a30017a8aed9e9b60e7440cb80f2a566", i31);
        cod = a7;
        fmVarArr[i31] = a7;
        int i32 = i31 + 1;
        C2491fm a8 = C4105zY.m41624a(Station.class, "12a2d086a10493c6975fa70e8a0d1f01", i32);
        coe = a8;
        fmVarArr[i32] = a8;
        int i33 = i32 + 1;
        C2491fm a9 = C4105zY.m41624a(Station.class, "5dc93c8544e2a397155990fbf573497f", i33);
        cof = a9;
        fmVarArr[i33] = a9;
        int i34 = i33 + 1;
        C2491fm a10 = C4105zY.m41624a(Station.class, "338b0a7671cf1380589ab0d3880dec04", i34);
        cog = a10;
        fmVarArr[i34] = a10;
        int i35 = i34 + 1;
        C2491fm a11 = C4105zY.m41624a(Station.class, "91d236dc14f64c875903a254cb7c7b27", i35);
        coh = a11;
        fmVarArr[i35] = a11;
        int i36 = i35 + 1;
        C2491fm a12 = C4105zY.m41624a(Station.class, "34d6ac712054bd04ce3ea251ae1fab62", i36);
        f141Mw = a12;
        fmVarArr[i36] = a12;
        int i37 = i36 + 1;
        C2491fm a13 = C4105zY.m41624a(Station.class, "5d8de60037731fd556c74ba19af6e9d2", i37);
        f142Mx = a13;
        fmVarArr[i37] = a13;
        int i38 = i37 + 1;
        C2491fm a14 = C4105zY.m41624a(Station.class, "0e71cbf09f9d6e5f3fbafe5bf37b9472", i38);
        f143My = a14;
        fmVarArr[i38] = a14;
        int i39 = i38 + 1;
        C2491fm a15 = C4105zY.m41624a(Station.class, "1b79af5af349149782184a42c2d12429", i39);
        f148QE = a15;
        fmVarArr[i39] = a15;
        int i40 = i39 + 1;
        C2491fm a16 = C4105zY.m41624a(Station.class, "84d4c4ecfe4c5ceb238e36c49860402b", i40);
        f149QF = a16;
        fmVarArr[i40] = a16;
        int i41 = i40 + 1;
        C2491fm a17 = C4105zY.m41624a(Station.class, "19eedcfcad7dae52bcb3b1a2dd555c2c", i41);
        aMj = a17;
        fmVarArr[i41] = a17;
        int i42 = i41 + 1;
        C2491fm a18 = C4105zY.m41624a(Station.class, "626a8c20504d5d1719e4f8e574920f19", i42);
        coi = a18;
        fmVarArr[i42] = a18;
        int i43 = i42 + 1;
        C2491fm a19 = C4105zY.m41624a(Station.class, "67228a056662c28886b286fecd3b0365", i43);
        coj = a19;
        fmVarArr[i43] = a19;
        int i44 = i43 + 1;
        C2491fm a20 = C4105zY.m41624a(Station.class, "336de3996bcddbf0564da5827c8d821c", i44);
        cok = a20;
        fmVarArr[i44] = a20;
        int i45 = i44 + 1;
        C2491fm a21 = C4105zY.m41624a(Station.class, "15c8fcef6e8ed6dbfc409556b9c29d9e", i45);
        col = a21;
        fmVarArr[i45] = a21;
        int i46 = i45 + 1;
        C2491fm a22 = C4105zY.m41624a(Station.class, "03b4821c843d13077dbe8ccf48863bc6", i46);
        f10386com = a22;
        fmVarArr[i46] = a22;
        int i47 = i46 + 1;
        C2491fm a23 = C4105zY.m41624a(Station.class, "224db252d0d473e78e4b0e75aa080a59", i47);
        con = a23;
        fmVarArr[i47] = a23;
        int i48 = i47 + 1;
        C2491fm a24 = C4105zY.m41624a(Station.class, "7a9b57fa0378d7db7eccf8065d7bc20b", i48);
        bcN = a24;
        fmVarArr[i48] = a24;
        int i49 = i48 + 1;
        C2491fm a25 = C4105zY.m41624a(Station.class, "ab379794e5d9f6d2e73fde62bff0e6a1", i49);
        coo = a25;
        fmVarArr[i49] = a25;
        int i50 = i49 + 1;
        C2491fm a26 = C4105zY.m41624a(Station.class, "b896a100affb489bfee72b98ee96978d", i50);
        cop = a26;
        fmVarArr[i50] = a26;
        int i51 = i50 + 1;
        C2491fm a27 = C4105zY.m41624a(Station.class, "a3a01bb39ed44edee8c7f989b5800488", i51);
        coq = a27;
        fmVarArr[i51] = a27;
        int i52 = i51 + 1;
        C2491fm a28 = C4105zY.m41624a(Station.class, "57f1241ec9aed55fa7a86df6dfc48c86", i52);
        cor = a28;
        fmVarArr[i52] = a28;
        int i53 = i52 + 1;
        C2491fm a29 = C4105zY.m41624a(Station.class, "76a5188794581b364d12b8061978e573", i53);
        cos = a29;
        fmVarArr[i53] = a29;
        int i54 = i53 + 1;
        C2491fm a30 = C4105zY.m41624a(Station.class, "463089bc659945012514529af8a72dc7", i54);
        cot = a30;
        fmVarArr[i54] = a30;
        int i55 = i54 + 1;
        C2491fm a31 = C4105zY.m41624a(Station.class, "663c70d9202cc6cdf8a653bdfddc47b3", i55);
        cou = a31;
        fmVarArr[i55] = a31;
        int i56 = i55 + 1;
        C2491fm a32 = C4105zY.m41624a(Station.class, "3fab1ee031f9ac25af29d744b0f3ca86", i56);
        cov = a32;
        fmVarArr[i56] = a32;
        int i57 = i56 + 1;
        C2491fm a33 = C4105zY.m41624a(Station.class, "9535385d46f124a32fbe695ce475c995", i57);
        cow = a33;
        fmVarArr[i57] = a33;
        int i58 = i57 + 1;
        C2491fm a34 = C4105zY.m41624a(Station.class, "0dd1aa31d04979e836887502d47b2819", i58);
        aMl = a34;
        fmVarArr[i58] = a34;
        int i59 = i58 + 1;
        C2491fm a35 = C4105zY.m41624a(Station.class, "78add8809a01f2e75f7b2598fbabd88d", i59);
        bcI = a35;
        fmVarArr[i59] = a35;
        int i60 = i59 + 1;
        C2491fm a36 = C4105zY.m41624a(Station.class, "7ea3c77ef3ed8a196d7bb23f0f009c5a", i60);
        aMk = a36;
        fmVarArr[i60] = a36;
        int i61 = i60 + 1;
        C2491fm a37 = C4105zY.m41624a(Station.class, "2c83c95c6683c1af892187f6360e3f07", i61);
        f136MH = a37;
        fmVarArr[i61] = a37;
        int i62 = i61 + 1;
        C2491fm a38 = C4105zY.m41624a(Station.class, "f267fd5d2b8ec3df9d8b998bffa6247c", i62);
        cox = a38;
        fmVarArr[i62] = a38;
        int i63 = i62 + 1;
        C2491fm a39 = C4105zY.m41624a(Station.class, "75408e020ba6aad0467ccb0e2185b7ab", i63);
        coy = a39;
        fmVarArr[i63] = a39;
        int i64 = i63 + 1;
        C2491fm a40 = C4105zY.m41624a(Station.class, "14a20f4786f102b474c207971f48ce0e", i64);
        coz = a40;
        fmVarArr[i64] = a40;
        int i65 = i64 + 1;
        C2491fm a41 = C4105zY.m41624a(Station.class, "2692aa40b9a1c2b1ee86672e00bfa93e", i65);
        coA = a41;
        fmVarArr[i65] = a41;
        int i66 = i65 + 1;
        C2491fm a42 = C4105zY.m41624a(Station.class, "19254f0aa27913b10abf5a318ffddbc1", i66);
        coB = a42;
        fmVarArr[i66] = a42;
        int i67 = i66 + 1;
        C2491fm a43 = C4105zY.m41624a(Station.class, "51813e2f1a7de33f15ac96565187eaec", i67);
        coC = a43;
        fmVarArr[i67] = a43;
        int i68 = i67 + 1;
        C2491fm a44 = C4105zY.m41624a(Station.class, "701eb71e7fb201062466195db998bdd6", i68);
        coD = a44;
        fmVarArr[i68] = a44;
        int i69 = i68 + 1;
        C2491fm a45 = C4105zY.m41624a(Station.class, "1bd356c9ef363aa88b78f956fa9e0e8c", i69);
        coE = a45;
        fmVarArr[i69] = a45;
        int i70 = i69 + 1;
        C2491fm a46 = C4105zY.m41624a(Station.class, "ea2a4cecac20c832c3baf38dedf2c84c", i70);
        coF = a46;
        fmVarArr[i70] = a46;
        int i71 = i70 + 1;
        C2491fm a47 = C4105zY.m41624a(Station.class, "480f07b5de942c644caff409d757097b", i71);
        coG = a47;
        fmVarArr[i71] = a47;
        int i72 = i71 + 1;
        C2491fm a48 = C4105zY.m41624a(Station.class, "e6c85b7467a28a3e1a10417b52ae74c3", i72);
        coH = a48;
        fmVarArr[i72] = a48;
        int i73 = i72 + 1;
        C2491fm a49 = C4105zY.m41624a(Station.class, "d5977cab3a48bb001ced5fefb8814aba", i73);
        f165vc = a49;
        fmVarArr[i73] = a49;
        int i74 = i73 + 1;
        C2491fm a50 = C4105zY.m41624a(Station.class, "9f595fd58fbc1bbe3b1857bf87ca4ea9", i74);
        f166ve = a50;
        fmVarArr[i74] = a50;
        int i75 = i74 + 1;
        C2491fm a51 = C4105zY.m41624a(Station.class, "be2588e84afcb0ac8f50a28bfea76c56", i75);
        f169vn = a51;
        fmVarArr[i75] = a51;
        int i76 = i75 + 1;
        C2491fm a52 = C4105zY.m41624a(Station.class, "538f0f929229bc2acea2fa6ea70218c8", i76);
        coI = a52;
        fmVarArr[i76] = a52;
        int i77 = i76 + 1;
        C2491fm a53 = C4105zY.m41624a(Station.class, "561aac75601fbef7ec98dc2737a75509", i77);
        coJ = a53;
        fmVarArr[i77] = a53;
        int i78 = i77 + 1;
        C2491fm a54 = C4105zY.m41624a(Station.class, "dddb09954e683e87709bfafadb70f08a", i78);
        f170vo = a54;
        fmVarArr[i78] = a54;
        int i79 = i78 + 1;
        C2491fm a55 = C4105zY.m41624a(Station.class, "a735926810ec8ce4640b2e00c919623e", i79);
        coK = a55;
        fmVarArr[i79] = a55;
        int i80 = i79 + 1;
        C2491fm a56 = C4105zY.m41624a(Station.class, "6903fb91667faabc2ac01282aa5652c0", i80);
        f168vm = a56;
        fmVarArr[i80] = a56;
        int i81 = i80 + 1;
        C2491fm a57 = C4105zY.m41624a(Station.class, "35c40bae7cb0cbeb7b44356bd57b4cbc", i81);
        f167vi = a57;
        fmVarArr[i81] = a57;
        int i82 = i81 + 1;
        C2491fm a58 = C4105zY.m41624a(Station.class, "a2ab6f9d740741baa1598246f5c45652", i82);
        f171vs = a58;
        fmVarArr[i82] = a58;
        int i83 = i82 + 1;
        C2491fm a59 = C4105zY.m41624a(Station.class, "cc591e776b3b1fad62a6f6a333fa6741", i83);
        _f_onResurrect_0020_0028_0029V = a59;
        fmVarArr[i83] = a59;
        int i84 = i83 + 1;
        C2491fm a60 = C4105zY.m41624a(Station.class, "cc5d03cbb8226874f2701078fccf43e7", i84);
        f135Lm = a60;
        fmVarArr[i84] = a60;
        int i85 = i84 + 1;
        C2491fm a61 = C4105zY.m41624a(Station.class, "bd4fa755f77554e3d219d3707c876bc1", i85);
        coL = a61;
        fmVarArr[i85] = a61;
        int i86 = i85 + 1;
        C2491fm a62 = C4105zY.m41624a(Station.class, "7a053f4dfeb56b265178a6872fb1346f", i86);
        coM = a62;
        fmVarArr[i86] = a62;
        int i87 = i86 + 1;
        C2491fm a63 = C4105zY.m41624a(Station.class, "0ad9970ecd16048f969920cc4847c670", i87);
        coN = a63;
        fmVarArr[i87] = a63;
        int i88 = i87 + 1;
        C2491fm a64 = C4105zY.m41624a(Station.class, "edd47268d6478cb9e78185d586ba2776", i88);
        coO = a64;
        fmVarArr[i88] = a64;
        int i89 = i88 + 1;
        C2491fm a65 = C4105zY.m41624a(Station.class, "d639e126f293205d3bdc0e35aeb3c26a", i89);
        coP = a65;
        fmVarArr[i89] = a65;
        int i90 = i89 + 1;
        C2491fm a66 = C4105zY.m41624a(Station.class, "302280b6493cec26657cb77549a192d5", i90);
        f137MM = a66;
        fmVarArr[i90] = a66;
        int i91 = i90 + 1;
        C2491fm a67 = C4105zY.m41624a(Station.class, "cf642ca34a6195cbbdcdd92caee4038d", i91);
        f164uY = a67;
        fmVarArr[i91] = a67;
        int i92 = i91 + 1;
        C2491fm a68 = C4105zY.m41624a(Station.class, "559add23d3ef9638068126959f593760", i92);
        f138MN = a68;
        fmVarArr[i92] = a68;
        int i93 = i92 + 1;
        C2491fm a69 = C4105zY.m41624a(Station.class, "937de7770eac0b16fa7065099310db79", i93);
        aMn = a69;
        fmVarArr[i93] = a69;
        int i94 = i93 + 1;
        C2491fm a70 = C4105zY.m41624a(Station.class, "4a3358e4eb640903e1974b13b7107906", i94);
        aMo = a70;
        fmVarArr[i94] = a70;
        int i95 = i94 + 1;
        C2491fm a71 = C4105zY.m41624a(Station.class, "303de65302ab95c58f1f2fd1288d529a", i95);
        aMp = a71;
        fmVarArr[i95] = a71;
        int i96 = i95 + 1;
        C2491fm a72 = C4105zY.m41624a(Station.class, "e27b794201662bf6fd34c8f8ca52dfc5", i96);
        f145Ob = a72;
        fmVarArr[i96] = a72;
        int i97 = i96 + 1;
        C2491fm a73 = C4105zY.m41624a(Station.class, "d3f6c97bfba41e86df4e2a525bf98f37", i97);
        f146Oc = a73;
        fmVarArr[i97] = a73;
        int i98 = i97 + 1;
        C2491fm a74 = C4105zY.m41624a(Station.class, "6d9a3e1424cd3844468acb2a34685c88", i98);
        aMr = a74;
        fmVarArr[i98] = a74;
        int i99 = i98 + 1;
        C2491fm a75 = C4105zY.m41624a(Station.class, "3ea93361be0944725ff08a22ae4c815b", i99);
        aMu = a75;
        fmVarArr[i99] = a75;
        int i100 = i99 + 1;
        C2491fm a76 = C4105zY.m41624a(Station.class, "11408f3a02ef9112fb7767c1a59787b4", i100);
        aMv = a76;
        fmVarArr[i100] = a76;
        int i101 = i100 + 1;
        C2491fm a77 = C4105zY.m41624a(Station.class, "63c02af5bfc9fd183c43b53f561e5612", i101);
        coQ = a77;
        fmVarArr[i101] = a77;
        int i102 = i101 + 1;
        C2491fm a78 = C4105zY.m41624a(Station.class, "ca1fd51f929475a0fe7b38b319772278", i102);
        coR = a78;
        fmVarArr[i102] = a78;
        int i103 = i102 + 1;
        C2491fm a79 = C4105zY.m41624a(Station.class, "29fcc264b9a83a542ce04bc9b9cfad77", i103);
        coS = a79;
        fmVarArr[i103] = a79;
        int i104 = i103 + 1;
        C2491fm a80 = C4105zY.m41624a(Station.class, "4a8abb155b6c74750e14499497c8d0e2", i104);
        coT = a80;
        fmVarArr[i104] = a80;
        int i105 = i104 + 1;
        C2491fm a81 = C4105zY.m41624a(Station.class, "4a2929c3433c4523592ebe12ebdaab3c", i105);
        _f_getType_0020_0028_0029Ljava_002flang_002fObject_003b = a81;
        fmVarArr[i105] = a81;
        int i106 = i105 + 1;
        _m_methods = fmVarArr;
        C1634Xv.m11725a((Object[]) Actor._m_methods, (Object[]) _m_methods);
        ___iScriptClass = aUO.m18566a(Station.class, C2947lx.class, _m_fields, _m_methods);
    }

    /* renamed from: a */
    public static void m872a(C1260Sc sc, C3971xY xYVar) {
        if (xYVar.apf() || xYVar.get("spawned") == null || Boolean.FALSE.equals(xYVar.get("spawned"))) {
            String str = (String) xYVar.get("name");
            for (C0665JT jt : (Collection) sc.aPm().get("stellarSystemList")) {
                Iterator it = ((Collection) jt.get("nodes")).iterator();
                while (true) {
                    if (it.hasNext()) {
                        Iterator it2 = ((Collection) ((C0665JT) it.next()).get("staticObjects")).iterator();
                        while (true) {
                            if (it2.hasNext()) {
                                C0665JT jt2 = (C0665JT) it2.next();
                                if (Station.class.getName().equals(jt2.bau()) && jt2 != xYVar && str.equals(jt2.get("name"))) {
                                    System.out.println("Station " + xYVar + " (" + str + ") becoming " + jt2);
                                    xYVar.mo10080c(jt2);
                                    return;
                                }
                            }
                        }
                    }
                }
            }
            System.err.println("Could not merge station " + xYVar + " (" + str + ")");
        }
    }

    /* renamed from: H */
    private void m853H(String str) {
        throw new C6039afL();
    }

    /* renamed from: Ll */
    private C3438ra m854Ll() {
        return (C3438ra) bFf().mo5608dq().mo3214p(awd);
    }

    /* renamed from: RR */
    private DatabaseCategory m855RR() {
        return (DatabaseCategory) bFf().mo5608dq().mo3214p(aMh);
    }

    /* renamed from: RS */
    private float m856RS() {
        return bFf().mo5608dq().mo3211m(aMi);
    }

    @C0064Am(aul = "14a20f4786f102b474c207971f48ce0e", aum = 0)
    @C5566aOg
    /* renamed from: a */
    private C0286Dh.C0287a m865a(Character acx, boolean z) {
        throw new aWi(new aCE(this, coz, new Object[]{acx, new Boolean(z)}));
    }

    @C0064Am(aul = "2692aa40b9a1c2b1ee86672e00bfa93e", aum = 0)
    @C5566aOg
    /* renamed from: a */
    private C0286Dh.C0287a m866a(Character acx, boolean z, boolean z2) {
        throw new aWi(new aCE(this, coA, new Object[]{acx, new Boolean(z), new Boolean(z2)}));
    }

    /* renamed from: a */
    private void m867a(UndockTasklet cVar) {
        bFf().mo5608dq().mo3197f(cob, cVar);
    }

    @C3248pc(aYR = C3248pc.C3250b.ADDER, displayName = "Banned Factions")
    @C0064Am(aul = "34d6ac712054bd04ce3ea251ae1fab62", aum = 0)
    @C5566aOg
    /* renamed from: a */
    private void m873a(Faction xm) {
        throw new aWi(new aCE(this, f141Mw, new Object[]{xm}));
    }

    @C0064Am(aul = "75408e020ba6aad0467ccb0e2185b7ab", aum = 0)
    @C5566aOg
    @C2499fr(mo18857qh = 2)
    /* renamed from: a */
    private void m874a(Character acx, float f) {
        throw new aWi(new aCE(this, coy, new Object[]{acx, new Float(f)}));
    }

    @C4034yP
    @C5566aOg
    @C0064Am(aul = "19254f0aa27913b10abf5a318ffddbc1", aum = 0)
    @C2499fr
    /* renamed from: a */
    private void m875a(Character acx, boolean z, boolean z2, Ship fAVar) {
        throw new aWi(new aCE(this, coB, new Object[]{acx, new Boolean(z), new Boolean(z2), fAVar}));
    }

    /* renamed from: a */
    private void m876a(DatabaseCategory aik) {
        bFf().mo5608dq().mo3197f(aMh, aik);
    }

    @C3248pc(aYR = C3248pc.C3250b.ADDER, displayName = "Taikopedia Entries")
    @C0064Am(aul = "0dd1aa31d04979e836887502d47b2819", aum = 0)
    @C5566aOg
    /* renamed from: a */
    private void m877a(TaikopediaEntry aiz) {
        throw new aWi(new aCE(this, aMl, new Object[]{aiz}));
    }

    @C3248pc(aYR = C3248pc.C3250b.ALTERNATE_ADDER, displayName = "Agents")
    @C0064Am(aul = "a30017a8aed9e9b60e7440cb80f2a566", aum = 0)
    @C5566aOg
    /* renamed from: a */
    private void m879a(AgentType goVar) {
        throw new aWi(new aCE(this, cod, new Object[]{goVar}));
    }

    @C0064Am(aul = "626a8c20504d5d1719e4f8e574920f19", aum = 0)
    @C5566aOg
    /* renamed from: a */
    private void m880a(Hangar kRVar) {
        throw new aWi(new aCE(this, coi, new Object[]{kRVar}));
    }

    /* renamed from: a */
    private void m881a(CloningCenter koVar) {
        bFf().mo5608dq().mo3197f(cnT, koVar);
    }

    /* renamed from: a */
    private void m882a(String str) {
        bFf().mo5608dq().mo3197f(f152bM, str);
    }

    /* renamed from: a */
    private void m883a(UUID uuid) {
        bFf().mo5608dq().mo3197f(f151bL, uuid);
    }

    /* renamed from: a */
    private void m884a(Orientation orientation) {
        throw new C6039afL();
    }

    @C0064Am(aul = "ea2a4cecac20c832c3baf38dedf2c84c", aum = 0)
    @C5566aOg
    /* renamed from: a */
    private boolean m886a(Character acx, Ship fAVar) {
        throw new aWi(new aCE(this, coF, new Object[]{acx, fAVar}));
    }

    /* renamed from: ae */
    private void m888ae(C3438ra raVar) {
        bFf().mo5608dq().mo3197f(cnY, raVar);
    }

    @C0064Am(aul = "ab379794e5d9f6d2e73fde62bff0e6a1", aum = 0)
    @C5566aOg
    /* renamed from: af */
    private void m889af(Player aku) {
        throw new aWi(new aCE(this, coo, new Object[]{aku}));
    }

    /* renamed from: af */
    private void m890af(C3438ra raVar) {
        bFf().mo5608dq().mo3197f(cnZ, raVar);
    }

    @C0064Am(aul = "b896a100affb489bfee72b98ee96978d", aum = 0)
    @C5566aOg
    @C2499fr
    /* renamed from: ah */
    private void m891ah(Player aku) {
        throw new aWi(new aCE(this, cop, new Object[]{aku}));
    }

    /* renamed from: ai */
    private void m892ai(Asset tCVar) {
        throw new C6039afL();
    }

    @C0401Fa
    /* renamed from: ak */
    private void m894ak(Player aku) {
        switch (bFf().mo6893i(coC)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, coC, new Object[]{aku}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, coC, new Object[]{aku}));
                break;
        }
        m893aj(aku);
    }

    @C0064Am(aul = "538f0f929229bc2acea2fa6ea70218c8", aum = 0)
    @C5566aOg
    @C2499fr
    /* renamed from: al */
    private Storage m895al(Player aku) {
        throw new aWi(new aCE(this, coI, new Object[]{aku}));
    }

    @C0064Am(aul = "561aac75601fbef7ec98dc2737a75509", aum = 0)
    @C5566aOg
    @C2499fr
    /* renamed from: an */
    private Storage m896an(Player aku) {
        throw new aWi(new aCE(this, coJ, new Object[]{aku}));
    }

    /* renamed from: an */
    private UUID m897an() {
        return (UUID) bFf().mo5608dq().mo3214p(f151bL);
    }

    @C5566aOg
    @C2499fr
    /* renamed from: ao */
    private Storage m898ao(Player aku) {
        switch (bFf().mo6893i(coJ)) {
            case 0:
                return null;
            case 2:
                return (Storage) bFf().mo5606d(new aCE(this, coJ, new Object[]{aku}));
            case 3:
                bFf().mo5606d(new aCE(this, coJ, new Object[]{aku}));
                break;
        }
        return m896an(aku);
    }

    /* renamed from: ao */
    private String m899ao() {
        return (String) bFf().mo5608dq().mo3214p(f152bM);
    }

    @C0064Am(aul = "29fcc264b9a83a542ce04bc9b9cfad77", aum = 0)
    @C5566aOg
    /* renamed from: ap */
    private void m901ap(Player aku) {
        throw new aWi(new aCE(this, coS, new Object[]{aku}));
    }

    @C0064Am(aul = "4a8abb155b6c74750e14499497c8d0e2", aum = 0)
    @C5566aOg
    /* renamed from: ar */
    private Storage m902ar(Player aku) {
        throw new aWi(new aCE(this, coT, new Object[]{aku}));
    }

    @C0064Am(aul = "a735926810ec8ce4640b2e00c919623e", aum = 0)
    @C5566aOg
    @C2499fr
    private void azQ() {
        throw new aWi(new aCE(this, coK, new Object[0]));
    }

    @C0064Am(aul = "d639e126f293205d3bdc0e35aeb3c26a", aum = 0)
    @C5566aOg
    @C2499fr
    private void azT() {
        throw new aWi(new aCE(this, coP, new Object[0]));
    }

    @C0064Am(aul = "ca1fd51f929475a0fe7b38b319772278", aum = 0)
    @C5566aOg
    @C2499fr(mo18857qh = 1)
    private Agent azX() {
        throw new aWi(new aCE(this, coR, new Object[0]));
    }

    private Asset azn() {
        return ((StationType) getType()).bhj();
    }

    private Store azo() {
        return (Store) bFf().mo5608dq().mo3214p(cnS);
    }

    private CloningCenter azp() {
        return (CloningCenter) bFf().mo5608dq().mo3214p(cnT);
    }

    private Faction azq() {
        return (Faction) bFf().mo5608dq().mo3214p(cnU);
    }

    private C2686iZ azr() {
        return (C2686iZ) bFf().mo5608dq().mo3214p(cnV);
    }

    private C2686iZ azs() {
        return (C2686iZ) bFf().mo5608dq().mo3214p(cnW);
    }

    private C2686iZ azt() {
        return (C2686iZ) bFf().mo5608dq().mo3214p(cnX);
    }

    private C3438ra azu() {
        return (C3438ra) bFf().mo5608dq().mo3214p(cnY);
    }

    private C3438ra azv() {
        return (C3438ra) bFf().mo5608dq().mo3214p(cnZ);
    }

    private C1556Wo azw() {
        return (C1556Wo) bFf().mo5608dq().mo3214p(coa);
    }

    private UndockTasklet azx() {
        return (UndockTasklet) bFf().mo5608dq().mo3214p(cob);
    }

    @C3248pc(aYR = C3248pc.C3250b.ADDER, displayName = "Agents")
    @C0064Am(aul = "45f0b2d43c5d98b9f28f4a75993f3029", aum = 0)
    @C5566aOg
    /* renamed from: b */
    private void m905b(Agent abk) {
        throw new aWi(new aCE(this, coc, new Object[]{abk}));
    }

    /* access modifiers changed from: private */
    @C4034yP
    @C5566aOg
    @C2499fr
    /* renamed from: b */
    public void m906b(Character acx, boolean z, boolean z2, Ship fAVar) {
        switch (bFf().mo6893i(coB)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, coB, new Object[]{acx, new Boolean(z), new Boolean(z2), fAVar}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, coB, new Object[]{acx, new Boolean(z), new Boolean(z2), fAVar}));
                break;
        }
        m875a(acx, z, z2, fAVar);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Database Category")
    @C0064Am(aul = "4a3358e4eb640903e1974b13b7107906", aum = 0)
    @C5566aOg
    /* renamed from: b */
    private void m907b(DatabaseCategory aik) {
        throw new aWi(new aCE(this, aMo, new Object[]{aik}));
    }

    /* renamed from: b */
    private void m908b(Store hfVar) {
        bFf().mo5608dq().mo3197f(cnS, hfVar);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Cloning Center")
    @C0064Am(aul = "663c70d9202cc6cdf8a653bdfddc47b3", aum = 0)
    @C5566aOg
    /* renamed from: b */
    private void m909b(CloningCenter koVar) {
        throw new aWi(new aCE(this, cou, new Object[]{koVar}));
    }

    @C5566aOg
    /* renamed from: b */
    private boolean m912b(Character acx, Ship fAVar) {
        switch (bFf().mo6893i(coF)) {
            case 0:
                return false;
            case 2:
                return ((Boolean) bFf().mo5606d(new aCE(this, coF, new Object[]{acx, fAVar}))).booleanValue();
            case 3:
                bFf().mo5606d(new aCE(this, coF, new Object[]{acx, fAVar}));
                break;
        }
        return m886a(acx, fAVar);
    }

    /* renamed from: br */
    private void m913br(I18NString i18NString) {
        bFf().mo5608dq().mo3197f(f147QA, i18NString);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Description")
    @C0064Am(aul = "84d4c4ecfe4c5ceb238e36c49860402b", aum = 0)
    @C5566aOg
    /* renamed from: bu */
    private void m914bu(I18NString i18NString) {
        throw new aWi(new aCE(this, f149QF, new Object[]{i18NString}));
    }

    @C3248pc(aYR = C3248pc.C3250b.REMOVER, displayName = "Banned Factions")
    @C0064Am(aul = "5d8de60037731fd556c74ba19af6e9d2", aum = 0)
    @C5566aOg
    /* renamed from: c */
    private void m918c(Faction xm) {
        throw new aWi(new aCE(this, f142Mx, new Object[]{xm}));
    }

    @C4034yP
    @C5566aOg
    @C0064Am(aul = "7a9b57fa0378d7db7eccf8065d7bc20b", aum = 0)
    @C2499fr
    /* renamed from: c */
    private void m919c(Player aku, String str) {
        throw new aWi(new aCE(this, bcN, new Object[]{aku, str}));
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Store")
    @C0064Am(aul = "76a5188794581b364d12b8061978e573", aum = 0)
    @C5566aOg
    /* renamed from: c */
    private void m920c(Store hfVar) {
        throw new aWi(new aCE(this, cos, new Object[]{hfVar}));
    }

    @C0064Am(aul = "67228a056662c28886b286fecd3b0365", aum = 0)
    @C5566aOg
    @C2499fr
    /* renamed from: c */
    private void m921c(Hangar kRVar) {
        throw new aWi(new aCE(this, coj, new Object[]{kRVar}));
    }

    /* renamed from: c */
    private void m923c(UUID uuid) {
        switch (bFf().mo6893i(f154bO)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f154bO, new Object[]{uuid}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f154bO, new Object[]{uuid}));
                break;
        }
        m911b(uuid);
    }

    /* renamed from: cx */
    private void m924cx(float f) {
        bFf().mo5608dq().mo3150a(aMi, f);
    }

    @C3248pc(aYR = C3248pc.C3250b.REMOVER, displayName = "Agents")
    @C0064Am(aul = "12a2d086a10493c6975fa70e8a0d1f01", aum = 0)
    @C5566aOg
    /* renamed from: d */
    private void m927d(Agent abk) {
        throw new aWi(new aCE(this, coe, new Object[]{abk}));
    }

    /* renamed from: e */
    private void m928e(C0665JT jt) {
        switch (bFf().mo6893i(coM)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, coM, new Object[]{jt}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, coM, new Object[]{jt}));
                break;
        }
        m926d(jt);
    }

    @C3248pc(aYR = C3248pc.C3250b.REMOVER, displayName = "Taikopedia Entries")
    @C0064Am(aul = "78add8809a01f2e75f7b2598fbabd88d", aum = 0)
    @C5566aOg
    /* renamed from: e */
    private void m929e(TaikopediaEntry aiz) {
        throw new aWi(new aCE(this, bcI, new Object[]{aiz}));
    }

    @C5566aOg
    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Agents Order")
    @C0064Am(aul = "91d236dc14f64c875903a254cb7c7b27", aum = 0)
    /* renamed from: h */
    private void m930h(Map<Agent, Integer> map) {
        throw new aWi(new aCE(this, coh, new Object[]{map}));
    }

    @C0064Am(aul = "15c8fcef6e8ed6dbfc409556b9c29d9e", aum = 0)
    @C5566aOg
    /* renamed from: i */
    private void m931i(Storage qzVar) {
        throw new aWi(new aCE(this, col, new Object[]{qzVar}));
    }

    /* renamed from: i */
    private void m932i(Vec3f vec3f) {
        throw new C6039afL();
    }

    /* renamed from: ij */
    private String m933ij() {
        return ((StationType) getType()).mo4106iu();
    }

    @C5566aOg
    /* renamed from: j */
    private void m938j(Storage qzVar) {
        switch (bFf().mo6893i(col)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, col, new Object[]{qzVar}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, col, new Object[]{qzVar}));
                break;
        }
        m931i(qzVar);
    }

    @C0064Am(aul = "03b4821c843d13077dbe8ccf48863bc6", aum = 0)
    @C5566aOg
    /* renamed from: k */
    private void m939k(Storage qzVar) {
        throw new aWi(new aCE(this, f10386com, new Object[]{qzVar}));
    }

    /* renamed from: l */
    private void m940l(Faction xm) {
        bFf().mo5608dq().mo3197f(cnU, xm);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Faction")
    @C0064Am(aul = "9535385d46f124a32fbe695ce475c995", aum = 0)
    @C5566aOg
    /* renamed from: m */
    private void m941m(Faction xm) {
        throw new aWi(new aCE(this, cow, new Object[]{xm}));
    }

    @C0064Am(aul = "f267fd5d2b8ec3df9d8b998bffa6247c", aum = 0)
    @C5566aOg
    @C2499fr(mo18857qh = 2)
    /* renamed from: n */
    private C0286Dh.C0287a m942n(Character acx) {
        throw new aWi(new aCE(this, cox, new Object[]{acx}));
    }

    @C0064Am(aul = "1bd356c9ef363aa88b78f956fa9e0e8c", aum = 0)
    @C5566aOg
    /* renamed from: p */
    private boolean m943p(Character acx) {
        throw new aWi(new aCE(this, coE, new Object[]{acx}));
    }

    /* renamed from: q */
    private void m944q(C2686iZ iZVar) {
        bFf().mo5608dq().mo3197f(cnV, iZVar);
    }

    /* renamed from: q */
    private void m945q(C3438ra raVar) {
        bFf().mo5608dq().mo3197f(f140Mf, raVar);
    }

    @C0064Am(aul = "cc5d03cbb8226874f2701078fccf43e7", aum = 0)
    @C5566aOg
    @C2499fr
    /* renamed from: qU */
    private void m946qU() {
        throw new aWi(new aCE(this, f135Lm, new Object[0]));
    }

    @C0064Am(aul = "4a2929c3433c4523592ebe12ebdaab3c", aum = 0)
    /* renamed from: qW */
    private Object m947qW() {
        return azP();
    }

    /* renamed from: r */
    private void m948r(C2686iZ iZVar) {
        bFf().mo5608dq().mo3197f(cnW, iZVar);
    }

    /* renamed from: r */
    private void m949r(Asset tCVar) {
        throw new C6039afL();
    }

    @C0064Am(aul = "e6c85b7467a28a3e1a10417b52ae74c3", aum = 0)
    @C5566aOg
    /* renamed from: r */
    private boolean m950r(Character acx) {
        throw new aWi(new aCE(this, coH, new Object[]{acx}));
    }

    /* renamed from: rh */
    private Asset m953rh() {
        return ((StationType) getType()).mo3521Nu();
    }

    /* renamed from: rj */
    private Vec3f m954rj() {
        return ((StationType) getType()).mo4109rJ();
    }

    /* renamed from: rk */
    private Orientation m955rk() {
        return ((StationType) getType()).mo4108rH();
    }

    /* renamed from: ro */
    private C3438ra m956ro() {
        return (C3438ra) bFf().mo5608dq().mo3214p(f140Mf);
    }

    /* renamed from: s */
    private void m957s(C2686iZ iZVar) {
        bFf().mo5608dq().mo3197f(cnX, iZVar);
    }

    /* renamed from: sG */
    private Asset m958sG() {
        return (Asset) bFf().mo5608dq().mo3214p(f144NY);
    }

    /* renamed from: t */
    private void m960t(Asset tCVar) {
        bFf().mo5608dq().mo3197f(f144NY, tCVar);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Icon")
    @C0064Am(aul = "d3f6c97bfba41e86df4e2a525bf98f37", aum = 0)
    @C5566aOg
    /* renamed from: u */
    private void m961u(Asset tCVar) {
        throw new aWi(new aCE(this, f146Oc, new Object[]{tCVar}));
    }

    /* renamed from: vT */
    private I18NString m962vT() {
        return (I18NString) bFf().mo5608dq().mo3214p(f147QA);
    }

    /* renamed from: x */
    private void m964x(C3438ra raVar) {
        bFf().mo5608dq().mo3197f(awd, raVar);
    }

    /* renamed from: z */
    private void m965z(C1556Wo wo) {
        bFf().mo5608dq().mo3197f(coa, wo);
    }

    /* renamed from: RU */
    public I18NString mo182RU() {
        switch (bFf().mo6893i(aMj)) {
            case 0:
                return null;
            case 2:
                return (I18NString) bFf().mo5606d(new aCE(this, aMj, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, aMj, new Object[0]));
                break;
        }
        return m857RT();
    }

    @C3248pc(aYR = C3248pc.C3250b.ITERATOR, displayName = "Taikopedia Entries")
    /* renamed from: RW */
    public List<TaikopediaEntry> mo183RW() {
        switch (bFf().mo6893i(aMk)) {
            case 0:
                return null;
            case 2:
                return (List) bFf().mo5606d(new aCE(this, aMk, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, aMk, new Object[0]));
                break;
        }
        return m858RV();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Database Category")
    /* renamed from: RY */
    public DatabaseCategory mo184RY() {
        switch (bFf().mo6893i(aMn)) {
            case 0:
                return null;
            case 2:
                return (DatabaseCategory) bFf().mo5606d(new aCE(this, aMn, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, aMn, new Object[0]));
                break;
        }
        return m859RX();
    }

    /* renamed from: Sa */
    public String mo185Sa() {
        switch (bFf().mo6893i(aMp)) {
            case 0:
                return null;
            case 2:
                return (String) bFf().mo5606d(new aCE(this, aMp, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, aMp, new Object[0]));
                break;
        }
        return m860RZ();
    }

    /* renamed from: Se */
    public Asset mo187Se() {
        switch (bFf().mo6893i(aMr)) {
            case 0:
                return null;
            case 2:
                return (Asset) bFf().mo5606d(new aCE(this, aMr, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, aMr, new Object[0]));
                break;
        }
        return m861Sd();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Pda MP Multiplier")
    /* renamed from: Sg */
    public float mo188Sg() {
        switch (bFf().mo6893i(aMu)) {
            case 0:
                return 0.0f;
            case 2:
                return ((Float) bFf().mo5606d(new aCE(this, aMu, new Object[0]))).floatValue();
            case 3:
                bFf().mo5606d(new aCE(this, aMu, new Object[0]));
                break;
        }
        return m862Sf();
    }

    /* renamed from: T */
    public C6494any mo11T() {
        return ___iScriptClass;
    }

    /* renamed from: U */
    public C2491fm[] mo12U() {
        return _m_methods;
    }

    /* renamed from: W */
    public Object mo13W() {
        return new C2947lx(this);
    }

    /* renamed from: Y */
    public boolean mo599Y(float f) {
        switch (bFf().mo6893i(f137MM)) {
            case 0:
                return false;
            case 2:
                return ((Boolean) bFf().mo5606d(new aCE(this, f137MM, new Object[]{new Float(f)}))).booleanValue();
            case 3:
                bFf().mo5606d(new aCE(this, f137MM, new Object[]{new Float(f)}));
                break;
        }
        return m864X(f);
    }

    /* renamed from: a */
    public Object mo14a(C0495Gr gr) {
        Object[] args = gr.getArgs();
        switch (gr.mo2417hq() - Actor._m_methodCount) {
            case 0:
                return m900ap();
            case 1:
                m911b((UUID) args[0]);
                return null;
            case 2:
                return m904au();
            case 3:
                return m903ar();
            case 4:
                m910b((String) args[0]);
                return null;
            case 5:
                m905b((Agent) args[0]);
                return null;
            case 6:
                m879a((AgentType) args[0]);
                return null;
            case 7:
                m927d((Agent) args[0]);
                return null;
            case 8:
                return azy();
            case 9:
                return azA();
            case 10:
                m930h((Map) args[0]);
                return null;
            case 11:
                m873a((Faction) args[0]);
                return null;
            case 12:
                m918c((Faction) args[0]);
                return null;
            case 13:
                return m951rA();
            case 14:
                return m963vV();
            case 15:
                m914bu((I18NString) args[0]);
                return null;
            case 16:
                return m857RT();
            case 17:
                m880a((Hangar) args[0]);
                return null;
            case 18:
                m921c((Hangar) args[0]);
                return null;
            case 19:
                return azC();
            case 20:
                m931i((Storage) args[0]);
                return null;
            case 21:
                m939k((Storage) args[0]);
                return null;
            case 22:
                return azE();
            case 23:
                m919c((Player) args[0], (String) args[1]);
                return null;
            case 24:
                m889af((Player) args[0]);
                return null;
            case 25:
                m891ah((Player) args[0]);
                return null;
            case 26:
                return azG();
            case 27:
                return azI();
            case 28:
                m920c((Store) args[0]);
                return null;
            case 29:
                return azK();
            case 30:
                m909b((CloningCenter) args[0]);
                return null;
            case 31:
                return azM();
            case 32:
                m941m((Faction) args[0]);
                return null;
            case 33:
                m877a((TaikopediaEntry) args[0]);
                return null;
            case 34:
                m929e((TaikopediaEntry) args[0]);
                return null;
            case 35:
                return m858RV();
            case 36:
                return m915c((Character) args[0]);
            case 37:
                return m942n((Character) args[0]);
            case 38:
                m874a((Character) args[0], ((Float) args[1]).floatValue());
                return null;
            case 39:
                return m865a((Character) args[0], ((Boolean) args[1]).booleanValue());
            case 40:
                return m866a((Character) args[0], ((Boolean) args[1]).booleanValue(), ((Boolean) args[2]).booleanValue());
            case 41:
                m875a((Character) args[0], ((Boolean) args[1]).booleanValue(), ((Boolean) args[2]).booleanValue(), (Ship) args[3]);
                return null;
            case 42:
                m893aj((Player) args[0]);
                return null;
            case 43:
                m922c((Runnable) args[0]);
                return null;
            case 44:
                return new Boolean(m943p((Character) args[0]));
            case 45:
                return new Boolean(m886a((Character) args[0], (Ship) args[1]));
            case 46:
                return azO();
            case 47:
                return new Boolean(m950r((Character) args[0]));
            case 48:
                return m934io();
            case 49:
                return m935iq();
            case 50:
                m917c((Actor) args[0], (C5260aCm) args[1], (Vec3f) args[2], (Vec3f) args[3]);
                return null;
            case 51:
                return m895al((Player) args[0]);
            case 52:
                return m896an((Player) args[0]);
            case 53:
                return m937iz();
            case 54:
                azQ();
                return null;
            case 55:
                m871a((Actor) args[0], (C5260aCm) args[1], (Vec3f) args[2], (Vec3f) args[3]);
                return null;
            case 56:
                return m936it();
            case 57:
                m870a((Actor.C0200h) args[0]);
                return null;
            case 58:
                m887aG();
                return null;
            case 59:
                m946qU();
                return null;
            case 60:
                m878a((C6417amZ) args[0]);
                return null;
            case 61:
                m926d((C0665JT) args[0]);
                return null;
            case 62:
                return new Boolean(azS());
            case 63:
                return new Boolean(m885a((C0437GE) args[0], (Player) args[1]));
            case 64:
                azT();
                return null;
            case 65:
                return new Boolean(m864X(((Float) args[0]).floatValue()));
            case 66:
                return m916c((Space) args[0], ((Long) args[1]).longValue());
            case 67:
                return m952rO();
            case 68:
                return m859RX();
            case 69:
                m907b((DatabaseCategory) args[0]);
                return null;
            case 70:
                return m860RZ();
            case 71:
                return m959sJ();
            case 72:
                m961u((Asset) args[0]);
                return null;
            case 73:
                return m861Sd();
            case 74:
                return new Float(m862Sf());
            case 75:
                m925cy(((Float) args[0]).floatValue());
                return null;
            case 76:
                return azV();
            case 77:
                return azX();
            case 78:
                m901ap((Player) args[0]);
                return null;
            case 79:
                return m902ar((Player) args[0]);
            case 80:
                return m947qW();
            default:
                return super.mo14a(gr);
        }
    }

    /* renamed from: a */
    public void mo15a(Collection collection, C0495Gr gr) {
        super.mo15a(collection, gr);
    }

    /* renamed from: aH */
    public void mo70aH() {
        switch (bFf().mo6893i(_f_onResurrect_0020_0028_0029V)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, _f_onResurrect_0020_0028_0029V, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, _f_onResurrect_0020_0028_0029V, new Object[0]));
                break;
        }
        m887aG();
    }

    @C5566aOg
    /* renamed from: ag */
    public void mo600ag(Player aku) {
        switch (bFf().mo6893i(coo)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, coo, new Object[]{aku}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, coo, new Object[]{aku}));
                break;
        }
        m889af(aku);
    }

    @C5566aOg
    @C2499fr
    /* renamed from: ai */
    public void mo601ai(Player aku) {
        switch (bFf().mo6893i(cop)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, cop, new Object[]{aku}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, cop, new Object[]{aku}));
                break;
        }
        m891ah(aku);
    }

    @C5566aOg
    @C2499fr
    /* renamed from: am */
    public Storage mo602am(Player aku) {
        switch (bFf().mo6893i(coI)) {
            case 0:
                return null;
            case 2:
                return (Storage) bFf().mo5606d(new aCE(this, coI, new Object[]{aku}));
            case 3:
                bFf().mo5606d(new aCE(this, coI, new Object[]{aku}));
                break;
        }
        return m895al(aku);
    }

    /* renamed from: aq */
    public UUID mo18aq() {
        switch (bFf().mo6893i(f153bN)) {
            case 0:
                return null;
            case 2:
                return (UUID) bFf().mo5606d(new aCE(this, f153bN, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f153bN, new Object[0]));
                break;
        }
        return m900ap();
    }

    @C5566aOg
    /* renamed from: aq */
    public void mo603aq(Player aku) {
        switch (bFf().mo6893i(coS)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, coS, new Object[]{aku}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, coS, new Object[]{aku}));
                break;
        }
        m901ap(aku);
    }

    @C5566aOg
    /* renamed from: as */
    public Storage mo604as(Player aku) {
        switch (bFf().mo6893i(coT)) {
            case 0:
                return null;
            case 2:
                return (Storage) bFf().mo5606d(new aCE(this, coT, new Object[]{aku}));
            case 3:
                bFf().mo5606d(new aCE(this, coT, new Object[]{aku}));
                break;
        }
        return m902ar(aku);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Agents Order")
    public Map<Agent, Integer> azB() {
        switch (bFf().mo6893i(cog)) {
            case 0:
                return null;
            case 2:
                return (Map) bFf().mo5606d(new aCE(this, cog, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, cog, new Object[0]));
                break;
        }
        return azA();
    }

    public Set<Hangar> azD() {
        switch (bFf().mo6893i(cok)) {
            case 0:
                return null;
            case 2:
                return (Set) bFf().mo5606d(new aCE(this, cok, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, cok, new Object[0]));
                break;
        }
        return azC();
    }

    public Set<Storage> azF() {
        switch (bFf().mo6893i(con)) {
            case 0:
                return null;
            case 2:
                return (Set) bFf().mo5606d(new aCE(this, con, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, con, new Object[0]));
                break;
        }
        return azE();
    }

    public Set<Player> azH() {
        switch (bFf().mo6893i(coq)) {
            case 0:
                return null;
            case 2:
                return (Set) bFf().mo5606d(new aCE(this, coq, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, coq, new Object[0]));
                break;
        }
        return azG();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Store")
    public Store azJ() {
        switch (bFf().mo6893i(cor)) {
            case 0:
                return null;
            case 2:
                return (Store) bFf().mo5606d(new aCE(this, cor, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, cor, new Object[0]));
                break;
        }
        return azI();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Cloning Center")
    public CloningCenter azL() {
        switch (bFf().mo6893i(cot)) {
            case 0:
                return null;
            case 2:
                return (CloningCenter) bFf().mo5606d(new aCE(this, cot, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, cot, new Object[0]));
                break;
        }
        return azK();
    }

    @aFW("Faction")
    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Faction")
    public Faction azN() {
        switch (bFf().mo6893i(cov)) {
            case 0:
                return null;
            case 2:
                return (Faction) bFf().mo5606d(new aCE(this, cov, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, cov, new Object[0]));
                break;
        }
        return azM();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Type")
    public StationType azP() {
        switch (bFf().mo6893i(coG)) {
            case 0:
                return null;
            case 2:
                return (StationType) bFf().mo5606d(new aCE(this, coG, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, coG, new Object[0]));
                break;
        }
        return azO();
    }

    @C5566aOg
    @C2499fr
    public void azR() {
        switch (bFf().mo6893i(coK)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, coK, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, coK, new Object[0]));
                break;
        }
        azQ();
    }

    @C5566aOg
    @C2499fr
    public void azU() {
        switch (bFf().mo6893i(coP)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, coP, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, coP, new Object[0]));
                break;
        }
        azT();
    }

    @aFW("Node")
    public Node azW() {
        switch (bFf().mo6893i(coQ)) {
            case 0:
                return null;
            case 2:
                return (Node) bFf().mo5606d(new aCE(this, coQ, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, coQ, new Object[0]));
                break;
        }
        return azV();
    }

    @C5566aOg
    @C2499fr(mo18857qh = 1)
    public Agent azY() {
        switch (bFf().mo6893i(coR)) {
            case 0:
                return null;
            case 2:
                return (Agent) bFf().mo5606d(new aCE(this, coR, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, coR, new Object[0]));
                break;
        }
        return azX();
    }

    @aFW("Agents")
    @C3248pc(aYR = C3248pc.C3250b.ITERATOR, displayName = "Agents")
    public List<Agent> azz() {
        switch (bFf().mo6893i(cof)) {
            case 0:
                return null;
            case 2:
                return (List) bFf().mo5606d(new aCE(this, cof, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, cof, new Object[0]));
                break;
        }
        return azy();
    }

    @C5566aOg
    /* renamed from: b */
    public C0286Dh.C0287a mo618b(Character acx, boolean z) {
        switch (bFf().mo6893i(coz)) {
            case 0:
                return null;
            case 2:
                return (C0286Dh.C0287a) bFf().mo5606d(new aCE(this, coz, new Object[]{acx, new Boolean(z)}));
            case 3:
                bFf().mo5606d(new aCE(this, coz, new Object[]{acx, new Boolean(z)}));
                break;
        }
        return m865a(acx, z);
    }

    @C5566aOg
    /* renamed from: b */
    public C0286Dh.C0287a mo619b(Character acx, boolean z, boolean z2) {
        switch (bFf().mo6893i(coA)) {
            case 0:
                return null;
            case 2:
                return (C0286Dh.C0287a) bFf().mo5606d(new aCE(this, coA, new Object[]{acx, new Boolean(z), new Boolean(z2)}));
            case 3:
                bFf().mo5606d(new aCE(this, coA, new Object[]{acx, new Boolean(z), new Boolean(z2)}));
                break;
        }
        return m866a(acx, z, z2);
    }

    /* renamed from: b */
    public void mo620b(Actor.C0200h hVar) {
        switch (bFf().mo6893i(f171vs)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f171vs, new Object[]{hVar}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f171vs, new Object[]{hVar}));
                break;
        }
        m870a(hVar);
    }

    /* renamed from: b */
    public void mo621b(Actor cr, C5260aCm acm, Vec3f vec3f, Vec3f vec3f2) {
        switch (bFf().mo6893i(f168vm)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f168vm, new Object[]{cr, acm, vec3f, vec3f2}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f168vm, new Object[]{cr, acm, vec3f, vec3f2}));
                break;
        }
        m871a(cr, acm, vec3f, vec3f2);
    }

    @C3248pc(aYR = C3248pc.C3250b.ADDER, displayName = "Banned Factions")
    @C5566aOg
    /* renamed from: b */
    public void mo622b(Faction xm) {
        switch (bFf().mo6893i(f141Mw)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f141Mw, new Object[]{xm}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f141Mw, new Object[]{xm}));
                break;
        }
        m873a(xm);
    }

    @C5566aOg
    @C2499fr(mo18857qh = 2)
    /* renamed from: b */
    public void mo623b(Character acx, float f) {
        switch (bFf().mo6893i(coy)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, coy, new Object[]{acx, new Float(f)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, coy, new Object[]{acx, new Float(f)}));
                break;
        }
        m874a(acx, f);
    }

    @C3248pc(aYR = C3248pc.C3250b.ADDER, displayName = "Taikopedia Entries")
    @C5566aOg
    /* renamed from: b */
    public void mo624b(TaikopediaEntry aiz) {
        switch (bFf().mo6893i(aMl)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, aMl, new Object[]{aiz}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, aMl, new Object[]{aiz}));
                break;
        }
        m877a(aiz);
    }

    /* renamed from: b */
    public void mo625b(C6417amZ amz) {
        switch (bFf().mo6893i(coL)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, coL, new Object[]{amz}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, coL, new Object[]{amz}));
                break;
        }
        m878a(amz);
    }

    @C3248pc(aYR = C3248pc.C3250b.ALTERNATE_ADDER, displayName = "Agents")
    @C5566aOg
    /* renamed from: b */
    public void mo626b(AgentType goVar) {
        switch (bFf().mo6893i(cod)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, cod, new Object[]{goVar}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, cod, new Object[]{goVar}));
                break;
        }
        m879a(goVar);
    }

    @C5566aOg
    /* renamed from: b */
    public void mo627b(Hangar kRVar) {
        switch (bFf().mo6893i(coi)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, coi, new Object[]{kRVar}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, coi, new Object[]{kRVar}));
                break;
        }
        m880a(kRVar);
    }

    /* renamed from: b */
    public boolean mo628b(C0437GE ge, Player aku) {
        switch (bFf().mo6893i(coO)) {
            case 0:
                return false;
            case 2:
                return ((Boolean) bFf().mo5606d(new aCE(this, coO, new Object[]{ge, aku}))).booleanValue();
            case 3:
                bFf().mo5606d(new aCE(this, coO, new Object[]{ge, aku}));
                break;
        }
        return m885a(ge, aku);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Description")
    @C5566aOg
    /* renamed from: bv */
    public void mo629bv(I18NString i18NString) {
        switch (bFf().mo6893i(f149QF)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f149QF, new Object[]{i18NString}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f149QF, new Object[]{i18NString}));
                break;
        }
        m914bu(i18NString);
    }

    @C3248pc(aYR = C3248pc.C3250b.ADDER, displayName = "Agents")
    @C5566aOg
    /* renamed from: c */
    public void mo630c(Agent abk) {
        switch (bFf().mo6893i(coc)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, coc, new Object[]{abk}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, coc, new Object[]{abk}));
                break;
        }
        m905b(abk);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Database Category")
    @C5566aOg
    /* renamed from: c */
    public void mo631c(DatabaseCategory aik) {
        switch (bFf().mo6893i(aMo)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, aMo, new Object[]{aik}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, aMo, new Object[]{aik}));
                break;
        }
        m907b(aik);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Cloning Center")
    @C5566aOg
    /* renamed from: c */
    public void mo632c(CloningCenter koVar) {
        switch (bFf().mo6893i(cou)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, cou, new Object[]{koVar}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, cou, new Object[]{koVar}));
                break;
        }
        m909b(koVar);
    }

    /* renamed from: c */
    public C5663aRz[] mo25c() {
        return _m_fields;
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Pda MP Multiplier")
    /* renamed from: cz */
    public void mo633cz(float f) {
        switch (bFf().mo6893i(aMv)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, aMv, new Object[]{new Float(f)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, aMv, new Object[]{new Float(f)}));
                break;
        }
        m925cy(f);
    }

    /* renamed from: d */
    public C0286Dh.C0287a mo634d(Character acx) {
        switch (bFf().mo6893i(f136MH)) {
            case 0:
                return null;
            case 2:
                return (C0286Dh.C0287a) bFf().mo5606d(new aCE(this, f136MH, new Object[]{acx}));
            case 3:
                bFf().mo5606d(new aCE(this, f136MH, new Object[]{acx}));
                break;
        }
        return m915c(acx);
    }

    /* access modifiers changed from: protected */
    @C2198cg
    @C1253SX
    /* renamed from: d */
    public C0520HN mo635d(Space ea, long j) {
        switch (bFf().mo6893i(f164uY)) {
            case 0:
                return null;
            case 2:
                return (C0520HN) bFf().mo5606d(new aCE(this, f164uY, new Object[]{ea, new Long(j)}));
            case 3:
                bFf().mo5606d(new aCE(this, f164uY, new Object[]{ea, new Long(j)}));
                break;
        }
        return m916c(ea, j);
    }

    /* renamed from: d */
    public void mo636d(Actor cr, C5260aCm acm, Vec3f vec3f, Vec3f vec3f2) {
        switch (bFf().mo6893i(f169vn)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f169vn, new Object[]{cr, acm, vec3f, vec3f2}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f169vn, new Object[]{cr, acm, vec3f, vec3f2}));
                break;
        }
        m917c(cr, acm, vec3f, vec3f2);
    }

    @C3248pc(aYR = C3248pc.C3250b.REMOVER, displayName = "Banned Factions")
    @C5566aOg
    /* renamed from: d */
    public void mo637d(Faction xm) {
        switch (bFf().mo6893i(f142Mx)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f142Mx, new Object[]{xm}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f142Mx, new Object[]{xm}));
                break;
        }
        m918c(xm);
    }

    @C4034yP
    @C5566aOg
    @C2499fr
    /* renamed from: d */
    public void mo638d(Player aku, String str) {
        switch (bFf().mo6893i(bcN)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, bcN, new Object[]{aku, str}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, bcN, new Object[]{aku, str}));
                break;
        }
        m919c(aku, str);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Store")
    @C5566aOg
    /* renamed from: d */
    public void mo639d(Store hfVar) {
        switch (bFf().mo6893i(cos)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, cos, new Object[]{hfVar}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, cos, new Object[]{hfVar}));
                break;
        }
        m920c(hfVar);
    }

    @C5566aOg
    @C2499fr
    /* renamed from: d */
    public void mo640d(Hangar kRVar) {
        switch (bFf().mo6893i(coj)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, coj, new Object[]{kRVar}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, coj, new Object[]{kRVar}));
                break;
        }
        m921c(kRVar);
    }

    @C4034yP
    @C2499fr
    @C1253SX
    /* renamed from: d */
    public void mo641d(Runnable runnable) {
        switch (bFf().mo6893i(coD)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, coD, new Object[]{runnable}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, coD, new Object[]{runnable}));
                break;
        }
        m922c(runnable);
    }

    @C3248pc(aYR = C3248pc.C3250b.REMOVER, displayName = "Agents")
    @C5566aOg
    /* renamed from: e */
    public void mo643e(Agent abk) {
        switch (bFf().mo6893i(coe)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, coe, new Object[]{abk}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, coe, new Object[]{abk}));
                break;
        }
        m927d(abk);
    }

    @C3248pc(aYR = C3248pc.C3250b.REMOVER, displayName = "Taikopedia Entries")
    @C5566aOg
    /* renamed from: f */
    public void mo644f(TaikopediaEntry aiz) {
        switch (bFf().mo6893i(bcI)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, bcI, new Object[]{aiz}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, bcI, new Object[]{aiz}));
                break;
        }
        m929e(aiz);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Handle")
    public String getHandle() {
        switch (bFf().mo6893i(f155bP)) {
            case 0:
                return null;
            case 2:
                return (String) bFf().mo5606d(new aCE(this, f155bP, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f155bP, new Object[0]));
                break;
        }
        return m903ar();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Handle")
    public void setHandle(String str) {
        switch (bFf().mo6893i(f156bQ)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f156bQ, new Object[]{str}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f156bQ, new Object[]{str}));
                break;
        }
        m910b(str);
    }

    public /* bridge */ /* synthetic */ Object getType() {
        switch (bFf().mo6893i(_f_getType_0020_0028_0029Ljava_002flang_002fObject_003b)) {
            case 0:
                return null;
            case 2:
                return bFf().mo5606d(new aCE(this, _f_getType_0020_0028_0029Ljava_002flang_002fObject_003b, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, _f_getType_0020_0028_0029Ljava_002flang_002fObject_003b, new Object[0]));
                break;
        }
        return m947qW();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Agents Order")
    @C5566aOg
    /* renamed from: i */
    public void mo646i(Map<Agent, Integer> map) {
        switch (bFf().mo6893i(coh)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, coh, new Object[]{map}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, coh, new Object[]{map}));
                break;
        }
        m930h(map);
    }

    /* renamed from: iA */
    public String mo647iA() {
        switch (bFf().mo6893i(f170vo)) {
            case 0:
                return null;
            case 2:
                return (String) bFf().mo5606d(new aCE(this, f170vo, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f170vo, new Object[0]));
                break;
        }
        return m937iz();
    }

    /* renamed from: ip */
    public String mo648ip() {
        switch (bFf().mo6893i(f165vc)) {
            case 0:
                return null;
            case 2:
                return (String) bFf().mo5606d(new aCE(this, f165vc, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f165vc, new Object[0]));
                break;
        }
        return m934io();
    }

    /* renamed from: ir */
    public String mo649ir() {
        switch (bFf().mo6893i(f166ve)) {
            case 0:
                return null;
            case 2:
                return (String) bFf().mo5606d(new aCE(this, f166ve, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f166ve, new Object[0]));
                break;
        }
        return m935iq();
    }

    public boolean isAlive() {
        switch (bFf().mo6893i(coN)) {
            case 0:
                return false;
            case 2:
                return ((Boolean) bFf().mo5606d(new aCE(this, coN, new Object[0]))).booleanValue();
            case 3:
                bFf().mo5606d(new aCE(this, coN, new Object[0]));
                break;
        }
        return azS();
    }

    /* renamed from: iu */
    public String mo651iu() {
        switch (bFf().mo6893i(f167vi)) {
            case 0:
                return null;
            case 2:
                return (String) bFf().mo5606d(new aCE(this, f167vi, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f167vi, new Object[0]));
                break;
        }
        return m936it();
    }

    @C5566aOg
    /* renamed from: l */
    public void mo652l(Storage qzVar) {
        switch (bFf().mo6893i(f10386com)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f10386com, new Object[]{qzVar}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f10386com, new Object[]{qzVar}));
                break;
        }
        m939k(qzVar);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Faction")
    @C5566aOg
    /* renamed from: n */
    public void mo653n(Faction xm) {
        switch (bFf().mo6893i(cow)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, cow, new Object[]{xm}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, cow, new Object[]{xm}));
                break;
        }
        m941m(xm);
    }

    @C5566aOg
    @C2499fr(mo18857qh = 2)
    /* renamed from: o */
    public C0286Dh.C0287a mo654o(Character acx) {
        switch (bFf().mo6893i(cox)) {
            case 0:
                return null;
            case 2:
                return (C0286Dh.C0287a) bFf().mo5606d(new aCE(this, cox, new Object[]{acx}));
            case 3:
                bFf().mo5606d(new aCE(this, cox, new Object[]{acx}));
                break;
        }
        return m942n(acx);
    }

    @C5566aOg
    /* renamed from: q */
    public boolean mo655q(Character acx) {
        switch (bFf().mo6893i(coE)) {
            case 0:
                return false;
            case 2:
                return ((Boolean) bFf().mo5606d(new aCE(this, coE, new Object[]{acx}))).booleanValue();
            case 3:
                bFf().mo5606d(new aCE(this, coE, new Object[]{acx}));
                break;
        }
        return m943p(acx);
    }

    @C5566aOg
    @C2499fr
    /* renamed from: qV */
    public void mo656qV() {
        switch (bFf().mo6893i(f135Lm)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f135Lm, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f135Lm, new Object[0]));
                break;
        }
        m946qU();
    }

    @C3248pc(aYR = C3248pc.C3250b.ITERATOR, displayName = "Banned Factions")
    /* renamed from: rB */
    public List<Faction> mo657rB() {
        switch (bFf().mo6893i(f143My)) {
            case 0:
                return null;
            case 2:
                return (List) bFf().mo5606d(new aCE(this, f143My, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f143My, new Object[0]));
                break;
        }
        return m951rA();
    }

    /* renamed from: rP */
    public I18NString mo195rP() {
        switch (bFf().mo6893i(f138MN)) {
            case 0:
                return null;
            case 2:
                return (I18NString) bFf().mo5606d(new aCE(this, f138MN, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f138MN, new Object[0]));
                break;
        }
        return m952rO();
    }

    @C5566aOg
    /* renamed from: s */
    public boolean mo658s(Character acx) {
        switch (bFf().mo6893i(coH)) {
            case 0:
                return false;
            case 2:
                return ((Boolean) bFf().mo5606d(new aCE(this, coH, new Object[]{acx}))).booleanValue();
            case 3:
                bFf().mo5606d(new aCE(this, coH, new Object[]{acx}));
                break;
        }
        return m950r(acx);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Icon")
    /* renamed from: sK */
    public Asset mo659sK() {
        switch (bFf().mo6893i(f145Ob)) {
            case 0:
                return null;
            case 2:
                return (Asset) bFf().mo5606d(new aCE(this, f145Ob, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f145Ob, new Object[0]));
                break;
        }
        return m959sJ();
    }

    public String toString() {
        switch (bFf().mo6893i(_f_toString_0020_0028_0029Ljava_002flang_002fString_003b)) {
            case 0:
                return null;
            case 2:
                return (String) bFf().mo5606d(new aCE(this, _f_toString_0020_0028_0029Ljava_002flang_002fString_003b, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, _f_toString_0020_0028_0029Ljava_002flang_002fString_003b, new Object[0]));
                break;
        }
        return m904au();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Icon")
    @C5566aOg
    /* renamed from: v */
    public void mo661v(Asset tCVar) {
        switch (bFf().mo6893i(f146Oc)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f146Oc, new Object[]{tCVar}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f146Oc, new Object[]{tCVar}));
                break;
        }
        m961u(tCVar);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Description")
    /* renamed from: vW */
    public I18NString mo662vW() {
        switch (bFf().mo6893i(f148QE)) {
            case 0:
                return null;
            case 2:
                return (I18NString) bFf().mo5606d(new aCE(this, f148QE, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f148QE, new Object[0]));
                break;
        }
        return m963vV();
    }

    @C0064Am(aul = "9871476ad57e9ff631f15d7dab2271bd", aum = 0)
    /* renamed from: ap */
    private UUID m900ap() {
        return m897an();
    }

    @C0064Am(aul = "56f561fffa1f5bc5a6d7ccd8c23ace45", aum = 0)
    /* renamed from: b */
    private void m911b(UUID uuid) {
        m883a(uuid);
    }

    /* access modifiers changed from: protected */
    /* renamed from: S */
    public void mo10S() {
        super.mo10S();
        m883a(UUID.randomUUID());
    }

    /* renamed from: e */
    public void mo642e(StationType mx) {
        super.mo967a((C2961mJ) mx);
        setStatic(true);
        setHandle(String.valueOf(azP().getHandle()) + "_" + cWm());
        m883a(UUID.randomUUID());
    }

    @C0064Am(aul = "8aa057ccfe057a21464330f6810ca0f4", aum = 0)
    /* renamed from: au */
    private String m904au() {
        return "Station: [" + getName() + "]";
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Handle")
    @C0064Am(aul = "f0e8f4e77027f5d485ab1089e1cc8e15", aum = 0)
    /* renamed from: ar */
    private String m903ar() {
        return m899ao();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Handle")
    @C0064Am(aul = "76bc37157cf7e0d422ab2f01ebf71e71", aum = 0)
    /* renamed from: b */
    private void m910b(String str) {
        m882a(str);
    }

    @aFW("Agents")
    @C3248pc(aYR = C3248pc.C3250b.ITERATOR, displayName = "Agents")
    @C0064Am(aul = "5dc93c8544e2a397155990fbf573497f", aum = 0)
    private List<Agent> azy() {
        return Collections.unmodifiableList(new ArrayList(azw().keySet()));
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Agents Order")
    @C0064Am(aul = "338b0a7671cf1380589ab0d3880dec04", aum = 0)
    private Map<Agent, Integer> azA() {
        HashMap hashMap = new HashMap();
        hashMap.putAll(azw());
        return Collections.unmodifiableMap(hashMap);
    }

    @C3248pc(aYR = C3248pc.C3250b.ITERATOR, displayName = "Banned Factions")
    @C0064Am(aul = "0e71cbf09f9d6e5f3fbafe5bf37b9472", aum = 0)
    /* renamed from: rA */
    private List<Faction> m951rA() {
        return Collections.unmodifiableList(m956ro());
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Description")
    @C0064Am(aul = "1b79af5af349149782184a42c2d12429", aum = 0)
    /* renamed from: vV */
    private I18NString m963vV() {
        return m962vT();
    }

    @C0064Am(aul = "19eedcfcad7dae52bcb3b1a2dd555c2c", aum = 0)
    /* renamed from: RT */
    private I18NString m857RT() {
        return mo662vW();
    }

    @C0064Am(aul = "336de3996bcddbf0564da5827c8d821c", aum = 0)
    private Set<Hangar> azC() {
        return azr();
    }

    @C0064Am(aul = "224db252d0d473e78e4b0e75aa080a59", aum = 0)
    private Set<Storage> azE() {
        return azs();
    }

    @C0064Am(aul = "a3a01bb39ed44edee8c7f989b5800488", aum = 0)
    private Set<Player> azG() {
        return azt();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Store")
    @C0064Am(aul = "57f1241ec9aed55fa7a86df6dfc48c86", aum = 0)
    private Store azI() {
        return azo();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Cloning Center")
    @C0064Am(aul = "463089bc659945012514529af8a72dc7", aum = 0)
    private CloningCenter azK() {
        return azp();
    }

    @aFW("Faction")
    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Faction")
    @C0064Am(aul = "3fab1ee031f9ac25af29d744b0f3ca86", aum = 0)
    private Faction azM() {
        return azq();
    }

    @C3248pc(aYR = C3248pc.C3250b.ITERATOR, displayName = "Taikopedia Entries")
    @C0064Am(aul = "7ea3c77ef3ed8a196d7bb23f0f009c5a", aum = 0)
    /* renamed from: RV */
    private List<TaikopediaEntry> m858RV() {
        return Collections.unmodifiableList(m854Ll());
    }

    @C0064Am(aul = "2c83c95c6683c1af892187f6360e3f07", aum = 0)
    /* renamed from: c */
    private C0286Dh.C0287a m915c(Character acx) {
        try {
            return mo654o(acx);
        } catch (C6046afS e) {
            throw new C5475aKt();
        }
    }

    @C0401Fa
    @C0064Am(aul = "51813e2f1a7de33f15ac96565187eaec", aum = 0)
    /* renamed from: aj */
    private void m893aj(Player aku) {
        try {
            aDR bFs = aku.bOx().cSV().bFs();
            if (bFs != null) {
                mo8366w(bFs);
                try {
                    aku.dxc().cOt();
                } finally {
                    cVm();
                }
            }
        } catch (NullPointerException e) {
        }
    }

    @C4034yP
    @C0064Am(aul = "701eb71e7fb201062466195db998bdd6", aum = 0)
    @C2499fr
    @C1253SX
    /* renamed from: c */
    private void m922c(Runnable runnable) {
        runnable.run();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Type")
    @C0064Am(aul = "480f07b5de942c644caff409d757097b", aum = 0)
    private StationType azO() {
        return (StationType) super.getType();
    }

    @C0064Am(aul = "d5977cab3a48bb001ced5fefb8814aba", aum = 0)
    /* renamed from: io */
    private String m934io() {
        return m953rh().getHandle();
    }

    @C0064Am(aul = "9f595fd58fbc1bbe3b1857bf87ca4ea9", aum = 0)
    /* renamed from: iq */
    private String m935iq() {
        return m953rh().getFile();
    }

    @C0064Am(aul = "be2588e84afcb0ac8f50a28bfea76c56", aum = 0)
    /* renamed from: c */
    private void m917c(Actor cr, C5260aCm acm, Vec3f vec3f, Vec3f vec3f2) {
    }

    @C0064Am(aul = "dddb09954e683e87709bfafadb70f08a", aum = 0)
    /* renamed from: iz */
    private String m937iz() {
        return ala().aIW().avC();
    }

    @C0064Am(aul = "6903fb91667faabc2ac01282aa5652c0", aum = 0)
    /* renamed from: a */
    private void m871a(Actor cr, C5260aCm acm, Vec3f vec3f, Vec3f vec3f2) {
    }

    @C0064Am(aul = "35c40bae7cb0cbeb7b44356bd57b4cbc", aum = 0)
    /* renamed from: it */
    private String m936it() {
        return m933ij();
    }

    @C0064Am(aul = "a2ab6f9d740741baa1598246f5c45652", aum = 0)
    /* renamed from: a */
    private void m870a(Actor.C0200h hVar) {
        if (m953rh() == null) {
            throw new IllegalStateException("ShapedObject of class '" + getClass().getName() + "' does not have a RenderAsset");
        }
        String file = m953rh().getFile();
        if (!bGX() || !(ald().getLoaderTrail() instanceof LoaderTrail)) {
            hVar.mo1103c(C2606hU.m32703b(file, true));
        } else {
            ((LoaderTrail) ald().getLoaderTrail()).mo19098a(file, new C0097a(hVar), getName(), true);
        }
    }

    @C0064Am(aul = "cc591e776b3b1fad62a6f6a333fa6741", aum = 0)
    /* renamed from: aG */
    private void m887aG() {
        super.mo70aH();
        if (m899ao() == null) {
            setHandle(String.valueOf(azP().getHandle()) + "_" + cWm());
        }
    }

    @C0064Am(aul = "bd4fa755f77554e3d219d3707c876bc1", aum = 0)
    /* renamed from: a */
    private void m878a(C6417amZ amz) {
        if (amz.ape() && amz.aph() != null) {
            m928e(amz.aph());
        }
        if (amz.ape() && amz.ckr() != null) {
            for (C0665JT next : amz.ckr()) {
                if (((C6417amZ) next).apf()) {
                    m928e(next);
                }
            }
        }
        if (amz.apf()) {
            mo8358lY("Station not present on HEAD but present on LIVE: " + getName() + " " + bFY());
        }
    }

    @C0064Am(aul = "7a053f4dfeb56b265178a6872fb1346f", aum = 0)
    /* renamed from: d */
    private void m926d(C0665JT jt) {
        List<C0665JT> list = (List) jt.get("players");
        if (list != null) {
            HashSet hashSet = new HashSet(azt());
            for (C0665JT baw : list) {
                Player aku = (Player) baw.baw();
                if (!hashSet.contains(aku)) {
                    azt().add(aku);
                    hashSet.add(aku);
                }
            }
        }
        List<C0665JT> list2 = (List) jt.get("storages");
        if (list2 != null) {
            HashMap hashMap = new HashMap();
            for (Storage qzVar : azs()) {
                hashMap.put(qzVar.ajj(), qzVar);
            }
            for (C0665JT baw2 : list2) {
                Storage qzVar2 = (Storage) baw2.baw();
                hashMap.put(qzVar2.ajj(), qzVar2);
            }
            azs().clear();
            azs().addAll(hashMap.values());
        }
        List<C0665JT> list3 = (List) jt.get("localHangars");
        if (list3 != null) {
            HashMap hashMap2 = new HashMap();
            for (Hangar kRVar : azr()) {
                hashMap2.put(kRVar.mo20058KY(), kRVar);
            }
            for (C0665JT baw3 : list3) {
                Hangar kRVar2 = (Hangar) baw3.baw();
                hashMap2.put(kRVar2.mo20058KY(), kRVar2);
            }
            azr().clear();
            azr().addAll(hashMap2.values());
        }
    }

    @C0064Am(aul = "0ad9970ecd16048f969920cc4847c670", aum = 0)
    private boolean azS() {
        return true;
    }

    @C0064Am(aul = "edd47268d6478cb9e78185d586ba2776", aum = 0)
    /* renamed from: a */
    private boolean m885a(C0437GE ge, Player aku) {
        if (ge != C0437GE.CLONING) {
            if (ge == C0437GE.CORP_STORAGE) {
            }
            return false;
        } else if (azL() != null) {
            return true;
        } else {
            return false;
        }
    }

    @C0064Am(aul = "302280b6493cec26657cb77549a192d5", aum = 0)
    /* renamed from: X */
    private boolean m864X(float f) {
        return f <= PlayerController.m39266c((C0286Dh) this);
    }

    @C0064Am(aul = "cf642ca34a6195cbbdcdd92caee4038d", aum = 0)
    @C2198cg
    @C1253SX
    /* renamed from: c */
    private C0520HN m916c(Space ea, long j) {
        C6543aov aov = new C6543aov(ea, this, mo958IL());
        aov.mo2449a(ea.cKn().mo5725a(j, (C2235dB) aov, 13));
        return aov;
    }

    @C0064Am(aul = "559add23d3ef9638068126959f593760", aum = 0)
    /* renamed from: rO */
    private I18NString m952rO() {
        return new I18NString(getName());
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Database Category")
    @C0064Am(aul = "937de7770eac0b16fa7065099310db79", aum = 0)
    /* renamed from: RX */
    private DatabaseCategory m859RX() {
        return m855RR();
    }

    @C0064Am(aul = "303de65302ab95c58f1f2fd1288d529a", aum = 0)
    /* renamed from: RZ */
    private String m860RZ() {
        if (m958sG() != null) {
            return m958sG().getHandle();
        }
        return null;
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Icon")
    @C0064Am(aul = "e27b794201662bf6fd34c8f8ca52dfc5", aum = 0)
    /* renamed from: sJ */
    private Asset m959sJ() {
        return m958sG();
    }

    @C0064Am(aul = "6d9a3e1424cd3844468acb2a34685c88", aum = 0)
    /* renamed from: Sd */
    private Asset m861Sd() {
        return m953rh();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Pda MP Multiplier")
    @C0064Am(aul = "3ea93361be0944725ff08a22ae4c815b", aum = 0)
    /* renamed from: Sf */
    private float m862Sf() {
        return m856RS();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Pda MP Multiplier")
    @C0064Am(aul = "11408f3a02ef9112fb7767c1a59787b4", aum = 0)
    /* renamed from: cy */
    private void m925cy(float f) {
        m924cx(f);
    }

    @aFW("Node")
    @C0064Am(aul = "63c02af5bfc9fd183c43b53f561e5612", aum = 0)
    private Node azV() {
        return super.azW();
    }

    @C6485anp
    @C5511aMd
    /* renamed from: a.BF$c */
    /* compiled from: a */
    public class UndockTasklet extends TaskletImpl implements C1616Xf, C3161oY.C3162a {

        /* renamed from: JD */
        public static final C2491fm f172JD = null;
        /* renamed from: _f_onObjectDisposed_0020_0028Ltaikodom_002finfra_002fscript_002fScriptObject_003b_0029V */
        public static final C2491fm f173x13860637 = null;
        public static final int _m_fieldCount = 0;
        public static final C5663aRz[] _m_fields = null;
        public static final int _m_methodCount = 0;
        public static final C2491fm[] _m_methods = null;
        /* renamed from: aT */
        public static final C5663aRz f174aT = null;
        public static final C2491fm coy = null;
        public static final C5663aRz iEo = null;
        public static final C2491fm iEp = null;
        /* renamed from: kX */
        public static final C5663aRz f176kX = null;
        private static final long serialVersionUID = 0;
        public static C6494any ___iScriptClass;
        @C0064Am(aul = "986481f19784dc991844a38c8ccfa00b", aum = 2)
        static /* synthetic */ Station bCF;
        @C0064Am(aul = "2df6acb7d74594aa8b1c44637b1ae768", aum = 1)
        private static C1556Wo<Character, Long> bCE;
        @C0064Am(aul = "637d4448e8e9e93e54464d2d572147e2", aum = 0)

        /* renamed from: iH */
        private static Station f175iH;

        static {
            m1031V();
        }

        public UndockTasklet() {
            super((C5540aNg) null);
            super.mo10S();
        }

        public UndockTasklet(Station bf, aDJ adj) {
            super((C5540aNg) null);
            super._m_script_init(bf, adj);
        }

        public UndockTasklet(C5540aNg ang) {
            super(ang);
        }

        /* renamed from: V */
        static void m1031V() {
            _m_fieldCount = TaskletImpl._m_fieldCount + 3;
            _m_methodCount = TaskletImpl._m_methodCount + 4;
            int i = TaskletImpl._m_fieldCount;
            C5663aRz[] arzArr = new C5663aRz[(i + 3)];
            C5663aRz b = C5640aRc.m17844b(UndockTasklet.class, "637d4448e8e9e93e54464d2d572147e2", i);
            f176kX = b;
            arzArr[i] = b;
            int i2 = i + 1;
            C5663aRz b2 = C5640aRc.m17844b(UndockTasklet.class, "2df6acb7d74594aa8b1c44637b1ae768", i2);
            iEo = b2;
            arzArr[i2] = b2;
            int i3 = i2 + 1;
            C5663aRz b3 = C5640aRc.m17844b(UndockTasklet.class, "986481f19784dc991844a38c8ccfa00b", i3);
            f174aT = b3;
            arzArr[i3] = b3;
            int i4 = i3 + 1;
            _m_fields = arzArr;
            C1634Xv.m11725a((Object[]) TaskletImpl._m_fields, (Object[]) _m_fields);
            int i5 = TaskletImpl._m_methodCount;
            C2491fm[] fmVarArr = new C2491fm[(i5 + 4)];
            C2491fm a = C4105zY.m41624a(UndockTasklet.class, "8b18be3ad916ef5f5264e96f3da7057b", i5);
            iEp = a;
            fmVarArr[i5] = a;
            int i6 = i5 + 1;
            C2491fm a2 = C4105zY.m41624a(UndockTasklet.class, "319a0c83146cdf3ea1df2054e86a79ff", i6);
            coy = a2;
            fmVarArr[i6] = a2;
            int i7 = i6 + 1;
            C2491fm a3 = C4105zY.m41624a(UndockTasklet.class, "0a0b678234d34a5c2979d61247b69acd", i7);
            f172JD = a3;
            fmVarArr[i7] = a3;
            int i8 = i7 + 1;
            C2491fm a4 = C4105zY.m41624a(UndockTasklet.class, "37666a40826467108f6cc2103b158b13", i8);
            f173x13860637 = a4;
            fmVarArr[i8] = a4;
            int i9 = i8 + 1;
            _m_methods = fmVarArr;
            C1634Xv.m11725a((Object[]) TaskletImpl._m_methods, (Object[]) _m_methods);
            ___iScriptClass = aUO.m18566a(UndockTasklet.class, C3916wk.class, _m_fields, _m_methods);
        }

        /* renamed from: a */
        private void m1032a(Station bf) {
            bFf().mo5608dq().mo3197f(f176kX, bf);
        }

        /* renamed from: aB */
        private void m1034aB(Station bf) {
            bFf().mo5608dq().mo3197f(f174aT, bf);
        }

        /* renamed from: ar */
        private void m1035ar(C1556Wo wo) {
            bFf().mo5608dq().mo3197f(iEo, wo);
        }

        private C1556Wo dpB() {
            return (C1556Wo) bFf().mo5608dq().mo3214p(iEo);
        }

        private Station dpC() {
            return (Station) bFf().mo5608dq().mo3214p(f174aT);
        }

        private void dpE() {
            switch (bFf().mo6893i(iEp)) {
                case 0:
                    return;
                case 2:
                    bFf().mo5606d(new aCE(this, iEp, new Object[0]));
                    return;
                case 3:
                    bFf().mo5606d(new aCE(this, iEp, new Object[0]));
                    break;
            }
            dpD();
        }

        /* renamed from: eF */
        private Station m1037eF() {
            return (Station) bFf().mo5608dq().mo3214p(f176kX);
        }

        /* renamed from: S */
        public void mo10S() {
            super.mo10S();
        }

        /* renamed from: T */
        public C6494any mo11T() {
            return ___iScriptClass;
        }

        /* renamed from: U */
        public C2491fm[] mo12U() {
            return _m_methods;
        }

        /* renamed from: W */
        public Object mo13W() {
            return new C3916wk(this);
        }

        /* renamed from: a */
        public Object mo14a(C0495Gr gr) {
            Object[] args = gr.getArgs();
            switch (gr.mo2417hq() - TaskletImpl._m_methodCount) {
                case 0:
                    dpD();
                    return null;
                case 1:
                    m1033a((Character) args[0], ((Float) args[1]).floatValue());
                    return null;
                case 2:
                    m1038pi();
                    return null;
                case 3:
                    m1036b((aDJ) args[0]);
                    return null;
                default:
                    return super.mo14a(gr);
            }
        }

        /* renamed from: a */
        public void mo15a(Collection collection, C0495Gr gr) {
            super.mo15a(collection, gr);
        }

        /* renamed from: b */
        public void mo666b(Character acx, float f) {
            switch (bFf().mo6893i(coy)) {
                case 0:
                    return;
                case 2:
                    bFf().mo5606d(new aCE(this, coy, new Object[]{acx, new Float(f)}));
                    return;
                case 3:
                    bFf().mo5606d(new aCE(this, coy, new Object[]{acx, new Float(f)}));
                    break;
            }
            m1033a(acx, f);
        }

        /* renamed from: c */
        public void mo98c(aDJ adj) {
            switch (bFf().mo6893i(f173x13860637)) {
                case 0:
                    return;
                case 2:
                    bFf().mo5606d(new aCE(this, f173x13860637, new Object[]{adj}));
                    return;
                case 3:
                    bFf().mo5606d(new aCE(this, f173x13860637, new Object[]{adj}));
                    break;
            }
            m1036b(adj);
        }

        /* renamed from: c */
        public C5663aRz[] mo25c() {
            return _m_fields;
        }

        /* access modifiers changed from: protected */
        /* renamed from: pj */
        public void mo667pj() {
            switch (bFf().mo6893i(f172JD)) {
                case 0:
                    return;
                case 2:
                    bFf().mo5606d(new aCE(this, f172JD, new Object[0]));
                    return;
                case 3:
                    bFf().mo5606d(new aCE(this, f172JD, new Object[0]));
                    break;
            }
            m1038pi();
        }

        /* renamed from: a */
        public void mo665a(Station bf, aDJ adj) {
            m1034aB(bf);
            super.mo4706l(adj);
            if (adj instanceof Station) {
                m1032a((Station) adj);
            }
        }

        @C0064Am(aul = "8b18be3ad916ef5f5264e96f3da7057b", aum = 0)
        private void dpD() {
            stop();
            long currentTimeMillis = currentTimeMillis();
            Iterator it = dpB().entrySet().iterator();
            long j = Long.MAX_VALUE;
            while (it.hasNext()) {
                Map.Entry entry = (Map.Entry) it.next();
                if (((Long) entry.getValue()).longValue() <= currentTimeMillis) {
                    m1037eF().mo658s((Character) entry.getKey());
                    it.remove();
                } else if (j > ((Long) entry.getValue()).longValue()) {
                    j = ((Long) entry.getValue()).longValue();
                }
            }
            if (j < Long.MAX_VALUE) {
                mo4704hk(((float) (j - currentTimeMillis)) / 1000.0f);
            }
        }

        @C0064Am(aul = "319a0c83146cdf3ea1df2054e86a79ff", aum = 0)
        /* renamed from: a */
        private void m1033a(Character acx, float f) {
            dpB().put(acx, Long.valueOf(currentTimeMillis() + ((long) (1000.0f * f))));
            dpE();
        }

        @C0064Am(aul = "0a0b678234d34a5c2979d61247b69acd", aum = 0)
        /* renamed from: pi */
        private void m1038pi() {
            dpE();
        }

        @C0064Am(aul = "37666a40826467108f6cc2103b158b13", aum = 0)
        /* renamed from: b */
        private void m1036b(aDJ adj) {
            Iterator it = dpB().entrySet().iterator();
            boolean z = false;
            while (it.hasNext()) {
                if (((Map.Entry) it.next()).getKey() == adj) {
                    it.remove();
                    z = true;
                }
            }
            if (z) {
                dpE();
            }
        }
    }

    /* renamed from: a.BF$b */
    /* compiled from: a */
    class C0098b implements C6681ard {

        private final /* synthetic */ Ship eQW;
        private final /* synthetic */ boolean hIA;
        private final /* synthetic */ boolean hIB;
        private final /* synthetic */ Character hIz;

        C0098b(Ship fAVar, Character acx, boolean z, boolean z2) {
            this.eQW = fAVar;
            this.hIz = acx;
            this.hIA = z;
            this.hIB = z2;
        }

        public void run() {
            if (this.eQW.mo1013bx((Actor) Station.this) < PlayerController.m39266c((C0286Dh) Station.this)) {
                try {
                    Station.this.m906b(this.hIz, this.hIA, this.hIB, this.eQW);
                } catch (C6046afS e) {
                    Station.this.mo8354g("Error docking", (Throwable) e);
                }
            }
        }
    }

    /* renamed from: a.BF$a */
    class C0097a implements C2601hQ {
        private final /* synthetic */ Actor.C0200h eyn;

        C0097a(Actor.C0200h hVar) {
            this.eyn = hVar;
        }

        /* renamed from: b */
        public void mo663b(C4029yK yKVar) {
            this.eyn.mo1103c(yKVar);
        }
    }
}
