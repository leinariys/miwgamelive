package game.script.ship;

import game.network.message.externalizable.aCE;
import game.script.Character;
import game.script.item.Item;
import game.script.item.ItemLocation;
import game.script.item.RawMaterial;
import game.script.nls.NLSItem;
import game.script.nls.NLSManager;
import game.script.player.Player;
import game.script.space.AsteroidLootType;
import game.script.space.LootItems;
import game.script.space.LootType;
import game.script.util.AdapterLikedList;
import game.script.util.GameObjectAdapter;
import logic.baa.*;
import logic.data.mbean.C3595si;
import logic.data.mbean.C6703arz;
import logic.res.code.C5640aRc;
import logic.res.code.C5663aRz;
import logic.res.code.C6494any;
import logic.res.html.C2491fm;
import p001a.*;

import java.util.Collection;

@C5829abJ("2.0.0")
@C6485anp
@C2712iu(mo19786Bt = BaseCargoHoldType.class, version = "2.0.0")
@C5511aMd
/* renamed from: a.wI */
/* compiled from: a */
public class CargoHold extends ItemLocation implements C6480ank<CargoHoldAdapter>, C6872avM, C6872avM {

    /* renamed from: Lm */
    public static final C2491fm f9473Lm = null;

    /* renamed from: Pv */
    public static final C5663aRz f9474Pv = null;

    /* renamed from: RA */
    public static final C2491fm f9475RA = null;

    /* renamed from: RD */
    public static final C2491fm f9476RD = null;

    /* renamed from: Rz */
    public static final C2491fm f9477Rz = null;
    /* renamed from: _f_getShip_0020_0028_0029Ltaikodom_002fgame_002fscript_002fship_002fShip_003b */
    public static final C2491fm f9478x851d656b = null;
    public static final C2491fm _f_onResurrect_0020_0028_0029V = null;
    public static final int _m_fieldCount = 0;
    public static final C5663aRz[] _m_fields = null;
    public static final int _m_methodCount = 0;
    public static final C2491fm[] _m_methods = null;
    public static final C5663aRz aOP = null;
    public static final C5663aRz aOx = null;
    public static final C2491fm aPe = null;
    public static final C2491fm aPf = null;
    public static final C2491fm aPi = null;
    public static final C2491fm aPj = null;
    public static final C2491fm aPk = null;
    public static final C2491fm aPl = null;
    public static final C2491fm aPm = null;
    public static final C5663aRz bve = null;
    public static final C2491fm bvf = null;
    public static final C2491fm bvi = null;
    public static final C2491fm emP = null;
    public static final C2491fm emQ = null;
    /* renamed from: ku */
    public static final C2491fm f9479ku = null;
    public static final long serialVersionUID = 0;
    public static C6494any ___iScriptClass;
    @C0064Am(aul = "8aa4c2db7fd5a6b3a69e963e0e779b4d", aum = 1)
    private static Ship aOO;
    @C0064Am(aul = "5e94d92cb28f70ab377e77e7ce7415cc", aum = 3)
    @C5566aOg
    private static AdapterLikedList<CargoHoldAdapter> aOw;
    @C0064Am(aul = "5c79dd35bb4b25df84afde6440d4f0b1", aum = 2)
    private static float aUC;
    @C0064Am(aul = "c8979053f765f77fd33d2ec5865690c4", aum = 0)
    private static float cvd;

    static {
        m40581V();
    }

    public CargoHold() {
        super((C5540aNg) null);
        super.mo10S();
    }

    public CargoHold(C5540aNg ang) {
        super(ang);
    }

    public CargoHold(CargoHoldType arb) {
        super((C5540aNg) null);
        super._m_script_init(arb);
    }

    /* renamed from: V */
    static void m40581V() {
        serialVersionUID = (long) 1;
        _m_fieldCount = ItemLocation._m_fieldCount + 4;
        _m_methodCount = ItemLocation._m_methodCount + 18;
        int i = ItemLocation._m_fieldCount;
        C5663aRz[] arzArr = new C5663aRz[(i + 4)];
        C5663aRz b = C5640aRc.m17844b(CargoHold.class, "c8979053f765f77fd33d2ec5865690c4", i);
        f9474Pv = b;
        arzArr[i] = b;
        int i2 = i + 1;
        C5663aRz b2 = C5640aRc.m17844b(CargoHold.class, "8aa4c2db7fd5a6b3a69e963e0e779b4d", i2);
        aOP = b2;
        arzArr[i2] = b2;
        int i3 = i2 + 1;
        C5663aRz b3 = C5640aRc.m17844b(CargoHold.class, "5c79dd35bb4b25df84afde6440d4f0b1", i3);
        bve = b3;
        arzArr[i3] = b3;
        int i4 = i3 + 1;
        C5663aRz b4 = C5640aRc.m17844b(CargoHold.class, "5e94d92cb28f70ab377e77e7ce7415cc", i4);
        aOx = b4;
        arzArr[i4] = b4;
        int i5 = i4 + 1;
        _m_fields = arzArr;
        C1634Xv.m11725a((Object[]) ItemLocation._m_fields, (Object[]) _m_fields);
        int i6 = ItemLocation._m_methodCount;
        C2491fm[] fmVarArr = new C2491fm[(i6 + 18)];
        C2491fm a = C4105zY.m41624a(CargoHold.class, "7bbea6afb002e34b28547c8591d93a75", i6);
        f9477Rz = a;
        fmVarArr[i6] = a;
        int i7 = i6 + 1;
        C2491fm a2 = C4105zY.m41624a(CargoHold.class, "2259c432b6010bb2165abb94adc99f4e", i7);
        emP = a2;
        fmVarArr[i7] = a2;
        int i8 = i7 + 1;
        C2491fm a3 = C4105zY.m41624a(CargoHold.class, "d902ac3343fdfa349d8411ab182d1de4", i8);
        f9478x851d656b = a3;
        fmVarArr[i8] = a3;
        int i9 = i8 + 1;
        C2491fm a4 = C4105zY.m41624a(CargoHold.class, "83b6ef78abd631d9331a9bab68c0ec3f", i9);
        aPi = a4;
        fmVarArr[i9] = a4;
        int i10 = i9 + 1;
        C2491fm a5 = C4105zY.m41624a(CargoHold.class, "984b0757232c438658f9270bf090d2cc", i10);
        bvi = a5;
        fmVarArr[i10] = a5;
        int i11 = i10 + 1;
        C2491fm a6 = C4105zY.m41624a(CargoHold.class, "653bd84e047c28ec67bc71965a2153cc", i11);
        bvf = a6;
        fmVarArr[i11] = a6;
        int i12 = i11 + 1;
        C2491fm a7 = C4105zY.m41624a(CargoHold.class, "01a90f5d54f9a9d199ea9e34d93e2d83", i12);
        aPe = a7;
        fmVarArr[i12] = a7;
        int i13 = i12 + 1;
        C2491fm a8 = C4105zY.m41624a(CargoHold.class, "722b403a045b10b3534ca26651ba3e55", i13);
        aPf = a8;
        fmVarArr[i13] = a8;
        int i14 = i13 + 1;
        C2491fm a9 = C4105zY.m41624a(CargoHold.class, "e75481f3a67c9ab57c08246fa9532553", i14);
        _f_onResurrect_0020_0028_0029V = a9;
        fmVarArr[i14] = a9;
        int i15 = i14 + 1;
        C2491fm a10 = C4105zY.m41624a(CargoHold.class, "6a21799379eaad32f212016de0d2470f", i15);
        emQ = a10;
        fmVarArr[i15] = a10;
        int i16 = i15 + 1;
        C2491fm a11 = C4105zY.m41624a(CargoHold.class, "bb07e7a3a7a9b5a8540cfcb73e3e73e6", i16);
        f9473Lm = a11;
        fmVarArr[i16] = a11;
        int i17 = i16 + 1;
        C2491fm a12 = C4105zY.m41624a(CargoHold.class, "16791cca35cc3df6415484afcc0459ed", i17);
        f9475RA = a12;
        fmVarArr[i17] = a12;
        int i18 = i17 + 1;
        C2491fm a13 = C4105zY.m41624a(CargoHold.class, "31aa580bf37c785b75866c5fa35fad44", i18);
        f9476RD = a13;
        fmVarArr[i18] = a13;
        int i19 = i18 + 1;
        C2491fm a14 = C4105zY.m41624a(CargoHold.class, "ff416dd408d13f18336a9bcd5bd7bb81", i19);
        f9479ku = a14;
        fmVarArr[i19] = a14;
        int i20 = i19 + 1;
        C2491fm a15 = C4105zY.m41624a(CargoHold.class, "429497e00f614e88804334b116937c47", i20);
        aPl = a15;
        fmVarArr[i20] = a15;
        int i21 = i20 + 1;
        C2491fm a16 = C4105zY.m41624a(CargoHold.class, "327b7acf8fb00f342ce3ab759daf069a", i21);
        aPj = a16;
        fmVarArr[i21] = a16;
        int i22 = i21 + 1;
        C2491fm a17 = C4105zY.m41624a(CargoHold.class, "af959854aaa9ea3577cd61d4d274c653", i22);
        aPk = a17;
        fmVarArr[i22] = a17;
        int i23 = i22 + 1;
        C2491fm a18 = C4105zY.m41624a(CargoHold.class, "8931489eccc736a958a24afd6ac7dc3b", i23);
        aPm = a18;
        fmVarArr[i23] = a18;
        int i24 = i23 + 1;
        _m_methods = fmVarArr;
        C1634Xv.m11725a((Object[]) ItemLocation._m_methods, (Object[]) _m_methods);
        ___iScriptClass = aUO.m18566a(CargoHold.class, C6703arz.class, _m_fields, _m_methods);
    }

    /* renamed from: E */
    private boolean m40569E(Item auq) {
        switch (bFf().mo6893i(emP)) {
            case 0:
                return false;
            case 2:
                return ((Boolean) bFf().mo5606d(new aCE(this, emP, new Object[]{auq}))).booleanValue();
            case 3:
                bFf().mo5606d(new aCE(this, emP, new Object[]{auq}));
                break;
        }
        return m40568D(auq);
    }

    @C0064Am(aul = "01a90f5d54f9a9d199ea9e34d93e2d83", aum = 0)
    @C5566aOg
    /* renamed from: TG */
    private void m40571TG() {
        throw new aWi(new aCE(this, aPe, new Object[0]));
    }

    @C5566aOg
    /* renamed from: TH */
    private void m40572TH() {
        switch (bFf().mo6893i(aPe)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, aPe, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, aPe, new Object[0]));
                break;
        }
        m40571TG();
    }

    @C0064Am(aul = "327b7acf8fb00f342ce3ab759daf069a", aum = 0)
    @C5566aOg
    /* renamed from: TK */
    private void m40574TK() {
        throw new aWi(new aCE(this, aPj, new Object[0]));
    }

    @C0064Am(aul = "af959854aaa9ea3577cd61d4d274c653", aum = 0)
    @C5566aOg
    /* renamed from: TM */
    private void m40575TM() {
        throw new aWi(new aCE(this, aPk, new Object[0]));
    }

    /* renamed from: TP */
    private void m40577TP() {
        switch (bFf().mo6893i(aPl)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, aPl, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, aPl, new Object[0]));
                break;
        }
        m40576TO();
    }

    /* renamed from: Te */
    private AdapterLikedList m40579Te() {
        return (AdapterLikedList) bFf().mo5608dq().mo3214p(aOx);
    }

    /* renamed from: Tn */
    private Ship m40580Tn() {
        return (Ship) bFf().mo5608dq().mo3214p(aOP);
    }

    /* renamed from: a */
    private void m40583a(AdapterLikedList zAVar) {
        bFf().mo5608dq().mo3197f(aOx, zAVar);
    }

    private float aHj() {
        return ((CargoHoldType) getType()).mo11172wE();
    }

    private float ajf() {
        return bFf().mo5608dq().mo3211m(bve);
    }

    @C0064Am(aul = "984b0757232c438658f9270bf090d2cc", aum = 0)
    @C5566aOg
    @C2499fr
    private void ajk() {
        throw new aWi(new aCE(this, bvi, new Object[0]));
    }

    /* renamed from: dN */
    private void m40585dN(float f) {
        bFf().mo5608dq().mo3150a(bve, f);
    }

    /* renamed from: fc */
    private void m40586fc(float f) {
        throw new C6039afL();
    }

    /* renamed from: k */
    private void m40588k(Ship fAVar) {
        bFf().mo5608dq().mo3197f(aOP, fAVar);
    }

    @C0064Am(aul = "bb07e7a3a7a9b5a8540cfcb73e3e73e6", aum = 0)
    @C5566aOg
    @C2499fr
    /* renamed from: qU */
    private void m40591qU() {
        throw new aWi(new aCE(this, f9473Lm, new Object[0]));
    }

    /* renamed from: S */
    public void mo10S() {
        super.mo10S();
    }

    /* renamed from: T */
    public C6494any mo11T() {
        return ___iScriptClass;
    }

    /* renamed from: TJ */
    public AdapterLikedList<CargoHoldAdapter> mo5353TJ() {
        switch (bFf().mo6893i(aPf)) {
            case 0:
                return null;
            case 2:
                return (AdapterLikedList) bFf().mo5606d(new aCE(this, aPf, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, aPf, new Object[0]));
                break;
        }
        return m40573TI();
    }

    @C5566aOg
    /* renamed from: TL */
    public void mo12905TL() {
        switch (bFf().mo6893i(aPj)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, aPj, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, aPj, new Object[0]));
                break;
        }
        m40574TK();
    }

    @C5566aOg
    /* renamed from: TN */
    public void mo12906TN() {
        switch (bFf().mo6893i(aPk)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, aPk, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, aPk, new Object[0]));
                break;
        }
        m40575TM();
    }

    /* renamed from: TR */
    public void mo22730TR() {
        switch (bFf().mo6893i(aPm)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, aPm, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, aPm, new Object[0]));
                break;
        }
        m40578TQ();
    }

    /* renamed from: U */
    public C2491fm[] mo12U() {
        return _m_methods;
    }

    /* renamed from: W */
    public Object mo13W() {
        return new C6703arz(this);
    }

    /* renamed from: a */
    public Object mo14a(C0495Gr gr) {
        Object[] args = gr.getArgs();
        switch (gr.mo2417hq() - ItemLocation._m_methodCount) {
            case 0:
                return new Float(m40592wD());
            case 1:
                return new Boolean(m40568D((Item) args[0]));
            case 2:
                return m40570Mo();
            case 3:
                m40589l((Ship) args[0]);
                return null;
            case 4:
                ajk();
                return null;
            case 5:
                return new Float(ajg());
            case 6:
                m40571TG();
                return null;
            case 7:
                return m40573TI();
            case 8:
                m40584aG();
                return null;
            case 9:
                return bxi();
            case 10:
                m40591qU();
                return null;
            case 11:
                m40582a((Item) args[0], (ItemLocation) args[1]);
                return null;
            case 12:
                m40590m((Item) args[0]);
                return null;
            case 13:
                m40587g((Item) args[0]);
                return null;
            case 14:
                m40576TO();
                return null;
            case 15:
                m40574TK();
                return null;
            case 16:
                m40575TM();
                return null;
            case 17:
                m40578TQ();
                return null;
            default:
                return super.mo14a(gr);
        }
    }

    /* renamed from: a */
    public void mo15a(Collection collection, C0495Gr gr) {
        super.mo15a(collection, gr);
    }

    /* renamed from: aH */
    public void mo70aH() {
        switch (bFf().mo6893i(_f_onResurrect_0020_0028_0029V)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, _f_onResurrect_0020_0028_0029V, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, _f_onResurrect_0020_0028_0029V, new Object[0]));
                break;
        }
        m40584aG();
    }

    public float ajh() {
        switch (bFf().mo6893i(bvf)) {
            case 0:
                return 0.0f;
            case 2:
                return ((Float) bFf().mo5606d(new aCE(this, bvf, new Object[0]))).floatValue();
            case 3:
                bFf().mo5606d(new aCE(this, bvf, new Object[0]));
                break;
        }
        return ajg();
    }

    /* renamed from: al */
    public Ship mo22732al() {
        switch (bFf().mo6893i(f9478x851d656b)) {
            case 0:
                return null;
            case 2:
                return (Ship) bFf().mo5606d(new aCE(this, f9478x851d656b, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f9478x851d656b, new Object[0]));
                break;
        }
        return m40570Mo();
    }

    /* access modifiers changed from: protected */
    /* renamed from: b */
    public void mo1887b(Item auq, ItemLocation aag) {
        switch (bFf().mo6893i(f9475RA)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f9475RA, new Object[]{auq, aag}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f9475RA, new Object[]{auq, aag}));
                break;
        }
        m40582a(auq, aag);
    }

    public CargoHoldType bxj() {
        switch (bFf().mo6893i(emQ)) {
            case 0:
                return null;
            case 2:
                return (CargoHoldType) bFf().mo5606d(new aCE(this, emQ, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, emQ, new Object[0]));
                break;
        }
        return bxi();
    }

    /* renamed from: c */
    public C5663aRz[] mo25c() {
        return _m_fields;
    }

    /* access modifiers changed from: protected */
    /* renamed from: h */
    public void mo1889h(Item auq) {
        switch (bFf().mo6893i(f9479ku)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f9479ku, new Object[]{auq}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f9479ku, new Object[]{auq}));
                break;
        }
        m40587g(auq);
    }

    /* renamed from: m */
    public void mo22734m(Ship fAVar) {
        switch (bFf().mo6893i(aPi)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, aPi, new Object[]{fAVar}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, aPi, new Object[]{fAVar}));
                break;
        }
        m40589l(fAVar);
    }

    /* access modifiers changed from: protected */
    /* renamed from: n */
    public void mo1890n(Item auq) {
        switch (bFf().mo6893i(f9476RD)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f9476RD, new Object[]{auq}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f9476RD, new Object[]{auq}));
                break;
        }
        m40590m(auq);
    }

    @C5566aOg
    @C2499fr
    public void pushState() {
        switch (bFf().mo6893i(bvi)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, bvi, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, bvi, new Object[0]));
                break;
        }
        ajk();
    }

    @C5566aOg
    @C2499fr
    /* renamed from: qV */
    public void mo4433qV() {
        switch (bFf().mo6893i(f9473Lm)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f9473Lm, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f9473Lm, new Object[0]));
                break;
        }
        m40591qU();
    }

    /* renamed from: wE */
    public float mo2696wE() {
        switch (bFf().mo6893i(f9477Rz)) {
            case 0:
                return 0.0f;
            case 2:
                return ((Float) bFf().mo5606d(new aCE(this, f9477Rz, new Object[0]))).floatValue();
            case 3:
                bFf().mo5606d(new aCE(this, f9477Rz, new Object[0]));
                break;
        }
        return m40592wD();
    }

    /* renamed from: a */
    public void mo9150a(CargoHoldType arb) {
        super.mo967a((C2961mJ) arb);
        m40572TH();
    }

    @C0064Am(aul = "7bbea6afb002e34b28547c8591d93a75", aum = 0)
    /* renamed from: wD */
    private float m40592wD() {
        return ajf();
    }

    @C0064Am(aul = "2259c432b6010bb2165abb94adc99f4e", aum = 0)
    /* renamed from: D */
    private boolean m40568D(Item auq) {
        if (auq.equals(mo22732al().agr())) {
            return false;
        }
        if (!m40580Tn().bae() || !(auq.bNh() instanceof ShipSector)) {
            return true;
        }
        return false;
    }

    @C0064Am(aul = "d902ac3343fdfa349d8411ab182d1de4", aum = 0)
    /* renamed from: Mo */
    private Ship m40570Mo() {
        return m40580Tn();
    }

    @C0064Am(aul = "83b6ef78abd631d9331a9bab68c0ec3f", aum = 0)
    /* renamed from: l */
    private void m40589l(Ship fAVar) {
        m40588k(fAVar);
    }

    @C0064Am(aul = "653bd84e047c28ec67bc71965a2153cc", aum = 0)
    private float ajg() {
        return aHj();
    }

    @C0064Am(aul = "722b403a045b10b3534ca26651ba3e55", aum = 0)
    /* renamed from: TI */
    private AdapterLikedList<CargoHoldAdapter> m40573TI() {
        if (m40579Te() == null) {
            m40572TH();
        }
        return m40579Te();
    }

    @C0064Am(aul = "e75481f3a67c9ab57c08246fa9532553", aum = 0)
    /* renamed from: aG */
    private void m40584aG() {
        super.mo70aH();
        if (m40579Te() == null) {
            AdapterLikedList zAVar = (AdapterLikedList) bFf().mo6865M(AdapterLikedList.class);
            BaseAdapter aVar = (BaseAdapter) bFf().mo6865M(BaseAdapter.class);
            aVar.mo22737c(this);
            zAVar.mo23260c(aVar);
            m40583a(zAVar);
        }
    }

    @C0064Am(aul = "6a21799379eaad32f212016de0d2470f", aum = 0)
    private CargoHoldType bxi() {
        return (CargoHoldType) getType();
    }

    @C0064Am(aul = "16791cca35cc3df6415484afcc0459ed", aum = 0)
    /* renamed from: a */
    private void m40582a(Item auq, ItemLocation aag) {
        Character agj = m40580Tn().agj();
        if (agj instanceof Player) {
            Player aku = (Player) agj;
            if (aag instanceof LootItems) {
                LootItems ez = (LootItems) aag;
                LDScriptingController bQG = aku.bQG();
                LootType dty = ez.aPj().dty();
                bQG.mo20259b(dty, auq.bAP(), auq.mo302Iq());
                if ((dty instanceof AsteroidLootType) && (auq instanceof RawMaterial)) {
                    ala().aJu().mo7846b(auq.bAP(), (float) auq.mo302Iq(), aku);
                    aku.mo12659ly().mo20728b((RawMaterial) auq, ez);
                }
            }
            agj.mo12641b(auq, aag, this);
        }
    }

    @C0064Am(aul = "31aa580bf37c785b75866c5fa35fad44", aum = 0)
    /* renamed from: m */
    private void m40590m(Item auq) {
        Character agj = m40580Tn().agj();
        if (agj != null) {
            agj.mo12651d(auq, this);
        }
    }

    @C0064Am(aul = "ff416dd408d13f18336a9bcd5bd7bb81", aum = 0)
    /* renamed from: g */
    private void m40587g(Item auq) {
        if (!m40569E(auq)) {
            throw new C2293dh(((NLSItem) ala().aIY().mo6310c(NLSManager.C1472a.ITEM)).bto());
        }
    }

    @C0064Am(aul = "429497e00f614e88804334b116937c47", aum = 0)
    /* renamed from: TO */
    private void m40576TO() {
        m40585dN(((CargoHoldAdapter) m40579Te().ase()).mo1289wE());
    }

    @C0064Am(aul = "8931489eccc736a958a24afd6ac7dc3b", aum = 0)
    /* renamed from: TQ */
    private void m40578TQ() {
        if (m40579Te() == null) {
            mo8358lY(String.valueOf(bFY()) + " with NULL adapterList");
            return;
        }
        m40579Te().mo23259b((C6872avM) this);
        m40577TP();
    }

    @C6485anp
    @C5511aMd
    /* renamed from: a.wI$a */
    public class BaseAdapter extends CargoHoldAdapter implements C1616Xf {

        /* renamed from: PF */
        public static final C2491fm f9480PF = null;

        /* renamed from: Rz */
        public static final C2491fm f9481Rz = null;
        public static final int _m_fieldCount = 0;
        public static final C5663aRz[] _m_fields = null;
        public static final int _m_methodCount = 0;
        public static final C2491fm[] _m_methods = null;
        /* renamed from: aT */
        public static final C5663aRz f9482aT = null;
        public static final C2491fm bEL = null;
        public static final long serialVersionUID = 0;
        public static C6494any ___iScriptClass;
        @C0064Am(aul = "3714e863681f32c7c987acef667e4b2c", aum = 0)
        static /* synthetic */ CargoHold bgf;

        static {
            m40613V();
        }

        public BaseAdapter() {
            super((C5540aNg) null);
            super.mo10S();
        }

        public BaseAdapter(C5540aNg ang) {
            super(ang);
        }

        public BaseAdapter(CargoHold wIVar) {
            super((C5540aNg) null);
            super._m_script_init(wIVar);
        }

        /* renamed from: V */
        static void m40613V() {
            serialVersionUID = (long) 1;
            _m_fieldCount = CargoHoldAdapter._m_fieldCount + 1;
            _m_methodCount = CargoHoldAdapter._m_methodCount + 3;
            int i = CargoHoldAdapter._m_fieldCount;
            C5663aRz[] arzArr = new C5663aRz[(i + 1)];
            C5663aRz b = C5640aRc.m17844b(BaseAdapter.class, "3714e863681f32c7c987acef667e4b2c", i);
            f9482aT = b;
            arzArr[i] = b;
            int i2 = i + 1;
            _m_fields = arzArr;
            C1634Xv.m11725a((Object[]) CargoHoldAdapter._m_fields, (Object[]) _m_fields);
            int i3 = CargoHoldAdapter._m_methodCount;
            C2491fm[] fmVarArr = new C2491fm[(i3 + 3)];
            C2491fm a = C4105zY.m41624a(BaseAdapter.class, "2bd41f7ac77f1b4797585d2be30cfcad", i3);
            f9481Rz = a;
            fmVarArr[i3] = a;
            int i4 = i3 + 1;
            C2491fm a2 = C4105zY.m41624a(BaseAdapter.class, "70e5a3b32d4f52ba2bffa955be0b220e", i4);
            bEL = a2;
            fmVarArr[i4] = a2;
            int i5 = i4 + 1;
            C2491fm a3 = C4105zY.m41624a(BaseAdapter.class, "97829b250a04b184dc4109bc719573ff", i5);
            f9480PF = a3;
            fmVarArr[i5] = a3;
            int i6 = i5 + 1;
            _m_methods = fmVarArr;
            C1634Xv.m11725a((Object[]) CargoHoldAdapter._m_methods, (Object[]) _m_methods);
            ___iScriptClass = aUO.m18566a(BaseAdapter.class, C3595si.class, _m_fields, _m_methods);
        }

        @C0064Am(aul = "97829b250a04b184dc4109bc719573ff", aum = 0)
        /* renamed from: a */
        private void m40615a(GameObjectAdapter ato, GameObjectAdapter ato2, GameObjectAdapter ato3) {
            mo22736b((CargoHoldAdapter) ato, (CargoHoldAdapter) ato2, (CargoHoldAdapter) ato3);
        }

        private CargoHold anh() {
            return (CargoHold) bFf().mo5608dq().mo3214p(f9482aT);
        }

        /* renamed from: b */
        private void m40616b(CargoHold wIVar) {
            bFf().mo5608dq().mo3197f(f9482aT, wIVar);
        }

        /* renamed from: S */
        public void mo10S() {
            super.mo10S();
        }

        /* renamed from: T */
        public C6494any mo11T() {
            return ___iScriptClass;
        }

        /* renamed from: U */
        public C2491fm[] mo12U() {
            return _m_methods;
        }

        /* renamed from: W */
        public Object mo13W() {
            return new C3595si(this);
        }

        /* renamed from: a */
        public Object mo14a(C0495Gr gr) {
            Object[] args = gr.getArgs();
            switch (gr.mo2417hq() - CargoHoldAdapter._m_methodCount) {
                case 0:
                    return new Float(m40617wD());
                case 1:
                    m40614a((CargoHoldAdapter) args[0], (CargoHoldAdapter) args[1], (CargoHoldAdapter) args[2]);
                    return null;
                case 2:
                    m40615a((GameObjectAdapter) args[0], (GameObjectAdapter) args[1], (GameObjectAdapter) args[2]);
                    return null;
                default:
                    return super.mo14a(gr);
            }
        }

        /* renamed from: a */
        public void mo15a(Collection collection, C0495Gr gr) {
            super.mo15a(collection, gr);
        }

        /* renamed from: b */
        public void mo22736b(CargoHoldAdapter sm, CargoHoldAdapter sm2, CargoHoldAdapter sm3) {
            switch (bFf().mo6893i(bEL)) {
                case 0:
                    return;
                case 2:
                    bFf().mo5606d(new aCE(this, bEL, new Object[]{sm, sm2, sm3}));
                    return;
                case 3:
                    bFf().mo5606d(new aCE(this, bEL, new Object[]{sm, sm2, sm3}));
                    break;
            }
            m40614a(sm, sm2, sm3);
        }

        /* renamed from: b */
        public /* bridge */ /* synthetic */ void mo12950b(GameObjectAdapter ato, GameObjectAdapter ato2, GameObjectAdapter ato3) {
            switch (bFf().mo6893i(f9480PF)) {
                case 0:
                    return;
                case 2:
                    bFf().mo5606d(new aCE(this, f9480PF, new Object[]{ato, ato2, ato3}));
                    return;
                case 3:
                    bFf().mo5606d(new aCE(this, f9480PF, new Object[]{ato, ato2, ato3}));
                    break;
            }
            m40615a(ato, ato2, ato3);
        }

        /* renamed from: c */
        public C5663aRz[] mo25c() {
            return _m_fields;
        }

        /* renamed from: wE */
        public float mo1289wE() {
            switch (bFf().mo6893i(f9481Rz)) {
                case 0:
                    return 0.0f;
                case 2:
                    return ((Float) bFf().mo5606d(new aCE(this, f9481Rz, new Object[0]))).floatValue();
                case 3:
                    bFf().mo5606d(new aCE(this, f9481Rz, new Object[0]));
                    break;
            }
            return m40617wD();
        }

        /* renamed from: c */
        public void mo22737c(CargoHold wIVar) {
            m40616b(wIVar);
            super.mo10S();
        }

        @C0064Am(aul = "2bd41f7ac77f1b4797585d2be30cfcad", aum = 0)
        /* renamed from: wD */
        private float m40617wD() {
            return anh().ajh();
        }

        @C0064Am(aul = "70e5a3b32d4f52ba2bffa955be0b220e", aum = 0)
        /* renamed from: a */
        private void m40614a(CargoHoldAdapter sm, CargoHoldAdapter sm2, CargoHoldAdapter sm3) {
            if (sm != null) {
                throw new IllegalArgumentException();
            } else if (sm3 != this) {
                throw new IllegalArgumentException();
            } else {
                super.mo12950b(sm, sm2, sm3);
            }
        }
    }
}
