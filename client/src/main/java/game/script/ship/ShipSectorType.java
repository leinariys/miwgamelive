package game.script.ship;

import game.network.message.externalizable.aCE;
import logic.baa.*;
import logic.data.mbean.C1456VQ;
import logic.res.code.C5640aRc;
import logic.res.code.C5663aRz;
import logic.res.code.C6494any;
import logic.res.html.C2491fm;
import p001a.*;

import java.util.Collection;

@C0566Hp
@C5829abJ("1.1.5")
@C6485anp
@C5511aMd
/* renamed from: a.aUX */
/* compiled from: a */
public class ShipSectorType extends BaseShipSectorType implements C1616Xf {

    /* renamed from: NW */
    public static final C5663aRz f3866NW = null;
    public static final int _m_fieldCount = 0;
    public static final C5663aRz[] _m_fields = null;
    public static final int _m_methodCount = 0;
    public static final C2491fm[] _m_methods = null;
    /* renamed from: dN */
    public static final C2491fm f3867dN = null;
    public static final C5663aRz dOQ = null;
    public static final C5663aRz dOR = null;
    public static final C5663aRz dOS = null;
    public static final C2491fm dOY = null;
    public static final C2491fm dOZ = null;
    public static final C2491fm iXU = null;
    public static final C2491fm iXV = null;
    public static final C2491fm iXW = null;
    public static final C2491fm iXX = null;
    public static final C2491fm iXY = null;
    public static final C2491fm iXZ = null;
    public static final C2491fm iYa = null;
    public static final long serialVersionUID = 0;
    public static C6494any ___iScriptClass;
    @C0064Am(aul = "d318f239e395d72d63815ee7017e819a", aum = 0)
    private static int daj;
    @C0064Am(aul = "bce51736cf657cd6d8a796f03cc97ecb", aum = 1)
    private static boolean dak;
    @C0064Am(aul = "e56837e8be7b024e420ac5ba3481ca21", aum = 2)
    private static int dal;
    @C0064Am(aul = "9f617ad5fbbc6f0fbdbdf93986f5a967", aum = 3)
    private static SectorCategory dam;

    static {
        m18604V();
    }

    public ShipSectorType() {
        super((C5540aNg) null);
        super.mo10S();
    }

    public ShipSectorType(C5540aNg ang) {
        super(ang);
    }

    /* renamed from: V */
    static void m18604V() {
        serialVersionUID = (long) 1;
        _m_fieldCount = BaseShipSectorType._m_fieldCount + 4;
        _m_methodCount = BaseShipSectorType._m_methodCount + 10;
        int i = BaseShipSectorType._m_fieldCount;
        C5663aRz[] arzArr = new C5663aRz[(i + 4)];
        C5663aRz b = C5640aRc.m17844b(ShipSectorType.class, "d318f239e395d72d63815ee7017e819a", i);
        dOQ = b;
        arzArr[i] = b;
        int i2 = i + 1;
        C5663aRz b2 = C5640aRc.m17844b(ShipSectorType.class, "bce51736cf657cd6d8a796f03cc97ecb", i2);
        dOR = b2;
        arzArr[i2] = b2;
        int i3 = i2 + 1;
        C5663aRz b3 = C5640aRc.m17844b(ShipSectorType.class, "e56837e8be7b024e420ac5ba3481ca21", i3);
        dOS = b3;
        arzArr[i3] = b3;
        int i4 = i3 + 1;
        C5663aRz b4 = C5640aRc.m17844b(ShipSectorType.class, "9f617ad5fbbc6f0fbdbdf93986f5a967", i4);
        f3866NW = b4;
        arzArr[i4] = b4;
        int i5 = i4 + 1;
        _m_fields = arzArr;
        C1634Xv.m11725a((Object[]) BaseShipSectorType._m_fields, (Object[]) _m_fields);
        int i6 = BaseShipSectorType._m_methodCount;
        C2491fm[] fmVarArr = new C2491fm[(i6 + 10)];
        C2491fm a = C4105zY.m41624a(ShipSectorType.class, "d7e41d6d2f196bca146f1366468f45fb", i6);
        iXU = a;
        fmVarArr[i6] = a;
        int i7 = i6 + 1;
        C2491fm a2 = C4105zY.m41624a(ShipSectorType.class, "9fcceaf1c4f6cc109140e26a0f3c00aa", i7);
        iXV = a2;
        fmVarArr[i7] = a2;
        int i8 = i7 + 1;
        C2491fm a3 = C4105zY.m41624a(ShipSectorType.class, "a4d82dbc602866cbdcea1ccec2340e06", i8);
        iXW = a3;
        fmVarArr[i8] = a3;
        int i9 = i8 + 1;
        C2491fm a4 = C4105zY.m41624a(ShipSectorType.class, "79ea73f9a3f19e3c4b3341fb014778ff", i9);
        iXX = a4;
        fmVarArr[i9] = a4;
        int i10 = i9 + 1;
        C2491fm a5 = C4105zY.m41624a(ShipSectorType.class, "0a6605d0738a683daa651249ec65cebb", i10);
        dOY = a5;
        fmVarArr[i10] = a5;
        int i11 = i10 + 1;
        C2491fm a6 = C4105zY.m41624a(ShipSectorType.class, "5b084c800d5c0ff04701a505f96933e1", i11);
        iXY = a6;
        fmVarArr[i11] = a6;
        int i12 = i11 + 1;
        C2491fm a7 = C4105zY.m41624a(ShipSectorType.class, "97d042f42b64d9fc3884da7f4be7fe91", i12);
        dOZ = a7;
        fmVarArr[i12] = a7;
        int i13 = i12 + 1;
        C2491fm a8 = C4105zY.m41624a(ShipSectorType.class, "2d30b23796e2679edd5e7e3bb32cd973", i13);
        iXZ = a8;
        fmVarArr[i13] = a8;
        int i14 = i13 + 1;
        C2491fm a9 = C4105zY.m41624a(ShipSectorType.class, "7492e3c3cc935bd946f4109b653faa76", i14);
        iYa = a9;
        fmVarArr[i14] = a9;
        int i15 = i14 + 1;
        C2491fm a10 = C4105zY.m41624a(ShipSectorType.class, "2b0dcd51638e2947f94e9f5df180f18a", i15);
        f3867dN = a10;
        fmVarArr[i15] = a10;
        int i16 = i15 + 1;
        _m_methods = fmVarArr;
        C1634Xv.m11725a((Object[]) BaseShipSectorType._m_methods, (Object[]) _m_methods);
        ___iScriptClass = aUO.m18566a(ShipSectorType.class, C1456VQ.class, _m_fields, _m_methods);
    }

    private int bmJ() {
        return bFf().mo5608dq().mo3212n(dOQ);
    }

    private boolean bmK() {
        return bFf().mo5608dq().mo3201h(dOR);
    }

    private int bmL() {
        return bFf().mo5608dq().mo3212n(dOS);
    }

    private SectorCategory bmM() {
        return (SectorCategory) bFf().mo5608dq().mo3214p(f3866NW);
    }

    /* renamed from: dh */
    private void m18606dh(boolean z) {
        bFf().mo5608dq().mo3153a(dOR, z);
    }

    /* renamed from: g */
    private void m18607g(SectorCategory fMVar) {
        bFf().mo5608dq().mo3197f(f3866NW, fMVar);
    }

    /* renamed from: mq */
    private void m18609mq(int i) {
        bFf().mo5608dq().mo3183b(dOQ, i);
    }

    /* renamed from: mr */
    private void m18610mr(int i) {
        bFf().mo5608dq().mo3183b(dOS, i);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Slots Size")
    /* renamed from: AZ */
    public void mo11638AZ(int i) {
        switch (bFf().mo6893i(iXV)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, iXV, new Object[]{new Integer(i)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, iXV, new Object[]{new Integer(i)}));
                break;
        }
        m18602AY(i);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Sector Position")
    /* renamed from: Bb */
    public void mo11639Bb(int i) {
        switch (bFf().mo6893i(iXY)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, iXY, new Object[]{new Integer(i)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, iXY, new Object[]{new Integer(i)}));
                break;
        }
        m18603Ba(i);
    }

    /* renamed from: T */
    public C6494any mo11T() {
        return ___iScriptClass;
    }

    /* renamed from: U */
    public C2491fm[] mo12U() {
        return _m_methods;
    }

    /* renamed from: W */
    public Object mo13W() {
        return new C1456VQ(this);
    }

    /* renamed from: a */
    public Object mo14a(C0495Gr gr) {
        Object[] args = gr.getArgs();
        switch (gr.mo2417hq() - BaseShipSectorType._m_methodCount) {
            case 0:
                return new Integer(dAE());
            case 1:
                m18602AY(((Integer) args[0]).intValue());
                return null;
            case 2:
                return new Boolean(dAG());
            case 3:
                m18608kw(((Boolean) args[0]).booleanValue());
                return null;
            case 4:
                return new Integer(bmT());
            case 5:
                m18603Ba(((Integer) args[0]).intValue());
                return null;
            case 6:
                return bmV();
            case 7:
                m18611q((SectorCategory) args[0]);
                return null;
            case 8:
                return dAI();
            case 9:
                return m18605aT();
            default:
                return super.mo14a(gr);
        }
    }

    /* renamed from: a */
    public void mo15a(Collection collection, C0495Gr gr) {
        super.mo15a(collection, gr);
    }

    /* access modifiers changed from: protected */
    /* renamed from: aU */
    public <T extends aDJ> T mo745aU() {
        switch (bFf().mo6893i(f3867dN)) {
            case 0:
                return null;
            case 2:
                return (aDJ) bFf().mo5606d(new aCE(this, f3867dN, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f3867dN, new Object[0]));
                break;
        }
        return m18605aT();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Sector Position")
    public int bmU() {
        switch (bFf().mo6893i(dOY)) {
            case 0:
                return 0;
            case 2:
                return ((Integer) bFf().mo5606d(new aCE(this, dOY, new Object[0]))).intValue();
            case 3:
                bFf().mo5606d(new aCE(this, dOY, new Object[0]));
                break;
        }
        return bmT();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Sector Category")
    public SectorCategory bmW() {
        switch (bFf().mo6893i(dOZ)) {
            case 0:
                return null;
            case 2:
                return (SectorCategory) bFf().mo5606d(new aCE(this, dOZ, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, dOZ, new Object[0]));
                break;
        }
        return bmV();
    }

    /* renamed from: c */
    public C5663aRz[] mo25c() {
        return _m_fields;
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Slots Size")
    public int dAF() {
        switch (bFf().mo6893i(iXU)) {
            case 0:
                return 0;
            case 2:
                return ((Integer) bFf().mo5606d(new aCE(this, iXU, new Object[0]))).intValue();
            case 3:
                bFf().mo5606d(new aCE(this, iXU, new Object[0]));
                break;
        }
        return dAE();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Can Be Destroyed")
    public boolean dAH() {
        switch (bFf().mo6893i(iXW)) {
            case 0:
                return false;
            case 2:
                return ((Boolean) bFf().mo5606d(new aCE(this, iXW, new Object[0]))).booleanValue();
            case 3:
                bFf().mo5606d(new aCE(this, iXW, new Object[0]));
                break;
        }
        return dAG();
    }

    public C0981OR dAJ() {
        switch (bFf().mo6893i(iYa)) {
            case 0:
                return null;
            case 2:
                return (C0981OR) bFf().mo5606d(new aCE(this, iYa, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, iYa, new Object[0]));
                break;
        }
        return dAI();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Can Be Destroyed")
    /* renamed from: kx */
    public void mo11645kx(boolean z) {
        switch (bFf().mo6893i(iXX)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, iXX, new Object[]{new Boolean(z)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, iXX, new Object[]{new Boolean(z)}));
                break;
        }
        m18608kw(z);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Sector Category")
    /* renamed from: r */
    public void mo11646r(SectorCategory fMVar) {
        switch (bFf().mo6893i(iXZ)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, iXZ, new Object[]{fMVar}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, iXZ, new Object[]{fMVar}));
                break;
        }
        m18611q(fMVar);
    }

    /* renamed from: S */
    public void mo10S() {
        super.mo10S();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Slots Size")
    @C0064Am(aul = "d7e41d6d2f196bca146f1366468f45fb", aum = 0)
    private int dAE() {
        return bmJ();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Slots Size")
    @C0064Am(aul = "9fcceaf1c4f6cc109140e26a0f3c00aa", aum = 0)
    /* renamed from: AY */
    private void m18602AY(int i) {
        m18609mq(i);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Can Be Destroyed")
    @C0064Am(aul = "a4d82dbc602866cbdcea1ccec2340e06", aum = 0)
    private boolean dAG() {
        return bmK();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Can Be Destroyed")
    @C0064Am(aul = "79ea73f9a3f19e3c4b3341fb014778ff", aum = 0)
    /* renamed from: kw */
    private void m18608kw(boolean z) {
        m18606dh(z);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Sector Position")
    @C0064Am(aul = "0a6605d0738a683daa651249ec65cebb", aum = 0)
    private int bmT() {
        return bmL();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Sector Position")
    @C0064Am(aul = "5b084c800d5c0ff04701a505f96933e1", aum = 0)
    /* renamed from: Ba */
    private void m18603Ba(int i) {
        m18610mr(i);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Sector Category")
    @C0064Am(aul = "97d042f42b64d9fc3884da7f4be7fe91", aum = 0)
    private SectorCategory bmV() {
        return bmM();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Sector Category")
    @C0064Am(aul = "2d30b23796e2679edd5e7e3bb32cd973", aum = 0)
    /* renamed from: q */
    private void m18611q(SectorCategory fMVar) {
        m18607g(fMVar);
    }

    @C0064Am(aul = "7492e3c3cc935bd946f4109b653faa76", aum = 0)
    private C0981OR dAI() {
        return (C0981OR) mo745aU();
    }

    @C0064Am(aul = "2b0dcd51638e2947f94e9f5df180f18a", aum = 0)
    /* renamed from: aT */
    private <T extends aDJ> T m18605aT() {
        T t = (C0981OR) bFf().mo6865M(C0981OR.class);
        t.mo4430e(this);
        return t;
    }
}