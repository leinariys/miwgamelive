package game.script.ship;

import game.network.message.externalizable.aCE;
import logic.baa.*;
import logic.data.mbean.aIA;
import logic.res.code.C5663aRz;
import logic.res.code.C6494any;
import logic.res.html.C2491fm;
import p001a.*;

import java.util.Collection;

@C0566Hp
@C5511aMd
@C6485anp
/* renamed from: a.aBo  reason: case insensitive filesystem */
/* compiled from: a */
public class RestrictedCargoHoldType extends CargoHoldType implements C1616Xf {
    public static final int _m_fieldCount = 0;
    public static final C5663aRz[] _m_fields = null;
    public static final int _m_methodCount = 0;
    public static final C2491fm[] _m_methods = null;
    /* renamed from: dN */
    public static final C2491fm f2442dN = null;
    public static final C2491fm hhf = null;
    public static final long serialVersionUID = 0;
    public static C6494any ___iScriptClass;

    static {
        m13077V();
    }

    public RestrictedCargoHoldType() {
        super((C5540aNg) null);
        super.mo10S();
    }

    public RestrictedCargoHoldType(C5540aNg ang) {
        super(ang);
    }

    /* renamed from: V */
    static void m13077V() {
        serialVersionUID = (long) 1;
        _m_fieldCount = CargoHoldType._m_fieldCount + 0;
        _m_methodCount = CargoHoldType._m_methodCount + 2;
        _m_fields = new C5663aRz[(CargoHoldType._m_fieldCount + 0)];
        C1634Xv.m11725a((Object[]) CargoHoldType._m_fields, (Object[]) _m_fields);
        int i = CargoHoldType._m_methodCount;
        C2491fm[] fmVarArr = new C2491fm[(i + 2)];
        C2491fm a = C4105zY.m41624a(RestrictedCargoHoldType.class, "e8e3d299095f9ff431547e6d87753d9b", i);
        hhf = a;
        fmVarArr[i] = a;
        int i2 = i + 1;
        C2491fm a2 = C4105zY.m41624a(RestrictedCargoHoldType.class, "1b1cb10dae6332fc1cb4f3ed0328b6f7", i2);
        f2442dN = a2;
        fmVarArr[i2] = a2;
        int i3 = i2 + 1;
        _m_methods = fmVarArr;
        C1634Xv.m11725a((Object[]) CargoHoldType._m_methods, (Object[]) _m_methods);
        ___iScriptClass = aUO.m18566a(RestrictedCargoHoldType.class, aIA.class, _m_fields, _m_methods);
    }

    /* renamed from: T */
    public C6494any mo11T() {
        return ___iScriptClass;
    }

    /* renamed from: U */
    public C2491fm[] mo12U() {
        return _m_methods;
    }

    /* renamed from: W */
    public Object mo13W() {
        return new aIA(this);
    }

    /* renamed from: a */
    public Object mo14a(C0495Gr gr) {
        gr.getArgs();
        switch (gr.mo2417hq() - CargoHoldType._m_methodCount) {
            case 0:
                return cIZ();
            case 1:
                return m13078aT();
            default:
                return super.mo14a(gr);
        }
    }

    /* renamed from: a */
    public void mo15a(Collection collection, C0495Gr gr) {
        super.mo15a(collection, gr);
    }

    /* access modifiers changed from: protected */
    /* renamed from: aU */
    public <T extends aDJ> T mo745aU() {
        switch (bFf().mo6893i(f2442dN)) {
            case 0:
                return null;
            case 2:
                return (aDJ) bFf().mo5606d(new aCE(this, f2442dN, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f2442dN, new Object[0]));
                break;
        }
        return m13078aT();
    }

    /* renamed from: c */
    public C5663aRz[] mo25c() {
        return _m_fields;
    }

    public RestrictedCargoHold cJa() {
        switch (bFf().mo6893i(hhf)) {
            case 0:
                return null;
            case 2:
                return (RestrictedCargoHold) bFf().mo5606d(new aCE(this, hhf, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, hhf, new Object[0]));
                break;
        }
        return cIZ();
    }

    /* renamed from: S */
    public void mo10S() {
        super.mo10S();
    }

    @C0064Am(aul = "e8e3d299095f9ff431547e6d87753d9b", aum = 0)
    private RestrictedCargoHold cIZ() {
        return (RestrictedCargoHold) mo745aU();
    }

    @C0064Am(aul = "1b1cb10dae6332fc1cb4f3ed0328b6f7", aum = 0)
    /* renamed from: aT */
    private <T extends aDJ> T m13078aT() {
        T t = (RestrictedCargoHold) bFf().mo6865M(RestrictedCargoHold.class);
        t.mo9150a((CargoHoldType) this);
        return t;
    }
}
