package game.script.ship.categories;

import game.network.message.externalizable.aCE;
import game.script.ship.CargoHold;
import game.script.ship.CargoHoldType;
import game.script.ship.Ship;
import game.script.ship.ShipType;
import logic.baa.C1616Xf;
import logic.baa.C1634Xv;
import logic.baa.C5511aMd;
import logic.baa.C6485anp;
import logic.data.mbean.C0788LQ;
import logic.res.code.C5640aRc;
import logic.res.code.C5663aRz;
import logic.res.code.C6494any;
import logic.res.html.C2491fm;
import p001a.*;

import java.util.Collection;

@C6485anp
@aEZ("Freighter")
@C2712iu(version = "1.0.0")
@C5511aMd
/* renamed from: a.JG */
/* compiled from: a */
public class Freighter extends Ship implements C1616Xf {

    /* renamed from: Lm */
    public static final C2491fm f746Lm = null;
    public static final C2491fm _f_dispose_0020_0028_0029V = null;
    public static final C2491fm _f_onResurrect_0020_0028_0029V = null;
    public static final int _m_fieldCount = 0;
    public static final C5663aRz[] _m_fields = null;
    public static final int _m_methodCount = 0;
    public static final C2491fm[] _m_methods = null;
    public static final C5663aRz dlm = null;
    public static final C5663aRz dlo = null;
    public static final C2491fm dlp = null;
    public static final long serialVersionUID = 0;
    public static C6494any ___iScriptClass;
    @C0064Am(aul = "1f58cc3a72cc5f7d14bde835fc2bb221", aum = 0)
    private static CargoHoldType dll;
    @C0064Am(aul = "750889105577686c549a9c10278e7bde", aum = 1)
    private static CargoHold dln;

    static {
        m5570V();
    }

    public Freighter() {
        super((C5540aNg) null);
        super.mo10S();
    }

    public Freighter(C5540aNg ang) {
        super(ang);
    }

    public Freighter(FreighterType afn) {
        super((C5540aNg) null);
        super._m_script_init(afn);
    }

    /* renamed from: V */
    static void m5570V() {
        serialVersionUID = (long) 1;
        _m_fieldCount = Ship._m_fieldCount + 2;
        _m_methodCount = Ship._m_methodCount + 4;
        int i = Ship._m_fieldCount;
        C5663aRz[] arzArr = new C5663aRz[(i + 2)];
        C5663aRz b = C5640aRc.m17844b(Freighter.class, "1f58cc3a72cc5f7d14bde835fc2bb221", i);
        dlm = b;
        arzArr[i] = b;
        int i2 = i + 1;
        C5663aRz b2 = C5640aRc.m17844b(Freighter.class, "750889105577686c549a9c10278e7bde", i2);
        dlo = b2;
        arzArr[i2] = b2;
        int i3 = i2 + 1;
        _m_fields = arzArr;
        C1634Xv.m11725a((Object[]) Ship._m_fields, (Object[]) _m_fields);
        int i4 = Ship._m_methodCount;
        C2491fm[] fmVarArr = new C2491fm[(i4 + 4)];
        C2491fm a = C4105zY.m41624a(Freighter.class, "fb1a84f6e13cca77ac5ee4216cb49dde", i4);
        dlp = a;
        fmVarArr[i4] = a;
        int i5 = i4 + 1;
        C2491fm a2 = C4105zY.m41624a(Freighter.class, "b2cbe7a896b3dfbd5964382f1492c7ac", i5);
        f746Lm = a2;
        fmVarArr[i5] = a2;
        int i6 = i5 + 1;
        C2491fm a3 = C4105zY.m41624a(Freighter.class, "a5e6a6a0da93dd385602509e7454f2ad", i6);
        _f_onResurrect_0020_0028_0029V = a3;
        fmVarArr[i6] = a3;
        int i7 = i6 + 1;
        C2491fm a4 = C4105zY.m41624a(Freighter.class, "db5ec7d191ce0c0aee3f1f57e88ea4bc", i7);
        _f_dispose_0020_0028_0029V = a4;
        fmVarArr[i7] = a4;
        int i8 = i7 + 1;
        _m_methods = fmVarArr;
        C1634Xv.m11725a((Object[]) Ship._m_methods, (Object[]) _m_methods);
        ___iScriptClass = aUO.m18566a(Freighter.class, C0788LQ.class, _m_fields, _m_methods);
    }

    private CargoHoldType aZv() {
        return ((FreighterType) getType()).bVL();
    }

    private CargoHold aZw() {
        return (CargoHold) bFf().mo5608dq().mo3214p(dlo);
    }

    /* renamed from: d */
    private void m5572d(CargoHold wIVar) {
        bFf().mo5608dq().mo3197f(dlo, wIVar);
    }

    /* renamed from: g */
    private void m5574g(CargoHoldType arb) {
        throw new C6039afL();
    }

    @C0064Am(aul = "b2cbe7a896b3dfbd5964382f1492c7ac", aum = 0)
    @C5566aOg
    @C2499fr
    /* renamed from: qU */
    private void m5575qU() {
        throw new aWi(new aCE(this, f746Lm, new Object[0]));
    }

    /* renamed from: S */
    public void mo10S() {
        super.mo10S();
    }

    /* renamed from: T */
    public C6494any mo11T() {
        return ___iScriptClass;
    }

    /* renamed from: U */
    public C2491fm[] mo12U() {
        return _m_methods;
    }

    /* renamed from: W */
    public Object mo13W() {
        return new C0788LQ(this);
    }

    /* renamed from: a */
    public Object mo14a(C0495Gr gr) {
        gr.getArgs();
        switch (gr.mo2417hq() - Ship._m_methodCount) {
            case 0:
                return aZx();
            case 1:
                m5575qU();
                return null;
            case 2:
                m5571aG();
                return null;
            case 3:
                m5573fg();
                return null;
            default:
                return super.mo14a(gr);
        }
    }

    /* renamed from: a */
    public void mo15a(Collection collection, C0495Gr gr) {
        super.mo15a(collection, gr);
    }

    /* renamed from: aH */
    public void mo70aH() {
        switch (bFf().mo6893i(_f_onResurrect_0020_0028_0029V)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, _f_onResurrect_0020_0028_0029V, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, _f_onResurrect_0020_0028_0029V, new Object[0]));
                break;
        }
        m5571aG();
    }

    public CargoHold aZy() {
        switch (bFf().mo6893i(dlp)) {
            case 0:
                return null;
            case 2:
                return (CargoHold) bFf().mo5606d(new aCE(this, dlp, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, dlp, new Object[0]));
                break;
        }
        return aZx();
    }

    /* renamed from: c */
    public C5663aRz[] mo25c() {
        return _m_fields;
    }

    public void dispose() {
        switch (bFf().mo6893i(_f_dispose_0020_0028_0029V)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, _f_dispose_0020_0028_0029V, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, _f_dispose_0020_0028_0029V, new Object[0]));
                break;
        }
        m5573fg();
    }

    @C5566aOg
    @C2499fr
    /* renamed from: qV */
    public void mo656qV() {
        switch (bFf().mo6893i(f746Lm)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f746Lm, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f746Lm, new Object[0]));
                break;
        }
        m5575qU();
    }

    /* renamed from: a */
    public void mo2958a(FreighterType afn) {
        super.mo18318h((ShipType) afn);
        CargoHoldType aZv = aZv();
        if (aZv() == null) {
            aZv = ala().aJe().mo19007xH().cVd();
        }
        m5572d(aZv.drd());
        aZw().mo22734m((Ship) this);
    }

    @C0064Am(aul = "fb1a84f6e13cca77ac5ee4216cb49dde", aum = 0)
    private CargoHold aZx() {
        return aZw();
    }

    @C0064Am(aul = "a5e6a6a0da93dd385602509e7454f2ad", aum = 0)
    /* renamed from: aG */
    private void m5571aG() {
        super.mo70aH();
        if (agH().agp() == 0.0f) {
            agH().mo4209gV(10000.0f);
        }
    }

    @C0064Am(aul = "db5ec7d191ce0c0aee3f1f57e88ea4bc", aum = 0)
    /* renamed from: fg */
    private void m5573fg() {
        super.dispose();
        if (aZw() != null) {
            aZw().dispose();
            m5572d((CargoHold) null);
        }
    }
}
