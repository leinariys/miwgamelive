package game.script.ship.categories;

import game.network.message.externalizable.aCE;
import game.script.ship.ShipType;
import logic.baa.C1616Xf;
import logic.baa.C1634Xv;
import logic.baa.C5511aMd;
import logic.baa.C6485anp;
import logic.data.mbean.C5384aHg;
import logic.res.code.C5663aRz;
import logic.res.code.C6494any;
import logic.res.html.C2491fm;
import p001a.*;

import java.util.Collection;

@C5511aMd
@C6485anp
/* renamed from: a.J */
/* compiled from: a */
public abstract class BomberBaseType extends ShipType implements C1616Xf {
    public static final C2491fm _f_onResurrect_0020_0028_0029V = null;
    public static final int _m_fieldCount = 0;
    public static final C5663aRz[] _m_fields = null;
    public static final int _m_methodCount = 0;
    public static final C2491fm[] _m_methods = null;
    public static final long serialVersionUID = 0;
    public static C6494any ___iScriptClass;

    static {
        m5520V();
    }

    public BomberBaseType() {
        super((C5540aNg) null);
        super.mo10S();
    }

    public BomberBaseType(C5540aNg ang) {
        super(ang);
    }

    /* renamed from: V */
    static void m5520V() {
        serialVersionUID = (long) 1;
        _m_fieldCount = ShipType._m_fieldCount + 0;
        _m_methodCount = ShipType._m_methodCount + 1;
        _m_fields = new C5663aRz[(ShipType._m_fieldCount + 0)];
        C1634Xv.m11725a((Object[]) ShipType._m_fields, (Object[]) _m_fields);
        int i = ShipType._m_methodCount;
        C2491fm[] fmVarArr = new C2491fm[(i + 1)];
        C2491fm a = C4105zY.m41624a(BomberBaseType.class, "a6131578820917ea72b99e25b303244f", i);
        _f_onResurrect_0020_0028_0029V = a;
        fmVarArr[i] = a;
        int i2 = i + 1;
        _m_methods = fmVarArr;
        C1634Xv.m11725a((Object[]) ShipType._m_methods, (Object[]) _m_methods);
        ___iScriptClass = aUO.m18566a(BomberBaseType.class, C5384aHg.class, _m_fields, _m_methods);
    }

    /* renamed from: T */
    public C6494any mo11T() {
        return ___iScriptClass;
    }

    /* renamed from: U */
    public C2491fm[] mo12U() {
        return _m_methods;
    }

    /* renamed from: W */
    public Object mo13W() {
        return new C5384aHg(this);
    }

    /* renamed from: a */
    public Object mo14a(C0495Gr gr) {
        gr.getArgs();
        switch (gr.mo2417hq() - ShipType._m_methodCount) {
            case 0:
                m5521aG();
                return null;
            default:
                return super.mo14a(gr);
        }
    }

    /* renamed from: a */
    public void mo15a(Collection collection, C0495Gr gr) {
        super.mo15a(collection, gr);
    }

    /* renamed from: aH */
    public void mo70aH() {
        switch (bFf().mo6893i(_f_onResurrect_0020_0028_0029V)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, _f_onResurrect_0020_0028_0029V, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, _f_onResurrect_0020_0028_0029V, new Object[0]));
                break;
        }
        m5521aG();
    }

    /* renamed from: c */
    public C5663aRz[] mo25c() {
        return _m_fields;
    }

    /* renamed from: S */
    public void mo10S() {
        super.mo10S();
    }

    @C0064Am(aul = "a6131578820917ea72b99e25b303244f", aum = 0)
    /* renamed from: aG */
    private void m5521aG() {
        super.mo70aH();
    }
}
