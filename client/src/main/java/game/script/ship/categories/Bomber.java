package game.script.ship.categories;

import game.network.message.externalizable.aCE;
import game.script.damage.DamageType;
import game.script.item.WeaponAdapter;
import game.script.ship.Ship;
import game.script.ship.ShipType;
import game.script.ship.strike.Strike;
import game.script.ship.strike.StrikeType;
import logic.baa.C1616Xf;
import logic.baa.C1634Xv;
import logic.baa.C5511aMd;
import logic.baa.C6485anp;
import logic.data.mbean.C0219Ck;
import logic.data.mbean.C5370aGs;
import logic.res.code.C5640aRc;
import logic.res.code.C5663aRz;
import logic.res.code.C6494any;
import logic.res.html.C2491fm;
import p001a.*;

import java.util.Collection;

@C6485anp
@aEZ("Bomber")
@C2712iu(mo19786Bt = BomberBaseType.class, version = "1.0.0")
@C5511aMd
/* renamed from: a.aHc  reason: case insensitive filesystem */
/* compiled from: a */
public class Bomber extends Ship implements C1616Xf {

    /* renamed from: Lm */
    public static final C2491fm f2971Lm = null;
    /* renamed from: UE */
    public static final C5663aRz f2973UE = null;
    public static final C2491fm _f_dispose_0020_0028_0029V = null;
    public static final C2491fm _f_onResurrect_0020_0028_0029V = null;
    public static final int _m_fieldCount = 0;
    public static final C5663aRz[] _m_fields = null;
    public static final int _m_methodCount = 0;
    public static final C2491fm[] _m_methods = null;
    public static final C2491fm bka = null;
    public static final C2491fm bkd = null;
    public static final C2491fm bqj = null;
    public static final C2491fm dDO = null;
    public static final C5663aRz hUJ = null;
    public static final C2491fm hUK = null;
    public static final C2491fm hUL = null;

    public static C6494any ___iScriptClass = null;
    @C0064Am(aul = "5832adc52a2d480f1343a0540382aa77", aum = 0)

    /* renamed from: UD */
    private static StrikeType f2972UD = null;
    @C0064Am(aul = "8c651229f4acf558853c61251b2e33ed", aum = 1)
    private static Strike hPM = null;

    static {
        m15246V();
    }

    public Bomber() {
        super((C5540aNg) null);
        super.mo10S();
    }

    public Bomber(C5540aNg ang) {
        super(ang);
    }

    public Bomber(BomberType hvVar) {
        super((C5540aNg) null);
        super._m_script_init(hvVar);
    }

    /* renamed from: V */
    static void m15246V() {
        _m_fieldCount = Ship._m_fieldCount + 2;
        _m_methodCount = Ship._m_methodCount + 9;
        int i = Ship._m_fieldCount;
        C5663aRz[] arzArr = new C5663aRz[(i + 2)];
        C5663aRz b = C5640aRc.m17844b(Bomber.class, "5832adc52a2d480f1343a0540382aa77", i);
        f2973UE = b;
        arzArr[i] = b;
        int i2 = i + 1;
        C5663aRz b2 = C5640aRc.m17844b(Bomber.class, "8c651229f4acf558853c61251b2e33ed", i2);
        hUJ = b2;
        arzArr[i2] = b2;
        int i3 = i2 + 1;
        _m_fields = arzArr;
        C1634Xv.m11725a((Object[]) Ship._m_fields, (Object[]) _m_fields);
        int i4 = Ship._m_methodCount;
        C2491fm[] fmVarArr = new C2491fm[(i4 + 9)];
        C2491fm a = C4105zY.m41624a(Bomber.class, "b106bc59567b4094b492e2e6e50dff85", i4);
        bka = a;
        fmVarArr[i4] = a;
        int i5 = i4 + 1;
        C2491fm a2 = C4105zY.m41624a(Bomber.class, "59b090e628647f07cf07b57f9760f26e", i5);
        bkd = a2;
        fmVarArr[i5] = a2;
        int i6 = i5 + 1;
        C2491fm a3 = C4105zY.m41624a(Bomber.class, "f017de3e89a8c0a71c775c8b040bcd2e", i6);
        _f_onResurrect_0020_0028_0029V = a3;
        fmVarArr[i6] = a3;
        int i7 = i6 + 1;
        C2491fm a4 = C4105zY.m41624a(Bomber.class, "1353ff1569d049e2f214e9aca1081655", i7);
        hUK = a4;
        fmVarArr[i7] = a4;
        int i8 = i7 + 1;
        C2491fm a5 = C4105zY.m41624a(Bomber.class, "e7148548ea5d2615c0f36fc559a6bc72", i8);
        f2971Lm = a5;
        fmVarArr[i8] = a5;
        int i9 = i8 + 1;
        C2491fm a6 = C4105zY.m41624a(Bomber.class, "1738085dba789edc84b21c296285d8d8", i9);
        hUL = a6;
        fmVarArr[i9] = a6;
        int i10 = i9 + 1;
        C2491fm a7 = C4105zY.m41624a(Bomber.class, "4b3e4f375cd69ff1aa2ace7e93c13948", i10);
        dDO = a7;
        fmVarArr[i10] = a7;
        int i11 = i10 + 1;
        C2491fm a8 = C4105zY.m41624a(Bomber.class, "18c73391543976cc57e922ffbb63c6fc", i11);
        _f_dispose_0020_0028_0029V = a8;
        fmVarArr[i11] = a8;
        int i12 = i11 + 1;
        C2491fm a9 = C4105zY.m41624a(Bomber.class, "764c56b56c23f203586bd221196ad2ab", i12);
        bqj = a9;
        fmVarArr[i12] = a9;
        int i13 = i12 + 1;
        _m_methods = fmVarArr;
        C1634Xv.m11725a((Object[]) Ship._m_methods, (Object[]) _m_methods);
        ___iScriptClass = aUO.m18566a(Bomber.class, C5370aGs.class, _m_fields, _m_methods);
    }

    /* renamed from: a */
    private void m15247a(StrikeType add) {
        throw new C6039afL();
    }

    @C4034yP
    @C5566aOg
    @C0064Am(aul = "b106bc59567b4094b492e2e6e50dff85", aum = 0)
    @C2499fr
    private void acK() {
        throw new aWi(new aCE(this, bka, new Object[0]));
    }

    @C4034yP
    @C5566aOg
    @C0064Am(aul = "59b090e628647f07cf07b57f9760f26e", aum = 0)
    @C2499fr
    private void acP() {
        throw new aWi(new aCE(this, bkd, new Object[0]));
    }

    @C0064Am(aul = "764c56b56c23f203586bd221196ad2ab", aum = 0)
    private ShipType agG() {
        return dcL();
    }

    /* renamed from: c */
    private void m15249c(Strike dQVar) {
        bFf().mo5608dq().mo3197f(hUJ, dQVar);
    }

    private Strike dcJ() {
        return (Strike) bFf().mo5608dq().mo3214p(hUJ);
    }

    @C0064Am(aul = "e7148548ea5d2615c0f36fc559a6bc72", aum = 0)
    @C5566aOg
    @C2499fr
    /* renamed from: qU */
    private void m15251qU() {
        throw new aWi(new aCE(this, f2971Lm, new Object[0]));
    }

    /* renamed from: ys */
    private StrikeType m15252ys() {
        return ((BomberType) getType()).mo19422yu();
    }

    /* renamed from: S */
    public void mo10S() {
        super.mo10S();
    }

    /* renamed from: T */
    public C6494any mo11T() {
        return ___iScriptClass;
    }

    /* renamed from: U */
    public C2491fm[] mo12U() {
        return _m_methods;
    }

    /* renamed from: W */
    public Object mo13W() {
        return new C5370aGs(this);
    }

    /* renamed from: a */
    public Object mo14a(C0495Gr gr) {
        gr.getArgs();
        switch (gr.mo2417hq() - Ship._m_methodCount) {
            case 0:
                acK();
                return null;
            case 1:
                acP();
                return null;
            case 2:
                m15248aG();
                return null;
            case 3:
                return dcK();
            case 4:
                m15251qU();
                return null;
            case 5:
                return dcM();
            case 6:
                return new Boolean(biK());
            case 7:
                m15250fg();
                return null;
            case 8:
                return agG();
            default:
                return super.mo14a(gr);
        }
    }

    /* renamed from: a */
    public void mo15a(Collection collection, C0495Gr gr) {
        super.mo15a(collection, gr);
    }

    /* renamed from: aH */
    public void mo70aH() {
        switch (bFf().mo6893i(_f_onResurrect_0020_0028_0029V)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, _f_onResurrect_0020_0028_0029V, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, _f_onResurrect_0020_0028_0029V, new Object[0]));
                break;
        }
        m15248aG();
    }

    @C4034yP
    @C5566aOg
    @C2499fr
    public void abort() {
        switch (bFf().mo6893i(bkd)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, bkd, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, bkd, new Object[0]));
                break;
        }
        acP();
    }

    @C4034yP
    @C5566aOg
    @C2499fr
    public void activate() {
        switch (bFf().mo6893i(bka)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, bka, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, bka, new Object[0]));
                break;
        }
        acK();
    }

    public /* bridge */ /* synthetic */ ShipType agH() {
        switch (bFf().mo6893i(bqj)) {
            case 0:
                return null;
            case 2:
                return (ShipType) bFf().mo5606d(new aCE(this, bqj, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, bqj, new Object[0]));
                break;
        }
        return agG();
    }

    public boolean biL() {
        switch (bFf().mo6893i(dDO)) {
            case 0:
                return false;
            case 2:
                return ((Boolean) bFf().mo5606d(new aCE(this, dDO, new Object[0]))).booleanValue();
            case 3:
                bFf().mo5606d(new aCE(this, dDO, new Object[0]));
                break;
        }
        return biK();
    }

    /* renamed from: c */
    public C5663aRz[] mo25c() {
        return _m_fields;
    }

    public BomberType dcL() {
        switch (bFf().mo6893i(hUK)) {
            case 0:
                return null;
            case 2:
                return (BomberType) bFf().mo5606d(new aCE(this, hUK, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, hUK, new Object[0]));
                break;
        }
        return dcK();
    }

    public Strike dcN() {
        switch (bFf().mo6893i(hUL)) {
            case 0:
                return null;
            case 2:
                return (Strike) bFf().mo5606d(new aCE(this, hUL, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, hUL, new Object[0]));
                break;
        }
        return dcM();
    }

    public void dispose() {
        switch (bFf().mo6893i(_f_dispose_0020_0028_0029V)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, _f_dispose_0020_0028_0029V, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, _f_dispose_0020_0028_0029V, new Object[0]));
                break;
        }
        m15250fg();
    }

    @C5566aOg
    @C2499fr
    /* renamed from: qV */
    public void mo656qV() {
        switch (bFf().mo6893i(f2971Lm)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f2971Lm, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f2971Lm, new Object[0]));
                break;
        }
        m15251qU();
    }

    /* renamed from: a */
    public void mo9244a(BomberType hvVar) {
        super.mo18318h((ShipType) hvVar);
        StrikeType ys = m15252ys();
        if (m15252ys() == null) {
            ys = ala().aJe().mo19007xH().cUX();
        }
        m15249c(ys.cVB());
        dcJ().mo17800m((Ship) this);
    }

    @C0064Am(aul = "f017de3e89a8c0a71c775c8b040bcd2e", aum = 0)
    /* renamed from: aG */
    private void m15248aG() {
        super.mo70aH();
        if (dcL().agp() == 0.0f) {
            dcL().mo4209gV(10000.0f);
        }
    }

    @C0064Am(aul = "1353ff1569d049e2f214e9aca1081655", aum = 0)
    private BomberType dcK() {
        return (BomberType) super.agH();
    }

    @C0064Am(aul = "1738085dba789edc84b21c296285d8d8", aum = 0)
    private Strike dcM() {
        return dcJ();
    }

    @C0064Am(aul = "4b3e4f375cd69ff1aa2ace7e93c13948", aum = 0)
    private boolean biK() {
        return true;
    }

    @C0064Am(aul = "18c73391543976cc57e922ffbb63c6fc", aum = 0)
    /* renamed from: fg */
    private void m15250fg() {
        super.dispose();
        if (dcJ() != null) {
            dcJ().dispose();
            m15249c((Strike) null);
        }
    }

    @C6485anp
    @C5511aMd
    /* renamed from: a.aHc$a */
    public class BomberDamageEnhancerAdapter extends WeaponAdapter implements C1616Xf {

        /* renamed from: AI */
        public static final C2491fm f2974AI = null;
        public static final int _m_fieldCount = 0;
        public static final C5663aRz[] _m_fields = null;
        public static final int _m_methodCount = 0;
        public static final C2491fm[] _m_methods = null;
        /* renamed from: aT */
        public static final C5663aRz f2975aT = null;
        public static final long serialVersionUID = 0;
        public static C6494any ___iScriptClass;
        @C0064Am(aul = "cab28441ff37553251a5d10496755d38", aum = 0)
        static /* synthetic */ Bomber cvb;

        static {
            m15263V();
        }

        public BomberDamageEnhancerAdapter() {
            super((C5540aNg) null);
            super.mo10S();
        }

        public BomberDamageEnhancerAdapter(Bomber ahc) {
            super((C5540aNg) null);
            super._m_script_init(ahc);
        }

        public BomberDamageEnhancerAdapter(C5540aNg ang) {
            super(ang);
        }

        /* renamed from: V */
        static void m15263V() {
            serialVersionUID = (long) 1;
            _m_fieldCount = WeaponAdapter._m_fieldCount + 1;
            _m_methodCount = WeaponAdapter._m_methodCount + 1;
            int i = WeaponAdapter._m_fieldCount;
            C5663aRz[] arzArr = new C5663aRz[(i + 1)];
            C5663aRz b = C5640aRc.m17844b(BomberDamageEnhancerAdapter.class, "cab28441ff37553251a5d10496755d38", i);
            f2975aT = b;
            arzArr[i] = b;
            int i2 = i + 1;
            _m_fields = arzArr;
            C1634Xv.m11725a((Object[]) WeaponAdapter._m_fields, (Object[]) _m_fields);
            int i3 = WeaponAdapter._m_methodCount;
            C2491fm[] fmVarArr = new C2491fm[(i3 + 1)];
            C2491fm a = C4105zY.m41624a(BomberDamageEnhancerAdapter.class, "da0cdcc33f519106310263df975c5c9a", i3);
            f2974AI = a;
            fmVarArr[i3] = a;
            int i4 = i3 + 1;
            _m_methods = fmVarArr;
            C1634Xv.m11725a((Object[]) WeaponAdapter._m_methods, (Object[]) _m_methods);
            ___iScriptClass = aUO.m18566a(BomberDamageEnhancerAdapter.class, C0219Ck.class, _m_fields, _m_methods);
        }

        /* renamed from: a */
        private void m15264a(Bomber ahc) {
            bFf().mo5608dq().mo3197f(f2975aT, ahc);
        }

        private Bomber dfw() {
            return (Bomber) bFf().mo5608dq().mo3214p(f2975aT);
        }

        /* renamed from: S */
        public void mo10S() {
            super.mo10S();
        }

        /* renamed from: T */
        public C6494any mo11T() {
            return ___iScriptClass;
        }

        /* renamed from: U */
        public C2491fm[] mo12U() {
            return _m_methods;
        }

        /* renamed from: W */
        public Object mo13W() {
            return new C0219Ck(this);
        }

        /* renamed from: a */
        public Object mo14a(C0495Gr gr) {
            Object[] args = gr.getArgs();
            switch (gr.mo2417hq() - WeaponAdapter._m_methodCount) {
                case 0:
                    return new Float(m15265c((DamageType) args[0]));
                default:
                    return super.mo14a(gr);
            }
        }

        /* renamed from: a */
        public void mo15a(Collection collection, C0495Gr gr) {
            super.mo15a(collection, gr);
        }

        /* renamed from: c */
        public C5663aRz[] mo25c() {
            return _m_fields;
        }

        /* renamed from: d */
        public float mo5375d(DamageType fr) {
            switch (bFf().mo6893i(f2974AI)) {
                case 0:
                    return 0.0f;
                case 2:
                    return ((Float) bFf().mo5606d(new aCE(this, f2974AI, new Object[]{fr}))).floatValue();
                case 3:
                    bFf().mo5606d(new aCE(this, f2974AI, new Object[]{fr}));
                    break;
            }
            return m15265c(fr);
        }

        /* renamed from: b */
        public void mo9251b(Bomber ahc) {
            m15264a(ahc);
            super.mo10S();
        }

        @C0064Am(aul = "da0cdcc33f519106310263df975c5c9a", aum = 0)
        /* renamed from: c */
        private float m15265c(DamageType fr) {
            return 0.0f;
        }
    }
}
