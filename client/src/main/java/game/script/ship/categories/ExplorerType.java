package game.script.ship.categories;

import game.network.message.externalizable.aCE;
import game.script.ship.ShipType;
import game.script.ship.hazardshield.HazardShieldType;
import logic.baa.*;
import logic.data.mbean.C0202CT;
import logic.res.code.C5640aRc;
import logic.res.code.C5663aRz;
import logic.res.code.C6494any;
import logic.res.html.C2491fm;
import p001a.*;

import java.util.Collection;

@C0566Hp
@C5829abJ("1.1.0")
@C6485anp
@C5511aMd
/* renamed from: a.rd */
/* compiled from: a */
public class ExplorerType extends ShipType implements C1616Xf {
    public static final int _m_fieldCount = 0;
    public static final C5663aRz[] _m_fields = null;
    public static final int _m_methodCount = 0;
    public static final C2491fm[] _m_methods = null;
    public static final C5663aRz aZD = null;
    public static final C2491fm aZE = null;
    public static final C2491fm aZF = null;
    public static final C2491fm aZG = null;
    /* renamed from: dN */
    public static final C2491fm f9057dN = null;
    public static final long serialVersionUID = 0;
    public static C6494any ___iScriptClass;
    @C0064Am(aul = "b8ff25d926afd9276cffd48b18afa868", aum = 0)
    private static HazardShieldType aZC;

    static {
        m38472V();
    }

    public ExplorerType() {
        super((C5540aNg) null);
        super.mo10S();
    }

    public ExplorerType(C5540aNg ang) {
        super(ang);
    }

    /* renamed from: V */
    static void m38472V() {
        serialVersionUID = (long) 1;
        _m_fieldCount = ShipType._m_fieldCount + 1;
        _m_methodCount = ShipType._m_methodCount + 4;
        int i = ShipType._m_fieldCount;
        C5663aRz[] arzArr = new C5663aRz[(i + 1)];
        C5663aRz b = C5640aRc.m17844b(ExplorerType.class, "b8ff25d926afd9276cffd48b18afa868", i);
        aZD = b;
        arzArr[i] = b;
        int i2 = i + 1;
        _m_fields = arzArr;
        C1634Xv.m11725a((Object[]) ShipType._m_fields, (Object[]) _m_fields);
        int i3 = ShipType._m_methodCount;
        C2491fm[] fmVarArr = new C2491fm[(i3 + 4)];
        C2491fm a = C4105zY.m41624a(ExplorerType.class, "aae7f28009873557cedd55b493f48255", i3);
        aZE = a;
        fmVarArr[i3] = a;
        int i4 = i3 + 1;
        C2491fm a2 = C4105zY.m41624a(ExplorerType.class, "e0307e94b28ee7777d015411a6fc9f40", i4);
        aZF = a2;
        fmVarArr[i4] = a2;
        int i5 = i4 + 1;
        C2491fm a3 = C4105zY.m41624a(ExplorerType.class, "4abd92d13b9217ada51935eec6c4ecf7", i5);
        aZG = a3;
        fmVarArr[i5] = a3;
        int i6 = i5 + 1;
        C2491fm a4 = C4105zY.m41624a(ExplorerType.class, "ee4ccad7050b4ec7fd5abb1d2f9822f4", i6);
        f9057dN = a4;
        fmVarArr[i6] = a4;
        int i7 = i6 + 1;
        _m_methods = fmVarArr;
        C1634Xv.m11725a((Object[]) ShipType._m_methods, (Object[]) _m_methods);
        ___iScriptClass = aUO.m18566a(ExplorerType.class, C0202CT.class, _m_fields, _m_methods);
    }

    /* renamed from: Yh */
    private HazardShieldType m38473Yh() {
        return (HazardShieldType) bFf().mo5608dq().mo3214p(aZD);
    }

    /* renamed from: a */
    private void m38476a(HazardShieldType rGVar) {
        bFf().mo5608dq().mo3197f(aZD, rGVar);
    }

    /* renamed from: T */
    public C6494any mo11T() {
        return ___iScriptClass;
    }

    /* renamed from: U */
    public C2491fm[] mo12U() {
        return _m_methods;
    }

    /* renamed from: W */
    public Object mo13W() {
        return new C0202CT(this);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Hazard Shield Type")
    /* renamed from: Yj */
    public HazardShieldType mo21696Yj() {
        switch (bFf().mo6893i(aZE)) {
            case 0:
                return null;
            case 2:
                return (HazardShieldType) bFf().mo5606d(new aCE(this, aZE, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, aZE, new Object[0]));
                break;
        }
        return m38474Yi();
    }

    /* renamed from: Yl */
    public Explorer mo21697Yl() {
        switch (bFf().mo6893i(aZG)) {
            case 0:
                return null;
            case 2:
                return (Explorer) bFf().mo5606d(new aCE(this, aZG, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, aZG, new Object[0]));
                break;
        }
        return m38475Yk();
    }

    /* renamed from: a */
    public Object mo14a(C0495Gr gr) {
        Object[] args = gr.getArgs();
        switch (gr.mo2417hq() - ShipType._m_methodCount) {
            case 0:
                return m38474Yi();
            case 1:
                m38478b((HazardShieldType) args[0]);
                return null;
            case 2:
                return m38475Yk();
            case 3:
                return m38477aT();
            default:
                return super.mo14a(gr);
        }
    }

    /* renamed from: a */
    public void mo15a(Collection collection, C0495Gr gr) {
        super.mo15a(collection, gr);
    }

    /* access modifiers changed from: protected */
    /* renamed from: aU */
    public <T extends aDJ> T mo745aU() {
        switch (bFf().mo6893i(f9057dN)) {
            case 0:
                return null;
            case 2:
                return (aDJ) bFf().mo5606d(new aCE(this, f9057dN, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f9057dN, new Object[0]));
                break;
        }
        return m38477aT();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Hazard Shield Type")
    /* renamed from: c */
    public void mo21698c(HazardShieldType rGVar) {
        switch (bFf().mo6893i(aZF)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, aZF, new Object[]{rGVar}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, aZF, new Object[]{rGVar}));
                break;
        }
        m38478b(rGVar);
    }

    /* renamed from: c */
    public C5663aRz[] mo25c() {
        return _m_fields;
    }

    /* renamed from: S */
    public void mo10S() {
        super.mo10S();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Hazard Shield Type")
    @C0064Am(aul = "aae7f28009873557cedd55b493f48255", aum = 0)
    /* renamed from: Yi */
    private HazardShieldType m38474Yi() {
        return m38473Yh();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Hazard Shield Type")
    @C0064Am(aul = "e0307e94b28ee7777d015411a6fc9f40", aum = 0)
    /* renamed from: b */
    private void m38478b(HazardShieldType rGVar) {
        m38476a(rGVar);
    }

    @C0064Am(aul = "4abd92d13b9217ada51935eec6c4ecf7", aum = 0)
    /* renamed from: Yk */
    private Explorer m38475Yk() {
        return (Explorer) mo745aU();
    }

    @C0064Am(aul = "ee4ccad7050b4ec7fd5abb1d2f9822f4", aum = 0)
    /* renamed from: aT */
    private <T extends aDJ> T m38477aT() {
        T t = (Explorer) bFf().mo6865M(Explorer.class);
        t.mo18701a(this);
        return t;
    }
}
