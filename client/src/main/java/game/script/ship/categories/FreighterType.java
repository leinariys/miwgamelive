package game.script.ship.categories;

import game.network.message.externalizable.aCE;
import game.script.ship.CargoHoldType;
import game.script.ship.ShipType;
import logic.baa.*;
import logic.data.mbean.C5882acK;
import logic.res.code.C5640aRc;
import logic.res.code.C5663aRz;
import logic.res.code.C6494any;
import logic.res.html.C2491fm;
import p001a.*;

import java.util.Collection;

@C0566Hp
@C5829abJ("1.0.0")
@C6485anp
@C5511aMd
/* renamed from: a.afN  reason: case insensitive filesystem */
/* compiled from: a */
public class FreighterType extends ShipType implements C1616Xf {
    public static final int _m_fieldCount = 0;
    public static final C5663aRz[] _m_fields = null;
    public static final int _m_methodCount = 0;
    public static final C2491fm[] _m_methods = null;
    /* renamed from: dN */
    public static final C2491fm f4464dN = null;
    public static final C5663aRz dlm = null;
    public static final C2491fm fvC = null;
    public static final C2491fm fvD = null;
    public static final C2491fm fvE = null;
    public static final long serialVersionUID = 0;
    public static C6494any ___iScriptClass;
    @C0064Am(aul = "7f2b46af74fd70d0a696f4be819243b5", aum = 0)
    private static CargoHoldType dll;

    static {
        m21524V();
    }

    public FreighterType() {
        super((C5540aNg) null);
        super.mo10S();
    }

    public FreighterType(C5540aNg ang) {
        super(ang);
    }

    /* renamed from: V */
    static void m21524V() {
        serialVersionUID = (long) 1;
        _m_fieldCount = ShipType._m_fieldCount + 1;
        _m_methodCount = ShipType._m_methodCount + 4;
        int i = ShipType._m_fieldCount;
        C5663aRz[] arzArr = new C5663aRz[(i + 1)];
        C5663aRz b = C5640aRc.m17844b(FreighterType.class, "7f2b46af74fd70d0a696f4be819243b5", i);
        dlm = b;
        arzArr[i] = b;
        int i2 = i + 1;
        _m_fields = arzArr;
        C1634Xv.m11725a((Object[]) ShipType._m_fields, (Object[]) _m_fields);
        int i3 = ShipType._m_methodCount;
        C2491fm[] fmVarArr = new C2491fm[(i3 + 4)];
        C2491fm a = C4105zY.m41624a(FreighterType.class, "bf989b50a34e1a85dbd06a5a6c5fd5d1", i3);
        fvC = a;
        fmVarArr[i3] = a;
        int i4 = i3 + 1;
        C2491fm a2 = C4105zY.m41624a(FreighterType.class, "85c7da4a87f18417d12c34990233b196", i4);
        fvD = a2;
        fmVarArr[i4] = a2;
        int i5 = i4 + 1;
        C2491fm a3 = C4105zY.m41624a(FreighterType.class, "0232fd5ecb692a80ebed8ee6da8cca4d", i5);
        fvE = a3;
        fmVarArr[i5] = a3;
        int i6 = i5 + 1;
        C2491fm a4 = C4105zY.m41624a(FreighterType.class, "292d837f6fa6c1054bc66261f46a9cf5", i6);
        f4464dN = a4;
        fmVarArr[i6] = a4;
        int i7 = i6 + 1;
        _m_methods = fmVarArr;
        C1634Xv.m11725a((Object[]) ShipType._m_methods, (Object[]) _m_methods);
        ___iScriptClass = aUO.m18566a(FreighterType.class, C5882acK.class, _m_fields, _m_methods);
    }

    private CargoHoldType aZv() {
        return (CargoHoldType) bFf().mo5608dq().mo3214p(dlm);
    }

    /* renamed from: g */
    private void m21526g(CargoHoldType arb) {
        bFf().mo5608dq().mo3197f(dlm, arb);
    }

    /* renamed from: T */
    public C6494any mo11T() {
        return ___iScriptClass;
    }

    /* renamed from: U */
    public C2491fm[] mo12U() {
        return _m_methods;
    }

    /* renamed from: W */
    public Object mo13W() {
        return new C5882acK(this);
    }

    /* renamed from: a */
    public Object mo14a(C0495Gr gr) {
        Object[] args = gr.getArgs();
        switch (gr.mo2417hq() - ShipType._m_methodCount) {
            case 0:
                return bVK();
            case 1:
                m21527j((CargoHoldType) args[0]);
                return null;
            case 2:
                return bVM();
            case 3:
                return m21525aT();
            default:
                return super.mo14a(gr);
        }
    }

    /* renamed from: a */
    public void mo15a(Collection collection, C0495Gr gr) {
        super.mo15a(collection, gr);
    }

    /* access modifiers changed from: protected */
    /* renamed from: aU */
    public <T extends aDJ> T mo745aU() {
        switch (bFf().mo6893i(f4464dN)) {
            case 0:
                return null;
            case 2:
                return (aDJ) bFf().mo5606d(new aCE(this, f4464dN, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f4464dN, new Object[0]));
                break;
        }
        return m21525aT();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Ship Vault Type")
    public CargoHoldType bVL() {
        switch (bFf().mo6893i(fvC)) {
            case 0:
                return null;
            case 2:
                return (CargoHoldType) bFf().mo5606d(new aCE(this, fvC, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, fvC, new Object[0]));
                break;
        }
        return bVK();
    }

    public Freighter bVN() {
        switch (bFf().mo6893i(fvE)) {
            case 0:
                return null;
            case 2:
                return (Freighter) bFf().mo5606d(new aCE(this, fvE, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, fvE, new Object[0]));
                break;
        }
        return bVM();
    }

    /* renamed from: c */
    public C5663aRz[] mo25c() {
        return _m_fields;
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Ship Vault Type")
    /* renamed from: k */
    public void mo13250k(CargoHoldType arb) {
        switch (bFf().mo6893i(fvD)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, fvD, new Object[]{arb}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, fvD, new Object[]{arb}));
                break;
        }
        m21527j(arb);
    }

    /* renamed from: S */
    public void mo10S() {
        super.mo10S();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Ship Vault Type")
    @C0064Am(aul = "bf989b50a34e1a85dbd06a5a6c5fd5d1", aum = 0)
    private CargoHoldType bVK() {
        return aZv();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Ship Vault Type")
    @C0064Am(aul = "85c7da4a87f18417d12c34990233b196", aum = 0)
    /* renamed from: j */
    private void m21527j(CargoHoldType arb) {
        m21526g(arb);
    }

    @C0064Am(aul = "0232fd5ecb692a80ebed8ee6da8cca4d", aum = 0)
    private Freighter bVM() {
        return (Freighter) mo745aU();
    }

    @C0064Am(aul = "292d837f6fa6c1054bc66261f46a9cf5", aum = 0)
    /* renamed from: aT */
    private <T extends aDJ> T m21525aT() {
        T t = (Freighter) bFf().mo6865M(Freighter.class);
        t.mo2958a(this);
        return t;
    }
}
