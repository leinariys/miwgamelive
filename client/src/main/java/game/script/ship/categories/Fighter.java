package game.script.ship.categories;

import game.network.message.externalizable.C6950awp;
import game.network.message.externalizable.aCE;
import game.script.ship.Ship;
import game.script.ship.ShipAdapter;
import game.script.ship.ShipType;
import game.script.ship.speedBoost.SpeedBoost;
import game.script.ship.speedBoost.SpeedBoostType;
import game.script.simulation.Space;
import logic.baa.C1616Xf;
import logic.baa.C1634Xv;
import logic.baa.C5511aMd;
import logic.baa.C6485anp;
import logic.data.mbean.C5753aVl;
import logic.data.mbean.C6313akZ;
import logic.res.code.C5640aRc;
import logic.res.code.C5663aRz;
import logic.res.code.C6494any;
import logic.res.html.C2491fm;
import p001a.*;

import java.util.Collection;

@C5829abJ("1.0.0")
@C6485anp
@aEZ("Fighter")
@C2712iu(version = "1.0.0")
@C5511aMd
/* renamed from: a.pF */
/* compiled from: a */
public class Fighter extends Ship implements C1616Xf {

    /* renamed from: Lm */
    public static final C2491fm f8812Lm = null;
    public static final C2491fm _f_dispose_0020_0028_0029V = null;
    public static final int _m_fieldCount = 0;
    public static final C5663aRz[] _m_fields = null;
    public static final int _m_methodCount = 0;
    public static final C2491fm[] _m_methods = null;
    public static final C2491fm bka = null;
    public static final C2491fm bpD = null;
    public static final C2491fm brG = null;
    public static final C2491fm brH = null;
    public static final C2491fm brJ = null;
    public static final C5663aRz dDG = null;
    public static final C5663aRz dDI = null;
    public static final C2491fm dDJ = null;
    public static final C2491fm dDK = null;
    public static final C2491fm dDL = null;
    public static final C2491fm dDM = null;
    public static final C2491fm dDN = null;
    public static final C2491fm dDO = null;
    public static final C2491fm dDP = null;
    public static final long serialVersionUID = 0;
    /* renamed from: vq */
    public static final C2491fm f8813vq = null;
    public static C6494any ___iScriptClass;
    @C0064Am(aul = "3ac2768fac77612aeeddc30d53149ddd", aum = 0)
    private static SpeedBoostType dDF;
    @C0064Am(aul = "d1bb063167dc89e8fe98eff1c2e8cdd8", aum = 1)
    private static SpeedBoost dDH;

    static {
        m36987V();
    }

    public Fighter() {
        super((C5540aNg) null);
        super.mo10S();
    }

    public Fighter(FighterType aml) {
        super((C5540aNg) null);
        super._m_script_init(aml);
    }

    public Fighter(C5540aNg ang) {
        super(ang);
    }

    /* renamed from: V */
    static void m36987V() {
        serialVersionUID = (long) 1;
        _m_fieldCount = Ship._m_fieldCount + 2;
        _m_methodCount = Ship._m_methodCount + 15;
        int i = Ship._m_fieldCount;
        C5663aRz[] arzArr = new C5663aRz[(i + 2)];
        C5663aRz b = C5640aRc.m17844b(Fighter.class, "3ac2768fac77612aeeddc30d53149ddd", i);
        dDG = b;
        arzArr[i] = b;
        int i2 = i + 1;
        C5663aRz b2 = C5640aRc.m17844b(Fighter.class, "d1bb063167dc89e8fe98eff1c2e8cdd8", i2);
        dDI = b2;
        arzArr[i2] = b2;
        int i3 = i2 + 1;
        _m_fields = arzArr;
        C1634Xv.m11725a((Object[]) Ship._m_fields, (Object[]) _m_fields);
        int i4 = Ship._m_methodCount;
        C2491fm[] fmVarArr = new C2491fm[(i4 + 15)];
        C2491fm a = C4105zY.m41624a(Fighter.class, "bd628d3b09a9b0f3370964e438749a87", i4);
        brH = a;
        fmVarArr[i4] = a;
        int i5 = i4 + 1;
        C2491fm a2 = C4105zY.m41624a(Fighter.class, "ac91b3192a8884302b7e5ccc36d733ab", i5);
        bka = a2;
        fmVarArr[i5] = a2;
        int i6 = i5 + 1;
        C2491fm a3 = C4105zY.m41624a(Fighter.class, "5329c32b25ece83a64653e74f4929891", i6);
        dDJ = a3;
        fmVarArr[i6] = a3;
        int i7 = i6 + 1;
        C2491fm a4 = C4105zY.m41624a(Fighter.class, "d2dcd197e45479dce960fc3c4b193933", i7);
        dDK = a4;
        fmVarArr[i7] = a4;
        int i8 = i7 + 1;
        C2491fm a5 = C4105zY.m41624a(Fighter.class, "64ee53c5849861ddca0dd9438e7342a1", i8);
        dDL = a5;
        fmVarArr[i8] = a5;
        int i9 = i8 + 1;
        C2491fm a6 = C4105zY.m41624a(Fighter.class, "bb454cf1b217008b7209ebca47bea5e1", i9);
        f8812Lm = a6;
        fmVarArr[i9] = a6;
        int i10 = i9 + 1;
        C2491fm a7 = C4105zY.m41624a(Fighter.class, "da1fb525fe3873f5756bd4796d84be89", i10);
        brG = a7;
        fmVarArr[i10] = a7;
        int i11 = i10 + 1;
        C2491fm a8 = C4105zY.m41624a(Fighter.class, "201651359970b1856a88e418a9b424fa", i11);
        dDM = a8;
        fmVarArr[i11] = a8;
        int i12 = i11 + 1;
        C2491fm a9 = C4105zY.m41624a(Fighter.class, "978fcb53b420819bf94cb7cf28da8ffd", i12);
        dDN = a9;
        fmVarArr[i12] = a9;
        int i13 = i12 + 1;
        C2491fm a10 = C4105zY.m41624a(Fighter.class, "eb7a065a83bc4d24a0ac889b8b49f8bd", i13);
        dDO = a10;
        fmVarArr[i13] = a10;
        int i14 = i13 + 1;
        C2491fm a11 = C4105zY.m41624a(Fighter.class, "1762ea0abf57ff9163b0d428d45dfd29", i14);
        bpD = a11;
        fmVarArr[i14] = a11;
        int i15 = i14 + 1;
        C2491fm a12 = C4105zY.m41624a(Fighter.class, "4f25d093d6264fd4ca2bc9e4f9221292", i15);
        f8813vq = a12;
        fmVarArr[i15] = a12;
        int i16 = i15 + 1;
        C2491fm a13 = C4105zY.m41624a(Fighter.class, "d3ebfb804f0a1fd13bb437bd760d0faa", i16);
        dDP = a13;
        fmVarArr[i16] = a13;
        int i17 = i16 + 1;
        C2491fm a14 = C4105zY.m41624a(Fighter.class, "42c57c95e24695a7e3f5cb896b93c34d", i17);
        _f_dispose_0020_0028_0029V = a14;
        fmVarArr[i17] = a14;
        int i18 = i17 + 1;
        C2491fm a15 = C4105zY.m41624a(Fighter.class, "56b5a576c66bd029b9fb62f74a7fb4a4", i18);
        brJ = a15;
        fmVarArr[i18] = a15;
        int i19 = i18 + 1;
        _m_methods = fmVarArr;
        C1634Xv.m11725a((Object[]) Ship._m_methods, (Object[]) _m_methods);
        ___iScriptClass = aUO.m18566a(Fighter.class, C6313akZ.class, _m_fields, _m_methods);
    }

    /* renamed from: a */
    private void m36989a(SpeedBoostType lJVar) {
        throw new C6039afL();
    }

    @C0064Am(aul = "56b5a576c66bd029b9fb62f74a7fb4a4", aum = 0)
    private C6901avp ahN() {
        return biN();
    }

    private SpeedBoost biA() {
        return (SpeedBoost) bFf().mo5608dq().mo3214p(dDI);
    }

    private SpeedBoostType biz() {
        return ((FighterType) getType()).djC();
    }

    /* renamed from: c */
    private void m36990c(SpeedBoost dcVar) {
        bFf().mo5608dq().mo3197f(dDI, dcVar);
    }

    @C0064Am(aul = "bb454cf1b217008b7209ebca47bea5e1", aum = 0)
    @C5566aOg
    @C2499fr
    /* renamed from: qU */
    private void m36994qU() {
        throw new aWi(new aCE(this, f8812Lm, new Object[0]));
    }

    /* renamed from: S */
    public void mo10S() {
        super.mo10S();
    }

    /* renamed from: T */
    public C6494any mo11T() {
        return ___iScriptClass;
    }

    /* renamed from: U */
    public C2491fm[] mo12U() {
        return _m_methods;
    }

    /* renamed from: W */
    public Object mo13W() {
        return new C6313akZ(this);
    }

    /* renamed from: a */
    public Object mo14a(C0495Gr gr) {
        Object[] args = gr.getArgs();
        switch (gr.mo2417hq() - Ship._m_methodCount) {
            case 0:
                return new Boolean(ahL());
            case 1:
                acK();
                return null;
            case 2:
                biB();
                return null;
            case 3:
                return new Boolean(biC());
            case 4:
                return new Boolean(biE());
            case 5:
                m36994qU();
                return null;
            case 6:
                m36992dd(((Long) args[0]).longValue());
                return null;
            case 7:
                return biG();
            case 8:
                return biI();
            case 9:
                return new Boolean(biK());
            case 10:
                return m36991d((Space) args[0]);
            case 11:
                m36988a(((Integer) args[0]).intValue(), ((Long) args[1]).longValue(), (Collection) args[2], (C6705asB) args[3]);
                return null;
            case 12:
                return biM();
            case 13:
                m36993fg();
                return null;
            case 14:
                return ahN();
            default:
                return super.mo14a(gr);
        }
    }

    /* renamed from: a */
    public void mo15a(Collection collection, C0495Gr gr) {
        super.mo15a(collection, gr);
    }

    public void activate() {
        switch (bFf().mo6893i(bka)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, bka, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, bka, new Object[0]));
                break;
        }
        acK();
    }

    /* access modifiers changed from: protected */
    public boolean ahM() {
        switch (bFf().mo6893i(brH)) {
            case 0:
                return false;
            case 2:
                return ((Boolean) bFf().mo5606d(new aCE(this, brH, new Object[0]))).booleanValue();
            case 3:
                bFf().mo5606d(new aCE(this, brH, new Object[0]));
                break;
        }
        return ahL();
    }

    public /* bridge */ /* synthetic */ C6901avp ahO() {
        switch (bFf().mo6893i(brJ)) {
            case 0:
                return null;
            case 2:
                return (C6901avp) bFf().mo5606d(new aCE(this, brJ, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, brJ, new Object[0]));
                break;
        }
        return ahN();
    }

    @C2198cg
    /* renamed from: b */
    public void mo990b(int i, long j, Collection<C6625aqZ> collection, C6705asB asb) {
        switch (bFf().mo6893i(f8813vq)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f8813vq, new Object[]{new Integer(i), new Long(j), collection, asb}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f8813vq, new Object[]{new Integer(i), new Long(j), collection, asb}));
                break;
        }
        m36988a(i, j, collection, asb);
    }

    public boolean biD() {
        switch (bFf().mo6893i(dDK)) {
            case 0:
                return false;
            case 2:
                return ((Boolean) bFf().mo5606d(new aCE(this, dDK, new Object[0]))).booleanValue();
            case 3:
                bFf().mo5606d(new aCE(this, dDK, new Object[0]));
                break;
        }
        return biC();
    }

    public boolean biF() {
        switch (bFf().mo6893i(dDL)) {
            case 0:
                return false;
            case 2:
                return ((Boolean) bFf().mo5606d(new aCE(this, dDL, new Object[0]))).booleanValue();
            case 3:
                bFf().mo5606d(new aCE(this, dDL, new Object[0]));
                break;
        }
        return biE();
    }

    public SpeedBoost.C2286b biH() {
        switch (bFf().mo6893i(dDM)) {
            case 0:
                return null;
            case 2:
                return (SpeedBoost.C2286b) bFf().mo5606d(new aCE(this, dDM, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, dDM, new Object[0]));
                break;
        }
        return biG();
    }

    public SpeedBoost biJ() {
        switch (bFf().mo6893i(dDN)) {
            case 0:
                return null;
            case 2:
                return (SpeedBoost) bFf().mo5606d(new aCE(this, dDN, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, dDN, new Object[0]));
                break;
        }
        return biI();
    }

    public boolean biL() {
        switch (bFf().mo6893i(dDO)) {
            case 0:
                return false;
            case 2:
                return ((Boolean) bFf().mo5606d(new aCE(this, dDO, new Object[0]))).booleanValue();
            case 3:
                bFf().mo5606d(new aCE(this, dDO, new Object[0]));
                break;
        }
        return biK();
    }

    public C2623hh biN() {
        switch (bFf().mo6893i(dDP)) {
            case 0:
                return null;
            case 2:
                return (C2623hh) bFf().mo5606d(new aCE(this, dDP, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, dDP, new Object[0]));
                break;
        }
        return biM();
    }

    /* renamed from: c */
    public C5663aRz[] mo25c() {
        return _m_fields;
    }

    /* renamed from: de */
    public void mo7599de(long j) {
        switch (bFf().mo6893i(brG)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, brG, new Object[]{new Long(j)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, brG, new Object[]{new Long(j)}));
                break;
        }
        m36992dd(j);
    }

    public void deactivate() {
        switch (bFf().mo6893i(dDJ)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, dDJ, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, dDJ, new Object[0]));
                break;
        }
        biB();
    }

    public void dispose() {
        switch (bFf().mo6893i(_f_dispose_0020_0028_0029V)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, _f_dispose_0020_0028_0029V, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, _f_dispose_0020_0028_0029V, new Object[0]));
                break;
        }
        m36993fg();
    }

    /* access modifiers changed from: protected */
    /* renamed from: e */
    public C6901avp mo18314e(Space ea) {
        switch (bFf().mo6893i(bpD)) {
            case 0:
                return null;
            case 2:
                return (C6901avp) bFf().mo5606d(new aCE(this, bpD, new Object[]{ea}));
            case 3:
                bFf().mo5606d(new aCE(this, bpD, new Object[]{ea}));
                break;
        }
        return m36991d(ea);
    }

    @C5566aOg
    @C2499fr
    /* renamed from: qV */
    public void mo656qV() {
        switch (bFf().mo6893i(f8812Lm)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f8812Lm, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f8812Lm, new Object[0]));
                break;
        }
        m36994qU();
    }

    /* renamed from: a */
    public void mo21109a(FighterType aml) {
        super.mo18318h((ShipType) aml);
        SpeedBoostType biz = biz();
        if (biz() == null) {
            biz = ala().aJe().mo19007xH().cVb();
        }
        m36990c((SpeedBoost) biz.mo7459NK());
        biA().mo17890e(this);
    }

    @C0064Am(aul = "bd628d3b09a9b0f3370964e438749a87", aum = 0)
    private boolean ahL() {
        return super.ahM() && biA().biH() != SpeedBoost.C2286b.CONSUMING;
    }

    @C0064Am(aul = "ac91b3192a8884302b7e5ccc36d733ab", aum = 0)
    private void acK() {
        if (!agZ() && !ahh() && !biF()) {
            biA().activate();
        }
    }

    @C0064Am(aul = "5329c32b25ece83a64653e74f4929891", aum = 0)
    private void biB() {
        if (biF()) {
            biA().activate();
        }
    }

    @C0064Am(aul = "d2dcd197e45479dce960fc3c4b193933", aum = 0)
    private boolean biC() {
        return biA().biD();
    }

    @C0064Am(aul = "64ee53c5849861ddca0dd9438e7342a1", aum = 0)
    private boolean biE() {
        return biA().biH() == SpeedBoost.C2286b.CONSUMING;
    }

    @C0064Am(aul = "da1fb525fe3873f5756bd4796d84be89", aum = 0)
    /* renamed from: dd */
    private void m36992dd(long j) {
        if (biA().biH() != SpeedBoost.C2286b.CONSUMING) {
            super.mo7599de(j);
        }
    }

    @C0064Am(aul = "201651359970b1856a88e418a9b424fa", aum = 0)
    private SpeedBoost.C2286b biG() {
        return biA().biH();
    }

    @C0064Am(aul = "978fcb53b420819bf94cb7cf28da8ffd", aum = 0)
    private SpeedBoost biI() {
        return biA();
    }

    @C0064Am(aul = "eb7a065a83bc4d24a0ac889b8b49f8bd", aum = 0)
    private boolean biK() {
        return true;
    }

    @C0064Am(aul = "1762ea0abf57ff9163b0d428d45dfd29", aum = 0)
    /* renamed from: d */
    private C6901avp m36991d(Space ea) {
        return new C2623hh(ea, this, mo958IL());
    }

    @C0064Am(aul = "4f25d093d6264fd4ca2bc9e4f9221292", aum = 0)
    @C2198cg
    /* renamed from: a */
    private void m36988a(int i, long j, Collection<C6625aqZ> collection, C6705asB asb) {
        if (cLd() != null) {
            asb.mo13024a(collection, (C6950awp) new C5874acC(i, this, cLd().mo2472kQ(), j));
        }
    }

    @C0064Am(aul = "d3ebfb804f0a1fd13bb437bd760d0faa", aum = 0)
    private C2623hh biM() {
        if (super.ahO() instanceof C2623hh) {
            return (C2623hh) super.ahO();
        }
        if (super.ahO() != null) {
            mo8358lY("NULL POINTER AHEAD, solid for " + this + " is not a FighterSolid");
        }
        return null;
    }

    @C0064Am(aul = "42c57c95e24695a7e3f5cb896b93c34d", aum = 0)
    /* renamed from: fg */
    private void m36993fg() {
        super.dispose();
        if (biA() != null) {
            biA().dispose();
            m36990c((SpeedBoost) null);
        }
    }

    @C6485anp
    @C5511aMd
    @Deprecated
    /* renamed from: a.pF$a */
    public class ShipBoost extends ShipAdapter implements C1616Xf {
        public static final int _m_fieldCount = 0;
        public static final C5663aRz[] _m_fields = null;
        public static final int _m_methodCount = 0;
        public static final C2491fm[] _m_methods = null;
        /* renamed from: aT */
        public static final C5663aRz f8814aT = null;
        public static final C2491fm aTe = null;
        public static final C2491fm aTf = null;
        public static final C2491fm aTg = null;
        public static final C2491fm aTh = null;
        public static final long serialVersionUID = 0;
        public static C6494any ___iScriptClass;
        @C0064Am(aul = "b8fbc343a0a7811040634459f4140841", aum = 0)
        static /* synthetic */ Fighter aTd;

        static {
            m37007V();
        }

        public ShipBoost() {
            super((C5540aNg) null);
            super.mo10S();
        }

        public ShipBoost(C5540aNg ang) {
            super(ang);
        }

        public ShipBoost(Fighter pFVar) {
            super((C5540aNg) null);
            super._m_script_init(pFVar);
        }

        /* renamed from: V */
        static void m37007V() {
            serialVersionUID = (long) 1;
            _m_fieldCount = ShipAdapter._m_fieldCount + 1;
            _m_methodCount = ShipAdapter._m_methodCount + 4;
            int i = ShipAdapter._m_fieldCount;
            C5663aRz[] arzArr = new C5663aRz[(i + 1)];
            C5663aRz b = C5640aRc.m17844b(ShipBoost.class, "b8fbc343a0a7811040634459f4140841", i);
            f8814aT = b;
            arzArr[i] = b;
            int i2 = i + 1;
            _m_fields = arzArr;
            C1634Xv.m11725a((Object[]) ShipAdapter._m_fields, (Object[]) _m_fields);
            int i3 = ShipAdapter._m_methodCount;
            C2491fm[] fmVarArr = new C2491fm[(i3 + 4)];
            C2491fm a = C4105zY.m41624a(ShipBoost.class, "29c40d765242a9614c18387e27085d60", i3);
            aTe = a;
            fmVarArr[i3] = a;
            int i4 = i3 + 1;
            C2491fm a2 = C4105zY.m41624a(ShipBoost.class, "1dfb3298d398f1350f95ed93f1644cec", i4);
            aTf = a2;
            fmVarArr[i4] = a2;
            int i5 = i4 + 1;
            C2491fm a3 = C4105zY.m41624a(ShipBoost.class, "f65ced13b7e4e00701874d29617b899f", i5);
            aTg = a3;
            fmVarArr[i5] = a3;
            int i6 = i5 + 1;
            C2491fm a4 = C4105zY.m41624a(ShipBoost.class, "70380c9df160960e8bd543e722720b6a", i6);
            aTh = a4;
            fmVarArr[i6] = a4;
            int i7 = i6 + 1;
            _m_methods = fmVarArr;
            C1634Xv.m11725a((Object[]) ShipAdapter._m_methods, (Object[]) _m_methods);
            ___iScriptClass = aUO.m18566a(ShipBoost.class, C5753aVl.class, _m_fields, _m_methods);
        }

        /* renamed from: VD */
        private Fighter m37008VD() {
            return (Fighter) bFf().mo5608dq().mo3214p(f8814aT);
        }

        /* renamed from: a */
        private void m37013a(Fighter pFVar) {
            bFf().mo5608dq().mo3197f(f8814aT, pFVar);
        }

        /* renamed from: S */
        public void mo10S() {
            super.mo10S();
        }

        /* renamed from: T */
        public C6494any mo11T() {
            return ___iScriptClass;
        }

        /* renamed from: U */
        public C2491fm[] mo12U() {
            return _m_methods;
        }

        /* renamed from: VF */
        public float mo5665VF() {
            switch (bFf().mo6893i(aTe)) {
                case 0:
                    return 0.0f;
                case 2:
                    return ((Float) bFf().mo5606d(new aCE(this, aTe, new Object[0]))).floatValue();
                case 3:
                    bFf().mo5606d(new aCE(this, aTe, new Object[0]));
                    break;
            }
            return m37009VE();
        }

        /* renamed from: VH */
        public float mo5666VH() {
            switch (bFf().mo6893i(aTf)) {
                case 0:
                    return 0.0f;
                case 2:
                    return ((Float) bFf().mo5606d(new aCE(this, aTf, new Object[0]))).floatValue();
                case 3:
                    bFf().mo5606d(new aCE(this, aTf, new Object[0]));
                    break;
            }
            return m37010VG();
        }

        /* renamed from: W */
        public Object mo13W() {
            return new C5753aVl(this);
        }

        /* renamed from: a */
        public Object mo14a(C0495Gr gr) {
            gr.getArgs();
            switch (gr.mo2417hq() - ShipAdapter._m_methodCount) {
                case 0:
                    return new Float(m37009VE());
                case 1:
                    return new Float(m37010VG());
                case 2:
                    return new Float(m37011VI());
                case 3:
                    return new Float(m37012VJ());
                default:
                    return super.mo14a(gr);
            }
        }

        /* renamed from: a */
        public void mo15a(Collection collection, C0495Gr gr) {
            super.mo15a(collection, gr);
        }

        /* renamed from: c */
        public C5663aRz[] mo25c() {
            return _m_fields;
        }

        /* renamed from: ra */
        public float mo5668ra() {
            switch (bFf().mo6893i(aTh)) {
                case 0:
                    return 0.0f;
                case 2:
                    return ((Float) bFf().mo5606d(new aCE(this, aTh, new Object[0]))).floatValue();
                case 3:
                    bFf().mo5606d(new aCE(this, aTh, new Object[0]));
                    break;
            }
            return m37012VJ();
        }

        /* renamed from: rb */
        public float mo5669rb() {
            switch (bFf().mo6893i(aTg)) {
                case 0:
                    return 0.0f;
                case 2:
                    return ((Float) bFf().mo5606d(new aCE(this, aTg, new Object[0]))).floatValue();
                case 3:
                    bFf().mo5606d(new aCE(this, aTg, new Object[0]));
                    break;
            }
            return m37011VI();
        }

        /* renamed from: b */
        public void mo21117b(Fighter pFVar) {
            m37013a(pFVar);
            super.mo10S();
        }

        @C0064Am(aul = "29c40d765242a9614c18387e27085d60", aum = 0)
        /* renamed from: VE */
        private float m37009VE() {
            return 0.0f;
        }

        @C0064Am(aul = "1dfb3298d398f1350f95ed93f1644cec", aum = 0)
        /* renamed from: VG */
        private float m37010VG() {
            return 0.0f;
        }

        @C0064Am(aul = "f65ced13b7e4e00701874d29617b899f", aum = 0)
        /* renamed from: VI */
        private float m37011VI() {
            return 0.0f;
        }

        @C0064Am(aul = "70380c9df160960e8bd543e722720b6a", aum = 0)
        /* renamed from: VJ */
        private float m37012VJ() {
            return 0.0f;
        }
    }
}
