package game.script.ship.categories;

import game.network.message.externalizable.aCE;
import game.script.ship.strike.StrikeType;
import logic.baa.*;
import logic.data.mbean.C2908lg;
import logic.res.code.C5640aRc;
import logic.res.code.C5663aRz;
import logic.res.code.C6494any;
import logic.res.html.C2491fm;
import p001a.*;

import java.util.Collection;

@C0566Hp
@C5829abJ("1.0.0")
@C6485anp
@C5511aMd
/* renamed from: a.hv */
/* compiled from: a */
public class BomberType extends BomberBaseType implements C1616Xf {
    /* renamed from: UE */
    public static final C5663aRz f8077UE = null;
    /* renamed from: UF */
    public static final C2491fm f8078UF = null;
    /* renamed from: UG */
    public static final C2491fm f8079UG = null;
    /* renamed from: UH */
    public static final C2491fm f8080UH = null;
    public static final int _m_fieldCount = 0;
    public static final C5663aRz[] _m_fields = null;
    public static final int _m_methodCount = 0;
    public static final C2491fm[] _m_methods = null;
    /* renamed from: dN */
    public static final C2491fm f8081dN = null;
    public static final long serialVersionUID = 0;
    public static C6494any ___iScriptClass;
    @C0064Am(aul = "d400a84483edbad9e108996713e28041", aum = 0)

    /* renamed from: UD */
    private static StrikeType f8076UD;

    static {
        m33006V();
    }

    public BomberType() {
        super((C5540aNg) null);
        super.mo10S();
    }

    public BomberType(C5540aNg ang) {
        super(ang);
    }

    /* renamed from: V */
    static void m33006V() {
        serialVersionUID = (long) 1;
        _m_fieldCount = BomberBaseType._m_fieldCount + 1;
        _m_methodCount = BomberBaseType._m_methodCount + 4;
        int i = BomberBaseType._m_fieldCount;
        C5663aRz[] arzArr = new C5663aRz[(i + 1)];
        C5663aRz b = C5640aRc.m17844b(BomberType.class, "d400a84483edbad9e108996713e28041", i);
        f8077UE = b;
        arzArr[i] = b;
        int i2 = i + 1;
        _m_fields = arzArr;
        C1634Xv.m11725a((Object[]) BomberBaseType._m_fields, (Object[]) _m_fields);
        int i3 = BomberBaseType._m_methodCount;
        C2491fm[] fmVarArr = new C2491fm[(i3 + 4)];
        C2491fm a = C4105zY.m41624a(BomberType.class, "25db2e209a50c26abe5c8b98b82eb453", i3);
        f8078UF = a;
        fmVarArr[i3] = a;
        int i4 = i3 + 1;
        C2491fm a2 = C4105zY.m41624a(BomberType.class, "aa7730b118fa3664913f53cf0c9a5481", i4);
        f8079UG = a2;
        fmVarArr[i4] = a2;
        int i5 = i4 + 1;
        C2491fm a3 = C4105zY.m41624a(BomberType.class, "343c16d022abb05433e2f7f0b7716652", i5);
        f8080UH = a3;
        fmVarArr[i5] = a3;
        int i6 = i5 + 1;
        C2491fm a4 = C4105zY.m41624a(BomberType.class, "5e5f1279ea5c3a48fa26391c82972f6a", i6);
        f8081dN = a4;
        fmVarArr[i6] = a4;
        int i7 = i6 + 1;
        _m_methods = fmVarArr;
        C1634Xv.m11725a((Object[]) BomberBaseType._m_methods, (Object[]) _m_methods);
        ___iScriptClass = aUO.m18566a(BomberType.class, C2908lg.class, _m_fields, _m_methods);
    }

    /* renamed from: a */
    private void m33007a(StrikeType add) {
        bFf().mo5608dq().mo3197f(f8077UE, add);
    }

    /* renamed from: ys */
    private StrikeType m33010ys() {
        return (StrikeType) bFf().mo5608dq().mo3214p(f8077UE);
    }

    /* renamed from: T */
    public C6494any mo11T() {
        return ___iScriptClass;
    }

    /* renamed from: U */
    public C2491fm[] mo12U() {
        return _m_methods;
    }

    /* renamed from: W */
    public Object mo13W() {
        return new C2908lg(this);
    }

    /* renamed from: a */
    public Object mo14a(C0495Gr gr) {
        Object[] args = gr.getArgs();
        switch (gr.mo2417hq() - BomberBaseType._m_methodCount) {
            case 0:
                return m33011yt();
            case 1:
                m33009b((StrikeType) args[0]);
                return null;
            case 2:
                return m33012yv();
            case 3:
                return m33008aT();
            default:
                return super.mo14a(gr);
        }
    }

    /* renamed from: a */
    public void mo15a(Collection collection, C0495Gr gr) {
        super.mo15a(collection, gr);
    }

    /* access modifiers changed from: protected */
    /* renamed from: aU */
    public <T extends aDJ> T mo745aU() {
        switch (bFf().mo6893i(f8081dN)) {
            case 0:
                return null;
            case 2:
                return (aDJ) bFf().mo5606d(new aCE(this, f8081dN, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f8081dN, new Object[0]));
                break;
        }
        return m33008aT();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Omega Strike Type")
    /* renamed from: c */
    public void mo19421c(StrikeType add) {
        switch (bFf().mo6893i(f8079UG)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f8079UG, new Object[]{add}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f8079UG, new Object[]{add}));
                break;
        }
        m33009b(add);
    }

    /* renamed from: c */
    public C5663aRz[] mo25c() {
        return _m_fields;
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Omega Strike Type")
    /* renamed from: yu */
    public StrikeType mo19422yu() {
        switch (bFf().mo6893i(f8078UF)) {
            case 0:
                return null;
            case 2:
                return (StrikeType) bFf().mo5606d(new aCE(this, f8078UF, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f8078UF, new Object[0]));
                break;
        }
        return m33011yt();
    }

    /* renamed from: yw */
    public Bomber mo19423yw() {
        switch (bFf().mo6893i(f8080UH)) {
            case 0:
                return null;
            case 2:
                return (Bomber) bFf().mo5606d(new aCE(this, f8080UH, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f8080UH, new Object[0]));
                break;
        }
        return m33012yv();
    }

    /* renamed from: S */
    public void mo10S() {
        super.mo10S();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Omega Strike Type")
    @C0064Am(aul = "25db2e209a50c26abe5c8b98b82eb453", aum = 0)
    /* renamed from: yt */
    private StrikeType m33011yt() {
        return m33010ys();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Omega Strike Type")
    @C0064Am(aul = "aa7730b118fa3664913f53cf0c9a5481", aum = 0)
    /* renamed from: b */
    private void m33009b(StrikeType add) {
        m33007a(add);
    }

    @C0064Am(aul = "343c16d022abb05433e2f7f0b7716652", aum = 0)
    /* renamed from: yv */
    private Bomber m33012yv() {
        return (Bomber) mo745aU();
    }

    @C0064Am(aul = "5e5f1279ea5c3a48fa26391c82972f6a", aum = 0)
    /* renamed from: aT */
    private <T extends aDJ> T m33008aT() {
        T t = (Bomber) bFf().mo6865M(Bomber.class);
        t.mo9244a(this);
        return t;
    }
}
