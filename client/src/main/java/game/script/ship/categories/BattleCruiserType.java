package game.script.ship.categories;

import game.network.message.externalizable.aCE;
import game.script.ship.ShipType;
import logic.baa.*;
import logic.data.mbean.C6949awo;
import logic.res.code.C5663aRz;
import logic.res.code.C6494any;
import logic.res.html.C2491fm;
import p001a.*;

import java.util.Collection;

@C0566Hp
@C5829abJ("1.0.0")
@C6485anp
@C5511aMd
/* renamed from: a.auS  reason: case insensitive filesystem */
/* compiled from: a */
public class BattleCruiserType extends ShipType implements C1616Xf {
    public static final int _m_fieldCount = 0;
    public static final C5663aRz[] _m_fields = null;
    public static final int _m_methodCount = 0;
    public static final C2491fm[] _m_methods = null;
    /* renamed from: dN */
    public static final C2491fm f5385dN = null;
    public static final C2491fm gFq = null;
    public static final long serialVersionUID = 0;
    public static C6494any ___iScriptClass;

    static {
        m26317V();
    }

    public BattleCruiserType() {
        super((C5540aNg) null);
        super.mo10S();
    }

    public BattleCruiserType(C5540aNg ang) {
        super(ang);
    }

    /* renamed from: V */
    static void m26317V() {
        serialVersionUID = (long) 1;
        _m_fieldCount = ShipType._m_fieldCount + 0;
        _m_methodCount = ShipType._m_methodCount + 2;
        _m_fields = new C5663aRz[(ShipType._m_fieldCount + 0)];
        C1634Xv.m11725a((Object[]) ShipType._m_fields, (Object[]) _m_fields);
        int i = ShipType._m_methodCount;
        C2491fm[] fmVarArr = new C2491fm[(i + 2)];
        C2491fm a = C4105zY.m41624a(BattleCruiserType.class, "87b5c40593e1ef97f377e94aa8105411", i);
        gFq = a;
        fmVarArr[i] = a;
        int i2 = i + 1;
        C2491fm a2 = C4105zY.m41624a(BattleCruiserType.class, "c7cc775d14fb8b7667a3e207879cc04d", i2);
        f5385dN = a2;
        fmVarArr[i2] = a2;
        int i3 = i2 + 1;
        _m_methods = fmVarArr;
        C1634Xv.m11725a((Object[]) ShipType._m_methods, (Object[]) _m_methods);
        ___iScriptClass = aUO.m18566a(BattleCruiserType.class, C6949awo.class, _m_fields, _m_methods);
    }

    /* renamed from: T */
    public C6494any mo11T() {
        return ___iScriptClass;
    }

    /* renamed from: U */
    public C2491fm[] mo12U() {
        return _m_methods;
    }

    /* renamed from: W */
    public Object mo13W() {
        return new C6949awo(this);
    }

    /* renamed from: a */
    public Object mo14a(C0495Gr gr) {
        gr.getArgs();
        switch (gr.mo2417hq() - ShipType._m_methodCount) {
            case 0:
                return cyx();
            case 1:
                return m26318aT();
            default:
                return super.mo14a(gr);
        }
    }

    /* renamed from: a */
    public void mo15a(Collection collection, C0495Gr gr) {
        super.mo15a(collection, gr);
    }

    /* access modifiers changed from: protected */
    /* renamed from: aU */
    public <T extends aDJ> T mo745aU() {
        switch (bFf().mo6893i(f5385dN)) {
            case 0:
                return null;
            case 2:
                return (aDJ) bFf().mo5606d(new aCE(this, f5385dN, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f5385dN, new Object[0]));
                break;
        }
        return m26318aT();
    }

    /* renamed from: c */
    public C5663aRz[] mo25c() {
        return _m_fields;
    }

    public BattleCruiser cyy() {
        switch (bFf().mo6893i(gFq)) {
            case 0:
                return null;
            case 2:
                return (BattleCruiser) bFf().mo5606d(new aCE(this, gFq, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, gFq, new Object[0]));
                break;
        }
        return cyx();
    }

    /* renamed from: S */
    public void mo10S() {
        super.mo10S();
    }

    @C0064Am(aul = "87b5c40593e1ef97f377e94aa8105411", aum = 0)
    private BattleCruiser cyx() {
        return (BattleCruiser) mo745aU();
    }

    @C0064Am(aul = "c7cc775d14fb8b7667a3e207879cc04d", aum = 0)
    /* renamed from: aT */
    private <T extends aDJ> T m26318aT() {
        T t = (BattleCruiser) bFf().mo6865M(BattleCruiser.class);
        t.mo20754a(this);
        return t;
    }
}
