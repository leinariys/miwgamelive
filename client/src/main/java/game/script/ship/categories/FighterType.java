package game.script.ship.categories;

import game.network.message.externalizable.aCE;
import game.script.ship.ShipType;
import game.script.ship.speedBoost.SpeedBoostType;
import logic.baa.*;
import logic.data.mbean.C6827auT;
import logic.res.code.C5640aRc;
import logic.res.code.C5663aRz;
import logic.res.code.C6494any;
import logic.res.html.C2491fm;
import p001a.*;

import java.util.Collection;

@C0566Hp
@C5829abJ("1.0.0")
@C6485anp
@C5511aMd
/* renamed from: a.aML */
/* compiled from: a */
public class FighterType extends ShipType implements C1616Xf {
    public static final int _m_fieldCount = 0;
    public static final C5663aRz[] _m_fields = null;
    public static final int _m_methodCount = 0;
    public static final C2491fm[] _m_methods = null;
    public static final C5663aRz dDG = null;
    /* renamed from: dN */
    public static final C2491fm f3352dN = null;
    public static final C2491fm ipZ = null;
    public static final C2491fm iqa = null;
    public static final C2491fm iqb = null;
    public static final long serialVersionUID = 0;
    public static C6494any ___iScriptClass;
    @C0064Am(aul = "17c794981d0e9896cfb7bb55d697dafd", aum = 0)
    private static SpeedBoostType dDF;

    static {
        m16350V();
    }

    public FighterType() {
        super((C5540aNg) null);
        super.mo10S();
    }

    public FighterType(C5540aNg ang) {
        super(ang);
    }

    /* renamed from: V */
    static void m16350V() {
        serialVersionUID = (long) 1;
        _m_fieldCount = ShipType._m_fieldCount + 1;
        _m_methodCount = ShipType._m_methodCount + 4;
        int i = ShipType._m_fieldCount;
        C5663aRz[] arzArr = new C5663aRz[(i + 1)];
        C5663aRz b = C5640aRc.m17844b(FighterType.class, "17c794981d0e9896cfb7bb55d697dafd", i);
        dDG = b;
        arzArr[i] = b;
        int i2 = i + 1;
        _m_fields = arzArr;
        C1634Xv.m11725a((Object[]) ShipType._m_fields, (Object[]) _m_fields);
        int i3 = ShipType._m_methodCount;
        C2491fm[] fmVarArr = new C2491fm[(i3 + 4)];
        C2491fm a = C4105zY.m41624a(FighterType.class, "7127bd6334d0bb6ca9fd9727e423261d", i3);
        ipZ = a;
        fmVarArr[i3] = a;
        int i4 = i3 + 1;
        C2491fm a2 = C4105zY.m41624a(FighterType.class, "74fb1eef4dd4278eef93035a292f5e76", i4);
        iqa = a2;
        fmVarArr[i4] = a2;
        int i5 = i4 + 1;
        C2491fm a3 = C4105zY.m41624a(FighterType.class, "c9c7c7638dfb6e3ea304e97a37e447ac", i5);
        iqb = a3;
        fmVarArr[i5] = a3;
        int i6 = i5 + 1;
        C2491fm a4 = C4105zY.m41624a(FighterType.class, "59e94e3a10eadf838b9e8ec8b2ed76eb", i6);
        f3352dN = a4;
        fmVarArr[i6] = a4;
        int i7 = i6 + 1;
        _m_methods = fmVarArr;
        C1634Xv.m11725a((Object[]) ShipType._m_methods, (Object[]) _m_methods);
        ___iScriptClass = aUO.m18566a(FighterType.class, C6827auT.class, _m_fields, _m_methods);
    }

    /* renamed from: a */
    private void m16351a(SpeedBoostType lJVar) {
        bFf().mo5608dq().mo3197f(dDG, lJVar);
    }

    private SpeedBoostType biz() {
        return (SpeedBoostType) bFf().mo5608dq().mo3214p(dDG);
    }

    /* renamed from: T */
    public C6494any mo11T() {
        return ___iScriptClass;
    }

    /* renamed from: U */
    public C2491fm[] mo12U() {
        return _m_methods;
    }

    /* renamed from: W */
    public Object mo13W() {
        return new C6827auT(this);
    }

    /* renamed from: a */
    public Object mo14a(C0495Gr gr) {
        Object[] args = gr.getArgs();
        switch (gr.mo2417hq() - ShipType._m_methodCount) {
            case 0:
                return djB();
            case 1:
                m16353f((SpeedBoostType) args[0]);
                return null;
            case 2:
                return djD();
            case 3:
                return m16352aT();
            default:
                return super.mo14a(gr);
        }
    }

    /* renamed from: a */
    public void mo15a(Collection collection, C0495Gr gr) {
        super.mo15a(collection, gr);
    }

    /* access modifiers changed from: protected */
    /* renamed from: aU */
    public <T extends aDJ> T mo745aU() {
        switch (bFf().mo6893i(f3352dN)) {
            case 0:
                return null;
            case 2:
                return (aDJ) bFf().mo5606d(new aCE(this, f3352dN, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f3352dN, new Object[0]));
                break;
        }
        return m16352aT();
    }

    /* renamed from: c */
    public C5663aRz[] mo25c() {
        return _m_fields;
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Speed Boost Type")
    public SpeedBoostType djC() {
        switch (bFf().mo6893i(ipZ)) {
            case 0:
                return null;
            case 2:
                return (SpeedBoostType) bFf().mo5606d(new aCE(this, ipZ, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, ipZ, new Object[0]));
                break;
        }
        return djB();
    }

    public Fighter djE() {
        switch (bFf().mo6893i(iqb)) {
            case 0:
                return null;
            case 2:
                return (Fighter) bFf().mo5606d(new aCE(this, iqb, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, iqb, new Object[0]));
                break;
        }
        return djD();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Speed Boost Type")
    /* renamed from: g */
    public void mo10029g(SpeedBoostType lJVar) {
        switch (bFf().mo6893i(iqa)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, iqa, new Object[]{lJVar}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, iqa, new Object[]{lJVar}));
                break;
        }
        m16353f(lJVar);
    }

    /* renamed from: S */
    public void mo10S() {
        super.mo10S();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Speed Boost Type")
    @C0064Am(aul = "7127bd6334d0bb6ca9fd9727e423261d", aum = 0)
    private SpeedBoostType djB() {
        return biz();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Speed Boost Type")
    @C0064Am(aul = "74fb1eef4dd4278eef93035a292f5e76", aum = 0)
    /* renamed from: f */
    private void m16353f(SpeedBoostType lJVar) {
        m16351a(lJVar);
    }

    @C0064Am(aul = "c9c7c7638dfb6e3ea304e97a37e447ac", aum = 0)
    private Fighter djD() {
        return (Fighter) mo745aU();
    }

    @C0064Am(aul = "59e94e3a10eadf838b9e8ec8b2ed76eb", aum = 0)
    /* renamed from: aT */
    private <T extends aDJ> T m16352aT() {
        T t = (Fighter) bFf().mo6865M(Fighter.class);
        t.mo21109a(this);
        return t;
    }
}
