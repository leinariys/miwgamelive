package game.script.ship.categories;

import game.network.message.externalizable.aCE;
import game.script.damage.DamageType;
import game.script.ship.*;
import game.script.ship.hazardshield.HazardShield;
import game.script.ship.hazardshield.HazardShieldType;
import logic.baa.*;
import logic.data.mbean.*;
import logic.res.code.C5640aRc;
import logic.res.code.C5663aRz;
import logic.res.code.C6494any;
import logic.res.html.C2491fm;
import p001a.*;

import java.util.Collection;

@C5829abJ("1.1.0")
@C6485anp
@aEZ("Explorer")
@C2712iu(version = "1.1.0")
@C5511aMd
/* renamed from: a.fY */
/* compiled from: a */
public class Explorer extends Ship implements C1616Xf, aOW {

    /* renamed from: Lm */
    public static final C2491fm f7382Lm = null;

    /* renamed from: Wp */
    public static final C2491fm f7383Wp = null;
    public static final C2491fm _f_dispose_0020_0028_0029V = null;
    public static final int _m_fieldCount = 0;
    public static final C5663aRz[] _m_fields = null;
    public static final int _m_methodCount = 0;
    public static final C2491fm[] _m_methods = null;
    public static final C5663aRz aZD = null;
    public static final C2491fm bka = null;
    public static final C2491fm bkd = null;
    public static final C2491fm dDO = null;
    public static final C5663aRz goe = null;
    public static final C2491fm gof = null;
    public static final long serialVersionUID = 0;
    public static C6494any ___iScriptClass;
    @C0064Am(aul = "880a24dfdb98e6369e894e8e16c65f9f", aum = 0)
    private static HazardShieldType aZC;
    @C0064Am(aul = "abea0f8c30ab08e80cf4cea7bc235ba6", aum = 1)
    private static HazardShield eex;

    static {
        m31095V();
    }

    public Explorer() {
        super((C5540aNg) null);
        super.mo10S();
    }

    public Explorer(C5540aNg ang) {
        super(ang);
    }

    public Explorer(ExplorerType rdVar) {
        super((C5540aNg) null);
        super._m_script_init(rdVar);
    }

    /* renamed from: V */
    static void m31095V() {
        serialVersionUID = (long) 1;
        _m_fieldCount = Ship._m_fieldCount + 2;
        _m_methodCount = Ship._m_methodCount + 7;
        int i = Ship._m_fieldCount;
        C5663aRz[] arzArr = new C5663aRz[(i + 2)];
        C5663aRz b = C5640aRc.m17844b(Explorer.class, "880a24dfdb98e6369e894e8e16c65f9f", i);
        aZD = b;
        arzArr[i] = b;
        int i2 = i + 1;
        C5663aRz b2 = C5640aRc.m17844b(Explorer.class, "abea0f8c30ab08e80cf4cea7bc235ba6", i2);
        goe = b2;
        arzArr[i2] = b2;
        int i3 = i2 + 1;
        _m_fields = arzArr;
        C1634Xv.m11725a((Object[]) Ship._m_fields, (Object[]) _m_fields);
        int i4 = Ship._m_methodCount;
        C2491fm[] fmVarArr = new C2491fm[(i4 + 7)];
        C2491fm a = C4105zY.m41624a(Explorer.class, "09e09560def832c5ccb3f2bcf4bca240", i4);
        bka = a;
        fmVarArr[i4] = a;
        int i5 = i4 + 1;
        C2491fm a2 = C4105zY.m41624a(Explorer.class, "15d1094a7845466943069e642f02f687", i5);
        bkd = a2;
        fmVarArr[i5] = a2;
        int i6 = i5 + 1;
        C2491fm a3 = C4105zY.m41624a(Explorer.class, "74322a8282d05c5e3e31b1994e29fc79", i6);
        _f_dispose_0020_0028_0029V = a3;
        fmVarArr[i6] = a3;
        int i7 = i6 + 1;
        C2491fm a4 = C4105zY.m41624a(Explorer.class, "fddecafef9a1e77f09da258f226bbea5", i7);
        f7382Lm = a4;
        fmVarArr[i7] = a4;
        int i8 = i7 + 1;
        C2491fm a5 = C4105zY.m41624a(Explorer.class, "e19ce61b62669228c54f504ea2264f59", i8);
        f7383Wp = a5;
        fmVarArr[i8] = a5;
        int i9 = i8 + 1;
        C2491fm a6 = C4105zY.m41624a(Explorer.class, "0d0fe1ef24cb232c89c62789039da913", i9);
        gof = a6;
        fmVarArr[i9] = a6;
        int i10 = i9 + 1;
        C2491fm a7 = C4105zY.m41624a(Explorer.class, "253f147389a4e944b0f24cfa740d11e1", i10);
        dDO = a7;
        fmVarArr[i10] = a7;
        int i11 = i10 + 1;
        _m_methods = fmVarArr;
        C1634Xv.m11725a((Object[]) Ship._m_methods, (Object[]) _m_methods);
        ___iScriptClass = aUO.m18566a(Explorer.class, C1290Sx.class, _m_fields, _m_methods);
    }

    /* renamed from: Yh */
    private HazardShieldType m31096Yh() {
        return ((ExplorerType) getType()).mo21696Yj();
    }

    /* renamed from: a */
    private void m31097a(HazardShieldType rGVar) {
        throw new C6039afL();
    }

    @C4034yP
    @C5566aOg
    @C0064Am(aul = "09e09560def832c5ccb3f2bcf4bca240", aum = 0)
    @C2499fr
    private void acK() {
        throw new aWi(new aCE(this, bka, new Object[0]));
    }

    @C4034yP
    @C5566aOg
    @C0064Am(aul = "15d1094a7845466943069e642f02f687", aum = 0)
    @C2499fr
    private void acP() {
        throw new aWi(new aCE(this, bkd, new Object[0]));
    }

    /* renamed from: c */
    private void m31098c(HazardShield caVar) {
        bFf().mo5608dq().mo3197f(goe, caVar);
    }

    private HazardShield cqt() {
        return (HazardShield) bFf().mo5608dq().mo3214p(goe);
    }

    @C0064Am(aul = "fddecafef9a1e77f09da258f226bbea5", aum = 0)
    @C5566aOg
    @C2499fr
    /* renamed from: qU */
    private void m31100qU() {
        throw new aWi(new aCE(this, f7382Lm, new Object[0]));
    }

    /* renamed from: S */
    public void mo10S() {
        super.mo10S();
    }

    /* renamed from: T */
    public C6494any mo11T() {
        return ___iScriptClass;
    }

    /* renamed from: U */
    public C2491fm[] mo12U() {
        return _m_methods;
    }

    /* renamed from: W */
    public Object mo13W() {
        return new C1290Sx(this);
    }

    /* renamed from: a */
    public Object mo14a(C0495Gr gr) {
        gr.getArgs();
        switch (gr.mo2417hq() - Ship._m_methodCount) {
            case 0:
                acK();
                return null;
            case 1:
                acP();
                return null;
            case 2:
                m31099fg();
                return null;
            case 3:
                m31100qU();
                return null;
            case 4:
                m31101zw();
                return null;
            case 5:
                return cqu();
            case 6:
                return new Boolean(biK());
            default:
                return super.mo14a(gr);
        }
    }

    /* renamed from: a */
    public void mo15a(Collection collection, C0495Gr gr) {
        super.mo15a(collection, gr);
    }

    @C4034yP
    @C5566aOg
    @C2499fr
    public void abort() {
        switch (bFf().mo6893i(bkd)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, bkd, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, bkd, new Object[0]));
                break;
        }
        acP();
    }

    @C4034yP
    @C5566aOg
    @C2499fr
    public void activate() {
        switch (bFf().mo6893i(bka)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, bka, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, bka, new Object[0]));
                break;
        }
        acK();
    }

    public boolean biL() {
        switch (bFf().mo6893i(dDO)) {
            case 0:
                return false;
            case 2:
                return ((Boolean) bFf().mo5606d(new aCE(this, dDO, new Object[0]))).booleanValue();
            case 3:
                bFf().mo5606d(new aCE(this, dDO, new Object[0]));
                break;
        }
        return biK();
    }

    /* renamed from: c */
    public C5663aRz[] mo25c() {
        return _m_fields;
    }

    public HazardShield cqv() {
        switch (bFf().mo6893i(gof)) {
            case 0:
                return null;
            case 2:
                return (HazardShield) bFf().mo5606d(new aCE(this, gof, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, gof, new Object[0]));
                break;
        }
        return cqu();
    }

    public void dispose() {
        switch (bFf().mo6893i(_f_dispose_0020_0028_0029V)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, _f_dispose_0020_0028_0029V, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, _f_dispose_0020_0028_0029V, new Object[0]));
                break;
        }
        m31099fg();
    }

    @C5566aOg
    @C2499fr
    /* renamed from: qV */
    public void mo656qV() {
        switch (bFf().mo6893i(f7382Lm)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f7382Lm, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f7382Lm, new Object[0]));
                break;
        }
        m31100qU();
    }

    /* renamed from: zx */
    public void mo1099zx() {
        switch (bFf().mo6893i(f7383Wp)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f7383Wp, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f7383Wp, new Object[0]));
                break;
        }
        m31101zw();
    }

    /* renamed from: a */
    public void mo18701a(ExplorerType rdVar) {
        super.mo18318h((ShipType) rdVar);
        HazardShieldType Yh = m31096Yh();
        if (m31096Yh() == null) {
            Yh = ala().aJe().mo19007xH().cUZ();
        }
        m31098c(Yh.mo21567YI());
        cqt().mo17634ao(this);
    }

    @C0064Am(aul = "74322a8282d05c5e3e31b1994e29fc79", aum = 0)
    /* renamed from: fg */
    private void m31099fg() {
        super.dispose();
        if (cqt() != null) {
            cqt().dispose();
            m31098c((HazardShield) null);
        }
    }

    @C0064Am(aul = "e19ce61b62669228c54f504ea2264f59", aum = 0)
    /* renamed from: zw */
    private void m31101zw() {
        super.mo1099zx();
        if (cqt() != null) {
            cqt().mo17645zx();
        }
    }

    @C0064Am(aul = "0d0fe1ef24cb232c89c62789039da913", aum = 0)
    private HazardShield cqu() {
        return cqt();
    }

    @C0064Am(aul = "253f147389a4e944b0f24cfa740d11e1", aum = 0)
    private boolean biK() {
        return true;
    }

    /* renamed from: a.fY$b */
    /* compiled from: a */
    public enum C2447b {
        WARMIN_UP,
        COOLING_DOWN,
        READY,
        ACTIVE
    }

    @C6485anp
    @C5511aMd
    @Deprecated
    /* renamed from: a.fY$e */
    /* compiled from: a */
    public class ShieldBlock extends ShieldAdapter implements C1616Xf {
        public static final int _m_fieldCount = 0;
        public static final C5663aRz[] _m_fields = null;
        public static final int _m_methodCount = 0;
        public static final C2491fm[] _m_methods = null;
        /* renamed from: aT */
        public static final C5663aRz f7393aT = null;
        /* renamed from: qa */
        public static final C2491fm f7394qa = null;
        /* renamed from: qb */
        public static final C2491fm f7395qb = null;
        public static final long serialVersionUID = 0;
        public static C6494any ___iScriptClass;
        @C0064Am(aul = "bace11141ddf226745bcb5b3309d3cc3", aum = 0)

        /* renamed from: OD */
        static /* synthetic */ Explorer f7392OD;

        static {
            m31149V();
        }

        public ShieldBlock() {
            super((C5540aNg) null);
            super.mo10S();
        }

        public ShieldBlock(C5540aNg ang) {
            super(ang);
        }

        public ShieldBlock(Explorer fYVar) {
            super((C5540aNg) null);
            super._m_script_init(fYVar);
        }

        /* renamed from: V */
        static void m31149V() {
            serialVersionUID = (long) 1;
            _m_fieldCount = ShieldAdapter._m_fieldCount + 1;
            _m_methodCount = ShieldAdapter._m_methodCount + 2;
            int i = ShieldAdapter._m_fieldCount;
            C5663aRz[] arzArr = new C5663aRz[(i + 1)];
            C5663aRz b = C5640aRc.m17844b(ShieldBlock.class, "bace11141ddf226745bcb5b3309d3cc3", i);
            f7393aT = b;
            arzArr[i] = b;
            int i2 = i + 1;
            _m_fields = arzArr;
            C1634Xv.m11725a((Object[]) ShieldAdapter._m_fields, (Object[]) _m_fields);
            int i3 = ShieldAdapter._m_methodCount;
            C2491fm[] fmVarArr = new C2491fm[(i3 + 2)];
            C2491fm a = C4105zY.m41624a(ShieldBlock.class, "73ea3dea0c50cb5cbfccdf123114dee6", i3);
            f7394qa = a;
            fmVarArr[i3] = a;
            int i4 = i3 + 1;
            C2491fm a2 = C4105zY.m41624a(ShieldBlock.class, "47ee0d7d8847c80f356eeb2b2bfea613", i4);
            f7395qb = a2;
            fmVarArr[i4] = a2;
            int i5 = i4 + 1;
            _m_methods = fmVarArr;
            C1634Xv.m11725a((Object[]) ShieldAdapter._m_methods, (Object[]) _m_methods);
            ___iScriptClass = aUO.m18566a(ShieldBlock.class, C0898NC.class, _m_fields, _m_methods);
        }

        /* renamed from: a */
        private void m31151a(Explorer fYVar) {
            bFf().mo5608dq().mo3197f(f7393aT, fYVar);
        }

        /* renamed from: uH */
        private Explorer m31153uH() {
            return (Explorer) bFf().mo5608dq().mo3214p(f7393aT);
        }

        /* renamed from: S */
        public void mo10S() {
            super.mo10S();
        }

        /* renamed from: T */
        public C6494any mo11T() {
            return ___iScriptClass;
        }

        /* renamed from: U */
        public C2491fm[] mo12U() {
            return _m_methods;
        }

        /* renamed from: W */
        public Object mo13W() {
            return new C0898NC(this);
        }

        /* renamed from: a */
        public Object mo14a(C0495Gr gr) {
            Object[] args = gr.getArgs();
            switch (gr.mo2417hq() - ShieldAdapter._m_methodCount) {
                case 0:
                    return new Float(m31150a((DamageType) args[0], ((Float) args[1]).floatValue()));
                case 1:
                    return new Float(m31152hg());
                default:
                    return super.mo14a(gr);
            }
        }

        /* renamed from: a */
        public void mo15a(Collection collection, C0495Gr gr) {
            super.mo15a(collection, gr);
        }

        /* renamed from: b */
        public float mo3759b(DamageType fr, float f) {
            switch (bFf().mo6893i(f7394qa)) {
                case 0:
                    return 0.0f;
                case 2:
                    return ((Float) bFf().mo5606d(new aCE(this, f7394qa, new Object[]{fr, new Float(f)}))).floatValue();
                case 3:
                    bFf().mo5606d(new aCE(this, f7394qa, new Object[]{fr, new Float(f)}));
                    break;
            }
            return m31150a(fr, f);
        }

        /* renamed from: c */
        public C5663aRz[] mo25c() {
            return _m_fields;
        }

        /* renamed from: hh */
        public float mo3761hh() {
            switch (bFf().mo6893i(f7395qb)) {
                case 0:
                    return 0.0f;
                case 2:
                    return ((Float) bFf().mo5606d(new aCE(this, f7395qb, new Object[0]))).floatValue();
                case 3:
                    bFf().mo5606d(new aCE(this, f7395qb, new Object[0]));
                    break;
            }
            return m31152hg();
        }

        /* renamed from: b */
        public void mo18708b(Explorer fYVar) {
            m31151a(fYVar);
            super.mo10S();
        }

        @C0064Am(aul = "73ea3dea0c50cb5cbfccdf123114dee6", aum = 0)
        /* renamed from: a */
        private float m31150a(DamageType fr, float f) {
            return 0.0f;
        }

        @C0064Am(aul = "47ee0d7d8847c80f356eeb2b2bfea613", aum = 0)
        /* renamed from: hg */
        private float m31152hg() {
            return 0.0f;
        }
    }

    @C6485anp
    @C5511aMd
    @Deprecated
    /* renamed from: a.fY$a */
    public class HullBlock extends HullAdapter implements C1616Xf {
        public static final int _m_fieldCount = 0;
        public static final C5663aRz[] _m_fields = null;
        public static final int _m_methodCount = 0;
        public static final C2491fm[] _m_methods = null;
        /* renamed from: aT */
        public static final C5663aRz f7385aT = null;
        /* renamed from: qa */
        public static final C2491fm f7386qa = null;
        public static final long serialVersionUID = 0;
        public static C6494any ___iScriptClass;
        @C0064Am(aul = "2216d1e1855bb63d7b6228db62dbe143", aum = 0)

        /* renamed from: OD */
        static /* synthetic */ Explorer f7384OD;

        static {
            m31112V();
        }

        public HullBlock() {
            super((C5540aNg) null);
            super.mo10S();
        }

        public HullBlock(C5540aNg ang) {
            super(ang);
        }

        public HullBlock(Explorer fYVar) {
            super((C5540aNg) null);
            super._m_script_init(fYVar);
        }

        /* renamed from: V */
        static void m31112V() {
            serialVersionUID = (long) 1;
            _m_fieldCount = HullAdapter._m_fieldCount + 1;
            _m_methodCount = HullAdapter._m_methodCount + 1;
            int i = HullAdapter._m_fieldCount;
            C5663aRz[] arzArr = new C5663aRz[(i + 1)];
            C5663aRz b = C5640aRc.m17844b(HullBlock.class, "2216d1e1855bb63d7b6228db62dbe143", i);
            f7385aT = b;
            arzArr[i] = b;
            int i2 = i + 1;
            _m_fields = arzArr;
            C1634Xv.m11725a((Object[]) HullAdapter._m_fields, (Object[]) _m_fields);
            int i3 = HullAdapter._m_methodCount;
            C2491fm[] fmVarArr = new C2491fm[(i3 + 1)];
            C2491fm a = C4105zY.m41624a(HullBlock.class, "f527f2072e8749c64020c675a95549a2", i3);
            f7386qa = a;
            fmVarArr[i3] = a;
            int i4 = i3 + 1;
            _m_methods = fmVarArr;
            C1634Xv.m11725a((Object[]) HullAdapter._m_methods, (Object[]) _m_methods);
            ___iScriptClass = aUO.m18566a(HullBlock.class, C6464anU.class, _m_fields, _m_methods);
        }

        /* renamed from: a */
        private void m31114a(Explorer fYVar) {
            bFf().mo5608dq().mo3197f(f7385aT, fYVar);
        }

        /* renamed from: uH */
        private Explorer m31115uH() {
            return (Explorer) bFf().mo5608dq().mo3214p(f7385aT);
        }

        /* renamed from: S */
        public void mo10S() {
            super.mo10S();
        }

        /* renamed from: T */
        public C6494any mo11T() {
            return ___iScriptClass;
        }

        /* renamed from: U */
        public C2491fm[] mo12U() {
            return _m_methods;
        }

        /* renamed from: W */
        public Object mo13W() {
            return new C6464anU(this);
        }

        /* renamed from: a */
        public Object mo14a(C0495Gr gr) {
            Object[] args = gr.getArgs();
            switch (gr.mo2417hq() - HullAdapter._m_methodCount) {
                case 0:
                    return new Float(m31113a((DamageType) args[0], ((Float) args[1]).floatValue()));
                default:
                    return super.mo14a(gr);
            }
        }

        /* renamed from: a */
        public void mo15a(Collection collection, C0495Gr gr) {
            super.mo15a(collection, gr);
        }

        /* renamed from: b */
        public float mo9877b(DamageType fr, float f) {
            switch (bFf().mo6893i(f7386qa)) {
                case 0:
                    return 0.0f;
                case 2:
                    return ((Float) bFf().mo5606d(new aCE(this, f7386qa, new Object[]{fr, new Float(f)}))).floatValue();
                case 3:
                    bFf().mo5606d(new aCE(this, f7386qa, new Object[]{fr, new Float(f)}));
                    break;
            }
            return m31113a(fr, f);
        }

        /* renamed from: c */
        public C5663aRz[] mo25c() {
            return _m_fields;
        }

        /* renamed from: b */
        public void mo18704b(Explorer fYVar) {
            m31114a(fYVar);
            super.mo10S();
        }

        @C0064Am(aul = "f527f2072e8749c64020c675a95549a2", aum = 0)
        /* renamed from: a */
        private float m31113a(DamageType fr, float f) {
            return 0.0f;
        }
    }

    @C6485anp
    @C5511aMd
    @Deprecated
    /* renamed from: a.fY$c */
    /* compiled from: a */
    public class RadarUpgrade extends ShipAdapter implements C1616Xf {
        public static final int _m_fieldCount = 0;
        public static final C5663aRz[] _m_fields = null;
        public static final int _m_methodCount = 0;
        public static final C2491fm[] _m_methods = null;
        /* renamed from: aT */
        public static final C5663aRz f7388aT = null;
        public static final C2491fm bpZ = null;
        public static final long serialVersionUID = 0;
        public static C6494any ___iScriptClass;
        @C0064Am(aul = "0736857e37d38a43a9cd617d00fe71b7", aum = 0)

        /* renamed from: OD */
        static /* synthetic */ Explorer f7387OD;

        static {
            m31125V();
        }

        public RadarUpgrade() {
            super((C5540aNg) null);
            super.mo10S();
        }

        public RadarUpgrade(C5540aNg ang) {
            super(ang);
        }

        public RadarUpgrade(Explorer fYVar) {
            super((C5540aNg) null);
            super._m_script_init(fYVar);
        }

        /* renamed from: V */
        static void m31125V() {
            serialVersionUID = (long) 1;
            _m_fieldCount = ShipAdapter._m_fieldCount + 1;
            _m_methodCount = ShipAdapter._m_methodCount + 1;
            int i = ShipAdapter._m_fieldCount;
            C5663aRz[] arzArr = new C5663aRz[(i + 1)];
            C5663aRz b = C5640aRc.m17844b(RadarUpgrade.class, "0736857e37d38a43a9cd617d00fe71b7", i);
            f7388aT = b;
            arzArr[i] = b;
            int i2 = i + 1;
            _m_fields = arzArr;
            C1634Xv.m11725a((Object[]) ShipAdapter._m_fields, (Object[]) _m_fields);
            int i3 = ShipAdapter._m_methodCount;
            C2491fm[] fmVarArr = new C2491fm[(i3 + 1)];
            C2491fm a = C4105zY.m41624a(RadarUpgrade.class, "e841d385a41e381047696c51e2aa22da", i3);
            bpZ = a;
            fmVarArr[i3] = a;
            int i4 = i3 + 1;
            _m_methods = fmVarArr;
            C1634Xv.m11725a((Object[]) ShipAdapter._m_methods, (Object[]) _m_methods);
            ___iScriptClass = aUO.m18566a(RadarUpgrade.class, C1757Zx.class, _m_fields, _m_methods);
        }

        /* renamed from: a */
        private void m31126a(Explorer fYVar) {
            bFf().mo5608dq().mo3197f(f7388aT, fYVar);
        }

        /* renamed from: uH */
        private Explorer m31127uH() {
            return (Explorer) bFf().mo5608dq().mo3214p(f7388aT);
        }

        /* renamed from: S */
        public void mo10S() {
            super.mo10S();
        }

        /* renamed from: T */
        public C6494any mo11T() {
            return ___iScriptClass;
        }

        /* renamed from: U */
        public C2491fm[] mo12U() {
            return _m_methods;
        }

        /* renamed from: W */
        public Object mo13W() {
            return new C1757Zx(this);
        }

        /* renamed from: a */
        public Object mo14a(C0495Gr gr) {
            gr.getArgs();
            switch (gr.mo2417hq() - ShipAdapter._m_methodCount) {
                case 0:
                    return new Float(ago());
                default:
                    return super.mo14a(gr);
            }
        }

        /* renamed from: a */
        public void mo15a(Collection collection, C0495Gr gr) {
            super.mo15a(collection, gr);
        }

        public float agp() {
            switch (bFf().mo6893i(bpZ)) {
                case 0:
                    return 0.0f;
                case 2:
                    return ((Float) bFf().mo5606d(new aCE(this, bpZ, new Object[0]))).floatValue();
                case 3:
                    bFf().mo5606d(new aCE(this, bpZ, new Object[0]));
                    break;
            }
            return ago();
        }

        /* renamed from: c */
        public C5663aRz[] mo25c() {
            return _m_fields;
        }

        /* renamed from: b */
        public void mo18705b(Explorer fYVar) {
            m31126a(fYVar);
            super.mo10S();
        }

        @C0064Am(aul = "e841d385a41e381047696c51e2aa22da", aum = 0)
        private float ago() {
            return super.agp();
        }
    }

    @C6485anp
    @C5511aMd
    @Deprecated
    /* renamed from: a.fY$f */
    /* compiled from: a */
    public class InvulnerabilityEnterer extends TaskletImpl implements C1616Xf {

        /* renamed from: JD */
        public static final C2491fm f7396JD = null;
        public static final int _m_fieldCount = 0;
        public static final C5663aRz[] _m_fields = null;
        public static final int _m_methodCount = 0;
        public static final C2491fm[] _m_methods = null;
        /* renamed from: aT */
        public static final C5663aRz f7398aT = null;
        public static final C2491fm fns = null;
        public static final long serialVersionUID = 0;
        public static C6494any ___iScriptClass;
        @C0064Am(aul = "d753c4e09bbba2c552f57d3e8296ca97", aum = 0)

        /* renamed from: OD */
        static /* synthetic */ Explorer f7397OD;

        static {
            m31164V();
        }

        public InvulnerabilityEnterer() {
            super((C5540aNg) null);
            super.mo10S();
        }

        public InvulnerabilityEnterer(C5540aNg ang) {
            super(ang);
        }

        public InvulnerabilityEnterer(Explorer fYVar, aDJ adj) {
            super((C5540aNg) null);
            super._m_script_init(fYVar, adj);
        }

        /* renamed from: V */
        static void m31164V() {
            serialVersionUID = (long) 1;
            _m_fieldCount = TaskletImpl._m_fieldCount + 1;
            _m_methodCount = TaskletImpl._m_methodCount + 2;
            int i = TaskletImpl._m_fieldCount;
            C5663aRz[] arzArr = new C5663aRz[(i + 1)];
            C5663aRz b = C5640aRc.m17844b(InvulnerabilityEnterer.class, "d753c4e09bbba2c552f57d3e8296ca97", i);
            f7398aT = b;
            arzArr[i] = b;
            int i2 = i + 1;
            _m_fields = arzArr;
            C1634Xv.m11725a((Object[]) TaskletImpl._m_fields, (Object[]) _m_fields);
            int i3 = TaskletImpl._m_methodCount;
            C2491fm[] fmVarArr = new C2491fm[(i3 + 2)];
            C2491fm a = C4105zY.m41624a(InvulnerabilityEnterer.class, "839ea201f091481f43de44ae09d72de4", i3);
            f7396JD = a;
            fmVarArr[i3] = a;
            int i4 = i3 + 1;
            C2491fm a2 = C4105zY.m41624a(InvulnerabilityEnterer.class, "5f2a8f48e2edebe7231b426a5e172a6b", i4);
            fns = a2;
            fmVarArr[i4] = a2;
            int i5 = i4 + 1;
            _m_methods = fmVarArr;
            C1634Xv.m11725a((Object[]) TaskletImpl._m_methods, (Object[]) _m_methods);
            ___iScriptClass = aUO.m18566a(InvulnerabilityEnterer.class, C0282Dd.class, _m_fields, _m_methods);
        }

        /* renamed from: a */
        private void m31165a(Explorer fYVar) {
            bFf().mo5608dq().mo3197f(f7398aT, fYVar);
        }

        /* renamed from: uH */
        private Explorer m31167uH() {
            return (Explorer) bFf().mo5608dq().mo3214p(f7398aT);
        }

        /* renamed from: S */
        public void mo10S() {
            super.mo10S();
        }

        /* renamed from: T */
        public C6494any mo11T() {
            return ___iScriptClass;
        }

        /* renamed from: U */
        public C2491fm[] mo12U() {
            return _m_methods;
        }

        /* renamed from: W */
        public Object mo13W() {
            return new C0282Dd(this);
        }

        /* renamed from: a */
        public Object mo14a(C0495Gr gr) {
            gr.getArgs();
            switch (gr.mo2417hq() - TaskletImpl._m_methodCount) {
                case 0:
                    m31166pi();
                    return null;
                case 1:
                    bUg();
                    return null;
                default:
                    return super.mo14a(gr);
            }
        }

        /* renamed from: a */
        public void mo15a(Collection collection, C0495Gr gr) {
            super.mo15a(collection, gr);
        }

        /* access modifiers changed from: package-private */
        public void bUh() {
            switch (bFf().mo6893i(fns)) {
                case 0:
                    return;
                case 2:
                    bFf().mo5606d(new aCE(this, fns, new Object[0]));
                    return;
                case 3:
                    bFf().mo5606d(new aCE(this, fns, new Object[0]));
                    break;
            }
            bUg();
        }

        /* renamed from: c */
        public C5663aRz[] mo25c() {
            return _m_fields;
        }

        /* access modifiers changed from: protected */
        /* renamed from: pj */
        public void mo667pj() {
            switch (bFf().mo6893i(f7396JD)) {
                case 0:
                    return;
                case 2:
                    bFf().mo5606d(new aCE(this, f7396JD, new Object[0]));
                    return;
                case 3:
                    bFf().mo5606d(new aCE(this, f7396JD, new Object[0]));
                    break;
            }
            m31166pi();
        }

        /* renamed from: a */
        public void mo18709a(Explorer fYVar, aDJ adj) {
            m31165a(fYVar);
            super.mo4706l(adj);
        }

        @C0064Am(aul = "839ea201f091481f43de44ae09d72de4", aum = 0)
        /* renamed from: pi */
        private void m31166pi() {
        }

        @C0064Am(aul = "5f2a8f48e2edebe7231b426a5e172a6b", aum = 0)
        private void bUg() {
        }
    }

    @C6485anp
    @C5511aMd
    @Deprecated
    /* renamed from: a.fY$d */
    /* compiled from: a */
    public class InvulnerabilityExiter extends TaskletImpl implements C1616Xf {

        /* renamed from: JD */
        public static final C2491fm f7389JD = null;
        public static final int _m_fieldCount = 0;
        public static final C5663aRz[] _m_fields = null;
        public static final int _m_methodCount = 0;
        public static final C2491fm[] _m_methods = null;
        /* renamed from: aT */
        public static final C5663aRz f7391aT = null;
        public static final C2491fm dvf = null;
        public static final long serialVersionUID = 0;
        public static C6494any ___iScriptClass;
        @C0064Am(aul = "13418f1e4f0026e61343e2882bdaa972", aum = 0)

        /* renamed from: OD */
        static /* synthetic */ Explorer f7390OD;

        static {
            m31136V();
        }

        public InvulnerabilityExiter() {
            super((C5540aNg) null);
            super.mo10S();
        }

        public InvulnerabilityExiter(C5540aNg ang) {
            super(ang);
        }

        public InvulnerabilityExiter(Explorer fYVar, aDJ adj) {
            super((C5540aNg) null);
            super._m_script_init(fYVar, adj);
        }

        /* renamed from: V */
        static void m31136V() {
            serialVersionUID = (long) 1;
            _m_fieldCount = TaskletImpl._m_fieldCount + 1;
            _m_methodCount = TaskletImpl._m_methodCount + 2;
            int i = TaskletImpl._m_fieldCount;
            C5663aRz[] arzArr = new C5663aRz[(i + 1)];
            C5663aRz b = C5640aRc.m17844b(InvulnerabilityExiter.class, "13418f1e4f0026e61343e2882bdaa972", i);
            f7391aT = b;
            arzArr[i] = b;
            int i2 = i + 1;
            _m_fields = arzArr;
            C1634Xv.m11725a((Object[]) TaskletImpl._m_fields, (Object[]) _m_fields);
            int i3 = TaskletImpl._m_methodCount;
            C2491fm[] fmVarArr = new C2491fm[(i3 + 2)];
            C2491fm a = C4105zY.m41624a(InvulnerabilityExiter.class, "e438d49fe561e076bf36f9e1581020f4", i3);
            f7389JD = a;
            fmVarArr[i3] = a;
            int i4 = i3 + 1;
            C2491fm a2 = C4105zY.m41624a(InvulnerabilityExiter.class, "cf0f64bfd3618c46adbe421214f09aa3", i4);
            dvf = a2;
            fmVarArr[i4] = a2;
            int i5 = i4 + 1;
            _m_methods = fmVarArr;
            C1634Xv.m11725a((Object[]) TaskletImpl._m_methods, (Object[]) _m_methods);
            ___iScriptClass = aUO.m18566a(InvulnerabilityExiter.class, C0252DF.class, _m_fields, _m_methods);
        }

        /* renamed from: a */
        private void m31137a(Explorer fYVar) {
            bFf().mo5608dq().mo3197f(f7391aT, fYVar);
        }

        /* renamed from: uH */
        private Explorer m31139uH() {
            return (Explorer) bFf().mo5608dq().mo3214p(f7391aT);
        }

        /* renamed from: S */
        public void mo10S() {
            super.mo10S();
        }

        /* renamed from: T */
        public C6494any mo11T() {
            return ___iScriptClass;
        }

        /* renamed from: U */
        public C2491fm[] mo12U() {
            return _m_methods;
        }

        /* renamed from: W */
        public Object mo13W() {
            return new C0252DF(this);
        }

        /* renamed from: a */
        public Object mo14a(C0495Gr gr) {
            gr.getArgs();
            switch (gr.mo2417hq() - TaskletImpl._m_methodCount) {
                case 0:
                    m31138pi();
                    return null;
                case 1:
                    bfJ();
                    return null;
                default:
                    return super.mo14a(gr);
            }
        }

        /* renamed from: a */
        public void mo15a(Collection collection, C0495Gr gr) {
            super.mo15a(collection, gr);
        }

        /* access modifiers changed from: package-private */
        public void bfK() {
            switch (bFf().mo6893i(dvf)) {
                case 0:
                    return;
                case 2:
                    bFf().mo5606d(new aCE(this, dvf, new Object[0]));
                    return;
                case 3:
                    bFf().mo5606d(new aCE(this, dvf, new Object[0]));
                    break;
            }
            bfJ();
        }

        /* renamed from: c */
        public C5663aRz[] mo25c() {
            return _m_fields;
        }

        /* access modifiers changed from: protected */
        /* renamed from: pj */
        public void mo667pj() {
            switch (bFf().mo6893i(f7389JD)) {
                case 0:
                    return;
                case 2:
                    bFf().mo5606d(new aCE(this, f7389JD, new Object[0]));
                    return;
                case 3:
                    bFf().mo5606d(new aCE(this, f7389JD, new Object[0]));
                    break;
            }
            m31138pi();
        }

        /* renamed from: a */
        public void mo18706a(Explorer fYVar, aDJ adj) {
            m31137a(fYVar);
            super.mo4706l(adj);
        }

        @C0064Am(aul = "e438d49fe561e076bf36f9e1581020f4", aum = 0)
        /* renamed from: pi */
        private void m31138pi() {
        }

        @C0064Am(aul = "cf0f64bfd3618c46adbe421214f09aa3", aum = 0)
        private void bfJ() {
        }
    }
}
