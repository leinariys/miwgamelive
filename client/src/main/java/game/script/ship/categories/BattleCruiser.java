package game.script.ship.categories;

import game.script.ship.Ship;
import game.script.ship.ShipType;
import logic.baa.C1616Xf;
import logic.baa.C1634Xv;
import logic.baa.C5511aMd;
import logic.baa.C6485anp;
import logic.data.mbean.C5952adc;
import logic.res.code.C5663aRz;
import logic.res.code.C6494any;
import logic.res.html.C2491fm;
import p001a.*;

import java.util.Collection;

@C6485anp
@aEZ("BattleCruiser")
@C2712iu(version = "1.0.0")
@C5511aMd
/* renamed from: a.nU */
/* compiled from: a */
public class BattleCruiser extends Ship implements C1616Xf {
    public static final int _m_fieldCount = 0;
    public static final C5663aRz[] _m_fields = null;
    public static final int _m_methodCount = 0;
    public static final C2491fm[] _m_methods = null;
    public static final long serialVersionUID = 0;
    public static C6494any ___iScriptClass;

    static {
        m36105V();
    }

    public BattleCruiser() {
        super((C5540aNg) null);
        super.mo10S();
    }

    public BattleCruiser(C5540aNg ang) {
        super(ang);
    }

    public BattleCruiser(BattleCruiserType aus) {
        super((C5540aNg) null);
        super._m_script_init(aus);
    }

    /* renamed from: V */
    static void m36105V() {
        serialVersionUID = (long) 1;
        _m_fieldCount = Ship._m_fieldCount + 0;
        _m_methodCount = Ship._m_methodCount + 0;
        _m_fields = new C5663aRz[(Ship._m_fieldCount + 0)];
        C1634Xv.m11725a((Object[]) Ship._m_fields, (Object[]) _m_fields);
        _m_methods = new C2491fm[(Ship._m_methodCount + 0)];
        C1634Xv.m11725a((Object[]) Ship._m_methods, (Object[]) _m_methods);
        ___iScriptClass = aUO.m18566a(BattleCruiser.class, C5952adc.class, _m_fields, _m_methods);
    }

    /* renamed from: S */
    public void mo10S() {
        super.mo10S();
    }

    /* renamed from: T */
    public C6494any mo11T() {
        return ___iScriptClass;
    }

    /* renamed from: U */
    public C2491fm[] mo12U() {
        return _m_methods;
    }

    /* renamed from: W */
    public Object mo13W() {
        return new C5952adc(this);
    }

    /* renamed from: a */
    public Object mo14a(C0495Gr gr) {
        return super.mo14a(gr);
    }

    /* renamed from: a */
    public void mo15a(Collection collection, C0495Gr gr) {
        super.mo15a(collection, gr);
    }

    /* renamed from: c */
    public C5663aRz[] mo25c() {
        return _m_fields;
    }

    /* renamed from: a */
    public void mo20754a(BattleCruiserType aus) {
        super.mo18318h((ShipType) aus);
    }
}
