package game.script.ship;

import game.network.message.externalizable.aCE;
import logic.baa.*;
import logic.data.mbean.C0222Cn;
import logic.res.code.C5640aRc;
import logic.res.code.C5663aRz;
import logic.res.code.C6494any;
import logic.res.html.C2491fm;
import p001a.*;

import java.util.Collection;

@C0566Hp
@C5829abJ("2.0.0")
@C6485anp
@C5511aMd
/* renamed from: a.aRb  reason: case insensitive filesystem */
/* compiled from: a */
public class CargoHoldType extends BaseCargoHoldType implements C1616Xf {

    /* renamed from: Pv */
    public static final C5663aRz f3711Pv = null;

    /* renamed from: Rz */
    public static final C2491fm f3712Rz = null;
    public static final int _m_fieldCount = 0;
    public static final C5663aRz[] _m_fields = null;
    public static final int _m_methodCount = 0;
    public static final C2491fm[] _m_methods = null;
    public static final C2491fm cIC = null;
    /* renamed from: dN */
    public static final C2491fm f3713dN = null;
    public static final C2491fm iGu = null;
    public static final long serialVersionUID = 0;
    public static C6494any ___iScriptClass;
    @C0064Am(aul = "186f1cf9c473ede39fc829088b23fc1a", aum = 0)
    private static float cvd;

    static {
        m17829V();
    }

    public CargoHoldType() {
        super((C5540aNg) null);
        super.mo10S();
    }

    public CargoHoldType(C5540aNg ang) {
        super(ang);
    }

    /* renamed from: V */
    static void m17829V() {
        serialVersionUID = (long) 1;
        _m_fieldCount = BaseCargoHoldType._m_fieldCount + 1;
        _m_methodCount = BaseCargoHoldType._m_methodCount + 4;
        int i = BaseCargoHoldType._m_fieldCount;
        C5663aRz[] arzArr = new C5663aRz[(i + 1)];
        C5663aRz b = C5640aRc.m17844b(CargoHoldType.class, "186f1cf9c473ede39fc829088b23fc1a", i);
        f3711Pv = b;
        arzArr[i] = b;
        int i2 = i + 1;
        _m_fields = arzArr;
        C1634Xv.m11725a((Object[]) BaseCargoHoldType._m_fields, (Object[]) _m_fields);
        int i3 = BaseCargoHoldType._m_methodCount;
        C2491fm[] fmVarArr = new C2491fm[(i3 + 4)];
        C2491fm a = C4105zY.m41624a(CargoHoldType.class, "1ce56a803504c58f4a68c38cc94eaf82", i3);
        f3712Rz = a;
        fmVarArr[i3] = a;
        int i4 = i3 + 1;
        C2491fm a2 = C4105zY.m41624a(CargoHoldType.class, "d0475cb549789f8246ef84aba6c0be50", i4);
        cIC = a2;
        fmVarArr[i4] = a2;
        int i5 = i4 + 1;
        C2491fm a3 = C4105zY.m41624a(CargoHoldType.class, "320f10cec33c565bdf5f6e1725fcef1e", i5);
        iGu = a3;
        fmVarArr[i5] = a3;
        int i6 = i5 + 1;
        C2491fm a4 = C4105zY.m41624a(CargoHoldType.class, "7a9952127021d705083072708b339223", i6);
        f3713dN = a4;
        fmVarArr[i6] = a4;
        int i7 = i6 + 1;
        _m_methods = fmVarArr;
        C1634Xv.m11725a((Object[]) BaseCargoHoldType._m_methods, (Object[]) _m_methods);
        ___iScriptClass = aUO.m18566a(CargoHoldType.class, C0222Cn.class, _m_fields, _m_methods);
    }

    private float aHj() {
        return bFf().mo5608dq().mo3211m(f3711Pv);
    }

    /* renamed from: fc */
    private void m17831fc(float f) {
        bFf().mo5608dq().mo3150a(f3711Pv, f);
    }

    /* renamed from: T */
    public C6494any mo11T() {
        return ___iScriptClass;
    }

    /* renamed from: U */
    public C2491fm[] mo12U() {
        return _m_methods;
    }

    /* renamed from: W */
    public Object mo13W() {
        return new C0222Cn(this);
    }

    /* renamed from: a */
    public Object mo14a(C0495Gr gr) {
        Object[] args = gr.getArgs();
        switch (gr.mo2417hq() - BaseCargoHoldType._m_methodCount) {
            case 0:
                return new Float(m17833wD());
            case 1:
                m17832fd(((Float) args[0]).floatValue());
                return null;
            case 2:
                return drc();
            case 3:
                return m17830aT();
            default:
                return super.mo14a(gr);
        }
    }

    /* renamed from: a */
    public void mo15a(Collection collection, C0495Gr gr) {
        super.mo15a(collection, gr);
    }

    /* access modifiers changed from: protected */
    /* renamed from: aU */
    public <T extends aDJ> T mo745aU() {
        switch (bFf().mo6893i(f3713dN)) {
            case 0:
                return null;
            case 2:
                return (aDJ) bFf().mo5606d(new aCE(this, f3713dN, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f3713dN, new Object[0]));
                break;
        }
        return m17830aT();
    }

    /* renamed from: c */
    public C5663aRz[] mo25c() {
        return _m_fields;
    }

    public CargoHold drd() {
        switch (bFf().mo6893i(iGu)) {
            case 0:
                return null;
            case 2:
                return (CargoHold) bFf().mo5606d(new aCE(this, iGu, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, iGu, new Object[0]));
                break;
        }
        return drc();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Max Volume")
    /* renamed from: fe */
    public void mo11171fe(float f) {
        switch (bFf().mo6893i(cIC)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, cIC, new Object[]{new Float(f)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, cIC, new Object[]{new Float(f)}));
                break;
        }
        m17832fd(f);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Max Volume")
    /* renamed from: wE */
    public float mo11172wE() {
        switch (bFf().mo6893i(f3712Rz)) {
            case 0:
                return 0.0f;
            case 2:
                return ((Float) bFf().mo5606d(new aCE(this, f3712Rz, new Object[0]))).floatValue();
            case 3:
                bFf().mo5606d(new aCE(this, f3712Rz, new Object[0]));
                break;
        }
        return m17833wD();
    }

    /* renamed from: S */
    public void mo10S() {
        super.mo10S();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Max Volume")
    @C0064Am(aul = "1ce56a803504c58f4a68c38cc94eaf82", aum = 0)
    /* renamed from: wD */
    private float m17833wD() {
        return aHj();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Max Volume")
    @C0064Am(aul = "d0475cb549789f8246ef84aba6c0be50", aum = 0)
    /* renamed from: fd */
    private void m17832fd(float f) {
        m17831fc(f);
    }

    @C0064Am(aul = "320f10cec33c565bdf5f6e1725fcef1e", aum = 0)
    private CargoHold drc() {
        return (CargoHold) mo745aU();
    }

    @C0064Am(aul = "7a9952127021d705083072708b339223", aum = 0)
    /* renamed from: aT */
    private <T extends aDJ> T m17830aT() {
        T t = (CargoHold) bFf().mo6865M(CargoHold.class);
        t.mo9150a(this);
        return t;
    }
}
