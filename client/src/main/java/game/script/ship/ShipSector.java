package game.script.ship;

import game.network.message.externalizable.aCE;
import game.script.Character;
import game.script.item.*;
import game.script.nls.NLSItem;
import game.script.nls.NLSManager;
import game.script.nls.NLSProgression;
import game.script.player.Player;
import game.script.resource.Asset;
import logic.baa.*;
import logic.data.mbean.C0547Hc;
import logic.res.code.C5640aRc;
import logic.res.code.C5663aRz;
import logic.res.code.C6494any;
import logic.res.html.C2491fm;
import p001a.*;
import taikodom.infra.script.I18NString;

import java.util.Collection;

@C2712iu(mo19786Bt = BaseShipSectorType.class, version = "1.1.5")
@C5511aMd
@C6485anp
/* renamed from: a.OR */
/* compiled from: a */
public class ShipSector extends ItemLocation implements C1616Xf {

    /* renamed from: Lm */
    public static final C2491fm f1308Lm = null;

    /* renamed from: NW */
    public static final C5663aRz f1309NW = null;

    /* renamed from: Ob */
    public static final C2491fm f1310Ob = null;

    /* renamed from: RA */
    public static final C2491fm f1311RA = null;

    /* renamed from: RD */
    public static final C2491fm f1312RD = null;
    public static final C2491fm _f_getType_0020_0028_0029Ljava_002flang_002fObject_003b = null;
    public static final int _m_fieldCount = 0;
    public static final C5663aRz[] _m_fields = null;
    public static final int _m_methodCount = 0;
    public static final C2491fm[] _m_methods = null;
    public static final C5663aRz dOQ = null;
    public static final C5663aRz dOR = null;
    public static final C5663aRz dOS = null;
    public static final C5663aRz dOT = null;
    public static final C5663aRz dOU = null;
    public static final C2491fm dOV = null;
    public static final C2491fm dOW = null;
    public static final C2491fm dOX = null;
    public static final C2491fm dOY = null;
    public static final C2491fm dOZ = null;
    public static final C2491fm dPa = null;
    public static final C2491fm dPb = null;
    public static final C2491fm dPc = null;
    /* renamed from: ku */
    public static final C2491fm f1313ku = null;
    public static final long serialVersionUID = 0;
    /* renamed from: zT */
    public static final C2491fm f1314zT = null;
    public static C6494any ___iScriptClass;
    @C0064Am(aul = "bb6db3e775b0f3cb101ea41d9bfc9b99", aum = 0)
    private static int daj;
    @C0064Am(aul = "d8b63fb838c8fdcc076e0f2f02b67175", aum = 1)
    private static boolean dak;
    @C0064Am(aul = "04b0445e639ba85dfe1617a9a1214af4", aum = 2)
    private static int dal;
    @C0064Am(aul = "ea0bfa2523f843a01a23ec4475fa0fab", aum = 3)
    private static SectorCategory dam;
    @C0064Am(aul = "2dec14ca5c9dda332101ae1af7f8eea6", aum = 4)
    private static int dan;
    @C0064Am(aul = "45789f9feae75a60069ecccf90f2fe0a", aum = 5)
    private static ShipStructure dao;

    static {
        m8035V();
    }

    public ShipSector() {
        super((C5540aNg) null);
        super.mo10S();
    }

    public ShipSector(C5540aNg ang) {
        super(ang);
    }

    public ShipSector(ShipSectorType aux) {
        super((C5540aNg) null);
        super._m_script_init(aux);
    }

    /* renamed from: V */
    static void m8035V() {
        serialVersionUID = (long) 1;
        _m_fieldCount = ItemLocation._m_fieldCount + 6;
        _m_methodCount = ItemLocation._m_methodCount + 15;
        int i = ItemLocation._m_fieldCount;
        C5663aRz[] arzArr = new C5663aRz[(i + 6)];
        C5663aRz b = C5640aRc.m17844b(ShipSector.class, "bb6db3e775b0f3cb101ea41d9bfc9b99", i);
        dOQ = b;
        arzArr[i] = b;
        int i2 = i + 1;
        C5663aRz b2 = C5640aRc.m17844b(ShipSector.class, "d8b63fb838c8fdcc076e0f2f02b67175", i2);
        dOR = b2;
        arzArr[i2] = b2;
        int i3 = i2 + 1;
        C5663aRz b3 = C5640aRc.m17844b(ShipSector.class, "04b0445e639ba85dfe1617a9a1214af4", i3);
        dOS = b3;
        arzArr[i3] = b3;
        int i4 = i3 + 1;
        C5663aRz b4 = C5640aRc.m17844b(ShipSector.class, "ea0bfa2523f843a01a23ec4475fa0fab", i4);
        f1309NW = b4;
        arzArr[i4] = b4;
        int i5 = i4 + 1;
        C5663aRz b5 = C5640aRc.m17844b(ShipSector.class, "2dec14ca5c9dda332101ae1af7f8eea6", i5);
        dOT = b5;
        arzArr[i5] = b5;
        int i6 = i5 + 1;
        C5663aRz b6 = C5640aRc.m17844b(ShipSector.class, "45789f9feae75a60069ecccf90f2fe0a", i6);
        dOU = b6;
        arzArr[i6] = b6;
        int i7 = i6 + 1;
        _m_fields = arzArr;
        C1634Xv.m11725a((Object[]) ItemLocation._m_fields, (Object[]) _m_fields);
        int i8 = ItemLocation._m_methodCount;
        C2491fm[] fmVarArr = new C2491fm[(i8 + 15)];
        C2491fm a = C4105zY.m41624a(ShipSector.class, "28fbb05fd9c2885ec016a9c7bf61ac24", i8);
        dOV = a;
        fmVarArr[i8] = a;
        int i9 = i8 + 1;
        C2491fm a2 = C4105zY.m41624a(ShipSector.class, "33cd40b02dbdbd640512081faada03f1", i9);
        dOW = a2;
        fmVarArr[i9] = a2;
        int i10 = i9 + 1;
        C2491fm a3 = C4105zY.m41624a(ShipSector.class, "0f4296b944df2b3e6b5f926916b50af7", i10);
        dOX = a3;
        fmVarArr[i10] = a3;
        int i11 = i10 + 1;
        C2491fm a4 = C4105zY.m41624a(ShipSector.class, "58507eac67e3a28864d26814b553fda4", i11);
        f1310Ob = a4;
        fmVarArr[i11] = a4;
        int i12 = i11 + 1;
        C2491fm a5 = C4105zY.m41624a(ShipSector.class, "51eb39d7cf5e5aafc145e1c614ab7447", i12);
        f1314zT = a5;
        fmVarArr[i12] = a5;
        int i13 = i12 + 1;
        C2491fm a6 = C4105zY.m41624a(ShipSector.class, "345e281a3be8d265ec611e196ced6085", i13);
        dOY = a6;
        fmVarArr[i13] = a6;
        int i14 = i13 + 1;
        C2491fm a7 = C4105zY.m41624a(ShipSector.class, "ddbbc4c35cdeaaca099d2323b777f85f", i14);
        dOZ = a7;
        fmVarArr[i14] = a7;
        int i15 = i14 + 1;
        C2491fm a8 = C4105zY.m41624a(ShipSector.class, "c724022b9a8eb264f204885f9e042f52", i15);
        dPa = a8;
        fmVarArr[i15] = a8;
        int i16 = i15 + 1;
        C2491fm a9 = C4105zY.m41624a(ShipSector.class, "d4a03d8b0e6a69095218a2daedb6e983", i16);
        f1311RA = a9;
        fmVarArr[i16] = a9;
        int i17 = i16 + 1;
        C2491fm a10 = C4105zY.m41624a(ShipSector.class, "20243dc0f1561645905aa7ea4325ac0d", i17);
        f1312RD = a10;
        fmVarArr[i17] = a10;
        int i18 = i17 + 1;
        C2491fm a11 = C4105zY.m41624a(ShipSector.class, "b7cceee82dfea086c346f8a33ed546a4", i18);
        f1313ku = a11;
        fmVarArr[i18] = a11;
        int i19 = i18 + 1;
        C2491fm a12 = C4105zY.m41624a(ShipSector.class, "1394aaa491ff48c2be670830b9d4d342", i19);
        dPb = a12;
        fmVarArr[i19] = a12;
        int i20 = i19 + 1;
        C2491fm a13 = C4105zY.m41624a(ShipSector.class, "4515d70fa161d15bb1acf1ef202b5059", i20);
        dPc = a13;
        fmVarArr[i20] = a13;
        int i21 = i20 + 1;
        C2491fm a14 = C4105zY.m41624a(ShipSector.class, "b889e6ee0ce1e6165f1fdc782add50d7", i21);
        f1308Lm = a14;
        fmVarArr[i21] = a14;
        int i22 = i21 + 1;
        C2491fm a15 = C4105zY.m41624a(ShipSector.class, "8d30ae80d45ee22f950cfb78e8807de4", i22);
        _f_getType_0020_0028_0029Ljava_002flang_002fObject_003b = a15;
        fmVarArr[i22] = a15;
        int i23 = i22 + 1;
        _m_methods = fmVarArr;
        C1634Xv.m11725a((Object[]) ItemLocation._m_methods, (Object[]) _m_methods);
        ___iScriptClass = aUO.m18566a(ShipSector.class, C0547Hc.class, _m_fields, _m_methods);
    }

    private int bmJ() {
        return ((ShipSectorType) getType()).dAF();
    }

    private boolean bmK() {
        return ((ShipSectorType) getType()).dAH();
    }

    private int bmL() {
        return ((ShipSectorType) getType()).bmU();
    }

    private SectorCategory bmM() {
        return ((ShipSectorType) getType()).bmW();
    }

    private int bmN() {
        return bFf().mo5608dq().mo3212n(dOT);
    }

    private ShipStructure bmO() {
        return (ShipStructure) bFf().mo5608dq().mo3214p(dOU);
    }

    /* renamed from: c */
    private void m8037c(ShipStructure afk) {
        bFf().mo5608dq().mo3197f(dOU, afk);
    }

    /* renamed from: dh */
    private void m8039dh(boolean z) {
        throw new C6039afL();
    }

    /* renamed from: g */
    private void m8041g(SectorCategory fMVar) {
        throw new C6039afL();
    }

    /* renamed from: mq */
    private void m8044mq(int i) {
        throw new C6039afL();
    }

    /* renamed from: mr */
    private void m8045mr(int i) {
        throw new C6039afL();
    }

    /* renamed from: ms */
    private void m8046ms(int i) {
        bFf().mo5608dq().mo3183b(dOT, i);
    }

    @C0064Am(aul = "8d30ae80d45ee22f950cfb78e8807de4", aum = 0)
    /* renamed from: qW */
    private Object m8048qW() {
        return bmQ();
    }

    /* renamed from: B */
    public boolean mo4423B(ItemType jCVar) {
        switch (bFf().mo6893i(dPa)) {
            case 0:
                return false;
            case 2:
                return ((Boolean) bFf().mo5606d(new aCE(this, dPa, new Object[]{jCVar}))).booleanValue();
            case 3:
                bFf().mo5606d(new aCE(this, dPa, new Object[]{jCVar}));
                break;
        }
        return m8034A(jCVar);
    }

    /* renamed from: S */
    public void mo10S() {
        super.mo10S();
    }

    /* renamed from: T */
    public C6494any mo11T() {
        return ___iScriptClass;
    }

    /* renamed from: U */
    public C2491fm[] mo12U() {
        return _m_methods;
    }

    /* renamed from: W */
    public Object mo13W() {
        return new C0547Hc(this);
    }

    /* renamed from: a */
    public Object mo14a(C0495Gr gr) {
        Object[] args = gr.getArgs();
        switch (gr.mo2417hq() - ItemLocation._m_methodCount) {
            case 0:
                return bmP();
            case 1:
                return bmR();
            case 2:
                m8038d((ShipStructure) args[0]);
                return null;
            case 3:
                return m8049sJ();
            case 4:
                return m8042kd();
            case 5:
                return new Integer(bmT());
            case 6:
                return bmV();
            case 7:
                return new Boolean(m8034A((ItemType) args[0]));
            case 8:
                m8036a((Item) args[0], (ItemLocation) args[1]);
                return null;
            case 9:
                m8043m((Item) args[0]);
                return null;
            case 10:
                m8040g((Item) args[0]);
                return null;
            case 11:
                return new Integer(bmX());
            case 12:
                return new Integer(bmZ());
            case 13:
                m8047qU();
                return null;
            case 14:
                return m8048qW();
            default:
                return super.mo14a(gr);
        }
    }

    /* renamed from: a */
    public void mo15a(Collection collection, C0495Gr gr) {
        super.mo15a(collection, gr);
    }

    /* access modifiers changed from: protected */
    /* renamed from: b */
    public void mo1887b(Item auq, ItemLocation aag) {
        switch (bFf().mo6893i(f1311RA)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f1311RA, new Object[]{auq, aag}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f1311RA, new Object[]{auq, aag}));
                break;
        }
        m8036a(auq, aag);
    }

    public ShipSectorType bmQ() {
        switch (bFf().mo6893i(dOV)) {
            case 0:
                return null;
            case 2:
                return (ShipSectorType) bFf().mo5606d(new aCE(this, dOV, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, dOV, new Object[0]));
                break;
        }
        return bmP();
    }

    public ShipStructure bmS() {
        switch (bFf().mo6893i(dOW)) {
            case 0:
                return null;
            case 2:
                return (ShipStructure) bFf().mo5606d(new aCE(this, dOW, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, dOW, new Object[0]));
                break;
        }
        return bmR();
    }

    public int bmU() {
        switch (bFf().mo6893i(dOY)) {
            case 0:
                return 0;
            case 2:
                return ((Integer) bFf().mo5606d(new aCE(this, dOY, new Object[0]))).intValue();
            case 3:
                bFf().mo5606d(new aCE(this, dOY, new Object[0]));
                break;
        }
        return bmT();
    }

    public SectorCategory bmW() {
        switch (bFf().mo6893i(dOZ)) {
            case 0:
                return null;
            case 2:
                return (SectorCategory) bFf().mo5606d(new aCE(this, dOZ, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, dOZ, new Object[0]));
                break;
        }
        return bmV();
    }

    public int bmY() {
        switch (bFf().mo6893i(dPb)) {
            case 0:
                return 0;
            case 2:
                return ((Integer) bFf().mo5606d(new aCE(this, dPb, new Object[0]))).intValue();
            case 3:
                bFf().mo5606d(new aCE(this, dPb, new Object[0]));
                break;
        }
        return bmX();
    }

    public int bna() {
        switch (bFf().mo6893i(dPc)) {
            case 0:
                return 0;
            case 2:
                return ((Integer) bFf().mo5606d(new aCE(this, dPc, new Object[0]))).intValue();
            case 3:
                bFf().mo5606d(new aCE(this, dPc, new Object[0]));
                break;
        }
        return bmZ();
    }

    /* renamed from: c */
    public C5663aRz[] mo25c() {
        return _m_fields;
    }

    /* renamed from: e */
    public void mo4431e(ShipStructure afk) {
        switch (bFf().mo6893i(dOX)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, dOX, new Object[]{afk}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, dOX, new Object[]{afk}));
                break;
        }
        m8038d(afk);
    }

    public /* bridge */ /* synthetic */ Object getType() {
        switch (bFf().mo6893i(_f_getType_0020_0028_0029Ljava_002flang_002fObject_003b)) {
            case 0:
                return null;
            case 2:
                return bFf().mo5606d(new aCE(this, _f_getType_0020_0028_0029Ljava_002flang_002fObject_003b, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, _f_getType_0020_0028_0029Ljava_002flang_002fObject_003b, new Object[0]));
                break;
        }
        return m8048qW();
    }

    /* access modifiers changed from: protected */
    /* renamed from: h */
    public void mo1889h(Item auq) {
        switch (bFf().mo6893i(f1313ku)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f1313ku, new Object[]{auq}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f1313ku, new Object[]{auq}));
                break;
        }
        m8040g(auq);
    }

    /* renamed from: ke */
    public I18NString mo4432ke() {
        switch (bFf().mo6893i(f1314zT)) {
            case 0:
                return null;
            case 2:
                return (I18NString) bFf().mo5606d(new aCE(this, f1314zT, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f1314zT, new Object[0]));
                break;
        }
        return m8042kd();
    }

    /* access modifiers changed from: protected */
    /* renamed from: n */
    public void mo1890n(Item auq) {
        switch (bFf().mo6893i(f1312RD)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f1312RD, new Object[]{auq}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f1312RD, new Object[]{auq}));
                break;
        }
        m8043m(auq);
    }

    /* renamed from: qV */
    public void mo4433qV() {
        switch (bFf().mo6893i(f1308Lm)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f1308Lm, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f1308Lm, new Object[0]));
                break;
        }
        m8047qU();
    }

    /* renamed from: sK */
    public Asset mo4434sK() {
        switch (bFf().mo6893i(f1310Ob)) {
            case 0:
                return null;
            case 2:
                return (Asset) bFf().mo5606d(new aCE(this, f1310Ob, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f1310Ob, new Object[0]));
                break;
        }
        return m8049sJ();
    }

    /* renamed from: e */
    public void mo4430e(ShipSectorType aux) {
        super.mo967a((C2961mJ) aux);
    }

    @C0064Am(aul = "28fbb05fd9c2885ec016a9c7bf61ac24", aum = 0)
    private ShipSectorType bmP() {
        return (ShipSectorType) super.getType();
    }

    @C0064Am(aul = "33cd40b02dbdbd640512081faada03f1", aum = 0)
    private ShipStructure bmR() {
        return bmO();
    }

    @C0064Am(aul = "0f4296b944df2b3e6b5f926916b50af7", aum = 0)
    /* renamed from: d */
    private void m8038d(ShipStructure afk) {
        m8037c(afk);
    }

    @C0064Am(aul = "58507eac67e3a28864d26814b553fda4", aum = 0)
    /* renamed from: sJ */
    private Asset m8049sJ() {
        if (bmM() == null) {
            return null;
        }
        return bmM().mo18440sK();
    }

    @C0064Am(aul = "51eb39d7cf5e5aafc145e1c614ab7447", aum = 0)
    /* renamed from: kd */
    private I18NString m8042kd() {
        if (bmM() == null) {
            return null;
        }
        return bmM().mo18438ke();
    }

    @C0064Am(aul = "345e281a3be8d265ec611e196ced6085", aum = 0)
    private int bmT() {
        return bmL();
    }

    @C0064Am(aul = "ddbbc4c35cdeaaca099d2323b777f85f", aum = 0)
    private SectorCategory bmV() {
        return bmM();
    }

    @C0064Am(aul = "c724022b9a8eb264f204885f9e042f52", aum = 0)
    /* renamed from: A */
    private boolean m8034A(ItemType jCVar) {
        boolean z;
        boolean z2;
        if (!(jCVar instanceof ComponentType)) {
            return false;
        }
        ComponentType aef = (ComponentType) jCVar;
        if (bmQ().dAF() - bmN() < aef.cuH().ordinal() + 1) {
            return false;
        }
        SectorCategory cXV = aef.cXV();
        SectorCategory bmW = bmQ().bmW();
        if (cXV == null || bmW == null) {
            if (cXV == null && bmW == null) {
                mo6317hy("Ship Sector with null category accepting Item with null category, both were wrongfully created.");
                Thread.dumpStack();
                z = true;
            } else {
                z = false;
            }
        } else if (cXV.mo18441sM() == bmW.mo18441sM()) {
            z = true;
        } else {
            z = false;
        }
        Character agj = bmS().mo13241al().agj();
        if (agj instanceof Player) {
            z2 = jCVar.mo22858m(agj);
        } else {
            z2 = true;
        }
        if (!z || !z2) {
            return false;
        }
        return true;
    }

    @C0064Am(aul = "d4a03d8b0e6a69095218a2daedb6e983", aum = 0)
    /* renamed from: a */
    private void m8036a(Item auq, ItemLocation aag) {
        bmS().mo13241al().mo18316f((Component) auq);
        m8046ms(((Component) auq).cJS().cuH().ordinal() + 1 + bmN());
        if (auq.cxn()) {
            auq.setBound(true);
            if (((Component) auq).isUsable() && auq.cxw()) {
                Character agj = bmS().mo13241al().agj();
                if (agj instanceof Player) {
                    ((Player) agj).mo14343X(auq);
                } else {
                    mo8358lY("Item is billing and have no owner - " + auq);
                }
            }
        }
    }

    @C0064Am(aul = "20243dc0f1561645905aa7ea4325ac0d", aum = 0)
    /* renamed from: m */
    private void m8043m(Item auq) {
        bmS().mo13241al().mo18319h((Component) auq);
        m8046ms(bmN() - (((Component) auq).cJS().cuH().ordinal() + 1));
    }

    @C0064Am(aul = "b7cceee82dfea086c346f8a33ed546a4", aum = 0)
    /* renamed from: g */
    private void m8040g(Item auq) {
        Character agj = bmS().mo13241al().agj();
        if ((agj instanceof Player) && !auq.mo16383bW((Player) agj)) {
            throw new C2293dh(((NLSProgression) ala().aIY().mo6310c(NLSManager.C1472a.PROGRESSION)).cyg());
        } else if (!mo4423B(auq.bAP())) {
            throw new C2293dh(((NLSItem) ala().aIY().mo6310c(NLSManager.C1472a.ITEM)).bto());
        }
    }

    @C0064Am(aul = "1394aaa491ff48c2be670830b9d4d342", aum = 0)
    private int bmX() {
        return bmN();
    }

    @C0064Am(aul = "4515d70fa161d15bb1acf1ef202b5059", aum = 0)
    private int bmZ() {
        return bmJ() - bmN();
    }

    @C0064Am(aul = "b889e6ee0ce1e6165f1fdc782add50d7", aum = 0)
    /* renamed from: qU */
    private void m8047qU() {
        super.mo4433qV();
        if (bmO() != null) {
            bmO().push();
        }
    }
}
