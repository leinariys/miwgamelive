package game.script.ship;

import game.network.message.externalizable.aCE;
import game.script.damage.DamageType;
import game.script.util.GameObjectAdapter;
import logic.baa.C1616Xf;
import logic.baa.C1634Xv;
import logic.baa.C5511aMd;
import logic.baa.C6485anp;
import logic.data.mbean.C3119nz;
import logic.res.code.C5663aRz;
import logic.res.code.C6494any;
import logic.res.html.C2491fm;
import p001a.*;

import java.util.Collection;

@C5511aMd
@C6485anp
/* renamed from: a.cy */
/* compiled from: a */
public class ShieldAdapter extends GameObjectAdapter<ShieldAdapter> implements C1616Xf {
    public static final int _m_fieldCount = 0;
    public static final C5663aRz[] _m_fields = null;
    public static final int _m_methodCount = 0;
    public static final C2491fm[] _m_methods = null;
    /* renamed from: qa */
    public static final C2491fm f6417qa = null;
    /* renamed from: qb */
    public static final C2491fm f6418qb = null;
    public static final long serialVersionUID = 0;
    /* renamed from: uG */
    public static final C2491fm f6419uG = null;
    /* renamed from: uH */
    public static final C2491fm f6420uH = null;
    /* renamed from: uI */
    public static final C2491fm f6421uI = null;
    public static C6494any ___iScriptClass;

    static {
        m28693V();
    }

    public ShieldAdapter() {
        super((C5540aNg) null);
        super.mo10S();
    }

    public ShieldAdapter(C5540aNg ang) {
        super(ang);
    }

    /* renamed from: V */
    static void m28693V() {
        serialVersionUID = (long) 1;
        _m_fieldCount = GameObjectAdapter._m_fieldCount + 0;
        _m_methodCount = GameObjectAdapter._m_methodCount + 5;
        _m_fields = new C5663aRz[(GameObjectAdapter._m_fieldCount + 0)];
        C1634Xv.m11725a((Object[]) GameObjectAdapter._m_fields, (Object[]) _m_fields);
        int i = GameObjectAdapter._m_methodCount;
        C2491fm[] fmVarArr = new C2491fm[(i + 5)];
        C2491fm a = C4105zY.m41624a(ShieldAdapter.class, "f514bfa4e7045fc09b9e94d4a1ad844b", i);
        f6417qa = a;
        fmVarArr[i] = a;
        int i2 = i + 1;
        C2491fm a2 = C4105zY.m41624a(ShieldAdapter.class, "8c8c311aa189b976fae1b641ed583ac4", i2);
        f6418qb = a2;
        fmVarArr[i2] = a2;
        int i3 = i2 + 1;
        C2491fm a3 = C4105zY.m41624a(ShieldAdapter.class, "f9f83c5a03f78f53f12845d45c99729f", i3);
        f6419uG = a3;
        fmVarArr[i3] = a3;
        int i4 = i3 + 1;
        C2491fm a4 = C4105zY.m41624a(ShieldAdapter.class, "3db3b44a3f4d5ce3d351e7bf95cff04f", i4);
        f6420uH = a4;
        fmVarArr[i4] = a4;
        int i5 = i4 + 1;
        C2491fm a5 = C4105zY.m41624a(ShieldAdapter.class, "b85293ff5b23dca88c69555bd0a65024", i5);
        f6421uI = a5;
        fmVarArr[i5] = a5;
        int i6 = i5 + 1;
        _m_methods = fmVarArr;
        C1634Xv.m11725a((Object[]) GameObjectAdapter._m_methods, (Object[]) _m_methods);
        ___iScriptClass = aUO.m18566a(ShieldAdapter.class, C3119nz.class, _m_fields, _m_methods);
    }

    /* renamed from: T */
    public C6494any mo11T() {
        return ___iScriptClass;
    }

    /* renamed from: U */
    public C2491fm[] mo12U() {
        return _m_methods;
    }

    /* renamed from: W */
    public Object mo13W() {
        return new C3119nz(this);
    }

    /* renamed from: a */
    public Object mo14a(C0495Gr gr) {
        Object[] args = gr.getArgs();
        switch (gr.mo2417hq() - GameObjectAdapter._m_methodCount) {
            case 0:
                return new Float(m28695a((DamageType) args[0], ((Float) args[1]).floatValue()));
            case 1:
                return new Float(m28697hg());
            case 2:
                return new Float(m28696hZ());
            case 3:
                return new Float(m28698ib());
            case 4:
                return new Float(m28694a((DamageType) args[0]));
            default:
                return super.mo14a(gr);
        }
    }

    /* renamed from: a */
    public void mo15a(Collection collection, C0495Gr gr) {
        super.mo15a(collection, gr);
    }

    /* renamed from: b */
    public float mo3758b(DamageType fr) {
        switch (bFf().mo6893i(f6421uI)) {
            case 0:
                return 0.0f;
            case 2:
                return ((Float) bFf().mo5606d(new aCE(this, f6421uI, new Object[]{fr}))).floatValue();
            case 3:
                bFf().mo5606d(new aCE(this, f6421uI, new Object[]{fr}));
                break;
        }
        return m28694a(fr);
    }

    /* renamed from: b */
    public float mo3759b(DamageType fr, float f) {
        switch (bFf().mo6893i(f6417qa)) {
            case 0:
                return 0.0f;
            case 2:
                return ((Float) bFf().mo5606d(new aCE(this, f6417qa, new Object[]{fr, new Float(f)}))).floatValue();
            case 3:
                bFf().mo5606d(new aCE(this, f6417qa, new Object[]{fr, new Float(f)}));
                break;
        }
        return m28695a(fr, f);
    }

    /* renamed from: c */
    public C5663aRz[] mo25c() {
        return _m_fields;
    }

    /* renamed from: hh */
    public float mo3761hh() {
        switch (bFf().mo6893i(f6418qb)) {
            case 0:
                return 0.0f;
            case 2:
                return ((Float) bFf().mo5606d(new aCE(this, f6418qb, new Object[0]))).floatValue();
            case 3:
                bFf().mo5606d(new aCE(this, f6418qb, new Object[0]));
                break;
        }
        return m28697hg();
    }

    /* renamed from: ia */
    public float mo3762ia() {
        switch (bFf().mo6893i(f6419uG)) {
            case 0:
                return 0.0f;
            case 2:
                return ((Float) bFf().mo5606d(new aCE(this, f6419uG, new Object[0]))).floatValue();
            case 3:
                bFf().mo5606d(new aCE(this, f6419uG, new Object[0]));
                break;
        }
        return m28696hZ();
    }

    /* renamed from: ic */
    public float mo3763ic() {
        switch (bFf().mo6893i(f6420uH)) {
            case 0:
                return 0.0f;
            case 2:
                return ((Float) bFf().mo5606d(new aCE(this, f6420uH, new Object[0]))).floatValue();
            case 3:
                bFf().mo5606d(new aCE(this, f6420uH, new Object[0]));
                break;
        }
        return m28698ib();
    }

    /* renamed from: S */
    public void mo10S() {
        super.mo10S();
    }

    @C0064Am(aul = "f514bfa4e7045fc09b9e94d4a1ad844b", aum = 0)
    /* renamed from: a */
    private float m28695a(DamageType fr, float f) {
        return ((ShieldAdapter) mo16071IG()).mo3759b(fr, f);
    }

    @C0064Am(aul = "8c8c311aa189b976fae1b641ed583ac4", aum = 0)
    /* renamed from: hg */
    private float m28697hg() {
        return ((ShieldAdapter) mo16071IG()).mo3761hh();
    }

    @C0064Am(aul = "f9f83c5a03f78f53f12845d45c99729f", aum = 0)
    /* renamed from: hZ */
    private float m28696hZ() {
        return ((ShieldAdapter) mo16071IG()).mo3762ia();
    }

    @C0064Am(aul = "3db3b44a3f4d5ce3d351e7bf95cff04f", aum = 0)
    /* renamed from: ib */
    private float m28698ib() {
        return ((ShieldAdapter) mo16071IG()).mo3763ic();
    }

    @C0064Am(aul = "b85293ff5b23dca88c69555bd0a65024", aum = 0)
    /* renamed from: a */
    private float m28694a(DamageType fr) {
        return ((ShieldAdapter) mo16071IG()).mo3758b(fr);
    }
}
