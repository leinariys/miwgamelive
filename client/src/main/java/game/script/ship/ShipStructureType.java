package game.script.ship;

import game.network.message.externalizable.aCE;
import game.network.message.serializable.C3438ra;
import game.script.template.BaseTaikodomContent;
import logic.baa.*;
import logic.data.link.aLI;
import logic.data.mbean.C6482anm;
import logic.res.code.C5640aRc;
import logic.res.code.C5663aRz;
import logic.res.code.C6494any;
import logic.res.html.C2491fm;
import p001a.*;

import java.util.Collection;
import java.util.Collections;
import java.util.List;

@C0566Hp
@C5511aMd
@C6485anp
/* renamed from: a.aKS */
/* compiled from: a */
public class ShipStructureType extends BaseTaikodomContent implements C1616Xf {
    public static final int _m_fieldCount = 0;
    public static final C5663aRz[] _m_fields = null;
    public static final int _m_methodCount = 0;
    public static final C2491fm[] _m_methods = null;
    public static final C2491fm aDR = null;
    public static final C2491fm aTt = null;
    public static final C5663aRz cJJ = null;
    /* renamed from: dN */
    public static final C2491fm f3247dN = null;
    public static final C5663aRz dmj = null;
    public static final C2491fm fvv = null;
    public static final C2491fm geH = null;
    public static final C2491fm ijK = null;
    public static final C2491fm ijL = null;
    public static final C2491fm ijM = null;
    public static final C2491fm ijN = null;
    public static final long serialVersionUID = 0;
    public static C6494any ___iScriptClass;
    @C0064Am(aul = "e2afc7750d156dd49b6eb620b8c347a4", aum = 0)
    private static C3438ra<ShipSectorType> cJI;
    @C0064Am(aul = "7136691a90fb788c749ca5f4023b11ed", aum = 1)
    private static ShieldType fPa;

    static {
        m15981V();
    }

    public ShipStructureType() {
        super((C5540aNg) null);
        super.mo10S();
    }

    public ShipStructureType(C5540aNg ang) {
        super(ang);
    }

    /* renamed from: V */
    static void m15981V() {
        serialVersionUID = (long) 1;
        _m_fieldCount = BaseTaikodomContent._m_fieldCount + 2;
        _m_methodCount = BaseTaikodomContent._m_methodCount + 9;
        int i = BaseTaikodomContent._m_fieldCount;
        C5663aRz[] arzArr = new C5663aRz[(i + 2)];
        C5663aRz b = C5640aRc.m17844b(ShipStructureType.class, "e2afc7750d156dd49b6eb620b8c347a4", i);
        cJJ = b;
        arzArr[i] = b;
        int i2 = i + 1;
        C5663aRz b2 = C5640aRc.m17844b(ShipStructureType.class, "7136691a90fb788c749ca5f4023b11ed", i2);
        dmj = b2;
        arzArr[i2] = b2;
        int i3 = i2 + 1;
        _m_fields = arzArr;
        C1634Xv.m11725a((Object[]) BaseTaikodomContent._m_fields, (Object[]) _m_fields);
        int i4 = BaseTaikodomContent._m_methodCount;
        C2491fm[] fmVarArr = new C2491fm[(i4 + 9)];
        C2491fm a = C4105zY.m41624a(ShipStructureType.class, "c9df3af6be28b42ae352cfc89afb8c57", i4);
        ijK = a;
        fmVarArr[i4] = a;
        int i5 = i4 + 1;
        C2491fm a2 = C4105zY.m41624a(ShipStructureType.class, "934488924d0c797197bfdc617dcdf6c9", i5);
        ijL = a2;
        fmVarArr[i5] = a2;
        int i6 = i5 + 1;
        C2491fm a3 = C4105zY.m41624a(ShipStructureType.class, "3e4d7aaa1c521c92ac1fa4cce65a6e5d", i6);
        fvv = a3;
        fmVarArr[i6] = a3;
        int i7 = i6 + 1;
        C2491fm a4 = C4105zY.m41624a(ShipStructureType.class, "47bef956e0f7b52feb721ed16ddb5183", i7);
        ijM = a4;
        fmVarArr[i7] = a4;
        int i8 = i7 + 1;
        C2491fm a5 = C4105zY.m41624a(ShipStructureType.class, "46c2012f215f89d06359f534f406f50d", i8);
        aTt = a5;
        fmVarArr[i8] = a5;
        int i9 = i8 + 1;
        C2491fm a6 = C4105zY.m41624a(ShipStructureType.class, "0afe9760ec0e83c4812bbd251ee3fe4d", i9);
        geH = a6;
        fmVarArr[i9] = a6;
        int i10 = i9 + 1;
        C2491fm a7 = C4105zY.m41624a(ShipStructureType.class, "9a9af1d63e60687c91c85d06540e764a", i10);
        aDR = a7;
        fmVarArr[i10] = a7;
        int i11 = i10 + 1;
        C2491fm a8 = C4105zY.m41624a(ShipStructureType.class, "055ba9758752fc2c33da0f39cde87b71", i11);
        ijN = a8;
        fmVarArr[i11] = a8;
        int i12 = i11 + 1;
        C2491fm a9 = C4105zY.m41624a(ShipStructureType.class, "a1867b883cafb1fe7348d083b636b28f", i12);
        f3247dN = a9;
        fmVarArr[i12] = a9;
        int i13 = i12 + 1;
        _m_methods = fmVarArr;
        C1634Xv.m11725a((Object[]) BaseTaikodomContent._m_methods, (Object[]) _m_methods);
        ___iScriptClass = aUO.m18566a(ShipStructureType.class, C6482anm.class, _m_fields, _m_methods);
    }

    private C3438ra aHC() {
        return (C3438ra) bFf().mo5608dq().mo3214p(cJJ);
    }

    /* renamed from: az */
    private void m15984az(C3438ra raVar) {
        bFf().mo5608dq().mo3197f(cJJ, raVar);
    }

    private ShieldType clI() {
        return (ShieldType) bFf().mo5608dq().mo3214p(dmj);
    }

    /* renamed from: f */
    private void m15987f(ShieldType aph) {
        bFf().mo5608dq().mo3197f(dmj, aph);
    }

    /* renamed from: T */
    public C6494any mo11T() {
        return ___iScriptClass;
    }

    /* renamed from: U */
    public C2491fm[] mo12U() {
        return _m_methods;
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Shield")
    /* renamed from: VX */
    public ShieldType mo9731VX() {
        switch (bFf().mo6893i(aTt)) {
            case 0:
                return null;
            case 2:
                return (ShieldType) bFf().mo5606d(new aCE(this, aTt, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, aTt, new Object[0]));
                break;
        }
        return m15982VW();
    }

    /* renamed from: W */
    public Object mo13W() {
        return new C6482anm(this);
    }

    /* renamed from: a */
    public Object mo14a(C0495Gr gr) {
        Object[] args = gr.getArgs();
        switch (gr.mo2417hq() - BaseTaikodomContent._m_methodCount) {
            case 0:
                m15986f((ShipSectorType) args[0]);
                return null;
            case 1:
                m15989h((ShipSectorType) args[0]);
                return null;
            case 2:
                return bVF();
            case 3:
                return dgE();
            case 4:
                return m15982VW();
            case 5:
                m15988g((ShieldType) args[0]);
                return null;
            case 6:
                m15985e((aDJ) args[0]);
                return null;
            case 7:
                return dgG();
            case 8:
                return m15983aT();
            default:
                return super.mo14a(gr);
        }
    }

    /* renamed from: a */
    public void mo15a(Collection collection, C0495Gr gr) {
        super.mo15a(collection, gr);
    }

    /* access modifiers changed from: protected */
    /* renamed from: aU */
    public <T extends aDJ> T mo745aU() {
        switch (bFf().mo6893i(f3247dN)) {
            case 0:
                return null;
            case 2:
                return (aDJ) bFf().mo5606d(new aCE(this, f3247dN, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f3247dN, new Object[0]));
                break;
        }
        return m15983aT();
    }

    public C3438ra<ShipSectorType> bVG() {
        switch (bFf().mo6893i(fvv)) {
            case 0:
                return null;
            case 2:
                return (C3438ra) bFf().mo5606d(new aCE(this, fvv, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, fvv, new Object[0]));
                break;
        }
        return bVF();
    }

    /* renamed from: c */
    public C5663aRz[] mo25c() {
        return _m_fields;
    }

    @C3248pc(aYR = C3248pc.C3250b.ITERATOR, displayName = "Ship Sectors")
    public List<ShipSectorType> dgF() {
        switch (bFf().mo6893i(ijM)) {
            case 0:
                return null;
            case 2:
                return (List) bFf().mo5606d(new aCE(this, ijM, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, ijM, new Object[0]));
                break;
        }
        return dgE();
    }

    public ShipStructure dgH() {
        switch (bFf().mo6893i(ijN)) {
            case 0:
                return null;
            case 2:
                return (ShipStructure) bFf().mo5606d(new aCE(this, ijN, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, ijN, new Object[0]));
                break;
        }
        return dgG();
    }

    /* renamed from: f */
    public void mo2631f(aDJ adj) {
        switch (bFf().mo6893i(aDR)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, aDR, new Object[]{adj}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, aDR, new Object[]{adj}));
                break;
        }
        m15985e(adj);
    }

    @C3248pc(aYR = C3248pc.C3250b.ADDER, displayName = "Ship Sectors")
    /* renamed from: g */
    public void mo9735g(ShipSectorType aux) {
        switch (bFf().mo6893i(ijK)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, ijK, new Object[]{aux}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, ijK, new Object[]{aux}));
                break;
        }
        m15986f(aux);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Shield")
    /* renamed from: h */
    public void mo9736h(ShieldType aph) {
        switch (bFf().mo6893i(geH)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, geH, new Object[]{aph}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, geH, new Object[]{aph}));
                break;
        }
        m15988g(aph);
    }

    @C3248pc(aYR = C3248pc.C3250b.REMOVER, displayName = "Ship Sectors")
    /* renamed from: i */
    public void mo9737i(ShipSectorType aux) {
        switch (bFf().mo6893i(ijL)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, ijL, new Object[]{aux}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, ijL, new Object[]{aux}));
                break;
        }
        m15989h(aux);
    }

    /* renamed from: S */
    public void mo10S() {
        super.mo10S();
    }

    @C3248pc(aYR = C3248pc.C3250b.ADDER, displayName = "Ship Sectors")
    @C0064Am(aul = "c9df3af6be28b42ae352cfc89afb8c57", aum = 0)
    /* renamed from: f */
    private void m15986f(ShipSectorType aux) {
        aHC().add(aux);
    }

    @C3248pc(aYR = C3248pc.C3250b.REMOVER, displayName = "Ship Sectors")
    @C0064Am(aul = "934488924d0c797197bfdc617dcdf6c9", aum = 0)
    /* renamed from: h */
    private void m15989h(ShipSectorType aux) {
        aHC().remove(aux);
    }

    @C0064Am(aul = "3e4d7aaa1c521c92ac1fa4cce65a6e5d", aum = 0)
    private C3438ra<ShipSectorType> bVF() {
        return aHC();
    }

    @C3248pc(aYR = C3248pc.C3250b.ITERATOR, displayName = "Ship Sectors")
    @C0064Am(aul = "47bef956e0f7b52feb721ed16ddb5183", aum = 0)
    private List<ShipSectorType> dgE() {
        return Collections.unmodifiableList(aHC());
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Shield")
    @C0064Am(aul = "46c2012f215f89d06359f534f406f50d", aum = 0)
    /* renamed from: VW */
    private ShieldType m15982VW() {
        return clI();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Shield")
    @C0064Am(aul = "0afe9760ec0e83c4812bbd251ee3fe4d", aum = 0)
    /* renamed from: g */
    private void m15988g(ShieldType aph) {
        m15987f(aph);
    }

    @C0064Am(aul = "9a9af1d63e60687c91c85d06540e764a", aum = 0)
    /* renamed from: e */
    private void m15985e(aDJ adj) {
        super.mo2631f(adj);
        if (adj instanceof ShipStructure) {
            C1616Xf xf = adj;
            if (clI() != null) {
                xf.mo6765g(aLI.aMI, clI().mo7459NK());
            }
        }
    }

    @C0064Am(aul = "055ba9758752fc2c33da0f39cde87b71", aum = 0)
    private ShipStructure dgG() {
        return (ShipStructure) mo745aU();
    }

    @C0064Am(aul = "a1867b883cafb1fe7348d083b636b28f", aum = 0)
    /* renamed from: aT */
    private <T extends aDJ> T m15983aT() {
        T t = (ShipStructure) bFf().mo6865M(ShipStructure.class);
        t.mo13244i(this);
        return t;
    }
}
