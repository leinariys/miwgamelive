package game.script.ship;

import com.hoplon.geometry.Vec3f;
import game.network.message.externalizable.aCE;
import game.script.Actor;
import game.script.resource.Asset;
import game.script.simulation.Space;
import game.script.space.SpaceCategory;
import game.script.template.BaseTaikodomContent;
import logic.aaa.C2235dB;
import logic.baa.*;
import logic.bbb.C4029yK;
import logic.data.mbean.aWl;
import logic.res.LoaderTrail;
import logic.res.code.C5640aRc;
import logic.res.code.C5663aRz;
import logic.res.code.C6494any;
import logic.res.html.C2491fm;
import p001a.*;

import java.util.Collection;
import java.util.UUID;

@C2712iu(mo19785Bs = {C6251ajP.class}, mo19786Bt = BaseTaikodomContent.class)
@C5511aMd
@C6485anp
/* renamed from: a.Un */
/* compiled from: a */
public class Scenery extends Actor implements C0468GU, C1616Xf {
    /* renamed from: LR */
    public static final C5663aRz f1799LR = null;
    /* renamed from: Lm */
    public static final C2491fm f1800Lm = null;
    /* renamed from: MO */
    public static final C2491fm f1801MO = null;
    /* renamed from: MP */
    public static final C2491fm f1802MP = null;
    /* renamed from: Ml */
    public static final C5663aRz f1804Ml = null;
    public static final C2491fm _f_getType_0020_0028_0029Ljava_002flang_002fObject_003b = null;
    public static final C2491fm _f_onResurrect_0020_0028_0029V = null;
    public static final C2491fm _f_toString_0020_0028_0029Ljava_002flang_002fString_003b = null;
    public static final int _m_fieldCount = 0;
    public static final C5663aRz[] _m_fields = null;
    public static final int _m_methodCount = 0;
    public static final C2491fm[] _m_methods = null;
    /* renamed from: bL */
    public static final C5663aRz f1806bL = null;
    /* renamed from: bM */
    public static final C5663aRz f1807bM = null;
    /* renamed from: bN */
    public static final C2491fm f1808bN = null;
    /* renamed from: bO */
    public static final C2491fm f1809bO = null;
    /* renamed from: bP */
    public static final C2491fm f1810bP = null;
    /* renamed from: bQ */
    public static final C2491fm f1811bQ = null;
    public static final C5663aRz enM = null;
    public static final C2491fm enN = null;
    public static final long serialVersionUID = 0;
    /* renamed from: uU */
    public static final C5663aRz f1813uU = null;
    /* renamed from: uY */
    public static final C2491fm f1814uY = null;
    /* renamed from: vc */
    public static final C2491fm f1815vc = null;
    /* renamed from: ve */
    public static final C2491fm f1816ve = null;
    /* renamed from: vi */
    public static final C2491fm f1817vi = null;
    /* renamed from: vm */
    public static final C2491fm f1818vm = null;
    /* renamed from: vn */
    public static final C2491fm f1819vn = null;
    /* renamed from: vo */
    public static final C2491fm f1820vo = null;
    /* renamed from: vs */
    public static final C2491fm f1821vs = null;
    public static C6494any ___iScriptClass;
    @C0064Am(aul = "1f650898fe50a0256e6e21dbd0f87ca6", aum = 0)

    /* renamed from: LQ */
    private static Asset f1798LQ;
    @C0064Am(aul = "38eef692bda6f0788db3937d52b975a8", aum = 3)

    /* renamed from: Mk */
    private static SpaceCategory f1803Mk;
    @C0064Am(aul = "afb43e15287f047d21fa9b98ede2b27e", aum = 4)

    /* renamed from: bK */
    private static UUID f1805bK;
    @C0064Am(aul = "cf58da7a825537725bb6288f36fea103", aum = 2)
    private static boolean enL;
    @C0064Am(aul = "5c0e80cc70406dbfe98c80edf3164c9f", aum = 5)
    private static String handle;
    @C0064Am(aul = "ef86f0e71d260db379fdf86ca8cc9cae", aum = 1)

    /* renamed from: uT */
    private static String f1812uT;

    static {
        m10341V();
    }

    public Scenery() {
        super((C5540aNg) null);
        super.mo10S();
    }

    public Scenery(C5540aNg ang) {
        super(ang);
    }

    public Scenery(SceneryType atc) {
        super((C5540aNg) null);
        super._m_script_init(atc);
    }

    /* renamed from: V */
    static void m10341V() {
        serialVersionUID = (long) 1;
        _m_fieldCount = Actor._m_fieldCount + 6;
        _m_methodCount = Actor._m_methodCount + 19;
        int i = Actor._m_fieldCount;
        C5663aRz[] arzArr = new C5663aRz[(i + 6)];
        C5663aRz b = C5640aRc.m17844b(Scenery.class, "1f650898fe50a0256e6e21dbd0f87ca6", i);
        f1799LR = b;
        arzArr[i] = b;
        int i2 = i + 1;
        C5663aRz b2 = C5640aRc.m17844b(Scenery.class, "ef86f0e71d260db379fdf86ca8cc9cae", i2);
        f1813uU = b2;
        arzArr[i2] = b2;
        int i3 = i2 + 1;
        C5663aRz b3 = C5640aRc.m17844b(Scenery.class, "cf58da7a825537725bb6288f36fea103", i3);
        enM = b3;
        arzArr[i3] = b3;
        int i4 = i3 + 1;
        C5663aRz b4 = C5640aRc.m17844b(Scenery.class, "38eef692bda6f0788db3937d52b975a8", i4);
        f1804Ml = b4;
        arzArr[i4] = b4;
        int i5 = i4 + 1;
        C5663aRz b5 = C5640aRc.m17844b(Scenery.class, "afb43e15287f047d21fa9b98ede2b27e", i5);
        f1806bL = b5;
        arzArr[i5] = b5;
        int i6 = i5 + 1;
        C5663aRz b6 = C5640aRc.m17844b(Scenery.class, "5c0e80cc70406dbfe98c80edf3164c9f", i6);
        f1807bM = b6;
        arzArr[i6] = b6;
        int i7 = i6 + 1;
        _m_fields = arzArr;
        C1634Xv.m11725a((Object[]) Actor._m_fields, (Object[]) _m_fields);
        int i8 = Actor._m_methodCount;
        C2491fm[] fmVarArr = new C2491fm[(i8 + 19)];
        C2491fm a = C4105zY.m41624a(Scenery.class, "3a155b48b040c3770c28c7927582e21e", i8);
        f1808bN = a;
        fmVarArr[i8] = a;
        int i9 = i8 + 1;
        C2491fm a2 = C4105zY.m41624a(Scenery.class, "ef9f10c3d6d3c4e828e4ced5dacbf9ee", i9);
        f1809bO = a2;
        fmVarArr[i9] = a2;
        int i10 = i9 + 1;
        C2491fm a3 = C4105zY.m41624a(Scenery.class, "daec193e78b17d20b3aa2d32ea82f6c3", i10);
        f1810bP = a3;
        fmVarArr[i10] = a3;
        int i11 = i10 + 1;
        C2491fm a4 = C4105zY.m41624a(Scenery.class, "5047a9da2a5fff0add43deaf731bd6cc", i11);
        f1811bQ = a4;
        fmVarArr[i11] = a4;
        int i12 = i11 + 1;
        C2491fm a5 = C4105zY.m41624a(Scenery.class, "35968400e3e161403d97de34a9836616", i12);
        _f_toString_0020_0028_0029Ljava_002flang_002fString_003b = a5;
        fmVarArr[i12] = a5;
        int i13 = i12 + 1;
        C2491fm a6 = C4105zY.m41624a(Scenery.class, "ed73a41b8591f9052e540cdad695953c", i13);
        enN = a6;
        fmVarArr[i13] = a6;
        int i14 = i13 + 1;
        C2491fm a7 = C4105zY.m41624a(Scenery.class, "8f4351a9096ce4b34746df4b045445c5", i14);
        f1815vc = a7;
        fmVarArr[i14] = a7;
        int i15 = i14 + 1;
        C2491fm a8 = C4105zY.m41624a(Scenery.class, "a32699cd3dd3144bc45873f3c6d0e026", i15);
        f1816ve = a8;
        fmVarArr[i15] = a8;
        int i16 = i15 + 1;
        C2491fm a9 = C4105zY.m41624a(Scenery.class, "84334d1830dcd1c1bea2655a7bea2f9c", i16);
        f1819vn = a9;
        fmVarArr[i16] = a9;
        int i17 = i16 + 1;
        C2491fm a10 = C4105zY.m41624a(Scenery.class, "a25f03eb7f11ab1d91a2f7d46b3f2f59", i17);
        f1820vo = a10;
        fmVarArr[i17] = a10;
        int i18 = i17 + 1;
        C2491fm a11 = C4105zY.m41624a(Scenery.class, "ec7dff7ec3140788001cd0a67649e1f7", i18);
        f1818vm = a11;
        fmVarArr[i18] = a11;
        int i19 = i18 + 1;
        C2491fm a12 = C4105zY.m41624a(Scenery.class, "559b0b69dded1fbcd5a101b66c207054", i19);
        _f_onResurrect_0020_0028_0029V = a12;
        fmVarArr[i19] = a12;
        int i20 = i19 + 1;
        C2491fm a13 = C4105zY.m41624a(Scenery.class, "ed41fb5c29b3c4e613a20cee9ff4ff2d", i20);
        f1817vi = a13;
        fmVarArr[i20] = a13;
        int i21 = i20 + 1;
        C2491fm a14 = C4105zY.m41624a(Scenery.class, "6661a4f2cf6ca63ee1a544c4ad339f63", i21);
        f1821vs = a14;
        fmVarArr[i21] = a14;
        int i22 = i21 + 1;
        C2491fm a15 = C4105zY.m41624a(Scenery.class, "b9b7d22f9c1856cc4bcbb23068632937", i22);
        f1800Lm = a15;
        fmVarArr[i22] = a15;
        int i23 = i22 + 1;
        C2491fm a16 = C4105zY.m41624a(Scenery.class, "80b3a0d81e07ebd7d73133946b9fc782", i23);
        f1814uY = a16;
        fmVarArr[i23] = a16;
        int i24 = i23 + 1;
        C2491fm a17 = C4105zY.m41624a(Scenery.class, "e3cecb7b0b6e87b8e394771468762234", i24);
        f1801MO = a17;
        fmVarArr[i24] = a17;
        int i25 = i24 + 1;
        C2491fm a18 = C4105zY.m41624a(Scenery.class, "347a9a2e4a6cd766fd629ab2fcdd1e25", i25);
        f1802MP = a18;
        fmVarArr[i25] = a18;
        int i26 = i25 + 1;
        C2491fm a19 = C4105zY.m41624a(Scenery.class, "2006c514f1cc587a0435056f8d8cd337", i26);
        _f_getType_0020_0028_0029Ljava_002flang_002fObject_003b = a19;
        fmVarArr[i26] = a19;
        int i27 = i26 + 1;
        _m_methods = fmVarArr;
        C1634Xv.m11725a((Object[]) Actor._m_methods, (Object[]) _m_methods);
        ___iScriptClass = aUO.m18566a(Scenery.class, aWl.class, _m_fields, _m_methods);
    }

    /* renamed from: H */
    private void m10340H(String str) {
        throw new C6039afL();
    }

    /* renamed from: a */
    private void m10342a(SpaceCategory ak) {
        bFf().mo5608dq().mo3197f(f1804Ml, ak);
    }

    /* renamed from: a */
    private void m10345a(String str) {
        bFf().mo5608dq().mo3197f(f1807bM, str);
    }

    /* renamed from: a */
    private void m10346a(UUID uuid) {
        bFf().mo5608dq().mo3197f(f1806bL, uuid);
    }

    /* renamed from: an */
    private UUID m10348an() {
        return (UUID) bFf().mo5608dq().mo3214p(f1806bL);
    }

    /* renamed from: ao */
    private String m10349ao() {
        return (String) bFf().mo5608dq().mo3214p(f1807bM);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Space Category")
    @C0064Am(aul = "347a9a2e4a6cd766fd629ab2fcdd1e25", aum = 0)
    @C5566aOg
    /* renamed from: b */
    private void m10353b(SpaceCategory ak) {
        throw new aWi(new aCE(this, f1802MP, new Object[]{ak}));
    }

    private boolean bxE() {
        return ((SceneryType) getType()).cuW();
    }

    /* renamed from: c */
    private void m10358c(UUID uuid) {
        switch (bFf().mo6893i(f1809bO)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f1809bO, new Object[]{uuid}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f1809bO, new Object[]{uuid}));
                break;
        }
        m10355b(uuid);
    }

    /* renamed from: dw */
    private void m10359dw(boolean z) {
        throw new C6039afL();
    }

    /* renamed from: ij */
    private String m10360ij() {
        return ((SceneryType) getType()).mo16103iu();
    }

    @C0064Am(aul = "b9b7d22f9c1856cc4bcbb23068632937", aum = 0)
    @C5566aOg
    @C2499fr
    /* renamed from: qU */
    private void m10365qU() {
        throw new aWi(new aCE(this, f1800Lm, new Object[0]));
    }

    @C0064Am(aul = "2006c514f1cc587a0435056f8d8cd337", aum = 0)
    /* renamed from: qW */
    private Object m10366qW() {
        return bxG();
    }

    /* renamed from: r */
    private void m10367r(Asset tCVar) {
        throw new C6039afL();
    }

    /* renamed from: rh */
    private Asset m10369rh() {
        return ((SceneryType) getType()).mo3521Nu();
    }

    /* renamed from: rr */
    private SpaceCategory m10370rr() {
        return (SpaceCategory) bFf().mo5608dq().mo3214p(f1804Ml);
    }

    /* renamed from: S */
    public void mo10S() {
        super.mo10S();
    }

    /* renamed from: T */
    public C6494any mo11T() {
        return ___iScriptClass;
    }

    /* renamed from: U */
    public C2491fm[] mo12U() {
        return _m_methods;
    }

    /* renamed from: W */
    public Object mo13W() {
        return new aWl(this);
    }

    /* renamed from: a */
    public Object mo14a(C0495Gr gr) {
        Object[] args = gr.getArgs();
        switch (gr.mo2417hq() - Actor._m_methodCount) {
            case 0:
                return m10350ap();
            case 1:
                m10355b((UUID) args[0]);
                return null;
            case 2:
                return m10351ar();
            case 3:
                m10354b((String) args[0]);
                return null;
            case 4:
                return m10352au();
            case 5:
                return bxF();
            case 6:
                return m10361io();
            case 7:
                return m10362iq();
            case 8:
                m10357c((Actor) args[0], (C5260aCm) args[1], (Vec3f) args[2], (Vec3f) args[3]);
                return null;
            case 9:
                return m10364iz();
            case 10:
                m10344a((Actor) args[0], (C5260aCm) args[1], (Vec3f) args[2], (Vec3f) args[3]);
                return null;
            case 11:
                m10347aG();
                return null;
            case 12:
                return m10363it();
            case 13:
                m10343a((Actor.C0200h) args[0]);
                return null;
            case 14:
                m10365qU();
                return null;
            case 15:
                return m10356c((Space) args[0], ((Long) args[1]).longValue());
            case 16:
                return m10368rQ();
            case 17:
                m10353b((SpaceCategory) args[0]);
                return null;
            case 18:
                return m10366qW();
            default:
                return super.mo14a(gr);
        }
    }

    /* renamed from: a */
    public void mo15a(Collection collection, C0495Gr gr) {
        super.mo15a(collection, gr);
    }

    /* renamed from: aH */
    public void mo70aH() {
        switch (bFf().mo6893i(_f_onResurrect_0020_0028_0029V)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, _f_onResurrect_0020_0028_0029V, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, _f_onResurrect_0020_0028_0029V, new Object[0]));
                break;
        }
        m10347aG();
    }

    /* renamed from: aq */
    public UUID mo18aq() {
        switch (bFf().mo6893i(f1808bN)) {
            case 0:
                return null;
            case 2:
                return (UUID) bFf().mo5606d(new aCE(this, f1808bN, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f1808bN, new Object[0]));
                break;
        }
        return m10350ap();
    }

    /* renamed from: b */
    public void mo620b(Actor.C0200h hVar) {
        switch (bFf().mo6893i(f1821vs)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f1821vs, new Object[]{hVar}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f1821vs, new Object[]{hVar}));
                break;
        }
        m10343a(hVar);
    }

    /* renamed from: b */
    public void mo621b(Actor cr, C5260aCm acm, Vec3f vec3f, Vec3f vec3f2) {
        switch (bFf().mo6893i(f1818vm)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f1818vm, new Object[]{cr, acm, vec3f, vec3f2}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f1818vm, new Object[]{cr, acm, vec3f, vec3f2}));
                break;
        }
        m10344a(cr, acm, vec3f, vec3f2);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Type")
    public SceneryType bxG() {
        switch (bFf().mo6893i(enN)) {
            case 0:
                return null;
            case 2:
                return (SceneryType) bFf().mo5606d(new aCE(this, enN, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, enN, new Object[0]));
                break;
        }
        return bxF();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Space Category")
    @C5566aOg
    /* renamed from: c */
    public void mo5911c(SpaceCategory ak) {
        switch (bFf().mo6893i(f1802MP)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f1802MP, new Object[]{ak}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f1802MP, new Object[]{ak}));
                break;
        }
        m10353b(ak);
    }

    /* renamed from: c */
    public C5663aRz[] mo25c() {
        return _m_fields;
    }

    /* access modifiers changed from: protected */
    @C2198cg
    @C1253SX
    /* renamed from: d */
    public C0520HN mo635d(Space ea, long j) {
        switch (bFf().mo6893i(f1814uY)) {
            case 0:
                return null;
            case 2:
                return (C0520HN) bFf().mo5606d(new aCE(this, f1814uY, new Object[]{ea, new Long(j)}));
            case 3:
                bFf().mo5606d(new aCE(this, f1814uY, new Object[]{ea, new Long(j)}));
                break;
        }
        return m10356c(ea, j);
    }

    /* renamed from: d */
    public void mo636d(Actor cr, C5260aCm acm, Vec3f vec3f, Vec3f vec3f2) {
        switch (bFf().mo6893i(f1819vn)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f1819vn, new Object[]{cr, acm, vec3f, vec3f2}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f1819vn, new Object[]{cr, acm, vec3f, vec3f2}));
                break;
        }
        m10357c(cr, acm, vec3f, vec3f2);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Handle")
    public String getHandle() {
        switch (bFf().mo6893i(f1810bP)) {
            case 0:
                return null;
            case 2:
                return (String) bFf().mo5606d(new aCE(this, f1810bP, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f1810bP, new Object[0]));
                break;
        }
        return m10351ar();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Handle")
    public void setHandle(String str) {
        switch (bFf().mo6893i(f1811bQ)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f1811bQ, new Object[]{str}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f1811bQ, new Object[]{str}));
                break;
        }
        m10354b(str);
    }

    public /* bridge */ /* synthetic */ Object getType() {
        switch (bFf().mo6893i(_f_getType_0020_0028_0029Ljava_002flang_002fObject_003b)) {
            case 0:
                return null;
            case 2:
                return bFf().mo5606d(new aCE(this, _f_getType_0020_0028_0029Ljava_002flang_002fObject_003b, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, _f_getType_0020_0028_0029Ljava_002flang_002fObject_003b, new Object[0]));
                break;
        }
        return m10366qW();
    }

    /* renamed from: iA */
    public String mo647iA() {
        switch (bFf().mo6893i(f1820vo)) {
            case 0:
                return null;
            case 2:
                return (String) bFf().mo5606d(new aCE(this, f1820vo, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f1820vo, new Object[0]));
                break;
        }
        return m10364iz();
    }

    /* renamed from: ip */
    public String mo648ip() {
        switch (bFf().mo6893i(f1815vc)) {
            case 0:
                return null;
            case 2:
                return (String) bFf().mo5606d(new aCE(this, f1815vc, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f1815vc, new Object[0]));
                break;
        }
        return m10361io();
    }

    /* renamed from: ir */
    public String mo649ir() {
        switch (bFf().mo6893i(f1816ve)) {
            case 0:
                return null;
            case 2:
                return (String) bFf().mo5606d(new aCE(this, f1816ve, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f1816ve, new Object[0]));
                break;
        }
        return m10362iq();
    }

    /* renamed from: iu */
    public String mo651iu() {
        switch (bFf().mo6893i(f1817vi)) {
            case 0:
                return null;
            case 2:
                return (String) bFf().mo5606d(new aCE(this, f1817vi, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f1817vi, new Object[0]));
                break;
        }
        return m10363it();
    }

    @C5566aOg
    @C2499fr
    /* renamed from: qV */
    public void mo656qV() {
        switch (bFf().mo6893i(f1800Lm)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f1800Lm, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f1800Lm, new Object[0]));
                break;
        }
        m10365qU();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Space Category")
    /* renamed from: rR */
    public SpaceCategory mo5913rR() {
        switch (bFf().mo6893i(f1801MO)) {
            case 0:
                return null;
            case 2:
                return (SpaceCategory) bFf().mo5606d(new aCE(this, f1801MO, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f1801MO, new Object[0]));
                break;
        }
        return m10368rQ();
    }

    public String toString() {
        switch (bFf().mo6893i(_f_toString_0020_0028_0029Ljava_002flang_002fString_003b)) {
            case 0:
                return null;
            case 2:
                return (String) bFf().mo5606d(new aCE(this, _f_toString_0020_0028_0029Ljava_002flang_002fString_003b, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, _f_toString_0020_0028_0029Ljava_002flang_002fString_003b, new Object[0]));
                break;
        }
        return m10352au();
    }

    /* renamed from: i */
    public void mo5912i(SceneryType atc) {
        super.mo967a((C2961mJ) atc);
        setStatic(true);
        m10346a(UUID.randomUUID());
        setHandle(String.valueOf(bxG().getHandle()) + "_" + cWm());
    }

    @C0064Am(aul = "3a155b48b040c3770c28c7927582e21e", aum = 0)
    /* renamed from: ap */
    private UUID m10350ap() {
        return m10348an();
    }

    @C0064Am(aul = "ef9f10c3d6d3c4e828e4ced5dacbf9ee", aum = 0)
    /* renamed from: b */
    private void m10355b(UUID uuid) {
        m10346a(uuid);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Handle")
    @C0064Am(aul = "daec193e78b17d20b3aa2d32ea82f6c3", aum = 0)
    /* renamed from: ar */
    private String m10351ar() {
        return m10349ao();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Handle")
    @C0064Am(aul = "5047a9da2a5fff0add43deaf731bd6cc", aum = 0)
    /* renamed from: b */
    private void m10354b(String str) {
        m10345a(str);
    }

    @C0064Am(aul = "35968400e3e161403d97de34a9836616", aum = 0)
    /* renamed from: au */
    private String m10352au() {
        return "Scenery: [" + m10349ao() + "]";
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Type")
    @C0064Am(aul = "ed73a41b8591f9052e540cdad695953c", aum = 0)
    private SceneryType bxF() {
        return (SceneryType) super.getType();
    }

    @C0064Am(aul = "8f4351a9096ce4b34746df4b045445c5", aum = 0)
    /* renamed from: io */
    private String m10361io() {
        return m10369rh().getHandle();
    }

    @C0064Am(aul = "a32699cd3dd3144bc45873f3c6d0e026", aum = 0)
    /* renamed from: iq */
    private String m10362iq() {
        return m10369rh().getFile();
    }

    @C0064Am(aul = "84334d1830dcd1c1bea2655a7bea2f9c", aum = 0)
    /* renamed from: c */
    private void m10357c(Actor cr, C5260aCm acm, Vec3f vec3f, Vec3f vec3f2) {
    }

    @C0064Am(aul = "a25f03eb7f11ab1d91a2f7d46b3f2f59", aum = 0)
    /* renamed from: iz */
    private String m10364iz() {
        return null;
    }

    @C0064Am(aul = "ec7dff7ec3140788001cd0a67649e1f7", aum = 0)
    /* renamed from: a */
    private void m10344a(Actor cr, C5260aCm acm, Vec3f vec3f, Vec3f vec3f2) {
    }

    @C0064Am(aul = "559b0b69dded1fbcd5a101b66c207054", aum = 0)
    /* renamed from: aG */
    private void m10347aG() {
        super.mo70aH();
        if (m10349ao() == null) {
            setHandle(String.valueOf(bxG().getHandle()) + "_" + cWm());
        }
    }

    @C0064Am(aul = "ed41fb5c29b3c4e613a20cee9ff4ff2d", aum = 0)
    /* renamed from: it */
    private String m10363it() {
        return m10360ij();
    }

    @C0064Am(aul = "6661a4f2cf6ca63ee1a544c4ad339f63", aum = 0)
    /* renamed from: a */
    private void m10343a(Actor.C0200h hVar) {
        if (m10369rh() == null) {
            throw new IllegalStateException("ShapedObject of class '" + getClass().getName() + "' does not have a RenderAsset. Type is " + bxG().getHandle() + ": " + bxG().cWm());
        }
        String file = m10369rh().getFile();
        if (!bGX() || !(ald().getLoaderTrail() instanceof LoaderTrail)) {
            hVar.mo1103c(C2606hU.m32703b(file, bxE()));
        } else {
            ((LoaderTrail) ald().getLoaderTrail()).mo19098a(file, new C1420a(hVar), getName(), bxE());
        }
    }

    @C0064Am(aul = "80b3a0d81e07ebd7d73133946b9fc782", aum = 0)
    @C2198cg
    @C1253SX
    /* renamed from: c */
    private C0520HN m10356c(Space ea, long j) {
        C6543aov aov = new C6543aov(ea, this, mo958IL());
        aov.mo2449a(ea.cKn().mo5725a(j, (C2235dB) aov, 13));
        return aov;
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Space Category")
    @C0064Am(aul = "e3cecb7b0b6e87b8e394771468762234", aum = 0)
    /* renamed from: rQ */
    private SpaceCategory m10368rQ() {
        return m10370rr();
    }

    /* renamed from: a.Un$a */
    class C1420a implements C2601hQ {
        private final /* synthetic */ Actor.C0200h eyn;

        C1420a(Actor.C0200h hVar) {
            this.eyn = hVar;
        }

        /* renamed from: b */
        public void mo663b(C4029yK yKVar) {
            this.eyn.mo1103c(yKVar);
        }
    }
}
