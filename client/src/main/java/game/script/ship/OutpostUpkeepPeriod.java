package game.script.ship;

import game.network.message.externalizable.aCE;
import game.script.TaikodomObject;
import game.script.TaskletImpl;
import logic.baa.*;
import logic.data.mbean.C3526rz;
import logic.data.mbean.awX;
import logic.res.code.C5640aRc;
import logic.res.code.C5663aRz;
import logic.res.code.C6494any;
import logic.res.html.C2491fm;
import p001a.*;

import java.util.Collection;

@C5829abJ("1.1.0")
@C6485anp
@C5511aMd
/* renamed from: a.Rc */
/* compiled from: a */
public class OutpostUpkeepPeriod extends TaikodomObject implements C1616Xf, aOW {

    /* renamed from: Do */
    public static final C2491fm f1497Do = null;
    public static final int _m_fieldCount = 0;
    public static final C5663aRz[] _m_fields = null;
    public static final int _m_methodCount = 0;
    public static final C2491fm[] _m_methods = null;
    public static final C2491fm bkF = null;
    public static final C5663aRz dXK = null;
    public static final C5663aRz dXL = null;
    public static final C5663aRz dXM = null;
    public static final C2491fm dXN = null;
    public static final C2491fm dXO = null;
    public static final C2491fm dXP = null;
    public static final C2491fm dXQ = null;
    public static final C2491fm dXR = null;
    public static final C2491fm dXS = null;
    public static final C2491fm dXT = null;
    public static final C2491fm dXU = null;
    public static final C2491fm dXV = null;
    public static final C5663aRz dfl = null;
    public static final long serialVersionUID = 0;
    private static final long dXI = 86400;
    private static final long dXJ = 86400000;
    public static C6494any ___iScriptClass = null;
    @C0064Am(aul = "961a6ebdf565dbb31ae526bf706969a8", aum = 0)
    private static Outpost baT = null;
    @C0064Am(aul = "6270b37cb7ee88d61df496e20b155aa4", aum = 1)
    private static long baU = 0;
    @C0064Am(aul = "6f267e3ae248e6e69d9f7c49beb986c4", aum = 2)
    private static boolean baV = false;
    @C0064Am(aul = "22c7217c166a2d185638cdc2f3d82b9e", aum = 3)
    private static OutpostUpkeepTicker baW = null;

    static {
        m9201V();
    }

    public OutpostUpkeepPeriod() {
        super((C5540aNg) null);
        super.mo10S();
    }

    public OutpostUpkeepPeriod(C5540aNg ang) {
        super(ang);
    }

    public OutpostUpkeepPeriod(Outpost qZVar) {
        super((C5540aNg) null);
        super._m_script_init(qZVar);
    }

    /* renamed from: V */
    static void m9201V() {
        serialVersionUID = (long) 1;
        _m_fieldCount = TaikodomObject._m_fieldCount + 4;
        _m_methodCount = TaikodomObject._m_methodCount + 11;
        int i = TaikodomObject._m_fieldCount;
        C5663aRz[] arzArr = new C5663aRz[(i + 4)];
        C5663aRz b = C5640aRc.m17844b(OutpostUpkeepPeriod.class, "961a6ebdf565dbb31ae526bf706969a8", i);
        dfl = b;
        arzArr[i] = b;
        int i2 = i + 1;
        C5663aRz b2 = C5640aRc.m17844b(OutpostUpkeepPeriod.class, "6270b37cb7ee88d61df496e20b155aa4", i2);
        dXK = b2;
        arzArr[i2] = b2;
        int i3 = i2 + 1;
        C5663aRz b3 = C5640aRc.m17844b(OutpostUpkeepPeriod.class, "6f267e3ae248e6e69d9f7c49beb986c4", i3);
        dXL = b3;
        arzArr[i3] = b3;
        int i4 = i3 + 1;
        C5663aRz b4 = C5640aRc.m17844b(OutpostUpkeepPeriod.class, "22c7217c166a2d185638cdc2f3d82b9e", i4);
        dXM = b4;
        arzArr[i4] = b4;
        int i5 = i4 + 1;
        _m_fields = arzArr;
        C1634Xv.m11725a((Object[]) TaikodomObject._m_fields, (Object[]) _m_fields);
        int i6 = TaikodomObject._m_methodCount;
        C2491fm[] fmVarArr = new C2491fm[(i6 + 11)];
        C2491fm a = C4105zY.m41624a(OutpostUpkeepPeriod.class, "c63a4492bed709a9ab8b2467b48cf858", i6);
        dXN = a;
        fmVarArr[i6] = a;
        int i7 = i6 + 1;
        C2491fm a2 = C4105zY.m41624a(OutpostUpkeepPeriod.class, "c957bb8e669504740480573f0a6164b1", i7);
        dXO = a2;
        fmVarArr[i7] = a2;
        int i8 = i7 + 1;
        C2491fm a3 = C4105zY.m41624a(OutpostUpkeepPeriod.class, "691bd1d6bb8251fcdd2a48b2bda38781", i8);
        bkF = a3;
        fmVarArr[i8] = a3;
        int i9 = i8 + 1;
        C2491fm a4 = C4105zY.m41624a(OutpostUpkeepPeriod.class, "7724ff070cbd4033051e669d8356f0dc", i9);
        dXP = a4;
        fmVarArr[i9] = a4;
        int i10 = i9 + 1;
        C2491fm a5 = C4105zY.m41624a(OutpostUpkeepPeriod.class, "3147d4262d766801b0b111732a50b946", i10);
        dXQ = a5;
        fmVarArr[i10] = a5;
        int i11 = i10 + 1;
        C2491fm a6 = C4105zY.m41624a(OutpostUpkeepPeriod.class, "96cdb3e560d053d4a6ab49a9a0b3710d", i11);
        dXR = a6;
        fmVarArr[i11] = a6;
        int i12 = i11 + 1;
        C2491fm a7 = C4105zY.m41624a(OutpostUpkeepPeriod.class, "3eb27bea2e64cad1dfff2d59e2852aab", i12);
        dXS = a7;
        fmVarArr[i12] = a7;
        int i13 = i12 + 1;
        C2491fm a8 = C4105zY.m41624a(OutpostUpkeepPeriod.class, "f8b7ccce32d9ede52302fa1827584f45", i13);
        dXT = a8;
        fmVarArr[i13] = a8;
        int i14 = i13 + 1;
        C2491fm a9 = C4105zY.m41624a(OutpostUpkeepPeriod.class, "08a379759af4ce6af61698a6c1ffabbe", i14);
        dXU = a9;
        fmVarArr[i14] = a9;
        int i15 = i14 + 1;
        C2491fm a10 = C4105zY.m41624a(OutpostUpkeepPeriod.class, "aa885f85c58552146eeccc0de593eb85", i15);
        dXV = a10;
        fmVarArr[i15] = a10;
        int i16 = i15 + 1;
        C2491fm a11 = C4105zY.m41624a(OutpostUpkeepPeriod.class, "6be8d476e8ac624c39f21dec5f6ea3f6", i16);
        f1497Do = a11;
        fmVarArr[i16] = a11;
        int i17 = i16 + 1;
        _m_methods = fmVarArr;
        C1634Xv.m11725a((Object[]) TaikodomObject._m_methods, (Object[]) _m_methods);
        ___iScriptClass = aUO.m18566a(OutpostUpkeepPeriod.class, C3526rz.class, _m_fields, _m_methods);
    }

    /* renamed from: a */
    private void m9203a(OutpostUpkeepTicker aVar) {
        bFf().mo5608dq().mo3197f(dXM, aVar);
    }

    private Outpost aWb() {
        return (Outpost) bFf().mo5608dq().mo3214p(dfl);
    }

    @C0064Am(aul = "691bd1d6bb8251fcdd2a48b2bda38781", aum = 0)
    @C5566aOg
    private void adk() {
        throw new aWi(new aCE(this, bkF, new Object[0]));
    }

    private long bqP() {
        return bFf().mo5608dq().mo3213o(dXK);
    }

    private boolean bqQ() {
        return bFf().mo5608dq().mo3201h(dXL);
    }

    private OutpostUpkeepTicker bqR() {
        return (OutpostUpkeepTicker) bFf().mo5608dq().mo3214p(dXM);
    }

    /* access modifiers changed from: private */
    public void bqT() {
        switch (bFf().mo6893i(dXN)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, dXN, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, dXN, new Object[0]));
                break;
        }
        bqS();
    }

    @C0064Am(aul = "7724ff070cbd4033051e669d8356f0dc", aum = 0)
    @C5566aOg
    private void bqU() {
        throw new aWi(new aCE(this, dXP, new Object[0]));
    }

    private long brb() {
        switch (bFf().mo6893i(dXS)) {
            case 0:
                return 0;
            case 2:
                return ((Long) bFf().mo5606d(new aCE(this, dXS, new Object[0]))).longValue();
            case 3:
                bFf().mo5606d(new aCE(this, dXS, new Object[0]));
                break;
        }
        return bra();
    }

    private long brd() {
        switch (bFf().mo6893i(dXT)) {
            case 0:
                return 0;
            case 2:
                return ((Long) bFf().mo5606d(new aCE(this, dXT, new Object[0]))).longValue();
            case 3:
                bFf().mo5606d(new aCE(this, dXT, new Object[0]));
                break;
        }
        return brc();
    }

    private long brf() {
        switch (bFf().mo6893i(dXU)) {
            case 0:
                return 0;
            case 2:
                return ((Long) bFf().mo5606d(new aCE(this, dXU, new Object[0]))).longValue();
            case 3:
                bFf().mo5606d(new aCE(this, dXU, new Object[0]));
                break;
        }
        return bre();
    }

    /* renamed from: dj */
    private void m9205dj(boolean z) {
        bFf().mo5608dq().mo3153a(dXL, z);
    }

    /* renamed from: e */
    private void m9206e(Outpost qZVar) {
        bFf().mo5608dq().mo3197f(dfl, qZVar);
    }

    /* renamed from: fB */
    private boolean m9208fB(long j) {
        switch (bFf().mo6893i(dXV)) {
            case 0:
                return false;
            case 2:
                return ((Boolean) bFf().mo5606d(new aCE(this, dXV, new Object[]{new Long(j)}))).booleanValue();
            case 3:
                bFf().mo5606d(new aCE(this, dXV, new Object[]{new Long(j)}));
                break;
        }
        return m9207fA(j);
    }

    /* renamed from: fx */
    private void m9209fx(long j) {
        bFf().mo5608dq().mo3184b(dXK, j);
    }

    /* renamed from: S */
    public void mo10S() {
        super.mo10S();
    }

    /* renamed from: T */
    public C6494any mo11T() {
        return ___iScriptClass;
    }

    /* renamed from: U */
    public C2491fm[] mo12U() {
        return _m_methods;
    }

    /* renamed from: W */
    public Object mo13W() {
        return new C3526rz(this);
    }

    /* renamed from: a */
    public Object mo14a(C0495Gr gr) {
        Object[] args = gr.getArgs();
        switch (gr.mo2417hq() - TaikodomObject._m_methodCount) {
            case 0:
                bqS();
                return null;
            case 1:
                m9210fy(((Long) args[0]).longValue());
                return null;
            case 2:
                adk();
                return null;
            case 3:
                bqU();
                return null;
            case 4:
                return new Boolean(bqW());
            case 5:
                return new Integer(bqY());
            case 6:
                return new Long(bra());
            case 7:
                return new Long(brc());
            case 8:
                return new Long(bre());
            case 9:
                return new Boolean(m9207fA(((Long) args[0]).longValue()));
            case 10:
                m9202a((C0665JT) args[0]);
                return null;
            default:
                return super.mo14a(gr);
        }
    }

    /* renamed from: a */
    public void mo15a(Collection collection, C0495Gr gr) {
        super.mo15a(collection, gr);
    }

    /* renamed from: b */
    public void mo24b(C0665JT jt) {
        switch (bFf().mo6893i(f1497Do)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f1497Do, new Object[]{jt}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f1497Do, new Object[]{jt}));
                break;
        }
        m9202a(jt);
    }

    /* access modifiers changed from: package-private */
    @C5566aOg
    public void bqV() {
        switch (bFf().mo6893i(dXP)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, dXP, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, dXP, new Object[0]));
                break;
        }
        bqU();
    }

    /* access modifiers changed from: package-private */
    public boolean bqX() {
        switch (bFf().mo6893i(dXQ)) {
            case 0:
                return false;
            case 2:
                return ((Boolean) bFf().mo5606d(new aCE(this, dXQ, new Object[0]))).booleanValue();
            case 3:
                bFf().mo5606d(new aCE(this, dXQ, new Object[0]));
                break;
        }
        return bqW();
    }

    /* access modifiers changed from: package-private */
    public int bqZ() {
        switch (bFf().mo6893i(dXR)) {
            case 0:
                return 0;
            case 2:
                return ((Integer) bFf().mo5606d(new aCE(this, dXR, new Object[0]))).intValue();
            case 3:
                bFf().mo5606d(new aCE(this, dXR, new Object[0]));
                break;
        }
        return bqY();
    }

    /* renamed from: c */
    public C5663aRz[] mo25c() {
        return _m_fields;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: fz */
    public void mo5258fz(long j) {
        switch (bFf().mo6893i(dXO)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, dXO, new Object[]{new Long(j)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, dXO, new Object[]{new Long(j)}));
                break;
        }
        m9210fy(j);
    }

    /* access modifiers changed from: package-private */
    @C5566aOg
    public void reset() {
        switch (bFf().mo6893i(bkF)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, bkF, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, bkF, new Object[0]));
                break;
        }
        adk();
    }

    /* renamed from: f */
    public void mo5257f(Outpost qZVar) {
        super.mo10S();
        m9206e(qZVar);
        reset();
        OutpostUpkeepTicker aVar = (OutpostUpkeepTicker) bFf().mo6865M(OutpostUpkeepTicker.class);
        aVar.mo5260a(this, (aDJ) this);
        m9203a(aVar);
        bqR().mo4704hk(86400.0f);
    }

    @C0064Am(aul = "c63a4492bed709a9ab8b2467b48cf858", aum = 0)
    private void bqS() {
        mo5258fz(cVr());
    }

    @C0064Am(aul = "c957bb8e669504740480573f0a6164b1", aum = 0)
    /* renamed from: fy */
    private void m9210fy(long j) {
        if (aWb().bXV() == Outpost.C3349b.SOLD) {
            if (m9208fB(j)) {
                if (bqQ()) {
                    reset();
                } else if (aWb().isEnabled()) {
                    aWb().disable();
                    m9209fx(j);
                } else {
                    aWb().bYb();
                }
            }
            aWb().mo21395eC(false);
        }
    }

    @C0064Am(aul = "3147d4262d766801b0b111732a50b946", aum = 0)
    private boolean bqW() {
        return bqQ();
    }

    @C0064Am(aul = "96cdb3e560d053d4a6ab49a9a0b3710d", aum = 0)
    private int bqY() {
        return (int) Math.floor(((double) (brf() - cVr())) / 8.64E7d);
    }

    @C0064Am(aul = "3eb27bea2e64cad1dfff2d59e2852aab", aum = 0)
    private long bra() {
        return ((long) ala().aJe().mo19003wZ().cRl()) * dXJ;
    }

    @C0064Am(aul = "f8b7ccce32d9ede52302fa1827584f45", aum = 0)
    private long brc() {
        return ((long) ala().aJe().mo19003wZ().cRn()) * dXJ;
    }

    @C0064Am(aul = "08a379759af4ce6af61698a6c1ffabbe", aum = 0)
    private long bre() {
        if (aWb().isEnabled()) {
            return bqP() + brb();
        }
        return bqP() + brd();
    }

    @C0064Am(aul = "aa885f85c58552146eeccc0de593eb85", aum = 0)
    /* renamed from: fA */
    private boolean m9207fA(long j) {
        return j >= brf();
    }

    @C0064Am(aul = "6be8d476e8ac624c39f21dec5f6ea3f6", aum = 0)
    /* renamed from: a */
    private void m9202a(C0665JT jt) {
        if (jt.mo3117j(1, 1, 0) && bqR() == null) {
            OutpostUpkeepTicker aVar = (OutpostUpkeepTicker) bFf().mo6865M(OutpostUpkeepTicker.class);
            aVar.mo5260a(this, (aDJ) this);
            m9203a(aVar);
        }
    }

    @C6485anp
    @C5511aMd
    /* renamed from: a.Rc$a */
    public class OutpostUpkeepTicker extends TaskletImpl implements C1616Xf {

        /* renamed from: JD */
        public static final C2491fm f1498JD = null;
        public static final int _m_fieldCount = 0;
        public static final C5663aRz[] _m_fields = null;
        public static final int _m_methodCount = 0;
        public static final C2491fm[] _m_methods = null;
        /* renamed from: aT */
        public static final C5663aRz f1499aT = null;
        public static final long serialVersionUID = 0;
        public static C6494any ___iScriptClass;
        @C0064Am(aul = "e2346a3b50bc181247e3235058d67da7", aum = 0)
        static /* synthetic */ OutpostUpkeepPeriod gOp;

        static {
            m9221V();
        }

        public OutpostUpkeepTicker() {
            super((C5540aNg) null);
            super.mo10S();
        }

        public OutpostUpkeepTicker(OutpostUpkeepPeriod rc, aDJ adj) {
            super((C5540aNg) null);
            super._m_script_init(rc, adj);
        }

        public OutpostUpkeepTicker(C5540aNg ang) {
            super(ang);
        }

        /* renamed from: V */
        static void m9221V() {
            serialVersionUID = (long) 1;
            _m_fieldCount = TaskletImpl._m_fieldCount + 1;
            _m_methodCount = TaskletImpl._m_methodCount + 1;
            int i = TaskletImpl._m_fieldCount;
            C5663aRz[] arzArr = new C5663aRz[(i + 1)];
            C5663aRz b = C5640aRc.m17844b(OutpostUpkeepTicker.class, "e2346a3b50bc181247e3235058d67da7", i);
            f1499aT = b;
            arzArr[i] = b;
            int i2 = i + 1;
            _m_fields = arzArr;
            C1634Xv.m11725a((Object[]) TaskletImpl._m_fields, (Object[]) _m_fields);
            int i3 = TaskletImpl._m_methodCount;
            C2491fm[] fmVarArr = new C2491fm[(i3 + 1)];
            C2491fm a = C4105zY.m41624a(OutpostUpkeepTicker.class, "85cf52d1d03a2ddf008bed8a24116743", i3);
            f1498JD = a;
            fmVarArr[i3] = a;
            int i4 = i3 + 1;
            _m_methods = fmVarArr;
            C1634Xv.m11725a((Object[]) TaskletImpl._m_methods, (Object[]) _m_methods);
            ___iScriptClass = aUO.m18566a(OutpostUpkeepTicker.class, awX.class, _m_fields, _m_methods);
        }

        /* renamed from: c */
        private void m9222c(OutpostUpkeepPeriod rc) {
            bFf().mo5608dq().mo3197f(f1499aT, rc);
        }

        private OutpostUpkeepPeriod cHk() {
            return (OutpostUpkeepPeriod) bFf().mo5608dq().mo3214p(f1499aT);
        }

        /* renamed from: S */
        public void mo10S() {
            super.mo10S();
        }

        /* renamed from: T */
        public C6494any mo11T() {
            return ___iScriptClass;
        }

        /* renamed from: U */
        public C2491fm[] mo12U() {
            return _m_methods;
        }

        /* renamed from: W */
        public Object mo13W() {
            return new awX(this);
        }

        /* renamed from: a */
        public Object mo14a(C0495Gr gr) {
            gr.getArgs();
            switch (gr.mo2417hq() - TaskletImpl._m_methodCount) {
                case 0:
                    m9223pi();
                    return null;
                default:
                    return super.mo14a(gr);
            }
        }

        /* renamed from: a */
        public void mo15a(Collection collection, C0495Gr gr) {
            super.mo15a(collection, gr);
        }

        /* renamed from: c */
        public C5663aRz[] mo25c() {
            return _m_fields;
        }

        /* access modifiers changed from: protected */
        /* renamed from: pj */
        public void mo667pj() {
            switch (bFf().mo6893i(f1498JD)) {
                case 0:
                    return;
                case 2:
                    bFf().mo5606d(new aCE(this, f1498JD, new Object[0]));
                    return;
                case 3:
                    bFf().mo5606d(new aCE(this, f1498JD, new Object[0]));
                    break;
            }
            m9223pi();
        }

        /* renamed from: a */
        public void mo5260a(OutpostUpkeepPeriod rc, aDJ adj) {
            m9222c(rc);
            super.mo4706l(adj);
        }

        @C0064Am(aul = "85cf52d1d03a2ddf008bed8a24116743", aum = 0)
        /* renamed from: pi */
        private void m9223pi() {
            cHk().bqT();
        }
    }
}
