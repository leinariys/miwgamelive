package game.script;

import game.network.message.externalizable.aCE;
import game.script.avatar.Avatar;
import game.script.player.Player;
import game.script.progression.ProgressionCareer;
import logic.baa.C1616Xf;
import logic.baa.C1634Xv;
import logic.baa.C5511aMd;
import logic.baa.C6485anp;
import logic.data.mbean.C0178CE;
import logic.res.code.C5640aRc;
import logic.res.code.C5663aRz;
import logic.res.code.C6494any;
import logic.res.html.C2491fm;
import p001a.*;

import java.util.Collection;

@C5511aMd
@C6485anp
/* renamed from: a.azE */
/* compiled from: a */
public class CharacterView extends TaikodomObject implements C1616Xf {
    /* renamed from: BP */
    public static final C5663aRz f5661BP = null;
    /* renamed from: BV */
    public static final C5663aRz f5663BV = null;
    /* renamed from: Cq */
    public static final C2491fm f5664Cq = null;
    /* renamed from: Cr */
    public static final C2491fm f5665Cr = null;
    /* renamed from: Cw */
    public static final C2491fm f5666Cw = null;
    /* renamed from: Cx */
    public static final C2491fm f5667Cx = null;
    /* renamed from: Lm */
    public static final C2491fm f5668Lm = null;
    public static final C2491fm _f_dispose_0020_0028_0029V = null;
    public static final int _m_fieldCount = 0;
    public static final C5663aRz[] _m_fields = null;
    public static final int _m_methodCount = 0;
    public static final C2491fm[] _m_methods = null;
    public static final C2491fm aLg = null;
    public static final C5663aRz avF = null;
    public static final C2491fm cSM = null;
    public static final C2491fm eIY = null;
    /* renamed from: gQ */
    public static final C2491fm f5669gQ = null;
    public static final C5663aRz haM = null;
    public static final C2491fm haN = null;
    public static final long serialVersionUID = 0;
    public static C6494any ___iScriptClass;
    @C0064Am(aul = "d8f512d5d334f2ed0b7d3e53cc1c6978", aum = 2)

    /* renamed from: BO */
    private static int f5660BO;
    @C0064Am(aul = "1224ca8b7f1e873667305ee2a502e886", aum = 1)

    /* renamed from: BU */
    private static ProgressionCareer f5662BU;
    @C0064Am(aul = "be448d3c7bf420ffcff02e5128b70483", aum = 0)
    private static Character aKN;
    @C0064Am(aul = "40bc153d2b3af675dff24d61c3d51ed8", aum = 3)
    private static String cyd;

    static {
        m27564V();
    }

    public CharacterView() {
        super((C5540aNg) null);
        super.mo10S();
    }

    public CharacterView(C5540aNg ang) {
        super(ang);
    }

    public CharacterView(Character acx) {
        super((C5540aNg) null);
        super._m_script_init(acx);
    }

    /* renamed from: V */
    static void m27564V() {
        serialVersionUID = (long) 1;
        _m_fieldCount = TaikodomObject._m_fieldCount + 4;
        _m_methodCount = TaikodomObject._m_methodCount + 11;
        int i = TaikodomObject._m_fieldCount;
        C5663aRz[] arzArr = new C5663aRz[(i + 4)];
        C5663aRz b = C5640aRc.m17844b(CharacterView.class, "be448d3c7bf420ffcff02e5128b70483", i);
        avF = b;
        arzArr[i] = b;
        int i2 = i + 1;
        C5663aRz b2 = C5640aRc.m17844b(CharacterView.class, "1224ca8b7f1e873667305ee2a502e886", i2);
        f5663BV = b2;
        arzArr[i2] = b2;
        int i3 = i2 + 1;
        C5663aRz b3 = C5640aRc.m17844b(CharacterView.class, "d8f512d5d334f2ed0b7d3e53cc1c6978", i3);
        f5661BP = b3;
        arzArr[i3] = b3;
        int i4 = i3 + 1;
        C5663aRz b4 = C5640aRc.m17844b(CharacterView.class, "40bc153d2b3af675dff24d61c3d51ed8", i4);
        haM = b4;
        arzArr[i4] = b4;
        int i5 = i4 + 1;
        _m_fields = arzArr;
        C1634Xv.m11725a((Object[]) TaikodomObject._m_fields, (Object[]) _m_fields);
        int i6 = TaikodomObject._m_methodCount;
        C2491fm[] fmVarArr = new C2491fm[(i6 + 11)];
        C2491fm a = C4105zY.m41624a(CharacterView.class, "329e45b723091d24b76ab18248bbec4e", i6);
        f5664Cq = a;
        fmVarArr[i6] = a;
        int i7 = i6 + 1;
        C2491fm a2 = C4105zY.m41624a(CharacterView.class, "1c11b712b692fe7ed31abb7202a39a72", i7);
        f5665Cr = a2;
        fmVarArr[i7] = a2;
        int i8 = i7 + 1;
        C2491fm a3 = C4105zY.m41624a(CharacterView.class, "af714016850fa42c9cb73fbf284b7e78", i8);
        aLg = a3;
        fmVarArr[i8] = a3;
        int i9 = i8 + 1;
        C2491fm a4 = C4105zY.m41624a(CharacterView.class, "36ebcc874bed9ad9d3b56cee242f8d6f", i9);
        f5666Cw = a4;
        fmVarArr[i9] = a4;
        int i10 = i9 + 1;
        C2491fm a5 = C4105zY.m41624a(CharacterView.class, "b8ecfe273da35ef1bc21833cea3bd06d", i10);
        eIY = a5;
        fmVarArr[i10] = a5;
        int i11 = i10 + 1;
        C2491fm a6 = C4105zY.m41624a(CharacterView.class, "ff6f2b38c5f35c1bef98bec70c18ae9c", i11);
        f5667Cx = a6;
        fmVarArr[i11] = a6;
        int i12 = i11 + 1;
        C2491fm a7 = C4105zY.m41624a(CharacterView.class, "57f558661831e9a355add2eae4f1c98b", i12);
        _f_dispose_0020_0028_0029V = a7;
        fmVarArr[i12] = a7;
        int i13 = i12 + 1;
        C2491fm a8 = C4105zY.m41624a(CharacterView.class, "9bd44d7ce538211b67c9f544d7dc111e", i13);
        f5669gQ = a8;
        fmVarArr[i13] = a8;
        int i14 = i13 + 1;
        C2491fm a9 = C4105zY.m41624a(CharacterView.class, "5a8ed379c8213abd75eb8b0ca3c776f9", i14);
        f5668Lm = a9;
        fmVarArr[i14] = a9;
        int i15 = i14 + 1;
        C2491fm a10 = C4105zY.m41624a(CharacterView.class, "696926aaec32e15e5b890fbfad034dee", i15);
        haN = a10;
        fmVarArr[i15] = a10;
        int i16 = i15 + 1;
        C2491fm a11 = C4105zY.m41624a(CharacterView.class, "ee5f1b2f4a1e3394c298f5d9a612851e", i16);
        cSM = a11;
        fmVarArr[i16] = a11;
        int i17 = i16 + 1;
        _m_methods = fmVarArr;
        C1634Xv.m11725a((Object[]) TaikodomObject._m_methods, (Object[]) _m_methods);
        ___iScriptClass = aUO.m18566a(CharacterView.class, C0178CE.class, _m_fields, _m_methods);
    }

    /* renamed from: QY */
    private Character m27562QY() {
        return (Character) bFf().mo5608dq().mo3214p(avF);
    }

    /* renamed from: ar */
    private void m27565ar(int i) {
        bFf().mo5608dq().mo3183b(f5661BP, i);
    }

    @C0064Am(aul = "1c11b712b692fe7ed31abb7202a39a72", aum = 0)
    @C5566aOg
    /* renamed from: ax */
    private void m27566ax(int i) {
        throw new aWi(new aCE(this, f5665Cr, new Object[]{new Integer(i)}));
    }

    private String cGp() {
        return (String) bFf().mo5608dq().mo3214p(haM);
    }

    /* renamed from: e */
    private void m27568e(ProgressionCareer sbVar) {
        bFf().mo5608dq().mo3197f(f5663BV, sbVar);
    }

    @C0064Am(aul = "36ebcc874bed9ad9d3b56cee242f8d6f", aum = 0)
    @C5566aOg
    /* renamed from: f */
    private void m27569f(ProgressionCareer sbVar) {
        throw new aWi(new aCE(this, f5666Cw, new Object[]{sbVar}));
    }

    /* renamed from: g */
    private void m27571g(Character acx) {
        bFf().mo5608dq().mo3197f(avF, acx);
    }

    /* renamed from: kP */
    private void m27572kP(String str) {
        bFf().mo5608dq().mo3197f(haM, str);
    }

    @C0064Am(aul = "696926aaec32e15e5b890fbfad034dee", aum = 0)
    @C5566aOg
    /* renamed from: kQ */
    private void m27573kQ(String str) {
        throw new aWi(new aCE(this, haN, new Object[]{str}));
    }

    /* renamed from: le */
    private int m27574le() {
        return bFf().mo5608dq().mo3212n(f5661BP);
    }

    /* renamed from: lh */
    private ProgressionCareer m27575lh() {
        return (ProgressionCareer) bFf().mo5608dq().mo3214p(f5663BV);
    }

    /* renamed from: Rn */
    public ProgressionCareer mo17142Rn() {
        switch (bFf().mo6893i(aLg)) {
            case 0:
                return null;
            case 2:
                return (ProgressionCareer) bFf().mo5606d(new aCE(this, aLg, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, aLg, new Object[0]));
                break;
        }
        return m27563Rm();
    }

    /* renamed from: S */
    public void mo10S() {
        super.mo10S();
    }

    /* renamed from: T */
    public C6494any mo11T() {
        return ___iScriptClass;
    }

    /* renamed from: U */
    public C2491fm[] mo12U() {
        return _m_methods;
    }

    /* renamed from: W */
    public Object mo13W() {
        return new C0178CE(this);
    }

    /* renamed from: a */
    public Object mo14a(C0495Gr gr) {
        Object[] args = gr.getArgs();
        switch (gr.mo2417hq() - TaikodomObject._m_methodCount) {
            case 0:
                return new Integer(m27576ls());
            case 1:
                m27566ax(((Integer) args[0]).intValue());
                return null;
            case 2:
                return m27563Rm();
            case 3:
                m27569f((ProgressionCareer) args[0]);
                return null;
            case 4:
                bFN();
                return null;
            case 5:
                return m27577lz();
            case 6:
                m27570fg();
                return null;
            case 7:
                m27567dd();
                return null;
            case 8:
                m27578qU();
                return null;
            case 9:
                m27573kQ((String) args[0]);
                return null;
            case 10:
                return aOx();
            default:
                return super.mo14a(gr);
        }
    }

    /* renamed from: a */
    public void mo15a(Collection collection, C0495Gr gr) {
        super.mo15a(collection, gr);
    }

    public String aOy() {
        switch (bFf().mo6893i(cSM)) {
            case 0:
                return null;
            case 2:
                return (String) bFf().mo5606d(new aCE(this, cSM, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, cSM, new Object[0]));
                break;
        }
        return aOx();
    }

    @C5566aOg
    /* renamed from: ay */
    public void mo17144ay(int i) {
        switch (bFf().mo6893i(f5665Cr)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f5665Cr, new Object[]{new Integer(i)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f5665Cr, new Object[]{new Integer(i)}));
                break;
        }
        m27566ax(i);
    }

    /* renamed from: c */
    public C5663aRz[] mo25c() {
        return _m_fields;
    }

    /* renamed from: de */
    public void mo6851de() {
        switch (bFf().mo6893i(f5669gQ)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f5669gQ, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f5669gQ, new Object[0]));
                break;
        }
        m27567dd();
    }

    public void dispose() {
        switch (bFf().mo6893i(_f_dispose_0020_0028_0029V)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, _f_dispose_0020_0028_0029V, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, _f_dispose_0020_0028_0029V, new Object[0]));
                break;
        }
        m27570fg();
    }

    @C5566aOg
    /* renamed from: g */
    public void mo17145g(ProgressionCareer sbVar) {
        switch (bFf().mo6893i(f5666Cw)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f5666Cw, new Object[]{sbVar}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f5666Cw, new Object[]{sbVar}));
                break;
        }
        m27569f(sbVar);
    }

    @C5566aOg
    /* renamed from: kR */
    public void mo17146kR(String str) {
        switch (bFf().mo6893i(haN)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, haN, new Object[]{str}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, haN, new Object[]{str}));
                break;
        }
        m27573kQ(str);
    }

    /* renamed from: lA */
    public Character mo6854lA() {
        switch (bFf().mo6893i(f5667Cx)) {
            case 0:
                return null;
            case 2:
                return (Character) bFf().mo5606d(new aCE(this, f5667Cx, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f5667Cx, new Object[0]));
                break;
        }
        return m27577lz();
    }

    /* renamed from: lt */
    public int mo17147lt() {
        switch (bFf().mo6893i(f5664Cq)) {
            case 0:
                return 0;
            case 2:
                return ((Integer) bFf().mo5606d(new aCE(this, f5664Cq, new Object[0]))).intValue();
            case 3:
                bFf().mo5606d(new aCE(this, f5664Cq, new Object[0]));
                break;
        }
        return m27576ls();
    }

    /* renamed from: qV */
    public void mo6855qV() {
        switch (bFf().mo6893i(f5668Lm)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f5668Lm, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f5668Lm, new Object[0]));
                break;
        }
        m27578qU();
    }

    public void refresh() {
        switch (bFf().mo6893i(eIY)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, eIY, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, eIY, new Object[0]));
                break;
        }
        bFN();
    }

    /* renamed from: h */
    public void mo14234h(Character acx) {
        super.mo10S();
        m27571g(acx);
    }

    @C0064Am(aul = "329e45b723091d24b76ab18248bbec4e", aum = 0)
    /* renamed from: ls */
    private int m27576ls() {
        return m27574le();
    }

    @C0064Am(aul = "af714016850fa42c9cb73fbf284b7e78", aum = 0)
    /* renamed from: Rm */
    private ProgressionCareer m27563Rm() {
        return m27575lh();
    }

    @C0064Am(aul = "b8ecfe273da35ef1bc21833cea3bd06d", aum = 0)
    private void bFN() {
        mo17144ay(mo6854lA().mo12658lt());
        ProgressionCareer sbVar = null;
        if (mo6854lA().mo12637Rn() != null) {
            sbVar = mo6854lA().mo12637Rn();
        }
        mo17145g(sbVar);
        if (m27562QY().bQI() == null) {
            mo17146kR("0");
        } else if (m27562QY() instanceof Player) {
            Avatar ev = (Avatar) ((Player) m27562QY()).bQI();
            if (ev.agj() == null) {
                ev.mo2033k(m27562QY());
            }
            ev.aOA();
        } else {
            mo17146kR(m27562QY().bQI().aOy());
        }
    }

    @C0064Am(aul = "ff6f2b38c5f35c1bef98bec70c18ae9c", aum = 0)
    /* renamed from: lz */
    private Character m27577lz() {
        return m27562QY();
    }

    @C0064Am(aul = "57f558661831e9a355add2eae4f1c98b", aum = 0)
    /* renamed from: fg */
    private void m27570fg() {
        if (m27562QY() != null) {
            m27571g((Character) null);
        }
        if (m27575lh() != null) {
            m27568e((ProgressionCareer) null);
        }
        if (cGp() != null) {
            m27572kP((String) null);
        }
        super.dispose();
    }

    @C0064Am(aul = "9bd44d7ce538211b67c9f544d7dc111e", aum = 0)
    /* renamed from: dd */
    private void m27567dd() {
        push();
    }

    @C0064Am(aul = "5a8ed379c8213abd75eb8b0ca3c776f9", aum = 0)
    /* renamed from: qU */
    private void m27578qU() {
        push();
    }

    @C0064Am(aul = "ee5f1b2f4a1e3394c298f5d9a612851e", aum = 0)
    private String aOx() {
        return cGp();
    }
}
