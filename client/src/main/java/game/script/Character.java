package game.script;

import game.network.message.externalizable.aCE;
import game.network.message.serializable.C2686iZ;
import game.script.avatar.Avatar;
import game.script.avatar.body.AvatarAppearanceType;
import game.script.faction.Faction;
import game.script.item.Item;
import game.script.item.ItemLocation;
import game.script.login.UserConnection;
import game.script.newmarket.CommercialOrder;
import game.script.player.Player;
import game.script.progression.CharacterEvolution;
import game.script.progression.CharacterProgression;
import game.script.progression.ProgressionCareer;
import game.script.ship.Ship;
import game.script.ship.Station;
import logic.baa.*;
import logic.data.link.C3161oY;
import logic.data.link.C5386aHi;
import logic.data.mbean.C0205CW;
import logic.res.code.C5640aRc;
import logic.res.code.C5663aRz;
import logic.res.code.C6494any;
import logic.res.html.C2491fm;
import p001a.*;
import taikodom.infra.script.I18NString;

import java.util.Collection;
import java.util.Iterator;

@C5511aMd
@C6485anp
/* renamed from: a.acX  reason: case insensitive filesystem */
/* compiled from: a */
public abstract class Character extends TaikodomObject implements C0847MM, C1616Xf, C3161oY.C3162a {

    /* renamed from: BX */
    public static final C5663aRz f4247BX = null;

    /* renamed from: Cq */
    public static final C2491fm f4248Cq = null;

    /* renamed from: Cs */
    public static final C2491fm f4249Cs = null;

    /* renamed from: Cv */
    public static final C2491fm f4250Cv = null;

    /* renamed from: Lm */
    public static final C2491fm f4251Lm = null;

    /* renamed from: Pf */
    public static final C2491fm f4252Pf = null;
    public static final C2491fm _f_dispose_0020_0028_0029V = null;
    public static final C2491fm _f_hasHollowField_0020_0028_0029Z = null;
    /* renamed from: _f_onObjectDisposed_0020_0028Ltaikodom_002finfra_002fscript_002fScriptObject_003b_0029V */
    public static final C2491fm f4253x13860637 = null;
    public static final C2491fm _f_onResurrect_0020_0028_0029V = null;
    public static final int _m_fieldCount = 0;
    public static final C5663aRz[] _m_fields = null;
    public static final int _m_methodCount = 0;
    public static final C2491fm[] _m_methods = null;
    public static final C2491fm aLV = null;
    public static final C2491fm aLW = null;
    public static final C2491fm aLg = null;
    public static final C2491fm aLh = null;
    public static final C2491fm aRm = null;
    public static final C2491fm bpJ = null;
    public static final C2491fm brK = null;
    public static final C2491fm cov = null;
    public static final C2491fm cow = null;
    public static final C5663aRz ffU = null;
    public static final C5663aRz ffV = null;
    public static final C5663aRz ffW = null;
    public static final C5663aRz ffX = null;
    public static final C5663aRz ffY = null;
    public static final C5663aRz ffZ = null;
    public static final C5663aRz fga = null;
    public static final C2491fm fgb = null;
    public static final C2491fm fgc = null;
    public static final C2491fm fgd = null;
    public static final C2491fm fge = null;
    public static final C2491fm fgf = null;
    public static final C2491fm fgg = null;
    public static final C2491fm fgh = null;
    public static final C2491fm fgi = null;
    public static final C2491fm fgj = null;
    public static final C2491fm fgk = null;
    public static final C2491fm fgl = null;
    public static final C2491fm fgm = null;
    public static final C2491fm fgn = null;
    public static final C2491fm fgo = null;
    public static final C2491fm fgp = null;
    public static final C2491fm fgq = null;
    public static final C2491fm fgr = null;
    public static final C2491fm fgs = null;
    public static final C2491fm fgt = null;
    public static final C2491fm fgu = null;
    public static final C2491fm fgv = null;
    public static final C2491fm fgw = null;
    public static final C2491fm fgx = null;
    public static final C2491fm fgy = null;
    /* renamed from: gQ */
    public static final C2491fm f4254gQ = null;
    public static final long serialVersionUID = 0;
    /* renamed from: yH */
    public static final C2491fm f4256yH = null;
    public static C6494any ___iScriptClass;
    @C0064Am(aul = "69b8ca7851c060d0088f3d13eeca262f", aum = 0)
    private static Controller cCG;
    @C0064Am(aul = "a5b0aefc45f7da97189f30aa36497c4d", aum = 1)
    private static C2686iZ<Character> cCH;
    @C0064Am(aul = "a839415b84e472cdba450e8abd7051d3", aum = 2)
    private static Actor cCI;
    @C0064Am(aul = "f5b04bc5b78149e943ae29d9da0054e4", aum = 3)
    private static LDScriptingController cCJ;
    @C0064Am(aul = "29b922298bfb107d776a289019824c03", aum = 4)
    private static Avatar cCK;
    @C0064Am(aul = "9e81bcb31bde2975c0622a3d4365eb64", aum = 5)
    private static Ship cCL;
    @C0064Am(aul = "99c72f679bc78d75f85c432fab5ff687", aum = 7)
    private static CharacterView cCM;
    @C0064Am(aul = "cd522d38c4334184ed39f93d6bf95088", aum = 6)

    /* renamed from: jp */
    private static CharacterProgression f4255jp;

    static {
        m20446V();
    }

    public Character() {
        super((C5540aNg) null);
        super.mo10S();
    }

    public Character(C5540aNg ang) {
        super(ang);
    }

    public Character(C2961mJ mJVar) {
        super((C5540aNg) null);
        super.mo967a(mJVar);
    }

    /* renamed from: V */
    static void m20446V() {
        serialVersionUID = (long) 1;
        _m_fieldCount = TaikodomObject._m_fieldCount + 8;
        _m_methodCount = TaikodomObject._m_methodCount + 44;
        int i = TaikodomObject._m_fieldCount;
        C5663aRz[] arzArr = new C5663aRz[(i + 8)];
        C5663aRz b = C5640aRc.m17844b(Character.class, "69b8ca7851c060d0088f3d13eeca262f", i);
        ffU = b;
        arzArr[i] = b;
        int i2 = i + 1;
        C5663aRz b2 = C5640aRc.m17844b(Character.class, "a5b0aefc45f7da97189f30aa36497c4d", i2);
        ffV = b2;
        arzArr[i2] = b2;
        int i3 = i2 + 1;
        C5663aRz b3 = C5640aRc.m17844b(Character.class, "a839415b84e472cdba450e8abd7051d3", i3);
        ffW = b3;
        arzArr[i3] = b3;
        int i4 = i3 + 1;
        C5663aRz b4 = C5640aRc.m17844b(Character.class, "f5b04bc5b78149e943ae29d9da0054e4", i4);
        ffX = b4;
        arzArr[i4] = b4;
        int i5 = i4 + 1;
        C5663aRz b5 = C5640aRc.m17844b(Character.class, "29b922298bfb107d776a289019824c03", i5);
        ffY = b5;
        arzArr[i5] = b5;
        int i6 = i5 + 1;
        C5663aRz b6 = C5640aRc.m17844b(Character.class, "9e81bcb31bde2975c0622a3d4365eb64", i6);
        ffZ = b6;
        arzArr[i6] = b6;
        int i7 = i6 + 1;
        C5663aRz b7 = C5640aRc.m17844b(Character.class, "cd522d38c4334184ed39f93d6bf95088", i7);
        f4247BX = b7;
        arzArr[i7] = b7;
        int i8 = i7 + 1;
        C5663aRz b8 = C5640aRc.m17844b(Character.class, "99c72f679bc78d75f85c432fab5ff687", i8);
        fga = b8;
        arzArr[i8] = b8;
        int i9 = i8 + 1;
        _m_fields = arzArr;
        C1634Xv.m11725a((Object[]) TaikodomObject._m_fields, (Object[]) _m_fields);
        int i10 = TaikodomObject._m_methodCount;
        C2491fm[] fmVarArr = new C2491fm[(i10 + 44)];
        C2491fm a = C4105zY.m41624a(Character.class, "c4ce24843181021a25acbe39b64f4cb4", i10);
        f4256yH = a;
        fmVarArr[i10] = a;
        int i11 = i10 + 1;
        C2491fm a2 = C4105zY.m41624a(Character.class, "9c7d054ae1f57fcf90fb9d2f7868e6ce", i11);
        _f_dispose_0020_0028_0029V = a2;
        fmVarArr[i11] = a2;
        int i12 = i11 + 1;
        C2491fm a3 = C4105zY.m41624a(Character.class, C5386aHi.bgb, i12);
        aRm = a3;
        fmVarArr[i12] = a3;
        int i13 = i12 + 1;
        C2491fm a4 = C4105zY.m41624a(Character.class, "b39bff3f717a011a1a82ee6ef1563d3d", i13);
        fgb = a4;
        fmVarArr[i13] = a4;
        int i14 = i13 + 1;
        C2491fm a5 = C4105zY.m41624a(Character.class, "8dea88116a78ef2572ca064b86950508", i14);
        fgc = a5;
        fmVarArr[i14] = a5;
        int i15 = i14 + 1;
        C2491fm a6 = C4105zY.m41624a(Character.class, "c413efe64023ce6326a2337ad6c11ede", i15);
        fgd = a6;
        fmVarArr[i15] = a6;
        int i16 = i15 + 1;
        C2491fm a7 = C4105zY.m41624a(Character.class, "2932ffcc6757f1bb8925ae0a562b6f74", i16);
        fge = a7;
        fmVarArr[i16] = a7;
        int i17 = i16 + 1;
        C2491fm a8 = C4105zY.m41624a(Character.class, "f04f40993c5871f24a83db6f469c6e92", i17);
        fgf = a8;
        fmVarArr[i17] = a8;
        int i18 = i17 + 1;
        C2491fm a9 = C4105zY.m41624a(Character.class, "2364cd91a26b4e1bf7f6eb3a749fa5a7", i18);
        fgg = a9;
        fmVarArr[i18] = a9;
        int i19 = i18 + 1;
        C2491fm a10 = C4105zY.m41624a(Character.class, "6e9944bc5f0262fea3c7e9b70d4a3cdf", i19);
        fgh = a10;
        fmVarArr[i19] = a10;
        int i20 = i19 + 1;
        C2491fm a11 = C4105zY.m41624a(Character.class, "f2ff8aa3b6ecf3fbf2ec9dcc031cee86", i20);
        f4252Pf = a11;
        fmVarArr[i20] = a11;
        int i21 = i20 + 1;
        C2491fm a12 = C4105zY.m41624a(Character.class, "e60f8eab566520e91105c6499358d449", i21);
        fgi = a12;
        fmVarArr[i21] = a12;
        int i22 = i21 + 1;
        C2491fm a13 = C4105zY.m41624a(Character.class, "5e794946277c6d72add0e23b0c971146", i22);
        brK = a13;
        fmVarArr[i22] = a13;
        int i23 = i22 + 1;
        C2491fm a14 = C4105zY.m41624a(Character.class, "6418b39cfba167b97ebf1970cc981e75", i23);
        bpJ = a14;
        fmVarArr[i23] = a14;
        int i24 = i23 + 1;
        C2491fm a15 = C4105zY.m41624a(Character.class, "1451ecdf987561fe3ff2c85eaee8e2eb", i24);
        fgj = a15;
        fmVarArr[i24] = a15;
        int i25 = i24 + 1;
        C2491fm a16 = C4105zY.m41624a(Character.class, "7b4c8dc63eaa09eccb3fd7e45f4a56b4", i25);
        fgk = a16;
        fmVarArr[i25] = a16;
        int i26 = i25 + 1;
        C2491fm a17 = C4105zY.m41624a(Character.class, "929f7c032bab8169132ad9e67e4d85ad", i26);
        fgl = a17;
        fmVarArr[i26] = a17;
        int i27 = i26 + 1;
        C2491fm a18 = C4105zY.m41624a(Character.class, "d5b8cd667ac377c118d6b707d984146f", i27);
        f4253x13860637 = a18;
        fmVarArr[i27] = a18;
        int i28 = i27 + 1;
        C2491fm a19 = C4105zY.m41624a(Character.class, "c30cff30db82f7c0b58052f681ab8863", i28);
        fgm = a19;
        fmVarArr[i28] = a19;
        int i29 = i28 + 1;
        C2491fm a20 = C4105zY.m41624a(Character.class, "5a7383a4cb70440e9cdb0912f61f9e4b", i29);
        fgn = a20;
        fmVarArr[i29] = a20;
        int i30 = i29 + 1;
        C2491fm a21 = C4105zY.m41624a(Character.class, "6ad7f891d203d61cfc6d5b23e827c561", i30);
        fgo = a21;
        fmVarArr[i30] = a21;
        int i31 = i30 + 1;
        C2491fm a22 = C4105zY.m41624a(Character.class, "da21cf17918378c01329a157c8e9b7b0", i31);
        fgp = a22;
        fmVarArr[i31] = a22;
        int i32 = i31 + 1;
        C2491fm a23 = C4105zY.m41624a(Character.class, "d142f25f94dec0931c09b95f858ceb7b", i32);
        cov = a23;
        fmVarArr[i32] = a23;
        int i33 = i32 + 1;
        C2491fm a24 = C4105zY.m41624a(Character.class, "0b7e8088a1147d6e9b341bf0641d4947", i33);
        cow = a24;
        fmVarArr[i33] = a24;
        int i34 = i33 + 1;
        C2491fm a25 = C4105zY.m41624a(Character.class, "4284e3a29185a27cda78f7d716fc1d62", i34);
        f4251Lm = a25;
        fmVarArr[i34] = a25;
        int i35 = i34 + 1;
        C2491fm a26 = C4105zY.m41624a(Character.class, "963d886530abd1a819de1b046db8a3ce", i35);
        _f_hasHollowField_0020_0028_0029Z = a26;
        fmVarArr[i35] = a26;
        int i36 = i35 + 1;
        C2491fm a27 = C4105zY.m41624a(Character.class, "4143c99a89a347e981fdd08c3b08f0ca", i36);
        _f_onResurrect_0020_0028_0029V = a27;
        fmVarArr[i36] = a27;
        int i37 = i36 + 1;
        C2491fm a28 = C4105zY.m41624a(Character.class, "2f7c390553e42608d9c6be1f2be8fa73", i37);
        fgq = a28;
        fmVarArr[i37] = a28;
        int i38 = i37 + 1;
        C2491fm a29 = C4105zY.m41624a(Character.class, "5f12e1df33497499d627c8c571b774d2", i38);
        fgr = a29;
        fmVarArr[i38] = a29;
        int i39 = i38 + 1;
        C2491fm a30 = C4105zY.m41624a(Character.class, "af50bfb7e5bcf4c1f856c3bfb90869e8", i39);
        fgs = a30;
        fmVarArr[i39] = a30;
        int i40 = i39 + 1;
        C2491fm a31 = C4105zY.m41624a(Character.class, "a8460d726f6fcb7d9f425c4db11e3cd3", i40);
        fgt = a31;
        fmVarArr[i40] = a31;
        int i41 = i40 + 1;
        C2491fm a32 = C4105zY.m41624a(Character.class, "c5fd6eaf667e228c230a7b05063cbe5a", i41);
        aLV = a32;
        fmVarArr[i41] = a32;
        int i42 = i41 + 1;
        C2491fm a33 = C4105zY.m41624a(Character.class, "cbed905cd9903fac983c401e31323bc1", i42);
        aLW = a33;
        fmVarArr[i42] = a33;
        int i43 = i42 + 1;
        C2491fm a34 = C4105zY.m41624a(Character.class, "cbbd21333fbf07061d4d842e4719865e", i43);
        f4250Cv = a34;
        fmVarArr[i43] = a34;
        int i44 = i43 + 1;
        C2491fm a35 = C4105zY.m41624a(Character.class, "d6c7741a4f46f10606eaf64ddc878260", i44);
        aLh = a35;
        fmVarArr[i44] = a35;
        int i45 = i44 + 1;
        C2491fm a36 = C4105zY.m41624a(Character.class, "e0e17ec9ec3ca76dc81c5340241a887d", i45);
        fgu = a36;
        fmVarArr[i45] = a36;
        int i46 = i45 + 1;
        C2491fm a37 = C4105zY.m41624a(Character.class, "cf7c2e06a41bf725c930a6c81917ae18", i46);
        f4248Cq = a37;
        fmVarArr[i46] = a37;
        int i47 = i46 + 1;
        C2491fm a38 = C4105zY.m41624a(Character.class, "0b42dff40f24305af4b14197169b87b9", i47);
        f4249Cs = a38;
        fmVarArr[i47] = a38;
        int i48 = i47 + 1;
        C2491fm a39 = C4105zY.m41624a(Character.class, "d7ce2462c79b651c0c0b611100be9770", i48);
        aLg = a39;
        fmVarArr[i48] = a39;
        int i49 = i48 + 1;
        C2491fm a40 = C4105zY.m41624a(Character.class, "5fb17d6bb9db2ccae8da1babb21b6f82", i49);
        fgv = a40;
        fmVarArr[i49] = a40;
        int i50 = i49 + 1;
        C2491fm a41 = C4105zY.m41624a(Character.class, "8551b053a9c48fdf50b439bf856ca8f7", i50);
        fgw = a41;
        fmVarArr[i50] = a41;
        int i51 = i50 + 1;
        C2491fm a42 = C4105zY.m41624a(Character.class, "c18ba1ac9d4aab1d37ab5c041cc7a542", i51);
        fgx = a42;
        fmVarArr[i51] = a42;
        int i52 = i51 + 1;
        C2491fm a43 = C4105zY.m41624a(Character.class, "b7e8ceeb9d6013233edb9ee6fabbff02", i52);
        fgy = a43;
        fmVarArr[i52] = a43;
        int i53 = i52 + 1;
        C2491fm a44 = C4105zY.m41624a(Character.class, "18a1c60a0d66a97063bbb4bdddf44c3a", i53);
        f4254gQ = a44;
        fmVarArr[i53] = a44;
        int i54 = i53 + 1;
        _m_methods = fmVarArr;
        C1634Xv.m11725a((Object[]) TaikodomObject._m_methods, (Object[]) _m_methods);
        ___iScriptClass = aUO.m18566a(Character.class, C0205CW.class, _m_fields, _m_methods);
    }

    @C0064Am(aul = "c30cff30db82f7c0b58052f681ab8863", aum = 0)
    @C5566aOg
    /* renamed from: M */
    private void m20435M(Character acx) {
        throw new aWi(new aCE(this, fgm, new Object[]{acx}));
    }

    /* renamed from: N */
    private void m20436N(Ship fAVar) {
        bFf().mo5608dq().mo3197f(ffZ, fAVar);
    }

    @C0064Am(aul = "5a7383a4cb70440e9cdb0912f61f9e4b", aum = 0)
    @C5566aOg
    /* renamed from: O */
    private void m20437O(Character acx) {
        throw new aWi(new aCE(this, fgn, new Object[]{acx}));
    }

    @C0064Am(aul = "8dea88116a78ef2572ca064b86950508", aum = 0)
    @C5566aOg
    @C2499fr
    /* renamed from: O */
    private void m20438O(Ship fAVar) {
        throw new aWi(new aCE(this, fgc, new Object[]{fAVar}));
    }

    /* renamed from: P */
    private void m20439P(C2686iZ iZVar) {
        bFf().mo5608dq().mo3197f(ffV, iZVar);
    }

    @C0064Am(aul = "c413efe64023ce6326a2337ad6c11ede", aum = 0)
    @C5566aOg
    /* renamed from: Q */
    private void m20440Q(Ship fAVar) {
        throw new aWi(new aCE(this, fgd, new Object[]{fAVar}));
    }

    /* renamed from: a */
    private void m20447a(Avatar ev) {
        bFf().mo5608dq().mo3197f(ffY, ev);
    }

    /* renamed from: a */
    private void m20448a(Controller jx) {
        bFf().mo5608dq().mo3197f(ffU, jx);
    }

    /* renamed from: a */
    private void m20450a(CharacterView aze) {
        bFf().mo5608dq().mo3197f(fga, aze);
    }

    /* renamed from: a */
    private void m20451a(CharacterProgression nKVar) {
        bFf().mo5608dq().mo3197f(f4247BX, nKVar);
    }

    /* renamed from: aN */
    private void m20453aN(Actor cr) {
        bFf().mo5608dq().mo3197f(ffW, cr);
    }

    @C0064Am(aul = "6e9944bc5f0262fea3c7e9b70d4a3cdf", aum = 0)
    @C5566aOg
    /* renamed from: aO */
    private void m20454aO(Actor cr) {
        throw new aWi(new aCE(this, fgh, new Object[]{cr}));
    }

    @C0064Am(aul = "5e794946277c6d72add0e23b0c971146", aum = 0)
    private String ahP() {
        throw new aWi(new aCE(this, brK, new Object[0]));
    }

    @C0064Am(aul = "d142f25f94dec0931c09b95f858ceb7b", aum = 0)
    private Faction azM() {
        throw new aWi(new aCE(this, cov, new Object[0]));
    }

    @C0064Am(aul = "a8460d726f6fcb7d9f425c4db11e3cd3", aum = 0)
    @C5566aOg
    /* renamed from: b */
    private void m20455b(Avatar ev) {
        throw new aWi(new aCE(this, fgt, new Object[]{ev}));
    }

    @C0064Am(aul = "1451ecdf987561fe3ff2c85eaee8e2eb", aum = 0)
    @C5566aOg
    /* renamed from: b */
    private void m20456b(Controller jx) {
        throw new aWi(new aCE(this, fgj, new Object[]{jx}));
    }

    @C0064Am(aul = "b7e8ceeb9d6013233edb9ee6fabbff02", aum = 0)
    @C5566aOg
    /* renamed from: b */
    private void m20458b(CharacterView aze) {
        throw new aWi(new aCE(this, fgy, new Object[]{aze}));
    }

    @C0064Am(aul = "e60f8eab566520e91105c6499358d449", aum = 0)
    @C5566aOg
    private I18NString bQD() {
        throw new aWi(new aCE(this, fgi, new Object[0]));
    }

    @C0064Am(aul = "af50bfb7e5bcf4c1f856c3bfb90869e8", aum = 0)
    @C5566aOg
    @C2499fr
    private aVE bQH() {
        throw new aWi(new aCE(this, fgs, new Object[0]));
    }

    @C0064Am(aul = "5fb17d6bb9db2ccae8da1babb21b6f82", aum = 0)
    private CharacterProgression bQJ() {
        throw new aWi(new aCE(this, fgv, new Object[0]));
    }

    @C0064Am(aul = "8551b053a9c48fdf50b439bf856ca8f7", aum = 0)
    private void bQL() {
        throw new aWi(new aCE(this, fgw, new Object[0]));
    }

    private Controller bQp() {
        return (Controller) bFf().mo5608dq().mo3214p(ffU);
    }

    private C2686iZ bQq() {
        return (C2686iZ) bFf().mo5608dq().mo3214p(ffV);
    }

    private Actor bQr() {
        return (Actor) bFf().mo5608dq().mo3214p(ffW);
    }

    private LDScriptingController bQs() {
        return (LDScriptingController) bFf().mo5608dq().mo3214p(ffX);
    }

    private Avatar bQt() {
        return (Avatar) bFf().mo5608dq().mo3214p(ffY);
    }

    private Ship bQu() {
        return (Ship) bFf().mo5608dq().mo3214p(ffZ);
    }

    private CharacterView bQv() {
        return (CharacterView) bFf().mo5608dq().mo3214p(fga);
    }

    @C0064Am(aul = "2932ffcc6757f1bb8925ae0a562b6f74", aum = 0)
    @C5566aOg
    @C2499fr
    private void bQy() {
        throw new aWi(new aCE(this, fge, new Object[0]));
    }

    /* renamed from: c */
    private void m20460c(LDScriptingController lYVar) {
        bFf().mo5608dq().mo3197f(ffX, lYVar);
    }

    @C0064Am(aul = "929f7c032bab8169132ad9e67e4d85ad", aum = 0)
    @C5566aOg
    /* renamed from: d */
    private void m20463d(LDScriptingController lYVar) {
        throw new aWi(new aCE(this, fgl, new Object[]{lYVar}));
    }

    @C0064Am(aul = "18a1c60a0d66a97063bbb4bdddf44c3a", aum = 0)
    @C5566aOg
    /* renamed from: dd */
    private void m20464dd() {
        throw new aWi(new aCE(this, f4254gQ, new Object[0]));
    }

    @C5566aOg
    private void init() {
        switch (bFf().mo6893i(f4256yH)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f4256yH, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f4256yH, new Object[0]));
                break;
        }
        m20467jF();
    }

    @C0064Am(aul = "c4ce24843181021a25acbe39b64f4cb4", aum = 0)
    @C5566aOg
    /* renamed from: jF */
    private void m20467jF() {
        throw new aWi(new aCE(this, f4256yH, new Object[0]));
    }

    @C0064Am(aul = "5f12e1df33497499d627c8c571b774d2", aum = 0)
    @C5566aOg
    @C2499fr
    /* renamed from: l */
    private aVE m20468l(AvatarAppearanceType t) {
        throw new aWi(new aCE(this, fgr, new Object[]{t}));
    }

    /* renamed from: li */
    private CharacterProgression m20469li() {
        return (CharacterProgression) bFf().mo5608dq().mo3214p(f4247BX);
    }

    @C0064Am(aul = "0b7e8088a1147d6e9b341bf0641d4947", aum = 0)
    /* renamed from: m */
    private void m20473m(Faction xm) {
        throw new aWi(new aCE(this, cow, new Object[]{xm}));
    }

    @C5566aOg
    @C0064Am(aul = "4284e3a29185a27cda78f7d716fc1d62", aum = 0)
    @C2499fr
    @Deprecated
    /* renamed from: qU */
    private void m20474qU() {
        throw new aWi(new aCE(this, f4251Lm, new Object[0]));
    }

    @C0064Am(aul = "f2ff8aa3b6ecf3fbf2ec9dcc031cee86", aum = 0)
    /* renamed from: uV */
    private String m20475uV() {
        throw new aWi(new aCE(this, f4252Pf, new Object[0]));
    }

    @C5566aOg
    /* renamed from: N */
    public void mo12632N(Character acx) {
        switch (bFf().mo6893i(fgm)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, fgm, new Object[]{acx}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, fgm, new Object[]{acx}));
                break;
        }
        m20435M(acx);
    }

    @C5566aOg
    /* renamed from: P */
    public void mo12633P(Character acx) {
        switch (bFf().mo6893i(fgn)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, fgn, new Object[]{acx}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, fgn, new Object[]{acx}));
                break;
        }
        m20437O(acx);
    }

    @C5566aOg
    @C2499fr
    /* renamed from: P */
    public void mo12634P(Ship fAVar) {
        switch (bFf().mo6893i(fgc)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, fgc, new Object[]{fAVar}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, fgc, new Object[]{fAVar}));
                break;
        }
        m20438O(fAVar);
    }

    @ClientOnly
    /* renamed from: QW */
    public boolean mo961QW() {
        switch (bFf().mo6893i(_f_hasHollowField_0020_0028_0029Z)) {
            case 0:
                return false;
            case 2:
                return ((Boolean) bFf().mo5606d(new aCE(this, _f_hasHollowField_0020_0028_0029Z, new Object[0]))).booleanValue();
            case 3:
                bFf().mo5606d(new aCE(this, _f_hasHollowField_0020_0028_0029Z, new Object[0]));
                break;
        }
        return m20442QV();
    }

    /* access modifiers changed from: protected */
    @C5566aOg
    /* renamed from: R */
    public void mo12635R(Ship fAVar) {
        switch (bFf().mo6893i(fgd)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, fgd, new Object[]{fAVar}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, fgd, new Object[]{fAVar}));
                break;
        }
        m20440Q(fAVar);
    }

    /* renamed from: R */
    public boolean mo12636R(Character acx) {
        switch (bFf().mo6893i(fgo)) {
            case 0:
                return false;
            case 2:
                return ((Boolean) bFf().mo5606d(new aCE(this, fgo, new Object[]{acx}))).booleanValue();
            case 3:
                bFf().mo5606d(new aCE(this, fgo, new Object[]{acx}));
                break;
        }
        return m20441Q(acx);
    }

    /* renamed from: Rn */
    public ProgressionCareer mo12637Rn() {
        switch (bFf().mo6893i(aLg)) {
            case 0:
                return null;
            case 2:
                return (ProgressionCareer) bFf().mo5606d(new aCE(this, aLg, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, aLg, new Object[0]));
                break;
        }
        return m20443Rm();
    }

    /* renamed from: Rp */
    public CharacterEvolution mo12638Rp() {
        switch (bFf().mo6893i(aLh)) {
            case 0:
                return null;
            case 2:
                return (CharacterEvolution) bFf().mo5606d(new aCE(this, aLh, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, aLh, new Object[0]));
                break;
        }
        return m20444Ro();
    }

    /* renamed from: T */
    public Faction.C1624a mo12639T(Character acx) {
        switch (bFf().mo6893i(fgp)) {
            case 0:
                return null;
            case 2:
                return (Faction.C1624a) bFf().mo5606d(new aCE(this, fgp, new Object[]{acx}));
            case 3:
                bFf().mo5606d(new aCE(this, fgp, new Object[]{acx}));
                break;
        }
        return m20445S(acx);
    }

    /* renamed from: T */
    public C6494any mo11T() {
        return ___iScriptClass;
    }

    /* renamed from: U */
    public C2491fm[] mo12U() {
        return _m_methods;
    }

    /* renamed from: W */
    public Object mo13W() {
        return new C0205CW(this);
    }

    /* renamed from: a */
    public Object mo14a(C0495Gr gr) {
        Object[] args = gr.getArgs();
        switch (gr.mo2417hq() - TaikodomObject._m_methodCount) {
            case 0:
                m20467jF();
                return null;
            case 1:
                m20466fg();
                return null;
            case 2:
                return new Boolean(m20461c((C1722ZT<UserConnection>) (C1722ZT) args[0]));
            case 3:
                return bQw();
            case 4:
                m20438O((Ship) args[0]);
                return null;
            case 5:
                m20440Q((Ship) args[0]);
                return null;
            case 6:
                bQy();
                return null;
            case 7:
                return new Boolean(bQA());
            case 8:
                return bQC();
            case 9:
                m20454aO((Actor) args[0]);
                return null;
            case 10:
                return m20475uV();
            case 11:
                return bQD();
            case 12:
                return ahP();
            case 13:
                return afJ();
            case 14:
                m20456b((Controller) args[0]);
                return null;
            case 15:
                return bQF();
            case 16:
                m20463d((LDScriptingController) args[0]);
                return null;
            case 17:
                m20457b((aDJ) args[0]);
                return null;
            case 18:
                m20435M((Character) args[0]);
                return null;
            case 19:
                m20437O((Character) args[0]);
                return null;
            case 20:
                return new Boolean(m20441Q((Character) args[0]));
            case 21:
                return m20445S((Character) args[0]);
            case 22:
                return azM();
            case 23:
                m20473m((Faction) args[0]);
                return null;
            case 24:
                m20474qU();
                return null;
            case 25:
                return new Boolean(m20442QV());
            case 26:
                m20452aG();
                return null;
            case 27:
                m20462d((CommercialOrder) args[0]);
                return null;
            case 28:
                return m20468l((AvatarAppearanceType) args[0]);
            case 29:
                return bQH();
            case 30:
                m20455b((Avatar) args[0]);
                return null;
            case 31:
                m20449a((Item) args[0], (ItemLocation) args[1], (ItemLocation) args[2]);
                return null;
            case 32:
                m20459c((Item) args[0], (ItemLocation) args[1]);
                return null;
            case 33:
                return m20472lx();
            case 34:
                return m20444Ro();
            case 35:
                m20465e((CharacterProgression) args[0]);
                return null;
            case 36:
                return new Integer(m20470ls());
            case 37:
                return new Long(m20471lu());
            case 38:
                return m20443Rm();
            case 39:
                return bQJ();
            case 40:
                bQL();
                return null;
            case 41:
                return bQN();
            case 42:
                m20458b((CharacterView) args[0]);
                return null;
            case 43:
                m20464dd();
                return null;
            default:
                return super.mo14a(gr);
        }
    }

    /* renamed from: a */
    public void mo3918a(CommercialOrder aJVar) {
        switch (bFf().mo6893i(fgq)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, fgq, new Object[]{aJVar}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, fgq, new Object[]{aJVar}));
                break;
        }
        m20462d(aJVar);
    }

    /* renamed from: a */
    public void mo15a(Collection collection, C0495Gr gr) {
        super.mo15a(collection, gr);
    }

    /* renamed from: aH */
    public void mo70aH() {
        switch (bFf().mo6893i(_f_onResurrect_0020_0028_0029V)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, _f_onResurrect_0020_0028_0029V, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, _f_onResurrect_0020_0028_0029V, new Object[0]));
                break;
        }
        m20452aG();
    }

    @C5566aOg
    /* renamed from: aP */
    public void mo12640aP(Actor cr) {
        switch (bFf().mo6893i(fgh)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, fgh, new Object[]{cr}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, fgh, new Object[]{cr}));
                break;
        }
        m20454aO(cr);
    }

    public String ahQ() {
        switch (bFf().mo6893i(brK)) {
            case 0:
                return null;
            case 2:
                return (String) bFf().mo5606d(new aCE(this, brK, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, brK, new Object[0]));
                break;
        }
        return ahP();
    }

    public Faction azN() {
        switch (bFf().mo6893i(cov)) {
            case 0:
                return null;
            case 2:
                return (Faction) bFf().mo5606d(new aCE(this, cov, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, cov, new Object[0]));
                break;
        }
        return azM();
    }

    /* renamed from: b */
    public void mo12641b(Item auq, ItemLocation aag, ItemLocation aag2) {
        switch (bFf().mo6893i(aLV)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, aLV, new Object[]{auq, aag, aag2}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, aLV, new Object[]{auq, aag, aag2}));
                break;
        }
        m20449a(auq, aag, aag2);
    }

    public boolean bQB() {
        switch (bFf().mo6893i(fgf)) {
            case 0:
                return false;
            case 2:
                return ((Boolean) bFf().mo5606d(new aCE(this, fgf, new Object[0]))).booleanValue();
            case 3:
                bFf().mo5606d(new aCE(this, fgf, new Object[0]));
                break;
        }
        return bQA();
    }

    @C5566aOg
    public I18NString bQE() {
        switch (bFf().mo6893i(fgi)) {
            case 0:
                return null;
            case 2:
                return (I18NString) bFf().mo5606d(new aCE(this, fgi, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, fgi, new Object[0]));
                break;
        }
        return bQD();
    }

    public LDScriptingController bQG() {
        switch (bFf().mo6893i(fgk)) {
            case 0:
                return null;
            case 2:
                return (LDScriptingController) bFf().mo5606d(new aCE(this, fgk, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, fgk, new Object[0]));
                break;
        }
        return bQF();
    }

    @C5566aOg
    @C2499fr
    public aVE bQI() {
        switch (bFf().mo6893i(fgs)) {
            case 0:
                return null;
            case 2:
                return (aVE) bFf().mo5606d(new aCE(this, fgs, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, fgs, new Object[0]));
                break;
        }
        return bQH();
    }

    public CharacterProgression bQK() {
        switch (bFf().mo6893i(fgv)) {
            case 0:
                return null;
            case 2:
                return (CharacterProgression) bFf().mo5606d(new aCE(this, fgv, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, fgv, new Object[0]));
                break;
        }
        return bQJ();
    }

    public void bQM() {
        switch (bFf().mo6893i(fgw)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, fgw, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, fgw, new Object[0]));
                break;
        }
        bQL();
    }

    public CharacterView bQO() {
        switch (bFf().mo6893i(fgx)) {
            case 0:
                return null;
            case 2:
                return (CharacterView) bFf().mo5606d(new aCE(this, fgx, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, fgx, new Object[0]));
                break;
        }
        return bQN();
    }

    public Ship bQx() {
        switch (bFf().mo6893i(fgb)) {
            case 0:
                return null;
            case 2:
                return (Ship) bFf().mo5606d(new aCE(this, fgb, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, fgb, new Object[0]));
                break;
        }
        return bQw();
    }

    @C5566aOg
    @C2499fr
    public void bQz() {
        switch (bFf().mo6893i(fge)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, fge, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, fge, new Object[0]));
                break;
        }
        bQy();
    }

    public Actor bhE() {
        switch (bFf().mo6893i(fgg)) {
            case 0:
                return null;
            case 2:
                return (Actor) bFf().mo5606d(new aCE(this, fgg, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, fgg, new Object[0]));
                break;
        }
        return bQC();
    }

    @C5566aOg
    /* renamed from: c */
    public void mo12648c(Avatar ev) {
        switch (bFf().mo6893i(fgt)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, fgt, new Object[]{ev}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, fgt, new Object[]{ev}));
                break;
        }
        m20455b(ev);
    }

    @C5566aOg
    /* renamed from: c */
    public void mo12649c(Controller jx) {
        switch (bFf().mo6893i(fgj)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, fgj, new Object[]{jx}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, fgj, new Object[]{jx}));
                break;
        }
        m20456b(jx);
    }

    /* renamed from: c */
    public void mo98c(aDJ adj) {
        switch (bFf().mo6893i(f4253x13860637)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f4253x13860637, new Object[]{adj}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f4253x13860637, new Object[]{adj}));
                break;
        }
        m20457b(adj);
    }

    @C5566aOg
    /* renamed from: c */
    public void mo12650c(CharacterView aze) {
        switch (bFf().mo6893i(fgy)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, fgy, new Object[]{aze}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, fgy, new Object[]{aze}));
                break;
        }
        m20458b(aze);
    }

    /* renamed from: c */
    public C5663aRz[] mo25c() {
        return _m_fields;
    }

    /* renamed from: d */
    public void mo12651d(Item auq, ItemLocation aag) {
        switch (bFf().mo6893i(aLW)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, aLW, new Object[]{auq, aag}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, aLW, new Object[]{auq, aag}));
                break;
        }
        m20459c(auq, aag);
    }

    @C5472aKq
    /* renamed from: d */
    public boolean mo12652d(C1722ZT<UserConnection> zt) {
        switch (bFf().mo6893i(aRm)) {
            case 0:
                return false;
            case 2:
                return ((Boolean) bFf().mo5606d(new aCE(this, aRm, new Object[]{zt}))).booleanValue();
            case 3:
                bFf().mo5606d(new aCE(this, aRm, new Object[]{zt}));
                break;
        }
        return m20461c(zt);
    }

    @C5566aOg
    /* renamed from: de */
    public void mo12653de() {
        switch (bFf().mo6893i(f4254gQ)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f4254gQ, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f4254gQ, new Object[0]));
                break;
        }
        m20464dd();
    }

    public void dispose() {
        switch (bFf().mo6893i(_f_dispose_0020_0028_0029V)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, _f_dispose_0020_0028_0029V, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, _f_dispose_0020_0028_0029V, new Object[0]));
                break;
        }
        m20466fg();
    }

    @C5566aOg
    /* renamed from: e */
    public void mo12654e(LDScriptingController lYVar) {
        switch (bFf().mo6893i(fgl)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, fgl, new Object[]{lYVar}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, fgl, new Object[]{lYVar}));
                break;
        }
        m20463d(lYVar);
    }

    /* renamed from: en */
    public long mo12655en() {
        switch (bFf().mo6893i(f4249Cs)) {
            case 0:
                return 0;
            case 2:
                return ((Long) bFf().mo5606d(new aCE(this, f4249Cs, new Object[0]))).longValue();
            case 3:
                bFf().mo5606d(new aCE(this, f4249Cs, new Object[0]));
                break;
        }
        return m20471lu();
    }

    /* renamed from: f */
    public void mo12656f(CharacterProgression nKVar) {
        switch (bFf().mo6893i(fgu)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, fgu, new Object[]{nKVar}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, fgu, new Object[]{nKVar}));
                break;
        }
        m20465e(nKVar);
    }

    public String getName() {
        switch (bFf().mo6893i(f4252Pf)) {
            case 0:
                return null;
            case 2:
                return (String) bFf().mo5606d(new aCE(this, f4252Pf, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f4252Pf, new Object[0]));
                break;
        }
        return m20475uV();
    }

    /* renamed from: hb */
    public Controller mo12657hb() {
        switch (bFf().mo6893i(bpJ)) {
            case 0:
                return null;
            case 2:
                return (Controller) bFf().mo5606d(new aCE(this, bpJ, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, bpJ, new Object[0]));
                break;
        }
        return afJ();
    }

    /* renamed from: lt */
    public int mo12658lt() {
        switch (bFf().mo6893i(f4248Cq)) {
            case 0:
                return 0;
            case 2:
                return ((Integer) bFf().mo5606d(new aCE(this, f4248Cq, new Object[0]))).intValue();
            case 3:
                bFf().mo5606d(new aCE(this, f4248Cq, new Object[0]));
                break;
        }
        return m20470ls();
    }

    /* renamed from: ly */
    public CharacterProgression mo12659ly() {
        switch (bFf().mo6893i(f4250Cv)) {
            case 0:
                return null;
            case 2:
                return (CharacterProgression) bFf().mo5606d(new aCE(this, f4250Cv, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f4250Cv, new Object[0]));
                break;
        }
        return m20472lx();
    }

    @C5566aOg
    @C2499fr
    /* renamed from: m */
    public aVE mo12660m(AvatarAppearanceType t) {
        switch (bFf().mo6893i(fgr)) {
            case 0:
                return null;
            case 2:
                return (aVE) bFf().mo5606d(new aCE(this, fgr, new Object[]{t}));
            case 3:
                bFf().mo5606d(new aCE(this, fgr, new Object[]{t}));
                break;
        }
        return m20468l(t);
    }

    /* renamed from: n */
    public void mo11676n(Faction xm) {
        switch (bFf().mo6893i(cow)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, cow, new Object[]{xm}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, cow, new Object[]{xm}));
                break;
        }
        m20473m(xm);
    }

    @C5566aOg
    @C2499fr
    @Deprecated
    /* renamed from: qV */
    public void mo12661qV() {
        switch (bFf().mo6893i(f4251Lm)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f4251Lm, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f4251Lm, new Object[0]));
                break;
        }
        m20474qU();
    }

    /* renamed from: S */
    public void mo10S() {
        super.mo10S();
        init();
    }

    /* renamed from: a */
    public void mo967a(C2961mJ mJVar) {
        super.mo967a(mJVar);
        init();
    }

    @C0064Am(aul = "9c7d054ae1f57fcf90fb9d2f7868e6ce", aum = 0)
    /* renamed from: fg */
    private void m20466fg() {
        if (bQp() != null) {
            bQp().dispose();
        }
        if (m20469li() != null) {
            m20469li().dispose();
            m20451a((CharacterProgression) null);
        }
        if (bQv() != null) {
            bQv().dispose();
            m20450a((CharacterView) null);
        }
        bQs().dispose();
        super.dispose();
    }

    @C0064Am(aul = "7c9f40c686343ecd9803463a216638ad", aum = 0)
    @C5472aKq
    /* renamed from: c */
    private boolean m20461c(C1722ZT<UserConnection> zt) {
        if (!(this instanceof Player)) {
            return false;
        }
        if (zt != null) {
            return zt.bFq().mo15388dL() == this;
        }
        mo8358lY("Null replication context. Ignoring replication rule");
        return false;
    }

    @C0064Am(aul = "b39bff3f717a011a1a82ee6ef1563d3d", aum = 0)
    private Ship bQw() {
        return bQu();
    }

    @C0064Am(aul = "f04f40993c5871f24a83db6f469c6e92", aum = 0)
    private boolean bQA() {
        return bQr() instanceof Station;
    }

    @C0064Am(aul = "2364cd91a26b4e1bf7f6eb3a749fa5a7", aum = 0)
    private Actor bQC() {
        return bQr();
    }

    @C0064Am(aul = "6418b39cfba167b97ebf1970cc981e75", aum = 0)
    private Controller afJ() {
        return bQp();
    }

    @C0064Am(aul = "7b4c8dc63eaa09eccb3fd7e45f4a56b4", aum = 0)
    private LDScriptingController bQF() {
        return bQs();
    }

    @C0064Am(aul = "d5b8cd667ac377c118d6b707d984146f", aum = 0)
    /* renamed from: b */
    private void m20457b(aDJ adj) {
        bQq().remove(adj);
    }

    @C0064Am(aul = "6ad7f891d203d61cfc6d5b23e827c561", aum = 0)
    /* renamed from: Q */
    private boolean m20441Q(Character acx) {
        return bQq().contains(acx);
    }

    @C0064Am(aul = "da21cf17918378c01329a157c8e9b7b0", aum = 0)
    /* renamed from: S */
    private Faction.C1624a m20445S(Character acx) {
        if (acx == null || azN() == null || (!azN().mo6826x(acx.azN()) && !mo12636R(acx) && !acx.mo12636R(this))) {
            return Faction.C1624a.NEUTRAL;
        }
        return Faction.C1624a.ENEMY;
    }

    @C0064Am(aul = "963d886530abd1a819de1b046db8a3ce", aum = 0)
    @ClientOnly
    /* renamed from: QV */
    private boolean m20442QV() {
        if (!super.mo961QW() && !C1298TD.m9830t(azN()).bFT() && !C1298TD.m9830t(bQG()).bFT() && !bQG().mo961QW() && !C1298TD.m9830t(mo12659ly()).bFT() && !C1298TD.m9830t(mo12659ly()).bFT() && !C1298TD.m9830t(mo12638Rp()).bFT()) {
            return false;
        }
        return true;
    }

    @C0064Am(aul = "4143c99a89a347e981fdd08c3b08f0ca", aum = 0)
    /* renamed from: aG */
    private void m20452aG() {
        super.mo70aH();
        Iterator it = bQq().iterator();
        while (it.hasNext()) {
            Character acx = (Character) it.next();
            if (acx == null || acx.isDisposed()) {
                it.remove();
            }
        }
    }

    @C0064Am(aul = "2f7c390553e42608d9c6be1f2be8fa73", aum = 0)
    /* renamed from: d */
    private void m20462d(CommercialOrder aJVar) {
    }

    @C0064Am(aul = "c5fd6eaf667e228c230a7b05063cbe5a", aum = 0)
    /* renamed from: a */
    private void m20449a(Item auq, ItemLocation aag, ItemLocation aag2) {
        if (bQs() != null) {
            bQs().mo20261b(auq, aag, aag2);
        }
    }

    @C0064Am(aul = "cbed905cd9903fac983c401e31323bc1", aum = 0)
    /* renamed from: c */
    private void m20459c(Item auq, ItemLocation aag) {
        if (bQs() != null) {
            bQs().mo20276d(auq, aag);
        }
    }

    @C0064Am(aul = "cbbd21333fbf07061d4d842e4719865e", aum = 0)
    /* renamed from: lx */
    private CharacterProgression m20472lx() {
        return m20469li();
    }

    @C0064Am(aul = "d6c7741a4f46f10606eaf64ddc878260", aum = 0)
    /* renamed from: Ro */
    private CharacterEvolution m20444Ro() {
        if (mo12659ly() != null) {
            return mo12659ly().mo20722Rp();
        }
        return null;
    }

    @C0064Am(aul = "e0e17ec9ec3ca76dc81c5340241a887d", aum = 0)
    /* renamed from: e */
    private void m20465e(CharacterProgression nKVar) {
        m20451a(nKVar);
    }

    @C0064Am(aul = "cf7c2e06a41bf725c930a6c81917ae18", aum = 0)
    /* renamed from: ls */
    private int m20470ls() {
        if (mo12659ly() != null) {
            return mo12659ly().mo20740lt();
        }
        return 0;
    }

    @C0064Am(aul = "0b42dff40f24305af4b14197169b87b9", aum = 0)
    /* renamed from: lu */
    private long m20471lu() {
        if (mo12659ly() != null) {
            return mo12659ly().mo20734en();
        }
        return 0;
    }

    @C0064Am(aul = "d7ce2462c79b651c0c0b611100be9770", aum = 0)
    /* renamed from: Rm */
    private ProgressionCareer m20443Rm() {
        if (mo12659ly() != null) {
            return mo12659ly().mo20721Rn();
        }
        return null;
    }

    @C0064Am(aul = "c18ba1ac9d4aab1d37ab5c041cc7a542", aum = 0)
    private CharacterView bQN() {
        return bQv();
    }
}
