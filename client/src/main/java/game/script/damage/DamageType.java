package game.script.damage;

import game.network.message.externalizable.aCE;
import game.script.resource.Asset;
import logic.baa.*;
import logic.data.mbean.C6040afM;
import logic.res.code.C5640aRc;
import logic.res.code.C5663aRz;
import logic.res.code.C6494any;
import logic.res.html.C2491fm;
import p001a.*;
import taikodom.infra.script.I18NString;

import java.util.Collection;
import java.util.UUID;

@C5829abJ("1.0.0")
@C5511aMd
@C6485anp
/* renamed from: a.FR */
/* compiled from: a */
public class DamageType extends TaikodomObject implements C0468GU, C1616Xf {

    /* renamed from: NY */
    public static final C5663aRz f566NY = null;

    /* renamed from: Ob */
    public static final C2491fm f567Ob = null;

    /* renamed from: Oc */
    public static final C2491fm f568Oc = null;
    public static final C2491fm _f_toString_0020_0028_0029Ljava_002flang_002fString_003b = null;
    public static final int _m_fieldCount = 0;
    public static final C5663aRz[] _m_fields = null;
    public static final int _m_methodCount = 0;
    public static final C2491fm[] _m_methods = null;
    /* renamed from: bL */
    public static final C5663aRz f570bL = null;
    /* renamed from: bM */
    public static final C5663aRz f571bM = null;
    /* renamed from: bN */
    public static final C2491fm f572bN = null;
    /* renamed from: bO */
    public static final C2491fm f573bO = null;
    /* renamed from: bP */
    public static final C2491fm f574bP = null;
    /* renamed from: bQ */
    public static final C2491fm f575bQ = null;
    public static final C5663aRz cWO = null;
    public static final C5663aRz cWQ = null;
    public static final C2491fm cWR = null;
    public static final C2491fm cWS = null;
    public static final C2491fm cWT = null;
    public static final C2491fm cWU = null;
    public static final C2491fm cWV = null;
    public static final long serialVersionUID = 0;
    public static C6494any ___iScriptClass;
    @C0064Am(aul = "a3a1af3d4df0a13fb2ecbae729fe9a26", aum = 4)

    /* renamed from: bK */
    private static UUID f569bK;
    @C0064Am(aul = "e617b36ceba6047ba7b3ca215a3f3561", aum = 0)
    private static I18NString cWN;
    @C0064Am(aul = "ffcd9ab0ce22f43298f6932526a99f94", aum = 2)
    private static boolean cWP;
    @C0064Am(aul = "fc28152ae77561bc80491f83ba891af2", aum = 1)
    private static String handle;
    @C0064Am(aul = "823d31057e9546d49a9d3c1fe05817d6", aum = 3)

    /* renamed from: jn */
    private static Asset f576jn;

    static {
        m3190V();
    }

    public DamageType() {
        super((C5540aNg) null);
        super.mo10S();
    }

    public DamageType(C5540aNg ang) {
        super(ang);
    }

    /* renamed from: V */
    static void m3190V() {
        serialVersionUID = (long) 1;
        _m_fieldCount = TaikodomObject._m_fieldCount + 5;
        _m_methodCount = TaikodomObject._m_methodCount + 12;
        int i = TaikodomObject._m_fieldCount;
        C5663aRz[] arzArr = new C5663aRz[(i + 5)];
        C5663aRz b = C5640aRc.m17844b(DamageType.class, "e617b36ceba6047ba7b3ca215a3f3561", i);
        cWO = b;
        arzArr[i] = b;
        int i2 = i + 1;
        C5663aRz b2 = C5640aRc.m17844b(DamageType.class, "fc28152ae77561bc80491f83ba891af2", i2);
        f571bM = b2;
        arzArr[i2] = b2;
        int i3 = i2 + 1;
        C5663aRz b3 = C5640aRc.m17844b(DamageType.class, "ffcd9ab0ce22f43298f6932526a99f94", i3);
        cWQ = b3;
        arzArr[i3] = b3;
        int i4 = i3 + 1;
        C5663aRz b4 = C5640aRc.m17844b(DamageType.class, "823d31057e9546d49a9d3c1fe05817d6", i4);
        f566NY = b4;
        arzArr[i4] = b4;
        int i5 = i4 + 1;
        C5663aRz b5 = C5640aRc.m17844b(DamageType.class, "a3a1af3d4df0a13fb2ecbae729fe9a26", i5);
        f570bL = b5;
        arzArr[i5] = b5;
        int i6 = i5 + 1;
        _m_fields = arzArr;
        C1634Xv.m11725a((Object[]) TaikodomObject._m_fields, (Object[]) _m_fields);
        int i7 = TaikodomObject._m_methodCount;
        C2491fm[] fmVarArr = new C2491fm[(i7 + 12)];
        C2491fm a = C4105zY.m41624a(DamageType.class, "4a34acaa99f2cb64ecfa5da57a92ac89", i7);
        f572bN = a;
        fmVarArr[i7] = a;
        int i8 = i7 + 1;
        C2491fm a2 = C4105zY.m41624a(DamageType.class, "4ec23ab41b57cacb3deb82d22ca49d8e", i8);
        f573bO = a2;
        fmVarArr[i8] = a2;
        int i9 = i8 + 1;
        C2491fm a3 = C4105zY.m41624a(DamageType.class, "5c745a61c6743a3047dd161750853688", i9);
        f574bP = a3;
        fmVarArr[i9] = a3;
        int i10 = i9 + 1;
        C2491fm a4 = C4105zY.m41624a(DamageType.class, "4a37555c02820c541e334ba7bcbb3b92", i10);
        f575bQ = a4;
        fmVarArr[i10] = a4;
        int i11 = i10 + 1;
        C2491fm a5 = C4105zY.m41624a(DamageType.class, "aed5c2c371ccbe96d99fc8cc2e0d7630", i11);
        cWR = a5;
        fmVarArr[i11] = a5;
        int i12 = i11 + 1;
        C2491fm a6 = C4105zY.m41624a(DamageType.class, "c283b2c7021285da6638f38ee0b3ebaf", i12);
        cWS = a6;
        fmVarArr[i12] = a6;
        int i13 = i12 + 1;
        C2491fm a7 = C4105zY.m41624a(DamageType.class, "8c1afdf053faf116c71a1762f8fd7980", i13);
        cWT = a7;
        fmVarArr[i13] = a7;
        int i14 = i13 + 1;
        C2491fm a8 = C4105zY.m41624a(DamageType.class, "65531c14fa74bc770f5c5db37592f7e5", i14);
        cWU = a8;
        fmVarArr[i14] = a8;
        int i15 = i14 + 1;
        C2491fm a9 = C4105zY.m41624a(DamageType.class, "d61a55e61d1bf2d9e28cfb77316f4b0b", i15);
        cWV = a9;
        fmVarArr[i15] = a9;
        int i16 = i15 + 1;
        C2491fm a10 = C4105zY.m41624a(DamageType.class, "9f1595dd6513e88febd60214978a1c78", i16);
        _f_toString_0020_0028_0029Ljava_002flang_002fString_003b = a10;
        fmVarArr[i16] = a10;
        int i17 = i16 + 1;
        C2491fm a11 = C4105zY.m41624a(DamageType.class, "554a883e7fd41cd974e65ac3c879a97c", i17);
        f567Ob = a11;
        fmVarArr[i17] = a11;
        int i18 = i17 + 1;
        C2491fm a12 = C4105zY.m41624a(DamageType.class, "910a00976abe7117c5a2a5b9638b06ad", i18);
        f568Oc = a12;
        fmVarArr[i18] = a12;
        int i19 = i18 + 1;
        _m_methods = fmVarArr;
        C1634Xv.m11725a((Object[]) TaikodomObject._m_methods, (Object[]) _m_methods);
        ___iScriptClass = aUO.m18566a(DamageType.class, C6040afM.class, _m_fields, _m_methods);
    }

    /* renamed from: a */
    private void m3191a(String str) {
        bFf().mo5608dq().mo3197f(f571bM, str);
    }

    /* renamed from: a */
    private void m3192a(UUID uuid) {
        bFf().mo5608dq().mo3197f(f570bL, uuid);
    }

    private I18NString aQj() {
        return (I18NString) bFf().mo5608dq().mo3214p(cWO);
    }

    private boolean aQk() {
        return bFf().mo5608dq().mo3201h(cWQ);
    }

    /* renamed from: an */
    private UUID m3193an() {
        return (UUID) bFf().mo5608dq().mo3214p(f570bL);
    }

    /* renamed from: ao */
    private String m3194ao() {
        return (String) bFf().mo5608dq().mo3214p(f571bM);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Handle")
    @C0064Am(aul = "4a37555c02820c541e334ba7bcbb3b92", aum = 0)
    @C5566aOg
    /* renamed from: b */
    private void m3198b(String str) {
        throw new aWi(new aCE(this, f575bQ, new Object[]{str}));
    }

    /* renamed from: c */
    private void m3200c(UUID uuid) {
        switch (bFf().mo6893i(f573bO)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f573bO, new Object[]{uuid}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f573bO, new Object[]{uuid}));
                break;
        }
        m3199b(uuid);
    }

    /* renamed from: cK */
    private void m3201cK(boolean z) {
        bFf().mo5608dq().mo3153a(cWQ, z);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Shield Piercing")
    @C0064Am(aul = "65531c14fa74bc770f5c5db37592f7e5", aum = 0)
    @C5566aOg
    /* renamed from: cL */
    private void m3202cL(boolean z) {
        throw new aWi(new aCE(this, cWU, new Object[]{new Boolean(z)}));
    }

    /* renamed from: gP */
    private void m3203gP(I18NString i18NString) {
        bFf().mo5608dq().mo3197f(cWO, i18NString);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Damage Name")
    @C0064Am(aul = "c283b2c7021285da6638f38ee0b3ebaf", aum = 0)
    @C5566aOg
    /* renamed from: gQ */
    private void m3204gQ(I18NString i18NString) {
        throw new aWi(new aCE(this, cWS, new Object[]{i18NString}));
    }

    /* renamed from: sG */
    private Asset m3205sG() {
        return (Asset) bFf().mo5608dq().mo3214p(f566NY);
    }

    /* renamed from: t */
    private void m3207t(Asset tCVar) {
        bFf().mo5608dq().mo3197f(f566NY, tCVar);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Icon")
    @C0064Am(aul = "910a00976abe7117c5a2a5b9638b06ad", aum = 0)
    @C5566aOg
    /* renamed from: u */
    private void m3208u(Asset tCVar) {
        throw new aWi(new aCE(this, f568Oc, new Object[]{tCVar}));
    }

    /* renamed from: T */
    public C6494any mo11T() {
        return ___iScriptClass;
    }

    /* renamed from: U */
    public C2491fm[] mo12U() {
        return _m_methods;
    }

    /* renamed from: W */
    public Object mo13W() {
        return new C6040afM(this);
    }

    /* renamed from: a */
    public Object mo14a(C0495Gr gr) {
        Object[] args = gr.getArgs();
        switch (gr.mo2417hq() - TaikodomObject._m_methodCount) {
            case 0:
                return m3195ap();
            case 1:
                m3199b((UUID) args[0]);
                return null;
            case 2:
                return m3196ar();
            case 3:
                m3198b((String) args[0]);
                return null;
            case 4:
                return aQl();
            case 5:
                m3204gQ((I18NString) args[0]);
                return null;
            case 6:
                return new Boolean(aQn());
            case 7:
                m3202cL(((Boolean) args[0]).booleanValue());
                return null;
            case 8:
                return new Boolean(aQp());
            case 9:
                return m3197au();
            case 10:
                return m3206sJ();
            case 11:
                m3208u((Asset) args[0]);
                return null;
            default:
                return super.mo14a(gr);
        }
    }

    /* renamed from: a */
    public void mo15a(Collection collection, C0495Gr gr) {
        super.mo15a(collection, gr);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Damage Name")
    public I18NString aQm() {
        switch (bFf().mo6893i(cWR)) {
            case 0:
                return null;
            case 2:
                return (I18NString) bFf().mo5606d(new aCE(this, cWR, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, cWR, new Object[0]));
                break;
        }
        return aQl();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Shield Piercing")
    public boolean aQo() {
        switch (bFf().mo6893i(cWT)) {
            case 0:
                return false;
            case 2:
                return ((Boolean) bFf().mo5606d(new aCE(this, cWT, new Object[0]))).booleanValue();
            case 3:
                bFf().mo5606d(new aCE(this, cWT, new Object[0]));
                break;
        }
        return aQn();
    }

    public boolean aQq() {
        switch (bFf().mo6893i(cWV)) {
            case 0:
                return false;
            case 2:
                return ((Boolean) bFf().mo5606d(new aCE(this, cWV, new Object[0]))).booleanValue();
            case 3:
                bFf().mo5606d(new aCE(this, cWV, new Object[0]));
                break;
        }
        return aQp();
    }

    /* renamed from: aq */
    public UUID mo18aq() {
        switch (bFf().mo6893i(f572bN)) {
            case 0:
                return null;
            case 2:
                return (UUID) bFf().mo5606d(new aCE(this, f572bN, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f572bN, new Object[0]));
                break;
        }
        return m3195ap();
    }

    /* renamed from: c */
    public C5663aRz[] mo25c() {
        return _m_fields;
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Shield Piercing")
    @C5566aOg
    /* renamed from: cM */
    public void mo2143cM(boolean z) {
        switch (bFf().mo6893i(cWU)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, cWU, new Object[]{new Boolean(z)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, cWU, new Object[]{new Boolean(z)}));
                break;
        }
        m3202cL(z);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Damage Name")
    @C5566aOg
    /* renamed from: gR */
    public void mo2144gR(I18NString i18NString) {
        switch (bFf().mo6893i(cWS)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, cWS, new Object[]{i18NString}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, cWS, new Object[]{i18NString}));
                break;
        }
        m3204gQ(i18NString);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Handle")
    public String getHandle() {
        switch (bFf().mo6893i(f574bP)) {
            case 0:
                return null;
            case 2:
                return (String) bFf().mo5606d(new aCE(this, f574bP, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f574bP, new Object[0]));
                break;
        }
        return m3196ar();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Handle")
    @C5566aOg
    public void setHandle(String str) {
        switch (bFf().mo6893i(f575bQ)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f575bQ, new Object[]{str}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f575bQ, new Object[]{str}));
                break;
        }
        m3198b(str);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Icon")
    /* renamed from: sK */
    public Asset mo2145sK() {
        switch (bFf().mo6893i(f567Ob)) {
            case 0:
                return null;
            case 2:
                return (Asset) bFf().mo5606d(new aCE(this, f567Ob, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f567Ob, new Object[0]));
                break;
        }
        return m3206sJ();
    }

    public String toString() {
        switch (bFf().mo6893i(_f_toString_0020_0028_0029Ljava_002flang_002fString_003b)) {
            case 0:
                return null;
            case 2:
                return (String) bFf().mo5606d(new aCE(this, _f_toString_0020_0028_0029Ljava_002flang_002fString_003b, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, _f_toString_0020_0028_0029Ljava_002flang_002fString_003b, new Object[0]));
                break;
        }
        return m3197au();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Icon")
    @C5566aOg
    /* renamed from: v */
    public void mo2147v(Asset tCVar) {
        switch (bFf().mo6893i(f568Oc)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f568Oc, new Object[]{tCVar}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f568Oc, new Object[]{tCVar}));
                break;
        }
        m3208u(tCVar);
    }

    @C0064Am(aul = "4a34acaa99f2cb64ecfa5da57a92ac89", aum = 0)
    /* renamed from: ap */
    private UUID m3195ap() {
        return m3193an();
    }

    @C0064Am(aul = "4ec23ab41b57cacb3deb82d22ca49d8e", aum = 0)
    /* renamed from: b */
    private void m3199b(UUID uuid) {
        m3192a(uuid);
    }

    /* renamed from: S */
    public void mo10S() {
        super.mo10S();
        m3192a(UUID.randomUUID());
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Handle")
    @C0064Am(aul = "5c745a61c6743a3047dd161750853688", aum = 0)
    /* renamed from: ar */
    private String m3196ar() {
        return m3194ao();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Damage Name")
    @C0064Am(aul = "aed5c2c371ccbe96d99fc8cc2e0d7630", aum = 0)
    private I18NString aQl() {
        return aQj();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Shield Piercing")
    @C0064Am(aul = "8c1afdf053faf116c71a1762f8fd7980", aum = 0)
    private boolean aQn() {
        return aQk();
    }

    @C0064Am(aul = "d61a55e61d1bf2d9e28cfb77316f4b0b", aum = 0)
    private boolean aQp() {
        return aQk();
    }

    @C0064Am(aul = "9f1595dd6513e88febd60214978a1c78", aum = 0)
    /* renamed from: au */
    private String m3197au() {
        return aQm().get();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Icon")
    @C0064Am(aul = "554a883e7fd41cd974e65ac3c879a97c", aum = 0)
    /* renamed from: sJ */
    private Asset m3206sJ() {
        return m3205sG();
    }
}
