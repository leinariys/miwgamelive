package game.script.faction;

import game.network.message.externalizable.aCE;
import game.network.message.serializable.C3438ra;
import game.script.pda.DatabaseCategory;
import game.script.pda.TaikopediaEntry;
import game.script.resource.Asset;
import logic.baa.*;
import logic.data.mbean.C0383FI;
import logic.res.code.C5640aRc;
import logic.res.code.C5663aRz;
import logic.res.code.C6494any;
import logic.res.html.C2491fm;
import p001a.*;
import taikodom.infra.script.I18NString;

import java.text.SimpleDateFormat;
import java.util.*;

@C5511aMd
@C6485anp
/* renamed from: a.Xm */
/* compiled from: a */
public class Faction extends TaikodomObject implements C0468GU, C1616Xf, C4068yr {

    /* renamed from: MN */
    public static final C2491fm f2136MN = null;

    /* renamed from: NY */
    public static final C5663aRz f2137NY = null;

    /* renamed from: Ob */
    public static final C2491fm f2138Ob = null;

    /* renamed from: Oc */
    public static final C2491fm f2139Oc = null;

    /* renamed from: QA */
    public static final C5663aRz f2140QA = null;

    /* renamed from: QE */
    public static final C2491fm f2141QE = null;

    /* renamed from: QF */
    public static final C2491fm f2142QF = null;
    public static final C2491fm _f_toString_0020_0028_0029Ljava_002flang_002fString_003b = null;
    public static final int _m_fieldCount = 0;
    public static final C5663aRz[] _m_fields = null;
    public static final int _m_methodCount = 0;
    public static final C2491fm[] _m_methods = null;
    public static final C5663aRz aMg = null;
    public static final C5663aRz aMh = null;
    public static final C5663aRz aMi = null;
    public static final C2491fm aMj = null;
    public static final C2491fm aMk = null;
    public static final C2491fm aMn = null;
    public static final C2491fm aMo = null;
    public static final C2491fm aMp = null;
    public static final C2491fm aMq = null;
    public static final C2491fm aMr = null;
    public static final C2491fm aMs = null;
    public static final C2491fm aMu = null;
    public static final C2491fm aMv = null;
    public static final C5663aRz awd = null;
    /* renamed from: bL */
    public static final C5663aRz f2144bL = null;
    /* renamed from: bM */
    public static final C5663aRz f2145bM = null;
    /* renamed from: bN */
    public static final C2491fm f2146bN = null;
    /* renamed from: bO */
    public static final C2491fm f2147bO = null;
    /* renamed from: bP */
    public static final C2491fm f2148bP = null;
    /* renamed from: bQ */
    public static final C2491fm f2149bQ = null;
    public static final C5663aRz fsM = null;
    public static final C5663aRz fsN = null;
    public static final C5663aRz fsO = null;
    public static final C5663aRz fsP = null;
    public static final C5663aRz fsQ = null;
    public static final C2491fm fsR = null;
    public static final C2491fm fsS = null;
    public static final C2491fm fsT = null;
    public static final C2491fm fsU = null;
    public static final C2491fm fsV = null;
    public static final C2491fm fsW = null;
    public static final C2491fm fsX = null;
    public static final C2491fm fsY = null;
    public static final C2491fm fsZ = null;
    public static final C2491fm fta = null;
    public static final C2491fm ftb = null;
    public static final C2491fm ftc = null;
    public static final C2491fm ftd = null;
    public static final C2491fm fte = null;
    public static final C2491fm ftf = null;
    /* renamed from: gQ */
    public static final C2491fm f2150gQ = null;
    public static final long serialVersionUID = 0;
    /* renamed from: zQ */
    public static final C5663aRz f2158zQ = null;
    /* renamed from: zT */
    public static final C2491fm f2159zT = null;
    /* renamed from: zU */
    public static final C2491fm f2160zU = null;
    public static C6494any ___iScriptClass;
    @C0064Am(aul = "6fc936bab74200fedbfa5b0597a160e9", aum = 13)

    /* renamed from: bK */
    private static UUID f2143bK;
    @C0064Am(aul = "df0038e8813855fa3fcfc403b9edf07a", aum = 4)
    private static C3438ra<Faction> cWd;
    @C0064Am(aul = "f09589e5e64ce2ab3afda5418098214e", aum = 5)
    private static C3438ra<Faction> cWe;
    @C0064Am(aul = "e3e85a3b65ef49f20ac3e2ec85d19a0b", aum = 9)
    private static int cWf;
    @C0064Am(aul = "6377ff610424fdc253485ec6b10849fd", aum = 10)
    private static int cWg;
    @C0064Am(aul = "a8b7d9363596623f2c780c55ca1276b3", aum = 11)
    private static long cWh;
    @C0064Am(aul = "641909968f3566016ee5a8b24d5a117d", aum = 0)
    private static String handle;
    @C0064Am(aul = "78365578377015d444860deb33bdeddd", aum = 7)

    /* renamed from: jS */
    private static DatabaseCategory f2151jS;
    @C0064Am(aul = "c183c661fbd2daf29d37d3de9c5f7d0a", aum = 6)

    /* renamed from: jT */
    private static C3438ra<TaikopediaEntry> f2152jT;
    @C0064Am(aul = "16d32119c8015dc0de404e7fc61981b7", aum = 2)

    /* renamed from: jU */
    private static Asset f2153jU;
    @C0064Am(aul = "0b8a94e8468294381dae89fbfa8e5eac", aum = 12)

    /* renamed from: jV */
    private static float f2154jV;
    @C0064Am(aul = "3b5260d03c510100872172ff4e01bf6d", aum = 1)

    /* renamed from: jn */
    private static Asset f2155jn;
    @C0064Am(aul = "87be180810a5f6bd6248a128a908ce1d", aum = 8)

    /* renamed from: nh */
    private static I18NString f2156nh;
    @C0064Am(aul = "01373be1e563edf6cd2fd417e133add1", aum = 3)

    /* renamed from: zP */
    private static I18NString f2157zP;

    static {
        m11597V();
    }

    public Faction() {
        super((C5540aNg) null);
        super.mo10S();
    }

    public Faction(C5540aNg ang) {
        super(ang);
    }

    /* renamed from: V */
    static void m11597V() {
        serialVersionUID = (long) 1;
        _m_fieldCount = TaikodomObject._m_fieldCount + 14;
        _m_methodCount = TaikodomObject._m_methodCount + 38;
        int i = TaikodomObject._m_fieldCount;
        C5663aRz[] arzArr = new C5663aRz[(i + 14)];
        C5663aRz b = C5640aRc.m17844b(Faction.class, "641909968f3566016ee5a8b24d5a117d", i);
        f2145bM = b;
        arzArr[i] = b;
        int i2 = i + 1;
        C5663aRz b2 = C5640aRc.m17844b(Faction.class, "3b5260d03c510100872172ff4e01bf6d", i2);
        f2137NY = b2;
        arzArr[i2] = b2;
        int i3 = i2 + 1;
        C5663aRz b3 = C5640aRc.m17844b(Faction.class, "16d32119c8015dc0de404e7fc61981b7", i3);
        aMg = b3;
        arzArr[i3] = b3;
        int i4 = i3 + 1;
        C5663aRz b4 = C5640aRc.m17844b(Faction.class, "01373be1e563edf6cd2fd417e133add1", i4);
        f2158zQ = b4;
        arzArr[i4] = b4;
        int i5 = i4 + 1;
        C5663aRz b5 = C5640aRc.m17844b(Faction.class, "df0038e8813855fa3fcfc403b9edf07a", i5);
        fsM = b5;
        arzArr[i5] = b5;
        int i6 = i5 + 1;
        C5663aRz b6 = C5640aRc.m17844b(Faction.class, "f09589e5e64ce2ab3afda5418098214e", i6);
        fsN = b6;
        arzArr[i6] = b6;
        int i7 = i6 + 1;
        C5663aRz b7 = C5640aRc.m17844b(Faction.class, "c183c661fbd2daf29d37d3de9c5f7d0a", i7);
        awd = b7;
        arzArr[i7] = b7;
        int i8 = i7 + 1;
        C5663aRz b8 = C5640aRc.m17844b(Faction.class, "78365578377015d444860deb33bdeddd", i8);
        aMh = b8;
        arzArr[i8] = b8;
        int i9 = i8 + 1;
        C5663aRz b9 = C5640aRc.m17844b(Faction.class, "87be180810a5f6bd6248a128a908ce1d", i9);
        f2140QA = b9;
        arzArr[i9] = b9;
        int i10 = i9 + 1;
        C5663aRz b10 = C5640aRc.m17844b(Faction.class, "e3e85a3b65ef49f20ac3e2ec85d19a0b", i10);
        fsO = b10;
        arzArr[i10] = b10;
        int i11 = i10 + 1;
        C5663aRz b11 = C5640aRc.m17844b(Faction.class, "6377ff610424fdc253485ec6b10849fd", i11);
        fsP = b11;
        arzArr[i11] = b11;
        int i12 = i11 + 1;
        C5663aRz b12 = C5640aRc.m17844b(Faction.class, "a8b7d9363596623f2c780c55ca1276b3", i12);
        fsQ = b12;
        arzArr[i12] = b12;
        int i13 = i12 + 1;
        C5663aRz b13 = C5640aRc.m17844b(Faction.class, "0b8a94e8468294381dae89fbfa8e5eac", i13);
        aMi = b13;
        arzArr[i13] = b13;
        int i14 = i13 + 1;
        C5663aRz b14 = C5640aRc.m17844b(Faction.class, "6fc936bab74200fedbfa5b0597a160e9", i14);
        f2144bL = b14;
        arzArr[i14] = b14;
        int i15 = i14 + 1;
        _m_fields = arzArr;
        C1634Xv.m11725a((Object[]) TaikodomObject._m_fields, (Object[]) _m_fields);
        int i16 = TaikodomObject._m_methodCount;
        C2491fm[] fmVarArr = new C2491fm[(i16 + 38)];
        C2491fm a = C4105zY.m41624a(Faction.class, "b4fceac4d0dbc1dbc86ea2c0368fd312", i16);
        f2146bN = a;
        fmVarArr[i16] = a;
        int i17 = i16 + 1;
        C2491fm a2 = C4105zY.m41624a(Faction.class, "0f34167e96fd5471991b534e060d6937", i17);
        f2147bO = a2;
        fmVarArr[i17] = a2;
        int i18 = i17 + 1;
        C2491fm a3 = C4105zY.m41624a(Faction.class, "154b1a980c8c1989143681bfda0050ea", i18);
        f2149bQ = a3;
        fmVarArr[i18] = a3;
        int i19 = i18 + 1;
        C2491fm a4 = C4105zY.m41624a(Faction.class, "b8f0dd3ff98c1f280de6405ab9ece6da", i19);
        f2148bP = a4;
        fmVarArr[i19] = a4;
        int i20 = i19 + 1;
        C2491fm a5 = C4105zY.m41624a(Faction.class, "dc471ce8d2d33e51297ae3fa7d2c8b8c", i20);
        f2160zU = a5;
        fmVarArr[i20] = a5;
        int i21 = i20 + 1;
        C2491fm a6 = C4105zY.m41624a(Faction.class, "768b4153cbb4f117c443a8dd901356f8", i21);
        f2159zT = a6;
        fmVarArr[i21] = a6;
        int i22 = i21 + 1;
        C2491fm a7 = C4105zY.m41624a(Faction.class, "6f37d495c4bc66f7d1880ea00b395495", i22);
        fsR = a7;
        fmVarArr[i22] = a7;
        int i23 = i22 + 1;
        C2491fm a8 = C4105zY.m41624a(Faction.class, "ba6458c91dd83d89d9f9803239fdf236", i23);
        fsS = a8;
        fmVarArr[i23] = a8;
        int i24 = i23 + 1;
        C2491fm a9 = C4105zY.m41624a(Faction.class, "704cb3f5c78961c8deb01f6f0ad7311a", i24);
        fsT = a9;
        fmVarArr[i24] = a9;
        int i25 = i24 + 1;
        C2491fm a10 = C4105zY.m41624a(Faction.class, "95eab90d00ea115e5ce93faeb03d97b8", i25);
        fsU = a10;
        fmVarArr[i25] = a10;
        int i26 = i25 + 1;
        C2491fm a11 = C4105zY.m41624a(Faction.class, "43edcece3952390637698ad7051974c1", i26);
        fsV = a11;
        fmVarArr[i26] = a11;
        int i27 = i26 + 1;
        C2491fm a12 = C4105zY.m41624a(Faction.class, "458aa5f0607cfb99f158685553a59d3a", i27);
        fsW = a12;
        fmVarArr[i27] = a12;
        int i28 = i27 + 1;
        C2491fm a13 = C4105zY.m41624a(Faction.class, "98aaed7a5b5e22300fcab0a4da0f9cbc", i28);
        fsX = a13;
        fmVarArr[i28] = a13;
        int i29 = i28 + 1;
        C2491fm a14 = C4105zY.m41624a(Faction.class, "0340731aef753402dbd54317b4e61855", i29);
        fsY = a14;
        fmVarArr[i29] = a14;
        int i30 = i29 + 1;
        C2491fm a15 = C4105zY.m41624a(Faction.class, "7080a2ec94bacbfc748ecae08952d086", i30);
        _f_toString_0020_0028_0029Ljava_002flang_002fString_003b = a15;
        fmVarArr[i30] = a15;
        int i31 = i30 + 1;
        C2491fm a16 = C4105zY.m41624a(Faction.class, "75764c236e05dd54c11051f08adeabac", i31);
        aMn = a16;
        fmVarArr[i31] = a16;
        int i32 = i31 + 1;
        C2491fm a17 = C4105zY.m41624a(Faction.class, "beec123e60e06014850c5d12df27cd91", i32);
        aMo = a17;
        fmVarArr[i32] = a17;
        int i33 = i32 + 1;
        C2491fm a18 = C4105zY.m41624a(Faction.class, "1360b12c5fef318a4cd41599932cc789", i33);
        aMk = a18;
        fmVarArr[i33] = a18;
        int i34 = i33 + 1;
        C2491fm a19 = C4105zY.m41624a(Faction.class, "ed51f8fb5616772408acbf85a341b5b0", i34);
        f2141QE = a19;
        fmVarArr[i34] = a19;
        int i35 = i34 + 1;
        C2491fm a20 = C4105zY.m41624a(Faction.class, "1736e0c71e125092f6119597654c92c7", i35);
        f2142QF = a20;
        fmVarArr[i35] = a20;
        int i36 = i35 + 1;
        C2491fm a21 = C4105zY.m41624a(Faction.class, "db520eb61681bfc0a767441450f0a73e", i36);
        aMj = a21;
        fmVarArr[i36] = a21;
        int i37 = i36 + 1;
        C2491fm a22 = C4105zY.m41624a(Faction.class, "98c8562ef386281811e12bfda208b1ab", i37);
        f2136MN = a22;
        fmVarArr[i37] = a22;
        int i38 = i37 + 1;
        C2491fm a23 = C4105zY.m41624a(Faction.class, "d922f8d12da4ed6b6fb96741e6454cb5", i38);
        fsZ = a23;
        fmVarArr[i38] = a23;
        int i39 = i38 + 1;
        C2491fm a24 = C4105zY.m41624a(Faction.class, "710c48bf315499ce0b824202a7f00aa6", i39);
        fta = a24;
        fmVarArr[i39] = a24;
        int i40 = i39 + 1;
        C2491fm a25 = C4105zY.m41624a(Faction.class, "55966b38da535d9d461f40777c0c42b3", i40);
        ftb = a25;
        fmVarArr[i40] = a25;
        int i41 = i40 + 1;
        C2491fm a26 = C4105zY.m41624a(Faction.class, "855577e3d93ffc092a727dde0dcfa2d8", i41);
        ftc = a26;
        fmVarArr[i41] = a26;
        int i42 = i41 + 1;
        C2491fm a27 = C4105zY.m41624a(Faction.class, "f5f86669ab2460a6d220718c34ddb334", i42);
        ftd = a27;
        fmVarArr[i42] = a27;
        int i43 = i42 + 1;
        C2491fm a28 = C4105zY.m41624a(Faction.class, "fbd1a8a9bac0de2c944eb51bd915375e", i43);
        fte = a28;
        fmVarArr[i43] = a28;
        int i44 = i43 + 1;
        C2491fm a29 = C4105zY.m41624a(Faction.class, "c40aa57353c43fe613a7fb6ece077d72", i44);
        ftf = a29;
        fmVarArr[i44] = a29;
        int i45 = i44 + 1;
        C2491fm a30 = C4105zY.m41624a(Faction.class, "1e70489266836bc408afcdd323effc97", i45);
        aMp = a30;
        fmVarArr[i45] = a30;
        int i46 = i45 + 1;
        C2491fm a31 = C4105zY.m41624a(Faction.class, "6429a5ffd7521ee5c8a3e5f4a7104bbe", i46);
        f2139Oc = a31;
        fmVarArr[i46] = a31;
        int i47 = i46 + 1;
        C2491fm a32 = C4105zY.m41624a(Faction.class, "4ed8b742c8041b97fc161172985be61b", i47);
        f2138Ob = a32;
        fmVarArr[i47] = a32;
        int i48 = i47 + 1;
        C2491fm a33 = C4105zY.m41624a(Faction.class, "a2a908b74bafc9719e9322dc42250ba4", i48);
        aMq = a33;
        fmVarArr[i48] = a33;
        int i49 = i48 + 1;
        C2491fm a34 = C4105zY.m41624a(Faction.class, "e82a4b07287e4f3322770d3f21d930d7", i49);
        aMr = a34;
        fmVarArr[i49] = a34;
        int i50 = i49 + 1;
        C2491fm a35 = C4105zY.m41624a(Faction.class, "d996e19eee9643ca5c09e01de1b881e9", i50);
        aMs = a35;
        fmVarArr[i50] = a35;
        int i51 = i50 + 1;
        C2491fm a36 = C4105zY.m41624a(Faction.class, "d8232a0d48c2fbb16f7cfc6855beac46", i51);
        aMu = a36;
        fmVarArr[i51] = a36;
        int i52 = i51 + 1;
        C2491fm a37 = C4105zY.m41624a(Faction.class, "7a34cba4dbadf50199b31d856f261e45", i52);
        aMv = a37;
        fmVarArr[i52] = a37;
        int i53 = i52 + 1;
        C2491fm a38 = C4105zY.m41624a(Faction.class, "8beed3c37b1c8c4fcf1c5f7ffe3f3e08", i53);
        f2150gQ = a38;
        fmVarArr[i53] = a38;
        int i54 = i53 + 1;
        _m_methods = fmVarArr;
        C1634Xv.m11725a((Object[]) TaikodomObject._m_methods, (Object[]) _m_methods);
        ___iScriptClass = aUO.m18566a(Faction.class, C0383FI.class, _m_fields, _m_methods);
    }

    /* renamed from: L */
    private void m11584L(Asset tCVar) {
        bFf().mo5608dq().mo3197f(aMg, tCVar);
    }

    /* renamed from: Ll */
    private C3438ra m11585Ll() {
        return (C3438ra) bFf().mo5608dq().mo3214p(awd);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Pda Render")
    @C0064Am(aul = "d996e19eee9643ca5c09e01de1b881e9", aum = 0)
    @C5566aOg
    /* renamed from: M */
    private void m11586M(Asset tCVar) {
        throw new aWi(new aCE(this, aMs, new Object[]{tCVar}));
    }

    /* renamed from: RQ */
    private Asset m11587RQ() {
        return (Asset) bFf().mo5608dq().mo3214p(aMg);
    }

    /* renamed from: RR */
    private DatabaseCategory m11588RR() {
        return (DatabaseCategory) bFf().mo5608dq().mo3214p(aMh);
    }

    /* renamed from: RS */
    private float m11589RS() {
        return bFf().mo5608dq().mo3211m(aMi);
    }

    /* renamed from: a */
    private void m11598a(DatabaseCategory aik) {
        bFf().mo5608dq().mo3197f(aMh, aik);
    }

    /* renamed from: a */
    private void m11599a(String str) {
        bFf().mo5608dq().mo3197f(f2145bM, str);
    }

    /* renamed from: a */
    private void m11600a(UUID uuid) {
        bFf().mo5608dq().mo3197f(f2144bL, uuid);
    }

    /* renamed from: an */
    private UUID m11601an() {
        return (UUID) bFf().mo5608dq().mo3214p(f2144bL);
    }

    /* renamed from: ao */
    private String m11602ao() {
        return (String) bFf().mo5608dq().mo3214p(f2145bM);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Database Category")
    @C0064Am(aul = "beec123e60e06014850c5d12df27cd91", aum = 0)
    @C5566aOg
    /* renamed from: b */
    private void m11606b(DatabaseCategory aik) {
        throw new aWi(new aCE(this, aMo, new Object[]{aik}));
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Handle")
    @C0064Am(aul = "154b1a980c8c1989143681bfda0050ea", aum = 0)
    @C5566aOg
    /* renamed from: b */
    private void m11607b(String str) {
        throw new aWi(new aCE(this, f2149bQ, new Object[]{str}));
    }

    /* renamed from: bM */
    private void m11609bM(C3438ra raVar) {
        bFf().mo5608dq().mo3197f(fsM, raVar);
    }

    /* renamed from: bN */
    private void m11610bN(C3438ra raVar) {
        bFf().mo5608dq().mo3197f(fsN, raVar);
    }

    private C3438ra bUZ() {
        return (C3438ra) bFf().mo5608dq().mo3214p(fsM);
    }

    private C3438ra bVa() {
        return (C3438ra) bFf().mo5608dq().mo3214p(fsN);
    }

    private int bVb() {
        return bFf().mo5608dq().mo3212n(fsO);
    }

    private int bVc() {
        return bFf().mo5608dq().mo3212n(fsP);
    }

    private long bVd() {
        return bFf().mo5608dq().mo3213o(fsQ);
    }

    /* renamed from: br */
    private void m11611br(I18NString i18NString) {
        bFf().mo5608dq().mo3197f(f2140QA, i18NString);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Description")
    @C0064Am(aul = "1736e0c71e125092f6119597654c92c7", aum = 0)
    @C5566aOg
    /* renamed from: bu */
    private void m11612bu(I18NString i18NString) {
        throw new aWi(new aCE(this, f2142QF, new Object[]{i18NString}));
    }

    /* renamed from: c */
    private void m11613c(UUID uuid) {
        switch (bFf().mo6893i(f2147bO)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f2147bO, new Object[]{uuid}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f2147bO, new Object[]{uuid}));
                break;
        }
        m11608b(uuid);
    }

    /* renamed from: cx */
    private void m11614cx(float f) {
        bFf().mo5608dq().mo3150a(aMi, f);
    }

    /* renamed from: d */
    private void m11616d(I18NString i18NString) {
        bFf().mo5608dq().mo3197f(f2158zQ, i18NString);
    }

    @C0064Am(aul = "8beed3c37b1c8c4fcf1c5f7ffe3f3e08", aum = 0)
    @C5566aOg
    /* renamed from: dd */
    private void m11617dd() {
        throw new aWi(new aCE(this, f2150gQ, new Object[0]));
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Name")
    @C0064Am(aul = "dc471ce8d2d33e51297ae3fa7d2c8b8c", aum = 0)
    @C5566aOg
    /* renamed from: e */
    private void m11618e(I18NString i18NString) {
        throw new aWi(new aCE(this, f2160zU, new Object[]{i18NString}));
    }

    /* renamed from: ha */
    private void m11619ha(long j) {
        bFf().mo5608dq().mo3184b(fsQ, j);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Foundation Date")
    @C0064Am(aul = "c40aa57353c43fe613a7fb6ece077d72", aum = 0)
    @C5566aOg
    /* renamed from: iB */
    private void m11620iB(String str) {
        throw new aWi(new aCE(this, ftf, new Object[]{str}));
    }

    /* renamed from: kb */
    private I18NString m11621kb() {
        return (I18NString) bFf().mo5608dq().mo3214p(f2158zQ);
    }

    @C3248pc(aYR = C3248pc.C3250b.ADDER, displayName = "EnemyFactions")
    @C0064Am(aul = "6f37d495c4bc66f7d1880ea00b395495", aum = 0)
    @C5566aOg
    /* renamed from: o */
    private void m11623o(Faction xm) {
        throw new aWi(new aCE(this, fsR, new Object[]{xm}));
    }

    @C3248pc(aYR = C3248pc.C3250b.REMOVER, displayName = "EnemyFactions")
    @C0064Am(aul = "ba6458c91dd83d89d9f9803239fdf236", aum = 0)
    @C5566aOg
    /* renamed from: q */
    private void m11624q(Faction xm) {
        throw new aWi(new aCE(this, fsS, new Object[]{xm}));
    }

    /* renamed from: qo */
    private void m11625qo(int i) {
        bFf().mo5608dq().mo3183b(fsO, i);
    }

    /* renamed from: qp */
    private void m11626qp(int i) {
        bFf().mo5608dq().mo3183b(fsP, i);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Ranking")
    @C0064Am(aul = "710c48bf315499ce0b824202a7f00aa6", aum = 0)
    @C5566aOg
    /* renamed from: qq */
    private void m11627qq(int i) {
        throw new aWi(new aCE(this, fta, new Object[]{new Integer(i)}));
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Affiliates count")
    @C0064Am(aul = "855577e3d93ffc092a727dde0dcfa2d8", aum = 0)
    @C5566aOg
    /* renamed from: qs */
    private void m11628qs(int i) {
        throw new aWi(new aCE(this, ftc, new Object[]{new Integer(i)}));
    }

    @C3248pc(aYR = C3248pc.C3250b.ADDER, displayName = "AlliedFactions")
    @C0064Am(aul = "95eab90d00ea115e5ce93faeb03d97b8", aum = 0)
    @C5566aOg
    /* renamed from: s */
    private void m11630s(Faction xm) {
        throw new aWi(new aCE(this, fsU, new Object[]{xm}));
    }

    /* renamed from: sG */
    private Asset m11631sG() {
        return (Asset) bFf().mo5608dq().mo3214p(f2137NY);
    }

    /* renamed from: t */
    private void m11633t(Asset tCVar) {
        bFf().mo5608dq().mo3197f(f2137NY, tCVar);
    }

    @C3248pc(aYR = C3248pc.C3250b.REMOVER, displayName = "AlliedFactions")
    @C0064Am(aul = "43edcece3952390637698ad7051974c1", aum = 0)
    @C5566aOg
    /* renamed from: u */
    private void m11634u(Faction xm) {
        throw new aWi(new aCE(this, fsV, new Object[]{xm}));
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Icon")
    @C0064Am(aul = "6429a5ffd7521ee5c8a3e5f4a7104bbe", aum = 0)
    @C5566aOg
    /* renamed from: u */
    private void m11635u(Asset tCVar) {
        throw new aWi(new aCE(this, f2139Oc, new Object[]{tCVar}));
    }

    /* renamed from: vT */
    private I18NString m11636vT() {
        return (I18NString) bFf().mo5608dq().mo3214p(f2140QA);
    }

    /* renamed from: x */
    private void m11639x(C3438ra raVar) {
        bFf().mo5608dq().mo3197f(awd, raVar);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Pda Render")
    @C5566aOg
    /* renamed from: N */
    public void mo6801N(Asset tCVar) {
        switch (bFf().mo6893i(aMs)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, aMs, new Object[]{tCVar}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, aMs, new Object[]{tCVar}));
                break;
        }
        m11586M(tCVar);
    }

    /* renamed from: RU */
    public I18NString mo182RU() {
        switch (bFf().mo6893i(aMj)) {
            case 0:
                return null;
            case 2:
                return (I18NString) bFf().mo5606d(new aCE(this, aMj, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, aMj, new Object[0]));
                break;
        }
        return m11590RT();
    }

    /* renamed from: RW */
    public List<TaikopediaEntry> mo183RW() {
        switch (bFf().mo6893i(aMk)) {
            case 0:
                return null;
            case 2:
                return (List) bFf().mo5606d(new aCE(this, aMk, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, aMk, new Object[0]));
                break;
        }
        return m11591RV();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Database Category")
    /* renamed from: RY */
    public DatabaseCategory mo184RY() {
        switch (bFf().mo6893i(aMn)) {
            case 0:
                return null;
            case 2:
                return (DatabaseCategory) bFf().mo5606d(new aCE(this, aMn, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, aMn, new Object[0]));
                break;
        }
        return m11592RX();
    }

    /* renamed from: Sa */
    public String mo185Sa() {
        switch (bFf().mo6893i(aMp)) {
            case 0:
                return null;
            case 2:
                return (String) bFf().mo5606d(new aCE(this, aMp, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, aMp, new Object[0]));
                break;
        }
        return m11593RZ();
    }

    /* renamed from: Sc */
    public String mo6802Sc() {
        switch (bFf().mo6893i(aMq)) {
            case 0:
                return null;
            case 2:
                return (String) bFf().mo5606d(new aCE(this, aMq, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, aMq, new Object[0]));
                break;
        }
        return m11594Sb();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Pda Render")
    /* renamed from: Se */
    public Asset mo187Se() {
        switch (bFf().mo6893i(aMr)) {
            case 0:
                return null;
            case 2:
                return (Asset) bFf().mo5606d(new aCE(this, aMr, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, aMr, new Object[0]));
                break;
        }
        return m11595Sd();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Pda MP Multiplier")
    /* renamed from: Sg */
    public float mo188Sg() {
        switch (bFf().mo6893i(aMu)) {
            case 0:
                return 0.0f;
            case 2:
                return ((Float) bFf().mo5606d(new aCE(this, aMu, new Object[0]))).floatValue();
            case 3:
                bFf().mo5606d(new aCE(this, aMu, new Object[0]));
                break;
        }
        return m11596Sf();
    }

    /* renamed from: T */
    public C6494any mo11T() {
        return ___iScriptClass;
    }

    /* renamed from: U */
    public C2491fm[] mo12U() {
        return _m_methods;
    }

    /* renamed from: W */
    public Object mo13W() {
        return new C0383FI(this);
    }

    /* renamed from: a */
    public Object mo14a(C0495Gr gr) {
        Object[] args = gr.getArgs();
        switch (gr.mo2417hq() - TaikodomObject._m_methodCount) {
            case 0:
                return m11603ap();
            case 1:
                m11608b((UUID) args[0]);
                return null;
            case 2:
                m11607b((String) args[0]);
                return null;
            case 3:
                return m11604ar();
            case 4:
                m11618e((I18NString) args[0]);
                return null;
            case 5:
                return m11622kd();
            case 6:
                m11623o((Faction) args[0]);
                return null;
            case 7:
                m11624q((Faction) args[0]);
                return null;
            case 8:
                return bVe();
            case 9:
                m11630s((Faction) args[0]);
                return null;
            case 10:
                m11634u((Faction) args[0]);
                return null;
            case 11:
                return bVg();
            case 12:
                return new Boolean(m11638w((Faction) args[0]));
            case 13:
                return new Boolean(m11640y((Faction) args[0]));
            case 14:
                return m11605au();
            case 15:
                return m11592RX();
            case 16:
                m11606b((DatabaseCategory) args[0]);
                return null;
            case 17:
                return m11591RV();
            case 18:
                return m11637vV();
            case 19:
                m11612bu((I18NString) args[0]);
                return null;
            case 20:
                return m11590RT();
            case 21:
                return m11629rO();
            case 22:
                return bVi();
            case 23:
                m11627qq(((Integer) args[0]).intValue());
                return null;
            case 24:
                return bVk();
            case 25:
                m11628qs(((Integer) args[0]).intValue());
                return null;
            case 26:
                return bVm();
            case 27:
                return bVo();
            case 28:
                m11620iB((String) args[0]);
                return null;
            case 29:
                return m11593RZ();
            case 30:
                m11635u((Asset) args[0]);
                return null;
            case 31:
                return m11632sJ();
            case 32:
                return m11594Sb();
            case 33:
                return m11595Sd();
            case 34:
                m11586M((Asset) args[0]);
                return null;
            case 35:
                return new Float(m11596Sf());
            case 36:
                m11615cy(((Float) args[0]).floatValue());
                return null;
            case 37:
                m11617dd();
                return null;
            default:
                return super.mo14a(gr);
        }
    }

    /* renamed from: a */
    public void mo15a(Collection collection, C0495Gr gr) {
        super.mo15a(collection, gr);
    }

    /* renamed from: aq */
    public UUID mo18aq() {
        switch (bFf().mo6893i(f2146bN)) {
            case 0:
                return null;
            case 2:
                return (UUID) bFf().mo5606d(new aCE(this, f2146bN, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f2146bN, new Object[0]));
                break;
        }
        return m11603ap();
    }

    @C3248pc(aYR = C3248pc.C3250b.ITERATOR, displayName = "EnemyFactions")
    public List<Faction> bVf() {
        switch (bFf().mo6893i(fsT)) {
            case 0:
                return null;
            case 2:
                return (List) bFf().mo5606d(new aCE(this, fsT, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, fsT, new Object[0]));
                break;
        }
        return bVe();
    }

    @C3248pc(aYR = C3248pc.C3250b.ITERATOR, displayName = "AlliedFactions")
    public List<Faction> bVh() {
        switch (bFf().mo6893i(fsW)) {
            case 0:
                return null;
            case 2:
                return (List) bFf().mo5606d(new aCE(this, fsW, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, fsW, new Object[0]));
                break;
        }
        return bVg();
    }

    @aFW("Ranking")
    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Ranking")
    public Integer bVj() {
        switch (bFf().mo6893i(fsZ)) {
            case 0:
                return null;
            case 2:
                return (Integer) bFf().mo5606d(new aCE(this, fsZ, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, fsZ, new Object[0]));
                break;
        }
        return bVi();
    }

    @aFW("Number of Affiliates")
    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Affiliates count")
    public Integer bVl() {
        switch (bFf().mo6893i(ftb)) {
            case 0:
                return null;
            case 2:
                return (Integer) bFf().mo5606d(new aCE(this, ftb, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, ftb, new Object[0]));
                break;
        }
        return bVk();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Foundation Date")
    public String bVn() {
        switch (bFf().mo6893i(ftd)) {
            case 0:
                return null;
            case 2:
                return (String) bFf().mo5606d(new aCE(this, ftd, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, ftd, new Object[0]));
                break;
        }
        return bVm();
    }

    @aFW("Foundation Date")
    public Date bVp() {
        switch (bFf().mo6893i(fte)) {
            case 0:
                return null;
            case 2:
                return (Date) bFf().mo5606d(new aCE(this, fte, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, fte, new Object[0]));
                break;
        }
        return bVo();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Description")
    @C5566aOg
    /* renamed from: bv */
    public void mo6809bv(I18NString i18NString) {
        switch (bFf().mo6893i(f2142QF)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f2142QF, new Object[]{i18NString}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f2142QF, new Object[]{i18NString}));
                break;
        }
        m11612bu(i18NString);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Database Category")
    @C5566aOg
    /* renamed from: c */
    public void mo6810c(DatabaseCategory aik) {
        switch (bFf().mo6893i(aMo)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, aMo, new Object[]{aik}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, aMo, new Object[]{aik}));
                break;
        }
        m11606b(aik);
    }

    /* renamed from: c */
    public C5663aRz[] mo25c() {
        return _m_fields;
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Pda MP Multiplier")
    /* renamed from: cz */
    public void mo6811cz(float f) {
        switch (bFf().mo6893i(aMv)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, aMv, new Object[]{new Float(f)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, aMv, new Object[]{new Float(f)}));
                break;
        }
        m11615cy(f);
    }

    @C5566aOg
    /* renamed from: de */
    public void mo6812de() {
        switch (bFf().mo6893i(f2150gQ)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f2150gQ, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f2150gQ, new Object[0]));
                break;
        }
        m11617dd();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Name")
    @C5566aOg
    /* renamed from: f */
    public void mo6813f(I18NString i18NString) {
        switch (bFf().mo6893i(f2160zU)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f2160zU, new Object[]{i18NString}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f2160zU, new Object[]{i18NString}));
                break;
        }
        m11618e(i18NString);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Handle")
    public String getHandle() {
        switch (bFf().mo6893i(f2148bP)) {
            case 0:
                return null;
            case 2:
                return (String) bFf().mo5606d(new aCE(this, f2148bP, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f2148bP, new Object[0]));
                break;
        }
        return m11604ar();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Handle")
    @C5566aOg
    public void setHandle(String str) {
        switch (bFf().mo6893i(f2149bQ)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f2149bQ, new Object[]{str}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f2149bQ, new Object[]{str}));
                break;
        }
        m11607b(str);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Foundation Date")
    @C5566aOg
    /* renamed from: iC */
    public void mo6814iC(String str) {
        switch (bFf().mo6893i(ftf)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, ftf, new Object[]{str}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, ftf, new Object[]{str}));
                break;
        }
        m11620iB(str);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Name")
    /* renamed from: ke */
    public I18NString mo6815ke() {
        switch (bFf().mo6893i(f2159zT)) {
            case 0:
                return null;
            case 2:
                return (I18NString) bFf().mo5606d(new aCE(this, f2159zT, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f2159zT, new Object[0]));
                break;
        }
        return m11622kd();
    }

    @C3248pc(aYR = C3248pc.C3250b.ADDER, displayName = "EnemyFactions")
    @C5566aOg
    /* renamed from: p */
    public void mo6816p(Faction xm) {
        switch (bFf().mo6893i(fsR)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, fsR, new Object[]{xm}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, fsR, new Object[]{xm}));
                break;
        }
        m11623o(xm);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Ranking")
    @C5566aOg
    /* renamed from: qr */
    public void mo6817qr(int i) {
        switch (bFf().mo6893i(fta)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, fta, new Object[]{new Integer(i)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, fta, new Object[]{new Integer(i)}));
                break;
        }
        m11627qq(i);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Affiliates count")
    @C5566aOg
    /* renamed from: qt */
    public void mo6818qt(int i) {
        switch (bFf().mo6893i(ftc)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, ftc, new Object[]{new Integer(i)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, ftc, new Object[]{new Integer(i)}));
                break;
        }
        m11628qs(i);
    }

    @C3248pc(aYR = C3248pc.C3250b.REMOVER, displayName = "EnemyFactions")
    @C5566aOg
    /* renamed from: r */
    public void mo6819r(Faction xm) {
        switch (bFf().mo6893i(fsS)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, fsS, new Object[]{xm}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, fsS, new Object[]{xm}));
                break;
        }
        m11624q(xm);
    }

    /* renamed from: rP */
    public I18NString mo195rP() {
        switch (bFf().mo6893i(f2136MN)) {
            case 0:
                return null;
            case 2:
                return (I18NString) bFf().mo5606d(new aCE(this, f2136MN, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f2136MN, new Object[0]));
                break;
        }
        return m11629rO();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Icon")
    /* renamed from: sK */
    public Asset mo6820sK() {
        switch (bFf().mo6893i(f2138Ob)) {
            case 0:
                return null;
            case 2:
                return (Asset) bFf().mo5606d(new aCE(this, f2138Ob, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f2138Ob, new Object[0]));
                break;
        }
        return m11632sJ();
    }

    @C3248pc(aYR = C3248pc.C3250b.ADDER, displayName = "AlliedFactions")
    @C5566aOg
    /* renamed from: t */
    public void mo6822t(Faction xm) {
        switch (bFf().mo6893i(fsU)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, fsU, new Object[]{xm}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, fsU, new Object[]{xm}));
                break;
        }
        m11630s(xm);
    }

    public String toString() {
        switch (bFf().mo6893i(_f_toString_0020_0028_0029Ljava_002flang_002fString_003b)) {
            case 0:
                return null;
            case 2:
                return (String) bFf().mo5606d(new aCE(this, _f_toString_0020_0028_0029Ljava_002flang_002fString_003b, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, _f_toString_0020_0028_0029Ljava_002flang_002fString_003b, new Object[0]));
                break;
        }
        return m11605au();
    }

    @C3248pc(aYR = C3248pc.C3250b.REMOVER, displayName = "AlliedFactions")
    @C5566aOg
    /* renamed from: v */
    public void mo6823v(Faction xm) {
        switch (bFf().mo6893i(fsV)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, fsV, new Object[]{xm}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, fsV, new Object[]{xm}));
                break;
        }
        m11634u(xm);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Icon")
    @C5566aOg
    /* renamed from: v */
    public void mo6824v(Asset tCVar) {
        switch (bFf().mo6893i(f2139Oc)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f2139Oc, new Object[]{tCVar}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f2139Oc, new Object[]{tCVar}));
                break;
        }
        m11635u(tCVar);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Description")
    /* renamed from: vW */
    public I18NString mo6825vW() {
        switch (bFf().mo6893i(f2141QE)) {
            case 0:
                return null;
            case 2:
                return (I18NString) bFf().mo5606d(new aCE(this, f2141QE, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f2141QE, new Object[0]));
                break;
        }
        return m11637vV();
    }

    /* renamed from: x */
    public boolean mo6826x(Faction xm) {
        switch (bFf().mo6893i(fsX)) {
            case 0:
                return false;
            case 2:
                return ((Boolean) bFf().mo5606d(new aCE(this, fsX, new Object[]{xm}))).booleanValue();
            case 3:
                bFf().mo5606d(new aCE(this, fsX, new Object[]{xm}));
                break;
        }
        return m11638w(xm);
    }

    /* renamed from: z */
    public boolean mo6827z(Faction xm) {
        switch (bFf().mo6893i(fsY)) {
            case 0:
                return false;
            case 2:
                return ((Boolean) bFf().mo5606d(new aCE(this, fsY, new Object[]{xm}))).booleanValue();
            case 3:
                bFf().mo5606d(new aCE(this, fsY, new Object[]{xm}));
                break;
        }
        return m11640y(xm);
    }

    @C0064Am(aul = "b4fceac4d0dbc1dbc86ea2c0368fd312", aum = 0)
    /* renamed from: ap */
    private UUID m11603ap() {
        return m11601an();
    }

    @C0064Am(aul = "0f34167e96fd5471991b534e060d6937", aum = 0)
    /* renamed from: b */
    private void m11608b(UUID uuid) {
        m11600a(uuid);
    }

    /* renamed from: S */
    public void mo10S() {
        super.mo10S();
        m11600a(UUID.randomUUID());
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Handle")
    @C0064Am(aul = "b8f0dd3ff98c1f280de6405ab9ece6da", aum = 0)
    /* renamed from: ar */
    private String m11604ar() {
        return m11602ao();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Name")
    @C0064Am(aul = "768b4153cbb4f117c443a8dd901356f8", aum = 0)
    /* renamed from: kd */
    private I18NString m11622kd() {
        return m11621kb();
    }

    @C3248pc(aYR = C3248pc.C3250b.ITERATOR, displayName = "EnemyFactions")
    @C0064Am(aul = "704cb3f5c78961c8deb01f6f0ad7311a", aum = 0)
    private List<Faction> bVe() {
        return Collections.unmodifiableList(bUZ());
    }

    @C3248pc(aYR = C3248pc.C3250b.ITERATOR, displayName = "AlliedFactions")
    @C0064Am(aul = "458aa5f0607cfb99f158685553a59d3a", aum = 0)
    private List<Faction> bVg() {
        return Collections.unmodifiableList(bVa());
    }

    @C0064Am(aul = "98aaed7a5b5e22300fcab0a4da0f9cbc", aum = 0)
    /* renamed from: w */
    private boolean m11638w(Faction xm) {
        return bUZ().contains(xm);
    }

    @C0064Am(aul = "0340731aef753402dbd54317b4e61855", aum = 0)
    /* renamed from: y */
    private boolean m11640y(Faction xm) {
        return bVa().contains(xm);
    }

    @C0064Am(aul = "7080a2ec94bacbfc748ecae08952d086", aum = 0)
    /* renamed from: au */
    private String m11605au() {
        return "[" + m11602ao() + "]: " + m11621kb().get();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Database Category")
    @C0064Am(aul = "75764c236e05dd54c11051f08adeabac", aum = 0)
    /* renamed from: RX */
    private DatabaseCategory m11592RX() {
        return m11588RR();
    }

    @C0064Am(aul = "1360b12c5fef318a4cd41599932cc789", aum = 0)
    /* renamed from: RV */
    private List<TaikopediaEntry> m11591RV() {
        return Collections.unmodifiableList(m11585Ll());
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Description")
    @C0064Am(aul = "ed51f8fb5616772408acbf85a341b5b0", aum = 0)
    /* renamed from: vV */
    private I18NString m11637vV() {
        return m11636vT();
    }

    @C0064Am(aul = "db520eb61681bfc0a767441450f0a73e", aum = 0)
    /* renamed from: RT */
    private I18NString m11590RT() {
        return mo6825vW();
    }

    @C0064Am(aul = "98c8562ef386281811e12bfda208b1ab", aum = 0)
    /* renamed from: rO */
    private I18NString m11629rO() {
        return mo6815ke();
    }

    @aFW("Ranking")
    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Ranking")
    @C0064Am(aul = "d922f8d12da4ed6b6fb96741e6454cb5", aum = 0)
    private Integer bVi() {
        return Integer.valueOf(bVb());
    }

    @aFW("Number of Affiliates")
    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Affiliates count")
    @C0064Am(aul = "55966b38da535d9d461f40777c0c42b3", aum = 0)
    private Integer bVk() {
        return Integer.valueOf(bVc());
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Foundation Date")
    @C0064Am(aul = "f5f86669ab2460a6d220718c34ddb334", aum = 0)
    private String bVm() {
        return new SimpleDateFormat("mm/dd/yyyy").format(new Date(bVd()));
    }

    @aFW("Foundation Date")
    @C0064Am(aul = "fbd1a8a9bac0de2c944eb51bd915375e", aum = 0)
    private Date bVo() {
        return new Date(bVd());
    }

    @C0064Am(aul = "1e70489266836bc408afcdd323effc97", aum = 0)
    /* renamed from: RZ */
    private String m11593RZ() {
        if (m11631sG() != null) {
            return m11631sG().getHandle();
        }
        return null;
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Icon")
    @C0064Am(aul = "4ed8b742c8041b97fc161172985be61b", aum = 0)
    /* renamed from: sJ */
    private Asset m11632sJ() {
        return m11631sG();
    }

    @C0064Am(aul = "a2a908b74bafc9719e9322dc42250ba4", aum = 0)
    /* renamed from: Sb */
    private String m11594Sb() {
        return m11587RQ().getHandle();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Pda Render")
    @C0064Am(aul = "e82a4b07287e4f3322770d3f21d930d7", aum = 0)
    /* renamed from: Sd */
    private Asset m11595Sd() {
        return m11587RQ();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Pda MP Multiplier")
    @C0064Am(aul = "d8232a0d48c2fbb16f7cfc6855beac46", aum = 0)
    /* renamed from: Sf */
    private float m11596Sf() {
        return m11589RS();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Pda MP Multiplier")
    @C0064Am(aul = "7a34cba4dbadf50199b31d856f261e45", aum = 0)
    /* renamed from: cy */
    private void m11615cy(float f) {
        m11614cx(f);
    }

    /* renamed from: a.Xm$a */
    public enum C1624a {
        ENEMY,
        NEUTRAL,
        ALLY
    }
}
