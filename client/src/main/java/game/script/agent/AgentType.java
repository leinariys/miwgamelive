package game.script.agent;

import game.network.message.externalizable.aCE;
import game.script.npc.NPCType;
import logic.baa.*;
import logic.data.mbean.C6690arm;
import logic.res.code.C5663aRz;
import logic.res.code.C6494any;
import logic.res.html.C2491fm;
import p001a.*;

import java.util.Collection;

@C0566Hp
@C5829abJ("1.1.1")
@C6485anp
@C5511aMd
/* renamed from: a.go */
/* compiled from: a */
public class AgentType extends NPCType implements C1616Xf {

    /* renamed from: PW */
    public static final C2491fm f7765PW = null;
    public static final int _m_fieldCount = 0;
    public static final C5663aRz[] _m_fields = null;
    public static final int _m_methodCount = 0;
    public static final C2491fm[] _m_methods = null;
    /* renamed from: dN */
    public static final C2491fm f7766dN = null;
    public static final long serialVersionUID = 0;
    public static C6494any ___iScriptClass;

    static {
        m32375V();
    }

    public AgentType() {
        super((C5540aNg) null);
        super.mo10S();
    }

    public AgentType(C5540aNg ang) {
        super(ang);
    }

    /* renamed from: V */
    static void m32375V() {
        serialVersionUID = (long) 1;
        _m_fieldCount = NPCType._m_fieldCount + 0;
        _m_methodCount = NPCType._m_methodCount + 2;
        _m_fields = new C5663aRz[(NPCType._m_fieldCount + 0)];
        C1634Xv.m11725a((Object[]) NPCType._m_fields, (Object[]) _m_fields);
        int i = NPCType._m_methodCount;
        C2491fm[] fmVarArr = new C2491fm[(i + 2)];
        C2491fm a = C4105zY.m41624a(AgentType.class, "da26a93bd14edcf6a44d9f95d329f406", i);
        f7765PW = a;
        fmVarArr[i] = a;
        int i2 = i + 1;
        C2491fm a2 = C4105zY.m41624a(AgentType.class, "791f886a73a1076eebb9f81a37fc904f", i2);
        f7766dN = a2;
        fmVarArr[i2] = a2;
        int i3 = i2 + 1;
        _m_methods = fmVarArr;
        C1634Xv.m11725a((Object[]) NPCType._m_methods, (Object[]) _m_methods);
        ___iScriptClass = aUO.m18566a(AgentType.class, C6690arm.class, _m_fields, _m_methods);
    }

    /* renamed from: T */
    public C6494any mo11T() {
        return ___iScriptClass;
    }

    /* renamed from: U */
    public C2491fm[] mo12U() {
        return _m_methods;
    }

    /* renamed from: W */
    public Object mo13W() {
        return new C6690arm(this);
    }

    /* renamed from: a */
    public Object mo14a(C0495Gr gr) {
        gr.getArgs();
        switch (gr.mo2417hq() - NPCType._m_methodCount) {
            case 0:
                return m32377vx();
            case 1:
                return m32376aT();
            default:
                return super.mo14a(gr);
        }
    }

    /* renamed from: a */
    public void mo15a(Collection collection, C0495Gr gr) {
        super.mo15a(collection, gr);
    }

    /* access modifiers changed from: protected */
    /* renamed from: aU */
    public <T extends aDJ> T mo745aU() {
        switch (bFf().mo6893i(f7766dN)) {
            case 0:
                return null;
            case 2:
                return (aDJ) bFf().mo5606d(new aCE(this, f7766dN, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f7766dN, new Object[0]));
                break;
        }
        return m32376aT();
    }

    /* renamed from: c */
    public C5663aRz[] mo25c() {
        return _m_fields;
    }

    /* renamed from: vy */
    public Agent mo19108vy() {
        switch (bFf().mo6893i(f7765PW)) {
            case 0:
                return null;
            case 2:
                return (Agent) bFf().mo5606d(new aCE(this, f7765PW, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f7765PW, new Object[0]));
                break;
        }
        return m32377vx();
    }

    /* renamed from: S */
    public void mo10S() {
        super.mo10S();
    }

    @C0064Am(aul = "da26a93bd14edcf6a44d9f95d329f406", aum = 0)
    /* renamed from: vx */
    private Agent m32377vx() {
        return (Agent) mo745aU();
    }

    @C0064Am(aul = "791f886a73a1076eebb9f81a37fc904f", aum = 0)
    /* renamed from: aT */
    private <T extends aDJ> T m32376aT() {
        T t = (Agent) bFf().mo6865M(Agent.class);
        t.mo12516c(this);
        return t;
    }
}
