package game.script.agent;

import game.network.message.externalizable.aCE;
import game.script.npc.NPC;
import game.script.npc.NPCType;
import logic.baa.*;
import logic.data.mbean.C5582aOw;
import logic.res.code.C5640aRc;
import logic.res.code.C5663aRz;
import logic.res.code.C6494any;
import logic.res.html.C2491fm;
import p001a.*;

import java.util.Collection;
import java.util.UUID;

@C2712iu(mo19786Bt = NPCType.class, version = "1.1.1")
@C5511aMd
@C6485anp
/* renamed from: a.abk  reason: case insensitive filesystem */
/* compiled from: a */
public class Agent extends NPC implements C0468GU, C1616Xf {
    public static final C2491fm _f_toString_0020_0028_0029Ljava_002flang_002fString_003b = null;
    public static final int _m_fieldCount = 0;
    public static final C5663aRz[] _m_fields = null;
    public static final int _m_methodCount = 0;
    public static final C2491fm[] _m_methods = null;
    /* renamed from: bL */
    public static final C5663aRz f4150bL = null;
    /* renamed from: bM */
    public static final C5663aRz f4151bM = null;
    /* renamed from: bN */
    public static final C2491fm f4152bN = null;
    /* renamed from: bO */
    public static final C2491fm f4153bO = null;
    /* renamed from: bP */
    public static final C2491fm f4154bP = null;
    /* renamed from: bQ */
    public static final C2491fm f4155bQ = null;

    public static C6494any ___iScriptClass = null;
    @C0064Am(aul = "c3d78a6e98a007893b927f8a82e65236", aum = 0)

    /* renamed from: bK */
    private static UUID f4149bK = null;
    @C0064Am(aul = "ac34396843caad8e61ddd6435a79766f", aum = 1)
    private static String handle = null;

    static {
        m20067V();
    }

    public Agent() {
        super((C5540aNg) null);
        super.mo10S();
    }

    public Agent(C5540aNg ang) {
        super(ang);
    }

    public Agent(AgentType goVar) {
        super((C5540aNg) null);
        super._m_script_init(goVar);
    }

    /* renamed from: V */
    static void m20067V() {
        _m_fieldCount = NPC._m_fieldCount + 2;
        _m_methodCount = NPC._m_methodCount + 5;
        int i = NPC._m_fieldCount;
        C5663aRz[] arzArr = new C5663aRz[(i + 2)];
        C5663aRz b = C5640aRc.m17844b(Agent.class, "c3d78a6e98a007893b927f8a82e65236", i);
        f4150bL = b;
        arzArr[i] = b;
        int i2 = i + 1;
        C5663aRz b2 = C5640aRc.m17844b(Agent.class, "ac34396843caad8e61ddd6435a79766f", i2);
        f4151bM = b2;
        arzArr[i2] = b2;
        int i3 = i2 + 1;
        _m_fields = arzArr;
        C1634Xv.m11725a((Object[]) NPC._m_fields, (Object[]) _m_fields);
        int i4 = NPC._m_methodCount;
        C2491fm[] fmVarArr = new C2491fm[(i4 + 5)];
        C2491fm a = C4105zY.m41624a(Agent.class, "fc54bc6f090cf5d415037a32277fd9ea", i4);
        f4152bN = a;
        fmVarArr[i4] = a;
        int i5 = i4 + 1;
        C2491fm a2 = C4105zY.m41624a(Agent.class, "9b3a3279988ade21b65dcb2ab881f054", i5);
        f4153bO = a2;
        fmVarArr[i5] = a2;
        int i6 = i5 + 1;
        C2491fm a3 = C4105zY.m41624a(Agent.class, "457bbd86fb76701c3fec9a2190bb538c", i6);
        f4154bP = a3;
        fmVarArr[i6] = a3;
        int i7 = i6 + 1;
        C2491fm a4 = C4105zY.m41624a(Agent.class, "c17c8d652ff00622680cc02e7a2c278e", i7);
        f4155bQ = a4;
        fmVarArr[i7] = a4;
        int i8 = i7 + 1;
        C2491fm a5 = C4105zY.m41624a(Agent.class, "01e258f6c4248b15a8a9694bc8a1de64", i8);
        _f_toString_0020_0028_0029Ljava_002flang_002fString_003b = a5;
        fmVarArr[i8] = a5;
        int i9 = i8 + 1;
        _m_methods = fmVarArr;
        C1634Xv.m11725a((Object[]) NPC._m_methods, (Object[]) _m_methods);
        ___iScriptClass = aUO.m18566a(Agent.class, C5582aOw.class, _m_fields, _m_methods);
    }

    /* renamed from: a */
    private void m20068a(String str) {
        bFf().mo5608dq().mo3197f(f4151bM, str);
    }

    /* renamed from: a */
    private void m20069a(UUID uuid) {
        bFf().mo5608dq().mo3197f(f4150bL, uuid);
    }

    /* renamed from: an */
    private UUID m20070an() {
        return (UUID) bFf().mo5608dq().mo3214p(f4150bL);
    }

    /* renamed from: ao */
    private String m20071ao() {
        return (String) bFf().mo5608dq().mo3214p(f4151bM);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Handle")
    @C0064Am(aul = "c17c8d652ff00622680cc02e7a2c278e", aum = 0)
    @C5566aOg
    /* renamed from: b */
    private void m20075b(String str) {
        throw new aWi(new aCE(this, f4155bQ, new Object[]{str}));
    }

    /* renamed from: c */
    private void m20077c(UUID uuid) {
        switch (bFf().mo6893i(f4153bO)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f4153bO, new Object[]{uuid}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f4153bO, new Object[]{uuid}));
                break;
        }
        m20076b(uuid);
    }

    /* renamed from: S */
    public void mo10S() {
        super.mo10S();
    }

    /* renamed from: T */
    public C6494any mo11T() {
        return ___iScriptClass;
    }

    /* renamed from: U */
    public C2491fm[] mo12U() {
        return _m_methods;
    }

    /* renamed from: W */
    public Object mo13W() {
        return new C5582aOw(this);
    }

    /* renamed from: a */
    public Object mo14a(C0495Gr gr) {
        Object[] args = gr.getArgs();
        switch (gr.mo2417hq() - NPC._m_methodCount) {
            case 0:
                return m20072ap();
            case 1:
                m20076b((UUID) args[0]);
                return null;
            case 2:
                return m20073ar();
            case 3:
                m20075b((String) args[0]);
                return null;
            case 4:
                return m20074au();
            default:
                return super.mo14a(gr);
        }
    }

    /* renamed from: a */
    public void mo15a(Collection collection, C0495Gr gr) {
        super.mo15a(collection, gr);
    }

    /* renamed from: aq */
    public UUID mo18aq() {
        switch (bFf().mo6893i(f4152bN)) {
            case 0:
                return null;
            case 2:
                return (UUID) bFf().mo5606d(new aCE(this, f4152bN, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f4152bN, new Object[0]));
                break;
        }
        return m20072ap();
    }

    /* renamed from: c */
    public C5663aRz[] mo25c() {
        return _m_fields;
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Handle")
    public String getHandle() {
        switch (bFf().mo6893i(f4154bP)) {
            case 0:
                return null;
            case 2:
                return (String) bFf().mo5606d(new aCE(this, f4154bP, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f4154bP, new Object[0]));
                break;
        }
        return m20073ar();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Handle")
    @C5566aOg
    public void setHandle(String str) {
        switch (bFf().mo6893i(f4155bQ)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f4155bQ, new Object[]{str}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f4155bQ, new Object[]{str}));
                break;
        }
        m20075b(str);
    }

    public String toString() {
        switch (bFf().mo6893i(_f_toString_0020_0028_0029Ljava_002flang_002fString_003b)) {
            case 0:
                return null;
            case 2:
                return (String) bFf().mo5606d(new aCE(this, _f_toString_0020_0028_0029Ljava_002flang_002fString_003b, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, _f_toString_0020_0028_0029Ljava_002flang_002fString_003b, new Object[0]));
                break;
        }
        return m20074au();
    }

    @C0064Am(aul = "fc54bc6f090cf5d415037a32277fd9ea", aum = 0)
    /* renamed from: ap */
    private UUID m20072ap() {
        return m20070an();
    }

    @C0064Am(aul = "9b3a3279988ade21b65dcb2ab881f054", aum = 0)
    /* renamed from: b */
    private void m20076b(UUID uuid) {
        m20069a(uuid);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Handle")
    @C0064Am(aul = "457bbd86fb76701c3fec9a2190bb538c", aum = 0)
    /* renamed from: ar */
    private String m20073ar() {
        return m20071ao();
    }

    /* renamed from: c */
    public void mo12516c(AgentType goVar) {
        super.mo11678y(goVar);
        m20069a(UUID.randomUUID());
    }

    @C0064Am(aul = "01e258f6c4248b15a8a9694bc8a1de64", aum = 0)
    /* renamed from: au */
    private String m20074au() {
        return "Agent: [" + mo11654Fs().getHandle() + "]";
    }
}
