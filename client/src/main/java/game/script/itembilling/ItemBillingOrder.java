package game.script.itembilling;

import game.network.message.externalizable.aCE;
import game.script.item.ItemType;
import logic.baa.*;
import logic.data.mbean.C5222aBa;
import logic.res.code.C5640aRc;
import logic.res.code.C5663aRz;
import logic.res.code.C6494any;
import logic.res.html.C2491fm;
import p001a.*;

import java.util.Collection;
import java.util.UUID;

@C5511aMd
@C6485anp
/* renamed from: a.uN */
/* compiled from: a */
public class ItemBillingOrder extends aDJ implements C0468GU, C1616Xf {
    public static final C2491fm _f_toString_0020_0028_0029Ljava_002flang_002fString_003b = null;
    public static final int _m_fieldCount = 0;
    public static final C5663aRz[] _m_fields = null;
    public static final int _m_methodCount = 0;
    public static final C2491fm[] _m_methods = null;
    /* renamed from: bL */
    public static final C5663aRz f9349bL = null;
    /* renamed from: bM */
    public static final C5663aRz f9350bM = null;
    /* renamed from: bN */
    public static final C2491fm f9351bN = null;
    /* renamed from: bO */
    public static final C2491fm f9352bO = null;
    /* renamed from: bP */
    public static final C2491fm f9353bP = null;
    /* renamed from: bQ */
    public static final C2491fm f9354bQ = null;
    public static final C5663aRz bxc = null;
    public static final C5663aRz bxe = null;
    public static final C5663aRz bxg = null;
    public static final C2491fm bxh = null;
    public static final C2491fm bxi = null;
    public static final C2491fm bxj = null;
    public static final C2491fm bxk = null;
    public static final C2491fm bxl = null;
    public static final C2491fm bxm = null;
    /* renamed from: cC */
    public static final C2491fm f9355cC = null;
    /* renamed from: cD */
    public static final C2491fm f9356cD = null;
    /* renamed from: cx */
    public static final C5663aRz f9358cx = null;
    public static final long serialVersionUID = 0;
    public static C6494any ___iScriptClass;
    @C0064Am(aul = "f696ff9cd0b702392590aedd8ace133c", aum = 5)

    /* renamed from: bK */
    private static UUID f9348bK;
    @C0064Am(aul = "fb8bbcc32937e29fafabf2f02999bed1", aum = 0)
    private static long bxb;
    @C0064Am(aul = "32ae47a6344c53ae4cc28fe401f46a08", aum = 1)
    private static long bxd;
    @C0064Am(aul = "7c1809624f577bc1cbd86471d1dfe203", aum = 3)
    private static String bxf;
    @C0064Am(aul = "9452917ff271c5c8834aba36bd6e929b", aum = 2)

    /* renamed from: cw */
    private static ItemType f9357cw;
    @C0064Am(aul = "cc0fa24962c3469da19ab7371a4cdd8c", aum = 4)
    private static String handle;

    static {
        m39969V();
    }

    public ItemBillingOrder() {
        super((C5540aNg) null);
        super.mo10S();
    }

    public ItemBillingOrder(C5540aNg ang) {
        super(ang);
    }

    public ItemBillingOrder(ItemType jCVar) {
        super((C5540aNg) null);
        super._m_script_init(jCVar);
    }

    /* renamed from: V */
    static void m39969V() {
        serialVersionUID = (long) 1;
        _m_fieldCount = aDJ._m_fieldCount + 6;
        _m_methodCount = aDJ._m_methodCount + 13;
        int i = aDJ._m_fieldCount;
        C5663aRz[] arzArr = new C5663aRz[(i + 6)];
        C5663aRz b = C5640aRc.m17844b(ItemBillingOrder.class, "fb8bbcc32937e29fafabf2f02999bed1", i);
        bxc = b;
        arzArr[i] = b;
        int i2 = i + 1;
        C5663aRz b2 = C5640aRc.m17844b(ItemBillingOrder.class, "32ae47a6344c53ae4cc28fe401f46a08", i2);
        bxe = b2;
        arzArr[i2] = b2;
        int i3 = i2 + 1;
        C5663aRz b3 = C5640aRc.m17844b(ItemBillingOrder.class, "9452917ff271c5c8834aba36bd6e929b", i3);
        f9358cx = b3;
        arzArr[i3] = b3;
        int i4 = i3 + 1;
        C5663aRz b4 = C5640aRc.m17844b(ItemBillingOrder.class, "7c1809624f577bc1cbd86471d1dfe203", i4);
        bxg = b4;
        arzArr[i4] = b4;
        int i5 = i4 + 1;
        C5663aRz b5 = C5640aRc.m17844b(ItemBillingOrder.class, "cc0fa24962c3469da19ab7371a4cdd8c", i5);
        f9350bM = b5;
        arzArr[i5] = b5;
        int i6 = i5 + 1;
        C5663aRz b6 = C5640aRc.m17844b(ItemBillingOrder.class, "f696ff9cd0b702392590aedd8ace133c", i6);
        f9349bL = b6;
        arzArr[i6] = b6;
        int i7 = i6 + 1;
        _m_fields = arzArr;
        C1634Xv.m11725a((Object[]) aDJ._m_fields, (Object[]) _m_fields);
        int i8 = aDJ._m_methodCount;
        C2491fm[] fmVarArr = new C2491fm[(i8 + 13)];
        C2491fm a = C4105zY.m41624a(ItemBillingOrder.class, "4b663eae19e04d902737095f2dea2b23", i8);
        f9351bN = a;
        fmVarArr[i8] = a;
        int i9 = i8 + 1;
        C2491fm a2 = C4105zY.m41624a(ItemBillingOrder.class, "8496afa582ba42941c6556f6711ad3b8", i9);
        f9352bO = a2;
        fmVarArr[i9] = a2;
        int i10 = i9 + 1;
        C2491fm a3 = C4105zY.m41624a(ItemBillingOrder.class, "89b6e09bde3d64122c979f4528f4e9f8", i10);
        f9353bP = a3;
        fmVarArr[i10] = a3;
        int i11 = i10 + 1;
        C2491fm a4 = C4105zY.m41624a(ItemBillingOrder.class, "5d9ef7da3aa613fbc50f5710009f5483", i11);
        f9354bQ = a4;
        fmVarArr[i11] = a4;
        int i12 = i11 + 1;
        C2491fm a5 = C4105zY.m41624a(ItemBillingOrder.class, "faf459f0e15021b805f22694b9f5c873", i12);
        bxh = a5;
        fmVarArr[i12] = a5;
        int i13 = i12 + 1;
        C2491fm a6 = C4105zY.m41624a(ItemBillingOrder.class, "a9b2039796878b259859af19063966ef", i13);
        bxi = a6;
        fmVarArr[i13] = a6;
        int i14 = i13 + 1;
        C2491fm a7 = C4105zY.m41624a(ItemBillingOrder.class, "cf2dfdffdeddf615ee9d7b1d4c9b0369", i14);
        bxj = a7;
        fmVarArr[i14] = a7;
        int i15 = i14 + 1;
        C2491fm a8 = C4105zY.m41624a(ItemBillingOrder.class, "6f9827a243d4467010b6f04d8cd54065", i15);
        bxk = a8;
        fmVarArr[i15] = a8;
        int i16 = i15 + 1;
        C2491fm a9 = C4105zY.m41624a(ItemBillingOrder.class, "68262bd83f7c8ab01a3cec8793a7b6c2", i16);
        bxl = a9;
        fmVarArr[i16] = a9;
        int i17 = i16 + 1;
        C2491fm a10 = C4105zY.m41624a(ItemBillingOrder.class, "7620505661ceecc32ad95b0a539786fc", i17);
        bxm = a10;
        fmVarArr[i17] = a10;
        int i18 = i17 + 1;
        C2491fm a11 = C4105zY.m41624a(ItemBillingOrder.class, "d0258f9a6051b224ac5ed73512891de9", i18);
        f9355cC = a11;
        fmVarArr[i18] = a11;
        int i19 = i18 + 1;
        C2491fm a12 = C4105zY.m41624a(ItemBillingOrder.class, "f0fad0ac1f3a1dcd3b4239e2de93673f", i19);
        f9356cD = a12;
        fmVarArr[i19] = a12;
        int i20 = i19 + 1;
        C2491fm a13 = C4105zY.m41624a(ItemBillingOrder.class, "263a3b323f7dba84a623657f6bed4042", i20);
        _f_toString_0020_0028_0029Ljava_002flang_002fString_003b = a13;
        fmVarArr[i20] = a13;
        int i21 = i20 + 1;
        _m_methods = fmVarArr;
        C1634Xv.m11725a((Object[]) aDJ._m_methods, (Object[]) _m_methods);
        ___iScriptClass = aUO.m18566a(ItemBillingOrder.class, C5222aBa.class, _m_fields, _m_methods);
    }

    /* renamed from: a */
    private void m39970a(ItemType jCVar) {
        bFf().mo5608dq().mo3197f(f9358cx, jCVar);
    }

    /* renamed from: a */
    private void m39971a(String str) {
        bFf().mo5608dq().mo3197f(f9350bM, str);
    }

    /* renamed from: a */
    private void m39972a(UUID uuid) {
        bFf().mo5608dq().mo3197f(f9349bL, uuid);
    }

    private long ajO() {
        return bFf().mo5608dq().mo3213o(bxc);
    }

    private long ajP() {
        return bFf().mo5608dq().mo3213o(bxe);
    }

    private String ajQ() {
        return (String) bFf().mo5608dq().mo3214p(bxg);
    }

    /* renamed from: an */
    private UUID m39973an() {
        return (UUID) bFf().mo5608dq().mo3214p(f9349bL);
    }

    /* renamed from: ao */
    private String m39974ao() {
        return (String) bFf().mo5608dq().mo3214p(f9350bM);
    }

    /* renamed from: av */
    private ItemType m39978av() {
        return (ItemType) bFf().mo5608dq().mo3214p(f9358cx);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "ItemType")
    @C0064Am(aul = "f0fad0ac1f3a1dcd3b4239e2de93673f", aum = 0)
    @C5566aOg
    /* renamed from: b */
    private void m39980b(ItemType jCVar) {
        throw new aWi(new aCE(this, f9356cD, new Object[]{jCVar}));
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Handle")
    @C0064Am(aul = "5d9ef7da3aa613fbc50f5710009f5483", aum = 0)
    @C5566aOg
    /* renamed from: b */
    private void m39981b(String str) {
        throw new aWi(new aCE(this, f9354bQ, new Object[]{str}));
    }

    /* renamed from: c */
    private void m39983c(UUID uuid) {
        switch (bFf().mo6893i(f9352bO)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f9352bO, new Object[]{uuid}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f9352bO, new Object[]{uuid}));
                break;
        }
        m39982b(uuid);
    }

    /* renamed from: cl */
    private void m39984cl(String str) {
        bFf().mo5608dq().mo3197f(bxg, str);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Promotional Announcement")
    @C0064Am(aul = "a9b2039796878b259859af19063966ef", aum = 0)
    @C5566aOg
    /* renamed from: cm */
    private void m39985cm(String str) {
        throw new aWi(new aCE(this, bxi, new Object[]{str}));
    }

    /* renamed from: df */
    private void m39986df(long j) {
        bFf().mo5608dq().mo3184b(bxc, j);
    }

    /* renamed from: dg */
    private void m39987dg(long j) {
        bFf().mo5608dq().mo3184b(bxe, j);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Hoplons Price")
    @C0064Am(aul = "6f9827a243d4467010b6f04d8cd54065", aum = 0)
    @C5566aOg
    /* renamed from: dh */
    private void m39988dh(long j) {
        throw new aWi(new aCE(this, bxk, new Object[]{new Long(j)}));
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Item Durability")
    @C0064Am(aul = "7620505661ceecc32ad95b0a539786fc", aum = 0)
    @C5566aOg
    /* renamed from: dj */
    private void m39989dj(long j) {
        throw new aWi(new aCE(this, bxm, new Object[]{new Long(j)}));
    }

    /* renamed from: S */
    public void mo10S() {
        super.mo10S();
    }

    /* renamed from: T */
    public C6494any mo11T() {
        return ___iScriptClass;
    }

    /* renamed from: U */
    public C2491fm[] mo12U() {
        return _m_methods;
    }

    /* renamed from: W */
    public Object mo13W() {
        return new C5222aBa(this);
    }

    /* renamed from: a */
    public Object mo14a(C0495Gr gr) {
        Object[] args = gr.getArgs();
        switch (gr.mo2417hq() - aDJ._m_methodCount) {
            case 0:
                return m39975ap();
            case 1:
                m39982b((UUID) args[0]);
                return null;
            case 2:
                return m39976ar();
            case 3:
                m39981b((String) args[0]);
                return null;
            case 4:
                return ajR();
            case 5:
                m39985cm((String) args[0]);
                return null;
            case 6:
                return new Long(ajT());
            case 7:
                m39988dh(((Long) args[0]).longValue());
                return null;
            case 8:
                return new Long(ajV());
            case 9:
                m39989dj(((Long) args[0]).longValue());
                return null;
            case 10:
                return m39979ay();
            case 11:
                m39980b((ItemType) args[0]);
                return null;
            case 12:
                return m39977au();
            default:
                return super.mo14a(gr);
        }
    }

    /* renamed from: a */
    public void mo15a(Collection collection, C0495Gr gr) {
        super.mo15a(collection, gr);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Promotional Announcement")
    public String ajS() {
        switch (bFf().mo6893i(bxh)) {
            case 0:
                return null;
            case 2:
                return (String) bFf().mo5606d(new aCE(this, bxh, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, bxh, new Object[0]));
                break;
        }
        return ajR();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Hoplons Price")
    public long ajU() {
        switch (bFf().mo6893i(bxj)) {
            case 0:
                return 0;
            case 2:
                return ((Long) bFf().mo5606d(new aCE(this, bxj, new Object[0]))).longValue();
            case 3:
                bFf().mo5606d(new aCE(this, bxj, new Object[0]));
                break;
        }
        return ajT();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Item Durability")
    public long ajW() {
        switch (bFf().mo6893i(bxl)) {
            case 0:
                return 0;
            case 2:
                return ((Long) bFf().mo5606d(new aCE(this, bxl, new Object[0]))).longValue();
            case 3:
                bFf().mo5606d(new aCE(this, bxl, new Object[0]));
                break;
        }
        return ajV();
    }

    /* renamed from: aq */
    public UUID mo18aq() {
        switch (bFf().mo6893i(f9351bN)) {
            case 0:
                return null;
            case 2:
                return (UUID) bFf().mo5606d(new aCE(this, f9351bN, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f9351bN, new Object[0]));
                break;
        }
        return m39975ap();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "ItemType")
    /* renamed from: az */
    public ItemType mo22367az() {
        switch (bFf().mo6893i(f9355cC)) {
            case 0:
                return null;
            case 2:
                return (ItemType) bFf().mo5606d(new aCE(this, f9355cC, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f9355cC, new Object[0]));
                break;
        }
        return m39979ay();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "ItemType")
    @C5566aOg
    /* renamed from: c */
    public void mo22368c(ItemType jCVar) {
        switch (bFf().mo6893i(f9356cD)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f9356cD, new Object[]{jCVar}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f9356cD, new Object[]{jCVar}));
                break;
        }
        m39980b(jCVar);
    }

    /* renamed from: c */
    public C5663aRz[] mo25c() {
        return _m_fields;
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Promotional Announcement")
    @C5566aOg
    /* renamed from: cn */
    public void mo22369cn(String str) {
        switch (bFf().mo6893i(bxi)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, bxi, new Object[]{str}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, bxi, new Object[]{str}));
                break;
        }
        m39985cm(str);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Hoplons Price")
    @C5566aOg
    /* renamed from: di */
    public void mo22370di(long j) {
        switch (bFf().mo6893i(bxk)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, bxk, new Object[]{new Long(j)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, bxk, new Object[]{new Long(j)}));
                break;
        }
        m39988dh(j);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Item Durability")
    @C5566aOg
    /* renamed from: dk */
    public void mo22371dk(long j) {
        switch (bFf().mo6893i(bxm)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, bxm, new Object[]{new Long(j)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, bxm, new Object[]{new Long(j)}));
                break;
        }
        m39989dj(j);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Handle")
    public String getHandle() {
        switch (bFf().mo6893i(f9353bP)) {
            case 0:
                return null;
            case 2:
                return (String) bFf().mo5606d(new aCE(this, f9353bP, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f9353bP, new Object[0]));
                break;
        }
        return m39976ar();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Handle")
    @C5566aOg
    public void setHandle(String str) {
        switch (bFf().mo6893i(f9354bQ)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f9354bQ, new Object[]{str}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f9354bQ, new Object[]{str}));
                break;
        }
        m39981b(str);
    }

    public String toString() {
        switch (bFf().mo6893i(_f_toString_0020_0028_0029Ljava_002flang_002fString_003b)) {
            case 0:
                return null;
            case 2:
                return (String) bFf().mo5606d(new aCE(this, _f_toString_0020_0028_0029Ljava_002flang_002fString_003b, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, _f_toString_0020_0028_0029Ljava_002flang_002fString_003b, new Object[0]));
                break;
        }
        return m39977au();
    }

    @C0064Am(aul = "4b663eae19e04d902737095f2dea2b23", aum = 0)
    /* renamed from: ap */
    private UUID m39975ap() {
        return m39973an();
    }

    @C0064Am(aul = "8496afa582ba42941c6556f6711ad3b8", aum = 0)
    /* renamed from: b */
    private void m39982b(UUID uuid) {
        m39972a(uuid);
    }

    /* renamed from: n */
    public void mo22372n(ItemType jCVar) {
        super.mo10S();
        m39970a(jCVar);
        m39972a(UUID.randomUUID());
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Handle")
    @C0064Am(aul = "89b6e09bde3d64122c979f4528f4e9f8", aum = 0)
    /* renamed from: ar */
    private String m39976ar() {
        return m39974ao();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Promotional Announcement")
    @C0064Am(aul = "faf459f0e15021b805f22694b9f5c873", aum = 0)
    private String ajR() {
        return ajQ();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Hoplons Price")
    @C0064Am(aul = "cf2dfdffdeddf615ee9d7b1d4c9b0369", aum = 0)
    private long ajT() {
        return ajO();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Item Durability")
    @C0064Am(aul = "68262bd83f7c8ab01a3cec8793a7b6c2", aum = 0)
    private long ajV() {
        return ajP();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "ItemType")
    @C0064Am(aul = "d0258f9a6051b224ac5ed73512891de9", aum = 0)
    /* renamed from: ay */
    private ItemType m39979ay() {
        return m39978av();
    }

    @C0064Am(aul = "263a3b323f7dba84a623657f6bed4042", aum = 0)
    /* renamed from: au */
    private String m39977au() {
        if (mo22367az() == null) {
            return "NULL";
        }
        return mo22367az().mo19891ke().get();
    }
}
