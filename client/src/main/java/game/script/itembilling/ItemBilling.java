package game.script.itembilling;

import game.network.message.externalizable.aCE;
import game.network.message.serializable.C2686iZ;
import game.script.TaikodomObject;
import game.script.item.BulkItemType;
import game.script.item.ItemLocation;
import game.script.item.ItemType;
import game.script.player.Player;
import logic.baa.*;
import logic.data.mbean.C5460aKe;
import logic.res.code.C5640aRc;
import logic.res.code.C5663aRz;
import logic.res.code.C6494any;
import logic.res.html.C2491fm;
import p001a.*;

import java.util.Collection;
import java.util.Collections;
import java.util.Set;
import java.util.UUID;

@C5511aMd
@C6485anp
/* renamed from: a.ac */
/* compiled from: a */
public class ItemBilling extends TaikodomObject implements C0468GU, C1616Xf {
    public static final C2491fm _f_onResurrect_0020_0028_0029V = null;
    public static final int _m_fieldCount = 0;
    public static final C5663aRz[] _m_fields = null;
    public static final int _m_methodCount = 0;
    public static final C2491fm[] _m_methods = null;
    /* renamed from: bL */
    public static final C5663aRz f4166bL = null;
    /* renamed from: bN */
    public static final C2491fm f4167bN = null;
    /* renamed from: bO */
    public static final C2491fm f4168bO = null;
    /* renamed from: bP */
    public static final C2491fm f4169bP = null;
    /* renamed from: gA */
    public static final C5663aRz f4170gA = null;
    /* renamed from: gB */
    public static final C2491fm f4171gB = null;
    /* renamed from: gC */
    public static final C2491fm f4172gC = null;
    /* renamed from: gD */
    public static final C2491fm f4173gD = null;
    /* renamed from: gE */
    public static final C2491fm f4174gE = null;
    /* renamed from: gF */
    public static final C2491fm f4175gF = null;
    /* renamed from: gG */
    public static final C2491fm f4176gG = null;
    /* renamed from: gH */
    public static final C2491fm f4177gH = null;
    /* renamed from: gI */
    public static final C2491fm f4178gI = null;
    /* renamed from: gJ */
    public static final C2491fm f4179gJ = null;
    /* renamed from: gK */
    public static final C2491fm f4180gK = null;
    /* renamed from: gL */
    public static final C2491fm f4181gL = null;
    /* renamed from: gM */
    public static final C2491fm f4182gM = null;
    /* renamed from: gN */
    public static final C2491fm f4183gN = null;
    /* renamed from: gO */
    public static final C2491fm f4184gO = null;
    /* renamed from: gP */
    public static final C2491fm f4185gP = null;
    /* renamed from: gQ */
    public static final C2491fm f4186gQ = null;
    /* renamed from: gx */
    public static final String f4187gx = "http://taikodom.com.br/game_billing";
    /* renamed from: gz */
    public static final C5663aRz f4189gz = null;
    public static final long serialVersionUID = 0;
    public static C6494any ___iScriptClass = null;
    @C0064Am(aul = "b73655b5afb673913d708b445d23e247", aum = 1)

    /* renamed from: bK */
    private static UUID f4165bK = null;
    @C0064Am(aul = "229b86048beda61b6d33048fdd15d13b", aum = 0)

    /* renamed from: gy */
    private static C2686iZ<ItemBillingOrder> f4188gy;
    @C0064Am(aul = "ac9467a31ab485c42fa47f0eb6d978cb", aum = 2)
    private static String url;

    static {
        m20142V();
    }

    public ItemBilling() {
        super((C5540aNg) null);
        super.mo10S();
    }

    public ItemBilling(C5540aNg ang) {
        super(ang);
    }

    /* renamed from: V */
    static void m20142V() {
        serialVersionUID = (long) 1;
        _m_fieldCount = TaikodomObject._m_fieldCount + 3;
        _m_methodCount = TaikodomObject._m_methodCount + 20;
        int i = TaikodomObject._m_fieldCount;
        C5663aRz[] arzArr = new C5663aRz[(i + 3)];
        C5663aRz b = C5640aRc.m17844b(ItemBilling.class, "229b86048beda61b6d33048fdd15d13b", i);
        f4189gz = b;
        arzArr[i] = b;
        int i2 = i + 1;
        C5663aRz b2 = C5640aRc.m17844b(ItemBilling.class, "b73655b5afb673913d708b445d23e247", i2);
        f4166bL = b2;
        arzArr[i2] = b2;
        int i3 = i2 + 1;
        C5663aRz b3 = C5640aRc.m17844b(ItemBilling.class, "ac9467a31ab485c42fa47f0eb6d978cb", i3);
        f4170gA = b3;
        arzArr[i3] = b3;
        int i4 = i3 + 1;
        _m_fields = arzArr;
        C1634Xv.m11725a((Object[]) TaikodomObject._m_fields, (Object[]) _m_fields);
        int i5 = TaikodomObject._m_methodCount;
        C2491fm[] fmVarArr = new C2491fm[(i5 + 20)];
        C2491fm a = C4105zY.m41624a(ItemBilling.class, "7cbd954f0cec0995e6c508f04c4cf8a2", i5);
        f4167bN = a;
        fmVarArr[i5] = a;
        int i6 = i5 + 1;
        C2491fm a2 = C4105zY.m41624a(ItemBilling.class, "be71f88adf16c966d23cd32c8ee61ccb", i6);
        f4168bO = a2;
        fmVarArr[i6] = a2;
        int i7 = i6 + 1;
        C2491fm a3 = C4105zY.m41624a(ItemBilling.class, "a987f16dd6c91d7dfaaeada5b61cdce6", i7);
        f4169bP = a3;
        fmVarArr[i7] = a3;
        int i8 = i7 + 1;
        C2491fm a4 = C4105zY.m41624a(ItemBilling.class, "99320297b70d2cedec2988eee907a236", i8);
        _f_onResurrect_0020_0028_0029V = a4;
        fmVarArr[i8] = a4;
        int i9 = i8 + 1;
        C2491fm a5 = C4105zY.m41624a(ItemBilling.class, "336c80070807a77f8c6ea928abb785d2", i9);
        f4171gB = a5;
        fmVarArr[i9] = a5;
        int i10 = i9 + 1;
        C2491fm a6 = C4105zY.m41624a(ItemBilling.class, "135668e6bd1bfa6df8c0b3127edc4358", i10);
        f4172gC = a6;
        fmVarArr[i10] = a6;
        int i11 = i10 + 1;
        C2491fm a7 = C4105zY.m41624a(ItemBilling.class, "439e7347c47cf14e9db03707e76006fa", i11);
        f4173gD = a7;
        fmVarArr[i11] = a7;
        int i12 = i11 + 1;
        C2491fm a8 = C4105zY.m41624a(ItemBilling.class, "5a7fb11d6f8491a8296ae612133c7ea3", i12);
        f4174gE = a8;
        fmVarArr[i12] = a8;
        int i13 = i12 + 1;
        C2491fm a9 = C4105zY.m41624a(ItemBilling.class, "7adebe1ea170c224d6425c25121a3f0b", i13);
        f4175gF = a9;
        fmVarArr[i13] = a9;
        int i14 = i13 + 1;
        C2491fm a10 = C4105zY.m41624a(ItemBilling.class, "829d8fd0cc8f98fa5923a3301c248da9", i14);
        f4176gG = a10;
        fmVarArr[i14] = a10;
        int i15 = i14 + 1;
        C2491fm a11 = C4105zY.m41624a(ItemBilling.class, "488f196f8e56381fd3e8e20618a8bda1", i15);
        f4177gH = a11;
        fmVarArr[i15] = a11;
        int i16 = i15 + 1;
        C2491fm a12 = C4105zY.m41624a(ItemBilling.class, "7f1e980b47398b50f7c0137ea8af3b09", i16);
        f4178gI = a12;
        fmVarArr[i16] = a12;
        int i17 = i16 + 1;
        C2491fm a13 = C4105zY.m41624a(ItemBilling.class, "f1da421578c4efc9e969fe3ffdc853f1", i17);
        f4179gJ = a13;
        fmVarArr[i17] = a13;
        int i18 = i17 + 1;
        C2491fm a14 = C4105zY.m41624a(ItemBilling.class, "77bce2b7654cc75b104ace685f8cc3c3", i18);
        f4180gK = a14;
        fmVarArr[i18] = a14;
        int i19 = i18 + 1;
        C2491fm a15 = C4105zY.m41624a(ItemBilling.class, "0840d6d3ca30acfc669b91491452171c", i19);
        f4181gL = a15;
        fmVarArr[i19] = a15;
        int i20 = i19 + 1;
        C2491fm a16 = C4105zY.m41624a(ItemBilling.class, "e6a1eccb755daa903347c77ad30dbbea", i20);
        f4182gM = a16;
        fmVarArr[i20] = a16;
        int i21 = i20 + 1;
        C2491fm a17 = C4105zY.m41624a(ItemBilling.class, "08e138394d208d05cc5e51169e4b518d", i21);
        f4183gN = a17;
        fmVarArr[i21] = a17;
        int i22 = i21 + 1;
        C2491fm a18 = C4105zY.m41624a(ItemBilling.class, "863a31feecafba995dd21895694c1d1b", i22);
        f4184gO = a18;
        fmVarArr[i22] = a18;
        int i23 = i22 + 1;
        C2491fm a19 = C4105zY.m41624a(ItemBilling.class, "90939fff7c231a3b5817cc8f1148db9b", i23);
        f4185gP = a19;
        fmVarArr[i23] = a19;
        int i24 = i23 + 1;
        C2491fm a20 = C4105zY.m41624a(ItemBilling.class, "978779a386184dba3c759a2bb052ed15", i24);
        f4186gQ = a20;
        fmVarArr[i24] = a20;
        int i25 = i24 + 1;
        _m_methods = fmVarArr;
        C1634Xv.m11725a((Object[]) TaikodomObject._m_methods, (Object[]) _m_methods);
        ___iScriptClass = aUO.m18566a(ItemBilling.class, C5460aKe.class, _m_fields, _m_methods);
    }

    @C0064Am(aul = "f1da421578c4efc9e969fe3ffdc853f1", aum = 0)
    @C5566aOg
    @C2499fr
    /* renamed from: a */
    private void m20143a(Player aku, ItemBillingOrder uNVar) {
        throw new aWi(new aCE(this, f4179gJ, new Object[]{aku, uNVar}));
    }

    @C0064Am(aul = "08e138394d208d05cc5e51169e4b518d", aum = 0)
    @C5566aOg
    /* renamed from: a */
    private void m20144a(BulkItemType amh, int i, ItemLocation aag) {
        throw new aWi(new aCE(this, f4183gN, new Object[]{amh, new Integer(i), aag}));
    }

    @C3248pc(aYR = C3248pc.C3250b.ADDER, displayName = "Item Billing Orders")
    @C0064Am(aul = "135668e6bd1bfa6df8c0b3127edc4358", aum = 0)
    @C5566aOg
    /* renamed from: a */
    private void m20145a(ItemBillingOrder uNVar) {
        throw new aWi(new aCE(this, f4172gC, new Object[]{uNVar}));
    }

    /* renamed from: a */
    private void m20146a(UUID uuid) {
        bFf().mo5608dq().mo3197f(f4166bL, uuid);
    }

    /* renamed from: an */
    private UUID m20148an() {
        return (UUID) bFf().mo5608dq().mo3214p(f4166bL);
    }

    @C5566aOg
    /* renamed from: b */
    private void m20151b(BulkItemType amh, int i, ItemLocation aag) {
        switch (bFf().mo6893i(f4183gN)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f4183gN, new Object[]{amh, new Integer(i), aag}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f4183gN, new Object[]{amh, new Integer(i), aag}));
                break;
        }
        m20144a(amh, i, aag);
    }

    /* renamed from: b */
    private void m20152b(C2686iZ iZVar) {
        bFf().mo5608dq().mo3197f(f4189gz, iZVar);
    }

    @C0064Am(aul = "77bce2b7654cc75b104ace685f8cc3c3", aum = 0)
    @C5566aOg
    /* renamed from: c */
    private void m20154c(Player aku, ItemBillingOrder uNVar) {
        throw new aWi(new aCE(this, f4180gK, new Object[]{aku, uNVar}));
    }

    @C3248pc(aYR = C3248pc.C3250b.REMOVER, displayName = "Item Billing Orders")
    @C0064Am(aul = "5a7fb11d6f8491a8296ae612133c7ea3", aum = 0)
    @C5566aOg
    /* renamed from: c */
    private void m20155c(ItemBillingOrder uNVar) {
        throw new aWi(new aCE(this, f4174gE, new Object[]{uNVar}));
    }

    /* renamed from: c */
    private void m20156c(UUID uuid) {
        switch (bFf().mo6893i(f4168bO)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f4168bO, new Object[]{uuid}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f4168bO, new Object[]{uuid}));
                break;
        }
        m20153b(uuid);
    }

    /* renamed from: cS */
    private C2686iZ m20157cS() {
        return (C2686iZ) bFf().mo5608dq().mo3214p(f4189gz);
    }

    /* renamed from: cT */
    private String m20158cT() {
        return (String) bFf().mo5608dq().mo3214p(f4170gA);
    }

    @C5566aOg
    /* renamed from: d */
    private void m20162d(Player aku, ItemBillingOrder uNVar) {
        switch (bFf().mo6893i(f4180gK)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f4180gK, new Object[]{aku, uNVar}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f4180gK, new Object[]{aku, uNVar}));
                break;
        }
        m20154c(aku, uNVar);
    }

    @C3248pc(aYR = C3248pc.C3250b.ALTERNATE_ADDER, displayName = "Item Billing Orders")
    @C0064Am(aul = "439e7347c47cf14e9db03707e76006fa", aum = 0)
    @C5566aOg
    /* renamed from: d */
    private void m20163d(ItemType jCVar) {
        throw new aWi(new aCE(this, f4173gD, new Object[]{jCVar}));
    }

    @C0064Am(aul = "7f1e980b47398b50f7c0137ea8af3b09", aum = 0)
    @C5566aOg
    @C2499fr
    /* renamed from: da */
    private boolean m20164da() {
        throw new aWi(new aCE(this, f4178gI, new Object[0]));
    }

    @C0064Am(aul = "978779a386184dba3c759a2bb052ed15", aum = 0)
    @C5566aOg
    /* renamed from: dd */
    private void m20165dd() {
        throw new aWi(new aCE(this, f4186gQ, new Object[0]));
    }

    @C0064Am(aul = "0840d6d3ca30acfc669b91491452171c", aum = 0)
    @C5566aOg
    /* renamed from: e */
    private void m20166e(Player aku, ItemBillingOrder uNVar) {
        throw new aWi(new aCE(this, f4181gL, new Object[]{aku, uNVar}));
    }

    @C5566aOg
    /* renamed from: f */
    private void m20167f(Player aku, ItemBillingOrder uNVar) {
        switch (bFf().mo6893i(f4181gL)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f4181gL, new Object[]{aku, uNVar}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f4181gL, new Object[]{aku, uNVar}));
                break;
        }
        m20166e(aku, uNVar);
    }

    @C0064Am(aul = "e6a1eccb755daa903347c77ad30dbbea", aum = 0)
    @C5566aOg
    /* renamed from: g */
    private void m20168g(Player aku, ItemBillingOrder uNVar) {
        throw new aWi(new aCE(this, f4182gM, new Object[]{aku, uNVar}));
    }

    @C5566aOg
    /* renamed from: h */
    private void m20169h(Player aku, ItemBillingOrder uNVar) {
        switch (bFf().mo6893i(f4182gM)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f4182gM, new Object[]{aku, uNVar}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f4182gM, new Object[]{aku, uNVar}));
                break;
        }
        m20168g(aku, uNVar);
    }

    /* renamed from: h */
    private void m20170h(String str) {
        bFf().mo5608dq().mo3197f(f4170gA, str);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Billing URL")
    @C0064Am(aul = "829d8fd0cc8f98fa5923a3301c248da9", aum = 0)
    @C5566aOg
    /* renamed from: i */
    private void m20171i(String str) {
        throw new aWi(new aCE(this, f4176gG, new Object[]{str}));
    }

    @C0064Am(aul = "863a31feecafba995dd21895694c1d1b", aum = 0)
    @C5566aOg
    /* renamed from: i */
    private boolean m20172i(Player aku, ItemBillingOrder uNVar) {
        throw new aWi(new aCE(this, f4184gO, new Object[]{aku, uNVar}));
    }

    @C5566aOg
    /* renamed from: j */
    private boolean m20173j(Player aku, ItemBillingOrder uNVar) {
        switch (bFf().mo6893i(f4184gO)) {
            case 0:
                return false;
            case 2:
                return ((Boolean) bFf().mo5606d(new aCE(this, f4184gO, new Object[]{aku, uNVar}))).booleanValue();
            case 3:
                bFf().mo5606d(new aCE(this, f4184gO, new Object[]{aku, uNVar}));
                break;
        }
        return m20172i(aku, uNVar);
    }

    @C0064Am(aul = "90939fff7c231a3b5817cc8f1148db9b", aum = 0)
    @C5566aOg
    /* renamed from: k */
    private ItemLocation m20174k(Player aku, ItemBillingOrder uNVar) {
        throw new aWi(new aCE(this, f4185gP, new Object[]{aku, uNVar}));
    }

    @C5566aOg
    /* renamed from: l */
    private ItemLocation m20175l(Player aku, ItemBillingOrder uNVar) {
        switch (bFf().mo6893i(f4185gP)) {
            case 0:
                return null;
            case 2:
                return (ItemLocation) bFf().mo5606d(new aCE(this, f4185gP, new Object[]{aku, uNVar}));
            case 3:
                bFf().mo5606d(new aCE(this, f4185gP, new Object[]{aku, uNVar}));
                break;
        }
        return m20174k(aku, uNVar);
    }

    /* renamed from: T */
    public C6494any mo11T() {
        return ___iScriptClass;
    }

    /* renamed from: U */
    public C2491fm[] mo12U() {
        return _m_methods;
    }

    /* renamed from: W */
    public Object mo13W() {
        return new C5460aKe(this);
    }

    /* renamed from: a */
    public Object mo14a(C0495Gr gr) {
        Object[] args = gr.getArgs();
        switch (gr.mo2417hq() - TaikodomObject._m_methodCount) {
            case 0:
                return m20149ap();
            case 1:
                m20153b((UUID) args[0]);
                return null;
            case 2:
                return m20150ar();
            case 3:
                m20147aG();
                return null;
            case 4:
                return m20159cU();
            case 5:
                m20145a((ItemBillingOrder) args[0]);
                return null;
            case 6:
                m20163d((ItemType) args[0]);
                return null;
            case 7:
                m20155c((ItemBillingOrder) args[0]);
                return null;
            case 8:
                return m20160cW();
            case 9:
                m20171i((String) args[0]);
                return null;
            case 10:
                return m20161cY();
            case 11:
                return new Boolean(m20164da());
            case 12:
                m20143a((Player) args[0], (ItemBillingOrder) args[1]);
                return null;
            case 13:
                m20154c((Player) args[0], (ItemBillingOrder) args[1]);
                return null;
            case 14:
                m20166e((Player) args[0], (ItemBillingOrder) args[1]);
                return null;
            case 15:
                m20168g((Player) args[0], (ItemBillingOrder) args[1]);
                return null;
            case 16:
                m20144a((BulkItemType) args[0], ((Integer) args[1]).intValue(), (ItemLocation) args[2]);
                return null;
            case 17:
                return new Boolean(m20172i((Player) args[0], (ItemBillingOrder) args[1]));
            case 18:
                return m20174k((Player) args[0], (ItemBillingOrder) args[1]);
            case 19:
                m20165dd();
                return null;
            default:
                return super.mo14a(gr);
        }
    }

    /* renamed from: a */
    public void mo15a(Collection collection, C0495Gr gr) {
        super.mo15a(collection, gr);
    }

    /* renamed from: aH */
    public void mo70aH() {
        switch (bFf().mo6893i(_f_onResurrect_0020_0028_0029V)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, _f_onResurrect_0020_0028_0029V, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, _f_onResurrect_0020_0028_0029V, new Object[0]));
                break;
        }
        m20147aG();
    }

    /* renamed from: aq */
    public UUID mo18aq() {
        switch (bFf().mo6893i(f4167bN)) {
            case 0:
                return null;
            case 2:
                return (UUID) bFf().mo5606d(new aCE(this, f4167bN, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f4167bN, new Object[0]));
                break;
        }
        return m20149ap();
    }

    @C5566aOg
    @C2499fr
    /* renamed from: b */
    public void mo12550b(Player aku, ItemBillingOrder uNVar) {
        switch (bFf().mo6893i(f4179gJ)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f4179gJ, new Object[]{aku, uNVar}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f4179gJ, new Object[]{aku, uNVar}));
                break;
        }
        m20143a(aku, uNVar);
    }

    @C3248pc(aYR = C3248pc.C3250b.ADDER, displayName = "Item Billing Orders")
    @C5566aOg
    /* renamed from: b */
    public void mo12551b(ItemBillingOrder uNVar) {
        switch (bFf().mo6893i(f4172gC)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f4172gC, new Object[]{uNVar}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f4172gC, new Object[]{uNVar}));
                break;
        }
        m20145a(uNVar);
    }

    /* renamed from: c */
    public C5663aRz[] mo25c() {
        return _m_fields;
    }

    @C3248pc(aYR = C3248pc.C3250b.ITERATOR, displayName = "Item Billing Orders")
    /* renamed from: cV */
    public Set<ItemBillingOrder> mo12552cV() {
        switch (bFf().mo6893i(f4171gB)) {
            case 0:
                return null;
            case 2:
                return (Set) bFf().mo5606d(new aCE(this, f4171gB, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f4171gB, new Object[0]));
                break;
        }
        return m20159cU();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Billing URL")
    /* renamed from: cX */
    public String mo12553cX() {
        switch (bFf().mo6893i(f4175gF)) {
            case 0:
                return null;
            case 2:
                return (String) bFf().mo5606d(new aCE(this, f4175gF, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f4175gF, new Object[0]));
                break;
        }
        return m20160cW();
    }

    /* renamed from: cZ */
    public Set<ItemBillingOrder> mo12554cZ() {
        switch (bFf().mo6893i(f4177gH)) {
            case 0:
                return null;
            case 2:
                return (Set) bFf().mo5606d(new aCE(this, f4177gH, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f4177gH, new Object[0]));
                break;
        }
        return m20161cY();
    }

    @C3248pc(aYR = C3248pc.C3250b.REMOVER, displayName = "Item Billing Orders")
    @C5566aOg
    /* renamed from: d */
    public void mo12555d(ItemBillingOrder uNVar) {
        switch (bFf().mo6893i(f4174gE)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f4174gE, new Object[]{uNVar}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f4174gE, new Object[]{uNVar}));
                break;
        }
        m20155c(uNVar);
    }

    @C5566aOg
    @C2499fr
    /* renamed from: db */
    public boolean mo12556db() {
        switch (bFf().mo6893i(f4178gI)) {
            case 0:
                return false;
            case 2:
                return ((Boolean) bFf().mo5606d(new aCE(this, f4178gI, new Object[0]))).booleanValue();
            case 3:
                bFf().mo5606d(new aCE(this, f4178gI, new Object[0]));
                break;
        }
        return m20164da();
    }

    @C5566aOg
    /* renamed from: de */
    public void mo12557de() {
        switch (bFf().mo6893i(f4186gQ)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f4186gQ, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f4186gQ, new Object[0]));
                break;
        }
        m20165dd();
    }

    @C3248pc(aYR = C3248pc.C3250b.ALTERNATE_ADDER, displayName = "Item Billing Orders")
    @C5566aOg
    /* renamed from: e */
    public void mo12558e(ItemType jCVar) {
        switch (bFf().mo6893i(f4173gD)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f4173gD, new Object[]{jCVar}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f4173gD, new Object[]{jCVar}));
                break;
        }
        m20163d(jCVar);
    }

    public String getHandle() {
        switch (bFf().mo6893i(f4169bP)) {
            case 0:
                return null;
            case 2:
                return (String) bFf().mo5606d(new aCE(this, f4169bP, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f4169bP, new Object[0]));
                break;
        }
        return m20150ar();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Billing URL")
    @C5566aOg
    /* renamed from: j */
    public void mo12559j(String str) {
        switch (bFf().mo6893i(f4176gG)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f4176gG, new Object[]{str}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f4176gG, new Object[]{str}));
                break;
        }
        m20171i(str);
    }

    /* renamed from: S */
    public void mo10S() {
        super.mo10S();
    }

    @C0064Am(aul = "7cbd954f0cec0995e6c508f04c4cf8a2", aum = 0)
    /* renamed from: ap */
    private UUID m20149ap() {
        return m20148an();
    }

    @C0064Am(aul = "be71f88adf16c966d23cd32c8ee61ccb", aum = 0)
    /* renamed from: b */
    private void m20153b(UUID uuid) {
        m20146a(uuid);
    }

    @C0064Am(aul = "a987f16dd6c91d7dfaaeada5b61cdce6", aum = 0)
    /* renamed from: ar */
    private String m20150ar() {
        return "item_billing";
    }

    @C0064Am(aul = "99320297b70d2cedec2988eee907a236", aum = 0)
    /* renamed from: aG */
    private void m20147aG() {
        super.mo70aH();
    }

    @C3248pc(aYR = C3248pc.C3250b.ITERATOR, displayName = "Item Billing Orders")
    @C0064Am(aul = "336c80070807a77f8c6ea928abb785d2", aum = 0)
    /* renamed from: cU */
    private Set<ItemBillingOrder> m20159cU() {
        return Collections.unmodifiableSet(m20157cS());
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Billing URL")
    @C0064Am(aul = "7adebe1ea170c224d6425c25121a3f0b", aum = 0)
    /* renamed from: cW */
    private String m20160cW() {
        return m20158cT();
    }

    @C0064Am(aul = "488f196f8e56381fd3e8e20618a8bda1", aum = 0)
    /* renamed from: cY */
    private Set<ItemBillingOrder> m20161cY() {
        return m20157cS();
    }
}
