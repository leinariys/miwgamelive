package game.script;

import game.network.message.externalizable.aCE;
import game.network.message.serializable.C3438ra;
import logic.baa.C1616Xf;
import logic.baa.C1634Xv;
import logic.baa.C5511aMd;
import logic.baa.C6485anp;
import logic.data.mbean.C0816Ln;
import logic.res.code.C5640aRc;
import logic.res.code.C5663aRz;
import logic.res.code.C6494any;
import logic.res.html.C2491fm;
import p001a.*;

import java.util.Collection;

@C5511aMd
@C6485anp
/* renamed from: a.Dv */
/* compiled from: a */
public class NPCParty extends TaikodomObject implements C1616Xf {
    public static final int _m_fieldCount = 0;
    public static final C5663aRz[] _m_fields = null;
    public static final int _m_methodCount = 0;
    public static final C2491fm[] _m_methods = null;
    public static final C2491fm aJH = null;
    public static final C2491fm cHA = null;
    public static final C2491fm cHB = null;
    public static final C2491fm cHC = null;
    public static final C5663aRz cHv = null;
    public static final C5663aRz cHx = null;
    public static final C2491fm cHy = null;
    public static final C2491fm cHz = null;
    public static final long serialVersionUID = 0;
    public static C6494any ___iScriptClass;
    @C0064Am(aul = "e423f0d6af3c32ab958c22a6f49904a9", aum = 0)
    private static C3438ra<java.lang.Character> cHu;
    @C0064Am(aul = "64ac6fb00b7927a4cadaf67fd38b9ecb", aum = 1)
    private static java.lang.Character cHw;

    static {
        m2741V();
    }

    public NPCParty() {
        super((C5540aNg) null);
        super.mo10S();
    }

    public NPCParty(C5540aNg ang) {
        super(ang);
    }

    /* renamed from: V */
    static void m2741V() {
        serialVersionUID = (long) 1;
        _m_fieldCount = TaikodomObject._m_fieldCount + 2;
        _m_methodCount = TaikodomObject._m_methodCount + 6;
        int i = TaikodomObject._m_fieldCount;
        C5663aRz[] arzArr = new C5663aRz[(i + 2)];
        C5663aRz b = C5640aRc.m17844b(NPCParty.class, "e423f0d6af3c32ab958c22a6f49904a9", i);
        cHv = b;
        arzArr[i] = b;
        int i2 = i + 1;
        C5663aRz b2 = C5640aRc.m17844b(NPCParty.class, "64ac6fb00b7927a4cadaf67fd38b9ecb", i2);
        cHx = b2;
        arzArr[i2] = b2;
        int i3 = i2 + 1;
        _m_fields = arzArr;
        C1634Xv.m11725a((Object[]) TaikodomObject._m_fields, (Object[]) _m_fields);
        int i4 = TaikodomObject._m_methodCount;
        C2491fm[] fmVarArr = new C2491fm[(i4 + 6)];
        C2491fm a = C4105zY.m41624a(NPCParty.class, "07d3c54d1b303c67e0d9589d2bff49a3", i4);
        cHy = a;
        fmVarArr[i4] = a;
        int i5 = i4 + 1;
        C2491fm a2 = C4105zY.m41624a(NPCParty.class, "bfaeb2573f2cc632e406d6b284fd5278", i5);
        aJH = a2;
        fmVarArr[i5] = a2;
        int i6 = i5 + 1;
        C2491fm a3 = C4105zY.m41624a(NPCParty.class, "2b3ceb35f3c8b91d1ca54cb3d9628b40", i6);
        cHz = a3;
        fmVarArr[i6] = a3;
        int i7 = i6 + 1;
        C2491fm a4 = C4105zY.m41624a(NPCParty.class, "deec41484ead7bed42ef2b59ec393443", i7);
        cHA = a4;
        fmVarArr[i7] = a4;
        int i8 = i7 + 1;
        C2491fm a5 = C4105zY.m41624a(NPCParty.class, "9d9046ccbad6d7349947bb34efffbabf", i8);
        cHB = a5;
        fmVarArr[i8] = a5;
        int i9 = i8 + 1;
        C2491fm a6 = C4105zY.m41624a(NPCParty.class, "62643a4351fa46ca6b7e0865abab2200", i9);
        cHC = a6;
        fmVarArr[i9] = a6;
        int i10 = i9 + 1;
        _m_methods = fmVarArr;
        C1634Xv.m11725a((Object[]) TaikodomObject._m_methods, (Object[]) _m_methods);
        ___iScriptClass = aUO.m18566a(NPCParty.class, C0816Ln.class, _m_fields, _m_methods);
    }

    private C3438ra aGo() {
        return (C3438ra) bFf().mo5608dq().mo3214p(cHv);
    }

    private java.lang.Character aGp() {
        return (java.lang.Character) bFf().mo5608dq().mo3214p(cHx);
    }

    /* renamed from: ai */
    private void m2742ai(C3438ra raVar) {
        bFf().mo5608dq().mo3197f(cHv, raVar);
    }

    /* renamed from: v */
    private void m2743v(java.lang.Character acx) {
        bFf().mo5608dq().mo3197f(cHx, acx);
    }

    @C0064Am(aul = "2b3ceb35f3c8b91d1ca54cb3d9628b40", aum = 0)
    @C5566aOg
    /* renamed from: w */
    private void m2744w(java.lang.Character acx) {
        throw new aWi(new aCE(this, cHz, new Object[]{acx}));
    }

    @C0064Am(aul = "deec41484ead7bed42ef2b59ec393443", aum = 0)
    @C5566aOg
    /* renamed from: y */
    private void m2745y(java.lang.Character acx) {
        throw new aWi(new aCE(this, cHA, new Object[]{acx}));
    }

    /* renamed from: B */
    public void mo1779B(java.lang.Character acx) {
        switch (bFf().mo6893i(cHC)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, cHC, new Object[]{acx}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, cHC, new Object[]{acx}));
                break;
        }
        m2739A(acx);
    }

    /* renamed from: T */
    public C6494any mo11T() {
        return ___iScriptClass;
    }

    /* renamed from: U */
    public C2491fm[] mo12U() {
        return _m_methods;
    }

    /* renamed from: W */
    public Object mo13W() {
        return new C0816Ln(this);
    }

    /* renamed from: a */
    public Object mo14a(C0495Gr gr) {
        Object[] args = gr.getArgs();
        switch (gr.mo2417hq() - TaikodomObject._m_methodCount) {
            case 0:
                return aGq();
            case 1:
                return new Integer(m2740QD());
            case 2:
                m2744w((java.lang.Character) args[0]);
                return null;
            case 3:
                m2745y((java.lang.Character) args[0]);
                return null;
            case 4:
                return aGs();
            case 5:
                m2739A((java.lang.Character) args[0]);
                return null;
            default:
                return super.mo14a(gr);
        }
    }

    /* renamed from: a */
    public void mo15a(Collection collection, C0495Gr gr) {
        super.mo15a(collection, gr);
    }

    public C3438ra<java.lang.Character> aGr() {
        switch (bFf().mo6893i(cHy)) {
            case 0:
                return null;
            case 2:
                return (C3438ra) bFf().mo5606d(new aCE(this, cHy, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, cHy, new Object[0]));
                break;
        }
        return aGq();
    }

    public java.lang.Character aGt() {
        switch (bFf().mo6893i(cHB)) {
            case 0:
                return null;
            case 2:
                return (java.lang.Character) bFf().mo5606d(new aCE(this, cHB, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, cHB, new Object[0]));
                break;
        }
        return aGs();
    }

    /* renamed from: c */
    public C5663aRz[] mo25c() {
        return _m_fields;
    }

    public int size() {
        switch (bFf().mo6893i(aJH)) {
            case 0:
                return 0;
            case 2:
                return ((Integer) bFf().mo5606d(new aCE(this, aJH, new Object[0]))).intValue();
            case 3:
                bFf().mo5606d(new aCE(this, aJH, new Object[0]));
                break;
        }
        return m2740QD();
    }

    @C5566aOg
    /* renamed from: x */
    public void mo1783x(java.lang.Character acx) {
        switch (bFf().mo6893i(cHz)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, cHz, new Object[]{acx}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, cHz, new Object[]{acx}));
                break;
        }
        m2744w(acx);
    }

    @C5566aOg
    /* renamed from: z */
    public void mo1784z(java.lang.Character acx) {
        switch (bFf().mo6893i(cHA)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, cHA, new Object[]{acx}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, cHA, new Object[]{acx}));
                break;
        }
        m2745y(acx);
    }

    /* renamed from: S */
    public void mo10S() {
        super.mo10S();
    }

    @C0064Am(aul = "07d3c54d1b303c67e0d9589d2bff49a3", aum = 0)
    private C3438ra<java.lang.Character> aGq() {
        return aGo();
    }

    @C0064Am(aul = "bfaeb2573f2cc632e406d6b284fd5278", aum = 0)
    /* renamed from: QD */
    private int m2740QD() {
        return aGo().size();
    }

    @C0064Am(aul = "9d9046ccbad6d7349947bb34efffbabf", aum = 0)
    private java.lang.Character aGs() {
        return aGp();
    }

    @C0064Am(aul = "62643a4351fa46ca6b7e0865abab2200", aum = 0)
    /* renamed from: A */
    private void m2739A(java.lang.Character acx) {
        m2743v(acx);
    }
}
