package game.script.offload;

import game.network.message.externalizable.aCE;
import game.network.message.serializable.C1556Wo;
import game.network.message.serializable.C3438ra;
import game.script.TaikodomObject;
import game.script.login.UserConnection;
import game.script.simulation.Space;
import logic.baa.C1616Xf;
import logic.baa.C1634Xv;
import logic.baa.C5511aMd;
import logic.baa.C6485anp;
import logic.bbb.aDR;
import logic.data.link.C1680Yk;
import logic.data.mbean.C0475Ga;
import logic.res.code.C5640aRc;
import logic.res.code.C5663aRz;
import logic.res.code.C6494any;
import logic.res.html.C2491fm;
import p001a.*;

import java.util.Collection;

@C6485anp
@C2499fr(mo18855qf = {"fc1ebac279a7b78f263ed3408f629757"})
@C1020Ot(drz = C1020Ot.C1021a.NOT_PERSISTED)
@C5511aMd
/* renamed from: a.cx */
/* compiled from: a */
public class OffloadNode extends TaikodomObject implements C1616Xf {
    public static final int _m_fieldCount = 0;
    public static final C5663aRz[] _m_fields = null;
    public static final int _m_methodCount = 0;
    public static final C2491fm[] _m_methods = null;
    public static final long serialVersionUID = 0;
    /* renamed from: uA */
    public static final C2491fm f6392uA = null;
    /* renamed from: uB */
    public static final C2491fm f6393uB = null;
    /* renamed from: uC */
    public static final C2491fm f6394uC = null;
    /* renamed from: uD */
    public static final C2491fm f6395uD = null;
    /* renamed from: uE */
    public static final C2491fm f6396uE = null;
    /* renamed from: uF */
    public static final C2491fm f6397uF = null;
    /* renamed from: ui */
    public static final C5663aRz f6399ui = null;
    /* renamed from: uk */
    public static final C5663aRz f6401uk = null;
    /* renamed from: um */
    public static final C5663aRz f6403um = null;
    /* renamed from: uo */
    public static final C5663aRz f6405uo = null;
    /* renamed from: uq */
    public static final C5663aRz f6407uq = null;
    /* renamed from: us */
    public static final C5663aRz f6409us = null;
    /* renamed from: ut */
    public static final C2491fm f6410ut = null;
    /* renamed from: uu */
    public static final C2491fm f6411uu = null;
    /* renamed from: uv */
    public static final C2491fm f6412uv = null;
    /* renamed from: uw */
    public static final C2491fm f6413uw = null;
    /* renamed from: ux */
    public static final C2491fm f6414ux = null;
    /* renamed from: uy */
    public static final C2491fm f6415uy = null;
    /* renamed from: uz */
    public static final C2491fm f6416uz = null;
    public static C6494any ___iScriptClass;
    @C0064Am(aul = "410a253188f8acb3761a8e3cf7d24e88", aum = 0)
    @C5566aOg

    /* renamed from: uh */
    private static aDR f6398uh;
    @C0064Am(aul = "6b750be5dcb7440a1debf767396cc8e3", aum = 1)

    /* renamed from: uj */
    private static C3438ra<Space> f6400uj;
    @C0064Am(aul = "74dea7e0f074826e40a2439f6a35f244", aum = 2)

    /* renamed from: ul */
    private static C3438ra<Space> f6402ul;
    @C5566aOg
    @C3122oB(mo20937Uq = C3122oB.C3123a.NOT_REPLICATED)
    @C0064Am(aul = "4824759737845798775ea6a570268733", aum = 3)
    @C1020Ot(drz = C1020Ot.C1021a.NOT_PERSISTED)

    /* renamed from: un */
    private static long f6404un;
    @C5566aOg
    @C3122oB(mo20937Uq = C3122oB.C3123a.NOT_REPLICATED)
    @C0064Am(aul = "3837a7f4c698bf3685c43c487c2f833e", aum = 4)
    @C1020Ot(drz = C1020Ot.C1021a.NOT_PERSISTED)

    /* renamed from: up */
    private static C1556Wo<Space, Long> f6406up;
    @C0064Am(aul = "df512dce9e105f1d157a9e41ac4e1245", aum = 5)

    /* renamed from: ur */
    private static String f6408ur;

    static {
        m28648V();
    }

    public OffloadNode() {
        super((C5540aNg) null);
        super.mo10S();
    }

    public OffloadNode(aDR adr) {
        super((C5540aNg) null);
        super._m_script_init(adr);
    }

    public OffloadNode(C5540aNg ang) {
        super(ang);
    }

    /* renamed from: V */
    static void m28648V() {
        serialVersionUID = (long) 1;
        _m_fieldCount = TaikodomObject._m_fieldCount + 6;
        _m_methodCount = TaikodomObject._m_methodCount + 13;
        int i = TaikodomObject._m_fieldCount;
        C5663aRz[] arzArr = new C5663aRz[(i + 6)];
        C5663aRz b = C5640aRc.m17844b(OffloadNode.class, "410a253188f8acb3761a8e3cf7d24e88", i);
        f6399ui = b;
        arzArr[i] = b;
        int i2 = i + 1;
        C5663aRz b2 = C5640aRc.m17844b(OffloadNode.class, "6b750be5dcb7440a1debf767396cc8e3", i2);
        f6401uk = b2;
        arzArr[i2] = b2;
        int i3 = i2 + 1;
        C5663aRz b3 = C5640aRc.m17844b(OffloadNode.class, "74dea7e0f074826e40a2439f6a35f244", i3);
        f6403um = b3;
        arzArr[i3] = b3;
        int i4 = i3 + 1;
        C5663aRz b4 = C5640aRc.m17844b(OffloadNode.class, "4824759737845798775ea6a570268733", i4);
        f6405uo = b4;
        arzArr[i4] = b4;
        int i5 = i4 + 1;
        C5663aRz b5 = C5640aRc.m17844b(OffloadNode.class, "3837a7f4c698bf3685c43c487c2f833e", i5);
        f6407uq = b5;
        arzArr[i5] = b5;
        int i6 = i5 + 1;
        C5663aRz b6 = C5640aRc.m17844b(OffloadNode.class, "df512dce9e105f1d157a9e41ac4e1245", i6);
        f6409us = b6;
        arzArr[i6] = b6;
        int i7 = i6 + 1;
        _m_fields = arzArr;
        C1634Xv.m11725a((Object[]) TaikodomObject._m_fields, (Object[]) _m_fields);
        int i8 = TaikodomObject._m_methodCount;
        C2491fm[] fmVarArr = new C2491fm[(i8 + 13)];
        C2491fm a = C4105zY.m41624a(OffloadNode.class, C1680Yk.eLp, i8);
        f6410ut = a;
        fmVarArr[i8] = a;
        int i9 = i8 + 1;
        C2491fm a2 = C4105zY.m41624a(OffloadNode.class, "a82c0d9b6a11f21b7439b2bb5a746bbf", i9);
        f6411uu = a2;
        fmVarArr[i9] = a2;
        int i10 = i9 + 1;
        C2491fm a3 = C4105zY.m41624a(OffloadNode.class, "2d1aee707374de4fd74f0bd5e1a2ac60", i10);
        f6412uv = a3;
        fmVarArr[i10] = a3;
        int i11 = i10 + 1;
        C2491fm a4 = C4105zY.m41624a(OffloadNode.class, "bb10fd12c5ef1e055891f8495449bf64", i11);
        f6413uw = a4;
        fmVarArr[i11] = a4;
        int i12 = i11 + 1;
        C2491fm a5 = C4105zY.m41624a(OffloadNode.class, "76bb8b97f6a4baaa147c40bcb4bd5f22", i12);
        f6414ux = a5;
        fmVarArr[i12] = a5;
        int i13 = i12 + 1;
        C2491fm a6 = C4105zY.m41624a(OffloadNode.class, "de54c2c2f44115de7221411f61905158", i13);
        f6415uy = a6;
        fmVarArr[i13] = a6;
        int i14 = i13 + 1;
        C2491fm a7 = C4105zY.m41624a(OffloadNode.class, "18f030edb3227f379f1c7078ffd46ff3", i14);
        f6416uz = a7;
        fmVarArr[i14] = a7;
        int i15 = i14 + 1;
        C2491fm a8 = C4105zY.m41624a(OffloadNode.class, "19ea2c11bccfeb9974b2efb8f9d434de", i15);
        f6392uA = a8;
        fmVarArr[i15] = a8;
        int i16 = i15 + 1;
        C2491fm a9 = C4105zY.m41624a(OffloadNode.class, "550c694a111af57a611f76961f135711", i16);
        f6393uB = a9;
        fmVarArr[i16] = a9;
        int i17 = i16 + 1;
        C2491fm a10 = C4105zY.m41624a(OffloadNode.class, "cc47ba9ade50ccaf10d0a2ec177bfa6b", i17);
        f6394uC = a10;
        fmVarArr[i17] = a10;
        int i18 = i17 + 1;
        C2491fm a11 = C4105zY.m41624a(OffloadNode.class, "71b0b85a9d3f4caa2fa9ae9ee587289b", i18);
        f6395uD = a11;
        fmVarArr[i18] = a11;
        int i19 = i18 + 1;
        C2491fm a12 = C4105zY.m41624a(OffloadNode.class, "4fa86b2c88adfb02131aba5adf34feed", i19);
        f6396uE = a12;
        fmVarArr[i19] = a12;
        int i20 = i19 + 1;
        C2491fm a13 = C4105zY.m41624a(OffloadNode.class, "0c2dd11de72f2946ef0f2f8dfd4a842d", i20);
        f6397uF = a13;
        fmVarArr[i20] = a13;
        int i21 = i20 + 1;
        _m_methods = fmVarArr;
        C1634Xv.m11725a((Object[]) TaikodomObject._m_methods, (Object[]) _m_methods);
        ___iScriptClass = aUO.m18566a(OffloadNode.class, C0475Ga.class, _m_fields, _m_methods);
    }

    /* renamed from: C */
    private void m28646C(String str) {
        bFf().mo5608dq().mo3197f(f6409us, str);
    }

    @C0064Am(aul = "4fa86b2c88adfb02131aba5adf34feed", aum = 0)
    @C5566aOg
    @C2499fr
    /* renamed from: D */
    private void m28647D(String str) {
        throw new aWi(new aCE(this, f6396uE, new Object[]{str}));
    }

    @C4034yP
    @C5566aOg
    @C0064Am(aul = "19ea2c11bccfeb9974b2efb8f9d434de", aum = 0)
    @C2499fr
    /* renamed from: a */
    private void m28649a(Space ea, long j) {
        throw new aWi(new aCE(this, f6392uA, new Object[]{ea, new Long(j)}));
    }

    @C0064Am(aul = "bb10fd12c5ef1e055891f8495449bf64", aum = 0)
    @C5566aOg
    /* renamed from: a */
    private void m28650a(Collection<Space> collection) {
        throw new aWi(new aCE(this, f6413uw, new Object[]{collection}));
    }

    /* renamed from: b */
    private void m28652b(C1556Wo wo) {
        bFf().mo5608dq().mo3197f(f6407uq, wo);
    }

    @C4034yP
    @ClientOnly(mo22945pZ = false)
    @C2499fr
    /* renamed from: d */
    private void m28654d(Collection<Space> collection) {
        switch (bFf().mo6893i(f6414ux)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f6414ux, new Object[]{collection}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f6414ux, new Object[]{collection}));
                break;
        }
        m28653c(collection);
    }

    @C0064Am(aul = "de54c2c2f44115de7221411f61905158", aum = 0)
    @C5566aOg
    /* renamed from: e */
    private void m28655e(Collection<Space> collection) {
        throw new aWi(new aCE(this, f6415uy, new Object[]{collection}));
    }

    /* renamed from: f */
    private void m28656f(aDR adr) {
        bFf().mo5608dq().mo3197f(f6399ui, adr);
    }

    @C4034yP
    @ClientOnly(mo22945pZ = false)
    @C2499fr
    /* renamed from: h */
    private void m28658h(Collection<Space> collection) {
        switch (bFf().mo6893i(f6416uz)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f6416uz, new Object[]{collection}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f6416uz, new Object[]{collection}));
                break;
        }
        m28657g(collection);
    }

    /* renamed from: hJ */
    private aDR m28659hJ() {
        return (aDR) bFf().mo5608dq().mo3214p(f6399ui);
    }

    /* renamed from: hK */
    private C3438ra m28660hK() {
        return (C3438ra) bFf().mo5608dq().mo3214p(f6401uk);
    }

    /* renamed from: hL */
    private C3438ra m28661hL() {
        return (C3438ra) bFf().mo5608dq().mo3214p(f6403um);
    }

    /* renamed from: hM */
    private long m28662hM() {
        return bFf().mo5608dq().mo3213o(f6405uo);
    }

    /* renamed from: hN */
    private C1556Wo m28663hN() {
        return (C1556Wo) bFf().mo5608dq().mo3214p(f6407uq);
    }

    /* renamed from: hO */
    private String m28664hO() {
        return (String) bFf().mo5608dq().mo3214p(f6409us);
    }

    @C0064Am(aul = "cc47ba9ade50ccaf10d0a2ec177bfa6b", aum = 0)
    @C5566aOg
    /* renamed from: hT */
    private long m28667hT() {
        throw new aWi(new aCE(this, f6394uC, new Object[0]));
    }

    @C0064Am(aul = "71b0b85a9d3f4caa2fa9ae9ee587289b", aum = 0)
    @C5566aOg
    /* renamed from: hV */
    private C1556Wo<Space, Long> m28668hV() {
        throw new aWi(new aCE(this, f6395uD, new Object[0]));
    }

    /* renamed from: n */
    private void m28670n(C3438ra raVar) {
        bFf().mo5608dq().mo3197f(f6401uk, raVar);
    }

    /* renamed from: o */
    private void m28671o(long j) {
        bFf().mo5608dq().mo3184b(f6405uo, j);
    }

    /* renamed from: o */
    private void m28672o(C3438ra raVar) {
        bFf().mo5608dq().mo3197f(f6403um, raVar);
    }

    @C4034yP
    @C5566aOg
    @C0064Am(aul = "550c694a111af57a611f76961f135711", aum = 0)
    @C2499fr
    /* renamed from: p */
    private void m28673p(long j) {
        throw new aWi(new aCE(this, f6393uB, new Object[]{new Long(j)}));
    }

    @C5566aOg
    @C2499fr
    /* renamed from: E */
    public void mo17693E(String str) {
        switch (bFf().mo6893i(f6396uE)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f6396uE, new Object[]{str}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f6396uE, new Object[]{str}));
                break;
        }
        m28647D(str);
    }

    /* renamed from: S */
    public void mo10S() {
        super.mo10S();
    }

    /* renamed from: T */
    public C6494any mo11T() {
        return ___iScriptClass;
    }

    /* renamed from: U */
    public C2491fm[] mo12U() {
        return _m_methods;
    }

    /* renamed from: W */
    public Object mo13W() {
        return new C0475Ga(this);
    }

    /* renamed from: a */
    public Object mo14a(C0495Gr gr) {
        Object[] args = gr.getArgs();
        switch (gr.mo2417hq() - TaikodomObject._m_methodCount) {
            case 0:
                return new Boolean(m28651a((C1722ZT<UserConnection>) (C1722ZT) args[0]));
            case 1:
                return m28665hP();
            case 2:
                return m28666hR();
            case 3:
                m28650a((Collection<Space>) (Collection) args[0]);
                return null;
            case 4:
                m28653c((Collection) args[0]);
                return null;
            case 5:
                m28655e((Collection) args[0]);
                return null;
            case 6:
                m28657g((Collection<Space>) (Collection) args[0]);
                return null;
            case 7:
                m28649a((Space) args[0], ((Long) args[1]).longValue());
                return null;
            case 8:
                m28673p(((Long) args[0]).longValue());
                return null;
            case 9:
                return new Long(m28667hT());
            case 10:
                return m28668hV();
            case 11:
                m28647D((String) args[0]);
                return null;
            case 12:
                return m28669hX();
            default:
                return super.mo14a(gr);
        }
    }

    /* renamed from: a */
    public void mo15a(Collection collection, C0495Gr gr) {
        super.mo15a(collection, gr);
    }

    @C4034yP
    @C5566aOg
    @C2499fr
    /* renamed from: b */
    public void mo17694b(Space ea, long j) {
        switch (bFf().mo6893i(f6392uA)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f6392uA, new Object[]{ea, new Long(j)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f6392uA, new Object[]{ea, new Long(j)}));
                break;
        }
        m28649a(ea, j);
    }

    @C5566aOg
    /* renamed from: b */
    public void mo17695b(Collection<Space> collection) {
        switch (bFf().mo6893i(f6413uw)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f6413uw, new Object[]{collection}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f6413uw, new Object[]{collection}));
                break;
        }
        m28650a(collection);
    }

    @C5472aKq
    /* renamed from: b */
    public boolean mo17696b(C1722ZT<UserConnection> zt) {
        switch (bFf().mo6893i(f6410ut)) {
            case 0:
                return false;
            case 2:
                return ((Boolean) bFf().mo5606d(new aCE(this, f6410ut, new Object[]{zt}))).booleanValue();
            case 3:
                bFf().mo5606d(new aCE(this, f6410ut, new Object[]{zt}));
                break;
        }
        return m28651a(zt);
    }

    /* renamed from: c */
    public C5663aRz[] mo25c() {
        return _m_fields;
    }

    @C5566aOg
    /* renamed from: f */
    public void mo17697f(Collection<Space> collection) {
        switch (bFf().mo6893i(f6415uy)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f6415uy, new Object[]{collection}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f6415uy, new Object[]{collection}));
                break;
        }
        m28655e(collection);
    }

    /* renamed from: hQ */
    public C3438ra<Space> mo17699hQ() {
        switch (bFf().mo6893i(f6411uu)) {
            case 0:
                return null;
            case 2:
                return (C3438ra) bFf().mo5606d(new aCE(this, f6411uu, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f6411uu, new Object[0]));
                break;
        }
        return m28665hP();
    }

    /* renamed from: hS */
    public C3438ra<Space> mo17700hS() {
        switch (bFf().mo6893i(f6412uv)) {
            case 0:
                return null;
            case 2:
                return (C3438ra) bFf().mo5606d(new aCE(this, f6412uv, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f6412uv, new Object[0]));
                break;
        }
        return m28666hR();
    }

    @C5566aOg
    /* renamed from: hU */
    public long mo17701hU() {
        switch (bFf().mo6893i(f6394uC)) {
            case 0:
                return 0;
            case 2:
                return ((Long) bFf().mo5606d(new aCE(this, f6394uC, new Object[0]))).longValue();
            case 3:
                bFf().mo5606d(new aCE(this, f6394uC, new Object[0]));
                break;
        }
        return m28667hT();
    }

    @C5566aOg
    /* renamed from: hW */
    public C1556Wo<Space, Long> mo17702hW() {
        switch (bFf().mo6893i(f6395uD)) {
            case 0:
                return null;
            case 2:
                return (C1556Wo) bFf().mo5606d(new aCE(this, f6395uD, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f6395uD, new Object[0]));
                break;
        }
        return m28668hV();
    }

    /* renamed from: hY */
    public String mo17703hY() {
        switch (bFf().mo6893i(f6397uF)) {
            case 0:
                return null;
            case 2:
                return (String) bFf().mo5606d(new aCE(this, f6397uF, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f6397uF, new Object[0]));
                break;
        }
        return m28669hX();
    }

    @C4034yP
    @C5566aOg
    @C2499fr
    /* renamed from: q */
    public void mo17704q(long j) {
        switch (bFf().mo6893i(f6393uB)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f6393uB, new Object[]{new Long(j)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f6393uB, new Object[]{new Long(j)}));
                break;
        }
        m28673p(j);
    }

    /* renamed from: g */
    public void mo17698g(aDR adr) {
        super.mo10S();
        m28656f(adr);
    }

    @C0064Am(aul = "fc1ebac279a7b78f263ed3408f629757", aum = 0)
    @C5472aKq
    /* renamed from: a */
    private boolean m28651a(C1722ZT<UserConnection> zt) {
        return m28659hJ() == null || zt.bFs() == m28659hJ();
    }

    @C0064Am(aul = "a82c0d9b6a11f21b7439b2bb5a746bbf", aum = 0)
    /* renamed from: hP */
    private C3438ra<Space> m28665hP() {
        return m28660hK();
    }

    @C0064Am(aul = "2d1aee707374de4fd74f0bd5e1a2ac60", aum = 0)
    /* renamed from: hR */
    private C3438ra<Space> m28666hR() {
        return m28661hL();
    }

    @C4034yP
    @ClientOnly(mo22945pZ = false)
    @C0064Am(aul = "76bb8b97f6a4baaa147c40bcb4bd5f22", aum = 0)
    @C2499fr
    /* renamed from: c */
    private void m28653c(Collection<Space> collection) {
        mo8359ma("Stopping spaces: " + collection);
        for (Space next : collection) {
            if (next.cKF()) {
                next.stop();
            }
        }
    }

    @C4034yP
    @ClientOnly(mo22945pZ = false)
    @C0064Am(aul = "18f030edb3227f379f1c7078ffd46ff3", aum = 0)
    @C2499fr
    /* renamed from: g */
    private void m28657g(Collection<Space> collection) {
        mo8359ma("Starting spaces: " + collection);
        for (Space next : collection) {
            if (!next.cKF()) {
                next.init();
            }
        }
    }

    @C0064Am(aul = "0c2dd11de72f2946ef0f2f8dfd4a842d", aum = 0)
    /* renamed from: hX */
    private String m28669hX() {
        return m28664hO();
    }
}
