package game.script.offload;

import game.engine.DataGameEvent;
import game.network.message.externalizable.aCE;
import game.network.message.serializable.C1556Wo;
import game.script.TaikodomObject;
import game.script.simulation.Space;
import logic.baa.C1616Xf;
import logic.baa.C1634Xv;
import logic.baa.C5511aMd;
import logic.baa.C6485anp;
import logic.bbb.aDR;
import logic.data.mbean.C3104nr;
import logic.res.code.C5640aRc;
import logic.res.code.C5663aRz;
import logic.res.code.C6494any;
import logic.res.html.C2491fm;
import p001a.*;

import java.io.File;
import java.io.IOException;
import java.lang.management.ManagementFactory;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;

@C3437rZ(aak = "", aal = "taikodom.game.script.OffloadUtils")
@C5511aMd
@C6485anp
/* renamed from: a.aQh  reason: case insensitive filesystem */
/* compiled from: a */
public class OffloadUtils extends TaikodomObject implements C1616Xf {
    public static final int _m_fieldCount = 0;
    public static final C5663aRz[] _m_fields = null;
    public static final int _m_methodCount = 0;
    public static final C2491fm[] _m_methods = null;
    public static final C5663aRz iEf = null;
    public static final C2491fm iEg = null;
    public static final C2491fm iEh = null;
    public static final C2491fm iEi = null;
    public static final C2491fm iEj = null;
    public static final C2491fm iEk = null;
    public static final C2491fm iEl = null;
    public static final C2491fm iEm = null;
    public static final C2491fm iEn = null;
    public static final long serialVersionUID = 0;
    public static C6494any ___iScriptClass;
    @C5566aOg
    @C3122oB(mo20937Uq = C3122oB.C3123a.NOT_REPLICATED)
    @C0064Am(aul = "04f629cabd5d11f1620e5be3db05fd4b", aum = 0)
    @C1020Ot(drz = C1020Ot.C1021a.NOT_PERSISTED)
    private static C1556Wo<aDR, OffloadNode> aIn;

    static {
        m17611V();
    }

    public OffloadUtils() {
        super((C5540aNg) null);
        super.mo10S();
    }

    public OffloadUtils(C5540aNg ang) {
        super(ang);
    }

    /* renamed from: V */
    static void m17611V() {
        serialVersionUID = (long) 1;
        _m_fieldCount = TaikodomObject._m_fieldCount + 1;
        _m_methodCount = TaikodomObject._m_methodCount + 8;
        int i = TaikodomObject._m_fieldCount;
        C5663aRz[] arzArr = new C5663aRz[(i + 1)];
        C5663aRz b = C5640aRc.m17844b(OffloadUtils.class, "04f629cabd5d11f1620e5be3db05fd4b", i);
        iEf = b;
        arzArr[i] = b;
        int i2 = i + 1;
        _m_fields = arzArr;
        C1634Xv.m11725a((Object[]) TaikodomObject._m_fields, (Object[]) _m_fields);
        int i3 = TaikodomObject._m_methodCount;
        C2491fm[] fmVarArr = new C2491fm[(i3 + 8)];
        C2491fm a = C4105zY.m41624a(OffloadUtils.class, "eb0fba79aab96aaa22b18629c97742ae", i3);
        iEg = a;
        fmVarArr[i3] = a;
        int i4 = i3 + 1;
        C2491fm a2 = C4105zY.m41624a(OffloadUtils.class, "c042e3fccf0b603ec0e685548dd2e720", i4);
        iEh = a2;
        fmVarArr[i4] = a2;
        int i5 = i4 + 1;
        C2491fm a3 = C4105zY.m41624a(OffloadUtils.class, "e96075425984cfa1e285782136c7cce7", i5);
        iEi = a3;
        fmVarArr[i5] = a3;
        int i6 = i5 + 1;
        C2491fm a4 = C4105zY.m41624a(OffloadUtils.class, "8afe5e71c94b2f3c5cb21813ce95db7c", i6);
        iEj = a4;
        fmVarArr[i6] = a4;
        int i7 = i6 + 1;
        C2491fm a5 = C4105zY.m41624a(OffloadUtils.class, "05d579437568c97d0ae13e88f1e8e82a", i7);
        iEk = a5;
        fmVarArr[i7] = a5;
        int i8 = i7 + 1;
        C2491fm a6 = C4105zY.m41624a(OffloadUtils.class, "e704b9873c4ef5e15b5c4b5e4921eeab", i8);
        iEl = a6;
        fmVarArr[i8] = a6;
        int i9 = i8 + 1;
        C2491fm a7 = C4105zY.m41624a(OffloadUtils.class, "e1a3c7124efd629dbb0949f7931c629a", i9);
        iEm = a7;
        fmVarArr[i9] = a7;
        int i10 = i9 + 1;
        C2491fm a8 = C4105zY.m41624a(OffloadUtils.class, "3e2ef6dd9e358c8a68c9b44d5c07dd23", i10);
        iEn = a8;
        fmVarArr[i10] = a8;
        int i11 = i10 + 1;
        _m_methods = fmVarArr;
        C1634Xv.m11725a((Object[]) TaikodomObject._m_methods, (Object[]) _m_methods);
        ___iScriptClass = aUO.m18566a(OffloadUtils.class, C3104nr.class, _m_fields, _m_methods);
    }

    /* renamed from: hY */
    public static String m17613hY() {
        System.out.println(System.getProperty("os.name"));
        if (System.getProperty("os.name").startsWith("Win")) {
            return dpA();
        }
        return dpz();
    }

    private static String dpz() {
        try {
            return new File("/proc/self").getCanonicalFile().getName();
        } catch (IOException e) {
            return "noPid";
        }
    }

    private static String dpA() {
        try {
            return ManagementFactory.getRuntimeMXBean().getName().split("@")[0];
        } catch (Exception e) {
            return "noPid";
        }
    }

    @C0064Am(aul = "e96075425984cfa1e285782136c7cce7", aum = 0)
    @C5566aOg
    /* renamed from: B */
    private void m17607B(aDR adr) {
        throw new aWi(new aCE(this, iEi, new Object[]{adr}));
    }

    @C5566aOg
    @C0064Am(aul = "e1a3c7124efd629dbb0949f7931c629a", aum = 0)
    @C2499fr
    /* renamed from: L */
    private OffloadNode m17608L(Map<String, String> map) {
        throw new aWi(new aCE(this, iEm, new Object[]{map}));
    }

    /* renamed from: M */
    private void m17610M(List<Space> list) {
        switch (bFf().mo6893i(iEj)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, iEj, new Object[]{list}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, iEj, new Object[]{list}));
                break;
        }
        m17609L(list);
    }

    /* renamed from: aq */
    private void m17612aq(C1556Wo wo) {
        bFf().mo5608dq().mo3197f(iEf, wo);
    }

    private C1556Wo dpq() {
        return (C1556Wo) bFf().mo5608dq().mo3214p(iEf);
    }

    @C0064Am(aul = "c042e3fccf0b603ec0e685548dd2e720", aum = 0)
    @C5566aOg
    private void dpr() {
        throw new aWi(new aCE(this, iEh, new Object[0]));
    }

    private ArrayList<OffloadNode> dpu() {
        switch (bFf().mo6893i(iEk)) {
            case 0:
                return null;
            case 2:
                return (ArrayList) bFf().mo5606d(new aCE(this, iEk, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, iEk, new Object[0]));
                break;
        }
        return dpt();
    }

    private DataGameEvent dpw() {
        switch (bFf().mo6893i(iEl)) {
            case 0:
                return null;
            case 2:
                return (DataGameEvent) bFf().mo5606d(new aCE(this, iEl, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, iEl, new Object[0]));
                break;
        }
        return dpv();
    }

    @C0064Am(aul = "3e2ef6dd9e358c8a68c9b44d5c07dd23", aum = 0)
    @C5566aOg
    private C1556Wo<aDR, OffloadNode> dpx() {
        throw new aWi(new aCE(this, iEn, new Object[0]));
    }

    @C0064Am(aul = "eb0fba79aab96aaa22b18629c97742ae", aum = 0)
    @C5566aOg
    /* renamed from: z */
    private void m17614z(aDR adr) {
        throw new aWi(new aCE(this, iEg, new Object[]{adr}));
    }

    @C5566aOg
    /* renamed from: A */
    public void mo11044A(aDR adr) {
        switch (bFf().mo6893i(iEg)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, iEg, new Object[]{adr}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, iEg, new Object[]{adr}));
                break;
        }
        m17614z(adr);
    }

    @C5566aOg
    /* renamed from: C */
    public void mo11045C(aDR adr) {
        switch (bFf().mo6893i(iEi)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, iEi, new Object[]{adr}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, iEi, new Object[]{adr}));
                break;
        }
        m17607B(adr);
    }

    @C5566aOg
    @C2499fr
    /* renamed from: M */
    public OffloadNode mo11046M(Map<String, String> map) {
        switch (bFf().mo6893i(iEm)) {
            case 0:
                return null;
            case 2:
                return (OffloadNode) bFf().mo5606d(new aCE(this, iEm, new Object[]{map}));
            case 3:
                bFf().mo5606d(new aCE(this, iEm, new Object[]{map}));
                break;
        }
        return m17608L(map);
    }

    /* renamed from: T */
    public C6494any mo11T() {
        return ___iScriptClass;
    }

    /* renamed from: U */
    public C2491fm[] mo12U() {
        return _m_methods;
    }

    /* renamed from: W */
    public Object mo13W() {
        return new C3104nr(this);
    }

    /* renamed from: a */
    public Object mo14a(C0495Gr gr) {
        Object[] args = gr.getArgs();
        switch (gr.mo2417hq() - TaikodomObject._m_methodCount) {
            case 0:
                m17614z((aDR) args[0]);
                return null;
            case 1:
                dpr();
                return null;
            case 2:
                m17607B((aDR) args[0]);
                return null;
            case 3:
                m17609L((List<Space>) (List) args[0]);
                return null;
            case 4:
                return dpt();
            case 5:
                return dpv();
            case 6:
                return m17608L((Map<String, String>) (Map) args[0]);
            case 7:
                return dpx();
            default:
                return super.mo14a(gr);
        }
    }

    /* renamed from: a */
    public void mo15a(Collection collection, C0495Gr gr) {
        super.mo15a(collection, gr);
    }

    /* renamed from: c */
    public C5663aRz[] mo25c() {
        return _m_fields;
    }

    @C5566aOg
    public void dps() {
        switch (bFf().mo6893i(iEh)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, iEh, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, iEh, new Object[0]));
                break;
        }
        dpr();
    }

    @C5566aOg
    public C1556Wo<aDR, OffloadNode> dpy() {
        switch (bFf().mo6893i(iEn)) {
            case 0:
                return null;
            case 2:
                return (C1556Wo) bFf().mo5606d(new aCE(this, iEn, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, iEn, new Object[0]));
                break;
        }
        return dpx();
    }

    /* renamed from: S */
    public void mo10S() {
        super.mo10S();
    }

    @C0064Am(aul = "8afe5e71c94b2f3c5cb21813ce95db7c", aum = 0)
    /* renamed from: L */
    private void m17609L(List<Space> list) {
        if (!dpq().isEmpty() && !list.isEmpty()) {
            int size = dpq().size() - dpu().size();
            if (size <= 0) {
                mo6317hy("All offloads are stuck to spaces.");
                return;
            }
            double size2 = ((double) list.size()) / ((double) size);
            double d = 0.0d;
            for (OffloadNode f : dpq().values()) {
                int floor = (int) Math.floor(size2 + d);
                d += size2 - ((double) floor);
                List<Space> subList = list.subList(0, floor);
                f.mo17697f((Collection<Space>) new ArrayList(subList));
                subList.clear();
            }
        }
    }

    @C0064Am(aul = "05d579437568c97d0ae13e88f1e8e82a", aum = 0)
    private ArrayList<OffloadNode> dpt() {
        ArrayList<OffloadNode> arrayList = new ArrayList<>();
        for (OffloadNode cxVar : dpq().values()) {
            if (cxVar.mo17700hS() != null && cxVar.mo17700hS().size() > 0) {
                arrayList.add(cxVar);
            }
        }
        return arrayList;
    }

    @C0064Am(aul = "e704b9873c4ef5e15b5c4b5e4921eeab", aum = 0)
    private DataGameEvent dpv() {
        return bFf().mo6866PM();
    }
}
