package game.script.simulation;

import com.hoplon.geometry.Vec3f;
import game.engine.DataGameEvent;
import game.geometry.Vec3d;
import game.network.channel.client.ClientConnect;
import game.network.message.externalizable.C3856vr;
import game.network.message.externalizable.aCE;
import game.network.message.serializable.C2686iZ;
import game.network.message.serializable.C3438ra;
import game.script.Actor;
import game.script.TaikodomObject;
import game.script.item.Shot;
import game.script.mines.Mine;
import game.script.missile.Missile;
import game.script.mission.MissionTrigger;
import game.script.ship.Ship;
import game.script.ship.Station;
import game.script.ship.categories.Fighter;
import game.script.space.Gate;
import game.script.space.Loot;
import gnu.trove.THashSet;
import logic.WrapRunnable;
import logic.baa.*;
import logic.bbb.aDR;
import logic.data.mbean.aGX;
import logic.res.code.C5640aRc;
import logic.res.code.C5663aRz;
import logic.res.code.C6494any;
import logic.res.html.C2491fm;
import logic.thred.C6615aqP;
import logic.thred.LogPrinter;
import org.mozilla1.classfile.C6192aiI;
import p001a.*;
import taikodom.addon.C6144ahM;
import taikodom.infra.script.I18NString;

import java.util.*;

@C5511aMd
@C6485anp
/* renamed from: a.Ea */
/* compiled from: a */
public class Space extends TaikodomObject implements C1616Xf, aOW, C6222aim {

    /* renamed from: Do */
    public static final C2491fm f495Do = null;

    /* renamed from: Lm */
    public static final C2491fm f496Lm = null;
    /* renamed from: _f_getOffloaders_0020_0028_0029Ljava_002futil_002fCollection_003b */
    public static final C2491fm f497x99c43db3 = null;
    public static final C2491fm _f_stop_0020_0028_0029V = null;
    public static final int _m_fieldCount = 0;
    public static final C5663aRz[] _m_fields = null;
    public static final int _m_methodCount = 0;
    public static final C2491fm[] _m_methods = null;
    public static final C2491fm djk = null;
    public static final C5663aRz hiT = null;
    public static final C2491fm hjA = null;
    public static final C2491fm hjB = null;
    public static final C2491fm hjC = null;
    public static final C2491fm hjD = null;
    public static final C2491fm hjE = null;
    public static final C2491fm hjF = null;
    public static final C2491fm hjG = null;
    public static final C2491fm hjH = null;
    public static final C2491fm hjI = null;
    public static final C2491fm hjJ = null;
    public static final C2491fm hjK = null;
    public static final C2491fm hjL = null;
    public static final C2491fm hjM = null;
    public static final C5663aRz hjf = null;
    public static final C2491fm hjg = null;
    public static final C2491fm hjh = null;
    public static final C2491fm hji = null;
    public static final C2491fm hjj = null;
    public static final C2491fm hjk = null;
    public static final C2491fm hjl = null;
    public static final C2491fm hjm = null;
    public static final C2491fm hjn = null;
    public static final C2491fm hjo = null;
    public static final C2491fm hjp = null;
    public static final C2491fm hjq = null;
    public static final C2491fm hjr = null;
    public static final C2491fm hjs = null;
    public static final C2491fm hjt = null;
    public static final C2491fm hju = null;
    public static final C2491fm hjv = null;
    public static final C2491fm hjw = null;
    public static final C2491fm hjx = null;
    public static final C2491fm hjy = null;
    public static final C2491fm hjz = null;
    /* access modifiers changed from: private */
    public static final LogPrinter logger = LogPrinter.m10275K(TaikodomObject.class);
    /* renamed from: pp */
    public static final C2491fm f498pp = null;
    public static final long serialVersionUID = 0;
    /* renamed from: yH */
    public static final C2491fm f499yH = null;
    /* renamed from: zQ */
    public static final C5663aRz f501zQ = null;
    /* renamed from: zT */
    public static final C2491fm f502zT = null;
    /* renamed from: zU */
    public static final C2491fm f503zU = null;
    public static final long hiR = 10000;
    public static C6494any ___iScriptClass = null;
    @C0064Am(aul = "f5878334d1b40970d5943a6f73d00a87", aum = 0)
    @C5566aOg
    private static C2686iZ<Actor> hiS;
    @C5566aOg
    @C3122oB(mo20937Uq = C3122oB.C3123a.NOT_REPLICATED)
    @C0064Am(aul = "4decb81796ed85816268f44d4c9151d3", aum = 2)
    @C1020Ot(drz = C1020Ot.C1021a.NOT_PERSISTED)
    private static C3438ra<aDR> hje;
    @C0064Am(aul = "be20ef5cbcd911d1cb7b622f74b0b433", aum = 1)

    /* renamed from: zP */
    private static I18NString f500zP;

    static {
        m2903V();
    }

    /* access modifiers changed from: private */
    @ClientOnly
    public THashSet<Actor> hiY;
    @ClientOnly
    public transient long hja;
    /* access modifiers changed from: private */
    @ClientOnly
    public boolean hjd;
    @ClientOnly
    private Set<Actor> hiU;
    @ClientOnly
    private C0339a hiV;
    @ClientOnly
    private C1355Tk hiW;
    @ClientOnly
    private long hiX;
    private transient aDR hiZ;
    @ClientOnly
    private Set<C3856vr> hjb;
    @ClientOnly
    private Set<C5471aKp> hjc;
    @ClientOnly
    private boolean inited;

    public Space() {
        super((C5540aNg) null);
        super.mo10S();
    }

    public Space(C5540aNg ang) {
        super(ang);
    }

    public Space(String str) {
        super((C5540aNg) null);
        super._m_script_init(str);
    }

    public Space(I18NString i18NString) {
        super((C5540aNg) null);
        super._m_script_init(i18NString);
    }

    /* renamed from: V */
    static void m2903V() {
        serialVersionUID = (long) 1;
        _m_fieldCount = TaikodomObject._m_fieldCount + 3;
        _m_methodCount = TaikodomObject._m_methodCount + 42;
        int i = TaikodomObject._m_fieldCount;
        C5663aRz[] arzArr = new C5663aRz[(i + 3)];
        C5663aRz b = C5640aRc.m17844b(Space.class, "f5878334d1b40970d5943a6f73d00a87", i);
        hiT = b;
        arzArr[i] = b;
        int i2 = i + 1;
        C5663aRz b2 = C5640aRc.m17844b(Space.class, "be20ef5cbcd911d1cb7b622f74b0b433", i2);
        f501zQ = b2;
        arzArr[i2] = b2;
        int i3 = i2 + 1;
        C5663aRz b3 = C5640aRc.m17844b(Space.class, "4decb81796ed85816268f44d4c9151d3", i3);
        hjf = b3;
        arzArr[i3] = b3;
        int i4 = i3 + 1;
        _m_fields = arzArr;
        C1634Xv.m11725a((Object[]) TaikodomObject._m_fields, (Object[]) _m_fields);
        int i5 = TaikodomObject._m_methodCount;
        C2491fm[] fmVarArr = new C2491fm[(i5 + 42)];
        C2491fm a = C4105zY.m41624a(Space.class, "6c3fbc424fd2e71efe42b7876b0f9647", i5);
        hjg = a;
        fmVarArr[i5] = a;
        int i6 = i5 + 1;
        C2491fm a2 = C4105zY.m41624a(Space.class, "5f7783a48d23f4c9e45d230ca20bcc5c", i6);
        hjh = a2;
        fmVarArr[i6] = a2;
        int i7 = i6 + 1;
        C2491fm a3 = C4105zY.m41624a(Space.class, "c4d20eb2ea34b73ca6d1714e0e647238", i7);
        hji = a3;
        fmVarArr[i7] = a3;
        int i8 = i7 + 1;
        C2491fm a4 = C4105zY.m41624a(Space.class, "2d849fdff1497069a174d7921d034f21", i8);
        hjj = a4;
        fmVarArr[i8] = a4;
        int i9 = i8 + 1;
        C2491fm a5 = C4105zY.m41624a(Space.class, "0e6fd300325d2b75d7b881803cf062fc", i9);
        hjk = a5;
        fmVarArr[i9] = a5;
        int i10 = i9 + 1;
        C2491fm a6 = C4105zY.m41624a(Space.class, "ca115641abcf2e38a6dac69e9e04d639", i10);
        hjl = a6;
        fmVarArr[i10] = a6;
        int i11 = i10 + 1;
        C2491fm a7 = C4105zY.m41624a(Space.class, "fe55ce4105d196b2c8a8b8aa1c4c6c1a", i11);
        hjm = a7;
        fmVarArr[i11] = a7;
        int i12 = i11 + 1;
        C2491fm a8 = C4105zY.m41624a(Space.class, "36f0bb109bd637a026afe2594f3bf8b0", i12);
        hjn = a8;
        fmVarArr[i12] = a8;
        int i13 = i12 + 1;
        C2491fm a9 = C4105zY.m41624a(Space.class, "c94498a4163c8fbc0045e6f14ea59129", i13);
        f502zT = a9;
        fmVarArr[i13] = a9;
        int i14 = i13 + 1;
        C2491fm a10 = C4105zY.m41624a(Space.class, "d46c34e2083df7db64733f1a64e17f83", i14);
        f503zU = a10;
        fmVarArr[i14] = a10;
        int i15 = i14 + 1;
        C2491fm a11 = C4105zY.m41624a(Space.class, "64a1e63d3ca981b60c8eb81173d8f40a", i15);
        f499yH = a11;
        fmVarArr[i15] = a11;
        int i16 = i15 + 1;
        C2491fm a12 = C4105zY.m41624a(Space.class, "1d77f3ac831a22088a8e24aa4e80f20f", i16);
        hjo = a12;
        fmVarArr[i16] = a12;
        int i17 = i16 + 1;
        C2491fm a13 = C4105zY.m41624a(Space.class, "8062bac06bf3094313b36ec46a5a502e", i17);
        hjp = a13;
        fmVarArr[i17] = a13;
        int i18 = i17 + 1;
        C2491fm a14 = C4105zY.m41624a(Space.class, "13eaf05f9a342da0005590b097295a75", i18);
        f498pp = a14;
        fmVarArr[i18] = a14;
        int i19 = i18 + 1;
        C2491fm a15 = C4105zY.m41624a(Space.class, "604a5d51a72c934765cf3eba7b58ccb1", i19);
        hjq = a15;
        fmVarArr[i19] = a15;
        int i20 = i19 + 1;
        C2491fm a16 = C4105zY.m41624a(Space.class, "d93e25cc6d18ae28f370ad87e6158bfd", i20);
        hjr = a16;
        fmVarArr[i20] = a16;
        int i21 = i20 + 1;
        C2491fm a17 = C4105zY.m41624a(Space.class, "4fa12c4f1fb891efc99889807ff3beea", i21);
        hjs = a17;
        fmVarArr[i21] = a17;
        int i22 = i21 + 1;
        C2491fm a18 = C4105zY.m41624a(Space.class, "9d3c2eb8e7e94fe6225d41a377b98b7d", i22);
        hjt = a18;
        fmVarArr[i22] = a18;
        int i23 = i22 + 1;
        C2491fm a19 = C4105zY.m41624a(Space.class, "ef6659bbebeb8463b54e66185dbcb407", i23);
        hju = a19;
        fmVarArr[i23] = a19;
        int i24 = i23 + 1;
        C2491fm a20 = C4105zY.m41624a(Space.class, "42d869f2b6f156b5ad7313b7abcebe55", i24);
        hjv = a20;
        fmVarArr[i24] = a20;
        int i25 = i24 + 1;
        C2491fm a21 = C4105zY.m41624a(Space.class, "dabf1f0fcaf32993fbc2140fabc7ef82", i25);
        hjw = a21;
        fmVarArr[i25] = a21;
        int i26 = i25 + 1;
        C2491fm a22 = C4105zY.m41624a(Space.class, "7c00474df6658b2a90ebf43918a53281", i26);
        hjx = a22;
        fmVarArr[i26] = a22;
        int i27 = i26 + 1;
        C2491fm a23 = C4105zY.m41624a(Space.class, "2e1e24ed252bbee54d200a57bc62d15b", i27);
        hjy = a23;
        fmVarArr[i27] = a23;
        int i28 = i27 + 1;
        C2491fm a24 = C4105zY.m41624a(Space.class, "87dadd5e778c9b6545b48bc6a3ab5f69", i28);
        f496Lm = a24;
        fmVarArr[i28] = a24;
        int i29 = i28 + 1;
        C2491fm a25 = C4105zY.m41624a(Space.class, "e406c6bbb0be4e1d7c1d86def6f774ae", i29);
        djk = a25;
        fmVarArr[i29] = a25;
        int i30 = i29 + 1;
        C2491fm a26 = C4105zY.m41624a(Space.class, "1586b9f01dc774ccbea0e6e7cd7d587c", i30);
        f495Do = a26;
        fmVarArr[i30] = a26;
        int i31 = i30 + 1;
        C2491fm a27 = C4105zY.m41624a(Space.class, "b4bf824067956d00103177f0d9f9366f", i31);
        hjz = a27;
        fmVarArr[i31] = a27;
        int i32 = i31 + 1;
        C2491fm a28 = C4105zY.m41624a(Space.class, "c72c4aa5972b999d459d3de3fe73724c", i32);
        hjA = a28;
        fmVarArr[i32] = a28;
        int i33 = i32 + 1;
        C2491fm a29 = C4105zY.m41624a(Space.class, "ba68a242e8c47a20060f41ae49eed2b8", i33);
        hjB = a29;
        fmVarArr[i33] = a29;
        int i34 = i33 + 1;
        C2491fm a30 = C4105zY.m41624a(Space.class, "f9400dd5ee1706844d26cf95ad3eb981", i34);
        hjC = a30;
        fmVarArr[i34] = a30;
        int i35 = i34 + 1;
        C2491fm a31 = C4105zY.m41624a(Space.class, "ec82adfd609e13e5ca426a396dc0597b", i35);
        _f_stop_0020_0028_0029V = a31;
        fmVarArr[i35] = a31;
        int i36 = i35 + 1;
        C2491fm a32 = C4105zY.m41624a(Space.class, "597a73e821decac1c2bf3965b72eb81f", i36);
        hjD = a32;
        fmVarArr[i36] = a32;
        int i37 = i36 + 1;
        C2491fm a33 = C4105zY.m41624a(Space.class, "5a6d039f23341a8849fbf4af4d830c20", i37);
        hjE = a33;
        fmVarArr[i37] = a33;
        int i38 = i37 + 1;
        C2491fm a34 = C4105zY.m41624a(Space.class, "fb94a20f08ad6b6c2de1b5ec1d748ae4", i38);
        hjF = a34;
        fmVarArr[i38] = a34;
        int i39 = i38 + 1;
        C2491fm a35 = C4105zY.m41624a(Space.class, "495ff7016538198d2ff9ef583d4317a4", i39);
        hjG = a35;
        fmVarArr[i39] = a35;
        int i40 = i39 + 1;
        C2491fm a36 = C4105zY.m41624a(Space.class, "4f72c80ac61679f79d33f55c1b7c60b5", i40);
        hjH = a36;
        fmVarArr[i40] = a36;
        int i41 = i40 + 1;
        C2491fm a37 = C4105zY.m41624a(Space.class, "0db736213cfe7ae12bf0169851630f5a", i41);
        hjI = a37;
        fmVarArr[i41] = a37;
        int i42 = i41 + 1;
        C2491fm a38 = C4105zY.m41624a(Space.class, "60b377afce23db36ee76a424398d9b26", i42);
        hjJ = a38;
        fmVarArr[i42] = a38;
        int i43 = i42 + 1;
        C2491fm a39 = C4105zY.m41624a(Space.class, "969ec3ac933928eeada98dd03bf76392", i43);
        hjK = a39;
        fmVarArr[i43] = a39;
        int i44 = i43 + 1;
        C2491fm a40 = C4105zY.m41624a(Space.class, "f4395c2d1959e337579fb648dd9a0ab5", i44);
        hjL = a40;
        fmVarArr[i44] = a40;
        int i45 = i44 + 1;
        C2491fm a41 = C4105zY.m41624a(Space.class, "d74d790b869e4bd8bd0bec2bf56a39bc", i45);
        hjM = a41;
        fmVarArr[i45] = a41;
        int i46 = i45 + 1;
        C2491fm a42 = C4105zY.m41624a(Space.class, "5912022b49f467329bec74d3a93631cc", i46);
        f497x99c43db3 = a42;
        fmVarArr[i46] = a42;
        int i47 = i46 + 1;
        _m_methods = fmVarArr;
        C1634Xv.m11725a((Object[]) TaikodomObject._m_methods, (Object[]) _m_methods);
        ___iScriptClass = aUO.m18566a(Space.class, aGX.class, _m_fields, _m_methods);
    }

    @C4034yP
    @C5566aOg
    @C0064Am(aul = "4fa12c4f1fb891efc99889807ff3beea", aum = 0)
    @C2499fr
    /* renamed from: C */
    private void m2901C(List<C6272ajk> list) {
        throw new aWi(new aCE(this, hjs, new Object[]{list}));
    }

    @C4034yP
    @C5566aOg
    @C2499fr
    /* renamed from: D */
    private void m2902D(List<C6272ajk> list) {
        switch (bFf().mo6893i(hjs)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, hjs, new Object[]{list}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, hjs, new Object[]{list}));
                break;
        }
        m2901C(list);
    }

    @C4034yP
    @C5566aOg
    @C0064Am(aul = "dabf1f0fcaf32993fbc2140fabc7ef82", aum = 0)
    @C2499fr
    /* renamed from: a */
    private void m2913a(C4031yM yMVar) {
        throw new aWi(new aCE(this, hjw, new Object[]{yMVar}));
    }

    /* renamed from: ad */
    private void m2915ad(C2686iZ iZVar) {
        bFf().mo5608dq().mo3197f(hiT, iZVar);
    }

    @C4034yP
    @C5566aOg
    @C2499fr
    /* renamed from: b */
    private void m2918b(C4031yM yMVar) {
        switch (bFf().mo6893i(hjw)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, hjw, new Object[]{yMVar}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, hjw, new Object[]{yMVar}));
                break;
        }
        m2913a(yMVar);
    }

    /* renamed from: bk */
    private boolean m2923bk(Actor cr) {
        switch (bFf().mo6893i(hjl)) {
            case 0:
                return false;
            case 2:
                return ((Boolean) bFf().mo5606d(new aCE(this, hjl, new Object[]{cr}))).booleanValue();
            case 3:
                bFf().mo5606d(new aCE(this, hjl, new Object[]{cr}));
                break;
        }
        return m2922bj(cr);
    }

    @C4034yP
    @C2499fr
    @C1253SX
    /* renamed from: bm */
    private void m2925bm(Actor cr) {
        switch (bFf().mo6893i(hjm)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, hjm, new Object[]{cr}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, hjm, new Object[]{cr}));
                break;
        }
        m2924bl(cr);
    }

    /* access modifiers changed from: private */
    /* renamed from: bo */
    public boolean m2927bo(Actor cr) {
        switch (bFf().mo6893i(hjH)) {
            case 0:
                return false;
            case 2:
                return ((Boolean) bFf().mo5606d(new aCE(this, hjH, new Object[]{cr}))).booleanValue();
            case 3:
                bFf().mo5606d(new aCE(this, hjH, new Object[]{cr}));
                break;
        }
        return m2926bn(cr);
    }

    /* access modifiers changed from: private */
    /* renamed from: bq */
    public boolean m2929bq(Actor cr) {
        switch (bFf().mo6893i(hjI)) {
            case 0:
                return false;
            case 2:
                return ((Boolean) bFf().mo5606d(new aCE(this, hjI, new Object[]{cr}))).booleanValue();
            case 3:
                bFf().mo5606d(new aCE(this, hjI, new Object[]{cr}));
                break;
        }
        return m2928bp(cr);
    }

    /* access modifiers changed from: private */
    /* renamed from: bs */
    public boolean m2931bs(Actor cr) {
        switch (bFf().mo6893i(hjJ)) {
            case 0:
                return false;
            case 2:
                return ((Boolean) bFf().mo5606d(new aCE(this, hjJ, new Object[]{cr}))).booleanValue();
            case 3:
                bFf().mo5606d(new aCE(this, hjJ, new Object[]{cr}));
                break;
        }
        return m2930br(cr);
    }

    private C2686iZ cKi() {
        return (C2686iZ) bFf().mo5608dq().mo3214p(hiT);
    }

    private C3438ra cKj() {
        return (C3438ra) bFf().mo5608dq().mo3214p(hjf);
    }

    @C4034yP
    @C5566aOg
    @C0064Am(aul = "1d77f3ac831a22088a8e24aa4e80f20f", aum = 0)
    @C2499fr(mo18857qh = 2)
    private void cKo() {
        throw new aWi(new aCE(this, hjo, new Object[0]));
    }

    @C4034yP
    @C5566aOg
    @C2499fr(mo18857qh = 2)
    private void cKp() {
        switch (bFf().mo6893i(hjo)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, hjo, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, hjo, new Object[0]));
                break;
        }
        cKo();
    }

    /* renamed from: cr */
    private void m2934cr(C3438ra raVar) {
        bFf().mo5608dq().mo3197f(hjf, raVar);
    }

    @C4034yP
    @C2499fr
    @C1253SX
    /* renamed from: d */
    private void m2935d(C4031yM yMVar) {
        switch (bFf().mo6893i(hjx)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, hjx, new Object[]{yMVar}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, hjx, new Object[]{yMVar}));
                break;
        }
        m2932c(yMVar);
    }

    /* renamed from: d */
    private void m2936d(I18NString i18NString) {
        bFf().mo5608dq().mo3197f(f501zQ, i18NString);
    }

    /* renamed from: kb */
    private I18NString m2942kb() {
        return (I18NString) bFf().mo5608dq().mo3214p(f501zQ);
    }

    /* renamed from: S */
    public void mo10S() {
        super.mo10S();
    }

    /* renamed from: T */
    public C6494any mo11T() {
        return ___iScriptClass;
    }

    /* renamed from: U */
    public C2491fm[] mo12U() {
        return _m_methods;
    }

    /* renamed from: W */
    public Object mo13W() {
        return new aGX(this);
    }

    /* renamed from: a */
    public Object mo14a(C0495Gr gr) {
        Object[] args = gr.getArgs();
        switch (gr.mo2417hq() - TaikodomObject._m_methodCount) {
            case 0:
                return cKk();
            case 1:
                m2944n((aDR) args[0]);
                return null;
            case 2:
                m2920bf((Actor) args[0]);
                return null;
            case 3:
                m2921bh((Actor) args[0]);
                return null;
            case 4:
                m2905a(((Long) args[0]).longValue(), (Actor) args[1]);
                return null;
            case 5:
                return new Boolean(m2922bj((Actor) args[0]));
            case 6:
                m2924bl((Actor) args[0]);
                return null;
            case 7:
                return cKm();
            case 8:
                return m2943kd();
            case 9:
                m2937e((I18NString) args[0]);
                return null;
            case 10:
                m2940jF();
                return null;
            case 11:
                cKo();
                return null;
            case 12:
                return new Long(cKq());
            case 13:
                m2939gY();
                return null;
            case 14:
                m2941jI(((Long) args[0]).longValue());
                return null;
            case 15:
                cKs();
                return null;
            case 16:
                m2901C((List) args[0]);
                return null;
            case 17:
                return cKu();
            case 18:
                return cKw();
            case 19:
                return new Boolean(cKy());
            case 20:
                m2913a((C4031yM) args[0]);
                return null;
            case 21:
                m2932c((C4031yM) args[0]);
                return null;
            case 22:
                return cKA();
            case 23:
                m2947qU();
                return null;
            case 24:
                return m2917b((Vec3d) args[0], ((Float) args[1]).floatValue());
            case 25:
                m2909a((C0665JT) args[0]);
                return null;
            case 26:
                m2916ae((C2686iZ) args[0]);
                return null;
            case 27:
                return cKC();
            case 28:
                m2912a((C3856vr) args[0]);
                return null;
            case 29:
                m2911a((C5471aKp) args[0]);
                return null;
            case 30:
                m2946qO();
                return null;
            case 31:
                return new Boolean(cKE());
            case 32:
                return m2904a((Vec3d) args[0], (Vec3f) args[1], ((Float) args[2]).floatValue(), (List<C6615aqP>) (List) args[3]);
            case 33:
                m2910a((Vec3d) args[0], ((Float) args[1]).floatValue(), (Runnable) args[2]);
                return null;
            case 34:
                m2906a((Actor) args[0], (Vec3f) args[1], ((Float) args[2]).floatValue(), (Runnable) args[3]);
                return null;
            case 35:
                return new Boolean(m2926bn((Actor) args[0]));
            case 36:
                return new Boolean(m2928bp((Actor) args[0]));
            case 37:
                return new Boolean(m2930br((Actor) args[0]));
            case 38:
                m2945p((aDR) args[0]);
                return null;
            case 39:
                m2948r((aDR) args[0]);
                return null;
            case 40:
                cKG();
                return null;
            case 41:
                return aad();
            default:
                return super.mo14a(gr);
        }
    }

    /* renamed from: a */
    public void mo15a(Collection collection, C0495Gr gr) {
        super.mo15a(collection, gr);
    }

    public Collection<aDR> aae() {
        switch (bFf().mo6893i(f497x99c43db3)) {
            case 0:
                return null;
            case 2:
                return (Collection) bFf().mo5606d(new aCE(this, f497x99c43db3, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f497x99c43db3, new Object[0]));
                break;
        }
        return aad();
    }

    /* renamed from: af */
    public void mo1891af(C2686iZ<Actor> iZVar) {
        switch (bFf().mo6893i(hjz)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, hjz, new Object[]{iZVar}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, hjz, new Object[]{iZVar}));
                break;
        }
        m2916ae(iZVar);
    }

    /* renamed from: b */
    public Actor mo1892b(Vec3d ajr, Vec3f vec3f, float f, List<C6615aqP> list) {
        switch (bFf().mo6893i(hjE)) {
            case 0:
                return null;
            case 2:
                return (Actor) bFf().mo5606d(new aCE(this, hjE, new Object[]{ajr, vec3f, new Float(f), list}));
            case 3:
                bFf().mo5606d(new aCE(this, hjE, new Object[]{ajr, vec3f, new Float(f), list}));
                break;
        }
        return m2904a(ajr, vec3f, f, list);
    }

    @C4034yP
    @C2499fr
    @C1253SX
    /* renamed from: b */
    public void mo1893b(long j, Actor cr) {
        switch (bFf().mo6893i(hjk)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, hjk, new Object[]{new Long(j), cr}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, hjk, new Object[]{new Long(j), cr}));
                break;
        }
        m2905a(j, cr);
    }

    @C4034yP
    @C2499fr
    @C1253SX
    /* renamed from: b */
    public void mo1894b(Actor cr, Vec3f vec3f, float f, Runnable runnable) {
        switch (bFf().mo6893i(hjG)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, hjG, new Object[]{cr, vec3f, new Float(f), runnable}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, hjG, new Object[]{cr, vec3f, new Float(f), runnable}));
                break;
        }
        m2906a(cr, vec3f, f, runnable);
    }

    /* renamed from: b */
    public void mo24b(C0665JT jt) {
        switch (bFf().mo6893i(f495Do)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f495Do, new Object[]{jt}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f495Do, new Object[]{jt}));
                break;
        }
        m2909a(jt);
    }

    @C4034yP
    @C2499fr
    @C1253SX
    /* renamed from: b */
    public void mo1895b(Vec3d ajr, float f, Runnable runnable) {
        switch (bFf().mo6893i(hjF)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, hjF, new Object[]{ajr, new Float(f), runnable}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, hjF, new Object[]{ajr, new Float(f), runnable}));
                break;
        }
        m2910a(ajr, f, runnable);
    }

    @ClientOnly
    /* renamed from: b */
    public void mo1896b(C5471aKp akp) {
        switch (bFf().mo6893i(hjC)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, hjC, new Object[]{akp}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, hjC, new Object[]{akp}));
                break;
        }
        m2911a(akp);
    }

    @ClientOnly
    /* renamed from: b */
    public void mo1897b(C3856vr vrVar) {
        switch (bFf().mo6893i(hjB)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, hjB, new Object[]{vrVar}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, hjB, new Object[]{vrVar}));
                break;
        }
        m2912a(vrVar);
    }

    /* renamed from: bg */
    public void mo1898bg(Actor cr) {
        switch (bFf().mo6893i(hji)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, hji, new Object[]{cr}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, hji, new Object[]{cr}));
                break;
        }
        m2920bf(cr);
    }

    /* renamed from: bi */
    public void mo1899bi(Actor cr) {
        switch (bFf().mo6893i(hjj)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, hjj, new Object[]{cr}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, hjj, new Object[]{cr}));
                break;
        }
        m2921bh(cr);
    }

    /* renamed from: c */
    public List<Actor> mo1900c(Vec3d ajr, float f) {
        switch (bFf().mo6893i(djk)) {
            case 0:
                return null;
            case 2:
                return (List) bFf().mo5606d(new aCE(this, djk, new Object[]{ajr, new Float(f)}));
            case 3:
                bFf().mo5606d(new aCE(this, djk, new Object[]{ajr, new Float(f)}));
                break;
        }
        return m2917b(ajr, f);
    }

    /* renamed from: c */
    public C5663aRz[] mo25c() {
        return _m_fields;
    }

    /* access modifiers changed from: protected */
    public Set<? extends Actor> cKB() {
        switch (bFf().mo6893i(hjy)) {
            case 0:
                return null;
            case 2:
                return (Set) bFf().mo5606d(new aCE(this, hjy, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, hjy, new Object[0]));
                break;
        }
        return cKA();
    }

    public C2686iZ<Actor> cKD() {
        switch (bFf().mo6893i(hjA)) {
            case 0:
                return null;
            case 2:
                return (C2686iZ) bFf().mo5606d(new aCE(this, hjA, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, hjA, new Object[0]));
                break;
        }
        return cKC();
    }

    public boolean cKF() {
        switch (bFf().mo6893i(hjD)) {
            case 0:
                return false;
            case 2:
                return ((Boolean) bFf().mo5606d(new aCE(this, hjD, new Object[0]))).booleanValue();
            case 3:
                bFf().mo5606d(new aCE(this, hjD, new Object[0]));
                break;
        }
        return cKE();
    }

    public void cKH() {
        switch (bFf().mo6893i(hjM)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, hjM, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, hjM, new Object[0]));
                break;
        }
        cKG();
    }

    public aDR cKl() {
        switch (bFf().mo6893i(hjg)) {
            case 0:
                return null;
            case 2:
                return (aDR) bFf().mo5606d(new aCE(this, hjg, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, hjg, new Object[0]));
                break;
        }
        return cKk();
    }

    @C1253SX
    public C1355Tk cKn() {
        switch (bFf().mo6893i(hjn)) {
            case 0:
                return null;
            case 2:
                return (C1355Tk) bFf().mo5606d(new aCE(this, hjn, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, hjn, new Object[0]));
                break;
        }
        return cKm();
    }

    /* access modifiers changed from: protected */
    public long cKr() {
        switch (bFf().mo6893i(hjp)) {
            case 0:
                return 0;
            case 2:
                return ((Long) bFf().mo5606d(new aCE(this, hjp, new Object[0]))).longValue();
            case 3:
                bFf().mo5606d(new aCE(this, hjp, new Object[0]));
                break;
        }
        return cKq();
    }

    @ClientOnly
    public void cKt() {
        switch (bFf().mo6893i(hjr)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, hjr, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, hjr, new Object[0]));
                break;
        }
        cKs();
    }

    public Set<Actor> cKv() {
        switch (bFf().mo6893i(hjt)) {
            case 0:
                return null;
            case 2:
                return (Set) bFf().mo5606d(new aCE(this, hjt, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, hjt, new Object[0]));
                break;
        }
        return cKu();
    }

    public C0339a cKx() {
        switch (bFf().mo6893i(hju)) {
            case 0:
                return null;
            case 2:
                return (C0339a) bFf().mo5606d(new aCE(this, hju, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, hju, new Object[0]));
                break;
        }
        return cKw();
    }

    @ClientOnly
    public boolean cKz() {
        switch (bFf().mo6893i(hjv)) {
            case 0:
                return false;
            case 2:
                return ((Boolean) bFf().mo5606d(new aCE(this, hjv, new Object[0]))).booleanValue();
            case 3:
                bFf().mo5606d(new aCE(this, hjv, new Object[0]));
                break;
        }
        return cKy();
    }

    /* renamed from: f */
    public void mo1912f(I18NString i18NString) {
        switch (bFf().mo6893i(f503zU)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f503zU, new Object[]{i18NString}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f503zU, new Object[]{i18NString}));
                break;
        }
        m2937e(i18NString);
    }

    @C1253SX
    /* renamed from: gZ */
    public void mo1913gZ() {
        switch (bFf().mo6893i(f498pp)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f498pp, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f498pp, new Object[0]));
                break;
        }
        m2939gY();
    }

    @C1253SX
    public void init() {
        switch (bFf().mo6893i(f499yH)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f499yH, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f499yH, new Object[0]));
                break;
        }
        m2940jF();
    }

    @C1253SX
    /* renamed from: jB */
    public void mo1915jB(long j) {
        switch (bFf().mo6893i(hjq)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, hjq, new Object[]{new Long(j)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, hjq, new Object[]{new Long(j)}));
                break;
        }
        m2941jI(j);
    }

    /* renamed from: ke */
    public I18NString mo1916ke() {
        switch (bFf().mo6893i(f502zT)) {
            case 0:
                return null;
            case 2:
                return (I18NString) bFf().mo5606d(new aCE(this, f502zT, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f502zT, new Object[0]));
                break;
        }
        return m2943kd();
    }

    @C0401Fa
    /* renamed from: o */
    public void mo1919o(aDR adr) {
        switch (bFf().mo6893i(hjh)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, hjh, new Object[]{adr}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, hjh, new Object[]{adr}));
                break;
        }
        m2944n(adr);
    }

    /* renamed from: q */
    public void mo1920q(aDR adr) {
        switch (bFf().mo6893i(hjK)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, hjK, new Object[]{adr}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, hjK, new Object[]{adr}));
                break;
        }
        m2945p(adr);
    }

    /* renamed from: qV */
    public void mo1921qV() {
        switch (bFf().mo6893i(f496Lm)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f496Lm, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f496Lm, new Object[0]));
                break;
        }
        m2947qU();
    }

    /* renamed from: s */
    public void mo1922s(aDR adr) {
        switch (bFf().mo6893i(hjL)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, hjL, new Object[]{adr}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, hjL, new Object[]{adr}));
                break;
        }
        m2948r(adr);
    }

    @C1253SX
    public void stop() {
        switch (bFf().mo6893i(_f_stop_0020_0028_0029V)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, _f_stop_0020_0028_0029V, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, _f_stop_0020_0028_0029V, new Object[0]));
                break;
        }
        m2946qO();
    }

    /* renamed from: nz */
    public void mo1918nz(I18NString i18NString) {
        super.mo10S();
        this.hja = 0;
        m2936d(i18NString);
    }

    /* renamed from: lo */
    public void mo1917lo(String str) {
        super.mo10S();
        this.hja = 0;
        m2936d(new I18NString(str));
    }

    @C0064Am(aul = "6c3fbc424fd2e71efe42b7876b0f9647", aum = 0)
    private aDR cKk() {
        return this.hiZ;
    }

    @C0401Fa
    @C0064Am(aul = "5f7783a48d23f4c9e45d230ca20bcc5c", aum = 0)
    /* renamed from: n */
    private void m2944n(aDR adr) {
        this.hiZ = adr;
    }

    @C0064Am(aul = "c4d20eb2ea34b73ca6d1714e0e647238", aum = 0)
    /* renamed from: bf */
    private void m2920bf(Actor cr) {
        if (!bHa() || cKD().add(cr)) {
            if (bHa() && cKl() != null) {
                mo8366w(cKl());
                cr.mo656qV();
                cVm();
            }
            mo1893b(cVr(), cr);
        }
    }

    @C0064Am(aul = "2d849fdff1497069a174d7921d034f21", aum = 0)
    /* renamed from: bh */
    private void m2921bh(Actor cr) {
        if (!bHa() || cKD().remove(cr)) {
            m2925bm(cr);
        }
    }

    @C4034yP
    @C0064Am(aul = "0e6fd300325d2b75d7b881803cf062fc", aum = 0)
    @C2499fr
    @C1253SX
    /* renamed from: a */
    private void m2905a(long j, Actor cr) {
        if (!cr.cLZ() && !cKv().contains(cr)) {
            if (!bGX()) {
                cr.mo1065f(this, j);
                cKv().add(cr);
            } else if (ala().getPlayer().bQx() != null && cr.azW() != ala().getPlayer().bQx().azW()) {
                mo6317hy("Ignoring " + cr + " spawn cause its in another node (" + ala().getPlayer().getName() + ")");
            } else if (m2923bk(cr)) {
                cr.mo1065f(this, j);
                cKv().add(cr);
            } else {
                cKx().mo1924a(cr, j);
            }
        }
    }

    @C0064Am(aul = "ca115641abcf2e38a6dac69e9e04d639", aum = 0)
    /* renamed from: bj */
    private boolean m2922bj(Actor cr) {
        return (cr instanceof Shot) || (cr instanceof Missile) || (cr instanceof Mine) || (cr instanceof MissionTrigger) || (cr instanceof Loot) || cr.equals(getPlayer().bQx());
    }

    @C4034yP
    @C0064Am(aul = "fe55ce4105d196b2c8a8b8aa1c4c6c1a", aum = 0)
    @C2499fr
    @C1253SX
    /* renamed from: bl */
    private void m2924bl(Actor cr) {
        if (bGX()) {
            cKx().mo1925an(cr);
            return;
        }
        cKv().remove(cr);
        cr.aia();
    }

    @C0064Am(aul = "36f0bb109bd637a026afe2594f3bf8b0", aum = 0)
    @C1253SX
    private C1355Tk cKm() {
        return this.hiW;
    }

    @C0064Am(aul = "c94498a4163c8fbc0045e6f14ea59129", aum = 0)
    /* renamed from: kd */
    private I18NString m2943kd() {
        return m2942kb();
    }

    @C0064Am(aul = "d46c34e2083df7db64733f1a64e17f83", aum = 0)
    /* renamed from: e */
    private void m2937e(I18NString i18NString) {
        m2936d(i18NString);
    }

    @C0064Am(aul = "64a1e63d3ca981b60c8eb81173d8f40a", aum = 0)
    @C1253SX
    /* renamed from: jF */
    private void m2940jF() {
        this.hiW = new C6192aiI();
        C0341b bVar = new C0341b();
        bVar.mo4618b(3, 2, false);
        bVar.mo4618b(3, 1, false);
        bVar.mo4618b(3, 3, false);
        bVar.mo4618b(1, 1, false);
        bVar.mo4618b(2, 2, false);
        bVar.mo16722v(3, 4, 20);
        bVar.mo16722v(0, 4, 3);
        bVar.mo16722v(4, 0, 3);
        bVar.mo16722v(3, 0, 20);
        bVar.mo16722v(4, 4, 3);
        bVar.mo16722v(4, 2, 2);
        bVar.mo16722v(0, 2, 2);
        bVar.mo16722v(0, 0, 3);
        bVar.mo16722v(4, 1, 3);
        bVar.mo16722v(4, 3, 0);
        bVar.mo16722v(0, 3, 0);
        bVar.mo16722v(0, 1, 3);
        bVar.mo16722v(1, 0, 0);
        bVar.mo16722v(1, 4, 0);
        bVar.mo16722v(1, 2, 2);
        bVar.mo16722v(2, 1, 2);
        bVar.mo16722v(2, 4, 2);
        bVar.mo16722v(2, 0, 2);
        bVar.mo16723w(3, 2, 0);
        bVar.mo16723w(3, 1, 0);
        bVar.mo16723w(3, 3, 0);
        bVar.mo16723w(1, 1, 0);
        bVar.mo16723w(2, 2, 0);
        C3859vu vuVar = new C3859vu();
        vuVar.clear();
        vuVar.mo22672a(200000.0f, 8000, -1);
        vuVar.mo22672a(100000.0f, 6000, -1);
        vuVar.mo22672a(50000.0f, 4000, -1);
        vuVar.mo22672a(50000.0f, 2000, 0);
        vuVar.mo22672a(10000.0f, 2000, -1);
        vuVar.mo22672a(10000.0f, 500, 0);
        vuVar.mo22672a(5000.0f, 1000, -1);
        vuVar.mo22672a(5000.0f, 250, 0);
        for (int i = 0; i <= 4; i++) {
            for (int i2 = 0; i2 <= 4; i2++) {
                int aj = bVar.mo16721aj(i, i2);
                if (aj == Integer.MIN_VALUE) {
                    logger.error("*********************************");
                    logger.error(String.format("unset collisionReaction(%d, %d) = %d\n", new Object[]{Integer.valueOf(i), Integer.valueOf(i2), Integer.valueOf(aj)}));
                }
                boolean ai = bVar.mo16720ai(i, i2);
                if (aj != 0 && !ai) {
                    logger.error("*********************************");
                    logger.error(String.format("inconsistent mayCollide==" + ai + " with collisionReaction(%d, %d) = %d\n", new Object[]{Integer.valueOf(i), Integer.valueOf(i2), Integer.valueOf(aj)}));
                }
            }
        }
        this.hiW.mo5726a((C1037PD) bVar);
        this.hiW.mo13315a((C3876wB) vuVar);
        this.hiW.mo5729a(bFf().mo6866PM().getSynchronizerTime());
        this.hiX = this.hiW.bvf() + cKr();
        this.inited = true;
        if (bGY()) {
            cKp();
        }
        if (bGX()) {
            if (this.hjb != null) {
                for (C3856vr a : this.hjb) {
                    a.mo876a((ClientConnect) null, (DataGameEvent) null);
                }
                this.hjb.clear();
            }
            if (this.hjc != null) {
                for (C5471aKp next : this.hjc) {
                    next.mo9807ha().mo1081j(this, next.getWhen());
                }
                this.hjc.clear();
            }
        }
    }

    @C0064Am(aul = "8062bac06bf3094313b36ec46a5a502e", aum = 0)
    private long cKq() {
        return 250;
    }

    @C0064Am(aul = "13eaf05f9a342da0005590b097295a75", aum = 0)
    @C1253SX
    /* renamed from: gY */
    private void m2939gY() {
        mo1915jB(-1);
    }

    @C0064Am(aul = "604a5d51a72c934765cf3eba7b58ccb1", aum = 0)
    @C1253SX
    /* renamed from: jI */
    private void m2941jI(long j) {
        if (!this.inited) {
            throw new IllegalStateException("A space must be inited before stepping");
        }
        if (j == -1) {
            this.hiW.mo5739gZ();
        } else {
            this.hiW.mo5738fI(j);
        }
        if (bGY()) {
            long bvf = this.hiW.bvf();
            if (this.hiX <= bvf) {
                this.hiX = bvf + cKr();
                List<C6272ajk> bWW = this.hiW.bWW();
                if (bWW != null) {
                    m2902D(bWW);
                }
            }
        }
    }

    @C0064Am(aul = "d93e25cc6d18ae28f370ad87e6158bfd", aum = 0)
    @ClientOnly
    private void cKs() {
        long IO = this.hiW.mo5724IO();
        if (bGX()) {
            this.hjd = true;
            if (this.hiY == null) {
                this.hiY = new THashSet<>();
            }
            this.hiY.clear();
            this.hiW.bvi().mo8636a(new C0342c(IO));
            if (this.hjd) {
                Iterator it = this.hiY.iterator();
                while (it.hasNext()) {
                    Actor cr = (Actor) it.next();
                    if (cr.isStatic() && cr.azW() != ald().mo4089dL().bhE().azW()) {
                        mo6317hy("Removing: " + cr + " from client simulation");
                        m2925bm(cr);
                    }
                }
            }
        }
    }

    @C0064Am(aul = "9d3c2eb8e7e94fe6225d41a377b98b7d", aum = 0)
    private Set<Actor> cKu() {
        if (this.hiU == null) {
            this.hiU = new HashSet();
        }
        return this.hiU;
    }

    @C0064Am(aul = "ef6659bbebeb8463b54e66185dbcb407", aum = 0)
    private C0339a cKw() {
        if (this.hiV == null) {
            this.hiV = new C0339a();
        }
        if (!this.hiV.isRunning()) {
            this.hiV.start();
        }
        return this.hiV;
    }

    @C0064Am(aul = "42d869f2b6f156b5ad7313b7abcebe55", aum = 0)
    @ClientOnly
    private boolean cKy() {
        C4031yM yMVar;
        Actor bhE = ald().mo4089dL().bhE();
        C0520HN cLd = bhE.cLd();
        if (cLd == null) {
            return false;
        }
        if (bhE instanceof Fighter) {
            yMVar = new C5874acC(-1, (Fighter) bhE, cLd.mo2472kQ(), cLd.aRh().bfx());
        } else if (bhE instanceof Ship) {
            yMVar = new C6729asZ(-1, bhE, cLd.mo2472kQ(), cLd.aRh().bfx());
        } else {
            yMVar = new C4031yM(-1, bhE, cLd.mo2472kQ(), cLd.aRh().bfx());
        }
        m2918b(yMVar);
        return true;
    }

    @C4034yP
    @C0064Am(aul = "7c00474df6658b2a90ebf43918a53281", aum = 0)
    @C2499fr
    @C1253SX
    /* renamed from: c */
    private void m2932c(C4031yM yMVar) {
        yMVar.mo12575a(this.hiW);
    }

    @C0064Am(aul = "2e1e24ed252bbee54d200a57bc62d15b", aum = 0)
    private Set<? extends Actor> cKA() {
        return cKD();
    }

    @C0064Am(aul = "87dadd5e778c9b6545b48bc6a3ab5f69", aum = 0)
    /* renamed from: qU */
    private void m2947qU() {
        push();
    }

    @C0064Am(aul = "e406c6bbb0be4e1d7c1d86def6f774ae", aum = 0)
    /* renamed from: b */
    private List<Actor> m2917b(Vec3d ajr, float f) {
        if (this.hiW == null) {
            return Collections.emptyList();
        }
        ArrayList arrayList = new ArrayList();
        this.hiW.mo5727a(ajr, f, (C1355Tk.C1356a) new C0343d(arrayList));
        return arrayList;
    }

    @C0064Am(aul = "1586b9f01dc774ccbea0e6e7cd7d587c", aum = 0)
    /* renamed from: a */
    private void m2909a(C0665JT jt) {
    }

    @C0064Am(aul = "b4bf824067956d00103177f0d9f9366f", aum = 0)
    /* renamed from: ae */
    private void m2916ae(C2686iZ<Actor> iZVar) {
        m2915ad(iZVar);
    }

    @C0064Am(aul = "c72c4aa5972b999d459d3de3fe73724c", aum = 0)
    private C2686iZ<Actor> cKC() {
        return cKi();
    }

    @C0064Am(aul = "ba68a242e8c47a20060f41ae49eed2b8", aum = 0)
    @ClientOnly
    /* renamed from: a */
    private void m2912a(C3856vr vrVar) {
        if (this.hjb == null) {
            this.hjb = new HashSet();
        }
        this.hjb.add(vrVar);
    }

    @C0064Am(aul = "f9400dd5ee1706844d26cf95ad3eb981", aum = 0)
    @ClientOnly
    /* renamed from: a */
    private void m2911a(C5471aKp akp) {
        if (this.hjc == null) {
            this.hjc = new HashSet();
        }
        this.hjc.add(akp);
    }

    @C0064Am(aul = "ec82adfd609e13e5ca426a396dc0597b", aum = 0)
    @C1253SX
    /* renamed from: qO */
    private void m2946qO() {
        this.hiW.dispose();
        this.hiW = null;
        this.inited = false;
        if (this.hiU != null) {
            Iterator it = new ArrayList(this.hiU).iterator();
            while (it.hasNext()) {
                try {
                    ((Actor) it.next()).aia();
                } catch (Exception e) {
                    logger.error("Error unspawning actor", e);
                }
            }
            this.hiU.clear();
        }
    }

    @C0064Am(aul = "597a73e821decac1c2bf3965b72eb81f", aum = 0)
    private boolean cKE() {
        return this.inited;
    }

    @C0064Am(aul = "5a6d039f23341a8849fbf4af4d830c20", aum = 0)
    /* renamed from: a */
    private Actor m2904a(Vec3d ajr, Vec3f vec3f, float f, List<C6615aqP> list) {
        ArrayList arrayList = new ArrayList();
        this.hiW.mo5728a(ajr, vec3f, f, new C0344e(arrayList), list);
        if (arrayList.size() == 0) {
            return null;
        }
        return (Actor) arrayList.get(0);
    }

    @C4034yP
    @C0064Am(aul = "fb94a20f08ad6b6c2de1b5ec1d748ae4", aum = 0)
    @C2499fr
    @C1253SX
    /* renamed from: a */
    private void m2910a(Vec3d ajr, float f, Runnable runnable) {
    }

    @C4034yP
    @C0064Am(aul = "495ff7016538198d2ff9ef583d4317a4", aum = 0)
    @C2499fr
    @C1253SX
    /* renamed from: a */
    private void m2906a(Actor cr, Vec3f vec3f, float f, Runnable runnable) {
        mo1895b(cr.getPosition().mo9531q(vec3f), f, runnable);
    }

    @C0064Am(aul = "4f72c80ac61679f79d33f55c1b7c60b5", aum = 0)
    /* renamed from: bn */
    private boolean m2926bn(Actor cr) {
        return (cr instanceof Gate) || (cr instanceof Station);
    }

    @C0064Am(aul = "0db736213cfe7ae12bf0169851630f5a", aum = 0)
    /* renamed from: bp */
    private boolean m2928bp(Actor cr) {
        return cr instanceof Ship;
    }

    @C0064Am(aul = "60b377afce23db36ee76a424398d9b26", aum = 0)
    /* renamed from: br */
    private boolean m2930br(Actor cr) {
        return cr instanceof Turret;
    }

    @C0064Am(aul = "969ec3ac933928eeada98dd03bf76392", aum = 0)
    /* renamed from: p */
    private void m2945p(aDR adr) {
        mo8359ma("adding proxy " + adr + " to space ");
        cKj().add(adr);
    }

    @C0064Am(aul = "f4395c2d1959e337579fb648dd9a0ab5", aum = 0)
    /* renamed from: r */
    private void m2948r(aDR adr) {
        mo8359ma("removing proxy " + adr + " from space ");
        cKj().remove(adr);
    }

    @C0064Am(aul = "d74d790b869e4bd8bd0bec2bf56a39bc", aum = 0)
    private void cKG() {
        cKj().clear();
    }

    @C0064Am(aul = "5912022b49f467329bec74d3a93631cc", aum = 0)
    private Collection<aDR> aad() {
        return cKj();
    }

    /* renamed from: a.Ea$b */
    /* compiled from: a */
    class C0341b extends C6944awj {
        C0341b() {
        }

        /* renamed from: a */
        public boolean mo1932a(C6615aqP aqp, C6615aqP aqp2) {
            if (super.mo1932a(aqp, aqp2)) {
                try {
                    if (!((C0520HN) aqp).mo2545a((C0520HN) aqp2) || !((C0520HN) aqp2).mo2545a((C0520HN) aqp)) {
                        return false;
                    }
                    return true;
                } catch (ThreadDeath e) {
                    throw e;
                } catch (Throwable th) {
                    Space.logger.error("ignoring error at mayCollide", th);
                }
            }
            return false;
        }
    }

    /* renamed from: a.Ea$c */
    /* compiled from: a */
    class C0342c implements C3055nX.C3057b<C0819Lq, C0461GN> {
        private final /* synthetic */ long efJ;

        C0342c(long j) {
            this.efJ = j;
        }

        /* renamed from: a */
        public void mo1933a(C3055nX.C3056a<C0819Lq, C0461GN> aVar) {
            if (this.efJ - aVar.mo11191IO() >= 10000) {
                for (List<C0463GP> next : aVar.mo11192IP()) {
                    if (next != null) {
                        for (C0463GP gp : next) {
                            if (((C0819Lq) gp.mo2340kP()).bfw() instanceof C0520HN) {
                                Actor ha = ((C0520HN) ((C0819Lq) gp.mo2340kP()).bfw()).mo2568ha();
                                if (aVar.mo11191IO() == 0) {
                                    Space.this.mo8360mc("Voxel with last update 0.");
                                } else if (ha != Space.this.ala().aLW().ald().mo4089dL().bhE()) {
                                    Space.this.hiY.add(ha);
                                } else {
                                    Space.this.hjd = false;
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    /* renamed from: a.Ea$d */
    /* compiled from: a */
    class C0343d implements C1355Tk.C1356a {
        private final /* synthetic */ List efK;

        C0343d(List list) {
            this.efK = list;
        }

        /* renamed from: c */
        public boolean mo1934c(C6615aqP aqp) {
            if (!(aqp instanceof C0520HN)) {
                return true;
            }
            this.efK.add(((C0520HN) aqp).mo2568ha());
            return true;
        }
    }

    /* renamed from: a.Ea$e */
    /* compiled from: a */
    class C0344e implements C1355Tk.C1356a {
        private final /* synthetic */ List efK;

        C0344e(List list) {
            this.efK = list;
        }

        /* renamed from: c */
        public boolean mo1934c(C6615aqP aqp) {
            if (!(aqp instanceof C0520HN)) {
                return true;
            }
            this.efK.add(((C0520HN) aqp).mo2568ha());
            return false;
        }
    }

    /* renamed from: a.Ea$a */
    public class C0339a extends WrapRunnable {
        final int cQV = 10;
        private List<C0345f<Actor, Long>> cQW = new ArrayList();
        private List<C0345f<Actor, Long>> cQX = new ArrayList();
        private List<C0345f<Actor, Long>> cQY = new ArrayList();
        private boolean running = false;

        public C0339a() {
        }

        public void start() {
            Space.this.ald().alf().addTask("object paging", this, 200);
        }

        public boolean isRunning() {
            return this.running;
        }

        public void stop() {
            this.cQW.clear();
            this.cQY.clear();
            this.cQX.clear();
            this.running = false;
            cancel();
        }

        /* renamed from: a */
        public void mo1924a(Actor cr, long j) {
            C0345f fVar = new C0345f(cr, Long.valueOf(j));
            if (!Space.this.m2931bs(cr)) {
                if (Space.this.m2927bo(cr)) {
                    synchronized (this.cQW) {
                        if (!this.cQW.contains(fVar)) {
                            this.cQW.add(fVar);
                        }
                    }
                } else if (Space.this.m2929bq(cr)) {
                    synchronized (this.cQX) {
                        if (!this.cQX.contains(fVar)) {
                            this.cQX.add(fVar);
                        }
                    }
                } else {
                    synchronized (this.cQY) {
                        if (!this.cQY.contains(fVar)) {
                            this.cQY.add(fVar);
                        }
                    }
                }
            }
        }

        /* renamed from: an */
        public void mo1925an(Actor cr) {
            if (!Space.this.m2931bs(cr)) {
                if (Space.this.m2927bo(cr)) {
                    synchronized (this.cQW) {
                        if (this.cQW.contains(cr)) {
                            this.cQW.remove(cr);
                            return;
                        }
                    }
                } else if (Space.this.m2929bq(cr)) {
                    synchronized (this.cQX) {
                        if (this.cQX.contains(cr)) {
                            this.cQX.remove(cr);
                            return;
                        }
                    }
                } else {
                    synchronized (this.cQY) {
                        if (this.cQY.contains(cr)) {
                            this.cQY.remove(cr);
                            return;
                        }
                    }
                }
                Space.this.cKv().remove(cr);
                cr.aia();
            }
        }

        public void run() {
            int i = 0;
            if (Space.this.cKn() == null) {
                stop();
            } else if (this.cQW.isEmpty() && this.cQX.isEmpty() && this.cQY.isEmpty()) {
            } else {
                if (!this.cQW.isEmpty()) {
                    Iterator it = new ArrayList(this.cQW).iterator();
                    while (true) {
                        int i2 = i;
                        if (it.hasNext() && i2 < 10) {
                            m2978a((C0345f<Actor, Long>) (C0345f) it.next(), this.cQW);
                            i = i2 + 1;
                        } else {
                            return;
                        }
                    }
                } else if (!this.cQX.isEmpty()) {
                    Iterator it2 = new ArrayList(this.cQX).iterator();
                    while (true) {
                        int i3 = i;
                        if (it2.hasNext() && i3 < 10) {
                            m2978a((C0345f<Actor, Long>) (C0345f) it2.next(), this.cQX);
                            i = i3 + 1;
                        } else {
                            return;
                        }
                    }
                } else if (!this.cQY.isEmpty()) {
                    Iterator it3 = new ArrayList(this.cQY).iterator();
                    while (true) {
                        int i4 = i;
                        if (it3.hasNext() && i4 < 10) {
                            m2978a((C0345f<Actor, Long>) (C0345f) it3.next(), this.cQY);
                            i = i4 + 1;
                        } else {
                            return;
                        }
                    }
                }
            }
        }

        /* renamed from: a */
        private void m2978a(C0345f<Actor, Long> fVar, List<C0345f<Actor, Long>> list) {
            if (!((Actor) fVar.dpG).mo961QW()) {
                ((Actor) fVar.dpG).mo1065f(Space.this, ((Long) fVar.dpH).longValue());
                Space.this.cKv().add((Actor) fVar.dpG);
            } else {
                ((Actor) C3582se.m38985a((Actor) fVar.dpG, (C6144ahM<?>) new C0340a(fVar))).mo656qV();
            }
            synchronized (list) {
                list.remove(fVar);
            }
        }

        /* renamed from: a.Ea$a$a */
        class C0340a implements C6144ahM {
            private final /* synthetic */ C0345f hjR;

            C0340a(C0345f fVar) {
                this.hjR = fVar;
            }

            /* renamed from: n */
            public void mo1931n(Object obj) {
                ((Actor) this.hjR.dpG).mo1065f(Space.this, ((Long) this.hjR.dpH).longValue());
                Space.this.cKv().add((Actor) this.hjR.dpG);
            }

            /* renamed from: a */
            public void mo1930a(Throwable th) {
            }
        }
    }

    /* renamed from: a.Ea$f */
    /* compiled from: a */
    public class C0345f<T1, T2> {
        public final T1 dpG;
        public final T2 dpH;

        public C0345f(T1 t1, T2 t2) {
            this.dpG = t1;
            this.dpH = t2;
        }

        public boolean equals(Object obj) {
            if (obj instanceof C0345f) {
                if (((C0345f) obj).dpG.equals(this.dpG)) {
                    return true;
                }
                return false;
            } else if (!(obj instanceof Actor)) {
                return false;
            } else {
                if (!((Actor) obj).equals(this.dpG)) {
                    return false;
                }
                return true;
            }
        }

        public int hashCode() {
            return super.hashCode();
        }
    }
}
