package game.script.simulation;

import game.network.message.externalizable.aCE;
import game.script.Actor;
import game.script.TaikodomObject;
import game.script.ship.Scenery;
import game.script.ship.Ship;
import game.script.ship.Station;
import game.script.space.Asteroid;
import game.script.space.Gate;
import logic.baa.*;
import logic.data.mbean.C5417aIn;
import logic.res.code.C5640aRc;
import logic.res.code.C5663aRz;
import logic.res.code.C6494any;
import logic.res.html.C2491fm;
import p001a.*;

import java.util.Collection;
import java.util.UUID;

@C5511aMd
@C6485anp
/* renamed from: a.sS */
/* compiled from: a */
public class SurfaceDefaults extends TaikodomObject implements C0468GU, C1616Xf {
    public static final int _m_fieldCount = 0;
    public static final C5663aRz[] _m_fields = null;
    public static final int _m_methodCount = 0;
    public static final C2491fm[] _m_methods = null;
    /* renamed from: bL */
    public static final C5663aRz f9103bL = null;
    /* renamed from: bN */
    public static final C2491fm f9104bN = null;
    /* renamed from: bO */
    public static final C2491fm f9105bO = null;
    /* renamed from: bP */
    public static final C2491fm f9106bP = null;
    public static final C5663aRz biX = null;
    public static final C5663aRz biZ = null;
    public static final C5663aRz bjb = null;
    public static final C5663aRz bjd = null;
    public static final C5663aRz bjf = null;
    public static final C5663aRz bjh = null;
    public static final C2491fm bji = null;
    public static final C2491fm bjj = null;
    public static final C2491fm bjk = null;
    public static final C2491fm bjl = null;
    public static final C2491fm bjm = null;
    public static final C2491fm bjn = null;
    public static final C2491fm bjo = null;
    public static final C2491fm bjp = null;
    public static final C2491fm bjq = null;
    public static final C2491fm bjr = null;
    public static final C2491fm bjs = null;
    public static final C2491fm bjt = null;
    public static final C2491fm bju = null;
    public static final long serialVersionUID = 0;
    public static C6494any ___iScriptClass;
    @C0064Am(aul = "e97bf248bec55e7b004ca3247641dc44", aum = 6)

    /* renamed from: bK */
    private static UUID f9102bK;
    @C0064Am(aul = "7062d08f6f1e26ccff69458127f146a2", aum = 0)
    private static String biW;
    @C0064Am(aul = "1fa1d107e4bff5e847607065652573fe", aum = 1)
    private static String biY;
    @C0064Am(aul = "ac3bf38b150820c0a3178ad4a38cff35", aum = 2)
    private static String bja;
    @C0064Am(aul = "14a147b6d2c1d52eb8f77ea5dbaabbd7", aum = 3)
    private static String bjc;
    @C0064Am(aul = "de3889ee2114be99876b00afd02720ca", aum = 4)
    private static String bje;
    @C0064Am(aul = "1fd8f2825f6825350abb055f42848038", aum = 5)
    private static String bjg;

    static {
        m38737V();
    }

    public SurfaceDefaults() {
        super((C5540aNg) null);
        super.mo10S();
    }

    public SurfaceDefaults(C5540aNg ang) {
        super(ang);
    }

    /* renamed from: V */
    static void m38737V() {
        serialVersionUID = (long) 1;
        _m_fieldCount = TaikodomObject._m_fieldCount + 7;
        _m_methodCount = TaikodomObject._m_methodCount + 16;
        int i = TaikodomObject._m_fieldCount;
        C5663aRz[] arzArr = new C5663aRz[(i + 7)];
        C5663aRz b = C5640aRc.m17844b(SurfaceDefaults.class, "7062d08f6f1e26ccff69458127f146a2", i);
        biX = b;
        arzArr[i] = b;
        int i2 = i + 1;
        C5663aRz b2 = C5640aRc.m17844b(SurfaceDefaults.class, "1fa1d107e4bff5e847607065652573fe", i2);
        biZ = b2;
        arzArr[i2] = b2;
        int i3 = i2 + 1;
        C5663aRz b3 = C5640aRc.m17844b(SurfaceDefaults.class, "ac3bf38b150820c0a3178ad4a38cff35", i3);
        bjb = b3;
        arzArr[i3] = b3;
        int i4 = i3 + 1;
        C5663aRz b4 = C5640aRc.m17844b(SurfaceDefaults.class, "14a147b6d2c1d52eb8f77ea5dbaabbd7", i4);
        bjd = b4;
        arzArr[i4] = b4;
        int i5 = i4 + 1;
        C5663aRz b5 = C5640aRc.m17844b(SurfaceDefaults.class, "de3889ee2114be99876b00afd02720ca", i5);
        bjf = b5;
        arzArr[i5] = b5;
        int i6 = i5 + 1;
        C5663aRz b6 = C5640aRc.m17844b(SurfaceDefaults.class, "1fd8f2825f6825350abb055f42848038", i6);
        bjh = b6;
        arzArr[i6] = b6;
        int i7 = i6 + 1;
        C5663aRz b7 = C5640aRc.m17844b(SurfaceDefaults.class, "e97bf248bec55e7b004ca3247641dc44", i7);
        f9103bL = b7;
        arzArr[i7] = b7;
        int i8 = i7 + 1;
        _m_fields = arzArr;
        C1634Xv.m11725a((Object[]) TaikodomObject._m_fields, (Object[]) _m_fields);
        int i9 = TaikodomObject._m_methodCount;
        C2491fm[] fmVarArr = new C2491fm[(i9 + 16)];
        C2491fm a = C4105zY.m41624a(SurfaceDefaults.class, "d5882e7abcac391e507222b3c8249f1a", i9);
        f9104bN = a;
        fmVarArr[i9] = a;
        int i10 = i9 + 1;
        C2491fm a2 = C4105zY.m41624a(SurfaceDefaults.class, "fd99999a80d94dd69619a6199e6137f2", i10);
        f9105bO = a2;
        fmVarArr[i10] = a2;
        int i11 = i10 + 1;
        C2491fm a3 = C4105zY.m41624a(SurfaceDefaults.class, "0b12faa82c46a6ecf6db25a9a682b29f", i11);
        f9106bP = a3;
        fmVarArr[i11] = a3;
        int i12 = i11 + 1;
        C2491fm a4 = C4105zY.m41624a(SurfaceDefaults.class, "3c14c43ff1ac07f8657a1e9f11f08085", i12);
        bji = a4;
        fmVarArr[i12] = a4;
        int i13 = i12 + 1;
        C2491fm a5 = C4105zY.m41624a(SurfaceDefaults.class, "47537c2ea2908352ceee5bced9f13db2", i13);
        bjj = a5;
        fmVarArr[i13] = a5;
        int i14 = i13 + 1;
        C2491fm a6 = C4105zY.m41624a(SurfaceDefaults.class, "ecf8d90da0e09ef184833afdd870442f", i14);
        bjk = a6;
        fmVarArr[i14] = a6;
        int i15 = i14 + 1;
        C2491fm a7 = C4105zY.m41624a(SurfaceDefaults.class, "6c552fcda936b85cf9c655b7f0c2100c", i15);
        bjl = a7;
        fmVarArr[i15] = a7;
        int i16 = i15 + 1;
        C2491fm a8 = C4105zY.m41624a(SurfaceDefaults.class, "e8e5a9f5eefeaf8531b2fb49d415ed10", i16);
        bjm = a8;
        fmVarArr[i16] = a8;
        int i17 = i16 + 1;
        C2491fm a9 = C4105zY.m41624a(SurfaceDefaults.class, "a19c269e8197b132a3b164f6da0ce439", i17);
        bjn = a9;
        fmVarArr[i17] = a9;
        int i18 = i17 + 1;
        C2491fm a10 = C4105zY.m41624a(SurfaceDefaults.class, "216c584f408b815062e0b35316a55457", i18);
        bjo = a10;
        fmVarArr[i18] = a10;
        int i19 = i18 + 1;
        C2491fm a11 = C4105zY.m41624a(SurfaceDefaults.class, "1c2506dc6dd144a44689cefa9ebd993c", i19);
        bjp = a11;
        fmVarArr[i19] = a11;
        int i20 = i19 + 1;
        C2491fm a12 = C4105zY.m41624a(SurfaceDefaults.class, "b53ad0298412a1add3b5e3644ff260f1", i20);
        bjq = a12;
        fmVarArr[i20] = a12;
        int i21 = i20 + 1;
        C2491fm a13 = C4105zY.m41624a(SurfaceDefaults.class, "9515ec814cc3fb396c07d36368beaa13", i21);
        bjr = a13;
        fmVarArr[i21] = a13;
        int i22 = i21 + 1;
        C2491fm a14 = C4105zY.m41624a(SurfaceDefaults.class, "7e2f24c565cbe2dd4acba1c66af9f6fe", i22);
        bjs = a14;
        fmVarArr[i22] = a14;
        int i23 = i22 + 1;
        C2491fm a15 = C4105zY.m41624a(SurfaceDefaults.class, "96d86352b1cac10890bdd61202e195fb", i23);
        bjt = a15;
        fmVarArr[i23] = a15;
        int i24 = i23 + 1;
        C2491fm a16 = C4105zY.m41624a(SurfaceDefaults.class, "298c01e156fc73a9276467126d4a8eb6", i24);
        bju = a16;
        fmVarArr[i24] = a16;
        int i25 = i24 + 1;
        _m_methods = fmVarArr;
        C1634Xv.m11725a((Object[]) TaikodomObject._m_methods, (Object[]) _m_methods);
        ___iScriptClass = aUO.m18566a(SurfaceDefaults.class, C5417aIn.class, _m_fields, _m_methods);
    }

    /* renamed from: a */
    private void m38738a(UUID uuid) {
        bFf().mo5608dq().mo3197f(f9103bL, uuid);
    }

    private String acb() {
        return (String) bFf().mo5608dq().mo3214p(biX);
    }

    private String acc() {
        return (String) bFf().mo5608dq().mo3214p(biZ);
    }

    private String acd() {
        return (String) bFf().mo5608dq().mo3214p(bjb);
    }

    private String ace() {
        return (String) bFf().mo5608dq().mo3214p(bjd);
    }

    private String acf() {
        return (String) bFf().mo5608dq().mo3214p(bjf);
    }

    private String acg() {
        return (String) bFf().mo5608dq().mo3214p(bjh);
    }

    /* renamed from: an */
    private UUID m38739an() {
        return (UUID) bFf().mo5608dq().mo3214p(f9103bL);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Station Surface")
    @C0064Am(aul = "6c552fcda936b85cf9c655b7f0c2100c", aum = 0)
    @C5566aOg
    /* renamed from: bA */
    private void m38743bA(String str) {
        throw new aWi(new aCE(this, bjl, new Object[]{str}));
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Asteroid Surface")
    @C0064Am(aul = "a19c269e8197b132a3b164f6da0ce439", aum = 0)
    @C5566aOg
    /* renamed from: bC */
    private void m38744bC(String str) {
        throw new aWi(new aCE(this, bjn, new Object[]{str}));
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Scenery Surface")
    @C0064Am(aul = "1c2506dc6dd144a44689cefa9ebd993c", aum = 0)
    @C5566aOg
    /* renamed from: bE */
    private void m38745bE(String str) {
        throw new aWi(new aCE(this, bjp, new Object[]{str}));
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Gate Surface")
    @C0064Am(aul = "9515ec814cc3fb396c07d36368beaa13", aum = 0)
    @C5566aOg
    /* renamed from: bG */
    private void m38746bG(String str) {
        throw new aWi(new aCE(this, bjr, new Object[]{str}));
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Cruise Speed Sufix")
    @C0064Am(aul = "96d86352b1cac10890bdd61202e195fb", aum = 0)
    @C5566aOg
    /* renamed from: bI */
    private void m38747bI(String str) {
        throw new aWi(new aCE(this, bjt, new Object[]{str}));
    }

    /* renamed from: bs */
    private void m38748bs(String str) {
        bFf().mo5608dq().mo3197f(biX, str);
    }

    /* renamed from: bt */
    private void m38749bt(String str) {
        bFf().mo5608dq().mo3197f(biZ, str);
    }

    /* renamed from: bu */
    private void m38750bu(String str) {
        bFf().mo5608dq().mo3197f(bjb, str);
    }

    /* renamed from: bv */
    private void m38751bv(String str) {
        bFf().mo5608dq().mo3197f(bjd, str);
    }

    /* renamed from: bw */
    private void m38752bw(String str) {
        bFf().mo5608dq().mo3197f(bjf, str);
    }

    /* renamed from: bx */
    private void m38753bx(String str) {
        bFf().mo5608dq().mo3197f(bjh, str);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Ship Surface")
    @C0064Am(aul = "47537c2ea2908352ceee5bced9f13db2", aum = 0)
    @C5566aOg
    /* renamed from: by */
    private void m38754by(String str) {
        throw new aWi(new aCE(this, bjj, new Object[]{str}));
    }

    /* renamed from: c */
    private void m38755c(UUID uuid) {
        switch (bFf().mo6893i(f9105bO)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f9105bO, new Object[]{uuid}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f9105bO, new Object[]{uuid}));
                break;
        }
        m38742b(uuid);
    }

    /* renamed from: Q */
    public String mo21878Q(Actor cr) {
        switch (bFf().mo6893i(bju)) {
            case 0:
                return null;
            case 2:
                return (String) bFf().mo5606d(new aCE(this, bju, new Object[]{cr}));
            case 3:
                bFf().mo5606d(new aCE(this, bju, new Object[]{cr}));
                break;
        }
        return m38736P(cr);
    }

    /* renamed from: T */
    public C6494any mo11T() {
        return ___iScriptClass;
    }

    /* renamed from: U */
    public C2491fm[] mo12U() {
        return _m_methods;
    }

    /* renamed from: W */
    public Object mo13W() {
        return new C5417aIn(this);
    }

    /* renamed from: a */
    public Object mo14a(C0495Gr gr) {
        Object[] args = gr.getArgs();
        switch (gr.mo2417hq() - TaikodomObject._m_methodCount) {
            case 0:
                return m38740ap();
            case 1:
                m38742b((UUID) args[0]);
                return null;
            case 2:
                return m38741ar();
            case 3:
                return ach();
            case 4:
                m38754by((String) args[0]);
                return null;
            case 5:
                return acj();
            case 6:
                m38743bA((String) args[0]);
                return null;
            case 7:
                return acl();
            case 8:
                m38744bC((String) args[0]);
                return null;
            case 9:
                return acn();
            case 10:
                m38745bE((String) args[0]);
                return null;
            case 11:
                return acp();
            case 12:
                m38746bG((String) args[0]);
                return null;
            case 13:
                return acr();
            case 14:
                m38747bI((String) args[0]);
                return null;
            case 15:
                return m38736P((Actor) args[0]);
            default:
                return super.mo14a(gr);
        }
    }

    /* renamed from: a */
    public void mo15a(Collection collection, C0495Gr gr) {
        super.mo15a(collection, gr);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Ship Surface")
    public String aci() {
        switch (bFf().mo6893i(bji)) {
            case 0:
                return null;
            case 2:
                return (String) bFf().mo5606d(new aCE(this, bji, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, bji, new Object[0]));
                break;
        }
        return ach();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Station Surface")
    public String ack() {
        switch (bFf().mo6893i(bjk)) {
            case 0:
                return null;
            case 2:
                return (String) bFf().mo5606d(new aCE(this, bjk, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, bjk, new Object[0]));
                break;
        }
        return acj();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Asteroid Surface")
    public String acm() {
        switch (bFf().mo6893i(bjm)) {
            case 0:
                return null;
            case 2:
                return (String) bFf().mo5606d(new aCE(this, bjm, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, bjm, new Object[0]));
                break;
        }
        return acl();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Scenery Surface")
    public String aco() {
        switch (bFf().mo6893i(bjo)) {
            case 0:
                return null;
            case 2:
                return (String) bFf().mo5606d(new aCE(this, bjo, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, bjo, new Object[0]));
                break;
        }
        return acn();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Gate Surface")
    public String acq() {
        switch (bFf().mo6893i(bjq)) {
            case 0:
                return null;
            case 2:
                return (String) bFf().mo5606d(new aCE(this, bjq, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, bjq, new Object[0]));
                break;
        }
        return acp();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Cruise Speed Sufix")
    public String acs() {
        switch (bFf().mo6893i(bjs)) {
            case 0:
                return null;
            case 2:
                return (String) bFf().mo5606d(new aCE(this, bjs, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, bjs, new Object[0]));
                break;
        }
        return acr();
    }

    /* renamed from: aq */
    public UUID mo18aq() {
        switch (bFf().mo6893i(f9104bN)) {
            case 0:
                return null;
            case 2:
                return (UUID) bFf().mo5606d(new aCE(this, f9104bN, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f9104bN, new Object[0]));
                break;
        }
        return m38740ap();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Station Surface")
    @C5566aOg
    /* renamed from: bB */
    public void mo21885bB(String str) {
        switch (bFf().mo6893i(bjl)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, bjl, new Object[]{str}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, bjl, new Object[]{str}));
                break;
        }
        m38743bA(str);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Asteroid Surface")
    @C5566aOg
    /* renamed from: bD */
    public void mo21886bD(String str) {
        switch (bFf().mo6893i(bjn)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, bjn, new Object[]{str}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, bjn, new Object[]{str}));
                break;
        }
        m38744bC(str);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Scenery Surface")
    @C5566aOg
    /* renamed from: bF */
    public void mo21887bF(String str) {
        switch (bFf().mo6893i(bjp)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, bjp, new Object[]{str}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, bjp, new Object[]{str}));
                break;
        }
        m38745bE(str);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Gate Surface")
    @C5566aOg
    /* renamed from: bH */
    public void mo21888bH(String str) {
        switch (bFf().mo6893i(bjr)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, bjr, new Object[]{str}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, bjr, new Object[]{str}));
                break;
        }
        m38746bG(str);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Cruise Speed Sufix")
    @C5566aOg
    /* renamed from: bJ */
    public void mo21889bJ(String str) {
        switch (bFf().mo6893i(bjt)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, bjt, new Object[]{str}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, bjt, new Object[]{str}));
                break;
        }
        m38747bI(str);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Ship Surface")
    @C5566aOg
    /* renamed from: bz */
    public void mo21890bz(String str) {
        switch (bFf().mo6893i(bjj)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, bjj, new Object[]{str}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, bjj, new Object[]{str}));
                break;
        }
        m38754by(str);
    }

    /* renamed from: c */
    public C5663aRz[] mo25c() {
        return _m_fields;
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Handle")
    public String getHandle() {
        switch (bFf().mo6893i(f9106bP)) {
            case 0:
                return null;
            case 2:
                return (String) bFf().mo5606d(new aCE(this, f9106bP, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f9106bP, new Object[0]));
                break;
        }
        return m38741ar();
    }

    @C0064Am(aul = "d5882e7abcac391e507222b3c8249f1a", aum = 0)
    /* renamed from: ap */
    private UUID m38740ap() {
        return m38739an();
    }

    @C0064Am(aul = "fd99999a80d94dd69619a6199e6137f2", aum = 0)
    /* renamed from: b */
    private void m38742b(UUID uuid) {
        m38738a(uuid);
    }

    /* renamed from: S */
    public void mo10S() {
        super.mo10S();
        m38738a(UUID.randomUUID());
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Handle")
    @C0064Am(aul = "0b12faa82c46a6ecf6db25a9a682b29f", aum = 0)
    /* renamed from: ar */
    private String m38741ar() {
        return "surface_defaults";
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Ship Surface")
    @C0064Am(aul = "3c14c43ff1ac07f8657a1e9f11f08085", aum = 0)
    private String ach() {
        return acb();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Station Surface")
    @C0064Am(aul = "ecf8d90da0e09ef184833afdd870442f", aum = 0)
    private String acj() {
        return acc();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Asteroid Surface")
    @C0064Am(aul = "e8e5a9f5eefeaf8531b2fb49d415ed10", aum = 0)
    private String acl() {
        return acd();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Scenery Surface")
    @C0064Am(aul = "216c584f408b815062e0b35316a55457", aum = 0)
    private String acn() {
        return ace();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Gate Surface")
    @C0064Am(aul = "b53ad0298412a1add3b5e3644ff260f1", aum = 0)
    private String acp() {
        return acf();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Cruise Speed Sufix")
    @C0064Am(aul = "7e2f24c565cbe2dd4acba1c66af9f6fe", aum = 0)
    private String acr() {
        return acg();
    }

    @C0064Am(aul = "298c01e156fc73a9276467126d4a8eb6", aum = 0)
    /* renamed from: P */
    private String m38736P(Actor cr) {
        if (cr instanceof Ship) {
            return acb();
        }
        if (cr instanceof Station) {
            return acc();
        }
        if (cr instanceof Asteroid) {
            return acd();
        }
        if (cr instanceof Scenery) {
            return ace();
        }
        if (cr instanceof Gate) {
            return acf();
        }
        return null;
    }
}
