package game.script;

import game.network.message.externalizable.aCE;
import game.script.billing.ItemBillingDefaults;
import game.script.chat.ChatDefaults;
import game.script.cloning.CloningDefaults;
import game.script.corporation.CorporationDefaults;
import game.script.faction.Faction;
import game.script.insurance.InsuranceDefaults;
import game.script.item.durability.ItemDurabilityDefaults;
import game.script.map3d.Map3DDefaults;
import game.script.mission.MissionBeaconType;
import game.script.mission.MissionTriggerType;
import game.script.nursery.NurseryDefaults;
import game.script.pda.PdaDefaults;
import game.script.resource.Asset;
import game.script.ship.OutpostDefaults;
import game.script.ship.ShipDefaults;
import game.script.ship.Station;
import game.script.simulation.SurfaceDefaults;
import logic.baa.*;
import logic.data.mbean.C4099zS;
import logic.res.code.C5640aRc;
import logic.res.code.C5663aRz;
import logic.res.code.C6494any;
import logic.res.html.C2491fm;
import p001a.*;

import java.util.Collection;
import java.util.UUID;

@C5511aMd
@C6485anp
/* renamed from: a.gV */
/* compiled from: a */
public class TaikodomDefaultContents extends aDJ implements C0468GU, C1616Xf {
    /* renamed from: RF */
    public static final C5663aRz f7591RF = null;
    /* renamed from: RH */
    public static final C5663aRz f7593RH = null;
    /* renamed from: RJ */
    public static final C5663aRz f7595RJ = null;
    /* renamed from: RL */
    public static final C5663aRz f7597RL = null;
    /* renamed from: RN */
    public static final C5663aRz f7599RN = null;
    /* renamed from: RQ */
    public static final C5663aRz f7601RQ = null;
    /* renamed from: RS */
    public static final C5663aRz f7603RS = null;
    /* renamed from: RU */
    public static final C5663aRz f7605RU = null;
    /* renamed from: RW */
    public static final C5663aRz f7607RW = null;
    /* renamed from: RY */
    public static final C5663aRz f7609RY = null;
    /* renamed from: SA */
    public static final C2491fm f7611SA = null;
    /* renamed from: SB */
    public static final C2491fm f7612SB = null;
    /* renamed from: SC */
    public static final C2491fm f7613SC = null;
    /* renamed from: SD */
    public static final C2491fm f7614SD = null;
    /* renamed from: SE */
    public static final C2491fm f7615SE = null;
    /* renamed from: SF */
    public static final C2491fm f7616SF = null;
    /* renamed from: SG */
    public static final C2491fm f7617SG = null;
    /* renamed from: SH */
    public static final C2491fm f7618SH = null;
    /* renamed from: SI */
    public static final C2491fm f7619SI = null;
    /* renamed from: SJ */
    public static final C2491fm f7620SJ = null;
    /* renamed from: SK */
    public static final C2491fm f7621SK = null;
    /* renamed from: SL */
    public static final C2491fm f7622SL = null;
    /* renamed from: SM */
    public static final C2491fm f7623SM = null;
    /* renamed from: SN */
    public static final C2491fm f7624SN = null;
    /* renamed from: SO */
    public static final C2491fm f7625SO = null;
    /* renamed from: SP */
    public static final C2491fm f7626SP = null;
    /* renamed from: SQ */
    public static final C2491fm f7627SQ = null;
    /* renamed from: SR */
    public static final C2491fm f7628SR = null;
    /* renamed from: SS */
    public static final C2491fm f7629SS = null;
    /* renamed from: ST */
    public static final C2491fm f7630ST = null;
    /* renamed from: SU */
    public static final C2491fm f7631SU = null;
    /* renamed from: Sa */
    public static final C5663aRz f7632Sa = null;
    /* renamed from: Sc */
    public static final C5663aRz f7634Sc = null;
    /* renamed from: Se */
    public static final C5663aRz f7636Se = null;
    /* renamed from: Sg */
    public static final C5663aRz f7638Sg = null;
    /* renamed from: Si */
    public static final C5663aRz f7640Si = null;
    /* renamed from: Sk */
    public static final C5663aRz f7642Sk = null;
    /* renamed from: Sm */
    public static final C5663aRz f7644Sm = null;
    /* renamed from: So */
    public static final C5663aRz f7646So = null;
    /* renamed from: Sq */
    public static final C5663aRz f7648Sq = null;
    /* renamed from: Sr */
    public static final C2491fm f7649Sr = null;
    /* renamed from: Ss */
    public static final C2491fm f7650Ss = null;
    /* renamed from: St */
    public static final C2491fm f7651St = null;
    /* renamed from: Su */
    public static final C2491fm f7652Su = null;
    /* renamed from: Sv */
    public static final C2491fm f7653Sv = null;
    /* renamed from: Sw */
    public static final C2491fm f7654Sw = null;
    /* renamed from: Sx */
    public static final C2491fm f7655Sx = null;
    /* renamed from: Sy */
    public static final C2491fm f7656Sy = null;
    /* renamed from: Sz */
    public static final C2491fm f7657Sz = null;
    public static final int _m_fieldCount = 0;
    public static final C5663aRz[] _m_fields = null;
    public static final int _m_methodCount = 0;
    public static final C2491fm[] _m_methods = null;
    /* renamed from: bL */
    public static final C5663aRz f7659bL = null;
    /* renamed from: bN */
    public static final C2491fm f7660bN = null;
    /* renamed from: bO */
    public static final C2491fm f7661bO = null;
    /* renamed from: bP */
    public static final C2491fm f7662bP = null;
    public static final long serialVersionUID = 0;
    /* renamed from: yH */
    public static final C2491fm f7663yH = null;
    public static C6494any ___iScriptClass;
    @C0064Am(aul = "aadf3703843cdc9440faa27120679278", aum = 0)

    /* renamed from: RE */
    private static Faction f7590RE;
    @C0064Am(aul = "29f06c26a0a7d5c2ae3e79cce5349467", aum = 1)

    /* renamed from: RG */
    private static Station f7592RG;
    @C0064Am(aul = "067b00cdae37e152be0f921c782e95cf", aum = 2)

    /* renamed from: RI */
    private static MissionTriggerType f7594RI;
    @C0064Am(aul = "a71b0c2e2d4941ce5238409b58d6adb0", aum = 3)

    /* renamed from: RK */
    private static MissionBeaconType f7596RK;
    @C0064Am(aul = "de0a1dbe06e7e8dc1dc70f05ee9dda57", aum = 4)

    /* renamed from: RM */
    private static CorporationDefaults f7598RM;
    @C0064Am(aul = "a9d09612db39916ab6d63646e30f0973", aum = 5)

    /* renamed from: RO */
    private static OutpostDefaults f7600RO;
    @C0064Am(aul = "c83ab5b194c290a010ac81267439d4f7", aum = 6)

    /* renamed from: RR */
    private static CloningDefaults f7602RR;
    @C0064Am(aul = "6114faaf4c13ce481fcb05934e385d0f", aum = 7)

    /* renamed from: RT */
    private static InsuranceDefaults f7604RT;
    @C0064Am(aul = "5c70f4bbfe47e9c5feab6fc4ea8b3bf6", aum = 8)

    /* renamed from: RV */
    private static ItemBillingDefaults f7606RV;
    @C0064Am(aul = "a1ed1fce414d6ffef17b2e5e087d0109", aum = 9)

    /* renamed from: RX */
    private static ItemDurabilityDefaults f7608RX;
    @C0064Am(aul = "cc72b75eececccca40fba3f97f175031", aum = 10)

    /* renamed from: RZ */
    private static PdaDefaults f7610RZ;
    @C0064Am(aul = "8a57f6d76a1b6b36a9d91ed6135035db", aum = 11)

    /* renamed from: Sb */
    private static Map3DDefaults f7633Sb;
    @C0064Am(aul = "b1bdde80558bc765a4d319e5e5d0026b", aum = 12)

    /* renamed from: Sd */
    private static ChatDefaults f7635Sd;
    @C0064Am(aul = "9b7ee4e0dee9fe6ffcbaea2fcf5c865c", aum = 13)

    /* renamed from: Sf */
    private static NurseryDefaults f7637Sf;
    @C0064Am(aul = "4e40949657d96f110c55100be4829e2e", aum = 14)

    /* renamed from: Sh */
    private static ShipDefaults f7639Sh;
    @C0064Am(aul = "f7034cf83baf711281b0bb4d0838f7c0", aum = 15)

    /* renamed from: Sj */
    private static SurfaceDefaults f7641Sj;
    @C0064Am(aul = "f87318ca231bf39b83ab80152ad5949f", aum = 16)

    /* renamed from: Sl */
    private static float f7643Sl;
    @C0064Am(aul = "374390b79712898da1ce1da2441e7923", aum = 17)

    /* renamed from: Sn */
    private static Asset f7645Sn;
    @C0064Am(aul = "ea68925a09852dace27e16a3b6e2dbac", aum = 18)

    /* renamed from: Sp */
    private static float f7647Sp;
    @C0064Am(aul = "1f74b3bee6532a1e06f47bcd4c508b82", aum = 19)

    /* renamed from: bK */
    private static UUID f7658bK;

    static {
        m31985V();
    }

    public TaikodomDefaultContents() {
        super((C5540aNg) null);
        super.mo10S();
    }

    public TaikodomDefaultContents(C5540aNg ang) {
        super(ang);
    }

    /* renamed from: V */
    static void m31985V() {
        serialVersionUID = (long) 1;
        _m_fieldCount = aDJ._m_fieldCount + 20;
        _m_methodCount = aDJ._m_methodCount + 34;
        int i = aDJ._m_fieldCount;
        C5663aRz[] arzArr = new C5663aRz[(i + 20)];
        C5663aRz b = C5640aRc.m17844b(TaikodomDefaultContents.class, "aadf3703843cdc9440faa27120679278", i);
        f7591RF = b;
        arzArr[i] = b;
        int i2 = i + 1;
        C5663aRz b2 = C5640aRc.m17844b(TaikodomDefaultContents.class, "29f06c26a0a7d5c2ae3e79cce5349467", i2);
        f7593RH = b2;
        arzArr[i2] = b2;
        int i3 = i2 + 1;
        C5663aRz b3 = C5640aRc.m17844b(TaikodomDefaultContents.class, "067b00cdae37e152be0f921c782e95cf", i3);
        f7595RJ = b3;
        arzArr[i3] = b3;
        int i4 = i3 + 1;
        C5663aRz b4 = C5640aRc.m17844b(TaikodomDefaultContents.class, "a71b0c2e2d4941ce5238409b58d6adb0", i4);
        f7597RL = b4;
        arzArr[i4] = b4;
        int i5 = i4 + 1;
        C5663aRz b5 = C5640aRc.m17844b(TaikodomDefaultContents.class, "de0a1dbe06e7e8dc1dc70f05ee9dda57", i5);
        f7599RN = b5;
        arzArr[i5] = b5;
        int i6 = i5 + 1;
        C5663aRz b6 = C5640aRc.m17844b(TaikodomDefaultContents.class, "a9d09612db39916ab6d63646e30f0973", i6);
        f7601RQ = b6;
        arzArr[i6] = b6;
        int i7 = i6 + 1;
        C5663aRz b7 = C5640aRc.m17844b(TaikodomDefaultContents.class, "c83ab5b194c290a010ac81267439d4f7", i7);
        f7603RS = b7;
        arzArr[i7] = b7;
        int i8 = i7 + 1;
        C5663aRz b8 = C5640aRc.m17844b(TaikodomDefaultContents.class, "6114faaf4c13ce481fcb05934e385d0f", i8);
        f7605RU = b8;
        arzArr[i8] = b8;
        int i9 = i8 + 1;
        C5663aRz b9 = C5640aRc.m17844b(TaikodomDefaultContents.class, "5c70f4bbfe47e9c5feab6fc4ea8b3bf6", i9);
        f7607RW = b9;
        arzArr[i9] = b9;
        int i10 = i9 + 1;
        C5663aRz b10 = C5640aRc.m17844b(TaikodomDefaultContents.class, "a1ed1fce414d6ffef17b2e5e087d0109", i10);
        f7609RY = b10;
        arzArr[i10] = b10;
        int i11 = i10 + 1;
        C5663aRz b11 = C5640aRc.m17844b(TaikodomDefaultContents.class, "cc72b75eececccca40fba3f97f175031", i11);
        f7632Sa = b11;
        arzArr[i11] = b11;
        int i12 = i11 + 1;
        C5663aRz b12 = C5640aRc.m17844b(TaikodomDefaultContents.class, "8a57f6d76a1b6b36a9d91ed6135035db", i12);
        f7634Sc = b12;
        arzArr[i12] = b12;
        int i13 = i12 + 1;
        C5663aRz b13 = C5640aRc.m17844b(TaikodomDefaultContents.class, "b1bdde80558bc765a4d319e5e5d0026b", i13);
        f7636Se = b13;
        arzArr[i13] = b13;
        int i14 = i13 + 1;
        C5663aRz b14 = C5640aRc.m17844b(TaikodomDefaultContents.class, "9b7ee4e0dee9fe6ffcbaea2fcf5c865c", i14);
        f7638Sg = b14;
        arzArr[i14] = b14;
        int i15 = i14 + 1;
        C5663aRz b15 = C5640aRc.m17844b(TaikodomDefaultContents.class, "4e40949657d96f110c55100be4829e2e", i15);
        f7640Si = b15;
        arzArr[i15] = b15;
        int i16 = i15 + 1;
        C5663aRz b16 = C5640aRc.m17844b(TaikodomDefaultContents.class, "f7034cf83baf711281b0bb4d0838f7c0", i16);
        f7642Sk = b16;
        arzArr[i16] = b16;
        int i17 = i16 + 1;
        C5663aRz b17 = C5640aRc.m17844b(TaikodomDefaultContents.class, "f87318ca231bf39b83ab80152ad5949f", i17);
        f7644Sm = b17;
        arzArr[i17] = b17;
        int i18 = i17 + 1;
        C5663aRz b18 = C5640aRc.m17844b(TaikodomDefaultContents.class, "374390b79712898da1ce1da2441e7923", i18);
        f7646So = b18;
        arzArr[i18] = b18;
        int i19 = i18 + 1;
        C5663aRz b19 = C5640aRc.m17844b(TaikodomDefaultContents.class, "ea68925a09852dace27e16a3b6e2dbac", i19);
        f7648Sq = b19;
        arzArr[i19] = b19;
        int i20 = i19 + 1;
        C5663aRz b20 = C5640aRc.m17844b(TaikodomDefaultContents.class, "1f74b3bee6532a1e06f47bcd4c508b82", i20);
        f7659bL = b20;
        arzArr[i20] = b20;
        int i21 = i20 + 1;
        _m_fields = arzArr;
        C1634Xv.m11725a((Object[]) aDJ._m_fields, (Object[]) _m_fields);
        int i22 = aDJ._m_methodCount;
        C2491fm[] fmVarArr = new C2491fm[(i22 + 34)];
        C2491fm a = C4105zY.m41624a(TaikodomDefaultContents.class, "cea14c3d034ae0ad9a4263fa1bb6a99c", i22);
        f7660bN = a;
        fmVarArr[i22] = a;
        int i23 = i22 + 1;
        C2491fm a2 = C4105zY.m41624a(TaikodomDefaultContents.class, "94d05660d423ec69679fab4a909aedd7", i23);
        f7661bO = a2;
        fmVarArr[i23] = a2;
        int i24 = i23 + 1;
        C2491fm a3 = C4105zY.m41624a(TaikodomDefaultContents.class, "2bf7e377177da296dc1083846d5d6adb", i24);
        f7662bP = a3;
        fmVarArr[i24] = a3;
        int i25 = i24 + 1;
        C2491fm a4 = C4105zY.m41624a(TaikodomDefaultContents.class, "c3b3871a05304c1b3b7e57375aa12f5e", i25);
        f7663yH = a4;
        fmVarArr[i25] = a4;
        int i26 = i25 + 1;
        C2491fm a5 = C4105zY.m41624a(TaikodomDefaultContents.class, "ba6451759500386b93f89f3c067e9927", i26);
        f7649Sr = a5;
        fmVarArr[i26] = a5;
        int i27 = i26 + 1;
        C2491fm a6 = C4105zY.m41624a(TaikodomDefaultContents.class, "98c559feb9dafd145e2745dfa6383f8e", i27);
        f7650Ss = a6;
        fmVarArr[i27] = a6;
        int i28 = i27 + 1;
        C2491fm a7 = C4105zY.m41624a(TaikodomDefaultContents.class, "51565305a919d916ab4d54a28963b8df", i28);
        f7651St = a7;
        fmVarArr[i28] = a7;
        int i29 = i28 + 1;
        C2491fm a8 = C4105zY.m41624a(TaikodomDefaultContents.class, "f57c0c1d25f22a8239529b2e78334187", i29);
        f7652Su = a8;
        fmVarArr[i29] = a8;
        int i30 = i29 + 1;
        C2491fm a9 = C4105zY.m41624a(TaikodomDefaultContents.class, "575a78e26c1e273345b7d26fe99dda91", i30);
        f7653Sv = a9;
        fmVarArr[i30] = a9;
        int i31 = i30 + 1;
        C2491fm a10 = C4105zY.m41624a(TaikodomDefaultContents.class, "af42d24a19de181b3bb32ab13693bd48", i31);
        f7654Sw = a10;
        fmVarArr[i31] = a10;
        int i32 = i31 + 1;
        C2491fm a11 = C4105zY.m41624a(TaikodomDefaultContents.class, "22a395bdc8843f12b2b0e931962f1e97", i32);
        f7655Sx = a11;
        fmVarArr[i32] = a11;
        int i33 = i32 + 1;
        C2491fm a12 = C4105zY.m41624a(TaikodomDefaultContents.class, "1aeac7585e1c052e631762d2574d1aff", i33);
        f7656Sy = a12;
        fmVarArr[i33] = a12;
        int i34 = i33 + 1;
        C2491fm a13 = C4105zY.m41624a(TaikodomDefaultContents.class, "3c98f80f62ef0281bb15cc09a4e2b4d1", i34);
        f7657Sz = a13;
        fmVarArr[i34] = a13;
        int i35 = i34 + 1;
        C2491fm a14 = C4105zY.m41624a(TaikodomDefaultContents.class, "8f7bf17645bc002c58e19a3665c01d33", i35);
        f7611SA = a14;
        fmVarArr[i35] = a14;
        int i36 = i35 + 1;
        C2491fm a15 = C4105zY.m41624a(TaikodomDefaultContents.class, "bee5838f7f948f0ad3ca4e57b55d2298", i36);
        f7612SB = a15;
        fmVarArr[i36] = a15;
        int i37 = i36 + 1;
        C2491fm a16 = C4105zY.m41624a(TaikodomDefaultContents.class, "0161af5ef645a1825d5d4e9eb1ace198", i37);
        f7613SC = a16;
        fmVarArr[i37] = a16;
        int i38 = i37 + 1;
        C2491fm a17 = C4105zY.m41624a(TaikodomDefaultContents.class, "667eeb9f2e80df14bfa39c7c7f097a2e", i38);
        f7614SD = a17;
        fmVarArr[i38] = a17;
        int i39 = i38 + 1;
        C2491fm a18 = C4105zY.m41624a(TaikodomDefaultContents.class, "b0f96b8647bfd26cb6d083c156217a37", i39);
        f7615SE = a18;
        fmVarArr[i39] = a18;
        int i40 = i39 + 1;
        C2491fm a19 = C4105zY.m41624a(TaikodomDefaultContents.class, "c122608224491e82d04df86d7aae669e", i40);
        f7616SF = a19;
        fmVarArr[i40] = a19;
        int i41 = i40 + 1;
        C2491fm a20 = C4105zY.m41624a(TaikodomDefaultContents.class, "4c673ed1c556a83f10082b006f7e757e", i41);
        f7617SG = a20;
        fmVarArr[i41] = a20;
        int i42 = i41 + 1;
        C2491fm a21 = C4105zY.m41624a(TaikodomDefaultContents.class, "2fb8b6492f23a63d028a4de179f77d88", i42);
        f7618SH = a21;
        fmVarArr[i42] = a21;
        int i43 = i42 + 1;
        C2491fm a22 = C4105zY.m41624a(TaikodomDefaultContents.class, "50ebe1eea259606eb9572b1a837cded0", i43);
        f7619SI = a22;
        fmVarArr[i43] = a22;
        int i44 = i43 + 1;
        C2491fm a23 = C4105zY.m41624a(TaikodomDefaultContents.class, "0015d112716cc22280f0b09c18cdefca", i44);
        f7620SJ = a23;
        fmVarArr[i44] = a23;
        int i45 = i44 + 1;
        C2491fm a24 = C4105zY.m41624a(TaikodomDefaultContents.class, "6545d66e66110469d99c941b541da67f", i45);
        f7621SK = a24;
        fmVarArr[i45] = a24;
        int i46 = i45 + 1;
        C2491fm a25 = C4105zY.m41624a(TaikodomDefaultContents.class, "106d2fc65275fdea7ca9068e44dad2e7", i46);
        f7622SL = a25;
        fmVarArr[i46] = a25;
        int i47 = i46 + 1;
        C2491fm a26 = C4105zY.m41624a(TaikodomDefaultContents.class, "fa2428ef4c1347c67f25d1edad509069", i47);
        f7623SM = a26;
        fmVarArr[i47] = a26;
        int i48 = i47 + 1;
        C2491fm a27 = C4105zY.m41624a(TaikodomDefaultContents.class, "ba831b0fcea2f41c95e32dbe903091b9", i48);
        f7624SN = a27;
        fmVarArr[i48] = a27;
        int i49 = i48 + 1;
        C2491fm a28 = C4105zY.m41624a(TaikodomDefaultContents.class, "1cb90593d76bd26ccd9cfb72e3c5caf6", i49);
        f7625SO = a28;
        fmVarArr[i49] = a28;
        int i50 = i49 + 1;
        C2491fm a29 = C4105zY.m41624a(TaikodomDefaultContents.class, "1e5ed870b182910c1ec7e39e92e039a3", i50);
        f7626SP = a29;
        fmVarArr[i50] = a29;
        int i51 = i50 + 1;
        C2491fm a30 = C4105zY.m41624a(TaikodomDefaultContents.class, "e1a06e9c240f313193b4a8e89cdd34ff", i51);
        f7627SQ = a30;
        fmVarArr[i51] = a30;
        int i52 = i51 + 1;
        C2491fm a31 = C4105zY.m41624a(TaikodomDefaultContents.class, "3839c83bba0f5f34bfd35fcfe682d43e", i52);
        f7628SR = a31;
        fmVarArr[i52] = a31;
        int i53 = i52 + 1;
        C2491fm a32 = C4105zY.m41624a(TaikodomDefaultContents.class, "00ed65bc44458e7d998985f3568af01f", i53);
        f7629SS = a32;
        fmVarArr[i53] = a32;
        int i54 = i53 + 1;
        C2491fm a33 = C4105zY.m41624a(TaikodomDefaultContents.class, "7b8e31027f8c51bff7f0d28eff7070b6", i54);
        f7630ST = a33;
        fmVarArr[i54] = a33;
        int i55 = i54 + 1;
        C2491fm a34 = C4105zY.m41624a(TaikodomDefaultContents.class, "dbc6896c1d530921aae7d81fab3625e6", i55);
        f7631SU = a34;
        fmVarArr[i55] = a34;
        int i56 = i55 + 1;
        _m_methods = fmVarArr;
        C1634Xv.m11725a((Object[]) aDJ._m_methods, (Object[]) _m_methods);
        ___iScriptClass = aUO.m18566a(TaikodomDefaultContents.class, C4099zS.class, _m_fields, _m_methods);
    }

    /* renamed from: a */
    private void m31986a(CorporationDefaults ab) {
        bFf().mo5608dq().mo3197f(f7599RN, ab);
    }

    /* renamed from: a */
    private void m31987a(InsuranceDefaults ah) {
        bFf().mo5608dq().mo3197f(f7605RU, ah);
    }

    /* renamed from: a */
    private void m31988a(PdaDefaults abi) {
        bFf().mo5608dq().mo3197f(f7632Sa, abi);
    }

    /* renamed from: a */
    private void m31989a(OutpostDefaults acu) {
        bFf().mo5608dq().mo3197f(f7601RQ, acu);
    }

    /* renamed from: a */
    private void m31990a(ShipDefaults adv) {
        bFf().mo5608dq().mo3197f(f7640Si, adv);
    }

    /* renamed from: a */
    private void m31991a(MissionBeaconType afd) {
        bFf().mo5608dq().mo3197f(f7597RL, afd);
    }

    /* renamed from: a */
    private void m31992a(MissionTriggerType alb) {
        bFf().mo5608dq().mo3197f(f7595RJ, alb);
    }

    /* renamed from: a */
    private void m31993a(Map3DDefaults abb) {
        bFf().mo5608dq().mo3197f(f7634Sc, abb);
    }

    /* renamed from: a */
    private void m31994a(CloningDefaults ads) {
        bFf().mo5608dq().mo3197f(f7603RS, ads);
    }

    /* renamed from: a */
    private void m31995a(NurseryDefaults ahp) {
        bFf().mo5608dq().mo3197f(f7638Sg, ahp);
    }

    /* renamed from: a */
    private void m31996a(ItemDurabilityDefaults amn) {
        bFf().mo5608dq().mo3197f(f7609RY, amn);
    }

    /* renamed from: a */
    private void m31997a(ItemBillingDefaults arx) {
        bFf().mo5608dq().mo3197f(f7607RW, arx);
    }

    /* renamed from: a */
    private void m31998a(ChatDefaults khVar) {
        bFf().mo5608dq().mo3197f(f7636Se, khVar);
    }

    /* renamed from: a */
    private void m31999a(SurfaceDefaults sSVar) {
        bFf().mo5608dq().mo3197f(f7642Sk, sSVar);
    }

    /* renamed from: a */
    private void m32000a(UUID uuid) {
        bFf().mo5608dq().mo3197f(f7659bL, uuid);
    }

    /* renamed from: al */
    private void m32001al(float f) {
        bFf().mo5608dq().mo3150a(f7644Sm, f);
    }

    /* renamed from: am */
    private void m32002am(float f) {
        bFf().mo5608dq().mo3150a(f7648Sq, f);
    }

    /* renamed from: an */
    private UUID m32003an() {
        return (UUID) bFf().mo5608dq().mo3214p(f7659bL);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Global storage volume for all players")
    @C0064Am(aul = "c122608224491e82d04df86d7aae669e", aum = 0)
    @C5566aOg
    /* renamed from: an */
    private void m32004an(float f) {
        throw new aWi(new aCE(this, f7616SF, new Object[]{new Float(f)}));
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Undocked Trade Range Limit")
    @C0064Am(aul = "ba831b0fcea2f41c95e32dbe903091b9", aum = 0)
    @C5566aOg
    /* renamed from: ap */
    private void m32006ap(float f) {
        throw new aWi(new aCE(this, f7624SN, new Object[]{new Float(f)}));
    }

    /* renamed from: b */
    private void m32008b(Station bf) {
        bFf().mo5608dq().mo3197f(f7593RH, bf);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "PDA Defaults")
    @C0064Am(aul = "6545d66e66110469d99c941b541da67f", aum = 0)
    @C5566aOg
    /* renamed from: b */
    private void m32009b(PdaDefaults abi) {
        throw new aWi(new aCE(this, f7621SK, new Object[]{abi}));
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Ship Defaults")
    @C0064Am(aul = "00ed65bc44458e7d998985f3568af01f", aum = 0)
    @C5566aOg
    /* renamed from: b */
    private void m32010b(ShipDefaults adv) {
        throw new aWi(new aCE(this, f7629SS, new Object[]{adv}));
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Mission Beacon Type")
    @C0064Am(aul = "bee5838f7f948f0ad3ca4e57b55d2298", aum = 0)
    @C5566aOg
    /* renamed from: b */
    private void m32011b(MissionBeaconType afd) {
        throw new aWi(new aCE(this, f7612SB, new Object[]{afd}));
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Waypoint Type")
    @C0064Am(aul = "3c98f80f62ef0281bb15cc09a4e2b4d1", aum = 0)
    @C5566aOg
    /* renamed from: b */
    private void m32012b(MissionTriggerType alb) {
        throw new aWi(new aCE(this, f7657Sz, new Object[]{alb}));
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Nursery Defaults")
    @C0064Am(aul = "e1a06e9c240f313193b4a8e89cdd34ff", aum = 0)
    @C5566aOg
    /* renamed from: b */
    private void m32013b(NurseryDefaults ahp) {
        throw new aWi(new aCE(this, f7627SQ, new Object[]{ahp}));
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Surface Defaults")
    @C0064Am(aul = "dbc6896c1d530921aae7d81fab3625e6", aum = 0)
    @C5566aOg
    /* renamed from: b */
    private void m32014b(SurfaceDefaults sSVar) {
        throw new aWi(new aCE(this, f7631SU, new Object[]{sSVar}));
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Station")
    @C0064Am(aul = "22a395bdc8843f12b2b0e931962f1e97", aum = 0)
    @C5566aOg
    /* renamed from: c */
    private void m32016c(Station bf) {
        throw new aWi(new aCE(this, f7655Sx, new Object[]{bf}));
    }

    /* renamed from: c */
    private void m32017c(UUID uuid) {
        switch (bFf().mo6893i(f7661bO)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f7661bO, new Object[]{uuid}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f7661bO, new Object[]{uuid}));
                break;
        }
        m32015b(uuid);
    }

    /* renamed from: e */
    private void m32018e(Faction xm) {
        bFf().mo5608dq().mo3197f(f7591RF, xm);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Faction")
    @C0064Am(aul = "f57c0c1d25f22a8239529b2e78334187", aum = 0)
    @C5566aOg
    /* renamed from: f */
    private void m32019f(Faction xm) {
        throw new aWi(new aCE(this, f7652Su, new Object[]{xm}));
    }

    /* renamed from: w */
    private void m32021w(Asset tCVar) {
        bFf().mo5608dq().mo3197f(f7646So, tCVar);
    }

    /* renamed from: wF */
    private Faction m32022wF() {
        return (Faction) bFf().mo5608dq().mo3214p(f7591RF);
    }

    /* renamed from: wG */
    private Station m32023wG() {
        return (Station) bFf().mo5608dq().mo3214p(f7593RH);
    }

    /* renamed from: wH */
    private MissionTriggerType m32024wH() {
        return (MissionTriggerType) bFf().mo5608dq().mo3214p(f7595RJ);
    }

    /* renamed from: wI */
    private MissionBeaconType m32025wI() {
        return (MissionBeaconType) bFf().mo5608dq().mo3214p(f7597RL);
    }

    /* renamed from: wJ */
    private CorporationDefaults m32026wJ() {
        return (CorporationDefaults) bFf().mo5608dq().mo3214p(f7599RN);
    }

    /* renamed from: wK */
    private OutpostDefaults m32027wK() {
        return (OutpostDefaults) bFf().mo5608dq().mo3214p(f7601RQ);
    }

    /* renamed from: wL */
    private CloningDefaults m32028wL() {
        return (CloningDefaults) bFf().mo5608dq().mo3214p(f7603RS);
    }

    /* renamed from: wM */
    private InsuranceDefaults m32029wM() {
        return (InsuranceDefaults) bFf().mo5608dq().mo3214p(f7605RU);
    }

    /* renamed from: wN */
    private ItemBillingDefaults m32030wN() {
        return (ItemBillingDefaults) bFf().mo5608dq().mo3214p(f7607RW);
    }

    /* renamed from: wO */
    private ItemDurabilityDefaults m32031wO() {
        return (ItemDurabilityDefaults) bFf().mo5608dq().mo3214p(f7609RY);
    }

    /* renamed from: wP */
    private PdaDefaults m32032wP() {
        return (PdaDefaults) bFf().mo5608dq().mo3214p(f7632Sa);
    }

    /* renamed from: wQ */
    private Map3DDefaults m32033wQ() {
        return (Map3DDefaults) bFf().mo5608dq().mo3214p(f7634Sc);
    }

    /* renamed from: wR */
    private ChatDefaults m32034wR() {
        return (ChatDefaults) bFf().mo5608dq().mo3214p(f7636Se);
    }

    /* renamed from: wS */
    private NurseryDefaults m32035wS() {
        return (NurseryDefaults) bFf().mo5608dq().mo3214p(f7638Sg);
    }

    /* renamed from: wT */
    private ShipDefaults m32036wT() {
        return (ShipDefaults) bFf().mo5608dq().mo3214p(f7640Si);
    }

    /* renamed from: wU */
    private SurfaceDefaults m32037wU() {
        return (SurfaceDefaults) bFf().mo5608dq().mo3214p(f7642Sk);
    }

    /* renamed from: wV */
    private float m32038wV() {
        return bFf().mo5608dq().mo3211m(f7644Sm);
    }

    /* renamed from: wW */
    private Asset m32039wW() {
        return (Asset) bFf().mo5608dq().mo3214p(f7646So);
    }

    /* renamed from: wX */
    private float m32040wX() {
        return bFf().mo5608dq().mo3211m(f7648Sq);
    }

    /* renamed from: T */
    public C6494any mo11T() {
        return ___iScriptClass;
    }

    /* renamed from: U */
    public C2491fm[] mo12U() {
        return _m_methods;
    }

    /* renamed from: W */
    public Object mo13W() {
        return new C4099zS(this);
    }

    /* renamed from: a */
    public Object mo14a(C0495Gr gr) {
        Object[] args = gr.getArgs();
        switch (gr.mo2417hq() - aDJ._m_methodCount) {
            case 0:
                return m32005ap();
            case 1:
                m32015b((UUID) args[0]);
                return null;
            case 2:
                return m32007ar();
            case 3:
                m32020jF();
                return null;
            case 4:
                return m32041wY();
            case 5:
                return m32048xa();
            case 6:
                return m32049xc();
            case 7:
                m32019f((Faction) args[0]);
                return null;
            case 8:
                return m32050xe();
            case 9:
                return m32051xg();
            case 10:
                m32016c((Station) args[0]);
                return null;
            case 11:
                return m32052xi();
            case 12:
                m32012b((MissionTriggerType) args[0]);
                return null;
            case 13:
                return m32053xk();
            case 14:
                m32011b((MissionBeaconType) args[0]);
                return null;
            case 15:
                return m32054xm();
            case 16:
                return m32055xo();
            case 17:
                return new Float(m32056xq());
            case 18:
                m32004an(((Float) args[0]).floatValue());
                return null;
            case 19:
                return m32057xs();
            case 20:
                m32042x((Asset) args[0]);
                return null;
            case 21:
                return m32058xu();
            case 22:
                return m32059xw();
            case 23:
                m32009b((PdaDefaults) args[0]);
                return null;
            case 24:
                return m32060xy();
            case 25:
                return m32043xA();
            case 26:
                m32006ap(((Float) args[0]).floatValue());
                return null;
            case 27:
                return new Float(m32044xC());
            case 28:
                return m32045xE();
            case 29:
                m32013b((NurseryDefaults) args[0]);
                return null;
            case 30:
                return m32046xG();
            case 31:
                m32010b((ShipDefaults) args[0]);
                return null;
            case 32:
                return m32047xI();
            case 33:
                m32014b((SurfaceDefaults) args[0]);
                return null;
            default:
                return super.mo14a(gr);
        }
    }

    /* renamed from: a */
    public void mo15a(Collection collection, C0495Gr gr) {
        super.mo15a(collection, gr);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Global storage volume for all players")
    @C5566aOg
    /* renamed from: ao */
    public void mo18992ao(float f) {
        switch (bFf().mo6893i(f7616SF)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f7616SF, new Object[]{new Float(f)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f7616SF, new Object[]{new Float(f)}));
                break;
        }
        m32004an(f);
    }

    /* renamed from: aq */
    public UUID mo18aq() {
        switch (bFf().mo6893i(f7660bN)) {
            case 0:
                return null;
            case 2:
                return (UUID) bFf().mo5606d(new aCE(this, f7660bN, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f7660bN, new Object[0]));
                break;
        }
        return m32005ap();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Undocked Trade Range Limit")
    @C5566aOg
    /* renamed from: aq */
    public void mo18993aq(float f) {
        switch (bFf().mo6893i(f7624SN)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f7624SN, new Object[]{new Float(f)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f7624SN, new Object[]{new Float(f)}));
                break;
        }
        m32006ap(f);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "PDA Defaults")
    @C5566aOg
    /* renamed from: c */
    public void mo18994c(PdaDefaults abi) {
        switch (bFf().mo6893i(f7621SK)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f7621SK, new Object[]{abi}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f7621SK, new Object[]{abi}));
                break;
        }
        m32009b(abi);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Ship Defaults")
    @C5566aOg
    /* renamed from: c */
    public void mo18995c(ShipDefaults adv) {
        switch (bFf().mo6893i(f7629SS)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f7629SS, new Object[]{adv}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f7629SS, new Object[]{adv}));
                break;
        }
        m32010b(adv);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Mission Beacon Type")
    @C5566aOg
    /* renamed from: c */
    public void mo18996c(MissionBeaconType afd) {
        switch (bFf().mo6893i(f7612SB)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f7612SB, new Object[]{afd}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f7612SB, new Object[]{afd}));
                break;
        }
        m32011b(afd);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Waypoint Type")
    @C5566aOg
    /* renamed from: c */
    public void mo18997c(MissionTriggerType alb) {
        switch (bFf().mo6893i(f7657Sz)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f7657Sz, new Object[]{alb}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f7657Sz, new Object[]{alb}));
                break;
        }
        m32012b(alb);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Nursery Defaults")
    @C5566aOg
    /* renamed from: c */
    public void mo18998c(NurseryDefaults ahp) {
        switch (bFf().mo6893i(f7627SQ)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f7627SQ, new Object[]{ahp}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f7627SQ, new Object[]{ahp}));
                break;
        }
        m32013b(ahp);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Surface Defaults")
    @C5566aOg
    /* renamed from: c */
    public void mo18999c(SurfaceDefaults sSVar) {
        switch (bFf().mo6893i(f7631SU)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f7631SU, new Object[]{sSVar}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f7631SU, new Object[]{sSVar}));
                break;
        }
        m32014b(sSVar);
    }

    /* renamed from: c */
    public C5663aRz[] mo25c() {
        return _m_fields;
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Station")
    @C5566aOg
    /* renamed from: d */
    public void mo19000d(Station bf) {
        switch (bFf().mo6893i(f7655Sx)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f7655Sx, new Object[]{bf}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f7655Sx, new Object[]{bf}));
                break;
        }
        m32016c(bf);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Faction")
    @C5566aOg
    /* renamed from: g */
    public void mo19001g(Faction xm) {
        switch (bFf().mo6893i(f7652Su)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f7652Su, new Object[]{xm}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f7652Su, new Object[]{xm}));
                break;
        }
        m32019f(xm);
    }

    public String getHandle() {
        switch (bFf().mo6893i(f7662bP)) {
            case 0:
                return null;
            case 2:
                return (String) bFf().mo5606d(new aCE(this, f7662bP, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f7662bP, new Object[0]));
                break;
        }
        return m32007ar();
    }

    public void init() {
        switch (bFf().mo6893i(f7663yH)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f7663yH, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f7663yH, new Object[0]));
                break;
        }
        m32020jF();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Outpost Defaults")
    /* renamed from: wZ */
    public OutpostDefaults mo19003wZ() {
        switch (bFf().mo6893i(f7649Sr)) {
            case 0:
                return null;
            case 2:
                return (OutpostDefaults) bFf().mo5606d(new aCE(this, f7649Sr, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f7649Sr, new Object[0]));
                break;
        }
        return m32041wY();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Chat Defaults")
    /* renamed from: xB */
    public ChatDefaults mo19004xB() {
        switch (bFf().mo6893i(f7623SM)) {
            case 0:
                return null;
            case 2:
                return (ChatDefaults) bFf().mo5606d(new aCE(this, f7623SM, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f7623SM, new Object[0]));
                break;
        }
        return m32043xA();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Undocked Trade Range Limit")
    /* renamed from: xD */
    public float mo19005xD() {
        switch (bFf().mo6893i(f7625SO)) {
            case 0:
                return 0.0f;
            case 2:
                return ((Float) bFf().mo5606d(new aCE(this, f7625SO, new Object[0]))).floatValue();
            case 3:
                bFf().mo5606d(new aCE(this, f7625SO, new Object[0]));
                break;
        }
        return m32044xC();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Nursery Defaults")
    /* renamed from: xF */
    public NurseryDefaults mo19006xF() {
        switch (bFf().mo6893i(f7626SP)) {
            case 0:
                return null;
            case 2:
                return (NurseryDefaults) bFf().mo5606d(new aCE(this, f7626SP, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f7626SP, new Object[0]));
                break;
        }
        return m32045xE();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Ship Defaults")
    /* renamed from: xH */
    public ShipDefaults mo19007xH() {
        switch (bFf().mo6893i(f7628SR)) {
            case 0:
                return null;
            case 2:
                return (ShipDefaults) bFf().mo5606d(new aCE(this, f7628SR, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f7628SR, new Object[0]));
                break;
        }
        return m32046xG();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Surface Defaults")
    /* renamed from: xJ */
    public SurfaceDefaults mo19008xJ() {
        switch (bFf().mo6893i(f7630ST)) {
            case 0:
                return null;
            case 2:
                return (SurfaceDefaults) bFf().mo5606d(new aCE(this, f7630ST, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f7630ST, new Object[0]));
                break;
        }
        return m32047xI();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Corporation Defaults")
    /* renamed from: xb */
    public CorporationDefaults mo19009xb() {
        switch (bFf().mo6893i(f7650Ss)) {
            case 0:
                return null;
            case 2:
                return (CorporationDefaults) bFf().mo5606d(new aCE(this, f7650Ss, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f7650Ss, new Object[0]));
                break;
        }
        return m32048xa();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Cloning Defaults")
    /* renamed from: xd */
    public CloningDefaults mo19010xd() {
        switch (bFf().mo6893i(f7651St)) {
            case 0:
                return null;
            case 2:
                return (CloningDefaults) bFf().mo5606d(new aCE(this, f7651St, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f7651St, new Object[0]));
                break;
        }
        return m32049xc();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Faction")
    /* renamed from: xf */
    public Faction mo19011xf() {
        switch (bFf().mo6893i(f7653Sv)) {
            case 0:
                return null;
            case 2:
                return (Faction) bFf().mo5606d(new aCE(this, f7653Sv, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f7653Sv, new Object[0]));
                break;
        }
        return m32050xe();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Station")
    /* renamed from: xh */
    public Station mo19012xh() {
        switch (bFf().mo6893i(f7654Sw)) {
            case 0:
                return null;
            case 2:
                return (Station) bFf().mo5606d(new aCE(this, f7654Sw, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f7654Sw, new Object[0]));
                break;
        }
        return m32051xg();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Waypoint Type")
    /* renamed from: xj */
    public MissionTriggerType mo19013xj() {
        switch (bFf().mo6893i(f7656Sy)) {
            case 0:
                return null;
            case 2:
                return (MissionTriggerType) bFf().mo5606d(new aCE(this, f7656Sy, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f7656Sy, new Object[0]));
                break;
        }
        return m32052xi();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Mission Beacon Type")
    /* renamed from: xl */
    public MissionBeaconType mo19014xl() {
        switch (bFf().mo6893i(f7611SA)) {
            case 0:
                return null;
            case 2:
                return (MissionBeaconType) bFf().mo5606d(new aCE(this, f7611SA, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f7611SA, new Object[0]));
                break;
        }
        return m32053xk();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Item Billing Defaults")
    /* renamed from: xn */
    public ItemBillingDefaults mo19015xn() {
        switch (bFf().mo6893i(f7613SC)) {
            case 0:
                return null;
            case 2:
                return (ItemBillingDefaults) bFf().mo5606d(new aCE(this, f7613SC, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f7613SC, new Object[0]));
                break;
        }
        return m32054xm();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Item Durability Defaults")
    /* renamed from: xp */
    public ItemDurabilityDefaults mo19016xp() {
        switch (bFf().mo6893i(f7614SD)) {
            case 0:
                return null;
            case 2:
                return (ItemDurabilityDefaults) bFf().mo5606d(new aCE(this, f7614SD, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f7614SD, new Object[0]));
                break;
        }
        return m32055xo();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Global storage volume for all players")
    /* renamed from: xr */
    public float mo19017xr() {
        switch (bFf().mo6893i(f7615SE)) {
            case 0:
                return 0.0f;
            case 2:
                return ((Float) bFf().mo5606d(new aCE(this, f7615SE, new Object[0]))).floatValue();
            case 3:
                bFf().mo5606d(new aCE(this, f7615SE, new Object[0]));
                break;
        }
        return m32056xq();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Default item icon")
    /* renamed from: xt */
    public Asset mo19018xt() {
        switch (bFf().mo6893i(f7617SG)) {
            case 0:
                return null;
            case 2:
                return (Asset) bFf().mo5606d(new aCE(this, f7617SG, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f7617SG, new Object[0]));
                break;
        }
        return m32057xs();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Insurance Defaults")
    /* renamed from: xv */
    public InsuranceDefaults mo19019xv() {
        switch (bFf().mo6893i(f7619SI)) {
            case 0:
                return null;
            case 2:
                return (InsuranceDefaults) bFf().mo5606d(new aCE(this, f7619SI, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f7619SI, new Object[0]));
                break;
        }
        return m32058xu();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "PDA Defaults")
    /* renamed from: xx */
    public PdaDefaults mo19020xx() {
        switch (bFf().mo6893i(f7620SJ)) {
            case 0:
                return null;
            case 2:
                return (PdaDefaults) bFf().mo5606d(new aCE(this, f7620SJ, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f7620SJ, new Object[0]));
                break;
        }
        return m32059xw();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Map 3D Defaults")
    /* renamed from: xz */
    public Map3DDefaults mo19021xz() {
        switch (bFf().mo6893i(f7622SL)) {
            case 0:
                return null;
            case 2:
                return (Map3DDefaults) bFf().mo5606d(new aCE(this, f7622SL, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f7622SL, new Object[0]));
                break;
        }
        return m32060xy();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Default item icon")
    /* renamed from: y */
    public void mo19022y(Asset tCVar) {
        switch (bFf().mo6893i(f7618SH)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f7618SH, new Object[]{tCVar}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f7618SH, new Object[]{tCVar}));
                break;
        }
        m32042x(tCVar);
    }

    /* renamed from: S */
    public void mo10S() {
        super.mo10S();
    }

    @C0064Am(aul = "cea14c3d034ae0ad9a4263fa1bb6a99c", aum = 0)
    /* renamed from: ap */
    private UUID m32005ap() {
        return m32003an();
    }

    @C0064Am(aul = "94d05660d423ec69679fab4a909aedd7", aum = 0)
    /* renamed from: b */
    private void m32015b(UUID uuid) {
        m32000a(uuid);
    }

    @C0064Am(aul = "2bf7e377177da296dc1083846d5d6adb", aum = 0)
    /* renamed from: ar */
    private String m32007ar() {
        return "taikodom_default_contents";
    }

    @C0064Am(aul = "c3b3871a05304c1b3b7e57375aa12f5e", aum = 0)
    /* renamed from: jF */
    private void m32020jF() {
        if (m32026wJ() == null) {
            CorporationDefaults ab = (CorporationDefaults) bFf().mo6865M(CorporationDefaults.class);
            ab.mo10S();
            m31986a(ab);
        }
        if (m32028wL() == null) {
            CloningDefaults ads = (CloningDefaults) bFf().mo6865M(CloningDefaults.class);
            ads.mo10S();
            m31994a(ads);
        }
        if (m32027wK() == null) {
            OutpostDefaults acu = (OutpostDefaults) bFf().mo6865M(OutpostDefaults.class);
            acu.mo10S();
            m31989a(acu);
        }
        if (m32029wM() == null) {
            InsuranceDefaults ah = (InsuranceDefaults) bFf().mo6865M(InsuranceDefaults.class);
            ah.mo10S();
            m31987a(ah);
        }
        if (m32030wN() == null) {
            ItemBillingDefaults arx = (ItemBillingDefaults) bFf().mo6865M(ItemBillingDefaults.class);
            arx.mo10S();
            m31997a(arx);
        }
        if (m32031wO() == null) {
            ItemDurabilityDefaults amn = (ItemDurabilityDefaults) bFf().mo6865M(ItemDurabilityDefaults.class);
            amn.mo10S();
            m31996a(amn);
        }
        if (m32032wP() == null) {
            PdaDefaults abi = (PdaDefaults) bFf().mo6865M(PdaDefaults.class);
            abi.mo10S();
            m31988a(abi);
        }
        if (m32033wQ() == null) {
            Map3DDefaults abb = (Map3DDefaults) bFf().mo6865M(Map3DDefaults.class);
            abb.mo10S();
            m31993a(abb);
        }
        if (m32034wR() == null) {
            ChatDefaults khVar = (ChatDefaults) bFf().mo6865M(ChatDefaults.class);
            khVar.mo10S();
            m31998a(khVar);
        }
        if (m32035wS() == null) {
            NurseryDefaults ahp = (NurseryDefaults) bFf().mo6865M(NurseryDefaults.class);
            ahp.mo10S();
            m31995a(ahp);
        }
        if (m32036wT() == null) {
            ShipDefaults adv = (ShipDefaults) bFf().mo6865M(ShipDefaults.class);
            adv.mo10S();
            m31990a(adv);
        }
        if (m32037wU() == null) {
            SurfaceDefaults sSVar = (SurfaceDefaults) bFf().mo6865M(SurfaceDefaults.class);
            sSVar.mo10S();
            m31999a(sSVar);
        }
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Outpost Defaults")
    @C0064Am(aul = "ba6451759500386b93f89f3c067e9927", aum = 0)
    /* renamed from: wY */
    private OutpostDefaults m32041wY() {
        return m32027wK();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Corporation Defaults")
    @C0064Am(aul = "98c559feb9dafd145e2745dfa6383f8e", aum = 0)
    /* renamed from: xa */
    private CorporationDefaults m32048xa() {
        return m32026wJ();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Cloning Defaults")
    @C0064Am(aul = "51565305a919d916ab4d54a28963b8df", aum = 0)
    /* renamed from: xc */
    private CloningDefaults m32049xc() {
        return m32028wL();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Faction")
    @C0064Am(aul = "575a78e26c1e273345b7d26fe99dda91", aum = 0)
    /* renamed from: xe */
    private Faction m32050xe() {
        return m32022wF();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Station")
    @C0064Am(aul = "af42d24a19de181b3bb32ab13693bd48", aum = 0)
    /* renamed from: xg */
    private Station m32051xg() {
        return m32023wG();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Waypoint Type")
    @C0064Am(aul = "1aeac7585e1c052e631762d2574d1aff", aum = 0)
    /* renamed from: xi */
    private MissionTriggerType m32052xi() {
        return m32024wH();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Mission Beacon Type")
    @C0064Am(aul = "8f7bf17645bc002c58e19a3665c01d33", aum = 0)
    /* renamed from: xk */
    private MissionBeaconType m32053xk() {
        return m32025wI();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Item Billing Defaults")
    @C0064Am(aul = "0161af5ef645a1825d5d4e9eb1ace198", aum = 0)
    /* renamed from: xm */
    private ItemBillingDefaults m32054xm() {
        return m32030wN();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Item Durability Defaults")
    @C0064Am(aul = "667eeb9f2e80df14bfa39c7c7f097a2e", aum = 0)
    /* renamed from: xo */
    private ItemDurabilityDefaults m32055xo() {
        return m32031wO();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Global storage volume for all players")
    @C0064Am(aul = "b0f96b8647bfd26cb6d083c156217a37", aum = 0)
    /* renamed from: xq */
    private float m32056xq() {
        return m32038wV();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Default item icon")
    @C0064Am(aul = "4c673ed1c556a83f10082b006f7e757e", aum = 0)
    /* renamed from: xs */
    private Asset m32057xs() {
        return m32039wW();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Default item icon")
    @C0064Am(aul = "2fb8b6492f23a63d028a4de179f77d88", aum = 0)
    /* renamed from: x */
    private void m32042x(Asset tCVar) {
        m32021w(tCVar);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Insurance Defaults")
    @C0064Am(aul = "50ebe1eea259606eb9572b1a837cded0", aum = 0)
    /* renamed from: xu */
    private InsuranceDefaults m32058xu() {
        return m32029wM();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "PDA Defaults")
    @C0064Am(aul = "0015d112716cc22280f0b09c18cdefca", aum = 0)
    /* renamed from: xw */
    private PdaDefaults m32059xw() {
        return m32032wP();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Map 3D Defaults")
    @C0064Am(aul = "106d2fc65275fdea7ca9068e44dad2e7", aum = 0)
    /* renamed from: xy */
    private Map3DDefaults m32060xy() {
        return m32033wQ();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Chat Defaults")
    @C0064Am(aul = "fa2428ef4c1347c67f25d1edad509069", aum = 0)
    /* renamed from: xA */
    private ChatDefaults m32043xA() {
        return m32034wR();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Undocked Trade Range Limit")
    @C0064Am(aul = "1cb90593d76bd26ccd9cfb72e3c5caf6", aum = 0)
    /* renamed from: xC */
    private float m32044xC() {
        return m32040wX();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Nursery Defaults")
    @C0064Am(aul = "1e5ed870b182910c1ec7e39e92e039a3", aum = 0)
    /* renamed from: xE */
    private NurseryDefaults m32045xE() {
        return m32035wS();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Ship Defaults")
    @C0064Am(aul = "3839c83bba0f5f34bfd35fcfe682d43e", aum = 0)
    /* renamed from: xG */
    private ShipDefaults m32046xG() {
        return m32036wT();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Surface Defaults")
    @C0064Am(aul = "7b8e31027f8c51bff7f0d28eff7070b6", aum = 0)
    /* renamed from: xI */
    private SurfaceDefaults m32047xI() {
        return m32037wU();
    }
}
