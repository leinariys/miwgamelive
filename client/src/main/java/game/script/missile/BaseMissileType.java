package game.script.missile;

import game.network.message.externalizable.aCE;
import game.script.item.Clip;
import game.script.item.ClipType;
import logic.baa.*;
import logic.data.mbean.C3050nT;
import logic.res.code.C5663aRz;
import logic.res.code.C6494any;
import logic.res.html.C2491fm;
import p001a.*;

import java.util.Collection;

@C5511aMd
@C6485anp
/* renamed from: a.Zj */
/* compiled from: a */
public abstract class BaseMissileType extends ClipType implements C1616Xf {
    public static final int _m_fieldCount = 0;
    public static final C5663aRz[] _m_fields = null;
    public static final int _m_methodCount = 0;
    public static final C2491fm[] _m_methods = null;
    public static final C2491fm aDS = null;
    public static final C2491fm eNq = null;
    public static final long serialVersionUID = 0;
    public static C6494any ___iScriptClass;

    static {
        m12166V();
    }

    public BaseMissileType() {
        super((C5540aNg) null);
        super.mo10S();
    }

    public BaseMissileType(C5540aNg ang) {
        super(ang);
    }

    /* renamed from: V */
    static void m12166V() {
        serialVersionUID = (long) 1;
        _m_fieldCount = ClipType._m_fieldCount + 0;
        _m_methodCount = ClipType._m_methodCount + 2;
        _m_fields = new C5663aRz[(ClipType._m_fieldCount + 0)];
        C1634Xv.m11725a((Object[]) ClipType._m_fields, (Object[]) _m_fields);
        int i = ClipType._m_methodCount;
        C2491fm[] fmVarArr = new C2491fm[(i + 2)];
        C2491fm a = C4105zY.m41624a(BaseMissileType.class, "4e8b3d192cf802dd3ee178abe9a429e1", i);
        eNq = a;
        fmVarArr[i] = a;
        int i2 = i + 1;
        C2491fm a2 = C4105zY.m41624a(BaseMissileType.class, "223e9495130b210dd8640ed21e52d436", i2);
        aDS = a2;
        fmVarArr[i2] = a2;
        int i3 = i2 + 1;
        _m_methods = fmVarArr;
        C1634Xv.m11725a((Object[]) ClipType._m_methods, (Object[]) _m_methods);
        ___iScriptClass = aUO.m18566a(BaseMissileType.class, C3050nT.class, _m_fields, _m_methods);
    }

    /* renamed from: NK */
    public <T extends aDJ> T mo7459NK() {
        switch (bFf().mo6893i(aDS)) {
            case 0:
                return null;
            case 2:
                return (aDJ) bFf().mo5606d(new aCE(this, aDS, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, aDS, new Object[0]));
                break;
        }
        return m12165NJ();
    }

    /* renamed from: T */
    public C6494any mo11T() {
        return ___iScriptClass;
    }

    /* renamed from: U */
    public C2491fm[] mo12U() {
        return _m_methods;
    }

    /* renamed from: W */
    public Object mo13W() {
        return new C3050nT(this);
    }

    /* renamed from: a */
    public Object mo14a(C0495Gr gr) {
        gr.getArgs();
        switch (gr.mo2417hq() - ClipType._m_methodCount) {
            case 0:
                return bIl();
            case 1:
                return m12165NJ();
            default:
                return super.mo14a(gr);
        }
    }

    /* renamed from: a */
    public void mo15a(Collection collection, C0495Gr gr) {
        super.mo15a(collection, gr);
    }

    public Clip bIm() {
        switch (bFf().mo6893i(eNq)) {
            case 0:
                return null;
            case 2:
                return (Clip) bFf().mo5606d(new aCE(this, eNq, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, eNq, new Object[0]));
                break;
        }
        return bIl();
    }

    /* renamed from: c */
    public C5663aRz[] mo25c() {
        return _m_fields;
    }

    /* renamed from: S */
    public void mo10S() {
        super.mo10S();
    }

    @C0064Am(aul = "4e8b3d192cf802dd3ee178abe9a429e1", aum = 0)
    private Clip bIl() {
        Clip vo = (Clip) bFf().mo6865M(Clip.class);
        vo.mo6422l(this);
        return vo;
    }

    @C0064Am(aul = "223e9495130b210dd8640ed21e52d436", aum = 0)
    /* renamed from: NJ */
    private <T extends aDJ> T m12165NJ() {
        T t = (Clip) bFf().mo6865M(Clip.class);
        t.mo6422l(this);
        return t;
    }
}
