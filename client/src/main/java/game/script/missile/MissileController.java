package game.script.missile;

import game.network.message.externalizable.aCE;
import game.script.ai.npc.TrackShipAIController;
import logic.abb.C0573Hw;
import logic.abb.C0739KY;
import logic.abb.C6677arZ;
import logic.baa.C1616Xf;
import logic.baa.C1634Xv;
import logic.baa.C5511aMd;
import logic.baa.C6485anp;
import logic.data.mbean.C1050PO;
import logic.res.code.C5640aRc;
import logic.res.code.C5663aRz;
import logic.res.code.C6494any;
import logic.res.html.C2491fm;
import p001a.*;

import java.util.Collection;

@C5511aMd
@C6485anp
/* renamed from: a.is */
/* compiled from: a */
public class MissileController extends TrackShipAIController implements C1616Xf {
    /* renamed from: Yf */
    public static final C5663aRz f8274Yf = null;
    /* renamed from: Yg */
    public static final C2491fm f8275Yg = null;
    /* renamed from: Yh */
    public static final C2491fm f8276Yh = null;
    /* renamed from: Yi */
    public static final C2491fm f8277Yi = null;
    /* renamed from: Yj */
    public static final C2491fm f8278Yj = null;
    public static final C2491fm _f_changeCurrentAction_0020_0028_0029I = null;
    public static final C2491fm _f_setupBehaviours_0020_0028_0029V = null;
    public static final C5663aRz _f_startTime = null;
    public static final C2491fm _f_start_0020_0028_0029V = null;
    public static final int _m_fieldCount = 0;
    public static final C5663aRz[] _m_fields = null;
    public static final int _m_methodCount = 0;
    public static final C2491fm[] _m_methods = null;
    public static final long serialVersionUID = 0;
    public static final int WAITING = 0;
    /* renamed from: Yb */
    public static final int f8270Yb = 3;
    /* renamed from: Yc */
    public static final int f8271Yc = 1;
    /* renamed from: Yd */
    public static final int f8272Yd = 2;
    public static C6494any ___iScriptClass;
    @C0064Am(aul = "b98e8b9031abca4f0b8efce3d3b0eef6", aum = 1)

    /* renamed from: Ye */
    private static float f8273Ye;
    @C0064Am(aul = "b9b09ba16c4b616112073d683c453b8e", aum = 0)
    private static long startTime;

    static {
        m33595V();
    }

    public MissileController() {
        super((C5540aNg) null);
        super.mo10S();
    }

    public MissileController(C5540aNg ang) {
        super(ang);
    }

    /* renamed from: V */
    static void m33595V() {
        serialVersionUID = (long) 1;
        _m_fieldCount = TrackShipAIController._m_fieldCount + 2;
        _m_methodCount = TrackShipAIController._m_methodCount + 7;
        int i = TrackShipAIController._m_fieldCount;
        C5663aRz[] arzArr = new C5663aRz[(i + 2)];
        C5663aRz b = C5640aRc.m17844b(MissileController.class, "b9b09ba16c4b616112073d683c453b8e", i);
        _f_startTime = b;
        arzArr[i] = b;
        int i2 = i + 1;
        C5663aRz b2 = C5640aRc.m17844b(MissileController.class, "b98e8b9031abca4f0b8efce3d3b0eef6", i2);
        f8274Yf = b2;
        arzArr[i2] = b2;
        int i3 = i2 + 1;
        _m_fields = arzArr;
        C1634Xv.m11725a((Object[]) TrackShipAIController._m_fields, (Object[]) _m_fields);
        int i4 = TrackShipAIController._m_methodCount;
        C2491fm[] fmVarArr = new C2491fm[(i4 + 7)];
        C2491fm a = C4105zY.m41624a(MissileController.class, "07701644dc77dfee4939dc230da7869b", i4);
        _f_start_0020_0028_0029V = a;
        fmVarArr[i4] = a;
        int i5 = i4 + 1;
        C2491fm a2 = C4105zY.m41624a(MissileController.class, "1b9a3663f98058abb47e172c5af4bb10", i5);
        _f_setupBehaviours_0020_0028_0029V = a2;
        fmVarArr[i5] = a2;
        int i6 = i5 + 1;
        C2491fm a3 = C4105zY.m41624a(MissileController.class, "655a2836afb9963dc1b25c0c1354cf07", i6);
        _f_changeCurrentAction_0020_0028_0029I = a3;
        fmVarArr[i6] = a3;
        int i7 = i6 + 1;
        C2491fm a4 = C4105zY.m41624a(MissileController.class, "e70ea58564ac37a50c77e95e419f76bb", i7);
        f8275Yg = a4;
        fmVarArr[i7] = a4;
        int i8 = i7 + 1;
        C2491fm a5 = C4105zY.m41624a(MissileController.class, "60d08937feb34eca68a199a7ac7bb711", i8);
        f8276Yh = a5;
        fmVarArr[i8] = a5;
        int i9 = i8 + 1;
        C2491fm a6 = C4105zY.m41624a(MissileController.class, "fff000e0ed990babda7ad12a96bd2bd4", i9);
        f8277Yi = a6;
        fmVarArr[i9] = a6;
        int i10 = i9 + 1;
        C2491fm a7 = C4105zY.m41624a(MissileController.class, "024ef98f9bb42574ee81ddce61f7d4c9", i10);
        f8278Yj = a7;
        fmVarArr[i10] = a7;
        int i11 = i10 + 1;
        _m_methods = fmVarArr;
        C1634Xv.m11725a((Object[]) TrackShipAIController._m_methods, (Object[]) _m_methods);
        ___iScriptClass = aUO.m18566a(MissileController.class, C1050PO.class, _m_fields, _m_methods);
    }

    /* renamed from: Bk */
    private long m33590Bk() {
        return bFf().mo5608dq().mo3213o(_f_startTime);
    }

    /* renamed from: Bl */
    private float m33591Bl() {
        return bFf().mo5608dq().mo3211m(f8274Yf);
    }

    /* renamed from: aG */
    private void m33596aG(float f) {
        bFf().mo5608dq().mo3150a(f8274Yf, f);
    }

    @C0064Am(aul = "024ef98f9bb42574ee81ddce61f7d4c9", aum = 0)
    @C5566aOg
    /* renamed from: aH */
    private void m33597aH(float f) {
        throw new aWi(new aCE(this, f8278Yj, new Object[]{new Float(f)}));
    }

    /* renamed from: bz */
    private void m33598bz(long j) {
        bFf().mo5608dq().mo3184b(_f_startTime, j);
    }

    @C0064Am(aul = "07701644dc77dfee4939dc230da7869b", aum = 0)
    @C5566aOg
    /* renamed from: qN */
    private void m33599qN() {
        throw new aWi(new aCE(this, _f_start_0020_0028_0029V, new Object[0]));
    }

    /* renamed from: Bn */
    public Missile mo19781Bn() {
        switch (bFf().mo6893i(f8275Yg)) {
            case 0:
                return null;
            case 2:
                return (Missile) bFf().mo5606d(new aCE(this, f8275Yg, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f8275Yg, new Object[0]));
                break;
        }
        return m33592Bm();
    }

    /* renamed from: Bp */
    public MissileType mo19782Bp() {
        switch (bFf().mo6893i(f8276Yh)) {
            case 0:
                return null;
            case 2:
                return (MissileType) bFf().mo5606d(new aCE(this, f8276Yh, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f8276Yh, new Object[0]));
                break;
        }
        return m33593Bo();
    }

    /* renamed from: Br */
    public float mo19783Br() {
        switch (bFf().mo6893i(f8277Yi)) {
            case 0:
                return 0.0f;
            case 2:
                return ((Float) bFf().mo5606d(new aCE(this, f8277Yi, new Object[0]))).floatValue();
            case 3:
                bFf().mo5606d(new aCE(this, f8277Yi, new Object[0]));
                break;
        }
        return m33594Bq();
    }

    /* renamed from: T */
    public C6494any mo11T() {
        return ___iScriptClass;
    }

    /* renamed from: U */
    public C2491fm[] mo12U() {
        return _m_methods;
    }

    /* renamed from: W */
    public Object mo13W() {
        return new C1050PO(this);
    }

    /* renamed from: a */
    public Object mo14a(C0495Gr gr) {
        Object[] args = gr.getArgs();
        switch (gr.mo2417hq() - TrackShipAIController._m_methodCount) {
            case 0:
                m33599qN();
                return null;
            case 1:
                m33600zP();
                return null;
            case 2:
                return new Integer(m33601zT());
            case 3:
                return m33592Bm();
            case 4:
                return m33593Bo();
            case 5:
                return new Float(m33594Bq());
            case 6:
                m33597aH(((Float) args[0]).floatValue());
                return null;
            default:
                return super.mo14a(gr);
        }
    }

    /* renamed from: a */
    public void mo15a(Collection collection, C0495Gr gr) {
        super.mo15a(collection, gr);
    }

    @C5566aOg
    /* renamed from: aI */
    public void mo19784aI(float f) {
        switch (bFf().mo6893i(f8278Yj)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f8278Yj, new Object[]{new Float(f)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f8278Yj, new Object[]{new Float(f)}));
                break;
        }
        m33597aH(f);
    }

    /* renamed from: c */
    public C5663aRz[] mo25c() {
        return _m_fields;
    }

    @C5566aOg
    public void start() {
        switch (bFf().mo6893i(_f_start_0020_0028_0029V)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, _f_start_0020_0028_0029V, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, _f_start_0020_0028_0029V, new Object[0]));
                break;
        }
        m33599qN();
    }

    /* access modifiers changed from: protected */
    /* renamed from: zQ */
    public void mo2800zQ() {
        switch (bFf().mo6893i(_f_setupBehaviours_0020_0028_0029V)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, _f_setupBehaviours_0020_0028_0029V, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, _f_setupBehaviours_0020_0028_0029V, new Object[0]));
                break;
        }
        m33600zP();
    }

    /* access modifiers changed from: protected */
    /* renamed from: zU */
    public int mo2801zU() {
        switch (bFf().mo6893i(_f_changeCurrentAction_0020_0028_0029I)) {
            case 0:
                return 0;
            case 2:
                return ((Integer) bFf().mo5606d(new aCE(this, _f_changeCurrentAction_0020_0028_0029I, new Object[0]))).intValue();
            case 3:
                bFf().mo5606d(new aCE(this, _f_changeCurrentAction_0020_0028_0029I, new Object[0]));
                break;
        }
        return m33601zT();
    }

    /* renamed from: S */
    public void mo10S() {
        super.mo10S();
        m33596aG(0.0f);
    }

    @C0064Am(aul = "1b9a3663f98058abb47e172c5af4bb10", aum = 0)
    /* renamed from: zP */
    private void m33600zP() {
        mo3328ld(3);
        mo3327lb(0);
        mo3317b(0, new C0573Hw[0]);
        mo3317b(1, new C0739KY(bUT(), true));
        mo3317b(2, new C6677arZ());
    }

    @C0064Am(aul = "655a2836afb9963dc1b25c0c1354cf07", aum = 0)
    /* renamed from: zT */
    private int m33601zT() {
        switch (aYs()) {
            case 0:
                if (((float) (currentTimeMillis() - m33590Bk())) / 1000.0f > m33591Bl()) {
                    return 1;
                }
                break;
            case 1:
                if (!bnJ()) {
                    return 2;
                }
                break;
        }
        return -1;
    }

    @C0064Am(aul = "e70ea58564ac37a50c77e95e419f76bb", aum = 0)
    /* renamed from: Bm */
    private Missile m33592Bm() {
        return (Missile) aYa();
    }

    @C0064Am(aul = "60d08937feb34eca68a199a7ac7bb711", aum = 0)
    /* renamed from: Bo */
    private MissileType m33593Bo() {
        return mo19781Bn().cXS();
    }

    @C0064Am(aul = "fff000e0ed990babda7ad12a96bd2bd4", aum = 0)
    /* renamed from: Bq */
    private float m33594Bq() {
        return m33591Bl();
    }
}
