package game.script.missile;

import game.network.message.externalizable.aCE;
import game.script.item.ProjectileWeaponType;
import logic.baa.*;
import logic.data.mbean.C6057afd;
import logic.res.code.C5640aRc;
import logic.res.code.C5663aRz;
import logic.res.code.C6494any;
import logic.res.html.C2491fm;
import p001a.*;

import java.util.Collection;

@C0566Hp
@C5829abJ("2.1.1")
@C6485anp
@C5511aMd
/* renamed from: a.aTi  reason: case insensitive filesystem */
/* compiled from: a */
public class MissileWeaponType extends ProjectileWeaponType implements C1616Xf {
    public static final int _m_fieldCount = 0;
    public static final C5663aRz[] _m_fields = null;
    public static final int _m_methodCount = 0;
    public static final C2491fm[] _m_methods = null;
    public static final C2491fm clA = null;
    public static final C5663aRz clp = null;
    /* renamed from: dN */
    public static final C2491fm f3815dN = null;
    public static final C2491fm iOt = null;
    public static final C2491fm iOu = null;
    public static final long serialVersionUID = 0;
    public static C6494any ___iScriptClass;
    @C0064Am(aul = "27b2dbe95378a426f009c75093548f9b", aum = 0)
    private static long clo;

    static {
        m18367V();
    }

    public MissileWeaponType() {
        super((C5540aNg) null);
        super.mo10S();
    }

    public MissileWeaponType(C5540aNg ang) {
        super(ang);
    }

    /* renamed from: V */
    static void m18367V() {
        serialVersionUID = (long) 1;
        _m_fieldCount = ProjectileWeaponType._m_fieldCount + 1;
        _m_methodCount = ProjectileWeaponType._m_methodCount + 4;
        int i = ProjectileWeaponType._m_fieldCount;
        C5663aRz[] arzArr = new C5663aRz[(i + 1)];
        C5663aRz b = C5640aRc.m17844b(MissileWeaponType.class, "27b2dbe95378a426f009c75093548f9b", i);
        clp = b;
        arzArr[i] = b;
        int i2 = i + 1;
        _m_fields = arzArr;
        C1634Xv.m11725a((Object[]) ProjectileWeaponType._m_fields, (Object[]) _m_fields);
        int i3 = ProjectileWeaponType._m_methodCount;
        C2491fm[] fmVarArr = new C2491fm[(i3 + 4)];
        C2491fm a = C4105zY.m41624a(MissileWeaponType.class, "db3d2e221003e72cd24be6f526740f99", i3);
        clA = a;
        fmVarArr[i3] = a;
        int i4 = i3 + 1;
        C2491fm a2 = C4105zY.m41624a(MissileWeaponType.class, "7e22c6b20ae527713e7a7e577b5dafbe", i4);
        iOt = a2;
        fmVarArr[i4] = a2;
        int i5 = i4 + 1;
        C2491fm a3 = C4105zY.m41624a(MissileWeaponType.class, "ae2914d8172e2e809f56fc61c1da2c88", i5);
        iOu = a3;
        fmVarArr[i5] = a3;
        int i6 = i5 + 1;
        C2491fm a4 = C4105zY.m41624a(MissileWeaponType.class, "5dbfe4ef8f3d5c03be34762627df1354", i6);
        f3815dN = a4;
        fmVarArr[i6] = a4;
        int i7 = i6 + 1;
        _m_methods = fmVarArr;
        C1634Xv.m11725a((Object[]) ProjectileWeaponType._m_methods, (Object[]) _m_methods);
        ___iScriptClass = aUO.m18566a(MissileWeaponType.class, C6057afd.class, _m_fields, _m_methods);
    }

    private long ayr() {
        return bFf().mo5608dq().mo3213o(clp);
    }

    /* renamed from: dY */
    private void m18369dY(long j) {
        bFf().mo5608dq().mo3184b(clp, j);
    }

    /* renamed from: T */
    public C6494any mo11T() {
        return ___iScriptClass;
    }

    /* renamed from: U */
    public C2491fm[] mo12U() {
        return _m_methods;
    }

    /* renamed from: W */
    public Object mo13W() {
        return new C6057afd(this);
    }

    /* renamed from: a */
    public Object mo14a(C0495Gr gr) {
        Object[] args = gr.getArgs();
        switch (gr.mo2417hq() - ProjectileWeaponType._m_methodCount) {
            case 0:
                return new Long(ayC());
            case 1:
                m18370lW(((Long) args[0]).longValue());
                return null;
            case 2:
                return dvD();
            case 3:
                return m18368aT();
            default:
                return super.mo14a(gr);
        }
    }

    /* renamed from: a */
    public void mo15a(Collection collection, C0495Gr gr) {
        super.mo15a(collection, gr);
    }

    /* access modifiers changed from: protected */
    /* renamed from: aU */
    public <T extends aDJ> T mo745aU() {
        switch (bFf().mo6893i(f3815dN)) {
            case 0:
                return null;
            case 2:
                return (aDJ) bFf().mo5606d(new aCE(this, f3815dN, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f3815dN, new Object[0]));
                break;
        }
        return m18368aT();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Lock Time (s)")
    public long ayD() {
        switch (bFf().mo6893i(clA)) {
            case 0:
                return 0;
            case 2:
                return ((Long) bFf().mo5606d(new aCE(this, clA, new Object[0]))).longValue();
            case 3:
                bFf().mo5606d(new aCE(this, clA, new Object[0]));
                break;
        }
        return ayC();
    }

    /* renamed from: c */
    public C5663aRz[] mo25c() {
        return _m_fields;
    }

    public MissileWeapon dvE() {
        switch (bFf().mo6893i(iOu)) {
            case 0:
                return null;
            case 2:
                return (MissileWeapon) bFf().mo5606d(new aCE(this, iOu, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, iOu, new Object[0]));
                break;
        }
        return dvD();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Lock Time (s)")
    /* renamed from: lX */
    public void mo11523lX(long j) {
        switch (bFf().mo6893i(iOt)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, iOt, new Object[]{new Long(j)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, iOt, new Object[]{new Long(j)}));
                break;
        }
        m18370lW(j);
    }

    /* renamed from: S */
    public void mo10S() {
        super.mo10S();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Lock Time (s)")
    @C0064Am(aul = "db3d2e221003e72cd24be6f526740f99", aum = 0)
    private long ayC() {
        return ayr();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Lock Time (s)")
    @C0064Am(aul = "7e22c6b20ae527713e7a7e577b5dafbe", aum = 0)
    /* renamed from: lW */
    private void m18370lW(long j) {
        m18369dY(j);
    }

    @C0064Am(aul = "ae2914d8172e2e809f56fc61c1da2c88", aum = 0)
    private MissileWeapon dvD() {
        return (MissileWeapon) mo745aU();
    }

    @C0064Am(aul = "5dbfe4ef8f3d5c03be34762627df1354", aum = 0)
    /* renamed from: aT */
    private <T extends aDJ> T m18368aT() {
        T t = (MissileWeapon) bFf().mo6865M(MissileWeapon.class);
        t.mo823a(this);
        return t;
    }
}
