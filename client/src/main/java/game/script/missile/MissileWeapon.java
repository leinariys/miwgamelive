package game.script.missile;

import com.hoplon.geometry.Vec3f;
import game.geometry.Vec3d;
import game.network.message.externalizable.aCE;
import game.script.item.ProjectileWeapon;
import game.script.item.ProjectileWeaponType;
import game.script.ship.Ship;
import logic.baa.C1616Xf;
import logic.baa.C1634Xv;
import logic.baa.C5511aMd;
import logic.baa.C6485anp;
import logic.data.mbean.C6589app;
import logic.res.code.C5640aRc;
import logic.res.code.C5663aRz;
import logic.res.code.C6494any;
import logic.res.html.C2491fm;
import p001a.*;

import java.util.Collection;

@C5829abJ("2.1.1")
@C6485anp
@C2712iu(version = "2.1.1")
@C5511aMd
/* renamed from: a.Bp */
/* compiled from: a */
public class MissileWeapon extends ProjectileWeapon implements C1616Xf {
    public static final int _m_fieldCount = 0;
    public static final C5663aRz[] _m_fields = null;
    public static final int _m_methodCount = 0;
    public static final C2491fm[] _m_methods = null;
    public static final C2491fm clA = null;
    public static final C2491fm clB = null;
    public static final C2491fm clC = null;
    public static final C2491fm clD = null;
    public static final C2491fm clE = null;
    public static final C5663aRz clp = null;
    public static final C5663aRz clr = null;
    public static final C5663aRz clt = null;
    public static final C2491fm clu = null;
    public static final C2491fm clv = null;
    public static final C2491fm clw = null;
    public static final C2491fm clx = null;
    public static final C2491fm cly = null;
    public static final C2491fm clz = null;
    public static final long serialVersionUID = 0;
    public static C6494any ___iScriptClass;
    @C0064Am(aul = "3c42c78637f4d8d63fefd778bf07a933", aum = 0)
    @C5454aJy("Lock Time (s)")
    private static long clo;
    @C0064Am(aul = "d7955c853e755e5eeb22037ee5fdd21d", aum = 1)
    private static long clq;
    @C0064Am(aul = "229c780dc6ff76725609b7d49bad2751", aum = 2)
    private static boolean cls;

    static {
        m1315V();
    }

    public MissileWeapon() {
        super((C5540aNg) null);
        super.mo10S();
    }

    public MissileWeapon(C5540aNg ang) {
        super(ang);
    }

    public MissileWeapon(MissileWeaponType ati) {
        super((C5540aNg) null);
        super._m_script_init(ati);
    }

    /* renamed from: V */
    static void m1315V() {
        serialVersionUID = (long) 1;
        _m_fieldCount = ProjectileWeapon._m_fieldCount + 3;
        _m_methodCount = ProjectileWeapon._m_methodCount + 11;
        int i = ProjectileWeapon._m_fieldCount;
        C5663aRz[] arzArr = new C5663aRz[(i + 3)];
        C5663aRz b = C5640aRc.m17844b(MissileWeapon.class, "3c42c78637f4d8d63fefd778bf07a933", i);
        clp = b;
        arzArr[i] = b;
        int i2 = i + 1;
        C5663aRz b2 = C5640aRc.m17844b(MissileWeapon.class, "d7955c853e755e5eeb22037ee5fdd21d", i2);
        clr = b2;
        arzArr[i2] = b2;
        int i3 = i2 + 1;
        C5663aRz b3 = C5640aRc.m17844b(MissileWeapon.class, "229c780dc6ff76725609b7d49bad2751", i3);
        clt = b3;
        arzArr[i3] = b3;
        int i4 = i3 + 1;
        _m_fields = arzArr;
        C1634Xv.m11725a((Object[]) ProjectileWeapon._m_fields, (Object[]) _m_fields);
        int i5 = ProjectileWeapon._m_methodCount;
        C2491fm[] fmVarArr = new C2491fm[(i5 + 11)];
        C2491fm a = C4105zY.m41624a(MissileWeapon.class, "82df3e0815d6c36bf37438373d41d2f9", i5);
        clu = a;
        fmVarArr[i5] = a;
        int i6 = i5 + 1;
        C2491fm a2 = C4105zY.m41624a(MissileWeapon.class, "760935c9aec43aa2fe7d3a0128844181", i6);
        clv = a2;
        fmVarArr[i6] = a2;
        int i7 = i6 + 1;
        C2491fm a3 = C4105zY.m41624a(MissileWeapon.class, "57003159f9830052e4de55f4eda390a6", i7);
        clw = a3;
        fmVarArr[i7] = a3;
        int i8 = i7 + 1;
        C2491fm a4 = C4105zY.m41624a(MissileWeapon.class, "e98194980ec7b329379284563eca76a4", i8);
        clx = a4;
        fmVarArr[i8] = a4;
        int i9 = i8 + 1;
        C2491fm a5 = C4105zY.m41624a(MissileWeapon.class, "6ded9ebc7111eff21b42ff5beb2410d0", i9);
        cly = a5;
        fmVarArr[i9] = a5;
        int i10 = i9 + 1;
        C2491fm a6 = C4105zY.m41624a(MissileWeapon.class, "07ad4c3f6b41b29fd70a4271e217bb95", i10);
        clz = a6;
        fmVarArr[i10] = a6;
        int i11 = i10 + 1;
        C2491fm a7 = C4105zY.m41624a(MissileWeapon.class, "4866acda63ff170e10c5a666b81d3a9c", i11);
        clA = a7;
        fmVarArr[i11] = a7;
        int i12 = i11 + 1;
        C2491fm a8 = C4105zY.m41624a(MissileWeapon.class, "d99603a90905283e521f6455cef3f588", i12);
        clB = a8;
        fmVarArr[i12] = a8;
        int i13 = i12 + 1;
        C2491fm a9 = C4105zY.m41624a(MissileWeapon.class, "f5ade125a17271f77bc70f5a3e7387f9", i13);
        clC = a9;
        fmVarArr[i13] = a9;
        int i14 = i13 + 1;
        C2491fm a10 = C4105zY.m41624a(MissileWeapon.class, "db2ddd9bb8354de7b4b88b7bb69d509e", i14);
        clD = a10;
        fmVarArr[i14] = a10;
        int i15 = i14 + 1;
        C2491fm a11 = C4105zY.m41624a(MissileWeapon.class, "a42c3bc18f6a08484d681e144e2223e1", i15);
        clE = a11;
        fmVarArr[i15] = a11;
        int i16 = i15 + 1;
        _m_methods = fmVarArr;
        C1634Xv.m11725a((Object[]) ProjectileWeapon._m_methods, (Object[]) _m_methods);
        ___iScriptClass = aUO.m18566a(MissileWeapon.class, C6589app.class, _m_fields, _m_methods);
    }

    @C4034yP
    @C5566aOg
    @C0064Am(aul = "a42c3bc18f6a08484d681e144e2223e1", aum = 0)
    @C2499fr
    /* renamed from: a */
    private void m1316a(boolean z, long j) {
        throw new aWi(new aCE(this, clE, new Object[]{new Boolean(z), new Long(j)}));
    }

    private long ayr() {
        return ((MissileWeaponType) getType()).ayD();
    }

    private long ays() {
        return bFf().mo5608dq().mo3213o(clr);
    }

    private boolean ayt() {
        return bFf().mo5608dq().mo3201h(clt);
    }

    @C0064Am(aul = "6ded9ebc7111eff21b42ff5beb2410d0", aum = 0)
    @C5566aOg
    @C2499fr
    private void ayy() {
        throw new aWi(new aCE(this, cly, new Object[0]));
    }

    @C4034yP
    @C5566aOg
    @C2499fr
    /* renamed from: b */
    private void m1317b(boolean z, long j) {
        switch (bFf().mo6893i(clE)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, clE, new Object[]{new Boolean(z), new Long(j)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, clE, new Object[]{new Boolean(z), new Long(j)}));
                break;
        }
        m1316a(z, j);
    }

    /* renamed from: bP */
    private void m1318bP(boolean z) {
        bFf().mo5608dq().mo3153a(clt, z);
    }

    /* renamed from: dY */
    private void m1320dY(long j) {
        throw new C6039afL();
    }

    /* renamed from: dZ */
    private void m1321dZ(long j) {
        bFf().mo5608dq().mo3184b(clr, j);
    }

    @C4034yP
    @C5566aOg
    @C0064Am(aul = "e98194980ec7b329379284563eca76a4", aum = 0)
    @C2499fr(mo18857qh = 2)
    /* renamed from: y */
    private void m1323y(Vec3d ajr) {
        throw new aWi(new aCE(this, clx, new Object[]{ajr}));
    }

    @C4034yP
    @C5566aOg
    @C2499fr(mo18857qh = 2)
    /* renamed from: z */
    private void m1324z(Vec3d ajr) {
        switch (bFf().mo6893i(clx)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, clx, new Object[]{ajr}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, clx, new Object[]{ajr}));
                break;
        }
        m1323y(ajr);
    }

    /* renamed from: S */
    public void mo10S() {
        super.mo10S();
    }

    /* renamed from: T */
    public C6494any mo11T() {
        return ___iScriptClass;
    }

    /* renamed from: U */
    public C2491fm[] mo12U() {
        return _m_methods;
    }

    /* renamed from: W */
    public Object mo13W() {
        return new C6589app(this);
    }

    /* renamed from: a */
    public Object mo14a(C0495Gr gr) {
        Object[] args = gr.getArgs();
        switch (gr.mo2417hq() - ProjectileWeapon._m_methodCount) {
            case 0:
                return ayu();
            case 1:
                m1322ex(((Float) args[0]).floatValue());
                return null;
            case 2:
                ayw();
                return null;
            case 3:
                m1323y((Vec3d) args[0]);
                return null;
            case 4:
                ayy();
                return null;
            case 5:
                return new Float(ayA());
            case 6:
                return new Long(ayC());
            case 7:
                return new Boolean(ayE());
            case 8:
                return new Boolean(ayG());
            case 9:
                m1319bQ(((Boolean) args[0]).booleanValue());
                return null;
            case 10:
                m1316a(((Boolean) args[0]).booleanValue(), ((Long) args[1]).longValue());
                return null;
            default:
                return super.mo14a(gr);
        }
    }

    /* renamed from: a */
    public void mo15a(Collection collection, C0495Gr gr) {
        super.mo15a(collection, gr);
    }

    /* access modifiers changed from: protected */
    public float ayB() {
        switch (bFf().mo6893i(clz)) {
            case 0:
                return 0.0f;
            case 2:
                return ((Float) bFf().mo5606d(new aCE(this, clz, new Object[0]))).floatValue();
            case 3:
                bFf().mo5606d(new aCE(this, clz, new Object[0]));
                break;
        }
        return ayA();
    }

    public long ayD() {
        switch (bFf().mo6893i(clA)) {
            case 0:
                return 0;
            case 2:
                return ((Long) bFf().mo5606d(new aCE(this, clA, new Object[0]))).longValue();
            case 3:
                bFf().mo5606d(new aCE(this, clA, new Object[0]));
                break;
        }
        return ayC();
    }

    public boolean ayF() {
        switch (bFf().mo6893i(clB)) {
            case 0:
                return false;
            case 2:
                return ((Boolean) bFf().mo5606d(new aCE(this, clB, new Object[0]))).booleanValue();
            case 3:
                bFf().mo5606d(new aCE(this, clB, new Object[0]));
                break;
        }
        return ayE();
    }

    public Vec3f ayv() {
        switch (bFf().mo6893i(clu)) {
            case 0:
                return null;
            case 2:
                return (Vec3f) bFf().mo5606d(new aCE(this, clu, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, clu, new Object[0]));
                break;
        }
        return ayu();
    }

    /* access modifiers changed from: protected */
    @C3248pc
    public void ayx() {
        switch (bFf().mo6893i(clw)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, clw, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, clw, new Object[0]));
                break;
        }
        ayw();
    }

    /* access modifiers changed from: protected */
    @C5566aOg
    @C2499fr
    public void ayz() {
        switch (bFf().mo6893i(cly)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, cly, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, cly, new Object[0]));
                break;
        }
        ayy();
    }

    /* renamed from: bR */
    public void mo830bR(boolean z) {
        switch (bFf().mo6893i(clD)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, clD, new Object[]{new Boolean(z)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, clD, new Object[]{new Boolean(z)}));
                break;
        }
        m1319bQ(z);
    }

    /* renamed from: c */
    public C5663aRz[] mo25c() {
        return _m_fields;
    }

    /* access modifiers changed from: protected */
    @C4034yP
    @C2499fr
    @C1253SX
    /* renamed from: ey */
    public void mo831ey(float f) {
        switch (bFf().mo6893i(clv)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, clv, new Object[]{new Float(f)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, clv, new Object[]{new Float(f)}));
                break;
        }
        m1322ex(f);
    }

    public boolean isLocked() {
        switch (bFf().mo6893i(clC)) {
            case 0:
                return false;
            case 2:
                return ((Boolean) bFf().mo5606d(new aCE(this, clC, new Object[0]))).booleanValue();
            case 3:
                bFf().mo5606d(new aCE(this, clC, new Object[0]));
                break;
        }
        return ayG();
    }

    /* renamed from: a */
    public void mo823a(MissileWeaponType ati) {
        super.mo5354a((ProjectileWeaponType) ati);
    }

    @C0064Am(aul = "82df3e0815d6c36bf37438373d41d2f9", aum = 0)
    private Vec3f ayu() {
        if (cUn().afN() != null) {
            return cUn().afN().mo16402gL();
        }
        return cUn().afV();
    }

    @C4034yP
    @C0064Am(aul = "760935c9aec43aa2fe7d3a0128844181", aum = 0)
    @C2499fr
    @C1253SX
    /* renamed from: ex */
    private void m1322ex(float f) {
        super.mo831ey(f);
        bai();
    }

    @C3248pc
    @C0064Am(aul = "57003159f9830052e4de55f4eda390a6", aum = 0)
    private void ayw() {
        if (cUn() == null) {
            System.err.println("WARN: Trying to spawn shots with a null ship. stopping fire (late message?)");
            bai();
        } else if (cUn().azW().mo21606Nc() == null) {
            System.err.println("WARN: Trying to spawn shots with a null stellar system. stopping fire (late message?)");
            bai();
        } else if (!cUn().agv().bae()) {
            System.err.println("WARN: spawn shot received for non simulated ship. stopping fire (late message?)");
            bai();
        } else {
            m1324z(cUn().getPosition());
            if (bGX()) {
                mo12911b(cUn().agv(), ayv());
            }
        }
    }

    @C0064Am(aul = "07ad4c3f6b41b29fd70a4271e217bb95", aum = 0)
    private float ayA() {
        if (cUn() == null) {
            return super.ayB();
        }
        return 1.0f / buj().mo10793ml();
    }

    @C0064Am(aul = "4866acda63ff170e10c5a666b81d3a9c", aum = 0)
    private long ayC() {
        return ayr();
    }

    @C0064Am(aul = "d99603a90905283e521f6455cef3f588", aum = 0)
    private boolean ayE() {
        return ayt();
    }

    @C0064Am(aul = "f5ade125a17271f77bc70f5a3e7387f9", aum = 0)
    private boolean ayG() {
        return (cUl() instanceof Ship) && cVr() - ays() > ayr() * 1000 && ayt();
    }

    @C0064Am(aul = "db2ddd9bb8354de7b4b88b7bb69d509e", aum = 0)
    /* renamed from: bQ */
    private void m1319bQ(boolean z) {
        m1317b(z, cVr());
    }
}
