package game.script.missile;

import com.hoplon.geometry.Vec3f;
import game.geometry.Quat4fWrap;
import game.geometry.Vec3d;
import game.network.message.externalizable.C6128agw;
import game.network.message.externalizable.C6853aut;
import game.network.message.externalizable.aCE;
import game.script.Actor;
import game.script.damage.DamageType;
import game.script.item.Shot;
import game.script.mines.Mine;
import game.script.nls.NLSConsoleAlert;
import game.script.nls.NLSManager;
import game.script.resource.Asset;
import game.script.ship.Hull;
import game.script.ship.Shield;
import game.script.ship.Ship;
import game.script.simulation.Space;
import game.script.space.Node;
import game.script.space.StellarSystem;
import logic.aaa.C0284Df;
import logic.aaa.C1506WA;
import logic.aaa.C2235dB;
import logic.aaa.C2783jw;
import logic.baa.*;
import logic.bbb.C4029yK;
import logic.bbb.C6348alI;
import logic.data.link.C2530gR;
import logic.data.mbean.aVN;
import logic.res.code.C5640aRc;
import logic.res.code.C5663aRz;
import logic.res.code.C6494any;
import logic.res.html.C2491fm;
import logic.thred.C6339akz;
import org.mozilla1.javascript.ScriptRuntime;
import p001a.*;
import taikodom.render.helpers.SceneHelper;
import taikodom.render.scene.RParticleSystem;
import taikodom.render.scene.Scene;

import javax.vecmath.Quat4f;
import java.util.Collection;
import java.util.Map;

@C5829abJ("2.1.1")
@C6485anp
@C2712iu(mo19786Bt = BaseMissileType.class, version = "2.1.1")
@C1020Ot(mo4553Ur = 5000, drz = C1020Ot.C1021a.TIME_BASED)
@C5511aMd
/* renamed from: a.aED */
/* compiled from: a */
public class Missile extends Pawn implements C1616Xf, aDA, C2530gR.C2531a {

    /* renamed from: Lm */
    public static final C2491fm f2630Lm = null;

    /* renamed from: Mq */
    public static final C2491fm f2631Mq = null;
    /* renamed from: VW */
    public static final C5663aRz f2633VW = null;
    /* renamed from: VY */
    public static final C5663aRz f2635VY = null;
    /* renamed from: Wc */
    public static final C5663aRz f2637Wc = null;
    /* renamed from: We */
    public static final C5663aRz f2639We = null;
    /* renamed from: Wk */
    public static final C2491fm f2640Wk = null;
    /* renamed from: Wl */
    public static final C2491fm f2641Wl = null;
    /* renamed from: Wm */
    public static final C2491fm f2642Wm = null;
    /* renamed from: Wp */
    public static final C2491fm f2643Wp = null;
    /* renamed from: Wu */
    public static final C2491fm f2644Wu = null;
    /* renamed from: Wx */
    public static final C2491fm f2645Wx = null;
    public static final C2491fm _f_dispose_0020_0028_0029V = null;
    public static final C2491fm _f_getType_0020_0028_0029Ljava_002flang_002fObject_003b = null;
    public static final C2491fm _f_onResurrect_0020_0028_0029V = null;
    public static final C2491fm _f_push_0020_0028_0029V = null;
    /* renamed from: _f_setTrackedShip_0020_0028Ltaikodom_002fgame_002fscript_002fship_002fShip_003b_0029V */
    public static final C2491fm f2646x30fdc3fb = null;
    public static final C2491fm _f_step_0020_0028F_0029V = null;
    public static final C5663aRz _f_trackedShip = null;
    public static final int _m_fieldCount = 0;
    public static final C5663aRz[] _m_fields = null;
    public static final int _m_methodCount = 0;
    public static final C2491fm[] _m_methods = null;
    public static final C2491fm aTe = null;
    public static final C2491fm aTf = null;
    public static final C2491fm aTg = null;
    public static final C2491fm aTh = null;
    public static final C5663aRz bnO = null;
    public static final C5663aRz bnP = null;
    public static final C5663aRz bnU = null;
    public static final C5663aRz bnV = null;
    public static final C2491fm bpJ = null;
    public static final C2491fm bqJ = null;
    public static final C2491fm bqM = null;
    public static final C2491fm bqg = null;
    public static final C2491fm brN = null;
    public static final C2491fm brp = null;
    public static final C2491fm dmC = null;
    public static final C5663aRz hHi = null;
    public static final C5663aRz hHk = null;
    public static final C2491fm hHm = null;
    public static final C2491fm hHn = null;
    public static final C2491fm hHo = null;
    public static final C2491fm hHp = null;
    public static final C2491fm hHq = null;
    public static final C2491fm hHr = null;
    public static final C2491fm hHs = null;
    /* renamed from: kZ */
    public static final C5663aRz f2650kZ = null;
    public static final long serialVersionUID = 0;
    /* renamed from: uK */
    public static final C5663aRz f2652uK = null;
    /* renamed from: uW */
    public static final C2491fm f2653uW = null;
    /* renamed from: uX */
    public static final C2491fm f2654uX = null;
    /* renamed from: uY */
    public static final C2491fm f2655uY = null;
    /* renamed from: uZ */
    public static final C2491fm f2656uZ = null;
    /* renamed from: va */
    public static final C2491fm f2657va = null;
    /* renamed from: vc */
    public static final C2491fm f2658vc = null;
    /* renamed from: ve */
    public static final C2491fm f2659ve = null;
    /* renamed from: vi */
    public static final C2491fm f2660vi = null;
    /* renamed from: vm */
    public static final C2491fm f2661vm = null;
    /* renamed from: vn */
    public static final C2491fm f2662vn = null;
    /* renamed from: vo */
    public static final C2491fm f2663vo = null;
    /* renamed from: vs */
    public static final C2491fm f2664vs = null;
    public static C6494any ___iScriptClass;
    @C0064Am(aul = "a37a065ec7655dc17872819b0eb49d16", aum = 9)
    @C0803Ld

    /* renamed from: VV */
    private static Hull f2632VV;
    @C0064Am(aul = "81e82d2135c3a5ec73ea9d7164494456", aum = 5)

    /* renamed from: VX */
    private static float f2634VX;
    @C0064Am(aul = "458ad6213838f680faccaa443248c12c", aum = 6)

    /* renamed from: Wb */
    private static Asset f2636Wb;
    @C0064Am(aul = "cbf43e0528a6ce81227dae5118494ebb", aum = 7)

    /* renamed from: Wd */
    private static Asset f2638Wd;
    @C0064Am(aul = "5634d241e551b7249427890d4faf4e38", aum = 0)
    private static float bhk;
    @C0064Am(aul = "85b9a3643888c662d08f8e27090990a4", aum = 2)
    private static float bhl;
    @C0064Am(aul = "d465131b60fb0a733384e50c2f10ecce", aum = 8)
    private static Asset bhm;
    @C0064Am(aul = "391f192cd1294c3e3701a44df0c514b9", aum = 1)

    /* renamed from: dW */
    private static float f2647dW;
    @C0064Am(aul = "27b405c0c79424a5bb9a7b03d3070bac", aum = 3)

    /* renamed from: dX */
    private static float f2648dX;
    @C0064Am(aul = "140fc30eed46cef01050de3e7818b303", aum = 12)
    private static MissileWeapon hHj;
    @C0064Am(aul = "bdb08028c905e24e12333ffd716d2d23", aum = 4)
    @C5454aJy("Life Time (s)")

    /* renamed from: kY */
    private static long f2649kY;
    @C0064Am(aul = "b2192ff85ac044a6cd5a66e9e89b8187", aum = 10)
    private static Ship trackedShip;
    @C0064Am(aul = "f89e131fd383c9a3c38836b8a6b004d0", aum = 11)
    @C1020Ot(drz = C1020Ot.C1021a.NOT_PERSISTED)

    /* renamed from: uJ */
    private static C5260aCm f2651uJ;

    static {
        m13934V();
    }

    @ClientOnly

    /* renamed from: Wf */
    private float f2665Wf;
    @ClientOnly
    private transient C3257pi hHl;
    @ClientOnly
    private float radius;

    public Missile() {
        super((C5540aNg) null);
        super.mo10S();
    }

    public Missile(C5540aNg ang) {
        super(ang);
    }

    public Missile(MissileType avg) {
        super((C5540aNg) null);
        super._m_script_init(avg);
    }

    /* renamed from: V */
    static void m13934V() {
        serialVersionUID = (long) 1;
        _m_fieldCount = Pawn._m_fieldCount + 13;
        _m_methodCount = Pawn._m_methodCount + 44;
        int i = Pawn._m_fieldCount;
        C5663aRz[] arzArr = new C5663aRz[(i + 13)];
        C5663aRz b = C5640aRc.m17844b(Missile.class, "5634d241e551b7249427890d4faf4e38", i);
        bnU = b;
        arzArr[i] = b;
        int i2 = i + 1;
        C5663aRz b2 = C5640aRc.m17844b(Missile.class, "391f192cd1294c3e3701a44df0c514b9", i2);
        bnV = b2;
        arzArr[i2] = b2;
        int i3 = i2 + 1;
        C5663aRz b3 = C5640aRc.m17844b(Missile.class, "85b9a3643888c662d08f8e27090990a4", i3);
        bnO = b3;
        arzArr[i3] = b3;
        int i4 = i3 + 1;
        C5663aRz b4 = C5640aRc.m17844b(Missile.class, "27b405c0c79424a5bb9a7b03d3070bac", i4);
        bnP = b4;
        arzArr[i4] = b4;
        int i5 = i4 + 1;
        C5663aRz b5 = C5640aRc.m17844b(Missile.class, "bdb08028c905e24e12333ffd716d2d23", i5);
        f2650kZ = b5;
        arzArr[i5] = b5;
        int i6 = i5 + 1;
        C5663aRz b6 = C5640aRc.m17844b(Missile.class, "81e82d2135c3a5ec73ea9d7164494456", i6);
        f2635VY = b6;
        arzArr[i6] = b6;
        int i7 = i6 + 1;
        C5663aRz b7 = C5640aRc.m17844b(Missile.class, "458ad6213838f680faccaa443248c12c", i7);
        f2637Wc = b7;
        arzArr[i7] = b7;
        int i8 = i7 + 1;
        C5663aRz b8 = C5640aRc.m17844b(Missile.class, "cbf43e0528a6ce81227dae5118494ebb", i8);
        f2639We = b8;
        arzArr[i8] = b8;
        int i9 = i8 + 1;
        C5663aRz b9 = C5640aRc.m17844b(Missile.class, "d465131b60fb0a733384e50c2f10ecce", i9);
        hHi = b9;
        arzArr[i9] = b9;
        int i10 = i9 + 1;
        C5663aRz b10 = C5640aRc.m17844b(Missile.class, "a37a065ec7655dc17872819b0eb49d16", i10);
        f2633VW = b10;
        arzArr[i10] = b10;
        int i11 = i10 + 1;
        C5663aRz b11 = C5640aRc.m17844b(Missile.class, "b2192ff85ac044a6cd5a66e9e89b8187", i11);
        _f_trackedShip = b11;
        arzArr[i11] = b11;
        int i12 = i11 + 1;
        C5663aRz b12 = C5640aRc.m17844b(Missile.class, "f89e131fd383c9a3c38836b8a6b004d0", i12);
        f2652uK = b12;
        arzArr[i12] = b12;
        int i13 = i12 + 1;
        C5663aRz b13 = C5640aRc.m17844b(Missile.class, "140fc30eed46cef01050de3e7818b303", i13);
        hHk = b13;
        arzArr[i13] = b13;
        int i14 = i13 + 1;
        _m_fields = arzArr;
        C1634Xv.m11725a((Object[]) Pawn._m_fields, (Object[]) _m_fields);
        int i15 = Pawn._m_methodCount;
        C2491fm[] fmVarArr = new C2491fm[(i15 + 44)];
        C2491fm a = C4105zY.m41624a(Missile.class, "d34aac475ac216e2f46b6a554c30347d", i15);
        _f_onResurrect_0020_0028_0029V = a;
        fmVarArr[i15] = a;
        int i16 = i15 + 1;
        C2491fm a2 = C4105zY.m41624a(Missile.class, "e5a91e296792e5a2dc2dc4d03deabf29", i16);
        aTe = a2;
        fmVarArr[i16] = a2;
        int i17 = i16 + 1;
        C2491fm a3 = C4105zY.m41624a(Missile.class, "246bc8d80435d04809109a316a3fe4a6", i17);
        aTf = a3;
        fmVarArr[i17] = a3;
        int i18 = i17 + 1;
        C2491fm a4 = C4105zY.m41624a(Missile.class, "1820e9a2a183beeddf1ad801999ce233", i18);
        aTg = a4;
        fmVarArr[i18] = a4;
        int i19 = i18 + 1;
        C2491fm a5 = C4105zY.m41624a(Missile.class, "2d61d9475ca62b321bba2baf6e2d1056", i19);
        aTh = a5;
        fmVarArr[i19] = a5;
        int i20 = i19 + 1;
        C2491fm a6 = C4105zY.m41624a(Missile.class, "b66a0842f67e93a4dd504600fb6323b6", i20);
        bpJ = a6;
        fmVarArr[i20] = a6;
        int i21 = i20 + 1;
        C2491fm a7 = C4105zY.m41624a(Missile.class, "38895687a8de5f8f00ee171e287d4e3b", i21);
        f2663vo = a7;
        fmVarArr[i21] = a7;
        int i22 = i21 + 1;
        C2491fm a8 = C4105zY.m41624a(Missile.class, "1a4c9fc6bed1bdfd1cda3fcefed22aea", i22);
        f2656uZ = a8;
        fmVarArr[i22] = a8;
        int i23 = i22 + 1;
        C2491fm a9 = C4105zY.m41624a(Missile.class, "35a050c62d47d636d1a086418986db48", i23);
        hHm = a9;
        fmVarArr[i23] = a9;
        int i24 = i23 + 1;
        C2491fm a10 = C4105zY.m41624a(Missile.class, "919bc1bc7a71db8c6c9eefcb14e9c227", i24);
        _f_step_0020_0028F_0029V = a10;
        fmVarArr[i24] = a10;
        int i25 = i24 + 1;
        C2491fm a11 = C4105zY.m41624a(Missile.class, "48e5c27f00e8d1b55c6ab776ed71dc48", i25);
        hHn = a11;
        fmVarArr[i25] = a11;
        int i26 = i25 + 1;
        C2491fm a12 = C4105zY.m41624a(Missile.class, "eb93a1080127e85eccb4d45324a7851a", i26);
        hHo = a12;
        fmVarArr[i26] = a12;
        int i27 = i26 + 1;
        C2491fm a13 = C4105zY.m41624a(Missile.class, "c0a544e198422e690b8ed08fcf500987", i27);
        f2644Wu = a13;
        fmVarArr[i27] = a13;
        int i28 = i27 + 1;
        C2491fm a14 = C4105zY.m41624a(Missile.class, "0b3ad6f728356dc1430a99978f4bd07a", i28);
        f2640Wk = a14;
        fmVarArr[i28] = a14;
        int i29 = i28 + 1;
        C2491fm a15 = C4105zY.m41624a(Missile.class, "b164970fed13fb1f1d8f597f6a7175a6", i29);
        f2662vn = a15;
        fmVarArr[i29] = a15;
        int i30 = i29 + 1;
        C2491fm a16 = C4105zY.m41624a(Missile.class, "5be022e150314dec38328b83453670ce", i30);
        brp = a16;
        fmVarArr[i30] = a16;
        int i31 = i30 + 1;
        C2491fm a17 = C4105zY.m41624a(Missile.class, "1d2cab132ee3e89b6a57f5ec567b1af3", i31);
        f2658vc = a17;
        fmVarArr[i31] = a17;
        int i32 = i31 + 1;
        C2491fm a18 = C4105zY.m41624a(Missile.class, "70a8895c09c767c47a0fc57692e42aba", i32);
        f2659ve = a18;
        fmVarArr[i32] = a18;
        int i33 = i32 + 1;
        C2491fm a19 = C4105zY.m41624a(Missile.class, "819d52b42fa2b7ee85edab85f140fddc", i33);
        f2654uX = a19;
        fmVarArr[i33] = a19;
        int i34 = i33 + 1;
        C2491fm a20 = C4105zY.m41624a(Missile.class, "242902d3d2d815ae94b9b8fe1d9882e8", i34);
        f2630Lm = a20;
        fmVarArr[i34] = a20;
        int i35 = i34 + 1;
        C2491fm a21 = C4105zY.m41624a(Missile.class, "06b0156d0aeffbbfdc014677662ec69b", i35);
        _f_push_0020_0028_0029V = a21;
        fmVarArr[i35] = a21;
        int i36 = i35 + 1;
        C2491fm a22 = C4105zY.m41624a(Missile.class, "79e3bf4bc5ff6607e1bfa24a088ab8bb", i36);
        hHp = a22;
        fmVarArr[i36] = a22;
        int i37 = i36 + 1;
        C2491fm a23 = C4105zY.m41624a(Missile.class, "8cfc0d184dbfed95cbd0fdd8c7983ec0", i37);
        dmC = a23;
        fmVarArr[i37] = a23;
        int i38 = i37 + 1;
        C2491fm a24 = C4105zY.m41624a(Missile.class, "24cb154bb7c3a4e1ed668160572fe11f", i38);
        bqJ = a24;
        fmVarArr[i38] = a24;
        int i39 = i38 + 1;
        C2491fm a25 = C4105zY.m41624a(Missile.class, "21456534742bfef179a7b8cdd9933f2e", i39);
        hHq = a25;
        fmVarArr[i39] = a25;
        int i40 = i39 + 1;
        C2491fm a26 = C4105zY.m41624a(Missile.class, "3c78b8a668ce4c663217cd45091c0434", i40);
        f2657va = a26;
        fmVarArr[i40] = a26;
        int i41 = i40 + 1;
        C2491fm a27 = C4105zY.m41624a(Missile.class, "4ecfff626293f255b676525b96daa4b1", i41);
        f2661vm = a27;
        fmVarArr[i41] = a27;
        int i42 = i41 + 1;
        C2491fm a28 = C4105zY.m41624a(Missile.class, "7a41aff66c3ba26e286ff712c1ab9490", i42);
        f2660vi = a28;
        fmVarArr[i42] = a28;
        int i43 = i42 + 1;
        C2491fm a29 = C4105zY.m41624a(Missile.class, "2f99ca48bf585566f8435dce6f2d5005", i43);
        _f_dispose_0020_0028_0029V = a29;
        fmVarArr[i43] = a29;
        int i44 = i43 + 1;
        C2491fm a30 = C4105zY.m41624a(Missile.class, "fae733322f5729eb865890e329a8ce7b", i44);
        f2646x30fdc3fb = a30;
        fmVarArr[i44] = a30;
        int i45 = i44 + 1;
        C2491fm a31 = C4105zY.m41624a(Missile.class, "af1625948a084c0bd6672131f1e4e997", i45);
        bqg = a31;
        fmVarArr[i45] = a31;
        int i46 = i45 + 1;
        C2491fm a32 = C4105zY.m41624a(Missile.class, "e5543a457b982442a285612d18c09468", i46);
        f2631Mq = a32;
        fmVarArr[i46] = a32;
        int i47 = i46 + 1;
        C2491fm a33 = C4105zY.m41624a(Missile.class, "15fefe0fe89e1ff33bffa8fe76b94f8a", i47);
        f2643Wp = a33;
        fmVarArr[i47] = a33;
        int i48 = i47 + 1;
        C2491fm a34 = C4105zY.m41624a(Missile.class, "f277421a4ed34a9b55aca10ca6977a0e", i48);
        hHr = a34;
        fmVarArr[i48] = a34;
        int i49 = i48 + 1;
        C2491fm a35 = C4105zY.m41624a(Missile.class, "02f2acae26dc5c9942d16e67e12ce442", i49);
        f2641Wl = a35;
        fmVarArr[i49] = a35;
        int i50 = i49 + 1;
        C2491fm a36 = C4105zY.m41624a(Missile.class, "cf680585f702b4ce0b2ad69f6949d575", i50);
        f2642Wm = a36;
        fmVarArr[i50] = a36;
        int i51 = i50 + 1;
        C2491fm a37 = C4105zY.m41624a(Missile.class, "3e9ea3304dc906cc5f1c46cb85626ee3", i51);
        f2653uW = a37;
        fmVarArr[i51] = a37;
        int i52 = i51 + 1;
        C2491fm a38 = C4105zY.m41624a(Missile.class, "ae35031b27aa469a9943b5eba601a9af", i52);
        f2645Wx = a38;
        fmVarArr[i52] = a38;
        int i53 = i52 + 1;
        C2491fm a39 = C4105zY.m41624a(Missile.class, "3ba53101e0c067a6e558d521c2bc9e50", i53);
        brN = a39;
        fmVarArr[i53] = a39;
        int i54 = i53 + 1;
        C2491fm a40 = C4105zY.m41624a(Missile.class, "a11e1f9b99bc2db7b0debba8c6caae58", i54);
        hHs = a40;
        fmVarArr[i54] = a40;
        int i55 = i54 + 1;
        C2491fm a41 = C4105zY.m41624a(Missile.class, "1c75ee9307ed1769476b6ce23404ce44", i55);
        f2655uY = a41;
        fmVarArr[i55] = a41;
        int i56 = i55 + 1;
        C2491fm a42 = C4105zY.m41624a(Missile.class, "4f6bb191a528e6d60b4dc7ccbd8d5a4a", i56);
        f2664vs = a42;
        fmVarArr[i56] = a42;
        int i57 = i56 + 1;
        C2491fm a43 = C4105zY.m41624a(Missile.class, "7831f7be91e0e64fff93245aa917e821", i57);
        bqM = a43;
        fmVarArr[i57] = a43;
        int i58 = i57 + 1;
        C2491fm a44 = C4105zY.m41624a(Missile.class, "2fdce9061f9174cdbb71a9b5dd02976e", i58);
        _f_getType_0020_0028_0029Ljava_002flang_002fObject_003b = a44;
        fmVarArr[i58] = a44;
        int i59 = i58 + 1;
        _m_methods = fmVarArr;
        C1634Xv.m11725a((Object[]) Pawn._m_methods, (Object[]) _m_methods);
        ___iScriptClass = aUO.m18566a(Missile.class, aVN.class, _m_fields, _m_methods);
    }

    /* renamed from: A */
    private void m13931A(Asset tCVar) {
        throw new C6039afL();
    }

    @C0064Am(aul = "fae733322f5729eb865890e329a8ce7b", aum = 0)
    @C5566aOg
    /* renamed from: I */
    private void m13933I(Ship fAVar) {
        throw new aWi(new aCE(this, f2646x30fdc3fb, new Object[]{fAVar}));
    }

    /* renamed from: a */
    private void m13940a(MissileWeapon bp) {
        bFf().mo5608dq().mo3197f(hHk, bp);
    }

    /* renamed from: a */
    private void m13945a(C5260aCm acm) {
        bFf().mo5608dq().mo3197f(f2652uK, acm);
    }

    /* renamed from: a */
    private void m13946a(Hull jRVar) {
        bFf().mo5608dq().mo3197f(f2633VW, jRVar);
    }

    private float aeO() {
        return ((MissileType) getType()).mo11851VF();
    }

    private float aeP() {
        return ((MissileType) getType()).mo11852VH();
    }

    private float aeS() {
        return ((MissileType) getType()).mo11870rb();
    }

    private float aeT() {
        return ((MissileType) getType()).mo11869ra();
    }

    /* renamed from: ap */
    private void m13949ap(Ship fAVar) {
        bFf().mo5608dq().mo3197f(_f_trackedShip, fAVar);
    }

    @C4034yP
    @C5566aOg
    @C0064Am(aul = "eb93a1080127e85eccb4d45324a7851a", aum = 0)
    @C2499fr(mo18857qh = 2)
    /* renamed from: as */
    private void m13950as(Vec3d ajr) {
        throw new aWi(new aCE(this, hHo, new Object[]{ajr}));
    }

    @C4034yP
    @C5566aOg
    @C0064Am(aul = "21456534742bfef179a7b8cdd9933f2e", aum = 0)
    @C2499fr(mo18857qh = 2)
    /* renamed from: au */
    private void m13951au(Vec3d ajr) {
        throw new aWi(new aCE(this, hHq, new Object[]{ajr}));
    }

    @C4034yP
    @C5566aOg
    @C2499fr(mo18857qh = 2)
    /* renamed from: av */
    private void m13952av(Vec3d ajr) {
        switch (bFf().mo6893i(hHq)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, hHq, new Object[]{ajr}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, hHq, new Object[]{ajr}));
                break;
        }
        m13951au(ajr);
    }

    /* renamed from: aw */
    private void m13953aw(float f) {
        throw new C6039afL();
    }

    /* renamed from: bY */
    private void m13958bY(Asset tCVar) {
        throw new C6039afL();
    }

    private Asset cXM() {
        return ((MissileType) getType()).dAR();
    }

    private Ship cXN() {
        return (Ship) bFf().mo5608dq().mo3214p(_f_trackedShip);
    }

    private MissileWeapon cXO() {
        return (MissileWeapon) bFf().mo5608dq().mo3214p(hHk);
    }

    @C4034yP
    @C2499fr
    @C1253SX
    private void cXQ() {
        switch (bFf().mo6893i(hHn)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, hHn, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, hHn, new Object[0]));
                break;
        }
        cXP();
    }

    /* renamed from: d */
    private void m13963d(PlayerController tVar) {
        switch (bFf().mo6893i(brp)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, brp, new Object[]{tVar}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, brp, new Object[]{tVar}));
                break;
        }
        m13962c(tVar);
    }

    /* renamed from: dt */
    private void m13964dt(float f) {
        throw new C6039afL();
    }

    /* renamed from: du */
    private void m13965du(float f) {
        throw new C6039afL();
    }

    /* renamed from: dx */
    private void m13966dx(float f) {
        throw new C6039afL();
    }

    /* renamed from: dy */
    private void m13967dy(float f) {
        throw new C6039afL();
    }

    @C4034yP
    @C5566aOg
    @C0064Am(aul = "c0a544e198422e690b8ed08fcf500987", aum = 0)
    @C2499fr(mo18857qh = 2)
    /* renamed from: e */
    private void m13968e(Actor cr, C5260aCm acm, Vec3f vec3f, Vec3f vec3f2) {
        throw new aWi(new aCE(this, f2644Wu, new Object[]{cr, acm, vec3f, vec3f2}));
    }

    /* renamed from: eG */
    private long m13970eG() {
        return ((MissileType) getType()).dAP();
    }

    /* renamed from: id */
    private C5260aCm m13972id() {
        return (C5260aCm) bFf().mo5608dq().mo3214p(f2652uK);
    }

    @ClientOnly
    /* renamed from: jh */
    private void m13979jh(boolean z) {
        switch (bFf().mo6893i(hHr)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, hHr, new Object[]{new Boolean(z)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, hHr, new Object[]{new Boolean(z)}));
                break;
        }
        m13978jg(z);
    }

    /* renamed from: l */
    private void m13980l(long j) {
        throw new C6039afL();
    }

    @C0064Am(aul = "35a050c62d47d636d1a086418986db48", aum = 0)
    @C5566aOg
    /* renamed from: mx */
    private C5260aCm m13981mx(float f) {
        throw new aWi(new aCE(this, hHm, new Object[]{new Float(f)}));
    }

    @C0064Am(aul = "242902d3d2d815ae94b9b8fe1d9882e8", aum = 0)
    @C5566aOg
    @C2499fr
    /* renamed from: qU */
    private void m13982qU() {
        throw new aWi(new aCE(this, f2630Lm, new Object[0]));
    }

    @C0064Am(aul = "2fdce9061f9174cdbb71a9b5dd02976e", aum = 0)
    /* renamed from: qW */
    private Object m13983qW() {
        return cXS();
    }

    /* renamed from: z */
    private void m13984z(Asset tCVar) {
        throw new C6039afL();
    }

    /* renamed from: zi */
    private Hull m13985zi() {
        return (Hull) bFf().mo5608dq().mo3214p(f2633VW);
    }

    /* renamed from: zj */
    private float m13986zj() {
        return ((MissileType) getType()).mo11871zD();
    }

    /* renamed from: zl */
    private Asset m13987zl() {
        return ((MissileType) getType()).bkh();
    }

    /* renamed from: zm */
    private Asset m13988zm() {
        return ((MissileType) getType()).bkj();
    }

    /* renamed from: Fe */
    public void mo957Fe() {
        switch (bFf().mo6893i(bqM)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, bqM, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, bqM, new Object[0]));
                break;
        }
        ahp();
    }

    @C5566aOg
    /* renamed from: J */
    public void mo8534J(Ship fAVar) {
        switch (bFf().mo6893i(f2646x30fdc3fb)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f2646x30fdc3fb, new Object[]{fAVar}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f2646x30fdc3fb, new Object[]{fAVar}));
                break;
        }
        m13933I(fAVar);
    }

    /* renamed from: S */
    public void mo10S() {
        super.mo10S();
    }

    /* renamed from: T */
    public C6494any mo11T() {
        return ___iScriptClass;
    }

    /* renamed from: U */
    public C2491fm[] mo12U() {
        return _m_methods;
    }

    /* renamed from: VF */
    public float mo962VF() {
        switch (bFf().mo6893i(aTe)) {
            case 0:
                return 0.0f;
            case 2:
                return ((Float) bFf().mo5606d(new aCE(this, aTe, new Object[0]))).floatValue();
            case 3:
                bFf().mo5606d(new aCE(this, aTe, new Object[0]));
                break;
        }
        return m13935VE();
    }

    /* renamed from: VH */
    public float mo963VH() {
        switch (bFf().mo6893i(aTf)) {
            case 0:
                return 0.0f;
            case 2:
                return ((Float) bFf().mo5606d(new aCE(this, aTf, new Object[0]))).floatValue();
            case 3:
                bFf().mo5606d(new aCE(this, aTf, new Object[0]));
                break;
        }
        return m13936VG();
    }

    /* renamed from: W */
    public Object mo13W() {
        return new aVN(this);
    }

    /* renamed from: a */
    public Object mo14a(C0495Gr gr) {
        Object[] args = gr.getArgs();
        switch (gr.mo2417hq() - Pawn._m_methodCount) {
            case 0:
                m13948aG();
                return null;
            case 1:
                return new Float(m13935VE());
            case 2:
                return new Float(m13936VG());
            case 3:
                return new Float(m13937VI());
            case 4:
                return new Float(m13938VJ());
            case 5:
                return afJ();
            case 6:
                return m13977iz();
            case 7:
                m13939a(((Long) args[0]).longValue(), ((Boolean) args[1]).booleanValue());
                return null;
            case 8:
                return m13981mx(((Float) args[0]).floatValue());
            case 9:
                m13954ay(((Float) args[0]).floatValue());
                return null;
            case 10:
                cXP();
                return null;
            case 11:
                m13950as((Vec3d) args[0]);
                return null;
            case 12:
                m13968e((Actor) args[0], (C5260aCm) args[1], (Vec3f) args[2], (Vec3f) args[3]);
                return null;
            case 13:
                m13957b((Hull) args[0], (C5260aCm) args[1], (Actor) args[2]);
                return null;
            case 14:
                m13960c((Actor) args[0], (C5260aCm) args[1], (Vec3f) args[2], (Vec3f) args[3]);
                return null;
            case 15:
                m13962c((PlayerController) args[0]);
                return null;
            case 16:
                return m13974io();
            case 17:
                return m13975iq();
            case 18:
                m13943a((Actor) args[0], (Vec3f) args[1], (Vec3f) args[2], ((Float) args[3]).floatValue(), (C6339akz) args[4]);
                return null;
            case 19:
                m13982qU();
                return null;
            case 20:
                m13932Gd();
                return null;
            case 21:
                return cXR();
            case 22:
                m13969e((Vec3d) args[0], (Node) args[1], (StellarSystem) args[2]);
                return null;
            case 23:
                m13961c((Vec3d) args[0], (Node) args[1], (StellarSystem) args[2]);
                return null;
            case 24:
                m13951au((Vec3d) args[0]);
                return null;
            case 25:
                m13947a((C4029yK) args[0]);
                return null;
            case 26:
                m13942a((Actor) args[0], (C5260aCm) args[1], (Vec3f) args[2], (Vec3f) args[3]);
                return null;
            case 27:
                return m13976it();
            case 28:
                m13971fg();
                return null;
            case 29:
                m13933I((Ship) args[0]);
                return null;
            case 30:
                return agA();
            case 31:
                m13944a((StellarSystem) args[0]);
                return null;
            case 32:
                m13991zw();
                return null;
            case 33:
                m13978jg(((Boolean) args[0]).booleanValue());
                return null;
            case 34:
                return m13989zs();
            case 35:
                return m13990zu();
            case 36:
                return m13973ik();
            case 37:
                m13956b((C5260aCm) args[0]);
                return null;
            case 38:
                ahT();
                return null;
            case 39:
                m13955b((MissileWeapon) args[0]);
                return null;
            case 40:
                return m13959c((Space) args[0], ((Long) args[1]).longValue());
            case 41:
                m13941a((Actor.C0200h) args[0]);
                return null;
            case 42:
                ahp();
                return null;
            case 43:
                return m13983qW();
            default:
                return super.mo14a(gr);
        }
    }

    /* renamed from: a */
    public void mo8536a(Hull jRVar, C5260aCm acm, Actor cr) {
        switch (bFf().mo6893i(f2640Wk)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f2640Wk, new Object[]{jRVar, acm, cr}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f2640Wk, new Object[]{jRVar, acm, cr}));
                break;
        }
        m13957b(jRVar, acm, cr);
    }

    /* renamed from: a */
    public void mo15a(Collection collection, C0495Gr gr) {
        super.mo15a(collection, gr);
    }

    /* renamed from: aH */
    public void mo70aH() {
        switch (bFf().mo6893i(_f_onResurrect_0020_0028_0029V)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, _f_onResurrect_0020_0028_0029V, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, _f_onResurrect_0020_0028_0029V, new Object[0]));
                break;
        }
        m13948aG();
    }

    public Actor agB() {
        switch (bFf().mo6893i(bqg)) {
            case 0:
                return null;
            case 2:
                return (Actor) bFf().mo5606d(new aCE(this, bqg, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, bqg, new Object[0]));
                break;
        }
        return agA();
    }

    public void ahU() {
        switch (bFf().mo6893i(brN)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, brN, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, brN, new Object[0]));
                break;
        }
        ahT();
    }

    @C4034yP
    @C5566aOg
    @C2499fr(mo18857qh = 2)
    /* renamed from: at */
    public void mo8538at(Vec3d ajr) {
        switch (bFf().mo6893i(hHo)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, hHo, new Object[]{ajr}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, hHo, new Object[]{ajr}));
                break;
        }
        m13950as(ajr);
    }

    @C5307aEh
    /* renamed from: b */
    public void mo991b(long j, boolean z) {
        switch (bFf().mo6893i(f2656uZ)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f2656uZ, new Object[]{new Long(j), new Boolean(z)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f2656uZ, new Object[]{new Long(j), new Boolean(z)}));
                break;
        }
        m13939a(j, z);
    }

    /* renamed from: b */
    public void mo620b(Actor.C0200h hVar) {
        switch (bFf().mo6893i(f2664vs)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f2664vs, new Object[]{hVar}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f2664vs, new Object[]{hVar}));
                break;
        }
        m13941a(hVar);
    }

    /* renamed from: b */
    public void mo621b(Actor cr, C5260aCm acm, Vec3f vec3f, Vec3f vec3f2) {
        switch (bFf().mo6893i(f2661vm)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f2661vm, new Object[]{cr, acm, vec3f, vec3f2}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f2661vm, new Object[]{cr, acm, vec3f, vec3f2}));
                break;
        }
        m13942a(cr, acm, vec3f, vec3f2);
    }

    @C5307aEh
    /* renamed from: b */
    public void mo992b(Actor cr, Vec3f vec3f, Vec3f vec3f2, float f, C6339akz akz) {
        switch (bFf().mo6893i(f2654uX)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f2654uX, new Object[]{cr, vec3f, vec3f2, new Float(f), akz}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f2654uX, new Object[]{cr, vec3f, vec3f2, new Float(f), akz}));
                break;
        }
        m13943a(cr, vec3f, vec3f2, f, akz);
    }

    /* renamed from: b */
    public void mo993b(StellarSystem jj) {
        switch (bFf().mo6893i(f2631Mq)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f2631Mq, new Object[]{jj}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f2631Mq, new Object[]{jj}));
                break;
        }
        m13944a(jj);
    }

    /* renamed from: b */
    public void mo999b(C4029yK yKVar) {
        switch (bFf().mo6893i(f2657va)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f2657va, new Object[]{yKVar}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f2657va, new Object[]{yKVar}));
                break;
        }
        m13947a(yKVar);
    }

    /* renamed from: c */
    public void mo8539c(MissileWeapon bp) {
        switch (bFf().mo6893i(hHs)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, hHs, new Object[]{bp}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, hHs, new Object[]{bp}));
                break;
        }
        m13955b(bp);
    }

    /* renamed from: c */
    public void mo8540c(C5260aCm acm) {
        switch (bFf().mo6893i(f2645Wx)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f2645Wx, new Object[]{acm}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f2645Wx, new Object[]{acm}));
                break;
        }
        m13956b(acm);
    }

    /* renamed from: c */
    public C5663aRz[] mo25c() {
        return _m_fields;
    }

    public MissileType cXS() {
        switch (bFf().mo6893i(hHp)) {
            case 0:
                return null;
            case 2:
                return (MissileType) bFf().mo5606d(new aCE(this, hHp, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, hHp, new Object[0]));
                break;
        }
        return cXR();
    }

    /* access modifiers changed from: protected */
    /* renamed from: d */
    public C0520HN mo635d(Space ea, long j) {
        switch (bFf().mo6893i(f2655uY)) {
            case 0:
                return null;
            case 2:
                return (C0520HN) bFf().mo5606d(new aCE(this, f2655uY, new Object[]{ea, new Long(j)}));
            case 3:
                bFf().mo5606d(new aCE(this, f2655uY, new Object[]{ea, new Long(j)}));
                break;
        }
        return m13959c(ea, j);
    }

    /* renamed from: d */
    public void mo636d(Actor cr, C5260aCm acm, Vec3f vec3f, Vec3f vec3f2) {
        switch (bFf().mo6893i(f2662vn)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f2662vn, new Object[]{cr, acm, vec3f, vec3f2}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f2662vn, new Object[]{cr, acm, vec3f, vec3f2}));
                break;
        }
        m13960c(cr, acm, vec3f, vec3f2);
    }

    @C4034yP
    /* renamed from: d */
    public void mo8542d(Vec3d ajr, Node rPVar, StellarSystem jj) {
        switch (bFf().mo6893i(bqJ)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, bqJ, new Object[]{ajr, rPVar, jj}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, bqJ, new Object[]{ajr, rPVar, jj}));
                break;
        }
        m13961c(ajr, rPVar, jj);
    }

    public void dispose() {
        switch (bFf().mo6893i(_f_dispose_0020_0028_0029V)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, _f_dispose_0020_0028_0029V, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, _f_dispose_0020_0028_0029V, new Object[0]));
                break;
        }
        m13971fg();
    }

    @C4034yP
    @C5566aOg
    @C2499fr(mo18857qh = 2)
    /* renamed from: f */
    public void mo1064f(Actor cr, C5260aCm acm, Vec3f vec3f, Vec3f vec3f2) {
        switch (bFf().mo6893i(f2644Wu)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f2644Wu, new Object[]{cr, acm, vec3f, vec3f2}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f2644Wu, new Object[]{cr, acm, vec3f, vec3f2}));
                break;
        }
        m13968e(cr, acm, vec3f, vec3f2);
    }

    @C4034yP
    @C2499fr(mo18855qf = {"b4248dc0637bb4cbfc146f6209da6d2d"})
    @ClientOnly
    /* renamed from: f */
    public void mo8543f(Vec3d ajr, Node rPVar, StellarSystem jj) {
        switch (bFf().mo6893i(dmC)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, dmC, new Object[]{ajr, rPVar, jj}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, dmC, new Object[]{ajr, rPVar, jj}));
                break;
        }
        m13969e(ajr, rPVar, jj);
    }

    public /* bridge */ /* synthetic */ Object getType() {
        switch (bFf().mo6893i(_f_getType_0020_0028_0029Ljava_002flang_002fObject_003b)) {
            case 0:
                return null;
            case 2:
                return bFf().mo5606d(new aCE(this, _f_getType_0020_0028_0029Ljava_002flang_002fObject_003b, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, _f_getType_0020_0028_0029Ljava_002flang_002fObject_003b, new Object[0]));
                break;
        }
        return m13983qW();
    }

    /* renamed from: hb */
    public Controller mo2998hb() {
        switch (bFf().mo6893i(bpJ)) {
            case 0:
                return null;
            case 2:
                return (Controller) bFf().mo5606d(new aCE(this, bpJ, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, bpJ, new Object[0]));
                break;
        }
        return afJ();
    }

    /* renamed from: iA */
    public String mo647iA() {
        switch (bFf().mo6893i(f2663vo)) {
            case 0:
                return null;
            case 2:
                return (String) bFf().mo5606d(new aCE(this, f2663vo, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f2663vo, new Object[0]));
                break;
        }
        return m13977iz();
    }

    /* renamed from: il */
    public C5260aCm mo8544il() {
        switch (bFf().mo6893i(f2653uW)) {
            case 0:
                return null;
            case 2:
                return (C5260aCm) bFf().mo5606d(new aCE(this, f2653uW, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f2653uW, new Object[0]));
                break;
        }
        return m13973ik();
    }

    /* renamed from: ip */
    public String mo648ip() {
        switch (bFf().mo6893i(f2658vc)) {
            case 0:
                return null;
            case 2:
                return (String) bFf().mo5606d(new aCE(this, f2658vc, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f2658vc, new Object[0]));
                break;
        }
        return m13974io();
    }

    /* renamed from: ir */
    public String mo649ir() {
        switch (bFf().mo6893i(f2659ve)) {
            case 0:
                return null;
            case 2:
                return (String) bFf().mo5606d(new aCE(this, f2659ve, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f2659ve, new Object[0]));
                break;
        }
        return m13975iq();
    }

    /* renamed from: iu */
    public String mo651iu() {
        switch (bFf().mo6893i(f2660vi)) {
            case 0:
                return null;
            case 2:
                return (String) bFf().mo5606d(new aCE(this, f2660vi, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f2660vi, new Object[0]));
                break;
        }
        return m13976it();
    }

    @C5566aOg
    /* renamed from: my */
    public C5260aCm mo8545my(float f) {
        switch (bFf().mo6893i(hHm)) {
            case 0:
                return null;
            case 2:
                return (C5260aCm) bFf().mo5606d(new aCE(this, hHm, new Object[]{new Float(f)}));
            case 3:
                bFf().mo5606d(new aCE(this, hHm, new Object[]{new Float(f)}));
                break;
        }
        return m13981mx(f);
    }

    public void push() {
        switch (bFf().mo6893i(_f_push_0020_0028_0029V)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, _f_push_0020_0028_0029V, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, _f_push_0020_0028_0029V, new Object[0]));
                break;
        }
        m13932Gd();
    }

    @C5566aOg
    @C2499fr
    /* renamed from: qV */
    public void mo656qV() {
        switch (bFf().mo6893i(f2630Lm)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f2630Lm, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f2630Lm, new Object[0]));
                break;
        }
        m13982qU();
    }

    /* renamed from: ra */
    public float mo1091ra() {
        switch (bFf().mo6893i(aTh)) {
            case 0:
                return 0.0f;
            case 2:
                return ((Float) bFf().mo5606d(new aCE(this, aTh, new Object[0]))).floatValue();
            case 3:
                bFf().mo5606d(new aCE(this, aTh, new Object[0]));
                break;
        }
        return m13938VJ();
    }

    /* renamed from: rb */
    public float mo1092rb() {
        switch (bFf().mo6893i(aTg)) {
            case 0:
                return 0.0f;
            case 2:
                return ((Float) bFf().mo5606d(new aCE(this, aTg, new Object[0]))).floatValue();
            case 3:
                bFf().mo5606d(new aCE(this, aTg, new Object[0]));
                break;
        }
        return m13937VI();
    }

    public void step(float f) {
        switch (bFf().mo6893i(_f_step_0020_0028F_0029V)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, _f_step_0020_0028F_0029V, new Object[]{new Float(f)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, _f_step_0020_0028F_0029V, new Object[]{new Float(f)}));
                break;
        }
        m13954ay(f);
    }

    /* renamed from: zt */
    public Hull mo8287zt() {
        switch (bFf().mo6893i(f2641Wl)) {
            case 0:
                return null;
            case 2:
                return (Hull) bFf().mo5606d(new aCE(this, f2641Wl, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f2641Wl, new Object[0]));
                break;
        }
        return m13989zs();
    }

    /* renamed from: zv */
    public Shield mo8288zv() {
        switch (bFf().mo6893i(f2642Wm)) {
            case 0:
                return null;
            case 2:
                return (Shield) bFf().mo5606d(new aCE(this, f2642Wm, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f2642Wm, new Object[0]));
                break;
        }
        return m13990zu();
    }

    /* renamed from: zx */
    public void mo1099zx() {
        switch (bFf().mo6893i(f2643Wp)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f2643Wp, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f2643Wp, new Object[0]));
                break;
        }
        m13991zw();
    }

    /* renamed from: a */
    public void mo8535a(MissileType avg) {
        super.mo967a((C2961mJ) avg);
        this.radius = -1.0f;
        mo1066f(avg.mo19891ke());
        if (bHa()) {
            mo8287zt().mo8348d(C2530gR.C2531a.class, this);
        }
    }

    @C0064Am(aul = "d34aac475ac216e2f46b6a554c30347d", aum = 0)
    /* renamed from: aG */
    private void m13948aG() {
        m13952av(new Vec3d(ScriptRuntime.NaN, ScriptRuntime.NaN, ScriptRuntime.NaN));
        super.mo70aH();
    }

    @C0064Am(aul = "e5a91e296792e5a2dc2dc4d03deabf29", aum = 0)
    /* renamed from: VE */
    private float m13935VE() {
        return aeO();
    }

    @C0064Am(aul = "246bc8d80435d04809109a316a3fe4a6", aum = 0)
    /* renamed from: VG */
    private float m13936VG() {
        return aeP();
    }

    @C0064Am(aul = "1820e9a2a183beeddf1ad801999ce233", aum = 0)
    /* renamed from: VI */
    private float m13937VI() {
        return aeS();
    }

    @C0064Am(aul = "2d61d9475ca62b321bba2baf6e2d1056", aum = 0)
    /* renamed from: VJ */
    private float m13938VJ() {
        return aeT();
    }

    @C0064Am(aul = "b66a0842f67e93a4dd504600fb6323b6", aum = 0)
    private Controller afJ() {
        return null;
    }

    @C0064Am(aul = "38895687a8de5f8f00ee171e287d4e3b", aum = 0)
    /* renamed from: iz */
    private String m13977iz() {
        return ala().aIW().avu();
    }

    @C0064Am(aul = "1a4c9fc6bed1bdfd1cda3fcefed22aea", aum = 0)
    @C5307aEh
    /* renamed from: a */
    private void m13939a(long j, boolean z) {
        super.mo991b(j, z);
    }

    @C0064Am(aul = "919bc1bc7a71db8c6c9eefcb14e9c227", aum = 0)
    /* renamed from: ay */
    private void m13954ay(float f) {
        super.step(f);
        if (bGY() && !isDisposed() && !isDead()) {
            this.f2665Wf += f;
            if (this.f2665Wf >= ((float) m13970eG())) {
                this.f2665Wf = 0.0f;
                cXQ();
            }
        }
        if (bGX()) {
            m13979jh(true);
        }
    }

    @C4034yP
    @C0064Am(aul = "48e5c27f00e8d1b55c6ab776ed71dc48", aum = 0)
    @C2499fr
    @C1253SX
    private void cXP() {
        mo8538at(getPosition());
    }

    @C0064Am(aul = "0b3ad6f728356dc1430a99978f4bd07a", aum = 0)
    /* renamed from: b */
    private void m13957b(Hull jRVar, C5260aCm acm, Actor cr) {
        if (jRVar == m13985zi()) {
            Controller hb = ((Pawn) cr).mo2998hb();
            if (bae() && (cr instanceof Mine)) {
                mo8543f(getPosition(), azW(), mo960Nc());
            }
            if (hb instanceof PlayerController) {
                m13963d((PlayerController) hb);
                mo16606b(acm, (Pawn) cr);
            }
            cXQ();
        }
    }

    @C0064Am(aul = "b164970fed13fb1f1d8f597f6a7175a6", aum = 0)
    /* renamed from: c */
    private void m13960c(Actor cr, C5260aCm acm, Vec3f vec3f, Vec3f vec3f2) {
    }

    @C0064Am(aul = "5be022e150314dec38328b83453670ce", aum = 0)
    /* renamed from: c */
    private void m13962c(PlayerController tVar) {
        tVar.mo22135dL().mo14419f((C1506WA) new C0284Df(((NLSConsoleAlert) ala().aIY().mo6310c(NLSManager.C1472a.CONSOLEALERT)).bbG(), true, C6128agw.C1890a.RED, getName()));
    }

    @C0064Am(aul = "1d2cab132ee3e89b6a57f5ec567b1af3", aum = 0)
    /* renamed from: io */
    private String m13974io() {
        return cXS().mo3521Nu().getHandle();
    }

    @C0064Am(aul = "70a8895c09c767c47a0fc57692e42aba", aum = 0)
    /* renamed from: iq */
    private String m13975iq() {
        return cXS().mo3521Nu().getFile();
    }

    @C0064Am(aul = "819d52b42fa2b7ee85edab85f140fddc", aum = 0)
    @C5307aEh
    /* renamed from: a */
    private void m13943a(Actor cr, Vec3f vec3f, Vec3f vec3f2, float f, C6339akz akz) {
        C5260aCm acm;
        Vec3d position;
        super.mo992b(cr, vec3f, vec3f2, f, akz);
        if (!(cr instanceof Shot) && !(cr instanceof Missile) && bGY() && !isDisposed()) {
            if (cr == null) {
                System.err.println("OnCollide: other is null!!");
            } else if (cr == cLw()) {
            } else {
                if (cr instanceof C0286Dh) {
                    mo1099zx();
                    m13952av(cr.getPosition());
                } else if ((cr instanceof Actor) && !cr.equals(cLw())) {
                    if (!(cr instanceof Shot) || !((Shot) cr).cLw().equals(cLw())) {
                        if (m13972id() != null) {
                            acm = new C5260aCm(m13972id(), cXS().mo11042iu(), agB());
                            if (m13972id().cPJ() != null) {
                                acm.mo8232w(m13972id().cPJ());
                            }
                        } else {
                            acm = new C5260aCm((Map<DamageType, Float>) cXS().bpA(), cXS().mo11042iu(), agB());
                        }
                        Vec3d ajr = new Vec3d();
                        if (cr.aiD() != null) {
                            cr.mo995b(cr.aiD().mo2472kQ(), (Actor) cLw(), acm, vec3f2, vec3f);
                            ajr.mo9484aA(cr.aiD().getPosition());
                            position = ajr;
                        } else {
                            position = getPosition();
                            mo6317hy("Missile " + this + " collide on actor " + cr + " with null solid.");
                        }
                        mo1099zx();
                        m13952av(position);
                    }
                }
            }
        }
    }

    @C0064Am(aul = "06b0156d0aeffbbfdc014677662ec69b", aum = 0)
    /* renamed from: Gd */
    private void m13932Gd() {
        super.push();
        if (cXN() != null) {
            cXN().push();
        }
    }

    @C0064Am(aul = "79e3bf4bc5ff6607e1bfa24a088ab8bb", aum = 0)
    private MissileType cXR() {
        return (MissileType) super.getType();
    }

    @C4034yP
    @ClientOnly
    @C0064Am(aul = "8cfc0d184dbfed95cbd0fdd8c7983ec0", aum = 0)
    @C2499fr(mo18855qf = {"b4248dc0637bb4cbfc146f6209da6d2d"})
    /* renamed from: e */
    private void m13969e(Vec3d ajr, Node rPVar, StellarSystem jj) {
        if (mo1000b(rPVar, jj) && cXS() != null && bGX()) {
            new C3257pi().mo21209a(m13987zl(), ajr, C3257pi.C3261d.MID_RANGE);
            new C3257pi().mo21210a(m13988zm(), ajr, C3257pi.C3261d.MID_RANGE, this.radius * 2.0f);
            m13979jh(false);
        }
    }

    @C4034yP
    @C0064Am(aul = "24cb154bb7c3a4e1ed668160572fe11f", aum = 0)
    /* renamed from: c */
    private void m13961c(Vec3d ajr, Node rPVar, StellarSystem jj) {
        mo8543f(ajr, rPVar, jj);
        if (mo2998hb() != null) {
            mo2998hb().aYg();
        }
    }

    @C0064Am(aul = "3c78b8a668ce4c663217cd45091c0434", aum = 0)
    /* renamed from: a */
    private void m13947a(C4029yK yKVar) {
        yKVar.setDisableOnColision(false);
        yKVar.setTransparent(false);
        super.mo999b(yKVar);
    }

    @C0064Am(aul = "4ecfff626293f255b676525b96daa4b1", aum = 0)
    /* renamed from: a */
    private void m13942a(Actor cr, C5260aCm acm, Vec3f vec3f, Vec3f vec3f2) {
        mo8360mc("takeClientDamage : causer " + cr + " on " + getName());
        mo1073h(cr, acm, vec3f, vec3f2);
    }

    @C0064Am(aul = "7a41aff66c3ba26e286ff712c1ab9490", aum = 0)
    /* renamed from: it */
    private String m13976it() {
        return cXS().mo11042iu();
    }

    @C0064Am(aul = "2f99ca48bf585566f8435dce6f2d5005", aum = 0)
    /* renamed from: fg */
    private void m13971fg() {
        super.dispose();
        m13949ap((Ship) null);
    }

    @C0064Am(aul = "af1625948a084c0bd6672131f1e4e997", aum = 0)
    private Actor agA() {
        return cXN();
    }

    @C0064Am(aul = "e5543a457b982442a285612d18c09468", aum = 0)
    /* renamed from: a */
    private void m13944a(StellarSystem jj) {
        super.mo993b(jj);
        if (bHa() && cXN() != null && (cXN().mo2998hb() instanceof PlayerController)) {
            ((PlayerController) cXN().mo2998hb()).mo22135dL().mo14419f((C1506WA) new C2783jw(C2783jw.C2784a.SPAWNED));
        }
    }

    @C0064Am(aul = "15fefe0fe89e1ff33bffa8fe76b94f8a", aum = 0)
    /* renamed from: zw */
    private void m13991zw() {
        if (bGZ() && bae() && cXN() != null && (cXN().mo2998hb() instanceof PlayerController)) {
            ((PlayerController) cXN().mo2998hb()).mo22135dL().mo14419f((C1506WA) new C2783jw(C2783jw.C2784a.UNSPAWNED));
        }
        super.mo1099zx();
    }

    @C0064Am(aul = "f277421a4ed34a9b55aca10ca6977a0e", aum = 0)
    @ClientOnly
    /* renamed from: jg */
    private void m13978jg(boolean z) {
        if (z && cXS().dAR() != null && this.hHl == null) {
            this.hHl = new C3257pi();
            this.hHl.mo21213a(cXS().dAR(), new Vec3d(ScriptRuntime.NaN, ScriptRuntime.NaN, ScriptRuntime.NaN), cHg(), C3257pi.C3261d.NEAR_ENOUGH);
        } else if (!z && this.hHl != null) {
            this.hHl.asy();
            this.hHl = null;
        }
    }

    @C0064Am(aul = "02f2acae26dc5c9942d16e67e12ce442", aum = 0)
    /* renamed from: zs */
    private Hull m13989zs() {
        return m13985zi();
    }

    @C0064Am(aul = "cf680585f702b4ce0b2ad69f6949d575", aum = 0)
    /* renamed from: zu */
    private Shield m13990zu() {
        return null;
    }

    @C0064Am(aul = "3e9ea3304dc906cc5f1c46cb85626ee3", aum = 0)
    /* renamed from: ik */
    private C5260aCm m13973ik() {
        return m13972id();
    }

    @C0064Am(aul = "ae35031b27aa469a9943b5eba601a9af", aum = 0)
    /* renamed from: b */
    private void m13956b(C5260aCm acm) {
        m13945a(acm);
    }

    @C0064Am(aul = "3ba53101e0c067a6e558d521c2bc9e50", aum = 0)
    private void ahT() {
        if (this.hks != null) {
            Scene adZ = C5916acs.getSingolton().getEngineGraphics().adZ();
            for (RParticleSystem next : SceneHelper.getChildInstancesAndRemove(this.hks, RParticleSystem.class)) {
                adZ.addChild(next);
                next.setSystemLifeTime(0.01f);
            }
        }
        super.ahU();
    }

    @C0064Am(aul = "a11e1f9b99bc2db7b0debba8c6caae58", aum = 0)
    /* renamed from: b */
    private void m13955b(MissileWeapon bp) {
        m13940a(bp);
    }

    @C0064Am(aul = "1c75ee9307ed1769476b6ce23404ce44", aum = 0)
    /* renamed from: c */
    private C0520HN m13959c(Space ea, long j) {
        Quat4fWrap orientation;
        long j2 = j + 500;
        Ship al = cXO().mo7855al();
        Vec3d position = al.getPosition();
        C6853aut agh = al.agh();
        C6661arJ cUn = cXO().cUn();
        if (agh != null) {
            position = position.mo9531q(al.getOrientation().mo15209aT(agh.mo16402gL()));
            orientation = al.getOrientation().mo15233d((Quat4f) agh.getOrientation());
        } else {
            orientation = cUn.getOrientation();
        }
        mo988aj(position);
        mo1086l(orientation);
        Vec3f qZ = cUn.agv().mo1090qZ();
        mo1016bz(al.cLy());
        mo964W((qZ.length() + 5.0f) * 1.1f);
        C6486anq anq = new C6486anq(ea, this, mo958IL());
        anq.mo2449a(ea.cKn().mo5725a(j2, (C2235dB) anq, 11));
        return anq;
    }

    @C0064Am(aul = "4f6bb191a528e6d60b4dc7ccbd8d5a4a", aum = 0)
    /* renamed from: a */
    private void m13941a(Actor.C0200h hVar) {
        hVar.mo1103c(new C6348alI(1.0f));
    }

    @C0064Am(aul = "7831f7be91e0e64fff93245aa917e821", aum = 0)
    private void ahp() {
        super.mo957Fe();
        this.radius = (float) this.hks.getAabbLSLenght();
    }
}
