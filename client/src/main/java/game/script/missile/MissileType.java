package game.script.missile;

import game.network.message.externalizable.aCE;
import game.script.resource.Asset;
import game.script.ship.HullType;
import logic.baa.*;
import logic.data.link.C5941adR;
import logic.data.mbean.C3609st;
import logic.res.code.C5640aRc;
import logic.res.code.C5663aRz;
import logic.res.code.C6494any;
import logic.res.html.C2491fm;
import p001a.*;

import java.util.Collection;

@C0566Hp
@C5829abJ("2.1.1")
@C6485anp
@C5511aMd
/* renamed from: a.aVg  reason: case insensitive filesystem */
/* compiled from: a */
public class MissileType extends BaseMissileType implements C1616Xf {

    /* renamed from: VW */
    public static final C5663aRz f3936VW = null;
    /* renamed from: VY */
    public static final C5663aRz f3938VY = null;
    /* renamed from: Wc */
    public static final C5663aRz f3940Wc = null;
    /* renamed from: We */
    public static final C5663aRz f3942We = null;
    /* renamed from: Wt */
    public static final C2491fm f3943Wt = null;
    public static final int _m_fieldCount = 0;
    public static final C5663aRz[] _m_fields = null;
    public static final int _m_methodCount = 0;
    public static final C2491fm[] _m_methods = null;
    public static final C2491fm aDR = null;
    public static final C2491fm aTe = null;
    public static final C2491fm aTf = null;
    public static final C2491fm aTg = null;
    public static final C2491fm aTh = null;
    public static final C2491fm aTr = null;
    public static final C5663aRz bnO = null;
    public static final C5663aRz bnP = null;
    public static final C5663aRz bnU = null;
    public static final C5663aRz bnV = null;
    public static final C2491fm dGA = null;
    public static final C2491fm dGB = null;
    public static final C2491fm dGQ = null;
    public static final C2491fm dGR = null;
    public static final C2491fm dGS = null;
    public static final C2491fm dGT = null;
    public static final C2491fm dGy = null;
    public static final C2491fm dGz = null;
    public static final C2491fm dHz = null;
    /* renamed from: dN */
    public static final C2491fm f3944dN = null;
    public static final C5663aRz hHi = null;
    public static final C2491fm iYu = null;
    public static final C2491fm iYv = null;
    public static final C2491fm iYw = null;
    public static final C2491fm iYx = null;
    public static final C2491fm iYy = null;
    public static final C2491fm ipx = null;
    /* renamed from: kZ */
    public static final C5663aRz f3948kZ = null;
    public static final long serialVersionUID = 0;
    public static C6494any ___iScriptClass;
    @C0064Am(aul = "dab466e2c4c41e54388b1e5930ee5446", aum = 5)

    /* renamed from: VX */
    private static float f3937VX;
    @C0064Am(aul = "4f8c3f5e7323b2d6aae1ba16619a3881", aum = 6)

    /* renamed from: Wb */
    private static Asset f3939Wb;
    @C0064Am(aul = "69dda8d7cb9c32365e5927f99c7956ad", aum = 7)

    /* renamed from: Wd */
    private static Asset f3941Wd;
    @C0064Am(aul = "726dffb2106d93f0fce2c59378fd4566", aum = 0)
    private static float bhk;
    @C0064Am(aul = "beef6fa59d3e344243bf03b6890b05f2", aum = 2)
    private static float bhl;
    @C0064Am(aul = "a6c56c8864cefc6386a1dda44a1e2c9c", aum = 8)
    private static Asset bhm;
    @C0064Am(aul = "9799d9e319f41e8f250c5773ab267329", aum = 9)
    private static HullType bhn;
    @C0064Am(aul = "d159e8ef9426ab6f8a872fa0b8eb0f03", aum = 1)

    /* renamed from: dW */
    private static float f3945dW;
    @C0064Am(aul = "ff0c2daf3aefe7bbce78583612342d7f", aum = 3)

    /* renamed from: dX */
    private static float f3946dX;
    @C0064Am(aul = "f03f6d4767429c76c8f3239fd4b3a5ac", aum = 4)

    /* renamed from: kY */
    private static long f3947kY;

    static {
        m18899V();
    }

    public MissileType() {
        super((C5540aNg) null);
        super.mo10S();
    }

    public MissileType(C5540aNg ang) {
        super(ang);
    }

    /* renamed from: V */
    static void m18899V() {
        serialVersionUID = (long) 1;
        _m_fieldCount = BaseMissileType._m_fieldCount + 10;
        _m_methodCount = BaseMissileType._m_methodCount + 23;
        int i = BaseMissileType._m_fieldCount;
        C5663aRz[] arzArr = new C5663aRz[(i + 10)];
        C5663aRz b = C5640aRc.m17844b(MissileType.class, "726dffb2106d93f0fce2c59378fd4566", i);
        bnU = b;
        arzArr[i] = b;
        int i2 = i + 1;
        C5663aRz b2 = C5640aRc.m17844b(MissileType.class, "d159e8ef9426ab6f8a872fa0b8eb0f03", i2);
        bnV = b2;
        arzArr[i2] = b2;
        int i3 = i2 + 1;
        C5663aRz b3 = C5640aRc.m17844b(MissileType.class, "beef6fa59d3e344243bf03b6890b05f2", i3);
        bnO = b3;
        arzArr[i3] = b3;
        int i4 = i3 + 1;
        C5663aRz b4 = C5640aRc.m17844b(MissileType.class, "ff0c2daf3aefe7bbce78583612342d7f", i4);
        bnP = b4;
        arzArr[i4] = b4;
        int i5 = i4 + 1;
        C5663aRz b5 = C5640aRc.m17844b(MissileType.class, "f03f6d4767429c76c8f3239fd4b3a5ac", i5);
        f3948kZ = b5;
        arzArr[i5] = b5;
        int i6 = i5 + 1;
        C5663aRz b6 = C5640aRc.m17844b(MissileType.class, "dab466e2c4c41e54388b1e5930ee5446", i6);
        f3938VY = b6;
        arzArr[i6] = b6;
        int i7 = i6 + 1;
        C5663aRz b7 = C5640aRc.m17844b(MissileType.class, "4f8c3f5e7323b2d6aae1ba16619a3881", i7);
        f3940Wc = b7;
        arzArr[i7] = b7;
        int i8 = i7 + 1;
        C5663aRz b8 = C5640aRc.m17844b(MissileType.class, "69dda8d7cb9c32365e5927f99c7956ad", i8);
        f3942We = b8;
        arzArr[i8] = b8;
        int i9 = i8 + 1;
        C5663aRz b9 = C5640aRc.m17844b(MissileType.class, "a6c56c8864cefc6386a1dda44a1e2c9c", i9);
        hHi = b9;
        arzArr[i9] = b9;
        int i10 = i9 + 1;
        C5663aRz b10 = C5640aRc.m17844b(MissileType.class, "9799d9e319f41e8f250c5773ab267329", i10);
        f3936VW = b10;
        arzArr[i10] = b10;
        int i11 = i10 + 1;
        _m_fields = arzArr;
        C1634Xv.m11725a((Object[]) BaseMissileType._m_fields, (Object[]) _m_fields);
        int i12 = BaseMissileType._m_methodCount;
        C2491fm[] fmVarArr = new C2491fm[(i12 + 23)];
        C2491fm a = C4105zY.m41624a(MissileType.class, "603d454f4a3e829f504243faf5a20ad2", i12);
        aTg = a;
        fmVarArr[i12] = a;
        int i13 = i12 + 1;
        C2491fm a2 = C4105zY.m41624a(MissileType.class, "926ccb514c561b711e4e3f7015158396", i13);
        dGy = a2;
        fmVarArr[i13] = a2;
        int i14 = i13 + 1;
        C2491fm a3 = C4105zY.m41624a(MissileType.class, "a2d5f6c6282a6b30f2b7ec28a96d55a6", i14);
        aTh = a3;
        fmVarArr[i14] = a3;
        int i15 = i14 + 1;
        C2491fm a4 = C4105zY.m41624a(MissileType.class, "fd518c2b044c82891db278b4f7b88c1a", i15);
        dGz = a4;
        fmVarArr[i15] = a4;
        int i16 = i15 + 1;
        C2491fm a5 = C4105zY.m41624a(MissileType.class, "5012ccd878e22404a9891bdfc8414237", i16);
        aTe = a5;
        fmVarArr[i16] = a5;
        int i17 = i16 + 1;
        C2491fm a6 = C4105zY.m41624a(MissileType.class, "d07384f089b3ba6e412d3e0763ffcba6", i17);
        dGA = a6;
        fmVarArr[i17] = a6;
        int i18 = i17 + 1;
        C2491fm a7 = C4105zY.m41624a(MissileType.class, "7c872f7901612dc4384dfd95c26c7980", i18);
        aTf = a7;
        fmVarArr[i18] = a7;
        int i19 = i18 + 1;
        C2491fm a8 = C4105zY.m41624a(MissileType.class, "06976ee92b824a85f27efc3d50fc396c", i19);
        dGB = a8;
        fmVarArr[i19] = a8;
        int i20 = i19 + 1;
        C2491fm a9 = C4105zY.m41624a(MissileType.class, "95d1a52f7097558b8d165b6fe49ba822", i20);
        iYu = a9;
        fmVarArr[i20] = a9;
        int i21 = i20 + 1;
        C2491fm a10 = C4105zY.m41624a(MissileType.class, "fe441a0a32524c60b0703ffea185428c", i21);
        iYv = a10;
        fmVarArr[i21] = a10;
        int i22 = i21 + 1;
        C2491fm a11 = C4105zY.m41624a(MissileType.class, "ac1a7abd6a2a57d762f479c12d089158", i22);
        f3943Wt = a11;
        fmVarArr[i22] = a11;
        int i23 = i22 + 1;
        C2491fm a12 = C4105zY.m41624a(MissileType.class, "98aa129b1bb074c22bf79dcdd465653e", i23);
        ipx = a12;
        fmVarArr[i23] = a12;
        int i24 = i23 + 1;
        C2491fm a13 = C4105zY.m41624a(MissileType.class, "de00a9bb92e6ce02e88efc1bf07989d5", i24);
        dGQ = a13;
        fmVarArr[i24] = a13;
        int i25 = i24 + 1;
        C2491fm a14 = C4105zY.m41624a(MissileType.class, "e30fe576d479d79f9bba8a5914ff0204", i25);
        dGR = a14;
        fmVarArr[i25] = a14;
        int i26 = i25 + 1;
        C2491fm a15 = C4105zY.m41624a(MissileType.class, "30080eef1b1cb2ff1f182e0f48f51ed1", i26);
        dGS = a15;
        fmVarArr[i26] = a15;
        int i27 = i26 + 1;
        C2491fm a16 = C4105zY.m41624a(MissileType.class, "b3de4318a85cb867bdb6f228bd493227", i27);
        dGT = a16;
        fmVarArr[i27] = a16;
        int i28 = i27 + 1;
        C2491fm a17 = C4105zY.m41624a(MissileType.class, "67b80d2142cf0b20dc368b86e9884442", i28);
        iYw = a17;
        fmVarArr[i28] = a17;
        int i29 = i28 + 1;
        C2491fm a18 = C4105zY.m41624a(MissileType.class, "00737683366a65905c3d14ab02989c73", i29);
        iYx = a18;
        fmVarArr[i29] = a18;
        int i30 = i29 + 1;
        C2491fm a19 = C4105zY.m41624a(MissileType.class, "0c58ed8d02dbaebdb17225dad57bc86e", i30);
        aTr = a19;
        fmVarArr[i30] = a19;
        int i31 = i30 + 1;
        C2491fm a20 = C4105zY.m41624a(MissileType.class, "9f91e0cf8075f5fda61dae486dd1ae43", i31);
        dHz = a20;
        fmVarArr[i31] = a20;
        int i32 = i31 + 1;
        C2491fm a21 = C4105zY.m41624a(MissileType.class, "0c05726e8cb4f25469db54da440daa88", i32);
        aDR = a21;
        fmVarArr[i32] = a21;
        int i33 = i32 + 1;
        C2491fm a22 = C4105zY.m41624a(MissileType.class, "806aba3e6a040cdcab41b780d615522e", i33);
        iYy = a22;
        fmVarArr[i33] = a22;
        int i34 = i33 + 1;
        C2491fm a23 = C4105zY.m41624a(MissileType.class, "74ad19e847d2984cbdba7d16f94c32ce", i34);
        f3944dN = a23;
        fmVarArr[i34] = a23;
        int i35 = i34 + 1;
        _m_methods = fmVarArr;
        C1634Xv.m11725a((Object[]) BaseMissileType._m_methods, (Object[]) _m_methods);
        ___iScriptClass = aUO.m18566a(MissileType.class, C3609st.class, _m_fields, _m_methods);
    }

    /* renamed from: A */
    private void m18898A(Asset tCVar) {
        bFf().mo5608dq().mo3197f(f3942We, tCVar);
    }

    private float aeO() {
        return bFf().mo5608dq().mo3211m(bnO);
    }

    private float aeP() {
        return bFf().mo5608dq().mo3211m(bnP);
    }

    private float aeS() {
        return bFf().mo5608dq().mo3211m(bnU);
    }

    private float aeT() {
        return bFf().mo5608dq().mo3211m(bnV);
    }

    /* renamed from: aw */
    private void m18908aw(float f) {
        bFf().mo5608dq().mo3150a(f3938VY, f);
    }

    /* renamed from: bY */
    private void m18909bY(Asset tCVar) {
        bFf().mo5608dq().mo3197f(hHi, tCVar);
    }

    private HullType bjZ() {
        return (HullType) bFf().mo5608dq().mo3214p(f3936VW);
    }

    private Asset cXM() {
        return (Asset) bFf().mo5608dq().mo3214p(hHi);
    }

    /* renamed from: dt */
    private void m18911dt(float f) {
        bFf().mo5608dq().mo3150a(bnO, f);
    }

    /* renamed from: du */
    private void m18912du(float f) {
        bFf().mo5608dq().mo3150a(bnP, f);
    }

    /* renamed from: dx */
    private void m18913dx(float f) {
        bFf().mo5608dq().mo3150a(bnU, f);
    }

    /* renamed from: dy */
    private void m18914dy(float f) {
        bFf().mo5608dq().mo3150a(bnV, f);
    }

    /* renamed from: eG */
    private long m18916eG() {
        return bFf().mo5608dq().mo3213o(f3948kZ);
    }

    /* renamed from: f */
    private void m18917f(HullType wHVar) {
        bFf().mo5608dq().mo3197f(f3936VW, wHVar);
    }

    /* renamed from: l */
    private void m18923l(long j) {
        bFf().mo5608dq().mo3184b(f3948kZ, j);
    }

    /* renamed from: z */
    private void m18926z(Asset tCVar) {
        bFf().mo5608dq().mo3197f(f3940Wc, tCVar);
    }

    /* renamed from: zj */
    private float m18928zj() {
        return bFf().mo5608dq().mo3211m(f3938VY);
    }

    /* renamed from: zl */
    private Asset m18929zl() {
        return (Asset) bFf().mo5608dq().mo3214p(f3940Wc);
    }

    /* renamed from: zm */
    private Asset m18930zm() {
        return (Asset) bFf().mo5608dq().mo3214p(f3942We);
    }

    /* renamed from: T */
    public C6494any mo11T() {
        return ___iScriptClass;
    }

    /* renamed from: U */
    public C2491fm[] mo12U() {
        return _m_methods;
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Max Angular Acceleration")
    /* renamed from: VF */
    public float mo11851VF() {
        switch (bFf().mo6893i(aTe)) {
            case 0:
                return 0.0f;
            case 2:
                return ((Float) bFf().mo5606d(new aCE(this, aTe, new Object[0]))).floatValue();
            case 3:
                bFf().mo5606d(new aCE(this, aTe, new Object[0]));
                break;
        }
        return m18900VE();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Max Angular Velocity")
    /* renamed from: VH */
    public float mo11852VH() {
        switch (bFf().mo6893i(aTf)) {
            case 0:
                return 0.0f;
            case 2:
                return ((Float) bFf().mo5606d(new aCE(this, aTf, new Object[0]))).floatValue();
            case 3:
                bFf().mo5606d(new aCE(this, aTf, new Object[0]));
                break;
        }
        return m18901VG();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Hull")
    /* renamed from: VT */
    public HullType mo11853VT() {
        switch (bFf().mo6893i(aTr)) {
            case 0:
                return null;
            case 2:
                return (HullType) bFf().mo5606d(new aCE(this, aTr, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, aTr, new Object[0]));
                break;
        }
        return m18904VS();
    }

    /* renamed from: W */
    public Object mo13W() {
        return new C3609st(this);
    }

    /* renamed from: a */
    public Object mo14a(C0495Gr gr) {
        Object[] args = gr.getArgs();
        switch (gr.mo2417hq() - BaseMissileType._m_methodCount) {
            case 0:
                return new Float(m18902VI());
            case 1:
                m18919gA(((Float) args[0]).floatValue());
                return null;
            case 2:
                return new Float(m18903VJ());
            case 3:
                m18920gB(((Float) args[0]).floatValue());
                return null;
            case 4:
                return new Float(m18900VE());
            case 5:
                m18921gC(((Float) args[0]).floatValue());
                return null;
            case 6:
                return new Float(m18901VG());
            case 7:
                m18922gD(((Float) args[0]).floatValue());
                return null;
            case 8:
                return new Long(dAO());
            case 9:
                m18924mj(((Long) args[0]).longValue());
                return null;
            case 10:
                return new Float(m18927zC());
            case 11:
                m18925nb(((Float) args[0]).floatValue());
                return null;
            case 12:
                return bkg();
            case 13:
                m18905aR((Asset) args[0]);
                return null;
            case 14:
                return bki();
            case 15:
                m18907aT((Asset) args[0]);
                return null;
            case 16:
                return dAQ();
            case 17:
                m18910cp((Asset) args[0]);
                return null;
            case 18:
                return m18904VS();
            case 19:
                m18918g((HullType) args[0]);
                return null;
            case 20:
                m18915e((aDJ) args[0]);
                return null;
            case 21:
                return dAS();
            case 22:
                return m18906aT();
            default:
                return super.mo14a(gr);
        }
    }

    /* renamed from: a */
    public void mo15a(Collection collection, C0495Gr gr) {
        super.mo15a(collection, gr);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Explosion G F X")
    /* renamed from: aS */
    public void mo11854aS(Asset tCVar) {
        switch (bFf().mo6893i(dGR)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, dGR, new Object[]{tCVar}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, dGR, new Object[]{tCVar}));
                break;
        }
        m18905aR(tCVar);
    }

    /* access modifiers changed from: protected */
    /* renamed from: aU */
    public <T extends aDJ> T mo745aU() {
        switch (bFf().mo6893i(f3944dN)) {
            case 0:
                return null;
            case 2:
                return (aDJ) bFf().mo5606d(new aCE(this, f3944dN, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f3944dN, new Object[0]));
                break;
        }
        return m18906aT();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Explosion S F X")
    /* renamed from: aU */
    public void mo11855aU(Asset tCVar) {
        switch (bFf().mo6893i(dGT)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, dGT, new Object[]{tCVar}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, dGT, new Object[]{tCVar}));
                break;
        }
        m18907aT(tCVar);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Max Angular Velocity")
    /* renamed from: as */
    public void mo11856as(float f) {
        switch (bFf().mo6893i(dGB)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, dGB, new Object[]{new Float(f)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, dGB, new Object[]{new Float(f)}));
                break;
        }
        m18922gD(f);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Max Angular Acceleration")
    /* renamed from: at */
    public void mo11857at(float f) {
        switch (bFf().mo6893i(dGA)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, dGA, new Object[]{new Float(f)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, dGA, new Object[]{new Float(f)}));
                break;
        }
        m18921gC(f);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Max Linear Acceleration")
    /* renamed from: au */
    public void mo11858au(float f) {
        switch (bFf().mo6893i(dGy)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, dGy, new Object[]{new Float(f)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, dGy, new Object[]{new Float(f)}));
                break;
        }
        m18919gA(f);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Max Linear Velocity")
    /* renamed from: av */
    public void mo11859av(float f) {
        switch (bFf().mo6893i(dGz)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, dGz, new Object[]{new Float(f)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, dGz, new Object[]{new Float(f)}));
                break;
        }
        m18920gB(f);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Explosion G F X")
    public Asset bkh() {
        switch (bFf().mo6893i(dGQ)) {
            case 0:
                return null;
            case 2:
                return (Asset) bFf().mo5606d(new aCE(this, dGQ, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, dGQ, new Object[0]));
                break;
        }
        return bkg();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Explosion S F X")
    public Asset bkj() {
        switch (bFf().mo6893i(dGS)) {
            case 0:
                return null;
            case 2:
                return (Asset) bFf().mo5606d(new aCE(this, dGS, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, dGS, new Object[0]));
                break;
        }
        return bki();
    }

    /* renamed from: c */
    public C5663aRz[] mo25c() {
        return _m_fields;
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Loop S F X")
    /* renamed from: cq */
    public void mo11862cq(Asset tCVar) {
        switch (bFf().mo6893i(iYx)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, iYx, new Object[]{tCVar}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, iYx, new Object[]{tCVar}));
                break;
        }
        m18910cp(tCVar);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Life Time (s)")
    public long dAP() {
        switch (bFf().mo6893i(iYu)) {
            case 0:
                return 0;
            case 2:
                return ((Long) bFf().mo5606d(new aCE(this, iYu, new Object[0]))).longValue();
            case 3:
                bFf().mo5606d(new aCE(this, iYu, new Object[0]));
                break;
        }
        return dAO();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Loop S F X")
    public Asset dAR() {
        switch (bFf().mo6893i(iYw)) {
            case 0:
                return null;
            case 2:
                return (Asset) bFf().mo5606d(new aCE(this, iYw, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, iYw, new Object[0]));
                break;
        }
        return dAQ();
    }

    public Missile dAT() {
        switch (bFf().mo6893i(iYy)) {
            case 0:
                return null;
            case 2:
                return (Missile) bFf().mo5606d(new aCE(this, iYy, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, iYy, new Object[0]));
                break;
        }
        return dAS();
    }

    /* renamed from: f */
    public void mo2631f(aDJ adj) {
        switch (bFf().mo6893i(aDR)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, aDR, new Object[]{adj}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, aDR, new Object[]{adj}));
                break;
        }
        m18915e(adj);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Hull")
    /* renamed from: h */
    public void mo11866h(HullType wHVar) {
        switch (bFf().mo6893i(dHz)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, dHz, new Object[]{wHVar}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, dHz, new Object[]{wHVar}));
                break;
        }
        m18918g(wHVar);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Life Time (s)")
    /* renamed from: mk */
    public void mo11867mk(long j) {
        switch (bFf().mo6893i(iYv)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, iYv, new Object[]{new Long(j)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, iYv, new Object[]{new Long(j)}));
                break;
        }
        m18924mj(j);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Explosion Radius")
    /* renamed from: nc */
    public void mo11868nc(float f) {
        switch (bFf().mo6893i(ipx)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, ipx, new Object[]{new Float(f)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, ipx, new Object[]{new Float(f)}));
                break;
        }
        m18925nb(f);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Max Linear Velocity")
    /* renamed from: ra */
    public float mo11869ra() {
        switch (bFf().mo6893i(aTh)) {
            case 0:
                return 0.0f;
            case 2:
                return ((Float) bFf().mo5606d(new aCE(this, aTh, new Object[0]))).floatValue();
            case 3:
                bFf().mo5606d(new aCE(this, aTh, new Object[0]));
                break;
        }
        return m18903VJ();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Max Linear Acceleration")
    /* renamed from: rb */
    public float mo11870rb() {
        switch (bFf().mo6893i(aTg)) {
            case 0:
                return 0.0f;
            case 2:
                return ((Float) bFf().mo5606d(new aCE(this, aTg, new Object[0]))).floatValue();
            case 3:
                bFf().mo5606d(new aCE(this, aTg, new Object[0]));
                break;
        }
        return m18902VI();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Explosion Radius")
    /* renamed from: zD */
    public float mo11871zD() {
        switch (bFf().mo6893i(f3943Wt)) {
            case 0:
                return 0.0f;
            case 2:
                return ((Float) bFf().mo5606d(new aCE(this, f3943Wt, new Object[0]))).floatValue();
            case 3:
                bFf().mo5606d(new aCE(this, f3943Wt, new Object[0]));
                break;
        }
        return m18927zC();
    }

    /* renamed from: S */
    public void mo10S() {
        super.mo10S();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Max Linear Acceleration")
    @C0064Am(aul = "603d454f4a3e829f504243faf5a20ad2", aum = 0)
    /* renamed from: VI */
    private float m18902VI() {
        return aeS();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Max Linear Acceleration")
    @C0064Am(aul = "926ccb514c561b711e4e3f7015158396", aum = 0)
    /* renamed from: gA */
    private void m18919gA(float f) {
        m18913dx(f);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Max Linear Velocity")
    @C0064Am(aul = "a2d5f6c6282a6b30f2b7ec28a96d55a6", aum = 0)
    /* renamed from: VJ */
    private float m18903VJ() {
        return aeT();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Max Linear Velocity")
    @C0064Am(aul = "fd518c2b044c82891db278b4f7b88c1a", aum = 0)
    /* renamed from: gB */
    private void m18920gB(float f) {
        m18914dy(f);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Max Angular Acceleration")
    @C0064Am(aul = "5012ccd878e22404a9891bdfc8414237", aum = 0)
    /* renamed from: VE */
    private float m18900VE() {
        return aeO();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Max Angular Acceleration")
    @C0064Am(aul = "d07384f089b3ba6e412d3e0763ffcba6", aum = 0)
    /* renamed from: gC */
    private void m18921gC(float f) {
        m18911dt(f);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Max Angular Velocity")
    @C0064Am(aul = "7c872f7901612dc4384dfd95c26c7980", aum = 0)
    /* renamed from: VG */
    private float m18901VG() {
        return aeP();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Max Angular Velocity")
    @C0064Am(aul = "06976ee92b824a85f27efc3d50fc396c", aum = 0)
    /* renamed from: gD */
    private void m18922gD(float f) {
        m18912du(f);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Life Time (s)")
    @C0064Am(aul = "95d1a52f7097558b8d165b6fe49ba822", aum = 0)
    private long dAO() {
        return m18916eG();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Life Time (s)")
    @C0064Am(aul = "fe441a0a32524c60b0703ffea185428c", aum = 0)
    /* renamed from: mj */
    private void m18924mj(long j) {
        m18923l(j);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Explosion Radius")
    @C0064Am(aul = "ac1a7abd6a2a57d762f479c12d089158", aum = 0)
    /* renamed from: zC */
    private float m18927zC() {
        return m18928zj();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Explosion Radius")
    @C0064Am(aul = "98aa129b1bb074c22bf79dcdd465653e", aum = 0)
    /* renamed from: nb */
    private void m18925nb(float f) {
        m18908aw(f);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Explosion G F X")
    @C0064Am(aul = "de00a9bb92e6ce02e88efc1bf07989d5", aum = 0)
    private Asset bkg() {
        return m18929zl();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Explosion G F X")
    @C0064Am(aul = "e30fe576d479d79f9bba8a5914ff0204", aum = 0)
    /* renamed from: aR */
    private void m18905aR(Asset tCVar) {
        m18926z(tCVar);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Explosion S F X")
    @C0064Am(aul = "30080eef1b1cb2ff1f182e0f48f51ed1", aum = 0)
    private Asset bki() {
        return m18930zm();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Explosion S F X")
    @C0064Am(aul = "b3de4318a85cb867bdb6f228bd493227", aum = 0)
    /* renamed from: aT */
    private void m18907aT(Asset tCVar) {
        m18898A(tCVar);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Loop S F X")
    @C0064Am(aul = "67b80d2142cf0b20dc368b86e9884442", aum = 0)
    private Asset dAQ() {
        return cXM();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Loop S F X")
    @C0064Am(aul = "00737683366a65905c3d14ab02989c73", aum = 0)
    /* renamed from: cp */
    private void m18910cp(Asset tCVar) {
        m18909bY(tCVar);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Hull")
    @C0064Am(aul = "0c58ed8d02dbaebdb17225dad57bc86e", aum = 0)
    /* renamed from: VS */
    private HullType m18904VS() {
        return bjZ();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Hull")
    @C0064Am(aul = "9f91e0cf8075f5fda61dae486dd1ae43", aum = 0)
    /* renamed from: g */
    private void m18918g(HullType wHVar) {
        m18917f(wHVar);
    }

    @C0064Am(aul = "0c05726e8cb4f25469db54da440daa88", aum = 0)
    /* renamed from: e */
    private void m18915e(aDJ adj) {
        super.mo2631f(adj);
        if (adj instanceof Missile) {
            C1616Xf xf = adj;
            if (bjZ() != null) {
                xf.mo6765g(C5941adR.aMH, bjZ().mo7459NK());
            }
        }
    }

    @C0064Am(aul = "806aba3e6a040cdcab41b780d615522e", aum = 0)
    private Missile dAS() {
        return (Missile) mo745aU();
    }

    @C0064Am(aul = "74ad19e847d2984cbdba7d16f94c32ce", aum = 0)
    /* renamed from: aT */
    private <T extends aDJ> T m18906aT() {
        T t = (Missile) bFf().mo6865M(Missile.class);
        t.mo8535a(this);
        return t;
    }
}
