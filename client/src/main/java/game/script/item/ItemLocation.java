package game.script.item;

import game.network.message.externalizable.aCE;
import game.network.message.serializable.C2686iZ;
import game.script.TaikodomObject;
import logic.baa.*;
import logic.data.mbean.aAL;
import logic.res.code.C5640aRc;
import logic.res.code.C5663aRz;
import logic.res.code.C6494any;
import logic.res.html.C2491fm;
import logic.res.html.MessageContainer;
import logic.thred.LogPrinter;
import org.apache.commons.logging.Log;
import p001a.*;

import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;
import java.util.Set;

@C5511aMd
@C6485anp
/* renamed from: a.aAG */
/* compiled from: a */
public abstract class ItemLocation extends TaikodomObject implements C1493Vz, C1616Xf {

    /* renamed from: Lm */
    public static final C2491fm f2295Lm = null;

    /* renamed from: RA */
    public static final C2491fm f2296RA = null;

    /* renamed from: RD */
    public static final C2491fm f2297RD = null;

    /* renamed from: Rx */
    public static final C2491fm f2298Rx = null;

    /* renamed from: Rz */
    public static final C2491fm f2299Rz = null;
    public static final C2491fm _f_dispose_0020_0028_0029V = null;
    public static final C2491fm _f_hasHollowField_0020_0028_0029Z = null;
    public static final int _m_fieldCount = 0;
    public static final C5663aRz[] _m_fields = null;
    public static final int _m_methodCount = 0;
    public static final C2491fm[] _m_methods = null;
    public static final C2491fm aJH = null;
    public static final C2491fm bqB = null;
    /* renamed from: gz */
    public static final C5663aRz f2301gz = null;
    /* renamed from: hM */
    public static final C2491fm f2302hM = null;
    public static final C2491fm heP = null;
    public static final C2491fm heQ = null;
    public static final C2491fm heR = null;
    public static final C2491fm heS = null;
    public static final C2491fm heT = null;
    public static final C2491fm heU = null;
    public static final C2491fm heV = null;
    public static final C2491fm heW = null;
    public static final C2491fm heX = null;
    public static final C2491fm heY = null;
    public static final C2491fm heZ = null;
    public static final C2491fm hfa = null;
    public static final C2491fm hfb = null;
    public static final C2491fm hfc = null;
    public static final C2491fm hfd = null;
    public static final C2491fm hfe = null;
    public static final C2491fm hff = null;
    public static final C2491fm hfg = null;
    public static final C2491fm hfh = null;
    public static final C2491fm hfi = null;
    /* renamed from: ku */
    public static final C2491fm f2303ku = null;
    public static final long serialVersionUID = 0;
    private static final Log logger = LogPrinter.m10275K(ItemLocation.class);
    public static C6494any ___iScriptClass;
    @C0064Am(aul = "441dd3e0953e642bc894c451c3196208", aum = 0)

    /* renamed from: gy */
    private static C2686iZ<Item> f2300gy;

    static {
        m12464V();
    }

    public ItemLocation() {
        super((C5540aNg) null);
        super.mo10S();
    }

    public ItemLocation(C5540aNg ang) {
        super(ang);
    }

    public ItemLocation(C2961mJ mJVar) {
        super((C5540aNg) null);
        super.mo967a(mJVar);
    }

    /* renamed from: V */
    static void m12464V() {
        serialVersionUID = (long) 1;
        _m_fieldCount = TaikodomObject._m_fieldCount + 1;
        _m_methodCount = TaikodomObject._m_methodCount + 31;
        int i = TaikodomObject._m_fieldCount;
        C5663aRz[] arzArr = new C5663aRz[(i + 1)];
        C5663aRz b = C5640aRc.m17844b(ItemLocation.class, "441dd3e0953e642bc894c451c3196208", i);
        f2301gz = b;
        arzArr[i] = b;
        int i2 = i + 1;
        _m_fields = arzArr;
        C1634Xv.m11725a((Object[]) TaikodomObject._m_fields, (Object[]) _m_fields);
        int i3 = TaikodomObject._m_methodCount;
        C2491fm[] fmVarArr = new C2491fm[(i3 + 31)];
        C2491fm a = C4105zY.m41624a(ItemLocation.class, "2f0584dcd25e10beb154a82ea56f064d", i3);
        _f_dispose_0020_0028_0029V = a;
        fmVarArr[i3] = a;
        int i4 = i3 + 1;
        C2491fm a2 = C4105zY.m41624a(ItemLocation.class, "66d00737a010cbe324418db4e7d4ed8b", i4);
        f2299Rz = a2;
        fmVarArr[i4] = a2;
        int i5 = i4 + 1;
        C2491fm a3 = C4105zY.m41624a(ItemLocation.class, "808506c5837ad8284ce5597729a9516c", i5);
        f2298Rx = a3;
        fmVarArr[i5] = a3;
        int i6 = i5 + 1;
        C2491fm a4 = C4105zY.m41624a(ItemLocation.class, "026dd836fff21649a40b0720cfdf25da", i6);
        heP = a4;
        fmVarArr[i6] = a4;
        int i7 = i6 + 1;
        C2491fm a5 = C4105zY.m41624a(ItemLocation.class, "3298a9e14f4596105b132aded3d237e5", i7);
        heQ = a5;
        fmVarArr[i7] = a5;
        int i8 = i7 + 1;
        C2491fm a6 = C4105zY.m41624a(ItemLocation.class, "960371853c542840859d5f700580dd31", i8);
        heR = a6;
        fmVarArr[i8] = a6;
        int i9 = i8 + 1;
        C2491fm a7 = C4105zY.m41624a(ItemLocation.class, "7a465482ec8de62824bc00c03ecd6bcd", i9);
        heS = a7;
        fmVarArr[i9] = a7;
        int i10 = i9 + 1;
        C2491fm a8 = C4105zY.m41624a(ItemLocation.class, "88479c88df5f7d43004829f85c54814c", i10);
        heT = a8;
        fmVarArr[i10] = a8;
        int i11 = i10 + 1;
        C2491fm a9 = C4105zY.m41624a(ItemLocation.class, "6db302d49a0f92aeec111490ae79f331", i11);
        heU = a9;
        fmVarArr[i11] = a9;
        int i12 = i11 + 1;
        C2491fm a10 = C4105zY.m41624a(ItemLocation.class, "514176c0ac9258bcea7e3482aa241d75", i12);
        heV = a10;
        fmVarArr[i12] = a10;
        int i13 = i12 + 1;
        C2491fm a11 = C4105zY.m41624a(ItemLocation.class, "9ed8ed98c0ea055f30ae5a883d7fca3c", i13);
        heW = a11;
        fmVarArr[i13] = a11;
        int i14 = i13 + 1;
        C2491fm a12 = C4105zY.m41624a(ItemLocation.class, "7fb617f04cd39ccae22c20a22048e69e", i14);
        heX = a12;
        fmVarArr[i14] = a12;
        int i15 = i14 + 1;
        C2491fm a13 = C4105zY.m41624a(ItemLocation.class, "59623386aef0608e60b86502192dab42", i15);
        heY = a13;
        fmVarArr[i15] = a13;
        int i16 = i15 + 1;
        C2491fm a14 = C4105zY.m41624a(ItemLocation.class, "d1966869e3c2c80409ba7f76a438a713", i16);
        heZ = a14;
        fmVarArr[i16] = a14;
        int i17 = i16 + 1;
        C2491fm a15 = C4105zY.m41624a(ItemLocation.class, "8f9855edec2c02b38244f4f0cfdfe30f", i17);
        f2297RD = a15;
        fmVarArr[i17] = a15;
        int i18 = i17 + 1;
        C2491fm a16 = C4105zY.m41624a(ItemLocation.class, "0a64c24e1f372b7177fd9602b040ca93", i18);
        f2296RA = a16;
        fmVarArr[i18] = a16;
        int i19 = i18 + 1;
        C2491fm a17 = C4105zY.m41624a(ItemLocation.class, "7ccaf3a1ec98124f244b138eec791e80", i19);
        f2303ku = a17;
        fmVarArr[i19] = a17;
        int i20 = i19 + 1;
        C2491fm a18 = C4105zY.m41624a(ItemLocation.class, "34dd5f6471c4ce978ea8b01cf8095e35", i20);
        hfa = a18;
        fmVarArr[i20] = a18;
        int i21 = i20 + 1;
        C2491fm a19 = C4105zY.m41624a(ItemLocation.class, "a6df4fd00b685d54c55ae387aeb5be23", i21);
        hfb = a19;
        fmVarArr[i21] = a19;
        int i22 = i21 + 1;
        C2491fm a20 = C4105zY.m41624a(ItemLocation.class, "b78de489b516f3846b3c243813755150", i22);
        hfc = a20;
        fmVarArr[i22] = a20;
        int i23 = i22 + 1;
        C2491fm a21 = C4105zY.m41624a(ItemLocation.class, "fa07c0db7c9f66a2a88d2d3a0397975b", i23);
        f2302hM = a21;
        fmVarArr[i23] = a21;
        int i24 = i23 + 1;
        C2491fm a22 = C4105zY.m41624a(ItemLocation.class, "d7708ad833628d371592a34e16e256ec", i24);
        hfd = a22;
        fmVarArr[i24] = a22;
        int i25 = i24 + 1;
        C2491fm a23 = C4105zY.m41624a(ItemLocation.class, "8a4d6c7383c6943841d577142f98e217", i25);
        hfe = a23;
        fmVarArr[i25] = a23;
        int i26 = i25 + 1;
        C2491fm a24 = C4105zY.m41624a(ItemLocation.class, "4a9a6dfb22a10952d5a00de2b78bdb5e", i26);
        aJH = a24;
        fmVarArr[i26] = a24;
        int i27 = i26 + 1;
        C2491fm a25 = C4105zY.m41624a(ItemLocation.class, "3b17a8555f319439f1b6293b0b2bbc82", i27);
        hff = a25;
        fmVarArr[i27] = a25;
        int i28 = i27 + 1;
        C2491fm a26 = C4105zY.m41624a(ItemLocation.class, "c1becff23bb83deea93d73c8d25871ec", i28);
        bqB = a26;
        fmVarArr[i28] = a26;
        int i29 = i28 + 1;
        C2491fm a27 = C4105zY.m41624a(ItemLocation.class, "f9ae5897a3978cd9d93c97c14e89e95b", i29);
        hfg = a27;
        fmVarArr[i29] = a27;
        int i30 = i29 + 1;
        C2491fm a28 = C4105zY.m41624a(ItemLocation.class, "b8de59deefb0b6caf64bdf9cc7b03e33", i30);
        hfh = a28;
        fmVarArr[i30] = a28;
        int i31 = i30 + 1;
        C2491fm a29 = C4105zY.m41624a(ItemLocation.class, "1fd8cb05d5150928fc85e1eba30396c1", i31);
        hfi = a29;
        fmVarArr[i31] = a29;
        int i32 = i31 + 1;
        C2491fm a30 = C4105zY.m41624a(ItemLocation.class, "2306bdc20be7635ccc9a5b5967a89a9b", i32);
        f2295Lm = a30;
        fmVarArr[i32] = a30;
        int i33 = i32 + 1;
        C2491fm a31 = C4105zY.m41624a(ItemLocation.class, "e394e9d0bff1c2a9c9c4c3ae6c16ef96", i33);
        _f_hasHollowField_0020_0028_0029Z = a31;
        fmVarArr[i33] = a31;
        int i34 = i33 + 1;
        _m_methods = fmVarArr;
        C1634Xv.m11725a((Object[]) TaikodomObject._m_methods, (Object[]) _m_methods);
        ___iScriptClass = aUO.m18566a(ItemLocation.class, aAL.class, _m_fields, _m_methods);
    }

    @C0064Am(aul = "6db302d49a0f92aeec111490ae79f331", aum = 0)
    @C5566aOg
    @C2499fr
    /* renamed from: L */
    private Item m12456L(Item auq) {
        throw new aWi(new aCE(this, heU, new Object[]{auq}));
    }

    @C0064Am(aul = "34dd5f6471c4ce978ea8b01cf8095e35", aum = 0)
    @C5566aOg
    /* renamed from: M */
    private Item m12457M(Item auq) {
        throw new aWi(new aCE(this, hfa, new Object[]{auq}));
    }

    /* renamed from: P */
    private final boolean m12459P(Item auq) {
        switch (bFf().mo6893i(hfc)) {
            case 0:
                return false;
            case 2:
                return ((Boolean) bFf().mo5606d(new aCE(this, hfc, new Object[]{auq}))).booleanValue();
            case 3:
                bFf().mo5606d(new aCE(this, hfc, new Object[]{auq}));
                break;
        }
        return m12458O(auq);
    }

    @C0064Am(aul = "9ed8ed98c0ea055f30ae5a883d7fca3c", aum = 0)
    @C5566aOg
    @C2499fr
    /* renamed from: V */
    private Item m12463V(ItemType jCVar) {
        throw new aWi(new aCE(this, heW, new Object[]{jCVar}));
    }

    @C0064Am(aul = "59623386aef0608e60b86502192dab42", aum = 0)
    @C5566aOg
    @C2499fr
    /* renamed from: a */
    private Item m12465a(Item auq, boolean z) {
        throw new aWi(new aCE(this, heY, new Object[]{auq, new Boolean(z)}));
    }

    @C0064Am(aul = "d1966869e3c2c80409ba7f76a438a713", aum = 0)
    @C5566aOg
    /* renamed from: a */
    private Item m12466a(Item auq, boolean z, boolean z2) {
        throw new aWi(new aCE(this, heZ, new Object[]{auq, new Boolean(z), new Boolean(z2)}));
    }

    @C0064Am(aul = "7fb617f04cd39ccae22c20a22048e69e", aum = 0)
    @C5566aOg
    @C2499fr
    /* renamed from: a */
    private Item m12467a(ItemType jCVar, boolean z) {
        throw new aWi(new aCE(this, heX, new Object[]{jCVar, new Boolean(z)}));
    }

    @C0064Am(aul = "0a64c24e1f372b7177fd9602b040ca93", aum = 0)
    /* renamed from: a */
    private void m12468a(Item auq, ItemLocation aag) {
        throw new aWi(new aCE(this, f2296RA, new Object[]{auq, aag}));
    }

    @C5566aOg
    /* renamed from: b */
    private final Item m12469b(Item auq, boolean z, boolean z2) {
        switch (bFf().mo6893i(heZ)) {
            case 0:
                return null;
            case 2:
                return (Item) bFf().mo5606d(new aCE(this, heZ, new Object[]{auq, new Boolean(z), new Boolean(z2)}));
            case 3:
                bFf().mo5606d(new aCE(this, heZ, new Object[]{auq, new Boolean(z), new Boolean(z2)}));
                break;
        }
        return m12466a(auq, z, z2);
    }

    @C5566aOg
    @C2499fr
    /* renamed from: b */
    private final Item m12470b(ItemType jCVar, boolean z) {
        switch (bFf().mo6893i(heX)) {
            case 0:
                return null;
            case 2:
                return (Item) bFf().mo5606d(new aCE(this, heX, new Object[]{jCVar, new Boolean(z)}));
            case 3:
                bFf().mo5606d(new aCE(this, heX, new Object[]{jCVar, new Boolean(z)}));
                break;
        }
        return m12467a(jCVar, z);
    }

    /* renamed from: b */
    private void m12471b(C2686iZ iZVar) {
        bFf().mo5608dq().mo3197f(f2301gz, iZVar);
    }

    @C0064Am(aul = "a6df4fd00b685d54c55ae387aeb5be23", aum = 0)
    @C5566aOg
    /* renamed from: c */
    private Item m12472c(Item auq, boolean z) {
        throw new aWi(new aCE(this, hfb, new Object[]{auq, new Boolean(z)}));
    }

    @C0064Am(aul = "fa07c0db7c9f66a2a88d2d3a0397975b", aum = 0)
    @C5566aOg
    @C2499fr
    /* renamed from: c */
    private boolean m12473c(Item auq) {
        throw new aWi(new aCE(this, f2302hM, new Object[]{auq}));
    }

    @C0064Am(aul = "8a4d6c7383c6943841d577142f98e217", aum = 0)
    @C5566aOg
    @C2499fr
    private void cIz() {
        throw new aWi(new aCE(this, hfe, new Object[0]));
    }

    /* renamed from: cS */
    private C2686iZ m12474cS() {
        return (C2686iZ) bFf().mo5608dq().mo3214p(f2301gz);
    }

    @C5566aOg
    /* renamed from: d */
    private final Item m12475d(Item auq, boolean z) {
        switch (bFf().mo6893i(hfb)) {
            case 0:
                return null;
            case 2:
                return (Item) bFf().mo5606d(new aCE(this, hfb, new Object[]{auq, new Boolean(z)}));
            case 3:
                bFf().mo5606d(new aCE(this, hfb, new Object[]{auq, new Boolean(z)}));
                break;
        }
        return m12472c(auq, z);
    }

    @C0064Am(aul = "d7708ad833628d371592a34e16e256ec", aum = 0)
    @C5566aOg
    /* renamed from: e */
    private boolean m12476e(Item auq, boolean z) {
        throw new aWi(new aCE(this, hfd, new Object[]{auq, new Boolean(z)}));
    }

    @C0064Am(aul = "7ccaf3a1ec98124f244b138eec791e80", aum = 0)
    /* renamed from: g */
    private void m12478g(Item auq) {
        throw new aWi(new aCE(this, f2303ku, new Object[]{auq}));
    }

    @C0064Am(aul = "8f9855edec2c02b38244f4f0cfdfe30f", aum = 0)
    /* renamed from: m */
    private void m12481m(Item auq) {
        throw new aWi(new aCE(this, f2297RD, new Object[]{auq}));
    }

    @C0064Am(aul = "2306bdc20be7635ccc9a5b5967a89a9b", aum = 0)
    @C5566aOg
    @C2499fr
    /* renamed from: qU */
    private void m12482qU() {
        throw new aWi(new aCE(this, f2295Lm, new Object[0]));
    }

    @C5566aOg
    @C0064Am(aul = "514176c0ac9258bcea7e3482aa241d75", aum = 0)
    @C2499fr
    /* renamed from: u */
    private void m12483u(Collection<? extends Item> collection) {
        throw new aWi(new aCE(this, heV, new Object[]{collection}));
    }

    /* renamed from: K */
    public final boolean mo7614K(Item auq) {
        switch (bFf().mo6893i(heT)) {
            case 0:
                return false;
            case 2:
                return ((Boolean) bFf().mo5606d(new aCE(this, heT, new Object[]{auq}))).booleanValue();
            case 3:
                bFf().mo5606d(new aCE(this, heT, new Object[]{auq}));
                break;
        }
        return m12455J(auq);
    }

    @C5566aOg
    /* renamed from: N */
    public final Item mo7615N(Item auq) {
        switch (bFf().mo6893i(hfa)) {
            case 0:
                return null;
            case 2:
                return (Item) bFf().mo5606d(new aCE(this, hfa, new Object[]{auq}));
            case 3:
                bFf().mo5606d(new aCE(this, hfa, new Object[]{auq}));
                break;
        }
        return m12457M(auq);
    }

    @ClientOnly
    /* renamed from: QW */
    public boolean mo961QW() {
        switch (bFf().mo6893i(_f_hasHollowField_0020_0028_0029Z)) {
            case 0:
                return false;
            case 2:
                return ((Boolean) bFf().mo5606d(new aCE(this, _f_hasHollowField_0020_0028_0029Z, new Object[0]))).booleanValue();
            case 3:
                bFf().mo5606d(new aCE(this, _f_hasHollowField_0020_0028_0029Z, new Object[0]));
                break;
        }
        return m12461QV();
    }

    /* renamed from: T */
    public C6494any mo11T() {
        return ___iScriptClass;
    }

    /* renamed from: U */
    public final boolean mo7616U(ItemType jCVar) {
        switch (bFf().mo6893i(heS)) {
            case 0:
                return false;
            case 2:
                return ((Boolean) bFf().mo5606d(new aCE(this, heS, new Object[]{jCVar}))).booleanValue();
            case 3:
                bFf().mo5606d(new aCE(this, heS, new Object[]{jCVar}));
                break;
        }
        return m12462T(jCVar);
    }

    /* renamed from: U */
    public C2491fm[] mo12U() {
        return _m_methods;
    }

    /* renamed from: W */
    public Object mo13W() {
        return new aAL(this);
    }

    /* renamed from: a */
    public Object mo14a(C0495Gr gr) {
        Object[] args = gr.getArgs();
        switch (gr.mo2417hq() - TaikodomObject._m_methodCount) {
            case 0:
                m12477fg();
                return null;
            case 1:
                return new Float(m12484wD());
            case 2:
                return new Boolean(m12479k((Item) args[0]));
            case 3:
                return new Float(cIw());
            case 4:
                return new Float(cIx());
            case 5:
                return new Boolean(m12454I((Item) args[0]));
            case 6:
                return new Boolean(m12462T((ItemType) args[0]));
            case 7:
                return new Boolean(m12455J((Item) args[0]));
            case 8:
                return m12456L((Item) args[0]);
            case 9:
                m12483u((Collection<? extends Item>) (Collection) args[0]);
                return null;
            case 10:
                return m12463V((ItemType) args[0]);
            case 11:
                return m12467a((ItemType) args[0], ((Boolean) args[1]).booleanValue());
            case 12:
                return m12465a((Item) args[0], ((Boolean) args[1]).booleanValue());
            case 13:
                return m12466a((Item) args[0], ((Boolean) args[1]).booleanValue(), ((Boolean) args[2]).booleanValue());
            case 14:
                m12481m((Item) args[0]);
                return null;
            case 15:
                m12468a((Item) args[0], (ItemLocation) args[1]);
                return null;
            case 16:
                m12478g((Item) args[0]);
                return null;
            case 17:
                return m12457M((Item) args[0]);
            case 18:
                return m12472c((Item) args[0], ((Boolean) args[1]).booleanValue());
            case 19:
                return new Boolean(m12458O((Item) args[0]));
            case 20:
                return new Boolean(m12473c((Item) args[0]));
            case 21:
                return new Boolean(m12476e((Item) args[0], ((Boolean) args[1]).booleanValue()));
            case 22:
                cIz();
                return null;
            case 23:
                return new Integer(m12460QD());
            case 24:
                return cIA();
            case 25:
                return new Boolean(ahi());
            case 26:
                return cIC();
            case 27:
                return cID();
            case 28:
                return new Boolean(m12480l((Set<? extends Item>) (Set) args[0]));
            case 29:
                m12482qU();
                return null;
            case 30:
                return new Boolean(m12461QV());
            default:
                return super.mo14a(gr);
        }
    }

    /* renamed from: a */
    public void mo15a(Collection collection, C0495Gr gr) {
        super.mo15a(collection, gr);
    }

    public final float aRB() {
        switch (bFf().mo6893i(heP)) {
            case 0:
                return 0.0f;
            case 2:
                return ((Float) bFf().mo5606d(new aCE(this, heP, new Object[0]))).floatValue();
            case 3:
                bFf().mo5606d(new aCE(this, heP, new Object[0]));
                break;
        }
        return cIw();
    }

    @C3248pc(aYR = C3248pc.C3250b.ITERATOR, displayName = "Items")
    public Set<Item> aRz() {
        switch (bFf().mo6893i(hfg)) {
            case 0:
                return null;
            case 2:
                return (Set) bFf().mo5606d(new aCE(this, hfg, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, hfg, new Object[0]));
                break;
        }
        return cIC();
    }

    /* access modifiers changed from: protected */
    @C5566aOg
    @C2499fr
    /* renamed from: b */
    public final Item mo7617b(Item auq, boolean z) {
        switch (bFf().mo6893i(heY)) {
            case 0:
                return null;
            case 2:
                return (Item) bFf().mo5606d(new aCE(this, heY, new Object[]{auq, new Boolean(z)}));
            case 3:
                bFf().mo5606d(new aCE(this, heY, new Object[]{auq, new Boolean(z)}));
                break;
        }
        return m12465a(auq, z);
    }

    /* access modifiers changed from: protected */
    /* renamed from: b */
    public void mo1887b(Item auq, ItemLocation aag) {
        switch (bFf().mo6893i(f2296RA)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f2296RA, new Object[]{auq, aag}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f2296RA, new Object[]{auq, aag}));
                break;
        }
        m12468a(auq, aag);
    }

    /* renamed from: c */
    public C5663aRz[] mo25c() {
        return _m_fields;
    }

    public final Item cIB() {
        switch (bFf().mo6893i(hff)) {
            case 0:
                return null;
            case 2:
                return (Item) bFf().mo5606d(new aCE(this, hff, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, hff, new Object[0]));
                break;
        }
        return cIA();
    }

    public final float cIy() {
        switch (bFf().mo6893i(heQ)) {
            case 0:
                return 0.0f;
            case 2:
                return ((Float) bFf().mo5606d(new aCE(this, heQ, new Object[0]))).floatValue();
            case 3:
                bFf().mo5606d(new aCE(this, heQ, new Object[0]));
                break;
        }
        return cIx();
    }

    @C5566aOg
    @C2499fr
    /* renamed from: d */
    public final boolean mo2690d(Item auq) {
        switch (bFf().mo6893i(f2302hM)) {
            case 0:
                return false;
            case 2:
                return ((Boolean) bFf().mo5606d(new aCE(this, f2302hM, new Object[]{auq}))).booleanValue();
            case 3:
                bFf().mo5606d(new aCE(this, f2302hM, new Object[]{auq}));
                break;
        }
        return m12473c(auq);
    }

    public void dispose() {
        switch (bFf().mo6893i(_f_dispose_0020_0028_0029V)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, _f_dispose_0020_0028_0029V, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, _f_dispose_0020_0028_0029V, new Object[0]));
                break;
        }
        m12477fg();
    }

    /* access modifiers changed from: package-private */
    @C5566aOg
    /* renamed from: f */
    public final boolean mo7620f(Item auq, boolean z) {
        switch (bFf().mo6893i(hfd)) {
            case 0:
                return false;
            case 2:
                return ((Boolean) bFf().mo5606d(new aCE(this, hfd, new Object[]{auq, new Boolean(z)}))).booleanValue();
            case 3:
                bFf().mo5606d(new aCE(this, hfd, new Object[]{auq, new Boolean(z)}));
                break;
        }
        return m12476e(auq, z);
    }

    public Iterator<Item> getIterator() {
        switch (bFf().mo6893i(hfh)) {
            case 0:
                return null;
            case 2:
                return (Iterator) bFf().mo5606d(new aCE(this, hfh, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, hfh, new Object[0]));
                break;
        }
        return cID();
    }

    /* access modifiers changed from: protected */
    /* renamed from: h */
    public void mo1889h(Item auq) {
        switch (bFf().mo6893i(f2303ku)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f2303ku, new Object[]{auq}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f2303ku, new Object[]{auq}));
                break;
        }
        m12478g(auq);
    }

    public final boolean isEmpty() {
        switch (bFf().mo6893i(bqB)) {
            case 0:
                return false;
            case 2:
                return ((Boolean) bFf().mo5606d(new aCE(this, bqB, new Object[0]))).booleanValue();
            case 3:
                bFf().mo5606d(new aCE(this, bqB, new Object[0]));
                break;
        }
        return ahi();
    }

    @C5566aOg
    @C2499fr
    /* renamed from: k */
    public final void mo2691k(Collection<? extends Item> collection) {
        switch (bFf().mo6893i(heV)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, heV, new Object[]{collection}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, heV, new Object[]{collection}));
                break;
        }
        m12483u(collection);
    }

    /* renamed from: l */
    public boolean mo2878l(Item auq) {
        switch (bFf().mo6893i(f2298Rx)) {
            case 0:
                return false;
            case 2:
                return ((Boolean) bFf().mo5606d(new aCE(this, f2298Rx, new Object[]{auq}))).booleanValue();
            case 3:
                bFf().mo5606d(new aCE(this, f2298Rx, new Object[]{auq}));
                break;
        }
        return m12479k(auq);
    }

    /* renamed from: m */
    public boolean mo7623m(Set<? extends Item> set) {
        switch (bFf().mo6893i(hfi)) {
            case 0:
                return false;
            case 2:
                return ((Boolean) bFf().mo5606d(new aCE(this, hfi, new Object[]{set}))).booleanValue();
            case 3:
                bFf().mo5606d(new aCE(this, hfi, new Object[]{set}));
                break;
        }
        return m12480l(set);
    }

    /* access modifiers changed from: protected */
    /* renamed from: n */
    public void mo1890n(Item auq) {
        switch (bFf().mo6893i(f2297RD)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f2297RD, new Object[]{auq}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f2297RD, new Object[]{auq}));
                break;
        }
        m12481m(auq);
    }

    @C5566aOg
    @C2499fr
    /* renamed from: qV */
    public void mo4433qV() {
        switch (bFf().mo6893i(f2295Lm)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f2295Lm, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f2295Lm, new Object[0]));
                break;
        }
        m12482qU();
    }

    @C5566aOg
    @C2499fr
    public final void removeAll() {
        switch (bFf().mo6893i(hfe)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, hfe, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, hfe, new Object[0]));
                break;
        }
        cIz();
    }

    @C5566aOg
    @C2499fr
    /* renamed from: s */
    public final Item mo2693s(Item auq) {
        switch (bFf().mo6893i(heU)) {
            case 0:
                return null;
            case 2:
                return (Item) bFf().mo5606d(new aCE(this, heU, new Object[]{auq}));
            case 3:
                bFf().mo5606d(new aCE(this, heU, new Object[]{auq}));
                break;
        }
        return m12456L(auq);
    }

    public final int size() {
        switch (bFf().mo6893i(aJH)) {
            case 0:
                return 0;
            case 2:
                return ((Integer) bFf().mo5606d(new aCE(this, aJH, new Object[0]))).intValue();
            case 3:
                bFf().mo5606d(new aCE(this, aJH, new Object[0]));
                break;
        }
        return m12460QD();
    }

    /* renamed from: t */
    public final boolean mo2694t(Item auq) {
        switch (bFf().mo6893i(heR)) {
            case 0:
                return false;
            case 2:
                return ((Boolean) bFf().mo5606d(new aCE(this, heR, new Object[]{auq}))).booleanValue();
            case 3:
                bFf().mo5606d(new aCE(this, heR, new Object[]{auq}));
                break;
        }
        return m12454I(auq);
    }

    @C5566aOg
    @C2499fr
    /* renamed from: u */
    public final Item mo2695u(ItemType jCVar) {
        switch (bFf().mo6893i(heW)) {
            case 0:
                return null;
            case 2:
                return (Item) bFf().mo5606d(new aCE(this, heW, new Object[]{jCVar}));
            case 3:
                bFf().mo5606d(new aCE(this, heW, new Object[]{jCVar}));
                break;
        }
        return m12463V(jCVar);
    }

    /* renamed from: wE */
    public float mo2696wE() {
        switch (bFf().mo6893i(f2299Rz)) {
            case 0:
                return 0.0f;
            case 2:
                return ((Float) bFf().mo5606d(new aCE(this, f2299Rz, new Object[0]))).floatValue();
            case 3:
                bFf().mo5606d(new aCE(this, f2299Rz, new Object[0]));
                break;
        }
        return m12484wD();
    }

    /* renamed from: S */
    public void mo10S() {
        super.mo10S();
    }

    /* renamed from: a */
    public void mo967a(C2961mJ mJVar) {
        super.mo967a(mJVar);
    }

    @C0064Am(aul = "2f0584dcd25e10beb154a82ea56f064d", aum = 0)
    /* renamed from: fg */
    private void m12477fg() {
        for (Item dispose : aRz()) {
            dispose.dispose();
        }
        super.dispose();
    }

    @C0064Am(aul = "66d00737a010cbe324418db4e7d4ed8b", aum = 0)
    /* renamed from: wD */
    private float m12484wD() {
        return Float.MAX_VALUE;
    }

    @C0064Am(aul = "808506c5837ad8284ce5597729a9516c", aum = 0)
    /* renamed from: k */
    private boolean m12479k(Item auq) {
        return true;
    }

    @C0064Am(aul = "026dd836fff21649a40b0720cfdf25da", aum = 0)
    private float cIw() {
        float f = 0.0f;
        Iterator it = m12474cS().iterator();
        while (true) {
            float f2 = f;
            if (!it.hasNext()) {
                return f2;
            }
            Item auq = (Item) it.next();
            if (Float.MAX_VALUE - auq.getVolume() <= f2) {
                return Float.MAX_VALUE;
            }
            f = auq.getVolume() + f2;
        }
    }

    @C0064Am(aul = "3298a9e14f4596105b132aded3d237e5", aum = 0)
    private float cIx() {
        return mo2696wE() - aRB();
    }

    @C0064Am(aul = "960371853c542840859d5f700580dd31", aum = 0)
    /* renamed from: I */
    private boolean m12454I(Item auq) {
        return m12474cS().contains(auq);
    }

    @C0064Am(aul = "7a465482ec8de62824bc00c03ecd6bcd", aum = 0)
    /* renamed from: T */
    private boolean m12462T(ItemType jCVar) {
        for (Item bAP : m12474cS()) {
            if (bAP.bAP() == jCVar) {
                return true;
            }
        }
        return false;
    }

    @C0064Am(aul = "88479c88df5f7d43004829f85c54814c", aum = 0)
    /* renamed from: J */
    private boolean m12455J(Item auq) {
        if (!m12459P(auq)) {
            return false;
        }
        try {
            mo1889h(auq);
            return true;
        } catch (C2293dh e) {
            return false;
        }
    }

    @C0064Am(aul = "b78de489b516f3846b3c243813755150", aum = 0)
    /* renamed from: O */
    private boolean m12458O(Item auq) {
        if (auq.isDisposed()) {
            logger.error("The Item " + auq.cWm() + " is disposed and being moved to ItemLocation " + cWm());
            return false;
        } else if (mo2694t(auq) || !auq.aNN() || mo2696wE() - auq.getVolume() < aRB()) {
            return false;
        } else {
            return true;
        }
    }

    @C0064Am(aul = "4a9a6dfb22a10952d5a00de2b78bdb5e", aum = 0)
    /* renamed from: QD */
    private int m12460QD() {
        return m12474cS().size();
    }

    @C0064Am(aul = "3b17a8555f319439f1b6293b0b2bbc82", aum = 0)
    private Item cIA() {
        return (Item) m12474cS().iterator().next();
    }

    @C0064Am(aul = "c1becff23bb83deea93d73c8d25871ec", aum = 0)
    private boolean ahi() {
        return m12474cS().isEmpty();
    }

    @C3248pc(aYR = C3248pc.C3250b.ITERATOR, displayName = "Items")
    @C0064Am(aul = "f9ae5897a3978cd9d93c97c14e89e95b", aum = 0)
    private Set<Item> cIC() {
        return MessageContainer.m16003v(m12474cS());
    }

    @C0064Am(aul = "b8de59deefb0b6caf64bdf9cc7b03e33", aum = 0)
    private Iterator<Item> cID() {
        return Collections.unmodifiableSet(m12474cS()).iterator();
    }

    @C0064Am(aul = "1fd8cb05d5150928fc85e1eba30396c1", aum = 0)
    /* renamed from: l */
    private boolean m12480l(Set<? extends Item> set) {
        try {
            for (Item s : set) {
                mo2693s(s);
            }
            return true;
        } catch (C2293dh e) {
            return false;
        }
    }

    @C0064Am(aul = "e394e9d0bff1c2a9c9c4c3ae6c16ef96", aum = 0)
    @ClientOnly
    /* renamed from: QV */
    private boolean m12461QV() {
        if (super.mo961QW()) {
            return true;
        }
        for (Item t : m12474cS()) {
            if (C1298TD.m9830t(t).bFT()) {
                return true;
            }
        }
        return false;
    }
}
