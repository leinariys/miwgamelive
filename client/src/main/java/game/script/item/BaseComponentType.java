package game.script.item;

import game.network.message.externalizable.aCE;
import game.script.ship.Slot;
import logic.baa.C1616Xf;
import logic.baa.C1634Xv;
import logic.baa.C5511aMd;
import logic.baa.C6485anp;
import logic.data.mbean.C5642aRe;
import logic.res.code.C5663aRz;
import logic.res.code.C6494any;
import logic.res.html.C2491fm;
import p001a.*;

import java.util.Collection;

@C5511aMd
@C6485anp
/* renamed from: a.asG  reason: case insensitive filesystem */
/* compiled from: a */
public abstract class BaseComponentType extends ItemType implements C1616Xf {
    public static final int _m_fieldCount = 0;
    public static final C5663aRz[] _m_fields = null;
    public static final int _m_methodCount = 0;
    public static final C2491fm[] _m_methods = null;
    public static final C2491fm gvJ = null;
    public static final C2491fm gvK = null;
    public static final long serialVersionUID = 0;
    public static C6494any ___iScriptClass;

    static {
        m25638V();
    }

    public BaseComponentType() {
        super((C5540aNg) null);
        super.mo10S();
    }

    public BaseComponentType(C5540aNg ang) {
        super(ang);
    }

    /* renamed from: V */
    static void m25638V() {
        serialVersionUID = (long) 1;
        _m_fieldCount = ItemType._m_fieldCount + 0;
        _m_methodCount = ItemType._m_methodCount + 2;
        _m_fields = new C5663aRz[(ItemType._m_fieldCount + 0)];
        C1634Xv.m11725a((Object[]) ItemType._m_fields, (Object[]) _m_fields);
        int i = ItemType._m_methodCount;
        C2491fm[] fmVarArr = new C2491fm[(i + 2)];
        C2491fm a = C4105zY.m41624a(BaseComponentType.class, "0fd6681095259effeb8a0b6ad7805465", i);
        gvJ = a;
        fmVarArr[i] = a;
        int i2 = i + 1;
        C2491fm a2 = C4105zY.m41624a(BaseComponentType.class, "ebe14e94b7b00250aaad769907b99db9", i2);
        gvK = a2;
        fmVarArr[i2] = a2;
        int i3 = i2 + 1;
        _m_methods = fmVarArr;
        C1634Xv.m11725a((Object[]) ItemType._m_methods, (Object[]) _m_methods);
        ___iScriptClass = aUO.m18566a(BaseComponentType.class, C5642aRe.class, _m_fields, _m_methods);
    }

    @C0064Am(aul = "0fd6681095259effeb8a0b6ad7805465", aum = 0)
    private Slot.C2893a cuG() {
        throw new aWi(new aCE(this, gvJ, new Object[0]));
    }

    /* renamed from: T */
    public C6494any mo11T() {
        return ___iScriptClass;
    }

    /* renamed from: U */
    public C2491fm[] mo12U() {
        return _m_methods;
    }

    /* renamed from: W */
    public Object mo13W() {
        return new C5642aRe(this);
    }

    /* renamed from: a */
    public Object mo14a(C0495Gr gr) {
        gr.getArgs();
        switch (gr.mo2417hq() - ItemType._m_methodCount) {
            case 0:
                return cuG();
            case 1:
                return new Integer(cuI());
            default:
                return super.mo14a(gr);
        }
    }

    /* renamed from: a */
    public void mo15a(Collection collection, C0495Gr gr) {
        super.mo15a(collection, gr);
    }

    /* renamed from: c */
    public C5663aRz[] mo25c() {
        return _m_fields;
    }

    public Slot.C2893a cuH() {
        switch (bFf().mo6893i(gvJ)) {
            case 0:
                return null;
            case 2:
                return (Slot.C2893a) bFf().mo5606d(new aCE(this, gvJ, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, gvJ, new Object[0]));
                break;
        }
        return cuG();
    }

    public int cuJ() {
        switch (bFf().mo6893i(gvK)) {
            case 0:
                return 0;
            case 2:
                return ((Integer) bFf().mo5606d(new aCE(this, gvK, new Object[0]))).intValue();
            case 3:
                bFf().mo5606d(new aCE(this, gvK, new Object[0]));
                break;
        }
        return cuI();
    }

    /* renamed from: S */
    public void mo10S() {
        super.mo10S();
    }

    @C0064Am(aul = "ebe14e94b7b00250aaad769907b99db9", aum = 0)
    private int cuI() {
        return cuH().ordinal() + 1;
    }
}
