package game.script.item;

import game.network.message.externalizable.aCE;
import game.network.message.serializable.C1556Wo;
import game.script.damage.DamageType;
import game.script.resource.Asset;
import logic.baa.*;
import logic.data.mbean.C3967xU;
import logic.res.code.C5640aRc;
import logic.res.code.C5663aRz;
import logic.res.code.C6494any;
import logic.res.html.C2491fm;
import p001a.*;

import java.util.Collection;
import java.util.Map;

@C0566Hp
@C5829abJ("2.1.1")
@C6485anp
@C5511aMd
/* renamed from: a.aPT */
/* compiled from: a */
public abstract class WeaponType extends BaseWeaponType implements C1616Xf {

    /* renamed from: DJ */
    public static final C2491fm f3522DJ = null;
    public static final int _m_fieldCount = 0;
    public static final C5663aRz[] _m_fields = null;
    public static final int _m_methodCount = 0;
    public static final C2491fm[] _m_methods = null;
    public static final C5663aRz bEc = null;
    public static final C5663aRz bEk = null;
    public static final C2491fm cHV = null;
    public static final C2491fm cHW = null;
    public static final C2491fm dGE = null;
    public static final C2491fm dGF = null;
    public static final C5663aRz dGv = null;
    public static final C2491fm dVN = null;
    public static final C5663aRz hzS = null;
    public static final C5663aRz hzT = null;
    public static final C5663aRz hzU = null;
    public static final C5663aRz hzV = null;
    public static final C5663aRz hzW = null;
    public static final C5663aRz hzX = null;
    public static final C5663aRz hzY = null;
    public static final C5663aRz hzZ = null;
    public static final C2491fm iDg = null;
    public static final C2491fm iDh = null;
    public static final C2491fm iDi = null;
    public static final C2491fm iDj = null;
    public static final C2491fm iDk = null;
    public static final C2491fm iDl = null;
    public static final C2491fm iDm = null;
    public static final C2491fm iDn = null;
    public static final C2491fm iDo = null;
    public static final C2491fm iDp = null;
    public static final C2491fm iDq = null;
    public static final C2491fm iDr = null;
    public static final C2491fm iDs = null;
    public static final C2491fm iDt = null;
    public static final C2491fm iDu = null;
    public static final C2491fm iDv = null;
    public static final C2491fm iDw = null;
    public static final C2491fm iDx = null;
    public static final C2491fm iDy = null;
    public static final long serialVersionUID = 0;
    /* renamed from: uK */
    public static final C5663aRz f3523uK = null;
    /* renamed from: uN */
    public static final C5663aRz f3524uN = null;
    /* renamed from: uS */
    public static final C5663aRz f3525uS = null;
    /* renamed from: vb */
    public static final C2491fm f3526vb = null;
    /* renamed from: vg */
    public static final C2491fm f3527vg = null;
    /* renamed from: vh */
    public static final C2491fm f3528vh = null;
    public static C6494any ___iScriptClass;
    @C0064Am(aul = "ffbb079176806b263834c27c54a95030", aum = 3)
    private static boolean bEj;
    @C0064Am(aul = "8e2844cc72025f55abd9b215cd43ea60", aum = 9)
    private static Asset bIA;
    @C0064Am(aul = "6ac911c8b43db88be1727e540349898d", aum = 10)
    private static Asset bIB;
    @C0064Am(aul = "d81f212b089c7b8ac14762b1f4587fa9", aum = 11)
    private static Asset bIC;
    @C0064Am(aul = "209e0e46cfbe1a01239785dc9908ee46", aum = 12)
    private static Asset bID;
    @C0064Am(aul = "cccee2ab5e319f1f992d6f775a1f0a41", aum = 0)
    private static boolean bIt;
    @C0064Am(aul = "96d742392c453ec60cce2ded7d01cff8", aum = 1)
    private static Asset bIu;
    @C0064Am(aul = "0c8c747a62c1e716b0d33b84c6edf3a3", aum = 2)
    private static C1556Wo<DamageType, Float> bIv;
    @C0064Am(aul = "bb756382bfd39997268aaf69f4df98e4", aum = 4)
    private static float bIw;
    @C0064Am(aul = "cfc88d17fa9f7fe54e6b2c45da5b8763", aum = 5)
    private static Asset bIx;
    @C0064Am(aul = "4403966a748eced21ae657296d6c8a1d", aum = 7)
    private static float bIy;
    @C0064Am(aul = "ef97165a79b65a84c0704955d4104db3", aum = 8)
    private static Asset bIz;
    @C0064Am(aul = "13d5837e1fadef8c2806b67e14cd5046", aum = 6)
    private static float range;
    @C0064Am(aul = "ec7c9f1ac22ce45968d64c868a62071d", aum = 13)
    private static float speed;

    static {
        m17206V();
    }

    public WeaponType() {
        super((C5540aNg) null);
        super.mo10S();
    }

    public WeaponType(C5540aNg ang) {
        super(ang);
    }

    /* renamed from: V */
    static void m17206V() {
        serialVersionUID = (long) 1;
        _m_fieldCount = BaseWeaponType._m_fieldCount + 14;
        _m_methodCount = BaseWeaponType._m_methodCount + 28;
        int i = BaseWeaponType._m_fieldCount;
        C5663aRz[] arzArr = new C5663aRz[(i + 14)];
        C5663aRz b = C5640aRc.m17844b(WeaponType.class, "cccee2ab5e319f1f992d6f775a1f0a41", i);
        hzS = b;
        arzArr[i] = b;
        int i2 = i + 1;
        C5663aRz b2 = C5640aRc.m17844b(WeaponType.class, "96d742392c453ec60cce2ded7d01cff8", i2);
        dGv = b2;
        arzArr[i2] = b2;
        int i3 = i2 + 1;
        C5663aRz b3 = C5640aRc.m17844b(WeaponType.class, "0c8c747a62c1e716b0d33b84c6edf3a3", i3);
        f3523uK = b3;
        arzArr[i3] = b3;
        int i4 = i3 + 1;
        C5663aRz b4 = C5640aRc.m17844b(WeaponType.class, "ffbb079176806b263834c27c54a95030", i4);
        bEk = b4;
        arzArr[i4] = b4;
        int i5 = i4 + 1;
        C5663aRz b5 = C5640aRc.m17844b(WeaponType.class, "bb756382bfd39997268aaf69f4df98e4", i5);
        bEc = b5;
        arzArr[i5] = b5;
        int i6 = i5 + 1;
        C5663aRz b6 = C5640aRc.m17844b(WeaponType.class, "cfc88d17fa9f7fe54e6b2c45da5b8763", i6);
        hzT = b6;
        arzArr[i6] = b6;
        int i7 = i6 + 1;
        C5663aRz b7 = C5640aRc.m17844b(WeaponType.class, "13d5837e1fadef8c2806b67e14cd5046", i7);
        f3524uN = b7;
        arzArr[i7] = b7;
        int i8 = i7 + 1;
        C5663aRz b8 = C5640aRc.m17844b(WeaponType.class, "4403966a748eced21ae657296d6c8a1d", i8);
        hzU = b8;
        arzArr[i8] = b8;
        int i9 = i8 + 1;
        C5663aRz b9 = C5640aRc.m17844b(WeaponType.class, "ef97165a79b65a84c0704955d4104db3", i9);
        hzV = b9;
        arzArr[i9] = b9;
        int i10 = i9 + 1;
        C5663aRz b10 = C5640aRc.m17844b(WeaponType.class, "8e2844cc72025f55abd9b215cd43ea60", i10);
        hzW = b10;
        arzArr[i10] = b10;
        int i11 = i10 + 1;
        C5663aRz b11 = C5640aRc.m17844b(WeaponType.class, "6ac911c8b43db88be1727e540349898d", i11);
        hzX = b11;
        arzArr[i11] = b11;
        int i12 = i11 + 1;
        C5663aRz b12 = C5640aRc.m17844b(WeaponType.class, "d81f212b089c7b8ac14762b1f4587fa9", i12);
        hzY = b12;
        arzArr[i12] = b12;
        int i13 = i12 + 1;
        C5663aRz b13 = C5640aRc.m17844b(WeaponType.class, "209e0e46cfbe1a01239785dc9908ee46", i13);
        hzZ = b13;
        arzArr[i13] = b13;
        int i14 = i13 + 1;
        C5663aRz b14 = C5640aRc.m17844b(WeaponType.class, "ec7c9f1ac22ce45968d64c868a62071d", i14);
        f3525uS = b14;
        arzArr[i14] = b14;
        int i15 = i14 + 1;
        _m_fields = arzArr;
        C1634Xv.m11725a((Object[]) BaseWeaponType._m_fields, (Object[]) _m_fields);
        int i16 = BaseWeaponType._m_methodCount;
        C2491fm[] fmVarArr = new C2491fm[(i16 + 28)];
        C2491fm a = C4105zY.m41624a(WeaponType.class, "33576925a361b9fc6296853349b3f7ad", i16);
        iDg = a;
        fmVarArr[i16] = a;
        int i17 = i16 + 1;
        C2491fm a2 = C4105zY.m41624a(WeaponType.class, "342f69a65b102faa3aeb617bb76d4fa2", i17);
        iDh = a2;
        fmVarArr[i17] = a2;
        int i18 = i17 + 1;
        C2491fm a3 = C4105zY.m41624a(WeaponType.class, "498cbeef59d8b2fb45b8a9dba400a45a", i18);
        dGE = a3;
        fmVarArr[i18] = a3;
        int i19 = i18 + 1;
        C2491fm a4 = C4105zY.m41624a(WeaponType.class, "e8733786c728bc1ec9e81a08df88f7cb", i19);
        dGF = a4;
        fmVarArr[i19] = a4;
        int i20 = i19 + 1;
        C2491fm a5 = C4105zY.m41624a(WeaponType.class, "721137dc5f57fd3ba383b678c585c538", i20);
        dVN = a5;
        fmVarArr[i20] = a5;
        int i21 = i20 + 1;
        C2491fm a6 = C4105zY.m41624a(WeaponType.class, "5620d12241a31c4e0b699e390be24e92", i21);
        iDi = a6;
        fmVarArr[i21] = a6;
        int i22 = i21 + 1;
        C2491fm a7 = C4105zY.m41624a(WeaponType.class, "95442a442775c2404903032fb7d9e8f5", i22);
        cHV = a7;
        fmVarArr[i22] = a7;
        int i23 = i22 + 1;
        C2491fm a8 = C4105zY.m41624a(WeaponType.class, "cd8c9f7053f7dee77ff4f1a2770f43e0", i23);
        cHW = a8;
        fmVarArr[i23] = a8;
        int i24 = i23 + 1;
        C2491fm a9 = C4105zY.m41624a(WeaponType.class, "92cb2dafb00bb99a860e365f12fe617c", i24);
        f3522DJ = a9;
        fmVarArr[i24] = a9;
        int i25 = i24 + 1;
        C2491fm a10 = C4105zY.m41624a(WeaponType.class, "3bedef40ee2a2721031378e31ee70fb1", i25);
        iDj = a10;
        fmVarArr[i25] = a10;
        int i26 = i25 + 1;
        C2491fm a11 = C4105zY.m41624a(WeaponType.class, "4f53afc7853bdfe036435a4df13c810d", i26);
        iDk = a11;
        fmVarArr[i26] = a11;
        int i27 = i26 + 1;
        C2491fm a12 = C4105zY.m41624a(WeaponType.class, "461450d0004f08f83025a8b52c6f27c1", i27);
        iDl = a12;
        fmVarArr[i27] = a12;
        int i28 = i27 + 1;
        C2491fm a13 = C4105zY.m41624a(WeaponType.class, "e11af8dbada0bbea3d644a09b7220eee", i28);
        f3526vb = a13;
        fmVarArr[i28] = a13;
        int i29 = i28 + 1;
        C2491fm a14 = C4105zY.m41624a(WeaponType.class, "64df37804753f07e76a07eaa34b4fb82", i29);
        iDm = a14;
        fmVarArr[i29] = a14;
        int i30 = i29 + 1;
        C2491fm a15 = C4105zY.m41624a(WeaponType.class, "b856cc3fbd8102ea5ee2105c264417de", i30);
        iDn = a15;
        fmVarArr[i30] = a15;
        int i31 = i30 + 1;
        C2491fm a16 = C4105zY.m41624a(WeaponType.class, "8d67dd10621e278ffed2e78c6e91d7d6", i31);
        iDo = a16;
        fmVarArr[i31] = a16;
        int i32 = i31 + 1;
        C2491fm a17 = C4105zY.m41624a(WeaponType.class, "76daa31b945345296df8db2042718a52", i32);
        iDp = a17;
        fmVarArr[i32] = a17;
        int i33 = i32 + 1;
        C2491fm a18 = C4105zY.m41624a(WeaponType.class, "c15853bca710aee494eebb4f4c5885d8", i33);
        iDq = a18;
        fmVarArr[i33] = a18;
        int i34 = i33 + 1;
        C2491fm a19 = C4105zY.m41624a(WeaponType.class, "c06d0292c4dd8c705a223b215bdc336e", i34);
        iDr = a19;
        fmVarArr[i34] = a19;
        int i35 = i34 + 1;
        C2491fm a20 = C4105zY.m41624a(WeaponType.class, "fcdc07af19d27367d971ffffd86d59c1", i35);
        iDs = a20;
        fmVarArr[i35] = a20;
        int i36 = i35 + 1;
        C2491fm a21 = C4105zY.m41624a(WeaponType.class, "0844684bcf17c6ee2279b1777bb9d8ca", i36);
        iDt = a21;
        fmVarArr[i36] = a21;
        int i37 = i36 + 1;
        C2491fm a22 = C4105zY.m41624a(WeaponType.class, "ab4e5323e33b041f04d811f91709a9f9", i37);
        iDu = a22;
        fmVarArr[i37] = a22;
        int i38 = i37 + 1;
        C2491fm a23 = C4105zY.m41624a(WeaponType.class, "582b095356e363cc91323c7943a5aa96", i38);
        iDv = a23;
        fmVarArr[i38] = a23;
        int i39 = i38 + 1;
        C2491fm a24 = C4105zY.m41624a(WeaponType.class, "288dd3b7eaacce84f92156ffc634b47a", i39);
        iDw = a24;
        fmVarArr[i39] = a24;
        int i40 = i39 + 1;
        C2491fm a25 = C4105zY.m41624a(WeaponType.class, "789b3fb27127ae41159ab9015ffe1cbe", i40);
        iDx = a25;
        fmVarArr[i40] = a25;
        int i41 = i40 + 1;
        C2491fm a26 = C4105zY.m41624a(WeaponType.class, "77d3a0058f858950a72e37fc1ce733ac", i41);
        iDy = a26;
        fmVarArr[i41] = a26;
        int i42 = i41 + 1;
        C2491fm a27 = C4105zY.m41624a(WeaponType.class, "6581b0269aa037cd791fe99de6f2f61b", i42);
        f3528vh = a27;
        fmVarArr[i42] = a27;
        int i43 = i42 + 1;
        C2491fm a28 = C4105zY.m41624a(WeaponType.class, "0e31b492804253894188798025b6d474", i43);
        f3527vg = a28;
        fmVarArr[i43] = a28;
        int i44 = i43 + 1;
        _m_methods = fmVarArr;
        C1634Xv.m11725a((Object[]) BaseWeaponType._m_methods, (Object[]) _m_methods);
        ___iScriptClass = aUO.m18566a(WeaponType.class, C3967xU.class, _m_fields, _m_methods);
    }

    /* renamed from: G */
    private void m17201G(float f) {
        bFf().mo5608dq().mo3150a(f3524uN, f);
    }

    /* renamed from: H */
    private void m17202H(float f) {
        bFf().mo5608dq().mo3150a(f3525uS, f);
    }

    /* renamed from: S */
    private void m17205S(C1556Wo wo) {
        bFf().mo5608dq().mo3197f(f3523uK, wo);
    }

    /* renamed from: aJ */
    private void m17207aJ(Asset tCVar) {
        bFf().mo5608dq().mo3197f(dGv, tCVar);
    }

    private boolean amJ() {
        return bFf().mo5608dq().mo3201h(bEk);
    }

    private C1556Wo bAH() {
        return (C1556Wo) bFf().mo5608dq().mo3214p(f3523uK);
    }

    /* renamed from: bC */
    private void m17209bC(boolean z) {
        bFf().mo5608dq().mo3153a(bEk, z);
    }

    /* renamed from: bS */
    private void m17210bS(Asset tCVar) {
        bFf().mo5608dq().mo3197f(hzT, tCVar);
    }

    /* renamed from: bT */
    private void m17211bT(Asset tCVar) {
        bFf().mo5608dq().mo3197f(hzV, tCVar);
    }

    /* renamed from: bU */
    private void m17212bU(Asset tCVar) {
        bFf().mo5608dq().mo3197f(hzW, tCVar);
    }

    /* renamed from: bV */
    private void m17213bV(Asset tCVar) {
        bFf().mo5608dq().mo3197f(hzX, tCVar);
    }

    /* renamed from: bW */
    private void m17214bW(Asset tCVar) {
        bFf().mo5608dq().mo3197f(hzY, tCVar);
    }

    /* renamed from: bX */
    private void m17215bX(Asset tCVar) {
        bFf().mo5608dq().mo3197f(hzZ, tCVar);
    }

    private Asset bjR() {
        return (Asset) bFf().mo5608dq().mo3214p(dGv);
    }

    private boolean cTF() {
        return bFf().mo5608dq().mo3201h(hzS);
    }

    private float cTG() {
        return bFf().mo5608dq().mo3211m(bEc);
    }

    private Asset cTH() {
        return (Asset) bFf().mo5608dq().mo3214p(hzT);
    }

    private float cTI() {
        return bFf().mo5608dq().mo3211m(hzU);
    }

    private Asset cTJ() {
        return (Asset) bFf().mo5608dq().mo3214p(hzV);
    }

    private Asset cTK() {
        return (Asset) bFf().mo5608dq().mo3214p(hzW);
    }

    private Asset cTL() {
        return (Asset) bFf().mo5608dq().mo3214p(hzX);
    }

    private Asset cTM() {
        return (Asset) bFf().mo5608dq().mo3214p(hzY);
    }

    private Asset cTN() {
        return (Asset) bFf().mo5608dq().mo3214p(hzZ);
    }

    /* renamed from: iN */
    private void m17223iN(boolean z) {
        bFf().mo5608dq().mo3153a(hzS, z);
    }

    /* renamed from: if */
    private float m17224if() {
        return bFf().mo5608dq().mo3211m(f3524uN);
    }

    /* renamed from: ii */
    private float m17225ii() {
        return bFf().mo5608dq().mo3211m(f3525uS);
    }

    /* renamed from: mn */
    private void m17230mn(float f) {
        bFf().mo5608dq().mo3150a(bEc, f);
    }

    /* renamed from: mo */
    private void m17231mo(float f) {
        bFf().mo5608dq().mo3150a(hzU, f);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Damage")
    /* renamed from: K */
    public void mo10770K(Map<DamageType, Float> map) {
        switch (bFf().mo6893i(iDi)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, iDi, new Object[]{map}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, iDi, new Object[]{map}));
                break;
        }
        m17204J(map);
    }

    /* renamed from: T */
    public C6494any mo11T() {
        return ___iScriptClass;
    }

    /* renamed from: U */
    public C2491fm[] mo12U() {
        return _m_methods;
    }

    /* renamed from: W */
    public Object mo13W() {
        return new C3967xU(this);
    }

    /* renamed from: a */
    public Object mo14a(C0495Gr gr) {
        Object[] args = gr.getArgs();
        switch (gr.mo2417hq() - BaseWeaponType._m_methodCount) {
            case 0:
                return new Boolean(doK());
            case 1:
                m17228jW(((Boolean) args[0]).booleanValue());
                return null;
            case 2:
                return bjT();
            case 3:
                m17208aL((Asset) args[0]);
                return null;
            case 4:
                return bpz();
            case 5:
                m17204J((Map) args[0]);
                return null;
            case 6:
                return new Boolean(aGH());
            case 7:
                m17216cA(((Boolean) args[0]).booleanValue());
                return null;
            case 8:
                return new Float(m17229mk());
            case 9:
                m17232nM(((Float) args[0]).floatValue());
                return null;
            case 10:
                return doM();
            case 11:
                m17217cb((Asset) args[0]);
                return null;
            case 12:
                return new Float(m17226im());
            case 13:
                m17233nO(((Float) args[0]).floatValue());
                return null;
            case 14:
                return new Float(doO());
            case 15:
                m17234nP(((Float) args[0]).floatValue());
                return null;
            case 16:
                return doQ();
            case 17:
                m17218cd((Asset) args[0]);
                return null;
            case 18:
                return doS();
            case 19:
                m17219cf((Asset) args[0]);
                return null;
            case 20:
                return doU();
            case 21:
                m17220ch((Asset) args[0]);
                return null;
            case 22:
                return doW();
            case 23:
                m17221cj((Asset) args[0]);
                return null;
            case 24:
                return doY();
            case 25:
                m17222cl((Asset) args[0]);
                return null;
            case 26:
                return new Float(m17227is());
            case 27:
                m17203I(((Float) args[0]).floatValue());
                return null;
            default:
                return super.mo14a(gr);
        }
    }

    /* renamed from: a */
    public void mo15a(Collection collection, C0495Gr gr) {
        super.mo15a(collection, gr);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Double Shot")
    public boolean aGI() {
        switch (bFf().mo6893i(cHV)) {
            case 0:
                return false;
            case 2:
                return ((Boolean) bFf().mo5606d(new aCE(this, cHV, new Object[0]))).booleanValue();
            case 3:
                bFf().mo5606d(new aCE(this, cHV, new Object[0]));
                break;
        }
        return aGH();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Activate S F X")
    /* renamed from: aM */
    public void mo10772aM(Asset tCVar) {
        switch (bFf().mo6893i(dGF)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, dGF, new Object[]{tCVar}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, dGF, new Object[]{tCVar}));
                break;
        }
        m17208aL(tCVar);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Activate S F X")
    public Asset bjU() {
        switch (bFf().mo6893i(dGE)) {
            case 0:
                return null;
            case 2:
                return (Asset) bFf().mo5606d(new aCE(this, dGE, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, dGE, new Object[0]));
                break;
        }
        return bjT();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Damage")
    public C1556Wo<DamageType, Float> bpA() {
        switch (bFf().mo6893i(dVN)) {
            case 0:
                return null;
            case 2:
                return (C1556Wo) bFf().mo5606d(new aCE(this, dVN, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, dVN, new Object[0]));
                break;
        }
        return bpz();
    }

    /* renamed from: c */
    public C5663aRz[] mo25c() {
        return _m_fields;
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Double Shot")
    /* renamed from: cB */
    public void mo10774cB(boolean z) {
        switch (bFf().mo6893i(cHW)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, cHW, new Object[]{new Boolean(z)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, cHW, new Object[]{new Boolean(z)}));
                break;
        }
        m17216cA(z);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Muzzle G F X")
    /* renamed from: cc */
    public void mo10775cc(Asset tCVar) {
        switch (bFf().mo6893i(iDl)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, iDl, new Object[]{tCVar}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, iDl, new Object[]{tCVar}));
                break;
        }
        m17217cb(tCVar);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Shot S F X")
    /* renamed from: ce */
    public void mo10776ce(Asset tCVar) {
        switch (bFf().mo6893i(iDq)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, iDq, new Object[]{tCVar}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, iDq, new Object[]{tCVar}));
                break;
        }
        m17218cd(tCVar);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Empty S F X")
    /* renamed from: cg */
    public void mo10777cg(Asset tCVar) {
        switch (bFf().mo6893i(iDs)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, iDs, new Object[]{tCVar}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, iDs, new Object[]{tCVar}));
                break;
        }
        m17219cf(tCVar);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Reammo S F X")
    /* renamed from: ci */
    public void mo10778ci(Asset tCVar) {
        switch (bFf().mo6893i(iDu)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, iDu, new Object[]{tCVar}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, iDu, new Object[]{tCVar}));
                break;
        }
        m17220ch(tCVar);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Firing Loop S F X")
    /* renamed from: ck */
    public void mo10779ck(Asset tCVar) {
        switch (bFf().mo6893i(iDw)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, iDw, new Object[]{tCVar}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, iDw, new Object[]{tCVar}));
                break;
        }
        m17221cj(tCVar);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Stop Firing S F X")
    /* renamed from: cm */
    public void mo10780cm(Asset tCVar) {
        switch (bFf().mo6893i(iDy)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, iDy, new Object[]{tCVar}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, iDy, new Object[]{tCVar}));
                break;
        }
        m17222cl(tCVar);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Ignore Aim Correction")
    public boolean doL() {
        switch (bFf().mo6893i(iDg)) {
            case 0:
                return false;
            case 2:
                return ((Boolean) bFf().mo5606d(new aCE(this, iDg, new Object[0]))).booleanValue();
            case 3:
                bFf().mo5606d(new aCE(this, iDg, new Object[0]));
                break;
        }
        return doK();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Muzzle G F X")
    public Asset doN() {
        switch (bFf().mo6893i(iDk)) {
            case 0:
                return null;
            case 2:
                return (Asset) bFf().mo5606d(new aCE(this, iDk, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, iDk, new Object[0]));
                break;
        }
        return doM();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Shape Size")
    public float doP() {
        switch (bFf().mo6893i(iDn)) {
            case 0:
                return 0.0f;
            case 2:
                return ((Float) bFf().mo5606d(new aCE(this, iDn, new Object[0]))).floatValue();
            case 3:
                bFf().mo5606d(new aCE(this, iDn, new Object[0]));
                break;
        }
        return doO();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Shot S F X")
    public Asset doR() {
        switch (bFf().mo6893i(iDp)) {
            case 0:
                return null;
            case 2:
                return (Asset) bFf().mo5606d(new aCE(this, iDp, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, iDp, new Object[0]));
                break;
        }
        return doQ();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Empty S F X")
    public Asset doT() {
        switch (bFf().mo6893i(iDr)) {
            case 0:
                return null;
            case 2:
                return (Asset) bFf().mo5606d(new aCE(this, iDr, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, iDr, new Object[0]));
                break;
        }
        return doS();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Reammo S F X")
    public Asset doV() {
        switch (bFf().mo6893i(iDt)) {
            case 0:
                return null;
            case 2:
                return (Asset) bFf().mo5606d(new aCE(this, iDt, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, iDt, new Object[0]));
                break;
        }
        return doU();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Firing Loop S F X")
    public Asset doX() {
        switch (bFf().mo6893i(iDv)) {
            case 0:
                return null;
            case 2:
                return (Asset) bFf().mo5606d(new aCE(this, iDv, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, iDv, new Object[0]));
                break;
        }
        return doW();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Stop Firing S F X")
    public Asset doZ() {
        switch (bFf().mo6893i(iDx)) {
            case 0:
                return null;
            case 2:
                return (Asset) bFf().mo5606d(new aCE(this, iDx, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, iDx, new Object[0]));
                break;
        }
        return doY();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Speed")
    public float getSpeed() {
        switch (bFf().mo6893i(f3528vh)) {
            case 0:
                return 0.0f;
            case 2:
                return ((Float) bFf().mo5606d(new aCE(this, f3528vh, new Object[0]))).floatValue();
            case 3:
                bFf().mo5606d(new aCE(this, f3528vh, new Object[0]));
                break;
        }
        return m17227is();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Speed")
    public void setSpeed(float f) {
        switch (bFf().mo6893i(f3527vg)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f3527vg, new Object[]{new Float(f)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f3527vg, new Object[]{new Float(f)}));
                break;
        }
        m17203I(f);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Range")
    /* renamed from: in */
    public float mo10790in() {
        switch (bFf().mo6893i(f3526vb)) {
            case 0:
                return 0.0f;
            case 2:
                return ((Float) bFf().mo5606d(new aCE(this, f3526vb, new Object[0]))).floatValue();
            case 3:
                bFf().mo5606d(new aCE(this, f3526vb, new Object[0]));
                break;
        }
        return m17226im();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Range")
    /* renamed from: jD */
    public void mo10791jD(float f) {
        switch (bFf().mo6893i(iDm)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, iDm, new Object[]{new Float(f)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, iDm, new Object[]{new Float(f)}));
                break;
        }
        m17233nO(f);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Ignore Aim Correction")
    /* renamed from: jX */
    public void mo10792jX(boolean z) {
        switch (bFf().mo6893i(iDh)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, iDh, new Object[]{new Boolean(z)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, iDh, new Object[]{new Boolean(z)}));
                break;
        }
        m17228jW(z);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Fire Rate")
    /* renamed from: ml */
    public float mo10793ml() {
        switch (bFf().mo6893i(f3522DJ)) {
            case 0:
                return 0.0f;
            case 2:
                return ((Float) bFf().mo5606d(new aCE(this, f3522DJ, new Object[0]))).floatValue();
            case 3:
                bFf().mo5606d(new aCE(this, f3522DJ, new Object[0]));
                break;
        }
        return m17229mk();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Fire Rate")
    /* renamed from: nN */
    public void mo10794nN(float f) {
        switch (bFf().mo6893i(iDj)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, iDj, new Object[]{new Float(f)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, iDj, new Object[]{new Float(f)}));
                break;
        }
        m17232nM(f);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Shape Size")
    /* renamed from: nQ */
    public void mo10795nQ(float f) {
        switch (bFf().mo6893i(iDo)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, iDo, new Object[]{new Float(f)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, iDo, new Object[]{new Float(f)}));
                break;
        }
        m17234nP(f);
    }

    /* renamed from: S */
    public void mo10S() {
        super.mo10S();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Ignore Aim Correction")
    @C0064Am(aul = "33576925a361b9fc6296853349b3f7ad", aum = 0)
    private boolean doK() {
        return cTF();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Ignore Aim Correction")
    @C0064Am(aul = "342f69a65b102faa3aeb617bb76d4fa2", aum = 0)
    /* renamed from: jW */
    private void m17228jW(boolean z) {
        m17223iN(z);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Activate S F X")
    @C0064Am(aul = "498cbeef59d8b2fb45b8a9dba400a45a", aum = 0)
    private Asset bjT() {
        return bjR();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Activate S F X")
    @C0064Am(aul = "e8733786c728bc1ec9e81a08df88f7cb", aum = 0)
    /* renamed from: aL */
    private void m17208aL(Asset tCVar) {
        m17207aJ(tCVar);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Damage")
    @C0064Am(aul = "721137dc5f57fd3ba383b678c585c538", aum = 0)
    private C1556Wo<DamageType, Float> bpz() {
        return bAH();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Damage")
    @C0064Am(aul = "5620d12241a31c4e0b699e390be24e92", aum = 0)
    /* renamed from: J */
    private void m17204J(Map<DamageType, Float> map) {
        bAH().clear();
        bAH().putAll(map);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Double Shot")
    @C0064Am(aul = "95442a442775c2404903032fb7d9e8f5", aum = 0)
    private boolean aGH() {
        return amJ();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Double Shot")
    @C0064Am(aul = "cd8c9f7053f7dee77ff4f1a2770f43e0", aum = 0)
    /* renamed from: cA */
    private void m17216cA(boolean z) {
        m17209bC(z);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Fire Rate")
    @C0064Am(aul = "92cb2dafb00bb99a860e365f12fe617c", aum = 0)
    /* renamed from: mk */
    private float m17229mk() {
        return cTG();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Fire Rate")
    @C0064Am(aul = "3bedef40ee2a2721031378e31ee70fb1", aum = 0)
    /* renamed from: nM */
    private void m17232nM(float f) {
        m17230mn(f);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Muzzle G F X")
    @C0064Am(aul = "4f53afc7853bdfe036435a4df13c810d", aum = 0)
    private Asset doM() {
        return cTH();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Muzzle G F X")
    @C0064Am(aul = "461450d0004f08f83025a8b52c6f27c1", aum = 0)
    /* renamed from: cb */
    private void m17217cb(Asset tCVar) {
        m17210bS(tCVar);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Range")
    @C0064Am(aul = "e11af8dbada0bbea3d644a09b7220eee", aum = 0)
    /* renamed from: im */
    private float m17226im() {
        return m17224if();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Range")
    @C0064Am(aul = "64df37804753f07e76a07eaa34b4fb82", aum = 0)
    /* renamed from: nO */
    private void m17233nO(float f) {
        m17201G(f);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Shape Size")
    @C0064Am(aul = "b856cc3fbd8102ea5ee2105c264417de", aum = 0)
    private float doO() {
        return cTI();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Shape Size")
    @C0064Am(aul = "8d67dd10621e278ffed2e78c6e91d7d6", aum = 0)
    /* renamed from: nP */
    private void m17234nP(float f) {
        m17231mo(f);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Shot S F X")
    @C0064Am(aul = "76daa31b945345296df8db2042718a52", aum = 0)
    private Asset doQ() {
        return cTJ();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Shot S F X")
    @C0064Am(aul = "c15853bca710aee494eebb4f4c5885d8", aum = 0)
    /* renamed from: cd */
    private void m17218cd(Asset tCVar) {
        m17211bT(tCVar);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Empty S F X")
    @C0064Am(aul = "c06d0292c4dd8c705a223b215bdc336e", aum = 0)
    private Asset doS() {
        return cTK();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Empty S F X")
    @C0064Am(aul = "fcdc07af19d27367d971ffffd86d59c1", aum = 0)
    /* renamed from: cf */
    private void m17219cf(Asset tCVar) {
        m17212bU(tCVar);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Reammo S F X")
    @C0064Am(aul = "0844684bcf17c6ee2279b1777bb9d8ca", aum = 0)
    private Asset doU() {
        return cTL();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Reammo S F X")
    @C0064Am(aul = "ab4e5323e33b041f04d811f91709a9f9", aum = 0)
    /* renamed from: ch */
    private void m17220ch(Asset tCVar) {
        m17213bV(tCVar);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Firing Loop S F X")
    @C0064Am(aul = "582b095356e363cc91323c7943a5aa96", aum = 0)
    private Asset doW() {
        return cTM();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Firing Loop S F X")
    @C0064Am(aul = "288dd3b7eaacce84f92156ffc634b47a", aum = 0)
    /* renamed from: cj */
    private void m17221cj(Asset tCVar) {
        m17214bW(tCVar);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Stop Firing S F X")
    @C0064Am(aul = "789b3fb27127ae41159ab9015ffe1cbe", aum = 0)
    private Asset doY() {
        return cTN();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Stop Firing S F X")
    @C0064Am(aul = "77d3a0058f858950a72e37fc1ce733ac", aum = 0)
    /* renamed from: cl */
    private void m17222cl(Asset tCVar) {
        m17215bX(tCVar);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Speed")
    @C0064Am(aul = "6581b0269aa037cd791fe99de6f2f61b", aum = 0)
    /* renamed from: is */
    private float m17227is() {
        return m17225ii();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Speed")
    @C0064Am(aul = "0e31b492804253894188798025b6d474", aum = 0)
    /* renamed from: I */
    private void m17203I(float f) {
        m17202H(f);
    }
}
