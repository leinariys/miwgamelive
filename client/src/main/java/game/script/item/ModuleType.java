package game.script.item;

import game.network.message.externalizable.aCE;
import game.network.message.serializable.C2686iZ;
import game.script.item.buff.module.AttributeBuffType;
import game.script.resource.Asset;
import logic.baa.*;
import logic.data.mbean.C5733aUr;
import logic.res.code.C5640aRc;
import logic.res.code.C5663aRz;
import logic.res.code.C6494any;
import logic.res.html.C2491fm;
import p001a.*;

import java.util.Collection;
import java.util.Collections;
import java.util.Set;

@C0566Hp
@C5829abJ("2.0.2")
@C6485anp
@C5511aMd
/* renamed from: a.KQ */
/* compiled from: a */
public class ModuleType extends BaseModuleType implements C1616Xf {
    public static final int _m_fieldCount = 0;
    public static final C5663aRz[] _m_fields = null;
    public static final int _m_methodCount = 0;
    public static final C2491fm[] _m_methods = null;
    /* renamed from: dN */
    public static final C2491fm f957dN = null;
    public static final C5663aRz dqU = null;
    public static final C5663aRz dqV = null;
    public static final C5663aRz dqW = null;
    public static final C5663aRz dqX = null;
    public static final C5663aRz dqY = null;
    public static final C5663aRz dqZ = null;
    public static final C5663aRz dra = null;
    public static final C5663aRz drb = null;
    public static final C2491fm drc = null;
    public static final C2491fm drd = null;
    public static final C2491fm dre = null;
    public static final C2491fm drf = null;
    public static final C2491fm drg = null;
    public static final C2491fm drh = null;
    public static final C2491fm dri = null;
    public static final C2491fm drj = null;
    public static final C2491fm drk = null;
    public static final C2491fm drl = null;
    public static final C2491fm drm = null;
    public static final C2491fm drn = null;
    public static final C2491fm dro = null;
    public static final C2491fm drp = null;
    public static final C2491fm drq = null;
    public static final C2491fm drr = null;
    public static final C2491fm drs = null;
    public static final C2491fm drt = null;
    public static final C2491fm dru = null;
    public static final long serialVersionUID = 0;
    public static C6494any ___iScriptClass;
    @C0064Am(aul = "667ea0aaf9b7c24aab4ca39b1668d067", aum = 0)
    private static float bKI;
    @C0064Am(aul = "edb4bd494549767006cf5cbc96400f64", aum = 1)
    private static float bKJ;
    @C0064Am(aul = "4d2211b4ffa8a2e43ed5b3119f51fd85", aum = 2)
    private static float bKK;
    @C0064Am(aul = "6860c26c366a744264a08780426e2fda", aum = 3)
    private static Asset bKL;
    @C0064Am(aul = "93e684948d1a2ac9e68c7877b0385042", aum = 4)
    private static Asset bKM;
    @C0064Am(aul = "43c01a85efbb48bfeaf7966e1c8866be", aum = 5)
    private static Asset bKN;
    @C0064Am(aul = "53af20313c0fcff9fb8e6751886fc3c1", aum = 6)
    private static float bKO;
    @C0064Am(aul = "318dda89e756a47147dcc0f35e93a6b2", aum = 7)
    private static C2686iZ<AttributeBuffType> bKP;

    static {
        m6344V();
    }

    public ModuleType() {
        super((C5540aNg) null);
        super.mo10S();
    }

    public ModuleType(C5540aNg ang) {
        super(ang);
    }

    /* renamed from: V */
    static void m6344V() {
        serialVersionUID = (long) 1;
        _m_fieldCount = BaseModuleType._m_fieldCount + 8;
        _m_methodCount = BaseModuleType._m_methodCount + 20;
        int i = BaseModuleType._m_fieldCount;
        C5663aRz[] arzArr = new C5663aRz[(i + 8)];
        C5663aRz b = C5640aRc.m17844b(ModuleType.class, "667ea0aaf9b7c24aab4ca39b1668d067", i);
        dqU = b;
        arzArr[i] = b;
        int i2 = i + 1;
        C5663aRz b2 = C5640aRc.m17844b(ModuleType.class, "edb4bd494549767006cf5cbc96400f64", i2);
        dqV = b2;
        arzArr[i2] = b2;
        int i3 = i2 + 1;
        C5663aRz b3 = C5640aRc.m17844b(ModuleType.class, "4d2211b4ffa8a2e43ed5b3119f51fd85", i3);
        dqW = b3;
        arzArr[i3] = b3;
        int i4 = i3 + 1;
        C5663aRz b4 = C5640aRc.m17844b(ModuleType.class, "6860c26c366a744264a08780426e2fda", i4);
        dqX = b4;
        arzArr[i4] = b4;
        int i5 = i4 + 1;
        C5663aRz b5 = C5640aRc.m17844b(ModuleType.class, "93e684948d1a2ac9e68c7877b0385042", i5);
        dqY = b5;
        arzArr[i5] = b5;
        int i6 = i5 + 1;
        C5663aRz b6 = C5640aRc.m17844b(ModuleType.class, "43c01a85efbb48bfeaf7966e1c8866be", i6);
        dqZ = b6;
        arzArr[i6] = b6;
        int i7 = i6 + 1;
        C5663aRz b7 = C5640aRc.m17844b(ModuleType.class, "53af20313c0fcff9fb8e6751886fc3c1", i7);
        dra = b7;
        arzArr[i7] = b7;
        int i8 = i7 + 1;
        C5663aRz b8 = C5640aRc.m17844b(ModuleType.class, "318dda89e756a47147dcc0f35e93a6b2", i8);
        drb = b8;
        arzArr[i8] = b8;
        int i9 = i8 + 1;
        _m_fields = arzArr;
        C1634Xv.m11725a((Object[]) BaseModuleType._m_fields, (Object[]) _m_fields);
        int i10 = BaseModuleType._m_methodCount;
        C2491fm[] fmVarArr = new C2491fm[(i10 + 20)];
        C2491fm a = C4105zY.m41624a(ModuleType.class, "c91ae9ccc30dd0051f4ca9b3504650ce", i10);
        drc = a;
        fmVarArr[i10] = a;
        int i11 = i10 + 1;
        C2491fm a2 = C4105zY.m41624a(ModuleType.class, "28f842db872b474bc3ca0d6f0d39f9a8", i11);
        drd = a2;
        fmVarArr[i11] = a2;
        int i12 = i11 + 1;
        C2491fm a3 = C4105zY.m41624a(ModuleType.class, "dc832d8579fabc603887889e152dcf5f", i12);
        dre = a3;
        fmVarArr[i12] = a3;
        int i13 = i12 + 1;
        C2491fm a4 = C4105zY.m41624a(ModuleType.class, "e2f32e56625ed7abe056cb5b215fb4a0", i13);
        drf = a4;
        fmVarArr[i13] = a4;
        int i14 = i13 + 1;
        C2491fm a5 = C4105zY.m41624a(ModuleType.class, "cc113421dc47f471de4f1149d00ed5d1", i14);
        drg = a5;
        fmVarArr[i14] = a5;
        int i15 = i14 + 1;
        C2491fm a6 = C4105zY.m41624a(ModuleType.class, "9bcc4500d48709ca47414db545272a36", i15);
        drh = a6;
        fmVarArr[i15] = a6;
        int i16 = i15 + 1;
        C2491fm a7 = C4105zY.m41624a(ModuleType.class, "bc871fa6badf369faeb6ac16e63d54c4", i16);
        dri = a7;
        fmVarArr[i16] = a7;
        int i17 = i16 + 1;
        C2491fm a8 = C4105zY.m41624a(ModuleType.class, "273c24c7e99da8e185e1e8abc2be76f1", i17);
        drj = a8;
        fmVarArr[i17] = a8;
        int i18 = i17 + 1;
        C2491fm a9 = C4105zY.m41624a(ModuleType.class, "823aa54c3d6adadba0d0bdd2486c6c19", i18);
        drk = a9;
        fmVarArr[i18] = a9;
        int i19 = i18 + 1;
        C2491fm a10 = C4105zY.m41624a(ModuleType.class, "68bcdf50163af3f02fa0a2d1ecdb9eaa", i19);
        drl = a10;
        fmVarArr[i19] = a10;
        int i20 = i19 + 1;
        C2491fm a11 = C4105zY.m41624a(ModuleType.class, "9124ff10224f1695ec80388249d4fd33", i20);
        drm = a11;
        fmVarArr[i20] = a11;
        int i21 = i20 + 1;
        C2491fm a12 = C4105zY.m41624a(ModuleType.class, "6edca9e20d7dff342559a7168c787235", i21);
        drn = a12;
        fmVarArr[i21] = a12;
        int i22 = i21 + 1;
        C2491fm a13 = C4105zY.m41624a(ModuleType.class, "3415e7d8aaaa920d9dc674e0bd899c0c", i22);
        dro = a13;
        fmVarArr[i22] = a13;
        int i23 = i22 + 1;
        C2491fm a14 = C4105zY.m41624a(ModuleType.class, "f78a0e4578ee45feb20c7adff1934bb6", i23);
        drp = a14;
        fmVarArr[i23] = a14;
        int i24 = i23 + 1;
        C2491fm a15 = C4105zY.m41624a(ModuleType.class, "d3d4b767d7d0282c263fd0268c876e3f", i24);
        drq = a15;
        fmVarArr[i24] = a15;
        int i25 = i24 + 1;
        C2491fm a16 = C4105zY.m41624a(ModuleType.class, "ca0a1160525418511b3c11e903b2b420", i25);
        drr = a16;
        fmVarArr[i25] = a16;
        int i26 = i25 + 1;
        C2491fm a17 = C4105zY.m41624a(ModuleType.class, "fcfac6818525f41d370c0f28c292a2c3", i26);
        drs = a17;
        fmVarArr[i26] = a17;
        int i27 = i26 + 1;
        C2491fm a18 = C4105zY.m41624a(ModuleType.class, "e3f11579e1d6773f82def515a121bfb8", i27);
        drt = a18;
        fmVarArr[i27] = a18;
        int i28 = i27 + 1;
        C2491fm a19 = C4105zY.m41624a(ModuleType.class, "d8493ffbdd1aa1972c2cca6ac39a88ad", i28);
        dru = a19;
        fmVarArr[i28] = a19;
        int i29 = i28 + 1;
        C2491fm a20 = C4105zY.m41624a(ModuleType.class, "044f7d13967283b8f7c6db591a569d8f", i29);
        f957dN = a20;
        fmVarArr[i29] = a20;
        int i30 = i29 + 1;
        _m_methods = fmVarArr;
        C1634Xv.m11725a((Object[]) BaseModuleType._m_methods, (Object[]) _m_methods);
        ___iScriptClass = aUO.m18566a(ModuleType.class, C5733aUr.class, _m_fields, _m_methods);
    }

    /* renamed from: I */
    private void m6343I(C2686iZ iZVar) {
        bFf().mo5608dq().mo3197f(drb, iZVar);
    }

    /* renamed from: at */
    private void m6347at(Asset tCVar) {
        bFf().mo5608dq().mo3197f(dqX, tCVar);
    }

    /* renamed from: au */
    private void m6348au(Asset tCVar) {
        bFf().mo5608dq().mo3197f(dqY, tCVar);
    }

    /* renamed from: av */
    private void m6349av(Asset tCVar) {
        bFf().mo5608dq().mo3197f(dqZ, tCVar);
    }

    private float bdk() {
        return bFf().mo5608dq().mo3211m(dqU);
    }

    private float bdl() {
        return bFf().mo5608dq().mo3211m(dqV);
    }

    private float bdm() {
        return bFf().mo5608dq().mo3211m(dqW);
    }

    private Asset bdn() {
        return (Asset) bFf().mo5608dq().mo3214p(dqX);
    }

    private Asset bdo() {
        return (Asset) bFf().mo5608dq().mo3214p(dqY);
    }

    private Asset bdp() {
        return (Asset) bFf().mo5608dq().mo3214p(dqZ);
    }

    private float bdq() {
        return bFf().mo5608dq().mo3211m(dra);
    }

    private C2686iZ bdr() {
        return (C2686iZ) bFf().mo5608dq().mo3214p(drb);
    }

    /* renamed from: fX */
    private void m6353fX(float f) {
        bFf().mo5608dq().mo3150a(dqU, f);
    }

    /* renamed from: fY */
    private void m6354fY(float f) {
        bFf().mo5608dq().mo3150a(dqV, f);
    }

    /* renamed from: fZ */
    private void m6355fZ(float f) {
        bFf().mo5608dq().mo3150a(dqW, f);
    }

    /* renamed from: ga */
    private void m6357ga(float f) {
        bFf().mo5608dq().mo3150a(dra, f);
    }

    /* renamed from: T */
    public C6494any mo11T() {
        return ___iScriptClass;
    }

    /* renamed from: U */
    public C2491fm[] mo12U() {
        return _m_methods;
    }

    /* renamed from: W */
    public Object mo13W() {
        return new C5733aUr(this);
    }

    /* renamed from: a */
    public Object mo14a(C0495Gr gr) {
        Object[] args = gr.getArgs();
        switch (gr.mo2417hq() - BaseModuleType._m_methodCount) {
            case 0:
                return new Float(bds());
            case 1:
                m6358gb(((Float) args[0]).floatValue());
                return null;
            case 2:
                return new Float(bdu());
            case 3:
                m6359gd(((Float) args[0]).floatValue());
                return null;
            case 4:
                return new Float(bdw());
            case 5:
                m6360gf(((Float) args[0]).floatValue());
                return null;
            case 6:
                return bdy();
            case 7:
                m6350aw((Asset) args[0]);
                return null;
            case 8:
                return bdA();
            case 9:
                m6351ay((Asset) args[0]);
                return null;
            case 10:
                return bdC();
            case 11:
                m6345aA((Asset) args[0]);
                return null;
            case 12:
                return new Float(bdE());
            case 13:
                m6361gh(((Float) args[0]).floatValue());
                return null;
            case 14:
                m6352e((AttributeBuffType) args[0]);
                return null;
            case 15:
                m6356g((AttributeBuffType) args[0]);
                return null;
            case 16:
                return bdG();
            case 17:
                return bdI();
            case 18:
                return bdK();
            case 19:
                return m6346aT();
            default:
                return super.mo14a(gr);
        }
    }

    /* renamed from: a */
    public void mo15a(Collection collection, C0495Gr gr) {
        super.mo15a(collection, gr);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Asset Active G F X")
    /* renamed from: aB */
    public void mo3524aB(Asset tCVar) {
        switch (bFf().mo6893i(drn)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, drn, new Object[]{tCVar}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, drn, new Object[]{tCVar}));
                break;
        }
        m6345aA(tCVar);
    }

    /* access modifiers changed from: protected */
    /* renamed from: aU */
    public <T extends aDJ> T mo745aU() {
        switch (bFf().mo6893i(f957dN)) {
            case 0:
                return null;
            case 2:
                return (aDJ) bFf().mo5606d(new aCE(this, f957dN, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f957dN, new Object[0]));
                break;
        }
        return m6346aT();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Asset Warmup G F X")
    /* renamed from: ax */
    public void mo3525ax(Asset tCVar) {
        switch (bFf().mo6893i(drj)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, drj, new Object[]{tCVar}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, drj, new Object[]{tCVar}));
                break;
        }
        m6350aw(tCVar);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Asset Cooldown G F X")
    /* renamed from: az */
    public void mo3526az(Asset tCVar) {
        switch (bFf().mo6893i(drl)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, drl, new Object[]{tCVar}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, drl, new Object[]{tCVar}));
                break;
        }
        m6351ay(tCVar);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Asset Cooldown G F X")
    public Asset bdB() {
        switch (bFf().mo6893i(drk)) {
            case 0:
                return null;
            case 2:
                return (Asset) bFf().mo5606d(new aCE(this, drk, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, drk, new Object[0]));
                break;
        }
        return bdA();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Asset Active G F X")
    public Asset bdD() {
        switch (bFf().mo6893i(drm)) {
            case 0:
                return null;
            case 2:
                return (Asset) bFf().mo5606d(new aCE(this, drm, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, drm, new Object[0]));
                break;
        }
        return bdC();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Module Range")
    public float bdF() {
        switch (bFf().mo6893i(dro)) {
            case 0:
                return 0.0f;
            case 2:
                return ((Float) bFf().mo5606d(new aCE(this, dro, new Object[0]))).floatValue();
            case 3:
                bFf().mo5606d(new aCE(this, dro, new Object[0]));
                break;
        }
        return bdE();
    }

    public C2686iZ<AttributeBuffType> bdH() {
        switch (bFf().mo6893i(drs)) {
            case 0:
                return null;
            case 2:
                return (C2686iZ) bFf().mo5606d(new aCE(this, drs, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, drs, new Object[0]));
                break;
        }
        return bdG();
    }

    @C3248pc(aYR = C3248pc.C3250b.ITERATOR, displayName = "Attribute Buff Types")
    public Set<AttributeBuffType> bdJ() {
        switch (bFf().mo6893i(drt)) {
            case 0:
                return null;
            case 2:
                return (Set) bFf().mo5606d(new aCE(this, drt, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, drt, new Object[0]));
                break;
        }
        return bdI();
    }

    public Module bdL() {
        switch (bFf().mo6893i(dru)) {
            case 0:
                return null;
            case 2:
                return (Module) bFf().mo5606d(new aCE(this, dru, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, dru, new Object[0]));
                break;
        }
        return bdK();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Module Duration")
    public float bdt() {
        switch (bFf().mo6893i(drc)) {
            case 0:
                return 0.0f;
            case 2:
                return ((Float) bFf().mo5606d(new aCE(this, drc, new Object[0]))).floatValue();
            case 3:
                bFf().mo5606d(new aCE(this, drc, new Object[0]));
                break;
        }
        return bds();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Module Warmup")
    public float bdv() {
        switch (bFf().mo6893i(dre)) {
            case 0:
                return 0.0f;
            case 2:
                return ((Float) bFf().mo5606d(new aCE(this, dre, new Object[0]))).floatValue();
            case 3:
                bFf().mo5606d(new aCE(this, dre, new Object[0]));
                break;
        }
        return bdu();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Module Cooldown")
    public float bdx() {
        switch (bFf().mo6893i(drg)) {
            case 0:
                return 0.0f;
            case 2:
                return ((Float) bFf().mo5606d(new aCE(this, drg, new Object[0]))).floatValue();
            case 3:
                bFf().mo5606d(new aCE(this, drg, new Object[0]));
                break;
        }
        return bdw();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Asset Warmup G F X")
    public Asset bdz() {
        switch (bFf().mo6893i(dri)) {
            case 0:
                return null;
            case 2:
                return (Asset) bFf().mo5606d(new aCE(this, dri, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, dri, new Object[0]));
                break;
        }
        return bdy();
    }

    /* renamed from: c */
    public C5663aRz[] mo25c() {
        return _m_fields;
    }

    @C3248pc(aYR = C3248pc.C3250b.ADDER, displayName = "Attribute Buff Types")
    /* renamed from: f */
    public void mo3537f(AttributeBuffType api) {
        switch (bFf().mo6893i(drq)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, drq, new Object[]{api}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, drq, new Object[]{api}));
                break;
        }
        m6352e(api);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Module Duration")
    /* renamed from: gc */
    public void mo3538gc(float f) {
        switch (bFf().mo6893i(drd)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, drd, new Object[]{new Float(f)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, drd, new Object[]{new Float(f)}));
                break;
        }
        m6358gb(f);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Module Warmup")
    /* renamed from: ge */
    public void mo3539ge(float f) {
        switch (bFf().mo6893i(drf)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, drf, new Object[]{new Float(f)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, drf, new Object[]{new Float(f)}));
                break;
        }
        m6359gd(f);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Module Cooldown")
    /* renamed from: gg */
    public void mo3540gg(float f) {
        switch (bFf().mo6893i(drh)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, drh, new Object[]{new Float(f)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, drh, new Object[]{new Float(f)}));
                break;
        }
        m6360gf(f);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Module Range")
    /* renamed from: gi */
    public void mo3541gi(float f) {
        switch (bFf().mo6893i(drp)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, drp, new Object[]{new Float(f)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, drp, new Object[]{new Float(f)}));
                break;
        }
        m6361gh(f);
    }

    @C3248pc(aYR = C3248pc.C3250b.REMOVER, displayName = "Attribute Buff Types")
    /* renamed from: h */
    public void mo3542h(AttributeBuffType api) {
        switch (bFf().mo6893i(drr)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, drr, new Object[]{api}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, drr, new Object[]{api}));
                break;
        }
        m6356g(api);
    }

    /* renamed from: S */
    public void mo10S() {
        super.mo10S();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Module Duration")
    @C0064Am(aul = "c91ae9ccc30dd0051f4ca9b3504650ce", aum = 0)
    private float bds() {
        return bdk();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Module Duration")
    @C0064Am(aul = "28f842db872b474bc3ca0d6f0d39f9a8", aum = 0)
    /* renamed from: gb */
    private void m6358gb(float f) {
        m6353fX(f);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Module Warmup")
    @C0064Am(aul = "dc832d8579fabc603887889e152dcf5f", aum = 0)
    private float bdu() {
        return bdl();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Module Warmup")
    @C0064Am(aul = "e2f32e56625ed7abe056cb5b215fb4a0", aum = 0)
    /* renamed from: gd */
    private void m6359gd(float f) {
        m6354fY(f);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Module Cooldown")
    @C0064Am(aul = "cc113421dc47f471de4f1149d00ed5d1", aum = 0)
    private float bdw() {
        return bdm();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Module Cooldown")
    @C0064Am(aul = "9bcc4500d48709ca47414db545272a36", aum = 0)
    /* renamed from: gf */
    private void m6360gf(float f) {
        m6355fZ(f);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Asset Warmup G F X")
    @C0064Am(aul = "bc871fa6badf369faeb6ac16e63d54c4", aum = 0)
    private Asset bdy() {
        return bdn();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Asset Warmup G F X")
    @C0064Am(aul = "273c24c7e99da8e185e1e8abc2be76f1", aum = 0)
    /* renamed from: aw */
    private void m6350aw(Asset tCVar) {
        m6347at(tCVar);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Asset Cooldown G F X")
    @C0064Am(aul = "823aa54c3d6adadba0d0bdd2486c6c19", aum = 0)
    private Asset bdA() {
        return bdo();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Asset Cooldown G F X")
    @C0064Am(aul = "68bcdf50163af3f02fa0a2d1ecdb9eaa", aum = 0)
    /* renamed from: ay */
    private void m6351ay(Asset tCVar) {
        m6348au(tCVar);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Asset Active G F X")
    @C0064Am(aul = "9124ff10224f1695ec80388249d4fd33", aum = 0)
    private Asset bdC() {
        return bdp();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Asset Active G F X")
    @C0064Am(aul = "6edca9e20d7dff342559a7168c787235", aum = 0)
    /* renamed from: aA */
    private void m6345aA(Asset tCVar) {
        m6349av(tCVar);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Module Range")
    @C0064Am(aul = "3415e7d8aaaa920d9dc674e0bd899c0c", aum = 0)
    private float bdE() {
        return bdq();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Module Range")
    @C0064Am(aul = "f78a0e4578ee45feb20c7adff1934bb6", aum = 0)
    /* renamed from: gh */
    private void m6361gh(float f) {
        m6357ga(f);
    }

    @C3248pc(aYR = C3248pc.C3250b.ADDER, displayName = "Attribute Buff Types")
    @C0064Am(aul = "d3d4b767d7d0282c263fd0268c876e3f", aum = 0)
    /* renamed from: e */
    private void m6352e(AttributeBuffType api) {
        bdr().add(api);
    }

    @C3248pc(aYR = C3248pc.C3250b.REMOVER, displayName = "Attribute Buff Types")
    @C0064Am(aul = "ca0a1160525418511b3c11e903b2b420", aum = 0)
    /* renamed from: g */
    private void m6356g(AttributeBuffType api) {
        bdr().remove(api);
    }

    @C0064Am(aul = "fcfac6818525f41d370c0f28c292a2c3", aum = 0)
    private C2686iZ<AttributeBuffType> bdG() {
        return bdr();
    }

    @C3248pc(aYR = C3248pc.C3250b.ITERATOR, displayName = "Attribute Buff Types")
    @C0064Am(aul = "e3f11579e1d6773f82def515a121bfb8", aum = 0)
    private Set<AttributeBuffType> bdI() {
        return Collections.unmodifiableSet(bdr());
    }

    @C0064Am(aul = "d8493ffbdd1aa1972c2cca6ac39a88ad", aum = 0)
    private Module bdK() {
        return (Module) mo745aU();
    }

    @C0064Am(aul = "044f7d13967283b8f7c6db591a569d8f", aum = 0)
    /* renamed from: aT */
    private <T extends aDJ> T m6346aT() {
        T t = (Module) bFf().mo6865M(Module.class);
        t.mo23063e(this);
        return t;
    }
}
