package game.script.item;

import game.network.message.externalizable.aCE;
import logic.baa.C1616Xf;
import logic.baa.C1634Xv;
import logic.baa.C5511aMd;
import logic.baa.C6485anp;
import logic.data.mbean.C5838abS;
import logic.res.code.C5640aRc;
import logic.res.code.C5663aRz;
import logic.res.code.C6494any;
import logic.res.html.C2491fm;
import p001a.*;

import java.util.Collection;

@C2712iu
@C5511aMd
@C6485anp
/* renamed from: a.AT */
/* compiled from: a */
public abstract class BagItem extends Item implements C1616Xf {

    /* renamed from: Rx */
    public static final C2491fm f61Rx = null;
    public static final int _m_fieldCount = 0;
    public static final C5663aRz[] _m_fields = null;
    public static final int _m_methodCount = 0;
    public static final C2491fm[] _m_methods = null;
    public static final C2491fm aGt = null;
    public static final C2491fm bHF = null;
    public static final C5663aRz ckk = null;
    public static final C2491fm ckl = null;
    public static final long serialVersionUID = 0;
    public static C6494any ___iScriptClass;
    @C0064Am(aul = "e0e9b8029e282475625d70d8e45e5872", aum = 0)
    private static float ckj;

    static {
        m394V();
    }

    public BagItem() {
        super((C5540aNg) null);
        super.mo10S();
    }

    public BagItem(C5540aNg ang) {
        super(ang);
    }

    public BagItem(ItemType jCVar) {
        super((C5540aNg) null);
        super.mo306n(jCVar);
    }

    /* renamed from: V */
    static void m394V() {
        serialVersionUID = (long) 1;
        _m_fieldCount = Item._m_fieldCount + 1;
        _m_methodCount = Item._m_methodCount + 4;
        int i = Item._m_fieldCount;
        C5663aRz[] arzArr = new C5663aRz[(i + 1)];
        C5663aRz b = C5640aRc.m17844b(BagItem.class, "e0e9b8029e282475625d70d8e45e5872", i);
        ckk = b;
        arzArr[i] = b;
        int i2 = i + 1;
        _m_fields = arzArr;
        C1634Xv.m11725a((Object[]) Item._m_fields, (Object[]) _m_fields);
        int i3 = Item._m_methodCount;
        C2491fm[] fmVarArr = new C2491fm[(i3 + 4)];
        C2491fm a = C4105zY.m41624a(BagItem.class, "4cadc566c44387f99dfaffab9e54a61b", i3);
        bHF = a;
        fmVarArr[i3] = a;
        int i4 = i3 + 1;
        C2491fm a2 = C4105zY.m41624a(BagItem.class, "cf47c248396f94a841302e20875da840", i4);
        f61Rx = a2;
        fmVarArr[i4] = a2;
        int i5 = i4 + 1;
        C2491fm a3 = C4105zY.m41624a(BagItem.class, "7c257c1e39291d051a1a2417cab5fb29", i5);
        aGt = a3;
        fmVarArr[i5] = a3;
        int i6 = i5 + 1;
        C2491fm a4 = C4105zY.m41624a(BagItem.class, "9d31480ad9d76768b5c2fff39b556379", i6);
        ckl = a4;
        fmVarArr[i6] = a4;
        int i7 = i6 + 1;
        _m_methods = fmVarArr;
        C1634Xv.m11725a((Object[]) Item._m_methods, (Object[]) _m_methods);
        ___iScriptClass = aUO.m18566a(BagItem.class, C5838abS.class, _m_fields, _m_methods);
    }

    @C0064Am(aul = "4cadc566c44387f99dfaffab9e54a61b", aum = 0)
    private ItemLocation aoF() {
        throw new aWi(new aCE(this, bHF, new Object[0]));
    }

    private float ayb() {
        return ((BagItemType) getType()).ayd();
    }

    /* renamed from: ew */
    private void m395ew(float f) {
        throw new C6039afL();
    }

    /* renamed from: Iq */
    public int mo302Iq() {
        switch (bFf().mo6893i(aGt)) {
            case 0:
                return 0;
            case 2:
                return ((Integer) bFf().mo5606d(new aCE(this, aGt, new Object[0]))).intValue();
            case 3:
                bFf().mo5606d(new aCE(this, aGt, new Object[0]));
                break;
        }
        return m393PK();
    }

    /* renamed from: S */
    public void mo10S() {
        super.mo10S();
    }

    /* renamed from: T */
    public C6494any mo11T() {
        return ___iScriptClass;
    }

    /* renamed from: U */
    public C2491fm[] mo12U() {
        return _m_methods;
    }

    /* renamed from: W */
    public Object mo13W() {
        return new C5838abS(this);
    }

    /* renamed from: a */
    public Object mo14a(C0495Gr gr) {
        Object[] args = gr.getArgs();
        switch (gr.mo2417hq() - Item._m_methodCount) {
            case 0:
                return aoF();
            case 1:
                return new Boolean(m396k((Item) args[0]));
            case 2:
                return new Integer(m393PK());
            case 3:
                return new Float(ayc());
            default:
                return super.mo14a(gr);
        }
    }

    /* renamed from: a */
    public void mo15a(Collection collection, C0495Gr gr) {
        super.mo15a(collection, gr);
    }

    public ItemLocation aoG() {
        switch (bFf().mo6893i(bHF)) {
            case 0:
                return null;
            case 2:
                return (ItemLocation) bFf().mo5606d(new aCE(this, bHF, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, bHF, new Object[0]));
                break;
        }
        return aoF();
    }

    public float ayd() {
        switch (bFf().mo6893i(ckl)) {
            case 0:
                return 0.0f;
            case 2:
                return ((Float) bFf().mo5606d(new aCE(this, ckl, new Object[0]))).floatValue();
            case 3:
                bFf().mo5606d(new aCE(this, ckl, new Object[0]));
                break;
        }
        return ayc();
    }

    /* renamed from: c */
    public C5663aRz[] mo25c() {
        return _m_fields;
    }

    /* renamed from: l */
    public boolean mo305l(Item auq) {
        switch (bFf().mo6893i(f61Rx)) {
            case 0:
                return false;
            case 2:
                return ((Boolean) bFf().mo5606d(new aCE(this, f61Rx, new Object[]{auq}))).booleanValue();
            case 3:
                bFf().mo5606d(new aCE(this, f61Rx, new Object[]{auq}));
                break;
        }
        return m396k(auq);
    }

    /* renamed from: n */
    public void mo306n(ItemType jCVar) {
        super.mo306n(jCVar);
        mo16393gL(true);
    }

    @C0064Am(aul = "cf47c248396f94a841302e20875da840", aum = 0)
    /* renamed from: k */
    private boolean m396k(Item auq) {
        return true;
    }

    @C0064Am(aul = "7c257c1e39291d051a1a2417cab5fb29", aum = 0)
    /* renamed from: PK */
    private int m393PK() {
        return 1;
    }

    @C0064Am(aul = "9d31480ad9d76768b5c2fff39b556379", aum = 0)
    private float ayc() {
        return ayb();
    }
}
