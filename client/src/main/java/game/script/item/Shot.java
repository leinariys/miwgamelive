package game.script.item;

import com.hoplon.geometry.Vec3f;
import game.geometry.Quat4fWrap;
import game.geometry.Vec3d;
import game.network.message.externalizable.aCE;
import game.script.Actor;
import game.script.ship.Ship;
import game.script.simulation.Space;
import logic.aaa.C2235dB;
import logic.baa.C1616Xf;
import logic.baa.C1634Xv;
import logic.baa.C5511aMd;
import logic.baa.C6485anp;
import logic.bbb.C4029yK;
import logic.bbb.C6348alI;
import logic.data.mbean.C6514aoS;
import logic.res.code.C5640aRc;
import logic.res.code.C5663aRz;
import logic.res.code.C6494any;
import logic.res.html.C2491fm;
import logic.thred.C6339akz;
import logic.thred.LogPrinter;
import org.apache.commons.logging.Log;
import p001a.*;

import java.util.Collection;

@C6485anp
@C3122oB(mo20937Uq = C3122oB.C3123a.NOT_REPLICATED)
@C1020Ot(drz = C1020Ot.C1021a.NOT_PERSISTED)
@C5511aMd
@C0909NL
/* renamed from: a.cA */
/* compiled from: a */
public class Shot extends Actor implements C1616Xf {
    public static final int _m_fieldCount = 0;
    public static final C5663aRz[] _m_fields = null;
    public static final int _m_methodCount = 0;
    public static final C2491fm[] _m_methods = null;
    public static final long serialVersionUID = 0;
    /* renamed from: uK */
    public static final C5663aRz f5948uK = null;
    /* renamed from: uM */
    public static final C5663aRz f5950uM = null;
    /* renamed from: uN */
    public static final C5663aRz f5951uN = null;
    /* renamed from: uP */
    public static final C5663aRz f5953uP = null;
    /* renamed from: uR */
    public static final C5663aRz f5955uR = null;
    /* renamed from: uS */
    public static final C5663aRz f5956uS = null;
    /* renamed from: uU */
    public static final C5663aRz f5958uU = null;
    /* renamed from: uV */
    public static final float f5959uV = 10000.0f;
    /* renamed from: uW */
    public static final C2491fm f5960uW = null;
    /* renamed from: uX */
    public static final C2491fm f5961uX = null;
    /* renamed from: uY */
    public static final C2491fm f5962uY = null;
    /* renamed from: uZ */
    public static final C2491fm f5963uZ = null;
    /* renamed from: va */
    public static final C2491fm f5964va = null;
    /* renamed from: vb */
    public static final C2491fm f5965vb = null;
    /* renamed from: vc */
    public static final C2491fm f5966vc = null;
    /* renamed from: vd */
    public static final C2491fm f5967vd = null;
    /* renamed from: ve */
    public static final C2491fm f5968ve = null;
    /* renamed from: vf */
    public static final C2491fm f5969vf = null;
    /* renamed from: vg */
    public static final C2491fm f5970vg = null;
    /* renamed from: vh */
    public static final C2491fm f5971vh = null;
    /* renamed from: vi */
    public static final C2491fm f5972vi = null;
    /* renamed from: vj */
    public static final C2491fm f5973vj = null;
    /* renamed from: vk */
    public static final C2491fm f5974vk = null;
    /* renamed from: vl */
    public static final C2491fm f5975vl = null;
    /* renamed from: vm */
    public static final C2491fm f5976vm = null;
    /* renamed from: vn */
    public static final C2491fm f5977vn = null;
    /* renamed from: vo */
    public static final C2491fm f5978vo = null;
    /* renamed from: vp */
    public static final C2491fm f5979vp = null;
    /* renamed from: vq */
    public static final C2491fm f5980vq = null;
    /* renamed from: vr */
    public static final C2491fm f5981vr = null;
    /* renamed from: vs */
    public static final C2491fm f5982vs = null;
    /* renamed from: vt */
    public static final C2491fm f5983vt = null;
    private static final Log logger = LogPrinter.setClass(Shot.class);
    public static C6494any ___iScriptClass = null;
    @C0064Am(aul = "89775bbf1e7d192b90974eed9ca7a830", aum = 2)
    private static float range = 0.0f;
    @C0064Am(aul = "eb14e9cc6a6b6ba7f279db046ecb1a1d", aum = 5)
    private static float speed = 0.0f;
    @C0064Am(aul = "6b33bbd61e79e4456cce240ad4a65239", aum = 0)

    /* renamed from: uJ */
    private static C5260aCm f5947uJ = null;
    @C0064Am(aul = "9156aef2a5471a2d4c9b5ad976f676aa", aum = 1)

    /* renamed from: uL */
    private static Vec3d f5949uL = null;
    @C0064Am(aul = "ce7883cdc659628b08cfba96dda1fbbc", aum = 3)
    @C5256aCi

    /* renamed from: uO */
    private static String f5952uO = null;
    @C0064Am(aul = "56867addc136999c5073792a53bc2f78", aum = 4)

    /* renamed from: uQ */
    private static String f5954uQ = null;
    @C0064Am(aul = "3859a7b5413c7e09bc9267be96bf39cf", aum = 6)

    /* renamed from: uT */
    private static String f5957uT = null;

    static {
        m28160V();
    }

    public Shot() {
        super((C5540aNg) null);
        super.mo10S();
    }

    public Shot(C5260aCm acm, float f) {
        super((C5540aNg) null);
        super._m_script_init(acm, f);
    }

    public Shot(C5540aNg ang) {
        super(ang);
    }

    /* renamed from: V */
    static void m28160V() {
        serialVersionUID = (long) 1;
        _m_fieldCount = Actor._m_fieldCount + 7;
        _m_methodCount = Actor._m_methodCount + 24;
        int i = Actor._m_fieldCount;
        C5663aRz[] arzArr = new C5663aRz[(i + 7)];
        C5663aRz b = C5640aRc.m17844b(Shot.class, "6b33bbd61e79e4456cce240ad4a65239", i);
        f5948uK = b;
        arzArr[i] = b;
        int i2 = i + 1;
        C5663aRz b2 = C5640aRc.m17844b(Shot.class, "9156aef2a5471a2d4c9b5ad976f676aa", i2);
        f5950uM = b2;
        arzArr[i2] = b2;
        int i3 = i2 + 1;
        C5663aRz b3 = C5640aRc.m17844b(Shot.class, "89775bbf1e7d192b90974eed9ca7a830", i3);
        f5951uN = b3;
        arzArr[i3] = b3;
        int i4 = i3 + 1;
        C5663aRz b4 = C5640aRc.m17844b(Shot.class, "ce7883cdc659628b08cfba96dda1fbbc", i4);
        f5953uP = b4;
        arzArr[i4] = b4;
        int i5 = i4 + 1;
        C5663aRz b5 = C5640aRc.m17844b(Shot.class, "56867addc136999c5073792a53bc2f78", i5);
        f5955uR = b5;
        arzArr[i5] = b5;
        int i6 = i5 + 1;
        C5663aRz b6 = C5640aRc.m17844b(Shot.class, "eb14e9cc6a6b6ba7f279db046ecb1a1d", i6);
        f5956uS = b6;
        arzArr[i6] = b6;
        int i7 = i6 + 1;
        C5663aRz b7 = C5640aRc.m17844b(Shot.class, "3859a7b5413c7e09bc9267be96bf39cf", i7);
        f5958uU = b7;
        arzArr[i7] = b7;
        int i8 = i7 + 1;
        _m_fields = arzArr;
        C1634Xv.m11725a((Object[]) Actor._m_fields, (Object[]) _m_fields);
        int i9 = Actor._m_methodCount;
        C2491fm[] fmVarArr = new C2491fm[(i9 + 24)];
        C2491fm a = C4105zY.m41624a(Shot.class, "5f7575d91c69c86064422d3c126f70b6", i9);
        f5960uW = a;
        fmVarArr[i9] = a;
        int i10 = i9 + 1;
        C2491fm a2 = C4105zY.m41624a(Shot.class, "3b48c68971fd2dbe3624f4c2e758471f", i10);
        f5961uX = a2;
        fmVarArr[i10] = a2;
        int i11 = i10 + 1;
        C2491fm a3 = C4105zY.m41624a(Shot.class, "7599770ebed7c7fc4899966ac115399d", i11);
        f5962uY = a3;
        fmVarArr[i11] = a3;
        int i12 = i11 + 1;
        C2491fm a4 = C4105zY.m41624a(Shot.class, "6292f06f317ab8f24cb637c985dad806", i12);
        f5963uZ = a4;
        fmVarArr[i12] = a4;
        int i13 = i12 + 1;
        C2491fm a5 = C4105zY.m41624a(Shot.class, "4bd0fb1dfa0e1cc8120f2be7f6b2a045", i13);
        f5964va = a5;
        fmVarArr[i13] = a5;
        int i14 = i13 + 1;
        C2491fm a6 = C4105zY.m41624a(Shot.class, "bc4f9b55c4a40626c26d03eb0d92575e", i14);
        f5965vb = a6;
        fmVarArr[i14] = a6;
        int i15 = i14 + 1;
        C2491fm a7 = C4105zY.m41624a(Shot.class, "b823556053a10423a6c8a83123bba513", i15);
        f5966vc = a7;
        fmVarArr[i15] = a7;
        int i16 = i15 + 1;
        C2491fm a8 = C4105zY.m41624a(Shot.class, "b608de20bede31947e2a064c5ea15822", i16);
        f5967vd = a8;
        fmVarArr[i16] = a8;
        int i17 = i16 + 1;
        C2491fm a9 = C4105zY.m41624a(Shot.class, "c23add059f1bb54c8d052b63630ea0c7", i17);
        f5968ve = a9;
        fmVarArr[i17] = a9;
        int i18 = i17 + 1;
        C2491fm a10 = C4105zY.m41624a(Shot.class, "db2404a1ee72c2f56c456b678bc73a69", i18);
        f5969vf = a10;
        fmVarArr[i18] = a10;
        int i19 = i18 + 1;
        C2491fm a11 = C4105zY.m41624a(Shot.class, "613b2b856c4c48dcca1ec397b7eada07", i19);
        f5970vg = a11;
        fmVarArr[i19] = a11;
        int i20 = i19 + 1;
        C2491fm a12 = C4105zY.m41624a(Shot.class, "53f10e5327cd40609478ef2825dad1ec", i20);
        f5971vh = a12;
        fmVarArr[i20] = a12;
        int i21 = i20 + 1;
        C2491fm a13 = C4105zY.m41624a(Shot.class, "90d51840848d324b37cdee147a4d6ed0", i21);
        f5972vi = a13;
        fmVarArr[i21] = a13;
        int i22 = i21 + 1;
        C2491fm a14 = C4105zY.m41624a(Shot.class, "0586eedb539071fb57fd66f40a279e4e", i22);
        f5973vj = a14;
        fmVarArr[i22] = a14;
        int i23 = i22 + 1;
        C2491fm a15 = C4105zY.m41624a(Shot.class, "42840fef89bf561cbcecd4219adbcf42", i23);
        f5974vk = a15;
        fmVarArr[i23] = a15;
        int i24 = i23 + 1;
        C2491fm a16 = C4105zY.m41624a(Shot.class, "2ab838bfedab76ddd1100dc391f98847", i24);
        f5975vl = a16;
        fmVarArr[i24] = a16;
        int i25 = i24 + 1;
        C2491fm a17 = C4105zY.m41624a(Shot.class, "3574865d02ebe90417fe3c6d410e6478", i25);
        f5976vm = a17;
        fmVarArr[i25] = a17;
        int i26 = i25 + 1;
        C2491fm a18 = C4105zY.m41624a(Shot.class, "ff9c19847a5e22c695027f40d49e9072", i26);
        f5977vn = a18;
        fmVarArr[i26] = a18;
        int i27 = i26 + 1;
        C2491fm a19 = C4105zY.m41624a(Shot.class, "ca67968a1ab7ae820625a7a3e3cdfca6", i27);
        f5978vo = a19;
        fmVarArr[i27] = a19;
        int i28 = i27 + 1;
        C2491fm a20 = C4105zY.m41624a(Shot.class, "f032bb80091d7d96aa8f9b8e9c1eced3", i28);
        f5979vp = a20;
        fmVarArr[i28] = a20;
        int i29 = i28 + 1;
        C2491fm a21 = C4105zY.m41624a(Shot.class, "ba7669e5631967a59f06818dcc56241b", i29);
        f5980vq = a21;
        fmVarArr[i29] = a21;
        int i30 = i29 + 1;
        C2491fm a22 = C4105zY.m41624a(Shot.class, "3cf043a8038355375a20989a99ce57af", i30);
        f5981vr = a22;
        fmVarArr[i30] = a22;
        int i31 = i30 + 1;
        C2491fm a23 = C4105zY.m41624a(Shot.class, "c3969079ae654f16293061d2d01fb557", i31);
        f5982vs = a23;
        fmVarArr[i31] = a23;
        int i32 = i31 + 1;
        C2491fm a24 = C4105zY.m41624a(Shot.class, "17f1fa5a9fd16d07657dada1cfe01e74", i32);
        f5983vt = a24;
        fmVarArr[i32] = a24;
        int i33 = i32 + 1;
        _m_methods = fmVarArr;
        C1634Xv.m11725a((Object[]) Actor._m_methods, (Object[]) _m_methods);
        ___iScriptClass = aUO.m18566a(Shot.class, C6514aoS.class, _m_fields, _m_methods);
    }

    /* renamed from: F */
    private void m28151F(String str) {
        bFf().mo5608dq().mo3197f(f5953uP, str);
    }

    /* renamed from: G */
    private void m28152G(float f) {
        bFf().mo5608dq().mo3150a(f5951uN, f);
    }

    /* renamed from: G */
    private void m28153G(String str) {
        bFf().mo5608dq().mo3197f(f5955uR, str);
    }

    /* renamed from: H */
    private void m28154H(float f) {
        bFf().mo5608dq().mo3150a(f5956uS, f);
    }

    /* renamed from: H */
    private void m28155H(String str) {
        bFf().mo5608dq().mo3197f(f5958uU, str);
    }

    /* renamed from: a */
    private void m28166a(C5260aCm acm) {
        bFf().mo5608dq().mo3197f(f5948uK, acm);
    }

    /* renamed from: b */
    private void m28169b(Vec3d ajr) {
        bFf().mo5608dq().mo3197f(f5950uM, ajr);
    }

    /* renamed from: id */
    private C5260aCm m28174id() {
        return (C5260aCm) bFf().mo5608dq().mo3214p(f5948uK);
    }

    /* renamed from: ie */
    private Vec3d m28175ie() {
        return (Vec3d) bFf().mo5608dq().mo3214p(f5950uM);
    }

    /* renamed from: if */
    private float m28176if() {
        return bFf().mo5608dq().mo3211m(f5951uN);
    }

    /* renamed from: ig */
    private String m28177ig() {
        return (String) bFf().mo5608dq().mo3214p(f5953uP);
    }

    /* renamed from: ih */
    private String m28178ih() {
        return (String) bFf().mo5608dq().mo3214p(f5955uR);
    }

    /* renamed from: ii */
    private float m28179ii() {
        return bFf().mo5608dq().mo3211m(f5956uS);
    }

    /* renamed from: ij */
    private String m28180ij() {
        return (String) bFf().mo5608dq().mo3214p(f5958uU);
    }

    /* renamed from: iy */
    private Shot m28189iy() {
        switch (bFf().mo6893i(f5975vl)) {
            case 0:
                return null;
            case 2:
                return (Shot) bFf().mo5606d(new aCE(this, f5975vl, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f5975vl, new Object[0]));
                break;
        }
        return m28188ix();
    }

    /* renamed from: J */
    public void mo17511J(String str) {
        switch (bFf().mo6893i(f5967vd)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f5967vd, new Object[]{str}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f5967vd, new Object[]{str}));
                break;
        }
        m28157I(str);
    }

    /* renamed from: L */
    public void mo17512L(String str) {
        switch (bFf().mo6893i(f5969vf)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f5969vf, new Object[]{str}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f5969vf, new Object[]{str}));
                break;
        }
        m28158K(str);
    }

    /* renamed from: N */
    public void mo17513N(String str) {
        switch (bFf().mo6893i(f5973vj)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f5973vj, new Object[]{str}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f5973vj, new Object[]{str}));
                break;
        }
        m28159M(str);
    }

    /* renamed from: S */
    public void mo10S() {
        super.mo10S();
    }

    /* renamed from: T */
    public C6494any mo11T() {
        return ___iScriptClass;
    }

    /* renamed from: U */
    public C2491fm[] mo12U() {
        return _m_methods;
    }

    /* renamed from: W */
    public Object mo13W() {
        return new C6514aoS(this);
    }

    /* renamed from: a */
    public Object mo14a(C0495Gr gr) {
        Object[] args = gr.getArgs();
        switch (gr.mo2417hq() - Actor._m_methodCount) {
            case 0:
                return m28181ik();
            case 1:
                m28165a((Actor) args[0], (Vec3f) args[1], (Vec3f) args[2], ((Float) args[3]).floatValue(), (C6339akz) args[4]);
                return null;
            case 2:
                return m28170c((Space) args[0], ((Long) args[1]).longValue());
            case 3:
                m28162a(((Long) args[0]).longValue(), ((Boolean) args[1]).booleanValue());
                return null;
            case 4:
                m28168a((C4029yK) args[0]);
                return null;
            case 5:
                return new Float(m28182im());
            case 6:
                return m28183io();
            case 7:
                m28157I((String) args[0]);
                return null;
            case 8:
                return m28184iq();
            case 9:
                m28158K((String) args[0]);
                return null;
            case 10:
                m28156I(((Float) args[0]).floatValue());
                return null;
            case 11:
                return new Float(m28185is());
            case 12:
                return m28186it();
            case 13:
                m28159M((String) args[0]);
                return null;
            case 14:
                return new Boolean(m28187iv());
            case 15:
                return m28188ix();
            case 16:
                m28164a((Actor) args[0], (C5260aCm) args[1], (Vec3f) args[2], (Vec3f) args[3]);
                return null;
            case 17:
                m28171c((Actor) args[0], (C5260aCm) args[1], (Vec3f) args[2], (Vec3f) args[3]);
                return null;
            case 18:
                return m28190iz();
            case 19:
                m28172c((Vec3d) args[0]);
                return null;
            case 20:
                m28161a(((Integer) args[0]).intValue(), ((Long) args[1]).longValue(), (Collection<C6625aqZ>) (Collection) args[2], (C6705asB) args[3]);
                return null;
            case 21:
                m28173iB();
                return null;
            case 22:
                m28163a((Actor.C0200h) args[0]);
                return null;
            case 23:
                m28167a((Vec3d) args[0], (Quat4fWrap) args[1]);
                return null;
            default:
                return super.mo14a(gr);
        }
    }

    /* renamed from: a */
    public void mo15a(Collection collection, C0495Gr gr) {
        super.mo15a(collection, gr);
    }

    /* renamed from: b */
    public void mo990b(int i, long j, Collection<C6625aqZ> collection, C6705asB asb) {
        switch (bFf().mo6893i(f5980vq)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f5980vq, new Object[]{new Integer(i), new Long(j), collection, asb}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f5980vq, new Object[]{new Integer(i), new Long(j), collection, asb}));
                break;
        }
        m28161a(i, j, collection, asb);
    }

    @C5307aEh
    /* renamed from: b */
    public void mo991b(long j, boolean z) {
        switch (bFf().mo6893i(f5963uZ)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f5963uZ, new Object[]{new Long(j), new Boolean(z)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f5963uZ, new Object[]{new Long(j), new Boolean(z)}));
                break;
        }
        m28162a(j, z);
    }

    /* renamed from: b */
    public void mo620b(Actor.C0200h hVar) {
        switch (bFf().mo6893i(f5982vs)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f5982vs, new Object[]{hVar}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f5982vs, new Object[]{hVar}));
                break;
        }
        m28163a(hVar);
    }

    /* renamed from: b */
    public void mo621b(Actor cr, C5260aCm acm, Vec3f vec3f, Vec3f vec3f2) {
        switch (bFf().mo6893i(f5976vm)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f5976vm, new Object[]{cr, acm, vec3f, vec3f2}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f5976vm, new Object[]{cr, acm, vec3f, vec3f2}));
                break;
        }
        m28164a(cr, acm, vec3f, vec3f2);
    }

    @C5307aEh
    /* renamed from: b */
    public void mo992b(Actor cr, Vec3f vec3f, Vec3f vec3f2, float f, C6339akz akz) {
        switch (bFf().mo6893i(f5961uX)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f5961uX, new Object[]{cr, vec3f, vec3f2, new Float(f), akz}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f5961uX, new Object[]{cr, vec3f, vec3f2, new Float(f), akz}));
                break;
        }
        m28165a(cr, vec3f, vec3f2, f, akz);
    }

    /* access modifiers changed from: protected */
    @ClientOnly
    /* renamed from: b */
    public void mo996b(Vec3d ajr, Quat4fWrap aoy) {
        switch (bFf().mo6893i(f5983vt)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f5983vt, new Object[]{ajr, aoy}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f5983vt, new Object[]{ajr, aoy}));
                break;
        }
        m28167a(ajr, aoy);
    }

    /* renamed from: b */
    public void mo999b(C4029yK yKVar) {
        switch (bFf().mo6893i(f5964va)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f5964va, new Object[]{yKVar}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f5964va, new Object[]{yKVar}));
                break;
        }
        m28168a(yKVar);
    }

    /* renamed from: c */
    public C5663aRz[] mo25c() {
        return _m_fields;
    }

    /* access modifiers changed from: protected */
    @C1253SX
    /* renamed from: d */
    public C0520HN mo635d(Space ea, long j) {
        switch (bFf().mo6893i(f5962uY)) {
            case 0:
                return null;
            case 2:
                return (C0520HN) bFf().mo5606d(new aCE(this, f5962uY, new Object[]{ea, new Long(j)}));
            case 3:
                bFf().mo5606d(new aCE(this, f5962uY, new Object[]{ea, new Long(j)}));
                break;
        }
        return m28170c(ea, j);
    }

    /* renamed from: d */
    public void mo636d(Actor cr, C5260aCm acm, Vec3f vec3f, Vec3f vec3f2) {
        switch (bFf().mo6893i(f5977vn)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f5977vn, new Object[]{cr, acm, vec3f, vec3f2}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f5977vn, new Object[]{cr, acm, vec3f, vec3f2}));
                break;
        }
        m28171c(cr, acm, vec3f, vec3f2);
    }

    public float getSpeed() {
        switch (bFf().mo6893i(f5971vh)) {
            case 0:
                return 0.0f;
            case 2:
                return ((Float) bFf().mo5606d(new aCE(this, f5971vh, new Object[0]))).floatValue();
            case 3:
                bFf().mo5606d(new aCE(this, f5971vh, new Object[0]));
                break;
        }
        return m28185is();
    }

    public void setSpeed(float f) {
        switch (bFf().mo6893i(f5970vg)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f5970vg, new Object[]{new Float(f)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f5970vg, new Object[]{new Float(f)}));
                break;
        }
        m28156I(f);
    }

    /* renamed from: iA */
    public String mo647iA() {
        switch (bFf().mo6893i(f5978vo)) {
            case 0:
                return null;
            case 2:
                return (String) bFf().mo5606d(new aCE(this, f5978vo, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f5978vo, new Object[0]));
                break;
        }
        return m28190iz();
    }

    /* renamed from: iC */
    public void mo1076iC() {
        switch (bFf().mo6893i(f5981vr)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f5981vr, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f5981vr, new Object[0]));
                break;
        }
        m28173iB();
    }

    /* renamed from: il */
    public C5260aCm mo17516il() {
        switch (bFf().mo6893i(f5960uW)) {
            case 0:
                return null;
            case 2:
                return (C5260aCm) bFf().mo5606d(new aCE(this, f5960uW, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f5960uW, new Object[0]));
                break;
        }
        return m28181ik();
    }

    /* renamed from: in */
    public float mo17517in() {
        switch (bFf().mo6893i(f5965vb)) {
            case 0:
                return 0.0f;
            case 2:
                return ((Float) bFf().mo5606d(new aCE(this, f5965vb, new Object[0]))).floatValue();
            case 3:
                bFf().mo5606d(new aCE(this, f5965vb, new Object[0]));
                break;
        }
        return m28182im();
    }

    /* renamed from: ip */
    public String mo648ip() {
        switch (bFf().mo6893i(f5966vc)) {
            case 0:
                return null;
            case 2:
                return (String) bFf().mo5606d(new aCE(this, f5966vc, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f5966vc, new Object[0]));
                break;
        }
        return m28183io();
    }

    /* renamed from: ir */
    public String mo649ir() {
        switch (bFf().mo6893i(f5968ve)) {
            case 0:
                return null;
            case 2:
                return (String) bFf().mo5606d(new aCE(this, f5968ve, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f5968ve, new Object[0]));
                break;
        }
        return m28184iq();
    }

    /* renamed from: iu */
    public String mo651iu() {
        switch (bFf().mo6893i(f5972vi)) {
            case 0:
                return null;
            case 2:
                return (String) bFf().mo5606d(new aCE(this, f5972vi, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f5972vi, new Object[0]));
                break;
        }
        return m28186it();
    }

    /* renamed from: iw */
    public boolean mo1080iw() {
        switch (bFf().mo6893i(f5974vk)) {
            case 0:
                return false;
            case 2:
                return ((Boolean) bFf().mo5606d(new aCE(this, f5974vk, new Object[0]))).booleanValue();
            case 3:
                bFf().mo5606d(new aCE(this, f5974vk, new Object[0]));
                break;
        }
        return m28187iv();
    }

    public void setPosition(Vec3d ajr) {
        switch (bFf().mo6893i(f5979vp)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f5979vp, new Object[]{ajr}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f5979vp, new Object[]{ajr}));
                break;
        }
        m28172c(ajr);
    }

    /* renamed from: a */
    public void mo17514a(C5260aCm acm, float f) {
        super.mo10S();
        m28166a(acm);
        m28152G(f);
    }

    @C0064Am(aul = "5f7575d91c69c86064422d3c126f70b6", aum = 0)
    /* renamed from: ik */
    private C5260aCm m28181ik() {
        return m28174id();
    }

    @C0064Am(aul = "3b48c68971fd2dbe3624f4c2e758471f", aum = 0)
    @C5307aEh
    /* renamed from: a */
    private void m28165a(Actor cr, Vec3f vec3f, Vec3f vec3f2, float f, C6339akz akz) {
        super.mo992b(cr, vec3f, vec3f2, f, akz);
        Pawn cLw = cLw();
        if (cr != cLw) {
            if (cLw instanceof Ship) {
                for (Turret jq : ((Ship) cLw).agF()) {
                    if (cr == jq) {
                        return;
                    }
                }
            }
            if (bGY()) {
                if (cr.aiD() != null) {
                    cr.mo995b(cr.aiD().mo2472kQ(), (Actor) cLw, m28174id(), vec3f2, vec3f);
                } else {
                    mo6317hy("Shot collided with actor " + cr.aSp() + " but it is disposed." + " Consider removing it from local space.");
                }
            }
            mo1099zx();
            dispose();
            bFR();
        }
    }

    @C0064Am(aul = "7599770ebed7c7fc4899966ac115399d", aum = 0)
    @C1253SX
    /* renamed from: c */
    private C0520HN m28170c(Space ea, long j) {
        C1364Ts ts = new C1364Ts(ea, this, mo958IL(), m28176if());
        C1355Tk cKn = ea.cKn();
        if (cKn != null) {
            ts.mo2449a(cKn.mo5725a(j, (C2235dB) ts, 10));
        }
        return ts;
    }

    @C0064Am(aul = "6292f06f317ab8f24cb637c985dad806", aum = 0)
    @C5307aEh
    /* renamed from: a */
    private void m28162a(long j, boolean z) {
        super.mo991b(j, z);
        if (!cLZ()) {
            mo8358lY("Sending Simulation data Update for a shot that has no simulated");
        } else if (m28175ie() != null && z && getPosition().mo9486aC(m28175ie()) > m28176if() * m28176if()) {
            mo1099zx();
            bFR();
        }
    }

    @C0064Am(aul = "4bd0fb1dfa0e1cc8120f2be7f6b2a045", aum = 0)
    /* renamed from: a */
    private void m28168a(C4029yK yKVar) {
        yKVar.setDisableOnColision(false);
        yKVar.setTransparent(true);
        super.mo999b(yKVar);
    }

    @C0064Am(aul = "bc4f9b55c4a40626c26d03eb0d92575e", aum = 0)
    /* renamed from: im */
    private float m28182im() {
        return m28176if();
    }

    @C0064Am(aul = "b823556053a10423a6c8a83123bba513", aum = 0)
    /* renamed from: io */
    private String m28183io() {
        return m28177ig();
    }

    @C0064Am(aul = "b608de20bede31947e2a064c5ea15822", aum = 0)
    /* renamed from: I */
    private void m28157I(String str) {
        m28151F(str);
    }

    @C0064Am(aul = "c23add059f1bb54c8d052b63630ea0c7", aum = 0)
    /* renamed from: iq */
    private String m28184iq() {
        return m28178ih();
    }

    @C0064Am(aul = "db2404a1ee72c2f56c456b678bc73a69", aum = 0)
    /* renamed from: K */
    private void m28158K(String str) {
        m28153G(str);
    }

    @C0064Am(aul = "613b2b856c4c48dcca1ec397b7eada07", aum = 0)
    /* renamed from: I */
    private void m28156I(float f) {
        m28154H(f);
        mo1016bz(new Vec3f(0.0f, 0.0f, -f));
    }

    @C0064Am(aul = "53f10e5327cd40609478ef2825dad1ec", aum = 0)
    /* renamed from: is */
    private float m28185is() {
        return m28179ii();
    }

    @C0064Am(aul = "90d51840848d324b37cdee147a4d6ed0", aum = 0)
    /* renamed from: it */
    private String m28186it() {
        return m28180ij();
    }

    @C0064Am(aul = "0586eedb539071fb57fd66f40a279e4e", aum = 0)
    /* renamed from: M */
    private void m28159M(String str) {
        m28155H(str);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:3:0x0056, code lost:
        if (r0 < r2) goto L_0x0058;
     */
    @p001a.C0064Am(aul = "42840fef89bf561cbcecd4219adbcf42", aum = 0)
    /* renamed from: iv */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private boolean m28187iv() {
        /*
            r8 = this;
            r2 = 0
            a.HN r0 = r8.aiD()
            a.aJR r0 = r0.getPosition()
            a.aJR r1 = new a.aJR
            a.cA r4 = r8.m28189iy()
            float r4 = r4.mo17517in()
            float r4 = -r4
            double r6 = (double) r4
            r4 = r2
            r1.<init>(r2, r4, r6)
            a.aJR r2 = new a.aJR
            r2.<init>()
            a.HN r3 = r8.aiD()
            a.aoY r3 = r3.getOrientation()
            p001a.C6520aoY.m24425b((p001a.C6520aoY) r3, (p001a.aJR) r1, (p001a.aJR) r2)
            a.aJR r1 = r2.mo9504d((javax.vecmath.Tuple3d) r0)
            a.akU r2 = r8.aPC()
            a.fA r2 = r2.bQx()
            a.aJR r2 = r2.getPosition()
            double r2 = p001a.aJR.m15647e(r0, r1, r2)
            a.adE r4 = r8.ald()
            a.aBD r4 = r4.ale()
            if (r4 == 0) goto L_0x00a0
            taikodom.render.camera.Camera r4 = r4.adY()
            a.aJR r4 = r4.getPosition()
            double r0 = p001a.aJR.m15647e(r0, r1, r4)
            int r4 = (r0 > r2 ? 1 : (r0 == r2 ? 0 : -1))
            if (r4 >= 0) goto L_0x00a0
        L_0x0058:
            r2 = 4666723172467343360(0x40c3880000000000, double:10000.0)
            int r0 = (r0 > r2 ? 1 : (r0 == r2 ? 0 : -1))
            if (r0 <= 0) goto L_0x0063
            r0 = 0
        L_0x0062:
            return r0
        L_0x0063:
            boolean r0 = super.mo1080iw()
            if (r0 == 0) goto L_0x0098
            boolean r1 = r8.bHb()
            if (r1 == 0) goto L_0x0062
            a.HN r1 = r8.aiD()
            r2 = 1176256512(0x461c4000, float:10000.0)
            r1.mo2562au(r2)
            a.HN r1 = r8.aiD()
            a.cA r2 = r8.m28189iy()
            float r2 = r2.getSpeed()
            r1.mo2563av(r2)
            a.HN r1 = r8.aiD()
            a.cA r2 = r8.m28189iy()
            float r2 = r2.getSpeed()
            r1.mo2540W(r2)
            goto L_0x0062
        L_0x0098:
            org.apache.commons.logging.Log r1 = logger
            java.lang.String r2 = "Not added to simulation because Simulated was not added"
            r1.warn(r2)
            goto L_0x0062
        L_0x00a0:
            r0 = r2
            goto L_0x0058
        */
        throw new UnsupportedOperationException("Method not decompiled: p001a.C2117cA.m28187iv():boolean");
    }

    @C0064Am(aul = "2ab838bfedab76ddd1100dc391f98847", aum = 0)
    /* renamed from: ix */
    private Shot m28188ix() {
        return this;
    }

    @C0064Am(aul = "3574865d02ebe90417fe3c6d410e6478", aum = 0)
    /* renamed from: a */
    private void m28164a(Actor cr, C5260aCm acm, Vec3f vec3f, Vec3f vec3f2) {
    }

    @C0064Am(aul = "ff9c19847a5e22c695027f40d49e9072", aum = 0)
    /* renamed from: c */
    private void m28171c(Actor cr, C5260aCm acm, Vec3f vec3f, Vec3f vec3f2) {
    }

    @C0064Am(aul = "ca67968a1ab7ae820625a7a3e3cdfca6", aum = 0)
    /* renamed from: iz */
    private String m28190iz() {
        return null;
    }

    @C0064Am(aul = "f032bb80091d7d96aa8f9b8e9c1eced3", aum = 0)
    /* renamed from: c */
    private void m28172c(Vec3d ajr) {
        if (cLZ()) {
            mo978ah(ajr);
        }
        mo988aj(ajr);
    }

    @C0064Am(aul = "ba7669e5631967a59f06818dcc56241b", aum = 0)
    /* renamed from: a */
    private void m28161a(int i, long j, Collection<C6625aqZ> collection, C6705asB asb) {
    }

    @C0064Am(aul = "3cf043a8038355375a20989a99ce57af", aum = 0)
    /* renamed from: iB */
    private void m28173iB() {
    }

    @C0064Am(aul = "c3969079ae654f16293061d2d01fb557", aum = 0)
    /* renamed from: a */
    private void m28163a(Actor.C0200h hVar) {
        hVar.mo1103c(new C6348alI(1.0f));
    }

    @C0064Am(aul = "17f1fa5a9fd16d07657dada1cfe01e74", aum = 0)
    @ClientOnly
    /* renamed from: a */
    private void m28167a(Vec3d ajr, Quat4fWrap aoy) {
        this.hks.setPosition(ajr);
    }
}
