package game.script.item;

import game.network.message.externalizable.aCE;
import logic.baa.C1616Xf;
import logic.baa.C1634Xv;
import logic.baa.C5511aMd;
import logic.baa.C6485anp;
import logic.data.mbean.C1359Tn;
import logic.res.code.C5640aRc;
import logic.res.code.C5663aRz;
import logic.res.code.C6494any;
import logic.res.html.C2491fm;
import logic.thred.LogPrinter;
import org.apache.commons.logging.Log;
import p001a.*;

import java.util.Collection;

@C2712iu(mo19786Bt = BaseBulkItemType.class)
@C5511aMd
@C6485anp
/* renamed from: a.aTv  reason: case insensitive filesystem */
/* compiled from: a */
public abstract class BulkItem extends Item implements C1616Xf {
    public static final int _m_fieldCount = 0;
    public static final C5663aRz[] _m_fields = null;
    public static final int _m_methodCount = 0;
    public static final C2491fm[] _m_methods = null;
    public static final C5663aRz aGq = null;
    public static final C2491fm aGt = null;
    public static final C2491fm aGw = null;
    public static final C2491fm bHG = null;
    public static final C5663aRz fZO = null;
    public static final C2491fm gDi = null;
    public static final C2491fm iPD = null;
    public static final C2491fm iPE = null;
    public static final C2491fm iPF = null;
    public static final long serialVersionUID = 0;
    private static final Log logger = LogPrinter.setClass(BulkItem.class);
    public static C6494any ___iScriptClass;
    @C0064Am(aul = "0e91695d9dd33aec518566b172e54764", aum = 1)
    @C5256aCi

    /* renamed from: cZ */
    private static int f3837cZ;
    @C0064Am(aul = "d0146ef1868bc4ab7d96a95f7027bad8", aum = 0)
    private static int eiS;

    static {
        m18446V();
    }

    public BulkItem() {
        super((C5540aNg) null);
        super.mo10S();
    }

    public BulkItem(C5540aNg ang) {
        super(ang);
    }

    public BulkItem(BulkItemType amh) {
        super((C5540aNg) null);
        super._m_script_init(amh);
    }

    /* renamed from: V */
    static void m18446V() {
        serialVersionUID = (long) 1;
        _m_fieldCount = Item._m_fieldCount + 2;
        _m_methodCount = Item._m_methodCount + 7;
        int i = Item._m_fieldCount;
        C5663aRz[] arzArr = new C5663aRz[(i + 2)];
        C5663aRz b = C5640aRc.m17844b(BulkItem.class, "d0146ef1868bc4ab7d96a95f7027bad8", i);
        fZO = b;
        arzArr[i] = b;
        int i2 = i + 1;
        C5663aRz b2 = C5640aRc.m17844b(BulkItem.class, "0e91695d9dd33aec518566b172e54764", i2);
        aGq = b2;
        arzArr[i2] = b2;
        int i3 = i2 + 1;
        _m_fields = arzArr;
        C1634Xv.m11725a((Object[]) Item._m_fields, (Object[]) _m_fields);
        int i4 = Item._m_methodCount;
        C2491fm[] fmVarArr = new C2491fm[(i4 + 7)];
        C2491fm a = C4105zY.m41624a(BulkItem.class, "bfcc2ec01185f44cfde93573831079c4", i4);
        aGt = a;
        fmVarArr[i4] = a;
        int i5 = i4 + 1;
        C2491fm a2 = C4105zY.m41624a(BulkItem.class, "784a3fe43611deefe51013a0ef127b82", i5);
        aGw = a2;
        fmVarArr[i5] = a2;
        int i6 = i5 + 1;
        C2491fm a3 = C4105zY.m41624a(BulkItem.class, "ac1928a90095d3974e7fd00fe40df73c", i6);
        bHG = a3;
        fmVarArr[i6] = a3;
        int i7 = i6 + 1;
        C2491fm a4 = C4105zY.m41624a(BulkItem.class, "35aac76f718c4642109b7b2987fdc039", i7);
        iPD = a4;
        fmVarArr[i7] = a4;
        int i8 = i7 + 1;
        C2491fm a5 = C4105zY.m41624a(BulkItem.class, "08b5fb2aa6fc67cdde82ff21fc40da93", i8);
        iPE = a5;
        fmVarArr[i8] = a5;
        int i9 = i8 + 1;
        C2491fm a6 = C4105zY.m41624a(BulkItem.class, "493cf2ca55d60b8a14337383d4b85792", i9);
        gDi = a6;
        fmVarArr[i9] = a6;
        int i10 = i9 + 1;
        C2491fm a7 = C4105zY.m41624a(BulkItem.class, "6a574f95678a0f9bbb21f2bc47c1d676", i10);
        iPF = a7;
        fmVarArr[i10] = a7;
        int i11 = i10 + 1;
        _m_methods = fmVarArr;
        C1634Xv.m11725a((Object[]) Item._m_methods, (Object[]) _m_methods);
        ___iScriptClass = aUO.m18566a(BulkItem.class, C1359Tn.class, _m_fields, _m_methods);
    }

    @C0064Am(aul = "08b5fb2aa6fc67cdde82ff21fc40da93", aum = 0)
    @C5566aOg
    @C2499fr
    /* renamed from: AP */
    private BulkItem m18443AP(int i) {
        throw new aWi(new aCE(this, iPE, new Object[]{new Integer(i)}));
    }

    /* renamed from: PF */
    private int m18444PF() {
        return bFf().mo5608dq().mo3212n(aGq);
    }

    @C0064Am(aul = "35aac76f718c4642109b7b2987fdc039", aum = 0)
    @C5566aOg
    @C2499fr
    /* renamed from: b */
    private void m18447b(BulkItem atv) {
        throw new aWi(new aCE(this, iPD, new Object[]{atv}));
    }

    /* renamed from: cZ */
    private void m18448cZ(int i) {
        bFf().mo5608dq().mo3183b(aGq, i);
    }

    private int ciK() {
        return ((BulkItemType) getType()).mo13880dX();
    }

    @C0064Am(aul = "6a574f95678a0f9bbb21f2bc47c1d676", aum = 0)
    @C5566aOg
    /* renamed from: i */
    private boolean m18450i(ItemLocation aag) {
        throw new aWi(new aCE(this, iPF, new Object[]{aag}));
    }

    /* renamed from: sx */
    private void m18451sx(int i) {
        throw new C6039afL();
    }

    @C5566aOg
    @C2499fr
    /* renamed from: AQ */
    public BulkItem mo11560AQ(int i) {
        switch (bFf().mo6893i(iPE)) {
            case 0:
                return null;
            case 2:
                return (BulkItem) bFf().mo5606d(new aCE(this, iPE, new Object[]{new Integer(i)}));
            case 3:
                bFf().mo5606d(new aCE(this, iPE, new Object[]{new Integer(i)}));
                break;
        }
        return m18443AP(i);
    }

    /* renamed from: Iq */
    public int mo302Iq() {
        switch (bFf().mo6893i(aGt)) {
            case 0:
                return 0;
            case 2:
                return ((Integer) bFf().mo5606d(new aCE(this, aGt, new Object[0]))).intValue();
            case 3:
                bFf().mo5606d(new aCE(this, aGt, new Object[0]));
                break;
        }
        return m18445PK();
    }

    /* renamed from: S */
    public void mo10S() {
        super.mo10S();
    }

    /* renamed from: T */
    public C6494any mo11T() {
        return ___iScriptClass;
    }

    /* renamed from: U */
    public C2491fm[] mo12U() {
        return _m_methods;
    }

    /* renamed from: W */
    public Object mo13W() {
        return new C1359Tn(this);
    }

    /* renamed from: a */
    public Object mo14a(C0495Gr gr) {
        Object[] args = gr.getArgs();
        switch (gr.mo2417hq() - Item._m_methodCount) {
            case 0:
                return new Integer(m18445PK());
            case 1:
                m18449da(((Integer) args[0]).intValue());
                return null;
            case 2:
                return new Float(aoH());
            case 3:
                m18447b((BulkItem) args[0]);
                return null;
            case 4:
                return m18443AP(((Integer) args[0]).intValue());
            case 5:
                return cxk();
            case 6:
                return new Boolean(m18450i((ItemLocation) args[0]));
            default:
                return super.mo14a(gr);
        }
    }

    /* renamed from: a */
    public void mo15a(Collection collection, C0495Gr gr) {
        super.mo15a(collection, gr);
    }

    @C5566aOg
    @C2499fr
    /* renamed from: c */
    public void mo11562c(BulkItem atv) {
        switch (bFf().mo6893i(iPD)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, iPD, new Object[]{atv}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, iPD, new Object[]{atv}));
                break;
        }
        m18447b(atv);
    }

    /* renamed from: c */
    public C5663aRz[] mo25c() {
        return _m_fields;
    }

    public Item cxl() {
        switch (bFf().mo6893i(gDi)) {
            case 0:
                return null;
            case 2:
                return (Item) bFf().mo5606d(new aCE(this, gDi, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, gDi, new Object[0]));
                break;
        }
        return cxk();
    }

    /* renamed from: db */
    public void mo11564db(int i) {
        switch (bFf().mo6893i(aGw)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, aGw, new Object[]{new Integer(i)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, aGw, new Object[]{new Integer(i)}));
                break;
        }
        m18449da(i);
    }

    public final float getVolume() {
        switch (bFf().mo6893i(bHG)) {
            case 0:
                return 0.0f;
            case 2:
                return ((Float) bFf().mo5606d(new aCE(this, bHG, new Object[0]))).floatValue();
            case 3:
                bFf().mo5606d(new aCE(this, bHG, new Object[0]));
                break;
        }
        return aoH();
    }

    @C5566aOg
    /* renamed from: j */
    public boolean mo11565j(ItemLocation aag) {
        switch (bFf().mo6893i(iPF)) {
            case 0:
                return false;
            case 2:
                return ((Boolean) bFf().mo5606d(new aCE(this, iPF, new Object[]{aag}))).booleanValue();
            case 3:
                bFf().mo5606d(new aCE(this, iPF, new Object[]{aag}));
                break;
        }
        return m18450i(aag);
    }

    /* renamed from: b */
    public void mo11561b(BulkItemType amh) {
        super.mo306n(amh);
        mo11564db(1);
    }

    @C0064Am(aul = "bfcc2ec01185f44cfde93573831079c4", aum = 0)
    /* renamed from: PK */
    private int m18445PK() {
        return m18444PF();
    }

    @C0064Am(aul = "784a3fe43611deefe51013a0ef127b82", aum = 0)
    /* renamed from: da */
    private void m18449da(int i) {
        m18448cZ(i);
    }

    @C0064Am(aul = "ac1928a90095d3974e7fd00fe40df73c", aum = 0)
    private float aoH() {
        return ((BulkItemType) bAP()).mo19865HA() * ((float) mo302Iq());
    }

    @C0064Am(aul = "493cf2ca55d60b8a14337383d4b85792", aum = 0)
    private Item cxk() {
        ItemType bAP = bAP();
        for (Item next : bNh().aRz()) {
            if (bAP == next.bAP() && next != this) {
                BulkItem atv = (BulkItem) next;
                atv.mo11562c(this);
                if (isDisposed()) {
                    cxf();
                    return atv;
                }
            }
        }
        return this;
    }
}
