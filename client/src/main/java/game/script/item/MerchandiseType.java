package game.script.item;

import game.network.message.externalizable.aCE;
import game.network.message.serializable.C3438ra;
import game.script.pda.DatabaseCategory;
import game.script.pda.TaikopediaEntry;
import game.script.resource.Asset;
import logic.baa.*;
import logic.data.mbean.C7034azv;
import logic.res.code.C5640aRc;
import logic.res.code.C5663aRz;
import logic.res.code.C6494any;
import logic.res.html.C2491fm;
import p001a.*;

import java.util.Collection;
import java.util.Collections;
import java.util.List;

@C0566Hp
@C5511aMd
@C6485anp
/* renamed from: a.Mf */
/* compiled from: a */
public class MerchandiseType extends MerchandiseBaseType implements C1616Xf {
    public static final int _m_fieldCount = 0;
    public static final C5663aRz[] _m_fields = null;
    public static final int _m_methodCount = 0;
    public static final C2491fm[] _m_methods = null;
    public static final C5663aRz aMg = null;
    public static final C5663aRz aMh = null;
    public static final C5663aRz aMi = null;
    public static final C2491fm aMk = null;
    public static final C2491fm aMl = null;
    public static final C2491fm aMm = null;
    public static final C2491fm aMn = null;
    public static final C2491fm aMo = null;
    public static final C2491fm aMr = null;
    public static final C2491fm aMs = null;
    public static final C2491fm aMu = null;
    public static final C2491fm aMv = null;
    public static final C5663aRz awd = null;
    /* renamed from: dN */
    public static final C2491fm f1106dN = null;
    public static final C5663aRz dyY = null;
    public static final C2491fm dyZ = null;
    public static final C2491fm dza = null;
    public static final C2491fm dzb = null;
    public static final C2491fm dzc = null;
    public static final C2491fm dzd = null;
    public static final long serialVersionUID = 0;
    public static C6494any ___iScriptClass;
    @C0064Am(aul = "b4afdcc6187b7ff0be213d659fd8694a", aum = 3)
    private static boolean dyX;
    @C0064Am(aul = "f972c66b9579a13af0ef9809b00073fa", aum = 0)

    /* renamed from: jS */
    private static DatabaseCategory f1107jS;
    @C0064Am(aul = "462de04eca7320138c725d04397a509f", aum = 1)

    /* renamed from: jT */
    private static C3438ra<TaikopediaEntry> f1108jT;
    @C0064Am(aul = "3af022392f37b08eb8b9bf8f5ba4ed73", aum = 2)

    /* renamed from: jU */
    private static Asset f1109jU;
    @C0064Am(aul = "a352d22e13f4580021458934056314df", aum = 4)

    /* renamed from: jV */
    private static float f1110jV;

    static {
        m7092V();
    }

    public MerchandiseType() {
        super((C5540aNg) null);
        super.mo10S();
    }

    public MerchandiseType(C5540aNg ang) {
        super(ang);
    }

    /* renamed from: V */
    static void m7092V() {
        serialVersionUID = (long) 1;
        _m_fieldCount = MerchandiseBaseType._m_fieldCount + 5;
        _m_methodCount = MerchandiseBaseType._m_methodCount + 15;
        int i = MerchandiseBaseType._m_fieldCount;
        C5663aRz[] arzArr = new C5663aRz[(i + 5)];
        C5663aRz b = C5640aRc.m17844b(MerchandiseType.class, "f972c66b9579a13af0ef9809b00073fa", i);
        aMh = b;
        arzArr[i] = b;
        int i2 = i + 1;
        C5663aRz b2 = C5640aRc.m17844b(MerchandiseType.class, "462de04eca7320138c725d04397a509f", i2);
        awd = b2;
        arzArr[i2] = b2;
        int i3 = i2 + 1;
        C5663aRz b3 = C5640aRc.m17844b(MerchandiseType.class, "3af022392f37b08eb8b9bf8f5ba4ed73", i3);
        aMg = b3;
        arzArr[i3] = b3;
        int i4 = i3 + 1;
        C5663aRz b4 = C5640aRc.m17844b(MerchandiseType.class, "b4afdcc6187b7ff0be213d659fd8694a", i4);
        dyY = b4;
        arzArr[i4] = b4;
        int i5 = i4 + 1;
        C5663aRz b5 = C5640aRc.m17844b(MerchandiseType.class, "a352d22e13f4580021458934056314df", i5);
        aMi = b5;
        arzArr[i5] = b5;
        int i6 = i5 + 1;
        _m_fields = arzArr;
        C1634Xv.m11725a((Object[]) MerchandiseBaseType._m_fields, (Object[]) _m_fields);
        int i7 = MerchandiseBaseType._m_methodCount;
        C2491fm[] fmVarArr = new C2491fm[(i7 + 15)];
        C2491fm a = C4105zY.m41624a(MerchandiseType.class, "1bafb399bde115d02c038902b1f55fa5", i7);
        aMn = a;
        fmVarArr[i7] = a;
        int i8 = i7 + 1;
        C2491fm a2 = C4105zY.m41624a(MerchandiseType.class, "e5165df5fba299b48202b8a1827f5bd1", i8);
        aMo = a2;
        fmVarArr[i8] = a2;
        int i9 = i8 + 1;
        C2491fm a3 = C4105zY.m41624a(MerchandiseType.class, "3485e72b540f3e1c9ab467b3c42e76fb", i9);
        aMl = a3;
        fmVarArr[i9] = a3;
        int i10 = i9 + 1;
        C2491fm a4 = C4105zY.m41624a(MerchandiseType.class, "ce3a7c23f66e5fa83f0abf79f639bd4b", i10);
        aMm = a4;
        fmVarArr[i10] = a4;
        int i11 = i10 + 1;
        C2491fm a5 = C4105zY.m41624a(MerchandiseType.class, "9aecae34f7239d8c968c4be338e78037", i11);
        dyZ = a5;
        fmVarArr[i11] = a5;
        int i12 = i11 + 1;
        C2491fm a6 = C4105zY.m41624a(MerchandiseType.class, "21c9a86bbd1e6315d8bf4d6a0b58f4f5", i12);
        dza = a6;
        fmVarArr[i12] = a6;
        int i13 = i12 + 1;
        C2491fm a7 = C4105zY.m41624a(MerchandiseType.class, "73d2aabde0d703d7148c1abc378bbe98", i13);
        aMr = a7;
        fmVarArr[i13] = a7;
        int i14 = i13 + 1;
        C2491fm a8 = C4105zY.m41624a(MerchandiseType.class, "05ce417e45a0b88eb4fafc749a8c2e99", i14);
        aMs = a8;
        fmVarArr[i14] = a8;
        int i15 = i14 + 1;
        C2491fm a9 = C4105zY.m41624a(MerchandiseType.class, "bd5c7b52b7e2be2e8ea0c06a46122664", i15);
        dzb = a9;
        fmVarArr[i15] = a9;
        int i16 = i15 + 1;
        C2491fm a10 = C4105zY.m41624a(MerchandiseType.class, "fc7aa56b8aeba2e7a6f3ba5007295088", i16);
        dzc = a10;
        fmVarArr[i16] = a10;
        int i17 = i16 + 1;
        C2491fm a11 = C4105zY.m41624a(MerchandiseType.class, "965927a43675207bc6c44d28c550d464", i17);
        aMu = a11;
        fmVarArr[i17] = a11;
        int i18 = i17 + 1;
        C2491fm a12 = C4105zY.m41624a(MerchandiseType.class, "ca4db0d4b95e4281bc5f7cead7e497f2", i18);
        aMv = a12;
        fmVarArr[i18] = a12;
        int i19 = i18 + 1;
        C2491fm a13 = C4105zY.m41624a(MerchandiseType.class, "75fe5e27073de338aa8ff81b397ce82a", i19);
        dzd = a13;
        fmVarArr[i19] = a13;
        int i20 = i19 + 1;
        C2491fm a14 = C4105zY.m41624a(MerchandiseType.class, "b0bfdc67803252498914c4f14bdcbe27", i20);
        f1106dN = a14;
        fmVarArr[i20] = a14;
        int i21 = i20 + 1;
        C2491fm a15 = C4105zY.m41624a(MerchandiseType.class, "587a35f626805d5c319e730e0973c8c9", i21);
        aMk = a15;
        fmVarArr[i21] = a15;
        int i22 = i21 + 1;
        _m_methods = fmVarArr;
        C1634Xv.m11725a((Object[]) MerchandiseBaseType._m_methods, (Object[]) _m_methods);
        ___iScriptClass = aUO.m18566a(MerchandiseType.class, C7034azv.class, _m_fields, _m_methods);
    }

    /* renamed from: L */
    private void m7082L(Asset tCVar) {
        bFf().mo5608dq().mo3197f(aMg, tCVar);
    }

    /* renamed from: Ll */
    private C3438ra m7083Ll() {
        return (C3438ra) bFf().mo5608dq().mo3214p(awd);
    }

    /* renamed from: RQ */
    private Asset m7085RQ() {
        return (Asset) bFf().mo5608dq().mo3214p(aMg);
    }

    /* renamed from: RR */
    private DatabaseCategory m7086RR() {
        return (DatabaseCategory) bFf().mo5608dq().mo3214p(aMh);
    }

    /* renamed from: RS */
    private float m7087RS() {
        return bFf().mo5608dq().mo3211m(aMi);
    }

    @C0064Am(aul = "587a35f626805d5c319e730e0973c8c9", aum = 0)
    /* renamed from: RV */
    private List m7088RV() {
        return bgF();
    }

    /* renamed from: a */
    private void m7093a(DatabaseCategory aik) {
        bFf().mo5608dq().mo3197f(aMh, aik);
    }

    private boolean bgD() {
        return bFf().mo5608dq().mo3201h(dyY);
    }

    /* renamed from: cx */
    private void m7098cx(float f) {
        bFf().mo5608dq().mo3150a(aMi, f);
    }

    /* renamed from: da */
    private void m7100da(boolean z) {
        bFf().mo5608dq().mo3153a(dyY, z);
    }

    /* renamed from: x */
    private void m7102x(C3438ra raVar) {
        bFf().mo5608dq().mo3197f(awd, raVar);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Pda Render")
    /* renamed from: N */
    public void mo4005N(Asset tCVar) {
        switch (bFf().mo6893i(aMs)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, aMs, new Object[]{tCVar}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, aMs, new Object[]{tCVar}));
                break;
        }
        m7084M(tCVar);
    }

    /* renamed from: RW */
    public /* bridge */ /* synthetic */ List mo183RW() {
        switch (bFf().mo6893i(aMk)) {
            case 0:
                return null;
            case 2:
                return (List) bFf().mo5606d(new aCE(this, aMk, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, aMk, new Object[0]));
                break;
        }
        return m7088RV();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Database Category")
    /* renamed from: RY */
    public DatabaseCategory mo184RY() {
        switch (bFf().mo6893i(aMn)) {
            case 0:
                return null;
            case 2:
                return (DatabaseCategory) bFf().mo5606d(new aCE(this, aMn, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, aMn, new Object[0]));
                break;
        }
        return m7089RX();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Pda Render")
    /* renamed from: Se */
    public Asset mo187Se() {
        switch (bFf().mo6893i(aMr)) {
            case 0:
                return null;
            case 2:
                return (Asset) bFf().mo5606d(new aCE(this, aMr, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, aMr, new Object[0]));
                break;
        }
        return m7090Sd();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Pda M P Multiplier")
    /* renamed from: Sg */
    public float mo188Sg() {
        switch (bFf().mo6893i(aMu)) {
            case 0:
                return 0.0f;
            case 2:
                return ((Float) bFf().mo5606d(new aCE(this, aMu, new Object[0]))).floatValue();
            case 3:
                bFf().mo5606d(new aCE(this, aMu, new Object[0]));
                break;
        }
        return m7091Sf();
    }

    /* renamed from: T */
    public C6494any mo11T() {
        return ___iScriptClass;
    }

    /* renamed from: U */
    public C2491fm[] mo12U() {
        return _m_methods;
    }

    /* renamed from: W */
    public Object mo13W() {
        return new C7034azv(this);
    }

    /* renamed from: a */
    public Object mo14a(C0495Gr gr) {
        Object[] args = gr.getArgs();
        switch (gr.mo2417hq() - MerchandiseBaseType._m_methodCount) {
            case 0:
                return m7089RX();
            case 1:
                m7096b((DatabaseCategory) args[0]);
                return null;
            case 2:
                m7094a((TaikopediaEntry) args[0]);
                return null;
            case 3:
                m7097c((TaikopediaEntry) args[0]);
                return null;
            case 4:
                return bgE();
            case 5:
                return bgG();
            case 6:
                return m7090Sd();
            case 7:
                m7084M((Asset) args[0]);
                return null;
            case 8:
                return new Boolean(bgI());
            case 9:
                m7101db(((Boolean) args[0]).booleanValue());
                return null;
            case 10:
                return new Float(m7091Sf());
            case 11:
                m7099cy(((Float) args[0]).floatValue());
                return null;
            case 12:
                return bgK();
            case 13:
                return m7095aT();
            case 14:
                return m7088RV();
            default:
                return super.mo14a(gr);
        }
    }

    /* renamed from: a */
    public void mo15a(Collection collection, C0495Gr gr) {
        super.mo15a(collection, gr);
    }

    /* access modifiers changed from: protected */
    /* renamed from: aU */
    public <T extends aDJ> T mo745aU() {
        switch (bFf().mo6893i(f1106dN)) {
            case 0:
                return null;
            case 2:
                return (aDJ) bFf().mo5606d(new aCE(this, f1106dN, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f1106dN, new Object[0]));
                break;
        }
        return m7095aT();
    }

    @C3248pc(aYR = C3248pc.C3250b.ADDER, displayName = "Taikopedia Entries")
    /* renamed from: b */
    public void mo4006b(TaikopediaEntry aiz) {
        switch (bFf().mo6893i(aMl)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, aMl, new Object[]{aiz}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, aMl, new Object[]{aiz}));
                break;
        }
        m7094a(aiz);
    }

    public C3438ra<TaikopediaEntry> bgF() {
        switch (bFf().mo6893i(dyZ)) {
            case 0:
                return null;
            case 2:
                return (C3438ra) bFf().mo5606d(new aCE(this, dyZ, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, dyZ, new Object[0]));
                break;
        }
        return bgE();
    }

    @C3248pc(aYR = C3248pc.C3250b.ITERATOR, displayName = "Taikopedia Entries")
    public List<TaikopediaEntry> bgH() {
        switch (bFf().mo6893i(dza)) {
            case 0:
                return null;
            case 2:
                return (List) bFf().mo5606d(new aCE(this, dza, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, dza, new Object[0]));
                break;
        }
        return bgG();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Pda Visible")
    public boolean bgJ() {
        switch (bFf().mo6893i(dzb)) {
            case 0:
                return false;
            case 2:
                return ((Boolean) bFf().mo5606d(new aCE(this, dzb, new Object[0]))).booleanValue();
            case 3:
                bFf().mo5606d(new aCE(this, dzb, new Object[0]));
                break;
        }
        return bgI();
    }

    public Merchandise bgL() {
        switch (bFf().mo6893i(dzd)) {
            case 0:
                return null;
            case 2:
                return (Merchandise) bFf().mo5606d(new aCE(this, dzd, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, dzd, new Object[0]));
                break;
        }
        return bgK();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Database Category")
    /* renamed from: c */
    public void mo4011c(DatabaseCategory aik) {
        switch (bFf().mo6893i(aMo)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, aMo, new Object[]{aik}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, aMo, new Object[]{aik}));
                break;
        }
        m7096b(aik);
    }

    /* renamed from: c */
    public C5663aRz[] mo25c() {
        return _m_fields;
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Pda M P Multiplier")
    /* renamed from: cz */
    public void mo4012cz(float f) {
        switch (bFf().mo6893i(aMv)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, aMv, new Object[]{new Float(f)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, aMv, new Object[]{new Float(f)}));
                break;
        }
        m7099cy(f);
    }

    @C3248pc(aYR = C3248pc.C3250b.REMOVER, displayName = "Taikopedia Entries")
    /* renamed from: d */
    public void mo4013d(TaikopediaEntry aiz) {
        switch (bFf().mo6893i(aMm)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, aMm, new Object[]{aiz}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, aMm, new Object[]{aiz}));
                break;
        }
        m7097c(aiz);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Pda Visible")
    /* renamed from: dc */
    public void mo4014dc(boolean z) {
        switch (bFf().mo6893i(dzc)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, dzc, new Object[]{new Boolean(z)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, dzc, new Object[]{new Boolean(z)}));
                break;
        }
        m7101db(z);
    }

    /* renamed from: S */
    public void mo10S() {
        super.mo10S();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Database Category")
    @C0064Am(aul = "1bafb399bde115d02c038902b1f55fa5", aum = 0)
    /* renamed from: RX */
    private DatabaseCategory m7089RX() {
        return m7086RR();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Database Category")
    @C0064Am(aul = "e5165df5fba299b48202b8a1827f5bd1", aum = 0)
    /* renamed from: b */
    private void m7096b(DatabaseCategory aik) {
        m7093a(aik);
    }

    @C3248pc(aYR = C3248pc.C3250b.ADDER, displayName = "Taikopedia Entries")
    @C0064Am(aul = "3485e72b540f3e1c9ab467b3c42e76fb", aum = 0)
    /* renamed from: a */
    private void m7094a(TaikopediaEntry aiz) {
        m7083Ll().add(aiz);
    }

    @C3248pc(aYR = C3248pc.C3250b.REMOVER, displayName = "Taikopedia Entries")
    @C0064Am(aul = "ce3a7c23f66e5fa83f0abf79f639bd4b", aum = 0)
    /* renamed from: c */
    private void m7097c(TaikopediaEntry aiz) {
        m7083Ll().remove(aiz);
    }

    @C0064Am(aul = "9aecae34f7239d8c968c4be338e78037", aum = 0)
    private C3438ra<TaikopediaEntry> bgE() {
        return m7083Ll();
    }

    @C3248pc(aYR = C3248pc.C3250b.ITERATOR, displayName = "Taikopedia Entries")
    @C0064Am(aul = "21c9a86bbd1e6315d8bf4d6a0b58f4f5", aum = 0)
    private List<TaikopediaEntry> bgG() {
        return Collections.unmodifiableList(m7083Ll());
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Pda Render")
    @C0064Am(aul = "73d2aabde0d703d7148c1abc378bbe98", aum = 0)
    /* renamed from: Sd */
    private Asset m7090Sd() {
        return m7085RQ();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Pda Render")
    @C0064Am(aul = "05ce417e45a0b88eb4fafc749a8c2e99", aum = 0)
    /* renamed from: M */
    private void m7084M(Asset tCVar) {
        m7082L(tCVar);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Pda Visible")
    @C0064Am(aul = "bd5c7b52b7e2be2e8ea0c06a46122664", aum = 0)
    private boolean bgI() {
        return bgD();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Pda Visible")
    @C0064Am(aul = "fc7aa56b8aeba2e7a6f3ba5007295088", aum = 0)
    /* renamed from: db */
    private void m7101db(boolean z) {
        m7100da(z);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Pda M P Multiplier")
    @C0064Am(aul = "965927a43675207bc6c44d28c550d464", aum = 0)
    /* renamed from: Sf */
    private float m7091Sf() {
        return m7087RS();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Pda M P Multiplier")
    @C0064Am(aul = "ca4db0d4b95e4281bc5f7cead7e497f2", aum = 0)
    /* renamed from: cy */
    private void m7099cy(float f) {
        m7098cx(f);
    }

    @C0064Am(aul = "75fe5e27073de338aa8ff81b397ce82a", aum = 0)
    private Merchandise bgK() {
        return (Merchandise) mo745aU();
    }

    @C0064Am(aul = "b0bfdc67803252498914c4f14bdcbe27", aum = 0)
    /* renamed from: aT */
    private <T extends aDJ> T m7095aT() {
        T t = (Merchandise) bFf().mo6865M(Merchandise.class);
        t.mo11606e(this);
        return t;
    }
}
