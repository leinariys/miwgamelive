package game.script.item;

import game.network.message.externalizable.aCE;
import game.network.message.serializable.C1556Wo;
import game.script.damage.DamageType;
import game.script.resource.Asset;
import logic.baa.*;
import logic.data.mbean.C4022yD;
import logic.res.code.C5640aRc;
import logic.res.code.C5663aRz;
import logic.res.code.C6494any;
import logic.res.html.C2491fm;
import p001a.*;

import java.util.Collection;
import java.util.Map;

@C5829abJ("2.1.1")
@C6485anp
@C2712iu(mo19785Bs = {C6251ajP.class}, mo19786Bt = BaseClipType.class, version = "2.1.1")
@C5511aMd
/* renamed from: a.Vo */
/* compiled from: a */
public class Clip extends BulkItem implements C1616Xf {
    /* renamed from: LR */
    public static final C5663aRz f1912LR = null;
    public static final int _m_fieldCount = 0;
    public static final C5663aRz[] _m_fields = null;
    public static final int _m_methodCount = 0;
    public static final C2491fm[] _m_methods = null;
    public static final C5663aRz efi = null;
    public static final C2491fm est = null;
    public static final C2491fm esu = null;
    public static final C2491fm esv = null;
    public static final C2491fm esw = null;
    public static final long serialVersionUID = 0;
    /* renamed from: uK */
    public static final C5663aRz f1913uK = null;
    /* renamed from: uU */
    public static final C5663aRz f1915uU = null;
    public static C6494any ___iScriptClass;
    @C0064Am(aul = "881df08b80f055100a20b82aeb462df6", aum = 0)

    /* renamed from: LQ */
    private static Asset f1911LQ;
    @C0064Am(aul = "aeddc3a81f653633176039a8d63298f7", aum = 2)
    private static int atB;
    @C0064Am(aul = "ffacce78cc87785c9c709c4562de3808", aum = 1)
    private static C1556Wo<DamageType, Float> bIv;
    @C0064Am(aul = "aa9f699ef05f7b38691f00e8650664be", aum = 3)

    /* renamed from: uT */
    private static String f1914uT;

    static {
        m10880V();
    }

    public Clip() {
        super((C5540aNg) null);
        super.mo10S();
    }

    public Clip(C5540aNg ang) {
        super(ang);
    }

    public Clip(ClipType aqf) {
        super((C5540aNg) null);
        super._m_script_init(aqf);
    }

    /* renamed from: V */
    static void m10880V() {
        serialVersionUID = (long) 1;
        _m_fieldCount = BulkItem._m_fieldCount + 4;
        _m_methodCount = BulkItem._m_methodCount + 4;
        int i = BulkItem._m_fieldCount;
        C5663aRz[] arzArr = new C5663aRz[(i + 4)];
        C5663aRz b = C5640aRc.m17844b(Clip.class, "881df08b80f055100a20b82aeb462df6", i);
        f1912LR = b;
        arzArr[i] = b;
        int i2 = i + 1;
        C5663aRz b2 = C5640aRc.m17844b(Clip.class, "ffacce78cc87785c9c709c4562de3808", i2);
        f1913uK = b2;
        arzArr[i2] = b2;
        int i3 = i2 + 1;
        C5663aRz b3 = C5640aRc.m17844b(Clip.class, "aeddc3a81f653633176039a8d63298f7", i3);
        efi = b3;
        arzArr[i3] = b3;
        int i4 = i3 + 1;
        C5663aRz b4 = C5640aRc.m17844b(Clip.class, "aa9f699ef05f7b38691f00e8650664be", i4);
        f1915uU = b4;
        arzArr[i4] = b4;
        int i5 = i4 + 1;
        _m_fields = arzArr;
        C1634Xv.m11725a((Object[]) BulkItem._m_fields, (Object[]) _m_fields);
        int i6 = BulkItem._m_methodCount;
        C2491fm[] fmVarArr = new C2491fm[(i6 + 4)];
        C2491fm a = C4105zY.m41624a(Clip.class, "d97f65731199fa36f085e607775006fb", i6);
        est = a;
        fmVarArr[i6] = a;
        int i7 = i6 + 1;
        C2491fm a2 = C4105zY.m41624a(Clip.class, "07a9368760d51eb482f0f951b61b9d8c", i7);
        esu = a2;
        fmVarArr[i7] = a2;
        int i8 = i7 + 1;
        C2491fm a3 = C4105zY.m41624a(Clip.class, "ccf8f4d03417a0a42fb204661b2792c6", i8);
        esv = a3;
        fmVarArr[i8] = a3;
        int i9 = i8 + 1;
        C2491fm a4 = C4105zY.m41624a(Clip.class, "a0bfa0fea72ff6d5d9945c37703c333d", i9);
        esw = a4;
        fmVarArr[i9] = a4;
        int i10 = i9 + 1;
        _m_methods = fmVarArr;
        C1634Xv.m11725a((Object[]) BulkItem._m_methods, (Object[]) _m_methods);
        ___iScriptClass = aUO.m18566a(Clip.class, C4022yD.class, _m_fields, _m_methods);
    }

    /* renamed from: H */
    private void m10878H(String str) {
        throw new C6039afL();
    }

    /* renamed from: S */
    private void m10879S(C1556Wo wo) {
        throw new C6039afL();
    }

    private C1556Wo bAH() {
        return ((ClipType) getType()).bpA();
    }

    @C0064Am(aul = "a0bfa0fea72ff6d5d9945c37703c333d", aum = 0)
    private ItemType bAO() {
        return bAJ();
    }

    private int buf() {
        return ((ClipType) getType()).bun();
    }

    /* renamed from: ij */
    private String m10881ij() {
        return ((ClipType) getType()).mo11042iu();
    }

    /* renamed from: nl */
    private void m10882nl(int i) {
        throw new C6039afL();
    }

    /* renamed from: r */
    private void m10883r(Asset tCVar) {
        throw new C6039afL();
    }

    /* renamed from: rh */
    private Asset m10884rh() {
        return ((ClipType) getType()).mo3521Nu();
    }

    /* renamed from: S */
    public void mo10S() {
        super.mo10S();
    }

    /* renamed from: T */
    public C6494any mo11T() {
        return ___iScriptClass;
    }

    /* renamed from: U */
    public C2491fm[] mo12U() {
        return _m_methods;
    }

    /* renamed from: W */
    public Object mo13W() {
        return new C4022yD(this);
    }

    /* renamed from: a */
    public Object mo14a(C0495Gr gr) {
        gr.getArgs();
        switch (gr.mo2417hq() - BulkItem._m_methodCount) {
            case 0:
                return bAI();
            case 1:
                return bAK();
            case 2:
                return new Boolean(bAM());
            case 3:
                return bAO();
            default:
                return super.mo14a(gr);
        }
    }

    /* renamed from: a */
    public void mo15a(Collection collection, C0495Gr gr) {
        super.mo15a(collection, gr);
    }

    public ClipType bAJ() {
        switch (bFf().mo6893i(est)) {
            case 0:
                return null;
            case 2:
                return (ClipType) bFf().mo5606d(new aCE(this, est, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, est, new Object[0]));
                break;
        }
        return bAI();
    }

    public Map<DamageType, Float> bAL() {
        switch (bFf().mo6893i(esu)) {
            case 0:
                return null;
            case 2:
                return (Map) bFf().mo5606d(new aCE(this, esu, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, esu, new Object[0]));
                break;
        }
        return bAK();
    }

    public boolean bAN() {
        switch (bFf().mo6893i(esv)) {
            case 0:
                return false;
            case 2:
                return ((Boolean) bFf().mo5606d(new aCE(this, esv, new Object[0]))).booleanValue();
            case 3:
                bFf().mo5606d(new aCE(this, esv, new Object[0]));
                break;
        }
        return bAM();
    }

    public /* bridge */ /* synthetic */ ItemType bAP() {
        switch (bFf().mo6893i(esw)) {
            case 0:
                return null;
            case 2:
                return (ItemType) bFf().mo5606d(new aCE(this, esw, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, esw, new Object[0]));
                break;
        }
        return bAO();
    }

    /* renamed from: c */
    public C5663aRz[] mo25c() {
        return _m_fields;
    }

    /* renamed from: l */
    public void mo6422l(ClipType aqf) {
        super.mo11561b((BulkItemType) aqf);
        mo11564db(aqf.mo13880dX());
    }

    @C0064Am(aul = "d97f65731199fa36f085e607775006fb", aum = 0)
    private ClipType bAI() {
        return (ClipType) super.bAP();
    }

    @C0064Am(aul = "07a9368760d51eb482f0f951b61b9d8c", aum = 0)
    private Map<DamageType, Float> bAK() {
        return bAH();
    }

    @C0064Am(aul = "ccf8f4d03417a0a42fb204661b2792c6", aum = 0)
    private boolean bAM() {
        return mo302Iq() > 0;
    }
}
