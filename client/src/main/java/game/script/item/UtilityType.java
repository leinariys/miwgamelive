package game.script.item;

import game.network.message.externalizable.aCE;
import logic.baa.*;
import logic.data.mbean.aCV;
import logic.res.code.C5663aRz;
import logic.res.code.C6494any;
import logic.res.html.C2491fm;
import p001a.*;

import java.util.Collection;

@C0566Hp
@C5829abJ("2.0.1")
@C6485anp
@C5511aMd
/* renamed from: a.acL  reason: case insensitive filesystem */
/* compiled from: a */
public class UtilityType extends ComponentType implements C1616Xf {
    public static final int _m_fieldCount = 0;
    public static final C5663aRz[] _m_fields = null;
    public static final int _m_methodCount = 0;
    public static final C2491fm[] _m_methods = null;
    /* renamed from: dN */
    public static final C2491fm f4216dN = null;
    public static final C2491fm feY = null;
    public static final long serialVersionUID = 0;
    public static C6494any ___iScriptClass;

    static {
        m20319V();
    }

    public UtilityType() {
        super((C5540aNg) null);
        super.mo10S();
    }

    public UtilityType(C5540aNg ang) {
        super(ang);
    }

    /* renamed from: V */
    static void m20319V() {
        serialVersionUID = (long) 1;
        _m_fieldCount = ComponentType._m_fieldCount + 0;
        _m_methodCount = ComponentType._m_methodCount + 2;
        _m_fields = new C5663aRz[(ComponentType._m_fieldCount + 0)];
        C1634Xv.m11725a((Object[]) ComponentType._m_fields, (Object[]) _m_fields);
        int i = ComponentType._m_methodCount;
        C2491fm[] fmVarArr = new C2491fm[(i + 2)];
        C2491fm a = C4105zY.m41624a(UtilityType.class, "e279010c132ceb9a1c6a53ebde670f28", i);
        feY = a;
        fmVarArr[i] = a;
        int i2 = i + 1;
        C2491fm a2 = C4105zY.m41624a(UtilityType.class, "be54ddcb56b58c86db5427c3c1bc3371", i2);
        f4216dN = a2;
        fmVarArr[i2] = a2;
        int i3 = i2 + 1;
        _m_methods = fmVarArr;
        C1634Xv.m11725a((Object[]) ComponentType._m_methods, (Object[]) _m_methods);
        ___iScriptClass = aUO.m18566a(UtilityType.class, aCV.class, _m_fields, _m_methods);
    }

    /* renamed from: T */
    public C6494any mo11T() {
        return ___iScriptClass;
    }

    /* renamed from: U */
    public C2491fm[] mo12U() {
        return _m_methods;
    }

    /* renamed from: W */
    public Object mo13W() {
        return new aCV(this);
    }

    /* renamed from: a */
    public Object mo14a(C0495Gr gr) {
        gr.getArgs();
        switch (gr.mo2417hq() - ComponentType._m_methodCount) {
            case 0:
                return bPW();
            case 1:
                return m20320aT();
            default:
                return super.mo14a(gr);
        }
    }

    /* renamed from: a */
    public void mo15a(Collection collection, C0495Gr gr) {
        super.mo15a(collection, gr);
    }

    /* access modifiers changed from: protected */
    /* renamed from: aU */
    public <T extends aDJ> T mo745aU() {
        switch (bFf().mo6893i(f4216dN)) {
            case 0:
                return null;
            case 2:
                return (aDJ) bFf().mo5606d(new aCE(this, f4216dN, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f4216dN, new Object[0]));
                break;
        }
        return m20320aT();
    }

    public Utility bPX() {
        switch (bFf().mo6893i(feY)) {
            case 0:
                return null;
            case 2:
                return (Utility) bFf().mo5606d(new aCE(this, feY, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, feY, new Object[0]));
                break;
        }
        return bPW();
    }

    /* renamed from: c */
    public C5663aRz[] mo25c() {
        return _m_fields;
    }

    /* renamed from: S */
    public void mo10S() {
        super.mo10S();
    }

    @C0064Am(aul = "e279010c132ceb9a1c6a53ebde670f28", aum = 0)
    private Utility bPW() {
        return (Utility) mo745aU();
    }

    @C0064Am(aul = "be54ddcb56b58c86db5427c3c1bc3371", aum = 0)
    /* renamed from: aT */
    private <T extends aDJ> T m20320aT() {
        T t = (Utility) bFf().mo6865M(Utility.class);
        t.mo15636a(this);
        return t;
    }
}
