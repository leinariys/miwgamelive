package game.script.item;

import game.network.message.externalizable.aCE;
import logic.baa.*;
import logic.data.mbean.C6182ahy;
import logic.res.code.C5640aRc;
import logic.res.code.C5663aRz;
import logic.res.code.C6494any;
import logic.res.html.C2491fm;
import p001a.*;

import java.util.Collection;

@C0566Hp
@C5511aMd
@C6485anp
/* renamed from: a.amh  reason: case insensitive filesystem */
/* compiled from: a */
public abstract class BulkItemType extends BaseBulkItemType implements C1616Xf {
    public static final int _m_fieldCount = 0;
    public static final C5663aRz[] _m_fields = null;
    public static final int _m_methodCount = 0;
    public static final C2491fm[] _m_methods = null;
    public static final C5663aRz fZO = null;
    public static final C2491fm fZP = null;
    /* renamed from: hR */
    public static final C2491fm f4924hR = null;
    public static final long serialVersionUID = 0;
    public static C6494any ___iScriptClass;
    @C0064Am(aul = "26624646325ffa7cd2c7daab9a073a11", aum = 0)
    private static int eiS;

    static {
        m23963V();
    }

    public BulkItemType() {
        super((C5540aNg) null);
        super.mo10S();
    }

    public BulkItemType(C5540aNg ang) {
        super(ang);
    }

    /* renamed from: V */
    static void m23963V() {
        serialVersionUID = (long) 1;
        _m_fieldCount = BaseBulkItemType._m_fieldCount + 1;
        _m_methodCount = BaseBulkItemType._m_methodCount + 2;
        int i = BaseBulkItemType._m_fieldCount;
        C5663aRz[] arzArr = new C5663aRz[(i + 1)];
        C5663aRz b = C5640aRc.m17844b(BulkItemType.class, "26624646325ffa7cd2c7daab9a073a11", i);
        fZO = b;
        arzArr[i] = b;
        int i2 = i + 1;
        _m_fields = arzArr;
        C1634Xv.m11725a((Object[]) BaseBulkItemType._m_fields, (Object[]) _m_fields);
        int i3 = BaseBulkItemType._m_methodCount;
        C2491fm[] fmVarArr = new C2491fm[(i3 + 2)];
        C2491fm a = C4105zY.m41624a(BulkItemType.class, "22f2e3f1bac05f2bc186a1f0057ed0de", i3);
        f4924hR = a;
        fmVarArr[i3] = a;
        int i4 = i3 + 1;
        C2491fm a2 = C4105zY.m41624a(BulkItemType.class, "8da1e62e8665e974602ee882e3e3160b", i4);
        fZP = a2;
        fmVarArr[i4] = a2;
        int i5 = i4 + 1;
        _m_methods = fmVarArr;
        C1634Xv.m11725a((Object[]) BaseBulkItemType._m_methods, (Object[]) _m_methods);
        ___iScriptClass = aUO.m18566a(BulkItemType.class, C6182ahy.class, _m_fields, _m_methods);
    }

    private int ciK() {
        return bFf().mo5608dq().mo3212n(fZO);
    }

    /* renamed from: sx */
    private void m23965sx(int i) {
        bFf().mo5608dq().mo3183b(fZO, i);
    }

    /* renamed from: T */
    public C6494any mo11T() {
        return ___iScriptClass;
    }

    /* renamed from: U */
    public C2491fm[] mo12U() {
        return _m_methods;
    }

    /* renamed from: W */
    public Object mo13W() {
        return new C6182ahy(this);
    }

    /* renamed from: a */
    public Object mo14a(C0495Gr gr) {
        Object[] args = gr.getArgs();
        switch (gr.mo2417hq() - BaseBulkItemType._m_methodCount) {
            case 0:
                return new Integer(m23964dW());
            case 1:
                m23966sy(((Integer) args[0]).intValue());
                return null;
            default:
                return super.mo14a(gr);
        }
    }

    /* renamed from: a */
    public void mo15a(Collection collection, C0495Gr gr) {
        super.mo15a(collection, gr);
    }

    /* renamed from: c */
    public C5663aRz[] mo25c() {
        return _m_fields;
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Max Amount")
    /* renamed from: dX */
    public int mo13880dX() {
        switch (bFf().mo6893i(f4924hR)) {
            case 0:
                return 0;
            case 2:
                return ((Integer) bFf().mo5606d(new aCE(this, f4924hR, new Object[0]))).intValue();
            case 3:
                bFf().mo5606d(new aCE(this, f4924hR, new Object[0]));
                break;
        }
        return m23964dW();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Max Amount")
    /* renamed from: sz */
    public void mo14878sz(int i) {
        switch (bFf().mo6893i(fZP)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, fZP, new Object[]{new Integer(i)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, fZP, new Object[]{new Integer(i)}));
                break;
        }
        m23966sy(i);
    }

    /* renamed from: S */
    public void mo10S() {
        super.mo10S();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Max Amount")
    @C0064Am(aul = "22f2e3f1bac05f2bc186a1f0057ed0de", aum = 0)
    /* renamed from: dW */
    private int m23964dW() {
        return ciK();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Max Amount")
    @C0064Am(aul = "8da1e62e8665e974602ee882e3e3160b", aum = 0)
    /* renamed from: sy */
    private void m23966sy(int i) {
        m23965sx(i);
    }
}
