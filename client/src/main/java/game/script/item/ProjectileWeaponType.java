package game.script.item;

import game.network.message.externalizable.aCE;
import logic.baa.*;
import logic.data.mbean.C1353Ti;
import logic.res.code.C5640aRc;
import logic.res.code.C5663aRz;
import logic.res.code.C6494any;
import logic.res.html.C2491fm;
import p001a.*;

import java.util.Collection;

@C0566Hp
@C5829abJ("2.1.1")
@C6485anp
@C5511aMd
/* renamed from: a.aGj  reason: case insensitive filesystem */
/* compiled from: a */
public class ProjectileWeaponType extends WeaponType implements C1616Xf {
    public static final int _m_fieldCount = 0;
    public static final C5663aRz[] _m_fields = null;
    public static final int _m_methodCount = 0;
    public static final C2491fm[] _m_methods = null;
    /* renamed from: dN */
    public static final C2491fm f2876dN = null;
    public static final C5663aRz efg = null;
    public static final C5663aRz efh = null;
    public static final C5663aRz efi = null;
    public static final C2491fm eft = null;
    public static final C2491fm hOX = null;
    public static final C2491fm hOY = null;
    public static final C2491fm hOZ = null;
    public static final C2491fm hPa = null;
    public static final C2491fm hPb = null;
    public static final C2491fm hPc = null;
    public static final long serialVersionUID = 0;
    public static C6494any ___iScriptClass;
    @C0064Am(aul = "5c740806a41dbd3f96728c0c9117ff18", aum = 1)
    private static ClipType atA;
    @C0064Am(aul = "213a6c0ab6aca4bb021383ca941231da", aum = 2)
    private static int atB;
    @C0064Am(aul = "d022dcf28047f08d93dbca448bc6c38f", aum = 0)
    private static int atz;

    static {
        m14950V();
    }

    public ProjectileWeaponType() {
        super((C5540aNg) null);
        super.mo10S();
    }

    public ProjectileWeaponType(C5540aNg ang) {
        super(ang);
    }

    /* renamed from: V */
    static void m14950V() {
        serialVersionUID = (long) 1;
        _m_fieldCount = WeaponType._m_fieldCount + 3;
        _m_methodCount = WeaponType._m_methodCount + 8;
        int i = WeaponType._m_fieldCount;
        C5663aRz[] arzArr = new C5663aRz[(i + 3)];
        C5663aRz b = C5640aRc.m17844b(ProjectileWeaponType.class, "d022dcf28047f08d93dbca448bc6c38f", i);
        efg = b;
        arzArr[i] = b;
        int i2 = i + 1;
        C5663aRz b2 = C5640aRc.m17844b(ProjectileWeaponType.class, "5c740806a41dbd3f96728c0c9117ff18", i2);
        efh = b2;
        arzArr[i2] = b2;
        int i3 = i2 + 1;
        C5663aRz b3 = C5640aRc.m17844b(ProjectileWeaponType.class, "213a6c0ab6aca4bb021383ca941231da", i3);
        efi = b3;
        arzArr[i3] = b3;
        int i4 = i3 + 1;
        _m_fields = arzArr;
        C1634Xv.m11725a((Object[]) WeaponType._m_fields, (Object[]) _m_fields);
        int i5 = WeaponType._m_methodCount;
        C2491fm[] fmVarArr = new C2491fm[(i5 + 8)];
        C2491fm a = C4105zY.m41624a(ProjectileWeaponType.class, "8a003d2037775f4f21064494beca8da1", i5);
        hOX = a;
        fmVarArr[i5] = a;
        int i6 = i5 + 1;
        C2491fm a2 = C4105zY.m41624a(ProjectileWeaponType.class, "2cf3c9fe127b079caeef363a4a43149e", i6);
        hOY = a2;
        fmVarArr[i6] = a2;
        int i7 = i6 + 1;
        C2491fm a3 = C4105zY.m41624a(ProjectileWeaponType.class, "5a02a55cb6fec26987deb858de3d216b", i7);
        hOZ = a3;
        fmVarArr[i7] = a3;
        int i8 = i7 + 1;
        C2491fm a4 = C4105zY.m41624a(ProjectileWeaponType.class, "87f45be8bf6e668d26331dab77a28977", i8);
        hPa = a4;
        fmVarArr[i8] = a4;
        int i9 = i8 + 1;
        C2491fm a5 = C4105zY.m41624a(ProjectileWeaponType.class, "4708527acaf4f1a6d2cfc8ea84ac973f", i9);
        eft = a5;
        fmVarArr[i9] = a5;
        int i10 = i9 + 1;
        C2491fm a6 = C4105zY.m41624a(ProjectileWeaponType.class, "0c5ed6a158136d4cec9270bb457fb2b9", i10);
        hPb = a6;
        fmVarArr[i10] = a6;
        int i11 = i10 + 1;
        C2491fm a7 = C4105zY.m41624a(ProjectileWeaponType.class, "1faf7e8df5595e2ac6decc2afacac834", i11);
        hPc = a7;
        fmVarArr[i11] = a7;
        int i12 = i11 + 1;
        C2491fm a8 = C4105zY.m41624a(ProjectileWeaponType.class, "ab9c6f076a02c19761703a3e2715726d", i12);
        f2876dN = a8;
        fmVarArr[i12] = a8;
        int i13 = i12 + 1;
        _m_methods = fmVarArr;
        C1634Xv.m11725a((Object[]) WeaponType._m_methods, (Object[]) _m_methods);
        ___iScriptClass = aUO.m18566a(ProjectileWeaponType.class, C1353Ti.class, _m_fields, _m_methods);
    }

    private int bud() {
        return bFf().mo5608dq().mo3212n(efg);
    }

    private ClipType bue() {
        return (ClipType) bFf().mo5608dq().mo3214p(efh);
    }

    private int buf() {
        return bFf().mo5608dq().mo3212n(efi);
    }

    /* renamed from: i */
    private void m14952i(ClipType aqf) {
        bFf().mo5608dq().mo3197f(efh, aqf);
    }

    /* renamed from: nk */
    private void m14954nk(int i) {
        bFf().mo5608dq().mo3183b(efg, i);
    }

    /* renamed from: nl */
    private void m14955nl(int i) {
        bFf().mo5608dq().mo3183b(efi, i);
    }

    /* renamed from: T */
    public C6494any mo11T() {
        return ___iScriptClass;
    }

    /* renamed from: U */
    public C2491fm[] mo12U() {
        return _m_methods;
    }

    /* renamed from: W */
    public Object mo13W() {
        return new C1353Ti(this);
    }

    /* renamed from: a */
    public Object mo14a(C0495Gr gr) {
        Object[] args = gr.getArgs();
        switch (gr.mo2417hq() - WeaponType._m_methodCount) {
            case 0:
                return new Integer(cZW());
            case 1:
                m14956xV(((Integer) args[0]).intValue());
                return null;
            case 2:
                return cZY();
            case 3:
                m14953n((ClipType) args[0]);
                return null;
            case 4:
                return new Integer(bum());
            case 5:
                m14957xX(((Integer) args[0]).intValue());
                return null;
            case 6:
                return daa();
            case 7:
                return m14951aT();
            default:
                return super.mo14a(gr);
        }
    }

    /* renamed from: a */
    public void mo15a(Collection collection, C0495Gr gr) {
        super.mo15a(collection, gr);
    }

    /* access modifiers changed from: protected */
    /* renamed from: aU */
    public <T extends aDJ> T mo745aU() {
        switch (bFf().mo6893i(f2876dN)) {
            case 0:
                return null;
            case 2:
                return (aDJ) bFf().mo5606d(new aCE(this, f2876dN, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f2876dN, new Object[0]));
                break;
        }
        return m14951aT();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Caliber")
    public int bun() {
        switch (bFf().mo6893i(eft)) {
            case 0:
                return 0;
            case 2:
                return ((Integer) bFf().mo5606d(new aCE(this, eft, new Object[0]))).intValue();
            case 3:
                bFf().mo5606d(new aCE(this, eft, new Object[0]));
                break;
        }
        return bum();
    }

    /* renamed from: c */
    public C5663aRz[] mo25c() {
        return _m_fields;
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Max Ammo")
    public int cZX() {
        switch (bFf().mo6893i(hOX)) {
            case 0:
                return 0;
            case 2:
                return ((Integer) bFf().mo5606d(new aCE(this, hOX, new Object[0]))).intValue();
            case 3:
                bFf().mo5606d(new aCE(this, hOX, new Object[0]));
                break;
        }
        return cZW();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Default Ammo")
    public ClipType cZZ() {
        switch (bFf().mo6893i(hOZ)) {
            case 0:
                return null;
            case 2:
                return (ClipType) bFf().mo5606d(new aCE(this, hOZ, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, hOZ, new Object[0]));
                break;
        }
        return cZY();
    }

    public ProjectileWeapon dab() {
        switch (bFf().mo6893i(hPc)) {
            case 0:
                return null;
            case 2:
                return (ProjectileWeapon) bFf().mo5606d(new aCE(this, hPc, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, hPc, new Object[0]));
                break;
        }
        return daa();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Default Ammo")
    /* renamed from: o */
    public void mo9111o(ClipType aqf) {
        switch (bFf().mo6893i(hPa)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, hPa, new Object[]{aqf}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, hPa, new Object[]{aqf}));
                break;
        }
        m14953n(aqf);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Max Ammo")
    /* renamed from: xW */
    public void mo9112xW(int i) {
        switch (bFf().mo6893i(hOY)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, hOY, new Object[]{new Integer(i)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, hOY, new Object[]{new Integer(i)}));
                break;
        }
        m14956xV(i);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Caliber")
    /* renamed from: xY */
    public void mo9113xY(int i) {
        switch (bFf().mo6893i(hPb)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, hPb, new Object[]{new Integer(i)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, hPb, new Object[]{new Integer(i)}));
                break;
        }
        m14957xX(i);
    }

    /* renamed from: S */
    public void mo10S() {
        super.mo10S();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Max Ammo")
    @C0064Am(aul = "8a003d2037775f4f21064494beca8da1", aum = 0)
    private int cZW() {
        return bud();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Max Ammo")
    @C0064Am(aul = "2cf3c9fe127b079caeef363a4a43149e", aum = 0)
    /* renamed from: xV */
    private void m14956xV(int i) {
        m14954nk(i);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Default Ammo")
    @C0064Am(aul = "5a02a55cb6fec26987deb858de3d216b", aum = 0)
    private ClipType cZY() {
        return bue();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Default Ammo")
    @C0064Am(aul = "87f45be8bf6e668d26331dab77a28977", aum = 0)
    /* renamed from: n */
    private void m14953n(ClipType aqf) {
        m14952i(aqf);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Caliber")
    @C0064Am(aul = "4708527acaf4f1a6d2cfc8ea84ac973f", aum = 0)
    private int bum() {
        return buf();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Caliber")
    @C0064Am(aul = "0c5ed6a158136d4cec9270bb457fb2b9", aum = 0)
    /* renamed from: xX */
    private void m14957xX(int i) {
        m14955nl(i);
    }

    @C0064Am(aul = "1faf7e8df5595e2ac6decc2afacac834", aum = 0)
    private ProjectileWeapon daa() {
        return (ProjectileWeapon) mo745aU();
    }

    @C0064Am(aul = "ab9c6f076a02c19761703a3e2715726d", aum = 0)
    /* renamed from: aT */
    private <T extends aDJ> T m14951aT() {
        T t = (ProjectileWeapon) bFf().mo6865M(ProjectileWeapon.class);
        t.mo5354a(this);
        return t;
    }
}
