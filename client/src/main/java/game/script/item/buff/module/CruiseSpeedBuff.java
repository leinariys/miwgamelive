package game.script.item.buff.module;

import game.network.message.externalizable.aCE;
import game.script.ship.CruiseSpeedAdapter;
import logic.baa.C1616Xf;
import logic.baa.C1634Xv;
import logic.baa.C5511aMd;
import logic.baa.C6485anp;
import logic.data.mbean.C5591aPf;
import logic.data.mbean.C5701aTl;
import logic.res.code.C5640aRc;
import logic.res.code.C5663aRz;
import logic.res.code.C6494any;
import logic.res.html.C2491fm;
import p001a.*;

import java.util.Collection;

@C5829abJ("1.1.2")
@C6485anp
@C2712iu(version = "1.1.2")
@C5511aMd
/* renamed from: a.aPo  reason: case insensitive filesystem */
/* compiled from: a */
public class CruiseSpeedBuff extends AttributeBuff implements C1616Xf {
    /* renamed from: Vt */
    public static final C5663aRz f3548Vt = null;
    /* renamed from: Vv */
    public static final C5663aRz f3550Vv = null;
    /* renamed from: Vx */
    public static final C5663aRz f3552Vx = null;
    /* renamed from: Vz */
    public static final C5663aRz f3554Vz = null;
    public static final int _m_fieldCount = 0;
    public static final C5663aRz[] _m_fields = null;
    public static final int _m_methodCount = 0;
    public static final C2491fm[] _m_methods = null;
    public static final C2491fm aPH = null;
    public static final C2491fm aPI = null;
    public static final C5663aRz aYe = null;
    public static final C2491fm aYg = null;
    public static final C2491fm aYh = null;
    public static final C2491fm aYi = null;
    public static final C2491fm aYj = null;
    public static final C5663aRz atQ = null;
    public static final C5663aRz atT = null;
    public static final C2491fm aua = null;
    public static final C2491fm aub = null;
    public static final C5663aRz bEm = null;
    public static final C2491fm jaZ = null;
    public static final long serialVersionUID = 0;
    public static C6494any ___iScriptClass;
    @C0064Am(aul = "a447b11b400d1e77774ccc9c9075c43e", aum = 0)

    /* renamed from: Vs */
    private static C3892wO f3547Vs;
    @C0064Am(aul = "b0e587372d4b73e5bf26a10b9a61253b", aum = 1)

    /* renamed from: Vu */
    private static C3892wO f3549Vu;
    @C0064Am(aul = "97021fceb0d3f8c5f683fda91c875202", aum = 2)

    /* renamed from: Vw */
    private static C3892wO f3551Vw;
    @C0064Am(aul = "02748d4dd5dea9a7c1f8bd5639e2af4a", aum = 3)

    /* renamed from: Vy */
    private static C3892wO f3553Vy;
    @C0064Am(aul = "10421d1a2383fea796e72c7783897ebf", aum = 4)

    /* renamed from: Yu */
    private static C3892wO f3555Yu;
    @C0064Am(aul = "6aeb85a4fbc519cacabe1c2dad055819", aum = 5)

    /* renamed from: Yv */
    private static C3892wO f3556Yv;
    @C0064Am(aul = "b69e01713a216abb7e3db7284eb077df", aum = 7)
    private static CruiseSpeedAdapter aYd;
    @C0064Am(aul = "ac3feb7b5563d32eb0d882e1bf39ec2d", aum = 6)
    private static boolean bEl;

    static {
        m17336V();
    }

    public CruiseSpeedBuff() {
        super((C5540aNg) null);
        super.mo10S();
    }

    public CruiseSpeedBuff(C5540aNg ang) {
        super(ang);
    }

    public CruiseSpeedBuff(CruiseSpeedBuffType alt) {
        super((C5540aNg) null);
        super._m_script_init(alt);
    }

    /* renamed from: V */
    static void m17336V() {
        serialVersionUID = (long) 1;
        _m_fieldCount = AttributeBuff._m_fieldCount + 8;
        _m_methodCount = AttributeBuff._m_methodCount + 9;
        int i = AttributeBuff._m_fieldCount;
        C5663aRz[] arzArr = new C5663aRz[(i + 8)];
        C5663aRz b = C5640aRc.m17844b(CruiseSpeedBuff.class, "a447b11b400d1e77774ccc9c9075c43e", i);
        f3548Vt = b;
        arzArr[i] = b;
        int i2 = i + 1;
        C5663aRz b2 = C5640aRc.m17844b(CruiseSpeedBuff.class, "b0e587372d4b73e5bf26a10b9a61253b", i2);
        f3550Vv = b2;
        arzArr[i2] = b2;
        int i3 = i2 + 1;
        C5663aRz b3 = C5640aRc.m17844b(CruiseSpeedBuff.class, "97021fceb0d3f8c5f683fda91c875202", i3);
        f3552Vx = b3;
        arzArr[i3] = b3;
        int i4 = i3 + 1;
        C5663aRz b4 = C5640aRc.m17844b(CruiseSpeedBuff.class, "02748d4dd5dea9a7c1f8bd5639e2af4a", i4);
        f3554Vz = b4;
        arzArr[i4] = b4;
        int i5 = i4 + 1;
        C5663aRz b5 = C5640aRc.m17844b(CruiseSpeedBuff.class, "10421d1a2383fea796e72c7783897ebf", i5);
        atQ = b5;
        arzArr[i5] = b5;
        int i6 = i5 + 1;
        C5663aRz b6 = C5640aRc.m17844b(CruiseSpeedBuff.class, "6aeb85a4fbc519cacabe1c2dad055819", i6);
        atT = b6;
        arzArr[i6] = b6;
        int i7 = i6 + 1;
        C5663aRz b7 = C5640aRc.m17844b(CruiseSpeedBuff.class, "ac3feb7b5563d32eb0d882e1bf39ec2d", i7);
        bEm = b7;
        arzArr[i7] = b7;
        int i8 = i7 + 1;
        C5663aRz b8 = C5640aRc.m17844b(CruiseSpeedBuff.class, "b69e01713a216abb7e3db7284eb077df", i8);
        aYe = b8;
        arzArr[i8] = b8;
        int i9 = i8 + 1;
        _m_fields = arzArr;
        C1634Xv.m11725a((Object[]) AttributeBuff._m_fields, (Object[]) _m_fields);
        int i10 = AttributeBuff._m_methodCount;
        C2491fm[] fmVarArr = new C2491fm[(i10 + 9)];
        C2491fm a = C4105zY.m41624a(CruiseSpeedBuff.class, "3edd32a752112509c25000314d21ee76", i10);
        aPH = a;
        fmVarArr[i10] = a;
        int i11 = i10 + 1;
        C2491fm a2 = C4105zY.m41624a(CruiseSpeedBuff.class, "203c2744d6f1c4f3c8b37bd7101b135d", i11);
        aPI = a2;
        fmVarArr[i11] = a2;
        int i12 = i11 + 1;
        C2491fm a3 = C4105zY.m41624a(CruiseSpeedBuff.class, "71ce8812c454e71bd9255748c165ccc5", i12);
        aYg = a3;
        fmVarArr[i12] = a3;
        int i13 = i12 + 1;
        C2491fm a4 = C4105zY.m41624a(CruiseSpeedBuff.class, "ee265367eafe45140609694583ff6aed", i13);
        aYh = a4;
        fmVarArr[i13] = a4;
        int i14 = i13 + 1;
        C2491fm a5 = C4105zY.m41624a(CruiseSpeedBuff.class, "a979628086ea91e2427f72a8db8f4131", i14);
        aYi = a5;
        fmVarArr[i14] = a5;
        int i15 = i14 + 1;
        C2491fm a6 = C4105zY.m41624a(CruiseSpeedBuff.class, "43e64c74ae60b31114a620a655ade5e2", i15);
        aYj = a6;
        fmVarArr[i15] = a6;
        int i16 = i15 + 1;
        C2491fm a7 = C4105zY.m41624a(CruiseSpeedBuff.class, "68e17b0ee442b343f0332149d221ba41", i16);
        jaZ = a7;
        fmVarArr[i16] = a7;
        int i17 = i16 + 1;
        C2491fm a8 = C4105zY.m41624a(CruiseSpeedBuff.class, "4ef9ae16624025b3ebed347a18b84649", i17);
        aua = a8;
        fmVarArr[i17] = a8;
        int i18 = i17 + 1;
        C2491fm a9 = C4105zY.m41624a(CruiseSpeedBuff.class, "e200dad623d2a3e65f4c8e1d7be0c3ce", i18);
        aub = a9;
        fmVarArr[i18] = a9;
        int i19 = i18 + 1;
        _m_methods = fmVarArr;
        C1634Xv.m11725a((Object[]) AttributeBuff._m_methods, (Object[]) _m_methods);
        ___iScriptClass = aUO.m18566a(CruiseSpeedBuff.class, C5701aTl.class, _m_fields, _m_methods);
    }

    /* renamed from: Kn */
    private C3892wO m17330Kn() {
        return ((CruiseSpeedBuffType) getType()).bLz();
    }

    /* renamed from: Kp */
    private C3892wO m17331Kp() {
        return ((CruiseSpeedBuffType) getType()).bLD();
    }

    @C0064Am(aul = "3edd32a752112509c25000314d21ee76", aum = 0)
    @C5566aOg
    /* renamed from: Ue */
    private void m17334Ue() {
        throw new aWi(new aCE(this, aPH, new Object[0]));
    }

    @C0064Am(aul = "203c2744d6f1c4f3c8b37bd7101b135d", aum = 0)
    @C5566aOg
    /* renamed from: Ug */
    private void m17335Ug() {
        throw new aWi(new aCE(this, aPI, new Object[0]));
    }

    /* renamed from: XN */
    private CruiseSpeedAdapter m17337XN() {
        return (CruiseSpeedAdapter) bFf().mo5608dq().mo3214p(aYe);
    }

    /* renamed from: a */
    private void m17342a(CruiseSpeedAdapter eq) {
        bFf().mo5608dq().mo3197f(aYe, eq);
    }

    private boolean amK() {
        return ((CruiseSpeedBuffType) getType()).cfI();
    }

    /* renamed from: bD */
    private void m17343bD(boolean z) {
        throw new C6039afL();
    }

    /* renamed from: d */
    private void m17344d(C3892wO wOVar) {
        throw new C6039afL();
    }

    /* renamed from: e */
    private void m17345e(C3892wO wOVar) {
        throw new C6039afL();
    }

    /* renamed from: f */
    private void m17346f(C3892wO wOVar) {
        throw new C6039afL();
    }

    /* renamed from: g */
    private void m17347g(C3892wO wOVar) {
        throw new C6039afL();
    }

    /* renamed from: s */
    private void m17348s(C3892wO wOVar) {
        throw new C6039afL();
    }

    /* renamed from: u */
    private void m17349u(C3892wO wOVar) {
        throw new C6039afL();
    }

    /* renamed from: yE */
    private C3892wO m17350yE() {
        return ((CruiseSpeedBuffType) getType()).mo14681yL();
    }

    /* renamed from: yF */
    private C3892wO m17351yF() {
        return ((CruiseSpeedBuffType) getType()).mo14682yN();
    }

    /* renamed from: yG */
    private C3892wO m17352yG() {
        return ((CruiseSpeedBuffType) getType()).mo14683yP();
    }

    /* renamed from: yH */
    private C3892wO m17353yH() {
        return ((CruiseSpeedBuffType) getType()).mo14684yR();
    }

    /* access modifiers changed from: protected */
    /* renamed from: KA */
    public C3892wO mo10830KA() {
        switch (bFf().mo6893i(aub)) {
            case 0:
                return null;
            case 2:
                return (C3892wO) bFf().mo5606d(new aCE(this, aub, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, aub, new Object[0]));
                break;
        }
        return m17333Kz();
    }

    /* access modifiers changed from: protected */
    /* renamed from: Ky */
    public C3892wO mo10831Ky() {
        switch (bFf().mo6893i(aua)) {
            case 0:
                return null;
            case 2:
                return (C3892wO) bFf().mo5606d(new aCE(this, aua, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, aua, new Object[0]));
                break;
        }
        return m17332Kx();
    }

    /* renamed from: S */
    public void mo10S() {
        super.mo10S();
    }

    /* renamed from: T */
    public C6494any mo11T() {
        return ___iScriptClass;
    }

    /* renamed from: U */
    public C2491fm[] mo12U() {
        return _m_methods;
    }

    @C5566aOg
    /* renamed from: Uf */
    public void mo3751Uf() {
        switch (bFf().mo6893i(aPH)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, aPH, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, aPH, new Object[0]));
                break;
        }
        m17334Ue();
    }

    @C5566aOg
    /* renamed from: Uh */
    public void mo3752Uh() {
        switch (bFf().mo6893i(aPI)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, aPI, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, aPI, new Object[0]));
                break;
        }
        m17335Ug();
    }

    /* renamed from: W */
    public Object mo13W() {
        return new C5701aTl(this);
    }

    /* access modifiers changed from: protected */
    /* renamed from: XR */
    public C3892wO mo10832XR() {
        switch (bFf().mo6893i(aYg)) {
            case 0:
                return null;
            case 2:
                return (C3892wO) bFf().mo5606d(new aCE(this, aYg, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, aYg, new Object[0]));
                break;
        }
        return m17338XQ();
    }

    /* access modifiers changed from: protected */
    /* renamed from: XT */
    public C3892wO mo10833XT() {
        switch (bFf().mo6893i(aYh)) {
            case 0:
                return null;
            case 2:
                return (C3892wO) bFf().mo5606d(new aCE(this, aYh, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, aYh, new Object[0]));
                break;
        }
        return m17339XS();
    }

    /* access modifiers changed from: protected */
    /* renamed from: XV */
    public C3892wO mo10834XV() {
        switch (bFf().mo6893i(aYi)) {
            case 0:
                return null;
            case 2:
                return (C3892wO) bFf().mo5606d(new aCE(this, aYi, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, aYi, new Object[0]));
                break;
        }
        return m17340XU();
    }

    /* access modifiers changed from: protected */
    /* renamed from: XX */
    public C3892wO mo10835XX() {
        switch (bFf().mo6893i(aYj)) {
            case 0:
                return null;
            case 2:
                return (C3892wO) bFf().mo5606d(new aCE(this, aYj, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, aYj, new Object[0]));
                break;
        }
        return m17341XW();
    }

    /* renamed from: a */
    public Object mo14a(C0495Gr gr) {
        gr.getArgs();
        switch (gr.mo2417hq() - AttributeBuff._m_methodCount) {
            case 0:
                m17334Ue();
                return null;
            case 1:
                m17335Ug();
                return null;
            case 2:
                return m17338XQ();
            case 3:
                return m17339XS();
            case 4:
                return m17340XU();
            case 5:
                return m17341XW();
            case 6:
                return new Boolean(dBY());
            case 7:
                return m17332Kx();
            case 8:
                return m17333Kz();
            default:
                return super.mo14a(gr);
        }
    }

    /* renamed from: a */
    public void mo15a(Collection collection, C0495Gr gr) {
        super.mo15a(collection, gr);
    }

    /* renamed from: c */
    public C5663aRz[] mo25c() {
        return _m_fields;
    }

    /* access modifiers changed from: protected */
    public boolean dBZ() {
        switch (bFf().mo6893i(jaZ)) {
            case 0:
                return false;
            case 2:
                return ((Boolean) bFf().mo5606d(new aCE(this, jaZ, new Object[0]))).booleanValue();
            case 3:
                bFf().mo5606d(new aCE(this, jaZ, new Object[0]));
                break;
        }
        return dBY();
    }

    /* renamed from: a */
    public void mo10836a(CruiseSpeedBuffType alt) {
        super.mo3934i(alt);
        CruiseAmplifierAdapter aVar = (CruiseAmplifierAdapter) bFf().mo6865M(CruiseAmplifierAdapter.class);
        aVar.mo10838b(this);
        m17342a((CruiseSpeedAdapter) aVar);
    }

    @C0064Am(aul = "71ce8812c454e71bd9255748c165ccc5", aum = 0)
    /* renamed from: XQ */
    private C3892wO m17338XQ() {
        return m17350yE();
    }

    @C0064Am(aul = "ee265367eafe45140609694583ff6aed", aum = 0)
    /* renamed from: XS */
    private C3892wO m17339XS() {
        return m17351yF();
    }

    @C0064Am(aul = "a979628086ea91e2427f72a8db8f4131", aum = 0)
    /* renamed from: XU */
    private C3892wO m17340XU() {
        return m17352yG();
    }

    @C0064Am(aul = "43e64c74ae60b31114a620a655ade5e2", aum = 0)
    /* renamed from: XW */
    private C3892wO m17341XW() {
        return m17353yH();
    }

    @C0064Am(aul = "68e17b0ee442b343f0332149d221ba41", aum = 0)
    private boolean dBY() {
        return amK();
    }

    @C0064Am(aul = "4ef9ae16624025b3ebed347a18b84649", aum = 0)
    /* renamed from: Kx */
    private C3892wO m17332Kx() {
        return m17331Kp();
    }

    @C0064Am(aul = "e200dad623d2a3e65f4c8e1d7be0c3ce", aum = 0)
    /* renamed from: Kz */
    private C3892wO m17333Kz() {
        return m17330Kn();
    }

    @C6485anp
    @C5511aMd
    /* renamed from: a.aPo$a */
    public class CruiseAmplifierAdapter extends CruiseSpeedAdapter implements C1616Xf {
        public static final int _m_fieldCount = 0;
        public static final C5663aRz[] _m_fields = null;
        public static final int _m_methodCount = 0;
        public static final C2491fm[] _m_methods = null;
        /* renamed from: aT */
        public static final C5663aRz f3557aT = null;
        public static final C2491fm aTe = null;
        public static final C2491fm aTf = null;
        public static final C2491fm aTg = null;
        public static final C2491fm aTh = null;
        public static final C2491fm bFx = null;
        public static final C2491fm bFy = null;
        public static final C2491fm cRZ = null;
        public static final long serialVersionUID = 0;
        public static C6494any ___iScriptClass;
        @C0064Am(aul = "510dd575a81d9ec8d05468ac5951f314", aum = 0)
        static /* synthetic */ CruiseSpeedBuff iAS;

        static {
            m17370V();
        }

        public CruiseAmplifierAdapter() {
            super((C5540aNg) null);
            super.mo10S();
        }

        public CruiseAmplifierAdapter(C5540aNg ang) {
            super(ang);
        }

        public CruiseAmplifierAdapter(CruiseSpeedBuff apo) {
            super((C5540aNg) null);
            super._m_script_init(apo);
        }

        /* renamed from: V */
        static void m17370V() {
            serialVersionUID = (long) 1;
            _m_fieldCount = CruiseSpeedAdapter._m_fieldCount + 1;
            _m_methodCount = CruiseSpeedAdapter._m_methodCount + 7;
            int i = CruiseSpeedAdapter._m_fieldCount;
            C5663aRz[] arzArr = new C5663aRz[(i + 1)];
            C5663aRz b = C5640aRc.m17844b(CruiseAmplifierAdapter.class, "510dd575a81d9ec8d05468ac5951f314", i);
            f3557aT = b;
            arzArr[i] = b;
            int i2 = i + 1;
            _m_fields = arzArr;
            C1634Xv.m11725a((Object[]) CruiseSpeedAdapter._m_fields, (Object[]) _m_fields);
            int i3 = CruiseSpeedAdapter._m_methodCount;
            C2491fm[] fmVarArr = new C2491fm[(i3 + 7)];
            C2491fm a = C4105zY.m41624a(CruiseAmplifierAdapter.class, "f3dc40ee67728a96b41c6452a953ee0b", i3);
            aTe = a;
            fmVarArr[i3] = a;
            int i4 = i3 + 1;
            C2491fm a2 = C4105zY.m41624a(CruiseAmplifierAdapter.class, "916752502c1fab466d0c05cf69cf57b7", i4);
            aTf = a2;
            fmVarArr[i4] = a2;
            int i5 = i4 + 1;
            C2491fm a3 = C4105zY.m41624a(CruiseAmplifierAdapter.class, "ef05f55a01dd3373c9d40cd9f1368f4e", i5);
            aTg = a3;
            fmVarArr[i5] = a3;
            int i6 = i5 + 1;
            C2491fm a4 = C4105zY.m41624a(CruiseAmplifierAdapter.class, "6f9298540d8cf529b618eb677b5b48a7", i6);
            aTh = a4;
            fmVarArr[i6] = a4;
            int i7 = i6 + 1;
            C2491fm a5 = C4105zY.m41624a(CruiseAmplifierAdapter.class, "8e4903122664f99625d693561eeec25b", i7);
            bFx = a5;
            fmVarArr[i7] = a5;
            int i8 = i7 + 1;
            C2491fm a6 = C4105zY.m41624a(CruiseAmplifierAdapter.class, "1dfada65679bd542455e89b0d60a04d6", i8);
            bFy = a6;
            fmVarArr[i8] = a6;
            int i9 = i8 + 1;
            C2491fm a7 = C4105zY.m41624a(CruiseAmplifierAdapter.class, "cfeda6d2e6674b5957a3145b0cc9c544", i9);
            cRZ = a7;
            fmVarArr[i9] = a7;
            int i10 = i9 + 1;
            _m_methods = fmVarArr;
            C1634Xv.m11725a((Object[]) CruiseSpeedAdapter._m_methods, (Object[]) _m_methods);
            ___iScriptClass = aUO.m18566a(CruiseAmplifierAdapter.class, C5591aPf.class, _m_fields, _m_methods);
        }

        /* renamed from: a */
        private void m17375a(CruiseSpeedBuff apo) {
            bFf().mo5608dq().mo3197f(f3557aT, apo);
        }

        private CruiseSpeedBuff dog() {
            return (CruiseSpeedBuff) bFf().mo5608dq().mo3214p(f3557aT);
        }

        /* renamed from: S */
        public void mo10S() {
            super.mo10S();
        }

        /* renamed from: T */
        public C6494any mo11T() {
            return ___iScriptClass;
        }

        /* renamed from: U */
        public C2491fm[] mo12U() {
            return _m_methods;
        }

        /* renamed from: VF */
        public float mo1996VF() {
            switch (bFf().mo6893i(aTe)) {
                case 0:
                    return 0.0f;
                case 2:
                    return ((Float) bFf().mo5606d(new aCE(this, aTe, new Object[0]))).floatValue();
                case 3:
                    bFf().mo5606d(new aCE(this, aTe, new Object[0]));
                    break;
            }
            return m17371VE();
        }

        /* renamed from: VH */
        public float mo1997VH() {
            switch (bFf().mo6893i(aTf)) {
                case 0:
                    return 0.0f;
                case 2:
                    return ((Float) bFf().mo5606d(new aCE(this, aTf, new Object[0]))).floatValue();
                case 3:
                    bFf().mo5606d(new aCE(this, aTf, new Object[0]));
                    break;
            }
            return m17372VG();
        }

        /* renamed from: W */
        public Object mo13W() {
            return new C5591aPf(this);
        }

        /* renamed from: a */
        public Object mo14a(C0495Gr gr) {
            gr.getArgs();
            switch (gr.mo2417hq() - CruiseSpeedAdapter._m_methodCount) {
                case 0:
                    return new Float(m17371VE());
                case 1:
                    return new Float(m17372VG());
                case 2:
                    return new Float(m17373VI());
                case 3:
                    return new Float(m17374VJ());
                case 4:
                    return new Float(anq());
                case 5:
                    return new Float(ans());
                case 6:
                    return new Boolean(aNS());
                default:
                    return super.mo14a(gr);
            }
        }

        /* renamed from: a */
        public void mo15a(Collection collection, C0495Gr gr) {
            super.mo15a(collection, gr);
        }

        public boolean aNT() {
            switch (bFf().mo6893i(cRZ)) {
                case 0:
                    return false;
                case 2:
                    return ((Boolean) bFf().mo5606d(new aCE(this, cRZ, new Object[0]))).booleanValue();
                case 3:
                    bFf().mo5606d(new aCE(this, cRZ, new Object[0]));
                    break;
            }
            return aNS();
        }

        public float anr() {
            switch (bFf().mo6893i(bFx)) {
                case 0:
                    return 0.0f;
                case 2:
                    return ((Float) bFf().mo5606d(new aCE(this, bFx, new Object[0]))).floatValue();
                case 3:
                    bFf().mo5606d(new aCE(this, bFx, new Object[0]));
                    break;
            }
            return anq();
        }

        public float ant() {
            switch (bFf().mo6893i(bFy)) {
                case 0:
                    return 0.0f;
                case 2:
                    return ((Float) bFf().mo5606d(new aCE(this, bFy, new Object[0]))).floatValue();
                case 3:
                    bFf().mo5606d(new aCE(this, bFy, new Object[0]));
                    break;
            }
            return ans();
        }

        /* renamed from: c */
        public C5663aRz[] mo25c() {
            return _m_fields;
        }

        /* renamed from: ra */
        public float mo2001ra() {
            switch (bFf().mo6893i(aTh)) {
                case 0:
                    return 0.0f;
                case 2:
                    return ((Float) bFf().mo5606d(new aCE(this, aTh, new Object[0]))).floatValue();
                case 3:
                    bFf().mo5606d(new aCE(this, aTh, new Object[0]));
                    break;
            }
            return m17374VJ();
        }

        /* renamed from: rb */
        public float mo2002rb() {
            switch (bFf().mo6893i(aTg)) {
                case 0:
                    return 0.0f;
                case 2:
                    return ((Float) bFf().mo5606d(new aCE(this, aTg, new Object[0]))).floatValue();
                case 3:
                    bFf().mo5606d(new aCE(this, aTg, new Object[0]));
                    break;
            }
            return m17373VI();
        }

        /* renamed from: b */
        public void mo10838b(CruiseSpeedBuff apo) {
            m17375a(apo);
            super.mo10S();
        }

        @C0064Am(aul = "f3dc40ee67728a96b41c6452a953ee0b", aum = 0)
        /* renamed from: VE */
        private float m17371VE() {
            return dog().mo10835XX().mo22753v(((CruiseSpeedAdapter) mo16070IE()).mo1996VF(), super.mo1996VF());
        }

        @C0064Am(aul = "916752502c1fab466d0c05cf69cf57b7", aum = 0)
        /* renamed from: VG */
        private float m17372VG() {
            return dog().mo10834XV().mo22753v(((CruiseSpeedAdapter) mo16070IE()).mo1997VH(), super.mo1997VH());
        }

        @C0064Am(aul = "ef05f55a01dd3373c9d40cd9f1368f4e", aum = 0)
        /* renamed from: VI */
        private float m17373VI() {
            return dog().mo10833XT().mo22753v(((CruiseSpeedAdapter) mo16070IE()).mo2002rb(), super.mo2002rb());
        }

        @C0064Am(aul = "6f9298540d8cf529b618eb677b5b48a7", aum = 0)
        /* renamed from: VJ */
        private float m17374VJ() {
            return dog().mo10832XR().mo22753v(((CruiseSpeedAdapter) mo16070IE()).mo2001ra(), super.mo2001ra());
        }

        @C0064Am(aul = "8e4903122664f99625d693561eeec25b", aum = 0)
        private float anq() {
            return dog().mo10831Ky().mo22753v(((CruiseSpeedAdapter) mo16070IE()).anr(), super.anr());
        }

        @C0064Am(aul = "1dfada65679bd542455e89b0d60a04d6", aum = 0)
        private float ans() {
            return dog().mo10830KA().mo22753v(((CruiseSpeedAdapter) mo16070IE()).ant(), super.ant());
        }

        @C0064Am(aul = "cfeda6d2e6674b5957a3145b0cc9c544", aum = 0)
        private boolean aNS() {
            return dog().dBZ() || super.aNT();
        }
    }
}
