package game.script.item.buff.module;

import game.network.message.externalizable.aCE;
import logic.baa.*;
import logic.data.mbean.C1064PZ;
import logic.res.code.C5640aRc;
import logic.res.code.C5663aRz;
import logic.res.code.C6494any;
import logic.res.html.C2491fm;
import p001a.*;

import java.util.Collection;

@C0566Hp
@C5829abJ("1.1.2")
@C6485anp
@C5511aMd
/* renamed from: a.aRT */
/* compiled from: a */
public class ShieldBuffType extends AttributeBuffType implements C1616Xf {
    public static final int _m_fieldCount = 0;
    public static final C5663aRz[] _m_fields = null;
    public static final int _m_methodCount = 0;
    public static final C2491fm[] _m_methods = null;
    public static final C5663aRz aOJ = null;
    public static final C5663aRz aOr = null;
    public static final C5663aRz aOt = null;
    public static final C5663aRz aOz = null;
    public static final C2491fm cTX = null;
    public static final C2491fm cTY = null;
    public static final C2491fm cTZ = null;
    public static final C2491fm cUa = null;
    public static final C2491fm cqF = null;
    public static final C2491fm cqG = null;
    public static final C2491fm cqH = null;
    public static final C2491fm cqI = null;
    /* renamed from: dN */
    public static final C2491fm f3702dN = null;
    public static final C2491fm iLe = null;
    public static final long serialVersionUID = 0;
    public static C6494any ___iScriptClass;
    @C0064Am(aul = "f58037ad73314b7a576adef74091cb3c", aum = 0)
    private static C3892wO aBJ;
    @C0064Am(aul = "3f452d0d4aa002e89525b7a0dc233f3d", aum = 1)
    private static C3892wO aBK;
    @C0064Am(aul = "697840596e289689f129908d275094ee", aum = 2)
    private static C3892wO aBL;
    @C0064Am(aul = "15d0c2771675964ee08fbbbfbe0001b6", aum = 3)
    private static C3892wO aBM;

    static {
        m17788V();
    }

    public ShieldBuffType() {
        super((C5540aNg) null);
        super.mo10S();
    }

    public ShieldBuffType(C5540aNg ang) {
        super(ang);
    }

    /* renamed from: V */
    static void m17788V() {
        serialVersionUID = (long) 1;
        _m_fieldCount = AttributeBuffType._m_fieldCount + 4;
        _m_methodCount = AttributeBuffType._m_methodCount + 10;
        int i = AttributeBuffType._m_fieldCount;
        C5663aRz[] arzArr = new C5663aRz[(i + 4)];
        C5663aRz b = C5640aRc.m17844b(ShieldBuffType.class, "f58037ad73314b7a576adef74091cb3c", i);
        aOJ = b;
        arzArr[i] = b;
        int i2 = i + 1;
        C5663aRz b2 = C5640aRc.m17844b(ShieldBuffType.class, "3f452d0d4aa002e89525b7a0dc233f3d", i2);
        aOr = b2;
        arzArr[i2] = b2;
        int i3 = i2 + 1;
        C5663aRz b3 = C5640aRc.m17844b(ShieldBuffType.class, "697840596e289689f129908d275094ee", i3);
        aOz = b3;
        arzArr[i3] = b3;
        int i4 = i3 + 1;
        C5663aRz b4 = C5640aRc.m17844b(ShieldBuffType.class, "15d0c2771675964ee08fbbbfbe0001b6", i4);
        aOt = b4;
        arzArr[i4] = b4;
        int i5 = i4 + 1;
        _m_fields = arzArr;
        C1634Xv.m11725a((Object[]) AttributeBuffType._m_fields, (Object[]) _m_fields);
        int i6 = AttributeBuffType._m_methodCount;
        C2491fm[] fmVarArr = new C2491fm[(i6 + 10)];
        C2491fm a = C4105zY.m41624a(ShieldBuffType.class, "96f9b135b9bf42f2ec198d06ab9a9ac6", i6);
        cqF = a;
        fmVarArr[i6] = a;
        int i7 = i6 + 1;
        C2491fm a2 = C4105zY.m41624a(ShieldBuffType.class, "5035ef0fa668b655d8e563f1fd97e775", i7);
        cqG = a2;
        fmVarArr[i7] = a2;
        int i8 = i7 + 1;
        C2491fm a3 = C4105zY.m41624a(ShieldBuffType.class, "f136ea339854bb999b658df4fac4fc0f", i8);
        cqH = a3;
        fmVarArr[i8] = a3;
        int i9 = i8 + 1;
        C2491fm a4 = C4105zY.m41624a(ShieldBuffType.class, "8aad5f25024eff0ccf235846a832275d", i9);
        cqI = a4;
        fmVarArr[i9] = a4;
        int i10 = i9 + 1;
        C2491fm a5 = C4105zY.m41624a(ShieldBuffType.class, "b1bbebdf7b621e0101fe020b51e617ba", i10);
        cTX = a5;
        fmVarArr[i10] = a5;
        int i11 = i10 + 1;
        C2491fm a6 = C4105zY.m41624a(ShieldBuffType.class, "cf3f62688d96504c26bd6b6f499a781b", i11);
        cTY = a6;
        fmVarArr[i11] = a6;
        int i12 = i11 + 1;
        C2491fm a7 = C4105zY.m41624a(ShieldBuffType.class, "ac59bb906fbd096cbfbca35f1206d3dd", i12);
        cTZ = a7;
        fmVarArr[i12] = a7;
        int i13 = i12 + 1;
        C2491fm a8 = C4105zY.m41624a(ShieldBuffType.class, "5f5468a72d0668443548c01065d9dc54", i13);
        cUa = a8;
        fmVarArr[i13] = a8;
        int i14 = i13 + 1;
        C2491fm a9 = C4105zY.m41624a(ShieldBuffType.class, "96c5f7d10b277f2ce92fc64f5aabfdb7", i14);
        iLe = a9;
        fmVarArr[i14] = a9;
        int i15 = i14 + 1;
        C2491fm a10 = C4105zY.m41624a(ShieldBuffType.class, "f30ad8e7d4d653ad34b73bda48f79bda", i15);
        f3702dN = a10;
        fmVarArr[i15] = a10;
        int i16 = i15 + 1;
        _m_methods = fmVarArr;
        C1634Xv.m11725a((Object[]) AttributeBuffType._m_methods, (Object[]) _m_methods);
        ___iScriptClass = aUO.m18566a(ShieldBuffType.class, C1064PZ.class, _m_fields, _m_methods);
    }

    /* renamed from: U */
    private void m17785U(C3892wO wOVar) {
        bFf().mo5608dq().mo3197f(aOz, wOVar);
    }

    /* renamed from: Ub */
    private C3892wO m17786Ub() {
        return (C3892wO) bFf().mo5608dq().mo3214p(aOJ);
    }

    /* renamed from: Uc */
    private C3892wO m17787Uc() {
        return (C3892wO) bFf().mo5608dq().mo3214p(aOr);
    }

    /* renamed from: V */
    private void m17789V(C3892wO wOVar) {
        bFf().mo5608dq().mo3197f(aOt, wOVar);
    }

    private C3892wO aOS() {
        return (C3892wO) bFf().mo5608dq().mo3214p(aOz);
    }

    private C3892wO aOT() {
        return (C3892wO) bFf().mo5608dq().mo3214p(aOt);
    }

    /* renamed from: v */
    private void m17793v(C3892wO wOVar) {
        bFf().mo5608dq().mo3197f(aOJ, wOVar);
    }

    /* renamed from: w */
    private void m17794w(C3892wO wOVar) {
        bFf().mo5608dq().mo3197f(aOr, wOVar);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Health Points")
    /* renamed from: H */
    public void mo11133H(C3892wO wOVar) {
        switch (bFf().mo6893i(cqG)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, cqG, new Object[]{wOVar}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, cqG, new Object[]{wOVar}));
                break;
        }
        m17783G(wOVar);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Damage Reduction")
    /* renamed from: J */
    public void mo11134J(C3892wO wOVar) {
        switch (bFf().mo6893i(cqI)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, cqI, new Object[]{wOVar}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, cqI, new Object[]{wOVar}));
                break;
        }
        m17784I(wOVar);
    }

    /* renamed from: T */
    public C6494any mo11T() {
        return ___iScriptClass;
    }

    /* renamed from: U */
    public C2491fm[] mo12U() {
        return _m_methods;
    }

    /* renamed from: W */
    public Object mo13W() {
        return new C1064PZ(this);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Regeneration")
    /* renamed from: X */
    public void mo11135X(C3892wO wOVar) {
        switch (bFf().mo6893i(cTY)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, cTY, new Object[]{wOVar}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, cTY, new Object[]{wOVar}));
                break;
        }
        m17790W(wOVar);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Recovery Time")
    /* renamed from: Z */
    public void mo11136Z(C3892wO wOVar) {
        switch (bFf().mo6893i(cUa)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, cUa, new Object[]{wOVar}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, cUa, new Object[]{wOVar}));
                break;
        }
        m17791Y(wOVar);
    }

    /* renamed from: a */
    public Object mo14a(C0495Gr gr) {
        Object[] args = gr.getArgs();
        switch (gr.mo2417hq() - AttributeBuffType._m_methodCount) {
            case 0:
                return aBb();
            case 1:
                m17783G((C3892wO) args[0]);
                return null;
            case 2:
                return aBd();
            case 3:
                m17784I((C3892wO) args[0]);
                return null;
            case 4:
                return aOU();
            case 5:
                m17790W((C3892wO) args[0]);
                return null;
            case 6:
                return aOW();
            case 7:
                m17791Y((C3892wO) args[0]);
                return null;
            case 8:
                return dtQ();
            case 9:
                return m17792aT();
            default:
                return super.mo14a(gr);
        }
    }

    /* renamed from: a */
    public void mo15a(Collection collection, C0495Gr gr) {
        super.mo15a(collection, gr);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Health Points")
    public C3892wO aBc() {
        switch (bFf().mo6893i(cqF)) {
            case 0:
                return null;
            case 2:
                return (C3892wO) bFf().mo5606d(new aCE(this, cqF, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, cqF, new Object[0]));
                break;
        }
        return aBb();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Damage Reduction")
    public C3892wO aBe() {
        switch (bFf().mo6893i(cqH)) {
            case 0:
                return null;
            case 2:
                return (C3892wO) bFf().mo5606d(new aCE(this, cqH, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, cqH, new Object[0]));
                break;
        }
        return aBd();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Regeneration")
    public C3892wO aOV() {
        switch (bFf().mo6893i(cTX)) {
            case 0:
                return null;
            case 2:
                return (C3892wO) bFf().mo5606d(new aCE(this, cTX, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, cTX, new Object[0]));
                break;
        }
        return aOU();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Recovery Time")
    public C3892wO aOX() {
        switch (bFf().mo6893i(cTZ)) {
            case 0:
                return null;
            case 2:
                return (C3892wO) bFf().mo5606d(new aCE(this, cTZ, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, cTZ, new Object[0]));
                break;
        }
        return aOW();
    }

    /* access modifiers changed from: protected */
    /* renamed from: aU */
    public <T extends aDJ> T mo745aU() {
        switch (bFf().mo6893i(f3702dN)) {
            case 0:
                return null;
            case 2:
                return (aDJ) bFf().mo5606d(new aCE(this, f3702dN, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f3702dN, new Object[0]));
                break;
        }
        return m17792aT();
    }

    /* renamed from: c */
    public C5663aRz[] mo25c() {
        return _m_fields;
    }

    public ShieldBuff dtR() {
        switch (bFf().mo6893i(iLe)) {
            case 0:
                return null;
            case 2:
                return (ShieldBuff) bFf().mo5606d(new aCE(this, iLe, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, iLe, new Object[0]));
                break;
        }
        return dtQ();
    }

    /* renamed from: S */
    public void mo10S() {
        super.mo10S();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Health Points")
    @C0064Am(aul = "96f9b135b9bf42f2ec198d06ab9a9ac6", aum = 0)
    private C3892wO aBb() {
        return m17786Ub();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Health Points")
    @C0064Am(aul = "5035ef0fa668b655d8e563f1fd97e775", aum = 0)
    /* renamed from: G */
    private void m17783G(C3892wO wOVar) {
        m17793v(wOVar);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Damage Reduction")
    @C0064Am(aul = "f136ea339854bb999b658df4fac4fc0f", aum = 0)
    private C3892wO aBd() {
        return m17787Uc();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Damage Reduction")
    @C0064Am(aul = "8aad5f25024eff0ccf235846a832275d", aum = 0)
    /* renamed from: I */
    private void m17784I(C3892wO wOVar) {
        m17794w(wOVar);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Regeneration")
    @C0064Am(aul = "b1bbebdf7b621e0101fe020b51e617ba", aum = 0)
    private C3892wO aOU() {
        return aOS();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Regeneration")
    @C0064Am(aul = "cf3f62688d96504c26bd6b6f499a781b", aum = 0)
    /* renamed from: W */
    private void m17790W(C3892wO wOVar) {
        m17785U(wOVar);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Recovery Time")
    @C0064Am(aul = "ac59bb906fbd096cbfbca35f1206d3dd", aum = 0)
    private C3892wO aOW() {
        return aOT();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Recovery Time")
    @C0064Am(aul = "5f5468a72d0668443548c01065d9dc54", aum = 0)
    /* renamed from: Y */
    private void m17791Y(C3892wO wOVar) {
        m17789V(wOVar);
    }

    @C0064Am(aul = "96c5f7d10b277f2ce92fc64f5aabfdb7", aum = 0)
    private ShieldBuff dtQ() {
        return (ShieldBuff) mo745aU();
    }

    @C0064Am(aul = "f30ad8e7d4d653ad34b73bda48f79bda", aum = 0)
    /* renamed from: aT */
    private <T extends aDJ> T m17792aT() {
        T t = (ShieldBuff) bFf().mo6865M(ShieldBuff.class);
        t.mo3755a(this);
        return t;
    }
}
