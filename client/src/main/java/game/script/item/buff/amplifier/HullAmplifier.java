package game.script.item.buff.amplifier;

import game.network.message.externalizable.aCE;
import game.script.damage.DamageType;
import game.script.item.Amplifier;
import game.script.item.AmplifierType;
import game.script.ship.HullAdapter;
import logic.baa.C1616Xf;
import logic.baa.C1634Xv;
import logic.baa.C5511aMd;
import logic.baa.C6485anp;
import logic.data.mbean.C3134oK;
import logic.data.mbean.C5636aQy;
import logic.res.code.C5640aRc;
import logic.res.code.C5663aRz;
import logic.res.code.C6494any;
import logic.res.html.C2491fm;
import p001a.*;

import java.util.Collection;

@C5829abJ("2.0.0")
@C6485anp
@C2712iu(mo19786Bt = AmplifierType.class, version = "2.0.1")
@C5511aMd
/* renamed from: a.acH  reason: case insensitive filesystem */
/* compiled from: a */
public class HullAmplifier extends Amplifier implements C1616Xf {

    /* renamed from: Lm */
    public static final C2491fm f4210Lm = null;
    public static final int _m_fieldCount = 0;
    public static final C5663aRz[] _m_fields = null;
    public static final int _m_methodCount = 0;
    public static final C2491fm[] _m_methods = null;
    public static final C5663aRz aOJ = null;
    public static final C5663aRz aOr = null;
    public static final C5663aRz aPG = null;
    public static final C2491fm aPJ = null;
    public static final C2491fm aPK = null;
    public static final C2491fm atW = null;
    public static final C2491fm atX = null;
    public static final C2491fm eNp = null;
    public static final long serialVersionUID = 0;
    public static C6494any ___iScriptClass;
    @C0064Am(aul = "9abfbfae32b13da458d1135ea6ab26ea", aum = 1)
    private static C3892wO aBJ;
    @C0064Am(aul = "99acb06e1ba508042bab81378d1e9734", aum = 2)
    private static C3892wO aBK;
    @C0064Am(aul = "d767328ee98b375c65976dccb33ad86f", aum = 0)
    private static HullAdapter aPF;

    static {
        m20252V();
    }

    public HullAmplifier() {
        super((C5540aNg) null);
        super.mo10S();
    }

    public HullAmplifier(C5540aNg ang) {
        super(ang);
    }

    public HullAmplifier(AmplifierType iDVar) {
        super((C5540aNg) null);
        super.mo1956e(iDVar);
    }

    /* renamed from: V */
    static void m20252V() {
        serialVersionUID = (long) 1;
        _m_fieldCount = Amplifier._m_fieldCount + 3;
        _m_methodCount = Amplifier._m_methodCount + 6;
        int i = Amplifier._m_fieldCount;
        C5663aRz[] arzArr = new C5663aRz[(i + 3)];
        C5663aRz b = C5640aRc.m17844b(HullAmplifier.class, "d767328ee98b375c65976dccb33ad86f", i);
        aPG = b;
        arzArr[i] = b;
        int i2 = i + 1;
        C5663aRz b2 = C5640aRc.m17844b(HullAmplifier.class, "9abfbfae32b13da458d1135ea6ab26ea", i2);
        aOJ = b2;
        arzArr[i2] = b2;
        int i3 = i2 + 1;
        C5663aRz b3 = C5640aRc.m17844b(HullAmplifier.class, "99acb06e1ba508042bab81378d1e9734", i3);
        aOr = b3;
        arzArr[i3] = b3;
        int i4 = i3 + 1;
        _m_fields = arzArr;
        C1634Xv.m11725a((Object[]) Amplifier._m_fields, (Object[]) _m_fields);
        int i5 = Amplifier._m_methodCount;
        C2491fm[] fmVarArr = new C2491fm[(i5 + 6)];
        C2491fm a = C4105zY.m41624a(HullAmplifier.class, "73d4c304dc0e35fe2e167bad4966f4c6", i5);
        atW = a;
        fmVarArr[i5] = a;
        int i6 = i5 + 1;
        C2491fm a2 = C4105zY.m41624a(HullAmplifier.class, "ece29ec838caa9aa04ef2636905a9667", i6);
        atX = a2;
        fmVarArr[i6] = a2;
        int i7 = i6 + 1;
        C2491fm a3 = C4105zY.m41624a(HullAmplifier.class, "9016a410fb8c63669c5513ddcfd6d410", i7);
        aPJ = a3;
        fmVarArr[i7] = a3;
        int i8 = i7 + 1;
        C2491fm a4 = C4105zY.m41624a(HullAmplifier.class, "f6848950349fe1245e33f08ab5d0a10f", i8);
        aPK = a4;
        fmVarArr[i8] = a4;
        int i9 = i8 + 1;
        C2491fm a5 = C4105zY.m41624a(HullAmplifier.class, "6ce2a999796d2ab7dc9017213071e9e1", i9);
        eNp = a5;
        fmVarArr[i9] = a5;
        int i10 = i9 + 1;
        C2491fm a6 = C4105zY.m41624a(HullAmplifier.class, "51059492195e0175fb1c64b3ede80533", i10);
        f4210Lm = a6;
        fmVarArr[i10] = a6;
        int i11 = i10 + 1;
        _m_methods = fmVarArr;
        C1634Xv.m11725a((Object[]) Amplifier._m_methods, (Object[]) _m_methods);
        ___iScriptClass = aUO.m18566a(HullAmplifier.class, C3134oK.class, _m_fields, _m_methods);
    }

    @C0064Am(aul = "73d4c304dc0e35fe2e167bad4966f4c6", aum = 0)
    @C5566aOg
    /* renamed from: Kr */
    private void m20245Kr() {
        throw new aWi(new aCE(this, atW, new Object[0]));
    }

    @C0064Am(aul = "ece29ec838caa9aa04ef2636905a9667", aum = 0)
    @C5566aOg
    /* renamed from: Kt */
    private void m20246Kt() {
        throw new aWi(new aCE(this, atX, new Object[0]));
    }

    /* renamed from: Ub */
    private C3892wO m20247Ub() {
        return ((HullAmplifierType) getType()).aBc();
    }

    /* renamed from: Uc */
    private C3892wO m20248Uc() {
        return ((HullAmplifierType) getType()).aBe();
    }

    /* renamed from: Ud */
    private HullAdapter m20249Ud() {
        return (HullAdapter) bFf().mo5608dq().mo3214p(aPG);
    }

    /* renamed from: a */
    private void m20253a(HullAdapter alm) {
        bFf().mo5608dq().mo3197f(aPG, alm);
    }

    @C0064Am(aul = "51059492195e0175fb1c64b3ede80533", aum = 0)
    @C5566aOg
    @C2499fr
    /* renamed from: qU */
    private void m20254qU() {
        throw new aWi(new aCE(this, f4210Lm, new Object[0]));
    }

    /* renamed from: v */
    private void m20255v(C3892wO wOVar) {
        throw new C6039afL();
    }

    /* renamed from: w */
    private void m20256w(C3892wO wOVar) {
        throw new C6039afL();
    }

    @C5566aOg
    /* renamed from: Ks */
    public void mo1952Ks() {
        switch (bFf().mo6893i(atW)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, atW, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, atW, new Object[0]));
                break;
        }
        m20245Kr();
    }

    @C5566aOg
    /* renamed from: Ku */
    public void mo1953Ku() {
        switch (bFf().mo6893i(atX)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, atX, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, atX, new Object[0]));
                break;
        }
        m20246Kt();
    }

    /* renamed from: S */
    public void mo10S() {
        super.mo10S();
    }

    /* renamed from: T */
    public C6494any mo11T() {
        return ___iScriptClass;
    }

    /* renamed from: U */
    public C2491fm[] mo12U() {
        return _m_methods;
    }

    /* access modifiers changed from: protected */
    /* renamed from: Uj */
    public C3892wO mo12585Uj() {
        switch (bFf().mo6893i(aPJ)) {
            case 0:
                return null;
            case 2:
                return (C3892wO) bFf().mo5606d(new aCE(this, aPJ, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, aPJ, new Object[0]));
                break;
        }
        return m20250Ui();
    }

    /* access modifiers changed from: protected */
    /* renamed from: Ul */
    public C3892wO mo12586Ul() {
        switch (bFf().mo6893i(aPK)) {
            case 0:
                return null;
            case 2:
                return (C3892wO) bFf().mo5606d(new aCE(this, aPK, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, aPK, new Object[0]));
                break;
        }
        return m20251Uk();
    }

    /* renamed from: W */
    public Object mo13W() {
        return new C3134oK(this);
    }

    /* renamed from: a */
    public Object mo14a(C0495Gr gr) {
        gr.getArgs();
        switch (gr.mo2417hq() - Amplifier._m_methodCount) {
            case 0:
                m20245Kr();
                return null;
            case 1:
                m20246Kt();
                return null;
            case 2:
                return m20250Ui();
            case 3:
                return m20251Uk();
            case 4:
                return new Float(bIj());
            case 5:
                m20254qU();
                return null;
            default:
                return super.mo14a(gr);
        }
    }

    /* renamed from: a */
    public void mo15a(Collection collection, C0495Gr gr) {
        super.mo15a(collection, gr);
    }

    /* access modifiers changed from: protected */
    public float bIk() {
        switch (bFf().mo6893i(eNp)) {
            case 0:
                return 0.0f;
            case 2:
                return ((Float) bFf().mo5606d(new aCE(this, eNp, new Object[0]))).floatValue();
            case 3:
                bFf().mo5606d(new aCE(this, eNp, new Object[0]));
                break;
        }
        return bIj();
    }

    /* renamed from: c */
    public C5663aRz[] mo25c() {
        return _m_fields;
    }

    @C5566aOg
    @C2499fr
    /* renamed from: qV */
    public void mo1959qV() {
        switch (bFf().mo6893i(f4210Lm)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f4210Lm, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f4210Lm, new Object[0]));
                break;
        }
        m20254qU();
    }

    /* renamed from: e */
    public void mo1956e(AmplifierType iDVar) {
        super.mo1956e(iDVar);
    }

    @C0064Am(aul = "9016a410fb8c63669c5513ddcfd6d410", aum = 0)
    /* renamed from: Ui */
    private C3892wO m20250Ui() {
        return m20248Uc();
    }

    @C0064Am(aul = "f6848950349fe1245e33f08ab5d0a10f", aum = 0)
    /* renamed from: Uk */
    private C3892wO m20251Uk() {
        return m20247Ub();
    }

    @C0064Am(aul = "6ce2a999796d2ab7dc9017213071e9e1", aum = 0)
    private float bIj() {
        if (mo12586Ul().coZ() == C3892wO.C3893a.PERCENT) {
            return (float) ((int) (((HullAdapter) m20249Ud().mo16070IE()).mo9878hh() * (mo12586Ul().getValue() / 100.0f)));
        }
        return mo12586Ul().getValue();
    }

    @C6485anp
    @C5511aMd
    /* renamed from: a.acH$a */
    public class HullAmplifierAdapter extends HullAdapter implements C1616Xf {
        public static final int _m_fieldCount = 0;
        public static final C5663aRz[] _m_fields = null;
        public static final int _m_methodCount = 0;
        public static final C2491fm[] _m_methods = null;
        /* renamed from: aT */
        public static final C5663aRz f4211aT = null;
        /* renamed from: qa */
        public static final C2491fm f4212qa = null;
        /* renamed from: qb */
        public static final C2491fm f4213qb = null;
        public static final long serialVersionUID = 0;
        /* renamed from: uI */
        public static final C2491fm f4214uI = null;
        public static C6494any ___iScriptClass;
        @C0064Am(aul = "1f5101a9369a5bcd62dd73bef5edef18", aum = 0)
        static /* synthetic */ HullAmplifier feI;

        static {
            m20270V();
        }

        public HullAmplifierAdapter() {
            super((C5540aNg) null);
            super.mo10S();
        }

        public HullAmplifierAdapter(C5540aNg ang) {
            super(ang);
        }

        public HullAmplifierAdapter(HullAmplifier ach) {
            super((C5540aNg) null);
            super._m_script_init(ach);
        }

        /* renamed from: V */
        static void m20270V() {
            serialVersionUID = (long) 1;
            _m_fieldCount = HullAdapter._m_fieldCount + 1;
            _m_methodCount = HullAdapter._m_methodCount + 3;
            int i = HullAdapter._m_fieldCount;
            C5663aRz[] arzArr = new C5663aRz[(i + 1)];
            C5663aRz b = C5640aRc.m17844b(HullAmplifierAdapter.class, "1f5101a9369a5bcd62dd73bef5edef18", i);
            f4211aT = b;
            arzArr[i] = b;
            int i2 = i + 1;
            _m_fields = arzArr;
            C1634Xv.m11725a((Object[]) HullAdapter._m_fields, (Object[]) _m_fields);
            int i3 = HullAdapter._m_methodCount;
            C2491fm[] fmVarArr = new C2491fm[(i3 + 3)];
            C2491fm a = C4105zY.m41624a(HullAmplifierAdapter.class, "3a16ad9844b57703ac0018e577381307", i3);
            f4212qa = a;
            fmVarArr[i3] = a;
            int i4 = i3 + 1;
            C2491fm a2 = C4105zY.m41624a(HullAmplifierAdapter.class, "87913039c6c26a3938475dfd119ae77a", i4);
            f4214uI = a2;
            fmVarArr[i4] = a2;
            int i5 = i4 + 1;
            C2491fm a3 = C4105zY.m41624a(HullAmplifierAdapter.class, "aac15564bf837bce343550d118d2ecf5", i5);
            f4213qb = a3;
            fmVarArr[i5] = a3;
            int i6 = i5 + 1;
            _m_methods = fmVarArr;
            C1634Xv.m11725a((Object[]) HullAdapter._m_methods, (Object[]) _m_methods);
            ___iScriptClass = aUO.m18566a(HullAmplifierAdapter.class, C5636aQy.class, _m_fields, _m_methods);
        }

        /* renamed from: a */
        private void m20273a(HullAmplifier ach) {
            bFf().mo5608dq().mo3197f(f4211aT, ach);
        }

        private HullAmplifier bPI() {
            return (HullAmplifier) bFf().mo5608dq().mo3214p(f4211aT);
        }

        /* renamed from: S */
        public void mo10S() {
            super.mo10S();
        }

        /* renamed from: T */
        public C6494any mo11T() {
            return ___iScriptClass;
        }

        /* renamed from: U */
        public C2491fm[] mo12U() {
            return _m_methods;
        }

        /* renamed from: W */
        public Object mo13W() {
            return new C5636aQy(this);
        }

        /* renamed from: a */
        public Object mo14a(C0495Gr gr) {
            Object[] args = gr.getArgs();
            switch (gr.mo2417hq() - HullAdapter._m_methodCount) {
                case 0:
                    return new Float(m20272a((DamageType) args[0], ((Float) args[1]).floatValue()));
                case 1:
                    return new Float(m20271a((DamageType) args[0]));
                case 2:
                    return new Float(m20274hg());
                default:
                    return super.mo14a(gr);
            }
        }

        /* renamed from: a */
        public void mo15a(Collection collection, C0495Gr gr) {
            super.mo15a(collection, gr);
        }

        /* renamed from: b */
        public float mo9876b(DamageType fr) {
            switch (bFf().mo6893i(f4214uI)) {
                case 0:
                    return 0.0f;
                case 2:
                    return ((Float) bFf().mo5606d(new aCE(this, f4214uI, new Object[]{fr}))).floatValue();
                case 3:
                    bFf().mo5606d(new aCE(this, f4214uI, new Object[]{fr}));
                    break;
            }
            return m20271a(fr);
        }

        /* renamed from: b */
        public float mo9877b(DamageType fr, float f) {
            switch (bFf().mo6893i(f4212qa)) {
                case 0:
                    return 0.0f;
                case 2:
                    return ((Float) bFf().mo5606d(new aCE(this, f4212qa, new Object[]{fr, new Float(f)}))).floatValue();
                case 3:
                    bFf().mo5606d(new aCE(this, f4212qa, new Object[]{fr, new Float(f)}));
                    break;
            }
            return m20272a(fr, f);
        }

        /* renamed from: c */
        public C5663aRz[] mo25c() {
            return _m_fields;
        }

        /* renamed from: hh */
        public float mo9878hh() {
            switch (bFf().mo6893i(f4213qb)) {
                case 0:
                    return 0.0f;
                case 2:
                    return ((Float) bFf().mo5606d(new aCE(this, f4213qb, new Object[0]))).floatValue();
                case 3:
                    bFf().mo5606d(new aCE(this, f4213qb, new Object[0]));
                    break;
            }
            return m20274hg();
        }

        /* renamed from: b */
        public void mo12588b(HullAmplifier ach) {
            m20273a(ach);
            super.mo10S();
        }

        @C0064Am(aul = "3a16ad9844b57703ac0018e577381307", aum = 0)
        /* renamed from: a */
        private float m20272a(DamageType fr, float f) {
            return bPI().mo12585Uj().mo22753v(((HullAdapter) mo16070IE()).mo9877b(fr, f), super.mo9877b(fr, f));
        }

        @C0064Am(aul = "87913039c6c26a3938475dfd119ae77a", aum = 0)
        /* renamed from: a */
        private float m20271a(DamageType fr) {
            return bPI().mo12585Uj().mo22753v(((HullAdapter) mo16070IE()).mo9876b(fr), super.mo9876b(fr));
        }

        @C0064Am(aul = "aac15564bf837bce343550d118d2ecf5", aum = 0)
        /* renamed from: hg */
        private float m20274hg() {
            float v = bPI().mo12586Ul().mo22753v(((HullAdapter) mo16070IE()).mo9878hh(), super.mo9878hh());
            if (v > 0.0f) {
                return v;
            }
            return 1.0f;
        }
    }
}
