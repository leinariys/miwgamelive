package game.script.item.buff.module;

import com.hoplon.geometry.Vec3f;
import game.network.message.externalizable.aCE;
import game.script.Actor;
import game.script.damage.DamageType;
import game.script.ship.Ship;
import logic.baa.*;
import logic.bbb.aDR;
import logic.data.mbean.aGV;
import logic.res.code.C5640aRc;
import logic.res.code.C5663aRz;
import logic.res.code.C6494any;
import logic.res.html.C2491fm;
import p001a.*;

import java.util.Collection;

@C5829abJ("1.1.2")
@C6485anp
@C2712iu(version = "1.1.2")
@C5511aMd
/* renamed from: a.aRq  reason: case insensitive filesystem */
/* compiled from: a */
public class Damager extends AttributeBuff implements C1616Xf, C6222aim {
    /* renamed from: _f_getOffloaders_0020_0028_0029Ljava_002futil_002fCollection_003b */
    public static final C2491fm f3725x99c43db3 = null;
    public static final C2491fm _f_onResurrect_0020_0028_0029V = null;
    public static final C2491fm _f_tick_0020_0028F_0029V = null;
    public static final int _m_fieldCount = 0;
    public static final C5663aRz[] _m_fields = null;
    public static final int _m_methodCount = 0;
    public static final C2491fm[] _m_methods = null;
    public static final C2491fm aPH = null;
    public static final C2491fm aPI = null;
    public static final C5663aRz bEg = null;
    public static final C5663aRz bEi = null;
    public static final C5663aRz hhL = null;
    public static final C5663aRz iCV = null;
    public static final C5663aRz iCW = null;
    public static final C5663aRz iHp = null;
    public static final C5663aRz iHq = null;
    public static final C2491fm iHr = null;
    public static final long serialVersionUID = 0;
    public static C6494any ___iScriptClass;
    @C0064Am(aul = "6e97a180910a233d22ff7abd6c84e5d8", aum = 6)
    private static boolean apZ;
    @C0064Am(aul = "b6354d1ce81e16e147f23d90522b8081", aum = 0)
    private static float eJl;
    @C0064Am(aul = "872636c9d77ecd5dd2a171ef5e092caf", aum = 1)
    private static float eJm;
    @C0064Am(aul = "dd9f3a4b4b3c0e22056a9affbef715a2", aum = 2)
    private static DamageType eJn;
    @C0064Am(aul = "8b1ab0ffb2b1e4923e00b96baf284550", aum = 3)
    private static DamageType eJo;
    @C0064Am(aul = "2d118185023278678be81c0937e34fe0", aum = 4)
    private static float hTP;
    @C0064Am(aul = "74c56fed51ec69e215696fa87d482017", aum = 5)
    private static float hTQ;

    static {
        m17974V();
    }

    public Damager() {
        super((C5540aNg) null);
        super.mo10S();
    }

    public Damager(C5540aNg ang) {
        super(ang);
    }

    public Damager(DamagerType aps) {
        super((C5540aNg) null);
        super._m_script_init(aps);
    }

    /* renamed from: V */
    static void m17974V() {
        serialVersionUID = (long) 1;
        _m_fieldCount = AttributeBuff._m_fieldCount + 7;
        _m_methodCount = AttributeBuff._m_methodCount + 6;
        int i = AttributeBuff._m_fieldCount;
        C5663aRz[] arzArr = new C5663aRz[(i + 7)];
        C5663aRz b = C5640aRc.m17844b(Damager.class, "b6354d1ce81e16e147f23d90522b8081", i);
        bEg = b;
        arzArr[i] = b;
        int i2 = i + 1;
        C5663aRz b2 = C5640aRc.m17844b(Damager.class, "872636c9d77ecd5dd2a171ef5e092caf", i2);
        bEi = b2;
        arzArr[i2] = b2;
        int i3 = i2 + 1;
        C5663aRz b3 = C5640aRc.m17844b(Damager.class, "dd9f3a4b4b3c0e22056a9affbef715a2", i3);
        iCV = b3;
        arzArr[i3] = b3;
        int i4 = i3 + 1;
        C5663aRz b4 = C5640aRc.m17844b(Damager.class, "8b1ab0ffb2b1e4923e00b96baf284550", i4);
        iCW = b4;
        arzArr[i4] = b4;
        int i5 = i4 + 1;
        C5663aRz b5 = C5640aRc.m17844b(Damager.class, "2d118185023278678be81c0937e34fe0", i5);
        iHp = b5;
        arzArr[i5] = b5;
        int i6 = i5 + 1;
        C5663aRz b6 = C5640aRc.m17844b(Damager.class, "74c56fed51ec69e215696fa87d482017", i6);
        iHq = b6;
        arzArr[i6] = b6;
        int i7 = i6 + 1;
        C5663aRz b7 = C5640aRc.m17844b(Damager.class, "6e97a180910a233d22ff7abd6c84e5d8", i7);
        hhL = b7;
        arzArr[i7] = b7;
        int i8 = i7 + 1;
        _m_fields = arzArr;
        C1634Xv.m11725a((Object[]) AttributeBuff._m_fields, (Object[]) _m_fields);
        int i9 = AttributeBuff._m_methodCount;
        C2491fm[] fmVarArr = new C2491fm[(i9 + 6)];
        C2491fm a = C4105zY.m41624a(Damager.class, "bd3713d6a239a83283aabe3fd230bb68", i9);
        aPH = a;
        fmVarArr[i9] = a;
        int i10 = i9 + 1;
        C2491fm a2 = C4105zY.m41624a(Damager.class, "9aa3ef2ab1eab5091b4fb035849de23b", i10);
        aPI = a2;
        fmVarArr[i10] = a2;
        int i11 = i10 + 1;
        C2491fm a3 = C4105zY.m41624a(Damager.class, "20f3be224ad520bc1dc8bbf91b0eb2f4", i11);
        iHr = a3;
        fmVarArr[i11] = a3;
        int i12 = i11 + 1;
        C2491fm a4 = C4105zY.m41624a(Damager.class, "f0ce6386f3de4f020db005bd322a4763", i12);
        _f_tick_0020_0028F_0029V = a4;
        fmVarArr[i12] = a4;
        int i13 = i12 + 1;
        C2491fm a5 = C4105zY.m41624a(Damager.class, "9c869d4328c8de2decec416c048dbe2b", i13);
        _f_onResurrect_0020_0028_0029V = a5;
        fmVarArr[i13] = a5;
        int i14 = i13 + 1;
        C2491fm a6 = C4105zY.m41624a(Damager.class, "cea5597d736901b970dc4e43c8d71993", i14);
        f3725x99c43db3 = a6;
        fmVarArr[i14] = a6;
        int i15 = i14 + 1;
        _m_methods = fmVarArr;
        C1634Xv.m11725a((Object[]) AttributeBuff._m_methods, (Object[]) _m_methods);
        ___iScriptClass = aUO.m18566a(Damager.class, aGV.class, _m_fields, _m_methods);
    }

    /* renamed from: A */
    private void m17969A(DamageType fr) {
        throw new C6039afL();
    }

    /* renamed from: B */
    private void m17970B(DamageType fr) {
        throw new C6039afL();
    }

    @C0064Am(aul = "f0ce6386f3de4f020db005bd322a4763", aum = 0)
    @C5566aOg
    /* renamed from: U */
    private void m17971U(float f) {
        throw new aWi(new aCE(this, _f_tick_0020_0028F_0029V, new Object[]{new Float(f)}));
    }

    @C0064Am(aul = "bd3713d6a239a83283aabe3fd230bb68", aum = 0)
    @C5566aOg
    /* renamed from: Ue */
    private void m17972Ue() {
        throw new aWi(new aCE(this, aPH, new Object[0]));
    }

    @C0064Am(aul = "9aa3ef2ab1eab5091b4fb035849de23b", aum = 0)
    @C5566aOg
    /* renamed from: Ug */
    private void m17973Ug() {
        throw new aWi(new aCE(this, aPI, new Object[0]));
    }

    private boolean cJA() {
        return bFf().mo5608dq().mo3201h(hhL);
    }

    private float dow() {
        return ((DamagerType) getType()).doB();
    }

    private float dox() {
        return ((DamagerType) getType()).doD();
    }

    private DamageType doy() {
        return ((DamagerType) getType()).doF();
    }

    private DamageType doz() {
        return ((DamagerType) getType()).doH();
    }

    private float drA() {
        return bFf().mo5608dq().mo3211m(iHp);
    }

    private float drB() {
        return bFf().mo5608dq().mo3211m(iHq);
    }

    @C4034yP
    @C2499fr
    @C1253SX
    private void drD() {
        switch (bFf().mo6893i(iHr)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, iHr, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, iHr, new Object[0]));
                break;
        }
        drC();
    }

    /* renamed from: hR */
    private void m17976hR(boolean z) {
        bFf().mo5608dq().mo3153a(hhL, z);
    }

    /* renamed from: nG */
    private void m17977nG(float f) {
        throw new C6039afL();
    }

    /* renamed from: nH */
    private void m17978nH(float f) {
        throw new C6039afL();
    }

    /* renamed from: oq */
    private void m17979oq(float f) {
        bFf().mo5608dq().mo3150a(iHp, f);
    }

    /* renamed from: or */
    private void m17980or(float f) {
        bFf().mo5608dq().mo3150a(iHq, f);
    }

    /* renamed from: S */
    public void mo10S() {
        super.mo10S();
    }

    /* renamed from: T */
    public C6494any mo11T() {
        return ___iScriptClass;
    }

    /* renamed from: U */
    public C2491fm[] mo12U() {
        return _m_methods;
    }

    @C5566aOg
    /* renamed from: Uf */
    public void mo3751Uf() {
        switch (bFf().mo6893i(aPH)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, aPH, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, aPH, new Object[0]));
                break;
        }
        m17972Ue();
    }

    @C5566aOg
    /* renamed from: Uh */
    public void mo3752Uh() {
        switch (bFf().mo6893i(aPI)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, aPI, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, aPI, new Object[0]));
                break;
        }
        m17973Ug();
    }

    @C5566aOg
    /* renamed from: V */
    public void mo4709V(float f) {
        switch (bFf().mo6893i(_f_tick_0020_0028F_0029V)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, _f_tick_0020_0028F_0029V, new Object[]{new Float(f)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, _f_tick_0020_0028F_0029V, new Object[]{new Float(f)}));
                break;
        }
        m17971U(f);
    }

    /* renamed from: W */
    public Object mo13W() {
        return new aGV(this);
    }

    /* renamed from: a */
    public Object mo14a(C0495Gr gr) {
        Object[] args = gr.getArgs();
        switch (gr.mo2417hq() - AttributeBuff._m_methodCount) {
            case 0:
                m17972Ue();
                return null;
            case 1:
                m17973Ug();
                return null;
            case 2:
                drC();
                return null;
            case 3:
                m17971U(((Float) args[0]).floatValue());
                return null;
            case 4:
                m17975aG();
                return null;
            case 5:
                return aad();
            default:
                return super.mo14a(gr);
        }
    }

    /* renamed from: a */
    public void mo15a(Collection collection, C0495Gr gr) {
        super.mo15a(collection, gr);
    }

    /* renamed from: aH */
    public void mo70aH() {
        switch (bFf().mo6893i(_f_onResurrect_0020_0028_0029V)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, _f_onResurrect_0020_0028_0029V, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, _f_onResurrect_0020_0028_0029V, new Object[0]));
                break;
        }
        m17975aG();
    }

    public Collection<aDR> aae() {
        switch (bFf().mo6893i(f3725x99c43db3)) {
            case 0:
                return null;
            case 2:
                return (Collection) bFf().mo5606d(new aCE(this, f3725x99c43db3, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f3725x99c43db3, new Object[0]));
                break;
        }
        return aad();
    }

    /* renamed from: c */
    public C5663aRz[] mo25c() {
        return _m_fields;
    }

    /* renamed from: a */
    public void mo11220a(DamagerType aps) {
        super.mo3934i(aps);
    }

    @C4034yP
    @C0064Am(aul = "20f3be224ad520bc1dc8bbf91b0eb2f4", aum = 0)
    @C2499fr
    @C1253SX
    private void drC() {
        Ship bhP = bhP();
        if (bhP != null) {
            C5260aCm acm = new C5260aCm();
            if (doy() != null && drA() > 1.0E-5f) {
                acm.mo8227g(doy(), drA());
            }
            if (doz() != null && drB() > 1.0E-5f) {
                acm.mo8227g(doz(), drB());
            }
            Actor bhN = bhN();
            bhP.mo1064f(bhN, acm, bhP.getPosition().mo9485aB(bhN.getPosition()), (Vec3f) null);
        }
    }

    @C0064Am(aul = "9c869d4328c8de2decec416c048dbe2b", aum = 0)
    /* renamed from: aG */
    private void m17975aG() {
        super.mo70aH();
        if (bHa()) {
            m17976hR(false);
        }
    }

    @C0064Am(aul = "cea5597d736901b970dc4e43c8d71993", aum = 0)
    private Collection<aDR> aad() {
        return bhP().aae();
    }
}
