package game.script.item.buff.module;

import game.network.message.externalizable.aCE;
import logic.baa.*;
import logic.data.mbean.C5995aeT;
import logic.res.code.C5640aRc;
import logic.res.code.C5663aRz;
import logic.res.code.C6494any;
import logic.res.html.C2491fm;
import p001a.*;

import java.util.Collection;

@C0566Hp
@C5829abJ("1.1.2")
@C6485anp
@C5511aMd
/* renamed from: a.ajS  reason: case insensitive filesystem */
/* compiled from: a */
public class WeaponBuffType extends AttributeBuffType implements C1616Xf {
    public static final int _m_fieldCount = 0;
    public static final C5663aRz[] _m_fields = null;
    public static final int _m_methodCount = 0;
    public static final C2491fm[] _m_methods = null;
    public static final C5663aRz bEc = null;
    public static final C5663aRz bEg = null;
    public static final C5663aRz bEi = null;
    public static final C5663aRz bEk = null;
    public static final C5663aRz bEm = null;
    public static final C2491fm cHL = null;
    public static final C2491fm cHM = null;
    public static final C2491fm cHN = null;
    public static final C2491fm cHO = null;
    public static final C2491fm cHP = null;
    public static final C2491fm cHQ = null;
    public static final C2491fm cHR = null;
    public static final C2491fm cHS = null;
    public static final C2491fm cHT = null;
    public static final C2491fm cHU = null;
    public static final C2491fm cHV = null;
    public static final C2491fm cHW = null;
    /* renamed from: dN */
    public static final C2491fm f4698dN = null;
    public static final C2491fm fSr = null;
    public static final C2491fm fSs = null;
    public static final C2491fm fSt = null;
    public static final long serialVersionUID = 0;
    /* renamed from: uN */
    public static final C5663aRz f4699uN = null;
    /* renamed from: uS */
    public static final C5663aRz f4700uS = null;
    public static C6494any ___iScriptClass;
    @C0064Am(aul = "ba4f06dbbbea7cd8dc351667bb1132eb", aum = 0)
    private static C3892wO bEb;
    @C0064Am(aul = "415807d41691860180a5712a4e53708d", aum = 1)
    private static C3892wO bEd;
    @C0064Am(aul = "3c2053a9492e443ed9f9b5f7663c4ea9", aum = 2)
    private static C3892wO bEe;
    @C0064Am(aul = "89ab15270fd8409323174217ca7b7487", aum = 3)
    private static C3892wO bEf;
    @C0064Am(aul = "adc7632be2369f83733d34ee1d1d687a", aum = 4)
    private static C3892wO bEh;
    @C0064Am(aul = "e04471dbbb2aea8498160acbfaa73350", aum = 5)
    private static boolean bEj;
    @C0064Am(aul = "a1fd0f3cc2f3561e7a2a78bfc47023f3", aum = 6)
    private static boolean bEl;

    static {
        m22907V();
    }

    public WeaponBuffType() {
        super((C5540aNg) null);
        super.mo10S();
    }

    public WeaponBuffType(C5540aNg ang) {
        super(ang);
    }

    /* renamed from: V */
    static void m22907V() {
        serialVersionUID = (long) 1;
        _m_fieldCount = AttributeBuffType._m_fieldCount + 7;
        _m_methodCount = AttributeBuffType._m_methodCount + 16;
        int i = AttributeBuffType._m_fieldCount;
        C5663aRz[] arzArr = new C5663aRz[(i + 7)];
        C5663aRz b = C5640aRc.m17844b(WeaponBuffType.class, "ba4f06dbbbea7cd8dc351667bb1132eb", i);
        bEc = b;
        arzArr[i] = b;
        int i2 = i + 1;
        C5663aRz b2 = C5640aRc.m17844b(WeaponBuffType.class, "415807d41691860180a5712a4e53708d", i2);
        f4700uS = b2;
        arzArr[i2] = b2;
        int i3 = i2 + 1;
        C5663aRz b3 = C5640aRc.m17844b(WeaponBuffType.class, "3c2053a9492e443ed9f9b5f7663c4ea9", i3);
        f4699uN = b3;
        arzArr[i3] = b3;
        int i4 = i3 + 1;
        C5663aRz b4 = C5640aRc.m17844b(WeaponBuffType.class, "89ab15270fd8409323174217ca7b7487", i4);
        bEg = b4;
        arzArr[i4] = b4;
        int i5 = i4 + 1;
        C5663aRz b5 = C5640aRc.m17844b(WeaponBuffType.class, "adc7632be2369f83733d34ee1d1d687a", i5);
        bEi = b5;
        arzArr[i5] = b5;
        int i6 = i5 + 1;
        C5663aRz b6 = C5640aRc.m17844b(WeaponBuffType.class, "e04471dbbb2aea8498160acbfaa73350", i6);
        bEk = b6;
        arzArr[i6] = b6;
        int i7 = i6 + 1;
        C5663aRz b7 = C5640aRc.m17844b(WeaponBuffType.class, "a1fd0f3cc2f3561e7a2a78bfc47023f3", i7);
        bEm = b7;
        arzArr[i7] = b7;
        int i8 = i7 + 1;
        _m_fields = arzArr;
        C1634Xv.m11725a((Object[]) AttributeBuffType._m_fields, (Object[]) _m_fields);
        int i9 = AttributeBuffType._m_methodCount;
        C2491fm[] fmVarArr = new C2491fm[(i9 + 16)];
        C2491fm a = C4105zY.m41624a(WeaponBuffType.class, "38da75789d3453835af0e08138573fd7", i9);
        cHL = a;
        fmVarArr[i9] = a;
        int i10 = i9 + 1;
        C2491fm a2 = C4105zY.m41624a(WeaponBuffType.class, "b3dd0af044654351f20139c7430419fb", i10);
        cHM = a2;
        fmVarArr[i10] = a2;
        int i11 = i10 + 1;
        C2491fm a3 = C4105zY.m41624a(WeaponBuffType.class, "8a37a19f2c5913c7f76cd6bdb7ab0971", i11);
        cHN = a3;
        fmVarArr[i11] = a3;
        int i12 = i11 + 1;
        C2491fm a4 = C4105zY.m41624a(WeaponBuffType.class, "b85bfa5b5b83a7135d045f82fa36a2f4", i12);
        cHO = a4;
        fmVarArr[i12] = a4;
        int i13 = i12 + 1;
        C2491fm a5 = C4105zY.m41624a(WeaponBuffType.class, "fb45b5a91b68a953020999dd2af803c5", i13);
        cHP = a5;
        fmVarArr[i13] = a5;
        int i14 = i13 + 1;
        C2491fm a6 = C4105zY.m41624a(WeaponBuffType.class, "a8ef376df2ebdb67d7b9d9150d3300d0", i14);
        cHQ = a6;
        fmVarArr[i14] = a6;
        int i15 = i14 + 1;
        C2491fm a7 = C4105zY.m41624a(WeaponBuffType.class, "9217ca18a15a44ce0aaa8abbe8211550", i15);
        cHR = a7;
        fmVarArr[i15] = a7;
        int i16 = i15 + 1;
        C2491fm a8 = C4105zY.m41624a(WeaponBuffType.class, "a12230fa944b3b6888efd04130c9b538", i16);
        cHS = a8;
        fmVarArr[i16] = a8;
        int i17 = i16 + 1;
        C2491fm a9 = C4105zY.m41624a(WeaponBuffType.class, "d63dca538ddf09819dce0deb9d683640", i17);
        cHT = a9;
        fmVarArr[i17] = a9;
        int i18 = i17 + 1;
        C2491fm a10 = C4105zY.m41624a(WeaponBuffType.class, "a32f07c69e2858530a5a31c2abe10170", i18);
        cHU = a10;
        fmVarArr[i18] = a10;
        int i19 = i18 + 1;
        C2491fm a11 = C4105zY.m41624a(WeaponBuffType.class, "847bb43e40eb754eb240393d78644bd2", i19);
        cHV = a11;
        fmVarArr[i19] = a11;
        int i20 = i19 + 1;
        C2491fm a12 = C4105zY.m41624a(WeaponBuffType.class, "dfb58f09e870cc15114b7718a3766cfe", i20);
        cHW = a12;
        fmVarArr[i20] = a12;
        int i21 = i20 + 1;
        C2491fm a13 = C4105zY.m41624a(WeaponBuffType.class, "4659ff57b61082c4cbb0cc7a569ed6ab", i21);
        fSr = a13;
        fmVarArr[i21] = a13;
        int i22 = i21 + 1;
        C2491fm a14 = C4105zY.m41624a(WeaponBuffType.class, "c3f619bf2d93d2fa6d25c1016f75cc64", i22);
        fSs = a14;
        fmVarArr[i22] = a14;
        int i23 = i22 + 1;
        C2491fm a15 = C4105zY.m41624a(WeaponBuffType.class, "e95c96b6ac7a62dae9eecc0c649c8626", i23);
        fSt = a15;
        fmVarArr[i23] = a15;
        int i24 = i23 + 1;
        C2491fm a16 = C4105zY.m41624a(WeaponBuffType.class, "aa0e64068d23afe4d120288cda0e1537", i24);
        f4698dN = a16;
        fmVarArr[i24] = a16;
        int i25 = i24 + 1;
        _m_methods = fmVarArr;
        C1634Xv.m11725a((Object[]) AttributeBuffType._m_methods, (Object[]) _m_methods);
        ___iScriptClass = aUO.m18566a(WeaponBuffType.class, C5995aeT.class, _m_fields, _m_methods);
    }

    /* renamed from: B */
    private void m22897B(C3892wO wOVar) {
        bFf().mo5608dq().mo3197f(bEc, wOVar);
    }

    /* renamed from: C */
    private void m22898C(C3892wO wOVar) {
        bFf().mo5608dq().mo3197f(f4700uS, wOVar);
    }

    /* renamed from: D */
    private void m22899D(C3892wO wOVar) {
        bFf().mo5608dq().mo3197f(f4699uN, wOVar);
    }

    /* renamed from: E */
    private void m22900E(C3892wO wOVar) {
        bFf().mo5608dq().mo3197f(bEg, wOVar);
    }

    /* renamed from: F */
    private void m22901F(C3892wO wOVar) {
        bFf().mo5608dq().mo3197f(bEi, wOVar);
    }

    private C3892wO amE() {
        return (C3892wO) bFf().mo5608dq().mo3214p(bEc);
    }

    private C3892wO amF() {
        return (C3892wO) bFf().mo5608dq().mo3214p(f4700uS);
    }

    private C3892wO amG() {
        return (C3892wO) bFf().mo5608dq().mo3214p(f4699uN);
    }

    private C3892wO amH() {
        return (C3892wO) bFf().mo5608dq().mo3214p(bEg);
    }

    private C3892wO amI() {
        return (C3892wO) bFf().mo5608dq().mo3214p(bEi);
    }

    private boolean amJ() {
        return bFf().mo5608dq().mo3201h(bEk);
    }

    private boolean amK() {
        return bFf().mo5608dq().mo3201h(bEm);
    }

    /* renamed from: bC */
    private void m22909bC(boolean z) {
        bFf().mo5608dq().mo3153a(bEk, z);
    }

    /* renamed from: bD */
    private void m22910bD(boolean z) {
        bFf().mo5608dq().mo3153a(bEm, z);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Fire Rate")
    /* renamed from: L */
    public void mo14084L(C3892wO wOVar) {
        switch (bFf().mo6893i(cHM)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, cHM, new Object[]{wOVar}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, cHM, new Object[]{wOVar}));
                break;
        }
        m22902K(wOVar);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Speed")
    /* renamed from: N */
    public void mo14085N(C3892wO wOVar) {
        switch (bFf().mo6893i(cHO)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, cHO, new Object[]{wOVar}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, cHO, new Object[]{wOVar}));
                break;
        }
        m22903M(wOVar);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Range")
    /* renamed from: P */
    public void mo14086P(C3892wO wOVar) {
        switch (bFf().mo6893i(cHQ)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, cHQ, new Object[]{wOVar}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, cHQ, new Object[]{wOVar}));
                break;
        }
        m22904O(wOVar);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Shield Damage")
    /* renamed from: R */
    public void mo14087R(C3892wO wOVar) {
        switch (bFf().mo6893i(cHS)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, cHS, new Object[]{wOVar}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, cHS, new Object[]{wOVar}));
                break;
        }
        m22905Q(wOVar);
    }

    /* renamed from: T */
    public C6494any mo11T() {
        return ___iScriptClass;
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Hull Damage")
    /* renamed from: T */
    public void mo14088T(C3892wO wOVar) {
        switch (bFf().mo6893i(cHU)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, cHU, new Object[]{wOVar}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, cHU, new Object[]{wOVar}));
                break;
        }
        m22906S(wOVar);
    }

    /* renamed from: U */
    public C2491fm[] mo12U() {
        return _m_methods;
    }

    /* renamed from: W */
    public Object mo13W() {
        return new C5995aeT(this);
    }

    /* renamed from: a */
    public Object mo14a(C0495Gr gr) {
        Object[] args = gr.getArgs();
        switch (gr.mo2417hq() - AttributeBuffType._m_methodCount) {
            case 0:
                return aGx();
            case 1:
                m22902K((C3892wO) args[0]);
                return null;
            case 2:
                return aGz();
            case 3:
                m22903M((C3892wO) args[0]);
                return null;
            case 4:
                return aGB();
            case 5:
                m22904O((C3892wO) args[0]);
                return null;
            case 6:
                return aGD();
            case 7:
                m22905Q((C3892wO) args[0]);
                return null;
            case 8:
                return aGF();
            case 9:
                m22906S((C3892wO) args[0]);
                return null;
            case 10:
                return new Boolean(aGH());
            case 11:
                m22911cA(((Boolean) args[0]).booleanValue());
                return null;
            case 12:
                return new Boolean(cfH());
            case 13:
                m22912fb(((Boolean) args[0]).booleanValue());
                return null;
            case 14:
                return cfJ();
            case 15:
                return m22908aT();
            default:
                return super.mo14a(gr);
        }
    }

    /* renamed from: a */
    public void mo15a(Collection collection, C0495Gr gr) {
        super.mo15a(collection, gr);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Speed")
    public C3892wO aGA() {
        switch (bFf().mo6893i(cHN)) {
            case 0:
                return null;
            case 2:
                return (C3892wO) bFf().mo5606d(new aCE(this, cHN, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, cHN, new Object[0]));
                break;
        }
        return aGz();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Range")
    public C3892wO aGC() {
        switch (bFf().mo6893i(cHP)) {
            case 0:
                return null;
            case 2:
                return (C3892wO) bFf().mo5606d(new aCE(this, cHP, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, cHP, new Object[0]));
                break;
        }
        return aGB();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Shield Damage")
    public C3892wO aGE() {
        switch (bFf().mo6893i(cHR)) {
            case 0:
                return null;
            case 2:
                return (C3892wO) bFf().mo5606d(new aCE(this, cHR, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, cHR, new Object[0]));
                break;
        }
        return aGD();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Hull Damage")
    public C3892wO aGG() {
        switch (bFf().mo6893i(cHT)) {
            case 0:
                return null;
            case 2:
                return (C3892wO) bFf().mo5606d(new aCE(this, cHT, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, cHT, new Object[0]));
                break;
        }
        return aGF();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Double Shot")
    public boolean aGI() {
        switch (bFf().mo6893i(cHV)) {
            case 0:
                return false;
            case 2:
                return ((Boolean) bFf().mo5606d(new aCE(this, cHV, new Object[0]))).booleanValue();
            case 3:
                bFf().mo5606d(new aCE(this, cHV, new Object[0]));
                break;
        }
        return aGH();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Fire Rate")
    public C3892wO aGy() {
        switch (bFf().mo6893i(cHL)) {
            case 0:
                return null;
            case 2:
                return (C3892wO) bFf().mo5606d(new aCE(this, cHL, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, cHL, new Object[0]));
                break;
        }
        return aGx();
    }

    /* access modifiers changed from: protected */
    /* renamed from: aU */
    public <T extends aDJ> T mo745aU() {
        switch (bFf().mo6893i(f4698dN)) {
            case 0:
                return null;
            case 2:
                return (aDJ) bFf().mo5606d(new aCE(this, f4698dN, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f4698dN, new Object[0]));
                break;
        }
        return m22908aT();
    }

    /* renamed from: c */
    public C5663aRz[] mo25c() {
        return _m_fields;
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Double Shot")
    /* renamed from: cB */
    public void mo14095cB(boolean z) {
        switch (bFf().mo6893i(cHW)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, cHW, new Object[]{new Boolean(z)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, cHW, new Object[]{new Boolean(z)}));
                break;
        }
        m22911cA(z);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Jammed")
    public boolean cfI() {
        switch (bFf().mo6893i(fSr)) {
            case 0:
                return false;
            case 2:
                return ((Boolean) bFf().mo5606d(new aCE(this, fSr, new Object[0]))).booleanValue();
            case 3:
                bFf().mo5606d(new aCE(this, fSr, new Object[0]));
                break;
        }
        return cfH();
    }

    public WeaponBuff cfK() {
        switch (bFf().mo6893i(fSt)) {
            case 0:
                return null;
            case 2:
                return (WeaponBuff) bFf().mo5606d(new aCE(this, fSt, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, fSt, new Object[0]));
                break;
        }
        return cfJ();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Jammed")
    /* renamed from: fc */
    public void mo14098fc(boolean z) {
        switch (bFf().mo6893i(fSs)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, fSs, new Object[]{new Boolean(z)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, fSs, new Object[]{new Boolean(z)}));
                break;
        }
        m22912fb(z);
    }

    /* renamed from: S */
    public void mo10S() {
        super.mo10S();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Fire Rate")
    @C0064Am(aul = "38da75789d3453835af0e08138573fd7", aum = 0)
    private C3892wO aGx() {
        return amE();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Fire Rate")
    @C0064Am(aul = "b3dd0af044654351f20139c7430419fb", aum = 0)
    /* renamed from: K */
    private void m22902K(C3892wO wOVar) {
        m22897B(wOVar);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Speed")
    @C0064Am(aul = "8a37a19f2c5913c7f76cd6bdb7ab0971", aum = 0)
    private C3892wO aGz() {
        return amF();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Speed")
    @C0064Am(aul = "b85bfa5b5b83a7135d045f82fa36a2f4", aum = 0)
    /* renamed from: M */
    private void m22903M(C3892wO wOVar) {
        m22898C(wOVar);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Range")
    @C0064Am(aul = "fb45b5a91b68a953020999dd2af803c5", aum = 0)
    private C3892wO aGB() {
        return amG();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Range")
    @C0064Am(aul = "a8ef376df2ebdb67d7b9d9150d3300d0", aum = 0)
    /* renamed from: O */
    private void m22904O(C3892wO wOVar) {
        m22899D(wOVar);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Shield Damage")
    @C0064Am(aul = "9217ca18a15a44ce0aaa8abbe8211550", aum = 0)
    private C3892wO aGD() {
        return amH();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Shield Damage")
    @C0064Am(aul = "a12230fa944b3b6888efd04130c9b538", aum = 0)
    /* renamed from: Q */
    private void m22905Q(C3892wO wOVar) {
        m22900E(wOVar);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Hull Damage")
    @C0064Am(aul = "d63dca538ddf09819dce0deb9d683640", aum = 0)
    private C3892wO aGF() {
        return amI();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Hull Damage")
    @C0064Am(aul = "a32f07c69e2858530a5a31c2abe10170", aum = 0)
    /* renamed from: S */
    private void m22906S(C3892wO wOVar) {
        m22901F(wOVar);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Double Shot")
    @C0064Am(aul = "847bb43e40eb754eb240393d78644bd2", aum = 0)
    private boolean aGH() {
        return amJ();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Double Shot")
    @C0064Am(aul = "dfb58f09e870cc15114b7718a3766cfe", aum = 0)
    /* renamed from: cA */
    private void m22911cA(boolean z) {
        m22909bC(z);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Jammed")
    @C0064Am(aul = "4659ff57b61082c4cbb0cc7a569ed6ab", aum = 0)
    private boolean cfH() {
        return amK();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Jammed")
    @C0064Am(aul = "c3f619bf2d93d2fa6d25c1016f75cc64", aum = 0)
    /* renamed from: fb */
    private void m22912fb(boolean z) {
        m22910bD(z);
    }

    @C0064Am(aul = "e95c96b6ac7a62dae9eecc0c649c8626", aum = 0)
    private WeaponBuff cfJ() {
        return (WeaponBuff) mo745aU();
    }

    @C0064Am(aul = "aa0e64068d23afe4d120288cda0e1537", aum = 0)
    /* renamed from: aT */
    private <T extends aDJ> T m22908aT() {
        T t = (WeaponBuff) bFf().mo6865M(WeaponBuff.class);
        t.mo22713a(this);
        return t;
    }
}
