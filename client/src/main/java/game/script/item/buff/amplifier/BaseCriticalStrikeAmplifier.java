package game.script.item.buff.amplifier;

import game.network.message.externalizable.aCE;
import game.network.message.serializable.C1556Wo;
import game.script.damage.DamageType;
import game.script.item.AmplifierType;
import logic.baa.C1616Xf;
import logic.baa.C1634Xv;
import logic.baa.C5511aMd;
import logic.baa.C6485anp;
import logic.data.mbean.C5346aFu;
import logic.res.code.C5663aRz;
import logic.res.code.C6494any;
import logic.res.html.C2491fm;
import p001a.*;

import java.util.Collection;

@C5511aMd
@C6485anp
/* renamed from: a.oS */
/* compiled from: a */
public abstract class BaseCriticalStrikeAmplifier extends AmplifierType implements C1616Xf {
    public static final int _m_fieldCount = 0;
    public static final C5663aRz[] _m_fields = null;
    public static final int _m_methodCount = 0;
    public static final C2491fm[] _m_methods = null;
    public static final C2491fm aQB = null;
    public static final long serialVersionUID = 0;
    public static C6494any ___iScriptClass;

    static {
        m36725V();
    }

    public BaseCriticalStrikeAmplifier() {
        super((C5540aNg) null);
        super.mo10S();
    }

    public BaseCriticalStrikeAmplifier(C5540aNg ang) {
        super(ang);
    }

    /* renamed from: V */
    static void m36725V() {
        serialVersionUID = (long) 1;
        _m_fieldCount = AmplifierType._m_fieldCount + 0;
        _m_methodCount = AmplifierType._m_methodCount + 1;
        _m_fields = new C5663aRz[(AmplifierType._m_fieldCount + 0)];
        C1634Xv.m11725a((Object[]) AmplifierType._m_fields, (Object[]) _m_fields);
        int i = AmplifierType._m_methodCount;
        C2491fm[] fmVarArr = new C2491fm[(i + 1)];
        C2491fm a = C4105zY.m41624a(BaseCriticalStrikeAmplifier.class, "c68eb433b69eee793e91c2d8efa63a65", i);
        aQB = a;
        fmVarArr[i] = a;
        int i2 = i + 1;
        _m_methods = fmVarArr;
        C1634Xv.m11725a((Object[]) AmplifierType._m_methods, (Object[]) _m_methods);
        ___iScriptClass = aUO.m18566a(BaseCriticalStrikeAmplifier.class, C5346aFu.class, _m_fields, _m_methods);
    }

    @C0064Am(aul = "c68eb433b69eee793e91c2d8efa63a65", aum = 0)
    /* renamed from: UC */
    private C1556Wo<DamageType, C3892wO> m36724UC() {
        throw new aWi(new aCE(this, aQB, new Object[0]));
    }

    /* renamed from: T */
    public C6494any mo11T() {
        return ___iScriptClass;
    }

    /* renamed from: U */
    public C2491fm[] mo12U() {
        return _m_methods;
    }

    /* renamed from: UD */
    public C1556Wo<DamageType, C3892wO> mo12332UD() {
        switch (bFf().mo6893i(aQB)) {
            case 0:
                return null;
            case 2:
                return (C1556Wo) bFf().mo5606d(new aCE(this, aQB, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, aQB, new Object[0]));
                break;
        }
        return m36724UC();
    }

    /* renamed from: W */
    public Object mo13W() {
        return new C5346aFu(this);
    }

    /* renamed from: a */
    public Object mo14a(C0495Gr gr) {
        gr.getArgs();
        switch (gr.mo2417hq() - AmplifierType._m_methodCount) {
            case 0:
                return m36724UC();
            default:
                return super.mo14a(gr);
        }
    }

    /* renamed from: a */
    public void mo15a(Collection collection, C0495Gr gr) {
        super.mo15a(collection, gr);
    }

    /* renamed from: c */
    public C5663aRz[] mo25c() {
        return _m_fields;
    }

    /* renamed from: S */
    public void mo10S() {
        super.mo10S();
        for (DamageType put : ala().aLs()) {
            mo12332UD().put(put, new C3892wO());
        }
    }
}
