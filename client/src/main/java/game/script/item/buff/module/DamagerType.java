package game.script.item.buff.module;

import game.network.message.externalizable.aCE;
import game.script.damage.DamageType;
import logic.baa.*;
import logic.data.mbean.C1576XA;
import logic.res.code.C5640aRc;
import logic.res.code.C5663aRz;
import logic.res.code.C6494any;
import logic.res.html.C2491fm;
import p001a.*;

import java.util.Collection;

@C0566Hp
@C5829abJ("1.1.2")
@C6485anp
@C5511aMd
/* renamed from: a.aPS */
/* compiled from: a */
public class DamagerType extends AttributeBuffType implements C1616Xf {
    public static final int _m_fieldCount = 0;
    public static final C5663aRz[] _m_fields = null;
    public static final int _m_methodCount = 0;
    public static final C2491fm[] _m_methods = null;
    public static final C5663aRz bEg = null;
    public static final C5663aRz bEi = null;
    /* renamed from: dN */
    public static final C2491fm f3521dN = null;
    public static final C5663aRz iCV = null;
    public static final C5663aRz iCW = null;
    public static final C2491fm iCX = null;
    public static final C2491fm iCY = null;
    public static final C2491fm iCZ = null;
    public static final C2491fm iDa = null;
    public static final C2491fm iDb = null;
    public static final C2491fm iDc = null;
    public static final C2491fm iDd = null;
    public static final C2491fm iDe = null;
    public static final C2491fm iDf = null;
    public static final long serialVersionUID = 0;
    public static C6494any ___iScriptClass;
    @C0064Am(aul = "5d0eb03ad1a1bf4e777567a3727b9ab5", aum = 0)
    private static float eJl;
    @C0064Am(aul = "211da7ea8b59c239b74d366b204da661", aum = 1)
    private static float eJm;
    @C0064Am(aul = "9c767c8c92453a213a2f5b298b7e73a2", aum = 2)
    private static DamageType eJn;
    @C0064Am(aul = "be4de1b9fd63343c92355a8d453386ea", aum = 3)
    private static DamageType eJo;

    static {
        m17183V();
    }

    public DamagerType() {
        super((C5540aNg) null);
        super.mo10S();
    }

    public DamagerType(C5540aNg ang) {
        super(ang);
    }

    /* renamed from: V */
    static void m17183V() {
        serialVersionUID = (long) 1;
        _m_fieldCount = AttributeBuffType._m_fieldCount + 4;
        _m_methodCount = AttributeBuffType._m_methodCount + 10;
        int i = AttributeBuffType._m_fieldCount;
        C5663aRz[] arzArr = new C5663aRz[(i + 4)];
        C5663aRz b = C5640aRc.m17844b(DamagerType.class, "5d0eb03ad1a1bf4e777567a3727b9ab5", i);
        bEg = b;
        arzArr[i] = b;
        int i2 = i + 1;
        C5663aRz b2 = C5640aRc.m17844b(DamagerType.class, "211da7ea8b59c239b74d366b204da661", i2);
        bEi = b2;
        arzArr[i2] = b2;
        int i3 = i2 + 1;
        C5663aRz b3 = C5640aRc.m17844b(DamagerType.class, "9c767c8c92453a213a2f5b298b7e73a2", i3);
        iCV = b3;
        arzArr[i3] = b3;
        int i4 = i3 + 1;
        C5663aRz b4 = C5640aRc.m17844b(DamagerType.class, "be4de1b9fd63343c92355a8d453386ea", i4);
        iCW = b4;
        arzArr[i4] = b4;
        int i5 = i4 + 1;
        _m_fields = arzArr;
        C1634Xv.m11725a((Object[]) AttributeBuffType._m_fields, (Object[]) _m_fields);
        int i6 = AttributeBuffType._m_methodCount;
        C2491fm[] fmVarArr = new C2491fm[(i6 + 10)];
        C2491fm a = C4105zY.m41624a(DamagerType.class, "3e7a2d01523e0efd1f9bbd41918e8310", i6);
        iCX = a;
        fmVarArr[i6] = a;
        int i7 = i6 + 1;
        C2491fm a2 = C4105zY.m41624a(DamagerType.class, "292155382b2a68501462856ae0fd56b1", i7);
        iCY = a2;
        fmVarArr[i7] = a2;
        int i8 = i7 + 1;
        C2491fm a3 = C4105zY.m41624a(DamagerType.class, "9f476de637e33536b62d73d7daf8023a", i8);
        iCZ = a3;
        fmVarArr[i8] = a3;
        int i9 = i8 + 1;
        C2491fm a4 = C4105zY.m41624a(DamagerType.class, "b516237357fc2a818dbea64211a530b1", i9);
        iDa = a4;
        fmVarArr[i9] = a4;
        int i10 = i9 + 1;
        C2491fm a5 = C4105zY.m41624a(DamagerType.class, "dd592bfca6ada315c910237ff8e6b9c5", i10);
        iDb = a5;
        fmVarArr[i10] = a5;
        int i11 = i10 + 1;
        C2491fm a6 = C4105zY.m41624a(DamagerType.class, "20cf32cc17b6d5951b031b0c93a8dc5f", i11);
        iDc = a6;
        fmVarArr[i11] = a6;
        int i12 = i11 + 1;
        C2491fm a7 = C4105zY.m41624a(DamagerType.class, "07a6842ab0b2c1f06734f3a890584b8d", i12);
        iDd = a7;
        fmVarArr[i12] = a7;
        int i13 = i12 + 1;
        C2491fm a8 = C4105zY.m41624a(DamagerType.class, "da88ec644dd58c2cb656ad2cf501c882", i13);
        iDe = a8;
        fmVarArr[i13] = a8;
        int i14 = i13 + 1;
        C2491fm a9 = C4105zY.m41624a(DamagerType.class, "7cb801809c5ab33155ab31f5b506c90e", i14);
        iDf = a9;
        fmVarArr[i14] = a9;
        int i15 = i14 + 1;
        C2491fm a10 = C4105zY.m41624a(DamagerType.class, "3a98a33c33ad77837d1127b5090604b9", i15);
        f3521dN = a10;
        fmVarArr[i15] = a10;
        int i16 = i15 + 1;
        _m_methods = fmVarArr;
        C1634Xv.m11725a((Object[]) AttributeBuffType._m_methods, (Object[]) _m_methods);
        ___iScriptClass = aUO.m18566a(DamagerType.class, C1576XA.class, _m_fields, _m_methods);
    }

    /* renamed from: A */
    private void m17179A(DamageType fr) {
        bFf().mo5608dq().mo3197f(iCV, fr);
    }

    /* renamed from: B */
    private void m17180B(DamageType fr) {
        bFf().mo5608dq().mo3197f(iCW, fr);
    }

    private float dow() {
        return bFf().mo5608dq().mo3211m(bEg);
    }

    private float dox() {
        return bFf().mo5608dq().mo3211m(bEi);
    }

    private DamageType doy() {
        return (DamageType) bFf().mo5608dq().mo3214p(iCV);
    }

    private DamageType doz() {
        return (DamageType) bFf().mo5608dq().mo3214p(iCW);
    }

    /* renamed from: nG */
    private void m17185nG(float f) {
        bFf().mo5608dq().mo3150a(bEg, f);
    }

    /* renamed from: nH */
    private void m17186nH(float f) {
        bFf().mo5608dq().mo3150a(bEi, f);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Shield Damage Type")
    /* renamed from: D */
    public void mo10761D(DamageType fr) {
        switch (bFf().mo6893i(iDc)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, iDc, new Object[]{fr}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, iDc, new Object[]{fr}));
                break;
        }
        m17181C(fr);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Hull Damage Type")
    /* renamed from: F */
    public void mo10762F(DamageType fr) {
        switch (bFf().mo6893i(iDe)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, iDe, new Object[]{fr}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, iDe, new Object[]{fr}));
                break;
        }
        m17182E(fr);
    }

    /* renamed from: T */
    public C6494any mo11T() {
        return ___iScriptClass;
    }

    /* renamed from: U */
    public C2491fm[] mo12U() {
        return _m_methods;
    }

    /* renamed from: W */
    public Object mo13W() {
        return new C1576XA(this);
    }

    /* renamed from: a */
    public Object mo14a(C0495Gr gr) {
        Object[] args = gr.getArgs();
        switch (gr.mo2417hq() - AttributeBuffType._m_methodCount) {
            case 0:
                return new Float(doA());
            case 1:
                m17187nI(((Float) args[0]).floatValue());
                return null;
            case 2:
                return new Float(doC());
            case 3:
                m17188nK(((Float) args[0]).floatValue());
                return null;
            case 4:
                return doE();
            case 5:
                m17181C((DamageType) args[0]);
                return null;
            case 6:
                return doG();
            case 7:
                m17182E((DamageType) args[0]);
                return null;
            case 8:
                return doI();
            case 9:
                return m17184aT();
            default:
                return super.mo14a(gr);
        }
    }

    /* renamed from: a */
    public void mo15a(Collection collection, C0495Gr gr) {
        super.mo15a(collection, gr);
    }

    /* access modifiers changed from: protected */
    /* renamed from: aU */
    public <T extends aDJ> T mo745aU() {
        switch (bFf().mo6893i(f3521dN)) {
            case 0:
                return null;
            case 2:
                return (aDJ) bFf().mo5606d(new aCE(this, f3521dN, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f3521dN, new Object[0]));
                break;
        }
        return m17184aT();
    }

    /* renamed from: c */
    public C5663aRz[] mo25c() {
        return _m_fields;
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Shield Damage")
    public float doB() {
        switch (bFf().mo6893i(iCX)) {
            case 0:
                return 0.0f;
            case 2:
                return ((Float) bFf().mo5606d(new aCE(this, iCX, new Object[0]))).floatValue();
            case 3:
                bFf().mo5606d(new aCE(this, iCX, new Object[0]));
                break;
        }
        return doA();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Hull Damage")
    public float doD() {
        switch (bFf().mo6893i(iCZ)) {
            case 0:
                return 0.0f;
            case 2:
                return ((Float) bFf().mo5606d(new aCE(this, iCZ, new Object[0]))).floatValue();
            case 3:
                bFf().mo5606d(new aCE(this, iCZ, new Object[0]));
                break;
        }
        return doC();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Shield Damage Type")
    public DamageType doF() {
        switch (bFf().mo6893i(iDb)) {
            case 0:
                return null;
            case 2:
                return (DamageType) bFf().mo5606d(new aCE(this, iDb, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, iDb, new Object[0]));
                break;
        }
        return doE();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Hull Damage Type")
    public DamageType doH() {
        switch (bFf().mo6893i(iDd)) {
            case 0:
                return null;
            case 2:
                return (DamageType) bFf().mo5606d(new aCE(this, iDd, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, iDd, new Object[0]));
                break;
        }
        return doG();
    }

    public Damager doJ() {
        switch (bFf().mo6893i(iDf)) {
            case 0:
                return null;
            case 2:
                return (Damager) bFf().mo5606d(new aCE(this, iDf, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, iDf, new Object[0]));
                break;
        }
        return doI();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Shield Damage")
    /* renamed from: nJ */
    public void mo10768nJ(float f) {
        switch (bFf().mo6893i(iCY)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, iCY, new Object[]{new Float(f)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, iCY, new Object[]{new Float(f)}));
                break;
        }
        m17187nI(f);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Hull Damage")
    /* renamed from: nL */
    public void mo10769nL(float f) {
        switch (bFf().mo6893i(iDa)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, iDa, new Object[]{new Float(f)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, iDa, new Object[]{new Float(f)}));
                break;
        }
        m17188nK(f);
    }

    /* renamed from: S */
    public void mo10S() {
        super.mo10S();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Shield Damage")
    @C0064Am(aul = "3e7a2d01523e0efd1f9bbd41918e8310", aum = 0)
    private float doA() {
        return dow();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Shield Damage")
    @C0064Am(aul = "292155382b2a68501462856ae0fd56b1", aum = 0)
    /* renamed from: nI */
    private void m17187nI(float f) {
        m17185nG(f);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Hull Damage")
    @C0064Am(aul = "9f476de637e33536b62d73d7daf8023a", aum = 0)
    private float doC() {
        return dox();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Hull Damage")
    @C0064Am(aul = "b516237357fc2a818dbea64211a530b1", aum = 0)
    /* renamed from: nK */
    private void m17188nK(float f) {
        m17186nH(f);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Shield Damage Type")
    @C0064Am(aul = "dd592bfca6ada315c910237ff8e6b9c5", aum = 0)
    private DamageType doE() {
        return doy();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Shield Damage Type")
    @C0064Am(aul = "20cf32cc17b6d5951b031b0c93a8dc5f", aum = 0)
    /* renamed from: C */
    private void m17181C(DamageType fr) {
        m17179A(fr);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Hull Damage Type")
    @C0064Am(aul = "07a6842ab0b2c1f06734f3a890584b8d", aum = 0)
    private DamageType doG() {
        return doz();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Hull Damage Type")
    @C0064Am(aul = "da88ec644dd58c2cb656ad2cf501c882", aum = 0)
    /* renamed from: E */
    private void m17182E(DamageType fr) {
        m17180B(fr);
    }

    @C0064Am(aul = "7cb801809c5ab33155ab31f5b506c90e", aum = 0)
    private Damager doI() {
        return (Damager) mo745aU();
    }

    @C0064Am(aul = "3a98a33c33ad77837d1127b5090604b9", aum = 0)
    /* renamed from: aT */
    private <T extends aDJ> T m17184aT() {
        T t = (Damager) bFf().mo6865M(Damager.class);
        t.mo11220a(this);
        return t;
    }
}
