package game.script.item.buff.amplifier;

import game.network.message.externalizable.aCE;
import game.script.item.AmplifierType;
import logic.baa.*;
import logic.data.mbean.C2656iE;
import logic.res.code.C5640aRc;
import logic.res.code.C5663aRz;
import logic.res.code.C6494any;
import logic.res.html.C2491fm;
import p001a.*;

import java.util.Collection;

@C0566Hp
@C5829abJ("2.0.1")
@C6485anp
@C5511aMd
/* renamed from: a.acW  reason: case insensitive filesystem */
/* compiled from: a */
public class CruiseSpeedAmplifierType extends AmplifierType implements C1616Xf {

    /* renamed from: VE */
    public static final C2491fm f4228VE = null;

    /* renamed from: VF */
    public static final C2491fm f4229VF = null;

    /* renamed from: VG */
    public static final C2491fm f4230VG = null;

    /* renamed from: VH */
    public static final C2491fm f4231VH = null;

    /* renamed from: VI */
    public static final C2491fm f4232VI = null;

    /* renamed from: VJ */
    public static final C2491fm f4233VJ = null;

    /* renamed from: VK */
    public static final C2491fm f4234VK = null;

    /* renamed from: VL */
    public static final C2491fm f4235VL = null;
    /* renamed from: Vt */
    public static final C5663aRz f4237Vt = null;
    /* renamed from: Vv */
    public static final C5663aRz f4239Vv = null;
    /* renamed from: Vx */
    public static final C5663aRz f4241Vx = null;
    /* renamed from: Vz */
    public static final C5663aRz f4243Vz = null;
    public static final int _m_fieldCount = 0;
    public static final C5663aRz[] _m_fields = null;
    public static final int _m_methodCount = 0;
    public static final C2491fm[] _m_methods = null;
    public static final C5663aRz atQ = null;
    public static final C5663aRz atT = null;
    /* renamed from: dN */
    public static final C2491fm f4246dN = null;
    public static final C2491fm eUO = null;
    public static final C2491fm eUP = null;
    public static final C2491fm eUS = null;
    public static final C2491fm eUT = null;
    public static final C2491fm ffT = null;
    public static final long serialVersionUID = 0;
    public static C6494any ___iScriptClass;
    @C0064Am(aul = "fa2538493a59c809f13552f4d5de8222", aum = 0)

    /* renamed from: Vs */
    private static C3892wO f4236Vs;
    @C0064Am(aul = "112690eaa6aa0779a1f7f251420cf93d", aum = 1)

    /* renamed from: Vu */
    private static C3892wO f4238Vu;
    @C0064Am(aul = "cb4486be44ee553ee886d62682bad125", aum = 2)

    /* renamed from: Vw */
    private static C3892wO f4240Vw;
    @C0064Am(aul = "2e5e8407c127f79923ad52859fda599c", aum = 3)

    /* renamed from: Vy */
    private static C3892wO f4242Vy;
    @C0064Am(aul = "43c372a8b928b0afd43e816032354d61", aum = 4)

    /* renamed from: Yu */
    private static C3892wO f4244Yu;
    @C0064Am(aul = "e35b1b356e502743c81bfc406ba72728", aum = 5)

    /* renamed from: Yv */
    private static C3892wO f4245Yv;

    static {
        m20395V();
    }

    public CruiseSpeedAmplifierType() {
        super((C5540aNg) null);
        super.mo10S();
    }

    public CruiseSpeedAmplifierType(C5540aNg ang) {
        super(ang);
    }

    /* renamed from: V */
    static void m20395V() {
        serialVersionUID = (long) 1;
        _m_fieldCount = AmplifierType._m_fieldCount + 6;
        _m_methodCount = AmplifierType._m_methodCount + 14;
        int i = AmplifierType._m_fieldCount;
        C5663aRz[] arzArr = new C5663aRz[(i + 6)];
        C5663aRz b = C5640aRc.m17844b(CruiseSpeedAmplifierType.class, "fa2538493a59c809f13552f4d5de8222", i);
        f4237Vt = b;
        arzArr[i] = b;
        int i2 = i + 1;
        C5663aRz b2 = C5640aRc.m17844b(CruiseSpeedAmplifierType.class, "112690eaa6aa0779a1f7f251420cf93d", i2);
        f4239Vv = b2;
        arzArr[i2] = b2;
        int i3 = i2 + 1;
        C5663aRz b3 = C5640aRc.m17844b(CruiseSpeedAmplifierType.class, "cb4486be44ee553ee886d62682bad125", i3);
        f4241Vx = b3;
        arzArr[i3] = b3;
        int i4 = i3 + 1;
        C5663aRz b4 = C5640aRc.m17844b(CruiseSpeedAmplifierType.class, "2e5e8407c127f79923ad52859fda599c", i4);
        f4243Vz = b4;
        arzArr[i4] = b4;
        int i5 = i4 + 1;
        C5663aRz b5 = C5640aRc.m17844b(CruiseSpeedAmplifierType.class, "43c372a8b928b0afd43e816032354d61", i5);
        atQ = b5;
        arzArr[i5] = b5;
        int i6 = i5 + 1;
        C5663aRz b6 = C5640aRc.m17844b(CruiseSpeedAmplifierType.class, "e35b1b356e502743c81bfc406ba72728", i6);
        atT = b6;
        arzArr[i6] = b6;
        int i7 = i6 + 1;
        _m_fields = arzArr;
        C1634Xv.m11725a((Object[]) AmplifierType._m_fields, (Object[]) _m_fields);
        int i8 = AmplifierType._m_methodCount;
        C2491fm[] fmVarArr = new C2491fm[(i8 + 14)];
        C2491fm a = C4105zY.m41624a(CruiseSpeedAmplifierType.class, "4c70a867e8d5ad9052e8ecfd24ea3d22", i8);
        f4228VE = a;
        fmVarArr[i8] = a;
        int i9 = i8 + 1;
        C2491fm a2 = C4105zY.m41624a(CruiseSpeedAmplifierType.class, "13a963b63e9e372cd41134c8598eaab6", i9);
        f4229VF = a2;
        fmVarArr[i9] = a2;
        int i10 = i9 + 1;
        C2491fm a3 = C4105zY.m41624a(CruiseSpeedAmplifierType.class, "b42254e2a783b35adb5d2ae27270ce03", i10);
        f4230VG = a3;
        fmVarArr[i10] = a3;
        int i11 = i10 + 1;
        C2491fm a4 = C4105zY.m41624a(CruiseSpeedAmplifierType.class, "27426e5c4f10e5b599aec1b63820b1d7", i11);
        f4231VH = a4;
        fmVarArr[i11] = a4;
        int i12 = i11 + 1;
        C2491fm a5 = C4105zY.m41624a(CruiseSpeedAmplifierType.class, "995f707c3816709b06f6b1e36abb7da1", i12);
        f4232VI = a5;
        fmVarArr[i12] = a5;
        int i13 = i12 + 1;
        C2491fm a6 = C4105zY.m41624a(CruiseSpeedAmplifierType.class, "00447d28a4a4d9963c549bb8fa0f38dd", i13);
        f4233VJ = a6;
        fmVarArr[i13] = a6;
        int i14 = i13 + 1;
        C2491fm a7 = C4105zY.m41624a(CruiseSpeedAmplifierType.class, "5b008e74e1adedceecf64d678d4e15a8", i14);
        f4234VK = a7;
        fmVarArr[i14] = a7;
        int i15 = i14 + 1;
        C2491fm a8 = C4105zY.m41624a(CruiseSpeedAmplifierType.class, "974fb3f40830bf39f5d369078dd93824", i15);
        f4235VL = a8;
        fmVarArr[i15] = a8;
        int i16 = i15 + 1;
        C2491fm a9 = C4105zY.m41624a(CruiseSpeedAmplifierType.class, "2dd58a64da4750f31cd630a3db1cd408", i16);
        eUO = a9;
        fmVarArr[i16] = a9;
        int i17 = i16 + 1;
        C2491fm a10 = C4105zY.m41624a(CruiseSpeedAmplifierType.class, "86214025ef19f29339c54b1be9dfa1e8", i17);
        eUP = a10;
        fmVarArr[i17] = a10;
        int i18 = i17 + 1;
        C2491fm a11 = C4105zY.m41624a(CruiseSpeedAmplifierType.class, "4d1adc7c81655938692ae0e837c6a0b4", i18);
        eUS = a11;
        fmVarArr[i18] = a11;
        int i19 = i18 + 1;
        C2491fm a12 = C4105zY.m41624a(CruiseSpeedAmplifierType.class, "fe4e65ea1c2b1fac300b5d3b5921fdda", i19);
        eUT = a12;
        fmVarArr[i19] = a12;
        int i20 = i19 + 1;
        C2491fm a13 = C4105zY.m41624a(CruiseSpeedAmplifierType.class, "fed20b220fdf55ae4cc712f949e22575", i20);
        ffT = a13;
        fmVarArr[i20] = a13;
        int i21 = i20 + 1;
        C2491fm a14 = C4105zY.m41624a(CruiseSpeedAmplifierType.class, "87fbab174c42a174659c61c16c6871c9", i21);
        f4246dN = a14;
        fmVarArr[i21] = a14;
        int i22 = i21 + 1;
        _m_methods = fmVarArr;
        C1634Xv.m11725a((Object[]) AmplifierType._m_methods, (Object[]) _m_methods);
        ___iScriptClass = aUO.m18566a(CruiseSpeedAmplifierType.class, C2656iE.class, _m_fields, _m_methods);
    }

    /* renamed from: Kn */
    private C3892wO m20393Kn() {
        return (C3892wO) bFf().mo5608dq().mo3214p(atQ);
    }

    /* renamed from: Kp */
    private C3892wO m20394Kp() {
        return (C3892wO) bFf().mo5608dq().mo3214p(atT);
    }

    /* renamed from: d */
    private void m20399d(C3892wO wOVar) {
        bFf().mo5608dq().mo3197f(f4237Vt, wOVar);
    }

    /* renamed from: e */
    private void m20400e(C3892wO wOVar) {
        bFf().mo5608dq().mo3197f(f4239Vv, wOVar);
    }

    /* renamed from: f */
    private void m20401f(C3892wO wOVar) {
        bFf().mo5608dq().mo3197f(f4241Vx, wOVar);
    }

    /* renamed from: g */
    private void m20402g(C3892wO wOVar) {
        bFf().mo5608dq().mo3197f(f4243Vz, wOVar);
    }

    /* renamed from: s */
    private void m20407s(C3892wO wOVar) {
        bFf().mo5608dq().mo3197f(atQ, wOVar);
    }

    /* renamed from: u */
    private void m20408u(C3892wO wOVar) {
        bFf().mo5608dq().mo3197f(atT, wOVar);
    }

    /* renamed from: yE */
    private C3892wO m20409yE() {
        return (C3892wO) bFf().mo5608dq().mo3214p(f4237Vt);
    }

    /* renamed from: yF */
    private C3892wO m20410yF() {
        return (C3892wO) bFf().mo5608dq().mo3214p(f4239Vv);
    }

    /* renamed from: yG */
    private C3892wO m20411yG() {
        return (C3892wO) bFf().mo5608dq().mo3214p(f4241Vx);
    }

    /* renamed from: yH */
    private C3892wO m20412yH() {
        return (C3892wO) bFf().mo5608dq().mo3214p(f4243Vz);
    }

    /* renamed from: T */
    public C6494any mo11T() {
        return ___iScriptClass;
    }

    /* renamed from: U */
    public C2491fm[] mo12U() {
        return _m_methods;
    }

    /* renamed from: W */
    public Object mo13W() {
        return new C2656iE(this);
    }

    /* renamed from: a */
    public Object mo14a(C0495Gr gr) {
        Object[] args = gr.getArgs();
        switch (gr.mo2417hq() - AmplifierType._m_methodCount) {
            case 0:
                return m20413yK();
            case 1:
                m20403i((C3892wO) args[0]);
                return null;
            case 2:
                return m20414yM();
            case 3:
                m20404k((C3892wO) args[0]);
                return null;
            case 4:
                return m20415yO();
            case 5:
                m20405m((C3892wO) args[0]);
                return null;
            case 6:
                return m20416yQ();
            case 7:
                m20406o((C3892wO) args[0]);
                return null;
            case 8:
                return bLy();
            case 9:
                m20397aa((C3892wO) args[0]);
                return null;
            case 10:
                return bLC();
            case 11:
                m20398ae((C3892wO) args[0]);
                return null;
            case 12:
                return bQn();
            case 13:
                return m20396aT();
            default:
                return super.mo14a(gr);
        }
    }

    /* renamed from: a */
    public void mo15a(Collection collection, C0495Gr gr) {
        super.mo15a(collection, gr);
    }

    /* access modifiers changed from: protected */
    /* renamed from: aU */
    public <T extends aDJ> T mo745aU() {
        switch (bFf().mo6893i(f4246dN)) {
            case 0:
                return null;
            case 2:
                return (aDJ) bFf().mo5606d(new aCE(this, f4246dN, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f4246dN, new Object[0]));
                break;
        }
        return m20396aT();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Cooldown")
    /* renamed from: ab */
    public void mo12619ab(C3892wO wOVar) {
        switch (bFf().mo6893i(eUP)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, eUP, new Object[]{wOVar}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, eUP, new Object[]{wOVar}));
                break;
        }
        m20397aa(wOVar);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Warmup")
    /* renamed from: af */
    public void mo12620af(C3892wO wOVar) {
        switch (bFf().mo6893i(eUT)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, eUT, new Object[]{wOVar}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, eUT, new Object[]{wOVar}));
                break;
        }
        m20398ae(wOVar);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Warmup")
    public C3892wO bLD() {
        switch (bFf().mo6893i(eUS)) {
            case 0:
                return null;
            case 2:
                return (C3892wO) bFf().mo5606d(new aCE(this, eUS, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, eUS, new Object[0]));
                break;
        }
        return bLC();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Cooldown")
    public C3892wO bLz() {
        switch (bFf().mo6893i(eUO)) {
            case 0:
                return null;
            case 2:
                return (C3892wO) bFf().mo5606d(new aCE(this, eUO, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, eUO, new Object[0]));
                break;
        }
        return bLy();
    }

    public CruiseSpeedAmplifier bQo() {
        switch (bFf().mo6893i(ffT)) {
            case 0:
                return null;
            case 2:
                return (CruiseSpeedAmplifier) bFf().mo5606d(new aCE(this, ffT, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, ffT, new Object[0]));
                break;
        }
        return bQn();
    }

    /* renamed from: c */
    public C5663aRz[] mo25c() {
        return _m_fields;
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Linear Velocity")
    /* renamed from: j */
    public void mo12624j(C3892wO wOVar) {
        switch (bFf().mo6893i(f4229VF)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f4229VF, new Object[]{wOVar}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f4229VF, new Object[]{wOVar}));
                break;
        }
        m20403i(wOVar);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Linear Acceleration")
    /* renamed from: l */
    public void mo12625l(C3892wO wOVar) {
        switch (bFf().mo6893i(f4231VH)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f4231VH, new Object[]{wOVar}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f4231VH, new Object[]{wOVar}));
                break;
        }
        m20404k(wOVar);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Angular Velocity")
    /* renamed from: n */
    public void mo12626n(C3892wO wOVar) {
        switch (bFf().mo6893i(f4233VJ)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f4233VJ, new Object[]{wOVar}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f4233VJ, new Object[]{wOVar}));
                break;
        }
        m20405m(wOVar);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Angular Acceleration")
    /* renamed from: p */
    public void mo12627p(C3892wO wOVar) {
        switch (bFf().mo6893i(f4235VL)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f4235VL, new Object[]{wOVar}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f4235VL, new Object[]{wOVar}));
                break;
        }
        m20406o(wOVar);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Linear Velocity")
    /* renamed from: yL */
    public C3892wO mo12628yL() {
        switch (bFf().mo6893i(f4228VE)) {
            case 0:
                return null;
            case 2:
                return (C3892wO) bFf().mo5606d(new aCE(this, f4228VE, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f4228VE, new Object[0]));
                break;
        }
        return m20413yK();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Linear Acceleration")
    /* renamed from: yN */
    public C3892wO mo12629yN() {
        switch (bFf().mo6893i(f4230VG)) {
            case 0:
                return null;
            case 2:
                return (C3892wO) bFf().mo5606d(new aCE(this, f4230VG, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f4230VG, new Object[0]));
                break;
        }
        return m20414yM();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Angular Velocity")
    /* renamed from: yP */
    public C3892wO mo12630yP() {
        switch (bFf().mo6893i(f4232VI)) {
            case 0:
                return null;
            case 2:
                return (C3892wO) bFf().mo5606d(new aCE(this, f4232VI, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f4232VI, new Object[0]));
                break;
        }
        return m20415yO();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Angular Acceleration")
    /* renamed from: yR */
    public C3892wO mo12631yR() {
        switch (bFf().mo6893i(f4234VK)) {
            case 0:
                return null;
            case 2:
                return (C3892wO) bFf().mo5606d(new aCE(this, f4234VK, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f4234VK, new Object[0]));
                break;
        }
        return m20416yQ();
    }

    /* renamed from: S */
    public void mo10S() {
        super.mo10S();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Linear Velocity")
    @C0064Am(aul = "4c70a867e8d5ad9052e8ecfd24ea3d22", aum = 0)
    /* renamed from: yK */
    private C3892wO m20413yK() {
        return m20409yE();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Linear Velocity")
    @C0064Am(aul = "13a963b63e9e372cd41134c8598eaab6", aum = 0)
    /* renamed from: i */
    private void m20403i(C3892wO wOVar) {
        m20399d(wOVar);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Linear Acceleration")
    @C0064Am(aul = "b42254e2a783b35adb5d2ae27270ce03", aum = 0)
    /* renamed from: yM */
    private C3892wO m20414yM() {
        return m20410yF();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Linear Acceleration")
    @C0064Am(aul = "27426e5c4f10e5b599aec1b63820b1d7", aum = 0)
    /* renamed from: k */
    private void m20404k(C3892wO wOVar) {
        m20400e(wOVar);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Angular Velocity")
    @C0064Am(aul = "995f707c3816709b06f6b1e36abb7da1", aum = 0)
    /* renamed from: yO */
    private C3892wO m20415yO() {
        return m20411yG();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Angular Velocity")
    @C0064Am(aul = "00447d28a4a4d9963c549bb8fa0f38dd", aum = 0)
    /* renamed from: m */
    private void m20405m(C3892wO wOVar) {
        m20401f(wOVar);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Angular Acceleration")
    @C0064Am(aul = "5b008e74e1adedceecf64d678d4e15a8", aum = 0)
    /* renamed from: yQ */
    private C3892wO m20416yQ() {
        return m20412yH();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Angular Acceleration")
    @C0064Am(aul = "974fb3f40830bf39f5d369078dd93824", aum = 0)
    /* renamed from: o */
    private void m20406o(C3892wO wOVar) {
        m20402g(wOVar);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Cooldown")
    @C0064Am(aul = "2dd58a64da4750f31cd630a3db1cd408", aum = 0)
    private C3892wO bLy() {
        return m20393Kn();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Cooldown")
    @C0064Am(aul = "86214025ef19f29339c54b1be9dfa1e8", aum = 0)
    /* renamed from: aa */
    private void m20397aa(C3892wO wOVar) {
        m20407s(wOVar);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Warmup")
    @C0064Am(aul = "4d1adc7c81655938692ae0e837c6a0b4", aum = 0)
    private C3892wO bLC() {
        return m20394Kp();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Warmup")
    @C0064Am(aul = "fe4e65ea1c2b1fac300b5d3b5921fdda", aum = 0)
    /* renamed from: ae */
    private void m20398ae(C3892wO wOVar) {
        m20408u(wOVar);
    }

    @C0064Am(aul = "fed20b220fdf55ae4cc712f949e22575", aum = 0)
    private CruiseSpeedAmplifier bQn() {
        return (CruiseSpeedAmplifier) mo745aU();
    }

    @C0064Am(aul = "87fbab174c42a174659c61c16c6871c9", aum = 0)
    /* renamed from: aT */
    private <T extends aDJ> T m20396aT() {
        T t = (CruiseSpeedAmplifier) bFf().mo6865M(CruiseSpeedAmplifier.class);
        t.mo1956e((AmplifierType) this);
        return t;
    }
}
