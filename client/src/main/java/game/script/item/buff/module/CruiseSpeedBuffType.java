package game.script.item.buff.module;

import game.network.message.externalizable.aCE;
import logic.baa.*;
import logic.data.mbean.C5381aHd;
import logic.res.code.C5640aRc;
import logic.res.code.C5663aRz;
import logic.res.code.C6494any;
import logic.res.html.C2491fm;
import p001a.*;

import java.util.Collection;

@C0566Hp
@C5829abJ("1.1.2")
@C6485anp
@C5511aMd
/* renamed from: a.alT  reason: case insensitive filesystem */
/* compiled from: a */
public class CruiseSpeedBuffType extends AttributeBuffType implements C1616Xf {

    /* renamed from: VE */
    public static final C2491fm f4853VE = null;

    /* renamed from: VF */
    public static final C2491fm f4854VF = null;

    /* renamed from: VG */
    public static final C2491fm f4855VG = null;

    /* renamed from: VH */
    public static final C2491fm f4856VH = null;

    /* renamed from: VI */
    public static final C2491fm f4857VI = null;

    /* renamed from: VJ */
    public static final C2491fm f4858VJ = null;

    /* renamed from: VK */
    public static final C2491fm f4859VK = null;

    /* renamed from: VL */
    public static final C2491fm f4860VL = null;
    /* renamed from: Vt */
    public static final C5663aRz f4862Vt = null;
    /* renamed from: Vv */
    public static final C5663aRz f4864Vv = null;
    /* renamed from: Vx */
    public static final C5663aRz f4866Vx = null;
    /* renamed from: Vz */
    public static final C5663aRz f4868Vz = null;
    public static final int _m_fieldCount = 0;
    public static final C5663aRz[] _m_fields = null;
    public static final int _m_methodCount = 0;
    public static final C2491fm[] _m_methods = null;
    public static final C5663aRz atQ = null;
    public static final C5663aRz atT = null;
    public static final C5663aRz bEm = null;
    /* renamed from: dN */
    public static final C2491fm f4871dN = null;
    public static final C2491fm eUO = null;
    public static final C2491fm eUP = null;
    public static final C2491fm eUS = null;
    public static final C2491fm eUT = null;
    public static final C2491fm fSr = null;
    public static final C2491fm fSs = null;
    public static final C2491fm fYf = null;
    public static final long serialVersionUID = 0;
    public static C6494any ___iScriptClass;
    @C0064Am(aul = "5c77a98a6c65cd19bd5fe125d161d3f1", aum = 0)

    /* renamed from: Vs */
    private static C3892wO f4861Vs;
    @C0064Am(aul = "0c2631fed5b4bf4a405b46963662db11", aum = 1)

    /* renamed from: Vu */
    private static C3892wO f4863Vu;
    @C0064Am(aul = "b72deba8732ff9b8a9115b2c844d7318", aum = 2)

    /* renamed from: Vw */
    private static C3892wO f4865Vw;
    @C0064Am(aul = "99b61c91970d65103aa07a6979165c59", aum = 3)

    /* renamed from: Vy */
    private static C3892wO f4867Vy;
    @C0064Am(aul = "869681cf5cc92868951b78d04a3ddb02", aum = 4)

    /* renamed from: Yu */
    private static C3892wO f4869Yu;
    @C0064Am(aul = "0c7e175450ec2e11a930b9b02c2c237b", aum = 5)

    /* renamed from: Yv */
    private static C3892wO f4870Yv;
    @C0064Am(aul = "0b24a4526a3f99fd0907b005ef2ae7b7", aum = 6)
    private static boolean bEl;

    static {
        m23718V();
    }

    public CruiseSpeedBuffType() {
        super((C5540aNg) null);
        super.mo10S();
    }

    public CruiseSpeedBuffType(C5540aNg ang) {
        super(ang);
    }

    /* renamed from: V */
    static void m23718V() {
        serialVersionUID = (long) 1;
        _m_fieldCount = AttributeBuffType._m_fieldCount + 7;
        _m_methodCount = AttributeBuffType._m_methodCount + 16;
        int i = AttributeBuffType._m_fieldCount;
        C5663aRz[] arzArr = new C5663aRz[(i + 7)];
        C5663aRz b = C5640aRc.m17844b(CruiseSpeedBuffType.class, "5c77a98a6c65cd19bd5fe125d161d3f1", i);
        f4862Vt = b;
        arzArr[i] = b;
        int i2 = i + 1;
        C5663aRz b2 = C5640aRc.m17844b(CruiseSpeedBuffType.class, "0c2631fed5b4bf4a405b46963662db11", i2);
        f4864Vv = b2;
        arzArr[i2] = b2;
        int i3 = i2 + 1;
        C5663aRz b3 = C5640aRc.m17844b(CruiseSpeedBuffType.class, "b72deba8732ff9b8a9115b2c844d7318", i3);
        f4866Vx = b3;
        arzArr[i3] = b3;
        int i4 = i3 + 1;
        C5663aRz b4 = C5640aRc.m17844b(CruiseSpeedBuffType.class, "99b61c91970d65103aa07a6979165c59", i4);
        f4868Vz = b4;
        arzArr[i4] = b4;
        int i5 = i4 + 1;
        C5663aRz b5 = C5640aRc.m17844b(CruiseSpeedBuffType.class, "869681cf5cc92868951b78d04a3ddb02", i5);
        atQ = b5;
        arzArr[i5] = b5;
        int i6 = i5 + 1;
        C5663aRz b6 = C5640aRc.m17844b(CruiseSpeedBuffType.class, "0c7e175450ec2e11a930b9b02c2c237b", i6);
        atT = b6;
        arzArr[i6] = b6;
        int i7 = i6 + 1;
        C5663aRz b7 = C5640aRc.m17844b(CruiseSpeedBuffType.class, "0b24a4526a3f99fd0907b005ef2ae7b7", i7);
        bEm = b7;
        arzArr[i7] = b7;
        int i8 = i7 + 1;
        _m_fields = arzArr;
        C1634Xv.m11725a((Object[]) AttributeBuffType._m_fields, (Object[]) _m_fields);
        int i9 = AttributeBuffType._m_methodCount;
        C2491fm[] fmVarArr = new C2491fm[(i9 + 16)];
        C2491fm a = C4105zY.m41624a(CruiseSpeedBuffType.class, "ada519fb8dfe7311b3f6b952f0c5791d", i9);
        f4853VE = a;
        fmVarArr[i9] = a;
        int i10 = i9 + 1;
        C2491fm a2 = C4105zY.m41624a(CruiseSpeedBuffType.class, "8ed2babbf67d076f420c37824e48f7fd", i10);
        f4854VF = a2;
        fmVarArr[i10] = a2;
        int i11 = i10 + 1;
        C2491fm a3 = C4105zY.m41624a(CruiseSpeedBuffType.class, "77885923a97c4a264e5edba336293ebd", i11);
        f4855VG = a3;
        fmVarArr[i11] = a3;
        int i12 = i11 + 1;
        C2491fm a4 = C4105zY.m41624a(CruiseSpeedBuffType.class, "46deb6218b116524b70f0d59895fef2b", i12);
        f4856VH = a4;
        fmVarArr[i12] = a4;
        int i13 = i12 + 1;
        C2491fm a5 = C4105zY.m41624a(CruiseSpeedBuffType.class, "8dec00a183379d407ad575a6e6d84f4d", i13);
        f4857VI = a5;
        fmVarArr[i13] = a5;
        int i14 = i13 + 1;
        C2491fm a6 = C4105zY.m41624a(CruiseSpeedBuffType.class, "c523332a70821733adca37f7ef90b63b", i14);
        f4858VJ = a6;
        fmVarArr[i14] = a6;
        int i15 = i14 + 1;
        C2491fm a7 = C4105zY.m41624a(CruiseSpeedBuffType.class, "9ea9e5c74ec8cb48082244074c2fbd61", i15);
        f4859VK = a7;
        fmVarArr[i15] = a7;
        int i16 = i15 + 1;
        C2491fm a8 = C4105zY.m41624a(CruiseSpeedBuffType.class, "2979fbf29cd4ea4fcfe9497d13f3209f", i16);
        f4860VL = a8;
        fmVarArr[i16] = a8;
        int i17 = i16 + 1;
        C2491fm a9 = C4105zY.m41624a(CruiseSpeedBuffType.class, "bfce9ff7649d1ec70911330cd3b145bc", i17);
        eUO = a9;
        fmVarArr[i17] = a9;
        int i18 = i17 + 1;
        C2491fm a10 = C4105zY.m41624a(CruiseSpeedBuffType.class, "844ba112452b96db3fe0b27cc2b1c5cc", i18);
        eUP = a10;
        fmVarArr[i18] = a10;
        int i19 = i18 + 1;
        C2491fm a11 = C4105zY.m41624a(CruiseSpeedBuffType.class, "cf4f8e7a2f1d981eaf4bd39e534c426f", i19);
        eUS = a11;
        fmVarArr[i19] = a11;
        int i20 = i19 + 1;
        C2491fm a12 = C4105zY.m41624a(CruiseSpeedBuffType.class, "a94a333a0745730e6ee67fc129dfef8d", i20);
        eUT = a12;
        fmVarArr[i20] = a12;
        int i21 = i20 + 1;
        C2491fm a13 = C4105zY.m41624a(CruiseSpeedBuffType.class, "c7d16dfc534ca75347a49f0c9ab653dc", i21);
        fSr = a13;
        fmVarArr[i21] = a13;
        int i22 = i21 + 1;
        C2491fm a14 = C4105zY.m41624a(CruiseSpeedBuffType.class, "8d4af7b54c5412cd2808396c85a27abc", i22);
        fSs = a14;
        fmVarArr[i22] = a14;
        int i23 = i22 + 1;
        C2491fm a15 = C4105zY.m41624a(CruiseSpeedBuffType.class, "742f807811cdb22ee36300ea7cc1db7c", i23);
        fYf = a15;
        fmVarArr[i23] = a15;
        int i24 = i23 + 1;
        C2491fm a16 = C4105zY.m41624a(CruiseSpeedBuffType.class, "b79b76c98fda9daf90e0eb59e9dd3d76", i24);
        f4871dN = a16;
        fmVarArr[i24] = a16;
        int i25 = i24 + 1;
        _m_methods = fmVarArr;
        C1634Xv.m11725a((Object[]) AttributeBuffType._m_methods, (Object[]) _m_methods);
        ___iScriptClass = aUO.m18566a(CruiseSpeedBuffType.class, C5381aHd.class, _m_fields, _m_methods);
    }

    /* renamed from: Kn */
    private C3892wO m23716Kn() {
        return (C3892wO) bFf().mo5608dq().mo3214p(atQ);
    }

    /* renamed from: Kp */
    private C3892wO m23717Kp() {
        return (C3892wO) bFf().mo5608dq().mo3214p(atT);
    }

    private boolean amK() {
        return bFf().mo5608dq().mo3201h(bEm);
    }

    /* renamed from: bD */
    private void m23722bD(boolean z) {
        bFf().mo5608dq().mo3153a(bEm, z);
    }

    /* renamed from: d */
    private void m23723d(C3892wO wOVar) {
        bFf().mo5608dq().mo3197f(f4862Vt, wOVar);
    }

    /* renamed from: e */
    private void m23724e(C3892wO wOVar) {
        bFf().mo5608dq().mo3197f(f4864Vv, wOVar);
    }

    /* renamed from: f */
    private void m23725f(C3892wO wOVar) {
        bFf().mo5608dq().mo3197f(f4866Vx, wOVar);
    }

    /* renamed from: g */
    private void m23727g(C3892wO wOVar) {
        bFf().mo5608dq().mo3197f(f4868Vz, wOVar);
    }

    /* renamed from: s */
    private void m23732s(C3892wO wOVar) {
        bFf().mo5608dq().mo3197f(atQ, wOVar);
    }

    /* renamed from: u */
    private void m23733u(C3892wO wOVar) {
        bFf().mo5608dq().mo3197f(atT, wOVar);
    }

    /* renamed from: yE */
    private C3892wO m23734yE() {
        return (C3892wO) bFf().mo5608dq().mo3214p(f4862Vt);
    }

    /* renamed from: yF */
    private C3892wO m23735yF() {
        return (C3892wO) bFf().mo5608dq().mo3214p(f4864Vv);
    }

    /* renamed from: yG */
    private C3892wO m23736yG() {
        return (C3892wO) bFf().mo5608dq().mo3214p(f4866Vx);
    }

    /* renamed from: yH */
    private C3892wO m23737yH() {
        return (C3892wO) bFf().mo5608dq().mo3214p(f4868Vz);
    }

    /* renamed from: T */
    public C6494any mo11T() {
        return ___iScriptClass;
    }

    /* renamed from: U */
    public C2491fm[] mo12U() {
        return _m_methods;
    }

    /* renamed from: W */
    public Object mo13W() {
        return new C5381aHd(this);
    }

    /* renamed from: a */
    public Object mo14a(C0495Gr gr) {
        Object[] args = gr.getArgs();
        switch (gr.mo2417hq() - AttributeBuffType._m_methodCount) {
            case 0:
                return m23738yK();
            case 1:
                m23728i((C3892wO) args[0]);
                return null;
            case 2:
                return m23739yM();
            case 3:
                m23729k((C3892wO) args[0]);
                return null;
            case 4:
                return m23740yO();
            case 5:
                m23730m((C3892wO) args[0]);
                return null;
            case 6:
                return m23741yQ();
            case 7:
                m23731o((C3892wO) args[0]);
                return null;
            case 8:
                return bLy();
            case 9:
                m23720aa((C3892wO) args[0]);
                return null;
            case 10:
                return bLC();
            case 11:
                m23721ae((C3892wO) args[0]);
                return null;
            case 12:
                return new Boolean(cfH());
            case 13:
                m23726fb(((Boolean) args[0]).booleanValue());
                return null;
            case 14:
                return cim();
            case 15:
                return m23719aT();
            default:
                return super.mo14a(gr);
        }
    }

    /* renamed from: a */
    public void mo15a(Collection collection, C0495Gr gr) {
        super.mo15a(collection, gr);
    }

    /* access modifiers changed from: protected */
    /* renamed from: aU */
    public <T extends aDJ> T mo745aU() {
        switch (bFf().mo6893i(f4871dN)) {
            case 0:
                return null;
            case 2:
                return (aDJ) bFf().mo5606d(new aCE(this, f4871dN, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f4871dN, new Object[0]));
                break;
        }
        return m23719aT();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Cooldown")
    /* renamed from: ab */
    public void mo14670ab(C3892wO wOVar) {
        switch (bFf().mo6893i(eUP)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, eUP, new Object[]{wOVar}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, eUP, new Object[]{wOVar}));
                break;
        }
        m23720aa(wOVar);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Warmup")
    /* renamed from: af */
    public void mo14671af(C3892wO wOVar) {
        switch (bFf().mo6893i(eUT)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, eUT, new Object[]{wOVar}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, eUT, new Object[]{wOVar}));
                break;
        }
        m23721ae(wOVar);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Warmup")
    public C3892wO bLD() {
        switch (bFf().mo6893i(eUS)) {
            case 0:
                return null;
            case 2:
                return (C3892wO) bFf().mo5606d(new aCE(this, eUS, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, eUS, new Object[0]));
                break;
        }
        return bLC();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Cooldown")
    public C3892wO bLz() {
        switch (bFf().mo6893i(eUO)) {
            case 0:
                return null;
            case 2:
                return (C3892wO) bFf().mo5606d(new aCE(this, eUO, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, eUO, new Object[0]));
                break;
        }
        return bLy();
    }

    /* renamed from: c */
    public C5663aRz[] mo25c() {
        return _m_fields;
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Jammed")
    public boolean cfI() {
        switch (bFf().mo6893i(fSr)) {
            case 0:
                return false;
            case 2:
                return ((Boolean) bFf().mo5606d(new aCE(this, fSr, new Object[0]))).booleanValue();
            case 3:
                bFf().mo5606d(new aCE(this, fSr, new Object[0]));
                break;
        }
        return cfH();
    }

    public CruiseSpeedBuff cin() {
        switch (bFf().mo6893i(fYf)) {
            case 0:
                return null;
            case 2:
                return (CruiseSpeedBuff) bFf().mo5606d(new aCE(this, fYf, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, fYf, new Object[0]));
                break;
        }
        return cim();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Jammed")
    /* renamed from: fc */
    public void mo14676fc(boolean z) {
        switch (bFf().mo6893i(fSs)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, fSs, new Object[]{new Boolean(z)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, fSs, new Object[]{new Boolean(z)}));
                break;
        }
        m23726fb(z);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Linear Velocity")
    /* renamed from: j */
    public void mo14677j(C3892wO wOVar) {
        switch (bFf().mo6893i(f4854VF)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f4854VF, new Object[]{wOVar}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f4854VF, new Object[]{wOVar}));
                break;
        }
        m23728i(wOVar);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Linear Acceleration")
    /* renamed from: l */
    public void mo14678l(C3892wO wOVar) {
        switch (bFf().mo6893i(f4856VH)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f4856VH, new Object[]{wOVar}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f4856VH, new Object[]{wOVar}));
                break;
        }
        m23729k(wOVar);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Angular Velocity")
    /* renamed from: n */
    public void mo14679n(C3892wO wOVar) {
        switch (bFf().mo6893i(f4858VJ)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f4858VJ, new Object[]{wOVar}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f4858VJ, new Object[]{wOVar}));
                break;
        }
        m23730m(wOVar);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Angular Acceleration")
    /* renamed from: p */
    public void mo14680p(C3892wO wOVar) {
        switch (bFf().mo6893i(f4860VL)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f4860VL, new Object[]{wOVar}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f4860VL, new Object[]{wOVar}));
                break;
        }
        m23731o(wOVar);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Linear Velocity")
    /* renamed from: yL */
    public C3892wO mo14681yL() {
        switch (bFf().mo6893i(f4853VE)) {
            case 0:
                return null;
            case 2:
                return (C3892wO) bFf().mo5606d(new aCE(this, f4853VE, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f4853VE, new Object[0]));
                break;
        }
        return m23738yK();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Linear Acceleration")
    /* renamed from: yN */
    public C3892wO mo14682yN() {
        switch (bFf().mo6893i(f4855VG)) {
            case 0:
                return null;
            case 2:
                return (C3892wO) bFf().mo5606d(new aCE(this, f4855VG, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f4855VG, new Object[0]));
                break;
        }
        return m23739yM();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Angular Velocity")
    /* renamed from: yP */
    public C3892wO mo14683yP() {
        switch (bFf().mo6893i(f4857VI)) {
            case 0:
                return null;
            case 2:
                return (C3892wO) bFf().mo5606d(new aCE(this, f4857VI, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f4857VI, new Object[0]));
                break;
        }
        return m23740yO();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Angular Acceleration")
    /* renamed from: yR */
    public C3892wO mo14684yR() {
        switch (bFf().mo6893i(f4859VK)) {
            case 0:
                return null;
            case 2:
                return (C3892wO) bFf().mo5606d(new aCE(this, f4859VK, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f4859VK, new Object[0]));
                break;
        }
        return m23741yQ();
    }

    /* renamed from: S */
    public void mo10S() {
        super.mo10S();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Linear Velocity")
    @C0064Am(aul = "ada519fb8dfe7311b3f6b952f0c5791d", aum = 0)
    /* renamed from: yK */
    private C3892wO m23738yK() {
        return m23734yE();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Linear Velocity")
    @C0064Am(aul = "8ed2babbf67d076f420c37824e48f7fd", aum = 0)
    /* renamed from: i */
    private void m23728i(C3892wO wOVar) {
        m23723d(wOVar);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Linear Acceleration")
    @C0064Am(aul = "77885923a97c4a264e5edba336293ebd", aum = 0)
    /* renamed from: yM */
    private C3892wO m23739yM() {
        return m23735yF();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Linear Acceleration")
    @C0064Am(aul = "46deb6218b116524b70f0d59895fef2b", aum = 0)
    /* renamed from: k */
    private void m23729k(C3892wO wOVar) {
        m23724e(wOVar);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Angular Velocity")
    @C0064Am(aul = "8dec00a183379d407ad575a6e6d84f4d", aum = 0)
    /* renamed from: yO */
    private C3892wO m23740yO() {
        return m23736yG();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Angular Velocity")
    @C0064Am(aul = "c523332a70821733adca37f7ef90b63b", aum = 0)
    /* renamed from: m */
    private void m23730m(C3892wO wOVar) {
        m23725f(wOVar);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Angular Acceleration")
    @C0064Am(aul = "9ea9e5c74ec8cb48082244074c2fbd61", aum = 0)
    /* renamed from: yQ */
    private C3892wO m23741yQ() {
        return m23737yH();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Angular Acceleration")
    @C0064Am(aul = "2979fbf29cd4ea4fcfe9497d13f3209f", aum = 0)
    /* renamed from: o */
    private void m23731o(C3892wO wOVar) {
        m23727g(wOVar);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Cooldown")
    @C0064Am(aul = "bfce9ff7649d1ec70911330cd3b145bc", aum = 0)
    private C3892wO bLy() {
        return m23716Kn();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Cooldown")
    @C0064Am(aul = "844ba112452b96db3fe0b27cc2b1c5cc", aum = 0)
    /* renamed from: aa */
    private void m23720aa(C3892wO wOVar) {
        m23732s(wOVar);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Warmup")
    @C0064Am(aul = "cf4f8e7a2f1d981eaf4bd39e534c426f", aum = 0)
    private C3892wO bLC() {
        return m23717Kp();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Warmup")
    @C0064Am(aul = "a94a333a0745730e6ee67fc129dfef8d", aum = 0)
    /* renamed from: ae */
    private void m23721ae(C3892wO wOVar) {
        m23733u(wOVar);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Jammed")
    @C0064Am(aul = "c7d16dfc534ca75347a49f0c9ab653dc", aum = 0)
    private boolean cfH() {
        return amK();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Jammed")
    @C0064Am(aul = "8d4af7b54c5412cd2808396c85a27abc", aum = 0)
    /* renamed from: fb */
    private void m23726fb(boolean z) {
        m23722bD(z);
    }

    @C0064Am(aul = "742f807811cdb22ee36300ea7cc1db7c", aum = 0)
    private CruiseSpeedBuff cim() {
        return (CruiseSpeedBuff) mo745aU();
    }

    @C0064Am(aul = "b79b76c98fda9daf90e0eb59e9dd3d76", aum = 0)
    /* renamed from: aT */
    private <T extends aDJ> T m23719aT() {
        T t = (CruiseSpeedBuff) bFf().mo6865M(CruiseSpeedBuff.class);
        t.mo10836a(this);
        return t;
    }
}
