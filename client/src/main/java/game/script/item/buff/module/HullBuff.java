package game.script.item.buff.module;

import game.network.message.externalizable.aCE;
import game.script.damage.DamageType;
import game.script.ship.HullAdapter;
import logic.baa.C1616Xf;
import logic.baa.C1634Xv;
import logic.baa.C5511aMd;
import logic.baa.C6485anp;
import logic.data.mbean.C5998aeW;
import logic.data.mbean.C6610aqK;
import logic.res.code.C5640aRc;
import logic.res.code.C5663aRz;
import logic.res.code.C6494any;
import logic.res.html.C2491fm;
import p001a.*;

import java.util.Collection;

@C5829abJ("1.1.2")
@C6485anp
@C2712iu(version = "1.1.2")
@C5511aMd
/* renamed from: a.ox */
/* compiled from: a */
public class HullBuff extends AttributeBuff implements C1616Xf {
    public static final int _m_fieldCount = 0;
    public static final C5663aRz[] _m_fields = null;
    public static final int _m_methodCount = 0;
    public static final C2491fm[] _m_methods = null;
    public static final C5663aRz aOJ = null;
    public static final C5663aRz aOr = null;
    public static final C5663aRz aPG = null;
    public static final C2491fm aPH = null;
    public static final C2491fm aPI = null;
    public static final C2491fm aPJ = null;
    public static final C2491fm aPK = null;
    public static final long serialVersionUID = 0;
    public static C6494any ___iScriptClass;
    @C0064Am(aul = "a25972163a7706bbc8a86e388979b463", aum = 0)
    private static C3892wO aBJ;
    @C0064Am(aul = "c5f1389ae1b40f3db7d29a2c7e039a5d", aum = 1)
    private static C3892wO aBK;
    @C0064Am(aul = "00c60e5a42d91a37f8b902f8c3cd2970", aum = 2)
    private static HullAdapter aPF;

    static {
        m36936V();
    }

    public HullBuff() {
        super((C5540aNg) null);
        super.mo10S();
    }

    public HullBuff(HullBuffType aes) {
        super((C5540aNg) null);
        super._m_script_init(aes);
    }

    public HullBuff(C5540aNg ang) {
        super(ang);
    }

    /* renamed from: V */
    static void m36936V() {
        serialVersionUID = (long) 1;
        _m_fieldCount = AttributeBuff._m_fieldCount + 3;
        _m_methodCount = AttributeBuff._m_methodCount + 4;
        int i = AttributeBuff._m_fieldCount;
        C5663aRz[] arzArr = new C5663aRz[(i + 3)];
        C5663aRz b = C5640aRc.m17844b(HullBuff.class, "a25972163a7706bbc8a86e388979b463", i);
        aOJ = b;
        arzArr[i] = b;
        int i2 = i + 1;
        C5663aRz b2 = C5640aRc.m17844b(HullBuff.class, "c5f1389ae1b40f3db7d29a2c7e039a5d", i2);
        aOr = b2;
        arzArr[i2] = b2;
        int i3 = i2 + 1;
        C5663aRz b3 = C5640aRc.m17844b(HullBuff.class, "00c60e5a42d91a37f8b902f8c3cd2970", i3);
        aPG = b3;
        arzArr[i3] = b3;
        int i4 = i3 + 1;
        _m_fields = arzArr;
        C1634Xv.m11725a((Object[]) AttributeBuff._m_fields, (Object[]) _m_fields);
        int i5 = AttributeBuff._m_methodCount;
        C2491fm[] fmVarArr = new C2491fm[(i5 + 4)];
        C2491fm a = C4105zY.m41624a(HullBuff.class, "c83a60a88b7bd767b0ed938e40218fb3", i5);
        aPH = a;
        fmVarArr[i5] = a;
        int i6 = i5 + 1;
        C2491fm a2 = C4105zY.m41624a(HullBuff.class, "fe528d4772f2548c9b7359d3dd5aee77", i6);
        aPI = a2;
        fmVarArr[i6] = a2;
        int i7 = i6 + 1;
        C2491fm a3 = C4105zY.m41624a(HullBuff.class, "5ecc1e2e63597a3c789cec0d7527c6a7", i7);
        aPJ = a3;
        fmVarArr[i7] = a3;
        int i8 = i7 + 1;
        C2491fm a4 = C4105zY.m41624a(HullBuff.class, "f250dc7a5778a07beca5d48fd9ef23ce", i8);
        aPK = a4;
        fmVarArr[i8] = a4;
        int i9 = i8 + 1;
        _m_methods = fmVarArr;
        C1634Xv.m11725a((Object[]) AttributeBuff._m_methods, (Object[]) _m_methods);
        ___iScriptClass = aUO.m18566a(HullBuff.class, C5998aeW.class, _m_fields, _m_methods);
    }

    /* renamed from: Ub */
    private C3892wO m36929Ub() {
        return ((HullBuffType) getType()).aBc();
    }

    /* renamed from: Uc */
    private C3892wO m36930Uc() {
        return ((HullBuffType) getType()).aBe();
    }

    /* renamed from: Ud */
    private HullAdapter m36931Ud() {
        return (HullAdapter) bFf().mo5608dq().mo3214p(aPG);
    }

    @C0064Am(aul = "c83a60a88b7bd767b0ed938e40218fb3", aum = 0)
    @C5566aOg
    /* renamed from: Ue */
    private void m36932Ue() {
        throw new aWi(new aCE(this, aPH, new Object[0]));
    }

    @C0064Am(aul = "fe528d4772f2548c9b7359d3dd5aee77", aum = 0)
    @C5566aOg
    /* renamed from: Ug */
    private void m36933Ug() {
        throw new aWi(new aCE(this, aPI, new Object[0]));
    }

    /* renamed from: a */
    private void m36937a(HullAdapter alm) {
        bFf().mo5608dq().mo3197f(aPG, alm);
    }

    /* renamed from: v */
    private void m36938v(C3892wO wOVar) {
        throw new C6039afL();
    }

    /* renamed from: w */
    private void m36939w(C3892wO wOVar) {
        throw new C6039afL();
    }

    /* renamed from: S */
    public void mo10S() {
        super.mo10S();
    }

    /* renamed from: T */
    public C6494any mo11T() {
        return ___iScriptClass;
    }

    /* renamed from: U */
    public C2491fm[] mo12U() {
        return _m_methods;
    }

    @C5566aOg
    /* renamed from: Uf */
    public void mo3751Uf() {
        switch (bFf().mo6893i(aPH)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, aPH, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, aPH, new Object[0]));
                break;
        }
        m36932Ue();
    }

    @C5566aOg
    /* renamed from: Uh */
    public void mo3752Uh() {
        switch (bFf().mo6893i(aPI)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, aPI, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, aPI, new Object[0]));
                break;
        }
        m36933Ug();
    }

    /* access modifiers changed from: protected */
    /* renamed from: Uj */
    public C3892wO mo21102Uj() {
        switch (bFf().mo6893i(aPJ)) {
            case 0:
                return null;
            case 2:
                return (C3892wO) bFf().mo5606d(new aCE(this, aPJ, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, aPJ, new Object[0]));
                break;
        }
        return m36934Ui();
    }

    /* access modifiers changed from: protected */
    /* renamed from: Ul */
    public C3892wO mo21103Ul() {
        switch (bFf().mo6893i(aPK)) {
            case 0:
                return null;
            case 2:
                return (C3892wO) bFf().mo5606d(new aCE(this, aPK, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, aPK, new Object[0]));
                break;
        }
        return m36935Uk();
    }

    /* renamed from: W */
    public Object mo13W() {
        return new C5998aeW(this);
    }

    /* renamed from: a */
    public Object mo14a(C0495Gr gr) {
        gr.getArgs();
        switch (gr.mo2417hq() - AttributeBuff._m_methodCount) {
            case 0:
                m36932Ue();
                return null;
            case 1:
                m36933Ug();
                return null;
            case 2:
                return m36934Ui();
            case 3:
                return m36935Uk();
            default:
                return super.mo14a(gr);
        }
    }

    /* renamed from: a */
    public void mo15a(Collection collection, C0495Gr gr) {
        super.mo15a(collection, gr);
    }

    /* renamed from: c */
    public C5663aRz[] mo25c() {
        return _m_fields;
    }

    /* renamed from: a */
    public void mo21104a(HullBuffType aes) {
        super.mo3934i(aes);
        HullModuleAdapter aVar = (HullModuleAdapter) bFf().mo6865M(HullModuleAdapter.class);
        aVar.mo21105b(this);
        m36937a((HullAdapter) aVar);
    }

    @C0064Am(aul = "5ecc1e2e63597a3c789cec0d7527c6a7", aum = 0)
    /* renamed from: Ui */
    private C3892wO m36934Ui() {
        return m36930Uc();
    }

    @C0064Am(aul = "f250dc7a5778a07beca5d48fd9ef23ce", aum = 0)
    /* renamed from: Uk */
    private C3892wO m36935Uk() {
        return m36929Ub();
    }

    @C6485anp
    @C5511aMd
    /* renamed from: a.ox$a */
    public class HullModuleAdapter extends HullAdapter implements C1616Xf {
        public static final int _m_fieldCount = 0;
        public static final C5663aRz[] _m_fields = null;
        public static final int _m_methodCount = 0;
        public static final C2491fm[] _m_methods = null;
        /* renamed from: aT */
        public static final C5663aRz f8806aT = null;
        /* renamed from: qa */
        public static final C2491fm f8807qa = null;
        /* renamed from: qb */
        public static final C2491fm f8808qb = null;
        public static final long serialVersionUID = 0;
        /* renamed from: uI */
        public static final C2491fm f8809uI = null;
        public static C6494any ___iScriptClass;
        @C0064Am(aul = "0414b6436c78cb172bba983c1835c54b", aum = 0)
        static /* synthetic */ HullBuff eJv;

        static {
            m36952V();
        }

        public HullModuleAdapter() {
            super((C5540aNg) null);
            super.mo10S();
        }

        public HullModuleAdapter(C5540aNg ang) {
            super(ang);
        }

        public HullModuleAdapter(HullBuff oxVar) {
            super((C5540aNg) null);
            super._m_script_init(oxVar);
        }

        /* renamed from: V */
        static void m36952V() {
            serialVersionUID = (long) 1;
            _m_fieldCount = HullAdapter._m_fieldCount + 1;
            _m_methodCount = HullAdapter._m_methodCount + 3;
            int i = HullAdapter._m_fieldCount;
            C5663aRz[] arzArr = new C5663aRz[(i + 1)];
            C5663aRz b = C5640aRc.m17844b(HullModuleAdapter.class, "0414b6436c78cb172bba983c1835c54b", i);
            f8806aT = b;
            arzArr[i] = b;
            int i2 = i + 1;
            _m_fields = arzArr;
            C1634Xv.m11725a((Object[]) HullAdapter._m_fields, (Object[]) _m_fields);
            int i3 = HullAdapter._m_methodCount;
            C2491fm[] fmVarArr = new C2491fm[(i3 + 3)];
            C2491fm a = C4105zY.m41624a(HullModuleAdapter.class, "63eab1bd5a3f129e38c2c0333e889ec3", i3);
            f8807qa = a;
            fmVarArr[i3] = a;
            int i4 = i3 + 1;
            C2491fm a2 = C4105zY.m41624a(HullModuleAdapter.class, "cc2f367b18e730dedd58958f64ed6f92", i4);
            f8809uI = a2;
            fmVarArr[i4] = a2;
            int i5 = i4 + 1;
            C2491fm a3 = C4105zY.m41624a(HullModuleAdapter.class, "fe823d83fe76a42609ac9ca754f1e8d2", i5);
            f8808qb = a3;
            fmVarArr[i5] = a3;
            int i6 = i5 + 1;
            _m_methods = fmVarArr;
            C1634Xv.m11725a((Object[]) HullAdapter._m_methods, (Object[]) _m_methods);
            ___iScriptClass = aUO.m18566a(HullModuleAdapter.class, C6610aqK.class, _m_fields, _m_methods);
        }

        /* renamed from: a */
        private void m36955a(HullBuff oxVar) {
            bFf().mo5608dq().mo3197f(f8806aT, oxVar);
        }

        private HullBuff bGc() {
            return (HullBuff) bFf().mo5608dq().mo3214p(f8806aT);
        }

        /* renamed from: S */
        public void mo10S() {
            super.mo10S();
        }

        /* renamed from: T */
        public C6494any mo11T() {
            return ___iScriptClass;
        }

        /* renamed from: U */
        public C2491fm[] mo12U() {
            return _m_methods;
        }

        /* renamed from: W */
        public Object mo13W() {
            return new C6610aqK(this);
        }

        /* renamed from: a */
        public Object mo14a(C0495Gr gr) {
            Object[] args = gr.getArgs();
            switch (gr.mo2417hq() - HullAdapter._m_methodCount) {
                case 0:
                    return new Float(m36954a((DamageType) args[0], ((Float) args[1]).floatValue()));
                case 1:
                    return new Float(m36953a((DamageType) args[0]));
                case 2:
                    return new Float(m36956hg());
                default:
                    return super.mo14a(gr);
            }
        }

        /* renamed from: a */
        public void mo15a(Collection collection, C0495Gr gr) {
            super.mo15a(collection, gr);
        }

        /* renamed from: b */
        public float mo9876b(DamageType fr) {
            switch (bFf().mo6893i(f8809uI)) {
                case 0:
                    return 0.0f;
                case 2:
                    return ((Float) bFf().mo5606d(new aCE(this, f8809uI, new Object[]{fr}))).floatValue();
                case 3:
                    bFf().mo5606d(new aCE(this, f8809uI, new Object[]{fr}));
                    break;
            }
            return m36953a(fr);
        }

        /* renamed from: b */
        public float mo9877b(DamageType fr, float f) {
            switch (bFf().mo6893i(f8807qa)) {
                case 0:
                    return 0.0f;
                case 2:
                    return ((Float) bFf().mo5606d(new aCE(this, f8807qa, new Object[]{fr, new Float(f)}))).floatValue();
                case 3:
                    bFf().mo5606d(new aCE(this, f8807qa, new Object[]{fr, new Float(f)}));
                    break;
            }
            return m36954a(fr, f);
        }

        /* renamed from: c */
        public C5663aRz[] mo25c() {
            return _m_fields;
        }

        /* renamed from: hh */
        public float mo9878hh() {
            switch (bFf().mo6893i(f8808qb)) {
                case 0:
                    return 0.0f;
                case 2:
                    return ((Float) bFf().mo5606d(new aCE(this, f8808qb, new Object[0]))).floatValue();
                case 3:
                    bFf().mo5606d(new aCE(this, f8808qb, new Object[0]));
                    break;
            }
            return m36956hg();
        }

        /* renamed from: b */
        public void mo21105b(HullBuff oxVar) {
            m36955a(oxVar);
            super.mo10S();
        }

        @C0064Am(aul = "63eab1bd5a3f129e38c2c0333e889ec3", aum = 0)
        /* renamed from: a */
        private float m36954a(DamageType fr, float f) {
            return bGc().mo21102Uj().mo22753v(((HullAdapter) mo16070IE()).mo9877b(fr, f), super.mo9877b(fr, f));
        }

        @C0064Am(aul = "cc2f367b18e730dedd58958f64ed6f92", aum = 0)
        /* renamed from: a */
        private float m36953a(DamageType fr) {
            return bGc().mo21102Uj().mo22753v(((HullAdapter) mo16070IE()).mo9876b(fr), super.mo9876b(fr));
        }

        @C0064Am(aul = "fe823d83fe76a42609ac9ca754f1e8d2", aum = 0)
        /* renamed from: hg */
        private float m36956hg() {
            float v = bGc().mo21103Ul().mo22753v(((HullAdapter) mo16070IE()).mo9878hh(), super.mo9878hh());
            if (v > 0.0f) {
                return v;
            }
            return 1.0f;
        }
    }
}
