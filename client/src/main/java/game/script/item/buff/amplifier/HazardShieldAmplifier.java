package game.script.item.buff.amplifier;

import game.network.message.externalizable.aCE;
import game.script.item.Amplifier;
import game.script.item.AmplifierType;
import game.script.ship.categories.Explorer;
import game.script.ship.hazardshield.HazardShieldAdapter;
import logic.baa.C1616Xf;
import logic.baa.C1634Xv;
import logic.baa.C5511aMd;
import logic.baa.C6485anp;
import logic.data.mbean.C2521gJ;
import logic.data.mbean.C5762aVu;
import logic.res.code.C5640aRc;
import logic.res.code.C5663aRz;
import logic.res.code.C6494any;
import logic.res.html.C2491fm;
import p001a.*;

import java.util.Collection;

@C6485anp
@C2712iu
@C5511aMd
/* renamed from: a.aRh  reason: case insensitive filesystem */
/* compiled from: a */
public class HazardShieldAmplifier extends Amplifier implements C1616Xf {
    public static final int _m_fieldCount = 0;
    public static final C5663aRz[] _m_fields = null;
    public static final int _m_methodCount = 0;
    public static final C2491fm[] _m_methods = null;
    public static final C5663aRz atV = null;
    public static final C2491fm atW = null;
    public static final C2491fm atX = null;
    public static final C5663aRz bbI = null;
    public static final C2491fm hyl = null;
    public static final long serialVersionUID = 0;
    public static C6494any ___iScriptClass;
    @C0064Am(aul = "64c9ed5a69ecb56dc2fb3accfc806b86", aum = 0)
    private static C3892wO bbH;
    @C0064Am(aul = "9f2f56140d1a56211929dbef7edaec7d", aum = 1)
    private static HazardShieldAmplififerAdapter iZw;

    static {
        m17876V();
    }

    public HazardShieldAmplifier() {
        super((C5540aNg) null);
        super.mo10S();
    }

    public HazardShieldAmplifier(C5540aNg ang) {
        super(ang);
    }

    public HazardShieldAmplifier(AmplifierType iDVar) {
        super((C5540aNg) null);
        super.mo1956e(iDVar);
    }

    /* renamed from: V */
    static void m17876V() {
        serialVersionUID = (long) 1;
        _m_fieldCount = Amplifier._m_fieldCount + 2;
        _m_methodCount = Amplifier._m_methodCount + 3;
        int i = Amplifier._m_fieldCount;
        C5663aRz[] arzArr = new C5663aRz[(i + 2)];
        C5663aRz b = C5640aRc.m17844b(HazardShieldAmplifier.class, "64c9ed5a69ecb56dc2fb3accfc806b86", i);
        bbI = b;
        arzArr[i] = b;
        int i2 = i + 1;
        C5663aRz b2 = C5640aRc.m17844b(HazardShieldAmplifier.class, "9f2f56140d1a56211929dbef7edaec7d", i2);
        atV = b2;
        arzArr[i2] = b2;
        int i3 = i2 + 1;
        _m_fields = arzArr;
        C1634Xv.m11725a((Object[]) Amplifier._m_fields, (Object[]) _m_fields);
        int i4 = Amplifier._m_methodCount;
        C2491fm[] fmVarArr = new C2491fm[(i4 + 3)];
        C2491fm a = C4105zY.m41624a(HazardShieldAmplifier.class, "1fd8f2a7e01f34cd2d2055634333056c", i4);
        atW = a;
        fmVarArr[i4] = a;
        int i5 = i4 + 1;
        C2491fm a2 = C4105zY.m41624a(HazardShieldAmplifier.class, "804c1141e3cae8c7c37d7a02ae599339", i5);
        atX = a2;
        fmVarArr[i5] = a2;
        int i6 = i5 + 1;
        C2491fm a3 = C4105zY.m41624a(HazardShieldAmplifier.class, "cac1fb10380430c6fdf364a9394e2e5f", i6);
        hyl = a3;
        fmVarArr[i6] = a3;
        int i7 = i6 + 1;
        _m_methods = fmVarArr;
        C1634Xv.m11725a((Object[]) Amplifier._m_methods, (Object[]) _m_methods);
        ___iScriptClass = aUO.m18566a(HazardShieldAmplifier.class, C5762aVu.class, _m_fields, _m_methods);
    }

    /* renamed from: Yv */
    private C3892wO m17877Yv() {
        return ((HazardShieldAmplifierType) getType()).mo22813LG();
    }

    /* renamed from: a */
    private void m17878a(HazardShieldAmplififerAdapter aVar) {
        bFf().mo5608dq().mo3197f(atV, aVar);
    }

    private HazardShieldAmplififerAdapter dBU() {
        return (HazardShieldAmplififerAdapter) bFf().mo5608dq().mo3214p(atV);
    }

    /* renamed from: x */
    private void m17879x(C3892wO wOVar) {
        throw new C6039afL();
    }

    /* renamed from: Ks */
    public void mo1952Ks() {
        switch (bFf().mo6893i(atW)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, atW, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, atW, new Object[0]));
                break;
        }
        m17874Kr();
    }

    /* renamed from: Ku */
    public void mo1953Ku() {
        switch (bFf().mo6893i(atX)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, atX, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, atX, new Object[0]));
                break;
        }
        m17875Kt();
    }

    /* renamed from: S */
    public void mo10S() {
        super.mo10S();
    }

    /* renamed from: T */
    public C6494any mo11T() {
        return ___iScriptClass;
    }

    /* renamed from: U */
    public C2491fm[] mo12U() {
        return _m_methods;
    }

    /* renamed from: W */
    public Object mo13W() {
        return new C5762aVu(this);
    }

    /* renamed from: a */
    public Object mo14a(C0495Gr gr) {
        gr.getArgs();
        switch (gr.mo2417hq() - Amplifier._m_methodCount) {
            case 0:
                m17874Kr();
                return null;
            case 1:
                m17875Kt();
                return null;
            case 2:
                return cSl();
            default:
                return super.mo14a(gr);
        }
    }

    /* renamed from: a */
    public void mo15a(Collection collection, C0495Gr gr) {
        super.mo15a(collection, gr);
    }

    /* renamed from: c */
    public C5663aRz[] mo25c() {
        return _m_fields;
    }

    public C3892wO cSm() {
        switch (bFf().mo6893i(hyl)) {
            case 0:
                return null;
            case 2:
                return (C3892wO) bFf().mo5606d(new aCE(this, hyl, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, hyl, new Object[0]));
                break;
        }
        return cSl();
    }

    /* renamed from: e */
    public void mo1956e(AmplifierType iDVar) {
        super.mo1956e(iDVar);
    }

    @C0064Am(aul = "1fd8f2a7e01f34cd2d2055634333056c", aum = 0)
    /* renamed from: Kr */
    private void m17874Kr() {
        if (mo7855al() instanceof Explorer) {
            HazardShieldAmplififerAdapter aVar = (HazardShieldAmplififerAdapter) bFf().mo6865M(HazardShieldAmplififerAdapter.class);
            aVar.mo11182b(this);
            m17878a(aVar);
            dBU().push();
            ((Explorer) mo7855al()).cqv().mo17626TJ().mo23261e(dBU());
            asW();
        }
    }

    @C0064Am(aul = "804c1141e3cae8c7c37d7a02ae599339", aum = 0)
    /* renamed from: Kt */
    private void m17875Kt() {
        if (mo7855al() instanceof Explorer) {
            ((Explorer) mo7855al()).cqv().mo17626TJ().mo23262g(dBU());
            asW();
        }
    }

    @C0064Am(aul = "cac1fb10380430c6fdf364a9394e2e5f", aum = 0)
    private C3892wO cSl() {
        return m17877Yv();
    }

    @C6485anp
    @C5511aMd
    /* renamed from: a.aRh$a */
    public class HazardShieldAmplififerAdapter extends HazardShieldAdapter implements C1616Xf {
        public static final int _m_fieldCount = 0;
        public static final C5663aRz[] _m_fields = null;
        public static final int _m_methodCount = 0;
        public static final C2491fm[] _m_methods = null;
        /* renamed from: aT */
        public static final C5663aRz f3715aT = null;
        public static final C2491fm axb = null;
        public static final long serialVersionUID = 0;
        public static C6494any ___iScriptClass;
        @C0064Am(aul = "5e77422b7db79074aa44084e208bd67a", aum = 0)

        /* renamed from: QZ */
        static /* synthetic */ HazardShieldAmplifier f3714QZ;

        static {
            m17891V();
        }

        public HazardShieldAmplififerAdapter() {
            super((C5540aNg) null);
            super.mo10S();
        }

        public HazardShieldAmplififerAdapter(C5540aNg ang) {
            super(ang);
        }

        public HazardShieldAmplififerAdapter(HazardShieldAmplifier arh) {
            super((C5540aNg) null);
            super._m_script_init(arh);
        }

        /* renamed from: V */
        static void m17891V() {
            serialVersionUID = (long) 1;
            _m_fieldCount = HazardShieldAdapter._m_fieldCount + 1;
            _m_methodCount = HazardShieldAdapter._m_methodCount + 1;
            int i = HazardShieldAdapter._m_fieldCount;
            C5663aRz[] arzArr = new C5663aRz[(i + 1)];
            C5663aRz b = C5640aRc.m17844b(HazardShieldAmplififerAdapter.class, "5e77422b7db79074aa44084e208bd67a", i);
            f3715aT = b;
            arzArr[i] = b;
            int i2 = i + 1;
            _m_fields = arzArr;
            C1634Xv.m11725a((Object[]) HazardShieldAdapter._m_fields, (Object[]) _m_fields);
            int i3 = HazardShieldAdapter._m_methodCount;
            C2491fm[] fmVarArr = new C2491fm[(i3 + 1)];
            C2491fm a = C4105zY.m41624a(HazardShieldAmplififerAdapter.class, "29d3ed38751d59093ed5d256e43b6557", i3);
            axb = a;
            fmVarArr[i3] = a;
            int i4 = i3 + 1;
            _m_methods = fmVarArr;
            C1634Xv.m11725a((Object[]) HazardShieldAdapter._m_methods, (Object[]) _m_methods);
            ___iScriptClass = aUO.m18566a(HazardShieldAmplififerAdapter.class, C2521gJ.class, _m_fields, _m_methods);
        }

        /* renamed from: a */
        private void m17892a(HazardShieldAmplifier arh) {
            bFf().mo5608dq().mo3197f(f3715aT, arh);
        }

        private HazardShieldAmplifier drm() {
            return (HazardShieldAmplifier) bFf().mo5608dq().mo3214p(f3715aT);
        }

        /* renamed from: LG */
        public C3892wO mo7993LG() {
            switch (bFf().mo6893i(axb)) {
                case 0:
                    return null;
                case 2:
                    return (C3892wO) bFf().mo5606d(new aCE(this, axb, new Object[0]));
                case 3:
                    bFf().mo5606d(new aCE(this, axb, new Object[0]));
                    break;
            }
            return m17890LF();
        }

        /* renamed from: S */
        public void mo10S() {
            super.mo10S();
        }

        /* renamed from: T */
        public C6494any mo11T() {
            return ___iScriptClass;
        }

        /* renamed from: U */
        public C2491fm[] mo12U() {
            return _m_methods;
        }

        /* renamed from: W */
        public Object mo13W() {
            return new C2521gJ(this);
        }

        /* renamed from: a */
        public Object mo14a(C0495Gr gr) {
            gr.getArgs();
            switch (gr.mo2417hq() - HazardShieldAdapter._m_methodCount) {
                case 0:
                    return m17890LF();
                default:
                    return super.mo14a(gr);
            }
        }

        /* renamed from: a */
        public void mo15a(Collection collection, C0495Gr gr) {
            super.mo15a(collection, gr);
        }

        /* renamed from: c */
        public C5663aRz[] mo25c() {
            return _m_fields;
        }

        /* renamed from: b */
        public void mo11182b(HazardShieldAmplifier arh) {
            m17892a(arh);
            super.mo10S();
        }

        @C0064Am(aul = "29d3ed38751d59093ed5d256e43b6557", aum = 0)
        /* renamed from: LF */
        private C3892wO m17890LF() {
            if (drm().cSm().getValue() == 0.0f) {
                return ((HazardShieldAdapter) mo16071IG()).mo7993LG();
            }
            return drm().cSm().mo22745ah(((HazardShieldAdapter) mo16071IG()).mo7993LG());
        }
    }
}
