package game.script.item.buff.amplifier;

import game.network.message.externalizable.aCE;
import game.script.item.AmplifierType;
import logic.baa.C1616Xf;
import logic.baa.C1634Xv;
import logic.baa.C5511aMd;
import logic.baa.C6485anp;
import logic.data.mbean.C0780LI;
import logic.res.code.C5663aRz;
import logic.res.code.C6494any;
import logic.res.html.C2491fm;
import p001a.*;

import java.util.Collection;

@C2712iu
@C5511aMd
@C6485anp
/* renamed from: a.aGq  reason: case insensitive filesystem */
/* compiled from: a */
public class VaultAmplifier extends CargoHoldAmplifier implements C1616Xf {
    public static final int _m_fieldCount = 0;
    public static final C5663aRz[] _m_fields = null;
    public static final int _m_methodCount = 0;
    public static final C2491fm[] _m_methods = null;
    public static final C2491fm atW = null;
    public static final C2491fm atX = null;
    public static final long serialVersionUID = 0;
    public static C6494any ___iScriptClass;

    static {
        m15009V();
    }

    public VaultAmplifier() {
        super((C5540aNg) null);
        super.mo10S();
    }

    public VaultAmplifier(C5540aNg ang) {
        super(ang);
    }

    public VaultAmplifier(AmplifierType iDVar) {
        super((C5540aNg) null);
        super.mo1956e(iDVar);
    }

    /* renamed from: V */
    static void m15009V() {
        serialVersionUID = (long) 1;
        _m_fieldCount = CargoHoldAmplifier._m_fieldCount + 0;
        _m_methodCount = CargoHoldAmplifier._m_methodCount + 2;
        _m_fields = new C5663aRz[(CargoHoldAmplifier._m_fieldCount + 0)];
        C1634Xv.m11725a((Object[]) CargoHoldAmplifier._m_fields, (Object[]) _m_fields);
        int i = CargoHoldAmplifier._m_methodCount;
        C2491fm[] fmVarArr = new C2491fm[(i + 2)];
        C2491fm a = C4105zY.m41624a(VaultAmplifier.class, "1aecb3e6c01a92e4f714a2abcec48ae7", i);
        atW = a;
        fmVarArr[i] = a;
        int i2 = i + 1;
        C2491fm a2 = C4105zY.m41624a(VaultAmplifier.class, "eb5d435889f8b60645fbce3b150b1810", i2);
        atX = a2;
        fmVarArr[i2] = a2;
        int i3 = i2 + 1;
        _m_methods = fmVarArr;
        C1634Xv.m11725a((Object[]) CargoHoldAmplifier._m_methods, (Object[]) _m_methods);
        ___iScriptClass = aUO.m18566a(VaultAmplifier.class, C0780LI.class, _m_fields, _m_methods);
    }

    @C0064Am(aul = "1aecb3e6c01a92e4f714a2abcec48ae7", aum = 0)
    @C5566aOg
    /* renamed from: Kr */
    private void m15007Kr() {
        throw new aWi(new aCE(this, atW, new Object[0]));
    }

    @C0064Am(aul = "eb5d435889f8b60645fbce3b150b1810", aum = 0)
    @C5566aOg
    /* renamed from: Kt */
    private void m15008Kt() {
        throw new aWi(new aCE(this, atX, new Object[0]));
    }

    @C5566aOg
    /* renamed from: Ks */
    public void mo1952Ks() {
        switch (bFf().mo6893i(atW)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, atW, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, atW, new Object[0]));
                break;
        }
        m15007Kr();
    }

    @C5566aOg
    /* renamed from: Ku */
    public void mo1953Ku() {
        switch (bFf().mo6893i(atX)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, atX, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, atX, new Object[0]));
                break;
        }
        m15008Kt();
    }

    /* renamed from: S */
    public void mo10S() {
        super.mo10S();
    }

    /* renamed from: T */
    public C6494any mo11T() {
        return ___iScriptClass;
    }

    /* renamed from: U */
    public C2491fm[] mo12U() {
        return _m_methods;
    }

    /* renamed from: W */
    public Object mo13W() {
        return new C0780LI(this);
    }

    /* renamed from: a */
    public Object mo14a(C0495Gr gr) {
        gr.getArgs();
        switch (gr.mo2417hq() - CargoHoldAmplifier._m_methodCount) {
            case 0:
                m15007Kr();
                return null;
            case 1:
                m15008Kt();
                return null;
            default:
                return super.mo14a(gr);
        }
    }

    /* renamed from: a */
    public void mo15a(Collection collection, C0495Gr gr) {
        super.mo15a(collection, gr);
    }

    /* renamed from: c */
    public C5663aRz[] mo25c() {
        return _m_fields;
    }

    /* renamed from: e */
    public void mo1956e(AmplifierType iDVar) {
        super.mo1956e(iDVar);
    }
}
