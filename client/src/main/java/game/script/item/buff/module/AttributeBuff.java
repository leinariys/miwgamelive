package game.script.item.buff.module;

import game.network.message.externalizable.aCE;
import game.script.Actor;
import game.script.TaikodomObject;
import game.script.item.Component;
import game.script.item.Module;
import game.script.ship.Ship;
import game.script.template.BaseTaikodomContent;
import logic.baa.*;
import logic.data.mbean.C4037yS;
import logic.res.code.C5640aRc;
import logic.res.code.C5663aRz;
import logic.res.code.C6494any;
import logic.res.html.C2491fm;
import p001a.*;

import java.util.Collection;

@C5829abJ("1.1.2")
@C6485anp
@C2712iu(mo19786Bt = BaseTaikodomContent.class, version = "1.1.2")
@C5511aMd
/* renamed from: a.MP */
/* compiled from: a */
public abstract class AttributeBuff extends TaikodomObject implements C1616Xf {
    public static final int _m_fieldCount = 0;
    public static final C5663aRz[] _m_fields = null;
    public static final int _m_methodCount = 0;
    public static final C2491fm[] _m_methods = null;
    public static final C2491fm aPH = null;
    public static final C2491fm aPI = null;
    public static final C2491fm bEv = null;
    public static final C2491fm bEw = null;
    public static final C5663aRz boP = null;
    public static final C5663aRz dCo = null;
    public static final C5663aRz dCp = null;
    public static final C5663aRz dCq = null;
    public static final C2491fm dCr = null;
    public static final C2491fm dCs = null;
    public static final C2491fm dCt = null;
    public static final C2491fm dCu = null;
    public static final C2491fm dCv = null;
    public static final C2491fm dCw = null;
    public static final C2491fm dCx = null;
    public static final C2491fm dro = null;
    public static final long serialVersionUID = 0;
    /* renamed from: yg */
    public static final C2491fm f1101yg = null;
    public static C6494any ___iScriptClass;
    @C0064Am(aul = "5fa5a464bcc117b86788cb009bbe9885", aum = 1)
    private static Ship bMA;
    @C0064Am(aul = "8e53d233efba6f4206f2bd8ca64bf913", aum = 2)
    private static Actor bMB;
    @C0064Am(aul = "e0567a1edaa66efa9759890399bbfa1c", aum = 3)
    private static Module bMC;
    @C0064Am(aul = "329f90115f3a7f712c3330ff2e6f1141", aum = 0)
    private static Module.C4010b bMz;

    static {
        m6978V();
    }

    public AttributeBuff() {
        super((C5540aNg) null);
        super.mo10S();
    }

    public AttributeBuff(C5540aNg ang) {
        super(ang);
    }

    public AttributeBuff(AttributeBuffType api) {
        super((C5540aNg) null);
        super._m_script_init(api);
    }

    /* renamed from: V */
    static void m6978V() {
        serialVersionUID = (long) 1;
        _m_fieldCount = TaikodomObject._m_fieldCount + 4;
        _m_methodCount = TaikodomObject._m_methodCount + 13;
        int i = TaikodomObject._m_fieldCount;
        C5663aRz[] arzArr = new C5663aRz[(i + 4)];
        C5663aRz b = C5640aRc.m17844b(AttributeBuff.class, "329f90115f3a7f712c3330ff2e6f1141", i);
        dCo = b;
        arzArr[i] = b;
        int i2 = i + 1;
        C5663aRz b2 = C5640aRc.m17844b(AttributeBuff.class, "5fa5a464bcc117b86788cb009bbe9885", i2);
        boP = b2;
        arzArr[i2] = b2;
        int i3 = i2 + 1;
        C5663aRz b3 = C5640aRc.m17844b(AttributeBuff.class, "8e53d233efba6f4206f2bd8ca64bf913", i3);
        dCp = b3;
        arzArr[i3] = b3;
        int i4 = i3 + 1;
        C5663aRz b4 = C5640aRc.m17844b(AttributeBuff.class, "e0567a1edaa66efa9759890399bbfa1c", i4);
        dCq = b4;
        arzArr[i4] = b4;
        int i5 = i4 + 1;
        _m_fields = arzArr;
        C1634Xv.m11725a((Object[]) TaikodomObject._m_fields, (Object[]) _m_fields);
        int i6 = TaikodomObject._m_methodCount;
        C2491fm[] fmVarArr = new C2491fm[(i6 + 13)];
        C2491fm a = C4105zY.m41624a(AttributeBuff.class, "93fc7df3b8eed6f4a92e79cb27558394", i6);
        aPH = a;
        fmVarArr[i6] = a;
        int i7 = i6 + 1;
        C2491fm a2 = C4105zY.m41624a(AttributeBuff.class, "44cdc061254c4371b5c203c9770a3a93", i7);
        aPI = a2;
        fmVarArr[i7] = a2;
        int i8 = i7 + 1;
        C2491fm a3 = C4105zY.m41624a(AttributeBuff.class, "4fc3b6de4c2714404509c4e3d8bf47bd", i8);
        dCr = a3;
        fmVarArr[i8] = a3;
        int i9 = i8 + 1;
        C2491fm a4 = C4105zY.m41624a(AttributeBuff.class, "3c7f251566e578e549b8f24b142ab9a9", i9);
        dro = a4;
        fmVarArr[i9] = a4;
        int i10 = i9 + 1;
        C2491fm a5 = C4105zY.m41624a(AttributeBuff.class, "b60b47fee35fc530d1a9bf207f51e21c", i10);
        dCs = a5;
        fmVarArr[i10] = a5;
        int i11 = i10 + 1;
        C2491fm a6 = C4105zY.m41624a(AttributeBuff.class, "750ce58f39182ccf9d1ed86103cdf72c", i11);
        dCt = a6;
        fmVarArr[i11] = a6;
        int i12 = i11 + 1;
        C2491fm a7 = C4105zY.m41624a(AttributeBuff.class, "33dc479a0fd07d6bf4a0d28a250469d4", i12);
        dCu = a7;
        fmVarArr[i12] = a7;
        int i13 = i12 + 1;
        C2491fm a8 = C4105zY.m41624a(AttributeBuff.class, "f332d42139460e68d0765fa3a5a127cd", i13);
        f1101yg = a8;
        fmVarArr[i13] = a8;
        int i14 = i13 + 1;
        C2491fm a9 = C4105zY.m41624a(AttributeBuff.class, "019f36baafc17b03a9127d522b05b049", i14);
        bEv = a9;
        fmVarArr[i14] = a9;
        int i15 = i14 + 1;
        C2491fm a10 = C4105zY.m41624a(AttributeBuff.class, "3dc7db0b48daf53cbb14643f7509ffaf", i15);
        bEw = a10;
        fmVarArr[i15] = a10;
        int i16 = i15 + 1;
        C2491fm a11 = C4105zY.m41624a(AttributeBuff.class, "2bbdc69e1e90705902e6493ad62df0ab", i16);
        dCv = a11;
        fmVarArr[i16] = a11;
        int i17 = i16 + 1;
        C2491fm a12 = C4105zY.m41624a(AttributeBuff.class, "22077d46ab17dc2cd4334b4344847399", i17);
        dCw = a12;
        fmVarArr[i17] = a12;
        int i18 = i17 + 1;
        C2491fm a13 = C4105zY.m41624a(AttributeBuff.class, "bee4349bd091e48ae9e2d922f1e8d87d", i18);
        dCx = a13;
        fmVarArr[i18] = a13;
        int i19 = i18 + 1;
        _m_methods = fmVarArr;
        C1634Xv.m11725a((Object[]) TaikodomObject._m_methods, (Object[]) _m_methods);
        ___iScriptClass = aUO.m18566a(AttributeBuff.class, C4037yS.class, _m_fields, _m_methods);
    }

    /* renamed from: F */
    private void m6974F(Ship fAVar) {
        bFf().mo5608dq().mo3197f(boP, fAVar);
    }

    @C0064Am(aul = "93fc7df3b8eed6f4a92e79cb27558394", aum = 0)
    /* renamed from: Ue */
    private void m6976Ue() {
        throw new aWi(new aCE(this, aPH, new Object[0]));
    }

    @C0064Am(aul = "44cdc061254c4371b5c203c9770a3a93", aum = 0)
    /* renamed from: Ug */
    private void m6977Ug() {
        throw new aWi(new aCE(this, aPI, new Object[0]));
    }

    /* renamed from: a */
    private void m6980a(Module.C4010b bVar) {
        throw new C6039afL();
    }

    /* renamed from: a */
    private void m6981a(Module yAVar) {
        bFf().mo5608dq().mo3197f(dCq, yAVar);
    }

    /* renamed from: aA */
    private void m6982aA(Actor cr) {
        bFf().mo5608dq().mo3197f(dCp, cr);
    }

    private Module.C4010b bhG() {
        return ((AttributeBuffType) getType()).dod();
    }

    private Ship bhH() {
        return (Ship) bFf().mo5608dq().mo3214p(boP);
    }

    private Actor bhI() {
        return (Actor) bFf().mo5608dq().mo3214p(dCp);
    }

    private Module bhJ() {
        return (Module) bFf().mo5608dq().mo3214p(dCq);
    }

    /* renamed from: H */
    public void mo3924H(Ship fAVar) {
        switch (bFf().mo6893i(dCw)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, dCw, new Object[]{fAVar}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, dCw, new Object[]{fAVar}));
                break;
        }
        m6975G(fAVar);
    }

    /* renamed from: S */
    public void mo10S() {
        super.mo10S();
    }

    /* renamed from: T */
    public C6494any mo11T() {
        return ___iScriptClass;
    }

    /* renamed from: U */
    public C2491fm[] mo12U() {
        return _m_methods;
    }

    /* renamed from: Uf */
    public void mo3751Uf() {
        switch (bFf().mo6893i(aPH)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, aPH, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, aPH, new Object[0]));
                break;
        }
        m6976Ue();
    }

    /* renamed from: Uh */
    public void mo3752Uh() {
        switch (bFf().mo6893i(aPI)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, aPI, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, aPI, new Object[0]));
                break;
        }
        m6977Ug();
    }

    /* renamed from: W */
    public Object mo13W() {
        return new C4037yS(this);
    }

    /* renamed from: a */
    public Object mo14a(C0495Gr gr) {
        Object[] args = gr.getArgs();
        switch (gr.mo2417hq() - TaikodomObject._m_methodCount) {
            case 0:
                m6976Ue();
                return null;
            case 1:
                m6977Ug();
                return null;
            case 2:
                m6984b((Module) args[0]);
                return null;
            case 3:
                return new Float(bdE());
            case 4:
                return bhK();
            case 5:
                return bhM();
            case 6:
                m6983aB((Actor) args[0]);
                return null;
            case 7:
                return new Float(m6986js());
            case 8:
                m6979a((Ship) args[0], (Component) args[1]);
                return null;
            case 9:
                m6985c((Ship) args[0], (Component) args[1]);
                return null;
            case 10:
                return bhO();
            case 11:
                m6975G((Ship) args[0]);
                return null;
            case 12:
                return bhQ();
            default:
                return super.mo14a(gr);
        }
    }

    /* renamed from: a */
    public void mo15a(Collection collection, C0495Gr gr) {
        super.mo15a(collection, gr);
    }

    /* renamed from: aC */
    public void mo3925aC(Actor cr) {
        switch (bFf().mo6893i(dCu)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, dCu, new Object[]{cr}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, dCu, new Object[]{cr}));
                break;
        }
        m6983aB(cr);
    }

    /* renamed from: b */
    public void mo3926b(Ship fAVar, Component abl) {
        switch (bFf().mo6893i(bEv)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, bEv, new Object[]{fAVar, abl}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, bEv, new Object[]{fAVar, abl}));
                break;
        }
        m6979a(fAVar, abl);
    }

    public float bdF() {
        switch (bFf().mo6893i(dro)) {
            case 0:
                return 0.0f;
            case 2:
                return ((Float) bFf().mo5606d(new aCE(this, dro, new Object[0]))).floatValue();
            case 3:
                bFf().mo5606d(new aCE(this, dro, new Object[0]));
                break;
        }
        return bdE();
    }

    public Module bhL() {
        switch (bFf().mo6893i(dCs)) {
            case 0:
                return null;
            case 2:
                return (Module) bFf().mo5606d(new aCE(this, dCs, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, dCs, new Object[0]));
                break;
        }
        return bhK();
    }

    public Actor bhN() {
        switch (bFf().mo6893i(dCt)) {
            case 0:
                return null;
            case 2:
                return (Actor) bFf().mo5606d(new aCE(this, dCt, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, dCt, new Object[0]));
                break;
        }
        return bhM();
    }

    public Ship bhP() {
        switch (bFf().mo6893i(dCv)) {
            case 0:
                return null;
            case 2:
                return (Ship) bFf().mo5606d(new aCE(this, dCv, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, dCv, new Object[0]));
                break;
        }
        return bhO();
    }

    public Module.C4010b bhR() {
        switch (bFf().mo6893i(dCx)) {
            case 0:
                return null;
            case 2:
                return (Module.C4010b) bFf().mo5606d(new aCE(this, dCx, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, dCx, new Object[0]));
                break;
        }
        return bhQ();
    }

    /* renamed from: c */
    public void mo3932c(Module yAVar) {
        switch (bFf().mo6893i(dCr)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, dCr, new Object[]{yAVar}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, dCr, new Object[]{yAVar}));
                break;
        }
        m6984b(yAVar);
    }

    /* renamed from: c */
    public C5663aRz[] mo25c() {
        return _m_fields;
    }

    /* renamed from: d */
    public void mo3933d(Ship fAVar, Component abl) {
        switch (bFf().mo6893i(bEw)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, bEw, new Object[]{fAVar, abl}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, bEw, new Object[]{fAVar, abl}));
                break;
        }
        m6985c(fAVar, abl);
    }

    /* renamed from: jt */
    public float mo3935jt() {
        switch (bFf().mo6893i(f1101yg)) {
            case 0:
                return 0.0f;
            case 2:
                return ((Float) bFf().mo5606d(new aCE(this, f1101yg, new Object[0]))).floatValue();
            case 3:
                bFf().mo5606d(new aCE(this, f1101yg, new Object[0]));
                break;
        }
        return m6986js();
    }

    /* renamed from: i */
    public void mo3934i(AttributeBuffType api) {
        super.mo967a((C2961mJ) api);
    }

    @C0064Am(aul = "4fc3b6de4c2714404509c4e3d8bf47bd", aum = 0)
    /* renamed from: b */
    private void m6984b(Module yAVar) {
        m6981a(yAVar);
        m6982aA(yAVar.dzm());
    }

    @C0064Am(aul = "3c7f251566e578e549b8f24b142ab9a9", aum = 0)
    private float bdE() {
        if (bhJ() == null) {
            return 0.0f;
        }
        return bhJ().bdF();
    }

    @C0064Am(aul = "b60b47fee35fc530d1a9bf207f51e21c", aum = 0)
    private Module bhK() {
        return bhJ();
    }

    @C0064Am(aul = "750ce58f39182ccf9d1ed86103cdf72c", aum = 0)
    private Actor bhM() {
        if (bhJ() == null) {
            return bhI();
        }
        return bhJ().dzm();
    }

    @C0064Am(aul = "33dc479a0fd07d6bf4a0d28a250469d4", aum = 0)
    /* renamed from: aB */
    private void m6983aB(Actor cr) {
        m6982aA(cr);
    }

    @C0064Am(aul = "f332d42139460e68d0765fa3a5a127cd", aum = 0)
    /* renamed from: js */
    private float m6986js() {
        if (bhJ() == null) {
            return 0.0f;
        }
        return bhJ().mo16301jt();
    }

    @C0064Am(aul = "019f36baafc17b03a9127d522b05b049", aum = 0)
    /* renamed from: a */
    private void m6979a(Ship fAVar, Component abl) {
    }

    @C0064Am(aul = "3dc7db0b48daf53cbb14643f7509ffaf", aum = 0)
    /* renamed from: c */
    private void m6985c(Ship fAVar, Component abl) {
    }

    @C0064Am(aul = "2bbdc69e1e90705902e6493ad62df0ab", aum = 0)
    private Ship bhO() {
        return bhH();
    }

    @C0064Am(aul = "22077d46ab17dc2cd4334b4344847399", aum = 0)
    /* renamed from: G */
    private void m6975G(Ship fAVar) {
        m6974F(fAVar);
    }

    @C0064Am(aul = "bee4349bd091e48ae9e2d922f1e8d87d", aum = 0)
    private Module.C4010b bhQ() {
        return bhG();
    }
}
