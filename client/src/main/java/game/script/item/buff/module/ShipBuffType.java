package game.script.item.buff.module;

import game.network.message.externalizable.aCE;
import logic.baa.*;
import logic.data.mbean.C6141ahJ;
import logic.res.code.C5640aRc;
import logic.res.code.C5663aRz;
import logic.res.code.C6494any;
import logic.res.html.C2491fm;
import p001a.*;

import java.util.Collection;

@C0566Hp
@C5829abJ("1.1.2")
@C6485anp
@C5511aMd
/* renamed from: a.hO */
/* compiled from: a */
public class ShipBuffType extends AttributeBuffType implements C1616Xf {
    /* renamed from: VB */
    public static final C5663aRz f7874VB = null;
    /* renamed from: VD */
    public static final C5663aRz f7876VD = null;
    /* renamed from: VE */
    public static final C2491fm f7877VE = null;
    /* renamed from: VF */
    public static final C2491fm f7878VF = null;
    /* renamed from: VG */
    public static final C2491fm f7879VG = null;
    /* renamed from: VH */
    public static final C2491fm f7880VH = null;
    /* renamed from: VI */
    public static final C2491fm f7881VI = null;
    /* renamed from: VJ */
    public static final C2491fm f7882VJ = null;
    /* renamed from: VK */
    public static final C2491fm f7883VK = null;
    /* renamed from: VL */
    public static final C2491fm f7884VL = null;
    /* renamed from: VM */
    public static final C2491fm f7885VM = null;
    /* renamed from: VN */
    public static final C2491fm f7886VN = null;
    /* renamed from: VO */
    public static final C2491fm f7887VO = null;
    /* renamed from: VP */
    public static final C2491fm f7888VP = null;
    /* renamed from: VQ */
    public static final C2491fm f7889VQ = null;
    /* renamed from: Vt */
    public static final C5663aRz f7891Vt = null;
    /* renamed from: Vv */
    public static final C5663aRz f7893Vv = null;
    /* renamed from: Vx */
    public static final C5663aRz f7895Vx = null;
    /* renamed from: Vz */
    public static final C5663aRz f7897Vz = null;
    public static final int _m_fieldCount = 0;
    public static final C5663aRz[] _m_fields = null;
    public static final int _m_methodCount = 0;
    public static final C2491fm[] _m_methods = null;
    /* renamed from: dN */
    public static final C2491fm f7898dN = null;
    public static final long serialVersionUID = 0;
    public static C6494any ___iScriptClass;
    @C0064Am(aul = "7ae7cb8af88672bf5c3778d241143b4d", aum = 4)

    /* renamed from: VA */
    private static C3892wO f7873VA;
    @C0064Am(aul = "c7ccdb6b6f44d05a290d2bea6a914c41", aum = 5)

    /* renamed from: VC */
    private static boolean f7875VC;
    @C0064Am(aul = "d76705b223f66eedd615b2a34fb0a2da", aum = 0)

    /* renamed from: Vs */
    private static C3892wO f7890Vs;
    @C0064Am(aul = "be7fac5171944777bb4076ec232935a8", aum = 1)

    /* renamed from: Vu */
    private static C3892wO f7892Vu;
    @C0064Am(aul = "812dc31524cb72c3a70ca9a9c9c8d228", aum = 2)

    /* renamed from: Vw */
    private static C3892wO f7894Vw;
    @C0064Am(aul = "c1b49c66726aa1ea433ee6ce8c8e1d35", aum = 3)

    /* renamed from: Vy */
    private static C3892wO f7896Vy;

    static {
        m32556V();
    }

    public ShipBuffType() {
        super((C5540aNg) null);
        super.mo10S();
    }

    public ShipBuffType(C5540aNg ang) {
        super(ang);
    }

    /* renamed from: V */
    static void m32556V() {
        serialVersionUID = (long) 1;
        _m_fieldCount = AttributeBuffType._m_fieldCount + 6;
        _m_methodCount = AttributeBuffType._m_methodCount + 14;
        int i = AttributeBuffType._m_fieldCount;
        C5663aRz[] arzArr = new C5663aRz[(i + 6)];
        C5663aRz b = C5640aRc.m17844b(ShipBuffType.class, "d76705b223f66eedd615b2a34fb0a2da", i);
        f7891Vt = b;
        arzArr[i] = b;
        int i2 = i + 1;
        C5663aRz b2 = C5640aRc.m17844b(ShipBuffType.class, "be7fac5171944777bb4076ec232935a8", i2);
        f7893Vv = b2;
        arzArr[i2] = b2;
        int i3 = i2 + 1;
        C5663aRz b3 = C5640aRc.m17844b(ShipBuffType.class, "812dc31524cb72c3a70ca9a9c9c8d228", i3);
        f7895Vx = b3;
        arzArr[i3] = b3;
        int i4 = i3 + 1;
        C5663aRz b4 = C5640aRc.m17844b(ShipBuffType.class, "c1b49c66726aa1ea433ee6ce8c8e1d35", i4);
        f7897Vz = b4;
        arzArr[i4] = b4;
        int i5 = i4 + 1;
        C5663aRz b5 = C5640aRc.m17844b(ShipBuffType.class, "7ae7cb8af88672bf5c3778d241143b4d", i5);
        f7874VB = b5;
        arzArr[i5] = b5;
        int i6 = i5 + 1;
        C5663aRz b6 = C5640aRc.m17844b(ShipBuffType.class, "c7ccdb6b6f44d05a290d2bea6a914c41", i6);
        f7876VD = b6;
        arzArr[i6] = b6;
        int i7 = i6 + 1;
        _m_fields = arzArr;
        C1634Xv.m11725a((Object[]) AttributeBuffType._m_fields, (Object[]) _m_fields);
        int i8 = AttributeBuffType._m_methodCount;
        C2491fm[] fmVarArr = new C2491fm[(i8 + 14)];
        C2491fm a = C4105zY.m41624a(ShipBuffType.class, "40c31d2d7fe033c19f8b8c7779a4e2ab", i8);
        f7877VE = a;
        fmVarArr[i8] = a;
        int i9 = i8 + 1;
        C2491fm a2 = C4105zY.m41624a(ShipBuffType.class, "cef57a56c59fcf20bfdffbb6664b1c62", i9);
        f7878VF = a2;
        fmVarArr[i9] = a2;
        int i10 = i9 + 1;
        C2491fm a3 = C4105zY.m41624a(ShipBuffType.class, "77cd2a9f134b963f61766a7ad1e891f7", i10);
        f7879VG = a3;
        fmVarArr[i10] = a3;
        int i11 = i10 + 1;
        C2491fm a4 = C4105zY.m41624a(ShipBuffType.class, "35fe5246b18a0fd8a8ea393e94ce406b", i11);
        f7880VH = a4;
        fmVarArr[i11] = a4;
        int i12 = i11 + 1;
        C2491fm a5 = C4105zY.m41624a(ShipBuffType.class, "116b6ca1f8c8cd7f04c4859e70d4a4a2", i12);
        f7881VI = a5;
        fmVarArr[i12] = a5;
        int i13 = i12 + 1;
        C2491fm a6 = C4105zY.m41624a(ShipBuffType.class, "80e1519ffef3c0b14c93f27e6fa34797", i13);
        f7882VJ = a6;
        fmVarArr[i13] = a6;
        int i14 = i13 + 1;
        C2491fm a7 = C4105zY.m41624a(ShipBuffType.class, "1d98736ac2260552179c4efda51f62e7", i14);
        f7883VK = a7;
        fmVarArr[i14] = a7;
        int i15 = i14 + 1;
        C2491fm a8 = C4105zY.m41624a(ShipBuffType.class, "b0ad35e83e1ae918057c1673449f21e7", i15);
        f7884VL = a8;
        fmVarArr[i15] = a8;
        int i16 = i15 + 1;
        C2491fm a9 = C4105zY.m41624a(ShipBuffType.class, "75bfed8898b274a7b1d7fe9237c322fc", i16);
        f7885VM = a9;
        fmVarArr[i16] = a9;
        int i17 = i16 + 1;
        C2491fm a10 = C4105zY.m41624a(ShipBuffType.class, "d5f768896d59f7be245d2d28db93ffac", i17);
        f7886VN = a10;
        fmVarArr[i17] = a10;
        int i18 = i17 + 1;
        C2491fm a11 = C4105zY.m41624a(ShipBuffType.class, "bdf21d4fc21717fec99fdb80146aa011", i18);
        f7887VO = a11;
        fmVarArr[i18] = a11;
        int i19 = i18 + 1;
        C2491fm a12 = C4105zY.m41624a(ShipBuffType.class, "a7f99f4ccdad9f9b4f7b8ecc7d963bbe", i19);
        f7888VP = a12;
        fmVarArr[i19] = a12;
        int i20 = i19 + 1;
        C2491fm a13 = C4105zY.m41624a(ShipBuffType.class, "84598583ed3b4b61fbaca349b641419f", i20);
        f7889VQ = a13;
        fmVarArr[i20] = a13;
        int i21 = i20 + 1;
        C2491fm a14 = C4105zY.m41624a(ShipBuffType.class, "8ec6786499d2c1bf82298fcbfd58f879", i21);
        f7898dN = a14;
        fmVarArr[i21] = a14;
        int i22 = i21 + 1;
        _m_methods = fmVarArr;
        C1634Xv.m11725a((Object[]) AttributeBuffType._m_methods, (Object[]) _m_methods);
        ___iScriptClass = aUO.m18566a(ShipBuffType.class, C6141ahJ.class, _m_fields, _m_methods);
    }

    /* renamed from: S */
    private void m32554S(boolean z) {
        bFf().mo5608dq().mo3153a(f7876VD, z);
    }

    /* renamed from: d */
    private void m32558d(C3892wO wOVar) {
        bFf().mo5608dq().mo3197f(f7891Vt, wOVar);
    }

    /* renamed from: e */
    private void m32559e(C3892wO wOVar) {
        bFf().mo5608dq().mo3197f(f7893Vv, wOVar);
    }

    /* renamed from: f */
    private void m32560f(C3892wO wOVar) {
        bFf().mo5608dq().mo3197f(f7895Vx, wOVar);
    }

    /* renamed from: g */
    private void m32561g(C3892wO wOVar) {
        bFf().mo5608dq().mo3197f(f7897Vz, wOVar);
    }

    /* renamed from: h */
    private void m32562h(C3892wO wOVar) {
        bFf().mo5608dq().mo3197f(f7874VB, wOVar);
    }

    /* renamed from: yE */
    private C3892wO m32568yE() {
        return (C3892wO) bFf().mo5608dq().mo3214p(f7891Vt);
    }

    /* renamed from: yF */
    private C3892wO m32569yF() {
        return (C3892wO) bFf().mo5608dq().mo3214p(f7893Vv);
    }

    /* renamed from: yG */
    private C3892wO m32570yG() {
        return (C3892wO) bFf().mo5608dq().mo3214p(f7895Vx);
    }

    /* renamed from: yH */
    private C3892wO m32571yH() {
        return (C3892wO) bFf().mo5608dq().mo3214p(f7897Vz);
    }

    /* renamed from: yI */
    private C3892wO m32572yI() {
        return (C3892wO) bFf().mo5608dq().mo3214p(f7874VB);
    }

    /* renamed from: yJ */
    private boolean m32573yJ() {
        return bFf().mo5608dq().mo3201h(f7876VD);
    }

    /* renamed from: T */
    public C6494any mo11T() {
        return ___iScriptClass;
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Disable Components")
    /* renamed from: U */
    public void mo19240U(boolean z) {
        switch (bFf().mo6893i(f7888VP)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f7888VP, new Object[]{new Boolean(z)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f7888VP, new Object[]{new Boolean(z)}));
                break;
        }
        m32555T(z);
    }

    /* renamed from: U */
    public C2491fm[] mo12U() {
        return _m_methods;
    }

    /* renamed from: W */
    public Object mo13W() {
        return new C6141ahJ(this);
    }

    /* renamed from: a */
    public Object mo14a(C0495Gr gr) {
        Object[] args = gr.getArgs();
        switch (gr.mo2417hq() - AttributeBuffType._m_methodCount) {
            case 0:
                return m32574yK();
            case 1:
                m32563i((C3892wO) args[0]);
                return null;
            case 2:
                return m32575yM();
            case 3:
                m32564k((C3892wO) args[0]);
                return null;
            case 4:
                return m32576yO();
            case 5:
                m32565m((C3892wO) args[0]);
                return null;
            case 6:
                return m32577yQ();
            case 7:
                m32566o((C3892wO) args[0]);
                return null;
            case 8:
                return m32578yS();
            case 9:
                m32567q((C3892wO) args[0]);
                return null;
            case 10:
                return new Boolean(m32579yU());
            case 11:
                m32555T(((Boolean) args[0]).booleanValue());
                return null;
            case 12:
                return m32580yW();
            case 13:
                return m32557aT();
            default:
                return super.mo14a(gr);
        }
    }

    /* renamed from: a */
    public void mo15a(Collection collection, C0495Gr gr) {
        super.mo15a(collection, gr);
    }

    /* access modifiers changed from: protected */
    /* renamed from: aU */
    public <T extends aDJ> T mo745aU() {
        switch (bFf().mo6893i(f7898dN)) {
            case 0:
                return null;
            case 2:
                return (aDJ) bFf().mo5606d(new aCE(this, f7898dN, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f7898dN, new Object[0]));
                break;
        }
        return m32557aT();
    }

    /* renamed from: c */
    public C5663aRz[] mo25c() {
        return _m_fields;
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Linear Velocity")
    /* renamed from: j */
    public void mo19241j(C3892wO wOVar) {
        switch (bFf().mo6893i(f7878VF)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f7878VF, new Object[]{wOVar}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f7878VF, new Object[]{wOVar}));
                break;
        }
        m32563i(wOVar);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Linear Acceleration")
    /* renamed from: l */
    public void mo19242l(C3892wO wOVar) {
        switch (bFf().mo6893i(f7880VH)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f7880VH, new Object[]{wOVar}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f7880VH, new Object[]{wOVar}));
                break;
        }
        m32564k(wOVar);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Angular Velocity")
    /* renamed from: n */
    public void mo19243n(C3892wO wOVar) {
        switch (bFf().mo6893i(f7882VJ)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f7882VJ, new Object[]{wOVar}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f7882VJ, new Object[]{wOVar}));
                break;
        }
        m32565m(wOVar);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Angular Acceleration")
    /* renamed from: p */
    public void mo19244p(C3892wO wOVar) {
        switch (bFf().mo6893i(f7884VL)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f7884VL, new Object[]{wOVar}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f7884VL, new Object[]{wOVar}));
                break;
        }
        m32566o(wOVar);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Radar Range")
    /* renamed from: r */
    public void mo19245r(C3892wO wOVar) {
        switch (bFf().mo6893i(f7886VN)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f7886VN, new Object[]{wOVar}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f7886VN, new Object[]{wOVar}));
                break;
        }
        m32567q(wOVar);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Linear Velocity")
    /* renamed from: yL */
    public C3892wO mo19246yL() {
        switch (bFf().mo6893i(f7877VE)) {
            case 0:
                return null;
            case 2:
                return (C3892wO) bFf().mo5606d(new aCE(this, f7877VE, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f7877VE, new Object[0]));
                break;
        }
        return m32574yK();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Linear Acceleration")
    /* renamed from: yN */
    public C3892wO mo19247yN() {
        switch (bFf().mo6893i(f7879VG)) {
            case 0:
                return null;
            case 2:
                return (C3892wO) bFf().mo5606d(new aCE(this, f7879VG, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f7879VG, new Object[0]));
                break;
        }
        return m32575yM();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Angular Velocity")
    /* renamed from: yP */
    public C3892wO mo19248yP() {
        switch (bFf().mo6893i(f7881VI)) {
            case 0:
                return null;
            case 2:
                return (C3892wO) bFf().mo5606d(new aCE(this, f7881VI, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f7881VI, new Object[0]));
                break;
        }
        return m32576yO();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Angular Acceleration")
    /* renamed from: yR */
    public C3892wO mo19249yR() {
        switch (bFf().mo6893i(f7883VK)) {
            case 0:
                return null;
            case 2:
                return (C3892wO) bFf().mo5606d(new aCE(this, f7883VK, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f7883VK, new Object[0]));
                break;
        }
        return m32577yQ();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Radar Range")
    /* renamed from: yT */
    public C3892wO mo19250yT() {
        switch (bFf().mo6893i(f7885VM)) {
            case 0:
                return null;
            case 2:
                return (C3892wO) bFf().mo5606d(new aCE(this, f7885VM, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f7885VM, new Object[0]));
                break;
        }
        return m32578yS();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Disable Components")
    /* renamed from: yV */
    public boolean mo19251yV() {
        switch (bFf().mo6893i(f7887VO)) {
            case 0:
                return false;
            case 2:
                return ((Boolean) bFf().mo5606d(new aCE(this, f7887VO, new Object[0]))).booleanValue();
            case 3:
                bFf().mo5606d(new aCE(this, f7887VO, new Object[0]));
                break;
        }
        return m32579yU();
    }

    /* renamed from: yX */
    public ShipBuff mo19252yX() {
        switch (bFf().mo6893i(f7889VQ)) {
            case 0:
                return null;
            case 2:
                return (ShipBuff) bFf().mo5606d(new aCE(this, f7889VQ, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f7889VQ, new Object[0]));
                break;
        }
        return m32580yW();
    }

    /* renamed from: S */
    public void mo10S() {
        super.mo10S();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Linear Velocity")
    @C0064Am(aul = "40c31d2d7fe033c19f8b8c7779a4e2ab", aum = 0)
    /* renamed from: yK */
    private C3892wO m32574yK() {
        return m32568yE();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Linear Velocity")
    @C0064Am(aul = "cef57a56c59fcf20bfdffbb6664b1c62", aum = 0)
    /* renamed from: i */
    private void m32563i(C3892wO wOVar) {
        m32558d(wOVar);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Linear Acceleration")
    @C0064Am(aul = "77cd2a9f134b963f61766a7ad1e891f7", aum = 0)
    /* renamed from: yM */
    private C3892wO m32575yM() {
        return m32569yF();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Linear Acceleration")
    @C0064Am(aul = "35fe5246b18a0fd8a8ea393e94ce406b", aum = 0)
    /* renamed from: k */
    private void m32564k(C3892wO wOVar) {
        m32559e(wOVar);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Angular Velocity")
    @C0064Am(aul = "116b6ca1f8c8cd7f04c4859e70d4a4a2", aum = 0)
    /* renamed from: yO */
    private C3892wO m32576yO() {
        return m32570yG();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Angular Velocity")
    @C0064Am(aul = "80e1519ffef3c0b14c93f27e6fa34797", aum = 0)
    /* renamed from: m */
    private void m32565m(C3892wO wOVar) {
        m32560f(wOVar);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Angular Acceleration")
    @C0064Am(aul = "1d98736ac2260552179c4efda51f62e7", aum = 0)
    /* renamed from: yQ */
    private C3892wO m32577yQ() {
        return m32571yH();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Angular Acceleration")
    @C0064Am(aul = "b0ad35e83e1ae918057c1673449f21e7", aum = 0)
    /* renamed from: o */
    private void m32566o(C3892wO wOVar) {
        m32561g(wOVar);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Radar Range")
    @C0064Am(aul = "75bfed8898b274a7b1d7fe9237c322fc", aum = 0)
    /* renamed from: yS */
    private C3892wO m32578yS() {
        return m32572yI();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Radar Range")
    @C0064Am(aul = "d5f768896d59f7be245d2d28db93ffac", aum = 0)
    /* renamed from: q */
    private void m32567q(C3892wO wOVar) {
        m32562h(wOVar);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Disable Components")
    @C0064Am(aul = "bdf21d4fc21717fec99fdb80146aa011", aum = 0)
    /* renamed from: yU */
    private boolean m32579yU() {
        return m32573yJ();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Disable Components")
    @C0064Am(aul = "a7f99f4ccdad9f9b4f7b8ecc7d963bbe", aum = 0)
    /* renamed from: T */
    private void m32555T(boolean z) {
        m32554S(z);
    }

    @C0064Am(aul = "84598583ed3b4b61fbaca349b641419f", aum = 0)
    /* renamed from: yW */
    private ShipBuff m32580yW() {
        return (ShipBuff) mo745aU();
    }

    @C0064Am(aul = "8ec6786499d2c1bf82298fcbfd58f879", aum = 0)
    /* renamed from: aT */
    private <T extends aDJ> T m32557aT() {
        T t = (ShipBuff) bFf().mo6865M(ShipBuff.class);
        t.mo21924a(this);
        return t;
    }
}
