package game.script.item.buff.amplifier;

import game.network.message.externalizable.aCE;
import game.network.message.serializable.C3438ra;
import game.script.item.AmplifierType;
import logic.baa.*;
import logic.data.mbean.C5294aDu;
import logic.res.code.C5640aRc;
import logic.res.code.C5663aRz;
import logic.res.code.C6494any;
import logic.res.html.C2491fm;
import p001a.*;

import java.util.Collection;
import java.util.Collections;
import java.util.List;

@C0566Hp
@C5829abJ("2.0.1")
@C6485anp
@C5511aMd
/* renamed from: a.aJM */
/* compiled from: a */
public class ComposedAmplifierType extends AmplifierType implements C1616Xf {

    /* renamed from: Pm */
    public static final C2491fm f3192Pm = null;

    /* renamed from: Pn */
    public static final C2491fm f3193Pn = null;
    public static final int _m_fieldCount = 0;
    public static final C5663aRz[] _m_fields = null;
    public static final int _m_methodCount = 0;
    public static final C2491fm[] _m_methods = null;
    public static final C5663aRz cRq = null;
    /* renamed from: dN */
    public static final C2491fm f3194dN = null;
    public static final C2491fm igB = null;
    public static final C2491fm igC = null;
    public static final C2491fm igD = null;
    public static final long serialVersionUID = 0;
    public static C6494any ___iScriptClass;
    @C0064Am(aul = "fbdcea3959fbef1dc8424af2944e5958", aum = 0)
    private static C3438ra<AmplifierType> cRp;

    static {
        m15626V();
    }

    public ComposedAmplifierType() {
        super((C5540aNg) null);
        super.mo10S();
    }

    public ComposedAmplifierType(C5540aNg ang) {
        super(ang);
    }

    /* renamed from: V */
    static void m15626V() {
        serialVersionUID = (long) 1;
        _m_fieldCount = AmplifierType._m_fieldCount + 1;
        _m_methodCount = AmplifierType._m_methodCount + 6;
        int i = AmplifierType._m_fieldCount;
        C5663aRz[] arzArr = new C5663aRz[(i + 1)];
        C5663aRz b = C5640aRc.m17844b(ComposedAmplifierType.class, "fbdcea3959fbef1dc8424af2944e5958", i);
        cRq = b;
        arzArr[i] = b;
        int i2 = i + 1;
        _m_fields = arzArr;
        C1634Xv.m11725a((Object[]) AmplifierType._m_fields, (Object[]) _m_fields);
        int i3 = AmplifierType._m_methodCount;
        C2491fm[] fmVarArr = new C2491fm[(i3 + 6)];
        C2491fm a = C4105zY.m41624a(ComposedAmplifierType.class, "ba7a93dbd079bc9526005d3d723f97d3", i3);
        f3192Pm = a;
        fmVarArr[i3] = a;
        int i4 = i3 + 1;
        C2491fm a2 = C4105zY.m41624a(ComposedAmplifierType.class, "c54f6d0d2b9350edd60c81ed045d0dc4", i4);
        f3193Pn = a2;
        fmVarArr[i4] = a2;
        int i5 = i4 + 1;
        C2491fm a3 = C4105zY.m41624a(ComposedAmplifierType.class, "434f1b80021833a7d847ca8b41ed257b", i5);
        igB = a3;
        fmVarArr[i5] = a3;
        int i6 = i5 + 1;
        C2491fm a4 = C4105zY.m41624a(ComposedAmplifierType.class, "4304eb57d6ebb2596822a35033ddbaa2", i6);
        igC = a4;
        fmVarArr[i6] = a4;
        int i7 = i6 + 1;
        C2491fm a5 = C4105zY.m41624a(ComposedAmplifierType.class, "536eae305b9b1bb16ec3bf30c9bd6b8f", i7);
        igD = a5;
        fmVarArr[i7] = a5;
        int i8 = i7 + 1;
        C2491fm a6 = C4105zY.m41624a(ComposedAmplifierType.class, "88b8e01c9521f4fd0ed46631127262df", i8);
        f3194dN = a6;
        fmVarArr[i8] = a6;
        int i9 = i8 + 1;
        _m_methods = fmVarArr;
        C1634Xv.m11725a((Object[]) AmplifierType._m_methods, (Object[]) _m_methods);
        ___iScriptClass = aUO.m18566a(ComposedAmplifierType.class, C5294aDu.class, _m_fields, _m_methods);
    }

    private C3438ra aNL() {
        return (C3438ra) bFf().mo5608dq().mo3214p(cRq);
    }

    /* renamed from: aY */
    private void m15629aY(C3438ra raVar) {
        bFf().mo5608dq().mo3197f(cRq, raVar);
    }

    /* renamed from: T */
    public C6494any mo11T() {
        return ___iScriptClass;
    }

    /* renamed from: U */
    public C2491fm[] mo12U() {
        return _m_methods;
    }

    /* renamed from: W */
    public Object mo13W() {
        return new C5294aDu(this);
    }

    /* renamed from: a */
    public Object mo14a(C0495Gr gr) {
        Object[] args = gr.getArgs();
        switch (gr.mo2417hq() - AmplifierType._m_methodCount) {
            case 0:
                m15627a((AmplifierType) args[0]);
                return null;
            case 1:
                m15630c((AmplifierType) args[0]);
                return null;
            case 2:
                return dfI();
            case 3:
                return dfK();
            case 4:
                return dfM();
            case 5:
                return m15628aT();
            default:
                return super.mo14a(gr);
        }
    }

    /* renamed from: a */
    public void mo15a(Collection collection, C0495Gr gr) {
        super.mo15a(collection, gr);
    }

    /* access modifiers changed from: protected */
    /* renamed from: aU */
    public <T extends aDJ> T mo745aU() {
        switch (bFf().mo6893i(f3194dN)) {
            case 0:
                return null;
            case 2:
                return (aDJ) bFf().mo5606d(new aCE(this, f3194dN, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f3194dN, new Object[0]));
                break;
        }
        return m15628aT();
    }

    @C3248pc(aYR = C3248pc.C3250b.ADDER, displayName = "Amplifier Types")
    /* renamed from: b */
    public void mo9469b(AmplifierType iDVar) {
        switch (bFf().mo6893i(f3192Pm)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f3192Pm, new Object[]{iDVar}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f3192Pm, new Object[]{iDVar}));
                break;
        }
        m15627a(iDVar);
    }

    /* renamed from: c */
    public C5663aRz[] mo25c() {
        return _m_fields;
    }

    @C3248pc(aYR = C3248pc.C3250b.REMOVER, displayName = "Amplifier Types")
    /* renamed from: d */
    public void mo9470d(AmplifierType iDVar) {
        switch (bFf().mo6893i(f3193Pn)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f3193Pn, new Object[]{iDVar}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f3193Pn, new Object[]{iDVar}));
                break;
        }
        m15630c(iDVar);
    }

    public C3438ra<AmplifierType> dfJ() {
        switch (bFf().mo6893i(igB)) {
            case 0:
                return null;
            case 2:
                return (C3438ra) bFf().mo5606d(new aCE(this, igB, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, igB, new Object[0]));
                break;
        }
        return dfI();
    }

    @C3248pc(aYR = C3248pc.C3250b.ITERATOR, displayName = "Amplifier Types")
    public List<AmplifierType> dfL() {
        switch (bFf().mo6893i(igC)) {
            case 0:
                return null;
            case 2:
                return (List) bFf().mo5606d(new aCE(this, igC, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, igC, new Object[0]));
                break;
        }
        return dfK();
    }

    public ComposedAmplifier dfN() {
        switch (bFf().mo6893i(igD)) {
            case 0:
                return null;
            case 2:
                return (ComposedAmplifier) bFf().mo5606d(new aCE(this, igD, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, igD, new Object[0]));
                break;
        }
        return dfM();
    }

    /* renamed from: S */
    public void mo10S() {
        super.mo10S();
    }

    @C3248pc(aYR = C3248pc.C3250b.ADDER, displayName = "Amplifier Types")
    @C0064Am(aul = "ba7a93dbd079bc9526005d3d723f97d3", aum = 0)
    /* renamed from: a */
    private void m15627a(AmplifierType iDVar) {
        aNL().add(iDVar);
    }

    @C3248pc(aYR = C3248pc.C3250b.REMOVER, displayName = "Amplifier Types")
    @C0064Am(aul = "c54f6d0d2b9350edd60c81ed045d0dc4", aum = 0)
    /* renamed from: c */
    private void m15630c(AmplifierType iDVar) {
        aNL().remove(iDVar);
    }

    @C0064Am(aul = "434f1b80021833a7d847ca8b41ed257b", aum = 0)
    private C3438ra<AmplifierType> dfI() {
        return aNL();
    }

    @C3248pc(aYR = C3248pc.C3250b.ITERATOR, displayName = "Amplifier Types")
    @C0064Am(aul = "4304eb57d6ebb2596822a35033ddbaa2", aum = 0)
    private List<AmplifierType> dfK() {
        return Collections.unmodifiableList(aNL());
    }

    @C0064Am(aul = "536eae305b9b1bb16ec3bf30c9bd6b8f", aum = 0)
    private ComposedAmplifier dfM() {
        return (ComposedAmplifier) mo745aU();
    }

    @C0064Am(aul = "88b8e01c9521f4fd0ed46631127262df", aum = 0)
    /* renamed from: aT */
    private <T extends aDJ> T m15628aT() {
        T t = (ComposedAmplifier) bFf().mo6865M(ComposedAmplifier.class);
        t.mo1956e(this);
        return t;
    }
}
