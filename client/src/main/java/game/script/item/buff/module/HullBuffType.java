package game.script.item.buff.module;

import game.network.message.externalizable.aCE;
import logic.baa.*;
import logic.data.mbean.C0745Ke;
import logic.res.code.C5640aRc;
import logic.res.code.C5663aRz;
import logic.res.code.C6494any;
import logic.res.html.C2491fm;
import p001a.*;

import java.util.Collection;

@C0566Hp
@C5829abJ("1.1.2")
@C6485anp
@C5511aMd
/* renamed from: a.aES */
/* compiled from: a */
public class HullBuffType extends AttributeBuffType implements C1616Xf {
    public static final int _m_fieldCount = 0;
    public static final C5663aRz[] _m_fields = null;
    public static final int _m_methodCount = 0;
    public static final C2491fm[] _m_methods = null;
    public static final C5663aRz aOJ = null;
    public static final C5663aRz aOr = null;
    public static final C2491fm cqF = null;
    public static final C2491fm cqG = null;
    public static final C2491fm cqH = null;
    public static final C2491fm cqI = null;
    /* renamed from: dN */
    public static final C2491fm f2688dN = null;
    public static final C2491fm hId = null;
    public static final long serialVersionUID = 0;
    public static C6494any ___iScriptClass;
    @C0064Am(aul = "dddf0a3c3211437f807eecb828aa1677", aum = 0)
    private static C3892wO aBJ;
    @C0064Am(aul = "0f985626bc0fea669b96f7c3084b1f1b", aum = 1)
    private static C3892wO aBK;

    static {
        m14164V();
    }

    public HullBuffType() {
        super((C5540aNg) null);
        super.mo10S();
    }

    public HullBuffType(C5540aNg ang) {
        super(ang);
    }

    /* renamed from: V */
    static void m14164V() {
        serialVersionUID = (long) 1;
        _m_fieldCount = AttributeBuffType._m_fieldCount + 2;
        _m_methodCount = AttributeBuffType._m_methodCount + 6;
        int i = AttributeBuffType._m_fieldCount;
        C5663aRz[] arzArr = new C5663aRz[(i + 2)];
        C5663aRz b = C5640aRc.m17844b(HullBuffType.class, "dddf0a3c3211437f807eecb828aa1677", i);
        aOJ = b;
        arzArr[i] = b;
        int i2 = i + 1;
        C5663aRz b2 = C5640aRc.m17844b(HullBuffType.class, "0f985626bc0fea669b96f7c3084b1f1b", i2);
        aOr = b2;
        arzArr[i2] = b2;
        int i3 = i2 + 1;
        _m_fields = arzArr;
        C1634Xv.m11725a((Object[]) AttributeBuffType._m_fields, (Object[]) _m_fields);
        int i4 = AttributeBuffType._m_methodCount;
        C2491fm[] fmVarArr = new C2491fm[(i4 + 6)];
        C2491fm a = C4105zY.m41624a(HullBuffType.class, "8a3b28575b9e1ad6bffdb0945fdcdaae", i4);
        cqF = a;
        fmVarArr[i4] = a;
        int i5 = i4 + 1;
        C2491fm a2 = C4105zY.m41624a(HullBuffType.class, "4728a12d0d802ea5ef29b84183e7084a", i5);
        cqG = a2;
        fmVarArr[i5] = a2;
        int i6 = i5 + 1;
        C2491fm a3 = C4105zY.m41624a(HullBuffType.class, "1f479ec7290f0d93a6b4c42e904b2b01", i6);
        cqH = a3;
        fmVarArr[i6] = a3;
        int i7 = i6 + 1;
        C2491fm a4 = C4105zY.m41624a(HullBuffType.class, "e9c9a543f109d63a7d1f8c38da337db8", i7);
        cqI = a4;
        fmVarArr[i7] = a4;
        int i8 = i7 + 1;
        C2491fm a5 = C4105zY.m41624a(HullBuffType.class, "8c0782475fabb1769581a2d1fbbf5554", i8);
        hId = a5;
        fmVarArr[i8] = a5;
        int i9 = i8 + 1;
        C2491fm a6 = C4105zY.m41624a(HullBuffType.class, "ed4fb5cd1083c3188b73d14590906d86", i9);
        f2688dN = a6;
        fmVarArr[i9] = a6;
        int i10 = i9 + 1;
        _m_methods = fmVarArr;
        C1634Xv.m11725a((Object[]) AttributeBuffType._m_methods, (Object[]) _m_methods);
        ___iScriptClass = aUO.m18566a(HullBuffType.class, C0745Ke.class, _m_fields, _m_methods);
    }

    /* renamed from: Ub */
    private C3892wO m14162Ub() {
        return (C3892wO) bFf().mo5608dq().mo3214p(aOJ);
    }

    /* renamed from: Uc */
    private C3892wO m14163Uc() {
        return (C3892wO) bFf().mo5608dq().mo3214p(aOr);
    }

    /* renamed from: v */
    private void m14166v(C3892wO wOVar) {
        bFf().mo5608dq().mo3197f(aOJ, wOVar);
    }

    /* renamed from: w */
    private void m14167w(C3892wO wOVar) {
        bFf().mo5608dq().mo3197f(aOr, wOVar);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Health Points")
    /* renamed from: H */
    public void mo8604H(C3892wO wOVar) {
        switch (bFf().mo6893i(cqG)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, cqG, new Object[]{wOVar}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, cqG, new Object[]{wOVar}));
                break;
        }
        m14160G(wOVar);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Damage Reduction")
    /* renamed from: J */
    public void mo8605J(C3892wO wOVar) {
        switch (bFf().mo6893i(cqI)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, cqI, new Object[]{wOVar}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, cqI, new Object[]{wOVar}));
                break;
        }
        m14161I(wOVar);
    }

    /* renamed from: T */
    public C6494any mo11T() {
        return ___iScriptClass;
    }

    /* renamed from: U */
    public C2491fm[] mo12U() {
        return _m_methods;
    }

    /* renamed from: W */
    public Object mo13W() {
        return new C0745Ke(this);
    }

    /* renamed from: a */
    public Object mo14a(C0495Gr gr) {
        Object[] args = gr.getArgs();
        switch (gr.mo2417hq() - AttributeBuffType._m_methodCount) {
            case 0:
                return aBb();
            case 1:
                m14160G((C3892wO) args[0]);
                return null;
            case 2:
                return aBd();
            case 3:
                m14161I((C3892wO) args[0]);
                return null;
            case 4:
                return cYd();
            case 5:
                return m14165aT();
            default:
                return super.mo14a(gr);
        }
    }

    /* renamed from: a */
    public void mo15a(Collection collection, C0495Gr gr) {
        super.mo15a(collection, gr);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Health Points")
    public C3892wO aBc() {
        switch (bFf().mo6893i(cqF)) {
            case 0:
                return null;
            case 2:
                return (C3892wO) bFf().mo5606d(new aCE(this, cqF, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, cqF, new Object[0]));
                break;
        }
        return aBb();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Damage Reduction")
    public C3892wO aBe() {
        switch (bFf().mo6893i(cqH)) {
            case 0:
                return null;
            case 2:
                return (C3892wO) bFf().mo5606d(new aCE(this, cqH, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, cqH, new Object[0]));
                break;
        }
        return aBd();
    }

    /* access modifiers changed from: protected */
    /* renamed from: aU */
    public <T extends aDJ> T mo745aU() {
        switch (bFf().mo6893i(f2688dN)) {
            case 0:
                return null;
            case 2:
                return (aDJ) bFf().mo5606d(new aCE(this, f2688dN, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f2688dN, new Object[0]));
                break;
        }
        return m14165aT();
    }

    /* renamed from: c */
    public C5663aRz[] mo25c() {
        return _m_fields;
    }

    public HullBuff cYe() {
        switch (bFf().mo6893i(hId)) {
            case 0:
                return null;
            case 2:
                return (HullBuff) bFf().mo5606d(new aCE(this, hId, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, hId, new Object[0]));
                break;
        }
        return cYd();
    }

    /* renamed from: S */
    public void mo10S() {
        super.mo10S();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Health Points")
    @C0064Am(aul = "8a3b28575b9e1ad6bffdb0945fdcdaae", aum = 0)
    private C3892wO aBb() {
        return m14162Ub();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Health Points")
    @C0064Am(aul = "4728a12d0d802ea5ef29b84183e7084a", aum = 0)
    /* renamed from: G */
    private void m14160G(C3892wO wOVar) {
        m14166v(wOVar);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Damage Reduction")
    @C0064Am(aul = "1f479ec7290f0d93a6b4c42e904b2b01", aum = 0)
    private C3892wO aBd() {
        return m14163Uc();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Damage Reduction")
    @C0064Am(aul = "e9c9a543f109d63a7d1f8c38da337db8", aum = 0)
    /* renamed from: I */
    private void m14161I(C3892wO wOVar) {
        m14167w(wOVar);
    }

    @C0064Am(aul = "8c0782475fabb1769581a2d1fbbf5554", aum = 0)
    private HullBuff cYd() {
        return (HullBuff) mo745aU();
    }

    @C0064Am(aul = "ed4fb5cd1083c3188b73d14590906d86", aum = 0)
    /* renamed from: aT */
    private <T extends aDJ> T m14165aT() {
        T t = (HullBuff) bFf().mo6865M(HullBuff.class);
        t.mo21104a(this);
        return t;
    }
}
