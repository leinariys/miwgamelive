package game.script.item.buff.module;

import game.network.message.externalizable.aCE;
import logic.baa.C1616Xf;
import logic.baa.C1634Xv;
import logic.baa.C5511aMd;
import logic.baa.C6485anp;
import logic.data.mbean.C2755jZ;
import logic.res.code.C5640aRc;
import logic.res.code.C5663aRz;
import logic.res.code.C6494any;
import logic.res.html.C2491fm;
import p001a.*;

import java.util.Collection;

@C5829abJ("1.1.2")
@C6485anp
@C2712iu(version = "1.1.2")
@C5511aMd
/* renamed from: a.aBJ */
/* compiled from: a */
public class Healer extends AttributeBuff implements C1616Xf {
    public static final C2491fm _f_onResurrect_0020_0028_0029V = null;
    public static final C2491fm _f_tick_0020_0028F_0029V = null;
    public static final int _m_fieldCount = 0;
    public static final C5663aRz[] _m_fields = null;
    public static final int _m_methodCount = 0;
    public static final C2491fm[] _m_methods = null;
    public static final C2491fm aPH = null;
    public static final C2491fm aPI = null;
    public static final C5663aRz cSd = null;
    public static final C5663aRz cSe = null;
    public static final C5663aRz hhJ = null;
    public static final C5663aRz hhK = null;
    public static final C5663aRz hhL = null;
    public static final long serialVersionUID = 0;
    public static C6494any ___iScriptClass;
    @C0064Am(aul = "54a3272b23aa89be0f3f94e0fbc70715", aum = 0)
    private static float apV;
    @C0064Am(aul = "1f7cf52ab1fb2a889393b05298242a57", aum = 1)
    private static float apW;
    @C0064Am(aul = "e58366d4c5c20bc3726d17f38a9fbbc6", aum = 2)
    private static float apX;
    @C0064Am(aul = "09bafe8ec9fbee12370d9b719b5fdc83", aum = 3)
    private static float apY;
    @C0064Am(aul = "d0dc992c7ecb3ab2ef2bb99a33f71790", aum = 4)
    private static boolean apZ;

    static {
        m12784V();
    }

    public Healer() {
        super((C5540aNg) null);
        super.mo10S();
    }

    public Healer(HealerType eu) {
        super((C5540aNg) null);
        super._m_script_init(eu);
    }

    public Healer(C5540aNg ang) {
        super(ang);
    }

    /* renamed from: V */
    static void m12784V() {
        serialVersionUID = (long) 1;
        _m_fieldCount = AttributeBuff._m_fieldCount + 5;
        _m_methodCount = AttributeBuff._m_methodCount + 4;
        int i = AttributeBuff._m_fieldCount;
        C5663aRz[] arzArr = new C5663aRz[(i + 5)];
        C5663aRz b = C5640aRc.m17844b(Healer.class, "54a3272b23aa89be0f3f94e0fbc70715", i);
        cSd = b;
        arzArr[i] = b;
        int i2 = i + 1;
        C5663aRz b2 = C5640aRc.m17844b(Healer.class, "1f7cf52ab1fb2a889393b05298242a57", i2);
        cSe = b2;
        arzArr[i2] = b2;
        int i3 = i2 + 1;
        C5663aRz b3 = C5640aRc.m17844b(Healer.class, "e58366d4c5c20bc3726d17f38a9fbbc6", i3);
        hhJ = b3;
        arzArr[i3] = b3;
        int i4 = i3 + 1;
        C5663aRz b4 = C5640aRc.m17844b(Healer.class, "09bafe8ec9fbee12370d9b719b5fdc83", i4);
        hhK = b4;
        arzArr[i4] = b4;
        int i5 = i4 + 1;
        C5663aRz b5 = C5640aRc.m17844b(Healer.class, "d0dc992c7ecb3ab2ef2bb99a33f71790", i5);
        hhL = b5;
        arzArr[i5] = b5;
        int i6 = i5 + 1;
        _m_fields = arzArr;
        C1634Xv.m11725a((Object[]) AttributeBuff._m_fields, (Object[]) _m_fields);
        int i7 = AttributeBuff._m_methodCount;
        C2491fm[] fmVarArr = new C2491fm[(i7 + 4)];
        C2491fm a = C4105zY.m41624a(Healer.class, "0621ee7952aa15a99e85ab7f23d751eb", i7);
        aPH = a;
        fmVarArr[i7] = a;
        int i8 = i7 + 1;
        C2491fm a2 = C4105zY.m41624a(Healer.class, "76194e6176d242ac7a6d3583c8d5484c", i8);
        aPI = a2;
        fmVarArr[i8] = a2;
        int i9 = i8 + 1;
        C2491fm a3 = C4105zY.m41624a(Healer.class, "e0a33e0fccc65f8702cd9ee27f3c1b85", i9);
        _f_tick_0020_0028F_0029V = a3;
        fmVarArr[i9] = a3;
        int i10 = i9 + 1;
        C2491fm a4 = C4105zY.m41624a(Healer.class, "cd099513251dce71d61c16dbb66f6949", i10);
        _f_onResurrect_0020_0028_0029V = a4;
        fmVarArr[i10] = a4;
        int i11 = i10 + 1;
        _m_methods = fmVarArr;
        C1634Xv.m11725a((Object[]) AttributeBuff._m_methods, (Object[]) _m_methods);
        ___iScriptClass = aUO.m18566a(Healer.class, C2755jZ.class, _m_fields, _m_methods);
    }

    @C0064Am(aul = "e0a33e0fccc65f8702cd9ee27f3c1b85", aum = 0)
    @C5566aOg
    /* renamed from: U */
    private void m12781U(float f) {
        throw new aWi(new aCE(this, _f_tick_0020_0028F_0029V, new Object[]{new Float(f)}));
    }

    @C0064Am(aul = "0621ee7952aa15a99e85ab7f23d751eb", aum = 0)
    @C5566aOg
    /* renamed from: Ue */
    private void m12782Ue() {
        throw new aWi(new aCE(this, aPH, new Object[0]));
    }

    @C0064Am(aul = "76194e6176d242ac7a6d3583c8d5484c", aum = 0)
    @C5566aOg
    /* renamed from: Ug */
    private void m12783Ug() {
        throw new aWi(new aCE(this, aPI, new Object[0]));
    }

    private float aNV() {
        return ((HealerType) getType()).aNY();
    }

    private float aNW() {
        return ((HealerType) getType()).aOa();
    }

    private boolean cJA() {
        return bFf().mo5608dq().mo3201h(hhL);
    }

    private float cJy() {
        return bFf().mo5608dq().mo3211m(hhJ);
    }

    private float cJz() {
        return bFf().mo5608dq().mo3211m(hhK);
    }

    /* renamed from: ff */
    private void m12786ff(float f) {
        throw new C6039afL();
    }

    /* renamed from: fg */
    private void m12787fg(float f) {
        throw new C6039afL();
    }

    /* renamed from: hR */
    private void m12788hR(boolean z) {
        bFf().mo5608dq().mo3153a(hhL, z);
    }

    /* renamed from: lo */
    private void m12789lo(float f) {
        bFf().mo5608dq().mo3150a(hhJ, f);
    }

    /* renamed from: lp */
    private void m12790lp(float f) {
        bFf().mo5608dq().mo3150a(hhK, f);
    }

    /* renamed from: S */
    public void mo10S() {
        super.mo10S();
    }

    /* renamed from: T */
    public C6494any mo11T() {
        return ___iScriptClass;
    }

    /* renamed from: U */
    public C2491fm[] mo12U() {
        return _m_methods;
    }

    @C5566aOg
    /* renamed from: Uf */
    public void mo3751Uf() {
        switch (bFf().mo6893i(aPH)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, aPH, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, aPH, new Object[0]));
                break;
        }
        m12782Ue();
    }

    @C5566aOg
    /* renamed from: Uh */
    public void mo3752Uh() {
        switch (bFf().mo6893i(aPI)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, aPI, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, aPI, new Object[0]));
                break;
        }
        m12783Ug();
    }

    @C5566aOg
    /* renamed from: V */
    public void mo4709V(float f) {
        switch (bFf().mo6893i(_f_tick_0020_0028F_0029V)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, _f_tick_0020_0028F_0029V, new Object[]{new Float(f)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, _f_tick_0020_0028F_0029V, new Object[]{new Float(f)}));
                break;
        }
        m12781U(f);
    }

    /* renamed from: W */
    public Object mo13W() {
        return new C2755jZ(this);
    }

    /* renamed from: a */
    public Object mo14a(C0495Gr gr) {
        Object[] args = gr.getArgs();
        switch (gr.mo2417hq() - AttributeBuff._m_methodCount) {
            case 0:
                m12782Ue();
                return null;
            case 1:
                m12783Ug();
                return null;
            case 2:
                m12781U(((Float) args[0]).floatValue());
                return null;
            case 3:
                m12785aG();
                return null;
            default:
                return super.mo14a(gr);
        }
    }

    /* renamed from: a */
    public void mo15a(Collection collection, C0495Gr gr) {
        super.mo15a(collection, gr);
    }

    /* renamed from: aH */
    public void mo70aH() {
        switch (bFf().mo6893i(_f_onResurrect_0020_0028_0029V)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, _f_onResurrect_0020_0028_0029V, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, _f_onResurrect_0020_0028_0029V, new Object[0]));
                break;
        }
        m12785aG();
    }

    /* renamed from: c */
    public C5663aRz[] mo25c() {
        return _m_fields;
    }

    /* renamed from: a */
    public void mo7840a(HealerType eu) {
        super.mo3934i(eu);
    }

    @C0064Am(aul = "cd099513251dce71d61c16dbb66f6949", aum = 0)
    /* renamed from: aG */
    private void m12785aG() {
        super.mo70aH();
        if (bHa()) {
            m12788hR(false);
        }
    }
}
