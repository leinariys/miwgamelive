package game.script.item.buff.amplifier;

import game.network.message.externalizable.aCE;
import game.script.item.AmplifierType;
import logic.baa.*;
import logic.data.mbean.C2347eI;
import logic.res.code.C5640aRc;
import logic.res.code.C5663aRz;
import logic.res.code.C6494any;
import logic.res.html.C2491fm;
import p001a.*;

import java.util.Collection;

@C0566Hp
@C5829abJ("2.0.1")
@C6485anp
@C5511aMd
/* renamed from: a.gh */
/* compiled from: a */
public class CargoHoldAmplifierType extends AmplifierType implements C1616Xf {
    /* renamed from: Pv */
    public static final C5663aRz f7724Pv = null;
    /* renamed from: Pw */
    public static final C2491fm f7725Pw = null;
    /* renamed from: Px */
    public static final C2491fm f7726Px = null;
    /* renamed from: Py */
    public static final C2491fm f7727Py = null;
    public static final int _m_fieldCount = 0;
    public static final C5663aRz[] _m_fields = null;
    public static final int _m_methodCount = 0;
    public static final C2491fm[] _m_methods = null;
    /* renamed from: dN */
    public static final C2491fm f7728dN = null;
    public static final long serialVersionUID = 0;
    public static C6494any ___iScriptClass;
    @C0064Am(aul = "5047e81d8c1f3d258880fc37e1e97209", aum = 0)

    /* renamed from: DL */
    private static C3892wO f7723DL;

    static {
        m32193V();
    }

    public CargoHoldAmplifierType() {
        super((C5540aNg) null);
        super.mo10S();
    }

    public CargoHoldAmplifierType(C5540aNg ang) {
        super(ang);
    }

    /* renamed from: V */
    static void m32193V() {
        serialVersionUID = (long) 1;
        _m_fieldCount = AmplifierType._m_fieldCount + 1;
        _m_methodCount = AmplifierType._m_methodCount + 4;
        int i = AmplifierType._m_fieldCount;
        C5663aRz[] arzArr = new C5663aRz[(i + 1)];
        C5663aRz b = C5640aRc.m17844b(CargoHoldAmplifierType.class, "5047e81d8c1f3d258880fc37e1e97209", i);
        f7724Pv = b;
        arzArr[i] = b;
        int i2 = i + 1;
        _m_fields = arzArr;
        C1634Xv.m11725a((Object[]) AmplifierType._m_fields, (Object[]) _m_fields);
        int i3 = AmplifierType._m_methodCount;
        C2491fm[] fmVarArr = new C2491fm[(i3 + 4)];
        C2491fm a = C4105zY.m41624a(CargoHoldAmplifierType.class, "57d9334231e28edabf5ffc993eb0c58e", i3);
        f7725Pw = a;
        fmVarArr[i3] = a;
        int i4 = i3 + 1;
        C2491fm a2 = C4105zY.m41624a(CargoHoldAmplifierType.class, "2c29a0bd53ba1cdfa9129eb7a30bb51e", i4);
        f7726Px = a2;
        fmVarArr[i4] = a2;
        int i5 = i4 + 1;
        C2491fm a3 = C4105zY.m41624a(CargoHoldAmplifierType.class, "1ef5b6bbe9da392f4697a7ecfa7b4284", i5);
        f7727Py = a3;
        fmVarArr[i5] = a3;
        int i6 = i5 + 1;
        C2491fm a4 = C4105zY.m41624a(CargoHoldAmplifierType.class, "b6351c8b486967f78c1e154c30abd442", i6);
        f7728dN = a4;
        fmVarArr[i6] = a4;
        int i7 = i6 + 1;
        _m_methods = fmVarArr;
        C1634Xv.m11725a((Object[]) AmplifierType._m_methods, (Object[]) _m_methods);
        ___iScriptClass = aUO.m18566a(CargoHoldAmplifierType.class, C2347eI.class, _m_fields, _m_methods);
    }

    /* renamed from: a */
    private void m32194a(C3892wO wOVar) {
        bFf().mo5608dq().mo3197f(f7724Pv, wOVar);
    }

    /* renamed from: vi */
    private C3892wO m32197vi() {
        return (C3892wO) bFf().mo5608dq().mo3214p(f7724Pv);
    }

    /* renamed from: T */
    public C6494any mo11T() {
        return ___iScriptClass;
    }

    /* renamed from: U */
    public C2491fm[] mo12U() {
        return _m_methods;
    }

    /* renamed from: W */
    public Object mo13W() {
        return new C2347eI(this);
    }

    /* renamed from: a */
    public Object mo14a(C0495Gr gr) {
        Object[] args = gr.getArgs();
        switch (gr.mo2417hq() - AmplifierType._m_methodCount) {
            case 0:
                return m32198vj();
            case 1:
                m32196b((C3892wO) args[0]);
                return null;
            case 2:
                return m32199vl();
            case 3:
                return m32195aT();
            default:
                return super.mo14a(gr);
        }
    }

    /* renamed from: a */
    public void mo15a(Collection collection, C0495Gr gr) {
        super.mo15a(collection, gr);
    }

    /* access modifiers changed from: protected */
    /* renamed from: aU */
    public <T extends aDJ> T mo745aU() {
        switch (bFf().mo6893i(f7728dN)) {
            case 0:
                return null;
            case 2:
                return (aDJ) bFf().mo5606d(new aCE(this, f7728dN, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f7728dN, new Object[0]));
                break;
        }
        return m32195aT();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Max Volume")
    /* renamed from: c */
    public void mo19056c(C3892wO wOVar) {
        switch (bFf().mo6893i(f7726Px)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f7726Px, new Object[]{wOVar}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f7726Px, new Object[]{wOVar}));
                break;
        }
        m32196b(wOVar);
    }

    /* renamed from: c */
    public C5663aRz[] mo25c() {
        return _m_fields;
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Max Volume")
    /* renamed from: vk */
    public C3892wO mo19057vk() {
        switch (bFf().mo6893i(f7725Pw)) {
            case 0:
                return null;
            case 2:
                return (C3892wO) bFf().mo5606d(new aCE(this, f7725Pw, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f7725Pw, new Object[0]));
                break;
        }
        return m32198vj();
    }

    /* renamed from: vm */
    public CargoHoldAmplifier mo19058vm() {
        switch (bFf().mo6893i(f7727Py)) {
            case 0:
                return null;
            case 2:
                return (CargoHoldAmplifier) bFf().mo5606d(new aCE(this, f7727Py, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f7727Py, new Object[0]));
                break;
        }
        return m32199vl();
    }

    /* renamed from: S */
    public void mo10S() {
        super.mo10S();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Max Volume")
    @C0064Am(aul = "57d9334231e28edabf5ffc993eb0c58e", aum = 0)
    /* renamed from: vj */
    private C3892wO m32198vj() {
        return m32197vi();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Max Volume")
    @C0064Am(aul = "2c29a0bd53ba1cdfa9129eb7a30bb51e", aum = 0)
    /* renamed from: b */
    private void m32196b(C3892wO wOVar) {
        m32194a(wOVar);
    }

    @C0064Am(aul = "1ef5b6bbe9da392f4697a7ecfa7b4284", aum = 0)
    /* renamed from: vl */
    private CargoHoldAmplifier m32199vl() {
        return (CargoHoldAmplifier) mo745aU();
    }

    @C0064Am(aul = "b6351c8b486967f78c1e154c30abd442", aum = 0)
    /* renamed from: aT */
    private <T extends aDJ> T m32195aT() {
        T t = (CargoHoldAmplifier) bFf().mo6865M(CargoHoldAmplifier.class);
        t.mo1956e(this);
        return t;
    }
}
