package game.script.item.buff.amplifier;

import game.network.message.externalizable.aCE;
import game.script.item.Amplifier;
import game.script.item.AmplifierType;
import game.script.ship.ShipAdapter;
import logic.baa.C1616Xf;
import logic.baa.C1634Xv;
import logic.baa.C5511aMd;
import logic.baa.C6485anp;
import logic.data.mbean.C3951xH;
import logic.data.mbean.C6981axu;
import logic.res.code.C5640aRc;
import logic.res.code.C5663aRz;
import logic.res.code.C6494any;
import logic.res.html.C2491fm;
import p001a.*;

import java.util.Collection;

@C5829abJ("2.0.0")
@C6485anp
@C2712iu(mo19786Bt = AmplifierType.class, version = "2.0.1")
@C5511aMd
/* renamed from: a.qW */
/* compiled from: a */
public class ShipAmplifier extends Amplifier implements C1616Xf {

    /* renamed from: Lm */
    public static final C2491fm f8922Lm = null;
    /* renamed from: VB */
    public static final C5663aRz f8924VB = null;
    /* renamed from: Vt */
    public static final C5663aRz f8926Vt = null;
    /* renamed from: Vv */
    public static final C5663aRz f8928Vv = null;
    /* renamed from: Vx */
    public static final C5663aRz f8930Vx = null;
    /* renamed from: Vz */
    public static final C5663aRz f8932Vz = null;
    public static final int _m_fieldCount = 0;
    public static final C5663aRz[] _m_fields = null;
    public static final int _m_methodCount = 0;
    public static final C2491fm[] _m_methods = null;
    public static final C2491fm aYg = null;
    public static final C2491fm aYh = null;
    public static final C2491fm aYi = null;
    public static final C2491fm aYj = null;
    public static final C5663aRz aZg = null;
    public static final C2491fm aZh = null;
    public static final C2491fm aZi = null;
    public static final C2491fm atW = null;
    public static final C2491fm atX = null;
    public static final long serialVersionUID = 0;
    public static C6494any ___iScriptClass;
    @C0064Am(aul = "e809ff6cade60ffa877623ed9ee3ae01", aum = 4)

    /* renamed from: VA */
    private static C3892wO f8923VA;
    @C0064Am(aul = "0abec28a4c6db63b1b8af6384b313456", aum = 0)

    /* renamed from: Vs */
    private static C3892wO f8925Vs;
    @C0064Am(aul = "58d8edbde89b9b8e3303629fb5117000", aum = 1)

    /* renamed from: Vu */
    private static C3892wO f8927Vu;
    @C0064Am(aul = "0c4e3f30c461394c9128a5636ee74d78", aum = 2)

    /* renamed from: Vw */
    private static C3892wO f8929Vw;
    @C0064Am(aul = "04c8147f3680352dac03812302f8e5e7", aum = 3)

    /* renamed from: Vy */
    private static C3892wO f8931Vy;
    @C0064Am(aul = "4b4ffe4deb7ee9118992cc619ac6f849", aum = 5)
    private static ShipAmplifierAdapter aZf;

    static {
        m37587V();
    }

    public ShipAmplifier() {
        super((C5540aNg) null);
        super.mo10S();
    }

    public ShipAmplifier(C5540aNg ang) {
        super(ang);
    }

    public ShipAmplifier(AmplifierType iDVar) {
        super((C5540aNg) null);
        super.mo1956e(iDVar);
    }

    /* renamed from: V */
    static void m37587V() {
        serialVersionUID = (long) 1;
        _m_fieldCount = Amplifier._m_fieldCount + 6;
        _m_methodCount = Amplifier._m_methodCount + 9;
        int i = Amplifier._m_fieldCount;
        C5663aRz[] arzArr = new C5663aRz[(i + 6)];
        C5663aRz b = C5640aRc.m17844b(ShipAmplifier.class, "0abec28a4c6db63b1b8af6384b313456", i);
        f8926Vt = b;
        arzArr[i] = b;
        int i2 = i + 1;
        C5663aRz b2 = C5640aRc.m17844b(ShipAmplifier.class, "58d8edbde89b9b8e3303629fb5117000", i2);
        f8928Vv = b2;
        arzArr[i2] = b2;
        int i3 = i2 + 1;
        C5663aRz b3 = C5640aRc.m17844b(ShipAmplifier.class, "0c4e3f30c461394c9128a5636ee74d78", i3);
        f8930Vx = b3;
        arzArr[i3] = b3;
        int i4 = i3 + 1;
        C5663aRz b4 = C5640aRc.m17844b(ShipAmplifier.class, "04c8147f3680352dac03812302f8e5e7", i4);
        f8932Vz = b4;
        arzArr[i4] = b4;
        int i5 = i4 + 1;
        C5663aRz b5 = C5640aRc.m17844b(ShipAmplifier.class, "e809ff6cade60ffa877623ed9ee3ae01", i5);
        f8924VB = b5;
        arzArr[i5] = b5;
        int i6 = i5 + 1;
        C5663aRz b6 = C5640aRc.m17844b(ShipAmplifier.class, "4b4ffe4deb7ee9118992cc619ac6f849", i6);
        aZg = b6;
        arzArr[i6] = b6;
        int i7 = i6 + 1;
        _m_fields = arzArr;
        C1634Xv.m11725a((Object[]) Amplifier._m_fields, (Object[]) _m_fields);
        int i8 = Amplifier._m_methodCount;
        C2491fm[] fmVarArr = new C2491fm[(i8 + 9)];
        C2491fm a = C4105zY.m41624a(ShipAmplifier.class, "cda6ac6bd45dabb34a960fb0b7246e54", i8);
        aZh = a;
        fmVarArr[i8] = a;
        int i9 = i8 + 1;
        C2491fm a2 = C4105zY.m41624a(ShipAmplifier.class, "2d0bd63beafab9dcc28412cbc88c6230", i9);
        atW = a2;
        fmVarArr[i9] = a2;
        int i10 = i9 + 1;
        C2491fm a3 = C4105zY.m41624a(ShipAmplifier.class, "4b51cb12022c7a1e511f5668aff8b07c", i10);
        atX = a3;
        fmVarArr[i10] = a3;
        int i11 = i10 + 1;
        C2491fm a4 = C4105zY.m41624a(ShipAmplifier.class, "32a42d5242fce2f7ff2926ce95738eb8", i11);
        aYg = a4;
        fmVarArr[i11] = a4;
        int i12 = i11 + 1;
        C2491fm a5 = C4105zY.m41624a(ShipAmplifier.class, "b7562fe1ad1dd763660efbf50741bb80", i12);
        aYh = a5;
        fmVarArr[i12] = a5;
        int i13 = i12 + 1;
        C2491fm a6 = C4105zY.m41624a(ShipAmplifier.class, "62c9f3418761b704fa7f8b85ea1b9094", i13);
        aYi = a6;
        fmVarArr[i13] = a6;
        int i14 = i13 + 1;
        C2491fm a7 = C4105zY.m41624a(ShipAmplifier.class, "0d019bfd58217ddcd808a2ef60377cc0", i14);
        aYj = a7;
        fmVarArr[i14] = a7;
        int i15 = i14 + 1;
        C2491fm a8 = C4105zY.m41624a(ShipAmplifier.class, "9a3f20bd283301d533c6a08ed2f689f0", i15);
        aZi = a8;
        fmVarArr[i15] = a8;
        int i16 = i15 + 1;
        C2491fm a9 = C4105zY.m41624a(ShipAmplifier.class, "e6409dc23f6fd4511954dd5331d55f2d", i16);
        f8922Lm = a9;
        fmVarArr[i16] = a9;
        int i17 = i16 + 1;
        _m_methods = fmVarArr;
        C1634Xv.m11725a((Object[]) Amplifier._m_methods, (Object[]) _m_methods);
        ___iScriptClass = aUO.m18566a(ShipAmplifier.class, C3951xH.class, _m_fields, _m_methods);
    }

    @C0064Am(aul = "2d0bd63beafab9dcc28412cbc88c6230", aum = 0)
    @C5566aOg
    /* renamed from: Kr */
    private void m37585Kr() {
        throw new aWi(new aCE(this, atW, new Object[0]));
    }

    @C0064Am(aul = "4b51cb12022c7a1e511f5668aff8b07c", aum = 0)
    @C5566aOg
    /* renamed from: Kt */
    private void m37586Kt() {
        throw new aWi(new aCE(this, atX, new Object[0]));
    }

    /* renamed from: Yb */
    private ShipAmplifierAdapter m37592Yb() {
        return (ShipAmplifierAdapter) bFf().mo5608dq().mo3214p(aZg);
    }

    /* renamed from: a */
    private void m37595a(ShipAmplifierAdapter aVar) {
        bFf().mo5608dq().mo3197f(aZg, aVar);
    }

    /* renamed from: d */
    private void m37596d(C3892wO wOVar) {
        throw new C6039afL();
    }

    /* renamed from: e */
    private void m37597e(C3892wO wOVar) {
        throw new C6039afL();
    }

    /* renamed from: f */
    private void m37598f(C3892wO wOVar) {
        throw new C6039afL();
    }

    /* renamed from: g */
    private void m37599g(C3892wO wOVar) {
        throw new C6039afL();
    }

    /* renamed from: h */
    private void m37600h(C3892wO wOVar) {
        throw new C6039afL();
    }

    @C0064Am(aul = "e6409dc23f6fd4511954dd5331d55f2d", aum = 0)
    @C5566aOg
    @C2499fr
    /* renamed from: qU */
    private void m37601qU() {
        throw new aWi(new aCE(this, f8922Lm, new Object[0]));
    }

    /* renamed from: yE */
    private C3892wO m37602yE() {
        return ((ShipAmplifierType) getType()).mo7778yL();
    }

    /* renamed from: yF */
    private C3892wO m37603yF() {
        return ((ShipAmplifierType) getType()).mo7779yN();
    }

    /* renamed from: yG */
    private C3892wO m37604yG() {
        return ((ShipAmplifierType) getType()).mo7780yP();
    }

    /* renamed from: yH */
    private C3892wO m37605yH() {
        return ((ShipAmplifierType) getType()).mo7781yR();
    }

    /* renamed from: yI */
    private C3892wO m37606yI() {
        return ((ShipAmplifierType) getType()).mo7782yT();
    }

    @C5566aOg
    /* renamed from: Ks */
    public void mo1952Ks() {
        switch (bFf().mo6893i(atW)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, atW, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, atW, new Object[0]));
                break;
        }
        m37585Kr();
    }

    @C5566aOg
    /* renamed from: Ku */
    public void mo1953Ku() {
        switch (bFf().mo6893i(atX)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, atX, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, atX, new Object[0]));
                break;
        }
        m37586Kt();
    }

    /* renamed from: S */
    public void mo10S() {
        super.mo10S();
    }

    /* renamed from: T */
    public C6494any mo11T() {
        return ___iScriptClass;
    }

    /* renamed from: U */
    public C2491fm[] mo12U() {
        return _m_methods;
    }

    /* renamed from: W */
    public Object mo13W() {
        return new C3951xH(this);
    }

    /* access modifiers changed from: protected */
    /* renamed from: XR */
    public C3892wO mo21364XR() {
        switch (bFf().mo6893i(aYg)) {
            case 0:
                return null;
            case 2:
                return (C3892wO) bFf().mo5606d(new aCE(this, aYg, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, aYg, new Object[0]));
                break;
        }
        return m37588XQ();
    }

    /* access modifiers changed from: protected */
    /* renamed from: XT */
    public C3892wO mo21365XT() {
        switch (bFf().mo6893i(aYh)) {
            case 0:
                return null;
            case 2:
                return (C3892wO) bFf().mo5606d(new aCE(this, aYh, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, aYh, new Object[0]));
                break;
        }
        return m37589XS();
    }

    /* access modifiers changed from: protected */
    /* renamed from: XV */
    public C3892wO mo21366XV() {
        switch (bFf().mo6893i(aYi)) {
            case 0:
                return null;
            case 2:
                return (C3892wO) bFf().mo5606d(new aCE(this, aYi, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, aYi, new Object[0]));
                break;
        }
        return m37590XU();
    }

    /* access modifiers changed from: protected */
    /* renamed from: XX */
    public C3892wO mo21367XX() {
        switch (bFf().mo6893i(aYj)) {
            case 0:
                return null;
            case 2:
                return (C3892wO) bFf().mo5606d(new aCE(this, aYj, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, aYj, new Object[0]));
                break;
        }
        return m37591XW();
    }

    /* renamed from: Yd */
    public ShipAdapter mo21368Yd() {
        switch (bFf().mo6893i(aZh)) {
            case 0:
                return null;
            case 2:
                return (ShipAdapter) bFf().mo5606d(new aCE(this, aZh, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, aZh, new Object[0]));
                break;
        }
        return m37593Yc();
    }

    /* access modifiers changed from: protected */
    /* renamed from: Yf */
    public C3892wO mo21369Yf() {
        switch (bFf().mo6893i(aZi)) {
            case 0:
                return null;
            case 2:
                return (C3892wO) bFf().mo5606d(new aCE(this, aZi, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, aZi, new Object[0]));
                break;
        }
        return m37594Ye();
    }

    /* renamed from: a */
    public Object mo14a(C0495Gr gr) {
        gr.getArgs();
        switch (gr.mo2417hq() - Amplifier._m_methodCount) {
            case 0:
                return m37593Yc();
            case 1:
                m37585Kr();
                return null;
            case 2:
                m37586Kt();
                return null;
            case 3:
                return m37588XQ();
            case 4:
                return m37589XS();
            case 5:
                return m37590XU();
            case 6:
                return m37591XW();
            case 7:
                return m37594Ye();
            case 8:
                m37601qU();
                return null;
            default:
                return super.mo14a(gr);
        }
    }

    /* renamed from: a */
    public void mo15a(Collection collection, C0495Gr gr) {
        super.mo15a(collection, gr);
    }

    /* renamed from: c */
    public C5663aRz[] mo25c() {
        return _m_fields;
    }

    @C5566aOg
    @C2499fr
    /* renamed from: qV */
    public void mo1959qV() {
        switch (bFf().mo6893i(f8922Lm)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f8922Lm, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f8922Lm, new Object[0]));
                break;
        }
        m37601qU();
    }

    /* renamed from: e */
    public void mo1956e(AmplifierType iDVar) {
        super.mo1956e(iDVar);
    }

    @C0064Am(aul = "cda6ac6bd45dabb34a960fb0b7246e54", aum = 0)
    /* renamed from: Yc */
    private ShipAdapter m37593Yc() {
        return m37592Yb();
    }

    @C0064Am(aul = "32a42d5242fce2f7ff2926ce95738eb8", aum = 0)
    /* renamed from: XQ */
    private C3892wO m37588XQ() {
        return m37602yE();
    }

    @C0064Am(aul = "b7562fe1ad1dd763660efbf50741bb80", aum = 0)
    /* renamed from: XS */
    private C3892wO m37589XS() {
        return m37603yF();
    }

    @C0064Am(aul = "62c9f3418761b704fa7f8b85ea1b9094", aum = 0)
    /* renamed from: XU */
    private C3892wO m37590XU() {
        return m37604yG();
    }

    @C0064Am(aul = "0d019bfd58217ddcd808a2ef60377cc0", aum = 0)
    /* renamed from: XW */
    private C3892wO m37591XW() {
        return m37605yH();
    }

    @C0064Am(aul = "9a3f20bd283301d533c6a08ed2f689f0", aum = 0)
    /* renamed from: Ye */
    private C3892wO m37594Ye() {
        return m37606yI();
    }

    @C6485anp
    @C5511aMd
    /* renamed from: a.qW$a */
    public class ShipAmplifierAdapter extends ShipAdapter implements C1616Xf {
        public static final int _m_fieldCount = 0;
        public static final C5663aRz[] _m_fields = null;
        public static final int _m_methodCount = 0;
        public static final C2491fm[] _m_methods = null;
        /* renamed from: aT */
        public static final C5663aRz f8933aT = null;
        public static final C2491fm aTe = null;
        public static final C2491fm aTf = null;
        public static final C2491fm aTg = null;
        public static final C2491fm aTh = null;
        public static final C2491fm bpZ = null;
        public static final long serialVersionUID = 0;
        public static C6494any ___iScriptClass;
        @C0064Am(aul = "6fc18ad4e45dd49f8e1cbfc6444be356", aum = 0)
        static /* synthetic */ ShipAmplifier eoy;

        static {
            m37624V();
        }

        public ShipAmplifierAdapter() {
            super((C5540aNg) null);
            super.mo10S();
        }

        public ShipAmplifierAdapter(C5540aNg ang) {
            super(ang);
        }

        public ShipAmplifierAdapter(ShipAmplifier qWVar) {
            super((C5540aNg) null);
            super._m_script_init(qWVar);
        }

        /* renamed from: V */
        static void m37624V() {
            serialVersionUID = (long) 1;
            _m_fieldCount = ShipAdapter._m_fieldCount + 1;
            _m_methodCount = ShipAdapter._m_methodCount + 5;
            int i = ShipAdapter._m_fieldCount;
            C5663aRz[] arzArr = new C5663aRz[(i + 1)];
            C5663aRz b = C5640aRc.m17844b(ShipAmplifierAdapter.class, "6fc18ad4e45dd49f8e1cbfc6444be356", i);
            f8933aT = b;
            arzArr[i] = b;
            int i2 = i + 1;
            _m_fields = arzArr;
            C1634Xv.m11725a((Object[]) ShipAdapter._m_fields, (Object[]) _m_fields);
            int i3 = ShipAdapter._m_methodCount;
            C2491fm[] fmVarArr = new C2491fm[(i3 + 5)];
            C2491fm a = C4105zY.m41624a(ShipAmplifierAdapter.class, "14f7cdc1f0e25a78c812e1892751d4c1", i3);
            aTe = a;
            fmVarArr[i3] = a;
            int i4 = i3 + 1;
            C2491fm a2 = C4105zY.m41624a(ShipAmplifierAdapter.class, "789384e2de30c27b094cb8ac0bb0ffc9", i4);
            aTf = a2;
            fmVarArr[i4] = a2;
            int i5 = i4 + 1;
            C2491fm a3 = C4105zY.m41624a(ShipAmplifierAdapter.class, "3010de886560dedc4c2cb51028901544", i5);
            aTg = a3;
            fmVarArr[i5] = a3;
            int i6 = i5 + 1;
            C2491fm a4 = C4105zY.m41624a(ShipAmplifierAdapter.class, "7feae69aa3c092e5d9913d715da39823", i6);
            aTh = a4;
            fmVarArr[i6] = a4;
            int i7 = i6 + 1;
            C2491fm a5 = C4105zY.m41624a(ShipAmplifierAdapter.class, "ba698e5db7a489178b157c4122a6de54", i7);
            bpZ = a5;
            fmVarArr[i7] = a5;
            int i8 = i7 + 1;
            _m_methods = fmVarArr;
            C1634Xv.m11725a((Object[]) ShipAdapter._m_methods, (Object[]) _m_methods);
            ___iScriptClass = aUO.m18566a(ShipAmplifierAdapter.class, C6981axu.class, _m_fields, _m_methods);
        }

        /* renamed from: a */
        private void m37629a(ShipAmplifier qWVar) {
            bFf().mo5608dq().mo3197f(f8933aT, qWVar);
        }

        private ShipAmplifier bxJ() {
            return (ShipAmplifier) bFf().mo5608dq().mo3214p(f8933aT);
        }

        /* renamed from: S */
        public void mo10S() {
            super.mo10S();
        }

        /* renamed from: T */
        public C6494any mo11T() {
            return ___iScriptClass;
        }

        /* renamed from: U */
        public C2491fm[] mo12U() {
            return _m_methods;
        }

        /* renamed from: VF */
        public float mo5665VF() {
            switch (bFf().mo6893i(aTe)) {
                case 0:
                    return 0.0f;
                case 2:
                    return ((Float) bFf().mo5606d(new aCE(this, aTe, new Object[0]))).floatValue();
                case 3:
                    bFf().mo5606d(new aCE(this, aTe, new Object[0]));
                    break;
            }
            return m37625VE();
        }

        /* renamed from: VH */
        public float mo5666VH() {
            switch (bFf().mo6893i(aTf)) {
                case 0:
                    return 0.0f;
                case 2:
                    return ((Float) bFf().mo5606d(new aCE(this, aTf, new Object[0]))).floatValue();
                case 3:
                    bFf().mo5606d(new aCE(this, aTf, new Object[0]));
                    break;
            }
            return m37626VG();
        }

        /* renamed from: W */
        public Object mo13W() {
            return new C6981axu(this);
        }

        /* renamed from: a */
        public Object mo14a(C0495Gr gr) {
            gr.getArgs();
            switch (gr.mo2417hq() - ShipAdapter._m_methodCount) {
                case 0:
                    return new Float(m37625VE());
                case 1:
                    return new Float(m37626VG());
                case 2:
                    return new Float(m37627VI());
                case 3:
                    return new Float(m37628VJ());
                case 4:
                    return new Float(ago());
                default:
                    return super.mo14a(gr);
            }
        }

        /* renamed from: a */
        public void mo15a(Collection collection, C0495Gr gr) {
            super.mo15a(collection, gr);
        }

        public float agp() {
            switch (bFf().mo6893i(bpZ)) {
                case 0:
                    return 0.0f;
                case 2:
                    return ((Float) bFf().mo5606d(new aCE(this, bpZ, new Object[0]))).floatValue();
                case 3:
                    bFf().mo5606d(new aCE(this, bpZ, new Object[0]));
                    break;
            }
            return ago();
        }

        /* renamed from: c */
        public C5663aRz[] mo25c() {
            return _m_fields;
        }

        /* renamed from: ra */
        public float mo5668ra() {
            switch (bFf().mo6893i(aTh)) {
                case 0:
                    return 0.0f;
                case 2:
                    return ((Float) bFf().mo5606d(new aCE(this, aTh, new Object[0]))).floatValue();
                case 3:
                    bFf().mo5606d(new aCE(this, aTh, new Object[0]));
                    break;
            }
            return m37628VJ();
        }

        /* renamed from: rb */
        public float mo5669rb() {
            switch (bFf().mo6893i(aTg)) {
                case 0:
                    return 0.0f;
                case 2:
                    return ((Float) bFf().mo5606d(new aCE(this, aTg, new Object[0]))).floatValue();
                case 3:
                    bFf().mo5606d(new aCE(this, aTg, new Object[0]));
                    break;
            }
            return m37627VI();
        }

        /* renamed from: b */
        public void mo21370b(ShipAmplifier qWVar) {
            m37629a(qWVar);
            super.mo10S();
        }

        @C0064Am(aul = "14f7cdc1f0e25a78c812e1892751d4c1", aum = 0)
        /* renamed from: VE */
        private float m37625VE() {
            return bxJ().mo21367XX().mo22753v(((ShipAdapter) mo16070IE()).mo5665VF(), super.mo5665VF());
        }

        @C0064Am(aul = "789384e2de30c27b094cb8ac0bb0ffc9", aum = 0)
        /* renamed from: VG */
        private float m37626VG() {
            return bxJ().mo21366XV().mo22753v(((ShipAdapter) mo16070IE()).mo5666VH(), super.mo5666VH());
        }

        @C0064Am(aul = "3010de886560dedc4c2cb51028901544", aum = 0)
        /* renamed from: VI */
        private float m37627VI() {
            return bxJ().mo21365XT().mo22753v(((ShipAdapter) mo16070IE()).mo5669rb(), super.mo5669rb());
        }

        @C0064Am(aul = "7feae69aa3c092e5d9913d715da39823", aum = 0)
        /* renamed from: VJ */
        private float m37628VJ() {
            return bxJ().mo21364XR().mo22753v(((ShipAdapter) mo16070IE()).mo5668ra(), super.mo5668ra());
        }

        @C0064Am(aul = "ba698e5db7a489178b157c4122a6de54", aum = 0)
        private float ago() {
            return bxJ().mo21369Yf().mo22753v(((ShipAdapter) mo16070IE()).agp(), super.agp());
        }
    }
}
