package game.script.item.buff.amplifier;

import game.network.message.externalizable.C0939Nn;
import game.network.message.externalizable.C6623aqX;
import game.network.message.externalizable.aCE;
import game.script.item.Amplifier;
import game.script.item.AmplifierType;
import game.script.nls.NLSAmplifier;
import game.script.nls.NLSManager;
import game.script.player.Player;
import game.script.ship.CargoHoldAdapter;
import logic.aaa.C1506WA;
import logic.baa.C1616Xf;
import logic.baa.C1634Xv;
import logic.baa.C5511aMd;
import logic.baa.C6485anp;
import logic.data.mbean.C3005ms;
import logic.data.mbean.C6953aws;
import logic.res.code.C5640aRc;
import logic.res.code.C5663aRz;
import logic.res.code.C6494any;
import logic.res.html.C2491fm;
import p001a.*;

import java.util.Collection;

@C5829abJ("2.0.0")
@C6485anp
@C2712iu(mo19786Bt = AmplifierType.class, version = "2.0.1")
@C5511aMd
/* renamed from: a.MB */
/* compiled from: a */
public class CargoHoldAmplifier extends Amplifier implements C1616Xf {
    /* renamed from: Lm */
    public static final C2491fm f1078Lm = null;
    /* renamed from: Pv */
    public static final C5663aRz f1079Pv = null;
    public static final C2491fm _f_dispose_0020_0028_0029V = null;
    public static final int _m_fieldCount = 0;
    public static final C5663aRz[] _m_fields = null;
    public static final int _m_methodCount = 0;
    public static final C2491fm[] _m_methods = null;
    public static final C2491fm atW = null;
    public static final C2491fm atX = null;
    public static final C2491fm bLn = null;
    public static final C2491fm cRr = null;
    public static final C2491fm cRs = null;
    public static final C2491fm ccS = null;
    public static final C5663aRz gLx = null;
    public static final C2491fm gLy = null;
    public static final C2491fm gLz = null;
    private static final long serialVersionUID = -3910323838580860891L;
    public static C6494any ___iScriptClass = null;
    @C0064Am(aul = "92fee7c77d6042c8526b539dc66f9187", aum = 1)

    /* renamed from: DL */
    private static C3892wO f1077DL = null;
    @C0064Am(aul = "a034632fc008f9254d6f88e97bc1d573", aum = 0)
    private static CargoHoldAmplifierAdapter aCu = null;

    static {
        m6897V();
    }

    public CargoHoldAmplifier() {
        super((C5540aNg) null);
        super.mo10S();
    }

    public CargoHoldAmplifier(C5540aNg ang) {
        super(ang);
    }

    public CargoHoldAmplifier(AmplifierType iDVar) {
        super((C5540aNg) null);
        super.mo1956e(iDVar);
    }

    /* renamed from: V */
    static void m6897V() {
        _m_fieldCount = Amplifier._m_fieldCount + 2;
        _m_methodCount = Amplifier._m_methodCount + 10;
        int i = Amplifier._m_fieldCount;
        C5663aRz[] arzArr = new C5663aRz[(i + 2)];
        C5663aRz b = C5640aRc.m17844b(CargoHoldAmplifier.class, "a034632fc008f9254d6f88e97bc1d573", i);
        gLx = b;
        arzArr[i] = b;
        int i2 = i + 1;
        C5663aRz b2 = C5640aRc.m17844b(CargoHoldAmplifier.class, "92fee7c77d6042c8526b539dc66f9187", i2);
        f1079Pv = b2;
        arzArr[i2] = b2;
        int i3 = i2 + 1;
        _m_fields = arzArr;
        C1634Xv.m11725a((Object[]) Amplifier._m_fields, (Object[]) _m_fields);
        int i4 = Amplifier._m_methodCount;
        C2491fm[] fmVarArr = new C2491fm[(i4 + 10)];
        C2491fm a = C4105zY.m41624a(CargoHoldAmplifier.class, "ae1c267594691697c1ae1b4adf41f458", i4);
        atW = a;
        fmVarArr[i4] = a;
        int i5 = i4 + 1;
        C2491fm a2 = C4105zY.m41624a(CargoHoldAmplifier.class, "9c4344697f0708d348a7d4a6ff25f8fc", i5);
        atX = a2;
        fmVarArr[i5] = a2;
        int i6 = i5 + 1;
        C2491fm a3 = C4105zY.m41624a(CargoHoldAmplifier.class, "1be7ea873d641a5d826406575b19f390", i6);
        bLn = a3;
        fmVarArr[i6] = a3;
        int i7 = i6 + 1;
        C2491fm a4 = C4105zY.m41624a(CargoHoldAmplifier.class, "bfe66e0633e7833537084b96585fd6f5", i7);
        cRr = a4;
        fmVarArr[i7] = a4;
        int i8 = i7 + 1;
        C2491fm a5 = C4105zY.m41624a(CargoHoldAmplifier.class, "cf08e51619aeccc26cb9ae74f2144bce", i8);
        cRs = a5;
        fmVarArr[i8] = a5;
        int i9 = i8 + 1;
        C2491fm a6 = C4105zY.m41624a(CargoHoldAmplifier.class, "87024b2472f69038130c855c31443c3f", i9);
        ccS = a6;
        fmVarArr[i9] = a6;
        int i10 = i9 + 1;
        C2491fm a7 = C4105zY.m41624a(CargoHoldAmplifier.class, "d9ec3dba684eb6bb38f7ed8c6e55d973", i10);
        _f_dispose_0020_0028_0029V = a7;
        fmVarArr[i10] = a7;
        int i11 = i10 + 1;
        C2491fm a8 = C4105zY.m41624a(CargoHoldAmplifier.class, "29775ccbbfc7baec46e64600daabddc9", i11);
        f1078Lm = a8;
        fmVarArr[i11] = a8;
        int i12 = i11 + 1;
        C2491fm a9 = C4105zY.m41624a(CargoHoldAmplifier.class, "0b79864ab85f74659d274639e6efe1b0", i12);
        gLy = a9;
        fmVarArr[i12] = a9;
        int i13 = i12 + 1;
        C2491fm a10 = C4105zY.m41624a(CargoHoldAmplifier.class, "e82569b3b51ae407fe5d5cb1a8473145", i13);
        gLz = a10;
        fmVarArr[i13] = a10;
        int i14 = i13 + 1;
        _m_methods = fmVarArr;
        C1634Xv.m11725a((Object[]) Amplifier._m_methods, (Object[]) _m_methods);
        ___iScriptClass = aUO.m18566a(CargoHoldAmplifier.class, C3005ms.class, _m_fields, _m_methods);
    }

    @C0064Am(aul = "ae1c267594691697c1ae1b4adf41f458", aum = 0)
    @C5566aOg
    /* renamed from: Kr */
    private void m6895Kr() {
        throw new aWi(new aCE(this, atW, new Object[0]));
    }

    @C0064Am(aul = "9c4344697f0708d348a7d4a6ff25f8fc", aum = 0)
    @C5566aOg
    /* renamed from: Kt */
    private void m6896Kt() {
        throw new aWi(new aCE(this, atX, new Object[0]));
    }

    /* renamed from: a */
    private void m6898a(CargoHoldAmplifierAdapter aVar) {
        bFf().mo5608dq().mo3197f(gLx, aVar);
    }

    /* renamed from: a */
    private void m6899a(C3892wO wOVar) {
        throw new C6039afL();
    }

    /* access modifiers changed from: private */
    public CargoHoldAmplifierAdapter cAK() {
        return (CargoHoldAmplifierAdapter) bFf().mo5608dq().mo3214p(gLx);
    }

    @C0064Am(aul = "29775ccbbfc7baec46e64600daabddc9", aum = 0)
    @C5566aOg
    @C2499fr
    /* renamed from: qU */
    private void m6903qU() {
        throw new aWi(new aCE(this, f1078Lm, new Object[0]));
    }

    /* renamed from: vi */
    private C3892wO m6904vi() {
        return ((CargoHoldAmplifierType) getType()).mo19057vk();
    }

    @C5566aOg
    /* renamed from: Ks */
    public void mo1952Ks() {
        switch (bFf().mo6893i(atW)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, atW, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, atW, new Object[0]));
                break;
        }
        m6895Kr();
    }

    @C5566aOg
    /* renamed from: Ku */
    public void mo1953Ku() {
        switch (bFf().mo6893i(atX)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, atX, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, atX, new Object[0]));
                break;
        }
        m6896Kt();
    }

    /* renamed from: S */
    public void mo10S() {
        super.mo10S();
    }

    /* renamed from: T */
    public C6494any mo11T() {
        return ___iScriptClass;
    }

    /* renamed from: U */
    public C2491fm[] mo12U() {
        return _m_methods;
    }

    /* renamed from: W */
    public Object mo13W() {
        return new C3005ms(this);
    }

    /* renamed from: a */
    public Object mo14a(C0495Gr gr) {
        Object[] args = gr.getArgs();
        switch (gr.mo2417hq() - Amplifier._m_methodCount) {
            case 0:
                m6895Kr();
                return null;
            case 1:
                m6896Kt();
                return null;
            case 2:
                return apN();
            case 3:
                return new Boolean(aNM());
            case 4:
                return new Boolean(aNO());
            case 5:
                return new Boolean(asX());
            case 6:
                m6902fg();
                return null;
            case 7:
                m6903qU();
                return null;
            case 8:
                return cAL();
            case 9:
                m6900b((CargoHoldAmplifierAdapter) args[0]);
                return null;
            default:
                return super.mo14a(gr);
        }
    }

    /* renamed from: a */
    public void mo15a(Collection collection, C0495Gr gr) {
        super.mo15a(collection, gr);
    }

    public boolean aNN() {
        switch (bFf().mo6893i(cRr)) {
            case 0:
                return false;
            case 2:
                return ((Boolean) bFf().mo5606d(new aCE(this, cRr, new Object[0]))).booleanValue();
            case 3:
                bFf().mo5606d(new aCE(this, cRr, new Object[0]));
                break;
        }
        return aNM();
    }

    public boolean aNP() {
        switch (bFf().mo6893i(cRs)) {
            case 0:
                return false;
            case 2:
                return ((Boolean) bFf().mo5606d(new aCE(this, cRs, new Object[0]))).booleanValue();
            case 3:
                bFf().mo5606d(new aCE(this, cRs, new Object[0]));
                break;
        }
        return aNO();
    }

    /* access modifiers changed from: protected */
    public C3892wO apO() {
        switch (bFf().mo6893i(bLn)) {
            case 0:
                return null;
            case 2:
                return (C3892wO) bFf().mo5606d(new aCE(this, bLn, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, bLn, new Object[0]));
                break;
        }
        return apN();
    }

    public boolean asY() {
        switch (bFf().mo6893i(ccS)) {
            case 0:
                return false;
            case 2:
                return ((Boolean) bFf().mo5606d(new aCE(this, ccS, new Object[0]))).booleanValue();
            case 3:
                bFf().mo5606d(new aCE(this, ccS, new Object[0]));
                break;
        }
        return asX();
    }

    /* access modifiers changed from: protected */
    /* renamed from: c */
    public void mo3869c(CargoHoldAmplifierAdapter aVar) {
        switch (bFf().mo6893i(gLz)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, gLz, new Object[]{aVar}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, gLz, new Object[]{aVar}));
                break;
        }
        m6900b(aVar);
    }

    /* renamed from: c */
    public C5663aRz[] mo25c() {
        return _m_fields;
    }

    /* access modifiers changed from: protected */
    public CargoHoldAmplifierAdapter cAM() {
        switch (bFf().mo6893i(gLy)) {
            case 0:
                return null;
            case 2:
                return (CargoHoldAmplifierAdapter) bFf().mo5606d(new aCE(this, gLy, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, gLy, new Object[0]));
                break;
        }
        return cAL();
    }

    public void dispose() {
        switch (bFf().mo6893i(_f_dispose_0020_0028_0029V)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, _f_dispose_0020_0028_0029V, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, _f_dispose_0020_0028_0029V, new Object[0]));
                break;
        }
        m6902fg();
    }

    @C5566aOg
    @C2499fr
    /* renamed from: qV */
    public void mo1959qV() {
        switch (bFf().mo6893i(f1078Lm)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f1078Lm, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f1078Lm, new Object[0]));
                break;
        }
        m6903qU();
    }

    /* renamed from: e */
    public void mo1956e(AmplifierType iDVar) {
        super.mo1956e(iDVar);
    }

    @C0064Am(aul = "1be7ea873d641a5d826406575b19f390", aum = 0)
    private C3892wO apN() {
        return m6904vi();
    }

    @C0064Am(aul = "bfe66e0633e7833537084b96585fd6f5", aum = 0)
    private boolean aNM() {
        return asY();
    }

    @C0064Am(aul = "cf08e51619aeccc26cb9ae74f2144bce", aum = 0)
    private boolean aNO() {
        if (!super.aNP()) {
            return false;
        }
        return asY();
    }

    @C0064Am(aul = "87024b2472f69038130c855c31443c3f", aum = 0)
    private boolean asX() {
        if (mo7855al() == null) {
            return true;
        }
        if (((CargoHoldAdapter) cAK().mo16071IG()).mo1289wE() >= mo7855al().afI().aRB()) {
            return true;
        }
        if (!(mo7855al() == null || mo7855al().agj() == null || !(mo7855al().agj() instanceof Player))) {
            ((Player) mo7855al().agj()).mo14419f((C1506WA) new C6623aqX(C0939Nn.C0940a.ERROR, ((NLSAmplifier) ala().aIY().mo6310c(NLSManager.C1472a.AMPLIFIER)).aDh(), new Object[0]));
        }
        return false;
    }

    @C0064Am(aul = "d9ec3dba684eb6bb38f7ed8c6e55d973", aum = 0)
    /* renamed from: fg */
    private void m6902fg() {
        if (asY()) {
            super.dispose();
        }
    }

    @C0064Am(aul = "0b79864ab85f74659d274639e6efe1b0", aum = 0)
    private CargoHoldAmplifierAdapter cAL() {
        return cAK();
    }

    @C0064Am(aul = "e82569b3b51ae407fe5d5cb1a8473145", aum = 0)
    /* renamed from: b */
    private void m6900b(CargoHoldAmplifierAdapter aVar) {
        m6898a(aVar);
    }

    @C6485anp
    @C5511aMd
    /* renamed from: a.MB$a */
    public class CargoHoldAmplifierAdapter extends CargoHoldAdapter implements C1616Xf {

        /* renamed from: Rz */
        public static final C2491fm f1080Rz = null;
        public static final int _m_fieldCount = 0;
        public static final C5663aRz[] _m_fields = null;
        public static final int _m_methodCount = 0;
        public static final C2491fm[] _m_methods = null;
        /* renamed from: aT */
        public static final C5663aRz f1081aT = null;
        public static final long serialVersionUID = 0;
        public static C6494any ___iScriptClass;
        @C0064Am(aul = "7db130ff5275df2ef98edcf2a8b7ca1e", aum = 0)
        static /* synthetic */ CargoHoldAmplifier dBW;

        static {
            m6917V();
        }

        public CargoHoldAmplifierAdapter() {
            super((C5540aNg) null);
            super.mo10S();
        }

        public CargoHoldAmplifierAdapter(CargoHoldAmplifier mb) {
            super((C5540aNg) null);
            super._m_script_init(mb);
        }

        public CargoHoldAmplifierAdapter(C5540aNg ang) {
            super(ang);
        }

        /* renamed from: V */
        static void m6917V() {
            serialVersionUID = (long) 1;
            _m_fieldCount = CargoHoldAdapter._m_fieldCount + 1;
            _m_methodCount = CargoHoldAdapter._m_methodCount + 1;
            int i = CargoHoldAdapter._m_fieldCount;
            C5663aRz[] arzArr = new C5663aRz[(i + 1)];
            C5663aRz b = C5640aRc.m17844b(CargoHoldAmplifierAdapter.class, "7db130ff5275df2ef98edcf2a8b7ca1e", i);
            f1081aT = b;
            arzArr[i] = b;
            int i2 = i + 1;
            _m_fields = arzArr;
            C1634Xv.m11725a((Object[]) CargoHoldAdapter._m_fields, (Object[]) _m_fields);
            int i3 = CargoHoldAdapter._m_methodCount;
            C2491fm[] fmVarArr = new C2491fm[(i3 + 1)];
            C2491fm a = C4105zY.m41624a(CargoHoldAmplifierAdapter.class, "4fc31a55ada93a4bac875beb83deff5f", i3);
            f1080Rz = a;
            fmVarArr[i3] = a;
            int i4 = i3 + 1;
            _m_methods = fmVarArr;
            C1634Xv.m11725a((Object[]) CargoHoldAdapter._m_methods, (Object[]) _m_methods);
            ___iScriptClass = aUO.m18566a(CargoHoldAmplifierAdapter.class, C6953aws.class, _m_fields, _m_methods);
        }

        /* renamed from: a */
        private void m6918a(CargoHoldAmplifier mb) {
            bFf().mo5608dq().mo3197f(f1081aT, mb);
        }

        private CargoHoldAmplifier bhm() {
            return (CargoHoldAmplifier) bFf().mo5608dq().mo3214p(f1081aT);
        }

        /* renamed from: S */
        public void mo10S() {
            super.mo10S();
        }

        /* renamed from: T */
        public C6494any mo11T() {
            return ___iScriptClass;
        }

        /* renamed from: U */
        public C2491fm[] mo12U() {
            return _m_methods;
        }

        /* renamed from: W */
        public Object mo13W() {
            return new C6953aws(this);
        }

        /* renamed from: a */
        public Object mo14a(C0495Gr gr) {
            gr.getArgs();
            switch (gr.mo2417hq() - CargoHoldAdapter._m_methodCount) {
                case 0:
                    return new Float(m6919wD());
                default:
                    return super.mo14a(gr);
            }
        }

        /* renamed from: a */
        public void mo15a(Collection collection, C0495Gr gr) {
            super.mo15a(collection, gr);
        }

        /* renamed from: c */
        public C5663aRz[] mo25c() {
            return _m_fields;
        }

        /* renamed from: wE */
        public float mo1289wE() {
            switch (bFf().mo6893i(f1080Rz)) {
                case 0:
                    return 0.0f;
                case 2:
                    return ((Float) bFf().mo5606d(new aCE(this, f1080Rz, new Object[0]))).floatValue();
                case 3:
                    bFf().mo5606d(new aCE(this, f1080Rz, new Object[0]));
                    break;
            }
            return m6919wD();
        }

        /* renamed from: b */
        public void mo3871b(CargoHoldAmplifier mb) {
            m6918a(mb);
            super.mo10S();
        }

        @C0064Am(aul = "4fc31a55ada93a4bac875beb83deff5f", aum = 0)
        /* renamed from: wD */
        private float m6919wD() {
            return bhm().apO().mo22753v(((CargoHoldAdapter) bhm().cAK().mo16070IE()).mo1289wE(), super.mo1289wE());
        }
    }
}
