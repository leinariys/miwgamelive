package game.script.item.buff.module;

import game.network.message.externalizable.aCE;
import game.script.ship.ShipAdapter;
import logic.baa.*;
import logic.data.mbean.C2172cR;
import logic.data.mbean.C6081agB;
import logic.res.code.C5640aRc;
import logic.res.code.C5663aRz;
import logic.res.code.C6494any;
import logic.res.html.C2491fm;
import p001a.*;

import java.util.Collection;

@C5829abJ("1.1.2")
@C6485anp
@C2712iu(version = "1.1.2")
@C5511aMd
/* renamed from: a.sZ */
/* compiled from: a */
public class ShipBuff extends AttributeBuff implements C1616Xf, aOW {

    /* renamed from: Do */
    public static final C2491fm f9107Do = null;
    /* renamed from: VB */
    public static final C5663aRz f9109VB = null;
    /* renamed from: VD */
    public static final C5663aRz f9111VD = null;
    /* renamed from: Vt */
    public static final C5663aRz f9113Vt = null;
    /* renamed from: Vv */
    public static final C5663aRz f9115Vv = null;
    /* renamed from: Vx */
    public static final C5663aRz f9117Vx = null;
    /* renamed from: Vz */
    public static final C5663aRz f9119Vz = null;
    public static final int _m_fieldCount = 0;
    public static final C5663aRz[] _m_fields = null;
    public static final int _m_methodCount = 0;
    public static final C2491fm[] _m_methods = null;
    public static final C2491fm aPH = null;
    public static final C2491fm aPI = null;
    public static final C2491fm aYg = null;
    public static final C2491fm aYh = null;
    public static final C2491fm aYi = null;
    public static final C2491fm aYj = null;
    public static final C5663aRz aZg = null;
    public static final C2491fm aZi = null;
    public static final long serialVersionUID = 0;
    public static C6494any ___iScriptClass;
    @C0064Am(aul = "9374fcf556aa8804decd503f6738f6c6", aum = 4)

    /* renamed from: VA */
    private static C3892wO f9108VA;
    @C0064Am(aul = "24b865b4267967696214726616d40dbd", aum = 5)

    /* renamed from: VC */
    private static boolean f9110VC;
    @C0064Am(aul = "5bea99e79614072f5370f229354aa1c4", aum = 0)

    /* renamed from: Vs */
    private static C3892wO f9112Vs;
    @C0064Am(aul = "b0a194317c28a781385e5de8a4c5174c", aum = 1)

    /* renamed from: Vu */
    private static C3892wO f9114Vu;
    @C0064Am(aul = "59aa43df301fc26fb17165842be0352a", aum = 2)

    /* renamed from: Vw */
    private static C3892wO f9116Vw;
    @C0064Am(aul = "77c9bd3dea0143dfb308951123a25c17", aum = 3)

    /* renamed from: Vy */
    private static C3892wO f9118Vy;
    @C0064Am(aul = "50582fbd9328407036f3194c6a903aa6", aum = 6)
    private static ShipModuleAdapter bjI;

    static {
        m38854V();
    }

    public ShipBuff() {
        super((C5540aNg) null);
        super.mo10S();
    }

    public ShipBuff(C5540aNg ang) {
        super(ang);
    }

    public ShipBuff(ShipBuffType hOVar) {
        super((C5540aNg) null);
        super._m_script_init(hOVar);
    }

    /* renamed from: V */
    static void m38854V() {
        serialVersionUID = (long) 1;
        _m_fieldCount = AttributeBuff._m_fieldCount + 7;
        _m_methodCount = AttributeBuff._m_methodCount + 8;
        int i = AttributeBuff._m_fieldCount;
        C5663aRz[] arzArr = new C5663aRz[(i + 7)];
        C5663aRz b = C5640aRc.m17844b(ShipBuff.class, "5bea99e79614072f5370f229354aa1c4", i);
        f9113Vt = b;
        arzArr[i] = b;
        int i2 = i + 1;
        C5663aRz b2 = C5640aRc.m17844b(ShipBuff.class, "b0a194317c28a781385e5de8a4c5174c", i2);
        f9115Vv = b2;
        arzArr[i2] = b2;
        int i3 = i2 + 1;
        C5663aRz b3 = C5640aRc.m17844b(ShipBuff.class, "59aa43df301fc26fb17165842be0352a", i3);
        f9117Vx = b3;
        arzArr[i3] = b3;
        int i4 = i3 + 1;
        C5663aRz b4 = C5640aRc.m17844b(ShipBuff.class, "77c9bd3dea0143dfb308951123a25c17", i4);
        f9119Vz = b4;
        arzArr[i4] = b4;
        int i5 = i4 + 1;
        C5663aRz b5 = C5640aRc.m17844b(ShipBuff.class, "9374fcf556aa8804decd503f6738f6c6", i5);
        f9109VB = b5;
        arzArr[i5] = b5;
        int i6 = i5 + 1;
        C5663aRz b6 = C5640aRc.m17844b(ShipBuff.class, "24b865b4267967696214726616d40dbd", i6);
        f9111VD = b6;
        arzArr[i6] = b6;
        int i7 = i6 + 1;
        C5663aRz b7 = C5640aRc.m17844b(ShipBuff.class, "50582fbd9328407036f3194c6a903aa6", i7);
        aZg = b7;
        arzArr[i7] = b7;
        int i8 = i7 + 1;
        _m_fields = arzArr;
        C1634Xv.m11725a((Object[]) AttributeBuff._m_fields, (Object[]) _m_fields);
        int i9 = AttributeBuff._m_methodCount;
        C2491fm[] fmVarArr = new C2491fm[(i9 + 8)];
        C2491fm a = C4105zY.m41624a(ShipBuff.class, "6db4f72dce2000d31bd6bf17eedddcd6", i9);
        aPH = a;
        fmVarArr[i9] = a;
        int i10 = i9 + 1;
        C2491fm a2 = C4105zY.m41624a(ShipBuff.class, "225bd1f82de5cc3ec497defcaf28d2aa", i10);
        aPI = a2;
        fmVarArr[i10] = a2;
        int i11 = i10 + 1;
        C2491fm a3 = C4105zY.m41624a(ShipBuff.class, "7f215f0325e84c7ec2d439971e82b8af", i11);
        aYg = a3;
        fmVarArr[i11] = a3;
        int i12 = i11 + 1;
        C2491fm a4 = C4105zY.m41624a(ShipBuff.class, "538b53f1fdf3d022abf9c814727cf2a0", i12);
        aYh = a4;
        fmVarArr[i12] = a4;
        int i13 = i12 + 1;
        C2491fm a5 = C4105zY.m41624a(ShipBuff.class, "f61e7c7500fa1389c1569d8a0d61b101", i13);
        aYi = a5;
        fmVarArr[i13] = a5;
        int i14 = i13 + 1;
        C2491fm a6 = C4105zY.m41624a(ShipBuff.class, "54cc863f05b598209937a02914f11c18", i14);
        aYj = a6;
        fmVarArr[i14] = a6;
        int i15 = i14 + 1;
        C2491fm a7 = C4105zY.m41624a(ShipBuff.class, "6e17bf4921932d04324e4e7acbcbd0e3", i15);
        aZi = a7;
        fmVarArr[i15] = a7;
        int i16 = i15 + 1;
        C2491fm a8 = C4105zY.m41624a(ShipBuff.class, "dcd9f00ed89ded23815e26706832891f", i16);
        f9107Do = a8;
        fmVarArr[i16] = a8;
        int i17 = i16 + 1;
        _m_methods = fmVarArr;
        C1634Xv.m11725a((Object[]) AttributeBuff._m_methods, (Object[]) _m_methods);
        ___iScriptClass = aUO.m18566a(ShipBuff.class, C6081agB.class, _m_fields, _m_methods);
    }

    /* renamed from: S */
    private void m38851S(boolean z) {
        throw new C6039afL();
    }

    @C0064Am(aul = "6db4f72dce2000d31bd6bf17eedddcd6", aum = 0)
    @C5566aOg
    /* renamed from: Ue */
    private void m38852Ue() {
        throw new aWi(new aCE(this, aPH, new Object[0]));
    }

    @C0064Am(aul = "225bd1f82de5cc3ec497defcaf28d2aa", aum = 0)
    @C5566aOg
    /* renamed from: Ug */
    private void m38853Ug() {
        throw new aWi(new aCE(this, aPI, new Object[0]));
    }

    /* renamed from: a */
    private void m38861a(ShipModuleAdapter aVar) {
        bFf().mo5608dq().mo3197f(aZg, aVar);
    }

    private ShipModuleAdapter acw() {
        return (ShipModuleAdapter) bFf().mo5608dq().mo3214p(aZg);
    }

    /* renamed from: d */
    private void m38862d(C3892wO wOVar) {
        throw new C6039afL();
    }

    /* renamed from: e */
    private void m38863e(C3892wO wOVar) {
        throw new C6039afL();
    }

    /* renamed from: f */
    private void m38864f(C3892wO wOVar) {
        throw new C6039afL();
    }

    /* renamed from: g */
    private void m38865g(C3892wO wOVar) {
        throw new C6039afL();
    }

    /* renamed from: h */
    private void m38866h(C3892wO wOVar) {
        throw new C6039afL();
    }

    /* renamed from: yE */
    private C3892wO m38867yE() {
        return ((ShipBuffType) getType()).mo19246yL();
    }

    /* renamed from: yF */
    private C3892wO m38868yF() {
        return ((ShipBuffType) getType()).mo19247yN();
    }

    /* renamed from: yG */
    private C3892wO m38869yG() {
        return ((ShipBuffType) getType()).mo19248yP();
    }

    /* renamed from: yH */
    private C3892wO m38870yH() {
        return ((ShipBuffType) getType()).mo19249yR();
    }

    /* renamed from: yI */
    private C3892wO m38871yI() {
        return ((ShipBuffType) getType()).mo19250yT();
    }

    /* renamed from: yJ */
    private boolean m38872yJ() {
        return ((ShipBuffType) getType()).mo19251yV();
    }

    /* renamed from: S */
    public void mo10S() {
        super.mo10S();
    }

    /* renamed from: T */
    public C6494any mo11T() {
        return ___iScriptClass;
    }

    /* renamed from: U */
    public C2491fm[] mo12U() {
        return _m_methods;
    }

    @C5566aOg
    /* renamed from: Uf */
    public void mo3751Uf() {
        switch (bFf().mo6893i(aPH)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, aPH, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, aPH, new Object[0]));
                break;
        }
        m38852Ue();
    }

    @C5566aOg
    /* renamed from: Uh */
    public void mo3752Uh() {
        switch (bFf().mo6893i(aPI)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, aPI, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, aPI, new Object[0]));
                break;
        }
        m38853Ug();
    }

    /* renamed from: W */
    public Object mo13W() {
        return new C6081agB(this);
    }

    /* access modifiers changed from: protected */
    /* renamed from: XR */
    public C3892wO mo21919XR() {
        switch (bFf().mo6893i(aYg)) {
            case 0:
                return null;
            case 2:
                return (C3892wO) bFf().mo5606d(new aCE(this, aYg, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, aYg, new Object[0]));
                break;
        }
        return m38855XQ();
    }

    /* access modifiers changed from: protected */
    /* renamed from: XT */
    public C3892wO mo21920XT() {
        switch (bFf().mo6893i(aYh)) {
            case 0:
                return null;
            case 2:
                return (C3892wO) bFf().mo5606d(new aCE(this, aYh, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, aYh, new Object[0]));
                break;
        }
        return m38856XS();
    }

    /* access modifiers changed from: protected */
    /* renamed from: XV */
    public C3892wO mo21921XV() {
        switch (bFf().mo6893i(aYi)) {
            case 0:
                return null;
            case 2:
                return (C3892wO) bFf().mo5606d(new aCE(this, aYi, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, aYi, new Object[0]));
                break;
        }
        return m38857XU();
    }

    /* access modifiers changed from: protected */
    /* renamed from: XX */
    public C3892wO mo21922XX() {
        switch (bFf().mo6893i(aYj)) {
            case 0:
                return null;
            case 2:
                return (C3892wO) bFf().mo5606d(new aCE(this, aYj, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, aYj, new Object[0]));
                break;
        }
        return m38858XW();
    }

    /* access modifiers changed from: protected */
    /* renamed from: Yf */
    public C3892wO mo21923Yf() {
        switch (bFf().mo6893i(aZi)) {
            case 0:
                return null;
            case 2:
                return (C3892wO) bFf().mo5606d(new aCE(this, aZi, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, aZi, new Object[0]));
                break;
        }
        return m38859Ye();
    }

    /* renamed from: a */
    public Object mo14a(C0495Gr gr) {
        Object[] args = gr.getArgs();
        switch (gr.mo2417hq() - AttributeBuff._m_methodCount) {
            case 0:
                m38852Ue();
                return null;
            case 1:
                m38853Ug();
                return null;
            case 2:
                return m38855XQ();
            case 3:
                return m38856XS();
            case 4:
                return m38857XU();
            case 5:
                return m38858XW();
            case 6:
                return m38859Ye();
            case 7:
                m38860a((C0665JT) args[0]);
                return null;
            default:
                return super.mo14a(gr);
        }
    }

    /* renamed from: a */
    public void mo15a(Collection collection, C0495Gr gr) {
        super.mo15a(collection, gr);
    }

    /* renamed from: b */
    public void mo24b(C0665JT jt) {
        switch (bFf().mo6893i(f9107Do)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f9107Do, new Object[]{jt}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f9107Do, new Object[]{jt}));
                break;
        }
        m38860a(jt);
    }

    /* renamed from: c */
    public C5663aRz[] mo25c() {
        return _m_fields;
    }

    /* renamed from: a */
    public void mo21924a(ShipBuffType hOVar) {
        super.mo3934i(hOVar);
        ShipModuleAdapter aVar = (ShipModuleAdapter) bFf().mo6865M(ShipModuleAdapter.class);
        aVar.mo21925b(this);
        m38861a(aVar);
    }

    @C0064Am(aul = "7f215f0325e84c7ec2d439971e82b8af", aum = 0)
    /* renamed from: XQ */
    private C3892wO m38855XQ() {
        return m38867yE();
    }

    @C0064Am(aul = "538b53f1fdf3d022abf9c814727cf2a0", aum = 0)
    /* renamed from: XS */
    private C3892wO m38856XS() {
        return m38868yF();
    }

    @C0064Am(aul = "f61e7c7500fa1389c1569d8a0d61b101", aum = 0)
    /* renamed from: XU */
    private C3892wO m38857XU() {
        return m38869yG();
    }

    @C0064Am(aul = "54cc863f05b598209937a02914f11c18", aum = 0)
    /* renamed from: XW */
    private C3892wO m38858XW() {
        return m38870yH();
    }

    @C0064Am(aul = "6e17bf4921932d04324e4e7acbcbd0e3", aum = 0)
    /* renamed from: Ye */
    private C3892wO m38859Ye() {
        return m38871yI();
    }

    @C0064Am(aul = "dcd9f00ed89ded23815e26706832891f", aum = 0)
    /* renamed from: a */
    private void m38860a(C0665JT jt) {
        if (jt.mo3117j(1, 1, 2)) {
            try {
                System.out.println(jt.baw().toString());
            } catch (Exception e) {
                mo8354g("BaseModuleType error", (Throwable) e);
            }
        }
    }

    @C6485anp
    @C5511aMd
    /* renamed from: a.sZ$a */
    public class ShipModuleAdapter extends ShipAdapter implements C1616Xf {
        public static final int _m_fieldCount = 0;
        public static final C5663aRz[] _m_fields = null;
        public static final int _m_methodCount = 0;
        public static final C2491fm[] _m_methods = null;
        /* renamed from: aT */
        public static final C5663aRz f9120aT = null;
        public static final C2491fm aTe = null;
        public static final C2491fm aTf = null;
        public static final C2491fm aTg = null;
        public static final C2491fm aTh = null;
        public static final C2491fm bpZ = null;
        public static final long serialVersionUID = 0;
        public static C6494any ___iScriptClass;
        @C0064Am(aul = "75b6b55b227c389c091fe8764c2f800b", aum = 0)

        /* renamed from: xv */
        static /* synthetic */ ShipBuff f9121xv;

        static {
            m38889V();
        }

        public ShipModuleAdapter() {
            super((C5540aNg) null);
            super.mo10S();
        }

        public ShipModuleAdapter(C5540aNg ang) {
            super(ang);
        }

        public ShipModuleAdapter(ShipBuff sZVar) {
            super((C5540aNg) null);
            super._m_script_init(sZVar);
        }

        /* renamed from: V */
        static void m38889V() {
            serialVersionUID = (long) 1;
            _m_fieldCount = ShipAdapter._m_fieldCount + 1;
            _m_methodCount = ShipAdapter._m_methodCount + 5;
            int i = ShipAdapter._m_fieldCount;
            C5663aRz[] arzArr = new C5663aRz[(i + 1)];
            C5663aRz b = C5640aRc.m17844b(ShipModuleAdapter.class, "75b6b55b227c389c091fe8764c2f800b", i);
            f9120aT = b;
            arzArr[i] = b;
            int i2 = i + 1;
            _m_fields = arzArr;
            C1634Xv.m11725a((Object[]) ShipAdapter._m_fields, (Object[]) _m_fields);
            int i3 = ShipAdapter._m_methodCount;
            C2491fm[] fmVarArr = new C2491fm[(i3 + 5)];
            C2491fm a = C4105zY.m41624a(ShipModuleAdapter.class, "7be1b98d13aa968d28325a55d9add94a", i3);
            aTe = a;
            fmVarArr[i3] = a;
            int i4 = i3 + 1;
            C2491fm a2 = C4105zY.m41624a(ShipModuleAdapter.class, "c741e33792865ccd9019c54eeddb16df", i4);
            aTf = a2;
            fmVarArr[i4] = a2;
            int i5 = i4 + 1;
            C2491fm a3 = C4105zY.m41624a(ShipModuleAdapter.class, "39c184462d43c278bcac77dcaaab2738", i5);
            aTg = a3;
            fmVarArr[i5] = a3;
            int i6 = i5 + 1;
            C2491fm a4 = C4105zY.m41624a(ShipModuleAdapter.class, "632cd0bdd49f0490f4305a1c2aee276f", i6);
            aTh = a4;
            fmVarArr[i6] = a4;
            int i7 = i6 + 1;
            C2491fm a5 = C4105zY.m41624a(ShipModuleAdapter.class, "349c56af43754366d96fbe149eb65e18", i7);
            bpZ = a5;
            fmVarArr[i7] = a5;
            int i8 = i7 + 1;
            _m_methods = fmVarArr;
            C1634Xv.m11725a((Object[]) ShipAdapter._m_methods, (Object[]) _m_methods);
            ___iScriptClass = aUO.m18566a(ShipModuleAdapter.class, C2172cR.class, _m_fields, _m_methods);
        }

        /* renamed from: a */
        private void m38894a(ShipBuff sZVar) {
            bFf().mo5608dq().mo3197f(f9120aT, sZVar);
        }

        private ShipBuff aRK() {
            return (ShipBuff) bFf().mo5608dq().mo3214p(f9120aT);
        }

        /* renamed from: S */
        public void mo10S() {
            super.mo10S();
        }

        /* renamed from: T */
        public C6494any mo11T() {
            return ___iScriptClass;
        }

        /* renamed from: U */
        public C2491fm[] mo12U() {
            return _m_methods;
        }

        /* renamed from: VF */
        public float mo5665VF() {
            switch (bFf().mo6893i(aTe)) {
                case 0:
                    return 0.0f;
                case 2:
                    return ((Float) bFf().mo5606d(new aCE(this, aTe, new Object[0]))).floatValue();
                case 3:
                    bFf().mo5606d(new aCE(this, aTe, new Object[0]));
                    break;
            }
            return m38890VE();
        }

        /* renamed from: VH */
        public float mo5666VH() {
            switch (bFf().mo6893i(aTf)) {
                case 0:
                    return 0.0f;
                case 2:
                    return ((Float) bFf().mo5606d(new aCE(this, aTf, new Object[0]))).floatValue();
                case 3:
                    bFf().mo5606d(new aCE(this, aTf, new Object[0]));
                    break;
            }
            return m38891VG();
        }

        /* renamed from: W */
        public Object mo13W() {
            return new C2172cR(this);
        }

        /* renamed from: a */
        public Object mo14a(C0495Gr gr) {
            gr.getArgs();
            switch (gr.mo2417hq() - ShipAdapter._m_methodCount) {
                case 0:
                    return new Float(m38890VE());
                case 1:
                    return new Float(m38891VG());
                case 2:
                    return new Float(m38892VI());
                case 3:
                    return new Float(m38893VJ());
                case 4:
                    return new Float(ago());
                default:
                    return super.mo14a(gr);
            }
        }

        /* renamed from: a */
        public void mo15a(Collection collection, C0495Gr gr) {
            super.mo15a(collection, gr);
        }

        public float agp() {
            switch (bFf().mo6893i(bpZ)) {
                case 0:
                    return 0.0f;
                case 2:
                    return ((Float) bFf().mo5606d(new aCE(this, bpZ, new Object[0]))).floatValue();
                case 3:
                    bFf().mo5606d(new aCE(this, bpZ, new Object[0]));
                    break;
            }
            return ago();
        }

        /* renamed from: c */
        public C5663aRz[] mo25c() {
            return _m_fields;
        }

        /* renamed from: ra */
        public float mo5668ra() {
            switch (bFf().mo6893i(aTh)) {
                case 0:
                    return 0.0f;
                case 2:
                    return ((Float) bFf().mo5606d(new aCE(this, aTh, new Object[0]))).floatValue();
                case 3:
                    bFf().mo5606d(new aCE(this, aTh, new Object[0]));
                    break;
            }
            return m38893VJ();
        }

        /* renamed from: rb */
        public float mo5669rb() {
            switch (bFf().mo6893i(aTg)) {
                case 0:
                    return 0.0f;
                case 2:
                    return ((Float) bFf().mo5606d(new aCE(this, aTg, new Object[0]))).floatValue();
                case 3:
                    bFf().mo5606d(new aCE(this, aTg, new Object[0]));
                    break;
            }
            return m38892VI();
        }

        /* renamed from: b */
        public void mo21925b(ShipBuff sZVar) {
            m38894a(sZVar);
            super.mo10S();
        }

        @C0064Am(aul = "7be1b98d13aa968d28325a55d9add94a", aum = 0)
        /* renamed from: VE */
        private float m38890VE() {
            return aRK().mo21922XX().mo22753v(((ShipAdapter) mo16070IE()).mo5665VF(), super.mo5665VF());
        }

        @C0064Am(aul = "c741e33792865ccd9019c54eeddb16df", aum = 0)
        /* renamed from: VG */
        private float m38891VG() {
            return aRK().mo21921XV().mo22753v(((ShipAdapter) mo16070IE()).mo5666VH(), super.mo5666VH());
        }

        @C0064Am(aul = "39c184462d43c278bcac77dcaaab2738", aum = 0)
        /* renamed from: VI */
        private float m38892VI() {
            return aRK().mo21920XT().mo22753v(((ShipAdapter) mo16070IE()).mo5669rb(), super.mo5669rb());
        }

        @C0064Am(aul = "632cd0bdd49f0490f4305a1c2aee276f", aum = 0)
        /* renamed from: VJ */
        private float m38893VJ() {
            return aRK().mo21919XR().mo22753v(((ShipAdapter) mo16070IE()).mo5668ra(), super.mo5668ra());
        }

        @C0064Am(aul = "349c56af43754366d96fbe149eb65e18", aum = 0)
        private float ago() {
            return aRK().mo21923Yf().mo22753v(((ShipAdapter) mo16070IE()).agp(), super.agp());
        }
    }
}
