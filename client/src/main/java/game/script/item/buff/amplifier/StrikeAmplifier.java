package game.script.item.buff.amplifier;

import game.network.message.externalizable.aCE;
import game.network.message.serializable.C1556Wo;
import game.script.damage.DamageType;
import game.script.item.Amplifier;
import game.script.item.AmplifierType;
import game.script.ship.categories.Bomber;
import game.script.ship.strike.StrikeAdapter;
import logic.baa.C1616Xf;
import logic.baa.C1634Xv;
import logic.baa.C5511aMd;
import logic.baa.C6485anp;
import logic.data.mbean.C0171By;
import logic.data.mbean.C5573aOn;
import logic.res.code.C5640aRc;
import logic.res.code.C5663aRz;
import logic.res.code.C6494any;
import logic.res.html.C2491fm;
import p001a.*;

import java.util.Collection;

@C6485anp
@C2712iu(mo19786Bt = BaseCriticalStrikeAmplifier.class)
@C5511aMd
/* renamed from: a.kv */
/* compiled from: a */
public class StrikeAmplifier extends Amplifier implements C1616Xf {
    public static final int _m_fieldCount = 0;
    public static final C5663aRz[] _m_fields = null;
    public static final int _m_methodCount = 0;
    public static final C2491fm[] _m_methods = null;
    public static final C5663aRz atP = null;
    public static final C5663aRz atQ = null;
    public static final C5663aRz atS = null;
    public static final C5663aRz atT = null;
    public static final C5663aRz atV = null;
    public static final C2491fm atW = null;
    public static final C2491fm atX = null;
    public static final C2491fm atY = null;
    public static final C2491fm atZ = null;
    public static final C2491fm aua = null;
    public static final C2491fm aub = null;
    public static final long serialVersionUID = 0;
    public static C6494any ___iScriptClass;
    @C0064Am(aul = "b13e9f11de56a0de42fbb3a7b8e8244a", aum = 1)

    /* renamed from: Yu */
    private static C3892wO f8468Yu;
    @C0064Am(aul = "b9fb82e0733f92651d6592c7e6c0f353", aum = 3)

    /* renamed from: Yv */
    private static C3892wO f8469Yv;
    @C0064Am(aul = "0bed127b10a73bc20f1a884dfd60a487", aum = 0)
    private static C1556Wo<DamageType, C3892wO> atO;
    @C0064Am(aul = "0d65131028104100e8e29fd6849927b7", aum = 2)
    private static C3892wO atR;
    @C0064Am(aul = "10ab863d6fc2ad26f5085d0f49562b81", aum = 4)
    private static StrikeAmplifierAdapter atU;

    static {
        m34615V();
    }

    public StrikeAmplifier() {
        super((C5540aNg) null);
        super.mo10S();
    }

    public StrikeAmplifier(C5540aNg ang) {
        super(ang);
    }

    public StrikeAmplifier(AmplifierType iDVar) {
        super((C5540aNg) null);
        super.mo1956e(iDVar);
    }

    /* renamed from: V */
    static void m34615V() {
        serialVersionUID = (long) 1;
        _m_fieldCount = Amplifier._m_fieldCount + 5;
        _m_methodCount = Amplifier._m_methodCount + 6;
        int i = Amplifier._m_fieldCount;
        C5663aRz[] arzArr = new C5663aRz[(i + 5)];
        C5663aRz b = C5640aRc.m17844b(StrikeAmplifier.class, "0bed127b10a73bc20f1a884dfd60a487", i);
        atP = b;
        arzArr[i] = b;
        int i2 = i + 1;
        C5663aRz b2 = C5640aRc.m17844b(StrikeAmplifier.class, "b13e9f11de56a0de42fbb3a7b8e8244a", i2);
        atQ = b2;
        arzArr[i2] = b2;
        int i3 = i2 + 1;
        C5663aRz b3 = C5640aRc.m17844b(StrikeAmplifier.class, "0d65131028104100e8e29fd6849927b7", i3);
        atS = b3;
        arzArr[i3] = b3;
        int i4 = i3 + 1;
        C5663aRz b4 = C5640aRc.m17844b(StrikeAmplifier.class, "b9fb82e0733f92651d6592c7e6c0f353", i4);
        atT = b4;
        arzArr[i4] = b4;
        int i5 = i4 + 1;
        C5663aRz b5 = C5640aRc.m17844b(StrikeAmplifier.class, "10ab863d6fc2ad26f5085d0f49562b81", i5);
        atV = b5;
        arzArr[i5] = b5;
        int i6 = i5 + 1;
        _m_fields = arzArr;
        C1634Xv.m11725a((Object[]) Amplifier._m_fields, (Object[]) _m_fields);
        int i7 = Amplifier._m_methodCount;
        C2491fm[] fmVarArr = new C2491fm[(i7 + 6)];
        C2491fm a = C4105zY.m41624a(StrikeAmplifier.class, "62eb2edd610c5cf0b2c091f5299db09b", i7);
        atW = a;
        fmVarArr[i7] = a;
        int i8 = i7 + 1;
        C2491fm a2 = C4105zY.m41624a(StrikeAmplifier.class, "2c7b34fd72b0ff881fad8bced04b652d", i8);
        atX = a2;
        fmVarArr[i8] = a2;
        int i9 = i8 + 1;
        C2491fm a3 = C4105zY.m41624a(StrikeAmplifier.class, "ca51899c44bca4bcf6d9277882ad29d8", i9);
        atY = a3;
        fmVarArr[i9] = a3;
        int i10 = i9 + 1;
        C2491fm a4 = C4105zY.m41624a(StrikeAmplifier.class, "a337104064a3dce2af095d94abef751e", i10);
        atZ = a4;
        fmVarArr[i10] = a4;
        int i11 = i10 + 1;
        C2491fm a5 = C4105zY.m41624a(StrikeAmplifier.class, "ffb1524008bc6a96d0295193ee195dae", i11);
        aua = a5;
        fmVarArr[i11] = a5;
        int i12 = i11 + 1;
        C2491fm a6 = C4105zY.m41624a(StrikeAmplifier.class, "38e792e3c5916078666d4e088cff8a08", i12);
        aub = a6;
        fmVarArr[i12] = a6;
        int i13 = i12 + 1;
        _m_methods = fmVarArr;
        C1634Xv.m11725a((Object[]) Amplifier._m_methods, (Object[]) _m_methods);
        ___iScriptClass = aUO.m18566a(StrikeAmplifier.class, C5573aOn.class, _m_fields, _m_methods);
    }

    /* access modifiers changed from: private */
    /* renamed from: KA */
    public C3892wO m34602KA() {
        switch (bFf().mo6893i(aub)) {
            case 0:
                return null;
            case 2:
                return (C3892wO) bFf().mo5606d(new aCE(this, aub, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, aub, new Object[0]));
                break;
        }
        return m34614Kz();
    }

    /* renamed from: Km */
    private C1556Wo m34603Km() {
        return ((StrikeAmplifierType) getType()).mo12332UD();
    }

    /* renamed from: Kn */
    private C3892wO m34604Kn() {
        return ((StrikeAmplifierType) getType()).bLz();
    }

    /* renamed from: Ko */
    private C3892wO m34605Ko() {
        return ((StrikeAmplifierType) getType()).bLB();
    }

    /* renamed from: Kp */
    private C3892wO m34606Kp() {
        return ((StrikeAmplifierType) getType()).bLD();
    }

    /* renamed from: Kq */
    private StrikeAmplifierAdapter m34607Kq() {
        return (StrikeAmplifierAdapter) bFf().mo5608dq().mo3214p(atV);
    }

    /* access modifiers changed from: private */
    /* renamed from: Kw */
    public C3892wO m34611Kw() {
        switch (bFf().mo6893i(atZ)) {
            case 0:
                return null;
            case 2:
                return (C3892wO) bFf().mo5606d(new aCE(this, atZ, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, atZ, new Object[0]));
                break;
        }
        return m34610Kv();
    }

    /* access modifiers changed from: private */
    /* renamed from: Ky */
    public C3892wO m34613Ky() {
        switch (bFf().mo6893i(aua)) {
            case 0:
                return null;
            case 2:
                return (C3892wO) bFf().mo5606d(new aCE(this, aua, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, aua, new Object[0]));
                break;
        }
        return m34612Kx();
    }

    /* renamed from: a */
    private void m34618a(StrikeAmplifierAdapter aVar) {
        bFf().mo5608dq().mo3197f(atV, aVar);
    }

    /* access modifiers changed from: private */
    /* renamed from: f */
    public C3892wO m34622f(DamageType fr) {
        switch (bFf().mo6893i(atY)) {
            case 0:
                return null;
            case 2:
                return (C3892wO) bFf().mo5606d(new aCE(this, atY, new Object[]{fr}));
            case 3:
                bFf().mo5606d(new aCE(this, atY, new Object[]{fr}));
                break;
        }
        return m34621e(fr);
    }

    /* renamed from: i */
    private void m34623i(C1556Wo wo) {
        throw new C6039afL();
    }

    /* renamed from: s */
    private void m34624s(C3892wO wOVar) {
        throw new C6039afL();
    }

    /* renamed from: t */
    private void m34625t(C3892wO wOVar) {
        throw new C6039afL();
    }

    /* renamed from: u */
    private void m34626u(C3892wO wOVar) {
        throw new C6039afL();
    }

    /* renamed from: Ks */
    public void mo1952Ks() {
        switch (bFf().mo6893i(atW)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, atW, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, atW, new Object[0]));
                break;
        }
        m34608Kr();
    }

    /* renamed from: Ku */
    public void mo1953Ku() {
        switch (bFf().mo6893i(atX)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, atX, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, atX, new Object[0]));
                break;
        }
        m34609Kt();
    }

    /* renamed from: S */
    public void mo10S() {
        super.mo10S();
    }

    /* renamed from: T */
    public C6494any mo11T() {
        return ___iScriptClass;
    }

    /* renamed from: U */
    public C2491fm[] mo12U() {
        return _m_methods;
    }

    /* renamed from: W */
    public Object mo13W() {
        return new C5573aOn(this);
    }

    /* renamed from: a */
    public Object mo14a(C0495Gr gr) {
        Object[] args = gr.getArgs();
        switch (gr.mo2417hq() - Amplifier._m_methodCount) {
            case 0:
                m34608Kr();
                return null;
            case 1:
                m34609Kt();
                return null;
            case 2:
                return m34621e((DamageType) args[0]);
            case 3:
                return m34610Kv();
            case 4:
                return m34612Kx();
            case 5:
                return m34614Kz();
            default:
                return super.mo14a(gr);
        }
    }

    /* renamed from: a */
    public void mo15a(Collection collection, C0495Gr gr) {
        super.mo15a(collection, gr);
    }

    /* renamed from: c */
    public C5663aRz[] mo25c() {
        return _m_fields;
    }

    /* renamed from: e */
    public void mo1956e(AmplifierType iDVar) {
        super.mo1956e(iDVar);
    }

    @C0064Am(aul = "62eb2edd610c5cf0b2c091f5299db09b", aum = 0)
    /* renamed from: Kr */
    private void m34608Kr() {
        if (mo7855al() instanceof Bomber) {
            StrikeAmplifierAdapter aVar = (StrikeAmplifierAdapter) bFf().mo6865M(StrikeAmplifierAdapter.class);
            aVar.mo20153e(this);
            m34618a(aVar);
            m34607Kq().push();
            ((Bomber) mo7855al()).dcN().mo5353TJ().mo23261e(m34607Kq());
            asW();
        }
    }

    @C0064Am(aul = "2c7b34fd72b0ff881fad8bced04b652d", aum = 0)
    /* renamed from: Kt */
    private void m34609Kt() {
        if (mo7855al() instanceof Bomber) {
            ((Bomber) mo7855al()).dcN().mo5353TJ().mo23262g(m34607Kq());
            asW();
        }
    }

    @C0064Am(aul = "ca51899c44bca4bcf6d9277882ad29d8", aum = 0)
    /* renamed from: e */
    private C3892wO m34621e(DamageType fr) {
        return (C3892wO) m34603Km().get(fr);
    }

    @C0064Am(aul = "a337104064a3dce2af095d94abef751e", aum = 0)
    /* renamed from: Kv */
    private C3892wO m34610Kv() {
        return m34605Ko();
    }

    @C0064Am(aul = "ffb1524008bc6a96d0295193ee195dae", aum = 0)
    /* renamed from: Kx */
    private C3892wO m34612Kx() {
        return m34606Kp();
    }

    @C0064Am(aul = "38e792e3c5916078666d4e088cff8a08", aum = 0)
    /* renamed from: Kz */
    private C3892wO m34614Kz() {
        return m34604Kn();
    }

    @C6485anp
    @C5511aMd
    /* renamed from: a.kv$a */
    public class StrikeAmplifierAdapter extends StrikeAdapter implements C1616Xf {
        public static final int _m_fieldCount = 0;
        public static final C5663aRz[] _m_fields = null;
        public static final int _m_methodCount = 0;
        public static final C2491fm[] _m_methods = null;
        /* renamed from: aT */
        public static final C5663aRz f8470aT = null;
        public static final C2491fm bbS = null;
        public static final C2491fm bbU = null;
        public static final C2491fm ccE = null;
        public static final long serialVersionUID = 0;
        /* renamed from: yg */
        public static final C2491fm f8471yg = null;
        public static C6494any ___iScriptClass;
        @C0064Am(aul = "15c97e06c6e67df2644f72546511e5d2", aum = 0)
        static /* synthetic */ StrikeAmplifier ccD;

        static {
            m34637V();
        }

        public StrikeAmplifierAdapter() {
            super((C5540aNg) null);
            super.mo10S();
        }

        public StrikeAmplifierAdapter(C5540aNg ang) {
            super(ang);
        }

        public StrikeAmplifierAdapter(StrikeAmplifier kvVar) {
            super((C5540aNg) null);
            super._m_script_init(kvVar);
        }

        /* renamed from: V */
        static void m34637V() {
            serialVersionUID = (long) 1;
            _m_fieldCount = StrikeAdapter._m_fieldCount + 1;
            _m_methodCount = StrikeAdapter._m_methodCount + 4;
            int i = StrikeAdapter._m_fieldCount;
            C5663aRz[] arzArr = new C5663aRz[(i + 1)];
            C5663aRz b = C5640aRc.m17844b(StrikeAmplifierAdapter.class, "15c97e06c6e67df2644f72546511e5d2", i);
            f8470aT = b;
            arzArr[i] = b;
            int i2 = i + 1;
            _m_fields = arzArr;
            C1634Xv.m11725a((Object[]) StrikeAdapter._m_fields, (Object[]) _m_fields);
            int i3 = StrikeAdapter._m_methodCount;
            C2491fm[] fmVarArr = new C2491fm[(i3 + 4)];
            C2491fm a = C4105zY.m41624a(StrikeAmplifierAdapter.class, "627437156840f7b6081975a6926a7fc7", i3);
            bbU = a;
            fmVarArr[i3] = a;
            int i4 = i3 + 1;
            C2491fm a2 = C4105zY.m41624a(StrikeAmplifierAdapter.class, "d385f407f68a41855af90cc8fb9640ed", i4);
            bbS = a2;
            fmVarArr[i4] = a2;
            int i5 = i4 + 1;
            C2491fm a3 = C4105zY.m41624a(StrikeAmplifierAdapter.class, "847103601a965046d94c41216ddc1db1", i5);
            f8471yg = a3;
            fmVarArr[i5] = a3;
            int i6 = i5 + 1;
            C2491fm a4 = C4105zY.m41624a(StrikeAmplifierAdapter.class, "7f689a3f808bd0d10114cc67ba88abde", i6);
            ccE = a4;
            fmVarArr[i6] = a4;
            int i7 = i6 + 1;
            _m_methods = fmVarArr;
            C1634Xv.m11725a((Object[]) StrikeAdapter._m_methods, (Object[]) _m_methods);
            ___iScriptClass = aUO.m18566a(StrikeAmplifierAdapter.class, C0171By.class, _m_fields, _m_methods);
        }

        private StrikeAmplifier asO() {
            return (StrikeAmplifier) bFf().mo5608dq().mo3214p(f8470aT);
        }

        /* renamed from: d */
        private void m34640d(StrikeAmplifier kvVar) {
            bFf().mo5608dq().mo3197f(f8470aT, kvVar);
        }

        /* renamed from: S */
        public void mo10S() {
            super.mo10S();
        }

        /* renamed from: T */
        public C6494any mo11T() {
            return ___iScriptClass;
        }

        /* renamed from: U */
        public C2491fm[] mo12U() {
            return _m_methods;
        }

        /* renamed from: W */
        public Object mo13W() {
            return new C0171By(this);
        }

        /* renamed from: YE */
        public float mo7994YE() {
            switch (bFf().mo6893i(bbS)) {
                case 0:
                    return 0.0f;
                case 2:
                    return ((Float) bFf().mo5606d(new aCE(this, bbS, new Object[0]))).floatValue();
                case 3:
                    bFf().mo5606d(new aCE(this, bbS, new Object[0]));
                    break;
            }
            return m34638YD();
        }

        /* renamed from: YG */
        public float mo7995YG() {
            switch (bFf().mo6893i(bbU)) {
                case 0:
                    return 0.0f;
                case 2:
                    return ((Float) bFf().mo5606d(new aCE(this, bbU, new Object[0]))).floatValue();
                case 3:
                    bFf().mo5606d(new aCE(this, bbU, new Object[0]));
                    break;
            }
            return m34639YF();
        }

        /* renamed from: a */
        public Object mo14a(C0495Gr gr) {
            Object[] args = gr.getArgs();
            switch (gr.mo2417hq() - StrikeAdapter._m_methodCount) {
                case 0:
                    return new Float(m34639YF());
                case 1:
                    return new Float(m34638YD());
                case 2:
                    return new Float(m34641js());
                case 3:
                    return m34642s((DamageType) args[0]);
                default:
                    return super.mo14a(gr);
            }
        }

        /* renamed from: a */
        public void mo15a(Collection collection, C0495Gr gr) {
            super.mo15a(collection, gr);
        }

        /* renamed from: c */
        public C5663aRz[] mo25c() {
            return _m_fields;
        }

        /* renamed from: jt */
        public float mo7996jt() {
            switch (bFf().mo6893i(f8471yg)) {
                case 0:
                    return 0.0f;
                case 2:
                    return ((Float) bFf().mo5606d(new aCE(this, f8471yg, new Object[0]))).floatValue();
                case 3:
                    bFf().mo5606d(new aCE(this, f8471yg, new Object[0]));
                    break;
            }
            return m34641js();
        }

        /* renamed from: t */
        public C3892wO mo7997t(DamageType fr) {
            switch (bFf().mo6893i(ccE)) {
                case 0:
                    return null;
                case 2:
                    return (C3892wO) bFf().mo5606d(new aCE(this, ccE, new Object[]{fr}));
                case 3:
                    bFf().mo5606d(new aCE(this, ccE, new Object[]{fr}));
                    break;
            }
            return m34642s(fr);
        }

        /* renamed from: e */
        public void mo20153e(StrikeAmplifier kvVar) {
            m34640d(kvVar);
            super.mo10S();
        }

        @C0064Am(aul = "627437156840f7b6081975a6926a7fc7", aum = 0)
        /* renamed from: YF */
        private float m34639YF() {
            return asO().m34602KA().mo22753v(((StrikeAdapter) mo16070IE()).mo7995YG(), ((StrikeAdapter) mo16071IG()).mo7995YG());
        }

        @C0064Am(aul = "d385f407f68a41855af90cc8fb9640ed", aum = 0)
        /* renamed from: YD */
        private float m34638YD() {
            return asO().m34613Ky().mo22753v(((StrikeAdapter) mo16070IE()).mo7994YE(), ((StrikeAdapter) mo16071IG()).mo7994YE());
        }

        @C0064Am(aul = "847103601a965046d94c41216ddc1db1", aum = 0)
        /* renamed from: js */
        private float m34641js() {
            return asO().m34611Kw().mo22753v(((StrikeAdapter) mo16070IE()).mo7996jt(), ((StrikeAdapter) mo16071IG()).mo7996jt());
        }

        @C0064Am(aul = "7f689a3f808bd0d10114cc67ba88abde", aum = 0)
        /* renamed from: s */
        private C3892wO m34642s(DamageType fr) {
            if (asO().m34622f(fr).getValue() == 0.0f) {
                return ((StrikeAdapter) mo16071IG()).mo7997t(fr);
            }
            return asO().m34622f(fr).mo22745ah(((StrikeAdapter) mo16071IG()).mo7997t(fr));
        }
    }
}
