package game.script.item.buff.module;

import game.network.message.externalizable.aCE;
import game.script.Actor;
import game.script.damage.DamageType;
import game.script.ship.ShieldAdapter;
import logic.baa.C1616Xf;
import logic.baa.C1634Xv;
import logic.baa.C5511aMd;
import logic.baa.C6485anp;
import logic.data.mbean.C1514WH;
import logic.data.mbean.aUZ;
import logic.res.code.C5640aRc;
import logic.res.code.C5663aRz;
import logic.res.code.C6494any;
import logic.res.html.C2491fm;
import p001a.*;

import java.util.Collection;

@C5829abJ("1.1.2")
@C6485anp
@C2712iu(version = "1.1.2")
@C5511aMd
/* renamed from: a.LR */
/* compiled from: a */
public class ShieldBuff extends AttributeBuff implements C1616Xf {
    public static final int _m_fieldCount = 0;
    public static final C5663aRz[] _m_fields = null;
    public static final int _m_methodCount = 0;
    public static final C2491fm[] _m_methods = null;
    public static final C5663aRz aOJ = null;
    public static final C5663aRz aOr = null;
    public static final C5663aRz aOt = null;
    public static final C5663aRz aOz = null;
    public static final C2491fm aPH = null;
    public static final C2491fm aPI = null;
    public static final C2491fm aPJ = null;
    public static final C2491fm aPK = null;
    public static final C5663aRz eNm = null;
    public static final C2491fm eNn = null;
    public static final C2491fm eNo = null;
    public static final long serialVersionUID = 0;
    public static C6494any ___iScriptClass;
    @C0064Am(aul = "454e9b2732a3d613ae853cb3f741f875", aum = 0)
    private static C3892wO aBJ;
    @C0064Am(aul = "8377dea39d14749649790927519c5ed4", aum = 1)
    private static C3892wO aBK;
    @C0064Am(aul = "c85b486c9da831b01525756f51b10fd8", aum = 2)
    private static C3892wO aBL;
    @C0064Am(aul = "1e6e5302dfba79a09df1de3b80fef577", aum = 3)
    private static C3892wO aBM;
    @C0064Am(aul = "9780d2a0f44c18867fec5ad07f8de409", aum = 4)
    private static ShieldModuleAdapter eyr;

    static {
        m6745V();
    }

    public ShieldBuff() {
        super((C5540aNg) null);
        super.mo10S();
    }

    public ShieldBuff(C5540aNg ang) {
        super(ang);
    }

    public ShieldBuff(ShieldBuffType art) {
        super((C5540aNg) null);
        super._m_script_init(art);
    }

    /* renamed from: V */
    static void m6745V() {
        serialVersionUID = (long) 1;
        _m_fieldCount = AttributeBuff._m_fieldCount + 5;
        _m_methodCount = AttributeBuff._m_methodCount + 6;
        int i = AttributeBuff._m_fieldCount;
        C5663aRz[] arzArr = new C5663aRz[(i + 5)];
        C5663aRz b = C5640aRc.m17844b(ShieldBuff.class, "454e9b2732a3d613ae853cb3f741f875", i);
        aOJ = b;
        arzArr[i] = b;
        int i2 = i + 1;
        C5663aRz b2 = C5640aRc.m17844b(ShieldBuff.class, "8377dea39d14749649790927519c5ed4", i2);
        aOr = b2;
        arzArr[i2] = b2;
        int i3 = i2 + 1;
        C5663aRz b3 = C5640aRc.m17844b(ShieldBuff.class, "c85b486c9da831b01525756f51b10fd8", i3);
        aOz = b3;
        arzArr[i3] = b3;
        int i4 = i3 + 1;
        C5663aRz b4 = C5640aRc.m17844b(ShieldBuff.class, "1e6e5302dfba79a09df1de3b80fef577", i4);
        aOt = b4;
        arzArr[i4] = b4;
        int i5 = i4 + 1;
        C5663aRz b5 = C5640aRc.m17844b(ShieldBuff.class, "9780d2a0f44c18867fec5ad07f8de409", i5);
        eNm = b5;
        arzArr[i5] = b5;
        int i6 = i5 + 1;
        _m_fields = arzArr;
        C1634Xv.m11725a((Object[]) AttributeBuff._m_fields, (Object[]) _m_fields);
        int i7 = AttributeBuff._m_methodCount;
        C2491fm[] fmVarArr = new C2491fm[(i7 + 6)];
        C2491fm a = C4105zY.m41624a(ShieldBuff.class, "f73723053486dc750948187c1b393fb2", i7);
        aPH = a;
        fmVarArr[i7] = a;
        int i8 = i7 + 1;
        C2491fm a2 = C4105zY.m41624a(ShieldBuff.class, "aa76e2c63218af6f5a42e141a3735fd9", i8);
        aPI = a2;
        fmVarArr[i8] = a2;
        int i9 = i8 + 1;
        C2491fm a3 = C4105zY.m41624a(ShieldBuff.class, "e03a51bd7b89e92cad4a6bcf0812f7e5", i9);
        eNn = a3;
        fmVarArr[i9] = a3;
        int i10 = i9 + 1;
        C2491fm a4 = C4105zY.m41624a(ShieldBuff.class, "98a0493b2acb1f0c5a5c8dfe8c2a929a", i10);
        eNo = a4;
        fmVarArr[i10] = a4;
        int i11 = i10 + 1;
        C2491fm a5 = C4105zY.m41624a(ShieldBuff.class, "6e7606a0ce6701ea6b1b84233ab6e831", i11);
        aPJ = a5;
        fmVarArr[i11] = a5;
        int i12 = i11 + 1;
        C2491fm a6 = C4105zY.m41624a(ShieldBuff.class, "f77260fe5ad6f186a3d447ae810a0342", i12);
        aPK = a6;
        fmVarArr[i12] = a6;
        int i13 = i12 + 1;
        _m_methods = fmVarArr;
        C1634Xv.m11725a((Object[]) AttributeBuff._m_methods, (Object[]) _m_methods);
        ___iScriptClass = aUO.m18566a(ShieldBuff.class, C1514WH.class, _m_fields, _m_methods);
    }

    /* renamed from: U */
    private void m6738U(C3892wO wOVar) {
        throw new C6039afL();
    }

    /* renamed from: Ub */
    private C3892wO m6739Ub() {
        return ((ShieldBuffType) getType()).aBc();
    }

    /* renamed from: Uc */
    private C3892wO m6740Uc() {
        return ((ShieldBuffType) getType()).aBe();
    }

    /* renamed from: V */
    private void m6746V(C3892wO wOVar) {
        throw new C6039afL();
    }

    /* renamed from: a */
    private void m6747a(ShieldModuleAdapter aVar) {
        bFf().mo5608dq().mo3197f(eNm, aVar);
    }

    private C3892wO aOS() {
        return ((ShieldBuffType) getType()).aOV();
    }

    private C3892wO aOT() {
        return ((ShieldBuffType) getType()).aOX();
    }

    private ShieldModuleAdapter djx() {
        return (ShieldModuleAdapter) bFf().mo5608dq().mo3214p(eNm);
    }

    /* renamed from: v */
    private void m6748v(C3892wO wOVar) {
        throw new C6039afL();
    }

    /* renamed from: w */
    private void m6749w(C3892wO wOVar) {
        throw new C6039afL();
    }

    /* renamed from: S */
    public void mo10S() {
        super.mo10S();
    }

    /* renamed from: T */
    public C6494any mo11T() {
        return ___iScriptClass;
    }

    /* renamed from: U */
    public C2491fm[] mo12U() {
        return _m_methods;
    }

    /* renamed from: Uf */
    public void mo3751Uf() {
        switch (bFf().mo6893i(aPH)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, aPH, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, aPH, new Object[0]));
                break;
        }
        m6741Ue();
    }

    /* renamed from: Uh */
    public void mo3752Uh() {
        switch (bFf().mo6893i(aPI)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, aPI, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, aPI, new Object[0]));
                break;
        }
        m6742Ug();
    }

    /* access modifiers changed from: protected */
    /* renamed from: Uj */
    public C3892wO mo3753Uj() {
        switch (bFf().mo6893i(aPJ)) {
            case 0:
                return null;
            case 2:
                return (C3892wO) bFf().mo5606d(new aCE(this, aPJ, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, aPJ, new Object[0]));
                break;
        }
        return m6743Ui();
    }

    /* access modifiers changed from: protected */
    /* renamed from: Ul */
    public C3892wO mo3754Ul() {
        switch (bFf().mo6893i(aPK)) {
            case 0:
                return null;
            case 2:
                return (C3892wO) bFf().mo5606d(new aCE(this, aPK, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, aPK, new Object[0]));
                break;
        }
        return m6744Uk();
    }

    /* renamed from: W */
    public Object mo13W() {
        return new C1514WH(this);
    }

    /* renamed from: a */
    public Object mo14a(C0495Gr gr) {
        gr.getArgs();
        switch (gr.mo2417hq() - AttributeBuff._m_methodCount) {
            case 0:
                m6741Ue();
                return null;
            case 1:
                m6742Ug();
                return null;
            case 2:
                return bIf();
            case 3:
                return bIh();
            case 4:
                return m6743Ui();
            case 5:
                return m6744Uk();
            default:
                return super.mo14a(gr);
        }
    }

    /* renamed from: a */
    public void mo15a(Collection collection, C0495Gr gr) {
        super.mo15a(collection, gr);
    }

    /* access modifiers changed from: protected */
    public C3892wO bIg() {
        switch (bFf().mo6893i(eNn)) {
            case 0:
                return null;
            case 2:
                return (C3892wO) bFf().mo5606d(new aCE(this, eNn, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, eNn, new Object[0]));
                break;
        }
        return bIf();
    }

    /* access modifiers changed from: protected */
    public C3892wO bIi() {
        switch (bFf().mo6893i(eNo)) {
            case 0:
                return null;
            case 2:
                return (C3892wO) bFf().mo5606d(new aCE(this, eNo, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, eNo, new Object[0]));
                break;
        }
        return bIh();
    }

    /* renamed from: c */
    public C5663aRz[] mo25c() {
        return _m_fields;
    }

    /* renamed from: a */
    public void mo3755a(ShieldBuffType art) {
        super.mo3934i(art);
        ShieldModuleAdapter aVar = (ShieldModuleAdapter) bFf().mo6865M(ShieldModuleAdapter.class);
        aVar.mo3760b(this);
        m6747a(aVar);
    }

    @C0064Am(aul = "f73723053486dc750948187c1b393fb2", aum = 0)
    /* renamed from: Ue */
    private void m6741Ue() {
        float value;
        if (bhP() == null) {
            mo8358lY("No Target has being set");
            return;
        }
        bhP().mo8288zv().mo5353TJ().mo23261e(djx());
        if (mo3754Ul().coZ() == C3892wO.C3893a.PERCENT) {
            value = ((ShieldAdapter) djx().mo16070IE()).mo3761hh() * (mo3754Ul().getValue() / 100.0f);
        } else {
            value = mo3754Ul().getValue();
        }
        bhP().mo8288zv().mo19085b(value, bhN());
    }

    @C0064Am(aul = "aa76e2c63218af6f5a42e141a3735fd9", aum = 0)
    /* renamed from: Ug */
    private void m6742Ug() {
        if (bhP() == null) {
            mo8358lY("No Target has being set");
            return;
        }
        bhP().mo8288zv().mo5353TJ().mo23262g(djx());
        bhP().mo8288zv().mo19085b(0.0f, (Actor) null);
    }

    @C0064Am(aul = "e03a51bd7b89e92cad4a6bcf0812f7e5", aum = 0)
    private C3892wO bIf() {
        return aOT();
    }

    @C0064Am(aul = "98a0493b2acb1f0c5a5c8dfe8c2a929a", aum = 0)
    private C3892wO bIh() {
        return aOS();
    }

    @C0064Am(aul = "6e7606a0ce6701ea6b1b84233ab6e831", aum = 0)
    /* renamed from: Ui */
    private C3892wO m6743Ui() {
        return m6740Uc();
    }

    @C0064Am(aul = "f77260fe5ad6f186a3d447ae810a0342", aum = 0)
    /* renamed from: Uk */
    private C3892wO m6744Uk() {
        return m6739Ub();
    }

    @C6485anp
    @C5511aMd
    /* renamed from: a.LR$a */
    public class ShieldModuleAdapter extends ShieldAdapter implements C1616Xf {
        public static final int _m_fieldCount = 0;
        public static final C5663aRz[] _m_fields = null;
        public static final int _m_methodCount = 0;
        public static final C2491fm[] _m_methods = null;
        /* renamed from: aT */
        public static final C5663aRz f1043aT = null;
        /* renamed from: qa */
        public static final C2491fm f1044qa = null;
        /* renamed from: qb */
        public static final C2491fm f1045qb = null;
        public static final long serialVersionUID = 0;
        /* renamed from: uG */
        public static final C2491fm f1046uG = null;
        /* renamed from: uH */
        public static final C2491fm f1047uH = null;
        /* renamed from: uI */
        public static final C2491fm f1048uI = null;
        public static C6494any ___iScriptClass;
        @C0064Am(aul = "2404bdd4de19b614ef741ff18b334d44", aum = 0)
        static /* synthetic */ ShieldBuff dyp;

        static {
            m6762V();
        }

        public ShieldModuleAdapter() {
            super((C5540aNg) null);
            super.mo10S();
        }

        public ShieldModuleAdapter(ShieldBuff lr) {
            super((C5540aNg) null);
            super._m_script_init(lr);
        }

        public ShieldModuleAdapter(C5540aNg ang) {
            super(ang);
        }

        /* renamed from: V */
        static void m6762V() {
            serialVersionUID = (long) 1;
            _m_fieldCount = ShieldAdapter._m_fieldCount + 1;
            _m_methodCount = ShieldAdapter._m_methodCount + 5;
            int i = ShieldAdapter._m_fieldCount;
            C5663aRz[] arzArr = new C5663aRz[(i + 1)];
            C5663aRz b = C5640aRc.m17844b(ShieldModuleAdapter.class, "2404bdd4de19b614ef741ff18b334d44", i);
            f1043aT = b;
            arzArr[i] = b;
            int i2 = i + 1;
            _m_fields = arzArr;
            C1634Xv.m11725a((Object[]) ShieldAdapter._m_fields, (Object[]) _m_fields);
            int i3 = ShieldAdapter._m_methodCount;
            C2491fm[] fmVarArr = new C2491fm[(i3 + 5)];
            C2491fm a = C4105zY.m41624a(ShieldModuleAdapter.class, "61250636c058b5a47c5fc880634cb38b", i3);
            f1044qa = a;
            fmVarArr[i3] = a;
            int i4 = i3 + 1;
            C2491fm a2 = C4105zY.m41624a(ShieldModuleAdapter.class, "19426718bdd6149ead69b952355f62d6", i4);
            f1048uI = a2;
            fmVarArr[i4] = a2;
            int i5 = i4 + 1;
            C2491fm a3 = C4105zY.m41624a(ShieldModuleAdapter.class, "014fc7951d76c096e90ca08ef5134def", i5);
            f1045qb = a3;
            fmVarArr[i5] = a3;
            int i6 = i5 + 1;
            C2491fm a4 = C4105zY.m41624a(ShieldModuleAdapter.class, "5e6b375295d07b3f115a8b6786749f42", i6);
            f1047uH = a4;
            fmVarArr[i6] = a4;
            int i7 = i6 + 1;
            C2491fm a5 = C4105zY.m41624a(ShieldModuleAdapter.class, "327ca89162fbcabb3d977b3cc720e547", i7);
            f1046uG = a5;
            fmVarArr[i7] = a5;
            int i8 = i7 + 1;
            _m_methods = fmVarArr;
            C1634Xv.m11725a((Object[]) ShieldAdapter._m_methods, (Object[]) _m_methods);
            ___iScriptClass = aUO.m18566a(ShieldModuleAdapter.class, aUZ.class, _m_fields, _m_methods);
        }

        /* renamed from: a */
        private void m6765a(ShieldBuff lr) {
            bFf().mo5608dq().mo3197f(f1043aT, lr);
        }

        private ShieldBuff bgh() {
            return (ShieldBuff) bFf().mo5608dq().mo3214p(f1043aT);
        }

        /* renamed from: S */
        public void mo10S() {
            super.mo10S();
        }

        /* renamed from: T */
        public C6494any mo11T() {
            return ___iScriptClass;
        }

        /* renamed from: U */
        public C2491fm[] mo12U() {
            return _m_methods;
        }

        /* renamed from: W */
        public Object mo13W() {
            return new aUZ(this);
        }

        /* renamed from: a */
        public Object mo14a(C0495Gr gr) {
            Object[] args = gr.getArgs();
            switch (gr.mo2417hq() - ShieldAdapter._m_methodCount) {
                case 0:
                    return new Float(m6764a((DamageType) args[0], ((Float) args[1]).floatValue()));
                case 1:
                    return new Float(m6763a((DamageType) args[0]));
                case 2:
                    return new Float(m6767hg());
                case 3:
                    return new Float(m6768ib());
                case 4:
                    return new Float(m6766hZ());
                default:
                    return super.mo14a(gr);
            }
        }

        /* renamed from: a */
        public void mo15a(Collection collection, C0495Gr gr) {
            super.mo15a(collection, gr);
        }

        /* renamed from: b */
        public float mo3758b(DamageType fr) {
            switch (bFf().mo6893i(f1048uI)) {
                case 0:
                    return 0.0f;
                case 2:
                    return ((Float) bFf().mo5606d(new aCE(this, f1048uI, new Object[]{fr}))).floatValue();
                case 3:
                    bFf().mo5606d(new aCE(this, f1048uI, new Object[]{fr}));
                    break;
            }
            return m6763a(fr);
        }

        /* renamed from: b */
        public float mo3759b(DamageType fr, float f) {
            switch (bFf().mo6893i(f1044qa)) {
                case 0:
                    return 0.0f;
                case 2:
                    return ((Float) bFf().mo5606d(new aCE(this, f1044qa, new Object[]{fr, new Float(f)}))).floatValue();
                case 3:
                    bFf().mo5606d(new aCE(this, f1044qa, new Object[]{fr, new Float(f)}));
                    break;
            }
            return m6764a(fr, f);
        }

        /* renamed from: c */
        public C5663aRz[] mo25c() {
            return _m_fields;
        }

        /* renamed from: hh */
        public float mo3761hh() {
            switch (bFf().mo6893i(f1045qb)) {
                case 0:
                    return 0.0f;
                case 2:
                    return ((Float) bFf().mo5606d(new aCE(this, f1045qb, new Object[0]))).floatValue();
                case 3:
                    bFf().mo5606d(new aCE(this, f1045qb, new Object[0]));
                    break;
            }
            return m6767hg();
        }

        /* renamed from: ia */
        public float mo3762ia() {
            switch (bFf().mo6893i(f1046uG)) {
                case 0:
                    return 0.0f;
                case 2:
                    return ((Float) bFf().mo5606d(new aCE(this, f1046uG, new Object[0]))).floatValue();
                case 3:
                    bFf().mo5606d(new aCE(this, f1046uG, new Object[0]));
                    break;
            }
            return m6766hZ();
        }

        /* renamed from: ic */
        public float mo3763ic() {
            switch (bFf().mo6893i(f1047uH)) {
                case 0:
                    return 0.0f;
                case 2:
                    return ((Float) bFf().mo5606d(new aCE(this, f1047uH, new Object[0]))).floatValue();
                case 3:
                    bFf().mo5606d(new aCE(this, f1047uH, new Object[0]));
                    break;
            }
            return m6768ib();
        }

        /* renamed from: b */
        public void mo3760b(ShieldBuff lr) {
            m6765a(lr);
            super.mo10S();
        }

        @C0064Am(aul = "61250636c058b5a47c5fc880634cb38b", aum = 0)
        /* renamed from: a */
        private float m6764a(DamageType fr, float f) {
            return bgh().mo3753Uj().mo22753v(((ShieldAdapter) mo16070IE()).mo3759b(fr, f), super.mo3759b(fr, f));
        }

        @C0064Am(aul = "19426718bdd6149ead69b952355f62d6", aum = 0)
        /* renamed from: a */
        private float m6763a(DamageType fr) {
            return bgh().mo3753Uj().mo22753v(((ShieldAdapter) mo16070IE()).mo3758b(fr), super.mo3758b(fr));
        }

        @C0064Am(aul = "014fc7951d76c096e90ca08ef5134def", aum = 0)
        /* renamed from: hg */
        private float m6767hg() {
            float v = bgh().mo3754Ul().mo22753v(((ShieldAdapter) mo16070IE()).mo3761hh(), super.mo3761hh());
            if (v > 0.0f) {
                return v;
            }
            return 0.0f;
        }

        @C0064Am(aul = "5e6b375295d07b3f115a8b6786749f42", aum = 0)
        /* renamed from: ib */
        private float m6768ib() {
            return bgh().bIg().mo22753v(((ShieldAdapter) mo16070IE()).mo3763ic(), super.mo3763ic());
        }

        @C0064Am(aul = "327ca89162fbcabb3d977b3cc720e547", aum = 0)
        /* renamed from: hZ */
        private float m6766hZ() {
            return bgh().bIi().mo22753v(((ShieldAdapter) mo16070IE()).mo3762ia(), super.mo3762ia());
        }
    }
}
