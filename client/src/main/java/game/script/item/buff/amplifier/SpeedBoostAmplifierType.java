package game.script.item.buff.amplifier;

import game.network.message.externalizable.aCE;
import game.script.item.AmplifierType;
import logic.baa.*;
import logic.data.mbean.aRP;
import logic.res.code.C5640aRc;
import logic.res.code.C5663aRz;
import logic.res.code.C6494any;
import logic.res.html.C2491fm;
import p001a.*;

import java.util.Collection;

@C0566Hp
@C5511aMd
@C6485anp
/* renamed from: a.acA  reason: case insensitive filesystem */
/* compiled from: a */
public class SpeedBoostAmplifierType extends AmplifierType implements C1616Xf {

    /* renamed from: VE */
    public static final C2491fm f4190VE = null;

    /* renamed from: VF */
    public static final C2491fm f4191VF = null;

    /* renamed from: VG */
    public static final C2491fm f4192VG = null;

    /* renamed from: VH */
    public static final C2491fm f4193VH = null;

    /* renamed from: VI */
    public static final C2491fm f4194VI = null;

    /* renamed from: VJ */
    public static final C2491fm f4195VJ = null;

    /* renamed from: VK */
    public static final C2491fm f4196VK = null;

    /* renamed from: VL */
    public static final C2491fm f4197VL = null;
    /* renamed from: Vt */
    public static final C5663aRz f4199Vt = null;
    /* renamed from: Vv */
    public static final C5663aRz f4201Vv = null;
    /* renamed from: Vx */
    public static final C5663aRz f4203Vx = null;
    /* renamed from: Vz */
    public static final C5663aRz f4205Vz = null;
    public static final int _m_fieldCount = 0;
    public static final C5663aRz[] _m_fields = null;
    public static final int _m_methodCount = 0;
    public static final C2491fm[] _m_methods = null;
    public static final C5663aRz atS = null;
    /* renamed from: dN */
    public static final C2491fm f4206dN = null;
    public static final C2491fm eUQ = null;
    public static final C2491fm eUR = null;
    public static final C2491fm fcf = null;
    public static final long serialVersionUID = 0;
    public static C6494any ___iScriptClass;
    @C0064Am(aul = "49d4c27484c703a036ef1947ff98de2e", aum = 1)

    /* renamed from: Vs */
    private static C3892wO f4198Vs;
    @C0064Am(aul = "693ca6dc591e75ba49bcbf875f97ab0b", aum = 2)

    /* renamed from: Vu */
    private static C3892wO f4200Vu;
    @C0064Am(aul = "9df74af1b2a987e9be4559819b16a70a", aum = 4)

    /* renamed from: Vw */
    private static C3892wO f4202Vw;
    @C0064Am(aul = "997671b0fc9ac7ffac025c57b609afc4", aum = 3)

    /* renamed from: Vy */
    private static C3892wO f4204Vy;
    @C0064Am(aul = "506828a5e139329b66ab187d06f86ac9", aum = 0)
    private static C3892wO atR;

    static {
        m20196V();
    }

    public SpeedBoostAmplifierType() {
        super((C5540aNg) null);
        super.mo10S();
    }

    public SpeedBoostAmplifierType(C5540aNg ang) {
        super(ang);
    }

    /* renamed from: V */
    static void m20196V() {
        serialVersionUID = (long) 1;
        _m_fieldCount = AmplifierType._m_fieldCount + 5;
        _m_methodCount = AmplifierType._m_methodCount + 12;
        int i = AmplifierType._m_fieldCount;
        C5663aRz[] arzArr = new C5663aRz[(i + 5)];
        C5663aRz b = C5640aRc.m17844b(SpeedBoostAmplifierType.class, "506828a5e139329b66ab187d06f86ac9", i);
        atS = b;
        arzArr[i] = b;
        int i2 = i + 1;
        C5663aRz b2 = C5640aRc.m17844b(SpeedBoostAmplifierType.class, "49d4c27484c703a036ef1947ff98de2e", i2);
        f4199Vt = b2;
        arzArr[i2] = b2;
        int i3 = i2 + 1;
        C5663aRz b3 = C5640aRc.m17844b(SpeedBoostAmplifierType.class, "693ca6dc591e75ba49bcbf875f97ab0b", i3);
        f4201Vv = b3;
        arzArr[i3] = b3;
        int i4 = i3 + 1;
        C5663aRz b4 = C5640aRc.m17844b(SpeedBoostAmplifierType.class, "997671b0fc9ac7ffac025c57b609afc4", i4);
        f4205Vz = b4;
        arzArr[i4] = b4;
        int i5 = i4 + 1;
        C5663aRz b5 = C5640aRc.m17844b(SpeedBoostAmplifierType.class, "9df74af1b2a987e9be4559819b16a70a", i5);
        f4203Vx = b5;
        arzArr[i5] = b5;
        int i6 = i5 + 1;
        _m_fields = arzArr;
        C1634Xv.m11725a((Object[]) AmplifierType._m_fields, (Object[]) _m_fields);
        int i7 = AmplifierType._m_methodCount;
        C2491fm[] fmVarArr = new C2491fm[(i7 + 12)];
        C2491fm a = C4105zY.m41624a(SpeedBoostAmplifierType.class, "b1a5e399a9a0ff1c218a719c9df478b2", i7);
        eUQ = a;
        fmVarArr[i7] = a;
        int i8 = i7 + 1;
        C2491fm a2 = C4105zY.m41624a(SpeedBoostAmplifierType.class, "4d1ecd902bf323025f5c8fa084efd277", i8);
        eUR = a2;
        fmVarArr[i8] = a2;
        int i9 = i8 + 1;
        C2491fm a3 = C4105zY.m41624a(SpeedBoostAmplifierType.class, "4739f222b4849115af701085ba03be82", i9);
        f4190VE = a3;
        fmVarArr[i9] = a3;
        int i10 = i9 + 1;
        C2491fm a4 = C4105zY.m41624a(SpeedBoostAmplifierType.class, "3a0b158e666df2075ef99a7c18afd050", i10);
        f4191VF = a4;
        fmVarArr[i10] = a4;
        int i11 = i10 + 1;
        C2491fm a5 = C4105zY.m41624a(SpeedBoostAmplifierType.class, "850b8b273ca30ff61b4835180abb9626", i11);
        f4192VG = a5;
        fmVarArr[i11] = a5;
        int i12 = i11 + 1;
        C2491fm a6 = C4105zY.m41624a(SpeedBoostAmplifierType.class, "163fa1993e2101ef4f566ed7f1c3ad14", i12);
        f4193VH = a6;
        fmVarArr[i12] = a6;
        int i13 = i12 + 1;
        C2491fm a7 = C4105zY.m41624a(SpeedBoostAmplifierType.class, "395790ed0fa5e38b29c4206aab9a5a5f", i13);
        f4196VK = a7;
        fmVarArr[i13] = a7;
        int i14 = i13 + 1;
        C2491fm a8 = C4105zY.m41624a(SpeedBoostAmplifierType.class, "216188532a35b265b30a2bfaa8b3d67e", i14);
        f4197VL = a8;
        fmVarArr[i14] = a8;
        int i15 = i14 + 1;
        C2491fm a9 = C4105zY.m41624a(SpeedBoostAmplifierType.class, "45150e87ae7786fbe99378fa2bebeb73", i15);
        f4194VI = a9;
        fmVarArr[i15] = a9;
        int i16 = i15 + 1;
        C2491fm a10 = C4105zY.m41624a(SpeedBoostAmplifierType.class, "35f00f8743f91b177d8773fd27ff322c", i16);
        f4195VJ = a10;
        fmVarArr[i16] = a10;
        int i17 = i16 + 1;
        C2491fm a11 = C4105zY.m41624a(SpeedBoostAmplifierType.class, "1b5730538fd21b3a5109193c1fa23e95", i17);
        fcf = a11;
        fmVarArr[i17] = a11;
        int i18 = i17 + 1;
        C2491fm a12 = C4105zY.m41624a(SpeedBoostAmplifierType.class, "894e4e30a4a238f8215b942d27143154", i18);
        f4206dN = a12;
        fmVarArr[i18] = a12;
        int i19 = i18 + 1;
        _m_methods = fmVarArr;
        C1634Xv.m11725a((Object[]) AmplifierType._m_methods, (Object[]) _m_methods);
        ___iScriptClass = aUO.m18566a(SpeedBoostAmplifierType.class, aRP.class, _m_fields, _m_methods);
    }

    /* renamed from: Ko */
    private C3892wO m20195Ko() {
        return (C3892wO) bFf().mo5608dq().mo3214p(atS);
    }

    /* renamed from: d */
    private void m20199d(C3892wO wOVar) {
        bFf().mo5608dq().mo3197f(f4199Vt, wOVar);
    }

    /* renamed from: e */
    private void m20200e(C3892wO wOVar) {
        bFf().mo5608dq().mo3197f(f4201Vv, wOVar);
    }

    /* renamed from: f */
    private void m20201f(C3892wO wOVar) {
        bFf().mo5608dq().mo3197f(f4203Vx, wOVar);
    }

    /* renamed from: g */
    private void m20202g(C3892wO wOVar) {
        bFf().mo5608dq().mo3197f(f4205Vz, wOVar);
    }

    /* renamed from: t */
    private void m20207t(C3892wO wOVar) {
        bFf().mo5608dq().mo3197f(atS, wOVar);
    }

    /* renamed from: yE */
    private C3892wO m20208yE() {
        return (C3892wO) bFf().mo5608dq().mo3214p(f4199Vt);
    }

    /* renamed from: yF */
    private C3892wO m20209yF() {
        return (C3892wO) bFf().mo5608dq().mo3214p(f4201Vv);
    }

    /* renamed from: yG */
    private C3892wO m20210yG() {
        return (C3892wO) bFf().mo5608dq().mo3214p(f4203Vx);
    }

    /* renamed from: yH */
    private C3892wO m20211yH() {
        return (C3892wO) bFf().mo5608dq().mo3214p(f4205Vz);
    }

    /* renamed from: T */
    public C6494any mo11T() {
        return ___iScriptClass;
    }

    /* renamed from: U */
    public C2491fm[] mo12U() {
        return _m_methods;
    }

    /* renamed from: W */
    public Object mo13W() {
        return new aRP(this);
    }

    /* renamed from: a */
    public Object mo14a(C0495Gr gr) {
        Object[] args = gr.getArgs();
        switch (gr.mo2417hq() - AmplifierType._m_methodCount) {
            case 0:
                return bLA();
            case 1:
                m20198ac((C3892wO) args[0]);
                return null;
            case 2:
                return m20212yK();
            case 3:
                m20203i((C3892wO) args[0]);
                return null;
            case 4:
                return m20213yM();
            case 5:
                m20204k((C3892wO) args[0]);
                return null;
            case 6:
                return m20215yQ();
            case 7:
                m20206o((C3892wO) args[0]);
                return null;
            case 8:
                return m20214yO();
            case 9:
                m20205m((C3892wO) args[0]);
                return null;
            case 10:
                return bOZ();
            case 11:
                return m20197aT();
            default:
                return super.mo14a(gr);
        }
    }

    /* renamed from: a */
    public void mo15a(Collection collection, C0495Gr gr) {
        super.mo15a(collection, gr);
    }

    /* access modifiers changed from: protected */
    /* renamed from: aU */
    public <T extends aDJ> T mo745aU() {
        switch (bFf().mo6893i(f4206dN)) {
            case 0:
                return null;
            case 2:
                return (aDJ) bFf().mo5606d(new aCE(this, f4206dN, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f4206dN, new Object[0]));
                break;
        }
        return m20197aT();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Duration")
    /* renamed from: ad */
    public void mo12560ad(C3892wO wOVar) {
        switch (bFf().mo6893i(eUR)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, eUR, new Object[]{wOVar}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, eUR, new Object[]{wOVar}));
                break;
        }
        m20198ac(wOVar);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Duration")
    public C3892wO bLB() {
        switch (bFf().mo6893i(eUQ)) {
            case 0:
                return null;
            case 2:
                return (C3892wO) bFf().mo5606d(new aCE(this, eUQ, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, eUQ, new Object[0]));
                break;
        }
        return bLA();
    }

    public SpeedBoostAmplifier bPa() {
        switch (bFf().mo6893i(fcf)) {
            case 0:
                return null;
            case 2:
                return (SpeedBoostAmplifier) bFf().mo5606d(new aCE(this, fcf, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, fcf, new Object[0]));
                break;
        }
        return bOZ();
    }

    /* renamed from: c */
    public C5663aRz[] mo25c() {
        return _m_fields;
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Linear Velocity")
    /* renamed from: j */
    public void mo12563j(C3892wO wOVar) {
        switch (bFf().mo6893i(f4191VF)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f4191VF, new Object[]{wOVar}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f4191VF, new Object[]{wOVar}));
                break;
        }
        m20203i(wOVar);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Linear Acceleration")
    /* renamed from: l */
    public void mo12564l(C3892wO wOVar) {
        switch (bFf().mo6893i(f4193VH)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f4193VH, new Object[]{wOVar}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f4193VH, new Object[]{wOVar}));
                break;
        }
        m20204k(wOVar);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Angular Velocity")
    /* renamed from: n */
    public void mo12565n(C3892wO wOVar) {
        switch (bFf().mo6893i(f4195VJ)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f4195VJ, new Object[]{wOVar}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f4195VJ, new Object[]{wOVar}));
                break;
        }
        m20205m(wOVar);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Angular Acceleration")
    /* renamed from: p */
    public void mo12566p(C3892wO wOVar) {
        switch (bFf().mo6893i(f4197VL)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f4197VL, new Object[]{wOVar}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f4197VL, new Object[]{wOVar}));
                break;
        }
        m20206o(wOVar);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Linear Velocity")
    /* renamed from: yL */
    public C3892wO mo12567yL() {
        switch (bFf().mo6893i(f4190VE)) {
            case 0:
                return null;
            case 2:
                return (C3892wO) bFf().mo5606d(new aCE(this, f4190VE, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f4190VE, new Object[0]));
                break;
        }
        return m20212yK();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Linear Acceleration")
    /* renamed from: yN */
    public C3892wO mo12568yN() {
        switch (bFf().mo6893i(f4192VG)) {
            case 0:
                return null;
            case 2:
                return (C3892wO) bFf().mo5606d(new aCE(this, f4192VG, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f4192VG, new Object[0]));
                break;
        }
        return m20213yM();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Angular Velocity")
    /* renamed from: yP */
    public C3892wO mo12569yP() {
        switch (bFf().mo6893i(f4194VI)) {
            case 0:
                return null;
            case 2:
                return (C3892wO) bFf().mo5606d(new aCE(this, f4194VI, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f4194VI, new Object[0]));
                break;
        }
        return m20214yO();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Angular Acceleration")
    /* renamed from: yR */
    public C3892wO mo12570yR() {
        switch (bFf().mo6893i(f4196VK)) {
            case 0:
                return null;
            case 2:
                return (C3892wO) bFf().mo5606d(new aCE(this, f4196VK, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f4196VK, new Object[0]));
                break;
        }
        return m20215yQ();
    }

    /* renamed from: S */
    public void mo10S() {
        super.mo10S();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Duration")
    @C0064Am(aul = "b1a5e399a9a0ff1c218a719c9df478b2", aum = 0)
    private C3892wO bLA() {
        return m20195Ko();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Duration")
    @C0064Am(aul = "4d1ecd902bf323025f5c8fa084efd277", aum = 0)
    /* renamed from: ac */
    private void m20198ac(C3892wO wOVar) {
        m20207t(wOVar);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Linear Velocity")
    @C0064Am(aul = "4739f222b4849115af701085ba03be82", aum = 0)
    /* renamed from: yK */
    private C3892wO m20212yK() {
        return m20208yE();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Linear Velocity")
    @C0064Am(aul = "3a0b158e666df2075ef99a7c18afd050", aum = 0)
    /* renamed from: i */
    private void m20203i(C3892wO wOVar) {
        m20199d(wOVar);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Linear Acceleration")
    @C0064Am(aul = "850b8b273ca30ff61b4835180abb9626", aum = 0)
    /* renamed from: yM */
    private C3892wO m20213yM() {
        return m20209yF();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Linear Acceleration")
    @C0064Am(aul = "163fa1993e2101ef4f566ed7f1c3ad14", aum = 0)
    /* renamed from: k */
    private void m20204k(C3892wO wOVar) {
        m20200e(wOVar);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Angular Acceleration")
    @C0064Am(aul = "395790ed0fa5e38b29c4206aab9a5a5f", aum = 0)
    /* renamed from: yQ */
    private C3892wO m20215yQ() {
        return m20211yH();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Angular Acceleration")
    @C0064Am(aul = "216188532a35b265b30a2bfaa8b3d67e", aum = 0)
    /* renamed from: o */
    private void m20206o(C3892wO wOVar) {
        m20202g(wOVar);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Angular Velocity")
    @C0064Am(aul = "45150e87ae7786fbe99378fa2bebeb73", aum = 0)
    /* renamed from: yO */
    private C3892wO m20214yO() {
        return m20210yG();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Angular Velocity")
    @C0064Am(aul = "35f00f8743f91b177d8773fd27ff322c", aum = 0)
    /* renamed from: m */
    private void m20205m(C3892wO wOVar) {
        m20201f(wOVar);
    }

    @C0064Am(aul = "1b5730538fd21b3a5109193c1fa23e95", aum = 0)
    private SpeedBoostAmplifier bOZ() {
        return (SpeedBoostAmplifier) mo745aU();
    }

    @C0064Am(aul = "894e4e30a4a238f8215b942d27143154", aum = 0)
    /* renamed from: aT */
    private <T extends aDJ> T m20197aT() {
        T t = (SpeedBoostAmplifier) bFf().mo6865M(SpeedBoostAmplifier.class);
        t.mo1956e((AmplifierType) this);
        return t;
    }
}
