package game.script.item.buff.module;

import game.network.message.externalizable.aCE;
import game.script.item.Module;
import game.script.template.BaseTaikodomContent;
import logic.baa.*;
import logic.data.mbean.C5661aRx;
import logic.res.code.C5640aRc;
import logic.res.code.C5663aRz;
import logic.res.code.C6494any;
import logic.res.html.C2491fm;
import p001a.*;

import java.util.Collection;

@C0566Hp
@C5829abJ("1.1.2")
@C6485anp
@C5511aMd
/* renamed from: a.aPi  reason: case insensitive filesystem */
/* compiled from: a */
public abstract class AttributeBuffType extends BaseTaikodomContent implements C1616Xf {
    public static final int _m_fieldCount = 0;
    public static final C5663aRz[] _m_fields = null;
    public static final int _m_methodCount = 0;
    public static final C2491fm[] _m_methods = null;
    public static final C5663aRz dCo = null;
    public static final C2491fm iAT = null;
    public static final C2491fm iAU = null;
    public static final long serialVersionUID = 0;
    public static C6494any ___iScriptClass;
    @C0064Am(aul = "b0e9361d5c13d6cd9d4a7599e92d57eb", aum = 0)
    private static Module.C4010b bMz;

    static {
        m17317V();
    }

    public AttributeBuffType() {
        super((C5540aNg) null);
        super.mo10S();
    }

    public AttributeBuffType(C5540aNg ang) {
        super(ang);
    }

    /* renamed from: V */
    static void m17317V() {
        serialVersionUID = (long) 1;
        _m_fieldCount = BaseTaikodomContent._m_fieldCount + 1;
        _m_methodCount = BaseTaikodomContent._m_methodCount + 2;
        int i = BaseTaikodomContent._m_fieldCount;
        C5663aRz[] arzArr = new C5663aRz[(i + 1)];
        C5663aRz b = C5640aRc.m17844b(AttributeBuffType.class, "b0e9361d5c13d6cd9d4a7599e92d57eb", i);
        dCo = b;
        arzArr[i] = b;
        int i2 = i + 1;
        _m_fields = arzArr;
        C1634Xv.m11725a((Object[]) BaseTaikodomContent._m_fields, (Object[]) _m_fields);
        int i3 = BaseTaikodomContent._m_methodCount;
        C2491fm[] fmVarArr = new C2491fm[(i3 + 2)];
        C2491fm a = C4105zY.m41624a(AttributeBuffType.class, "c2d6a9a226ed7148f574dfad4616fc44", i3);
        iAT = a;
        fmVarArr[i3] = a;
        int i4 = i3 + 1;
        C2491fm a2 = C4105zY.m41624a(AttributeBuffType.class, "1a87a96a6c3d52942392fa253f477279", i4);
        iAU = a2;
        fmVarArr[i4] = a2;
        int i5 = i4 + 1;
        _m_methods = fmVarArr;
        C1634Xv.m11725a((Object[]) BaseTaikodomContent._m_methods, (Object[]) _m_methods);
        ___iScriptClass = aUO.m18566a(AttributeBuffType.class, C5661aRx.class, _m_fields, _m_methods);
    }

    /* renamed from: a */
    private void m17318a(Module.C4010b bVar) {
        bFf().mo5608dq().mo3197f(dCo, bVar);
    }

    private Module.C4010b bhG() {
        return (Module.C4010b) bFf().mo5608dq().mo3214p(dCo);
    }

    /* renamed from: T */
    public C6494any mo11T() {
        return ___iScriptClass;
    }

    /* renamed from: U */
    public C2491fm[] mo12U() {
        return _m_methods;
    }

    /* renamed from: W */
    public Object mo13W() {
        return new C5661aRx(this);
    }

    /* renamed from: a */
    public Object mo14a(C0495Gr gr) {
        Object[] args = gr.getArgs();
        switch (gr.mo2417hq() - BaseTaikodomContent._m_methodCount) {
            case 0:
                return doc();
            case 1:
                m17319b((Module.C4010b) args[0]);
                return null;
            default:
                return super.mo14a(gr);
        }
    }

    /* renamed from: a */
    public void mo15a(Collection collection, C0495Gr gr) {
        super.mo15a(collection, gr);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Module Target")
    /* renamed from: c */
    public void mo10827c(Module.C4010b bVar) {
        switch (bFf().mo6893i(iAU)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, iAU, new Object[]{bVar}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, iAU, new Object[]{bVar}));
                break;
        }
        m17319b(bVar);
    }

    /* renamed from: c */
    public C5663aRz[] mo25c() {
        return _m_fields;
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Module Target")
    public Module.C4010b dod() {
        switch (bFf().mo6893i(iAT)) {
            case 0:
                return null;
            case 2:
                return (Module.C4010b) bFf().mo5606d(new aCE(this, iAT, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, iAT, new Object[0]));
                break;
        }
        return doc();
    }

    /* renamed from: S */
    public void mo10S() {
        super.mo10S();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Module Target")
    @C0064Am(aul = "c2d6a9a226ed7148f574dfad4616fc44", aum = 0)
    private Module.C4010b doc() {
        return bhG();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Module Target")
    @C0064Am(aul = "1a87a96a6c3d52942392fa253f477279", aum = 0)
    /* renamed from: b */
    private void m17319b(Module.C4010b bVar) {
        m17318a(bVar);
    }
}
