package game.script.item.buff.amplifier;

import game.network.message.externalizable.aCE;
import game.script.item.Amplifier;
import game.script.item.AmplifierType;
import game.script.ship.CruiseSpeedAdapter;
import logic.baa.C1616Xf;
import logic.baa.C1634Xv;
import logic.baa.C5511aMd;
import logic.baa.C6485anp;
import logic.data.mbean.C1730ZZ;
import logic.data.mbean.C5301aEb;
import logic.res.code.C5640aRc;
import logic.res.code.C5663aRz;
import logic.res.code.C6494any;
import logic.res.html.C2491fm;
import p001a.*;

import java.util.Collection;

@C5829abJ("2.0.0")
@C6485anp
@C2712iu(mo19786Bt = AmplifierType.class, version = "2.0.1")
@C5511aMd
/* renamed from: a.qU */
/* compiled from: a */
public class CruiseSpeedAmplifier extends Amplifier implements C1616Xf {

    /* renamed from: Lm */
    public static final C2491fm f8910Lm = null;
    /* renamed from: Vt */
    public static final C5663aRz f8912Vt = null;
    /* renamed from: Vv */
    public static final C5663aRz f8914Vv = null;
    /* renamed from: Vx */
    public static final C5663aRz f8916Vx = null;
    /* renamed from: Vz */
    public static final C5663aRz f8918Vz = null;
    public static final int _m_fieldCount = 0;
    public static final C5663aRz[] _m_fields = null;
    public static final int _m_methodCount = 0;
    public static final C2491fm[] _m_methods = null;
    public static final C5663aRz aYe = null;
    public static final C2491fm aYf = null;
    public static final C2491fm aYg = null;
    public static final C2491fm aYh = null;
    public static final C2491fm aYi = null;
    public static final C2491fm aYj = null;
    public static final C5663aRz atQ = null;
    public static final C5663aRz atT = null;
    public static final C2491fm atW = null;
    public static final C2491fm atX = null;
    public static final C2491fm aua = null;
    public static final C2491fm aub = null;
    public static final long serialVersionUID = 0;
    public static C6494any ___iScriptClass;
    @C0064Am(aul = "9b3435bc39a5ed28e12d9aa3e368af3e", aum = 0)

    /* renamed from: Vs */
    private static C3892wO f8911Vs;
    @C0064Am(aul = "ec26eb47555a45c1efc0b89d11c4a5d8", aum = 1)

    /* renamed from: Vu */
    private static C3892wO f8913Vu;
    @C0064Am(aul = "d867fe013f607f3e0102355d37b11448", aum = 2)

    /* renamed from: Vw */
    private static C3892wO f8915Vw;
    @C0064Am(aul = "f915d2e32ef8676d7055c64d52e19625", aum = 3)

    /* renamed from: Vy */
    private static C3892wO f8917Vy;
    @C0064Am(aul = "6aeea736fcb1456c2a2403331e63a28b", aum = 4)

    /* renamed from: Yu */
    private static C3892wO f8919Yu;
    @C0064Am(aul = "02f964332e79f85bc7a2eca3d95d7415", aum = 5)

    /* renamed from: Yv */
    private static C3892wO f8920Yv;
    @C0064Am(aul = "4e7fbc8ba5480b887afc4453fa473709", aum = 6)
    private static CruiseSpeedAdapter aYd;

    static {
        m37530V();
    }

    public CruiseSpeedAmplifier() {
        super((C5540aNg) null);
        super.mo10S();
    }

    public CruiseSpeedAmplifier(C5540aNg ang) {
        super(ang);
    }

    public CruiseSpeedAmplifier(AmplifierType iDVar) {
        super((C5540aNg) null);
        super.mo1956e(iDVar);
    }

    /* renamed from: V */
    static void m37530V() {
        serialVersionUID = (long) 1;
        _m_fieldCount = Amplifier._m_fieldCount + 7;
        _m_methodCount = Amplifier._m_methodCount + 10;
        int i = Amplifier._m_fieldCount;
        C5663aRz[] arzArr = new C5663aRz[(i + 7)];
        C5663aRz b = C5640aRc.m17844b(CruiseSpeedAmplifier.class, "9b3435bc39a5ed28e12d9aa3e368af3e", i);
        f8912Vt = b;
        arzArr[i] = b;
        int i2 = i + 1;
        C5663aRz b2 = C5640aRc.m17844b(CruiseSpeedAmplifier.class, "ec26eb47555a45c1efc0b89d11c4a5d8", i2);
        f8914Vv = b2;
        arzArr[i2] = b2;
        int i3 = i2 + 1;
        C5663aRz b3 = C5640aRc.m17844b(CruiseSpeedAmplifier.class, "d867fe013f607f3e0102355d37b11448", i3);
        f8916Vx = b3;
        arzArr[i3] = b3;
        int i4 = i3 + 1;
        C5663aRz b4 = C5640aRc.m17844b(CruiseSpeedAmplifier.class, "f915d2e32ef8676d7055c64d52e19625", i4);
        f8918Vz = b4;
        arzArr[i4] = b4;
        int i5 = i4 + 1;
        C5663aRz b5 = C5640aRc.m17844b(CruiseSpeedAmplifier.class, "6aeea736fcb1456c2a2403331e63a28b", i5);
        atQ = b5;
        arzArr[i5] = b5;
        int i6 = i5 + 1;
        C5663aRz b6 = C5640aRc.m17844b(CruiseSpeedAmplifier.class, "02f964332e79f85bc7a2eca3d95d7415", i6);
        atT = b6;
        arzArr[i6] = b6;
        int i7 = i6 + 1;
        C5663aRz b7 = C5640aRc.m17844b(CruiseSpeedAmplifier.class, "4e7fbc8ba5480b887afc4453fa473709", i7);
        aYe = b7;
        arzArr[i7] = b7;
        int i8 = i7 + 1;
        _m_fields = arzArr;
        C1634Xv.m11725a((Object[]) Amplifier._m_fields, (Object[]) _m_fields);
        int i9 = Amplifier._m_methodCount;
        C2491fm[] fmVarArr = new C2491fm[(i9 + 10)];
        C2491fm a = C4105zY.m41624a(CruiseSpeedAmplifier.class, "8ec7b053bb197b0a62ce01b4798264ab", i9);
        aYf = a;
        fmVarArr[i9] = a;
        int i10 = i9 + 1;
        C2491fm a2 = C4105zY.m41624a(CruiseSpeedAmplifier.class, "7aa739e59cfde13b517bb0ce4f6f5fa2", i10);
        atW = a2;
        fmVarArr[i10] = a2;
        int i11 = i10 + 1;
        C2491fm a3 = C4105zY.m41624a(CruiseSpeedAmplifier.class, "06f32ab7dfa89b216d3bfd2134e540b7", i11);
        atX = a3;
        fmVarArr[i11] = a3;
        int i12 = i11 + 1;
        C2491fm a4 = C4105zY.m41624a(CruiseSpeedAmplifier.class, "65f44f68fbcb5d05eb9ca10ec1a9f857", i12);
        aYg = a4;
        fmVarArr[i12] = a4;
        int i13 = i12 + 1;
        C2491fm a5 = C4105zY.m41624a(CruiseSpeedAmplifier.class, "3e69dd263f6e1eec2e53ce38da238a1f", i13);
        aYh = a5;
        fmVarArr[i13] = a5;
        int i14 = i13 + 1;
        C2491fm a6 = C4105zY.m41624a(CruiseSpeedAmplifier.class, "09a736155c9e2a6388a84990a4ea8262", i14);
        aYi = a6;
        fmVarArr[i14] = a6;
        int i15 = i14 + 1;
        C2491fm a7 = C4105zY.m41624a(CruiseSpeedAmplifier.class, "4504e734bb14c45d20bcf4ba4073a288", i15);
        aYj = a7;
        fmVarArr[i15] = a7;
        int i16 = i15 + 1;
        C2491fm a8 = C4105zY.m41624a(CruiseSpeedAmplifier.class, "a055fe3bf1e7e750ce8f1dc629ffcb4e", i16);
        aua = a8;
        fmVarArr[i16] = a8;
        int i17 = i16 + 1;
        C2491fm a9 = C4105zY.m41624a(CruiseSpeedAmplifier.class, "152394cc340583d333e455198510fe70", i17);
        aub = a9;
        fmVarArr[i17] = a9;
        int i18 = i17 + 1;
        C2491fm a10 = C4105zY.m41624a(CruiseSpeedAmplifier.class, "2583947ff9e6244e7fda5a36639a4ca3", i18);
        f8910Lm = a10;
        fmVarArr[i18] = a10;
        int i19 = i18 + 1;
        _m_methods = fmVarArr;
        C1634Xv.m11725a((Object[]) Amplifier._m_methods, (Object[]) _m_methods);
        ___iScriptClass = aUO.m18566a(CruiseSpeedAmplifier.class, C5301aEb.class, _m_fields, _m_methods);
    }

    /* renamed from: Kn */
    private C3892wO m37524Kn() {
        return ((CruiseSpeedAmplifierType) getType()).bLz();
    }

    /* renamed from: Kp */
    private C3892wO m37525Kp() {
        return ((CruiseSpeedAmplifierType) getType()).bLD();
    }

    @C0064Am(aul = "7aa739e59cfde13b517bb0ce4f6f5fa2", aum = 0)
    @C5566aOg
    /* renamed from: Kr */
    private void m37526Kr() {
        throw new aWi(new aCE(this, atW, new Object[0]));
    }

    @C0064Am(aul = "06f32ab7dfa89b216d3bfd2134e540b7", aum = 0)
    @C5566aOg
    /* renamed from: Kt */
    private void m37527Kt() {
        throw new aWi(new aCE(this, atX, new Object[0]));
    }

    /* renamed from: XN */
    private CruiseSpeedAdapter m37531XN() {
        return (CruiseSpeedAdapter) bFf().mo5608dq().mo3214p(aYe);
    }

    /* renamed from: a */
    private void m37537a(CruiseSpeedAdapter eq) {
        bFf().mo5608dq().mo3197f(aYe, eq);
    }

    /* renamed from: d */
    private void m37538d(C3892wO wOVar) {
        throw new C6039afL();
    }

    /* renamed from: e */
    private void m37539e(C3892wO wOVar) {
        throw new C6039afL();
    }

    /* renamed from: f */
    private void m37540f(C3892wO wOVar) {
        throw new C6039afL();
    }

    /* renamed from: g */
    private void m37541g(C3892wO wOVar) {
        throw new C6039afL();
    }

    @C0064Am(aul = "2583947ff9e6244e7fda5a36639a4ca3", aum = 0)
    @C5566aOg
    @C2499fr
    /* renamed from: qU */
    private void m37542qU() {
        throw new aWi(new aCE(this, f8910Lm, new Object[0]));
    }

    /* renamed from: s */
    private void m37543s(C3892wO wOVar) {
        throw new C6039afL();
    }

    /* renamed from: u */
    private void m37544u(C3892wO wOVar) {
        throw new C6039afL();
    }

    /* renamed from: yE */
    private C3892wO m37545yE() {
        return ((CruiseSpeedAmplifierType) getType()).mo12628yL();
    }

    /* renamed from: yF */
    private C3892wO m37546yF() {
        return ((CruiseSpeedAmplifierType) getType()).mo12629yN();
    }

    /* renamed from: yG */
    private C3892wO m37547yG() {
        return ((CruiseSpeedAmplifierType) getType()).mo12630yP();
    }

    /* renamed from: yH */
    private C3892wO m37548yH() {
        return ((CruiseSpeedAmplifierType) getType()).mo12631yR();
    }

    /* access modifiers changed from: protected */
    /* renamed from: KA */
    public C3892wO mo21356KA() {
        switch (bFf().mo6893i(aub)) {
            case 0:
                return null;
            case 2:
                return (C3892wO) bFf().mo5606d(new aCE(this, aub, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, aub, new Object[0]));
                break;
        }
        return m37529Kz();
    }

    @C5566aOg
    /* renamed from: Ks */
    public void mo1952Ks() {
        switch (bFf().mo6893i(atW)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, atW, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, atW, new Object[0]));
                break;
        }
        m37526Kr();
    }

    @C5566aOg
    /* renamed from: Ku */
    public void mo1953Ku() {
        switch (bFf().mo6893i(atX)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, atX, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, atX, new Object[0]));
                break;
        }
        m37527Kt();
    }

    /* access modifiers changed from: protected */
    /* renamed from: Ky */
    public C3892wO mo21357Ky() {
        switch (bFf().mo6893i(aua)) {
            case 0:
                return null;
            case 2:
                return (C3892wO) bFf().mo5606d(new aCE(this, aua, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, aua, new Object[0]));
                break;
        }
        return m37528Kx();
    }

    /* renamed from: S */
    public void mo10S() {
        super.mo10S();
    }

    /* renamed from: T */
    public C6494any mo11T() {
        return ___iScriptClass;
    }

    /* renamed from: U */
    public C2491fm[] mo12U() {
        return _m_methods;
    }

    /* renamed from: W */
    public Object mo13W() {
        return new C5301aEb(this);
    }

    /* renamed from: XP */
    public CruiseSpeedAdapter mo21358XP() {
        switch (bFf().mo6893i(aYf)) {
            case 0:
                return null;
            case 2:
                return (CruiseSpeedAdapter) bFf().mo5606d(new aCE(this, aYf, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, aYf, new Object[0]));
                break;
        }
        return m37532XO();
    }

    /* access modifiers changed from: protected */
    /* renamed from: XR */
    public C3892wO mo21359XR() {
        switch (bFf().mo6893i(aYg)) {
            case 0:
                return null;
            case 2:
                return (C3892wO) bFf().mo5606d(new aCE(this, aYg, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, aYg, new Object[0]));
                break;
        }
        return m37533XQ();
    }

    /* access modifiers changed from: protected */
    /* renamed from: XT */
    public C3892wO mo21360XT() {
        switch (bFf().mo6893i(aYh)) {
            case 0:
                return null;
            case 2:
                return (C3892wO) bFf().mo5606d(new aCE(this, aYh, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, aYh, new Object[0]));
                break;
        }
        return m37534XS();
    }

    /* access modifiers changed from: protected */
    /* renamed from: XV */
    public C3892wO mo21361XV() {
        switch (bFf().mo6893i(aYi)) {
            case 0:
                return null;
            case 2:
                return (C3892wO) bFf().mo5606d(new aCE(this, aYi, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, aYi, new Object[0]));
                break;
        }
        return m37535XU();
    }

    /* access modifiers changed from: protected */
    /* renamed from: XX */
    public C3892wO mo21362XX() {
        switch (bFf().mo6893i(aYj)) {
            case 0:
                return null;
            case 2:
                return (C3892wO) bFf().mo5606d(new aCE(this, aYj, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, aYj, new Object[0]));
                break;
        }
        return m37536XW();
    }

    /* renamed from: a */
    public Object mo14a(C0495Gr gr) {
        gr.getArgs();
        switch (gr.mo2417hq() - Amplifier._m_methodCount) {
            case 0:
                return m37532XO();
            case 1:
                m37526Kr();
                return null;
            case 2:
                m37527Kt();
                return null;
            case 3:
                return m37533XQ();
            case 4:
                return m37534XS();
            case 5:
                return m37535XU();
            case 6:
                return m37536XW();
            case 7:
                return m37528Kx();
            case 8:
                return m37529Kz();
            case 9:
                m37542qU();
                return null;
            default:
                return super.mo14a(gr);
        }
    }

    /* renamed from: a */
    public void mo15a(Collection collection, C0495Gr gr) {
        super.mo15a(collection, gr);
    }

    /* renamed from: c */
    public C5663aRz[] mo25c() {
        return _m_fields;
    }

    @C5566aOg
    @C2499fr
    /* renamed from: qV */
    public void mo1959qV() {
        switch (bFf().mo6893i(f8910Lm)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f8910Lm, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f8910Lm, new Object[0]));
                break;
        }
        m37542qU();
    }

    /* renamed from: e */
    public void mo1956e(AmplifierType iDVar) {
        super.mo1956e(iDVar);
    }

    @C0064Am(aul = "8ec7b053bb197b0a62ce01b4798264ab", aum = 0)
    /* renamed from: XO */
    private CruiseSpeedAdapter m37532XO() {
        return m37531XN();
    }

    @C0064Am(aul = "65f44f68fbcb5d05eb9ca10ec1a9f857", aum = 0)
    /* renamed from: XQ */
    private C3892wO m37533XQ() {
        return m37545yE();
    }

    @C0064Am(aul = "3e69dd263f6e1eec2e53ce38da238a1f", aum = 0)
    /* renamed from: XS */
    private C3892wO m37534XS() {
        return m37546yF();
    }

    @C0064Am(aul = "09a736155c9e2a6388a84990a4ea8262", aum = 0)
    /* renamed from: XU */
    private C3892wO m37535XU() {
        return m37547yG();
    }

    @C0064Am(aul = "4504e734bb14c45d20bcf4ba4073a288", aum = 0)
    /* renamed from: XW */
    private C3892wO m37536XW() {
        return m37548yH();
    }

    @C0064Am(aul = "a055fe3bf1e7e750ce8f1dc629ffcb4e", aum = 0)
    /* renamed from: Kx */
    private C3892wO m37528Kx() {
        return m37525Kp();
    }

    @C0064Am(aul = "152394cc340583d333e455198510fe70", aum = 0)
    /* renamed from: Kz */
    private C3892wO m37529Kz() {
        return m37524Kn();
    }

    @C6485anp
    @C5511aMd
    /* renamed from: a.qU$a */
    public class CruiseAmplifierAdapter extends CruiseSpeedAdapter implements C1616Xf {
        public static final int _m_fieldCount = 0;
        public static final C5663aRz[] _m_fields = null;
        public static final int _m_methodCount = 0;
        public static final C2491fm[] _m_methods = null;
        /* renamed from: aT */
        public static final C5663aRz f8921aT = null;
        public static final C2491fm aTe = null;
        public static final C2491fm aTf = null;
        public static final C2491fm aTg = null;
        public static final C2491fm aTh = null;
        public static final C2491fm bFx = null;
        public static final C2491fm bFy = null;
        public static final long serialVersionUID = 0;
        public static C6494any ___iScriptClass;
        @C0064Am(aul = "4d3d363b83b0afe816a0fa6214f97fa1", aum = 0)
        static /* synthetic */ CruiseSpeedAmplifier bFw;

        static {
            m37567V();
        }

        public CruiseAmplifierAdapter() {
            super((C5540aNg) null);
            super.mo10S();
        }

        public CruiseAmplifierAdapter(C5540aNg ang) {
            super(ang);
        }

        public CruiseAmplifierAdapter(CruiseSpeedAmplifier qUVar) {
            super((C5540aNg) null);
            super._m_script_init(qUVar);
        }

        /* renamed from: V */
        static void m37567V() {
            serialVersionUID = (long) 1;
            _m_fieldCount = CruiseSpeedAdapter._m_fieldCount + 1;
            _m_methodCount = CruiseSpeedAdapter._m_methodCount + 6;
            int i = CruiseSpeedAdapter._m_fieldCount;
            C5663aRz[] arzArr = new C5663aRz[(i + 1)];
            C5663aRz b = C5640aRc.m17844b(CruiseAmplifierAdapter.class, "4d3d363b83b0afe816a0fa6214f97fa1", i);
            f8921aT = b;
            arzArr[i] = b;
            int i2 = i + 1;
            _m_fields = arzArr;
            C1634Xv.m11725a((Object[]) CruiseSpeedAdapter._m_fields, (Object[]) _m_fields);
            int i3 = CruiseSpeedAdapter._m_methodCount;
            C2491fm[] fmVarArr = new C2491fm[(i3 + 6)];
            C2491fm a = C4105zY.m41624a(CruiseAmplifierAdapter.class, "491f54ef8ca06f4d28b2fb3d82702f71", i3);
            aTe = a;
            fmVarArr[i3] = a;
            int i4 = i3 + 1;
            C2491fm a2 = C4105zY.m41624a(CruiseAmplifierAdapter.class, "1bfb707c9ad7597d77c52a93f6545ba0", i4);
            aTf = a2;
            fmVarArr[i4] = a2;
            int i5 = i4 + 1;
            C2491fm a3 = C4105zY.m41624a(CruiseAmplifierAdapter.class, "bd28c9aa9add81f8eef2c1816931d568", i5);
            aTg = a3;
            fmVarArr[i5] = a3;
            int i6 = i5 + 1;
            C2491fm a4 = C4105zY.m41624a(CruiseAmplifierAdapter.class, "4f0524b7b0e8d1eea8986d6183512414", i6);
            aTh = a4;
            fmVarArr[i6] = a4;
            int i7 = i6 + 1;
            C2491fm a5 = C4105zY.m41624a(CruiseAmplifierAdapter.class, "bcf7a43955dde77b253772f8d76ae232", i7);
            bFx = a5;
            fmVarArr[i7] = a5;
            int i8 = i7 + 1;
            C2491fm a6 = C4105zY.m41624a(CruiseAmplifierAdapter.class, "f768e7bbdee00b09ec65e4fd77723b26", i8);
            bFy = a6;
            fmVarArr[i8] = a6;
            int i9 = i8 + 1;
            _m_methods = fmVarArr;
            C1634Xv.m11725a((Object[]) CruiseSpeedAdapter._m_methods, (Object[]) _m_methods);
            ___iScriptClass = aUO.m18566a(CruiseAmplifierAdapter.class, C1730ZZ.class, _m_fields, _m_methods);
        }

        /* renamed from: a */
        private void m37572a(CruiseSpeedAmplifier qUVar) {
            bFf().mo5608dq().mo3197f(f8921aT, qUVar);
        }

        private CruiseSpeedAmplifier anp() {
            return (CruiseSpeedAmplifier) bFf().mo5608dq().mo3214p(f8921aT);
        }

        /* renamed from: S */
        public void mo10S() {
            super.mo10S();
        }

        /* renamed from: T */
        public C6494any mo11T() {
            return ___iScriptClass;
        }

        /* renamed from: U */
        public C2491fm[] mo12U() {
            return _m_methods;
        }

        /* renamed from: VF */
        public float mo1996VF() {
            switch (bFf().mo6893i(aTe)) {
                case 0:
                    return 0.0f;
                case 2:
                    return ((Float) bFf().mo5606d(new aCE(this, aTe, new Object[0]))).floatValue();
                case 3:
                    bFf().mo5606d(new aCE(this, aTe, new Object[0]));
                    break;
            }
            return m37568VE();
        }

        /* renamed from: VH */
        public float mo1997VH() {
            switch (bFf().mo6893i(aTf)) {
                case 0:
                    return 0.0f;
                case 2:
                    return ((Float) bFf().mo5606d(new aCE(this, aTf, new Object[0]))).floatValue();
                case 3:
                    bFf().mo5606d(new aCE(this, aTf, new Object[0]));
                    break;
            }
            return m37569VG();
        }

        /* renamed from: W */
        public Object mo13W() {
            return new C1730ZZ(this);
        }

        /* renamed from: a */
        public Object mo14a(C0495Gr gr) {
            gr.getArgs();
            switch (gr.mo2417hq() - CruiseSpeedAdapter._m_methodCount) {
                case 0:
                    return new Float(m37568VE());
                case 1:
                    return new Float(m37569VG());
                case 2:
                    return new Float(m37570VI());
                case 3:
                    return new Float(m37571VJ());
                case 4:
                    return new Float(anq());
                case 5:
                    return new Float(ans());
                default:
                    return super.mo14a(gr);
            }
        }

        /* renamed from: a */
        public void mo15a(Collection collection, C0495Gr gr) {
            super.mo15a(collection, gr);
        }

        public float anr() {
            switch (bFf().mo6893i(bFx)) {
                case 0:
                    return 0.0f;
                case 2:
                    return ((Float) bFf().mo5606d(new aCE(this, bFx, new Object[0]))).floatValue();
                case 3:
                    bFf().mo5606d(new aCE(this, bFx, new Object[0]));
                    break;
            }
            return anq();
        }

        public float ant() {
            switch (bFf().mo6893i(bFy)) {
                case 0:
                    return 0.0f;
                case 2:
                    return ((Float) bFf().mo5606d(new aCE(this, bFy, new Object[0]))).floatValue();
                case 3:
                    bFf().mo5606d(new aCE(this, bFy, new Object[0]));
                    break;
            }
            return ans();
        }

        /* renamed from: c */
        public C5663aRz[] mo25c() {
            return _m_fields;
        }

        /* renamed from: ra */
        public float mo2001ra() {
            switch (bFf().mo6893i(aTh)) {
                case 0:
                    return 0.0f;
                case 2:
                    return ((Float) bFf().mo5606d(new aCE(this, aTh, new Object[0]))).floatValue();
                case 3:
                    bFf().mo5606d(new aCE(this, aTh, new Object[0]));
                    break;
            }
            return m37571VJ();
        }

        /* renamed from: rb */
        public float mo2002rb() {
            switch (bFf().mo6893i(aTg)) {
                case 0:
                    return 0.0f;
                case 2:
                    return ((Float) bFf().mo5606d(new aCE(this, aTg, new Object[0]))).floatValue();
                case 3:
                    bFf().mo5606d(new aCE(this, aTg, new Object[0]));
                    break;
            }
            return m37570VI();
        }

        /* renamed from: b */
        public void mo21363b(CruiseSpeedAmplifier qUVar) {
            m37572a(qUVar);
            super.mo10S();
        }

        @C0064Am(aul = "491f54ef8ca06f4d28b2fb3d82702f71", aum = 0)
        /* renamed from: VE */
        private float m37568VE() {
            return anp().mo21362XX().mo22753v(((CruiseSpeedAdapter) mo16070IE()).mo1996VF(), super.mo1996VF());
        }

        @C0064Am(aul = "1bfb707c9ad7597d77c52a93f6545ba0", aum = 0)
        /* renamed from: VG */
        private float m37569VG() {
            return anp().mo21361XV().mo22753v(((CruiseSpeedAdapter) mo16070IE()).mo1997VH(), super.mo1997VH());
        }

        @C0064Am(aul = "bd28c9aa9add81f8eef2c1816931d568", aum = 0)
        /* renamed from: VI */
        private float m37570VI() {
            return anp().mo21360XT().mo22753v(((CruiseSpeedAdapter) mo16070IE()).mo2002rb(), super.mo2002rb());
        }

        @C0064Am(aul = "4f0524b7b0e8d1eea8986d6183512414", aum = 0)
        /* renamed from: VJ */
        private float m37571VJ() {
            return anp().mo21359XR().mo22753v(((CruiseSpeedAdapter) mo16070IE()).mo2001ra(), super.mo2001ra());
        }

        @C0064Am(aul = "bcf7a43955dde77b253772f8d76ae232", aum = 0)
        private float anq() {
            return anp().mo21357Ky().mo22753v(((CruiseSpeedAdapter) mo16070IE()).anr(), super.anr());
        }

        @C0064Am(aul = "f768e7bbdee00b09ec65e4fd77723b26", aum = 0)
        private float ans() {
            return anp().mo21356KA().mo22753v(((CruiseSpeedAdapter) mo16070IE()).ant(), super.ant());
        }
    }
}
