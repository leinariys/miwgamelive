package game.script.item.buff.amplifier;

import game.network.message.externalizable.C0939Nn;
import game.network.message.externalizable.C6623aqX;
import game.network.message.externalizable.aCE;
import game.network.message.serializable.C3438ra;
import game.script.item.Amplifier;
import game.script.item.AmplifierType;
import game.script.item.Component;
import game.script.nls.NLSAmplifier;
import game.script.nls.NLSManager;
import game.script.player.Player;
import logic.aaa.C1506WA;
import logic.baa.C1616Xf;
import logic.baa.C1634Xv;
import logic.baa.C5511aMd;
import logic.baa.C6485anp;
import logic.data.mbean.C6048afU;
import logic.res.code.C5640aRc;
import logic.res.code.C5663aRz;
import logic.res.code.C6494any;
import logic.res.html.C2491fm;
import p001a.*;

import java.util.Collection;

@C5829abJ("2.0.0")
@C6485anp
@C2712iu(mo19786Bt = AmplifierType.class, version = "2.0.1")
@C5511aMd
/* renamed from: a.Ee */
/* compiled from: a */
public class ComposedAmplifier extends Amplifier implements C1616Xf {

    /* renamed from: Lm */
    public static final C2491fm f505Lm = null;
    /* renamed from: Pa */
    public static final C5663aRz f507Pa = null;
    public static final C2491fm _f_dispose_0020_0028_0029V = null;
    public static final int _m_fieldCount = 0;
    public static final C5663aRz[] _m_fields = null;
    public static final int _m_methodCount = 0;
    public static final C2491fm[] _m_methods = null;
    public static final C2491fm atW = null;
    public static final C2491fm atX = null;
    public static final C5663aRz cRq = null;
    public static final C2491fm cRr = null;
    public static final C2491fm cRs = null;
    public static final C2491fm ccN = null;
    public static final C2491fm ccO = null;

    public static C6494any ___iScriptClass = null;
    @C0064Am(aul = "c30efa658639badfda6157e00d2c396f", aum = 1)

    /* renamed from: OZ */
    private static C3438ra<Amplifier> f506OZ = null;
    @C0064Am(aul = "822a21d4a6750bcba8128595b4d62d11", aum = 0)
    private static C3438ra<AmplifierType> cRp = null;

    static {
        m2999V();
    }

    public ComposedAmplifier() {
        super((C5540aNg) null);
        super.mo10S();
    }

    public ComposedAmplifier(C5540aNg ang) {
        super(ang);
    }

    public ComposedAmplifier(AmplifierType iDVar) {
        super((C5540aNg) null);
        super.mo1956e(iDVar);
    }

    /* renamed from: V */
    static void m2999V() {
        _m_fieldCount = Amplifier._m_fieldCount + 2;
        _m_methodCount = Amplifier._m_methodCount + 8;
        int i = Amplifier._m_fieldCount;
        C5663aRz[] arzArr = new C5663aRz[(i + 2)];
        C5663aRz b = C5640aRc.m17844b(ComposedAmplifier.class, "822a21d4a6750bcba8128595b4d62d11", i);
        cRq = b;
        arzArr[i] = b;
        int i2 = i + 1;
        C5663aRz b2 = C5640aRc.m17844b(ComposedAmplifier.class, "c30efa658639badfda6157e00d2c396f", i2);
        f507Pa = b2;
        arzArr[i2] = b2;
        int i3 = i2 + 1;
        _m_fields = arzArr;
        C1634Xv.m11725a((Object[]) Amplifier._m_fields, (Object[]) _m_fields);
        int i4 = Amplifier._m_methodCount;
        C2491fm[] fmVarArr = new C2491fm[(i4 + 8)];
        C2491fm a = C4105zY.m41624a(ComposedAmplifier.class, "1bab93ee45c778b860cb50deecece9bb", i4);
        atW = a;
        fmVarArr[i4] = a;
        int i5 = i4 + 1;
        C2491fm a2 = C4105zY.m41624a(ComposedAmplifier.class, "6304b0b21961457ec199e5d82858d9bc", i5);
        atX = a2;
        fmVarArr[i5] = a2;
        int i6 = i5 + 1;
        C2491fm a3 = C4105zY.m41624a(ComposedAmplifier.class, "77b2a00a182a18b736ef165093d31221", i6);
        ccN = a3;
        fmVarArr[i6] = a3;
        int i7 = i6 + 1;
        C2491fm a4 = C4105zY.m41624a(ComposedAmplifier.class, "02e82842c321f3a0ca2ea10f8ed21fe8", i7);
        ccO = a4;
        fmVarArr[i7] = a4;
        int i8 = i7 + 1;
        C2491fm a5 = C4105zY.m41624a(ComposedAmplifier.class, "44f081d00a66ada4deaae888bb65f75b", i8);
        cRr = a5;
        fmVarArr[i8] = a5;
        int i9 = i8 + 1;
        C2491fm a6 = C4105zY.m41624a(ComposedAmplifier.class, "79dcaa43f9588d026e61548629c8757b", i9);
        cRs = a6;
        fmVarArr[i9] = a6;
        int i10 = i9 + 1;
        C2491fm a7 = C4105zY.m41624a(ComposedAmplifier.class, "c266551a40a23f672c049b4a222a945f", i10);
        _f_dispose_0020_0028_0029V = a7;
        fmVarArr[i10] = a7;
        int i11 = i10 + 1;
        C2491fm a8 = C4105zY.m41624a(ComposedAmplifier.class, "15b64c6080cbc53d9535e43ddd2e890b", i11);
        f505Lm = a8;
        fmVarArr[i11] = a8;
        int i12 = i11 + 1;
        _m_methods = fmVarArr;
        C1634Xv.m11725a((Object[]) Amplifier._m_methods, (Object[]) _m_methods);
        ___iScriptClass = aUO.m18566a(ComposedAmplifier.class, C6048afU.class, _m_fields, _m_methods);
    }

    @C0064Am(aul = "1bab93ee45c778b860cb50deecece9bb", aum = 0)
    @C5566aOg
    /* renamed from: Kr */
    private void m2997Kr() {
        throw new aWi(new aCE(this, atW, new Object[0]));
    }

    @C0064Am(aul = "6304b0b21961457ec199e5d82858d9bc", aum = 0)
    @C5566aOg
    /* renamed from: Kt */
    private void m2998Kt() {
        throw new aWi(new aCE(this, atX, new Object[0]));
    }

    private C3438ra aNL() {
        return ((ComposedAmplifierType) getType()).dfJ();
    }

    /* renamed from: aY */
    private void m3000aY(C3438ra raVar) {
        throw new C6039afL();
    }

    @C0064Am(aul = "77b2a00a182a18b736ef165093d31221", aum = 0)
    @C5566aOg
    /* renamed from: m */
    private void m3002m(Component abl) {
        throw new aWi(new aCE(this, ccN, new Object[]{abl}));
    }

    @C0064Am(aul = "02e82842c321f3a0ca2ea10f8ed21fe8", aum = 0)
    @C5566aOg
    /* renamed from: o */
    private void m3003o(Component abl) {
        throw new aWi(new aCE(this, ccO, new Object[]{abl}));
    }

    @C0064Am(aul = "15b64c6080cbc53d9535e43ddd2e890b", aum = 0)
    @C5566aOg
    @C2499fr
    /* renamed from: qU */
    private void m3004qU() {
        throw new aWi(new aCE(this, f505Lm, new Object[0]));
    }

    /* renamed from: s */
    private void m3005s(C3438ra raVar) {
        bFf().mo5608dq().mo3197f(f507Pa, raVar);
    }

    /* renamed from: uR */
    private C3438ra m3006uR() {
        return (C3438ra) bFf().mo5608dq().mo3214p(f507Pa);
    }

    @C5566aOg
    /* renamed from: Ks */
    public void mo1952Ks() {
        switch (bFf().mo6893i(atW)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, atW, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, atW, new Object[0]));
                break;
        }
        m2997Kr();
    }

    @C5566aOg
    /* renamed from: Ku */
    public void mo1953Ku() {
        switch (bFf().mo6893i(atX)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, atX, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, atX, new Object[0]));
                break;
        }
        m2998Kt();
    }

    /* renamed from: S */
    public void mo10S() {
        super.mo10S();
    }

    /* renamed from: T */
    public C6494any mo11T() {
        return ___iScriptClass;
    }

    /* renamed from: U */
    public C2491fm[] mo12U() {
        return _m_methods;
    }

    /* renamed from: W */
    public Object mo13W() {
        return new C6048afU(this);
    }

    /* renamed from: a */
    public Object mo14a(C0495Gr gr) {
        Object[] args = gr.getArgs();
        switch (gr.mo2417hq() - Amplifier._m_methodCount) {
            case 0:
                m2997Kr();
                return null;
            case 1:
                m2998Kt();
                return null;
            case 2:
                m3002m((Component) args[0]);
                return null;
            case 3:
                m3003o((Component) args[0]);
                return null;
            case 4:
                return new Boolean(aNM());
            case 5:
                return new Boolean(aNO());
            case 6:
                m3001fg();
                return null;
            case 7:
                m3004qU();
                return null;
            default:
                return super.mo14a(gr);
        }
    }

    /* renamed from: a */
    public void mo15a(Collection collection, C0495Gr gr) {
        super.mo15a(collection, gr);
    }

    public boolean aNN() {
        switch (bFf().mo6893i(cRr)) {
            case 0:
                return false;
            case 2:
                return ((Boolean) bFf().mo5606d(new aCE(this, cRr, new Object[0]))).booleanValue();
            case 3:
                bFf().mo5606d(new aCE(this, cRr, new Object[0]));
                break;
        }
        return aNM();
    }

    public boolean aNP() {
        switch (bFf().mo6893i(cRs)) {
            case 0:
                return false;
            case 2:
                return ((Boolean) bFf().mo5606d(new aCE(this, cRs, new Object[0]))).booleanValue();
            case 3:
                bFf().mo5606d(new aCE(this, cRs, new Object[0]));
                break;
        }
        return aNO();
    }

    /* renamed from: c */
    public C5663aRz[] mo25c() {
        return _m_fields;
    }

    public void dispose() {
        switch (bFf().mo6893i(_f_dispose_0020_0028_0029V)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, _f_dispose_0020_0028_0029V, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, _f_dispose_0020_0028_0029V, new Object[0]));
                break;
        }
        m3001fg();
    }

    @C5566aOg
    /* renamed from: n */
    public void mo1957n(Component abl) {
        switch (bFf().mo6893i(ccN)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, ccN, new Object[]{abl}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, ccN, new Object[]{abl}));
                break;
        }
        m3002m(abl);
    }

    @C5566aOg
    /* renamed from: p */
    public void mo1958p(Component abl) {
        switch (bFf().mo6893i(ccO)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, ccO, new Object[]{abl}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, ccO, new Object[]{abl}));
                break;
        }
        m3003o(abl);
    }

    @C5566aOg
    @C2499fr
    /* renamed from: qV */
    public void mo1959qV() {
        switch (bFf().mo6893i(f505Lm)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f505Lm, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f505Lm, new Object[0]));
                break;
        }
        m3004qU();
    }

    /* renamed from: e */
    public void mo1956e(AmplifierType iDVar) {
        super.mo1956e(iDVar);
    }

    @C0064Am(aul = "44f081d00a66ada4deaae888bb65f75b", aum = 0)
    private boolean aNM() {
        for (Amplifier aNN : m3006uR()) {
            if (!aNN.aNN()) {
                return false;
            }
        }
        return true;
    }

    @C0064Am(aul = "79dcaa43f9588d026e61548629c8757b", aum = 0)
    private boolean aNO() {
        if (!super.aNP()) {
            return false;
        }
        return aNN();
    }

    @C0064Am(aul = "c266551a40a23f672c049b4a222a945f", aum = 0)
    /* renamed from: fg */
    private void m3001fg() {
        for (Amplifier zXVar : m3006uR()) {
            if (!asY()) {
                if (mo7855al() != null && mo7855al().agj() != null && (mo7855al().agj() instanceof Player)) {
                    ((Player) mo7855al().agj()).mo14419f((C1506WA) new C6623aqX(C0939Nn.C0940a.ERROR, ((NLSAmplifier) ala().aIY().mo6310c(NLSManager.C1472a.AMPLIFIER)).aDh(), new Object[0]));
                    return;
                }
                return;
            } else if (!zXVar.asY()) {
                if (mo7855al() != null && mo7855al().agj() != null && (mo7855al().agj() instanceof Player)) {
                    ((Player) mo7855al().agj()).mo14419f((C1506WA) new C6623aqX(C0939Nn.C0940a.ERROR, ((NLSAmplifier) ala().aIY().mo6310c(NLSManager.C1472a.AMPLIFIER)).aDh(), new Object[0]));
                    return;
                }
                return;
            }
        }
        super.dispose();
    }
}
