package game.script.item.buff.amplifier;

import game.network.message.externalizable.aCE;
import game.network.message.serializable.C1556Wo;
import game.script.damage.DamageType;
import game.script.item.AmplifierType;
import logic.baa.*;
import logic.data.mbean.aIK;
import logic.res.code.C5640aRc;
import logic.res.code.C5663aRz;
import logic.res.code.C6494any;
import logic.res.html.C2491fm;
import p001a.*;

import java.util.Collection;
import java.util.Map;

@C0566Hp
@C5511aMd
@C6485anp
/* renamed from: a.aas  reason: case insensitive filesystem */
/* compiled from: a */
public class StrikeAmplifierType extends BaseCriticalStrikeAmplifier implements C1616Xf {
    public static final int _m_fieldCount = 0;
    public static final C5663aRz[] _m_fields = null;
    public static final int _m_methodCount = 0;
    public static final C2491fm[] _m_methods = null;
    public static final C2491fm aQB = null;
    public static final C5663aRz atP = null;
    public static final C5663aRz atQ = null;
    public static final C5663aRz atS = null;
    public static final C5663aRz atT = null;
    /* renamed from: dN */
    public static final C2491fm f4119dN = null;
    public static final C2491fm eUN = null;
    public static final C2491fm eUO = null;
    public static final C2491fm eUP = null;
    public static final C2491fm eUQ = null;
    public static final C2491fm eUR = null;
    public static final C2491fm eUS = null;
    public static final C2491fm eUT = null;
    public static final C2491fm eUU = null;
    public static final long serialVersionUID = 0;
    public static C6494any ___iScriptClass;
    @C0064Am(aul = "6e63cd56fdf5168c0118f05978aae1ce", aum = 1)

    /* renamed from: Yu */
    private static C3892wO f4117Yu;
    @C0064Am(aul = "8e7c43dbdf57e2c4276386dd3e0ef4a3", aum = 3)

    /* renamed from: Yv */
    private static C3892wO f4118Yv;
    @C0064Am(aul = "f87735b8d6c33c9425ec6c0178570e27", aum = 0)
    private static C1556Wo<DamageType, C3892wO> atO;
    @C0064Am(aul = "67a7ea8d2e72b5691c28a2ab26d50292", aum = 2)
    private static C3892wO atR;

    static {
        m19813V();
    }

    public StrikeAmplifierType() {
        super((C5540aNg) null);
        super.mo10S();
    }

    public StrikeAmplifierType(C5540aNg ang) {
        super(ang);
    }

    /* renamed from: V */
    static void m19813V() {
        serialVersionUID = (long) 1;
        _m_fieldCount = BaseCriticalStrikeAmplifier._m_fieldCount + 4;
        _m_methodCount = BaseCriticalStrikeAmplifier._m_methodCount + 10;
        int i = BaseCriticalStrikeAmplifier._m_fieldCount;
        C5663aRz[] arzArr = new C5663aRz[(i + 4)];
        C5663aRz b = C5640aRc.m17844b(StrikeAmplifierType.class, "f87735b8d6c33c9425ec6c0178570e27", i);
        atP = b;
        arzArr[i] = b;
        int i2 = i + 1;
        C5663aRz b2 = C5640aRc.m17844b(StrikeAmplifierType.class, "6e63cd56fdf5168c0118f05978aae1ce", i2);
        atQ = b2;
        arzArr[i2] = b2;
        int i3 = i2 + 1;
        C5663aRz b3 = C5640aRc.m17844b(StrikeAmplifierType.class, "67a7ea8d2e72b5691c28a2ab26d50292", i3);
        atS = b3;
        arzArr[i3] = b3;
        int i4 = i3 + 1;
        C5663aRz b4 = C5640aRc.m17844b(StrikeAmplifierType.class, "8e7c43dbdf57e2c4276386dd3e0ef4a3", i4);
        atT = b4;
        arzArr[i4] = b4;
        int i5 = i4 + 1;
        _m_fields = arzArr;
        C1634Xv.m11725a((Object[]) BaseCriticalStrikeAmplifier._m_fields, (Object[]) _m_fields);
        int i6 = BaseCriticalStrikeAmplifier._m_methodCount;
        C2491fm[] fmVarArr = new C2491fm[(i6 + 10)];
        C2491fm a = C4105zY.m41624a(StrikeAmplifierType.class, "104cfa18c950cc9dafa8bfadd04b6e6a", i6);
        aQB = a;
        fmVarArr[i6] = a;
        int i7 = i6 + 1;
        C2491fm a2 = C4105zY.m41624a(StrikeAmplifierType.class, "bcc2f6b071a7ed063ca286a49a6ce128", i7);
        eUN = a2;
        fmVarArr[i7] = a2;
        int i8 = i7 + 1;
        C2491fm a3 = C4105zY.m41624a(StrikeAmplifierType.class, "dfc0d66658422f8dc25d405d018f2d05", i8);
        eUO = a3;
        fmVarArr[i8] = a3;
        int i9 = i8 + 1;
        C2491fm a4 = C4105zY.m41624a(StrikeAmplifierType.class, "6a0e456cccdbb50596786c90d93cbf90", i9);
        eUP = a4;
        fmVarArr[i9] = a4;
        int i10 = i9 + 1;
        C2491fm a5 = C4105zY.m41624a(StrikeAmplifierType.class, "2b7792fdb47c541297d530361e11816c", i10);
        eUQ = a5;
        fmVarArr[i10] = a5;
        int i11 = i10 + 1;
        C2491fm a6 = C4105zY.m41624a(StrikeAmplifierType.class, "31058e65ae893ca5e93335c206e0c8e1", i11);
        eUR = a6;
        fmVarArr[i11] = a6;
        int i12 = i11 + 1;
        C2491fm a7 = C4105zY.m41624a(StrikeAmplifierType.class, "da3d68172f00440b264804dcf86909b1", i12);
        eUS = a7;
        fmVarArr[i12] = a7;
        int i13 = i12 + 1;
        C2491fm a8 = C4105zY.m41624a(StrikeAmplifierType.class, "417c0437cddc0b39f055db224506c409", i13);
        eUT = a8;
        fmVarArr[i13] = a8;
        int i14 = i13 + 1;
        C2491fm a9 = C4105zY.m41624a(StrikeAmplifierType.class, "b36384d1718a1342b473f06f1b3e485e", i14);
        eUU = a9;
        fmVarArr[i14] = a9;
        int i15 = i14 + 1;
        C2491fm a10 = C4105zY.m41624a(StrikeAmplifierType.class, "59497a8586331cfdb13516a235bbdc37", i15);
        f4119dN = a10;
        fmVarArr[i15] = a10;
        int i16 = i15 + 1;
        _m_methods = fmVarArr;
        C1634Xv.m11725a((Object[]) BaseCriticalStrikeAmplifier._m_methods, (Object[]) _m_methods);
        ___iScriptClass = aUO.m18566a(StrikeAmplifierType.class, aIK.class, _m_fields, _m_methods);
    }

    /* renamed from: Km */
    private C1556Wo m19808Km() {
        return (C1556Wo) bFf().mo5608dq().mo3214p(atP);
    }

    /* renamed from: Kn */
    private C3892wO m19809Kn() {
        return (C3892wO) bFf().mo5608dq().mo3214p(atQ);
    }

    /* renamed from: Ko */
    private C3892wO m19810Ko() {
        return (C3892wO) bFf().mo5608dq().mo3214p(atS);
    }

    /* renamed from: Kp */
    private C3892wO m19811Kp() {
        return (C3892wO) bFf().mo5608dq().mo3214p(atT);
    }

    /* renamed from: i */
    private void m19818i(C1556Wo wo) {
        bFf().mo5608dq().mo3197f(atP, wo);
    }

    /* renamed from: s */
    private void m19819s(C3892wO wOVar) {
        bFf().mo5608dq().mo3197f(atQ, wOVar);
    }

    /* renamed from: t */
    private void m19820t(C3892wO wOVar) {
        bFf().mo5608dq().mo3197f(atS, wOVar);
    }

    /* renamed from: u */
    private void m19821u(C3892wO wOVar) {
        bFf().mo5608dq().mo3197f(atT, wOVar);
    }

    /* renamed from: T */
    public C6494any mo11T() {
        return ___iScriptClass;
    }

    /* renamed from: U */
    public C2491fm[] mo12U() {
        return _m_methods;
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Damages")
    /* renamed from: UD */
    public C1556Wo<DamageType, C3892wO> mo12332UD() {
        switch (bFf().mo6893i(aQB)) {
            case 0:
                return null;
            case 2:
                return (C1556Wo) bFf().mo5606d(new aCE(this, aQB, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, aQB, new Object[0]));
                break;
        }
        return m19812UC();
    }

    /* renamed from: W */
    public Object mo13W() {
        return new aIK(this);
    }

    /* renamed from: a */
    public Object mo14a(C0495Gr gr) {
        Object[] args = gr.getArgs();
        switch (gr.mo2417hq() - BaseCriticalStrikeAmplifier._m_methodCount) {
            case 0:
                return m19812UC();
            case 1:
                m19822u((Map<DamageType, C3892wO>) (Map) args[0]);
                return null;
            case 2:
                return bLy();
            case 3:
                m19815aa((C3892wO) args[0]);
                return null;
            case 4:
                return bLA();
            case 5:
                m19816ac((C3892wO) args[0]);
                return null;
            case 6:
                return bLC();
            case 7:
                m19817ae((C3892wO) args[0]);
                return null;
            case 8:
                return bLE();
            case 9:
                return m19814aT();
            default:
                return super.mo14a(gr);
        }
    }

    /* renamed from: a */
    public void mo15a(Collection collection, C0495Gr gr) {
        super.mo15a(collection, gr);
    }

    /* access modifiers changed from: protected */
    /* renamed from: aU */
    public <T extends aDJ> T mo745aU() {
        switch (bFf().mo6893i(f4119dN)) {
            case 0:
                return null;
            case 2:
                return (aDJ) bFf().mo5606d(new aCE(this, f4119dN, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f4119dN, new Object[0]));
                break;
        }
        return m19814aT();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Cooldown")
    /* renamed from: ab */
    public void mo12333ab(C3892wO wOVar) {
        switch (bFf().mo6893i(eUP)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, eUP, new Object[]{wOVar}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, eUP, new Object[]{wOVar}));
                break;
        }
        m19815aa(wOVar);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Duration")
    /* renamed from: ad */
    public void mo12334ad(C3892wO wOVar) {
        switch (bFf().mo6893i(eUR)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, eUR, new Object[]{wOVar}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, eUR, new Object[]{wOVar}));
                break;
        }
        m19816ac(wOVar);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Warmup")
    /* renamed from: af */
    public void mo12335af(C3892wO wOVar) {
        switch (bFf().mo6893i(eUT)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, eUT, new Object[]{wOVar}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, eUT, new Object[]{wOVar}));
                break;
        }
        m19817ae(wOVar);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Duration")
    public C3892wO bLB() {
        switch (bFf().mo6893i(eUQ)) {
            case 0:
                return null;
            case 2:
                return (C3892wO) bFf().mo5606d(new aCE(this, eUQ, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, eUQ, new Object[0]));
                break;
        }
        return bLA();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Warmup")
    public C3892wO bLD() {
        switch (bFf().mo6893i(eUS)) {
            case 0:
                return null;
            case 2:
                return (C3892wO) bFf().mo5606d(new aCE(this, eUS, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, eUS, new Object[0]));
                break;
        }
        return bLC();
    }

    public StrikeAmplifier bLF() {
        switch (bFf().mo6893i(eUU)) {
            case 0:
                return null;
            case 2:
                return (StrikeAmplifier) bFf().mo5606d(new aCE(this, eUU, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, eUU, new Object[0]));
                break;
        }
        return bLE();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Cooldown")
    public C3892wO bLz() {
        switch (bFf().mo6893i(eUO)) {
            case 0:
                return null;
            case 2:
                return (C3892wO) bFf().mo5606d(new aCE(this, eUO, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, eUO, new Object[0]));
                break;
        }
        return bLy();
    }

    /* renamed from: c */
    public C5663aRz[] mo25c() {
        return _m_fields;
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Damages")
    /* renamed from: v */
    public void mo12340v(Map<DamageType, C3892wO> map) {
        switch (bFf().mo6893i(eUN)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, eUN, new Object[]{map}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, eUN, new Object[]{map}));
                break;
        }
        m19822u(map);
    }

    /* renamed from: S */
    public void mo10S() {
        super.mo10S();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Damages")
    @C0064Am(aul = "104cfa18c950cc9dafa8bfadd04b6e6a", aum = 0)
    /* renamed from: UC */
    private C1556Wo<DamageType, C3892wO> m19812UC() {
        return m19808Km();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Damages")
    @C0064Am(aul = "bcc2f6b071a7ed063ca286a49a6ce128", aum = 0)
    /* renamed from: u */
    private void m19822u(Map<DamageType, C3892wO> map) {
        m19808Km().clear();
        m19808Km().putAll(map);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Cooldown")
    @C0064Am(aul = "dfc0d66658422f8dc25d405d018f2d05", aum = 0)
    private C3892wO bLy() {
        return m19809Kn();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Cooldown")
    @C0064Am(aul = "6a0e456cccdbb50596786c90d93cbf90", aum = 0)
    /* renamed from: aa */
    private void m19815aa(C3892wO wOVar) {
        m19819s(wOVar);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Duration")
    @C0064Am(aul = "2b7792fdb47c541297d530361e11816c", aum = 0)
    private C3892wO bLA() {
        return m19810Ko();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Duration")
    @C0064Am(aul = "31058e65ae893ca5e93335c206e0c8e1", aum = 0)
    /* renamed from: ac */
    private void m19816ac(C3892wO wOVar) {
        m19820t(wOVar);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Warmup")
    @C0064Am(aul = "da3d68172f00440b264804dcf86909b1", aum = 0)
    private C3892wO bLC() {
        return m19811Kp();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Warmup")
    @C0064Am(aul = "417c0437cddc0b39f055db224506c409", aum = 0)
    /* renamed from: ae */
    private void m19817ae(C3892wO wOVar) {
        m19821u(wOVar);
    }

    @C0064Am(aul = "b36384d1718a1342b473f06f1b3e485e", aum = 0)
    private StrikeAmplifier bLE() {
        return (StrikeAmplifier) mo745aU();
    }

    @C0064Am(aul = "59497a8586331cfdb13516a235bbdc37", aum = 0)
    /* renamed from: aT */
    private <T extends aDJ> T m19814aT() {
        T t = (StrikeAmplifier) bFf().mo6865M(StrikeAmplifier.class);
        t.mo1956e((AmplifierType) this);
        return t;
    }
}
