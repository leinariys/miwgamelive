package game.script.item.buff.amplifier;

import game.network.message.externalizable.aCE;
import game.network.message.serializable.C1556Wo;
import game.network.message.serializable.C2686iZ;
import game.script.damage.DamageType;
import game.script.item.*;
import logic.baa.C1616Xf;
import logic.baa.C1634Xv;
import logic.baa.C5511aMd;
import logic.baa.C6485anp;
import logic.data.mbean.C0303Dw;
import logic.data.mbean.aUI;
import logic.res.code.C5640aRc;
import logic.res.code.C5663aRz;
import logic.res.code.C6494any;
import logic.res.html.C2491fm;
import p001a.*;

import java.util.Collection;

@C5829abJ("2.0.0")
@C6485anp
@C2712iu(mo19786Bt = AmplifierType.class, version = "2.0.1")
@C5511aMd
/* renamed from: a.eH */
/* compiled from: a */
public class WeaponAmplifier extends Amplifier implements C1616Xf {

    /* renamed from: DK */
    public static final C2491fm f6720DK = null;

    /* renamed from: Lm */
    public static final C2491fm f6721Lm = null;

    /* renamed from: OY */
    public static final C5663aRz f6722OY = null;
    public static final int _m_fieldCount = 0;
    public static final C5663aRz[] _m_fields = null;
    public static final int _m_methodCount = 0;
    public static final C2491fm[] _m_methods = null;
    public static final C2491fm atW = null;
    public static final C2491fm atX = null;
    public static final C5663aRz bEc = null;
    public static final C5663aRz bEg = null;
    public static final C5663aRz bEi = null;
    public static final C5663aRz bEk = null;
    public static final C2491fm bEp = null;
    public static final C2491fm bEq = null;
    public static final C2491fm bEr = null;
    public static final C2491fm bEs = null;
    public static final C2491fm bEt = null;
    public static final C5663aRz bjM = null;
    public static final C2491fm bkh = null;
    public static final C5663aRz cHK = null;
    public static final C2491fm ccN = null;
    public static final C2491fm ccO = null;
    public static final long serialVersionUID = 0;
    /* renamed from: uN */
    public static final C5663aRz f6723uN = null;
    /* renamed from: uS */
    public static final C5663aRz f6724uS = null;
    public static C6494any ___iScriptClass;
    @C0064Am(aul = "6fa74346f871afe91fa21d1fb12b1641", aum = 0)
    private static C3892wO bEb;
    @C0064Am(aul = "97a17f04fc3cb2502ce613a39f1668b6", aum = 1)
    private static C3892wO bEd;
    @C0064Am(aul = "00de76aea9f75f17cb4417b6d5421ab6", aum = 2)
    private static C3892wO bEe;
    @C0064Am(aul = "c10ad9140131e5d47da2f29232156338", aum = 3)
    private static C3892wO bEf;
    @C0064Am(aul = "69fc5a20ea75797cb33505645f64a94d", aum = 4)
    private static C3892wO bEh;
    @C0064Am(aul = "ece4507578e61f6323470f9fb36ab358", aum = 5)
    private static boolean bEj;
    @C0064Am(aul = "7ccb93e3eeb184e2d3ccec7b632480f0", aum = 8)
    private static C1556Wo<Weapon, WeaponAmplifierAdapter> bjL;
    @C0064Am(aul = "fe0d5dcd35e6a9685146c9eb6c25a3de", aum = 6)
    private static C2686iZ<ItemTypeCategory> ckK;
    @C0064Am(aul = "7ca0a6c58d9bf5bdc6816f97ea9bcb10", aum = 7)
    private static C2686iZ<WeaponType> ckL;

    static {
        m29472V();
    }

    public WeaponAmplifier() {
        super((C5540aNg) null);
        super.mo10S();
    }

    public WeaponAmplifier(C5540aNg ang) {
        super(ang);
    }

    public WeaponAmplifier(AmplifierType iDVar) {
        super((C5540aNg) null);
        super.mo1956e(iDVar);
    }

    /* renamed from: V */
    static void m29472V() {
        serialVersionUID = (long) 1;
        _m_fieldCount = Amplifier._m_fieldCount + 9;
        _m_methodCount = Amplifier._m_methodCount + 12;
        int i = Amplifier._m_fieldCount;
        C5663aRz[] arzArr = new C5663aRz[(i + 9)];
        C5663aRz b = C5640aRc.m17844b(WeaponAmplifier.class, "6fa74346f871afe91fa21d1fb12b1641", i);
        bEc = b;
        arzArr[i] = b;
        int i2 = i + 1;
        C5663aRz b2 = C5640aRc.m17844b(WeaponAmplifier.class, "97a17f04fc3cb2502ce613a39f1668b6", i2);
        f6724uS = b2;
        arzArr[i2] = b2;
        int i3 = i2 + 1;
        C5663aRz b3 = C5640aRc.m17844b(WeaponAmplifier.class, "00de76aea9f75f17cb4417b6d5421ab6", i3);
        f6723uN = b3;
        arzArr[i3] = b3;
        int i4 = i3 + 1;
        C5663aRz b4 = C5640aRc.m17844b(WeaponAmplifier.class, "c10ad9140131e5d47da2f29232156338", i4);
        bEg = b4;
        arzArr[i4] = b4;
        int i5 = i4 + 1;
        C5663aRz b5 = C5640aRc.m17844b(WeaponAmplifier.class, "69fc5a20ea75797cb33505645f64a94d", i5);
        bEi = b5;
        arzArr[i5] = b5;
        int i6 = i5 + 1;
        C5663aRz b6 = C5640aRc.m17844b(WeaponAmplifier.class, "ece4507578e61f6323470f9fb36ab358", i6);
        bEk = b6;
        arzArr[i6] = b6;
        int i7 = i6 + 1;
        C5663aRz b7 = C5640aRc.m17844b(WeaponAmplifier.class, "fe0d5dcd35e6a9685146c9eb6c25a3de", i7);
        cHK = b7;
        arzArr[i7] = b7;
        int i8 = i7 + 1;
        C5663aRz b8 = C5640aRc.m17844b(WeaponAmplifier.class, "7ca0a6c58d9bf5bdc6816f97ea9bcb10", i8);
        f6722OY = b8;
        arzArr[i8] = b8;
        int i9 = i8 + 1;
        C5663aRz b9 = C5640aRc.m17844b(WeaponAmplifier.class, "7ccb93e3eeb184e2d3ccec7b632480f0", i9);
        bjM = b9;
        arzArr[i9] = b9;
        int i10 = i9 + 1;
        _m_fields = arzArr;
        C1634Xv.m11725a((Object[]) Amplifier._m_fields, (Object[]) _m_fields);
        int i11 = Amplifier._m_methodCount;
        C2491fm[] fmVarArr = new C2491fm[(i11 + 12)];
        C2491fm a = C4105zY.m41624a(WeaponAmplifier.class, "8fbbb7ba0b86d66303c7fbf3e0ed1097", i11);
        atW = a;
        fmVarArr[i11] = a;
        int i12 = i11 + 1;
        C2491fm a2 = C4105zY.m41624a(WeaponAmplifier.class, "b8e9f1c13cd0c68da261b41f79d997e3", i12);
        bkh = a2;
        fmVarArr[i12] = a2;
        int i13 = i12 + 1;
        C2491fm a3 = C4105zY.m41624a(WeaponAmplifier.class, "59f674da3c1af1317d2dc8a20636ad09", i13);
        atX = a3;
        fmVarArr[i13] = a3;
        int i14 = i13 + 1;
        C2491fm a4 = C4105zY.m41624a(WeaponAmplifier.class, "561fae7b8c819dfef00dab0edd4d265d", i14);
        ccN = a4;
        fmVarArr[i14] = a4;
        int i15 = i14 + 1;
        C2491fm a5 = C4105zY.m41624a(WeaponAmplifier.class, "4b43a6dee77502bb1cc531796bb06de6", i15);
        ccO = a5;
        fmVarArr[i15] = a5;
        int i16 = i15 + 1;
        C2491fm a6 = C4105zY.m41624a(WeaponAmplifier.class, "1285804ce422899bbe3e39b89c112b20", i16);
        bEp = a6;
        fmVarArr[i16] = a6;
        int i17 = i16 + 1;
        C2491fm a7 = C4105zY.m41624a(WeaponAmplifier.class, "7b652d82e5a8c62340c8f906df3a5430", i17);
        bEq = a7;
        fmVarArr[i17] = a7;
        int i18 = i17 + 1;
        C2491fm a8 = C4105zY.m41624a(WeaponAmplifier.class, "a03e0668cfc282ec866fcdab425580b3", i18);
        bEr = a8;
        fmVarArr[i18] = a8;
        int i19 = i18 + 1;
        C2491fm a9 = C4105zY.m41624a(WeaponAmplifier.class, "d81e5df3d3e3218b9cd45edc34f28a11", i19);
        bEs = a9;
        fmVarArr[i19] = a9;
        int i20 = i19 + 1;
        C2491fm a10 = C4105zY.m41624a(WeaponAmplifier.class, "c944c5bc74d07f7ea7f3d34704682de2", i20);
        bEt = a10;
        fmVarArr[i20] = a10;
        int i21 = i20 + 1;
        C2491fm a11 = C4105zY.m41624a(WeaponAmplifier.class, "d5942523196d205115e909bbb9208f05", i21);
        f6720DK = a11;
        fmVarArr[i21] = a11;
        int i22 = i21 + 1;
        C2491fm a12 = C4105zY.m41624a(WeaponAmplifier.class, "4e14412cb4dcad72a59a1247bd3fdcfe", i22);
        f6721Lm = a12;
        fmVarArr[i22] = a12;
        int i23 = i22 + 1;
        _m_methods = fmVarArr;
        C1634Xv.m11725a((Object[]) Amplifier._m_methods, (Object[]) _m_methods);
        ___iScriptClass = aUO.m18566a(WeaponAmplifier.class, aUI.class, _m_fields, _m_methods);
    }

    /* renamed from: B */
    private void m29465B(C3892wO wOVar) {
        throw new C6039afL();
    }

    /* renamed from: C */
    private void m29466C(C3892wO wOVar) {
        throw new C6039afL();
    }

    /* renamed from: D */
    private void m29467D(C3892wO wOVar) {
        throw new C6039afL();
    }

    /* renamed from: E */
    private void m29468E(C3892wO wOVar) {
        throw new C6039afL();
    }

    /* renamed from: F */
    private void m29469F(C3892wO wOVar) {
        throw new C6039afL();
    }

    @C0064Am(aul = "8fbbb7ba0b86d66303c7fbf3e0ed1097", aum = 0)
    @C5566aOg
    /* renamed from: Kr */
    private void m29470Kr() {
        throw new aWi(new aCE(this, atW, new Object[0]));
    }

    @C0064Am(aul = "59f674da3c1af1317d2dc8a20636ad09", aum = 0)
    @C5566aOg
    /* renamed from: Kt */
    private void m29471Kt() {
        throw new aWi(new aCE(this, atX, new Object[0]));
    }

    private C2686iZ aGv() {
        return ((WeaponAmplifierType) getType()).aGK();
    }

    private C2686iZ aGw() {
        return ((WeaponAmplifierType) getType()).aGO();
    }

    private C1556Wo acz() {
        return (C1556Wo) bFf().mo5608dq().mo3214p(bjM);
    }

    private C3892wO amE() {
        return ((WeaponAmplifierType) getType()).aGy();
    }

    private C3892wO amF() {
        return ((WeaponAmplifierType) getType()).aGA();
    }

    private C3892wO amG() {
        return ((WeaponAmplifierType) getType()).aGC();
    }

    private C3892wO amH() {
        return ((WeaponAmplifierType) getType()).aGE();
    }

    private C3892wO amI() {
        return ((WeaponAmplifierType) getType()).aGG();
    }

    private boolean amJ() {
        return ((WeaponAmplifierType) getType()).aGI();
    }

    @C0064Am(aul = "b8e9f1c13cd0c68da261b41f79d997e3", aum = 0)
    @C5566aOg
    /* renamed from: b */
    private void m29473b(Weapon adv) {
        throw new aWi(new aCE(this, bkh, new Object[]{adv}));
    }

    /* renamed from: bC */
    private void m29474bC(boolean z) {
        throw new C6039afL();
    }

    @C5566aOg
    /* renamed from: c */
    private void m29475c(Weapon adv) {
        switch (bFf().mo6893i(bkh)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, bkh, new Object[]{adv}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, bkh, new Object[]{adv}));
                break;
        }
        m29473b(adv);
    }

    @C0064Am(aul = "561fae7b8c819dfef00dab0edd4d265d", aum = 0)
    @C5566aOg
    /* renamed from: m */
    private void m29476m(Component abl) {
        throw new aWi(new aCE(this, ccN, new Object[]{abl}));
    }

    @C0064Am(aul = "4b43a6dee77502bb1cc531796bb06de6", aum = 0)
    @C5566aOg
    /* renamed from: o */
    private void m29478o(Component abl) {
        throw new aWi(new aCE(this, ccO, new Object[]{abl}));
    }

    /* renamed from: q */
    private void m29479q(C1556Wo wo) {
        bFf().mo5608dq().mo3197f(bjM, wo);
    }

    @C0064Am(aul = "4e14412cb4dcad72a59a1247bd3fdcfe", aum = 0)
    @C5566aOg
    @C2499fr
    /* renamed from: qU */
    private void m29480qU() {
        throw new aWi(new aCE(this, f6721Lm, new Object[0]));
    }

    /* renamed from: y */
    private void m29481y(C2686iZ iZVar) {
        throw new C6039afL();
    }

    /* renamed from: z */
    private void m29482z(C2686iZ iZVar) {
        throw new C6039afL();
    }

    @C5566aOg
    /* renamed from: Ks */
    public void mo1952Ks() {
        switch (bFf().mo6893i(atW)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, atW, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, atW, new Object[0]));
                break;
        }
        m29470Kr();
    }

    @C5566aOg
    /* renamed from: Ku */
    public void mo1953Ku() {
        switch (bFf().mo6893i(atX)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, atX, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, atX, new Object[0]));
                break;
        }
        m29471Kt();
    }

    /* renamed from: S */
    public void mo10S() {
        super.mo10S();
    }

    /* renamed from: T */
    public C6494any mo11T() {
        return ___iScriptClass;
    }

    /* renamed from: U */
    public C2491fm[] mo12U() {
        return _m_methods;
    }

    /* renamed from: W */
    public Object mo13W() {
        return new aUI(this);
    }

    /* renamed from: a */
    public Object mo14a(C0495Gr gr) {
        Object[] args = gr.getArgs();
        switch (gr.mo2417hq() - Amplifier._m_methodCount) {
            case 0:
                m29470Kr();
                return null;
            case 1:
                m29473b((Weapon) args[0]);
                return null;
            case 2:
                m29471Kt();
                return null;
            case 3:
                m29476m((Component) args[0]);
                return null;
            case 4:
                m29478o((Component) args[0]);
                return null;
            case 5:
                return amM();
            case 6:
                return amO();
            case 7:
                return amQ();
            case 8:
                return amS();
            case 9:
                return amU();
            case 10:
                return new Boolean(m29477mm());
            case 11:
                m29480qU();
                return null;
            default:
                return super.mo14a(gr);
        }
    }

    /* renamed from: a */
    public void mo15a(Collection collection, C0495Gr gr) {
        super.mo15a(collection, gr);
    }

    public C3892wO amN() {
        switch (bFf().mo6893i(bEp)) {
            case 0:
                return null;
            case 2:
                return (C3892wO) bFf().mo5606d(new aCE(this, bEp, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, bEp, new Object[0]));
                break;
        }
        return amM();
    }

    public C3892wO amP() {
        switch (bFf().mo6893i(bEq)) {
            case 0:
                return null;
            case 2:
                return (C3892wO) bFf().mo5606d(new aCE(this, bEq, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, bEq, new Object[0]));
                break;
        }
        return amO();
    }

    public C3892wO amR() {
        switch (bFf().mo6893i(bEr)) {
            case 0:
                return null;
            case 2:
                return (C3892wO) bFf().mo5606d(new aCE(this, bEr, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, bEr, new Object[0]));
                break;
        }
        return amQ();
    }

    public C3892wO amT() {
        switch (bFf().mo6893i(bEs)) {
            case 0:
                return null;
            case 2:
                return (C3892wO) bFf().mo5606d(new aCE(this, bEs, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, bEs, new Object[0]));
                break;
        }
        return amS();
    }

    public C3892wO amV() {
        switch (bFf().mo6893i(bEt)) {
            case 0:
                return null;
            case 2:
                return (C3892wO) bFf().mo5606d(new aCE(this, bEt, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, bEt, new Object[0]));
                break;
        }
        return amU();
    }

    /* renamed from: c */
    public C5663aRz[] mo25c() {
        return _m_fields;
    }

    /* renamed from: mn */
    public boolean mo17996mn() {
        switch (bFf().mo6893i(f6720DK)) {
            case 0:
                return false;
            case 2:
                return ((Boolean) bFf().mo5606d(new aCE(this, f6720DK, new Object[0]))).booleanValue();
            case 3:
                bFf().mo5606d(new aCE(this, f6720DK, new Object[0]));
                break;
        }
        return m29477mm();
    }

    @C5566aOg
    /* renamed from: n */
    public void mo1957n(Component abl) {
        switch (bFf().mo6893i(ccN)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, ccN, new Object[]{abl}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, ccN, new Object[]{abl}));
                break;
        }
        m29476m(abl);
    }

    @C5566aOg
    /* renamed from: p */
    public void mo1958p(Component abl) {
        switch (bFf().mo6893i(ccO)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, ccO, new Object[]{abl}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, ccO, new Object[]{abl}));
                break;
        }
        m29478o(abl);
    }

    @C5566aOg
    @C2499fr
    /* renamed from: qV */
    public void mo1959qV() {
        switch (bFf().mo6893i(f6721Lm)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f6721Lm, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f6721Lm, new Object[0]));
                break;
        }
        m29480qU();
    }

    /* renamed from: e */
    public void mo1956e(AmplifierType iDVar) {
        super.mo1956e(iDVar);
    }

    @C0064Am(aul = "1285804ce422899bbe3e39b89c112b20", aum = 0)
    private C3892wO amM() {
        return amE();
    }

    @C0064Am(aul = "7b652d82e5a8c62340c8f906df3a5430", aum = 0)
    private C3892wO amO() {
        return amF();
    }

    @C0064Am(aul = "a03e0668cfc282ec866fcdab425580b3", aum = 0)
    private C3892wO amQ() {
        return amG();
    }

    @C0064Am(aul = "d81e5df3d3e3218b9cd45edc34f28a11", aum = 0)
    private C3892wO amS() {
        return amH();
    }

    @C0064Am(aul = "c944c5bc74d07f7ea7f3d34704682de2", aum = 0)
    private C3892wO amU() {
        return amI();
    }

    @C0064Am(aul = "d5942523196d205115e909bbb9208f05", aum = 0)
    /* renamed from: mm */
    private boolean m29477mm() {
        return amJ();
    }

    @C6485anp
    @C5511aMd
    /* renamed from: a.eH$a */
    public class WeaponAmplifierAdapter extends WeaponAdapter implements C1616Xf {

        /* renamed from: AI */
        public static final C2491fm f6725AI = null;
        /* renamed from: DJ */
        public static final C2491fm f6727DJ = null;
        /* renamed from: DK */
        public static final C2491fm f6728DK = null;
        public static final int _m_fieldCount = 0;
        public static final C5663aRz[] _m_fields = null;
        public static final int _m_methodCount = 0;
        public static final C2491fm[] _m_methods = null;
        /* renamed from: aT */
        public static final C5663aRz f6729aT = null;
        public static final long serialVersionUID = 0;
        /* renamed from: vb */
        public static final C2491fm f6730vb = null;
        /* renamed from: vh */
        public static final C2491fm f6731vh = null;
        public static C6494any ___iScriptClass;
        @C0064Am(aul = "9970182609f9f8bdd667af0719216889", aum = 0)

        /* renamed from: DI */
        static /* synthetic */ WeaponAmplifier f6726DI;

        static {
            m29497V();
        }

        public WeaponAmplifierAdapter() {
            super((C5540aNg) null);
            super.mo10S();
        }

        public WeaponAmplifierAdapter(C5540aNg ang) {
            super(ang);
        }

        public WeaponAmplifierAdapter(WeaponAmplifier eHVar) {
            super((C5540aNg) null);
            super._m_script_init(eHVar);
        }

        /* renamed from: V */
        static void m29497V() {
            serialVersionUID = (long) 1;
            _m_fieldCount = WeaponAdapter._m_fieldCount + 1;
            _m_methodCount = WeaponAdapter._m_methodCount + 5;
            int i = WeaponAdapter._m_fieldCount;
            C5663aRz[] arzArr = new C5663aRz[(i + 1)];
            C5663aRz b = C5640aRc.m17844b(WeaponAmplifierAdapter.class, "9970182609f9f8bdd667af0719216889", i);
            f6729aT = b;
            arzArr[i] = b;
            int i2 = i + 1;
            _m_fields = arzArr;
            C1634Xv.m11725a((Object[]) WeaponAdapter._m_fields, (Object[]) _m_fields);
            int i3 = WeaponAdapter._m_methodCount;
            C2491fm[] fmVarArr = new C2491fm[(i3 + 5)];
            C2491fm a = C4105zY.m41624a(WeaponAmplifierAdapter.class, "1bb293c3e4845bc46ba4058d5207845d", i3);
            f6727DJ = a;
            fmVarArr[i3] = a;
            int i4 = i3 + 1;
            C2491fm a2 = C4105zY.m41624a(WeaponAmplifierAdapter.class, "9d504fd0eb807c3854d288702b9ccd00", i4);
            f6730vb = a2;
            fmVarArr[i4] = a2;
            int i5 = i4 + 1;
            C2491fm a3 = C4105zY.m41624a(WeaponAmplifierAdapter.class, "8fe35b7b094af5414394db0a701433e6", i5);
            f6725AI = a3;
            fmVarArr[i5] = a3;
            int i6 = i5 + 1;
            C2491fm a4 = C4105zY.m41624a(WeaponAmplifierAdapter.class, "10345ac076cf5b3011ea6940f28ee629", i6);
            f6731vh = a4;
            fmVarArr[i6] = a4;
            int i7 = i6 + 1;
            C2491fm a5 = C4105zY.m41624a(WeaponAmplifierAdapter.class, "85f2d0fba983446df33e444609e9763f", i7);
            f6728DK = a5;
            fmVarArr[i7] = a5;
            int i8 = i7 + 1;
            _m_methods = fmVarArr;
            C1634Xv.m11725a((Object[]) WeaponAdapter._m_methods, (Object[]) _m_methods);
            ___iScriptClass = aUO.m18566a(WeaponAmplifierAdapter.class, C0303Dw.class, _m_fields, _m_methods);
        }

        /* renamed from: a */
        private void m29498a(WeaponAmplifier eHVar) {
            bFf().mo5608dq().mo3197f(f6729aT, eHVar);
        }

        /* renamed from: mj */
        private WeaponAmplifier m29502mj() {
            return (WeaponAmplifier) bFf().mo5608dq().mo3214p(f6729aT);
        }

        /* renamed from: S */
        public void mo10S() {
            super.mo10S();
        }

        /* renamed from: T */
        public C6494any mo11T() {
            return ___iScriptClass;
        }

        /* renamed from: U */
        public C2491fm[] mo12U() {
            return _m_methods;
        }

        /* renamed from: W */
        public Object mo13W() {
            return new C0303Dw(this);
        }

        /* renamed from: a */
        public Object mo14a(C0495Gr gr) {
            Object[] args = gr.getArgs();
            switch (gr.mo2417hq() - WeaponAdapter._m_methodCount) {
                case 0:
                    return new Float(m29503mk());
                case 1:
                    return new Float(m29500im());
                case 2:
                    return new Float(m29499c((DamageType) args[0]));
                case 3:
                    return new Float(m29501is());
                case 4:
                    return new Boolean(m29504mm());
                default:
                    return super.mo14a(gr);
            }
        }

        /* renamed from: a */
        public void mo15a(Collection collection, C0495Gr gr) {
            super.mo15a(collection, gr);
        }

        /* renamed from: c */
        public C5663aRz[] mo25c() {
            return _m_fields;
        }

        /* renamed from: d */
        public float mo5375d(DamageType fr) {
            switch (bFf().mo6893i(f6725AI)) {
                case 0:
                    return 0.0f;
                case 2:
                    return ((Float) bFf().mo5606d(new aCE(this, f6725AI, new Object[]{fr}))).floatValue();
                case 3:
                    bFf().mo5606d(new aCE(this, f6725AI, new Object[]{fr}));
                    break;
            }
            return m29499c(fr);
        }

        public float getSpeed() {
            switch (bFf().mo6893i(f6731vh)) {
                case 0:
                    return 0.0f;
                case 2:
                    return ((Float) bFf().mo5606d(new aCE(this, f6731vh, new Object[0]))).floatValue();
                case 3:
                    bFf().mo5606d(new aCE(this, f6731vh, new Object[0]));
                    break;
            }
            return m29501is();
        }

        /* renamed from: in */
        public float mo12953in() {
            switch (bFf().mo6893i(f6730vb)) {
                case 0:
                    return 0.0f;
                case 2:
                    return ((Float) bFf().mo5606d(new aCE(this, f6730vb, new Object[0]))).floatValue();
                case 3:
                    bFf().mo5606d(new aCE(this, f6730vb, new Object[0]));
                    break;
            }
            return m29500im();
        }

        /* renamed from: ml */
        public float mo12954ml() {
            switch (bFf().mo6893i(f6727DJ)) {
                case 0:
                    return 0.0f;
                case 2:
                    return ((Float) bFf().mo5606d(new aCE(this, f6727DJ, new Object[0]))).floatValue();
                case 3:
                    bFf().mo5606d(new aCE(this, f6727DJ, new Object[0]));
                    break;
            }
            return m29503mk();
        }

        /* renamed from: mn */
        public boolean mo12955mn() {
            switch (bFf().mo6893i(f6728DK)) {
                case 0:
                    return false;
                case 2:
                    return ((Boolean) bFf().mo5606d(new aCE(this, f6728DK, new Object[0]))).booleanValue();
                case 3:
                    bFf().mo5606d(new aCE(this, f6728DK, new Object[0]));
                    break;
            }
            return m29504mm();
        }

        /* renamed from: b */
        public void mo17997b(WeaponAmplifier eHVar) {
            m29498a(eHVar);
            super.mo10S();
        }

        @C0064Am(aul = "1bb293c3e4845bc46ba4058d5207845d", aum = 0)
        /* renamed from: mk */
        private float m29503mk() {
            float v = m29502mj().amN().mo22753v(((WeaponAdapter) mo16070IE()).mo12954ml(), super.mo12954ml());
            if (v > 10.0f) {
                return 10.0f;
            }
            return v;
        }

        @C0064Am(aul = "9d504fd0eb807c3854d288702b9ccd00", aum = 0)
        /* renamed from: im */
        private float m29500im() {
            return m29502mj().amR().mo22753v(((WeaponAdapter) mo16070IE()).mo12953in(), super.mo12953in());
        }

        @C0064Am(aul = "8fe35b7b094af5414394db0a701433e6", aum = 0)
        /* renamed from: c */
        private float m29499c(DamageType fr) {
            return m29502mj().amT().mo22753v(((WeaponAdapter) mo16070IE()).mo5375d(fr), super.mo5375d(fr));
        }

        @C0064Am(aul = "10345ac076cf5b3011ea6940f28ee629", aum = 0)
        /* renamed from: is */
        private float m29501is() {
            return m29502mj().amP().mo22753v(((WeaponAdapter) mo16070IE()).getSpeed(), super.getSpeed());
        }

        @C0064Am(aul = "85f2d0fba983446df33e444609e9763f", aum = 0)
        /* renamed from: mm */
        private boolean m29504mm() {
            return m29502mj().mo17996mn() || super.mo12955mn();
        }
    }
}
