package game.script.item.buff.module;

import game.network.message.externalizable.aCE;
import logic.baa.*;
import logic.data.mbean.C5267aCt;
import logic.res.code.C5640aRc;
import logic.res.code.C5663aRz;
import logic.res.code.C6494any;
import logic.res.html.C2491fm;
import p001a.*;

import java.util.Collection;

@C0566Hp
@C5829abJ("1.1.2")
@C6485anp
@C5511aMd
/* renamed from: a.Eu */
/* compiled from: a */
public class HealerType extends AttributeBuffType implements C1616Xf {
    public static final int _m_fieldCount = 0;
    public static final C5663aRz[] _m_fields = null;
    public static final int _m_methodCount = 0;
    public static final C2491fm[] _m_methods = null;
    public static final C5663aRz cSd = null;
    public static final C5663aRz cSe = null;
    public static final C2491fm cSf = null;
    public static final C2491fm cSg = null;
    public static final C2491fm cSh = null;
    public static final C2491fm cSi = null;
    public static final C2491fm cSj = null;
    /* renamed from: dN */
    public static final C2491fm f512dN = null;
    public static final long serialVersionUID = 0;
    public static C6494any ___iScriptClass;
    @C0064Am(aul = "139888fef893ea5164afa2a072578a89", aum = 0)
    private static float apV;
    @C0064Am(aul = "0982050d95207190a1d5c04992fe1775", aum = 1)
    private static float apW;

    static {
        m3054V();
    }

    public HealerType() {
        super((C5540aNg) null);
        super.mo10S();
    }

    public HealerType(C5540aNg ang) {
        super(ang);
    }

    /* renamed from: V */
    static void m3054V() {
        serialVersionUID = (long) 1;
        _m_fieldCount = AttributeBuffType._m_fieldCount + 2;
        _m_methodCount = AttributeBuffType._m_methodCount + 6;
        int i = AttributeBuffType._m_fieldCount;
        C5663aRz[] arzArr = new C5663aRz[(i + 2)];
        C5663aRz b = C5640aRc.m17844b(HealerType.class, "139888fef893ea5164afa2a072578a89", i);
        cSd = b;
        arzArr[i] = b;
        int i2 = i + 1;
        C5663aRz b2 = C5640aRc.m17844b(HealerType.class, "0982050d95207190a1d5c04992fe1775", i2);
        cSe = b2;
        arzArr[i2] = b2;
        int i3 = i2 + 1;
        _m_fields = arzArr;
        C1634Xv.m11725a((Object[]) AttributeBuffType._m_fields, (Object[]) _m_fields);
        int i4 = AttributeBuffType._m_methodCount;
        C2491fm[] fmVarArr = new C2491fm[(i4 + 6)];
        C2491fm a = C4105zY.m41624a(HealerType.class, "9d64031ec65979a9e799a635fe6c93ea", i4);
        cSf = a;
        fmVarArr[i4] = a;
        int i5 = i4 + 1;
        C2491fm a2 = C4105zY.m41624a(HealerType.class, "be27eba7b0cd6a564c621080cbfb81ef", i5);
        cSg = a2;
        fmVarArr[i5] = a2;
        int i6 = i5 + 1;
        C2491fm a3 = C4105zY.m41624a(HealerType.class, "1a89b9eee0a53daf599c09cba8fdc0e8", i6);
        cSh = a3;
        fmVarArr[i6] = a3;
        int i7 = i6 + 1;
        C2491fm a4 = C4105zY.m41624a(HealerType.class, "9c137ec01ca7eefc8ff68ce516ce4f5b", i7);
        cSi = a4;
        fmVarArr[i7] = a4;
        int i8 = i7 + 1;
        C2491fm a5 = C4105zY.m41624a(HealerType.class, "7c75238eed138e877f04390fc4adb149", i8);
        cSj = a5;
        fmVarArr[i8] = a5;
        int i9 = i8 + 1;
        C2491fm a6 = C4105zY.m41624a(HealerType.class, "eb481fa0ece76e9bb5c01f745b573333", i9);
        f512dN = a6;
        fmVarArr[i9] = a6;
        int i10 = i9 + 1;
        _m_methods = fmVarArr;
        C1634Xv.m11725a((Object[]) AttributeBuffType._m_methods, (Object[]) _m_methods);
        ___iScriptClass = aUO.m18566a(HealerType.class, C5267aCt.class, _m_fields, _m_methods);
    }

    private float aNV() {
        return bFf().mo5608dq().mo3211m(cSd);
    }

    private float aNW() {
        return bFf().mo5608dq().mo3211m(cSe);
    }

    /* renamed from: ff */
    private void m3056ff(float f) {
        bFf().mo5608dq().mo3150a(cSd, f);
    }

    /* renamed from: fg */
    private void m3057fg(float f) {
        bFf().mo5608dq().mo3150a(cSe, f);
    }

    /* renamed from: T */
    public C6494any mo11T() {
        return ___iScriptClass;
    }

    /* renamed from: U */
    public C2491fm[] mo12U() {
        return _m_methods;
    }

    /* renamed from: W */
    public Object mo13W() {
        return new C5267aCt(this);
    }

    /* renamed from: a */
    public Object mo14a(C0495Gr gr) {
        Object[] args = gr.getArgs();
        switch (gr.mo2417hq() - AttributeBuffType._m_methodCount) {
            case 0:
                return new Float(aNX());
            case 1:
                m3058fh(((Float) args[0]).floatValue());
                return null;
            case 2:
                return new Float(aNZ());
            case 3:
                m3059fj(((Float) args[0]).floatValue());
                return null;
            case 4:
                return aOb();
            case 5:
                return m3055aT();
            default:
                return super.mo14a(gr);
        }
    }

    /* renamed from: a */
    public void mo15a(Collection collection, C0495Gr gr) {
        super.mo15a(collection, gr);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Shield Healing")
    public float aNY() {
        switch (bFf().mo6893i(cSf)) {
            case 0:
                return 0.0f;
            case 2:
                return ((Float) bFf().mo5606d(new aCE(this, cSf, new Object[0]))).floatValue();
            case 3:
                bFf().mo5606d(new aCE(this, cSf, new Object[0]));
                break;
        }
        return aNX();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Hull Healing")
    public float aOa() {
        switch (bFf().mo6893i(cSh)) {
            case 0:
                return 0.0f;
            case 2:
                return ((Float) bFf().mo5606d(new aCE(this, cSh, new Object[0]))).floatValue();
            case 3:
                bFf().mo5606d(new aCE(this, cSh, new Object[0]));
                break;
        }
        return aNZ();
    }

    public Healer aOc() {
        switch (bFf().mo6893i(cSj)) {
            case 0:
                return null;
            case 2:
                return (Healer) bFf().mo5606d(new aCE(this, cSj, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, cSj, new Object[0]));
                break;
        }
        return aOb();
    }

    /* access modifiers changed from: protected */
    /* renamed from: aU */
    public <T extends aDJ> T mo745aU() {
        switch (bFf().mo6893i(f512dN)) {
            case 0:
                return null;
            case 2:
                return (aDJ) bFf().mo5606d(new aCE(this, f512dN, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f512dN, new Object[0]));
                break;
        }
        return m3055aT();
    }

    /* renamed from: c */
    public C5663aRz[] mo25c() {
        return _m_fields;
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Shield Healing")
    /* renamed from: fi */
    public void mo2012fi(float f) {
        switch (bFf().mo6893i(cSg)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, cSg, new Object[]{new Float(f)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, cSg, new Object[]{new Float(f)}));
                break;
        }
        m3058fh(f);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Hull Healing")
    /* renamed from: fk */
    public void mo2013fk(float f) {
        switch (bFf().mo6893i(cSi)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, cSi, new Object[]{new Float(f)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, cSi, new Object[]{new Float(f)}));
                break;
        }
        m3059fj(f);
    }

    /* renamed from: S */
    public void mo10S() {
        super.mo10S();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Shield Healing")
    @C0064Am(aul = "9d64031ec65979a9e799a635fe6c93ea", aum = 0)
    private float aNX() {
        return aNV();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Shield Healing")
    @C0064Am(aul = "be27eba7b0cd6a564c621080cbfb81ef", aum = 0)
    /* renamed from: fh */
    private void m3058fh(float f) {
        m3056ff(f);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Hull Healing")
    @C0064Am(aul = "1a89b9eee0a53daf599c09cba8fdc0e8", aum = 0)
    private float aNZ() {
        return aNW();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Hull Healing")
    @C0064Am(aul = "9c137ec01ca7eefc8ff68ce516ce4f5b", aum = 0)
    /* renamed from: fj */
    private void m3059fj(float f) {
        m3057fg(f);
    }

    @C0064Am(aul = "7c75238eed138e877f04390fc4adb149", aum = 0)
    private Healer aOb() {
        return (Healer) mo745aU();
    }

    @C0064Am(aul = "eb481fa0ece76e9bb5c01f745b573333", aum = 0)
    /* renamed from: aT */
    private <T extends aDJ> T m3055aT() {
        T t = (Healer) bFf().mo6865M(Healer.class);
        t.mo7840a(this);
        return t;
    }
}
