package game.script.item.buff.amplifier;

import game.network.message.externalizable.aCE;
import game.script.item.AmplifierType;
import logic.baa.*;
import logic.data.mbean.C6418ama;
import logic.res.code.C5640aRc;
import logic.res.code.C5663aRz;
import logic.res.code.C6494any;
import logic.res.html.C2491fm;
import p001a.*;

import java.util.Collection;

@C0566Hp
@C5829abJ("2.0.1")
@C6485anp
@C5511aMd
/* renamed from: a.BU */
/* compiled from: a */
public class HullAmplifierType extends AmplifierType implements C1616Xf {
    public static final int _m_fieldCount = 0;
    public static final C5663aRz[] _m_fields = null;
    public static final int _m_methodCount = 0;
    public static final C2491fm[] _m_methods = null;
    public static final C5663aRz aOJ = null;
    public static final C5663aRz aOr = null;
    public static final C2491fm cqF = null;
    public static final C2491fm cqG = null;
    public static final C2491fm cqH = null;
    public static final C2491fm cqI = null;
    public static final C2491fm cqJ = null;
    /* renamed from: dN */
    public static final C2491fm f199dN = null;
    public static final long serialVersionUID = 0;
    public static C6494any ___iScriptClass;
    @C0064Am(aul = "af2acce03d892eb2682123aa34d10f59", aum = 0)
    private static C3892wO aBJ;
    @C0064Am(aul = "b6a672f889deef4e57fe7ff0ea462302", aum = 1)
    private static C3892wO aBK;

    static {
        m1228V();
    }

    public HullAmplifierType() {
        super((C5540aNg) null);
        super.mo10S();
    }

    public HullAmplifierType(C5540aNg ang) {
        super(ang);
    }

    /* renamed from: V */
    static void m1228V() {
        serialVersionUID = (long) 1;
        _m_fieldCount = AmplifierType._m_fieldCount + 2;
        _m_methodCount = AmplifierType._m_methodCount + 6;
        int i = AmplifierType._m_fieldCount;
        C5663aRz[] arzArr = new C5663aRz[(i + 2)];
        C5663aRz b = C5640aRc.m17844b(HullAmplifierType.class, "af2acce03d892eb2682123aa34d10f59", i);
        aOJ = b;
        arzArr[i] = b;
        int i2 = i + 1;
        C5663aRz b2 = C5640aRc.m17844b(HullAmplifierType.class, "b6a672f889deef4e57fe7ff0ea462302", i2);
        aOr = b2;
        arzArr[i2] = b2;
        int i3 = i2 + 1;
        _m_fields = arzArr;
        C1634Xv.m11725a((Object[]) AmplifierType._m_fields, (Object[]) _m_fields);
        int i4 = AmplifierType._m_methodCount;
        C2491fm[] fmVarArr = new C2491fm[(i4 + 6)];
        C2491fm a = C4105zY.m41624a(HullAmplifierType.class, "29d43d1edb32765326e46dabec1c8d23", i4);
        cqF = a;
        fmVarArr[i4] = a;
        int i5 = i4 + 1;
        C2491fm a2 = C4105zY.m41624a(HullAmplifierType.class, "1701aff68c4bc4a87725fbbd2a344c9a", i5);
        cqG = a2;
        fmVarArr[i5] = a2;
        int i6 = i5 + 1;
        C2491fm a3 = C4105zY.m41624a(HullAmplifierType.class, "485b8ee107eaa7857e384e85acb7a663", i6);
        cqH = a3;
        fmVarArr[i6] = a3;
        int i7 = i6 + 1;
        C2491fm a4 = C4105zY.m41624a(HullAmplifierType.class, "de1cd921decb8246e4b08c982ac407fa", i7);
        cqI = a4;
        fmVarArr[i7] = a4;
        int i8 = i7 + 1;
        C2491fm a5 = C4105zY.m41624a(HullAmplifierType.class, "6ec7569fc40df8aa948e57295b61f51b", i8);
        cqJ = a5;
        fmVarArr[i8] = a5;
        int i9 = i8 + 1;
        C2491fm a6 = C4105zY.m41624a(HullAmplifierType.class, "485e0e6d65a432e6a3dabe9f44770655", i9);
        f199dN = a6;
        fmVarArr[i9] = a6;
        int i10 = i9 + 1;
        _m_methods = fmVarArr;
        C1634Xv.m11725a((Object[]) AmplifierType._m_methods, (Object[]) _m_methods);
        ___iScriptClass = aUO.m18566a(HullAmplifierType.class, C6418ama.class, _m_fields, _m_methods);
    }

    /* renamed from: Ub */
    private C3892wO m1226Ub() {
        return (C3892wO) bFf().mo5608dq().mo3214p(aOJ);
    }

    /* renamed from: Uc */
    private C3892wO m1227Uc() {
        return (C3892wO) bFf().mo5608dq().mo3214p(aOr);
    }

    /* renamed from: v */
    private void m1230v(C3892wO wOVar) {
        bFf().mo5608dq().mo3197f(aOJ, wOVar);
    }

    /* renamed from: w */
    private void m1231w(C3892wO wOVar) {
        bFf().mo5608dq().mo3197f(aOr, wOVar);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Health Points")
    /* renamed from: H */
    public void mo740H(C3892wO wOVar) {
        switch (bFf().mo6893i(cqG)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, cqG, new Object[]{wOVar}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, cqG, new Object[]{wOVar}));
                break;
        }
        m1224G(wOVar);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Damage Reduction")
    /* renamed from: J */
    public void mo741J(C3892wO wOVar) {
        switch (bFf().mo6893i(cqI)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, cqI, new Object[]{wOVar}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, cqI, new Object[]{wOVar}));
                break;
        }
        m1225I(wOVar);
    }

    /* renamed from: T */
    public C6494any mo11T() {
        return ___iScriptClass;
    }

    /* renamed from: U */
    public C2491fm[] mo12U() {
        return _m_methods;
    }

    /* renamed from: W */
    public Object mo13W() {
        return new C6418ama(this);
    }

    /* renamed from: a */
    public Object mo14a(C0495Gr gr) {
        Object[] args = gr.getArgs();
        switch (gr.mo2417hq() - AmplifierType._m_methodCount) {
            case 0:
                return aBb();
            case 1:
                m1224G((C3892wO) args[0]);
                return null;
            case 2:
                return aBd();
            case 3:
                m1225I((C3892wO) args[0]);
                return null;
            case 4:
                return aBf();
            case 5:
                return m1229aT();
            default:
                return super.mo14a(gr);
        }
    }

    /* renamed from: a */
    public void mo15a(Collection collection, C0495Gr gr) {
        super.mo15a(collection, gr);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Health Points")
    public C3892wO aBc() {
        switch (bFf().mo6893i(cqF)) {
            case 0:
                return null;
            case 2:
                return (C3892wO) bFf().mo5606d(new aCE(this, cqF, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, cqF, new Object[0]));
                break;
        }
        return aBb();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Damage Reduction")
    public C3892wO aBe() {
        switch (bFf().mo6893i(cqH)) {
            case 0:
                return null;
            case 2:
                return (C3892wO) bFf().mo5606d(new aCE(this, cqH, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, cqH, new Object[0]));
                break;
        }
        return aBd();
    }

    public HullAmplifier aBg() {
        switch (bFf().mo6893i(cqJ)) {
            case 0:
                return null;
            case 2:
                return (HullAmplifier) bFf().mo5606d(new aCE(this, cqJ, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, cqJ, new Object[0]));
                break;
        }
        return aBf();
    }

    /* access modifiers changed from: protected */
    /* renamed from: aU */
    public <T extends aDJ> T mo745aU() {
        switch (bFf().mo6893i(f199dN)) {
            case 0:
                return null;
            case 2:
                return (aDJ) bFf().mo5606d(new aCE(this, f199dN, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f199dN, new Object[0]));
                break;
        }
        return m1229aT();
    }

    /* renamed from: c */
    public C5663aRz[] mo25c() {
        return _m_fields;
    }

    /* renamed from: S */
    public void mo10S() {
        super.mo10S();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Health Points")
    @C0064Am(aul = "29d43d1edb32765326e46dabec1c8d23", aum = 0)
    private C3892wO aBb() {
        return m1226Ub();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Health Points")
    @C0064Am(aul = "1701aff68c4bc4a87725fbbd2a344c9a", aum = 0)
    /* renamed from: G */
    private void m1224G(C3892wO wOVar) {
        m1230v(wOVar);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Damage Reduction")
    @C0064Am(aul = "485b8ee107eaa7857e384e85acb7a663", aum = 0)
    private C3892wO aBd() {
        return m1227Uc();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Damage Reduction")
    @C0064Am(aul = "de1cd921decb8246e4b08c982ac407fa", aum = 0)
    /* renamed from: I */
    private void m1225I(C3892wO wOVar) {
        m1231w(wOVar);
    }

    @C0064Am(aul = "6ec7569fc40df8aa948e57295b61f51b", aum = 0)
    private HullAmplifier aBf() {
        return (HullAmplifier) mo745aU();
    }

    @C0064Am(aul = "485e0e6d65a432e6a3dabe9f44770655", aum = 0)
    /* renamed from: aT */
    private <T extends aDJ> T m1229aT() {
        T t = (HullAmplifier) bFf().mo6865M(HullAmplifier.class);
        t.mo1956e(this);
        return t;
    }
}
