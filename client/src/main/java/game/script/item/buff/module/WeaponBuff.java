package game.script.item.buff.module;

import game.network.message.externalizable.aCE;
import game.network.message.serializable.C1556Wo;
import game.script.damage.DamageType;
import game.script.item.Component;
import game.script.item.Weapon;
import game.script.item.WeaponAdapter;
import game.script.ship.Ship;
import logic.baa.C1616Xf;
import logic.baa.C1634Xv;
import logic.baa.C5511aMd;
import logic.baa.C6485anp;
import logic.data.link.C3981xi;
import logic.data.mbean.C0326ER;
import logic.data.mbean.C3965xS;
import logic.res.code.C5640aRc;
import logic.res.code.C5663aRz;
import logic.res.code.C6494any;
import logic.res.html.C2491fm;
import p001a.*;

import java.util.Collection;

@C5829abJ("1.1.2")
@C6485anp
@C2712iu(version = "1.1.2")
@C5511aMd
/* renamed from: a.wF */
/* compiled from: a */
public class WeaponBuff extends AttributeBuff implements C1616Xf, C3981xi.C3983b, C3981xi.C3984c {

    /* renamed from: DK */
    public static final C2491fm f9462DK = null;
    public static final int _m_fieldCount = 0;
    public static final C5663aRz[] _m_fields = null;
    public static final int _m_methodCount = 0;
    public static final C2491fm[] _m_methods = null;
    public static final C2491fm aPH = null;
    public static final C2491fm aPI = null;
    public static final C5663aRz bEc = null;
    public static final C5663aRz bEg = null;
    public static final C5663aRz bEi = null;
    public static final C5663aRz bEk = null;
    public static final C5663aRz bEm = null;
    public static final C5663aRz bEo = null;
    public static final C2491fm bEp = null;
    public static final C2491fm bEq = null;
    public static final C2491fm bEr = null;
    public static final C2491fm bEs = null;
    public static final C2491fm bEt = null;
    public static final C2491fm bEu = null;
    public static final C2491fm bEv = null;
    public static final C2491fm bEw = null;
    public static final long serialVersionUID = 0;
    /* renamed from: uN */
    public static final C5663aRz f9463uN = null;
    /* renamed from: uS */
    public static final C5663aRz f9464uS = null;
    public static C6494any ___iScriptClass;
    @C0064Am(aul = "6d0b39325e28ed5270686057929bee71", aum = 0)
    private static C3892wO bEb;
    @C0064Am(aul = "9785c21e4911bbff3115eabec537c410", aum = 1)
    private static C3892wO bEd;
    @C0064Am(aul = "579045f142993eadb80ddaf77d535afa", aum = 2)
    private static C3892wO bEe;
    @C0064Am(aul = "3a5b84d36d20afdcac81ac58a9b0bfaf", aum = 3)
    private static C3892wO bEf;
    @C0064Am(aul = "08e94c4f7158ef3c8309ddc65d4465af", aum = 4)
    private static C3892wO bEh;
    @C0064Am(aul = "fcce36402cbb57e52da1a8c75d207aba", aum = 5)
    private static boolean bEj;
    @C0064Am(aul = "6420b7d0c2d8a7a2ce1d30afddbe81ef", aum = 6)
    private static boolean bEl;
    @C0064Am(aul = "353580931ef706a3e64fe9e71fa6dd91", aum = 7)
    private static C1556Wo<Weapon, WeaponModuleAdapter> bEn;

    static {
        m40504V();
    }

    public WeaponBuff() {
        super((C5540aNg) null);
        super.mo10S();
    }

    public WeaponBuff(C5540aNg ang) {
        super(ang);
    }

    public WeaponBuff(WeaponBuffType ajs) {
        super((C5540aNg) null);
        super._m_script_init(ajs);
    }

    /* renamed from: V */
    static void m40504V() {
        serialVersionUID = (long) 1;
        _m_fieldCount = AttributeBuff._m_fieldCount + 8;
        _m_methodCount = AttributeBuff._m_methodCount + 11;
        int i = AttributeBuff._m_fieldCount;
        C5663aRz[] arzArr = new C5663aRz[(i + 8)];
        C5663aRz b = C5640aRc.m17844b(WeaponBuff.class, "6d0b39325e28ed5270686057929bee71", i);
        bEc = b;
        arzArr[i] = b;
        int i2 = i + 1;
        C5663aRz b2 = C5640aRc.m17844b(WeaponBuff.class, "9785c21e4911bbff3115eabec537c410", i2);
        f9464uS = b2;
        arzArr[i2] = b2;
        int i3 = i2 + 1;
        C5663aRz b3 = C5640aRc.m17844b(WeaponBuff.class, "579045f142993eadb80ddaf77d535afa", i3);
        f9463uN = b3;
        arzArr[i3] = b3;
        int i4 = i3 + 1;
        C5663aRz b4 = C5640aRc.m17844b(WeaponBuff.class, "3a5b84d36d20afdcac81ac58a9b0bfaf", i4);
        bEg = b4;
        arzArr[i4] = b4;
        int i5 = i4 + 1;
        C5663aRz b5 = C5640aRc.m17844b(WeaponBuff.class, "08e94c4f7158ef3c8309ddc65d4465af", i5);
        bEi = b5;
        arzArr[i5] = b5;
        int i6 = i5 + 1;
        C5663aRz b6 = C5640aRc.m17844b(WeaponBuff.class, "fcce36402cbb57e52da1a8c75d207aba", i6);
        bEk = b6;
        arzArr[i6] = b6;
        int i7 = i6 + 1;
        C5663aRz b7 = C5640aRc.m17844b(WeaponBuff.class, "6420b7d0c2d8a7a2ce1d30afddbe81ef", i7);
        bEm = b7;
        arzArr[i7] = b7;
        int i8 = i7 + 1;
        C5663aRz b8 = C5640aRc.m17844b(WeaponBuff.class, "353580931ef706a3e64fe9e71fa6dd91", i8);
        bEo = b8;
        arzArr[i8] = b8;
        int i9 = i8 + 1;
        _m_fields = arzArr;
        C1634Xv.m11725a((Object[]) AttributeBuff._m_fields, (Object[]) _m_fields);
        int i10 = AttributeBuff._m_methodCount;
        C2491fm[] fmVarArr = new C2491fm[(i10 + 11)];
        C2491fm a = C4105zY.m41624a(WeaponBuff.class, "6f473ae6a3d2acec5cf62288bdf28f01", i10);
        aPH = a;
        fmVarArr[i10] = a;
        int i11 = i10 + 1;
        C2491fm a2 = C4105zY.m41624a(WeaponBuff.class, "cd0087af8145513ee83643d0ed179b1a", i11);
        aPI = a2;
        fmVarArr[i11] = a2;
        int i12 = i11 + 1;
        C2491fm a3 = C4105zY.m41624a(WeaponBuff.class, "2f6288548ca525f0c587016a31e8c164", i12);
        bEp = a3;
        fmVarArr[i12] = a3;
        int i13 = i12 + 1;
        C2491fm a4 = C4105zY.m41624a(WeaponBuff.class, "80ff2fd61e00476a77948b0b39e073af", i13);
        bEq = a4;
        fmVarArr[i13] = a4;
        int i14 = i13 + 1;
        C2491fm a5 = C4105zY.m41624a(WeaponBuff.class, "07dc1f78a625c2eaa69a376c5c99fac8", i14);
        bEr = a5;
        fmVarArr[i14] = a5;
        int i15 = i14 + 1;
        C2491fm a6 = C4105zY.m41624a(WeaponBuff.class, "8d2479cc57cd58ae800218851fddb67a", i15);
        bEs = a6;
        fmVarArr[i15] = a6;
        int i16 = i15 + 1;
        C2491fm a7 = C4105zY.m41624a(WeaponBuff.class, "bbd2d618b5063f5df3d65c45388c8a45", i16);
        bEt = a7;
        fmVarArr[i16] = a7;
        int i17 = i16 + 1;
        C2491fm a8 = C4105zY.m41624a(WeaponBuff.class, "366e2240d9938fcb2285ecfe3de47200", i17);
        f9462DK = a8;
        fmVarArr[i17] = a8;
        int i18 = i17 + 1;
        C2491fm a9 = C4105zY.m41624a(WeaponBuff.class, "15863659d7bd0c16fbb3fbd352be26ef", i18);
        bEu = a9;
        fmVarArr[i18] = a9;
        int i19 = i18 + 1;
        C2491fm a10 = C4105zY.m41624a(WeaponBuff.class, "09e823832da8ea08282c2bff73580d7f", i19);
        bEv = a10;
        fmVarArr[i19] = a10;
        int i20 = i19 + 1;
        C2491fm a11 = C4105zY.m41624a(WeaponBuff.class, "64a13d017d215cc0ddb80c28f720c0a3", i20);
        bEw = a11;
        fmVarArr[i20] = a11;
        int i21 = i20 + 1;
        _m_methods = fmVarArr;
        C1634Xv.m11725a((Object[]) AttributeBuff._m_methods, (Object[]) _m_methods);
        ___iScriptClass = aUO.m18566a(WeaponBuff.class, C0326ER.class, _m_fields, _m_methods);
    }

    /* renamed from: B */
    private void m40497B(C3892wO wOVar) {
        throw new C6039afL();
    }

    /* renamed from: C */
    private void m40498C(C3892wO wOVar) {
        throw new C6039afL();
    }

    /* renamed from: D */
    private void m40499D(C3892wO wOVar) {
        throw new C6039afL();
    }

    /* renamed from: E */
    private void m40500E(C3892wO wOVar) {
        throw new C6039afL();
    }

    /* renamed from: F */
    private void m40501F(C3892wO wOVar) {
        throw new C6039afL();
    }

    @C0064Am(aul = "6f473ae6a3d2acec5cf62288bdf28f01", aum = 0)
    @C5566aOg
    /* renamed from: Ue */
    private void m40502Ue() {
        throw new aWi(new aCE(this, aPH, new Object[0]));
    }

    @C0064Am(aul = "cd0087af8145513ee83643d0ed179b1a", aum = 0)
    @C5566aOg
    /* renamed from: Ug */
    private void m40503Ug() {
        throw new aWi(new aCE(this, aPI, new Object[0]));
    }

    private C3892wO amE() {
        return ((WeaponBuffType) getType()).aGy();
    }

    private C3892wO amF() {
        return ((WeaponBuffType) getType()).aGA();
    }

    private C3892wO amG() {
        return ((WeaponBuffType) getType()).aGC();
    }

    private C3892wO amH() {
        return ((WeaponBuffType) getType()).aGE();
    }

    private C3892wO amI() {
        return ((WeaponBuffType) getType()).aGG();
    }

    private boolean amJ() {
        return ((WeaponBuffType) getType()).aGI();
    }

    private boolean amK() {
        return ((WeaponBuffType) getType()).cfI();
    }

    private C1556Wo amL() {
        return (C1556Wo) bFf().mo5608dq().mo3214p(bEo);
    }

    /* renamed from: bC */
    private void m40506bC(boolean z) {
        throw new C6039afL();
    }

    /* renamed from: bD */
    private void m40507bD(boolean z) {
        throw new C6039afL();
    }

    /* renamed from: s */
    private void m40510s(C1556Wo wo) {
        bFf().mo5608dq().mo3197f(bEo, wo);
    }

    /* renamed from: S */
    public void mo10S() {
        super.mo10S();
    }

    /* renamed from: T */
    public C6494any mo11T() {
        return ___iScriptClass;
    }

    /* renamed from: U */
    public C2491fm[] mo12U() {
        return _m_methods;
    }

    @C5566aOg
    /* renamed from: Uf */
    public void mo3751Uf() {
        switch (bFf().mo6893i(aPH)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, aPH, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, aPH, new Object[0]));
                break;
        }
        m40502Ue();
    }

    @C5566aOg
    /* renamed from: Uh */
    public void mo3752Uh() {
        switch (bFf().mo6893i(aPI)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, aPI, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, aPI, new Object[0]));
                break;
        }
        m40503Ug();
    }

    /* renamed from: W */
    public Object mo13W() {
        return new C0326ER(this);
    }

    /* renamed from: a */
    public Object mo14a(C0495Gr gr) {
        Object[] args = gr.getArgs();
        switch (gr.mo2417hq() - AttributeBuff._m_methodCount) {
            case 0:
                m40502Ue();
                return null;
            case 1:
                m40503Ug();
                return null;
            case 2:
                return amM();
            case 3:
                return amO();
            case 4:
                return amQ();
            case 5:
                return amS();
            case 6:
                return amU();
            case 7:
                return new Boolean(m40509mm());
            case 8:
                return new Boolean(amW());
            case 9:
                m40505a((Ship) args[0], (Component) args[1]);
                return null;
            case 10:
                m40508c((Ship) args[0], (Component) args[1]);
                return null;
            default:
                return super.mo14a(gr);
        }
    }

    /* renamed from: a */
    public void mo15a(Collection collection, C0495Gr gr) {
        super.mo15a(collection, gr);
    }

    public C3892wO amN() {
        switch (bFf().mo6893i(bEp)) {
            case 0:
                return null;
            case 2:
                return (C3892wO) bFf().mo5606d(new aCE(this, bEp, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, bEp, new Object[0]));
                break;
        }
        return amM();
    }

    public C3892wO amP() {
        switch (bFf().mo6893i(bEq)) {
            case 0:
                return null;
            case 2:
                return (C3892wO) bFf().mo5606d(new aCE(this, bEq, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, bEq, new Object[0]));
                break;
        }
        return amO();
    }

    public C3892wO amR() {
        switch (bFf().mo6893i(bEr)) {
            case 0:
                return null;
            case 2:
                return (C3892wO) bFf().mo5606d(new aCE(this, bEr, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, bEr, new Object[0]));
                break;
        }
        return amQ();
    }

    public C3892wO amT() {
        switch (bFf().mo6893i(bEs)) {
            case 0:
                return null;
            case 2:
                return (C3892wO) bFf().mo5606d(new aCE(this, bEs, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, bEs, new Object[0]));
                break;
        }
        return amS();
    }

    public C3892wO amV() {
        switch (bFf().mo6893i(bEt)) {
            case 0:
                return null;
            case 2:
                return (C3892wO) bFf().mo5606d(new aCE(this, bEt, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, bEt, new Object[0]));
                break;
        }
        return amU();
    }

    public boolean amX() {
        switch (bFf().mo6893i(bEu)) {
            case 0:
                return false;
            case 2:
                return ((Boolean) bFf().mo5606d(new aCE(this, bEu, new Object[0]))).booleanValue();
            case 3:
                bFf().mo5606d(new aCE(this, bEu, new Object[0]));
                break;
        }
        return amW();
    }

    /* renamed from: b */
    public void mo3926b(Ship fAVar, Component abl) {
        switch (bFf().mo6893i(bEv)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, bEv, new Object[]{fAVar, abl}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, bEv, new Object[]{fAVar, abl}));
                break;
        }
        m40505a(fAVar, abl);
    }

    /* renamed from: c */
    public C5663aRz[] mo25c() {
        return _m_fields;
    }

    /* renamed from: d */
    public void mo3933d(Ship fAVar, Component abl) {
        switch (bFf().mo6893i(bEw)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, bEw, new Object[]{fAVar, abl}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, bEw, new Object[]{fAVar, abl}));
                break;
        }
        m40508c(fAVar, abl);
    }

    /* renamed from: mn */
    public boolean mo22720mn() {
        switch (bFf().mo6893i(f9462DK)) {
            case 0:
                return false;
            case 2:
                return ((Boolean) bFf().mo5606d(new aCE(this, f9462DK, new Object[0]))).booleanValue();
            case 3:
                bFf().mo5606d(new aCE(this, f9462DK, new Object[0]));
                break;
        }
        return m40509mm();
    }

    /* renamed from: a */
    public void mo22713a(WeaponBuffType ajs) {
        super.mo3934i(ajs);
    }

    @C0064Am(aul = "2f6288548ca525f0c587016a31e8c164", aum = 0)
    private C3892wO amM() {
        return amE();
    }

    @C0064Am(aul = "80ff2fd61e00476a77948b0b39e073af", aum = 0)
    private C3892wO amO() {
        return amF();
    }

    @C0064Am(aul = "07dc1f78a625c2eaa69a376c5c99fac8", aum = 0)
    private C3892wO amQ() {
        return amG();
    }

    @C0064Am(aul = "8d2479cc57cd58ae800218851fddb67a", aum = 0)
    private C3892wO amS() {
        return amH();
    }

    @C0064Am(aul = "bbd2d618b5063f5df3d65c45388c8a45", aum = 0)
    private C3892wO amU() {
        return amI();
    }

    @C0064Am(aul = "366e2240d9938fcb2285ecfe3de47200", aum = 0)
    /* renamed from: mm */
    private boolean m40509mm() {
        return amJ();
    }

    @C0064Am(aul = "15863659d7bd0c16fbb3fbd352be26ef", aum = 0)
    private boolean amW() {
        return amK();
    }

    @C0064Am(aul = "09e823832da8ea08282c2bff73580d7f", aum = 0)
    /* renamed from: a */
    private void m40505a(Ship fAVar, Component abl) {
        if (bhP() != null && bhP().equals(fAVar) && (abl instanceof Weapon)) {
            Weapon adv = (Weapon) abl;
            WeaponModuleAdapter aVar = (WeaponModuleAdapter) amL().get(adv);
            if (aVar != null) {
                adv.mo5353TJ().mo23262g(aVar);
            } else {
                mo8358lY("Missing weapon adapter");
            }
        }
    }

    @C0064Am(aul = "64a13d017d215cc0ddb80c28f720c0a3", aum = 0)
    /* renamed from: c */
    private void m40508c(Ship fAVar, Component abl) {
        if (bhP() != null && bhP().equals(fAVar) && (abl instanceof Weapon)) {
            Weapon adv = (Weapon) abl;
            WeaponModuleAdapter aVar = (WeaponModuleAdapter) bFf().mo6865M(WeaponModuleAdapter.class);
            aVar.mo22721b(this);
            adv.mo5353TJ().mo23261e(aVar);
            amL().put(adv, aVar);
        }
    }

    @C6485anp
    @C5511aMd
    /* renamed from: a.wF$a */
    public class WeaponModuleAdapter extends WeaponAdapter implements C1616Xf {

        /* renamed from: AI */
        public static final C2491fm f9465AI = null;

        /* renamed from: DJ */
        public static final C2491fm f9466DJ = null;

        /* renamed from: DK */
        public static final C2491fm f9467DK = null;
        public static final int _m_fieldCount = 0;
        public static final C5663aRz[] _m_fields = null;
        public static final int _m_methodCount = 0;
        public static final C2491fm[] _m_methods = null;
        /* renamed from: aT */
        public static final C5663aRz f9468aT = null;
        public static final long serialVersionUID = 0;
        /* renamed from: vb */
        public static final C2491fm f9469vb = null;
        /* renamed from: vh */
        public static final C2491fm f9470vh = null;
        public static C6494any ___iScriptClass;
        @C0064Am(aul = "162bc276633b6acdf513dbc1f6d8b9d5", aum = 0)
        static /* synthetic */ WeaponBuff bIo;

        static {
            m40524V();
        }

        public WeaponModuleAdapter() {
            super((C5540aNg) null);
            super.mo10S();
        }

        public WeaponModuleAdapter(C5540aNg ang) {
            super(ang);
        }

        public WeaponModuleAdapter(WeaponBuff wFVar) {
            super((C5540aNg) null);
            super._m_script_init(wFVar);
        }

        /* renamed from: V */
        static void m40524V() {
            serialVersionUID = (long) 1;
            _m_fieldCount = WeaponAdapter._m_fieldCount + 1;
            _m_methodCount = WeaponAdapter._m_methodCount + 5;
            int i = WeaponAdapter._m_fieldCount;
            C5663aRz[] arzArr = new C5663aRz[(i + 1)];
            C5663aRz b = C5640aRc.m17844b(WeaponModuleAdapter.class, "162bc276633b6acdf513dbc1f6d8b9d5", i);
            f9468aT = b;
            arzArr[i] = b;
            int i2 = i + 1;
            _m_fields = arzArr;
            C1634Xv.m11725a((Object[]) WeaponAdapter._m_fields, (Object[]) _m_fields);
            int i3 = WeaponAdapter._m_methodCount;
            C2491fm[] fmVarArr = new C2491fm[(i3 + 5)];
            C2491fm a = C4105zY.m41624a(WeaponModuleAdapter.class, "7e5f28238e3f613389ddf46757f3ea7c", i3);
            f9466DJ = a;
            fmVarArr[i3] = a;
            int i4 = i3 + 1;
            C2491fm a2 = C4105zY.m41624a(WeaponModuleAdapter.class, "66f849dd85ca8ef95bdfa1ddd1a9db26", i4);
            f9469vb = a2;
            fmVarArr[i4] = a2;
            int i5 = i4 + 1;
            C2491fm a3 = C4105zY.m41624a(WeaponModuleAdapter.class, "ea7001800b2648f167ec7dfb3fcb80ea", i5);
            f9465AI = a3;
            fmVarArr[i5] = a3;
            int i6 = i5 + 1;
            C2491fm a4 = C4105zY.m41624a(WeaponModuleAdapter.class, "02c7d0f23fc46330985497ec200c3120", i6);
            f9470vh = a4;
            fmVarArr[i6] = a4;
            int i7 = i6 + 1;
            C2491fm a5 = C4105zY.m41624a(WeaponModuleAdapter.class, "348aca54e7c6933453f295642d53548d", i7);
            f9467DK = a5;
            fmVarArr[i7] = a5;
            int i8 = i7 + 1;
            _m_methods = fmVarArr;
            C1634Xv.m11725a((Object[]) WeaponAdapter._m_methods, (Object[]) _m_methods);
            ___iScriptClass = aUO.m18566a(WeaponModuleAdapter.class, C3965xS.class, _m_fields, _m_methods);
        }

        /* renamed from: a */
        private void m40525a(WeaponBuff wFVar) {
            bFf().mo5608dq().mo3197f(f9468aT, wFVar);
        }

        private WeaponBuff cuc() {
            return (WeaponBuff) bFf().mo5608dq().mo3214p(f9468aT);
        }

        /* renamed from: S */
        public void mo10S() {
            super.mo10S();
        }

        /* renamed from: T */
        public C6494any mo11T() {
            return ___iScriptClass;
        }

        /* renamed from: U */
        public C2491fm[] mo12U() {
            return _m_methods;
        }

        /* renamed from: W */
        public Object mo13W() {
            return new C3965xS(this);
        }

        /* renamed from: a */
        public Object mo14a(C0495Gr gr) {
            Object[] args = gr.getArgs();
            switch (gr.mo2417hq() - WeaponAdapter._m_methodCount) {
                case 0:
                    return new Float(m40529mk());
                case 1:
                    return new Float(m40527im());
                case 2:
                    return new Float(m40526c((DamageType) args[0]));
                case 3:
                    return new Float(m40528is());
                case 4:
                    return new Boolean(m40530mm());
                default:
                    return super.mo14a(gr);
            }
        }

        /* renamed from: a */
        public void mo15a(Collection collection, C0495Gr gr) {
            super.mo15a(collection, gr);
        }

        /* renamed from: c */
        public C5663aRz[] mo25c() {
            return _m_fields;
        }

        /* renamed from: d */
        public float mo5375d(DamageType fr) {
            switch (bFf().mo6893i(f9465AI)) {
                case 0:
                    return 0.0f;
                case 2:
                    return ((Float) bFf().mo5606d(new aCE(this, f9465AI, new Object[]{fr}))).floatValue();
                case 3:
                    bFf().mo5606d(new aCE(this, f9465AI, new Object[]{fr}));
                    break;
            }
            return m40526c(fr);
        }

        public float getSpeed() {
            switch (bFf().mo6893i(f9470vh)) {
                case 0:
                    return 0.0f;
                case 2:
                    return ((Float) bFf().mo5606d(new aCE(this, f9470vh, new Object[0]))).floatValue();
                case 3:
                    bFf().mo5606d(new aCE(this, f9470vh, new Object[0]));
                    break;
            }
            return m40528is();
        }

        /* renamed from: in */
        public float mo12953in() {
            switch (bFf().mo6893i(f9469vb)) {
                case 0:
                    return 0.0f;
                case 2:
                    return ((Float) bFf().mo5606d(new aCE(this, f9469vb, new Object[0]))).floatValue();
                case 3:
                    bFf().mo5606d(new aCE(this, f9469vb, new Object[0]));
                    break;
            }
            return m40527im();
        }

        /* renamed from: ml */
        public float mo12954ml() {
            switch (bFf().mo6893i(f9466DJ)) {
                case 0:
                    return 0.0f;
                case 2:
                    return ((Float) bFf().mo5606d(new aCE(this, f9466DJ, new Object[0]))).floatValue();
                case 3:
                    bFf().mo5606d(new aCE(this, f9466DJ, new Object[0]));
                    break;
            }
            return m40529mk();
        }

        /* renamed from: mn */
        public boolean mo12955mn() {
            switch (bFf().mo6893i(f9467DK)) {
                case 0:
                    return false;
                case 2:
                    return ((Boolean) bFf().mo5606d(new aCE(this, f9467DK, new Object[0]))).booleanValue();
                case 3:
                    bFf().mo5606d(new aCE(this, f9467DK, new Object[0]));
                    break;
            }
            return m40530mm();
        }

        /* renamed from: b */
        public void mo22721b(WeaponBuff wFVar) {
            m40525a(wFVar);
            super.mo10S();
        }

        @C0064Am(aul = "7e5f28238e3f613389ddf46757f3ea7c", aum = 0)
        /* renamed from: mk */
        private float m40529mk() {
            float v = cuc().amN().mo22753v(((WeaponAdapter) mo16070IE()).mo12954ml(), super.mo12954ml());
            if (v > 10.0f) {
                return 10.0f;
            }
            return v;
        }

        @C0064Am(aul = "66f849dd85ca8ef95bdfa1ddd1a9db26", aum = 0)
        /* renamed from: im */
        private float m40527im() {
            return cuc().amR().mo22753v(((WeaponAdapter) mo16070IE()).mo12953in(), super.mo12953in());
        }

        @C0064Am(aul = "ea7001800b2648f167ec7dfb3fcb80ea", aum = 0)
        /* renamed from: c */
        private float m40526c(DamageType fr) {
            return cuc().amT().mo22753v(((WeaponAdapter) mo16070IE()).mo5375d(fr), super.mo5375d(fr));
        }

        @C0064Am(aul = "02c7d0f23fc46330985497ec200c3120", aum = 0)
        /* renamed from: is */
        private float m40528is() {
            return cuc().amP().mo22753v(((WeaponAdapter) mo16070IE()).getSpeed(), super.getSpeed());
        }

        @C0064Am(aul = "348aca54e7c6933453f295642d53548d", aum = 0)
        /* renamed from: mm */
        private boolean m40530mm() {
            return cuc().mo22720mn() || super.mo12955mn();
        }
    }
}
