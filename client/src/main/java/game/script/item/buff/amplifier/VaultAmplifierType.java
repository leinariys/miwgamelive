package game.script.item.buff.amplifier;

import game.network.message.externalizable.aCE;
import logic.baa.*;
import logic.data.mbean.C5943adT;
import logic.res.code.C5663aRz;
import logic.res.code.C6494any;
import logic.res.html.C2491fm;
import p001a.*;

import java.util.Collection;

@C0566Hp
@C5511aMd
@C6485anp
/* renamed from: a.aqY  reason: case insensitive filesystem */
/* compiled from: a */
public class VaultAmplifierType extends CargoHoldAmplifierType implements C1616Xf {
    public static final int _m_fieldCount = 0;
    public static final C5663aRz[] _m_fields = null;
    public static final int _m_methodCount = 0;
    public static final C2491fm[] _m_methods = null;
    /* renamed from: dN */
    public static final C2491fm f5175dN = null;
    public static final C2491fm grm = null;
    public static final long serialVersionUID = 0;
    public static C6494any ___iScriptClass;

    static {
        m25138V();
    }

    public VaultAmplifierType() {
        super((C5540aNg) null);
        super.mo10S();
    }

    public VaultAmplifierType(C5540aNg ang) {
        super(ang);
    }

    /* renamed from: V */
    static void m25138V() {
        serialVersionUID = (long) 1;
        _m_fieldCount = CargoHoldAmplifierType._m_fieldCount + 0;
        _m_methodCount = CargoHoldAmplifierType._m_methodCount + 2;
        _m_fields = new C5663aRz[(CargoHoldAmplifierType._m_fieldCount + 0)];
        C1634Xv.m11725a((Object[]) CargoHoldAmplifierType._m_fields, (Object[]) _m_fields);
        int i = CargoHoldAmplifierType._m_methodCount;
        C2491fm[] fmVarArr = new C2491fm[(i + 2)];
        C2491fm a = C4105zY.m41624a(VaultAmplifierType.class, "c40c022aebcd397e5324c815a0fbcaeb", i);
        grm = a;
        fmVarArr[i] = a;
        int i2 = i + 1;
        C2491fm a2 = C4105zY.m41624a(VaultAmplifierType.class, "64071a47a35452d132cb567f6cc5f01d", i2);
        f5175dN = a2;
        fmVarArr[i2] = a2;
        int i3 = i2 + 1;
        _m_methods = fmVarArr;
        C1634Xv.m11725a((Object[]) CargoHoldAmplifierType._m_methods, (Object[]) _m_methods);
        ___iScriptClass = aUO.m18566a(VaultAmplifierType.class, C5943adT.class, _m_fields, _m_methods);
    }

    /* renamed from: T */
    public C6494any mo11T() {
        return ___iScriptClass;
    }

    /* renamed from: U */
    public C2491fm[] mo12U() {
        return _m_methods;
    }

    /* renamed from: W */
    public Object mo13W() {
        return new C5943adT(this);
    }

    /* renamed from: a */
    public Object mo14a(C0495Gr gr) {
        gr.getArgs();
        switch (gr.mo2417hq() - CargoHoldAmplifierType._m_methodCount) {
            case 0:
                return crN();
            case 1:
                return m25139aT();
            default:
                return super.mo14a(gr);
        }
    }

    /* renamed from: a */
    public void mo15a(Collection collection, C0495Gr gr) {
        super.mo15a(collection, gr);
    }

    /* access modifiers changed from: protected */
    /* renamed from: aU */
    public <T extends aDJ> T mo745aU() {
        switch (bFf().mo6893i(f5175dN)) {
            case 0:
                return null;
            case 2:
                return (aDJ) bFf().mo5606d(new aCE(this, f5175dN, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f5175dN, new Object[0]));
                break;
        }
        return m25139aT();
    }

    /* renamed from: c */
    public C5663aRz[] mo25c() {
        return _m_fields;
    }

    public VaultAmplifier crO() {
        switch (bFf().mo6893i(grm)) {
            case 0:
                return null;
            case 2:
                return (VaultAmplifier) bFf().mo5606d(new aCE(this, grm, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, grm, new Object[0]));
                break;
        }
        return crN();
    }

    /* renamed from: S */
    public void mo10S() {
        super.mo10S();
    }

    @C0064Am(aul = "c40c022aebcd397e5324c815a0fbcaeb", aum = 0)
    private VaultAmplifier crN() {
        return (VaultAmplifier) mo745aU();
    }

    @C0064Am(aul = "64071a47a35452d132cb567f6cc5f01d", aum = 0)
    /* renamed from: aT */
    private <T extends aDJ> T m25139aT() {
        T t = (VaultAmplifier) bFf().mo6865M(VaultAmplifier.class);
        t.mo1956e(this);
        return t;
    }
}
