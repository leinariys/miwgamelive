package game.script.item.buff.amplifier;

import game.network.message.externalizable.aCE;
import game.network.message.serializable.C2686iZ;
import game.script.item.AmplifierType;
import game.script.item.ItemTypeCategory;
import game.script.item.WeaponType;
import logic.baa.*;
import logic.data.mbean.C0132Bd;
import logic.res.code.C5640aRc;
import logic.res.code.C5663aRz;
import logic.res.code.C6494any;
import logic.res.html.C2491fm;
import p001a.*;

import java.util.Collection;
import java.util.Collections;
import java.util.Set;

@C0566Hp
@C5829abJ("2.0.1")
@C6485anp
@C5511aMd
/* renamed from: a.DB */
/* compiled from: a */
public class WeaponAmplifierType extends AmplifierType implements C1616Xf {

    /* renamed from: OY */
    public static final C5663aRz f383OY = null;

    /* renamed from: Pj */
    public static final C2491fm f384Pj = null;

    /* renamed from: Pk */
    public static final C2491fm f385Pk = null;
    public static final int _m_fieldCount = 0;
    public static final C5663aRz[] _m_fields = null;
    public static final int _m_methodCount = 0;
    public static final C2491fm[] _m_methods = null;
    public static final C5663aRz bEc = null;
    public static final C5663aRz bEg = null;
    public static final C5663aRz bEi = null;
    public static final C5663aRz bEk = null;
    public static final C5663aRz cHK = null;
    public static final C2491fm cHL = null;
    public static final C2491fm cHM = null;
    public static final C2491fm cHN = null;
    public static final C2491fm cHO = null;
    public static final C2491fm cHP = null;
    public static final C2491fm cHQ = null;
    public static final C2491fm cHR = null;
    public static final C2491fm cHS = null;
    public static final C2491fm cHT = null;
    public static final C2491fm cHU = null;
    public static final C2491fm cHV = null;
    public static final C2491fm cHW = null;
    public static final C2491fm cHX = null;
    public static final C2491fm cHY = null;
    public static final C2491fm cHZ = null;
    public static final C2491fm cIa = null;
    public static final C2491fm cIb = null;
    public static final C2491fm cIc = null;
    public static final C2491fm cId = null;
    /* renamed from: dN */
    public static final C2491fm f386dN = null;
    public static final long serialVersionUID = 0;
    /* renamed from: uN */
    public static final C5663aRz f387uN = null;
    /* renamed from: uS */
    public static final C5663aRz f388uS = null;
    public static C6494any ___iScriptClass;
    @C0064Am(aul = "76aab741fad8a5e7becb565cad646949", aum = 0)
    private static C3892wO bEb;
    @C0064Am(aul = "b51de4f5fa3fae70520caa69561c3936", aum = 1)
    private static C3892wO bEd;
    @C0064Am(aul = "11289e64e55cbaf87a4e24730b506272", aum = 2)
    private static C3892wO bEe;
    @C0064Am(aul = "bf1ceb14f7d57ff6f24f94b7495cf82c", aum = 3)
    private static C3892wO bEf;
    @C0064Am(aul = "405d29460bb158ff3967216f6c0c5e20", aum = 4)
    private static C3892wO bEh;
    @C0064Am(aul = "db5737890e3f1a0d5135ab5c975d758f", aum = 5)
    private static boolean bEj;
    @C0064Am(aul = "cd217918f511282ad2b9d6c32e9e0854", aum = 6)
    private static C2686iZ<ItemTypeCategory> ckK;
    @C0064Am(aul = "ce3daddbe423a09e18614de24420bc31", aum = 7)
    private static C2686iZ<WeaponType> ckL;

    static {
        m1899V();
    }

    public WeaponAmplifierType() {
        super((C5540aNg) null);
        super.mo10S();
    }

    public WeaponAmplifierType(C5540aNg ang) {
        super(ang);
    }

    /* renamed from: V */
    static void m1899V() {
        serialVersionUID = (long) 1;
        _m_fieldCount = AmplifierType._m_fieldCount + 8;
        _m_methodCount = AmplifierType._m_methodCount + 22;
        int i = AmplifierType._m_fieldCount;
        C5663aRz[] arzArr = new C5663aRz[(i + 8)];
        C5663aRz b = C5640aRc.m17844b(WeaponAmplifierType.class, "76aab741fad8a5e7becb565cad646949", i);
        bEc = b;
        arzArr[i] = b;
        int i2 = i + 1;
        C5663aRz b2 = C5640aRc.m17844b(WeaponAmplifierType.class, "b51de4f5fa3fae70520caa69561c3936", i2);
        f388uS = b2;
        arzArr[i2] = b2;
        int i3 = i2 + 1;
        C5663aRz b3 = C5640aRc.m17844b(WeaponAmplifierType.class, "11289e64e55cbaf87a4e24730b506272", i3);
        f387uN = b3;
        arzArr[i3] = b3;
        int i4 = i3 + 1;
        C5663aRz b4 = C5640aRc.m17844b(WeaponAmplifierType.class, "bf1ceb14f7d57ff6f24f94b7495cf82c", i4);
        bEg = b4;
        arzArr[i4] = b4;
        int i5 = i4 + 1;
        C5663aRz b5 = C5640aRc.m17844b(WeaponAmplifierType.class, "405d29460bb158ff3967216f6c0c5e20", i5);
        bEi = b5;
        arzArr[i5] = b5;
        int i6 = i5 + 1;
        C5663aRz b6 = C5640aRc.m17844b(WeaponAmplifierType.class, "db5737890e3f1a0d5135ab5c975d758f", i6);
        bEk = b6;
        arzArr[i6] = b6;
        int i7 = i6 + 1;
        C5663aRz b7 = C5640aRc.m17844b(WeaponAmplifierType.class, "cd217918f511282ad2b9d6c32e9e0854", i7);
        cHK = b7;
        arzArr[i7] = b7;
        int i8 = i7 + 1;
        C5663aRz b8 = C5640aRc.m17844b(WeaponAmplifierType.class, "ce3daddbe423a09e18614de24420bc31", i8);
        f383OY = b8;
        arzArr[i8] = b8;
        int i9 = i8 + 1;
        _m_fields = arzArr;
        C1634Xv.m11725a((Object[]) AmplifierType._m_fields, (Object[]) _m_fields);
        int i10 = AmplifierType._m_methodCount;
        C2491fm[] fmVarArr = new C2491fm[(i10 + 22)];
        C2491fm a = C4105zY.m41624a(WeaponAmplifierType.class, "af4c419be60c7a5938d053a8f31b6b20", i10);
        cHL = a;
        fmVarArr[i10] = a;
        int i11 = i10 + 1;
        C2491fm a2 = C4105zY.m41624a(WeaponAmplifierType.class, "d7d0c7727aed717999849e1ec0e53b05", i11);
        cHM = a2;
        fmVarArr[i11] = a2;
        int i12 = i11 + 1;
        C2491fm a3 = C4105zY.m41624a(WeaponAmplifierType.class, "bf97521d00f6709b288d7afc20c0d534", i12);
        cHN = a3;
        fmVarArr[i12] = a3;
        int i13 = i12 + 1;
        C2491fm a4 = C4105zY.m41624a(WeaponAmplifierType.class, "9b7bfe0d7ffe1aa0157fb80bc1ce5a8d", i13);
        cHO = a4;
        fmVarArr[i13] = a4;
        int i14 = i13 + 1;
        C2491fm a5 = C4105zY.m41624a(WeaponAmplifierType.class, "be237c95c2165af074aaa6a9896795d3", i14);
        cHP = a5;
        fmVarArr[i14] = a5;
        int i15 = i14 + 1;
        C2491fm a6 = C4105zY.m41624a(WeaponAmplifierType.class, "332aa731199dc325fe77ba59f4c2abac", i15);
        cHQ = a6;
        fmVarArr[i15] = a6;
        int i16 = i15 + 1;
        C2491fm a7 = C4105zY.m41624a(WeaponAmplifierType.class, "ab3e7af141751b987f63afced93c209e", i16);
        cHR = a7;
        fmVarArr[i16] = a7;
        int i17 = i16 + 1;
        C2491fm a8 = C4105zY.m41624a(WeaponAmplifierType.class, "c2df5e835ce58acc739f41e351477a4d", i17);
        cHS = a8;
        fmVarArr[i17] = a8;
        int i18 = i17 + 1;
        C2491fm a9 = C4105zY.m41624a(WeaponAmplifierType.class, "30698bf43dac5a32d672b987a6dd9ff1", i18);
        cHT = a9;
        fmVarArr[i18] = a9;
        int i19 = i18 + 1;
        C2491fm a10 = C4105zY.m41624a(WeaponAmplifierType.class, "2d80c9968d3083257b8f070a4c61bcde", i19);
        cHU = a10;
        fmVarArr[i19] = a10;
        int i20 = i19 + 1;
        C2491fm a11 = C4105zY.m41624a(WeaponAmplifierType.class, "923f011c9f1b1944e6e7c788892c790a", i20);
        cHV = a11;
        fmVarArr[i20] = a11;
        int i21 = i20 + 1;
        C2491fm a12 = C4105zY.m41624a(WeaponAmplifierType.class, "f0be1385fc035fade4b71dd382633eeb", i21);
        cHW = a12;
        fmVarArr[i21] = a12;
        int i22 = i21 + 1;
        C2491fm a13 = C4105zY.m41624a(WeaponAmplifierType.class, "c365b59d977a356686114030c2074572", i22);
        cHX = a13;
        fmVarArr[i22] = a13;
        int i23 = i22 + 1;
        C2491fm a14 = C4105zY.m41624a(WeaponAmplifierType.class, "d4f7f830063fdec9870a1f4e0a918b5c", i23);
        cHY = a14;
        fmVarArr[i23] = a14;
        int i24 = i23 + 1;
        C2491fm a15 = C4105zY.m41624a(WeaponAmplifierType.class, "0f38a96ecd67e6b14935b4f50ecdce15", i24);
        cHZ = a15;
        fmVarArr[i24] = a15;
        int i25 = i24 + 1;
        C2491fm a16 = C4105zY.m41624a(WeaponAmplifierType.class, "2f5ff9e49c1717311fba52c934580687", i25);
        cIa = a16;
        fmVarArr[i25] = a16;
        int i26 = i25 + 1;
        C2491fm a17 = C4105zY.m41624a(WeaponAmplifierType.class, "0a53447e22cbca68778a21de52dd23f2", i26);
        f384Pj = a17;
        fmVarArr[i26] = a17;
        int i27 = i26 + 1;
        C2491fm a18 = C4105zY.m41624a(WeaponAmplifierType.class, "b5033d7d6fdfe1f89d7591f8688a6bcd", i27);
        f385Pk = a18;
        fmVarArr[i27] = a18;
        int i28 = i27 + 1;
        C2491fm a19 = C4105zY.m41624a(WeaponAmplifierType.class, "8cd8ad7b4c595d9ed444c47db6a6cd6e", i28);
        cIb = a19;
        fmVarArr[i28] = a19;
        int i29 = i28 + 1;
        C2491fm a20 = C4105zY.m41624a(WeaponAmplifierType.class, "82fc6a87923bca3980e84902f1fa5071", i29);
        cIc = a20;
        fmVarArr[i29] = a20;
        int i30 = i29 + 1;
        C2491fm a21 = C4105zY.m41624a(WeaponAmplifierType.class, "61d559b312067ebe11d1172ca3e14528", i30);
        cId = a21;
        fmVarArr[i30] = a21;
        int i31 = i30 + 1;
        C2491fm a22 = C4105zY.m41624a(WeaponAmplifierType.class, "a6adaade5d4664b32e5825d194521eaa", i31);
        f386dN = a22;
        fmVarArr[i31] = a22;
        int i32 = i31 + 1;
        _m_methods = fmVarArr;
        C1634Xv.m11725a((Object[]) AmplifierType._m_methods, (Object[]) _m_methods);
        ___iScriptClass = aUO.m18566a(WeaponAmplifierType.class, C0132Bd.class, _m_fields, _m_methods);
    }

    /* renamed from: B */
    private void m1889B(C3892wO wOVar) {
        bFf().mo5608dq().mo3197f(bEc, wOVar);
    }

    /* renamed from: C */
    private void m1890C(C3892wO wOVar) {
        bFf().mo5608dq().mo3197f(f388uS, wOVar);
    }

    /* renamed from: D */
    private void m1891D(C3892wO wOVar) {
        bFf().mo5608dq().mo3197f(f387uN, wOVar);
    }

    /* renamed from: E */
    private void m1892E(C3892wO wOVar) {
        bFf().mo5608dq().mo3197f(bEg, wOVar);
    }

    /* renamed from: F */
    private void m1893F(C3892wO wOVar) {
        bFf().mo5608dq().mo3197f(bEi, wOVar);
    }

    private C2686iZ aGv() {
        return (C2686iZ) bFf().mo5608dq().mo3214p(cHK);
    }

    private C2686iZ aGw() {
        return (C2686iZ) bFf().mo5608dq().mo3214p(f383OY);
    }

    private C3892wO amE() {
        return (C3892wO) bFf().mo5608dq().mo3214p(bEc);
    }

    private C3892wO amF() {
        return (C3892wO) bFf().mo5608dq().mo3214p(f388uS);
    }

    private C3892wO amG() {
        return (C3892wO) bFf().mo5608dq().mo3214p(f387uN);
    }

    private C3892wO amH() {
        return (C3892wO) bFf().mo5608dq().mo3214p(bEg);
    }

    private C3892wO amI() {
        return (C3892wO) bFf().mo5608dq().mo3214p(bEi);
    }

    private boolean amJ() {
        return bFf().mo5608dq().mo3201h(bEk);
    }

    /* renamed from: bC */
    private void m1902bC(boolean z) {
        bFf().mo5608dq().mo3153a(bEk, z);
    }

    /* renamed from: y */
    private void m1907y(C2686iZ iZVar) {
        bFf().mo5608dq().mo3197f(cHK, iZVar);
    }

    /* renamed from: z */
    private void m1908z(C2686iZ iZVar) {
        bFf().mo5608dq().mo3197f(f383OY, iZVar);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Fire Rate")
    /* renamed from: L */
    public void mo1201L(C3892wO wOVar) {
        switch (bFf().mo6893i(cHM)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, cHM, new Object[]{wOVar}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, cHM, new Object[]{wOVar}));
                break;
        }
        m1894K(wOVar);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Speed")
    /* renamed from: N */
    public void mo1202N(C3892wO wOVar) {
        switch (bFf().mo6893i(cHO)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, cHO, new Object[]{wOVar}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, cHO, new Object[]{wOVar}));
                break;
        }
        m1895M(wOVar);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Range")
    /* renamed from: P */
    public void mo1203P(C3892wO wOVar) {
        switch (bFf().mo6893i(cHQ)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, cHQ, new Object[]{wOVar}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, cHQ, new Object[]{wOVar}));
                break;
        }
        m1896O(wOVar);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Shield Damage")
    /* renamed from: R */
    public void mo1204R(C3892wO wOVar) {
        switch (bFf().mo6893i(cHS)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, cHS, new Object[]{wOVar}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, cHS, new Object[]{wOVar}));
                break;
        }
        m1897Q(wOVar);
    }

    /* renamed from: T */
    public C6494any mo11T() {
        return ___iScriptClass;
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Hull Damage")
    /* renamed from: T */
    public void mo1205T(C3892wO wOVar) {
        switch (bFf().mo6893i(cHU)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, cHU, new Object[]{wOVar}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, cHU, new Object[]{wOVar}));
                break;
        }
        m1898S(wOVar);
    }

    /* renamed from: U */
    public C2491fm[] mo12U() {
        return _m_methods;
    }

    /* renamed from: W */
    public Object mo13W() {
        return new C0132Bd(this);
    }

    /* renamed from: a */
    public Object mo14a(C0495Gr gr) {
        Object[] args = gr.getArgs();
        switch (gr.mo2417hq() - AmplifierType._m_methodCount) {
            case 0:
                return aGx();
            case 1:
                m1894K((C3892wO) args[0]);
                return null;
            case 2:
                return aGz();
            case 3:
                m1895M((C3892wO) args[0]);
                return null;
            case 4:
                return aGB();
            case 5:
                m1896O((C3892wO) args[0]);
                return null;
            case 6:
                return aGD();
            case 7:
                m1897Q((C3892wO) args[0]);
                return null;
            case 8:
                return aGF();
            case 9:
                m1898S((C3892wO) args[0]);
                return null;
            case 10:
                return new Boolean(aGH());
            case 11:
                m1904cA(((Boolean) args[0]).booleanValue());
                return null;
            case 12:
                m1905f((ItemTypeCategory) args[0]);
                return null;
            case 13:
                m1906h((ItemTypeCategory) args[0]);
                return null;
            case 14:
                return aGJ();
            case 15:
                return aGL();
            case 16:
                m1900a((WeaponType) args[0]);
                return null;
            case 17:
                m1903c((WeaponType) args[0]);
                return null;
            case 18:
                return aGN();
            case 19:
                return aGP();
            case 20:
                return aGR();
            case 21:
                return m1901aT();
            default:
                return super.mo14a(gr);
        }
    }

    /* renamed from: a */
    public void mo15a(Collection collection, C0495Gr gr) {
        super.mo15a(collection, gr);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Speed")
    public C3892wO aGA() {
        switch (bFf().mo6893i(cHN)) {
            case 0:
                return null;
            case 2:
                return (C3892wO) bFf().mo5606d(new aCE(this, cHN, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, cHN, new Object[0]));
                break;
        }
        return aGz();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Range")
    public C3892wO aGC() {
        switch (bFf().mo6893i(cHP)) {
            case 0:
                return null;
            case 2:
                return (C3892wO) bFf().mo5606d(new aCE(this, cHP, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, cHP, new Object[0]));
                break;
        }
        return aGB();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Shield Damage")
    public C3892wO aGE() {
        switch (bFf().mo6893i(cHR)) {
            case 0:
                return null;
            case 2:
                return (C3892wO) bFf().mo5606d(new aCE(this, cHR, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, cHR, new Object[0]));
                break;
        }
        return aGD();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Hull Damage")
    public C3892wO aGG() {
        switch (bFf().mo6893i(cHT)) {
            case 0:
                return null;
            case 2:
                return (C3892wO) bFf().mo5606d(new aCE(this, cHT, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, cHT, new Object[0]));
                break;
        }
        return aGF();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Double Shot")
    public boolean aGI() {
        switch (bFf().mo6893i(cHV)) {
            case 0:
                return false;
            case 2:
                return ((Boolean) bFf().mo5606d(new aCE(this, cHV, new Object[0]))).booleanValue();
            case 3:
                bFf().mo5606d(new aCE(this, cHV, new Object[0]));
                break;
        }
        return aGH();
    }

    public C2686iZ<ItemTypeCategory> aGK() {
        switch (bFf().mo6893i(cHZ)) {
            case 0:
                return null;
            case 2:
                return (C2686iZ) bFf().mo5606d(new aCE(this, cHZ, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, cHZ, new Object[0]));
                break;
        }
        return aGJ();
    }

    @C3248pc(aYR = C3248pc.C3250b.ITERATOR, displayName = "Category Types")
    public Set<ItemTypeCategory> aGM() {
        switch (bFf().mo6893i(cIa)) {
            case 0:
                return null;
            case 2:
                return (Set) bFf().mo5606d(new aCE(this, cIa, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, cIa, new Object[0]));
                break;
        }
        return aGL();
    }

    public C2686iZ<WeaponType> aGO() {
        switch (bFf().mo6893i(cIb)) {
            case 0:
                return null;
            case 2:
                return (C2686iZ) bFf().mo5606d(new aCE(this, cIb, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, cIb, new Object[0]));
                break;
        }
        return aGN();
    }

    @C3248pc(aYR = C3248pc.C3250b.ITERATOR, displayName = "Weapon Types")
    public Set<WeaponType> aGQ() {
        switch (bFf().mo6893i(cIc)) {
            case 0:
                return null;
            case 2:
                return (Set) bFf().mo5606d(new aCE(this, cIc, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, cIc, new Object[0]));
                break;
        }
        return aGP();
    }

    public WeaponAmplifier aGS() {
        switch (bFf().mo6893i(cId)) {
            case 0:
                return null;
            case 2:
                return (WeaponAmplifier) bFf().mo5606d(new aCE(this, cId, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, cId, new Object[0]));
                break;
        }
        return aGR();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Fire Rate")
    public C3892wO aGy() {
        switch (bFf().mo6893i(cHL)) {
            case 0:
                return null;
            case 2:
                return (C3892wO) bFf().mo5606d(new aCE(this, cHL, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, cHL, new Object[0]));
                break;
        }
        return aGx();
    }

    /* access modifiers changed from: protected */
    /* renamed from: aU */
    public <T extends aDJ> T mo745aU() {
        switch (bFf().mo6893i(f386dN)) {
            case 0:
                return null;
            case 2:
                return (aDJ) bFf().mo5606d(new aCE(this, f386dN, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f386dN, new Object[0]));
                break;
        }
        return m1901aT();
    }

    @C3248pc(aYR = C3248pc.C3250b.ADDER, displayName = "Weapon Types")
    /* renamed from: b */
    public void mo1217b(WeaponType apt) {
        switch (bFf().mo6893i(f384Pj)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f384Pj, new Object[]{apt}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f384Pj, new Object[]{apt}));
                break;
        }
        m1900a(apt);
    }

    /* renamed from: c */
    public C5663aRz[] mo25c() {
        return _m_fields;
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Double Shot")
    /* renamed from: cB */
    public void mo1218cB(boolean z) {
        switch (bFf().mo6893i(cHW)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, cHW, new Object[]{new Boolean(z)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, cHW, new Object[]{new Boolean(z)}));
                break;
        }
        m1904cA(z);
    }

    @C3248pc(aYR = C3248pc.C3250b.REMOVER, displayName = "Weapon Types")
    /* renamed from: d */
    public void mo1219d(WeaponType apt) {
        switch (bFf().mo6893i(f385Pk)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f385Pk, new Object[]{apt}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f385Pk, new Object[]{apt}));
                break;
        }
        m1903c(apt);
    }

    @C3248pc(aYR = C3248pc.C3250b.ADDER, displayName = "Category Types")
    /* renamed from: g */
    public void mo1220g(ItemTypeCategory aai) {
        switch (bFf().mo6893i(cHX)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, cHX, new Object[]{aai}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, cHX, new Object[]{aai}));
                break;
        }
        m1905f(aai);
    }

    @C3248pc(aYR = C3248pc.C3250b.REMOVER, displayName = "Category Types")
    /* renamed from: i */
    public void mo1221i(ItemTypeCategory aai) {
        switch (bFf().mo6893i(cHY)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, cHY, new Object[]{aai}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, cHY, new Object[]{aai}));
                break;
        }
        m1906h(aai);
    }

    /* renamed from: S */
    public void mo10S() {
        super.mo10S();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Fire Rate")
    @C0064Am(aul = "af4c419be60c7a5938d053a8f31b6b20", aum = 0)
    private C3892wO aGx() {
        return amE();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Fire Rate")
    @C0064Am(aul = "d7d0c7727aed717999849e1ec0e53b05", aum = 0)
    /* renamed from: K */
    private void m1894K(C3892wO wOVar) {
        m1889B(wOVar);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Speed")
    @C0064Am(aul = "bf97521d00f6709b288d7afc20c0d534", aum = 0)
    private C3892wO aGz() {
        return amF();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Speed")
    @C0064Am(aul = "9b7bfe0d7ffe1aa0157fb80bc1ce5a8d", aum = 0)
    /* renamed from: M */
    private void m1895M(C3892wO wOVar) {
        m1890C(wOVar);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Range")
    @C0064Am(aul = "be237c95c2165af074aaa6a9896795d3", aum = 0)
    private C3892wO aGB() {
        return amG();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Range")
    @C0064Am(aul = "332aa731199dc325fe77ba59f4c2abac", aum = 0)
    /* renamed from: O */
    private void m1896O(C3892wO wOVar) {
        m1891D(wOVar);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Shield Damage")
    @C0064Am(aul = "ab3e7af141751b987f63afced93c209e", aum = 0)
    private C3892wO aGD() {
        return amH();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Shield Damage")
    @C0064Am(aul = "c2df5e835ce58acc739f41e351477a4d", aum = 0)
    /* renamed from: Q */
    private void m1897Q(C3892wO wOVar) {
        m1892E(wOVar);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Hull Damage")
    @C0064Am(aul = "30698bf43dac5a32d672b987a6dd9ff1", aum = 0)
    private C3892wO aGF() {
        return amI();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Hull Damage")
    @C0064Am(aul = "2d80c9968d3083257b8f070a4c61bcde", aum = 0)
    /* renamed from: S */
    private void m1898S(C3892wO wOVar) {
        m1893F(wOVar);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Double Shot")
    @C0064Am(aul = "923f011c9f1b1944e6e7c788892c790a", aum = 0)
    private boolean aGH() {
        return amJ();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Double Shot")
    @C0064Am(aul = "f0be1385fc035fade4b71dd382633eeb", aum = 0)
    /* renamed from: cA */
    private void m1904cA(boolean z) {
        m1902bC(z);
    }

    @C3248pc(aYR = C3248pc.C3250b.ADDER, displayName = "Category Types")
    @C0064Am(aul = "c365b59d977a356686114030c2074572", aum = 0)
    /* renamed from: f */
    private void m1905f(ItemTypeCategory aai) {
        aGv().add(aai);
    }

    @C3248pc(aYR = C3248pc.C3250b.REMOVER, displayName = "Category Types")
    @C0064Am(aul = "d4f7f830063fdec9870a1f4e0a918b5c", aum = 0)
    /* renamed from: h */
    private void m1906h(ItemTypeCategory aai) {
        aGv().remove(aai);
    }

    @C0064Am(aul = "0f38a96ecd67e6b14935b4f50ecdce15", aum = 0)
    private C2686iZ<ItemTypeCategory> aGJ() {
        return aGv();
    }

    @C3248pc(aYR = C3248pc.C3250b.ITERATOR, displayName = "Category Types")
    @C0064Am(aul = "2f5ff9e49c1717311fba52c934580687", aum = 0)
    private Set<ItemTypeCategory> aGL() {
        return Collections.unmodifiableSet(aGv());
    }

    @C3248pc(aYR = C3248pc.C3250b.ADDER, displayName = "Weapon Types")
    @C0064Am(aul = "0a53447e22cbca68778a21de52dd23f2", aum = 0)
    /* renamed from: a */
    private void m1900a(WeaponType apt) {
        aGw().add(apt);
    }

    @C3248pc(aYR = C3248pc.C3250b.REMOVER, displayName = "Weapon Types")
    @C0064Am(aul = "b5033d7d6fdfe1f89d7591f8688a6bcd", aum = 0)
    /* renamed from: c */
    private void m1903c(WeaponType apt) {
        aGw().remove(apt);
    }

    @C0064Am(aul = "8cd8ad7b4c595d9ed444c47db6a6cd6e", aum = 0)
    private C2686iZ<WeaponType> aGN() {
        return aGw();
    }

    @C3248pc(aYR = C3248pc.C3250b.ITERATOR, displayName = "Weapon Types")
    @C0064Am(aul = "82fc6a87923bca3980e84902f1fa5071", aum = 0)
    private Set<WeaponType> aGP() {
        return Collections.unmodifiableSet(aGw());
    }

    @C0064Am(aul = "61d559b312067ebe11d1172ca3e14528", aum = 0)
    private WeaponAmplifier aGR() {
        return (WeaponAmplifier) mo745aU();
    }

    @C0064Am(aul = "a6adaade5d4664b32e5825d194521eaa", aum = 0)
    /* renamed from: aT */
    private <T extends aDJ> T m1901aT() {
        T t = (WeaponAmplifier) bFf().mo6865M(WeaponAmplifier.class);
        t.mo1956e(this);
        return t;
    }
}
