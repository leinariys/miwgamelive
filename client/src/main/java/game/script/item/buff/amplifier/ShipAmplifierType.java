package game.script.item.buff.amplifier;

import game.network.message.externalizable.aCE;
import game.script.item.AmplifierType;
import logic.baa.*;
import logic.data.mbean.C1464VY;
import logic.res.code.C5640aRc;
import logic.res.code.C5663aRz;
import logic.res.code.C6494any;
import logic.res.html.C2491fm;
import p001a.*;

import java.util.Collection;

@C0566Hp
@C5829abJ("2.0.1")
@C6485anp
@C5511aMd
/* renamed from: a.aAt  reason: case insensitive filesystem */
/* compiled from: a */
public class ShipAmplifierType extends AmplifierType implements C1616Xf {
    /* renamed from: VB */
    public static final C5663aRz f2345VB = null;
    /* renamed from: VE */
    public static final C2491fm f2346VE = null;
    /* renamed from: VF */
    public static final C2491fm f2347VF = null;
    /* renamed from: VG */
    public static final C2491fm f2348VG = null;
    /* renamed from: VH */
    public static final C2491fm f2349VH = null;
    /* renamed from: VI */
    public static final C2491fm f2350VI = null;
    /* renamed from: VJ */
    public static final C2491fm f2351VJ = null;
    /* renamed from: VK */
    public static final C2491fm f2352VK = null;
    /* renamed from: VL */
    public static final C2491fm f2353VL = null;
    /* renamed from: VM */
    public static final C2491fm f2354VM = null;
    /* renamed from: VN */
    public static final C2491fm f2355VN = null;
    /* renamed from: Vt */
    public static final C5663aRz f2357Vt = null;
    /* renamed from: Vv */
    public static final C5663aRz f2359Vv = null;
    /* renamed from: Vx */
    public static final C5663aRz f2361Vx = null;
    /* renamed from: Vz */
    public static final C5663aRz f2363Vz = null;
    public static final int _m_fieldCount = 0;
    public static final C5663aRz[] _m_fields = null;
    public static final int _m_methodCount = 0;
    public static final C2491fm[] _m_methods = null;
    /* renamed from: dN */
    public static final C2491fm f2364dN = null;
    public static final C2491fm hdU = null;
    public static final long serialVersionUID = 0;
    public static C6494any ___iScriptClass;
    @C0064Am(aul = "975db32725f4c6e41c1f400358dbdbad", aum = 4)

    /* renamed from: VA */
    private static C3892wO f2344VA;
    @C0064Am(aul = "9e616e48c462df3f8c93fcae469d11da", aum = 0)

    /* renamed from: Vs */
    private static C3892wO f2356Vs;
    @C0064Am(aul = "cb05163157664ce7a1daf9ecaeea8b8c", aum = 1)

    /* renamed from: Vu */
    private static C3892wO f2358Vu;
    @C0064Am(aul = "0e2d0a1f8431d479fcb7d0d6e6a731f9", aum = 2)

    /* renamed from: Vw */
    private static C3892wO f2360Vw;
    @C0064Am(aul = "a7e9ae6588f6b8e1da944aefced25a57", aum = 3)

    /* renamed from: Vy */
    private static C3892wO f2362Vy;

    static {
        m12645V();
    }

    public ShipAmplifierType() {
        super((C5540aNg) null);
        super.mo10S();
    }

    public ShipAmplifierType(C5540aNg ang) {
        super(ang);
    }

    /* renamed from: V */
    static void m12645V() {
        serialVersionUID = (long) 1;
        _m_fieldCount = AmplifierType._m_fieldCount + 5;
        _m_methodCount = AmplifierType._m_methodCount + 12;
        int i = AmplifierType._m_fieldCount;
        C5663aRz[] arzArr = new C5663aRz[(i + 5)];
        C5663aRz b = C5640aRc.m17844b(ShipAmplifierType.class, "9e616e48c462df3f8c93fcae469d11da", i);
        f2357Vt = b;
        arzArr[i] = b;
        int i2 = i + 1;
        C5663aRz b2 = C5640aRc.m17844b(ShipAmplifierType.class, "cb05163157664ce7a1daf9ecaeea8b8c", i2);
        f2359Vv = b2;
        arzArr[i2] = b2;
        int i3 = i2 + 1;
        C5663aRz b3 = C5640aRc.m17844b(ShipAmplifierType.class, "0e2d0a1f8431d479fcb7d0d6e6a731f9", i3);
        f2361Vx = b3;
        arzArr[i3] = b3;
        int i4 = i3 + 1;
        C5663aRz b4 = C5640aRc.m17844b(ShipAmplifierType.class, "a7e9ae6588f6b8e1da944aefced25a57", i4);
        f2363Vz = b4;
        arzArr[i4] = b4;
        int i5 = i4 + 1;
        C5663aRz b5 = C5640aRc.m17844b(ShipAmplifierType.class, "975db32725f4c6e41c1f400358dbdbad", i5);
        f2345VB = b5;
        arzArr[i5] = b5;
        int i6 = i5 + 1;
        _m_fields = arzArr;
        C1634Xv.m11725a((Object[]) AmplifierType._m_fields, (Object[]) _m_fields);
        int i7 = AmplifierType._m_methodCount;
        C2491fm[] fmVarArr = new C2491fm[(i7 + 12)];
        C2491fm a = C4105zY.m41624a(ShipAmplifierType.class, "38af30575e5511dfd70af8571057f9a9", i7);
        f2346VE = a;
        fmVarArr[i7] = a;
        int i8 = i7 + 1;
        C2491fm a2 = C4105zY.m41624a(ShipAmplifierType.class, "f44946cdf422ec8c01ad24324c5dc3d4", i8);
        f2347VF = a2;
        fmVarArr[i8] = a2;
        int i9 = i8 + 1;
        C2491fm a3 = C4105zY.m41624a(ShipAmplifierType.class, "da46b4147c9fd6103c57ef7422cd6662", i9);
        f2348VG = a3;
        fmVarArr[i9] = a3;
        int i10 = i9 + 1;
        C2491fm a4 = C4105zY.m41624a(ShipAmplifierType.class, "31e2c929e5fd7d4a60895bc11374111d", i10);
        f2349VH = a4;
        fmVarArr[i10] = a4;
        int i11 = i10 + 1;
        C2491fm a5 = C4105zY.m41624a(ShipAmplifierType.class, "8b38f13009711ecfd1e45ed0558ed26c", i11);
        f2350VI = a5;
        fmVarArr[i11] = a5;
        int i12 = i11 + 1;
        C2491fm a6 = C4105zY.m41624a(ShipAmplifierType.class, "267c0871e54b36a4b79c438d1e27464b", i12);
        f2351VJ = a6;
        fmVarArr[i12] = a6;
        int i13 = i12 + 1;
        C2491fm a7 = C4105zY.m41624a(ShipAmplifierType.class, "b47affb0d67e5a5891d814d25898cfcb", i13);
        f2352VK = a7;
        fmVarArr[i13] = a7;
        int i14 = i13 + 1;
        C2491fm a8 = C4105zY.m41624a(ShipAmplifierType.class, "8e052d364ad8e8894ca204e11dcce156", i14);
        f2353VL = a8;
        fmVarArr[i14] = a8;
        int i15 = i14 + 1;
        C2491fm a9 = C4105zY.m41624a(ShipAmplifierType.class, "a3ba0638cf10c13500fc939940fd4e99", i15);
        f2354VM = a9;
        fmVarArr[i15] = a9;
        int i16 = i15 + 1;
        C2491fm a10 = C4105zY.m41624a(ShipAmplifierType.class, "cf034da39e4590003a498f46b9a8d01c", i16);
        f2355VN = a10;
        fmVarArr[i16] = a10;
        int i17 = i16 + 1;
        C2491fm a11 = C4105zY.m41624a(ShipAmplifierType.class, "055792bdc00ab55153e49ace2b23b704", i17);
        hdU = a11;
        fmVarArr[i17] = a11;
        int i18 = i17 + 1;
        C2491fm a12 = C4105zY.m41624a(ShipAmplifierType.class, "eeb61c2053a3c8c03af7da3d5befb2f1", i18);
        f2364dN = a12;
        fmVarArr[i18] = a12;
        int i19 = i18 + 1;
        _m_methods = fmVarArr;
        C1634Xv.m11725a((Object[]) AmplifierType._m_methods, (Object[]) _m_methods);
        ___iScriptClass = aUO.m18566a(ShipAmplifierType.class, C1464VY.class, _m_fields, _m_methods);
    }

    /* renamed from: d */
    private void m12647d(C3892wO wOVar) {
        bFf().mo5608dq().mo3197f(f2357Vt, wOVar);
    }

    /* renamed from: e */
    private void m12648e(C3892wO wOVar) {
        bFf().mo5608dq().mo3197f(f2359Vv, wOVar);
    }

    /* renamed from: f */
    private void m12649f(C3892wO wOVar) {
        bFf().mo5608dq().mo3197f(f2361Vx, wOVar);
    }

    /* renamed from: g */
    private void m12650g(C3892wO wOVar) {
        bFf().mo5608dq().mo3197f(f2363Vz, wOVar);
    }

    /* renamed from: h */
    private void m12651h(C3892wO wOVar) {
        bFf().mo5608dq().mo3197f(f2345VB, wOVar);
    }

    /* renamed from: yE */
    private C3892wO m12657yE() {
        return (C3892wO) bFf().mo5608dq().mo3214p(f2357Vt);
    }

    /* renamed from: yF */
    private C3892wO m12658yF() {
        return (C3892wO) bFf().mo5608dq().mo3214p(f2359Vv);
    }

    /* renamed from: yG */
    private C3892wO m12659yG() {
        return (C3892wO) bFf().mo5608dq().mo3214p(f2361Vx);
    }

    /* renamed from: yH */
    private C3892wO m12660yH() {
        return (C3892wO) bFf().mo5608dq().mo3214p(f2363Vz);
    }

    /* renamed from: yI */
    private C3892wO m12661yI() {
        return (C3892wO) bFf().mo5608dq().mo3214p(f2345VB);
    }

    /* renamed from: T */
    public C6494any mo11T() {
        return ___iScriptClass;
    }

    /* renamed from: U */
    public C2491fm[] mo12U() {
        return _m_methods;
    }

    /* renamed from: W */
    public Object mo13W() {
        return new C1464VY(this);
    }

    /* renamed from: a */
    public Object mo14a(C0495Gr gr) {
        Object[] args = gr.getArgs();
        switch (gr.mo2417hq() - AmplifierType._m_methodCount) {
            case 0:
                return m12662yK();
            case 1:
                m12652i((C3892wO) args[0]);
                return null;
            case 2:
                return m12663yM();
            case 3:
                m12653k((C3892wO) args[0]);
                return null;
            case 4:
                return m12664yO();
            case 5:
                m12654m((C3892wO) args[0]);
                return null;
            case 6:
                return m12665yQ();
            case 7:
                m12655o((C3892wO) args[0]);
                return null;
            case 8:
                return m12666yS();
            case 9:
                m12656q((C3892wO) args[0]);
                return null;
            case 10:
                return cHN();
            case 11:
                return m12646aT();
            default:
                return super.mo14a(gr);
        }
    }

    /* renamed from: a */
    public void mo15a(Collection collection, C0495Gr gr) {
        super.mo15a(collection, gr);
    }

    /* access modifiers changed from: protected */
    /* renamed from: aU */
    public <T extends aDJ> T mo745aU() {
        switch (bFf().mo6893i(f2364dN)) {
            case 0:
                return null;
            case 2:
                return (aDJ) bFf().mo5606d(new aCE(this, f2364dN, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f2364dN, new Object[0]));
                break;
        }
        return m12646aT();
    }

    /* renamed from: c */
    public C5663aRz[] mo25c() {
        return _m_fields;
    }

    public ShipAmplifier cHO() {
        switch (bFf().mo6893i(hdU)) {
            case 0:
                return null;
            case 2:
                return (ShipAmplifier) bFf().mo5606d(new aCE(this, hdU, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, hdU, new Object[0]));
                break;
        }
        return cHN();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Linear Velocity")
    /* renamed from: j */
    public void mo7773j(C3892wO wOVar) {
        switch (bFf().mo6893i(f2347VF)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f2347VF, new Object[]{wOVar}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f2347VF, new Object[]{wOVar}));
                break;
        }
        m12652i(wOVar);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Linear Acceleration")
    /* renamed from: l */
    public void mo7774l(C3892wO wOVar) {
        switch (bFf().mo6893i(f2349VH)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f2349VH, new Object[]{wOVar}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f2349VH, new Object[]{wOVar}));
                break;
        }
        m12653k(wOVar);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Angular Velocity")
    /* renamed from: n */
    public void mo7775n(C3892wO wOVar) {
        switch (bFf().mo6893i(f2351VJ)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f2351VJ, new Object[]{wOVar}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f2351VJ, new Object[]{wOVar}));
                break;
        }
        m12654m(wOVar);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Angular Acceleration")
    /* renamed from: p */
    public void mo7776p(C3892wO wOVar) {
        switch (bFf().mo6893i(f2353VL)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f2353VL, new Object[]{wOVar}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f2353VL, new Object[]{wOVar}));
                break;
        }
        m12655o(wOVar);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Radar Range")
    /* renamed from: r */
    public void mo7777r(C3892wO wOVar) {
        switch (bFf().mo6893i(f2355VN)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f2355VN, new Object[]{wOVar}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f2355VN, new Object[]{wOVar}));
                break;
        }
        m12656q(wOVar);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Linear Velocity")
    /* renamed from: yL */
    public C3892wO mo7778yL() {
        switch (bFf().mo6893i(f2346VE)) {
            case 0:
                return null;
            case 2:
                return (C3892wO) bFf().mo5606d(new aCE(this, f2346VE, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f2346VE, new Object[0]));
                break;
        }
        return m12662yK();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Linear Acceleration")
    /* renamed from: yN */
    public C3892wO mo7779yN() {
        switch (bFf().mo6893i(f2348VG)) {
            case 0:
                return null;
            case 2:
                return (C3892wO) bFf().mo5606d(new aCE(this, f2348VG, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f2348VG, new Object[0]));
                break;
        }
        return m12663yM();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Angular Velocity")
    /* renamed from: yP */
    public C3892wO mo7780yP() {
        switch (bFf().mo6893i(f2350VI)) {
            case 0:
                return null;
            case 2:
                return (C3892wO) bFf().mo5606d(new aCE(this, f2350VI, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f2350VI, new Object[0]));
                break;
        }
        return m12664yO();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Angular Acceleration")
    /* renamed from: yR */
    public C3892wO mo7781yR() {
        switch (bFf().mo6893i(f2352VK)) {
            case 0:
                return null;
            case 2:
                return (C3892wO) bFf().mo5606d(new aCE(this, f2352VK, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f2352VK, new Object[0]));
                break;
        }
        return m12665yQ();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Radar Range")
    /* renamed from: yT */
    public C3892wO mo7782yT() {
        switch (bFf().mo6893i(f2354VM)) {
            case 0:
                return null;
            case 2:
                return (C3892wO) bFf().mo5606d(new aCE(this, f2354VM, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f2354VM, new Object[0]));
                break;
        }
        return m12666yS();
    }

    /* renamed from: S */
    public void mo10S() {
        super.mo10S();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Linear Velocity")
    @C0064Am(aul = "38af30575e5511dfd70af8571057f9a9", aum = 0)
    /* renamed from: yK */
    private C3892wO m12662yK() {
        return m12657yE();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Linear Velocity")
    @C0064Am(aul = "f44946cdf422ec8c01ad24324c5dc3d4", aum = 0)
    /* renamed from: i */
    private void m12652i(C3892wO wOVar) {
        m12647d(wOVar);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Linear Acceleration")
    @C0064Am(aul = "da46b4147c9fd6103c57ef7422cd6662", aum = 0)
    /* renamed from: yM */
    private C3892wO m12663yM() {
        return m12658yF();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Linear Acceleration")
    @C0064Am(aul = "31e2c929e5fd7d4a60895bc11374111d", aum = 0)
    /* renamed from: k */
    private void m12653k(C3892wO wOVar) {
        m12648e(wOVar);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Angular Velocity")
    @C0064Am(aul = "8b38f13009711ecfd1e45ed0558ed26c", aum = 0)
    /* renamed from: yO */
    private C3892wO m12664yO() {
        return m12659yG();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Angular Velocity")
    @C0064Am(aul = "267c0871e54b36a4b79c438d1e27464b", aum = 0)
    /* renamed from: m */
    private void m12654m(C3892wO wOVar) {
        m12649f(wOVar);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Angular Acceleration")
    @C0064Am(aul = "b47affb0d67e5a5891d814d25898cfcb", aum = 0)
    /* renamed from: yQ */
    private C3892wO m12665yQ() {
        return m12660yH();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Angular Acceleration")
    @C0064Am(aul = "8e052d364ad8e8894ca204e11dcce156", aum = 0)
    /* renamed from: o */
    private void m12655o(C3892wO wOVar) {
        m12650g(wOVar);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Radar Range")
    @C0064Am(aul = "a3ba0638cf10c13500fc939940fd4e99", aum = 0)
    /* renamed from: yS */
    private C3892wO m12666yS() {
        return m12661yI();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Radar Range")
    @C0064Am(aul = "cf034da39e4590003a498f46b9a8d01c", aum = 0)
    /* renamed from: q */
    private void m12656q(C3892wO wOVar) {
        m12651h(wOVar);
    }

    @C0064Am(aul = "055792bdc00ab55153e49ace2b23b704", aum = 0)
    private ShipAmplifier cHN() {
        return (ShipAmplifier) mo745aU();
    }

    @C0064Am(aul = "eeb61c2053a3c8c03af7da3d5befb2f1", aum = 0)
    /* renamed from: aT */
    private <T extends aDJ> T m12646aT() {
        T t = (ShipAmplifier) bFf().mo6865M(ShipAmplifier.class);
        t.mo1956e((AmplifierType) this);
        return t;
    }
}
