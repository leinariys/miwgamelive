package game.script.item.buff.amplifier;

import game.network.message.externalizable.aCE;
import game.script.damage.DamageType;
import game.script.item.Amplifier;
import game.script.item.AmplifierType;
import game.script.ship.ShieldAdapter;
import logic.baa.C1616Xf;
import logic.baa.C1634Xv;
import logic.baa.C5511aMd;
import logic.baa.C6485anp;
import logic.data.mbean.C2990mh;
import logic.data.mbean.C6335akv;
import logic.res.code.C5640aRc;
import logic.res.code.C5663aRz;
import logic.res.code.C6494any;
import logic.res.html.C2491fm;
import p001a.*;

import java.util.Collection;

@C5829abJ("2.0.0")
@C6485anp
@C2712iu(mo19786Bt = AmplifierType.class, version = "2.0.1")
@C5511aMd
/* renamed from: a.Zg */
/* compiled from: a */
public class ShieldAmplifier extends Amplifier implements C1616Xf {

    /* renamed from: Lm */
    public static final C2491fm f2232Lm = null;
    public static final C2491fm _f_onResurrect_0020_0028_0029V = null;
    public static final int _m_fieldCount = 0;
    public static final C5663aRz[] _m_fields = null;
    public static final int _m_methodCount = 0;
    public static final C2491fm[] _m_methods = null;
    public static final C5663aRz aOJ = null;
    public static final C5663aRz aOr = null;
    public static final C5663aRz aOt = null;
    public static final C5663aRz aOz = null;
    public static final C2491fm aPJ = null;
    public static final C2491fm aPK = null;
    public static final C2491fm atW = null;
    public static final C2491fm atX = null;
    public static final C5663aRz eNm = null;
    public static final C2491fm eNn = null;
    public static final C2491fm eNo = null;
    public static final C2491fm eNp = null;
    public static final long serialVersionUID = 0;
    public static C6494any ___iScriptClass;
    @C0064Am(aul = "500784775e723d8e7ffbb7879c97912a", aum = 0)
    private static ShieldAdapter aBI;
    @C0064Am(aul = "98d70c037ca1d097a7ddbf346826a5fb", aum = 1)
    private static C3892wO aBJ;
    @C0064Am(aul = "238c3511433d5f898315f71acf014f6a", aum = 2)
    private static C3892wO aBK;
    @C0064Am(aul = "60b99ec8c69d1f503b207349d6e1f575", aum = 3)
    private static C3892wO aBL;
    @C0064Am(aul = "202019085c719834fa25ab5182a290a9", aum = 4)
    private static C3892wO aBM;

    static {
        m12119V();
    }

    public ShieldAmplifier() {
        super((C5540aNg) null);
        super.mo10S();
    }

    public ShieldAmplifier(C5540aNg ang) {
        super(ang);
    }

    public ShieldAmplifier(AmplifierType iDVar) {
        super((C5540aNg) null);
        super.mo1956e(iDVar);
    }

    /* renamed from: V */
    static void m12119V() {
        serialVersionUID = (long) 1;
        _m_fieldCount = Amplifier._m_fieldCount + 5;
        _m_methodCount = Amplifier._m_methodCount + 9;
        int i = Amplifier._m_fieldCount;
        C5663aRz[] arzArr = new C5663aRz[(i + 5)];
        C5663aRz b = C5640aRc.m17844b(ShieldAmplifier.class, "500784775e723d8e7ffbb7879c97912a", i);
        eNm = b;
        arzArr[i] = b;
        int i2 = i + 1;
        C5663aRz b2 = C5640aRc.m17844b(ShieldAmplifier.class, "98d70c037ca1d097a7ddbf346826a5fb", i2);
        aOJ = b2;
        arzArr[i2] = b2;
        int i3 = i2 + 1;
        C5663aRz b3 = C5640aRc.m17844b(ShieldAmplifier.class, "238c3511433d5f898315f71acf014f6a", i3);
        aOr = b3;
        arzArr[i3] = b3;
        int i4 = i3 + 1;
        C5663aRz b4 = C5640aRc.m17844b(ShieldAmplifier.class, "60b99ec8c69d1f503b207349d6e1f575", i4);
        aOz = b4;
        arzArr[i4] = b4;
        int i5 = i4 + 1;
        C5663aRz b5 = C5640aRc.m17844b(ShieldAmplifier.class, "202019085c719834fa25ab5182a290a9", i5);
        aOt = b5;
        arzArr[i5] = b5;
        int i6 = i5 + 1;
        _m_fields = arzArr;
        C1634Xv.m11725a((Object[]) Amplifier._m_fields, (Object[]) _m_fields);
        int i7 = Amplifier._m_methodCount;
        C2491fm[] fmVarArr = new C2491fm[(i7 + 9)];
        C2491fm a = C4105zY.m41624a(ShieldAmplifier.class, "5f0abff2e8dbe1495a8d8aed3dc6c6d8", i7);
        atW = a;
        fmVarArr[i7] = a;
        int i8 = i7 + 1;
        C2491fm a2 = C4105zY.m41624a(ShieldAmplifier.class, "16e9d2ce5741d6f2f59f602888bbb1bc", i8);
        atX = a2;
        fmVarArr[i8] = a2;
        int i9 = i8 + 1;
        C2491fm a3 = C4105zY.m41624a(ShieldAmplifier.class, "f6ecd28ad480d20527436709b2e13d6c", i9);
        eNn = a3;
        fmVarArr[i9] = a3;
        int i10 = i9 + 1;
        C2491fm a4 = C4105zY.m41624a(ShieldAmplifier.class, "0c8429229837dd365bf243e7120873b6", i10);
        eNo = a4;
        fmVarArr[i10] = a4;
        int i11 = i10 + 1;
        C2491fm a5 = C4105zY.m41624a(ShieldAmplifier.class, "492878d492d1d95f9a4bcec398468e6b", i11);
        aPJ = a5;
        fmVarArr[i11] = a5;
        int i12 = i11 + 1;
        C2491fm a6 = C4105zY.m41624a(ShieldAmplifier.class, "80180fedc90e76df4b7b3f2104c265ae", i12);
        aPK = a6;
        fmVarArr[i12] = a6;
        int i13 = i12 + 1;
        C2491fm a7 = C4105zY.m41624a(ShieldAmplifier.class, "7c1c941a85efab0222d2b68afd788b28", i13);
        eNp = a7;
        fmVarArr[i13] = a7;
        int i14 = i13 + 1;
        C2491fm a8 = C4105zY.m41624a(ShieldAmplifier.class, "ce153bcf3886c082c55137a6017509cd", i14);
        f2232Lm = a8;
        fmVarArr[i14] = a8;
        int i15 = i14 + 1;
        C2491fm a9 = C4105zY.m41624a(ShieldAmplifier.class, "5c302327a18d928a788e3bb28d117bb6", i15);
        _f_onResurrect_0020_0028_0029V = a9;
        fmVarArr[i15] = a9;
        int i16 = i15 + 1;
        _m_methods = fmVarArr;
        C1634Xv.m11725a((Object[]) Amplifier._m_methods, (Object[]) _m_methods);
        ___iScriptClass = aUO.m18566a(ShieldAmplifier.class, C2990mh.class, _m_fields, _m_methods);
    }

    @C0064Am(aul = "5f0abff2e8dbe1495a8d8aed3dc6c6d8", aum = 0)
    @C5566aOg
    /* renamed from: Kr */
    private void m12112Kr() {
        throw new aWi(new aCE(this, atW, new Object[0]));
    }

    @C0064Am(aul = "16e9d2ce5741d6f2f59f602888bbb1bc", aum = 0)
    @C5566aOg
    /* renamed from: Kt */
    private void m12113Kt() {
        throw new aWi(new aCE(this, atX, new Object[0]));
    }

    /* renamed from: U */
    private void m12114U(C3892wO wOVar) {
        throw new C6039afL();
    }

    /* renamed from: Ub */
    private C3892wO m12115Ub() {
        return ((ShieldAmplifierType) getType()).aBc();
    }

    /* renamed from: Uc */
    private C3892wO m12116Uc() {
        return ((ShieldAmplifierType) getType()).aBe();
    }

    /* renamed from: V */
    private void m12120V(C3892wO wOVar) {
        throw new C6039afL();
    }

    /* renamed from: a */
    private void m12121a(ShieldAdapter cyVar) {
        bFf().mo5608dq().mo3197f(eNm, cyVar);
    }

    private C3892wO aOS() {
        return ((ShieldAmplifierType) getType()).aOV();
    }

    private C3892wO aOT() {
        return ((ShieldAmplifierType) getType()).aOX();
    }

    private ShieldAdapter bIe() {
        return (ShieldAdapter) bFf().mo5608dq().mo3214p(eNm);
    }

    @C0064Am(aul = "ce153bcf3886c082c55137a6017509cd", aum = 0)
    @C5566aOg
    @C2499fr
    /* renamed from: qU */
    private void m12123qU() {
        throw new aWi(new aCE(this, f2232Lm, new Object[0]));
    }

    /* renamed from: v */
    private void m12124v(C3892wO wOVar) {
        throw new C6039afL();
    }

    /* renamed from: w */
    private void m12125w(C3892wO wOVar) {
        throw new C6039afL();
    }

    @C5566aOg
    /* renamed from: Ks */
    public void mo1952Ks() {
        switch (bFf().mo6893i(atW)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, atW, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, atW, new Object[0]));
                break;
        }
        m12112Kr();
    }

    @C5566aOg
    /* renamed from: Ku */
    public void mo1953Ku() {
        switch (bFf().mo6893i(atX)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, atX, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, atX, new Object[0]));
                break;
        }
        m12113Kt();
    }

    /* renamed from: S */
    public void mo10S() {
        super.mo10S();
    }

    /* renamed from: T */
    public C6494any mo11T() {
        return ___iScriptClass;
    }

    /* renamed from: U */
    public C2491fm[] mo12U() {
        return _m_methods;
    }

    /* access modifiers changed from: protected */
    /* renamed from: Uj */
    public C3892wO mo7450Uj() {
        switch (bFf().mo6893i(aPJ)) {
            case 0:
                return null;
            case 2:
                return (C3892wO) bFf().mo5606d(new aCE(this, aPJ, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, aPJ, new Object[0]));
                break;
        }
        return m12117Ui();
    }

    /* access modifiers changed from: protected */
    /* renamed from: Ul */
    public C3892wO mo7451Ul() {
        switch (bFf().mo6893i(aPK)) {
            case 0:
                return null;
            case 2:
                return (C3892wO) bFf().mo5606d(new aCE(this, aPK, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, aPK, new Object[0]));
                break;
        }
        return m12118Uk();
    }

    /* renamed from: W */
    public Object mo13W() {
        return new C2990mh(this);
    }

    /* renamed from: a */
    public Object mo14a(C0495Gr gr) {
        gr.getArgs();
        switch (gr.mo2417hq() - Amplifier._m_methodCount) {
            case 0:
                m12112Kr();
                return null;
            case 1:
                m12113Kt();
                return null;
            case 2:
                return bIf();
            case 3:
                return bIh();
            case 4:
                return m12117Ui();
            case 5:
                return m12118Uk();
            case 6:
                return new Float(bIj());
            case 7:
                m12123qU();
                return null;
            case 8:
                m12122aG();
                return null;
            default:
                return super.mo14a(gr);
        }
    }

    /* renamed from: a */
    public void mo15a(Collection collection, C0495Gr gr) {
        super.mo15a(collection, gr);
    }

    /* renamed from: aH */
    public void mo70aH() {
        switch (bFf().mo6893i(_f_onResurrect_0020_0028_0029V)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, _f_onResurrect_0020_0028_0029V, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, _f_onResurrect_0020_0028_0029V, new Object[0]));
                break;
        }
        m12122aG();
    }

    /* access modifiers changed from: protected */
    public C3892wO bIg() {
        switch (bFf().mo6893i(eNn)) {
            case 0:
                return null;
            case 2:
                return (C3892wO) bFf().mo5606d(new aCE(this, eNn, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, eNn, new Object[0]));
                break;
        }
        return bIf();
    }

    /* access modifiers changed from: protected */
    public C3892wO bIi() {
        switch (bFf().mo6893i(eNo)) {
            case 0:
                return null;
            case 2:
                return (C3892wO) bFf().mo5606d(new aCE(this, eNo, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, eNo, new Object[0]));
                break;
        }
        return bIh();
    }

    /* access modifiers changed from: protected */
    public float bIk() {
        switch (bFf().mo6893i(eNp)) {
            case 0:
                return 0.0f;
            case 2:
                return ((Float) bFf().mo5606d(new aCE(this, eNp, new Object[0]))).floatValue();
            case 3:
                bFf().mo5606d(new aCE(this, eNp, new Object[0]));
                break;
        }
        return bIj();
    }

    /* renamed from: c */
    public C5663aRz[] mo25c() {
        return _m_fields;
    }

    @C5566aOg
    @C2499fr
    /* renamed from: qV */
    public void mo1959qV() {
        switch (bFf().mo6893i(f2232Lm)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f2232Lm, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f2232Lm, new Object[0]));
                break;
        }
        m12123qU();
    }

    /* renamed from: e */
    public void mo1956e(AmplifierType iDVar) {
        super.mo1956e(iDVar);
    }

    @C0064Am(aul = "f6ecd28ad480d20527436709b2e13d6c", aum = 0)
    private C3892wO bIf() {
        return aOT();
    }

    @C0064Am(aul = "0c8429229837dd365bf243e7120873b6", aum = 0)
    private C3892wO bIh() {
        return aOS();
    }

    @C0064Am(aul = "492878d492d1d95f9a4bcec398468e6b", aum = 0)
    /* renamed from: Ui */
    private C3892wO m12117Ui() {
        return m12116Uc();
    }

    @C0064Am(aul = "80180fedc90e76df4b7b3f2104c265ae", aum = 0)
    /* renamed from: Uk */
    private C3892wO m12118Uk() {
        return m12115Ub();
    }

    @C0064Am(aul = "7c1c941a85efab0222d2b68afd788b28", aum = 0)
    private float bIj() {
        if (mo7451Ul().coZ() == C3892wO.C3893a.PERCENT) {
            return (float) ((int) (((ShieldAdapter) bIe().mo16070IE()).mo3761hh() * (mo7451Ul().getValue() / 100.0f)));
        }
        return mo7451Ul().getValue();
    }

    @C0064Am(aul = "5c302327a18d928a788e3bb28d117bb6", aum = 0)
    /* renamed from: aG */
    private void m12122aG() {
        super.mo70aH();
    }

    @C6485anp
    @C5511aMd
    /* renamed from: a.Zg$a */
    public class ShieldAmplifierAdapter extends ShieldAdapter implements C1616Xf {
        public static final int _m_fieldCount = 0;
        public static final C5663aRz[] _m_fields = null;
        public static final int _m_methodCount = 0;
        public static final C2491fm[] _m_methods = null;
        /* renamed from: aT */
        public static final C5663aRz f2233aT = null;
        /* renamed from: qa */
        public static final C2491fm f2234qa = null;
        /* renamed from: qb */
        public static final C2491fm f2235qb = null;
        public static final long serialVersionUID = 0;
        /* renamed from: uG */
        public static final C2491fm f2236uG = null;
        /* renamed from: uH */
        public static final C2491fm f2237uH = null;
        /* renamed from: uI */
        public static final C2491fm f2238uI = null;
        public static C6494any ___iScriptClass;
        @C0064Am(aul = "b5464af19d445048fb4a5821d0d6f9c8", aum = 0)
        static /* synthetic */ ShieldAmplifier fTC;

        static {
            m12140V();
        }

        public ShieldAmplifierAdapter() {
            super((C5540aNg) null);
            super.mo10S();
        }

        public ShieldAmplifierAdapter(ShieldAmplifier zg) {
            super((C5540aNg) null);
            super._m_script_init(zg);
        }

        public ShieldAmplifierAdapter(C5540aNg ang) {
            super(ang);
        }

        /* renamed from: V */
        static void m12140V() {
            serialVersionUID = (long) 1;
            _m_fieldCount = ShieldAdapter._m_fieldCount + 1;
            _m_methodCount = ShieldAdapter._m_methodCount + 5;
            int i = ShieldAdapter._m_fieldCount;
            C5663aRz[] arzArr = new C5663aRz[(i + 1)];
            C5663aRz b = C5640aRc.m17844b(ShieldAmplifierAdapter.class, "b5464af19d445048fb4a5821d0d6f9c8", i);
            f2233aT = b;
            arzArr[i] = b;
            int i2 = i + 1;
            _m_fields = arzArr;
            C1634Xv.m11725a((Object[]) ShieldAdapter._m_fields, (Object[]) _m_fields);
            int i3 = ShieldAdapter._m_methodCount;
            C2491fm[] fmVarArr = new C2491fm[(i3 + 5)];
            C2491fm a = C4105zY.m41624a(ShieldAmplifierAdapter.class, "e3c2ed9ecb131beaf9868f75431d0cc5", i3);
            f2234qa = a;
            fmVarArr[i3] = a;
            int i4 = i3 + 1;
            C2491fm a2 = C4105zY.m41624a(ShieldAmplifierAdapter.class, "e95224b63de703b432ebb38855c991aa", i4);
            f2238uI = a2;
            fmVarArr[i4] = a2;
            int i5 = i4 + 1;
            C2491fm a3 = C4105zY.m41624a(ShieldAmplifierAdapter.class, "a5d4a202bba4a3964eab0c8e818b4897", i5);
            f2235qb = a3;
            fmVarArr[i5] = a3;
            int i6 = i5 + 1;
            C2491fm a4 = C4105zY.m41624a(ShieldAmplifierAdapter.class, "a30cdc53ce1730e1fb757f058dfc73a1", i6);
            f2237uH = a4;
            fmVarArr[i6] = a4;
            int i7 = i6 + 1;
            C2491fm a5 = C4105zY.m41624a(ShieldAmplifierAdapter.class, "bc4c72369aefb70a97d08831dd9bab1c", i7);
            f2236uG = a5;
            fmVarArr[i7] = a5;
            int i8 = i7 + 1;
            _m_methods = fmVarArr;
            C1634Xv.m11725a((Object[]) ShieldAdapter._m_methods, (Object[]) _m_methods);
            ___iScriptClass = aUO.m18566a(ShieldAmplifierAdapter.class, C6335akv.class, _m_fields, _m_methods);
        }

        /* renamed from: a */
        private void m12143a(ShieldAmplifier zg) {
            bFf().mo5608dq().mo3197f(f2233aT, zg);
        }

        private ShieldAmplifier cik() {
            return (ShieldAmplifier) bFf().mo5608dq().mo3214p(f2233aT);
        }

        /* renamed from: S */
        public void mo10S() {
            super.mo10S();
        }

        /* renamed from: T */
        public C6494any mo11T() {
            return ___iScriptClass;
        }

        /* renamed from: U */
        public C2491fm[] mo12U() {
            return _m_methods;
        }

        /* renamed from: W */
        public Object mo13W() {
            return new C6335akv(this);
        }

        /* renamed from: a */
        public Object mo14a(C0495Gr gr) {
            Object[] args = gr.getArgs();
            switch (gr.mo2417hq() - ShieldAdapter._m_methodCount) {
                case 0:
                    return new Float(m12142a((DamageType) args[0], ((Float) args[1]).floatValue()));
                case 1:
                    return new Float(m12141a((DamageType) args[0]));
                case 2:
                    return new Float(m12145hg());
                case 3:
                    return new Float(m12146ib());
                case 4:
                    return new Float(m12144hZ());
                default:
                    return super.mo14a(gr);
            }
        }

        /* renamed from: a */
        public void mo15a(Collection collection, C0495Gr gr) {
            super.mo15a(collection, gr);
        }

        /* renamed from: b */
        public float mo3758b(DamageType fr) {
            switch (bFf().mo6893i(f2238uI)) {
                case 0:
                    return 0.0f;
                case 2:
                    return ((Float) bFf().mo5606d(new aCE(this, f2238uI, new Object[]{fr}))).floatValue();
                case 3:
                    bFf().mo5606d(new aCE(this, f2238uI, new Object[]{fr}));
                    break;
            }
            return m12141a(fr);
        }

        /* renamed from: b */
        public float mo3759b(DamageType fr, float f) {
            switch (bFf().mo6893i(f2234qa)) {
                case 0:
                    return 0.0f;
                case 2:
                    return ((Float) bFf().mo5606d(new aCE(this, f2234qa, new Object[]{fr, new Float(f)}))).floatValue();
                case 3:
                    bFf().mo5606d(new aCE(this, f2234qa, new Object[]{fr, new Float(f)}));
                    break;
            }
            return m12142a(fr, f);
        }

        /* renamed from: c */
        public C5663aRz[] mo25c() {
            return _m_fields;
        }

        /* renamed from: hh */
        public float mo3761hh() {
            switch (bFf().mo6893i(f2235qb)) {
                case 0:
                    return 0.0f;
                case 2:
                    return ((Float) bFf().mo5606d(new aCE(this, f2235qb, new Object[0]))).floatValue();
                case 3:
                    bFf().mo5606d(new aCE(this, f2235qb, new Object[0]));
                    break;
            }
            return m12145hg();
        }

        /* renamed from: ia */
        public float mo3762ia() {
            switch (bFf().mo6893i(f2236uG)) {
                case 0:
                    return 0.0f;
                case 2:
                    return ((Float) bFf().mo5606d(new aCE(this, f2236uG, new Object[0]))).floatValue();
                case 3:
                    bFf().mo5606d(new aCE(this, f2236uG, new Object[0]));
                    break;
            }
            return m12144hZ();
        }

        /* renamed from: ic */
        public float mo3763ic() {
            switch (bFf().mo6893i(f2237uH)) {
                case 0:
                    return 0.0f;
                case 2:
                    return ((Float) bFf().mo5606d(new aCE(this, f2237uH, new Object[0]))).floatValue();
                case 3:
                    bFf().mo5606d(new aCE(this, f2237uH, new Object[0]));
                    break;
            }
            return m12146ib();
        }

        /* renamed from: b */
        public void mo7455b(ShieldAmplifier zg) {
            m12143a(zg);
            super.mo10S();
        }

        @C0064Am(aul = "e3c2ed9ecb131beaf9868f75431d0cc5", aum = 0)
        /* renamed from: a */
        private float m12142a(DamageType fr, float f) {
            return cik().mo7450Uj().mo22753v(((ShieldAdapter) mo16070IE()).mo3759b(fr, f), super.mo3759b(fr, f));
        }

        @C0064Am(aul = "e95224b63de703b432ebb38855c991aa", aum = 0)
        /* renamed from: a */
        private float m12141a(DamageType fr) {
            return cik().mo7450Uj().mo22753v(((ShieldAdapter) mo16070IE()).mo3758b(fr), super.mo3758b(fr));
        }

        @C0064Am(aul = "a5d4a202bba4a3964eab0c8e818b4897", aum = 0)
        /* renamed from: hg */
        private float m12145hg() {
            float v = cik().mo7451Ul().mo22753v(((ShieldAdapter) mo16070IE()).mo3761hh(), super.mo3761hh());
            if (v > 0.0f) {
                return v;
            }
            return 0.0f;
        }

        @C0064Am(aul = "a30cdc53ce1730e1fb757f058dfc73a1", aum = 0)
        /* renamed from: ib */
        private float m12146ib() {
            return cik().bIg().mo22753v(((ShieldAdapter) mo16070IE()).mo3763ic(), super.mo3763ic());
        }

        @C0064Am(aul = "bc4c72369aefb70a97d08831dd9bab1c", aum = 0)
        /* renamed from: hZ */
        private float m12144hZ() {
            return cik().bIi().mo22753v(((ShieldAdapter) mo16070IE()).mo3762ia(), super.mo3762ia());
        }
    }
}
