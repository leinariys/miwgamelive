package game.script.item.buff.amplifier;

import game.network.message.externalizable.aCE;
import game.script.item.Amplifier;
import game.script.item.AmplifierType;
import game.script.ship.categories.Fighter;
import game.script.ship.speedBoost.SpeedBoostAdapter;
import logic.baa.C1616Xf;
import logic.baa.C1634Xv;
import logic.baa.C5511aMd;
import logic.baa.C6485anp;
import logic.data.mbean.C0038AV;
import logic.data.mbean.C1140Qk;
import logic.res.code.C5640aRc;
import logic.res.code.C5663aRz;
import logic.res.code.C6494any;
import logic.res.html.C2491fm;
import logic.thred.LogPrinter;
import p001a.*;

import java.util.Collection;

@C6485anp
@C2712iu
@C5511aMd
/* renamed from: a.tT */
/* compiled from: a */
public class SpeedBoostAmplifier extends Amplifier implements C1616Xf {

    /* renamed from: KF */
    public static final C5663aRz f9242KF = null;
    /* renamed from: Vt */
    public static final C5663aRz f9244Vt = null;
    /* renamed from: Vv */
    public static final C5663aRz f9246Vv = null;
    /* renamed from: Vx */
    public static final C5663aRz f9248Vx = null;
    /* renamed from: Vz */
    public static final C5663aRz f9250Vz = null;
    public static final int _m_fieldCount = 0;
    public static final C5663aRz[] _m_fields = null;
    public static final int _m_methodCount = 0;
    public static final C2491fm[] _m_methods = null;
    public static final C2491fm aYg = null;
    public static final C2491fm aYh = null;
    public static final C2491fm aYi = null;
    public static final C2491fm aYj = null;
    public static final C5663aRz atS = null;
    public static final C5663aRz atV = null;
    public static final C2491fm atW = null;
    public static final C2491fm atX = null;
    public static final C2491fm atZ = null;
    public static final C2491fm duG = null;
    public static final long serialVersionUID = 0;
    private static final LogPrinter log = LogPrinter.setClass(Amplifier.class);
    public static C6494any ___iScriptClass;
    @C0064Am(aul = "c1bb226019458832d3fbb1658cafa78b", aum = 1)

    /* renamed from: Vs */
    private static C3892wO f9243Vs;
    @C0064Am(aul = "d15f2bde2981e220628eb6ad8d350621", aum = 2)

    /* renamed from: Vu */
    private static C3892wO f9245Vu;
    @C0064Am(aul = "ca5a9e056d1f9608cf7cd610b9ff2613", aum = 4)

    /* renamed from: Vw */
    private static C3892wO f9247Vw;
    @C0064Am(aul = "d2ac6f03cac3f05a7df1d0a28d3e290b", aum = 3)

    /* renamed from: Vy */
    private static C3892wO f9249Vy;
    @C0064Am(aul = "2919c0bbb8ad22015768e5642b9f5759", aum = 6)
    private static boolean active;
    @C0064Am(aul = "eba66e6e4f4e38575514e69c4902f35e", aum = 0)
    private static C3892wO atR;
    @C0064Am(aul = "c49111a8aee162d4980d1db2a2b5270a", aum = 5)
    private static SpeedBoostAdapter duF;

    static {
        m39597V();
    }

    public SpeedBoostAmplifier() {
        super((C5540aNg) null);
        super.mo10S();
    }

    public SpeedBoostAmplifier(C5540aNg ang) {
        super(ang);
    }

    public SpeedBoostAmplifier(AmplifierType iDVar) {
        super((C5540aNg) null);
        super.mo1956e(iDVar);
    }

    /* renamed from: V */
    static void m39597V() {
        serialVersionUID = (long) 1;
        _m_fieldCount = Amplifier._m_fieldCount + 7;
        _m_methodCount = Amplifier._m_methodCount + 8;
        int i = Amplifier._m_fieldCount;
        C5663aRz[] arzArr = new C5663aRz[(i + 7)];
        C5663aRz b = C5640aRc.m17844b(SpeedBoostAmplifier.class, "eba66e6e4f4e38575514e69c4902f35e", i);
        atS = b;
        arzArr[i] = b;
        int i2 = i + 1;
        C5663aRz b2 = C5640aRc.m17844b(SpeedBoostAmplifier.class, "c1bb226019458832d3fbb1658cafa78b", i2);
        f9244Vt = b2;
        arzArr[i2] = b2;
        int i3 = i2 + 1;
        C5663aRz b3 = C5640aRc.m17844b(SpeedBoostAmplifier.class, "d15f2bde2981e220628eb6ad8d350621", i3);
        f9246Vv = b3;
        arzArr[i3] = b3;
        int i4 = i3 + 1;
        C5663aRz b4 = C5640aRc.m17844b(SpeedBoostAmplifier.class, "d2ac6f03cac3f05a7df1d0a28d3e290b", i4);
        f9250Vz = b4;
        arzArr[i4] = b4;
        int i5 = i4 + 1;
        C5663aRz b5 = C5640aRc.m17844b(SpeedBoostAmplifier.class, "ca5a9e056d1f9608cf7cd610b9ff2613", i5);
        f9248Vx = b5;
        arzArr[i5] = b5;
        int i6 = i5 + 1;
        C5663aRz b6 = C5640aRc.m17844b(SpeedBoostAmplifier.class, "c49111a8aee162d4980d1db2a2b5270a", i6);
        atV = b6;
        arzArr[i6] = b6;
        int i7 = i6 + 1;
        C5663aRz b7 = C5640aRc.m17844b(SpeedBoostAmplifier.class, "2919c0bbb8ad22015768e5642b9f5759", i7);
        f9242KF = b7;
        arzArr[i7] = b7;
        int i8 = i7 + 1;
        _m_fields = arzArr;
        C1634Xv.m11725a((Object[]) Amplifier._m_fields, (Object[]) _m_fields);
        int i9 = Amplifier._m_methodCount;
        C2491fm[] fmVarArr = new C2491fm[(i9 + 8)];
        C2491fm a = C4105zY.m41624a(SpeedBoostAmplifier.class, "0e4e0c906dc8fd5adb2db05d133442d0", i9);
        atW = a;
        fmVarArr[i9] = a;
        int i10 = i9 + 1;
        C2491fm a2 = C4105zY.m41624a(SpeedBoostAmplifier.class, "8d97ee613c36e9c878b52b695a946135", i10);
        atX = a2;
        fmVarArr[i10] = a2;
        int i11 = i10 + 1;
        C2491fm a3 = C4105zY.m41624a(SpeedBoostAmplifier.class, "27f8f6da196848bff90d088813ddf7fb", i11);
        duG = a3;
        fmVarArr[i11] = a3;
        int i12 = i11 + 1;
        C2491fm a4 = C4105zY.m41624a(SpeedBoostAmplifier.class, "f26a4638e346a9e0f645217d989abbbb", i12);
        atZ = a4;
        fmVarArr[i12] = a4;
        int i13 = i12 + 1;
        C2491fm a5 = C4105zY.m41624a(SpeedBoostAmplifier.class, "b7c4cbd1b56e339b982e07bf678add52", i13);
        aYg = a5;
        fmVarArr[i13] = a5;
        int i14 = i13 + 1;
        C2491fm a6 = C4105zY.m41624a(SpeedBoostAmplifier.class, "9ae2ead49aba3ba58959250c4b9296a7", i14);
        aYh = a6;
        fmVarArr[i14] = a6;
        int i15 = i14 + 1;
        C2491fm a7 = C4105zY.m41624a(SpeedBoostAmplifier.class, "559bdd7042bcd83e53ac9c29092de190", i15);
        aYj = a7;
        fmVarArr[i15] = a7;
        int i16 = i15 + 1;
        C2491fm a8 = C4105zY.m41624a(SpeedBoostAmplifier.class, "2c8b99d9f348bf791257a731a962dc39", i16);
        aYi = a8;
        fmVarArr[i16] = a8;
        int i17 = i16 + 1;
        _m_methods = fmVarArr;
        C1634Xv.m11725a((Object[]) Amplifier._m_methods, (Object[]) _m_methods);
        ___iScriptClass = aUO.m18566a(SpeedBoostAmplifier.class, C1140Qk.class, _m_fields, _m_methods);
    }

    /* renamed from: F */
    private void m39592F(boolean z) {
        bFf().mo5608dq().mo3153a(f9242KF, z);
    }

    /* renamed from: Ko */
    private C3892wO m39593Ko() {
        return ((SpeedBoostAmplifierType) getType()).bLB();
    }

    /* renamed from: a */
    private void m39602a(SpeedBoostAdapter ahz) {
        bFf().mo5608dq().mo3197f(atV, ahz);
    }

    private SpeedBoostAdapter bfC() {
        return (SpeedBoostAdapter) bFf().mo5608dq().mo3214p(atV);
    }

    /* renamed from: d */
    private void m39603d(C3892wO wOVar) {
        throw new C6039afL();
    }

    /* renamed from: e */
    private void m39604e(C3892wO wOVar) {
        throw new C6039afL();
    }

    /* renamed from: f */
    private void m39605f(C3892wO wOVar) {
        throw new C6039afL();
    }

    /* renamed from: g */
    private void m39606g(C3892wO wOVar) {
        throw new C6039afL();
    }

    /* renamed from: qv */
    private boolean m39607qv() {
        return bFf().mo5608dq().mo3201h(f9242KF);
    }

    /* renamed from: t */
    private void m39608t(C3892wO wOVar) {
        throw new C6039afL();
    }

    /* renamed from: yE */
    private C3892wO m39609yE() {
        return ((SpeedBoostAmplifierType) getType()).mo12567yL();
    }

    /* renamed from: yF */
    private C3892wO m39610yF() {
        return ((SpeedBoostAmplifierType) getType()).mo12568yN();
    }

    /* renamed from: yG */
    private C3892wO m39611yG() {
        return ((SpeedBoostAmplifierType) getType()).mo12569yP();
    }

    /* renamed from: yH */
    private C3892wO m39612yH() {
        return ((SpeedBoostAmplifierType) getType()).mo12570yR();
    }

    /* renamed from: Ks */
    public void mo1952Ks() {
        switch (bFf().mo6893i(atW)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, atW, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, atW, new Object[0]));
                break;
        }
        m39594Kr();
    }

    /* renamed from: Ku */
    public void mo1953Ku() {
        switch (bFf().mo6893i(atX)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, atX, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, atX, new Object[0]));
                break;
        }
        m39595Kt();
    }

    /* renamed from: Kw */
    public C3892wO mo22232Kw() {
        switch (bFf().mo6893i(atZ)) {
            case 0:
                return null;
            case 2:
                return (C3892wO) bFf().mo5606d(new aCE(this, atZ, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, atZ, new Object[0]));
                break;
        }
        return m39596Kv();
    }

    /* renamed from: S */
    public void mo10S() {
        super.mo10S();
    }

    /* renamed from: T */
    public C6494any mo11T() {
        return ___iScriptClass;
    }

    /* renamed from: U */
    public C2491fm[] mo12U() {
        return _m_methods;
    }

    /* renamed from: W */
    public Object mo13W() {
        return new C1140Qk(this);
    }

    /* renamed from: XR */
    public C3892wO mo22233XR() {
        switch (bFf().mo6893i(aYg)) {
            case 0:
                return null;
            case 2:
                return (C3892wO) bFf().mo5606d(new aCE(this, aYg, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, aYg, new Object[0]));
                break;
        }
        return m39598XQ();
    }

    /* renamed from: XT */
    public C3892wO mo22234XT() {
        switch (bFf().mo6893i(aYh)) {
            case 0:
                return null;
            case 2:
                return (C3892wO) bFf().mo5606d(new aCE(this, aYh, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, aYh, new Object[0]));
                break;
        }
        return m39599XS();
    }

    /* renamed from: XV */
    public C3892wO mo22235XV() {
        switch (bFf().mo6893i(aYi)) {
            case 0:
                return null;
            case 2:
                return (C3892wO) bFf().mo5606d(new aCE(this, aYi, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, aYi, new Object[0]));
                break;
        }
        return m39600XU();
    }

    /* renamed from: XX */
    public C3892wO mo22236XX() {
        switch (bFf().mo6893i(aYj)) {
            case 0:
                return null;
            case 2:
                return (C3892wO) bFf().mo5606d(new aCE(this, aYj, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, aYj, new Object[0]));
                break;
        }
        return m39601XW();
    }

    /* renamed from: a */
    public Object mo14a(C0495Gr gr) {
        gr.getArgs();
        switch (gr.mo2417hq() - Amplifier._m_methodCount) {
            case 0:
                m39594Kr();
                return null;
            case 1:
                m39595Kt();
                return null;
            case 2:
                return bfD();
            case 3:
                return m39596Kv();
            case 4:
                return m39598XQ();
            case 5:
                return m39599XS();
            case 6:
                return m39601XW();
            case 7:
                return m39600XU();
            default:
                return super.mo14a(gr);
        }
    }

    /* renamed from: a */
    public void mo15a(Collection collection, C0495Gr gr) {
        super.mo15a(collection, gr);
    }

    public SpeedBoostAdapter bfE() {
        switch (bFf().mo6893i(duG)) {
            case 0:
                return null;
            case 2:
                return (SpeedBoostAdapter) bFf().mo5606d(new aCE(this, duG, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, duG, new Object[0]));
                break;
        }
        return bfD();
    }

    /* renamed from: c */
    public C5663aRz[] mo25c() {
        return _m_fields;
    }

    /* renamed from: e */
    public void mo1956e(AmplifierType iDVar) {
        super.mo1956e(iDVar);
    }

    @C0064Am(aul = "0e4e0c906dc8fd5adb2db05d133442d0", aum = 0)
    /* renamed from: Kr */
    private void m39594Kr() {
        if (!m39607qv() && (mo7855al() instanceof Fighter)) {
            if (bfC() == null) {
                SpeedBoosAmplifierAdapter aVar = (SpeedBoosAmplifierAdapter) bFf().mo6865M(SpeedBoosAmplifierAdapter.class);
                aVar.mo22238b(this);
                m39602a((SpeedBoostAdapter) aVar);
            }
            bfC().push();
            ((Fighter) mo7855al()).biJ().mo5353TJ().mo23261e(bfC());
            asW();
            m39592F(true);
        }
    }

    @C0064Am(aul = "8d97ee613c36e9c878b52b695a946135", aum = 0)
    /* renamed from: Kt */
    private void m39595Kt() {
        if (m39607qv() && (mo7855al() instanceof Fighter)) {
            ((Fighter) mo7855al()).biJ().mo5353TJ().mo23262g(bfC());
            asW();
            m39592F(false);
        }
    }

    @C0064Am(aul = "27f8f6da196848bff90d088813ddf7fb", aum = 0)
    private SpeedBoostAdapter bfD() {
        return bfC();
    }

    @C0064Am(aul = "f26a4638e346a9e0f645217d989abbbb", aum = 0)
    /* renamed from: Kv */
    private C3892wO m39596Kv() {
        return m39593Ko();
    }

    @C0064Am(aul = "b7c4cbd1b56e339b982e07bf678add52", aum = 0)
    /* renamed from: XQ */
    private C3892wO m39598XQ() {
        return m39609yE();
    }

    @C0064Am(aul = "9ae2ead49aba3ba58959250c4b9296a7", aum = 0)
    /* renamed from: XS */
    private C3892wO m39599XS() {
        return m39610yF();
    }

    @C0064Am(aul = "559bdd7042bcd83e53ac9c29092de190", aum = 0)
    /* renamed from: XW */
    private C3892wO m39601XW() {
        return m39612yH();
    }

    @C0064Am(aul = "2c8b99d9f348bf791257a731a962dc39", aum = 0)
    /* renamed from: XU */
    private C3892wO m39600XU() {
        return m39611yG();
    }

    @C6485anp
    @C5511aMd
    /* renamed from: a.tT$a */
    public class SpeedBoosAmplifierAdapter extends SpeedBoostAdapter implements C1616Xf {
        public static final int _m_fieldCount = 0;
        public static final C5663aRz[] _m_fields = null;
        public static final int _m_methodCount = 0;
        public static final C2491fm[] _m_methods = null;
        /* renamed from: aT */
        public static final C5663aRz f9251aT = null;
        public static final long serialVersionUID = 0;
        /* renamed from: yc */
        public static final C2491fm f9252yc = null;
        /* renamed from: yd */
        public static final C2491fm f9253yd = null;
        /* renamed from: ye */
        public static final C2491fm f9254ye = null;
        /* renamed from: yf */
        public static final C2491fm f9255yf = null;
        /* renamed from: yg */
        public static final C2491fm f9256yg = null;
        public static C6494any ___iScriptClass;
        @C0064Am(aul = "c0802d594416fda03fe6fc138ff329d4", aum = 0)
        static /* synthetic */ SpeedBoostAmplifier btJ;

        static {
            m39628V();
        }

        public SpeedBoosAmplifierAdapter() {
            super((C5540aNg) null);
            super.mo10S();
        }

        public SpeedBoosAmplifierAdapter(C5540aNg ang) {
            super(ang);
        }

        public SpeedBoosAmplifierAdapter(SpeedBoostAmplifier tTVar) {
            super((C5540aNg) null);
            super._m_script_init(tTVar);
        }

        /* renamed from: V */
        static void m39628V() {
            serialVersionUID = (long) 1;
            _m_fieldCount = SpeedBoostAdapter._m_fieldCount + 1;
            _m_methodCount = SpeedBoostAdapter._m_methodCount + 5;
            int i = SpeedBoostAdapter._m_fieldCount;
            C5663aRz[] arzArr = new C5663aRz[(i + 1)];
            C5663aRz b = C5640aRc.m17844b(SpeedBoosAmplifierAdapter.class, "c0802d594416fda03fe6fc138ff329d4", i);
            f9251aT = b;
            arzArr[i] = b;
            int i2 = i + 1;
            _m_fields = arzArr;
            C1634Xv.m11725a((Object[]) SpeedBoostAdapter._m_fields, (Object[]) _m_fields);
            int i3 = SpeedBoostAdapter._m_methodCount;
            C2491fm[] fmVarArr = new C2491fm[(i3 + 5)];
            C2491fm a = C4105zY.m41624a(SpeedBoosAmplifierAdapter.class, "07607c3c9a835bc03cb9e0afb75da9df", i3);
            f9252yc = a;
            fmVarArr[i3] = a;
            int i4 = i3 + 1;
            C2491fm a2 = C4105zY.m41624a(SpeedBoosAmplifierAdapter.class, "f1d04542bea21527542482f5c1524b0b", i4);
            f9253yd = a2;
            fmVarArr[i4] = a2;
            int i5 = i4 + 1;
            C2491fm a3 = C4105zY.m41624a(SpeedBoosAmplifierAdapter.class, "b2e04048f6752ee2242d60a85b408f13", i5);
            f9254ye = a3;
            fmVarArr[i5] = a3;
            int i6 = i5 + 1;
            C2491fm a4 = C4105zY.m41624a(SpeedBoosAmplifierAdapter.class, "fb6762726833aa745385c92c8c8785a8", i6);
            f9255yf = a4;
            fmVarArr[i6] = a4;
            int i7 = i6 + 1;
            C2491fm a5 = C4105zY.m41624a(SpeedBoosAmplifierAdapter.class, "1e3c6d81d7210b3f1d30c3028d3a9f0c", i7);
            f9256yg = a5;
            fmVarArr[i7] = a5;
            int i8 = i7 + 1;
            _m_methods = fmVarArr;
            C1634Xv.m11725a((Object[]) SpeedBoostAdapter._m_methods, (Object[]) _m_methods);
            ___iScriptClass = aUO.m18566a(SpeedBoosAmplifierAdapter.class, C0038AV.class, _m_fields, _m_methods);
        }

        /* renamed from: a */
        private void m39629a(SpeedBoostAmplifier tTVar) {
            bFf().mo5608dq().mo3197f(f9251aT, tTVar);
        }

        private SpeedBoostAmplifier aiO() {
            return (SpeedBoostAmplifier) bFf().mo5608dq().mo3214p(f9251aT);
        }

        /* renamed from: S */
        public void mo10S() {
            super.mo10S();
        }

        /* renamed from: T */
        public C6494any mo11T() {
            return ___iScriptClass;
        }

        /* renamed from: U */
        public C2491fm[] mo12U() {
            return _m_methods;
        }

        /* renamed from: W */
        public Object mo13W() {
            return new C0038AV(this);
        }

        /* renamed from: a */
        public Object mo14a(C0495Gr gr) {
            gr.getArgs();
            switch (gr.mo2417hq() - SpeedBoostAdapter._m_methodCount) {
                case 0:
                    return m39630jk();
                case 1:
                    return m39631jm();
                case 2:
                    return m39632jo();
                case 3:
                    return m39633jq();
                case 4:
                    return new Float(m39634js());
                default:
                    return super.mo14a(gr);
            }
        }

        /* renamed from: a */
        public void mo15a(Collection collection, C0495Gr gr) {
            super.mo15a(collection, gr);
        }

        /* renamed from: c */
        public C5663aRz[] mo25c() {
            return _m_fields;
        }

        /* renamed from: jl */
        public C3892wO mo9299jl() {
            switch (bFf().mo6893i(f9252yc)) {
                case 0:
                    return null;
                case 2:
                    return (C3892wO) bFf().mo5606d(new aCE(this, f9252yc, new Object[0]));
                case 3:
                    bFf().mo5606d(new aCE(this, f9252yc, new Object[0]));
                    break;
            }
            return m39630jk();
        }

        /* renamed from: jn */
        public C3892wO mo9300jn() {
            switch (bFf().mo6893i(f9253yd)) {
                case 0:
                    return null;
                case 2:
                    return (C3892wO) bFf().mo5606d(new aCE(this, f9253yd, new Object[0]));
                case 3:
                    bFf().mo5606d(new aCE(this, f9253yd, new Object[0]));
                    break;
            }
            return m39631jm();
        }

        /* renamed from: jp */
        public C3892wO mo9301jp() {
            switch (bFf().mo6893i(f9254ye)) {
                case 0:
                    return null;
                case 2:
                    return (C3892wO) bFf().mo5606d(new aCE(this, f9254ye, new Object[0]));
                case 3:
                    bFf().mo5606d(new aCE(this, f9254ye, new Object[0]));
                    break;
            }
            return m39632jo();
        }

        /* renamed from: jr */
        public C3892wO mo9302jr() {
            switch (bFf().mo6893i(f9255yf)) {
                case 0:
                    return null;
                case 2:
                    return (C3892wO) bFf().mo5606d(new aCE(this, f9255yf, new Object[0]));
                case 3:
                    bFf().mo5606d(new aCE(this, f9255yf, new Object[0]));
                    break;
            }
            return m39633jq();
        }

        /* renamed from: jt */
        public float mo9303jt() {
            switch (bFf().mo6893i(f9256yg)) {
                case 0:
                    return 0.0f;
                case 2:
                    return ((Float) bFf().mo5606d(new aCE(this, f9256yg, new Object[0]))).floatValue();
                case 3:
                    bFf().mo5606d(new aCE(this, f9256yg, new Object[0]));
                    break;
            }
            return m39634js();
        }

        /* renamed from: b */
        public void mo22238b(SpeedBoostAmplifier tTVar) {
            m39629a(tTVar);
            super.mo10S();
        }

        @C0064Am(aul = "07607c3c9a835bc03cb9e0afb75da9df", aum = 0)
        /* renamed from: jk */
        private C3892wO m39630jk() {
            if (aiO().mo22236XX().getValue() == 0.0f) {
                return super.mo9299jl();
            }
            return aiO().mo22236XX().mo22745ah(super.mo9299jl());
        }

        @C0064Am(aul = "f1d04542bea21527542482f5c1524b0b", aum = 0)
        /* renamed from: jm */
        private C3892wO m39631jm() {
            if (aiO().mo22235XV().getValue() == 0.0f) {
                return super.mo9300jn();
            }
            return aiO().mo22235XV().mo22745ah(super.mo9300jn());
        }

        @C0064Am(aul = "b2e04048f6752ee2242d60a85b408f13", aum = 0)
        /* renamed from: jo */
        private C3892wO m39632jo() {
            if (aiO().mo22234XT().getValue() == 0.0f) {
                return super.mo9301jp();
            }
            return aiO().mo22234XT().mo22745ah(super.mo9301jp());
        }

        @C0064Am(aul = "fb6762726833aa745385c92c8c8785a8", aum = 0)
        /* renamed from: jq */
        private C3892wO m39633jq() {
            if (aiO().mo22233XR().getValue() == 0.0f) {
                return super.mo9302jr();
            }
            return aiO().mo22233XR().mo22745ah(super.mo9302jr());
        }

        @C0064Am(aul = "1e3c6d81d7210b3f1d30c3028d3a9f0c", aum = 0)
        /* renamed from: js */
        private float m39634js() {
            return aiO().mo22232Kw().mo22753v(((SpeedBoostAdapter) mo16070IE()).mo9303jt(), super.mo9303jt());
        }
    }
}
