package game.script.item.buff.amplifier;

import game.network.message.externalizable.aCE;
import game.script.item.AmplifierType;
import logic.baa.*;
import logic.data.mbean.aBW;
import logic.res.code.C5640aRc;
import logic.res.code.C5663aRz;
import logic.res.code.C6494any;
import logic.res.html.C2491fm;
import p001a.*;

import java.util.Collection;

@C0566Hp
@C5511aMd
@C6485anp
/* renamed from: a.wn */
/* compiled from: a */
public class HazardShieldAmplifierType extends AmplifierType implements C1616Xf {
    public static final int _m_fieldCount = 0;
    public static final C5663aRz[] _m_fields = null;
    public static final int _m_methodCount = 0;
    public static final C2491fm[] _m_methods = null;
    public static final C2491fm axb = null;
    public static final C2491fm bCK = null;
    public static final C5663aRz bbI = null;
    public static final C2491fm bbN = null;
    /* renamed from: dN */
    public static final C2491fm f9496dN = null;
    public static final long serialVersionUID = 0;
    public static C6494any ___iScriptClass;
    @C0064Am(aul = "38339fb15c546b68a40258858842f2c5", aum = 0)
    private static C3892wO bbH;

    static {
        m40732V();
    }

    public HazardShieldAmplifierType() {
        super((C5540aNg) null);
        super.mo10S();
    }

    public HazardShieldAmplifierType(C5540aNg ang) {
        super(ang);
    }

    /* renamed from: V */
    static void m40732V() {
        serialVersionUID = (long) 1;
        _m_fieldCount = AmplifierType._m_fieldCount + 1;
        _m_methodCount = AmplifierType._m_methodCount + 4;
        int i = AmplifierType._m_fieldCount;
        C5663aRz[] arzArr = new C5663aRz[(i + 1)];
        C5663aRz b = C5640aRc.m17844b(HazardShieldAmplifierType.class, "38339fb15c546b68a40258858842f2c5", i);
        bbI = b;
        arzArr[i] = b;
        int i2 = i + 1;
        _m_fields = arzArr;
        C1634Xv.m11725a((Object[]) AmplifierType._m_fields, (Object[]) _m_fields);
        int i3 = AmplifierType._m_methodCount;
        C2491fm[] fmVarArr = new C2491fm[(i3 + 4)];
        C2491fm a = C4105zY.m41624a(HazardShieldAmplifierType.class, "72747082b32ed31f5ab1725a80652f55", i3);
        axb = a;
        fmVarArr[i3] = a;
        int i4 = i3 + 1;
        C2491fm a2 = C4105zY.m41624a(HazardShieldAmplifierType.class, "072c4475fbc76ffa7ffac95bd5e36747", i4);
        bbN = a2;
        fmVarArr[i4] = a2;
        int i5 = i4 + 1;
        C2491fm a3 = C4105zY.m41624a(HazardShieldAmplifierType.class, "e5a82a8d851f93fb944394f3fe3daeb1", i5);
        bCK = a3;
        fmVarArr[i5] = a3;
        int i6 = i5 + 1;
        C2491fm a4 = C4105zY.m41624a(HazardShieldAmplifierType.class, "6f8907749a7454e5754e814ee4224bbd", i6);
        f9496dN = a4;
        fmVarArr[i6] = a4;
        int i7 = i6 + 1;
        _m_methods = fmVarArr;
        C1634Xv.m11725a((Object[]) AmplifierType._m_methods, (Object[]) _m_methods);
        ___iScriptClass = aUO.m18566a(HazardShieldAmplifierType.class, aBW.class, _m_fields, _m_methods);
    }

    /* renamed from: Yv */
    private C3892wO m40733Yv() {
        return (C3892wO) bFf().mo5608dq().mo3214p(bbI);
    }

    /* renamed from: x */
    private void m40735x(C3892wO wOVar) {
        bFf().mo5608dq().mo3197f(bbI, wOVar);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Shield Life Reduction")
    /* renamed from: LG */
    public C3892wO mo22813LG() {
        switch (bFf().mo6893i(axb)) {
            case 0:
                return null;
            case 2:
                return (C3892wO) bFf().mo5606d(new aCE(this, axb, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, axb, new Object[0]));
                break;
        }
        return m40731LF();
    }

    /* renamed from: T */
    public C6494any mo11T() {
        return ___iScriptClass;
    }

    /* renamed from: U */
    public C2491fm[] mo12U() {
        return _m_methods;
    }

    /* renamed from: W */
    public Object mo13W() {
        return new aBW(this);
    }

    /* renamed from: a */
    public Object mo14a(C0495Gr gr) {
        Object[] args = gr.getArgs();
        switch (gr.mo2417hq() - AmplifierType._m_methodCount) {
            case 0:
                return m40731LF();
            case 1:
                m40736y((C3892wO) args[0]);
                return null;
            case 2:
                return amr();
            case 3:
                return m40734aT();
            default:
                return super.mo14a(gr);
        }
    }

    /* renamed from: a */
    public void mo15a(Collection collection, C0495Gr gr) {
        super.mo15a(collection, gr);
    }

    /* access modifiers changed from: protected */
    /* renamed from: aU */
    public <T extends aDJ> T mo745aU() {
        switch (bFf().mo6893i(f9496dN)) {
            case 0:
                return null;
            case 2:
                return (aDJ) bFf().mo5606d(new aCE(this, f9496dN, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f9496dN, new Object[0]));
                break;
        }
        return m40734aT();
    }

    public HazardShieldAmplifier ams() {
        switch (bFf().mo6893i(bCK)) {
            case 0:
                return null;
            case 2:
                return (HazardShieldAmplifier) bFf().mo5606d(new aCE(this, bCK, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, bCK, new Object[0]));
                break;
        }
        return amr();
    }

    /* renamed from: c */
    public C5663aRz[] mo25c() {
        return _m_fields;
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Shield Life Reduction")
    /* renamed from: z */
    public void mo22815z(C3892wO wOVar) {
        switch (bFf().mo6893i(bbN)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, bbN, new Object[]{wOVar}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, bbN, new Object[]{wOVar}));
                break;
        }
        m40736y(wOVar);
    }

    /* renamed from: S */
    public void mo10S() {
        super.mo10S();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Shield Life Reduction")
    @C0064Am(aul = "72747082b32ed31f5ab1725a80652f55", aum = 0)
    /* renamed from: LF */
    private C3892wO m40731LF() {
        return m40733Yv();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Shield Life Reduction")
    @C0064Am(aul = "072c4475fbc76ffa7ffac95bd5e36747", aum = 0)
    /* renamed from: y */
    private void m40736y(C3892wO wOVar) {
        m40735x(wOVar);
    }

    @C0064Am(aul = "e5a82a8d851f93fb944394f3fe3daeb1", aum = 0)
    private HazardShieldAmplifier amr() {
        return (HazardShieldAmplifier) mo745aU();
    }

    @C0064Am(aul = "6f8907749a7454e5754e814ee4224bbd", aum = 0)
    /* renamed from: aT */
    private <T extends aDJ> T m40734aT() {
        T t = (HazardShieldAmplifier) bFf().mo6865M(HazardShieldAmplifier.class);
        t.mo1956e(this);
        return t;
    }
}
