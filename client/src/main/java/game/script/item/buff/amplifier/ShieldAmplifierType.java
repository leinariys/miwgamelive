package game.script.item.buff.amplifier;

import game.network.message.externalizable.aCE;
import game.script.item.AmplifierType;
import logic.baa.*;
import logic.data.mbean.C6250ajO;
import logic.res.code.C5640aRc;
import logic.res.code.C5663aRz;
import logic.res.code.C6494any;
import logic.res.html.C2491fm;
import p001a.*;

import java.util.Collection;

@C0566Hp
@C5829abJ("2.0.1")
@C6485anp
@C5511aMd
/* renamed from: a.EO */
/* compiled from: a */
public class ShieldAmplifierType extends AmplifierType implements C1616Xf {
    public static final int _m_fieldCount = 0;
    public static final C5663aRz[] _m_fields = null;
    public static final int _m_methodCount = 0;
    public static final C2491fm[] _m_methods = null;
    public static final C5663aRz aOJ = null;
    public static final C5663aRz aOr = null;
    public static final C5663aRz aOt = null;
    public static final C5663aRz aOz = null;
    public static final C2491fm cTX = null;
    public static final C2491fm cTY = null;
    public static final C2491fm cTZ = null;
    public static final C2491fm cUa = null;
    public static final C2491fm cUb = null;
    public static final C2491fm cqF = null;
    public static final C2491fm cqG = null;
    public static final C2491fm cqH = null;
    public static final C2491fm cqI = null;
    /* renamed from: dN */
    public static final C2491fm f484dN = null;
    public static final long serialVersionUID = 0;
    public static C6494any ___iScriptClass;
    @C0064Am(aul = "7d5695cb56bf06ee445e2c5b08beae88", aum = 0)
    private static C3892wO aBJ;
    @C0064Am(aul = "57d379b003bf15f43de78a1dedfdb204", aum = 1)
    private static C3892wO aBK;
    @C0064Am(aul = "e455d77f1bcca35c259fd0924d9176a1", aum = 2)
    private static C3892wO aBL;
    @C0064Am(aul = "9c44336dc537b04308c2dd3227fa72f3", aum = 3)
    private static C3892wO aBM;

    static {
        m2810V();
    }

    public ShieldAmplifierType() {
        super((C5540aNg) null);
        super.mo10S();
    }

    public ShieldAmplifierType(C5540aNg ang) {
        super(ang);
    }

    /* renamed from: V */
    static void m2810V() {
        serialVersionUID = (long) 1;
        _m_fieldCount = AmplifierType._m_fieldCount + 4;
        _m_methodCount = AmplifierType._m_methodCount + 10;
        int i = AmplifierType._m_fieldCount;
        C5663aRz[] arzArr = new C5663aRz[(i + 4)];
        C5663aRz b = C5640aRc.m17844b(ShieldAmplifierType.class, "7d5695cb56bf06ee445e2c5b08beae88", i);
        aOJ = b;
        arzArr[i] = b;
        int i2 = i + 1;
        C5663aRz b2 = C5640aRc.m17844b(ShieldAmplifierType.class, "57d379b003bf15f43de78a1dedfdb204", i2);
        aOr = b2;
        arzArr[i2] = b2;
        int i3 = i2 + 1;
        C5663aRz b3 = C5640aRc.m17844b(ShieldAmplifierType.class, "e455d77f1bcca35c259fd0924d9176a1", i3);
        aOz = b3;
        arzArr[i3] = b3;
        int i4 = i3 + 1;
        C5663aRz b4 = C5640aRc.m17844b(ShieldAmplifierType.class, "9c44336dc537b04308c2dd3227fa72f3", i4);
        aOt = b4;
        arzArr[i4] = b4;
        int i5 = i4 + 1;
        _m_fields = arzArr;
        C1634Xv.m11725a((Object[]) AmplifierType._m_fields, (Object[]) _m_fields);
        int i6 = AmplifierType._m_methodCount;
        C2491fm[] fmVarArr = new C2491fm[(i6 + 10)];
        C2491fm a = C4105zY.m41624a(ShieldAmplifierType.class, "b2bdb47e1f28119f71e22883cfeb8952", i6);
        cqF = a;
        fmVarArr[i6] = a;
        int i7 = i6 + 1;
        C2491fm a2 = C4105zY.m41624a(ShieldAmplifierType.class, "a7a22b86dd3d7f400bd0b7c95e36e466", i7);
        cqG = a2;
        fmVarArr[i7] = a2;
        int i8 = i7 + 1;
        C2491fm a3 = C4105zY.m41624a(ShieldAmplifierType.class, "6134e01b27be64d0efb206b782024381", i8);
        cqH = a3;
        fmVarArr[i8] = a3;
        int i9 = i8 + 1;
        C2491fm a4 = C4105zY.m41624a(ShieldAmplifierType.class, "2c09981737e6c0d57dcde45fc084df33", i9);
        cqI = a4;
        fmVarArr[i9] = a4;
        int i10 = i9 + 1;
        C2491fm a5 = C4105zY.m41624a(ShieldAmplifierType.class, "59c1eb8b5abc6734b120f3ee36293f1b", i10);
        cTX = a5;
        fmVarArr[i10] = a5;
        int i11 = i10 + 1;
        C2491fm a6 = C4105zY.m41624a(ShieldAmplifierType.class, "7e0f38b38a142a8564a897b9979a7c96", i11);
        cTY = a6;
        fmVarArr[i11] = a6;
        int i12 = i11 + 1;
        C2491fm a7 = C4105zY.m41624a(ShieldAmplifierType.class, "d4dd5a5952c9c48fbe48aa0f2a18ba29", i12);
        cTZ = a7;
        fmVarArr[i12] = a7;
        int i13 = i12 + 1;
        C2491fm a8 = C4105zY.m41624a(ShieldAmplifierType.class, "6aebc348d937784087b81aab6fe4e094", i13);
        cUa = a8;
        fmVarArr[i13] = a8;
        int i14 = i13 + 1;
        C2491fm a9 = C4105zY.m41624a(ShieldAmplifierType.class, "9c4892df5cac268f0ed7c586abf7df9f", i14);
        cUb = a9;
        fmVarArr[i14] = a9;
        int i15 = i14 + 1;
        C2491fm a10 = C4105zY.m41624a(ShieldAmplifierType.class, "49499f2f7109ba0aeb938f4b73d6f4e3", i15);
        f484dN = a10;
        fmVarArr[i15] = a10;
        int i16 = i15 + 1;
        _m_methods = fmVarArr;
        C1634Xv.m11725a((Object[]) AmplifierType._m_methods, (Object[]) _m_methods);
        ___iScriptClass = aUO.m18566a(ShieldAmplifierType.class, C6250ajO.class, _m_fields, _m_methods);
    }

    /* renamed from: U */
    private void m2807U(C3892wO wOVar) {
        bFf().mo5608dq().mo3197f(aOz, wOVar);
    }

    /* renamed from: Ub */
    private C3892wO m2808Ub() {
        return (C3892wO) bFf().mo5608dq().mo3214p(aOJ);
    }

    /* renamed from: Uc */
    private C3892wO m2809Uc() {
        return (C3892wO) bFf().mo5608dq().mo3214p(aOr);
    }

    /* renamed from: V */
    private void m2811V(C3892wO wOVar) {
        bFf().mo5608dq().mo3197f(aOt, wOVar);
    }

    private C3892wO aOS() {
        return (C3892wO) bFf().mo5608dq().mo3214p(aOz);
    }

    private C3892wO aOT() {
        return (C3892wO) bFf().mo5608dq().mo3214p(aOt);
    }

    /* renamed from: v */
    private void m2815v(C3892wO wOVar) {
        bFf().mo5608dq().mo3197f(aOJ, wOVar);
    }

    /* renamed from: w */
    private void m2816w(C3892wO wOVar) {
        bFf().mo5608dq().mo3197f(aOr, wOVar);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Health Points")
    /* renamed from: H */
    public void mo1831H(C3892wO wOVar) {
        switch (bFf().mo6893i(cqG)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, cqG, new Object[]{wOVar}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, cqG, new Object[]{wOVar}));
                break;
        }
        m2805G(wOVar);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Damage Reduction")
    /* renamed from: J */
    public void mo1832J(C3892wO wOVar) {
        switch (bFf().mo6893i(cqI)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, cqI, new Object[]{wOVar}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, cqI, new Object[]{wOVar}));
                break;
        }
        m2806I(wOVar);
    }

    /* renamed from: T */
    public C6494any mo11T() {
        return ___iScriptClass;
    }

    /* renamed from: U */
    public C2491fm[] mo12U() {
        return _m_methods;
    }

    /* renamed from: W */
    public Object mo13W() {
        return new C6250ajO(this);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Regeneration")
    /* renamed from: X */
    public void mo1833X(C3892wO wOVar) {
        switch (bFf().mo6893i(cTY)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, cTY, new Object[]{wOVar}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, cTY, new Object[]{wOVar}));
                break;
        }
        m2812W(wOVar);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Recovery Time")
    /* renamed from: Z */
    public void mo1834Z(C3892wO wOVar) {
        switch (bFf().mo6893i(cUa)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, cUa, new Object[]{wOVar}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, cUa, new Object[]{wOVar}));
                break;
        }
        m2813Y(wOVar);
    }

    /* renamed from: a */
    public Object mo14a(C0495Gr gr) {
        Object[] args = gr.getArgs();
        switch (gr.mo2417hq() - AmplifierType._m_methodCount) {
            case 0:
                return aBb();
            case 1:
                m2805G((C3892wO) args[0]);
                return null;
            case 2:
                return aBd();
            case 3:
                m2806I((C3892wO) args[0]);
                return null;
            case 4:
                return aOU();
            case 5:
                m2812W((C3892wO) args[0]);
                return null;
            case 6:
                return aOW();
            case 7:
                m2813Y((C3892wO) args[0]);
                return null;
            case 8:
                return aOY();
            case 9:
                return m2814aT();
            default:
                return super.mo14a(gr);
        }
    }

    /* renamed from: a */
    public void mo15a(Collection collection, C0495Gr gr) {
        super.mo15a(collection, gr);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Health Points")
    public C3892wO aBc() {
        switch (bFf().mo6893i(cqF)) {
            case 0:
                return null;
            case 2:
                return (C3892wO) bFf().mo5606d(new aCE(this, cqF, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, cqF, new Object[0]));
                break;
        }
        return aBb();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Damage Reduction")
    public C3892wO aBe() {
        switch (bFf().mo6893i(cqH)) {
            case 0:
                return null;
            case 2:
                return (C3892wO) bFf().mo5606d(new aCE(this, cqH, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, cqH, new Object[0]));
                break;
        }
        return aBd();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Regeneration")
    public C3892wO aOV() {
        switch (bFf().mo6893i(cTX)) {
            case 0:
                return null;
            case 2:
                return (C3892wO) bFf().mo5606d(new aCE(this, cTX, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, cTX, new Object[0]));
                break;
        }
        return aOU();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Recovery Time")
    public C3892wO aOX() {
        switch (bFf().mo6893i(cTZ)) {
            case 0:
                return null;
            case 2:
                return (C3892wO) bFf().mo5606d(new aCE(this, cTZ, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, cTZ, new Object[0]));
                break;
        }
        return aOW();
    }

    public ShieldAmplifier aOZ() {
        switch (bFf().mo6893i(cUb)) {
            case 0:
                return null;
            case 2:
                return (ShieldAmplifier) bFf().mo5606d(new aCE(this, cUb, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, cUb, new Object[0]));
                break;
        }
        return aOY();
    }

    /* access modifiers changed from: protected */
    /* renamed from: aU */
    public <T extends aDJ> T mo745aU() {
        switch (bFf().mo6893i(f484dN)) {
            case 0:
                return null;
            case 2:
                return (aDJ) bFf().mo5606d(new aCE(this, f484dN, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f484dN, new Object[0]));
                break;
        }
        return m2814aT();
    }

    /* renamed from: c */
    public C5663aRz[] mo25c() {
        return _m_fields;
    }

    /* renamed from: S */
    public void mo10S() {
        super.mo10S();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Health Points")
    @C0064Am(aul = "b2bdb47e1f28119f71e22883cfeb8952", aum = 0)
    private C3892wO aBb() {
        return m2808Ub();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Health Points")
    @C0064Am(aul = "a7a22b86dd3d7f400bd0b7c95e36e466", aum = 0)
    /* renamed from: G */
    private void m2805G(C3892wO wOVar) {
        m2815v(wOVar);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Damage Reduction")
    @C0064Am(aul = "6134e01b27be64d0efb206b782024381", aum = 0)
    private C3892wO aBd() {
        return m2809Uc();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Damage Reduction")
    @C0064Am(aul = "2c09981737e6c0d57dcde45fc084df33", aum = 0)
    /* renamed from: I */
    private void m2806I(C3892wO wOVar) {
        m2816w(wOVar);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Regeneration")
    @C0064Am(aul = "59c1eb8b5abc6734b120f3ee36293f1b", aum = 0)
    private C3892wO aOU() {
        return aOS();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Regeneration")
    @C0064Am(aul = "7e0f38b38a142a8564a897b9979a7c96", aum = 0)
    /* renamed from: W */
    private void m2812W(C3892wO wOVar) {
        m2807U(wOVar);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Recovery Time")
    @C0064Am(aul = "d4dd5a5952c9c48fbe48aa0f2a18ba29", aum = 0)
    private C3892wO aOW() {
        return aOT();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Recovery Time")
    @C0064Am(aul = "6aebc348d937784087b81aab6fe4e094", aum = 0)
    /* renamed from: Y */
    private void m2813Y(C3892wO wOVar) {
        m2811V(wOVar);
    }

    @C0064Am(aul = "9c4892df5cac268f0ed7c586abf7df9f", aum = 0)
    private ShieldAmplifier aOY() {
        return (ShieldAmplifier) mo745aU();
    }

    @C0064Am(aul = "49499f2f7109ba0aeb938f4b73d6f4e3", aum = 0)
    /* renamed from: aT */
    private <T extends aDJ> T m2814aT() {
        T t = (ShieldAmplifier) bFf().mo6865M(ShieldAmplifier.class);
        t.mo1956e(this);
        return t;
    }
}
