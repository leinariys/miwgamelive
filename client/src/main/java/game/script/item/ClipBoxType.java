package game.script.item;

import game.network.message.externalizable.aCE;
import logic.baa.*;
import logic.data.mbean.C6389alx;
import logic.res.code.C5640aRc;
import logic.res.code.C5663aRz;
import logic.res.code.C6494any;
import logic.res.html.C2491fm;
import p001a.*;

import java.util.Collection;

@C0566Hp
@C5511aMd
@C6485anp
/* renamed from: a.aTy  reason: case insensitive filesystem */
/* compiled from: a */
public class ClipBoxType extends MerchandiseType implements C1616Xf {
    public static final int _m_fieldCount = 0;
    public static final C5663aRz[] _m_fields = null;
    public static final int _m_methodCount = 0;
    public static final C2491fm[] _m_methods = null;
    /* renamed from: dN */
    public static final C2491fm f3839dN = null;
    public static final C5663aRz eQh = null;
    public static final C5663aRz eQi = null;
    public static final C2491fm iPT = null;
    public static final C2491fm iPU = null;
    public static final C2491fm iPV = null;
    public static final C2491fm iPW = null;
    public static final C2491fm iPX = null;
    public static final long serialVersionUID = 0;
    public static C6494any ___iScriptClass;
    @C0064Am(aul = "526bdb6be6a0ff13f97a75ff8bf5c45f", aum = 0)
    private static int aVH;
    @C0064Am(aul = "e1481ad19c7b505f3d0d306884873ac5", aum = 1)
    private static ClipType aVI;

    static {
        m18475V();
    }

    public ClipBoxType() {
        super((C5540aNg) null);
        super.mo10S();
    }

    public ClipBoxType(C5540aNg ang) {
        super(ang);
    }

    /* renamed from: V */
    static void m18475V() {
        serialVersionUID = (long) 1;
        _m_fieldCount = MerchandiseType._m_fieldCount + 2;
        _m_methodCount = MerchandiseType._m_methodCount + 6;
        int i = MerchandiseType._m_fieldCount;
        C5663aRz[] arzArr = new C5663aRz[(i + 2)];
        C5663aRz b = C5640aRc.m17844b(ClipBoxType.class, "526bdb6be6a0ff13f97a75ff8bf5c45f", i);
        eQh = b;
        arzArr[i] = b;
        int i2 = i + 1;
        C5663aRz b2 = C5640aRc.m17844b(ClipBoxType.class, "e1481ad19c7b505f3d0d306884873ac5", i2);
        eQi = b2;
        arzArr[i2] = b2;
        int i3 = i2 + 1;
        _m_fields = arzArr;
        C1634Xv.m11725a((Object[]) MerchandiseType._m_fields, (Object[]) _m_fields);
        int i4 = MerchandiseType._m_methodCount;
        C2491fm[] fmVarArr = new C2491fm[(i4 + 6)];
        C2491fm a = C4105zY.m41624a(ClipBoxType.class, "2a776729bf1a7611987a8041ac6ef324", i4);
        iPT = a;
        fmVarArr[i4] = a;
        int i5 = i4 + 1;
        C2491fm a2 = C4105zY.m41624a(ClipBoxType.class, "dc1982ff540224cab4f8d84e3a5d5bda", i5);
        iPU = a2;
        fmVarArr[i5] = a2;
        int i6 = i5 + 1;
        C2491fm a3 = C4105zY.m41624a(ClipBoxType.class, "66dcab368450208449da3f5df4caf1b7", i6);
        iPV = a3;
        fmVarArr[i6] = a3;
        int i7 = i6 + 1;
        C2491fm a4 = C4105zY.m41624a(ClipBoxType.class, "74052903ef51af1c2be62f735326b40f", i7);
        iPW = a4;
        fmVarArr[i7] = a4;
        int i8 = i7 + 1;
        C2491fm a5 = C4105zY.m41624a(ClipBoxType.class, "f11a955efb0233b0c4b427acf731b75b", i8);
        iPX = a5;
        fmVarArr[i8] = a5;
        int i9 = i8 + 1;
        C2491fm a6 = C4105zY.m41624a(ClipBoxType.class, "16a1ffdd71b762d1ee48939fe06ccfd0", i9);
        f3839dN = a6;
        fmVarArr[i9] = a6;
        int i10 = i9 + 1;
        _m_methods = fmVarArr;
        C1634Xv.m11725a((Object[]) MerchandiseType._m_methods, (Object[]) _m_methods);
        ___iScriptClass = aUO.m18566a(ClipBoxType.class, C6389alx.class, _m_fields, _m_methods);
    }

    private int bJd() {
        return bFf().mo5608dq().mo3212n(eQh);
    }

    private ClipType bJe() {
        return (ClipType) bFf().mo5608dq().mo3214p(eQi);
    }

    /* renamed from: m */
    private void m18477m(ClipType aqf) {
        bFf().mo5608dq().mo3197f(eQi, aqf);
    }

    /* renamed from: pp */
    private void m18479pp(int i) {
        bFf().mo5608dq().mo3183b(eQh, i);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Amount Of Bullets")
    /* renamed from: AS */
    public void mo11586AS(int i) {
        switch (bFf().mo6893i(iPU)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, iPU, new Object[]{new Integer(i)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, iPU, new Object[]{new Integer(i)}));
                break;
        }
        m18474AR(i);
    }

    /* renamed from: T */
    public C6494any mo11T() {
        return ___iScriptClass;
    }

    /* renamed from: U */
    public C2491fm[] mo12U() {
        return _m_methods;
    }

    /* renamed from: W */
    public Object mo13W() {
        return new C6389alx(this);
    }

    /* renamed from: a */
    public Object mo14a(C0495Gr gr) {
        Object[] args = gr.getArgs();
        switch (gr.mo2417hq() - MerchandiseType._m_methodCount) {
            case 0:
                return new Integer(dvW());
            case 1:
                m18474AR(((Integer) args[0]).intValue());
                return null;
            case 2:
                return dvY();
            case 3:
                m18478p((ClipType) args[0]);
                return null;
            case 4:
                return dwa();
            case 5:
                return m18476aT();
            default:
                return super.mo14a(gr);
        }
    }

    /* renamed from: a */
    public void mo15a(Collection collection, C0495Gr gr) {
        super.mo15a(collection, gr);
    }

    /* access modifiers changed from: protected */
    /* renamed from: aU */
    public <T extends aDJ> T mo745aU() {
        switch (bFf().mo6893i(f3839dN)) {
            case 0:
                return null;
            case 2:
                return (aDJ) bFf().mo5606d(new aCE(this, f3839dN, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f3839dN, new Object[0]));
                break;
        }
        return m18476aT();
    }

    /* renamed from: c */
    public C5663aRz[] mo25c() {
        return _m_fields;
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Amount Of Bullets")
    public int dvX() {
        switch (bFf().mo6893i(iPT)) {
            case 0:
                return 0;
            case 2:
                return ((Integer) bFf().mo5606d(new aCE(this, iPT, new Object[0]))).intValue();
            case 3:
                bFf().mo5606d(new aCE(this, iPT, new Object[0]));
                break;
        }
        return dvW();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Related Clip")
    public ClipType dvZ() {
        switch (bFf().mo6893i(iPV)) {
            case 0:
                return null;
            case 2:
                return (ClipType) bFf().mo5606d(new aCE(this, iPV, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, iPV, new Object[0]));
                break;
        }
        return dvY();
    }

    public ClipBox dwb() {
        switch (bFf().mo6893i(iPX)) {
            case 0:
                return null;
            case 2:
                return (ClipBox) bFf().mo5606d(new aCE(this, iPX, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, iPX, new Object[0]));
                break;
        }
        return dwa();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Related Clip")
    /* renamed from: q */
    public void mo11590q(ClipType aqf) {
        switch (bFf().mo6893i(iPW)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, iPW, new Object[]{aqf}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, iPW, new Object[]{aqf}));
                break;
        }
        m18478p(aqf);
    }

    /* renamed from: S */
    public void mo10S() {
        super.mo10S();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Amount Of Bullets")
    @C0064Am(aul = "2a776729bf1a7611987a8041ac6ef324", aum = 0)
    private int dvW() {
        return bJd();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Amount Of Bullets")
    @C0064Am(aul = "dc1982ff540224cab4f8d84e3a5d5bda", aum = 0)
    /* renamed from: AR */
    private void m18474AR(int i) {
        m18479pp(i);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Related Clip")
    @C0064Am(aul = "66dcab368450208449da3f5df4caf1b7", aum = 0)
    private ClipType dvY() {
        return bJe();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Related Clip")
    @C0064Am(aul = "74052903ef51af1c2be62f735326b40f", aum = 0)
    /* renamed from: p */
    private void m18478p(ClipType aqf) {
        m18477m(aqf);
    }

    @C0064Am(aul = "f11a955efb0233b0c4b427acf731b75b", aum = 0)
    private ClipBox dwa() {
        return (ClipBox) mo745aU();
    }

    @C0064Am(aul = "16a1ffdd71b762d1ee48939fe06ccfd0", aum = 0)
    /* renamed from: aT */
    private <T extends aDJ> T m18476aT() {
        T t = (ClipBox) bFf().mo6865M(ClipBox.class);
        t.mo7378g(this);
        return t;
    }
}
