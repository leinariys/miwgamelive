package game.script.item;

import game.network.message.externalizable.aCE;
import game.script.damage.DamageType;
import game.script.resource.Asset;
import logic.baa.C1616Xf;
import logic.baa.C1634Xv;
import logic.baa.C5511aMd;
import logic.baa.C6485anp;
import logic.data.mbean.C0291Dl;
import logic.res.code.C5640aRc;
import logic.res.code.C5663aRz;
import logic.res.code.C6494any;
import logic.res.html.C2491fm;
import p001a.*;

import java.util.Collection;
import java.util.Map;

@C5829abJ("2.1.0")
@C6485anp
@C2712iu(version = "2.1.1")
@C5511aMd
/* renamed from: a.yG */
/* compiled from: a */
public class EnergyWeapon extends Weapon implements C1616Xf {

    /* renamed from: Lm */
    public static final C2491fm f9577Lm = null;
    public static final C2491fm _f_onResurrect_0020_0028_0029V = null;
    public static final int _m_fieldCount = 0;
    public static final C5663aRz[] _m_fields = null;
    public static final int _m_methodCount = 0;
    public static final C2491fm[] _m_methods = null;
    public static final C5663aRz bLY = null;
    public static final C5663aRz bLZ = null;
    public static final C5663aRz bMa = null;
    public static final C5663aRz bMb = null;
    public static final C5663aRz bMd = null;
    public static final C5663aRz bMf = null;
    public static final C2491fm bMg = null;
    public static final C2491fm bMh = null;
    public static final C2491fm bMi = null;
    public static final C2491fm bMj = null;
    public static final C2491fm bMk = null;
    public static final C2491fm bMl = null;
    public static final C2491fm bMm = null;
    public static final C2491fm bMn = null;
    public static final C2491fm bMo = null;
    public static final C2491fm bMp = null;
    public static final C2491fm bMq = null;
    public static final C2491fm bMr = null;
    public static final C2491fm bMs = null;
    public static final long serialVersionUID = 0;
    /* renamed from: uU */
    public static final C5663aRz f9579uU = null;
    /* renamed from: uW */
    public static final C2491fm f9580uW = null;
    public static C6494any ___iScriptClass;
    @C0064Am(aul = "eacf7bfd43b7af5e5a194f7f030b9544", aum = 0)
    private static int auV;
    @C0064Am(aul = "0d26b2ad4e4957cf73ae6a33f629331f", aum = 1)
    private static int auW;
    @C0064Am(aul = "89281fa210cb15b7e1ddefefda827425", aum = 2)
    private static Asset auX;
    @C0064Am(aul = "134ed211d5459d33372dd101163f53b6", aum = 3)
    private static float auY;
    @C3122oB(mo20937Uq = C3122oB.C3123a.TIME_BASED, mo20938Ur = 1000)
    @C0064Am(aul = "56585247b09cc94209f013cfb8ee822b", aum = 5)
    @C1020Ot(mo4553Ur = 5000, drz = C1020Ot.C1021a.TIME_BASED)
    @C5256aCi
    private static int bMc;
    @C3122oB(mo20937Uq = C3122oB.C3123a.TIME_BASED, mo20938Ur = 1000)
    @C0064Am(aul = "6fa2113c19ac51ceb1efaadd2b384a68", aum = 6)
    @C1020Ot(drz = C1020Ot.C1021a.NOT_PERSISTED)
    @C5256aCi
    private static long bMe;
    @C0064Am(aul = "5851f5a508f287cc734e4fd99cbaf614", aum = 4)
    @C5454aJy("Energy Shots' Surface Material")

    /* renamed from: uT */
    private static String f9578uT;

    static {
        m41233V();
    }

    public EnergyWeapon() {
        super((C5540aNg) null);
        super.mo10S();
    }

    public EnergyWeapon(C5540aNg ang) {
        super(ang);
    }

    public EnergyWeapon(EnergyWeaponType agc) {
        super((C5540aNg) null);
        super._m_script_init(agc);
    }

    /* renamed from: V */
    static void m41233V() {
        serialVersionUID = (long) 1;
        _m_fieldCount = Weapon._m_fieldCount + 7;
        _m_methodCount = Weapon._m_methodCount + 16;
        int i = Weapon._m_fieldCount;
        C5663aRz[] arzArr = new C5663aRz[(i + 7)];
        C5663aRz b = C5640aRc.m17844b(EnergyWeapon.class, "eacf7bfd43b7af5e5a194f7f030b9544", i);
        bLY = b;
        arzArr[i] = b;
        int i2 = i + 1;
        C5663aRz b2 = C5640aRc.m17844b(EnergyWeapon.class, "0d26b2ad4e4957cf73ae6a33f629331f", i2);
        bLZ = b2;
        arzArr[i2] = b2;
        int i3 = i2 + 1;
        C5663aRz b3 = C5640aRc.m17844b(EnergyWeapon.class, "89281fa210cb15b7e1ddefefda827425", i3);
        bMa = b3;
        arzArr[i3] = b3;
        int i4 = i3 + 1;
        C5663aRz b4 = C5640aRc.m17844b(EnergyWeapon.class, "134ed211d5459d33372dd101163f53b6", i4);
        bMb = b4;
        arzArr[i4] = b4;
        int i5 = i4 + 1;
        C5663aRz b5 = C5640aRc.m17844b(EnergyWeapon.class, "5851f5a508f287cc734e4fd99cbaf614", i5);
        f9579uU = b5;
        arzArr[i5] = b5;
        int i6 = i5 + 1;
        C5663aRz b6 = C5640aRc.m17844b(EnergyWeapon.class, "56585247b09cc94209f013cfb8ee822b", i6);
        bMd = b6;
        arzArr[i6] = b6;
        int i7 = i6 + 1;
        C5663aRz b7 = C5640aRc.m17844b(EnergyWeapon.class, "6fa2113c19ac51ceb1efaadd2b384a68", i7);
        bMf = b7;
        arzArr[i7] = b7;
        int i8 = i7 + 1;
        _m_fields = arzArr;
        C1634Xv.m11725a((Object[]) Weapon._m_fields, (Object[]) _m_fields);
        int i9 = Weapon._m_methodCount;
        C2491fm[] fmVarArr = new C2491fm[(i9 + 16)];
        C2491fm a = C4105zY.m41624a(EnergyWeapon.class, "61034fd9f21b0805fc8af13a40338837", i9);
        bMg = a;
        fmVarArr[i9] = a;
        int i10 = i9 + 1;
        C2491fm a2 = C4105zY.m41624a(EnergyWeapon.class, "8906c04274fa97a364eb9456a689a9cb", i10);
        bMh = a2;
        fmVarArr[i10] = a2;
        int i11 = i10 + 1;
        C2491fm a3 = C4105zY.m41624a(EnergyWeapon.class, "e45ffa462f4f59ecaaa02dcaeccafde6", i11);
        bMi = a3;
        fmVarArr[i11] = a3;
        int i12 = i11 + 1;
        C2491fm a4 = C4105zY.m41624a(EnergyWeapon.class, "8db52d23ccba658f4e02306d6d59fab5", i12);
        bMj = a4;
        fmVarArr[i12] = a4;
        int i13 = i12 + 1;
        C2491fm a5 = C4105zY.m41624a(EnergyWeapon.class, "5c005f07001162af0e52a8931a4ec051", i13);
        bMk = a5;
        fmVarArr[i13] = a5;
        int i14 = i13 + 1;
        C2491fm a6 = C4105zY.m41624a(EnergyWeapon.class, "709185bf921b907d8d3b97d70af1aa6c", i14);
        bMl = a6;
        fmVarArr[i14] = a6;
        int i15 = i14 + 1;
        C2491fm a7 = C4105zY.m41624a(EnergyWeapon.class, "ec29011b99582af6c644b12b7bd9dc8e", i15);
        bMm = a7;
        fmVarArr[i15] = a7;
        int i16 = i15 + 1;
        C2491fm a8 = C4105zY.m41624a(EnergyWeapon.class, "6f6b86dad21497490cf29f11c13b6947", i16);
        bMn = a8;
        fmVarArr[i16] = a8;
        int i17 = i16 + 1;
        C2491fm a9 = C4105zY.m41624a(EnergyWeapon.class, "cecab10fdee1db68a0fe37d75e61d9e9", i17);
        bMo = a9;
        fmVarArr[i17] = a9;
        int i18 = i17 + 1;
        C2491fm a10 = C4105zY.m41624a(EnergyWeapon.class, "42d6555d550113976834683097bd87b8", i18);
        f9580uW = a10;
        fmVarArr[i18] = a10;
        int i19 = i18 + 1;
        C2491fm a11 = C4105zY.m41624a(EnergyWeapon.class, "152ccf349617499c83aea49f51547d02", i19);
        bMp = a11;
        fmVarArr[i19] = a11;
        int i20 = i19 + 1;
        C2491fm a12 = C4105zY.m41624a(EnergyWeapon.class, "a31fdb6f46fcf17f97bf9d206dc29060", i20);
        bMq = a12;
        fmVarArr[i20] = a12;
        int i21 = i20 + 1;
        C2491fm a13 = C4105zY.m41624a(EnergyWeapon.class, "e580b0d7b0d48a72073dda09598ba61a", i21);
        bMr = a13;
        fmVarArr[i21] = a13;
        int i22 = i21 + 1;
        C2491fm a14 = C4105zY.m41624a(EnergyWeapon.class, "b8a0e56800fcaa8a49ca8be68cccc007", i22);
        f9577Lm = a14;
        fmVarArr[i22] = a14;
        int i23 = i22 + 1;
        C2491fm a15 = C4105zY.m41624a(EnergyWeapon.class, "719e32f8a0f5cbdf6d462b92caf40cc9", i23);
        _f_onResurrect_0020_0028_0029V = a15;
        fmVarArr[i23] = a15;
        int i24 = i23 + 1;
        C2491fm a16 = C4105zY.m41624a(EnergyWeapon.class, "bda097bb2764e36288e21fe61c5f5661", i24);
        bMs = a16;
        fmVarArr[i24] = a16;
        int i25 = i24 + 1;
        _m_methods = fmVarArr;
        C1634Xv.m11725a((Object[]) Weapon._m_methods, (Object[]) _m_methods);
        ___iScriptClass = aUO.m18566a(EnergyWeapon.class, C0291Dl.class, _m_fields, _m_methods);
    }

    /* renamed from: H */
    private void m41232H(String str) {
        throw new C6039afL();
    }

    /* renamed from: ad */
    private void m41235ad(Asset tCVar) {
        throw new C6039afL();
    }

    private int aqa() {
        return ((EnergyWeaponType) getType()).bVX();
    }

    private int aqb() {
        return ((EnergyWeaponType) getType()).bVZ();
    }

    private Asset aqc() {
        return ((EnergyWeaponType) getType()).bWb();
    }

    private float aqd() {
        return ((EnergyWeaponType) getType()).bWd();
    }

    private int aqe() {
        return bFf().mo5608dq().mo3212n(bMd);
    }

    private long aqf() {
        return bFf().mo5608dq().mo3213o(bMf);
    }

    @C0064Am(aul = "bda097bb2764e36288e21fe61c5f5661", aum = 0)
    private WeaponType aqy() {
        return aqp();
    }

    /* renamed from: dt */
    private void m41236dt(long j) {
        bFf().mo5608dq().mo3184b(bMf, j);
    }

    /* renamed from: ea */
    private void m41237ea(float f) {
        throw new C6039afL();
    }

    /* renamed from: fB */
    private void m41238fB(int i) {
        throw new C6039afL();
    }

    /* renamed from: fC */
    private void m41239fC(int i) {
        throw new C6039afL();
    }

    /* renamed from: fD */
    private void m41240fD(int i) {
        bFf().mo5608dq().mo3183b(bMd, i);
    }

    @C4034yP
    @C5566aOg
    @C0064Am(aul = "8db52d23ccba658f4e02306d6d59fab5", aum = 0)
    @C2499fr(mo18857qh = 2)
    /* renamed from: fG */
    private void m41242fG(int i) {
        throw new aWi(new aCE(this, bMj, new Object[]{new Integer(i)}));
    }

    /* renamed from: ij */
    private String m41244ij() {
        return ((EnergyWeaponType) getType()).mo13436iu();
    }

    @C0064Am(aul = "b8a0e56800fcaa8a49ca8be68cccc007", aum = 0)
    @C5566aOg
    @C2499fr
    /* renamed from: qU */
    private void m41246qU() {
        throw new aWi(new aCE(this, f9577Lm, new Object[0]));
    }

    /* renamed from: S */
    public void mo10S() {
        super.mo10S();
    }

    /* renamed from: T */
    public C6494any mo11T() {
        return ___iScriptClass;
    }

    /* renamed from: U */
    public C2491fm[] mo12U() {
        return _m_methods;
    }

    /* renamed from: W */
    public Object mo13W() {
        return new C0291Dl(this);
    }

    /* renamed from: a */
    public Object mo14a(C0495Gr gr) {
        Object[] args = gr.getArgs();
        switch (gr.mo2417hq() - Weapon._m_methodCount) {
            case 0:
                return new Integer(aqg());
            case 1:
                return new Integer(aqi());
            case 2:
                m41241fE(((Integer) args[0]).intValue());
                return null;
            case 3:
                m41242fG(((Integer) args[0]).intValue());
                return null;
            case 4:
                return new Integer(aqk());
            case 5:
                m41243fI(((Integer) args[0]).intValue());
                return null;
            case 6:
                return new Float(aqm());
            case 7:
                return aqo();
            case 8:
                return aqq();
            case 9:
                return m41245ik();
            case 10:
                return aqs();
            case 11:
                return aqu();
            case 12:
                return aqw();
            case 13:
                m41246qU();
                return null;
            case 14:
                m41234aG();
                return null;
            case 15:
                return aqy();
            default:
                return super.mo14a(gr);
        }
    }

    /* renamed from: a */
    public void mo15a(Collection collection, C0495Gr gr) {
        super.mo15a(collection, gr);
    }

    /* renamed from: aH */
    public void mo70aH() {
        switch (bFf().mo6893i(_f_onResurrect_0020_0028_0029V)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, _f_onResurrect_0020_0028_0029V, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, _f_onResurrect_0020_0028_0029V, new Object[0]));
                break;
        }
        m41234aG();
    }

    public int aqh() {
        switch (bFf().mo6893i(bMg)) {
            case 0:
                return 0;
            case 2:
                return ((Integer) bFf().mo5606d(new aCE(this, bMg, new Object[0]))).intValue();
            case 3:
                bFf().mo5606d(new aCE(this, bMg, new Object[0]));
                break;
        }
        return aqg();
    }

    public int aqj() {
        switch (bFf().mo6893i(bMh)) {
            case 0:
                return 0;
            case 2:
                return ((Integer) bFf().mo5606d(new aCE(this, bMh, new Object[0]))).intValue();
            case 3:
                bFf().mo5606d(new aCE(this, bMh, new Object[0]));
                break;
        }
        return aqi();
    }

    public int aql() {
        switch (bFf().mo6893i(bMk)) {
            case 0:
                return 0;
            case 2:
                return ((Integer) bFf().mo5606d(new aCE(this, bMk, new Object[0]))).intValue();
            case 3:
                bFf().mo5606d(new aCE(this, bMk, new Object[0]));
                break;
        }
        return aqk();
    }

    /* access modifiers changed from: protected */
    public float aqn() {
        switch (bFf().mo6893i(bMm)) {
            case 0:
                return 0.0f;
            case 2:
                return ((Float) bFf().mo5606d(new aCE(this, bMm, new Object[0]))).floatValue();
            case 3:
                bFf().mo5606d(new aCE(this, bMm, new Object[0]));
                break;
        }
        return aqm();
    }

    public EnergyWeaponType aqp() {
        switch (bFf().mo6893i(bMn)) {
            case 0:
                return null;
            case 2:
                return (EnergyWeaponType) bFf().mo5606d(new aCE(this, bMn, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, bMn, new Object[0]));
                break;
        }
        return aqo();
    }

    public String aqr() {
        switch (bFf().mo6893i(bMo)) {
            case 0:
                return null;
            case 2:
                return (String) bFf().mo5606d(new aCE(this, bMo, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, bMo, new Object[0]));
                break;
        }
        return aqq();
    }

    public String aqt() {
        switch (bFf().mo6893i(bMp)) {
            case 0:
                return null;
            case 2:
                return (String) bFf().mo5606d(new aCE(this, bMp, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, bMp, new Object[0]));
                break;
        }
        return aqs();
    }

    public String aqv() {
        switch (bFf().mo6893i(bMq)) {
            case 0:
                return null;
            case 2:
                return (String) bFf().mo5606d(new aCE(this, bMq, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, bMq, new Object[0]));
                break;
        }
        return aqu();
    }

    public String aqx() {
        switch (bFf().mo6893i(bMr)) {
            case 0:
                return null;
            case 2:
                return (String) bFf().mo5606d(new aCE(this, bMr, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, bMr, new Object[0]));
                break;
        }
        return aqw();
    }

    public /* bridge */ /* synthetic */ WeaponType aqz() {
        switch (bFf().mo6893i(bMs)) {
            case 0:
                return null;
            case 2:
                return (WeaponType) bFf().mo5606d(new aCE(this, bMs, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, bMs, new Object[0]));
                break;
        }
        return aqy();
    }

    /* renamed from: c */
    public C5663aRz[] mo25c() {
        return _m_fields;
    }

    /* renamed from: fF */
    public void mo5370fF(int i) {
        switch (bFf().mo6893i(bMi)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, bMi, new Object[]{new Integer(i)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, bMi, new Object[]{new Integer(i)}));
                break;
        }
        m41241fE(i);
    }

    @C4034yP
    @C5566aOg
    @C2499fr(mo18857qh = 2)
    /* renamed from: fH */
    public void mo23116fH(int i) {
        switch (bFf().mo6893i(bMj)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, bMj, new Object[]{new Integer(i)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, bMj, new Object[]{new Integer(i)}));
                break;
        }
        m41242fG(i);
    }

    /* renamed from: fJ */
    public void mo23117fJ(int i) {
        switch (bFf().mo6893i(bMl)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, bMl, new Object[]{new Integer(i)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, bMl, new Object[]{new Integer(i)}));
                break;
        }
        m41243fI(i);
    }

    /* renamed from: il */
    public C5260aCm mo5372il() {
        switch (bFf().mo6893i(f9580uW)) {
            case 0:
                return null;
            case 2:
                return (C5260aCm) bFf().mo5606d(new aCE(this, f9580uW, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f9580uW, new Object[0]));
                break;
        }
        return m41245ik();
    }

    @C5566aOg
    @C2499fr
    /* renamed from: qV */
    public void mo1959qV() {
        switch (bFf().mo6893i(f9577Lm)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f9577Lm, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f9577Lm, new Object[0]));
                break;
        }
        m41246qU();
    }

    /* renamed from: a */
    public void mo23114a(EnergyWeaponType agc) {
        super.mo12949x(agc);
        m41240fD(aqa());
    }

    @C0064Am(aul = "61034fd9f21b0805fc8af13a40338837", aum = 0)
    private int aqg() {
        return aql();
    }

    @C0064Am(aul = "8906c04274fa97a364eb9456a689a9cb", aum = 0)
    private int aqi() {
        return aqp().bVZ();
    }

    @C0064Am(aul = "e45ffa462f4f59ecaaa02dcaeccafde6", aum = 0)
    /* renamed from: fE */
    private void m41241fE(int i) {
        if (bGY()) {
            mo23116fH(i);
        }
        mo23117fJ(i);
    }

    @C0064Am(aul = "5c005f07001162af0e52a8931a4ec051", aum = 0)
    private int aqk() {
        int bVX = aqp().bVX();
        int aqe = aqe();
        if (aqe >= bVX) {
            return aqe;
        }
        long cVr = cVr();
        long aqf = aqf();
        if (aqf > 0 && aqf < cVr) {
            return Math.min(bVX, aqe + Math.round((((float) (cVr - aqf)) / 1000.0f) / aqn()));
        } else if (aqf >= 0) {
            return aqe;
        } else {
            m41236dt(cVr);
            return aqe;
        }
    }

    @C0064Am(aul = "709185bf921b907d8d3b97d70af1aa6c", aum = 0)
    /* renamed from: fI */
    private void m41243fI(int i) {
        m41236dt(cVr());
        m41240fD(i);
    }

    @C0064Am(aul = "ec29011b99582af6c644b12b7bd9dc8e", aum = 0)
    private float aqm() {
        return 1.0f / aqd();
    }

    @C0064Am(aul = "6f6b86dad21497490cf29f11c13b6947", aum = 0)
    private EnergyWeaponType aqo() {
        return (EnergyWeaponType) super.aqz();
    }

    @C0064Am(aul = "cecab10fdee1db68a0fe37d75e61d9e9", aum = 0)
    private String aqq() {
        return m41244ij();
    }

    @C0064Am(aul = "42d6555d550113976834683097bd87b8", aum = 0)
    /* renamed from: ik */
    private C5260aCm m41245ik() {
        C5260aCm acm = new C5260aCm();
        for (Map.Entry key : aqp().bpA().entrySet()) {
            DamageType fr = (DamageType) key.getKey();
            acm.mo8227g(fr, ((Float) cTZ().get(fr)).floatValue());
        }
        return acm;
    }

    @C0064Am(aul = "152ccf349617499c83aea49f51547d02", aum = 0)
    private String aqs() {
        return null;
    }

    @C0064Am(aul = "a31fdb6f46fcf17f97bf9d206dc29060", aum = 0)
    private String aqu() {
        return aqc().getHandle();
    }

    @C0064Am(aul = "e580b0d7b0d48a72073dda09598ba61a", aum = 0)
    private String aqw() {
        return aqc().getFile();
    }

    @C0064Am(aul = "719e32f8a0f5cbdf6d462b92caf40cc9", aum = 0)
    /* renamed from: aG */
    private void m41234aG() {
        if (aqf() == 0) {
            m41236dt(cVr());
        }
        super.mo70aH();
    }
}
