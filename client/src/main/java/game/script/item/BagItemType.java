package game.script.item;

import game.network.message.externalizable.aCE;
import logic.baa.*;
import logic.data.mbean.C0761Kr;
import logic.res.code.C5640aRc;
import logic.res.code.C5663aRz;
import logic.res.code.C6494any;
import logic.res.html.C2491fm;
import p001a.*;

import java.util.Collection;

@C0566Hp
@C5511aMd
@C6485anp
/* renamed from: a.aTo  reason: case insensitive filesystem */
/* compiled from: a */
public abstract class BagItemType extends ItemType implements C1616Xf {
    public static final int _m_fieldCount = 0;
    public static final C5663aRz[] _m_fields = null;
    public static final int _m_methodCount = 0;
    public static final C2491fm[] _m_methods = null;
    public static final C5663aRz ckk = null;
    public static final C2491fm ckl = null;
    public static final C2491fm iOy = null;
    public static final long serialVersionUID = 0;
    public static C6494any ___iScriptClass;
    @C0064Am(aul = "45c99ee88c9881b36b8859e6275f6605", aum = 0)
    private static float ckj;

    static {
        m18393V();
    }

    public BagItemType() {
        super((C5540aNg) null);
        super.mo10S();
    }

    public BagItemType(C5540aNg ang) {
        super(ang);
    }

    /* renamed from: V */
    static void m18393V() {
        serialVersionUID = (long) 1;
        _m_fieldCount = ItemType._m_fieldCount + 1;
        _m_methodCount = ItemType._m_methodCount + 2;
        int i = ItemType._m_fieldCount;
        C5663aRz[] arzArr = new C5663aRz[(i + 1)];
        C5663aRz b = C5640aRc.m17844b(BagItemType.class, "45c99ee88c9881b36b8859e6275f6605", i);
        ckk = b;
        arzArr[i] = b;
        int i2 = i + 1;
        _m_fields = arzArr;
        C1634Xv.m11725a((Object[]) ItemType._m_fields, (Object[]) _m_fields);
        int i3 = ItemType._m_methodCount;
        C2491fm[] fmVarArr = new C2491fm[(i3 + 2)];
        C2491fm a = C4105zY.m41624a(BagItemType.class, "6f8c146d28c603ba46fea87c27fdbe7d", i3);
        ckl = a;
        fmVarArr[i3] = a;
        int i4 = i3 + 1;
        C2491fm a2 = C4105zY.m41624a(BagItemType.class, "066647f83e04d97e32226bde9d196ba4", i4);
        iOy = a2;
        fmVarArr[i4] = a2;
        int i5 = i4 + 1;
        _m_methods = fmVarArr;
        C1634Xv.m11725a((Object[]) ItemType._m_methods, (Object[]) _m_methods);
        ___iScriptClass = aUO.m18566a(BagItemType.class, C0761Kr.class, _m_fields, _m_methods);
    }

    private float ayb() {
        return bFf().mo5608dq().mo3211m(ckk);
    }

    /* renamed from: ew */
    private void m18394ew(float f) {
        bFf().mo5608dq().mo3150a(ckk, f);
    }

    /* renamed from: T */
    public C6494any mo11T() {
        return ___iScriptClass;
    }

    /* renamed from: U */
    public C2491fm[] mo12U() {
        return _m_methods;
    }

    /* renamed from: W */
    public Object mo13W() {
        return new C0761Kr(this);
    }

    /* renamed from: a */
    public Object mo14a(C0495Gr gr) {
        Object[] args = gr.getArgs();
        switch (gr.mo2417hq() - ItemType._m_methodCount) {
            case 0:
                return new Float(ayc());
            case 1:
                m18395oD(((Float) args[0]).floatValue());
                return null;
            default:
                return super.mo14a(gr);
        }
    }

    /* renamed from: a */
    public void mo15a(Collection collection, C0495Gr gr) {
        super.mo15a(collection, gr);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Capacity")
    public float ayd() {
        switch (bFf().mo6893i(ckl)) {
            case 0:
                return 0.0f;
            case 2:
                return ((Float) bFf().mo5606d(new aCE(this, ckl, new Object[0]))).floatValue();
            case 3:
                bFf().mo5606d(new aCE(this, ckl, new Object[0]));
                break;
        }
        return ayc();
    }

    /* renamed from: c */
    public C5663aRz[] mo25c() {
        return _m_fields;
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Capacity")
    /* renamed from: oE */
    public void mo11529oE(float f) {
        switch (bFf().mo6893i(iOy)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, iOy, new Object[]{new Float(f)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, iOy, new Object[]{new Float(f)}));
                break;
        }
        m18395oD(f);
    }

    /* renamed from: S */
    public void mo10S() {
        super.mo10S();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Capacity")
    @C0064Am(aul = "6f8c146d28c603ba46fea87c27fdbe7d", aum = 0)
    private float ayc() {
        return ayb();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Capacity")
    @C0064Am(aul = "066647f83e04d97e32226bde9d196ba4", aum = 0)
    /* renamed from: oD */
    private void m18395oD(float f) {
        m18394ew(f);
    }
}
