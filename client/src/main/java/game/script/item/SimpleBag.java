package game.script.item;

import game.network.message.externalizable.aCE;
import game.script.nls.NLSItem;
import game.script.nls.NLSManager;
import logic.baa.C1616Xf;
import logic.baa.C1634Xv;
import logic.baa.C5511aMd;
import logic.baa.C6485anp;
import logic.data.mbean.C5277aDd;
import logic.data.mbean.C5586aPa;
import logic.res.code.C5640aRc;
import logic.res.code.C5663aRz;
import logic.res.code.C6494any;
import logic.res.html.C2491fm;
import p001a.*;

import java.util.Collection;

@C6485anp
@C2712iu
@C5511aMd
/* renamed from: a.gU */
/* compiled from: a */
public class SimpleBag extends BagItem implements C1616Xf {
    public static final C2491fm _f_dispose_0020_0028_0029V = null;
    public static final int _m_fieldCount = 0;
    public static final C5663aRz[] _m_fields = null;
    public static final int _m_methodCount = 0;
    public static final C2491fm[] _m_methods = null;
    public static final C5663aRz bHE = null;
    public static final C2491fm bHF = null;
    public static final C2491fm bHG = null;
    public static final long serialVersionUID = 0;
    public static C6494any ___iScriptClass;
    @C0064Am(aul = "3429fa69366c75734fe86a76faabc767", aum = 0)
    private static SimpleBagLocation bHD;

    static {
        m31950V();
    }

    public SimpleBag() {
        super((C5540aNg) null);
        super.mo10S();
    }

    public SimpleBag(C5540aNg ang) {
        super(ang);
    }

    public SimpleBag(ItemType jCVar) {
        super((C5540aNg) null);
        super.mo306n(jCVar);
    }

    /* renamed from: V */
    static void m31950V() {
        serialVersionUID = (long) 1;
        _m_fieldCount = BagItem._m_fieldCount + 1;
        _m_methodCount = BagItem._m_methodCount + 3;
        int i = BagItem._m_fieldCount;
        C5663aRz[] arzArr = new C5663aRz[(i + 1)];
        C5663aRz b = C5640aRc.m17844b(SimpleBag.class, "3429fa69366c75734fe86a76faabc767", i);
        bHE = b;
        arzArr[i] = b;
        int i2 = i + 1;
        _m_fields = arzArr;
        C1634Xv.m11725a((Object[]) BagItem._m_fields, (Object[]) _m_fields);
        int i3 = BagItem._m_methodCount;
        C2491fm[] fmVarArr = new C2491fm[(i3 + 3)];
        C2491fm a = C4105zY.m41624a(SimpleBag.class, "b9d60859b17ef378c860603bf82f11ed", i3);
        bHF = a;
        fmVarArr[i3] = a;
        int i4 = i3 + 1;
        C2491fm a2 = C4105zY.m41624a(SimpleBag.class, "78ddd3151be67cd37897d44eb4a86385", i4);
        bHG = a2;
        fmVarArr[i4] = a2;
        int i5 = i4 + 1;
        C2491fm a3 = C4105zY.m41624a(SimpleBag.class, "6dfb91ea9d874477e333f3a84053035b", i5);
        _f_dispose_0020_0028_0029V = a3;
        fmVarArr[i5] = a3;
        int i6 = i5 + 1;
        _m_methods = fmVarArr;
        C1634Xv.m11725a((Object[]) BagItem._m_methods, (Object[]) _m_methods);
        ___iScriptClass = aUO.m18566a(SimpleBag.class, C5277aDd.class, _m_fields, _m_methods);
    }

    /* renamed from: a */
    private void m31951a(SimpleBagLocation aVar) {
        bFf().mo5608dq().mo3197f(bHE, aVar);
    }

    private SimpleBagLocation aoE() {
        return (SimpleBagLocation) bFf().mo5608dq().mo3214p(bHE);
    }

    /* renamed from: S */
    public void mo10S() {
        super.mo10S();
    }

    /* renamed from: T */
    public C6494any mo11T() {
        return ___iScriptClass;
    }

    /* renamed from: U */
    public C2491fm[] mo12U() {
        return _m_methods;
    }

    /* renamed from: W */
    public Object mo13W() {
        return new C5277aDd(this);
    }

    /* renamed from: a */
    public Object mo14a(C0495Gr gr) {
        gr.getArgs();
        switch (gr.mo2417hq() - BagItem._m_methodCount) {
            case 0:
                return aoF();
            case 1:
                return new Float(aoH());
            case 2:
                m31952fg();
                return null;
            default:
                return super.mo14a(gr);
        }
    }

    /* renamed from: a */
    public void mo15a(Collection collection, C0495Gr gr) {
        super.mo15a(collection, gr);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Contents")
    public ItemLocation aoG() {
        switch (bFf().mo6893i(bHF)) {
            case 0:
                return null;
            case 2:
                return (ItemLocation) bFf().mo5606d(new aCE(this, bHF, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, bHF, new Object[0]));
                break;
        }
        return aoF();
    }

    /* renamed from: c */
    public C5663aRz[] mo25c() {
        return _m_fields;
    }

    public void dispose() {
        switch (bFf().mo6893i(_f_dispose_0020_0028_0029V)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, _f_dispose_0020_0028_0029V, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, _f_dispose_0020_0028_0029V, new Object[0]));
                break;
        }
        m31952fg();
    }

    public float getVolume() {
        switch (bFf().mo6893i(bHG)) {
            case 0:
                return 0.0f;
            case 2:
                return ((Float) bFf().mo5606d(new aCE(this, bHG, new Object[0]))).floatValue();
            case 3:
                bFf().mo5606d(new aCE(this, bHG, new Object[0]));
                break;
        }
        return aoH();
    }

    /* renamed from: n */
    public void mo306n(ItemType jCVar) {
        super.mo306n(jCVar);
        SimpleBagLocation aVar = (SimpleBagLocation) bFf().mo6865M(SimpleBagLocation.class);
        aVar.mo18990b(this);
        m31951a(aVar);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Contents")
    @C0064Am(aul = "b9d60859b17ef378c860603bf82f11ed", aum = 0)
    private ItemLocation aoF() {
        return aoE();
    }

    @C0064Am(aul = "78ddd3151be67cd37897d44eb4a86385", aum = 0)
    private float aoH() {
        return aoG().aRB();
    }

    @C0064Am(aul = "6dfb91ea9d874477e333f3a84053035b", aum = 0)
    /* renamed from: fg */
    private void m31952fg() {
        aoE().dispose();
        super.dispose();
    }

    @C6485anp
    @C5511aMd
    /* renamed from: a.gU$a */
    public static class SimpleBagLocation extends ItemLocation implements C1616Xf {

        /* renamed from: RA */
        public static final C2491fm f7582RA = null;

        /* renamed from: RD */
        public static final C2491fm f7583RD = null;
        /* renamed from: Rw */
        public static final C5663aRz f7585Rw = null;
        /* renamed from: Rx */
        public static final C2491fm f7586Rx = null;
        /* renamed from: Ry */
        public static final C2491fm f7587Ry = null;
        /* renamed from: Rz */
        public static final C2491fm f7588Rz = null;
        public static final C2491fm _f_dispose_0020_0028_0029V = null;
        public static final int _m_fieldCount = 0;
        public static final C5663aRz[] _m_fields = null;
        public static final int _m_methodCount = 0;
        public static final C2491fm[] _m_methods = null;
        /* renamed from: ku */
        public static final C2491fm f7589ku = null;
        public static final long serialVersionUID = 0;
        public static C6494any ___iScriptClass;
        @C0064Am(aul = "d8378d01978748cf74dddcaf4d7d7c64", aum = 0)

        /* renamed from: Rv */
        private static BagItem f7584Rv;

        static {
            m31961V();
        }

        public SimpleBagLocation() {
            super((C5540aNg) null);
            super.mo10S();
        }

        public SimpleBagLocation(BagItem at) {
            super((C5540aNg) null);
            super._m_script_init(at);
        }

        public SimpleBagLocation(C5540aNg ang) {
            super(ang);
        }

        /* renamed from: V */
        static void m31961V() {
            serialVersionUID = (long) 1;
            _m_fieldCount = ItemLocation._m_fieldCount + 1;
            _m_methodCount = ItemLocation._m_methodCount + 7;
            int i = ItemLocation._m_fieldCount;
            C5663aRz[] arzArr = new C5663aRz[(i + 1)];
            C5663aRz b = C5640aRc.m17844b(SimpleBagLocation.class, "d8378d01978748cf74dddcaf4d7d7c64", i);
            f7585Rw = b;
            arzArr[i] = b;
            int i2 = i + 1;
            _m_fields = arzArr;
            C1634Xv.m11725a((Object[]) ItemLocation._m_fields, (Object[]) _m_fields);
            int i3 = ItemLocation._m_methodCount;
            C2491fm[] fmVarArr = new C2491fm[(i3 + 7)];
            C2491fm a = C4105zY.m41624a(SimpleBagLocation.class, "93fbe1f0abc5f69c1a2c9ac550129ef1", i3);
            f7586Rx = a;
            fmVarArr[i3] = a;
            int i4 = i3 + 1;
            C2491fm a2 = C4105zY.m41624a(SimpleBagLocation.class, "132f6ec4ba110478912a485b6cba7638", i4);
            f7587Ry = a2;
            fmVarArr[i4] = a2;
            int i5 = i4 + 1;
            C2491fm a3 = C4105zY.m41624a(SimpleBagLocation.class, "d879e4934d9dfabdb6cdecc39f3b8e03", i5);
            f7588Rz = a3;
            fmVarArr[i5] = a3;
            int i6 = i5 + 1;
            C2491fm a4 = C4105zY.m41624a(SimpleBagLocation.class, "361797af66398a078f0d5511d0512b8e", i6);
            _f_dispose_0020_0028_0029V = a4;
            fmVarArr[i6] = a4;
            int i7 = i6 + 1;
            C2491fm a5 = C4105zY.m41624a(SimpleBagLocation.class, "ed803fa349eae2607a847bc09d37a9d1", i7);
            f7582RA = a5;
            fmVarArr[i7] = a5;
            int i8 = i7 + 1;
            C2491fm a6 = C4105zY.m41624a(SimpleBagLocation.class, "a68b08278fbf440386589f98197f63be", i8);
            f7583RD = a6;
            fmVarArr[i8] = a6;
            int i9 = i8 + 1;
            C2491fm a7 = C4105zY.m41624a(SimpleBagLocation.class, "79b0db81d82407a188aff6399435c3f1", i9);
            f7589ku = a7;
            fmVarArr[i9] = a7;
            int i10 = i9 + 1;
            _m_methods = fmVarArr;
            C1634Xv.m11725a((Object[]) ItemLocation._m_methods, (Object[]) _m_methods);
            ___iScriptClass = aUO.m18566a(SimpleBagLocation.class, C5586aPa.class, _m_fields, _m_methods);
        }

        /* renamed from: a */
        private void m31962a(BagItem at) {
            bFf().mo5608dq().mo3197f(f7585Rw, at);
        }

        /* renamed from: wA */
        private BagItem m31968wA() {
            return (BagItem) bFf().mo5608dq().mo3214p(f7585Rw);
        }

        /* renamed from: S */
        public void mo10S() {
            super.mo10S();
        }

        /* renamed from: T */
        public C6494any mo11T() {
            return ___iScriptClass;
        }

        /* renamed from: U */
        public C2491fm[] mo12U() {
            return _m_methods;
        }

        /* renamed from: W */
        public Object mo13W() {
            return new C5586aPa(this);
        }

        /* renamed from: a */
        public Object mo14a(C0495Gr gr) {
            Object[] args = gr.getArgs();
            switch (gr.mo2417hq() - ItemLocation._m_methodCount) {
                case 0:
                    return new Boolean(m31966k((Item) args[0]));
                case 1:
                    return m31969wB();
                case 2:
                    return new Float(m31970wD());
                case 3:
                    m31964fg();
                    return null;
                case 4:
                    m31963a((Item) args[0], (ItemLocation) args[1]);
                    return null;
                case 5:
                    m31967m((Item) args[0]);
                    return null;
                case 6:
                    m31965g((Item) args[0]);
                    return null;
                default:
                    return super.mo14a(gr);
            }
        }

        /* renamed from: a */
        public void mo15a(Collection collection, C0495Gr gr) {
            super.mo15a(collection, gr);
        }

        /* access modifiers changed from: protected */
        /* renamed from: b */
        public void mo1887b(Item auq, ItemLocation aag) {
            switch (bFf().mo6893i(f7582RA)) {
                case 0:
                    return;
                case 2:
                    bFf().mo5606d(new aCE(this, f7582RA, new Object[]{auq, aag}));
                    return;
                case 3:
                    bFf().mo5606d(new aCE(this, f7582RA, new Object[]{auq, aag}));
                    break;
            }
            m31963a(auq, aag);
        }

        /* renamed from: c */
        public C5663aRz[] mo25c() {
            return _m_fields;
        }

        public void dispose() {
            switch (bFf().mo6893i(_f_dispose_0020_0028_0029V)) {
                case 0:
                    return;
                case 2:
                    bFf().mo5606d(new aCE(this, _f_dispose_0020_0028_0029V, new Object[0]));
                    return;
                case 3:
                    bFf().mo5606d(new aCE(this, _f_dispose_0020_0028_0029V, new Object[0]));
                    break;
            }
            m31964fg();
        }

        /* access modifiers changed from: protected */
        /* renamed from: h */
        public void mo1889h(Item auq) {
            switch (bFf().mo6893i(f7589ku)) {
                case 0:
                    return;
                case 2:
                    bFf().mo5606d(new aCE(this, f7589ku, new Object[]{auq}));
                    return;
                case 3:
                    bFf().mo5606d(new aCE(this, f7589ku, new Object[]{auq}));
                    break;
            }
            m31965g(auq);
        }

        /* renamed from: l */
        public boolean mo2878l(Item auq) {
            switch (bFf().mo6893i(f7586Rx)) {
                case 0:
                    return false;
                case 2:
                    return ((Boolean) bFf().mo5606d(new aCE(this, f7586Rx, new Object[]{auq}))).booleanValue();
                case 3:
                    bFf().mo5606d(new aCE(this, f7586Rx, new Object[]{auq}));
                    break;
            }
            return m31966k(auq);
        }

        /* access modifiers changed from: protected */
        /* renamed from: n */
        public void mo1890n(Item auq) {
            switch (bFf().mo6893i(f7583RD)) {
                case 0:
                    return;
                case 2:
                    bFf().mo5606d(new aCE(this, f7583RD, new Object[]{auq}));
                    return;
                case 3:
                    bFf().mo5606d(new aCE(this, f7583RD, new Object[]{auq}));
                    break;
            }
            m31967m(auq);
        }

        /* renamed from: wC */
        public BagItem mo18991wC() {
            switch (bFf().mo6893i(f7587Ry)) {
                case 0:
                    return null;
                case 2:
                    return (BagItem) bFf().mo5606d(new aCE(this, f7587Ry, new Object[0]));
                case 3:
                    bFf().mo5606d(new aCE(this, f7587Ry, new Object[0]));
                    break;
            }
            return m31969wB();
        }

        /* renamed from: wE */
        public float mo2696wE() {
            switch (bFf().mo6893i(f7588Rz)) {
                case 0:
                    return 0.0f;
                case 2:
                    return ((Float) bFf().mo5606d(new aCE(this, f7588Rz, new Object[0]))).floatValue();
                case 3:
                    bFf().mo5606d(new aCE(this, f7588Rz, new Object[0]));
                    break;
            }
            return m31970wD();
        }

        /* renamed from: b */
        public void mo18990b(BagItem at) {
            super.mo10S();
            m31962a(at);
        }

        @C0064Am(aul = "93fbe1f0abc5f69c1a2c9ac550129ef1", aum = 0)
        /* renamed from: k */
        private boolean m31966k(Item auq) {
            return m31968wA().mo305l(auq) && super.mo2878l(auq);
        }

        @C0064Am(aul = "132f6ec4ba110478912a485b6cba7638", aum = 0)
        /* renamed from: wB */
        private BagItem m31969wB() {
            return m31968wA();
        }

        @C0064Am(aul = "d879e4934d9dfabdb6cdecc39f3b8e03", aum = 0)
        /* renamed from: wD */
        private float m31970wD() {
            return ((BagItemType) m31968wA().bAP()).ayd();
        }

        @C0064Am(aul = "361797af66398a078f0d5511d0512b8e", aum = 0)
        /* renamed from: fg */
        private void m31964fg() {
            m31962a((BagItem) null);
            super.dispose();
        }

        @C0064Am(aul = "ed803fa349eae2607a847bc09d37a9d1", aum = 0)
        /* renamed from: a */
        private void m31963a(Item auq, ItemLocation aag) {
        }

        @C0064Am(aul = "a68b08278fbf440386589f98197f63be", aum = 0)
        /* renamed from: m */
        private void m31967m(Item auq) {
        }

        @C0064Am(aul = "79b0db81d82407a188aff6399435c3f1", aum = 0)
        /* renamed from: g */
        private void m31965g(Item auq) {
            if (!(auq != m31968wA())) {
                throw new C2293dh(((NLSItem) ala().aIY().mo6310c(NLSManager.C1472a.ITEM)).bto());
            }
        }
    }
}
