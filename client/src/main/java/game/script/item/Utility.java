package game.script.item;

import logic.baa.C1616Xf;
import logic.baa.C1634Xv;
import logic.baa.C5511aMd;
import logic.baa.C6485anp;
import logic.data.mbean.C6861avB;
import logic.res.code.C5663aRz;
import logic.res.code.C6494any;
import logic.res.html.C2491fm;
import p001a.C0495Gr;
import p001a.C2712iu;
import p001a.C5540aNg;
import p001a.aUO;

import java.util.Collection;

@C2712iu(version = "2.0.1")
@C5511aMd
@C6485anp
/* renamed from: a.aqm  reason: case insensitive filesystem */
/* compiled from: a */
public class Utility extends Component implements C1616Xf {
    public static final int _m_fieldCount = 0;
    public static final C5663aRz[] _m_fields = null;
    public static final int _m_methodCount = 0;
    public static final C2491fm[] _m_methods = null;
    public static final long serialVersionUID = 0;
    public static C6494any ___iScriptClass;

    static {
        m25208V();
    }

    public Utility() {
        super((C5540aNg) null);
        super.mo10S();
    }

    public Utility(C5540aNg ang) {
        super(ang);
    }

    public Utility(UtilityType acl) {
        super((C5540aNg) null);
        super._m_script_init(acl);
    }

    /* renamed from: V */
    static void m25208V() {
        serialVersionUID = (long) 1;
        _m_fieldCount = Component._m_fieldCount + 0;
        _m_methodCount = Component._m_methodCount + 0;
        _m_fields = new C5663aRz[(Component._m_fieldCount + 0)];
        C1634Xv.m11725a((Object[]) Component._m_fields, (Object[]) _m_fields);
        _m_methods = new C2491fm[(Component._m_methodCount + 0)];
        C1634Xv.m11725a((Object[]) Component._m_methods, (Object[]) _m_methods);
        ___iScriptClass = aUO.m18566a(Utility.class, C6861avB.class, _m_fields, _m_methods);
    }

    /* renamed from: S */
    public void mo10S() {
        super.mo10S();
    }

    /* renamed from: T */
    public C6494any mo11T() {
        return ___iScriptClass;
    }

    /* renamed from: U */
    public C2491fm[] mo12U() {
        return _m_methods;
    }

    /* renamed from: W */
    public Object mo13W() {
        return new C6861avB(this);
    }

    /* renamed from: a */
    public Object mo14a(C0495Gr gr) {
        return super.mo14a(gr);
    }

    /* renamed from: a */
    public void mo15a(Collection collection, C0495Gr gr) {
        super.mo15a(collection, gr);
    }

    /* renamed from: c */
    public C5663aRz[] mo25c() {
        return _m_fields;
    }

    /* renamed from: a */
    public void mo15636a(UtilityType acl) {
        super.mo7854a((ComponentType) acl);
    }
}
