package game.script.item;

import game.network.message.externalizable.aCE;
import game.network.message.serializable.C3438ra;
import game.script.crafting.CraftingRecipe;
import logic.baa.*;
import logic.data.mbean.C3329qL;
import logic.res.code.C5640aRc;
import logic.res.code.C5663aRz;
import logic.res.code.C6494any;
import logic.res.html.C2491fm;
import p001a.*;
import taikodom.infra.script.I18NString;

import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.UUID;

@C5511aMd
@C6485anp
/* renamed from: a.xV */
/* compiled from: a */
public class IngredientsCategory extends aDJ implements C0468GU, C1616Xf {
    public static final int _m_fieldCount = 0;
    public static final C5663aRz[] _m_fields = null;
    public static final int _m_methodCount = 0;
    public static final C2491fm[] _m_methods = null;
    public static final C5663aRz bJc = null;
    public static final C2491fm bJd = null;
    public static final C2491fm bJe = null;
    public static final C2491fm bJf = null;
    /* renamed from: bL */
    public static final C5663aRz f9547bL = null;
    /* renamed from: bM */
    public static final C5663aRz f9548bM = null;
    /* renamed from: bN */
    public static final C2491fm f9549bN = null;
    /* renamed from: bO */
    public static final C2491fm f9550bO = null;
    /* renamed from: bP */
    public static final C2491fm f9551bP = null;
    /* renamed from: bQ */
    public static final C2491fm f9552bQ = null;
    public static final long serialVersionUID = 0;
    /* renamed from: zQ */
    public static final C5663aRz f9554zQ = null;
    /* renamed from: zT */
    public static final C2491fm f9555zT = null;
    /* renamed from: zU */
    public static final C2491fm f9556zU = null;
    public static C6494any ___iScriptClass;
    @C0064Am(aul = "edaced45b2672c95ce1cb4c33bd2021f", aum = 1)
    private static C3438ra<CraftingRecipe> aWq;
    @C0064Am(aul = "e89841dfa66f95697d7c5b6c42ccf331", aum = 2)

    /* renamed from: bK */
    private static UUID f9546bK;
    @C0064Am(aul = "3cbd0fc601375cf774e039c634275bb4", aum = 3)
    private static String handle;
    @C0064Am(aul = "f6f4e4ef479983556c10dfb761743463", aum = 0)

    /* renamed from: zP */
    private static I18NString f9553zP;

    static {
        m41003V();
    }

    public IngredientsCategory() {
        super((C5540aNg) null);
        super.mo10S();
    }

    public IngredientsCategory(C5540aNg ang) {
        super(ang);
    }

    /* renamed from: V */
    static void m41003V() {
        serialVersionUID = (long) 1;
        _m_fieldCount = aDJ._m_fieldCount + 4;
        _m_methodCount = aDJ._m_methodCount + 9;
        int i = aDJ._m_fieldCount;
        C5663aRz[] arzArr = new C5663aRz[(i + 4)];
        C5663aRz b = C5640aRc.m17844b(IngredientsCategory.class, "f6f4e4ef479983556c10dfb761743463", i);
        f9554zQ = b;
        arzArr[i] = b;
        int i2 = i + 1;
        C5663aRz b2 = C5640aRc.m17844b(IngredientsCategory.class, "edaced45b2672c95ce1cb4c33bd2021f", i2);
        bJc = b2;
        arzArr[i2] = b2;
        int i3 = i2 + 1;
        C5663aRz b3 = C5640aRc.m17844b(IngredientsCategory.class, "e89841dfa66f95697d7c5b6c42ccf331", i3);
        f9547bL = b3;
        arzArr[i3] = b3;
        int i4 = i3 + 1;
        C5663aRz b4 = C5640aRc.m17844b(IngredientsCategory.class, "3cbd0fc601375cf774e039c634275bb4", i4);
        f9548bM = b4;
        arzArr[i4] = b4;
        int i5 = i4 + 1;
        _m_fields = arzArr;
        C1634Xv.m11725a((Object[]) aDJ._m_fields, (Object[]) _m_fields);
        int i6 = aDJ._m_methodCount;
        C2491fm[] fmVarArr = new C2491fm[(i6 + 9)];
        C2491fm a = C4105zY.m41624a(IngredientsCategory.class, "8faee5294d92c57f1046890bf13b4de2", i6);
        f9549bN = a;
        fmVarArr[i6] = a;
        int i7 = i6 + 1;
        C2491fm a2 = C4105zY.m41624a(IngredientsCategory.class, "b8d18ce63f6f9c50ec8434075105d8a8", i7);
        f9550bO = a2;
        fmVarArr[i7] = a2;
        int i8 = i7 + 1;
        C2491fm a3 = C4105zY.m41624a(IngredientsCategory.class, "b65572b346c1a2374e864622a8ac3c9b", i8);
        f9551bP = a3;
        fmVarArr[i8] = a3;
        int i9 = i8 + 1;
        C2491fm a4 = C4105zY.m41624a(IngredientsCategory.class, "9d9ab602292364f2e307a777905820c8", i9);
        f9552bQ = a4;
        fmVarArr[i9] = a4;
        int i10 = i9 + 1;
        C2491fm a5 = C4105zY.m41624a(IngredientsCategory.class, "ee022ed7dccc5798d33e8a614e8b9add", i10);
        f9555zT = a5;
        fmVarArr[i10] = a5;
        int i11 = i10 + 1;
        C2491fm a6 = C4105zY.m41624a(IngredientsCategory.class, "8a1f6b8a69f73d9d2c2b5726c1899ef2", i11);
        f9556zU = a6;
        fmVarArr[i11] = a6;
        int i12 = i11 + 1;
        C2491fm a7 = C4105zY.m41624a(IngredientsCategory.class, "4e477b64470b19a290364c32d59190f1", i12);
        bJd = a7;
        fmVarArr[i12] = a7;
        int i13 = i12 + 1;
        C2491fm a8 = C4105zY.m41624a(IngredientsCategory.class, "04ed9764fb3ab1dc529314f11c159a52", i13);
        bJe = a8;
        fmVarArr[i13] = a8;
        int i14 = i13 + 1;
        C2491fm a9 = C4105zY.m41624a(IngredientsCategory.class, "cbe6ac0b97d874e1c21c01bfd5ff9097", i14);
        bJf = a9;
        fmVarArr[i14] = a9;
        int i15 = i14 + 1;
        _m_methods = fmVarArr;
        C1634Xv.m11725a((Object[]) aDJ._m_methods, (Object[]) _m_methods);
        ___iScriptClass = aUO.m18566a(IngredientsCategory.class, C3329qL.class, _m_fields, _m_methods);
    }

    /* renamed from: Y */
    private void m41004Y(C3438ra raVar) {
        bFf().mo5608dq().mo3197f(bJc, raVar);
    }

    @C3248pc(aYR = C3248pc.C3250b.ADDER, displayName = "Crafting Recipes")
    @C0064Am(aul = "4e477b64470b19a290364c32d59190f1", aum = 0)
    @C5566aOg
    /* renamed from: a */
    private void m41005a(CraftingRecipe b) {
        throw new aWi(new aCE(this, bJd, new Object[]{b}));
    }

    /* renamed from: a */
    private void m41006a(String str) {
        bFf().mo5608dq().mo3197f(f9548bM, str);
    }

    /* renamed from: a */
    private void m41007a(UUID uuid) {
        bFf().mo5608dq().mo3197f(f9547bL, uuid);
    }

    /* renamed from: an */
    private UUID m41008an() {
        return (UUID) bFf().mo5608dq().mo3214p(f9547bL);
    }

    /* renamed from: ao */
    private String m41009ao() {
        return (String) bFf().mo5608dq().mo3214p(f9548bM);
    }

    private C3438ra apb() {
        return (C3438ra) bFf().mo5608dq().mo3214p(bJc);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Handle")
    @C0064Am(aul = "9d9ab602292364f2e307a777905820c8", aum = 0)
    @C5566aOg
    /* renamed from: b */
    private void m41012b(String str) {
        throw new aWi(new aCE(this, f9552bQ, new Object[]{str}));
    }

    @C3248pc(aYR = C3248pc.C3250b.REMOVER, displayName = "Crafting Recipes")
    @C0064Am(aul = "04ed9764fb3ab1dc529314f11c159a52", aum = 0)
    @C5566aOg
    /* renamed from: c */
    private void m41014c(CraftingRecipe b) {
        throw new aWi(new aCE(this, bJe, new Object[]{b}));
    }

    /* renamed from: c */
    private void m41015c(UUID uuid) {
        switch (bFf().mo6893i(f9550bO)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f9550bO, new Object[]{uuid}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f9550bO, new Object[]{uuid}));
                break;
        }
        m41013b(uuid);
    }

    /* renamed from: d */
    private void m41016d(I18NString i18NString) {
        bFf().mo5608dq().mo3197f(f9554zQ, i18NString);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Ingredients Category Name")
    @C0064Am(aul = "8a1f6b8a69f73d9d2c2b5726c1899ef2", aum = 0)
    @C5566aOg
    /* renamed from: e */
    private void m41017e(I18NString i18NString) {
        throw new aWi(new aCE(this, f9556zU, new Object[]{i18NString}));
    }

    /* renamed from: kb */
    private I18NString m41018kb() {
        return (I18NString) bFf().mo5608dq().mo3214p(f9554zQ);
    }

    /* renamed from: T */
    public C6494any mo11T() {
        return ___iScriptClass;
    }

    /* renamed from: U */
    public C2491fm[] mo12U() {
        return _m_methods;
    }

    /* renamed from: W */
    public Object mo13W() {
        return new C3329qL(this);
    }

    /* renamed from: a */
    public Object mo14a(C0495Gr gr) {
        Object[] args = gr.getArgs();
        switch (gr.mo2417hq() - aDJ._m_methodCount) {
            case 0:
                return m41010ap();
            case 1:
                m41013b((UUID) args[0]);
                return null;
            case 2:
                return m41011ar();
            case 3:
                m41012b((String) args[0]);
                return null;
            case 4:
                return m41019kd();
            case 5:
                m41017e((I18NString) args[0]);
                return null;
            case 6:
                m41005a((CraftingRecipe) args[0]);
                return null;
            case 7:
                m41014c((CraftingRecipe) args[0]);
                return null;
            case 8:
                return apc();
            default:
                return super.mo14a(gr);
        }
    }

    /* renamed from: a */
    public void mo15a(Collection collection, C0495Gr gr) {
        super.mo15a(collection, gr);
    }

    @C3248pc(aYR = C3248pc.C3250b.ITERATOR, displayName = "Crafting Recipes")
    public List<CraftingRecipe> apd() {
        switch (bFf().mo6893i(bJf)) {
            case 0:
                return null;
            case 2:
                return (List) bFf().mo5606d(new aCE(this, bJf, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, bJf, new Object[0]));
                break;
        }
        return apc();
    }

    /* renamed from: aq */
    public UUID mo18aq() {
        switch (bFf().mo6893i(f9549bN)) {
            case 0:
                return null;
            case 2:
                return (UUID) bFf().mo5606d(new aCE(this, f9549bN, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f9549bN, new Object[0]));
                break;
        }
        return m41010ap();
    }

    @C3248pc(aYR = C3248pc.C3250b.ADDER, displayName = "Crafting Recipes")
    @C5566aOg
    /* renamed from: b */
    public void mo22927b(CraftingRecipe b) {
        switch (bFf().mo6893i(bJd)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, bJd, new Object[]{b}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, bJd, new Object[]{b}));
                break;
        }
        m41005a(b);
    }

    /* renamed from: c */
    public C5663aRz[] mo25c() {
        return _m_fields;
    }

    @C3248pc(aYR = C3248pc.C3250b.REMOVER, displayName = "Crafting Recipes")
    @C5566aOg
    /* renamed from: d */
    public void mo22928d(CraftingRecipe b) {
        switch (bFf().mo6893i(bJe)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, bJe, new Object[]{b}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, bJe, new Object[]{b}));
                break;
        }
        m41014c(b);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Ingredients Category Name")
    @C5566aOg
    /* renamed from: f */
    public void mo22929f(I18NString i18NString) {
        switch (bFf().mo6893i(f9556zU)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f9556zU, new Object[]{i18NString}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f9556zU, new Object[]{i18NString}));
                break;
        }
        m41017e(i18NString);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Handle")
    public String getHandle() {
        switch (bFf().mo6893i(f9551bP)) {
            case 0:
                return null;
            case 2:
                return (String) bFf().mo5606d(new aCE(this, f9551bP, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f9551bP, new Object[0]));
                break;
        }
        return m41011ar();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Handle")
    @C5566aOg
    public void setHandle(String str) {
        switch (bFf().mo6893i(f9552bQ)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f9552bQ, new Object[]{str}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f9552bQ, new Object[]{str}));
                break;
        }
        m41012b(str);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Ingredients Category Name")
    /* renamed from: ke */
    public I18NString mo22930ke() {
        switch (bFf().mo6893i(f9555zT)) {
            case 0:
                return null;
            case 2:
                return (I18NString) bFf().mo5606d(new aCE(this, f9555zT, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f9555zT, new Object[0]));
                break;
        }
        return m41019kd();
    }

    @C0064Am(aul = "8faee5294d92c57f1046890bf13b4de2", aum = 0)
    /* renamed from: ap */
    private UUID m41010ap() {
        return m41008an();
    }

    @C0064Am(aul = "b8d18ce63f6f9c50ec8434075105d8a8", aum = 0)
    /* renamed from: b */
    private void m41013b(UUID uuid) {
        m41007a(uuid);
    }

    /* renamed from: S */
    public void mo10S() {
        super.mo10S();
        m41007a(UUID.randomUUID());
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Handle")
    @C0064Am(aul = "b65572b346c1a2374e864622a8ac3c9b", aum = 0)
    /* renamed from: ar */
    private String m41011ar() {
        return m41009ao();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Ingredients Category Name")
    @C0064Am(aul = "ee022ed7dccc5798d33e8a614e8b9add", aum = 0)
    /* renamed from: kd */
    private I18NString m41019kd() {
        return m41018kb();
    }

    @C3248pc(aYR = C3248pc.C3250b.ITERATOR, displayName = "Crafting Recipes")
    @C0064Am(aul = "cbe6ac0b97d874e1c21c01bfd5ff9097", aum = 0)
    private List<CraftingRecipe> apc() {
        return Collections.unmodifiableList(apb());
    }
}
