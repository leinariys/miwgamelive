package game.script.item;

import game.network.message.externalizable.aCE;
import game.script.item.buff.module.*;
import logic.baa.*;
import logic.data.mbean.aTT;
import logic.res.code.C5663aRz;
import logic.res.code.C6494any;
import logic.res.html.C2491fm;
import p001a.*;

import java.util.Collection;
import java.util.List;

@C5829abJ("2.0.0")
@C6793atl({"taikodom.game.script.item.buff.module.ShieldModuleType", "", "taikodom.game.script.item.buff.module.HullModuleType", "", "taikodom.game.script.item.buff.module.WeaponModuleType", "", "taikodom.game.script.item.buff.module.ShipModuleType", "", "taikodom.game.script.item.buff.module.ComposedModuleType", ""})
@C6485anp
@C5511aMd
/* renamed from: a.aFm  reason: case insensitive filesystem */
/* compiled from: a */
public abstract class BaseModuleType extends ComponentType implements C1616Xf, aOW {

    /* renamed from: Do */
    public static final C2491fm f2843Do = null;
    public static final int _m_fieldCount = 0;
    public static final C5663aRz[] _m_fields = null;
    public static final int _m_methodCount = 0;
    public static final C2491fm[] _m_methods = null;
    public static final long serialVersionUID = 0;
    public static C6494any ___iScriptClass;

    static {
        m14636V();
    }

    public BaseModuleType() {
        super((C5540aNg) null);
        super.mo10S();
    }

    public BaseModuleType(C5540aNg ang) {
        super(ang);
    }

    /* renamed from: V */
    static void m14636V() {
        serialVersionUID = (long) 1;
        _m_fieldCount = ComponentType._m_fieldCount + 0;
        _m_methodCount = ComponentType._m_methodCount + 1;
        _m_fields = new C5663aRz[(ComponentType._m_fieldCount + 0)];
        C1634Xv.m11725a((Object[]) ComponentType._m_fields, (Object[]) _m_fields);
        int i = ComponentType._m_methodCount;
        C2491fm[] fmVarArr = new C2491fm[(i + 1)];
        C2491fm a = C4105zY.m41624a(BaseModuleType.class, "add37e80cd80389e5b9e0d66c2c5de36", i);
        f2843Do = a;
        fmVarArr[i] = a;
        int i2 = i + 1;
        _m_methods = fmVarArr;
        C1634Xv.m11725a((Object[]) ComponentType._m_methods, (Object[]) _m_methods);
        ___iScriptClass = aUO.m18566a(BaseModuleType.class, aTT.class, _m_fields, _m_methods);
    }

    /* renamed from: a */
    public static void m14638a(C6859auz auz) {
        if (auz.mo3117j(2, 0, 0)) {
            if (auz.bav().equals("taikodom.game.script.item.buff.module.ComposedModuleType") && ((List) auz.get("modulesTypes")) != null) {
                System.out.println("UAR");
            }
            auz.setClass(ModuleType.class);
        }
    }

    /* renamed from: T */
    public C6494any mo11T() {
        return ___iScriptClass;
    }

    /* renamed from: U */
    public C2491fm[] mo12U() {
        return _m_methods;
    }

    /* renamed from: W */
    public Object mo13W() {
        return new aTT(this);
    }

    /* renamed from: a */
    public Object mo14a(C0495Gr gr) {
        Object[] args = gr.getArgs();
        switch (gr.mo2417hq() - ComponentType._m_methodCount) {
            case 0:
                m14637a((C0665JT) args[0]);
                return null;
            default:
                return super.mo14a(gr);
        }
    }

    /* renamed from: a */
    public void mo15a(Collection collection, C0495Gr gr) {
        super.mo15a(collection, gr);
    }

    /* renamed from: b */
    public void mo24b(C0665JT jt) {
        switch (bFf().mo6893i(f2843Do)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f2843Do, new Object[]{jt}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f2843Do, new Object[]{jt}));
                break;
        }
        m14637a(jt);
    }

    /* renamed from: c */
    public C5663aRz[] mo25c() {
        return _m_fields;
    }

    /* renamed from: S */
    public void mo10S() {
        super.mo10S();
    }

    @C0064Am(aul = "add37e80cd80389e5b9e0d66c2c5de36", aum = 0)
    /* renamed from: a */
    private void m14637a(C0665JT jt) {
        if (jt.mo3117j(2, 0, 0)) {
            try {
                ModuleType kq = (ModuleType) jt.baw();
                if (jt.bav().equals("taikodom.game.script.item.buff.module.ShieldModuleType")) {
                    System.out.println("** CONVERTING SHIELD MODULE ***");
                    ShieldBuffType art = (ShieldBuffType) bFf().mo6865M(ShieldBuffType.class);
                    art.mo10S();
                    art.mo11134J((C3892wO) jt.get("damageReduction"));
                    art.mo11133H((C3892wO) jt.get("healthPoints"));
                    art.mo11136Z((C3892wO) jt.get("recoveryTime"));
                    art.mo11135X((C3892wO) jt.get("regeneration"));
                    art.mo10827c(Module.C4010b.SELF);
                    kq.bdH().add(art);
                    System.out.println("** Converted " + kq.mo19891ke().get() + "  ***");
                } else if (jt.bav().equals("taikodom.game.script.item.buff.module.HullModuleType")) {
                    System.out.println("** CONVERTING HULL MODULE ***");
                    HullBuffType aes = (HullBuffType) bFf().mo6865M(HullBuffType.class);
                    aes.mo10S();
                    aes.mo8605J((C3892wO) jt.get("damageReduction"));
                    aes.mo8604H((C3892wO) jt.get("healthPoints"));
                    aes.mo10827c(Module.C4010b.SELF);
                    kq.bdH().add(aes);
                } else if (jt.bav().equals("taikodom.game.script.item.buff.module.ShipModuleType")) {
                    System.out.println("** CONVERTING SHIP MODULE ***");
                    ShipBuffType hOVar = (ShipBuffType) bFf().mo6865M(ShipBuffType.class);
                    hOVar.mo10S();
                    hOVar.mo19241j((C3892wO) jt.get("linearVelocity"));
                    hOVar.mo19242l((C3892wO) jt.get("linearAcceleration"));
                    hOVar.mo19243n((C3892wO) jt.get("angularVelocity"));
                    hOVar.mo19244p((C3892wO) jt.get("angularAcceleration"));
                    hOVar.mo19245r((C3892wO) jt.get("radarRange"));
                    hOVar.mo10827c(Module.C4010b.SELF);
                    kq.bdH().add(hOVar);
                } else if (jt.bav().equals("taikodom.game.script.item.buff.module.WeaponModuleType")) {
                    System.out.println("** CONVERTING WEAPON MODULE ***");
                    WeaponBuffType ajs = (WeaponBuffType) bFf().mo6865M(WeaponBuffType.class);
                    ajs.mo10S();
                    ajs.mo14084L((C3892wO) jt.get("fireRate"));
                    ajs.mo14085N((C3892wO) jt.get("speed"));
                    ajs.mo14086P((C3892wO) jt.get("range"));
                    ajs.mo14087R((C3892wO) jt.get("shieldDamage"));
                    ajs.mo14088T((C3892wO) jt.get("hullDamage"));
                    ajs.mo14095cB(((Boolean) jt.get("doubleShot")).booleanValue());
                    ajs.mo10827c(Module.C4010b.SELF);
                    kq.bdH().add(ajs);
                } else if (jt.bav().equals("taikodom.game.script.item.buff.module.CruiseSpeedModuleType")) {
                    System.out.println("** CONVERTING CRUISESPEED MODULE ***");
                    CruiseSpeedBuffType alt = (CruiseSpeedBuffType) bFf().mo6865M(CruiseSpeedBuffType.class);
                    alt.mo10S();
                    alt.mo14677j((C3892wO) jt.get("linearVelocity"));
                    alt.mo14678l((C3892wO) jt.get("linearAcceleration"));
                    alt.mo14679n((C3892wO) jt.get("angularVelocity"));
                    alt.mo14680p((C3892wO) jt.get("angularAcceleration"));
                    alt.mo10827c(Module.C4010b.SELF);
                    kq.bdH().add(alt);
                } else if (jt.bav().equals("taikodom.game.script.item.buff.module.ComposedModuleType")) {
                    System.out.println("** CONVERTING COMPOSED MODULE ***");
                    List<C0665JT> list = (List) jt.mo3113eE("modulesTypes");
                    if (list != null) {
                        for (C0665JT jt2 : list) {
                            if (jt2.bav().equals("taikodom.game.script.item.buff.module.ShieldModuleType")) {
                                System.out.println("** CONVERTING SHIELD MODULE ***");
                                ShieldBuffType art2 = (ShieldBuffType) bFf().mo6865M(ShieldBuffType.class);
                                art2.mo10S();
                                art2.mo11134J((C3892wO) jt.get("damageReduction"));
                                art2.mo11133H((C3892wO) jt.get("healthPoints"));
                                art2.mo11136Z((C3892wO) jt.get("recoveryTime"));
                                art2.mo11135X((C3892wO) jt.get("regeneration"));
                                art2.mo10827c(Module.C4010b.SELF);
                                kq.bdH().add(art2);
                                System.out.println("** Converted " + kq.mo19891ke().get() + "  ***");
                            } else if (jt2.bav().equals("taikodom.game.script.item.buff.module.HullModuleType")) {
                                System.out.println("** CONVERTING HULL MODULE ***");
                                HullBuffType aes2 = (HullBuffType) bFf().mo6865M(HullBuffType.class);
                                aes2.mo10S();
                                aes2.mo8605J((C3892wO) jt.get("damageReduction"));
                                aes2.mo8604H((C3892wO) jt.get("healthPoints"));
                                aes2.mo10827c(Module.C4010b.SELF);
                                kq.bdH().add(aes2);
                            } else if (jt2.bav().equals("taikodom.game.script.item.buff.module.ShipModuleType")) {
                                System.out.println("** CONVERTING SHIP MODULE ***");
                                ShipBuffType hOVar2 = (ShipBuffType) bFf().mo6865M(ShipBuffType.class);
                                hOVar2.mo10S();
                                hOVar2.mo19241j((C3892wO) jt.get("linearVelocity"));
                                hOVar2.mo19242l((C3892wO) jt.get("linearAcceleration"));
                                hOVar2.mo19243n((C3892wO) jt.get("angularVelocity"));
                                hOVar2.mo19244p((C3892wO) jt.get("angularAcceleration"));
                                hOVar2.mo19245r((C3892wO) jt.get("radarRange"));
                                hOVar2.mo10827c(Module.C4010b.SELF);
                                kq.bdH().add(hOVar2);
                            } else if (jt2.bav().equals("taikodom.game.script.item.buff.module.WeaponModuleType")) {
                                System.out.println("** CONVERTING WEAPON MODULE ***");
                                WeaponBuffType ajs2 = (WeaponBuffType) bFf().mo6865M(WeaponBuffType.class);
                                ajs2.mo10S();
                                ajs2.mo14084L((C3892wO) jt.get("fireRate"));
                                ajs2.mo14085N((C3892wO) jt.get("speed"));
                                ajs2.mo14086P((C3892wO) jt.get("range"));
                                ajs2.mo14087R((C3892wO) jt.get("shieldDamage"));
                                ajs2.mo14088T((C3892wO) jt.get("hullDamage"));
                                ajs2.mo14095cB(((Boolean) jt.get("doubleShot")).booleanValue());
                                ajs2.mo10827c(Module.C4010b.SELF);
                                kq.bdH().add(ajs2);
                            } else if (jt2.bav().equals("taikodom.game.script.item.buff.module.CruiseSpeedModuleType")) {
                                System.out.println("** CONVERTING CRUISESPEED MODULE ***");
                                CruiseSpeedBuffType alt2 = (CruiseSpeedBuffType) bFf().mo6865M(CruiseSpeedBuffType.class);
                                alt2.mo10S();
                                alt2.mo14677j((C3892wO) jt.get("linearVelocity"));
                                alt2.mo14678l((C3892wO) jt.get("linearAcceleration"));
                                alt2.mo14679n((C3892wO) jt.get("angularVelocity"));
                                alt2.mo14680p((C3892wO) jt.get("angularAcceleration"));
                                alt2.mo10827c(Module.C4010b.SELF);
                                kq.bdH().add(alt2);
                            }
                        }
                    }
                }
            } catch (Exception e) {
                mo8354g("BaseModuleType error", (Throwable) e);
            }
        }
    }
}
