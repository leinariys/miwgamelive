package game.script.item;

import game.network.message.serializable.C3438ra;
import game.script.crafting.CraftingRecipe;
import logic.baa.C1616Xf;
import logic.baa.C1634Xv;
import logic.baa.C5511aMd;
import logic.baa.C6485anp;
import logic.data.mbean.C0833MA;
import logic.res.code.C5640aRc;
import logic.res.code.C5663aRz;
import logic.res.code.C6494any;
import logic.res.html.C2491fm;
import p001a.*;

import java.util.Collection;

@C2712iu(mo19786Bt = MerchandiseType.class, version = "1.0.0")
@C5511aMd
@C6485anp
/* renamed from: a.afl  reason: case insensitive filesystem */
/* compiled from: a */
public class Blueprint extends Merchandise implements C1616Xf {
    public static final int _m_fieldCount = 0;
    public static final C5663aRz[] _m_fields = null;
    public static final int _m_methodCount = 0;
    public static final C2491fm[] _m_methods = null;
    public static final C5663aRz dCG = null;
    public static final C5663aRz dCH = null;
    public static final C5663aRz dCI = null;
    public static final long serialVersionUID = 0;
    public static C6494any ___iScriptClass;
    @C0064Am(aul = "3c51cb6c41020f7e647701c866553337", aum = 0)
    private static C3438ra<IngredientsCategory> dBN;
    @C0064Am(aul = "a31ebc7cb7b2a5dc244bd98d2536c79e", aum = 1)
    private static CraftingRecipe dBO;
    @C0064Am(aul = "c6c5e0843bd55998eb97bd408fc1ede5", aum = 2)
    private static long dBP;

    static {
        m21620V();
    }

    public Blueprint() {
        super((C5540aNg) null);
        super.mo10S();
    }

    public Blueprint(BlueprintType mr) {
        super((C5540aNg) null);
        super._m_script_init(mr);
    }

    public Blueprint(C5540aNg ang) {
        super(ang);
    }

    /* renamed from: V */
    static void m21620V() {
        serialVersionUID = (long) 1;
        _m_fieldCount = Merchandise._m_fieldCount + 3;
        _m_methodCount = Merchandise._m_methodCount + 0;
        int i = Merchandise._m_fieldCount;
        C5663aRz[] arzArr = new C5663aRz[(i + 3)];
        C5663aRz b = C5640aRc.m17844b(Blueprint.class, "3c51cb6c41020f7e647701c866553337", i);
        dCG = b;
        arzArr[i] = b;
        int i2 = i + 1;
        C5663aRz b2 = C5640aRc.m17844b(Blueprint.class, "a31ebc7cb7b2a5dc244bd98d2536c79e", i2);
        dCH = b2;
        arzArr[i2] = b2;
        int i3 = i2 + 1;
        C5663aRz b3 = C5640aRc.m17844b(Blueprint.class, "c6c5e0843bd55998eb97bd408fc1ede5", i3);
        dCI = b3;
        arzArr[i3] = b3;
        int i4 = i3 + 1;
        _m_fields = arzArr;
        C1634Xv.m11725a((Object[]) Merchandise._m_fields, (Object[]) _m_fields);
        _m_methods = new C2491fm[(Merchandise._m_methodCount + 0)];
        C1634Xv.m11725a((Object[]) Merchandise._m_methods, (Object[]) _m_methods);
        ___iScriptClass = aUO.m18566a(Blueprint.class, C0833MA.class, _m_fields, _m_methods);
    }

    private C3438ra bhS() {
        return ((BlueprintType) getType()).bhW();
    }

    private CraftingRecipe bhT() {
        return ((BlueprintType) getType()).bia();
    }

    private long bhU() {
        return ((BlueprintType) getType()).bic();
    }

    /* renamed from: bi */
    private void m21621bi(C3438ra raVar) {
        throw new C6039afL();
    }

    /* renamed from: e */
    private void m21622e(CraftingRecipe b) {
        throw new C6039afL();
    }

    /* renamed from: fc */
    private void m21623fc(long j) {
        throw new C6039afL();
    }

    /* renamed from: S */
    public void mo10S() {
        super.mo10S();
    }

    /* renamed from: T */
    public C6494any mo11T() {
        return ___iScriptClass;
    }

    /* renamed from: U */
    public C2491fm[] mo12U() {
        return _m_methods;
    }

    /* renamed from: W */
    public Object mo13W() {
        return new C0833MA(this);
    }

    /* renamed from: a */
    public Object mo14a(C0495Gr gr) {
        return super.mo14a(gr);
    }

    /* renamed from: a */
    public void mo15a(Collection collection, C0495Gr gr) {
        super.mo15a(collection, gr);
    }

    /* renamed from: c */
    public C5663aRz[] mo25c() {
        return _m_fields;
    }

    /* renamed from: e */
    public void mo13284e(BlueprintType mr) {
        super.mo11606e(mr);
    }
}
