package game.script.item;

import game.network.message.externalizable.aCE;
import game.script.TaikodomObject;
import game.script.item.durability.ItemDurability;
import game.script.player.Player;
import game.script.progression.ProgressionAbility;
import game.script.resource.Asset;
import logic.baa.*;
import logic.data.mbean.C0887Ms;
import logic.res.code.C5640aRc;
import logic.res.code.C5663aRz;
import logic.res.code.C6494any;
import logic.res.html.C2491fm;
import p001a.*;
import taikodom.infra.script.I18NString;

import java.util.Collection;

@C2712iu(mo19785Bs = {aWv.class}, mo19786Bt = BaseItemType.class)
@C5511aMd
@C6485anp
/* renamed from: a.auq  reason: case insensitive filesystem */
/* compiled from: a */
public abstract class Item extends TaikodomObject implements C1616Xf {

    /* renamed from: Lm */
    public static final C2491fm f5422Lm = null;

    /* renamed from: NY */
    public static final C5663aRz f5423NY = null;

    /* renamed from: QA */
    public static final C5663aRz f5424QA = null;
    public static final C2491fm _f_dispose_0020_0028_0029V = null;
    public static final C5663aRz _f_enabled = null;
    public static final C2491fm _f_getType_0020_0028_0029Ljava_002flang_002fObject_003b = null;
    public static final C2491fm _f_toString_0020_0028_0029Ljava_002flang_002fString_003b = null;
    public static final int _m_fieldCount = 0;
    public static final C5663aRz[] _m_fields = null;
    public static final int _m_methodCount = 0;
    public static final C2491fm[] _m_methods = null;
    public static final C2491fm aGt = null;
    public static final C5663aRz amV = null;
    public static final C5663aRz amX = null;
    public static final C5663aRz amZ = null;
    public static final C5663aRz anb = null;
    public static final C5663aRz and = null;
    public static final C5663aRz anf = null;
    public static final C5663aRz anh = null;
    public static final C5663aRz anj = null;
    public static final C5663aRz anl = null;
    public static final C5663aRz ann = null;
    public static final C5663aRz anp = null;
    public static final C5663aRz anr = null;
    public static final C2491fm bHG = null;
    public static final C2491fm cRr = null;
    public static final C2491fm cRs = null;
    public static final C2491fm ccL = null;
    public static final C2491fm ccM = null;
    public static final C2491fm esw = null;
    public static final C5663aRz ffW = null;
    public static final C5663aRz gCV = null;
    public static final C5663aRz gCW = null;
    public static final C5663aRz gCX = null;
    public static final C5663aRz gCY = null;
    public static final C2491fm gCZ = null;
    public static final C2491fm gDa = null;
    public static final C2491fm gDb = null;
    public static final C2491fm gDc = null;
    public static final C2491fm gDd = null;
    public static final C2491fm gDe = null;
    public static final C2491fm gDf = null;
    public static final C2491fm gDg = null;
    public static final C2491fm gDh = null;
    public static final C2491fm gDi = null;
    public static final C2491fm gDj = null;
    public static final C2491fm gDk = null;
    public static final C2491fm gDl = null;
    public static final C2491fm gDm = null;
    public static final C2491fm gDn = null;
    public static final C2491fm gDo = null;
    public static final C2491fm gDp = null;
    public static final C2491fm gDq = null;
    public static final C2491fm gDr = null;
    public static final C2491fm gDs = null;
    public static final C2491fm gDt = null;
    public static final long serialVersionUID = 0;
    /* renamed from: zQ */
    public static final C5663aRz f5428zQ = null;
    public static C6494any ___iScriptClass;
    @C0064Am(aul = "c7452a0dc441c90d3a53aabc27e4c1fc", aum = 2)
    private static long amU;
    @C0064Am(aul = "9c5539b1279d8bd9fe780e45622ab34b", aum = 3)
    @C2616he("UnitVolume")
    private static float amW;
    @C0064Am(aul = "af7b3499839a154ef6bc93e5dea76412", aum = 4)
    private static ItemTypeCategory amY;
    @C0064Am(aul = "b4343d42bc813f96de1273b1fa544374", aum = 6)
    private static ProgressionAbility ana;
    @C0064Am(aul = "1c6ea46d83971e4c1287e307e65f3861", aum = 7)
    @C5566aOg
    private static long anc;
    @C0064Am(aul = "725d666e1d4cbcba5e14994444fb6a3f", aum = 8)
    @C5566aOg
    private static long ane;
    @C0064Am(aul = "6132cf056f1e8309fbe04942b0dcfdd5", aum = 9)
    @C5566aOg
    private static long ang;
    @C0064Am(aul = "40be3b8685328860ee01609d3beb5b4e", aum = 10)
    @C5566aOg
    private static int ani;
    @C0064Am(aul = "c1f45ed45e7875b30881ee70a4c39c8e", aum = 11)
    @C5566aOg
    private static int ank;
    @C0064Am(aul = "13fd94945c288e737e625bbcf075462d", aum = 12)
    @C5566aOg
    private static int anm;
    @C0064Am(aul = "86678bf4ad3b6eb5f931fd0e118cb07d", aum = 13)
    @C5566aOg
    private static int ano;
    @C0064Am(aul = "53cb5dbadd7e2f7436dcb8a50c9f2a42", aum = 14)
    @C5566aOg
    private static int anq;
    @C0064Am(aul = "69304c502c1a41b528915e749a9144cd", aum = 19)
    private static ItemLocation apb;
    @C0064Am(aul = "219a042b26267a43f7e1b801a1332d30", aum = 16)
    private static boolean dAa;
    @C0064Am(aul = "6a4c2a2c913fb14c91a6e1c42296c372", aum = 17)
    private static boolean dAb;
    @C0064Am(aul = "821804cd355b30f54c561e30d7d25ea0", aum = 20)
    private static ItemDurability dAc;
    @C0064Am(aul = "459baf016d809a4bb4da06ac821b3fb6", aum = 15)
    private static boolean dzZ;
    @C0064Am(aul = "ca428daaff0a5283aa010914f6f49ae9", aum = 18)
    private static boolean enabled;
    @C0064Am(aul = "8074b666b5934f5c82462b12c2e322d6", aum = 5)

    /* renamed from: jn */
    private static Asset f5425jn;
    @C0064Am(aul = "7679847a1e7157016fadec2efb6cd451", aum = 1)

    /* renamed from: nh */
    private static I18NString f5426nh;
    @C0064Am(aul = "65cc2adcbcbe32ef26363345f0f65861", aum = 0)

    /* renamed from: zP */
    private static I18NString f5427zP;

    static {
        m26409V();
    }

    public Item() {
        super((C5540aNg) null);
        super.mo10S();
    }

    public Item(C5540aNg ang2) {
        super(ang2);
    }

    public Item(ItemType jCVar) {
        super((C5540aNg) null);
        super._m_script_init(jCVar);
    }

    /* renamed from: V */
    static void m26409V() {
        serialVersionUID = (long) 1;
        _m_fieldCount = TaikodomObject._m_fieldCount + 21;
        _m_methodCount = TaikodomObject._m_methodCount + 32;
        int i = TaikodomObject._m_fieldCount;
        C5663aRz[] arzArr = new C5663aRz[(i + 21)];
        C5663aRz b = C5640aRc.m17844b(Item.class, "65cc2adcbcbe32ef26363345f0f65861", i);
        f5428zQ = b;
        arzArr[i] = b;
        int i2 = i + 1;
        C5663aRz b2 = C5640aRc.m17844b(Item.class, "7679847a1e7157016fadec2efb6cd451", i2);
        f5424QA = b2;
        arzArr[i2] = b2;
        int i3 = i2 + 1;
        C5663aRz b3 = C5640aRc.m17844b(Item.class, "c7452a0dc441c90d3a53aabc27e4c1fc", i3);
        amV = b3;
        arzArr[i3] = b3;
        int i4 = i3 + 1;
        C5663aRz b4 = C5640aRc.m17844b(Item.class, "9c5539b1279d8bd9fe780e45622ab34b", i4);
        amX = b4;
        arzArr[i4] = b4;
        int i5 = i4 + 1;
        C5663aRz b5 = C5640aRc.m17844b(Item.class, "af7b3499839a154ef6bc93e5dea76412", i5);
        amZ = b5;
        arzArr[i5] = b5;
        int i6 = i5 + 1;
        C5663aRz b6 = C5640aRc.m17844b(Item.class, "8074b666b5934f5c82462b12c2e322d6", i6);
        f5423NY = b6;
        arzArr[i6] = b6;
        int i7 = i6 + 1;
        C5663aRz b7 = C5640aRc.m17844b(Item.class, "b4343d42bc813f96de1273b1fa544374", i7);
        anb = b7;
        arzArr[i7] = b7;
        int i8 = i7 + 1;
        C5663aRz b8 = C5640aRc.m17844b(Item.class, "1c6ea46d83971e4c1287e307e65f3861", i8);
        and = b8;
        arzArr[i8] = b8;
        int i9 = i8 + 1;
        C5663aRz b9 = C5640aRc.m17844b(Item.class, "725d666e1d4cbcba5e14994444fb6a3f", i9);
        anf = b9;
        arzArr[i9] = b9;
        int i10 = i9 + 1;
        C5663aRz b10 = C5640aRc.m17844b(Item.class, "6132cf056f1e8309fbe04942b0dcfdd5", i10);
        anh = b10;
        arzArr[i10] = b10;
        int i11 = i10 + 1;
        C5663aRz b11 = C5640aRc.m17844b(Item.class, "40be3b8685328860ee01609d3beb5b4e", i11);
        anj = b11;
        arzArr[i11] = b11;
        int i12 = i11 + 1;
        C5663aRz b12 = C5640aRc.m17844b(Item.class, "c1f45ed45e7875b30881ee70a4c39c8e", i12);
        anl = b12;
        arzArr[i12] = b12;
        int i13 = i12 + 1;
        C5663aRz b13 = C5640aRc.m17844b(Item.class, "13fd94945c288e737e625bbcf075462d", i13);
        ann = b13;
        arzArr[i13] = b13;
        int i14 = i13 + 1;
        C5663aRz b14 = C5640aRc.m17844b(Item.class, "86678bf4ad3b6eb5f931fd0e118cb07d", i14);
        anp = b14;
        arzArr[i14] = b14;
        int i15 = i14 + 1;
        C5663aRz b15 = C5640aRc.m17844b(Item.class, "53cb5dbadd7e2f7436dcb8a50c9f2a42", i15);
        anr = b15;
        arzArr[i15] = b15;
        int i16 = i15 + 1;
        C5663aRz b16 = C5640aRc.m17844b(Item.class, "459baf016d809a4bb4da06ac821b3fb6", i16);
        gCV = b16;
        arzArr[i16] = b16;
        int i17 = i16 + 1;
        C5663aRz b17 = C5640aRc.m17844b(Item.class, "219a042b26267a43f7e1b801a1332d30", i17);
        gCW = b17;
        arzArr[i17] = b17;
        int i18 = i17 + 1;
        C5663aRz b18 = C5640aRc.m17844b(Item.class, "6a4c2a2c913fb14c91a6e1c42296c372", i18);
        gCX = b18;
        arzArr[i18] = b18;
        int i19 = i18 + 1;
        C5663aRz b19 = C5640aRc.m17844b(Item.class, "ca428daaff0a5283aa010914f6f49ae9", i19);
        _f_enabled = b19;
        arzArr[i19] = b19;
        int i20 = i19 + 1;
        C5663aRz b20 = C5640aRc.m17844b(Item.class, "69304c502c1a41b528915e749a9144cd", i20);
        ffW = b20;
        arzArr[i20] = b20;
        int i21 = i20 + 1;
        C5663aRz b21 = C5640aRc.m17844b(Item.class, "821804cd355b30f54c561e30d7d25ea0", i21);
        gCY = b21;
        arzArr[i21] = b21;
        int i22 = i21 + 1;
        _m_fields = arzArr;
        C1634Xv.m11725a((Object[]) TaikodomObject._m_fields, (Object[]) _m_fields);
        int i23 = TaikodomObject._m_methodCount;
        C2491fm[] fmVarArr = new C2491fm[(i23 + 32)];
        C2491fm a = C4105zY.m41624a(Item.class, "fb1c8e68fc86943d31fb56a8495c459e", i23);
        _f_toString_0020_0028_0029Ljava_002flang_002fString_003b = a;
        fmVarArr[i23] = a;
        int i24 = i23 + 1;
        C2491fm a2 = C4105zY.m41624a(Item.class, "c03ef372ff1fc61163f8b78e540b805b", i24);
        aGt = a2;
        fmVarArr[i24] = a2;
        int i25 = i24 + 1;
        C2491fm a3 = C4105zY.m41624a(Item.class, "c5225e210a39d30a1938d61737e68915", i25);
        bHG = a3;
        fmVarArr[i25] = a3;
        int i26 = i25 + 1;
        C2491fm a4 = C4105zY.m41624a(Item.class, "f16cb8e25d1042a4428ddddd781e9c7c", i26);
        ccL = a4;
        fmVarArr[i26] = a4;
        int i27 = i26 + 1;
        C2491fm a5 = C4105zY.m41624a(Item.class, "d2ab11a1e9e24a10d457d3e57eb32dae", i27);
        ccM = a5;
        fmVarArr[i27] = a5;
        int i28 = i27 + 1;
        C2491fm a6 = C4105zY.m41624a(Item.class, "ed0d1ae4d8ad41e920c0c920f982b21f", i28);
        esw = a6;
        fmVarArr[i28] = a6;
        int i29 = i28 + 1;
        C2491fm a7 = C4105zY.m41624a(Item.class, "e5642fe3ffc2b090d751bea5ed63fc91", i29);
        cRr = a7;
        fmVarArr[i29] = a7;
        int i30 = i29 + 1;
        C2491fm a8 = C4105zY.m41624a(Item.class, "4bb37d5683d04a8bb097f40ec6c1b595", i30);
        gCZ = a8;
        fmVarArr[i30] = a8;
        int i31 = i30 + 1;
        C2491fm a9 = C4105zY.m41624a(Item.class, "0c7938b733de7cc697ff17e77778cd69", i31);
        cRs = a9;
        fmVarArr[i31] = a9;
        int i32 = i31 + 1;
        C2491fm a10 = C4105zY.m41624a(Item.class, "c594abd537e707fe3bf9933ddc1c226b", i32);
        gDa = a10;
        fmVarArr[i32] = a10;
        int i33 = i32 + 1;
        C2491fm a11 = C4105zY.m41624a(Item.class, "906d6166a9b9b71ebf03761ea37a6c3f", i33);
        gDb = a11;
        fmVarArr[i33] = a11;
        int i34 = i33 + 1;
        C2491fm a12 = C4105zY.m41624a(Item.class, "7a82ad0e45c0b2d26e7fbcd9c9d7c9fd", i34);
        gDc = a12;
        fmVarArr[i34] = a12;
        int i35 = i34 + 1;
        C2491fm a13 = C4105zY.m41624a(Item.class, "5ceaaa31a1f34f0042ab46242a7a4de8", i35);
        gDd = a13;
        fmVarArr[i35] = a13;
        int i36 = i35 + 1;
        C2491fm a14 = C4105zY.m41624a(Item.class, "5364832a444369159b64914656d7b31e", i36);
        gDe = a14;
        fmVarArr[i36] = a14;
        int i37 = i36 + 1;
        C2491fm a15 = C4105zY.m41624a(Item.class, "1787e651c1de11529b8c0650c9de21c3", i37);
        gDf = a15;
        fmVarArr[i37] = a15;
        int i38 = i37 + 1;
        C2491fm a16 = C4105zY.m41624a(Item.class, "5006fd22d71eecc5c7be951727778d7e", i38);
        gDg = a16;
        fmVarArr[i38] = a16;
        int i39 = i38 + 1;
        C2491fm a17 = C4105zY.m41624a(Item.class, "86410baf9c1f40403be9325de43de873", i39);
        _f_dispose_0020_0028_0029V = a17;
        fmVarArr[i39] = a17;
        int i40 = i39 + 1;
        C2491fm a18 = C4105zY.m41624a(Item.class, "20b9fe10f15ab766725651a7b1640fb5", i40);
        gDh = a18;
        fmVarArr[i40] = a18;
        int i41 = i40 + 1;
        C2491fm a19 = C4105zY.m41624a(Item.class, "2f90d31e7cfab2c4a2561d3e83aa2f9f", i41);
        gDi = a19;
        fmVarArr[i41] = a19;
        int i42 = i41 + 1;
        C2491fm a20 = C4105zY.m41624a(Item.class, "26a131844e1b0bd76e6a9fe39d968c2c", i42);
        f5422Lm = a20;
        fmVarArr[i42] = a20;
        int i43 = i42 + 1;
        C2491fm a21 = C4105zY.m41624a(Item.class, "a83e21ee5d79eab7a52f21ef9d576b61", i43);
        gDj = a21;
        fmVarArr[i43] = a21;
        int i44 = i43 + 1;
        C2491fm a22 = C4105zY.m41624a(Item.class, "92988420d72da5784a1a2b5a9bc92e86", i44);
        gDk = a22;
        fmVarArr[i44] = a22;
        int i45 = i44 + 1;
        C2491fm a23 = C4105zY.m41624a(Item.class, "69d78cd1552928ed0a56ebc2a9bc56d6", i45);
        gDl = a23;
        fmVarArr[i45] = a23;
        int i46 = i45 + 1;
        C2491fm a24 = C4105zY.m41624a(Item.class, "fd7a5f2a53cccd7ee70b37a153589772", i46);
        gDm = a24;
        fmVarArr[i46] = a24;
        int i47 = i46 + 1;
        C2491fm a25 = C4105zY.m41624a(Item.class, "5cff93aa112e15a9caff9e70543918a5", i47);
        gDn = a25;
        fmVarArr[i47] = a25;
        int i48 = i47 + 1;
        C2491fm a26 = C4105zY.m41624a(Item.class, "7b31865bfbd29672e03a148dbcb29d42", i48);
        gDo = a26;
        fmVarArr[i48] = a26;
        int i49 = i48 + 1;
        C2491fm a27 = C4105zY.m41624a(Item.class, "81343063cbe53f3d768b433bd11a61dd", i49);
        gDp = a27;
        fmVarArr[i49] = a27;
        int i50 = i49 + 1;
        C2491fm a28 = C4105zY.m41624a(Item.class, "44ff143d6bce09b808ba0e78ea787715", i50);
        gDq = a28;
        fmVarArr[i50] = a28;
        int i51 = i50 + 1;
        C2491fm a29 = C4105zY.m41624a(Item.class, "5bfe24705a1876fbddba1671b7a49c38", i51);
        gDr = a29;
        fmVarArr[i51] = a29;
        int i52 = i51 + 1;
        C2491fm a30 = C4105zY.m41624a(Item.class, "0d836465964f19590038698ba82b20ee", i52);
        gDs = a30;
        fmVarArr[i52] = a30;
        int i53 = i52 + 1;
        C2491fm a31 = C4105zY.m41624a(Item.class, "40462d54d6c5d15ab90753969a672a7d", i53);
        gDt = a31;
        fmVarArr[i53] = a31;
        int i54 = i53 + 1;
        C2491fm a32 = C4105zY.m41624a(Item.class, "42d7d37306a85232294aa9f1f755c185", i54);
        _f_getType_0020_0028_0029Ljava_002flang_002fObject_003b = a32;
        fmVarArr[i54] = a32;
        int i55 = i54 + 1;
        _m_methods = fmVarArr;
        C1634Xv.m11725a((Object[]) TaikodomObject._m_methods, (Object[]) _m_methods);
        ___iScriptClass = aUO.m18566a(Item.class, C0887Ms.class, _m_fields, _m_methods);
    }

    /* renamed from: Hl */
    private long m26395Hl() {
        return ((ItemType) getType()).mo19876Hy();
    }

    /* renamed from: Hm */
    private float m26396Hm() {
        return ((ItemType) getType()).mo19865HA();
    }

    /* renamed from: Hn */
    private ItemTypeCategory m26397Hn() {
        return ((ItemType) getType()).mo19866HC();
    }

    /* renamed from: Ho */
    private ProgressionAbility m26398Ho() {
        return ((ItemType) getType()).mo19867HE();
    }

    /* renamed from: Hp */
    private long m26399Hp() {
        return ((ItemType) getType()).mo19868HG();
    }

    /* renamed from: Hq */
    private long m26400Hq() {
        return ((ItemType) getType()).mo19869HI();
    }

    /* renamed from: Hr */
    private long m26401Hr() {
        return ((ItemType) getType()).mo19870HK();
    }

    /* renamed from: Hs */
    private int m26402Hs() {
        return ((ItemType) getType()).mo19871HM();
    }

    /* renamed from: Ht */
    private int m26403Ht() {
        return ((ItemType) getType()).mo19872HO();
    }

    /* renamed from: Hu */
    private int m26404Hu() {
        return ((ItemType) getType()).mo19873HQ();
    }

    /* renamed from: Hv */
    private int m26405Hv() {
        return ((ItemType) getType()).mo19874HS();
    }

    /* renamed from: Hw */
    private int m26406Hw() {
        return ((ItemType) getType()).mo19875HU();
    }

    /* renamed from: J */
    private void m26407J(boolean z) {
        bFf().mo5608dq().mo3153a(_f_enabled, z);
    }

    @C0064Am(aul = "c03ef372ff1fc61163f8b78e540b805b", aum = 0)
    /* renamed from: PK */
    private int m26408PK() {
        throw new aWi(new aCE(this, aGt, new Object[0]));
    }

    /* renamed from: a */
    private void m26410a(ItemDurability azm) {
        bFf().mo5608dq().mo3197f(gCY, azm);
    }

    /* renamed from: aP */
    private void m26411aP(float f) {
        throw new C6039afL();
    }

    @C0064Am(aul = "c5225e210a39d30a1938d61737e68915", aum = 0)
    private float aoH() {
        throw new aWi(new aCE(this, bHG, new Object[0]));
    }

    @C0064Am(aul = "f16cb8e25d1042a4428ddddd781e9c7c", aum = 0)
    @C5566aOg
    private void asR() {
        throw new aWi(new aCE(this, ccL, new Object[0]));
    }

    /* renamed from: b */
    private void m26413b(ProgressionAbility lk) {
        throw new C6039afL();
    }

    /* renamed from: bN */
    private void m26414bN(long j) {
        throw new C6039afL();
    }

    /* renamed from: bO */
    private void m26415bO(long j) {
        throw new C6039afL();
    }

    /* renamed from: bP */
    private void m26416bP(long j) {
        throw new C6039afL();
    }

    /* renamed from: bQ */
    private void m26417bQ(long j) {
        throw new C6039afL();
    }

    /* renamed from: br */
    private void m26419br(I18NString i18NString) {
        throw new C6039afL();
    }

    @C0064Am(aul = "d2ab11a1e9e24a10d457d3e57eb32dae", aum = 0)
    @C5566aOg
    /* renamed from: c */
    private void m26420c(ItemLocation aag) {
        throw new aWi(new aCE(this, ccM, new Object[]{aag}));
    }

    /* renamed from: c */
    private void m26421c(ItemTypeCategory aai) {
        throw new C6039afL();
    }

    /* renamed from: ci */
    private void m26422ci(int i) {
        throw new C6039afL();
    }

    /* renamed from: cj */
    private void m26423cj(int i) {
        throw new C6039afL();
    }

    /* renamed from: ck */
    private void m26424ck(int i) {
        throw new C6039afL();
    }

    /* renamed from: cl */
    private void m26425cl(int i) {
        throw new C6039afL();
    }

    /* renamed from: cm */
    private void m26426cm(int i) {
        throw new C6039afL();
    }

    private boolean cwU() {
        return bFf().mo5608dq().mo3201h(gCV);
    }

    private boolean cwV() {
        return bFf().mo5608dq().mo3201h(gCW);
    }

    private boolean cwW() {
        return bFf().mo5608dq().mo3201h(gCX);
    }

    private ItemLocation cwX() {
        return (ItemLocation) bFf().mo5608dq().mo3214p(ffW);
    }

    private ItemDurability cwY() {
        return (ItemDurability) bFf().mo5608dq().mo3214p(gCY);
    }

    @C0064Am(aul = "7a82ad0e45c0b2d26e7fbcd9c9d7c9fd", aum = 0)
    @C5566aOg
    private void cxe() {
        throw new aWi(new aCE(this, gDc, new Object[0]));
    }

    @C4034yP
    @C5566aOg
    @C0064Am(aul = "20b9fe10f15ab766725651a7b1640fb5", aum = 0)
    @C2499fr
    private void cxi() {
        throw new aWi(new aCE(this, gDh, new Object[0]));
    }

    @C0064Am(aul = "0d836465964f19590038698ba82b20ee", aum = 0)
    @C5566aOg
    private boolean cxv() {
        throw new aWi(new aCE(this, gDs, new Object[0]));
    }

    @C0064Am(aul = "40462d54d6c5d15ab90753969a672a7d", aum = 0)
    @C5566aOg
    private void cxx() {
        throw new aWi(new aCE(this, gDt, new Object[0]));
    }

    /* renamed from: d */
    private void m26427d(I18NString i18NString) {
        throw new C6039afL();
    }

    /* renamed from: f */
    private void m26428f(ItemLocation aag) {
        bFf().mo5608dq().mo3197f(ffW, aag);
    }

    @C0064Am(aul = "86410baf9c1f40403be9325de43de873", aum = 0)
    @C5566aOg
    @C2499fr
    /* renamed from: fg */
    private void m26429fg() {
        throw new aWi(new aCE(this, _f_dispose_0020_0028_0029V, new Object[0]));
    }

    @C0064Am(aul = "5364832a444369159b64914656d7b31e", aum = 0)
    @C5566aOg
    /* renamed from: g */
    private void m26430g(ItemLocation aag) {
        throw new aWi(new aCE(this, gDe, new Object[]{aag}));
    }

    /* renamed from: gF */
    private void m26431gF(boolean z) {
        bFf().mo5608dq().mo3153a(gCV, z);
    }

    /* renamed from: gG */
    private void m26432gG(boolean z) {
        bFf().mo5608dq().mo3153a(gCW, z);
    }

    /* renamed from: gH */
    private void m26433gH(boolean z) {
        bFf().mo5608dq().mo3153a(gCX, z);
    }

    @C0064Am(aul = "5ceaaa31a1f34f0042ab46242a7a4de8", aum = 0)
    @C5566aOg
    /* renamed from: gI */
    private void m26434gI(boolean z) {
        throw new aWi(new aCE(this, gDd, new Object[]{new Boolean(z)}));
    }

    @C5566aOg
    /* renamed from: gJ */
    private final void m26435gJ(boolean z) {
        switch (bFf().mo6893i(gDd)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, gDd, new Object[]{new Boolean(z)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, gDd, new Object[]{new Boolean(z)}));
                break;
        }
        m26434gI(z);
    }

    @C0064Am(aul = "5006fd22d71eecc5c7be951727778d7e", aum = 0)
    @C5566aOg
    /* renamed from: gK */
    private void m26436gK(boolean z) {
        throw new aWi(new aCE(this, gDg, new Object[]{new Boolean(z)}));
    }

    @C0064Am(aul = "69d78cd1552928ed0a56ebc2a9bc56d6", aum = 0)
    @C5566aOg
    /* renamed from: gM */
    private void m26437gM(boolean z) {
        throw new aWi(new aCE(this, gDl, new Object[]{new Boolean(z)}));
    }

    @C0064Am(aul = "5cff93aa112e15a9caff9e70543918a5", aum = 0)
    @C5566aOg
    /* renamed from: gO */
    private void m26438gO(boolean z) {
        throw new aWi(new aCE(this, gDn, new Object[]{new Boolean(z)}));
    }

    @C0064Am(aul = "5bfe24705a1876fbddba1671b7a49c38", aum = 0)
    @C5566aOg
    /* renamed from: iV */
    private void m26439iV(long j) {
        throw new aWi(new aCE(this, gDr, new Object[]{new Long(j)}));
    }

    /* renamed from: kb */
    private I18NString m26440kb() {
        return ((ItemType) getType()).mo19891ke();
    }

    @C0064Am(aul = "26a131844e1b0bd76e6a9fe39d968c2c", aum = 0)
    @C5566aOg
    @C2499fr
    /* renamed from: qU */
    private void m26441qU() {
        throw new aWi(new aCE(this, f5422Lm, new Object[0]));
    }

    @C0064Am(aul = "42d7d37306a85232294aa9f1f755c185", aum = 0)
    /* renamed from: qW */
    private Object m26442qW() {
        return bAP();
    }

    /* renamed from: rm */
    private boolean m26443rm() {
        return bFf().mo5608dq().mo3201h(_f_enabled);
    }

    /* renamed from: sG */
    private Asset m26444sG() {
        return ((ItemType) getType()).mo12100sK();
    }

    /* renamed from: t */
    private void m26445t(Asset tCVar) {
        throw new C6039afL();
    }

    /* renamed from: vT */
    private I18NString m26446vT() {
        return ((ItemType) getType()).mo19893vW();
    }

    /* renamed from: Iq */
    public int mo302Iq() {
        switch (bFf().mo6893i(aGt)) {
            case 0:
                return 0;
            case 2:
                return ((Integer) bFf().mo5606d(new aCE(this, aGt, new Object[0]))).intValue();
            case 3:
                bFf().mo5606d(new aCE(this, aGt, new Object[0]));
                break;
        }
        return m26408PK();
    }

    /* renamed from: S */
    public void mo10S() {
        super.mo10S();
    }

    /* renamed from: T */
    public C6494any mo11T() {
        return ___iScriptClass;
    }

    /* renamed from: U */
    public C2491fm[] mo12U() {
        return _m_methods;
    }

    /* renamed from: W */
    public Object mo13W() {
        return new C0887Ms(this);
    }

    /* renamed from: a */
    public Object mo14a(C0495Gr gr) {
        Object[] args = gr.getArgs();
        switch (gr.mo2417hq() - TaikodomObject._m_methodCount) {
            case 0:
                return m26412au();
            case 1:
                return new Integer(m26408PK());
            case 2:
                return new Float(aoH());
            case 3:
                asR();
                return null;
            case 4:
                m26420c((ItemLocation) args[0]);
                return null;
            case 5:
                return bAO();
            case 6:
                return new Boolean(aNM());
            case 7:
                return new Boolean(cwZ());
            case 8:
                return new Boolean(aNO());
            case 9:
                return new Boolean(cxb());
            case 10:
                return cxd();
            case 11:
                cxe();
                return null;
            case 12:
                m26434gI(((Boolean) args[0]).booleanValue());
                return null;
            case 13:
                m26430g((ItemLocation) args[0]);
                return null;
            case 14:
                return new Boolean(cxg());
            case 15:
                m26436gK(((Boolean) args[0]).booleanValue());
                return null;
            case 16:
                m26429fg();
                return null;
            case 17:
                cxi();
                return null;
            case 18:
                return cxk();
            case 19:
                m26441qU();
                return null;
            case 20:
                return new Boolean(m26418bV((Player) args[0]));
            case 21:
                return new Boolean(cxm());
            case 22:
                m26437gM(((Boolean) args[0]).booleanValue());
                return null;
            case 23:
                return new Boolean(cxo());
            case 24:
                m26438gO(((Boolean) args[0]).booleanValue());
                return null;
            case 25:
                return new Boolean(cxp());
            case 26:
                return new Long(cxr());
            case 27:
                return new Long(cxt());
            case 28:
                m26439iV(((Long) args[0]).longValue());
                return null;
            case 29:
                return new Boolean(cxv());
            case 30:
                cxx();
                return null;
            case 31:
                return m26442qW();
            default:
                return super.mo14a(gr);
        }
    }

    /* renamed from: a */
    public void mo15a(Collection collection, C0495Gr gr) {
        super.mo15a(collection, gr);
    }

    public boolean aNN() {
        switch (bFf().mo6893i(cRr)) {
            case 0:
                return false;
            case 2:
                return ((Boolean) bFf().mo5606d(new aCE(this, cRr, new Object[0]))).booleanValue();
            case 3:
                bFf().mo5606d(new aCE(this, cRr, new Object[0]));
                break;
        }
        return aNM();
    }

    public boolean aNP() {
        switch (bFf().mo6893i(cRs)) {
            case 0:
                return false;
            case 2:
                return ((Boolean) bFf().mo5606d(new aCE(this, cRs, new Object[0]))).booleanValue();
            case 3:
                bFf().mo5606d(new aCE(this, cRs, new Object[0]));
                break;
        }
        return aNO();
    }

    @C5566aOg
    public void asS() {
        switch (bFf().mo6893i(ccL)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, ccL, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, ccL, new Object[0]));
                break;
        }
        asR();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "ItemType")
    public ItemType bAP() {
        switch (bFf().mo6893i(esw)) {
            case 0:
                return null;
            case 2:
                return (ItemType) bFf().mo5606d(new aCE(this, esw, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, esw, new Object[0]));
                break;
        }
        return bAO();
    }

    public final ItemLocation bNh() {
        switch (bFf().mo6893i(gDb)) {
            case 0:
                return null;
            case 2:
                return (ItemLocation) bFf().mo5606d(new aCE(this, gDb, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, gDb, new Object[0]));
                break;
        }
        return cxd();
    }

    /* renamed from: bW */
    public final boolean mo16383bW(Player aku) {
        switch (bFf().mo6893i(gDj)) {
            case 0:
                return false;
            case 2:
                return ((Boolean) bFf().mo5606d(new aCE(this, gDj, new Object[]{aku}))).booleanValue();
            case 3:
                bFf().mo5606d(new aCE(this, gDj, new Object[]{aku}));
                break;
        }
        return m26418bV(aku);
    }

    /* renamed from: c */
    public C5663aRz[] mo25c() {
        return _m_fields;
    }

    public boolean cxa() {
        switch (bFf().mo6893i(gCZ)) {
            case 0:
                return false;
            case 2:
                return ((Boolean) bFf().mo5606d(new aCE(this, gCZ, new Object[0]))).booleanValue();
            case 3:
                bFf().mo5606d(new aCE(this, gCZ, new Object[0]));
                break;
        }
        return cwZ();
    }

    public boolean cxc() {
        switch (bFf().mo6893i(gDa)) {
            case 0:
                return false;
            case 2:
                return ((Boolean) bFf().mo5606d(new aCE(this, gDa, new Object[0]))).booleanValue();
            case 3:
                bFf().mo5606d(new aCE(this, gDa, new Object[0]));
                break;
        }
        return cxb();
    }

    @C5566aOg
    public final void cxf() {
        switch (bFf().mo6893i(gDc)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, gDc, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, gDc, new Object[0]));
                break;
        }
        cxe();
    }

    public final boolean cxh() {
        switch (bFf().mo6893i(gDf)) {
            case 0:
                return false;
            case 2:
                return ((Boolean) bFf().mo5606d(new aCE(this, gDf, new Object[0]))).booleanValue();
            case 3:
                bFf().mo5606d(new aCE(this, gDf, new Object[0]));
                break;
        }
        return cxg();
    }

    @C4034yP
    @C5566aOg
    @C2499fr
    public void cxj() {
        switch (bFf().mo6893i(gDh)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, gDh, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, gDh, new Object[0]));
                break;
        }
        cxi();
    }

    public Item cxl() {
        switch (bFf().mo6893i(gDi)) {
            case 0:
                return null;
            case 2:
                return (Item) bFf().mo5606d(new aCE(this, gDi, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, gDi, new Object[0]));
                break;
        }
        return cxk();
    }

    public final boolean cxn() {
        switch (bFf().mo6893i(gDk)) {
            case 0:
                return false;
            case 2:
                return ((Boolean) bFf().mo5606d(new aCE(this, gDk, new Object[0]))).booleanValue();
            case 3:
                bFf().mo5606d(new aCE(this, gDk, new Object[0]));
                break;
        }
        return cxm();
    }

    public boolean cxq() {
        switch (bFf().mo6893i(gDo)) {
            case 0:
                return false;
            case 2:
                return ((Boolean) bFf().mo5606d(new aCE(this, gDo, new Object[0]))).booleanValue();
            case 3:
                bFf().mo5606d(new aCE(this, gDo, new Object[0]));
                break;
        }
        return cxp();
    }

    public long cxs() {
        switch (bFf().mo6893i(gDp)) {
            case 0:
                return 0;
            case 2:
                return ((Long) bFf().mo5606d(new aCE(this, gDp, new Object[0]))).longValue();
            case 3:
                bFf().mo5606d(new aCE(this, gDp, new Object[0]));
                break;
        }
        return cxr();
    }

    public long cxu() {
        switch (bFf().mo6893i(gDq)) {
            case 0:
                return 0;
            case 2:
                return ((Long) bFf().mo5606d(new aCE(this, gDq, new Object[0]))).longValue();
            case 3:
                bFf().mo5606d(new aCE(this, gDq, new Object[0]));
                break;
        }
        return cxt();
    }

    @C5566aOg
    public boolean cxw() {
        switch (bFf().mo6893i(gDs)) {
            case 0:
                return false;
            case 2:
                return ((Boolean) bFf().mo5606d(new aCE(this, gDs, new Object[0]))).booleanValue();
            case 3:
                bFf().mo5606d(new aCE(this, gDs, new Object[0]));
                break;
        }
        return cxv();
    }

    @C5566aOg
    public void cxy() {
        switch (bFf().mo6893i(gDt)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, gDt, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, gDt, new Object[0]));
                break;
        }
        cxx();
    }

    @C5566aOg
    /* renamed from: d */
    public void mo12934d(ItemLocation aag) {
        switch (bFf().mo6893i(ccM)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, ccM, new Object[]{aag}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, ccM, new Object[]{aag}));
                break;
        }
        m26420c(aag);
    }

    @C5566aOg
    @C2499fr
    public void dispose() {
        switch (bFf().mo6893i(_f_dispose_0020_0028_0029V)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, _f_dispose_0020_0028_0029V, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, _f_dispose_0020_0028_0029V, new Object[0]));
                break;
        }
        m26429fg();
    }

    @C5566aOg
    /* renamed from: gL */
    public final void mo16393gL(boolean z) {
        switch (bFf().mo6893i(gDg)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, gDg, new Object[]{new Boolean(z)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, gDg, new Object[]{new Boolean(z)}));
                break;
        }
        m26436gK(z);
    }

    @C5566aOg
    /* renamed from: gN */
    public final void mo16394gN(boolean z) {
        switch (bFf().mo6893i(gDl)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, gDl, new Object[]{new Boolean(z)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, gDl, new Object[]{new Boolean(z)}));
                break;
        }
        m26437gM(z);
    }

    public /* bridge */ /* synthetic */ Object getType() {
        switch (bFf().mo6893i(_f_getType_0020_0028_0029Ljava_002flang_002fObject_003b)) {
            case 0:
                return null;
            case 2:
                return bFf().mo5606d(new aCE(this, _f_getType_0020_0028_0029Ljava_002flang_002fObject_003b, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, _f_getType_0020_0028_0029Ljava_002flang_002fObject_003b, new Object[0]));
                break;
        }
        return m26442qW();
    }

    public float getVolume() {
        switch (bFf().mo6893i(bHG)) {
            case 0:
                return 0.0f;
            case 2:
                return ((Float) bFf().mo5606d(new aCE(this, bHG, new Object[0]))).floatValue();
            case 3:
                bFf().mo5606d(new aCE(this, bHG, new Object[0]));
                break;
        }
        return aoH();
    }

    /* access modifiers changed from: package-private */
    @C5566aOg
    /* renamed from: h */
    public final void mo16395h(ItemLocation aag) {
        switch (bFf().mo6893i(gDe)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, gDe, new Object[]{aag}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, gDe, new Object[]{aag}));
                break;
        }
        m26430g(aag);
    }

    @C5566aOg
    /* renamed from: iW */
    public void mo16396iW(long j) {
        switch (bFf().mo6893i(gDr)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, gDr, new Object[]{new Long(j)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, gDr, new Object[]{new Long(j)}));
                break;
        }
        m26439iV(j);
    }

    public boolean isBound() {
        switch (bFf().mo6893i(gDm)) {
            case 0:
                return false;
            case 2:
                return ((Boolean) bFf().mo5606d(new aCE(this, gDm, new Object[0]))).booleanValue();
            case 3:
                bFf().mo5606d(new aCE(this, gDm, new Object[0]));
                break;
        }
        return cxo();
    }

    @C5566aOg
    public void setBound(boolean z) {
        switch (bFf().mo6893i(gDn)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, gDn, new Object[]{new Boolean(z)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, gDn, new Object[]{new Boolean(z)}));
                break;
        }
        m26438gO(z);
    }

    @C5566aOg
    @C2499fr
    /* renamed from: qV */
    public void mo1959qV() {
        switch (bFf().mo6893i(f5422Lm)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f5422Lm, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f5422Lm, new Object[0]));
                break;
        }
        m26441qU();
    }

    public final String toString() {
        switch (bFf().mo6893i(_f_toString_0020_0028_0029Ljava_002flang_002fString_003b)) {
            case 0:
                return null;
            case 2:
                return (String) bFf().mo5606d(new aCE(this, _f_toString_0020_0028_0029Ljava_002flang_002fString_003b, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, _f_toString_0020_0028_0029Ljava_002flang_002fString_003b, new Object[0]));
                break;
        }
        return m26412au();
    }

    @C0064Am(aul = "fb1c8e68fc86943d31fb56a8495c459e", aum = 0)
    /* renamed from: au */
    private String m26412au() {
        return "[" + bAP().getHandle() + "], " + cWm() + ", (amnt: " + mo302Iq() + "), (vol: " + getVolume() + "), (loc: " + bNh() + ")";
    }

    /* renamed from: n */
    public void mo306n(ItemType jCVar) {
        super.mo967a((C2961mJ) jCVar);
        ItemDurability azm = (ItemDurability) bFf().mo6865M(ItemDurability.class);
        azm.mo10S();
        m26410a(azm);
        m26407J(true);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "ItemType")
    @C0064Am(aul = "ed0d1ae4d8ad41e920c0c920f982b21f", aum = 0)
    private ItemType bAO() {
        return (ItemType) super.getType();
    }

    @C0064Am(aul = "e5642fe3ffc2b090d751bea5ed63fc91", aum = 0)
    private boolean aNM() {
        return true;
    }

    @C0064Am(aul = "4bb37d5683d04a8bb097f40ec6c1b595", aum = 0)
    private boolean cwZ() {
        return true;
    }

    @C0064Am(aul = "0c7938b733de7cc697ff17e77778cd69", aum = 0)
    private boolean aNO() {
        if (cxn() || isBound() || cxh()) {
            return false;
        }
        return true;
    }

    @C0064Am(aul = "c594abd537e707fe3bf9933ddc1c226b", aum = 0)
    private boolean cxb() {
        if (isBound()) {
            return false;
        }
        return true;
    }

    @C0064Am(aul = "906d6166a9b9b71ebf03761ea37a6c3f", aum = 0)
    private ItemLocation cxd() {
        return cwX();
    }

    @C0064Am(aul = "1787e651c1de11529b8c0650c9de21c3", aum = 0)
    private boolean cxg() {
        return cwU();
    }

    @C0064Am(aul = "2f90d31e7cfab2c4a2561d3e83aa2f9f", aum = 0)
    private Item cxk() {
        return this;
    }

    @C0064Am(aul = "a83e21ee5d79eab7a52f21ef9d576b61", aum = 0)
    /* renamed from: bV */
    private boolean m26418bV(Player aku) {
        return bAP().mo22858m(aku);
    }

    @C0064Am(aul = "92988420d72da5784a1a2b5a9bc92e86", aum = 0)
    private boolean cxm() {
        return cwV();
    }

    @C0064Am(aul = "fd7a5f2a53cccd7ee70b37a153589772", aum = 0)
    private boolean cxo() {
        return cwW();
    }

    @C0064Am(aul = "7b31865bfbd29672e03a148dbcb29d42", aum = 0)
    private boolean cxp() {
        return cwY().cGZ();
    }

    @C0064Am(aul = "81343063cbe53f3d768b433bd11a61dd", aum = 0)
    private long cxr() {
        return cwY().getRemainingTime();
    }

    @C0064Am(aul = "44ff143d6bce09b808ba0e78ea787715", aum = 0)
    private long cxt() {
        return cwY().getTotalTime();
    }
}
