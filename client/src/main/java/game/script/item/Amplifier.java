package game.script.item;

import game.network.message.externalizable.aCE;
import game.script.Actor;
import game.script.nls.NLSManager;
import game.script.nls.NLSWindowAlert;
import game.script.player.Player;
import game.script.ship.Ship;
import logic.aaa.C0284Df;
import logic.aaa.C1506WA;
import logic.aaa.C1654YM;
import logic.baa.*;
import logic.data.link.C3396rD;
import logic.data.link.C3981xi;
import logic.data.mbean.C3108nv;
import logic.res.code.C5640aRc;
import logic.res.code.C5663aRz;
import logic.res.code.C6494any;
import logic.res.html.C2491fm;
import logic.thred.LogPrinter;
import org.apache.commons.logging.Log;
import p001a.*;

import java.util.Collection;

@C5829abJ("2.0.0")
@C6485anp
@C2712iu(mo19786Bt = AmplifierBaseType.class, version = "2.0.1")
@C5511aMd
/* renamed from: a.zX */
/* compiled from: a */
public abstract class Amplifier extends Component implements C1616Xf, aOW, C3396rD.C3397a, C3396rD.C3398b, C3981xi.C3983b, C3981xi.C3984c {

    /* renamed from: CB */
    public static final C2491fm f9694CB = null;

    /* renamed from: Do */
    public static final C2491fm f9695Do = null;

    /* renamed from: KF */
    public static final C5663aRz f9696KF = null;

    /* renamed from: Lc */
    public static final C2491fm f9697Lc = null;

    /* renamed from: Lm */
    public static final C2491fm f9698Lm = null;
    public static final C2491fm _f_dispose_0020_0028_0029V = null;
    /* renamed from: _f_getShip_0020_0028_0029Ltaikodom_002fgame_002fscript_002fship_002fShip_003b */
    public static final C2491fm f9699x851d656b = null;
    public static final C2491fm _f_onResurrect_0020_0028_0029V = null;
    public static final int _m_fieldCount = 0;
    public static final C5663aRz[] _m_fields = null;
    public static final int _m_methodCount = 0;
    public static final C2491fm[] _m_methods = null;
    public static final C2491fm atW = null;
    public static final C2491fm atX = null;
    public static final C2491fm bEv = null;
    public static final C2491fm bEw = null;
    public static final C2491fm bwB = null;
    public static final C5663aRz ccJ = null;
    public static final C5663aRz ccK = null;
    public static final C2491fm ccL = null;
    public static final C2491fm ccM = null;
    public static final C2491fm ccN = null;
    public static final C2491fm ccO = null;
    public static final C2491fm ccP = null;
    public static final C2491fm ccQ = null;
    public static final C2491fm ccR = null;
    public static final C2491fm ccS = null;
    public static final C2491fm ccT = null;
    public static final C2491fm ccU = null;
    public static final C2491fm ccV = null;
    public static final long serialVersionUID = 0;
    private static final Log log = LogPrinter.setClass(Amplifier.class);
    public static C6494any ___iScriptClass;
    @C0064Am(aul = "dde47cd6bdcaebc69048ca274e534a9f", aum = 0)
    @C5566aOg
    private static Ship aIq;
    @C0064Am(aul = "fbf45096f5852ecb0e4b899ee20ddf9a", aum = 1)
    private static boolean aIr;
    @C0064Am(aul = "296a0761c4df0d0066d7a4046c912f4b", aum = 2)
    private static boolean active;

    static {
        m41584V();
    }

    public Amplifier() {
        super((C5540aNg) null);
        super.mo10S();
    }

    public Amplifier(C5540aNg ang) {
        super(ang);
    }

    public Amplifier(AmplifierType iDVar) {
        super((C5540aNg) null);
        super._m_script_init(iDVar);
    }

    /* renamed from: V */
    static void m41584V() {
        serialVersionUID = (long) 1;
        _m_fieldCount = Component._m_fieldCount + 3;
        _m_methodCount = Component._m_methodCount + 23;
        int i = Component._m_fieldCount;
        C5663aRz[] arzArr = new C5663aRz[(i + 3)];
        C5663aRz b = C5640aRc.m17844b(Amplifier.class, "dde47cd6bdcaebc69048ca274e534a9f", i);
        ccJ = b;
        arzArr[i] = b;
        int i2 = i + 1;
        C5663aRz b2 = C5640aRc.m17844b(Amplifier.class, "fbf45096f5852ecb0e4b899ee20ddf9a", i2);
        ccK = b2;
        arzArr[i2] = b2;
        int i3 = i2 + 1;
        C5663aRz b3 = C5640aRc.m17844b(Amplifier.class, "296a0761c4df0d0066d7a4046c912f4b", i3);
        f9696KF = b3;
        arzArr[i3] = b3;
        int i4 = i3 + 1;
        _m_fields = arzArr;
        C1634Xv.m11725a((Object[]) Component._m_fields, (Object[]) _m_fields);
        int i5 = Component._m_methodCount;
        C2491fm[] fmVarArr = new C2491fm[(i5 + 23)];
        C2491fm a = C4105zY.m41624a(Amplifier.class, "9d7f4f4284fb3c706de9e48293f077a1", i5);
        f9694CB = a;
        fmVarArr[i5] = a;
        int i6 = i5 + 1;
        C2491fm a2 = C4105zY.m41624a(Amplifier.class, "4c7abd18265dc2f516f240eb58e9bf8b", i6);
        bwB = a2;
        fmVarArr[i6] = a2;
        int i7 = i6 + 1;
        C2491fm a3 = C4105zY.m41624a(Amplifier.class, "df2b0b1215f33cc1b770c5f41bbf4f36", i7);
        ccL = a3;
        fmVarArr[i7] = a3;
        int i8 = i7 + 1;
        C2491fm a4 = C4105zY.m41624a(Amplifier.class, "df6d079909db3d4b0086bac8478ecc5c", i8);
        ccM = a4;
        fmVarArr[i8] = a4;
        int i9 = i8 + 1;
        C2491fm a5 = C4105zY.m41624a(Amplifier.class, "90afe035abb784d287718086f07c6a46", i9);
        bEv = a5;
        fmVarArr[i9] = a5;
        int i10 = i9 + 1;
        C2491fm a6 = C4105zY.m41624a(Amplifier.class, "0671dd5f73b59c572819ac4f31d21c69", i10);
        bEw = a6;
        fmVarArr[i10] = a6;
        int i11 = i10 + 1;
        C2491fm a7 = C4105zY.m41624a(Amplifier.class, "be6743d78e4d0fe19e66af0bd2ee04d8", i11);
        atX = a7;
        fmVarArr[i11] = a7;
        int i12 = i11 + 1;
        C2491fm a8 = C4105zY.m41624a(Amplifier.class, "e11a78ad1d173545e0339dda3ada131f", i12);
        atW = a8;
        fmVarArr[i12] = a8;
        int i13 = i12 + 1;
        C2491fm a9 = C4105zY.m41624a(Amplifier.class, "2aa77d2fc5fdccf6b7ba5ad08d2b960d", i13);
        ccN = a9;
        fmVarArr[i13] = a9;
        int i14 = i13 + 1;
        C2491fm a10 = C4105zY.m41624a(Amplifier.class, "3b1f3cf5e8161a80ec30750ff263f70f", i14);
        ccO = a10;
        fmVarArr[i14] = a10;
        int i15 = i14 + 1;
        C2491fm a11 = C4105zY.m41624a(Amplifier.class, "9577347b95ce7b8228435611a18c71aa", i15);
        _f_dispose_0020_0028_0029V = a11;
        fmVarArr[i15] = a11;
        int i16 = i15 + 1;
        C2491fm a12 = C4105zY.m41624a(Amplifier.class, "fde5117bd13084cda12625a70a51712f", i16);
        _f_onResurrect_0020_0028_0029V = a12;
        fmVarArr[i16] = a12;
        int i17 = i16 + 1;
        C2491fm a13 = C4105zY.m41624a(Amplifier.class, "0825069bf46d61146f5c735524daa705", i17);
        ccP = a13;
        fmVarArr[i17] = a13;
        int i18 = i17 + 1;
        C2491fm a14 = C4105zY.m41624a(Amplifier.class, "75455f7f0668fb563eed73b97b58d6f5", i18);
        ccQ = a14;
        fmVarArr[i18] = a14;
        int i19 = i18 + 1;
        C2491fm a15 = C4105zY.m41624a(Amplifier.class, "f7000186f1ffefcb655be2804a79073e", i19);
        f9699x851d656b = a15;
        fmVarArr[i19] = a15;
        int i20 = i19 + 1;
        C2491fm a16 = C4105zY.m41624a(Amplifier.class, "b5e33ba470388a42355309cfeff377bd", i20);
        f9695Do = a16;
        fmVarArr[i20] = a16;
        int i21 = i20 + 1;
        C2491fm a17 = C4105zY.m41624a(Amplifier.class, "06cd74aa4cbf2fdddf217c0238e3c541", i21);
        ccR = a17;
        fmVarArr[i21] = a17;
        int i22 = i21 + 1;
        C2491fm a18 = C4105zY.m41624a(Amplifier.class, "3261ec6a5d05e949ffe52c2823a3f775", i22);
        ccS = a18;
        fmVarArr[i22] = a18;
        int i23 = i22 + 1;
        C2491fm a19 = C4105zY.m41624a(Amplifier.class, "84af38403bb600c375200e7c20790b8c", i23);
        ccT = a19;
        fmVarArr[i23] = a19;
        int i24 = i23 + 1;
        C2491fm a20 = C4105zY.m41624a(Amplifier.class, "e39d36dadd4c1242a99f9b561ce7afd8", i24);
        f9698Lm = a20;
        fmVarArr[i24] = a20;
        int i25 = i24 + 1;
        C2491fm a21 = C4105zY.m41624a(Amplifier.class, "876ae95477b4582ace04cfde63e16b4e", i25);
        f9697Lc = a21;
        fmVarArr[i25] = a21;
        int i26 = i25 + 1;
        C2491fm a22 = C4105zY.m41624a(Amplifier.class, "fdaf3460674f48840f9f570c509baac6", i26);
        ccU = a22;
        fmVarArr[i26] = a22;
        int i27 = i26 + 1;
        C2491fm a23 = C4105zY.m41624a(Amplifier.class, "03e961755bb23e7ef684825c51ddc370", i27);
        ccV = a23;
        fmVarArr[i27] = a23;
        int i28 = i27 + 1;
        _m_methods = fmVarArr;
        C1634Xv.m11725a((Object[]) Component._m_methods, (Object[]) _m_methods);
        ___iScriptClass = aUO.m18566a(Amplifier.class, C3108nv.class, _m_fields, _m_methods);
    }

    /* renamed from: F */
    private void m41580F(boolean z) {
        bFf().mo5608dq().mo3153a(f9696KF, z);
    }

    @C0064Am(aul = "e11a78ad1d173545e0339dda3ada131f", aum = 0)
    @C5566aOg
    /* renamed from: Kr */
    private void m41581Kr() {
        throw new aWi(new aCE(this, atW, new Object[0]));
    }

    @C0064Am(aul = "be6743d78e4d0fe19e66af0bd2ee04d8", aum = 0)
    @C5566aOg
    /* renamed from: Kt */
    private void m41582Kt() {
        throw new aWi(new aCE(this, atX, new Object[0]));
    }

    private Ship asP() {
        return (Ship) bFf().mo5608dq().mo3214p(ccJ);
    }

    private boolean asQ() {
        return bFf().mo5608dq().mo3201h(ccK);
    }

    @C0064Am(aul = "df2b0b1215f33cc1b770c5f41bbf4f36", aum = 0)
    @C5566aOg
    private void asR() {
        throw new aWi(new aCE(this, ccL, new Object[0]));
    }

    @C0064Am(aul = "03e961755bb23e7ef684825c51ddc370", aum = 0)
    @C5566aOg
    private void atb() {
        throw new aWi(new aCE(this, ccV, new Object[0]));
    }

    /* renamed from: bL */
    private void m41589bL(boolean z) {
        bFf().mo5608dq().mo3153a(ccK, z);
    }

    @C0064Am(aul = "df6d079909db3d4b0086bac8478ecc5c", aum = 0)
    @C5566aOg
    /* renamed from: c */
    private void m41591c(ItemLocation aag) {
        throw new aWi(new aCE(this, ccM, new Object[]{aag}));
    }

    @C0064Am(aul = "9577347b95ce7b8228435611a18c71aa", aum = 0)
    @C5566aOg
    @C2499fr
    /* renamed from: fg */
    private void m41593fg() {
        throw new aWi(new aCE(this, _f_dispose_0020_0028_0029V, new Object[0]));
    }

    @C0064Am(aul = "2aa77d2fc5fdccf6b7ba5ad08d2b960d", aum = 0)
    @C5566aOg
    /* renamed from: m */
    private void m41595m(Component abl) {
        throw new aWi(new aCE(this, ccN, new Object[]{abl}));
    }

    @C0064Am(aul = "3b1f3cf5e8161a80ec30750ff263f70f", aum = 0)
    @C5566aOg
    /* renamed from: o */
    private void m41596o(Component abl) {
        throw new aWi(new aCE(this, ccO, new Object[]{abl}));
    }

    @C0064Am(aul = "e39d36dadd4c1242a99f9b561ce7afd8", aum = 0)
    @C5566aOg
    @C2499fr
    /* renamed from: qU */
    private void m41598qU() {
        throw new aWi(new aCE(this, f9698Lm, new Object[0]));
    }

    /* renamed from: qv */
    private boolean m41599qv() {
        return bFf().mo5608dq().mo3201h(f9696KF);
    }

    /* renamed from: z */
    private void m41600z(Ship fAVar) {
        bFf().mo5608dq().mo3197f(ccJ, fAVar);
    }

    /* renamed from: B */
    public void mo23333B(Ship fAVar) {
        switch (bFf().mo6893i(ccQ)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, ccQ, new Object[]{fAVar}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, ccQ, new Object[]{fAVar}));
                break;
        }
        m41579A(fAVar);
    }

    @C5566aOg
    /* renamed from: Ks */
    public void mo1952Ks() {
        switch (bFf().mo6893i(atW)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, atW, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, atW, new Object[0]));
                break;
        }
        m41581Kr();
    }

    @C5566aOg
    /* renamed from: Ku */
    public void mo1953Ku() {
        switch (bFf().mo6893i(atX)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, atX, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, atX, new Object[0]));
                break;
        }
        m41582Kt();
    }

    /* renamed from: S */
    public void mo10S() {
        super.mo10S();
    }

    /* renamed from: T */
    public C6494any mo11T() {
        return ___iScriptClass;
    }

    /* renamed from: U */
    public C2491fm[] mo12U() {
        return _m_methods;
    }

    /* renamed from: W */
    public Object mo13W() {
        return new C3108nv(this);
    }

    /* renamed from: a */
    public Object mo14a(C0495Gr gr) {
        Object[] args = gr.getArgs();
        switch (gr.mo2417hq() - Component._m_methodCount) {
            case 0:
                m41594l((Actor) args[0]);
                return null;
            case 1:
                m41588ag((Actor) args[0]);
                return null;
            case 2:
                asR();
                return null;
            case 3:
                m41591c((ItemLocation) args[0]);
                return null;
            case 4:
                m41586a((Ship) args[0], (Component) args[1]);
                return null;
            case 5:
                m41592c((Ship) args[0], (Component) args[1]);
                return null;
            case 6:
                m41582Kt();
                return null;
            case 7:
                m41581Kr();
                return null;
            case 8:
                m41595m((Component) args[0]);
                return null;
            case 9:
                m41596o((Component) args[0]);
                return null;
            case 10:
                m41593fg();
                return null;
            case 11:
                m41587aG();
                return null;
            case 12:
                return asT();
            case 13:
                m41579A((Ship) args[0]);
                return null;
            case 14:
                return m41583Mo();
            case 15:
                m41585a((C0665JT) args[0]);
                return null;
            case 16:
                asV();
                return null;
            case 17:
                return new Boolean(asX());
            case 18:
                return new Boolean(asZ());
            case 19:
                m41598qU();
                return null;
            case 20:
                return new Boolean(m41597qP());
            case 21:
                m41590bM(((Boolean) args[0]).booleanValue());
                return null;
            case 22:
                atb();
                return null;
            default:
                return super.mo14a(gr);
        }
    }

    /* renamed from: a */
    public void mo15a(Collection collection, C0495Gr gr) {
        super.mo15a(collection, gr);
    }

    /* renamed from: aH */
    public void mo70aH() {
        switch (bFf().mo6893i(_f_onResurrect_0020_0028_0029V)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, _f_onResurrect_0020_0028_0029V, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, _f_onResurrect_0020_0028_0029V, new Object[0]));
                break;
        }
        m41587aG();
    }

    /* renamed from: ah */
    public void mo21556ah(Actor cr) {
        switch (bFf().mo6893i(bwB)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, bwB, new Object[]{cr}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, bwB, new Object[]{cr}));
                break;
        }
        m41588ag(cr);
    }

    /* renamed from: al */
    public Ship mo7855al() {
        switch (bFf().mo6893i(f9699x851d656b)) {
            case 0:
                return null;
            case 2:
                return (Ship) bFf().mo5606d(new aCE(this, f9699x851d656b, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f9699x851d656b, new Object[0]));
                break;
        }
        return m41583Mo();
    }

    @C5566aOg
    public void asS() {
        switch (bFf().mo6893i(ccL)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, ccL, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, ccL, new Object[0]));
                break;
        }
        asR();
    }

    public Ship asU() {
        switch (bFf().mo6893i(ccP)) {
            case 0:
                return null;
            case 2:
                return (Ship) bFf().mo5606d(new aCE(this, ccP, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, ccP, new Object[0]));
                break;
        }
        return asT();
    }

    /* access modifiers changed from: protected */
    public void asW() {
        switch (bFf().mo6893i(ccR)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, ccR, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, ccR, new Object[0]));
                break;
        }
        asV();
    }

    public boolean asY() {
        switch (bFf().mo6893i(ccS)) {
            case 0:
                return false;
            case 2:
                return ((Boolean) bFf().mo5606d(new aCE(this, ccS, new Object[0]))).booleanValue();
            case 3:
                bFf().mo5606d(new aCE(this, ccS, new Object[0]));
                break;
        }
        return asX();
    }

    public boolean ata() {
        switch (bFf().mo6893i(ccT)) {
            case 0:
                return false;
            case 2:
                return ((Boolean) bFf().mo5606d(new aCE(this, ccT, new Object[0]))).booleanValue();
            case 3:
                bFf().mo5606d(new aCE(this, ccT, new Object[0]));
                break;
        }
        return asZ();
    }

    @C5566aOg
    public void atc() {
        switch (bFf().mo6893i(ccV)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, ccV, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, ccV, new Object[0]));
                break;
        }
        atb();
    }

    /* renamed from: b */
    public void mo24b(C0665JT jt) {
        switch (bFf().mo6893i(f9695Do)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f9695Do, new Object[]{jt}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f9695Do, new Object[]{jt}));
                break;
        }
        m41585a(jt);
    }

    /* renamed from: b */
    public void mo3926b(Ship fAVar, Component abl) {
        switch (bFf().mo6893i(bEv)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, bEv, new Object[]{fAVar, abl}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, bEv, new Object[]{fAVar, abl}));
                break;
        }
        m41586a(fAVar, abl);
    }

    /* renamed from: bN */
    public void mo7856bN(boolean z) {
        switch (bFf().mo6893i(ccU)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, ccU, new Object[]{new Boolean(z)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, ccU, new Object[]{new Boolean(z)}));
                break;
        }
        m41590bM(z);
    }

    /* renamed from: c */
    public C5663aRz[] mo25c() {
        return _m_fields;
    }

    @C5566aOg
    /* renamed from: d */
    public void mo12934d(ItemLocation aag) {
        switch (bFf().mo6893i(ccM)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, ccM, new Object[]{aag}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, ccM, new Object[]{aag}));
                break;
        }
        m41591c(aag);
    }

    /* renamed from: d */
    public void mo3933d(Ship fAVar, Component abl) {
        switch (bFf().mo6893i(bEw)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, bEw, new Object[]{fAVar, abl}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, bEw, new Object[]{fAVar, abl}));
                break;
        }
        m41592c(fAVar, abl);
    }

    @C5566aOg
    @C2499fr
    public void dispose() {
        switch (bFf().mo6893i(_f_dispose_0020_0028_0029V)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, _f_dispose_0020_0028_0029V, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, _f_dispose_0020_0028_0029V, new Object[0]));
                break;
        }
        m41593fg();
    }

    public boolean isActive() {
        switch (bFf().mo6893i(f9697Lc)) {
            case 0:
                return false;
            case 2:
                return ((Boolean) bFf().mo5606d(new aCE(this, f9697Lc, new Object[0]))).booleanValue();
            case 3:
                bFf().mo5606d(new aCE(this, f9697Lc, new Object[0]));
                break;
        }
        return m41597qP();
    }

    /* renamed from: m */
    public void mo18227m(Actor cr) {
        switch (bFf().mo6893i(f9694CB)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f9694CB, new Object[]{cr}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f9694CB, new Object[]{cr}));
                break;
        }
        m41594l(cr);
    }

    @C5566aOg
    /* renamed from: n */
    public void mo1957n(Component abl) {
        switch (bFf().mo6893i(ccN)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, ccN, new Object[]{abl}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, ccN, new Object[]{abl}));
                break;
        }
        m41595m(abl);
    }

    @C5566aOg
    /* renamed from: p */
    public void mo1958p(Component abl) {
        switch (bFf().mo6893i(ccO)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, ccO, new Object[]{abl}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, ccO, new Object[]{abl}));
                break;
        }
        m41596o(abl);
    }

    @C5566aOg
    @C2499fr
    /* renamed from: qV */
    public void mo1959qV() {
        switch (bFf().mo6893i(f9698Lm)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f9698Lm, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f9698Lm, new Object[0]));
                break;
        }
        m41598qU();
    }

    /* renamed from: e */
    public void mo1956e(AmplifierType iDVar) {
        super.mo7854a((ComponentType) iDVar);
    }

    @C0064Am(aul = "9d7f4f4284fb3c706de9e48293f077a1", aum = 0)
    /* renamed from: l */
    private void m41594l(Actor cr) {
    }

    @C0064Am(aul = "4c7abd18265dc2f516f240eb58e9bf8b", aum = 0)
    /* renamed from: ag */
    private void m41588ag(Actor cr) {
    }

    @C0064Am(aul = "90afe035abb784d287718086f07c6a46", aum = 0)
    /* renamed from: a */
    private void m41586a(Ship fAVar, Component abl) {
        log.trace("onComponentUnequiped");
        mo1958p(abl);
    }

    @C0064Am(aul = "0671dd5f73b59c572819ac4f31d21c69", aum = 0)
    /* renamed from: c */
    private void m41592c(Ship fAVar, Component abl) {
        log.trace("onComponentEquiped");
        mo1957n(abl);
    }

    @C0064Am(aul = "fde5117bd13084cda12625a70a51712f", aum = 0)
    /* renamed from: aG */
    private void m41587aG() {
        super.mo70aH();
        if (bNh() == null) {
        }
    }

    @C0064Am(aul = "0825069bf46d61146f5c735524daa705", aum = 0)
    private Ship asT() {
        return asP();
    }

    @C0064Am(aul = "75455f7f0668fb563eed73b97b58d6f5", aum = 0)
    /* renamed from: A */
    private void m41579A(Ship fAVar) {
        m41600z(fAVar);
    }

    @C0064Am(aul = "f7000186f1ffefcb655be2804a79073e", aum = 0)
    /* renamed from: Mo */
    private Ship m41583Mo() {
        if (super.mo7855al() != null) {
            return super.mo7855al();
        }
        return asP();
    }

    @C0064Am(aul = "b5e33ba470388a42355309cfeff377bd", aum = 0)
    /* renamed from: a */
    private void m41585a(C0665JT jt) {
    }

    @C0064Am(aul = "06cd74aa4cbf2fdddf217c0238e3c541", aum = 0)
    private void asV() {
        if (mo7855al().agj() instanceof Player) {
            ((Player) mo7855al().agj()).mo14419f((C1506WA) new C1654YM(this));
        }
    }

    @C0064Am(aul = "3261ec6a5d05e949ffe52c2823a3f775", aum = 0)
    private boolean asX() {
        return true;
    }

    @C0064Am(aul = "84af38403bb600c375200e7c20790b8c", aum = 0)
    private boolean asZ() {
        return asQ();
    }

    @C0064Am(aul = "876ae95477b4582ace04cfde63e16b4e", aum = 0)
    /* renamed from: qP */
    private boolean m41597qP() {
        return m41599qv();
    }

    @C0064Am(aul = "fdaf3460674f48840f9f570c509baac6", aum = 0)
    /* renamed from: bM */
    private void m41590bM(boolean z) {
        super.mo7856bN(z);
        if (!isUsable() && isActive()) {
            atc();
            ((Player) mo7855al().agj()).mo14419f((C1506WA) new C0284Df(((NLSWindowAlert) ala().aIY().mo6310c(NLSManager.C1472a.WINDOWALERT)).dcu(), false, cJS().mo19891ke().get()));
        }
    }
}
