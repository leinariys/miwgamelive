package game.script.item;

import game.network.message.externalizable.aCE;
import game.script.resource.Asset;
import game.script.ship.*;
import logic.baa.C1616Xf;
import logic.baa.C1634Xv;
import logic.baa.C5511aMd;
import logic.baa.C6485anp;
import logic.data.mbean.C0496Gs;
import logic.res.code.C5640aRc;
import logic.res.code.C5663aRz;
import logic.res.code.C6494any;
import logic.res.html.C2491fm;
import p001a.*;

import java.util.Collection;

@C2712iu(mo19786Bt = BaseComponentType.class, version = "2.0.2")
@C5511aMd
@C6485anp
/* renamed from: a.aBL */
/* compiled from: a */
public abstract class Component extends Item implements C1616Xf {

    /* renamed from: KP */
    public static final C5663aRz f2409KP = null;

    /* renamed from: Od */
    public static final C2491fm f2410Od = null;
    /* renamed from: _f_getShip_0020_0028_0029Ltaikodom_002fgame_002fscript_002fship_002fShip_003b */
    public static final C2491fm f2411x851d656b = null;
    public static final int _m_fieldCount = 0;
    public static final C5663aRz[] _m_fields = null;
    public static final int _m_methodCount = 0;
    public static final C2491fm[] _m_methods = null;
    public static final C2491fm aGt = null;
    public static final C2491fm bHG = null;
    public static final C2491fm ccU = null;
    public static final C2491fm esw = null;
    public static final C5663aRz hiu = null;
    public static final C5663aRz hiv = null;
    public static final C5663aRz hiw = null;
    public static final C2491fm hix = null;
    public static final C2491fm hiy = null;
    public static final long serialVersionUID = 0;
    public static C6494any ___iScriptClass;
    @C0064Am(aul = "7e2b131fb2f49d8628ea8eee0c93a2c2", aum = 3)
    private static boolean cSS;
    @C0064Am(aul = "df7f6689513493cd12c4756a811beaed", aum = 0)
    private static SectorCategory cYq;
    @C0064Am(aul = "cda8c1b1fb76edad05f3282bb5358bb2", aum = 1)
    private static Slot.C2893a cYr;
    @C0064Am(aul = "d0c4f39608e73be9c2c8441574401605", aum = 2)
    private static Asset cYs;

    static {
        m12851V();
    }

    public Component() {
        super((C5540aNg) null);
        super.mo10S();
    }

    public Component(ComponentType aef) {
        super((C5540aNg) null);
        super._m_script_init(aef);
    }

    public Component(C5540aNg ang) {
        super(ang);
    }

    /* renamed from: V */
    static void m12851V() {
        serialVersionUID = (long) 1;
        _m_fieldCount = Item._m_fieldCount + 4;
        _m_methodCount = Item._m_methodCount + 8;
        int i = Item._m_fieldCount;
        C5663aRz[] arzArr = new C5663aRz[(i + 4)];
        C5663aRz b = C5640aRc.m17844b(Component.class, "df7f6689513493cd12c4756a811beaed", i);
        f2409KP = b;
        arzArr[i] = b;
        int i2 = i + 1;
        C5663aRz b2 = C5640aRc.m17844b(Component.class, "cda8c1b1fb76edad05f3282bb5358bb2", i2);
        hiu = b2;
        arzArr[i2] = b2;
        int i3 = i2 + 1;
        C5663aRz b3 = C5640aRc.m17844b(Component.class, "d0c4f39608e73be9c2c8441574401605", i3);
        hiv = b3;
        arzArr[i3] = b3;
        int i4 = i3 + 1;
        C5663aRz b4 = C5640aRc.m17844b(Component.class, "7e2b131fb2f49d8628ea8eee0c93a2c2", i4);
        hiw = b4;
        arzArr[i4] = b4;
        int i5 = i4 + 1;
        _m_fields = arzArr;
        C1634Xv.m11725a((Object[]) Item._m_fields, (Object[]) _m_fields);
        int i6 = Item._m_methodCount;
        C2491fm[] fmVarArr = new C2491fm[(i6 + 8)];
        C2491fm a = C4105zY.m41624a(Component.class, "506ea4d01226ec16ca0c06afc5046347", i6);
        bHG = a;
        fmVarArr[i6] = a;
        int i7 = i6 + 1;
        C2491fm a2 = C4105zY.m41624a(Component.class, "e36ebb543d614b0e9bc5c5b91e2aaae1", i7);
        aGt = a2;
        fmVarArr[i7] = a2;
        int i8 = i7 + 1;
        C2491fm a3 = C4105zY.m41624a(Component.class, "854098708bb2a9c780f3b6f72b486f6b", i8);
        f2411x851d656b = a3;
        fmVarArr[i8] = a3;
        int i9 = i8 + 1;
        C2491fm a4 = C4105zY.m41624a(Component.class, "83d88ede7738c42df5a92fa611c40754", i9);
        hix = a4;
        fmVarArr[i9] = a4;
        int i10 = i9 + 1;
        C2491fm a5 = C4105zY.m41624a(Component.class, "03c64bbcf8d1cd9cf47a6821b5e7121d", i10);
        hiy = a5;
        fmVarArr[i10] = a5;
        int i11 = i10 + 1;
        C2491fm a6 = C4105zY.m41624a(Component.class, "2340f807d1aa71cd42940099fb91b2cd", i11);
        ccU = a6;
        fmVarArr[i11] = a6;
        int i12 = i11 + 1;
        C2491fm a7 = C4105zY.m41624a(Component.class, "2c2d9be869e37af8ffcc6bc4a99b03d1", i12);
        f2410Od = a7;
        fmVarArr[i12] = a7;
        int i13 = i12 + 1;
        C2491fm a8 = C4105zY.m41624a(Component.class, "33d776685b9f896c8bd5eaeccf455b52", i13);
        esw = a8;
        fmVarArr[i13] = a8;
        int i14 = i13 + 1;
        _m_methods = fmVarArr;
        C1634Xv.m11725a((Object[]) Item._m_methods, (Object[]) _m_methods);
        ___iScriptClass = aUO.m18566a(Component.class, C0496Gs.class, _m_fields, _m_methods);
    }

    /* renamed from: a */
    private void m12852a(Slot.C2893a aVar) {
        throw new C6039afL();
    }

    @C0064Am(aul = "33d776685b9f896c8bd5eaeccf455b52", aum = 0)
    private ItemType bAO() {
        return cJS();
    }

    @C0064Am(aul = "2340f807d1aa71cd42940099fb91b2cd", aum = 0)
    @C5566aOg
    /* renamed from: bM */
    private void m12853bM(boolean z) {
        throw new aWi(new aCE(this, ccU, new Object[]{new Boolean(z)}));
    }

    /* renamed from: bP */
    private void m12854bP(Asset tCVar) {
        throw new C6039afL();
    }

    private SectorCategory cJN() {
        return ((ComponentType) getType()).cXV();
    }

    private Slot.C2893a cJO() {
        return ((ComponentType) getType()).cuH();
    }

    private Asset cJP() {
        return ((ComponentType) getType()).cXX();
    }

    private boolean cJQ() {
        return bFf().mo5608dq().mo3201h(hiw);
    }

    /* renamed from: h */
    private void m12855h(SectorCategory fMVar) {
        throw new C6039afL();
    }

    /* renamed from: hS */
    private void m12856hS(boolean z) {
        bFf().mo5608dq().mo3153a(hiw, z);
    }

    /* renamed from: Iq */
    public final int mo302Iq() {
        switch (bFf().mo6893i(aGt)) {
            case 0:
                return 0;
            case 2:
                return ((Integer) bFf().mo5606d(new aCE(this, aGt, new Object[0]))).intValue();
            case 3:
                bFf().mo5606d(new aCE(this, aGt, new Object[0]));
                break;
        }
        return m12850PK();
    }

    /* renamed from: S */
    public void mo10S() {
        super.mo10S();
    }

    /* renamed from: T */
    public C6494any mo11T() {
        return ___iScriptClass;
    }

    /* renamed from: U */
    public C2491fm[] mo12U() {
        return _m_methods;
    }

    /* renamed from: W */
    public Object mo13W() {
        return new C0496Gs(this);
    }

    /* renamed from: a */
    public Object mo14a(C0495Gr gr) {
        Object[] args = gr.getArgs();
        switch (gr.mo2417hq() - Item._m_methodCount) {
            case 0:
                return new Float(aoH());
            case 1:
                return new Integer(m12850PK());
            case 2:
                return m12849Mo();
            case 3:
                return cJR();
            case 4:
                return new Boolean(cJT());
            case 5:
                m12853bM(((Boolean) args[0]).booleanValue());
                return null;
            case 6:
                return m12857sL();
            case 7:
                return bAO();
            default:
                return super.mo14a(gr);
        }
    }

    /* renamed from: a */
    public void mo15a(Collection collection, C0495Gr gr) {
        super.mo15a(collection, gr);
    }

    /* renamed from: al */
    public Ship mo7855al() {
        switch (bFf().mo6893i(f2411x851d656b)) {
            case 0:
                return null;
            case 2:
                return (Ship) bFf().mo5606d(new aCE(this, f2411x851d656b, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f2411x851d656b, new Object[0]));
                break;
        }
        return m12849Mo();
    }

    public /* bridge */ /* synthetic */ ItemType bAP() {
        switch (bFf().mo6893i(esw)) {
            case 0:
                return null;
            case 2:
                return (ItemType) bFf().mo5606d(new aCE(this, esw, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, esw, new Object[0]));
                break;
        }
        return bAO();
    }

    @C5566aOg
    /* renamed from: bN */
    public void mo7856bN(boolean z) {
        switch (bFf().mo6893i(ccU)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, ccU, new Object[]{new Boolean(z)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, ccU, new Object[]{new Boolean(z)}));
                break;
        }
        m12853bM(z);
    }

    /* renamed from: c */
    public C5663aRz[] mo25c() {
        return _m_fields;
    }

    public ComponentType cJS() {
        switch (bFf().mo6893i(hix)) {
            case 0:
                return null;
            case 2:
                return (ComponentType) bFf().mo5606d(new aCE(this, hix, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, hix, new Object[0]));
                break;
        }
        return cJR();
    }

    public final float getVolume() {
        switch (bFf().mo6893i(bHG)) {
            case 0:
                return 0.0f;
            case 2:
                return ((Float) bFf().mo5606d(new aCE(this, bHG, new Object[0]))).floatValue();
            case 3:
                bFf().mo5606d(new aCE(this, bHG, new Object[0]));
                break;
        }
        return aoH();
    }

    public boolean isUsable() {
        switch (bFf().mo6893i(hiy)) {
            case 0:
                return false;
            case 2:
                return ((Boolean) bFf().mo5606d(new aCE(this, hiy, new Object[0]))).booleanValue();
            case 3:
                bFf().mo5606d(new aCE(this, hiy, new Object[0]));
                break;
        }
        return cJT();
    }

    /* renamed from: sM */
    public C3904wY mo7860sM() {
        switch (bFf().mo6893i(f2410Od)) {
            case 0:
                return null;
            case 2:
                return (C3904wY) bFf().mo5606d(new aCE(this, f2410Od, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f2410Od, new Object[0]));
                break;
        }
        return m12857sL();
    }

    /* renamed from: a */
    public void mo7854a(ComponentType aef) {
        super.mo306n(aef);
        m12856hS(true);
    }

    @C0064Am(aul = "506ea4d01226ec16ca0c06afc5046347", aum = 0)
    private float aoH() {
        return cJS().mo19865HA();
    }

    @C0064Am(aul = "e36ebb543d614b0e9bc5c5b91e2aaae1", aum = 0)
    /* renamed from: PK */
    private int m12850PK() {
        return 1;
    }

    @C0064Am(aul = "854098708bb2a9c780f3b6f72b486f6b", aum = 0)
    /* renamed from: Mo */
    private Ship m12849Mo() {
        if (!(bNh() instanceof ShipSector)) {
            return null;
        }
        ShipStructure bmS = ((ShipSector) bNh()).bmS();
        if (bmS != null) {
            return bmS.mo13241al();
        }
        mo8360mc("ERROR: weapon.getShip(): struct is null!");
        return null;
    }

    @C0064Am(aul = "83d88ede7738c42df5a92fa611c40754", aum = 0)
    private ComponentType cJR() {
        return (ComponentType) super.bAP();
    }

    @C0064Am(aul = "03c64bbcf8d1cd9cf47a6821b5e7121d", aum = 0)
    private boolean cJT() {
        return cJQ();
    }

    @C0064Am(aul = "2c2d9be869e37af8ffcc6bc4a99b03d1", aum = 0)
    /* renamed from: sL */
    private C3904wY m12857sL() {
        if (cJS() == null || cJS().cXV() == null) {
            return null;
        }
        return cJS().cXV().mo18441sM();
    }
}
