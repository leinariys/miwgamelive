package game.script.item;

import game.network.message.externalizable.aCE;
import logic.baa.C1616Xf;
import logic.baa.C1634Xv;
import logic.baa.C5511aMd;
import logic.baa.C6485anp;
import logic.data.mbean.C3379qr;
import logic.res.code.C5640aRc;
import logic.res.code.C5663aRz;
import logic.res.code.C6494any;
import logic.res.html.C2491fm;
import p001a.*;

import java.util.Collection;

@C2712iu
@C5511aMd
@C6485anp
/* renamed from: a.ZK */
/* compiled from: a */
public class ClipBox extends Merchandise implements C1616Xf {
    public static final int _m_fieldCount = 0;
    public static final C5663aRz[] _m_fields = null;
    public static final int _m_methodCount = 0;
    public static final C2491fm[] _m_methods = null;
    public static final C5663aRz eQh = null;
    public static final C5663aRz eQi = null;
    public static final C2491fm eQj = null;
    public static final C2491fm eQk = null;
    public static final C2491fm esw = null;
    public static final long serialVersionUID = 0;
    public static C6494any ___iScriptClass;
    @C0064Am(aul = "ef2ccc2974662dd9b9aff35a595228f6", aum = 0)
    private static int aVH;
    @C0064Am(aul = "97a7567579281d60577e9afccc676f99", aum = 1)
    private static ClipType aVI;

    static {
        m12022V();
    }

    public ClipBox() {
        super((C5540aNg) null);
        super.mo10S();
    }

    public ClipBox(C5540aNg ang) {
        super(ang);
    }

    public ClipBox(ClipBoxType aty) {
        super((C5540aNg) null);
        super._m_script_init(aty);
    }

    /* renamed from: V */
    static void m12022V() {
        serialVersionUID = (long) 1;
        _m_fieldCount = Merchandise._m_fieldCount + 2;
        _m_methodCount = Merchandise._m_methodCount + 3;
        int i = Merchandise._m_fieldCount;
        C5663aRz[] arzArr = new C5663aRz[(i + 2)];
        C5663aRz b = C5640aRc.m17844b(ClipBox.class, "ef2ccc2974662dd9b9aff35a595228f6", i);
        eQh = b;
        arzArr[i] = b;
        int i2 = i + 1;
        C5663aRz b2 = C5640aRc.m17844b(ClipBox.class, "97a7567579281d60577e9afccc676f99", i2);
        eQi = b2;
        arzArr[i2] = b2;
        int i3 = i2 + 1;
        _m_fields = arzArr;
        C1634Xv.m11725a((Object[]) Merchandise._m_fields, (Object[]) _m_fields);
        int i4 = Merchandise._m_methodCount;
        C2491fm[] fmVarArr = new C2491fm[(i4 + 3)];
        C2491fm a = C4105zY.m41624a(ClipBox.class, "bb83f602d43c1446439ed63eb19d8e9e", i4);
        eQj = a;
        fmVarArr[i4] = a;
        int i5 = i4 + 1;
        C2491fm a2 = C4105zY.m41624a(ClipBox.class, "a4e6fa368230df59f7f0590082fd39af", i5);
        eQk = a2;
        fmVarArr[i5] = a2;
        int i6 = i5 + 1;
        C2491fm a3 = C4105zY.m41624a(ClipBox.class, "0424818009879292cafcc0277d56ebf7", i6);
        esw = a3;
        fmVarArr[i6] = a3;
        int i7 = i6 + 1;
        _m_methods = fmVarArr;
        C1634Xv.m11725a((Object[]) Merchandise._m_methods, (Object[]) _m_methods);
        ___iScriptClass = aUO.m18566a(ClipBox.class, C3379qr.class, _m_fields, _m_methods);
    }

    @C0064Am(aul = "0424818009879292cafcc0277d56ebf7", aum = 0)
    private ItemType bAO() {
        return bJg();
    }

    private int bJd() {
        return ((ClipBoxType) getType()).dvX();
    }

    private ClipType bJe() {
        return ((ClipBoxType) getType()).dvZ();
    }

    @C0064Am(aul = "a4e6fa368230df59f7f0590082fd39af", aum = 0)
    @C5566aOg
    @C2499fr
    private Clip[] bJh() {
        throw new aWi(new aCE(this, eQk, new Object[0]));
    }

    /* renamed from: m */
    private void m12023m(ClipType aqf) {
        throw new C6039afL();
    }

    /* renamed from: pp */
    private void m12024pp(int i) {
        throw new C6039afL();
    }

    /* renamed from: S */
    public void mo10S() {
        super.mo10S();
    }

    /* renamed from: T */
    public C6494any mo11T() {
        return ___iScriptClass;
    }

    /* renamed from: U */
    public C2491fm[] mo12U() {
        return _m_methods;
    }

    /* renamed from: W */
    public Object mo13W() {
        return new C3379qr(this);
    }

    /* renamed from: a */
    public Object mo14a(C0495Gr gr) {
        gr.getArgs();
        switch (gr.mo2417hq() - Merchandise._m_methodCount) {
            case 0:
                return bJf();
            case 1:
                return bJh();
            case 2:
                return bAO();
            default:
                return super.mo14a(gr);
        }
    }

    /* renamed from: a */
    public void mo15a(Collection collection, C0495Gr gr) {
        super.mo15a(collection, gr);
    }

    public /* bridge */ /* synthetic */ ItemType bAP() {
        switch (bFf().mo6893i(esw)) {
            case 0:
                return null;
            case 2:
                return (ItemType) bFf().mo5606d(new aCE(this, esw, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, esw, new Object[0]));
                break;
        }
        return bAO();
    }

    public ClipBoxType bJg() {
        switch (bFf().mo6893i(eQj)) {
            case 0:
                return null;
            case 2:
                return (ClipBoxType) bFf().mo5606d(new aCE(this, eQj, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, eQj, new Object[0]));
                break;
        }
        return bJf();
    }

    @C5566aOg
    @C2499fr
    public Clip[] bJi() {
        switch (bFf().mo6893i(eQk)) {
            case 0:
                return null;
            case 2:
                return (Clip[]) bFf().mo5606d(new aCE(this, eQk, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, eQk, new Object[0]));
                break;
        }
        return bJh();
    }

    /* renamed from: c */
    public C5663aRz[] mo25c() {
        return _m_fields;
    }

    /* renamed from: g */
    public void mo7378g(ClipBoxType aty) {
        super.mo11606e(aty);
    }

    @C0064Am(aul = "bb83f602d43c1446439ed63eb19d8e9e", aum = 0)
    private ClipBoxType bJf() {
        return (ClipBoxType) super.bAP();
    }
}
