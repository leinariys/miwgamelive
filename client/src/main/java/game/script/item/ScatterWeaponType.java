package game.script.item;

import game.network.message.externalizable.aCE;
import logic.baa.*;
import logic.data.mbean.C5750aVi;
import logic.res.code.C5640aRc;
import logic.res.code.C5663aRz;
import logic.res.code.C6494any;
import logic.res.html.C2491fm;
import p001a.*;

import java.util.Collection;

@C0566Hp
@C5829abJ("2.1.1")
@C6485anp
@C5511aMd
/* renamed from: a.aLZ */
/* compiled from: a */
public class ScatterWeaponType extends ProjectileWeaponType implements C1616Xf {
    public static final int _m_fieldCount = 0;
    public static final C5663aRz[] _m_fields = null;
    public static final int _m_methodCount = 0;
    public static final C2491fm[] _m_methods = null;
    /* renamed from: dN */
    public static final C2491fm f3319dN = null;
    public static final C5663aRz eJt = null;
    public static final C5663aRz eJu = null;
    public static final C2491fm ioe = null;
    public static final C2491fm iof = null;
    public static final C2491fm iog = null;
    public static final C2491fm ioh = null;
    public static final C2491fm ioi = null;
    public static final long serialVersionUID = 0;
    public static C6494any ___iScriptClass;
    @C0064Am(aul = "848e34500435626ca2b707b4576352e8", aum = 0)

    /* renamed from: XT */
    private static int f3317XT;
    @C0064Am(aul = "1b43e3cbdb8575585176abafe7e8591f", aum = 1)

    /* renamed from: XU */
    private static float f3318XU;

    static {
        m16190V();
    }

    public ScatterWeaponType() {
        super((C5540aNg) null);
        super.mo10S();
    }

    public ScatterWeaponType(C5540aNg ang) {
        super(ang);
    }

    /* renamed from: V */
    static void m16190V() {
        serialVersionUID = (long) 1;
        _m_fieldCount = ProjectileWeaponType._m_fieldCount + 2;
        _m_methodCount = ProjectileWeaponType._m_methodCount + 6;
        int i = ProjectileWeaponType._m_fieldCount;
        C5663aRz[] arzArr = new C5663aRz[(i + 2)];
        C5663aRz b = C5640aRc.m17844b(ScatterWeaponType.class, "848e34500435626ca2b707b4576352e8", i);
        eJt = b;
        arzArr[i] = b;
        int i2 = i + 1;
        C5663aRz b2 = C5640aRc.m17844b(ScatterWeaponType.class, "1b43e3cbdb8575585176abafe7e8591f", i2);
        eJu = b2;
        arzArr[i2] = b2;
        int i3 = i2 + 1;
        _m_fields = arzArr;
        C1634Xv.m11725a((Object[]) ProjectileWeaponType._m_fields, (Object[]) _m_fields);
        int i4 = ProjectileWeaponType._m_methodCount;
        C2491fm[] fmVarArr = new C2491fm[(i4 + 6)];
        C2491fm a = C4105zY.m41624a(ScatterWeaponType.class, "e356222bf86899ff71b4d7bd4d96fb8b", i4);
        ioe = a;
        fmVarArr[i4] = a;
        int i5 = i4 + 1;
        C2491fm a2 = C4105zY.m41624a(ScatterWeaponType.class, "384e0aa9007093b1b5a1232837c3e8f5", i5);
        iof = a2;
        fmVarArr[i5] = a2;
        int i6 = i5 + 1;
        C2491fm a3 = C4105zY.m41624a(ScatterWeaponType.class, "ee08886e4dc0d7da9cb785e94f0aeef0", i6);
        iog = a3;
        fmVarArr[i6] = a3;
        int i7 = i6 + 1;
        C2491fm a4 = C4105zY.m41624a(ScatterWeaponType.class, "a6e95ad899371bd633b5131275d6ec0d", i7);
        ioh = a4;
        fmVarArr[i7] = a4;
        int i8 = i7 + 1;
        C2491fm a5 = C4105zY.m41624a(ScatterWeaponType.class, "b8f9b53d136b9dd5aea9bdaefc85eef2", i8);
        ioi = a5;
        fmVarArr[i8] = a5;
        int i9 = i8 + 1;
        C2491fm a6 = C4105zY.m41624a(ScatterWeaponType.class, "499051453b5d95d089a99c5cbeb06555", i9);
        f3319dN = a6;
        fmVarArr[i9] = a6;
        int i10 = i9 + 1;
        _m_methods = fmVarArr;
        C1634Xv.m11725a((Object[]) ProjectileWeaponType._m_methods, (Object[]) _m_methods);
        ___iScriptClass = aUO.m18566a(ScatterWeaponType.class, C5750aVi.class, _m_fields, _m_methods);
    }

    private int bGa() {
        return bFf().mo5608dq().mo3212n(eJt);
    }

    private float bGb() {
        return bFf().mo5608dq().mo3211m(eJu);
    }

    /* renamed from: iJ */
    private void m16192iJ(float f) {
        bFf().mo5608dq().mo3150a(eJu, f);
    }

    /* renamed from: pg */
    private void m16194pg(int i) {
        bFf().mo5608dq().mo3183b(eJt, i);
    }

    /* renamed from: T */
    public C6494any mo11T() {
        return ___iScriptClass;
    }

    /* renamed from: U */
    public C2491fm[] mo12U() {
        return _m_methods;
    }

    /* renamed from: W */
    public Object mo13W() {
        return new C5750aVi(this);
    }

    /* renamed from: a */
    public Object mo14a(C0495Gr gr) {
        Object[] args = gr.getArgs();
        switch (gr.mo2417hq() - ProjectileWeaponType._m_methodCount) {
            case 0:
                return new Integer(diI());
            case 1:
                m16195yF(((Integer) args[0]).intValue());
                return null;
            case 2:
                return new Float(diK());
            case 3:
                m16193mZ(((Float) args[0]).floatValue());
                return null;
            case 4:
                return diM();
            case 5:
                return m16191aT();
            default:
                return super.mo14a(gr);
        }
    }

    /* renamed from: a */
    public void mo15a(Collection collection, C0495Gr gr) {
        super.mo15a(collection, gr);
    }

    /* access modifiers changed from: protected */
    /* renamed from: aU */
    public <T extends aDJ> T mo745aU() {
        switch (bFf().mo6893i(f3319dN)) {
            case 0:
                return null;
            case 2:
                return (aDJ) bFf().mo5606d(new aCE(this, f3319dN, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f3319dN, new Object[0]));
                break;
        }
        return m16191aT();
    }

    /* renamed from: c */
    public C5663aRz[] mo25c() {
        return _m_fields;
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Num Projectiles Per Shot")
    public int diJ() {
        switch (bFf().mo6893i(ioe)) {
            case 0:
                return 0;
            case 2:
                return ((Integer) bFf().mo5606d(new aCE(this, ioe, new Object[0]))).intValue();
            case 3:
                bFf().mo5606d(new aCE(this, ioe, new Object[0]));
                break;
        }
        return diI();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Spread Cone Angle")
    public float diL() {
        switch (bFf().mo6893i(iog)) {
            case 0:
                return 0.0f;
            case 2:
                return ((Float) bFf().mo5606d(new aCE(this, iog, new Object[0]))).floatValue();
            case 3:
                bFf().mo5606d(new aCE(this, iog, new Object[0]));
                break;
        }
        return diK();
    }

    public ScatterWeapon diN() {
        switch (bFf().mo6893i(ioi)) {
            case 0:
                return null;
            case 2:
                return (ScatterWeapon) bFf().mo5606d(new aCE(this, ioi, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, ioi, new Object[0]));
                break;
        }
        return diM();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Spread Cone Angle")
    /* renamed from: na */
    public void mo9906na(float f) {
        switch (bFf().mo6893i(ioh)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, ioh, new Object[]{new Float(f)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, ioh, new Object[]{new Float(f)}));
                break;
        }
        m16193mZ(f);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Num Projectiles Per Shot")
    /* renamed from: yG */
    public void mo9907yG(int i) {
        switch (bFf().mo6893i(iof)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, iof, new Object[]{new Integer(i)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, iof, new Object[]{new Integer(i)}));
                break;
        }
        m16195yF(i);
    }

    /* renamed from: S */
    public void mo10S() {
        super.mo10S();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Num Projectiles Per Shot")
    @C0064Am(aul = "e356222bf86899ff71b4d7bd4d96fb8b", aum = 0)
    private int diI() {
        return bGa();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Num Projectiles Per Shot")
    @C0064Am(aul = "384e0aa9007093b1b5a1232837c3e8f5", aum = 0)
    /* renamed from: yF */
    private void m16195yF(int i) {
        m16194pg(i);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Spread Cone Angle")
    @C0064Am(aul = "ee08886e4dc0d7da9cb785e94f0aeef0", aum = 0)
    private float diK() {
        return bGb();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Spread Cone Angle")
    @C0064Am(aul = "a6e95ad899371bd633b5131275d6ec0d", aum = 0)
    /* renamed from: mZ */
    private void m16193mZ(float f) {
        m16192iJ(f);
    }

    @C0064Am(aul = "b8f9b53d136b9dd5aea9bdaefc85eef2", aum = 0)
    private ScatterWeapon diM() {
        return (ScatterWeapon) mo745aU();
    }

    @C0064Am(aul = "499051453b5d95d089a99c5cbeb06555", aum = 0)
    /* renamed from: aT */
    private <T extends aDJ> T m16191aT() {
        T t = (ScatterWeapon) bFf().mo6865M(ScatterWeapon.class);
        t.mo6693a(this);
        return t;
    }
}
