package game.script.item;

import game.network.message.externalizable.aCE;
import game.network.message.serializable.C1556Wo;
import game.script.damage.DamageType;
import game.script.resource.Asset;
import logic.baa.*;
import logic.data.mbean.C5293aDt;
import logic.res.code.C5640aRc;
import logic.res.code.C5663aRz;
import logic.res.code.C6494any;
import logic.res.html.C2491fm;
import p001a.*;

import java.util.Collection;
import java.util.Map;

@C0566Hp
@C5829abJ("2.1.1")
@C6485anp
@C5511aMd
/* renamed from: a.aQf  reason: case insensitive filesystem */
/* compiled from: a */
public class ClipType extends BaseClipType implements C1616Xf, C6251ajP {
    /* renamed from: LR */
    public static final C5663aRz f3642LR = null;
    public static final int _m_fieldCount = 0;
    public static final C5663aRz[] _m_fields = null;
    public static final int _m_methodCount = 0;
    public static final C2491fm[] _m_methods = null;
    public static final C2491fm aDk = null;
    public static final C2491fm aDl = null;
    /* renamed from: dN */
    public static final C2491fm f3643dN = null;
    public static final C2491fm dVN = null;
    public static final C2491fm eNq = null;
    public static final C5663aRz efi = null;
    public static final C2491fm eft = null;
    public static final C2491fm hPb = null;
    public static final C2491fm iDi = null;
    public static final long serialVersionUID = 0;
    /* renamed from: uK */
    public static final C5663aRz f3644uK = null;
    /* renamed from: uU */
    public static final C5663aRz f3646uU = null;
    /* renamed from: vi */
    public static final C2491fm f3647vi = null;
    /* renamed from: vj */
    public static final C2491fm f3648vj = null;
    public static C6494any ___iScriptClass;
    @C0064Am(aul = "79fbdc1d68184f34c73bfb5f020f3e71", aum = 0)

    /* renamed from: LQ */
    private static Asset f3641LQ;
    @C0064Am(aul = "6e3457b04d8e2d00b5704e251d4a224f", aum = 2)
    private static int atB;
    @C0064Am(aul = "52710271c155382bf100772222ebb571", aum = 1)
    private static C1556Wo<DamageType, Float> bIv;
    @C0064Am(aul = "25d4dbe5604c50f621728045b24720e1", aum = 3)

    /* renamed from: uT */
    private static String f3645uT;

    static {
        m17585V();
    }

    public ClipType() {
        super((C5540aNg) null);
        super.mo10S();
    }

    public ClipType(C5540aNg ang) {
        super(ang);
    }

    /* renamed from: V */
    static void m17585V() {
        serialVersionUID = (long) 1;
        _m_fieldCount = BaseClipType._m_fieldCount + 4;
        _m_methodCount = BaseClipType._m_methodCount + 10;
        int i = BaseClipType._m_fieldCount;
        C5663aRz[] arzArr = new C5663aRz[(i + 4)];
        C5663aRz b = C5640aRc.m17844b(ClipType.class, "79fbdc1d68184f34c73bfb5f020f3e71", i);
        f3642LR = b;
        arzArr[i] = b;
        int i2 = i + 1;
        C5663aRz b2 = C5640aRc.m17844b(ClipType.class, "52710271c155382bf100772222ebb571", i2);
        f3644uK = b2;
        arzArr[i2] = b2;
        int i3 = i2 + 1;
        C5663aRz b3 = C5640aRc.m17844b(ClipType.class, "6e3457b04d8e2d00b5704e251d4a224f", i3);
        efi = b3;
        arzArr[i3] = b3;
        int i4 = i3 + 1;
        C5663aRz b4 = C5640aRc.m17844b(ClipType.class, "25d4dbe5604c50f621728045b24720e1", i4);
        f3646uU = b4;
        arzArr[i4] = b4;
        int i5 = i4 + 1;
        _m_fields = arzArr;
        C1634Xv.m11725a((Object[]) BaseClipType._m_fields, (Object[]) _m_fields);
        int i6 = BaseClipType._m_methodCount;
        C2491fm[] fmVarArr = new C2491fm[(i6 + 10)];
        C2491fm a = C4105zY.m41624a(ClipType.class, "26f0b9c5d8cef367a6a5f066fa643614", i6);
        aDk = a;
        fmVarArr[i6] = a;
        int i7 = i6 + 1;
        C2491fm a2 = C4105zY.m41624a(ClipType.class, "7ebe707229da70bb6c3e6df30283142a", i7);
        aDl = a2;
        fmVarArr[i7] = a2;
        int i8 = i7 + 1;
        C2491fm a3 = C4105zY.m41624a(ClipType.class, "61088221bca958245580864e0c51c702", i8);
        dVN = a3;
        fmVarArr[i8] = a3;
        int i9 = i8 + 1;
        C2491fm a4 = C4105zY.m41624a(ClipType.class, "d7eaf5d821223aceeb342d0d9613a246", i9);
        iDi = a4;
        fmVarArr[i9] = a4;
        int i10 = i9 + 1;
        C2491fm a5 = C4105zY.m41624a(ClipType.class, "fb35cb440f650ad3faeec2b4cd28b442", i10);
        eft = a5;
        fmVarArr[i10] = a5;
        int i11 = i10 + 1;
        C2491fm a6 = C4105zY.m41624a(ClipType.class, "d9fcc7cc9247db1185c74c2d50d9e060", i11);
        hPb = a6;
        fmVarArr[i11] = a6;
        int i12 = i11 + 1;
        C2491fm a7 = C4105zY.m41624a(ClipType.class, "aa13cd2dc17218f9ca45dee55468243b", i12);
        f3647vi = a7;
        fmVarArr[i12] = a7;
        int i13 = i12 + 1;
        C2491fm a8 = C4105zY.m41624a(ClipType.class, "3b4dc6a6e356fb0c2a357773a5d66db4", i13);
        f3648vj = a8;
        fmVarArr[i13] = a8;
        int i14 = i13 + 1;
        C2491fm a9 = C4105zY.m41624a(ClipType.class, "5e858f1eb982ffba166c4049f47e21ca", i14);
        eNq = a9;
        fmVarArr[i14] = a9;
        int i15 = i14 + 1;
        C2491fm a10 = C4105zY.m41624a(ClipType.class, "a7908da6d1b2605e86106effbdeee357", i15);
        f3643dN = a10;
        fmVarArr[i15] = a10;
        int i16 = i15 + 1;
        _m_methods = fmVarArr;
        C1634Xv.m11725a((Object[]) BaseClipType._m_methods, (Object[]) _m_methods);
        ___iScriptClass = aUO.m18566a(ClipType.class, C5293aDt.class, _m_fields, _m_methods);
    }

    /* renamed from: H */
    private void m17580H(String str) {
        bFf().mo5608dq().mo3197f(f3646uU, str);
    }

    /* renamed from: S */
    private void m17584S(C1556Wo wo) {
        bFf().mo5608dq().mo3197f(f3644uK, wo);
    }

    private C1556Wo bAH() {
        return (C1556Wo) bFf().mo5608dq().mo3214p(f3644uK);
    }

    private int buf() {
        return bFf().mo5608dq().mo3212n(efi);
    }

    /* renamed from: ij */
    private String m17587ij() {
        return (String) bFf().mo5608dq().mo3214p(f3646uU);
    }

    /* renamed from: nl */
    private void m17589nl(int i) {
        bFf().mo5608dq().mo3183b(efi, i);
    }

    /* renamed from: r */
    private void m17590r(Asset tCVar) {
        bFf().mo5608dq().mo3197f(f3642LR, tCVar);
    }

    /* renamed from: rh */
    private Asset m17591rh() {
        return (Asset) bFf().mo5608dq().mo3214p(f3642LR);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Render Asset")
    /* renamed from: I */
    public void mo11037I(Asset tCVar) {
        switch (bFf().mo6893i(aDl)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, aDl, new Object[]{tCVar}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, aDl, new Object[]{tCVar}));
                break;
        }
        m17579H(tCVar);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Damage")
    /* renamed from: K */
    public void mo11038K(Map<DamageType, Float> map) {
        switch (bFf().mo6893i(iDi)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, iDi, new Object[]{map}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, iDi, new Object[]{map}));
                break;
        }
        m17581J(map);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Surface Material")
    /* renamed from: N */
    public void mo11039N(String str) {
        switch (bFf().mo6893i(f3648vj)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f3648vj, new Object[]{str}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f3648vj, new Object[]{str}));
                break;
        }
        m17582M(str);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Render Asset")
    /* renamed from: Nu */
    public Asset mo3521Nu() {
        switch (bFf().mo6893i(aDk)) {
            case 0:
                return null;
            case 2:
                return (Asset) bFf().mo5606d(new aCE(this, aDk, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, aDk, new Object[0]));
                break;
        }
        return m17583Nt();
    }

    /* renamed from: T */
    public C6494any mo11T() {
        return ___iScriptClass;
    }

    /* renamed from: U */
    public C2491fm[] mo12U() {
        return _m_methods;
    }

    /* renamed from: W */
    public Object mo13W() {
        return new C5293aDt(this);
    }

    /* renamed from: a */
    public Object mo14a(C0495Gr gr) {
        Object[] args = gr.getArgs();
        switch (gr.mo2417hq() - BaseClipType._m_methodCount) {
            case 0:
                return m17583Nt();
            case 1:
                m17579H((Asset) args[0]);
                return null;
            case 2:
                return bpz();
            case 3:
                m17581J((Map) args[0]);
                return null;
            case 4:
                return new Integer(bum());
            case 5:
                m17592xX(((Integer) args[0]).intValue());
                return null;
            case 6:
                return m17588it();
            case 7:
                m17582M((String) args[0]);
                return null;
            case 8:
                return bIl();
            case 9:
                return m17586aT();
            default:
                return super.mo14a(gr);
        }
    }

    /* renamed from: a */
    public void mo15a(Collection collection, C0495Gr gr) {
        super.mo15a(collection, gr);
    }

    /* access modifiers changed from: protected */
    /* renamed from: aU */
    public <T extends aDJ> T mo745aU() {
        switch (bFf().mo6893i(f3643dN)) {
            case 0:
                return null;
            case 2:
                return (aDJ) bFf().mo5606d(new aCE(this, f3643dN, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f3643dN, new Object[0]));
                break;
        }
        return m17586aT();
    }

    public Clip bIm() {
        switch (bFf().mo6893i(eNq)) {
            case 0:
                return null;
            case 2:
                return (Clip) bFf().mo5606d(new aCE(this, eNq, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, eNq, new Object[0]));
                break;
        }
        return bIl();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Damage")
    public C1556Wo<DamageType, Float> bpA() {
        switch (bFf().mo6893i(dVN)) {
            case 0:
                return null;
            case 2:
                return (C1556Wo) bFf().mo5606d(new aCE(this, dVN, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, dVN, new Object[0]));
                break;
        }
        return bpz();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Caliber")
    public int bun() {
        switch (bFf().mo6893i(eft)) {
            case 0:
                return 0;
            case 2:
                return ((Integer) bFf().mo5606d(new aCE(this, eft, new Object[0]))).intValue();
            case 3:
                bFf().mo5606d(new aCE(this, eft, new Object[0]));
                break;
        }
        return bum();
    }

    /* renamed from: c */
    public C5663aRz[] mo25c() {
        return _m_fields;
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Surface Material")
    /* renamed from: iu */
    public String mo11042iu() {
        switch (bFf().mo6893i(f3647vi)) {
            case 0:
                return null;
            case 2:
                return (String) bFf().mo5606d(new aCE(this, f3647vi, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f3647vi, new Object[0]));
                break;
        }
        return m17588it();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Caliber")
    /* renamed from: xY */
    public void mo11043xY(int i) {
        switch (bFf().mo6893i(hPb)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, hPb, new Object[]{new Integer(i)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, hPb, new Object[]{new Integer(i)}));
                break;
        }
        m17592xX(i);
    }

    /* renamed from: S */
    public void mo10S() {
        super.mo10S();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Render Asset")
    @C0064Am(aul = "26f0b9c5d8cef367a6a5f066fa643614", aum = 0)
    /* renamed from: Nt */
    private Asset m17583Nt() {
        return m17591rh();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Render Asset")
    @C0064Am(aul = "7ebe707229da70bb6c3e6df30283142a", aum = 0)
    /* renamed from: H */
    private void m17579H(Asset tCVar) {
        m17590r(tCVar);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Damage")
    @C0064Am(aul = "61088221bca958245580864e0c51c702", aum = 0)
    private C1556Wo<DamageType, Float> bpz() {
        return bAH();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Damage")
    @C0064Am(aul = "d7eaf5d821223aceeb342d0d9613a246", aum = 0)
    /* renamed from: J */
    private void m17581J(Map<DamageType, Float> map) {
        bAH().clear();
        bAH().putAll(map);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Caliber")
    @C0064Am(aul = "fb35cb440f650ad3faeec2b4cd28b442", aum = 0)
    private int bum() {
        return buf();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Caliber")
    @C0064Am(aul = "d9fcc7cc9247db1185c74c2d50d9e060", aum = 0)
    /* renamed from: xX */
    private void m17592xX(int i) {
        m17589nl(i);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Surface Material")
    @C0064Am(aul = "aa13cd2dc17218f9ca45dee55468243b", aum = 0)
    /* renamed from: it */
    private String m17588it() {
        return m17587ij();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Surface Material")
    @C0064Am(aul = "3b4dc6a6e356fb0c2a357773a5d66db4", aum = 0)
    /* renamed from: M */
    private void m17582M(String str) {
        m17580H(str);
    }

    @C0064Am(aul = "5e858f1eb982ffba166c4049f47e21ca", aum = 0)
    private Clip bIl() {
        return (Clip) mo745aU();
    }

    @C0064Am(aul = "a7908da6d1b2605e86106effbdeee357", aum = 0)
    /* renamed from: aT */
    private <T extends aDJ> T m17586aT() {
        T t = (Clip) bFf().mo6865M(Clip.class);
        t.mo6422l(this);
        return t;
    }
}
