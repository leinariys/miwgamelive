package game.script.item;

import game.network.message.externalizable.aCE;
import game.network.message.serializable.C3438ra;
import game.script.pda.DatabaseCategory;
import game.script.pda.TaikopediaEntry;
import game.script.player.Player;
import game.script.progression.ProgressionAbility;
import game.script.resource.Asset;
import logic.baa.*;
import logic.data.mbean.C1679Yj;
import logic.res.code.C5640aRc;
import logic.res.code.C5663aRz;
import logic.res.code.C6494any;
import logic.res.html.C2491fm;
import p001a.*;
import taikodom.infra.script.I18NString;

import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.UUID;

@C5511aMd
@C6485anp
/* renamed from: a.aai  reason: case insensitive filesystem */
/* compiled from: a */
public class ItemTypeCategory extends aDJ implements C0468GU, C1616Xf, aWv, C4068yr {

    /* renamed from: MN */
    public static final C2491fm f4088MN = null;

    /* renamed from: NY */
    public static final C5663aRz f4089NY = null;

    /* renamed from: Ob */
    public static final C2491fm f4090Ob = null;

    /* renamed from: Oc */
    public static final C2491fm f4091Oc = null;

    /* renamed from: QA */
    public static final C5663aRz f4092QA = null;

    /* renamed from: QE */
    public static final C2491fm f4093QE = null;

    /* renamed from: QF */
    public static final C2491fm f4094QF = null;
    public static final C2491fm _f_onResurrect_0020_0028_0029V = null;
    public static final C2491fm _f_toString_0020_0028_0029Ljava_002flang_002fString_003b = null;
    public static final int _m_fieldCount = 0;
    public static final C5663aRz[] _m_fields = null;
    public static final int _m_methodCount = 0;
    public static final C2491fm[] _m_methods = null;
    public static final C5663aRz aMg = null;
    public static final C5663aRz aMh = null;
    public static final C5663aRz aMi = null;
    public static final C2491fm aMj = null;
    public static final C2491fm aMk = null;
    public static final C2491fm aMl = null;
    public static final C2491fm aMn = null;
    public static final C2491fm aMo = null;
    public static final C2491fm aMp = null;
    public static final C2491fm aMq = null;
    public static final C2491fm aMr = null;
    public static final C2491fm aMs = null;
    public static final C2491fm aMu = null;
    public static final C2491fm aMv = null;
    public static final C5663aRz anb = null;
    public static final C2491fm any = null;
    public static final C2491fm anz = null;
    public static final C5663aRz awd = null;
    public static final C2491fm bHi = null;
    /* renamed from: bL */
    public static final C5663aRz f4096bL = null;
    /* renamed from: bM */
    public static final C5663aRz f4097bM = null;
    /* renamed from: bN */
    public static final C2491fm f4098bN = null;
    /* renamed from: bO */
    public static final C2491fm f4099bO = null;
    /* renamed from: bP */
    public static final C2491fm f4100bP = null;
    /* renamed from: bQ */
    public static final C2491fm f4101bQ = null;
    public static final C2491fm bcI = null;
    public static final C5663aRz dSF = null;
    public static final C5663aRz dyY = null;
    public static final C2491fm dzc = null;
    public static final C2491fm eSA = null;
    public static final C2491fm eSB = null;
    public static final C5663aRz eSv = null;
    public static final C2491fm eSw = null;
    public static final C2491fm eSx = null;
    public static final C2491fm eSy = null;
    public static final C2491fm eSz = null;
    public static final long serialVersionUID = 0;
    /* renamed from: zQ */
    public static final C5663aRz f4109zQ = null;
    /* renamed from: zT */
    public static final C2491fm f4110zT = null;
    /* renamed from: zU */
    public static final C2491fm f4111zU = null;
    public static C6494any ___iScriptClass;
    @C0064Am(aul = "39da1f3858f20d0b3e029908e3e4d9d2", aum = 9)
    private static ProgressionAbility ana;
    @C0064Am(aul = "b0553ac47dde3fbb23f4687696359b5d", aum = 12)

    /* renamed from: bK */
    private static UUID f4095bK;
    @C0064Am(aul = "7dba25eeecc7e2d4627ab8621357732b", aum = 10)
    private static boolean dyX;
    @C0064Am(aul = "f452849beae42130023013af95b97315", aum = 2)
    private static C3438ra<ItemTypeCategory> eLj;
    @C0064Am(aul = "619e803eb8933cd0ca8228c052f903c0", aum = 3)
    private static ItemTypeCategory eLk;
    @C0064Am(aul = "5e96d760c331c65c9660cb725288e6a7", aum = 1)
    private static String handle;
    @C0064Am(aul = "8b9ba86467cda49bb9f43e572e5ba075", aum = 6)

    /* renamed from: jS */
    private static DatabaseCategory f4102jS;
    @C0064Am(aul = "127c687bda709d7fcc067219e276c412", aum = 8)

    /* renamed from: jT */
    private static C3438ra<TaikopediaEntry> f4103jT;
    @C0064Am(aul = "639c65de6fe8bdebebf32beb363180ff", aum = 5)

    /* renamed from: jU */
    private static Asset f4104jU;
    @C0064Am(aul = "0c2d8f26200f21dd6de6716a656487f3", aum = 11)

    /* renamed from: jV */
    private static float f4105jV;
    @C0064Am(aul = "3f25dc4f1a3255ec45a70510f1896c16", aum = 4)

    /* renamed from: jn */
    private static Asset f4106jn;
    @C0064Am(aul = "5cb6234f0923c66c217789d000b397d2", aum = 7)

    /* renamed from: nh */
    private static I18NString f4107nh;
    @C0064Am(aul = "6267ac88fde876f5d33962ace7483f87", aum = 0)

    /* renamed from: zP */
    private static I18NString f4108zP;

    static {
        m19581V();
    }

    public ItemTypeCategory() {
        super((C5540aNg) null);
        super.mo10S();
    }

    public ItemTypeCategory(C5540aNg ang) {
        super(ang);
    }

    /* renamed from: V */
    static void m19581V() {
        serialVersionUID = (long) 1;
        _m_fieldCount = aDJ._m_fieldCount + 13;
        _m_methodCount = aDJ._m_methodCount + 35;
        int i = aDJ._m_fieldCount;
        C5663aRz[] arzArr = new C5663aRz[(i + 13)];
        C5663aRz b = C5640aRc.m17844b(ItemTypeCategory.class, "6267ac88fde876f5d33962ace7483f87", i);
        f4109zQ = b;
        arzArr[i] = b;
        int i2 = i + 1;
        C5663aRz b2 = C5640aRc.m17844b(ItemTypeCategory.class, "5e96d760c331c65c9660cb725288e6a7", i2);
        f4097bM = b2;
        arzArr[i2] = b2;
        int i3 = i2 + 1;
        C5663aRz b3 = C5640aRc.m17844b(ItemTypeCategory.class, "f452849beae42130023013af95b97315", i3);
        eSv = b3;
        arzArr[i3] = b3;
        int i4 = i3 + 1;
        C5663aRz b4 = C5640aRc.m17844b(ItemTypeCategory.class, "619e803eb8933cd0ca8228c052f903c0", i4);
        dSF = b4;
        arzArr[i4] = b4;
        int i5 = i4 + 1;
        C5663aRz b5 = C5640aRc.m17844b(ItemTypeCategory.class, "3f25dc4f1a3255ec45a70510f1896c16", i5);
        f4089NY = b5;
        arzArr[i5] = b5;
        int i6 = i5 + 1;
        C5663aRz b6 = C5640aRc.m17844b(ItemTypeCategory.class, "639c65de6fe8bdebebf32beb363180ff", i6);
        aMg = b6;
        arzArr[i6] = b6;
        int i7 = i6 + 1;
        C5663aRz b7 = C5640aRc.m17844b(ItemTypeCategory.class, "8b9ba86467cda49bb9f43e572e5ba075", i7);
        aMh = b7;
        arzArr[i7] = b7;
        int i8 = i7 + 1;
        C5663aRz b8 = C5640aRc.m17844b(ItemTypeCategory.class, "5cb6234f0923c66c217789d000b397d2", i8);
        f4092QA = b8;
        arzArr[i8] = b8;
        int i9 = i8 + 1;
        C5663aRz b9 = C5640aRc.m17844b(ItemTypeCategory.class, "127c687bda709d7fcc067219e276c412", i9);
        awd = b9;
        arzArr[i9] = b9;
        int i10 = i9 + 1;
        C5663aRz b10 = C5640aRc.m17844b(ItemTypeCategory.class, "39da1f3858f20d0b3e029908e3e4d9d2", i10);
        anb = b10;
        arzArr[i10] = b10;
        int i11 = i10 + 1;
        C5663aRz b11 = C5640aRc.m17844b(ItemTypeCategory.class, "7dba25eeecc7e2d4627ab8621357732b", i11);
        dyY = b11;
        arzArr[i11] = b11;
        int i12 = i11 + 1;
        C5663aRz b12 = C5640aRc.m17844b(ItemTypeCategory.class, "0c2d8f26200f21dd6de6716a656487f3", i12);
        aMi = b12;
        arzArr[i12] = b12;
        int i13 = i12 + 1;
        C5663aRz b13 = C5640aRc.m17844b(ItemTypeCategory.class, "b0553ac47dde3fbb23f4687696359b5d", i13);
        f4096bL = b13;
        arzArr[i13] = b13;
        int i14 = i13 + 1;
        _m_fields = arzArr;
        C1634Xv.m11725a((Object[]) aDJ._m_fields, (Object[]) _m_fields);
        int i15 = aDJ._m_methodCount;
        C2491fm[] fmVarArr = new C2491fm[(i15 + 35)];
        C2491fm a = C4105zY.m41624a(ItemTypeCategory.class, "71ac1b9d7ce70f8c747b95467f9d94be", i15);
        f4098bN = a;
        fmVarArr[i15] = a;
        int i16 = i15 + 1;
        C2491fm a2 = C4105zY.m41624a(ItemTypeCategory.class, "87d7191b764cb689efcdd6a4808f38be", i16);
        f4099bO = a2;
        fmVarArr[i16] = a2;
        int i17 = i16 + 1;
        C2491fm a3 = C4105zY.m41624a(ItemTypeCategory.class, "fd252a6d396b1504c04929f06aac4151", i17);
        eSw = a3;
        fmVarArr[i17] = a3;
        int i18 = i17 + 1;
        C2491fm a4 = C4105zY.m41624a(ItemTypeCategory.class, "459e37cda2dca45f1391793111b8145f", i18);
        eSx = a4;
        fmVarArr[i18] = a4;
        int i19 = i18 + 1;
        C2491fm a5 = C4105zY.m41624a(ItemTypeCategory.class, "da2dd19eaeb9290468e79c2a0a13b700", i19);
        f4100bP = a5;
        fmVarArr[i19] = a5;
        int i20 = i19 + 1;
        C2491fm a6 = C4105zY.m41624a(ItemTypeCategory.class, "ebbd05b010e2d581ab73edd09bdfa9ff", i20);
        f4101bQ = a6;
        fmVarArr[i20] = a6;
        int i21 = i20 + 1;
        C2491fm a7 = C4105zY.m41624a(ItemTypeCategory.class, "c3cbef85990e8f1462e1c56b79e5dca3", i21);
        f4110zT = a7;
        fmVarArr[i21] = a7;
        int i22 = i21 + 1;
        C2491fm a8 = C4105zY.m41624a(ItemTypeCategory.class, "d52103696995c799186bad713862f9f3", i22);
        f4111zU = a8;
        fmVarArr[i22] = a8;
        int i23 = i22 + 1;
        C2491fm a9 = C4105zY.m41624a(ItemTypeCategory.class, "1b8c9259ab45e2a27a92d398b2485e28", i23);
        eSy = a9;
        fmVarArr[i23] = a9;
        int i24 = i23 + 1;
        C2491fm a10 = C4105zY.m41624a(ItemTypeCategory.class, "0c92a150f882aa8fd9563992813669c1", i24);
        eSz = a10;
        fmVarArr[i24] = a10;
        int i25 = i24 + 1;
        C2491fm a11 = C4105zY.m41624a(ItemTypeCategory.class, "2dddaa31c5c9e862475e64dba8add5d8", i25);
        _f_toString_0020_0028_0029Ljava_002flang_002fString_003b = a11;
        fmVarArr[i25] = a11;
        int i26 = i25 + 1;
        C2491fm a12 = C4105zY.m41624a(ItemTypeCategory.class, "834dda90ab6ad9499feef8388b917701", i26);
        f4090Ob = a12;
        fmVarArr[i26] = a12;
        int i27 = i26 + 1;
        C2491fm a13 = C4105zY.m41624a(ItemTypeCategory.class, "373107756d4797a069fab7667f35bbba", i27);
        f4091Oc = a13;
        fmVarArr[i27] = a13;
        int i28 = i27 + 1;
        C2491fm a14 = C4105zY.m41624a(ItemTypeCategory.class, "7c1af8483f64df9bfd009b923046ef96", i28);
        eSA = a14;
        fmVarArr[i28] = a14;
        int i29 = i28 + 1;
        C2491fm a15 = C4105zY.m41624a(ItemTypeCategory.class, "0f3815d3c94373bea3b874ba5c71f744", i29);
        bHi = a15;
        fmVarArr[i29] = a15;
        int i30 = i29 + 1;
        C2491fm a16 = C4105zY.m41624a(ItemTypeCategory.class, "885dded67b4700347286784161435337", i30);
        _f_onResurrect_0020_0028_0029V = a16;
        fmVarArr[i30] = a16;
        int i31 = i30 + 1;
        C2491fm a17 = C4105zY.m41624a(ItemTypeCategory.class, "ff7c39ae7f9b0704ba2a58784b0eeeac", i31);
        aMn = a17;
        fmVarArr[i31] = a17;
        int i32 = i31 + 1;
        C2491fm a18 = C4105zY.m41624a(ItemTypeCategory.class, "c913ebaf35d6267aeb0a9e368b8122ae", i32);
        aMo = a18;
        fmVarArr[i32] = a18;
        int i33 = i32 + 1;
        C2491fm a19 = C4105zY.m41624a(ItemTypeCategory.class, "567586a0e722cc365a96e4464ac00dbe", i33);
        f4093QE = a19;
        fmVarArr[i33] = a19;
        int i34 = i33 + 1;
        C2491fm a20 = C4105zY.m41624a(ItemTypeCategory.class, "fb166a67f2ce4da99a653a0f468c5a75", i34);
        f4094QF = a20;
        fmVarArr[i34] = a20;
        int i35 = i34 + 1;
        C2491fm a21 = C4105zY.m41624a(ItemTypeCategory.class, "9b5f5af3a1c920d48e724b2c191cb04d", i35);
        aMj = a21;
        fmVarArr[i35] = a21;
        int i36 = i35 + 1;
        C2491fm a22 = C4105zY.m41624a(ItemTypeCategory.class, "63c5ac18fc300749c87f62c1c763e1a2", i36);
        aMl = a22;
        fmVarArr[i36] = a22;
        int i37 = i36 + 1;
        C2491fm a23 = C4105zY.m41624a(ItemTypeCategory.class, "878b94895a2e4069b796f5bfc6eb5dde", i37);
        bcI = a23;
        fmVarArr[i37] = a23;
        int i38 = i37 + 1;
        C2491fm a24 = C4105zY.m41624a(ItemTypeCategory.class, "ed79cd29631913d98f11d7c500bb2792", i38);
        aMk = a24;
        fmVarArr[i38] = a24;
        int i39 = i38 + 1;
        C2491fm a25 = C4105zY.m41624a(ItemTypeCategory.class, "3a9d3580d0b74d11513ff92f37347f0a", i39);
        any = a25;
        fmVarArr[i39] = a25;
        int i40 = i39 + 1;
        C2491fm a26 = C4105zY.m41624a(ItemTypeCategory.class, "c2313813c469a34e715a8a3722bf9b7e", i40);
        anz = a26;
        fmVarArr[i40] = a26;
        int i41 = i40 + 1;
        C2491fm a27 = C4105zY.m41624a(ItemTypeCategory.class, "085fc1e24d4ea3fd991b80de0355e682", i41);
        f4088MN = a27;
        fmVarArr[i41] = a27;
        int i42 = i41 + 1;
        C2491fm a28 = C4105zY.m41624a(ItemTypeCategory.class, "073e96a2f0a001591ba2fd9aa52e680d", i42);
        aMp = a28;
        fmVarArr[i42] = a28;
        int i43 = i42 + 1;
        C2491fm a29 = C4105zY.m41624a(ItemTypeCategory.class, "4ebb00c19c50824b86292af44e75f674", i43);
        aMq = a29;
        fmVarArr[i43] = a29;
        int i44 = i43 + 1;
        C2491fm a30 = C4105zY.m41624a(ItemTypeCategory.class, "20dd1a7b93d5c5882acc4f140781117a", i44);
        aMr = a30;
        fmVarArr[i44] = a30;
        int i45 = i44 + 1;
        C2491fm a31 = C4105zY.m41624a(ItemTypeCategory.class, "0ab4ffe749d8f8d58b2a585068225c97", i45);
        aMs = a31;
        fmVarArr[i45] = a31;
        int i46 = i45 + 1;
        C2491fm a32 = C4105zY.m41624a(ItemTypeCategory.class, "1cb82f28ac01207a7d4b972c5ae3cb1d", i46);
        eSB = a32;
        fmVarArr[i46] = a32;
        int i47 = i46 + 1;
        C2491fm a33 = C4105zY.m41624a(ItemTypeCategory.class, "50bf4fc8f0bb93cacd7c9f8f02beac70", i47);
        dzc = a33;
        fmVarArr[i47] = a33;
        int i48 = i47 + 1;
        C2491fm a34 = C4105zY.m41624a(ItemTypeCategory.class, "ff132817f68800830fde804d2e196c3b", i48);
        aMu = a34;
        fmVarArr[i48] = a34;
        int i49 = i48 + 1;
        C2491fm a35 = C4105zY.m41624a(ItemTypeCategory.class, "a73d7ca2ea718c75341579b7d4f33f5b", i49);
        aMv = a35;
        fmVarArr[i49] = a35;
        int i50 = i49 + 1;
        _m_methods = fmVarArr;
        C1634Xv.m11725a((Object[]) aDJ._m_methods, (Object[]) _m_methods);
        ___iScriptClass = aUO.m18566a(ItemTypeCategory.class, C1679Yj.class, _m_fields, _m_methods);
    }

    /* renamed from: Ho */
    private ProgressionAbility m19567Ho() {
        return (ProgressionAbility) bFf().mo5608dq().mo3214p(anb);
    }

    /* renamed from: L */
    private void m19568L(Asset tCVar) {
        bFf().mo5608dq().mo3197f(aMg, tCVar);
    }

    /* renamed from: Ll */
    private C3438ra m19569Ll() {
        return (C3438ra) bFf().mo5608dq().mo3214p(awd);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Pda Render")
    @C0064Am(aul = "0ab4ffe749d8f8d58b2a585068225c97", aum = 0)
    @C5566aOg
    /* renamed from: M */
    private void m19570M(Asset tCVar) {
        throw new aWi(new aCE(this, aMs, new Object[]{tCVar}));
    }

    /* renamed from: RQ */
    private Asset m19571RQ() {
        return (Asset) bFf().mo5608dq().mo3214p(aMg);
    }

    /* renamed from: RR */
    private DatabaseCategory m19572RR() {
        return (DatabaseCategory) bFf().mo5608dq().mo3214p(aMh);
    }

    /* renamed from: RS */
    private float m19573RS() {
        return bFf().mo5608dq().mo3211m(aMi);
    }

    /* renamed from: a */
    private void m19582a(DatabaseCategory aik) {
        bFf().mo5608dq().mo3197f(aMh, aik);
    }

    @C3248pc(aYR = C3248pc.C3250b.ADDER, displayName = "Taikopedia Entries")
    @C0064Am(aul = "63c5ac18fc300749c87f62c1c763e1a2", aum = 0)
    @C5566aOg
    /* renamed from: a */
    private void m19583a(TaikopediaEntry aiz) {
        throw new aWi(new aCE(this, aMl, new Object[]{aiz}));
    }

    /* renamed from: a */
    private void m19584a(String str) {
        bFf().mo5608dq().mo3197f(f4097bM, str);
    }

    /* renamed from: a */
    private void m19585a(UUID uuid) {
        bFf().mo5608dq().mo3197f(f4096bL, uuid);
    }

    /* renamed from: an */
    private UUID m19587an() {
        return (UUID) bFf().mo5608dq().mo3214p(f4096bL);
    }

    /* renamed from: ao */
    private String m19588ao() {
        return (String) bFf().mo5608dq().mo3214p(f4097bM);
    }

    /* renamed from: b */
    private void m19592b(ProgressionAbility lk) {
        bFf().mo5608dq().mo3197f(anb, lk);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Database Category")
    @C0064Am(aul = "c913ebaf35d6267aeb0a9e368b8122ae", aum = 0)
    @C5566aOg
    /* renamed from: b */
    private void m19593b(DatabaseCategory aik) {
        throw new aWi(new aCE(this, aMo, new Object[]{aik}));
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Handle")
    @C0064Am(aul = "ebbd05b010e2d581ab73edd09bdfa9ff", aum = 0)
    @C5566aOg
    /* renamed from: b */
    private void m19594b(String str) {
        throw new aWi(new aCE(this, f4101bQ, new Object[]{str}));
    }

    /* renamed from: bF */
    private void m19596bF(C3438ra raVar) {
        bFf().mo5608dq().mo3197f(eSv, raVar);
    }

    private C3438ra bJN() {
        return (C3438ra) bFf().mo5608dq().mo3214p(eSv);
    }

    private ItemTypeCategory bJO() {
        return (ItemTypeCategory) bFf().mo5608dq().mo3214p(dSF);
    }

    private boolean bgD() {
        return bFf().mo5608dq().mo3201h(dyY);
    }

    /* renamed from: br */
    private void m19598br(I18NString i18NString) {
        bFf().mo5608dq().mo3197f(f4092QA, i18NString);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Description")
    @C0064Am(aul = "fb166a67f2ce4da99a653a0f468c5a75", aum = 0)
    @C5566aOg
    /* renamed from: bu */
    private void m19600bu(I18NString i18NString) {
        throw new aWi(new aCE(this, f4094QF, new Object[]{i18NString}));
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Progression Requirement")
    @C0064Am(aul = "c2313813c469a34e715a8a3722bf9b7e", aum = 0)
    @C5566aOg
    /* renamed from: c */
    private void m19601c(ProgressionAbility lk) {
        throw new aWi(new aCE(this, anz, new Object[]{lk}));
    }

    /* renamed from: c */
    private void m19602c(UUID uuid) {
        switch (bFf().mo6893i(f4099bO)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f4099bO, new Object[]{uuid}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f4099bO, new Object[]{uuid}));
                break;
        }
        m19595b(uuid);
    }

    /* renamed from: cx */
    private void m19603cx(float f) {
        bFf().mo5608dq().mo3150a(aMi, f);
    }

    /* renamed from: d */
    private void m19605d(I18NString i18NString) {
        bFf().mo5608dq().mo3197f(f4109zQ, i18NString);
    }

    /* renamed from: da */
    private void m19606da(boolean z) {
        bFf().mo5608dq().mo3153a(dyY, z);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Pda Visible")
    @C0064Am(aul = "50bf4fc8f0bb93cacd7c9f8f02beac70", aum = 0)
    @C5566aOg
    /* renamed from: db */
    private void m19607db(boolean z) {
        throw new aWi(new aCE(this, dzc, new Object[]{new Boolean(z)}));
    }

    @C3248pc(aYR = C3248pc.C3250b.REMOVER, displayName = "Taikopedia Entries")
    @C0064Am(aul = "878b94895a2e4069b796f5bfc6eb5dde", aum = 0)
    @C5566aOg
    /* renamed from: e */
    private void m19608e(TaikopediaEntry aiz) {
        throw new aWi(new aCE(this, bcI, new Object[]{aiz}));
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Name")
    @C0064Am(aul = "d52103696995c799186bad713862f9f3", aum = 0)
    @C5566aOg
    /* renamed from: e */
    private void m19609e(I18NString i18NString) {
        throw new aWi(new aCE(this, f4111zU, new Object[]{i18NString}));
    }

    /* renamed from: kb */
    private I18NString m19610kb() {
        return (I18NString) bFf().mo5608dq().mo3214p(f4109zQ);
    }

    /* renamed from: n */
    private void m19612n(ItemTypeCategory aai) {
        bFf().mo5608dq().mo3197f(dSF, aai);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Parent")
    @C0064Am(aul = "0c92a150f882aa8fd9563992813669c1", aum = 0)
    @C5566aOg
    /* renamed from: o */
    private void m19613o(ItemTypeCategory aai) {
        throw new aWi(new aCE(this, eSz, new Object[]{aai}));
    }

    /* renamed from: sG */
    private Asset m19615sG() {
        return (Asset) bFf().mo5608dq().mo3214p(f4089NY);
    }

    /* renamed from: t */
    private void m19617t(Asset tCVar) {
        bFf().mo5608dq().mo3197f(f4089NY, tCVar);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "[Asset] Icon")
    @C0064Am(aul = "373107756d4797a069fab7667f35bbba", aum = 0)
    @C5566aOg
    /* renamed from: u */
    private void m19618u(Asset tCVar) {
        throw new aWi(new aCE(this, f4091Oc, new Object[]{tCVar}));
    }

    /* renamed from: vT */
    private I18NString m19619vT() {
        return (I18NString) bFf().mo5608dq().mo3214p(f4092QA);
    }

    /* renamed from: x */
    private void m19621x(C3438ra raVar) {
        bFf().mo5608dq().mo3197f(awd, raVar);
    }

    /* access modifiers changed from: protected */
    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Progression Requirement")
    /* renamed from: HE */
    public ProgressionAbility mo12229HE() {
        switch (bFf().mo6893i(any)) {
            case 0:
                return null;
            case 2:
                return (ProgressionAbility) bFf().mo5606d(new aCE(this, any, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, any, new Object[0]));
                break;
        }
        return m19566HD();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Pda Render")
    @C5566aOg
    /* renamed from: N */
    public void mo12230N(Asset tCVar) {
        switch (bFf().mo6893i(aMs)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, aMs, new Object[]{tCVar}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, aMs, new Object[]{tCVar}));
                break;
        }
        m19570M(tCVar);
    }

    /* renamed from: RU */
    public I18NString mo182RU() {
        switch (bFf().mo6893i(aMj)) {
            case 0:
                return null;
            case 2:
                return (I18NString) bFf().mo5606d(new aCE(this, aMj, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, aMj, new Object[0]));
                break;
        }
        return m19574RT();
    }

    @C3248pc(aYR = C3248pc.C3250b.ITERATOR, displayName = "Taikopedia Entries")
    /* renamed from: RW */
    public List<TaikopediaEntry> mo183RW() {
        switch (bFf().mo6893i(aMk)) {
            case 0:
                return null;
            case 2:
                return (List) bFf().mo5606d(new aCE(this, aMk, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, aMk, new Object[0]));
                break;
        }
        return m19575RV();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Database Category")
    /* renamed from: RY */
    public DatabaseCategory mo184RY() {
        switch (bFf().mo6893i(aMn)) {
            case 0:
                return null;
            case 2:
                return (DatabaseCategory) bFf().mo5606d(new aCE(this, aMn, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, aMn, new Object[0]));
                break;
        }
        return m19576RX();
    }

    /* renamed from: Sa */
    public String mo185Sa() {
        switch (bFf().mo6893i(aMp)) {
            case 0:
                return null;
            case 2:
                return (String) bFf().mo5606d(new aCE(this, aMp, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, aMp, new Object[0]));
                break;
        }
        return m19577RZ();
    }

    /* renamed from: Sc */
    public String mo12231Sc() {
        switch (bFf().mo6893i(aMq)) {
            case 0:
                return null;
            case 2:
                return (String) bFf().mo5606d(new aCE(this, aMq, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, aMq, new Object[0]));
                break;
        }
        return m19578Sb();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Pda Render")
    /* renamed from: Se */
    public Asset mo187Se() {
        switch (bFf().mo6893i(aMr)) {
            case 0:
                return null;
            case 2:
                return (Asset) bFf().mo5606d(new aCE(this, aMr, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, aMr, new Object[0]));
                break;
        }
        return m19579Sd();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Pda MP Multiplier")
    /* renamed from: Sg */
    public float mo188Sg() {
        switch (bFf().mo6893i(aMu)) {
            case 0:
                return 0.0f;
            case 2:
                return ((Float) bFf().mo5606d(new aCE(this, aMu, new Object[0]))).floatValue();
            case 3:
                bFf().mo5606d(new aCE(this, aMu, new Object[0]));
                break;
        }
        return m19580Sf();
    }

    /* renamed from: T */
    public C6494any mo11T() {
        return ___iScriptClass;
    }

    /* renamed from: U */
    public C2491fm[] mo12U() {
        return _m_methods;
    }

    /* renamed from: W */
    public Object mo13W() {
        return new C1679Yj(this);
    }

    /* renamed from: a */
    public Object mo14a(C0495Gr gr) {
        Object[] args = gr.getArgs();
        switch (gr.mo2417hq() - aDJ._m_methodCount) {
            case 0:
                return m19589ap();
            case 1:
                m19595b((UUID) args[0]);
                return null;
            case 2:
                return bJP();
            case 3:
                m19597bG((C3438ra) args[0]);
                return null;
            case 4:
                return m19590ar();
            case 5:
                m19594b((String) args[0]);
                return null;
            case 6:
                return m19611kd();
            case 7:
                m19609e((I18NString) args[0]);
                return null;
            case 8:
                return bJR();
            case 9:
                m19613o((ItemTypeCategory) args[0]);
                return null;
            case 10:
                return m19591au();
            case 11:
                return m19616sJ();
            case 12:
                m19618u((Asset) args[0]);
                return null;
            case 13:
                return new Boolean(m19599bt((Player) args[0]));
            case 14:
                return anK();
            case 15:
                m19586aG();
                return null;
            case 16:
                return m19576RX();
            case 17:
                m19593b((DatabaseCategory) args[0]);
                return null;
            case 18:
                return m19620vV();
            case 19:
                m19600bu((I18NString) args[0]);
                return null;
            case 20:
                return m19574RT();
            case 21:
                m19583a((TaikopediaEntry) args[0]);
                return null;
            case 22:
                m19608e((TaikopediaEntry) args[0]);
                return null;
            case 23:
                return m19575RV();
            case 24:
                return m19566HD();
            case 25:
                m19601c((ProgressionAbility) args[0]);
                return null;
            case 26:
                return m19614rO();
            case 27:
                return m19577RZ();
            case 28:
                return m19578Sb();
            case 29:
                return m19579Sd();
            case 30:
                m19570M((Asset) args[0]);
                return null;
            case 31:
                return new Boolean(bJT());
            case 32:
                m19607db(((Boolean) args[0]).booleanValue());
                return null;
            case 33:
                return new Float(m19580Sf());
            case 34:
                m19604cy(((Float) args[0]).floatValue());
                return null;
            default:
                return super.mo14a(gr);
        }
    }

    /* renamed from: a */
    public void mo15a(Collection collection, C0495Gr gr) {
        super.mo15a(collection, gr);
    }

    /* renamed from: aH */
    public void mo70aH() {
        switch (bFf().mo6893i(_f_onResurrect_0020_0028_0029V)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, _f_onResurrect_0020_0028_0029V, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, _f_onResurrect_0020_0028_0029V, new Object[0]));
                break;
        }
        m19586aG();
    }

    public ProgressionAbility anL() {
        switch (bFf().mo6893i(bHi)) {
            case 0:
                return null;
            case 2:
                return (ProgressionAbility) bFf().mo5606d(new aCE(this, bHi, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, bHi, new Object[0]));
                break;
        }
        return anK();
    }

    /* renamed from: aq */
    public UUID mo18aq() {
        switch (bFf().mo6893i(f4098bN)) {
            case 0:
                return null;
            case 2:
                return (UUID) bFf().mo5606d(new aCE(this, f4098bN, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f4098bN, new Object[0]));
                break;
        }
        return m19589ap();
    }

    @C3248pc(aYR = C3248pc.C3250b.ADDER, displayName = "Taikopedia Entries")
    @C5566aOg
    /* renamed from: b */
    public void mo12233b(TaikopediaEntry aiz) {
        switch (bFf().mo6893i(aMl)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, aMl, new Object[]{aiz}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, aMl, new Object[]{aiz}));
                break;
        }
        m19583a(aiz);
    }

    /* renamed from: bH */
    public void mo12234bH(C3438ra<ItemTypeCategory> raVar) {
        switch (bFf().mo6893i(eSx)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, eSx, new Object[]{raVar}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, eSx, new Object[]{raVar}));
                break;
        }
        m19597bG(raVar);
    }

    @C3248pc(aYR = C3248pc.C3250b.ITERATOR, displayName = "Children")
    public C3438ra<ItemTypeCategory> bJQ() {
        switch (bFf().mo6893i(eSw)) {
            case 0:
                return null;
            case 2:
                return (C3438ra) bFf().mo5606d(new aCE(this, eSw, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, eSw, new Object[0]));
                break;
        }
        return bJP();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Parent")
    public ItemTypeCategory bJS() {
        switch (bFf().mo6893i(eSy)) {
            case 0:
                return null;
            case 2:
                return (ItemTypeCategory) bFf().mo5606d(new aCE(this, eSy, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, eSy, new Object[0]));
                break;
        }
        return bJR();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Pda Visible")
    public boolean bJU() {
        switch (bFf().mo6893i(eSB)) {
            case 0:
                return false;
            case 2:
                return ((Boolean) bFf().mo5606d(new aCE(this, eSB, new Object[0]))).booleanValue();
            case 3:
                bFf().mo5606d(new aCE(this, eSB, new Object[0]));
                break;
        }
        return bJT();
    }

    /* renamed from: bu */
    public boolean mo12238bu(Player aku) {
        switch (bFf().mo6893i(eSA)) {
            case 0:
                return false;
            case 2:
                return ((Boolean) bFf().mo5606d(new aCE(this, eSA, new Object[]{aku}))).booleanValue();
            case 3:
                bFf().mo5606d(new aCE(this, eSA, new Object[]{aku}));
                break;
        }
        return m19599bt(aku);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Description")
    @C5566aOg
    /* renamed from: bv */
    public void mo12239bv(I18NString i18NString) {
        switch (bFf().mo6893i(f4094QF)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f4094QF, new Object[]{i18NString}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f4094QF, new Object[]{i18NString}));
                break;
        }
        m19600bu(i18NString);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Database Category")
    @C5566aOg
    /* renamed from: c */
    public void mo12240c(DatabaseCategory aik) {
        switch (bFf().mo6893i(aMo)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, aMo, new Object[]{aik}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, aMo, new Object[]{aik}));
                break;
        }
        m19593b(aik);
    }

    /* renamed from: c */
    public C5663aRz[] mo25c() {
        return _m_fields;
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Pda MP Multiplier")
    /* renamed from: cz */
    public void mo12241cz(float f) {
        switch (bFf().mo6893i(aMv)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, aMv, new Object[]{new Float(f)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, aMv, new Object[]{new Float(f)}));
                break;
        }
        m19604cy(f);
    }

    /* access modifiers changed from: protected */
    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Progression Requirement")
    @C5566aOg
    /* renamed from: d */
    public void mo12242d(ProgressionAbility lk) {
        switch (bFf().mo6893i(anz)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, anz, new Object[]{lk}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, anz, new Object[]{lk}));
                break;
        }
        m19601c(lk);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Pda Visible")
    @C5566aOg
    /* renamed from: dc */
    public void mo12243dc(boolean z) {
        switch (bFf().mo6893i(dzc)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, dzc, new Object[]{new Boolean(z)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, dzc, new Object[]{new Boolean(z)}));
                break;
        }
        m19607db(z);
    }

    @C3248pc(aYR = C3248pc.C3250b.REMOVER, displayName = "Taikopedia Entries")
    @C5566aOg
    /* renamed from: f */
    public void mo12244f(TaikopediaEntry aiz) {
        switch (bFf().mo6893i(bcI)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, bcI, new Object[]{aiz}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, bcI, new Object[]{aiz}));
                break;
        }
        m19608e(aiz);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Name")
    @C5566aOg
    /* renamed from: f */
    public void mo12245f(I18NString i18NString) {
        switch (bFf().mo6893i(f4111zU)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f4111zU, new Object[]{i18NString}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f4111zU, new Object[]{i18NString}));
                break;
        }
        m19609e(i18NString);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Handle")
    public String getHandle() {
        switch (bFf().mo6893i(f4100bP)) {
            case 0:
                return null;
            case 2:
                return (String) bFf().mo5606d(new aCE(this, f4100bP, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f4100bP, new Object[0]));
                break;
        }
        return m19590ar();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Handle")
    @C5566aOg
    public void setHandle(String str) {
        switch (bFf().mo6893i(f4101bQ)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f4101bQ, new Object[]{str}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f4101bQ, new Object[]{str}));
                break;
        }
        m19594b(str);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Name")
    /* renamed from: ke */
    public I18NString mo12246ke() {
        switch (bFf().mo6893i(f4110zT)) {
            case 0:
                return null;
            case 2:
                return (I18NString) bFf().mo5606d(new aCE(this, f4110zT, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f4110zT, new Object[0]));
                break;
        }
        return m19611kd();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Parent")
    @C5566aOg
    /* renamed from: p */
    public void mo12247p(ItemTypeCategory aai) {
        switch (bFf().mo6893i(eSz)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, eSz, new Object[]{aai}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, eSz, new Object[]{aai}));
                break;
        }
        m19613o(aai);
    }

    /* renamed from: rP */
    public I18NString mo195rP() {
        switch (bFf().mo6893i(f4088MN)) {
            case 0:
                return null;
            case 2:
                return (I18NString) bFf().mo5606d(new aCE(this, f4088MN, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f4088MN, new Object[0]));
                break;
        }
        return m19614rO();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "[Asset] Icon")
    /* renamed from: sK */
    public Asset mo12100sK() {
        switch (bFf().mo6893i(f4090Ob)) {
            case 0:
                return null;
            case 2:
                return (Asset) bFf().mo5606d(new aCE(this, f4090Ob, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f4090Ob, new Object[0]));
                break;
        }
        return m19616sJ();
    }

    public String toString() {
        switch (bFf().mo6893i(_f_toString_0020_0028_0029Ljava_002flang_002fString_003b)) {
            case 0:
                return null;
            case 2:
                return (String) bFf().mo5606d(new aCE(this, _f_toString_0020_0028_0029Ljava_002flang_002fString_003b, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, _f_toString_0020_0028_0029Ljava_002flang_002fString_003b, new Object[0]));
                break;
        }
        return m19591au();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "[Asset] Icon")
    @C5566aOg
    /* renamed from: v */
    public void mo12249v(Asset tCVar) {
        switch (bFf().mo6893i(f4091Oc)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f4091Oc, new Object[]{tCVar}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f4091Oc, new Object[]{tCVar}));
                break;
        }
        m19618u(tCVar);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Description")
    /* renamed from: vW */
    public I18NString mo12250vW() {
        switch (bFf().mo6893i(f4093QE)) {
            case 0:
                return null;
            case 2:
                return (I18NString) bFf().mo5606d(new aCE(this, f4093QE, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f4093QE, new Object[0]));
                break;
        }
        return m19620vV();
    }

    @C0064Am(aul = "71ac1b9d7ce70f8c747b95467f9d94be", aum = 0)
    /* renamed from: ap */
    private UUID m19589ap() {
        return m19587an();
    }

    @C0064Am(aul = "87d7191b764cb689efcdd6a4808f38be", aum = 0)
    /* renamed from: b */
    private void m19595b(UUID uuid) {
        m19585a(uuid);
    }

    /* renamed from: S */
    public void mo10S() {
        super.mo10S();
        setHandle(C0468GU.dac);
        m19585a(UUID.randomUUID());
    }

    @C3248pc(aYR = C3248pc.C3250b.ITERATOR, displayName = "Children")
    @C0064Am(aul = "fd252a6d396b1504c04929f06aac4151", aum = 0)
    private C3438ra<ItemTypeCategory> bJP() {
        return bJN();
    }

    @C0064Am(aul = "459e37cda2dca45f1391793111b8145f", aum = 0)
    /* renamed from: bG */
    private void m19597bG(C3438ra<ItemTypeCategory> raVar) {
        m19596bF(raVar);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Handle")
    @C0064Am(aul = "da2dd19eaeb9290468e79c2a0a13b700", aum = 0)
    /* renamed from: ar */
    private String m19590ar() {
        return m19588ao();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Name")
    @C0064Am(aul = "c3cbef85990e8f1462e1c56b79e5dca3", aum = 0)
    /* renamed from: kd */
    private I18NString m19611kd() {
        return m19610kb();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Parent")
    @C0064Am(aul = "1b8c9259ab45e2a27a92d398b2485e28", aum = 0)
    private ItemTypeCategory bJR() {
        return bJO();
    }

    @C0064Am(aul = "2dddaa31c5c9e862475e64dba8add5d8", aum = 0)
    /* renamed from: au */
    private String m19591au() {
        if (bJO() != null) {
            return bJO().mo12246ke() + " - " + m19610kb().get();
        }
        return m19610kb().get();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "[Asset] Icon")
    @C0064Am(aul = "834dda90ab6ad9499feef8388b917701", aum = 0)
    /* renamed from: sJ */
    private Asset m19616sJ() {
        return m19615sG();
    }

    @C0064Am(aul = "7c1af8483f64df9bfd009b923046ef96", aum = 0)
    /* renamed from: bt */
    private boolean m19599bt(Player aku) {
        ProgressionAbility anL = anL();
        if (anL == null) {
            return true;
        }
        return aku.mo12659ly().mo20741n(anL);
    }

    @C0064Am(aul = "0f3815d3c94373bea3b874ba5c71f744", aum = 0)
    private ProgressionAbility anK() {
        if (mo12229HE() != null) {
            return mo12229HE();
        }
        if (bJO() == null) {
            return null;
        }
        return bJO().anL();
    }

    @C0064Am(aul = "885dded67b4700347286784161435337", aum = 0)
    /* renamed from: aG */
    private void m19586aG() {
        super.mo70aH();
        if (m19572RR() != null) {
            m19606da(true);
        }
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Database Category")
    @C0064Am(aul = "ff7c39ae7f9b0704ba2a58784b0eeeac", aum = 0)
    /* renamed from: RX */
    private DatabaseCategory m19576RX() {
        return m19572RR();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Description")
    @C0064Am(aul = "567586a0e722cc365a96e4464ac00dbe", aum = 0)
    /* renamed from: vV */
    private I18NString m19620vV() {
        return m19619vT();
    }

    @C0064Am(aul = "9b5f5af3a1c920d48e724b2c191cb04d", aum = 0)
    /* renamed from: RT */
    private I18NString m19574RT() {
        return mo12250vW();
    }

    @C3248pc(aYR = C3248pc.C3250b.ITERATOR, displayName = "Taikopedia Entries")
    @C0064Am(aul = "ed79cd29631913d98f11d7c500bb2792", aum = 0)
    /* renamed from: RV */
    private List<TaikopediaEntry> m19575RV() {
        return Collections.unmodifiableList(m19569Ll());
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Progression Requirement")
    @C0064Am(aul = "3a9d3580d0b74d11513ff92f37347f0a", aum = 0)
    /* renamed from: HD */
    private ProgressionAbility m19566HD() {
        return m19567Ho();
    }

    @C0064Am(aul = "085fc1e24d4ea3fd991b80de0355e682", aum = 0)
    /* renamed from: rO */
    private I18NString m19614rO() {
        return mo12246ke();
    }

    @C0064Am(aul = "073e96a2f0a001591ba2fd9aa52e680d", aum = 0)
    /* renamed from: RZ */
    private String m19577RZ() {
        if (m19615sG() != null) {
            return m19615sG().getHandle();
        }
        return null;
    }

    @C0064Am(aul = "4ebb00c19c50824b86292af44e75f674", aum = 0)
    /* renamed from: Sb */
    private String m19578Sb() {
        return m19571RQ().getHandle();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Pda Render")
    @C0064Am(aul = "20dd1a7b93d5c5882acc4f140781117a", aum = 0)
    /* renamed from: Sd */
    private Asset m19579Sd() {
        return m19571RQ();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Pda Visible")
    @C0064Am(aul = "1cb82f28ac01207a7d4b972c5ae3cb1d", aum = 0)
    private boolean bJT() {
        return bgD();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Pda MP Multiplier")
    @C0064Am(aul = "ff132817f68800830fde804d2e196c3b", aum = 0)
    /* renamed from: Sf */
    private float m19580Sf() {
        return m19573RS();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Pda MP Multiplier")
    @C0064Am(aul = "a73d7ca2ea718c75341579b7d4f33f5b", aum = 0)
    /* renamed from: cy */
    private void m19604cy(float f) {
        m19603cx(f);
    }
}
