package game.script.item;

import game.network.message.externalizable.aCE;
import game.script.resource.Asset;
import logic.baa.*;
import logic.data.mbean.C5534aNa;
import logic.res.code.C5663aRz;
import logic.res.code.C6494any;
import logic.res.html.C2491fm;
import p001a.*;
import taikodom.infra.script.I18NString;

import java.util.Collection;

@C5511aMd
@C6485anp
/* renamed from: a.PE */
/* compiled from: a */
public abstract class MerchandiseBaseType extends BulkItemType implements C1616Xf, C4068yr {

    /* renamed from: MN */
    public static final C2491fm f1354MN = null;
    public static final C2491fm _f_onResurrect_0020_0028_0029V = null;
    public static final int _m_fieldCount = 0;
    public static final C5663aRz[] _m_fields = null;
    public static final int _m_methodCount = 0;
    public static final C2491fm[] _m_methods = null;
    public static final C2491fm aMp = null;
    public static final C2491fm aMq = null;
    public static final C2491fm aMr = null;
    public static final C2491fm dzc = null;
    public static final long serialVersionUID = 0;
    public static C6494any ___iScriptClass;

    static {
        m8332V();
    }

    public MerchandiseBaseType() {
        super((C5540aNg) null);
        super.mo10S();
    }

    public MerchandiseBaseType(C5540aNg ang) {
        super(ang);
    }

    /* renamed from: V */
    static void m8332V() {
        serialVersionUID = (long) 1;
        _m_fieldCount = BulkItemType._m_fieldCount + 0;
        _m_methodCount = BulkItemType._m_methodCount + 6;
        _m_fields = new C5663aRz[(BulkItemType._m_fieldCount + 0)];
        C1634Xv.m11725a((Object[]) BulkItemType._m_fields, (Object[]) _m_fields);
        int i = BulkItemType._m_methodCount;
        C2491fm[] fmVarArr = new C2491fm[(i + 6)];
        C2491fm a = C4105zY.m41624a(MerchandiseBaseType.class, "142c3af727c60e35d67fc0b75126dc4a", i);
        f1354MN = a;
        fmVarArr[i] = a;
        int i2 = i + 1;
        C2491fm a2 = C4105zY.m41624a(MerchandiseBaseType.class, "4bc7d1d045d1e6fee69996436dc9d7f0", i2);
        aMp = a2;
        fmVarArr[i2] = a2;
        int i3 = i2 + 1;
        C2491fm a3 = C4105zY.m41624a(MerchandiseBaseType.class, "63c637e6c3e83804f408d097aaea0ff6", i3);
        aMq = a3;
        fmVarArr[i3] = a3;
        int i4 = i3 + 1;
        C2491fm a4 = C4105zY.m41624a(MerchandiseBaseType.class, "7136f76a47d8c5598aebeff873a6b928", i4);
        _f_onResurrect_0020_0028_0029V = a4;
        fmVarArr[i4] = a4;
        int i5 = i4 + 1;
        C2491fm a5 = C4105zY.m41624a(MerchandiseBaseType.class, "3a1da6845146b4dbab7f4eb2c1e33df3", i5);
        dzc = a5;
        fmVarArr[i5] = a5;
        int i6 = i5 + 1;
        C2491fm a6 = C4105zY.m41624a(MerchandiseBaseType.class, "bf8c8acff05b5ce39d7e2ba1e3dee034", i6);
        aMr = a6;
        fmVarArr[i6] = a6;
        int i7 = i6 + 1;
        _m_methods = fmVarArr;
        C1634Xv.m11725a((Object[]) BulkItemType._m_methods, (Object[]) _m_methods);
        ___iScriptClass = aUO.m18566a(MerchandiseBaseType.class, C5534aNa.class, _m_fields, _m_methods);
    }

    @C0064Am(aul = "bf8c8acff05b5ce39d7e2ba1e3dee034", aum = 0)
    /* renamed from: Sd */
    private Asset m8331Sd() {
        throw new aWi(new aCE(this, aMr, new Object[0]));
    }

    @C0064Am(aul = "3a1da6845146b4dbab7f4eb2c1e33df3", aum = 0)
    /* renamed from: db */
    private void m8334db(boolean z) {
        throw new aWi(new aCE(this, dzc, new Object[]{new Boolean(z)}));
    }

    /* renamed from: Sa */
    public String mo185Sa() {
        switch (bFf().mo6893i(aMp)) {
            case 0:
                return null;
            case 2:
                return (String) bFf().mo5606d(new aCE(this, aMp, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, aMp, new Object[0]));
                break;
        }
        return m8329RZ();
    }

    /* renamed from: Sc */
    public String mo4619Sc() {
        switch (bFf().mo6893i(aMq)) {
            case 0:
                return null;
            case 2:
                return (String) bFf().mo5606d(new aCE(this, aMq, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, aMq, new Object[0]));
                break;
        }
        return m8330Sb();
    }

    /* renamed from: Se */
    public Asset mo187Se() {
        switch (bFf().mo6893i(aMr)) {
            case 0:
                return null;
            case 2:
                return (Asset) bFf().mo5606d(new aCE(this, aMr, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, aMr, new Object[0]));
                break;
        }
        return m8331Sd();
    }

    /* renamed from: T */
    public C6494any mo11T() {
        return ___iScriptClass;
    }

    /* renamed from: U */
    public C2491fm[] mo12U() {
        return _m_methods;
    }

    /* renamed from: W */
    public Object mo13W() {
        return new C5534aNa(this);
    }

    /* renamed from: a */
    public Object mo14a(C0495Gr gr) {
        Object[] args = gr.getArgs();
        switch (gr.mo2417hq() - BulkItemType._m_methodCount) {
            case 0:
                return m8335rO();
            case 1:
                return m8329RZ();
            case 2:
                return m8330Sb();
            case 3:
                m8333aG();
                return null;
            case 4:
                m8334db(((Boolean) args[0]).booleanValue());
                return null;
            case 5:
                return m8331Sd();
            default:
                return super.mo14a(gr);
        }
    }

    /* renamed from: a */
    public void mo15a(Collection collection, C0495Gr gr) {
        super.mo15a(collection, gr);
    }

    /* renamed from: aH */
    public void mo70aH() {
        switch (bFf().mo6893i(_f_onResurrect_0020_0028_0029V)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, _f_onResurrect_0020_0028_0029V, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, _f_onResurrect_0020_0028_0029V, new Object[0]));
                break;
        }
        m8333aG();
    }

    /* renamed from: c */
    public C5663aRz[] mo25c() {
        return _m_fields;
    }

    /* renamed from: dc */
    public void mo4014dc(boolean z) {
        switch (bFf().mo6893i(dzc)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, dzc, new Object[]{new Boolean(z)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, dzc, new Object[]{new Boolean(z)}));
                break;
        }
        m8334db(z);
    }

    /* renamed from: rP */
    public I18NString mo195rP() {
        switch (bFf().mo6893i(f1354MN)) {
            case 0:
                return null;
            case 2:
                return (I18NString) bFf().mo5606d(new aCE(this, f1354MN, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f1354MN, new Object[0]));
                break;
        }
        return m8335rO();
    }

    /* renamed from: S */
    public void mo10S() {
        super.mo10S();
    }

    @C0064Am(aul = "142c3af727c60e35d67fc0b75126dc4a", aum = 0)
    /* renamed from: rO */
    private I18NString m8335rO() {
        return mo19891ke();
    }

    @C0064Am(aul = "4bc7d1d045d1e6fee69996436dc9d7f0", aum = 0)
    /* renamed from: RZ */
    private String m8329RZ() {
        if (mo12100sK() != null) {
            return mo12100sK().getHandle();
        }
        return null;
    }

    @C0064Am(aul = "63c637e6c3e83804f408d097aaea0ff6", aum = 0)
    /* renamed from: Sb */
    private String m8330Sb() {
        if (mo187Se() != null) {
            return mo187Se().getHandle();
        }
        return null;
    }

    @C0064Am(aul = "7136f76a47d8c5598aebeff873a6b928", aum = 0)
    /* renamed from: aG */
    private void m8333aG() {
        super.mo70aH();
        if (mo184RY() != null) {
            mo4014dc(true);
        }
    }
}
