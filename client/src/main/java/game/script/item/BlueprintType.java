package game.script.item;

import game.network.message.externalizable.aCE;
import game.network.message.serializable.C3438ra;
import game.script.crafting.CraftingRecipe;
import logic.baa.*;
import logic.data.mbean.C6010aei;
import logic.res.code.C5640aRc;
import logic.res.code.C5663aRz;
import logic.res.code.C6494any;
import logic.res.html.C2491fm;
import p001a.*;

import java.util.Collection;
import java.util.Collections;
import java.util.List;

@C0566Hp
@C5829abJ("1.0.0")
@C6485anp
@C5511aMd
/* renamed from: a.MR */
/* compiled from: a */
public class BlueprintType extends MerchandiseType implements C1616Xf {
    public static final int _m_fieldCount = 0;
    public static final C5663aRz[] _m_fields = null;
    public static final int _m_methodCount = 0;
    public static final C2491fm[] _m_methods = null;
    public static final C5663aRz dCG = null;
    public static final C5663aRz dCH = null;
    public static final C5663aRz dCI = null;
    public static final C2491fm dCJ = null;
    public static final C2491fm dCK = null;
    public static final C2491fm dCL = null;
    public static final C2491fm dCM = null;
    public static final C2491fm dCN = null;
    public static final C2491fm dCO = null;
    public static final C2491fm dCP = null;
    public static final C2491fm dCQ = null;
    public static final C2491fm dCR = null;
    /* renamed from: dN */
    public static final C2491fm f1105dN = null;
    public static final long serialVersionUID = 0;
    public static C6494any ___iScriptClass;
    @C0064Am(aul = "0ea9322091fb222d5e9a756212fad285", aum = 0)
    private static C3438ra<IngredientsCategory> dBN;
    @C0064Am(aul = "f779b755fe972322f0bb24c3c46d3c7a", aum = 1)
    private static CraftingRecipe dBO;
    @C0064Am(aul = "d8b9fa552edf75c60407c45e21c429aa", aum = 2)
    private static long dBP;

    static {
        m7017V();
    }

    public BlueprintType() {
        super((C5540aNg) null);
        super.mo10S();
    }

    public BlueprintType(C5540aNg ang) {
        super(ang);
    }

    /* renamed from: V */
    static void m7017V() {
        serialVersionUID = (long) 1;
        _m_fieldCount = MerchandiseType._m_fieldCount + 3;
        _m_methodCount = MerchandiseType._m_methodCount + 10;
        int i = MerchandiseType._m_fieldCount;
        C5663aRz[] arzArr = new C5663aRz[(i + 3)];
        C5663aRz b = C5640aRc.m17844b(BlueprintType.class, "0ea9322091fb222d5e9a756212fad285", i);
        dCG = b;
        arzArr[i] = b;
        int i2 = i + 1;
        C5663aRz b2 = C5640aRc.m17844b(BlueprintType.class, "f779b755fe972322f0bb24c3c46d3c7a", i2);
        dCH = b2;
        arzArr[i2] = b2;
        int i3 = i2 + 1;
        C5663aRz b3 = C5640aRc.m17844b(BlueprintType.class, "d8b9fa552edf75c60407c45e21c429aa", i3);
        dCI = b3;
        arzArr[i3] = b3;
        int i4 = i3 + 1;
        _m_fields = arzArr;
        C1634Xv.m11725a((Object[]) MerchandiseType._m_fields, (Object[]) _m_fields);
        int i5 = MerchandiseType._m_methodCount;
        C2491fm[] fmVarArr = new C2491fm[(i5 + 10)];
        C2491fm a = C4105zY.m41624a(BlueprintType.class, "cb41117d773d7d528f78eb0928d51a81", i5);
        dCJ = a;
        fmVarArr[i5] = a;
        int i6 = i5 + 1;
        C2491fm a2 = C4105zY.m41624a(BlueprintType.class, "fc6a3c25955da1131ae5aca594440920", i6);
        dCK = a2;
        fmVarArr[i6] = a2;
        int i7 = i6 + 1;
        C2491fm a3 = C4105zY.m41624a(BlueprintType.class, "b435bb82c325202c77aa904e90d9d5ff", i7);
        dCL = a3;
        fmVarArr[i7] = a3;
        int i8 = i7 + 1;
        C2491fm a4 = C4105zY.m41624a(BlueprintType.class, "d2aef060f4a59fe6685723d2a9bdd192", i8);
        dCM = a4;
        fmVarArr[i8] = a4;
        int i9 = i8 + 1;
        C2491fm a5 = C4105zY.m41624a(BlueprintType.class, "4b7fbd1cec56183c88eca99784caa118", i9);
        dCN = a5;
        fmVarArr[i9] = a5;
        int i10 = i9 + 1;
        C2491fm a6 = C4105zY.m41624a(BlueprintType.class, "4359524702c593f85feaec7c0f108dda", i10);
        dCO = a6;
        fmVarArr[i10] = a6;
        int i11 = i10 + 1;
        C2491fm a7 = C4105zY.m41624a(BlueprintType.class, "a59e25dfbcaf515c4ae30fe3954b9a87", i11);
        dCP = a7;
        fmVarArr[i11] = a7;
        int i12 = i11 + 1;
        C2491fm a8 = C4105zY.m41624a(BlueprintType.class, "435c1d4c5fc0aa2ccfcfb584118ec1fd", i12);
        dCQ = a8;
        fmVarArr[i12] = a8;
        int i13 = i12 + 1;
        C2491fm a9 = C4105zY.m41624a(BlueprintType.class, "037a0580f9da99a9661aafda9c18465d", i13);
        dCR = a9;
        fmVarArr[i13] = a9;
        int i14 = i13 + 1;
        C2491fm a10 = C4105zY.m41624a(BlueprintType.class, "2b994b6fa48f7ce18f482c5733090d83", i14);
        f1105dN = a10;
        fmVarArr[i14] = a10;
        int i15 = i14 + 1;
        _m_methods = fmVarArr;
        C1634Xv.m11725a((Object[]) MerchandiseType._m_methods, (Object[]) _m_methods);
        ___iScriptClass = aUO.m18566a(BlueprintType.class, C6010aei.class, _m_fields, _m_methods);
    }

    private C3438ra bhS() {
        return (C3438ra) bFf().mo5608dq().mo3214p(dCG);
    }

    private CraftingRecipe bhT() {
        return (CraftingRecipe) bFf().mo5608dq().mo3214p(dCH);
    }

    private long bhU() {
        return bFf().mo5608dq().mo3213o(dCI);
    }

    /* renamed from: bi */
    private void m7020bi(C3438ra raVar) {
        bFf().mo5608dq().mo3197f(dCG, raVar);
    }

    /* renamed from: e */
    private void m7022e(CraftingRecipe b) {
        bFf().mo5608dq().mo3197f(dCH, b);
    }

    /* renamed from: fc */
    private void m7024fc(long j) {
        bFf().mo5608dq().mo3184b(dCI, j);
    }

    /* renamed from: T */
    public C6494any mo11T() {
        return ___iScriptClass;
    }

    /* renamed from: U */
    public C2491fm[] mo12U() {
        return _m_methods;
    }

    /* renamed from: W */
    public Object mo13W() {
        return new C6010aei(this);
    }

    /* renamed from: a */
    public Object mo14a(C0495Gr gr) {
        Object[] args = gr.getArgs();
        switch (gr.mo2417hq() - MerchandiseType._m_methodCount) {
            case 0:
                m7018a((IngredientsCategory) args[0]);
                return null;
            case 1:
                m7021c((IngredientsCategory) args[0]);
                return null;
            case 2:
                return bhV();
            case 3:
                return bhX();
            case 4:
                return bhZ();
            case 5:
                m7023f((CraftingRecipe) args[0]);
                return null;
            case 6:
                return new Long(bib());
            case 7:
                m7025fd(((Long) args[0]).longValue());
                return null;
            case 8:
                return bid();
            case 9:
                return m7019aT();
            default:
                return super.mo14a(gr);
        }
    }

    /* renamed from: a */
    public void mo15a(Collection collection, C0495Gr gr) {
        super.mo15a(collection, gr);
    }

    /* access modifiers changed from: protected */
    /* renamed from: aU */
    public <T extends aDJ> T mo745aU() {
        switch (bFf().mo6893i(f1105dN)) {
            case 0:
                return null;
            case 2:
                return (aDJ) bFf().mo5606d(new aCE(this, f1105dN, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f1105dN, new Object[0]));
                break;
        }
        return m7019aT();
    }

    @C3248pc(aYR = C3248pc.C3250b.ADDER, displayName = "Ingredient Categories")
    /* renamed from: b */
    public void mo3948b(IngredientsCategory xVVar) {
        switch (bFf().mo6893i(dCJ)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, dCJ, new Object[]{xVVar}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, dCJ, new Object[]{xVVar}));
                break;
        }
        m7018a(xVVar);
    }

    public C3438ra<IngredientsCategory> bhW() {
        switch (bFf().mo6893i(dCL)) {
            case 0:
                return null;
            case 2:
                return (C3438ra) bFf().mo5606d(new aCE(this, dCL, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, dCL, new Object[0]));
                break;
        }
        return bhV();
    }

    @C3248pc(aYR = C3248pc.C3250b.ITERATOR, displayName = "Ingredient Categories")
    public List<IngredientsCategory> bhY() {
        switch (bFf().mo6893i(dCM)) {
            case 0:
                return null;
            case 2:
                return (List) bFf().mo5606d(new aCE(this, dCM, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, dCM, new Object[0]));
                break;
        }
        return bhX();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Crafting Result")
    public CraftingRecipe bia() {
        switch (bFf().mo6893i(dCN)) {
            case 0:
                return null;
            case 2:
                return (CraftingRecipe) bFf().mo5606d(new aCE(this, dCN, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, dCN, new Object[0]));
                break;
        }
        return bhZ();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Crafting Cost")
    public long bic() {
        switch (bFf().mo6893i(dCP)) {
            case 0:
                return 0;
            case 2:
                return ((Long) bFf().mo5606d(new aCE(this, dCP, new Object[0]))).longValue();
            case 3:
                bFf().mo5606d(new aCE(this, dCP, new Object[0]));
                break;
        }
        return bib();
    }

    public Blueprint bie() {
        switch (bFf().mo6893i(dCR)) {
            case 0:
                return null;
            case 2:
                return (Blueprint) bFf().mo5606d(new aCE(this, dCR, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, dCR, new Object[0]));
                break;
        }
        return bid();
    }

    /* renamed from: c */
    public C5663aRz[] mo25c() {
        return _m_fields;
    }

    @C3248pc(aYR = C3248pc.C3250b.REMOVER, displayName = "Ingredient Categories")
    /* renamed from: d */
    public void mo3954d(IngredientsCategory xVVar) {
        switch (bFf().mo6893i(dCK)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, dCK, new Object[]{xVVar}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, dCK, new Object[]{xVVar}));
                break;
        }
        m7021c(xVVar);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Crafting Cost")
    /* renamed from: fe */
    public void mo3955fe(long j) {
        switch (bFf().mo6893i(dCQ)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, dCQ, new Object[]{new Long(j)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, dCQ, new Object[]{new Long(j)}));
                break;
        }
        m7025fd(j);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Crafting Result")
    /* renamed from: g */
    public void mo3956g(CraftingRecipe b) {
        switch (bFf().mo6893i(dCO)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, dCO, new Object[]{b}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, dCO, new Object[]{b}));
                break;
        }
        m7023f(b);
    }

    /* renamed from: S */
    public void mo10S() {
        super.mo10S();
    }

    @C3248pc(aYR = C3248pc.C3250b.ADDER, displayName = "Ingredient Categories")
    @C0064Am(aul = "cb41117d773d7d528f78eb0928d51a81", aum = 0)
    /* renamed from: a */
    private void m7018a(IngredientsCategory xVVar) {
        bhS().add(xVVar);
    }

    @C3248pc(aYR = C3248pc.C3250b.REMOVER, displayName = "Ingredient Categories")
    @C0064Am(aul = "fc6a3c25955da1131ae5aca594440920", aum = 0)
    /* renamed from: c */
    private void m7021c(IngredientsCategory xVVar) {
        bhS().remove(xVVar);
    }

    @C0064Am(aul = "b435bb82c325202c77aa904e90d9d5ff", aum = 0)
    private C3438ra<IngredientsCategory> bhV() {
        return bhS();
    }

    @C3248pc(aYR = C3248pc.C3250b.ITERATOR, displayName = "Ingredient Categories")
    @C0064Am(aul = "d2aef060f4a59fe6685723d2a9bdd192", aum = 0)
    private List<IngredientsCategory> bhX() {
        return Collections.unmodifiableList(bhS());
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Crafting Result")
    @C0064Am(aul = "4b7fbd1cec56183c88eca99784caa118", aum = 0)
    private CraftingRecipe bhZ() {
        return bhT();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Crafting Result")
    @C0064Am(aul = "4359524702c593f85feaec7c0f108dda", aum = 0)
    /* renamed from: f */
    private void m7023f(CraftingRecipe b) {
        m7022e(b);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Crafting Cost")
    @C0064Am(aul = "a59e25dfbcaf515c4ae30fe3954b9a87", aum = 0)
    private long bib() {
        return bhU();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Crafting Cost")
    @C0064Am(aul = "435c1d4c5fc0aa2ccfcfb584118ec1fd", aum = 0)
    /* renamed from: fd */
    private void m7025fd(long j) {
        m7024fc(j);
    }

    @C0064Am(aul = "037a0580f9da99a9661aafda9c18465d", aum = 0)
    private Blueprint bid() {
        return (Blueprint) mo745aU();
    }

    @C0064Am(aul = "2b994b6fa48f7ce18f482c5733090d83", aum = 0)
    /* renamed from: aT */
    private <T extends aDJ> T m7019aT() {
        T t = (Blueprint) bFf().mo6865M(Blueprint.class);
        t.mo13284e(this);
        return t;
    }
}
