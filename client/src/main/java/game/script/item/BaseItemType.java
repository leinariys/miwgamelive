package game.script.item;

import game.network.message.externalizable.aCE;
import game.script.Character;
import game.script.progression.ProgressionAbility;
import game.script.resource.Asset;
import game.script.template.BaseTaikodomContent;
import logic.baa.C1616Xf;
import logic.baa.C1634Xv;
import logic.baa.C5511aMd;
import logic.baa.C6485anp;
import logic.data.mbean.C5410aIg;
import logic.res.code.C5640aRc;
import logic.res.code.C5663aRz;
import logic.res.code.C6494any;
import logic.res.html.C2491fm;
import p001a.*;
import taikodom.infra.script.I18NString;

import java.util.Collection;

@C5511aMd
@C6485anp
/* renamed from: a.xB */
/* compiled from: a */
public abstract class BaseItemType extends BaseTaikodomContent implements C1616Xf {

    /* renamed from: MN */
    public static final C2491fm f9513MN = null;

    /* renamed from: Ob */
    public static final C2491fm f9514Ob = null;

    /* renamed from: QE */
    public static final C2491fm f9515QE = null;
    public static final C2491fm _f_onResurrect_0020_0028_0029V = null;
    public static final C2491fm _f_toString_0020_0028_0029Ljava_002flang_002fString_003b = null;
    public static final int _m_fieldCount = 0;
    public static final C5663aRz[] _m_fields = null;
    public static final int _m_methodCount = 0;
    public static final C2491fm[] _m_methods = null;
    public static final C2491fm aMj = null;
    public static final C2491fm aMp = null;
    public static final C2491fm anA = null;
    public static final C2491fm anB = null;
    public static final C2491fm anC = null;
    public static final C2491fm anD = null;
    public static final C2491fm anE = null;
    public static final C2491fm anF = null;
    public static final C2491fm anG = null;
    public static final C2491fm anH = null;
    public static final C2491fm anI = null;
    public static final C2491fm anJ = null;
    public static final C2491fm anK = null;
    public static final C2491fm anL = null;
    public static final C2491fm anM = null;
    public static final C2491fm anN = null;
    public static final C2491fm anO = null;
    public static final C2491fm anP = null;
    public static final C2491fm ans = null;
    public static final C2491fm anw = null;
    public static final C2491fm any = null;
    public static final C5663aRz bHc = null;
    public static final C5663aRz bHe = null;
    public static final C5663aRz bHg = null;
    public static final C2491fm bHh = null;
    public static final C2491fm bHi = null;
    public static final C2491fm bHj = null;
    public static final C2491fm bHk = null;
    public static final C2491fm bHl = null;
    public static final C2491fm bHm = null;
    public static final C2491fm bHn = null;
    public static final C2491fm bHo = null;
    public static final C2491fm bHp = null;
    public static final C2491fm bkF = null;
    public static final long serialVersionUID = 0;
    /* renamed from: zT */
    public static final C2491fm f9516zT = null;
    public static C6494any ___iScriptClass;
    @C0064Am(aul = "979d3aa82965b42c976875345efbedc0", aum = 0)
    @C5566aOg
    private static boolean bHb;
    @C0064Am(aul = "28b7ae123e26550f834d7a1e86c3a745", aum = 1)
    @C5566aOg
    private static int bHd;
    @C0064Am(aul = "ffb84d912adef729ebecdeb035df6c08", aum = 2)
    @C5566aOg
    private static long bHf;

    static {
        m40822V();
    }

    public BaseItemType() {
        super((C5540aNg) null);
        super.mo10S();
    }

    public BaseItemType(C5540aNg ang) {
        super(ang);
    }

    /* renamed from: V */
    static void m40822V() {
        serialVersionUID = (long) 1;
        _m_fieldCount = BaseTaikodomContent._m_fieldCount + 3;
        _m_methodCount = BaseTaikodomContent._m_methodCount + 37;
        int i = BaseTaikodomContent._m_fieldCount;
        C5663aRz[] arzArr = new C5663aRz[(i + 3)];
        C5663aRz b = C5640aRc.m17844b(BaseItemType.class, "979d3aa82965b42c976875345efbedc0", i);
        bHc = b;
        arzArr[i] = b;
        int i2 = i + 1;
        C5663aRz b2 = C5640aRc.m17844b(BaseItemType.class, "28b7ae123e26550f834d7a1e86c3a745", i2);
        bHe = b2;
        arzArr[i2] = b2;
        int i3 = i2 + 1;
        C5663aRz b3 = C5640aRc.m17844b(BaseItemType.class, "ffb84d912adef729ebecdeb035df6c08", i3);
        bHg = b3;
        arzArr[i3] = b3;
        int i4 = i3 + 1;
        _m_fields = arzArr;
        C1634Xv.m11725a((Object[]) BaseTaikodomContent._m_fields, (Object[]) _m_fields);
        int i5 = BaseTaikodomContent._m_methodCount;
        C2491fm[] fmVarArr = new C2491fm[(i5 + 37)];
        C2491fm a = C4105zY.m41624a(BaseItemType.class, "1361457e3c11a83600917da799012d65", i5);
        anw = a;
        fmVarArr[i5] = a;
        int i6 = i5 + 1;
        C2491fm a2 = C4105zY.m41624a(BaseItemType.class, "f204ecebd91c4492024e7716be889e09", i6);
        f9516zT = a2;
        fmVarArr[i6] = a2;
        int i7 = i6 + 1;
        C2491fm a3 = C4105zY.m41624a(BaseItemType.class, "80b48776c3b197e75cf51e7aa461488c", i7);
        ans = a3;
        fmVarArr[i7] = a3;
        int i8 = i7 + 1;
        C2491fm a4 = C4105zY.m41624a(BaseItemType.class, "560229ecbc18b972dd12f465c89c017b", i8);
        _f_toString_0020_0028_0029Ljava_002flang_002fString_003b = a4;
        fmVarArr[i8] = a4;
        int i9 = i8 + 1;
        C2491fm a5 = C4105zY.m41624a(BaseItemType.class, "dffd6f68bea109484cf128d6275b7c8e", i9);
        any = a5;
        fmVarArr[i9] = a5;
        int i10 = i9 + 1;
        C2491fm a6 = C4105zY.m41624a(BaseItemType.class, "5f01d2ded047850ec5c83d62ac72a1ff", i10);
        bHh = a6;
        fmVarArr[i10] = a6;
        int i11 = i10 + 1;
        C2491fm a7 = C4105zY.m41624a(BaseItemType.class, "a51b77a16e607d62283336157d177e56", i11);
        bHi = a7;
        fmVarArr[i11] = a7;
        int i12 = i11 + 1;
        C2491fm a8 = C4105zY.m41624a(BaseItemType.class, "f8e545b077658da975a9740333d0c904", i12);
        f9514Ob = a8;
        fmVarArr[i12] = a8;
        int i13 = i12 + 1;
        C2491fm a9 = C4105zY.m41624a(BaseItemType.class, "782e7a81b7ce5c561a1cc6a9a95eec9d", i13);
        bHj = a9;
        fmVarArr[i13] = a9;
        int i14 = i13 + 1;
        C2491fm a10 = C4105zY.m41624a(BaseItemType.class, "3dd405c32b0f5890ad542844775e46ae", i14);
        anA = a10;
        fmVarArr[i14] = a10;
        int i15 = i14 + 1;
        C2491fm a11 = C4105zY.m41624a(BaseItemType.class, "18f2a103319e7b75eb2d9b27d647e6c1", i15);
        anB = a11;
        fmVarArr[i15] = a11;
        int i16 = i15 + 1;
        C2491fm a12 = C4105zY.m41624a(BaseItemType.class, "bfae9d28260a8edb1b9562e50174162a", i16);
        anC = a12;
        fmVarArr[i16] = a12;
        int i17 = i16 + 1;
        C2491fm a13 = C4105zY.m41624a(BaseItemType.class, "3ab9819c85071ce2b3ab44006c9c3911", i17);
        anE = a13;
        fmVarArr[i17] = a13;
        int i18 = i17 + 1;
        C2491fm a14 = C4105zY.m41624a(BaseItemType.class, "153051fc2ad88833732b8aaf4ccb780a", i18);
        anD = a14;
        fmVarArr[i18] = a14;
        int i19 = i18 + 1;
        C2491fm a15 = C4105zY.m41624a(BaseItemType.class, "7fd00f3edb65e333fabd1e23ec205542", i19);
        anF = a15;
        fmVarArr[i19] = a15;
        int i20 = i19 + 1;
        C2491fm a16 = C4105zY.m41624a(BaseItemType.class, "bb5dd010d25253311a439d6b83aae2f0", i20);
        anG = a16;
        fmVarArr[i20] = a16;
        int i21 = i20 + 1;
        C2491fm a17 = C4105zY.m41624a(BaseItemType.class, "f7b539fc037ddd54f6d127aaef6b2b6a", i21);
        anH = a17;
        fmVarArr[i21] = a17;
        int i22 = i21 + 1;
        C2491fm a18 = C4105zY.m41624a(BaseItemType.class, "64eeb5dd0aaf3b3cad6540dd234c5471", i22);
        anI = a18;
        fmVarArr[i22] = a18;
        int i23 = i22 + 1;
        C2491fm a19 = C4105zY.m41624a(BaseItemType.class, "33b1676345f802f648ba4b61a5f89ed5", i23);
        anJ = a19;
        fmVarArr[i23] = a19;
        int i24 = i23 + 1;
        C2491fm a20 = C4105zY.m41624a(BaseItemType.class, "6d5afb45f5920859e2ac244083115288", i24);
        anK = a20;
        fmVarArr[i24] = a20;
        int i25 = i24 + 1;
        C2491fm a21 = C4105zY.m41624a(BaseItemType.class, "e3e8921d13aa5be2c116371693bec234", i25);
        anL = a21;
        fmVarArr[i25] = a21;
        int i26 = i25 + 1;
        C2491fm a22 = C4105zY.m41624a(BaseItemType.class, "1b31d61c639ce45dc1c0a0d77cd30a17", i26);
        anM = a22;
        fmVarArr[i26] = a22;
        int i27 = i26 + 1;
        C2491fm a23 = C4105zY.m41624a(BaseItemType.class, "9a404d7df3a825df220ebe67a26e24ee", i27);
        anN = a23;
        fmVarArr[i27] = a23;
        int i28 = i27 + 1;
        C2491fm a24 = C4105zY.m41624a(BaseItemType.class, "ab9cee722e2f6338578b3d75ce2d2f93", i28);
        anO = a24;
        fmVarArr[i28] = a24;
        int i29 = i28 + 1;
        C2491fm a25 = C4105zY.m41624a(BaseItemType.class, "382843538ecda58465d4f698591e7256", i29);
        anP = a25;
        fmVarArr[i29] = a25;
        int i30 = i29 + 1;
        C2491fm a26 = C4105zY.m41624a(BaseItemType.class, "36aff8901fdcff491bce410837fe753c", i30);
        bHk = a26;
        fmVarArr[i30] = a26;
        int i31 = i30 + 1;
        C2491fm a27 = C4105zY.m41624a(BaseItemType.class, "8160b7fece50d9980a153ad241539ba5", i31);
        bHl = a27;
        fmVarArr[i31] = a27;
        int i32 = i31 + 1;
        C2491fm a28 = C4105zY.m41624a(BaseItemType.class, "7fc109546bfa7f6b9e567c857dfefb98", i32);
        bkF = a28;
        fmVarArr[i32] = a28;
        int i33 = i32 + 1;
        C2491fm a29 = C4105zY.m41624a(BaseItemType.class, "b1bd01b1126ff53b06889885bc31d4e7", i33);
        bHm = a29;
        fmVarArr[i33] = a29;
        int i34 = i33 + 1;
        C2491fm a30 = C4105zY.m41624a(BaseItemType.class, "5c26b0cd015b0931e99a0f73849cb167", i34);
        bHn = a30;
        fmVarArr[i34] = a30;
        int i35 = i34 + 1;
        C2491fm a31 = C4105zY.m41624a(BaseItemType.class, "9beff620b559550fd67e2c2ee01952be", i35);
        bHo = a31;
        fmVarArr[i35] = a31;
        int i36 = i35 + 1;
        C2491fm a32 = C4105zY.m41624a(BaseItemType.class, "05ae629b2ae5c643f9a38b2ca66d724e", i36);
        bHp = a32;
        fmVarArr[i36] = a32;
        int i37 = i36 + 1;
        C2491fm a33 = C4105zY.m41624a(BaseItemType.class, "6ce8aaac712cd291a920b2a2f87d7c6e", i37);
        _f_onResurrect_0020_0028_0029V = a33;
        fmVarArr[i37] = a33;
        int i38 = i37 + 1;
        C2491fm a34 = C4105zY.m41624a(BaseItemType.class, "c26ac5d5d64156f7f43abb6b389866bf", i38);
        f9513MN = a34;
        fmVarArr[i38] = a34;
        int i39 = i38 + 1;
        C2491fm a35 = C4105zY.m41624a(BaseItemType.class, "5c0d4dbfdc9fd059ee55c8a82985b309", i39);
        aMp = a35;
        fmVarArr[i39] = a35;
        int i40 = i39 + 1;
        C2491fm a36 = C4105zY.m41624a(BaseItemType.class, "8eb73cfa9554259c8ea28dc597d2d75d", i40);
        f9515QE = a36;
        fmVarArr[i40] = a36;
        int i41 = i40 + 1;
        C2491fm a37 = C4105zY.m41624a(BaseItemType.class, "5d25648a9bb4c558c4a20f500a5921ea", i41);
        aMj = a37;
        fmVarArr[i41] = a37;
        int i42 = i41 + 1;
        _m_methods = fmVarArr;
        C1634Xv.m11725a((Object[]) BaseTaikodomContent._m_methods, (Object[]) _m_methods);
        ___iScriptClass = aUO.m18566a(BaseItemType.class, C5410aIg.class, _m_fields, _m_methods);
    }

    @C0064Am(aul = "1361457e3c11a83600917da799012d65", aum = 0)
    /* renamed from: HB */
    private ItemTypeCategory m40809HB() {
        throw new aWi(new aCE(this, anw, new Object[0]));
    }

    @C0064Am(aul = "dffd6f68bea109484cf128d6275b7c8e", aum = 0)
    /* renamed from: HD */
    private ProgressionAbility m40810HD() {
        throw new aWi(new aCE(this, any, new Object[0]));
    }

    @C0064Am(aul = "3dd405c32b0f5890ad542844775e46ae", aum = 0)
    /* renamed from: HF */
    private long m40811HF() {
        throw new aWi(new aCE(this, anA, new Object[0]));
    }

    @C0064Am(aul = "bfae9d28260a8edb1b9562e50174162a", aum = 0)
    /* renamed from: HH */
    private long m40812HH() {
        throw new aWi(new aCE(this, anC, new Object[0]));
    }

    @C0064Am(aul = "3ab9819c85071ce2b3ab44006c9c3911", aum = 0)
    /* renamed from: HJ */
    private long m40813HJ() {
        throw new aWi(new aCE(this, anE, new Object[0]));
    }

    @C0064Am(aul = "bb5dd010d25253311a439d6b83aae2f0", aum = 0)
    /* renamed from: HL */
    private int m40814HL() {
        throw new aWi(new aCE(this, anG, new Object[0]));
    }

    @C0064Am(aul = "64eeb5dd0aaf3b3cad6540dd234c5471", aum = 0)
    /* renamed from: HN */
    private int m40815HN() {
        throw new aWi(new aCE(this, anI, new Object[0]));
    }

    @C0064Am(aul = "6d5afb45f5920859e2ac244083115288", aum = 0)
    /* renamed from: HP */
    private int m40816HP() {
        throw new aWi(new aCE(this, anK, new Object[0]));
    }

    @C0064Am(aul = "1b31d61c639ce45dc1c0a0d77cd30a17", aum = 0)
    /* renamed from: HR */
    private int m40817HR() {
        throw new aWi(new aCE(this, anM, new Object[0]));
    }

    @C0064Am(aul = "ab9cee722e2f6338578b3d75ce2d2f93", aum = 0)
    /* renamed from: HT */
    private int m40818HT() {
        throw new aWi(new aCE(this, anO, new Object[0]));
    }

    @C0064Am(aul = "80b48776c3b197e75cf51e7aa461488c", aum = 0)
    /* renamed from: Hx */
    private long m40819Hx() {
        throw new aWi(new aCE(this, ans, new Object[0]));
    }

    @C0064Am(aul = "7fc109546bfa7f6b9e567c857dfefb98", aum = 0)
    @C5566aOg
    private void adk() {
        throw new aWi(new aCE(this, bkF, new Object[0]));
    }

    private boolean anH() {
        return bFf().mo5608dq().mo3201h(bHc);
    }

    private int anI() {
        return bFf().mo5608dq().mo3212n(bHe);
    }

    private long anJ() {
        return bFf().mo5608dq().mo3213o(bHg);
    }

    @C0064Am(aul = "8160b7fece50d9980a153ad241539ba5", aum = 0)
    @C5566aOg
    private void anO() {
        throw new aWi(new aCE(this, bHl, new Object[0]));
    }

    @C0064Am(aul = "b1bd01b1126ff53b06889885bc31d4e7", aum = 0)
    @C5566aOg
    private long anQ() {
        throw new aWi(new aCE(this, bHm, new Object[0]));
    }

    @C0064Am(aul = "5c26b0cd015b0931e99a0f73849cb167", aum = 0)
    @C5566aOg
    private boolean anS() {
        throw new aWi(new aCE(this, bHn, new Object[0]));
    }

    @C0064Am(aul = "9beff620b559550fd67e2c2ee01952be", aum = 0)
    @C5566aOg
    private int anU() {
        throw new aWi(new aCE(this, bHo, new Object[0]));
    }

    /* renamed from: bF */
    private void m40825bF(boolean z) {
        bFf().mo5608dq().mo3153a(bHc, z);
    }

    @C0064Am(aul = "18f2a103319e7b75eb2d9b27d647e6c1", aum = 0)
    /* renamed from: bT */
    private void m40826bT(long j) {
        throw new aWi(new aCE(this, anB, new Object[]{new Long(j)}));
    }

    @C0064Am(aul = "153051fc2ad88833732b8aaf4ccb780a", aum = 0)
    /* renamed from: bV */
    private void m40827bV(long j) {
        throw new aWi(new aCE(this, anD, new Object[]{new Long(j)}));
    }

    @C0064Am(aul = "7fd00f3edb65e333fabd1e23ec205542", aum = 0)
    /* renamed from: bX */
    private void m40828bX(long j) {
        throw new aWi(new aCE(this, anF, new Object[]{new Long(j)}));
    }

    @C0064Am(aul = "36aff8901fdcff491bce410837fe753c", aum = 0)
    @C5566aOg
    /* renamed from: c */
    private void m40829c(long j, int i) {
        throw new aWi(new aCE(this, bHk, new Object[]{new Long(j), new Integer(i)}));
    }

    @C0064Am(aul = "f7b539fc037ddd54f6d127aaef6b2b6a", aum = 0)
    /* renamed from: cn */
    private void m40830cn(int i) {
        throw new aWi(new aCE(this, anH, new Object[]{new Integer(i)}));
    }

    @C0064Am(aul = "33b1676345f802f648ba4b61a5f89ed5", aum = 0)
    /* renamed from: cp */
    private void m40831cp(int i) {
        throw new aWi(new aCE(this, anJ, new Object[]{new Integer(i)}));
    }

    @C0064Am(aul = "e3e8921d13aa5be2c116371693bec234", aum = 0)
    /* renamed from: cr */
    private void m40832cr(int i) {
        throw new aWi(new aCE(this, anL, new Object[]{new Integer(i)}));
    }

    @C0064Am(aul = "9a404d7df3a825df220ebe67a26e24ee", aum = 0)
    /* renamed from: ct */
    private void m40833ct(int i) {
        throw new aWi(new aCE(this, anN, new Object[]{new Integer(i)}));
    }

    @C0064Am(aul = "382843538ecda58465d4f698591e7256", aum = 0)
    /* renamed from: cv */
    private void m40834cv(int i) {
        throw new aWi(new aCE(this, anP, new Object[]{new Integer(i)}));
    }

    /* renamed from: dp */
    private void m40835dp(long j) {
        bFf().mo5608dq().mo3184b(bHg, j);
    }

    /* renamed from: fv */
    private void m40836fv(int i) {
        bFf().mo5608dq().mo3183b(bHe, i);
    }

    @C0064Am(aul = "05ae629b2ae5c643f9a38b2ca66d724e", aum = 0)
    @C5566aOg
    /* renamed from: fw */
    private void m40837fw(int i) {
        throw new aWi(new aCE(this, bHp, new Object[]{new Integer(i)}));
    }

    @C0064Am(aul = "f204ecebd91c4492024e7716be889e09", aum = 0)
    /* renamed from: kd */
    private I18NString m40838kd() {
        throw new aWi(new aCE(this, f9516zT, new Object[0]));
    }

    @C0064Am(aul = "f8e545b077658da975a9740333d0c904", aum = 0)
    /* renamed from: sJ */
    private Asset m40841sJ() {
        throw new aWi(new aCE(this, f9514Ob, new Object[0]));
    }

    @C0064Am(aul = "8eb73cfa9554259c8ea28dc597d2d75d", aum = 0)
    /* renamed from: vV */
    private I18NString m40842vV() {
        throw new aWi(new aCE(this, f9515QE, new Object[0]));
    }

    /* renamed from: HC */
    public ItemTypeCategory mo19866HC() {
        switch (bFf().mo6893i(anw)) {
            case 0:
                return null;
            case 2:
                return (ItemTypeCategory) bFf().mo5606d(new aCE(this, anw, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, anw, new Object[0]));
                break;
        }
        return m40809HB();
    }

    /* renamed from: HE */
    public ProgressionAbility mo19867HE() {
        switch (bFf().mo6893i(any)) {
            case 0:
                return null;
            case 2:
                return (ProgressionAbility) bFf().mo5606d(new aCE(this, any, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, any, new Object[0]));
                break;
        }
        return m40810HD();
    }

    /* renamed from: HG */
    public long mo19868HG() {
        switch (bFf().mo6893i(anA)) {
            case 0:
                return 0;
            case 2:
                return ((Long) bFf().mo5606d(new aCE(this, anA, new Object[0]))).longValue();
            case 3:
                bFf().mo5606d(new aCE(this, anA, new Object[0]));
                break;
        }
        return m40811HF();
    }

    /* renamed from: HI */
    public long mo19869HI() {
        switch (bFf().mo6893i(anC)) {
            case 0:
                return 0;
            case 2:
                return ((Long) bFf().mo5606d(new aCE(this, anC, new Object[0]))).longValue();
            case 3:
                bFf().mo5606d(new aCE(this, anC, new Object[0]));
                break;
        }
        return m40812HH();
    }

    /* renamed from: HK */
    public long mo19870HK() {
        switch (bFf().mo6893i(anE)) {
            case 0:
                return 0;
            case 2:
                return ((Long) bFf().mo5606d(new aCE(this, anE, new Object[0]))).longValue();
            case 3:
                bFf().mo5606d(new aCE(this, anE, new Object[0]));
                break;
        }
        return m40813HJ();
    }

    /* renamed from: HM */
    public int mo19871HM() {
        switch (bFf().mo6893i(anG)) {
            case 0:
                return 0;
            case 2:
                return ((Integer) bFf().mo5606d(new aCE(this, anG, new Object[0]))).intValue();
            case 3:
                bFf().mo5606d(new aCE(this, anG, new Object[0]));
                break;
        }
        return m40814HL();
    }

    /* renamed from: HO */
    public int mo19872HO() {
        switch (bFf().mo6893i(anI)) {
            case 0:
                return 0;
            case 2:
                return ((Integer) bFf().mo5606d(new aCE(this, anI, new Object[0]))).intValue();
            case 3:
                bFf().mo5606d(new aCE(this, anI, new Object[0]));
                break;
        }
        return m40815HN();
    }

    /* renamed from: HQ */
    public int mo19873HQ() {
        switch (bFf().mo6893i(anK)) {
            case 0:
                return 0;
            case 2:
                return ((Integer) bFf().mo5606d(new aCE(this, anK, new Object[0]))).intValue();
            case 3:
                bFf().mo5606d(new aCE(this, anK, new Object[0]));
                break;
        }
        return m40816HP();
    }

    /* renamed from: HS */
    public int mo19874HS() {
        switch (bFf().mo6893i(anM)) {
            case 0:
                return 0;
            case 2:
                return ((Integer) bFf().mo5606d(new aCE(this, anM, new Object[0]))).intValue();
            case 3:
                bFf().mo5606d(new aCE(this, anM, new Object[0]));
                break;
        }
        return m40817HR();
    }

    /* renamed from: HU */
    public int mo19875HU() {
        switch (bFf().mo6893i(anO)) {
            case 0:
                return 0;
            case 2:
                return ((Integer) bFf().mo5606d(new aCE(this, anO, new Object[0]))).intValue();
            case 3:
                bFf().mo5606d(new aCE(this, anO, new Object[0]));
                break;
        }
        return m40818HT();
    }

    /* renamed from: Hy */
    public long mo19876Hy() {
        switch (bFf().mo6893i(ans)) {
            case 0:
                return 0;
            case 2:
                return ((Long) bFf().mo5606d(new aCE(this, ans, new Object[0]))).longValue();
            case 3:
                bFf().mo5606d(new aCE(this, ans, new Object[0]));
                break;
        }
        return m40819Hx();
    }

    /* renamed from: RU */
    public I18NString mo182RU() {
        switch (bFf().mo6893i(aMj)) {
            case 0:
                return null;
            case 2:
                return (I18NString) bFf().mo5606d(new aCE(this, aMj, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, aMj, new Object[0]));
                break;
        }
        return m40820RT();
    }

    /* renamed from: Sa */
    public String mo185Sa() {
        switch (bFf().mo6893i(aMp)) {
            case 0:
                return null;
            case 2:
                return (String) bFf().mo5606d(new aCE(this, aMp, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, aMp, new Object[0]));
                break;
        }
        return m40821RZ();
    }

    /* renamed from: T */
    public C6494any mo11T() {
        return ___iScriptClass;
    }

    /* renamed from: U */
    public C2491fm[] mo12U() {
        return _m_methods;
    }

    /* renamed from: W */
    public Object mo13W() {
        return new C5410aIg(this);
    }

    /* renamed from: a */
    public Object mo14a(C0495Gr gr) {
        Object[] args = gr.getArgs();
        switch (gr.mo2417hq() - BaseTaikodomContent._m_methodCount) {
            case 0:
                return m40809HB();
            case 1:
                return m40838kd();
            case 2:
                return new Long(m40819Hx());
            case 3:
                return m40824au();
            case 4:
                return m40810HD();
            case 5:
                return new Boolean(m40839l((Character) args[0]));
            case 6:
                return anK();
            case 7:
                return m40841sJ();
            case 8:
                return anM();
            case 9:
                return new Long(m40811HF());
            case 10:
                m40826bT(((Long) args[0]).longValue());
                return null;
            case 11:
                return new Long(m40812HH());
            case 12:
                return new Long(m40813HJ());
            case 13:
                m40827bV(((Long) args[0]).longValue());
                return null;
            case 14:
                m40828bX(((Long) args[0]).longValue());
                return null;
            case 15:
                return new Integer(m40814HL());
            case 16:
                m40830cn(((Integer) args[0]).intValue());
                return null;
            case 17:
                return new Integer(m40815HN());
            case 18:
                m40831cp(((Integer) args[0]).intValue());
                return null;
            case 19:
                return new Integer(m40816HP());
            case 20:
                m40832cr(((Integer) args[0]).intValue());
                return null;
            case 21:
                return new Integer(m40817HR());
            case 22:
                m40833ct(((Integer) args[0]).intValue());
                return null;
            case 23:
                return new Integer(m40818HT());
            case 24:
                m40834cv(((Integer) args[0]).intValue());
                return null;
            case 25:
                m40829c(((Long) args[0]).longValue(), ((Integer) args[1]).intValue());
                return null;
            case 26:
                anO();
                return null;
            case 27:
                adk();
                return null;
            case 28:
                return new Long(anQ());
            case 29:
                return new Boolean(anS());
            case 30:
                return new Integer(anU());
            case 31:
                m40837fw(((Integer) args[0]).intValue());
                return null;
            case 32:
                m40823aG();
                return null;
            case 33:
                return m40840rO();
            case 34:
                return m40821RZ();
            case 35:
                return m40842vV();
            case 36:
                return m40820RT();
            default:
                return super.mo14a(gr);
        }
    }

    /* renamed from: a */
    public void mo15a(Collection collection, C0495Gr gr) {
        super.mo15a(collection, gr);
    }

    /* renamed from: aH */
    public void mo70aH() {
        switch (bFf().mo6893i(_f_onResurrect_0020_0028_0029V)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, _f_onResurrect_0020_0028_0029V, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, _f_onResurrect_0020_0028_0029V, new Object[0]));
                break;
        }
        m40823aG();
    }

    public ProgressionAbility anL() {
        switch (bFf().mo6893i(bHi)) {
            case 0:
                return null;
            case 2:
                return (ProgressionAbility) bFf().mo5606d(new aCE(this, bHi, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, bHi, new Object[0]));
                break;
        }
        return anK();
    }

    public Asset anN() {
        switch (bFf().mo6893i(bHj)) {
            case 0:
                return null;
            case 2:
                return (Asset) bFf().mo5606d(new aCE(this, bHj, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, bHj, new Object[0]));
                break;
        }
        return anM();
    }

    @C5566aOg
    public void anP() {
        switch (bFf().mo6893i(bHl)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, bHl, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, bHl, new Object[0]));
                break;
        }
        anO();
    }

    @C5566aOg
    public long anR() {
        switch (bFf().mo6893i(bHm)) {
            case 0:
                return 0;
            case 2:
                return ((Long) bFf().mo5606d(new aCE(this, bHm, new Object[0]))).longValue();
            case 3:
                bFf().mo5606d(new aCE(this, bHm, new Object[0]));
                break;
        }
        return anQ();
    }

    @C5566aOg
    public boolean anT() {
        switch (bFf().mo6893i(bHn)) {
            case 0:
                return false;
            case 2:
                return ((Boolean) bFf().mo5606d(new aCE(this, bHn, new Object[0]))).booleanValue();
            case 3:
                bFf().mo5606d(new aCE(this, bHn, new Object[0]));
                break;
        }
        return anS();
    }

    @C5566aOg
    public int anV() {
        switch (bFf().mo6893i(bHo)) {
            case 0:
                return 0;
            case 2:
                return ((Integer) bFf().mo5606d(new aCE(this, bHo, new Object[0]))).intValue();
            case 3:
                bFf().mo5606d(new aCE(this, bHo, new Object[0]));
                break;
        }
        return anU();
    }

    /* renamed from: bU */
    public void mo19879bU(long j) {
        switch (bFf().mo6893i(anB)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, anB, new Object[]{new Long(j)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, anB, new Object[]{new Long(j)}));
                break;
        }
        m40826bT(j);
    }

    /* renamed from: bW */
    public void mo19880bW(long j) {
        switch (bFf().mo6893i(anD)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, anD, new Object[]{new Long(j)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, anD, new Object[]{new Long(j)}));
                break;
        }
        m40827bV(j);
    }

    /* renamed from: bY */
    public void mo19881bY(long j) {
        switch (bFf().mo6893i(anF)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, anF, new Object[]{new Long(j)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, anF, new Object[]{new Long(j)}));
                break;
        }
        m40828bX(j);
    }

    /* renamed from: c */
    public C5663aRz[] mo25c() {
        return _m_fields;
    }

    /* renamed from: co */
    public void mo19883co(int i) {
        switch (bFf().mo6893i(anH)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, anH, new Object[]{new Integer(i)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, anH, new Object[]{new Integer(i)}));
                break;
        }
        m40830cn(i);
    }

    /* renamed from: cq */
    public void mo19884cq(int i) {
        switch (bFf().mo6893i(anJ)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, anJ, new Object[]{new Integer(i)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, anJ, new Object[]{new Integer(i)}));
                break;
        }
        m40831cp(i);
    }

    /* renamed from: cs */
    public void mo19885cs(int i) {
        switch (bFf().mo6893i(anL)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, anL, new Object[]{new Integer(i)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, anL, new Object[]{new Integer(i)}));
                break;
        }
        m40832cr(i);
    }

    /* renamed from: cu */
    public void mo19886cu(int i) {
        switch (bFf().mo6893i(anN)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, anN, new Object[]{new Integer(i)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, anN, new Object[]{new Integer(i)}));
                break;
        }
        m40833ct(i);
    }

    /* renamed from: cw */
    public void mo19887cw(int i) {
        switch (bFf().mo6893i(anP)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, anP, new Object[]{new Integer(i)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, anP, new Object[]{new Integer(i)}));
                break;
        }
        m40834cv(i);
    }

    @C5566aOg
    /* renamed from: d */
    public void mo22856d(long j, int i) {
        switch (bFf().mo6893i(bHk)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, bHk, new Object[]{new Long(j), new Integer(i)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, bHk, new Object[]{new Long(j), new Integer(i)}));
                break;
        }
        m40829c(j, i);
    }

    @C5566aOg
    /* renamed from: fx */
    public void mo22857fx(int i) {
        switch (bFf().mo6893i(bHp)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, bHp, new Object[]{new Integer(i)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, bHp, new Object[]{new Integer(i)}));
                break;
        }
        m40837fw(i);
    }

    /* renamed from: ke */
    public I18NString mo19891ke() {
        switch (bFf().mo6893i(f9516zT)) {
            case 0:
                return null;
            case 2:
                return (I18NString) bFf().mo5606d(new aCE(this, f9516zT, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f9516zT, new Object[0]));
                break;
        }
        return m40838kd();
    }

    /* renamed from: m */
    public boolean mo22858m(Character acx) {
        switch (bFf().mo6893i(bHh)) {
            case 0:
                return false;
            case 2:
                return ((Boolean) bFf().mo5606d(new aCE(this, bHh, new Object[]{acx}))).booleanValue();
            case 3:
                bFf().mo5606d(new aCE(this, bHh, new Object[]{acx}));
                break;
        }
        return m40839l(acx);
    }

    /* renamed from: rP */
    public I18NString mo195rP() {
        switch (bFf().mo6893i(f9513MN)) {
            case 0:
                return null;
            case 2:
                return (I18NString) bFf().mo5606d(new aCE(this, f9513MN, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f9513MN, new Object[0]));
                break;
        }
        return m40840rO();
    }

    @C5566aOg
    public void reset() {
        switch (bFf().mo6893i(bkF)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, bkF, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, bkF, new Object[0]));
                break;
        }
        adk();
    }

    /* renamed from: sK */
    public Asset mo12100sK() {
        switch (bFf().mo6893i(f9514Ob)) {
            case 0:
                return null;
            case 2:
                return (Asset) bFf().mo5606d(new aCE(this, f9514Ob, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f9514Ob, new Object[0]));
                break;
        }
        return m40841sJ();
    }

    public String toString() {
        switch (bFf().mo6893i(_f_toString_0020_0028_0029Ljava_002flang_002fString_003b)) {
            case 0:
                return null;
            case 2:
                return (String) bFf().mo5606d(new aCE(this, _f_toString_0020_0028_0029Ljava_002flang_002fString_003b, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, _f_toString_0020_0028_0029Ljava_002flang_002fString_003b, new Object[0]));
                break;
        }
        return m40824au();
    }

    /* renamed from: vW */
    public I18NString mo19893vW() {
        switch (bFf().mo6893i(f9515QE)) {
            case 0:
                return null;
            case 2:
                return (I18NString) bFf().mo5606d(new aCE(this, f9515QE, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f9515QE, new Object[0]));
                break;
        }
        return m40842vV();
    }

    /* renamed from: S */
    public void mo10S() {
        super.mo10S();
        m40825bF(false);
    }

    @C0064Am(aul = "560229ecbc18b972dd12f465c89c017b", aum = 0)
    /* renamed from: au */
    private String m40824au() {
        return "[" + mo19866HC() + "] " + mo19891ke();
    }

    @C0064Am(aul = "5f01d2ded047850ec5c83d62ac72a1ff", aum = 0)
    /* renamed from: l */
    private boolean m40839l(Character acx) {
        ProgressionAbility anL2 = anL();
        if (anL2 == null) {
            return true;
        }
        return acx.mo12659ly().mo20741n(anL2);
    }

    @C0064Am(aul = "a51b77a16e607d62283336157d177e56", aum = 0)
    private ProgressionAbility anK() {
        if (mo19867HE() != null) {
            return mo19867HE();
        }
        if (mo19866HC() == null) {
            return null;
        }
        return mo19866HC().anL();
    }

    @C0064Am(aul = "782e7a81b7ce5c561a1cc6a9a95eec9d", aum = 0)
    private Asset anM() {
        if (mo12100sK() != null || !bGX()) {
            return mo12100sK();
        }
        return ala().aJe().mo19018xt();
    }

    @C0064Am(aul = "6ce8aaac712cd291a920b2a2f87d7c6e", aum = 0)
    /* renamed from: aG */
    private void m40823aG() {
        super.mo70aH();
    }

    @C0064Am(aul = "c26ac5d5d64156f7f43abb6b389866bf", aum = 0)
    /* renamed from: rO */
    private I18NString m40840rO() {
        return mo19891ke();
    }

    @C0064Am(aul = "5c0d4dbfdc9fd059ee55c8a82985b309", aum = 0)
    /* renamed from: RZ */
    private String m40821RZ() {
        if (mo12100sK() != null) {
            return mo12100sK().getHandle();
        }
        return null;
    }

    @C0064Am(aul = "5d25648a9bb4c558c4a20f500a5921ea", aum = 0)
    /* renamed from: RT */
    private I18NString m40820RT() {
        return mo19893vW();
    }
}
