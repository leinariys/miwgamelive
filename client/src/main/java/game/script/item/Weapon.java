package game.script.item;

import com.hoplon.geometry.Color;
import com.hoplon.geometry.Vec3f;
import game.geometry.Matrix4fWrap;
import game.geometry.Quat4fWrap;
import game.geometry.Vec3d;
import game.network.message.externalizable.C1515WI;
import game.network.message.externalizable.aCE;
import game.network.message.serializable.C1556Wo;
import game.network.message.serializable.C6400amI;
import game.script.Actor;
import game.script.damage.DamageType;
import game.script.login.UserConnection;
import game.script.nls.NLSManager;
import game.script.nls.NLSWindowAlert;
import game.script.player.Player;
import game.script.resource.Asset;
import game.script.ship.Ship;
import game.script.space.Node;
import game.script.util.AdapterLikedList;
import game.script.util.GameObjectAdapter;
import logic.aaa.C0284Df;
import logic.aaa.C1506WA;
import logic.baa.*;
import logic.bbb.C4029yK;
import logic.bbb.C6348alI;
import logic.bbb.aDR;
import logic.data.link.C0057Af;
import logic.data.link.C3161oY;
import logic.data.mbean.C1716ZO;
import logic.data.mbean.C6338aky;
import logic.render.IEngineGraphics;
import logic.res.code.C5640aRc;
import logic.res.code.C5663aRz;
import logic.res.code.C6494any;
import logic.res.html.C2491fm;
import logic.thred.LogPrinter;
import org.apache.commons.logging.Log;
import p001a.*;
import taikodom.geom.Orientation;
import taikodom.render.scene.RPulsingLight;

import javax.vecmath.Vector3d;
import javax.vecmath.Vector3f;
import java.util.Collection;

@C5829abJ("2.1.1")
@C6485anp
@C2712iu(mo19786Bt = BaseWeaponType.class, version = "2.1.1")
@C5511aMd
/* renamed from: a.adv  reason: case insensitive filesystem */
/* compiled from: a */
public abstract class Weapon extends Component implements C6480ank<WeaponAdapter>, C6222aim, C3161oY.C3162a, C6872avM, C3161oY.C3162a {

    /* renamed from: DJ */
    public static final C2491fm f4343DJ = null;

    /* renamed from: DK */
    public static final C2491fm f4344DK = null;

    /* renamed from: Lm */
    public static final C2491fm f4345Lm = null;
    /* renamed from: _f_getOffloaders_0020_0028_0029Ljava_002futil_002fCollection_003b */
    public static final C2491fm f4346x99c43db3 = null;
    /* renamed from: _f_onObjectDisposed_0020_0028Ltaikodom_002finfra_002fscript_002fScriptObject_003b_0029V */
    public static final C2491fm f4347x13860637 = null;
    public static final C2491fm _f_onResurrect_0020_0028_0029V = null;
    public static final C2491fm _f_step_0020_0028F_0029V = null;
    public static final C2491fm _f_tick_0020_0028F_0029V = null;
    public static final int _m_fieldCount = 0;
    public static final C5663aRz[] _m_fields = null;
    public static final int _m_methodCount = 0;
    public static final C2491fm[] _m_methods = null;
    public static final C5663aRz aOx = null;
    public static final C2491fm aPe = null;
    public static final C2491fm aPf = null;
    public static final C2491fm aPj = null;
    public static final C2491fm aPk = null;
    public static final C2491fm aPl = null;
    public static final C2491fm aPm = null;
    public static final C5663aRz bEc = null;
    public static final C5663aRz bEk = null;
    public static final C2491fm bMg = null;
    public static final C2491fm bMh = null;
    public static final C2491fm bMi = null;
    public static final C2491fm bMm = null;
    public static final C2491fm bMo = null;
    public static final C2491fm bMp = null;
    public static final C2491fm bMq = null;
    public static final C2491fm bMr = null;
    public static final C2491fm bMs = null;
    public static final C2491fm ccM = null;
    public static final C2491fm clu = null;
    public static final C2491fm clv = null;
    public static final C2491fm clw = null;
    public static final C2491fm clz = null;
    public static final C5663aRz dGv = null;
    public static final C2491fm dmG = null;
    public static final C2491fm dmH = null;
    public static final C2491fm hAA = null;
    public static final C2491fm hAB = null;
    public static final C2491fm hAC = null;
    public static final C2491fm hAD = null;
    public static final C2491fm hAE = null;
    public static final C2491fm hAF = null;
    public static final C2491fm hAG = null;
    public static final C2491fm hAH = null;
    public static final C2491fm hAI = null;
    public static final C2491fm hAJ = null;
    public static final C2491fm hAK = null;
    public static final C2491fm hAL = null;
    public static final C2491fm hAM = null;
    public static final C2491fm hAN = null;
    public static final C2491fm hAO = null;
    public static final C2491fm hAP = null;
    public static final C2491fm hAQ = null;
    public static final C2491fm hAR = null;
    public static final C2491fm hAS = null;
    public static final C2491fm hAT = null;
    public static final C2491fm hAU = null;
    public static final C5663aRz hAb = null;
    public static final C5663aRz hAf = null;
    public static final C5663aRz hAg = null;
    public static final C5663aRz hAh = null;
    public static final C5663aRz hAi = null;
    public static final C5663aRz hAj = null;
    public static final C5663aRz hAk = null;
    public static final C5663aRz hAl = null;
    public static final C2491fm hAn = null;
    public static final C2491fm hAo = null;
    public static final C2491fm hAp = null;
    public static final C2491fm hAq = null;
    public static final C2491fm hAr = null;
    public static final C2491fm hAs = null;
    public static final C2491fm hAt = null;
    public static final C2491fm hAu = null;
    public static final C2491fm hAv = null;
    public static final C2491fm hAw = null;
    public static final C2491fm hAx = null;
    public static final C2491fm hAy = null;
    public static final C2491fm hAz = null;
    public static final C2491fm hix = null;
    public static final C2491fm hlM = null;
    public static final C5663aRz hzS = null;
    public static final C5663aRz hzT = null;
    public static final C5663aRz hzU = null;
    public static final C5663aRz hzV = null;
    public static final C5663aRz hzW = null;
    public static final C5663aRz hzX = null;
    public static final C5663aRz hzY = null;
    public static final C5663aRz hzZ = null;
    public static final long serialVersionUID = 0;
    /* renamed from: uK */
    public static final C5663aRz f4348uK = null;
    /* renamed from: uN */
    public static final C5663aRz f4349uN = null;
    /* renamed from: uS */
    public static final C5663aRz f4350uS = null;
    /* renamed from: uW */
    public static final C2491fm f4351uW = null;
    /* renamed from: vb */
    public static final C2491fm f4352vb = null;
    /* renamed from: vh */
    public static final C2491fm f4353vh = null;
    private static final Log logger = LogPrinter.setClass(Weapon.class);
    public static C6494any ___iScriptClass;
    @C0064Am(aul = "1908d4b18b30e236c9ca6f1a5795e80b", aum = 14)
    @C5566aOg
    private static AdapterLikedList<WeaponAdapter> aOw;
    @C0064Am(aul = "0b89e7fb588c235be6adbb41c7f0aaeb", aum = 3)
    private static boolean bEj;
    @C0064Am(aul = "ddf141510f9bf75a3a8512f4a7e75ebd", aum = 9)
    private static Asset bIA;
    @C0064Am(aul = "0f77233765171f81d767a4f3930124ab", aum = 10)
    private static Asset bIB;
    @C0064Am(aul = "e286423a89fb1077b0d3fda35f5ca650", aum = 11)
    private static Asset bIC;
    @C0064Am(aul = "5091d457eb48a785d3a009f97df3076f", aum = 12)
    private static Asset bID;
    @C0064Am(aul = "e5a94482480be8789b930b2c36e60215", aum = 0)
    private static boolean bIt;
    @C0064Am(aul = "a92452d908e30e61b1d210c51b3f1696", aum = 1)
    private static Asset bIu;
    @C0064Am(aul = "2663f34cccae299be98a837220849635", aum = 2)
    private static C1556Wo<DamageType, Float> bIv;
    @C0064Am(aul = "ea767002f442f7ddcdf724f334e954b3", aum = 4)
    private static float bIw;
    @C0064Am(aul = "ac5b0b6d70e10232649ad46a7c5f33ac", aum = 5)
    private static Asset bIx;
    @C0064Am(aul = "ab15e7fed836622bf82f723df88c2045", aum = 7)
    private static float bIy;
    @C0064Am(aul = "b55ccbc00627101f76f41d4cd2f019af", aum = 8)
    private static Asset bIz;
    @C0064Am(aul = "e66413102801c0cc17c24a2dcb7a92a6", aum = 20)
    private static boolean eQA;
    @C0064Am(aul = "8cfdbbd2a4d6ff4c5812ebd2989b15e4", aum = 21)
    private static float eQB;
    @C0064Am(aul = "3195b405f75626196fbf5fe9520e506b", aum = 22)
    private static float eQC;
    @C0064Am(aul = "d7af72c8346a750c70c9967bd932fde4", aum = 15)
    @C1020Ot(drz = C1020Ot.C1021a.NOT_PERSISTED)
    @C5256aCi
    private static boolean eQv;
    @C0064Am(aul = "874e0404fddbb8fd8cdaff40919fdb7a", aum = 16)
    @C1020Ot(drz = C1020Ot.C1021a.NOT_PERSISTED)
    @C5256aCi
    private static Actor eQw;
    @C0064Am(aul = "9b19e0c3d9c083a4c6c9e0429096bd21", aum = 17)
    private static C6661arJ eQx;
    @C0064Am(aul = "dc1126ae2f6fd703228d03f97618eb7a", aum = 18)
    private static C1556Wo<DamageType, Float> eQy;
    @C0064Am(aul = "f02240d9a0a665959ea7c26bb44b6e25", aum = 19)
    private static float eQz;
    @C0064Am(aul = "f1d744481799a93d56aa362e2a8b7bf1", aum = 6)
    private static float range;
    @C0064Am(aul = "28d193b4b41f7eeae3b52a7761c88ec7", aum = 13)
    private static float speed;

    static {
        m20960V();
    }

    private transient Orientation hAa;
    @ClientOnly
    private boolean hAc;
    private transient long hAd;
    @ClientOnly
    private transient RPulsingLight hAe;
    @ClientOnly
    private transient C3257pi hAm;
    private transient boolean initialized;

    public Weapon() {
        super((C5540aNg) null);
        super.mo10S();
    }

    public Weapon(C5540aNg ang) {
        super(ang);
    }

    public Weapon(WeaponType apt) {
        super((C5540aNg) null);
        super._m_script_init(apt);
    }

    /* renamed from: V */
    static void m20960V() {
        serialVersionUID = (long) 1;
        _m_fieldCount = Component._m_fieldCount + 23;
        _m_methodCount = Component._m_methodCount + 69;
        int i = Component._m_fieldCount;
        C5663aRz[] arzArr = new C5663aRz[(i + 23)];
        C5663aRz b = C5640aRc.m17844b(Weapon.class, "e5a94482480be8789b930b2c36e60215", i);
        hzS = b;
        arzArr[i] = b;
        int i2 = i + 1;
        C5663aRz b2 = C5640aRc.m17844b(Weapon.class, "a92452d908e30e61b1d210c51b3f1696", i2);
        dGv = b2;
        arzArr[i2] = b2;
        int i3 = i2 + 1;
        C5663aRz b3 = C5640aRc.m17844b(Weapon.class, "2663f34cccae299be98a837220849635", i3);
        f4348uK = b3;
        arzArr[i3] = b3;
        int i4 = i3 + 1;
        C5663aRz b4 = C5640aRc.m17844b(Weapon.class, "0b89e7fb588c235be6adbb41c7f0aaeb", i4);
        bEk = b4;
        arzArr[i4] = b4;
        int i5 = i4 + 1;
        C5663aRz b5 = C5640aRc.m17844b(Weapon.class, "ea767002f442f7ddcdf724f334e954b3", i5);
        bEc = b5;
        arzArr[i5] = b5;
        int i6 = i5 + 1;
        C5663aRz b6 = C5640aRc.m17844b(Weapon.class, "ac5b0b6d70e10232649ad46a7c5f33ac", i6);
        hzT = b6;
        arzArr[i6] = b6;
        int i7 = i6 + 1;
        C5663aRz b7 = C5640aRc.m17844b(Weapon.class, "f1d744481799a93d56aa362e2a8b7bf1", i7);
        f4349uN = b7;
        arzArr[i7] = b7;
        int i8 = i7 + 1;
        C5663aRz b8 = C5640aRc.m17844b(Weapon.class, "ab15e7fed836622bf82f723df88c2045", i8);
        hzU = b8;
        arzArr[i8] = b8;
        int i9 = i8 + 1;
        C5663aRz b9 = C5640aRc.m17844b(Weapon.class, "b55ccbc00627101f76f41d4cd2f019af", i9);
        hzV = b9;
        arzArr[i9] = b9;
        int i10 = i9 + 1;
        C5663aRz b10 = C5640aRc.m17844b(Weapon.class, "ddf141510f9bf75a3a8512f4a7e75ebd", i10);
        hzW = b10;
        arzArr[i10] = b10;
        int i11 = i10 + 1;
        C5663aRz b11 = C5640aRc.m17844b(Weapon.class, "0f77233765171f81d767a4f3930124ab", i11);
        hzX = b11;
        arzArr[i11] = b11;
        int i12 = i11 + 1;
        C5663aRz b12 = C5640aRc.m17844b(Weapon.class, "e286423a89fb1077b0d3fda35f5ca650", i12);
        hzY = b12;
        arzArr[i12] = b12;
        int i13 = i12 + 1;
        C5663aRz b13 = C5640aRc.m17844b(Weapon.class, "5091d457eb48a785d3a009f97df3076f", i13);
        hzZ = b13;
        arzArr[i13] = b13;
        int i14 = i13 + 1;
        C5663aRz b14 = C5640aRc.m17844b(Weapon.class, "28d193b4b41f7eeae3b52a7761c88ec7", i14);
        f4350uS = b14;
        arzArr[i14] = b14;
        int i15 = i14 + 1;
        C5663aRz b15 = C5640aRc.m17844b(Weapon.class, "1908d4b18b30e236c9ca6f1a5795e80b", i15);
        aOx = b15;
        arzArr[i15] = b15;
        int i16 = i15 + 1;
        C5663aRz b16 = C5640aRc.m17844b(Weapon.class, "d7af72c8346a750c70c9967bd932fde4", i16);
        hAb = b16;
        arzArr[i16] = b16;
        int i17 = i16 + 1;
        C5663aRz b17 = C5640aRc.m17844b(Weapon.class, "874e0404fddbb8fd8cdaff40919fdb7a", i17);
        hAf = b17;
        arzArr[i17] = b17;
        int i18 = i17 + 1;
        C5663aRz b18 = C5640aRc.m17844b(Weapon.class, "9b19e0c3d9c083a4c6c9e0429096bd21", i18);
        hAg = b18;
        arzArr[i18] = b18;
        int i19 = i18 + 1;
        C5663aRz b19 = C5640aRc.m17844b(Weapon.class, "dc1126ae2f6fd703228d03f97618eb7a", i19);
        hAh = b19;
        arzArr[i19] = b19;
        int i20 = i19 + 1;
        C5663aRz b20 = C5640aRc.m17844b(Weapon.class, "f02240d9a0a665959ea7c26bb44b6e25", i20);
        hAi = b20;
        arzArr[i20] = b20;
        int i21 = i20 + 1;
        C5663aRz b21 = C5640aRc.m17844b(Weapon.class, "e66413102801c0cc17c24a2dcb7a92a6", i21);
        hAj = b21;
        arzArr[i21] = b21;
        int i22 = i21 + 1;
        C5663aRz b22 = C5640aRc.m17844b(Weapon.class, "8cfdbbd2a4d6ff4c5812ebd2989b15e4", i22);
        hAk = b22;
        arzArr[i22] = b22;
        int i23 = i22 + 1;
        C5663aRz b23 = C5640aRc.m17844b(Weapon.class, "3195b405f75626196fbf5fe9520e506b", i23);
        hAl = b23;
        arzArr[i23] = b23;
        int i24 = i23 + 1;
        _m_fields = arzArr;
        C1634Xv.m11725a((Object[]) Component._m_fields, (Object[]) _m_fields);
        int i25 = Component._m_methodCount;
        C2491fm[] fmVarArr = new C2491fm[(i25 + 69)];
        C2491fm a = C4105zY.m41624a(Weapon.class, "0979729a59814649513d6f57887b5ea7", i25);
        bMh = a;
        fmVarArr[i25] = a;
        int i26 = i25 + 1;
        C2491fm a2 = C4105zY.m41624a(Weapon.class, C0057Af.cdf, i26);
        hAn = a2;
        fmVarArr[i26] = a2;
        int i27 = i26 + 1;
        C2491fm a3 = C4105zY.m41624a(Weapon.class, C0057Af.aME, i27);
        dmG = a3;
        fmVarArr[i27] = a3;
        int i28 = i27 + 1;
        C2491fm a4 = C4105zY.m41624a(Weapon.class, "bf8ebe7f288045bd2b353448bbfea9f7", i28);
        hAo = a4;
        fmVarArr[i28] = a4;
        int i29 = i28 + 1;
        C2491fm a5 = C4105zY.m41624a(Weapon.class, "eb0652369178dbd6615344356a84a69d", i29);
        aPe = a5;
        fmVarArr[i29] = a5;
        int i30 = i29 + 1;
        C2491fm a6 = C4105zY.m41624a(Weapon.class, "558846ee6438c03dceeffa8ae5529c83", i30);
        hAp = a6;
        fmVarArr[i30] = a6;
        int i31 = i30 + 1;
        C2491fm a7 = C4105zY.m41624a(Weapon.class, "578fdadfc18dbd5d6942a0895b1683f5", i31);
        clv = a7;
        fmVarArr[i31] = a7;
        int i32 = i31 + 1;
        C2491fm a8 = C4105zY.m41624a(Weapon.class, "aa5981f15dedce64c6debf8de9066839", i32);
        hAq = a8;
        fmVarArr[i32] = a8;
        int i33 = i32 + 1;
        C2491fm a9 = C4105zY.m41624a(Weapon.class, "c653b9a436426826aa9eede789fcbbaf", i33);
        hAr = a9;
        fmVarArr[i33] = a9;
        int i34 = i33 + 1;
        C2491fm a10 = C4105zY.m41624a(Weapon.class, "f0f6025c34be707dd1490233866c9e64", i34);
        aPf = a10;
        fmVarArr[i34] = a10;
        int i35 = i34 + 1;
        C2491fm a11 = C4105zY.m41624a(Weapon.class, "92b17cc8963fabacc33d096306447ef2", i35);
        bMg = a11;
        fmVarArr[i35] = a11;
        int i36 = i35 + 1;
        C2491fm a12 = C4105zY.m41624a(Weapon.class, "d9ec79401f015fe97f14e727bc2eee42", i36);
        bMp = a12;
        fmVarArr[i36] = a12;
        int i37 = i36 + 1;
        C2491fm a13 = C4105zY.m41624a(Weapon.class, "90f584161eab7e76fe9c925efbd9a30f", i37);
        bMq = a13;
        fmVarArr[i37] = a13;
        int i38 = i37 + 1;
        C2491fm a14 = C4105zY.m41624a(Weapon.class, "9a404f57deaf7677d2319f9f9e465cc3", i38);
        bMr = a14;
        fmVarArr[i38] = a14;
        int i39 = i38 + 1;
        C2491fm a15 = C4105zY.m41624a(Weapon.class, "05b2b0113a79f2131bf3ab409e3b142a", i39);
        hAs = a15;
        fmVarArr[i39] = a15;
        int i40 = i39 + 1;
        C2491fm a16 = C4105zY.m41624a(Weapon.class, "930896f148ccdb769db19c991ee1892d", i40);
        clu = a16;
        fmVarArr[i40] = a16;
        int i41 = i40 + 1;
        C2491fm a17 = C4105zY.m41624a(Weapon.class, "de8bfa79b14da75c1dc3b785bbf2725e", i41);
        f4351uW = a17;
        fmVarArr[i41] = a17;
        int i42 = i41 + 1;
        C2491fm a18 = C4105zY.m41624a(Weapon.class, "5056969d47141a5e61d181f898663564", i42);
        hAt = a18;
        fmVarArr[i42] = a18;
        int i43 = i42 + 1;
        C2491fm a19 = C4105zY.m41624a(Weapon.class, "4598294bd1bf5f811f12706b20f1faa9", i43);
        hAu = a19;
        fmVarArr[i43] = a19;
        int i44 = i43 + 1;
        C2491fm a20 = C4105zY.m41624a(Weapon.class, "619ec9b3f1ef97208a6b51f300c681e3", i44);
        clz = a20;
        fmVarArr[i44] = a20;
        int i45 = i44 + 1;
        C2491fm a21 = C4105zY.m41624a(Weapon.class, "c00f934d5d5d99c663e93f2f4da1b4c1", i45);
        f4343DJ = a21;
        fmVarArr[i45] = a21;
        int i46 = i45 + 1;
        C2491fm a22 = C4105zY.m41624a(Weapon.class, "5031ed492c3ef2f2fd102cec33622e67", i46);
        hAv = a22;
        fmVarArr[i46] = a22;
        int i47 = i46 + 1;
        C2491fm a23 = C4105zY.m41624a(Weapon.class, "b733bc72ed095043fa37c87607ae6d97", i47);
        hAw = a23;
        fmVarArr[i47] = a23;
        int i48 = i47 + 1;
        C2491fm a24 = C4105zY.m41624a(Weapon.class, "fff7504a84d2a7fcaf09bb739a2715bb", i48);
        hAx = a24;
        fmVarArr[i48] = a24;
        int i49 = i48 + 1;
        C2491fm a25 = C4105zY.m41624a(Weapon.class, "13653d3818c7da9e0c14bfd81f296314", i49);
        f4352vb = a25;
        fmVarArr[i49] = a25;
        int i50 = i49 + 1;
        C2491fm a26 = C4105zY.m41624a(Weapon.class, "98f5b461a507a0b3194d64564aa89ecd", i50);
        bMm = a26;
        fmVarArr[i50] = a26;
        int i51 = i50 + 1;
        C2491fm a27 = C4105zY.m41624a(Weapon.class, "4d6a2659b92315948b5e865bedcc0694", i51);
        bMo = a27;
        fmVarArr[i51] = a27;
        int i52 = i51 + 1;
        C2491fm a28 = C4105zY.m41624a(Weapon.class, "181511b14b6abbdf5ff210813d7c8b64", i52);
        f4353vh = a28;
        fmVarArr[i52] = a28;
        int i53 = i52 + 1;
        C2491fm a29 = C4105zY.m41624a(Weapon.class, "17b3b59f43706456d20d36d455cbfb41", i53);
        hAy = a29;
        fmVarArr[i53] = a29;
        int i54 = i53 + 1;
        C2491fm a30 = C4105zY.m41624a(Weapon.class, "b6bbd23eec0ea832de059f8e9afb2a69", i54);
        hAz = a30;
        fmVarArr[i54] = a30;
        int i55 = i54 + 1;
        C2491fm a31 = C4105zY.m41624a(Weapon.class, "04f89db697ff9494211d589d465ac606", i55);
        bMs = a31;
        fmVarArr[i55] = a31;
        int i56 = i55 + 1;
        C2491fm a32 = C4105zY.m41624a(Weapon.class, "7d86853aa8b06610a86172f53ecd4da5", i56);
        hAA = a32;
        fmVarArr[i56] = a32;
        int i57 = i56 + 1;
        C2491fm a33 = C4105zY.m41624a(Weapon.class, "b8608b3a8d16aa6a2b6f0b18f0ae2e70", i57);
        hAB = a33;
        fmVarArr[i57] = a33;
        int i58 = i57 + 1;
        C2491fm a34 = C4105zY.m41624a(Weapon.class, "444f5aad329fb2ce322b954f505e9b4d", i58);
        hAC = a34;
        fmVarArr[i58] = a34;
        int i59 = i58 + 1;
        C2491fm a35 = C4105zY.m41624a(Weapon.class, "66b7e6b179ccc6ed8f76dead69771988", i59);
        hAD = a35;
        fmVarArr[i59] = a35;
        int i60 = i59 + 1;
        C2491fm a36 = C4105zY.m41624a(Weapon.class, "7c0a07d190bcf7ee314acf772cf9e4aa", i60);
        hAE = a36;
        fmVarArr[i60] = a36;
        int i61 = i60 + 1;
        C2491fm a37 = C4105zY.m41624a(Weapon.class, "eda4f40a9c0f7811a4849551c65d5c08", i61);
        hAF = a37;
        fmVarArr[i61] = a37;
        int i62 = i61 + 1;
        C2491fm a38 = C4105zY.m41624a(Weapon.class, "00ec65bc1484320a2cb45d7134946376", i62);
        hAG = a38;
        fmVarArr[i62] = a38;
        int i63 = i62 + 1;
        C2491fm a39 = C4105zY.m41624a(Weapon.class, "273895a5acf8679628ed6343f2d6b003", i63);
        hAH = a39;
        fmVarArr[i63] = a39;
        int i64 = i63 + 1;
        C2491fm a40 = C4105zY.m41624a(Weapon.class, "5ec5103c9ba1d113e05e2e261c403a02", i64);
        f4344DK = a40;
        fmVarArr[i64] = a40;
        int i65 = i64 + 1;
        C2491fm a41 = C4105zY.m41624a(Weapon.class, "21f7a36a086369452f58d12ed013a645", i65);
        hAI = a41;
        fmVarArr[i65] = a41;
        int i66 = i65 + 1;
        C2491fm a42 = C4105zY.m41624a(Weapon.class, "141543a59799e804b4d86dec69565322", i66);
        ccM = a42;
        fmVarArr[i66] = a42;
        int i67 = i66 + 1;
        C2491fm a43 = C4105zY.m41624a(Weapon.class, "f5f3baed3f7647db5fa7bc0a1b51896a", i67);
        _f_onResurrect_0020_0028_0029V = a43;
        fmVarArr[i67] = a43;
        int i68 = i67 + 1;
        C2491fm a44 = C4105zY.m41624a(Weapon.class, "f6fcd1deff89a5bb4490330d57de85bf", i68);
        f4345Lm = a44;
        fmVarArr[i68] = a44;
        int i69 = i68 + 1;
        C2491fm a45 = C4105zY.m41624a(Weapon.class, C0057Af.bbr, i69);
        hlM = a45;
        fmVarArr[i69] = a45;
        int i70 = i69 + 1;
        C2491fm a46 = C4105zY.m41624a(Weapon.class, "e0fdd05b6b087516a0e0acddfeb369d0", i70);
        bMi = a46;
        fmVarArr[i70] = a46;
        int i71 = i70 + 1;
        C2491fm a47 = C4105zY.m41624a(Weapon.class, "240dcfe40821aa82d55cc0822b833307", i71);
        hAJ = a47;
        fmVarArr[i71] = a47;
        int i72 = i71 + 1;
        C2491fm a48 = C4105zY.m41624a(Weapon.class, "62d63c8b97c846d6225f43f38c941e03", i72);
        hAK = a48;
        fmVarArr[i72] = a48;
        int i73 = i72 + 1;
        C2491fm a49 = C4105zY.m41624a(Weapon.class, "0eff4b931b2a211deeffa9978f7e0bcd", i73);
        hAL = a49;
        fmVarArr[i73] = a49;
        int i74 = i73 + 1;
        C2491fm a50 = C4105zY.m41624a(Weapon.class, "bedca0a992475c9a2bf0b04c461aca19", i74);
        hAM = a50;
        fmVarArr[i74] = a50;
        int i75 = i74 + 1;
        C2491fm a51 = C4105zY.m41624a(Weapon.class, "e37edaf461f7c8ba097178fd9ff80b48", i75);
        hAN = a51;
        fmVarArr[i75] = a51;
        int i76 = i75 + 1;
        C2491fm a52 = C4105zY.m41624a(Weapon.class, "67059caecd71693e8f9e3390a2914196", i76);
        hAO = a52;
        fmVarArr[i76] = a52;
        int i77 = i76 + 1;
        C2491fm a53 = C4105zY.m41624a(Weapon.class, "243af9a50cc9e7166666d754729f8122", i77);
        hAP = a53;
        fmVarArr[i77] = a53;
        int i78 = i77 + 1;
        C2491fm a54 = C4105zY.m41624a(Weapon.class, "31c33bdf5055e76c4f12cc41d2cda74c", i78);
        hAQ = a54;
        fmVarArr[i78] = a54;
        int i79 = i78 + 1;
        C2491fm a55 = C4105zY.m41624a(Weapon.class, "be03e5a0c445976daa2e7c8b159967bf", i79);
        hAR = a55;
        fmVarArr[i79] = a55;
        int i80 = i79 + 1;
        C2491fm a56 = C4105zY.m41624a(Weapon.class, "042d95b2266f50269f1f764182a77aa7", i80);
        hAS = a56;
        fmVarArr[i80] = a56;
        int i81 = i80 + 1;
        C2491fm a57 = C4105zY.m41624a(Weapon.class, "2f3a51421b09723f0d467539e03728c4", i81);
        clw = a57;
        fmVarArr[i81] = a57;
        int i82 = i81 + 1;
        C2491fm a58 = C4105zY.m41624a(Weapon.class, "cd3a8867cf7480b4947582e52428b33a", i82);
        hAT = a58;
        fmVarArr[i82] = a58;
        int i83 = i82 + 1;
        C2491fm a59 = C4105zY.m41624a(Weapon.class, "8dbd2143db7d166337d8f5d5f8a07461", i83);
        dmH = a59;
        fmVarArr[i83] = a59;
        int i84 = i83 + 1;
        C2491fm a60 = C4105zY.m41624a(Weapon.class, "211440bea97a7ad44ac7ac00e342bf6c", i84);
        _f_tick_0020_0028F_0029V = a60;
        fmVarArr[i84] = a60;
        int i85 = i84 + 1;
        C2491fm a61 = C4105zY.m41624a(Weapon.class, "0ea5a7cf943f163ce6eec4f464725bab", i85);
        hAU = a61;
        fmVarArr[i85] = a61;
        int i86 = i85 + 1;
        C2491fm a62 = C4105zY.m41624a(Weapon.class, "519366e581db0984b4521e07ceff6861", i86);
        _f_step_0020_0028F_0029V = a62;
        fmVarArr[i86] = a62;
        int i87 = i86 + 1;
        C2491fm a63 = C4105zY.m41624a(Weapon.class, "41bc0a59830f48f99b3bcf97a533c713", i87);
        f4346x99c43db3 = a63;
        fmVarArr[i87] = a63;
        int i88 = i87 + 1;
        C2491fm a64 = C4105zY.m41624a(Weapon.class, "c647250f4a1c71d13bece058871a6766", i88);
        f4347x13860637 = a64;
        fmVarArr[i88] = a64;
        int i89 = i88 + 1;
        C2491fm a65 = C4105zY.m41624a(Weapon.class, "228a2f63b933fb3b0f2c345ea886b0d9", i89);
        aPl = a65;
        fmVarArr[i89] = a65;
        int i90 = i89 + 1;
        C2491fm a66 = C4105zY.m41624a(Weapon.class, "f9ee9c24aa18785aa332fcd3543ad26e", i90);
        aPj = a66;
        fmVarArr[i90] = a66;
        int i91 = i90 + 1;
        C2491fm a67 = C4105zY.m41624a(Weapon.class, "45a03d70d237ce971f0591d0d5544061", i91);
        aPk = a67;
        fmVarArr[i91] = a67;
        int i92 = i91 + 1;
        C2491fm a68 = C4105zY.m41624a(Weapon.class, "84c576b13fb3738f10932b8eb60ac289", i92);
        aPm = a68;
        fmVarArr[i92] = a68;
        int i93 = i92 + 1;
        C2491fm a69 = C4105zY.m41624a(Weapon.class, "9d26c29fcb8e792e0f20c21356cd0206", i93);
        hix = a69;
        fmVarArr[i93] = a69;
        int i94 = i93 + 1;
        _m_methods = fmVarArr;
        C1634Xv.m11725a((Object[]) Component._m_methods, (Object[]) _m_methods);
        ___iScriptClass = aUO.m18566a(Weapon.class, C1716ZO.class, _m_fields, _m_methods);
    }

    /* renamed from: G */
    private void m20948G(float f) {
        throw new C6039afL();
    }

    /* renamed from: H */
    private void m20949H(float f) {
        throw new C6039afL();
    }

    /* renamed from: S */
    private void m20950S(C1556Wo wo) {
        throw new C6039afL();
    }

    @C0064Am(aul = "eb0652369178dbd6615344356a84a69d", aum = 0)
    @C5566aOg
    /* renamed from: TG */
    private void m20951TG() {
        throw new aWi(new aCE(this, aPe, new Object[0]));
    }

    @C5566aOg
    /* renamed from: TH */
    private void m20952TH() {
        switch (bFf().mo6893i(aPe)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, aPe, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, aPe, new Object[0]));
                break;
        }
        m20951TG();
    }

    @C0064Am(aul = "f0f6025c34be707dd1490233866c9e64", aum = 0)
    @C5566aOg
    /* renamed from: TI */
    private AdapterLikedList<WeaponAdapter> m20953TI() {
        throw new aWi(new aCE(this, aPf, new Object[0]));
    }

    /* renamed from: Te */
    private AdapterLikedList m20958Te() {
        return (AdapterLikedList) bFf().mo5608dq().mo3214p(aOx);
    }

    /* renamed from: a */
    private void m20961a(C6661arJ arj) {
        bFf().mo5608dq().mo3197f(hAg, arj);
    }

    /* renamed from: a */
    private void m20963a(AdapterLikedList zAVar) {
        bFf().mo5608dq().mo3197f(aOx, zAVar);
    }

    /* renamed from: aJ */
    private void m20965aJ(Asset tCVar) {
        throw new C6039afL();
    }

    /* renamed from: ad */
    private void m20966ad(C1556Wo wo) {
        bFf().mo5608dq().mo3197f(hAh, wo);
    }

    private boolean amJ() {
        return ((WeaponType) getType()).aGI();
    }

    @C0064Am(aul = "92b17cc8963fabacc33d096306447ef2", aum = 0)
    private int aqg() {
        throw new aWi(new aCE(this, bMg, new Object[0]));
    }

    @C0064Am(aul = "0979729a59814649513d6f57887b5ea7", aum = 0)
    private int aqi() {
        throw new aWi(new aCE(this, bMh, new Object[0]));
    }

    @C0064Am(aul = "4d6a2659b92315948b5e865bedcc0694", aum = 0)
    private String aqq() {
        throw new aWi(new aCE(this, bMo, new Object[0]));
    }

    @C0064Am(aul = "d9ec79401f015fe97f14e727bc2eee42", aum = 0)
    private String aqs() {
        throw new aWi(new aCE(this, bMp, new Object[0]));
    }

    @C0064Am(aul = "90f584161eab7e76fe9c925efbd9a30f", aum = 0)
    private String aqu() {
        throw new aWi(new aCE(this, bMq, new Object[0]));
    }

    @C0064Am(aul = "9a404f57deaf7677d2319f9f9e465cc3", aum = 0)
    private String aqw() {
        throw new aWi(new aCE(this, bMr, new Object[0]));
    }

    private C1556Wo bAH() {
        return ((WeaponType) getType()).bpA();
    }

    /* renamed from: bC */
    private void m20971bC(boolean z) {
        throw new C6039afL();
    }

    /* renamed from: bS */
    private void m20972bS(Asset tCVar) {
        throw new C6039afL();
    }

    /* renamed from: bT */
    private void m20973bT(Asset tCVar) {
        throw new C6039afL();
    }

    /* renamed from: bU */
    private void m20974bU(Asset tCVar) {
        throw new C6039afL();
    }

    /* renamed from: bV */
    private void m20975bV(Asset tCVar) {
        throw new C6039afL();
    }

    /* renamed from: bW */
    private void m20976bW(Asset tCVar) {
        throw new C6039afL();
    }

    /* renamed from: bX */
    private void m20977bX(Asset tCVar) {
        throw new C6039afL();
    }

    /* renamed from: bY */
    private void m20978bY(Actor cr) {
        bFf().mo5608dq().mo3197f(hAf, cr);
    }

    private Asset bjR() {
        return ((WeaponType) getType()).bjU();
    }

    @C0064Am(aul = "141543a59799e804b4d86dec69565322", aum = 0)
    @C5566aOg
    /* renamed from: c */
    private void m20980c(ItemLocation aag) {
        throw new aWi(new aCE(this, ccM, new Object[]{aag}));
    }

    @C0064Am(aul = "9d26c29fcb8e792e0f20c21356cd0206", aum = 0)
    private ComponentType cJR() {
        return aqz();
    }

    private boolean cTF() {
        return ((WeaponType) getType()).doL();
    }

    private float cTG() {
        return ((WeaponType) getType()).mo10793ml();
    }

    private Asset cTH() {
        return ((WeaponType) getType()).doN();
    }

    private float cTI() {
        return ((WeaponType) getType()).doP();
    }

    private Asset cTJ() {
        return ((WeaponType) getType()).doR();
    }

    private Asset cTK() {
        return ((WeaponType) getType()).doT();
    }

    private Asset cTL() {
        return ((WeaponType) getType()).doV();
    }

    private Asset cTM() {
        return ((WeaponType) getType()).doX();
    }

    private Asset cTN() {
        return ((WeaponType) getType()).doZ();
    }

    private boolean cTO() {
        return bFf().mo5608dq().mo3201h(hAb);
    }

    private Actor cTP() {
        return (Actor) bFf().mo5608dq().mo3214p(hAf);
    }

    private C6661arJ cTQ() {
        return (C6661arJ) bFf().mo5608dq().mo3214p(hAg);
    }

    private C1556Wo cTR() {
        return (C1556Wo) bFf().mo5608dq().mo3214p(hAh);
    }

    private float cTS() {
        return bFf().mo5608dq().mo3211m(hAi);
    }

    private boolean cTT() {
        return bFf().mo5608dq().mo3201h(hAj);
    }

    private float cTU() {
        return bFf().mo5608dq().mo3211m(hAk);
    }

    private float cTV() {
        return bFf().mo5608dq().mo3211m(hAl);
    }

    private Vec3d cUb() {
        switch (bFf().mo6893i(hAu)) {
            case 0:
                return null;
            case 2:
                return (Vec3d) bFf().mo5606d(new aCE(this, hAu, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, hAu, new Object[0]));
                break;
        }
        return cUa();
    }

    @C0064Am(aul = "e0fdd05b6b087516a0e0acddfeb369d0", aum = 0)
    /* renamed from: fE */
    private void m20984fE(int i) {
        throw new aWi(new aCE(this, bMi, new Object[]{new Integer(i)}));
    }

    /* renamed from: iN */
    private void m20987iN(boolean z) {
        throw new C6039afL();
    }

    /* renamed from: iO */
    private void m20988iO(boolean z) {
        bFf().mo5608dq().mo3153a(hAb, z);
    }

    /* renamed from: iP */
    private void m20989iP(boolean z) {
        bFf().mo5608dq().mo3153a(hAj, z);
    }

    @C4034yP
    @C5566aOg
    @C0064Am(aul = "0eff4b931b2a211deeffa9978f7e0bcd", aum = 0)
    @C2499fr(mo18857qh = 2)
    /* renamed from: iS */
    private void m20991iS(boolean z) {
        throw new aWi(new aCE(this, hAL, new Object[]{new Boolean(z)}));
    }

    @C4034yP
    @C5566aOg
    @C2499fr(mo18857qh = 2)
    /* renamed from: iT */
    private void m20992iT(boolean z) {
        switch (bFf().mo6893i(hAL)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, hAL, new Object[]{new Boolean(z)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, hAL, new Object[]{new Boolean(z)}));
                break;
        }
        m20991iS(z);
    }

    /* renamed from: iV */
    private void m20994iV(boolean z) {
        switch (bFf().mo6893i(hAM)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, hAM, new Object[]{new Boolean(z)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, hAM, new Object[]{new Boolean(z)}));
                break;
        }
        m20993iU(z);
    }

    /* renamed from: if */
    private float m20995if() {
        return ((WeaponType) getType()).mo10790in();
    }

    /* renamed from: ii */
    private float m20996ii() {
        return ((WeaponType) getType()).getSpeed();
    }

    @C0064Am(aul = "de8bfa79b14da75c1dc3b785bbf2725e", aum = 0)
    /* renamed from: ik */
    private C5260aCm m20997ik() {
        throw new aWi(new aCE(this, f4351uW, new Object[0]));
    }

    @C4034yP
    @C5566aOg
    @C0064Am(aul = "67059caecd71693e8f9e3390a2914196", aum = 0)
    @C2499fr
    /* renamed from: l */
    private void m21002l(Orientation orientation) {
        throw new aWi(new aCE(this, hAO, new Object[]{orientation}));
    }

    /* renamed from: mn */
    private void m21006mn(float f) {
        throw new C6039afL();
    }

    /* renamed from: mo */
    private void m21007mo(float f) {
        throw new C6039afL();
    }

    /* renamed from: mp */
    private void m21008mp(float f) {
        bFf().mo5608dq().mo3150a(hAi, f);
    }

    /* renamed from: mq */
    private void m21009mq(float f) {
        bFf().mo5608dq().mo3150a(hAk, f);
    }

    /* renamed from: mr */
    private void m21010mr(float f) {
        bFf().mo5608dq().mo3150a(hAl, f);
    }

    @C0064Am(aul = "f6fcd1deff89a5bb4490330d57de85bf", aum = 0)
    @C5566aOg
    @C2499fr
    /* renamed from: qU */
    private void m21011qU() {
        throw new aWi(new aCE(this, f4345Lm, new Object[0]));
    }

    /* renamed from: S */
    public void mo10S() {
        super.mo10S();
    }

    /* renamed from: T */
    public C6494any mo11T() {
        return ___iScriptClass;
    }

    @C5566aOg
    /* renamed from: TJ */
    public AdapterLikedList<WeaponAdapter> mo5353TJ() {
        switch (bFf().mo6893i(aPf)) {
            case 0:
                return null;
            case 2:
                return (AdapterLikedList) bFf().mo5606d(new aCE(this, aPf, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, aPf, new Object[0]));
                break;
        }
        return m20953TI();
    }

    /* renamed from: TL */
    public void mo12905TL() {
        switch (bFf().mo6893i(aPj)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, aPj, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, aPj, new Object[0]));
                break;
        }
        m20954TK();
    }

    /* renamed from: TN */
    public void mo12906TN() {
        switch (bFf().mo6893i(aPk)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, aPk, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, aPk, new Object[0]));
                break;
        }
        m20955TM();
    }

    /* access modifiers changed from: protected */
    /* renamed from: TP */
    public void mo12907TP() {
        switch (bFf().mo6893i(aPl)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, aPl, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, aPl, new Object[0]));
                break;
        }
        m20956TO();
    }

    /* renamed from: TR */
    public void mo12908TR() {
        switch (bFf().mo6893i(aPm)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, aPm, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, aPm, new Object[0]));
                break;
        }
        m20957TQ();
    }

    /* renamed from: U */
    public C2491fm[] mo12U() {
        return _m_methods;
    }

    @C5307aEh
    /* renamed from: V */
    public void mo4709V(float f) {
        switch (bFf().mo6893i(_f_tick_0020_0028F_0029V)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, _f_tick_0020_0028F_0029V, new Object[]{new Float(f)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, _f_tick_0020_0028F_0029V, new Object[]{new Float(f)}));
                break;
        }
        m20959U(f);
    }

    /* renamed from: W */
    public Object mo13W() {
        return new C1716ZO(this);
    }

    /* renamed from: a */
    public Object mo14a(C0495Gr gr) {
        Object[] args = gr.getArgs();
        switch (gr.mo2417hq() - Component._m_methodCount) {
            case 0:
                return new Integer(aqi());
            case 1:
                return new Boolean(m21012s((C1722ZT) args[0]));
            case 2:
                return new Boolean(m20985g((C1722ZT<UserConnection>) (C1722ZT) args[0]));
            case 3:
                cTW();
                return null;
            case 4:
                m20951TG();
                return null;
            case 5:
                return m20979bZ((Actor) args[0]);
            case 6:
                m20982ex(((Float) args[0]).floatValue());
                return null;
            case 7:
                m20990iQ(((Boolean) args[0]).booleanValue());
                return null;
            case 8:
                m20962a((Pawn) args[0], (Vec3f) args[1]);
                return null;
            case 9:
                return m20953TI();
            case 10:
                return new Integer(aqg());
            case 11:
                return aqs();
            case 12:
                return aqu();
            case 13:
                return aqw();
            case 14:
                return m20967aq((Vec3d) args[0]);
            case 15:
                return ayu();
            case 16:
                return m20997ik();
            case 17:
                return cTY();
            case 18:
                return cUa();
            case 19:
                return new Float(ayA());
            case 20:
                return new Float(m21004mk());
            case 21:
                return new Long(cUc());
            case 22:
                return cUe();
            case 23:
                return cUg();
            case 24:
                return new Float(m20998im());
            case 25:
                return new Float(aqm());
            case 26:
                return aqq();
            case 27:
                return new Float(m20999is());
            case 28:
                return new Long(cUi());
            case 29:
                return cUk();
            case 30:
                return aqy();
            case 31:
                return cUm();
            case 32:
                cUo();
                return null;
            case 33:
                return cUq();
            case 34:
                return new Float(cUs());
            case 35:
                return new Boolean(cUu());
            case 36:
                return new Float(cUw());
            case 37:
                return new Float(cUy());
            case 38:
                m20983f((Orientation) args[0]);
                return null;
            case 39:
                return new Boolean(m21005mm());
            case 40:
                return new Boolean(cUA());
            case 41:
                m20980c((ItemLocation) args[0]);
                return null;
            case 42:
                m20964aG();
                return null;
            case 43:
                m21011qU();
                return null;
            case 44:
                return new Boolean(m21003m((C1722ZT<UserConnection>) (C1722ZT) args[0]));
            case 45:
                m20984fE(((Integer) args[0]).intValue());
                return null;
            case 46:
                m20986h((Orientation) args[0]);
                return null;
            case 47:
                m21000j((Orientation) args[0]);
                return null;
            case 48:
                m20991iS(((Boolean) args[0]).booleanValue());
                return null;
            case 49:
                m20993iU(((Boolean) args[0]).booleanValue());
                return null;
            case 50:
                m21001kt(((Long) args[0]).longValue());
                return null;
            case 51:
                m21002l((Orientation) args[0]);
                return null;
            case 52:
                m20981cb((Actor) args[0]);
                return null;
            case 53:
                m20970b((C6661arJ) args[0]);
                return null;
            case 54:
                return new Boolean(cUC());
            case 55:
                return new Boolean(cUE());
            case 56:
                ayw();
                return null;
            case 57:
                cUG();
                return null;
            case 58:
                bah();
                return null;
            case 59:
                m20959U(((Float) args[0]).floatValue());
                return null;
            case 60:
                return cUH();
            case 61:
                m20968ay(((Float) args[0]).floatValue());
                return null;
            case 62:
                return aad();
            case 63:
                m20969b((aDJ) args[0]);
                return null;
            case 64:
                m20956TO();
                return null;
            case 65:
                m20954TK();
                return null;
            case 66:
                m20955TM();
                return null;
            case 67:
                m20957TQ();
                return null;
            case 68:
                return cJR();
            default:
                return super.mo14a(gr);
        }
    }

    /* renamed from: a */
    public void mo15a(Collection collection, C0495Gr gr) {
        super.mo15a(collection, gr);
    }

    /* renamed from: aH */
    public void mo70aH() {
        switch (bFf().mo6893i(_f_onResurrect_0020_0028_0029V)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, _f_onResurrect_0020_0028_0029V, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, _f_onResurrect_0020_0028_0029V, new Object[0]));
                break;
        }
        m20964aG();
    }

    public Collection<aDR> aae() {
        switch (bFf().mo6893i(f4346x99c43db3)) {
            case 0:
                return null;
            case 2:
                return (Collection) bFf().mo5606d(new aCE(this, f4346x99c43db3, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f4346x99c43db3, new Object[0]));
                break;
        }
        return aad();
    }

    public int aqh() {
        switch (bFf().mo6893i(bMg)) {
            case 0:
                return 0;
            case 2:
                return ((Integer) bFf().mo5606d(new aCE(this, bMg, new Object[0]))).intValue();
            case 3:
                bFf().mo5606d(new aCE(this, bMg, new Object[0]));
                break;
        }
        return aqg();
    }

    public int aqj() {
        switch (bFf().mo6893i(bMh)) {
            case 0:
                return 0;
            case 2:
                return ((Integer) bFf().mo5606d(new aCE(this, bMh, new Object[0]))).intValue();
            case 3:
                bFf().mo5606d(new aCE(this, bMh, new Object[0]));
                break;
        }
        return aqi();
    }

    /* access modifiers changed from: protected */
    public float aqn() {
        switch (bFf().mo6893i(bMm)) {
            case 0:
                return 0.0f;
            case 2:
                return ((Float) bFf().mo5606d(new aCE(this, bMm, new Object[0]))).floatValue();
            case 3:
                bFf().mo5606d(new aCE(this, bMm, new Object[0]));
                break;
        }
        return aqm();
    }

    public String aqr() {
        switch (bFf().mo6893i(bMo)) {
            case 0:
                return null;
            case 2:
                return (String) bFf().mo5606d(new aCE(this, bMo, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, bMo, new Object[0]));
                break;
        }
        return aqq();
    }

    public String aqt() {
        switch (bFf().mo6893i(bMp)) {
            case 0:
                return null;
            case 2:
                return (String) bFf().mo5606d(new aCE(this, bMp, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, bMp, new Object[0]));
                break;
        }
        return aqs();
    }

    public String aqv() {
        switch (bFf().mo6893i(bMq)) {
            case 0:
                return null;
            case 2:
                return (String) bFf().mo5606d(new aCE(this, bMq, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, bMq, new Object[0]));
                break;
        }
        return aqu();
    }

    public String aqx() {
        switch (bFf().mo6893i(bMr)) {
            case 0:
                return null;
            case 2:
                return (String) bFf().mo5606d(new aCE(this, bMr, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, bMr, new Object[0]));
                break;
        }
        return aqw();
    }

    public WeaponType aqz() {
        switch (bFf().mo6893i(bMs)) {
            case 0:
                return null;
            case 2:
                return (WeaponType) bFf().mo5606d(new aCE(this, bMs, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, bMs, new Object[0]));
                break;
        }
        return aqy();
    }

    /* renamed from: ar */
    public Quat4fWrap mo12910ar(Vec3d ajr) {
        switch (bFf().mo6893i(hAs)) {
            case 0:
                return null;
            case 2:
                return (Quat4fWrap) bFf().mo5606d(new aCE(this, hAs, new Object[]{ajr}));
            case 3:
                bFf().mo5606d(new aCE(this, hAs, new Object[]{ajr}));
                break;
        }
        return m20967aq(ajr);
    }

    /* access modifiers changed from: protected */
    public float ayB() {
        switch (bFf().mo6893i(clz)) {
            case 0:
                return 0.0f;
            case 2:
                return ((Float) bFf().mo5606d(new aCE(this, clz, new Object[0]))).floatValue();
            case 3:
                bFf().mo5606d(new aCE(this, clz, new Object[0]));
                break;
        }
        return ayA();
    }

    public Vec3f ayv() {
        switch (bFf().mo6893i(clu)) {
            case 0:
                return null;
            case 2:
                return (Vec3f) bFf().mo5606d(new aCE(this, clu, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, clu, new Object[0]));
                break;
        }
        return ayu();
    }

    /* access modifiers changed from: protected */
    @C1253SX
    public void ayx() {
        switch (bFf().mo6893i(clw)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, clw, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, clw, new Object[0]));
                break;
        }
        ayw();
    }

    @ClientOnly
    /* renamed from: b */
    public void mo12911b(Pawn avi, Vec3f vec3f) {
        switch (bFf().mo6893i(hAr)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, hAr, new Object[]{avi, vec3f}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, hAr, new Object[]{avi, vec3f}));
                break;
        }
        m20962a(avi, vec3f);
    }

    public void bai() {
        switch (bFf().mo6893i(dmH)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, dmH, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, dmH, new Object[0]));
                break;
        }
        bah();
    }

    /* renamed from: c */
    public void mo98c(aDJ adj) {
        switch (bFf().mo6893i(f4347x13860637)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f4347x13860637, new Object[]{adj}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f4347x13860637, new Object[]{adj}));
                break;
        }
        m20969b(adj);
    }

    /* renamed from: c */
    public void mo12913c(C6661arJ arj) {
        switch (bFf().mo6893i(hAQ)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, hAQ, new Object[]{arj}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, hAQ, new Object[]{arj}));
                break;
        }
        m20970b(arj);
    }

    /* renamed from: c */
    public C5663aRz[] mo25c() {
        return _m_fields;
    }

    public void cDL() {
        switch (bFf().mo6893i(hAT)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, hAT, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, hAT, new Object[0]));
                break;
        }
        cUG();
    }

    public /* bridge */ /* synthetic */ ComponentType cJS() {
        switch (bFf().mo6893i(hix)) {
            case 0:
                return null;
            case 2:
                return (ComponentType) bFf().mo5606d(new aCE(this, hix, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, hix, new Object[0]));
                break;
        }
        return cJR();
    }

    public void cTX() {
        switch (bFf().mo6893i(hAo)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, hAo, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, hAo, new Object[0]));
                break;
        }
        cTW();
    }

    public C1556Wo<DamageType, Float> cTZ() {
        switch (bFf().mo6893i(hAt)) {
            case 0:
                return null;
            case 2:
                return (C1556Wo) bFf().mo5606d(new aCE(this, hAt, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, hAt, new Object[0]));
                break;
        }
        return cTY();
    }

    public boolean cUB() {
        switch (bFf().mo6893i(hAI)) {
            case 0:
                return false;
            case 2:
                return ((Boolean) bFf().mo5606d(new aCE(this, hAI, new Object[0]))).booleanValue();
            case 3:
                bFf().mo5606d(new aCE(this, hAI, new Object[0]));
                break;
        }
        return cUA();
    }

    /* access modifiers changed from: protected */
    public boolean cUD() {
        switch (bFf().mo6893i(hAR)) {
            case 0:
                return false;
            case 2:
                return ((Boolean) bFf().mo5606d(new aCE(this, hAR, new Object[0]))).booleanValue();
            case 3:
                bFf().mo5606d(new aCE(this, hAR, new Object[0]));
                break;
        }
        return cUC();
    }

    /* access modifiers changed from: protected */
    @C3248pc
    public boolean cUF() {
        switch (bFf().mo6893i(hAS)) {
            case 0:
                return false;
            case 2:
                return ((Boolean) bFf().mo5606d(new aCE(this, hAS, new Object[0]))).booleanValue();
            case 3:
                bFf().mo5606d(new aCE(this, hAS, new Object[0]));
                break;
        }
        return cUE();
    }

    public Weapon cUI() {
        switch (bFf().mo6893i(hAU)) {
            case 0:
                return null;
            case 2:
                return (Weapon) bFf().mo5606d(new aCE(this, hAU, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, hAU, new Object[0]));
                break;
        }
        return cUH();
    }

    /* access modifiers changed from: protected */
    public long cUd() {
        switch (bFf().mo6893i(hAv)) {
            case 0:
                return 0;
            case 2:
                return ((Long) bFf().mo5606d(new aCE(this, hAv, new Object[0]))).longValue();
            case 3:
                bFf().mo5606d(new aCE(this, hAv, new Object[0]));
                break;
        }
        return cUc();
    }

    public Vec3d cUf() {
        switch (bFf().mo6893i(hAw)) {
            case 0:
                return null;
            case 2:
                return (Vec3d) bFf().mo5606d(new aCE(this, hAw, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, hAw, new Object[0]));
                break;
        }
        return cUe();
    }

    /* access modifiers changed from: package-private */
    public RPulsingLight cUh() {
        switch (bFf().mo6893i(hAx)) {
            case 0:
                return null;
            case 2:
                return (RPulsingLight) bFf().mo5606d(new aCE(this, hAx, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, hAx, new Object[0]));
                break;
        }
        return cUg();
    }

    /* access modifiers changed from: protected */
    public long cUj() {
        switch (bFf().mo6893i(hAy)) {
            case 0:
                return 0;
            case 2:
                return ((Long) bFf().mo5606d(new aCE(this, hAy, new Object[0]))).longValue();
            case 3:
                bFf().mo5606d(new aCE(this, hAy, new Object[0]));
                break;
        }
        return cUi();
    }

    public Actor cUl() {
        switch (bFf().mo6893i(hAz)) {
            case 0:
                return null;
            case 2:
                return (Actor) bFf().mo5606d(new aCE(this, hAz, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, hAz, new Object[0]));
                break;
        }
        return cUk();
    }

    public C6661arJ cUn() {
        switch (bFf().mo6893i(hAA)) {
            case 0:
                return null;
            case 2:
                return (C6661arJ) bFf().mo5606d(new aCE(this, hAA, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, hAA, new Object[0]));
                break;
        }
        return cUm();
    }

    @ClientOnly
    public void cUp() {
        switch (bFf().mo6893i(hAB)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, hAB, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, hAB, new Object[0]));
                break;
        }
        cUo();
    }

    public C1556Wo<DamageType, Float> cUr() {
        switch (bFf().mo6893i(hAC)) {
            case 0:
                return null;
            case 2:
                return (C1556Wo) bFf().mo5606d(new aCE(this, hAC, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, hAC, new Object[0]));
                break;
        }
        return cUq();
    }

    public float cUt() {
        switch (bFf().mo6893i(hAD)) {
            case 0:
                return 0.0f;
            case 2:
                return ((Float) bFf().mo5606d(new aCE(this, hAD, new Object[0]))).floatValue();
            case 3:
                bFf().mo5606d(new aCE(this, hAD, new Object[0]));
                break;
        }
        return cUs();
    }

    public boolean cUv() {
        switch (bFf().mo6893i(hAE)) {
            case 0:
                return false;
            case 2:
                return ((Boolean) bFf().mo5606d(new aCE(this, hAE, new Object[0]))).booleanValue();
            case 3:
                bFf().mo5606d(new aCE(this, hAE, new Object[0]));
                break;
        }
        return cUu();
    }

    public float cUx() {
        switch (bFf().mo6893i(hAF)) {
            case 0:
                return 0.0f;
            case 2:
                return ((Float) bFf().mo5606d(new aCE(this, hAF, new Object[0]))).floatValue();
            case 3:
                bFf().mo5606d(new aCE(this, hAF, new Object[0]));
                break;
        }
        return cUw();
    }

    public float cUz() {
        switch (bFf().mo6893i(hAG)) {
            case 0:
                return 0.0f;
            case 2:
                return ((Float) bFf().mo5606d(new aCE(this, hAG, new Object[0]))).floatValue();
            case 3:
                bFf().mo5606d(new aCE(this, hAG, new Object[0]));
                break;
        }
        return cUy();
    }

    /* renamed from: ca */
    public Vec3d mo12932ca(Actor cr) {
        switch (bFf().mo6893i(hAp)) {
            case 0:
                return null;
            case 2:
                return (Vec3d) bFf().mo5606d(new aCE(this, hAp, new Object[]{cr}));
            case 3:
                bFf().mo5606d(new aCE(this, hAp, new Object[]{cr}));
                break;
        }
        return m20979bZ(cr);
    }

    /* renamed from: cc */
    public void mo12933cc(Actor cr) {
        switch (bFf().mo6893i(hAP)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, hAP, new Object[]{cr}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, hAP, new Object[]{cr}));
                break;
        }
        m20981cb(cr);
    }

    @C5566aOg
    /* renamed from: d */
    public void mo12934d(ItemLocation aag) {
        switch (bFf().mo6893i(ccM)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, ccM, new Object[]{aag}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, ccM, new Object[]{aag}));
                break;
        }
        m20980c(aag);
    }

    /* access modifiers changed from: protected */
    @C4034yP
    @C2499fr
    @C1253SX
    /* renamed from: ey */
    public void mo831ey(float f) {
        switch (bFf().mo6893i(clv)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, clv, new Object[]{new Float(f)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, clv, new Object[]{new Float(f)}));
                break;
        }
        m20982ex(f);
    }

    /* access modifiers changed from: protected */
    /* renamed from: fF */
    public void mo5370fF(int i) {
        switch (bFf().mo6893i(bMi)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, bMi, new Object[]{new Integer(i)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, bMi, new Object[]{new Integer(i)}));
                break;
        }
        m20984fE(i);
    }

    /* access modifiers changed from: protected */
    /* renamed from: g */
    public void mo12935g(Orientation orientation) {
        switch (bFf().mo6893i(hAH)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, hAH, new Object[]{orientation}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, hAH, new Object[]{orientation}));
                break;
        }
        m20983f(orientation);
    }

    public float getSpeed() {
        switch (bFf().mo6893i(f4353vh)) {
            case 0:
                return 0.0f;
            case 2:
                return ((Float) bFf().mo5606d(new aCE(this, f4353vh, new Object[0]))).floatValue();
            case 3:
                bFf().mo5606d(new aCE(this, f4353vh, new Object[0]));
                break;
        }
        return m20999is();
    }

    @C5472aKq
    /* renamed from: h */
    public boolean mo12937h(C1722ZT<UserConnection> zt) {
        switch (bFf().mo6893i(dmG)) {
            case 0:
                return false;
            case 2:
                return ((Boolean) bFf().mo5606d(new aCE(this, dmG, new Object[]{zt}))).booleanValue();
            case 3:
                bFf().mo5606d(new aCE(this, dmG, new Object[]{zt}));
                break;
        }
        return m20985g(zt);
    }

    /* access modifiers changed from: protected */
    @C4034yP
    @C2499fr(mo18855qf = {"224b72906480d9f699bfdb0ff983318a"})
    @ClientOnly
    /* renamed from: i */
    public void mo12938i(Orientation orientation) {
        switch (bFf().mo6893i(hAJ)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, hAJ, new Object[]{orientation}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, hAJ, new Object[]{orientation}));
                break;
        }
        m20986h(orientation);
    }

    @ClientOnly
    /* renamed from: iR */
    public void mo12939iR(boolean z) {
        switch (bFf().mo6893i(hAq)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, hAq, new Object[]{new Boolean(z)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, hAq, new Object[]{new Boolean(z)}));
                break;
        }
        m20990iQ(z);
    }

    /* renamed from: il */
    public C5260aCm mo5372il() {
        switch (bFf().mo6893i(f4351uW)) {
            case 0:
                return null;
            case 2:
                return (C5260aCm) bFf().mo5606d(new aCE(this, f4351uW, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f4351uW, new Object[0]));
                break;
        }
        return m20997ik();
    }

    /* renamed from: in */
    public float mo12940in() {
        switch (bFf().mo6893i(f4352vb)) {
            case 0:
                return 0.0f;
            case 2:
                return ((Float) bFf().mo5606d(new aCE(this, f4352vb, new Object[0]))).floatValue();
            case 3:
                bFf().mo5606d(new aCE(this, f4352vb, new Object[0]));
                break;
        }
        return m20998im();
    }

    /* renamed from: k */
    public void mo12941k(Orientation orientation) {
        switch (bFf().mo6893i(hAK)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, hAK, new Object[]{orientation}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, hAK, new Object[]{orientation}));
                break;
        }
        m21000j(orientation);
    }

    /* access modifiers changed from: protected */
    /* renamed from: ku */
    public void mo12942ku(long j) {
        switch (bFf().mo6893i(hAN)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, hAN, new Object[]{new Long(j)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, hAN, new Object[]{new Long(j)}));
                break;
        }
        m21001kt(j);
    }

    /* access modifiers changed from: protected */
    @C4034yP
    @C5566aOg
    @C2499fr
    /* renamed from: m */
    public void mo12943m(Orientation orientation) {
        switch (bFf().mo6893i(hAO)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, hAO, new Object[]{orientation}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, hAO, new Object[]{orientation}));
                break;
        }
        m21002l(orientation);
    }

    /* renamed from: ml */
    public float mo12944ml() {
        switch (bFf().mo6893i(f4343DJ)) {
            case 0:
                return 0.0f;
            case 2:
                return ((Float) bFf().mo5606d(new aCE(this, f4343DJ, new Object[0]))).floatValue();
            case 3:
                bFf().mo5606d(new aCE(this, f4343DJ, new Object[0]));
                break;
        }
        return m21004mk();
    }

    /* renamed from: mn */
    public boolean mo12945mn() {
        switch (bFf().mo6893i(f4344DK)) {
            case 0:
                return false;
            case 2:
                return ((Boolean) bFf().mo5606d(new aCE(this, f4344DK, new Object[0]))).booleanValue();
            case 3:
                bFf().mo5606d(new aCE(this, f4344DK, new Object[0]));
                break;
        }
        return m21005mm();
    }

    @C5472aKq
    /* renamed from: n */
    public boolean mo12946n(C1722ZT<UserConnection> zt) {
        switch (bFf().mo6893i(hlM)) {
            case 0:
                return false;
            case 2:
                return ((Boolean) bFf().mo5606d(new aCE(this, hlM, new Object[]{zt}))).booleanValue();
            case 3:
                bFf().mo5606d(new aCE(this, hlM, new Object[]{zt}));
                break;
        }
        return m21003m(zt);
    }

    @C5566aOg
    @C2499fr
    /* renamed from: qV */
    public void mo1959qV() {
        switch (bFf().mo6893i(f4345Lm)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f4345Lm, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f4345Lm, new Object[0]));
                break;
        }
        m21011qU();
    }

    public void step(float f) {
        switch (bFf().mo6893i(_f_step_0020_0028F_0029V)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, _f_step_0020_0028F_0029V, new Object[]{new Float(f)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, _f_step_0020_0028F_0029V, new Object[]{new Float(f)}));
                break;
        }
        m20968ay(f);
    }

    @C5472aKq
    /* renamed from: t */
    public boolean mo12948t(C1722ZT<UserConnection> zt) {
        switch (bFf().mo6893i(hAn)) {
            case 0:
                return false;
            case 2:
                return ((Boolean) bFf().mo5606d(new aCE(this, hAn, new Object[]{zt}))).booleanValue();
            case 3:
                bFf().mo5606d(new aCE(this, hAn, new Object[]{zt}));
                break;
        }
        return m21012s(zt);
    }

    /* renamed from: x */
    public void mo12949x(WeaponType apt) {
        super.mo7854a((ComponentType) apt);
        this.initialized = false;
        this.hAe = null;
        m20952TH();
    }

    @C0064Am(aul = "0a7ce28f2c52c99b44aedd562aa55b34", aum = 0)
    @C5472aKq
    /* renamed from: s */
    private boolean m21012s(C1722ZT<UserConnection> zt) {
        boolean z;
        if (zt == null) {
            mo8358lY("Null replication context. Ignoring replication rule");
            return false;
        }
        UserConnection bFq = zt.bFq();
        if (bFq.mo15388dL() == null || bFq.mo15388dL().bQx() == cUn()) {
            z = false;
        } else {
            z = true;
        }
        return z;
    }

    @C0064Am(aul = "9fd1c6d28dce7e77506742b528426e27", aum = 0)
    @C5472aKq
    /* renamed from: g */
    private boolean m20985g(C1722ZT<UserConnection> zt) {
        Ship bQx;
        boolean z;
        if (zt == null) {
            mo8358lY("Null replication context. Ignoring replication rule");
            return false;
        }
        Player dL = zt.bFq().mo15388dL();
        if (dL == null || (bQx = dL.bQx()) == null) {
            return false;
        }
        if (bQx.mo960Nc() == null) {
            mo6317hy("connection ship " + bQx.getName() + " is not on stellar system");
            return false;
        } else if (cUn().azW().mo21606Nc() == null) {
            mo6317hy("this shooter " + cUn() + " is not on stellar system");
            return false;
        } else {
            Node azW = bQx.azW();
            if (azW == null || azW != cUn().azW()) {
                z = false;
            } else {
                z = true;
            }
            return z;
        }
    }

    @C0064Am(aul = "bf8ebe7f288045bd2b353448bbfea9f7", aum = 0)
    private void cTW() {
        if (bGX()) {
            cUp();
        }
    }

    @C0064Am(aul = "558846ee6438c03dceeffa8ae5529c83", aum = 0)
    /* renamed from: bZ */
    private Vec3d m20979bZ(Actor cr) {
        if (cTF()) {
            return cr.getPosition().dfY();
        }
        float speed2 = aqz().getSpeed();
        float ax = (float) cUI().cUn().getPosition().mo9491ax(cr.getPosition());
        if (ax >= 50000.0f || cr.aiD() == null || speed2 <= 1.0f) {
            return cr.getPosition().dfY();
        }
        C6400amI ami = new C6400amI();
        ((C6400amI) cr.aiD().mo2472kQ()).mo14818b(ax / speed2, ami);
        return ami.blk().dfY();
    }

    @C4034yP
    @C0064Am(aul = "578fdadfc18dbd5d6942a0895b1683f5", aum = 0)
    @C2499fr
    @C1253SX
    /* renamed from: ex */
    private void m20982ex(float f) {
        String bFY;
        int i = 2;
        if (!isUsable() && cxn()) {
            logger.warn("Weapon deactivated, ignoring shot");
        } else if (cUn() == null) {
            logger.warn("No weapon owner set, ignoring shot");
        } else {
            Pawn agv = cUn().agv();
            if (agv == null || !agv.bae()) {
                Log log = logger;
                if (("Spawning a shot for a weapon whose instigator is null or not spawned - Weapon is " + bFY() + " owned by " + cUn() + " instigated by " + agv) == null) {
                    bFY = null;
                } else {
                    bFY = agv.bFY();
                }
                log.warn(bFY);
                bai();
            } else if (!bGX() || !getPlayer().bQB()) {
                if (logger.isTraceEnabled()) {
                    logger.trace("fire weapon, timestep " + f);
                }
                if (cUn().afR() == this || cUn().afT() == this) {
                    long cVr = cVr();
                    float f2 = ((float) (cVr - this.hAd)) / 1000.0f;
                    if (aqh() < aqj()) {
                        bai();
                    } else if (f2 >= ayB()) {
                        ayx();
                        if (mo12945mn() && aqh() > 1) {
                            ayx();
                        }
                        if (logger.isTraceEnabled()) {
                            logger.trace("setLastShootTime " + cVr);
                        }
                        this.hAd = cVr;
                        if (bHa() || bGY()) {
                            int aqh = aqh();
                            int aqj = aqj();
                            if (!mo12945mn() || aqh() <= 1) {
                                i = 1;
                            }
                            mo5370fF(aqh - (i * aqj));
                        } else if (cTQ().mo2998hb() == getPlayer().dxc()) {
                            int aqh2 = aqh();
                            int aqj2 = aqj();
                            if (!mo12945mn() || aqh() <= 1) {
                                i = 1;
                            }
                            mo5370fF(aqh2 - (i * aqj2));
                        }
                        if (aqh() <= 0) {
                            if (logger.isTraceEnabled()) {
                                logger.trace("no ammo, stopping fire ");
                            }
                            m20994iV(false);
                        }
                    } else if (logger.isTraceEnabled()) {
                        logger.trace("cant fire!!!! dt " + f2 + " FP " + ayB());
                    }
                } else {
                    if (logger.isTraceEnabled()) {
                        logger.trace("forcing stop firing since this is not the current weapon");
                    }
                    bai();
                }
            } else {
                logger.warn("Player is docked, ignoring start firing (" + getPlayer().getName() + ")");
            }
        }
    }

    @C0064Am(aul = "aa5981f15dedce64c6debf8de9066839", aum = 0)
    @ClientOnly
    /* renamed from: iQ */
    private void m20990iQ(boolean z) {
        boolean z2 = false;
        WeaponType aqz = cUI().aqz();
        if (z && aqz.doX() != null && this.hAm == null) {
            Pawn agv = cUn().agv();
            Vec3f vec3f = new Vec3f(0.0f, 0.0f, 0.0f);
            if (agv instanceof Ship) {
                z2 = ((Ship) agv).afP();
            }
            this.hAm = new C3257pi();
            this.hAm.mo21214a(aqz.doX(), vec3f.dfR(), agv.cHg(), C3257pi.C3261d.NEAR_ENOUGH, z2);
        } else if (!z && this.hAm != null) {
            Pawn agv2 = cUn().agv();
            Vec3f vec3f2 = new Vec3f(0.0f, 0.0f, 0.0f);
            this.hAm.asy();
            this.hAm = null;
            if (aqz.doZ() != null) {
                if (agv2 instanceof Ship) {
                    z2 = ((Ship) agv2).afP();
                }
                new C3257pi().mo21214a(aqz.doZ(), vec3f2.dfR(), agv2.cHg(), C3257pi.C3261d.NEAR_ENOUGH, z2);
            }
        }
    }

    @C0064Am(aul = "c653b9a436426826aa9eede789fcbbaf", aum = 0)
    @ClientOnly
    /* renamed from: a */
    private void m20962a(Pawn avi, Vec3f vec3f) {
        if (avi.cHg() != null) {
            WeaponType aqz = cUI().aqz();
            Vec3d dfR = vec3f.dfR();
            new C3257pi().mo21213a(aqz.doN(), dfR, avi.cHg(), C3257pi.C3261d.NEAR_ENOUGH);
            if (avi instanceof Ship) {
                new C3257pi().mo21214a(aqz.doR(), dfR, avi.cHg(), C3257pi.C3261d.NEAR_ENOUGH, ((Ship) avi).afP());
            } else {
                new C3257pi().mo21213a(aqz.doR(), dfR, avi.cHg(), C3257pi.C3261d.NEAR_ENOUGH);
            }
            RPulsingLight cUh = cUh();
            if (cUh != null) {
                Vec3d ajr = new Vec3d();
                avi.cHg().getGlobalTransform().mo17339a((Vector3f) vec3f, (Vector3d) ajr);
                cUh.setPosition(ajr);
                cUh.trigger();
            }
        }
    }

    @C0064Am(aul = "05b2b0113a79f2131bf3ab409e3b142a", aum = 0)
    /* renamed from: aq */
    private Quat4fWrap m20967aq(Vec3d ajr) {
        Vec3f aB = cUb().mo9485aB(ajr);
        aB.normalize();
        Vec3f vec3f = new Vec3f(0.0f, 1.0f, 0.0f);
        if (vec3f.dot(aB) == 1.0f) {
            vec3f = new Vec3f(1.0f, 0.0f, 0.0f);
            if (vec3f.dot(aB) == 1.0f) {
                vec3f = new Vec3f(0.0f, 0.0f, 1.0f);
                if (vec3f.dot(aB) == 1.0f) {
                    System.err.println("can not find orthogonal vector!!");
                }
            }
        }
        Vec3f b = aB.mo23476b(vec3f);
        Vec3f b2 = b.mo23476b(aB);
        Matrix4fWrap ajk = new Matrix4fWrap();
        ajk.mo13988aD(b);
        ajk.mo13989aE(b2);
        aB.negate();
        ajk.mo13990aF(aB);
        return new Quat4fWrap(ajk);
    }

    @C0064Am(aul = "930896f148ccdb769db19c991ee1892d", aum = 0)
    private Vec3f ayu() {
        if (cUn().afN() != null) {
            return cUn().afN().mo16402gL();
        }
        return cUn().afV();
    }

    @C0064Am(aul = "5056969d47141a5e61d181f898663564", aum = 0)
    private C1556Wo<DamageType, Float> cTY() {
        return cTR();
    }

    @C0064Am(aul = "4598294bd1bf5f811f12706b20f1faa9", aum = 0)
    private Vec3d cUa() {
        Vec3f aT;
        Vec3f vec3f = new Vec3f(0.0f, 0.0f, -1.0f);
        if (cUn() instanceof Ship) {
            aT = cTQ().getOrientation().mo15209aT(((Ship) cUn()).aip());
        } else {
            aT = cTQ().getOrientation().mo15209aT(vec3f);
        }
        float f = m20995if();
        Actor cUl = cUl();
        Vec3d position = cUn().getPosition();
        if (cUl != null && (!bGX() || !C1298TD.m9830t(cUl).bFT())) {
            Vec3d ca = mo12932ca(cUl);
            Vec3f aB = ca.mo9485aB(position);
            if (aB.length() <= f) {
                aB.normalize();
                float dot = aT.dot(aB);
                float cos = (float) Math.cos((double) cUn().agD());
                if (dot > 0.0f && dot >= cos) {
                    return ca;
                }
            }
        }
        return cUn().getPosition().mo9531q(cUn().getOrientation().mo15209aT(cUn().afV())).mo9531q(aT.mo23510mS(f));
    }

    @C0064Am(aul = "619ec9b3f1ef97208a6b51f300c681e3", aum = 0)
    private float ayA() {
        if (mo12944ml() > 0.0f) {
            return 1.0f / mo12944ml();
        }
        return 1.0f;
    }

    @C0064Am(aul = "c00f934d5d5d99c663e93f2f4da1b4c1", aum = 0)
    /* renamed from: mk */
    private float m21004mk() {
        return cTS();
    }

    @C0064Am(aul = "5031ed492c3ef2f2fd102cec33622e67", aum = 0)
    private long cUc() {
        return this.hAd;
    }

    @C0064Am(aul = "b733bc72ed095043fa37c87607ae6d97", aum = 0)
    private Vec3d cUe() {
        return cTQ().getPosition().mo9531q(cTQ().getOrientation().mo15209aT(cTQ().afV()));
    }

    @C0064Am(aul = "fff7504a84d2a7fcaf09bb739a2715bb", aum = 0)
    private RPulsingLight cUg() {
        if (!this.initialized) {
            IEngineGraphics ale = C5916acs.getSingolton().getEngineGraphics();
            if (ale == null) {
                return null;
            }
            String str = String.valueOf(cUI().aqv()) + "_pulselight";
            if (ale.aej().getStockEntry(str) != null) {
                this.hAe = (RPulsingLight) ale.mo3048bU(str);
                if (this.hAe != null) {
                    ale.adZ().addChild(this.hAe);
                }
            } else {
                this.hAe = new RPulsingLight();
                this.hAe.setInitialColor(new Color(1.0f, 1.0f, 1.0f, 1.0f));
                this.hAe.setFinalColor(new Color(1.0f, 1.0f, 1.0f, 1.0f));
                this.hAe.setInitialSize(10.0f);
                this.hAe.setFinalSize(0.0f);
                this.hAe.setLifeTime(0.1f);
                ale.adZ().addChild(this.hAe);
            }
            this.initialized = true;
        }
        return this.hAe;
    }

    @C0064Am(aul = "13653d3818c7da9e0c14bfd81f296314", aum = 0)
    /* renamed from: im */
    private float m20998im() {
        return cTU();
    }

    @C0064Am(aul = "98f5b461a507a0b3194d64564aa89ecd", aum = 0)
    private float aqm() {
        return -1.0f;
    }

    @C0064Am(aul = "181511b14b6abbdf5ff210813d7c8b64", aum = 0)
    /* renamed from: is */
    private float m20999is() {
        return cTV();
    }

    @C0064Am(aul = "17b3b59f43706456d20d36d455cbfb41", aum = 0)
    private long cUi() {
        return 0;
    }

    @C0064Am(aul = "b6bbd23eec0ea832de059f8e9afb2a69", aum = 0)
    private Actor cUk() {
        return cTP();
    }

    @C0064Am(aul = "04f89db697ff9494211d589d465ac606", aum = 0)
    private WeaponType aqy() {
        return (WeaponType) super.cJS();
    }

    @C0064Am(aul = "7d86853aa8b06610a86172f53ecd4da5", aum = 0)
    private C6661arJ cUm() {
        return cTQ();
    }

    @C0064Am(aul = "b8608b3a8d16aa6a2b6f0b18f0ae2e70", aum = 0)
    @ClientOnly
    private void cUo() {
        IEngineGraphics ale = ald().getEngineGraphics();
        if (ale != null) {
            if (this.hAe != null) {
                ale.adZ().removeChild(this.hAe);
                this.hAe.dispose();
                this.hAe = null;
            }
            this.initialized = false;
        }
    }

    @C0064Am(aul = "444f5aad329fb2ce322b954f505e9b4d", aum = 0)
    private C1556Wo<DamageType, Float> cUq() {
        return bAH();
    }

    @C0064Am(aul = "66b7e6b179ccc6ed8f76dead69771988", aum = 0)
    private float cUs() {
        return cTG();
    }

    @C0064Am(aul = "7c0a07d190bcf7ee314acf772cf9e4aa", aum = 0)
    private boolean cUu() {
        return amJ();
    }

    @C0064Am(aul = "eda4f40a9c0f7811a4849551c65d5c08", aum = 0)
    private float cUw() {
        return m20995if();
    }

    @C0064Am(aul = "00ec65bc1484320a2cb45d7134946376", aum = 0)
    private float cUy() {
        return m20996ii();
    }

    @C0064Am(aul = "273895a5acf8679628ed6343f2d6b003", aum = 0)
    /* renamed from: f */
    private void m20983f(Orientation orientation) {
        this.hAa = orientation;
    }

    @C0064Am(aul = "5ec5103c9ba1d113e05e2e261c403a02", aum = 0)
    /* renamed from: mm */
    private boolean m21005mm() {
        return cTT();
    }

    @C0064Am(aul = "21f7a36a086369452f58d12ed013a645", aum = 0)
    private boolean cUA() {
        C6661arJ cUn;
        if (!bGX() || (cUn = cUn()) == null || !(cUn instanceof Ship) || !(((Ship) cUn).agj() instanceof Player) || !getPlayer().equals(((Ship) cUn).agj())) {
            return cTO();
        }
        return this.hAc;
    }

    @C0064Am(aul = "f5f3baed3f7647db5fa7bc0a1b51896a", aum = 0)
    /* renamed from: aG */
    private void m20964aG() {
        super.mo70aH();
        m20952TH();
    }

    @C0064Am(aul = "224b72906480d9f699bfdb0ff983318a", aum = 0)
    @C5472aKq
    /* renamed from: m */
    private boolean m21003m(C1722ZT<UserConnection> zt) {
        if (zt == null) {
            mo8358lY("Null replication context. Ignoring replication rule");
            return false;
        }
        Player dL = zt.bFq().mo15388dL();
        if (dL == null) {
            return false;
        }
        if (dL.bQB()) {
            return false;
        }
        if (dL.bQx() == null) {
            return false;
        }
        if (dL.bQx().getPosition().mo9486aC(cUn().getPosition()) > 1.41006541E9f) {
            return false;
        }
        return true;
    }

    @C4034yP
    @ClientOnly
    @C0064Am(aul = "240dcfe40821aa82d55cc0822b833307", aum = 0)
    @C2499fr(mo18855qf = {"224b72906480d9f699bfdb0ff983318a"})
    /* renamed from: h */
    private void m20986h(Orientation orientation) {
        mo12935g(orientation);
    }

    @C0064Am(aul = "62d63c8b97c846d6225f43f38c941e03", aum = 0)
    /* renamed from: j */
    private void m21000j(Orientation orientation) {
        mo12938i(orientation);
        mo12943m(orientation);
    }

    @C0064Am(aul = "bedca0a992475c9a2bf0b04c461aca19", aum = 0)
    /* renamed from: iU */
    private void m20993iU(boolean z) {
        C6661arJ cUn;
        if (bGX() && (cUn = cUn()) != null && (cUn instanceof Ship) && (((Ship) cUn).agj() instanceof Player) && getPlayer().equals(((Ship) cUn).agj())) {
            this.hAc = z;
        }
        m20992iT(z);
        m20988iO(z);
    }

    @C0064Am(aul = "e37edaf461f7c8ba097178fd9ff80b48", aum = 0)
    /* renamed from: kt */
    private void m21001kt(long j) {
        this.hAd = j;
    }

    @C0064Am(aul = "243af9a50cc9e7166666d754729f8122", aum = 0)
    /* renamed from: cb */
    private void m20981cb(Actor cr) {
        if (bHa() && cTP() != null) {
            cTP().mo8355h(C3161oY.C3162a.class, this);
        }
        m20978bY(cr);
        if (bHa() && cTP() != null) {
            cTP().mo8348d(C3161oY.C3162a.class, this);
        }
    }

    @C0064Am(aul = "31c33bdf5055e76c4f12cc41d2cda74c", aum = 0)
    /* renamed from: b */
    private void m20970b(C6661arJ arj) {
        m20961a(arj);
    }

    @C0064Am(aul = "be03e5a0c445976daa2e7c8b159967bf", aum = 0)
    private boolean cUC() {
        if (cUn() != null) {
            if (bGX()) {
                if (!getPlayer().bQB()) {
                    return true;
                }
                return false;
            } else if (cUB()) {
                return false;
            }
            return true;
        } else if (!logger.isDebugEnabled()) {
            return false;
        } else {
            logger.debug("Weapon isn't in a ship, so it can't start firing!!");
            return false;
        }
    }

    @C3248pc
    @C0064Am(aul = "042d95b2266f50269f1f764182a77aa7", aum = 0)
    private boolean cUE() {
        if (cUB()) {
            return true;
        }
        if (logger.isDebugEnabled()) {
            logger.debug("Weapon is not firing");
        }
        return false;
    }

    @C0064Am(aul = "2f3a51421b09723f0d467539e03728c4", aum = 0)
    @C1253SX
    private void ayw() {
        if ((!C5877acF.RUNNING_CLIENT_BOT || !bGX()) && cUn() != null && cUn().azW() != null && cUn().azW().mo21606Nc() != null && cUn().azW().mo21618Zc().cKn() != null) {
            if (logger.isTraceEnabled()) {
                logger.trace("Spawning shot");
            }
            float cTI = cTI();
            if (cTI == 0.0f) {
                cTI = 5.0f;
            }
            C6348alI ali = new C6348alI(cTI);
            C5260aCm acm = new C5260aCm(mo5372il().cPK(), aqr(), cUl());
            acm.mo8232w(aqz());
            float in = mo12940in();
            Shot cAVar = (Shot) bFf().mo6865M(Shot.class);
            cAVar.mo17514a(acm, in);
            cAVar.mo1085k(cUn().agv());
            Quat4fWrap orientation = cUn().getOrientation();
            if (cUn().afN() == null) {
                cAVar.setPosition(cTQ().getPosition().mo9531q(cTQ().getOrientation().mo15209aT(cTQ().afV())));
            } else {
                cAVar.setPosition(cTQ().getPosition().mo9531q(orientation.mo15209aT(ayv())));
            }
            cAVar.setOrientation(mo12910ar(cAVar.getPosition()));
            cAVar.setSpeed(getSpeed());
            cAVar.mo17511J(aqv());
            cAVar.mo17512L(aqx());
            cAVar.mo999b((C4029yK) ali);
            cAVar.mo1085k(cUn().agv());
            cAVar.mo993b(cUn().azW().mo21606Nc());
            if (bGX()) {
                mo12911b(cUn().agv(), cTQ().agx());
            }
            if (logger.isDebugEnabled()) {
                logger.debug("Shot Spawned");
            }
            cUn().afE();
        }
    }

    @C0064Am(aul = "cd3a8867cf7480b4947582e52428b33a", aum = 0)
    private void cUG() {
        if (mo7855al() != null && (mo7855al().agj() instanceof Player)) {
            Player aku = (Player) mo7855al().agj();
            NLSWindowAlert ags = (NLSWindowAlert) ala().aIY().mo6310c(NLSManager.C1472a.WINDOWALERT);
            if (isUsable() || !cxn()) {
                if (mo7855al().agr().cxn() && !mo7855al().agr().isUsable()) {
                    aku.mo14419f((C1506WA) new C0284Df(ags.dcu(), false, mo7855al().agH().mo19891ke().get()));
                    return;
                }
            } else if (mo7855al().agj() instanceof Player) {
                aku.mo14419f((C1506WA) new C0284Df(ags.dcu(), false, aqz().mo19891ke().get()));
                return;
            } else {
                return;
            }
        }
        if (logger.isDebugEnabled()) {
            logger.debug("START FIRING " + this);
        }
        if (aqj() < 0) {
            logger.warn("Shooting a weapon that does not consume ammo " + aqz().mo19891ke());
        }
        if (cUD()) {
            m20994iV(true);
            mo831ey(-1.0f);
            if (cUn() != null && (cUn() instanceof Ship) && (((Ship) cUn()).agj() instanceof Player)) {
                ((Player) ((Ship) cUn()).agj()).mo14419f((C1506WA) new C1515WI(true, mo7860sM(), this));
            }
        }
    }

    @C0064Am(aul = "8dbd2143db7d166337d8f5d5f8a07461", aum = 0)
    private void bah() {
        Player aku;
        if (logger.isDebugEnabled()) {
            logger.debug("STOP FIRING");
        }
        m20994iV(false);
        if (bGX()) {
            mo12939iR(false);
            if (cUn() != null && (cUn() instanceof Ship) && (((Ship) cUn()).agj() instanceof Player) && (aku = (Player) ((Ship) cUn()).agj()) == getPlayer()) {
                aku.mo14419f((C1506WA) new C1515WI(false, mo7860sM(), this));
            }
        }
    }

    @C0064Am(aul = "211440bea97a7ad44ac7ac00e342bf6c", aum = 0)
    @C5307aEh
    /* renamed from: U */
    private void m20959U(float f) {
    }

    @C0064Am(aul = "0ea5a7cf943f163ce6eec4f464725bab", aum = 0)
    private Weapon cUH() {
        return this;
    }

    @C0064Am(aul = "519366e581db0984b4521e07ceff6861", aum = 0)
    /* renamed from: ay */
    private void m20968ay(float f) {
        if (logger.isTraceEnabled()) {
            logger.trace("TICK: is firing?" + cUB());
        }
        if (logger.isTraceEnabled()) {
            logger.trace("getThisSideLoad: " + aqh());
        }
        if (cUB()) {
            if (aqh() <= 0) {
                if (logger.isTraceEnabled()) {
                    logger.trace("TICK: stoping fire: isFiring? " + cUB() + " ammo: " + aqh());
                }
                m20994iV(false);
            } else if (mo7855al() == null || (mo7855al().isAlive() && mo7855al().bae())) {
                mo831ey(f);
                if (bGX()) {
                    mo12939iR(true);
                    return;
                }
                return;
            } else {
                m20994iV(false);
            }
        }
        if (bGX()) {
            mo12939iR(false);
        }
    }

    @C0064Am(aul = "41bc0a59830f48f99b3bcf97a533c713", aum = 0)
    private Collection<aDR> aad() {
        if (mo7855al() == null) {
            return null;
        }
        return mo7855al().aae();
    }

    @C0064Am(aul = "c647250f4a1c71d13bece058871a6766", aum = 0)
    /* renamed from: b */
    private void m20969b(aDJ adj) {
        if (adj == cTP()) {
            mo12933cc((Actor) null);
        }
    }

    @C0064Am(aul = "228a2f63b933fb3b0f2c345ea886b0d9", aum = 0)
    /* renamed from: TO */
    private void m20956TO() {
        for (DamageType next : ala().aLs()) {
            cTR().put(next, Float.valueOf(((WeaponAdapter) m20958Te().ase()).mo5375d(next)));
        }
        m21008mp(((WeaponAdapter) m20958Te().ase()).mo12954ml());
        m20989iP(((WeaponAdapter) m20958Te().ase()).mo12955mn());
        m21009mq(((WeaponAdapter) m20958Te().ase()).mo12953in());
        m21010mr(((WeaponAdapter) m20958Te().ase()).getSpeed());
    }

    @C0064Am(aul = "f9ee9c24aa18785aa332fcd3543ad26e", aum = 0)
    /* renamed from: TK */
    private void m20954TK() {
        mo12907TP();
    }

    @C0064Am(aul = "45a03d70d237ce971f0591d0d5544061", aum = 0)
    /* renamed from: TM */
    private void m20955TM() {
        mo12907TP();
    }

    @C0064Am(aul = "84c576b13fb3738f10932b8eb60ac289", aum = 0)
    /* renamed from: TQ */
    private void m20957TQ() {
        if (m20958Te() == null) {
            logger.error(String.valueOf(bFY()) + "with NULL adapterList");
            return;
        }
        m20958Te().mo23259b((C6872avM) this);
        mo12907TP();
    }

    @C6485anp
    @C5511aMd
    /* renamed from: a.adv$a */
    public class BaseAdapter extends WeaponAdapter implements C1616Xf {

        /* renamed from: AI */
        public static final C2491fm f4354AI = null;

        /* renamed from: DJ */
        public static final C2491fm f4355DJ = null;

        /* renamed from: DK */
        public static final C2491fm f4356DK = null;

        /* renamed from: PF */
        public static final C2491fm f4357PF = null;
        public static final int _m_fieldCount = 0;
        public static final C5663aRz[] _m_fields = null;
        public static final int _m_methodCount = 0;
        public static final C2491fm[] _m_methods = null;
        /* renamed from: aT */
        public static final C5663aRz f4358aT = null;
        public static final C2491fm fjP = null;
        public static final long serialVersionUID = 0;
        /* renamed from: vb */
        public static final C2491fm f4359vb = null;
        /* renamed from: vh */
        public static final C2491fm f4360vh = null;
        public static C6494any ___iScriptClass;
        @C0064Am(aul = "1471718a3ed5bf8b46869921a98dc348", aum = 0)
        static /* synthetic */ Weapon fjO;

        static {
            m21051V();
        }

        public BaseAdapter() {
            super((C5540aNg) null);
            super.mo10S();
        }

        public BaseAdapter(C5540aNg ang) {
            super(ang);
        }

        public BaseAdapter(Weapon adv) {
            super((C5540aNg) null);
            super._m_script_init(adv);
        }

        /* renamed from: V */
        static void m21051V() {
            serialVersionUID = (long) 1;
            _m_fieldCount = WeaponAdapter._m_fieldCount + 1;
            _m_methodCount = WeaponAdapter._m_methodCount + 7;
            int i = WeaponAdapter._m_fieldCount;
            C5663aRz[] arzArr = new C5663aRz[(i + 1)];
            C5663aRz b = C5640aRc.m17844b(BaseAdapter.class, "1471718a3ed5bf8b46869921a98dc348", i);
            f4358aT = b;
            arzArr[i] = b;
            int i2 = i + 1;
            _m_fields = arzArr;
            C1634Xv.m11725a((Object[]) WeaponAdapter._m_fields, (Object[]) _m_fields);
            int i3 = WeaponAdapter._m_methodCount;
            C2491fm[] fmVarArr = new C2491fm[(i3 + 7)];
            C2491fm a = C4105zY.m41624a(BaseAdapter.class, "2046d9e8ab68418933c2097fa20b47c4", i3);
            f4354AI = a;
            fmVarArr[i3] = a;
            int i4 = i3 + 1;
            C2491fm a2 = C4105zY.m41624a(BaseAdapter.class, "2f25a7c03a75c570700c83f7e1b60534", i4);
            f4355DJ = a2;
            fmVarArr[i4] = a2;
            int i5 = i4 + 1;
            C2491fm a3 = C4105zY.m41624a(BaseAdapter.class, "9cbf3efbb2dad23eb00bae8f2df0dcfb", i5);
            f4359vb = a3;
            fmVarArr[i5] = a3;
            int i6 = i5 + 1;
            C2491fm a4 = C4105zY.m41624a(BaseAdapter.class, "e3285995ed5bb0e1729cadb4f2e88c86", i6);
            f4360vh = a4;
            fmVarArr[i6] = a4;
            int i7 = i6 + 1;
            C2491fm a5 = C4105zY.m41624a(BaseAdapter.class, "cffe87049a216cf6da3b7a90511fa2cc", i7);
            f4356DK = a5;
            fmVarArr[i7] = a5;
            int i8 = i7 + 1;
            C2491fm a6 = C4105zY.m41624a(BaseAdapter.class, "37d514ab4fbac76d94ecfebbefabe9a2", i8);
            fjP = a6;
            fmVarArr[i8] = a6;
            int i9 = i8 + 1;
            C2491fm a7 = C4105zY.m41624a(BaseAdapter.class, "4be15a237530cbbac8b29faf8057bde4", i9);
            f4357PF = a7;
            fmVarArr[i9] = a7;
            int i10 = i9 + 1;
            _m_methods = fmVarArr;
            C1634Xv.m11725a((Object[]) WeaponAdapter._m_methods, (Object[]) _m_methods);
            ___iScriptClass = aUO.m18566a(BaseAdapter.class, C6338aky.class, _m_fields, _m_methods);
        }

        @C0064Am(aul = "4be15a237530cbbac8b29faf8057bde4", aum = 0)
        /* renamed from: a */
        private void m21052a(GameObjectAdapter ato, GameObjectAdapter ato2, GameObjectAdapter ato3) {
            mo12951b((WeaponAdapter) ato, (WeaponAdapter) ato2, (WeaponAdapter) ato3);
        }

        private Weapon bRj() {
            return (Weapon) bFf().mo5608dq().mo3214p(f4358aT);
        }

        /* renamed from: s */
        private void m21059s(Weapon adv) {
            bFf().mo5608dq().mo3197f(f4358aT, adv);
        }

        /* renamed from: S */
        public void mo10S() {
            super.mo10S();
        }

        /* renamed from: T */
        public C6494any mo11T() {
            return ___iScriptClass;
        }

        /* renamed from: U */
        public C2491fm[] mo12U() {
            return _m_methods;
        }

        /* renamed from: W */
        public Object mo13W() {
            return new C6338aky(this);
        }

        /* renamed from: a */
        public Object mo14a(C0495Gr gr) {
            Object[] args = gr.getArgs();
            switch (gr.mo2417hq() - WeaponAdapter._m_methodCount) {
                case 0:
                    return new Float(m21054c((DamageType) args[0]));
                case 1:
                    return new Float(m21057mk());
                case 2:
                    return new Float(m21055im());
                case 3:
                    return new Float(m21056is());
                case 4:
                    return new Boolean(m21058mm());
                case 5:
                    m21053a((WeaponAdapter) args[0], (WeaponAdapter) args[1], (WeaponAdapter) args[2]);
                    return null;
                case 6:
                    m21052a((GameObjectAdapter) args[0], (GameObjectAdapter) args[1], (GameObjectAdapter) args[2]);
                    return null;
                default:
                    return super.mo14a(gr);
            }
        }

        /* renamed from: a */
        public void mo15a(Collection collection, C0495Gr gr) {
            super.mo15a(collection, gr);
        }

        /* renamed from: b */
        public /* bridge */ /* synthetic */ void mo12950b(GameObjectAdapter ato, GameObjectAdapter ato2, GameObjectAdapter ato3) {
            switch (bFf().mo6893i(f4357PF)) {
                case 0:
                    return;
                case 2:
                    bFf().mo5606d(new aCE(this, f4357PF, new Object[]{ato, ato2, ato3}));
                    return;
                case 3:
                    bFf().mo5606d(new aCE(this, f4357PF, new Object[]{ato, ato2, ato3}));
                    break;
            }
            m21052a(ato, ato2, ato3);
        }

        /* renamed from: b */
        public void mo12951b(WeaponAdapter fxVar, WeaponAdapter fxVar2, WeaponAdapter fxVar3) {
            switch (bFf().mo6893i(fjP)) {
                case 0:
                    return;
                case 2:
                    bFf().mo5606d(new aCE(this, fjP, new Object[]{fxVar, fxVar2, fxVar3}));
                    return;
                case 3:
                    bFf().mo5606d(new aCE(this, fjP, new Object[]{fxVar, fxVar2, fxVar3}));
                    break;
            }
            m21053a(fxVar, fxVar2, fxVar3);
        }

        /* renamed from: c */
        public C5663aRz[] mo25c() {
            return _m_fields;
        }

        /* renamed from: d */
        public float mo5375d(DamageType fr) {
            switch (bFf().mo6893i(f4354AI)) {
                case 0:
                    return 0.0f;
                case 2:
                    return ((Float) bFf().mo5606d(new aCE(this, f4354AI, new Object[]{fr}))).floatValue();
                case 3:
                    bFf().mo5606d(new aCE(this, f4354AI, new Object[]{fr}));
                    break;
            }
            return m21054c(fr);
        }

        public float getSpeed() {
            switch (bFf().mo6893i(f4360vh)) {
                case 0:
                    return 0.0f;
                case 2:
                    return ((Float) bFf().mo5606d(new aCE(this, f4360vh, new Object[0]))).floatValue();
                case 3:
                    bFf().mo5606d(new aCE(this, f4360vh, new Object[0]));
                    break;
            }
            return m21056is();
        }

        /* renamed from: in */
        public float mo12953in() {
            switch (bFf().mo6893i(f4359vb)) {
                case 0:
                    return 0.0f;
                case 2:
                    return ((Float) bFf().mo5606d(new aCE(this, f4359vb, new Object[0]))).floatValue();
                case 3:
                    bFf().mo5606d(new aCE(this, f4359vb, new Object[0]));
                    break;
            }
            return m21055im();
        }

        /* renamed from: ml */
        public float mo12954ml() {
            switch (bFf().mo6893i(f4355DJ)) {
                case 0:
                    return 0.0f;
                case 2:
                    return ((Float) bFf().mo5606d(new aCE(this, f4355DJ, new Object[0]))).floatValue();
                case 3:
                    bFf().mo5606d(new aCE(this, f4355DJ, new Object[0]));
                    break;
            }
            return m21057mk();
        }

        /* renamed from: mn */
        public boolean mo12955mn() {
            switch (bFf().mo6893i(f4356DK)) {
                case 0:
                    return false;
                case 2:
                    return ((Boolean) bFf().mo5606d(new aCE(this, f4356DK, new Object[0]))).booleanValue();
                case 3:
                    bFf().mo5606d(new aCE(this, f4356DK, new Object[0]));
                    break;
            }
            return m21058mm();
        }

        /* renamed from: t */
        public void mo12956t(Weapon adv) {
            m21059s(adv);
            super.mo10S();
        }

        @C0064Am(aul = "2046d9e8ab68418933c2097fa20b47c4", aum = 0)
        /* renamed from: c */
        private float m21054c(DamageType fr) {
            Float f = (Float) bRj().cUr().get(fr);
            if (f == null) {
                return 0.0f;
            }
            return f.floatValue();
        }

        @C0064Am(aul = "2f25a7c03a75c570700c83f7e1b60534", aum = 0)
        /* renamed from: mk */
        private float m21057mk() {
            return bRj().cUt();
        }

        @C0064Am(aul = "9cbf3efbb2dad23eb00bae8f2df0dcfb", aum = 0)
        /* renamed from: im */
        private float m21055im() {
            return bRj().cUx();
        }

        @C0064Am(aul = "e3285995ed5bb0e1729cadb4f2e88c86", aum = 0)
        /* renamed from: is */
        private float m21056is() {
            return bRj().cUz();
        }

        @C0064Am(aul = "cffe87049a216cf6da3b7a90511fa2cc", aum = 0)
        /* renamed from: mm */
        private boolean m21058mm() {
            return bRj().cUv();
        }

        @C0064Am(aul = "37d514ab4fbac76d94ecfebbefabe9a2", aum = 0)
        /* renamed from: a */
        private void m21053a(WeaponAdapter fxVar, WeaponAdapter fxVar2, WeaponAdapter fxVar3) {
            if (fxVar != null) {
                throw new IllegalArgumentException();
            } else if (fxVar3 != this) {
                throw new IllegalArgumentException("can not change base. Expected " + this + " received " + fxVar3);
            } else {
                super.mo12950b(fxVar, fxVar2, fxVar3);
            }
        }
    }
}
