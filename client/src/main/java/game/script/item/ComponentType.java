package game.script.item;

import game.network.message.externalizable.aCE;
import game.script.resource.Asset;
import game.script.ship.SectorCategory;
import game.script.ship.Slot;
import logic.baa.*;
import logic.data.mbean.ayR;
import logic.res.code.C5640aRc;
import logic.res.code.C5663aRz;
import logic.res.code.C6494any;
import logic.res.html.C2491fm;
import p001a.*;

import java.util.Collection;

@C0566Hp
@C5829abJ("2.0.2")
@C6485anp
@C5511aMd
/* renamed from: a.aEF */
/* compiled from: a */
public abstract class ComponentType extends BaseComponentType implements C1616Xf {

    /* renamed from: KP */
    public static final C5663aRz f2666KP = null;
    public static final int _m_fieldCount = 0;
    public static final C5663aRz[] _m_fields = null;
    public static final int _m_methodCount = 0;
    public static final C2491fm[] _m_methods = null;
    public static final C2491fm gvJ = null;
    public static final C2491fm hHA = null;
    public static final C2491fm hHB = null;
    public static final C2491fm hHC = null;
    public static final C2491fm hHy = null;
    public static final C2491fm hHz = null;
    public static final C5663aRz hiu = null;
    public static final C5663aRz hiv = null;
    public static final long serialVersionUID = 0;
    public static C6494any ___iScriptClass;
    @C0064Am(aul = "b108efa1c230e6231b8a836400b0180b", aum = 0)
    private static SectorCategory cYq;
    @C0064Am(aul = "0c963138aa468449e30970828a51f245", aum = 1)
    private static Slot.C2893a cYr;
    @C0064Am(aul = "7a543ee56aec5700ea9ebce71bb5717c", aum = 2)
    private static Asset cYs;

    static {
        m14033V();
    }

    public ComponentType() {
        super((C5540aNg) null);
        super.mo10S();
    }

    public ComponentType(C5540aNg ang) {
        super(ang);
    }

    /* renamed from: V */
    static void m14033V() {
        serialVersionUID = (long) 1;
        _m_fieldCount = BaseComponentType._m_fieldCount + 3;
        _m_methodCount = BaseComponentType._m_methodCount + 6;
        int i = BaseComponentType._m_fieldCount;
        C5663aRz[] arzArr = new C5663aRz[(i + 3)];
        C5663aRz b = C5640aRc.m17844b(ComponentType.class, "b108efa1c230e6231b8a836400b0180b", i);
        f2666KP = b;
        arzArr[i] = b;
        int i2 = i + 1;
        C5663aRz b2 = C5640aRc.m17844b(ComponentType.class, "0c963138aa468449e30970828a51f245", i2);
        hiu = b2;
        arzArr[i2] = b2;
        int i3 = i2 + 1;
        C5663aRz b3 = C5640aRc.m17844b(ComponentType.class, "7a543ee56aec5700ea9ebce71bb5717c", i3);
        hiv = b3;
        arzArr[i3] = b3;
        int i4 = i3 + 1;
        _m_fields = arzArr;
        C1634Xv.m11725a((Object[]) BaseComponentType._m_fields, (Object[]) _m_fields);
        int i5 = BaseComponentType._m_methodCount;
        C2491fm[] fmVarArr = new C2491fm[(i5 + 6)];
        C2491fm a = C4105zY.m41624a(ComponentType.class, "c9cf6a50b179de872dc418dbdc7e8346", i5);
        hHy = a;
        fmVarArr[i5] = a;
        int i6 = i5 + 1;
        C2491fm a2 = C4105zY.m41624a(ComponentType.class, "631dc854a068031b8ee80e2389320e5f", i6);
        hHz = a2;
        fmVarArr[i6] = a2;
        int i7 = i6 + 1;
        C2491fm a3 = C4105zY.m41624a(ComponentType.class, "9bfb08a00772647c3f0dd62b6bc95557", i7);
        gvJ = a3;
        fmVarArr[i7] = a3;
        int i8 = i7 + 1;
        C2491fm a4 = C4105zY.m41624a(ComponentType.class, "e4bce24f2d4f3f08671f99a4155523c0", i8);
        hHA = a4;
        fmVarArr[i8] = a4;
        int i9 = i8 + 1;
        C2491fm a5 = C4105zY.m41624a(ComponentType.class, "0a11c4e19df6b30b402e1af56bf2f1d6", i9);
        hHB = a5;
        fmVarArr[i9] = a5;
        int i10 = i9 + 1;
        C2491fm a6 = C4105zY.m41624a(ComponentType.class, "d4c7204738adcd9d6a6f3de9d3a34e38", i10);
        hHC = a6;
        fmVarArr[i10] = a6;
        int i11 = i10 + 1;
        _m_methods = fmVarArr;
        C1634Xv.m11725a((Object[]) BaseComponentType._m_methods, (Object[]) _m_methods);
        ___iScriptClass = aUO.m18566a(ComponentType.class, ayR.class, _m_fields, _m_methods);
    }

    /* renamed from: a */
    private void m14034a(Slot.C2893a aVar) {
        bFf().mo5608dq().mo3197f(hiu, aVar);
    }

    /* renamed from: bP */
    private void m14036bP(Asset tCVar) {
        bFf().mo5608dq().mo3197f(hiv, tCVar);
    }

    private SectorCategory cJN() {
        return (SectorCategory) bFf().mo5608dq().mo3214p(f2666KP);
    }

    private Slot.C2893a cJO() {
        return (Slot.C2893a) bFf().mo5608dq().mo3214p(hiu);
    }

    private Asset cJP() {
        return (Asset) bFf().mo5608dq().mo3214p(hiv);
    }

    /* renamed from: h */
    private void m14038h(SectorCategory fMVar) {
        bFf().mo5608dq().mo3197f(f2666KP, fMVar);
    }

    /* renamed from: T */
    public C6494any mo11T() {
        return ___iScriptClass;
    }

    /* renamed from: U */
    public C2491fm[] mo12U() {
        return _m_methods;
    }

    /* renamed from: W */
    public Object mo13W() {
        return new ayR(this);
    }

    /* renamed from: a */
    public Object mo14a(C0495Gr gr) {
        Object[] args = gr.getArgs();
        switch (gr.mo2417hq() - BaseComponentType._m_methodCount) {
            case 0:
                return cXU();
            case 1:
                m14039m((SectorCategory) args[0]);
                return null;
            case 2:
                return cuG();
            case 3:
                m14035b((Slot.C2893a) args[0]);
                return null;
            case 4:
                return cXW();
            case 5:
                m14037bZ((Asset) args[0]);
                return null;
            default:
                return super.mo14a(gr);
        }
    }

    /* renamed from: a */
    public void mo15a(Collection collection, C0495Gr gr) {
        super.mo15a(collection, gr);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Size")
    /* renamed from: c */
    public void mo8551c(Slot.C2893a aVar) {
        switch (bFf().mo6893i(hHA)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, hHA, new Object[]{aVar}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, hHA, new Object[]{aVar}));
                break;
        }
        m14035b(aVar);
    }

    /* renamed from: c */
    public C5663aRz[] mo25c() {
        return _m_fields;
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Category")
    public SectorCategory cXV() {
        switch (bFf().mo6893i(hHy)) {
            case 0:
                return null;
            case 2:
                return (SectorCategory) bFf().mo5606d(new aCE(this, hHy, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, hHy, new Object[0]));
                break;
        }
        return cXU();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Equip S F X")
    public Asset cXX() {
        switch (bFf().mo6893i(hHB)) {
            case 0:
                return null;
            case 2:
                return (Asset) bFf().mo5606d(new aCE(this, hHB, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, hHB, new Object[0]));
                break;
        }
        return cXW();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Equip S F X")
    /* renamed from: ca */
    public void mo8554ca(Asset tCVar) {
        switch (bFf().mo6893i(hHC)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, hHC, new Object[]{tCVar}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, hHC, new Object[]{tCVar}));
                break;
        }
        m14037bZ(tCVar);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Size")
    public Slot.C2893a cuH() {
        switch (bFf().mo6893i(gvJ)) {
            case 0:
                return null;
            case 2:
                return (Slot.C2893a) bFf().mo5606d(new aCE(this, gvJ, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, gvJ, new Object[0]));
                break;
        }
        return cuG();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Category")
    /* renamed from: n */
    public void mo8556n(SectorCategory fMVar) {
        switch (bFf().mo6893i(hHz)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, hHz, new Object[]{fMVar}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, hHz, new Object[]{fMVar}));
                break;
        }
        m14039m(fMVar);
    }

    /* renamed from: S */
    public void mo10S() {
        super.mo10S();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Category")
    @C0064Am(aul = "c9cf6a50b179de872dc418dbdc7e8346", aum = 0)
    private SectorCategory cXU() {
        return cJN();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Category")
    @C0064Am(aul = "631dc854a068031b8ee80e2389320e5f", aum = 0)
    /* renamed from: m */
    private void m14039m(SectorCategory fMVar) {
        m14038h(fMVar);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Size")
    @C0064Am(aul = "9bfb08a00772647c3f0dd62b6bc95557", aum = 0)
    private Slot.C2893a cuG() {
        return cJO();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Size")
    @C0064Am(aul = "e4bce24f2d4f3f08671f99a4155523c0", aum = 0)
    /* renamed from: b */
    private void m14035b(Slot.C2893a aVar) {
        m14034a(aVar);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Equip S F X")
    @C0064Am(aul = "0a11c4e19df6b30b402e1af56bf2f1d6", aum = 0)
    private Asset cXW() {
        return cJP();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Equip S F X")
    @C0064Am(aul = "d4c7204738adcd9d6a6f3de9d3a34e38", aum = 0)
    /* renamed from: bZ */
    private void m14037bZ(Asset tCVar) {
        m14036bP(tCVar);
    }
}
