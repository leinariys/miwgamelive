package game.script.item;

import game.network.message.externalizable.aCE;
import game.network.message.serializable.C3438ra;
import game.script.pda.DatabaseCategory;
import game.script.pda.TaikopediaEntry;
import game.script.resource.Asset;
import logic.baa.*;
import logic.data.mbean.C1778aD;
import logic.res.code.C5640aRc;
import logic.res.code.C5663aRz;
import logic.res.code.C6494any;
import logic.res.html.C2491fm;
import p001a.*;

import java.util.Collection;
import java.util.Collections;
import java.util.List;

@C0566Hp
@C5511aMd
@C6485anp
/* renamed from: a.aGy  reason: case insensitive filesystem */
/* compiled from: a */
public class RawMaterialType extends RawMaterialBaseType implements C1616Xf {

    /* renamed from: BR */
    public static final C5663aRz f2893BR = null;
    public static final int _m_fieldCount = 0;
    public static final C5663aRz[] _m_fields = null;
    public static final int _m_methodCount = 0;
    public static final C2491fm[] _m_methods = null;
    public static final C5663aRz aMg = null;
    public static final C5663aRz aMh = null;
    public static final C5663aRz aMi = null;
    public static final C2491fm aMk = null;
    public static final C2491fm aMl = null;
    public static final C2491fm aMm = null;
    public static final C2491fm aMn = null;
    public static final C2491fm aMo = null;
    public static final C2491fm aMr = null;
    public static final C2491fm aMs = null;
    public static final C2491fm aMu = null;
    public static final C2491fm aMv = null;
    public static final C5663aRz awd = null;
    /* renamed from: dN */
    public static final C2491fm f2894dN = null;
    public static final C2491fm dyZ = null;
    public static final C2491fm dza = null;
    public static final C2491fm hPU = null;
    public static final C2491fm hPV = null;
    public static final C2491fm hPW = null;
    public static final long serialVersionUID = 0;
    public static C6494any ___iScriptClass;
    @C0064Am(aul = "705b2ef607eb5e6f12c877ca759f83bd", aum = 0)

    /* renamed from: jS */
    private static DatabaseCategory f2895jS;
    @C0064Am(aul = "12d78c3588efb2e5616c79699d49ba9f", aum = 1)

    /* renamed from: jT */
    private static C3438ra<TaikopediaEntry> f2896jT;
    @C0064Am(aul = "5f53d0ddef0171d5e45fc6f1e0d24947", aum = 2)

    /* renamed from: jU */
    private static Asset f2897jU;
    @C0064Am(aul = "9d31a43f7d727e51f3503f62e38e13ea", aum = 3)

    /* renamed from: jV */
    private static float f2898jV;
    @C0064Am(aul = "a1ac1c216ff14fae6a8369c2a96712d5", aum = 4)

    /* renamed from: jW */
    private static int f2899jW;

    static {
        m15041V();
    }

    public RawMaterialType() {
        super((C5540aNg) null);
        super.mo10S();
    }

    public RawMaterialType(C5540aNg ang) {
        super(ang);
    }

    /* renamed from: V */
    static void m15041V() {
        serialVersionUID = (long) 1;
        _m_fieldCount = RawMaterialBaseType._m_fieldCount + 5;
        _m_methodCount = RawMaterialBaseType._m_methodCount + 15;
        int i = RawMaterialBaseType._m_fieldCount;
        C5663aRz[] arzArr = new C5663aRz[(i + 5)];
        C5663aRz b = C5640aRc.m17844b(RawMaterialType.class, "705b2ef607eb5e6f12c877ca759f83bd", i);
        aMh = b;
        arzArr[i] = b;
        int i2 = i + 1;
        C5663aRz b2 = C5640aRc.m17844b(RawMaterialType.class, "12d78c3588efb2e5616c79699d49ba9f", i2);
        awd = b2;
        arzArr[i2] = b2;
        int i3 = i2 + 1;
        C5663aRz b3 = C5640aRc.m17844b(RawMaterialType.class, "5f53d0ddef0171d5e45fc6f1e0d24947", i3);
        aMg = b3;
        arzArr[i3] = b3;
        int i4 = i3 + 1;
        C5663aRz b4 = C5640aRc.m17844b(RawMaterialType.class, "9d31a43f7d727e51f3503f62e38e13ea", i4);
        aMi = b4;
        arzArr[i4] = b4;
        int i5 = i4 + 1;
        C5663aRz b5 = C5640aRc.m17844b(RawMaterialType.class, "a1ac1c216ff14fae6a8369c2a96712d5", i5);
        f2893BR = b5;
        arzArr[i5] = b5;
        int i6 = i5 + 1;
        _m_fields = arzArr;
        C1634Xv.m11725a((Object[]) RawMaterialBaseType._m_fields, (Object[]) _m_fields);
        int i7 = RawMaterialBaseType._m_methodCount;
        C2491fm[] fmVarArr = new C2491fm[(i7 + 15)];
        C2491fm a = C4105zY.m41624a(RawMaterialType.class, "e6396751830846091273b06e5048e179", i7);
        aMn = a;
        fmVarArr[i7] = a;
        int i8 = i7 + 1;
        C2491fm a2 = C4105zY.m41624a(RawMaterialType.class, "4c7eb17e08a449b466add77470c436d5", i8);
        aMo = a2;
        fmVarArr[i8] = a2;
        int i9 = i8 + 1;
        C2491fm a3 = C4105zY.m41624a(RawMaterialType.class, "a80bae377063f47e6fa551e79e9b2aae", i9);
        aMl = a3;
        fmVarArr[i9] = a3;
        int i10 = i9 + 1;
        C2491fm a4 = C4105zY.m41624a(RawMaterialType.class, "7194a9d9c705ac6f7de29641483e07df", i10);
        aMm = a4;
        fmVarArr[i10] = a4;
        int i11 = i10 + 1;
        C2491fm a5 = C4105zY.m41624a(RawMaterialType.class, "9f49f8b035689654cd7e704203f79c80", i11);
        dyZ = a5;
        fmVarArr[i11] = a5;
        int i12 = i11 + 1;
        C2491fm a6 = C4105zY.m41624a(RawMaterialType.class, "dafdb5c59c48c8102a36422f8b16504c", i12);
        dza = a6;
        fmVarArr[i12] = a6;
        int i13 = i12 + 1;
        C2491fm a7 = C4105zY.m41624a(RawMaterialType.class, "89149011d630d28b06f1962b07f2024c", i13);
        aMr = a7;
        fmVarArr[i13] = a7;
        int i14 = i13 + 1;
        C2491fm a8 = C4105zY.m41624a(RawMaterialType.class, "964a9201599c93f3642a65809a38621d", i14);
        aMs = a8;
        fmVarArr[i14] = a8;
        int i15 = i14 + 1;
        C2491fm a9 = C4105zY.m41624a(RawMaterialType.class, "88d8f21664911c9528698116afed16dc", i15);
        aMu = a9;
        fmVarArr[i15] = a9;
        int i16 = i15 + 1;
        C2491fm a10 = C4105zY.m41624a(RawMaterialType.class, "a0890f5fdeb080eb7283ae7c590f99b3", i16);
        aMv = a10;
        fmVarArr[i16] = a10;
        int i17 = i16 + 1;
        C2491fm a11 = C4105zY.m41624a(RawMaterialType.class, "9f1043905ce9c09e06fec2c56a983f5f", i17);
        hPU = a11;
        fmVarArr[i17] = a11;
        int i18 = i17 + 1;
        C2491fm a12 = C4105zY.m41624a(RawMaterialType.class, "2c7d87675289081853a4b4f1a9958a14", i18);
        hPV = a12;
        fmVarArr[i18] = a12;
        int i19 = i18 + 1;
        C2491fm a13 = C4105zY.m41624a(RawMaterialType.class, "22854dfc041eb47e60c16077d8c0c605", i19);
        hPW = a13;
        fmVarArr[i19] = a13;
        int i20 = i19 + 1;
        C2491fm a14 = C4105zY.m41624a(RawMaterialType.class, "7a8905f9ced61210419b4326fc4b99d5", i20);
        f2894dN = a14;
        fmVarArr[i20] = a14;
        int i21 = i20 + 1;
        C2491fm a15 = C4105zY.m41624a(RawMaterialType.class, "091d67da6bd4d73014ec3009151d5849", i21);
        aMk = a15;
        fmVarArr[i21] = a15;
        int i22 = i21 + 1;
        _m_methods = fmVarArr;
        C1634Xv.m11725a((Object[]) RawMaterialBaseType._m_methods, (Object[]) _m_methods);
        ___iScriptClass = aUO.m18566a(RawMaterialType.class, C1778aD.class, _m_fields, _m_methods);
    }

    /* renamed from: L */
    private void m15031L(Asset tCVar) {
        bFf().mo5608dq().mo3197f(aMg, tCVar);
    }

    /* renamed from: Ll */
    private C3438ra m15032Ll() {
        return (C3438ra) bFf().mo5608dq().mo3214p(awd);
    }

    /* renamed from: RQ */
    private Asset m15034RQ() {
        return (Asset) bFf().mo5608dq().mo3214p(aMg);
    }

    /* renamed from: RR */
    private DatabaseCategory m15035RR() {
        return (DatabaseCategory) bFf().mo5608dq().mo3214p(aMh);
    }

    /* renamed from: RS */
    private float m15036RS() {
        return bFf().mo5608dq().mo3211m(aMi);
    }

    @C0064Am(aul = "091d67da6bd4d73014ec3009151d5849", aum = 0)
    /* renamed from: RV */
    private List m15037RV() {
        return bgF();
    }

    /* renamed from: a */
    private void m15042a(DatabaseCategory aik) {
        bFf().mo5608dq().mo3197f(aMh, aik);
    }

    private int bNZ() {
        return bFf().mo5608dq().mo3212n(f2893BR);
    }

    /* renamed from: cx */
    private void m15047cx(float f) {
        bFf().mo5608dq().mo3150a(aMi, f);
    }

    /* renamed from: pO */
    private void m15049pO(int i) {
        bFf().mo5608dq().mo3183b(f2893BR, i);
    }

    /* renamed from: x */
    private void m15050x(C3438ra raVar) {
        bFf().mo5608dq().mo3197f(awd, raVar);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Pda Render")
    /* renamed from: N */
    public void mo9138N(Asset tCVar) {
        switch (bFf().mo6893i(aMs)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, aMs, new Object[]{tCVar}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, aMs, new Object[]{tCVar}));
                break;
        }
        m15033M(tCVar);
    }

    /* renamed from: RW */
    public /* bridge */ /* synthetic */ List mo183RW() {
        switch (bFf().mo6893i(aMk)) {
            case 0:
                return null;
            case 2:
                return (List) bFf().mo5606d(new aCE(this, aMk, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, aMk, new Object[0]));
                break;
        }
        return m15037RV();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Database Category")
    /* renamed from: RY */
    public DatabaseCategory mo184RY() {
        switch (bFf().mo6893i(aMn)) {
            case 0:
                return null;
            case 2:
                return (DatabaseCategory) bFf().mo5606d(new aCE(this, aMn, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, aMn, new Object[0]));
                break;
        }
        return m15038RX();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Pda Render")
    /* renamed from: Se */
    public Asset mo187Se() {
        switch (bFf().mo6893i(aMr)) {
            case 0:
                return null;
            case 2:
                return (Asset) bFf().mo5606d(new aCE(this, aMr, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, aMr, new Object[0]));
                break;
        }
        return m15039Sd();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Pda M P Multiplier")
    /* renamed from: Sg */
    public float mo188Sg() {
        switch (bFf().mo6893i(aMu)) {
            case 0:
                return 0.0f;
            case 2:
                return ((Float) bFf().mo5606d(new aCE(this, aMu, new Object[0]))).floatValue();
            case 3:
                bFf().mo5606d(new aCE(this, aMu, new Object[0]));
                break;
        }
        return m15040Sf();
    }

    /* renamed from: T */
    public C6494any mo11T() {
        return ___iScriptClass;
    }

    /* renamed from: U */
    public C2491fm[] mo12U() {
        return _m_methods;
    }

    /* renamed from: W */
    public Object mo13W() {
        return new C1778aD(this);
    }

    /* renamed from: a */
    public Object mo14a(C0495Gr gr) {
        Object[] args = gr.getArgs();
        switch (gr.mo2417hq() - RawMaterialBaseType._m_methodCount) {
            case 0:
                return m15038RX();
            case 1:
                m15045b((DatabaseCategory) args[0]);
                return null;
            case 2:
                m15043a((TaikopediaEntry) args[0]);
                return null;
            case 3:
                m15046c((TaikopediaEntry) args[0]);
                return null;
            case 4:
                return bgE();
            case 5:
                return bgG();
            case 6:
                return m15039Sd();
            case 7:
                m15033M((Asset) args[0]);
                return null;
            case 8:
                return new Float(m15040Sf());
            case 9:
                m15048cy(((Float) args[0]).floatValue());
                return null;
            case 10:
                return new Integer(dam());
            case 11:
                m15051ya(((Integer) args[0]).intValue());
                return null;
            case 12:
                return dao();
            case 13:
                return m15044aT();
            case 14:
                return m15037RV();
            default:
                return super.mo14a(gr);
        }
    }

    /* renamed from: a */
    public void mo15a(Collection collection, C0495Gr gr) {
        super.mo15a(collection, gr);
    }

    /* access modifiers changed from: protected */
    /* renamed from: aU */
    public <T extends aDJ> T mo745aU() {
        switch (bFf().mo6893i(f2894dN)) {
            case 0:
                return null;
            case 2:
                return (aDJ) bFf().mo5606d(new aCE(this, f2894dN, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f2894dN, new Object[0]));
                break;
        }
        return m15044aT();
    }

    @C3248pc(aYR = C3248pc.C3250b.ADDER, displayName = "Taikopedia Entries")
    /* renamed from: b */
    public void mo9139b(TaikopediaEntry aiz) {
        switch (bFf().mo6893i(aMl)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, aMl, new Object[]{aiz}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, aMl, new Object[]{aiz}));
                break;
        }
        m15043a(aiz);
    }

    public C3438ra<TaikopediaEntry> bgF() {
        switch (bFf().mo6893i(dyZ)) {
            case 0:
                return null;
            case 2:
                return (C3438ra) bFf().mo5606d(new aCE(this, dyZ, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, dyZ, new Object[0]));
                break;
        }
        return bgE();
    }

    @C3248pc(aYR = C3248pc.C3250b.ITERATOR, displayName = "Taikopedia Entries")
    public List<TaikopediaEntry> bgH() {
        switch (bFf().mo6893i(dza)) {
            case 0:
                return null;
            case 2:
                return (List) bFf().mo5606d(new aCE(this, dza, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, dza, new Object[0]));
                break;
        }
        return bgG();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Database Category")
    /* renamed from: c */
    public void mo9142c(DatabaseCategory aik) {
        switch (bFf().mo6893i(aMo)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, aMo, new Object[]{aik}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, aMo, new Object[]{aik}));
                break;
        }
        m15045b(aik);
    }

    /* renamed from: c */
    public C5663aRz[] mo25c() {
        return _m_fields;
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Pda M P Multiplier")
    /* renamed from: cz */
    public void mo9143cz(float f) {
        switch (bFf().mo6893i(aMv)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, aMv, new Object[]{new Float(f)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, aMv, new Object[]{new Float(f)}));
                break;
        }
        m15048cy(f);
    }

    @C3248pc(aYR = C3248pc.C3250b.REMOVER, displayName = "Taikopedia Entries")
    /* renamed from: d */
    public void mo9144d(TaikopediaEntry aiz) {
        switch (bFf().mo6893i(aMm)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, aMm, new Object[]{aiz}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, aMm, new Object[]{aiz}));
                break;
        }
        m15046c(aiz);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Merit Points")
    public int dan() {
        switch (bFf().mo6893i(hPU)) {
            case 0:
                return 0;
            case 2:
                return ((Integer) bFf().mo5606d(new aCE(this, hPU, new Object[0]))).intValue();
            case 3:
                bFf().mo5606d(new aCE(this, hPU, new Object[0]));
                break;
        }
        return dam();
    }

    public RawMaterial dap() {
        switch (bFf().mo6893i(hPW)) {
            case 0:
                return null;
            case 2:
                return (RawMaterial) bFf().mo5606d(new aCE(this, hPW, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, hPW, new Object[0]));
                break;
        }
        return dao();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Merit Points")
    /* renamed from: yb */
    public void mo9147yb(int i) {
        switch (bFf().mo6893i(hPV)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, hPV, new Object[]{new Integer(i)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, hPV, new Object[]{new Integer(i)}));
                break;
        }
        m15051ya(i);
    }

    /* renamed from: S */
    public void mo10S() {
        super.mo10S();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Database Category")
    @C0064Am(aul = "e6396751830846091273b06e5048e179", aum = 0)
    /* renamed from: RX */
    private DatabaseCategory m15038RX() {
        return m15035RR();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Database Category")
    @C0064Am(aul = "4c7eb17e08a449b466add77470c436d5", aum = 0)
    /* renamed from: b */
    private void m15045b(DatabaseCategory aik) {
        m15042a(aik);
    }

    @C3248pc(aYR = C3248pc.C3250b.ADDER, displayName = "Taikopedia Entries")
    @C0064Am(aul = "a80bae377063f47e6fa551e79e9b2aae", aum = 0)
    /* renamed from: a */
    private void m15043a(TaikopediaEntry aiz) {
        m15032Ll().add(aiz);
    }

    @C3248pc(aYR = C3248pc.C3250b.REMOVER, displayName = "Taikopedia Entries")
    @C0064Am(aul = "7194a9d9c705ac6f7de29641483e07df", aum = 0)
    /* renamed from: c */
    private void m15046c(TaikopediaEntry aiz) {
        m15032Ll().remove(aiz);
    }

    @C0064Am(aul = "9f49f8b035689654cd7e704203f79c80", aum = 0)
    private C3438ra<TaikopediaEntry> bgE() {
        return m15032Ll();
    }

    @C3248pc(aYR = C3248pc.C3250b.ITERATOR, displayName = "Taikopedia Entries")
    @C0064Am(aul = "dafdb5c59c48c8102a36422f8b16504c", aum = 0)
    private List<TaikopediaEntry> bgG() {
        return Collections.unmodifiableList(m15032Ll());
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Pda Render")
    @C0064Am(aul = "89149011d630d28b06f1962b07f2024c", aum = 0)
    /* renamed from: Sd */
    private Asset m15039Sd() {
        return m15034RQ();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Pda Render")
    @C0064Am(aul = "964a9201599c93f3642a65809a38621d", aum = 0)
    /* renamed from: M */
    private void m15033M(Asset tCVar) {
        m15031L(tCVar);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Pda M P Multiplier")
    @C0064Am(aul = "88d8f21664911c9528698116afed16dc", aum = 0)
    /* renamed from: Sf */
    private float m15040Sf() {
        return m15036RS();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Pda M P Multiplier")
    @C0064Am(aul = "a0890f5fdeb080eb7283ae7c590f99b3", aum = 0)
    /* renamed from: cy */
    private void m15048cy(float f) {
        m15047cx(f);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Merit Points")
    @C0064Am(aul = "9f1043905ce9c09e06fec2c56a983f5f", aum = 0)
    private int dam() {
        return bNZ();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Merit Points")
    @C0064Am(aul = "2c7d87675289081853a4b4f1a9958a14", aum = 0)
    /* renamed from: ya */
    private void m15051ya(int i) {
        m15049pO(i);
    }

    @C0064Am(aul = "22854dfc041eb47e60c16077d8c0c605", aum = 0)
    private RawMaterial dao() {
        return (RawMaterial) mo745aU();
    }

    @C0064Am(aul = "7a8905f9ced61210419b4326fc4b99d5", aum = 0)
    /* renamed from: aT */
    private <T extends aDJ> T m15044aT() {
        T t = (RawMaterial) bFf().mo6865M(RawMaterial.class);
        t.mo12458e(this);
        return t;
    }
}
