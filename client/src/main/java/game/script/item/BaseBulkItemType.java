package game.script.item;

import game.network.message.externalizable.aCE;
import logic.baa.C1616Xf;
import logic.baa.C1634Xv;
import logic.baa.C5511aMd;
import logic.baa.C6485anp;
import logic.data.mbean.C4023yE;
import logic.res.code.C5663aRz;
import logic.res.code.C6494any;
import logic.res.html.C2491fm;
import p001a.*;

import java.util.Collection;
import java.util.Set;

@C5511aMd
@C6485anp
/* renamed from: a.aj */
/* compiled from: a */
public abstract class BaseBulkItemType extends ItemType implements C1616Xf {
    public static final int _m_fieldCount = 0;
    public static final C5663aRz[] _m_fields = null;
    public static final int _m_methodCount = 0;
    public static final C2491fm[] _m_methods = null;
    /* renamed from: hR */
    public static final C2491fm f4674hR = null;
    /* renamed from: hS */
    public static final C2491fm f4675hS = null;
    /* renamed from: hT */
    public static final C2491fm f4676hT = null;
    public static final long serialVersionUID = 0;
    public static C6494any ___iScriptClass;

    static {
        m22730V();
    }

    public BaseBulkItemType() {
        super((C5540aNg) null);
        super.mo10S();
    }

    public BaseBulkItemType(C5540aNg ang) {
        super(ang);
    }

    /* renamed from: V */
    static void m22730V() {
        serialVersionUID = (long) 1;
        _m_fieldCount = ItemType._m_fieldCount + 0;
        _m_methodCount = ItemType._m_methodCount + 3;
        _m_fields = new C5663aRz[(ItemType._m_fieldCount + 0)];
        C1634Xv.m11725a((Object[]) ItemType._m_fields, (Object[]) _m_fields);
        int i = ItemType._m_methodCount;
        C2491fm[] fmVarArr = new C2491fm[(i + 3)];
        C2491fm a = C4105zY.m41624a(BaseBulkItemType.class, "54e45ee2ccbdba6147e573203f852981", i);
        f4674hR = a;
        fmVarArr[i] = a;
        int i2 = i + 1;
        C2491fm a2 = C4105zY.m41624a(BaseBulkItemType.class, "4bfe3bf2e54adc494113a5b4b78e8883", i2);
        f4675hS = a2;
        fmVarArr[i2] = a2;
        int i3 = i2 + 1;
        C2491fm a3 = C4105zY.m41624a(BaseBulkItemType.class, "6c207fbb3f6fc2dd6d48fb2133c7f78a", i3);
        f4676hT = a3;
        fmVarArr[i3] = a3;
        int i4 = i3 + 1;
        _m_methods = fmVarArr;
        C1634Xv.m11725a((Object[]) ItemType._m_methods, (Object[]) _m_methods);
        ___iScriptClass = aUO.m18566a(BaseBulkItemType.class, C4023yE.class, _m_fields, _m_methods);
    }

    @C0064Am(aul = "6c207fbb3f6fc2dd6d48fb2133c7f78a", aum = 0)
    @C5566aOg
    @C2499fr
    /* renamed from: a */
    private void m22731a(ItemLocation aag, int i) {
        throw new aWi(new aCE(this, f4676hT, new Object[]{aag, new Integer(i)}));
    }

    @C0064Am(aul = "54e45ee2ccbdba6147e573203f852981", aum = 0)
    /* renamed from: dW */
    private int m22732dW() {
        throw new aWi(new aCE(this, f4674hR, new Object[0]));
    }

    @C0064Am(aul = "4bfe3bf2e54adc494113a5b4b78e8883", aum = 0)
    @C5566aOg
    /* renamed from: n */
    private Set<BulkItem> m22733n(int i) {
        throw new aWi(new aCE(this, f4675hS, new Object[]{new Integer(i)}));
    }

    /* renamed from: T */
    public C6494any mo11T() {
        return ___iScriptClass;
    }

    /* renamed from: U */
    public C2491fm[] mo12U() {
        return _m_methods;
    }

    /* renamed from: W */
    public Object mo13W() {
        return new C4023yE(this);
    }

    /* renamed from: a */
    public Object mo14a(C0495Gr gr) {
        Object[] args = gr.getArgs();
        switch (gr.mo2417hq() - ItemType._m_methodCount) {
            case 0:
                return new Integer(m22732dW());
            case 1:
                return m22733n(((Integer) args[0]).intValue());
            case 2:
                m22731a((ItemLocation) args[0], ((Integer) args[1]).intValue());
                return null;
            default:
                return super.mo14a(gr);
        }
    }

    /* renamed from: a */
    public void mo15a(Collection collection, C0495Gr gr) {
        super.mo15a(collection, gr);
    }

    @C5566aOg
    @C2499fr
    /* renamed from: b */
    public void mo13879b(ItemLocation aag, int i) {
        switch (bFf().mo6893i(f4676hT)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f4676hT, new Object[]{aag, new Integer(i)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f4676hT, new Object[]{aag, new Integer(i)}));
                break;
        }
        m22731a(aag, i);
    }

    /* renamed from: c */
    public C5663aRz[] mo25c() {
        return _m_fields;
    }

    /* renamed from: dX */
    public int mo13880dX() {
        switch (bFf().mo6893i(f4674hR)) {
            case 0:
                return 0;
            case 2:
                return ((Integer) bFf().mo5606d(new aCE(this, f4674hR, new Object[0]))).intValue();
            case 3:
                bFf().mo5606d(new aCE(this, f4674hR, new Object[0]));
                break;
        }
        return m22732dW();
    }

    @C5566aOg
    /* renamed from: o */
    public Set<BulkItem> mo13881o(int i) {
        switch (bFf().mo6893i(f4675hS)) {
            case 0:
                return null;
            case 2:
                return (Set) bFf().mo5606d(new aCE(this, f4675hS, new Object[]{new Integer(i)}));
            case 3:
                bFf().mo5606d(new aCE(this, f4675hS, new Object[]{new Integer(i)}));
                break;
        }
        return m22733n(i);
    }

    /* renamed from: S */
    public void mo10S() {
        super.mo10S();
    }
}
