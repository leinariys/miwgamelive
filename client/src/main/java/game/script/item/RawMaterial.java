package game.script.item;

import game.network.message.externalizable.aCE;
import game.network.message.serializable.C3438ra;
import game.script.pda.DatabaseCategory;
import game.script.pda.TaikopediaEntry;
import game.script.resource.Asset;
import logic.baa.C1616Xf;
import logic.baa.C1634Xv;
import logic.baa.C5511aMd;
import logic.baa.C6485anp;
import logic.data.mbean.C1396UT;
import logic.res.code.C5640aRc;
import logic.res.code.C5663aRz;
import logic.res.code.C6494any;
import logic.res.html.C2491fm;
import p001a.*;

import java.util.Collection;

@C2712iu(mo19786Bt = RawMaterialBaseType.class)
@C5511aMd
@C6485anp
/* renamed from: a.abK  reason: case insensitive filesystem */
/* compiled from: a */
public class RawMaterial extends BulkItem implements C1616Xf {

    /* renamed from: BR */
    public static final C5663aRz f4127BR = null;
    public static final int _m_fieldCount = 0;
    public static final C5663aRz[] _m_fields = null;
    public static final int _m_methodCount = 0;
    public static final C2491fm[] _m_methods = null;
    public static final C5663aRz aMg = null;
    public static final C5663aRz aMh = null;
    public static final C5663aRz aMi = null;
    public static final C5663aRz awd = null;
    public static final C2491fm eZK = null;
    public static final C2491fm esw = null;
    public static final long serialVersionUID = 0;
    public static C6494any ___iScriptClass;
    @C0064Am(aul = "7d45a02a72980654cfa0eae714d593ce", aum = 0)

    /* renamed from: jS */
    private static DatabaseCategory f4128jS;
    @C0064Am(aul = "b298d30341214badb1ea8f3951e39a01", aum = 1)

    /* renamed from: jT */
    private static C3438ra<TaikopediaEntry> f4129jT;
    @C0064Am(aul = "87ff61ccace573a3139651a313d63c80", aum = 2)

    /* renamed from: jU */
    private static Asset f4130jU;
    @C0064Am(aul = "da3d56c88a5770f209d55d6b6cb7e99e", aum = 3)

    /* renamed from: jV */
    private static float f4131jV;
    @C0064Am(aul = "13903a083726a38595b76b5b56d8e120", aum = 4)

    /* renamed from: jW */
    private static int f4132jW;

    static {
        m19947V();
    }

    public RawMaterial() {
        super((C5540aNg) null);
        super.mo10S();
    }

    public RawMaterial(RawMaterialType agy) {
        super((C5540aNg) null);
        super._m_script_init(agy);
    }

    public RawMaterial(C5540aNg ang) {
        super(ang);
    }

    /* renamed from: V */
    static void m19947V() {
        serialVersionUID = (long) 1;
        _m_fieldCount = BulkItem._m_fieldCount + 5;
        _m_methodCount = BulkItem._m_methodCount + 2;
        int i = BulkItem._m_fieldCount;
        C5663aRz[] arzArr = new C5663aRz[(i + 5)];
        C5663aRz b = C5640aRc.m17844b(RawMaterial.class, "7d45a02a72980654cfa0eae714d593ce", i);
        aMh = b;
        arzArr[i] = b;
        int i2 = i + 1;
        C5663aRz b2 = C5640aRc.m17844b(RawMaterial.class, "b298d30341214badb1ea8f3951e39a01", i2);
        awd = b2;
        arzArr[i2] = b2;
        int i3 = i2 + 1;
        C5663aRz b3 = C5640aRc.m17844b(RawMaterial.class, "87ff61ccace573a3139651a313d63c80", i3);
        aMg = b3;
        arzArr[i3] = b3;
        int i4 = i3 + 1;
        C5663aRz b4 = C5640aRc.m17844b(RawMaterial.class, "da3d56c88a5770f209d55d6b6cb7e99e", i4);
        aMi = b4;
        arzArr[i4] = b4;
        int i5 = i4 + 1;
        C5663aRz b5 = C5640aRc.m17844b(RawMaterial.class, "13903a083726a38595b76b5b56d8e120", i5);
        f4127BR = b5;
        arzArr[i5] = b5;
        int i6 = i5 + 1;
        _m_fields = arzArr;
        C1634Xv.m11725a((Object[]) BulkItem._m_fields, (Object[]) _m_fields);
        int i7 = BulkItem._m_methodCount;
        C2491fm[] fmVarArr = new C2491fm[(i7 + 2)];
        C2491fm a = C4105zY.m41624a(RawMaterial.class, "a6e367f5234a6d3fbcb7a7fa20a40147", i7);
        eZK = a;
        fmVarArr[i7] = a;
        int i8 = i7 + 1;
        C2491fm a2 = C4105zY.m41624a(RawMaterial.class, "75bf39a474742b1069431a7e0f84fd1e", i8);
        esw = a2;
        fmVarArr[i8] = a2;
        int i9 = i8 + 1;
        _m_methods = fmVarArr;
        C1634Xv.m11725a((Object[]) BulkItem._m_methods, (Object[]) _m_methods);
        ___iScriptClass = aUO.m18566a(RawMaterial.class, C1396UT.class, _m_fields, _m_methods);
    }

    /* renamed from: L */
    private void m19942L(Asset tCVar) {
        throw new C6039afL();
    }

    /* renamed from: Ll */
    private C3438ra m19943Ll() {
        return ((RawMaterialType) getType()).bgF();
    }

    /* renamed from: RQ */
    private Asset m19944RQ() {
        return ((RawMaterialType) getType()).mo187Se();
    }

    /* renamed from: RR */
    private DatabaseCategory m19945RR() {
        return ((RawMaterialType) getType()).mo184RY();
    }

    /* renamed from: RS */
    private float m19946RS() {
        return ((RawMaterialType) getType()).mo188Sg();
    }

    /* renamed from: a */
    private void m19948a(DatabaseCategory aik) {
        throw new C6039afL();
    }

    @C0064Am(aul = "75bf39a474742b1069431a7e0f84fd1e", aum = 0)
    private ItemType bAO() {
        return bOb();
    }

    private int bNZ() {
        return ((RawMaterialType) getType()).dan();
    }

    /* renamed from: cx */
    private void m19949cx(float f) {
        throw new C6039afL();
    }

    /* renamed from: pO */
    private void m19950pO(int i) {
        throw new C6039afL();
    }

    /* renamed from: x */
    private void m19951x(C3438ra raVar) {
        throw new C6039afL();
    }

    /* renamed from: S */
    public void mo10S() {
        super.mo10S();
    }

    /* renamed from: T */
    public C6494any mo11T() {
        return ___iScriptClass;
    }

    /* renamed from: U */
    public C2491fm[] mo12U() {
        return _m_methods;
    }

    /* renamed from: W */
    public Object mo13W() {
        return new C1396UT(this);
    }

    /* renamed from: a */
    public Object mo14a(C0495Gr gr) {
        gr.getArgs();
        switch (gr.mo2417hq() - BulkItem._m_methodCount) {
            case 0:
                return bOa();
            case 1:
                return bAO();
            default:
                return super.mo14a(gr);
        }
    }

    /* renamed from: a */
    public void mo15a(Collection collection, C0495Gr gr) {
        super.mo15a(collection, gr);
    }

    public /* bridge */ /* synthetic */ ItemType bAP() {
        switch (bFf().mo6893i(esw)) {
            case 0:
                return null;
            case 2:
                return (ItemType) bFf().mo5606d(new aCE(this, esw, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, esw, new Object[0]));
                break;
        }
        return bAO();
    }

    public RawMaterialType bOb() {
        switch (bFf().mo6893i(eZK)) {
            case 0:
                return null;
            case 2:
                return (RawMaterialType) bFf().mo5606d(new aCE(this, eZK, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, eZK, new Object[0]));
                break;
        }
        return bOa();
    }

    /* renamed from: c */
    public C5663aRz[] mo25c() {
        return _m_fields;
    }

    /* renamed from: e */
    public void mo12458e(RawMaterialType agy) {
        super.mo11561b((BulkItemType) agy);
    }

    @C0064Am(aul = "a6e367f5234a6d3fbcb7a7fa20a40147", aum = 0)
    private RawMaterialType bOa() {
        return (RawMaterialType) super.bAP();
    }
}
