package game.script.item;

import game.network.message.externalizable.aCE;
import game.network.message.serializable.C2686iZ;
import game.script.Actor;
import game.script.item.buff.module.AttributeBuff;
import game.script.item.buff.module.AttributeBuffType;
import game.script.player.Player;
import game.script.resource.Asset;
import game.script.ship.Ship;
import logic.aaa.C3504rh;
import logic.baa.C1616Xf;
import logic.baa.C1634Xv;
import logic.baa.C5511aMd;
import logic.baa.C6485anp;
import logic.data.link.C3396rD;
import logic.data.link.C3981xi;
import logic.data.mbean.C4073yw;
import logic.res.code.C5640aRc;
import logic.res.code.C5663aRz;
import logic.res.code.C6494any;
import logic.res.html.C2491fm;
import p001a.*;

import java.util.Collection;

@C5829abJ("2.0.2")
@C6485anp
@C2712iu(mo19786Bt = BaseModuleType.class, version = "2.0.2")
@C5511aMd
/* renamed from: a.yA */
/* compiled from: a */
public class Module extends Component implements C1616Xf, C6809auB, C3396rD.C3397a, C3981xi.C3983b, C3981xi.C3984c {

    /* renamed from: CB */
    public static final C2491fm f9573CB = null;
    public static final C2491fm _f_dispose_0020_0028_0029V = null;
    public static final C2491fm _f_onResurrect_0020_0028_0029V = null;
    public static final C5663aRz _f_startTime = null;
    public static final int _m_fieldCount = 0;
    public static final C5663aRz[] _m_fields = null;
    public static final int _m_methodCount = 0;
    public static final C2491fm[] _m_methods = null;
    public static final C2491fm bEv = null;
    public static final C2491fm bEw = null;
    public static final C2491fm bbS = null;
    public static final C2491fm bbU = null;
    public static final C5663aRz bid = null;
    public static final C2491fm bka = null;
    public static final C2491fm bkd = null;
    public static final C2491fm bkk = null;
    public static final C2491fm bkl = null;
    public static final C2491fm bqQ = null;
    public static final C2491fm bqf = null;
    public static final C2491fm bqk = null;
    public static final C2491fm brL = null;
    public static final C5663aRz ccJ = null;
    public static final C2491fm ccL = null;
    public static final C2491fm ccP = null;
    public static final C2491fm ccQ = null;
    public static final C5663aRz dqU = null;
    public static final C5663aRz dqV = null;
    public static final C5663aRz dqW = null;
    public static final C5663aRz dqX = null;
    public static final C5663aRz dqY = null;
    public static final C5663aRz dqZ = null;
    public static final C5663aRz dra = null;
    public static final C5663aRz drb = null;
    public static final C2491fm dro = null;
    public static final C5663aRz iVK = null;
    public static final C2491fm iVM = null;
    public static final C2491fm iVN = null;
    public static final C2491fm iVO = null;
    public static final C2491fm iVP = null;
    public static final C2491fm iVQ = null;
    public static final C2491fm iVR = null;
    public static final C2491fm iVS = null;
    public static final C2491fm iVT = null;
    public static final C2491fm iVU = null;
    public static final C2491fm iVV = null;
    public static final C2491fm iVW = null;
    public static final C2491fm iVX = null;
    public static final C2491fm iVY = null;
    public static final C2491fm iVZ = null;
    public static final C2491fm iWa = null;
    public static final C2491fm iWb = null;
    public static final C2491fm iWc = null;
    public static final C2491fm iWd = null;
    public static final long serialVersionUID = 0;
    /* renamed from: yg */
    public static final C2491fm f9574yg = null;
    public static C6494any ___iScriptClass;
    @C0064Am(aul = "fe5a6bbc9df9985c72f51e22ef6ed8c3", aum = 9)
    private static Ship aIq;
    @C0064Am(aul = "ec16a479dab15e0c5d1fffe74280367b", aum = 10)
    private static C6809auB.C1996a aZI;
    @C0064Am(aul = "65374c997fc0d789c7b577825d12f7d4", aum = 0)
    private static float bKI;
    @C0064Am(aul = "4860ac100d9a927cdffa18eac07c6463", aum = 1)
    private static float bKJ;
    @C0064Am(aul = "f5e8399560a50d881246faa10e55dd9f", aum = 2)
    private static float bKK;
    @C0064Am(aul = "5146f0f389fd1793b1631c098d0abcc3", aum = 3)
    private static Asset bKL;
    @C0064Am(aul = "a52eea79c745edb31a9735935a932dec", aum = 4)
    private static Asset bKM;
    @C0064Am(aul = "e6530fbc7a1304caba5848ab0b452d8c", aum = 5)
    private static Asset bKN;
    @C0064Am(aul = "ae909815adf42bedb4a279f9c4442614", aum = 6)
    private static float bKO;
    @C0064Am(aul = "cfba01ab7380acc6c6e83d9f0fef483f", aum = 7)
    private static C2686iZ<AttributeBuffType> bKP;
    @C0064Am(aul = "c31baf72cafedef863090635f0ae502a", aum = 8)
    private static C2686iZ<AttributeBuff> bKQ;
    @C0064Am(aul = "7ff928d202e7c123bac6ea0d5261dfed", aum = 11)
    private static long startTime;

    static {
        m41159V();
    }

    private transient int iVL;

    public Module() {
        super((C5540aNg) null);
        super.mo10S();
    }

    public Module(ModuleType kq) {
        super((C5540aNg) null);
        super._m_script_init(kq);
    }

    public Module(C5540aNg ang) {
        super(ang);
    }

    /* renamed from: V */
    static void m41159V() {
        serialVersionUID = (long) 1;
        _m_fieldCount = Component._m_fieldCount + 12;
        _m_methodCount = Component._m_methodCount + 38;
        int i = Component._m_fieldCount;
        C5663aRz[] arzArr = new C5663aRz[(i + 12)];
        C5663aRz b = C5640aRc.m17844b(Module.class, "65374c997fc0d789c7b577825d12f7d4", i);
        dqU = b;
        arzArr[i] = b;
        int i2 = i + 1;
        C5663aRz b2 = C5640aRc.m17844b(Module.class, "4860ac100d9a927cdffa18eac07c6463", i2);
        dqV = b2;
        arzArr[i2] = b2;
        int i3 = i2 + 1;
        C5663aRz b3 = C5640aRc.m17844b(Module.class, "f5e8399560a50d881246faa10e55dd9f", i3);
        dqW = b3;
        arzArr[i3] = b3;
        int i4 = i3 + 1;
        C5663aRz b4 = C5640aRc.m17844b(Module.class, "5146f0f389fd1793b1631c098d0abcc3", i4);
        dqX = b4;
        arzArr[i4] = b4;
        int i5 = i4 + 1;
        C5663aRz b5 = C5640aRc.m17844b(Module.class, "a52eea79c745edb31a9735935a932dec", i5);
        dqY = b5;
        arzArr[i5] = b5;
        int i6 = i5 + 1;
        C5663aRz b6 = C5640aRc.m17844b(Module.class, "e6530fbc7a1304caba5848ab0b452d8c", i6);
        dqZ = b6;
        arzArr[i6] = b6;
        int i7 = i6 + 1;
        C5663aRz b7 = C5640aRc.m17844b(Module.class, "ae909815adf42bedb4a279f9c4442614", i7);
        dra = b7;
        arzArr[i7] = b7;
        int i8 = i7 + 1;
        C5663aRz b8 = C5640aRc.m17844b(Module.class, "cfba01ab7380acc6c6e83d9f0fef483f", i8);
        drb = b8;
        arzArr[i8] = b8;
        int i9 = i8 + 1;
        C5663aRz b9 = C5640aRc.m17844b(Module.class, "c31baf72cafedef863090635f0ae502a", i9);
        iVK = b9;
        arzArr[i9] = b9;
        int i10 = i9 + 1;
        C5663aRz b10 = C5640aRc.m17844b(Module.class, "fe5a6bbc9df9985c72f51e22ef6ed8c3", i10);
        ccJ = b10;
        arzArr[i10] = b10;
        int i11 = i10 + 1;
        C5663aRz b11 = C5640aRc.m17844b(Module.class, "ec16a479dab15e0c5d1fffe74280367b", i11);
        bid = b11;
        arzArr[i11] = b11;
        int i12 = i11 + 1;
        C5663aRz b12 = C5640aRc.m17844b(Module.class, "7ff928d202e7c123bac6ea0d5261dfed", i12);
        _f_startTime = b12;
        arzArr[i12] = b12;
        int i13 = i12 + 1;
        _m_fields = arzArr;
        C1634Xv.m11725a((Object[]) Component._m_fields, (Object[]) _m_fields);
        int i14 = Component._m_methodCount;
        C2491fm[] fmVarArr = new C2491fm[(i14 + 38)];
        C2491fm a = C4105zY.m41624a(Module.class, "2c60685cac08a9d5782503a4b67ecb7f", i14);
        ccL = a;
        fmVarArr[i14] = a;
        int i15 = i14 + 1;
        C2491fm a2 = C4105zY.m41624a(Module.class, "328e5e0752dc6855cdbb8b73093636f1", i15);
        bbU = a2;
        fmVarArr[i15] = a2;
        int i16 = i15 + 1;
        C2491fm a3 = C4105zY.m41624a(Module.class, "9107efd218292c3523cf5382adb3e5b8", i16);
        bbS = a3;
        fmVarArr[i16] = a3;
        int i17 = i16 + 1;
        C2491fm a4 = C4105zY.m41624a(Module.class, "c5ab48f3d62d099d72c8e2c190e9a5aa", i17);
        f9574yg = a4;
        fmVarArr[i17] = a4;
        int i18 = i17 + 1;
        C2491fm a5 = C4105zY.m41624a(Module.class, "48203ef4ac67d6cfbc3b14e1e3c61d55", i18);
        bkk = a5;
        fmVarArr[i18] = a5;
        int i19 = i18 + 1;
        C2491fm a6 = C4105zY.m41624a(Module.class, "01499d54507b338413bd7091fb80aeda", i19);
        bqf = a6;
        fmVarArr[i19] = a6;
        int i20 = i19 + 1;
        C2491fm a7 = C4105zY.m41624a(Module.class, "425ce454948e18af98a1f01ba373ebd3", i20);
        iVM = a7;
        fmVarArr[i20] = a7;
        int i21 = i20 + 1;
        C2491fm a8 = C4105zY.m41624a(Module.class, "4c2bda6b649b8343daea2f566ee25ddb", i21);
        iVN = a8;
        fmVarArr[i21] = a8;
        int i22 = i21 + 1;
        C2491fm a9 = C4105zY.m41624a(Module.class, "ffef17a5ee0661fdde40e0be60f53128", i22);
        iVO = a9;
        fmVarArr[i22] = a9;
        int i23 = i22 + 1;
        C2491fm a10 = C4105zY.m41624a(Module.class, "12748be7d8c33a43a13d0166531d3a5c", i23);
        iVP = a10;
        fmVarArr[i23] = a10;
        int i24 = i23 + 1;
        C2491fm a11 = C4105zY.m41624a(Module.class, "9f27ecb828e253455d9b304397a3307a", i24);
        iVQ = a11;
        fmVarArr[i24] = a11;
        int i25 = i24 + 1;
        C2491fm a12 = C4105zY.m41624a(Module.class, "20ad1943ab3325fa6168866770bf5437", i25);
        iVR = a12;
        fmVarArr[i25] = a12;
        int i26 = i25 + 1;
        C2491fm a13 = C4105zY.m41624a(Module.class, "91da1f56e5360e53ae7eed36b7050dde", i26);
        iVS = a13;
        fmVarArr[i26] = a13;
        int i27 = i26 + 1;
        C2491fm a14 = C4105zY.m41624a(Module.class, "b6a6fccba88e2e3db86ce0514c1fcc56", i27);
        brL = a14;
        fmVarArr[i27] = a14;
        int i28 = i27 + 1;
        C2491fm a15 = C4105zY.m41624a(Module.class, "b4584eb5499e13a12e71e30fb7e851c0", i28);
        iVT = a15;
        fmVarArr[i28] = a15;
        int i29 = i28 + 1;
        C2491fm a16 = C4105zY.m41624a(Module.class, "de0a3514355a3a8b5eea508ddfce977c", i29);
        iVU = a16;
        fmVarArr[i29] = a16;
        int i30 = i29 + 1;
        C2491fm a17 = C4105zY.m41624a(Module.class, "7ef44751ec13771780dc6cf620bc1036", i30);
        bka = a17;
        fmVarArr[i30] = a17;
        int i31 = i30 + 1;
        C2491fm a18 = C4105zY.m41624a(Module.class, "0fcc4dda5a1e2c4ca0a7f56f121fc16f", i31);
        iVV = a18;
        fmVarArr[i31] = a18;
        int i32 = i31 + 1;
        C2491fm a19 = C4105zY.m41624a(Module.class, "afdc0917af70b0ddaf94dab7fe450038", i32);
        bkd = a19;
        fmVarArr[i32] = a19;
        int i33 = i32 + 1;
        C2491fm a20 = C4105zY.m41624a(Module.class, "11c3672824d01c2065f2875dba769bf2", i33);
        iVW = a20;
        fmVarArr[i33] = a20;
        int i34 = i33 + 1;
        C2491fm a21 = C4105zY.m41624a(Module.class, "557f4c9b1a361c37b612b33113a627c0", i34);
        iVX = a21;
        fmVarArr[i34] = a21;
        int i35 = i34 + 1;
        C2491fm a22 = C4105zY.m41624a(Module.class, "2f5f908550fc4cd5db2c50558c99e2f2", i35);
        iVY = a22;
        fmVarArr[i35] = a22;
        int i36 = i35 + 1;
        C2491fm a23 = C4105zY.m41624a(Module.class, "d5331f13eb868b3ae229542c8651586b", i36);
        iVZ = a23;
        fmVarArr[i36] = a23;
        int i37 = i36 + 1;
        C2491fm a24 = C4105zY.m41624a(Module.class, "39966beb1ea8d94259f2318f2c69089f", i37);
        _f_onResurrect_0020_0028_0029V = a24;
        fmVarArr[i37] = a24;
        int i38 = i37 + 1;
        C2491fm a25 = C4105zY.m41624a(Module.class, "7dd977de04ae741d7b66d5b114579fe9", i38);
        iWa = a25;
        fmVarArr[i38] = a25;
        int i39 = i38 + 1;
        C2491fm a26 = C4105zY.m41624a(Module.class, "5720e0fd0e69bd233232cf81fcbbbce5", i39);
        bkl = a26;
        fmVarArr[i39] = a26;
        int i40 = i39 + 1;
        C2491fm a27 = C4105zY.m41624a(Module.class, "acfc870af5392c75c638560b157ca485", i40);
        bqQ = a27;
        fmVarArr[i40] = a27;
        int i41 = i40 + 1;
        C2491fm a28 = C4105zY.m41624a(Module.class, "d68a795a13fe2e956013043164212141", i41);
        bqk = a28;
        fmVarArr[i41] = a28;
        int i42 = i41 + 1;
        C2491fm a29 = C4105zY.m41624a(Module.class, "bc97d12587bc4902687e2c738a7b27f6", i42);
        iWb = a29;
        fmVarArr[i42] = a29;
        int i43 = i42 + 1;
        C2491fm a30 = C4105zY.m41624a(Module.class, "553a345583cbb22afed143d173162620", i43);
        _f_dispose_0020_0028_0029V = a30;
        fmVarArr[i43] = a30;
        int i44 = i43 + 1;
        C2491fm a31 = C4105zY.m41624a(Module.class, "c1ed5317b8cf6b92c70518ecb6e5bce8", i44);
        iWc = a31;
        fmVarArr[i44] = a31;
        int i45 = i44 + 1;
        C2491fm a32 = C4105zY.m41624a(Module.class, "007459506c83772fcf52683d41aef854", i45);
        iWd = a32;
        fmVarArr[i45] = a32;
        int i46 = i45 + 1;
        C2491fm a33 = C4105zY.m41624a(Module.class, "b58e2174e7cff89889eed00efb131582", i46);
        ccP = a33;
        fmVarArr[i46] = a33;
        int i47 = i46 + 1;
        C2491fm a34 = C4105zY.m41624a(Module.class, "4002aa5833a88bfa983c449c6cc4e47c", i47);
        ccQ = a34;
        fmVarArr[i47] = a34;
        int i48 = i47 + 1;
        C2491fm a35 = C4105zY.m41624a(Module.class, "8406ec859cbdeeaa92418bb798de5110", i48);
        bEv = a35;
        fmVarArr[i48] = a35;
        int i49 = i48 + 1;
        C2491fm a36 = C4105zY.m41624a(Module.class, "96f3fec96dc8e931067ba9aa03181ff5", i49);
        bEw = a36;
        fmVarArr[i49] = a36;
        int i50 = i49 + 1;
        C2491fm a37 = C4105zY.m41624a(Module.class, "efee45448fd5d880877c64bfc11da296", i50);
        f9573CB = a37;
        fmVarArr[i50] = a37;
        int i51 = i50 + 1;
        C2491fm a38 = C4105zY.m41624a(Module.class, "47333f1502c8afd174b92881be37031c", i51);
        dro = a38;
        fmVarArr[i51] = a38;
        int i52 = i51 + 1;
        _m_methods = fmVarArr;
        C1634Xv.m11725a((Object[]) Component._m_methods, (Object[]) _m_methods);
        ___iScriptClass = aUO.m18566a(Module.class, C4073yw.class, _m_fields, _m_methods);
    }

    /* renamed from: a */
    public static void m41164a(C6859auz auz) {
        auz.mo3117j(2, 0, 0);
    }

    @C0064Am(aul = "4002aa5833a88bfa983c449c6cc4e47c", aum = 0)
    @C5566aOg
    /* renamed from: A */
    private void m41154A(Ship fAVar) {
        throw new aWi(new aCE(this, ccQ, new Object[]{fAVar}));
    }

    @C0064Am(aul = "11c3672824d01c2065f2875dba769bf2", aum = 0)
    @C5566aOg
    /* renamed from: AU */
    private void m41155AU(int i) {
        throw new aWi(new aCE(this, iVW, new Object[]{new Integer(i)}));
    }

    @C5566aOg
    /* renamed from: AV */
    private void m41156AV(int i) {
        switch (bFf().mo6893i(iVW)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, iVW, new Object[]{new Integer(i)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, iVW, new Object[]{new Integer(i)}));
                break;
        }
        m41155AU(i);
    }

    /* renamed from: Bk */
    private long m41157Bk() {
        return bFf().mo5608dq().mo3213o(_f_startTime);
    }

    /* renamed from: I */
    private void m41158I(C2686iZ iZVar) {
        throw new C6039afL();
    }

    @C0064Am(aul = "c1ed5317b8cf6b92c70518ecb6e5bce8", aum = 0)
    @C5566aOg
    /* renamed from: a */
    private C4009a m41162a(AttributeBuff mp) {
        throw new aWi(new aCE(this, iWc, new Object[]{mp}));
    }

    /* renamed from: a */
    private void m41163a(C6809auB.C1996a aVar) {
        bFf().mo5608dq().mo3197f(bid, aVar);
    }

    @C0064Am(aul = "b6a6fccba88e2e3db86ce0514c1fcc56", aum = 0)
    @C5566aOg
    /* renamed from: ab */
    private void m41168ab(Asset tCVar) {
        throw new aWi(new aCE(this, brL, new Object[]{tCVar}));
    }

    @C5566aOg
    /* renamed from: ac */
    private void m41169ac(Asset tCVar) {
        switch (bFf().mo6893i(brL)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, brL, new Object[]{tCVar}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, brL, new Object[]{tCVar}));
                break;
        }
        m41168ab(tCVar);
    }

    @C4034yP
    @C5566aOg
    @C0064Am(aul = "afdc0917af70b0ddaf94dab7fe450038", aum = 0)
    @C2499fr
    private void acP() {
        throw new aWi(new aCE(this, bkd, new Object[0]));
    }

    private C6809auB.C1996a acy() {
        return (C6809auB.C1996a) bFf().mo5608dq().mo3214p(bid);
    }

    /* renamed from: aq */
    private void m41170aq(C2686iZ iZVar) {
        bFf().mo5608dq().mo3197f(iVK, iZVar);
    }

    private Ship asP() {
        return (Ship) bFf().mo5608dq().mo3214p(ccJ);
    }

    @C0064Am(aul = "2c60685cac08a9d5782503a4b67ecb7f", aum = 0)
    @C5566aOg
    private void asR() {
        throw new aWi(new aCE(this, ccL, new Object[0]));
    }

    /* renamed from: at */
    private void m41171at(Asset tCVar) {
        throw new C6039afL();
    }

    /* renamed from: au */
    private void m41172au(Asset tCVar) {
        throw new C6039afL();
    }

    /* renamed from: av */
    private void m41173av(Asset tCVar) {
        throw new C6039afL();
    }

    private float bdk() {
        return ((ModuleType) getType()).bdt();
    }

    private float bdl() {
        return ((ModuleType) getType()).bdv();
    }

    private float bdm() {
        return ((ModuleType) getType()).bdx();
    }

    private Asset bdn() {
        return ((ModuleType) getType()).bdz();
    }

    private Asset bdo() {
        return ((ModuleType) getType()).bdB();
    }

    private Asset bdp() {
        return ((ModuleType) getType()).bdD();
    }

    private float bdq() {
        return ((ModuleType) getType()).bdF();
    }

    private C2686iZ bdr() {
        return ((ModuleType) getType()).bdH();
    }

    /* renamed from: bz */
    private void m41174bz(long j) {
        bFf().mo5608dq().mo3184b(_f_startTime, j);
    }

    @C0064Am(aul = "91da1f56e5360e53ae7eed36b7050dde", aum = 0)
    @C5566aOg
    /* renamed from: cn */
    private void m41176cn(Asset tCVar) {
        throw new aWi(new aCE(this, iVS, new Object[]{tCVar}));
    }

    @C5566aOg
    /* renamed from: co */
    private void m41177co(Asset tCVar) {
        switch (bFf().mo6893i(iVS)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, iVS, new Object[]{tCVar}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, iVS, new Object[]{tCVar}));
                break;
        }
        m41176cn(tCVar);
    }

    @C0064Am(aul = "5720e0fd0e69bd233232cf81fcbbbce5", aum = 0)
    @C5566aOg
    /* renamed from: d */
    private void m41178d(C6809auB.C1996a aVar) {
        throw new aWi(new aCE(this, bkl, new Object[]{aVar}));
    }

    private C2686iZ dyK() {
        return (C2686iZ) bFf().mo5608dq().mo3214p(iVK);
    }

    private void dyM() {
        switch (bFf().mo6893i(iVM)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, iVM, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, iVM, new Object[0]));
                break;
        }
        dyL();
    }

    private void dyO() {
        switch (bFf().mo6893i(iVN)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, iVN, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, iVN, new Object[0]));
                break;
        }
        dyN();
    }

    private void dyQ() {
        switch (bFf().mo6893i(iVO)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, iVO, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, iVO, new Object[0]));
                break;
        }
        dyP();
    }

    private void dyS() {
        switch (bFf().mo6893i(iVP)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, iVP, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, iVP, new Object[0]));
                break;
        }
        dyR();
    }

    private void dyU() {
        switch (bFf().mo6893i(iVQ)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, iVQ, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, iVQ, new Object[0]));
                break;
        }
        dyT();
    }

    private void dyW() {
        switch (bFf().mo6893i(iVR)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, iVR, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, iVR, new Object[0]));
                break;
        }
        dyV();
    }

    @C0064Am(aul = "b4584eb5499e13a12e71e30fb7e851c0", aum = 0)
    @C5566aOg
    private void dyX() {
        throw new aWi(new aCE(this, iVT, new Object[0]));
    }

    @C5566aOg
    private void dyY() {
        switch (bFf().mo6893i(iVT)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, iVT, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, iVT, new Object[0]));
                break;
        }
        dyX();
    }

    @C0064Am(aul = "de0a3514355a3a8b5eea508ddfce977c", aum = 0)
    @C5566aOg
    private void dyZ() {
        throw new aWi(new aCE(this, iVU, new Object[0]));
    }

    @C5566aOg
    private void dza() {
        switch (bFf().mo6893i(iVU)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, iVU, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, iVU, new Object[0]));
                break;
        }
        dyZ();
    }

    @C4034yP
    @C5566aOg
    @C0064Am(aul = "0fcc4dda5a1e2c4ca0a7f56f121fc16f", aum = 0)
    @C2499fr
    private void dzb() {
        throw new aWi(new aCE(this, iVV, new Object[0]));
    }

    @C0064Am(aul = "557f4c9b1a361c37b612b33113a627c0", aum = 0)
    @C5566aOg
    private void dzd() {
        throw new aWi(new aCE(this, iVX, new Object[0]));
    }

    @C5566aOg
    private void dze() {
        switch (bFf().mo6893i(iVX)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, iVX, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, iVX, new Object[0]));
                break;
        }
        dzd();
    }

    @C0064Am(aul = "2f5f908550fc4cd5db2c50558c99e2f2", aum = 0)
    @C5566aOg
    private void dzf() {
        throw new aWi(new aCE(this, iVY, new Object[0]));
    }

    @C5566aOg
    private void dzg() {
        switch (bFf().mo6893i(iVY)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, iVY, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, iVY, new Object[0]));
                break;
        }
        dzf();
    }

    @C0064Am(aul = "d5331f13eb868b3ae229542c8651586b", aum = 0)
    @C5566aOg
    private void dzh() {
        throw new aWi(new aCE(this, iVZ, new Object[0]));
    }

    @C5566aOg
    private void dzi() {
        switch (bFf().mo6893i(iVZ)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, iVZ, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, iVZ, new Object[0]));
                break;
        }
        dzh();
    }

    /* renamed from: fX */
    private void m41179fX(float f) {
        throw new C6039afL();
    }

    /* renamed from: fY */
    private void m41180fY(float f) {
        throw new C6039afL();
    }

    /* renamed from: fZ */
    private void m41181fZ(float f) {
        throw new C6039afL();
    }

    @C0064Am(aul = "553a345583cbb22afed143d173162620", aum = 0)
    @C5566aOg
    @C2499fr
    /* renamed from: fg */
    private void m41182fg() {
        throw new aWi(new aCE(this, _f_dispose_0020_0028_0029V, new Object[0]));
    }

    /* renamed from: ga */
    private void m41183ga(float f) {
        throw new C6039afL();
    }

    /* renamed from: z */
    private void m41186z(Ship fAVar) {
        bFf().mo5608dq().mo3197f(ccJ, fAVar);
    }

    @C5566aOg
    /* renamed from: B */
    public void mo23055B(Ship fAVar) {
        switch (bFf().mo6893i(ccQ)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, ccQ, new Object[]{fAVar}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, ccQ, new Object[]{fAVar}));
                break;
        }
        m41154A(fAVar);
    }

    /* renamed from: S */
    public void mo10S() {
        super.mo10S();
    }

    /* renamed from: T */
    public C6494any mo11T() {
        return ___iScriptClass;
    }

    /* renamed from: U */
    public C2491fm[] mo12U() {
        return _m_methods;
    }

    /* renamed from: W */
    public Object mo13W() {
        return new C4073yw(this);
    }

    /* renamed from: YE */
    public float mo16294YE() {
        switch (bFf().mo6893i(bbS)) {
            case 0:
                return 0.0f;
            case 2:
                return ((Float) bFf().mo5606d(new aCE(this, bbS, new Object[0]))).floatValue();
            case 3:
                bFf().mo5606d(new aCE(this, bbS, new Object[0]));
                break;
        }
        return m41160YD();
    }

    /* renamed from: YG */
    public float mo16295YG() {
        switch (bFf().mo6893i(bbU)) {
            case 0:
                return 0.0f;
            case 2:
                return ((Float) bFf().mo5606d(new aCE(this, bbU, new Object[0]))).floatValue();
            case 3:
                bFf().mo5606d(new aCE(this, bbU, new Object[0]));
                break;
        }
        return m41161YF();
    }

    /* renamed from: a */
    public Object mo14a(C0495Gr gr) {
        Object[] args = gr.getArgs();
        switch (gr.mo2417hq() - Component._m_methodCount) {
            case 0:
                asR();
                return null;
            case 1:
                return new Float(m41161YF());
            case 2:
                return new Float(m41160YD());
            case 3:
                return new Float(m41184js());
            case 4:
                return acY();
            case 5:
                return new Float(agy());
            case 6:
                dyL();
                return null;
            case 7:
                dyN();
                return null;
            case 8:
                dyP();
                return null;
            case 9:
                dyR();
                return null;
            case 10:
                dyT();
                return null;
            case 11:
                dyV();
                return null;
            case 12:
                m41176cn((Asset) args[0]);
                return null;
            case 13:
                m41168ab((Asset) args[0]);
                return null;
            case 14:
                dyX();
                return null;
            case 15:
                dyZ();
                return null;
            case 16:
                acK();
                return null;
            case 17:
                dzb();
                return null;
            case 18:
                acP();
                return null;
            case 19:
                m41155AU(((Integer) args[0]).intValue());
                return null;
            case 20:
                dzd();
                return null;
            case 21:
                dzf();
                return null;
            case 22:
                dzh();
                return null;
            case 23:
                m41167aG();
                return null;
            case 24:
                return dzj();
            case 25:
                m41178d((C6809auB.C1996a) args[0]);
                return null;
            case 26:
                ahu();
                return null;
            case 27:
                return agI();
            case 28:
                return dzl();
            case 29:
                m41182fg();
                return null;
            case 30:
                return m41162a((AttributeBuff) args[0]);
            case 31:
                return new Boolean(m41166aB((Ship) args[0]));
            case 32:
                return asT();
            case 33:
                m41154A((Ship) args[0]);
                return null;
            case 34:
                m41165a((Ship) args[0], (Component) args[1]);
                return null;
            case 35:
                m41175c((Ship) args[0], (Component) args[1]);
                return null;
            case 36:
                m41185l((Actor) args[0]);
                return null;
            case 37:
                return new Float(bdE());
            default:
                return super.mo14a(gr);
        }
    }

    /* renamed from: a */
    public void mo15a(Collection collection, C0495Gr gr) {
        super.mo15a(collection, gr);
    }

    /* renamed from: aC */
    public boolean mo23056aC(Ship fAVar) {
        switch (bFf().mo6893i(iWd)) {
            case 0:
                return false;
            case 2:
                return ((Boolean) bFf().mo5606d(new aCE(this, iWd, new Object[]{fAVar}))).booleanValue();
            case 3:
                bFf().mo5606d(new aCE(this, iWd, new Object[]{fAVar}));
                break;
        }
        return m41166aB(fAVar);
    }

    /* renamed from: aH */
    public void mo70aH() {
        switch (bFf().mo6893i(_f_onResurrect_0020_0028_0029V)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, _f_onResurrect_0020_0028_0029V, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, _f_onResurrect_0020_0028_0029V, new Object[0]));
                break;
        }
        m41167aG();
    }

    @C4034yP
    @C5566aOg
    @C2499fr
    public void abort() {
        switch (bFf().mo6893i(bkd)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, bkd, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, bkd, new Object[0]));
                break;
        }
        acP();
    }

    public C6809auB.C1996a acZ() {
        switch (bFf().mo6893i(bkk)) {
            case 0:
                return null;
            case 2:
                return (C6809auB.C1996a) bFf().mo5606d(new aCE(this, bkk, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, bkk, new Object[0]));
                break;
        }
        return acY();
    }

    @ClientOnly
    public void activate() {
        switch (bFf().mo6893i(bka)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, bka, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, bka, new Object[0]));
                break;
        }
        acK();
    }

    public String agJ() {
        switch (bFf().mo6893i(bqk)) {
            case 0:
                return null;
            case 2:
                return (String) bFf().mo5606d(new aCE(this, bqk, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, bqk, new Object[0]));
                break;
        }
        return agI();
    }

    public float agz() {
        switch (bFf().mo6893i(bqf)) {
            case 0:
                return 0.0f;
            case 2:
                return ((Float) bFf().mo5606d(new aCE(this, bqf, new Object[0]))).floatValue();
            case 3:
                bFf().mo5606d(new aCE(this, bqf, new Object[0]));
                break;
        }
        return agy();
    }

    public void ahv() {
        switch (bFf().mo6893i(bqQ)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, bqQ, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, bqQ, new Object[0]));
                break;
        }
        ahu();
    }

    @C5566aOg
    public void asS() {
        switch (bFf().mo6893i(ccL)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, ccL, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, ccL, new Object[0]));
                break;
        }
        asR();
    }

    public Ship asU() {
        switch (bFf().mo6893i(ccP)) {
            case 0:
                return null;
            case 2:
                return (Ship) bFf().mo5606d(new aCE(this, ccP, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, ccP, new Object[0]));
                break;
        }
        return asT();
    }

    @C5566aOg
    /* renamed from: b */
    public C4009a mo23058b(AttributeBuff mp) {
        switch (bFf().mo6893i(iWc)) {
            case 0:
                return null;
            case 2:
                return (C4009a) bFf().mo5606d(new aCE(this, iWc, new Object[]{mp}));
            case 3:
                bFf().mo5606d(new aCE(this, iWc, new Object[]{mp}));
                break;
        }
        return m41162a(mp);
    }

    /* renamed from: b */
    public void mo3926b(Ship fAVar, Component abl) {
        switch (bFf().mo6893i(bEv)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, bEv, new Object[]{fAVar, abl}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, bEv, new Object[]{fAVar, abl}));
                break;
        }
        m41165a(fAVar, abl);
    }

    public float bdF() {
        switch (bFf().mo6893i(dro)) {
            case 0:
                return 0.0f;
            case 2:
                return ((Float) bFf().mo5606d(new aCE(this, dro, new Object[0]))).floatValue();
            case 3:
                bFf().mo5606d(new aCE(this, dro, new Object[0]));
                break;
        }
        return bdE();
    }

    /* renamed from: c */
    public C5663aRz[] mo25c() {
        return _m_fields;
    }

    /* renamed from: d */
    public void mo3933d(Ship fAVar, Component abl) {
        switch (bFf().mo6893i(bEw)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, bEw, new Object[]{fAVar, abl}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, bEw, new Object[]{fAVar, abl}));
                break;
        }
        m41175c(fAVar, abl);
    }

    @C5566aOg
    @C2499fr
    public void dispose() {
        switch (bFf().mo6893i(_f_dispose_0020_0028_0029V)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, _f_dispose_0020_0028_0029V, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, _f_dispose_0020_0028_0029V, new Object[0]));
                break;
        }
        m41182fg();
    }

    @C4034yP
    @C5566aOg
    @C2499fr
    public void dzc() {
        switch (bFf().mo6893i(iVV)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, iVV, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, iVV, new Object[0]));
                break;
        }
        dzb();
    }

    public ModuleType dzk() {
        switch (bFf().mo6893i(iWa)) {
            case 0:
                return null;
            case 2:
                return (ModuleType) bFf().mo5606d(new aCE(this, iWa, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, iWa, new Object[0]));
                break;
        }
        return dzj();
    }

    public Ship dzm() {
        switch (bFf().mo6893i(iWb)) {
            case 0:
                return null;
            case 2:
                return (Ship) bFf().mo5606d(new aCE(this, iWb, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, iWb, new Object[0]));
                break;
        }
        return dzl();
    }

    /* access modifiers changed from: protected */
    @C5566aOg
    /* renamed from: e */
    public void mo23064e(C6809auB.C1996a aVar) {
        switch (bFf().mo6893i(bkl)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, bkl, new Object[]{aVar}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, bkl, new Object[]{aVar}));
                break;
        }
        m41178d(aVar);
    }

    /* renamed from: jt */
    public float mo16301jt() {
        switch (bFf().mo6893i(f9574yg)) {
            case 0:
                return 0.0f;
            case 2:
                return ((Float) bFf().mo5606d(new aCE(this, f9574yg, new Object[0]))).floatValue();
            case 3:
                bFf().mo5606d(new aCE(this, f9574yg, new Object[0]));
                break;
        }
        return m41184js();
    }

    /* renamed from: m */
    public void mo18227m(Actor cr) {
        switch (bFf().mo6893i(f9573CB)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f9573CB, new Object[]{cr}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f9573CB, new Object[]{cr}));
                break;
        }
        m41185l(cr);
    }

    /* renamed from: e */
    public void mo23063e(ModuleType kq) {
        super.mo7854a((ComponentType) kq);
        mo23064e(C6809auB.C1996a.DEACTIVE);
        for (AttributeBuffType NK : bdr()) {
            dyK().add((AttributeBuff) NK.mo7459NK());
        }
        for (AttributeBuff c : dyK()) {
            c.mo3932c(this);
        }
    }

    @C0064Am(aul = "328e5e0752dc6855cdbb8b73093636f1", aum = 0)
    /* renamed from: YF */
    private float m41161YF() {
        return bdm();
    }

    @C0064Am(aul = "9107efd218292c3523cf5382adb3e5b8", aum = 0)
    /* renamed from: YD */
    private float m41160YD() {
        return bdl();
    }

    @C0064Am(aul = "c5ab48f3d62d099d72c8e2c190e9a5aa", aum = 0)
    /* renamed from: js */
    private float m41184js() {
        return bdk();
    }

    @C0064Am(aul = "48203ef4ac67d6cfbc3b14e1e3c61d55", aum = 0)
    private C6809auB.C1996a acY() {
        return acy();
    }

    @C0064Am(aul = "01499d54507b338413bd7091fb80aeda", aum = 0)
    private float agy() {
        float cVr = ((float) (cVr() - m41157Bk())) / 1000.0f;
        if (acy() == C6809auB.C1996a.ACTIVE) {
            return Math.max((bdk() + bdl()) - cVr, 0.0f);
        }
        if (acy() == C6809auB.C1996a.COOLDOWN) {
            return Math.max(((bdk() + bdl()) + bdm()) - cVr, 0.0f);
        }
        if (acy() == C6809auB.C1996a.WARMUP) {
            return Math.max(bdl() - cVr, 0.0f);
        }
        return 0.0f;
    }

    @C0064Am(aul = "425ce454948e18af98a1f01ba373ebd3", aum = 0)
    private void dyL() {
        m41177co(bdp());
    }

    @C0064Am(aul = "4c2bda6b649b8343daea2f566ee25ddb", aum = 0)
    private void dyN() {
        m41169ac(bdp());
    }

    @C0064Am(aul = "ffef17a5ee0661fdde40e0be60f53128", aum = 0)
    private void dyP() {
        dzm().mo18259Y(bdn());
    }

    @C0064Am(aul = "12748be7d8c33a43a13d0166531d3a5c", aum = 0)
    private void dyR() {
        dzm().mo18262ac(bdn());
    }

    @C0064Am(aul = "9f27ecb828e253455d9b304397a3307a", aum = 0)
    private void dyT() {
        dzm().mo18259Y(bdo());
    }

    @C0064Am(aul = "20ad1943ab3325fa6168866770bf5437", aum = 0)
    private void dyV() {
        dzm().mo18262ac(bdo());
    }

    @C0064Am(aul = "7ef44751ec13771780dc6cf620bc1036", aum = 0)
    @ClientOnly
    private void acK() {
        if (asU().mo2998hb() == null || !(asP().mo2998hb() instanceof PlayerController) || !((PlayerController) asP().mo2998hb()).cPf().mo22170R()) {
            dzc();
        }
    }

    @C0064Am(aul = "39966beb1ea8d94259f2318f2c69089f", aum = 0)
    /* renamed from: aG */
    private void m41167aG() {
        super.mo70aH();
        if (bHa()) {
            float agz = agz();
            if (acy() == C6809auB.C1996a.ACTIVE) {
                ((Module) mo8362mt(agz)).dzi();
            } else if (acy() == C6809auB.C1996a.COOLDOWN) {
                ((Module) mo8362mt(agz)).dze();
            } else {
                mo23064e(C6809auB.C1996a.DEACTIVE);
            }
            if (asU() != null && asU().mo5353TJ().ask()) {
                mo6317hy("Module sanity failed, reseting it " + cJS().mo19891ke().get());
                dza();
                dyY();
            }
        }
    }

    @C0064Am(aul = "7dd977de04ae741d7b66d5b114579fe9", aum = 0)
    private ModuleType dzj() {
        return (ModuleType) cJS();
    }

    @C0064Am(aul = "acfc870af5392c75c638560b157ca485", aum = 0)
    private void ahu() {
        if (mo7855al() != null && (mo7855al().agj() instanceof Player)) {
            ((Player) mo7855al().agj()).mo14338A(agJ(), new C3504rh(acy()));
        }
    }

    @C0064Am(aul = "d68a795a13fe2e956013043164212141", aum = 0)
    private String agI() {
        return Long.toString(cWm());
    }

    @C0064Am(aul = "bc97d12587bc4902687e2c738a7b27f6", aum = 0)
    private Ship dzl() {
        if (asP() != null) {
            return asP();
        }
        return mo7855al();
    }

    @C0064Am(aul = "007459506c83772fcf52683d41aef854", aum = 0)
    /* renamed from: aB */
    private boolean m41166aB(Ship fAVar) {
        return dzm().mo1011bv((Actor) fAVar) <= bdF();
    }

    @C0064Am(aul = "b58e2174e7cff89889eed00efb131582", aum = 0)
    private Ship asT() {
        return asP();
    }

    @C0064Am(aul = "8406ec859cbdeeaa92418bb798de5110", aum = 0)
    /* renamed from: a */
    private void m41165a(Ship fAVar, Component abl) {
        for (AttributeBuff b : dyK()) {
            b.mo3926b(fAVar, abl);
        }
    }

    @C0064Am(aul = "96f3fec96dc8e931067ba9aa03181ff5", aum = 0)
    /* renamed from: c */
    private void m41175c(Ship fAVar, Component abl) {
        for (AttributeBuff d : dyK()) {
            d.mo3933d(fAVar, abl);
        }
    }

    @C0064Am(aul = "efee45448fd5d880877c64bfc11da296", aum = 0)
    /* renamed from: l */
    private void m41185l(Actor cr) {
        if (cr != asU()) {
            return;
        }
        if (acy() == C6809auB.C1996a.ACTIVE) {
            dzi();
        } else if (acy() == C6809auB.C1996a.WARMUP) {
            abort();
        }
    }

    @C0064Am(aul = "47333f1502c8afd174b92881be37031c", aum = 0)
    private float bdE() {
        return bdq();
    }

    /* renamed from: a.yA$a */
    public enum C4009a {
        OK,
        INVALID_TARGET,
        OUT_OF_RANGE,
        FAILED
    }

    /* renamed from: a.yA$b */
    /* compiled from: a */
    public enum C4010b {
        SELF,
        PARTY,
        AREA,
        TARGET
    }
}
