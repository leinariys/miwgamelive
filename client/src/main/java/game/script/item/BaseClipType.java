package game.script.item;

import game.network.message.externalizable.aCE;
import game.script.damage.DamageType;
import logic.baa.*;
import logic.data.mbean.aKP;
import logic.res.code.C5663aRz;
import logic.res.code.C6494any;
import logic.res.html.C2491fm;
import p001a.*;
import taikodom.infra.script.I18NString;

import java.util.Collection;

@C5511aMd
@C6485anp
/* renamed from: a.aig  reason: case insensitive filesystem */
/* compiled from: a */
public abstract class BaseClipType extends BulkItemType implements C1616Xf, aOW {

    /* renamed from: Do */
    public static final C2491fm f4661Do = null;
    public static final int _m_fieldCount = 0;
    public static final C5663aRz[] _m_fields = null;
    public static final int _m_methodCount = 0;
    public static final C2491fm[] _m_methods = null;
    public static final long serialVersionUID = 0;
    public static C6494any ___iScriptClass;

    static {
        m22626V();
    }

    public BaseClipType() {
        super((C5540aNg) null);
        super.mo10S();
    }

    public BaseClipType(C5540aNg ang) {
        super(ang);
    }

    /* renamed from: V */
    static void m22626V() {
        serialVersionUID = (long) 1;
        _m_fieldCount = BulkItemType._m_fieldCount + 0;
        _m_methodCount = BulkItemType._m_methodCount + 1;
        _m_fields = new C5663aRz[(BulkItemType._m_fieldCount + 0)];
        C1634Xv.m11725a((Object[]) BulkItemType._m_fields, (Object[]) _m_fields);
        int i = BulkItemType._m_methodCount;
        C2491fm[] fmVarArr = new C2491fm[(i + 1)];
        C2491fm a = C4105zY.m41624a(BaseClipType.class, "140ddd86fd10d8daa506a4d28a79c6ce", i);
        f4661Do = a;
        fmVarArr[i] = a;
        int i2 = i + 1;
        _m_methods = fmVarArr;
        C1634Xv.m11725a((Object[]) BulkItemType._m_methods, (Object[]) _m_methods);
        ___iScriptClass = aUO.m18566a(BaseClipType.class, aKP.class, _m_fields, _m_methods);
    }

    /* renamed from: T */
    public C6494any mo11T() {
        return ___iScriptClass;
    }

    /* renamed from: U */
    public C2491fm[] mo12U() {
        return _m_methods;
    }

    /* renamed from: W */
    public Object mo13W() {
        return new aKP(this);
    }

    /* renamed from: a */
    public Object mo14a(C0495Gr gr) {
        Object[] args = gr.getArgs();
        switch (gr.mo2417hq() - BulkItemType._m_methodCount) {
            case 0:
                m22627a((C0665JT) args[0]);
                return null;
            default:
                return super.mo14a(gr);
        }
    }

    /* renamed from: a */
    public void mo15a(Collection collection, C0495Gr gr) {
        super.mo15a(collection, gr);
    }

    /* renamed from: b */
    public void mo24b(C0665JT jt) {
        switch (bFf().mo6893i(f4661Do)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f4661Do, new Object[]{jt}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f4661Do, new Object[]{jt}));
                break;
        }
        m22627a(jt);
    }

    /* renamed from: c */
    public C5663aRz[] mo25c() {
        return _m_fields;
    }

    /* renamed from: S */
    public void mo10S() {
        super.mo10S();
    }

    @C0064Am(aul = "140ddd86fd10d8daa506a4d28a79c6ce", aum = 0)
    /* renamed from: a */
    private void m22627a(C0665JT jt) {
        DamageType fr;
        float f;
        float f2;
        if (jt.mo3117j(2, 1, 1)) {
            DamageType ek = ala().mo1499ek("HULL");
            DamageType ek2 = ala().mo1499ek("SHIELD");
            if (ek2 == null) {
                ek2 = (DamageType) bFf().mo6865M(DamageType.class);
                ek2.mo10S();
                I18NString i18NString = new I18NString();
                i18NString.set("pt", "ESCUDO");
                i18NString.set(I18NString.DEFAULT_LOCATION, "SHIELD");
                ek2.setHandle("SHIELD");
                ek2.mo2144gR(i18NString);
                ala().mo1547x(ek2);
            }
            DamageType fr2 = ek2;
            if (ek == null) {
                DamageType fr3 = (DamageType) bFf().mo6865M(DamageType.class);
                fr3.mo10S();
                I18NString i18NString2 = new I18NString();
                i18NString2.set("pt", "CASCO");
                i18NString2.set(I18NString.DEFAULT_LOCATION, "HULL");
                fr3.setHandle("HULL");
                fr3.mo2144gR(i18NString2);
                ala().mo1547x(fr3);
                fr = fr3;
            } else {
                fr = ek;
            }
            try {
                ClipType aqf = (ClipType) jt.baw();
                Object obj = jt.get("extraHullDamage");
                if (obj instanceof Integer) {
                    f = ((Integer) obj).floatValue();
                } else if (obj instanceof Float) {
                    f = ((Float) obj).floatValue();
                } else {
                    f = 0.0f;
                }
                Object obj2 = jt.get("extraShieldDamage");
                if (obj2 instanceof Integer) {
                    f2 = ((Integer) obj2).floatValue();
                } else if (obj2 instanceof Float) {
                    f2 = ((Float) obj2).floatValue();
                } else {
                    f2 = 0.0f;
                }
                System.out.println("Clip.OnLoadVersion-> Converting " + aqf.mo19891ke().get() + " S: " + f2 + ", H: " + f);
                aqf.bpA().put(fr2, Float.valueOf(f2));
                aqf.bpA().put(fr, Float.valueOf(f));
            } catch (Exception e) {
                System.err.println("BaseClipType isOlderThan(2,0,0)" + e);
            }
        }
    }
}
