package game.script.item;

import game.network.message.externalizable.aCE;
import game.script.Character;
import game.script.hangar.Bay;
import game.script.player.Player;
import game.script.ship.Ship;
import logic.baa.C1616Xf;
import logic.baa.C1634Xv;
import logic.baa.C5511aMd;
import logic.baa.C6485anp;
import logic.data.mbean.C0371Ex;
import logic.res.code.C5640aRc;
import logic.res.code.C5663aRz;
import logic.res.code.C6494any;
import logic.res.html.C2491fm;
import p001a.*;

import java.util.Collection;

@C5511aMd
@C6485anp
/* renamed from: a.aRQ */
/* compiled from: a */
public class ShipItem extends Item implements C1616Xf {
    public static final C2491fm _f_dispose_0020_0028_0029V = null;
    /* renamed from: _f_getShip_0020_0028_0029Ltaikodom_002fgame_002fscript_002fship_002fShip_003b */
    public static final C2491fm f3697x851d656b = null;
    public static final int _m_fieldCount = 0;
    public static final C5663aRz[] _m_fields = null;
    public static final int _m_methodCount = 0;
    public static final C2491fm[] _m_methods = null;
    public static final C2491fm aGt = null;
    public static final C5663aRz aOP = null;
    public static final C2491fm bHG = null;
    public static final C2491fm cRs = null;
    public static final C2491fm ccL = null;
    public static final C2491fm ccU = null;
    public static final C2491fm gCZ = null;
    public static final C2491fm gDa = null;
    public static final C5663aRz hiw = null;
    public static final C2491fm hiy = null;
    public static final long serialVersionUID = 0;
    public static C6494any ___iScriptClass;
    @C0064Am(aul = "54f1370fe99eb1584eebcc9c64db9673", aum = 1)
    private static Ship aOO;
    @C0064Am(aul = "c76f76c288a0fdd7195aa544502c705f", aum = 0)
    private static boolean cSS;

    static {
        m17764V();
    }

    public ShipItem() {
        super((C5540aNg) null);
        super.mo10S();
    }

    public ShipItem(C5540aNg ang) {
        super(ang);
    }

    public ShipItem(ItemType jCVar, Ship fAVar) {
        super((C5540aNg) null);
        super._m_script_init(jCVar, fAVar);
    }

    /* renamed from: V */
    static void m17764V() {
        serialVersionUID = (long) 1;
        _m_fieldCount = Item._m_fieldCount + 2;
        _m_methodCount = Item._m_methodCount + 10;
        int i = Item._m_fieldCount;
        C5663aRz[] arzArr = new C5663aRz[(i + 2)];
        C5663aRz b = C5640aRc.m17844b(ShipItem.class, "c76f76c288a0fdd7195aa544502c705f", i);
        hiw = b;
        arzArr[i] = b;
        int i2 = i + 1;
        C5663aRz b2 = C5640aRc.m17844b(ShipItem.class, "54f1370fe99eb1584eebcc9c64db9673", i2);
        aOP = b2;
        arzArr[i2] = b2;
        int i3 = i2 + 1;
        _m_fields = arzArr;
        C1634Xv.m11725a((Object[]) Item._m_fields, (Object[]) _m_fields);
        int i4 = Item._m_methodCount;
        C2491fm[] fmVarArr = new C2491fm[(i4 + 10)];
        C2491fm a = C4105zY.m41624a(ShipItem.class, "5a41cc9bff0f0274afd22bf467992ba4", i4);
        f3697x851d656b = a;
        fmVarArr[i4] = a;
        int i5 = i4 + 1;
        C2491fm a2 = C4105zY.m41624a(ShipItem.class, "b7fb53db6aeba687370f5baafcd10a19", i5);
        bHG = a2;
        fmVarArr[i5] = a2;
        int i6 = i5 + 1;
        C2491fm a3 = C4105zY.m41624a(ShipItem.class, "a47152f1181d2e66d0202a90268a238d", i6);
        ccL = a3;
        fmVarArr[i6] = a3;
        int i7 = i6 + 1;
        C2491fm a4 = C4105zY.m41624a(ShipItem.class, "c807dcbeedbc3f77d0b14f7c8601cdfb", i7);
        aGt = a4;
        fmVarArr[i7] = a4;
        int i8 = i7 + 1;
        C2491fm a5 = C4105zY.m41624a(ShipItem.class, "1c15a95d424fc465ba7c259cce1ab785", i8);
        _f_dispose_0020_0028_0029V = a5;
        fmVarArr[i8] = a5;
        int i9 = i8 + 1;
        C2491fm a6 = C4105zY.m41624a(ShipItem.class, "90f4f1b6e488834ad61285ce2063e0b0", i9);
        cRs = a6;
        fmVarArr[i9] = a6;
        int i10 = i9 + 1;
        C2491fm a7 = C4105zY.m41624a(ShipItem.class, "37ccc86362ade69306af0df4adbed393", i10);
        gCZ = a7;
        fmVarArr[i10] = a7;
        int i11 = i10 + 1;
        C2491fm a8 = C4105zY.m41624a(ShipItem.class, "776949baed07033d84e70871251a07a6", i11);
        hiy = a8;
        fmVarArr[i11] = a8;
        int i12 = i11 + 1;
        C2491fm a9 = C4105zY.m41624a(ShipItem.class, "333e086b03866ced0d75b32b2ccfbe5c", i12);
        ccU = a9;
        fmVarArr[i12] = a9;
        int i13 = i12 + 1;
        C2491fm a10 = C4105zY.m41624a(ShipItem.class, "05d87ad26d03ac3ee3936653d69905bb", i13);
        gDa = a10;
        fmVarArr[i13] = a10;
        int i14 = i13 + 1;
        _m_methods = fmVarArr;
        C1634Xv.m11725a((Object[]) Item._m_methods, (Object[]) _m_methods);
        ___iScriptClass = aUO.m18566a(ShipItem.class, C0371Ex.class, _m_fields, _m_methods);
    }

    /* renamed from: Tn */
    private Ship m17763Tn() {
        return (Ship) bFf().mo5608dq().mo3214p(aOP);
    }

    @C0064Am(aul = "333e086b03866ced0d75b32b2ccfbe5c", aum = 0)
    @C5566aOg
    /* renamed from: bM */
    private void m17765bM(boolean z) {
        throw new aWi(new aCE(this, ccU, new Object[]{new Boolean(z)}));
    }

    private boolean cJQ() {
        return bFf().mo5608dq().mo3201h(hiw);
    }

    /* renamed from: hS */
    private void m17767hS(boolean z) {
        bFf().mo5608dq().mo3153a(hiw, z);
    }

    /* renamed from: k */
    private void m17768k(Ship fAVar) {
        bFf().mo5608dq().mo3197f(aOP, fAVar);
    }

    /* renamed from: Iq */
    public final int mo302Iq() {
        switch (bFf().mo6893i(aGt)) {
            case 0:
                return 0;
            case 2:
                return ((Integer) bFf().mo5606d(new aCE(this, aGt, new Object[0]))).intValue();
            case 3:
                bFf().mo5606d(new aCE(this, aGt, new Object[0]));
                break;
        }
        return m17762PK();
    }

    /* renamed from: S */
    public void mo10S() {
        super.mo10S();
    }

    /* renamed from: T */
    public C6494any mo11T() {
        return ___iScriptClass;
    }

    /* renamed from: U */
    public C2491fm[] mo12U() {
        return _m_methods;
    }

    /* renamed from: W */
    public Object mo13W() {
        return new C0371Ex(this);
    }

    /* renamed from: a */
    public Object mo14a(C0495Gr gr) {
        Object[] args = gr.getArgs();
        switch (gr.mo2417hq() - Item._m_methodCount) {
            case 0:
                return m17761Mo();
            case 1:
                return new Float(aoH());
            case 2:
                asR();
                return null;
            case 3:
                return new Integer(m17762PK());
            case 4:
                m17766fg();
                return null;
            case 5:
                return new Boolean(aNO());
            case 6:
                return new Boolean(cwZ());
            case 7:
                return new Boolean(cJT());
            case 8:
                m17765bM(((Boolean) args[0]).booleanValue());
                return null;
            case 9:
                return new Boolean(cxb());
            default:
                return super.mo14a(gr);
        }
    }

    /* renamed from: a */
    public void mo15a(Collection collection, C0495Gr gr) {
        super.mo15a(collection, gr);
    }

    public boolean aNP() {
        switch (bFf().mo6893i(cRs)) {
            case 0:
                return false;
            case 2:
                return ((Boolean) bFf().mo5606d(new aCE(this, cRs, new Object[0]))).booleanValue();
            case 3:
                bFf().mo5606d(new aCE(this, cRs, new Object[0]));
                break;
        }
        return aNO();
    }

    /* renamed from: al */
    public Ship mo11127al() {
        switch (bFf().mo6893i(f3697x851d656b)) {
            case 0:
                return null;
            case 2:
                return (Ship) bFf().mo5606d(new aCE(this, f3697x851d656b, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f3697x851d656b, new Object[0]));
                break;
        }
        return m17761Mo();
    }

    public void asS() {
        switch (bFf().mo6893i(ccL)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, ccL, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, ccL, new Object[0]));
                break;
        }
        asR();
    }

    @C5566aOg
    /* renamed from: bN */
    public void mo11129bN(boolean z) {
        switch (bFf().mo6893i(ccU)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, ccU, new Object[]{new Boolean(z)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, ccU, new Object[]{new Boolean(z)}));
                break;
        }
        m17765bM(z);
    }

    /* renamed from: c */
    public C5663aRz[] mo25c() {
        return _m_fields;
    }

    public boolean cxa() {
        switch (bFf().mo6893i(gCZ)) {
            case 0:
                return false;
            case 2:
                return ((Boolean) bFf().mo5606d(new aCE(this, gCZ, new Object[0]))).booleanValue();
            case 3:
                bFf().mo5606d(new aCE(this, gCZ, new Object[0]));
                break;
        }
        return cwZ();
    }

    public boolean cxc() {
        switch (bFf().mo6893i(gDa)) {
            case 0:
                return false;
            case 2:
                return ((Boolean) bFf().mo5606d(new aCE(this, gDa, new Object[0]))).booleanValue();
            case 3:
                bFf().mo5606d(new aCE(this, gDa, new Object[0]));
                break;
        }
        return cxb();
    }

    public void dispose() {
        switch (bFf().mo6893i(_f_dispose_0020_0028_0029V)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, _f_dispose_0020_0028_0029V, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, _f_dispose_0020_0028_0029V, new Object[0]));
                break;
        }
        m17766fg();
    }

    public float getVolume() {
        switch (bFf().mo6893i(bHG)) {
            case 0:
                return 0.0f;
            case 2:
                return ((Float) bFf().mo5606d(new aCE(this, bHG, new Object[0]))).floatValue();
            case 3:
                bFf().mo5606d(new aCE(this, bHG, new Object[0]));
                break;
        }
        return aoH();
    }

    public boolean isUsable() {
        switch (bFf().mo6893i(hiy)) {
            case 0:
                return false;
            case 2:
                return ((Boolean) bFf().mo5606d(new aCE(this, hiy, new Object[0]))).booleanValue();
            case 3:
                bFf().mo5606d(new aCE(this, hiy, new Object[0]));
                break;
        }
        return cJT();
    }

    /* renamed from: a */
    public void mo11126a(ItemType jCVar, Ship fAVar) {
        super.mo306n(jCVar);
        m17768k(fAVar);
        m17767hS(true);
    }

    @C0064Am(aul = "5a41cc9bff0f0274afd22bf467992ba4", aum = 0)
    /* renamed from: Mo */
    private Ship m17761Mo() {
        return m17763Tn();
    }

    @C0064Am(aul = "b7fb53db6aeba687370f5baafcd10a19", aum = 0)
    private float aoH() {
        return bAP().mo19865HA();
    }

    @C0064Am(aul = "a47152f1181d2e66d0202a90268a238d", aum = 0)
    private void asR() {
        Character agj = mo11127al().agj();
        if (agj instanceof Player) {
            Player aku = (Player) agj;
            if (aku.bQx() != null && aku.bQx().agr() == this && !(aku.bQx().agr().bNh() instanceof Bay)) {
                aku.mo12634P((Ship) null);
            }
        }
    }

    @C0064Am(aul = "c807dcbeedbc3f77d0b14f7c8601cdfb", aum = 0)
    /* renamed from: PK */
    private int m17762PK() {
        return 1;
    }

    @C0064Am(aul = "1c15a95d424fc465ba7c259cce1ab785", aum = 0)
    /* renamed from: fg */
    private void m17766fg() {
        if (isDisposed()) {
            mo8358lY("ShipItem " + bFf().getObjectId() + " is being disposed again");
            return;
        }
        super.dispose();
        if (!m17763Tn().isDisposed()) {
            m17763Tn().dispose();
        }
    }

    @C0064Am(aul = "90f4f1b6e488834ad61285ce2063e0b0", aum = 0)
    private boolean aNO() {
        if (!super.aNP()) {
            return false;
        }
        return m17763Tn().isEmpty();
    }

    @C0064Am(aul = "37ccc86362ade69306af0df4adbed393", aum = 0)
    private boolean cwZ() {
        return m17763Tn().isEmpty();
    }

    @C0064Am(aul = "776949baed07033d84e70871251a07a6", aum = 0)
    private boolean cJT() {
        return cJQ();
    }

    @C0064Am(aul = "05d87ad26d03ac3ee3936653d69905bb", aum = 0)
    private boolean cxb() {
        if (!super.cxc()) {
            return false;
        }
        return mo11127al().isEmpty();
    }
}
