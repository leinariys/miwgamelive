package game.script.item.durability;

import game.network.message.externalizable.aCE;
import logic.baa.*;
import logic.data.mbean.C2745jQ;
import logic.res.code.C5640aRc;
import logic.res.code.C5663aRz;
import logic.res.code.C6494any;
import logic.res.html.C2491fm;
import p001a.*;

import java.util.Collection;

@C5511aMd
@C6485anp
/* renamed from: a.azM */
/* compiled from: a */
public class ItemDurability extends aDJ implements C1616Xf {
    public static final C5663aRz _f_startTime = null;
    public static final int _m_fieldCount = 0;
    public static final C5663aRz[] _m_fields = null;
    public static final int _m_methodCount = 0;
    public static final C2491fm[] _m_methods = null;
    public static final C2491fm eIY = null;
    public static final C2491fm gDr = null;
    public static final C5663aRz hbn = null;
    public static final C5663aRz hbo = null;
    public static final C5663aRz hbp = null;
    public static final C5663aRz hbq = null;
    public static final C2491fm hbr = null;
    public static final C2491fm hbs = null;
    public static final C2491fm hbt = null;
    public static final C2491fm hbu = null;
    public static final C2491fm hbv = null;
    public static final C2491fm hbw = null;
    public static final C2491fm hbx = null;
    public static final long serialVersionUID = 0;
    private static final long ONE_DAY = 86400000;
    private static final long ONE_HOUR = 3600000;
    private static final long ONE_SECOND = 1000;
    public static C6494any ___iScriptClass;
    @C0064Am(aul = "87e0914360eaa6771e2a52d5d00d2819", aum = 0)
    private static long apo;
    @C0064Am(aul = "f2a74a412f16e6141835c0ca915d5aea", aum = 3)
    private static boolean app;
    @C0064Am(aul = "fba7e3287637825e9ae9468f5363dae6", aum = 4)
    private static boolean apq;
    @C0064Am(aul = "6b8ad227ef328e2dc29c3b65d5ecf1b1", aum = 1)
    private static long startTime;
    @C0064Am(aul = "73a0e9cd986152b71656cda4949a71a9", aum = 2)
    private static boolean started;

    static {
        m27626V();
    }

    public ItemDurability() {
        super((C5540aNg) null);
        super.mo10S();
    }

    public ItemDurability(long j) {
        super((C5540aNg) null);
        super._m_script_init(j);
    }

    public ItemDurability(C5540aNg ang) {
        super(ang);
    }

    /* renamed from: V */
    static void m27626V() {
        serialVersionUID = (long) 1;
        _m_fieldCount = aDJ._m_fieldCount + 5;
        _m_methodCount = aDJ._m_methodCount + 9;
        int i = aDJ._m_fieldCount;
        C5663aRz[] arzArr = new C5663aRz[(i + 5)];
        C5663aRz b = C5640aRc.m17844b(ItemDurability.class, "87e0914360eaa6771e2a52d5d00d2819", i);
        hbn = b;
        arzArr[i] = b;
        int i2 = i + 1;
        C5663aRz b2 = C5640aRc.m17844b(ItemDurability.class, "6b8ad227ef328e2dc29c3b65d5ecf1b1", i2);
        _f_startTime = b2;
        arzArr[i2] = b2;
        int i3 = i2 + 1;
        C5663aRz b3 = C5640aRc.m17844b(ItemDurability.class, "73a0e9cd986152b71656cda4949a71a9", i3);
        hbo = b3;
        arzArr[i3] = b3;
        int i4 = i3 + 1;
        C5663aRz b4 = C5640aRc.m17844b(ItemDurability.class, "f2a74a412f16e6141835c0ca915d5aea", i4);
        hbp = b4;
        arzArr[i4] = b4;
        int i5 = i4 + 1;
        C5663aRz b5 = C5640aRc.m17844b(ItemDurability.class, "fba7e3287637825e9ae9468f5363dae6", i5);
        hbq = b5;
        arzArr[i5] = b5;
        int i6 = i5 + 1;
        _m_fields = arzArr;
        C1634Xv.m11725a((Object[]) aDJ._m_fields, (Object[]) _m_fields);
        int i7 = aDJ._m_methodCount;
        C2491fm[] fmVarArr = new C2491fm[(i7 + 9)];
        C2491fm a = C4105zY.m41624a(ItemDurability.class, "3e1e552d562785476247b5e2e698a6ce", i7);
        hbr = a;
        fmVarArr[i7] = a;
        int i8 = i7 + 1;
        C2491fm a2 = C4105zY.m41624a(ItemDurability.class, "62f5236ce9cc79ea8b8649e7c5416b74", i8);
        hbs = a2;
        fmVarArr[i8] = a2;
        int i9 = i8 + 1;
        C2491fm a3 = C4105zY.m41624a(ItemDurability.class, "d27cccf60e115742674fdde137673f9c", i9);
        hbt = a3;
        fmVarArr[i9] = a3;
        int i10 = i9 + 1;
        C2491fm a4 = C4105zY.m41624a(ItemDurability.class, "59f69ce366634749f092109a95cee7e5", i10);
        hbu = a4;
        fmVarArr[i10] = a4;
        int i11 = i10 + 1;
        C2491fm a5 = C4105zY.m41624a(ItemDurability.class, "1babd1524f12b42926cc1a02726e55a6", i11);
        eIY = a5;
        fmVarArr[i11] = a5;
        int i12 = i11 + 1;
        C2491fm a6 = C4105zY.m41624a(ItemDurability.class, "3e3e860bfebec9bad32dfc35fb91250d", i12);
        hbv = a6;
        fmVarArr[i12] = a6;
        int i13 = i12 + 1;
        C2491fm a7 = C4105zY.m41624a(ItemDurability.class, "098341db842537489f825eafffd1028e", i13);
        hbw = a7;
        fmVarArr[i13] = a7;
        int i14 = i13 + 1;
        C2491fm a8 = C4105zY.m41624a(ItemDurability.class, "538cf33d6fb00b8216c6d5f80bec1b5c", i14);
        hbx = a8;
        fmVarArr[i14] = a8;
        int i15 = i14 + 1;
        C2491fm a9 = C4105zY.m41624a(ItemDurability.class, "6f20de81276e1f8321f1aad56ae0dbb5", i15);
        gDr = a9;
        fmVarArr[i15] = a9;
        int i16 = i15 + 1;
        _m_methods = fmVarArr;
        C1634Xv.m11725a((Object[]) aDJ._m_methods, (Object[]) _m_methods);
        ___iScriptClass = aUO.m18566a(ItemDurability.class, C2745jQ.class, _m_fields, _m_methods);
    }

    /* renamed from: Bk */
    private long m27625Bk() {
        return bFf().mo5608dq().mo3213o(_f_startTime);
    }

    @C0064Am(aul = "1babd1524f12b42926cc1a02726e55a6", aum = 0)
    @C5566aOg
    private void bFN() {
        throw new aWi(new aCE(this, eIY, new Object[0]));
    }

    /* renamed from: bz */
    private void m27627bz(long j) {
        bFf().mo5608dq().mo3184b(_f_startTime, j);
    }

    private long cGP() {
        return bFf().mo5608dq().mo3213o(hbn);
    }

    private boolean cGQ() {
        return bFf().mo5608dq().mo3201h(hbo);
    }

    private boolean cGR() {
        return bFf().mo5608dq().mo3201h(hbp);
    }

    private boolean cGS() {
        return bFf().mo5608dq().mo3201h(hbq);
    }

    @C0064Am(aul = "3e1e552d562785476247b5e2e698a6ce", aum = 0)
    @C5566aOg
    private boolean cGT() {
        throw new aWi(new aCE(this, hbr, new Object[0]));
    }

    @C0064Am(aul = "62f5236ce9cc79ea8b8649e7c5416b74", aum = 0)
    @C5566aOg
    private void cGV() {
        throw new aWi(new aCE(this, hbs, new Object[0]));
    }

    @C5566aOg
    private void cGW() {
        switch (bFf().mo6893i(hbs)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, hbs, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, hbs, new Object[0]));
                break;
        }
        cGV();
    }

    /* renamed from: hA */
    private void m27628hA(boolean z) {
        bFf().mo5608dq().mo3153a(hbq, z);
    }

    /* renamed from: hy */
    private void m27629hy(boolean z) {
        bFf().mo5608dq().mo3153a(hbo, z);
    }

    /* renamed from: hz */
    private void m27630hz(boolean z) {
        bFf().mo5608dq().mo3153a(hbp, z);
    }

    @C0064Am(aul = "6f20de81276e1f8321f1aad56ae0dbb5", aum = 0)
    @C5566aOg
    /* renamed from: iV */
    private void m27631iV(long j) {
        throw new aWi(new aCE(this, gDr, new Object[]{new Long(j)}));
    }

    /* renamed from: ju */
    private void m27632ju(long j) {
        bFf().mo5608dq().mo3184b(hbn, j);
    }

    /* renamed from: T */
    public C6494any mo11T() {
        return ___iScriptClass;
    }

    /* renamed from: U */
    public C2491fm[] mo12U() {
        return _m_methods;
    }

    /* renamed from: W */
    public Object mo13W() {
        return new C2745jQ(this);
    }

    /* renamed from: a */
    public Object mo14a(C0495Gr gr) {
        Object[] args = gr.getArgs();
        switch (gr.mo2417hq() - aDJ._m_methodCount) {
            case 0:
                return new Boolean(cGT());
            case 1:
                cGV();
                return null;
            case 2:
                return new Boolean(cGX());
            case 3:
                return new Boolean(cGY());
            case 4:
                bFN();
                return null;
            case 5:
                return new Long(cHa());
            case 6:
                return new Long(cHb());
            case 7:
                return new Boolean(cHc());
            case 8:
                m27631iV(((Long) args[0]).longValue());
                return null;
            default:
                return super.mo14a(gr);
        }
    }

    /* renamed from: a */
    public void mo15a(Collection collection, C0495Gr gr) {
        super.mo15a(collection, gr);
    }

    /* renamed from: c */
    public C5663aRz[] mo25c() {
        return _m_fields;
    }

    @C5566aOg
    public boolean cGU() {
        switch (bFf().mo6893i(hbr)) {
            case 0:
                return false;
            case 2:
                return ((Boolean) bFf().mo5606d(new aCE(this, hbr, new Object[0]))).booleanValue();
            case 3:
                bFf().mo5606d(new aCE(this, hbr, new Object[0]));
                break;
        }
        return cGT();
    }

    public boolean cGZ() {
        switch (bFf().mo6893i(hbu)) {
            case 0:
                return false;
            case 2:
                return ((Boolean) bFf().mo5606d(new aCE(this, hbu, new Object[0]))).booleanValue();
            case 3:
                bFf().mo5606d(new aCE(this, hbu, new Object[0]));
                break;
        }
        return cGY();
    }

    public boolean cHd() {
        switch (bFf().mo6893i(hbx)) {
            case 0:
                return false;
            case 2:
                return ((Boolean) bFf().mo5606d(new aCE(this, hbx, new Object[0]))).booleanValue();
            case 3:
                bFf().mo5606d(new aCE(this, hbx, new Object[0]));
                break;
        }
        return cHc();
    }

    public long getRemainingTime() {
        switch (bFf().mo6893i(hbv)) {
            case 0:
                return 0;
            case 2:
                return ((Long) bFf().mo5606d(new aCE(this, hbv, new Object[0]))).longValue();
            case 3:
                bFf().mo5606d(new aCE(this, hbv, new Object[0]));
                break;
        }
        return cHa();
    }

    public long getTotalTime() {
        switch (bFf().mo6893i(hbw)) {
            case 0:
                return 0;
            case 2:
                return ((Long) bFf().mo5606d(new aCE(this, hbw, new Object[0]))).longValue();
            case 3:
                bFf().mo5606d(new aCE(this, hbw, new Object[0]));
                break;
        }
        return cHb();
    }

    @C5566aOg
    /* renamed from: iW */
    public void mo17186iW(long j) {
        switch (bFf().mo6893i(gDr)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, gDr, new Object[]{new Long(j)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, gDr, new Object[]{new Long(j)}));
                break;
        }
        m27631iV(j);
    }

    public boolean isStarted() {
        switch (bFf().mo6893i(hbt)) {
            case 0:
                return false;
            case 2:
                return ((Boolean) bFf().mo5606d(new aCE(this, hbt, new Object[0]))).booleanValue();
            case 3:
                bFf().mo5606d(new aCE(this, hbt, new Object[0]));
                break;
        }
        return cGX();
    }

    @C5566aOg
    public void refresh() {
        switch (bFf().mo6893i(eIY)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, eIY, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, eIY, new Object[0]));
                break;
        }
        bFN();
    }

    /* renamed from: S */
    public void mo10S() {
        mo17183gR(0);
    }

    /* renamed from: gR */
    public void mo17183gR(long j) {
        super.mo10S();
        m27628hA(j == 0);
        if (cGS()) {
            m27632ju(120000);
        } else {
            m27632ju(ONE_SECOND * j * 10);
        }
        m27629hy(false);
        m27630hz(false);
    }

    @C0064Am(aul = "d27cccf60e115742674fdde137673f9c", aum = 0)
    private boolean cGX() {
        return cGQ();
    }

    @C0064Am(aul = "59f69ce366634749f092109a95cee7e5", aum = 0)
    private boolean cGY() {
        return cGR();
    }

    @C0064Am(aul = "3e3e860bfebec9bad32dfc35fb91250d", aum = 0)
    private long cHa() {
        return !cGR() ? cVr() - m27625Bk() : cGP();
    }

    @C0064Am(aul = "098341db842537489f825eafffd1028e", aum = 0)
    private long cHb() {
        return cGP();
    }

    @C0064Am(aul = "538cf33d6fb00b8216c6d5f80bec1b5c", aum = 0)
    private boolean cHc() {
        return cGS();
    }
}
