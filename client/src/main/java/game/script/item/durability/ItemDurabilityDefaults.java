package game.script.item.durability;

import game.network.message.externalizable.aCE;
import logic.baa.*;
import logic.data.mbean.C1189RZ;
import logic.res.code.C5640aRc;
import logic.res.code.C5663aRz;
import logic.res.code.C6494any;
import logic.res.html.C2491fm;
import p001a.*;

import java.util.Collection;
import java.util.UUID;

@C5511aMd
@C6485anp
/* renamed from: a.amn  reason: case insensitive filesystem */
/* compiled from: a */
public class ItemDurabilityDefaults extends aDJ implements C0468GU, C1616Xf {
    public static final int _m_fieldCount = 0;
    public static final C5663aRz[] _m_fields = null;
    public static final int _m_methodCount = 0;
    public static final C2491fm[] _m_methods = null;
    /* renamed from: bL */
    public static final C5663aRz f4929bL = null;
    /* renamed from: bN */
    public static final C2491fm f4930bN = null;
    /* renamed from: bO */
    public static final C2491fm f4931bO = null;
    /* renamed from: bP */
    public static final C2491fm f4932bP = null;
    public static final C5663aRz gaa = null;
    public static final C5663aRz gab = null;
    public static final C2491fm gac = null;
    public static final C2491fm gad = null;
    public static final C2491fm gae = null;
    public static final C2491fm gaf = null;
    public static final long serialVersionUID = 0;
    public static C6494any ___iScriptClass;
    @C0064Am(aul = "dd7013ee64dc17ba31907efcab6638e4", aum = 2)

    /* renamed from: bK */
    private static UUID f4928bK;
    @C0064Am(aul = "b9efa6caed4550b2188d45e88f4297ad", aum = 0)
    private static long eca;
    @C0064Am(aul = "ebbc15844f5b904f786d503eeec46f68", aum = 1)
    private static long ecb;

    static {
        m23981V();
    }

    public ItemDurabilityDefaults() {
        super((C5540aNg) null);
        super.mo10S();
    }

    public ItemDurabilityDefaults(C5540aNg ang) {
        super(ang);
    }

    /* renamed from: V */
    static void m23981V() {
        serialVersionUID = (long) 1;
        _m_fieldCount = aDJ._m_fieldCount + 3;
        _m_methodCount = aDJ._m_methodCount + 7;
        int i = aDJ._m_fieldCount;
        C5663aRz[] arzArr = new C5663aRz[(i + 3)];
        C5663aRz b = C5640aRc.m17844b(ItemDurabilityDefaults.class, "b9efa6caed4550b2188d45e88f4297ad", i);
        gaa = b;
        arzArr[i] = b;
        int i2 = i + 1;
        C5663aRz b2 = C5640aRc.m17844b(ItemDurabilityDefaults.class, "ebbc15844f5b904f786d503eeec46f68", i2);
        gab = b2;
        arzArr[i2] = b2;
        int i3 = i2 + 1;
        C5663aRz b3 = C5640aRc.m17844b(ItemDurabilityDefaults.class, "dd7013ee64dc17ba31907efcab6638e4", i3);
        f4929bL = b3;
        arzArr[i3] = b3;
        int i4 = i3 + 1;
        _m_fields = arzArr;
        C1634Xv.m11725a((Object[]) aDJ._m_fields, (Object[]) _m_fields);
        int i5 = aDJ._m_methodCount;
        C2491fm[] fmVarArr = new C2491fm[(i5 + 7)];
        C2491fm a = C4105zY.m41624a(ItemDurabilityDefaults.class, "076fd644b00e2b444d5e7d924e1ef7d5", i5);
        f4930bN = a;
        fmVarArr[i5] = a;
        int i6 = i5 + 1;
        C2491fm a2 = C4105zY.m41624a(ItemDurabilityDefaults.class, "343b906a5ce5ff5b795a482995c99fb7", i6);
        f4931bO = a2;
        fmVarArr[i6] = a2;
        int i7 = i6 + 1;
        C2491fm a3 = C4105zY.m41624a(ItemDurabilityDefaults.class, "357d139da612f74c2f9d6a0864165780", i7);
        f4932bP = a3;
        fmVarArr[i7] = a3;
        int i8 = i7 + 1;
        C2491fm a4 = C4105zY.m41624a(ItemDurabilityDefaults.class, "aedbded0ec762c7d66762f65120598ad", i8);
        gac = a4;
        fmVarArr[i8] = a4;
        int i9 = i8 + 1;
        C2491fm a5 = C4105zY.m41624a(ItemDurabilityDefaults.class, "15f7f132d3f4010da6fef8ee564520df", i9);
        gad = a5;
        fmVarArr[i9] = a5;
        int i10 = i9 + 1;
        C2491fm a6 = C4105zY.m41624a(ItemDurabilityDefaults.class, "b2a08261584858eaeace54a9f1d7897a", i10);
        gae = a6;
        fmVarArr[i10] = a6;
        int i11 = i10 + 1;
        C2491fm a7 = C4105zY.m41624a(ItemDurabilityDefaults.class, "8834b6bf3d79573f7faad21b923f11c4", i11);
        gaf = a7;
        fmVarArr[i11] = a7;
        int i12 = i11 + 1;
        _m_methods = fmVarArr;
        C1634Xv.m11725a((Object[]) aDJ._m_methods, (Object[]) _m_methods);
        ___iScriptClass = aUO.m18566a(ItemDurabilityDefaults.class, C1189RZ.class, _m_fields, _m_methods);
    }

    /* renamed from: a */
    private void m23982a(UUID uuid) {
        bFf().mo5608dq().mo3197f(f4929bL, uuid);
    }

    /* renamed from: an */
    private UUID m23983an() {
        return (UUID) bFf().mo5608dq().mo3214p(f4929bL);
    }

    /* renamed from: c */
    private void m23987c(UUID uuid) {
        switch (bFf().mo6893i(f4931bO)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f4931bO, new Object[]{uuid}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f4931bO, new Object[]{uuid}));
                break;
        }
        m23986b(uuid);
    }

    private long ciO() {
        return bFf().mo5608dq().mo3213o(gaa);
    }

    private long ciP() {
        return bFf().mo5608dq().mo3213o(gab);
    }

    /* renamed from: hT */
    private void m23988hT(long j) {
        bFf().mo5608dq().mo3184b(gaa, j);
    }

    /* renamed from: hU */
    private void m23989hU(long j) {
        bFf().mo5608dq().mo3184b(gab, j);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Durability Time")
    @C0064Am(aul = "15f7f132d3f4010da6fef8ee564520df", aum = 0)
    @C5566aOg
    /* renamed from: hV */
    private void m23990hV(long j) {
        throw new aWi(new aCE(this, gad, new Object[]{new Long(j)}));
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Refresh Rate Time")
    @C0064Am(aul = "8834b6bf3d79573f7faad21b923f11c4", aum = 0)
    @C5566aOg
    /* renamed from: hX */
    private void m23991hX(long j) {
        throw new aWi(new aCE(this, gaf, new Object[]{new Long(j)}));
    }

    /* renamed from: T */
    public C6494any mo11T() {
        return ___iScriptClass;
    }

    /* renamed from: U */
    public C2491fm[] mo12U() {
        return _m_methods;
    }

    /* renamed from: W */
    public Object mo13W() {
        return new C1189RZ(this);
    }

    /* renamed from: a */
    public Object mo14a(C0495Gr gr) {
        Object[] args = gr.getArgs();
        switch (gr.mo2417hq() - aDJ._m_methodCount) {
            case 0:
                return m23984ap();
            case 1:
                m23986b((UUID) args[0]);
                return null;
            case 2:
                return m23985ar();
            case 3:
                return new Long(ciQ());
            case 4:
                m23990hV(((Long) args[0]).longValue());
                return null;
            case 5:
                return new Long(ciS());
            case 6:
                m23991hX(((Long) args[0]).longValue());
                return null;
            default:
                return super.mo14a(gr);
        }
    }

    /* renamed from: a */
    public void mo15a(Collection collection, C0495Gr gr) {
        super.mo15a(collection, gr);
    }

    /* renamed from: aq */
    public UUID mo18aq() {
        switch (bFf().mo6893i(f4930bN)) {
            case 0:
                return null;
            case 2:
                return (UUID) bFf().mo5606d(new aCE(this, f4930bN, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f4930bN, new Object[0]));
                break;
        }
        return m23984ap();
    }

    /* renamed from: c */
    public C5663aRz[] mo25c() {
        return _m_fields;
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Durability Time")
    public long ciR() {
        switch (bFf().mo6893i(gac)) {
            case 0:
                return 0;
            case 2:
                return ((Long) bFf().mo5606d(new aCE(this, gac, new Object[0]))).longValue();
            case 3:
                bFf().mo5606d(new aCE(this, gac, new Object[0]));
                break;
        }
        return ciQ();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Refresh Rate Time")
    public long ciT() {
        switch (bFf().mo6893i(gae)) {
            case 0:
                return 0;
            case 2:
                return ((Long) bFf().mo5606d(new aCE(this, gae, new Object[0]))).longValue();
            case 3:
                bFf().mo5606d(new aCE(this, gae, new Object[0]));
                break;
        }
        return ciS();
    }

    public String getHandle() {
        switch (bFf().mo6893i(f4932bP)) {
            case 0:
                return null;
            case 2:
                return (String) bFf().mo5606d(new aCE(this, f4932bP, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f4932bP, new Object[0]));
                break;
        }
        return m23985ar();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Durability Time")
    @C5566aOg
    /* renamed from: hW */
    public void mo14891hW(long j) {
        switch (bFf().mo6893i(gad)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, gad, new Object[]{new Long(j)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, gad, new Object[]{new Long(j)}));
                break;
        }
        m23990hV(j);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Refresh Rate Time")
    @C5566aOg
    /* renamed from: hY */
    public void mo14892hY(long j) {
        switch (bFf().mo6893i(gaf)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, gaf, new Object[]{new Long(j)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, gaf, new Object[]{new Long(j)}));
                break;
        }
        m23991hX(j);
    }

    @C0064Am(aul = "076fd644b00e2b444d5e7d924e1ef7d5", aum = 0)
    /* renamed from: ap */
    private UUID m23984ap() {
        return m23983an();
    }

    @C0064Am(aul = "343b906a5ce5ff5b795a482995c99fb7", aum = 0)
    /* renamed from: b */
    private void m23986b(UUID uuid) {
        m23982a(uuid);
    }

    @C0064Am(aul = "357d139da612f74c2f9d6a0864165780", aum = 0)
    /* renamed from: ar */
    private String m23985ar() {
        return "item_durability_defaults";
    }

    /* renamed from: S */
    public void mo10S() {
        super.mo10S();
        m23982a(UUID.randomUUID());
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Durability Time")
    @C0064Am(aul = "aedbded0ec762c7d66762f65120598ad", aum = 0)
    private long ciQ() {
        return ciO();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Refresh Rate Time")
    @C0064Am(aul = "b2a08261584858eaeace54a9f1d7897a", aum = 0)
    private long ciS() {
        return ciP();
    }
}
