package game.script.item;

import game.network.message.externalizable.aCE;
import game.network.message.serializable.C1556Wo;
import game.script.damage.DamageType;
import logic.baa.*;
import logic.data.mbean.C5364aGm;
import logic.res.code.C5663aRz;
import logic.res.code.C6494any;
import logic.res.html.C2491fm;
import p001a.*;
import taikodom.infra.script.I18NString;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

@C5511aMd
@C6485anp
/* renamed from: a.Qs */
/* compiled from: a */
public abstract class BaseWeaponType extends ComponentType implements C1616Xf, aOW {

    /* renamed from: Do */
    public static final C2491fm f1476Do = null;
    public static final C2491fm _f_onResurrect_0020_0028_0029V = null;
    public static final int _m_fieldCount = 0;
    public static final C5663aRz[] _m_fields = null;
    public static final int _m_methodCount = 0;
    public static final C2491fm[] _m_methods = null;
    public static final C2491fm dVN = null;
    public static final C2491fm dVO = null;
    public static final C2491fm dVP = null;
    public static final long serialVersionUID = 0;
    public static C6494any ___iScriptClass;

    static {
        m9009V();
    }

    private transient List<aNY> dVL;
    private transient boolean dVM;

    public BaseWeaponType() {
        super((C5540aNg) null);
        super.mo10S();
    }

    public BaseWeaponType(C5540aNg ang) {
        super(ang);
    }

    /* renamed from: V */
    static void m9009V() {
        serialVersionUID = (long) 1;
        _m_fieldCount = ComponentType._m_fieldCount + 0;
        _m_methodCount = ComponentType._m_methodCount + 5;
        _m_fields = new C5663aRz[(ComponentType._m_fieldCount + 0)];
        C1634Xv.m11725a((Object[]) ComponentType._m_fields, (Object[]) _m_fields);
        int i = ComponentType._m_methodCount;
        C2491fm[] fmVarArr = new C2491fm[(i + 5)];
        C2491fm a = C4105zY.m41624a(BaseWeaponType.class, "fb963b2f12b6625a677c7802fb156ec5", i);
        dVN = a;
        fmVarArr[i] = a;
        int i2 = i + 1;
        C2491fm a2 = C4105zY.m41624a(BaseWeaponType.class, "f31ba0ea157fe92b114d33ff4c394f1d", i2);
        _f_onResurrect_0020_0028_0029V = a2;
        fmVarArr[i2] = a2;
        int i3 = i2 + 1;
        C2491fm a3 = C4105zY.m41624a(BaseWeaponType.class, "3aef76284efa14539701ba05cd29b466", i3);
        f1476Do = a3;
        fmVarArr[i3] = a3;
        int i4 = i3 + 1;
        C2491fm a4 = C4105zY.m41624a(BaseWeaponType.class, "2ddbec5c287341451b38668b53f84d32", i4);
        dVO = a4;
        fmVarArr[i4] = a4;
        int i5 = i4 + 1;
        C2491fm a5 = C4105zY.m41624a(BaseWeaponType.class, "c56ff5c9a5a30f21c72c4b0bd2f796f1", i5);
        dVP = a5;
        fmVarArr[i5] = a5;
        int i6 = i5 + 1;
        _m_methods = fmVarArr;
        C1634Xv.m11725a((Object[]) ComponentType._m_methods, (Object[]) _m_methods);
        ___iScriptClass = aUO.m18566a(BaseWeaponType.class, C5364aGm.class, _m_fields, _m_methods);
    }

    @C0064Am(aul = "fb963b2f12b6625a677c7802fb156ec5", aum = 0)
    private C1556Wo<DamageType, Float> bpz() {
        throw new aWi(new aCE(this, dVN, new Object[0]));
    }

    /* renamed from: T */
    public C6494any mo11T() {
        return ___iScriptClass;
    }

    /* renamed from: U */
    public C2491fm[] mo12U() {
        return _m_methods;
    }

    /* renamed from: W */
    public Object mo13W() {
        return new C5364aGm(this);
    }

    /* renamed from: a */
    public Object mo14a(C0495Gr gr) {
        Object[] args = gr.getArgs();
        switch (gr.mo2417hq() - ComponentType._m_methodCount) {
            case 0:
                return bpz();
            case 1:
                m9011aG();
                return null;
            case 2:
                m9010a((C0665JT) args[0]);
                return null;
            case 3:
                return bpB();
            case 4:
                return new Float(m9012gs((String) args[0]));
            default:
                return super.mo14a(gr);
        }
    }

    /* renamed from: a */
    public void mo15a(Collection collection, C0495Gr gr) {
        super.mo15a(collection, gr);
    }

    /* renamed from: aH */
    public void mo70aH() {
        switch (bFf().mo6893i(_f_onResurrect_0020_0028_0029V)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, _f_onResurrect_0020_0028_0029V, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, _f_onResurrect_0020_0028_0029V, new Object[0]));
                break;
        }
        m9011aG();
    }

    /* renamed from: b */
    public void mo24b(C0665JT jt) {
        switch (bFf().mo6893i(f1476Do)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f1476Do, new Object[]{jt}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f1476Do, new Object[]{jt}));
                break;
        }
        m9010a(jt);
    }

    public C1556Wo<DamageType, Float> bpA() {
        switch (bFf().mo6893i(dVN)) {
            case 0:
                return null;
            case 2:
                return (C1556Wo) bFf().mo5606d(new aCE(this, dVN, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, dVN, new Object[0]));
                break;
        }
        return bpz();
    }

    public List<aNY> bpC() {
        switch (bFf().mo6893i(dVO)) {
            case 0:
                return null;
            case 2:
                return (List) bFf().mo5606d(new aCE(this, dVO, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, dVO, new Object[0]));
                break;
        }
        return bpB();
    }

    /* renamed from: c */
    public C5663aRz[] mo25c() {
        return _m_fields;
    }

    /* renamed from: gt */
    public float mo5100gt(String str) {
        switch (bFf().mo6893i(dVP)) {
            case 0:
                return 0.0f;
            case 2:
                return ((Float) bFf().mo5606d(new aCE(this, dVP, new Object[]{str}))).floatValue();
            case 3:
                bFf().mo5606d(new aCE(this, dVP, new Object[]{str}));
                break;
        }
        return m9012gs(str);
    }

    /* renamed from: S */
    public void mo10S() {
        super.mo10S();
        C1556Wo<DamageType, Float> bpA = bpA();
        for (DamageType put : ala().aLs()) {
            bpA.put(put, Float.valueOf(0.0f));
        }
    }

    @C0064Am(aul = "f31ba0ea157fe92b114d33ff4c394f1d", aum = 0)
    /* renamed from: aG */
    private void m9011aG() {
        super.mo70aH();
        C1556Wo<DamageType, Float> bpA = bpA();
        for (DamageType next : ala().aLs()) {
            if (!bpA.containsKey(next)) {
                bpA.put(next, Float.valueOf(0.0f));
            }
        }
    }

    @C0064Am(aul = "3aef76284efa14539701ba05cd29b466", aum = 0)
    /* renamed from: a */
    private void m9010a(C0665JT jt) {
        DamageType fr;
        float floatValue;
        float floatValue2;
        if (jt.mo3117j(2, 1, 0)) {
            DamageType ek = ala().mo1499ek("HULL");
            DamageType ek2 = ala().mo1499ek("SHIELD");
            if (ek2 == null) {
                ek2 = (DamageType) bFf().mo6865M(DamageType.class);
                ek2.mo10S();
                I18NString i18NString = new I18NString();
                i18NString.set("pt", "ESCUDO");
                i18NString.set(I18NString.DEFAULT_LOCATION, "SHIELD");
                ek2.setHandle("SHIELD");
                ek2.mo2144gR(i18NString);
                ala().mo1547x(ek2);
            }
            DamageType fr2 = ek2;
            if (ek == null) {
                DamageType fr3 = (DamageType) bFf().mo6865M(DamageType.class);
                fr3.mo10S();
                I18NString i18NString2 = new I18NString();
                i18NString2.set("pt", "CASCO");
                i18NString2.set(I18NString.DEFAULT_LOCATION, "HULL");
                fr3.setHandle("HULL");
                fr3.mo2144gR(i18NString2);
                ala().mo1547x(fr3);
                fr = fr3;
            } else {
                fr = ek;
            }
            try {
                WeaponType apt = (WeaponType) jt.baw();
                Object obj = jt.get("shieldDamage");
                Object obj2 = jt.get("hullDamage");
                if (obj instanceof Integer) {
                    floatValue = ((Integer) obj).floatValue();
                } else {
                    floatValue = ((Float) obj).floatValue();
                }
                if (obj2 instanceof Integer) {
                    floatValue2 = ((Integer) obj2).floatValue();
                } else {
                    floatValue2 = ((Float) obj2).floatValue();
                }
                apt.bpA().put(fr2, Float.valueOf(floatValue));
                apt.bpA().put(fr, Float.valueOf(floatValue2));
            } catch (Exception e) {
                System.err.println("WeaponType 2.0.0, " + e);
            }
        }
    }

    @C0064Am(aul = "2ddbec5c287341451b38668b53f84d32", aum = 0)
    private List<aNY> bpB() {
        if (this.dVL == null) {
            try {
                this.dVL = aBX.cMP();
            } catch (C6719asP e) {
                mo8358lY("Problems Reading Shot Size List file. Check the file");
                return null;
            }
        }
        return new ArrayList(this.dVL);
    }

    @C0064Am(aul = "c56ff5c9a5a30f21c72c4b0bd2f796f1", aum = 0)
    /* renamed from: gs */
    private float m9012gs(String str) {
        if (this.dVL == null) {
            if (this.dVM) {
                return 30.0f;
            }
            this.dVL = bpC();
            this.dVM = true;
            if (this.dVL == null) {
                mo8358lY("Problems reading bone file.");
                return 30.0f;
            }
        }
        for (aNY next : this.dVL) {
            if (next.getName().equals(str)) {
                return next.getLength();
            }
        }
        return 30.0f;
    }
}
