package game.script.item;

import com.hoplon.geometry.Vec3f;
import game.geometry.Vec3d;
import game.network.message.externalizable.aCE;
import game.script.Actor;
import logic.baa.C1616Xf;
import logic.baa.C1634Xv;
import logic.baa.C5511aMd;
import logic.baa.C6485anp;
import logic.data.mbean.C3262pj;
import logic.render.IEngineGraphics;
import logic.res.code.C5640aRc;
import logic.res.code.C5663aRz;
import logic.res.code.C6494any;
import logic.res.html.C2491fm;
import logic.thred.C6615aqP;
import p001a.*;
import taikodom.render.scene.RBillboard;
import taikodom.render.scene.RRay;
import taikodom.render.scene.Scene;
import taikodom.render.scene.SceneObject;

import javax.vecmath.Tuple3f;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

@C5829abJ("2.1.1")
@C6485anp
@C2712iu(version = "2.1.1")
@C5511aMd
/* renamed from: a.aVV */
/* compiled from: a */
public class RayWeapon extends EnergyWeapon implements C1616Xf {
    public static final int _m_fieldCount = 0;
    public static final C5663aRz[] _m_fields = null;
    public static final int _m_methodCount = 0;
    public static final C2491fm[] _m_methods = null;
    public static final C2491fm bMn = null;
    public static final C2491fm clw = null;
    public static final C2491fm hAB = null;
    public static final C2491fm jdg = null;
    public static final C2491fm jdh = null;
    public static final C2491fm jdi = null;
    public static final C2491fm jdj = null;
    public static final C2491fm jdk = null;
    /* renamed from: kZ */
    public static final C5663aRz f3919kZ = null;
    public static final long serialVersionUID = 0;
    public static C6494any ___iScriptClass;
    @C0064Am(aul = "df2d8a20fec672f8631d978ea261230b", aum = 0)
    @C5454aJy("Pulse Life Time")
    private static float lifeTime;

    static {
        m18862V();
    }

    private transient RBillboard jde;
    private transient RRay jdf;

    public RayWeapon() {
        super((C5540aNg) null);
        super.mo10S();
    }

    public RayWeapon(RayWeaponType gj) {
        super((C5540aNg) null);
        super._m_script_init(gj);
    }

    public RayWeapon(C5540aNg ang) {
        super(ang);
    }

    /* renamed from: V */
    static void m18862V() {
        serialVersionUID = (long) 1;
        _m_fieldCount = EnergyWeapon._m_fieldCount + 1;
        _m_methodCount = EnergyWeapon._m_methodCount + 8;
        int i = EnergyWeapon._m_fieldCount;
        C5663aRz[] arzArr = new C5663aRz[(i + 1)];
        C5663aRz b = C5640aRc.m17844b(RayWeapon.class, "df2d8a20fec672f8631d978ea261230b", i);
        f3919kZ = b;
        arzArr[i] = b;
        int i2 = i + 1;
        _m_fields = arzArr;
        C1634Xv.m11725a((Object[]) EnergyWeapon._m_fields, (Object[]) _m_fields);
        int i3 = EnergyWeapon._m_methodCount;
        C2491fm[] fmVarArr = new C2491fm[(i3 + 8)];
        C2491fm a = C4105zY.m41624a(RayWeapon.class, "638c1c794b3e1a741573739499443004", i3);
        jdg = a;
        fmVarArr[i3] = a;
        int i4 = i3 + 1;
        C2491fm a2 = C4105zY.m41624a(RayWeapon.class, "4e2c009bf4c0330facc1cec233dd819f", i4);
        jdh = a2;
        fmVarArr[i4] = a2;
        int i5 = i4 + 1;
        C2491fm a3 = C4105zY.m41624a(RayWeapon.class, "ded0ec0a2ba75c49a3f643b44209ba5a", i5);
        jdi = a3;
        fmVarArr[i5] = a3;
        int i6 = i5 + 1;
        C2491fm a4 = C4105zY.m41624a(RayWeapon.class, "54ace09e2fbf022f80a8ae20481286e4", i6);
        hAB = a4;
        fmVarArr[i6] = a4;
        int i7 = i6 + 1;
        C2491fm a5 = C4105zY.m41624a(RayWeapon.class, "39464e9ec3b0a4bea2ad2d959252e379", i7);
        clw = a5;
        fmVarArr[i7] = a5;
        int i8 = i7 + 1;
        C2491fm a6 = C4105zY.m41624a(RayWeapon.class, "1c1dd38ecc68158c369c4536ef7f6b06", i8);
        jdj = a6;
        fmVarArr[i8] = a6;
        int i9 = i8 + 1;
        C2491fm a7 = C4105zY.m41624a(RayWeapon.class, "a3b7e1f6ca71e035607bf3e0a145e1a1", i9);
        jdk = a7;
        fmVarArr[i9] = a7;
        int i10 = i9 + 1;
        C2491fm a8 = C4105zY.m41624a(RayWeapon.class, "52fba82d04bc016b14ad51a03d83c1ef", i10);
        bMn = a8;
        fmVarArr[i10] = a8;
        int i11 = i10 + 1;
        _m_methods = fmVarArr;
        C1634Xv.m11725a((Object[]) EnergyWeapon._m_methods, (Object[]) _m_methods);
        ___iScriptClass = aUO.m18566a(RayWeapon.class, C3262pj.class, _m_fields, _m_methods);
    }

    private float aQG() {
        return ((RayWeaponType) getType()).getLifeTime();
    }

    @C0064Am(aul = "52fba82d04bc016b14ad51a03d83c1ef", aum = 0)
    private EnergyWeaponType aqo() {
        return dDr();
    }

    /* renamed from: b */
    private void m18864b(C1003Om om, Actor cr, RayWeaponType gj, C6661arJ arj, Vec3f vec3f) {
        switch (bFf().mo6893i(jdk)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, jdk, new Object[]{om, cr, gj, arj, vec3f}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, jdk, new Object[]{om, cr, gj, arj, vec3f}));
                break;
        }
        m18863a(om, cr, gj, arj, vec3f);
    }

    private RRay dDn() {
        switch (bFf().mo6893i(jdg)) {
            case 0:
                return null;
            case 2:
                return (RRay) bFf().mo5606d(new aCE(this, jdg, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, jdg, new Object[0]));
                break;
        }
        return dDm();
    }

    /* renamed from: fm */
    private void m18865fm(float f) {
        throw new C6039afL();
    }

    /* renamed from: S */
    public void mo10S() {
        super.mo10S();
    }

    /* renamed from: T */
    public C6494any mo11T() {
        return ___iScriptClass;
    }

    /* renamed from: U */
    public C2491fm[] mo12U() {
        return _m_methods;
    }

    /* renamed from: W */
    public Object mo13W() {
        return new C3262pj(this);
    }

    /* renamed from: a */
    public Object mo14a(C0495Gr gr) {
        Object[] args = gr.getArgs();
        switch (gr.mo2417hq() - EnergyWeapon._m_methodCount) {
            case 0:
                return dDm();
            case 1:
                return dDo();
            case 2:
                return dDq();
            case 3:
                cUo();
                return null;
            case 4:
                ayw();
                return null;
            case 5:
                dDs();
                return null;
            case 6:
                m18863a((C1003Om) args[0], (Actor) args[1], (RayWeaponType) args[2], (C6661arJ) args[3], (Vec3f) args[4]);
                return null;
            case 7:
                return aqo();
            default:
                return super.mo14a(gr);
        }
    }

    /* renamed from: a */
    public void mo15a(Collection collection, C0495Gr gr) {
        super.mo15a(collection, gr);
    }

    public /* bridge */ /* synthetic */ EnergyWeaponType aqp() {
        switch (bFf().mo6893i(bMn)) {
            case 0:
                return null;
            case 2:
                return (EnergyWeaponType) bFf().mo5606d(new aCE(this, bMn, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, bMn, new Object[0]));
                break;
        }
        return aqo();
    }

    /* access modifiers changed from: protected */
    public void ayx() {
        switch (bFf().mo6893i(clw)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, clw, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, clw, new Object[0]));
                break;
        }
        ayw();
    }

    /* renamed from: c */
    public C5663aRz[] mo25c() {
        return _m_fields;
    }

    public void cUp() {
        switch (bFf().mo6893i(hAB)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, hAB, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, hAB, new Object[0]));
                break;
        }
        cUo();
    }

    public RayWeapon dDp() {
        switch (bFf().mo6893i(jdh)) {
            case 0:
                return null;
            case 2:
                return (RayWeapon) bFf().mo5606d(new aCE(this, jdh, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, jdh, new Object[0]));
                break;
        }
        return dDo();
    }

    public RayWeaponType dDr() {
        switch (bFf().mo6893i(jdi)) {
            case 0:
                return null;
            case 2:
                return (RayWeaponType) bFf().mo5606d(new aCE(this, jdi, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, jdi, new Object[0]));
                break;
        }
        return dDq();
    }

    public void trigger() {
        switch (bFf().mo6893i(jdj)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, jdj, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, jdj, new Object[0]));
                break;
        }
        dDs();
    }

    @ClientOnly
    /* renamed from: a */
    public void mo11797a(RayWeaponType gj) {
        super.mo23114a((EnergyWeaponType) gj);
    }

    @C0064Am(aul = "638c1c794b3e1a741573739499443004", aum = 0)
    private RRay dDm() {
        IEngineGraphics ale = ald().getEngineGraphics();
        if (this.jdf == null) {
            if (ale == null) {
                return null;
            }
            Scene adZ = ale.adZ();
            this.jdf = new RRay();
            ale.mo3049bV(dDp().dDr().bWb().getFile());
            SceneObject sceneObject = (SceneObject) ale.mo3047bT(dDp().dDr().bWb().getHandle());
            if (sceneObject instanceof RBillboard) {
                this.jde = (RBillboard) sceneObject;
                this.jdf.setRay(this.jde, dDp().dDr().getLifeTime());
                adZ.addChild(this.jdf);
            } else {
                System.err.println("[ERROR]: " + dDp().dDr().bWb().getHandle() + " must be a RBillboard");
            }
            if (!adZ.hasChild(this.jdf)) {
                adZ.addChild(this.jdf);
            }
        }
        return this.jdf;
    }

    @C0064Am(aul = "4e2c009bf4c0330facc1cec233dd819f", aum = 0)
    private RayWeapon dDo() {
        return this;
    }

    @C0064Am(aul = "ded0ec0a2ba75c49a3f643b44209ba5a", aum = 0)
    private RayWeaponType dDq() {
        return (RayWeaponType) super.aqp();
    }

    @C0064Am(aul = "54ace09e2fbf022f80a8ae20481286e4", aum = 0)
    private void cUo() {
        super.cUp();
        if (this.jdf != null) {
            ald().getEngineGraphics().adZ().removeChild(this.jdf);
            this.jdf.dispose();
            this.jdf = null;
        }
        if (this.jde != null) {
            this.jde.dispose();
            this.jde = null;
        }
    }

    @C0064Am(aul = "39464e9ec3b0a4bea2ad2d959252e379", aum = 0)
    private void ayw() {
        trigger();
        if (bGX()) {
            mo12911b(cUn().agv(), ayv());
        }
    }

    @C0064Am(aul = "1c1dd38ecc68158c369c4536ef7f6b06", aum = 0)
    private void dDs() {
        C0520HN aiD;
        if (!C5877acF.RUNNING_CLIENT_BOT) {
            C6661arJ cUn = dDp().cUn();
            RayWeaponType dDr = dDp().dDr();
            ArrayList arrayList = new ArrayList();
            arrayList.add(((Actor) cUn).cLd());
            Vec3f vec3f = new Vec3f();
            cUn.getOrientation().mo15204F(cUn.aip(), vec3f);
            Vec3d q = cUn.getPosition().mo9531q(cUn.getOrientation().mo15209aT(ayv()));
            float in = dDr.mo10790in();
            Actor b = mo7855al().mo965Zc().mo1892b(q, vec3f, in, (List<C6615aqP>) arrayList);
            Vec3f vec3f2 = new Vec3f(0.0f, 0.0f, -1.0f);
            Vec3f j = cUn.aip().mo23506j(vec3f2);
            j.scale(-0.5f * in);
            vec3f2.negate();
            vec3f2.scale(0.5f * in);
            vec3f2.add(j);
            cUn.getOrientation().mo15204F(vec3f2, j);
            if (bGX()) {
                Vec3d ajr = new Vec3d();
                ajr.mo9484aA(q.mo9539s((Tuple3f) j));
                dDn().trigger(ajr, vec3f, in);
                cUn().afE();
            }
            if (b != null && (aiD = b.aiD()) != null) {
                m18864b(aiD.mo2472kQ(), b, dDr, cUn, vec3f);
            }
        }
    }

    @C0064Am(aul = "a3b7e1f6ca71e035607bf3e0a145e1a1", aum = 0)
    /* renamed from: a */
    private void m18863a(C1003Om om, Actor cr, RayWeaponType gj, C6661arJ arj, Vec3f vec3f) {
        if (cr != null) {
            C5260aCm acm = new C5260aCm(dDp().mo5372il().cPK(), gj.mo13436iu(), dDp().cUl());
            acm.mo8232w(dDp().dDr());
            if (!bGX() || dDp().mo7855al().mo2998hb() != C5916acs.getSingolton().alb()) {
                cr.mo1064f(arj.agv(), acm, new Vec3f(0.0f, 0.0f, 0.0f), vec3f);
            } else {
                cr.mo621b((Actor) arj.agv(), acm, new Vec3f(0.0f, 0.0f, 0.0f), vec3f);
            }
        }
    }
}
