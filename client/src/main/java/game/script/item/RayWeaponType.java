package game.script.item;

import game.network.message.externalizable.aCE;
import logic.baa.*;
import logic.data.mbean.C6998ayl;
import logic.res.code.C5640aRc;
import logic.res.code.C5663aRz;
import logic.res.code.C6494any;
import logic.res.html.C2491fm;
import p001a.*;

import java.util.Collection;

@C0566Hp
@C5829abJ("2.1.1")
@C6485anp
@C5511aMd
/* renamed from: a.Gj */
/* compiled from: a */
public class RayWeaponType extends EnergyWeaponType implements C1616Xf {
    public static final int _m_fieldCount = 0;
    public static final C5663aRz[] _m_fields = null;
    public static final int _m_methodCount = 0;
    public static final C2491fm[] _m_methods = null;
    public static final C2491fm cXN = null;
    public static final C2491fm cXO = null;
    public static final C2491fm cXP = null;
    /* renamed from: dN */
    public static final C2491fm f644dN = null;
    /* renamed from: kZ */
    public static final C5663aRz f645kZ = null;
    public static final long serialVersionUID = 0;
    public static C6494any ___iScriptClass;
    @C0064Am(aul = "361857f4abaa3b8e2ab31956101ee679", aum = 0)
    private static float lifeTime;

    static {
        m3466V();
    }

    public RayWeaponType() {
        super((C5540aNg) null);
        super.mo10S();
    }

    public RayWeaponType(C5540aNg ang) {
        super(ang);
    }

    /* renamed from: V */
    static void m3466V() {
        serialVersionUID = (long) 1;
        _m_fieldCount = EnergyWeaponType._m_fieldCount + 1;
        _m_methodCount = EnergyWeaponType._m_methodCount + 4;
        int i = EnergyWeaponType._m_fieldCount;
        C5663aRz[] arzArr = new C5663aRz[(i + 1)];
        C5663aRz b = C5640aRc.m17844b(RayWeaponType.class, "361857f4abaa3b8e2ab31956101ee679", i);
        f645kZ = b;
        arzArr[i] = b;
        int i2 = i + 1;
        _m_fields = arzArr;
        C1634Xv.m11725a((Object[]) EnergyWeaponType._m_fields, (Object[]) _m_fields);
        int i3 = EnergyWeaponType._m_methodCount;
        C2491fm[] fmVarArr = new C2491fm[(i3 + 4)];
        C2491fm a = C4105zY.m41624a(RayWeaponType.class, "9d9e11dac727805b3b700d5315580b1e", i3);
        cXN = a;
        fmVarArr[i3] = a;
        int i4 = i3 + 1;
        C2491fm a2 = C4105zY.m41624a(RayWeaponType.class, "cba88c5a7a33b956ea1b99c28596015e", i4);
        cXO = a2;
        fmVarArr[i4] = a2;
        int i5 = i4 + 1;
        C2491fm a3 = C4105zY.m41624a(RayWeaponType.class, "331f72528c14c7320ee2032e3f403b78", i5);
        cXP = a3;
        fmVarArr[i5] = a3;
        int i6 = i5 + 1;
        C2491fm a4 = C4105zY.m41624a(RayWeaponType.class, "2f5828b36cda56165ee1ef6ab7c97ce4", i6);
        f644dN = a4;
        fmVarArr[i6] = a4;
        int i7 = i6 + 1;
        _m_methods = fmVarArr;
        C1634Xv.m11725a((Object[]) EnergyWeaponType._m_methods, (Object[]) _m_methods);
        ___iScriptClass = aUO.m18566a(RayWeaponType.class, C6998ayl.class, _m_fields, _m_methods);
    }

    private float aQG() {
        return bFf().mo5608dq().mo3211m(f645kZ);
    }

    /* renamed from: fm */
    private void m3468fm(float f) {
        bFf().mo5608dq().mo3150a(f645kZ, f);
    }

    /* renamed from: T */
    public C6494any mo11T() {
        return ___iScriptClass;
    }

    /* renamed from: U */
    public C2491fm[] mo12U() {
        return _m_methods;
    }

    /* renamed from: W */
    public Object mo13W() {
        return new C6998ayl(this);
    }

    /* renamed from: a */
    public Object mo14a(C0495Gr gr) {
        Object[] args = gr.getArgs();
        switch (gr.mo2417hq() - EnergyWeaponType._m_methodCount) {
            case 0:
                return new Float(aQH());
            case 1:
                m3469fn(((Float) args[0]).floatValue());
                return null;
            case 2:
                return aQI();
            case 3:
                return m3467aT();
            default:
                return super.mo14a(gr);
        }
    }

    /* renamed from: a */
    public void mo15a(Collection collection, C0495Gr gr) {
        super.mo15a(collection, gr);
    }

    public RayWeapon aQJ() {
        switch (bFf().mo6893i(cXP)) {
            case 0:
                return null;
            case 2:
                return (RayWeapon) bFf().mo5606d(new aCE(this, cXP, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, cXP, new Object[0]));
                break;
        }
        return aQI();
    }

    /* access modifiers changed from: protected */
    /* renamed from: aU */
    public <T extends aDJ> T mo745aU() {
        switch (bFf().mo6893i(f644dN)) {
            case 0:
                return null;
            case 2:
                return (aDJ) bFf().mo5606d(new aCE(this, f644dN, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f644dN, new Object[0]));
                break;
        }
        return m3467aT();
    }

    /* renamed from: c */
    public C5663aRz[] mo25c() {
        return _m_fields;
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Pulse Life Time")
    public float getLifeTime() {
        switch (bFf().mo6893i(cXN)) {
            case 0:
                return 0.0f;
            case 2:
                return ((Float) bFf().mo5606d(new aCE(this, cXN, new Object[0]))).floatValue();
            case 3:
                bFf().mo5606d(new aCE(this, cXN, new Object[0]));
                break;
        }
        return aQH();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Pulse Life Time")
    public void setLifeTime(float f) {
        switch (bFf().mo6893i(cXO)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, cXO, new Object[]{new Float(f)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, cXO, new Object[]{new Float(f)}));
                break;
        }
        m3469fn(f);
    }

    /* renamed from: S */
    public void mo10S() {
        super.mo10S();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Pulse Life Time")
    @C0064Am(aul = "9d9e11dac727805b3b700d5315580b1e", aum = 0)
    private float aQH() {
        return aQG();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Pulse Life Time")
    @C0064Am(aul = "cba88c5a7a33b956ea1b99c28596015e", aum = 0)
    /* renamed from: fn */
    private void m3469fn(float f) {
        m3468fm(f);
    }

    @C0064Am(aul = "331f72528c14c7320ee2032e3f403b78", aum = 0)
    private RayWeapon aQI() {
        return (RayWeapon) mo745aU();
    }

    @C0064Am(aul = "2f5828b36cda56165ee1ef6ab7c97ce4", aum = 0)
    /* renamed from: aT */
    private <T extends aDJ> T m3467aT() {
        T t = (RayWeapon) bFf().mo6865M(RayWeapon.class);
        t.mo11797a(this);
        return t;
    }
}
