package game.script.item;

import game.network.message.externalizable.aCE;
import game.script.progression.ProgressionAbility;
import game.script.resource.Asset;
import logic.baa.*;
import logic.data.mbean.aST;
import logic.res.code.C5640aRc;
import logic.res.code.C5663aRz;
import logic.res.code.C6494any;
import logic.res.html.C2491fm;
import p001a.*;
import taikodom.infra.script.I18NString;

import java.util.Collection;

@C0566Hp
@C5511aMd
@C6485anp
/* renamed from: a.jC */
/* compiled from: a */
public abstract class ItemType extends BaseItemType implements C1616Xf, aWv {

    /* renamed from: NY */
    public static final C5663aRz f8285NY = null;

    /* renamed from: Ob */
    public static final C2491fm f8286Ob = null;

    /* renamed from: Oc */
    public static final C2491fm f8287Oc = null;

    /* renamed from: QA */
    public static final C5663aRz f8288QA = null;

    /* renamed from: QE */
    public static final C2491fm f8289QE = null;

    /* renamed from: QF */
    public static final C2491fm f8290QF = null;
    public static final int _m_fieldCount = 0;
    public static final C5663aRz[] _m_fields = null;
    public static final int _m_methodCount = 0;
    public static final C2491fm[] _m_methods = null;
    public static final C5663aRz amV = null;
    public static final C5663aRz amX = null;
    public static final C5663aRz amZ = null;
    public static final C2491fm anA = null;
    public static final C2491fm anB = null;
    public static final C2491fm anC = null;
    public static final C2491fm anD = null;
    public static final C2491fm anE = null;
    public static final C2491fm anF = null;
    public static final C2491fm anG = null;
    public static final C2491fm anH = null;
    public static final C2491fm anI = null;
    public static final C2491fm anJ = null;
    public static final C2491fm anK = null;
    public static final C2491fm anL = null;
    public static final C2491fm anM = null;
    public static final C2491fm anN = null;
    public static final C2491fm anO = null;
    public static final C2491fm anP = null;
    public static final C5663aRz anb = null;
    public static final C5663aRz and = null;
    public static final C5663aRz anf = null;
    public static final C5663aRz anh = null;
    public static final C5663aRz anj = null;
    public static final C5663aRz anl = null;
    public static final C5663aRz ann = null;
    public static final C5663aRz anp = null;
    public static final C5663aRz anr = null;
    public static final C2491fm ans = null;
    public static final C2491fm ant = null;
    public static final C2491fm anu = null;
    public static final C2491fm anv = null;
    public static final C2491fm anw = null;
    public static final C2491fm anx = null;
    public static final C2491fm any = null;
    public static final C2491fm anz = null;
    public static final long serialVersionUID = 0;
    /* renamed from: zQ */
    public static final C5663aRz f8294zQ = null;
    /* renamed from: zT */
    public static final C2491fm f8295zT = null;
    /* renamed from: zU */
    public static final C2491fm f8296zU = null;
    public static C6494any ___iScriptClass;
    @C0064Am(aul = "bcad2e885b4f68134eddc2082200e89f", aum = 2)
    private static long amU;
    @C0064Am(aul = "7d9208fa0c0113dc9d261c58e126a60d", aum = 3)
    private static float amW;
    @C0064Am(aul = "b2990b6f6c11b6894a25cc1fda9840e6", aum = 4)
    private static ItemTypeCategory amY;
    @C0064Am(aul = "c76a92c0d5bedad6da3a97477d34b280", aum = 6)
    private static ProgressionAbility ana;
    @C0064Am(aul = "e099eeb7ae68d88d7438a15f4e3e5f42", aum = 7)
    private static long anc;
    @C0064Am(aul = "cda504a2095dbb23715c088c7551c026", aum = 8)
    private static long ane;
    @C0064Am(aul = "e4a11f2e33dcccd875f620bd542265be", aum = 9)
    private static long ang;
    @C0064Am(aul = "06ed10ec7d09d542e55b21672bf6b58d", aum = 10)
    private static int ani;
    @C0064Am(aul = "a292518e57111128e4499b2b2268a921", aum = 11)
    private static int ank;
    @C0064Am(aul = "10398010a94e368f32b5ebb52bfd2ed5", aum = 12)
    private static int anm;
    @C0064Am(aul = "ef5b4c1d7a3ce5364911f8fd60b4c4b1", aum = 13)
    private static int ano;
    @C0064Am(aul = "b28c26cd18ed6696cc8ae1f9c7cff1f7", aum = 14)
    private static int anq;
    @C0064Am(aul = "63541c716fd844ab4cbedab3ca98fd98", aum = 5)

    /* renamed from: jn */
    private static Asset f8291jn;
    @C0064Am(aul = "4fc136745aa2d2d223ddaf097123dd95", aum = 1)

    /* renamed from: nh */
    private static I18NString f8292nh;
    @C0064Am(aul = "7917b905a25f932f49a2312027908d37", aum = 0)

    /* renamed from: zP */
    private static I18NString f8293zP;

    static {
        m33780V();
    }

    public ItemType() {
        super((C5540aNg) null);
        super.mo10S();
    }

    public ItemType(C5540aNg ang2) {
        super(ang2);
    }

    /* renamed from: V */
    static void m33780V() {
        serialVersionUID = (long) 1;
        _m_fieldCount = BaseItemType._m_fieldCount + 15;
        _m_methodCount = BaseItemType._m_methodCount + 30;
        int i = BaseItemType._m_fieldCount;
        C5663aRz[] arzArr = new C5663aRz[(i + 15)];
        C5663aRz b = C5640aRc.m17844b(ItemType.class, "7917b905a25f932f49a2312027908d37", i);
        f8294zQ = b;
        arzArr[i] = b;
        int i2 = i + 1;
        C5663aRz b2 = C5640aRc.m17844b(ItemType.class, "4fc136745aa2d2d223ddaf097123dd95", i2);
        f8288QA = b2;
        arzArr[i2] = b2;
        int i3 = i2 + 1;
        C5663aRz b3 = C5640aRc.m17844b(ItemType.class, "bcad2e885b4f68134eddc2082200e89f", i3);
        amV = b3;
        arzArr[i3] = b3;
        int i4 = i3 + 1;
        C5663aRz b4 = C5640aRc.m17844b(ItemType.class, "7d9208fa0c0113dc9d261c58e126a60d", i4);
        amX = b4;
        arzArr[i4] = b4;
        int i5 = i4 + 1;
        C5663aRz b5 = C5640aRc.m17844b(ItemType.class, "b2990b6f6c11b6894a25cc1fda9840e6", i5);
        amZ = b5;
        arzArr[i5] = b5;
        int i6 = i5 + 1;
        C5663aRz b6 = C5640aRc.m17844b(ItemType.class, "63541c716fd844ab4cbedab3ca98fd98", i6);
        f8285NY = b6;
        arzArr[i6] = b6;
        int i7 = i6 + 1;
        C5663aRz b7 = C5640aRc.m17844b(ItemType.class, "c76a92c0d5bedad6da3a97477d34b280", i7);
        anb = b7;
        arzArr[i7] = b7;
        int i8 = i7 + 1;
        C5663aRz b8 = C5640aRc.m17844b(ItemType.class, "e099eeb7ae68d88d7438a15f4e3e5f42", i8);
        and = b8;
        arzArr[i8] = b8;
        int i9 = i8 + 1;
        C5663aRz b9 = C5640aRc.m17844b(ItemType.class, "cda504a2095dbb23715c088c7551c026", i9);
        anf = b9;
        arzArr[i9] = b9;
        int i10 = i9 + 1;
        C5663aRz b10 = C5640aRc.m17844b(ItemType.class, "e4a11f2e33dcccd875f620bd542265be", i10);
        anh = b10;
        arzArr[i10] = b10;
        int i11 = i10 + 1;
        C5663aRz b11 = C5640aRc.m17844b(ItemType.class, "06ed10ec7d09d542e55b21672bf6b58d", i11);
        anj = b11;
        arzArr[i11] = b11;
        int i12 = i11 + 1;
        C5663aRz b12 = C5640aRc.m17844b(ItemType.class, "a292518e57111128e4499b2b2268a921", i12);
        anl = b12;
        arzArr[i12] = b12;
        int i13 = i12 + 1;
        C5663aRz b13 = C5640aRc.m17844b(ItemType.class, "10398010a94e368f32b5ebb52bfd2ed5", i13);
        ann = b13;
        arzArr[i13] = b13;
        int i14 = i13 + 1;
        C5663aRz b14 = C5640aRc.m17844b(ItemType.class, "ef5b4c1d7a3ce5364911f8fd60b4c4b1", i14);
        anp = b14;
        arzArr[i14] = b14;
        int i15 = i14 + 1;
        C5663aRz b15 = C5640aRc.m17844b(ItemType.class, "b28c26cd18ed6696cc8ae1f9c7cff1f7", i15);
        anr = b15;
        arzArr[i15] = b15;
        int i16 = i15 + 1;
        _m_fields = arzArr;
        C1634Xv.m11725a((Object[]) BaseItemType._m_fields, (Object[]) _m_fields);
        int i17 = BaseItemType._m_methodCount;
        C2491fm[] fmVarArr = new C2491fm[(i17 + 30)];
        C2491fm a = C4105zY.m41624a(ItemType.class, "05efeb23e2caade13af33b1c5f8a4076", i17);
        f8295zT = a;
        fmVarArr[i17] = a;
        int i18 = i17 + 1;
        C2491fm a2 = C4105zY.m41624a(ItemType.class, "9fb68b6a995cb64344979f2cba78c83f", i18);
        f8296zU = a2;
        fmVarArr[i18] = a2;
        int i19 = i18 + 1;
        C2491fm a3 = C4105zY.m41624a(ItemType.class, "e190174d26c77dd6215a5044150b7ff2", i19);
        f8289QE = a3;
        fmVarArr[i19] = a3;
        int i20 = i19 + 1;
        C2491fm a4 = C4105zY.m41624a(ItemType.class, "0b0f191a47ec3aa792a20d60b9a3a777", i20);
        f8290QF = a4;
        fmVarArr[i20] = a4;
        int i21 = i20 + 1;
        C2491fm a5 = C4105zY.m41624a(ItemType.class, "659b6241db4af6148545920d97a4ff43", i21);
        ans = a5;
        fmVarArr[i21] = a5;
        int i22 = i21 + 1;
        C2491fm a6 = C4105zY.m41624a(ItemType.class, "f7a79e789fb4853818491503384a85d9", i22);
        ant = a6;
        fmVarArr[i22] = a6;
        int i23 = i22 + 1;
        C2491fm a7 = C4105zY.m41624a(ItemType.class, "785f79624db68a95a0453ec0130637ff", i23);
        anu = a7;
        fmVarArr[i23] = a7;
        int i24 = i23 + 1;
        C2491fm a8 = C4105zY.m41624a(ItemType.class, "cd7fdc381f6087e51752672072510857", i24);
        anv = a8;
        fmVarArr[i24] = a8;
        int i25 = i24 + 1;
        C2491fm a9 = C4105zY.m41624a(ItemType.class, "3880c46bc94f714dca38be55fb1dd398", i25);
        anw = a9;
        fmVarArr[i25] = a9;
        int i26 = i25 + 1;
        C2491fm a10 = C4105zY.m41624a(ItemType.class, "40987bbb1d97757e0d8b69e232e7d9b7", i26);
        anx = a10;
        fmVarArr[i26] = a10;
        int i27 = i26 + 1;
        C2491fm a11 = C4105zY.m41624a(ItemType.class, "973d4e78c8ba4d2ce6684c3dd680a699", i27);
        f8286Ob = a11;
        fmVarArr[i27] = a11;
        int i28 = i27 + 1;
        C2491fm a12 = C4105zY.m41624a(ItemType.class, "0b7ab8a2338660d86b9b546700a11810", i28);
        f8287Oc = a12;
        fmVarArr[i28] = a12;
        int i29 = i28 + 1;
        C2491fm a13 = C4105zY.m41624a(ItemType.class, "aa4cde73429e615379c8e8835653c6be", i29);
        any = a13;
        fmVarArr[i29] = a13;
        int i30 = i29 + 1;
        C2491fm a14 = C4105zY.m41624a(ItemType.class, "91413053353920bdda5ce7420b2cd120", i30);
        anz = a14;
        fmVarArr[i30] = a14;
        int i31 = i30 + 1;
        C2491fm a15 = C4105zY.m41624a(ItemType.class, "bf77c7754e4a22731fbc2bbf404cd8a1", i31);
        anA = a15;
        fmVarArr[i31] = a15;
        int i32 = i31 + 1;
        C2491fm a16 = C4105zY.m41624a(ItemType.class, "737246d1b58a1b58cb6ca9d3c0a2143f", i32);
        anB = a16;
        fmVarArr[i32] = a16;
        int i33 = i32 + 1;
        C2491fm a17 = C4105zY.m41624a(ItemType.class, "5a720a3e32aa17f0f25f17f611f6166e", i33);
        anC = a17;
        fmVarArr[i33] = a17;
        int i34 = i33 + 1;
        C2491fm a18 = C4105zY.m41624a(ItemType.class, "8a8e399a85e1a51090a535a8fdfa3456", i34);
        anD = a18;
        fmVarArr[i34] = a18;
        int i35 = i34 + 1;
        C2491fm a19 = C4105zY.m41624a(ItemType.class, "25b2c30ddbd3b42701935f9414362807", i35);
        anE = a19;
        fmVarArr[i35] = a19;
        int i36 = i35 + 1;
        C2491fm a20 = C4105zY.m41624a(ItemType.class, "c089c895edf41213bf3929db6c72bbfd", i36);
        anF = a20;
        fmVarArr[i36] = a20;
        int i37 = i36 + 1;
        C2491fm a21 = C4105zY.m41624a(ItemType.class, "3f7b9ba9ff2910206000cf93952786a0", i37);
        anG = a21;
        fmVarArr[i37] = a21;
        int i38 = i37 + 1;
        C2491fm a22 = C4105zY.m41624a(ItemType.class, "76ab63d8cf1aaef19b683833c98cf00c", i38);
        anH = a22;
        fmVarArr[i38] = a22;
        int i39 = i38 + 1;
        C2491fm a23 = C4105zY.m41624a(ItemType.class, "856d0901b97d8e8a4fda465d67177bf1", i39);
        anI = a23;
        fmVarArr[i39] = a23;
        int i40 = i39 + 1;
        C2491fm a24 = C4105zY.m41624a(ItemType.class, "0fda47a2fe21bb87704d2a8f4c7bd394", i40);
        anJ = a24;
        fmVarArr[i40] = a24;
        int i41 = i40 + 1;
        C2491fm a25 = C4105zY.m41624a(ItemType.class, "d435bff1826ee64aad1f39a9fcd764e1", i41);
        anK = a25;
        fmVarArr[i41] = a25;
        int i42 = i41 + 1;
        C2491fm a26 = C4105zY.m41624a(ItemType.class, "5562f2b309ad71a3505eb1d17a46f673", i42);
        anL = a26;
        fmVarArr[i42] = a26;
        int i43 = i42 + 1;
        C2491fm a27 = C4105zY.m41624a(ItemType.class, "f713207570068eb6f774694294ce9996", i43);
        anM = a27;
        fmVarArr[i43] = a27;
        int i44 = i43 + 1;
        C2491fm a28 = C4105zY.m41624a(ItemType.class, "a08f2da822978d9af4aed7393bbdc0a1", i44);
        anN = a28;
        fmVarArr[i44] = a28;
        int i45 = i44 + 1;
        C2491fm a29 = C4105zY.m41624a(ItemType.class, "cf90f56419b8e89c204f20616971b4ec", i45);
        anO = a29;
        fmVarArr[i45] = a29;
        int i46 = i45 + 1;
        C2491fm a30 = C4105zY.m41624a(ItemType.class, "77817a4924d76956f661099f01f9649c", i46);
        anP = a30;
        fmVarArr[i46] = a30;
        int i47 = i46 + 1;
        _m_methods = fmVarArr;
        C1634Xv.m11725a((Object[]) BaseItemType._m_methods, (Object[]) _m_methods);
        ___iScriptClass = aUO.m18566a(ItemType.class, aST.class, _m_fields, _m_methods);
    }

    /* renamed from: Hl */
    private long m33766Hl() {
        return bFf().mo5608dq().mo3213o(amV);
    }

    /* renamed from: Hm */
    private float m33767Hm() {
        return bFf().mo5608dq().mo3211m(amX);
    }

    /* renamed from: Hn */
    private ItemTypeCategory m33768Hn() {
        return (ItemTypeCategory) bFf().mo5608dq().mo3214p(amZ);
    }

    /* renamed from: Ho */
    private ProgressionAbility m33769Ho() {
        return (ProgressionAbility) bFf().mo5608dq().mo3214p(anb);
    }

    /* renamed from: Hp */
    private long m33770Hp() {
        return bFf().mo5608dq().mo3213o(and);
    }

    /* renamed from: Hq */
    private long m33771Hq() {
        return bFf().mo5608dq().mo3213o(anf);
    }

    /* renamed from: Hr */
    private long m33772Hr() {
        return bFf().mo5608dq().mo3213o(anh);
    }

    /* renamed from: Hs */
    private int m33773Hs() {
        return bFf().mo5608dq().mo3212n(anj);
    }

    /* renamed from: Ht */
    private int m33774Ht() {
        return bFf().mo5608dq().mo3212n(anl);
    }

    /* renamed from: Hu */
    private int m33775Hu() {
        return bFf().mo5608dq().mo3212n(ann);
    }

    /* renamed from: Hv */
    private int m33776Hv() {
        return bFf().mo5608dq().mo3212n(anp);
    }

    /* renamed from: Hw */
    private int m33777Hw() {
        return bFf().mo5608dq().mo3212n(anr);
    }

    /* renamed from: aP */
    private void m33781aP(float f) {
        bFf().mo5608dq().mo3150a(amX, f);
    }

    /* renamed from: b */
    private void m33783b(ProgressionAbility lk) {
        bFf().mo5608dq().mo3197f(anb, lk);
    }

    /* renamed from: bN */
    private void m33784bN(long j) {
        bFf().mo5608dq().mo3184b(amV, j);
    }

    /* renamed from: bO */
    private void m33785bO(long j) {
        bFf().mo5608dq().mo3184b(and, j);
    }

    /* renamed from: bP */
    private void m33786bP(long j) {
        bFf().mo5608dq().mo3184b(anf, j);
    }

    /* renamed from: bQ */
    private void m33787bQ(long j) {
        bFf().mo5608dq().mo3184b(anh, j);
    }

    /* renamed from: br */
    private void m33792br(I18NString i18NString) {
        bFf().mo5608dq().mo3197f(f8288QA, i18NString);
    }

    /* renamed from: c */
    private void m33795c(ItemTypeCategory aai) {
        bFf().mo5608dq().mo3197f(amZ, aai);
    }

    /* renamed from: ci */
    private void m33796ci(int i) {
        bFf().mo5608dq().mo3183b(anj, i);
    }

    /* renamed from: cj */
    private void m33797cj(int i) {
        bFf().mo5608dq().mo3183b(anl, i);
    }

    /* renamed from: ck */
    private void m33798ck(int i) {
        bFf().mo5608dq().mo3183b(ann, i);
    }

    /* renamed from: cl */
    private void m33799cl(int i) {
        bFf().mo5608dq().mo3183b(anp, i);
    }

    /* renamed from: cm */
    private void m33800cm(int i) {
        bFf().mo5608dq().mo3183b(anr, i);
    }

    /* renamed from: d */
    private void m33807d(I18NString i18NString) {
        bFf().mo5608dq().mo3197f(f8294zQ, i18NString);
    }

    /* renamed from: kb */
    private I18NString m33809kb() {
        return (I18NString) bFf().mo5608dq().mo3214p(f8294zQ);
    }

    /* renamed from: sG */
    private Asset m33811sG() {
        return (Asset) bFf().mo5608dq().mo3214p(f8285NY);
    }

    /* renamed from: t */
    private void m33813t(Asset tCVar) {
        bFf().mo5608dq().mo3197f(f8285NY, tCVar);
    }

    /* renamed from: vT */
    private I18NString m33815vT() {
        return (I18NString) bFf().mo5608dq().mo3214p(f8288QA);
    }

    @aFW("UnitVolume")
    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Unit Volume")
    /* renamed from: HA */
    public float mo19865HA() {
        switch (bFf().mo6893i(anu)) {
            case 0:
                return 0.0f;
            case 2:
                return ((Float) bFf().mo5606d(new aCE(this, anu, new Object[0]))).floatValue();
            case 3:
                bFf().mo5606d(new aCE(this, anu, new Object[0]));
                break;
        }
        return m33779Hz();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Item Type Category")
    /* renamed from: HC */
    public ItemTypeCategory mo19866HC() {
        switch (bFf().mo6893i(anw)) {
            case 0:
                return null;
            case 2:
                return (ItemTypeCategory) bFf().mo5606d(new aCE(this, anw, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, anw, new Object[0]));
                break;
        }
        return m33756HB();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Requirement")
    /* renamed from: HE */
    public ProgressionAbility mo19867HE() {
        switch (bFf().mo6893i(any)) {
            case 0:
                return null;
            case 2:
                return (ProgressionAbility) bFf().mo5606d(new aCE(this, any, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, any, new Object[0]));
                break;
        }
        return m33757HD();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Economy Avg Price")
    /* renamed from: HG */
    public long mo19868HG() {
        switch (bFf().mo6893i(anA)) {
            case 0:
                return 0;
            case 2:
                return ((Long) bFf().mo5606d(new aCE(this, anA, new Object[0]))).longValue();
            case 3:
                bFf().mo5606d(new aCE(this, anA, new Object[0]));
                break;
        }
        return m33758HF();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Economy Avg Price Min")
    /* renamed from: HI */
    public long mo19869HI() {
        switch (bFf().mo6893i(anC)) {
            case 0:
                return 0;
            case 2:
                return ((Long) bFf().mo5606d(new aCE(this, anC, new Object[0]))).longValue();
            case 3:
                bFf().mo5606d(new aCE(this, anC, new Object[0]));
                break;
        }
        return m33759HH();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Economy Avg Price Max")
    /* renamed from: HK */
    public long mo19870HK() {
        switch (bFf().mo6893i(anE)) {
            case 0:
                return 0;
            case 2:
                return ((Long) bFf().mo5606d(new aCE(this, anE, new Object[0]))).longValue();
            case 3:
                bFf().mo5606d(new aCE(this, anE, new Object[0]));
                break;
        }
        return m33760HJ();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Economy Max Trading Units")
    /* renamed from: HM */
    public int mo19871HM() {
        switch (bFf().mo6893i(anG)) {
            case 0:
                return 0;
            case 2:
                return ((Integer) bFf().mo5606d(new aCE(this, anG, new Object[0]))).intValue();
            case 3:
                bFf().mo5606d(new aCE(this, anG, new Object[0]));
                break;
        }
        return m33761HL();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Economy Min Lot Cheap Sell")
    /* renamed from: HO */
    public int mo19872HO() {
        switch (bFf().mo6893i(anI)) {
            case 0:
                return 0;
            case 2:
                return ((Integer) bFf().mo5606d(new aCE(this, anI, new Object[0]))).intValue();
            case 3:
                bFf().mo5606d(new aCE(this, anI, new Object[0]));
                break;
        }
        return m33762HN();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Economy Min Lot Expensive Sell")
    /* renamed from: HQ */
    public int mo19873HQ() {
        switch (bFf().mo6893i(anK)) {
            case 0:
                return 0;
            case 2:
                return ((Integer) bFf().mo5606d(new aCE(this, anK, new Object[0]))).intValue();
            case 3:
                bFf().mo5606d(new aCE(this, anK, new Object[0]));
                break;
        }
        return m33763HP();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Economy Min Lot Active Buy")
    /* renamed from: HS */
    public int mo19874HS() {
        switch (bFf().mo6893i(anM)) {
            case 0:
                return 0;
            case 2:
                return ((Integer) bFf().mo5606d(new aCE(this, anM, new Object[0]))).intValue();
            case 3:
                bFf().mo5606d(new aCE(this, anM, new Object[0]));
                break;
        }
        return m33764HR();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Economy Min Lot Any Buy")
    /* renamed from: HU */
    public int mo19875HU() {
        switch (bFf().mo6893i(anO)) {
            case 0:
                return 0;
            case 2:
                return ((Integer) bFf().mo5606d(new aCE(this, anO, new Object[0]))).intValue();
            case 3:
                bFf().mo5606d(new aCE(this, anO, new Object[0]));
                break;
        }
        return m33765HT();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Base Unit Price")
    /* renamed from: Hy */
    public long mo19876Hy() {
        switch (bFf().mo6893i(ans)) {
            case 0:
                return 0;
            case 2:
                return ((Long) bFf().mo5606d(new aCE(this, ans, new Object[0]))).longValue();
            case 3:
                bFf().mo5606d(new aCE(this, ans, new Object[0]));
                break;
        }
        return m33778Hx();
    }

    /* renamed from: T */
    public C6494any mo11T() {
        return ___iScriptClass;
    }

    /* renamed from: U */
    public C2491fm[] mo12U() {
        return _m_methods;
    }

    /* renamed from: W */
    public Object mo13W() {
        return new aST(this);
    }

    /* renamed from: a */
    public Object mo14a(C0495Gr gr) {
        Object[] args = gr.getArgs();
        switch (gr.mo2417hq() - BaseItemType._m_methodCount) {
            case 0:
                return m33810kd();
            case 1:
                m33808e((I18NString) args[0]);
                return null;
            case 2:
                return m33816vV();
            case 3:
                m33793bu((I18NString) args[0]);
                return null;
            case 4:
                return new Long(m33778Hx());
            case 5:
                m33788bR(((Long) args[0]).longValue());
                return null;
            case 6:
                return new Float(m33779Hz());
            case 7:
                m33782aQ(((Float) args[0]).floatValue());
                return null;
            case 8:
                return m33756HB();
            case 9:
                m33806d((ItemTypeCategory) args[0]);
                return null;
            case 10:
                return m33812sJ();
            case 11:
                m33814u((Asset) args[0]);
                return null;
            case 12:
                return m33757HD();
            case 13:
                m33794c((ProgressionAbility) args[0]);
                return null;
            case 14:
                return new Long(m33758HF());
            case 15:
                m33789bT(((Long) args[0]).longValue());
                return null;
            case 16:
                return new Long(m33759HH());
            case 17:
                m33790bV(((Long) args[0]).longValue());
                return null;
            case 18:
                return new Long(m33760HJ());
            case 19:
                m33791bX(((Long) args[0]).longValue());
                return null;
            case 20:
                return new Integer(m33761HL());
            case 21:
                m33801cn(((Integer) args[0]).intValue());
                return null;
            case 22:
                return new Integer(m33762HN());
            case 23:
                m33802cp(((Integer) args[0]).intValue());
                return null;
            case 24:
                return new Integer(m33763HP());
            case 25:
                m33803cr(((Integer) args[0]).intValue());
                return null;
            case 26:
                return new Integer(m33764HR());
            case 27:
                m33804ct(((Integer) args[0]).intValue());
                return null;
            case 28:
                return new Integer(m33765HT());
            case 29:
                m33805cv(((Integer) args[0]).intValue());
                return null;
            default:
                return super.mo14a(gr);
        }
    }

    /* renamed from: a */
    public void mo15a(Collection collection, C0495Gr gr) {
        super.mo15a(collection, gr);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Unit Volume")
    /* renamed from: aR */
    public void mo19877aR(float f) {
        switch (bFf().mo6893i(anv)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, anv, new Object[]{new Float(f)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, anv, new Object[]{new Float(f)}));
                break;
        }
        m33782aQ(f);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Base Unit Price")
    /* renamed from: bS */
    public void mo19878bS(long j) {
        switch (bFf().mo6893i(ant)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, ant, new Object[]{new Long(j)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, ant, new Object[]{new Long(j)}));
                break;
        }
        m33788bR(j);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Economy Avg Price")
    /* renamed from: bU */
    public void mo19879bU(long j) {
        switch (bFf().mo6893i(anB)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, anB, new Object[]{new Long(j)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, anB, new Object[]{new Long(j)}));
                break;
        }
        m33789bT(j);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Economy Avg Price Min")
    /* renamed from: bW */
    public void mo19880bW(long j) {
        switch (bFf().mo6893i(anD)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, anD, new Object[]{new Long(j)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, anD, new Object[]{new Long(j)}));
                break;
        }
        m33790bV(j);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Economy Avg Price Max")
    /* renamed from: bY */
    public void mo19881bY(long j) {
        switch (bFf().mo6893i(anF)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, anF, new Object[]{new Long(j)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, anF, new Object[]{new Long(j)}));
                break;
        }
        m33791bX(j);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Description")
    /* renamed from: bv */
    public void mo19882bv(I18NString i18NString) {
        switch (bFf().mo6893i(f8290QF)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f8290QF, new Object[]{i18NString}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f8290QF, new Object[]{i18NString}));
                break;
        }
        m33793bu(i18NString);
    }

    /* renamed from: c */
    public C5663aRz[] mo25c() {
        return _m_fields;
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Economy Max Trading Units")
    /* renamed from: co */
    public void mo19883co(int i) {
        switch (bFf().mo6893i(anH)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, anH, new Object[]{new Integer(i)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, anH, new Object[]{new Integer(i)}));
                break;
        }
        m33801cn(i);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Economy Min Lot Cheap Sell")
    /* renamed from: cq */
    public void mo19884cq(int i) {
        switch (bFf().mo6893i(anJ)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, anJ, new Object[]{new Integer(i)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, anJ, new Object[]{new Integer(i)}));
                break;
        }
        m33802cp(i);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Economy Min Lot Expensive Sell")
    /* renamed from: cs */
    public void mo19885cs(int i) {
        switch (bFf().mo6893i(anL)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, anL, new Object[]{new Integer(i)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, anL, new Object[]{new Integer(i)}));
                break;
        }
        m33803cr(i);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Economy Min Lot Active Buy")
    /* renamed from: cu */
    public void mo19886cu(int i) {
        switch (bFf().mo6893i(anN)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, anN, new Object[]{new Integer(i)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, anN, new Object[]{new Integer(i)}));
                break;
        }
        m33804ct(i);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Economy Min Lot Any Buy")
    /* renamed from: cw */
    public void mo19887cw(int i) {
        switch (bFf().mo6893i(anP)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, anP, new Object[]{new Integer(i)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, anP, new Object[]{new Integer(i)}));
                break;
        }
        m33805cv(i);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Requirement")
    /* renamed from: d */
    public void mo19888d(ProgressionAbility lk) {
        switch (bFf().mo6893i(anz)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, anz, new Object[]{lk}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, anz, new Object[]{lk}));
                break;
        }
        m33794c(lk);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Item Type Category")
    /* renamed from: e */
    public void mo19889e(ItemTypeCategory aai) {
        switch (bFf().mo6893i(anx)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, anx, new Object[]{aai}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, anx, new Object[]{aai}));
                break;
        }
        m33806d(aai);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Name")
    /* renamed from: f */
    public void mo19890f(I18NString i18NString) {
        switch (bFf().mo6893i(f8296zU)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f8296zU, new Object[]{i18NString}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f8296zU, new Object[]{i18NString}));
                break;
        }
        m33808e(i18NString);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Name")
    /* renamed from: ke */
    public I18NString mo19891ke() {
        switch (bFf().mo6893i(f8295zT)) {
            case 0:
                return null;
            case 2:
                return (I18NString) bFf().mo5606d(new aCE(this, f8295zT, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f8295zT, new Object[0]));
                break;
        }
        return m33810kd();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Icon")
    /* renamed from: sK */
    public Asset mo12100sK() {
        switch (bFf().mo6893i(f8286Ob)) {
            case 0:
                return null;
            case 2:
                return (Asset) bFf().mo5606d(new aCE(this, f8286Ob, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f8286Ob, new Object[0]));
                break;
        }
        return m33812sJ();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Icon")
    /* renamed from: v */
    public void mo19892v(Asset tCVar) {
        switch (bFf().mo6893i(f8287Oc)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f8287Oc, new Object[]{tCVar}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f8287Oc, new Object[]{tCVar}));
                break;
        }
        m33814u(tCVar);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Description")
    /* renamed from: vW */
    public I18NString mo19893vW() {
        switch (bFf().mo6893i(f8289QE)) {
            case 0:
                return null;
            case 2:
                return (I18NString) bFf().mo5606d(new aCE(this, f8289QE, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f8289QE, new Object[0]));
                break;
        }
        return m33816vV();
    }

    /* renamed from: S */
    public void mo10S() {
        super.mo10S();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Name")
    @C0064Am(aul = "05efeb23e2caade13af33b1c5f8a4076", aum = 0)
    /* renamed from: kd */
    private I18NString m33810kd() {
        return m33809kb();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Name")
    @C0064Am(aul = "9fb68b6a995cb64344979f2cba78c83f", aum = 0)
    /* renamed from: e */
    private void m33808e(I18NString i18NString) {
        m33807d(i18NString);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Description")
    @C0064Am(aul = "e190174d26c77dd6215a5044150b7ff2", aum = 0)
    /* renamed from: vV */
    private I18NString m33816vV() {
        return m33815vT();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Description")
    @C0064Am(aul = "0b0f191a47ec3aa792a20d60b9a3a777", aum = 0)
    /* renamed from: bu */
    private void m33793bu(I18NString i18NString) {
        m33792br(i18NString);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Base Unit Price")
    @C0064Am(aul = "659b6241db4af6148545920d97a4ff43", aum = 0)
    /* renamed from: Hx */
    private long m33778Hx() {
        return m33766Hl();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Base Unit Price")
    @C0064Am(aul = "f7a79e789fb4853818491503384a85d9", aum = 0)
    /* renamed from: bR */
    private void m33788bR(long j) {
        m33784bN(j);
    }

    @aFW("UnitVolume")
    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Unit Volume")
    @C0064Am(aul = "785f79624db68a95a0453ec0130637ff", aum = 0)
    /* renamed from: Hz */
    private float m33779Hz() {
        return m33767Hm();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Unit Volume")
    @C0064Am(aul = "cd7fdc381f6087e51752672072510857", aum = 0)
    /* renamed from: aQ */
    private void m33782aQ(float f) {
        m33781aP(f);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Item Type Category")
    @C0064Am(aul = "3880c46bc94f714dca38be55fb1dd398", aum = 0)
    /* renamed from: HB */
    private ItemTypeCategory m33756HB() {
        return m33768Hn();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Item Type Category")
    @C0064Am(aul = "40987bbb1d97757e0d8b69e232e7d9b7", aum = 0)
    /* renamed from: d */
    private void m33806d(ItemTypeCategory aai) {
        m33795c(aai);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Icon")
    @C0064Am(aul = "973d4e78c8ba4d2ce6684c3dd680a699", aum = 0)
    /* renamed from: sJ */
    private Asset m33812sJ() {
        return m33811sG();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Icon")
    @C0064Am(aul = "0b7ab8a2338660d86b9b546700a11810", aum = 0)
    /* renamed from: u */
    private void m33814u(Asset tCVar) {
        m33813t(tCVar);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Requirement")
    @C0064Am(aul = "aa4cde73429e615379c8e8835653c6be", aum = 0)
    /* renamed from: HD */
    private ProgressionAbility m33757HD() {
        return m33769Ho();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Requirement")
    @C0064Am(aul = "91413053353920bdda5ce7420b2cd120", aum = 0)
    /* renamed from: c */
    private void m33794c(ProgressionAbility lk) {
        m33783b(lk);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Economy Avg Price")
    @C0064Am(aul = "bf77c7754e4a22731fbc2bbf404cd8a1", aum = 0)
    /* renamed from: HF */
    private long m33758HF() {
        return m33770Hp();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Economy Avg Price")
    @C0064Am(aul = "737246d1b58a1b58cb6ca9d3c0a2143f", aum = 0)
    /* renamed from: bT */
    private void m33789bT(long j) {
        m33785bO(j);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Economy Avg Price Min")
    @C0064Am(aul = "5a720a3e32aa17f0f25f17f611f6166e", aum = 0)
    /* renamed from: HH */
    private long m33759HH() {
        return m33771Hq();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Economy Avg Price Min")
    @C0064Am(aul = "8a8e399a85e1a51090a535a8fdfa3456", aum = 0)
    /* renamed from: bV */
    private void m33790bV(long j) {
        m33786bP(j);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Economy Avg Price Max")
    @C0064Am(aul = "25b2c30ddbd3b42701935f9414362807", aum = 0)
    /* renamed from: HJ */
    private long m33760HJ() {
        return m33772Hr();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Economy Avg Price Max")
    @C0064Am(aul = "c089c895edf41213bf3929db6c72bbfd", aum = 0)
    /* renamed from: bX */
    private void m33791bX(long j) {
        m33787bQ(j);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Economy Max Trading Units")
    @C0064Am(aul = "3f7b9ba9ff2910206000cf93952786a0", aum = 0)
    /* renamed from: HL */
    private int m33761HL() {
        return m33773Hs();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Economy Max Trading Units")
    @C0064Am(aul = "76ab63d8cf1aaef19b683833c98cf00c", aum = 0)
    /* renamed from: cn */
    private void m33801cn(int i) {
        m33796ci(i);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Economy Min Lot Cheap Sell")
    @C0064Am(aul = "856d0901b97d8e8a4fda465d67177bf1", aum = 0)
    /* renamed from: HN */
    private int m33762HN() {
        return m33774Ht();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Economy Min Lot Cheap Sell")
    @C0064Am(aul = "0fda47a2fe21bb87704d2a8f4c7bd394", aum = 0)
    /* renamed from: cp */
    private void m33802cp(int i) {
        m33797cj(i);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Economy Min Lot Expensive Sell")
    @C0064Am(aul = "d435bff1826ee64aad1f39a9fcd764e1", aum = 0)
    /* renamed from: HP */
    private int m33763HP() {
        return m33775Hu();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Economy Min Lot Expensive Sell")
    @C0064Am(aul = "5562f2b309ad71a3505eb1d17a46f673", aum = 0)
    /* renamed from: cr */
    private void m33803cr(int i) {
        m33798ck(i);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Economy Min Lot Active Buy")
    @C0064Am(aul = "f713207570068eb6f774694294ce9996", aum = 0)
    /* renamed from: HR */
    private int m33764HR() {
        return m33776Hv();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Economy Min Lot Active Buy")
    @C0064Am(aul = "a08f2da822978d9af4aed7393bbdc0a1", aum = 0)
    /* renamed from: ct */
    private void m33804ct(int i) {
        m33799cl(i);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Economy Min Lot Any Buy")
    @C0064Am(aul = "cf90f56419b8e89c204f20616971b4ec", aum = 0)
    /* renamed from: HT */
    private int m33765HT() {
        return m33777Hw();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Economy Min Lot Any Buy")
    @C0064Am(aul = "77817a4924d76956f661099f01f9649c", aum = 0)
    /* renamed from: cv */
    private void m33805cv(int i) {
        m33800cm(i);
    }
}
