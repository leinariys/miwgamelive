package game.script.item;

import game.network.manager.C6546aoy;
import game.network.message.externalizable.aCE;
import game.script.damage.DamageType;
import game.script.util.AdapterLikedList;
import logic.baa.C1616Xf;
import logic.baa.C1634Xv;
import logic.baa.C5511aMd;
import logic.baa.C6485anp;
import logic.data.mbean.C2841ku;
import logic.data.mbean.C6956awv;
import logic.res.code.C5640aRc;
import logic.res.code.C5663aRz;
import logic.res.code.C6494any;
import logic.res.html.C2491fm;
import p001a.*;

import java.util.Collection;
import java.util.Map;

@C5829abJ("2.1.1")
@C6485anp
@C2712iu(version = "2.1.1")
@C5511aMd
/* renamed from: a.SG */
/* compiled from: a */
public class ProjectileWeapon extends Weapon implements C1616Xf {

    /* renamed from: Lm */
    public static final C2491fm f1532Lm = null;
    public static final C2491fm _f_hasHollowField_0020_0028_0029Z = null;
    public static final C2491fm _f_onResurrect_0020_0028_0029V = null;
    public static final int _m_fieldCount = 0;
    public static final C5663aRz[] _m_fields = null;
    public static final int _m_methodCount = 0;
    public static final C2491fm[] _m_methods = null;
    public static final C2491fm aPf = null;
    public static final C2491fm bMg = null;
    public static final C2491fm bMh = null;
    public static final C2491fm bMi = null;
    public static final C2491fm bMj = null;
    public static final C2491fm bMo = null;
    public static final C2491fm bMp = null;
    public static final C2491fm bMq = null;
    public static final C2491fm bMr = null;
    public static final C2491fm bMs = null;
    public static final C2491fm cYO = null;
    public static final C2491fm clz = null;
    public static final C5663aRz efg = null;
    public static final C5663aRz efh = null;
    public static final C5663aRz efi = null;
    public static final C5663aRz efj = null;
    public static final C5663aRz efk = null;
    public static final C2491fm efl = null;
    public static final C2491fm efm = null;
    public static final C2491fm efn = null;
    public static final C2491fm efo = null;
    public static final C2491fm efp = null;
    public static final C2491fm efq = null;
    public static final C2491fm efr = null;
    public static final C2491fm efs = null;
    public static final C2491fm eft = null;
    public static final C2491fm efu = null;
    public static final long serialVersionUID = 0;
    /* renamed from: uW */
    public static final C2491fm f1533uW = null;
    public static C6494any ___iScriptClass;
    @C0064Am(aul = "f88658dd58cf697d90d167ec4ab81e2c", aum = 1)
    private static ClipType atA;
    @C0064Am(aul = "96bf36d74ba3e2e929e984ac67e6baf9", aum = 2)
    private static int atB;
    @C0064Am(aul = "7fa1f36226edb8d37c852d19e09c2fa4", aum = 3)
    @C5256aCi
    private static Clip atC;
    @C0064Am(aul = "affa215a6dcc1d34cfeb582beb9fb930", aum = 4)
    private static ProjectileAdapter atD;
    @C0064Am(aul = "a1f95993c4f572dc5d31c48d79c72252", aum = 0)
    private static int atz;

    static {
        m9326V();
    }

    public ProjectileWeapon() {
        super((C5540aNg) null);
        super.mo10S();
    }

    public ProjectileWeapon(ProjectileWeaponType agj) {
        super((C5540aNg) null);
        super._m_script_init(agj);
    }

    public ProjectileWeapon(C5540aNg ang) {
        super(ang);
    }

    /* renamed from: V */
    static void m9326V() {
        serialVersionUID = (long) 1;
        _m_fieldCount = Weapon._m_fieldCount + 5;
        _m_methodCount = Weapon._m_methodCount + 26;
        int i = Weapon._m_fieldCount;
        C5663aRz[] arzArr = new C5663aRz[(i + 5)];
        C5663aRz b = C5640aRc.m17844b(ProjectileWeapon.class, "a1f95993c4f572dc5d31c48d79c72252", i);
        efg = b;
        arzArr[i] = b;
        int i2 = i + 1;
        C5663aRz b2 = C5640aRc.m17844b(ProjectileWeapon.class, "f88658dd58cf697d90d167ec4ab81e2c", i2);
        efh = b2;
        arzArr[i2] = b2;
        int i3 = i2 + 1;
        C5663aRz b3 = C5640aRc.m17844b(ProjectileWeapon.class, "96bf36d74ba3e2e929e984ac67e6baf9", i3);
        efi = b3;
        arzArr[i3] = b3;
        int i4 = i3 + 1;
        C5663aRz b4 = C5640aRc.m17844b(ProjectileWeapon.class, "7fa1f36226edb8d37c852d19e09c2fa4", i4);
        efj = b4;
        arzArr[i4] = b4;
        int i5 = i4 + 1;
        C5663aRz b5 = C5640aRc.m17844b(ProjectileWeapon.class, "affa215a6dcc1d34cfeb582beb9fb930", i5);
        efk = b5;
        arzArr[i5] = b5;
        int i6 = i5 + 1;
        _m_fields = arzArr;
        C1634Xv.m11725a((Object[]) Weapon._m_fields, (Object[]) _m_fields);
        int i7 = Weapon._m_methodCount;
        C2491fm[] fmVarArr = new C2491fm[(i7 + 26)];
        C2491fm a = C4105zY.m41624a(ProjectileWeapon.class, "e6f450d5b55dd837fba1efa06c65d6b5", i7);
        _f_onResurrect_0020_0028_0029V = a;
        fmVarArr[i7] = a;
        int i8 = i7 + 1;
        C2491fm a2 = C4105zY.m41624a(ProjectileWeapon.class, "b3d63dc6b59d31f8321b2eac72862f16", i8);
        bMg = a2;
        fmVarArr[i8] = a2;
        int i9 = i8 + 1;
        C2491fm a3 = C4105zY.m41624a(ProjectileWeapon.class, "1874c632153bfd171975e302145f9a77", i9);
        efl = a3;
        fmVarArr[i9] = a3;
        int i10 = i9 + 1;
        C2491fm a4 = C4105zY.m41624a(ProjectileWeapon.class, "91ca4ca2bd87accde0b698202e7de30c", i10);
        bMi = a4;
        fmVarArr[i10] = a4;
        int i11 = i10 + 1;
        C2491fm a5 = C4105zY.m41624a(ProjectileWeapon.class, "1799257b081c817cb19f47f2b4c5fee8", i11);
        bMj = a5;
        fmVarArr[i11] = a5;
        int i12 = i11 + 1;
        C2491fm a6 = C4105zY.m41624a(ProjectileWeapon.class, "37f05257a1e267c75c0ce65ab1ced6f2", i12);
        bMh = a6;
        fmVarArr[i12] = a6;
        int i13 = i12 + 1;
        C2491fm a7 = C4105zY.m41624a(ProjectileWeapon.class, "e5b257d43adaee1453e3c3ac1a14f86a", i13);
        cYO = a7;
        fmVarArr[i13] = a7;
        int i14 = i13 + 1;
        C2491fm a8 = C4105zY.m41624a(ProjectileWeapon.class, "d5004c5a15052f0c2ec5a9507663274e", i14);
        efm = a8;
        fmVarArr[i14] = a8;
        int i15 = i14 + 1;
        C2491fm a9 = C4105zY.m41624a(ProjectileWeapon.class, "bf590fe24fd4a36637371b069ad7b3c5", i15);
        efn = a9;
        fmVarArr[i15] = a9;
        int i16 = i15 + 1;
        C2491fm a10 = C4105zY.m41624a(ProjectileWeapon.class, "a80b96fc1d63fdba22e2789b7a112a54", i16);
        efo = a10;
        fmVarArr[i16] = a10;
        int i17 = i16 + 1;
        C2491fm a11 = C4105zY.m41624a(ProjectileWeapon.class, "c04a8da3a75a371ca6cb0dd51a7f01fe", i17);
        efp = a11;
        fmVarArr[i17] = a11;
        int i18 = i17 + 1;
        C2491fm a12 = C4105zY.m41624a(ProjectileWeapon.class, "3d0e5eb98d3d4a18e7ce25f9ccfc3369", i18);
        efq = a12;
        fmVarArr[i18] = a12;
        int i19 = i18 + 1;
        C2491fm a13 = C4105zY.m41624a(ProjectileWeapon.class, "7fea10f27abf32c5a8410b0534a68b91", i19);
        efr = a13;
        fmVarArr[i19] = a13;
        int i20 = i19 + 1;
        C2491fm a14 = C4105zY.m41624a(ProjectileWeapon.class, "4f4f1643713804699295690d6f7eb0c1", i20);
        efs = a14;
        fmVarArr[i20] = a14;
        int i21 = i20 + 1;
        C2491fm a15 = C4105zY.m41624a(ProjectileWeapon.class, "ad5a95869f544d3dd05cebd8d1e6de18", i21);
        eft = a15;
        fmVarArr[i21] = a15;
        int i22 = i21 + 1;
        C2491fm a16 = C4105zY.m41624a(ProjectileWeapon.class, "3f27de92c3283411a0e7a3259760a91a", i22);
        bMo = a16;
        fmVarArr[i22] = a16;
        int i23 = i22 + 1;
        C2491fm a17 = C4105zY.m41624a(ProjectileWeapon.class, "133dac72bc85b8d8d78dd54005eba22e", i23);
        aPf = a17;
        fmVarArr[i23] = a17;
        int i24 = i23 + 1;
        C2491fm a18 = C4105zY.m41624a(ProjectileWeapon.class, "a543776efe710f62ffc9dd9e0c6dc731", i24);
        f1533uW = a18;
        fmVarArr[i24] = a18;
        int i25 = i24 + 1;
        C2491fm a19 = C4105zY.m41624a(ProjectileWeapon.class, "c61ccf8bed448fa699202055a5fba2c7", i25);
        bMp = a19;
        fmVarArr[i25] = a19;
        int i26 = i25 + 1;
        C2491fm a20 = C4105zY.m41624a(ProjectileWeapon.class, "d1185c72532384351d1f9054224ce33e", i26);
        bMq = a20;
        fmVarArr[i26] = a20;
        int i27 = i26 + 1;
        C2491fm a21 = C4105zY.m41624a(ProjectileWeapon.class, "21fb823439db302a16f07b4b7666e7e0", i27);
        bMr = a21;
        fmVarArr[i27] = a21;
        int i28 = i27 + 1;
        C2491fm a22 = C4105zY.m41624a(ProjectileWeapon.class, "0f5dd6483e820e27899de7814920f41c", i28);
        efu = a22;
        fmVarArr[i28] = a22;
        int i29 = i28 + 1;
        C2491fm a23 = C4105zY.m41624a(ProjectileWeapon.class, "e3848fe6cabc25c3cc86987d9c18ac8c", i29);
        clz = a23;
        fmVarArr[i29] = a23;
        int i30 = i29 + 1;
        C2491fm a24 = C4105zY.m41624a(ProjectileWeapon.class, "b69d772eaefaddfa254d7f99f16cfc18", i30);
        f1532Lm = a24;
        fmVarArr[i30] = a24;
        int i31 = i30 + 1;
        C2491fm a25 = C4105zY.m41624a(ProjectileWeapon.class, "56311fc94e41a7697c0dad46e01f311f", i31);
        _f_hasHollowField_0020_0028_0029Z = a25;
        fmVarArr[i31] = a25;
        int i32 = i31 + 1;
        C2491fm a26 = C4105zY.m41624a(ProjectileWeapon.class, "cc717801a402a953a53b5632033cf615", i32);
        bMs = a26;
        fmVarArr[i32] = a26;
        int i33 = i32 + 1;
        _m_methods = fmVarArr;
        C1634Xv.m11725a((Object[]) Weapon._m_methods, (Object[]) _m_methods);
        ___iScriptClass = aUO.m18566a(ProjectileWeapon.class, C2841ku.class, _m_fields, _m_methods);
    }

    /* renamed from: a */
    private void m9328a(ProjectileAdapter aVar) {
        bFf().mo5608dq().mo3197f(efk, aVar);
    }

    /* renamed from: a */
    private void m9329a(Clip vo) {
        bFf().mo5608dq().mo3197f(efj, vo);
    }

    @C0064Am(aul = "a80b96fc1d63fdba22e2789b7a112a54", aum = 0)
    @C5566aOg
    @C2499fr
    /* renamed from: a */
    private void m9330a(ClipBox zk) {
        throw new aWi(new aCE(this, efo, new Object[]{zk}));
    }

    @C0064Am(aul = "cc717801a402a953a53b5632033cf615", aum = 0)
    private WeaponType aqy() {
        return buj();
    }

    private int bud() {
        return ((ProjectileWeaponType) getType()).cZX();
    }

    private ClipType bue() {
        return ((ProjectileWeaponType) getType()).cZZ();
    }

    private int buf() {
        return ((ProjectileWeaponType) getType()).bun();
    }

    /* access modifiers changed from: private */
    public Clip bug() {
        return (Clip) bFf().mo5608dq().mo3214p(efj);
    }

    private ProjectileAdapter buh() {
        return (ProjectileAdapter) bFf().mo5608dq().mo3214p(efk);
    }

    @C0064Am(aul = "bf590fe24fd4a36637371b069ad7b3c5", aum = 0)
    @C5566aOg
    private void buk() {
        throw new aWi(new aCE(this, efn, new Object[0]));
    }

    @C5566aOg
    private void bul() {
        switch (bFf().mo6893i(efn)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, efn, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, efn, new Object[0]));
                break;
        }
        buk();
    }

    @C0064Am(aul = "c04a8da3a75a371ca6cb0dd51a7f01fe", aum = 0)
    @C5566aOg
    /* renamed from: c */
    private void m9333c(ClipBox zk) {
        throw new aWi(new aCE(this, efp, new Object[]{zk}));
    }

    @C0064Am(aul = "4f4f1643713804699295690d6f7eb0c1", aum = 0)
    @C5566aOg
    @C2499fr
    /* renamed from: d */
    private void m9334d(Clip vo) {
        throw new aWi(new aCE(this, efs, new Object[]{vo}));
    }

    @C0064Am(aul = "7fea10f27abf32c5a8410b0534a68b91", aum = 0)
    @C5566aOg
    /* renamed from: e */
    private boolean m9335e(ClipBoxType aty) {
        throw new aWi(new aCE(this, efr, new Object[]{aty}));
    }

    @C4034yP
    @C5566aOg
    @C0064Am(aul = "1799257b081c817cb19f47f2b4c5fee8", aum = 0)
    @C2499fr(mo18857qh = 2)
    /* renamed from: fG */
    private void m9337fG(int i) {
        throw new aWi(new aCE(this, bMj, new Object[]{new Integer(i)}));
    }

    /* renamed from: i */
    private void m9338i(ClipType aqf) {
        throw new C6039afL();
    }

    private boolean isFull() {
        switch (bFf().mo6893i(cYO)) {
            case 0:
                return false;
            case 2:
                return ((Boolean) bFf().mo5606d(new aCE(this, cYO, new Object[0]))).booleanValue();
            case 3:
                bFf().mo5606d(new aCE(this, cYO, new Object[0]));
                break;
        }
        return aQV();
    }

    @C0064Am(aul = "3d0e5eb98d3d4a18e7ce25f9ccfc3369", aum = 0)
    @C5566aOg
    /* renamed from: j */
    private int m9340j(ClipType aqf) {
        throw new aWi(new aCE(this, efq, new Object[]{aqf}));
    }

    /* renamed from: nk */
    private void m9341nk(int i) {
        throw new C6039afL();
    }

    /* renamed from: nl */
    private void m9342nl(int i) {
        throw new C6039afL();
    }

    @C0064Am(aul = "b69d772eaefaddfa254d7f99f16cfc18", aum = 0)
    @C5566aOg
    @C2499fr
    /* renamed from: qU */
    private void m9343qU() {
        throw new aWi(new aCE(this, f1532Lm, new Object[0]));
    }

    @ClientOnly
    /* renamed from: QW */
    public boolean mo961QW() {
        switch (bFf().mo6893i(_f_hasHollowField_0020_0028_0029Z)) {
            case 0:
                return false;
            case 2:
                return ((Boolean) bFf().mo5606d(new aCE(this, _f_hasHollowField_0020_0028_0029Z, new Object[0]))).booleanValue();
            case 3:
                bFf().mo5606d(new aCE(this, _f_hasHollowField_0020_0028_0029Z, new Object[0]));
                break;
        }
        return m9324QV();
    }

    /* renamed from: S */
    public void mo10S() {
        super.mo10S();
    }

    /* renamed from: T */
    public C6494any mo11T() {
        return ___iScriptClass;
    }

    /* renamed from: TJ */
    public AdapterLikedList<WeaponAdapter> mo5353TJ() {
        switch (bFf().mo6893i(aPf)) {
            case 0:
                return null;
            case 2:
                return (AdapterLikedList) bFf().mo5606d(new aCE(this, aPf, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, aPf, new Object[0]));
                break;
        }
        return m9325TI();
    }

    /* renamed from: U */
    public C2491fm[] mo12U() {
        return _m_methods;
    }

    /* renamed from: W */
    public Object mo13W() {
        return new C2841ku(this);
    }

    /* renamed from: a */
    public Object mo14a(C0495Gr gr) {
        Object[] args = gr.getArgs();
        switch (gr.mo2417hq() - Weapon._m_methodCount) {
            case 0:
                m9331aG();
                return null;
            case 1:
                return new Integer(aqg());
            case 2:
                m9332b((Clip) args[0]);
                return null;
            case 3:
                m9336fE(((Integer) args[0]).intValue());
                return null;
            case 4:
                m9337fG(((Integer) args[0]).intValue());
                return null;
            case 5:
                return new Integer(aqi());
            case 6:
                return new Boolean(aQV());
            case 7:
                return bui();
            case 8:
                buk();
                return null;
            case 9:
                m9330a((ClipBox) args[0]);
                return null;
            case 10:
                m9333c((ClipBox) args[0]);
                return null;
            case 11:
                return new Integer(m9340j((ClipType) args[0]));
            case 12:
                return new Boolean(m9335e((ClipBoxType) args[0]));
            case 13:
                m9334d((Clip) args[0]);
                return null;
            case 14:
                return new Integer(bum());
            case 15:
                return aqq();
            case 16:
                return m9325TI();
            case 17:
                return m9339ik();
            case 18:
                return aqs();
            case 19:
                return aqu();
            case 20:
                return aqw();
            case 21:
                return buo();
            case 22:
                return new Float(ayA());
            case 23:
                m9343qU();
                return null;
            case 24:
                return new Boolean(m9324QV());
            case 25:
                return aqy();
            default:
                return super.mo14a(gr);
        }
    }

    /* renamed from: a */
    public void mo15a(Collection collection, C0495Gr gr) {
        super.mo15a(collection, gr);
    }

    /* renamed from: aH */
    public void mo70aH() {
        switch (bFf().mo6893i(_f_onResurrect_0020_0028_0029V)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, _f_onResurrect_0020_0028_0029V, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, _f_onResurrect_0020_0028_0029V, new Object[0]));
                break;
        }
        m9331aG();
    }

    public int aqh() {
        switch (bFf().mo6893i(bMg)) {
            case 0:
                return 0;
            case 2:
                return ((Integer) bFf().mo5606d(new aCE(this, bMg, new Object[0]))).intValue();
            case 3:
                bFf().mo5606d(new aCE(this, bMg, new Object[0]));
                break;
        }
        return aqg();
    }

    public int aqj() {
        switch (bFf().mo6893i(bMh)) {
            case 0:
                return 0;
            case 2:
                return ((Integer) bFf().mo5606d(new aCE(this, bMh, new Object[0]))).intValue();
            case 3:
                bFf().mo5606d(new aCE(this, bMh, new Object[0]));
                break;
        }
        return aqi();
    }

    public String aqr() {
        switch (bFf().mo6893i(bMo)) {
            case 0:
                return null;
            case 2:
                return (String) bFf().mo5606d(new aCE(this, bMo, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, bMo, new Object[0]));
                break;
        }
        return aqq();
    }

    public String aqt() {
        switch (bFf().mo6893i(bMp)) {
            case 0:
                return null;
            case 2:
                return (String) bFf().mo5606d(new aCE(this, bMp, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, bMp, new Object[0]));
                break;
        }
        return aqs();
    }

    public String aqv() {
        switch (bFf().mo6893i(bMq)) {
            case 0:
                return null;
            case 2:
                return (String) bFf().mo5606d(new aCE(this, bMq, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, bMq, new Object[0]));
                break;
        }
        return aqu();
    }

    public String aqx() {
        switch (bFf().mo6893i(bMr)) {
            case 0:
                return null;
            case 2:
                return (String) bFf().mo5606d(new aCE(this, bMr, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, bMr, new Object[0]));
                break;
        }
        return aqw();
    }

    public /* bridge */ /* synthetic */ WeaponType aqz() {
        switch (bFf().mo6893i(bMs)) {
            case 0:
                return null;
            case 2:
                return (WeaponType) bFf().mo5606d(new aCE(this, bMs, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, bMs, new Object[0]));
                break;
        }
        return aqy();
    }

    /* access modifiers changed from: protected */
    public float ayB() {
        switch (bFf().mo6893i(clz)) {
            case 0:
                return 0.0f;
            case 2:
                return ((Float) bFf().mo5606d(new aCE(this, clz, new Object[0]))).floatValue();
            case 3:
                bFf().mo5606d(new aCE(this, clz, new Object[0]));
                break;
        }
        return ayA();
    }

    @C5566aOg
    @C2499fr
    /* renamed from: b */
    public void mo5362b(ClipBox zk) {
        switch (bFf().mo6893i(efo)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, efo, new Object[]{zk}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, efo, new Object[]{zk}));
                break;
        }
        m9330a(zk);
    }

    public ProjectileWeaponType buj() {
        switch (bFf().mo6893i(efm)) {
            case 0:
                return null;
            case 2:
                return (ProjectileWeaponType) bFf().mo5606d(new aCE(this, efm, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, efm, new Object[0]));
                break;
        }
        return bui();
    }

    public int bun() {
        switch (bFf().mo6893i(eft)) {
            case 0:
                return 0;
            case 2:
                return ((Integer) bFf().mo5606d(new aCE(this, eft, new Object[0]))).intValue();
            case 3:
                bFf().mo5606d(new aCE(this, eft, new Object[0]));
                break;
        }
        return bum();
    }

    public Clip bup() {
        switch (bFf().mo6893i(efu)) {
            case 0:
                return null;
            case 2:
                return (Clip) bFf().mo5606d(new aCE(this, efu, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, efu, new Object[0]));
                break;
        }
        return buo();
    }

    /* renamed from: c */
    public void mo5366c(Clip vo) {
        switch (bFf().mo6893i(efl)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, efl, new Object[]{vo}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, efl, new Object[]{vo}));
                break;
        }
        m9332b(vo);
    }

    /* renamed from: c */
    public C5663aRz[] mo25c() {
        return _m_fields;
    }

    @C5566aOg
    /* renamed from: d */
    public void mo5367d(ClipBox zk) {
        switch (bFf().mo6893i(efp)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, efp, new Object[]{zk}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, efp, new Object[]{zk}));
                break;
        }
        m9333c(zk);
    }

    @C5566aOg
    @C2499fr
    /* renamed from: e */
    public void mo5368e(Clip vo) {
        switch (bFf().mo6893i(efs)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, efs, new Object[]{vo}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, efs, new Object[]{vo}));
                break;
        }
        m9334d(vo);
    }

    @C5566aOg
    /* renamed from: f */
    public boolean mo5369f(ClipBoxType aty) {
        switch (bFf().mo6893i(efr)) {
            case 0:
                return false;
            case 2:
                return ((Boolean) bFf().mo5606d(new aCE(this, efr, new Object[]{aty}))).booleanValue();
            case 3:
                bFf().mo5606d(new aCE(this, efr, new Object[]{aty}));
                break;
        }
        return m9335e(aty);
    }

    /* renamed from: fF */
    public void mo5370fF(int i) {
        switch (bFf().mo6893i(bMi)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, bMi, new Object[]{new Integer(i)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, bMi, new Object[]{new Integer(i)}));
                break;
        }
        m9336fE(i);
    }

    @C4034yP
    @C5566aOg
    @C2499fr(mo18857qh = 2)
    /* renamed from: fH */
    public void mo5371fH(int i) {
        switch (bFf().mo6893i(bMj)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, bMj, new Object[]{new Integer(i)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, bMj, new Object[]{new Integer(i)}));
                break;
        }
        m9337fG(i);
    }

    /* renamed from: il */
    public C5260aCm mo5372il() {
        switch (bFf().mo6893i(f1533uW)) {
            case 0:
                return null;
            case 2:
                return (C5260aCm) bFf().mo5606d(new aCE(this, f1533uW, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f1533uW, new Object[0]));
                break;
        }
        return m9339ik();
    }

    @C5566aOg
    /* renamed from: k */
    public int mo5373k(ClipType aqf) {
        switch (bFf().mo6893i(efq)) {
            case 0:
                return 0;
            case 2:
                return ((Integer) bFf().mo5606d(new aCE(this, efq, new Object[]{aqf}))).intValue();
            case 3:
                bFf().mo5606d(new aCE(this, efq, new Object[]{aqf}));
                break;
        }
        return m9340j(aqf);
    }

    @C5566aOg
    @C2499fr
    /* renamed from: qV */
    public void mo1959qV() {
        switch (bFf().mo6893i(f1532Lm)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f1532Lm, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f1532Lm, new Object[0]));
                break;
        }
        m9343qU();
    }

    /* renamed from: a */
    public void mo5354a(ProjectileWeaponType agj) {
        super.mo12949x(agj);
        ProjectileAdapter aVar = (ProjectileAdapter) bFf().mo6865M(ProjectileAdapter.class);
        aVar.mo5374c(this);
        m9328a(aVar);
        mo5353TJ().mo23261e(buh());
        try {
            bul();
        } catch (C1230b e) {
            mo8354g("Error auto-loading weapon " + bFY() + " with its default ammo", (Throwable) e);
        }
    }

    @C0064Am(aul = "e6f450d5b55dd837fba1efa06c65d6b5", aum = 0)
    /* renamed from: aG */
    private void m9331aG() {
        super.mo70aH();
        if (buh() == null) {
            ProjectileAdapter aVar = (ProjectileAdapter) bFf().mo6865M(ProjectileAdapter.class);
            aVar.mo5374c(this);
            m9328a(aVar);
            mo5353TJ().mo23261e(buh());
        }
    }

    @C0064Am(aul = "b3d63dc6b59d31f8321b2eac72862f16", aum = 0)
    private int aqg() {
        if (bug() == null) {
            return 0;
        }
        return bug().mo302Iq();
    }

    @C0064Am(aul = "1874c632153bfd171975e302145f9a77", aum = 0)
    /* renamed from: b */
    private void m9332b(Clip vo) {
        m9329a(vo);
        if (bHa()) {
            mo12907TP();
        }
    }

    @C0064Am(aul = "91ca4ca2bd87accde0b698202e7de30c", aum = 0)
    /* renamed from: fE */
    private void m9336fE(int i) {
        if (bGY()) {
            mo5371fH(i);
        }
        if (bug() != null) {
            bug().mo11564db(i);
        }
    }

    @C0064Am(aul = "37f05257a1e267c75c0ce65ab1ced6f2", aum = 0)
    private int aqi() {
        return 1;
    }

    @C0064Am(aul = "e5b257d43adaee1453e3c3ac1a14f86a", aum = 0)
    private boolean aQV() {
        if (bug() != null && bug().mo302Iq() == bud()) {
            return true;
        }
        return false;
    }

    @C0064Am(aul = "d5004c5a15052f0c2ec5a9507663274e", aum = 0)
    private ProjectileWeaponType bui() {
        return (ProjectileWeaponType) super.aqz();
    }

    @C0064Am(aul = "ad5a95869f544d3dd05cebd8d1e6de18", aum = 0)
    private int bum() {
        return buf();
    }

    @C0064Am(aul = "3f27de92c3283411a0e7a3259760a91a", aum = 0)
    private String aqq() {
        if (bug() != null) {
            return bug().bAJ().mo11042iu();
        }
        throw new IllegalStateException("ProjectileWeapon is trying to retrieve ammo's SurfaceMaterial but it's not loaded");
    }

    @C0064Am(aul = "133dac72bc85b8d8d78dd54005eba22e", aum = 0)
    /* renamed from: TI */
    private AdapterLikedList<WeaponAdapter> m9325TI() {
        return super.mo5353TJ();
    }

    @C0064Am(aul = "a543776efe710f62ffc9dd9e0c6dc731", aum = 0)
    /* renamed from: ik */
    private C5260aCm m9339ik() {
        C5260aCm acm = new C5260aCm();
        for (Map.Entry key : buj().bpA().entrySet()) {
            DamageType fr = (DamageType) key.getKey();
            acm.mo8227g(fr, ((Float) cTZ().get(fr)).floatValue());
        }
        return acm;
    }

    @C0064Am(aul = "c61ccf8bed448fa699202055a5fba2c7", aum = 0)
    private String aqs() {
        if (bug() != null) {
            return bug().bAJ().mo12100sK().getHandle();
        }
        return null;
    }

    @C0064Am(aul = "d1185c72532384351d1f9054224ce33e", aum = 0)
    private String aqu() {
        if (bug() != null) {
            return bug().bAJ().mo3521Nu().getHandle();
        }
        return null;
    }

    @C0064Am(aul = "21fb823439db302a16f07b4b7666e7e0", aum = 0)
    private String aqw() {
        return bug().bAJ().mo3521Nu().getFile();
    }

    @C0064Am(aul = "0f5dd6483e820e27899de7814920f41c", aum = 0)
    private Clip buo() {
        return bug();
    }

    @C0064Am(aul = "e3848fe6cabc25c3cc86987d9c18ac8c", aum = 0)
    private float ayA() {
        if (cUn() == null) {
            return super.ayB();
        }
        if (buj().mo10793ml() > 0.0f) {
            return 1.0f / buj().mo10793ml();
        }
        return 1.0f;
    }

    @C0064Am(aul = "56311fc94e41a7697c0dad46e01f311f", aum = 0)
    @ClientOnly
    /* renamed from: QV */
    private boolean m9324QV() {
        if (super.mo961QW()) {
            return true;
        }
        if (bug() == null || !C1298TD.m9830t(bug()).bFT()) {
            return false;
        }
        return true;
    }

    @C6546aoy
    /* renamed from: a.SG$b */
    /* compiled from: a */
    public static class C1230b extends Exception {


        public C1230b(C2293dh dhVar) {
            super(dhVar);
        }

        public C1230b() {
        }

        public C1230b(String str) {
            super(str);
        }
    }

    @C6485anp
    @C5511aMd
    /* renamed from: a.SG$a */
    public class ProjectileAdapter extends WeaponAdapter implements C1616Xf {

        /* renamed from: AI */
        public static final C2491fm f1534AI = null;
        public static final int _m_fieldCount = 0;
        public static final C5663aRz[] _m_fields = null;
        public static final int _m_methodCount = 0;
        public static final C2491fm[] _m_methods = null;
        /* renamed from: aT */
        public static final C5663aRz f1535aT = null;
        public static final long serialVersionUID = 0;
        public static C6494any ___iScriptClass;
        @C0064Am(aul = "aee9fef7de962bfa64e9b03429334c0d", aum = 0)
        static /* synthetic */ ProjectileWeapon gaZ;

        static {
            m9365V();
        }

        public ProjectileAdapter() {
            super((C5540aNg) null);
            super.mo10S();
        }

        public ProjectileAdapter(ProjectileWeapon sg) {
            super((C5540aNg) null);
            super._m_script_init(sg);
        }

        public ProjectileAdapter(C5540aNg ang) {
            super(ang);
        }

        /* renamed from: V */
        static void m9365V() {
            serialVersionUID = (long) 1;
            _m_fieldCount = WeaponAdapter._m_fieldCount + 1;
            _m_methodCount = WeaponAdapter._m_methodCount + 1;
            int i = WeaponAdapter._m_fieldCount;
            C5663aRz[] arzArr = new C5663aRz[(i + 1)];
            C5663aRz b = C5640aRc.m17844b(ProjectileAdapter.class, "aee9fef7de962bfa64e9b03429334c0d", i);
            f1535aT = b;
            arzArr[i] = b;
            int i2 = i + 1;
            _m_fields = arzArr;
            C1634Xv.m11725a((Object[]) WeaponAdapter._m_fields, (Object[]) _m_fields);
            int i3 = WeaponAdapter._m_methodCount;
            C2491fm[] fmVarArr = new C2491fm[(i3 + 1)];
            C2491fm a = C4105zY.m41624a(ProjectileAdapter.class, "13713d2de478cb2af3373217a007e32c", i3);
            f1534AI = a;
            fmVarArr[i3] = a;
            int i4 = i3 + 1;
            _m_methods = fmVarArr;
            C1634Xv.m11725a((Object[]) WeaponAdapter._m_methods, (Object[]) _m_methods);
            ___iScriptClass = aUO.m18566a(ProjectileAdapter.class, C6956awv.class, _m_fields, _m_methods);
        }

        /* renamed from: b */
        private void m9366b(ProjectileWeapon sg) {
            bFf().mo5608dq().mo3197f(f1535aT, sg);
        }

        private ProjectileWeapon cjI() {
            return (ProjectileWeapon) bFf().mo5608dq().mo3214p(f1535aT);
        }

        /* renamed from: S */
        public void mo10S() {
            super.mo10S();
        }

        /* renamed from: T */
        public C6494any mo11T() {
            return ___iScriptClass;
        }

        /* renamed from: U */
        public C2491fm[] mo12U() {
            return _m_methods;
        }

        /* renamed from: W */
        public Object mo13W() {
            return new C6956awv(this);
        }

        /* renamed from: a */
        public Object mo14a(C0495Gr gr) {
            Object[] args = gr.getArgs();
            switch (gr.mo2417hq() - WeaponAdapter._m_methodCount) {
                case 0:
                    return new Float(m9367c((DamageType) args[0]));
                default:
                    return super.mo14a(gr);
            }
        }

        /* renamed from: a */
        public void mo15a(Collection collection, C0495Gr gr) {
            super.mo15a(collection, gr);
        }

        /* renamed from: c */
        public C5663aRz[] mo25c() {
            return _m_fields;
        }

        /* renamed from: d */
        public float mo5375d(DamageType fr) {
            switch (bFf().mo6893i(f1534AI)) {
                case 0:
                    return 0.0f;
                case 2:
                    return ((Float) bFf().mo5606d(new aCE(this, f1534AI, new Object[]{fr}))).floatValue();
                case 3:
                    bFf().mo5606d(new aCE(this, f1534AI, new Object[]{fr}));
                    break;
            }
            return m9367c(fr);
        }

        /* renamed from: c */
        public void mo5374c(ProjectileWeapon sg) {
            m9366b(sg);
            super.mo10S();
        }

        @C0064Am(aul = "13713d2de478cb2af3373217a007e32c", aum = 0)
        /* renamed from: c */
        private float m9367c(DamageType fr) {
            float f;
            Float f2;
            if (cjI().bug() == null || (f2 = cjI().bug().bAL().get(fr)) == null) {
                f = 0.0f;
            } else {
                f = f2.floatValue();
            }
            Float valueOf = Float.valueOf(((Float) cjI().cUr().get(fr)).floatValue() + f);
            if (valueOf == null) {
                return 0.0f;
            }
            return valueOf.floatValue();
        }
    }
}
