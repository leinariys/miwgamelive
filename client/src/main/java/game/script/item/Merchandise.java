package game.script.item;

import game.network.message.serializable.C3438ra;
import game.script.pda.DatabaseCategory;
import game.script.pda.TaikopediaEntry;
import game.script.resource.Asset;
import logic.baa.C1616Xf;
import logic.baa.C1634Xv;
import logic.baa.C5511aMd;
import logic.baa.C6485anp;
import logic.data.mbean.aJB;
import logic.res.code.C5640aRc;
import logic.res.code.C5663aRz;
import logic.res.code.C6494any;
import logic.res.html.C2491fm;
import p001a.*;

import java.util.Collection;

@C2712iu(mo19786Bt = MerchandiseBaseType.class)
@C5511aMd
@C6485anp
/* renamed from: a.aUH */
/* compiled from: a */
public class Merchandise extends BulkItem implements C1616Xf {
    public static final int _m_fieldCount = 0;
    public static final C5663aRz[] _m_fields = null;
    public static final int _m_methodCount = 0;
    public static final C2491fm[] _m_methods = null;
    public static final C5663aRz aMg = null;
    public static final C5663aRz aMh = null;
    public static final C5663aRz aMi = null;
    public static final C5663aRz awd = null;
    public static final C5663aRz dyY = null;
    public static final long serialVersionUID = 0;
    public static C6494any ___iScriptClass;
    @C0064Am(aul = "d8fa7d7998da402ed79390bc57836782", aum = 3)
    private static boolean dyX;
    @C0064Am(aul = "d40ee4fa922933d1280c6af35a686131", aum = 0)

    /* renamed from: jS */
    private static DatabaseCategory f3840jS;
    @C0064Am(aul = "9393cf680dc9574f1d4af750a1a58e7d", aum = 1)

    /* renamed from: jT */
    private static C3438ra<TaikopediaEntry> f3841jT;
    @C0064Am(aul = "e3b2f70a726eadc1ead1f7836bc3a5b4", aum = 2)

    /* renamed from: jU */
    private static Asset f3842jU;
    @C0064Am(aul = "9e52d38c0bd6adbbe72663da21b90ff2", aum = 4)

    /* renamed from: jV */
    private static float f3843jV;

    static {
        m18532V();
    }

    public Merchandise() {
        super((C5540aNg) null);
        super.mo10S();
    }

    public Merchandise(MerchandiseType mf) {
        super((C5540aNg) null);
        super._m_script_init(mf);
    }

    public Merchandise(C5540aNg ang) {
        super(ang);
    }

    /* renamed from: V */
    static void m18532V() {
        serialVersionUID = (long) 1;
        _m_fieldCount = BulkItem._m_fieldCount + 5;
        _m_methodCount = BulkItem._m_methodCount + 0;
        int i = BulkItem._m_fieldCount;
        C5663aRz[] arzArr = new C5663aRz[(i + 5)];
        C5663aRz b = C5640aRc.m17844b(Merchandise.class, "d40ee4fa922933d1280c6af35a686131", i);
        aMh = b;
        arzArr[i] = b;
        int i2 = i + 1;
        C5663aRz b2 = C5640aRc.m17844b(Merchandise.class, "9393cf680dc9574f1d4af750a1a58e7d", i2);
        awd = b2;
        arzArr[i2] = b2;
        int i3 = i2 + 1;
        C5663aRz b3 = C5640aRc.m17844b(Merchandise.class, "e3b2f70a726eadc1ead1f7836bc3a5b4", i3);
        aMg = b3;
        arzArr[i3] = b3;
        int i4 = i3 + 1;
        C5663aRz b4 = C5640aRc.m17844b(Merchandise.class, "d8fa7d7998da402ed79390bc57836782", i4);
        dyY = b4;
        arzArr[i4] = b4;
        int i5 = i4 + 1;
        C5663aRz b5 = C5640aRc.m17844b(Merchandise.class, "9e52d38c0bd6adbbe72663da21b90ff2", i5);
        aMi = b5;
        arzArr[i5] = b5;
        int i6 = i5 + 1;
        _m_fields = arzArr;
        C1634Xv.m11725a((Object[]) BulkItem._m_fields, (Object[]) _m_fields);
        _m_methods = new C2491fm[(BulkItem._m_methodCount + 0)];
        C1634Xv.m11725a((Object[]) BulkItem._m_methods, (Object[]) _m_methods);
        ___iScriptClass = aUO.m18566a(Merchandise.class, aJB.class, _m_fields, _m_methods);
    }

    /* renamed from: L */
    private void m18527L(Asset tCVar) {
        throw new C6039afL();
    }

    /* renamed from: Ll */
    private C3438ra m18528Ll() {
        return ((MerchandiseType) getType()).bgF();
    }

    /* renamed from: RQ */
    private Asset m18529RQ() {
        return ((MerchandiseType) getType()).mo187Se();
    }

    /* renamed from: RR */
    private DatabaseCategory m18530RR() {
        return ((MerchandiseType) getType()).mo184RY();
    }

    /* renamed from: RS */
    private float m18531RS() {
        return ((MerchandiseType) getType()).mo188Sg();
    }

    /* renamed from: a */
    private void m18533a(DatabaseCategory aik) {
        throw new C6039afL();
    }

    private boolean bgD() {
        return ((MerchandiseType) getType()).bgJ();
    }

    /* renamed from: cx */
    private void m18534cx(float f) {
        throw new C6039afL();
    }

    /* renamed from: da */
    private void m18535da(boolean z) {
        throw new C6039afL();
    }

    /* renamed from: x */
    private void m18536x(C3438ra raVar) {
        throw new C6039afL();
    }

    /* renamed from: S */
    public void mo10S() {
        super.mo10S();
    }

    /* renamed from: T */
    public C6494any mo11T() {
        return ___iScriptClass;
    }

    /* renamed from: U */
    public C2491fm[] mo12U() {
        return _m_methods;
    }

    /* renamed from: W */
    public Object mo13W() {
        return new aJB(this);
    }

    /* renamed from: a */
    public Object mo14a(C0495Gr gr) {
        return super.mo14a(gr);
    }

    /* renamed from: a */
    public void mo15a(Collection collection, C0495Gr gr) {
        super.mo15a(collection, gr);
    }

    /* renamed from: c */
    public C5663aRz[] mo25c() {
        return _m_fields;
    }

    /* renamed from: e */
    public void mo11606e(MerchandiseType mf) {
        super.mo11561b((BulkItemType) mf);
    }
}
