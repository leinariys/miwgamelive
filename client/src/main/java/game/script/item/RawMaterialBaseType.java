package game.script.item;

import game.network.message.externalizable.aCE;
import game.script.resource.Asset;
import logic.baa.*;
import logic.data.mbean.C3909wd;
import logic.res.code.C5663aRz;
import logic.res.code.C6494any;
import logic.res.html.C2491fm;
import p001a.*;
import taikodom.infra.script.I18NString;

import java.util.Collection;

@C5511aMd
@C6485anp
/* renamed from: a.aOf  reason: case insensitive filesystem */
/* compiled from: a */
public abstract class RawMaterialBaseType extends BulkItemType implements C1616Xf, C4068yr {

    /* renamed from: MN */
    public static final C2491fm f3475MN = null;
    public static final int _m_fieldCount = 0;
    public static final C5663aRz[] _m_fields = null;
    public static final int _m_methodCount = 0;
    public static final C2491fm[] _m_methods = null;
    public static final C2491fm aMp = null;
    public static final C2491fm aMq = null;
    public static final C2491fm aMr = null;
    public static final long serialVersionUID = 0;
    public static C6494any ___iScriptClass;

    static {
        m16940V();
    }

    public RawMaterialBaseType() {
        super((C5540aNg) null);
        super.mo10S();
    }

    public RawMaterialBaseType(C5540aNg ang) {
        super(ang);
    }

    /* renamed from: V */
    static void m16940V() {
        serialVersionUID = (long) 1;
        _m_fieldCount = BulkItemType._m_fieldCount + 0;
        _m_methodCount = BulkItemType._m_methodCount + 4;
        _m_fields = new C5663aRz[(BulkItemType._m_fieldCount + 0)];
        C1634Xv.m11725a((Object[]) BulkItemType._m_fields, (Object[]) _m_fields);
        int i = BulkItemType._m_methodCount;
        C2491fm[] fmVarArr = new C2491fm[(i + 4)];
        C2491fm a = C4105zY.m41624a(RawMaterialBaseType.class, "ce768409d6836bd22c674dd44bbd7f11", i);
        f3475MN = a;
        fmVarArr[i] = a;
        int i2 = i + 1;
        C2491fm a2 = C4105zY.m41624a(RawMaterialBaseType.class, "1161d86850bbc79dab20d7c7ce83909d", i2);
        aMp = a2;
        fmVarArr[i2] = a2;
        int i3 = i2 + 1;
        C2491fm a3 = C4105zY.m41624a(RawMaterialBaseType.class, "9965c15c50c5045e9ba36661d79a9ce6", i3);
        aMq = a3;
        fmVarArr[i3] = a3;
        int i4 = i3 + 1;
        C2491fm a4 = C4105zY.m41624a(RawMaterialBaseType.class, "3dc5796a70ce450e5fa33d81e2350c25", i4);
        aMr = a4;
        fmVarArr[i4] = a4;
        int i5 = i4 + 1;
        _m_methods = fmVarArr;
        C1634Xv.m11725a((Object[]) BulkItemType._m_methods, (Object[]) _m_methods);
        ___iScriptClass = aUO.m18566a(RawMaterialBaseType.class, C3909wd.class, _m_fields, _m_methods);
    }

    @C0064Am(aul = "3dc5796a70ce450e5fa33d81e2350c25", aum = 0)
    /* renamed from: Sd */
    private Asset m16939Sd() {
        throw new aWi(new aCE(this, aMr, new Object[0]));
    }

    /* renamed from: Sa */
    public String mo185Sa() {
        switch (bFf().mo6893i(aMp)) {
            case 0:
                return null;
            case 2:
                return (String) bFf().mo5606d(new aCE(this, aMp, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, aMp, new Object[0]));
                break;
        }
        return m16937RZ();
    }

    /* renamed from: Sc */
    public String mo10655Sc() {
        switch (bFf().mo6893i(aMq)) {
            case 0:
                return null;
            case 2:
                return (String) bFf().mo5606d(new aCE(this, aMq, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, aMq, new Object[0]));
                break;
        }
        return m16938Sb();
    }

    /* renamed from: Se */
    public Asset mo187Se() {
        switch (bFf().mo6893i(aMr)) {
            case 0:
                return null;
            case 2:
                return (Asset) bFf().mo5606d(new aCE(this, aMr, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, aMr, new Object[0]));
                break;
        }
        return m16939Sd();
    }

    /* renamed from: T */
    public C6494any mo11T() {
        return ___iScriptClass;
    }

    /* renamed from: U */
    public C2491fm[] mo12U() {
        return _m_methods;
    }

    /* renamed from: W */
    public Object mo13W() {
        return new C3909wd(this);
    }

    /* renamed from: a */
    public Object mo14a(C0495Gr gr) {
        gr.getArgs();
        switch (gr.mo2417hq() - BulkItemType._m_methodCount) {
            case 0:
                return m16941rO();
            case 1:
                return m16937RZ();
            case 2:
                return m16938Sb();
            case 3:
                return m16939Sd();
            default:
                return super.mo14a(gr);
        }
    }

    /* renamed from: a */
    public void mo15a(Collection collection, C0495Gr gr) {
        super.mo15a(collection, gr);
    }

    /* renamed from: c */
    public C5663aRz[] mo25c() {
        return _m_fields;
    }

    /* renamed from: rP */
    public I18NString mo195rP() {
        switch (bFf().mo6893i(f3475MN)) {
            case 0:
                return null;
            case 2:
                return (I18NString) bFf().mo5606d(new aCE(this, f3475MN, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f3475MN, new Object[0]));
                break;
        }
        return m16941rO();
    }

    /* renamed from: S */
    public void mo10S() {
        super.mo10S();
    }

    @C0064Am(aul = "ce768409d6836bd22c674dd44bbd7f11", aum = 0)
    /* renamed from: rO */
    private I18NString m16941rO() {
        return mo19891ke();
    }

    @C0064Am(aul = "1161d86850bbc79dab20d7c7ce83909d", aum = 0)
    /* renamed from: RZ */
    private String m16937RZ() {
        if (mo12100sK() != null) {
            return mo12100sK().getHandle();
        }
        return null;
    }

    @C0064Am(aul = "9965c15c50c5045e9ba36661d79a9ce6", aum = 0)
    /* renamed from: Sb */
    private String m16938Sb() {
        if (mo187Se() != null) {
            return mo187Se().getHandle();
        }
        return null;
    }
}
