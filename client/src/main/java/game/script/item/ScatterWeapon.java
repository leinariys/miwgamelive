package game.script.item;

import game.geometry.Quat4fWrap;
import game.network.message.externalizable.aCE;
import logic.baa.C1616Xf;
import logic.baa.C1634Xv;
import logic.baa.C5511aMd;
import logic.baa.C6485anp;
import logic.bbb.C4029yK;
import logic.bbb.C6348alI;
import logic.data.mbean.C2708iq;
import logic.res.code.C5640aRc;
import logic.res.code.C5663aRz;
import logic.res.code.C6494any;
import logic.res.html.C2491fm;
import logic.thred.LogPrinter;
import org.apache.commons.logging.Log;
import org.mozilla1.javascript.ScriptRuntime;
import p001a.*;
import taikodom.geom.Orientation;

import javax.vecmath.Quat4f;
import java.util.Collection;
import java.util.Random;

@C5829abJ("2.1.1")
@C6485anp
@C2712iu(version = "2.1.1")
@C5511aMd
/* renamed from: a.XB */
/* compiled from: a */
public class ScatterWeapon extends ProjectileWeapon implements C1616Xf {
    public static final int _m_fieldCount = 0;
    public static final C5663aRz[] _m_fields = null;
    public static final int _m_methodCount = 0;
    public static final C2491fm[] _m_methods = null;
    public static final C2491fm clw = null;
    public static final C5663aRz eJt = null;
    public static final C5663aRz eJu = null;
    public static final long serialVersionUID = 0;
    private static final Log log = LogPrinter.setClass(Weapon.class);
    public static C6494any ___iScriptClass;
    @C0064Am(aul = "c85c82d6652441f7d8136839f58c0f0d", aum = 0)

    /* renamed from: XT */
    private static int f2099XT;
    @C0064Am(aul = "b2d42ce0f7356c9b0278280b681f70f3", aum = 1)

    /* renamed from: XU */
    private static float f2100XU;

    static {
        m11431V();
    }

    public ScatterWeapon() {
        super((C5540aNg) null);
        super.mo10S();
    }

    public ScatterWeapon(ScatterWeaponType alz) {
        super((C5540aNg) null);
        super._m_script_init(alz);
    }

    public ScatterWeapon(C5540aNg ang) {
        super(ang);
    }

    /* renamed from: V */
    static void m11431V() {
        serialVersionUID = (long) 1;
        _m_fieldCount = ProjectileWeapon._m_fieldCount + 2;
        _m_methodCount = ProjectileWeapon._m_methodCount + 1;
        int i = ProjectileWeapon._m_fieldCount;
        C5663aRz[] arzArr = new C5663aRz[(i + 2)];
        C5663aRz b = C5640aRc.m17844b(ScatterWeapon.class, "c85c82d6652441f7d8136839f58c0f0d", i);
        eJt = b;
        arzArr[i] = b;
        int i2 = i + 1;
        C5663aRz b2 = C5640aRc.m17844b(ScatterWeapon.class, "b2d42ce0f7356c9b0278280b681f70f3", i2);
        eJu = b2;
        arzArr[i2] = b2;
        int i3 = i2 + 1;
        _m_fields = arzArr;
        C1634Xv.m11725a((Object[]) ProjectileWeapon._m_fields, (Object[]) _m_fields);
        int i4 = ProjectileWeapon._m_methodCount;
        C2491fm[] fmVarArr = new C2491fm[(i4 + 1)];
        C2491fm a = C4105zY.m41624a(ScatterWeapon.class, "5c85d8223d0d6df85f08fb2c5b1ebe53", i4);
        clw = a;
        fmVarArr[i4] = a;
        int i5 = i4 + 1;
        _m_methods = fmVarArr;
        C1634Xv.m11725a((Object[]) ProjectileWeapon._m_methods, (Object[]) _m_methods);
        ___iScriptClass = aUO.m18566a(ScatterWeapon.class, C2708iq.class, _m_fields, _m_methods);
    }

    private int bGa() {
        return ((ScatterWeaponType) getType()).diJ();
    }

    private float bGb() {
        return ((ScatterWeaponType) getType()).diL();
    }

    /* renamed from: iJ */
    private void m11432iJ(float f) {
        throw new C6039afL();
    }

    /* renamed from: pg */
    private void m11433pg(int i) {
        throw new C6039afL();
    }

    /* renamed from: S */
    public void mo10S() {
        super.mo10S();
    }

    /* renamed from: T */
    public C6494any mo11T() {
        return ___iScriptClass;
    }

    /* renamed from: U */
    public C2491fm[] mo12U() {
        return _m_methods;
    }

    /* renamed from: W */
    public Object mo13W() {
        return new C2708iq(this);
    }

    /* renamed from: a */
    public Object mo14a(C0495Gr gr) {
        gr.getArgs();
        switch (gr.mo2417hq() - ProjectileWeapon._m_methodCount) {
            case 0:
                ayw();
                return null;
            default:
                return super.mo14a(gr);
        }
    }

    /* renamed from: a */
    public void mo15a(Collection collection, C0495Gr gr) {
        super.mo15a(collection, gr);
    }

    /* access modifiers changed from: protected */
    public void ayx() {
        switch (bFf().mo6893i(clw)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, clw, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, clw, new Object[0]));
                break;
        }
        ayw();
    }

    /* renamed from: c */
    public C5663aRz[] mo25c() {
        return _m_fields;
    }

    /* renamed from: a */
    public void mo6693a(ScatterWeaponType alz) {
        super.mo5354a((ProjectileWeaponType) alz);
    }

    @C0064Am(aul = "5c85d8223d0d6df85f08fb2c5b1ebe53", aum = 0)
    private void ayw() {
        float f;
        if (!C5877acF.RUNNING_CLIENT_BOT || !bGX()) {
            if (log.isTraceEnabled()) {
                log.trace("Spawning shot");
            }
            float doP = buj().doP();
            if (doP == 0.0f) {
                f = 5.0f;
            } else {
                f = doP;
            }
            Random random = new Random();
            C6661arJ cUn = cUn();
            int i = 0;
            while (true) {
                int i2 = i;
                if (i2 >= bGa()) {
                    break;
                }
                C6348alI ali = new C6348alI(f);
                Orientation e = Orientation.m45809b(ScriptRuntime.NaN, ScriptRuntime.NaN, (double) (random.nextFloat() * 360.0f)).mo24728e(Orientation.m45809b((double) (random.nextFloat() * bGb()), ScriptRuntime.NaN, ScriptRuntime.NaN));
                C5260aCm acm = new C5260aCm(mo5372il().cPK(), aqr(), cUl());
                acm.mo8232w(buj());
                float in = mo12940in();
                Shot cAVar = (Shot) bFf().mo6865M(Shot.class);
                cAVar.mo17514a(acm, in);
                cAVar.mo1085k(cUn.agv());
                Quat4fWrap orientation = cUn.getOrientation();
                if (cUn.afN() == null) {
                    cAVar.setPosition(cUn.getPosition().mo9531q(cUn.getOrientation().mo15209aT(cUn.afV())));
                    cAVar.setOrientation(orientation.mo15233d((Quat4f) e));
                } else {
                    cAVar.setPosition(cUn.getPosition().mo9531q(orientation.mo15209aT(ayv())));
                    cAVar.setOrientation(mo12910ar(cAVar.getPosition()).mo15233d((Quat4f) e));
                }
                cAVar.setSpeed(buj().getSpeed());
                cAVar.mo17511J(aqv());
                cAVar.mo17512L(aqx());
                cAVar.mo999b((C4029yK) ali);
                cAVar.mo1085k(cUn.agv());
                cAVar.mo993b(cUn.azW().mo21606Nc());
                if (log.isDebugEnabled()) {
                    log.debug("Shot Spawned");
                }
                cUn.afE();
                i = i2 + 1;
            }
            if (bGX()) {
                mo12911b(cUn.agv(), cUn.agx());
            }
        }
    }
}
