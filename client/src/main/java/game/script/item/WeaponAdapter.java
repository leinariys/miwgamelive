package game.script.item;

import game.network.message.externalizable.aCE;
import game.script.damage.DamageType;
import game.script.util.GameObjectAdapter;
import logic.baa.*;
import logic.data.mbean.C3976xd;
import logic.res.code.C5663aRz;
import logic.res.code.C6494any;
import logic.res.html.C2491fm;
import p001a.*;

import java.util.Collection;

@C5511aMd
@C6485anp
/* renamed from: a.fx */
/* compiled from: a */
public class WeaponAdapter extends GameObjectAdapter<WeaponAdapter> implements C1616Xf {

    /* renamed from: AI */
    public static final C2491fm f7538AI = null;

    /* renamed from: DJ */
    public static final C2491fm f7539DJ = null;

    /* renamed from: DK */
    public static final C2491fm f7540DK = null;
    public static final int _m_fieldCount = 0;
    public static final C5663aRz[] _m_fields = null;
    public static final int _m_methodCount = 0;
    public static final C2491fm[] _m_methods = null;
    public static final long serialVersionUID = 0;
    /* renamed from: vb */
    public static final C2491fm f7541vb = null;
    /* renamed from: vh */
    public static final C2491fm f7542vh = null;
    public static C6494any ___iScriptClass;

    static {
        m31623V();
    }

    public WeaponAdapter() {
        super((C5540aNg) null);
        super.mo10S();
    }

    public WeaponAdapter(C5540aNg ang) {
        super(ang);
    }

    public WeaponAdapter(C2961mJ mJVar) {
        super((C5540aNg) null);
        super.mo967a(mJVar);
    }

    /* renamed from: V */
    static void m31623V() {
        serialVersionUID = (long) 1;
        _m_fieldCount = GameObjectAdapter._m_fieldCount + 0;
        _m_methodCount = GameObjectAdapter._m_methodCount + 5;
        _m_fields = new C5663aRz[(GameObjectAdapter._m_fieldCount + 0)];
        C1634Xv.m11725a((Object[]) GameObjectAdapter._m_fields, (Object[]) _m_fields);
        int i = GameObjectAdapter._m_methodCount;
        C2491fm[] fmVarArr = new C2491fm[(i + 5)];
        C2491fm a = C4105zY.m41624a(WeaponAdapter.class, "b90c9d0745bf0863ab6970a961c82d4f", i);
        f7539DJ = a;
        fmVarArr[i] = a;
        int i2 = i + 1;
        C2491fm a2 = C4105zY.m41624a(WeaponAdapter.class, "a98a4b0c5dcab927e4d944c812fc3267", i2);
        f7542vh = a2;
        fmVarArr[i2] = a2;
        int i3 = i2 + 1;
        C2491fm a3 = C4105zY.m41624a(WeaponAdapter.class, "c23b9297de198ac1edc22f06e0be1df1", i3);
        f7541vb = a3;
        fmVarArr[i3] = a3;
        int i4 = i3 + 1;
        C2491fm a4 = C4105zY.m41624a(WeaponAdapter.class, "da730adcb816619152b101d403f0ba19", i4);
        f7538AI = a4;
        fmVarArr[i4] = a4;
        int i5 = i4 + 1;
        C2491fm a5 = C4105zY.m41624a(WeaponAdapter.class, "a00cb1f222df77c85cfd199ab0c7b77b", i5);
        f7540DK = a5;
        fmVarArr[i5] = a5;
        int i6 = i5 + 1;
        _m_methods = fmVarArr;
        C1634Xv.m11725a((Object[]) GameObjectAdapter._m_methods, (Object[]) _m_methods);
        ___iScriptClass = aUO.m18566a(WeaponAdapter.class, C3976xd.class, _m_fields, _m_methods);
    }

    /* renamed from: T */
    public C6494any mo11T() {
        return ___iScriptClass;
    }

    /* renamed from: U */
    public C2491fm[] mo12U() {
        return _m_methods;
    }

    /* renamed from: W */
    public Object mo13W() {
        return new C3976xd(this);
    }

    /* renamed from: a */
    public Object mo14a(C0495Gr gr) {
        Object[] args = gr.getArgs();
        switch (gr.mo2417hq() - GameObjectAdapter._m_methodCount) {
            case 0:
                return new Float(m31627mk());
            case 1:
                return new Float(m31626is());
            case 2:
                return new Float(m31625im());
            case 3:
                return new Float(m31624c((DamageType) args[0]));
            case 4:
                return new Boolean(m31628mm());
            default:
                return super.mo14a(gr);
        }
    }

    /* renamed from: a */
    public void mo15a(Collection collection, C0495Gr gr) {
        super.mo15a(collection, gr);
    }

    /* renamed from: c */
    public C5663aRz[] mo25c() {
        return _m_fields;
    }

    /* renamed from: d */
    public float mo5375d(DamageType fr) {
        switch (bFf().mo6893i(f7538AI)) {
            case 0:
                return 0.0f;
            case 2:
                return ((Float) bFf().mo5606d(new aCE(this, f7538AI, new Object[]{fr}))).floatValue();
            case 3:
                bFf().mo5606d(new aCE(this, f7538AI, new Object[]{fr}));
                break;
        }
        return m31624c(fr);
    }

    public float getSpeed() {
        switch (bFf().mo6893i(f7542vh)) {
            case 0:
                return 0.0f;
            case 2:
                return ((Float) bFf().mo5606d(new aCE(this, f7542vh, new Object[0]))).floatValue();
            case 3:
                bFf().mo5606d(new aCE(this, f7542vh, new Object[0]));
                break;
        }
        return m31626is();
    }

    /* renamed from: in */
    public float mo12953in() {
        switch (bFf().mo6893i(f7541vb)) {
            case 0:
                return 0.0f;
            case 2:
                return ((Float) bFf().mo5606d(new aCE(this, f7541vb, new Object[0]))).floatValue();
            case 3:
                bFf().mo5606d(new aCE(this, f7541vb, new Object[0]));
                break;
        }
        return m31625im();
    }

    /* renamed from: ml */
    public float mo12954ml() {
        switch (bFf().mo6893i(f7539DJ)) {
            case 0:
                return 0.0f;
            case 2:
                return ((Float) bFf().mo5606d(new aCE(this, f7539DJ, new Object[0]))).floatValue();
            case 3:
                bFf().mo5606d(new aCE(this, f7539DJ, new Object[0]));
                break;
        }
        return m31627mk();
    }

    /* renamed from: mn */
    public boolean mo12955mn() {
        switch (bFf().mo6893i(f7540DK)) {
            case 0:
                return false;
            case 2:
                return ((Boolean) bFf().mo5606d(new aCE(this, f7540DK, new Object[0]))).booleanValue();
            case 3:
                bFf().mo5606d(new aCE(this, f7540DK, new Object[0]));
                break;
        }
        return m31628mm();
    }

    /* renamed from: S */
    public void mo10S() {
        super.mo10S();
    }

    /* renamed from: a */
    public void mo967a(C2961mJ mJVar) {
        super.mo967a(mJVar);
    }

    @C0064Am(aul = "b90c9d0745bf0863ab6970a961c82d4f", aum = 0)
    /* renamed from: mk */
    private float m31627mk() {
        return ((WeaponAdapter) mo16071IG()).mo12954ml();
    }

    @C0064Am(aul = "a98a4b0c5dcab927e4d944c812fc3267", aum = 0)
    /* renamed from: is */
    private float m31626is() {
        return ((WeaponAdapter) mo16071IG()).getSpeed();
    }

    @C0064Am(aul = "c23b9297de198ac1edc22f06e0be1df1", aum = 0)
    /* renamed from: im */
    private float m31625im() {
        return ((WeaponAdapter) mo16071IG()).mo12953in();
    }

    @C0064Am(aul = "da730adcb816619152b101d403f0ba19", aum = 0)
    /* renamed from: c */
    private float m31624c(DamageType fr) {
        return ((WeaponAdapter) mo16071IG()).mo5375d(fr);
    }

    @C0064Am(aul = "a00cb1f222df77c85cfd199ab0c7b77b", aum = 0)
    /* renamed from: mm */
    private boolean m31628mm() {
        return ((WeaponAdapter) mo16071IG()).mo12955mn();
    }
}
