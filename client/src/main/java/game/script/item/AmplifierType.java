package game.script.item;

import logic.baa.*;
import logic.data.mbean.C2421fH;
import logic.res.code.C5663aRz;
import logic.res.code.C6494any;
import logic.res.html.C2491fm;
import p001a.C0495Gr;
import p001a.C5540aNg;
import p001a.C5829abJ;
import p001a.aUO;

import java.util.Collection;

@C0566Hp
@C5829abJ("2.0.1")
@C6485anp
@C5511aMd
/* renamed from: a.iD */
/* compiled from: a */
public abstract class AmplifierType extends AmplifierBaseType implements C1616Xf {
    public static final int _m_fieldCount = 0;
    public static final C5663aRz[] _m_fields = null;
    public static final int _m_methodCount = 0;
    public static final C2491fm[] _m_methods = null;
    public static final long serialVersionUID = 0;
    public static C6494any ___iScriptClass;

    static {
        m33048V();
    }

    public AmplifierType() {
        super((C5540aNg) null);
        super.mo10S();
    }

    public AmplifierType(C5540aNg ang) {
        super(ang);
    }

    /* renamed from: V */
    static void m33048V() {
        serialVersionUID = (long) 1;
        _m_fieldCount = AmplifierBaseType._m_fieldCount + 0;
        _m_methodCount = AmplifierBaseType._m_methodCount + 0;
        _m_fields = new C5663aRz[(AmplifierBaseType._m_fieldCount + 0)];
        C1634Xv.m11725a((Object[]) AmplifierBaseType._m_fields, (Object[]) _m_fields);
        _m_methods = new C2491fm[(AmplifierBaseType._m_methodCount + 0)];
        C1634Xv.m11725a((Object[]) AmplifierBaseType._m_methods, (Object[]) _m_methods);
        ___iScriptClass = aUO.m18566a(AmplifierType.class, C2421fH.class, _m_fields, _m_methods);
    }

    /* renamed from: T */
    public C6494any mo11T() {
        return ___iScriptClass;
    }

    /* renamed from: U */
    public C2491fm[] mo12U() {
        return _m_methods;
    }

    /* renamed from: W */
    public Object mo13W() {
        return new C2421fH(this);
    }

    /* renamed from: a */
    public Object mo14a(C0495Gr gr) {
        return super.mo14a(gr);
    }

    /* renamed from: a */
    public void mo15a(Collection collection, C0495Gr gr) {
        super.mo15a(collection, gr);
    }

    /* renamed from: c */
    public C5663aRz[] mo25c() {
        return _m_fields;
    }

    /* renamed from: S */
    public void mo10S() {
        super.mo10S();
    }
}
