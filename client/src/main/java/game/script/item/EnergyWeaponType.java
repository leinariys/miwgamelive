package game.script.item;

import game.network.message.externalizable.aCE;
import game.script.resource.Asset;
import logic.baa.*;
import logic.data.mbean.C2805kM;
import logic.res.code.C5640aRc;
import logic.res.code.C5663aRz;
import logic.res.code.C6494any;
import logic.res.html.C2491fm;
import p001a.*;

import java.util.Collection;

@C0566Hp
@C5829abJ("2.1.1")
@C6485anp
@C5511aMd
/* renamed from: a.agc  reason: case insensitive filesystem */
/* compiled from: a */
public class EnergyWeaponType extends WeaponType implements C1616Xf {
    public static final int _m_fieldCount = 0;
    public static final C5663aRz[] _m_fields = null;
    public static final int _m_methodCount = 0;
    public static final C2491fm[] _m_methods = null;
    public static final C5663aRz bLY = null;
    public static final C5663aRz bLZ = null;
    public static final C5663aRz bMa = null;
    public static final C5663aRz bMb = null;
    /* renamed from: dN */
    public static final C2491fm f4525dN = null;
    public static final C2491fm fwo = null;
    public static final C2491fm fwp = null;
    public static final C2491fm fwq = null;
    public static final C2491fm fwr = null;
    public static final C2491fm fws = null;
    public static final C2491fm fwt = null;
    public static final C2491fm fwu = null;
    public static final C2491fm fwv = null;
    public static final C2491fm fww = null;
    public static final long serialVersionUID = 0;
    /* renamed from: uU */
    public static final C5663aRz f4527uU = null;
    /* renamed from: vi */
    public static final C2491fm f4528vi = null;
    /* renamed from: vj */
    public static final C2491fm f4529vj = null;
    public static C6494any ___iScriptClass;
    @C0064Am(aul = "b5d9640168ece5382ba1aa32645030fd", aum = 0)
    private static int auV;
    @C0064Am(aul = "b34beb9c98f23c6d768e91a987f57e6e", aum = 1)
    private static int auW;
    @C0064Am(aul = "431a433bcddba5a80b39b3ad5320b793", aum = 2)
    private static Asset auX;
    @C0064Am(aul = "72bb756c385b81486c2f1d6b17223a63", aum = 3)
    private static float auY;
    @C0064Am(aul = "5c75a391205a11580a1164e813db5207", aum = 4)

    /* renamed from: uT */
    private static String f4526uT;

    static {
        m22065V();
    }

    public EnergyWeaponType() {
        super((C5540aNg) null);
        super.mo10S();
    }

    public EnergyWeaponType(C5540aNg ang) {
        super(ang);
    }

    /* renamed from: V */
    static void m22065V() {
        serialVersionUID = (long) 1;
        _m_fieldCount = WeaponType._m_fieldCount + 5;
        _m_methodCount = WeaponType._m_methodCount + 12;
        int i = WeaponType._m_fieldCount;
        C5663aRz[] arzArr = new C5663aRz[(i + 5)];
        C5663aRz b = C5640aRc.m17844b(EnergyWeaponType.class, "b5d9640168ece5382ba1aa32645030fd", i);
        bLY = b;
        arzArr[i] = b;
        int i2 = i + 1;
        C5663aRz b2 = C5640aRc.m17844b(EnergyWeaponType.class, "b34beb9c98f23c6d768e91a987f57e6e", i2);
        bLZ = b2;
        arzArr[i2] = b2;
        int i3 = i2 + 1;
        C5663aRz b3 = C5640aRc.m17844b(EnergyWeaponType.class, "431a433bcddba5a80b39b3ad5320b793", i3);
        bMa = b3;
        arzArr[i3] = b3;
        int i4 = i3 + 1;
        C5663aRz b4 = C5640aRc.m17844b(EnergyWeaponType.class, "72bb756c385b81486c2f1d6b17223a63", i4);
        bMb = b4;
        arzArr[i4] = b4;
        int i5 = i4 + 1;
        C5663aRz b5 = C5640aRc.m17844b(EnergyWeaponType.class, "5c75a391205a11580a1164e813db5207", i5);
        f4527uU = b5;
        arzArr[i5] = b5;
        int i6 = i5 + 1;
        _m_fields = arzArr;
        C1634Xv.m11725a((Object[]) WeaponType._m_fields, (Object[]) _m_fields);
        int i7 = WeaponType._m_methodCount;
        C2491fm[] fmVarArr = new C2491fm[(i7 + 12)];
        C2491fm a = C4105zY.m41624a(EnergyWeaponType.class, "4aaa0ee188c8d83857466775f2b58e49", i7);
        fwo = a;
        fmVarArr[i7] = a;
        int i8 = i7 + 1;
        C2491fm a2 = C4105zY.m41624a(EnergyWeaponType.class, "db21b932d22ea9bebf2e13331d9b2276", i8);
        fwp = a2;
        fmVarArr[i8] = a2;
        int i9 = i8 + 1;
        C2491fm a3 = C4105zY.m41624a(EnergyWeaponType.class, "43971283279fda977a9b986a9af64d38", i9);
        fwq = a3;
        fmVarArr[i9] = a3;
        int i10 = i9 + 1;
        C2491fm a4 = C4105zY.m41624a(EnergyWeaponType.class, "9fde6317a394269987f74d9405a372f6", i10);
        fwr = a4;
        fmVarArr[i10] = a4;
        int i11 = i10 + 1;
        C2491fm a5 = C4105zY.m41624a(EnergyWeaponType.class, "4939b90a11d392a2df7940ca6fcc9cb2", i11);
        fws = a5;
        fmVarArr[i11] = a5;
        int i12 = i11 + 1;
        C2491fm a6 = C4105zY.m41624a(EnergyWeaponType.class, "791dbd1d2a9f0d4fd085a61e02409fe0", i12);
        fwt = a6;
        fmVarArr[i12] = a6;
        int i13 = i12 + 1;
        C2491fm a7 = C4105zY.m41624a(EnergyWeaponType.class, "6e30fe57fcf47f8c45fb3187dd926c87", i13);
        fwu = a7;
        fmVarArr[i13] = a7;
        int i14 = i13 + 1;
        C2491fm a8 = C4105zY.m41624a(EnergyWeaponType.class, "03f745b3c9378c12b4cf9780ae70fc0f", i14);
        fwv = a8;
        fmVarArr[i14] = a8;
        int i15 = i14 + 1;
        C2491fm a9 = C4105zY.m41624a(EnergyWeaponType.class, "84fa5078b31a5b4812e5416b11d9ca9b", i15);
        f4528vi = a9;
        fmVarArr[i15] = a9;
        int i16 = i15 + 1;
        C2491fm a10 = C4105zY.m41624a(EnergyWeaponType.class, "7eec2ce4c9f9afd1419a4a81cebd6e7c", i16);
        f4529vj = a10;
        fmVarArr[i16] = a10;
        int i17 = i16 + 1;
        C2491fm a11 = C4105zY.m41624a(EnergyWeaponType.class, "ebe178b9b550ff38ca900877c1414c71", i17);
        fww = a11;
        fmVarArr[i17] = a11;
        int i18 = i17 + 1;
        C2491fm a12 = C4105zY.m41624a(EnergyWeaponType.class, "53473eb7a8c278dbdc7af5cb59feb39a", i18);
        f4525dN = a12;
        fmVarArr[i18] = a12;
        int i19 = i18 + 1;
        _m_methods = fmVarArr;
        C1634Xv.m11725a((Object[]) WeaponType._m_methods, (Object[]) _m_methods);
        ___iScriptClass = aUO.m18566a(EnergyWeaponType.class, C2805kM.class, _m_fields, _m_methods);
    }

    /* renamed from: H */
    private void m22063H(String str) {
        bFf().mo5608dq().mo3197f(f4527uU, str);
    }

    /* renamed from: ad */
    private void m22067ad(Asset tCVar) {
        bFf().mo5608dq().mo3197f(bMa, tCVar);
    }

    private int aqa() {
        return bFf().mo5608dq().mo3212n(bLY);
    }

    private int aqb() {
        return bFf().mo5608dq().mo3212n(bLZ);
    }

    private Asset aqc() {
        return (Asset) bFf().mo5608dq().mo3214p(bMa);
    }

    private float aqd() {
        return bFf().mo5608dq().mo3211m(bMb);
    }

    /* renamed from: ea */
    private void m22069ea(float f) {
        bFf().mo5608dq().mo3150a(bMb, f);
    }

    /* renamed from: fB */
    private void m22070fB(int i) {
        bFf().mo5608dq().mo3183b(bLY, i);
    }

    /* renamed from: fC */
    private void m22071fC(int i) {
        bFf().mo5608dq().mo3183b(bLZ, i);
    }

    /* renamed from: ij */
    private String m22072ij() {
        return (String) bFf().mo5608dq().mo3214p(f4527uU);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Energy Shots' Surface Material")
    /* renamed from: N */
    public void mo13429N(String str) {
        switch (bFf().mo6893i(f4529vj)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f4529vj, new Object[]{str}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f4529vj, new Object[]{str}));
                break;
        }
        m22064M(str);
    }

    /* renamed from: T */
    public C6494any mo11T() {
        return ___iScriptClass;
    }

    /* renamed from: U */
    public C2491fm[] mo12U() {
        return _m_methods;
    }

    /* renamed from: W */
    public Object mo13W() {
        return new C2805kM(this);
    }

    /* renamed from: a */
    public Object mo14a(C0495Gr gr) {
        Object[] args = gr.getArgs();
        switch (gr.mo2417hq() - WeaponType._m_methodCount) {
            case 0:
                return new Integer(bVW());
            case 1:
                m22075qA(((Integer) args[0]).intValue());
                return null;
            case 2:
                return new Integer(bVY());
            case 3:
                m22076qC(((Integer) args[0]).intValue());
                return null;
            case 4:
                return bWa();
            case 5:
                m22068bq((Asset) args[0]);
                return null;
            case 6:
                return new Float(bWc());
            case 7:
                m22074jn(((Float) args[0]).floatValue());
                return null;
            case 8:
                return m22073it();
            case 9:
                m22064M((String) args[0]);
                return null;
            case 10:
                return bWe();
            case 11:
                return m22066aT();
            default:
                return super.mo14a(gr);
        }
    }

    /* renamed from: a */
    public void mo15a(Collection collection, C0495Gr gr) {
        super.mo15a(collection, gr);
    }

    /* access modifiers changed from: protected */
    /* renamed from: aU */
    public <T extends aDJ> T mo745aU() {
        switch (bFf().mo6893i(f4525dN)) {
            case 0:
                return null;
            case 2:
                return (aDJ) bFf().mo5606d(new aCE(this, f4525dN, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f4525dN, new Object[0]));
                break;
        }
        return m22066aT();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Max Energy")
    public int bVX() {
        switch (bFf().mo6893i(fwo)) {
            case 0:
                return 0;
            case 2:
                return ((Integer) bFf().mo5606d(new aCE(this, fwo, new Object[0]))).intValue();
            case 3:
                bFf().mo5606d(new aCE(this, fwo, new Object[0]));
                break;
        }
        return bVW();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Energy Per Shot")
    public int bVZ() {
        switch (bFf().mo6893i(fwq)) {
            case 0:
                return 0;
            case 2:
                return ((Integer) bFf().mo5606d(new aCE(this, fwq, new Object[0]))).intValue();
            case 3:
                bFf().mo5606d(new aCE(this, fwq, new Object[0]));
                break;
        }
        return bVY();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Ammo Asset")
    public Asset bWb() {
        switch (bFf().mo6893i(fws)) {
            case 0:
                return null;
            case 2:
                return (Asset) bFf().mo5606d(new aCE(this, fws, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, fws, new Object[0]));
                break;
        }
        return bWa();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Recharge Rate")
    public float bWd() {
        switch (bFf().mo6893i(fwu)) {
            case 0:
                return 0.0f;
            case 2:
                return ((Float) bFf().mo5606d(new aCE(this, fwu, new Object[0]))).floatValue();
            case 3:
                bFf().mo5606d(new aCE(this, fwu, new Object[0]));
                break;
        }
        return bWc();
    }

    public EnergyWeapon bWf() {
        switch (bFf().mo6893i(fww)) {
            case 0:
                return null;
            case 2:
                return (EnergyWeapon) bFf().mo5606d(new aCE(this, fww, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, fww, new Object[0]));
                break;
        }
        return bWe();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Ammo Asset")
    /* renamed from: br */
    public void mo13435br(Asset tCVar) {
        switch (bFf().mo6893i(fwt)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, fwt, new Object[]{tCVar}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, fwt, new Object[]{tCVar}));
                break;
        }
        m22068bq(tCVar);
    }

    /* renamed from: c */
    public C5663aRz[] mo25c() {
        return _m_fields;
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Energy Shots' Surface Material")
    /* renamed from: iu */
    public String mo13436iu() {
        switch (bFf().mo6893i(f4528vi)) {
            case 0:
                return null;
            case 2:
                return (String) bFf().mo5606d(new aCE(this, f4528vi, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f4528vi, new Object[0]));
                break;
        }
        return m22073it();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Recharge Rate")
    /* renamed from: jo */
    public void mo13437jo(float f) {
        switch (bFf().mo6893i(fwv)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, fwv, new Object[]{new Float(f)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, fwv, new Object[]{new Float(f)}));
                break;
        }
        m22074jn(f);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Max Energy")
    /* renamed from: qB */
    public void mo13438qB(int i) {
        switch (bFf().mo6893i(fwp)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, fwp, new Object[]{new Integer(i)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, fwp, new Object[]{new Integer(i)}));
                break;
        }
        m22075qA(i);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Energy Per Shot")
    /* renamed from: qD */
    public void mo13439qD(int i) {
        switch (bFf().mo6893i(fwr)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, fwr, new Object[]{new Integer(i)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, fwr, new Object[]{new Integer(i)}));
                break;
        }
        m22076qC(i);
    }

    /* renamed from: S */
    public void mo10S() {
        super.mo10S();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Max Energy")
    @C0064Am(aul = "4aaa0ee188c8d83857466775f2b58e49", aum = 0)
    private int bVW() {
        return aqa();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Max Energy")
    @C0064Am(aul = "db21b932d22ea9bebf2e13331d9b2276", aum = 0)
    /* renamed from: qA */
    private void m22075qA(int i) {
        m22070fB(i);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Energy Per Shot")
    @C0064Am(aul = "43971283279fda977a9b986a9af64d38", aum = 0)
    private int bVY() {
        return aqb();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Energy Per Shot")
    @C0064Am(aul = "9fde6317a394269987f74d9405a372f6", aum = 0)
    /* renamed from: qC */
    private void m22076qC(int i) {
        m22071fC(i);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Ammo Asset")
    @C0064Am(aul = "4939b90a11d392a2df7940ca6fcc9cb2", aum = 0)
    private Asset bWa() {
        return aqc();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Ammo Asset")
    @C0064Am(aul = "791dbd1d2a9f0d4fd085a61e02409fe0", aum = 0)
    /* renamed from: bq */
    private void m22068bq(Asset tCVar) {
        m22067ad(tCVar);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Recharge Rate")
    @C0064Am(aul = "6e30fe57fcf47f8c45fb3187dd926c87", aum = 0)
    private float bWc() {
        return aqd();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Recharge Rate")
    @C0064Am(aul = "03f745b3c9378c12b4cf9780ae70fc0f", aum = 0)
    /* renamed from: jn */
    private void m22074jn(float f) {
        m22069ea(f);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Energy Shots' Surface Material")
    @C0064Am(aul = "84fa5078b31a5b4812e5416b11d9ca9b", aum = 0)
    /* renamed from: it */
    private String m22073it() {
        return m22072ij();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Energy Shots' Surface Material")
    @C0064Am(aul = "7eec2ce4c9f9afd1419a4a81cebd6e7c", aum = 0)
    /* renamed from: M */
    private void m22064M(String str) {
        m22063H(str);
    }

    @C0064Am(aul = "ebe178b9b550ff38ca900877c1414c71", aum = 0)
    private EnergyWeapon bWe() {
        return (EnergyWeapon) mo745aU();
    }

    @C0064Am(aul = "53473eb7a8c278dbdc7af5cb59feb39a", aum = 0)
    /* renamed from: aT */
    private <T extends aDJ> T m22066aT() {
        T t = (EnergyWeapon) bFf().mo6865M(EnergyWeapon.class);
        t.mo23114a(this);
        return t;
    }
}
