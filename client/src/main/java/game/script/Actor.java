package game.script;

import com.hoplon.geometry.BoundingBox;
import com.hoplon.geometry.Color;
import com.hoplon.geometry.Vec3f;
import game.CollisionFXSet;
import game.geometry.Quat4fWrap;
import game.geometry.TransformWrap;
import game.geometry.Vec3d;
import game.network.message.externalizable.C6950awp;
import game.network.message.externalizable.aCE;
import game.network.message.serializable.C2686iZ;
import game.network.message.serializable.C3901wW;
import game.script.item.Shot;
import game.script.login.UserConnection;
import game.script.mines.Mine;
import game.script.missile.Missile;
import game.script.mission.MissionTrigger;
import game.script.player.Player;
import game.script.ship.Scenery;
import game.script.ship.Ship;
import game.script.ship.Station;
import game.script.simulation.Space;
import game.script.space.*;
import logic.WrapRunnable;
import logic.aaa.C2041bS;
import logic.aaa.C2235dB;
import logic.baa.*;
import logic.bbb.*;
import logic.data.link.C3396rD;
import logic.data.mbean.C2217cs;
import logic.render.IEngineGraphics;
import logic.res.ILoaderTrail;
import logic.res.code.C5640aRc;
import logic.res.code.C5663aRz;
import logic.res.code.C6494any;
import logic.res.html.C2491fm;
import logic.res.sound.C0907NJ;
import logic.thred.C6339akz;
import logic.thred.LogPrinter;
import org.apache.commons.logging.Log;
import org.mozilla1.javascript.ScriptRuntime;
import p001a.*;
import taikodom.geom.Orientation;
import taikodom.infra.script.I18NString;
import taikodom.render.RenderView;
import taikodom.render.enums.GeometryType;
import taikodom.render.helpers.RenderTask;
import taikodom.render.loader.RenderAsset;
import taikodom.render.scene.RLine;
import taikodom.render.scene.RenderObject;
import taikodom.render.scene.Scene;
import taikodom.render.scene.SceneObject;
import taikodom.render.shaders.Shader;
import taikodom.render.shaders.ShaderPass;
import taikodom.render.textures.Material;

import javax.vecmath.Tuple3d;
import javax.vecmath.Tuple3f;
import javax.vecmath.Vector3f;
import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;

@C5829abJ("1.1.0")
@C6485anp
@C5511aMd
/* renamed from: a.CR */
/* compiled from: a */
public abstract class Actor extends TaikodomObject implements C0907NJ, C1616Xf, aOW, C5824abE, C6222aim {

    /* renamed from: Do */
    public static final C2491fm f303Do = null;

    /* renamed from: Lm */
    public static final C2491fm f304Lm = null;

    /* renamed from: Mq */
    public static final C2491fm f305Mq = null;

    /* renamed from: Pf */
    public static final C2491fm f306Pf = null;

    /* renamed from: Pg */
    public static final C2491fm f307Pg = null;

    /* renamed from: Wp */
    public static final C2491fm f308Wp = null;

    /* renamed from: Wu */
    public static final C2491fm f309Wu = null;
    public static final C2491fm _f_dispose_0020_0028_0029V = null;
    /* renamed from: _f_getOffloaders_0020_0028_0029Ljava_002futil_002fCollection_003b */
    public static final C2491fm f310x99c43db3 = null;
    public static final C2491fm _f_hasHollowField_0020_0028_0029Z = null;
    public static final C2491fm _f_onResurrect_0020_0028_0029V = null;
    public static final C2491fm _f_step_0020_0028F_0029V = null;
    public static final C2491fm _f_toString_0020_0028_0029Ljava_002flang_002fString_003b = null;
    public static final int _m_fieldCount = 0;
    public static final C5663aRz[] _m_fields = null;
    public static final int _m_methodCount = 0;
    public static final C2491fm[] _m_methods = null;
    public static final C5663aRz aBo = null;
    public static final C2491fm aBs = null;
    public static final C2491fm aBt = null;
    public static final C2491fm aTe = null;
    public static final C2491fm aTf = null;
    public static final C2491fm aTg = null;
    public static final C2491fm aTh = null;
    public static final C2491fm awo = null;
    public static final C2491fm bcA = null;
    public static final C2491fm bcB = null;
    public static final C2491fm bcC = null;
    public static final C2491fm bcG = null;
    public static final C5663aRz bcu = null;
    public static final C5663aRz bcv = null;
    public static final C2491fm bpB = null;
    public static final C2491fm bqM = null;
    public static final C2491fm bqO = null;
    public static final C2491fm brK = null;
    public static final C2491fm brN = null;
    public static final C2491fm brO = null;
    public static final C2491fm brQ = null;
    public static final C2491fm brR = null;
    public static final C2491fm brS = null;
    public static final C2491fm brY = null;
    public static final C2491fm brq = null;
    public static final C2491fm brr = null;
    public static final C2491fm brs = null;
    public static final C2491fm bsq = null;
    public static final C2491fm cQF = null;
    public static final C2491fm cQG = null;
    public static final C2491fm coQ = null;
    public static final C2491fm dmA = null;
    public static final C2491fm dmF = null;
    public static final C2491fm dmq = null;
    public static final C2491fm dmr = null;
    public static final C2491fm dmu = null;
    public static final C2491fm dmv = null;
    public static final C2491fm dmw = null;
    public static final C2491fm dmz = null;
    public static final C5663aRz eSv = null;
    public static final C2491fm gDT = null;
    public static final C2491fm gDU = null;
    public static final float hjT = 1.41006541E9f;
    public static final long hjU = 10000;
    public static final C5663aRz hjZ = null;
    public static final C2491fm hjl = null;
    public static final C2491fm hkD = null;
    public static final C2491fm hkE = null;
    public static final C2491fm hkF = null;
    public static final C2491fm hkG = null;
    public static final C2491fm hkH = null;
    public static final C2491fm hkI = null;
    public static final C2491fm hkJ = null;
    public static final C2491fm hkK = null;
    public static final C2491fm hkL = null;
    public static final C2491fm hkM = null;
    public static final C2491fm hkN = null;
    public static final C2491fm hkO = null;
    public static final C2491fm hkP = null;
    public static final C2491fm hkQ = null;
    public static final C2491fm hkR = null;
    public static final C2491fm hkS = null;
    public static final C2491fm hkT = null;
    public static final C2491fm hkU = null;
    public static final C2491fm hkV = null;
    public static final C2491fm hkW = null;
    public static final C2491fm hkX = null;
    public static final C2491fm hkY = null;
    public static final C2491fm hkZ = null;
    public static final C5663aRz hka = null;
    public static final C5663aRz hkb = null;
    public static final C5663aRz hke = null;
    public static final C5663aRz hkh = null;
    public static final C5663aRz hkm = null;
    public static final C5663aRz hkn = null;
    public static final C5663aRz hko = null;
    public static final C5663aRz hkq = null;
    public static final C5663aRz hkr = null;
    public static final C5663aRz hkv = null;
    public static final C5663aRz hkw = null;
    public static final C2491fm hlA = null;
    public static final C2491fm hlB = null;
    public static final C2491fm hlC = null;
    public static final C2491fm hlD = null;
    public static final C2491fm hlE = null;
    public static final C2491fm hlF = null;
    public static final C2491fm hlG = null;
    public static final C2491fm hlH = null;
    public static final C2491fm hlI = null;
    public static final C2491fm hlJ = null;
    public static final C2491fm hlK = null;
    public static final C2491fm hlL = null;
    public static final C2491fm hlM = null;
    public static final C2491fm hlN = null;
    public static final C2491fm hlO = null;
    public static final C2491fm hlP = null;
    public static final C2491fm hlQ = null;
    public static final C2491fm hlR = null;
    public static final C2491fm hlS = null;
    public static final C2491fm hlT = null;
    public static final C2491fm hlU = null;
    public static final C2491fm hlV = null;
    public static final C2491fm hlW = null;
    public static final C2491fm hlX = null;
    public static final C2491fm hlY = null;
    public static final C2491fm hlZ = null;
    public static final C2491fm hla = null;
    public static final C2491fm hlb = null;
    public static final C2491fm hlc = null;
    public static final C2491fm hld = null;
    public static final C2491fm hle = null;
    public static final C2491fm hlf = null;
    public static final C2491fm hlg = null;
    public static final C2491fm hlh = null;
    public static final C2491fm hli = null;
    public static final C2491fm hlj = null;
    public static final C2491fm hlk = null;
    public static final C2491fm hll = null;
    public static final C2491fm hlm = null;
    public static final C2491fm hln = null;
    public static final C2491fm hlo = null;
    public static final C2491fm hlp = null;
    public static final C2491fm hlq = null;
    public static final C2491fm hlr = null;
    public static final C2491fm hls = null;
    public static final C2491fm hlt = null;
    public static final C2491fm hlu = null;
    public static final C2491fm hlv = null;
    public static final C2491fm hlw = null;
    public static final C2491fm hlx = null;
    public static final C2491fm hly = null;
    public static final C2491fm hlz = null;
    public static final C2491fm hma = null;
    public static final C2491fm hmb = null;
    public static final C2491fm hmc = null;
    public static final C2491fm hmd = null;
    public static final C2491fm hme = null;
    public static final C2491fm hmf = null;
    public static final C2491fm hmg = null;
    public static final C2491fm hmh = null;
    public static final C2491fm hmi = null;
    public static final C2491fm hmj = null;
    public static final C2491fm hmk = null;
    public static final C2491fm hml = null;
    public static final C2491fm hmm = null;
    public static final C2491fm hmn = null;
    public static final C2491fm hmo = null;
    public static final C2491fm hmp = null;
    public static final C2491fm hmq = null;
    public static final C2491fm hmr = null;
    public static final C2491fm hms = null;
    public static final C2491fm hmt = null;
    public static final C2491fm hmu = null;
    public static final C2491fm hmv = null;
    public static final C2491fm hmw = null;
    public static final C2491fm hmx = null;
    public static final C2491fm hmy = null;
    public static final C2491fm hmz = null;
    /* access modifiers changed from: private */
    public static final Log logger = LogPrinter.setClass(Actor.class);
    public static final long serialVersionUID = 0;
    /* renamed from: uX */
    public static final C2491fm f325uX = null;
    /* renamed from: uY */
    public static final C2491fm f326uY = null;
    /* renamed from: uZ */
    public static final C2491fm f327uZ = null;
    /* renamed from: va */
    public static final C2491fm f328va = null;
    /* renamed from: vc */
    public static final C2491fm f329vc = null;
    /* renamed from: ve */
    public static final C2491fm f330ve = null;
    /* renamed from: vi */
    public static final C2491fm f331vi = null;
    /* renamed from: vk */
    public static final C2491fm f332vk = null;
    /* renamed from: vm */
    public static final C2491fm f333vm = null;
    /* renamed from: vn */
    public static final C2491fm f334vn = null;
    /* renamed from: vo */
    public static final C2491fm f335vo = null;
    /* renamed from: vp */
    public static final C2491fm f336vp = null;
    /* renamed from: vq */
    public static final C2491fm f337vq = null;
    /* renamed from: vr */
    public static final C2491fm f338vr = null;
    /* renamed from: vs */
    public static final C2491fm f339vs = null;
    /* renamed from: vt */
    public static final C2491fm f340vt = null;
    /* renamed from: zU */
    public static final C2491fm f341zU = null;
    private static final boolean hjV = false;
    private static final long hjW = 5000;
    private static final int hjX = 10;
    private static final int hjY = 1000;
    public static C6494any ___iScriptClass = null;
    @C0064Am(aul = "30e07c4a83ade1e327032ad87cee176b", aum = 10)
    @C1020Ot(mo4553Ur = 2000, drz = C1020Ot.C1021a.TIME_BASED)
    @C5256aCi
    private static Quat4fWrap orientation;
    @C0064Am(aul = "edb0e66f936dca77af3aee12ebe04082", aum = 11)
    @C1020Ot(mo4553Ur = 2000, drz = C1020Ot.C1021a.TIME_BASED)
    @C5256aCi
    private static Vec3d position;
    @C0064Am(aul = "18880dbcb97b692a0e0f58790deaa20e", aum = 2)

    /* renamed from: rA */
    private static C2686iZ<Actor> f311rA;
    @C0064Am(aul = "7d187fb42cc298453221b0b97fb66e14", aum = 3)
    @C5256aCi

    /* renamed from: rB */
    private static boolean f312rB;
    @C0064Am(aul = "66f7fbfb45e906eaba15a29293988dce", aum = 4)

    /* renamed from: rC */
    private static boolean f313rC;
    @C0064Am(aul = "c8e87e5dae9bd28c5dd68ed24a212353", aum = 5)
    @C5256aCi

    /* renamed from: rD */
    private static Pawn f314rD;
    @C0064Am(aul = "6950ee21a27c9b33478239361b06012e", aum = 6)
    @C1020Ot(mo4553Ur = 2000, drz = C1020Ot.C1021a.TIME_BASED)
    @C5256aCi

    /* renamed from: rE */
    private static Vec3f f315rE;
    @C0064Am(aul = "643e0f783e741647789a48071c1f3bbd", aum = 7)
    @C1020Ot(mo4553Ur = 2000, drz = C1020Ot.C1021a.TIME_BASED)
    @C5256aCi

    /* renamed from: rF */
    private static Vec3f f316rF;
    @C0064Am(aul = "38997360d48b51df96f7c0b393c0a43d", aum = 8)

    /* renamed from: rG */
    private static I18NString f317rG;
    @C0064Am(aul = "584698af0f9efd8a233180c704ebbf7d", aum = 9)
    @C5256aCi

    /* renamed from: rH */
    private static boolean f318rH;
    @C0064Am(aul = "4eeee5d4e9f83c40debb2bf18bf53646", aum = 12)

    /* renamed from: rI */
    private static StellarSystem f319rI;
    @C0064Am(aul = "5ef7d60da3a8b2ceb8ad24111107bebf", aum = 13)
    @C5256aCi

    /* renamed from: rJ */
    private static boolean f320rJ;
    @C0064Am(aul = "ee970e8a1f16d5b7dee4c99e5eb7ea18", aum = 14)
    @C5256aCi

    /* renamed from: rK */
    private static Node f321rK;
    @C0064Am(aul = "a3458a5b27bf26c0b6b7991dfe0f864d", aum = 15)

    /* renamed from: rL */
    private static Space f322rL;
    @C0064Am(aul = "bf81f67cc27de4cb991f3dcbed63f57e", aum = 0)
    @C1020Ot(drz = C1020Ot.C1021a.NOT_PERSISTED)

    /* renamed from: ry */
    private static float f323ry;
    @C0064Am(aul = "dceb75b2bf8b667232e642770a9fcfba", aum = 1)

    /* renamed from: rz */
    private static Actor f324rz;

    static {
        m1534V();
    }

    @ClientOnly
    public transient C6017aep hkg;
    @ClientOnly
    public transient SceneObject hks;
    @ClientOnly
    public transient SceneObject hkt;
    @ClientOnly
    private float bfM;
    @C3122oB(mo20937Uq = C3122oB.C3123a.NOT_REPLICATED)
    @C1020Ot(drz = C1020Ot.C1021a.NOT_PERSISTED)
    @C5256aCi
    private transient C4029yK cYZ;
    @ClientOnly
    private boolean dEC;
    @C1020Ot(drz = C1020Ot.C1021a.NOT_PERSISTED)
    @ClientOnly
    private float hkA;
    @ClientOnly
    private boolean hkB;
    @ClientOnly
    private RLine hkC;
    @ClientOnly
    private C5473aKr<aDJ, Object> hkc;
    @ClientOnly
    private transient int hkd;
    @ClientOnly
    private C0520HN hkf;
    @ClientOnly
    private transient long hki;
    private transient long hkj;
    private transient Quat4fWrap hkk;
    private transient Vec3d hkl;
    private transient boolean hkp;
    @ClientOnly
    private transient float hku;
    @ClientOnly
    private C1706ZG hkx;
    @ClientOnly
    private WrapRunnable hky;
    @ClientOnly
    private long hkz;

    public Actor() {
        super((C5540aNg) null);
        super.mo10S();
    }

    public Actor(C5540aNg ang) {
        super(ang);
    }

    public Actor(C2961mJ mJVar) {
        super((C5540aNg) null);
        super.mo967a(mJVar);
    }

    /* renamed from: V */
    static void m1534V() {
        serialVersionUID = (long) 1;
        _m_fieldCount = TaikodomObject._m_fieldCount + 16;
        _m_methodCount = TaikodomObject._m_methodCount + 170;
        int i = TaikodomObject._m_fieldCount;
        C5663aRz[] arzArr = new C5663aRz[(i + 16)];
        C5663aRz b = C5640aRc.m17844b(Actor.class, "bf81f67cc27de4cb991f3dcbed63f57e", i);
        hjZ = b;
        arzArr[i] = b;
        int i2 = i + 1;
        C5663aRz b2 = C5640aRc.m17844b(Actor.class, "dceb75b2bf8b667232e642770a9fcfba", i2);
        hka = b2;
        arzArr[i2] = b2;
        int i3 = i2 + 1;
        C5663aRz b3 = C5640aRc.m17844b(Actor.class, "18880dbcb97b692a0e0f58790deaa20e", i3);
        eSv = b3;
        arzArr[i3] = b3;
        int i4 = i3 + 1;
        C5663aRz b4 = C5640aRc.m17844b(Actor.class, "7d187fb42cc298453221b0b97fb66e14", i4);
        hkb = b4;
        arzArr[i4] = b4;
        int i5 = i4 + 1;
        C5663aRz b5 = C5640aRc.m17844b(Actor.class, "66f7fbfb45e906eaba15a29293988dce", i5);
        hke = b5;
        arzArr[i5] = b5;
        int i6 = i5 + 1;
        C5663aRz b6 = C5640aRc.m17844b(Actor.class, "c8e87e5dae9bd28c5dd68ed24a212353", i6);
        hkh = b6;
        arzArr[i6] = b6;
        int i7 = i6 + 1;
        C5663aRz b7 = C5640aRc.m17844b(Actor.class, "6950ee21a27c9b33478239361b06012e", i7);
        hkm = b7;
        arzArr[i7] = b7;
        int i8 = i7 + 1;
        C5663aRz b8 = C5640aRc.m17844b(Actor.class, "643e0f783e741647789a48071c1f3bbd", i8);
        hkn = b8;
        arzArr[i8] = b8;
        int i9 = i8 + 1;
        C5663aRz b9 = C5640aRc.m17844b(Actor.class, "38997360d48b51df96f7c0b393c0a43d", i9);
        hko = b9;
        arzArr[i9] = b9;
        int i10 = i9 + 1;
        C5663aRz b10 = C5640aRc.m17844b(Actor.class, "584698af0f9efd8a233180c704ebbf7d", i10);
        hkq = b10;
        arzArr[i10] = b10;
        int i11 = i10 + 1;
        C5663aRz b11 = C5640aRc.m17844b(Actor.class, "30e07c4a83ade1e327032ad87cee176b", i11);
        hkr = b11;
        arzArr[i11] = b11;
        int i12 = i11 + 1;
        C5663aRz b12 = C5640aRc.m17844b(Actor.class, "edb0e66f936dca77af3aee12ebe04082", i12);
        bcu = b12;
        arzArr[i12] = b12;
        int i13 = i12 + 1;
        C5663aRz b13 = C5640aRc.m17844b(Actor.class, "4eeee5d4e9f83c40debb2bf18bf53646", i13);
        aBo = b13;
        arzArr[i13] = b13;
        int i14 = i13 + 1;
        C5663aRz b14 = C5640aRc.m17844b(Actor.class, "5ef7d60da3a8b2ceb8ad24111107bebf", i14);
        hkv = b14;
        arzArr[i14] = b14;
        int i15 = i14 + 1;
        C5663aRz b15 = C5640aRc.m17844b(Actor.class, "ee970e8a1f16d5b7dee4c99e5eb7ea18", i15);
        hkw = b15;
        arzArr[i15] = b15;
        int i16 = i15 + 1;
        C5663aRz b16 = C5640aRc.m17844b(Actor.class, "a3458a5b27bf26c0b6b7991dfe0f864d", i16);
        bcv = b16;
        arzArr[i16] = b16;
        int i17 = i16 + 1;
        _m_fields = arzArr;
        C1634Xv.m11725a((Object[]) TaikodomObject._m_fields, (Object[]) _m_fields);
        int i18 = TaikodomObject._m_methodCount;
        C2491fm[] fmVarArr = new C2491fm[(i18 + 170)];
        C2491fm a = C4105zY.m41624a(Actor.class, "ab5e7dabab74b0caa5c5007e60c64ad7", i18);
        hkD = a;
        fmVarArr[i18] = a;
        int i19 = i18 + 1;
        C2491fm a2 = C4105zY.m41624a(Actor.class, "0c68706197d451a4bf7bf4cba6ee5dfd", i19);
        brr = a2;
        fmVarArr[i19] = a2;
        int i20 = i19 + 1;
        C2491fm a3 = C4105zY.m41624a(Actor.class, "d71e7f5c542834162d59bc2c0cbca42d", i20);
        hkE = a3;
        fmVarArr[i20] = a3;
        int i21 = i20 + 1;
        C2491fm a4 = C4105zY.m41624a(Actor.class, "8c6a008ad84bd053225e4949aed1a231", i21);
        hkF = a4;
        fmVarArr[i21] = a4;
        int i22 = i21 + 1;
        C2491fm a5 = C4105zY.m41624a(Actor.class, "84f05d65f47768a8b0c84d75f2851a89", i22);
        f333vm = a5;
        fmVarArr[i22] = a5;
        int i23 = i22 + 1;
        C2491fm a6 = C4105zY.m41624a(Actor.class, "9eef3485ac80fae8da9a9611b65e287e", i23);
        hkG = a6;
        fmVarArr[i23] = a6;
        int i24 = i23 + 1;
        C2491fm a7 = C4105zY.m41624a(Actor.class, "a9bfb81c062731fcb34ee0d63bf3f279", i24);
        hkH = a7;
        fmVarArr[i24] = a7;
        int i25 = i24 + 1;
        C2491fm a8 = C4105zY.m41624a(Actor.class, "2e4f47080e95a533f548883e745163f8", i25);
        f340vt = a8;
        fmVarArr[i25] = a8;
        int i26 = i25 + 1;
        C2491fm a9 = C4105zY.m41624a(Actor.class, "08fda20e31483af98da774d57b5c74fb", i26);
        bpB = a9;
        fmVarArr[i26] = a9;
        int i27 = i26 + 1;
        C2491fm a10 = C4105zY.m41624a(Actor.class, "158e2747b2a10645fef45edbcf29d86d", i27);
        f326uY = a10;
        fmVarArr[i27] = a10;
        int i28 = i27 + 1;
        C2491fm a11 = C4105zY.m41624a(Actor.class, "2e19dfd28ec4dc920349483dfffbbeca", i28);
        f334vn = a11;
        fmVarArr[i28] = a11;
        int i29 = i28 + 1;
        C2491fm a12 = C4105zY.m41624a(Actor.class, "043ac92b5a2e90b3caa870494890c31f", i29);
        _f_dispose_0020_0028_0029V = a12;
        fmVarArr[i29] = a12;
        int i30 = i29 + 1;
        C2491fm a13 = C4105zY.m41624a(Actor.class, "9113be1d57614e0684078e63d77a3dfa", i30);
        hkI = a13;
        fmVarArr[i30] = a13;
        int i31 = i30 + 1;
        C2491fm a14 = C4105zY.m41624a(Actor.class, "d2ac2f85b7f3e2c501cd31280acd4d50", i31);
        hkJ = a14;
        fmVarArr[i31] = a14;
        int i32 = i31 + 1;
        C2491fm a15 = C4105zY.m41624a(Actor.class, "17fdd51668c85d58af3e4aff45e1a8e3", i32);
        f337vq = a15;
        fmVarArr[i32] = a15;
        int i33 = i32 + 1;
        C2491fm a16 = C4105zY.m41624a(Actor.class, "446b2c3094b81c4163b99a33e5a69daf", i33);
        hkK = a16;
        fmVarArr[i33] = a16;
        int i34 = i33 + 1;
        C2491fm a17 = C4105zY.m41624a(Actor.class, "9c2ef509ce3eb0c09493a6c34cbb7860", i34);
        hkL = a17;
        fmVarArr[i34] = a17;
        int i35 = i34 + 1;
        C2491fm a18 = C4105zY.m41624a(Actor.class, "63495ab62d04d7db8589527810c7610a", i35);
        hkM = a18;
        fmVarArr[i35] = a18;
        int i36 = i35 + 1;
        C2491fm a19 = C4105zY.m41624a(Actor.class, "44f79aa977cb0801f7eafdc24cc4efcb", i36);
        bcA = a19;
        fmVarArr[i36] = a19;
        int i37 = i36 + 1;
        C2491fm a20 = C4105zY.m41624a(Actor.class, "d7dd6238ba953ab35a7a0f7051a0697c", i37);
        hkN = a20;
        fmVarArr[i37] = a20;
        int i38 = i37 + 1;
        C2491fm a21 = C4105zY.m41624a(Actor.class, "758b859c5bb97ba98c08722a7cd4a508", i38);
        hkO = a21;
        fmVarArr[i38] = a21;
        int i39 = i38 + 1;
        C2491fm a22 = C4105zY.m41624a(Actor.class, "2f73c77fce7fad25cdde733011781569", i39);
        hkP = a22;
        fmVarArr[i39] = a22;
        int i40 = i39 + 1;
        C2491fm a23 = C4105zY.m41624a(Actor.class, "8977249d6fcd08ba4bd1c6106393227d", i40);
        hkQ = a23;
        fmVarArr[i40] = a23;
        int i41 = i40 + 1;
        C2491fm a24 = C4105zY.m41624a(Actor.class, "2972874d0d62f772b8ddc659b0aececc", i41);
        hkR = a24;
        fmVarArr[i41] = a24;
        int i42 = i41 + 1;
        C2491fm a25 = C4105zY.m41624a(Actor.class, "3973e395306065beb941f8b74f05f65e", i42);
        hkS = a25;
        fmVarArr[i42] = a25;
        int i43 = i42 + 1;
        C2491fm a26 = C4105zY.m41624a(Actor.class, "4e5df5f1bf7acc432ea9f1681d58c1d8", i43);
        hkT = a26;
        fmVarArr[i43] = a26;
        int i44 = i43 + 1;
        C2491fm a27 = C4105zY.m41624a(Actor.class, "90ad6d89de22000472ff4d53681c1fcd", i44);
        hkU = a27;
        fmVarArr[i44] = a27;
        int i45 = i44 + 1;
        C2491fm a28 = C4105zY.m41624a(Actor.class, "e63bf092e0f67286bcd2db1e96aa0787", i45);
        hkV = a28;
        fmVarArr[i45] = a28;
        int i46 = i45 + 1;
        C2491fm a29 = C4105zY.m41624a(Actor.class, "cafb43d9b89caa1d5324607852f9c610", i46);
        hkW = a29;
        fmVarArr[i46] = a29;
        int i47 = i46 + 1;
        C2491fm a30 = C4105zY.m41624a(Actor.class, "25524b7915496930f55a846b63388883", i47);
        hkX = a30;
        fmVarArr[i47] = a30;
        int i48 = i47 + 1;
        C2491fm a31 = C4105zY.m41624a(Actor.class, "9525a92efc7f051004d704ae3d7a0eac", i48);
        hkY = a31;
        fmVarArr[i48] = a31;
        int i49 = i48 + 1;
        C2491fm a32 = C4105zY.m41624a(Actor.class, "684506448de994d5784b98fff4acb394", i49);
        hkZ = a32;
        fmVarArr[i49] = a32;
        int i50 = i49 + 1;
        C2491fm a33 = C4105zY.m41624a(Actor.class, "e9163c6f9a033ea2aaa0c6de979aac85", i50);
        hla = a33;
        fmVarArr[i50] = a33;
        int i51 = i50 + 1;
        C2491fm a34 = C4105zY.m41624a(Actor.class, "ee0dd8d60dab8728d881484339c19223", i51);
        dmu = a34;
        fmVarArr[i51] = a34;
        int i52 = i51 + 1;
        C2491fm a35 = C4105zY.m41624a(Actor.class, "57330e52407f5615fdcdb82059dbc3c5", i52);
        dmv = a35;
        fmVarArr[i52] = a35;
        int i53 = i52 + 1;
        C2491fm a36 = C4105zY.m41624a(Actor.class, "eebbb815edb9de1cfecf6ccf23707ba9", i53);
        hlb = a36;
        fmVarArr[i53] = a36;
        int i54 = i53 + 1;
        C2491fm a37 = C4105zY.m41624a(Actor.class, "ffcdddb1ceb6a6f8ee65577c81edd4dc", i54);
        aTe = a37;
        fmVarArr[i54] = a37;
        int i55 = i54 + 1;
        C2491fm a38 = C4105zY.m41624a(Actor.class, "95d58756e985b38878579ccb0fde0272", i55);
        aTf = a38;
        fmVarArr[i55] = a38;
        int i56 = i55 + 1;
        C2491fm a39 = C4105zY.m41624a(Actor.class, "51efcd87430f850af9603c792eafb977", i56);
        aTg = a39;
        fmVarArr[i56] = a39;
        int i57 = i56 + 1;
        C2491fm a40 = C4105zY.m41624a(Actor.class, "c65104f18429142b59e3438c7f2682eb", i57);
        aTh = a40;
        fmVarArr[i57] = a40;
        int i58 = i57 + 1;
        C2491fm a41 = C4105zY.m41624a(Actor.class, "d4ab3e57b56733fbefa848beb877ea70", i58);
        dmw = a41;
        fmVarArr[i58] = a41;
        int i59 = i58 + 1;
        C2491fm a42 = C4105zY.m41624a(Actor.class, "b7f47c949e7cedda01aeb5ea834819ed", i59);
        f306Pf = a42;
        fmVarArr[i59] = a42;
        int i60 = i59 + 1;
        C2491fm a43 = C4105zY.m41624a(Actor.class, "d99600991f86fefca50640fedba5ae27", i60);
        brK = a43;
        fmVarArr[i60] = a43;
        int i61 = i60 + 1;
        C2491fm a44 = C4105zY.m41624a(Actor.class, "6649c13802cb082c30416931f222144f", i61);
        hlc = a44;
        fmVarArr[i61] = a44;
        int i62 = i61 + 1;
        C2491fm a45 = C4105zY.m41624a(Actor.class, "110ada56bcb3aff91e87434682752da4", i62);
        f341zU = a45;
        fmVarArr[i62] = a45;
        int i63 = i62 + 1;
        C2491fm a46 = C4105zY.m41624a(Actor.class, "cf4ac236276ae2107d553ae0f91e252d", i63);
        hld = a46;
        fmVarArr[i63] = a46;
        int i64 = i63 + 1;
        C2491fm a47 = C4105zY.m41624a(Actor.class, "f6832587c00b8edb3078ba3d9252118b", i64);
        bcC = a47;
        fmVarArr[i64] = a47;
        int i65 = i64 + 1;
        C2491fm a48 = C4105zY.m41624a(Actor.class, "08b6c24ea50747fbde0a517086861e20", i65);
        hle = a48;
        fmVarArr[i65] = a48;
        int i66 = i65 + 1;
        C2491fm a49 = C4105zY.m41624a(Actor.class, "30f9a4f5116854d7babfaa1c68cdb788", i66);
        hlf = a49;
        fmVarArr[i66] = a49;
        int i67 = i66 + 1;
        C2491fm a50 = C4105zY.m41624a(Actor.class, "996be2fe6a78eb0f13e95295c92d6220", i67);
        f329vc = a50;
        fmVarArr[i67] = a50;
        int i68 = i67 + 1;
        C2491fm a51 = C4105zY.m41624a(Actor.class, "ddde60b589d3721e442fa5466d256d53", i68);
        f330ve = a51;
        fmVarArr[i68] = a51;
        int i69 = i68 + 1;
        C2491fm a52 = C4105zY.m41624a(Actor.class, "f57c3fbfc1dba36bfaaacad845c3bb74", i69);
        f335vo = a52;
        fmVarArr[i69] = a52;
        int i70 = i69 + 1;
        C2491fm a53 = C4105zY.m41624a(Actor.class, "81842d9bf0f5ace91dfffb9b1c6dffcc", i70);
        hlg = a53;
        fmVarArr[i70] = a53;
        int i71 = i70 + 1;
        C2491fm a54 = C4105zY.m41624a(Actor.class, "5fc86dc267891806f8738dc2bf287cbb", i71);
        hlh = a54;
        fmVarArr[i71] = a54;
        int i72 = i71 + 1;
        C2491fm a55 = C4105zY.m41624a(Actor.class, "6a91187a4e8e4a80240ccf447497749d", i72);
        hli = a55;
        fmVarArr[i72] = a55;
        int i73 = i72 + 1;
        C2491fm a56 = C4105zY.m41624a(Actor.class, "4f042ec0f08a894e4c4e0d383d6fe31b", i73);
        hlj = a56;
        fmVarArr[i73] = a56;
        int i74 = i73 + 1;
        C2491fm a57 = C4105zY.m41624a(Actor.class, "d853ae5b9bcd3f07142708b3c7b51a97", i74);
        hlk = a57;
        fmVarArr[i74] = a57;
        int i75 = i74 + 1;
        C2491fm a58 = C4105zY.m41624a(Actor.class, "1555b75712594ba62312ed9ad588b570", i75);
        aBs = a58;
        fmVarArr[i75] = a58;
        int i76 = i75 + 1;
        C2491fm a59 = C4105zY.m41624a(Actor.class, "6406c5736d27b192609d506e6bcb8e50", i76);
        aBt = a59;
        fmVarArr[i76] = a59;
        int i77 = i76 + 1;
        C2491fm a60 = C4105zY.m41624a(Actor.class, "222cbc6b66be770b9e1134835273403e", i77);
        hll = a60;
        fmVarArr[i77] = a60;
        int i78 = i77 + 1;
        C2491fm a61 = C4105zY.m41624a(Actor.class, "2de65395000667a4756043bffef798a9", i78);
        f331vi = a61;
        fmVarArr[i78] = a61;
        int i79 = i78 + 1;
        C2491fm a62 = C4105zY.m41624a(Actor.class, "989a4247e5cfb1e152a9e212684416d1", i79);
        hlm = a62;
        fmVarArr[i79] = a62;
        int i80 = i79 + 1;
        C2491fm a63 = C4105zY.m41624a(Actor.class, "8192ebd1a146b03b042a02f59f3e459b", i80);
        bcG = a63;
        fmVarArr[i80] = a63;
        int i81 = i80 + 1;
        C2491fm a64 = C4105zY.m41624a(Actor.class, "b035d157ea8c337eb0d63431aaa3e363", i81);
        hln = a64;
        fmVarArr[i81] = a64;
        int i82 = i81 + 1;
        C2491fm a65 = C4105zY.m41624a(Actor.class, "6da6ab6c366b06ba03a2878dd414c8cc", i82);
        awo = a65;
        fmVarArr[i82] = a65;
        int i83 = i82 + 1;
        C2491fm a66 = C4105zY.m41624a(Actor.class, "dc5614b7f8c77b98c9dddd24befafc22", i83);
        hlo = a66;
        fmVarArr[i83] = a66;
        int i84 = i83 + 1;
        C2491fm a67 = C4105zY.m41624a(Actor.class, "7f1904cf9dc542b34b28046124a3d483", i84);
        hlp = a67;
        fmVarArr[i84] = a67;
        int i85 = i84 + 1;
        C2491fm a68 = C4105zY.m41624a(Actor.class, "c63a2c7832eb2e56148c2b0d45b1cc06", i85);
        hlq = a68;
        fmVarArr[i85] = a68;
        int i86 = i85 + 1;
        C2491fm a69 = C4105zY.m41624a(Actor.class, "25b25c96daea0a77b4728227ad26ca95", i86);
        hlr = a69;
        fmVarArr[i86] = a69;
        int i87 = i86 + 1;
        C2491fm a70 = C4105zY.m41624a(Actor.class, "7924b24659970bc9828efc0052654a08", i87);
        hls = a70;
        fmVarArr[i87] = a70;
        int i88 = i87 + 1;
        C2491fm a71 = C4105zY.m41624a(Actor.class, "32bf87581e9c99611eb09d8a0e21c22f", i88);
        hlt = a71;
        fmVarArr[i88] = a71;
        int i89 = i88 + 1;
        C2491fm a72 = C4105zY.m41624a(Actor.class, "d780edbadbb1b833fd5c757e238103ef", i89);
        hlu = a72;
        fmVarArr[i89] = a72;
        int i90 = i89 + 1;
        C2491fm a73 = C4105zY.m41624a(Actor.class, "b70bed4f41a90af57f324ad803f87583", i90);
        hlv = a73;
        fmVarArr[i90] = a73;
        int i91 = i90 + 1;
        C2491fm a74 = C4105zY.m41624a(Actor.class, "15b3fac6863fade80b3851989c5ac499", i91);
        hlw = a74;
        fmVarArr[i91] = a74;
        int i92 = i91 + 1;
        C2491fm a75 = C4105zY.m41624a(Actor.class, "926b3cf528a22260a7666255599f9c48", i92);
        hlx = a75;
        fmVarArr[i92] = a75;
        int i93 = i92 + 1;
        C2491fm a76 = C4105zY.m41624a(Actor.class, "1b89c3eb8dc4ec3bb4f721193bd9d582", i93);
        hly = a76;
        fmVarArr[i93] = a76;
        int i94 = i93 + 1;
        C2491fm a77 = C4105zY.m41624a(Actor.class, "a610f101fff0504aa68d6b7da771c0e6", i94);
        hlz = a77;
        fmVarArr[i94] = a77;
        int i95 = i94 + 1;
        C2491fm a78 = C4105zY.m41624a(Actor.class, "c21f0ba6371a9adfdf9d7b53e466f06d", i95);
        hlA = a78;
        fmVarArr[i95] = a78;
        int i96 = i95 + 1;
        C2491fm a79 = C4105zY.m41624a(Actor.class, "524ce9269c9dd540ec3732f287cb383a", i96);
        dmz = a79;
        fmVarArr[i96] = a79;
        int i97 = i96 + 1;
        C2491fm a80 = C4105zY.m41624a(Actor.class, "69a6ac4cec17840625f70f1142a9febf", i97);
        dmA = a80;
        fmVarArr[i97] = a80;
        int i98 = i97 + 1;
        C2491fm a81 = C4105zY.m41624a(Actor.class, "3a0616dff6367f0f4abb6975a1da823b", i98);
        cQG = a81;
        fmVarArr[i98] = a81;
        int i99 = i98 + 1;
        C2491fm a82 = C4105zY.m41624a(Actor.class, "0b375d05b58ca7ad0eee054d4ea6509a", i99);
        hlB = a82;
        fmVarArr[i99] = a82;
        int i100 = i99 + 1;
        C2491fm a83 = C4105zY.m41624a(Actor.class, "fc2e3aafdd515fdf9b75e1a7d231aafc", i100);
        hlC = a83;
        fmVarArr[i100] = a83;
        int i101 = i100 + 1;
        C2491fm a84 = C4105zY.m41624a(Actor.class, "6dbc2f15252ca3be308de9581aa0065d", i101);
        hlD = a84;
        fmVarArr[i101] = a84;
        int i102 = i101 + 1;
        C2491fm a85 = C4105zY.m41624a(Actor.class, "e20ea1ec9f73a52cac6732f12be4ad69", i102);
        dmr = a85;
        fmVarArr[i102] = a85;
        int i103 = i102 + 1;
        C2491fm a86 = C4105zY.m41624a(Actor.class, "fc11662772ed4d3cc37112a41b35d214", i103);
        gDT = a86;
        fmVarArr[i103] = a86;
        int i104 = i103 + 1;
        C2491fm a87 = C4105zY.m41624a(Actor.class, "a25fc60c9daad6b1a9614a56adc0bce7", i104);
        gDU = a87;
        fmVarArr[i104] = a87;
        int i105 = i104 + 1;
        C2491fm a88 = C4105zY.m41624a(Actor.class, "0fbff0ad81302ac84856a1c68783b9d1", i105);
        f325uX = a88;
        fmVarArr[i105] = a88;
        int i106 = i105 + 1;
        C2491fm a89 = C4105zY.m41624a(Actor.class, "2865a5bdfab225d257ae3ddff1a00de2", i106);
        hlE = a89;
        fmVarArr[i106] = a89;
        int i107 = i106 + 1;
        C2491fm a90 = C4105zY.m41624a(Actor.class, "6922361ca76be0ae2471f7cc29872adc", i107);
        bqM = a90;
        fmVarArr[i107] = a90;
        int i108 = i107 + 1;
        C2491fm a91 = C4105zY.m41624a(Actor.class, "d2ae1653eb77397d51e1ffec85044c42", i108);
        f338vr = a91;
        fmVarArr[i108] = a91;
        int i109 = i108 + 1;
        C2491fm a92 = C4105zY.m41624a(Actor.class, "bf766335daf3747cd194b07702c90511", i109);
        _f_onResurrect_0020_0028_0029V = a92;
        fmVarArr[i109] = a92;
        int i110 = i109 + 1;
        C2491fm a93 = C4105zY.m41624a(Actor.class, "7aac1158ca865bda82cbe4a11a7bb0a7", i110);
        f339vs = a93;
        fmVarArr[i110] = a93;
        int i111 = i110 + 1;
        C2491fm a94 = C4105zY.m41624a(Actor.class, "679826008219ae8df09676b943d3898d", i111);
        bqO = a94;
        fmVarArr[i111] = a94;
        int i112 = i111 + 1;
        C2491fm a95 = C4105zY.m41624a(Actor.class, "41fd612518cf661c7d76096d0008b1a3", i112);
        brs = a95;
        fmVarArr[i112] = a95;
        int i113 = i112 + 1;
        C2491fm a96 = C4105zY.m41624a(Actor.class, "6d4cdfe5d0b31c8b98d611bed2dc678d", i113);
        hjl = a96;
        fmVarArr[i113] = a96;
        int i114 = i113 + 1;
        C2491fm a97 = C4105zY.m41624a(Actor.class, "c7538b323f9be482a5a11d48009841bf", i114);
        hlF = a97;
        fmVarArr[i114] = a97;
        int i115 = i114 + 1;
        C2491fm a98 = C4105zY.m41624a(Actor.class, "a8d6c9e74c106b88321cde9e3fdb9cd0", i115);
        hlG = a98;
        fmVarArr[i115] = a98;
        int i116 = i115 + 1;
        C2491fm a99 = C4105zY.m41624a(Actor.class, "39bfa99708740c8a70a57df60f21435a", i116);
        brN = a99;
        fmVarArr[i116] = a99;
        int i117 = i116 + 1;
        C2491fm a100 = C4105zY.m41624a(Actor.class, "6d8333f699af0939976a835bab870477", i117);
        hlH = a100;
        fmVarArr[i117] = a100;
        int i118 = i117 + 1;
        C2491fm a101 = C4105zY.m41624a(Actor.class, "993e24837f8214f8ca2df0c6996572ab", i118);
        hlI = a101;
        fmVarArr[i118] = a101;
        int i119 = i118 + 1;
        C2491fm a102 = C4105zY.m41624a(Actor.class, "755485cf1fda57b2c3dcd54b29b83cf0", i119);
        hlJ = a102;
        fmVarArr[i119] = a102;
        int i120 = i119 + 1;
        C2491fm a103 = C4105zY.m41624a(Actor.class, "b5b4086db53626d30fda904b64b35c7d", i120);
        f327uZ = a103;
        fmVarArr[i120] = a103;
        int i121 = i120 + 1;
        C2491fm a104 = C4105zY.m41624a(Actor.class, "f867d8b5c5c512a10e19a6e82ded2602", i121);
        hlK = a104;
        fmVarArr[i121] = a104;
        int i122 = i121 + 1;
        C2491fm a105 = C4105zY.m41624a(Actor.class, "4961bdc5a637de9ec06054f3dcd31e7e", i122);
        f304Lm = a105;
        fmVarArr[i122] = a105;
        int i123 = i122 + 1;
        C2491fm a106 = C4105zY.m41624a(Actor.class, "f6113d59159b9c66cbc1f8331f63b25c", i123);
        f332vk = a106;
        fmVarArr[i123] = a106;
        int i124 = i123 + 1;
        C2491fm a107 = C4105zY.m41624a(Actor.class, "18a77410d3c6805b828f85020c308d2d", i124);
        hlL = a107;
        fmVarArr[i124] = a107;
        int i125 = i124 + 1;
        C2491fm a108 = C4105zY.m41624a(Actor.class, "06a8a4026b47f9eef28c8fb736b1986f", i125);
        brO = a108;
        fmVarArr[i125] = a108;
        int i126 = i125 + 1;
        C2491fm a109 = C4105zY.m41624a(Actor.class, "8015dc979aa9491ba51ad69e9969cf4c", i126);
        brQ = a109;
        fmVarArr[i126] = a109;
        int i127 = i126 + 1;
        C2491fm a110 = C4105zY.m41624a(Actor.class, C3396rD.bbr, i127);
        hlM = a110;
        fmVarArr[i127] = a110;
        int i128 = i127 + 1;
        C2491fm a111 = C4105zY.m41624a(Actor.class, "5e18b9ed05691bc13c1f0e9fa7edde8a", i128);
        hlN = a111;
        fmVarArr[i128] = a111;
        int i129 = i128 + 1;
        C2491fm a112 = C4105zY.m41624a(Actor.class, "7e9c2c5a0dd26ef439cdb2a473ea9ac0", i129);
        hlO = a112;
        fmVarArr[i129] = a112;
        int i130 = i129 + 1;
        C2491fm a113 = C4105zY.m41624a(Actor.class, "7309d149af175a0c160d48f772475628", i130);
        hlP = a113;
        fmVarArr[i130] = a113;
        int i131 = i130 + 1;
        C2491fm a114 = C4105zY.m41624a(Actor.class, "c2da08faa65fabb5fcf667f9cb98ad51", i131);
        hlQ = a114;
        fmVarArr[i131] = a114;
        int i132 = i131 + 1;
        C2491fm a115 = C4105zY.m41624a(Actor.class, "e07e47d71b130d3e2ea86f84f54f22c9", i132);
        hlR = a115;
        fmVarArr[i132] = a115;
        int i133 = i132 + 1;
        C2491fm a116 = C4105zY.m41624a(Actor.class, "89a7ffcf6f8ac65ee98a160c3d5bf9f7", i133);
        hlS = a116;
        fmVarArr[i133] = a116;
        int i134 = i133 + 1;
        C2491fm a117 = C4105zY.m41624a(Actor.class, "031ada94ec3add03f30093b7bf2e548c", i134);
        hlT = a117;
        fmVarArr[i134] = a117;
        int i135 = i134 + 1;
        C2491fm a118 = C4105zY.m41624a(Actor.class, "a2e8807706aeb4a574ecaaa9e66763d1", i135);
        hlU = a118;
        fmVarArr[i135] = a118;
        int i136 = i135 + 1;
        C2491fm a119 = C4105zY.m41624a(Actor.class, "4a39e321ee4a7bdf44f8c36ac84bb85a", i136);
        hlV = a119;
        fmVarArr[i136] = a119;
        int i137 = i136 + 1;
        C2491fm a120 = C4105zY.m41624a(Actor.class, "40a1de632b91abea61600a5f7f32ff29", i137);
        hlW = a120;
        fmVarArr[i137] = a120;
        int i138 = i137 + 1;
        C2491fm a121 = C4105zY.m41624a(Actor.class, "d350ec6e7d24a0ae73e825b39e62199c", i138);
        hlX = a121;
        fmVarArr[i138] = a121;
        int i139 = i138 + 1;
        C2491fm a122 = C4105zY.m41624a(Actor.class, "b71e64a77efe6e5736ae1e92ea4c136b", i139);
        hlY = a122;
        fmVarArr[i139] = a122;
        int i140 = i139 + 1;
        C2491fm a123 = C4105zY.m41624a(Actor.class, "e1ba20feb567020ec89af9e4a5760036", i140);
        hlZ = a123;
        fmVarArr[i140] = a123;
        int i141 = i140 + 1;
        C2491fm a124 = C4105zY.m41624a(Actor.class, "609b619e1f30fde710df836b49568c84", i141);
        hma = a124;
        fmVarArr[i141] = a124;
        int i142 = i141 + 1;
        C2491fm a125 = C4105zY.m41624a(Actor.class, "fcd875a8f6db027acd39ae45c0bdf668", i142);
        dmF = a125;
        fmVarArr[i142] = a125;
        int i143 = i142 + 1;
        C2491fm a126 = C4105zY.m41624a(Actor.class, "5c62b0ee1fdaba4639d3e1447bbf2d9b", i143);
        f307Pg = a126;
        fmVarArr[i143] = a126;
        int i144 = i143 + 1;
        C2491fm a127 = C4105zY.m41624a(Actor.class, "70ff3e303614203b4e217c1817386c97", i144);
        hmb = a127;
        fmVarArr[i144] = a127;
        int i145 = i144 + 1;
        C2491fm a128 = C4105zY.m41624a(Actor.class, "904cc6682051b05bf4393303a74c32a3", i145);
        hmc = a128;
        fmVarArr[i145] = a128;
        int i146 = i145 + 1;
        C2491fm a129 = C4105zY.m41624a(Actor.class, "abeca4f5d4e04c783048ea0be88c98ee", i146);
        f336vp = a129;
        fmVarArr[i146] = a129;
        int i147 = i146 + 1;
        C2491fm a130 = C4105zY.m41624a(Actor.class, "9eaf6eb5b5741d9a1c589b63bc796cd5", i147);
        f328va = a130;
        fmVarArr[i147] = a130;
        int i148 = i147 + 1;
        C2491fm a131 = C4105zY.m41624a(Actor.class, "66dde3204e37b52b97470d4e8d367d97", i148);
        hmd = a131;
        fmVarArr[i148] = a131;
        int i149 = i148 + 1;
        C2491fm a132 = C4105zY.m41624a(Actor.class, "dad5ccb7a229dc82032128e418fb267e", i149);
        cQF = a132;
        fmVarArr[i149] = a132;
        int i150 = i149 + 1;
        C2491fm a133 = C4105zY.m41624a(Actor.class, "6a8a490edc583706011450fd76112c60", i150);
        hme = a133;
        fmVarArr[i150] = a133;
        int i151 = i150 + 1;
        C2491fm a134 = C4105zY.m41624a(Actor.class, "7abbe2da51e612b00ad5bfc3016f85c7", i151);
        dmq = a134;
        fmVarArr[i151] = a134;
        int i152 = i151 + 1;
        C2491fm a135 = C4105zY.m41624a(Actor.class, "a29389f67b879665b0d4c69d8d6db2d9", i152);
        hmf = a135;
        fmVarArr[i152] = a135;
        int i153 = i152 + 1;
        C2491fm a136 = C4105zY.m41624a(Actor.class, "e5c9536c272fd7f282d61451b709db0d", i153);
        hmg = a136;
        fmVarArr[i153] = a136;
        int i154 = i153 + 1;
        C2491fm a137 = C4105zY.m41624a(Actor.class, "7b4a4572b6bc69b63c2805c75d2e7d02", i154);
        hmh = a137;
        fmVarArr[i154] = a137;
        int i155 = i154 + 1;
        C2491fm a138 = C4105zY.m41624a(Actor.class, "43a086848cbdef0d8dfdf25f9bfe4b8e", i155);
        hmi = a138;
        fmVarArr[i155] = a138;
        int i156 = i155 + 1;
        C2491fm a139 = C4105zY.m41624a(Actor.class, "ec3fcf699d662100e7b0857d07864809", i156);
        hmj = a139;
        fmVarArr[i156] = a139;
        int i157 = i156 + 1;
        C2491fm a140 = C4105zY.m41624a(Actor.class, "ca325e076a73479af42063b733e040b4", i157);
        bsq = a140;
        fmVarArr[i157] = a140;
        int i158 = i157 + 1;
        C2491fm a141 = C4105zY.m41624a(Actor.class, "5f1f9b9c1b5d6a0cf73aff49bfe7a411", i158);
        hmk = a141;
        fmVarArr[i158] = a141;
        int i159 = i158 + 1;
        C2491fm a142 = C4105zY.m41624a(Actor.class, "dd78647e651e43d5a4b30e6f45c383ce", i159);
        hml = a142;
        fmVarArr[i159] = a142;
        int i160 = i159 + 1;
        C2491fm a143 = C4105zY.m41624a(Actor.class, "89cb9d23e071fef7b0ae0318d43240c5", i160);
        f309Wu = a143;
        fmVarArr[i160] = a143;
        int i161 = i160 + 1;
        C2491fm a144 = C4105zY.m41624a(Actor.class, "a35c72e6b921e71aace4582c0b59b821", i161);
        hmm = a144;
        fmVarArr[i161] = a144;
        int i162 = i161 + 1;
        C2491fm a145 = C4105zY.m41624a(Actor.class, "9acade43cede0d05d6a6dd7cc99b723e", i162);
        hmn = a145;
        fmVarArr[i162] = a145;
        int i163 = i162 + 1;
        C2491fm a146 = C4105zY.m41624a(Actor.class, "ba1033fb150299c1cf8f1fa1cfec899f", i163);
        _f_toString_0020_0028_0029Ljava_002flang_002fString_003b = a146;
        fmVarArr[i163] = a146;
        int i164 = i163 + 1;
        C2491fm a147 = C4105zY.m41624a(Actor.class, "ad659d86dedbfa4646807b375225e5a1", i164);
        hmo = a147;
        fmVarArr[i164] = a147;
        int i165 = i164 + 1;
        C2491fm a148 = C4105zY.m41624a(Actor.class, "534fe3c19f6946019e347b985e958d10", i165);
        hmp = a148;
        fmVarArr[i165] = a148;
        int i166 = i165 + 1;
        C2491fm a149 = C4105zY.m41624a(Actor.class, "1cedc09b919916134519e29ebbaaf9ac", i166);
        coQ = a149;
        fmVarArr[i166] = a149;
        int i167 = i166 + 1;
        C2491fm a150 = C4105zY.m41624a(Actor.class, "e4f7ff8b14a774a996c1af2f29b8ef43", i167);
        f305Mq = a150;
        fmVarArr[i167] = a150;
        int i168 = i167 + 1;
        C2491fm a151 = C4105zY.m41624a(Actor.class, "29abf23504e8d217fc6ea8dff2378012", i168);
        bcB = a151;
        fmVarArr[i168] = a151;
        int i169 = i168 + 1;
        C2491fm a152 = C4105zY.m41624a(Actor.class, "b0a3d9b7cc67028181cf415c4c9dff77", i169);
        brq = a152;
        fmVarArr[i169] = a152;
        int i170 = i169 + 1;
        C2491fm a153 = C4105zY.m41624a(Actor.class, "2fe4da14aa5711b949fd7b99f5f39f04", i170);
        hmq = a153;
        fmVarArr[i170] = a153;
        int i171 = i170 + 1;
        C2491fm a154 = C4105zY.m41624a(Actor.class, "b6da00e7a80fd4abaaec3a216c5fb21a", i171);
        hmr = a154;
        fmVarArr[i171] = a154;
        int i172 = i171 + 1;
        C2491fm a155 = C4105zY.m41624a(Actor.class, "d820cef5781910dcecf8aa47042058e4", i172);
        f308Wp = a155;
        fmVarArr[i172] = a155;
        int i173 = i172 + 1;
        C2491fm a156 = C4105zY.m41624a(Actor.class, "7a9cb380313c83df5cc3e5cd385d8d4f", i173);
        f303Do = a156;
        fmVarArr[i173] = a156;
        int i174 = i173 + 1;
        C2491fm a157 = C4105zY.m41624a(Actor.class, "f67e1cf9aeda95383b258365e9e7c2e7", i174);
        _f_step_0020_0028F_0029V = a157;
        fmVarArr[i174] = a157;
        int i175 = i174 + 1;
        C2491fm a158 = C4105zY.m41624a(Actor.class, "263bc71d60bcb849cda6325e35b8aff2", i175);
        hms = a158;
        fmVarArr[i175] = a158;
        int i176 = i175 + 1;
        C2491fm a159 = C4105zY.m41624a(Actor.class, "13e36d07a38dd87928e3610e2e23c0f9", i176);
        hmt = a159;
        fmVarArr[i176] = a159;
        int i177 = i176 + 1;
        C2491fm a160 = C4105zY.m41624a(Actor.class, "715c5ecfaff763c9608250f64e32cab4", i177);
        hmu = a160;
        fmVarArr[i177] = a160;
        int i178 = i177 + 1;
        C2491fm a161 = C4105zY.m41624a(Actor.class, "203fa362d89e6f984647b2e69876a7cf", i178);
        brR = a161;
        fmVarArr[i178] = a161;
        int i179 = i178 + 1;
        C2491fm a162 = C4105zY.m41624a(Actor.class, "5aa6e89a3b544ac7a62fd79cbf45ef5d", i179);
        brS = a162;
        fmVarArr[i179] = a162;
        int i180 = i179 + 1;
        C2491fm a163 = C4105zY.m41624a(Actor.class, "b33a60c40682a1fcabfce1d08d5bea57", i180);
        brY = a163;
        fmVarArr[i180] = a163;
        int i181 = i180 + 1;
        C2491fm a164 = C4105zY.m41624a(Actor.class, "c5711b3d12470104654260b5b5233155", i181);
        hmv = a164;
        fmVarArr[i181] = a164;
        int i182 = i181 + 1;
        C2491fm a165 = C4105zY.m41624a(Actor.class, "67022f70bad168d44d1e2d47875a5500", i182);
        hmw = a165;
        fmVarArr[i182] = a165;
        int i183 = i182 + 1;
        C2491fm a166 = C4105zY.m41624a(Actor.class, "aa41f206cf35c3bf6a86bc9d9cddf78e", i183);
        hmx = a166;
        fmVarArr[i183] = a166;
        int i184 = i183 + 1;
        C2491fm a167 = C4105zY.m41624a(Actor.class, "c5de637463acce39a3c962323fa28039", i184);
        hmy = a167;
        fmVarArr[i184] = a167;
        int i185 = i184 + 1;
        C2491fm a168 = C4105zY.m41624a(Actor.class, "39671a114e902fc60e1b6aa605af4537", i185);
        f310x99c43db3 = a168;
        fmVarArr[i185] = a168;
        int i186 = i185 + 1;
        C2491fm a169 = C4105zY.m41624a(Actor.class, "72ed7de63e15af1e91aa23632131714d", i186);
        _f_hasHollowField_0020_0028_0029Z = a169;
        fmVarArr[i186] = a169;
        int i187 = i186 + 1;
        C2491fm a170 = C4105zY.m41624a(Actor.class, "58297b99c8b5b0c9694e839df9a12a71", i187);
        hmz = a170;
        fmVarArr[i187] = a170;
        int i188 = i187 + 1;
        _m_methods = fmVarArr;
        C1634Xv.m11725a((Object[]) TaikodomObject._m_methods, (Object[]) _m_methods);
        ___iScriptClass = aUO.m18566a(Actor.class, C2217cs.class, _m_fields, _m_methods);
    }

    /* renamed from: MX */
    private StellarSystem m1531MX() {
        return (StellarSystem) bFf().mo5608dq().mo3214p(aBo);
    }

    /* renamed from: YX */
    private Vec3d m1539YX() {
        return (Vec3d) bFf().mo5608dq().mo3214p(bcu);
    }

    /* renamed from: YY */
    private Space m1540YY() {
        return (Space) bFf().mo5608dq().mo3214p(bcv);
    }

    @C0064Am(aul = "7aac1158ca865bda82cbe4a11a7bb0a7", aum = 0)
    /* renamed from: a */
    private void m1547a(C0200h hVar) {
        throw new aWi(new aCE(this, f339vs, new Object[]{hVar}));
    }

    @C4034yP
    @C5566aOg
    @C0064Am(aul = "84f05d65f47768a8b0c84d75f2851a89", aum = 0)
    @C2499fr
    /* renamed from: a */
    private void m1548a(Actor cr, C5260aCm acm, Vec3f vec3f, Vec3f vec3f2) {
        throw new aWi(new aCE(this, f333vm, new Object[]{cr, acm, vec3f, vec3f2}));
    }

    /* renamed from: a */
    private void m1550a(Space ea) {
        bFf().mo5608dq().mo3197f(bcv, ea);
    }

    @C4034yP
    @C5566aOg
    @C0064Am(aul = "67022f70bad168d44d1e2d47875a5500", aum = 0)
    @C2499fr(mo18857qh = 2)
    /* renamed from: a */
    private void m1554a(C1003Om om, Actor cr, C5260aCm acm, Vec3f vec3f, Vec3f vec3f2) {
        throw new aWi(new aCE(this, hmw, new Object[]{om, cr, acm, vec3f, vec3f2}));
    }

    /* renamed from: ag */
    private void m1562ag(C2686iZ iZVar) {
        bFf().mo5608dq().mo3197f(eSv, iZVar);
    }

    /* renamed from: al */
    private boolean m1565al(Vec3d ajr) {
        switch (bFf().mo6893i(hmf)) {
            case 0:
                return false;
            case 2:
                return ((Boolean) bFf().mo5606d(new aCE(this, hmf, new Object[]{ajr}))).booleanValue();
            case 3:
                bFf().mo5606d(new aCE(this, hmf, new Object[]{ajr}));
                break;
        }
        return m1564ak(ajr);
    }

    @C0064Am(aul = "5c62b0ee1fdaba4639d3e1447bbf2d9b", aum = 0)
    @C5566aOg
    /* renamed from: aq */
    private void m1567aq(String str) {
        throw new aWi(new aCE(this, f307Pg, new Object[]{str}));
    }

    /* renamed from: b */
    private Vec3d m1570b(Vec3d ajr, Vec3d ajr2, Vec3d ajr3) {
        switch (bFf().mo6893i(hkH)) {
            case 0:
                return null;
            case 2:
                return (Vec3d) bFf().mo5606d(new aCE(this, hkH, new Object[]{ajr, ajr2, ajr3}));
            case 3:
                bFf().mo5606d(new aCE(this, hkH, new Object[]{ajr, ajr2, ajr3}));
                break;
        }
        return m1544a(ajr, ajr2, ajr3);
    }

    @C0064Am(aul = "29abf23504e8d217fc6ea8dff2378012", aum = 0)
    @C5566aOg
    /* renamed from: b */
    private void m1571b(Space ea) {
        throw new aWi(new aCE(this, bcB, new Object[]{ea}));
    }

    /* renamed from: bi */
    private void m1580bi(Vec3f vec3f) {
        bFf().mo5608dq().mo3197f(hkm, vec3f);
    }

    /* renamed from: bj */
    private void m1581bj(Vec3f vec3f) {
        bFf().mo5608dq().mo3197f(hkn, vec3f);
    }

    /* renamed from: bk */
    private boolean m1584bk(Actor cr) {
        switch (bFf().mo6893i(hjl)) {
            case 0:
                return false;
            case 2:
                return ((Boolean) bFf().mo5606d(new aCE(this, hjl, new Object[]{cr}))).booleanValue();
            case 3:
                bFf().mo5606d(new aCE(this, hjl, new Object[]{cr}));
                break;
        }
        return m1582bj(cr);
    }

    /* renamed from: bt */
    private void m1589bt(Actor cr) {
        bFf().mo5608dq().mo3197f(hka, cr);
    }

    @C0064Am(aul = "2e19dfd28ec4dc920349483dfffbbeca", aum = 0)
    @C5566aOg
    /* renamed from: c */
    private void m1597c(Actor cr, C5260aCm acm, Vec3f vec3f, Vec3f vec3f2) {
        throw new aWi(new aCE(this, f334vn, new Object[]{cr, acm, vec3f, vec3f2}));
    }

    /* renamed from: c */
    private void m1598c(StellarSystem jj) {
        bFf().mo5608dq().mo3197f(aBo, jj);
    }

    @C4034yP
    @C5566aOg
    @C0064Am(aul = "aa41f206cf35c3bf6a86bc9d9cddf78e", aum = 0)
    @C2499fr(mo18857qh = 2)
    /* renamed from: c */
    private void m1599c(C1003Om om) {
        throw new aWi(new aCE(this, hmx, new Object[]{om}));
    }

    /* renamed from: c */
    private void m1600c(C1706ZG.C1707a aVar) {
        switch (bFf().mo6893i(hlI)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, hlI, new Object[]{aVar}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, hlI, new Object[]{aVar}));
                break;
        }
        m1572b(aVar);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Position")
    @C0064Am(aul = "abeca4f5d4e04c783048ea0be88c98ee", aum = 0)
    @C5566aOg
    /* renamed from: c */
    private void m1601c(Vec3d ajr) {
        throw new aWi(new aCE(this, f336vp, new Object[]{ajr}));
    }

    private float cKK() {
        return bFf().mo5608dq().mo3211m(hjZ);
    }

    private Actor cKL() {
        return (Actor) bFf().mo5608dq().mo3214p(hka);
    }

    private C2686iZ cKM() {
        return (C2686iZ) bFf().mo5608dq().mo3214p(eSv);
    }

    private boolean cKN() {
        return bFf().mo5608dq().mo3201h(hkb);
    }

    private boolean cKO() {
        return bFf().mo5608dq().mo3201h(hke);
    }

    private Pawn cKP() {
        return (Pawn) bFf().mo5608dq().mo3214p(hkh);
    }

    private Vec3f cKQ() {
        return (Vec3f) bFf().mo5608dq().mo3214p(hkm);
    }

    private Vec3f cKR() {
        return (Vec3f) bFf().mo5608dq().mo3214p(hkn);
    }

    private I18NString cKS() {
        return (I18NString) bFf().mo5608dq().mo3214p(hko);
    }

    private boolean cKT() {
        return bFf().mo5608dq().mo3201h(hkq);
    }

    private Quat4fWrap cKU() {
        return (Quat4fWrap) bFf().mo5608dq().mo3214p(hkr);
    }

    private boolean cKV() {
        return bFf().mo5608dq().mo3201h(hkv);
    }

    private Node cKW() {
        return (Node) bFf().mo5608dq().mo3214p(hkw);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "NameI")
    private I18NString cLA() {
        switch (bFf().mo6893i(hlc)) {
            case 0:
                return null;
            case 2:
                return (I18NString) bFf().mo5606d(new aCE(this, hlc, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, hlc, new Object[0]));
                break;
        }
        return cLz();
    }

    private boolean cLP() {
        switch (bFf().mo6893i(hlo)) {
            case 0:
                return false;
            case 2:
                return ((Boolean) bFf().mo5606d(new aCE(this, hlo, new Object[0]))).booleanValue();
            case 3:
                bFf().mo5606d(new aCE(this, hlo, new Object[0]));
                break;
        }
        return cLO();
    }

    private boolean cME() {
        switch (bFf().mo6893i(hmt)) {
            case 0:
                return false;
            case 2:
                return ((Boolean) bFf().mo5606d(new aCE(this, hmt, new Object[0]))).booleanValue();
            case 3:
                bFf().mo5606d(new aCE(this, hmt, new Object[0]));
                break;
        }
        return cMD();
    }

    @C0064Am(aul = "58297b99c8b5b0c9694e839df9a12a71", aum = 0)
    @C5566aOg
    @C2499fr
    private void cMK() {
        throw new aWi(new aCE(this, hmz, new Object[0]));
    }

    private void cMg() {
        switch (bFf().mo6893i(hlG)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, hlG, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, hlG, new Object[0]));
                break;
        }
        cMf();
    }

    /* access modifiers changed from: private */
    @ClientOnly
    public void cMi() {
        switch (bFf().mo6893i(hlH)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, hlH, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, hlH, new Object[0]));
                break;
        }
        cMh();
    }

    @C0064Am(aul = "dd78647e651e43d5a4b30e6f45c383ce", aum = 0)
    @C6580apg
    private void cMv() {
        bFf().mo5600a((Class<? extends C4062yl>) C3396rD.C3398b.class, (C0495Gr) new aCE(this, hml, new Object[0]));
    }

    @C6580apg
    private void cMw() {
        switch (bFf().mo6893i(hml)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, hml, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, hml, new Object[0]));
                break;
        }
        cMv();
    }

    @C0064Am(aul = "ad659d86dedbfa4646807b375225e5a1", aum = 0)
    @C6580apg
    private void cMx() {
        bFf().mo5600a((Class<? extends C4062yl>) C3396rD.C3397a.class, (C0495Gr) new aCE(this, hmo, new Object[0]));
    }

    @C6580apg
    private void cMy() {
        switch (bFf().mo6893i(hmo)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, hmo, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, hmo, new Object[0]));
                break;
        }
        cMx();
    }

    @C4034yP
    @C5566aOg
    @C0064Am(aul = "89cb9d23e071fef7b0ae0318d43240c5", aum = 0)
    @C2499fr(mo18857qh = 2)
    /* renamed from: e */
    private void m1608e(Actor cr, C5260aCm acm, Vec3f vec3f, Vec3f vec3f2) {
        throw new aWi(new aCE(this, f309Wu, new Object[]{cr, acm, vec3f, vec3f2}));
    }

    /* renamed from: e */
    private void m1610e(C1706ZG.C1707a aVar) {
        switch (bFf().mo6893i(hlJ)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, hlJ, new Object[]{aVar}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, hlJ, new Object[]{aVar}));
                break;
        }
        m1606d(aVar);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "NameI")
    @C0064Am(aul = "110ada56bcb3aff91e87434682752da4", aum = 0)
    @C5566aOg
    /* renamed from: e */
    private void m1611e(I18NString i18NString) {
        throw new aWi(new aCE(this, f341zU, new Object[]{i18NString}));
    }

    /* renamed from: h */
    private void m1615h(Quat4fWrap aoy) {
        bFf().mo5608dq().mo3197f(hkr, aoy);
    }

    /* renamed from: hT */
    private void m1617hT(boolean z) {
        bFf().mo5608dq().mo3153a(hkb, z);
    }

    /* renamed from: hU */
    private void m1618hU(boolean z) {
        bFf().mo5608dq().mo3153a(hke, z);
    }

    /* renamed from: hV */
    private void m1619hV(boolean z) {
        bFf().mo5608dq().mo3153a(hkq, z);
    }

    /* renamed from: hW */
    private void m1620hW(boolean z) {
        bFf().mo5608dq().mo3153a(hkv, z);
    }

    /* renamed from: i */
    private void m1625i(Pawn avi) {
        bFf().mo5608dq().mo3197f(hkh, avi);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Enable Fog of war")
    @C0064Am(aul = "d350ec6e7d24a0ae73e825b39e62199c", aum = 0)
    @C5566aOg
    /* renamed from: ib */
    private void m1627ib(boolean z) {
        throw new aWi(new aCE(this, hlX, new Object[]{new Boolean(z)}));
    }

    @C0064Am(aul = "996be2fe6a78eb0f13e95295c92d6220", aum = 0)
    @ClientOnly
    /* renamed from: io */
    private String m1629io() {
        throw new aWi(new aCE(this, f329vc, new Object[0]));
    }

    @C0064Am(aul = "ddde60b589d3721e442fa5466d256d53", aum = 0)
    @ClientOnly
    /* renamed from: iq */
    private String m1630iq() {
        throw new aWi(new aCE(this, f330ve, new Object[0]));
    }

    @C0064Am(aul = "2de65395000667a4756043bffef798a9", aum = 0)
    /* renamed from: it */
    private String m1631it() {
        throw new aWi(new aCE(this, f331vi, new Object[0]));
    }

    @C0064Am(aul = "f57c3fbfc1dba36bfaaacad845c3bb74", aum = 0)
    @ClientOnly
    /* renamed from: iz */
    private String m1633iz() {
        throw new aWi(new aCE(this, f335vo, new Object[0]));
    }

    /* renamed from: l */
    private void m1638l(Node rPVar) {
        bFf().mo5608dq().mo3197f(hkw, rPVar);
    }

    /* renamed from: lq */
    private void m1640lq(float f) {
        bFf().mo5608dq().mo3150a(hjZ, f);
    }

    /* renamed from: m */
    private Vec3d[] m1646m(Vec3d ajr, Vec3d ajr2) {
        switch (bFf().mo6893i(hlm)) {
            case 0:
                return null;
            case 2:
                return (Vec3d[]) bFf().mo5606d(new aCE(this, hlm, new Object[]{ajr, ajr2}));
            case 3:
                bFf().mo5606d(new aCE(this, hlm, new Object[]{ajr, ajr2}));
                break;
        }
        return m1639l(ajr, ajr2);
    }

    /* renamed from: n */
    private void m1647n(Collection collection, C0495Gr gr) {
        Iterator it = collection.iterator();
        while (it.hasNext()) {
            ((C3396rD.C3398b) it.next()).mo21556ah(this);
        }
    }

    /* renamed from: nA */
    private void m1648nA(I18NString i18NString) {
        bFf().mo5608dq().mo3197f(hko, i18NString);
    }

    /* renamed from: o */
    private void m1649o(Collection collection, C0495Gr gr) {
        Iterator it = collection.iterator();
        while (it.hasNext()) {
            ((C3396rD.C3397a) it.next()).mo18227m(this);
        }
    }

    @C0064Am(aul = "4961bdc5a637de9ec06054f3dcd31e7e", aum = 0)
    @C5566aOg
    /* renamed from: qU */
    private void m1650qU() {
        throw new aWi(new aCE(this, f304Lm, new Object[0]));
    }

    /* renamed from: r */
    private void m1651r(Vec3d ajr) {
        bFf().mo5608dq().mo3197f(bcu, ajr);
    }

    @ClientOnly
    /* renamed from: Fe */
    public void mo957Fe() {
        switch (bFf().mo6893i(bqM)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, bqM, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, bqM, new Object[0]));
                break;
        }
        ahp();
    }

    /* renamed from: IL */
    public final C4029yK mo958IL() {
        switch (bFf().mo6893i(hlj)) {
            case 0:
                return null;
            case 2:
                return (C4029yK) bFf().mo5606d(new aCE(this, hlj, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, hlj, new Object[0]));
                break;
        }
        return cLH();
    }

    /* renamed from: Ls */
    public boolean mo959Ls() {
        switch (bFf().mo6893i(awo)) {
            case 0:
                return false;
            case 2:
                return ((Boolean) bFf().mo5606d(new aCE(this, awo, new Object[0]))).booleanValue();
            case 3:
                bFf().mo5606d(new aCE(this, awo, new Object[0]));
                break;
        }
        return m1530Lr();
    }

    /* renamed from: Nc */
    public StellarSystem mo960Nc() {
        switch (bFf().mo6893i(aBs)) {
            case 0:
                return null;
            case 2:
                return (StellarSystem) bFf().mo5606d(new aCE(this, aBs, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, aBs, new Object[0]));
                break;
        }
        return m1532Nb();
    }

    @ClientOnly
    /* renamed from: QW */
    public boolean mo961QW() {
        switch (bFf().mo6893i(_f_hasHollowField_0020_0028_0029Z)) {
            case 0:
                return false;
            case 2:
                return ((Boolean) bFf().mo5606d(new aCE(this, _f_hasHollowField_0020_0028_0029Z, new Object[0]))).booleanValue();
            case 3:
                bFf().mo5606d(new aCE(this, _f_hasHollowField_0020_0028_0029Z, new Object[0]));
                break;
        }
        return m1533QV();
    }

    /* renamed from: T */
    public C6494any mo11T() {
        return ___iScriptClass;
    }

    /* renamed from: U */
    public C2491fm[] mo12U() {
        return _m_methods;
    }

    /* renamed from: VF */
    public float mo962VF() {
        switch (bFf().mo6893i(aTe)) {
            case 0:
                return 0.0f;
            case 2:
                return ((Float) bFf().mo5606d(new aCE(this, aTe, new Object[0]))).floatValue();
            case 3:
                bFf().mo5606d(new aCE(this, aTe, new Object[0]));
                break;
        }
        return m1535VE();
    }

    /* renamed from: VH */
    public float mo963VH() {
        switch (bFf().mo6893i(aTf)) {
            case 0:
                return 0.0f;
            case 2:
                return ((Float) bFf().mo5606d(new aCE(this, aTf, new Object[0]))).floatValue();
            case 3:
                bFf().mo5606d(new aCE(this, aTf, new Object[0]));
                break;
        }
        return m1536VG();
    }

    /* renamed from: W */
    public Object mo13W() {
        return new C2217cs(this);
    }

    @C4034yP
    @C2499fr
    @C1253SX
    /* renamed from: W */
    public void mo964W(float f) {
        switch (bFf().mo6893i(hlW)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, hlW, new Object[]{new Float(f)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, hlW, new Object[]{new Float(f)}));
                break;
        }
        m1641lr(f);
    }

    /* renamed from: Zc */
    public Space mo965Zc() {
        switch (bFf().mo6893i(bcA)) {
            case 0:
                return null;
            case 2:
                return (Space) bFf().mo5606d(new aCE(this, bcA, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, bcA, new Object[0]));
                break;
        }
        return m1541Zb();
    }

    /* renamed from: Zi */
    public float mo966Zi() {
        switch (bFf().mo6893i(bcG)) {
            case 0:
                return 0.0f;
            case 2:
                return ((Float) bFf().mo5606d(new aCE(this, bcG, new Object[0]))).floatValue();
            case 3:
                bFf().mo5606d(new aCE(this, bcG, new Object[0]));
                break;
        }
        return m1543Zh();
    }

    /* renamed from: a */
    public Object mo14a(C0495Gr gr) {
        Object[] args = gr.getArgs();
        switch (gr.mo2417hq() - TaikodomObject._m_methodCount) {
            case 0:
                return cKX();
            case 1:
                m1609e((Space) args[0], ((Long) args[1]).longValue());
                return null;
            case 2:
                m1623i((Space) args[0], ((Long) args[1]).longValue());
                return null;
            case 3:
                return new Boolean(m1602c(((Boolean) args[0]).booleanValue(), (String) args[1]));
            case 4:
                m1548a((Actor) args[0], (C5260aCm) args[1], (Vec3f) args[2], (Vec3f) args[3]);
                return null;
            case 5:
                m1614g((Actor) args[0], (C5260aCm) args[1], (Vec3f) args[2], (Vec3f) args[3]);
                return null;
            case 6:
                return m1544a((Vec3d) args[0], (Vec3d) args[1], (Vec3d) args[2]);
            case 7:
                m1555a((Vec3d) args[0], (Quat4fWrap) args[1]);
                return null;
            case 8:
                afF();
                return null;
            case 9:
                return m1596c((Space) args[0], ((Long) args[1]).longValue());
            case 10:
                m1597c((Actor) args[0], (C5260aCm) args[1], (Vec3f) args[2], (Vec3f) args[3]);
                return null;
            case 11:
                m1613fg();
                return null;
            case 12:
                return new Float(m1590bu((Actor) args[0]));
            case 13:
                return new Float(m1592bw((Actor) args[0]));
            case 14:
                m1545a(((Integer) args[0]).intValue(), ((Long) args[1]).longValue(), (Collection<C6625aqZ>) (Collection) args[2], (C6705asB) args[3]);
                return null;
            case 15:
                return cKY();
            case 16:
                return cLa();
            case 17:
                return cLc();
            case 18:
                return m1541Zb();
            case 19:
                return cLe();
            case 20:
                return cLg();
            case 21:
                return cLi();
            case 22:
                return cLk();
            case 23:
                return new Float(cLm());
            case 24:
                return new Float(cLn());
            case 25:
                return new Float(cLo());
            case 26:
                return new Float(cLp());
            case 27:
                return cLq();
            case 28:
                return new Float(m1594by((Actor) args[0]));
            case 29:
                return new Float(m1575bA((Actor) args[0]));
            case 30:
                return new Boolean(cLr());
            case 31:
                return cLt();
            case 32:
                return cLv();
            case 33:
                return aZV();
            case 34:
                return aZW();
            case 35:
                return cLx();
            case 36:
                return new Float(m1535VE());
            case 37:
                return new Float(m1536VG());
            case 38:
                return new Float(m1537VI());
            case 39:
                return new Float(m1538VJ());
            case 40:
                return new Float(aZX());
            case 41:
                return m1653uV();
            case 42:
                return ahP();
            case 43:
                return cLz();
            case 44:
                m1611e((I18NString) args[0]);
                return null;
            case 45:
                return cLB();
            case 46:
                return m1542Zd();
            case 47:
                m1573b((Vec3f) args[0], (Vec3d) args[1]);
                return null;
            case 48:
                m1634j((Vec3d) args[0], (Vec3d) args[1]);
                return null;
            case 49:
                return m1629io();
            case 50:
                return m1630iq();
            case 51:
                return m1633iz();
            case 52:
                return cLC();
            case 53:
                return cLD();
            case 54:
                return cLF();
            case 55:
                return cLH();
            case 56:
                return cLI();
            case 57:
                return m1532Nb();
            case 58:
                m1605d((StellarSystem) args[0]);
                return null;
            case 59:
                return cLK();
            case 60:
                return m1631it();
            case 61:
                return m1639l((Vec3d) args[0], (Vec3d) args[1]);
            case 62:
                return new Float(m1543Zh());
            case 63:
                return new Boolean(cLM());
            case 64:
                return new Boolean(m1530Lr());
            case 65:
                return new Boolean(cLO());
            case 66:
                return cLQ();
            case 67:
                return cLS();
            case 68:
                return cLU();
            case 69:
                return cLW();
            case 70:
                m1583bk((Vec3f) args[0]);
                return null;
            case 71:
                m1585bm((Vec3f) args[0]);
                return null;
            case 72:
                m1624i((Quat4fWrap) args[0]);
                return null;
            case 73:
                m1561ag((Vec3d) args[0]);
                return null;
            case 74:
                m1621hX(((Boolean) args[0]).booleanValue());
                return null;
            case 75:
                return new Boolean(cLY());
            case 76:
                return new Boolean(cMa());
            case 77:
                return new Boolean(cMc());
            case 78:
                return new Boolean(bad());
            case 79:
                return new Boolean(baf());
            case 80:
                return new Boolean(aMM());
            case 81:
                return new Boolean(m1616h((UserConnection) args[0]));
            case 82:
                m1622hZ(((Boolean) args[0]).booleanValue());
                return null;
            case 83:
                m1607d((C4029yK) args[0]);
                return null;
            case 84:
                m1612fP(((Float) args[0]).floatValue());
                return null;
            case 85:
                cxD();
                return null;
            case 86:
                cxF();
                return null;
            case 87:
                m1549a((Actor) args[0], (Vec3f) args[1], (Vec3f) args[2], ((Float) args[3]).floatValue(), (C6339akz) args[4]);
                return null;
            case 88:
                m1636jJ(((Long) args[0]).longValue());
                return null;
            case 89:
                ahp();
                return null;
            case 90:
                m1626iB();
                return null;
            case 91:
                m1560aG();
                return null;
            case 92:
                m1547a((C0200h) args[0]);
                return null;
            case 93:
                ahs();
                return null;
            case 94:
                m1579bZ((String) args[0]);
                return null;
            case 95:
                return new Boolean(m1582bj((Actor) args[0]));
            case 96:
                cMd();
                return null;
            case 97:
                cMf();
                return null;
            case 98:
                ahT();
                return null;
            case 99:
                cMh();
                return null;
            case 100:
                m1572b((C1706ZG.C1707a) args[0]);
                return null;
            case 101:
                m1606d((C1706ZG.C1707a) args[0]);
                return null;
            case 102:
                m1546a(((Long) args[0]).longValue(), ((Boolean) args[1]).booleanValue());
                return null;
            case 103:
                cMj();
                return null;
            case 104:
                m1650qU();
                return null;
            case 105:
                return new Boolean(m1632iv());
            case 106:
                return new Boolean(cMl());
            case 107:
                ahV();
                return null;
            case 108:
                ahZ();
                return null;
            case 109:
                return new Boolean(m1645m((C1722ZT<UserConnection>) (C1722ZT) args[0]));
            case 110:
                return m1586bo((Vec3f) args[0]);
            case 111:
                return m1587bq((Vec3f) args[0]);
            case 112:
                m1577bC((Actor) args[0]);
                return null;
            case 113:
                m1588bs((Vec3f) args[0]);
                return null;
            case 114:
                m1591bu((Vec3f) args[0]);
                return null;
            case 115:
                m1637k((Quat4fWrap) args[0]);
                return null;
            case 116:
                m1556a((Vec3d) args[0], (Quat4fWrap) args[1], (Vec3f) args[2], (Vec3f) args[3]);
                return null;
            case 117:
                m1563ai((Vec3d) args[0]);
                return null;
            case 118:
                m1652u(((Float) args[0]).floatValue(), ((Float) args[1]).floatValue(), ((Float) args[2]).floatValue());
                return null;
            case 119:
                m1641lr(((Float) args[0]).floatValue());
                return null;
            case 120:
                m1627ib(((Boolean) args[0]).booleanValue());
                return null;
            case 121:
                m1635j((Pawn) args[0]);
                return null;
            case 122:
                m1593bw((Vec3f) args[0]);
                return null;
            case 123:
                m1595by((Vec3f) args[0]);
                return null;
            case 124:
                m1574b((RenderAsset) args[0]);
                return null;
            case 125:
                m1567aq((String) args[0]);
                return null;
            case 126:
                m1628id(((Boolean) args[0]).booleanValue());
                return null;
            case 127:
                m1644m((Quat4fWrap) args[0]);
                return null;
            case 128:
                m1601c((Vec3d) args[0]);
                return null;
            case 129:
                m1558a((C4029yK) args[0]);
                return null;
            case 130:
                m1557a((Vec3d) args[0], (Quat4fWrap) args[1], (Vec3f) args[2], (Vec3f) args[3], ((Float) args[4]).floatValue(), ((Float) args[5]).floatValue(), ((Float) args[6]).floatValue(), ((Float) args[7]).floatValue(), ((Long) args[8]).longValue(), ((Boolean) args[9]).booleanValue());
                return null;
            case 131:
                m1603cF(((Boolean) args[0]).booleanValue());
                return null;
            case 132:
                m1642ls(((Float) args[0]).floatValue());
                return null;
            case 133:
                m1604cU(((Boolean) args[0]).booleanValue());
                return null;
            case 134:
                return new Boolean(m1564ak((Vec3d) args[0]));
            case 135:
                return new Boolean(cMn());
            case 136:
                return new Boolean(m1559a((Node) args[0], (StellarSystem) args[1]));
            case 137:
                return cMp();
            case 138:
                return cMr();
            case 139:
                return aiC();
            case 140:
                cMt();
                return null;
            case 141:
                cMv();
                return null;
            case 142:
                m1608e((Actor) args[0], (C5260aCm) args[1], (Vec3f) args[2], (Vec3f) args[3]);
                return null;
            case 143:
                return m1566am((Vec3d) args[0]);
            case 144:
                return m1576bA((Vec3f) args[0]);
            case 145:
                return m1568au();
            case 146:
                cMx();
                return null;
            case 147:
                cMz();
                return null;
            case 148:
                return azV();
            case 149:
                m1552a((StellarSystem) args[0]);
                return null;
            case 150:
                m1571b((Space) args[0]);
                return null;
            case 151:
                ahB();
                return null;
            case 152:
                m1553a((StellarSystem) args[0], (Vec3d) args[1]);
                return null;
            case 153:
                cMB();
                return null;
            case 154:
                m1654zw();
                return null;
            case 155:
                m1551a((C0665JT) args[0]);
                return null;
            case 156:
                m1569ay(((Float) args[0]).floatValue());
                return null;
            case 157:
                m1643lu(((Float) args[0]).floatValue());
                return null;
            case 158:
                return new Boolean(cMD());
            case 159:
                return cMF();
            case 160:
                aib();
                return null;
            case 161:
                aid();
                return null;
            case 162:
                return ail();
            case 163:
                return cMH();
            case 164:
                m1554a((C1003Om) args[0], (Actor) args[1], (C5260aCm) args[2], (Vec3f) args[3], (Vec3f) args[4]);
                return null;
            case 165:
                m1599c((C1003Om) args[0]);
                return null;
            case 166:
                return new Long(cMJ());
            case 167:
                return aad();
            case 168:
                return new Boolean(m1533QV());
            case 169:
                cMK();
                return null;
            default:
                return super.mo14a(gr);
        }
    }

    /* renamed from: a */
    public void mo15a(Collection collection, C0495Gr gr) {
        switch (gr.mo2417hq() - TaikodomObject._m_methodCount) {
            case 141:
                m1647n(collection, gr);
                return;
            case 146:
                m1649o(collection, gr);
                return;
            default:
                super.mo15a(collection, gr);
                return;
        }
    }

    @ClientOnly
    /* renamed from: a */
    public void mo968a(RenderAsset renderAsset) {
        switch (bFf().mo6893i(dmF)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, dmF, new Object[]{renderAsset}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, dmF, new Object[]{renderAsset}));
                break;
        }
        m1574b(renderAsset);
    }

    /* renamed from: aH */
    public void mo70aH() {
        switch (bFf().mo6893i(_f_onResurrect_0020_0028_0029V)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, _f_onResurrect_0020_0028_0029V, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, _f_onResurrect_0020_0028_0029V, new Object[0]));
                break;
        }
        m1560aG();
    }

    public boolean aSc() {
        switch (bFf().mo6893i(hlA)) {
            case 0:
                return false;
            case 2:
                return ((Boolean) bFf().mo5606d(new aCE(this, hlA, new Object[0]))).booleanValue();
            case 3:
                bFf().mo5606d(new aCE(this, hlA, new Object[0]));
                break;
        }
        return cMc();
    }

    public Vec3f aSf() {
        switch (bFf().mo6893i(dmu)) {
            case 0:
                return null;
            case 2:
                return (Vec3f) bFf().mo5606d(new aCE(this, dmu, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, dmu, new Object[0]));
                break;
        }
        return aZV();
    }

    public float aSg() {
        switch (bFf().mo6893i(hkS)) {
            case 0:
                return 0.0f;
            case 2:
                return ((Float) bFf().mo5606d(new aCE(this, hkS, new Object[0]))).floatValue();
            case 3:
                bFf().mo5606d(new aCE(this, hkS, new Object[0]));
                break;
        }
        return cLn();
    }

    public float aSh() {
        switch (bFf().mo6893i(hkT)) {
            case 0:
                return 0.0f;
            case 2:
                return ((Float) bFf().mo5606d(new aCE(this, hkT, new Object[0]))).floatValue();
            case 3:
                bFf().mo5606d(new aCE(this, hkT, new Object[0]));
                break;
        }
        return cLo();
    }

    public float aSi() {
        switch (bFf().mo6893i(hkU)) {
            case 0:
                return 0.0f;
            case 2:
                return ((Float) bFf().mo5606d(new aCE(this, hkU, new Object[0]))).floatValue();
            case 3:
                bFf().mo5606d(new aCE(this, hkU, new Object[0]));
                break;
        }
        return cLp();
    }

    public float aSj() {
        switch (bFf().mo6893i(hkR)) {
            case 0:
                return 0.0f;
            case 2:
                return ((Float) bFf().mo5606d(new aCE(this, hkR, new Object[0]))).floatValue();
            case 3:
                bFf().mo5606d(new aCE(this, hkR, new Object[0]));
                break;
        }
        return cLm();
    }

    public Actor aSp() {
        switch (bFf().mo6893i(hkD)) {
            case 0:
                return null;
            case 2:
                return (Actor) bFf().mo5606d(new aCE(this, hkD, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, hkD, new Object[0]));
                break;
        }
        return cKX();
    }

    public float aZY() {
        switch (bFf().mo6893i(dmw)) {
            case 0:
                return 0.0f;
            case 2:
                return ((Float) bFf().mo5606d(new aCE(this, dmw, new Object[0]))).floatValue();
            case 3:
                bFf().mo5606d(new aCE(this, dmw, new Object[0]));
                break;
        }
        return aZX();
    }

    public Collection<aDR> aae() {
        switch (bFf().mo6893i(f310x99c43db3)) {
            case 0:
                return null;
            case 2:
                return (Collection) bFf().mo5606d(new aCE(this, f310x99c43db3, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f310x99c43db3, new Object[0]));
                break;
        }
        return aad();
    }

    @ClientOnly
    public void afG() {
        switch (bFf().mo6893i(bpB)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, bpB, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, bpB, new Object[0]));
                break;
        }
        afF();
    }

    /* renamed from: ah */
    public void mo978ah(Vec3d ajr) {
        switch (bFf().mo6893i(hlw)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, hlw, new Object[]{ajr}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, hlw, new Object[]{ajr}));
                break;
        }
        m1561ag(ajr);
    }

    public void ahC() {
        switch (bFf().mo6893i(brq)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, brq, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, brq, new Object[0]));
                break;
        }
        ahB();
    }

    public String ahQ() {
        switch (bFf().mo6893i(brK)) {
            case 0:
                return null;
            case 2:
                return (String) bFf().mo5606d(new aCE(this, brK, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, brK, new Object[0]));
                break;
        }
        return ahP();
    }

    public void ahU() {
        switch (bFf().mo6893i(brN)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, brN, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, brN, new Object[0]));
                break;
        }
        ahT();
    }

    @ClientOnly
    public void ahW() {
        switch (bFf().mo6893i(brO)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, brO, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, brO, new Object[0]));
                break;
        }
        ahV();
    }

    /* access modifiers changed from: protected */
    @ClientOnly
    public void aht() {
        switch (bFf().mo6893i(bqO)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, bqO, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, bqO, new Object[0]));
                break;
        }
        ahs();
    }

    public C0520HN aiD() {
        switch (bFf().mo6893i(bsq)) {
            case 0:
                return null;
            case 2:
                return (C0520HN) bFf().mo5606d(new aCE(this, bsq, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, bsq, new Object[0]));
                break;
        }
        return aiC();
    }

    @C2198cg
    @C1253SX
    public void aia() {
        switch (bFf().mo6893i(brQ)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, brQ, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, brQ, new Object[0]));
                break;
        }
        ahZ();
    }

    @ClientOnly
    public void aic() {
        switch (bFf().mo6893i(brR)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, brR, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, brR, new Object[0]));
                break;
        }
        aib();
    }

    @ClientOnly
    public void aie() {
        switch (bFf().mo6893i(brS)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, brS, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, brS, new Object[0]));
                break;
        }
        aid();
    }

    /* renamed from: aj */
    public void mo988aj(Vec3d ajr) {
        switch (bFf().mo6893i(hlU)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, hlU, new Object[]{ajr}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, hlU, new Object[]{ajr}));
                break;
        }
        m1563ai(ajr);
    }

    /* renamed from: an */
    public Vec3f mo989an(Vec3d ajr) {
        switch (bFf().mo6893i(hmm)) {
            case 0:
                return null;
            case 2:
                return (Vec3f) bFf().mo5606d(new aCE(this, hmm, new Object[]{ajr}));
            case 3:
                bFf().mo5606d(new aCE(this, hmm, new Object[]{ajr}));
                break;
        }
        return m1566am(ajr);
    }

    public Node azW() {
        switch (bFf().mo6893i(coQ)) {
            case 0:
                return null;
            case 2:
                return (Node) bFf().mo5606d(new aCE(this, coQ, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, coQ, new Object[0]));
                break;
        }
        return azV();
    }

    @C2198cg
    /* renamed from: b */
    public void mo990b(int i, long j, Collection<C6625aqZ> collection, C6705asB asb) {
        switch (bFf().mo6893i(f337vq)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f337vq, new Object[]{new Integer(i), new Long(j), collection, asb}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f337vq, new Object[]{new Integer(i), new Long(j), collection, asb}));
                break;
        }
        m1545a(i, j, collection, asb);
    }

    @C5307aEh
    /* renamed from: b */
    public void mo991b(long j, boolean z) {
        switch (bFf().mo6893i(f327uZ)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f327uZ, new Object[]{new Long(j), new Boolean(z)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f327uZ, new Object[]{new Long(j), new Boolean(z)}));
                break;
        }
        m1546a(j, z);
    }

    /* renamed from: b */
    public void mo620b(C0200h hVar) {
        switch (bFf().mo6893i(f339vs)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f339vs, new Object[]{hVar}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f339vs, new Object[]{hVar}));
                break;
        }
        m1547a(hVar);
    }

    @C4034yP
    @C5566aOg
    @C2499fr
    /* renamed from: b */
    public void mo621b(Actor cr, C5260aCm acm, Vec3f vec3f, Vec3f vec3f2) {
        switch (bFf().mo6893i(f333vm)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f333vm, new Object[]{cr, acm, vec3f, vec3f2}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f333vm, new Object[]{cr, acm, vec3f, vec3f2}));
                break;
        }
        m1548a(cr, acm, vec3f, vec3f2);
    }

    @C5307aEh
    /* renamed from: b */
    public void mo992b(Actor cr, Vec3f vec3f, Vec3f vec3f2, float f, C6339akz akz) {
        switch (bFf().mo6893i(f325uX)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f325uX, new Object[]{cr, vec3f, vec3f2, new Float(f), akz}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f325uX, new Object[]{cr, vec3f, vec3f2, new Float(f), akz}));
                break;
        }
        m1549a(cr, vec3f, vec3f2, f, akz);
    }

    /* renamed from: b */
    public void mo24b(C0665JT jt) {
        switch (bFf().mo6893i(f303Do)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f303Do, new Object[]{jt}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f303Do, new Object[]{jt}));
                break;
        }
        m1551a(jt);
    }

    /* renamed from: b */
    public void mo993b(StellarSystem jj) {
        switch (bFf().mo6893i(f305Mq)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f305Mq, new Object[]{jj}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f305Mq, new Object[]{jj}));
                break;
        }
        m1552a(jj);
    }

    /* renamed from: b */
    public final void mo994b(StellarSystem jj, Vec3d ajr) {
        switch (bFf().mo6893i(hmq)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, hmq, new Object[]{jj, ajr}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, hmq, new Object[]{jj, ajr}));
                break;
        }
        m1553a(jj, ajr);
    }

    @C4034yP
    @C5566aOg
    @C2499fr(mo18857qh = 2)
    /* renamed from: b */
    public void mo995b(C1003Om om, Actor cr, C5260aCm acm, Vec3f vec3f, Vec3f vec3f2) {
        switch (bFf().mo6893i(hmw)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, hmw, new Object[]{om, cr, acm, vec3f, vec3f2}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, hmw, new Object[]{om, cr, acm, vec3f, vec3f2}));
                break;
        }
        m1554a(om, cr, acm, vec3f, vec3f2);
    }

    /* access modifiers changed from: protected */
    @ClientOnly
    /* renamed from: b */
    public void mo996b(Vec3d ajr, Quat4fWrap aoy) {
        switch (bFf().mo6893i(f340vt)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f340vt, new Object[]{ajr, aoy}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f340vt, new Object[]{ajr, aoy}));
                break;
        }
        m1555a(ajr, aoy);
    }

    @C5307aEh
    /* renamed from: b */
    public void mo997b(Vec3d ajr, Quat4fWrap aoy, Vec3f vec3f, Vec3f vec3f2) {
        switch (bFf().mo6893i(hlT)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, hlT, new Object[]{ajr, aoy, vec3f, vec3f2}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, hlT, new Object[]{ajr, aoy, vec3f, vec3f2}));
                break;
        }
        m1556a(ajr, aoy, vec3f, vec3f2);
    }

    @C4034yP
    @C2499fr
    @C1253SX
    /* renamed from: b */
    public void mo998b(Vec3d ajr, Quat4fWrap aoy, Vec3f vec3f, Vec3f vec3f2, float f, float f2, float f3, float f4, long j, boolean z) {
        switch (bFf().mo6893i(hmd)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, hmd, new Object[]{ajr, aoy, vec3f, vec3f2, new Float(f), new Float(f2), new Float(f3), new Float(f4), new Long(j), new Boolean(z)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, hmd, new Object[]{ajr, aoy, vec3f, vec3f2, new Float(f), new Float(f2), new Float(f3), new Float(f4), new Long(j), new Boolean(z)}));
                break;
        }
        m1557a(ajr, aoy, vec3f, vec3f2, f, f2, f3, f4, j, z);
    }

    /* renamed from: b */
    public void mo999b(C4029yK yKVar) {
        switch (bFf().mo6893i(f328va)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f328va, new Object[]{yKVar}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f328va, new Object[]{yKVar}));
                break;
        }
        m1558a(yKVar);
    }

    @ClientOnly
    /* renamed from: b */
    public boolean mo1000b(Node rPVar, StellarSystem jj) {
        switch (bFf().mo6893i(hmh)) {
            case 0:
                return false;
            case 2:
                return ((Boolean) bFf().mo5606d(new aCE(this, hmh, new Object[]{rPVar, jj}))).booleanValue();
            case 3:
                bFf().mo5606d(new aCE(this, hmh, new Object[]{rPVar, jj}));
                break;
        }
        return m1559a(rPVar, jj);
    }

    /* renamed from: bB */
    public float mo1001bB(Actor cr) {
        switch (bFf().mo6893i(hkX)) {
            case 0:
                return 0.0f;
            case 2:
                return ((Float) bFf().mo5606d(new aCE(this, hkX, new Object[]{cr}))).floatValue();
            case 3:
                bFf().mo5606d(new aCE(this, hkX, new Object[]{cr}));
                break;
        }
        return m1575bA(cr);
    }

    /* renamed from: bB */
    public Vec3d mo1002bB(Vec3f vec3f) {
        switch (bFf().mo6893i(hmn)) {
            case 0:
                return null;
            case 2:
                return (Vec3d) bFf().mo5606d(new aCE(this, hmn, new Object[]{vec3f}));
            case 3:
                bFf().mo5606d(new aCE(this, hmn, new Object[]{vec3f}));
                break;
        }
        return m1576bA(vec3f);
    }

    /* access modifiers changed from: package-private */
    /* renamed from: bD */
    public void mo1003bD(Actor cr) {
        switch (bFf().mo6893i(hlP)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, hlP, new Object[]{cr}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, hlP, new Object[]{cr}));
                break;
        }
        m1577bC(cr);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Spawned")
    public boolean bae() {
        switch (bFf().mo6893i(dmz)) {
            case 0:
                return false;
            case 2:
                return ((Boolean) bFf().mo5606d(new aCE(this, dmz, new Object[0]))).booleanValue();
            case 3:
                bFf().mo5606d(new aCE(this, dmz, new Object[0]));
                break;
        }
        return bad();
    }

    @ClientOnly
    public boolean bag() {
        switch (bFf().mo6893i(dmA)) {
            case 0:
                return false;
            case 2:
                return ((Boolean) bFf().mo5606d(new aCE(this, dmA, new Object[0]))).booleanValue();
            case 3:
                bFf().mo5606d(new aCE(this, dmA, new Object[0]));
                break;
        }
        return baf();
    }

    /* renamed from: bl */
    public void mo1006bl(Vec3f vec3f) {
        switch (bFf().mo6893i(hlt)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, hlt, new Object[]{vec3f}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, hlt, new Object[]{vec3f}));
                break;
        }
        m1583bk(vec3f);
    }

    /* renamed from: bn */
    public void mo1007bn(Vec3f vec3f) {
        switch (bFf().mo6893i(hlu)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, hlu, new Object[]{vec3f}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, hlu, new Object[]{vec3f}));
                break;
        }
        m1585bm(vec3f);
    }

    /* renamed from: bp */
    public Vec3f mo1008bp(Vec3f vec3f) {
        switch (bFf().mo6893i(hlN)) {
            case 0:
                return null;
            case 2:
                return (Vec3f) bFf().mo5606d(new aCE(this, hlN, new Object[]{vec3f}));
            case 3:
                bFf().mo5606d(new aCE(this, hlN, new Object[]{vec3f}));
                break;
        }
        return m1586bo(vec3f);
    }

    /* renamed from: br */
    public Vec3f mo1009br(Vec3f vec3f) {
        switch (bFf().mo6893i(hlO)) {
            case 0:
                return null;
            case 2:
                return (Vec3f) bFf().mo5606d(new aCE(this, hlO, new Object[]{vec3f}));
            case 3:
                bFf().mo5606d(new aCE(this, hlO, new Object[]{vec3f}));
                break;
        }
        return m1587bq(vec3f);
    }

    /* renamed from: bt */
    public void mo1010bt(Vec3f vec3f) {
        switch (bFf().mo6893i(hlQ)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, hlQ, new Object[]{vec3f}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, hlQ, new Object[]{vec3f}));
                break;
        }
        m1588bs(vec3f);
    }

    /* renamed from: bv */
    public float mo1011bv(Actor cr) {
        switch (bFf().mo6893i(hkI)) {
            case 0:
                return 0.0f;
            case 2:
                return ((Float) bFf().mo5606d(new aCE(this, hkI, new Object[]{cr}))).floatValue();
            case 3:
                bFf().mo5606d(new aCE(this, hkI, new Object[]{cr}));
                break;
        }
        return m1590bu(cr);
    }

    /* renamed from: bv */
    public void mo1012bv(Vec3f vec3f) {
        switch (bFf().mo6893i(hlR)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, hlR, new Object[]{vec3f}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, hlR, new Object[]{vec3f}));
                break;
        }
        m1591bu(vec3f);
    }

    /* renamed from: bx */
    public float mo1013bx(Actor cr) {
        switch (bFf().mo6893i(hkJ)) {
            case 0:
                return 0.0f;
            case 2:
                return ((Float) bFf().mo5606d(new aCE(this, hkJ, new Object[]{cr}))).floatValue();
            case 3:
                bFf().mo5606d(new aCE(this, hkJ, new Object[]{cr}));
                break;
        }
        return m1592bw(cr);
    }

    /* renamed from: bx */
    public void mo1014bx(Vec3f vec3f) {
        switch (bFf().mo6893i(hlZ)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, hlZ, new Object[]{vec3f}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, hlZ, new Object[]{vec3f}));
                break;
        }
        m1593bw(vec3f);
    }

    /* renamed from: bz */
    public float mo1015bz(Actor cr) {
        switch (bFf().mo6893i(hkW)) {
            case 0:
                return 0.0f;
            case 2:
                return ((Float) bFf().mo5606d(new aCE(this, hkW, new Object[]{cr}))).floatValue();
            case 3:
                bFf().mo5606d(new aCE(this, hkW, new Object[]{cr}));
                break;
        }
        return m1594by(cr);
    }

    /* renamed from: bz */
    public void mo1016bz(Vec3f vec3f) {
        switch (bFf().mo6893i(hma)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, hma, new Object[]{vec3f}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, hma, new Object[]{vec3f}));
                break;
        }
        m1595by(vec3f);
    }

    @C5566aOg
    /* renamed from: c */
    public void mo1017c(Space ea) {
        switch (bFf().mo6893i(bcB)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, bcB, new Object[]{ea}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, bcB, new Object[]{ea}));
                break;
        }
        m1571b(ea);
    }

    /* renamed from: c */
    public void mo1018c(Vec3f vec3f, Vec3d ajr) {
        switch (bFf().mo6893i(hle)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, hle, new Object[]{vec3f, ajr}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, hle, new Object[]{vec3f, ajr}));
                break;
        }
        m1573b(vec3f, ajr);
    }

    /* renamed from: c */
    public C5663aRz[] mo25c() {
        return _m_fields;
    }

    @ClientOnly
    public SceneObject cHg() {
        switch (bFf().mo6893i(hlg)) {
            case 0:
                return null;
            case 2:
                return (SceneObject) bFf().mo5606d(new aCE(this, hlg, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, hlg, new Object[0]));
                break;
        }
        return cLC();
    }

    public Actor cKZ() {
        switch (bFf().mo6893i(hkK)) {
            case 0:
                return null;
            case 2:
                return (Actor) bFf().mo5606d(new aCE(this, hkK, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, hkK, new Object[0]));
                break;
        }
        return cKY();
    }

    @ClientOnly
    public TransformWrap cLE() {
        switch (bFf().mo6893i(hlh)) {
            case 0:
                return null;
            case 2:
                return (TransformWrap) bFf().mo5606d(new aCE(this, hlh, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, hlh, new Object[0]));
                break;
        }
        return cLD();
    }

    @ClientOnly
    public TransformWrap cLG() {
        switch (bFf().mo6893i(hli)) {
            case 0:
                return null;
            case 2:
                return (TransformWrap) bFf().mo5606d(new aCE(this, hli, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, hli, new Object[0]));
                break;
        }
        return cLF();
    }

    public C0520HN cLJ() {
        switch (bFf().mo6893i(hlk)) {
            case 0:
                return null;
            case 2:
                return (C0520HN) bFf().mo5606d(new aCE(this, hlk, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, hlk, new Object[0]));
                break;
        }
        return cLI();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Stellar System")
    public String cLL() {
        switch (bFf().mo6893i(hll)) {
            case 0:
                return null;
            case 2:
                return (String) bFf().mo5606d(new aCE(this, hll, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, hll, new Object[0]));
                break;
        }
        return cLK();
    }

    @ClientOnly
    public boolean cLN() {
        switch (bFf().mo6893i(hln)) {
            case 0:
                return false;
            case 2:
                return ((Boolean) bFf().mo5606d(new aCE(this, hln, new Object[0]))).booleanValue();
            case 3:
                bFf().mo5606d(new aCE(this, hln, new Object[0]));
                break;
        }
        return cLM();
    }

    public Vec3f cLR() {
        switch (bFf().mo6893i(hlp)) {
            case 0:
                return null;
            case 2:
                return (Vec3f) bFf().mo5606d(new aCE(this, hlp, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, hlp, new Object[0]));
                break;
        }
        return cLQ();
    }

    public Vec3f cLT() {
        switch (bFf().mo6893i(hlq)) {
            case 0:
                return null;
            case 2:
                return (Vec3f) bFf().mo5606d(new aCE(this, hlq, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, hlq, new Object[0]));
                break;
        }
        return cLS();
    }

    public Quat4fWrap cLV() {
        switch (bFf().mo6893i(hlr)) {
            case 0:
                return null;
            case 2:
                return (Quat4fWrap) bFf().mo5606d(new aCE(this, hlr, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, hlr, new Object[0]));
                break;
        }
        return cLU();
    }

    public Vec3d cLX() {
        switch (bFf().mo6893i(hls)) {
            case 0:
                return null;
            case 2:
                return (Vec3d) bFf().mo5606d(new aCE(this, hls, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, hls, new Object[0]));
                break;
        }
        return cLW();
    }

    public boolean cLZ() {
        switch (bFf().mo6893i(hly)) {
            case 0:
                return false;
            case 2:
                return ((Boolean) bFf().mo5606d(new aCE(this, hly, new Object[0]))).booleanValue();
            case 3:
                bFf().mo5606d(new aCE(this, hly, new Object[0]));
                break;
        }
        return cLY();
    }

    public Node cLb() {
        switch (bFf().mo6893i(hkL)) {
            case 0:
                return null;
            case 2:
                return (Node) bFf().mo5606d(new aCE(this, hkL, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, hkL, new Object[0]));
                break;
        }
        return cLa();
    }

    @C2198cg
    public C0520HN cLd() {
        switch (bFf().mo6893i(hkM)) {
            case 0:
                return null;
            case 2:
                return (C0520HN) bFf().mo5606d(new aCE(this, hkM, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, hkM, new Object[0]));
                break;
        }
        return cLc();
    }

    public Vec3f cLf() {
        switch (bFf().mo6893i(hkN)) {
            case 0:
                return null;
            case 2:
                return (Vec3f) bFf().mo5606d(new aCE(this, hkN, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, hkN, new Object[0]));
                break;
        }
        return cLe();
    }

    public Vec3f cLh() {
        switch (bFf().mo6893i(hkO)) {
            case 0:
                return null;
            case 2:
                return (Vec3f) bFf().mo5606d(new aCE(this, hkO, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, hkO, new Object[0]));
                break;
        }
        return cLg();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Orientation")
    public Quat4fWrap cLj() {
        switch (bFf().mo6893i(hkP)) {
            case 0:
                return null;
            case 2:
                return (Quat4fWrap) bFf().mo5606d(new aCE(this, hkP, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, hkP, new Object[0]));
                break;
        }
        return cLi();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Position")
    public Vec3d cLl() {
        switch (bFf().mo6893i(hkQ)) {
            case 0:
                return null;
            case 2:
                return (Vec3d) bFf().mo5606d(new aCE(this, hkQ, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, hkQ, new Object[0]));
                break;
        }
        return cLk();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Enable Fog of war")
    public boolean cLs() {
        switch (bFf().mo6893i(hkY)) {
            case 0:
                return false;
            case 2:
                return ((Boolean) bFf().mo5606d(new aCE(this, hkY, new Object[0]))).booleanValue();
            case 3:
                bFf().mo5606d(new aCE(this, hkY, new Object[0]));
                break;
        }
        return cLr();
    }

    public Vec3f cLu() {
        switch (bFf().mo6893i(hkZ)) {
            case 0:
                return null;
            case 2:
                return (Vec3f) bFf().mo5606d(new aCE(this, hkZ, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, hkZ, new Object[0]));
                break;
        }
        return cLt();
    }

    public Pawn cLw() {
        switch (bFf().mo6893i(hla)) {
            case 0:
                return null;
            case 2:
                return (Pawn) bFf().mo5606d(new aCE(this, hla, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, hla, new Object[0]));
                break;
        }
        return cLv();
    }

    public Vec3f cLy() {
        switch (bFf().mo6893i(hlb)) {
            case 0:
                return null;
            case 2:
                return (Vec3f) bFf().mo5606d(new aCE(this, hlb, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, hlb, new Object[0]));
                break;
        }
        return cLx();
    }

    @ClientOnly
    public void cMA() {
        switch (bFf().mo6893i(hmp)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, hmp, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, hmp, new Object[0]));
                break;
        }
        cMz();
    }

    public void cMC() {
        switch (bFf().mo6893i(hmr)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, hmr, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, hmr, new Object[0]));
                break;
        }
        cMB();
    }

    public C2686iZ<Actor> cMG() {
        switch (bFf().mo6893i(hmu)) {
            case 0:
                return null;
            case 2:
                return (C2686iZ) bFf().mo5606d(new aCE(this, hmu, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, hmu, new Object[0]));
                break;
        }
        return cMF();
    }

    @ClientOnly
    public C6017aep cMI() {
        switch (bFf().mo6893i(hmv)) {
            case 0:
                return null;
            case 2:
                return (C6017aep) bFf().mo5606d(new aCE(this, hmv, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, hmv, new Object[0]));
                break;
        }
        return cMH();
    }

    @C5566aOg
    @C2499fr
    public void cML() {
        switch (bFf().mo6893i(hmz)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, hmz, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, hmz, new Object[0]));
                break;
        }
        cMK();
    }

    public boolean cMb() {
        switch (bFf().mo6893i(hlz)) {
            case 0:
                return false;
            case 2:
                return ((Boolean) bFf().mo5606d(new aCE(this, hlz, new Object[0]))).booleanValue();
            case 3:
                bFf().mo5606d(new aCE(this, hlz, new Object[0]));
                break;
        }
        return cMa();
    }

    /* access modifiers changed from: protected */
    public void cMe() {
        switch (bFf().mo6893i(hlF)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, hlF, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, hlF, new Object[0]));
                break;
        }
        cMd();
    }

    /* access modifiers changed from: protected */
    public void cMk() {
        switch (bFf().mo6893i(hlK)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, hlK, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, hlK, new Object[0]));
                break;
        }
        cMj();
    }

    /* access modifiers changed from: protected */
    public boolean cMm() {
        switch (bFf().mo6893i(hlL)) {
            case 0:
                return false;
            case 2:
                return ((Boolean) bFf().mo5606d(new aCE(this, hlL, new Object[0]))).booleanValue();
            case 3:
                bFf().mo5606d(new aCE(this, hlL, new Object[0]));
                break;
        }
        return cMl();
    }

    /* access modifiers changed from: protected */
    public boolean cMo() {
        switch (bFf().mo6893i(hmg)) {
            case 0:
                return false;
            case 2:
                return ((Boolean) bFf().mo5606d(new aCE(this, hmg, new Object[0]))).booleanValue();
            case 3:
                bFf().mo5606d(new aCE(this, hmg, new Object[0]));
                break;
        }
        return cMn();
    }

    /* access modifiers changed from: protected */
    public C0520HN cMq() {
        switch (bFf().mo6893i(hmi)) {
            case 0:
                return null;
            case 2:
                return (C0520HN) bFf().mo5606d(new aCE(this, hmi, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, hmi, new Object[0]));
                break;
        }
        return cMp();
    }

    public Actor cMs() {
        switch (bFf().mo6893i(hmj)) {
            case 0:
                return null;
            case 2:
                return (Actor) bFf().mo5606d(new aCE(this, hmj, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, hmj, new Object[0]));
                break;
        }
        return cMr();
    }

    public void cMu() {
        switch (bFf().mo6893i(hmk)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, hmk, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, hmk, new Object[0]));
                break;
        }
        cMt();
    }

    /* access modifiers changed from: protected */
    /* renamed from: cO */
    public void mo1054cO(boolean z) {
        switch (bFf().mo6893i(hmb)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, hmb, new Object[]{new Boolean(z)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, hmb, new Object[]{new Boolean(z)}));
                break;
        }
        m1628id(z);
    }

    /* access modifiers changed from: protected */
    @ClientOnly
    /* renamed from: ca */
    public void mo1055ca(String str) {
        switch (bFf().mo6893i(brs)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, brs, new Object[]{str}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, brs, new Object[]{str}));
                break;
        }
        m1579bZ(str);
    }

    @ClientOnly
    public long crP() {
        switch (bFf().mo6893i(hmy)) {
            case 0:
                return 0;
            case 2:
                return ((Long) bFf().mo5606d(new aCE(this, hmy, new Object[0]))).longValue();
            case 3:
                bFf().mo5606d(new aCE(this, hmy, new Object[0]));
                break;
        }
        return cMJ();
    }

    @ClientOnly
    public void cxE() {
        switch (bFf().mo6893i(gDT)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, gDT, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, gDT, new Object[0]));
                break;
        }
        cxD();
    }

    @ClientOnly
    public void cxG() {
        switch (bFf().mo6893i(gDU)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, gDU, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, gDU, new Object[0]));
                break;
        }
        cxF();
    }

    /* access modifiers changed from: protected */
    @C2198cg
    @C1253SX
    /* renamed from: d */
    public C0520HN mo635d(Space ea, long j) {
        switch (bFf().mo6893i(f326uY)) {
            case 0:
                return null;
            case 2:
                return (C0520HN) bFf().mo5606d(new aCE(this, f326uY, new Object[]{ea, new Long(j)}));
            case 3:
                bFf().mo5606d(new aCE(this, f326uY, new Object[]{ea, new Long(j)}));
                break;
        }
        return m1596c(ea, j);
    }

    @C5566aOg
    /* renamed from: d */
    public void mo636d(Actor cr, C5260aCm acm, Vec3f vec3f, Vec3f vec3f2) {
        switch (bFf().mo6893i(f334vn)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f334vn, new Object[]{cr, acm, vec3f, vec3f2}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f334vn, new Object[]{cr, acm, vec3f, vec3f2}));
                break;
        }
        m1597c(cr, acm, vec3f, vec3f2);
    }

    @C4034yP
    @C5566aOg
    @C2499fr(mo18857qh = 2)
    /* renamed from: d */
    public void mo1059d(C1003Om om) {
        switch (bFf().mo6893i(hmx)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, hmx, new Object[]{om}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, hmx, new Object[]{om}));
                break;
        }
        m1599c(om);
    }

    /* access modifiers changed from: protected */
    /* renamed from: d */
    public boolean mo1060d(boolean z, String str) {
        switch (bFf().mo6893i(hkF)) {
            case 0:
                return false;
            case 2:
                return ((Boolean) bFf().mo5606d(new aCE(this, hkF, new Object[]{new Boolean(z), str}))).booleanValue();
            case 3:
                bFf().mo5606d(new aCE(this, hkF, new Object[]{new Boolean(z), str}));
                break;
        }
        return m1602c(z, str);
    }

    public void dispose() {
        switch (bFf().mo6893i(_f_dispose_0020_0028_0029V)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, _f_dispose_0020_0028_0029V, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, _f_dispose_0020_0028_0029V, new Object[0]));
                break;
        }
        m1613fg();
    }

    /* renamed from: e */
    public void mo1061e(StellarSystem jj) {
        switch (bFf().mo6893i(aBt)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, aBt, new Object[]{jj}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, aBt, new Object[]{jj}));
                break;
        }
        m1605d(jj);
    }

    @C0401Fa
    /* renamed from: e */
    public void mo1062e(C4029yK yKVar) {
        switch (bFf().mo6893i(hlD)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, hlD, new Object[]{yKVar}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, hlD, new Object[]{yKVar}));
                break;
        }
        m1607d(yKVar);
    }

    @C0401Fa
    /* renamed from: f */
    public void mo1063f(float f, float f2, float f3) {
        switch (bFf().mo6893i(hlV)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, hlV, new Object[]{new Float(f), new Float(f2), new Float(f3)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, hlV, new Object[]{new Float(f), new Float(f2), new Float(f3)}));
                break;
        }
        m1652u(f, f2, f3);
    }

    @C4034yP
    @C5566aOg
    @C2499fr(mo18857qh = 2)
    /* renamed from: f */
    public void mo1064f(Actor cr, C5260aCm acm, Vec3f vec3f, Vec3f vec3f2) {
        switch (bFf().mo6893i(f309Wu)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f309Wu, new Object[]{cr, acm, vec3f, vec3f2}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f309Wu, new Object[]{cr, acm, vec3f, vec3f2}));
                break;
        }
        m1608e(cr, acm, vec3f, vec3f2);
    }

    @C1253SX
    /* renamed from: f */
    public void mo1065f(Space ea, long j) {
        switch (bFf().mo6893i(brr)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, brr, new Object[]{ea, new Long(j)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, brr, new Object[]{ea, new Long(j)}));
                break;
        }
        m1609e(ea, j);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "NameI")
    @C5566aOg
    /* renamed from: f */
    public void mo1066f(I18NString i18NString) {
        switch (bFf().mo6893i(f341zU)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f341zU, new Object[]{i18NString}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f341zU, new Object[]{i18NString}));
                break;
        }
        m1611e(i18NString);
    }

    @ClientOnly
    /* renamed from: fQ */
    public void mo1067fQ(float f) {
        switch (bFf().mo6893i(dmr)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, dmr, new Object[]{new Float(f)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, dmr, new Object[]{new Float(f)}));
                break;
        }
        m1612fP(f);
    }

    @ClientOnly
    public Color getColor() {
        switch (bFf().mo6893i(brY)) {
            case 0:
                return null;
            case 2:
                return (Color) bFf().mo5606d(new aCE(this, brY, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, brY, new Object[0]));
                break;
        }
        return ail();
    }

    public String getDisplayName() {
        switch (bFf().mo6893i(hkV)) {
            case 0:
                return null;
            case 2:
                return (String) bFf().mo5606d(new aCE(this, hkV, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, hkV, new Object[0]));
                break;
        }
        return cLq();
    }

    public String getName() {
        switch (bFf().mo6893i(f306Pf)) {
            case 0:
                return null;
            case 2:
                return (String) bFf().mo5606d(new aCE(this, f306Pf, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f306Pf, new Object[0]));
                break;
        }
        return m1653uV();
    }

    @C5566aOg
    public void setName(String str) {
        switch (bFf().mo6893i(f307Pg)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f307Pg, new Object[]{str}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f307Pg, new Object[]{str}));
                break;
        }
        m1567aq(str);
    }

    public Quat4fWrap getOrientation() {
        switch (bFf().mo6893i(hld)) {
            case 0:
                return null;
            case 2:
                return (Quat4fWrap) bFf().mo5606d(new aCE(this, hld, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, hld, new Object[0]));
                break;
        }
        return cLB();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Orientation")
    public void setOrientation(Quat4fWrap aoy) {
        switch (bFf().mo6893i(hmc)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, hmc, new Object[]{aoy}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, hmc, new Object[]{aoy}));
                break;
        }
        m1644m(aoy);
    }

    public Vec3d getPosition() {
        switch (bFf().mo6893i(bcC)) {
            case 0:
                return null;
            case 2:
                return (Vec3d) bFf().mo5606d(new aCE(this, bcC, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, bcC, new Object[0]));
                break;
        }
        return m1542Zd();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Position")
    @C5566aOg
    public void setPosition(Vec3d ajr) {
        switch (bFf().mo6893i(f336vp)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f336vp, new Object[]{ajr}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f336vp, new Object[]{ajr}));
                break;
        }
        m1601c(ajr);
    }

    /* access modifiers changed from: protected */
    @C4034yP
    @C2499fr(mo18857qh = 4)
    @C1253SX
    /* renamed from: h */
    public void mo1073h(Actor cr, C5260aCm acm, Vec3f vec3f, Vec3f vec3f2) {
        switch (bFf().mo6893i(hkG)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, hkG, new Object[]{cr, acm, vec3f, vec3f2}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, hkG, new Object[]{cr, acm, vec3f, vec3f2}));
                break;
        }
        m1614g(cr, acm, vec3f, vec3f2);
    }

    @C0401Fa
    /* renamed from: hY */
    public void mo1074hY(boolean z) {
        switch (bFf().mo6893i(hlx)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, hlx, new Object[]{new Boolean(z)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, hlx, new Object[]{new Boolean(z)}));
                break;
        }
        m1621hX(z);
    }

    /* renamed from: i */
    public boolean mo1075i(UserConnection apk) {
        switch (bFf().mo6893i(hlB)) {
            case 0:
                return false;
            case 2:
                return ((Boolean) bFf().mo5606d(new aCE(this, hlB, new Object[]{apk}))).booleanValue();
            case 3:
                bFf().mo5606d(new aCE(this, hlB, new Object[]{apk}));
                break;
        }
        return m1616h(apk);
    }

    @ClientOnly
    /* renamed from: iA */
    public String mo647iA() {
        switch (bFf().mo6893i(f335vo)) {
            case 0:
                return null;
            case 2:
                return (String) bFf().mo5606d(new aCE(this, f335vo, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f335vo, new Object[0]));
                break;
        }
        return m1633iz();
    }

    @ClientOnly
    /* renamed from: iC */
    public void mo1076iC() {
        switch (bFf().mo6893i(f338vr)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f338vr, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f338vr, new Object[0]));
                break;
        }
        m1626iB();
    }

    @C0401Fa
    /* renamed from: ia */
    public void mo1077ia(boolean z) {
        switch (bFf().mo6893i(hlC)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, hlC, new Object[]{new Boolean(z)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, hlC, new Object[]{new Boolean(z)}));
                break;
        }
        m1622hZ(z);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Enable Fog of war")
    @C5566aOg
    /* renamed from: ic */
    public void mo1078ic(boolean z) {
        switch (bFf().mo6893i(hlX)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, hlX, new Object[]{new Boolean(z)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, hlX, new Object[]{new Boolean(z)}));
                break;
        }
        m1627ib(z);
    }

    @ClientOnly
    /* renamed from: ip */
    public String mo648ip() {
        switch (bFf().mo6893i(f329vc)) {
            case 0:
                return null;
            case 2:
                return (String) bFf().mo5606d(new aCE(this, f329vc, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f329vc, new Object[0]));
                break;
        }
        return m1629io();
    }

    @ClientOnly
    /* renamed from: ir */
    public String mo649ir() {
        switch (bFf().mo6893i(f330ve)) {
            case 0:
                return null;
            case 2:
                return (String) bFf().mo5606d(new aCE(this, f330ve, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f330ve, new Object[0]));
                break;
        }
        return m1630iq();
    }

    public boolean isStatic() {
        switch (bFf().mo6893i(cQG)) {
            case 0:
                return false;
            case 2:
                return ((Boolean) bFf().mo5606d(new aCE(this, cQG, new Object[0]))).booleanValue();
            case 3:
                bFf().mo5606d(new aCE(this, cQG, new Object[0]));
                break;
        }
        return aMM();
    }

    public void setStatic(boolean z) {
        switch (bFf().mo6893i(cQF)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, cQF, new Object[]{new Boolean(z)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, cQF, new Object[]{new Boolean(z)}));
                break;
        }
        m1603cF(z);
    }

    /* renamed from: iu */
    public String mo651iu() {
        switch (bFf().mo6893i(f331vi)) {
            case 0:
                return null;
            case 2:
                return (String) bFf().mo5606d(new aCE(this, f331vi, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f331vi, new Object[0]));
                break;
        }
        return m1631it();
    }

    @ClientOnly
    /* renamed from: iw */
    public boolean mo1080iw() {
        switch (bFf().mo6893i(f332vk)) {
            case 0:
                return false;
            case 2:
                return ((Boolean) bFf().mo5606d(new aCE(this, f332vk, new Object[0]))).booleanValue();
            case 3:
                bFf().mo5606d(new aCE(this, f332vk, new Object[0]));
                break;
        }
        return m1632iv();
    }

    /* renamed from: j */
    public void mo1081j(Space ea, long j) {
        switch (bFf().mo6893i(hkE)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, hkE, new Object[]{ea, new Long(j)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, hkE, new Object[]{ea, new Long(j)}));
                break;
        }
        m1623i(ea, j);
    }

    /* renamed from: j */
    public void mo1082j(Quat4fWrap aoy) {
        switch (bFf().mo6893i(hlv)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, hlv, new Object[]{aoy}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, hlv, new Object[]{aoy}));
                break;
        }
        m1624i(aoy);
    }

    /* renamed from: jK */
    public void mo1083jK(long j) {
        switch (bFf().mo6893i(hlE)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, hlE, new Object[]{new Long(j)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, hlE, new Object[]{new Long(j)}));
                break;
        }
        m1636jJ(j);
    }

    /* renamed from: k */
    public void mo1084k(Vec3d ajr, Vec3d ajr2) {
        switch (bFf().mo6893i(hlf)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, hlf, new Object[]{ajr, ajr2}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, hlf, new Object[]{ajr, ajr2}));
                break;
        }
        m1634j(ajr, ajr2);
    }

    /* renamed from: k */
    public void mo1085k(Pawn avi) {
        switch (bFf().mo6893i(hlY)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, hlY, new Object[]{avi}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, hlY, new Object[]{avi}));
                break;
        }
        m1635j(avi);
    }

    /* renamed from: l */
    public void mo1086l(Quat4fWrap aoy) {
        switch (bFf().mo6893i(hlS)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, hlS, new Object[]{aoy}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, hlS, new Object[]{aoy}));
                break;
        }
        m1637k(aoy);
    }

    @ClientOnly
    /* renamed from: lt */
    public void mo1087lt(float f) {
        switch (bFf().mo6893i(hme)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, hme, new Object[]{new Float(f)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, hme, new Object[]{new Float(f)}));
                break;
        }
        m1642ls(f);
    }

    /* renamed from: lv */
    public void mo1088lv(float f) {
        switch (bFf().mo6893i(hms)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, hms, new Object[]{new Float(f)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, hms, new Object[]{new Float(f)}));
                break;
        }
        m1643lu(f);
    }

    @C5472aKq
    /* renamed from: n */
    public boolean mo1089n(C1722ZT<UserConnection> zt) {
        switch (bFf().mo6893i(hlM)) {
            case 0:
                return false;
            case 2:
                return ((Boolean) bFf().mo5606d(new aCE(this, hlM, new Object[]{zt}))).booleanValue();
            case 3:
                bFf().mo5606d(new aCE(this, hlM, new Object[]{zt}));
                break;
        }
        return m1645m(zt);
    }

    @C5566aOg
    /* renamed from: qV */
    public void mo656qV() {
        switch (bFf().mo6893i(f304Lm)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f304Lm, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f304Lm, new Object[0]));
                break;
        }
        m1650qU();
    }

    /* renamed from: qZ */
    public Vec3f mo1090qZ() {
        switch (bFf().mo6893i(dmv)) {
            case 0:
                return null;
            case 2:
                return (Vec3f) bFf().mo5606d(new aCE(this, dmv, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, dmv, new Object[0]));
                break;
        }
        return aZW();
    }

    /* renamed from: ra */
    public float mo1091ra() {
        switch (bFf().mo6893i(aTh)) {
            case 0:
                return 0.0f;
            case 2:
                return ((Float) bFf().mo5606d(new aCE(this, aTh, new Object[0]))).floatValue();
            case 3:
                bFf().mo5606d(new aCE(this, aTh, new Object[0]));
                break;
        }
        return m1538VJ();
    }

    /* renamed from: rb */
    public float mo1092rb() {
        switch (bFf().mo6893i(aTg)) {
            case 0:
                return 0.0f;
            case 2:
                return ((Float) bFf().mo5606d(new aCE(this, aTg, new Object[0]))).floatValue();
            case 3:
                bFf().mo5606d(new aCE(this, aTg, new Object[0]));
                break;
        }
        return m1537VI();
    }

    public void setVisible(boolean z) {
        switch (bFf().mo6893i(dmq)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, dmq, new Object[]{new Boolean(z)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, dmq, new Object[]{new Boolean(z)}));
                break;
        }
        m1604cU(z);
    }

    @C1253SX
    public void step(float f) {
        switch (bFf().mo6893i(_f_step_0020_0028F_0029V)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, _f_step_0020_0028F_0029V, new Object[]{new Float(f)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, _f_step_0020_0028F_0029V, new Object[]{new Float(f)}));
                break;
        }
        m1569ay(f);
    }

    public String toString() {
        switch (bFf().mo6893i(_f_toString_0020_0028_0029Ljava_002flang_002fString_003b)) {
            case 0:
                return null;
            case 2:
                return (String) bFf().mo5606d(new aCE(this, _f_toString_0020_0028_0029Ljava_002flang_002fString_003b, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, _f_toString_0020_0028_0029Ljava_002flang_002fString_003b, new Object[0]));
                break;
        }
        return m1568au();
    }

    /* renamed from: zx */
    public void mo1099zx() {
        switch (bFf().mo6893i(f308Wp)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f308Wp, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f308Wp, new Object[0]));
                break;
        }
        m1654zw();
    }

    /* renamed from: S */
    public void mo10S() {
        super.mo10S();
        m1617hT(false);
        this.hki = 0;
        this.hkj = 0;
        m1580bi(new Vec3f());
        m1581bj(new Vec3f());
        this.hkp = false;
        m1615h(new Quat4fWrap(0.0f, 0.0f, 0.0f, 1.0f));
        m1651r(new Vec3d());
        this.hku = -1.0f;
        this.dEC = true;
    }

    /* renamed from: a */
    public void mo967a(C2961mJ mJVar) {
        super.mo967a(mJVar);
        m1617hT(false);
        this.hki = 0;
        this.hkj = 0;
        m1580bi(new Vec3f());
        m1581bj(new Vec3f());
        this.hkp = false;
        m1615h(new Quat4fWrap(0.0f, 0.0f, 0.0f, 1.0f));
        m1651r(new Vec3d());
        this.hku = -1.0f;
        this.dEC = true;
        if (bHa()) {
            mo620b((C0200h) new C0199g());
        }
    }

    @C0064Am(aul = "ab5e7dabab74b0caa5c5007e60c64ad7", aum = 0)
    private Actor cKX() {
        return this;
    }

    @C0064Am(aul = "0c68706197d451a4bf7bf4cba6ee5dfd", aum = 0)
    @C1253SX
    /* renamed from: e */
    private void m1609e(Space ea, long j) {
        if (isDisposed()) {
            logger.warn("A disposed Actor (" + bFY() + ") is being added to local space. Ignoring...");
            return;
        }
        if (this.hkf != null) {
            this.hkf.dispose();
            this.hkf = null;
        }
        if (cKL() != null && !cKL().cLZ()) {
            return;
        }
        if (this.cYZ == null) {
            mo620b((C0200h) new C0198f(ea, j));
        } else {
            mo1081j(ea, j);
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:24:0x0095  */
    /* JADX WARNING: Removed duplicated region for block: B:39:0x00e3  */
    @p001a.C0064Am(aul = "d71e7f5c542834162d59bc2c0cbca42d", aum = 0)
    /* renamed from: i */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void m1623i(Space r11, long r12) {
        /*
            r10 = this;
            r4 = 0
            boolean r0 = r10.bGX()
            if (r0 == 0) goto L_0x0016
            a.Tk r0 = r11.cKn()
            if (r0 != 0) goto L_0x0016
            a.aKp r0 = new a.aKp
            r0.<init>(r12, r10)
            r11.mo1896b((p001a.C5471aKp) r0)
        L_0x0015:
            return
        L_0x0016:
            a.HN r0 = r10.mo635d((p001a.C0338Ea) r11, (long) r12)
            r10.hkf = r0
            a.aJR r5 = r10.getPosition()
            boolean r0 = r10.bGY()
            if (r0 == 0) goto L_0x009a
            boolean r0 = r10 instanceof p001a.C2410fA
            if (r0 == 0) goto L_0x009a
            a.HN r0 = r10.hkf
            boolean r0 = r0.isStatic()
            if (r0 != 0) goto L_0x009a
            r2 = 0
            a.aJR r0 = r10.getPosition()
            a.rP r1 = r10.azW()
            a.aJR r1 = r1.getPosition()
            a.aJR r0 = r0.mo9517f((javax.vecmath.Tuple3d) r1)
            a.aJR r0 = r0.dfW()
            r1 = r0
            r3 = r4
            r6 = r4
        L_0x004a:
            a.aJR r0 = r10.getPosition()
            a.yK r7 = r10.mo958IL()
            float r7 = r7.getOuterSphereRadius()
            java.util.List r0 = r11.mo1900c((p001a.aJR) r0, (float) r7)
            java.util.Iterator r7 = r0.iterator()
        L_0x005e:
            boolean r0 = r7.hasNext()
            if (r0 != 0) goto L_0x00bf
        L_0x0064:
            if (r3 == 0) goto L_0x0117
            r0 = 2
            if (r6 <= r0) goto L_0x00e1
            r0 = 1148846080(0x447a0000, float:1000.0)
        L_0x006b:
            double r8 = (double) r0
            a.aJR r0 = r1.mo9476Z(r8)
            a.aJR r5 = r5.mo9504d((javax.vecmath.Tuple3d) r0)
            a.HN r1 = r10.hkf
            a.Om r1 = r1.mo2472kQ()
            a.aJR r1 = r1.blk()
            r1.mo9484aA(r5)
            a.HN r1 = r10.hkf
            a.aNW r1 = r1.aRh()
            r1.mo3841kO()
            int r6 = r6 + 1
            r3 = r4
        L_0x008d:
            if (r3 == 0) goto L_0x0093
            r1 = 10
            if (r6 < r1) goto L_0x0114
        L_0x0093:
            if (r3 == 0) goto L_0x00e3
            java.lang.String r0 = "I tried to find a suitable place, but this spawn is not safe."
            r10.mo8358lY(r0)
        L_0x009a:
            r10.hkz = r12
            a.iZ r0 = r10.cKM()
            java.util.Iterator r1 = r0.iterator()
        L_0x00a4:
            boolean r0 = r1.hasNext()
            if (r0 != 0) goto L_0x00fe
            a.adE r0 = r10.ald()
            if (r0 == 0) goto L_0x0015
            a.adE r0 = r10.ald()
            boolean r0 = r0.bhf()
            if (r0 == 0) goto L_0x0015
            r10.mo1080iw()
            goto L_0x0015
        L_0x00bf:
            java.lang.Object r0 = r7.next()
            a.CR r0 = (p001a.C0192CR) r0
            a.HN r8 = r0.cLJ()
            if (r8 == 0) goto L_0x005e
            a.HN r8 = r0.cLJ()
            a.HN r9 = r10.hkf
            boolean r8 = r8.mo2564b(r9)
            if (r8 == 0) goto L_0x005e
            a.yK r0 = r0.mo958IL()
            float r2 = r0.getOuterSphereRadius()
            r3 = 1
            goto L_0x0064
        L_0x00e1:
            r0 = r2
            goto L_0x006b
        L_0x00e3:
            if (r6 <= 0) goto L_0x009a
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            java.lang.String r1 = "It took me "
            r0.<init>(r1)
            java.lang.StringBuilder r0 = r0.append(r6)
            java.lang.String r1 = " iterations to find a suitable place."
            java.lang.StringBuilder r0 = r0.append(r1)
            java.lang.String r0 = r0.toString()
            r10.mo6317hy(r0)
            goto L_0x009a
        L_0x00fe:
            java.lang.Object r0 = r1.next()
            a.CR r0 = (p001a.C0192CR) r0
            boolean r2 = r0.cLZ()
            if (r2 != 0) goto L_0x00a4
            boolean r2 = r0.bae()
            if (r2 == 0) goto L_0x00a4
            r0.mo1065f(r11, r12)
            goto L_0x00a4
        L_0x0114:
            r1 = r0
            goto L_0x004a
        L_0x0117:
            r0 = r1
            goto L_0x008d
        */
        throw new UnsupportedOperationException("Method not decompiled: p001a.C0192CR.m1623i(a.Ea, long):void");
    }

    @C0064Am(aul = "8c6a008ad84bd053225e4949aed1a231", aum = 0)
    /* renamed from: c */
    private boolean m1602c(boolean z, String str) {
        if (z) {
            return true;
        }
        logger.warn(String.valueOf(str) + getDisplayName());
        return false;
    }

    @C4034yP
    @C0064Am(aul = "9eef3485ac80fae8da9a9611b65e287e", aum = 0)
    @C2499fr(mo18857qh = 4)
    @C1253SX
    /* renamed from: g */
    private void m1614g(Actor cr, C5260aCm acm, Vec3f vec3f, Vec3f vec3f2) {
        if (aiD() == null) {
            mo8358lY("Null Solid on client cause damage. The GFX for this damage may not appear.");
            mo1064f(cr, acm, vec3f, vec3f2);
            return;
        }
        mo995b(aiD().mo2472kQ(), cr, acm, vec3f, vec3f2);
    }

    @C0064Am(aul = "a9bfb81c062731fcb34ee0d63bf3f279", aum = 0)
    /* renamed from: a */
    private Vec3d m1544a(Vec3d ajr, Vec3d ajr2, Vec3d ajr3) {
        Vec3d ajr4 = new Vec3d(ajr3);
        if (ajr3.x < ajr2.x) {
            ajr4.x = ajr2.x;
        } else if (ajr3.x > ajr.x) {
            ajr4.x = ajr.x;
        }
        if (ajr3.y < ajr2.y) {
            ajr4.y = ajr2.y;
        } else if (ajr3.y > ajr.y) {
            ajr4.y = ajr.y;
        }
        if (ajr3.z < ajr2.z) {
            ajr4.z = ajr2.z;
        } else if (ajr3.z > ajr.z) {
            ajr4.z = ajr.z;
        }
        return ajr4;
    }

    @C0064Am(aul = "2e4f47080e95a533f548883e745163f8", aum = 0)
    @ClientOnly
    /* renamed from: a */
    private void m1555a(Vec3d ajr, Quat4fWrap aoy) {
        this.hks.setTransform(ajr, aoy);
    }

    @C0064Am(aul = "08fda20e31483af98da774d57b5c74fb", aum = 0)
    @ClientOnly
    private void afF() {
        Vec3d blk;
        Quat4fWrap bll;
        if (bGX()) {
            try {
                if (aiD() != null && !aiD().isDisposed() && cMs().mo959Ls() && this.hks != null && aiD() != null) {
                    C1003Om aSl = aiD().aSl();
                    blk = aSl.blk();
                    bll = aSl.bll();
                    if (bll.anA() && blk.anA()) {
                        mo996b(blk, bll);
                        if (aiD().mo2571qZ().anA()) {
                            this.hks.setVelocity(aiD().mo2571qZ());
                        }
                        if (aiD().aSf().anA()) {
                            this.hks.setAngularVelocity(aiD().aSf());
                        }
                    } else if (this.hkd < 2) {
                        logger.warn("Physical problems refreshing object");
                        this.hkd++;
                        mo1099zx();
                        cMC();
                    } else {
                        logger.warn("Physical problems droping object");
                        aia();
                    }
                }
            } catch (Exception e) {
                throw new IllegalArgumentException("Exception happened on copy data to render, quaternion is " + bll.toString() + " " + blk + " game.script:" + bFY(), e);
            } catch (Error e2) {
                throw e2;
            } catch (RuntimeException e3) {
                throw e3;
            }
        }
    }

    @C0064Am(aul = "158e2747b2a10645fef45edbcf29d86d", aum = 0)
    @C2198cg
    @C1253SX
    /* renamed from: c */
    private C0520HN m1596c(Space ea, long j) {
        C0520HN hn = new C0520HN(ea, this, mo958IL());
        hn.mo2449a(ea.cKn().mo5725a(j, (C2235dB) hn, 13));
        return hn;
    }

    @C0064Am(aul = "043ac92b5a2e90b3caa870494890c31f", aum = 0)
    /* renamed from: fg */
    private void m1613fg() {
        if (cLZ()) {
            mo1099zx();
        }
        if (bae() && mo960Nc() != null) {
            mo1099zx();
        }
        super.dispose();
    }

    @C0064Am(aul = "9113be1d57614e0684078e63d77a3dfa", aum = 0)
    /* renamed from: bu */
    private float m1590bu(Actor cr) {
        return (float) Math.sqrt((double) mo1013bx(cr));
    }

    @C0064Am(aul = "d2ac2f85b7f3e2c501cd31280acd4d50", aum = 0)
    /* renamed from: bw */
    private float m1592bw(Actor cr) {
        if (cr.mo958IL() == null || aSp().mo958IL() == null) {
            return getPosition().mo9486aC(cr.getPosition());
        }
        Vec3d d = aSp().mo958IL().getBoundingBox().dqQ().mo23485d((Tuple3d) aSp().getPosition());
        Vec3d d2 = aSp().mo958IL().getBoundingBox().dqP().mo23485d((Tuple3d) aSp().getPosition());
        if (cr.mo958IL() instanceof C6348alI) {
            Vec3d position2 = cr.getPosition();
            double n = Vec3d.m15653n(position2, m1570b(d, d2, position2)) - ((double) ((C6348alI) cr.mo958IL()).getRadius());
            if (n < ScriptRuntime.NaN) {
                n = 0.0d;
            }
            return (float) (n * n);
        }
        BoundingBox boundingBox = cr.mo958IL().getBoundingBox();
        Vec3d position3 = cr.getPosition();
        Vec3d d3 = boundingBox.dqQ().mo23485d((Tuple3d) position3);
        Vec3d d4 = boundingBox.dqP().mo23485d((Tuple3d) position3);
        double d5 = 0.0d;
        for (int i = 0; i < 3; i++) {
            if (d3.get(i) < d2.get(i)) {
                double d6 = d2.get(i) - d3.get(i);
                d5 += d6 * d6;
            } else if (d.get(i) < d4.get(i)) {
                double d7 = d4.get(i) - d.get(i);
                d5 += d7 * d7;
            }
        }
        return (float) d5;
    }

    @C0064Am(aul = "17fdd51668c85d58af3e4aff45e1a8e3", aum = 0)
    @C2198cg
    /* renamed from: a */
    private void m1545a(int i, long j, Collection<C6625aqZ> collection, C6705asB asb) {
        if (cLd() != null) {
            asb.mo13024a(collection, (C6950awp) new C4031yM(i, this, cLd().mo2472kQ(), j));
        }
    }

    @C0064Am(aul = "446b2c3094b81c4163b99a33e5a69daf", aum = 0)
    private Actor cKY() {
        return cKL();
    }

    @C0064Am(aul = "9c2ef509ce3eb0c09493a6c34cbb7860", aum = 0)
    private Node cLa() {
        Node azW = azW();
        if (azW == null) {
            return null;
        }
        if (azW.mo21600H(this)) {
            return azW;
        }
        return null;
    }

    @C0064Am(aul = "63495ab62d04d7db8589527810c7610a", aum = 0)
    @C2198cg
    private C0520HN cLc() {
        return aiD();
    }

    @C0064Am(aul = "44f79aa977cb0801f7eafdc24cc4efcb", aum = 0)
    /* renamed from: Zb */
    private Space m1541Zb() {
        return m1540YY();
    }

    @C0064Am(aul = "d7dd6238ba953ab35a7a0f7051a0697c", aum = 0)
    private Vec3f cLe() {
        return cKQ();
    }

    @C0064Am(aul = "758b859c5bb97ba98c08722a7cd4a508", aum = 0)
    private Vec3f cLg() {
        return cKR();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Orientation")
    @C0064Am(aul = "2f73c77fce7fad25cdde733011781569", aum = 0)
    private Quat4fWrap cLi() {
        return cKU();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Position")
    @C0064Am(aul = "8977249d6fcd08ba4bd1c6106393227d", aum = 0)
    private Vec3d cLk() {
        return m1539YX();
    }

    @C0064Am(aul = "2972874d0d62f772b8ddc659b0aececc", aum = 0)
    private float cLm() {
        if (!cLP()) {
            return 0.0f;
        }
        return aiD().aSj();
    }

    @C0064Am(aul = "3973e395306065beb941f8b74f05f65e", aum = 0)
    private float cLn() {
        if (!cLP()) {
            return 0.0f;
        }
        return aiD().aSg();
    }

    @C0064Am(aul = "4e5df5f1bf7acc432ea9f1681d58c1d8", aum = 0)
    private float cLo() {
        if (!cLP()) {
            return 0.0f;
        }
        return aiD().aSh();
    }

    @C0064Am(aul = "90ad6d89de22000472ff4d53681c1fcd", aum = 0)
    private float cLp() {
        if (!cLP()) {
            return 0.0f;
        }
        return aiD().aSi();
    }

    @C0064Am(aul = "e63bf092e0f67286bcd2db1e96aa0787", aum = 0)
    private String cLq() {
        Pawn cLw = cMs().cLw();
        String str = "";
        if (cMs() instanceof Actor) {
            str = String.valueOf(str) + cMs().getName() + " ";
        }
        String str2 = String.valueOf(str) + cMs().getClass().getSimpleName() + ": ";
        if (cLw != null) {
            return String.valueOf(str2) + cLw.getName();
        }
        return String.valueOf(str2) + cMs().mo648ip() + " (?)";
    }

    @C0064Am(aul = "cafb43d9b89caa1d5324607852f9c610", aum = 0)
    /* renamed from: by */
    private float m1594by(Actor cr) {
        return (float) Math.sqrt((double) mo1001bB(cr));
    }

    @C0064Am(aul = "25524b7915496930f55a846b63388883", aum = 0)
    /* renamed from: bA */
    private float m1575bA(Actor cr) {
        return (float) Math.max((getPosition().mo9517f((Tuple3d) cr.getPosition()).lengthSquared() - ((double) mo958IL().getBoundingBox().diu())) - ((double) cr.mo958IL().getBoundingBox().diu()), ScriptRuntime.NaN);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Enable Fog of war")
    @C0064Am(aul = "9525a92efc7f051004d704ae3d7a0eac", aum = 0)
    private boolean cLr() {
        return cKO();
    }

    @C0064Am(aul = "684506448de994d5784b98fff4acb394", aum = 0)
    private Vec3f cLt() {
        return mo1009br(mo1090qZ());
    }

    @C0064Am(aul = "e9163c6f9a033ea2aaa0c6de979aac85", aum = 0)
    private Pawn cLv() {
        return cKP();
    }

    @C0064Am(aul = "ee0dd8d60dab8728d881484339c19223", aum = 0)
    private Vec3f aZV() {
        if (cLZ()) {
            return cLR();
        }
        return cKQ();
    }

    @C0064Am(aul = "57330e52407f5615fdcdb82059dbc3c5", aum = 0)
    private Vec3f aZW() {
        if (cLZ()) {
            return cLT();
        }
        return cKR();
    }

    @C0064Am(aul = "eebbb815edb9de1cfecf6ccf23707ba9", aum = 0)
    private Vec3f cLx() {
        if (cLZ()) {
            return cLT().dfQ();
        }
        return cKR().dfQ();
    }

    @C0064Am(aul = "ffcdddb1ceb6a6f8ee65577c81edd4dc", aum = 0)
    /* renamed from: VE */
    private float m1535VE() {
        return 0.0f;
    }

    @C0064Am(aul = "95d58756e985b38878579ccb0fde0272", aum = 0)
    /* renamed from: VG */
    private float m1536VG() {
        return 0.0f;
    }

    @C0064Am(aul = "51efcd87430f850af9603c792eafb977", aum = 0)
    /* renamed from: VI */
    private float m1537VI() {
        return 0.0f;
    }

    @C0064Am(aul = "c65104f18429142b59e3438c7f2682eb", aum = 0)
    /* renamed from: VJ */
    private float m1538VJ() {
        return 0.0f;
    }

    @C0064Am(aul = "d4ab3e57b56733fbefa848beb877ea70", aum = 0)
    private float aZX() {
        return -1.0f;
    }

    @C0064Am(aul = "b7f47c949e7cedda01aeb5ea834819ed", aum = 0)
    /* renamed from: uV */
    private String m1653uV() {
        return cKS().get();
    }

    @C0064Am(aul = "d99600991f86fefca50640fedba5ae27", aum = 0)
    private String ahP() {
        return getName();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "NameI")
    @C0064Am(aul = "6649c13802cb082c30416931f222144f", aum = 0)
    private I18NString cLz() {
        return cKS();
    }

    @C0064Am(aul = "cf4ac236276ae2107d553ae0f91e252d", aum = 0)
    private Quat4fWrap cLB() {
        if (cLZ()) {
            return cLV();
        }
        return cKU();
    }

    @C0064Am(aul = "f6832587c00b8edb3078ba3d9252118b", aum = 0)
    /* renamed from: Zd */
    private Vec3d m1542Zd() {
        if (cLZ()) {
            return cLX();
        }
        return m1539YX();
    }

    @C0064Am(aul = "08b6c24ea50747fbde0a517086861e20", aum = 0)
    /* renamed from: b */
    private void m1573b(Vec3f vec3f, Vec3d ajr) {
        Quat4fWrap orientation2 = getOrientation();
        Vec3f vec3f2 = new Vec3f();
        orientation2.mo15204F(vec3f, vec3f2);
        ajr.mo9484aA(getPosition());
        ajr.mo9533r((Tuple3f) vec3f2);
    }

    @C0064Am(aul = "30f9a4f5116854d7babfaa1c68cdb788", aum = 0)
    /* renamed from: j */
    private void m1634j(Vec3d ajr, Vec3d ajr2) {
        Quat4fWrap orientation2 = getOrientation();
        Vec3d ajr3 = new Vec3d();
        Quat4fWrap.m24425b(orientation2, ajr, ajr3);
        ajr2.mo9484aA(getPosition());
        ajr2.mo9512e((Tuple3d) ajr3);
    }

    @C0064Am(aul = "81842d9bf0f5ace91dfffb9b1c6dffcc", aum = 0)
    @ClientOnly
    private SceneObject cLC() {
        return this.hks;
    }

    @C0064Am(aul = "5fc86dc267891806f8738dc2bf287cbb", aum = 0)
    @ClientOnly
    private TransformWrap cLD() {
        if (this.hks != null) {
            return this.hks.getTransform();
        }
        return new TransformWrap();
    }

    @C0064Am(aul = "6a91187a4e8e4a80240ccf447497749d", aum = 0)
    @ClientOnly
    private TransformWrap cLF() {
        if (this.hks != null) {
            return this.hks.getGlobalTransform();
        }
        return new TransformWrap();
    }

    @C0064Am(aul = "4f042ec0f08a894e4c4e0d383d6fe31b", aum = 0)
    private C4029yK cLH() {
        return this.cYZ;
    }

    @C0064Am(aul = "d853ae5b9bcd3f07142708b3c7b51a97", aum = 0)
    private C0520HN cLI() {
        return aiD();
    }

    @C0064Am(aul = "1555b75712594ba62312ed9ad588b570", aum = 0)
    /* renamed from: Nb */
    private StellarSystem m1532Nb() {
        return m1531MX();
    }

    @C0064Am(aul = "6406c5736d27b192609d506e6bcb8e50", aum = 0)
    /* renamed from: d */
    private void m1605d(StellarSystem jj) {
        m1598c(jj);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Stellar System")
    @C0064Am(aul = "222cbc6b66be770b9e1134835273403e", aum = 0)
    private String cLK() {
        if (m1531MX() != null) {
            return m1531MX().mo3250ke().get();
        }
        return "null";
    }

    @C0064Am(aul = "989a4247e5cfb1e152a9e212684416d1", aum = 0)
    /* renamed from: l */
    private Vec3d[] m1639l(Vec3d ajr, Vec3d ajr2) {
        return new Vec3d[]{new Vec3d(ajr.x, ajr.y, ajr.z), new Vec3d(ajr2.x, ajr.y, ajr.z), new Vec3d(ajr.x, ajr2.y, ajr.z), new Vec3d(ajr2.x, ajr2.y, ajr.z), new Vec3d(ajr.x, ajr.y, ajr2.z), new Vec3d(ajr2.x, ajr.y, ajr2.z), new Vec3d(ajr.x, ajr2.y, ajr2.z), new Vec3d(ajr2.x, ajr2.y, ajr2.z)};
    }

    @C0064Am(aul = "8192ebd1a146b03b042a02f59f3e459b", aum = 0)
    /* renamed from: Zh */
    private float m1543Zh() {
        return 0.0f;
    }

    @C0064Am(aul = "b035d157ea8c337eb0d63431aaa3e363", aum = 0)
    @ClientOnly
    private boolean cLM() {
        return cHg() != null;
    }

    @C0064Am(aul = "6da6ab6c366b06ba03a2878dd414c8cc", aum = 0)
    /* renamed from: Lr */
    private boolean m1530Lr() {
        return (mo648ip() == null || mo649ir() == null) ? false : true;
    }

    @C0064Am(aul = "dc5614b7f8c77b98c9dddd24befafc22", aum = 0)
    private boolean cLO() {
        return aiD() != null;
    }

    @C0064Am(aul = "7f1904cf9dc542b34b28046124a3d483", aum = 0)
    private Vec3f cLQ() {
        if (!mo1060d(aiD() != null, "This actor doesn't have a physical representation ")) {
            return new Vec3f(0.0f, 0.0f, 0.0f);
        }
        return aiD().aSf();
    }

    @C0064Am(aul = "c63a2c7832eb2e56148c2b0d45b1cc06", aum = 0)
    private Vec3f cLS() {
        if (!mo1060d(aiD() != null, "This actor doesn't have a physical representation ")) {
            return new Vec3f(0.0f, 0.0f, 0.0f);
        }
        return new Vec3f((Vector3f) aiD().mo2571qZ());
    }

    @C0064Am(aul = "25b25c96daea0a77b4728227ad26ca95", aum = 0)
    private Quat4fWrap cLU() {
        if (!mo1060d(aiD() != null, "This actor doesn't have a physical representation ")) {
            return new Orientation(1.0f, 0.0f, 0.0f, 0.0f);
        }
        if (!bGX()) {
            return aiD().getOrientation();
        }
        return aiD().aSd();
    }

    @C0064Am(aul = "7924b24659970bc9828efc0052654a08", aum = 0)
    private Vec3d cLW() {
        Vec3d aSe;
        if (!mo1060d(aiD() != null, "This actor doesn't have a physical representation ")) {
            return new Vec3d(ScriptRuntime.NaN, ScriptRuntime.NaN, ScriptRuntime.NaN);
        }
        if (!bGX()) {
            aSe = aiD().getPosition();
        } else {
            aSe = aiD().aSe();
        }
        return aSe;
    }

    @C0064Am(aul = "32bf87581e9c99611eb09d8a0e21c22f", aum = 0)
    /* renamed from: bk */
    private void m1583bk(Vec3f vec3f) {
        if (mo1060d(aiD() != null, "This actor doesn't have a physical representation ")) {
            if (!C5877acF.RUNNING_CLIENT_BOT) {
                logger.warn("Ang Velocity is beeing set on a flying object. Are you sure? -> " + cMs());
            }
            aiD().mo2541a(-1, aiD().getPosition(), aiD().getOrientation(), aiD().mo2571qZ(), vec3f);
        }
    }

    @C0064Am(aul = "d780edbadbb1b833fd5c757e238103ef", aum = 0)
    /* renamed from: bm */
    private void m1585bm(Vec3f vec3f) {
        if (mo1060d(aiD() != null, "This actor doesn't have a physical representation ")) {
            if (!C5877acF.RUNNING_CLIENT_BOT) {
                logger.warn("Lin Velocity is beeing set on a flying object. Are you sure? -> " + cMs());
            }
            aiD().mo2541a(-1, aiD().getPosition(), aiD().getOrientation(), vec3f, aiD().aSf());
        }
    }

    @C0064Am(aul = "b70bed4f41a90af57f324ad803f87583", aum = 0)
    /* renamed from: i */
    private void m1624i(Quat4fWrap aoy) {
        Quat4fWrap aoy2;
        mo1060d(aiD() != null, "This actor doesn't have a physical representation ");
        if (!C5877acF.RUNNING_CLIENT_BOT) {
            logger.warn("Orientation is beeing set on a flying object. Are you sure? -> " + cMs());
        }
        if (!aoy.anA()) {
            logger.error("***********************************************************************");
            logger.error("relativePosition sum is not finite: " + aoy);
            logger.error("reseting it to zero and ignoring the error");
            logger.error("***********************************************************************");
            aoy2 = new Orientation();
        } else {
            aoy2 = aoy;
        }
        aiD().mo2541a(-1, aiD().getPosition(), aoy2, aiD().mo2571qZ(), aiD().aSf());
    }

    @C0064Am(aul = "15b3fac6863fade80b3851989c5ac499", aum = 0)
    /* renamed from: ag */
    private void m1561ag(Vec3d ajr) {
        Vec3d ajr2;
        //У этого актера нет физического представления
        if (mo1060d(aiD() != null, "This actor doesn't have a physical representation ")) {
            if (!ajr.anA()) {
                logger.error("***********************************************************************");
                logger.error("relativePosition sum is not finite: " + ajr);
                logger.error("reseting it to zero and ignoring the error");
                logger.error("***********************************************************************");
                ajr2 = new Vec3d(ScriptRuntime.NaN, ScriptRuntime.NaN, ScriptRuntime.NaN);
            } else {
                ajr2 = ajr;
            }
            if (!C5877acF.RUNNING_CLIENT_BOT) {
                logger.warn("Position is beeing set on a flying object. Are you sure? -> " + cMs());
            }
            aiD().mo2541a(-1, ajr2, aiD().getOrientation(), aiD().mo2571qZ(), aiD().aSf());
        }
    }

    @C0401Fa
    @C0064Am(aul = "926b3cf528a22260a7666255599f9c48", aum = 0)
    /* renamed from: hX */
    private void m1621hX(boolean z) {
        if (aiD() != null) {
            throw new IllegalStateException("Can't set static while simulating");
        }
    }

    @C0064Am(aul = "1b89c3eb8dc4ec3bb4f721193bd9d582", aum = 0)
    private boolean cLY() {
        return aiD() != null;
    }

    @C0064Am(aul = "a610f101fff0504aa68d6b7da771c0e6", aum = 0)
    private boolean cMa() {
        return cKO();
    }

    @C0064Am(aul = "c21f0ba6371a9adfdf9d7b53e466f06d", aum = 0)
    private boolean cMc() {
        return cKT();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Spawned")
    @C0064Am(aul = "524ce9269c9dd540ec3732f287cb383a", aum = 0)
    private boolean bad() {
        return cKV();
    }

    @C0064Am(aul = "69a6ac4cec17840625f70f1142a9febf", aum = 0)
    @ClientOnly
    private boolean baf() {
        if (bGX()) {
            return cLZ();
        }
        //isSpawnedOnClient должен вызываться только на стороне клиента
        throw new RuntimeException("isSpawnedOnClient must be called only on client side");
    }

    @C0064Am(aul = "3a0616dff6367f0f4abb6975a1da823b", aum = 0)
    private boolean aMM() {
        return cKN();
    }

    @C0064Am(aul = "0b375d05b58ca7ad0eee054d4ea6509a", aum = 0)
    /* renamed from: h */
    private boolean m1616h(UserConnection apk) {
        Player dL;
        Ship bQx;
        C0520HN aiD = aiD();
        if (aiD == null || (dL = apk.mo15388dL()) == null || dL.bQB() || (bQx = dL.bQx()) == null) {
            return false;
        }
        if ((bQx != cMs() || !dL.dxc().cNT()) && bQx.getPosition().mo9486aC(aiD.getPosition()) <= 1.41006541E9f) {
            return true;
        }
        return false;
    }

    @C0401Fa
    @C0064Am(aul = "fc2e3aafdd515fdf9b75e1a7d231aafc", aum = 0)
    /* renamed from: hZ */
    private void m1622hZ(boolean z) {
        if (aiD() == null) {
            return;
        }
        if (z || !aiD().aSc()) {
            aiD().mo2565cO(z);
            return;
        }
        throw new IllegalStateException("On off collision can only be set, never reseted");
    }

    @C0401Fa
    @C0064Am(aul = "6dbc2f15252ca3be308de9581aa0065d", aum = 0)
    /* renamed from: d */
    private void m1607d(C4029yK yKVar) {
        if (aiD() != null) {
            throw new IllegalStateException("Can't change shape while simulating");
        }
    }

    @C0064Am(aul = "e20ea1ec9f73a52cac6732f12be4ad69", aum = 0)
    @ClientOnly
    /* renamed from: fP */
    private void m1612fP(float f) {
        if (this.hks != null && (this.hks instanceof RenderObject)) {
            RenderObject renderObject = (RenderObject) this.hks;
            Color primitiveColor = renderObject.getPrimitiveColor();
            primitiveColor.w = f;
            renderObject.setPrimitiveColor(primitiveColor);
        }
    }

    @C0064Am(aul = "fc11662772ed4d3cc37112a41b35d214", aum = 0)
    @ClientOnly
    private void cxD() {
        if (ald().alb() != null) {
            ald().alb().mo3287ar(this);
            if (bGX()) {
                this.hkc = new C0194b();
            }
        }
    }

    @C0064Am(aul = "a25fc60c9daad6b1a9614a56adc0bce7", aum = 0)
    @ClientOnly
    private void cxF() {
        if (ald() != null && ald().bhf() && ald().getEventManager() != null) {
            ald().getEventManager().mo13975h(new C2041bS(this));
        }
    }

    @C0064Am(aul = "0fbff0ad81302ac84856a1c68783b9d1", aum = 0)
    @C5307aEh
    /* renamed from: a */
    private void m1549a(Actor cr, Vec3f vec3f, Vec3f vec3f2, float f, C6339akz akz) {
    }

    @C0064Am(aul = "2865a5bdfab225d257ae3ddff1a00de2", aum = 0)
    /* renamed from: jJ */
    private void m1636jJ(long j) {
    }

    @C0064Am(aul = "6922361ca76be0ae2471f7cc29872adc", aum = 0)
    @ClientOnly
    private void ahp() {
        ald().alb().mo22073bN(this);
    }

    @C0064Am(aul = "d2ae1653eb77397d51e1ffec85044c42", aum = 0)
    @ClientOnly
    /* renamed from: iB */
    private void m1626iB() {
        ald().alb().mo3288at(this);
    }

    @C0064Am(aul = "bf766335daf3747cd194b07702c90511", aum = 0)
    /* renamed from: aG */
    private void m1560aG() {
        super.mo70aH();
        m1620hW(m1531MX() != null && m1531MX().aXJ().contains(this));
        if (bHa()) {
            mo620b((C0200h) new C0195c());
        }
    }

    @C0064Am(aul = "679826008219ae8df09676b943d3898d", aum = 0)
    @ClientOnly
    private void ahs() {
        Vec3d position2 = cMs().getPosition();
        Quat4fWrap orientation2 = cMs().getOrientation();
        try {
            this.hks.setPosition(position2);
            this.hks.setOrientation(orientation2);
            mo957Fe();
            if (m1584bk(this)) {
                cMe();
            } else {
                m1610e((C1706ZG.C1707a) new C0196d());
            }
            mo1055ca(mo651iu());
        } catch (Exception e) {
            throw new IllegalArgumentException("Exception happened on copy data to render, quaternion is " + orientation2.toString() + " " + position2 + " game.script:" + bFY(), e);
        }
    }

    @C0064Am(aul = "41fd612518cf661c7d76096d0008b1a3", aum = 0)
    @ClientOnly
    /* renamed from: bZ */
    private void m1579bZ(String str) {
        if (str != null) {
            ILoaderTrail alg = ald().getLoaderTrail();
            for (CollisionFXSet apu : ala().aLc()) {
                Iterator<C6909avx> it = apu.apu().iterator();
                while (true) {
                    if (!it.hasNext()) {
                        break;
                    }
                    C6909avx next = it.next();
                    if (next.czv().equals(str)) {
                        if (next.czp() != null) {
                            alg.mo4995a(next.czp().getFile(), (String) null, (Scene) null, (C0907NJ) null, "Preloading actor FX");
                        }
                        if (next.czr() != null) {
                            alg.mo4995a(next.czr().getFile(), (String) null, (Scene) null, (C0907NJ) null, "Preloading actor FX");
                        }
                        if (next.czt() != null) {
                            alg.mo4995a(next.czt().getFile(), (String) null, (Scene) null, (C0907NJ) null, "Preloading actor FX");
                        }
                    }
                }
            }
        }
    }

    @C0064Am(aul = "6d4cdfe5d0b31c8b98d611bed2dc678d", aum = 0)
    /* renamed from: bj */
    private boolean m1582bj(Actor cr) {
        return (cr instanceof Shot) || (cr instanceof Missile) || (cr instanceof Mine) || (cr instanceof MissionTrigger) || cr.equals(getPlayer().bQx());
    }

    @C0064Am(aul = "c7538b323f9be482a5a11d48009841bf", aum = 0)
    private void cMd() {
        if (this.hks != null) {
            Scene adZ = ald().getEngineGraphics().adZ();
            if ((this instanceof Station) || (this instanceof Asteroid) || (this instanceof Advertise) || (this instanceof Scenery) || (this instanceof Gate)) {
                adZ.addStaticChild(this.hks);
            } else {
                adZ.addChild(this.hks);
            }
            cMA();
            if (ala().aLS().adu()) {
                cMg();
            }
        }
    }

    @C0064Am(aul = "a8d6c9e74c106b88321cde9e3fdb9cd0", aum = 0)
    private void cMf() {
        if (mo958IL() instanceof C2384eh) {
            this.hkC = new RLine();
            this.hkC.setMaterial(new Material());
            this.hkC.getMaterial().setShader(new Shader());
            this.hkC.getMaterial().getShader().setPass(0, new ShaderPass());
            aRF arf = (aRF) ((C2384eh) mo958IL()).aQF();
            Vec3f[] vec3fArr = {new Vec3f(), new Vec3f(), new Vec3f()};
            Vec3f vec3f = new Vec3f((Vector3f) arf.getScaling());
            for (C3901wW next : arf.dtP()) {
                for (int i = 0; i < next.bFf; i++) {
                    int i2 = next.bFh * i;
                    int i3 = next.bFg.getInt(i2 + 0) * next.bFj;
                    vec3fArr[0].set(next.bFi.getFloat(i3 + 0) * vec3f.x, next.bFi.getFloat(i3 + 4) * vec3f.y, next.bFi.getFloat(i3 + 8) * vec3f.z);
                    int i4 = next.bFg.getInt(i2 + 4) * next.bFh;
                    vec3fArr[1].set(next.bFi.getFloat(i4 + 0) * vec3f.x, next.bFi.getFloat(i4 + 4) * vec3f.y, next.bFi.getFloat(i4 + 8) * vec3f.z);
                    int i5 = next.bFg.getInt(i2 + 8) * next.bFh;
                    vec3fArr[2].set(next.bFi.getFloat(i5 + 0) * vec3f.x, next.bFi.getFloat(i5 + 4) * vec3f.y, next.bFi.getFloat(i5 + 8) * vec3f.z);
                    this.hkC.addPoint(vec3fArr[0]);
                    this.hkC.addPoint(vec3fArr[1]);
                    this.hkC.addPoint(vec3fArr[2]);
                }
            }
            this.hkC.setGeometryType(GeometryType.POINTS);
            this.hks.addChild(this.hkC);
        }
        if (mo958IL() instanceof C1223SC) {
            this.hkC = new RLine();
            this.hkC.setMaterial(new Material());
            this.hkC.getMaterial().setShader(new Shader());
            this.hkC.getMaterial().getShader().setPass(0, new ShaderPass());
            for (Vec3f addPoint : ((C1223SC) mo958IL()).bsr()) {
                this.hkC.addPoint(addPoint);
            }
            this.hkC.setGeometryType(GeometryType.POINTS);
            this.hks.addChild(this.hkC);
        }
        if (mo958IL() instanceof C0512HH) {
            for (C0512HH.C0513a aVar : ((C0512HH) mo958IL()).mo2516Jo()) {
                if (aVar.mo2519IL() instanceof C3601so) {
                    C3601so soVar = (C3601so) aVar.mo2519IL();
                    this.hkC = new RLine();
                    this.hkC.setMaterial(new Material());
                    this.hkC.getMaterial().setShader(new Shader());
                    this.hkC.getMaterial().getShader().setPass(0, new ShaderPass());
                    float f = soVar.aby().x / 2.0f;
                    float f2 = soVar.aby().y / 2.0f;
                    float f3 = soVar.aby().z / 2.0f;
                    this.hkC.addPoint(new Vec3f(-f, f2, -f3));
                    this.hkC.addPoint(new Vec3f(f, f2, -f3));
                    this.hkC.addPoint(new Vec3f(f, -f2, -f3));
                    this.hkC.addPoint(new Vec3f(-f, -f2, -f3));
                    this.hkC.addPoint(new Vec3f(f, f2, -f3));
                    this.hkC.addPoint(new Vec3f(f, f2, f3));
                    this.hkC.addPoint(new Vec3f(f, -f2, f3));
                    this.hkC.addPoint(new Vec3f(f, -f2, -f3));
                    this.hkC.addPoint(new Vec3f(f, f2, f3));
                    this.hkC.addPoint(new Vec3f(-f, f2, f3));
                    this.hkC.addPoint(new Vec3f(-f, -f2, f3));
                    this.hkC.addPoint(new Vec3f(f, -f2, f3));
                    this.hkC.addPoint(new Vec3f(-f, f2, f3));
                    this.hkC.addPoint(new Vec3f(-f, f2, -f3));
                    this.hkC.addPoint(new Vec3f(-f, -f2, -f3));
                    this.hkC.addPoint(new Vec3f(-f, -f2, f3));
                    this.hkC.addPoint(new Vec3f(-f, f2, f3));
                    this.hkC.addPoint(new Vec3f(f, f2, f3));
                    this.hkC.addPoint(new Vec3f(f, f2, -f3));
                    this.hkC.addPoint(new Vec3f(-f, f2, -f3));
                    this.hkC.addPoint(new Vec3f(-f, -f2, -f3));
                    this.hkC.addPoint(new Vec3f(f, -f2, -f3));
                    this.hkC.addPoint(new Vec3f(f, -f2, f3));
                    this.hkC.addPoint(new Vec3f(-f, -f2, f3));
                    this.hkC.setTransform(aVar.getTransform());
                    this.hkC.setGeometryType(GeometryType.QUADS);
                    this.hks.addChild(this.hkC);
                }
            }
        }
    }

    @C0064Am(aul = "39bfa99708740c8a70a57df60f21435a", aum = 0)
    private void ahT() {
        if (bGX() && this.hks != null) {
            ald().getEngineGraphics().adZ().removeChild(this.hks);
            Ship bQx = getPlayer().bQx();
            if (m1584bk(this) || !bQx.cLZ() || ((bQx.mo960Nc() != null && bQx.mo960Nc().equals(mo960Nc())) || ((this instanceof Pawn) && ((Pawn) this).isDead()))) {
                cMi();
            } else {
                m1600c((C1706ZG.C1707a) new C0197e());
            }
        }
    }

    @C0064Am(aul = "6d8333f699af0939976a835bab870477", aum = 0)
    @ClientOnly
    private void cMh() {
        aSp().cxG();
        if (this.hks != null) {
            this.hks.dispose();
        }
        this.hks = null;
        this.hkB = false;
        if (this.hkt != null) {
            this.hkt.dispose();
            this.hkt = null;
        }
        if (this.hkg != null) {
            this.hkg.hide();
            this.hkg.destroy();
            this.hkg = null;
        }
        mo1076iC();
    }

    @C0064Am(aul = "993e24837f8214f8ca2df0c6996572ab", aum = 0)
    /* renamed from: b */
    private void m1572b(C1706ZG.C1707a aVar) {
        if (this.hkx == null) {
            this.hkx = new C1706ZG();
        }
        this.hkx.mo7338a(aVar);
        this.hkx.mo7337a(this, 3.0f, new Color(1.0f, 1.0f, 1.0f, 1.0f), new Color(1.0f, 1.0f, 1.0f, 0.0f));
    }

    @C0064Am(aul = "755485cf1fda57b2c3dcd54b29b83cf0", aum = 0)
    /* renamed from: d */
    private void m1606d(C1706ZG.C1707a aVar) {
        if (this.hkx == null) {
            this.hkx = new C1706ZG();
        }
        this.hkx.mo7338a(aVar);
        this.hkx.mo7337a(this, 3.0f, new Color(1.0f, 1.0f, 1.0f, 0.0f), new Color(1.0f, 1.0f, 1.0f, 1.0f));
    }

    @C0064Am(aul = "b5b4086db53626d30fda904b64b35c7d", aum = 0)
    @C5307aEh
    /* renamed from: a */
    private void m1546a(long j, boolean z) {
        if (!isDisposed()) {
            if (z) {
                this.hkp = true;
            }
            if (!this.hkp) {
                return;
            }
            if (!cLZ()) {
                logger.warn("SimulatedInfra : Sending Simulation data Update for SimulatedInfa that has no simulated");
            } else if (!bHa()) {
                this.hkp = false;
                afG();
            } else if (j - this.hki > hjW) {
                cMs().mo997b(aiD().getPosition(), aiD().getOrientation(), new Vec3f((Vector3f) aiD().mo2571qZ()), new Vec3f((Vector3f) aiD().aSf()));
                this.hki = j;
                this.hkp = false;
            }
        }
    }

    @C0064Am(aul = "f867d8b5c5c512a10e19a6e82ded2602", aum = 0)
    private void cMj() {
        mo8362mt(1.0f).dispose();
    }

    @C0064Am(aul = "f6113d59159b9c66cbc1f8331f63b25c", aum = 0)
    @ClientOnly
    /* renamed from: iv */
    private boolean m1632iv() {
        if (cMo()) {
            return false;
        }
        boolean cMm = cMm();
        if (!cMm) {
            logger.warn("Not added to simulation because Actor was not added");
            return cMm;
        } else if (!bGX()) {
            return cMm;
        } else {
            if (aSp().mo647iA() != null && aSp().mo647iA().length() > 0 && ald().getEngineGraphics() != null && this.hkg == null) {
                this.hkg = new C6017aep(this);
            }
            aSp().cxE();
            return cMm;
        }
    }

    @C0064Am(aul = "18a77410d3c6805b828f85020c308d2d", aum = 0)
    private boolean cMl() {
        boolean z;
        boolean z2;
        boolean z3;
        boolean z4;
        IEngineGraphics ale;
        if (C5877acF.RUNNING_CLIENT_BOT && aiD() != null) {
            return false;
        }
        if (!mo1060d(!bGX() || this.hks == null, "This actor already have a render representation: ")) {
            return false;
        }
        if (cMs().mo960Nc() != null) {
            z = true;
        } else {
            z = false;
        }
        if (!mo1060d(z, "This actor need to be in a Stellar System: ")) {
            return false;
        }
        if (cMs().getPosition() != null) {
            z2 = true;
        } else {
            z2 = false;
        }
        if (!mo1060d(z2, "This actor need to have a position: ")) {
            return false;
        }
        if (cMs().getOrientation() != null) {
            z3 = true;
        } else {
            z3 = false;
        }
        if (!mo1060d(z3, "This actor need to have a orientation: ")) {
            return false;
        }
        if (cMs().mo958IL() != null) {
            z4 = true;
        } else {
            z4 = false;
        }
        if (!mo1060d(z4, "This actor need to have a shape: ")) {
            return false;
        }
        if (cMs().cKZ() != null && cMs().cKZ() == null) {
            throw new RuntimeException("Attached simulatable is not spawned . Actor is " + getName());
        } else if (!mo1060d(cMs().cLj().anA(), "Orientation is invalid")) {
            return false;
        } else {
            aiD().mo2565cO(cMs().aSc());
            if (bGX() && (ale = ald().getEngineGraphics()) != null && cMs().mo959Ls()) {
                String ir = cMs().mo649ir();
                String ip = cMs().mo648ip();
                if (this instanceof MissionTrigger) {
                    this.hks = new SceneObject();
                } else {
                    this.hks = (SceneObject) ald().getLoaderTrail().mo5004l(ir, ip);
                }
                if (this.hks == null) {
                    ald().getLoaderTrail().mo4995a(ir, ip, (Scene) null, this, "SimulatedInfra:AddToSimulation for simulated " + cMs().bFY());
                } else {
                    ale.mo3007a((RenderTask) new C0193a(this.hks));
                }
            }
            return true;
        }
    }

    @C0064Am(aul = "06a8a4026b47f9eef28c8fb736b1986f", aum = 0)
    @ClientOnly
    private void ahV() {
        if (this instanceof Ship) {
            System.out.println("Remove " + this + " from local space. Space" + mo965Zc().mo1916ke().get());
        }
        if (!(getPlayer().bQx() == null || getPlayer().bQx().mo965Zc() == null)) {
            getPlayer().bQx().mo965Zc().cKv().remove(this);
        }
        if (this.hkf != null) {
            for (Actor aia : cKM()) {
                aia.aia();
            }
            if (bGX()) {
                cMy();
            }
            if (this.hkf != null) {
                this.hkf.dispose();
                this.hkf = null;
            } else {
                mo6317hy("Trying to remove " + this + " without solid from local space");
            }
            ahU();
        }
    }

    @C0064Am(aul = "8015dc979aa9491ba51ad69e9969cf4c", aum = 0)
    @C2198cg
    @C1253SX
    private void ahZ() {
        if (mo965Zc() != null) {
            mo965Zc().cKv().remove(this);
        }
        if (this.hkf != null) {
            for (Actor aia : cKM()) {
                aia.aia();
            }
            if (bGX()) {
                cMy();
            }
            if (this.hkf != null) {
                this.hkf.dispose();
                this.hkf = null;
            } else {
                mo6317hy("Trying to remove " + this + " without solid from local space");
            }
            ahU();
        }
    }

    @C0064Am(aul = "b4248dc0637bb4cbfc146f6209da6d2d", aum = 0)
    @C5472aKq
    /* renamed from: m */
    private boolean m1645m(C1722ZT<UserConnection> zt) {
        if (zt.bFq() == null) {
            return true;
        }
        Player dL = zt.bFq().mo15388dL();
        if (dL == null) {
            return false;
        }
        if (dL.bQB()) {
            return false;
        }
        Ship bQx = dL.bQx();
        if (bQx == null) {
            return false;
        }
        Vec3d position2 = getPosition();
        if (position2 == null || bQx.getPosition().mo9486aC(position2) <= 1.41006541E9f) {
            return true;
        }
        return false;
    }

    @C0064Am(aul = "5e18b9ed05691bc13c1f0e9fa7edde8a", aum = 0)
    /* renamed from: bo */
    private Vec3f m1586bo(Vec3f vec3f) {
        return getOrientation().mo15210aU(vec3f);
    }

    @C0064Am(aul = "7e9c2c5a0dd26ef439cdb2a473ea9ac0", aum = 0)
    /* renamed from: bq */
    private Vec3f m1587bq(Vec3f vec3f) {
        return getOrientation().mo15209aT(vec3f);
    }

    @C0064Am(aul = "7309d149af175a0c160d48f772475628", aum = 0)
    /* renamed from: bC */
    private void m1577bC(Actor cr) {
        m1589bt(cr);
    }

    @C0064Am(aul = "c2da08faa65fabb5fcf667f9cb98ad51", aum = 0)
    /* renamed from: bs */
    private void m1588bs(Vec3f vec3f) {
        m1580bi(vec3f);
    }

    @C0064Am(aul = "e07e47d71b130d3e2ea86f84f54f22c9", aum = 0)
    /* renamed from: bu */
    private void m1591bu(Vec3f vec3f) {
        m1581bj(vec3f);
    }

    @C0064Am(aul = "89a7ffcf6f8ac65ee98a160c3d5bf9f7", aum = 0)
    /* renamed from: k */
    private void m1637k(Quat4fWrap aoy) {
        m1615h(aoy);
    }

    @C0064Am(aul = "031ada94ec3add03f30093b7bf2e548c", aum = 0)
    @C5307aEh
    /* renamed from: a */
    private void m1556a(Vec3d ajr, Quat4fWrap aoy, Vec3f vec3f, Vec3f vec3f2) {
        mo988aj(ajr);
        mo1086l(aoy);
        mo1012bv(vec3f);
        mo1010bt(vec3f2);
    }

    @C0064Am(aul = "a2e8807706aeb4a574ecaaa9e66763d1", aum = 0)
    /* renamed from: ai */
    private void m1563ai(Vec3d ajr) {
        m1651r(ajr);
    }

    @C0401Fa
    @C0064Am(aul = "4a39e321ee4a7bdf44f8c36ac84bb85a", aum = 0)
    /* renamed from: u */
    private void m1652u(float f, float f2, float f3) {
        if (cLP()) {
            aiD().mo2567f(f, f2, f3);
        }
    }

    @C4034yP
    @C0064Am(aul = "40a1de632b91abea61600a5f7f32ff29", aum = 0)
    @C2499fr
    @C1253SX
    /* renamed from: lr */
    private void m1641lr(float f) {
        if (cLP()) {
            aiD().mo2540W(f);
        }
    }

    @C0064Am(aul = "b71e64a77efe6e5736ae1e92ea4c136b", aum = 0)
    /* renamed from: j */
    private void m1635j(Pawn avi) {
        m1625i(avi);
    }

    @C0064Am(aul = "e1ba20feb567020ec89af9e4a5760036", aum = 0)
    /* renamed from: bw */
    private void m1593bw(Vec3f vec3f) {
        if (cLZ()) {
            mo1006bl(vec3f);
        }
        m1580bi(vec3f);
    }

    @C0064Am(aul = "609b619e1f30fde710df836b49568c84", aum = 0)
    /* renamed from: by */
    private void m1595by(Vec3f vec3f) {
        if (cLZ()) {
            mo1007bn(vec3f);
        }
        m1581bj(vec3f);
    }

    @C0064Am(aul = "fcd875a8f6db027acd39ae45c0bdf668", aum = 0)
    @ClientOnly
    /* renamed from: b */
    private void m1574b(RenderAsset renderAsset) {
        if (aiD() != null && !aiD().isDisposed()) {
            if (((SceneObject) renderAsset) != null) {
                this.hks = (SceneObject) renderAsset;
                if (cMs() == null || !cMs().cLZ()) {
                    this.hks.dispose();
                    if (this.hkt == null) {
                        this.hkt.dispose();
                        this.hkt = null;
                    }
                    logger.warn(this + " not being simulated");
                } else if ((aiD().aRh().getFlags() & 2) != 2) {
                    this.hkB = true;
                    aht();
                }
            } else {
                logger.error("render object not found " + cMs().mo648ip() + " for actor " + getName());
            }
        }
    }

    @C0064Am(aul = "70ff3e303614203b4e217c1817386c97", aum = 0)
    /* renamed from: id */
    private void m1628id(boolean z) {
        m1619hV(z);
        mo1077ia(z);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Orientation")
    @C0064Am(aul = "904cc6682051b05bf4393303a74c32a3", aum = 0)
    /* renamed from: m */
    private void m1644m(Quat4fWrap aoy) {
        if (cLZ()) {
            mo1082j(aoy);
        }
        m1615h(aoy);
    }

    @C0064Am(aul = "9eaf6eb5b5741d9a1c589b63bc796cd5", aum = 0)
    /* renamed from: a */
    private void m1558a(C4029yK yKVar) {
        this.cYZ = yKVar;
        mo1062e(yKVar);
    }

    @C4034yP
    @C0064Am(aul = "66dde3204e37b52b97470d4e8d367d97", aum = 0)
    @C2499fr
    @C1253SX
    /* renamed from: a */
    private void m1557a(Vec3d ajr, Quat4fWrap aoy, Vec3f vec3f, Vec3f vec3f2, float f, float f2, float f3, float f4, long j, boolean z) {
        if (cLZ()) {
            aiD().mo2540W(f);
            aiD().mo2567f(f2, f3, f4);
            aiD().mo2541a(j, ajr, aoy, vec3f, vec3f2);
        }
    }

    @C0064Am(aul = "dad5ccb7a229dc82032128e418fb267e", aum = 0)
    /* renamed from: cF */
    private void m1603cF(boolean z) {
        m1617hT(z);
        mo1074hY(z);
    }

    @C0064Am(aul = "6a8a490edc583706011450fd76112c60", aum = 0)
    @ClientOnly
    /* renamed from: ls */
    private void m1642ls(float f) {
        mo1067fQ(f);
    }

    @C0064Am(aul = "7abbe2da51e612b00ad5bfc3016f85c7", aum = 0)
    /* renamed from: cU */
    private void m1604cU(boolean z) {
        if (this.hks == null) {
            return;
        }
        if (z) {
            this.hks.show();
        } else {
            this.hks.hide();
        }
    }

    @C0064Am(aul = "a29389f67b879665b0d4c69d8d6db2d9", aum = 0)
    /* renamed from: ak */
    private boolean m1564ak(Vec3d ajr) {
        if (aSp() == getPlayer().bhE()) {
            return true;
        }
        if (ajr == null) {
            ajr = aSp().cLl();
        }
        if ((C5877acF.RUNNING_CLIENT_BOT || aSp().azW() != getPlayer().bQx().azW()) && getPlayer().bhE().getPosition().mo9486aC(ajr) >= 1.41006541E9f) {
            return false;
        }
        return true;
    }

    @C0064Am(aul = "e5c9536c272fd7f282d61451b709db0d", aum = 0)
    private boolean cMn() {
        if (!bGX() || m1565al((Vec3d) null)) {
            return false;
        }
        return true;
    }

    @C0064Am(aul = "7b4a4572b6bc69b63c2805c75d2e7d02", aum = 0)
    @ClientOnly
    /* renamed from: a */
    private boolean m1559a(Node rPVar, StellarSystem jj) {
        if (rPVar == null || jj == null) {
            mo6317hy("current node / stellar system is null, client wont see effect");
            return false;
        } else if (getPlayer().bQB()) {
            mo8360mc("player is docked... ignoring effects");
            return false;
        } else if (getPlayer().bQx() != null && getPlayer().bQx().azW() != rPVar) {
            mo8360mc("player is in other node... ignoring effects");
            return false;
        } else if (getPlayer().bQx() == null || getPlayer().bQx().mo960Nc() == jj) {
            return true;
        } else {
            mo8360mc("player is in other system... ignoring effects");
            return false;
        }
    }

    @C0064Am(aul = "43a086848cbdef0d8dfdf25f9bfe4b8e", aum = 0)
    private C0520HN cMp() {
        return aiD();
    }

    @C0064Am(aul = "ec3fcf699d662100e7b0857d07864809", aum = 0)
    private Actor cMr() {
        return this;
    }

    @C0064Am(aul = "ca325e076a73479af42063b733e040b4", aum = 0)
    private C0520HN aiC() {
        return this.hkf;
    }

    @C0064Am(aul = "5f1f9b9c1b5d6a0cf73aff49bfe7a411", aum = 0)
    private void cMt() {
    }

    @C0064Am(aul = "a35c72e6b921e71aace4582c0b59b821", aum = 0)
    /* renamed from: am */
    private Vec3f m1566am(Vec3d ajr) {
        return getOrientation().mo15210aU(ajr.mo9517f((Tuple3d) getPosition()).dfV());
    }

    @C0064Am(aul = "9acade43cede0d05d6a6dd7cc99b723e", aum = 0)
    /* renamed from: bA */
    private Vec3d m1576bA(Vec3f vec3f) {
        return getPosition().mo9531q(getOrientation().mo15209aT(vec3f));
    }

    @C0064Am(aul = "ba1033fb150299c1cf8f1fa1cfec899f", aum = 0)
    /* renamed from: au */
    private String m1568au() {
        return String.valueOf(getName()) + "-" + super.toString();
    }

    @C0064Am(aul = "534fe3c19f6946019e347b985e958d10", aum = 0)
    @ClientOnly
    private void cMz() {
        if (aSp().mo647iA() != null && aSp().mo647iA().length() > 0) {
            if (this.hkg == null) {
                this.hkg = new C6017aep(this);
            }
            this.hkg.mo13172l(cHg());
        }
    }

    @C0064Am(aul = "1cedc09b919916134519e29ebbaaf9ac", aum = 0)
    private Node azV() {
        StellarSystem MX = m1531MX();
        if (MX == null) {
            if (cKW() == null) {
                return null;
            }
            m1638l((Node) null);
            return null;
        } else if (cKW() != null || !(MX instanceof StellarSystem)) {
            return cKW();
        } else {
            Node C = MX.mo3229C(cLl());
            if (C == null) {
                return C;
            }
            m1638l(C);
            return C;
        }
    }

    @C0064Am(aul = "e4f7ff8b14a774a996c1af2f29b8ef43", aum = 0)
    /* renamed from: a */
    private void m1552a(StellarSystem jj) {
        if (!cKV() || jj != m1531MX()) {
            if (cKV()) {
                mo1099zx();
            }
            if (jj != null) {
                m1598c(jj);
                m1620hW(true);
                m1638l(jj.mo3229C(cLl()));
                cKW().mo21618Zc().mo1898bg(this);
                m1550a(cKW().mo21618Zc());
                ahC();
                return;
            }
            m1620hW(false);
        }
    }

    @C0064Am(aul = "b0a3d9b7cc67028181cf415c4c9dff77", aum = 0)
    private void ahB() {
        if (bHa()) {
            cMu();
            cMw();
        }
    }

    @C0064Am(aul = "2fe4da14aa5711b949fd7b99f5f39f04", aum = 0)
    /* renamed from: a */
    private void m1553a(StellarSystem jj, Vec3d ajr) {
        if (jj != m1531MX()) {
            mo1099zx();
            if (jj != null) {
                m1651r(ajr);
                mo993b(jj);
            }
        }
    }

    @C0064Am(aul = "b6da00e7a80fd4abaaec3a216c5fb21a", aum = 0)
    private void cMB() {
        if (m1531MX() != null) {
            mo993b(m1531MX());
        }
    }

    @C0064Am(aul = "d820cef5781910dcecf8aa47042058e4", aum = 0)
    /* renamed from: zw */
    private void m1654zw() {
        if (bHa() && cKV()) {
            cMy();
        }
        if (cKV()) {
            if (cKW() != null) {
                cKW().mo21618Zc().mo1899bi(this);
            }
            m1620hW(false);
        }
        if (bGX()) {
            this.hkx = null;
        }
    }

    @C0064Am(aul = "7a9cb380313c83df5cc3e5cd385d8d4f", aum = 0)
    /* renamed from: a */
    private void m1551a(C0665JT jt) {
        if (jt.get("name") != null && jt.get("nameI") == null) {
            if (jt.get("name") instanceof String) {
                m1648nA(new I18NString((String) jt.get("name")));
            } else if (jt.get("name") instanceof I18NString) {
                m1648nA((I18NString) jt.get("name"));
            }
        }
        if (jt.get("shape") != null) {
            this.cYZ = null;
        }
    }

    @C0064Am(aul = "f67e1cf9aeda95383b258365e9e7c2e7", aum = 0)
    @C1253SX
    /* renamed from: ay */
    private void m1569ay(float f) {
        for (Actor cr : cKM()) {
            if (cr.hkf != null) {
                cr.hkf.step(f);
            }
        }
        if (bGX() && !this.hkB) {
            boolean z = this.hkA >= cKK();
            if (!cME() || !z) {
                for (Actor aic : cKM()) {
                    aic.aic();
                }
            } else {
                aht();
                this.hkB = true;
                for (Actor aie : cKM()) {
                    aie.aie();
                }
            }
            this.hkA += f;
        }
    }

    @C0064Am(aul = "263bc71d60bcb849cda6325e35b8aff2", aum = 0)
    /* renamed from: lu */
    private void m1643lu(float f) {
        m1640lq(f);
    }

    @C0064Am(aul = "13e36d07a38dd87928e3610e2e23c0f9", aum = 0)
    private boolean cMD() {
        return this.hks != null;
    }

    @C0064Am(aul = "715c5ecfaff763c9608250f64e32cab4", aum = 0)
    private C2686iZ<Actor> cMF() {
        return cKM();
    }

    @C0064Am(aul = "203fa362d89e6f984647b2e69876a7cf", aum = 0)
    @ClientOnly
    private void aib() {
        if (this.hks != null) {
            this.hks.setRender(false);
        }
    }

    @C0064Am(aul = "5aa6e89a3b544ac7a62fd79cbf45ef5d", aum = 0)
    @ClientOnly
    private void aid() {
        if (this.hks != null) {
            this.hks.setRender(true);
        }
    }

    @C0064Am(aul = "b33a60c40682a1fcabfce1d08d5bea57", aum = 0)
    @ClientOnly
    private Color ail() {
        return new Color(0.7f, 0.7f, 0.7f, 0.7f);
    }

    @C0064Am(aul = "c5711b3d12470104654260b5b5233155", aum = 0)
    @ClientOnly
    private C6017aep cMH() {
        return this.hkg;
    }

    @C0064Am(aul = "c5de637463acce39a3c962323fa28039", aum = 0)
    @ClientOnly
    private long cMJ() {
        return this.hkz;
    }

    @C0064Am(aul = "39671a114e902fc60e1b6aa605af4537", aum = 0)
    private Collection<aDR> aad() {
        if (mo965Zc() == null) {
            return Collections.EMPTY_LIST;
        }
        return mo965Zc().aae();
    }

    @C0064Am(aul = "72ed7de63e15af1e91aa23632131714d", aum = 0)
    @ClientOnly
    /* renamed from: QV */
    private boolean m1533QV() {
        if (!super.mo961QW() && !C1298TD.m9830t(m1540YY()).bFT() && !C1298TD.m9830t(m1531MX()).bFT()) {
            return false;
        }
        return true;
    }

    /* renamed from: a.CR$g */
    /* compiled from: a */
    class C0199g extends C0200h {
        C0199g() {
            super();
        }

        /* renamed from: c */
        public void mo1103c(C4029yK yKVar) {
            Actor.this.mo999b(yKVar);
        }
    }

    /* renamed from: a.CR$f */
    /* compiled from: a */
    class C0198f extends C0200h {
        C0198f(Space ea, long j) {
            super(ea, j);
        }

        /* renamed from: c */
        public void mo1103c(C4029yK yKVar) {
            if (Actor.this.mo958IL() != null) {
                Actor.logger.warn("Please find out why we're setting shape if shape already not null on " + Actor.this.getName());
                return;
            }
            Actor.this.mo999b(yKVar);
            Actor.this.mo1081j(this.f342rL, this.when);
        }
    }

    /* renamed from: a.CR$h */
    /* compiled from: a */
    public abstract class C0200h {

        /* renamed from: rL */
        Space f342rL;
        long when;

        public C0200h() {
        }

        public C0200h(Space ea, long j) {
            this.f342rL = ea;
            this.when = j;
        }

        /* renamed from: c */
        public abstract void mo1103c(C4029yK yKVar);
    }

    /* renamed from: a.CR$b */
    /* compiled from: a */
    class C0194b implements C5473aKr<aDJ, Object> {
        C0194b() {
        }

        /* renamed from: b */
        public void mo1102b(aDJ adj, C5663aRz arz, Object[] objArr) {
            for (int i = 0; i < objArr.length; i++) {
            }
        }

        /* renamed from: a */
        public void mo1101a(aDJ adj, C5663aRz arz, Object[] objArr) {
            for (int i = 0; i < objArr.length; i++) {
            }
        }
    }

    /* renamed from: a.CR$c */
    /* compiled from: a */
    class C0195c extends C0200h {
        C0195c() {
            super();
        }

        /* renamed from: c */
        public void mo1103c(C4029yK yKVar) {
            Actor.this.mo999b(yKVar);
        }
    }

    /* renamed from: a.CR$d */
    /* compiled from: a */
    class C0196d implements C1706ZG.C1707a {
        C0196d() {
        }

        public void aDx() {
            Actor.this.cMe();
        }
    }

    /* renamed from: a.CR$e */
    /* compiled from: a */
    class C0197e implements C1706ZG.C1707a {
        C0197e() {
        }

        public void aDx() {
            Actor.this.cMi();
        }
    }

    /* renamed from: a.CR$a */
    class C0193a implements RenderTask {
        private final /* synthetic */ SceneObject cBd;

        C0193a(SceneObject sceneObject) {
            this.cBd = sceneObject;
        }

        public void run(RenderView renderView) {
            Actor.this.mo968a((RenderAsset) this.cBd);
        }
    }
}
