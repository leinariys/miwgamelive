package util;

import java.util.Comparator;

/* renamed from: a.Lz */
/* compiled from: a */
public class C0831Lz<T> extends C3969xW<T> {

    private final Comparator<T> comparator;

    public C0831Lz() {
        this.comparator = null;
    }

    public C0831Lz(Comparator<T> comparator2) {
        this.comparator = comparator2;
    }

    /* access modifiers changed from: protected */
    public int compare(T t, T t2) {
        if (this.comparator == null) {
            return ((Comparable) t).compareTo(t2);
        }
        return this.comparator.compare(t, t2);
    }
}
