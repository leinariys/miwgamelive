package util;


import java.io.*;
import java.lang.reflect.Array;
import java.nio.ByteBuffer;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/* renamed from: a.aK */
/* compiled from: a */
public class Syst {
    /* renamed from: t */
    public static String m15908t(String str) {
        if (str.length() > 0) {
            return String.valueOf(Character.toUpperCase(str.charAt(0))) + str.substring(1);
        }
        return str;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:2:0x0006, code lost:
        r0 = r3.charAt(0);
     */
    /* renamed from: a */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static java.lang.CharSequence m15893a(CharSequence paramCharSequence) {
        if (paramCharSequence.length() > 0) {
            char c1 = paramCharSequence.charAt(0);
            char c2 = Character.toUpperCase(c1);
            if (c2 != c1) {
                return c2 + "" + paramCharSequence.subSequence(1, paramCharSequence.length());
            }
        }
        return paramCharSequence;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:2:0x0006, code lost:
        r0 = r3.charAt(0);
     */
    /* renamed from: b */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static java.lang.CharSequence m15902b(CharSequence paramCharSequence) {
        if (paramCharSequence.length() > 0) {
            char c1 = paramCharSequence.charAt(0);
            char c2 = Character.toLowerCase(c1);
            if (c2 != c1) {
                return c2 + "" + paramCharSequence.subSequence(1, paramCharSequence.length());
            }
        }
        return paramCharSequence;
    }

    /* renamed from: u */
    public static String m15909u(String str) {
        if (str.length() > 0) {
            return String.valueOf(Character.toLowerCase(str.charAt(0))) + str.substring(1);
        }
        return str;
    }

    /* renamed from: v */
    public static String m15910v(String str) {
        StringBuffer stringBuffer = new StringBuffer();
        for (int i = 0; i < str.length(); i++) {
            char charAt = str.charAt(i);
            if (Character.isUpperCase(charAt)) {
                stringBuffer.append('_').append(charAt);
            } else {
                stringBuffer.append(Character.toUpperCase(charAt));
            }
        }
        return stringBuffer.toString();
    }

    /* renamed from: w */
    public static String getMd5(String str) {
        try {
            MessageDigest instance = MessageDigest.getInstance("md5");
            instance.update(str.getBytes("utf8"));
            return toHexString(instance.digest());
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (UnsupportedEncodingException e2) {
            e2.printStackTrace();
        }
        return String.valueOf(str.hashCode());
    }

    /* renamed from: a */
    public static String getMd5String(byte[] bArr) {
        try {
            MessageDigest instance = MessageDigest.getInstance("md5");
            instance.update(bArr);
            return toHexString(instance.digest());
        } catch (NoSuchAlgorithmException e) {
            throw new RuntimeException(e);
        }
    }

    /* renamed from: b */
    public static byte[] getMd5(byte[] bArr) {
        try {
            MessageDigest instance = MessageDigest.getInstance("md5");
            instance.update(bArr);
            return instance.digest();
        } catch (NoSuchAlgorithmException e) {
            throw new RuntimeException(e);
        }
    }

    /* renamed from: x */
    public static String getSha1(String str) {
        try {
            MessageDigest instance = MessageDigest.getInstance("sha-1");
            instance.update(str.getBytes("utf8"));
            return toHexString(instance.digest());
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (UnsupportedEncodingException e2) {
            e2.printStackTrace();
        }
        return String.valueOf(str.hashCode());
    }

    public static String toHexString(byte[] bArr) {
        return m15899a(bArr, (String) null);
    }

    /* renamed from: a */
    public static String m15899a(byte[] bArr, String str) {
        return m15900a(bArr, str, 0);
    }

    /* renamed from: a */
    public static String m15900a(byte[] bArr, String str, int i) {
        StringBuilder sb = new StringBuilder();
        if (bArr == null) {
            return "null";
        }
        int i2 = 0;
        for (byte b : bArr) {
            if (str != null && sb.length() > 0) {
                sb.append(str);
            }
            if ((b & 255) < 16) {
                sb.append("0").append(Integer.toHexString(b & 255));
            } else {
                sb.append(Integer.toHexString(b & 255));
            }
            i2++;
            if (i > 0 && i2 >= i) {
                sb.append("\n");
                i2 = 0;
            }
        }
        return new String(sb);
    }

    /* renamed from: a */
    public static String m15898a(byte[] bArr, int i, int i2, String str, int i3) {
        StringBuilder sb = new StringBuilder();
        if (bArr == null) {
            return "null";
        }
        int i4 = 0;
        while (true) {
            i2--;
            if (i2 < 0) {
                return new String(sb);
            }
            byte b = bArr[i];
            if (str != null && sb.length() > 0) {
                sb.append(str);
            }
            if ((b & 255) < 16) {
                sb.append("0").append(Integer.toHexString(b & 255));
            } else {
                sb.append(Integer.toHexString(b & 255));
            }
            i4++;
            if (i3 > 0 && i4 >= i3) {
                sb.append("\n");
                i4 = 0;
            }
            i++;
        }
    }

    /* renamed from: b */
    public static String byteToStringLog(byte[] bArr, int i, int i2, String str, int i3) {
        return byteToStringLog("", bArr, i, i2, str, i3);
    }

    /* renamed from: a */
    public static String byteToStringLog(String str, byte[] bArr, int i, int i2, String str2, int i3) {
        StringBuilder sb = new StringBuilder();
        if (bArr == null) {
            return "null";
        }
        int i4 = i + i2;
        int i5 = i;
        while (i5 < i4) {
            sb.append(str);
            int i6 = i5 + i3;
            int min = Math.min(i4, i6);
            sb.append(String.format("%04d - ", new Object[]{Integer.valueOf(i5 - i)}));
            int i7 = i5;
            while (i7 < min) {
                byte b = bArr[i7];
                if ((b & 255) < 16) {
                    sb.append("0").append(Integer.toHexString(b & 255));
                } else {
                    sb.append(Integer.toHexString(b & 255));
                }
                sb.append(str2);
                i7++;
            }
            while (i7 < i6) {
                sb.append("..");
                sb.append(str2);
                i7++;
            }
            sb.append(" [");
            for (int i8 = i5; i8 < min; i8++) {
                byte b2 = bArr[i8];
                sb.append(b2 > 31 ? Character.valueOf((char) b2) : b2 == 0 ? "_" : "?");
            }
            sb.append("]\n");
            i5 += i3;
        }
        return sb.toString();
    }

    /* renamed from: c */
    public static String m15906c(byte[] bArr) {
        StringBuffer stringBuffer = new StringBuffer();
        for (byte b : bArr) {
            int i = 8;
            while (true) {
                int i2 = i - 1;
                if (i2 < 0) {
                    break;
                }
                stringBuffer.append(1 == ((b >> i2) & 1) ? '1' : '0');
                i = i2;
            }
        }
        return stringBuffer.toString();
    }

    /* renamed from: a */
    public static ByteBuffer m15901a(File file) throws IOException {
        RandomAccessFile randomAccessFile = null;
        try {
            randomAccessFile = new RandomAccessFile(file, "r");
            ByteBuffer allocateDirect = ByteBuffer.allocateDirect((int) randomAccessFile.length());
            if (((long) randomAccessFile.getChannel().read(allocateDirect)) < randomAccessFile.length()) {
                randomAccessFile.close();
                return null;
            }
            allocateDirect.flip();
            randomAccessFile.close();
            return allocateDirect;
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            return null;
        } catch (IOException e2) {
            e2.printStackTrace();
            return null;
        } catch (Throwable th) {
            randomAccessFile.close();
            throw th;
        }
    }

    /* renamed from: b */
    public static String m15904b(byte[] bArr, String str) {
        try {
            return new String(bArr, str);
        } catch (UnsupportedEncodingException e) {
            return null;
        }
    }

    /* renamed from: a */
    public static String m15897a(byte[] bArr, int i, int i2, String str) {
        try {
            return new String(bArr, i, i2, str);
        } catch (UnsupportedEncodingException e) {
            return null;
        }
    }

    /* renamed from: a */
    public static String getClassName(Class<?> cls) {
        int lastIndexOf = cls.getName().lastIndexOf(46);
        return lastIndexOf == -1 ? cls.getName() : cls.getName().substring(lastIndexOf + 1);
    }

    /* renamed from: f */
    public static String m15907f(Object obj) {
        try {
            return String.valueOf(obj);
        } catch (Exception e) {
            try {
                return "Error at toString " + e.toString();
            } catch (Exception e2) {
                return "Seriour error at toString " + e.getClass();
            }
        }
    }

    public static byte[] copyOf(byte[] bArr, int i) {
        byte[] bArr2 = new byte[i];
        System.arraycopy(bArr, 0, bArr2, 0, Math.min(bArr.length, i));
        return bArr2;
    }

    public static byte[] copyOfRange(byte[] bArr, int i, int i2) {
        int i3 = i2 - i;
        if (i3 < 0) {
            throw new IllegalArgumentException(String.valueOf(i) + " > " + i2);
        }
        byte[] bArr2 = new byte[i3];
        System.arraycopy(bArr, i, bArr2, 0, Math.min(bArr.length - i, i3));
        return bArr2;
    }

    public static <T> T[] copyOf(T[] tArr, int i) {
        return (T[]) copyOf(tArr, i, tArr.getClass());
    }

    public static <T, U> T[] copyOf(U[] uArr, int i, Class<? extends T[]> cls) {
        T[] tArr;
        if (cls == Object[].class) {
            tArr = (T[]) new Object[i];
        } else {
            tArr = (T[]) Array.newInstance(cls.getComponentType(), i);
        }
        System.arraycopy(uArr, 0, tArr, 0, Math.min(uArr.length, i));
        return tArr;
    }

    public static long[] copyOf(long[] jArr, int i) {
        long[] jArr2 = new long[i];
        System.arraycopy(jArr, 0, jArr2, 0, Math.min(jArr.length, i));
        return jArr2;
    }
}
