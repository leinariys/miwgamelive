package util;

import java.util.ArrayList;
import java.util.Collection;

/* renamed from: a.xW */
/* compiled from: a */
public abstract class C3969xW<T> extends ArrayList<T> {

    /* access modifiers changed from: protected */
    public abstract int compare(T t, T t2);

    public boolean add(T t) {
        int size = size() - 1;
        int i = 0;
        while (i <= size) {
            int i2 = ((size - i) >> 1) + i;
            if (compare(t, get(i2)) > 0) {
                i = i2 + 1;
            } else {
                size = i2 - 1;
            }
        }
        super.add(i, t);
        return true;
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public void add(int i, T t) {
        super.add(i, t);
    }

    public boolean contains(Object obj) {
        int size = size() - 1;
        int i = 0;
        while (i <= size) {
            int i2 = ((size - i) >> 1) + i;
            int compare = compare((T) obj, get(i2));
            if (compare > 0) {
                i = i2 + 1;
            } else if (compare == 0) {
                return true;
            } else {
                size = i2 - 1;
            }
        }
        return false;
    }

    public int lastIndexOf(Object obj) {
        int size = size() - 1;
        int i = 0;
        while (i <= size) {
            int i2 = ((size - i) >> 1) + i;
            int compare = compare((T) obj, get(i2));
            if (compare > 0) {
                i = i2 + 1;
            } else if (compare == 0) {
                while (i2 < size && compare((T) obj, get(i2 + 1)) == 0) {
                    i2++;
                }
                return i2;
            } else {
                size = i2 - 1;
            }
        }
        return -1;
    }

    public boolean remove(Object obj) {
        int indexOf = indexOf(obj);
        if (indexOf == -1) {
            return false;
        }
        super.remove(indexOf);
        return true;
    }

    public int indexOf(Object obj) {
        int size = size() - 1;
        int i = 0;
        while (i <= size) {
            int i2 = ((size - i) >> 1) + i;
            int compare = compare((T) obj, get(i2));
            if (compare > 0) {
                i = i2 + 1;
            } else if (compare == 0) {
                while (i2 > i && compare((T) obj, get(i2 - 1)) == 0) {
                    i2--;
                }
                return i2;
            } else {
                size = i2 - 1;
            }
        }
        return -1;
    }

    public T set(int i, T t) {
        throw new IllegalAccessError();
    }

    public boolean addAll(Collection<? extends T> collection) {
        for (Object add : collection.toArray()) {
            add((T) add);
        }
        return true;
    }
}
