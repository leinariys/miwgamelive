package logic.sql;

import game.script.player.Player;

/* renamed from: a.HW */
/* compiled from: a */
public class C0535HW extends C5878acG {
    private String cWY;
    private String className;
    private int count;
    private String ded;
    private String dee;
    private int line;
    private String reason;
    private String version;

    public C0535HW() {
    }

    public C0535HW(String str, String str2, String str3, int i, String str4) {
        this(str, str2, str3, i, str4, 1);
    }

    public C0535HW(String str, String str2, String str3, int i, String str4, int i2) {
        super(0, (Player) null);
        this.ded = str;
        this.version = str2;
        this.className = str3;
        this.line = i;
        this.reason = str4;
        this.count = i2;
        this.cWY = "false";
        this.dee = "";
    }

    public String aVI() {
        return this.ded;
    }

    public String getVersion() {
        return this.version;
    }

    public String getClassName() {
        return this.className;
    }

    public void setClassName(String str) {
        this.className = str;
    }

    public int getLine() {
        return this.line;
    }

    public String getReason() {
        return this.reason;
    }

    public int getCount() {
        return this.count;
    }

    public void aVJ() {
        this.count++;
    }

    /* renamed from: c */
    public void mo2635c(Integer num) {
        this.count = num.intValue();
    }

    /* renamed from: ey */
    public void mo2637ey(String str) {
        this.cWY = str;
    }

    public String aQv() {
        return this.cWY;
    }

    /* renamed from: ez */
    public void mo2638ez(String str) {
        this.dee = str;
    }

    /* renamed from: eA */
    public void mo2636eA(String str) {
        this.ded = str;
    }
}
