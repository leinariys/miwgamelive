package logic.sql;

import game.script.player.Player;
import game.script.player.PlayerSessionLog;

/* renamed from: a.Uv */
/* compiled from: a */
public class C1428Uv extends C5878acG {

    /* renamed from: S */
    private int f1836S;

    /* renamed from: U */
    private int f1837U;

    /* renamed from: V */
    private int f1838V;

    /* renamed from: W */
    private int f1839W;

    /* renamed from: X */
    private int f1840X;
    private long bGr;
    private long ciV;
    private long ciX;
    private long cja;
    private int cjc;
    private int cje;
    private int cjg;
    private int cjm;
    private int cjq;
    private int cjs;
    private int cju;
    private int cjw;
    private int cjy;
    private String eev;
    private long eoo;
    private long eop;
    private int eoq;
    private int eor;
    private int eos;
    private int eot;
    private String eou;
    private int eov;
    private int eow;
    private String type;

    public C1428Uv() {
    }

    public C1428Uv(Player aku, int i, long j, long j2, long j3, long j4, long j5, int i2, int i3, long j6, int i4, int i5, int i6, int i7, int i8, int i9, int i10, int i11, int i12, int i13, String str, String str2, PlayerSessionLog.C0024b bVar, int i14, int i15, int i16, int i17, int i18, int i19, int i20) {
        super(System.currentTimeMillis(), aku);
        this.f1836S = i;
        this.eoo = j;
        this.ciV = j2;
        this.ciX = j3;
        this.eop = j4;
        this.bGr = j5;
        this.cjc = i2;
        this.eoq = i3;
        this.cja = j6;
        this.cje = i4;
        this.eor = i5;
        this.f1837U = i6;
        this.eos = i7;
        this.f1838V = i8;
        this.cjm = i9;
        this.f1839W = i10;
        this.eot = i11;
        this.f1840X = i12;
        this.cjq = i13;
        this.eou = str;
        this.eev = str2;
        this.type = bVar.name();
        this.cjg = i14;
        this.cjs = i15;
        this.cjw = i16;
        this.eov = i17;
        this.cju = i18;
        this.cjy = i19;
        this.eow = i20;
    }
}
