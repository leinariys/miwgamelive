package logic.sql;

import game.script.player.Player;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.lang.reflect.Field;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/* renamed from: a.acG  reason: case insensitive filesystem */
/* compiled from: a */
public abstract class C5878acG implements Serializable {
    public static final char dBB = ';';
    public static final String feB = "Log";
    public static final String feC = "yyyy-MM-dd HH:mm:ss.SSS";
    public static final long serialVersionUID = 1;
    private static final String dBA = "\\\"";
    private static final char dBC = ',';
    private static final SimpleDateFormat dBH = new SimpleDateFormat(feC);
    private static final char dBz = '\"';
    private static final String feD = "App";
    private static final String[][] feE = {new String[]{"LoginLog", "UserLoginLog"}, new String[]{"LogoutLog", "UserLogoutLog"}};
    private Date feF;
    private long feG;
    private String feH;

    public C5878acG() {
    }

    public C5878acG(long j, Player aku) {
        this.feF = new Date(j);
        if (aku != null) {
            this.feH = aku.getName();
            this.feG = aku.bFf().getObjectId().getId();
            return;
        }
        this.feH = null;
        this.feG = -1;
    }

    /* renamed from: W */
    public static String m20235W(Class<? extends C5878acG> cls) {
        return feD + m20236X(cls);
    }

    /* renamed from: X */
    public static String m20236X(Class<? extends C5878acG> cls) {
        String simpleName = cls.getSimpleName();
        if (simpleName.endsWith(feB)) {
            return simpleName.substring(0, simpleName.length() - 3);
        }
        return simpleName;
    }

    /* renamed from: Y */
    public static List<Field> m20237Y(Class<? extends C5878acG> cls) {
        ArrayList arrayList = new ArrayList();
        for (Field field : C5878acG.class.getDeclaredFields()) {
            if ((field.getModifiers() & 24) == 0) {
                arrayList.add(field);
            }
        }
        for (Field field2 : cls.getDeclaredFields()) {
            if ((field2.getModifiers() & 24) == 0) {
                arrayList.add(field2);
            }
        }
        return arrayList;
    }

    /* renamed from: Z */
    public static String m20238Z(Class<? extends C5878acG> cls) {
        StringBuffer stringBuffer = new StringBuffer();
        stringBuffer.append(m20236X(cls));
        for (Field name : m20237Y(cls)) {
            stringBuffer.append(dBB);
            stringBuffer.append(name.getName());
        }
        return stringBuffer.toString();
    }

    /* renamed from: aa */
    public static String m20239aa(Class<? extends C5878acG> cls) {
        StringBuffer stringBuffer = new StringBuffer();
        String W = m20235W(cls);
        stringBuffer.append("CREATE TABLE ");
        stringBuffer.append(W);
        stringBuffer.append(" (");
        boolean z = true;
        for (Field next : m20237Y(cls)) {
            if (!z) {
                stringBuffer.append(", ");
            } else {
                z = false;
            }
            stringBuffer.append(next.getName());
            Class<?> type = next.getType();
            if (type == String.class) {
                stringBuffer.append(" VARCHAR(256)");
            } else if (type == Float.class || (type.isPrimitive() && type == Float.TYPE)) {
                stringBuffer.append(" DECIMAL(16, 8)");
            } else if (type == Integer.class || (type.isPrimitive() && type == Integer.TYPE)) {
                stringBuffer.append(" INT");
            } else if (type == Long.class || (type.isPrimitive() && type == Long.TYPE)) {
                stringBuffer.append(" BIGINT");
            } else if (type == Date.class) {
                stringBuffer.append(" DATETIME");
            } else {
                stringBuffer.append(" <<<UNSUPPORTED TYPE!>>>");
            }
        }
        stringBuffer.append(");");
        return stringBuffer.toString();
    }

    /* renamed from: ab */
    public static String m20240ab(Class<? extends C5878acG> cls) {
        StringBuffer stringBuffer = new StringBuffer();
        String W = m20235W(cls);
        stringBuffer.append("DROP TABLE ");
        stringBuffer.append(W);
        stringBuffer.append(";");
        return stringBuffer.toString();
    }

    /* renamed from: ac */
    public static List<String> m20241ac(Class<? extends C5878acG> cls) {
        ArrayList arrayList = new ArrayList();
        arrayList.add(m20235W(cls));
        for (Field name : m20237Y(cls)) {
            arrayList.add(name.getName());
        }
        return arrayList;
    }

    private void writeObject(ObjectOutputStream objectOutputStream) throws IOException {
        objectOutputStream.writeObject(this.feF);
        objectOutputStream.writeObject(this.feH);
        objectOutputStream.writeLong(this.feG);
    }

    private void readObject(ObjectInputStream objectInputStream) throws IOException, ClassNotFoundException {
        this.feF = (Date) objectInputStream.readObject();
        this.feH = (String) objectInputStream.readObject();
        this.feG = objectInputStream.readLong();
    }

    /* renamed from: gC */
    public void mo12583gC(long j) {
        this.feF = new Date(j);
    }

    public String bPE() {
        String simpleName = getClass().getSimpleName();
        if (simpleName.endsWith(feB)) {
            return simpleName.substring(0, simpleName.length() - 3);
        }
        return simpleName;
    }

    private String encodeText(String str) {
        StringBuilder sb = new StringBuilder(34);
        for (int i = 0; i < str.length(); i++) {
            char charAt = str.charAt(i);
            switch (charAt) {
                case '\"':
                    sb.append(dBA);
                    break;
                case ';':
                    sb.append(dBC);
                    break;
                default:
                    sb.append(charAt);
                    break;
            }
        }
        return sb.toString();
    }

    /* renamed from: aj */
    private String m20242aj(Object obj) {
        String obj2;
        if (obj == null) {
            return "";
        }
        Class<?> cls = obj.getClass();
        if (cls == Player.class) {
            obj2 = ((Player) obj).getName();
        } else if (cls == Date.class) {
            obj2 = dBH.format((Date) obj);
        } else {
            obj2 = obj.toString();
        }
        return encodeText(obj2);
    }

    public String bPF() {
        StringBuilder sb = new StringBuilder();
        try {
            List<Field> Y = m20237Y(getClass());
            sb.append(bPE());
            for (Field next : Y) {
                next.setAccessible(true);
                sb.append(dBB);
                sb.append(encodeText(m20242aj(next.get(this))));
            }
        } catch (IllegalArgumentException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e2) {
            e2.printStackTrace();
        }
        return sb.toString();
    }

    /* renamed from: is */
    private String m20243is(String str) {
        String str2 = "";
        int i = 0;
        while (i < str.length()) {
            String str3 = String.valueOf(str2) + str.charAt(i);
            if (str.charAt(i) == '\'') {
                str3 = String.valueOf(str3) + '\'';
            }
            i++;
            str2 = str3;
        }
        return str2;
    }

    public String bPG() throws IllegalAccessException {
        boolean z;
        StringBuffer stringBuffer = new StringBuffer();
        Class<?> cls = getClass();
        List<Field> Y = m20237Y((Class<? extends C5878acG>) cls);
        String W = m20235W((Class<? extends C5878acG>) cls);
        stringBuffer.append("INSERT INTO ");
        stringBuffer.append(W);
        stringBuffer.append(" (");
        boolean z2 = true;
        for (Field next : Y) {
            if (!z2) {
                stringBuffer.append(", ");
            } else {
                z2 = false;
            }
            stringBuffer.append(next.getName());
        }
        stringBuffer.append(") values (");
        boolean z3 = true;
        for (Field next2 : Y) {
            next2.setAccessible(true);
            if (!z3) {
                stringBuffer.append(", ");
                z = z3;
            } else {
                z = false;
            }
            Object obj = next2.get(this);
            if (obj == null) {
                stringBuffer.append("null");
                z3 = z;
            } else if (next2.getType() == String.class) {
                stringBuffer.append("'");
                String str = (String) obj;
                if (str.length() > 256) {
                    str = str.substring(0, 256);
                }
                stringBuffer.append(m20243is(str));
                stringBuffer.append("'");
                z3 = z;
            } else if (next2.getType() == Date.class) {
                stringBuffer.append("'");
                stringBuffer.append(dBH.format(obj));
                stringBuffer.append("'");
                z3 = z;
            } else {
                stringBuffer.append(obj.toString());
                z3 = z;
            }
        }
        stringBuffer.append(");");
        return stringBuffer.toString();
    }

    public String bPH() throws IllegalAccessException {
        StringBuffer stringBuffer = new StringBuffer();
        Class<?> cls = getClass();
        List<Field> Y = m20237Y((Class<? extends C5878acG>) cls);
        String W = m20235W((Class<? extends C5878acG>) cls);
        stringBuffer.append("<" + W + ">");
        for (Field next : Y) {
            next.setAccessible(true);
            stringBuffer.append("<" + next.getName() + ">");
            Object obj = next.get(this);
            if (obj == null) {
                stringBuffer.append("NULL");
            } else if (next.getType() == String.class) {
                stringBuffer.append("'");
                String str = (String) obj;
                if (str.length() > 256) {
                    str = str.substring(0, 256);
                }
                stringBuffer.append(m20243is(str));
                stringBuffer.append("'");
            } else if (next.getType() == Date.class) {
                stringBuffer.append("'");
                stringBuffer.append(dBH.format(obj));
                stringBuffer.append("'");
            } else {
                stringBuffer.append(obj.toString());
            }
            stringBuffer.append("</" + next.getName() + ">");
        }
        stringBuffer.append("</" + W + ">");
        return stringBuffer.toString();
    }

    public Date getTimestamp() {
        return this.feF;
    }
}
