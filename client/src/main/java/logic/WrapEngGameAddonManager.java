package logic;

import taikodom.addon.AddonSettingsImpl;
import taikodom.addon.C2495fo;
import taikodom.render.loader.provider.FilePath;

/* renamed from: a.aUM */
/* compiled from: a */
public class WrapEngGameAddonManager implements IWrapEngGameAddonManager {
    /* renamed from: vS */
    private EngineGame engineGame;
    private IAddonManager addonManager;

    public WrapEngGameAddonManager(EngineGame engineGame) {
        this.engineGame = engineGame;
    }

    public boolean isPlayerNotNull() {
        return this.engineGame.isPlayerNotNull();
    }

    public Object getRoot() {
        return this.engineGame.getRoot();
    }

    public int getScreenHeight() {
        return this.engineGame.getScreenHeight();
    }

    public int getScreenWidth() {
        return this.engineGame.getScreenWidth();
    }

    public C6245ajJ getEventManager() {
        return this.engineGame.getEventManager();
    }

    /* renamed from: b */
    public void setAddonManager(IAddonManager addonManager) {
        this.addonManager = addonManager;
    }

    /* renamed from: a */
    public IAddonSettings initAddonSetting(IAddonManager axz, IWrapFileXmlOrJar avw, IAddonExecutor ams) {
        AddonSettingsImpl addonSettings = new AddonSettingsImpl(this.engineGame, axz, avw, (C2495fo) ams);
        ams.initAddon(addonSettings);
        return addonSettings;
    }

    public FilePath getRootPathRender() {
        return this.engineGame.getRootPathRender();
    }

    public FilePath getRootPath() {
        return this.engineGame.getRootPath();
    }
}
