package logic;

/**
 * Наследует интервйс interface fo который наследуют аддоны
 */
/* renamed from: a.aMS */
/* compiled from: a */
public interface IAddonExecutor<T extends IAddonSettings> {
    /**
     * Передаём в аддон логирование и локолизацию
     *
     * @param addonPropertie
     */
    /* renamed from: a */
    void initAddon(T addonPropertie);

    /**
     * Остановить работу аддона
     */
    void stop();
}
