package logic.baa;

import game.network.message.externalizable.aCE;
import logic.data.mbean.C6602aqC;
import logic.res.code.C5663aRz;
import logic.res.code.C6494any;
import logic.res.html.C2491fm;
import p001a.*;

import java.util.Collection;

@C0566Hp
@C5511aMd
@C6485anp
/* renamed from: a.mJ */
/* compiled from: a */
public abstract class C2961mJ extends aDJ implements C1616Xf {
    public static final int _m_fieldCount = 0;
    public static final C5663aRz[] _m_fields = null;
    public static final int _m_methodCount = 0;
    public static final C2491fm[] _m_methods = null;
    public static final C2491fm aDR = null;
    public static final C2491fm aDS = null;
    /* renamed from: dN */
    public static final C2491fm f8640dN = null;
    public static final long serialVersionUID = 0;
    public static C6494any ___iScriptClass;

    static {
        m35482V();
    }

    public C2961mJ() {
        super((C5540aNg) null);
        super.mo10S();
    }

    public C2961mJ(C5540aNg ang) {
        super(ang);
    }

    /* renamed from: V */
    static void m35482V() {
        serialVersionUID = (long) 1;
        _m_fieldCount = aDJ._m_fieldCount + 0;
        _m_methodCount = aDJ._m_methodCount + 3;
        _m_fields = new C5663aRz[(aDJ._m_fieldCount + 0)];
        C1634Xv.m11725a((Object[]) aDJ._m_fields, (Object[]) _m_fields);
        int i = aDJ._m_methodCount;
        C2491fm[] fmVarArr = new C2491fm[(i + 3)];
        C2491fm a = C4105zY.m41624a(C2961mJ.class, "d3d02f60fec958074e4ec8f5a02b1673", i);
        aDR = a;
        fmVarArr[i] = a;
        int i2 = i + 1;
        C2491fm a2 = C4105zY.m41624a(C2961mJ.class, "15f4af26e881ac9bc5bdbd3be536b893", i2);
        f8640dN = a2;
        fmVarArr[i2] = a2;
        int i3 = i2 + 1;
        C2491fm a3 = C4105zY.m41624a(C2961mJ.class, "0604387e36988b4394c81b805d8c69b7", i3);
        aDS = a3;
        fmVarArr[i3] = a3;
        int i4 = i3 + 1;
        _m_methods = fmVarArr;
        C1634Xv.m11725a((Object[]) aDJ._m_methods, (Object[]) _m_methods);
        ___iScriptClass = aUO.m18566a(C2961mJ.class, C6602aqC.class, _m_fields, _m_methods);
    }

    @C0064Am(aul = "15f4af26e881ac9bc5bdbd3be536b893", aum = 0)
    /* renamed from: aT */
    private <T extends aDJ> T m35483aT() {
        throw new aWi(new aCE(this, f8640dN, new Object[0]));
    }

    /* renamed from: NK */
    public <T extends aDJ> T mo7459NK() {
        switch (bFf().mo6893i(aDS)) {
            case 0:
                return null;
            case 2:
                return (aDJ) bFf().mo5606d(new aCE(this, aDS, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, aDS, new Object[0]));
                break;
        }
        return m35481NJ();
    }

    /* renamed from: T */
    public C6494any mo11T() {
        return ___iScriptClass;
    }

    /* renamed from: U */
    public C2491fm[] mo12U() {
        return _m_methods;
    }

    /* renamed from: W */
    public Object mo13W() {
        return new C6602aqC(this);
    }

    /* renamed from: a */
    public Object mo14a(C0495Gr gr) {
        Object[] args = gr.getArgs();
        switch (gr.mo2417hq() - aDJ._m_methodCount) {
            case 0:
                m35484e((aDJ) args[0]);
                return null;
            case 1:
                return m35483aT();
            case 2:
                return m35481NJ();
            default:
                return super.mo14a(gr);
        }
    }

    /* renamed from: a */
    public void mo15a(Collection collection, C0495Gr gr) {
        super.mo15a(collection, gr);
    }

    /* access modifiers changed from: protected */
    /* renamed from: aU */
    public <T extends aDJ> T mo745aU() {
        switch (bFf().mo6893i(f8640dN)) {
            case 0:
                return null;
            case 2:
                return (aDJ) bFf().mo5606d(new aCE(this, f8640dN, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f8640dN, new Object[0]));
                break;
        }
        return m35483aT();
    }

    /* renamed from: c */
    public C5663aRz[] mo25c() {
        return _m_fields;
    }

    /* renamed from: f */
    public void mo2631f(aDJ adj) {
        switch (bFf().mo6893i(aDR)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, aDR, new Object[]{adj}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, aDR, new Object[]{adj}));
                break;
        }
        m35484e(adj);
    }

    /* renamed from: S */
    public void mo10S() {
        super.mo10S();
    }

    @C0064Am(aul = "d3d02f60fec958074e4ec8f5a02b1673", aum = 0)
    /* renamed from: e */
    private void m35484e(aDJ adj) {
    }

    @C0064Am(aul = "0604387e36988b4394c81b805d8c69b7", aum = 0)
    /* renamed from: NJ */
    private <T extends aDJ> T m35481NJ() {
        return mo745aU();
    }
}
