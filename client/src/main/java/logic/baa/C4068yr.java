package logic.baa;

import game.script.pda.DatabaseCategory;
import game.script.pda.TaikopediaEntry;
import game.script.resource.Asset;

import java.util.List;

/* renamed from: a.yr */
/* compiled from: a */
public interface C4068yr extends C4033yO {
    /* renamed from: RW */
    List<TaikopediaEntry> mo183RW();

    /* renamed from: RY */
    DatabaseCategory mo184RY();

    /* renamed from: Sa */
    String mo185Sa();

    /* renamed from: Se */
    Asset mo187Se();

    /* renamed from: Sg */
    float mo188Sg();
}
