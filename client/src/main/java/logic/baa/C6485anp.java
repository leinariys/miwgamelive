package logic.baa;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

@Retention(RetentionPolicy.RUNTIME)
/* renamed from: a.anp  reason: case insensitive filesystem */
/* compiled from: a */
public @interface C6485anp {
    String aul() default "";

    int aum() default -1;
}
