package logic.baa;

import game.engine.DataGameEvent;
import logic.res.code.C5663aRz;
import logic.res.code.C6494any;
import logic.res.html.C2491fm;
import logic.res.html.C6663arL;
import logic.thred.LogPrinter;
import p001a.C0495Gr;
import p001a.C0612Ig;
import p001a.C5540aNg;
import p001a.C6147ahP;

import java.util.Collection;

/* renamed from: a.Xv */
/* compiled from: a */
public class C1634Xv implements C1616Xf {
    private static final LogPrinter log = LogPrinter.m10275K(C1634Xv.class);

    private C6663arL infra;

    public C1634Xv(C5540aNg ang) {
    }

    public C1634Xv() {
        DataGameEvent.m6166aa(this);
    }

    /* renamed from: a */
    public static void m11725a(Object[] objArr, Object[] objArr2) {
        if (objArr != null && objArr.length > 0) {
            System.arraycopy(objArr, 0, objArr2, 0, objArr.length);
        }
    }

    /* access modifiers changed from: protected */
    /* renamed from: S */
    public void mo10S() {
        if (this.infra == null) {
            throw new C0612Ig((C1616Xf) this);
        }
    }

    public void bFg() {
        mo10S();
    }

    /* renamed from: a */
    public Object mo14a(C0495Gr gr) {
        throw new IllegalAccessError("xx");
    }

    /* renamed from: a */
    public void mo15a(Collection collection, C0495Gr gr) {
        throw new IllegalAccessError();
    }

    /* renamed from: T */
    public C6494any mo11T() {
        throw new IllegalAccessError();
    }

    public C6147ahP[] bFh() {
        return mo11T().clz();
    }

    public Thread bFo() {
        return null;
    }

    public boolean isUseClientOnly() {
        return mo11T().mo15897hk();
    }

    public boolean bFn() {
        return mo11T().cly();
    }

    public boolean isUseServerOnly() {
        return mo11T().mo15898ho();
    }

    public boolean bFm() {
        return mo11T().mo15899hp();
    }

    /* renamed from: h */
    public void mo6766h(C1616Xf xf) {
    }

    /* renamed from: b */
    public void mo6754b(C6663arL arl) {
        this.infra = arl;
    }

    /* renamed from: a */
    public void mo6753a(Thread thread) {
    }

    public void bFj() {
    }

    public void bFi() {
    }

    /* renamed from: w */
    public Object mo6767w(C5663aRz arz) {
        return this.infra.mo6898w(arz);
    }

    public C6663arL bFf() {
        return this.infra;
    }

    /* renamed from: g */
    public void mo6765g(C5663aRz arz, Object obj) {
        this.infra.mo6889g(arz, obj);
    }

    public int hashCode() {
        if (bFf() != null) {
            return bFf().getObjectId().hashCode();
        }
        log.warn("Illegal call to hashCode() of uninitialized game.script.");
        return super.hashCode();
    }

    public boolean equals(Object obj) {
        if (!(obj instanceof C1616Xf)) {
            return false;
        }
        if (bFf() == null) {
            log.warn("Illegal call to equals() of uninitialized game.script.");
            return super.equals(obj);
        } else if (bFf().getObjectId().getId() == ((C1616Xf) obj).bFf().getObjectId().getId()) {
            return true;
        } else {
            return false;
        }
    }

    /* renamed from: U */
    public C2491fm[] mo12U() {
        throw new IllegalAccessError();
    }

    /* renamed from: c */
    public C5663aRz[] mo25c() {
        throw new IllegalAccessError();
    }

    /* renamed from: aH */
    public void mo70aH() {
    }

    /* renamed from: V */
    public void mo4709V(float f) {
    }

    /* access modifiers changed from: protected */
    public <T> T getType() {
        throw new IllegalAccessError();
    }

    /* renamed from: W */
    public Object mo13W() {
        return null;
    }
}
