package logic.baa;

import game.network.message.externalizable.aCE;
import game.network.message.serializable.C2686iZ;
import logic.data.mbean.C3527s;
import logic.render.QueueItem;
import logic.res.code.C5640aRc;
import logic.res.code.C5663aRz;
import logic.res.code.C6494any;
import logic.res.html.C2491fm;
import logic.ui.item.ListJ;
import p001a.*;

import java.util.*;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.locks.ReentrantReadWriteLock;

@C5511aMd
@C6485anp
/* renamed from: a.Ju */
/* compiled from: a */
public final class C0694Ju extends aDJ implements C1616Xf {
    public static final C2491fm _f_onResurrect_0020_0028_0029V = null;
    public static final int _m_fieldCount = 0;
    public static final C5663aRz[] _m_fields = null;
    public static final int _m_methodCount = 0;
    public static final C2491fm[] _m_methods = null;
    public static final C5663aRz dPR = null;
    public static final C2491fm dPS = null;
    public static final C2491fm dPU = null;
    public static final C2491fm ifA = null;
    public static final C2491fm ifu = null;
    public static final C2491fm ifv = null;
    public static final C2491fm ifw = null;
    public static final C2491fm ifx = null;
    public static final C2491fm ify = null;
    public static final C2491fm ifz = null;
    public static final long serialVersionUID = 0;
    /* renamed from: yH */
    public static final C2491fm f886yH = null;
    public static C6494any ___iScriptClass;
    @C0064Am(aul = "28f0fef035160d07b6051630b06cd3c8", aum = 0)

    /* renamed from: aM */
    private static C2686iZ<C6920awI> f885aM;

    static {
        m6021V();
    }

    private transient ReentrantReadWriteLock hIb;
    private transient BlockingQueue<QueueItem<Boolean, C6920awI>> ifr;
    private transient Set<C6920awI> ifs;
    private transient TreeMap<Long, C0695a> ift;

    public C0694Ju() {
        super((C5540aNg) null);
        super.mo10S();
    }

    public C0694Ju(C5540aNg ang) {
        super(ang);
    }

    /* renamed from: V */
    static void m6021V() {
        serialVersionUID = (long) 1;
        _m_fieldCount = aDJ._m_fieldCount + 1;
        _m_methodCount = aDJ._m_methodCount + 11;
        int i = aDJ._m_fieldCount;
        C5663aRz[] arzArr = new C5663aRz[(i + 1)];
        C5663aRz b = C5640aRc.m17844b(C0694Ju.class, "28f0fef035160d07b6051630b06cd3c8", i);
        dPR = b;
        arzArr[i] = b;
        int i2 = i + 1;
        _m_fields = arzArr;
        C1634Xv.m11725a((Object[]) aDJ._m_fields, (Object[]) _m_fields);
        int i3 = aDJ._m_methodCount;
        C2491fm[] fmVarArr = new C2491fm[(i3 + 11)];
        C2491fm a = C4105zY.m41624a(C0694Ju.class, "1faaaa4e49fd18e1fa4a25ea7f28232e", i3);
        _f_onResurrect_0020_0028_0029V = a;
        fmVarArr[i3] = a;
        int i4 = i3 + 1;
        C2491fm a2 = C4105zY.m41624a(C0694Ju.class, "6120debd9efe5e34f1ec5fafe0185ddf", i4);
        f886yH = a2;
        fmVarArr[i4] = a2;
        int i5 = i4 + 1;
        C2491fm a3 = C4105zY.m41624a(C0694Ju.class, "b794669e21888ea7d39b274d20d84cae", i5);
        dPU = a3;
        fmVarArr[i5] = a3;
        int i6 = i5 + 1;
        C2491fm a4 = C4105zY.m41624a(C0694Ju.class, "73cb969fbdbbc810aeea7e361a56fc97", i6);
        ifu = a4;
        fmVarArr[i6] = a4;
        int i7 = i6 + 1;
        C2491fm a5 = C4105zY.m41624a(C0694Ju.class, "a3c79f4af37ad4a2dc3b2fe20d2ec9af", i7);
        ifv = a5;
        fmVarArr[i7] = a5;
        int i8 = i7 + 1;
        C2491fm a6 = C4105zY.m41624a(C0694Ju.class, "4aab7bdf920bad9a43456d556625c8c0", i8);
        ifw = a6;
        fmVarArr[i8] = a6;
        int i9 = i8 + 1;
        C2491fm a7 = C4105zY.m41624a(C0694Ju.class, "1f900af81e81b164a4ec7e124c037de5", i9);
        ifx = a7;
        fmVarArr[i9] = a7;
        int i10 = i9 + 1;
        C2491fm a8 = C4105zY.m41624a(C0694Ju.class, "28d273f22d23c61441cbdeb9ad35c32b", i10);
        ify = a8;
        fmVarArr[i10] = a8;
        int i11 = i10 + 1;
        C2491fm a9 = C4105zY.m41624a(C0694Ju.class, "ac0068f7244f5ea7e8ae673621243d2a", i11);
        ifz = a9;
        fmVarArr[i11] = a9;
        int i12 = i11 + 1;
        C2491fm a10 = C4105zY.m41624a(C0694Ju.class, "6514623a117dcbf70b1b0714f94d6fd5", i12);
        dPS = a10;
        fmVarArr[i12] = a10;
        int i13 = i12 + 1;
        C2491fm a11 = C4105zY.m41624a(C0694Ju.class, "580b34df0dea413f962f2e04ae32f060", i13);
        ifA = a11;
        fmVarArr[i13] = a11;
        int i14 = i13 + 1;
        _m_methods = fmVarArr;
        C1634Xv.m11725a((Object[]) aDJ._m_methods, (Object[]) _m_methods);
        ___iScriptClass = aUO.m18566a(C0694Ju.class, C3527s.class, _m_fields, _m_methods);
    }

    @C0064Am(aul = "1f900af81e81b164a4ec7e124c037de5", aum = 0)
    @C5566aOg
    /* renamed from: I */
    private void m6019I(List<C6920awI> list) {
        throw new aWi(new aCE(this, ifx, new Object[]{list}));
    }

    /* renamed from: J */
    private void m6020J(C2686iZ iZVar) {
        bFf().mo5608dq().mo3197f(dPR, iZVar);
    }

    @C0064Am(aul = "b794669e21888ea7d39b274d20d84cae", aum = 0)
    @C5566aOg
    /* renamed from: a */
    private void m6023a(C6920awI awi) {
        throw new aWi(new aCE(this, dPU, new Object[]{awi}));
    }

    @C0064Am(aul = "4aab7bdf920bad9a43456d556625c8c0", aum = 0)
    @C5566aOg
    /* renamed from: a */
    private void m6024a(List<C6920awI> list, int i, long j) {
        throw new aWi(new aCE(this, ifw, new Object[]{list, new Integer(i), new Long(j)}));
    }

    private C2686iZ bnk() {
        return (C2686iZ) bFf().mo5608dq().mo3214p(dPR);
    }

    @C0064Am(aul = "6514623a117dcbf70b1b0714f94d6fd5", aum = 0)
    @C5566aOg
    private Set<C6920awI> bnl() {
        throw new aWi(new aCE(this, dPS, new Object[0]));
    }

    @C0401Fa
    /* renamed from: f */
    private void m6027f(C6920awI awi) {
        switch (bFf().mo6893i(ifu)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, ifu, new Object[]{awi}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, ifu, new Object[]{awi}));
                break;
        }
        m6026e(awi);
    }

    @C0401Fa
    /* renamed from: h */
    private void m6029h(C6920awI awi) {
        switch (bFf().mo6893i(ifv)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, ifv, new Object[]{awi}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, ifv, new Object[]{awi}));
                break;
        }
        m6028g(awi);
    }

    @C0064Am(aul = "28d273f22d23c61441cbdeb9ad35c32b", aum = 0)
    @C5566aOg
    /* renamed from: i */
    private void m6030i(C6920awI awi) {
        throw new aWi(new aCE(this, ify, new Object[]{awi}));
    }

    @C0401Fa
    /* renamed from: l */
    private void m6033l(C6920awI awi) {
        switch (bFf().mo6893i(ifz)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, ifz, new Object[]{awi}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, ifz, new Object[]{awi}));
                break;
        }
        m6032k(awi);
    }

    @C5566aOg
    /* renamed from: J */
    public void mo3267J(List<C6920awI> list) {
        switch (bFf().mo6893i(ifx)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, ifx, new Object[]{list}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, ifx, new Object[]{list}));
                break;
        }
        m6019I(list);
    }

    /* renamed from: T */
    public C6494any mo11T() {
        return ___iScriptClass;
    }

    /* renamed from: U */
    public C2491fm[] mo12U() {
        return _m_methods;
    }

    /* renamed from: W */
    public Object mo13W() {
        return new C3527s(this);
    }

    /* renamed from: a */
    public Object mo14a(C0495Gr gr) {
        Object[] args = gr.getArgs();
        switch (gr.mo2417hq() - aDJ._m_methodCount) {
            case 0:
                m6025aG();
                return null;
            case 1:
                m6031jF();
                return null;
            case 2:
                m6023a((C6920awI) args[0]);
                return null;
            case 3:
                m6026e((C6920awI) args[0]);
                return null;
            case 4:
                m6028g((C6920awI) args[0]);
                return null;
            case 5:
                m6024a((List) args[0], ((Integer) args[1]).intValue(), ((Long) args[2]).longValue());
                return null;
            case 6:
                m6019I((List) args[0]);
                return null;
            case 7:
                m6030i((C6920awI) args[0]);
                return null;
            case 8:
                m6032k((C6920awI) args[0]);
                return null;
            case 9:
                return bnl();
            case 10:
                m6022a(((Long) args[0]).longValue(), (C6920awI) args[1]);
                return null;
            default:
                return super.mo14a(gr);
        }
    }

    /* renamed from: a */
    public void mo15a(Collection collection, C0495Gr gr) {
        super.mo15a(collection, gr);
    }

    /* renamed from: aH */
    public void mo70aH() {
        switch (bFf().mo6893i(_f_onResurrect_0020_0028_0029V)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, _f_onResurrect_0020_0028_0029V, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, _f_onResurrect_0020_0028_0029V, new Object[0]));
                break;
        }
        m6025aG();
    }

    @C0401Fa
    /* renamed from: b */
    public void mo3268b(long j, C6920awI awi) {
        switch (bFf().mo6893i(ifA)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, ifA, new Object[]{new Long(j), awi}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, ifA, new Object[]{new Long(j), awi}));
                break;
        }
        m6022a(j, awi);
    }

    @C5566aOg
    /* renamed from: b */
    public void mo3269b(C6920awI awi) {
        switch (bFf().mo6893i(dPU)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, dPU, new Object[]{awi}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, dPU, new Object[]{awi}));
                break;
        }
        m6023a(awi);
    }

    @C5566aOg
    /* renamed from: b */
    public void mo3270b(ListJ<C6920awI> list, int i, long j) {
        switch (bFf().mo6893i(ifw)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, ifw, new Object[]{list, new Integer(i), new Long(j)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, ifw, new Object[]{list, new Integer(i), new Long(j)}));
                break;
        }
        m6024a(list, i, j);
    }

    @C5566aOg
    public Set<C6920awI> bnm() {
        switch (bFf().mo6893i(dPS)) {
            case 0:
                return null;
            case 2:
                return (Set) bFf().mo5606d(new aCE(this, dPS, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, dPS, new Object[0]));
                break;
        }
        return bnl();
    }

    /* renamed from: c */
    public C5663aRz[] mo25c() {
        return _m_fields;
    }

    public void init() {
        switch (bFf().mo6893i(f886yH)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, f886yH, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, f886yH, new Object[0]));
                break;
        }
        m6031jF();
    }

    @C5566aOg
    /* renamed from: j */
    public void mo3273j(C6920awI awi) {
        switch (bFf().mo6893i(ify)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, ify, new Object[]{awi}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, ify, new Object[]{awi}));
                break;
        }
        m6030i(awi);
    }

    /* renamed from: S */
    public void mo10S() {
        super.mo10S();
        init();
    }

    @C0064Am(aul = "1faaaa4e49fd18e1fa4a25ea7f28232e", aum = 0)
    /* renamed from: aG */
    private void m6025aG() {
        init();
        super.mo70aH();
    }

    @C0064Am(aul = "6120debd9efe5e34f1ec5fafe0185ddf", aum = 0)
    /* renamed from: jF */
    private void m6031jF() {
        if (this.ift == null) {
            this.hIb = new ReentrantReadWriteLock();
            this.ift = new TreeMap<>();
            for (C6920awI h : bnk()) {
                m6029h(h);
            }
            this.ifr = new LinkedBlockingQueue();
            this.ifs = Collections.synchronizedSet(new HashSet());
        }
    }

    @C0401Fa
    @C0064Am(aul = "73cb969fbdbbc810aeea7e361a56fc97", aum = 0)
    /* renamed from: e */
    private void m6026e(C6920awI awi) {
        this.ifr.offer(new QueueItem(true, awi));
        m6029h(awi);
    }

    @C0401Fa
    @C0064Am(aul = "a3c79f4af37ad4a2dc3b2fe20d2ec9af", aum = 0)
    /* renamed from: g */
    private void m6028g(C6920awI awi) {
        if (this.ifs.add(awi)) {
            long boA = awi.boA();
            C0695a aVar = new C0695a(boA);
            while (true) {
                C0695a aVar2 = aVar;
                this.hIb.readLock().lock();
                try {
                    aVar = this.ift.get(Long.valueOf(boA));
                    if (aVar == null) {
                        this.hIb.writeLock().lock();
                        try {
                            aVar = this.ift.get(Long.valueOf(boA));
                            if (aVar == null) {
                                C0695a put = this.ift.put(Long.valueOf(boA), aVar2);
                                aVar = aVar2;
                            }
                        } finally {
                            this.hIb.writeLock().unlock();
                        }
                    }
                    synchronized (aVar) {
                        if (!aVar.isDisposed()) {
                            aVar.aXX().add(awi);
                            return;
                        }
                    }
                } finally {
                    this.hIb.readLock().unlock();
                }
            }
        }
    }

    @C0401Fa
    @C0064Am(aul = "ac0068f7244f5ea7e8ae673621243d2a", aum = 0)
    /* renamed from: k */
    private void m6032k(C6920awI awi) {
        this.ifr.offer(new QueueItem(false, awi));
        this.ifs.remove(awi);
    }

    @C0401Fa
    @C0064Am(aul = "580b34df0dea413f962f2e04ae32f060", aum = 0)
    /* renamed from: a */
    private void m6022a(long j, C6920awI awi) {
        this.hIb.readLock().lock();
        try {
            C0695a aVar = this.ift.get(Long.valueOf(j));
            if (aVar != null) {
                synchronized (aVar) {
                    if (!aVar.isDisposed()) {
                        this.ifs.remove(awi);
                        aVar.aXX().remove(awi);
                    }
                }
                m6029h(awi);
            }
        } finally {
            this.hIb.readLock().unlock();
        }
    }

    /* renamed from: a.Ju$a */
    public static class C0695a {
        private final long timeout;
        private volatile boolean disposed;
        private ListJ<C6920awI> dkr;

        public C0695a(long j) {
            this.timeout = j;
        }

        public ListJ<C6920awI> aXX() {
            if (this.dkr == null) {
                this.dkr = new ArrayList();
            }
            return this.dkr;
        }

        public long getTimeout() {
            return this.timeout;
        }

        public void dispose() {
            this.disposed = true;
        }

        public boolean isDisposed() {
            return this.disposed;
        }
    }
}
