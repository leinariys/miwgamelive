package logic.baa;

import game.network.message.externalizable.aCE;
import game.network.message.serializable.C2686iZ;
import logic.data.mbean.C5808aao;
import logic.res.code.C5640aRc;
import logic.res.code.C5663aRz;
import logic.res.code.C6494any;
import logic.res.html.C2491fm;
import p001a.*;

import java.util.Collection;
import java.util.List;
import java.util.Set;

@C5511aMd
@Deprecated
@C6485anp
/* renamed from: a.Pl */
/* compiled from: a */
public final class C1077Pl extends aDJ implements C1616Xf {
    public static int _m_fieldCount = 0;
    public static C5663aRz[] _m_fields = null;
    public static int _m_methodCount = 0;
    public static C2491fm[] _m_methods = null;
    public static C5663aRz dPR = null;
    public static C2491fm dPS = null;
    public static C2491fm dPT = null;
    public static C2491fm dPU = null;
    public static C2491fm dPV = null;
    public static long serialVersionUID = 0;
    public static C6494any ___iScriptClass;
    @C0064Am(aul = "39ab2209e8a334b3ffb9ba3bd0c67623", aum = 0)

    /* renamed from: aM */
    private static C2686iZ<C6920awI> f1383aM;

    static {
        m8610V();
    }

    public C1077Pl() {
        super((C5540aNg) null);
        super.mo10S();
    }

    public C1077Pl(C5540aNg ang) {
        super(ang);
    }

    /* renamed from: V */
    static void m8610V() {
        serialVersionUID = (long) 1;
        _m_fieldCount = aDJ._m_fieldCount + 1;
        _m_methodCount = aDJ._m_methodCount + 4;
        int i = aDJ._m_fieldCount;
        C5663aRz[] arzArr = new C5663aRz[(i + 1)];
        C5663aRz b = C5640aRc.m17844b(C1077Pl.class, "39ab2209e8a334b3ffb9ba3bd0c67623", i);
        dPR = b;
        arzArr[i] = b;
        int i2 = i + 1;
        _m_fields = arzArr;
        C1634Xv.m11725a((Object[]) aDJ._m_fields, (Object[]) _m_fields);
        int i3 = aDJ._m_methodCount;
        C2491fm[] fmVarArr = new C2491fm[(i3 + 4)];
        C2491fm a = C4105zY.m41624a(C1077Pl.class, "46a122b68a67e461f9db2d80cc4cef07", i3);
        dPS = a;
        fmVarArr[i3] = a;
        int i4 = i3 + 1;
        C2491fm a2 = C4105zY.m41624a(C1077Pl.class, "25aacf12d4bbbb6d9cf2fd4cb5ca1411", i4);
        dPT = a2;
        fmVarArr[i4] = a2;
        int i5 = i4 + 1;
        C2491fm a3 = C4105zY.m41624a(C1077Pl.class, "24403632745f48aa838cd78874878277", i5);
        dPU = a3;
        fmVarArr[i5] = a3;
        int i6 = i5 + 1;
        C2491fm a4 = C4105zY.m41624a(C1077Pl.class, "b72c4d33e398d1d22d4ecf71a3caba86", i6);
        dPV = a4;
        fmVarArr[i6] = a4;
        int i7 = i6 + 1;
        _m_methods = fmVarArr;
        C1634Xv.m11725a((Object[]) aDJ._m_methods, (Object[]) _m_methods);
        ___iScriptClass = aUO.m18566a(C1077Pl.class, C5808aao.class, _m_fields, _m_methods);
    }

    /* renamed from: J */
    private void m8609J(C2686iZ iZVar) {
        bFf().mo5608dq().mo3197f(dPR, iZVar);
    }

    @C0064Am(aul = "b72c4d33e398d1d22d4ecf71a3caba86", aum = 0)
    @C5566aOg
    /* renamed from: a */
    private List<C6920awI> m8611a(int i, long j) {
        throw new aWi(new aCE(this, dPV, new Object[]{new Integer(i), new Long(j)}));
    }

    @C0064Am(aul = "24403632745f48aa838cd78874878277", aum = 0)
    @C5566aOg
    /* renamed from: a */
    private void m8612a(C6920awI awi) {
        throw new aWi(new aCE(this, dPU, new Object[]{awi}));
    }

    private C2686iZ bnk() {
        return (C2686iZ) bFf().mo5608dq().mo3214p(dPR);
    }

    @C0064Am(aul = "46a122b68a67e461f9db2d80cc4cef07", aum = 0)
    @C5566aOg
    private Set<C6920awI> bnl() {
        throw new aWi(new aCE(this, dPS, new Object[0]));
    }

    @C0064Am(aul = "25aacf12d4bbbb6d9cf2fd4cb5ca1411", aum = 0)
    @C5566aOg
    /* renamed from: g */
    private void m8613g(Set<C6920awI> set) {
        throw new aWi(new aCE(this, dPT, new Object[]{set}));
    }

    /* renamed from: T */
    public C6494any mo11T() {
        return ___iScriptClass;
    }

    /* renamed from: U */
    public C2491fm[] mo12U() {
        return _m_methods;
    }

    /* renamed from: W */
    public Object mo13W() {
        return new C5808aao(this);
    }

    /* renamed from: a */
    public Object mo14a(C0495Gr gr) {
        Object[] args = gr.getArgs();
        switch (gr.mo2417hq() - aDJ._m_methodCount) {
            case 0:
                return bnl();
            case 1:
                m8613g((Set) args[0]);
                return null;
            case 2:
                m8612a((C6920awI) args[0]);
                return null;
            case 3:
                return m8611a(((Integer) args[0]).intValue(), ((Long) args[1]).longValue());
            default:
                return super.mo14a(gr);
        }
    }

    /* renamed from: a */
    public void mo15a(Collection collection, C0495Gr gr) {
        super.mo15a(collection, gr);
    }

    @C5566aOg
    /* renamed from: b */
    public List<C6920awI> mo4831b(int i, long j) {
        switch (bFf().mo6893i(dPV)) {
            case 0:
                return null;
            case 2:
                return (List) bFf().mo5606d(new aCE(this, dPV, new Object[]{new Integer(i), new Long(j)}));
            case 3:
                bFf().mo5606d(new aCE(this, dPV, new Object[]{new Integer(i), new Long(j)}));
                break;
        }
        return m8611a(i, j);
    }

    @C5566aOg
    /* renamed from: b */
    public void mo4832b(C6920awI awi) {
        switch (bFf().mo6893i(dPU)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, dPU, new Object[]{awi}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, dPU, new Object[]{awi}));
                break;
        }
        m8612a(awi);
    }

    @C5566aOg
    public Set<C6920awI> bnm() {
        switch (bFf().mo6893i(dPS)) {
            case 0:
                return null;
            case 2:
                return (Set) bFf().mo5606d(new aCE(this, dPS, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, dPS, new Object[0]));
                break;
        }
        return bnl();
    }

    /* renamed from: c */
    public C5663aRz[] mo25c() {
        return _m_fields;
    }

    @C5566aOg
    /* renamed from: h */
    public void mo4834h(Set<C6920awI> set) {
        switch (bFf().mo6893i(dPT)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, dPT, new Object[]{set}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, dPT, new Object[]{set}));
                break;
        }
        m8613g(set);
    }

    /* renamed from: S */
    public void mo10S() {
        super.mo10S();
    }
}
