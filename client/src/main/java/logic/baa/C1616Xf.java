package logic.baa;

import game.DataNetworkInformation;
import logic.res.code.C5663aRz;
import logic.res.code.C6494any;
import logic.res.html.C2491fm;
import logic.res.html.C6663arL;
import p001a.C0495Gr;
import p001a.C6147ahP;

import java.io.Serializable;
import java.util.Collection;

/* renamed from: a.Xf */
/* compiled from: a */
public interface C1616Xf extends DataNetworkInformation, Serializable {
    /* renamed from: T */
    C6494any mo11T();

    /* renamed from: U */
    C2491fm[] mo12U();

    /* renamed from: V */
    void mo4709V(float f);

    /* renamed from: W */
    Object mo13W();

    /* renamed from: a */
    Object mo14a(C0495Gr gr);

    /* renamed from: a */
    void mo6753a(Thread thread);

    /* renamed from: a */
    void mo15a(Collection collection, C0495Gr gr);

    /* renamed from: aH */
    void mo70aH();

    /* renamed from: b */
    void mo6754b(C6663arL arl);

    C6663arL bFf();

    void bFg();

    C6147ahP[] bFh();

    void bFi();

    void bFj();

    boolean isUseClientOnly();

    boolean isUseServerOnly();

    boolean bFm();

    boolean bFn();

    Thread bFo();

    /* renamed from: c */
    C5663aRz[] mo25c();

    /* renamed from: g */
    void mo6765g(C5663aRz arz, Object obj);

    /* renamed from: h */
    void mo6766h(C1616Xf xf);

    /* renamed from: w */
    Object mo6767w(C5663aRz arz);
}
