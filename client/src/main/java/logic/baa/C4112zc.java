package logic.baa;

import game.network.message.externalizable.aCE;
import game.network.message.serializable.C2686iZ;
import logic.data.mbean.C1388UL;
import logic.res.code.C5640aRc;
import logic.res.code.C5663aRz;
import logic.res.code.C6494any;
import logic.res.html.C2491fm;
import p001a.*;

import java.util.Collection;
import java.util.Set;

@C5511aMd
@C6485anp
/* renamed from: a.zc */
/* compiled from: a */
public class C4112zc extends aDJ implements C1616Xf {
    public static final C2491fm _f_dispose_0020_0028_0029V = null;
    public static final int _m_fieldCount = 0;
    public static final C5663aRz[] _m_fields = null;
    public static final int _m_methodCount = 0;
    public static final C2491fm[] _m_methods = null;
    public static final C5663aRz bNh = null;
    public static final C2491fm bNi = null;
    public static final C2491fm bNj = null;
    public static final long serialVersionUID = 0;
    public static C6494any ___iScriptClass;
    @C0064Am(aul = "a0f6e9d63cb069fec861d4868dd4579f", aum = 0)
    private static C2686iZ<aDJ> bNg;

    static {
        m41650V();
    }

    public C4112zc() {
        super((C5540aNg) null);
        super.mo10S();
    }

    public C4112zc(C5540aNg ang) {
        super(ang);
    }

    /* renamed from: V */
    static void m41650V() {
        serialVersionUID = (long) 1;
        _m_fieldCount = aDJ._m_fieldCount + 1;
        _m_methodCount = aDJ._m_methodCount + 3;
        int i = aDJ._m_fieldCount;
        C5663aRz[] arzArr = new C5663aRz[(i + 1)];
        C5663aRz b = C5640aRc.m17844b(C4112zc.class, "a0f6e9d63cb069fec861d4868dd4579f", i);
        bNh = b;
        arzArr[i] = b;
        int i2 = i + 1;
        _m_fields = arzArr;
        C1634Xv.m11725a((Object[]) aDJ._m_fields, (Object[]) _m_fields);
        int i3 = aDJ._m_methodCount;
        C2491fm[] fmVarArr = new C2491fm[(i3 + 3)];
        C2491fm a = C4105zY.m41624a(C4112zc.class, "75a9f8438b9f6f4c80fd05d73c6af064", i3);
        bNi = a;
        fmVarArr[i3] = a;
        int i4 = i3 + 1;
        C2491fm a2 = C4105zY.m41624a(C4112zc.class, "c0780f963aff52bbd072e049ec213053", i4);
        _f_dispose_0020_0028_0029V = a2;
        fmVarArr[i4] = a2;
        int i5 = i4 + 1;
        C2491fm a3 = C4105zY.m41624a(C4112zc.class, "5903ab787e587c9af3e58a41315891dd", i5);
        bNj = a3;
        fmVarArr[i5] = a3;
        int i6 = i5 + 1;
        _m_methods = fmVarArr;
        C1634Xv.m11725a((Object[]) aDJ._m_methods, (Object[]) _m_methods);
        ___iScriptClass = aUO.m18566a(C4112zc.class, C1388UL.class, _m_fields, _m_methods);
    }

    private C2686iZ aqU() {
        return (C2686iZ) bFf().mo5608dq().mo3214p(bNh);
    }

    /* renamed from: o */
    private void m41653o(C2686iZ iZVar) {
        bFf().mo5608dq().mo3197f(bNh, iZVar);
    }

    /* renamed from: T */
    public C6494any mo11T() {
        return ___iScriptClass;
    }

    /* renamed from: U */
    public C2491fm[] mo12U() {
        return _m_methods;
    }

    /* renamed from: W */
    public Object mo13W() {
        return new C1388UL(this);
    }

    /* renamed from: a */
    public Object mo14a(C0495Gr gr) {
        Object[] args = gr.getArgs();
        switch (gr.mo2417hq() - aDJ._m_methodCount) {
            case 0:
                return aqV();
            case 1:
                m41651fg();
                return null;
            case 2:
                m41652g((aDJ) args[0]);
                return null;
            default:
                return super.mo14a(gr);
        }
    }

    /* renamed from: a */
    public void mo15a(Collection collection, C0495Gr gr) {
        super.mo15a(collection, gr);
    }

    public Set<aDJ> aqW() {
        switch (bFf().mo6893i(bNi)) {
            case 0:
                return null;
            case 2:
                return (Set) bFf().mo5606d(new aCE(this, bNi, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, bNi, new Object[0]));
                break;
        }
        return aqV();
    }

    /* renamed from: c */
    public C5663aRz[] mo25c() {
        return _m_fields;
    }

    public void dispose() {
        switch (bFf().mo6893i(_f_dispose_0020_0028_0029V)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, _f_dispose_0020_0028_0029V, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, _f_dispose_0020_0028_0029V, new Object[0]));
                break;
        }
        m41651fg();
    }

    /* renamed from: h */
    public void mo23358h(aDJ adj) {
        switch (bFf().mo6893i(bNj)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, bNj, new Object[]{adj}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, bNj, new Object[]{adj}));
                break;
        }
        m41652g(adj);
    }

    /* renamed from: S */
    public void mo10S() {
        super.mo10S();
    }

    @C0064Am(aul = "75a9f8438b9f6f4c80fd05d73c6af064", aum = 0)
    private Set<aDJ> aqV() {
        return aqU();
    }

    @C0064Am(aul = "c0780f963aff52bbd072e049ec213053", aum = 0)
    /* renamed from: fg */
    private void m41651fg() {
        aqU().clear();
        super.dispose();
    }

    @C0064Am(aul = "5903ab787e587c9af3e58a41315891dd", aum = 0)
    /* renamed from: g */
    private void m41652g(aDJ adj) {
        aqU().remove(adj);
    }
}
