package logic.baa;

import game.network.message.externalizable.aCE;
import game.script.ai.npc.AIControllerType;
import game.script.ai.npc.TurretAIController;
import logic.data.mbean.C5896acY;
import logic.res.code.C5640aRc;
import logic.res.code.C5663aRz;
import logic.res.code.C6494any;
import logic.res.html.C2491fm;
import p001a.*;

import java.util.Collection;

@C0566Hp
@C5511aMd
@C6485anp
/* renamed from: a.KI */
/* compiled from: a */
public class C0712KI extends AIControllerType implements C1616Xf {
    public static final int _m_fieldCount = 0;
    public static final C5663aRz[] _m_fields = null;
    public static final int _m_methodCount = 0;
    public static final C2491fm[] _m_methods = null;
    /* renamed from: dN */
    public static final C2491fm f935dN = null;
    public static final C5663aRz dqA = null;
    public static final C2491fm dqB = null;
    public static final C2491fm dqC = null;
    public static final C2491fm dqD = null;
    public static final C2491fm dqE = null;
    public static final C2491fm dqF = null;
    public static final C5663aRz dqy = null;
    public static final long serialVersionUID = 0;
    public static C6494any ___iScriptClass;
    @C0064Am(aul = "fd3715b4860dbb200e57d7d6af001c9e", aum = 0)
    private static float dqx;
    @C0064Am(aul = "6cdfebcebcb088abbed54d4a5e5a5574", aum = 1)
    private static float dqz;

    static {
        m6278V();
    }

    public C0712KI() {
        super((C5540aNg) null);
        super.mo10S();
    }

    public C0712KI(C5540aNg ang) {
        super(ang);
    }

    /* renamed from: V */
    static void m6278V() {
        serialVersionUID = (long) 1;
        _m_fieldCount = AIControllerType._m_fieldCount + 2;
        _m_methodCount = AIControllerType._m_methodCount + 6;
        int i = AIControllerType._m_fieldCount;
        C5663aRz[] arzArr = new C5663aRz[(i + 2)];
        C5663aRz b = C5640aRc.m17844b(C0712KI.class, "fd3715b4860dbb200e57d7d6af001c9e", i);
        dqy = b;
        arzArr[i] = b;
        int i2 = i + 1;
        C5663aRz b2 = C5640aRc.m17844b(C0712KI.class, "6cdfebcebcb088abbed54d4a5e5a5574", i2);
        dqA = b2;
        arzArr[i2] = b2;
        int i3 = i2 + 1;
        _m_fields = arzArr;
        C1634Xv.m11725a((Object[]) AIControllerType._m_fields, (Object[]) _m_fields);
        int i4 = AIControllerType._m_methodCount;
        C2491fm[] fmVarArr = new C2491fm[(i4 + 6)];
        C2491fm a = C4105zY.m41624a(C0712KI.class, "55c985cefd0b8d8d7bb6347a30e1f37c", i4);
        dqB = a;
        fmVarArr[i4] = a;
        int i5 = i4 + 1;
        C2491fm a2 = C4105zY.m41624a(C0712KI.class, "ecbeddcb3d5bdfc16807964d9b6ec196", i5);
        dqC = a2;
        fmVarArr[i5] = a2;
        int i6 = i5 + 1;
        C2491fm a3 = C4105zY.m41624a(C0712KI.class, "c1500f06db610d9cc864f7a6a0023fa4", i6);
        dqD = a3;
        fmVarArr[i6] = a3;
        int i7 = i6 + 1;
        C2491fm a4 = C4105zY.m41624a(C0712KI.class, "6f298b64ecd89cc68a8181aecf039862", i7);
        dqE = a4;
        fmVarArr[i7] = a4;
        int i8 = i7 + 1;
        C2491fm a5 = C4105zY.m41624a(C0712KI.class, "f074fff065d8e8716f04feab8fcc3718", i8);
        dqF = a5;
        fmVarArr[i8] = a5;
        int i9 = i8 + 1;
        C2491fm a6 = C4105zY.m41624a(C0712KI.class, "3de86391e5f9cb050018d250b54c7054", i9);
        f935dN = a6;
        fmVarArr[i9] = a6;
        int i10 = i9 + 1;
        _m_methods = fmVarArr;
        C1634Xv.m11725a((Object[]) AIControllerType._m_methods, (Object[]) _m_methods);
        ___iScriptClass = aUO.m18566a(C0712KI.class, C5896acY.class, _m_fields, _m_methods);
    }

    private float bcV() {
        return bFf().mo5608dq().mo3211m(dqy);
    }

    private float bcW() {
        return bFf().mo5608dq().mo3211m(dqA);
    }

    /* renamed from: fR */
    private void m6280fR(float f) {
        bFf().mo5608dq().mo3150a(dqy, f);
    }

    /* renamed from: fS */
    private void m6281fS(float f) {
        bFf().mo5608dq().mo3150a(dqA, f);
    }

    /* renamed from: T */
    public C6494any mo11T() {
        return ___iScriptClass;
    }

    /* renamed from: U */
    public C2491fm[] mo12U() {
        return _m_methods;
    }

    /* renamed from: W */
    public Object mo13W() {
        return new C5896acY(this);
    }

    /* renamed from: a */
    public Object mo14a(C0495Gr gr) {
        Object[] args = gr.getArgs();
        switch (gr.mo2417hq() - AIControllerType._m_methodCount) {
            case 0:
                return new Float(bcX());
            case 1:
                m6282fT(((Float) args[0]).floatValue());
                return null;
            case 2:
                return new Float(bcZ());
            case 3:
                m6283fV(((Float) args[0]).floatValue());
                return null;
            case 4:
                return bdb();
            case 5:
                return m6279aT();
            default:
                return super.mo14a(gr);
        }
    }

    /* renamed from: a */
    public void mo15a(Collection collection, C0495Gr gr) {
        super.mo15a(collection, gr);
    }

    /* access modifiers changed from: protected */
    /* renamed from: aU */
    public <T extends aDJ> T mo745aU() {
        switch (bFf().mo6893i(f935dN)) {
            case 0:
                return null;
            case 2:
                return (aDJ) bFf().mo5606d(new aCE(this, f935dN, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, f935dN, new Object[0]));
                break;
        }
        return m6279aT();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Time to remove an target from target list (s)\nUse 0 to never remove")
    public float bcY() {
        switch (bFf().mo6893i(dqB)) {
            case 0:
                return 0.0f;
            case 2:
                return ((Float) bFf().mo5606d(new aCE(this, dqB, new Object[0]))).floatValue();
            case 3:
                bFf().mo5606d(new aCE(this, dqB, new Object[0]));
                break;
        }
        return bcX();
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "How long a target will remain locked (s)\n(if there is more than one target in list)")
    public float bda() {
        switch (bFf().mo6893i(dqD)) {
            case 0:
                return 0.0f;
            case 2:
                return ((Float) bFf().mo5606d(new aCE(this, dqD, new Object[0]))).floatValue();
            case 3:
                bFf().mo5606d(new aCE(this, dqD, new Object[0]));
                break;
        }
        return bcZ();
    }

    public TurretAIController bdc() {
        switch (bFf().mo6893i(dqF)) {
            case 0:
                return null;
            case 2:
                return (TurretAIController) bFf().mo5606d(new aCE(this, dqF, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, dqF, new Object[0]));
                break;
        }
        return bdb();
    }

    /* renamed from: c */
    public C5663aRz[] mo25c() {
        return _m_fields;
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Time to remove an target from target list (s)\nUse 0 to never remove")
    /* renamed from: fU */
    public void mo3497fU(float f) {
        switch (bFf().mo6893i(dqC)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, dqC, new Object[]{new Float(f)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, dqC, new Object[]{new Float(f)}));
                break;
        }
        m6282fT(f);
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "How long a target will remain locked (s)\n(if there is more than one target in list)")
    /* renamed from: fW */
    public void mo3498fW(float f) {
        switch (bFf().mo6893i(dqE)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, dqE, new Object[]{new Float(f)}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, dqE, new Object[]{new Float(f)}));
                break;
        }
        m6283fV(f);
    }

    /* renamed from: S */
    public void mo10S() {
        super.mo10S();
        m6280fR(0.0f);
        m6281fS(20.0f);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "Time to remove an target from target list (s)\nUse 0 to never remove")
    @C0064Am(aul = "55c985cefd0b8d8d7bb6347a30e1f37c", aum = 0)
    private float bcX() {
        return bcV();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "Time to remove an target from target list (s)\nUse 0 to never remove")
    @C0064Am(aul = "ecbeddcb3d5bdfc16807964d9b6ec196", aum = 0)
    /* renamed from: fT */
    private void m6282fT(float f) {
        m6280fR(f);
    }

    @C3248pc(aYR = C3248pc.C3250b.GETTER, displayName = "How long a target will remain locked (s)\n(if there is more than one target in list)")
    @C0064Am(aul = "c1500f06db610d9cc864f7a6a0023fa4", aum = 0)
    private float bcZ() {
        return bcW();
    }

    @C3248pc(aYR = C3248pc.C3250b.SETTER, displayName = "How long a target will remain locked (s)\n(if there is more than one target in list)")
    @C0064Am(aul = "6f298b64ecd89cc68a8181aecf039862", aum = 0)
    /* renamed from: fV */
    private void m6283fV(float f) {
        m6281fS(f);
    }

    @C0064Am(aul = "f074fff065d8e8716f04feab8fcc3718", aum = 0)
    private TurretAIController bdb() {
        return (TurretAIController) mo745aU();
    }

    @C0064Am(aul = "3de86391e5f9cb050018d250b54c7054", aum = 0)
    /* renamed from: aT */
    private <T extends aDJ> T m6279aT() {
        T t = (TurretAIController) bFf().mo6865M(TurretAIController.class);
        t.mo15674d(this);
        return t;
    }
}
