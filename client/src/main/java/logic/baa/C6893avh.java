package logic.baa;

import game.network.message.externalizable.aCE;
import game.network.message.serializable.C3438ra;
import logic.data.mbean.C3611sv;
import logic.res.code.C5640aRc;
import logic.res.code.C5663aRz;
import logic.res.code.C6494any;
import logic.res.html.C2491fm;
import p001a.*;

import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;

@C5511aMd
@C6485anp
/* renamed from: a.avh  reason: case insensitive filesystem */
/* compiled from: a */
public class C6893avh<T> extends aDJ implements List<T>, List {
    public static final C2491fm _f_delete_0020_0028_0029V = null;
    public static final int _m_fieldCount = 0;
    public static final C5663aRz[] _m_fields = null;
    public static final int _m_methodCount = 0;
    public static final C2491fm[] _m_methods = null;
    public static final C2491fm aJH = null;
    public static final C2491fm bqB = null;
    public static final C2491fm cbl = null;
    public static final C2491fm cbm = null;
    public static final C2491fm dxE = null;
    public static final C2491fm dxF = null;
    public static final C2491fm dxG = null;
    public static final C2491fm dxH = null;
    public static final C2491fm dxI = null;
    public static final C2491fm dxJ = null;
    public static final C2491fm dxK = null;
    public static final C2491fm dxL = null;
    public static final C2491fm dxM = null;
    public static final C2491fm gFT = null;
    public static final C2491fm gFU = null;
    public static final C2491fm gFV = null;
    public static final C2491fm gFW = null;
    public static final C2491fm gFX = null;
    public static final C2491fm gFY = null;
    public static final C2491fm gFZ = null;
    public static final C2491fm gGa = null;
    public static final C2491fm gGb = null;
    public static final C2491fm gGc = null;
    public static final long serialVersionUID = 0;
    /* renamed from: zh */
    public static final C5663aRz f5452zh = null;
    public static C6494any ___iScriptClass;
    @C0064Am(aul = "d62f2f91c688be6286a4d000f07c6e78", aum = 0)

    /* renamed from: zg */
    private static C3438ra<T> f5451zg;

    static {
        m26624V();
    }

    public C6893avh() {
        super((C5540aNg) null);
        super.mo10S();
    }

    public C6893avh(C5540aNg ang) {
        super(ang);
    }

    /* renamed from: V */
    static void m26624V() {
        serialVersionUID = (long) 1;
        _m_fieldCount = aDJ._m_fieldCount + 1;
        _m_methodCount = aDJ._m_methodCount + 24;
        int i = aDJ._m_fieldCount;
        C5663aRz[] arzArr = new C5663aRz[(i + 1)];
        C5663aRz b = C5640aRc.m17844b(C6893avh.class, "d62f2f91c688be6286a4d000f07c6e78", i);
        f5452zh = b;
        arzArr[i] = b;
        int i2 = i + 1;
        _m_fields = arzArr;
        C1634Xv.m11725a((Object[]) aDJ._m_fields, (Object[]) _m_fields);
        int i3 = aDJ._m_methodCount;
        C2491fm[] fmVarArr = new C2491fm[(i3 + 24)];
        C2491fm a = C4105zY.m41624a(C6893avh.class, "e4f4772c2014384e149ffc5b9283302c", i3);
        dxE = a;
        fmVarArr[i3] = a;
        int i4 = i3 + 1;
        C2491fm a2 = C4105zY.m41624a(C6893avh.class, "a1c17fe680097207b768306d61bfd85e", i4);
        gFT = a2;
        fmVarArr[i4] = a2;
        int i5 = i4 + 1;
        C2491fm a3 = C4105zY.m41624a(C6893avh.class, "ed7597e68cd607c1bcbc1080d494c036", i5);
        dxF = a3;
        fmVarArr[i5] = a3;
        int i6 = i5 + 1;
        C2491fm a4 = C4105zY.m41624a(C6893avh.class, "01c4a3f4b8f921a2fcf32a19a25d8a72", i6);
        gFU = a4;
        fmVarArr[i6] = a4;
        int i7 = i6 + 1;
        C2491fm a5 = C4105zY.m41624a(C6893avh.class, "686c51014d04d9b18102111e71f5a8c7", i7);
        cbm = a5;
        fmVarArr[i7] = a5;
        int i8 = i7 + 1;
        C2491fm a6 = C4105zY.m41624a(C6893avh.class, "d94c25b818bf1c6e25483a5ddda56628", i8);
        dxG = a6;
        fmVarArr[i8] = a6;
        int i9 = i8 + 1;
        C2491fm a7 = C4105zY.m41624a(C6893avh.class, "888bbb1a6ff4711817912e632b69e4c7", i9);
        dxH = a7;
        fmVarArr[i9] = a7;
        int i10 = i9 + 1;
        C2491fm a8 = C4105zY.m41624a(C6893avh.class, "1d7905be8614dfe7a0c54d12edceafe9", i10);
        gFV = a8;
        fmVarArr[i10] = a8;
        int i11 = i10 + 1;
        C2491fm a9 = C4105zY.m41624a(C6893avh.class, "165e917b7642b3364983ed51926db1b9", i11);
        gFW = a9;
        fmVarArr[i11] = a9;
        int i12 = i11 + 1;
        C2491fm a10 = C4105zY.m41624a(C6893avh.class, "625bef537d75b6c944335ac2f3a8235a", i12);
        bqB = a10;
        fmVarArr[i12] = a10;
        int i13 = i12 + 1;
        C2491fm a11 = C4105zY.m41624a(C6893avh.class, "2e7c15a8931f8cbc7b0eda2349a33ecf", i13);
        dxI = a11;
        fmVarArr[i13] = a11;
        int i14 = i13 + 1;
        C2491fm a12 = C4105zY.m41624a(C6893avh.class, "41d6776e4d0f89c0e9373d96d6345841", i14);
        gFX = a12;
        fmVarArr[i14] = a12;
        int i15 = i14 + 1;
        C2491fm a13 = C4105zY.m41624a(C6893avh.class, "32a280370f5eb5254e333245f40b59fb", i15);
        gFY = a13;
        fmVarArr[i15] = a13;
        int i16 = i15 + 1;
        C2491fm a14 = C4105zY.m41624a(C6893avh.class, "babbef7e82b45a5f3561fa0e768b992d", i16);
        gFZ = a14;
        fmVarArr[i16] = a14;
        int i17 = i16 + 1;
        C2491fm a15 = C4105zY.m41624a(C6893avh.class, "1bf68d151ffc2e2a5c8da95aedb2d28c", i17);
        cbl = a15;
        fmVarArr[i17] = a15;
        int i18 = i17 + 1;
        C2491fm a16 = C4105zY.m41624a(C6893avh.class, "14e8781210f501892dedf816678acaeb", i18);
        gGa = a16;
        fmVarArr[i18] = a16;
        int i19 = i18 + 1;
        C2491fm a17 = C4105zY.m41624a(C6893avh.class, "1db4037085887d682f3fe8f781e35b2d", i19);
        dxJ = a17;
        fmVarArr[i19] = a17;
        int i20 = i19 + 1;
        C2491fm a18 = C4105zY.m41624a(C6893avh.class, "9fec2b3345d84fedf617c1b12894b7c0", i20);
        dxK = a18;
        fmVarArr[i20] = a18;
        int i21 = i20 + 1;
        C2491fm a19 = C4105zY.m41624a(C6893avh.class, "daf28719dbfcc82916107bea1898a25e", i21);
        gGb = a19;
        fmVarArr[i21] = a19;
        int i22 = i21 + 1;
        C2491fm a20 = C4105zY.m41624a(C6893avh.class, "f1b91bc02ca3cfec6a7306e983346cbf", i22);
        aJH = a20;
        fmVarArr[i22] = a20;
        int i23 = i22 + 1;
        C2491fm a21 = C4105zY.m41624a(C6893avh.class, "16f39cb811a0a5e6ddf217919b626821", i23);
        gGc = a21;
        fmVarArr[i23] = a21;
        int i24 = i23 + 1;
        C2491fm a22 = C4105zY.m41624a(C6893avh.class, "abf15856f6a7abac40bc3ca36a4a2973", i24);
        dxL = a22;
        fmVarArr[i24] = a22;
        int i25 = i24 + 1;
        C2491fm a23 = C4105zY.m41624a(C6893avh.class, "68f973c94cd6c92bc7cf9e5294fbe26b", i25);
        dxM = a23;
        fmVarArr[i25] = a23;
        int i26 = i25 + 1;
        C2491fm a24 = C4105zY.m41624a(C6893avh.class, "3a63e37208b65007d69957d757d42c1e", i26);
        _f_delete_0020_0028_0029V = a24;
        fmVarArr[i26] = a24;
        int i27 = i26 + 1;
        _m_methods = fmVarArr;
        C1634Xv.m11725a((Object[]) aDJ._m_methods, (Object[]) _m_methods);
        ___iScriptClass = aUO.m18566a(C6893avh.class, C3611sv.class, _m_fields, _m_methods);
    }

    /* renamed from: jP */
    private C3438ra m26632jP() {
        return (C3438ra) bFf().mo5608dq().mo3214p(f5452zh);
    }

    /* renamed from: p */
    private void m26637p(C3438ra raVar) {
        bFf().mo5608dq().mo3197f(f5452zh, raVar);
    }

    /* renamed from: T */
    public C6494any mo11T() {
        return ___iScriptClass;
    }

    /* renamed from: U */
    public C2491fm[] mo12U() {
        return _m_methods;
    }

    /* renamed from: W */
    public Object mo13W() {
        return new C3611sv(this);
    }

    /* renamed from: a */
    public Object mo14a(C0495Gr gr) {
        Object[] args = gr.getArgs();
        switch (gr.mo2417hq() - aDJ._m_methodCount) {
            case 0:
                return new Boolean(m26623U(args[0]));
            case 1:
                m26630e(((Integer) args[0]).intValue(), args[1]);
                return null;
            case 2:
                return new Boolean(m26633m((Collection) args[0]));
            case 3:
                return new Boolean(m26626a(((Integer) args[0]).intValue(), (Collection) args[1]));
            case 4:
                asC();
                return null;
            case 5:
                return new Boolean(m26625V(args[0]));
            case 6:
                return new Boolean(m26634n((Collection<?>) (Collection) args[0]));
            case 7:
                return m26639uN(((Integer) args[0]).intValue());
            case 8:
                return new Integer(m26628aw(args[0]));
            case 9:
                return new Boolean(ahi());
            case 10:
                return bgf();
            case 11:
                return new Integer(m26629ax(args[0]));
            case 12:
                return cyC();
            case 13:
                return m26640uO(((Integer) args[0]).intValue());
            case 14:
                return new Boolean(m26621J(args[0]));
            case 15:
                return m26641uP(((Integer) args[0]).intValue());
            case 16:
                return new Boolean(m26636o((Collection) args[0]));
            case 17:
                return new Boolean(m26638p((Collection<?>) (Collection) args[0]));
            case 18:
                return m26631f(((Integer) args[0]).intValue(), args[1]);
            case 19:
                return new Integer(m26622QD());
            case 20:
                return m26627ah(((Integer) args[0]).intValue(), ((Integer) args[1]).intValue());
            case 21:
                return bgg();
            case 22:
                return m26635n((T[]) (Object[]) args[0]);
            case 23:
                cyD();
                return null;
            default:
                return super.mo14a(gr);
        }
    }

    /* renamed from: a */
    public void mo15a(Collection collection, C0495Gr gr) {
        super.mo15a(collection, gr);
    }

    public void add(int i, T t) {
        switch (bFf().mo6893i(gFT)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, gFT, new Object[]{new Integer(i), t}));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, gFT, new Object[]{new Integer(i), t}));
                break;
        }
        m26630e(i, t);
    }

    public boolean add(T t) {
        switch (bFf().mo6893i(dxE)) {
            case 0:
                return false;
            case 2:
                return ((Boolean) bFf().mo5606d(new aCE(this, dxE, new Object[]{t}))).booleanValue();
            case 3:
                bFf().mo5606d(new aCE(this, dxE, new Object[]{t}));
                break;
        }
        return m26623U(t);
    }

    public boolean addAll(int i, Collection<? extends T> collection) {
        switch (bFf().mo6893i(gFU)) {
            case 0:
                return false;
            case 2:
                return ((Boolean) bFf().mo5606d(new aCE(this, gFU, new Object[]{new Integer(i), collection}))).booleanValue();
            case 3:
                bFf().mo5606d(new aCE(this, gFU, new Object[]{new Integer(i), collection}));
                break;
        }
        return m26626a(i, collection);
    }

    public boolean addAll(Collection<? extends T> collection) {
        switch (bFf().mo6893i(dxF)) {
            case 0:
                return false;
            case 2:
                return ((Boolean) bFf().mo5606d(new aCE(this, dxF, new Object[]{collection}))).booleanValue();
            case 3:
                bFf().mo5606d(new aCE(this, dxF, new Object[]{collection}));
                break;
        }
        return m26633m(collection);
    }

    /* renamed from: c */
    public C5663aRz[] mo25c() {
        return _m_fields;
    }

    public void clear() {
        switch (bFf().mo6893i(cbm)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, cbm, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, cbm, new Object[0]));
                break;
        }
        asC();
    }

    public boolean contains(Object obj) {
        switch (bFf().mo6893i(dxG)) {
            case 0:
                return false;
            case 2:
                return ((Boolean) bFf().mo5606d(new aCE(this, dxG, new Object[]{obj}))).booleanValue();
            case 3:
                bFf().mo5606d(new aCE(this, dxG, new Object[]{obj}));
                break;
        }
        return m26625V(obj);
    }

    public boolean containsAll(Collection<?> collection) {
        switch (bFf().mo6893i(dxH)) {
            case 0:
                return false;
            case 2:
                return ((Boolean) bFf().mo5606d(new aCE(this, dxH, new Object[]{collection}))).booleanValue();
            case 3:
                bFf().mo5606d(new aCE(this, dxH, new Object[]{collection}));
                break;
        }
        return m26634n(collection);
    }

    public void delete() {
        switch (bFf().mo6893i(_f_delete_0020_0028_0029V)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, _f_delete_0020_0028_0029V, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, _f_delete_0020_0028_0029V, new Object[0]));
                break;
        }
        cyD();
    }

    public T get(int i) {
        switch (bFf().mo6893i(gFV)) {
            case 0:
                return null;
            case 2:
                return bFf().mo5606d(new aCE(this, gFV, new Object[]{new Integer(i)}));
            case 3:
                bFf().mo5606d(new aCE(this, gFV, new Object[]{new Integer(i)}));
                break;
        }
        return m26639uN(i);
    }

    public int indexOf(Object obj) {
        switch (bFf().mo6893i(gFW)) {
            case 0:
                return 0;
            case 2:
                return ((Integer) bFf().mo5606d(new aCE(this, gFW, new Object[]{obj}))).intValue();
            case 3:
                bFf().mo5606d(new aCE(this, gFW, new Object[]{obj}));
                break;
        }
        return m26628aw(obj);
    }

    public boolean isEmpty() {
        switch (bFf().mo6893i(bqB)) {
            case 0:
                return false;
            case 2:
                return ((Boolean) bFf().mo5606d(new aCE(this, bqB, new Object[0]))).booleanValue();
            case 3:
                bFf().mo5606d(new aCE(this, bqB, new Object[0]));
                break;
        }
        return ahi();
    }

    public Iterator<T> iterator() {
        switch (bFf().mo6893i(dxI)) {
            case 0:
                return null;
            case 2:
                return (Iterator) bFf().mo5606d(new aCE(this, dxI, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, dxI, new Object[0]));
                break;
        }
        return bgf();
    }

    public int lastIndexOf(Object obj) {
        switch (bFf().mo6893i(gFX)) {
            case 0:
                return 0;
            case 2:
                return ((Integer) bFf().mo5606d(new aCE(this, gFX, new Object[]{obj}))).intValue();
            case 3:
                bFf().mo5606d(new aCE(this, gFX, new Object[]{obj}));
                break;
        }
        return m26629ax(obj);
    }

    public ListIterator<T> listIterator() {
        switch (bFf().mo6893i(gFY)) {
            case 0:
                return null;
            case 2:
                return (ListIterator) bFf().mo5606d(new aCE(this, gFY, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, gFY, new Object[0]));
                break;
        }
        return cyC();
    }

    public ListIterator<T> listIterator(int i) {
        switch (bFf().mo6893i(gFZ)) {
            case 0:
                return null;
            case 2:
                return (ListIterator) bFf().mo5606d(new aCE(this, gFZ, new Object[]{new Integer(i)}));
            case 3:
                bFf().mo5606d(new aCE(this, gFZ, new Object[]{new Integer(i)}));
                break;
        }
        return m26640uO(i);
    }

    public T remove(int i) {
        switch (bFf().mo6893i(gGa)) {
            case 0:
                return null;
            case 2:
                return bFf().mo5606d(new aCE(this, gGa, new Object[]{new Integer(i)}));
            case 3:
                bFf().mo5606d(new aCE(this, gGa, new Object[]{new Integer(i)}));
                break;
        }
        return m26641uP(i);
    }

    public boolean remove(Object obj) {
        switch (bFf().mo6893i(cbl)) {
            case 0:
                return false;
            case 2:
                return ((Boolean) bFf().mo5606d(new aCE(this, cbl, new Object[]{obj}))).booleanValue();
            case 3:
                bFf().mo5606d(new aCE(this, cbl, new Object[]{obj}));
                break;
        }
        return m26621J(obj);
    }

    public boolean removeAll(Collection<?> collection) {
        switch (bFf().mo6893i(dxJ)) {
            case 0:
                return false;
            case 2:
                return ((Boolean) bFf().mo5606d(new aCE(this, dxJ, new Object[]{collection}))).booleanValue();
            case 3:
                bFf().mo5606d(new aCE(this, dxJ, new Object[]{collection}));
                break;
        }
        return m26636o(collection);
    }

    public boolean retainAll(Collection<?> collection) {
        switch (bFf().mo6893i(dxK)) {
            case 0:
                return false;
            case 2:
                return ((Boolean) bFf().mo5606d(new aCE(this, dxK, new Object[]{collection}))).booleanValue();
            case 3:
                bFf().mo5606d(new aCE(this, dxK, new Object[]{collection}));
                break;
        }
        return m26638p(collection);
    }

    public T set(int i, T t) {
        switch (bFf().mo6893i(gGb)) {
            case 0:
                return null;
            case 2:
                return bFf().mo5606d(new aCE(this, gGb, new Object[]{new Integer(i), t}));
            case 3:
                bFf().mo5606d(new aCE(this, gGb, new Object[]{new Integer(i), t}));
                break;
        }
        return m26631f(i, t);
    }

    public int size() {
        switch (bFf().mo6893i(aJH)) {
            case 0:
                return 0;
            case 2:
                return ((Integer) bFf().mo5606d(new aCE(this, aJH, new Object[0]))).intValue();
            case 3:
                bFf().mo5606d(new aCE(this, aJH, new Object[0]));
                break;
        }
        return m26622QD();
    }

    public List<T> subList(int i, int i2) {
        switch (bFf().mo6893i(gGc)) {
            case 0:
                return null;
            case 2:
                return (List) bFf().mo5606d(new aCE(this, gGc, new Object[]{new Integer(i), new Integer(i2)}));
            case 3:
                bFf().mo5606d(new aCE(this, gGc, new Object[]{new Integer(i), new Integer(i2)}));
                break;
        }
        return m26627ah(i, i2);
    }

    public Object[] toArray() {
        switch (bFf().mo6893i(dxL)) {
            case 0:
                return null;
            case 2:
                return (Object[]) bFf().mo5606d(new aCE(this, dxL, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, dxL, new Object[0]));
                break;
        }
        return bgg();
    }

    public <T> T[] toArray(T[] tArr) {
        switch (bFf().mo6893i(dxM)) {
            case 0:
                return null;
            case 2:
                return (Object[]) bFf().mo5606d(new aCE(this, dxM, new Object[]{tArr}));
            case 3:
                bFf().mo5606d(new aCE(this, dxM, new Object[]{tArr}));
                break;
        }
        return m26635n(tArr);
    }

    /* renamed from: S */
    public void mo10S() {
        super.mo10S();
    }

    @C0064Am(aul = "e4f4772c2014384e149ffc5b9283302c", aum = 0)
    /* renamed from: U */
    private boolean m26623U(T t) {
        return m26632jP().add(t);
    }

    @C0064Am(aul = "a1c17fe680097207b768306d61bfd85e", aum = 0)
    /* renamed from: e */
    private void m26630e(int i, T t) {
        m26632jP().add(i, t);
    }

    @C0064Am(aul = "ed7597e68cd607c1bcbc1080d494c036", aum = 0)
    /* renamed from: m */
    private boolean m26633m(Collection<? extends T> collection) {
        return m26632jP().addAll(collection);
    }

    @C0064Am(aul = "01c4a3f4b8f921a2fcf32a19a25d8a72", aum = 0)
    /* renamed from: a */
    private boolean m26626a(int i, Collection<? extends T> collection) {
        return m26632jP().addAll(i, collection);
    }

    @C0064Am(aul = "686c51014d04d9b18102111e71f5a8c7", aum = 0)
    private void asC() {
        m26632jP().clear();
    }

    @C0064Am(aul = "d94c25b818bf1c6e25483a5ddda56628", aum = 0)
    /* renamed from: V */
    private boolean m26625V(Object obj) {
        return m26632jP().contains(obj);
    }

    @C0064Am(aul = "888bbb1a6ff4711817912e632b69e4c7", aum = 0)
    /* renamed from: n */
    private boolean m26634n(Collection<?> collection) {
        return m26632jP().containsAll(collection);
    }

    @C0064Am(aul = "1d7905be8614dfe7a0c54d12edceafe9", aum = 0)
    /* renamed from: uN */
    private T m26639uN(int i) {
        return m26632jP().get(i);
    }

    @C0064Am(aul = "165e917b7642b3364983ed51926db1b9", aum = 0)
    /* renamed from: aw */
    private int m26628aw(Object obj) {
        return m26632jP().indexOf(obj);
    }

    @C0064Am(aul = "625bef537d75b6c944335ac2f3a8235a", aum = 0)
    private boolean ahi() {
        return m26632jP().isEmpty();
    }

    @C0064Am(aul = "2e7c15a8931f8cbc7b0eda2349a33ecf", aum = 0)
    private Iterator<T> bgf() {
        return m26632jP().iterator();
    }

    @C0064Am(aul = "41d6776e4d0f89c0e9373d96d6345841", aum = 0)
    /* renamed from: ax */
    private int m26629ax(Object obj) {
        return m26632jP().lastIndexOf(obj);
    }

    @C0064Am(aul = "32a280370f5eb5254e333245f40b59fb", aum = 0)
    private ListIterator<T> cyC() {
        return m26632jP().listIterator();
    }

    @C0064Am(aul = "babbef7e82b45a5f3561fa0e768b992d", aum = 0)
    /* renamed from: uO */
    private ListIterator<T> m26640uO(int i) {
        return m26632jP().listIterator(i);
    }

    @C0064Am(aul = "1bf68d151ffc2e2a5c8da95aedb2d28c", aum = 0)
    /* renamed from: J */
    private boolean m26621J(Object obj) {
        return m26632jP().remove(obj);
    }

    @C0064Am(aul = "14e8781210f501892dedf816678acaeb", aum = 0)
    /* renamed from: uP */
    private T m26641uP(int i) {
        return m26632jP().remove(i);
    }

    @C0064Am(aul = "1db4037085887d682f3fe8f781e35b2d", aum = 0)
    /* renamed from: o */
    private boolean m26636o(Collection<?> collection) {
        return m26632jP().removeAll(collection);
    }

    @C0064Am(aul = "9fec2b3345d84fedf617c1b12894b7c0", aum = 0)
    /* renamed from: p */
    private boolean m26638p(Collection<?> collection) {
        return m26632jP().retainAll(collection);
    }

    @C0064Am(aul = "daf28719dbfcc82916107bea1898a25e", aum = 0)
    /* renamed from: f */
    private T m26631f(int i, T t) {
        return m26632jP().set(i, t);
    }

    @C0064Am(aul = "f1b91bc02ca3cfec6a7306e983346cbf", aum = 0)
    /* renamed from: QD */
    private int m26622QD() {
        return m26632jP().size();
    }

    @C0064Am(aul = "16f39cb811a0a5e6ddf217919b626821", aum = 0)
    /* renamed from: ah */
    private List<T> m26627ah(int i, int i2) {
        return m26632jP().subList(i, i2);
    }

    @C0064Am(aul = "abf15856f6a7abac40bc3ca36a4a2973", aum = 0)
    private Object[] bgg() {
        return m26632jP().toArray();
    }

    @C0064Am(aul = "68f973c94cd6c92bc7cf9e5294fbe26b", aum = 0)
    /* renamed from: n */
    private <T> T[] m26635n(T[] tArr) {
        return m26632jP().toArray(tArr);
    }

    @C0064Am(aul = "3a63e37208b65007d69957d757d42c1e", aum = 0)
    private void cyD() {
        super.delete();
    }
}
