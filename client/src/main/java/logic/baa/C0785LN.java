package logic.baa;

import game.network.message.externalizable.aCE;
import game.network.message.serializable.C3438ra;
import logic.data.mbean.C6647aqv;
import logic.res.code.C5640aRc;
import logic.res.code.C5663aRz;
import logic.res.code.C6494any;
import logic.res.html.C2491fm;
import p001a.*;

import java.util.Collection;
import java.util.Iterator;
import java.util.Set;

@C5511aMd
@C6485anp
/* renamed from: a.LN */
/* compiled from: a */
public class C0785LN<T> extends aDJ implements Set<T>, Set {
    public static final int _m_fieldCount = 0;
    public static final C5663aRz[] _m_fields = null;
    public static final int _m_methodCount = 0;
    public static final C2491fm[] _m_methods = null;
    public static final C2491fm aJH = null;
    public static final C2491fm bqB = null;
    public static final C2491fm cbl = null;
    public static final C2491fm cbm = null;
    public static final C5663aRz dxD = null;
    public static final C2491fm dxE = null;
    public static final C2491fm dxF = null;
    public static final C2491fm dxG = null;
    public static final C2491fm dxH = null;
    public static final C2491fm dxI = null;
    public static final C2491fm dxJ = null;
    public static final C2491fm dxK = null;
    public static final C2491fm dxL = null;
    public static final C2491fm dxM = null;
    public static final long serialVersionUID = 0;
    public static C6494any ___iScriptClass;
    @C0064Am(aul = "4d41cef55632ced25f8cbeb5dcad9a35", aum = 0)
    @C5566aOg
    private static C3438ra<T> dxC;

    static {
        m6717V();
    }

    public C0785LN() {
        super((C5540aNg) null);
        super.mo10S();
    }

    public C0785LN(C5540aNg ang) {
        super(ang);
    }

    /* renamed from: V */
    static void m6717V() {
        serialVersionUID = (long) 1;
        _m_fieldCount = aDJ._m_fieldCount + 1;
        _m_methodCount = aDJ._m_methodCount + 13;
        int i = aDJ._m_fieldCount;
        C5663aRz[] arzArr = new C5663aRz[(i + 1)];
        C5663aRz b = C5640aRc.m17844b(C0785LN.class, "4d41cef55632ced25f8cbeb5dcad9a35", i);
        dxD = b;
        arzArr[i] = b;
        int i2 = i + 1;
        _m_fields = arzArr;
        C1634Xv.m11725a((Object[]) aDJ._m_fields, (Object[]) _m_fields);
        int i3 = aDJ._m_methodCount;
        C2491fm[] fmVarArr = new C2491fm[(i3 + 13)];
        C2491fm a = C4105zY.m41624a(C0785LN.class, "3965939a75f766d095a540737841d7a3", i3);
        dxE = a;
        fmVarArr[i3] = a;
        int i4 = i3 + 1;
        C2491fm a2 = C4105zY.m41624a(C0785LN.class, "65d733001533294b8a6ebb54ec756c95", i4);
        dxF = a2;
        fmVarArr[i4] = a2;
        int i5 = i4 + 1;
        C2491fm a3 = C4105zY.m41624a(C0785LN.class, "166c78308234a420ecf2b9a1587e05f2", i5);
        cbm = a3;
        fmVarArr[i5] = a3;
        int i6 = i5 + 1;
        C2491fm a4 = C4105zY.m41624a(C0785LN.class, "54fb110de53a0c66952399d53cbe382b", i6);
        dxG = a4;
        fmVarArr[i6] = a4;
        int i7 = i6 + 1;
        C2491fm a5 = C4105zY.m41624a(C0785LN.class, "d4896671219e02f756d87993a759c008", i7);
        dxH = a5;
        fmVarArr[i7] = a5;
        int i8 = i7 + 1;
        C2491fm a6 = C4105zY.m41624a(C0785LN.class, "4dc52ab65bb9534e898729abdc575951", i8);
        bqB = a6;
        fmVarArr[i8] = a6;
        int i9 = i8 + 1;
        C2491fm a7 = C4105zY.m41624a(C0785LN.class, "5e9ef6d11fb861d563cc926f37caf666", i9);
        dxI = a7;
        fmVarArr[i9] = a7;
        int i10 = i9 + 1;
        C2491fm a8 = C4105zY.m41624a(C0785LN.class, "be4cb04bc3959942e93459a1a77499df", i10);
        cbl = a8;
        fmVarArr[i10] = a8;
        int i11 = i10 + 1;
        C2491fm a9 = C4105zY.m41624a(C0785LN.class, "32e1bd00c50473bcde20ae6a733cb657", i11);
        dxJ = a9;
        fmVarArr[i11] = a9;
        int i12 = i11 + 1;
        C2491fm a10 = C4105zY.m41624a(C0785LN.class, "b5d47b49809698c2180721e6a99bc95c", i12);
        dxK = a10;
        fmVarArr[i12] = a10;
        int i13 = i12 + 1;
        C2491fm a11 = C4105zY.m41624a(C0785LN.class, "9fe012711367cba3f6d69b070d4d30a5", i13);
        aJH = a11;
        fmVarArr[i13] = a11;
        int i14 = i13 + 1;
        C2491fm a12 = C4105zY.m41624a(C0785LN.class, "c79e989a8b32976241e1cf2f32aa2ca3", i14);
        dxL = a12;
        fmVarArr[i14] = a12;
        int i15 = i14 + 1;
        C2491fm a13 = C4105zY.m41624a(C0785LN.class, "c24dfac89d2867e399737c40279c0ce1", i15);
        dxM = a13;
        fmVarArr[i15] = a13;
        int i16 = i15 + 1;
        _m_methods = fmVarArr;
        C1634Xv.m11725a((Object[]) aDJ._m_methods, (Object[]) _m_methods);
        ___iScriptClass = aUO.m18566a(C0785LN.class, C6647aqv.class, _m_fields, _m_methods);
    }

    private C3438ra bge() {
        return (C3438ra) bFf().mo5608dq().mo3214p(dxD);
    }

    /* renamed from: bh */
    private void m6719bh(C3438ra raVar) {
        bFf().mo5608dq().mo3197f(dxD, raVar);
    }

    /* renamed from: T */
    public C6494any mo11T() {
        return ___iScriptClass;
    }

    /* renamed from: U */
    public C2491fm[] mo12U() {
        return _m_methods;
    }

    /* renamed from: W */
    public Object mo13W() {
        return new C6647aqv(this);
    }

    /* renamed from: a */
    public Object mo14a(C0495Gr gr) {
        Object[] args = gr.getArgs();
        switch (gr.mo2417hq() - aDJ._m_methodCount) {
            case 0:
                return new Boolean(m6716U(args[0]));
            case 1:
                return new Boolean(m6720m((Collection) args[0]));
            case 2:
                asC();
                return null;
            case 3:
                return new Boolean(m6718V(args[0]));
            case 4:
                return new Boolean(m6721n((Collection<?>) (Collection) args[0]));
            case 5:
                return new Boolean(ahi());
            case 6:
                return bgf();
            case 7:
                return new Boolean(m6714J(args[0]));
            case 8:
                return new Boolean(m6723o((Collection) args[0]));
            case 9:
                return new Boolean(m6724p((Collection) args[0]));
            case 10:
                return new Integer(m6715QD());
            case 11:
                return bgg();
            case 12:
                return m6722n((T[]) (Object[]) args[0]);
            default:
                return super.mo14a(gr);
        }
    }

    /* renamed from: a */
    public void mo15a(Collection collection, C0495Gr gr) {
        super.mo15a(collection, gr);
    }

    public boolean add(T t) {
        switch (bFf().mo6893i(dxE)) {
            case 0:
                return false;
            case 2:
                return ((Boolean) bFf().mo5606d(new aCE(this, dxE, new Object[]{t}))).booleanValue();
            case 3:
                bFf().mo5606d(new aCE(this, dxE, new Object[]{t}));
                break;
        }
        return m6716U(t);
    }

    public boolean addAll(Collection<? extends T> collection) {
        switch (bFf().mo6893i(dxF)) {
            case 0:
                return false;
            case 2:
                return ((Boolean) bFf().mo5606d(new aCE(this, dxF, new Object[]{collection}))).booleanValue();
            case 3:
                bFf().mo5606d(new aCE(this, dxF, new Object[]{collection}));
                break;
        }
        return m6720m(collection);
    }

    /* renamed from: c */
    public C5663aRz[] mo25c() {
        return _m_fields;
    }

    public void clear() {
        switch (bFf().mo6893i(cbm)) {
            case 0:
                return;
            case 2:
                bFf().mo5606d(new aCE(this, cbm, new Object[0]));
                return;
            case 3:
                bFf().mo5606d(new aCE(this, cbm, new Object[0]));
                break;
        }
        asC();
    }

    public boolean contains(Object obj) {
        switch (bFf().mo6893i(dxG)) {
            case 0:
                return false;
            case 2:
                return ((Boolean) bFf().mo5606d(new aCE(this, dxG, new Object[]{obj}))).booleanValue();
            case 3:
                bFf().mo5606d(new aCE(this, dxG, new Object[]{obj}));
                break;
        }
        return m6718V(obj);
    }

    public boolean containsAll(Collection<?> collection) {
        switch (bFf().mo6893i(dxH)) {
            case 0:
                return false;
            case 2:
                return ((Boolean) bFf().mo5606d(new aCE(this, dxH, new Object[]{collection}))).booleanValue();
            case 3:
                bFf().mo5606d(new aCE(this, dxH, new Object[]{collection}));
                break;
        }
        return m6721n(collection);
    }

    public boolean isEmpty() {
        switch (bFf().mo6893i(bqB)) {
            case 0:
                return false;
            case 2:
                return ((Boolean) bFf().mo5606d(new aCE(this, bqB, new Object[0]))).booleanValue();
            case 3:
                bFf().mo5606d(new aCE(this, bqB, new Object[0]));
                break;
        }
        return ahi();
    }

    public Iterator<T> iterator() {
        switch (bFf().mo6893i(dxI)) {
            case 0:
                return null;
            case 2:
                return (Iterator) bFf().mo5606d(new aCE(this, dxI, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, dxI, new Object[0]));
                break;
        }
        return bgf();
    }

    public boolean remove(Object obj) {
        switch (bFf().mo6893i(cbl)) {
            case 0:
                return false;
            case 2:
                return ((Boolean) bFf().mo5606d(new aCE(this, cbl, new Object[]{obj}))).booleanValue();
            case 3:
                bFf().mo5606d(new aCE(this, cbl, new Object[]{obj}));
                break;
        }
        return m6714J(obj);
    }

    public boolean removeAll(Collection<?> collection) {
        switch (bFf().mo6893i(dxJ)) {
            case 0:
                return false;
            case 2:
                return ((Boolean) bFf().mo5606d(new aCE(this, dxJ, new Object[]{collection}))).booleanValue();
            case 3:
                bFf().mo5606d(new aCE(this, dxJ, new Object[]{collection}));
                break;
        }
        return m6723o(collection);
    }

    public boolean retainAll(Collection<?> collection) {
        switch (bFf().mo6893i(dxK)) {
            case 0:
                return false;
            case 2:
                return ((Boolean) bFf().mo5606d(new aCE(this, dxK, new Object[]{collection}))).booleanValue();
            case 3:
                bFf().mo5606d(new aCE(this, dxK, new Object[]{collection}));
                break;
        }
        return m6724p(collection);
    }

    public int size() {
        switch (bFf().mo6893i(aJH)) {
            case 0:
                return 0;
            case 2:
                return ((Integer) bFf().mo5606d(new aCE(this, aJH, new Object[0]))).intValue();
            case 3:
                bFf().mo5606d(new aCE(this, aJH, new Object[0]));
                break;
        }
        return m6715QD();
    }

    public Object[] toArray() {
        switch (bFf().mo6893i(dxL)) {
            case 0:
                return null;
            case 2:
                return (Object[]) bFf().mo5606d(new aCE(this, dxL, new Object[0]));
            case 3:
                bFf().mo5606d(new aCE(this, dxL, new Object[0]));
                break;
        }
        return bgg();
    }

    public <T> T[] toArray(T[] tArr) {
        switch (bFf().mo6893i(dxM)) {
            case 0:
                return null;
            case 2:
                return (Object[]) bFf().mo5606d(new aCE(this, dxM, new Object[]{tArr}));
            case 3:
                bFf().mo5606d(new aCE(this, dxM, new Object[]{tArr}));
                break;
        }
        return m6722n(tArr);
    }

    /* renamed from: S */
    public void mo10S() {
        super.mo10S();
    }

    @C0064Am(aul = "3965939a75f766d095a540737841d7a3", aum = 0)
    /* renamed from: U */
    private boolean m6716U(T t) {
        return bge().add(t);
    }

    @C0064Am(aul = "65d733001533294b8a6ebb54ec756c95", aum = 0)
    /* renamed from: m */
    private boolean m6720m(Collection<? extends T> collection) {
        return bge().addAll(collection);
    }

    @C0064Am(aul = "166c78308234a420ecf2b9a1587e05f2", aum = 0)
    private void asC() {
        bge().clear();
    }

    @C0064Am(aul = "54fb110de53a0c66952399d53cbe382b", aum = 0)
    /* renamed from: V */
    private boolean m6718V(Object obj) {
        return bge().contains(obj);
    }

    @C0064Am(aul = "d4896671219e02f756d87993a759c008", aum = 0)
    /* renamed from: n */
    private boolean m6721n(Collection<?> collection) {
        return bge().containsAll(collection);
    }

    @C0064Am(aul = "4dc52ab65bb9534e898729abdc575951", aum = 0)
    private boolean ahi() {
        return bge().isEmpty();
    }

    @C0064Am(aul = "5e9ef6d11fb861d563cc926f37caf666", aum = 0)
    private Iterator<T> bgf() {
        return bge().iterator();
    }

    @C0064Am(aul = "be4cb04bc3959942e93459a1a77499df", aum = 0)
    /* renamed from: J */
    private boolean m6714J(Object obj) {
        return bge().remove(obj);
    }

    @C0064Am(aul = "32e1bd00c50473bcde20ae6a733cb657", aum = 0)
    /* renamed from: o */
    private boolean m6723o(Collection<?> collection) {
        return bge().removeAll(collection);
    }

    @C0064Am(aul = "b5d47b49809698c2180721e6a99bc95c", aum = 0)
    /* renamed from: p */
    private boolean m6724p(Collection<?> collection) {
        return bge().retainAll(collection);
    }

    @C0064Am(aul = "9fe012711367cba3f6d69b070d4d30a5", aum = 0)
    /* renamed from: QD */
    private int m6715QD() {
        return bge().size();
    }

    @C0064Am(aul = "c79e989a8b32976241e1cf2f32aa2ca3", aum = 0)
    private Object[] bgg() {
        return bge().toArray();
    }

    @C0064Am(aul = "c24dfac89d2867e399737c40279c0ce1", aum = 0)
    /* renamed from: n */
    private <T> T[] m6722n(T[] tArr) {
        return bge().toArray(tArr);
    }
}
