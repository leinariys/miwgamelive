package logic.metric;

import org.mozilla1.classfile.C0147Bi;
import p001a.C3819vP;
import p001a.C4077yz;
import p001a.C6445anB;

import java.io.*;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

/* renamed from: a.aHh  reason: case insensitive filesystem */
/* compiled from: a */
public class GoogleAdservicesNetWeb {
    private static final String addr_www = "www.googleadservices.com";
    private static final String conversion_name = "Conversion";
    private static final String user_agent = "Mozilla/5.0 (Windows; U; Windows NT 5.1; pt-BR; rv:1.9.0.1) Gecko/2008070208 Firefox/3.0.1";
    private static final String google_conversion_id = "1052789991";
    private static final String google_conversion_label = "V2OoCM-iYRDnmYH2Aw";
    private List<C4077yz> hWa = new ArrayList();
    private String hWb;
    private String hWc;
    private String hWd;
    private String hWe;
    private C4077yz hWf;

    public GoogleAdservicesNetWeb(String str) {
        this.hWb = str;
        this.hWc = google_conversion_label;
        dcQ();
        dcP();
    }

    public static void main(String[] strArr) {
        try {
            new GoogleAdservicesNetWeb(google_conversion_id).stertRequest(google_conversion_label);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /* renamed from: mH */
    public void stertRequest(String str) throws IOException {
        setAction(str);
        if (this.hWf == null) {
            dcR();
        }
        send();
    }

    /* renamed from: mI */
    public void mo9254mI(String str) {
        this.hWb = str;
        dcQ();
        dcP();
    }

    public void setAction(String str) {
        this.hWc = str;
        dcQ();
        dcP();
    }

    private void dcP() {
        this.hWe = "http://www.googleadservices.com/pagead/conversion/" + this.hWb + "/?label=" + this.hWc + "&game.script=0";
    }

    private void dcQ() {
        this.hWd = "/pagead/conversion/" + this.hWb + C0147Bi.SEPARATOR;
    }

    private void send() throws IOException {
        HttpURLConnection httpURLConnection = (HttpURLConnection) new URL(this.hWe).openConnection();
        httpURLConnection.setRequestProperty("User-Agent", user_agent);
        httpURLConnection.setRequestProperty("Accept", "text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8");
        httpURLConnection.setRequestProperty("Accept-Language", "pt-br,pt;q=0.8,en-us;q=0.5,en;q=0.3");
        httpURLConnection.setRequestProperty("Accept-Encoding", "gzip,deflate");
        httpURLConnection.setRequestProperty("Accept-Charset", "ISO-8859-1,utf-8;q=0.7,*;q=0.7");
        httpURLConnection.setRequestProperty("Keep-Alive", "300");
        httpURLConnection.setRequestProperty("If-Modified-Since", "Mon, 25 Apr 2005 20:57:44 GMT");
        if (this.hWf != null) {
            httpURLConnection.setRequestProperty("Cookie", String.valueOf(this.hWf.getName()) + "=" + this.hWf.getValue());
        }
        InputStream inputStream = httpURLConnection.getInputStream();
        File createTempFile = File.createTempFile("tkdrequest", ".gif");
        if (createTempFile.canWrite()) {
            FileOutputStream fileOutputStream = new FileOutputStream(createTempFile);
            byte[] bArr = new byte[10240];
            while (true) {
                int read = inputStream.read(bArr);
                if (read <= 0) {
                    break;
                }
                fileOutputStream.write(bArr, 0, read);
            }
            fileOutputStream.close();
            if (!new BufferedReader(new FileReader(createTempFile)).readLine().startsWith("GIF89a")) {
                createTempFile.delete();
                throw new IOException("Request did not return a GIF file.");
            } else {
                createTempFile.delete();
            }
        } else {
            throw new IOException("Can not store temporary file.");
        }
    }

    public void dcR() {
        dcS();
        dcT();
        this.hWf = dcU();
    }

    private void dcS() {
        List<C4077yz> n = C6445anB.m24036n(addr_www, this.hWd, conversion_name);
        if (n != null) {
            this.hWa.addAll(n);
        }
    }

    private void dcT() {
        C4077yz l = C3819vP.m40251l(addr_www, this.hWd, conversion_name);
        if (l != null) {
            this.hWa.add(l);
        }
    }

    private C4077yz dcU() {
        C4077yz yzVar = null;
        for (C4077yz next : this.hWa) {
            if (yzVar == null) {
                yzVar = next;
            } else if (next.apY() == 0) {
                return next;
            } else {
                if (yzVar.apY() < next.apY()) {
                    yzVar = next;
                }
            }
        }
        return yzVar;
    }
}
