package logic;

import game.engine.C0507HC;
import game.engine.C3658tN;
import game.engine.DataGameEvent;
import game.engine.IEngineGame;
import game.script.PlayerController;
import game.script.Taikodom;
import game.script.login.UserConnection;
import game.script.player.Player;
import game.script.player.User;
import logic.render.GUIModule;
import logic.render.IEngineGraphics;
import logic.res.ConfigManager;
import logic.res.ILoaderTrail;
import logic.res.LoaderTrail;
import logic.res.code.SoftTimer;
import p001a.C6596apw;
import taikodom.render.loader.provider.FilePath;

/* renamed from: a.Mr */
/* compiled from: a */
public class C0886Mr implements IEngineGame {
    private final SoftTimer dej;
    private C6245ajJ del = new EventManager();
    private UserConnection dzV;
    private C6596apw dzW = new C6596apw();
    private LoaderTrail dzX;
    private FilePath dzY;

    /* renamed from: ng */
    private FilePath f1152ng;

    public C0886Mr(LoaderTrail glVar, SoftTimer abg, FilePath ain, FilePath ain2) {
        this.dzX = glVar;
        this.dej = abg;
        this.dzY = ain;
        this.f1152ng = ain2;
    }

    /* renamed from: eK */
    public boolean mo4092eK(String str) {
        throw new UnsupportedOperationException();
    }

    public C3658tN bgX() {
        throw new UnsupportedOperationException();
    }

    public void cleanUp() {
        throw new UnsupportedOperationException();
    }

    public void bgY() {
        throw new UnsupportedOperationException();
    }

    public void bgZ() {
        throw new UnsupportedOperationException();
    }

    public void disconnect() {
        throw new UnsupportedOperationException();
    }

    public void bha() {
        throw new UnsupportedOperationException();
    }

    public void exit() {
        throw new UnsupportedOperationException();
    }

    /* renamed from: y */
    public <T> T mo4095y(Class<T> cls) {
        throw new UnsupportedOperationException();
    }

    public ConfigManager getConfigManager() {
        throw new UnsupportedOperationException();
    }

    public DataGameEvent bhc() {
        throw new UnsupportedOperationException();
    }

    public GUIModule getGuiModule() {
        throw new UnsupportedOperationException();
    }

    /* renamed from: dL */
    public Player mo4089dL() {
        return this.dzV.mo15388dL();
    }

    public PlayerController alb() {
        return mo4089dL().dxc();
    }

    public IEngineGraphics getEngineGraphics() {
        return null;
    }

    public ILoaderTrail getLoaderTrail() {
        return this.dzX;
    }

    public Taikodom getTaikodom() {
        throw new UnsupportedOperationException();
    }

    public SoftTimer alf() {
        return this.dej;
    }

    public User getUser() {
        return this.dzV.getUser();
    }

    public boolean bhf() {
        return true;
    }

    public boolean bhg() {
        throw new UnsupportedOperationException();
    }

    public boolean isVerbose() {
        throw new UnsupportedOperationException();
    }

    public C6245ajJ getEventManager() {
        return this.del;
    }

    /* renamed from: a */
    public void mo4065a(ILoaderTrail qu) {
        throw new UnsupportedOperationException();
    }

    /* renamed from: a */
    public void mo4066a(SoftTimer abg) {
        throw new UnsupportedOperationException();
    }

    public void initAllAddonWaitPlayer() {
        throw new UnsupportedOperationException();
    }

    /* renamed from: c */
    public void mo4087c(UserConnection apk) {
        this.dzV = apk;
    }

    public C0507HC bhh() {
        return this.dzW;
    }

    public FilePath getRootPathRender() {
        return this.f1152ng;
    }

    public FilePath getRootPath() {
        return this.dzY;
    }

    /* renamed from: e */
    public void mo4091e(Runnable runnable) {
        throw new IllegalAccessError("Illegal at bot client?");
    }
}
