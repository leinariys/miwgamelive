package logic;

import java.io.FileNotFoundException;
import java.io.InputStream;
import java.io.PrintStream;

/**
 * class pp Чтение настроек аддонов
 */
/* renamed from: a.aVf  reason: case insensitive filesystem */
/* compiled from: a */
public interface IAddonSettings {
    /* renamed from: P */
    void mo2317P(Object obj);

    /* renamed from: Q */
    void mo2318Q(Object obj);

    /* renamed from: U */
    <T> T mo11830U(Class<T> cls);

    /* renamed from: U */
    String mo11831U(String str, String str2);

    /* renamed from: V */
    String mo11832V(String str, String str2);

    /* renamed from: a */
    aPE mo11833a(long j, aPE ape);

    /* renamed from: a */
    String mo2320a(Class<?> cls, String str, String str2, Object... objArr);

    /* renamed from: a */
    String mo11834a(Class<?> cls, String str, Object... objArr);

    /* renamed from: a */
    String mo11835a(String str, String str2, Object... objArr);

    /* renamed from: a */
    void mo11836a(String str, C6307akT akt);

    C6245ajJ aVU();

    IAddonManager bHu();

    C6961axa bHv();

    PrintStream bHw();

    Object bHx();

    <T extends IAddonSettings> IAddonExecutor<T> bHy();

    IWrapFileXmlOrJar bHz();

    /* renamed from: c */
    void mo11844c(String str, String... strArr);

    void dispose();

    /* renamed from: e */
    void mo11845e(String str, String str2, String str3);

    /* renamed from: f */
    InputStream mo11846f(Object obj, String str) throws FileNotFoundException;

    /* renamed from: hE */
    InputStream mo11847hE(String str) throws FileNotFoundException;

    void restart();

    String translate(String str);

    String translate(String str, Object... objArr);
}
