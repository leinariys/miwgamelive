package logic.abb;

import com.hoplon.geometry.Vec3f;
import game.network.message.serializable.C1216Rw;
import game.script.Actor;
import game.script.item.Shot;
import game.script.ship.Ship;

/* renamed from: a.aV */
/* compiled from: a */
public class C1829aV extends C6659arH {

    /* renamed from: lN */
    private static final float f3885lN = 4.0f;

    /* renamed from: lO */
    private static final float f3886lO = 1.0f;

    /* renamed from: lP */
    private static final float f3887lP = 10000.0f;

    /* renamed from: lQ */
    private Actor f3888lQ;

    /* renamed from: lR */
    private transient float f3889lR = -1.0f;

    /* renamed from: a */
    public boolean mo31a(C1216Rw rw, float f) {
        if (this.f3889lR == -1.0f) {
            if (aYa() instanceof Ship) {
                this.f3889lR = Math.min(((Ship) aYa()).mo958IL().getOuterSphereRadius() * f3885lN, 10000.0f);
            } else {
                System.err.println("AvoidColisionBehaviour - Controlled pawn " + aYa() + " not a ship and trying to avoid obstacles!! FIX ME! ");
            }
        }
        Actor x = mo11763x(this.f3889lR);
        if (x == null || !aYa().aiD().mo2545a(x.aiD())) {
            return false;
        }
        Vec3f an = aYa().mo989an(x.getPosition());
        if (m25382a(an)) {
            an.set(0.0f, 0.0f, -1.0f);
        } else {
            rw.brw().set(an);
            rw.brw().normalize();
        }
        rw.brw().negate();
        rw.brw().scale(((float) Math.sqrt((double) (Math.max(1.0f, this.f3889lR - Math.max(0.0f, (an.lengthSquared() - x.mo958IL().getBoundingBox().diu()) - aYa().mo958IL().getBoundingBox().diu())) / this.f3889lR))) * aYa().mo1091ra() * 1.0f * 2.0f * (1.0f / f));
        rw.mo5308dm(true);
        return true;
    }

    /* renamed from: a */
    public void mo11762a(Actor cr) {
        this.f3888lQ = cr;
    }

    /* renamed from: x */
    public Actor mo11763x(float f) {
        float f2 = Float.MAX_VALUE;
        Actor cr = null;
        for (Actor next : mo15717Mn()) {
            if (next != this.f3888lQ && !(next instanceof Shot)) {
                float length = (aYa().mo989an(next.getPosition()).length() - aYa().mo958IL().getOuterSphereRadius()) - next.mo958IL().getOuterSphereRadius();
                if (length < f && length < f2) {
                    f2 = length;
                    cr = next;
                }
            }
        }
        return cr;
    }
}
