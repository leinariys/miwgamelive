package logic.abb;

import com.hoplon.geometry.Vec3f;
import game.geometry.Quat4fWrap;
import game.network.message.serializable.C1216Rw;
import org.mozilla1.javascript.ScriptRuntime;
import p001a.C3241pX;

import javax.vecmath.Vector3f;

/* renamed from: a.EV */
/* compiled from: a */
public class C0332EV extends C6659arH {
    public final transient Vec3f bDT = new Vec3f(0.0f, 0.0f, -1.0f);

    /* renamed from: a */
    public boolean mo31a(C1216Rw rw, float f) {
        float bas = aPd().bas();
        float baq = aPd().baq() - aPd().bac().clO();
        if (C3241pX.isZero(bas) && C3241pX.isZero(baq)) {
            return false;
        }
        rw.brw().set(Quat4fWrap.m24427c((double) (-baq), (double) (-bas), ScriptRuntime.NaN).mo15209aT(new Vec3f((Vector3f) this.bDT)));
        return true;
    }

    public Turret aPd() {
        return (Turret) aYa();
    }
}
