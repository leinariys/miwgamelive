package logic.abb;

import com.hoplon.geometry.Vec3f;
import game.geometry.Quat4fWrap;
import game.network.message.serializable.C1216Rw;
import game.script.Actor;
import p001a.C6294akG;

import javax.vecmath.Vector3f;

/* renamed from: a.Ar */
/* compiled from: a */
public class C0070Ar extends C6659arH {
    private static final float TOLERANCE = 0.1f;

    /* renamed from: pI */
    private Actor f98pI;

    /* renamed from: a */
    public boolean mo31a(C1216Rw rw, float f) {
        Vec3f vec3f;
        float f2;
        Vec3f aT;
        if (this.f98pI == null || this.f98pI.aiD() == null) {
            return false;
        }
        float ra = aYa().mo1091ra();
        float f3 = (3.0f * ra) / 4.0f;
        float outerSphereRadius = this.f98pI.aiD().mo2437IL().getOuterSphereRadius() + (aYa().aiD().mo2437IL().getOuterSphereRadius() * 1.5f);
        Vec3f an = aYa().mo989an(this.f98pI.aiD().getPosition());
        float length = an.length();
        if (outerSphereRadius > length) {
            Quat4fWrap aoy = new Quat4fWrap(0.0f, 0.7071f, 0.0f, 0.7071f);
            vec3f = an.dfS();
            vec3f.normalize();
            f2 = outerSphereRadius - length;
            vec3f.scale(f2);
            aT = aoy.mo15209aT(vec3f);
            aT.normalize();
        } else {
            Quat4fWrap aoy2 = new Quat4fWrap(0.0f, -0.7071f, 0.0f, 0.7071f);
            vec3f = new Vec3f((Vector3f) an);
            vec3f.normalize();
            f2 = length - outerSphereRadius;
            vec3f.scale(f2);
            aT = aoy2.mo15209aT(vec3f);
            aT.normalize();
        }
        if (f2 >= f3) {
            aT.scale(f3);
        } else {
            aT.scale((float) Math.sqrt((double) ((f3 * f3) - (f2 * f2))));
        }
        Vec3f i = vec3f.mo23503i(aT);
        if (i.lengthSquared() > ra * ra) {
            i.cox();
            i.scale(ra);
        }
        C6294akG.m23128a("orbital-desired", (Actor) aYa(), i);
        mo8603g(i, f);
        rw.brw().set(rw.brw().mo23503i(i));
        return true;
    }

    /* renamed from: am */
    public void mo476am(Actor cr) {
        this.f98pI = cr;
    }
}
