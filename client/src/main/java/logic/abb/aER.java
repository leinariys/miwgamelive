package logic.abb;

import com.hoplon.geometry.Vec3f;
import game.geometry.Vec3d;
import game.network.message.serializable.C1216Rw;
import game.script.ship.Ship;

/* renamed from: a.aER */
/* compiled from: a */
public class aER extends C5697aTh {
    private static final double hHW = Math.toRadians(15.0d);
    private Vec3d hHX = new Vec3d();
    private Vec3d hHY = new Vec3d();
    private Vec3d hHZ = new Vec3d();

    public aER() {
    }

    public aER(Ship fAVar) {
        super(fAVar, false);
    }

    /* renamed from: a */
    public boolean mo31a(C1216Rw rw, float f) {
        float cN;
        initialize();
        Vec3f an = aYa().mo989an(mo5315Vv());
        float lengthSquared = an.lengthSquared();
        rw.mo5311hr(0.0f);
        if (mo4408e(an, lengthSquared)) {
            rw.mo5309dn(true);
            cN = aYa().mo1091ra();
        } else {
            rw.mo5309dn(false);
            cN = mo3565cN(lengthSquared);
        }
        mo8602a(rw.brw(), f, an, cN);
        Pawn aYa = aYa();
        if (aYa instanceof Ship) {
            rw.mo5307dl(m14155aq((Ship) aYa) > hHW);
        }
        if (!mo15720aW(rw.brw())) {
            rw.brw().set(0.0f, 0.0f, 0.0f);
        }
        return true;
    }

    /* renamed from: aq */
    private double m14155aq(Ship fAVar) {
        fAVar.mo1018c(fAVar.aip(), this.hHZ);
        Vec3d position = this.trackedShip.getPosition();
        this.hHX.mo9484aA(fAVar.getPosition());
        this.hHY.mo9484aA(fAVar.getPosition());
        this.hHX.sub(position);
        this.hHX.normalize();
        this.hHY.sub(this.hHZ);
        this.hHY.normalize();
        return this.hHY.angle(this.hHX);
    }

    /* access modifiers changed from: protected */
    /* renamed from: cN */
    public float mo3565cN(float f) {
        float ra;
        boolean z;
        float sqrt = (float) Math.sqrt((double) f);
        float f2 = aYa().mo1090qZ().z;
        float rb = (f2 * f2) / ((aYa().mo1092rb() * 0.5f) * 2.0f);
        float abs = Math.abs(this.trackedShip.mo1090qZ().z);
        if (sqrt > (this.iOq * 3.0f) + rb) {
            ra = aYa().mo1091ra();
        } else if (((double) sqrt) < (((double) this.iOq) * 0.5d) + ((double) rb)) {
            if (!(aYa() instanceof Ship) || m14155aq((Ship) aYa()) <= Math.toRadians(90.0d)) {
                z = false;
            } else {
                z = true;
            }
            if (z) {
                ra = 0.0f;
            } else {
                ra = aYa().mo1091ra() * 0.5f;
            }
        } else if (abs > 70.0f) {
            ra = 0.0f;
        } else if (sqrt > this.iOq) {
            float f3 = sqrt / (this.iOq + rb);
            ra = f3 * aYa().mo1091ra() * f3;
        } else {
            ra = aYa().mo1091ra() * 0.5f;
        }
        if (ra < 1.0f || Float.isNaN(ra) || Float.isInfinite(ra)) {
            ra = 1.0f;
        }
        if (ra > aYa().mo1091ra()) {
            return aYa().mo1091ra();
        }
        return ra;
    }

    /* access modifiers changed from: protected */
    /* renamed from: g */
    public void mo8603g(Vec3f vec3f, float f) {
        super.mo8603g(vec3f, f);
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public void mo8602a(Vec3f vec3f, float f, Vec3f vec3f2, float f2) {
        super.mo8602a(vec3f, f, vec3f2, f2);
    }
}
