package logic.abb;

import com.hoplon.geometry.Vec3f;
import game.network.message.serializable.C1216Rw;
import game.script.Actor;
import game.script.damage.DamageType;
import game.script.item.EnergyWeapon;
import game.script.item.Item;
import game.script.item.Weapon;
import game.script.missile.MissileWeapon;
import game.script.ship.*;

import java.util.Iterator;
import java.util.Map;
import java.util.Set;

/* renamed from: a.akR  reason: case insensitive filesystem */
/* compiled from: a */
public class C6305akR extends C6659arH {
    private static final float fUy = 10.0f;
    private static final float fUz = 4000000.0f;
    private float fUA;
    private boolean fUB;
    private Ship fUC;
    private float fUD;
    private boolean fUE = true;
    private float fUF = -1.0f;
    private Ship trackedShip;

    public C6305akR() {
    }

    public C6305akR(Ship fAVar) {
        this.trackedShip = fAVar;
    }

    public C6305akR(float f, boolean z) {
        mo14327jN(f);
        this.fUB = z;
    }

    /* renamed from: a */
    public boolean mo31a(C1216Rw rw, float f) {
        Weapon adv;
        Ship fAVar;
        Weapon adv2;
        if (!(aYa() instanceof Ship)) {
            return false;
        }
        Ship fAVar2 = (Ship) aYa();
        m23175U(fAVar2);
        this.fUD += f;
        if (!this.fUE) {
            return false;
        }
        if (this.trackedShip != null) {
            fAVar = this.trackedShip;
            adv = m23180a(fAVar2, fAVar, this.fUF) ? m23177W(fAVar) : null;
        } else if (this.fUC == null || !m23180a(fAVar2, this.fUC, this.fUF)) {
            adv = null;
            fAVar = null;
        } else {
            adv = m23177W(this.fUC);
            fAVar = adv != null ? this.fUC : null;
        }
        if (fAVar == null) {
            Iterator<Actor> it = mo15717Mn().iterator();
            adv2 = adv;
            while (true) {
                if (!it.hasNext()) {
                    break;
                }
                Actor next = it.next();
                if (next instanceof Ship) {
                    Ship fAVar3 = (Ship) next;
                    if (m23180a(fAVar2, fAVar3, this.fUF) && (adv2 = m23177W(fAVar3)) != null) {
                        fAVar = fAVar3;
                        break;
                    }
                }
            }
        } else {
            adv2 = adv;
        }
        if (fAVar == null || adv2 == null) {
            rw.mo5308dm(true);
            return true;
        }
        this.fUC = fAVar;
        if (adv2 != fAVar2.afR()) {
            rw.mo5312r(adv2);
        }
        rw.mo5306dk(true);
        rw.mo5296aF(fAVar);
        return true;
    }

    /* renamed from: a */
    private boolean m23180a(Ship fAVar, Ship fAVar2, float f) {
        Vec3f bp;
        if (!mo15722aa(fAVar2)) {
            return false;
        }
        Vec3f an = fAVar.mo989an(fAVar2.getPosition());
        if (an.z > 0.0f) {
            return false;
        }
        if (this.fUB) {
            if (fAVar2.ahO() != null) {
                bp = fAVar.mo1008bp(fAVar2.ahO().mo2472kQ().blq());
            } else {
                bp = fAVar.mo1008bp(fAVar2.cLu());
            }
            an.x += bp.x;
            an.y += bp.y;
            an.z = bp.z + an.z;
            if (an.z > 0.0f) {
                return false;
            }
        }
        if (f < (-an.z)) {
            return false;
        }
        float max = Math.max(0.0f, ((an.x * an.x) + (an.y * an.y)) - fAVar2.mo958IL().getBoundingBox().diu());
        float f2 = (-an.z) * this.fUA;
        if (max > f2 * f2) {
            return false;
        }
        return true;
    }

    /* renamed from: U */
    private void m23175U(Ship fAVar) {
        if (this.fUE && this.fUF == -1.0f) {
            this.fUE = false;
            for (ShipStructure bVG : fAVar.agt()) {
                for (ShipSector iterator : bVG.bVG()) {
                    Iterator<Item> iterator2 = iterator.getIterator();
                    while (iterator2.hasNext()) {
                        Item next = iterator2.next();
                        if (next instanceof Weapon) {
                            Weapon adv = (Weapon) next;
                            if (adv.aqh() > 0) {
                                this.fUE = true;
                                this.fUF = Math.max(this.fUF, adv.mo12940in());
                            } else if (adv instanceof EnergyWeapon) {
                                this.fUE = true;
                            }
                        }
                    }
                }
            }
        }
    }

    /* renamed from: V */
    private Weapon m23176V(Ship fAVar) {
        for (ShipStructure bVG : fAVar.agt()) {
            Iterator it = bVG.bVG().iterator();
            while (true) {
                if (it.hasNext()) {
                    Iterator<Item> iterator = ((ShipSector) it.next()).getIterator();
                    while (true) {
                        if (iterator.hasNext()) {
                            Item next = iterator.next();
                            if (next instanceof Weapon) {
                                Weapon adv = (Weapon) next;
                                if (adv.aqh() > 0) {
                                    return adv;
                                }
                            }
                        }
                    }
                }
            }
        }
        return null;
    }

    /* renamed from: W */
    private Weapon m23177W(Ship fAVar) {
        Ship fAVar2 = (Ship) aYa();
        if (fAVar2.afR() != null && fAVar == this.fUC && this.fUD < fUy) {
            return fAVar2.afR();
        }
        this.fUD = 0.0f;
        return m23179a(fAVar2, fAVar);
    }

    /* renamed from: a */
    private Weapon m23179a(Ship fAVar, Ship fAVar2) {
        fAVar.mo1001bB((Actor) fAVar2);
        for (ShipStructure bVG : fAVar.agt()) {
            Iterator it = bVG.bVG().iterator();
            while (true) {
                if (it.hasNext()) {
                    Iterator<Item> iterator = ((ShipSector) it.next()).getIterator();
                    while (true) {
                        if (iterator.hasNext()) {
                            Item next = iterator.next();
                            if (next instanceof Weapon) {
                                Weapon adv = (Weapon) next;
                                if (adv.aqh() > 0) {
                                    return adv;
                                }
                            }
                        }
                    }
                }
            }
        }
        return null;
    }

    /* renamed from: a */
    private Weapon m23178a(Weapon adv, Weapon adv2, Ship fAVar, float f) {
        float in = adv.mo12940in();
        if (f > in * in) {
            return adv2;
        }
        if (f < fUz && (adv instanceof MissileWeapon) && ((MissileWeapon) adv).ayD() > 1) {
            return adv2;
        }
        Shield zv = fAVar.mo8288zv();
        Set<Map.Entry<DamageType, Float>> entrySet = adv.mo5372il().cPK().entrySet();
        if (!zv.mo19072TD()) {
            float f2 = 0.0f;
            float f3 = 0.0f;
            for (Map.Entry next : entrySet) {
                float d = f3 + zv.mo19086d((DamageType) next.getKey(), ((Float) next.getValue()).floatValue());
                f2 = zv.mo19086d((DamageType) next.getKey(), ((Float) next.getValue()).floatValue()) + f2;
                f3 = d;
            }
            if (f3 > f2) {
                return adv2;
            }
        } else {
            Hull zt = fAVar.mo8287zt();
            float f4 = 0.0f;
            float f5 = 0.0f;
            for (Map.Entry next2 : entrySet) {
                float d2 = f5 + zt.mo19931d((DamageType) next2.getKey(), ((Float) next2.getValue()).floatValue());
                f4 += zt.mo19931d((DamageType) next2.getKey(), ((Float) next2.getValue()).floatValue());
                f5 = d2;
            }
            if (f5 > f4) {
                return adv2;
            }
        }
        return adv;
    }

    public Ship bUT() {
        return this.trackedShip;
    }

    /* renamed from: J */
    public void mo14322J(Ship fAVar) {
        this.trackedShip = fAVar;
    }

    /* renamed from: jN */
    public void mo14327jN(float f) {
        this.fUA = (float) Math.sin(Math.toRadians((double) f));
    }

    public float chq() {
        return this.fUA;
    }

    public boolean chr() {
        return this.fUB;
    }

    /* renamed from: fe */
    public void mo14326fe(boolean z) {
        this.fUB = z;
    }
}
