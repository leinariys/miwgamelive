package logic.abb;

import com.hoplon.geometry.Vec3f;
import game.geometry.Quat4fWrap;
import game.geometry.Vec3d;
import game.network.message.serializable.C6400amI;
import game.script.Actor;
import game.script.ship.Ship;
import org.mozilla1.javascript.ScriptRuntime;
import p001a.C3241pX;
import p001a.C6078afy;

import javax.vecmath.Tuple3d;
import java.util.List;

/* renamed from: a.arH  reason: case insensitive filesystem */
/* compiled from: a */
public abstract class C6659arH implements C0573Hw {
    private StringBuffer debugStr;
    private C6078afy dyT;
    private String prefix;

    /* renamed from: a */
    public static float m25381a(Actor cr, Actor cr2) {
        return (float) Math.max(ScriptRuntime.NaN, (cr.getPosition().mo9517f((Tuple3d) cr2.getPosition()).lengthSquared() - ((double) cr.mo958IL().getBoundingBox().diu())) - ((double) cr2.mo958IL().getBoundingBox().diu()));
    }

    /* renamed from: a */
    public static boolean m25382a(Vec3f vec3f) {
        return C3241pX.isZero(vec3f.x) && C3241pX.isZero(vec3f.y) && C3241pX.isZero(vec3f.z);
    }

    public static boolean isZero(float f) {
        return C3241pX.isZero(f);
    }

    public C6078afy aRF() {
        return this.dyT;
    }

    /* renamed from: a */
    public void mo2697a(C6078afy afy) {
        this.dyT = afy;
    }

    /* access modifiers changed from: protected */
    public Pawn aYa() {
        return aRF().aYa();
    }

    public void start() {
    }

    /* access modifiers changed from: protected */
    /* renamed from: Mn */
    public List<Actor> mo15717Mn() {
        return aRF().mo3301Mn();
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public void mo8602a(Vec3f vec3f, float f, Vec3f vec3f2, float f2) {
        if (isZero(f2)) {
            vec3f.set(0.0f, 0.0f, 0.0f);
        } else {
            if (m25382a(vec3f2)) {
                vec3f.set(0.0f, 0.0f, -1.0f);
            } else {
                vec3f.set(vec3f2);
                vec3f.normalize();
            }
            vec3f.scale(f2);
        }
        mo8603g(vec3f, f);
    }

    /* access modifiers changed from: protected */
    /* renamed from: g */
    public void mo8603g(Vec3f vec3f, float f) {
        Vec3f qZ = aYa().mo1090qZ();
        vec3f.x -= qZ.x;
        vec3f.y -= qZ.y;
        vec3f.z -= qZ.z;
        if (!isZero(f)) {
            vec3f.scale(1.0f / f);
        } else {
            vec3f.scale(8.507059E37f);
        }
    }

    /* renamed from: aa */
    public boolean mo15722aa(Ship fAVar) {
        return aRF().mo3298D(fAVar);
    }

    /* access modifiers changed from: protected */
    /* renamed from: e */
    public Vec3f mo15728e(Actor cr, float f) {
        if (cr == null || cr.aiD() == null || f <= 0.0f) {
            return aYa().mo989an(cr.getPosition());
        }
        C6400amI ami = new C6400amI();
        ((C6400amI) cr.aiD().mo2472kQ()).mo14818b(f, ami);
        return aYa().mo989an(ami.blk());
    }

    /* access modifiers changed from: protected */
    /* renamed from: c */
    public Vec3f mo15725c(Ship fAVar, float f) {
        Vec3f dfQ = fAVar.mo1090qZ().dfQ();
        dfQ.scale(f);
        return Quat4fWrap.m24427c((double) ((fAVar.aSf().x * f) / 2.0f), (double) ((fAVar.aSf().y * f) / 2.0f), (double) ((fAVar.aSf().z * f) / 2.0f)).mo15209aT(dfQ);
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public Vec3f mo15719a(Actor cr, Actor cr2, Vec3f vec3f) {
        return cr.mo989an(cr2.mo1002bB(vec3f));
    }

    /* access modifiers changed from: protected */
    /* renamed from: c */
    public Vec3f mo15724c(Actor cr, Vec3f vec3f) {
        return mo15719a(aYa(), cr, vec3f);
    }

    /* access modifiers changed from: protected */
    /* renamed from: b */
    public Vec3f mo15723b(Actor cr, Actor cr2, Vec3f vec3f) {
        return cr.mo1008bp(cr2.mo1009br(vec3f));
    }

    /* access modifiers changed from: protected */
    /* renamed from: d */
    public Vec3f mo15727d(Actor cr, Vec3f vec3f) {
        return mo15723b(aYa(), cr, vec3f);
    }

    public String aRG() {
        return getClass().getSimpleName();
    }

    /* renamed from: a */
    public void mo2698a(StringBuffer stringBuffer, String str) {
        this.debugStr = stringBuffer;
        this.prefix = str;
    }

    /* access modifiers changed from: protected */
    public boolean cst() {
        return this.debugStr != null;
    }

    /* access modifiers changed from: protected */
    /* renamed from: eI */
    public void mo15729eI(String str) {
        if (this.debugStr != null) {
            this.debugStr.append(this.prefix);
            this.debugStr.append(str);
            this.debugStr.append("\n");
        }
    }

    /* access modifiers changed from: protected */
    /* renamed from: eJ */
    public void mo15730eJ(String str) {
        if (this.debugStr != null) {
            this.debugStr.append(this.prefix);
            this.debugStr.append(str);
        }
    }

    /* access modifiers changed from: protected */
    /* renamed from: aW */
    public boolean mo15720aW(Vec3f vec3f) {
        float f = vec3f.x + vec3f.y + vec3f.z;
        if (Float.isNaN(f) || Float.isInfinite(f)) {
            return false;
        }
        return true;
    }

    /* access modifiers changed from: protected */
    /* renamed from: Z */
    public boolean mo15718Z(Vec3d ajr) {
        float f = (float) (ajr.x + ajr.y + ajr.z);
        if (Float.isNaN(f) || Float.isInfinite(f)) {
            return false;
        }
        return true;
    }
}
