package logic.abb;

import com.hoplon.geometry.Vec3f;
import game.network.message.serializable.C1216Rw;

/* renamed from: a.aDg  reason: case insensitive filesystem */
/* compiled from: a */
public class C5280aDg extends C6659arH {
    private static final float hxV = 10.0f;
    private final Vec3f hxX = new Vec3f(0.0f, 0.0f, 0.0f);
    private float hxW = 0.5f;

    /* renamed from: a */
    public boolean mo31a(C1216Rw rw, float f) {
        this.hxW = 0.85f;
        float ra = (aYa().mo1091ra() * (1.0f - this.hxW)) / 2.0f;
        this.hxX.x += cRR();
        this.hxX.y += cRR();
        this.hxX.z += cRR();
        if (m25382a(this.hxX)) {
            this.hxX.set(1.0f, 0.0f, 0.0f);
        } else {
            this.hxX.normalize();
        }
        this.hxX.scale(ra);
        float ra2 = aYa().mo1091ra() * this.hxW;
        rw.brw().x = this.hxX.x;
        rw.brw().y = this.hxX.y;
        rw.brw().z = this.hxX.z - ra2;
        return true;
    }

    private float cRR() {
        return (aYa().mo1091ra() / hxV) * ((float) (Math.random() - 0.5d));
    }

    public void start() {
        this.hxX.set(0.0f, 0.0f, 0.0f);
    }

    public float cRS() {
        return this.hxW;
    }

    /* renamed from: mm */
    public void mo8426mm(float f) {
        this.hxW = Math.min(1.0f, Math.max(0.0f, f));
    }
}
