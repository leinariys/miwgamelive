package logic.abb;

import game.geometry.Vec3d;

/* renamed from: a.pw */
/* compiled from: a */
public class C3284pw extends C1218Ry {
    private static final float aSV = 4.0f;
    private static final float aSW = 9.0f;
    private float aSX;
    private Vec3d point;

    public C3284pw() {
        this((Vec3d) null);
    }

    public C3284pw(Vec3d ajr) {
        this.aSX = 0.0f;
        this.point = ajr;
    }

    /* renamed from: Vv */
    public Vec3d mo5315Vv() {
        return this.point;
    }

    /* access modifiers changed from: protected */
    /* renamed from: Vw */
    public float mo5316Vw() {
        return aYa().mo989an(this.point).lengthSquared();
    }

    /* renamed from: q */
    public void mo21252q(Vec3d ajr) {
        this.point = ajr;
    }

    /* access modifiers changed from: protected */
    /* renamed from: cN */
    public float mo3565cN(float f) {
        float f2 = f - (aSW * this.eaA);
        if (f2 <= this.aSX * this.aSX) {
            return 0.0f;
        }
        if (this.aSX != 0.0f) {
            float sqrt = ((float) Math.sqrt((double) f2)) - this.aSX;
            f2 = sqrt * sqrt;
        }
        float ra = (aYa().mo1091ra() / aYa().mo1092rb()) * aSV;
        float lengthSquared = ra * aYa().mo1090qZ().lengthSquared() * ra;
        if (f2 >= lengthSquared || lengthSquared < 1.0f) {
            return aYa().mo1091ra();
        }
        return ((float) Math.sqrt((double) (f2 / lengthSquared))) * aYa().mo1091ra();
    }

    /* access modifiers changed from: protected */
    public void initialize() {
        if (this.point == null) {
            throw new NullPointerException("Target position is null (ArrivalBehaviour)");
        }
        super.initialize();
    }

    /* renamed from: Vx */
    public float mo21250Vx() {
        return this.aSX;
    }

    /* renamed from: cO */
    public void mo21251cO(float f) {
        this.aSX = f;
    }
}
