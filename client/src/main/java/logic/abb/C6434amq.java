package logic.abb;

import game.network.message.serializable.C1216Rw;
import game.script.ship.Ship;

/* renamed from: a.amq  reason: case insensitive filesystem */
/* compiled from: a */
public class C6434amq extends C6659arH {
    /* renamed from: a */
    public boolean mo31a(C1216Rw rw, float f) {
        if (aYa() instanceof Ship) {
            Ship fAVar = (Ship) aYa();
            if (fAVar.afR() == null || !fAVar.afR().cUB()) {
                return false;
            }
            rw.mo5308dm(true);
            return true;
        } else if (!(aYa() instanceof Turret)) {
            return false;
        } else {
            Turret jq = (Turret) aYa();
            if (jq.afR() == null || !jq.afR().cUB()) {
                return false;
            }
            rw.mo5308dm(true);
            return true;
        }
    }
}
