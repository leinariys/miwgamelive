package logic.abb;

import com.hoplon.geometry.Vec3f;
import game.geometry.Vec3d;
import game.network.message.serializable.C6400amI;
import game.script.Actor;
import game.script.ship.Ship;

/* renamed from: a.aET */
/* compiled from: a */
public abstract class aET extends C1218Ry {
    private static final float hIf = 9000000.0f;
    public Ship trackedShip;
    private boolean fUB = true;
    private float hIg = 0.0f;

    public aET() {
    }

    public aET(Ship fAVar) {
        mo8609J(fAVar);
    }

    public aET(Ship fAVar, boolean z) {
        this.fUB = z;
        mo8609J(fAVar);
    }

    /* access modifiers changed from: protected */
    /* renamed from: Vv */
    public Vec3d mo5315Vv() {
        return this.trackedShip.getPosition();
    }

    /* access modifiers changed from: protected */
    /* renamed from: Vw */
    public float mo5316Vw() {
        return aYa().mo1013bx((Actor) this.trackedShip);
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public void mo5317a(Vec3f vec3f, float f, float f2) {
        if (this.fUB && f < hIf && this.trackedShip != null && this.trackedShip.ahO() != null && f2 > 1.0f) {
            float sqrt = ((float) Math.sqrt((double) f)) / f2;
            C6400amI ami = new C6400amI();
            ((C6400amI) this.trackedShip.ahO().mo2472kQ()).mo14818b(sqrt, ami);
            Vec3f an = aYa().mo989an(ami.blk());
            vec3f.x = an.x;
            vec3f.y = an.y;
            vec3f.z = an.z;
        }
    }

    /* access modifiers changed from: protected */
    public void initialize() {
        if (this.trackedShip == null) {
            throw new NullPointerException("Tracked ship is null (BasePursuitBehaviour)");
        }
        super.initialize();
    }

    public Ship bUT() {
        return this.trackedShip;
    }

    /* renamed from: J */
    public void mo8609J(Ship fAVar) {
        this.trackedShip = fAVar;
        if (fAVar == null || fAVar.mo958IL() == null) {
            this.hIg = 0.0f;
        } else {
            this.hIg = fAVar.mo958IL().getBoundingBox().diu();
        }
    }

    public boolean chr() {
        return this.fUB;
    }

    /* renamed from: fe */
    public void mo8612fe(boolean z) {
        this.fUB = z;
    }
}
