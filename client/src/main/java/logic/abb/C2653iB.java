package logic.abb;

import com.hoplon.geometry.Vec3f;
import game.network.message.serializable.C1216Rw;
import game.script.Actor;
import game.script.ship.Ship;

import java.util.Iterator;

/* renamed from: a.iB */
/* compiled from: a */
public class C2653iB extends C6659arH {

    /* renamed from: Yq */
    private float f8095Yq;

    /* renamed from: Yr */
    private float f8096Yr;

    /* renamed from: Ys */
    private float f8097Ys;

    /* renamed from: Yt */
    private float f8098Yt;
    private boolean disabled;

    /* renamed from: a */
    public boolean mo31a(C1216Rw rw, float f) {
        if (this.disabled) {
            return false;
        }
        Pawn aYa = aYa();
        Iterator<Actor> it = mo15717Mn().iterator();
        boolean z = false;
        while (true) {
            if (!it.hasNext()) {
                break;
            }
            Actor next = it.next();
            if ((next instanceof Ship) && !next.isStatic()) {
                Ship fAVar = (Ship) next;
                if (mo15722aa(fAVar)) {
                    Vec3f an = fAVar.mo989an(aYa.getPosition());
                    float f2 = -an.z;
                    if (f2 > 0.0f && f2 <= this.f8095Yq) {
                        float max = Math.max((((an.x * an.x) + (an.y * an.y)) - fAVar.mo958IL().getBoundingBox().diu()) - aYa.mo958IL().getBoundingBox().diu(), 0.0f);
                        float f3 = f2 * this.f8096Yr;
                        float f4 = f3 * f3;
                        if (max > f4) {
                            continue;
                        } else if (this.f8098Yt < this.f8097Ys) {
                            z = true;
                            break;
                        } else {
                            Vec3f vec3f = new Vec3f(an.x, an.y, 0.0f);
                            if (m25382a(vec3f)) {
                                vec3f.set(-1.0f, 0.0f, 0.0f);
                            } else {
                                vec3f.normalize();
                            }
                            vec3f.scale((0.5f + ((f4 - max) / f4)) * aYa.mo1091ra());
                            Vec3f bp = aYa.mo1008bp(fAVar.mo1009br(vec3f));
                            rw.brw().x += bp.x;
                            rw.brw().y += bp.y;
                            Vec3f brw = rw.brw();
                            brw.z = bp.z + brw.z;
                            z = true;
                        }
                    }
                } else {
                    continue;
                }
            }
        }
        if (z) {
            this.f8098Yt = Math.min(this.f8097Ys, this.f8098Yt + f);
            if (rw.brw().x == 0.0f && rw.brw().y == 0.0f && rw.brw().z == 0.0f) {
                return false;
            }
            return true;
        }
        this.f8098Yt = Math.max(0.0f, this.f8098Yt - f);
        return false;
    }

    /* renamed from: Bx */
    public float mo19445Bx() {
        return this.f8095Yq;
    }

    /* renamed from: aJ */
    public void mo19448aJ(float f) {
        this.f8095Yq = f;
    }

    /* renamed from: By */
    public float mo19446By() {
        return this.f8097Ys;
    }

    /* renamed from: aK */
    public void mo19449aK(float f) {
        this.f8097Ys = f;
    }

    public boolean isDisabled() {
        return this.disabled;
    }

    public void setDisabled(boolean z) {
        this.disabled = z;
    }

    /* renamed from: aL */
    public void mo19450aL(float f) {
        this.f8096Yr = (float) Math.sin(Math.toRadians((double) f));
    }

    /* renamed from: Bz */
    public float mo19447Bz() {
        return this.f8096Yr;
    }
}
