package logic.abb;

import com.hoplon.geometry.Vec3f;
import game.geometry.Vec3d;
import game.network.message.serializable.C1216Rw;
import game.script.ship.Ship;

/* renamed from: a.Ry */
/* compiled from: a */
public abstract class C1218Ry extends C6659arH implements C3254pg {
    private static final Vec3f cip = new Vec3f(0.0f, 0.0f, -1.0f);
    private static final float eaw = 1.6E7f;
    private static final float eax = 4000000.0f;
    private static final float eay = ((float) Math.cos(Math.toRadians(20.0d)));
    private static final float eaz = ((float) Math.cos(Math.toRadians(60.0d)));
    public float eaA = -1.0f;
    public boolean eaC = false;
    private boolean eaB = true;

    /* access modifiers changed from: protected */
    /* renamed from: Vv */
    public abstract Vec3d mo5315Vv();

    /* access modifiers changed from: protected */
    /* renamed from: Vw */
    public abstract float mo5316Vw();

    /* access modifiers changed from: protected */
    /* renamed from: cN */
    public abstract float mo3565cN(float f);

    /* renamed from: do */
    public void mo5318do(boolean z) {
        this.eaB = z;
    }

    /* renamed from: a */
    public boolean mo31a(C1216Rw rw, float f) {
        float cN;
        initialize();
        Vec3f an = aYa().mo989an(mo5315Vv());
        float Vw = mo5316Vw();
        if (Vw <= 250.0f) {
            this.eaC = true;
        }
        rw.mo5311hr(0.0f);
        if (mo4408e(an, Vw)) {
            rw.mo5309dn(true);
            cN = aYa().mo1091ra();
        } else {
            rw.mo5309dn(false);
            cN = mo3565cN(Vw);
            mo5317a(an, Vw, cN);
        }
        mo8602a(rw.brw(), f, an, cN);
        return true;
    }

    private boolean brB() {
        return ((Ship) aYa()).agZ();
    }

    /* access modifiers changed from: protected */
    /* renamed from: e */
    public boolean mo4408e(Vec3f vec3f, float f) {
        if (!this.eaB || !(aYa() instanceof Ship)) {
            return false;
        }
        if (brB()) {
            if (f < eax || vec3f.dot(cip) / vec3f.length() <= eaz) {
                return false;
            }
            return true;
        } else if (f < eaw || vec3f.dot(cip) / vec3f.length() <= eay) {
            return false;
        } else {
            return true;
        }
    }

    /* access modifiers changed from: protected */
    public void initialize() {
        if (this.eaA == -1.0f) {
            this.eaA = aYa().mo958IL().getOuterSphereRadius();
            this.eaA *= this.eaA;
        }
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public void mo5317a(Vec3f vec3f, float f, float f2) {
    }

    public boolean isEnded() {
        return this.eaC;
    }
}
