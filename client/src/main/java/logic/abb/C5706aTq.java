package logic.abb;

import game.geometry.Vec3d;

/* renamed from: a.aTq  reason: case insensitive filesystem */
/* compiled from: a */
public class C5706aTq extends C1218Ry {
    private Vec3d point;

    public C5706aTq() {
    }

    public C5706aTq(Vec3d ajr) {
        this.point = ajr;
    }

    public C5706aTq(Vec3d ajr, boolean z) {
        this(ajr);
        mo5318do(z);
    }

    /* renamed from: Vv */
    public Vec3d mo5315Vv() {
        return this.point;
    }

    /* access modifiers changed from: protected */
    /* renamed from: Vw */
    public float mo5316Vw() {
        return aYa().mo989an(this.point).lengthSquared();
    }

    /* renamed from: q */
    public void mo11544q(Vec3d ajr) {
        this.point = ajr;
    }

    /* access modifiers changed from: protected */
    /* renamed from: cN */
    public float mo3565cN(float f) {
        return aYa().mo1091ra();
    }
}
