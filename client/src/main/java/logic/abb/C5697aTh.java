package logic.abb;

import game.script.ship.Ship;

/* renamed from: a.aTh  reason: case insensitive filesystem */
/* compiled from: a */
public class C5697aTh extends aET {
    public static final float iOp = 0.5f;
    public float iOq = 200.0f;

    public C5697aTh() {
    }

    public C5697aTh(Ship fAVar) {
        super(fAVar);
    }

    public C5697aTh(Ship fAVar, boolean z) {
        super(fAVar, z);
    }

    /* access modifiers changed from: protected */
    /* renamed from: cN */
    public float mo3565cN(float f) {
        float f2 = 0.0f;
        float sqrt = (float) Math.sqrt((double) f);
        float rb = aYa().mo1092rb() * iOp;
        float f3 = aYa().mo1090qZ().z;
        float f4 = (f3 * f3) / (rb * 2.0f);
        float abs = Math.abs(this.trackedShip.mo1090qZ().z);
        if (sqrt > (this.iOq * 3.0f) + f4) {
            f2 = aYa().mo1091ra();
        } else if (((double) sqrt) >= (((double) this.iOq) * 0.5d) + ((double) f4)) {
            if (abs > 30.0f) {
                f2 = (abs * sqrt) / (this.iOq + f4);
            } else if (sqrt > this.iOq) {
                float f5 = sqrt / (this.iOq + f4);
                f2 = f5 * aYa().mo1091ra() * f5;
            }
        }
        if (f2 < 1.0f) {
            return 1.0f;
        }
        return f2;
    }

    /* renamed from: oC */
    public void mo11520oC(float f) {
        this.iOq = f;
    }

    public float dvA() {
        return this.iOq;
    }
}
