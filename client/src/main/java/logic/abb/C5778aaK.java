package logic.abb;

import com.hoplon.geometry.Vec3f;
import game.network.message.serializable.C1216Rw;
import game.script.Actor;

import javax.vecmath.Tuple3f;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/* renamed from: a.aaK  reason: case insensitive filesystem */
/* compiled from: a */
public class C5778aaK extends C6659arH {
    private static final float eWT = 100.0f;
    private static final float eWU = 2.0f;
    private static final float eWV = 15.0f;

    /* renamed from: lN */
    private static final float f4052lN = 10.0f;

    /* renamed from: lO */
    private static final float f4053lO = 1.0f;
    private List<Actor> eWW = new ArrayList();
    private Map<Actor, Float> eWX = new HashMap();

    public C5778aaK() {
    }

    public C5778aaK(Actor cr) {
        this.eWW.add(cr);
    }

    /* renamed from: a */
    public boolean mo31a(C1216Rw rw, float f) {
        float length = aYa().mo1090qZ().length();
        float ra = aYa().mo1091ra() * f4052lN;
        if (length < 1.0f) {
            return false;
        }
        Tuple3f brw = rw.brw();
        float outerSphereRadius = aYa().mo958IL().getOuterSphereRadius();
        Tuple3f tuple3f = brw;
        for (Actor next : mo15717Mn()) {
            if (!this.eWW.contains(next)) {
                if (this.eWX.containsKey(next)) {
                    this.eWX.get(next).floatValue();
                } else {
                    Vec3f an = aYa().mo989an(next.getPosition());
                    if (!(next.mo958IL() == null || next.mo958IL().getBoundingBox() == null)) {
                        float outerSphereRadius2 = next.mo958IL().getOuterSphereRadius();
                        float length2 = an.length();
                        float max = Math.max(0.001f, Math.min(2.0f, 0.2f + ((outerSphereRadius2 / outerSphereRadius) / eWT))) * ra;
                        float max2 = Math.max(0.0f, (length2 - outerSphereRadius2) - outerSphereRadius);
                        if (max2 <= max) {
                            an.negate();
                            float min = Math.min(1.0f, Math.max(0.0f, 1.0f - (max2 / max)) + 0.001f);
                            float f2 = (1.1f / (2.0f * ((1.0f - min) + 0.5f))) - (((1.0f - min) * 1.1f) / 3.0f);
                            float ra2 = length / aYa().mo1091ra();
                            float ra3 = ((1.1f / (eWV * ((1.0f - ra2) + 0.06666667f))) - ((1.1f - ra2) / 16.0f)) * aYa().mo1091ra();
                            an.normalize();
                            if (an.z > 0.9f) {
                                an.x += Math.signum(an.x) * 0.2f;
                                an.y += Math.signum(an.y) * 0.2f;
                            }
                            an.z = 0.0f;
                            an.normalize();
                            an.scale(f2 * ra3 * 1.0f * 1.0f * (1.0f / f));
                            tuple3f = tuple3f.mo23503i(an);
                        }
                    }
                }
            }
        }
        rw.brw().set(tuple3f);
        if (rw.brw().x == 0.0f && rw.brw().y == 0.0f && rw.brw().z == 0.0f) {
            return false;
        }
        return true;
    }

    /* renamed from: a */
    public void mo12169a(Actor... crArr) {
        for (Actor add : crArr) {
            this.eWW.add(add);
        }
    }

    public List bMI() {
        return this.eWW;
    }

    /* renamed from: aK */
    public boolean mo12170aK(Actor cr) {
        return this.eWW.remove(cr);
    }

    /* renamed from: c */
    public void mo12174c(Actor cr, float f) {
        this.eWX.put(cr, Float.valueOf(f));
    }

    public Map bMJ() {
        return this.eWX;
    }

    /* renamed from: aL */
    public Float mo12171aL(Actor cr) {
        return this.eWX.remove(cr);
    }
}
