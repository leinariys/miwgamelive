package logic.abb;

import com.hoplon.geometry.Vec3f;
import game.geometry.Quat4fWrap;
import game.network.message.serializable.C1216Rw;
import game.script.Actor;
import game.script.ship.Ship;

/* renamed from: a.AC */
/* compiled from: a */
public class C0009AC extends C6659arH {

    /* renamed from: Y */
    private static final Vec3f f8Y = new Vec3f(0.0f, 1.0f, 0.0f);
    private static final float aSV = 1.0f;
    private static final float cik = 9000000.0f;
    private static final float cil = 15.0f;
    private static final float cim = 2500.0f;
    private static final float cin = 5.0f;
    private static final float cio = 1.0f;
    private static final Vec3f cip = new Vec3f(0.0f, 0.0f, -1.0f);
    public Ship ciq;
    private Vec3f cir;
    private float cis;
    private transient float cit = -1.0f;

    public C0009AC(Ship fAVar, Vec3f vec3f, float f) {
        this.ciq = fAVar;
        this.cir = vec3f;
        this.cis = f;
    }

    /* renamed from: a */
    public boolean mo31a(C1216Rw rw, float f) {
        float ra;
        if (!awl()) {
            rw.brw().set(0.0f, 0.0f, 0.0f);
            mo8603g(rw.brw(), f);
        } else {
            if (this.cit < 0.0f) {
                this.cit = aYa().mo958IL().getBoundingBox().diu();
            }
            Vec3f awj = awj();
            float lengthSquared = awj.lengthSquared();
            Vec3f awi = awi();
            if (m41ev(lengthSquared)) {
                awj.set(awi);
                rw.mo5311hr(1.0f);
            } else {
                float length = ((aYa().mo1090qZ().length() * aYa().mo1091ra()) / aYa().mo1092rb()) * 1.0f;
                if (lengthSquared >= length * length) {
                    ra = aYa().mo1091ra();
                } else {
                    rw.mo5311hr(1.0f);
                    ra = ((lengthSquared / length) / length) * aYa().mo1091ra();
                }
                if (m25382a(awj)) {
                    awj.set(0.0f, 0.0f, -1.0f);
                } else {
                    awj.normalize();
                }
                awj.scale(ra);
                Vec3f awk = awk();
                awj.x += awk.x;
                awj.y += awk.y;
                awj.z = awk.z + awj.z;
            }
            rw.mo5309dn(m39a(lengthSquared, awi));
            if (!rw.bry() && m40d(awj, lengthSquared)) {
                Vec3f aT = Quat4fWrap.m24430d(cip, (double) this.cis).mo15209aT(mo15727d(this.ciq, f8Y));
                float degrees = (float) Math.toDegrees((double) aT.angle(f8Y));
                if (degrees <= 4.0f) {
                    degrees = 0.0f;
                } else if (f8Y.mo23476b(aT).dot(cip) < 0.0f) {
                    degrees = -degrees;
                }
                rw.setRoll(degrees);
            }
            mo8603g(awj, f);
            rw.brw().set(awj);
        }
        return true;
    }

    private Vec3f awi() {
        return aYa().getOrientation().mo15210aU(this.ciq.getOrientation().mo15209aT(cip));
    }

    private Vec3f awj() {
        return mo15724c((Actor) this.ciq, this.cir);
    }

    /* renamed from: ev */
    private boolean m41ev(float f) {
        return f < 1.0f * this.cit && this.ciq.mo1090qZ().z > -10.0f;
    }

    /* renamed from: d */
    private boolean m40d(Vec3f vec3f, float f) {
        return f < cim && m42i(vec3f, cip) < 5.0f;
    }

    /* renamed from: a */
    private boolean m39a(float f, Vec3f vec3f) {
        return (f > cik || this.ciq.ahh() || this.ciq.agZ()) && m42i(vec3f, cip) < cil;
    }

    /* renamed from: i */
    private float m42i(Vec3f vec3f, Vec3f vec3f2) {
        return (vec3f.angle(vec3f2) * 180.0f) / 3.1415927f;
    }

    private Vec3f awk() {
        return aYa().mo1008bp(this.ciq.cLu());
    }

    /* access modifiers changed from: protected */
    public boolean awl() {
        return this.ciq != null && !this.ciq.isDead() && this.ciq.bae();
    }

    public Ship awm() {
        return this.ciq;
    }

    /* renamed from: C */
    public void mo29C(Ship fAVar) {
        this.ciq = fAVar;
    }

    public Vec3f aqD() {
        return this.cir;
    }

    /* renamed from: J */
    public void mo30J(Vec3f vec3f) {
        this.cir = vec3f;
    }

    /* renamed from: ee */
    public void mo35ee(float f) {
        this.cis = f;
    }
}
