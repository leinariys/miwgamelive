package logic.abb;

import com.hoplon.geometry.Vec3f;
import game.network.message.serializable.C1216Rw;
import game.script.Actor;
import game.script.ai.npc.TurretAIController;
import game.script.item.Weapon;
import game.script.ship.Ship;
import logic.thred.LogPrinter;
import org.apache.commons.logging.Log;

/* renamed from: a.aNA */
/* compiled from: a */
public class aNA extends C6659arH {
    private static final Log log = LogPrinter.setClass(aNA.class);
    public Actor ams;
    private boolean fUB;

    public aNA() {
        this((Ship) null);
    }

    public aNA(Ship fAVar) {
        this(fAVar, false);
    }

    public aNA(Ship fAVar, boolean z) {
        mo10203X(fAVar);
        this.fUB = z;
    }

    /* renamed from: a */
    public boolean mo31a(C1216Rw rw, float f) {
        if (this.ams == null || this.ams.mo958IL() == null) {
            return false;
        }
        if (aPd() == null || rw == null || this.ams.mo1090qZ() == null || aPd().aZS() == null || aPd().aZS().mo1090qZ() == null) {
            if (aPd() == null) {
                log.error("TURRET BEHAVIOUR: <<0>>");
            } else if (this.ams.mo1090qZ() == null) {
                log.error("TURRET BEHAVIOUR: <<1>>");
            } else if (aPd().aZS() == null) {
                log.error("TURRET BEHAVIOUR: <<2>>");
            } else if (aPd().aZS().mo1090qZ() == null) {
                log.error("TURRET BEHAVIOUR: <<3>>");
            }
            ((TurretAIController) aRF()).stop();
            return false;
        }
        Pawn aZS = aPd().aZS();
        m16534cq(aZS);
        Vec3f brw = rw.brw();
        aPd().bam().mo4851b(this.ams.getPosition(), brw);
        float max = Math.max(0.0f, brw.length());
        Vec3f vec3f = new Vec3f(0.0f, 0.0f, -max);
        float lengthSquared = brw.mo23506j(vec3f).lengthSquared();
        Weapon afR = aPd().afR();
        if (lengthSquared / 10.0f > this.ams.mo958IL().getBoundingBox().diu()) {
            if (afR.cUB()) {
                rw.mo5308dm(true);
            }
        } else if (afR.aqh() <= 0) {
            rw.mo5308dm(true);
        } else if (!afR.cUB() && max < afR.mo12940in()) {
            rw.mo5306dk(true);
        }
        if (this.fUB) {
            float speed = afR.aqz().getSpeed();
            if (speed <= 1.0f) {
                log.warn("Error in weapon data: speed is " + speed + " (WeaponType = " + afR.aqz() + ")");
                speed = 1000.0f;
            }
            float f2 = max / speed;
            brw.set(mo15724c(this.ams, mo15728e(this.ams, f2)).mo23506j(mo15727d(aZS, mo15728e(aZS, f2))));
        }
        brw.sub(vec3f);
        return true;
    }

    /* renamed from: cq */
    private void m16534cq(Actor cr) {
        if (aRF() instanceof TurretAIController) {
            TurretAIController aqs = (TurretAIController) aRF();
            float length = this.ams.mo1090qZ().length();
            float length2 = cr.mo1090qZ().length();
            if (length > 50.0f || length2 > 50.0f) {
                aqs.mo15677jx(true);
            } else if (length < 20.0f || length2 < 20.0f) {
                aqs.mo15677jx(false);
            }
        }
    }

    public Turret aPd() {
        return (Turret) aYa();
    }

    /* renamed from: X */
    public void mo10203X(Actor cr) {
        this.ams = cr;
    }

    public boolean dki() {
        return this.fUB;
    }

    /* renamed from: fe */
    public void mo10206fe(boolean z) {
        this.fUB = z;
    }
}
