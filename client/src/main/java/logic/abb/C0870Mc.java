package logic.abb;

import game.network.message.serializable.C1216Rw;
import p001a.C6078afy;

import java.io.Serializable;

/* renamed from: a.Mc */
/* compiled from: a */
public class C0870Mc implements C0573Hw, Serializable {

    private final C0573Hw[] aue;
    private final C1216Rw dyU = new C1216Rw();
    private StringBuffer debugStr;
    private C6078afy dyT;
    private String prefix;

    public C0870Mc(C0573Hw... hwArr) {
        this.aue = hwArr;
    }

    public C6078afy aRF() {
        return this.dyT;
    }

    /* renamed from: a */
    public void mo2697a(C6078afy afy) {
        this.dyT = afy;
        for (C0573Hw a : this.aue) {
            a.mo2697a(afy);
        }
    }

    /* access modifiers changed from: protected */
    public Pawn aYa() {
        return aRF().aYa();
    }

    public void start() {
        for (C0573Hw start : this.aue) {
            start.start();
        }
    }

    /* renamed from: a */
    public boolean mo31a(C1216Rw rw, float f) {
        int i = 0;
        boolean z = false;
        for (C0573Hw hw : this.aue) {
            this.dyU.clear();
            if (hw.mo31a(this.dyU, f)) {
                i++;
                if (this.dyU.bry()) {
                    rw.mo5309dn(true);
                }
                if (this.dyU.brt()) {
                    rw.mo5306dk(true);
                    rw.mo5296aF(this.dyU.brz());
                    rw.mo5308dm(false);
                } else if (this.dyU.brv()) {
                    rw.mo5308dm(true);
                }
                if (this.dyU.brx() != null) {
                    rw.mo5312r(this.dyU.brx());
                }
                if (this.dyU.bru()) {
                    rw.mo5307dl(true);
                }
                rw.brw().x += this.dyU.brw().x;
                rw.brw().y += this.dyU.brw().y;
                rw.brw().z += this.dyU.brw().z;
                rw.setRoll(rw.getRoll() + this.dyU.getRoll());
                rw.mo5311hr(rw.brA() + this.dyU.brA());
                z = true;
            }
        }
        rw.mo5311hr(rw.brA() / ((float) i));
        return z;
    }

    public String aRG() {
        StringBuilder sb = new StringBuilder("Group: ");
        C0573Hw[] hwArr = this.aue;
        int length = hwArr.length;
        int i = 0;
        int i2 = 0;
        while (i < length) {
            C0573Hw hw = hwArr[i];
            if (i2 > 0) {
                sb.append(", ");
            }
            sb.append(hw.aRG());
            i++;
            i2++;
        }
        sb.append(" [");
        sb.append(i2);
        sb.append("]");
        return sb.toString();
    }

    public C0573Hw[] bgB() {
        return this.aue;
    }

    /* renamed from: a */
    public void mo2698a(StringBuffer stringBuffer, String str) {
        this.debugStr = stringBuffer;
        this.prefix = str;
    }

    /* access modifiers changed from: protected */
    /* renamed from: eI */
    public void mo3988eI(String str) {
        if (this.debugStr != null) {
            if (this.debugStr.lastIndexOf("\n") == this.debugStr.length() - 1) {
                this.debugStr.append(this.prefix);
            }
            this.debugStr.append(str);
            this.debugStr.append("\n");
        }
    }

    /* access modifiers changed from: protected */
    /* renamed from: eJ */
    public void mo3989eJ(String str) {
        if (this.debugStr != null) {
            if (this.debugStr.lastIndexOf("\n") == this.debugStr.length() - 1) {
                this.debugStr.append(this.prefix);
            }
            this.debugStr.append(str);
        }
    }
}
