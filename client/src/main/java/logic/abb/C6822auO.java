package logic.abb;

import com.hoplon.geometry.Vec3f;
import game.network.message.serializable.C1216Rw;
import game.script.Actor;
import game.script.item.Shot;
import p001a.C6294akG;

/* renamed from: a.auO  reason: case insensitive filesystem */
/* compiled from: a */
public class C6822auO extends C6659arH {
    private static final float eWU = 5.0f;

    /* renamed from: lP */
    private static final float f5376lP = 10000.0f;
    private float gFc = 100.0f;
    private float gFd = 1.0f;
    private boolean gFe = false;
    private String gFf = "obstacle-sum";
    private String gFg = "obstacle-force";

    /* renamed from: lQ */
    private Actor f5377lQ;

    public C6822auO() {
    }

    public C6822auO(Actor cr) {
        this.f5377lQ = cr;
    }

    /* renamed from: a */
    public boolean mo31a(C1216Rw rw, float f) {
        float min = Math.min(((2.0f * aYa().mo1091ra()) / aYa().mo963VH()) * this.gFc, 10000.0f);
        if (min < 1.0f) {
            return false;
        }
        Vec3f cLy = aYa().cLy();
        cLy.scale(0.1f);
        Vec3f vec3f = new Vec3f();
        float outerSphereRadius = aYa().mo958IL().getOuterSphereRadius();
        Vec3f vec3f2 = vec3f;
        for (Actor next : mo15717Mn()) {
            if (this.f5377lQ != next && aYa().aiD().mo2545a(next.aiD())) {
                Vec3f an = aYa().mo989an(next.getPosition());
                float outerSphereRadius2 = next.mo958IL().getOuterSphereRadius();
                float length = an.length();
                float hypot = (float) Math.hypot((double) an.x, (double) an.y);
                if ((hypot <= (1.2f * outerSphereRadius) + outerSphereRadius2 || (2.0f * outerSphereRadius) + outerSphereRadius2 >= length) && (an.z <= 0.0f || hypot / length >= 0.2f)) {
                    float max = Math.max(0.001f, Math.min(2.0f, 0.2f + ((outerSphereRadius2 / outerSphereRadius) / this.gFc))) * min;
                    float max2 = Math.max(0.0f, (length - outerSphereRadius2) - outerSphereRadius);
                    if (max2 <= max) {
                        if (!this.gFe) {
                            an.z = 0.0f;
                        }
                        an.negate();
                        float min2 = Math.min(1.0f, Math.max(0.0f, 1.0f - (max2 / max)) + 0.001f);
                        an.normalize();
                        Math.max(1.0f, Math.min(length, length / hypot));
                        an.scale(((1.0f / (5.0f * ((1.0f - min2) + 0.2f))) - ((1.0f - min2) / 6.0f)) * min * this.gFd * (1.0f / f));
                        if (next instanceof Shot) {
                            an.scale(0.25f);
                        }
                        Vec3f i = vec3f2.mo23503i(an);
                        if (this.gFe) {
                            vec3f2 = i.mo23503i(cLy);
                        } else {
                            vec3f2 = i;
                        }
                    }
                }
            }
        }
        C6294akG.m23128a(this.gFf, (Actor) aYa(), vec3f2);
        rw.brw().set(vec3f2);
        if (rw.brw().x == 0.0f && rw.brw().y == 0.0f && rw.brw().z == 0.0f) {
            return false;
        }
        return true;
    }

    /* renamed from: a */
    public void mo16334a(Actor cr) {
        this.f5377lQ = cr;
    }

    public Actor cyj() {
        return this.f5377lQ;
    }

    public float cyk() {
        return this.gFc;
    }

    /* renamed from: kP */
    public void mo16341kP(float f) {
        this.gFc = f;
    }

    public float cyl() {
        return this.gFd;
    }

    /* renamed from: kQ */
    public void mo16342kQ(float f) {
        this.gFd = f;
    }

    public void cym() {
        this.gFg = "collision-force";
        this.gFf = "collision-sum";
    }

    public boolean cyn() {
        return this.gFe;
    }

    /* renamed from: gQ */
    public void mo16340gQ(boolean z) {
        this.gFe = z;
    }
}
