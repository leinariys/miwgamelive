package logic.swing;

import logic.ui.ComponentManager;

import javax.swing.*;
import javax.swing.plaf.ComponentUI;
import javax.swing.plaf.basic.BasicButtonListener;
import javax.swing.plaf.basic.BasicToggleButtonUI;
import java.awt.*;

/* renamed from: a.aLO */
/* compiled from: a */
public class ToggleButtonUI extends BasicToggleButtonUI implements IComponentUi {

    /* renamed from: Rp */
    private ComponentManager f3304Rp;

    public ToggleButtonUI(JToggleButton jToggleButton) {
        jToggleButton.setRolloverEnabled(true);
        jToggleButton.setOpaque(false);
        this.f3304Rp = new C1401UY("toggle-button", jToggleButton);
    }

    public static ComponentUI createUI(JComponent jComponent) {
        return new ToggleButtonUI((JToggleButton) jComponent);
    }

    /* access modifiers changed from: protected */
    public BasicButtonListener createButtonListener(AbstractButton abstractButton) {
        return new C5371aGt(abstractButton, this.f3304Rp);
    }

    public void update(Graphics graphics, JComponent jComponent) {
        AdapterUiCss.m9084a((IComponentUi) this, graphics, (AbstractButton) jComponent);
        paint(graphics, jComponent);
    }

    public Dimension getPreferredSize(JComponent jComponent) {
        AdapterUiCss.m9088a((IComponentUi) this, (AbstractButton) jComponent);
        return getComponentManager().mo13059c(jComponent, ToggleButtonUI.super.getPreferredSize(jComponent));
    }

    public Dimension getMinimumSize(JComponent jComponent) {
        AdapterUiCss.m9088a((IComponentUi) this, (AbstractButton) jComponent);
        return getComponentManager().mo13057b(jComponent, ToggleButtonUI.super.getMinimumSize(jComponent));
    }

    public Dimension getMaximumSize(JComponent jComponent) {
        AdapterUiCss.m9088a((IComponentUi) this, (AbstractButton) jComponent);
        return getComponentManager().mo13052a(jComponent, ToggleButtonUI.super.getMaximumSize(jComponent));
    }

    /* renamed from: wy */
    public ComponentManager getComponentManager() {
        return this.f3304Rp;
    }
}
