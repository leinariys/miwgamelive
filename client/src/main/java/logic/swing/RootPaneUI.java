package logic.swing;

import logic.ui.ComponentManager;

import javax.swing.*;
import javax.swing.border.Border;
import javax.swing.plaf.ComponentUI;
import javax.swing.plaf.basic.BasicRootPaneUI;
import java.awt.*;

/* renamed from: a.aPU */
/* compiled from: a */
public class RootPaneUI extends BasicRootPaneUI implements IComponentUi {

    /* renamed from: Rp */
    private ComponentManager f3529Rp;

    public RootPaneUI(JRootPane jRootPane) {
        this.f3529Rp = new ComponentManager("rootpane", jRootPane);
        jRootPane.setOpaque(false);
        jRootPane.setBorder(BorderWrapper.bfB());
    }

    public static ComponentUI createUI(JComponent jComponent) {
        return new RootPaneUI((JRootPane) jComponent);
    }

    /* renamed from: wy */
    public ComponentManager getComponentManager() {
        return this.f3529Rp;
    }

    public Dimension getPreferredSize(JComponent jComponent) {
        return getComponentManager().mo13059c(jComponent, RootPaneUI.super.getPreferredSize(jComponent));
    }

    public Dimension getMinimumSize(JComponent jComponent) {
        return getComponentManager().mo13057b(jComponent, RootPaneUI.super.getMinimumSize(jComponent));
    }

    public Dimension getMaximumSize(JComponent jComponent) {
        return getComponentManager().mo13052a(jComponent, RootPaneUI.super.getMaximumSize(jComponent));
    }

    public void paint(Graphics graphics, JComponent jComponent) {
        AdapterUiCss.m9085a((IComponentUi) this, graphics, jComponent);
        Border border = jComponent.getBorder();
        if (border != null) {
            border.paintBorder(jComponent, graphics, 0, 0, jComponent.getWidth(), jComponent.getHeight());
        }
    }
}
