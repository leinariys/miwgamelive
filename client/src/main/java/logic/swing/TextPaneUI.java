package logic.swing;

import logic.ui.ComponentManager;

import javax.swing.*;
import javax.swing.plaf.ComponentUI;
import javax.swing.plaf.basic.BasicTextPaneUI;
import javax.swing.text.JTextComponent;
import java.awt.*;

/* renamed from: a.DM */
/* compiled from: a */
public class TextPaneUI extends BasicTextPaneUI implements IComponentUi {

    private final JComponent cIX;
    /* renamed from: Rp */
    private ComponentManager f396Rp;
    private final C0356Ek bHX = new C0356Ek(this.f396Rp);

    public TextPaneUI(JComponent jComponent) {
        this.cIX = jComponent;
        this.f396Rp = new ComponentManager("textpane", jComponent);
        jComponent.setOpaque(false);
    }

    public static ComponentUI createUI(JComponent jComponent) {
        return new TextPaneUI(jComponent);
    }

    /* access modifiers changed from: protected */
    public void installListeners() {
        TextPaneUI.super.installListeners();
        this.cIX.addKeyListener(this.bHX);
        this.cIX.addMouseListener(this.bHX);
    }

    /* access modifiers changed from: protected */
    public void uninstallListeners() {
        TextPaneUI.super.uninstallListeners();
        this.cIX.removeKeyListener(this.bHX);
        this.cIX.removeMouseListener(this.bHX);
    }

    public void update(Graphics graphics, JComponent jComponent) {
        AdapterUiCss.m9087a((IComponentUi) this, graphics, (JTextComponent) jComponent);
        TextPaneUI.super.paint(graphics, jComponent);
    }

    /* access modifiers changed from: protected */
    public void paintBackground(Graphics graphics) {
    }

    public Dimension getMinimumSize(JComponent jComponent) {
        AdapterUiCss.m9091a((IComponentUi) this, (JTextComponent) jComponent);
        return getComponentManager().mo13057b(jComponent, TextPaneUI.super.getMinimumSize(jComponent));
    }

    public Dimension getMaximumSize(JComponent jComponent) {
        AdapterUiCss.m9091a((IComponentUi) this, (JTextComponent) jComponent);
        return getComponentManager().mo13052a(jComponent, TextPaneUI.super.getMaximumSize(jComponent));
    }

    public Dimension getPreferredSize(JComponent jComponent) {
        AdapterUiCss.m9091a((IComponentUi) this, (JTextComponent) jComponent);
        return this.f396Rp.mo13059c(jComponent, TextPaneUI.super.getPreferredSize(jComponent));
    }

    /* renamed from: wy */
    public ComponentManager getComponentManager() {
        return this.f396Rp;
    }
}
