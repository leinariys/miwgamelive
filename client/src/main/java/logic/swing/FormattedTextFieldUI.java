package logic.swing;

import logic.ui.ComponentManager;

import javax.swing.*;
import javax.swing.plaf.ComponentUI;
import javax.swing.plaf.basic.BasicTextFieldUI;
import javax.swing.text.JTextComponent;
import java.awt.*;

/* renamed from: a.ait  reason: case insensitive filesystem */
/* compiled from: a */
public class FormattedTextFieldUI extends BasicTextFieldUI implements IComponentUi {

    private final JFormattedTextField fNp;
    /* renamed from: Rp */
    private ComponentManager f4670Rp;
    private final C0356Ek bHX = new C0356Ek(this.f4670Rp);

    public FormattedTextFieldUI(JFormattedTextField jFormattedTextField) {
        this.fNp = jFormattedTextField;
        this.f4670Rp = new C6233aix("textfield", jFormattedTextField);
        jFormattedTextField.setSelectionColor(new Color(12, 92, 111));
    }

    public static ComponentUI createUI(JComponent jComponent) {
        return new FormattedTextFieldUI((JFormattedTextField) jComponent);
    }

    public void update(Graphics graphics, JComponent jComponent) {
        AdapterUiCss.m9087a((IComponentUi) this, graphics, (JTextComponent) jComponent);
        FormattedTextFieldUI.super.paint(graphics, jComponent);
    }

    /* access modifiers changed from: protected */
    public void installListeners() {
        FormattedTextFieldUI.super.installListeners();
        this.fNp.addKeyListener(this.bHX);
        this.fNp.addMouseListener(this.bHX);
    }

    /* access modifiers changed from: protected */
    public void uninstallListeners() {
        FormattedTextFieldUI.super.uninstallListeners();
        this.fNp.removeKeyListener(this.bHX);
        this.fNp.removeMouseListener(this.bHX);
    }

    /* access modifiers changed from: protected */
    public void paintBackground(Graphics graphics) {
    }

    public Dimension getMinimumSize(JComponent jComponent) {
        AdapterUiCss.m9091a((IComponentUi) this, (JTextComponent) jComponent);
        return getComponentManager().mo13057b(jComponent, FormattedTextFieldUI.super.getMinimumSize(jComponent));
    }

    public Dimension getMaximumSize(JComponent jComponent) {
        AdapterUiCss.m9091a((IComponentUi) this, (JTextComponent) jComponent);
        return getComponentManager().mo13052a(jComponent, FormattedTextFieldUI.super.getMaximumSize(jComponent));
    }

    public Dimension getPreferredSize(JComponent jComponent) {
        AdapterUiCss.m9091a((IComponentUi) this, (JTextComponent) jComponent);
        return this.f4670Rp.mo13059c(jComponent, FormattedTextFieldUI.super.getPreferredSize(jComponent));
    }

    /* renamed from: wy */
    public ComponentManager getComponentManager() {
        return this.f4670Rp;
    }
}
