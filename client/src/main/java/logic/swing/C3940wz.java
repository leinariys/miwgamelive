package logic.swing;

import javax.swing.*;
import java.awt.*;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/* renamed from: a.wz */
/* compiled from: a */
public abstract class C3940wz {
    public static final String bDU = "gui.TooltipProvider";
    private static final Map<String, C3940wz> bDV = new ConcurrentHashMap();

    /* renamed from: a */
    public static void m40775a(String str, C3940wz wzVar) {
        bDV.put(str, wzVar);
    }

    /* renamed from: d */
    public static C3940wz m40778d(JComponent jComponent) {
        Object clientProperty = jComponent.getClientProperty(bDU);
        if (clientProperty != null) {
            if (clientProperty instanceof String) {
                return bDV.get(clientProperty);
            }
            if (clientProperty instanceof C3940wz) {
                return (C3940wz) clientProperty;
            }
        }
        return null;
    }

    /* renamed from: a */
    public static void m40777a(JComponent jComponent, String str) {
        jComponent.putClientProperty(bDU, str);
    }

    /* renamed from: a */
    public static void m40776a(JComponent jComponent, C3940wz wzVar) {
        jComponent.putClientProperty(bDU, wzVar);
    }

    /* renamed from: b */
    public abstract Component mo2675b(Component component);
}
