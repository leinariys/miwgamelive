package logic.swing;

/* renamed from: a.xo */
/* compiled from: a */
public enum C3991xo {
    REPEAT,
    REPEAT_X,
    REPEAT_Y,
    NO_REPEAT,
    STRETCH,
    STRETCH_X,
    STRETCH_Y,
    REPEAT_Y_STRETCH_X,
    REPEAT_X_STRETCH_Y
}
