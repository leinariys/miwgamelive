package logic.swing;

import logic.ui.ComponentManager;

import javax.swing.*;
import javax.swing.plaf.ComponentUI;
import javax.swing.plaf.basic.BasicSeparatorUI;
import java.awt.*;

/* renamed from: a.uw */
/* compiled from: a */
public class SeparatorUI extends BasicSeparatorUI implements IComponentUi {

    /* renamed from: Rp */
    private ComponentManager f9378Rp;

    public SeparatorUI(JSeparator jSeparator) {
        this.f9378Rp = new ComponentManager("separator", jSeparator);
        jSeparator.setOpaque(false);
    }

    public static ComponentUI createUI(JComponent jComponent) {
        return new SeparatorUI((JSeparator) jComponent);
    }

    public void update(Graphics graphics, JComponent jComponent) {
        AdapterUiCss.m9085a((IComponentUi) this, graphics, jComponent);
        paint(graphics, jComponent);
    }

    public Dimension getPreferredSize(JComponent jComponent) {
        AdapterUiCss.m9089a((IComponentUi) this, jComponent);
        return getComponentManager().mo13059c(jComponent, SeparatorUI.super.getPreferredSize(jComponent));
    }

    public Dimension getMinimumSize(JComponent jComponent) {
        AdapterUiCss.m9089a((IComponentUi) this, jComponent);
        return getComponentManager().mo13057b(jComponent, SeparatorUI.super.getMinimumSize(jComponent));
    }

    public Dimension getMaximumSize(JComponent jComponent) {
        AdapterUiCss.m9089a((IComponentUi) this, jComponent);
        return getComponentManager().mo13052a(jComponent, SeparatorUI.super.getMaximumSize(jComponent));
    }

    /* renamed from: wy */
    public ComponentManager getComponentManager() {
        return this.f9378Rp;
    }
}
