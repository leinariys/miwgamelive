package logic.swing;

import logic.ui.ComponentManager;
import logic.ui.IBaseUiTegXml;
import taikodom.render.scene.SoundObject;

import javax.swing.*;
import javax.swing.plaf.ComponentUI;
import javax.swing.plaf.basic.BasicScrollBarUI;
import java.awt.*;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;

/* renamed from: a.Ab */
/* compiled from: a */
public class ScrollBarUI extends BasicScrollBarUI implements IComponentUi {
    /* access modifiers changed from: private */
    public static final IBaseUiTegXml aPy = IBaseUiTegXml.initBaseUItegXML();
    /* access modifiers changed from: private */
    /* access modifiers changed from: private */
    public final JScrollBar scrollbar;
    /* renamed from: Rp */
    public ComponentManager f70Rp;
    /* access modifiers changed from: private */
    public SoundObject ccY;
    private JComponent ccZ;
    private JComponent cda;
    private JComponent cdb;
    private JComponent cdc;

    public ScrollBarUI(JScrollBar jScrollBar) {
        this.scrollbar = jScrollBar;
        this.f70Rp = new ComponentManager("scrollbar", jScrollBar);
        jScrollBar.setBackground((Color) null);
        jScrollBar.setOpaque(false);
    }

    public static ComponentUI createUI(JComponent jComponent) {
        return new ScrollBarUI((JScrollBar) jComponent);
    }

    /* access modifiers changed from: protected */
    public void installListeners() {
        ScrollBarUI.super.installListeners();
        this.scrollTimer = new C0050a(60, this.scrollListener);
        this.scrollTimer.setInitialDelay(300);
    }

    /* access modifiers changed from: protected */
    public BasicScrollBarUI.ArrowButtonListener createArrowButtonListener() {
        return new C0051b();
    }

    public void update(Graphics graphics, JComponent jComponent) {
        AdapterUiCss.m9085a((IComponentUi) this, graphics, jComponent);
        ScrollBarUI.super.update(graphics, jComponent);
    }

    /* renamed from: wy */
    public ComponentManager getComponentManager() {
        return this.f70Rp;
    }

    public Dimension getPreferredSize(JComponent jComponent) {
        int i = 0;
        int i2 = 0;
        if (this.scrollbar.getOrientation() == 1) {
            int i3 = 0;
            i2 = 0;
            i = 0;
            while (i3 < jComponent.getComponentCount()) {
                Dimension preferredSize = jComponent.getComponent(i3).getPreferredSize();
                int i4 = preferredSize.height + i2;
                i = Math.max(i, preferredSize.width);
                i3++;
                i2 = i4;
            }
        } else {
            int i5 = 0;
            int i6 = 0;
            for (int i7 = 0; i7 < jComponent.getComponentCount(); i7++) {
                Dimension preferredSize2 = jComponent.getComponent(i7).getPreferredSize();
                i6 = i + preferredSize2.width;
                i5 = Math.max(i2, preferredSize2.height);
            }
        }
        return getComponentManager().mo13059c(jComponent, new Dimension(i, i2));
    }

    public Dimension getMinimumSize(JComponent jComponent) {
        return getComponentManager().mo13057b(jComponent, ScrollBarUI.super.getMinimumSize(jComponent));
    }

    public Dimension getMaximumSize(JComponent jComponent) {
        return getComponentManager().mo13052a(jComponent, ScrollBarUI.super.getMaximumSize(jComponent));
    }

    /* access modifiers changed from: protected */
    public void paintTrack(Graphics graphics, JComponent jComponent, Rectangle rectangle) {
        JComponent e = mo342e(jComponent);
        e.setBounds(rectangle);
        e.setVisible(true);
    }

    /* access modifiers changed from: protected */
    public Rectangle getThumbBounds() {
        return ScrollBarUI.super.getThumbBounds();
    }

    /* access modifiers changed from: protected */
    public void paintThumb(Graphics graphics, JComponent jComponent, Rectangle rectangle) {
        int i = 0;
        if (this.scrollbar.isEnabled()) {
            JComponent f = mo343f(jComponent);
            int i2 = isThumbRollover() ? 32 : 0;
            if (this.isDragging) {
                i = 16;
            }
            ComponentManager.getCssHolder(f).mo13063q(48, i2 | i);
            f.setBounds(rectangle);
            f.setVisible(true);
        }
    }

    /* access modifiers changed from: protected */
    public JButton createDecreaseButton(int i) {
        String fR = m442fR(i);
        if (fR == null) {
            return null;
        }
        JButton jButton = new JButton();
        ComponentManager.getCssHolder(jButton).setAttribute("name", fR);
        jButton.setName(fR);
        this.scrollbar.add(jButton);
        return jButton;
    }

    /* access modifiers changed from: protected */
    public JButton createIncreaseButton(int i) {
        String fR = m442fR(i);
        if (fR == null) {
            return null;
        }
        JButton jButton = new JButton();
        ComponentManager.getCssHolder(jButton).setAttribute("name", fR);
        jButton.setName(fR);
        this.scrollbar.add(jButton);
        return jButton;
    }

    /* renamed from: fR */
    private String m442fR(int i) {
        switch (i) {
            case 1:
                return "start";
            case 3:
                return "right";
            case 5:
                return "end";
            case 7:
                return "left";
            default:
                return null;
        }
    }

    /* access modifiers changed from: protected */
    /* renamed from: e */
    public JComponent mo342e(JComponent jComponent) {
        if (this.scrollbar.getOrientation() == 1) {
            if (this.cdc == null) {
                this.cdc = new JLabel();
                this.cdc.setName("vertical-track");
                ComponentManager.getCssHolder(this.cdc).setAttribute("name", "vertical-track");
                jComponent.add(this.cdc);
            }
            if (this.cdb != null) {
                this.cdb.setVisible(false);
            }
            return this.cdc;
        }
        if (this.cdb == null) {
            this.cdb = new JLabel();
            this.cdb.setName("horizontal-track");
            ComponentManager.getCssHolder(this.cdb).setAttribute("name", "horizontal-track");
            jComponent.add(this.cdb);
        }
        if (this.cdc != null) {
            this.cdc.setVisible(false);
        }
        return this.cdb;
    }

    /* access modifiers changed from: protected */
    /* renamed from: f */
    public JComponent mo343f(JComponent jComponent) {
        if (this.scrollbar.getOrientation() == 1) {
            if (this.cda == null) {
                this.cda = new JLabel();
                this.cda.setName("vertical-thumb");
                ComponentManager.getCssHolder(this.cda).setAttribute("name", "vertical-thumb");
                jComponent.add(this.cda, 0);
            }
            if (this.ccZ != null) {
                this.ccZ.setVisible(false);
            }
            return this.cda;
        }
        if (this.ccZ == null) {
            this.ccZ = new JLabel();
            this.ccZ.setName("horizontal-thumb");
            ComponentManager.getCssHolder(this.ccZ).setAttribute("name", "horizontal-thumb");
            jComponent.add(this.ccZ, 0);
        }
        if (this.cda != null) {
            this.cda.setVisible(false);
        }
        return this.ccZ;
    }

    /* access modifiers changed from: protected */
    public BasicScrollBarUI.TrackListener createTrackListener() {
        return new C0052c();
    }

    /* renamed from: a.Ab$a */
    public class C0050a extends Timer {

        public C0050a(int i, ActionListener actionListener) {
            super(i, actionListener);
        }

        public void stop() {
            super.stop();
            if (ScrollBarUI.this.ccY != null) {
                ScrollBarUI.this.ccY.stop();
            }
        }
    }

    /* renamed from: a.Ab$b */
    /* compiled from: a */
    public class C0051b extends BasicScrollBarUI.ArrowButtonListener {

        public void mousePressed(MouseEvent mouseEvent) {
            super.mousePressed(mouseEvent);
            ScrollBarUI.aPy.mo13724cx(ScrollBarUI.this.f70Rp.mo13049Vr().atV());
            if ("start".equals(mouseEvent.getComponent().getName()) || "left".equals(mouseEvent.getComponent().getName())) {
                if (ScrollBarUI.this.scrollbar.getValue() <= ScrollBarUI.this.scrollbar.getMinimum()) {
                    return;
                }
            } else if (("end".equals(mouseEvent.getComponent().getName()) || "right".equals(mouseEvent.getComponent().getName())) && ScrollBarUI.this.scrollbar.getValue() + ScrollBarUI.this.scrollbar.getModel().getExtent() >= ScrollBarUI.this.scrollbar.getMaximum()) {
                return;
            }
            ScrollBarUI.this.ccY = ScrollBarUI.aPy.mo13724cx(ScrollBarUI.this.f70Rp.mo13049Vr().auj());
        }

        public void mouseReleased(MouseEvent mouseEvent) {
            super.mouseReleased(mouseEvent);
        }
    }

    /* renamed from: a.Ab$c */
    /* compiled from: a */
    class C0052c extends BasicScrollBarUI.TrackListener {

        public void mousePressed(MouseEvent mouseEvent) {
            boolean d = ScrollBarUI.this.isDragging;
            super.mousePressed(mouseEvent);
            if (SwingUtilities.isRightMouseButton(mouseEvent)) {
                return;
            }
            if ((ScrollBarUI.this.getSupportsAbsolutePositioning() || !SwingUtilities.isMiddleMouseButton(mouseEvent)) && d != ScrollBarUI.this.isDragging) {
                ScrollBarUI.this.mo343f(ScrollBarUI.this.scrollbar).repaint();
            }
        }

        public void mouseReleased(MouseEvent mouseEvent) {
            boolean d = ScrollBarUI.this.isDragging;
            super.mouseReleased(mouseEvent);
            if (SwingUtilities.isRightMouseButton(mouseEvent)) {
                return;
            }
            if ((ScrollBarUI.this.getSupportsAbsolutePositioning() || !SwingUtilities.isMiddleMouseButton(mouseEvent)) && d != ScrollBarUI.this.isDragging) {
                ScrollBarUI.this.mo343f(ScrollBarUI.this.scrollbar).repaint();
            }
        }
    }
}
