package logic.swing;

import javax.swing.*;
import javax.swing.plaf.ComponentUI;
import java.util.HashMap;
import java.util.Map;

/* renamed from: a.ps */
/* compiled from: a */
public class UIDefaultsUI extends UIDefaults {

    Map<String, C3277b> iTa = new HashMap<String, C3277b>();

    public UIDefaultsUI(int initialCapacity, float loadFactor) {
        super(initialCapacity, loadFactor);
        this.iTa.put("javax.swing.JLabel", new C3277b());
    }

    public ComponentUI getUI(JComponent jComponent) {
        Class<?> cls = jComponent.getClass();
        do {
            C3276a aVar = this.iTa.get(cls.getName());
            if (aVar != null) {
                return aVar.createUI(jComponent);
            }
        } while (cls != Object.class);
        return UIDefaultsUI.super.getUI(jComponent);
    }

    /* renamed from: a.ps$a */
    public static abstract class C3276a<T extends JComponent> {
        public abstract ComponentUI createUI(T t);
    }

    /* renamed from: a.ps$b */
    /* compiled from: a */
    class C3277b extends C3276a<JLabel> {
        /* renamed from: d */
        public ComponentUI createUI(JLabel jLabel) {
            return new LabelUI(jLabel);
        }
    }
}
