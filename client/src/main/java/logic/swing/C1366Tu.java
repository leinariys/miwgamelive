package logic.swing;

import java.awt.*;

/* renamed from: a.Tu */
/* compiled from: a */
public class C1366Tu {
    public static final C1366Tu fEC = new C1366Tu(0.0f, (C1367a) null);
    public static final C1366Tu fED = new C1366Tu(1.0E9f, C1367a.PX);
    public static final C1366Tu fEE = new C1366Tu(0.0f, C1367a.PX);
    public static final C1366Tu fEF = new C1366Tu(0.0f, C1367a.PERCENT);
    public static final C1366Tu fEG = new C1366Tu(100.0f, C1367a.PERCENT);
    private static /* synthetic */ int[] fEI;
    public final C1367a fEH;
    public final float value;

    public C1366Tu(float f, C1367a aVar) {
        this.value = f;
        this.fEH = aVar;
    }

    static /* synthetic */ int[] bXq() {
        int[] iArr = fEI;
        if (iArr == null) {
            iArr = new int[C1367a.values().length];
            try {
                iArr[C1367a.EM.ordinal()] = 1;
            } catch (NoSuchFieldError e) {
            }
            try {
                iArr[C1367a.PERCENT.ordinal()] = 2;
            } catch (NoSuchFieldError e2) {
            }
            try {
                iArr[C1367a.PT.ordinal()] = 4;
            } catch (NoSuchFieldError e3) {
            }
            try {
                iArr[C1367a.PX.ordinal()] = 3;
            } catch (NoSuchFieldError e4) {
            }
            fEI = iArr;
        }
        return iArr;
    }

    /* renamed from: jp */
    public int mo5756jp(float f) {
        if (this.fEH != null) {
            switch (bXq()[this.fEH.ordinal()]) {
                case 1:
                    return Math.round(this.value * f);
                case 2:
                    return Math.round((this.value * f) / 100.0f);
                case 3:
                    return Math.round(this.value);
                case 4:
                    return Math.round((this.value / 72.0f) / ((float) Toolkit.getDefaultToolkit().getScreenResolution()));
            }
        }
        return Math.round(this.value);
    }

    /* renamed from: jq */
    public int mo5757jq(float f) {
        int screenResolution = Toolkit.getDefaultToolkit().getScreenResolution();
        if (this.fEH != null) {
            switch (bXq()[this.fEH.ordinal()]) {
                case 1:
                    return Math.round(this.value * f);
                case 2:
                    return Math.round((this.value * f) / 100.0f);
                case 3:
                    return Math.round((((float) screenResolution) * this.value) / 72.0f);
                case 4:
                    return Math.round(this.value);
            }
        }
        return Math.round(this.value);
    }

    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof C1366Tu)) {
            return super.equals(obj);
        }
        C1366Tu tu = (C1366Tu) obj;
        if (tu.value == this.value && tu.fEH == this.fEH) {
            return true;
        }
        return false;
    }

    public int hashCode() {
        return (this.fEH != null ? this.fEH.ordinal() : -1) ^ (Float.floatToIntBits(this.value) * 31);
    }

    public String toString() {
        return String.valueOf(this.value) + " " + this.fEH;
    }

    /* renamed from: a.Tu$a */
    public enum C1367a {
        EM,
        PERCENT,
        PX,
        PT
    }
}
