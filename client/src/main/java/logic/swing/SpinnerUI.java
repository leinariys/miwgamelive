package logic.swing;

import logic.ui.ComponentManager;
import logic.ui.IComponentManager;

import javax.swing.*;
import javax.swing.plaf.ComponentUI;
import javax.swing.plaf.basic.BasicSpinnerUI;
import java.awt.*;

/* renamed from: a.Hx */
/* compiled from: a */
public class SpinnerUI extends BasicSpinnerUI implements IComponentUi {

    /* renamed from: BM */
    private IComponentManager f695BM;

    public SpinnerUI(JSpinner jSpinner) {
        jSpinner.setBorder(new BorderWrapper());
        this.f695BM = new ComponentManager("spinner", jSpinner);
    }

    public static ComponentUI createUI(JComponent jComponent) {
        return new SpinnerUI((JSpinner) jComponent);
    }

    /* renamed from: wz */
    public IComponentManager getComponentManager() {
        return this.f695BM;
    }

    public Dimension getPreferredSize(JComponent jComponent) {
        return getComponentManager().mo13059c(jComponent, SpinnerUI.super.getPreferredSize(jComponent));
    }

    public Dimension getMinimumSize(JComponent jComponent) {
        return getComponentManager().mo13057b(jComponent, SpinnerUI.super.getMinimumSize(jComponent));
    }

    public Dimension getMaximumSize(JComponent jComponent) {
        return getComponentManager().mo13052a(jComponent, SpinnerUI.super.getMaximumSize(jComponent));
    }

    /* access modifiers changed from: protected */
    public Component createNextButton() {
        JButton jButton = new JButton();
        jButton.setFocusable(false);
        ComponentManager.getCssHolder(jButton).setAttribute("name", "next");
        this.spinner.add(jButton);
        installNextButtonListeners(jButton);
        return jButton;
    }

    /* access modifiers changed from: protected */
    public Component createPreviousButton() {
        JButton jButton = new JButton();
        jButton.setFocusable(false);
        ComponentManager.getCssHolder(jButton).setAttribute("name", "previous");
        this.spinner.add(jButton);
        installPreviousButtonListeners(jButton);
        return jButton;
    }

    public void update(Graphics graphics, JComponent jComponent) {
        paint(graphics, jComponent);
    }
}
