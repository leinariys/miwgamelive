package logic.swing;

import logic.ui.ComponentManager;

import javax.swing.*;
import javax.swing.plaf.ComponentUI;
import javax.swing.plaf.basic.BasicPopupMenuUI;
import java.awt.*;

/* renamed from: a.KH */
/* compiled from: a */
public class PopupMenuUI extends BasicPopupMenuUI implements IComponentUi {

    /* renamed from: Rp */
    private ComponentManager f934Rp;

    public PopupMenuUI(JPopupMenu jPopupMenu) {
        jPopupMenu.setLightWeightPopupEnabled(true);
        this.f934Rp = new ComponentManager("popupmenu", jPopupMenu);
        jPopupMenu.setOpaque(false);
    }

    public static ComponentUI createUI(JComponent jComponent) {
        return new PopupMenuUI((JPopupMenu) jComponent);
    }

    public Popup getPopup(JPopupMenu jPopupMenu, int i, int i2) {
        return C5457aKb.getSharedInstance().getPopup(jPopupMenu.getInvoker(), jPopupMenu, i, i2);
    }

    public void update(Graphics graphics, JComponent jComponent) {
        AdapterUiCss.m9085a((IComponentUi) this, graphics, jComponent);
        paint(graphics, jComponent);
    }

    public Dimension getPreferredSize(JComponent jComponent) {
        AdapterUiCss.m9089a((IComponentUi) this, jComponent);
        return getComponentManager().mo13059c(jComponent, PopupMenuUI.super.getPreferredSize(jComponent));
    }

    public Dimension getMinimumSize(JComponent jComponent) {
        AdapterUiCss.m9089a((IComponentUi) this, jComponent);
        return getComponentManager().mo13057b(jComponent, PopupMenuUI.super.getMinimumSize(jComponent));
    }

    public Dimension getMaximumSize(JComponent jComponent) {
        AdapterUiCss.m9089a((IComponentUi) this, jComponent);
        return getComponentManager().mo13052a(jComponent, PopupMenuUI.super.getMaximumSize(jComponent));
    }

    /* renamed from: wy */
    public ComponentManager getComponentManager() {
        return this.f934Rp;
    }
}
