package logic.swing;

import logic.ui.PropertiesUiFromCss;

import java.awt.*;

/* renamed from: a.Z */
/* compiled from: a */
public class BorderWrapperAndComponent extends BorderWrapper {
    private Component component;

    public BorderWrapperAndComponent(Component component2) {
        this.component = component2;
    }

    public Insets getBorderInsets(Component component2) {
        return super.getBorderInsets(this.component);
    }

    public void paintBorder(Component component2, Graphics graphics, int i, int i2, int i3, int i4) {
        super.paintBorder(this.component, graphics, i, i2, i3, i4);
    }

    /* renamed from: a */
    public Insets mo3848a(PropertiesUiFromCss aj, Component component2) {
        return super.mo3848a(aj, this.component);
    }
}
