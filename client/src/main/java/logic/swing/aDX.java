package logic.swing;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/* renamed from: a.aDX */
/* compiled from: a */
public class aDX {
    private static Map<String, C5358aGg> iDP = new HashMap();

    static {
        iDP.put("", C5358aGg.hOF);
        iDP.put("NONE", C5358aGg.hOF);
        iDP.put("STRONG_IN", C5358aGg.hOJ);
        iDP.put("STRONG_OUT", C5358aGg.hOK);
        iDP.put("STRONG_IN_OUT", C5358aGg.hOL);
        iDP.put("REGULAR_IN", C5358aGg.hOG);
        iDP.put("REGULAR_OUT", C5358aGg.hOH);
        iDP.put("REGULAR_IN_OUT", C5358aGg.hOI);
        iDP.put("BACK_IN", C5358aGg.hOM);
        iDP.put("BACK_OUT", C5358aGg.hON);
        iDP.put("BACK_IN_OUT", C5358aGg.hOO);
        iDP.put("ELASTIC_IN", C5358aGg.hOP);
        iDP.put("ELASTIC_OUT", C5358aGg.hOQ);
        iDP.put("ELASTIC_IN_OUT", C5358aGg.hOR);
    }

    private final JComponent component;
    private final C2740jL<JComponent> iDQ;
    private C0454GJ atu;
    private int iDR;
    private int iDS;
    private int iDT;
    private C5358aGg iDU;
    private long startTime;
    private Timer timer;

    public aDX(JComponent jComponent, String str, C2740jL jLVar, C0454GJ gj, boolean z) {
        this.component = jComponent;
        this.iDQ = jLVar;
        String[] split = str.split(";");
        parse(split[0]);
        if (split.length > 1) {
            StringBuffer stringBuffer = new StringBuffer(split[1]);
            for (int i = 2; i < split.length; i++) {
                stringBuffer.append(";" + split[i]);
            }
            this.atu = new C1781a(new String(stringBuffer.toString()), jLVar, gj);
        } else {
            this.atu = gj;
        }
        System.out.flush();
        if (z) {
            dpf();
        }
    }

    public aDX(JComponent jComponent, String str, C2740jL jLVar, C0454GJ gj) {
        this(jComponent, str, jLVar, gj, true);
    }

    public void dpf() {
        this.startTime = System.currentTimeMillis();
        this.timer = new Timer(10, new C1782b());
        this.timer.setInitialDelay(0);
        this.timer.start();
    }

    private void parse(String str) {
        Matcher matcher = Pattern.compile("\\[(-?\\d{0,10})\\.\\.(-?\\d{0,10})\\] dur (\\d{1,10}).?([A-Za-z_]*)").matcher(str);
        if (!matcher.find()) {
            throw new IllegalArgumentException("Parse error: " + str);
        }
        if (matcher.group(1).length() == 0) {
            this.iDR = (int) this.iDQ.mo5a(this.component);
        } else {
            this.iDR = Integer.parseInt(matcher.group(1));
        }
        this.iDS = Integer.parseInt(matcher.group(2));
        this.iDT = Integer.parseInt(matcher.group(3));
        this.iDU = iDP.get(matcher.group(4).toUpperCase());
    }

    /* access modifiers changed from: protected */
    /* renamed from: gZ */
    public void mo8409gZ() {
        int currentTimeMillis = (int) (System.currentTimeMillis() - this.startTime);
        this.iDQ.mo7a(this.component, (float) ((int) (((((float) this.iDU.mo9101aD(currentTimeMillis, this.iDT)) / ((float) this.iDT)) * ((float) (this.iDS - this.iDR))) + ((float) this.iDR))));
        if (currentTimeMillis >= this.iDT) {
            this.timer.stop();
            if (this.atu != null) {
                this.atu.mo9a(this.component);
            }
        }
    }

    public void cAD() {
        if (this.atu != null) {
            this.atu.mo9a(this.component);
        }
        kill();
    }

    public void kill() {
        this.atu = null;
        this.iDT = 0;
        if (this.timer != null) {
            this.timer.stop();
        }
    }

    /* renamed from: iU */
    public C2740jL mo8410iU() {
        return this.iDQ;
    }

    /* renamed from: a.aDX$a */
    class C1781a implements C0454GJ {
        private final /* synthetic */ String hEM;
        private final /* synthetic */ C2740jL hEN;
        private final /* synthetic */ C0454GJ hEO;

        C1781a(String str, C2740jL jLVar, C0454GJ gj) {
            this.hEM = str;
            this.hEN = jLVar;
            this.hEO = gj;
        }

        /* renamed from: a */
        public void mo9a(JComponent jComponent) {
            new aDX(jComponent, this.hEM, this.hEN, this.hEO, true);
        }
    }

    /* renamed from: a.aDX$b */
    /* compiled from: a */
    class C1782b implements ActionListener {
        C1782b() {
        }

        public void actionPerformed(ActionEvent actionEvent) {
            aDX.this.mo8409gZ();
        }
    }
}
