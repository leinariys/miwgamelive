package logic.swing;

import logic.ui.ComponentManager;
import logic.ui.PropertiesUiFromCss;
import taikodom.render.graphics2d.RGraphics2;

import javax.swing.*;
import javax.swing.plaf.ComponentUI;
import javax.swing.text.JTextComponent;
import java.awt.*;
import java.awt.image.ImageObserver;

/* renamed from: a.RM */
/* compiled from: a */
public class AdapterUiCss {
    private static /* synthetic */ int[] eaL;

    static /* synthetic */ int[] brE() {
        int[] iArr = eaL;
        if (iArr == null) {
            iArr = new int[C3991xo.values().length];
            try {
                iArr[C3991xo.NO_REPEAT.ordinal()] = 4;
            } catch (NoSuchFieldError e) {
            }
            try {
                iArr[C3991xo.REPEAT.ordinal()] = 1;
            } catch (NoSuchFieldError e2) {
            }
            try {
                iArr[C3991xo.REPEAT_X.ordinal()] = 2;
            } catch (NoSuchFieldError e3) {
            }
            try {
                iArr[C3991xo.REPEAT_X_STRETCH_Y.ordinal()] = 9;
            } catch (NoSuchFieldError e4) {
            }
            try {
                iArr[C3991xo.REPEAT_Y.ordinal()] = 3;
            } catch (NoSuchFieldError e5) {
            }
            try {
                iArr[C3991xo.REPEAT_Y_STRETCH_X.ordinal()] = 8;
            } catch (NoSuchFieldError e6) {
            }
            try {
                iArr[C3991xo.STRETCH.ordinal()] = 5;
            } catch (NoSuchFieldError e7) {
            }
            try {
                iArr[C3991xo.STRETCH_X.ordinal()] = 6;
            } catch (NoSuchFieldError e8) {
            }
            try {
                iArr[C3991xo.STRETCH_Y.ordinal()] = 7;
            } catch (NoSuchFieldError e9) {
            }
            eaL = iArr;
        }
        return iArr;
    }

    /* renamed from: a */
    public static void m9099a(ComponentUI componentUI, Graphics graphics, JComponent jComponent) {
        if (ComponentManager.getCssHolder(jComponent).getAttribute("debug") != null) {
            graphics.setColor(Color.RED);
            graphics.drawRect(0, 0, jComponent.getWidth(), jComponent.getHeight());
            graphics.drawRect(jComponent.getWidth(), 0, jComponent.getWidth(), jComponent.getHeight());
            graphics.drawLine(0, 0, jComponent.getWidth(), 0);
            graphics.drawLine(0, 0, jComponent.getWidth(), jComponent.getHeight());
            if (jComponent.getName() != null && jComponent.getFont() != null) {
                graphics.setFont(jComponent.getFont());
                graphics.setColor(Color.LIGHT_GRAY);
                graphics.drawString(jComponent.getName(), 0, 12);
            }
        }
    }

    /* renamed from: a */
    public static void m9089a(IComponentUi aih, JComponent jComponent) {
        PropertiesUiFromCss Vp = aih.getComponentManager().mo13047Vp();
        if (Vp != null) {
            m9097a(jComponent, Vp);
        }
    }

    /* renamed from: a */
    public static void m9088a(IComponentUi aih, AbstractButton abstractButton) {
        PropertiesUiFromCss Vp = aih.getComponentManager().mo13047Vp();
        if (Vp != null) {
            m9096a(abstractButton, Vp);
        }
    }

    /* renamed from: a */
    public static void m9090a(IComponentUi aih, JLabel jLabel) {
        PropertiesUiFromCss Vp = aih.getComponentManager().mo13047Vp();
        if (Vp != null) {
            m9098a(jLabel, Vp);
        }
    }

    /* renamed from: a */
    public static void m9091a(IComponentUi aih, JTextComponent jTextComponent) {
        PropertiesUiFromCss Vp = aih.getComponentManager().mo13047Vp();
        if (Vp != null) {
            m9100a(jTextComponent, Vp);
        }
    }

    /* renamed from: eq */
    private static <T, V> boolean m9102eq(T t, V v) {
        if (t == null) {
            return v != null;
        }
        return t.equals(v);
    }

    /* renamed from: a */
    private static void m9097a(JComponent jComponent, PropertiesUiFromCss aj) {
        if (!m9102eq(jComponent.getFont(), aj.getFont())) {
            jComponent.setFont(aj.getFont());
        }
        if (!m9102eq(jComponent.getForeground(), aj.getColor())) {
            jComponent.setForeground(aj.getColor());
        }
        if (!m9102eq(aj.getCursor(), jComponent.getCursor())) {
            jComponent.setCursor(aj.getCursor());
        }
        if (!m9102eq(Boolean.valueOf(jComponent.getComponentOrientation() != null), aj.getComponentOrientation())) {
            jComponent.setComponentOrientation(aj.getComponentOrientation());
        }
    }

    /* renamed from: a */
    private static void m9096a(AbstractButton abstractButton, PropertiesUiFromCss aj) {
        if (!m9102eq(aj.aty(), Integer.valueOf(aj.aty().akW()))) {
            abstractButton.setHorizontalAlignment(aj.aty().akW());
        }
        if (!m9102eq(aj.atR(), Integer.valueOf(aj.atR().akW()))) {
            abstractButton.setVerticalAlignment(aj.atR().akW());
        }
        if (!m9102eq(aj.getIcon(), abstractButton.getIcon())) {
            abstractButton.setIcon(aj.getIcon());
        }
        m9097a((JComponent) abstractButton, aj);
    }

    /* renamed from: a */
    private static void m9098a(JLabel jLabel, PropertiesUiFromCss aj) {
        int akW = aj.aty().akW();
        if (!m9102eq(aj.aty(), Integer.valueOf(akW))) {
            jLabel.setHorizontalAlignment(akW);
        }
        int akW2 = aj.atR().akW();
        if (!m9102eq(aj.atR(), Integer.valueOf(akW2))) {
            jLabel.setVerticalAlignment(akW2);
        }
        Icon icon = aj.getIcon();
        if (!m9102eq(icon, jLabel.getIcon())) {
            jLabel.setIcon(icon);
        }
        m9097a((JComponent) jLabel, aj);
    }

    /* renamed from: a */
    private static void m9100a(JTextComponent jTextComponent, PropertiesUiFromCss aj) {
        Color color = aj.getColor();
        if (jTextComponent.getCaretColor() != color) {
            jTextComponent.setCaretColor(color);
        }
        m9097a((JComponent) jTextComponent, aj);
    }

    /* renamed from: a */
    private static void m9092a(Graphics graphics, PropertiesUiFromCss aj) {
        if (graphics instanceof RGraphics2) {
            Color colorMultiplier = aj.getColorMultiplier();
            RGraphics2 rGraphics2 = (RGraphics2) graphics;
            if (colorMultiplier != null && rGraphics2.getParent() != null) {
                rGraphics2.getParent().setColorMultiplier(colorMultiplier);
                rGraphics2.setColorMultiplier(Color.WHITE);
            }
        }
    }

    /* renamed from: a */
    public static void m9085a(IComponentUi aih, Graphics graphics, JComponent jComponent) {
        m9099a((ComponentUI) aih, graphics, jComponent);
        PropertiesUiFromCss Vp = aih.getComponentManager().mo13047Vp();
        if (Vp != null) {
            m9092a(graphics, Vp);
            m9097a(jComponent, Vp);
            m9093a(graphics, (Component) jComponent, Vp);
        }
    }

    /* renamed from: a */
    public static void m9093a(Graphics graphics, Component component, PropertiesUiFromCss aj) {
        m9094a(graphics, component, aj, (Rectangle) null);
    }

    /* renamed from: a */
    public static void m9094a(Graphics graphics, Component component, PropertiesUiFromCss aj, Rectangle rectangle) {
        int i;
        int i2;
        int i3;
        int i4;
        int width = component.getWidth();
        int height = component.getHeight();
        if (graphics instanceof RGraphics2) {
            if (aj.atj() != null) {
                Image atj = aj.atj();
                Rectangle clipBounds = graphics.getClipBounds();
                int width2 = atj.getWidth(component);
                int height2 = atj.getHeight(component);
                int i5 = width2 / 3;
                int i6 = height2 / 3;
                int i7 = width2 - i5;
                int i8 = height2 - i6;
                int i9 = clipBounds.x - (i5 / 2);
                int i10 = clipBounds.x + (i5 / 2);
                int i11 = (clipBounds.x + clipBounds.width) - (i5 / 2);
                int i12 = clipBounds.x + clipBounds.width + (i5 / 2);
                int i13 = clipBounds.y - (i6 / 2);
                int i14 = (i6 / 2) + clipBounds.y;
                int i15 = (clipBounds.y + clipBounds.height) - (i6 / 2);
                int i16 = clipBounds.y + clipBounds.height + (i6 / 2);
                graphics.setClip(i9, i13, clipBounds.width + i5, clipBounds.height + i6);
                graphics.drawImage(atj, i9, i13, i10, i14, 0, 0, i5, i6, (ImageObserver) null);
                graphics.drawImage(atj, i10, i13, i11, i14, i5, 0, i7, i6, (ImageObserver) null);
                graphics.drawImage(atj, i11, i13, i12, i14, i7, 0, width2, i6, (ImageObserver) null);
                graphics.drawImage(atj, i9, i14, i10, i15, 0, i6, i5, i8, (ImageObserver) null);
                graphics.drawImage(atj, i11, i14, i12, i15, i7, i6, width2, i8, (ImageObserver) null);
                graphics.drawImage(atj, i9, i15, i10, i16, 0, i8, i5, height2, (ImageObserver) null);
                graphics.drawImage(atj, i10, i15, i11, i16, i5, i8, i7, height2, (ImageObserver) null);
                graphics.drawImage(atj, i11, i15, i12, i16, i7, i8, width2, height2, (ImageObserver) null);
                graphics.setClip(clipBounds);
            }
            if (aj.ath() == Boolean.TRUE) {
                Insets a = m9083a(component, aj);
                ((RGraphics2) graphics).blur(a.left, a.top, (width - a.right) - a.left, (height - a.bottom) - a.top);
            }
        }
        int atT = aj.atT();
        if (atT > 0) {
            Insets a2 = m9083a(component, aj);
            if (rectangle == null) {
                i = a2.left;
                i2 = a2.top;
                i3 = width - a2.right;
                i4 = height - a2.bottom;
            } else {
                i = rectangle.x + a2.left;
                i2 = rectangle.y + a2.top;
                i3 = ((rectangle.width + i) - a2.right) - a2.left;
                i4 = ((rectangle.height + i2) - a2.top) - a2.bottom;
            }
            if ((graphics instanceof RGraphics2) && aj.ath() == Boolean.TRUE) {
                ((RGraphics2) graphics).blur(0, 0, width, height + 1);
            }
            for (int i17 = 0; i17 < atT; i17++) {
                Image fT = aj.mo428fT(i17);
                if (fT != null) {
                    Graphics graphics2 = graphics;
                    m9095a(graphics2, fT, aj.mo430fV(i17), aj.mo429fU(i17), i, i2, i3, i4, aj.atn(), component);
                }
            }
        }
    }

    /* renamed from: a */
    public static void m9095a(Graphics graphics, Image image, C3991xo xoVar, C2741jM jMVar, int i, int i2, int i3, int i4, Color color, ImageObserver imageObserver) {
        int i5;
        int i6;
        int jp;
        int width = image.getWidth(imageObserver);
        int height = image.getHeight(imageObserver);
        int i7 = i3 - i;
        int i8 = i4 - i2;
        if (jMVar != C2741jM.apd) {
            C1366Tu Iu = jMVar.mo19908Iu();
            if (Iu.fEH == C1366Tu.C1367a.PERCENT) {
                jp = ((int) Math.ceil((double) (Iu.mo5756jp((float) i7) - Iu.mo5756jp((float) width)))) + i;
                Iu.mo5756jp((float) width);
            } else {
                Iu.mo5756jp((float) i7);
                jp = Iu.mo5756jp((float) i7) + i;
            }
            C1366Tu Iv = jMVar.mo19909Iv();
            if (Iv.fEH == C1366Tu.C1367a.PERCENT) {
                i6 = i2 + ((int) Math.ceil((double) (Iv.mo5756jp((float) i8) - Iv.mo5756jp((float) height))));
                Iu.mo5756jp((float) width);
                i5 = jp;
            } else {
                Iu.mo5756jp((float) i8);
                i6 = i2 + Iv.mo5756jp((float) i8);
                i5 = jp;
            }
        } else {
            i5 = i;
            i6 = i2;
        }
        if (xoVar == null) {
            xoVar = C3991xo.STRETCH;
        }
        switch (brE()[xoVar.ordinal()]) {
            case 1:
                graphics.drawImage(image, i, i2, i3, i4, 0, 0, (int) ((((float) (i3 - i)) / ((float) width)) * ((float) width)), (int) ((((float) (i4 - i2)) / ((float) height)) * ((float) height)), color, imageObserver);
                return;
            case 2:
                int min = Math.min(i4 - i6, height);
                int i9 = (int) ((((float) (i3 - i)) / ((float) width)) * ((float) width));
                graphics.drawImage(image, i, i6, i3, i6 + min, 0, 0, i9, min, color, imageObserver);
                return;
            case 3:
                int min2 = Math.min(i3 - i5, width);
                Graphics graphics2 = graphics;
                Image image2 = image;
                int i10 = i2;
                graphics2.drawImage(image2, i5, i10, i5 + min2, i4, 0, 0, min2, (int) ((((float) (i4 - i2)) / ((float) height)) * ((float) height)), color, imageObserver);
                return;
            case 4:
                int min3 = Math.min(i3 - i5, width);
                int min4 = Math.min(i4 - i6, height);
                graphics.drawImage(image, i5, i6, i5 + min3, i6 + min4, 0, 0, min3, min4, color, imageObserver);
                return;
            case 5:
                graphics.drawImage(image, i, i2, i3, i4, 0, 0, width, height, color, imageObserver);
                return;
            case 6:
                int min5 = Math.min(i4 - i6, height);
                graphics.drawImage(image, i, i6, i3, i6 + min5, 0, 0, width, min5, color, imageObserver);
                return;
            case 7:
                int min6 = Math.min(i3 - i5, width);
                graphics.drawImage(image, i5, i2, i5 + min6, i4, 0, 0, min6, height, color, imageObserver);
                return;
            case 8:
                graphics.drawImage(image, i, i2, i3, i4, 0, 0, width, (int) ((((float) (i4 - i2)) / ((float) height)) * ((float) height)), color, imageObserver);
                return;
            case 9:
                graphics.drawImage(image, i, i2, i3, i4, 0, 0, (int) ((((float) (i3 - i)) / ((float) width)) * ((float) width)), height, color, imageObserver);
                return;
            default:
                return;
        }
    }

    /* renamed from: a */
    private static Insets m9083a(Component component, PropertiesUiFromCss aj) {
        if (!(component instanceof JComponent)) {
            return BorderWrapper.bfB().mo3848a(aj, component);
        }
        BorderWrapper border = (BorderWrapper) ((JComponent) component).getBorder();
        if (border instanceof BorderWrapper) {
            return border.mo3848a(aj, component);
        }
        return ((JComponent) component).getInsets();
    }

    /* renamed from: a */
    public static void m9084a(IComponentUi aih, Graphics graphics, AbstractButton abstractButton) {
        PropertiesUiFromCss Vp = aih.getComponentManager().mo13047Vp();
        if (Vp != null) {
            m9092a(graphics, Vp);
            m9096a(abstractButton, Vp);
            m9093a(graphics, (Component) abstractButton, Vp);
        }
        m9099a((ComponentUI) aih, graphics, (JComponent) abstractButton);
    }

    /* renamed from: a */
    public static void m9087a(IComponentUi aih, Graphics graphics, JTextComponent jTextComponent) {
        PropertiesUiFromCss Vp = aih.getComponentManager().mo13047Vp();
        if (Vp != null) {
            m9092a(graphics, Vp);
            m9100a(jTextComponent, Vp);
            m9093a(graphics, (Component) jTextComponent, Vp);
        }
        m9099a((ComponentUI) aih, graphics, (JComponent) jTextComponent);
    }

    /* renamed from: a */
    public static void m9086a(IComponentUi aih, Graphics graphics, JLabel jLabel) {
        PropertiesUiFromCss Vp = aih.getComponentManager().mo13047Vp();
        if (Vp != null) {
            m9092a(graphics, Vp);
            m9098a(jLabel, Vp);
            m9093a(graphics, (Component) jLabel, Vp);
        }
        m9099a((ComponentUI) aih, graphics, (JComponent) jLabel);
    }

    public static int hashCode(Object obj) {
        if (obj == null) {
            return 0;
        }
        return obj.hashCode();
    }

    /* renamed from: nd */
    public static int m9106nd(int i) {
        return i;
    }

    /* renamed from: l */
    public static int m9105l(short s) {
        return s;
    }

    /* renamed from: a */
    public static int m9082a(byte b) {
        return b;
    }

    /* renamed from: dp */
    public static int m9101dp(boolean z) {
        return z ? 1 : 0;
    }

    /* renamed from: hs */
    public static int m9104hs(float f) {
        return Float.floatToIntBits(f);
    }

    /* renamed from: D */
    public static int m9081D(double d) {
        long doubleToLongBits = Double.doubleToLongBits(d);
        return (int) (doubleToLongBits ^ (doubleToLongBits >>> 32));
    }

    /* renamed from: fF */
    public static int m9103fF(long j) {
        return (int) ((j >>> 32) ^ j);
    }
}
