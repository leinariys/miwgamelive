package logic.swing;

/* renamed from: a.jM */
/* compiled from: a */
public class C2741jM {
    public static final C2741jM apd = new C2741jM(C1366Tu.fEC, C1366Tu.fEC);
    public static final C2741jM ape = new C2741jM(C1366Tu.fEE, C1366Tu.fEE);
    public static final C2741jM apf = new C2741jM(C1366Tu.fEF, C1366Tu.fEF);
    final C1366Tu apg;
    final C1366Tu aph;

    public C2741jM(C1366Tu tu, C1366Tu tu2) {
        this.apg = tu;
        this.aph = tu2;
    }

    /* renamed from: Iu */
    public C1366Tu mo19908Iu() {
        return this.apg;
    }

    /* renamed from: Iv */
    public C1366Tu mo19909Iv() {
        return this.aph;
    }

    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof C2741jM)) {
            return super.equals(obj);
        }
        C2741jM jMVar = (C2741jM) obj;
        if (this.apg == jMVar.apg || (this.apg != null && this.apg.equals(jMVar.aph))) {
            if (this.aph == jMVar.aph) {
                return true;
            }
            if (this.aph == null || !this.aph.equals(jMVar.aph)) {
                return false;
            }
            return true;
        }
        return false;
    }

    public int hashCode() {
        int i;
        int i2 = 0;
        if (this.apg != null) {
            i = this.apg.hashCode();
        } else {
            i = 0;
        }
        int i3 = i * 31;
        if (this.aph != null) {
            i2 = this.aph.hashCode();
        }
        return i3 + i2;
    }

    public String toString() {
        return this.apg + " " + this.aph;
    }
}
