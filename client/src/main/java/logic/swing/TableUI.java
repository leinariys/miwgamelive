package logic.swing;

import logic.ui.ComponentManager;
import logic.ui.IComponentManager;
import logic.ui.Panel;
import logic.ui.PropertiesUiFromCss;

import javax.swing.*;
import javax.swing.plaf.ComponentUI;
import javax.swing.plaf.basic.BasicTableUI;
import javax.swing.table.JTableHeader;
import javax.swing.table.TableColumn;
import javax.swing.table.TableColumnModel;
import java.awt.*;

/* renamed from: a.ahX  reason: case insensitive filesystem */
/* compiled from: a */
public class TableUI extends BasicTableUI implements IComponentUi {

    /* renamed from: Rp */
    private ComponentManager f4561Rp;
    private Panel fLT;

    public TableUI(JTable jTable) {
        this.f4561Rp = new ComponentManager("table", jTable);
        jTable.setOpaque(false);
        jTable.setBackground(new Color(0, 0, 0, 0));
    }

    public static ComponentUI createUI(JComponent jComponent) {
        return new TableUI((JTable) jComponent);
    }

    public void installUI(JComponent jComponent) {
        this.table = (JTable) jComponent;
        this.rendererPane = new C6621aqV("cell");
        this.fLT = new C1893a();
        this.table.add(this.rendererPane);
        this.table.add(this.fLT);
        installDefaults();
        installListeners();
        installKeyboardActions();
    }

    public void paint(Graphics graphics, JComponent jComponent) {
        int i;
        Rectangle clipBounds = graphics.getClipBounds();
        Rectangle bounds = this.table.getBounds();
        bounds.y = 0;
        bounds.x = 0;
        if (this.table.getRowCount() > 0 && this.table.getColumnCount() > 0 && bounds.intersects(clipBounds)) {
            boolean isLeftToRight = this.table.getComponentOrientation().isLeftToRight();
            Point location = clipBounds.getLocation();
            if (!isLeftToRight) {
                location.x++;
            }
            Point point = new Point((clipBounds.width + clipBounds.x) - (isLeftToRight ? 1 : 0), clipBounds.height + clipBounds.y);
            int rowAtPoint = this.table.rowAtPoint(location);
            int rowAtPoint2 = this.table.rowAtPoint(point);
            if (rowAtPoint == -1) {
                rowAtPoint = 0;
            }
            if (rowAtPoint2 == -1) {
                rowAtPoint2 = this.table.getRowCount() - 1;
            }
            int columnAtPoint = this.table.columnAtPoint(isLeftToRight ? location : point);
            JTable jTable = this.table;
            if (!isLeftToRight) {
                point = location;
            }
            int columnAtPoint2 = jTable.columnAtPoint(point);
            if (columnAtPoint == -1) {
                i = 0;
            } else {
                i = columnAtPoint;
            }
            if (columnAtPoint2 == -1) {
                columnAtPoint2 = this.table.getColumnCount() - 1;
            }
            AdapterUiCss.m9085a((IComponentUi) this, graphics, jComponent);
            m22251a(graphics, rowAtPoint, rowAtPoint2, i, columnAtPoint2);
            m22255b(graphics, rowAtPoint, rowAtPoint2, i, columnAtPoint2);
            m22250a(graphics);
        }
    }

    /* renamed from: a */
    private void m22251a(Graphics graphics, int i, int i2, int i3, int i4) {
        int i5;
        graphics.setColor(this.table.getGridColor());
        Rectangle union = this.table.getCellRect(i, 0, false).union(this.table.getCellRect(i2, this.table.getColumnCount() - 1, false));
        while (i <= i2) {
            IComponentManager e = ComponentManager.getCssHolder(this.fLT);
            if (this.table.isRowSelected(i)) {
                i5 = 2;
            } else {
                i5 = 0;
            }
            e.mo13063q(2, i5);
            union.height = this.table.getRowHeight(i);
            AdapterUiCss.m9094a(graphics, this.fLT, PropertiesUiFromCss.m462g(this.fLT), union);
            this.fLT.getBorder().paintBorder(this.fLT, graphics, union.x, union.y, union.width, union.height);
            union.y += union.height;
            i++;
        }
    }

    /* renamed from: b */
    private void m22255b(Graphics graphics, int i, int i2, int i3, int i4) {
        TableColumn draggedColumn;
        JTableHeader tableHeader = this.table.getTableHeader();
        if (tableHeader == null) {
            draggedColumn = null;
        } else {
            draggedColumn = tableHeader.getDraggedColumn();
        }
        TableColumnModel columnModel = this.table.getColumnModel();
        int columnMargin = columnModel.getColumnMargin();
        if (this.table.getComponentOrientation().isLeftToRight()) {
            for (int i5 = i; i5 <= i2; i5++) {
                Rectangle cellRect = this.table.getCellRect(i5, i3, false);
                for (int i6 = i3; i6 <= i4; i6++) {
                    TableColumn column = columnModel.getColumn(i6);
                    int width = column.getWidth();
                    cellRect.width = width - columnMargin;
                    if (column != draggedColumn) {
                        m22253a(graphics, cellRect, i5, i6);
                    }
                    cellRect.x += width;
                }
            }
        } else {
            for (int i7 = i; i7 <= i2; i7++) {
                Rectangle cellRect2 = this.table.getCellRect(i7, i3, false);
                TableColumn column2 = columnModel.getColumn(i3);
                if (column2 != draggedColumn) {
                    cellRect2.width = column2.getWidth() - columnMargin;
                    m22253a(graphics, cellRect2, i7, i3);
                }
                for (int i8 = i3 + 1; i8 <= i4; i8++) {
                    TableColumn column3 = columnModel.getColumn(i8);
                    int width2 = column3.getWidth();
                    cellRect2.width = width2 - columnMargin;
                    cellRect2.x -= width2;
                    if (column3 != draggedColumn) {
                        m22253a(graphics, cellRect2, i7, i8);
                    }
                }
            }
        }
        if (draggedColumn != null) {
            m22252a(graphics, i, i2, draggedColumn, tableHeader.getDraggedDistance());
        }
        this.rendererPane.removeAll();
    }

    /* renamed from: a */
    private void m22253a(Graphics graphics, Rectangle rectangle, int i, int i2) {
        if (this.table.isEditing() && this.table.getEditingRow() == i && this.table.getEditingColumn() == i2) {
            this.table.getEditorComponent().setBounds(rectangle);
            return;
        }
        Graphics graphics2 = graphics;
        this.rendererPane.paintComponent(graphics2, this.table.prepareRenderer(this.table.getCellRenderer(i, i2), i, i2), this.table, rectangle.x, rectangle.y, rectangle.width, rectangle.height, true);
    }

    /* renamed from: a */
    private void m22252a(Graphics graphics, int i, int i2, TableColumn tableColumn, int i3) {
        int a = m22247a(tableColumn);
        Rectangle union = this.table.getCellRect(i, a, true).union(this.table.getCellRect(i2, a, true));
        PropertiesUiFromCss Vp = this.f4561Rp.mo13047Vp();
        if (Vp != null) {
            graphics.setColor(Vp.getColor());
        }
        graphics.setColor(new Color(0.4f, 0.4f, 0.4f, 0.4f));
        graphics.fillRect(union.x, union.y, union.width, union.height);
        union.x += i3;
        graphics.setColor(new Color(0.4f, 0.4f, 0.4f, 0.4f));
        graphics.fillRect(union.x, union.y, union.width, union.height);
        if (this.table.getShowVerticalLines()) {
            graphics.setColor(this.table.getGridColor());
            int i4 = union.x;
            int i5 = union.y;
            int i6 = (union.width + i4) - 1;
            int i7 = (union.height + i5) - 1;
            graphics.drawLine(i4 - 1, i5, i4 - 1, i7);
            graphics.drawLine(i6, i5, i6, i7);
        }
    }

    /* renamed from: a */
    private void m22250a(Graphics graphics) {
        JTable.DropLocation dropLocation = this.table.getDropLocation();
        if (dropLocation != null) {
            Color color = UIManager.getColor("Table.dropLineColor");
            Color color2 = UIManager.getColor("Table.dropLineShortColor");
            if (color != null || color2 != null) {
                Rectangle a = m22249a(dropLocation);
                if (a != null) {
                    int i = a.x;
                    int i2 = a.width;
                    if (color != null) {
                        m22248a(a, true);
                        graphics.setColor(color);
                        graphics.fillRect(a.x, a.y, a.width, a.height);
                    }
                    if (!dropLocation.isInsertColumn() && color2 != null) {
                        graphics.setColor(color2);
                        graphics.fillRect(i, a.y, i2, a.height);
                    }
                }
                Rectangle b = m22254b(dropLocation);
                if (b != null) {
                    int i3 = b.y;
                    int i4 = b.height;
                    if (color != null) {
                        m22248a(b, false);
                        graphics.setColor(color);
                        graphics.fillRect(b.x, b.y, b.width, b.height);
                    }
                    if (!dropLocation.isInsertRow() && color2 != null) {
                        graphics.setColor(color2);
                        graphics.fillRect(b.x, i3, b.width, i4);
                    }
                }
            }
        }
    }

    /* renamed from: a */
    private Rectangle m22249a(JTable.DropLocation dropLocation) {
        if (!dropLocation.isInsertRow()) {
            return null;
        }
        int row = dropLocation.getRow();
        int column = dropLocation.getColumn();
        if (column >= this.table.getColumnCount()) {
            column--;
        }
        Rectangle cellRect = this.table.getCellRect(row, column, true);
        if (row >= this.table.getRowCount()) {
            Rectangle cellRect2 = this.table.getCellRect(row - 1, column, true);
            cellRect.y = cellRect2.height + cellRect2.y;
        }
        if (cellRect.y == 0) {
            cellRect.y = -1;
        } else {
            cellRect.y -= 2;
        }
        cellRect.height = 3;
        return cellRect;
    }

    /* renamed from: b */
    private Rectangle m22254b(JTable.DropLocation dropLocation) {
        if (!dropLocation.isInsertColumn()) {
            return null;
        }
        boolean isLeftToRight = this.table.getComponentOrientation().isLeftToRight();
        int column = dropLocation.getColumn();
        Rectangle cellRect = this.table.getCellRect(dropLocation.getRow(), column, true);
        if (column >= this.table.getColumnCount()) {
            cellRect = this.table.getCellRect(dropLocation.getRow(), column - 1, true);
            if (isLeftToRight) {
                cellRect.x += cellRect.width;
            }
        } else if (!isLeftToRight) {
            cellRect.x += cellRect.width;
        }
        if (cellRect.x == 0) {
            cellRect.x = -1;
        } else {
            cellRect.x -= 2;
        }
        cellRect.width = 3;
        return cellRect;
    }

    /* renamed from: a */
    private int m22247a(TableColumn tableColumn) {
        TableColumnModel columnModel = this.table.getColumnModel();
        for (int i = 0; i < columnModel.getColumnCount(); i++) {
            if (columnModel.getColumn(i) == tableColumn) {
                return i;
            }
        }
        return -1;
    }

    /* renamed from: a */
    private Rectangle m22248a(Rectangle rectangle, boolean z) {
        if (rectangle != null) {
            if (z) {
                rectangle.x = 0;
                rectangle.width = this.table.getWidth();
            } else {
                rectangle.y = 0;
                if (this.table.getRowCount() != 0) {
                    Rectangle cellRect = this.table.getCellRect(this.table.getRowCount() - 1, 0, true);
                    rectangle.height = cellRect.height + cellRect.y;
                } else {
                    rectangle.height = this.table.getHeight();
                }
            }
        }
        return rectangle;
    }

    public Dimension getPreferredSize(JComponent jComponent) {
        return getComponentManager().mo13059c(jComponent, TableUI.super.getPreferredSize(jComponent));
    }

    public Dimension getMinimumSize(JComponent jComponent) {
        return getComponentManager().mo13057b(jComponent, TableUI.super.getMinimumSize(jComponent));
    }

    public Dimension getMaximumSize(JComponent jComponent) {
        return getComponentManager().mo13052a(jComponent, TableUI.super.getMaximumSize(jComponent));
    }

    /* renamed from: wy */
    public ComponentManager getComponentManager() {
        return this.f4561Rp;
    }

    /* renamed from: a.ahX$a */
    class C1893a extends Panel {


        C1893a() {
        }

        public String getElementName() {
            return "row";
        }
    }
}
