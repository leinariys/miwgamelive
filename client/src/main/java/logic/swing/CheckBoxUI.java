package logic.swing;

import logic.ui.ComponentManager;

import javax.swing.*;
import javax.swing.plaf.ComponentUI;
import javax.swing.plaf.basic.BasicButtonListener;
import javax.swing.plaf.basic.BasicCheckBoxUI;
import java.awt.*;

/* renamed from: a.pM */
/* compiled from: a */
public class CheckBoxUI extends BasicCheckBoxUI implements IComponentUi {

    /* renamed from: Rp */
    private ComponentManager f8817Rp;

    public CheckBoxUI(JCheckBox jCheckBox) {
        jCheckBox.setRolloverEnabled(true);
        jCheckBox.setOpaque(false);
        this.f8817Rp = new C1401UY("checkbox", jCheckBox);
    }

    public static ComponentUI createUI(JComponent jComponent) {
        return new CheckBoxUI((JCheckBox) jComponent);
    }

    /* access modifiers changed from: protected */
    public BasicButtonListener createButtonListener(AbstractButton abstractButton) {
        return new C5371aGt(abstractButton, this.f8817Rp);
    }

    public void update(Graphics graphics, JComponent jComponent) {
        AdapterUiCss.m9084a((IComponentUi) this, graphics, (AbstractButton) jComponent);
        CheckBoxUI.super.paint(graphics, jComponent);
    }

    public void paint(Graphics graphics, JComponent jComponent) {
        CheckBoxUI.super.paint(graphics, jComponent);
    }

    public Dimension getPreferredSize(JComponent jComponent) {
        AdapterUiCss.m9088a((IComponentUi) this, (AbstractButton) jComponent);
        return getComponentManager().mo13059c(jComponent, CheckBoxUI.super.getPreferredSize(jComponent));
    }

    public Dimension getMinimumSize(JComponent jComponent) {
        AdapterUiCss.m9088a((IComponentUi) this, (AbstractButton) jComponent);
        return getComponentManager().mo13057b(jComponent, CheckBoxUI.super.getMinimumSize(jComponent));
    }

    public Dimension getMaximumSize(JComponent jComponent) {
        AdapterUiCss.m9088a((IComponentUi) this, (AbstractButton) jComponent);
        return getComponentManager().mo13057b(jComponent, CheckBoxUI.super.getMaximumSize(jComponent));
    }

    /* renamed from: wy */
    public ComponentManager getComponentManager() {
        return this.f8817Rp;
    }
}
