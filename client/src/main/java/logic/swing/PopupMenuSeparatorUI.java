package logic.swing;

import logic.ui.ComponentManager;

import javax.swing.*;
import javax.swing.plaf.ComponentUI;
import javax.swing.plaf.basic.BasicPopupMenuSeparatorUI;
import java.awt.*;

/* renamed from: a.WQ */
/* compiled from: a */
public class PopupMenuSeparatorUI extends BasicPopupMenuSeparatorUI implements IComponentUi {

    /* renamed from: Rp */
    private ComponentManager f2030Rp;

    public PopupMenuSeparatorUI(JComponent jComponent) {
        this.f2030Rp = new ComponentManager("popupmenuseparator", jComponent);
        jComponent.setOpaque(false);
    }

    public static ComponentUI createUI(JComponent jComponent) {
        return new PopupMenuSeparatorUI(jComponent);
    }

    public void update(Graphics graphics, JComponent jComponent) {
        AdapterUiCss.m9085a((IComponentUi) this, graphics, jComponent);
        paint(graphics, jComponent);
    }

    public Dimension getPreferredSize(JComponent jComponent) {
        AdapterUiCss.m9089a((IComponentUi) this, jComponent);
        return getComponentManager().mo13059c(jComponent, PopupMenuSeparatorUI.super.getPreferredSize(jComponent));
    }

    public Dimension getMinimumSize(JComponent jComponent) {
        AdapterUiCss.m9089a((IComponentUi) this, jComponent);
        return getComponentManager().mo13057b(jComponent, PopupMenuSeparatorUI.super.getMinimumSize(jComponent));
    }

    public Dimension getMaximumSize(JComponent jComponent) {
        AdapterUiCss.m9089a((IComponentUi) this, jComponent);
        return getComponentManager().mo13052a(jComponent, PopupMenuSeparatorUI.super.getMaximumSize(jComponent));
    }

    /* renamed from: wy */
    public ComponentManager getComponentManager() {
        return this.f2030Rp;
    }
}
