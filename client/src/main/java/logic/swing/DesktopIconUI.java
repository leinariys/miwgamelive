package logic.swing;

import logic.ui.ComponentManager;

import javax.swing.*;
import javax.swing.plaf.ComponentUI;
import javax.swing.plaf.basic.BasicDesktopIconUI;
import java.awt.*;

/* renamed from: a.atY  reason: case insensitive filesystem */
/* compiled from: a */
public class DesktopIconUI extends BasicDesktopIconUI implements IComponentUi {

    /* renamed from: Rp */
    private ComponentManager f5320Rp;

    public DesktopIconUI(JInternalFrame.JDesktopIcon jDesktopIcon) {
        this.f5320Rp = new ComponentManager("desktopicon", jDesktopIcon);
        jDesktopIcon.setOpaque(false);
    }

    public static ComponentUI createUI(JComponent jComponent) {
        return new DesktopIconUI((JInternalFrame.JDesktopIcon) jComponent);
    }

    public void update(Graphics graphics, JComponent jComponent) {
        AdapterUiCss.m9085a((IComponentUi) this, graphics, jComponent);
        paint(graphics, jComponent);
    }

    public Dimension getPreferredSize(JComponent jComponent) {
        AdapterUiCss.m9089a((IComponentUi) this, jComponent);
        return getComponentManager().mo13059c(jComponent, DesktopIconUI.super.getPreferredSize(jComponent));
    }

    public Dimension getMinimumSize(JComponent jComponent) {
        AdapterUiCss.m9089a((IComponentUi) this, jComponent);
        return getComponentManager().mo13057b(jComponent, DesktopIconUI.super.getMinimumSize(jComponent));
    }

    public Dimension getMaximumSize(JComponent jComponent) {
        AdapterUiCss.m9089a((IComponentUi) this, jComponent);
        return getComponentManager().mo13052a(jComponent, DesktopIconUI.super.getMaximumSize(jComponent));
    }

    /* renamed from: wy */
    public ComponentManager getComponentManager() {
        return this.f5320Rp;
    }

    /* access modifiers changed from: protected */
    public void installComponents() {
        this.iconPane = new C2391el(this.frame);
        this.desktopIcon.setLayout(new BorderLayout());
        this.desktopIcon.add(this.iconPane, "Center");
    }
}
