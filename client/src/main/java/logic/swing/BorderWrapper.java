package logic.swing;

import logic.ui.PropertiesUiFromCss;

import javax.swing.border.Border;
import javax.swing.plaf.InsetsUIResource;
import java.awt.*;
import java.awt.image.ImageObserver;

/* renamed from: a.Lu */
/* compiled from: a */
public class BorderWrapper implements Border {

    public static Insets duz = new InsetsUIResource(0, 0, 0, 0);
    private static BorderWrapper currentInstance = null;
    private static C2741jM duB = new C2741jM(new C1366Tu(0.0f, C1366Tu.C1367a.PERCENT), new C1366Tu(0.0f, C1366Tu.C1367a.PERCENT));
    private static C2741jM duC = new C2741jM(new C1366Tu(0.0f, C1366Tu.C1367a.PERCENT), new C1366Tu(0.0f, C1366Tu.C1367a.PERCENT));
    private static C2741jM duD = new C2741jM(new C1366Tu(100.0f, C1366Tu.C1367a.PERCENT), new C1366Tu(0.0f, C1366Tu.C1367a.PERCENT));
    private static C2741jM duE = new C2741jM(new C1366Tu(0.0f, C1366Tu.C1367a.PERCENT), new C1366Tu(100.0f, C1366Tu.C1367a.PERCENT));

    public static Border getTextFieldBorder() {
        return new BorderWrapper();
    }

    public static BorderWrapper bfB() {
        if (currentInstance == null) {
            currentInstance = new BorderWrapper();
        }
        return currentInstance;
    }
    @Override
    ////Вызывается когда в class BaseUItegXML компонет добовляется в рут панель var6.add(var4);
    public Insets getBorderInsets(Component component) {
        PropertiesUiFromCss g = PropertiesUiFromCss.m462g(component);
        if (g == null) {
            return new Insets(0, 0, 0, 0);
        }
        int width = component.getWidth();
        int height = component.getHeight();
        Insets a = mo3848a(g, component);
        a.left += g.atK().mo5756jp((float) width);
        a.right = g.atL().mo5756jp((float) width) + a.right;
        a.top += g.atM().mo5756jp((float) height);
        a.bottom = g.atJ().mo5756jp((float) height) + a.bottom;
        return a;
    }

    /* renamed from: a */
    public Insets mo3848a(PropertiesUiFromCss aj, Component component) {
        int i;
        int width = component.getWidth();
        int height = component.getHeight();
        if (aj == null) {
            return new Insets(0, 0, 0, 0);
        }
        int jp = aj.atC().mo5756jp((float) width);
        int jp2 = aj.atD().mo5756jp((float) width);
        int jp3 = aj.atE().mo5756jp((float) height);
        int jp4 = aj.atB().mo5756jp((float) height);
        if ("compound".equals(aj.getBorderStyle())) {
            Image atv = aj.atv();
            Image atw = aj.atw();
            Image att = aj.att();
            Image ato = aj.ato();
            Image atq = aj.atq();
            Image atl = aj.atl();
            Image atm = aj.atm();
            Image ati = aj.ati();
            int i2 = 0;
            int i3 = 0;
            int i4 = 0;
            if (atv != null) {
                i2 = atv.getHeight(component);
            }
            if (att != null) {
                i3 = att.getHeight(component);
            }
            if (atw != null) {
                i4 = atw.getHeight(component);
            }
            int max = Math.max(i2, Math.max(i3, i4));
            int i5 = 0;
            int i6 = 0;
            int i7 = 0;
            if (atl != null) {
                i5 = atl.getHeight(component);
            }
            if (ati != null) {
                i6 = ati.getHeight(component);
            }
            if (atm != null) {
                i7 = atm.getHeight(component);
            }
            int max2 = Math.max(i5, Math.max(i6, i7));
            int i8 = 0;
            int i9 = 0;
            if (ato != null) {
                i8 = ato.getWidth(component);
            }
            if (atq != null) {
                i9 = atq.getWidth(component);
            }
            jp += i8;
            jp2 += i9;
            jp3 += max;
            i = jp4 + max2;
        } else {
            i = jp4;
        }
        return new Insets(jp3, jp, i, jp2);
    }

    @Override
    public boolean isBorderOpaque() {
        return false;
    }

    @Override
    public void paintBorder(Component component, Graphics graphics, int i, int i2, int i3, int i4) {
        int i5;
        int i6;
        int i7;
        int i8;
        int i9;
        int i10;
        int i11;
        int i12;
        int i13;
        PropertiesUiFromCss g = PropertiesUiFromCss.m462g(component);
        if (g != null && "compound".equals(g.getBorderStyle())) {
            Image atv = g.atv();
            Image att = g.att();
            Image atw = g.atw();
            Image ato = g.ato();
            Image atq = g.atq();
            Image atl = g.atl();
            Image ati = g.ati();
            Image atm = g.atm();
            int width = component.getWidth();
            int height = component.getHeight();
            int jp = g.atC().mo5756jp((float) width);
            int jp2 = g.atD().mo5756jp((float) width);
            int jp3 = g.atE().mo5756jp((float) height);
            int i14 = (i3 - jp) - jp2;
            int jp4 = (i4 - jp3) - g.atB().mo5756jp((float) height);
            int i15 = i + jp;
            int i16 = i2 + jp3;
            graphics.translate(i15, i16);
            int i17 = 0;
            int i18 = 0;
            if (atv != null) {
                i17 = atv.getWidth(component);
                i18 = atv.getHeight(component);
            }
            if (att != null) {
                i5 = att.getHeight(component);
            } else {
                i5 = 0;
            }
            if (atw != null) {
                int width2 = atw.getWidth(component);
                i6 = atw.getHeight(component);
                i7 = width2;
            } else {
                i6 = 0;
                i7 = 0;
            }
            int max = Math.max(i18, Math.max(i5, i6));
            int i19 = 0;
            if (atl != null) {
                int width3 = atl.getWidth(component);
                i8 = atl.getHeight(component);
                i9 = width3;
            } else {
                i8 = 0;
                i9 = 0;
            }
            if (ati != null) {
                i19 = ati.getHeight(component);
            }
            if (atm != null) {
                int width4 = atm.getWidth(component);
                i10 = atm.getHeight(component);
                i11 = width4;
            } else {
                i10 = 0;
                i11 = 0;
            }
            int max2 = Math.max(i8, Math.max(i19, i10));
            if (ato != null) {
                i12 = ato.getWidth(component);
            } else {
                i12 = 0;
            }
            if (atq != null) {
                i13 = atq.getWidth(component);
            } else {
                i13 = 0;
            }
            Color atn = g.atn();
            if (atv != null) {
                graphics.drawImage(atv, 0, max - i18, i17, max, 0, 0, i17, i18, atn, component);
            }
            if (att != null) {
                m6874a(graphics, att, g.atu(), duB, i17, max - i5, i14 - i7, max, atn, component);
            }
            if (atw != null) {
                graphics.drawImage(atw, i14 - i7, max - i6, i14, max, 0, 0, i7, i6, atn, component);
            }
            if (ato != null) {
                m6874a(graphics, ato, g.atp(), duC, 0, max, i12, jp4 - max2, atn, component);
            }
            if (atq != null) {
                m6874a(graphics, atq, g.atr(), duD, i14 - i13, max, i14, jp4 - max2, atn, component);
            }
            if (atl != null) {
                graphics.drawImage(atl, 0, jp4 - max2, i9, jp4, 0, 0, i9, i8, atn, component);
            }
            if (ati != null) {
                m6874a(graphics, ati, g.atk(), duE, i9, jp4 - max2, i14 - i11, jp4, atn, component);
            }
            if (atm != null) {
                graphics.drawImage(atm, i14 - i11, jp4 - max2, i14, jp4, 0, 0, i11, i10, atn, component);
            }
            graphics.translate(-i15, -i16);
        }
    }

    /* renamed from: a */
    private void m6874a(Graphics graphics, Image image, C3991xo xoVar, C2741jM jMVar, int i, int i2, int i3, int i4, Color color, ImageObserver imageObserver) {
        AdapterUiCss.m9095a(graphics, image, xoVar, jMVar, i, i2, i3, i4, color, imageObserver);
    }
}
