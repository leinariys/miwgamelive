package logic.swing;

import logic.ui.ComponentManager;

import javax.swing.*;
import javax.swing.plaf.ComponentUI;
import javax.swing.plaf.basic.BasicDesktopPaneUI;
import java.awt.*;

/* renamed from: a.aBn  reason: case insensitive filesystem */
/* compiled from: a */
public class DesktopPaneUI extends BasicDesktopPaneUI implements IComponentUi {

    /* renamed from: Rp */
    private ComponentManager f2441Rp;

    public DesktopPaneUI(JDesktopPane jDesktopPane) {
        this.f2441Rp = new ComponentManager("desktoppane", jDesktopPane);
        jDesktopPane.setOpaque(false);
    }

    public static ComponentUI createUI(JComponent jComponent) {
        return new DesktopPaneUI((JDesktopPane) jComponent);
    }

    public void update(Graphics graphics, JComponent jComponent) {
        paint(graphics, jComponent);
    }

    public void paint(Graphics graphics, JComponent jComponent) {
        AdapterUiCss.m9085a((IComponentUi) this, graphics, jComponent);
        AdapterUiCss.m9099a((ComponentUI) this, graphics, jComponent);
        DesktopPaneUI.super.paint(graphics, jComponent);
        AdapterUiCss.m9099a((ComponentUI) this, graphics, jComponent);
    }

    public Dimension getPreferredSize(JComponent jComponent) {
        return getComponentManager().mo13059c(jComponent, DesktopPaneUI.super.getPreferredSize(jComponent));
    }

    public Dimension getMinimumSize(JComponent jComponent) {
        return getComponentManager().mo13057b(jComponent, DesktopPaneUI.super.getMinimumSize(jComponent));
    }

    public Dimension getMaximumSize(JComponent jComponent) {
        return getComponentManager().mo13052a(jComponent, DesktopPaneUI.super.getMaximumSize(jComponent));
    }

    /* renamed from: wy */
    public ComponentManager getComponentManager() {
        return this.f2441Rp;
    }

    public boolean contains(JComponent jComponent, int i, int i2) {
        if (ComponentManager.getCssHolder(jComponent).mo13050Vs()) {
            return DesktopPaneUI.super.contains(jComponent, i, i2);
        }
        return false;
    }
}
