package logic.swing;

import logic.ui.ComponentManager;

import javax.swing.*;

/* renamed from: a.UY */
/* compiled from: a */
public class C1401UY extends ComponentManager {
    public C1401UY(String str, AbstractButton abstractButton) {
        super(str, abstractButton);
    }

    /* renamed from: Vi */
    public int mo5877Vi() {
        int i;
        int i2;
        int i3;
        int Vi = super.mo5877Vi() & -64;
        ButtonModel model = ((AbstractButton) getCurrentComponent()).getModel();
        if (model.isArmed()) {
            i = Vi | 1;
        } else {
            i = Vi;
        }
        if (model.isSelected()) {
            i |= 2;
        }
        if (model.isEnabled()) {
            i2 = i | 4;
        } else {
            i2 = i | 8;
        }
        if (model.isRollover()) {
            i3 = i2 | 32;
        } else {
            i3 = i2 & -33;
        }
        if (model.isPressed()) {
            return i3 | 16;
        }
        return i3;
    }
}
