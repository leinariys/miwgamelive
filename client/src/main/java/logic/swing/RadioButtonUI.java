package logic.swing;

import logic.ui.ComponentManager;

import javax.swing.*;
import javax.swing.plaf.ComponentUI;
import javax.swing.plaf.basic.BasicButtonListener;
import javax.swing.plaf.basic.BasicRadioButtonUI;
import java.awt.*;

/* renamed from: a.NA */
/* compiled from: a */
public class RadioButtonUI extends BasicRadioButtonUI implements IComponentUi {

    /* renamed from: Rp */
    private ComponentManager f1189Rp;

    public RadioButtonUI(JRadioButton jRadioButton) {
        jRadioButton.setBorderPainted(false);
        jRadioButton.setRolloverEnabled(true);
        jRadioButton.setOpaque(false);
        this.f1189Rp = new C1401UY("radiobutton", jRadioButton);
    }

    public static ComponentUI createUI(JComponent jComponent) {
        return new RadioButtonUI((JRadioButton) jComponent);
    }

    /* access modifiers changed from: protected */
    public BasicButtonListener createButtonListener(AbstractButton abstractButton) {
        return new C5371aGt(abstractButton, this.f1189Rp);
    }

    public void update(Graphics graphics, JComponent jComponent) {
        AdapterUiCss.m9084a((IComponentUi) this, graphics, (AbstractButton) jComponent);
        paint(graphics, jComponent);
    }

    public Dimension getPreferredSize(JComponent jComponent) {
        AdapterUiCss.m9088a((IComponentUi) this, (AbstractButton) jComponent);
        return getComponentManager().mo13059c(jComponent, RadioButtonUI.super.getPreferredSize(jComponent));
    }

    public Dimension getMinimumSize(JComponent jComponent) {
        AdapterUiCss.m9088a((IComponentUi) this, (AbstractButton) jComponent);
        return getComponentManager().mo13057b(jComponent, RadioButtonUI.super.getMinimumSize(jComponent));
    }

    public Dimension getMaximumSize(JComponent jComponent) {
        AdapterUiCss.m9088a((IComponentUi) this, (AbstractButton) jComponent);
        return getComponentManager().mo13052a(jComponent, RadioButtonUI.super.getMaximumSize(jComponent));
    }

    /* renamed from: wy */
    public ComponentManager getComponentManager() {
        return this.f1189Rp;
    }
}
