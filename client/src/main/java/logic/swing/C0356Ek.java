package logic.swing;

import logic.ui.ComponentManager;
import logic.ui.IBaseUiTegXml;

import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

/* renamed from: a.Ek */
/* compiled from: a */
public class C0356Ek implements KeyListener, MouseListener {
    private static final IBaseUiTegXml aPy = IBaseUiTegXml.initBaseUItegXML();

    /* renamed from: Rp */
    private final ComponentManager f511Rp;

    public C0356Ek(ComponentManager pvVar) {
        this.f511Rp = pvVar;
    }

    /* renamed from: a */
    private boolean m3023a(KeyEvent keyEvent) {
        switch (keyEvent.getKeyCode()) {
            case 16:
            case 17:
            case 18:
            case 524:
            case 525:
                return true;
            default:
                return false;
        }
    }

    public void keyPressed(KeyEvent keyEvent) {
        if (!m3023a(keyEvent)) {
            aPy.mo13724cx(this.f511Rp.mo13049Vr().atZ());
        }
    }

    public void keyReleased(KeyEvent keyEvent) {
        if (!m3023a(keyEvent)) {
            aPy.mo13724cx(this.f511Rp.mo13049Vr().atY());
        }
    }

    public void keyTyped(KeyEvent keyEvent) {
        if (!m3023a(keyEvent)) {
            aPy.mo13724cx(this.f511Rp.mo13049Vr().aua());
        }
    }

    public void mouseClicked(MouseEvent mouseEvent) {
        aPy.mo13724cx(this.f511Rp.mo13049Vr().atX());
    }

    public void mouseEntered(MouseEvent mouseEvent) {
        aPy.mo13728e(this.f511Rp.mo13049Vr().atU(), false);
    }

    public void mouseExited(MouseEvent mouseEvent) {
    }

    public void mousePressed(MouseEvent mouseEvent) {
        aPy.mo13724cx(this.f511Rp.mo13049Vr().atV());
    }

    public void mouseReleased(MouseEvent mouseEvent) {
        aPy.mo13724cx(this.f511Rp.mo13049Vr().atW());
    }
}
