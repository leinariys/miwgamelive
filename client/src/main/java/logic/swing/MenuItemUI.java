package logic.swing;

import logic.ui.ComponentManager;

import javax.swing.*;
import javax.swing.plaf.ComponentUI;
import javax.swing.plaf.basic.BasicMenuItemUI;
import java.awt.*;

/* renamed from: a.aFr  reason: case insensitive filesystem */
/* compiled from: a */
public class MenuItemUI extends BasicMenuItemUI implements IComponentUi {

    /* renamed from: Rp */
    private ComponentManager f2844Rp;

    public MenuItemUI(JMenuItem jMenuItem) {
        this.f2844Rp = new C1401UY("menuitem", jMenuItem);
        jMenuItem.setOpaque(false);
    }

    public static ComponentUI createUI(JComponent jComponent) {
        return new MenuItemUI((JMenuItem) jComponent);
    }

    public void update(Graphics graphics, JComponent jComponent) {
        AdapterUiCss.m9084a((IComponentUi) this, graphics, (AbstractButton) jComponent);
        paint(graphics, jComponent);
    }

    /* access modifiers changed from: protected */
    public void paintBackground(Graphics graphics, JMenuItem jMenuItem, Color color) {
    }

    public Dimension getPreferredSize(JComponent jComponent) {
        AdapterUiCss.m9088a((IComponentUi) this, (AbstractButton) jComponent);
        return getComponentManager().mo13059c(jComponent, MenuItemUI.super.getPreferredSize(jComponent));
    }

    public Dimension getMinimumSize(JComponent jComponent) {
        AdapterUiCss.m9088a((IComponentUi) this, (AbstractButton) jComponent);
        return getComponentManager().mo13057b(jComponent, MenuItemUI.super.getMinimumSize(jComponent));
    }

    public Dimension getMaximumSize(JComponent jComponent) {
        AdapterUiCss.m9088a((IComponentUi) this, (AbstractButton) jComponent);
        return getComponentManager().mo13052a(jComponent, MenuItemUI.super.getMaximumSize(jComponent));
    }

    /* renamed from: wy */
    public ComponentManager getComponentManager() {
        return this.f2844Rp;
    }
}
