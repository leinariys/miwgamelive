package logic.swing;

import java.util.HashMap;

/* renamed from: a.aHa  reason: case insensitive filesystem */
/* compiled from: a */
public enum C5378aHa {
    NEUTRAL("data/gui/imageset/imageset_neutral/"),
    CORP_FLAGS("data/corporations/"),
    CORP_INV_IMAGES("data/gui/imageset/imageset_corp/"),
    ICONOGRAPHY("data/gui/imageset/imageset_iconography/"),
    ITEMS("data/gui/imageset/imageset_items/"),
    CORE("data/gui/imageset/imageset_core/"),
    CORP_IMAGES("data/gui/imageset/imageset_corpimages/"),
    LOADING("data/gui/imageset/imageset_loading/"),
    HUD("data/gui/imageset/imageset_hud/"),
    NPC_AVATAR("data/gui/imageset/imageset_avatar/"),
    AVATAR_CREATION("data/gui/imageset/imageset_avatar_creation/"),
    CITIZENSHIP("data/gui/imageset/imageset_citizenship_neo/"),
    PROGRESSION("data/gui/imageset/imageset_iconbutton_neo/"),
    LOGIN("data/gui/imageset/imageset_login/");

    private String path;

    private C5378aHa(String str) {
        this.path = str;
    }

    public static void cNQ() {
        HashMap hashMap = new HashMap();
        for (C5378aHa aha : values()) {
            if (hashMap.containsKey(aha.path)) {
                System.err.println("ImageSet path '" + aha.path + "' is set for multiple ImageSets: " + hashMap.get(aha.path) + " and " + aha);
            } else {
                hashMap.put(aha.path, aha);
            }
        }
    }

    public String getPath() {
        return "res://" + this.path;
    }
}
