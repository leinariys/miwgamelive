package logic.swing;

import lombok.extern.slf4j.Slf4j;

import javax.swing.*;
import java.awt.*;

/* renamed from: a.aKb  reason: case insensitive filesystem */
/* compiled from: a */
@Slf4j
public class C5457aKb extends PopupFactory {
    private static PopupFactory ihP;
    private final PopupFactory aUu = new PopupFactory();

    public static PopupFactory getSharedInstance() {
        if (ihP == null) {
            ihP = new C5457aKb();
            PopupFactory.setSharedInstance(ihP);
        }
        return ihP;
    }

    public Popup getPopup(Component component, Component component2, int i, int i2) {
        Component component3 = component;
        while (!(component3 instanceof JApplet)) {
            component3 = component3.getParent();
            if (component3 == null) {
                this.log.warn("Popup creation failed, default popup created - JRootPane not found in component hierarchy of " + component3);
                return this.aUu.getPopup(component3, component2, i, i2);
            }
        }
        JComponent glassPane = (JComponent) ((JApplet) component3).getGlassPane();
        Popup nq = new C0914NQ(glassPane);
        try {
            ((C0914NQ) nq).mo4235a(glassPane, component2, i, i2);
            return nq;
        } catch (Exception e) {
            JComponent layeredPane = ((JApplet) component3).getLayeredPane();
            Popup nq2 = new C0914NQ(layeredPane);
            ((C0914NQ) nq2).mo4235a(layeredPane, component2, i, i2);
            return nq2;
        }
    }
}
