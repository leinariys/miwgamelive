package logic.swing;

import logic.ui.ComponentManager;
import logic.ui.Panel;

import javax.swing.*;
import javax.swing.text.JTextComponent;
import java.awt.*;

/* renamed from: a.aqV  reason: case insensitive filesystem */
/* compiled from: a */
public class C6621aqV extends CellRendererPane {

    public ComponentManager grc;

    public C6621aqV(String str) {
        this.grc = new ComponentManager(str, this);
    }

    public C6621aqV() {
        this("panel");
    }

    public Insets getInsets() {
        return BorderWrapper.bfB().getBorderInsets(this);
    }

    public void paintComponent(Graphics graphics, Component component, Container container, int i, int i2, int i3, int i4, boolean z) {
        AdapterUiCss.m9094a(graphics, this, this.grc.mo13047Vp(), new Rectangle(i, i2, i3, i4));
        BorderWrapper.bfB().paintBorder(this, graphics, i, i2, i3, i4);
        Insets borderInsets = BorderWrapper.bfB().getBorderInsets(this);
        int i5 = i + borderInsets.left;
        int i6 = i2 + borderInsets.top;
        int i7 = i3 - (borderInsets.left + borderInsets.right);
        int i8 = i4 - (borderInsets.bottom + borderInsets.top);
        if (component != null) {
            if (component.getParent() != this) {
                add(component);
            }
            component.setBounds(i5, i6, i7, i8);
            if (z) {
                if (component instanceof Panel) {
                    synchronized (((Panel) component).getTreeLock()) {
                        ((Panel) component).validateTree();
                    }
                } else if (!(component instanceof JLabel) && !(component instanceof JTextComponent)) {
                    component.validate();
                }
            }
            boolean z2 = false;
            if ((component instanceof JComponent) && ((JComponent) component).isDoubleBuffered()) {
                z2 = true;
                ((JComponent) component).setDoubleBuffered(false);
            }
            boolean z3 = z2;
            Graphics create = graphics.create(i5, i6, i7, i8);
            try {
                component.paint(create);
                if (z3 && (component instanceof JComponent)) {
                    ((JComponent) component).setDoubleBuffered(true);
                }
                component.setBounds(-i7, -i8, 0, 0);
            } finally {
                create.dispose();
            }
        } else if (container != null) {
            Color color = graphics.getColor();
            graphics.setColor(container.getBackground());
            graphics.fillRect(i5, i6, i7, i8);
            graphics.setColor(color);
        }
    }
}
