package logic.swing;

import logic.ui.ComponentManager;
import logic.ui.IComponentManager;
import logic.ui.PropertiesUiFromCss;
import logic.ui.item.Progress;

import javax.swing.*;
import javax.swing.border.Border;
import javax.swing.plaf.ComponentUI;
import javax.swing.plaf.basic.BasicProgressBarUI;
import java.awt.*;

/* renamed from: a.vR */
/* compiled from: a */
public class ProgressBarUI extends BasicProgressBarUI implements IComponentUi {

    /* renamed from: Rp */
    private ComponentManager f9414Rp;

    public ProgressBarUI(JProgressBar jProgressBar) {
        jProgressBar.setBorder((Border) null);
        jProgressBar.setBackground(new Color(0, 0, 0, 0));
        jProgressBar.setStringPainted(true);
        this.f9414Rp = new ComponentManager("progress", jProgressBar);
    }

    public static ComponentUI createUI(JComponent jComponent) {
        return new ProgressBarUI((JProgressBar) jComponent);
    }

    public void paint(Graphics graphics, JComponent jComponent) {
        AdapterUiCss.m9089a((IComponentUi) this, jComponent);
        Progress bnVar = (Progress) jComponent;
        PropertiesUiFromCss Vp = this.f9414Rp.mo13047Vp();
        if (Vp != null) {
            if (bnVar.mo17408gu()) {
                mo22580a(graphics, bnVar, Vp);
                return;
            }
            Image ato = Vp.ato();
            Image fT = Vp.mo428fT(0);
            Image atq = Vp.atq();
            if (ato != null && fT != null && atq != null) {
                Rectangle bounds = bnVar.getBounds();
                int width = ato.getWidth(jComponent);
                int width2 = atq.getWidth(jComponent);
                int i = (bounds.width - width) - width2;
                int height = ato.getHeight(jComponent);
                int i2 = bounds.height;
                graphics.drawImage(ato, 0, 0, width, i2, 0, 0, width, height, jComponent);
                graphics.drawImage(fT, width, 0, width + i, i2, 0, 0, fT.getWidth(jComponent), height, jComponent);
                graphics.drawImage(atq, width + i, 0, width + i + width2, i2, 0, 0, width2, height, jComponent);
                int value = bnVar.getValue();
                int maximum = bnVar.getMaximum();
                int i3 = maximum == 0 ? 0 : (value * i) / maximum;
                if (value > 0) {
                    Image atA = Vp.atA();
                    Image atN = Vp.atN();
                    Image atg = Vp.atg();
                    if (atA != null && atN != null && atg != null) {
                        graphics.drawImage(atA, 0, 0, width, i2, 0, 0, width, height, jComponent);
                        graphics.drawImage(atg, width, 0, width + i3, i2, 0, 0, fT.getWidth(jComponent), height, jComponent);
                        graphics.drawImage(atN, width + i3, 0, width + i3 + width2, i2, 0, 0, width2, height, jComponent);
                    } else {
                        return;
                    }
                }
                if (this.progressBar.isStringPainted()) {
                    paintString(graphics, 0, 0, bounds.width, bounds.height, i3 + width2, jComponent.getInsets());
                }
            }
        }
    }

    /* renamed from: a */
    public void mo22580a(Graphics graphics, Progress bnVar, PropertiesUiFromCss aj) {
        int i;
        int ceil;
        Insets insets = bnVar.getInsets();
        int width = bnVar.getWidth() - (insets.right + insets.left);
        int height = bnVar.getHeight() - (insets.bottom + insets.top);
        if (width > 0 && height > 0) {
            if (bnVar.mo17409gv()) {
                if (bnVar.mo17410gw() > 0) {
                    int ceil2 = (int) Math.ceil((double) (((float) width) / ((float) bnVar.mo17410gw())));
                    i = (int) Math.ceil((double) (((float) width) / ((float) ceil2)));
                    ceil = ceil2;
                } else {
                    return;
                }
            } else if (bnVar.mo17411gx() > 0) {
                int ceil3 = (int) Math.ceil((double) (((float) width) / ((float) bnVar.mo17411gx())));
                i = ceil3;
                ceil = (int) Math.ceil((double) (((float) width) / ((float) ceil3)));
            } else {
                return;
            }
            int gy = bnVar.mo17412gy();
            graphics.translate(-gy, 0);
            int i2 = 0;
            int i3 = i - gy;
            if (bnVar.getValue() > 0) {
                Image atg = aj.atg();
                Image atA = aj.atA();
                Image atN = aj.atN();
                if (atg != null && atA != null && atN != null) {
                    int round = Math.round((((float) bnVar.getValue()) / ((float) bnVar.getMaximum())) * ((float) ceil));
                    int i4 = 0;
                    while (i4 < round) {
                        int i5 = (i * i4) + gy;
                        int width2 = i5 + atA.getWidth(bnVar);
                        int i6 = width2 + i3;
                        int width3 = atN.getWidth(bnVar);
                        int height2 = atg.getHeight(bnVar);
                        graphics.drawImage(atA, i5, 0, width2, height, 0, 0, atA.getWidth(bnVar), height2, bnVar);
                        graphics.drawImage(atg, width2, 0, i6, height, 0, 0, atg.getWidth(bnVar), height2, bnVar);
                        graphics.drawImage(atN, i6, 0, i6 + width3, height, 0, 0, width3, height2, bnVar);
                        i4++;
                    }
                    i2 = i4;
                } else {
                    return;
                }
            }
            Image fT = aj.mo428fT(0);
            Image ato = aj.ato();
            Image atq = aj.atq();
            if (fT != null && ato != null && atq != null) {
                while (true) {
                    int i7 = i2;
                    if (i7 < ceil) {
                        int i8 = (i * i7) + gy;
                        int width4 = i8 + ato.getWidth(bnVar);
                        int i9 = width4 + i3;
                        int width5 = atq.getWidth(bnVar);
                        ato.getHeight(bnVar);
                        graphics.drawImage(ato, i8, 0, width4, height, 0, 0, ato.getWidth(bnVar), ato.getHeight(bnVar), bnVar);
                        graphics.drawImage(fT, width4, 0, i9, height, 0, 0, fT.getWidth(bnVar), fT.getHeight(bnVar), bnVar);
                        graphics.drawImage(atq, i9, 0, i9 + width5, height, 0, 0, width5, atq.getHeight(bnVar), bnVar);
                        i2 = i7 + 1;
                    } else {
                        return;
                    }
                }
            }
        }
    }

    /* access modifiers changed from: protected */
    public Color getSelectionBackground() {
        return this.f9414Rp.mo13047Vp().mo427fS(0);
    }

    /* access modifiers changed from: protected */
    public Color getSelectionForeground() {
        return this.f9414Rp.mo13047Vp().getColor();
    }

    public Dimension getPreferredSize(JComponent jComponent) {
        return this.f9414Rp.mo13059c(jComponent, ProgressBarUI.super.getPreferredSize(jComponent));
    }

    public Dimension getMinimumSize(JComponent jComponent) {
        int i;
        int i2;
        PropertiesUiFromCss Vp = this.f9414Rp.mo13047Vp();
        if (Vp == null) {
            return ProgressBarUI.super.getMinimumSize(jComponent);
        }
        Dimension minimumSize = Vp.getMinimumSize();
        if (minimumSize != null) {
            return minimumSize;
        }
        Image ato = Vp.ato();
        Image fT = Vp.mo428fT(0);
        Image atq = Vp.atq();
        if (ato != null) {
            i2 = ato.getWidth(jComponent) + 0;
            i = 0 + ato.getHeight(jComponent);
        } else {
            i = 0;
            i2 = 0;
        }
        if (atq != null) {
            i2 += atq.getWidth(jComponent);
        }
        if (fT != null) {
            i2 += fT.getWidth(jComponent);
        }
        return new Dimension(i2, i);
    }

    /* renamed from: wz */
    public IComponentManager getComponentManager() {
        return this.f9414Rp;
    }
}
