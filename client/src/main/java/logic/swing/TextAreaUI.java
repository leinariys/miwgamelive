package logic.swing;

import logic.ui.ComponentManager;

import javax.swing.*;
import javax.swing.plaf.ComponentUI;
import javax.swing.plaf.basic.BasicTextAreaUI;
import javax.swing.text.JTextComponent;
import java.awt.*;

/* renamed from: a.xQ */
/* compiled from: a */
public class TextAreaUI extends BasicTextAreaUI implements IComponentUi {

    private final JTextArea bHW;
    /* renamed from: Rp */
    private ComponentManager f9534Rp;
    private final C0356Ek bHX = new C0356Ek(this.f9534Rp);

    public TextAreaUI(JTextArea jTextArea) {
        this.bHW = jTextArea;
        jTextArea.setWrapStyleWord(true);
        this.f9534Rp = new C6233aix("textarea", jTextArea);
        jTextArea.setSelectionColor(new Color(12, 92, 111, 255));
    }

    public static ComponentUI createUI(JComponent jComponent) {
        return new TextAreaUI((JTextArea) jComponent);
    }

    /* access modifiers changed from: protected */
    public void installListeners() {
        TextAreaUI.super.installListeners();
        this.bHW.addKeyListener(this.bHX);
        this.bHW.addMouseListener(this.bHX);
    }

    /* access modifiers changed from: protected */
    public void uninstallListeners() {
        TextAreaUI.super.uninstallListeners();
        this.bHW.removeKeyListener(this.bHX);
        this.bHW.removeKeyListener(this.bHX);
    }

    public void update(Graphics graphics, JComponent jComponent) {
        AdapterUiCss.m9087a((IComponentUi) this, graphics, (JTextComponent) jComponent);
        TextAreaUI.super.paint(graphics, jComponent);
    }

    /* access modifiers changed from: protected */
    public void paintBackground(Graphics graphics) {
    }

    public Dimension getMinimumSize(JComponent jComponent) {
        AdapterUiCss.m9091a((IComponentUi) this, (JTextComponent) jComponent);
        return getComponentManager().mo13057b(jComponent, TextAreaUI.super.getMinimumSize(jComponent));
    }

    public Dimension getMaximumSize(JComponent jComponent) {
        AdapterUiCss.m9091a((IComponentUi) this, (JTextComponent) jComponent);
        return getComponentManager().mo13052a(jComponent, TextAreaUI.super.getMaximumSize(jComponent));
    }

    public Dimension getPreferredSize(JComponent jComponent) {
        AdapterUiCss.m9091a((IComponentUi) this, (JTextComponent) jComponent);
        return this.f9534Rp.mo13059c(jComponent, TextAreaUI.super.getPreferredSize(jComponent));
    }

    /* renamed from: wy */
    public ComponentManager getComponentManager() {
        return this.f9534Rp;
    }
}
