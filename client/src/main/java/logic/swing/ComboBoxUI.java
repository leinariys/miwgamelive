package logic.swing;

import logic.ui.C1276Sr;
import logic.ui.ComponentManager;
import logic.ui.IComponentManager;

import javax.swing.*;
import javax.swing.plaf.ComponentUI;
import javax.swing.plaf.basic.BasicComboBoxRenderer;
import javax.swing.plaf.basic.BasicComboBoxUI;
import javax.swing.plaf.basic.BasicComboPopup;
import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

/* renamed from: a.hy */
/* compiled from: a */
public class ComboBoxUI extends BasicComboBoxUI implements IComponentUi {

    /* access modifiers changed from: private */
    public final JComboBox dgo;
    public boolean dgp;
    /* renamed from: Rp */
    private ComponentManager f8083Rp;

    public ComboBoxUI(JComboBox jComboBox) {
        this.dgo = jComboBox;
        this.f8083Rp = new ComponentManager("combobox", jComboBox);
        this.currentValuePane = new C2649d();
        jComboBox.setOpaque(false);
    }

    public static ComponentUI createUI(JComponent jComponent) {
        return new ComboBoxUI((JComboBox) jComponent);
    }

    public void update(Graphics graphics, JComponent jComponent) {
        AdapterUiCss.m9085a((IComponentUi) this, graphics, jComponent);
        paint(graphics, jComponent);
    }

    public void installUI(JComponent jComponent) {
        ComboBoxUI.super.installUI(jComponent);
        jComponent.addMouseListener(new C2648c());
    }

    public void uninstallUI(JComponent jComponent) {
        ComboBoxUI.super.uninstallUI(jComponent);
    }

    public Dimension getMinimumSize(JComponent jComponent) {
        AdapterUiCss.m9089a((IComponentUi) this, jComponent);
        return getComponentManager().mo13057b(jComponent, ComboBoxUI.super.getMinimumSize(jComponent));
    }

    public Dimension getMaximumSize(JComponent jComponent) {
        AdapterUiCss.m9089a((IComponentUi) this, jComponent);
        return getComponentManager().mo13052a(jComponent, ComboBoxUI.super.getMaximumSize(jComponent));
    }

    public Dimension getPreferredSize(JComponent jComponent) {
        AdapterUiCss.m9089a((IComponentUi) this, jComponent);
        return this.f8083Rp.mo13059c(jComponent, ComboBoxUI.super.getPreferredSize(jComponent));
    }

    /* renamed from: wy */
    public ComponentManager getComponentManager() {
        return this.f8083Rp;
    }

    /* access modifiers changed from: protected */
    public ComboBoxEditor createEditor() {
        ComboBoxEditor createEditor = ComboBoxUI.super.createEditor();
        if (createEditor.getEditorComponent() instanceof JTextField) {
            ((JTextField) createEditor.getEditorComponent()).setBorder(new BorderWrapper());
        }
        return createEditor;
    }

    /* access modifiers changed from: protected */
    public ListCellRenderer createRenderer() {
        return new C2646a(this, (C2646a) null);
    }

    /* access modifiers changed from: protected */
    public JButton createArrowButton() {
        JButton jButton = new JButton();
        jButton.setFocusable(false);
        this.dgo.add(jButton);
        return jButton;
    }

    public void paint(Graphics graphics, JComponent jComponent) {
        ComboBoxUI.super.paint(graphics, jComponent);
    }

    public void paintCurrentValue(Graphics graphics, Rectangle rectangle, boolean z) {
        ComboBoxUI.super.paintCurrentValue(graphics, rectangle, z);
    }

    /* renamed from: a.hy$b */
    /* compiled from: a */
    public class C2647b extends BasicComboPopup {

        public C2647b(JComboBox jComboBox) {
            super(jComboBox);
        }
    }

    /* renamed from: a.hy$a */
    private final class C2646a extends BasicComboBoxRenderer.UIResource implements C1276Sr {

        private C2646a() {
        }

        /* synthetic */ C2646a(ComboBoxUI hyVar, C2646a aVar) {
            this();
        }

        public Container getParent() {
            return ComboBoxUI.this.dgo;
        }

        public String getElementName() {
            return "text";
        }
    }

    /* renamed from: a.hy$d */
    /* compiled from: a */
    class C2649d extends C6621aqV {


        C2649d() {
        }

        public void paintComponent(Graphics graphics, Component component, Container container, int i, int i2, int i3, int i4, boolean z) {
            int i5 = ComboBoxUI.this.dgp ? 32 : 0;
            this.grc.mo13063q(32, i5);
            IComponentManager e = ComponentManager.getCssHolder(component);
            if (e != null) {
                e.mo13063q(32, i5);
            }
            super.paintComponent(graphics, component, container, i, i2, i3, i4, z);
            if (e != null) {
                e.mo13063q(0, 0);
            }
        }
    }

    /* renamed from: a.hy$c */
    /* compiled from: a */
    class C2648c extends MouseAdapter {
        C2648c() {
        }

        public void mouseEntered(MouseEvent mouseEvent) {
            if (!ComboBoxUI.this.dgp) {
                ComboBoxUI.this.dgp = true;
                ComboBoxUI.this.comboBox.repaint(150);
            }
        }

        public void mouseExited(MouseEvent mouseEvent) {
            if (ComboBoxUI.this.dgp) {
                ComboBoxUI.this.dgp = false;
                ComboBoxUI.this.comboBox.repaint(150);
            }
        }
    }
}
