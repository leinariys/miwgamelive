package logic.swing;

/* renamed from: a.aGg  reason: case insensitive filesystem */
/* compiled from: a */
public class C5358aGg {
    public static final C5358aGg hOF = new C5358aGg(0, 0);
    public static final C5358aGg hOG = new C5358aGg(0, 1);
    public static final C5358aGg hOH = new C5358aGg(1, 1);
    public static final C5358aGg hOI = new C5358aGg(2, 1);
    public static final C5358aGg hOJ = new C5358aGg(0, 4);
    public static final C5358aGg hOK = new C5358aGg(1, 4);
    public static final C5358aGg hOL = new C5358aGg(2, 4);
    public static final C5358aGg hOM = new C5358aGg(0, 5);
    public static final C5358aGg hON = new C5358aGg(1, 5);
    public static final C5358aGg hOO = new C5358aGg(2, 5);
    public static final C5358aGg hOP = new C5358aGg(0, 6);
    public static final C5358aGg hOQ = new C5358aGg(1, 6);
    public static final C5358aGg hOR = new C5358aGg(2, 6);
    public static final int hOA = 2;
    public static final int hOB = 3;
    public static final int hOC = 4;
    public static final int hOD = 5;
    public static final int hOE = 6;
    public static final int hOv = 0;
    public static final int hOw = 1;
    public static final int hOx = 2;
    public static final int hOy = 0;
    public static final int hOz = 1;
    private final int hOS;
    private final float hOT;
    private final int type;

    public C5358aGg() {
        this(1, 0);
    }

    public C5358aGg(int i) {
        this(i, 0);
    }

    public C5358aGg(int i, int i2) {
        this(i, i2, 1.0d);
    }

    public C5358aGg(int i, int i2, double d) {
        this.type = i;
        this.hOS = i2;
        this.hOT = (float) d;
    }

    public C5358aGg(C5358aGg agg, double d) {
        this(agg.type, agg.hOS, d);
    }

    /* renamed from: aD */
    public int mo9101aD(int i, int i2) {
        float mE;
        if (i <= 0 || i2 <= 0) {
            return 0;
        }
        if (i >= i2) {
            return i2;
        }
        float f = ((float) i) / ((float) i2);
        switch (this.type) {
            case 0:
                mE = mo9102mE(f);
                break;
            case 1:
                mE = 1.0f - mo9102mE(1.0f - f);
                break;
            case 2:
                if (f >= 0.5f) {
                    mE = 1.0f - (mo9102mE(2.0f - (2.0f * f)) / 2.0f);
                    break;
                } else {
                    mE = mo9102mE(2.0f * f) / 2.0f;
                    break;
                }
            default:
                mE = f;
                break;
        }
        if (this.hOT != 1.0f) {
            mE = (mE * this.hOT) + (f * (1.0f - this.hOT));
        }
        return Math.round(mE * ((float) i2));
    }

    /* access modifiers changed from: protected */
    /* renamed from: mE */
    public float mo9102mE(float f) {
        switch (this.hOS) {
            case 1:
                return f * f;
            case 2:
                return f * f * f;
            case 3:
                float f2 = f * f;
                return f2 * f2;
            case 4:
                float f3 = f * f;
                return f * f3 * f3;
            case 5:
                float f4 = f * f;
                return (f4 + (f4 * f)) - f;
            case 6:
                float f5 = f * f;
                return f5 * (((((f5 * f) * 2.0f) + f5) - (4.0f * f)) + 2.0f) * ((float) (-Math.sin(((double) f) * 3.5d * 3.141592653589793d)));
            default:
                return f;
        }
    }
}
