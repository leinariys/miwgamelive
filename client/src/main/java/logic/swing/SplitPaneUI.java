package logic.swing;

import logic.ui.ComponentManager;

import javax.swing.*;
import javax.swing.plaf.ComponentUI;
import javax.swing.plaf.basic.BasicSplitPaneUI;
import java.awt.*;

/* renamed from: a.afE  reason: case insensitive filesystem */
/* compiled from: a */
public class SplitPaneUI extends BasicSplitPaneUI implements IComponentUi {

    /* renamed from: Rp */
    private ComponentManager f4452Rp;

    public SplitPaneUI(JSplitPane jSplitPane) {
        this.f4452Rp = new ComponentManager("splitpane", jSplitPane);
        jSplitPane.setOpaque(false);
    }

    public static ComponentUI createUI(JComponent jComponent) {
        return new SplitPaneUI((JSplitPane) jComponent);
    }

    public void update(Graphics graphics, JComponent jComponent) {
        AdapterUiCss.m9085a((IComponentUi) this, graphics, jComponent);
        paint(graphics, jComponent);
    }

    public Dimension getPreferredSize(JComponent jComponent) {
        AdapterUiCss.m9089a((IComponentUi) this, jComponent);
        return getComponentManager().mo13059c(jComponent, SplitPaneUI.super.getPreferredSize(jComponent));
    }

    public Dimension getMinimumSize(JComponent jComponent) {
        AdapterUiCss.m9089a((IComponentUi) this, jComponent);
        return getComponentManager().mo13057b(jComponent, SplitPaneUI.super.getMinimumSize(jComponent));
    }

    public Dimension getMaximumSize(JComponent jComponent) {
        AdapterUiCss.m9089a((IComponentUi) this, jComponent);
        return getComponentManager().mo13052a(jComponent, SplitPaneUI.super.getMaximumSize(jComponent));
    }

    /* renamed from: wy */
    public ComponentManager getComponentManager() {
        return this.f4452Rp;
    }
}
