package logic.swing;

import logic.ui.ComponentManager;
import logic.ui.IComponentManager;

import javax.swing.*;
import javax.swing.plaf.ComponentUI;
import java.awt.*;

/* renamed from: a.TA */
/* compiled from: a */
public class C1295TA extends ComponentUI implements IComponentUi {

    /* renamed from: BM */
    private IComponentManager f1692BM;

    public C1295TA(Container container) {
        this.f1692BM = new ComponentManager("cell", container);
    }

    /* renamed from: wz */
    public IComponentManager getComponentManager() {
        return this.f1692BM;
    }

    public void uninstallUI(JComponent jComponent) {
        this.f1692BM = null;
    }
}
