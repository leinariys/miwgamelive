package logic.swing;

import logic.ui.ComponentManager;

import javax.swing.*;
import javax.swing.plaf.ComponentUI;
import javax.swing.plaf.basic.BasicEditorPaneUI;
import javax.swing.text.JTextComponent;
import java.awt.*;

/* renamed from: a.Qx */
/* compiled from: a */
public class EditorPaneUI extends BasicEditorPaneUI implements IComponentUi {

    /* renamed from: Rp */
    private ComponentManager f1481Rp;

    public EditorPaneUI(JEditorPane jEditorPane) {
        this.f1481Rp = (ComponentManager) ComponentManager.getCssHolder(jEditorPane);
        if (this.f1481Rp == null) {
            this.f1481Rp = new C6233aix("editor", jEditorPane);
        }
        jEditorPane.setOpaque(false);
    }

    public static ComponentUI createUI(JComponent jComponent) {
        return new EditorPaneUI((JEditorPane) jComponent);
    }

    public void update(Graphics graphics, JComponent jComponent) {
        AdapterUiCss.m9087a((IComponentUi) this, graphics, (JTextComponent) jComponent);
        EditorPaneUI.super.paint(graphics, jComponent);
    }

    /* access modifiers changed from: protected */
    public void paintBackground(Graphics graphics) {
    }

    public Dimension getMinimumSize(JComponent jComponent) {
        AdapterUiCss.m9091a((IComponentUi) this, (JTextComponent) jComponent);
        return getComponentManager().mo13057b(jComponent, EditorPaneUI.super.getMinimumSize(jComponent));
    }

    public Dimension getMaximumSize(JComponent jComponent) {
        AdapterUiCss.m9091a((IComponentUi) this, (JTextComponent) jComponent);
        return getComponentManager().mo13052a(jComponent, EditorPaneUI.super.getMaximumSize(jComponent));
    }

    public Dimension getPreferredSize(JComponent jComponent) {
        AdapterUiCss.m9091a((IComponentUi) this, (JTextComponent) jComponent);
        return this.f1481Rp.mo13059c(jComponent, EditorPaneUI.super.getPreferredSize(jComponent));
    }

    /* renamed from: wy */
    public ComponentManager getComponentManager() {
        return this.f1481Rp;
    }
}
