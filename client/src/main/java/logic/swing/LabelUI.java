package logic.swing;

import logic.ui.ComponentManager;
import sun.swing.SwingUtilities2;

import javax.swing.*;
import javax.swing.plaf.ComponentUI;
import javax.swing.plaf.basic.BasicLabelUI;
import java.awt.*;

/* renamed from: a.asd  reason: case insensitive filesystem */
/* compiled from: a */
public class LabelUI extends BasicLabelUI implements IComponentUi {

    /* renamed from: Rp */
    private ComponentManager componentManager;
    private int gtR = -1;

    public LabelUI(JComponent jComponent) {
        this.componentManager = (ComponentManager) ComponentManager.getCssHolder(jComponent);
        if (this.componentManager == null) {
            this.componentManager = new ComponentManager("label", jComponent);
        }
        jComponent.setBorder(BorderWrapper.bfB());
    }

    public static ComponentUI createUI(JComponent jComponent) {
        return new LabelUI(jComponent);
    }

    /* renamed from: wy */
    public ComponentManager getComponentManager() {
        return this.componentManager;
    }

    public Dimension getPreferredSize(JComponent jComponent) {
        AdapterUiCss.m9090a((IComponentUi) this, (JLabel) jComponent);
        return this.componentManager.mo13059c(jComponent, LabelUI.super.getPreferredSize(jComponent));
    }

    public Dimension getMinimumSize(JComponent jComponent) {
        AdapterUiCss.m9090a((IComponentUi) this, (JLabel) jComponent);
        return getComponentManager().mo13057b(jComponent, LabelUI.super.getMinimumSize(jComponent));
    }

    public Dimension getMaximumSize(JComponent jComponent) {
        AdapterUiCss.m9090a((IComponentUi) this, (JLabel) jComponent);
        return getComponentManager().mo13052a(jComponent, LabelUI.super.getMaximumSize(jComponent));
    }

    public void update(Graphics graphics, JComponent jComponent) {
        AdapterUiCss.m9086a((IComponentUi) this, graphics, (JLabel) jComponent);
        paint(graphics, jComponent);
    }

    /* access modifiers changed from: protected */
    public void paintDisabledText(JLabel jLabel, Graphics graphics, String str, int i, int i2) {
        LabelUI.super.paintEnabledText(jLabel, graphics, str, i, i2);
    }

    /* access modifiers changed from: protected */
    public void paintEnabledText(JLabel jLabel, Graphics graphics, String str, int i, int i2) {
        if (this.gtR < 0) {
            LabelUI.super.paintEnabledText(jLabel, graphics, str, i, i2);
            return;
        }
        int displayedMnemonicIndex = jLabel.getDisplayedMnemonicIndex();
        graphics.setColor(jLabel.getForeground());
        SwingUtilities2.drawStringUnderlineCharAt(jLabel, graphics, str.substring(0, Math.min(this.gtR, str.length())), displayedMnemonicIndex, i, i2);
    }

    public int ctz() {
        return this.gtR;
    }

    /* renamed from: uk */
    public void mo15940uk(int i) {
        this.gtR = i;
    }
}
