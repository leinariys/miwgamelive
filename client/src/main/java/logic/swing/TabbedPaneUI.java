package logic.swing;

import logic.ui.ComponentManager;
import logic.ui.IComponentManager;
import logic.ui.Panel;
import logic.ui.PropertiesUiFromCss;
import logic.ui.item.TabbedPane;
import sun.swing.SwingUtilities2;

import javax.swing.*;
import javax.swing.plaf.ComponentUI;
import javax.swing.plaf.basic.BasicTabbedPaneUI;
import javax.swing.text.View;
import java.awt.*;

/* renamed from: a.afm  reason: case insensitive filesystem */
/* compiled from: a */
public class TabbedPaneUI extends BasicTabbedPaneUI implements IComponentUi {

    /* renamed from: BM */
    private IComponentManager f4470BM;
    private JLabel fud;
    private PropertiesUiFromCss fue;
    private Panel fuf;

    public TabbedPaneUI(TabbedPane qb) {
        this.f4470BM = new ComponentManager("tabpane", qb);
        this.fud = new C1880a(qb);
        this.fuf = new C1881b(qb);
    }

    public static ComponentUI createUI(JComponent jComponent) {
        return new TabbedPaneUI((TabbedPane) jComponent);
    }

    /* renamed from: wz */
    public IComponentManager getComponentManager() {
        return this.f4470BM;
    }

    public Dimension getPreferredSize(JComponent jComponent) {
        return getComponentManager().mo13059c(jComponent, TabbedPaneUI.super.getPreferredSize(jComponent));
    }

    public Dimension getMinimumSize(JComponent jComponent) {
        return getComponentManager().mo13057b(jComponent, TabbedPaneUI.super.getMinimumSize(jComponent));
    }

    public Dimension getMaximumSize(JComponent jComponent) {
        return getComponentManager().mo13052a(jComponent, TabbedPaneUI.super.getMaximumSize(jComponent));
    }

    private void bVu() {
        if (!this.tabPane.isValid()) {
            this.tabPane.validate();
        }
        if (!this.tabPane.isValid()) {
            ((TabbedPaneLayout) this.tabPane.getLayout()).calculateLayoutInfo();
        }
    }

    public void paint(Graphics graphics, JComponent jComponent) {
        int selectedIndex = this.tabPane.getSelectedIndex();
        int tabPlacement = this.tabPane.getTabPlacement();
        bVu();
        paintContentBorder(graphics, tabPlacement, selectedIndex);
        paintTabArea(graphics, tabPlacement, selectedIndex);
    }

    /* access modifiers changed from: protected */
    public void paintContentBorder(Graphics graphics, int i, int i2) {
        int width = this.tabPane.getWidth();
        int height = this.tabPane.getHeight();
        Insets insets = this.tabPane.getInsets();
        int i3 = insets.left;
        int i4 = insets.top;
        int i5 = (width - insets.right) - insets.left;
        int i6 = (height - insets.top) - insets.bottom;
        switch (i) {
            case 2:
                i3 += calculateTabAreaWidth(i, this.runCount, this.maxTabWidth);
                i5 -= i3 - insets.left;
                break;
            case 3:
                i6 -= calculateTabAreaHeight(i, this.runCount, this.maxTabHeight);
                break;
            case 4:
                i5 -= calculateTabAreaWidth(i, this.runCount, this.maxTabWidth);
                break;
            default:
                i4 += calculateTabAreaHeight(i, this.runCount, this.maxTabHeight);
                i6 -= i4 - insets.top;
                break;
        }
        if (this.tabPane.getTabCount() > 0 && this.tabPane.isOpaque()) {
            Color color = UIManager.getColor("TabbedPane.contentAreaColor");
            if (color != null) {
                graphics.setColor(color);
            } else if (i2 == -1) {
                graphics.setColor(this.tabPane.getBackground());
            } else {
                graphics.setColor(Color.RED);
            }
            graphics.fillRect(i3, i4, i5, i6);
        }
        if (this.tabPane.getSelectedIndex() < 0) {
            this.fuf.getBorder().paintBorder(this.fuf, graphics, i3, i4, i5, i6);
            return;
        }
        Rectangle rectangle = this.rects[this.tabPane.getSelectedIndex()];
        if (rectangle == null) {
            this.fuf.getBorder().paintBorder(this.fuf, graphics, i3, i4, i5, i6);
            return;
        }
        int i7 = i3 + rectangle.x;
        this.fuf.getBorder().paintBorder(this.fuf, graphics, i3, i4, i7, height);
        this.fuf.getBorder().paintBorder(this.fuf, graphics, i7 + rectangle.width, i4, i5 - i7, i6);
    }

    /* access modifiers changed from: protected */
    public void paintTabArea(Graphics graphics, int i, int i2) {
        int tabCount = this.tabPane.getTabCount();
        Rectangle rectangle = new Rectangle();
        Rectangle rectangle2 = new Rectangle();
        Rectangle clipBounds = graphics.getClipBounds();
        int i3 = this.runCount - 1;
        while (i3 >= 0) {
            int i4 = this.tabRuns[i3 == this.runCount + -1 ? 0 : i3 + 1];
            int i5 = i4 != 0 ? i4 - 1 : tabCount - 1;
            for (int i6 = this.tabRuns[i3]; i6 <= i5; i6++) {
                if (i6 != i2 && this.rects[i6].intersects(clipBounds)) {
                    paintTab(graphics, i, this.rects, i6, rectangle, rectangle2);
                }
            }
            i3--;
        }
        if (this.rects.length > 0 && i2 >= 0 && this.rects[i2].intersects(clipBounds)) {
            paintTab(graphics, i, this.rects, i2, rectangle, rectangle2);
        }
    }

    /* access modifiers changed from: protected */
    public void paintTab(Graphics graphics, int i, Rectangle[] rectangleArr, int i2, Rectangle rectangle, Rectangle rectangle2) {
        Rectangle rectangle3 = rectangleArr[i2];
        boolean z = this.tabPane.getSelectedIndex() == i2;
        ComponentManager.getCssHolder(this.fud).mo13063q(2, z ? 2 : 0);
        this.fud.setBounds(rectangle3);
        AdapterUiCss.m9093a(graphics.create(rectangle3.x, rectangle3.y, rectangle3.width, rectangle3.height), (Component) this.fud, PropertiesUiFromCss.m462g(this.fud));
        BorderWrapper border = (BorderWrapper) this.fud.getBorder();
        if (border != null) {
            border.paintBorder(this.fud, graphics, rectangle3.x, rectangle3.y, rectangle3.width, rectangle3.height);
        }
        String titleAt = this.tabPane.getTitleAt(i2);
        this.fue = ComponentManager.getCssHolder(this.fud).mo13047Vp();
        Font font = this.fue.getFont();
        if (font != null) {
            FontMetrics fontMetrics = SwingUtilities2.getFontMetrics(this.tabPane, graphics, font);
            layoutLabel(i, fontMetrics, i2, titleAt, (Icon) null, rectangle3, rectangle, rectangle2, z);
            paintText(graphics, i, this.fue.getFont(), fontMetrics, i2, titleAt, rectangle2, z);
        }
    }

    /* access modifiers changed from: protected */
    public void paintText(Graphics graphics, int i, Font font, FontMetrics fontMetrics, int i2, String str, Rectangle rectangle, boolean z) {
        graphics.setFont(font);
        View textViewForTab = getTextViewForTab(i2);
        if (textViewForTab != null) {
            textViewForTab.paint(graphics, rectangle);
            return;
        }
        int displayedMnemonicIndexAt = this.tabPane.getDisplayedMnemonicIndexAt(i2);
        Color color = this.fue.getColor();
        if (color != null) {
            graphics.setColor(color);
        }
        if (!this.tabPane.isEnabled() || !this.tabPane.isEnabledAt(i2)) {
            SwingUtilities2.drawStringUnderlineCharAt(this.tabPane, graphics, str, displayedMnemonicIndexAt, rectangle.x, rectangle.y + fontMetrics.getAscent());
            return;
        }
        SwingUtilities2.drawStringUnderlineCharAt(this.tabPane, graphics, str, displayedMnemonicIndexAt, rectangle.x, rectangle.y + fontMetrics.getAscent());
    }

    /* access modifiers changed from: protected */
    public int getTabLabelShiftY(int i, int i2, boolean z) {
        return 0;
    }

    /* access modifiers changed from: protected */
    public int getTabLabelShiftX(int i, int i2, boolean z) {
        return 0;
    }

    /* access modifiers changed from: protected */
    public Insets getSelectedTabPadInsets(int i) {
        return new Insets(0, 0, 0, 0);
    }

    /* renamed from: a.afm$a */
    class C1880a extends JLabel {
        private final /* synthetic */ TabbedPane jby;

        C1880a(TabbedPane qb) {
            this.jby = qb;
        }

        public Container getParent() {
            return this.jby;
        }
    }

    /* renamed from: a.afm$b */
    /* compiled from: a */
    class C1881b extends Panel {
        private final /* synthetic */ TabbedPane jby;

        C1881b(TabbedPane qb) {
            this.jby = qb;
        }

        public Container getParent() {
            return this.jby;
        }
    }
}
