package logic;

import logic.res.code.SoftTimer;
import logic.ui.IBaseUiTegXml;
import taikodom.render.loader.provider.FilePath;

import java.io.File;
import java.util.Collection;

/* renamed from: a.axz  reason: case insensitive filesystem */
/* compiled from: a */
public interface IAddonManager {
    Collection<IWrapFileXmlOrJar> getCloneLoadAddonFile();

    IWrapEngGameAddonManager getWrapEngGameAddonManager();

    void startAddonManager();

    boolean isPlayerNotNull();

    C6245ajJ getEventManager();

    void initAllAddonWaitPlayer();

    IBaseUiTegXml getBaseUItagXML();

    FilePath getRootPathRender();

    FilePath getRootPath();

    SoftTimer getSoftTimer();

    void dispose();

    /* renamed from: eB */
    <T> T getRes(String str);

    /* renamed from: g */
    void setFolderAddons(File file);

    /* renamed from: n */
    <T> T putRes(String str, T t);

    void step(float f);

    /* renamed from: y */
    <T> T getInstanceWrapFileXmlOrJar(Class<T> cls);
}
