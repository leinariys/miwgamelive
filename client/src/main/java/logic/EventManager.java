package logic;

import logic.render.QueueItem;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

/* renamed from: a.bD */
/* compiled from: a */
public class EventManager extends C1753Zu implements C6245ajJ {

    /* renamed from: oT */
    private aMQ<String, Object> f5747oT = new aMQ<>();

    /* renamed from: oU */
    private BlockingQueue<QueueItem<String, Object>> f5748oU = new LinkedBlockingQueue();

    public void publish(Object obj) {
        mo2869a("", obj);
    }

    /* renamed from: a */
    public void mo2869a(String str, Object obj) {
        this.f5747oT.put(str, obj);
        mo13972d(str, obj);
    }

    /* renamed from: b */
    public void mo13970b(String str, Object obj) {
        this.f5747oT.mo10044k(str, obj);
        mo7503m(str, obj);
    }

    /* renamed from: g */
    public void mo13974g(Object obj) {
        mo13970b("", obj);
    }

    /* renamed from: h */
    public void mo13975h(Object obj) {
        mo13972d("", obj);
    }

    /* renamed from: i */
    public void mo13976i(Object obj) {
        mo13971c("", obj);
    }

    /* renamed from: c */
    public void mo13971c(String str, Object obj) {
        this.f5748oU.add(new QueueItem(str, obj));
    }

    /* renamed from: d */
    public void mo13972d(String str, Object obj) {
        mo7502l(str, obj);
    }

    /* renamed from: a */
    public <T> Collection<T> mo13963a(String str, Class<T> cls) {
        ArrayList arrayList = null;
        Collection<Object> aR = this.f5747oT.mo10040aR(str);
        if (aR == null) {
            return Collections.EMPTY_LIST;
        }
        for (Object next : aR) {
            if (next.getClass() == cls) {
                if (arrayList == null) {
                    arrayList = new ArrayList();
                }
                arrayList.add(next);
            }
        }
        if (arrayList == null) {
            return Collections.EMPTY_LIST;
        }
        return arrayList;
    }

    /* renamed from: b */
    public <T> Collection<T> mo13967b(Class<T> cls) {
        return mo13963a("", cls);
    }

    /* renamed from: gz */
    public void mo17250gz() {
        try {
            QueueItem take = this.f5748oU.take();
            mo13972d((String) take.getFirst(), take.getLast());
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    /* renamed from: gA */
    public int mo17249gA() {
        return this.f5748oU.size();
    }

    /* renamed from: e */
    public <T> Collection<T> mo13973e(String str, T t) {
        ArrayList arrayList = null;
        for (Object next : this.f5747oT.mo10040aR(str)) {
            if (next.equals(t)) {
                if (arrayList == null) {
                    arrayList = new ArrayList();
                }
                arrayList.add(next);
            }
        }
        if (arrayList != null) {
            return arrayList;
        }
        return Collections.EMPTY_LIST;
    }

    /* renamed from: j */
    public <T> Collection<T> mo13977j(T t) {
        return mo13973e("", t);
    }
}
