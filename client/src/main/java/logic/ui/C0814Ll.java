package logic.ui;

import logic.res.css.C6868avI;
import logic.res.css.aKV;
import org.w3c.dom.css.CSSValue;

import java.util.ArrayList;
import java.util.List;

/* renamed from: a.Ll */
/* compiled from: a */
public class C0814Ll extends C6868avI {
    List<C6868avI> dtv;

    public C0814Ll() {
        this.dtv = new ArrayList();
    }

    public C0814Ll(C6868avI... aviArr) {
        this.dtv = new ArrayList(aviArr.length);
        for (C6868avI b : aviArr) {
            mo3819b(b);
        }
    }

    /* renamed from: b */
    public void mo3819b(C6868avI avi) {
        if (avi != null) {
            this.dtv.add(avi);
        }
    }

    /* renamed from: a */
    public CSSValue mo3817a(C1162RC rc, String str, boolean z) {
        int size = this.dtv.size();
        while (true) {
            int i = size - 1;
            if (i >= 0) {
                CSSValue a = this.dtv.get(i).mo3817a(rc, str, false);
                if (a != null) {
                    return a;
                }
                size = i;
            } else if (!z || rc.mo15974Vm() == null) {
                return null;
            } else {
                return mo3817a((C1162RC) rc.mo15974Vm(), str, z);
            }
        }
    }

    public void clear() {
        this.dtv.clear();
    }

    /* renamed from: a */
    public boolean mo3818a(C1162RC rc, aKV akv, boolean z) {
        int size = this.dtv.size();
        while (true) {
            int i = size - 1;
            if (i < 0) {
                return true;
            }
            if (!this.dtv.get(i).mo3818a(rc, akv, z)) {
                return false;
            }
            size = i;
        }
    }
}
