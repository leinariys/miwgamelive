package logic.ui;

import logic.swing.C0454GJ;
import logic.swing.C2740jL;
import logic.swing.aDX;
import logic.ui.item.Picture;
import logic.ui.item.Progress;
import logic.ui.item.Repeater;
import logic.ui.item.TextField;

import javax.swing.*;
import java.awt.*;

/* renamed from: a.il */
/* compiled from: a */
public interface C2698il extends C1276Sr {
    /* renamed from: Kk */
    void mo4911Kk();

    /* renamed from: Kl */
    void mo4912Kl();

    Component add(Component component);

    Component add(Component component, int i);

    Component add(String str, Component component);

    void add(Component component, Object obj);

    void add(Component component, Object obj, int i);

    /* renamed from: cb */
    JButton mo4913cb(String str);

    /* renamed from: cc */
    JComboBox mo4914cc(String str);

    /* renamed from: cd */
    <T extends Component> T mo4915cd(String str);

    /* renamed from: ce */
    <T extends C2698il> T mo4916ce(String str);

    /* renamed from: cf */
    JLabel mo4917cf(String str);

    /* renamed from: cg */
    Progress mo4918cg(String str);

    /* renamed from: ch */
    Repeater<?> mo4919ch(String str);

    /* renamed from: ci */
    TextField mo4920ci(String str);

    void destroy();

    Component getComponent(int i);

    int getComponentCount();

    Component[] getComponents();

    LayoutManager getLayout();

    void pack();

    void setEnabled(boolean z);

    void setFocusable(boolean z);

    /* renamed from: a.il$b */
    /* compiled from: a */
    public static class C2703b extends JPanel implements C1276Sr {
        private static final long serialVersionUID = 5750734131014885897L;

        public String getElementName() {
            return "overlay";
        }
    }

    /* renamed from: a.il$a */
    public static class C2699a {
        /* access modifiers changed from: private */

        /* renamed from: SZ */
        public aDX f8243SZ;
        /* access modifiers changed from: private */
        public Container ats;
        /* access modifiers changed from: private */
        public Picture att;
        /* access modifiers changed from: private */
        public C0454GJ atu;
        private boolean atq;
        private C2703b atr = new C2703b();

        public C2699a(Container container) {
            this.ats = container;
            this.atr = new C2703b();
            this.atr.setVisible(false);
            this.att = new Picture();
            ComponentManager.getCssHolder(this.att).setAttribute("class", "loading");
            this.att.setSize(50, 50);
        }

        /* renamed from: Kk */
        public void mo19774Kk() {
            this.atq = true;
            this.atr.setVisible(true);
            this.att.setVisible(true);
            ComponentManager.getCssHolder(this.ats).mo13055aO(false);
            this.ats.repaint();
            this.atu = new C2700a();
            this.f8243SZ = new aDX(this.att, "[0..360] dur 3000", new C2702b(), this.atu);
        }

        /* renamed from: Kl */
        public void mo19775Kl() {
            this.atq = false;
            this.atr.setVisible(false);
            this.att.setVisible(false);
            if (this.f8243SZ != null) {
                this.f8243SZ.kill();
                ComponentManager.getCssHolder(this.ats).mo13055aO(true);
            }
            this.ats.repaint();
        }

        public void paint(Graphics graphics) {
            if (this.atq) {
                this.atr.setBounds(this.ats.getBounds());
                this.atr.paint(graphics);
                graphics.translate((this.atr.getWidth() - this.att.getWidth()) / 2, (this.atr.getHeight() - this.att.getHeight()) / 2);
                this.att.paint(graphics);
            }
        }

        /* renamed from: a.il$a$a */
        class C2700a implements C0454GJ {
            C2700a() {
            }

            /* renamed from: a */
            public void mo9a(JComponent jComponent) {
                if (C2699a.this.ats.isShowing() && jComponent.isVisible()) {
                    C2699a.this.f8243SZ = new aDX(C2699a.this.att, "[0..360] dur 3000", new C2701a(), C2699a.this.atu);
                }
            }

            /* renamed from: a.il$a$a$a */
            class C2701a implements C2740jL<Picture> {
                C2701a() {
                }

                /* renamed from: a */
                public void mo7a(Picture axm, float f) {
                    axm.mo16831kU(0.017453292f * f);
                    C2699a.this.ats.repaint();
                }

                /* renamed from: a */
                public float mo5a(Picture axm) {
                    return axm.cCz();
                }
            }
        }

        /* renamed from: a.il$a$b */
        /* compiled from: a */
        class C2702b implements C2740jL<Picture> {
            C2702b() {
            }

            /* renamed from: a */
            public void mo7a(Picture axm, float f) {
                axm.mo16831kU(0.017453292f * f);
                C2699a.this.ats.repaint();
            }

            /* renamed from: a */
            public float mo5a(Picture axm) {
                return axm.cCz();
            }
        }
    }
}
