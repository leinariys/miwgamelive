package logic.ui.item;

import javax.swing.*;
import java.awt.*;
import java.util.List;

/* renamed from: a.aUR */
/* compiled from: a */
public class aUR {
    public static final String propertyKey = "component.views";
    private List<C6303akP> iXK;

    public aUR(JComponent jComponent, List<C6303akP> list) {
        this.iXK = list;
    }

    /* renamed from: nQ */
    public C6303akP mo11630nQ(String str) {
        int size = this.iXK.size();
        for (int i = 0; i < size; i++) {
            C6303akP akp = this.iXK.get(i);
            if (str.equals(akp.getName())) {
                return akp;
            }
        }
        return null;
    }

    /* renamed from: nR */
    public Component mo11631nR(String str) {
        C6303akP nQ = mo11630nQ(str);
        if (nQ != null) {
            return nQ.getComponent();
        }
        return null;
    }

    public Component dAx() {
        return this.iXK.get(0).getComponent();
    }
}
