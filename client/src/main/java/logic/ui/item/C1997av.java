package logic.ui.item;

import javax.swing.text.Element;
import javax.swing.text.StyleConstants;
import javax.swing.text.View;
import javax.swing.text.ViewFactory;
import javax.swing.text.html.HTML;
import javax.swing.text.html.HTMLEditorKit;

/* renamed from: a.av */
/* compiled from: a */
public class C1997av extends HTMLEditorKit {


    public ViewFactory getViewFactory() {
        return new C1998a();
    }

    /* renamed from: a.av$a */
    public static class C1998a extends HTMLEditorKit.HTMLFactory implements ViewFactory {
        public View create(Element element) {
            Object attribute = element.getAttributes().getAttribute(StyleConstants.NameAttribute);
            if (!(attribute instanceof HTML.Tag) || ((HTML.Tag) attribute) != HTML.Tag.IMG) {
                return super.create(element);
            }
            return new C1470Ve(element);
        }
    }
}
