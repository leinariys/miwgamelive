package logic.ui.item;

import logic.res.XmlNode;
import logic.ui.C2698il;
import logic.ui.C6342alC;
import lombok.extern.slf4j.Slf4j;

import javax.swing.*;
import java.awt.*;
import java.util.List;
import java.util.*;

/* renamed from: a.tY */
/* compiled from: a */
@Slf4j
public class Repeater<T> extends Container implements C2698il {

    public C6342alC buU;
    public C3671a<T> buV;
    public List<XmlNode> buW;
    public Container buX;
    public Map<T, ArrayList<Component>> buY;
    public List<Repeater<?>> buZ;

    public Repeater(C6342alC alc, XmlNode agy) {
        this.buZ = new ArrayList();
        this.buU = alc;
        this.buW = new ArrayList(agy.getListChildrenTag());
        this.buY = new HashMap();
    }

    public Repeater(C6342alC alc, Container container, XmlNode agy) {
        this(alc, agy);
        Container container2 = container;
        while (container2 != null && (container2 instanceof Repeater)) {
            container2 = ((Repeater) container2).aiS();
        }
        this.buX = container2;
    }

    /* access modifiers changed from: protected */
    public Container aiS() {
        return this.buX;
    }

    /* renamed from: a */
    public void mo22250a(C3671a<T> aVar) {
        this.buV = aVar;
    }

    /* renamed from: k */
    public void mo22255k(List<XmlNode> list) {
        this.buW = list;
    }

    /* renamed from: G */
    public List<Component> mo22248G(T t) {
        if (this.buX == null) {
            return null;
        }
        ArrayList arrayList = new ArrayList(this.buW.size());
        for (XmlNode next : this.buW) {
            IBaseItem gC = this.buU.getClassBaseUi(next.getTagName());
            if (gC == null) {
                log.error("There is no component named: '" + next.getTagName() + "'");
            } else {
                Component add = this.buX.add(gC.creatUi(this.buU, this, next));
                if (this.buV != null) {
                    this.buV.mo843a(t, add);
                }
                arrayList.add(add);
            }
        }
        this.buY.put(t, arrayList);
        this.buX.validate();
        return arrayList;
    }

    public List<Component> get(T t) {
        return this.buY.get(t);
    }

    /* renamed from: H */
    public List<Component> mo22249H(T t) {
        if (this.buX == null) {
            return null;
        }
        List<Component> remove = this.buY.remove(t);
        if (remove == null) {
            return null;
        }
        for (Component remove2 : remove) {
            this.buX.remove(remove2);
        }
        return remove;
    }

    public void clear() {
        for (ArrayList<Component> it : this.buY.values()) {
            Iterator it2 = it.iterator();
            while (it2.hasNext()) {
                Repeater tYVar = (Repeater) it2.next();
                this.buX.remove(tYVar);
                if (tYVar instanceof Repeater) {
                    tYVar.clear();
                    this.buZ.remove(tYVar);
                }
            }
        }
        this.buY.clear();
        for (Repeater<?> clear : this.buZ) {
            clear.clear();
        }
        this.buZ.clear();
    }

    /* renamed from: a */
    public void mo22251a(Repeater<?> tYVar) {
        this.buZ.add(tYVar);
    }

    public void setEnabled(boolean z) {
    }

    public void setFocusable(boolean z) {
    }

    public void destroy() {
    }

    /* renamed from: cb */
    public JButton mo4913cb(String str) {
        return mo4915cd(str);
    }

    /* renamed from: cc */
    public JComboBox mo4914cc(String str) {
        return mo4915cd(str);
    }

    private List<Component> aiT() {
        ArrayList arrayList = new ArrayList();
        for (ArrayList<Component> addAll : this.buY.values()) {
            arrayList.addAll(addAll);
        }
        return arrayList;
    }

    /* renamed from: cd */
    public <T extends Component> T mo4915cd(String str) {
        T cd;
        Iterator<Component> it = aiT().iterator();
        while (it.hasNext()) {
            T t = (T) it.next();
            if (str.equals(t.getName())) {
                return t;
            }
            if ((t instanceof C2698il) && (cd = ((C2698il) t).mo4915cd(str)) != null) {
                return cd;
            }
        }
        return null;
    }

    /* renamed from: ce */
    public C2698il mo4916ce(String str) {
        Iterator<Component> it = aiT().iterator();
        while (it.hasNext()) {
            C2698il ilVar = (C2698il) it.next();
            if (ilVar instanceof C2698il) {
                if (str.equals(((Repeater) ilVar).getName())) {
                    return ilVar;
                }
                C2698il ce = ilVar.mo4916ce(str);
                if (ce != null) {
                    return ce;
                }
            }
        }
        return null;
    }

    /* renamed from: cf */
    public JLabel mo4917cf(String str) {
        return mo4915cd(str);
    }

    /* renamed from: cg */
    public Progress mo4918cg(String str) {
        return mo4915cd(str);
    }

    /* renamed from: ch */
    public Repeater<?> mo4919ch(String str) {
        Repeater<?> ch;
        if (str == null) {
            return null;
        }
        for (Repeater<?> next : this.buZ) {
            if (str.equals(next.getName())) {
                return next;
            }
        }
        for (Component ilVar : getComponents()) {
            if ((ilVar instanceof C2698il) && (ch = ((C2698il) ilVar).mo4919ch(str)) != null) {
                return ch;
            }
        }
        return null;
    }

    /* renamed from: ci */
    public TextField mo4920ci(String str) {
        return mo4915cd(str);
    }

    public void pack() {
    }

    public String getElementName() {
        return "repeater";
    }

    /* renamed from: Kk */
    public void mo4911Kk() {
        if (this.buX instanceof C2698il) {
            ((C2698il) this.buX).mo4911Kk();
        }
    }

    /* renamed from: Kl */
    public void mo4912Kl() {
        if (this.buX instanceof C2698il) {
            ((C2698il) this.buX).mo4912Kl();
        }
    }

    /* renamed from: a.tY$a */
    public interface C3671a<T> {
        /* renamed from: a */
        void mo843a(T t, Component component);
    }
}
