package logic.ui.item;

import logic.res.XmlNode;
import logic.res.css.C6868avI;
import logic.swing.C3940wz;
import logic.ui.C6342alC;
import logic.ui.ComponentManager;
import logic.ui.IComponentManager;
import logic.ui.MapKeyValue;
import lombok.extern.slf4j.Slf4j;

import javax.swing.*;
import java.awt.*;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;

/* renamed from: a.DV */
/* compiled from: a */
@Slf4j
public abstract class BaseItem<ComponentType extends Component> implements IBaseItem {

    /* renamed from: a */
    public static Integer checkValueAttribute(XmlNode agy, String str, org.slf4j.Logger ur) {
        String attribute = agy.getAttribute(str);
        if (attribute == null) {
            return null;
        }
        try {
            return Integer.valueOf(Integer.parseInt(attribute));
        } catch (NumberFormatException e) {
            ur.error("Invalid parameter. Attribute: '" + str + "', value: '" + attribute + "'", e);
            return null;
        }
    }


    /**
     * Создание графического Component, работа с CSS и системой событий
     *
     * @param alc       ключ - значение,  класса перевода и путь
     * @param container Рут панель Пример avax.swing.JRootPane[,0,0,1024x780,invalid,layout=javax.swing.JRootPane$RootLayout,alignmentX=0.0,alignmentY=0.0,border=all.BorderWrapper@f34226,flags=16777664,maximumSize=,minimumSize=,preferredSize=]
     * @param agy       Пример <window class="empty" transparent="true" alwaysOnTop="true" focusable="false" layout="GridLayout" css="taikodomversion.css">...</window>
     * @return
     */
    /* renamed from: d */
    public ComponentType creatUi(C6342alC alc, Container container, XmlNode agy) {
        ComponentType componentType = creatComponentCustom(alc, container, agy);
        String attribute = agy.getAttribute("css");
        if (attribute != null) {
            C6868avI a = alc.getBaseUiTegXml().loadCss((Object) alc.getPathFile(), attribute);
            IComponentManager e = ComponentManager.getCssHolder(componentType);
            e.mo13054a(a);//Установить класс CSS стиль
            e.mo13045Vk();
        }
        createLayout(alc, componentType, agy);
        return componentType;
    }

    /**
     * Создать базовый класс компонента
     * Реализован в каждом классе Базовые элементы интерфейса BaseItem
     *
     * @param alc
     * @param container
     * @param agy
     * @return
     */
    /* access modifiers changed from: protected */
    /* renamed from: c */
    public abstract ComponentType creatComponentCustom(C6342alC alc, Container container, XmlNode agy);

    /**
     * Устанавливаем настройки системы событий графического Component и его самого
     *
     * @param alc       ключ - значение,  класса перевода и путь
     * @param component Крафический Component
     * @param agy       Пример <window class="empty" transparent="true" alwaysOnTop="true" focusable="false" layout="GridLayout" css="taikodomversion.css">...</window>
     */
    /* access modifiers changed from: protected */
    /* renamed from: a */
    public void createLayout(C6342alC alc, ComponentType component, XmlNode agy) {
        IComponentManager e = ComponentManager.getCssHolder(component);//ПОлучили систему событий тега?
        String attribute = agy.getAttribute("name");//Пример name="version"
        if (attribute != null) {
            component.setName(attribute);//Присваивае графическому Component  имя например version
        }
        String attribute2 = agy.getAttribute("enableMouseOver");//Добавить/удалить Обработчик события курсор над объектом
        if (attribute2 != null) {
            e.mo13056aP("true".equals(attribute2));
        }
        if ("false".equals(agy.getAttribute("focusable"))) {//может ли компонент получить фокус
            component.setFocusable(false);
        }
        if ("false".equals(agy.getAttribute("enabled"))) {//включенный ?
            component.setEnabled(false);
        }
        String attribute3 = agy.getAttribute("visible");
        if (attribute3 != null) {
            component.setVisible("true".equals(attribute3));
        }
        if (component instanceof JComponent) {
            String attribute4 = agy.getAttribute("tooltipProvider");
            if (attribute4 != null) {
                ((JComponent) component).setToolTipText("");
                C3940wz.m40777a((JComponent) component, attribute4);
            }
            String attribute5 = agy.getAttribute("tooltip");
            if (attribute5 != null) {
                ((JComponent) component).setToolTipText(attribute5);
            } else if ("true".equals(agy.getAttribute("enableTooltip"))) {
                ((JComponent) component).setToolTipText("");
            }
        }
        if (e != null) {
            String attribute6 = agy.getAttribute("style");//style="vertical-align:fill; text-align:fill"
            if (attribute6 != null) {
                e.setStyle(attribute6);
            }
            //Передать в систему событий тега все атрибуты
            e.setAttributes(agy.getAttributes());
            if ("true".equals(agy.getAttribute("debug"))) {
                e.setAttribute("debug", "true");
            }
        }
    }

    /**
     * Создание в JComponent других JComponent согласно структуре XML файла
     *
     * @param alc
     * @param jComponent Родитель JComponent
     * @param agy        структура XML
     * @return
     */
    /* access modifiers changed from: protected */
    /* renamed from: a */
    public boolean mo1631a(C6342alC alc, JComponent jComponent, XmlNode agy) {
        Component component = null;
        ArrayList arrayList = null;
        for (XmlNode next : agy.getListChildrenTag()) {
            if ("view".equals(next.getTagName()) && next.getListChildrenTag().size() != 0) {//нет потомков и не view
                if (next.getListChildrenTag().size() > 1) {
                    log.warn("Ignoring view with more than one component");
                }
                XmlNode agy2 = next.getListChildrenTag().get(0);
                IBaseItem gC = alc.getClassBaseUi(agy2.getTagName().toLowerCase());//Получить класс по имени тега в xml
                if (gC != null) {
                    component = gC.creatUi(alc, jComponent, agy2);
                } else if ("import".equals(agy2.getTagName())) {
                    try {
                        URL url = new URL(alc.getPathFile(), agy2.getAttribute("src"));
                        component = alc.getBaseUiTegXml().createJComponent((C6342alC) new MapKeyValue(alc, url), url);
                    } catch (MalformedURLException e) {
                        throw new RuntimeException(e);
                    }
                } else {
                    log.error("There is no component named '" + agy2.getTagName() + "'");
                }
                if (arrayList == null) {
                    arrayList = new ArrayList(agy2.getListChildrenTag().size());
                }
                C6303akP akp = new C6303akP(next.getAttribute("name"), component);
                akp.setAttributes(agy2.getAttributes());
                arrayList.add(akp);
            }
        }
        if (arrayList == null) {
            return false;
        }
        jComponent.putClientProperty(aUR.propertyKey, new aUR(jComponent, arrayList));
        return true;
    }
}
