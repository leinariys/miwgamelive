package logic.ui.item;

import gnu.trove.TObjectIntHashMap;
import logic.res.XmlNode;
import logic.ui.C6342alC;
import lombok.extern.slf4j.Slf4j;

import javax.swing.*;
import java.awt.*;
import java.util.ArrayList;

/* renamed from: a.ie */
/* compiled from: a */
@Slf4j
public class ListCustom extends BaseItem<JList> {

    /* renamed from: Xe */
    static TObjectIntHashMap<String> selectionModel = new TObjectIntHashMap<>();

    static {
        selectionModel.put("NO_SELECTION", -10);
        selectionModel.put("SINGLE_SELECTION", 0);
        selectionModel.put("SINGLE_INTERVAL_SELECTION", 1);
        selectionModel.put("MULTIPLE_INTERVAL_SELECTION", 2);
        selectionModel.put("HORIZONTAL_WRAP", 2);
        selectionModel.put("VERTICAL_WRAP", 1);
        selectionModel.put("VERTICAL", 0);
    }

    /* access modifiers changed from: protected */
    /* renamed from: h */
    public JList creatComponentCustom(C6342alC alc, Container container, XmlNode agy) {
        return new JList(new DefaultListModel());
    }

    /* renamed from: i */
    public JList creatUi(C6342alC alc, Container container, XmlNode agy) {
        JList d = super.creatUi(alc, container, agy);
        mo1631a(alc, d, agy);
        return d;
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public void createLayout(C6342alC alc, JList aan, XmlNode agy) {
        super.createLayout(alc, aan, agy);
        ArrayList arrayList = null;
        for (XmlNode next : agy.getListChildrenTag()) {
            if ("listItem".equalsIgnoreCase(next.getTagName())) {
                if (arrayList == null) {
                    arrayList = new ArrayList();
                }
                arrayList.add(next.getAttribute("text"));
            }
        }
        if (arrayList != null) {
            aan.setListData(arrayList.toArray(new String[arrayList.size()]));
        }
        String attribute = agy.getAttribute("selectionMode");
        if (attribute != null) {
            aan.setSelectionMode(selectionModel.get(attribute.replace('-', '_').toUpperCase()));
        }
        String attribute2 = agy.getAttribute("prototypeCellValue");
        if (attribute2 != null) {
            try {
                aan.setPrototypeCellValue(Integer.valueOf(Integer.parseInt(attribute2)));
            } catch (NumberFormatException e) {
                log.warn(String.valueOf(e));
            }
        }
        String attribute3 = agy.getAttribute("layoutOrientation");
        if (attribute3 != null) {
            aan.setLayoutOrientation(selectionModel.get(attribute3.replace('-', '_').toUpperCase()));
            aan.setVisibleRowCount(-1);
        }
        String attribute4 = agy.getAttribute("visibleRowCount");
        if (attribute4 != null) {
            try {
                aan.setVisibleRowCount(Integer.parseInt(attribute4));
            } catch (NumberFormatException e2) {
                log.warn(String.valueOf(e2));
            }
        }
    }
}
