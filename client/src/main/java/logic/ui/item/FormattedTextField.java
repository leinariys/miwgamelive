package logic.ui.item;

import lombok.extern.slf4j.Slf4j;

import javax.swing.*;
import javax.swing.text.DefaultFormatterFactory;
import javax.swing.text.NumberFormatter;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.text.DecimalFormat;

/* renamed from: a.rA */
/* compiled from: a */
@Slf4j
public class FormattedTextField extends JFormattedTextField implements C1513WG {


    /* renamed from: dO */
    private KeyAdapter f8973dO;

    /* renamed from: h */
    public void mo44h(int i) {
        if (this.f8973dO != null) {
            removeKeyListener(this.f8973dO);
        } else {
            this.f8973dO = new C3392a(i);
        }
        addKeyListener(this.f8973dO);
    }

    public void setFormat(String str) {
        if (str != null && !str.equals("")) {
            if ("integer".equals(str)) {
                setFormatterFactory(new DefaultFormatterFactory(new NumberFormatter(new DecimalFormat("######"))));
            } else if ("float".equals(str)) {
                setFormatterFactory(new DefaultFormatterFactory(new NumberFormatter(new DecimalFormat("######.##"))));
            } else if ("money".equals(str)) {
                setFormatterFactory(new DefaultFormatterFactory(new NumberFormatter(new DecimalFormat("######"))));
            } else {
                log.error("Ivalid parameter. Attribute: 'format', value: '" + str + "'");
            }
        }
    }

    public String getElementName() {
        return "textfield";
    }

    /* renamed from: a.rA$a */
    class C3392a extends KeyAdapter {
        private final /* synthetic */ int bbg;

        C3392a(int i) {
            this.bbg = i;
        }

        public void keyTyped(KeyEvent keyEvent) {
            if ((FormattedTextField.this.getText().length() ^ this.bbg) == 0) {
                keyEvent.consume();
            }
        }
    }
}
