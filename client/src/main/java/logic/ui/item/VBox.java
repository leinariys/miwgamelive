package logic.ui.item;

import logic.ui.C3810vI;
import logic.ui.PropertiesUiFromCss;
import logic.ui.aRU;

import javax.swing.plaf.ComponentUI;
import java.awt.*;

/* renamed from: a.vA */
/* compiled from: a */
public class VBox extends C2867lF {


    /* access modifiers changed from: protected */
    /* renamed from: b */
    public Dimension mo11122b(int i, int i2) {
        int componentCount = getComponentCount();
        int i3 = 0;
        int i4 = 0;
        int i5 = 0;
        while (i3 < componentCount) {
            Dimension d = mo20189d(getComponent(i3), i, i2);
            int i6 = d.height + i4;
            i5 = Math.max(i5, d.width);
            i3++;
            i4 = i6;
        }
        return new Dimension(i5, i4);
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public Dimension mo11120a(int i, int i2) {
        int componentCount = getComponentCount();
        int i3 = 0;
        int i4 = 0;
        int i5 = 0;
        while (i3 < componentCount) {
            Dimension c = mo20188c(getComponent(i3), i, i2);
            int i6 = c.height + i4;
            i5 = Math.max(i5, c.width);
            i3++;
            i4 = i6;
        }
        return new Dimension(i5, i4);
    }

    public void layout() {
        int componentCount = getComponentCount();
        PropertiesUiFromCss a = PropertiesUiFromCss.m456a((ComponentUI) getUI());
        C3810vI atR = a.atR();
        aRU aty = a.aty();
        Insets amu = amu();
        Dimension size = getSize();
        Dimension dimension = new Dimension((size.width - amu.right) - amu.left, (size.height - amu.top) - amu.bottom);
        int jp = a.ats().mo19909Iv().mo5756jp((float) dimension.height);
        C2867lF.C2869b bVar = new C2867lF.C2869b((float) (dimension.height - jp), componentCount);
        for (int i = 0; i < componentCount; i++) {
            bVar.mo20197a(i, getComponent(i), dimension.width, dimension.height - jp);
        }
        bVar.mo20199Mq();
        int t = atR.mo16934t(dimension.height, ((int) bVar.ghM) + jp);
        for (int i2 = 0; i2 < componentCount; i2++) {
            Component component = getComponent(i2);
            if (bVar.bjD[i2]) {
                int min = Math.min(dimension.width, bVar.bjC[i2].width);
                int i3 = bVar.ghH[i2];
                Point a2 = mo20182a(component, dimension.width, dimension.height);
                component.setBounds(aty.mo11143t(dimension.width, min) + a2.x + amu.left, a2.y + t + amu.top, aty.mo7766u(dimension.width, min), i3);
                t += i3 + jp;
            }
        }
    }

    public String getElementName() {
        return "vbox";
    }
}
