package logic.ui.item;

import gnu.trove.TObjectIntHashMap;
import logic.res.XmlNode;
import logic.ui.C6342alC;
import lombok.extern.slf4j.Slf4j;

import java.awt.*;

/* renamed from: a.aUA */
/* compiled from: a */
@Slf4j
public class FormattedFieldCustom extends TextComponent<FormattedTextField> {

    private static TObjectIntHashMap<String> model = new TObjectIntHashMap<>();

    static {
        model.put("LEFT", 2);
        model.put("CENTER", 0);
        model.put("RIGHT", 4);
        model.put("LEADING", 10);
        model.put("TRAILING", 11);
    }

    /* renamed from: Z */
    public FormattedTextField creatComponentCustom(C6342alC alc, Container container, XmlNode agy) {
        return new FormattedTextField();
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public void mo7340a(C6342alC alc, FormattedTextField rAVar, XmlNode agy) {
        super.createLayout(alc, rAVar, agy);
        rAVar.setFormat(agy.getAttribute("format"));
        Integer a = checkValueAttribute(agy, "cols", log);
        if (a != null) {
            rAVar.setColumns(a.intValue());
        }
        String attribute = agy.getAttribute("horizontalAlignment");
        if (attribute != null) {
            rAVar.setHorizontalAlignment(model.get(attribute.toUpperCase()));
        }
    }
}
