package logic.ui.item;

import logic.swing.C0454GJ;
import logic.swing.C2740jL;

import java.awt.*;

/* renamed from: a.lL */
/* compiled from: a */
public abstract class C2877lL {
    private Component component;

    /* renamed from: iU */
    public abstract C2740jL<Component> mo20217iU();

    /* renamed from: iV */
    public abstract String mo20218iV();

    /* renamed from: iT */
    public void mo20216iT() {
    }

    /* renamed from: iW */
    public C0454GJ mo20219iW() {
        return null;
    }

    /* access modifiers changed from: protected */
    public Component getComponent() {
        return this.component;
    }

    public void setComponent(Component component2) {
        this.component = component2;
    }
}
