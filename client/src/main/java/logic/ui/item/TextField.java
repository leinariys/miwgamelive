package logic.ui.item;

import javax.swing.*;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;

/* renamed from: a.ahW  reason: case insensitive filesystem */
/* compiled from: a */
public class TextField extends JTextField implements C1513WG {


    /* renamed from: dO */
    private KeyAdapter f4560dO;

    /* renamed from: h */
    public void mo44h(int i) {
        if (this.f4560dO != null) {
            removeKeyListener(this.f4560dO);
        } else {
            this.f4560dO = new C1892a(i);
        }
        addKeyListener(this.f4560dO);
    }

    public String getElementName() {
        return "textfield";
    }

    /* renamed from: a.ahW$a */
    class C1892a extends KeyAdapter {
        private final /* synthetic */ int bbg;

        C1892a(int i) {
            this.bbg = i;
        }

        public void keyTyped(KeyEvent keyEvent) {
            if ((TextField.this.getText().length() ^ this.bbg) == 0) {
                keyEvent.consume();
            }
        }
    }
}
