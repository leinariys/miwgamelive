package logic.ui.item;

import logic.swing.C3940wz;

import javax.swing.*;
import javax.swing.event.ListSelectionListener;
import java.awt.event.MouseEvent;

/* renamed from: a.aAn  reason: case insensitive filesystem */
/* compiled from: a */
public class ListJ extends JList {
    public static final int hde = -10;
    private static final long serialVersionUID = 4349201625258261309L;
    private MouseEvent hdf;

    public ListJ(DefaultListModel defaultListModel) {
        super(defaultListModel);
    }

    public String getToolTipText(MouseEvent mouseEvent) {
        int locationToIndex;
        this.hdf = mouseEvent;
        if (!(C3940wz.m40778d(this) == null || (locationToIndex = locationToIndex(mouseEvent.getPoint())) == -1)) {
            if (!getCellBounds(locationToIndex, locationToIndex + 1).contains(mouseEvent.getPoint())) {
                return null;
            }
            Object elementAt = getModel().getElementAt(locationToIndex);
            if (elementAt != null) {
                return elementAt.toString();
            }
        }
        return ListJ.super.getToolTipText(mouseEvent);
    }

    public MouseEvent cHt() {
        return this.hdf;
    }

    /* renamed from: c */
    public void mo7729c(MouseEvent mouseEvent) {
        this.hdf = mouseEvent;
    }

    public void setSelectionMode(int i) {
        switch (i) {
            case hde /*-10*/:
                setSelectionModel(new C1766a(this, (C1766a) null));
                return;
            default:
                setSelectionModel(new DefaultListSelectionModel());
                ListJ.super.setSelectionMode(i);
                return;
        }
    }

    /* renamed from: a.aAn$a */
    private class C1766a implements ListSelectionModel {
        private C1766a() {
        }

        /* synthetic */ C1766a(ListJ aan, C1766a aVar) {
            this();
        }

        public void addListSelectionListener(ListSelectionListener listSelectionListener) {
        }

        public void addSelectionInterval(int i, int i2) {
        }

        public void clearSelection() {
        }

        public int getAnchorSelectionIndex() {
            return -1;
        }

        public void setAnchorSelectionIndex(int i) {
        }

        public int getLeadSelectionIndex() {
            return -1;
        }

        public void setLeadSelectionIndex(int i) {
        }

        public int getMaxSelectionIndex() {
            return -1;
        }

        public int getMinSelectionIndex() {
            return -1;
        }

        public int getSelectionMode() {
            return -1;
        }

        public void setSelectionMode(int i) {
        }

        public boolean getValueIsAdjusting() {
            return false;
        }

        public void setValueIsAdjusting(boolean z) {
        }

        public void insertIndexInterval(int i, int i2, boolean z) {
        }

        public boolean isSelectedIndex(int i) {
            return false;
        }

        public boolean isSelectionEmpty() {
            return false;
        }

        public void removeIndexInterval(int i, int i2) {
        }

        public void removeListSelectionListener(ListSelectionListener listSelectionListener) {
        }

        public void removeSelectionInterval(int i, int i2) {
        }

        public void setSelectionInterval(int i, int i2) {
        }
    }
}
