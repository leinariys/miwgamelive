package logic.ui.item;

import logic.swing.C2740jL;
import logic.ui.ComponentManager;
import logic.ui.PropertiesUiFromCss;

import javax.swing.*;
import java.awt.*;

/* renamed from: a.kk */
/* compiled from: a */
public class C2830kk {
    public static final C2740jL<Component> asO = new C4125zp();
    public static final C2740jL<Component> asP = new C4127zr();
    public static final C2740jL<Component> asQ = new C4128zs();
    public static final C2740jL<Component> asR = new C4130zu();
    public static final C2740jL<Component> asS = new C4131zv();
    public static final C2740jL<JLabel> asT = new C4133zx();
    public static final C2740jL<Progress> asU = new C4095zO();
    public static final C2740jL<Picture> asV = new C4094zN();

    /* renamed from: a.kk$a */
    public static class C2831a implements C2740jL<Component> {
        private final Color enP;
        private final Color enQ;
        private float aSB = 0.0f;

        public C2831a(Color color, Color color2) {
            this.enP = color;
            this.enQ = color2;
        }

        /* renamed from: a */
        public void mo7a(Component component, float f) {
            PropertiesUiFromCss Vp = ComponentManager.getCssHolder(component).mo13047Vp();
            this.aSB = f;
            float f2 = this.aSB / 255.0f;
            Vp.setColorMultiplier(new Color(m34540a(this.enP.getRed(), this.enQ.getRed(), f2), m34540a(this.enP.getGreen(), this.enQ.getGreen(), f2), m34540a(this.enP.getBlue(), this.enQ.getBlue(), f2)));
            component.repaint();
        }

        /* renamed from: a */
        private int m34540a(int i, int i2, float f) {
            return ((int) Math.ceil((double) ((((float) i) * f) + (((float) i2) * (1.0f - f))))) & 255;
        }

        /* renamed from: a */
        public float mo5a(Component component) {
            return this.aSB;
        }
    }
}
