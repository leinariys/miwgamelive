package logic.ui.item;

import logic.res.XmlNode;
import logic.ui.BaseItemFactory;
import logic.ui.C6342alC;

import java.awt.*;

/* renamed from: a.aAd  reason: case insensitive filesystem */
/* compiled from: a */
public class LabeledIconCustom extends BaseItemFactory<LabeledIcon> {
    /* renamed from: K */
    public LabeledIcon creatComponentCustom(C6342alC alc, Container container, XmlNode agy) {
        return new LabeledIcon();
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public void mo361b(C6342alC alc, LabeledIcon hs, XmlNode agy) {
        super.createLayout(alc, hs, agy);
        String attribute = agy.getAttribute("ne-text");
        if (attribute != null) {
            hs.mo2602a(LabeledIcon.C0530a.NORTH_EAST, attribute);
        }
        String attribute2 = agy.getAttribute("nw-text");
        if (attribute2 != null) {
            hs.mo2602a(LabeledIcon.C0530a.NORTH_WEST, attribute2);
        }
        String attribute3 = agy.getAttribute("se-text");
        if (attribute3 != null) {
            hs.mo2602a(LabeledIcon.C0530a.SOUTH_EAST, attribute3);
        }
        String attribute4 = agy.getAttribute("sw-text");
        if (attribute4 != null) {
            hs.mo2602a(LabeledIcon.C0530a.SOUTH_WEST, attribute4);
        }
        String attribute5 = agy.getAttribute("ne-icon");
        if (attribute5 != null) {
            hs.mo2607b(LabeledIcon.C0530a.NORTH_EAST, attribute5);
        }
        String attribute6 = agy.getAttribute("nw-icon");
        if (attribute6 != null) {
            hs.mo2607b(LabeledIcon.C0530a.NORTH_WEST, attribute6);
        }
        String attribute7 = agy.getAttribute("se-icon");
        if (attribute7 != null) {
            hs.mo2607b(LabeledIcon.C0530a.SOUTH_EAST, attribute7);
        }
        String attribute8 = agy.getAttribute("sw-icon");
        if (attribute8 != null) {
            hs.mo2607b(LabeledIcon.C0530a.SOUTH_WEST, attribute8);
        }
        String attribute9 = agy.getAttribute("bg-image");
        if (attribute9 != null) {
            hs.setBackgroundImage(attribute9);
        }
    }
}
