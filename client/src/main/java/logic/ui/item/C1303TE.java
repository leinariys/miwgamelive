package logic.ui.item;

import logic.ui.C1276Sr;

import javax.swing.*;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.TreeCellRenderer;
import java.awt.*;
import java.util.HashMap;
import java.util.Map;

/* renamed from: a.TE */
/* compiled from: a */
public class C1303TE implements TreeCellRenderer {
    private Map<Object, C1304a> gqW = new HashMap();

    public Component getTreeCellRendererComponent(JTree jTree, Object obj, boolean z, boolean z2, boolean z3, int i, boolean z4) {
        Object obj2;
        C1304a aVar = this.gqW.get(obj);
        if (aVar != null) {
            return aVar;
        }
        Component aVar2 = new C1304a();
        if (obj instanceof DefaultMutableTreeNode) {
            obj2 = ((DefaultMutableTreeNode) obj).getUserObject();
        } else {
            obj2 = obj;
        }
        if (obj2 instanceof Component) {
            aVar2.add((PopupMenu) obj2);
        } else {
            aVar2.add(new PopupMenu(obj.toString()));
        }
        this.gqW.put(obj, (C1304a) aVar2);
        return aVar2;
    }

    /* renamed from: a.TE$a */
    public static class C1304a extends JPanel implements C1276Sr {
        public String getElementName() {
            return "treecell";
        }
    }
}
