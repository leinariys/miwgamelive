package logic.ui.item;

import gnu.trove.TObjectIntHashMap;
import logic.res.XmlNode;
import logic.ui.C6342alC;

import javax.swing.*;
import javax.swing.tree.DefaultTreeSelectionModel;
import java.awt.*;

/* renamed from: a.alR  reason: case insensitive filesystem */
/* compiled from: a */
public class TreeCustom extends BaseItem<JTree> {

    /* renamed from: Xe */
    private static TObjectIntHashMap<String> selectionModel = new TObjectIntHashMap<>();

    static {
        selectionModel.put("SINGLE_TREE_SELECTION", 1);
        selectionModel.put("CONTIGUOUS_TREE_SELECTION", 2);
        selectionModel.put("DISCONTIGUOUS_TREE_SELECTION", 4);
    }

    /* renamed from: D */
    public JTree creatUi(C6342alC alc, Container container, XmlNode agy) {
        JTree d = super.creatUi(alc, container, agy);
        mo1631a(alc, (JComponent) d, agy);
        return d;
    }

    /* access modifiers changed from: protected */
    /* renamed from: E */
    public JTree creatComponentCustom(C6342alC alc, Container container, XmlNode agy) {
        return new C5923acz();
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public void createLayout(C6342alC alc, JTree jTree, XmlNode agy) {
        super.createLayout(alc, jTree, agy);
        String attribute = agy.getAttribute("toggleClickCount");
        if (attribute != null) {
            jTree.setToggleClickCount(Integer.parseInt(attribute));
        }
        String attribute2 = agy.getAttribute("rowHeight");
        if (attribute2 != null) {
            jTree.setRowHeight(Integer.parseInt(attribute2));
        }
        jTree.setRootVisible(!"false".equals(agy.getAttribute("rootVisible")));
        String attribute3 = agy.getAttribute("selectionModel");
        if (attribute3 != null) {
            DefaultTreeSelectionModel defaultTreeSelectionModel = new DefaultTreeSelectionModel();
            defaultTreeSelectionModel.setSelectionMode(selectionModel.get(attribute3.replace('-', '_').toUpperCase()));
            jTree.setSelectionModel(defaultTreeSelectionModel);
        }
    }
}
