package logic.ui.item;

import logic.res.XmlNode;
import logic.ui.BaseItemFactory;
import logic.ui.C6342alC;

import java.awt.*;

/* renamed from: a.aQW */
/* compiled from: a */
public class RepeaterCustom<C extends Component> extends BaseItemFactory<Repeater<C>> {
    /* access modifiers changed from: protected */
    /* renamed from: U */
    public Repeater<C> creatComponentCustom(C6342alC alc, Container container, XmlNode agy) {
        return new Repeater<>(alc, container, agy);
    }
}
