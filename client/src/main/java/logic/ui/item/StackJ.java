package logic.ui.item;

import logic.ui.C3810vI;
import logic.ui.PropertiesUiFromCss;
import logic.ui.aRU;

import javax.swing.plaf.ComponentUI;
import java.awt.*;

/* renamed from: a.uH */
/* compiled from: a */
public class StackJ extends C2867lF {


    /* access modifiers changed from: protected */
    /* renamed from: b */
    public Dimension mo11122b(int i, int i2) {
        int componentCount = getComponentCount();
        int i3 = 0;
        int i4 = 0;
        for (int i5 = 0; i5 < componentCount; i5++) {
            Component component = getComponent(i5);
            if (component.isVisible()) {
                Dimension d = mo20189d(component, i, i2);
                Point a = mo20182a(component, i, i2);
                i4 = Math.max(i4, d.width + a.x);
                i3 = Math.max(i3, a.y + d.height);
            }
        }
        return new Dimension(i4, i3);
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public Dimension mo11120a(int i, int i2) {
        int componentCount = getComponentCount();
        int i3 = 0;
        int i4 = 0;
        for (int i5 = 0; i5 < componentCount; i5++) {
            Component component = getComponent(i5);
            if (component.isVisible()) {
                Dimension c = mo20188c(component, i, i2);
                Point a = mo20182a(component, i, i2);
                i4 = Math.max(i4, c.width + a.x);
                i3 = Math.max(i3, a.y + c.height);
            }
        }
        return new Dimension(i4, i3);
    }

    public void layout() {
        int componentCount = getComponentCount();
        Rectangle bounds = getBounds();
        PropertiesUiFromCss a = PropertiesUiFromCss.m456a((ComponentUI) getUI());
        aRU aty = a.aty();
        C3810vI atR = a.atR();
        for (int i = 0; i < componentCount; i++) {
            Component component = getComponent(i);
            if (component.isVisible()) {
                Dimension d = mo20189d(component, bounds.width, bounds.height);
                Point a2 = mo20182a(component, bounds.width, bounds.height);
                mo20183a(component, aty, atR, a2.x, a2.y, bounds.width, bounds.height, d.width, d.height);
            }
        }
    }

    public String getElementName() {
        return "stack";
    }
}
