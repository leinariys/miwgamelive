package logic.ui.item;

import logic.res.XmlNode;
import logic.ui.C6342alC;

import java.awt.*;

/* renamed from: a.aAQ */
/* compiled from: a */
public class ItemIconCustom extends BaseItem<ItemIcon> {
    /* renamed from: O */
    public ItemIcon creatComponentCustom(C6342alC alc, Container container, XmlNode agy) {
        return new ItemIcon();
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public void createLayout(C6342alC alc, ItemIcon nIVar, XmlNode agy) {
        super.createLayout(alc, nIVar, agy);
        String attribute = agy.getAttribute("src");
        if (attribute != null) {
            nIVar.mo20664aw(attribute);
        }
    }
}
