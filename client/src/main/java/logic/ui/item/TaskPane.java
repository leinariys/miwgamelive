package logic.ui.item;

import logic.swing.C0454GJ;
import logic.swing.C2740jL;
import logic.swing.aDX;
import logic.ui.ComponentManager;
import logic.ui.IBaseUiTegXml;
import logic.ui.Panel;

import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.List;

/* renamed from: a.bp */
/* compiled from: a */
public class TaskPane extends Panel implements C0567Hq {

    /* access modifiers changed from: private */
    public boolean eoS;
    /* access modifiers changed from: private */
    public Panel fuf;
    /* access modifiers changed from: private */
    public Panel ipc;
    /* access modifiers changed from: private */
    public List<C2075f> ipj = new ArrayList();
    /* access modifiers changed from: private */
    public List<C2073d> ipk = new ArrayList();
    /* access modifiers changed from: private */
    public boolean ipo;
    /* renamed from: DV */
    private Panel f5908DV;
    private JButton aTM;
    private TaskPaneContainer ipb;
    private JLabel ipd;
    private JButton ipe;
    private JToggleButton ipf;
    private List<C2072c> ipg = new ArrayList();
    private List<C2074e> iph = new ArrayList();
    private List<C2080h> ipi = new ArrayList();
    private C2070a ipl;
    private C2082j ipm;
    private C2083k ipn;
    private boolean ipp = true;
    private boolean ipq = false;
    private boolean resizable = true;

    public TaskPane() {
        super.setLayout(new GridLayout());
        this.f5908DV = (Panel) IBaseUiTegXml.initBaseUItegXML().createJComponent(TaskPane.class, "taskpane.xml");
        m28011iM();
        m28002Fa();
        super.addImpl(this.f5908DV, (Object) null, -1);
        pack();
        ComponentManager.getCssHolder(this.ipe).setAttribute("class", "expanded");
    }

    /* renamed from: iM */
    private void m28011iM() {
        this.ipc = (Panel) this.f5908DV.mo4916ce("northPane");
        this.ipd = this.ipc.mo4917cf("title");
        this.ipe = this.ipc.mo4915cd("collapse");
        this.ipf = this.ipc.mo4915cd("pin");
        this.fuf = (Panel) this.f5908DV.mo4916ce("contentPane");
        this.aTM = this.f5908DV.mo4913cb("resizeButton");
    }

    /* renamed from: Fa */
    private void m28002Fa() {
        C2076g gVar = new C2076g(this);
        this.ipd.addMouseListener(gVar);
        this.ipd.addMouseMotionListener(gVar);
        this.ipe.addMouseListener(gVar);
        this.ipf.addMouseListener(new C2081i());
        C2071b bVar = new C2071b(this, (C2071b) null);
        this.aTM.addMouseListener(bVar);
        this.aTM.addMouseMotionListener(bVar);
    }

    public String getTitle() {
        return this.ipd.getText();
    }

    public void setTitle(String str) {
        this.ipd.setText(str);
    }

    public JLabel djn() {
        return this.ipd;
    }

    public Panel djo() {
        return this.ipc;
    }

    public Panel djp() {
        return this.fuf;
    }

    /* renamed from: q */
    public void mo17446q(Component component) {
        if (this.fuf.getComponentCount() > 0) {
            this.fuf.removeAll();
        }
        addImpl(component, (Object) null, -1);
    }

    public TaskPaneContainer djq() {
        if (this.ipb == null) {
            Container parent = getParent();
            while (parent != null && !(parent instanceof TaskPaneContainer)) {
                parent = parent.getParent();
            }
            if (parent != null) {
                this.ipb = (TaskPaneContainer) parent;
            }
        }
        return this.ipb;
    }

    public boolean isResizable() {
        return this.resizable;
    }

    public void setResizable(boolean z) {
        this.resizable = z;
        this.aTM.setVisible(z);
    }

    /* access modifiers changed from: protected */
    public void addImpl(Component component, Object obj, int i) {
        this.fuf.add(component, obj, i);
    }

    public void setLayout(LayoutManager layoutManager) {
        if (this.fuf != null) {
            this.fuf.setLayout(layoutManager);
        }
    }

    public String getElementName() {
        return "taskpane";
    }

    public void setVisible(boolean z) {
        if (z && !djr()) {
            z = false;
        }
        super.setVisible(z);
        if (this.fuf != null) {
            this.fuf.setVisible(z);
        }
    }

    /* access modifiers changed from: private */
    /* renamed from: m */
    public boolean m28012m(int i, int i2) {
        return this.resizable && (!isPinned() || (this.resizable && (this.ipn == null || this.ipn.mo17465m(i, i2)))) && i2 >= getMinimumSize().height && i2 <= getMaximumSize().height && i2 >= djo().getHeight();
    }

    public void expand() {
        this.fuf.setVisible(true);
        if (mo17419LW()) {
            this.ipf.setEnabled(true);
            if (this.ipq) {
                mo17429aw(true);
            }
        }
        ComponentManager.getCssHolder(this.ipe).setAttribute("class", "expanded");
        for (int i = 0; i < this.ipi.size(); i++) {
            this.ipi.get(i).collapse(false);
        }
    }

    public void collapse() {
        int i = 0;
        this.ipq = isPinned();
        mo17429aw(false);
        if (mo17419LW()) {
            this.ipf.setEnabled(false);
        }
        ComponentManager.getCssHolder(this.ipe).setAttribute("class", "collapsed");
        ComponentManager.getCssHolder(this.ipe).mo13045Vk();
        Insets insets = getInsets();
        setSize(getWidth(), insets.bottom + this.ipc.getHeight() + insets.top);
        this.ipo = true;
        while (true) {
            int i2 = i;
            if (i2 < this.ipi.size()) {
                this.ipi.get(i2).collapse(true);
                i = i2 + 1;
            } else {
                return;
            }
        }
    }

    public boolean isCollapsed() {
        return this.ipo;
    }

    /* renamed from: m */
    public void mo17445m(boolean z) {
        boolean z2;
        boolean z3 = true;
        if (this.eoS != z) {
            this.eoS = z;
            this.ipe.setVisible(!z);
            if (this.ipp) {
                JToggleButton jToggleButton = this.ipf;
                if (z) {
                    z2 = false;
                } else {
                    z2 = true;
                }
                jToggleButton.setVisible(z2);
            }
            if (isResizable()) {
                JButton jButton = this.aTM;
                if (z) {
                    z3 = false;
                }
                jButton.setVisible(z3);
            }
            for (int i = 0; i < this.ipg.size(); i++) {
                this.ipg.get(i).mo17454av(z);
            }
        }
    }

    /* renamed from: aw */
    public void mo17429aw(boolean z) {
        int i = 0;
        if (z) {
            if (!isCollapsed()) {
                if (!mo17419LW()) {
                    this.ipf.setSelected(false);
                    return;
                }
            } else {
                return;
            }
        }
        if (z != this.ipf.isSelected()) {
            this.ipf.setSelected(z);
        }
        while (true) {
            int i2 = i;
            if (i2 < this.iph.size()) {
                this.iph.get(i2).mo17456aw(z);
                i = i2 + 1;
            } else {
                return;
            }
        }
    }

    /* renamed from: aA */
    public boolean mo878aA() {
        return false;
    }

    public boolean isPinned() {
        return this.ipf.isSelected();
    }

    /* renamed from: LW */
    public boolean mo17419LW() {
        return this.ipp && (this.ipm == null || this.ipm.mo17464LW());
    }

    /* renamed from: jR */
    public void mo17444jR(boolean z) {
        this.ipp = z;
        this.ipf.setVisible(z);
    }

    /* renamed from: a */
    public void mo17421a(C2072c cVar) {
        this.ipg.add(cVar);
    }

    /* renamed from: b */
    public void mo17430b(C2072c cVar) {
        this.ipg.remove(cVar);
    }

    /* renamed from: a */
    public void mo17423a(C2074e eVar) {
        this.iph.add(eVar);
    }

    /* renamed from: b */
    public void mo17432b(C2074e eVar) {
        this.iph.remove(eVar);
    }

    /* renamed from: a */
    public void mo17425a(C2080h hVar) {
        this.ipi.add(hVar);
    }

    /* renamed from: b */
    public void mo17434b(C2080h hVar) {
        this.ipi.remove(hVar);
    }

    /* renamed from: a */
    public void mo17424a(C2075f fVar) {
        this.ipj.add(fVar);
    }

    /* renamed from: b */
    public void mo17433b(C2075f fVar) {
        this.ipj.remove(fVar);
    }

    /* renamed from: a */
    public void mo17422a(C2073d dVar) {
        this.ipk.add(dVar);
    }

    /* renamed from: b */
    public void mo17431b(C2073d dVar) {
        this.ipk.remove(dVar);
    }

    /* renamed from: a */
    public void mo17420a(C2070a aVar) {
        this.ipl = aVar;
    }

    private boolean djr() {
        if (this.ipl != null) {
            return this.ipl.isEnabled();
        }
        return true;
    }

    /* renamed from: a */
    public void mo17426a(C2082j jVar) {
        this.ipm = jVar;
    }

    /* renamed from: a */
    public void mo17427a(C2083k kVar) {
        this.ipn = kVar;
    }

    /* renamed from: a.bp$a */
    public interface C2070a {
        boolean isEnabled();
    }

    /* renamed from: a.bp$c */
    /* compiled from: a */
    public interface C2072c {
        /* renamed from: av */
        void mo17454av(boolean z);
    }

    /* renamed from: a.bp$d */
    /* compiled from: a */
    public interface C2073d {
        /* renamed from: cX */
        void mo17455cX(int i);
    }

    /* renamed from: a.bp$e */
    /* compiled from: a */
    public interface C2074e {
        /* renamed from: aw */
        void mo17456aw(boolean z);
    }

    /* renamed from: a.bp$f */
    /* compiled from: a */
    public interface C2075f {
        /* renamed from: l */
        void mo17457l(int i, int i2);
    }

    /* renamed from: a.bp$h */
    /* compiled from: a */
    public interface C2080h {
        void collapse(boolean z);
    }

    /* renamed from: a.bp$j */
    /* compiled from: a */
    public interface C2082j {
        /* renamed from: LW */
        boolean mo17464LW();
    }

    /* renamed from: a.bp$k */
    /* compiled from: a */
    public interface C2083k {
        /* renamed from: m */
        boolean mo17465m(int i, int i2);
    }

    /* renamed from: a.bp$g */
    /* compiled from: a */
    private class C2076g extends MouseAdapter {
        /* access modifiers changed from: private */

        /* access modifiers changed from: package-private */
        public final /* synthetic */ TaskPane bFp;
        /* renamed from: SZ */
        public aDX f5909SZ = null;
        private boolean efC = false;
        private int ixW;
        private C0454GJ ixX;
        private C0454GJ ixY;
        private Cursor ixZ;
        private Cursor iya;
        private C2740jL<Component> iyb;
        private int time = 400;

        public C2076g(TaskPane bpVar) {
            boolean z = false;
            this.bFp = bpVar;
            bpVar.ipo = !bpVar.fuf.isVisible() ? true : z;
            this.ixX = new C2077a();
            this.ixY = new C2078b();
            this.iyb = new C2079c();
        }

        private void dmZ() {
            TaskPaneContainer djq = this.bFp.djq();
            if (!this.bFp.ipo) {
                this.bFp.ipo = true;
                int height = this.bFp.getHeight();
                Insets insets = this.bFp.getInsets();
                if (this.f5909SZ != null) {
                    this.f5909SZ.kill();
                }
                djq.mo23035du(true);
                this.bFp.collapse();
                this.f5909SZ = new aDX(this.bFp, "[" + height + ".." + (insets.bottom + this.bFp.ipc.getHeight() + insets.top) + "] dur " + this.time + " strong_out", this.iyb, this.ixY);
                IBaseUiTegXml.initBaseUItegXML().mo13724cx(ComponentManager.getCssHolder(this.bFp).mo13049Vr().aui());
                return;
            }
            this.bFp.ipo = false;
            if (this.f5909SZ != null) {
                this.f5909SZ.kill();
            }
            djq.mo23035du(true);
            this.bFp.expand();
            this.f5909SZ = new aDX(this.bFp, "[" + this.bFp.getHeight() + ".." + this.bFp.getPreferredSize().height + "] dur " + this.time + " strong_out", this.iyb, this.ixX);
            IBaseUiTegXml.initBaseUItegXML().mo13724cx(ComponentManager.getCssHolder(this.bFp).mo13049Vr().aug());
        }

        public void mouseClicked(MouseEvent mouseEvent) {
            if (mouseEvent.getButton() == 1 && mouseEvent.getClickCount() <= 1 && !this.bFp.eoS) {
                dmZ();
            }
        }

        public void mouseReleased(MouseEvent mouseEvent) {
            int i = 0;
            if (mouseEvent.getButton() == 1 && mouseEvent.getClickCount() <= 1 && !this.bFp.eoS && this.efC) {
                this.efC = false;
                mouseEvent.getComponent().setCursor(this.ixZ);
                TaskPaneContainer djq = this.bFp.djq();
                if (djq != null) {
                    djq.mo23033c(this.bFp);
                    while (true) {
                        int i2 = i;
                        if (i2 < this.bFp.ipk.size()) {
                            ((C2073d) this.bFp.ipk.get(i2)).mo17455cX(djq.mo23036l(this.bFp));
                            i = i2 + 1;
                        } else {
                            return;
                        }
                    }
                }
            }
        }

        public void mousePressed(MouseEvent mouseEvent) {
            this.ixW = mouseEvent.getY();
        }

        public void mouseDragged(MouseEvent mouseEvent) {
            if (!this.bFp.eoS) {
                this.iya = new Cursor(13);
                Component component = mouseEvent.getComponent();
                if (this.efC) {
                    TaskPaneContainer djq = this.bFp.djq();
                    if (djq != null) {
                        djq.mo23026b(this.bFp);
                        if (component.getCursor() != this.iya) {
                            component.setCursor(this.iya);
                        }
                    }
                } else if (this.f5909SZ == null && this.ixW + 3 <= Math.abs(mouseEvent.getY())) {
                    this.ixZ = component.getCursor();
                    component.setCursor(this.iya);
                    this.efC = true;
                }
            }
        }

        /* renamed from: a.bp$g$a */
        class C2077a implements C0454GJ {
            C2077a() {
            }

            /* renamed from: a */
            public void mo9a(JComponent jComponent) {
                TaskPaneContainer djq = C2076g.this.bFp.djq();
                if (djq != null) {
                    djq.validate();
                    djq.mo23035du(false);
                }
                C2076g.this.f5909SZ = null;
            }
        }

        /* renamed from: a.bp$g$b */
        /* compiled from: a */
        class C2078b implements C0454GJ {
            C2078b() {
            }

            /* renamed from: a */
            public void mo9a(JComponent jComponent) {
                C2076g.this.bFp.fuf.setVisible(false);
                TaskPaneContainer djq = C2076g.this.bFp.djq();
                if (djq != null) {
                    djq.validate();
                    djq.mo23035du(false);
                }
                C2076g.this.f5909SZ = null;
            }
        }

        /* renamed from: a.bp$g$c */
        /* compiled from: a */
        class C2079c implements C2740jL<Component> {
            C2079c() {
            }

            /* renamed from: a */
            public void mo7a(Component component, float f) {
                component.setSize(component.getWidth(), (int) f);
                C2076g.this.bFp.djq().validate();
            }

            /* renamed from: a */
            public float mo5a(Component component) {
                return (float) component.getHeight();
            }
        }
    }

    /* renamed from: a.bp$b */
    /* compiled from: a */
    private class C2071b extends MouseAdapter {
        private int bFn;
        private int bFo;

        private C2071b() {
            this.bFn = -1;
            this.bFo = 0;
        }

        /* synthetic */ C2071b(TaskPane bpVar, C2071b bVar) {
            this();
        }

        public void mousePressed(MouseEvent mouseEvent) {
            this.bFn = mouseEvent.getYOnScreen();
            this.bFo = TaskPane.this.getHeight();
        }

        public void mouseReleased(MouseEvent mouseEvent) {
            this.bFn = -1;
            TaskPane.this.setPreferredSize(TaskPane.this.getSize());
            int i = 0;
            while (true) {
                int i2 = i;
                if (i2 < TaskPane.this.ipj.size()) {
                    ((C2075f) TaskPane.this.ipj.get(i2)).mo17457l(this.bFo, TaskPane.this.getHeight());
                    i = i2 + 1;
                } else {
                    return;
                }
            }
        }

        public void mouseDragged(MouseEvent mouseEvent) {
            TaskPaneContainer djq;
            if (this.bFn >= 0 && (djq = TaskPane.this.djq()) != null) {
                int yOnScreen = (this.bFo + mouseEvent.getYOnScreen()) - this.bFn;
                if (TaskPane.this.m28012m(this.bFo, yOnScreen)) {
                    TaskPane.this.setSize(new Dimension(TaskPane.this.getWidth(), yOnScreen));
                    djq.validate();
                }
            }
        }
    }

    /* renamed from: a.bp$i */
    /* compiled from: a */
    class C2081i extends MouseAdapter {
        C2081i() {
        }

        public void mouseClicked(MouseEvent mouseEvent) {
            boolean z;
            if (mouseEvent.getClickCount() > 1) {
                JToggleButton component = (JToggleButton) mouseEvent.getComponent();
                if (((JToggleButton) mouseEvent.getComponent()).isSelected()) {
                    z = false;
                } else {
                    z = true;
                }
                component.setSelected(z);
                return;
            }
            TaskPane.this.mo17429aw(((JToggleButton) mouseEvent.getSource()).isSelected());
        }
    }
}
