package logic.ui.item;

import logic.res.XmlNode;
import logic.ui.BaseItemFactory;
import logic.ui.C6342alC;
import lombok.extern.slf4j.Slf4j;

import java.awt.*;

/* renamed from: a.akb  reason: case insensitive filesystem */
/* compiled from: a */
@Slf4j
public class GBoxCustom extends BaseItemFactory<GBox> {

    /* renamed from: C */
    public GBox creatComponentCustom(C6342alC alc, Container container, XmlNode agy) {
        return new GBox();
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public void mo361b(C6342alC alc, GBox arm, XmlNode agy) {
        super.createLayout(alc, arm, agy);
        Integer a = checkValueAttribute(agy, "cols", log);
        if (a != null) {
            arm.setColumns(a.intValue());
        }
    }
}
