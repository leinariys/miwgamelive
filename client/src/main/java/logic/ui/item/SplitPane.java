package logic.ui.item;

import logic.ui.C2698il;

import javax.swing.*;
import java.awt.*;

/* renamed from: a.aHZ */
/* compiled from: a */
public class SplitPane extends JSplitPane implements C2698il {

    private C2698il.C2699a bJg = new C2698il.C2699a(this);

    public void destroy() {
        if (getParent() != null) {
            getParent().remove(this);
        }
    }

    /* renamed from: cb */
    public JButton mo4913cb(String str) {
        JButton cb;
        JButton cb2;
        JButton leftComponent = (JButton) getLeftComponent();
        if ((leftComponent instanceof JButton) && str.equals(leftComponent.getName())) {
            return leftComponent;
        }
        if ((leftComponent instanceof C2698il) && (cb2 = ((C2698il) leftComponent).mo4913cb(str)) != null) {
            return cb2;
        }
        JButton rightComponent = (JButton) getRightComponent();
        if ((rightComponent instanceof JButton) && str.equals(rightComponent.getName())) {
            return rightComponent;
        }
        if (!(rightComponent instanceof C2698il) || (cb = ((C2698il) rightComponent).mo4913cb(str)) == null) {
            return null;
        }
        return cb;
    }

    /* renamed from: cc */
    public JComboBox mo4914cc(String str) {
        JComboBox cc;
        JComboBox cc2;
        JComboBox leftComponent = (JComboBox) getLeftComponent();
        if ((leftComponent instanceof JComboBox) && str.equals(leftComponent.getName())) {
            return leftComponent;
        }
        if ((leftComponent instanceof C2698il) && (cc2 = ((C2698il) leftComponent).mo4914cc(str)) != null) {
            return cc2;
        }
        JComboBox rightComponent = (JComboBox) getRightComponent();
        if ((rightComponent instanceof JComboBox) && str.equals(rightComponent.getName())) {
            return rightComponent;
        }
        if (!(rightComponent instanceof C2698il) || (cc = ((C2698il) rightComponent).mo4914cc(str)) == null) {
            return null;
        }
        return cc;
    }

    /* renamed from: cd */
    public Component mo4915cd(String str) {
        Component cd;
        Component cd2;
        C2698il leftComponent = (C2698il) getLeftComponent();
        if (str.equals(leftComponent.getElementName())) {
            return (Component) leftComponent;
        }
        if ((leftComponent instanceof C2698il) && (cd2 = leftComponent.mo4915cd(str)) != null) {
            return cd2;
        }
        C2698il rightComponent = (C2698il) getRightComponent();
        if (str.equals(rightComponent.getElementName())) {
            return (Component) rightComponent;
        }
        if (!(rightComponent instanceof C2698il) || (cd = rightComponent.mo4915cd(str)) == null) {
            return null;
        }
        return cd;
    }

    /* renamed from: ce */
    public C2698il mo4916ce(String str) {
        C2698il ce;
        C2698il ce2;
        C2698il leftComponent = (C2698il) getLeftComponent();
        if ((leftComponent instanceof C2698il) && str.equals(leftComponent.getElementName())) {
            return leftComponent;
        }
        if ((leftComponent instanceof C2698il) && (ce2 = leftComponent.mo4916ce(str)) != null) {
            return ce2;
        }
        C2698il rightComponent = (C2698il) getRightComponent();
        if ((rightComponent instanceof C2698il) && str.equals(rightComponent.getElementName())) {
            return rightComponent;
        }
        if (!(rightComponent instanceof C2698il) || (ce = rightComponent.mo4916ce(str)) == null) {
            return null;
        }
        return ce;
    }

    /* renamed from: cg */
    public Progress mo4918cg(String str) {
        Progress cg;
        for (Component bnVar : getComponents()) {
            if ((bnVar instanceof Progress) && str.equals(bnVar.getName())) {
                return (Progress) bnVar;
            }
            if ((bnVar instanceof C2698il) && (cg = ((C2698il) bnVar).mo4918cg(str)) != null) {
                return cg;
            }
        }
        return null;
    }

    /* renamed from: cf */
    public JLabel mo4917cf(String str) {
        JLabel cf;
        JLabel cf2;
        JLabel leftComponent = (JLabel) getLeftComponent();
        if ((leftComponent instanceof JLabel) && str.equals(leftComponent.getName())) {
            return leftComponent;
        }
        if ((leftComponent instanceof C2698il) && (cf2 = ((C2698il) leftComponent).mo4917cf(str)) != null) {
            return cf2;
        }
        JLabel rightComponent = (JLabel) getRightComponent();
        if ((rightComponent instanceof JLabel) && str.equals(rightComponent.getName())) {
            return rightComponent;
        }
        if (!(rightComponent instanceof C2698il) || (cf = ((C2698il) rightComponent).mo4917cf(str)) == null) {
            return null;
        }
        return cf;
    }

    /* renamed from: ch */
    public Repeater<?> mo4919ch(String str) {
        Repeater<?> ch;
        for (Component ilVar : getComponents()) {
            if ((ilVar instanceof C2698il) && (ch = ((C2698il) ilVar).mo4919ch(str)) != null) {
                return ch;
            }
        }
        return null;
    }

    /* renamed from: ci */
    public TextField mo4920ci(String str) {
        TextField ci;
        TextField ci2;
        TextField leftComponent = (TextField) getLeftComponent();
        if ((leftComponent instanceof TextField) && str.equals(leftComponent.getName())) {
            return leftComponent;
        }
        if ((leftComponent instanceof C2698il) && (ci2 = ((C2698il) leftComponent).mo4920ci(str)) != null) {
            return ci2;
        }
        TextField rightComponent = (TextField) getRightComponent();
        if ((rightComponent instanceof TextField) && str.equals(rightComponent.getName())) {
            return rightComponent;
        }
        if (!(rightComponent instanceof C2698il) || (ci = ((C2698il) rightComponent).mo4920ci(str)) == null) {
            return null;
        }
        return ci;
    }

    public void pack() {
        setSize(getPreferredSize());
        validate();
    }

    public String getElementName() {
        return "splitpane";
    }

    public void setEnabled(boolean z) {
        for (Component enabled : getComponents()) {
            enabled.setEnabled(z);
        }
        SplitPane.super.setEnabled(z);
    }

    public void setFocusable(boolean z) {
        for (Component focusable : getComponents()) {
            focusable.setFocusable(z);
        }
        SplitPane.super.setFocusable(z);
    }

    /* renamed from: Kk */
    public void mo4911Kk() {
        this.bJg.mo19774Kk();
    }

    /* renamed from: Kl */
    public void mo4912Kl() {
        this.bJg.mo19775Kl();
    }
}
