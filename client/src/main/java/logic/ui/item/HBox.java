package logic.ui.item;

import logic.ui.C3810vI;
import logic.ui.PropertiesUiFromCss;
import logic.ui.aRU;

import javax.swing.plaf.ComponentUI;
import java.awt.*;

/* renamed from: a.abv  reason: case insensitive filesystem */
/* compiled from: a */
public class HBox extends C2867lF {


    /* access modifiers changed from: protected */
    /* renamed from: b */
    public Dimension mo11122b(int i, int i2) {
        int componentCount = getComponentCount();
        int i3 = 0;
        int i4 = 0;
        for (int i5 = 0; i5 < componentCount; i5++) {
            Component component = getComponent(i5);
            if (component.isVisible()) {
                Dimension d = mo20189d(component, i, i2);
                i4 += d.width;
                i3 = Math.max(i3, d.height);
            }
        }
        return new Dimension(i4, i3);
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public Dimension mo11120a(int i, int i2) {
        int componentCount = getComponentCount();
        int i3 = 0;
        int i4 = 0;
        for (int i5 = 0; i5 < componentCount; i5++) {
            Component component = getComponent(i5);
            if (component.isVisible()) {
                Dimension c = mo20188c(component, i, i2);
                i4 += c.width;
                i3 = Math.max(i3, c.height);
            }
        }
        return new Dimension(i4, i3);
    }

    public void layout() {
        int i;
        int componentCount = getComponentCount();
        PropertiesUiFromCss a = PropertiesUiFromCss.m456a((ComponentUI) getUI());
        C3810vI atR = a.atR();
        aRU aty = a.aty();
        Insets amu = amu();
        Dimension size = getSize();
        Dimension dimension = new Dimension((size.width - amu.right) - amu.left, (size.height - amu.top) - amu.bottom);
        int amv = amv();
        int jp = a.ats().mo19908Iu().mo5756jp((float) dimension.width);
        int i2 = jp * (amv - 1);
        C2867lF.C2869b bVar = new C2867lF.C2869b((float) (dimension.width - i2), componentCount);
        for (int i3 = 0; i3 < componentCount; i3++) {
            bVar.mo20198b(i3, getComponent(i3), dimension.width - i2, dimension.height);
        }
        bVar.mo20199Mq();
        int t = aty.mo11143t(dimension.width, i2 + ((int) bVar.ghM));
        int i4 = 0;
        while (i4 < componentCount) {
            Component component = getComponent(i4);
            if (bVar.bjD[i4]) {
                Dimension dimension2 = bVar.bjC[i4];
                int i5 = bVar.ghH[i4];
                int min = Math.min(dimension.height, dimension2.height);
                Point a2 = mo20182a(component, dimension.width, dimension.height);
                int t2 = atR.mo16934t(dimension.height, min);
                if (atR == C3810vI.FILL) {
                    min = dimension.height;
                }
                component.setBounds(a2.x + t + amu.left, a2.y + t2 + amu.top, i5, min);
                i = i5 + jp + t;
            } else {
                i = t;
            }
            i4++;
            t = i;
        }
    }

    public String getElementName() {
        return "hbox";
    }
}
