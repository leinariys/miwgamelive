package logic.ui.item;

import logic.swing.BorderWrapper;
import logic.ui.C1276Sr;

import javax.swing.*;
import javax.swing.table.JTableHeader;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableColumnModel;
import java.awt.*;
import java.util.HashMap;
import java.util.Map;

/* renamed from: a.df */
/* compiled from: a */
public class C2289df extends JTableHeader implements C1276Sr {

    /* access modifiers changed from: private */
    public static C2289df hac = null;

    public C2289df(TableColumnModel tableColumnModel) {
        super(tableColumnModel);
        hac = this;
        setBorder(new BorderWrapper());
        setBackground((Color) null);
    }

    /* access modifiers changed from: protected */
    public TableCellRenderer createDefaultRenderer() {
        return new C2290a();
    }

    public String getElementName() {
        return "table-header";
    }

    /* renamed from: a.df$b */
    /* compiled from: a */
    public static final class C2291b extends JViewport implements C1276Sr {

        private boolean isValid = false;

        public C2291b() {
            setBackground(new Color(0, 0, 0, 0));
        }

        public void validate() {
            if (!this.isValid) {
                this.isValid = true;
                getParent().validate();
                this.isValid = false;
            }
        }

        public String getElementName() {
            return "background";
        }
    }

    /* renamed from: a.df$a */
    public class C2290a implements TableCellRenderer {


        /* renamed from: yk */
        private Map<Object, JLabel> f6577yk = new HashMap();

        public C2290a() {
        }

        public Component getTableCellRendererComponent(JTable jTable, Object obj, boolean z, boolean z2, int i, int i2) {
            JLabel jLabel = this.f6577yk.get(obj);
            if (jLabel != null) {
                return jLabel;
            }
            Component jLabel2 = new JLabel((String) obj);
            C2289df.hac.add(jLabel2);
            this.f6577yk.put(obj, (JLabel) jLabel2);
            return jLabel2;
        }
    }
}
