package logic.ui.item;

import logic.res.ILoaderImageInterface;
import logic.swing.C5378aHa;
import logic.ui.C1276Sr;

import javax.swing.*;
import java.awt.*;

/* renamed from: a.axm  reason: case insensitive filesystem */
/* compiled from: a */
public class Picture extends JLabel implements C1276Sr {
    private static final String PREFIX = "res://";

    private ILoaderImageInterface gPv;
    private Dimension gPw;
    private float gPx;

    public Picture() {
        this.gPx = 0.0f;
        setVerticalTextPosition(3);
        setHorizontalTextPosition(0);
        this.gPw = new Dimension(-1, -1);
    }

    public Picture(ILoaderImageInterface azb) {
        this();
        this.gPv = azb;
    }

    /* renamed from: a */
    public static void m27173a(Picture axm, Dimension dimension) {
        Dimension imageSize = axm.getImageSize();
        float f = ((float) dimension.height) / ((float) imageSize.height);
        float f2 = ((float) dimension.width) / ((float) imageSize.width);
        if (f > f2) {
            axm.setImageSize((int) Math.ceil((double) (((float) imageSize.width) * f)), dimension.height);
        } else {
            axm.setImageSize(dimension.width, (int) Math.ceil((double) (((float) imageSize.height) * f2)));
        }
    }

    public Dimension getImageSize() {
        Icon icon = getIcon();
        if (icon == null || this.gPw.height >= 0 || this.gPw.width >= 0) {
            return this.gPw;
        }
        return new Dimension(icon.getIconWidth(), icon.getIconHeight());
    }

    public void setImageSize(int i, int i2) {
        this.gPw.setSize(i, i2);
        if (getIcon() instanceof ImageIcon) {
            mo16826b(m27174d(((ImageIcon) getIcon()).getImage()));
        }
    }

    /* renamed from: aw */
    public void mo16825aw(String str) {
        if (str.startsWith(PREFIX)) {
            str = str.substring(PREFIX.length());
        }
        setImage(this.gPv.getImage(str));
    }

    public void setImage(Image image) {
        if (image != null) {
            if (this.gPw != null && this.gPw.width >= 0 && this.gPw.height >= 0) {
                image = m27174d(image);
            }
            mo16826b(image);
        }
    }

    /* access modifiers changed from: protected */
    /* renamed from: b */
    public void mo16826b(Image image) {
        setIcon(new ImageIcon(image));
    }

    /* renamed from: d */
    private Image m27174d(Image image) {
        return image.getScaledInstance(this.gPw.width, this.gPw.height, 2);
    }

    /* renamed from: a */
    public void mo16824a(C5378aHa aha, String str) {
        setImage(this.gPv.getImage(String.valueOf(aha.getPath()) + str));
    }

    public Icon cCx() {
        return getIcon();
    }

    public void cCy() {
        setIcon((Icon) null);
    }

    public String getElementName() {
        return "picture";
    }

    public void paint(Graphics graphics) {
        if (this.gPx != 0.0f) {
            Graphics2D create = (Graphics2D) graphics.create();
            create.rotate((double) this.gPx, (double) (getWidth() / 2), (double) (getHeight() / 2));
            Picture.super.paint(create);
            return;
        }
        Picture.super.paint(graphics);
    }

    public float cCz() {
        return this.gPx;
    }

    /* renamed from: kU */
    public void mo16831kU(float f) {
        this.gPx = f;
    }
}
