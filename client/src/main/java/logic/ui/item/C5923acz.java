package logic.ui.item;

import logic.swing.C3940wz;

import javax.swing.*;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.DefaultTreeModel;
import javax.swing.tree.MutableTreeNode;
import javax.swing.tree.TreePath;
import java.awt.event.MouseEvent;

/* renamed from: a.acz  reason: case insensitive filesystem */
/* compiled from: a */
public class C5923acz extends JTree {

    private DefaultMutableTreeNode fbY = new DefaultMutableTreeNode("hidden root");
    private TreePath fbZ = new TreePath(this.fbY);
    private DefaultTreeModel fca = new DefaultTreeModel(this.fbY);

    public C5923acz() {
        setCellRenderer(new C1303TE());
        setModel(this.fca);
        setToggleClickCount(1);
        setRowHeight(0);
        setRootVisible(false);
        setShowsRootHandles(true);
    }

    /* renamed from: a */
    public void mo12740a(MutableTreeNode mutableTreeNode) {
        this.fbY.add(mutableTreeNode);
    }

    public void bOW() {
        this.fbY.removeAllChildren();
        this.fca.reload();
    }

    public void bOX() {
        expandPath(this.fbZ);
    }

    public void expandAll() {
        expandPath(this.fbZ);
        for (int i = 0; i < getRowCount(); i++) {
            expandRow(i);
        }
    }

    public TreePath bOY() {
        return this.fbZ;
    }

    public String getToolTipText(MouseEvent mouseEvent) {
        TreePath pathForLocation;
        DefaultMutableTreeNode defaultMutableTreeNode;
        Object userObject;
        if (C3940wz.m40778d(this) == null || (pathForLocation = getPathForLocation(mouseEvent.getPoint().x, mouseEvent.getPoint().y)) == null || (defaultMutableTreeNode = (DefaultMutableTreeNode) pathForLocation.getLastPathComponent()) == null || (userObject = defaultMutableTreeNode.getUserObject()) == null) {
            return C5923acz.super.getToolTipText(mouseEvent);
        }
        return userObject.toString();
    }
}
