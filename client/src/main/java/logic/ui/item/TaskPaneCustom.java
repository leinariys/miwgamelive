package logic.ui.item;

import logic.res.XmlNode;
import logic.ui.BaseItemFactory;
import logic.ui.C6342alC;

import java.awt.*;

/* renamed from: a.mO */
/* compiled from: a */
public class TaskPaneCustom extends BaseItemFactory<TaskPane> {
    /* access modifiers changed from: protected */
    /* renamed from: k */
    public TaskPane creatComponentCustom(C6342alC alc, Container container, XmlNode agy) {
        return new TaskPane();
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public void mo361b(C6342alC alc, TaskPane bpVar, XmlNode agy) {
        super.createLayout(alc, bpVar, agy);
        String attribute = agy.getAttribute("title");
        if (attribute != null) {
            bpVar.setTitle(attribute);
        }
        if ("false".equals(agy.getAttribute("pinnable"))) {
            bpVar.mo17444jR(false);
        }
        if ("true".equals(agy.getAttribute("collapsed"))) {
            bpVar.collapse();
        } else if ("true".equals(agy.getAttribute("pinned")) && bpVar.mo17419LW()) {
            bpVar.mo17429aw(true);
        }
        if ("false".equals(agy.getAttribute("resizable"))) {
            bpVar.setResizable(false);
        }
    }
}
