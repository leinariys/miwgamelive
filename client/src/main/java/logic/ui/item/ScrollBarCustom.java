package logic.ui.item;

import logic.res.XmlNode;
import logic.ui.C6342alC;
import lombok.extern.slf4j.Slf4j;

import javax.swing.*;
import java.awt.*;

/* renamed from: a.aCg  reason: case insensitive filesystem */
/* compiled from: a */
@Slf4j
public class ScrollBarCustom extends BaseItem<JScrollBar> {
    /* access modifiers changed from: protected */
    /* renamed from: P */
    public JScrollBar creatComponentCustom(C6342alC alc, Container container, XmlNode agy) {
        int i;
        if ("vertical".equals(agy.getAttribute("orientation"))) {
            i = 1;
        } else {
            i = 0;
        }
        return new JScrollBar(i);
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public void createLayout(C6342alC alc, JScrollBar jScrollBar, XmlNode agy) {
        super.createLayout(alc, jScrollBar, agy);
        Integer a = checkValueAttribute(agy, "min", log);
        if (a != null) {
            jScrollBar.setMinimum(a.intValue());
        }
        Integer a2 = checkValueAttribute(agy, "max", log);
        if (a2 != null) {
            jScrollBar.setMaximum(a2.intValue());
        }
        Integer a3 = checkValueAttribute(agy, "value", log);
        if (a3 != null) {
            jScrollBar.setValue(a3.intValue());
        }
        Integer a4 = checkValueAttribute(agy, "major-ticks", log);
        if (a4 != null) {
            jScrollBar.setBlockIncrement(a4.intValue());
        }
        Integer a5 = checkValueAttribute(agy, "minor-ticks", log);
        if (a5 != null) {
            jScrollBar.setUnitIncrement(a5.intValue());
        }
    }
}
