package logic.ui.item;

import logic.ui.PropertiesUiFromCss;
import taikodom.render.graphics2d.RGraphics2;

import javax.swing.*;
import java.awt.*;

/* renamed from: a.aOA */
/* compiled from: a */
public class aOA extends JLabel {
    private boolean izI;

    public aOA() {
        setFocusable(false);
    }

    /* renamed from: jV */
    public void mo10543jV(boolean z) {
        this.izI = z;
    }

    public boolean dnK() {
        return this.izI;
    }

    /* access modifiers changed from: protected */
    public void firePropertyChange(String str, Object obj, Object obj2) {
        if (!dnK()) {
            aOA.super.firePropertyChange(str, obj, obj2);
        } else if (str == "text" || !((str != "font" && str != "foreground") || obj == obj2 || getClientProperty("html") == null)) {
            aOA.super.firePropertyChange(str, obj, obj2);
        }
    }

    public void paint(Graphics graphics) {
        PropertiesUiFromCss g;
        Color colorMultiplier;
        if (!(!(graphics instanceof RGraphics2) || (g = PropertiesUiFromCss.m462g(this)) == null || (colorMultiplier = g.getColorMultiplier()) == null)) {
            ((RGraphics2) graphics).setColorMultiplier(colorMultiplier);
        }
        aOA.super.paint(graphics);
    }
}
