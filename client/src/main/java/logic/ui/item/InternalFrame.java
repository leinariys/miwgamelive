package logic.ui.item;

import logic.swing.C0454GJ;
import logic.swing.C3940wz;
import logic.swing.InternalFrameUI;
import logic.swing.aDX;
import logic.ui.Panel;
import logic.ui.*;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ComponentEvent;
import java.awt.event.MouseListener;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/* renamed from: a.nx */
/* compiled from: a */
public class InternalFrame extends JInternalFrame implements C2698il {
    /* renamed from: SZ */
    public aDX f8749SZ;
    public List<Repeater<?>> buZ;
    private boolean gmA;
    private List<MouseListener> gmB;
    private boolean gmC;
    private boolean gmD;
    private boolean gmE;
    private boolean gmF;
    private boolean gmG;
    private C2877lL gmH;
    private C2877lL gmI;

    public InternalFrame() {
        this(true);
    }

    public InternalFrame(boolean z) {
        this.gmA = false;
        this.buZ = new ArrayList();
        this.gmC = true;
        this.gmD = true;
        this.gmE = false;
        this.f8749SZ = null;
        this.gmF = true;
        this.gmG = true;
        setFrameIcon((Icon) null);
        setBackground(new Color(0, 0, 0, 0));
        setContentPane(new C3112a());
        setTransferHandler(new C3117f());
        m36522fU(false);
        setDefaultCloseOperation(1);
        if (z) {
            cpS();
        }
    }

    private void cpS() {
    }

    /* renamed from: b */
    private synchronized void m36519b(C0454GJ gj) {
        if (this.f8749SZ != null) {
            this.f8749SZ.kill();
        }
        if (this.gmI == null) {
            setAlpha(0);
            this.f8749SZ = new aDX(this, "[0..255] dur 150", C2830kk.asS, new C3114c(gj));
        } else {
            this.gmI.mo20216iT();
            this.f8749SZ = new aDX(this, this.gmI.mo20218iV(), this.gmI.mo20217iU(), new C3113b(this.gmI.mo20219iW()));
        }
    }

    /* renamed from: c */
    private void m36521c(C0454GJ gj) {
        if (this.f8749SZ != null) {
            this.f8749SZ.kill();
        }
        if (this.gmH == null) {
            this.f8749SZ = new aDX(this, "[255..0] dur 150", C2830kk.asS, new C3116e(gj));
            return;
        }
        this.gmH.mo20216iT();
        this.f8749SZ = new aDX(this, this.gmH.mo20218iV(), this.gmH.mo20217iU(), new C3115d(this.gmH.mo20219iW()));
    }

    public boolean isAnimating() {
        return this.f8749SZ != null;
    }

    /* access modifiers changed from: private */
    /* renamed from: fU */
    public void m36522fU(boolean z) {
        InternalFrame.super.setVisible(z);
    }

    /* renamed from: la */
    public IComponentManager mo20882la() {
        return ComponentManager.getCssHolder(this);
    }

    public boolean isModal() {
        return this.gmA;
    }

    public void setModal(boolean z) {
        boolean z2;
        boolean z3 = false;
        if (z != this.gmA) {
            if (isVisible()) {
                IBaseUiTegXml.initBaseUItegXML().mo13705a(this, z);
                requestFocus();
            }
            if (z) {
                z2 = false;
            } else {
                z2 = true;
            }
            this.gmF = z2;
            if (!z) {
                z3 = true;
            }
            this.gmG = z3;
            this.gmA = z;
        }
    }

    /* renamed from: fV */
    public void mo20875fV(boolean z) {
        if (this.gmD && z != this.gmC) {
            JComponent northPane = ((InternalFrameUI) this.ui).getNorthPane();
            if (!z) {
                if (this.gmB == null) {
                    this.gmB = new ArrayList();
                } else {
                    this.gmB.clear();
                }
                for (MouseListener mouseListener : northPane.getMouseListeners()) {
                    this.gmB.add(mouseListener);
                    northPane.removeMouseListener(mouseListener);
                }
            } else if (this.gmB != null && this.gmB.size() > 0) {
                for (MouseListener addMouseListener : this.gmB) {
                    northPane.addMouseListener(addMouseListener);
                }
                this.gmB.clear();
            }
            this.gmC = z;
        }
    }

    public boolean cpT() {
        return this.gmC;
    }

    public boolean isAlwaysOnTop() {
        return this.gmE;
    }

    public void setAlwaysOnTop(boolean z) {
        Integer num;
        if (z != this.gmE) {
            if (z) {
                num = JLayeredPane.PALETTE_LAYER;
            } else {
                num = JLayeredPane.DEFAULT_LAYER;
            }
            setLayer(num);
            this.gmE = z;
        }
    }

    public void setVisible(boolean z) {
        mo20868b(z, (C0454GJ) null);
    }

    /* renamed from: b */
    public void mo20868b(boolean z, C0454GJ gj) {
        if (z == isVisible()) {
            if (this.f8749SZ != null) {
                this.f8749SZ.cAD();
            } else if (gj != null) {
                gj.mo9a(this);
                return;
            } else {
                return;
            }
        }
        if (z) {
            if (this.gmF) {
                m36519b(gj);
            } else {
                m36522fU(true);
                if (gj != null) {
                    gj.mo9a(this);
                }
            }
        }
        if (this.gmA) {
            IBaseUiTegXml.initBaseUItegXML().mo13705a(this, z);
            if (z) {
                processComponentEvent(new ComponentEvent(this, 102));
                requestFocus();
            } else {
                processComponentEvent(new ComponentEvent(this, 103));
            }
        }
        if (z) {
            return;
        }
        if (this.gmG) {
            m36521c(gj);
            return;
        }
        m36522fU(false);
        if (gj != null) {
            gj.mo9a(this);
        }
    }

    /* renamed from: fW */
    public void mo20876fW(boolean z) {
        if (this.f8749SZ != null) {
            this.f8749SZ.kill();
        }
        setAlpha(255);
        m36522fU(z);
    }

    public void center() {
        if (getParent() != null) {
            setLocation((getParent().getWidth() - getWidth()) / 2, (getParent().getHeight() - getHeight()) / 2);
        }
    }

    public void cpU() {
        this.gmD = false;
        ((InternalFrameUI) this.ui).setNorthPane((JComponent) null);
    }

    public void destroy() {
        dispose();
        if (getParent() != null) {
            getParent().remove(this);
        }
    }

    /* renamed from: cb */
    public JButton mo4913cb(String str) {
        JButton cb;
        for (Component jButton : getContentPane().getComponents()) {
            if ((jButton instanceof JButton) && str.equals(jButton.getName())) {
                return (JButton) jButton;
            }
            if ((jButton instanceof C2698il) && (cb = ((C2698il) jButton).mo4913cb(str)) != null) {
                return cb;
            }
        }
        return null;
    }

    /* renamed from: cg */
    public Progress mo4918cg(String str) {
        Progress cg;
        for (Component bnVar : getContentPane().getComponents()) {
            if ((bnVar instanceof Progress) && str.equals(bnVar.getName())) {
                return (Progress) bnVar;
            }
            if ((bnVar instanceof C2698il) && (cg = ((C2698il) bnVar).mo4918cg(str)) != null) {
                return cg;
            }
        }
        return null;
    }

    /* renamed from: cf */
    public JLabel mo4917cf(String str) {
        JLabel cf;
        for (Component jLabel : getContentPane().getComponents()) {
            if ((jLabel instanceof JLabel) && str.equals(jLabel.getName())) {
                return (JLabel) jLabel;
            }
            if ((jLabel instanceof C2698il) && (cf = ((C2698il) jLabel).mo4917cf(str)) != null) {
                return cf;
            }
        }
        return null;
    }

    /* renamed from: ci */
    public TextField mo4920ci(String str) {
        TextField ci;
        for (Component ahw : getContentPane().getComponents()) {
            if ((ahw instanceof TextField) && str.equals(ahw.getName())) {
                return (TextField) ahw;
            }
            if ((ahw instanceof C2698il) && (ci = ((C2698il) ahw).mo4920ci(str)) != null) {
                return ci;
            }
        }
        return null;
    }

    /* renamed from: cd */
    public <T extends Component> T mo4915cd(String str) {
        T cd;
        Iterator<Repeater<?>> it = this.buZ.iterator();
        while (it.hasNext()) {
            T t = (T) it.next();
            if (str.equals(t.getName())) {
                return t;
            }
        }
        for (Component t2 : getContentPane().getComponents()) {
            if (str.equals(t2.getName())) {
                return (T) t2;
            }
            if ((t2 instanceof C2698il) && (cd = ((C2698il) t2).mo4915cd(str)) != null) {
                return cd;
            }
        }
        return null;
    }

    /* renamed from: ce */
    public C2698il mo4916ce(String str) {
        for (Component ilVar : getContentPane().getComponents()) {
            if (ilVar instanceof C2698il) {
                if (str.equals(ilVar.getName())) {
                    return (C2698il) ilVar;
                }
                C2698il ce = ((C2698il) ilVar).mo4916ce(str);
                if (ce != null) {
                    return ce;
                }
            }
        }
        return null;
    }

    /* renamed from: ch */
    public Repeater<?> mo4919ch(String str) {
        Repeater<?> ch;
        if (str == null) {
            return null;
        }
        for (Repeater<?> next : this.buZ) {
            if (str.equals(next.getName())) {
                return next;
            }
        }
        for (Component ilVar : getContentPane().getComponents()) {
            if ((ilVar instanceof C2698il) && (ch = ((C2698il) ilVar).mo4919ch(str)) != null) {
                return ch;
            }
        }
        return null;
    }

    /* renamed from: a */
    public void mo20865a(Repeater<?> tYVar) {
        this.buZ.add(tYVar);
    }

    /* renamed from: cc */
    public JComboBox mo4914cc(String str) {
        JComboBox cc;
        for (Component jComboBox : getContentPane().getComponents()) {
            if ((jComboBox instanceof JComboBox) && str.equals(jComboBox.getName())) {
                return (JComboBox) jComboBox;
            }
            if ((jComboBox instanceof C2698il) && (cc = ((C2698il) jComboBox).mo4914cc(str)) != null) {
                return cc;
            }
        }
        return null;
    }

    public void pack() {
        setSize(getPreferredSize());
        validate();
    }

    public void setEnabled(boolean z) {
        for (Component enabled : getContentPane().getComponents()) {
            enabled.setEnabled(z);
        }
        getContentPane().setEnabled(z);
        InternalFrame.super.setEnabled(z);
    }

    public void setFocusable(boolean z) {
        for (Component focusable : getContentPane().getComponents()) {
            focusable.setFocusable(z);
        }
        getContentPane().setFocusable(z);
        InternalFrame.super.setFocusable(z);
    }

    /* access modifiers changed from: protected */
    /* renamed from: e */
    public void mo20874e(Container container) {
        synchronized (getTreeLock()) {
            int componentCount = container.getComponentCount();
            for (int i = 0; i < componentCount; i++) {
                Container component = (Container) container.getComponent(i);
                if (component instanceof Container) {
                    mo20874e(component);
                } else if (component.isValid()) {
                    component.invalidate();
                }
            }
            if (container.isValid()) {
                container.invalidate();
            }
        }
    }

    public String getElementName() {
        return "window";
    }

    public void close() {
        doDefaultCloseAction();
    }

    /* renamed from: jD */
    public void mo20881jD(String str) {
        IComponentManager e = ComponentManager.getCssHolder(this);
        e.setAttribute("class", str);
        e.mo13045Vk();
    }

    public int getAlpha() {
        Color colorMultiplier;
        PropertiesUiFromCss g = PropertiesUiFromCss.m462g(this);
        if (g == null || (colorMultiplier = g.getColorMultiplier()) == null) {
            return 255;
        }
        return colorMultiplier.getAlpha();
    }

    public void setAlpha(int i) {
        PropertiesUiFromCss g = PropertiesUiFromCss.m462g(this);
        if (g != null) {
            g.setColorMultiplier(new Color(i, i, i, i));
        }
    }

    public void removeNotify() {
        if (getParent() != null) {
            getParent().repaint(getX(), getY(), getWidth(), getHeight());
        }
        InternalFrame.super.removeNotify();
    }

    public C3940wz cpV() {
        return (C3940wz) getClientProperty(C3940wz.bDU);
    }

    /* renamed from: a */
    public void mo20866a(C3940wz wzVar) {
        putClientProperty(C3940wz.bDU, wzVar);
    }

    /* renamed from: a */
    public void mo20864a(C2877lL lLVar) {
        this.gmH = lLVar;
    }

    /* renamed from: b */
    public void mo20867b(C2877lL lLVar) {
        this.gmI = lLVar;
    }

    /* renamed from: Kk */
    public void mo4911Kk() {
        ((C2698il) getContentPane()).mo4911Kk();
    }

    /* renamed from: Kl */
    public void mo4912Kl() {
        ((C2698il) getContentPane()).mo4912Kl();
    }

    /* renamed from: a.nx$a */
    private final class C3112a extends Panel {


        public C3112a() {
            setName("contentpane");
            setLayout(new BorderLayout());
        }

        public String getElementName() {
            return "contentpane";
        }
    }

    /* renamed from: a.nx$f */
    /* compiled from: a */
    class C3117f extends TransferHandler {


        C3117f() {
        }

        public boolean canImport(TransferHandler.TransferSupport transferSupport) {
            return false;
        }
    }

    /* renamed from: a.nx$c */
    /* compiled from: a */
    class C3114c implements C0454GJ {
        private final /* synthetic */ C0454GJ hEO;

        C3114c(C0454GJ gj) {
            this.hEO = gj;
        }

        /* renamed from: a */
        public void mo9a(JComponent jComponent) {
            InternalFrame.this.m36522fU(true);
            InternalFrame.this.f8749SZ = null;
            if (this.hEO != null) {
                this.hEO.mo9a(jComponent);
            }
        }
    }

    /* renamed from: a.nx$b */
    /* compiled from: a */
    class C3113b implements C0454GJ {
        private final /* synthetic */ C0454GJ ing;

        C3113b(C0454GJ gj) {
            this.ing = gj;
        }

        /* renamed from: a */
        public void mo9a(JComponent jComponent) {
            InternalFrame.this.m36522fU(true);
            InternalFrame.this.f8749SZ = null;
            if (this.ing != null) {
                this.ing.mo9a(jComponent);
            }
        }
    }

    /* renamed from: a.nx$e */
    /* compiled from: a */
    class C3116e implements C0454GJ {
        private final /* synthetic */ C0454GJ hEO;

        C3116e(C0454GJ gj) {
            this.hEO = gj;
        }

        /* renamed from: a */
        public void mo9a(JComponent jComponent) {
            InternalFrame.this.m36522fU(false);
            InternalFrame.this.f8749SZ = null;
            if (this.hEO != null) {
                this.hEO.mo9a(jComponent);
            }
        }
    }

    /* renamed from: a.nx$d */
    /* compiled from: a */
    class C3115d implements C0454GJ {
        private final /* synthetic */ C0454GJ ing;

        C3115d(C0454GJ gj) {
            this.ing = gj;
        }

        /* renamed from: a */
        public void mo9a(JComponent jComponent) {
            InternalFrame.this.m36522fU(true);
            InternalFrame.this.f8749SZ = null;
            if (this.ing != null) {
                this.ing.mo9a(jComponent);
            }
        }
    }
}
