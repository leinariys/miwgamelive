package logic.ui.item;

import logic.res.XmlNode;
import logic.ui.C6342alC;
import lombok.extern.slf4j.Slf4j;

import java.awt.*;

/* renamed from: a.IU */
/* compiled from: a */
@Slf4j
public class IconViewerCustom extends BaseItem<IconViewer> {

    /* access modifiers changed from: protected */
    /* renamed from: r */
    public IconViewer creatComponentCustom(C6342alC alc, Container container, XmlNode agy) {
        return new IconViewer(alc.getBaseUiTegXml().adz());
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public void createLayout(C6342alC alc, IconViewer nFVar, XmlNode agy) {
        int i = -1;
        super.createLayout(alc, nFVar, agy);
        Integer a = checkValueAttribute(agy, "width", log);
        Integer valueOf = Integer.valueOf(a == null ? -1 : a.intValue());
        Integer a2 = checkValueAttribute(agy, "height", log);
        if (a2 != null) {
            i = a2.intValue();
        }
        nFVar.setImageSize(valueOf.intValue(), Integer.valueOf(i).intValue());
        String attribute = agy.getAttribute("src");
        if (attribute != null) {
            nFVar.mo16825aw(attribute);
        }
        String attribute2 = agy.getAttribute("text");
        if (attribute2 != null) {
            nFVar.setText(attribute2);
        }
        Integer a3 = checkValueAttribute(agy, "edge", log);
        if (a3 != null) {
            nFVar.mo20659lB(a3.intValue());
        }
    }
}
