package logic.ui.item;

import logic.res.XmlNode;
import logic.ui.C6342alC;

import java.awt.*;

/* renamed from: a.ana  reason: case insensitive filesystem */
/* compiled from: a */
public final class EditorCustom extends TextComponent<Editor> {
    /* renamed from: F */
    public Editor creatComponentCustom(C6342alC alc, Container container, XmlNode agy) {
        return new Editor();
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public void mo7340a(C6342alC alc, Editor l, XmlNode agy) {
        String attribute = agy.getAttribute("contentType");
        if (attribute != null) {
            l.setContentType(attribute);
        }
        super.createLayout(alc, l, agy);
        if ("false".equalsIgnoreCase(agy.getAttribute("editable"))) {
            l.setEditable(false);
        }
        XmlNode c = agy.findNodeChild(0, "text", (String) null, (String) null);
        if (c != null) {
            l.setText(c.getText());
        }
    }
}
