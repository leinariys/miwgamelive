package logic.ui.item;

import logic.res.XmlNode;
import logic.ui.BaseItemFactory;
import logic.ui.C6342alC;

import java.awt.*;

/* renamed from: a.Ad */
/* compiled from: a */
public class TabbedPaneCustom extends BaseItemFactory<TabbedPane> {
    /* access modifiers changed from: protected */
    /* renamed from: p */
    public TabbedPane creatComponentCustom(C6342alC alc, Container container, XmlNode agy) {
        return new TabbedPane();
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public void creatAllComponentCustom(C6342alC alc, TabbedPane qb, XmlNode agy) {
        for (XmlNode next : agy.getListChildrenTag()) {
            if ("tab".equals(next.getTagName())) {
                String attribute = next.getAttribute("title");
                if (attribute == null) {
                    attribute = "";
                }
                qb.addTab(attribute, new C0055a(this, (C0055a) null).creatUi(alc, qb, next));
            }
        }
    }

    /* access modifiers changed from: protected */
    /* renamed from: b */
    public void mo361b(C6342alC alc, TabbedPane qb, XmlNode agy) {
        super.createLayout(alc, qb, agy);
        String attribute = agy.getAttribute("tab-placement");
        if (attribute != null) {
            if ("bottom".equals(attribute)) {
                qb.setTabPlacement(3);
            } else if ("left".equals(attribute)) {
                qb.setTabPlacement(2);
            } else if ("right".equals(attribute)) {
                qb.setTabPlacement(4);
            } else if ("top".equals(attribute)) {
                qb.setTabPlacement(1);
            }
        }
    }

    /* renamed from: a.Ad$a */
    private class C0055a extends BaseItemFactory<TabbedPane.C1098a> {
        private C0055a() {
        }

        /* synthetic */ C0055a(TabbedPaneCustom ad, C0055a aVar) {
            this();
        }

        /* access modifiers changed from: protected */
        /* renamed from: G */
        public TabbedPane.C1098a creatComponentCustom(C6342alC alc, Container container, XmlNode agy) {
            return new TabbedPane.C1098a();
        }
    }
}
