package logic.ui.item;

import logic.res.XmlNode;
import logic.ui.C6342alC;
import lombok.extern.slf4j.Slf4j;

import java.awt.*;

/* renamed from: a.aNB */
/* compiled from: a */
@Slf4j
public class PasswordfieldCustom extends TextComponent<PasswordField> {

    /* renamed from: T */
    public PasswordField creatComponentCustom(C6342alC alc, Container container, XmlNode agy) {
        return new PasswordField();
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public void mo7340a(C6342alC alc, PasswordField anh, XmlNode agy) {
        char c;
        super.createLayout(alc, anh, agy);
        String attribute = agy.getAttribute("echo-char");
        if (attribute == null || attribute.equals("")) {
            c = '*';
        } else {
            c = attribute.charAt(0);
        }
        anh.setEchoChar(c);
        Integer a = checkValueAttribute(agy, "cols", log);
        if (a != null) {
            anh.setColumns(a.intValue());
        }
    }
}
