package logic.ui.layout;

import com.steadystate.css.parser.CSSOMParser;
import logic.thred.LogPrinter;
import org.w3c.css.sac.InputSource;
import org.w3c.dom.css.CSSStyleDeclaration;

import java.awt.*;
import java.io.Reader;
import java.io.StringReader;

/* renamed from: a.adA  reason: case insensitive filesystem */
/* compiled from: a */
public class BorderLayoutCustom extends BaseLayoutCssValue {
    private static LogPrinter log = LogPrinter.m10275K(BorderLayoutCustom.class);

    /* renamed from: a */
    public LayoutManager createLayout(Container container, String str) {
        BorderLayout borderLayout = new BorderLayout();
        if (str != null) {
            String trim = str.trim();
            if (!trim.isEmpty()) {
                try {
                    CSSStyleDeclaration d = new CSSOMParser().parseStyleDeclaration(new InputSource((Reader) new StringReader(trim)));
                    borderLayout.setHgap(mo15148a(d, "hgap", borderLayout.getHgap()));
                    borderLayout.setVgap(mo15148a(d, "vgap", borderLayout.getVgap()));
                } catch (Exception e) {
                    log.warn(e);
                }
            }
        }
        return borderLayout;
    }
}
