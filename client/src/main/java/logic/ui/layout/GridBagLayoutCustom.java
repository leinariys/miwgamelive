package logic.ui.layout;

import com.steadystate.css.parser.CSSOMParser;
import gnu.trove.TObjectIntHashMap;
import lombok.extern.slf4j.Slf4j;
import org.w3c.css.sac.InputSource;
import org.w3c.dom.css.CSSStyleDeclaration;

import java.awt.*;
import java.io.Reader;
import java.io.StringReader;

/* renamed from: a.adB  reason: case insensitive filesystem */
/* compiled from: a */
@Slf4j
public class GridBagLayoutCustom extends BaseLayoutCssValue {
    /* renamed from: oY */
    private static final TObjectIntHashMap<String> limitation = new TObjectIntHashMap<>();

    static {
        limitation.put("ABOVE_BASELINE", 1024);
        limitation.put("ABOVE_BASELINE_LEADING", 1280);
        limitation.put("ABOVE_BASELINE_TRAILING", 1536);
        limitation.put("BASELINE", 256);
        limitation.put("BASELINE_LEADING", 512);
        limitation.put("BASELINE_TRAILING", 768);
        limitation.put("BELOW_BASELINE", 1792);
        limitation.put("BELOW_BASELINE_LEADING", 2048);
        limitation.put("BELOW_BASELINE_TRAILING", 2304);
        limitation.put("CENTER", 10);
        limitation.put("EAST", 13);
        limitation.put("FIRST_LINE_END", 24);
        limitation.put("FIRST_LINE_START", 23);
        limitation.put("LAST_LINE_END", 26);
        limitation.put("LAST_LINE_START ", 25);
        limitation.put("LINE_END", 22);
        limitation.put("LINE_START", 21);
        limitation.put("NORTH", 11);
        limitation.put("NORTHEAST", 12);
        limitation.put("NORTHWEST", 18);
        limitation.put("PAGE_END", 20);
        limitation.put("PAGE_START", 19);
        limitation.put("SOUTH", 15);
        limitation.put("SOUTHEAST", 14);
        limitation.put("SOUTHWEST", 16);
        limitation.put("WEST", 17);
        limitation.put("NONE", 0);
        limitation.put("HORIZONTAL", 2);
        limitation.put("VERTICAL", 3);
        limitation.put("BOTH", 1);
    }

    /* renamed from: iw */
    public Object mo8427iw(String str) {
        GridBagConstraints gridBagConstraints = new GridBagConstraints();
        if (str != null) {
            try {
                CSSStyleDeclaration d = new CSSOMParser().parseStyleDeclaration(new InputSource((Reader) new StringReader("{" + str.trim() + "}")));
                gridBagConstraints.gridx = (int) mo15145a(d, "gridx", (float) gridBagConstraints.gridx);
                gridBagConstraints.gridy = (int) mo15145a(d, "gridy", (float) gridBagConstraints.gridx);
                gridBagConstraints.gridwidth = (int) mo15145a(d, "gridwidth", (float) gridBagConstraints.gridwidth);
                gridBagConstraints.gridheight = (int) mo15145a(d, "gridheight", (float) gridBagConstraints.gridheight);
                gridBagConstraints.weightx = (double) mo15145a(d, "weightx", (float) gridBagConstraints.weightx);
                gridBagConstraints.weighty = (double) mo15145a(d, "weighty", (float) gridBagConstraints.weighty);
                gridBagConstraints.anchor = mo15147a(d, limitation, "anchor", gridBagConstraints.anchor);
                gridBagConstraints.fill = mo15147a(d, limitation, "fill", gridBagConstraints.fill);
                gridBagConstraints.ipadx = (int) mo15145a(d, "ipadx", (float) gridBagConstraints.ipadx);
                gridBagConstraints.ipady = (int) mo15145a(d, "ipady", (float) gridBagConstraints.ipady);
                gridBagConstraints.insets = mo15149a(d, "insets", gridBagConstraints.insets);
            } catch (Exception e) {
                log.error(String.valueOf(e));
            }
        }
        return gridBagConstraints;
    }

    /* renamed from: a */
    public LayoutManager createLayout(Container container, String str) {
        GridBagLayout gridBagLayout = new GridBagLayout();
        if (str != null) {
            String trim = str.trim();
            if (!trim.isEmpty()) {
                try {
                    CSSStyleDeclaration d = new CSSOMParser().parseStyleDeclaration(new InputSource((Reader) new StringReader(trim)));
                    gridBagLayout.columnWidths = mo15151a(d, "columnWidths", gridBagLayout.columnWidths);
                    gridBagLayout.rowHeights = mo15151a(d, "rowHeights", gridBagLayout.rowHeights);
                    gridBagLayout.columnWeights = mo15150a(d, "columnWeights", gridBagLayout.columnWeights);
                    gridBagLayout.rowWeights = mo15150a(d, "rowWeights", gridBagLayout.rowWeights);
                } catch (Exception e) {
                    log.warn(String.valueOf(e));
                }
            }
        }
        return gridBagLayout;
    }
}
