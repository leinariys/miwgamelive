package logic.ui.layout;

import com.steadystate.css.parser.CSSOMParser;
import lombok.extern.slf4j.Slf4j;
import org.w3c.css.sac.InputSource;
import org.w3c.dom.css.CSSStyleDeclaration;

import java.awt.*;
import java.io.Reader;
import java.io.StringReader;

/* renamed from: a.asc  reason: case insensitive filesystem */
/* compiled from: a */
@Slf4j
public class CardLayoutCustom extends BaseLayoutCssValue {

    /* renamed from: a */
    public LayoutManager createLayout(Container container, String str) {
        CardLayout cardLayout = new CardLayout();
        if (str != null) {
            String trim = str.trim();
            if (!trim.isEmpty()) {
                try {
                    CSSStyleDeclaration d = new CSSOMParser().parseStyleDeclaration(new InputSource((Reader) new StringReader(trim)));
                    cardLayout.setHgap(mo15148a(d, "hgap", cardLayout.getHgap()));
                    cardLayout.setVgap(mo15148a(d, "vgap", cardLayout.getVgap()));
                } catch (Exception e) {
                    log.warn(String.valueOf(e));
                }
            }
        }
        return cardLayout;
    }
}
