package logic.ui.layout;

import javax.swing.*;
import java.awt.*;

/* renamed from: a.RE */
/* compiled from: a */
public class OverlayLayoutCustom extends BaseLayoutCssValue {
    /* renamed from: a */
    public LayoutManager createLayout(Container container, String str) {
        return new OverlayLayout(container);
    }
}
