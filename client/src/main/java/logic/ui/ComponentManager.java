package logic.ui;

import com.steadystate.css.parser.CSSOMParser;
import gnu.trove.TIntObjectHashMap;
import gnu.trove.TObjectIntHashMap;
import logic.res.css.C0325EQ;
import logic.res.css.C6868avI;
import logic.res.css.CSSStyleDeclarationImpl;
import logic.swing.C1366Tu;
import org.w3c.css.sac.InputSource;
import org.w3c.dom.css.CSSRule;
import org.w3c.dom.css.CSSStyleDeclaration;

import javax.swing.*;
import javax.swing.text.JTextComponent;
import java.awt.*;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.image.ImageObserver;
import java.io.Reader;
import java.io.StringReader;
import java.util.*;

/* renamed from: a.pv */
/* compiled from: a */
public class ComponentManager implements IComponentManager {
    public static final int STATE_EXPANDED = 256;
    public static final int STATE_FOCUSED = 128;
    public static final int STATE_PRESSED = 16;
    public static final int STATE_SELECTED = 2;
    public static final int STATE_ARMED = 1;
    public static final int STATE_ENABLED = 4;
    public static final int STATE_DISABLED = 8;
    public static final int STATE_ROLLOVER = 32;
    public static final int STATE_READ_ONLY = 64;
    private static final int aSs = 150;
    private static final TObjectIntHashMap<String> stateEvent = new TObjectIntHashMap<>();
    private static final C6179ahv aSw = new aLS();
    static C3783v<Component, IComponentManager> comGrapRes = new C3783v<>(16, 0.75f, 16, C3783v.C3792i.WEAK, C3783v.C3792i.STRONG, EnumSet.of(C3783v.C3791h.IDENTITY_COMPARISONS));
    private static C3282b mousEvent = new C3282b((C3282b) null);
    private static C3281a keyboardEvent = new C3281a((C3281a) null);

    static {
        stateEvent.put(IComponentManager.armed, STATE_ARMED);
        stateEvent.put(IComponentManager.selected, STATE_SELECTED);
        stateEvent.put(IComponentManager.expanded, STATE_EXPANDED);
        stateEvent.put(IComponentManager.enabled, STATE_ENABLED);
        stateEvent.put(IComponentManager.disabled, STATE_DISABLED);
        stateEvent.put(IComponentManager.pressed, STATE_PRESSED);
        stateEvent.put(IComponentManager.hover, STATE_ROLLOVER);
        stateEvent.put(IComponentManager.over, STATE_ROLLOVER);
        stateEvent.put(IComponentManager.rollover, STATE_ROLLOVER);
        stateEvent.put(IComponentManager.readOnly, STATE_READ_ONLY);
        stateEvent.put(IComponentManager.focused, STATE_FOCUSED);
    }

    /**
     * Ссылка на графический компонент
     */
    private final Component currentComponent;
    public boolean aSJ;
    public ComponentCheck aSK;
    public boolean focused;
    public boolean mouseOver;
    float aSB = 1.0f;
    Color color;
    /**
     * CSS стиль
     */
    private C6868avI cssNode = null;
    private CSSStyleDeclaration aSC;
    private ComponentManager aSD;
    /**
     * Потомок графического Component
     */
    private Component parentComponent;
    private C6179ahv aSF;
    private int aSG;
    private int aSH;
    private PropertiesUiFromCss aSI;
    private int aSL;
    private int aSM;
    private boolean aSN = true;
    /**
     * Добавлен ли Обработчик события курсор над объектом
     */
    private boolean isMouseListener;
    private Map<String, String> aSx;
    private String aSy;
    private TIntObjectHashMap<PropertiesUiFromCss> aSz;
    /**
     * Имя xml тега, список в class BaseUItegXML
     *
     * @return Пример progress
     */
    private String name;

    /**
     * Задаём графический Component, устанавливаем имя тега
     *
     * @param str
     * @param component2
     */
    public ComponentManager(String str, Component component2) {
        this.currentComponent = component2;
        if (component2 instanceof C1276Sr) {
            String elementName = ((C1276Sr) component2).getElementName();
            this.name = elementName != null ? elementName : str;
        } else {
            this.name = str;
        }
        if (component2 instanceof JComponent) {
            ((JComponent) component2).putClientProperty(IComponentManager.cssHolder, this);
        }
        comGrapRes.put(component2, this);
        if (component2 instanceof JTextComponent) {
            this.isMouseListener = true;
            component2.addMouseListener(mousEvent);
        }
        component2.addFocusListener(keyboardEvent);
    }

    /**
     * Поиск Система событий для этого графический Component из аддона
     *
     * @param component Графический Component компонент аддона
     * @return Система событий для этого графического Component
     */
    /* renamed from: e */
    public static IComponentManager getCssHolder(Component component) {
        if (component instanceof JComponent) {
            return (IComponentManager) ((JComponent) component).getClientProperty(IComponentManager.cssHolder);
        }
        return comGrapRes.get(component);
    }

    /* renamed from: Vn */
    public static Collection<IComponentManager> m37352Vn() {
        ArrayList arrayList = new ArrayList();
        arrayList.addAll(comGrapRes.values());
        return arrayList;
    }

    /* renamed from: Vh */
    public CSSStyleDeclaration mo5149Vh() {
        if (this.aSy != null && this.aSC == null) {
            try {
                this.aSC = m37353aY(this.aSy);
            } catch (Exception e) {
                e.printStackTrace();
                this.aSC = new CSSStyleDeclarationImpl((CSSRule) null);
            }
        }
        return this.aSC;
    }

    /* renamed from: aY */
    private CSSStyleDeclaration m37353aY(String str) {
        CSSStyleDeclaration d = new CSSOMParser().parseStyleDeclaration(new InputSource((Reader) new StringReader("{" + str + "}")));
        if (d != null) {
            C0325EQ.m2829a(d);
        }
        return d;
    }

    public String getAttribute(String str) {
        if (this.aSx == null) {
            return null;
        }
        return this.aSx.get(str);
    }

    /**
     * Задать атрибут
     *
     * @param str  ключ
     * @param str2 значение
     */
    public void setAttribute(String str, String str2) {
        if (this.aSx == null) {
            this.aSx = new HashMap();
        }
        this.aSx.put(str, str2);
        mo13045Vk();
    }

    /* renamed from: aZ */
    public void mo21243aZ(String str) {
        setAttribute("class", str);
        mo13045Vk();
    }

    public void setAttributes(Map<String, String> map) {
        for (Map.Entry next : map.entrySet()) {
            setAttribute((String) next.getKey(), (String) next.getValue());
        }
        mo13045Vk();
    }

    public boolean hasAttribute(String str) {
        if (this.aSx == null) {
            return false;
        }
        return this.aSx.containsKey(str);
    }

    /* access modifiers changed from: protected */
    /* renamed from: Vi */
    public int mo5877Vi() {
        int i;
        int i2 = this.aSH;
        if (this.currentComponent.isEnabled()) {
            i = i2 | 4;
        } else {
            i = i2 | 8;
        }
        if ((this.aSG & 128) == 0 && this.focused) {
            i |= 128;
        }
        if ((this.aSG & 32) != 0 || !this.mouseOver) {
            return i;
        }
        return i | 32;
    }

    /**
     * Получить СSS стиль
     *
     * @return
     */
    /* renamed from: Vj */
    public C6868avI mo13044Vj() {
        return this.cssNode;
    }

    /**
     * Установить CSS стиль
     *
     * @param cssNode
     */
    /* renamed from: a */
    public void mo13054a(C6868avI cssNode) {
        this.cssNode = cssNode;
    }

    /* renamed from: a */
    public Dimension mo13052a(JComponent jComponent, Dimension dimension) {
        PropertiesUiFromCss Vp = mo13047Vp();
        if (Vp == null) {
            return dimension;
        }
        C1366Tu atG = Vp.atG();
        C1366Tu atF = Vp.atF();
        if (atG == C1366Tu.fEC || atF == C1366Tu.fEC) {
            return dimension;
        }
        return new Dimension((int) atG.value, (int) atF.value);
    }

    /* renamed from: b */
    public Dimension mo13057b(JComponent jComponent, Dimension dimension) {
        int i;
        int i2;
        int jp;
        int jp2;
        int i3 = 0;
        PropertiesUiFromCss Vp = mo13047Vp();
        if (Vp == null) {
            return dimension;
        }
        C1366Tu atI = Vp.atI();
        C1366Tu atH = Vp.atH();
        if (atI == C1366Tu.fEC && atH == C1366Tu.fEC && dimension == null) {
            return null;
        }
        Container parent = jComponent.getParent();
        int width = parent != null ? parent.getWidth() : 0;
        if (parent != null) {
            i = parent.getHeight();
        } else {
            i = 0;
        }
        if (dimension == null) {
            dimension = jComponent.minimumSize();
        }
        if (atI != C1366Tu.fEC) {
            i2 = atI.mo5756jp((float) width);
        } else if (dimension != null) {
            i2 = dimension.width;
        } else {
            i2 = 0;
        }
        if (atH != C1366Tu.fEC) {
            i3 = atH.mo5756jp((float) i);
        } else if (dimension != null) {
            i3 = dimension.height;
        }
        C1366Tu atG = Vp.atG();
        C1366Tu atF = Vp.atF();
        if (atG != C1366Tu.fEC && i2 > (jp2 = atG.mo5756jp((float) width))) {
            i2 = jp2;
        }
        if (atF != C1366Tu.fEC && i3 > (jp = atF.mo5756jp((float) i))) {
            i3 = jp;
        }
        return new Dimension(i2, i3);
    }

    /* renamed from: c */
    public Dimension mo13059c(JComponent jComponent, Dimension dimension) {
        int i;
        int i2;
        int i3;
        int jp;
        int jp2;
        int jp3;
        int jp4;
        PropertiesUiFromCss Vp = mo13047Vp();
        if (Vp == null) {
            return dimension;
        }
        C1366Tu atS = Vp.atS();
        C1366Tu atx = Vp.atx();
        if (atS == C1366Tu.fEC && atx == C1366Tu.fEC && dimension == null) {
            return null;
        }
        Container parent = jComponent.getParent();
        int width = parent != null ? parent.getWidth() : 0;
        if (parent != null) {
            i = parent.getHeight();
        } else {
            i = 0;
        }
        if (atS != C1366Tu.fEC) {
            i2 = atS.mo5756jp((float) width);
            if (dimension != null) {
                i2 = Math.max(dimension.width, i2);
            }
        } else if (dimension != null) {
            i2 = dimension.width;
        } else {
            i2 = 0;
        }
        if (atx != C1366Tu.fEC) {
            i3 = atx.mo5756jp((float) i);
            if (dimension != null) {
                i3 = Math.max(dimension.height, i3);
            }
        } else {
            i3 = dimension != null ? dimension.height : 0;
        }
        Image fT = Vp.mo428fT(0);
        if (fT != null) {
            int width2 = fT.getWidth((ImageObserver) null);
            int height = fT.getHeight((ImageObserver) null);
            if (width2 != -1) {
                i2 = Math.max(width2, i2);
            }
            if (height != -1) {
                i3 = Math.max(height, i3);
            }
        }
        C1366Tu atI = Vp.atI();
        C1366Tu atH = Vp.atH();
        if (atI != C1366Tu.fEC && i2 < (jp4 = atI.mo5756jp((float) width))) {
            i2 = jp4;
        }
        if (atH != C1366Tu.fEC && i3 < (jp3 = atH.mo5756jp((float) i))) {
            i3 = jp3;
        }
        C1366Tu atG = Vp.atG();
        C1366Tu atF = Vp.atF();
        if (atG != C1366Tu.fEC && i2 > (jp2 = atG.mo5756jp((float) width))) {
            i2 = jp2;
        }
        if (atF != C1366Tu.fEC && i3 > (jp = atF.mo5756jp((float) i))) {
            i3 = jp;
        }
        return new Dimension(i2, i3);
    }

    /* renamed from: ba */
    public boolean mo13058ba(String str) {
        int i = stateEvent.get(str);
        if (i == 0 || (i & mo5877Vi()) == 0) {
            return false;
        }
        return true;
    }

    public void setStyle(String str) {
        if (this.aSy == str) {
            return;
        }
        if (this.aSy == null || !this.aSy.equals(str)) {
            this.aSy = str;
            this.aSC = null;
            mo13045Vk();
        }
    }

    /* renamed from: Vk */
    public void mo13045Vk() {
        if (this.aSz != null) {
            this.aSz.clear();
        }
        this.aSL++;
    }

    /* renamed from: Vl */
    public C6179ahv mo5150Vl() {
        if (this.aSF == null) {
            if (!(this.currentComponent instanceof Container)) {
                this.aSF = aSw;
            } else {
                this.aSF = new C3283c((Container) this.currentComponent);
            }
        }
        return this.aSF;
    }

    public String getTextContent() {
        return "";
    }

    public String getNodeName() {
        return this.name;
    }

    public int getNodeType() {
        return 0;
    }

    /* renamed from: Vm */
    public C6738asi mo15974Vm() {
        if (this.parentComponent != this.currentComponent.getParent() || this.aSD == null) {
            this.parentComponent = this.currentComponent.getParent();
            if (this.parentComponent != null) {
                this.aSD = (ComponentManager) getCssHolder(this.parentComponent);
                if (this.aSD == null) {
                    for (Component component2 = this.parentComponent; component2 != null; component2 = component2.getParent()) {
                        this.aSD = (ComponentManager) getCssHolder(component2);
                        if (this.aSD != null) {
                            break;
                        }
                    }
                }
            }
        }
        return this.aSD;
    }

    public float getAlpha() {
        return this.aSB;
    }

    public void setAlpha(float f) {
        this.aSB = f;
    }

    public Color getColor() {
        return this.color;
    }

    public void setColor(Color color2) {
        this.color = color2;
    }

    /* renamed from: Vo */
    public IComponentManager mo13046Vo() {
        if (this.parentComponent != this.currentComponent.getParent()) {
            this.parentComponent = this.currentComponent.getParent();
            this.aSD = (ComponentManager) getCssHolder(this.parentComponent);
        }
        return this.aSD;
    }

    public Component getCurrentComponent() {
        return this.currentComponent;
    }

    /* renamed from: Vp */
    public PropertiesUiFromCss mo13047Vp() {
        PropertiesUiFromCss aj;
        int i;
        int i2 = -1;
        if (this.parentComponent != this.currentComponent.getParent()) {
            this.parentComponent = this.currentComponent.getParent();
            this.aSD = (ComponentManager) getCssHolder(this.parentComponent);
            if (this.aSD != null) {
                i = this.aSD.aSL;
            } else {
                i = -1;
            }
            this.aSM = i;
            mo13045Vk();
        } else if (!(this.aSD == null || this.aSM == this.aSD.aSL)) {
            if (this.aSD != null) {
                i2 = this.aSD.aSL;
            }
            this.aSM = i2;
            mo13045Vk();
        }
        int Vi = mo5877Vi();
        if (this.aSz == null) {
            this.aSz = new TIntObjectHashMap<>();
            aj = null;
        } else {
            aj = (PropertiesUiFromCss) this.aSz.get(Vi);
        }
        if (aj != null) {
            return aj;
        }
        try {
            PropertiesUiFromCss aj2 = new PropertiesUiFromCss(this);
            this.aSz.put(Vi, aj2);
            return aj2;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    /* renamed from: Vq */
    public void mo13048Vq() {
        this.aSG = 0;
        this.aSH = 0;
    }

    /* renamed from: q */
    public void mo13063q(int i, int i2) {
        this.aSG = i;
        this.aSH = i2;
    }

    /* renamed from: Vr */
    public PropertiesUiFromCss mo13049Vr() {
        if (this.aSI == null) {
            this.aSI = new PropertiesUiFromCss(this, mo5149Vh());
        }
        return this.aSI;
    }

    /* renamed from: Vs */
    public boolean mo13050Vs() {
        return this.aSN && (this.aSK == null || this.aSK.isContains(this.currentComponent));
    }

    /* renamed from: aO */
    public void mo13055aO(boolean z) {
        this.aSN = z;
    }

    /* renamed from: Vt */
    public boolean mo13051Vt() {
        return this.isMouseListener;
    }

    /* renamed from: aP */
    public void mo13056aP(boolean z) {
        if (z != this.isMouseListener) {
            if (z) {
                this.currentComponent.addMouseListener(mousEvent);
            } else {
                this.currentComponent.removeMouseListener(mousEvent);
            }
            this.isMouseListener = z;
        }
    }

    /* renamed from: Vu */
    public ComponentCheck mo21242Vu() {
        return this.aSK;
    }

    /* renamed from: a */
    public void mo13053a(ComponentCheck aws) {
        this.aSK = aws;
    }

    /* renamed from: a.pv$a */
    private static final class C3281a implements FocusListener {
        private C3281a() {
        }

        /* synthetic */ C3281a(C3281a aVar) {
            this();
        }

        public void focusGained(FocusEvent focusEvent) {
            Component component = focusEvent.getComponent();
            ((ComponentManager) ComponentManager.getCssHolder(component)).focused = true;
            component.repaint(150);
            if ((component instanceof JTextComponent) && (component.getParent() instanceof JViewport) && (component.getParent().getParent() instanceof JScrollPane)) {
                ((ComponentManager) ComponentManager.getCssHolder(component.getParent())).focused = true;
                ((ComponentManager) ComponentManager.getCssHolder(component.getParent().getParent())).focused = true;
                component.getParent().getParent().repaint(150);
            }
        }

        public void focusLost(FocusEvent focusEvent) {
            Component component = focusEvent.getComponent();
            ((ComponentManager) ComponentManager.getCssHolder(component)).focused = false;
            component.repaint(150);
            if ((component instanceof JTextComponent) && (component.getParent() instanceof JViewport) && (component.getParent().getParent() instanceof JScrollPane)) {
                ((ComponentManager) ComponentManager.getCssHolder(component.getParent())).focused = false;
                ((ComponentManager) ComponentManager.getCssHolder(component.getParent().getParent())).focused = false;
                component.getParent().getParent().repaint(150);
            }
        }
    }

    /* renamed from: a.pv$b */
    /* compiled from: a */
    private static final class C3282b extends MouseAdapter {
        private C3282b() {
        }

        /* synthetic */ C3282b(C3282b bVar) {
            this();
        }

        public void mouseEntered(MouseEvent mouseEvent) {
            Component component = mouseEvent.getComponent();
            ((ComponentManager) ComponentManager.getCssHolder(component)).mouseOver = true;
            component.repaint(150);
            if ((component instanceof JTextComponent) && (component.getParent() instanceof JViewport) && (component.getParent().getParent() instanceof JScrollPane)) {
                ((ComponentManager) ComponentManager.getCssHolder(component.getParent())).mouseOver = true;
                ((ComponentManager) ComponentManager.getCssHolder(component.getParent().getParent())).mouseOver = true;
                component.getParent().getParent().repaint(150);
            }
        }

        public void mouseExited(MouseEvent mouseEvent) {
            Component component = mouseEvent.getComponent();
            ((ComponentManager) ComponentManager.getCssHolder(component)).mouseOver = false;
            component.repaint(150);
            if ((component instanceof JTextComponent) && (component.getParent() instanceof JViewport) && (component.getParent().getParent() instanceof JScrollPane)) {
                ((ComponentManager) ComponentManager.getCssHolder(component.getParent())).mouseOver = false;
                ((ComponentManager) ComponentManager.getCssHolder(component.getParent().getParent())).mouseOver = false;
                component.getParent().getParent().repaint(150);
            }
        }

        public void mousePressed(MouseEvent mouseEvent) {
            ((ComponentManager) ComponentManager.getCssHolder(mouseEvent.getComponent())).aSJ = true;
            mouseEvent.getComponent().repaint(150);
        }

        public void mouseReleased(MouseEvent mouseEvent) {
            ((ComponentManager) ComponentManager.getCssHolder(mouseEvent.getComponent())).aSJ = false;
            mouseEvent.getComponent().repaint(50);
        }
    }

    /* renamed from: a.pv$c */
    /* compiled from: a */
    class C3283c implements C6179ahv {
        private final /* synthetic */ Container inR;

        C3283c(Container container) {
            this.inR = container;
        }

        public int getLength() {
            return this.inR.getComponentCount();
        }

        /* renamed from: qR */
        public C6738asi mo9893qR(int i) {
            return ComponentManager.getCssHolder(this.inR.getComponent(i));
        }
    }
}
