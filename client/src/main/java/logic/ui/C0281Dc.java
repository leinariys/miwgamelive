package logic.ui;

import javax.swing.*;
import java.awt.*;

/* renamed from: a.Dc */
/* compiled from: a */
public class C0281Dc implements Icon {
    private Icon icon;

    public C0281Dc(Icon icon2) {
        this.icon = icon2;
    }

    public int getIconHeight() {
        return this.icon.getIconHeight();
    }

    public int getIconWidth() {
        return this.icon.getIconWidth();
    }

    public void paintIcon(Component component, Graphics graphics, int i, int i2) {
        this.icon.paintIcon(component, graphics, i, i2);
    }
}
