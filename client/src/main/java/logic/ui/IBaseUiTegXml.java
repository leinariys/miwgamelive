package logic.ui;

import logic.res.ILoadFonts;
import logic.res.ILoaderImageInterface;
import logic.res.css.C6868avI;
import logic.res.sound.ISoundPlayer;
import logic.ui.item.InternalFrame;
import org.w3c.dom.css.CSSStyleDeclaration;
import taikodom.render.RenderView;
import taikodom.render.helpers.RenderTask;
import taikodom.render.scene.SoundObject;

import javax.swing.*;
import java.awt.*;
import java.io.File;
import java.net.URL;

/* renamed from: a.xe */
/* compiled from: a */
public abstract class IBaseUiTegXml {
    /* renamed from: bFE */
    static IBaseUiTegXml thisClass;

    /* renamed from: a */
    public static void m41083a(IBaseUiTegXml xeVar) {
        thisClass = xeVar;
    }

    public static IBaseUiTegXml initBaseUItegXML() {
        if (thisClass == null) {
            thisClass = new BaseUiTegXml();
        }
        return thisClass;
    }

    /* renamed from: a */
    public abstract C6868avI mo13698a(Object obj, File file);

    /* renamed from: a */
    public abstract C6868avI loadCss(Object obj, String str);

    /* renamed from: a */
    public abstract Component createJComponent(C6342alC alc, URL url);

    /* renamed from: a */
    public abstract void mo13701a(ISoundPlayer ma);

    /* renamed from: a */
    public abstract void mo13703a(ILoadFonts age);

    /* renamed from: a */
    public abstract void setLoaderImage(ILoaderImageInterface azb);

    /* renamed from: a */
    public abstract void mo13705a(InternalFrame nxVar, boolean z);

    /* renamed from: a */
    public abstract void mo13706a(Object obj, Component component, File file);

    /* renamed from: a */
    public abstract void mo13707a(Object obj, Component component, String str);

    /* renamed from: a */
    public abstract void mo13708a(RenderView renderView);

    /* renamed from: a */
    public abstract void mo13709a(RenderTask renderTask);

    /* renamed from: aY */
    public abstract CSSStyleDeclaration mo13710aY(String str);

    public abstract ILoaderImageInterface adz();

    public abstract C6868avI getCssNodeCore();

    /* access modifiers changed from: protected */
    public abstract JLayeredPane setupJDesktopPaneInRootPane();

    public abstract boolean anx();

    public abstract ISoundPlayer any();

    /* renamed from: bE */
    public abstract void mo13717bE(boolean z);

    /* renamed from: c */
    public abstract Component createJComponent(URL url);

    /* renamed from: cx */
    public abstract SoundObject mo13724cx(String str);

    /* renamed from: d */
    public abstract Component createJComponent(Object obj, String str);

    /* renamed from: e */
    public abstract Component mo13726e(File file);

    /* renamed from: e */
    public abstract Component createJComponentAndAddInRootPane(Object obj, String str);

    /* renamed from: e */
    public abstract SoundObject mo13728e(String str, boolean z);

    public abstract Font getFont(String str, int i, int i2);

    public abstract JRootPane getRootPane();

    public abstract void setRootPane(JRootPane jRootPane);
}
