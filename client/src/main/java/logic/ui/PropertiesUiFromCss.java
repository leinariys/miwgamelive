package logic.ui;

import logic.res.css.C6868avI;
import logic.swing.*;
import org.w3c.dom.css.CSSPrimitiveValue;
import org.w3c.dom.css.CSSStyleDeclaration;
import org.w3c.dom.css.CSSValue;
import org.w3c.dom.css.CSSValueList;
import taikodom.render.TDesktop;

import javax.swing.*;
import javax.swing.plaf.ComponentUI;
import java.awt.*;
import java.util.HashMap;
import java.util.Map;

/**
 * Свойства UI компонента заполненое из CSS
 */
/* renamed from: a.Aj */
/* compiled from: a */
public class PropertiesUiFromCss {
    private static final Color cds = new Color(0, 0, 0, 0);
    static Map<String, C1366Tu> cdt = new HashMap();
    static Map<String, C3991xo> cdu = new HashMap();
    static Map<String, aRU> cdv = new HashMap();
    static Map<String, C3810vI> cdw = new HashMap();
    static Map<String, ComponentOrientation> cdx = new HashMap();

    static {
        cdw.put("super", C3810vI.TOP);
        cdw.put("top", C3810vI.TOP);
        cdw.put("bottom", C3810vI.BOTTOM);
        cdw.put("middle", C3810vI.CENTER);
        cdw.put("center", C3810vI.CENTER);
        cdw.put("fill", C3810vI.FILL);
        cdx.put("ltr", ComponentOrientation.LEFT_TO_RIGHT);
        cdx.put("rtl", ComponentOrientation.RIGHT_TO_LEFT);
        cdt.put("left", new C1366Tu(0.0f, C1366Tu.C1367a.PERCENT));
        cdt.put("top", new C1366Tu(0.0f, C1366Tu.C1367a.PERCENT));
        cdt.put("center", new C1366Tu(50.0f, C1366Tu.C1367a.PERCENT));
        cdt.put("bottom", new C1366Tu(100.0f, C1366Tu.C1367a.PERCENT));
        cdt.put("right", new C1366Tu(100.0f, C1366Tu.C1367a.PERCENT));
        cdu.put("stretch", C3991xo.STRETCH);
        cdu.put("repeat", C3991xo.REPEAT);
        cdu.put("repeat-x", C3991xo.REPEAT_X);
        cdu.put("repeat-x-stretch-y", C3991xo.REPEAT_X_STRETCH_Y);
        cdu.put("repeat-y", C3991xo.REPEAT_Y);
        cdu.put("repeat-y-stretch-x", C3991xo.REPEAT_Y_STRETCH_X);
        cdu.put("no-repeat", C3991xo.NO_REPEAT);
        cdv.put("middle", aRU.CENTER);
        cdv.put("center", aRU.CENTER);
        cdv.put("left", aRU.LEFT);
        cdv.put("right", aRU.RIGHT);
        cdv.put("fill", aRU.FILL);
    }

    public final IComponentManager cdV;
    /* renamed from: Td */
    public Image iconImage;
    public Image[] backgroundImage;
    public C2741jM[] backgroundPosition;
    public C3991xo[] backgroundRepeat;
    public Boolean blurBehind;
    public Image glowImage;
    public Image borderBottomImage;
    public C3991xo borderBottomImageRepeat;
    public Image borderBottomLeftImage;
    public Image borderBottomRightImage;
    public Color borderColorMultiplier;
    public Image borderLeftImage;
    public C3991xo borderLeftImageRepeat;
    public Image borderRightImage;
    public C3991xo borderRightImageRepeat;
    public C2741jM borderSpacing;
    public String borderStyle;
    public Image borderTopImage;
    public C3991xo borderTopImageRepeat;
    public Image borderTopLeftImage;
    public Image borderTopRightImage;
    public Cursor cursor;
    public C1366Tu height;
    public aRU textAlign;
    public Icon cdY;
    public C1366Tu left;
    public Color[] backgroundColor;
    public Image backgroundFill;
    public C3810vI verticalAlign;
    public C1366Tu width;
    public Image borderLeftFill;
    public C1366Tu marginBottom;
    public C1366Tu marginLeft;
    public C1366Tu marginRight;
    public C1366Tu marginTop;
    public C1366Tu maxHeight;
    public C1366Tu maxWidth;
    public C1366Tu minHeight;
    public Dimension minimumSize;
    public C1366Tu minWidth;
    public C1366Tu paddingBottom;
    public C1366Tu paddingLeft;
    public C1366Tu paddingRight;
    public C1366Tu paddingTop;
    public Image borderRightFill;
    public C1366Tu fontShadowX;
    public C1366Tu fontShadowY;
    public C1366Tu top;
    public Color color;
    public Color colorMultiplier;
    public Font font;
    public int hash;
    public Color shadowColor;
    private String dropSucessSound;
    private String dropFailSound;
    private String dragStartSound;
    private String openSound;
    private String closeSound;
    private String collapseSound;
    private String expandSound;
    private String scrollSound;
    private String typeAnimationSound;
    private ComponentOrientation direction;
    private String mouseOverSound;
    private String mousePressSound;
    private String mouseReleaseSound;
    private String mouseClickSound;
    private String keyPressSound;
    private String keyReleaseSound;
    private String keyTypedSound;
    private String actionSound;

    public PropertiesUiFromCss(IComponentManager aek) {
        this.cdV = aek;
        setCssToUI(new C5898aca(aek), true);
    }

    public PropertiesUiFromCss(IComponentManager aek, CSSStyleDeclaration cSSStyleDeclaration) {
        this.cdV = aek;
        if (cSSStyleDeclaration != null) {
            setCssToUI(new C5898aca(aek, cSSStyleDeclaration), false);
        }
    }

    /* renamed from: f */
    public static PropertiesUiFromCss m461f(Component component) {
        IComponentManager e = ComponentManager.getCssHolder(component);
        if (e != null) {
            return e.mo13047Vp();
        }
        return null;
    }

    /* renamed from: g */
    public static PropertiesUiFromCss m462g(Component component) {
        IComponentManager e = ComponentManager.getCssHolder(component);
        if (e != null) {
            return e.mo13047Vp();
        }
        return null;
    }

    /* renamed from: a */
    public static PropertiesUiFromCss m456a(ComponentUI componentUI) {
        if (componentUI instanceof IComponentUi) {
            return ((IComponentUi) componentUI).getComponentManager().mo13047Vp();
        }
        return null;
    }

    /* renamed from: h */
    public static TDesktop m463h(Component component) {
        Component component2 = component;
        while (!(component2 instanceof TDesktop)) {
            if (component2 == null) {
                return null;
            }
            component2 = component2.getParent();
        }
        return (TDesktop) component2;
    }

    /* renamed from: fS */
    public Color mo427fS(int i) {
        PropertiesUiFromCss Vr = this.cdV.mo13049Vr();
        if (Vr != null && Vr != this && Vr.backgroundColor != null && Vr.backgroundColor.length > i) {
            return Vr.backgroundColor[i];
        }
        if (this.backgroundColor == null || this.backgroundColor.length <= i) {
            return cds;
        }
        return this.backgroundColor[i];
    }

    public Image atg() {
        PropertiesUiFromCss Vr = this.cdV.mo13049Vr();
        if (Vr == null || Vr == this || Vr.backgroundFill == null) {
            return this.backgroundFill;
        }
        return Vr.backgroundFill;
    }

    /* renamed from: fT */
    public Image mo428fT(int i) {
        PropertiesUiFromCss Vr = this.cdV.mo13049Vr();
        if (Vr != null && Vr != this && Vr.backgroundImage != null && Vr.backgroundImage.length > i) {
            return Vr.backgroundImage[i];
        }
        if (this.backgroundImage == null || this.backgroundImage.length <= i) {
            return null;
        }
        return this.backgroundImage[i];
    }

    /* renamed from: fU */
    public C2741jM mo429fU(int i) {
        PropertiesUiFromCss Vr = this.cdV.mo13049Vr();
        if (Vr != null && Vr != this && Vr.backgroundPosition != null && Vr.backgroundPosition.length > i) {
            return Vr.backgroundPosition[i];
        }
        if (this.backgroundPosition == null || this.backgroundPosition.length <= i) {
            return C2741jM.apf;
        }
        return this.backgroundPosition[i];
    }

    /* renamed from: fV */
    public C3991xo mo430fV(int i) {
        PropertiesUiFromCss Vr = this.cdV.mo13049Vr();
        if (Vr != null && Vr != this && Vr.backgroundRepeat != null && Vr.backgroundRepeat.length > i) {
            return Vr.backgroundRepeat[i];
        }
        if (this.backgroundRepeat == null || this.backgroundRepeat.length <= i) {
            return C3991xo.STRETCH;
        }
        return this.backgroundRepeat[i];
    }

    public Boolean ath() {
        return this.blurBehind;
    }

    public Image ati() {
        PropertiesUiFromCss Vr = this.cdV.mo13049Vr();
        if (Vr == null || Vr == this || Vr.borderBottomImage == null) {
            return this.borderBottomImage;
        }
        return Vr.borderBottomImage;
    }

    public Image atj() {
        PropertiesUiFromCss Vr = this.cdV.mo13049Vr();
        if (Vr == null || Vr == this || Vr.glowImage == null) {
            return this.glowImage;
        }
        return Vr.glowImage;
    }

    public C3991xo atk() {
        PropertiesUiFromCss Vr = this.cdV.mo13049Vr();
        if (Vr == null || Vr == this || Vr.borderBottomImageRepeat == null) {
            return this.borderBottomImageRepeat;
        }
        return Vr.borderBottomImageRepeat;
    }

    public Image atl() {
        PropertiesUiFromCss Vr = this.cdV.mo13049Vr();
        if (Vr == null || Vr == this || Vr.borderBottomLeftImage == null) {
            return this.borderBottomLeftImage;
        }
        return Vr.borderBottomLeftImage;
    }

    public Image atm() {
        PropertiesUiFromCss Vr = this.cdV.mo13049Vr();
        if (Vr == null || Vr == this || Vr.borderBottomRightImage == null) {
            return this.borderBottomRightImage;
        }
        return Vr.borderBottomRightImage;
    }

    public Color atn() {
        PropertiesUiFromCss Vr = this.cdV.mo13049Vr();
        if (Vr == null || Vr == this || Vr.borderColorMultiplier == null) {
            return this.borderColorMultiplier;
        }
        return Vr.borderColorMultiplier;
    }

    public Image ato() {
        PropertiesUiFromCss Vr = this.cdV.mo13049Vr();
        if (Vr == null || Vr == this || Vr.borderLeftImage == null) {
            return this.borderLeftImage;
        }
        return Vr.borderLeftImage;
    }

    public C3991xo atp() {
        PropertiesUiFromCss Vr = this.cdV.mo13049Vr();
        if (Vr == null || Vr == this || Vr.borderLeftImageRepeat == null) {
            return this.borderLeftImageRepeat;
        }
        return Vr.borderLeftImageRepeat;
    }

    public Image atq() {
        PropertiesUiFromCss Vr = this.cdV.mo13049Vr();
        if (Vr == null || Vr == this || Vr.borderRightImage == null) {
            return this.borderRightImage;
        }
        return Vr.borderRightImage;
    }

    public C3991xo atr() {
        PropertiesUiFromCss Vr = this.cdV.mo13049Vr();
        if (Vr == null || Vr == this || Vr.borderRightImageRepeat == null) {
            return this.borderRightImageRepeat;
        }
        return Vr.borderRightImageRepeat;
    }

    public C2741jM ats() {
        PropertiesUiFromCss Vr = this.cdV.mo13049Vr();
        if (Vr == null || Vr == this || Vr.borderSpacing == null) {
            return this.borderSpacing;
        }
        return Vr.borderSpacing;
    }

    public String getBorderStyle() {
        PropertiesUiFromCss Vr = this.cdV.mo13049Vr();
        if (Vr == null || Vr == this || Vr.borderStyle == null) {
            return this.borderStyle;
        }
        return Vr.borderStyle;
    }

    public Image att() {
        PropertiesUiFromCss Vr = this.cdV.mo13049Vr();
        if (Vr == null || Vr == this || Vr.borderTopImage == null) {
            return this.borderTopImage;
        }
        return Vr.borderTopImage;
    }

    public C3991xo atu() {
        PropertiesUiFromCss Vr = this.cdV.mo13049Vr();
        if (Vr == null || Vr == this || Vr.borderTopImageRepeat == null) {
            return this.borderTopImageRepeat;
        }
        return Vr.borderTopImageRepeat;
    }

    public Image atv() {
        PropertiesUiFromCss Vr = this.cdV.mo13049Vr();
        if (Vr == null || Vr == this || Vr.borderTopLeftImage == null) {
            return this.borderTopLeftImage;
        }
        return Vr.borderTopLeftImage;
    }

    public Image atw() {
        PropertiesUiFromCss Vr = this.cdV.mo13049Vr();
        if (Vr == null || Vr == this || Vr.borderTopRightImage == null) {
            return this.borderTopRightImage;
        }
        return Vr.borderTopRightImage;
    }

    public Color getColor() {
        PropertiesUiFromCss Vr = this.cdV.mo13049Vr();
        if (Vr != null && Vr != this && Vr.color != null) {
            return Vr.color;
        }
        if (this.color != null) {
            return this.color;
        }
        IComponentManager Vo = this.cdV.mo13046Vo();
        if (Vo != null) {
            return Vo.mo13047Vp().getColor();
        }
        return Color.white;
    }

    public Color getColorMultiplier() {
        PropertiesUiFromCss Vr = this.cdV.mo13049Vr();
        if (Vr == null || Vr == this || Vr.colorMultiplier == null) {
            return this.colorMultiplier;
        }
        return Vr.colorMultiplier;
    }

    public void setColorMultiplier(Color color2) {
        this.colorMultiplier = color2;
        this.hash = 0;
    }

    public Cursor getCursor() {
        PropertiesUiFromCss Vr = this.cdV.mo13049Vr();
        if (Vr == null || Vr == this || Vr.cursor == null) {
            return this.cursor;
        }
        return Vr.cursor;
    }

    public Font getFont() {
        PropertiesUiFromCss Vr = this.cdV.mo13049Vr();
        if (Vr == null || Vr == this || Vr.font == null) {
            return this.font;
        }
        return Vr.font;
    }

    public C1366Tu atx() {
        PropertiesUiFromCss Vr = this.cdV.mo13049Vr();
        if (Vr == null || Vr == this || Vr.height == null) {
            return this.height;
        }
        return Vr.height;
    }

    public aRU aty() {
        PropertiesUiFromCss Vr = this.cdV.mo13049Vr();
        if (Vr == null || Vr == this || Vr.textAlign == null) {
            return this.textAlign;
        }
        return Vr.textAlign;
    }

    public Icon getIcon() {
        PropertiesUiFromCss Vr = this.cdV.mo13049Vr();
        if (Vr != null && Vr != this && Vr.iconImage != null) {
            return Vr.getIcon();
        }
        if (this.iconImage != null && this.cdY == null) {
            this.cdY = new C0281Dc(new ImageIcon(this.iconImage));
        }
        return this.cdY;
    }

    public Image getIconImage() {
        PropertiesUiFromCss Vr = this.cdV.mo13049Vr();
        if (Vr == null || Vr == this || Vr.iconImage == null) {
            return this.iconImage;
        }
        return Vr.iconImage;
    }

    public C1366Tu atz() {
        PropertiesUiFromCss Vr = this.cdV.mo13049Vr();
        if (Vr == null || Vr == this || Vr.left == null) {
            return this.left;
        }
        return Vr.left;
    }

    public Image atA() {
        PropertiesUiFromCss Vr = this.cdV.mo13049Vr();
        if (Vr == null || Vr == this || Vr.borderLeftFill == null) {
            return this.borderLeftFill;
        }
        return Vr.borderLeftFill;
    }

    public C1366Tu atB() {
        PropertiesUiFromCss Vr = this.cdV.mo13049Vr();
        if (Vr == null || Vr == this || Vr.marginBottom == null) {
            return this.marginBottom;
        }
        return Vr.marginBottom;
    }

    public C1366Tu atC() {
        PropertiesUiFromCss Vr = this.cdV.mo13049Vr();
        if (Vr == null || Vr == this || Vr.marginLeft == null) {
            return this.marginLeft;
        }
        return Vr.marginLeft;
    }

    public C1366Tu atD() {
        PropertiesUiFromCss Vr = this.cdV.mo13049Vr();
        if (Vr == null || Vr == this || Vr.marginRight == null) {
            return this.marginRight;
        }
        return Vr.marginRight;
    }

    public C1366Tu atE() {
        PropertiesUiFromCss Vr = this.cdV.mo13049Vr();
        if (Vr == null || Vr == this || Vr.marginTop == null) {
            return this.marginTop;
        }
        return Vr.marginTop;
    }

    public C1366Tu atF() {
        PropertiesUiFromCss Vr = this.cdV.mo13049Vr();
        if (Vr == null || Vr == this || Vr.maxHeight == null) {
            return this.maxHeight;
        }
        return Vr.maxHeight;
    }

    public C1366Tu atG() {
        PropertiesUiFromCss Vr = this.cdV.mo13049Vr();
        if (Vr == null || Vr == this || Vr.maxWidth == null) {
            return this.maxWidth;
        }
        return Vr.maxWidth;
    }

    public C1366Tu atH() {
        PropertiesUiFromCss Vr = this.cdV.mo13049Vr();
        if (Vr == null || Vr == this || Vr.minHeight == null) {
            return this.minHeight;
        }
        return Vr.minHeight;
    }

    public Dimension getMinimumSize() {
        PropertiesUiFromCss Vr = this.cdV.mo13049Vr();
        if (Vr == null || Vr == this || Vr.minimumSize == null) {
            return this.minimumSize;
        }
        return Vr.minimumSize;
    }

    public C1366Tu atI() {
        PropertiesUiFromCss Vr = this.cdV.mo13049Vr();
        if (Vr == null || Vr == this || Vr.minWidth == null) {
            return this.minWidth;
        }
        return Vr.minWidth;
    }

    public C1366Tu atJ() {
        PropertiesUiFromCss Vr = this.cdV.mo13049Vr();
        if (Vr == null || Vr == this || Vr.paddingBottom == null) {
            return this.paddingBottom;
        }
        return Vr.paddingBottom;
    }

    public C1366Tu atK() {
        PropertiesUiFromCss Vr = this.cdV.mo13049Vr();
        if (Vr == null || Vr == this || Vr.paddingLeft == null) {
            return this.paddingLeft;
        }
        return Vr.paddingLeft;
    }

    public C1366Tu atL() {
        PropertiesUiFromCss Vr = this.cdV.mo13049Vr();
        if (Vr == null || Vr == this || Vr.paddingRight == null) {
            return this.paddingRight;
        }
        return Vr.paddingRight;
    }

    public C1366Tu atM() {
        PropertiesUiFromCss Vr = this.cdV.mo13049Vr();
        if (Vr == null || Vr == this || Vr.paddingTop == null) {
            return this.paddingTop;
        }
        return Vr.paddingTop;
    }

    public Image atN() {
        PropertiesUiFromCss Vr = this.cdV.mo13049Vr();
        if (Vr == null || Vr == this || Vr.borderRightFill == null) {
            return this.borderRightFill;
        }
        return Vr.borderRightFill;
    }

    public Color getShadowColor() {
        PropertiesUiFromCss Vr = this.cdV.mo13049Vr();
        if (Vr != null && Vr != this && Vr.shadowColor != null) {
            return Vr.shadowColor;
        }
        if (this.shadowColor != null) {
            return this.shadowColor;
        }
        IComponentManager Vo = this.cdV.mo13046Vo();
        if (Vo != null) {
            return Vo.mo13047Vp().getShadowColor();
        }
        return Color.black;
    }

    public float atO() {
        PropertiesUiFromCss Vr = this.cdV.mo13049Vr();
        if (Vr != null && Vr != this && Vr.fontShadowX != null) {
            return Vr.atO();
        }
        if (this.fontShadowX != null) {
            return this.fontShadowX.value;
        }
        IComponentManager Vo = this.cdV.mo13046Vo();
        if (Vo != null) {
            return Vo.mo13047Vp().atO();
        }
        return 0.0f;
    }

    public float atP() {
        PropertiesUiFromCss Vr = this.cdV.mo13049Vr();
        if (Vr != null && Vr != this && Vr.fontShadowY != null) {
            return Vr.atP();
        }
        if (this.fontShadowY != null) {
            return this.fontShadowY.value;
        }
        IComponentManager Vo = this.cdV.mo13046Vo();
        if (Vo != null) {
            return Vo.mo13047Vp().atP();
        }
        return 0.0f;
    }

    public C1366Tu atQ() {
        PropertiesUiFromCss Vr = this.cdV.mo13049Vr();
        if (Vr == null || Vr == this || Vr.top == null) {
            return this.top;
        }
        return Vr.top;
    }

    public C3810vI atR() {
        PropertiesUiFromCss Vr = this.cdV.mo13049Vr();
        if (Vr == null || Vr == this || Vr.verticalAlign == null) {
            return this.verticalAlign;
        }
        return Vr.verticalAlign;
    }

    public C1366Tu atS() {
        PropertiesUiFromCss Vr = this.cdV.mo13049Vr();
        if (Vr == null || Vr == this || Vr.width == null) {
            return this.width;
        }
        return Vr.width;
    }

    public int hashCode() {
        int i = this.hash;
        if (i == 0) {
            i = ((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((2120326402 ^ AdapterUiCss.hashCode(this.cdV)) * 31) ^ AdapterUiCss.hashCode(this.backgroundColor)) * 31) ^ AdapterUiCss.hashCode(this.backgroundFill)) * 31) ^ AdapterUiCss.hashCode(this.backgroundImage)) * 31) ^ AdapterUiCss.hashCode(this.backgroundPosition)) * 31) ^ AdapterUiCss.hashCode(this.backgroundRepeat)) * 31) ^ AdapterUiCss.hashCode(this.blurBehind)) * 31) ^ AdapterUiCss.hashCode(this.borderColorMultiplier)) * 31) ^ AdapterUiCss.hashCode(this.borderSpacing)) * 31) ^ AdapterUiCss.hashCode(this.borderStyle)) * 31) ^ AdapterUiCss.hashCode(this.borderBottomImage)) * 31) ^ AdapterUiCss.hashCode(this.borderBottomLeftImage)) * 31) ^ AdapterUiCss.hashCode(this.borderBottomRightImage)) * 31) ^ AdapterUiCss.hashCode(this.color)) * 31) ^ AdapterUiCss.hashCode(this.colorMultiplier)) * 31) ^ AdapterUiCss.hashCode(this.font)) * 31) ^ AdapterUiCss.hashCode(this.height)) * 31) ^ AdapterUiCss.hashCode(this.textAlign)) * 31) ^ AdapterUiCss.hashCode(this.iconImage)) * 31) ^ AdapterUiCss.hashCode(this.cdY)) * 31) ^ AdapterUiCss.hashCode(this.left)) * 31) ^ AdapterUiCss.hashCode(this.borderLeftFill)) * 31) ^ AdapterUiCss.hashCode(this.borderLeftImage)) * 31) ^ AdapterUiCss.hashCode(this.marginBottom)) * 31) ^ AdapterUiCss.hashCode(this.marginLeft)) * 31) ^ AdapterUiCss.hashCode(this.marginRight)) * 31) ^ AdapterUiCss.hashCode(this.marginTop)) * 31) ^ AdapterUiCss.hashCode(this.maxHeight)) * 31) ^ AdapterUiCss.hashCode(this.maxWidth)) * 31) ^ AdapterUiCss.hashCode(this.minHeight)) * 31) ^ AdapterUiCss.hashCode(this.minimumSize)) * 31) ^ AdapterUiCss.hashCode(this.minWidth)) * 31) ^ AdapterUiCss.hashCode(this.paddingBottom)) * 31) ^ AdapterUiCss.hashCode(this.paddingLeft)) * 31) ^ AdapterUiCss.hashCode(this.paddingRight)) * 31) ^ AdapterUiCss.hashCode(this.paddingTop)) * 31) ^ AdapterUiCss.hashCode(this.borderRightFill)) * 31) ^ AdapterUiCss.hashCode(this.borderRightImage)) * 31) ^ AdapterUiCss.hashCode(this.shadowColor)) * 31) ^ AdapterUiCss.hashCode(this.fontShadowX)) * 31) ^ AdapterUiCss.hashCode(this.fontShadowY)) * 31) ^ AdapterUiCss.hashCode(this.top)) * 31) ^ AdapterUiCss.hashCode(this.borderTopImage)) * 31) ^ AdapterUiCss.hashCode(this.borderTopLeftImage)) * 31) ^ AdapterUiCss.hashCode(this.borderTopRightImage)) * 31) ^ AdapterUiCss.hashCode(this.verticalAlign)) * 31) ^ AdapterUiCss.hashCode(this.width);
            if (i == 0) {
                i = 1;
            }
            this.hash = i;
        }
        return i;
    }

    //Присваение параметров из стиля css системе событий
    /* access modifiers changed from: protected */
    /* JADX WARNING: Removed duplicated region for block: B:115:0x0516  */
    /* JADX WARNING: Removed duplicated region for block: B:139:0x0527 A[SYNTHETIC] */
    /* renamed from: a */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void setCssToUI(C5898aca r11, boolean r12) {
        /*
            r10 = this;
            a.aeK r0 = r10.cdV
            java.lang.String r1 = "vertical-align"
            r2 = 0
            org.w3c.dom.css.CSSValue r0 = r11.mo3817a((logic.ui.C1162RC) r0, (java.lang.String) r1, (boolean) r2)
            if (r0 == 0) goto L_0x001d
            java.lang.String r0 = r0.getCssText()
            java.lang.String r0 = r0.toLowerCase()
            java.util.Map<java.lang.String, a.vI> r1 = cdw
            java.lang.Object r0 = r1.get(r0)
            a.vI r0 = (logic.ui.C3810vI) r0
            r10.ceJ = r0
        L_0x001d:
            a.vI r0 = r10.ceJ
            if (r0 != 0) goto L_0x0027
            if (r12 == 0) goto L_0x0027
            a.vI r0 = logic.ui.C3810vI.CENTER
            r10.ceJ = r0
        L_0x0027:
            a.xe r0 = p001a.C3977xe.anu()
            a.azB r2 = r0.adz()
            a.aeK r0 = r10.cdV
            java.lang.String r1 = "text-align"
            r3 = 0
            org.w3c.dom.css.CSSValue r0 = r11.mo3817a((logic.ui.C1162RC) r0, (java.lang.String) r1, (boolean) r3)
            if (r0 == 0) goto L_0x004c
            java.lang.String r0 = r0.getCssText()
            java.lang.String r0 = r0.toLowerCase()
            java.util.Map<java.lang.String, a.aRU> r1 = cdv
            java.lang.Object r0 = r1.get(r0)
            a.aRU r0 = (logic.ui.aRU) r0
            r10.cdX = r0
        L_0x004c:
            a.aRU r0 = r10.cdX
            if (r0 != 0) goto L_0x0056
            if (r12 == 0) goto L_0x0056
            a.aRU r0 = logic.ui.aRU.CENTER
            r10.cdX = r0
        L_0x0056:
            a.aeK r0 = r10.cdV
            java.lang.String r1 = "cursor"
            r3 = 0
            java.lang.String r0 = r11.mo16549e(r0, r1, r3)
            if (r0 == 0) goto L_0x0088
            a.aeK r1 = r10.cdV
            java.lang.String r3 = "cursor-top"
            r4 = 0
            a.Tu r5 = logic.swing.C1366Tu.fEE
            a.Tu r1 = r11.mo16534a((logic.ui.C1162RC) r1, (java.lang.String) r3, (boolean) r4, (logic.swing.C1366Tu) r5)
            a.aeK r3 = r10.cdV
            java.lang.String r4 = "cursor-left"
            r5 = 0
            a.Tu r6 = logic.swing.C1366Tu.fEE
            a.Tu r3 = r11.mo16534a((logic.ui.C1162RC) r3, (java.lang.String) r4, (boolean) r5, (logic.swing.C1366Tu) r6)
            java.awt.Point r4 = new java.awt.Point
            float r3 = r3.value
            int r3 = (int) r3
            float r1 = r1.value
            int r1 = (int) r1
            r4.<init>(r3, r1)
            java.awt.Cursor r0 = r2.mo16001a(r0, r4)
            r10.cdU = r0
        L_0x0088:
            a.aeK r0 = r10.cdV
            java.lang.String r1 = "direction"
            r3 = 0
            org.w3c.dom.css.CSSValue r0 = r11.mo3817a((logic.ui.C1162RC) r0, (java.lang.String) r1, (boolean) r3)
            if (r0 == 0) goto L_0x00a5
            java.lang.String r0 = r0.getCssText()
            java.lang.String r0 = r0.toLowerCase()
            java.util.Map<java.lang.String, java.awt.ComponentOrientation> r1 = cdx
            java.lang.Object r0 = r1.get(r0)
            java.awt.ComponentOrientation r0 = (java.awt.ComponentOrientation) r0
            r10.ceL = r0
        L_0x00a5:
            java.awt.ComponentOrientation r0 = r10.ceL
            if (r0 != 0) goto L_0x00af
            if (r12 == 0) goto L_0x00af
            java.awt.ComponentOrientation r0 = java.awt.ComponentOrientation.UNKNOWN
            r10.ceL = r0
        L_0x00af:
            a.aeK r0 = r10.cdV
            java.lang.String r1 = "border-left-image-repeat"
            r3 = 0
            org.w3c.dom.css.CSSValue r1 = r11.mo3817a((logic.ui.C1162RC) r0, (java.lang.String) r1, (boolean) r3)
            if (r12 == 0) goto L_0x044e
            a.xo r0 = logic.swing.C3991xo.STRETCH_Y
        L_0x00bc:
            a.xo r0 = r10.m457a((org.w3c.dom.css.CSSValue) r1, (logic.swing.C3991xo) r0)
            r10.cdL = r0
            a.aeK r0 = r10.cdV
            java.lang.String r1 = "border-right-image-repeat"
            r3 = 0
            org.w3c.dom.css.CSSValue r1 = r11.mo3817a((logic.ui.C1162RC) r0, (java.lang.String) r1, (boolean) r3)
            if (r12 == 0) goto L_0x0451
            a.xo r0 = logic.swing.C3991xo.STRETCH_Y
        L_0x00cf:
            a.xo r0 = r10.m457a((org.w3c.dom.css.CSSValue) r1, (logic.swing.C3991xo) r0)
            r10.cdN = r0
            a.aeK r0 = r10.cdV
            java.lang.String r1 = "border-bottom-image-repeat"
            r3 = 0
            org.w3c.dom.css.CSSValue r1 = r11.mo3817a((logic.ui.C1162RC) r0, (java.lang.String) r1, (boolean) r3)
            if (r12 == 0) goto L_0x0454
            a.xo r0 = logic.swing.C3991xo.STRETCH_X
        L_0x00e2:
            a.xo r0 = r10.m457a((org.w3c.dom.css.CSSValue) r1, (logic.swing.C3991xo) r0)
            r10.cdG = r0
            a.aeK r0 = r10.cdV
            java.lang.String r1 = "border-top-image-repeat"
            r3 = 0
            org.w3c.dom.css.CSSValue r1 = r11.mo3817a((logic.ui.C1162RC) r0, (java.lang.String) r1, (boolean) r3)
            if (r12 == 0) goto L_0x0457
            a.xo r0 = logic.swing.C3991xo.STRETCH_X
        L_0x00f5:
            a.xo r0 = r10.m457a((org.w3c.dom.css.CSSValue) r1, (logic.swing.C3991xo) r0)
            r10.cdR = r0
            a.aeK r0 = r10.cdV
            java.lang.String r1 = "border-color-multiplier"
            r3 = 0
            java.awt.Color r0 = r11.mo16554g(r0, r1, r3)
            r10.cdJ = r0
            a.aeK r0 = r10.cdV
            r1 = 0
            java.awt.Color r0 = r11.mo16535a((logic.ui.C1162RC) r0, (boolean) r1)
            r10.color = r0
            a.aeK r0 = r10.cdV
            a.Ro r0 = r11.mo16543d((logic.ui.C1162RC) r0)
            if (r0 == 0) goto L_0x012d
            a.xe r1 = p001a.C3977xe.anu()
            java.lang.String r3 = r0.getFontName()
            int r4 = r0.getFontStyle()
            int r0 = r0.getFontSize()
            java.awt.Font r0 = r1.getFont(r3, r4, r0)
            r10.font = r0
        L_0x012d:
            a.aeK r0 = r10.cdV
            java.lang.String r1 = "background-color"
            r3 = 0
            org.w3c.dom.css.CSSValue r0 = r11.mo3817a((logic.ui.C1162RC) r0, (java.lang.String) r1, (boolean) r3)
            if (r0 == 0) goto L_0x014c
            short r1 = r0.getCssValueType()
            r3 = 2
            if (r1 != r3) goto L_0x0468
            org.w3c.dom.css.CSSValueList r0 = (org.w3c.dom.css.CSSValueList) r0
            int r3 = r0.getLength()
            java.awt.Color[] r4 = new java.awt.Color[r3]
            r1 = 0
        L_0x0148:
            if (r1 < r3) goto L_0x045a
            r10.cdy = r4
        L_0x014c:
            a.aeK r0 = r10.cdV
            java.lang.String r1 = "mouse-over-sound"
            r3 = 0
            java.lang.String r0 = r11.mo16549e(r0, r1, r3)
            r10.ces = r0
            a.aeK r0 = r10.cdV
            java.lang.String r1 = "mouse-press-sound"
            r3 = 0
            java.lang.String r0 = r11.mo16549e(r0, r1, r3)
            r10.cet = r0
            a.aeK r0 = r10.cdV
            java.lang.String r1 = "mouse-release-sound"
            r3 = 0
            java.lang.String r0 = r11.mo16549e(r0, r1, r3)
            r10.ceu = r0
            a.aeK r0 = r10.cdV
            java.lang.String r1 = "key-press-sound"
            r3 = 0
            java.lang.String r0 = r11.mo16549e(r0, r1, r3)
            r10.cew = r0
            a.aeK r0 = r10.cdV
            java.lang.String r1 = "key-release-sound"
            r3 = 0
            java.lang.String r0 = r11.mo16549e(r0, r1, r3)
            r10.cex = r0
            a.aeK r0 = r10.cdV
            java.lang.String r1 = "key-typed-sound"
            r3 = 0
            java.lang.String r0 = r11.mo16549e(r0, r1, r3)
            r10.cey = r0
            a.aeK r0 = r10.cdV
            java.lang.String r1 = "mouse-click-sound"
            r3 = 0
            java.lang.String r0 = r11.mo16549e(r0, r1, r3)
            r10.cev = r0
            a.aeK r0 = r10.cdV
            java.lang.String r1 = "action-sound"
            r3 = 0
            java.lang.String r0 = r11.mo16549e(r0, r1, r3)
            r10.cez = r0
            a.aeK r0 = r10.cdV
            java.lang.String r1 = "drop-sucess-sound"
            r3 = 0
            java.lang.String r0 = r11.mo16549e(r0, r1, r3)
            r10.ceA = r0
            a.aeK r0 = r10.cdV
            java.lang.String r1 = "drop-fail-sound"
            r3 = 0
            java.lang.String r0 = r11.mo16549e(r0, r1, r3)
            r10.ceB = r0
            a.aeK r0 = r10.cdV
            java.lang.String r1 = "drag-start-sound"
            r3 = 0
            java.lang.String r0 = r11.mo16549e(r0, r1, r3)
            r10.ceC = r0
            a.aeK r0 = r10.cdV
            java.lang.String r1 = "open-sound"
            r3 = 0
            java.lang.String r0 = r11.mo16549e(r0, r1, r3)
            r10.ceD = r0
            a.aeK r0 = r10.cdV
            java.lang.String r1 = "close-sound"
            r3 = 0
            java.lang.String r0 = r11.mo16549e(r0, r1, r3)
            r10.ceE = r0
            a.aeK r0 = r10.cdV
            java.lang.String r1 = "collapse-sound"
            r3 = 0
            java.lang.String r0 = r11.mo16549e(r0, r1, r3)
            r10.ceF = r0
            a.aeK r0 = r10.cdV
            java.lang.String r1 = "expand-sound"
            r3 = 0
            java.lang.String r0 = r11.mo16549e(r0, r1, r3)
            r10.ceG = r0
            a.aeK r0 = r10.cdV
            java.lang.String r1 = "scroll-sound"
            r3 = 0
            java.lang.String r0 = r11.mo16549e(r0, r1, r3)
            r10.ceH = r0
            a.aeK r0 = r10.cdV
            java.lang.String r1 = "type-animation-sound"
            r3 = 0
            java.lang.String r0 = r11.mo16549e(r0, r1, r3)
            r10.ceI = r0
            a.aeK r0 = r10.cdV
            java.lang.String r1 = "border-style"
            r3 = 0
            java.lang.String r0 = r11.mo16549e(r0, r1, r3)
            r10.cdP = r0
            a.aeK r0 = r10.cdV
            java.lang.String r1 = "border-top-left-image"
            r3 = 0
            java.lang.String r0 = r11.mo16549e(r0, r1, r3)
            java.awt.Image r0 = r2.getImage(r0)
            r10.cdS = r0
            a.aeK r0 = r10.cdV
            java.lang.String r1 = "border-top-right-image"
            r3 = 0
            java.lang.String r0 = r11.mo16549e(r0, r1, r3)
            java.awt.Image r0 = r2.getImage(r0)
            r10.cdT = r0
            a.aeK r0 = r10.cdV
            java.lang.String r1 = "border-top-image"
            r3 = 0
            java.lang.String r0 = r11.mo16549e(r0, r1, r3)
            java.awt.Image r0 = r2.getImage(r0)
            r10.cdQ = r0
            a.aeK r0 = r10.cdV
            java.lang.String r1 = "background-image"
            r3 = 0
            org.w3c.dom.css.CSSValue r0 = r11.mo3817a((logic.ui.C1162RC) r0, (java.lang.String) r1, (boolean) r3)
            if (r0 == 0) goto L_0x025e
            short r1 = r0.getCssValueType()
            r3 = 2
            if (r1 != r3) goto L_0x0488
            org.w3c.dom.css.CSSValueList r0 = (org.w3c.dom.css.CSSValueList) r0
            int r3 = r0.getLength()
            java.awt.Image[] r4 = new java.awt.Image[r3]
            r1 = 0
        L_0x025a:
            if (r1 < r3) goto L_0x0476
            r10.cdA = r4
        L_0x025e:
            a.aeK r0 = r10.cdV
            java.lang.String r1 = "background-repeat"
            r3 = 0
            org.w3c.dom.css.CSSValue r0 = r11.mo3817a((logic.ui.C1162RC) r0, (java.lang.String) r1, (boolean) r3)
            r1 = 0
            a.xo[] r0 = r10.m459a((org.w3c.dom.css.CSSValue) r0, (logic.swing.C3991xo[]) r1)
            r10.cdC = r0
            a.aeK r0 = r10.cdV
            java.lang.String r1 = "glow-image"
            r3 = 0
            java.lang.String r0 = r11.mo16549e(r0, r1, r3)
            java.awt.Image r0 = r2.getImage(r0)
            r10.cdE = r0
            a.aeK r0 = r10.cdV
            java.lang.String r1 = "border-left-image"
            r3 = 0
            java.lang.String r0 = r11.mo16549e(r0, r1, r3)
            java.awt.Image r0 = r2.getImage(r0)
            r10.cdK = r0
            a.aeK r0 = r10.cdV
            java.lang.String r1 = "border-right-image"
            r3 = 0
            java.lang.String r0 = r11.mo16549e(r0, r1, r3)
            java.awt.Image r0 = r2.getImage(r0)
            r10.cdM = r0
            a.aeK r0 = r10.cdV
            java.lang.String r1 = "border-bottom-image"
            r3 = 0
            java.lang.String r0 = r11.mo16549e(r0, r1, r3)
            java.awt.Image r0 = r2.getImage(r0)
            r10.cdF = r0
            a.aeK r0 = r10.cdV
            java.lang.String r1 = "border-bottom-left-image"
            r3 = 0
            java.lang.String r0 = r11.mo16549e(r0, r1, r3)
            java.awt.Image r0 = r2.getImage(r0)
            r10.cdH = r0
            a.aeK r0 = r10.cdV
            java.lang.String r1 = "border-bottom-right-image"
            r3 = 0
            java.lang.String r0 = r11.mo16549e(r0, r1, r3)
            java.awt.Image r0 = r2.getImage(r0)
            r10.cdI = r0
            a.aeK r0 = r10.cdV
            java.lang.String r1 = "background-fill"
            r3 = 0
            java.lang.String r0 = r11.mo16549e(r0, r1, r3)
            java.awt.Image r0 = r2.getImage(r0)
            r10.cdz = r0
            a.aeK r0 = r10.cdV
            java.lang.String r1 = "border-left-fill"
            r3 = 0
            java.lang.String r0 = r11.mo16549e(r0, r1, r3)
            java.awt.Image r0 = r2.getImage(r0)
            r10.cea = r0
            a.aeK r0 = r10.cdV
            java.lang.String r1 = "border-right-fill"
            r3 = 0
            java.lang.String r0 = r11.mo16549e(r0, r1, r3)
            java.awt.Image r0 = r2.getImage(r0)
            r10.ceo = r0
            a.aeK r0 = r10.cdV
            java.lang.String r1 = "blur-behind"
            r3 = 0
            java.lang.String r0 = r11.mo16549e(r0, r1, r3)
            if (r0 == 0) goto L_0x030c
            java.lang.String r1 = "true"
            boolean r0 = r1.equals(r0)
            java.lang.Boolean r0 = java.lang.Boolean.valueOf(r0)
            r10.cdD = r0
        L_0x030c:
            a.aeK r0 = r10.cdV
            java.lang.String r1 = "icon"
            r3 = 0
            java.lang.String r0 = r11.mo16549e(r0, r1, r3)
            java.awt.Image r0 = r2.getImage(r0)
            r10.f71Td = r0
            if (r12 == 0) goto L_0x049a
            a.Tu r0 = logic.swing.C1366Tu.fEC
            r3 = r0
        L_0x0320:
            a.aeK r0 = r10.cdV
            java.lang.String r1 = "margin-top"
            r2 = 0
            a.Tu r0 = r11.mo16534a((logic.ui.C1162RC) r0, (java.lang.String) r1, (boolean) r2, (logic.swing.C1366Tu) r3)
            r10.cee = r0
            a.aeK r0 = r10.cdV
            java.lang.String r1 = "margin-bottom"
            r2 = 0
            a.Tu r0 = r11.mo16534a((logic.ui.C1162RC) r0, (java.lang.String) r1, (boolean) r2, (logic.swing.C1366Tu) r3)
            r10.ceb = r0
            a.aeK r0 = r10.cdV
            java.lang.String r1 = "margin-left"
            r2 = 0
            a.Tu r0 = r11.mo16534a((logic.ui.C1162RC) r0, (java.lang.String) r1, (boolean) r2, (logic.swing.C1366Tu) r3)
            r10.cec = r0
            a.aeK r0 = r10.cdV
            java.lang.String r1 = "margin-right"
            r2 = 0
            a.Tu r0 = r11.mo16534a((logic.ui.C1162RC) r0, (java.lang.String) r1, (boolean) r2, (logic.swing.C1366Tu) r3)
            r10.ced = r0
            a.aeK r0 = r10.cdV
            java.lang.String r1 = "padding-top"
            r2 = 0
            a.Tu r0 = r11.mo16534a((logic.ui.C1162RC) r0, (java.lang.String) r1, (boolean) r2, (logic.swing.C1366Tu) r3)
            r10.cen = r0
            a.aeK r0 = r10.cdV
            java.lang.String r1 = "padding-bottom"
            r2 = 0
            a.Tu r0 = r11.mo16534a((logic.ui.C1162RC) r0, (java.lang.String) r1, (boolean) r2, (logic.swing.C1366Tu) r3)
            r10.cek = r0
            a.aeK r0 = r10.cdV
            java.lang.String r1 = "padding-left"
            r2 = 0
            a.Tu r0 = r11.mo16534a((logic.ui.C1162RC) r0, (java.lang.String) r1, (boolean) r2, (logic.swing.C1366Tu) r3)
            r10.cel = r0
            a.aeK r0 = r10.cdV
            java.lang.String r1 = "padding-right"
            r2 = 0
            a.Tu r0 = r11.mo16534a((logic.ui.C1162RC) r0, (java.lang.String) r1, (boolean) r2, (logic.swing.C1366Tu) r3)
            r10.cem = r0
            a.aeK r0 = r10.cdV
            java.lang.String r1 = "color-multiplier"
            r2 = 0
            java.awt.Color r0 = r11.mo16554g(r0, r1, r2)
            r10.colorMultiplier = r0
            a.aeK r0 = r10.cdV
            java.lang.String r1 = "font-shadow-x"
            r2 = 0
            a.Tu r0 = r11.mo16534a((logic.ui.C1162RC) r0, (java.lang.String) r1, (boolean) r2, (logic.swing.C1366Tu) r3)
            r10.cep = r0
            a.aeK r0 = r10.cdV
            java.lang.String r1 = "font-shadow-y"
            r2 = 0
            a.Tu r0 = r11.mo16534a((logic.ui.C1162RC) r0, (java.lang.String) r1, (boolean) r2, (logic.swing.C1366Tu) r3)
            r10.ceq = r0
            a.aeK r0 = r10.cdV
            java.lang.String r1 = "max-height"
            r2 = 0
            a.Tu r0 = r11.mo16534a((logic.ui.C1162RC) r0, (java.lang.String) r1, (boolean) r2, (logic.swing.C1366Tu) r3)
            r10.cef = r0
            a.aeK r0 = r10.cdV
            java.lang.String r1 = "max-width"
            r2 = 0
            a.Tu r0 = r11.mo16534a((logic.ui.C1162RC) r0, (java.lang.String) r1, (boolean) r2, (logic.swing.C1366Tu) r3)
            r10.ceg = r0
            a.aeK r0 = r10.cdV
            java.lang.String r1 = "border-spacing"
            r2 = 0
            org.w3c.dom.css.CSSValue r0 = r11.mo3817a((logic.ui.C1162RC) r0, (java.lang.String) r1, (boolean) r2)
            a.jM r0 = r11.mo16547e((org.w3c.dom.css.CSSValue) r0)
            r10.cdO = r0
            a.aeK r0 = r10.cdV
            java.lang.String r1 = "background-position"
            r2 = 0
            org.w3c.dom.css.CSSValue r1 = r11.mo3817a((logic.ui.C1162RC) r0, (java.lang.String) r1, (boolean) r2)
            if (r1 == 0) goto L_0x03f2
            r6 = 0
            r5 = 0
            java.util.ArrayList r8 = new java.util.ArrayList
            r8.<init>()
            r0 = 1
            short r2 = r1.getCssValueType()
            r4 = 2
            if (r2 != r4) goto L_0x054e
            r0 = r1
            org.w3c.dom.css.CSSValueList r0 = (org.w3c.dom.css.CSSValueList) r0
            int r0 = r0.getLength()
            r4 = r0
        L_0x03e0:
            r0 = 0
            r7 = r0
        L_0x03e2:
            if (r7 < r4) goto L_0x049e
            int r0 = r8.size()
            a.jM[] r0 = new logic.swing.C2741jM[r0]
            java.lang.Object[] r0 = r8.toArray(r0)
            a.jM[] r0 = (logic.swing.C2741jM[]) r0
            r10.cdB = r0
        L_0x03f2:
            a.aeK r0 = r10.cdV
            java.lang.String r1 = "min-height"
            r2 = 0
            a.Tu r0 = r11.mo16534a((logic.ui.C1162RC) r0, (java.lang.String) r1, (boolean) r2, (logic.swing.C1366Tu) r3)
            r10.ceh = r0
            a.aeK r1 = r10.cdV
            java.lang.String r2 = "min-width"
            r4 = 0
            a.Tu r1 = r11.mo16534a((logic.ui.C1162RC) r1, (java.lang.String) r2, (boolean) r4, (logic.swing.C1366Tu) r3)
            r10.cej = r1
            if (r0 == 0) goto L_0x0421
            if (r1 == 0) goto L_0x0421
            a.Tu r2 = logic.swing.C1366Tu.fEC
            if (r0 == r2) goto L_0x0421
            a.Tu r2 = logic.swing.C1366Tu.fEC
            if (r1 == r2) goto L_0x0421
            float r0 = r0.value
            int r0 = (int) r0
            float r1 = r1.value
            int r1 = (int) r1
            java.awt.Dimension r2 = new java.awt.Dimension
            r2.<init>(r1, r0)
            r10.cei = r2
        L_0x0421:
            a.aeK r0 = r10.cdV
            java.lang.String r1 = "height"
            r2 = 0
            a.Tu r0 = r11.mo16534a((logic.ui.C1162RC) r0, (java.lang.String) r1, (boolean) r2, (logic.swing.C1366Tu) r3)
            r10.cdW = r0
            a.aeK r0 = r10.cdV
            java.lang.String r1 = "width"
            r2 = 0
            a.Tu r0 = r11.mo16534a((logic.ui.C1162RC) r0, (java.lang.String) r1, (boolean) r2, (logic.swing.C1366Tu) r3)
            r10.ceK = r0
            a.aeK r0 = r10.cdV
            java.lang.String r1 = "top"
            r2 = 0
            a.Tu r0 = r11.mo16534a((logic.ui.C1162RC) r0, (java.lang.String) r1, (boolean) r2, (logic.swing.C1366Tu) r3)
            r10.cer = r0
            a.aeK r0 = r10.cdV
            java.lang.String r1 = "left"
            r2 = 0
            a.Tu r0 = r11.mo16534a((logic.ui.C1162RC) r0, (java.lang.String) r1, (boolean) r2, (logic.swing.C1366Tu) r3)
            r10.cdZ = r0
            return
        L_0x044e:
            r0 = 0
            goto L_0x00bc
        L_0x0451:
            r0 = 0
            goto L_0x00cf
        L_0x0454:
            r0 = 0
            goto L_0x00e2
        L_0x0457:
            r0 = 0
            goto L_0x00f5
        L_0x045a:
            org.w3c.dom.css.CSSValue r5 = r0.item(r1)
            java.awt.Color r5 = r11.mo16544d((org.w3c.dom.css.CSSValue) r5)
            r4[r1] = r5
            int r1 = r1 + 1
            goto L_0x0148
        L_0x0468:
            r1 = 1
            java.awt.Color[] r1 = new java.awt.Color[r1]
            r3 = 0
            java.awt.Color r0 = r11.mo16544d((org.w3c.dom.css.CSSValue) r0)
            r1[r3] = r0
            r10.cdy = r1
            goto L_0x014c
        L_0x0476:
            org.w3c.dom.css.CSSValue r5 = r0.item(r1)
            java.lang.String r5 = r10.m458a((org.w3c.dom.css.CSSValue) r5)
            java.awt.Image r5 = r2.getImage(r5)
            r4[r1] = r5
            int r1 = r1 + 1
            goto L_0x025a
        L_0x0488:
            r1 = 1
            java.awt.Image[] r1 = new java.awt.Image[r1]
            r3 = 0
            java.lang.String r0 = r10.m458a((org.w3c.dom.css.CSSValue) r0)
            java.awt.Image r0 = r2.getImage(r0)
            r1[r3] = r0
            r10.cdA = r1
            goto L_0x025e
        L_0x049a:
            r0 = 0
            r3 = r0
            goto L_0x0320
        L_0x049e:
            short r0 = r1.getCssValueType()
            r2 = 2
            if (r0 != r2) goto L_0x052d
            r0 = r1
            org.w3c.dom.css.CSSValueList r0 = (org.w3c.dom.css.CSSValueList) r0
            org.w3c.dom.css.CSSValue r2 = r0.item(r7)
        L_0x04ac:
            short r0 = r2.getCssValueType()
            r9 = 1
            if (r0 != r9) goto L_0x0545
            r0 = r2
            org.w3c.dom.css.CSSPrimitiveValue r0 = (org.w3c.dom.css.CSSPrimitiveValue) r0
            java.lang.String r0 = r0.getCssText()
            java.lang.String r9 = r0.toLowerCase()
            java.util.Map<java.lang.String, a.Tu> r0 = cdt
            java.lang.Object r0 = r0.get(r9)
            a.Tu r0 = (logic.swing.C1366Tu) r0
            if (r0 == 0) goto L_0x0530
            java.lang.String r2 = "left"
            boolean r2 = r2.equals(r9)
            if (r2 != 0) goto L_0x04d8
            java.lang.String r2 = "right"
            boolean r2 = r2.equals(r9)
            if (r2 == 0) goto L_0x054a
        L_0x04d8:
            if (r5 != 0) goto L_0x0547
            a.Tu r2 = new a.Tu
            r5 = 1112014848(0x42480000, float:50.0)
            a.Tu$a r6 = logic.swing.C1366Tu.C1367a.PERCENT
            r2.<init>(r5, r6)
            r6 = r0
        L_0x04e4:
            java.lang.String r5 = "center"
            boolean r5 = r5.equals(r9)
            if (r5 == 0) goto L_0x04f2
            if (r6 != 0) goto L_0x04ef
            r6 = r0
        L_0x04ef:
            if (r2 != 0) goto L_0x04f2
            r2 = r0
        L_0x04f2:
            java.lang.String r5 = "bottom"
            boolean r5 = r5.equals(r9)
            if (r5 != 0) goto L_0x0502
            java.lang.String r5 = "top"
            boolean r5 = r5.equals(r9)
            if (r5 == 0) goto L_0x050e
        L_0x0502:
            if (r6 != 0) goto L_0x054c
            a.Tu r6 = new a.Tu
            r2 = 1112014848(0x42480000, float:50.0)
            a.Tu$a r5 = logic.swing.C1366Tu.C1367a.PERCENT
            r6.<init>(r2, r5)
            r2 = r0
        L_0x050e:
            r0 = r7 & 1
            if (r0 != 0) goto L_0x0516
            int r0 = r4 + -1
            if (r7 != r0) goto L_0x0527
        L_0x0516:
            a.jM r9 = new a.jM
            if (r6 != 0) goto L_0x0541
            a.Tu r0 = logic.swing.C1366Tu.fEE
            r5 = r0
        L_0x051d:
            if (r2 != 0) goto L_0x0543
            a.Tu r0 = logic.swing.C1366Tu.fEE
        L_0x0521:
            r9.<init>(r5, r0)
            r8.add(r9)
        L_0x0527:
            int r0 = r7 + 1
            r7 = r0
            r5 = r2
            goto L_0x03e2
        L_0x052d:
            r2 = r1
            goto L_0x04ac
        L_0x0530:
            if (r7 != 0) goto L_0x0538
            a.Tu r6 = logic.res.css.C6868avI.m26540c((org.w3c.dom.css.CSSValue) r2)
            r2 = r5
            goto L_0x050e
        L_0x0538:
            r0 = 1
            if (r7 != r0) goto L_0x0545
            a.Tu r5 = logic.res.css.C6868avI.m26540c((org.w3c.dom.css.CSSValue) r2)
            r2 = r5
            goto L_0x050e
        L_0x0541:
            r5 = r6
            goto L_0x051d
        L_0x0543:
            r0 = r2
            goto L_0x0521
        L_0x0545:
            r2 = r5
            goto L_0x050e
        L_0x0547:
            r2 = r5
            r6 = r0
            goto L_0x04e4
        L_0x054a:
            r2 = r5
            goto L_0x04e4
        L_0x054c:
            r2 = r0
            goto L_0x050e
        L_0x054e:
            r4 = r0
            goto L_0x03e0
        */
        throw new UnsupportedOperationException("Method not decompiled: logic.ui.C0061Aj.mo368a(a.aca, boolean):void");
    }

    /* renamed from: a */
    private String m458a(CSSValue cSSValue) {
        if (cSSValue != null && cSSValue.getCssValueType() == 1) {
            return ((CSSPrimitiveValue) cSSValue).getStringValue();
        }
        return null;
    }

    /* renamed from: b */
    private C2741jM m460b(CSSValue cSSValue) {
        CSSValue cSSValue2;
        C1366Tu tu;
        C1366Tu tu2 = null;
        C1366Tu tu3 = null;
        for (int i = 0; i < 2; i++) {
            if (cSSValue.getCssValueType() != 2) {
                if (i > 0) {
                    break;
                }
                cSSValue2 = cSSValue;
            } else {
                cSSValue2 = ((CSSValueList) cSSValue).item(i);
            }
            if (cSSValue2.getCssValueType() == 1) {
                String lowerCase = ((CSSPrimitiveValue) cSSValue2).getCssText().toLowerCase();
                C1366Tu tu4 = cdt.get(lowerCase);
                if (tu4 != null) {
                    if (!"left".equals(lowerCase) && !"right".equals(lowerCase)) {
                        tu = tu2;
                    } else if (tu2 == null) {
                        tu = new C1366Tu(50.0f, C1366Tu.C1367a.PERCENT);
                        tu3 = tu4;
                    } else {
                        tu = tu2;
                        tu3 = tu4;
                    }
                    if ("center".equals(lowerCase)) {
                        if (tu3 == null) {
                            tu3 = tu4;
                        }
                        if (tu == null) {
                            tu = tu4;
                        }
                    }
                    if (!"bottom".equals(lowerCase) && !"top".equals(lowerCase)) {
                        tu2 = tu;
                    } else if (tu3 == null) {
                        tu3 = new C1366Tu(50.0f, C1366Tu.C1367a.PERCENT);
                        tu2 = tu4;
                    } else {
                        tu2 = tu4;
                    }
                } else if (i == 0) {
                    tu3 = C6868avI.m26540c(cSSValue2);
                } else if (i == 1) {
                    tu2 = C6868avI.m26540c(cSSValue2);
                }
            }
        }
        if (tu3 == null) {
            tu3 = C1366Tu.fEE;
        }
        if (tu2 == null) {
            tu2 = C1366Tu.fEE;
        }
        return new C2741jM(tu3, tu2);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:1:0x0002, code lost:
        r0 = cdu.get(r3.getCssText().toLowerCase());
     */
    /* renamed from: a */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private C3991xo m457a(CSSValue r3, C3991xo r4) {
        if (r3 != null) {
            String var3 = r3.getCssText().toLowerCase();
            C3991xo var4 = (C3991xo) cdu.get(var3);
            return var4 == null ? r4 : var4;
        } else {
            return r4;
        }
    }

    /* renamed from: a */
    private C3991xo[] m459a(CSSValue cSSValue, C3991xo[] xoVarArr) {
        if (cSSValue == null) {
            return xoVarArr;
        }
        if (cSSValue.getCssValueType() == 2) {
            CSSValueList cSSValueList = (CSSValueList) cSSValue;
            int length = cSSValueList.getLength();
            C3991xo[] xoVarArr2 = new C3991xo[length];
            for (int i = 0; i < length; i++) {
                xoVarArr2[i] = m457a(cSSValueList.item(i), C3991xo.STRETCH);
            }
            return xoVarArr2;
        }
        return new C3991xo[]{m457a(cSSValue, C3991xo.STRETCH)};
    }

    /* renamed from: a */
    public void mo369a(Image image) {
        if (this.backgroundImage == null) {
            this.backgroundImage = new Image[]{image};
            return;
        }
        this.backgroundImage[0] = image;
    }

    public int atT() {
        PropertiesUiFromCss Vr = this.cdV.mo13049Vr();
        if (Vr != null && Vr != this && Vr.backgroundImage != null) {
            return Vr.backgroundImage.length;
        }
        if (this.backgroundImage != null) {
            return this.backgroundImage.length;
        }
        return 0;
    }

    public ComponentOrientation getComponentOrientation() {
        return this.direction;
    }

    public String atU() {
        PropertiesUiFromCss Vp = this.cdV.mo13047Vp();
        if (Vp == null || Vp == this || Vp.mouseOverSound == null) {
            return this.mouseOverSound;
        }
        return Vp.mouseOverSound;
    }

    public String atV() {
        PropertiesUiFromCss Vp = this.cdV.mo13047Vp();
        if (Vp == null || Vp == this || Vp.mousePressSound == null) {
            return this.mousePressSound;
        }
        return Vp.mousePressSound;
    }

    public String atW() {
        PropertiesUiFromCss Vp = this.cdV.mo13047Vp();
        if (Vp == null || Vp == this || Vp.mouseReleaseSound == null) {
            return this.mouseReleaseSound;
        }
        return Vp.mouseReleaseSound;
    }

    public String atX() {
        PropertiesUiFromCss Vp = this.cdV.mo13047Vp();
        if (Vp == null || Vp == this || Vp.mouseClickSound == null) {
            return this.mouseClickSound;
        }
        return Vp.mouseClickSound;
    }

    public String atY() {
        PropertiesUiFromCss Vp = this.cdV.mo13047Vp();
        if (Vp == null || Vp == this || Vp.keyReleaseSound == null) {
            return this.keyReleaseSound;
        }
        return Vp.keyReleaseSound;
    }

    public String atZ() {
        PropertiesUiFromCss Vp = this.cdV.mo13047Vp();
        if (Vp == null || Vp == this || Vp.keyPressSound == null) {
            return this.keyPressSound;
        }
        return Vp.keyPressSound;
    }

    public String aua() {
        PropertiesUiFromCss Vp = this.cdV.mo13047Vp();
        if (Vp == null || Vp == this || Vp.keyTypedSound == null) {
            return this.keyTypedSound;
        }
        return Vp.keyTypedSound;
    }

    public String aub() {
        PropertiesUiFromCss Vp = this.cdV.mo13047Vp();
        if (Vp == null || Vp == this || Vp.actionSound == null) {
            return this.actionSound;
        }
        return Vp.actionSound;
    }

    public String auc() {
        PropertiesUiFromCss Vp = this.cdV.mo13047Vp();
        if (Vp == null || Vp == this || Vp.dropSucessSound == null) {
            return this.dropSucessSound;
        }
        return Vp.dropSucessSound;
    }

    public String aud() {
        PropertiesUiFromCss Vp = this.cdV.mo13047Vp();
        if (Vp == null || Vp == this || Vp.dropFailSound == null) {
            return this.dropFailSound;
        }
        return Vp.dropFailSound;
    }

    public String aue() {
        PropertiesUiFromCss Vp = this.cdV.mo13047Vp();
        if (Vp == null || Vp == this || Vp.dragStartSound == null) {
            return this.dragStartSound;
        }
        return Vp.dragStartSound;
    }

    public String auf() {
        PropertiesUiFromCss Vp = this.cdV.mo13047Vp();
        if (Vp == null || Vp == this || Vp.openSound == null) {
            return this.openSound;
        }
        return Vp.openSound;
    }

    public String aug() {
        PropertiesUiFromCss Vp = this.cdV.mo13047Vp();
        if (Vp == null || Vp == this || Vp.expandSound == null) {
            return this.expandSound;
        }
        return Vp.expandSound;
    }

    public String auh() {
        PropertiesUiFromCss Vp = this.cdV.mo13047Vp();
        if (Vp == null || Vp == this || Vp.closeSound == null) {
            return this.closeSound;
        }
        return Vp.closeSound;
    }

    public String aui() {
        PropertiesUiFromCss Vp = this.cdV.mo13047Vp();
        if (Vp == null || Vp == this || Vp.collapseSound == null) {
            return this.collapseSound;
        }
        return Vp.collapseSound;
    }

    public String auj() {
        PropertiesUiFromCss Vp = this.cdV.mo13047Vp();
        if (Vp == null || Vp == this || Vp.scrollSound == null) {
            return this.scrollSound;
        }
        return Vp.scrollSound;
    }

    public String auk() {
        PropertiesUiFromCss Vp = this.cdV.mo13047Vp();
        if (Vp == null || Vp == this || Vp.typeAnimationSound == null) {
            return this.typeAnimationSound;
        }
        return Vp.typeAnimationSound;
    }
}
