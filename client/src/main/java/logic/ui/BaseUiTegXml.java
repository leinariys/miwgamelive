package logic.ui;

import logic.res.*;
import logic.res.css.C6868avI;
import logic.res.css.CssNode;
import logic.res.css.LoaderCss;
import logic.res.sound.ISoundPlayer;
import logic.swing.BasicLookAndFeelSpaceEngine;
import logic.swing.C0454GJ;
import logic.swing.aDX;
import logic.ui.item.*;
import logic.ui.layout.*;
import lombok.extern.slf4j.Slf4j;
import org.w3c.dom.css.CSSStyleDeclaration;
import taikodom.render.RenderView;
import taikodom.render.helpers.RenderTask;
import taikodom.render.scene.SoundObject;

import javax.swing.*;
import javax.swing.event.MouseInputAdapter;
import java.awt.*;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

/**
 * Базовые элементы интерфейса
 */
/* renamed from: a.aiW  reason: case insensitive filesystem */
/* compiled from: a */
@Slf4j
public class BaseUiTegXml extends IBaseUiTegXml {

    /* renamed from: ghk */
    private static final String typeCharacter = "utf-8";
    /* access modifiers changed from: private */
    public MouseInputAdapter ghu = new MouseInputAdapter() {
    };
    /* access modifiers changed from: private */
    public aDX ghv;
    /* renamed from: Th */
    private ILoaderImageInterface loaderImage;
    /* renamed from: ghl */
    private LoaderFileXML loaderFileXml;
    /**
     * Ядро CSS стилей
     */
    /* renamed from: ghm */
    private C6868avI cssNodeCore;
    /**
     * класс озвучки интерфейса class Ix
     */
    private ISoundPlayer soundPlayer;
    private ILoadFonts fontFamily;
    private LoaderCss loaderCss;
    /**
     * Список Базовые элементы интерфейса
     * Соотношение XML тегов и графических Component
     */
    /* renamed from: ghq */
    private MapKeyValue listTagAsClass;
    private List<RenderTask> ghr = new ArrayList();
    /* renamed from: ghs */
    private JDesktopPane jDesktopPane;
    private int ght = 0;
    /**
     * Панель окна
     */
    private JRootPane rootPane = new JRootPane();

    public BaseUiTegXml() {
        if (IBaseUiTegXml.thisClass == null) {
            IBaseUiTegXml.m41083a((IBaseUiTegXml) this);
        }
        this.loaderFileXml = new LoaderFileXML();
        this.listTagAsClass = new MapKeyValue(this);
        this.listTagAsClass.add("GridBagLayout", (IBaseLayout) new GridBagLayoutCustom());
        this.listTagAsClass.add("BorderLayout", (IBaseLayout) new BorderLayoutCustom());
        this.listTagAsClass.add("GridLayout", (IBaseLayout) new GridLayoutCustom());
        this.listTagAsClass.add("FlowLayout", (IBaseLayout) new FlowLayoutCustom());
        this.listTagAsClass.add("BoxLayout", (IBaseLayout) new BoxLayoutCustom());
        this.listTagAsClass.add("CardLayout", (IBaseLayout) new CardLayoutCustom());
        this.listTagAsClass.add("OverlayLayout", (IBaseLayout) new OverlayLayoutCustom());
        this.listTagAsClass.add("fbox", (IBaseItem) new FBoxCustom());
        this.listTagAsClass.add("gbox", (IBaseItem) new GBoxCustom());
        this.listTagAsClass.add("hbox", (IBaseItem) new HBoxCustom());
        this.listTagAsClass.add("vbox", (IBaseItem) new VBoxCustom());
        this.listTagAsClass.add("panel", (IBaseItem) new PanelCustom());
        this.listTagAsClass.add("desktoppane", (IBaseItem) new DesktoppaneCustom());
        this.listTagAsClass.add("scrollable", (IBaseItem) new ScrollableCustom());
        this.listTagAsClass.add("table", (IBaseItem) new TableCustom());
        this.listTagAsClass.add("window", (IBaseItem) new WindowCustom());
        this.listTagAsClass.add("tabpane", (IBaseItem) new TabbedPaneCustom());
        this.listTagAsClass.add("list", (IBaseItem) new ListCustom());
        this.listTagAsClass.add("stack", (IBaseItem) new StackCustom());
        this.listTagAsClass.add("repeater", (IBaseItem) new RepeaterCustom());
        this.listTagAsClass.add("tree", (IBaseItem) new TreeCustom());
        this.listTagAsClass.add("split", (IBaseItem) new SplitCustom());
        this.listTagAsClass.add("splitpane", (IBaseItem) new SplitCustom());
        this.listTagAsClass.add("label", (IBaseItem) new LabelCustom());
        this.listTagAsClass.add("button", (IBaseItem) new ButtonCustom());
        this.listTagAsClass.add("toggle-button", (IBaseItem) new ToggleButtonCustom());
        this.listTagAsClass.add("textfield", (IBaseItem) new TextfieldCustom());
        this.listTagAsClass.add("editbox", (IBaseItem) new TextfieldCustom());
        this.listTagAsClass.add("checkbox", (IBaseItem) new CheckboxCustom());
        this.listTagAsClass.add("combobox", (IBaseItem) new ComboBoxCustom());
        this.listTagAsClass.add("radiobutton", (IBaseItem) new RadiobuttonCustom());
        this.listTagAsClass.add("textarea", (IBaseItem) new TextAreaCustom());
        this.listTagAsClass.add("editor", (IBaseItem) new EditorCustom());
        this.listTagAsClass.add("html", (IBaseItem) new HtmlCustom());
        this.listTagAsClass.add("toolbar", (IBaseItem) new ToolBarCustom());
        this.listTagAsClass.add("formattedfield", (IBaseItem) new FormattedFieldCustom());
        this.listTagAsClass.add("passwordfield", (IBaseItem) new PasswordfieldCustom());
        this.listTagAsClass.add("spinner", (IBaseItem) new SpinnerCustom());
        this.listTagAsClass.add("picture", (IBaseItem) new PictureCustom());
        this.listTagAsClass.add("item-icon", (IBaseItem) new ItemIconCustom());
        this.listTagAsClass.add("labeled-icon", (IBaseItem) new LabeledIconCustom());
        this.listTagAsClass.add("icon-viewer", (IBaseItem) new IconViewerCustom());
        this.listTagAsClass.add("progress", (IBaseItem) new ProgressCustom());
        this.listTagAsClass.add("slider", (IBaseItem) new SliderCustom());
        this.listTagAsClass.add("scrollbar", (IBaseItem) new ScrollBarCustom());
        this.listTagAsClass.add("component3d", (IBaseItem) new Component3dCustom());
        this.listTagAsClass.add("layeredpane", (IBaseItem) new LayeredPaneCustom());
        this.listTagAsClass.add("taskpane", (IBaseItem) new TaskPaneCustom());
        this.listTagAsClass.add("taskpane-container", (IBaseItem) new TaskPaneContainerCustom());
        this.loaderImage = new LoaderImagePng();
        this.cssNodeCore = new CssNode();
        try {
            if (!(UIManager.getLookAndFeel() instanceof BasicLookAndFeelSpaceEngine)) {
                UIManager.setLookAndFeel(new BasicLookAndFeelSpaceEngine());
            }
        } catch (UnsupportedLookAndFeelException e) {
            e.printStackTrace();
        }
    }

    public LoaderCss getLoaderCss() {
        if (this.loaderCss == null) {
            this.loaderCss = new LoaderCss();
        }
        return this.loaderCss;
    }

    /* renamed from: a */
    public void setLoaderCss(LoaderCss loaderCss) {
        this.loaderCss = loaderCss;
    }

    /* renamed from: a */
    public void mo13706a(Object obj, Component component, File file) {
        C6868avI Vj = ComponentManager.getCssHolder(component).mo13044Vj();
        if (Vj != null) {
            ((C0814Ll) Vj).mo3819b(mo13698a(obj, file));
            return;
        }
        C0814Ll ll = new C0814Ll();
        C6868avI a = mo13698a(obj, file);
        ll.mo3819b(getCssNodeCore());
        ll.mo3819b(a);
    }

    /* renamed from: a */
    public void mo13707a(Object obj, Component component, String str) {
        C6868avI Vj = ComponentManager.getCssHolder(component).mo13044Vj();
        if (Vj != null) {
            ((C0814Ll) Vj).mo3819b(loadCss(obj, str));
            return;
        }
        C0814Ll ll = new C0814Ll();
        C6868avI a = loadCss(obj, str);
        ll.mo3819b(getCssNodeCore());
        ll.mo3819b(a);
    }

    /* renamed from: e */
    public Component mo13726e(File file) {
        try {
            return createJComponent(file.toURI().toURL());
        } catch (MalformedURLException e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * Создание Component,
     *
     * @param pathFile адреса пути .xml файла
     * @return
     */
    /* renamed from: c */
    public Component createJComponent(URL pathFile) {
        return createJComponent((C6342alC) this.listTagAsClass, pathFile);
    }

    /**
     * Создание графического представления из .xml файла
     *
     * @param listTagAsClass Соотношение XML тегов и графических Component
     * @param pathFile       Пример file:/C:/.../taikodom/addon/login/login.xml
     * @return
     */
    /* renamed from: a */
    public Component createJComponent(C6342alC listTagAsClass, URL pathFile) {
        Component component = null;
        try {
            XmlNode xmlNode = this.loaderFileXml.loadFileXml(pathFile.openStream(), typeCharacter);
            if (xmlNode != null) {
                //получили класс тега
                IBaseItem baseUi = listTagAsClass.getClassBaseUi(xmlNode.getTagName().toLowerCase());//getTegName = window  toLowerCase = window
                if (baseUi != null) {
                    //Создание графического Component в баззовом класе тега, присоединение css и системы событий
                    component = baseUi.creatUi(new MapKeyValue(listTagAsClass, pathFile), this.rootPane, xmlNode);
                } else if ("import".equals(xmlNode.getTagName())) {
                    try {
                        component = createJComponent(new URL(pathFile, xmlNode.getAttribute("src")));
                    } catch (MalformedURLException e) {
                        throw new RuntimeException(e);
                    }
                } else {
                    log.error("There is no component named '" + xmlNode.getTagName() + "'");
                }
                JDesktopPane contentPane = (JDesktopPane) this.rootPane.getContentPane();
                if (component instanceof JInternalFrame) {
                    contentPane.add(component);
                    contentPane.moveToFront(component);
                } else {
                    contentPane.add(component);
                }
            }
        } catch (IOException e2) {
            log.error("Error opening XML file '" + pathFile + "'.", e2);
        }
        return component;
    }

    /**
     * Создание Component, Формирование адреса пути .xml файла в зависимости от класса
     *
     * @param objClass LoginAddon.class
     * @param fileName login.xml
     * @return
     */
    /* renamed from: d */
    public Component createJComponent(Object objClass, String fileName) {
        URL resource;
        if (objClass instanceof Class) {
            resource = ((Class) objClass).getResource(String.valueOf(((Class) objClass).getSimpleName()) + ".class");
        } else {
            resource = objClass != null ? objClass.getClass().getResource(".") : null;
        }
        try {
            return createJComponent(new URL(resource, fileName));
        } catch (MalformedURLException e) {
            throw new RuntimeException(e);
        }
    }

    /* renamed from: e */
    public Component createJComponentAndAddInRootPane(Object obj, String str) {
        Class<?> cls;
        Component component = null;
        try {
            XmlNode a = this.loaderFileXml.loadFileXml((InputStream) new ByteArrayInputStream(str.getBytes(typeCharacter)), typeCharacter);
            if (a != null) {
                IBaseItem gC = this.listTagAsClass.getClassBaseUi(a.getTagName().toLowerCase());
                if (gC == null) {
                    log.error("There is no component named '" + a.getTagName() + "'");
                } else {
                    if (obj instanceof Class) {
                        cls = (Class) obj;
                    } else {
                        cls = obj != null ? obj.getClass() : null;
                    }
                    component = gC.creatUi(new MapKeyValue((C6342alC) this.listTagAsClass, cls.getResource(".")), this.rootPane, a);
                    if (component instanceof JInternalFrame) {
                        this.rootPane.getLayeredPane().add(component);
                    } else {
                        this.rootPane.getContentPane().add(component);
                    }
                }
            }
        } catch (IOException e) {
            log.error("Error parsing XML", e);
        }
        return component;
    }

    public C6868avI getCssNodeCore() {
        return this.cssNodeCore;
    }

    public ILoaderImageInterface adz() {
        return this.loaderImage;
    }

    public JRootPane getRootPane() {
        return this.rootPane;
    }

    /**
     * Задать панель окна
     *
     * @param jRootPane javax.swing.JRootPane
     */
    public void setRootPane(JRootPane jRootPane) {
        this.rootPane = jRootPane;
        jRootPane.updateUI();
        IComponentManager e = ComponentManager.getCssHolder(this.rootPane);
        if (e != null) {
            e.mo13054a(this.cssNodeCore);
        }
    }

    /* access modifiers changed from: protected */
    public JLayeredPane setupJDesktopPaneInRootPane() {
        if (this.jDesktopPane == null || !(this.rootPane.getGlassPane() instanceof JDesktopPane)) {
            this.jDesktopPane = new JDesktopPane();
            this.jDesktopPane.setLayout((LayoutManager) null);
            this.jDesktopPane.setBounds(this.rootPane.getBounds());
            this.rootPane.setGlassPane(this.jDesktopPane);
        }
        return this.jDesktopPane;
    }

    /* renamed from: a */
    public synchronized void mo13705a(InternalFrame nxVar, boolean z) {
        JLayeredPane anw = setupJDesktopPaneInRootPane();
        if (z) {
            this.rootPane.getLayeredPane().remove(nxVar);
            int i = this.ght;
            this.ght = i + 1;
            anw.add(nxVar, new Integer(i), -1);
            nxVar.setFocusable(true);
            nxVar.setResizable(false);
            anw.setLayer(nxVar, JLayeredPane.MODAL_LAYER.intValue(), 0);
            nxVar.requestFocus();
        } else {
            if (nxVar.isVisible()) {
                this.rootPane.getLayeredPane().add(nxVar);
            }
            anw.remove(nxVar);
            this.ght--;
        }
        ComponentManager.getCssHolder(anw).mo13054a(ComponentManager.getCssHolder(nxVar).mo13044Vj());
        mo13717bE(z);
    }

    /* renamed from: bE */
    public synchronized void mo13717bE(boolean z) {
        synchronized (this) {
            JLayeredPane anw = setupJDesktopPaneInRootPane();
            IComponentManager e = ComponentManager.getCssHolder(anw);
            if (z) {
                e.setAttribute("name", "modal");
                e.setAlpha(0.0f);
                anw.repaint();
                if (this.ghv != null) {
                    this.ghv.kill();
                }
                if (this.ght == 1) {
                    this.ghv = new aDX(anw, "[0..255] dur 100", C2830kk.asS, new C1911b(e));
                    anw.addMouseMotionListener(this.ghu);
                }
                anw.setVisible(true);
                anw.repaint(50);
            } else if (this.ght <= 0) {
                for (int i = 0; i < anw.getComponentCount(); i++) {
                    anw.remove(i);
                }
                e.setAttribute("name", "");
                this.ght = 0;
                if (this.ghv != null) {
                    this.ghv.kill();
                }
                this.ghv = new aDX(anw, "[255..0] dur 500", C2830kk.asS, new C1910a(anw, e));
            }
        }
    }

    public boolean anx() {
        return this.ght > 0;
    }

    /* renamed from: a */
    public C6868avI mo13698a(Object obj, File file) {
        return getLoaderCss().mo6902a(obj, file);
    }

    /**
     * Загрузка CSS стилей
     *
     * @param obj null или  file:/C://addon/debugtools/taikodomversion/taikodomversion.xml
     * @param str res://data/styles/core.css или taikodomversion.css
     * @return
     */
    /* renamed from: a */
    public C6868avI loadCss(Object obj, String str) {
        return getLoaderCss().loadFileCSS(obj, str);
    }

    /* renamed from: aY */
    public CSSStyleDeclaration mo13710aY(String str) {
        return getLoaderCss().mo6904aY(str);
    }

    /* renamed from: a */
    public void setLoaderImage(ILoaderImageInterface azb) {
        this.loaderImage = azb;
    }

    /* renamed from: c */
    public void mo13719c(C6868avI avi) {
        this.cssNodeCore = avi;
        if (this.rootPane != null) {
            this.rootPane.updateUI();
            IComponentManager e = ComponentManager.getCssHolder(this.rootPane);
            if (e != null) {
                e.mo13054a(this.cssNodeCore);
            }
        }
    }

    /**
     * @param str Имя шрифта,
     * @param i   размер шрифта
     * @param i2  стиль шрифта
     * @return
     */
    public Font getFont(String str, int i, int i2) {
        if (this.fontFamily == null) {
            return new Font(str, i, i2);
        }
        return this.fontFamily.mo13453b(str, i, i2, false);
    }

    public ILoadFonts cnj() {
        return this.fontFamily;
    }

    /* renamed from: a */
    public void mo13703a(ILoadFonts age) {
        this.fontFamily = age;
    }

    /* renamed from: b */
    public C6868avI mo13716b(URL url) {
        try {
            return getLoaderCss().loadCSS(url);
        } catch (IOException e) {
            throw new RuntimeException("Error loading css: " + url, e);
        }
    }

    public C6342alC cnk() {
        return this.listTagAsClass;
    }

    public MapKeyValue cnl() {
        return this.listTagAsClass;
    }

    /* renamed from: a */
    public void mo13709a(RenderTask renderTask) {
        synchronized (this.ghr) {
            if (!this.ghr.contains(renderTask)) {
                this.ghr.add(renderTask);
            }
        }
    }

    /* renamed from: a */
    public void mo13708a(RenderView renderView) {
        ArrayList<RenderTask> arrayList;
        synchronized (this.ghr) {
            arrayList = new ArrayList<>(this.ghr);
            this.ghr.clear();
        }
        for (RenderTask run : arrayList) {
            run.run(renderView);
        }
    }

    /* renamed from: cx */
    public SoundObject mo13724cx(String str) {
        ISoundPlayer any = any();
        if (any == null || "none".equals(str)) {
            return null;
        }
        return any.mo2915cx(str);
    }

    /* renamed from: e */
    public SoundObject mo13728e(String str, boolean z) {
        ISoundPlayer any = any();
        if (any == null || "none".equals(str)) {
            return null;
        }
        return any.mo2916e(str, z);
    }

    public ISoundPlayer any() {
        return this.soundPlayer;
    }

    /* renamed from: a */
    public void mo13701a(ISoundPlayer ma) {
        this.soundPlayer = ma;
    }

    /* renamed from: a.aiW$b */
    /* compiled from: a */
    class C1911b implements C0454GJ {
        private final /* synthetic */ IComponentManager fOS;

        C1911b(IComponentManager aek) {
            this.fOS = aek;
        }

        /* renamed from: a */
        public void mo9a(JComponent jComponent) {
            this.fOS.setAlpha(1.0f);
            BaseUiTegXml.this.ghv = null;
        }
    }

    /* renamed from: a.aiW$a */
    class C1910a implements C0454GJ {
        private final /* synthetic */ JLayeredPane fOR;
        private final /* synthetic */ IComponentManager fOS;

        C1910a(JLayeredPane jLayeredPane, IComponentManager aek) {
            this.fOR = jLayeredPane;
            this.fOS = aek;
        }

        /* renamed from: a */
        public void mo9a(JComponent jComponent) {
            this.fOR.setVisible(false);
            this.fOS.setAttribute("class", "");
            this.fOR.removeMouseMotionListener(BaseUiTegXml.this.ghu);
            BaseUiTegXml.this.ghv = null;
        }
    }
}
