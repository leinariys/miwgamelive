package logic.ui;

import logic.ui.item.IBaseItem;
import logic.ui.layout.IBaseLayout;

import java.net.URL;

/* renamed from: a.alC  reason: case insensitive filesystem */
/* compiled from: a */
public interface C6342alC {
    IBaseUiTegXml getBaseUiTegXml();

    URL getPathFile();

    /* renamed from: gC */
    IBaseItem getClassBaseUi(String str);

    /* renamed from: gD */
    IBaseLayout getClassLayoutUi(String str);
}
