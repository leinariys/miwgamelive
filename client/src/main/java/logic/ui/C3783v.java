package logic.ui;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.lang.ref.Reference;
import java.lang.ref.ReferenceQueue;
import java.lang.ref.SoftReference;
import java.lang.ref.WeakReference;
import java.util.*;
import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.locks.ReentrantLock;

/* renamed from: a.v */
/* compiled from: a */
public class C3783v<K, V> extends AbstractMap<K, V> implements ConcurrentMap<K, V>, Serializable {
    static final int DEFAULT_INITIAL_CAPACITY = 16;
    static final float DEFAULT_LOAD_FACTOR = 0.75f;
    static final int MAXIMUM_CAPACITY = 1073741824;
    static final C3792i cIk = C3792i.WEAK;
    static final C3792i cIl = C3792i.STRONG;
    static final int cIm = 16;
    static final int cIn = 65536;
    static final int cIo = 2;
    private static final long serialVersionUID = 7249069246763182397L;
    final int cIp;
    final int cIq;
    final C3793j<K, V>[] cIr;
    boolean cIs;
    transient Set<Map.Entry<K, V>> entrySet;
    transient Set<K> keySet;
    transient Collection<V> values;

    public C3783v(int i, float f, int i2, C3792i iVar, C3792i iVar2, EnumSet<C3791h> enumSet) {
        boolean z;
        int i3 = 0;
        if (f <= 0.0f || i < 0 || i2 <= 0) {
            throw new IllegalArgumentException();
        }
        int i4 = 1;
        int i5 = 0;
        while (i4 < (i2 > 65536 ? 65536 : i2)) {
            i5++;
            i4 <<= 1;
        }
        this.cIq = 32 - i5;
        this.cIp = i4 - 1;
        this.cIr = C3793j.m40136no(i4);
        i = i > 1073741824 ? 1073741824 : i;
        int i6 = i / i4;
        int i7 = 1;
        while (i7 < (i6 * i4 < i ? i6 + 1 : i6)) {
            i7 <<= 1;
        }
        if (enumSet == null || !enumSet.contains(C3791h.IDENTITY_COMPARISONS)) {
            z = false;
        } else {
            z = true;
        }
        this.cIs = z;
        while (true) {
            int i8 = i3;
            if (i8 < this.cIr.length) {
                this.cIr[i8] = new C3793j<>(i7, f, iVar, iVar2, this.cIs);
                i3 = i8 + 1;
            } else {
                return;
            }
        }
    }

    public C3783v(int i, float f, int i2) {
        this(i, f, i2, cIk, cIl, (EnumSet<C3791h>) null);
    }

    public C3783v(int i, float f) {
        this(i, f, 16);
    }

    public C3783v(int i, C3792i iVar, C3792i iVar2) {
        this(i, DEFAULT_LOAD_FACTOR, 16, iVar, iVar2, (EnumSet<C3791h>) null);
    }

    public C3783v(int i) {
        this(i, (float) DEFAULT_LOAD_FACTOR, 16);
    }

    public C3783v() {
        this(16, (float) DEFAULT_LOAD_FACTOR, 16);
    }

    public C3783v(Map<? extends K, ? extends V> map) {
        this(Math.max(((int) (((float) map.size()) / DEFAULT_LOAD_FACTOR)) + 1, 16), (float) DEFAULT_LOAD_FACTOR, 16);
        putAll(map);
    }

    private static int hash(int i) {
        int i2 = ((i << 15) ^ -12931) + i;
        int i3 = i2 ^ (i2 >>> 10);
        int i4 = i3 + (i3 << 3);
        int i5 = i4 ^ (i4 >>> 6);
        int i6 = i5 + (i5 << 2) + (i5 << 14);
        return i6 ^ (i6 >>> 16);
    }

    /* access modifiers changed from: package-private */
    /* renamed from: gO */
    public final C3793j<K, V> mo22483gO(int i) {
        return this.cIr[(i >>> this.cIq) & this.cIp];
    }

    /* renamed from: M */
    private int m40128M(Object obj) {
        return hash(this.cIs ? System.identityHashCode(obj) : obj.hashCode());
    }

    public boolean isEmpty() {
        C3793j<K, V>[] jVarArr = this.cIr;
        int[] iArr = new int[jVarArr.length];
        int i = 0;
        for (int i2 = 0; i2 < jVarArr.length; i2++) {
            if (jVarArr[i2].count != 0) {
                return false;
            }
            int i3 = jVarArr[i2].modCount;
            iArr[i2] = i3;
            i += i3;
        }
        if (i != 0) {
            for (int i4 = 0; i4 < jVarArr.length; i4++) {
                if (jVarArr[i4].count != 0 || iArr[i4] != jVarArr[i4].modCount) {
                    return false;
                }
            }
        }
        return true;
    }

    public int size() {
        long j = 0;
        C3793j<K, V>[] jVarArr = this.cIr;
        int[] iArr = new int[jVarArr.length];
        int i = 0;
        long j2 = 0;
        long j3 = 0;
        while (true) {
            if (i >= 2) {
                break;
            }
            int i2 = 0;
            long j4 = 0;
            for (int i3 = 0; i3 < jVarArr.length; i3++) {
                j4 += (long) jVarArr[i3].count;
                int i4 = jVarArr[i3].modCount;
                iArr[i3] = i4;
                i2 += i4;
            }
            if (i2 != 0) {
                int i5 = 0;
                long j5 = 0;
                while (true) {
                    if (i5 >= jVarArr.length) {
                        j2 = j5;
                        break;
                    }
                    j5 += (long) jVarArr[i5].count;
                    if (iArr[i5] != jVarArr[i5].modCount) {
                        j2 = -1;
                        break;
                    }
                    i5++;
                }
            } else {
                j2 = 0;
            }
            if (j2 == j4) {
                j3 = j4;
                break;
            }
            i++;
            j3 = j4;
        }
        if (j2 != j3) {
            for (C3793j<K, V> lock : jVarArr) {
                lock.lock();
            }
            for (C3793j<K, V> jVar : jVarArr) {
                j += (long) jVar.count;
            }
            for (C3793j<K, V> unlock : jVarArr) {
                unlock.unlock();
            }
        } else {
            j = j3;
        }
        if (j > 2147483647L) {
            return Integer.MAX_VALUE;
        }
        return (int) j;
    }

    public V get(Object obj) {
        int M = m40128M(obj);
        return mo22483gO(M).get(obj, M);
    }

    public boolean containsKey(Object obj) {
        int M = m40128M(obj);
        return mo22483gO(M).mo22520a(obj, M);
    }

    public boolean containsValue(Object obj) {
        boolean z;
        boolean z2;
        int i = 0;
        if (obj == null) {
            throw new NullPointerException();
        }
        C3793j<K, V>[] jVarArr = this.cIr;
        int[] iArr = new int[jVarArr.length];
        for (int i2 = 0; i2 < 2; i2++) {
            int i3 = 0;
            for (int i4 = 0; i4 < jVarArr.length; i4++) {
                int i5 = jVarArr[i4].count;
                int i6 = jVarArr[i4].modCount;
                iArr[i4] = i6;
                i3 += i6;
                if (jVarArr[i4].containsValue(obj)) {
                    return true;
                }
            }
            if (i3 != 0) {
                int i7 = 0;
                while (true) {
                    if (i7 >= jVarArr.length) {
                        break;
                    }
                    int i8 = jVarArr[i7].count;
                    if (iArr[i7] != jVarArr[i7].modCount) {
                        z2 = false;
                        break;
                    }
                    i7++;
                }
            }
            z2 = true;
            if (z2) {
                return false;
            }
        }
        for (C3793j<K, V> lock : jVarArr) {
            lock.lock();
        }
        int i9 = 0;
        while (true) {
            try {
                if (i9 >= jVarArr.length) {
                    z = false;
                    break;
                } else if (jVarArr[i9].containsValue(obj)) {
                    z = true;
                    break;
                } else {
                    i9++;
                }
            } finally {
                while (i < jVarArr.length) {
                    jVarArr[i].unlock();
                    i++;
                }
            }
        }
        while (i < jVarArr.length) {
            jVarArr[i].unlock();
            i++;
        }
        return z;
    }

    public boolean contains(Object obj) {
        return containsValue(obj);
    }

    public V put(K k, V v) {
        if (v == null) {
            throw new NullPointerException();
        }
        int M = m40128M(k);
        return mo22483gO(M).mo22518a(k, M, v, false);
    }

    public V putIfAbsent(K k, V v) {
        if (v == null) {
            throw new NullPointerException();
        }
        int M = m40128M(k);
        return mo22483gO(M).mo22518a(k, M, v, true);
    }

    public void putAll(Map<? extends K, ? extends V> map) {
        for (Map.Entry next : map.entrySet()) {
            put((K) next.getKey(), (V) next.getValue());
        }
    }

    public V remove(Object obj) {
        int M = m40128M(obj);
        return mo22483gO(M).mo22522b(obj, M, (Object) null, false);
    }

    public boolean remove(Object obj, Object obj2) {
        int M = m40128M(obj);
        if (obj2 == null || mo22483gO(M).mo22522b(obj, M, obj2, false) == null) {
            return false;
        }
        return true;
    }

    public boolean replace(K k, V v, V v2) {
        if (v == null || v2 == null) {
            throw new NullPointerException();
        }
        int M = m40128M(k);
        return mo22483gO(M).mo22521a(k, M, v, v2);
    }

    public V replace(K k, V v) {
        if (v == null) {
            throw new NullPointerException();
        }
        int M = m40128M(k);
        return mo22483gO(M).mo22517a(k, M, v);
    }

    public void clear() {
        for (C3793j<K, V> clear : this.cIr) {
            clear.clear();
        }
    }

    public void aHa() {
        for (C3793j<K, V> buA : this.cIr) {
            buA.buA();
        }
    }

    public Set<K> keySet() {
        Set<K> set = this.keySet;
        if (set != null) {
            return set;
        }
        C3794k kVar = new C3794k();
        this.keySet = kVar;
        return kVar;
    }

    public Collection<V> values() {
        Collection<V> collection = this.values;
        if (collection != null) {
            return collection;
        }
        C3801r rVar = new C3801r();
        this.values = rVar;
        return rVar;
    }

    public Set<Map.Entry<K, V>> entrySet() {
        Set<Map.Entry<K, V>> set = this.entrySet;
        if (set != null) {
            return set;
        }
        C3785b bVar = new C3785b();
        this.entrySet = bVar;
        return bVar;
    }

    public Enumeration<K> keys() {
        return new C3788e();
    }

    public Enumeration<V> elements() {
        return new C3784a();
    }

    /* JADX INFO: finally extract failed */
    private void writeObject(ObjectOutputStream objectOutputStream) throws IOException {
        objectOutputStream.defaultWriteObject();
        int i = 0;
        while (i < this.cIr.length) {
            C3793j<K, V> jVar = this.cIr[i];
            jVar.lock();
            try {
                C3799p<K, V>[] pVarArr = jVar.efF;
                for (C3799p<K, V> pVar : pVarArr) {
                    while (pVar != null) {
                        K key = pVar.key();
                        if (key != null) {
                            objectOutputStream.writeObject(key);
                            objectOutputStream.writeObject(pVar.value());
                        }
                        pVar = pVar.hWH;
                    }
                }
                jVar.unlock();
                i++;
            } catch (Throwable th) {
                jVar.unlock();
                throw th;
            }
        }
        objectOutputStream.writeObject((Object) null);
        objectOutputStream.writeObject((Object) null);
    }

    private void readObject(ObjectInputStream objectInputStream) throws IOException, ClassNotFoundException {
        objectInputStream.defaultReadObject();
        for (C3793j<K, V> a : this.cIr) {
            a.mo22519a((C3799p<K, V>[]) new C3799p[1]);
        }
        while (true) {
            Object readObject = objectInputStream.readObject();
            Object readObject2 = objectInputStream.readObject();
            if (readObject != null) {
                put((K) readObject, (V) readObject2);
            } else {
                return;
            }
        }
    }

    /* renamed from: a.v$h */
    /* compiled from: a */
    public enum C3791h {
        IDENTITY_COMPARISONS
    }

    /* renamed from: a.v$i */
    /* compiled from: a */
    public enum C3792i {
        STRONG,
        WEAK,
        SOFT
    }

    /* renamed from: a.v$f */
    /* compiled from: a */
    interface C3789f {
        /* renamed from: Bi */
        int mo22505Bi();

        /* renamed from: Bj */
        Object mo22506Bj();
    }

    /* renamed from: a.v$n */
    /* compiled from: a */
    static final class C3797n<K> extends WeakReference<K> implements C3789f {
        final int hash;

        C3797n(K k, int i, ReferenceQueue<Object> referenceQueue) {
            super(k, referenceQueue);
            this.hash = i;
        }

        /* renamed from: Bi */
        public final int mo22505Bi() {
            return this.hash;
        }

        /* renamed from: Bj */
        public final Object mo22506Bj() {
            return this;
        }
    }

    /* renamed from: a.v$l */
    /* compiled from: a */
    static final class C3795l<K> extends SoftReference<K> implements C3789f {
        final int hash;

        C3795l(K k, int i, ReferenceQueue<Object> referenceQueue) {
            super(k, referenceQueue);
            this.hash = i;
        }

        /* renamed from: Bi */
        public final int mo22505Bi() {
            return this.hash;
        }

        /* renamed from: Bj */
        public final Object mo22506Bj() {
            return this;
        }
    }

    /* renamed from: a.v$o */
    /* compiled from: a */
    static final class C3798o<V> extends WeakReference<V> implements C3789f {

        /* renamed from: Ya */
        final Object f9390Ya;
        final int hash;

        C3798o(V v, Object obj, int i, ReferenceQueue<Object> referenceQueue) {
            super(v, referenceQueue);
            this.f9390Ya = obj;
            this.hash = i;
        }

        /* renamed from: Bi */
        public final int mo22505Bi() {
            return this.hash;
        }

        /* renamed from: Bj */
        public final Object mo22506Bj() {
            return this.f9390Ya;
        }
    }

    /* renamed from: a.v$c */
    /* compiled from: a */
    static final class C3786c<V> extends SoftReference<V> implements C3789f {

        /* renamed from: Ya */
        final Object f9385Ya;
        final int hash;

        C3786c(V v, Object obj, int i, ReferenceQueue<Object> referenceQueue) {
            super(v, referenceQueue);
            this.f9385Ya = obj;
            this.hash = i;
        }

        /* renamed from: Bi */
        public final int mo22505Bi() {
            return this.hash;
        }

        /* renamed from: Bj */
        public final Object mo22506Bj() {
            return this.f9385Ya;
        }
    }

    /* renamed from: a.v$p */
    /* compiled from: a */
    static final class C3799p<K, V> {

        /* renamed from: Ya */
        final Object f9391Ya;
        final C3799p<K, V> hWH;
        final int hash;
        volatile Object hWG;

        C3799p(K k, int i, C3799p<K, V> pVar, V v, C3792i iVar, C3792i iVar2, ReferenceQueue<Object> referenceQueue) {
            this.hash = i;
            this.hWH = pVar;
            this.f9391Ya = mo22540a(k, iVar, referenceQueue);
            this.hWG = mo22542b(v, iVar2, referenceQueue);
        }

        /* renamed from: yf */
        static final <K, V> C3799p<K, V>[] m40152yf(int i) {
            return new C3799p[i];
        }

        /* access modifiers changed from: package-private */
        /* renamed from: a */
        public final Object mo22540a(K k, C3792i iVar, ReferenceQueue<Object> referenceQueue) {
            if (iVar == C3792i.WEAK) {
                return new C3797n(k, this.hash, referenceQueue);
            }
            if (iVar == C3792i.SOFT) {
                return new C3795l(k, this.hash, referenceQueue);
            }
            return k;
        }

        /* access modifiers changed from: package-private */
        /* renamed from: b */
        public final Object mo22542b(V v, C3792i iVar, ReferenceQueue<Object> referenceQueue) {
            if (iVar == C3792i.WEAK) {
                return new C3798o(v, this.f9391Ya, this.hash, referenceQueue);
            }
            if (iVar == C3792i.SOFT) {
                return new C3786c(v, this.f9391Ya, this.hash, referenceQueue);
            }
            return v;
        }

        /* access modifiers changed from: package-private */
        public final K key() {
            if (this.f9391Ya instanceof Reference) {
                return (K) ((Reference) this.f9391Ya).get();
            }
            return (K) this.f9391Ya;
        }

        /* access modifiers changed from: package-private */
        public final V value() {
            return mo22541aH(this.hWG);
        }

        /* access modifiers changed from: package-private */
        /* renamed from: aH */
        public final V mo22541aH(Object obj) {
            if (obj instanceof Reference) {
                return (V) ((Reference) obj).get();
            }
            return (V) obj;
        }

        /* access modifiers changed from: package-private */
        /* renamed from: c */
        public final void mo22543c(V v, C3792i iVar, ReferenceQueue<Object> referenceQueue) {
            this.hWG = mo22542b(v, iVar, referenceQueue);
        }
    }

    /* renamed from: a.v$j */
    /* compiled from: a */
    static final class C3793j<K, V> extends ReentrantLock implements Serializable {
        private static final long serialVersionUID = 2249069246763182397L;
        final boolean cIs;
        final C3792i efH;
        final C3792i efI;
        final float loadFactor;
        volatile transient int count;
        volatile transient C3799p<K, V>[] efF;
        volatile transient ReferenceQueue<Object> efG;
        transient int modCount;
        transient int threshold;

        C3793j(int i, float f, C3792i iVar, C3792i iVar2, boolean z) {
            this.loadFactor = f;
            this.efH = iVar;
            this.efI = iVar2;
            this.cIs = z;
            mo22519a((C3799p<K, V>[]) C3799p.m40152yf(i));
        }

        /* renamed from: no */
        static final <K, V> C3793j<K, V>[] m40136no(int i) {
            return new C3793j[i];
        }

        /* renamed from: f */
        private boolean m40135f(Object obj, Object obj2) {
            if (this.cIs) {
                return obj == obj2;
            }
            return obj.equals(obj2);
        }

        /* access modifiers changed from: package-private */
        /* renamed from: a */
        public void mo22519a(C3799p<K, V>[] pVarArr) {
            this.threshold = (int) (((float) pVarArr.length) * this.loadFactor);
            this.efF = pVarArr;
            this.efG = new ReferenceQueue<>();
        }

        /* access modifiers changed from: package-private */
        /* renamed from: np */
        public C3799p<K, V> mo22528np(int i) {
            C3799p<K, V>[] pVarArr = this.efF;
            return pVarArr[(pVarArr.length - 1) & i];
        }

        /* access modifiers changed from: package-private */
        /* renamed from: a */
        public C3799p<K, V> mo22515a(K k, int i, C3799p<K, V> pVar, V v) {
            return new C3799p<>(k, i, pVar, v, this.efH, this.efI, this.efG);
        }

        /* access modifiers changed from: package-private */
        /* renamed from: a */
        public V mo22516a(C3799p<K, V> pVar) {
            lock();
            try {
                buA();
                return pVar.value();
            } finally {
                unlock();
            }
        }

        /* access modifiers changed from: package-private */
        public V get(Object obj, int i) {
            if (this.count != 0) {
                C3799p<K, V> np = mo22528np(i);
                while (np != null) {
                    if (np.hash != i || !m40135f(obj, np.key())) {
                        np = np.hWH;
                    } else {
                        Object obj2 = np.hWG;
                        if (obj2 != null) {
                            return np.mo22541aH(obj2);
                        }
                        return mo22516a(np);
                    }
                }
            }
            return null;
        }

        /* access modifiers changed from: package-private */
        /* renamed from: a */
        public boolean mo22520a(Object obj, int i) {
            if (this.count != 0) {
                for (C3799p<K, V> np = mo22528np(i); np != null; np = np.hWH) {
                    if (np.hash == i && m40135f(obj, np.key())) {
                        return true;
                    }
                }
            }
            return false;
        }

        /* access modifiers changed from: package-private */
        public boolean containsValue(Object obj) {
            V aH;
            if (this.count != 0) {
                for (C3799p<K, V> pVar : this.efF) {
                    while (pVar != null) {
                        Object obj2 = pVar.hWG;
                        if (obj2 == null) {
                            aH = mo22516a(pVar);
                        } else {
                            aH = pVar.mo22541aH(obj2);
                        }
                        if (obj.equals(aH)) {
                            return true;
                        }
                        pVar = pVar.hWH;
                    }
                }
            }
            return false;
        }

        /* access modifiers changed from: package-private */
        /* renamed from: a */
        public boolean mo22521a(K k, int i, V v, V v2) {
            lock();
            try {
                buA();
                C3799p<K, V> np = mo22528np(i);
                while (np != null && (np.hash != i || !m40135f(k, np.key()))) {
                    np = np.hWH;
                }
                boolean z = false;
                if (np != null && v.equals(np.value())) {
                    z = true;
                    np.mo22543c(v2, this.efI, this.efG);
                }
                return z;
            } finally {
                unlock();
            }
        }

        /* access modifiers changed from: package-private */
        /* renamed from: a */
        public V mo22517a(K k, int i, V v) {
            lock();
            try {
                buA();
                C3799p<K, V> np = mo22528np(i);
                while (np != null && (np.hash != i || !m40135f(k, np.key()))) {
                    np = np.hWH;
                }
                V v2 = null;
                if (np != null) {
                    v2 = np.value();
                    np.mo22543c(v, this.efI, this.efG);
                }
                return v2;
            } finally {
                unlock();
            }
        }

        /* access modifiers changed from: package-private */
        /* renamed from: a */
        public V mo22518a(K k, int i, V v, boolean z) {
            V v2;
            int buz;
            lock();
            try {
                buA();
                int i2 = this.count;
                int i3 = i2 + 1;
                if (i2 > this.threshold && (buz = buz()) > 0) {
                    i3 -= buz;
                    this.count = i3 - 1;
                }
                int i4 = i3;
                C3799p<K, V>[] pVarArr = this.efF;
                int length = i & (pVarArr.length - 1);
                C3799p<K, V> pVar = pVarArr[length];
                C3799p<K, V> pVar2 = pVar;
                while (pVar2 != null && (pVar2.hash != i || !m40135f(k, pVar2.key()))) {
                    pVar2 = pVar2.hWH;
                }
                if (pVar2 != null) {
                    v2 = pVar2.value();
                    if (!z) {
                        pVar2.mo22543c(v, this.efI, this.efG);
                    }
                } else {
                    v2 = null;
                    this.modCount++;
                    pVarArr[length] = mo22515a(k, i, pVar, v);
                    this.count = i4;
                }
                return v2;
            } finally {
                unlock();
            }
        }

        /* access modifiers changed from: package-private */
        public int buz() {
            int i;
            C3799p<K, V>[] pVarArr = this.efF;
            int length = pVarArr.length;
            if (length >= 1073741824) {
                return 0;
            }
            C3799p<K, V>[] yf = C3799p.m40152yf(length << 1);
            this.threshold = (int) (((float) yf.length) * this.loadFactor);
            int length2 = yf.length - 1;
            int i2 = 0;
            int i3 = 0;
            while (i2 < length) {
                C3799p<K, V> pVar = pVarArr[i2];
                if (pVar != null) {
                    C3799p<K, V> pVar2 = pVar.hWH;
                    int i4 = pVar.hash & length2;
                    if (pVar2 == null) {
                        yf[i4] = pVar;
                        i = i3;
                    } else {
                        C3799p<K, V> pVar3 = pVar;
                        while (pVar2 != null) {
                            int i5 = pVar2.hash & length2;
                            if (i5 != i4) {
                                pVar3 = pVar2;
                            } else {
                                i5 = i4;
                            }
                            pVar2 = pVar2.hWH;
                            i4 = i5;
                        }
                        yf[i4] = pVar3;
                        i = i3;
                        for (C3799p<K, V> pVar4 = pVar; pVar4 != pVar3; pVar4 = pVar4.hWH) {
                            K key = pVar4.key();
                            if (key == null) {
                                i++;
                            } else {
                                int i6 = pVar4.hash & length2;
                                yf[i6] = mo22515a(key, pVar4.hash, yf[i6], pVar4.value());
                            }
                        }
                    }
                } else {
                    i = i3;
                }
                i2++;
                i3 = i;
            }
            this.efF = yf;
            return i3;
        }

        /* access modifiers changed from: package-private */
        /* renamed from: b */
        public V mo22522b(Object obj, int i, Object obj2, boolean z) {
            lock();
            if (!z) {
                try {
                    buA();
                } catch (Throwable th) {
                    unlock();
                    throw th;
                }
            }
            int i2 = this.count - 1;
            C3799p<K, V>[] pVarArr = this.efF;
            int length = i & (pVarArr.length - 1);
            C3799p<K, V> pVar = pVarArr[length];
            C3799p<K, V> pVar2 = pVar;
            while (pVar2 != null && obj != pVar2.f9391Ya && (z || i != pVar2.hash || !m40135f(obj, pVar2.key()))) {
                pVar2 = pVar2.hWH;
            }
            V v = null;
            if (pVar2 != null) {
                V value = pVar2.value();
                if (obj2 == null || obj2.equals(value)) {
                    this.modCount++;
                    C3799p<K, V> pVar3 = pVar2.hWH;
                    while (pVar != pVar2) {
                        K key = pVar.key();
                        if (key == null) {
                            i2--;
                        } else {
                            pVar3 = mo22515a(key, pVar.hash, pVar3, pVar.value());
                        }
                        pVar = pVar.hWH;
                    }
                    pVarArr[length] = pVar3;
                    this.count = i2;
                    v = value;
                }
            }
            unlock();
            return v;
        }

        /* access modifiers changed from: package-private */
        public final void buA() {
            while (true) {
                C3789f fVar = (C3789f) this.efG.poll();
                if (fVar != null) {
                    mo22522b(fVar.mo22506Bj(), fVar.mo22505Bi(), (Object) null, true);
                } else {
                    return;
                }
            }
        }

        /* access modifiers changed from: package-private */
        public void clear() {
            if (this.count != 0) {
                lock();
                try {
                    C3799p<K, V>[] pVarArr = this.efF;
                    for (int i = 0; i < pVarArr.length; i++) {
                        pVarArr[i] = null;
                    }
                    this.modCount++;
                    this.efG = new ReferenceQueue<>();
                    this.count = 0;
                } finally {
                    unlock();
                }
            }
        }
    }

    /* renamed from: a.v$d */
    /* compiled from: a */
    static class C3787d<K, V> implements Map.Entry<K, V>, Serializable {
        private static final long serialVersionUID = -8499721149061103585L;
        private final K key;
        private V value;

        public C3787d(K k, V v) {
            this.key = k;
            this.value = v;
        }

        public C3787d(Map.Entry<? extends K, ? extends V> entry) {
            this.key = entry.getKey();
            this.value = entry.getValue();
        }

        /* renamed from: eq */
        private static boolean m40132eq(Object obj, Object obj2) {
            if (obj == null) {
                return obj2 == null;
            }
            return obj.equals(obj2);
        }

        public K getKey() {
            return this.key;
        }

        public V getValue() {
            return this.value;
        }

        public V setValue(V v) {
            V v2 = this.value;
            this.value = v;
            return v2;
        }

        public boolean equals(Object obj) {
            if (!(obj instanceof Map.Entry)) {
                return false;
            }
            Map.Entry entry = (Map.Entry) obj;
            if (!m40132eq(this.key, entry.getKey()) || !m40132eq(this.value, entry.getValue())) {
                return false;
            }
            return true;
        }

        public int hashCode() {
            int i = 0;
            int hashCode = this.key == null ? 0 : this.key.hashCode();
            if (this.value != null) {
                i = this.value.hashCode();
            }
            return hashCode ^ i;
        }

        public String toString() {
            return this.key + "=" + this.value;
        }
    }

    /* renamed from: a.v$m */
    /* compiled from: a */
    abstract class C3796m<K, V> {
        K currentKey;
        int hfQ;
        int hfR = -1;
        C3799p<K, V>[] hfS;
        C3799p<K, V> hfT;
        C3799p<K, V> hfU;

        C3796m() {
            this.hfQ = C3783v.this.cIr.length - 1;
            advance();
        }

        public boolean hasMoreElements() {
            return hasNext();
        }

        /* access modifiers changed from: package-private */
        public final void advance() {
            if (this.hfT != null) {
                C3799p<K, V> pVar = this.hfT.hWH;
                this.hfT = pVar;
                if (pVar != null) {
                    return;
                }
            }
            while (this.hfR >= 0) {
                C3799p<K, V>[] pVarArr = this.hfS;
                int i = this.hfR;
                this.hfR = i - 1;
                C3799p<K, V> pVar2 = pVarArr[i];
                this.hfT = pVar2;
                if (pVar2 != null) {
                    return;
                }
            }
            while (this.hfQ >= 0) {
                C3793j<K, V>[] jVarArr = (C3793j<K, V>[]) C3783v.this.cIr;
                int i2 = this.hfQ;
                this.hfQ = i2 - 1;
                C3793j<K, V> jVar = jVarArr[i2];
                if (jVar.count != 0) {
                    this.hfS = jVar.efF;
                    for (int length = this.hfS.length - 1; length >= 0; length--) {
                        C3799p<K, V> pVar3 = this.hfS[length];
                        this.hfT = pVar3;
                        if (pVar3 != null) {
                            this.hfR = length - 1;
                            return;
                        }
                    }
                    continue;
                }
            }
        }

        public boolean hasNext() {
            while (this.hfT != null) {
                if (this.hfT.key() != null) {
                    return true;
                }
                advance();
            }
            return false;
        }

        /* access modifiers changed from: package-private */
        public C3799p<K, V> cIT() {
            while (this.hfT != null) {
                this.hfU = this.hfT;
                this.currentKey = this.hfU.key();
                advance();
                if (this.currentKey != null) {
                    return this.hfU;
                }
            }
            throw new NoSuchElementException();
        }

        public void remove() {
            if (this.hfU == null) {
                throw new IllegalStateException();
            }
            C3783v.this.remove(this.currentKey);
            this.hfU = null;
        }
    }

    /* renamed from: a.v$e */
    /* compiled from: a */
    final class C3788e extends C3796m<K, V> implements Iterator<K>, Enumeration<K> {
        C3788e() {
            super();
        }

        /* JADX WARNING: type inference failed for: r1v0, types: [a.v$e, a.v$m] */
        public K next() {
            return super.cIT().key();
        }

        /* JADX WARNING: type inference failed for: r1v0, types: [a.v$e, a.v$m] */
        public K nextElement() {
            return super.cIT().key();
        }
    }

    /* renamed from: a.v$a */
    final class C3784a extends C3796m<K, V> implements Iterator<V>, Enumeration<V> {
        C3784a() {
            super();
        }

        /* JADX WARNING: type inference failed for: r1v0, types: [a.v$a, a.v$m] */
        public V next() {
            return super.cIT().value();
        }

        /* JADX WARNING: type inference failed for: r1v0, types: [a.v$a, a.v$m] */
        public V nextElement() {
            return super.cIT().value();
        }
    }

    /* renamed from: a.v$g */
    /* compiled from: a */
    final class C3790g extends C3787d<K, V> {
        private static final long serialVersionUID = -7900634345345313646L;

        C3790g(K k, V v) {
            super(k, v);
        }

        public V setValue(V v) {
            if (v == null) {
                throw new NullPointerException();
            }
            V value = super.setValue(v);
            C3783v.this.put(getKey(), v);
            return value;
        }
    }

    /* renamed from: a.v$q */
    /* compiled from: a */
    final class C3800q extends C3796m<K, V> implements Iterator<Map.Entry<K, V>> {
        C3800q() {
            super();
        }

        /* JADX WARNING: type inference failed for: r4v0, types: [a.v$q, a.v$m] */
        public Map.Entry<K, V> next() {
            C3799p cIT = super.cIT();
            return new C3790g((K) cIT.key(), (V) cIT.value());
        }
    }

    /* renamed from: a.v$k */
    /* compiled from: a */
    final class C3794k extends AbstractSet<K> {
        C3794k() {
        }

        public Iterator<K> iterator() {
            return new C3788e();
        }

        public int size() {
            return C3783v.this.size();
        }

        public boolean isEmpty() {
            return C3783v.this.isEmpty();
        }

        public boolean contains(Object obj) {
            return C3783v.this.containsKey(obj);
        }

        public boolean remove(Object obj) {
            return C3783v.this.remove(obj) != null;
        }

        public void clear() {
            C3783v.this.clear();
        }
    }

    /* renamed from: a.v$r */
    /* compiled from: a */
    final class C3801r extends AbstractCollection<V> {
        C3801r() {
        }

        public Iterator<V> iterator() {
            return new C3784a();
        }

        public int size() {
            return C3783v.this.size();
        }

        public boolean isEmpty() {
            return C3783v.this.isEmpty();
        }

        public boolean contains(Object obj) {
            return C3783v.this.containsValue(obj);
        }

        public void clear() {
            C3783v.this.clear();
        }
    }

    /* renamed from: a.v$b */
    /* compiled from: a */
    final class C3785b extends AbstractSet<Map.Entry<K, V>> {
        C3785b() {
        }

        public Iterator<Map.Entry<K, V>> iterator() {
            return new C3800q();
        }

        public boolean contains(Object obj) {
            if (!(obj instanceof Map.Entry)) {
                return false;
            }
            Map.Entry entry = (Map.Entry) obj;
            Object obj2 = C3783v.this.get(entry.getKey());
            if (obj2 == null || !obj2.equals(entry.getValue())) {
                return false;
            }
            return true;
        }

        public boolean remove(Object obj) {
            if (!(obj instanceof Map.Entry)) {
                return false;
            }
            Map.Entry entry = (Map.Entry) obj;
            return C3783v.this.remove(entry.getKey(), entry.getValue());
        }

        public int size() {
            return C3783v.this.size();
        }

        public boolean isEmpty() {
            return C3783v.this.isEmpty();
        }

        public void clear() {
            C3783v.this.clear();
        }
    }
}
