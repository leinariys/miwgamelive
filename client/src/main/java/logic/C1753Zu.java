package logic;

import java.util.Collection;

/* renamed from: a.Zu */
/* compiled from: a */
public abstract class C1753Zu {
    public static final String eNB = "";
    public aMQ<C1754a, aVH<?>> eNC = new aMQ<>();

    /* renamed from: a */
    public <T> void mo7499a(String str, Class<T> cls, aVH<T> avh) {
        if (avh == null) {
            throw new NullPointerException("Listener can not be null");
        }
        this.eNC.put(new C1754a(str, cls), avh);
    }

    /* renamed from: a */
    public <T> void mo7498a(Class<T> cls, aVH<T> avh) {
        mo7499a(eNB, cls, avh);
    }

    /* renamed from: a */
    public <T> void mo7497a(aVH<T> avh) {
        this.eNC.mo10039aQ(avh);
    }

    /* renamed from: b */
    public <T> void mo7500b(Class<T> cls, aVH<T> avh) {
        mo7501b(eNB, cls, avh);
    }

    /* renamed from: b */
    public <T> void mo7501b(String str, Class<T> cls, aVH<T> avh) {
        this.eNC.mo10044k(new C1754a(str, cls), avh);
    }

    /* access modifiers changed from: protected */
    /* renamed from: l */
    public void mo7502l(String str, Object obj) {
        try {
            Collection<aVH<?>> aR = this.eNC.mo10040aR(new C1754a(str, obj.getClass()));
            if (aR != null) {
                for (aVH<?> k : aR) {
                    k.mo2004k(str, obj);
                }
            }
        } catch (RuntimeException e) {
            e.printStackTrace();
        }
    }

    /* access modifiers changed from: protected */
    /* renamed from: m */
    public void mo7503m(String str, Object obj) {
        Collection<aVH<?>> aR = this.eNC.mo10040aR(new C1754a(str, obj.getClass()));
        if (aR != null) {
            for (aVH f : aR) {
                try {
                    f.mo2003f(str, obj);
                } catch (RuntimeException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    /* renamed from: a.Zu$a */
    public static class C1754a {
        private Class<?> clazz;
        private Object key;

        public C1754a(Object obj, Class<?> cls) {
            this.key = obj;
            this.clazz = cls;
        }

        public boolean equals(Object obj) {
            C1754a aVar = (C1754a) obj;
            return this.key.equals(aVar.key) && this.clazz.equals(aVar.clazz);
        }

        public int hashCode() {
            return (this.key.hashCode() * 31) + this.clazz.hashCode();
        }

        public Object getKey() {
            return this.key;
        }

        public Class<?> getClazz() {
            return this.clazz;
        }
    }
}
