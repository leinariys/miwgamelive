package logic.thred;

import java.io.Serializable;

/* renamed from: a.Md */
/* compiled from: a */
public final class C0871Md implements Serializable, Comparable<C0871Md> {

    private String className;
    private String fileName;
    private String methodName;

    public C0871Md(StackTraceElement stackTraceElement) {
        mo3990a(stackTraceElement);
    }

    public C0871Md() {
    }

    /* renamed from: eq */
    private static boolean m7078eq(Object obj, Object obj2) {
        return obj == obj2 || (obj != null && obj.equals(obj2));
    }

    /* renamed from: a */
    public void mo3990a(StackTraceElement stackTraceElement) {
        this.className = stackTraceElement.getClassName();
        this.methodName = stackTraceElement.getMethodName();
        this.fileName = stackTraceElement.getFileName();
    }

    public String toString() {
        return String.valueOf(String.valueOf(this.className) + "." + this.methodName) + (this.fileName != null ? "(" + this.fileName + ")" : "(Unknown Source)");
    }

    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof C0871Md)) {
            return false;
        }
        C0871Md md = (C0871Md) obj;
        if (!md.className.equals(this.className) || !m7078eq(this.methodName, md.methodName) || !m7078eq(this.fileName, md.fileName)) {
            return false;
        }
        return true;
    }

    public int hashCode() {
        return (this.fileName == null ? 0 : this.fileName.hashCode()) + (((this.className.hashCode() * 31) + this.methodName.hashCode()) * 31);
    }

    public String getDeclaringClass() {
        return this.className;
    }

    public String getMethodName() {
        return this.methodName;
    }

    public String getFileName() {
        return this.fileName;
    }

    public String getFullName() {
        return String.valueOf(this.className) + "." + this.methodName;
    }

    /* renamed from: b */
    public int compareTo(C0871Md md) {
        int compareTo = getDeclaringClass().compareTo(md.getDeclaringClass());
        if (compareTo == 0) {
            return this.methodName.compareTo(md.methodName);
        }
        return compareTo;
    }
}
