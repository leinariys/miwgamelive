package logic.thred;

import java.util.Map;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * Фабрика потоков для очереди потоков
 * NT
 */
/* renamed from: a.NT */
/* compiled from: a */
public class PoolThread {

    /* renamed from: NL */
    private static final String lineSeparator = System.getProperty("line.separator");

    /* renamed from: a */
    public static Thread initThreadWrapper(Runnable runnable, String name) {
        return new ThreadWrapper(runnable, name);
    }

    /* renamed from: a */
    public static ThreadPoolExecutor newFixedThreadPool(int corePoolSize, int maximumPoolSize, long keepAliveTime, TimeUnit timeUnit, BlockingQueue<Runnable> workQueue, String prefix, boolean isDaemon) {
        return new ThreadPoolExecutor(corePoolSize, maximumPoolSize, keepAliveTime, timeUnit, workQueue, new ThreadFactoryWrapper(prefix, isDaemon));
    }

    /**
     * @param objBlock объект для блокировки
     */
    /* renamed from: aC */
    public static void wait(Object objBlock) {
        try {
            objBlock.wait();
        } catch (InterruptedException e) {
            throw new RuntimeException("wait interrupted", e);
        }
    }

    /**
     * @param objBlock
     * @param timeout  the maximum time to wait in milliseconds.
     */
    /* renamed from: a */
    public static void wait(Object objBlock, long timeout) {
        try {
            objBlock.wait(timeout);
        } catch (InterruptedException e) {
            throw new RuntimeException("wait interrupted", e);
        }
    }

    /**
     * @param time to sleep in milliseconds
     */
    public static void sleep(long time) {
        try {
            Thread.sleep(time);
        } catch (InterruptedException e) {
            throw new RuntimeException("sleep interrupted", e);
        }
    }

    /**
     * @param runnable Задача для выполнения
     * @return
     */
    /* renamed from: g */
    public static Thread init(Runnable runnable) {
        ThreadWrapper threadWrapper = new ThreadWrapper(runnable);
        threadWrapper.setDaemon(true);
        threadWrapper.start();
        return threadWrapper;
    }

    /**
     * @param runnable Задача для выполнения
     * @param name     Имя потока
     * @return
     */
    /* renamed from: b */
    public static Thread init(Runnable runnable, String name) {
        ThreadWrapper threadWrapper = new ThreadWrapper(runnable, name);
        threadWrapper.setDaemon(true);
        threadWrapper.start();
        return threadWrapper;
    }

    /**
     * @param thread Ожидает, пока эта ветка умрет.
     */
    /* renamed from: d */
    public static void join(Thread thread) {
        try {
            thread.join();
        } catch (InterruptedException e) {
            throw new RuntimeException("join interrupted", e);
        }
    }

    /**
     * Статистика работы фабрики потоков
     *
     * @return
     */
    public static String statusPoolThread() {
        int index = 0;

        StringBuilder builder = new StringBuilder();
        builder.append("THREAD COUNT: " + Thread.activeCount() + lineSeparator + lineSeparator);
        Map<Thread, StackTraceElement[]> allStackTraces = Thread.getAllStackTraces();
        builder.append("NON DAEMON THREADS:" + lineSeparator);

        boolean isAliveNotDaemon = false;
        for (Thread keyStack : allStackTraces.keySet()) {
            if (keyStack.isAlive() && !keyStack.isDaemon()) {
                isAliveNotDaemon = true;
                builder.append("   * " + keyStack.getName() + lineSeparator);
            }
        }

        if (!isAliveNotDaemon) {
            builder.append(" ->  All active Threads are of daemon type" + lineSeparator);
        }

        builder.append(lineSeparator);
        builder.append("STACK TRACES:" + lineSeparator);

        for (Map.Entry<Thread, StackTraceElement[]> valueStack : allStackTraces.entrySet()) {
            builder.append(String.valueOf(index) + " - " + traceMethod(valueStack.getKey(), valueStack.getValue()));
            index++;
        }
        return builder.toString();
    }

    /**
     * Часть сообщения для статистики
     *
     * @param thread Поток
     * @return Часть сообщения
     */
    /* renamed from: e */
    public static String traceMethod(Thread thread) {
        return traceMethod(thread, thread.getStackTrace());
    }

    /* renamed from: a */
    public static String traceMethod(Thread thread, StackTraceElement[] traceMethodName) {
        StringBuilder builder = new StringBuilder();
        builder.append(thread.getName() + ":" + lineSeparator);

        if (traceMethodName.length > 0) {
            for (int i = 0; i < traceMethodName.length; i++) {
                builder.append("\tat " + traceMethodName[i] + lineSeparator);
            }
        } else {
            builder.append("\t<NO STACK TRACE INFORMATION>" + lineSeparator);
        }
        return builder.toString();
    }

    public static void main(String[] strArr) {
        RunnableTest runnableTest = new RunnableTest();
        Thread thread = new Thread(runnableTest, "T1-nondaemon");
        thread.setDaemon(false);
        Thread thread2 = new Thread(runnableTest, "T2-daemon");
        thread2.setDaemon(true);
        Thread thread3 = new Thread(runnableTest, "T3-doesnothing");
        thread.start();
        thread2.start();
        thread3.start();
        try {
            Thread.sleep(100);
        } catch (InterruptedException e) {
        }
        System.out.println(statusPoolThread());
    }

    /* renamed from: a.NT$b */
    /* compiled from: a */
    public static class ThreadFactoryWrapper implements ThreadFactory {
        boolean isDaemon;
        /**
         * Атомная операция
         * <getByteBuffer>Можно безопасно выполнять при параллельных
         * вычислениях не используя блокировщик и синхронизацию
         */
        AtomicInteger atomicInteger;
        /**
         * Имя потока
         */
        String prefix;

        public ThreadFactoryWrapper(String prefix, boolean isDaemon) {
            this.atomicInteger = new AtomicInteger();
            if (prefix == null) {
                this.prefix = "Thread";
            } else {
                this.prefix = prefix;
            }
            this.isDaemon = isDaemon;
        }

        public ThreadFactoryWrapper() {
            this.atomicInteger = new AtomicInteger();
            this.prefix = "Thread";
            this.isDaemon = false;
        }

        public Thread newThread(Runnable runnable) {
            ThreadWrapper ano = new ThreadWrapper(runnable, String.valueOf(this.prefix) + " " + this.atomicInteger.incrementAndGet());
            ano.setDaemon(this.isDaemon);
            return ano;
        }
    }

    /* renamed from: a.NT$a */
    static class RunnableTest implements Runnable {
        public void run() {
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
            }
        }
    }
}
