package logic.thred;

import java.io.File;
import java.io.FileFilter;
import java.io.RandomAccessFile;

/* renamed from: a.JY */
/* compiled from: a */
public class LockInstanceApp {
    private static final String pathLock = (String.valueOf(System.getProperty("user.home")) + File.separator + "taikodom");
    private static final String folder = "lock";
    private static final String expansion = ".lock";
    private static File pathFile;
    private static RandomAccessFile streamFile;

    /* renamed from: hn */
    public static LockInstanceApp m5831hn(String str) {
        return setMarkOriginal(str, pathLock);
    }

    /**
     * Создание файла, сигнализирующего о запуске клиента.
     *
     * @param nameFile C:\Projects\Java\TaikodomGameLIVE\lock
     * @param pathLock com.hoplon.Taikodom1537873712730922.lock
     * @return
     */
    /* renamed from: O */
    public static LockInstanceApp setMarkOriginal(String nameFile, String pathLock) {
        pathFile = new File(String.valueOf(pathLock) + File.separator + folder, String.valueOf(nameFile) + expansion);
        return markOriginal(pathFile);
    }

    /* renamed from: ho */
    public static LockInstanceApp m5832ho(String str) {
        return setMarkOriginalTime(str, pathLock);
    }

    /**
     * Создание блокирующего файла, сигнализирующего о запуске приложения, с добавлением метки времени
     *
     * @param namePacket "com.hoplon.Taikodom"
     * @param pathLock   Адресс пользовательской папки "C:\Projects\Java\TaikodomGameLIVE"
     * @return
     */
    /* renamed from: P */
    public static LockInstanceApp setMarkOriginalTime(String namePacket, String pathLock) {
        return setMarkOriginal(String.valueOf(namePacket) + new StringBuilder().append(System.currentTimeMillis()).append((long) (Math.random() * 1000.0d)).toString(), pathLock);
    }

    /* renamed from: hp */
    public static boolean m5833hp(String str) {
        return isCopyApp(str, pathLock);
    }

    /**
     * Проверка не запущенна ли ещё одна копия клиента
     *
     * @param str  "com.hoplon.Taikodom"
     * @param str2 Адресс пользовательской папки "C:\Projects\Java\TaikodomGameLIVE"
     * @return true -lock есть и не можем удалить, false - блокировки нет
     */
    /* renamed from: Q */
    public static boolean isCopyApp(String str, String str2) {
        File[] listFiles = new File(String.valueOf(str2) + File.separator + folder).listFiles(new C0672a(str));
        if (listFiles == null) {
            return false;
        }
        for (File file : listFiles) {
            if (file.exists() && !file.delete()) {
                return true;
            }
        }
        return false;
    }

    /**
     * Пометить приложение как оригинал
     * Открытие файла на запись чтение
     *
     * @param file
     * @return
     */
    /* renamed from: i */
    private static LockInstanceApp markOriginal(File file) {
        if (pathFile.exists() && !pathFile.delete()) {
            return null;
        }
        try {
            pathFile.getParentFile().mkdirs();
            streamFile = new RandomAccessFile(pathFile, "rw");
            streamFile.write(10);
            return new LockInstanceApp();
        } catch (Exception e) {
            System.out.println("Can not write to lock file: " + pathFile.getAbsolutePath());
            System.out.println(e.getMessage());
            return new LockInstanceApp();
        }
    }

    /* renamed from: a.JY$a */
    static class C0672a implements FileFilter {
        private final /* synthetic */ String dnr;

        C0672a(String str) {
            this.dnr = str;
        }

        public boolean accept(File file) {
            return file.getName().startsWith(this.dnr);
        }
    }
}
