package logic.thred;

import java.io.PrintWriter;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.TreeSet;

/* renamed from: a.CK */
/* compiled from: a */
public class C0185CK {
    private final C6001aeZ cAa = new C6001aeZ((C0871Md) null);
    private final C0871Md cAb = new C0871Md();
    private final Thread thread;
    public int cAd;
    public int[] cAe = new int[Thread.State.values().length];
    public int cAf;
    private Map<C0871Md, aBY> cAc = new HashMap();

    public C0185CK(Thread thread2) {
        this.thread = thread2;
    }

    public C6001aeZ aDv() {
        return this.cAa;
    }

    public Thread getThread() {
        return this.thread;
    }

    /* renamed from: a */
    public aBY mo947a(C0871Md md) {
        return this.cAc.get(md);
    }

    /* renamed from: a */
    public void mo948a(StackTraceElement[] stackTraceElementArr) {
        this.cAf++;
        this.cAd++;
        int[] iArr = this.cAe;
        int ordinal = this.thread.getState().ordinal();
        iArr[ordinal] = iArr[ordinal] + 1;
        C6001aeZ aez = this.cAa;
        int length = stackTraceElementArr.length;
        while (true) {
            length--;
            if (length >= 0) {
                StackTraceElement stackTraceElement = stackTraceElementArr[length];
                this.cAb.mo3990a(stackTraceElement);
                aBY a = mo947a(this.cAb);
                if (a == null) {
                    C0871Md md = new C0871Md(stackTraceElement);
                    Map<C0871Md, aBY> map = this.cAc;
                    a = new aBY(md);
                    map.put(md, a);
                }
                if (a.hmH != this.cAf) {
                    a.hmH = this.cAf;
                    a.mo7929b(stackTraceElement);
                }
                C0871Md bUY = a.bUY();
                Map<C0871Md, C6001aeZ> bUX = aez.bUX();
                C6001aeZ aez2 = bUX.get(bUY);
                if (aez2 == null) {
                    aez2 = new C6001aeZ(bUY);
                    bUX.put(bUY, aez2);
                }
                aez2.bUW();
                aez = aez2;
            } else {
                return;
            }
        }
    }

    public int aDw() {
        return this.cAd;
    }

    public void dump(PrintWriter printWriter) {
        int i = 0;
        printWriter.println(this.thread);
        for (int i2 = 0; i2 < this.cAe.length; i2++) {
            if (this.cAe[i2] > 0) {
                printWriter.print(" " + ((int) ((((float) this.cAe[i2]) * 100.0f) / ((float) this.cAd))) + "% " + Thread.State.values()[i2]);
            }
        }
        printWriter.println();
        Iterator it = new TreeSet(this.cAc.values()).iterator();
        while (it.hasNext()) {
            aBY aby = (aBY) it.next();
            if ((aby.cMQ() * 100) / ((long) this.cAd) > 1 && (i = i + 1) < 100) {
                printWriter.print(" *[");
                printWriter.print(aby.cMQ());
                printWriter.print(", ");
                printWriter.print((aby.cMQ() * 100) / ((long) this.cAd));
                printWriter.print("%] ");
                printWriter.println(aby.bUY().toString());
            }
        }
        printWriter.println();
        this.cAa.mo13088a(printWriter, this.cAd, " ");
    }
}
