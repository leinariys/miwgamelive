package logic.thred;

import java.io.FilterWriter;
import java.io.IOException;
import java.io.Writer;
import java.util.ArrayList;

/**
 * Запись в файл статистики потоков
 */
/* renamed from: a.hi */
/* compiled from: a */
public class C2624hi extends FilterWriter {

    /* renamed from: TM */
    private String f8025TM;

    /* renamed from: TN */
    private boolean f8026TN = false;

    /* renamed from: TO */
    private ArrayList<String> f8027TO;

    public C2624hi(Writer writer) {
        super(writer);
    }

    public String getIndent() {
        return this.f8025TM;
    }

    /* renamed from: az */
    public void mo19338az(String str) {
        this.f8025TM = str;
    }

    /* renamed from: aA */
    public void mo19337aA(String str) {
        push();
        this.f8025TM = String.valueOf(this.f8025TM) + str;
    }

    public void push() {
        if (this.f8027TO == null) {
            this.f8027TO = new ArrayList<>();
        }
        this.f8027TO.add(this.f8025TM);
    }

    public void pop() {
        if (this.f8027TO != null && this.f8027TO.size() > 0) {
            this.f8025TM = this.f8027TO.remove(this.f8027TO.size() - 1);
        }
    }

    public void write(String str, int i, int i2) throws IOException {
        while (i < str.length() && i2 - 1 >= 0) {
            write(str.charAt(i));
            i++;
        }
    }

    public void write(char[] cArr, int i, int i2) throws IOException {
        while (i < cArr.length && i2 - 1 >= 0) {
            write(cArr[i]);
            i++;
        }
    }

    public void write(int i) throws IOException {
        if (i == 10) {
            this.out.write(i);
            String indent = getIndent();
            if (indent != null) {
                this.out.write(indent);
            }
            this.f8026TN = false;
            return;
        }
        if (this.f8026TN) {
            this.out.write(10);
            String indent2 = getIndent();
            if (indent2 != null) {
                this.out.write(indent2);
            }
        }
        if (i == 13) {
            this.f8026TN = true;
        }
        this.out.write(i);
    }
}
