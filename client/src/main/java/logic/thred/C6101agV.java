package logic.thred;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;
import java.util.WeakHashMap;

/* renamed from: a.agV  reason: case insensitive filesystem */
/* compiled from: a */
public class C6101agV {
    /* access modifiers changed from: private */
    public final SimpleDateFormat dBH = new SimpleDateFormat("yyyyMMdd HH:mm:ss.SSS ");
    /* access modifiers changed from: private */
    public PrintWriter eqC;
    /* access modifiers changed from: private */
    public Map<C1886c, Integer> fKT = new WeakHashMap();
    /* access modifiers changed from: private */
    public double fKV = 6.0E10d;
    /* access modifiers changed from: private */
    public C2624hi fKW;
    private Thread fKR;
    private Thread fKS;
    private long fKU = 50;

    public C6101agV() {
        setPrintWriter(new PrintWriter(System.out, true));
    }

    public void start() {
        if (this.fKR == null) {
            this.fKR = new C1885b("Watch dog Thread");
            this.fKS = new C1884a("Watchdog dumper thread");
            this.fKS.setDaemon(true);
            this.fKR.setDaemon(true);
            this.fKR.start();
            this.fKS.start();
        }
    }

    public void stop() {
        if (this.fKR != null) {
            this.fKR.interrupt();
            this.fKR = null;
        }
    }

    public boolean isRunning() {
        return this.fKR != null;
    }

    /* access modifiers changed from: private */
    public void doRun() {
        try {
            Thread.sleep(this.fKU);
            caL();
        } catch (InterruptedException e) {
        }
    }

    public synchronized void caL() {
        C6615aqP UV = C3253pf.m37266UV();
        C6615aqP UW = C3253pf.m37267UW();
        if (!(UV == null || UW == null)) {
            C1886c cVar = new C1886c(UV, UW);
            Integer num = this.fKT.get(cVar);
            if (num == null) {
                num = new Integer(0);
            }
            this.fKT.put(cVar, new Integer(num.intValue() + 1));
        }
    }

    public synchronized void aXn() {
        try {
            this.eqC.println();
            for (C1886c a : this.fKT.keySet()) {
                mo13404a(a);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return;
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public void mo13404a(C1886c cVar) {
        Integer num = this.fKT.get(cVar);
        if (num != null) {
            StringWriter stringWriter = new StringWriter();
            PrintWriter printWriter = new PrintWriter(stringWriter);
            printWriter.print(num + " " + cVar.gYI + "\n\t\t" + cVar.gYJ);
            printWriter.flush();
            this.eqC.println(stringWriter.toString());
            this.eqC.flush();
        }
    }

    /* renamed from: c */
    public synchronized void mo13406c(Thread thread) {
        this.fKT.remove(thread);
    }

    public synchronized void reset() {
        this.fKT.clear();
    }

    public void setPrintWriter(PrintWriter printWriter) {
        this.fKW = new C2624hi(printWriter);
        this.eqC = new PrintWriter(this.fKW);
    }

    /* renamed from: ht */
    public void mo13408ht(long j) {
        this.fKU = j;
    }

    /* renamed from: K */
    public void mo13403K(double d) {
        this.fKV = d;
    }

    /* renamed from: a.agV$a */
    final class C1884a extends Thread {
        private long fFx = System.nanoTime();

        C1884a(String str) {
            super(str);
        }

        public void run() {
            while (true) {
                try {
                    doRun();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                    return;
                }
            }
        }

        private void doRun() throws InterruptedException {
            Thread.sleep(1000);
            long nanoTime = System.nanoTime();
            if (((double) nanoTime) > ((double) this.fFx) + C6101agV.this.fKV) {
                if (C6101agV.this.fKT.keySet().size() > 0) {
                    C6101agV.this.fKW.mo19338az(C6101agV.this.dBH.format(new Date(System.currentTimeMillis())));
                    C6101agV.this.eqC.println("-------------------- Collision Dump -------------------- ");
                    C6101agV.this.aXn();
                    C6101agV.this.eqC.println("------------------Collision Dump End ------------------ ");
                    C6101agV.this.eqC.flush();
                }
                C6101agV.this.reset();
                this.fFx = nanoTime;
            }
        }
    }

    /* renamed from: a.agV$b */
    /* compiled from: a */
    class C1885b extends Thread {
        C1885b(String str) {
            super(str);
        }

        public void run() {
            while (true) {
                C6101agV.this.doRun();
            }
        }
    }

    /* renamed from: a.agV$c */
    /* compiled from: a */
    private class C1886c {
        C6615aqP gYI;
        C6615aqP gYJ;

        public C1886c(C6615aqP aqp, C6615aqP aqp2) {
            this.gYI = aqp;
            this.gYJ = aqp2;
        }

        public int hashCode() {
            return this.gYI.mo2441IS().hashCode() + this.gYJ.mo2441IS().hashCode();
        }

        public boolean equals(Object obj) {
            if (!(obj instanceof C1886c)) {
                return super.equals(obj);
            }
            C1886c cVar = (C1886c) obj;
            return (this.gYI == cVar.gYI && this.gYJ == cVar.gYJ) || (this.gYI == cVar.gYJ && this.gYJ == cVar.gYI);
        }
    }
}
