package logic.thred;

import java.util.HashSet;
import java.util.Set;

/* renamed from: a.aBY */
/* compiled from: a */
public class aBY implements Comparable<aBY> {
    private final C0871Md fsB;
    public int hmH;
    private Set<StackTraceElement> elements = new HashSet();
    private long hmG;

    public aBY(C0871Md md) {
        this.fsB = md;
    }

    /* renamed from: a */
    public int compareTo(aBY aby) {
        int i = (int) (aby.hmG - this.hmG);
        if (i != 0) {
            return i;
        }
        int b = bUY().compareTo(aby.bUY());
        if (b == 0) {
            return System.identityHashCode(this) - System.identityHashCode(aby);
        }
        return b;
    }

    /* renamed from: b */
    public void mo7929b(StackTraceElement stackTraceElement) {
        this.hmG++;
        this.elements.add(stackTraceElement);
    }

    public long cMQ() {
        return this.hmG;
    }

    /* renamed from: jL */
    public void mo7933jL(long j) {
        this.hmG = j;
    }

    public String toString() {
        String str = String.valueOf(this.fsB.getDeclaringClass()) + "." + this.fsB.getMethodName();
        for (StackTraceElement next : this.elements) {
            if (next.getFileName() != null) {
                if (next.getLineNumber() > 0) {
                    str = String.valueOf(str) + "(" + next.getFileName() + ":" + next.getLineNumber() + ")";
                } else {
                    str = String.valueOf(str) + "(" + next.getFileName() + ")";
                }
            }
        }
        return str;
    }

    public C0871Md bUY() {
        return this.fsB;
    }
}
