package logic.thred;

import com.hoplon.geometry.Vec3f;
import game.geometry.Vec3d;
import logic.aaa.C6026aey;

/* renamed from: a.akz  reason: case insensitive filesystem */
/* compiled from: a */
public class C6339akz {

    public final Vec3f fTI = new Vec3f();
    public final Vec3f fTJ = new Vec3f();
    public final Vec3d fTK = new Vec3d();
    public final Vec3f fTL = new Vec3f();
    public final Vec3f fTM = new Vec3f();
    /* renamed from: Ok */
    public float f4816Ok = 0.0f;
    public C6026aey fTN;

    public Vec3f cgn() {
        return this.fTI;
    }

    public Vec3f cgo() {
        return this.fTJ;
    }

    public Vec3d cgp() {
        return this.fTK;
    }

    public Vec3d cgq() {
        return this.fTK;
    }

    public Vec3f cgr() {
        return this.fTL;
    }

    public Vec3f cgs() {
        return this.fTM;
    }

    /* renamed from: aN */
    public void mo14612aN(Vec3f vec3f) {
        this.fTI.set(vec3f);
    }

    /* renamed from: aO */
    public void mo14613aO(Vec3f vec3f) {
        this.fTJ.set(vec3f);
    }

    /* renamed from: W */
    public void mo14611W(Vec3d ajr) {
        this.fTK.mo9484aA(ajr);
    }

    /* renamed from: aP */
    public void mo14614aP(Vec3f vec3f) {
        this.fTL.set(vec3f);
    }

    /* renamed from: aQ */
    public void mo14615aQ(Vec3f vec3f) {
        this.fTM.set(vec3f);
    }
}
