package logic.thred;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * Диагностика работы потоков, статистика использования
 */
/* renamed from: a.akc  reason: case insensitive filesystem */
/* compiled from: a */
public class WatchDog {
    private static ThreadLocal<Map<String, Object[]>> hBd = new ThreadLocal() {
        @Override
        protected Map initialValue() {
            return new HashMap();
        }
    };
    private final SimpleDateFormat dateFormat = new SimpleDateFormat("yyyyMMdd HH:mm:ss.SSS ");
    /* access modifiers changed from: private */
    public PrintWriter eqC;
    /**
     * Интервал логирования
     */
    /* access modifiers changed from: private */
    public double fKV = 6.0E10d;
    private Thread mainWatchDog;
    private Thread dumperWatchDog;
    /**
     * millis - время сна потока
     */
    private long slipBeforeRun = 100;
    private C2624hi fKW;
    private Map<Thread, C0185CK> hBb = new WeakHashMap();
    private long hBc = 0;

    public WatchDog() {
        setPrintWriter(new PrintWriter(System.out, true));
    }

    public static void main(String[] strArr) throws InterruptedException {
        WatchDog akc = new WatchDog();
        akc.start();
        while (true) {
            m23482xA(4);
            akc.mo14456f(Thread.currentThread());
            akc.reset();
        }
    }

    /* renamed from: xA */
    private static void m23482xA(int i) throws InterruptedException {
        Thread.sleep(100);
        if (i > 0) {
            m23482xA(i - 1);
            m23483xB(i - 1);
        }
    }

    /* renamed from: xB */
    private static void m23483xB(int i) throws InterruptedException {
        m23482xA(i);
    }

    /* renamed from: c */
    public static void m23480c(Class<?> cls, String str, Object... objArr) {
        hBd.get().put(str, objArr);
    }

    /* renamed from: l */
    public static void m23481l(Class<?> cls, String str) {
        hBd.get().remove(str);
    }

    public void start() {
        if (this.mainWatchDog == null) {
            this.mainWatchDog = new C1929b("Watch dog Thread");
            this.dumperWatchDog = new C1928a("Watchdog dumper thread");
            this.dumperWatchDog.setDaemon(true);
            this.mainWatchDog.setDaemon(true);
            this.mainWatchDog.start();
            this.dumperWatchDog.start();
        }
    }

    public void stop() {
        if (this.mainWatchDog != null) {
            this.mainWatchDog.interrupt();
            this.mainWatchDog = null;
        }
    }

    public boolean isRunning() {
        return this.mainWatchDog != null;
    }

    /* access modifiers changed from: private */
    public void doRun() {
        try {
            Thread.sleep(this.slipBeforeRun);
            caL();
        } catch (InterruptedException e) {
        }
    }

    public synchronized void caL() {
        this.hBc++;
        for (Map.Entry next : Thread.getAllStackTraces().entrySet()) {
            C0185CK ck = this.hBb.get(next.getKey());
            if (ck == null) {
                C0185CK ck2 = new C0185CK((Thread) next.getKey());
                this.hBb.put((Thread) next.getKey(), ck2);
                ck = ck2;
            }
            ck.mo948a((StackTraceElement[]) next.getValue());
        }
    }

    /* renamed from: f */
    public synchronized void mo14456f(Thread thread) {
        try {
            this.fKW.mo19338az(this.dateFormat.format(new Date(System.currentTimeMillis())));
            Runtime runtime = Runtime.getRuntime();
            long maxMemory = runtime.maxMemory();
            long freeMemory = runtime.freeMemory();
            long j = runtime.totalMemory();
            long j2 = j - freeMemory;
            this.eqC.println();
            this.eqC.printf("Max Memory:   %,14d\r\n", new Object[]{Long.valueOf(maxMemory)});
            this.eqC.printf("Memory Left:  %,14d  %,4.1f%% of max\r\n", new Object[]{Long.valueOf((maxMemory - j) + freeMemory), Float.valueOf((((float) ((maxMemory - j) + freeMemory)) * 100.0f) / ((float) maxMemory))});
            this.eqC.printf("Aloc. Memory: %,14d  %,4.1f%% of max\r\n", new Object[]{Long.valueOf(j), Float.valueOf((((float) j) * 100.0f) / ((float) maxMemory))});
            this.eqC.printf("Free Memory:  %,14d  %,4.1f%% of max %,4.1f%% of aloc.\r\n", new Object[]{Long.valueOf(freeMemory), Float.valueOf((((float) freeMemory) * 100.0f) / ((float) maxMemory)), Float.valueOf((((float) freeMemory) * 100.0f) / ((float) j))});
            this.eqC.printf("Used Memory:  %,14d  %,4.1f%% of max %,4.1f%% of aloc.\r\n", new Object[]{Long.valueOf(j2), Float.valueOf((((float) j2) * 100.0f) / ((float) maxMemory)), Float.valueOf((((float) j2) * 100.0f) / ((float) j))});
            this.eqC.println();
            if (thread == null) {
                TreeSet treeSet = new TreeSet(new C1930c());
                treeSet.addAll(this.hBb.keySet());
                Iterator it = treeSet.iterator();
                while (it.hasNext()) {
                    Thread thread2 = (Thread) it.next();
                    if (!(thread2 == null || thread2 == this.dumperWatchDog || thread2 == this.mainWatchDog)) {
                        mo14457g(thread2);
                    }
                }
                this.eqC.flush();
            } else {
                mo14457g(thread);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return;
    }

    /* access modifiers changed from: protected */
    /* renamed from: g */
    public void mo14457g(Thread thread) {
        C0185CK ck = this.hBb.get(thread);
        if (ck != null) {
            StringWriter stringWriter = new StringWriter();
            PrintWriter printWriter = new PrintWriter(stringWriter);
            ck.dump(printWriter);
            printWriter.flush();
            this.eqC.println(stringWriter.toString());
            this.eqC.flush();
        }
    }

    /* renamed from: c */
    public synchronized void mo14454c(Thread thread) {
        this.hBb.remove(thread);
    }

    public synchronized void reset() {
        this.hBb.clear();
        this.hBc = 0;
    }

    public void setPrintWriter(PrintWriter printWriter) {
        this.fKW = new C2624hi(printWriter);
        this.eqC = new PrintWriter(this.fKW);
    }

    /* renamed from: ht */
    public void setSlipBeforeRun(long j) {
        this.slipBeforeRun = j;
    }

    /* renamed from: K */
    public void mo14453K(double d) {
        this.fKV = d;
    }

    /* renamed from: a.akc$a */
    final class C1928a extends Thread {
        private long fFx = System.nanoTime();

        C1928a(String str) {
            super(str);
        }

        public void run() {
            while (true) {
                try {
                    doRun();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                    return;
                }
            }
        }

        private void doRun() throws InterruptedException {
            Thread.sleep(1000);
            long nanoTime = System.nanoTime();
            if (((double) nanoTime) > ((double) this.fFx) + WatchDog.this.fKV) {
                WatchDog.this.eqC.println("-------------------- Thread Dump -------------------- ");
                WatchDog.this.mo14456f((Thread) null);
                WatchDog.this.eqC.println("------------------Thread Dump End ------------------ ");
                WatchDog.this.reset();
                this.fFx = nanoTime;
            }
        }
    }

    /* renamed from: a.akc$b */
    /* compiled from: a */
    class C1929b extends Thread {
        C1929b(String str) {
            super(str);
        }

        public void run() {
            while (true) {
                WatchDog.this.doRun();
            }
        }
    }

    /* renamed from: a.akc$c */
    /* compiled from: a */
    class C1930c implements Comparator<Thread> {
        C1930c() {
        }

        /* renamed from: a */
        public int compare(Thread thread, Thread thread2) {
            return (int) (thread.getId() - thread2.getId());
        }
    }
}
