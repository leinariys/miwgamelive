package logic.thred;

import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/* renamed from: a.aeZ  reason: case insensitive filesystem */
/* compiled from: a */
public class C6001aeZ implements Comparable<C6001aeZ> {
    private final Map<C0871Md, C6001aeZ> buY = new HashMap();
    private final C0871Md fsB;
    private int fsC;

    public C6001aeZ(C0871Md md) {
        this.fsB = md;
    }

    public int getCallCount() {
        return this.fsC;
    }

    /* renamed from: a */
    public int compareTo(C6001aeZ aez) {
        return this.fsC - aez.fsC;
    }

    public void bUW() {
        this.fsC++;
    }

    public Map<C0871Md, C6001aeZ> bUX() {
        return this.buY;
    }

    public C0871Md bUY() {
        return this.fsB;
    }

    public String toString() {
        return this.fsB.toString();
    }

    /* renamed from: a */
    public void mo13088a(PrintWriter printWriter, int i, String str) {
        mo13089a(printWriter, i, str, str);
    }

    /* renamed from: a */
    public void mo13089a(PrintWriter printWriter, int i, String str, String str2) {
        if (this.fsB != null) {
            printWriter.print(str);
            printWriter.print('[');
            printWriter.print(this.fsC);
            printWriter.print(", ");
            printWriter.print((this.fsC * 100) / i);
            printWriter.print("%] ");
            printWriter.println(this.fsB.toString());
        }
        ArrayList<C6001aeZ> arrayList = new ArrayList<>();
        for (C6001aeZ next : this.buY.values()) {
            if ((next.fsC * 100) / i > 1) {
                arrayList.add(next);
            }
        }
        int size = arrayList.size();
        int i2 = 0;
        for (C6001aeZ a : arrayList) {
            i2++;
            a.mo13089a(printWriter, i, String.valueOf(str2) + (i2 != size ? "+" : "+"), i2 != size ? String.valueOf(str2) + "|" : String.valueOf(str2) + " ");
        }
    }
}
