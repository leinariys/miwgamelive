package logic.thred;

/* renamed from: a.anO  reason: case insensitive filesystem */
/* compiled from: a */
public class ThreadWrapper extends Thread {
    private Object context;

    public ThreadWrapper(Runnable target) {
        super(target);
    }

    public ThreadWrapper(Runnable target, String name) {
        super(target, name);
    }

    public ThreadWrapper(String nameThread) {
        super(nameThread);
    }

    public Object getContext() {
        return this.context;
    }

    /* renamed from: ao */
    public void setContext(Object context) {
        this.context = context;
    }
}
