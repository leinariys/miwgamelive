package logic.thred;

import game.geometry.Quat4fWrap;
import game.geometry.TransformWrap;
import logic.aaa.C2235dB;
import logic.bbb.C4029yK;

/* renamed from: a.aqP  reason: case insensitive filesystem */
/* compiled from: a */
public interface C6615aqP extends C2235dB {
    /* renamed from: IL */
    C4029yK mo2437IL();

    /* renamed from: IM */
    byte mo2438IM();

    /* renamed from: IQ */
    TransformWrap mo2439IQ();

    /* renamed from: IR */
    void mo2440IR();

    /* renamed from: IS */
    Object mo2441IS();

    /* renamed from: a */
    void mo2543a(C1362Tq tq);

    Quat4fWrap getOrientation();

    boolean isStatic();
}
