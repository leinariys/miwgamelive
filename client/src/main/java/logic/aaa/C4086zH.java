package logic.aaa;

import logic.ui.C2698il;
import logic.ui.item.TabbedPane;

import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import java.awt.*;
import java.util.HashSet;
import java.util.Set;

/* renamed from: a.zH */
/* compiled from: a */
public class C4086zH extends C1506WA {

    private C4087a iMA;
    private TabbedPane iMy;
    private Set<C5685aSv> iMz;
    private String key;

    /* renamed from: qx */
    private C2698il f9642qx;

    public C4086zH(String str, C2698il ilVar) {
        this(str, ilVar, "");
    }

    public C4086zH(String str, C2698il ilVar, String str2) {
        this.key = str;
        this.f9642qx = ilVar;
        this.iMy = new TabbedPane();
        this.iMy.addChangeListener(new C4088b());
        if (str2 != null && !str2.isEmpty()) {
            ilVar = ilVar.mo4916ce(str2);
        }
        ilVar.add(this.iMy);
        this.iMy.setSize(ilVar.getSize());
        this.iMz = new HashSet();
    }

    public String getKey() {
        return this.key;
    }

    public Set<C5685aSv> duf() {
        return this.iMz;
    }

    public TabbedPane dug() {
        return this.iMy;
    }

    public C2698il dhP() {
        return this.f9642qx;
    }

    public C5685aSv duh() {
        for (C5685aSv next : this.iMz) {
            if (mo23273d(next)) {
                return next;
            }
        }
        return null;
    }

    /* renamed from: c */
    public void mo23272c(C5685aSv asv) {
        this.iMy.setSelectedComponent(asv.duv());
        asv.duw();
    }

    /* renamed from: d */
    public boolean mo23273d(C5685aSv asv) {
        return this.iMy.getSelectedComponent() == asv.duv();
    }

    /* renamed from: a */
    public void mo23269a(C5685aSv asv) {
        if (asv.dhP() == null) {
            asv.mo11453a((C2698il) asv.duu().bHv().mo16779a(asv.dut().getClass(), asv.dut().mo5290Io(), (C2698il) this.iMy));
            asv.dut().mo5291a(asv.dhP());
        }
        asv.mo11461s(this.iMy.add(asv.dut().getTitle(), (Component) asv.dhP()));
        this.iMz.add(asv);
        if (asv.dus()) {
            this.iMy.setSelectedComponent(asv.duv());
        }
        if (this.iMA != null) {
            this.iMA.mo23280df();
        }
    }

    /* renamed from: b */
    public void mo23271b(C5685aSv asv) {
        this.iMy.remove(asv.duv());
        if (asv.duv() != null) {
            asv.duv().setVisible(false);
        }
        asv.dut().hide();
        this.iMz.remove(asv);
        duj();
    }

    public void dui() {
        for (C5685aSv b : this.iMz) {
            mo23271b(b);
        }
    }

    /* access modifiers changed from: private */
    public void duj() {
        for (C5685aSv next : this.iMz) {
            if (next.duv().equals(this.iMy.getSelectedComponent())) {
                next.dut().show();
                next.duw();
            } else {
                next.dut().hide();
            }
        }
    }

    /* renamed from: a */
    public void mo23270a(C4087a aVar) {
        this.iMA = aVar;
    }

    /* renamed from: a.zH$a */
    public interface C4087a {
        /* renamed from: df */
        void mo23280df();
    }

    /* renamed from: a.zH$b */
    /* compiled from: a */
    class C4088b implements ChangeListener {
        C4088b() {
        }

        public void stateChanged(ChangeEvent changeEvent) {
            C4086zH.this.duj();
        }
    }
}
