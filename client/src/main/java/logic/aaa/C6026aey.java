package logic.aaa;

import com.hoplon.geometry.Vec3f;
import p001a.C0709KF;

/* renamed from: a.aey  reason: case insensitive filesystem */
/* compiled from: a */
public class C6026aey {
    public final Vec3f dLX = new Vec3f();
    public final Vec3f dLY = new Vec3f();
    public final Vec3f fpD = new Vec3f();
    public final Vec3f fpE = new Vec3f();
    public final Vec3f fpF = new Vec3f();
    public final Vec3f fpG = new Vec3f();
    public final Vec3f fpH = new Vec3f();
    public final Vec3f fpI = new Vec3f();
    public float dMa = 0.0f;
    public float dMd = 0.0f;
    public float dMe = 0.0f;
    public float dMf = 0.0f;
    public float dMg = 0.0f;
    public float fpA;
    public float fpB;
    public int fpC = 0;
    public C0709KF fpJ = null;
    public C0709KF fpK = null;
    public float fpx = 0.0f;
    public float fpy = 0.0f;
    public float fpz = 0.0f;

    public void reset() {
        this.dMa = 0.0f;
        this.fpx = 0.0f;
        this.fpy = 0.0f;
        this.fpz = 0.0f;
        this.dMf = 0.0f;
        this.fpA = 0.0f;
        this.fpB = 0.0f;
        this.fpC = 0;
        this.dMe = 0.0f;
        this.dMd = 0.0f;
        this.dMg = 0.0f;
        this.fpD.set(0.0f, 0.0f, 0.0f);
        this.fpE.set(0.0f, 0.0f, 0.0f);
        this.fpF.set(0.0f, 0.0f, 0.0f);
        this.fpG.set(0.0f, 0.0f, 0.0f);
        this.fpH.set(0.0f, 0.0f, 0.0f);
        this.fpI.set(0.0f, 0.0f, 0.0f);
        this.dLX.set(0.0f, 0.0f, 0.0f);
        this.dLY.set(0.0f, 0.0f, 0.0f);
        this.fpJ = null;
        this.fpK = null;
    }
}
