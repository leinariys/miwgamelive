package logic.aaa;

import taikodom.addon.C6144ahM;

import java.io.ObjectInput;
import java.io.ObjectOutput;

/* renamed from: a.iJ */
/* compiled from: a */
public class C2667iJ extends C1506WA {


    /* renamed from: Zl */
    private final C6144ahM<Boolean> f8158Zl;

    public C2667iJ() {
        this((C6144ahM<Boolean>) null);
    }

    public C2667iJ(C6144ahM<Boolean> ahm) {
        this.f8158Zl = ahm;
    }

    /* renamed from: BJ */
    public C6144ahM<Boolean> mo19473BJ() {
        return this.f8158Zl;
    }

    public void readExternal(ObjectInput objectInput) {
        throw new UnsupportedOperationException("Do not use this class serverside");
    }

    public void writeExternal(ObjectOutput objectOutput) {
        throw new UnsupportedOperationException("Do not use this class serverside");
    }
}
