package logic.aaa;

import game.network.message.externalizable.C6128agw;
import taikodom.infra.script.I18NString;

import java.io.ObjectInput;
import java.io.ObjectOutput;

/* renamed from: a.Df */
/* compiled from: a */
public class C0284Df extends C6128agw {
    private I18NString auP;

    public C0284Df() {
    }

    public C0284Df(I18NString i18NString, boolean z, C6128agw.C1890a aVar, Object... objArr) {
        super(z, aVar, objArr);
        this.auP = i18NString;
    }

    public C0284Df(I18NString i18NString, boolean z, boolean z2, C6128agw.C1890a aVar, Object... objArr) {
        super(z, z2, aVar, objArr);
        this.auP = i18NString;
    }

    public C0284Df(I18NString i18NString, boolean z, Object... objArr) {
        super(z, C6128agw.C1890a.WHITE, objArr);
        this.auP = i18NString;
    }

    public C0284Df(I18NString i18NString, boolean z, boolean z2, Object... objArr) {
        super(z, z2, C6128agw.C1890a.WHITE, objArr);
        this.auP = i18NString;
    }

    public C0284Df(I18NString i18NString, boolean z, boolean z2, boolean z3, Object... objArr) {
        super(z, z2, z3, C6128agw.C1890a.WHITE, objArr);
        this.auP = i18NString;
    }

    public void readExternal(ObjectInput objectInput) {
        super.readExternal(objectInput);
        this.auP = (I18NString) objectInput.readObject();
    }

    public void writeExternal(ObjectOutput objectOutput) {
        super.writeExternal(objectOutput);
        objectOutput.writeObject(this.auP);
    }

    public String getMessage() {
        return this.auP.get();
    }
}
