package logic.aaa;

import p001a.C6274ajm;
import p001a.C6622aqW;

/* renamed from: a.aKu  reason: case insensitive filesystem */
/* compiled from: a */
public class C5476aKu extends C1506WA {

    private C6622aqW aiA = null;
    private C6274ajm aiB;
    private String identifier;
    private C1809a ioL;
    private boolean ioM = false;

    public C5476aKu(String str, C6622aqW aqw, C6274ajm ajm, boolean z) {
        this.aiB = ajm;
        this.ioL = C1809a.ADD;
        this.identifier = str;
        this.aiA = aqw;
        this.ioM = z;
    }

    public C5476aKu(C1809a aVar, String str) {
        this.ioL = aVar;
        this.identifier = str;
        this.aiB = null;
    }

    public String getIdentifier() {
        return this.identifier;
    }

    /* renamed from: ER */
    public C6622aqW mo9808ER() {
        return this.aiA;
    }

    public String getTitle() {
        return this.aiB == null ? "<NO TITLE>" : this.aiB.title;
    }

    public boolean djb() {
        return this.ioM;
    }

    public C1809a djc() {
        return this.ioL;
    }

    /* renamed from: f */
    public boolean mo9812f(C5476aKu aku) {
        if (this.aiB == null || aku == null) {
            return false;
        }
        return this.aiB.mo14175a(aku.djd());
    }

    public Object getObject() {
        if (this.aiB == null) {
            return null;
        }
        return this.aiB.object;
    }

    public C6274ajm djd() {
        return this.aiB;
    }

    /* renamed from: a.aKu$a */
    public enum C1809a {
        ADD,
        REMOVE,
        CLEAR
    }
}
