package logic.aaa;

import logic.ui.Panel;
import taikodom.addon.C2495fo;

/* renamed from: a.Nd */
/* compiled from: a */
public class C0929Nd extends C1506WA implements Comparable<C0929Nd> {


    /* renamed from: DV */
    private Panel f1249DV;
    private C2495fo apa;
    private int order;

    public C0929Nd(C2495fo foVar, Panel aco) {
        this(foVar, aco, 0);
    }

    public C0929Nd(C2495fo foVar, Panel aco, int i) {
        this.apa = foVar;
        this.f1249DV = aco;
        this.order = i;
    }

    public C2495fo bil() {
        return this.apa;
    }

    public Panel bim() {
        return this.f1249DV;
    }

    public int getOrder() {
        return this.order;
    }

    /* renamed from: a */
    public int compareTo(C0929Nd nd) {
        int i = this.order;
        int order2 = nd.getOrder();
        if (i < order2) {
            return -1;
        }
        return i == order2 ? 0 : 1;
    }
}
