package logic.aaa;

import game.network.message.externalizable.C6128agw;
import p001a.C1166RG;
import p001a.C1459VT;

import java.io.ObjectInput;
import java.io.ObjectOutput;
import java.util.Timer;
import java.util.TimerTask;

/* renamed from: a.Hy */
/* compiled from: a */
public class C0575Hy extends C2733jJ {
    /* access modifiers changed from: private */
    public C1459VT dbS;
    /* access modifiers changed from: private */
    public C1166RG dbT;
    /* access modifiers changed from: private */
    public int dbV;
    private int dbU;

    public C0575Hy() {
    }

    public C0575Hy(C1459VT vt, String str, int i, int i2) {
        this(vt, str, i, i2, C6128agw.C1890a.WHITE, new Object[0]);
    }

    public C0575Hy(C1459VT vt, String str, int i, int i2, C6128agw.C1890a aVar, Object... objArr) {
        super(str, false, true, aVar, objArr);
        this.dbU = i;
        this.dbV = i2;
        this.dbS = vt;
    }

    /* renamed from: a */
    public void mo2708a(C1166RG rg) {
        this.dbT = rg;
    }

    public void start() {
        new Timer(true).schedule(new C0576a(), (long) this.dbU, (long) this.dbV);
    }

    public void readExternal(ObjectInput objectInput) {
        throw new UnsupportedOperationException("Do not use this class serverside");
    }

    public void writeExternal(ObjectOutput objectOutput) {
        throw new UnsupportedOperationException("Do not use this class serverside");
    }

    /* renamed from: a.Hy$a */
    class C0576a extends TimerTask {
        C0576a() {
        }

        public void run() {
            C0575Hy.this.dbS.mo6145a(C0575Hy.this.dbV, C0575Hy.this.args);
            if (C0575Hy.this.dbT != null) {
                C0575Hy.this.dbT.mo5160a(C0575Hy.this.bWP(), new Object[0]);
            }
            if (C0575Hy.this.dbS.mo6146b(C0575Hy.this.args)) {
                if (C0575Hy.this.dbT != null) {
                    C0575Hy.this.dbT.mo5161b(C0575Hy.this.bWP(), C0575Hy.this.args);
                }
                cancel();
            }
        }
    }
}
