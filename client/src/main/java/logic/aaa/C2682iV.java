package logic.aaa;

import taikodom.addon.C6144ahM;

import java.io.ObjectInput;
import java.io.ObjectOutput;

/* renamed from: a.iV */
/* compiled from: a */
public class C2682iV extends C1506WA {


    /* renamed from: Zl */
    private final C6144ahM<Boolean> f8203Zl;

    public C2682iV() {
        this((C6144ahM<Boolean>) null);
    }

    public C2682iV(C6144ahM<Boolean> ahm) {
        this.f8203Zl = ahm;
    }

    /* renamed from: BJ */
    public C6144ahM<Boolean> mo19622BJ() {
        return this.f8203Zl;
    }

    public void readExternal(ObjectInput objectInput) {
        throw new UnsupportedOperationException("Do not use this class serverside");
    }

    public void writeExternal(ObjectOutput objectOutput) {
        throw new UnsupportedOperationException("Do not use this class serverside");
    }
}
