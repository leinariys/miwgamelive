package logic.aaa;

import logic.ui.C2698il;
import p001a.C1215Rv;
import taikodom.addon.IAddonProperties;

import java.awt.*;

/* renamed from: a.aSv  reason: case insensitive filesystem */
/* compiled from: a */
public class C5685aSv extends C1506WA {

    private final IAddonProperties fVq;
    private final String iMN;
    private final C1215Rv iMO;
    private final boolean iMQ;
    private C2698il aoL;
    private boolean dirty;
    private Component iMP;

    public C5685aSv(String str, IAddonProperties vWVar, C1215Rv rv) {
        this(str, vWVar, rv, false);
    }

    public C5685aSv(String str, IAddonProperties vWVar, C1215Rv rv, boolean z) {
        this.iMN = str;
        this.iMO = rv;
        this.iMQ = z;
        this.fVq = vWVar;
        this.dirty = true;
    }

    public String dur() {
        return this.iMN;
    }

    public boolean dus() {
        return this.iMQ;
    }

    public C1215Rv dut() {
        return this.iMO;
    }

    public IAddonProperties duu() {
        return this.fVq;
    }

    public Component duv() {
        return this.iMP;
    }

    /* renamed from: s */
    public void mo11461s(Component component) {
        this.iMP = component;
    }

    /* renamed from: a */
    public void mo11453a(C2698il ilVar) {
        this.aoL = ilVar;
    }

    public C2698il dhP() {
        return this.aoL;
    }

    public void setDirty(boolean z) {
        this.dirty = z;
    }

    public void duw() {
        if (this.dirty) {
            this.iMO.update();
            this.dirty = false;
        }
    }
}
