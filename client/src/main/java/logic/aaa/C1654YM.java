package logic.aaa;

import game.script.item.Amplifier;

/* renamed from: a.YM */
/* compiled from: a */
public class C1654YM extends C1506WA {
    private static final long serialVersionUID = 1041221092416070968L;
    private final Amplifier eMI;

    public C1654YM(Amplifier zXVar) {
        this.eMI = zXVar;
    }

    public Amplifier bHO() {
        return this.eMI;
    }
}
