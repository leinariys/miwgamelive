package logic.aaa;

import game.script.item.Item;
import game.script.ship.Station;

import java.util.List;

/* renamed from: a.Ns */
/* compiled from: a */
public class C0945Ns extends C1506WA {
    private Station ajx;
    private List<Item> iWt;
    private C0946a iWu;

    public C0945Ns(C0946a aVar, List<Item> list, Station bf) {
        this.iWu = aVar;
        this.iWt = list;
        this.ajx = bf;
    }

    public List<Item> dzw() {
        return this.iWt;
    }

    /* renamed from: Fl */
    public Station mo4275Fl() {
        return this.ajx;
    }

    public C0946a dzx() {
        return this.iWu;
    }

    /* renamed from: a.Ns$a */
    public enum C0946a {
        REWARD,
        TRANSPORT
    }
}
