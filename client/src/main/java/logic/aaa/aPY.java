package logic.aaa;

import game.script.progression.ProgressionCareer;

import java.io.ObjectInput;
import java.io.ObjectOutput;

/* renamed from: a.aPY */
/* compiled from: a */
public class aPY extends C1506WA {

    /* renamed from: BU */
    private ProgressionCareer f3532BU;

    public aPY() {
    }

    public aPY(ProgressionCareer sbVar) {
        this.f3532BU = sbVar;
    }

    /* renamed from: Rn */
    public ProgressionCareer mo10808Rn() {
        return this.f3532BU;
    }

    public void readExternal(ObjectInput objectInput) {
        throw new UnsupportedOperationException("Do not use this class serverside");
    }

    public void writeExternal(ObjectOutput objectOutput) {
        throw new UnsupportedOperationException("Do not use this class serverside");
    }
}
