package logic.aaa;

import game.network.message.externalizable.C6128agw;

import java.io.ObjectInput;
import java.io.ObjectOutput;

/* renamed from: a.jJ */
/* compiled from: a */
public class C2733jJ extends C6128agw {
    private String message;

    public C2733jJ() {
    }

    public C2733jJ(String str, boolean z, boolean z2, C6128agw.C1890a aVar, Object... objArr) {
        super(z, z2, aVar, objArr);
        this.message = str;
    }

    public C2733jJ(String str, boolean z, Object... objArr) {
        super(z, false, C6128agw.C1890a.WHITE, objArr);
        this.message = str;
    }

    public C2733jJ(String str, boolean z, boolean z2, Object... objArr) {
        super(z, z2, C6128agw.C1890a.WHITE, objArr);
        this.message = str;
    }

    public void readExternal(ObjectInput objectInput) {
        throw new UnsupportedOperationException("Do not use this class serverside");
    }

    public void writeExternal(ObjectOutput objectOutput) {
        throw new UnsupportedOperationException("Do not use this class serverside");
    }

    public String getMessage() {
        return this.message;
    }
}
