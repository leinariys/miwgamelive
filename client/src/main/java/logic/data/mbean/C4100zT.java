package logic.data.mbean;

import game.script.contract.ContractReward;
import game.script.contract.types.ContractBaseInfo;
import game.script.contract.types.ContractTemplate;
import game.script.player.Player;
import game.script.ship.Station;
import logic.baa.C1616Xf;
import logic.res.code.C5663aRz;
import p001a.C0064Am;

/* renamed from: a.zT */
public class C4100zT extends C3805vD {
    @C0064Am(aul = "6d977181ef0cc11282d050b011fd44b9", aum = 7)

    /* renamed from: Q */
    public long f9683Q;
    @C0064Am(aul = "6d977181ef0cc11282d050b011fd44b9", aum = -2)

    /* renamed from: ah */
    public long f9684ah;
    @C0064Am(aul = "6d977181ef0cc11282d050b011fd44b9", aum = -1)

    /* renamed from: aw */
    public byte f9685aw;
    @C0064Am(aul = "bb32faef6ee9eda4ecf4090e71b75650", aum = -1)
    public byte azD;
    @C0064Am(aul = "bb32faef6ee9eda4ecf4090e71b75650", aum = -2)
    public long azo;
    @C0064Am(aul = "400fe96dc79c9406848cf021f68c4f5e", aum = 0)
    public ContractBaseInfo cbY;
    @C0064Am(aul = "d78341e00307e19a768b76c0438d0686", aum = 1)
    public ContractTemplate.C0104a cbZ;
    @C0064Am(aul = "5af2bb598c01386cee1d449bafa937e9", aum = 2)
    public Station cca;
    @C0064Am(aul = "bb32faef6ee9eda4ecf4090e71b75650", aum = 3)
    public ContractReward ccb;
    @C0064Am(aul = "adde3baeccb855786c6162eaf197599d", aum = 4)
    public long ccc;
    @C0064Am(aul = "ec4dfc420a31021b3df90f35433c519f", aum = 5)
    public Player ccd;
    @C0064Am(aul = "6a91467aaec640d7534650310199ef82", aum = 6)
    public Player cce;
    @C0064Am(aul = "2950f3f97b67a891c2166fa9c93d9a6a", aum = 8)
    public long ccf;
    @C0064Am(aul = "400fe96dc79c9406848cf021f68c4f5e", aum = -2)
    public long ccg;
    @C0064Am(aul = "5af2bb598c01386cee1d449bafa937e9", aum = -2)
    public long cch;
    @C0064Am(aul = "adde3baeccb855786c6162eaf197599d", aum = -2)
    public long cci;
    @C0064Am(aul = "ec4dfc420a31021b3df90f35433c519f", aum = -2)
    public long ccj;
    @C0064Am(aul = "6a91467aaec640d7534650310199ef82", aum = -2)
    public long cck;
    @C0064Am(aul = "2950f3f97b67a891c2166fa9c93d9a6a", aum = -2)
    public long ccl;
    @C0064Am(aul = "400fe96dc79c9406848cf021f68c4f5e", aum = -1)
    public byte ccm;
    @C0064Am(aul = "5af2bb598c01386cee1d449bafa937e9", aum = -1)
    public byte ccn;
    @C0064Am(aul = "adde3baeccb855786c6162eaf197599d", aum = -1)
    public byte cco;
    @C0064Am(aul = "ec4dfc420a31021b3df90f35433c519f", aum = -1)
    public byte ccp;
    @C0064Am(aul = "6a91467aaec640d7534650310199ef82", aum = -1)
    public byte ccq;
    @C0064Am(aul = "2950f3f97b67a891c2166fa9c93d9a6a", aum = -1)
    public byte ccr;
    @C0064Am(aul = "d78341e00307e19a768b76c0438d0686", aum = -2)

    /* renamed from: ol */
    public long f9686ol;
    @C0064Am(aul = "d78341e00307e19a768b76c0438d0686", aum = -1)

    /* renamed from: oo */
    public byte f9687oo;

    public C4100zT() {
    }

    public C4100zT(C1616Xf xf) {
        super(xf);
    }

    /* renamed from: c */
    public C5663aRz[] mo309c() {
        return ContractTemplate._m_fields;
    }

    /* renamed from: d */
    public void mo310d() {
        super.mo310d();
        this.cbY = null;
        this.cbZ = null;
        this.cca = null;
        this.ccb = null;
        this.ccc = 0;
        this.ccd = null;
        this.cce = null;
        this.f9683Q = 0;
        this.ccf = 0;
        this.ccg = 0;
        this.f9686ol = 0;
        this.cch = 0;
        this.azo = 0;
        this.cci = 0;
        this.ccj = 0;
        this.cck = 0;
        this.f9684ah = 0;
        this.ccl = 0;
        this.ccm = 0;
        this.f9687oo = 0;
        this.ccn = 0;
        this.azD = 0;
        this.cco = 0;
        this.ccp = 0;
        this.ccq = 0;
        this.f9685aw = 0;
        this.ccr = 0;
    }
}
