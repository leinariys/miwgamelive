package logic.data.mbean;

import com.hoplon.geometry.Color;
import logic.baa.C1616Xf;
import logic.res.code.C5663aRz;
import p001a.C0064Am;

import java.util.UUID;

/* renamed from: a.aFL */
public class aFL extends C3805vD {
    @C0064Am(aul = "3cf331e5090869a6f0ef4a3c68f6fe26", aum = -1)
    public byte azF;
    @C0064Am(aul = "3cf331e5090869a6f0ef4a3c68f6fe26", aum = -2)
    public long azq;
    @C0064Am(aul = "97d98d2d097450f00be77bed4d982868", aum = 24)

    /* renamed from: bK */
    public UUID f2795bK;
    @C0064Am(aul = "c8f2b0da9c040be62f51f3acb0cae9c7", aum = 0)
    public Color cfA;
    @C0064Am(aul = "3cf331e5090869a6f0ef4a3c68f6fe26", aum = 1)
    public Color cfC;
    @C0064Am(aul = "01f43b227ffb5f9048a5a8c241d37a5f", aum = 2)
    public Color cfE;
    @C0064Am(aul = "fd623bc36083365ae1277760ea90202c", aum = 3)
    public Color cfG;
    @C0064Am(aul = "db031006b6545d0dd1219e87e29a103b", aum = 4)
    public Color cfI;
    @C0064Am(aul = "579ff1d900ed5c66a0b131af4485f914", aum = 5)
    public Color cfK;
    @C0064Am(aul = "def1dc5b2939ab38aa2bdbfc17fea864", aum = 6)
    public Color cfM;
    @C0064Am(aul = "b638a910f309a20b3b6a4dc0bba3dabc", aum = 7)
    public Color cfO;
    @C0064Am(aul = "93c72e0af3efc25cd186d48343162d44", aum = 8)
    public Color cfQ;
    @C0064Am(aul = "e99f6892cdeef2f8673c841ea537b4f6", aum = 9)
    public Color cfS;
    @C0064Am(aul = "eb3e26797586193090b847eeb8c0c904", aum = 10)
    public Color cfU;
    @C0064Am(aul = "f1893203b55ad43b9ec43b5fa9d9710d", aum = 11)
    public Color cfW;
    @C0064Am(aul = "fb2e7f586ba6116e554f116a0e79124a", aum = 12)
    public String cfY;
    @C0064Am(aul = "2679a88635b5a9f1d840c22a024e5054", aum = 13)
    public String cga;
    @C0064Am(aul = "94a8b48f53b8c8409f672116161b562d", aum = 14)
    public String cgc;
    @C0064Am(aul = "7bb38c91aa42be2db793850a52ede3f8", aum = 15)
    public String cge;
    @C0064Am(aul = "4e61a5e07ad1ea71f0d5604a634131ce", aum = 16)
    public String cgg;
    @C0064Am(aul = "b2978c4c86110f6f2a95046beea5bc9c", aum = 17)
    public String cgi;
    @C0064Am(aul = "b283bb724b3dabe7c4812e39f863bdef", aum = 18)
    public String cgk;
    @C0064Am(aul = "e9f4ca8355b64f79fa5a66bfbc9e2aaa", aum = 19)
    public String cgm;
    @C0064Am(aul = "6cf057631707326507b9ccdf5d5df37f", aum = 20)
    public String cgo;
    @C0064Am(aul = "d7b0d22f3c361f0425ab2b9404493957", aum = 21)
    public String cgq;
    @C0064Am(aul = "0628116bf0908b536b76c58a32218749", aum = 22)
    public String cgs;
    @C0064Am(aul = "0c1ed47794370d4b3f51c95b1b838638", aum = 23)
    public String cgu;
    @C0064Am(aul = "c8f2b0da9c040be62f51f3acb0cae9c7", aum = -2)
    public long hJT;
    @C0064Am(aul = "01f43b227ffb5f9048a5a8c241d37a5f", aum = -2)
    public long hJU;
    @C0064Am(aul = "fd623bc36083365ae1277760ea90202c", aum = -2)
    public long hJV;
    @C0064Am(aul = "db031006b6545d0dd1219e87e29a103b", aum = -2)
    public long hJW;
    @C0064Am(aul = "579ff1d900ed5c66a0b131af4485f914", aum = -2)
    public long hJX;
    @C0064Am(aul = "def1dc5b2939ab38aa2bdbfc17fea864", aum = -2)
    public long hJY;
    @C0064Am(aul = "b638a910f309a20b3b6a4dc0bba3dabc", aum = -2)
    public long hJZ;
    @C0064Am(aul = "f1893203b55ad43b9ec43b5fa9d9710d", aum = -1)
    public byte hKA;
    @C0064Am(aul = "fb2e7f586ba6116e554f116a0e79124a", aum = -1)
    public byte hKB;
    @C0064Am(aul = "2679a88635b5a9f1d840c22a024e5054", aum = -1)
    public byte hKC;
    @C0064Am(aul = "94a8b48f53b8c8409f672116161b562d", aum = -1)
    public byte hKD;
    @C0064Am(aul = "7bb38c91aa42be2db793850a52ede3f8", aum = -1)
    public byte hKE;
    @C0064Am(aul = "4e61a5e07ad1ea71f0d5604a634131ce", aum = -1)
    public byte hKF;
    @C0064Am(aul = "b2978c4c86110f6f2a95046beea5bc9c", aum = -1)
    public byte hKG;
    @C0064Am(aul = "b283bb724b3dabe7c4812e39f863bdef", aum = -1)
    public byte hKH;
    @C0064Am(aul = "e9f4ca8355b64f79fa5a66bfbc9e2aaa", aum = -1)
    public byte hKI;
    @C0064Am(aul = "6cf057631707326507b9ccdf5d5df37f", aum = -1)
    public byte hKJ;
    @C0064Am(aul = "d7b0d22f3c361f0425ab2b9404493957", aum = -1)
    public byte hKK;
    @C0064Am(aul = "0628116bf0908b536b76c58a32218749", aum = -1)
    public byte hKL;
    @C0064Am(aul = "0c1ed47794370d4b3f51c95b1b838638", aum = -1)
    public byte hKM;
    @C0064Am(aul = "93c72e0af3efc25cd186d48343162d44", aum = -2)
    public long hKa;
    @C0064Am(aul = "e99f6892cdeef2f8673c841ea537b4f6", aum = -2)
    public long hKb;
    @C0064Am(aul = "eb3e26797586193090b847eeb8c0c904", aum = -2)
    public long hKc;
    @C0064Am(aul = "f1893203b55ad43b9ec43b5fa9d9710d", aum = -2)
    public long hKd;
    @C0064Am(aul = "fb2e7f586ba6116e554f116a0e79124a", aum = -2)
    public long hKe;
    @C0064Am(aul = "2679a88635b5a9f1d840c22a024e5054", aum = -2)
    public long hKf;
    @C0064Am(aul = "94a8b48f53b8c8409f672116161b562d", aum = -2)
    public long hKg;
    @C0064Am(aul = "7bb38c91aa42be2db793850a52ede3f8", aum = -2)
    public long hKh;
    @C0064Am(aul = "4e61a5e07ad1ea71f0d5604a634131ce", aum = -2)
    public long hKi;
    @C0064Am(aul = "b2978c4c86110f6f2a95046beea5bc9c", aum = -2)
    public long hKj;
    @C0064Am(aul = "b283bb724b3dabe7c4812e39f863bdef", aum = -2)
    public long hKk;
    @C0064Am(aul = "e9f4ca8355b64f79fa5a66bfbc9e2aaa", aum = -2)
    public long hKl;
    @C0064Am(aul = "6cf057631707326507b9ccdf5d5df37f", aum = -2)
    public long hKm;
    @C0064Am(aul = "d7b0d22f3c361f0425ab2b9404493957", aum = -2)
    public long hKn;
    @C0064Am(aul = "0628116bf0908b536b76c58a32218749", aum = -2)
    public long hKo;
    @C0064Am(aul = "0c1ed47794370d4b3f51c95b1b838638", aum = -2)
    public long hKp;
    @C0064Am(aul = "c8f2b0da9c040be62f51f3acb0cae9c7", aum = -1)
    public byte hKq;
    @C0064Am(aul = "01f43b227ffb5f9048a5a8c241d37a5f", aum = -1)
    public byte hKr;
    @C0064Am(aul = "fd623bc36083365ae1277760ea90202c", aum = -1)
    public byte hKs;
    @C0064Am(aul = "db031006b6545d0dd1219e87e29a103b", aum = -1)
    public byte hKt;
    @C0064Am(aul = "579ff1d900ed5c66a0b131af4485f914", aum = -1)
    public byte hKu;
    @C0064Am(aul = "def1dc5b2939ab38aa2bdbfc17fea864", aum = -1)
    public byte hKv;
    @C0064Am(aul = "b638a910f309a20b3b6a4dc0bba3dabc", aum = -1)
    public byte hKw;
    @C0064Am(aul = "93c72e0af3efc25cd186d48343162d44", aum = -1)
    public byte hKx;
    @C0064Am(aul = "e99f6892cdeef2f8673c841ea537b4f6", aum = -1)
    public byte hKy;
    @C0064Am(aul = "eb3e26797586193090b847eeb8c0c904", aum = -1)
    public byte hKz;
    @C0064Am(aul = "97d98d2d097450f00be77bed4d982868", aum = -2)

    /* renamed from: oL */
    public long f2796oL;
    @C0064Am(aul = "97d98d2d097450f00be77bed4d982868", aum = -1)

    /* renamed from: oS */
    public byte f2797oS;

    public aFL() {
    }

    public aFL(C1616Xf xf) {
        super(xf);
    }

    /* renamed from: c */
    public C5663aRz[] mo309c() {
        return MarkManager._m_fields;
    }

    /* renamed from: d */
    public void mo310d() {
        super.mo310d();
        this.cfA = null;
        this.cfC = null;
        this.cfE = null;
        this.cfG = null;
        this.cfI = null;
        this.cfK = null;
        this.cfM = null;
        this.cfO = null;
        this.cfQ = null;
        this.cfS = null;
        this.cfU = null;
        this.cfW = null;
        this.cfY = null;
        this.cga = null;
        this.cgc = null;
        this.cge = null;
        this.cgg = null;
        this.cgi = null;
        this.cgk = null;
        this.cgm = null;
        this.cgo = null;
        this.cgq = null;
        this.cgs = null;
        this.cgu = null;
        this.f2795bK = null;
        this.hJT = 0;
        this.azq = 0;
        this.hJU = 0;
        this.hJV = 0;
        this.hJW = 0;
        this.hJX = 0;
        this.hJY = 0;
        this.hJZ = 0;
        this.hKa = 0;
        this.hKb = 0;
        this.hKc = 0;
        this.hKd = 0;
        this.hKe = 0;
        this.hKf = 0;
        this.hKg = 0;
        this.hKh = 0;
        this.hKi = 0;
        this.hKj = 0;
        this.hKk = 0;
        this.hKl = 0;
        this.hKm = 0;
        this.hKn = 0;
        this.hKo = 0;
        this.hKp = 0;
        this.f2796oL = 0;
        this.hKq = 0;
        this.azF = 0;
        this.hKr = 0;
        this.hKs = 0;
        this.hKt = 0;
        this.hKu = 0;
        this.hKv = 0;
        this.hKw = 0;
        this.hKx = 0;
        this.hKy = 0;
        this.hKz = 0;
        this.hKA = 0;
        this.hKB = 0;
        this.hKC = 0;
        this.hKD = 0;
        this.hKE = 0;
        this.hKF = 0;
        this.hKG = 0;
        this.hKH = 0;
        this.hKI = 0;
        this.hKJ = 0;
        this.hKK = 0;
        this.hKL = 0;
        this.hKM = 0;
        this.f2797oS = 0;
    }
}
