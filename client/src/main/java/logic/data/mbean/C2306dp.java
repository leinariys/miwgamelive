package logic.data.mbean;

import game.script.resource.Asset;
import logic.baa.C1616Xf;
import logic.res.code.C5663aRz;
import p001a.C0064Am;

import java.util.UUID;

/* renamed from: a.dp */
public class C2306dp extends C3805vD {
    @C0064Am(aul = "52b09501f97d6963c27394a6a834afd8", aum = 0)

    /* renamed from: bK */
    public UUID f6623bK;
    @C0064Am(aul = "44fbbf9a20076b409d8dad802551abc8", aum = 2)
    public String file;
    @C0064Am(aul = "d327065d22c44b426792549a808a385d", aum = 1)
    public String handle;
    @C0064Am(aul = "52b09501f97d6963c27394a6a834afd8", aum = -2)

    /* renamed from: oL */
    public long f6624oL;
    @C0064Am(aul = "52b09501f97d6963c27394a6a834afd8", aum = -1)

    /* renamed from: oS */
    public byte f6625oS;
    @C0064Am(aul = "d327065d22c44b426792549a808a385d", aum = -2)

    /* renamed from: ok */
    public long f6626ok;
    @C0064Am(aul = "d327065d22c44b426792549a808a385d", aum = -1)

    /* renamed from: on */
    public byte f6627on;
    @C0064Am(aul = "44fbbf9a20076b409d8dad802551abc8", aum = -2)

    /* renamed from: zb */
    public long f6628zb;
    @C0064Am(aul = "44fbbf9a20076b409d8dad802551abc8", aum = -1)

    /* renamed from: zc */
    public byte f6629zc;

    public C2306dp() {
    }

    public C2306dp(C1616Xf xf) {
        super(xf);
    }

    /* renamed from: c */
    public C5663aRz[] mo309c() {
        return Asset._m_fields;
    }

    /* renamed from: d */
    public void mo310d() {
        super.mo310d();
        this.f6623bK = null;
        this.handle = null;
        this.file = null;
        this.f6624oL = 0;
        this.f6626ok = 0;
        this.f6628zb = 0;
        this.f6625oS = 0;
        this.f6627on = 0;
        this.f6629zc = 0;
    }
}
