package logic.data.mbean;

import game.script.item.ClipBoxType;
import game.script.item.ClipType;
import logic.baa.C1616Xf;
import logic.res.code.C5663aRz;
import p001a.C0064Am;

/* renamed from: a.alx  reason: case insensitive filesystem */
public class C6389alx extends C7034azv {
    @C0064Am(aul = "526bdb6be6a0ff13f97a75ff8bf5c45f", aum = 0)
    public int aVH;
    @C0064Am(aul = "e1481ad19c7b505f3d0d306884873ac5", aum = 1)
    public ClipType aVI;
    @C0064Am(aul = "526bdb6be6a0ff13f97a75ff8bf5c45f", aum = -2)
    public long aVJ;
    @C0064Am(aul = "e1481ad19c7b505f3d0d306884873ac5", aum = -2)
    public long aVK;
    @C0064Am(aul = "526bdb6be6a0ff13f97a75ff8bf5c45f", aum = -1)
    public byte aVL;
    @C0064Am(aul = "e1481ad19c7b505f3d0d306884873ac5", aum = -1)
    public byte aVM;

    public C6389alx() {
    }

    public C6389alx(C1616Xf xf) {
        super(xf);
    }

    /* renamed from: c */
    public C5663aRz[] mo309c() {
        return ClipBoxType._m_fields;
    }

    /* renamed from: d */
    public void mo310d() {
        super.mo310d();
        this.aVH = 0;
        this.aVI = null;
        this.aVJ = 0;
        this.aVK = 0;
        this.aVL = 0;
        this.aVM = 0;
    }
}
