package logic.data.mbean;

import game.script.item.durability.ItemDurability;
import logic.baa.C1616Xf;
import logic.res.code.C5663aRz;
import p001a.C0064Am;

/* renamed from: a.jQ */
public class C2745jQ extends C5292aDs {
    @C0064Am(aul = "fba7e3287637825e9ae9468f5363dae6", aum = -1)
    public byte apA;
    @C0064Am(aul = "87e0914360eaa6771e2a52d5d00d2819", aum = 0)
    public long apo;
    @C0064Am(aul = "f2a74a412f16e6141835c0ca915d5aea", aum = 3)
    public boolean app;
    @C0064Am(aul = "fba7e3287637825e9ae9468f5363dae6", aum = 4)
    public boolean apq;
    @C0064Am(aul = "87e0914360eaa6771e2a52d5d00d2819", aum = -2)
    public long apr;
    @C0064Am(aul = "6b8ad227ef328e2dc29c3b65d5ecf1b1", aum = -2)
    public long aps;
    @C0064Am(aul = "73a0e9cd986152b71656cda4949a71a9", aum = -2)
    public long apt;
    @C0064Am(aul = "f2a74a412f16e6141835c0ca915d5aea", aum = -2)
    public long apu;
    @C0064Am(aul = "fba7e3287637825e9ae9468f5363dae6", aum = -2)
    public long apv;
    @C0064Am(aul = "87e0914360eaa6771e2a52d5d00d2819", aum = -1)
    public byte apw;
    @C0064Am(aul = "6b8ad227ef328e2dc29c3b65d5ecf1b1", aum = -1)
    public byte apx;
    @C0064Am(aul = "73a0e9cd986152b71656cda4949a71a9", aum = -1)
    public byte apy;
    @C0064Am(aul = "f2a74a412f16e6141835c0ca915d5aea", aum = -1)
    public byte apz;
    @C0064Am(aul = "6b8ad227ef328e2dc29c3b65d5ecf1b1", aum = 1)
    public long startTime;
    @C0064Am(aul = "73a0e9cd986152b71656cda4949a71a9", aum = 2)
    public boolean started;

    public C2745jQ() {
    }

    public C2745jQ(C1616Xf xf) {
        super(xf);
    }

    /* renamed from: c */
    public C5663aRz[] mo309c() {
        return ItemDurability._m_fields;
    }

    /* renamed from: d */
    public void mo310d() {
        super.mo310d();
        this.apo = 0;
        this.startTime = 0;
        this.started = false;
        this.app = false;
        this.apq = false;
        this.apr = 0;
        this.aps = 0;
        this.apt = 0;
        this.apu = 0;
        this.apv = 0;
        this.apw = 0;
        this.apx = 0;
        this.apy = 0;
        this.apz = 0;
        this.apA = 0;
    }
}
