package logic.data.mbean;

import game.network.message.serializable.C1556Wo;
import game.script.item.RawMaterialType;
import game.script.progression.CharacterEvolution;
import game.script.progression.CharacterProgression;
import game.script.progression.ProgressionCareer;
import game.script.space.LootItems;
import logic.baa.C1616Xf;
import logic.res.code.C5663aRz;
import p001a.C0064Am;

import java.util.List;

/* renamed from: a.FE */
public class C0379FE extends C3805vD {
    @C0064Am(aul = "dbe553999407b8f280e3d490b55111be", aum = 0)

    /* renamed from: BO */
    public int f526BO;
    @C0064Am(aul = "7dd1faa63837a56ad8af8b39887836b1", aum = 1)

    /* renamed from: BQ */
    public long f527BQ;
    @C0064Am(aul = "b0781ac42ed6319a1e31106db078228f", aum = 2)

    /* renamed from: BS */
    public int f528BS;
    @C0064Am(aul = "35f4823bd662a858d3401e6411ea876b", aum = 3)

    /* renamed from: BU */
    public ProgressionCareer f529BU;
    @C0064Am(aul = "1c1a97eaf6bc29d2953f8bff49be7abb", aum = 5)

    /* renamed from: BY */
    public C1556Wo<LootItems, List<RawMaterialType>> f530BY;
    @C0064Am(aul = "dbe553999407b8f280e3d490b55111be", aum = -1)
    public byte azE;
    @C0064Am(aul = "dbe553999407b8f280e3d490b55111be", aum = -2)
    public long azp;
    @C0064Am(aul = "b0781ac42ed6319a1e31106db078228f", aum = -2)
    public long cVS;
    @C0064Am(aul = "1c1a97eaf6bc29d2953f8bff49be7abb", aum = -2)
    public long cVT;
    @C0064Am(aul = "b0781ac42ed6319a1e31106db078228f", aum = -1)
    public byte cVU;
    @C0064Am(aul = "1c1a97eaf6bc29d2953f8bff49be7abb", aum = -1)
    public byte cVV;
    @C0064Am(aul = "35f4823bd662a858d3401e6411ea876b", aum = -2)
    public long cye;
    @C0064Am(aul = "35f4823bd662a858d3401e6411ea876b", aum = -1)
    public byte cyg;
    @C0064Am(aul = "4fe4f6c697dd716c75318312beffa45d", aum = -2)

    /* renamed from: jA */
    public long f531jA;
    @C0064Am(aul = "4fe4f6c697dd716c75318312beffa45d", aum = -1)

    /* renamed from: jM */
    public byte f532jM;
    @C0064Am(aul = "4fe4f6c697dd716c75318312beffa45d", aum = 4)

    /* renamed from: jp */
    public CharacterProgression f533jp;
    @C0064Am(aul = "7dd1faa63837a56ad8af8b39887836b1", aum = -2)

    /* renamed from: kb */
    public long f534kb;
    @C0064Am(aul = "7dd1faa63837a56ad8af8b39887836b1", aum = -1)

    /* renamed from: kg */
    public byte f535kg;

    public C0379FE() {
    }

    public C0379FE(C1616Xf xf) {
        super(xf);
    }

    /* renamed from: c */
    public C5663aRz[] mo309c() {
        return CharacterEvolution._m_fields;
    }

    /* renamed from: d */
    public void mo310d() {
        super.mo310d();
        this.f526BO = 0;
        this.f527BQ = 0;
        this.f528BS = 0;
        this.f529BU = null;
        this.f533jp = null;
        this.f530BY = null;
        this.azp = 0;
        this.f534kb = 0;
        this.cVS = 0;
        this.cye = 0;
        this.f531jA = 0;
        this.cVT = 0;
        this.azE = 0;
        this.f535kg = 0;
        this.cVU = 0;
        this.cyg = 0;
        this.f532jM = 0;
        this.cVV = 0;
    }
}
