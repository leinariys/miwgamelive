package logic.data.mbean;

import game.network.message.serializable.C1556Wo;
import game.network.message.serializable.C2686iZ;
import game.script.damage.DamageType;
import game.script.ship.Hull;
import game.script.ship.HullAdapter;
import game.script.ship.Ship;
import game.script.util.AdapterLikedList;
import logic.baa.C1616Xf;
import logic.res.code.C5663aRz;
import p001a.C0064Am;
import p001a.C1649YI;

/* renamed from: a.aaI  reason: case insensitive filesystem */
public class C5776aaI extends C3805vD {
    @C0064Am(aul = "d36a5e54dcc50c1b2e3ae4dcaa8b4587", aum = -2)
    public long aBO;
    @C0064Am(aul = "d36a5e54dcc50c1b2e3ae4dcaa8b4587", aum = -1)
    public byte aBT;
    @C0064Am(aul = "69fee834371829196a17321b92abe328", aum = 4)
    public float aOA;
    @C0064Am(aul = "674c314385d57bed677ef90b3d1719eb", aum = 5)
    public C1556Wo<DamageType, Float> aOG;
    @C0064Am(aul = "d36a5e54dcc50c1b2e3ae4dcaa8b4587", aum = 6)
    public float aOI;
    @C0064Am(aul = "b9294ace7cc3d0badeaee4070f91a436", aum = 7)
    public Ship aOO;
    @C0064Am(aul = "a9cc0e2cbdc56a1b581816ca22a675d2", aum = 2)
    public float aOo;
    @C0064Am(aul = "58244a993453be22a790683bf5431ac3", aum = 0)
    public C1556Wo<DamageType, C1649YI> aOu;
    @C0064Am(aul = "5b1be121c46f0a0e1b56d0ee66a068d5", aum = 3)
    public AdapterLikedList<HullAdapter> aOw;
    @C0064Am(aul = "5b1be121c46f0a0e1b56d0ee66a068d5", aum = -2)
    public long aUE;
    @C0064Am(aul = "5b1be121c46f0a0e1b56d0ee66a068d5", aum = -1)
    public byte aUH;
    @C0064Am(aul = "e97448476ae488e5379338afff0a2839", aum = 1)
    public C2686iZ<DamageType> bEB;
    @C0064Am(aul = "b9294ace7cc3d0badeaee4070f91a436", aum = -2)
    public long cSU;
    @C0064Am(aul = "b9294ace7cc3d0badeaee4070f91a436", aum = -1)
    public byte cSW;
    @C0064Am(aul = "58244a993453be22a790683bf5431ac3", aum = -2)
    public long eVK;
    @C0064Am(aul = "e97448476ae488e5379338afff0a2839", aum = -2)
    public long eVL;
    @C0064Am(aul = "a9cc0e2cbdc56a1b581816ca22a675d2", aum = -2)
    public long eVM;
    @C0064Am(aul = "69fee834371829196a17321b92abe328", aum = -2)
    public long eVN;
    @C0064Am(aul = "674c314385d57bed677ef90b3d1719eb", aum = -2)
    public long eVO;
    @C0064Am(aul = "58244a993453be22a790683bf5431ac3", aum = -1)
    public byte eVP;
    @C0064Am(aul = "e97448476ae488e5379338afff0a2839", aum = -1)
    public byte eVQ;
    @C0064Am(aul = "a9cc0e2cbdc56a1b581816ca22a675d2", aum = -1)
    public byte eVR;
    @C0064Am(aul = "69fee834371829196a17321b92abe328", aum = -1)
    public byte eVS;
    @C0064Am(aul = "674c314385d57bed677ef90b3d1719eb", aum = -1)
    public byte eVT;

    public C5776aaI() {
    }

    public C5776aaI(C1616Xf xf) {
        super(xf);
    }

    /* renamed from: c */
    public C5663aRz[] mo309c() {
        return Hull._m_fields;
    }

    /* renamed from: d */
    public void mo310d() {
        super.mo310d();
        this.aOu = null;
        this.bEB = null;
        this.aOo = 0.0f;
        this.aOw = null;
        this.aOA = 0.0f;
        this.aOG = null;
        this.aOI = 0.0f;
        this.aOO = null;
        this.eVK = 0;
        this.eVL = 0;
        this.eVM = 0;
        this.aUE = 0;
        this.eVN = 0;
        this.eVO = 0;
        this.aBO = 0;
        this.cSU = 0;
        this.eVP = 0;
        this.eVQ = 0;
        this.eVR = 0;
        this.aUH = 0;
        this.eVS = 0;
        this.eVT = 0;
        this.aBT = 0;
        this.cSW = 0;
    }
}
