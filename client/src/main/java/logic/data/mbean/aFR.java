package logic.data.mbean;

import game.script.item.ItemType;
import game.script.newmarket.economy.VirtualWarehouse;
import game.script.newmarket.economy.WarehouseInventoryItem;
import logic.baa.C1616Xf;
import logic.res.code.C5663aRz;
import p001a.C0064Am;

/* renamed from: a.aFR */
public class aFR extends C5292aDs {
    @C0064Am(aul = "9bbd5edd13b89f5b4b9cbd9cc93ac2e0", aum = 0)

    /* renamed from: MQ */
    public ItemType f2810MQ;
    @C0064Am(aul = "9fc6d39e3cbe4e7f9fe60224f5b3e079", aum = 1)

    /* renamed from: MR */
    public long f2811MR;
    @C0064Am(aul = "73b3c0b24d39fa98cf4b0e0c8023f86e", aum = 3)

    /* renamed from: MU */
    public VirtualWarehouse f2812MU;
    @C0064Am(aul = "a11965f955d6dbd414b8750abe694eae", aum = 2)

    /* renamed from: db */
    public long f2813db;
    @C0064Am(aul = "9bbd5edd13b89f5b4b9cbd9cc93ac2e0", aum = -2)

    /* renamed from: df */
    public long f2814df;
    @C0064Am(aul = "a11965f955d6dbd414b8750abe694eae", aum = -2)

    /* renamed from: dj */
    public long f2815dj;
    @C0064Am(aul = "9bbd5edd13b89f5b4b9cbd9cc93ac2e0", aum = -1)

    /* renamed from: dm */
    public byte f2816dm;
    @C0064Am(aul = "a11965f955d6dbd414b8750abe694eae", aum = -1)

    /* renamed from: dq */
    public byte f2817dq;
    @C0064Am(aul = "9fc6d39e3cbe4e7f9fe60224f5b3e079", aum = -2)
    public long gsi;
    @C0064Am(aul = "9fc6d39e3cbe4e7f9fe60224f5b3e079", aum = -1)
    public byte gsk;
    @C0064Am(aul = "73b3c0b24d39fa98cf4b0e0c8023f86e", aum = -2)
    public long hNF;
    @C0064Am(aul = "73b3c0b24d39fa98cf4b0e0c8023f86e", aum = -1)
    public byte hNG;

    public aFR() {
    }

    public aFR(C1616Xf xf) {
        super(xf);
    }

    /* renamed from: c */
    public C5663aRz[] mo309c() {
        return WarehouseInventoryItem._m_fields;
    }

    /* renamed from: d */
    public void mo310d() {
        super.mo310d();
        this.f2810MQ = null;
        this.f2811MR = 0;
        this.f2813db = 0;
        this.f2812MU = null;
        this.f2814df = 0;
        this.gsi = 0;
        this.f2815dj = 0;
        this.hNF = 0;
        this.f2816dm = 0;
        this.gsk = 0;
        this.f2817dq = 0;
        this.hNG = 0;
    }
}
