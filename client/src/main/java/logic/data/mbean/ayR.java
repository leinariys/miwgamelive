package logic.data.mbean;

import game.script.item.ComponentType;
import game.script.resource.Asset;
import game.script.ship.SectorCategory;
import game.script.ship.Slot;
import logic.baa.C1616Xf;
import logic.res.code.C5663aRz;
import p001a.C0064Am;

/* renamed from: a.ayR */
public class ayR extends C5642aRe {
    @C0064Am(aul = "b108efa1c230e6231b8a836400b0180b", aum = -2)
    public long bwn;
    @C0064Am(aul = "b108efa1c230e6231b8a836400b0180b", aum = -1)
    public byte bwt;
    @C0064Am(aul = "b108efa1c230e6231b8a836400b0180b", aum = 0)
    public SectorCategory cYq;
    @C0064Am(aul = "0c963138aa468449e30970828a51f245", aum = 1)
    public Slot.C2893a cYr;
    @C0064Am(aul = "7a543ee56aec5700ea9ebce71bb5717c", aum = 2)
    public Asset cYs;
    @C0064Am(aul = "0c963138aa468449e30970828a51f245", aum = -2)
    public long cYt;
    @C0064Am(aul = "7a543ee56aec5700ea9ebce71bb5717c", aum = -2)
    public long cYu;
    @C0064Am(aul = "0c963138aa468449e30970828a51f245", aum = -1)
    public byte cYv;
    @C0064Am(aul = "7a543ee56aec5700ea9ebce71bb5717c", aum = -1)
    public byte cYw;

    public ayR() {
    }

    public ayR(C1616Xf xf) {
        super(xf);
    }

    /* renamed from: c */
    public C5663aRz[] mo309c() {
        return ComponentType._m_fields;
    }

    /* renamed from: d */
    public void mo310d() {
        super.mo310d();
        this.cYq = null;
        this.cYr = null;
        this.cYs = null;
        this.bwn = 0;
        this.cYt = 0;
        this.cYu = 0;
        this.bwt = 0;
        this.cYv = 0;
        this.cYw = 0;
    }
}
