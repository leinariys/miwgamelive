package logic.data.mbean;

import game.network.message.serializable.C3438ra;
import game.script.nls.NLSResourceLoader;
import game.view.TipString;
import logic.baa.C1616Xf;
import logic.res.code.C5663aRz;
import p001a.C0064Am;
import taikodom.infra.script.I18NString;

/* renamed from: a.BX */
public class C0121BX extends C2484fi {
    @C0064Am(aul = "4ed7212e371268200ec76c74e6d0d8c4", aum = 0)

    /* renamed from: Rs */
    public I18NString f210Rs;
    @C0064Am(aul = "4ed7212e371268200ec76c74e6d0d8c4", aum = -2)

    /* renamed from: Rt */
    public long f211Rt;
    @C0064Am(aul = "4ed7212e371268200ec76c74e6d0d8c4", aum = -1)

    /* renamed from: Ru */
    public byte f212Ru;
    @C0064Am(aul = "5862e953bf243bcddfd7008035976e5a", aum = 1)
    public C3438ra<TipString> cuE;
    @C0064Am(aul = "5862e953bf243bcddfd7008035976e5a", aum = -2)
    public long cuF;
    @C0064Am(aul = "5862e953bf243bcddfd7008035976e5a", aum = -1)
    public byte cuG;

    public C0121BX() {
    }

    public C0121BX(C1616Xf xf) {
        super(xf);
    }

    /* renamed from: c */
    public C5663aRz[] mo309c() {
        return NLSResourceLoader._m_fields;
    }

    /* renamed from: d */
    public void mo310d() {
        super.mo310d();
        this.f210Rs = null;
        this.cuE = null;
        this.f211Rt = 0;
        this.cuF = 0;
        this.f212Ru = 0;
        this.cuG = 0;
    }
}
