package logic.data.mbean;

import game.script.item.BlueprintType;
import game.script.npcchat.actions.CraftItemSpeechAction;
import logic.baa.C1616Xf;
import logic.res.code.C5663aRz;
import p001a.C0064Am;

/* renamed from: a.Qo */
public class C1145Qo extends C6352alM {
    @C0064Am(aul = "a497271193044a70d7d1d6c133250b26", aum = 0)
    public BlueprintType dVp;
    @C0064Am(aul = "a497271193044a70d7d1d6c133250b26", aum = -2)
    public long dVq;
    @C0064Am(aul = "a497271193044a70d7d1d6c133250b26", aum = -1)
    public byte dVr;

    public C1145Qo() {
    }

    public C1145Qo(C1616Xf xf) {
        super(xf);
    }

    /* renamed from: c */
    public C5663aRz[] mo309c() {
        return CraftItemSpeechAction._m_fields;
    }

    /* renamed from: d */
    public void mo310d() {
        super.mo310d();
        this.dVp = null;
        this.dVq = 0;
        this.dVr = 0;
    }
}
