package logic.data.mbean;

import game.script.nls.NLSProgression;
import logic.baa.C1616Xf;
import logic.res.code.C5663aRz;
import p001a.C0064Am;
import taikodom.infra.script.I18NString;

/* renamed from: a.OF */
public class C0968OF extends C2484fi {
    @C0064Am(aul = "9ea8a199b43efe27a6e7f9586581297a", aum = -1)
    public byte azE;
    @C0064Am(aul = "9ea8a199b43efe27a6e7f9586581297a", aum = -2)
    public long azp;
    @C0064Am(aul = "3332227ece75e2913e71e243eb33fe47", aum = 0)
    public I18NString dNg;
    @C0064Am(aul = "9cb090254c45266453e95bc6e3d5e933", aum = 1)
    public I18NString dNh;
    @C0064Am(aul = "d32e93bb62253d70a5a81d5efe714263", aum = 2)
    public I18NString dNi;
    @C0064Am(aul = "7b81445e26e5d88720018d72bc448bea", aum = 3)
    public I18NString dNj;
    @C0064Am(aul = "ec551d4a472b16c64061929d29db1b08", aum = 4)
    public I18NString dNk;
    @C0064Am(aul = "242d907f7acad7524d6ab90128ff3934", aum = 5)
    public I18NString dNl;
    @C0064Am(aul = "9ea8a199b43efe27a6e7f9586581297a", aum = 6)
    public I18NString dNm;
    @C0064Am(aul = "3332227ece75e2913e71e243eb33fe47", aum = -2)
    public long dNn;
    @C0064Am(aul = "9cb090254c45266453e95bc6e3d5e933", aum = -2)
    public long dNo;
    @C0064Am(aul = "d32e93bb62253d70a5a81d5efe714263", aum = -2)
    public long dNp;
    @C0064Am(aul = "7b81445e26e5d88720018d72bc448bea", aum = -2)
    public long dNq;
    @C0064Am(aul = "ec551d4a472b16c64061929d29db1b08", aum = -2)
    public long dNr;
    @C0064Am(aul = "3332227ece75e2913e71e243eb33fe47", aum = -1)
    public byte dNs;
    @C0064Am(aul = "9cb090254c45266453e95bc6e3d5e933", aum = -1)
    public byte dNt;
    @C0064Am(aul = "d32e93bb62253d70a5a81d5efe714263", aum = -1)
    public byte dNu;
    @C0064Am(aul = "7b81445e26e5d88720018d72bc448bea", aum = -1)
    public byte dNv;
    @C0064Am(aul = "ec551d4a472b16c64061929d29db1b08", aum = -1)
    public byte dNw;
    @C0064Am(aul = "242d907f7acad7524d6ab90128ff3934", aum = -2)

    /* renamed from: kb */
    public long f1281kb;
    @C0064Am(aul = "242d907f7acad7524d6ab90128ff3934", aum = -1)

    /* renamed from: kg */
    public byte f1282kg;

    public C0968OF() {
    }

    public C0968OF(C1616Xf xf) {
        super(xf);
    }

    /* renamed from: c */
    public C5663aRz[] mo309c() {
        return NLSProgression._m_fields;
    }

    /* renamed from: d */
    public void mo310d() {
        super.mo310d();
        this.dNg = null;
        this.dNh = null;
        this.dNi = null;
        this.dNj = null;
        this.dNk = null;
        this.dNl = null;
        this.dNm = null;
        this.dNn = 0;
        this.dNo = 0;
        this.dNp = 0;
        this.dNq = 0;
        this.dNr = 0;
        this.f1281kb = 0;
        this.azp = 0;
        this.dNs = 0;
        this.dNt = 0;
        this.dNu = 0;
        this.dNv = 0;
        this.dNw = 0;
        this.f1282kg = 0;
        this.azE = 0;
    }
}
