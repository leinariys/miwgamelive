package logic.data.mbean;

import game.network.message.serializable.C3438ra;
import game.script.npcchat.AbstractSpeech;
import game.script.npcchat.SpeechRequirement;
import game.script.npcchat.SpeechRequirementCollection;
import logic.baa.C1616Xf;
import logic.res.code.C5663aRz;
import p001a.C0064Am;

import java.util.UUID;

/* renamed from: a.pS */
public class C3224pS extends C3805vD {
    @C0064Am(aul = "9f6af750c66070889a21f96a2fbc4e97", aum = 0)
    public C3438ra<SpeechRequirement> aTP;
    @C0064Am(aul = "f5c58dc70a6d12c0eb3856d61035b1e3", aum = 1)
    public C3438ra<SpeechRequirementCollection> aTQ;
    @C0064Am(aul = "9f6af750c66070889a21f96a2fbc4e97", aum = -2)
    public long aTR;
    @C0064Am(aul = "f5c58dc70a6d12c0eb3856d61035b1e3", aum = -2)
    public long aTS;
    @C0064Am(aul = "32aa093cf320cf3d030ec68011e1d2c6", aum = -2)
    public long aTT;
    @C0064Am(aul = "9f6af750c66070889a21f96a2fbc4e97", aum = -1)
    public byte aTU;
    @C0064Am(aul = "f5c58dc70a6d12c0eb3856d61035b1e3", aum = -1)
    public byte aTV;
    @C0064Am(aul = "32aa093cf320cf3d030ec68011e1d2c6", aum = -1)
    public byte aTW;
    @C0064Am(aul = "32aa093cf320cf3d030ec68011e1d2c6", aum = 2)

    /* renamed from: bI */
    public AbstractSpeech f8818bI;
    @C0064Am(aul = "92653b6948371846d7e447d1334cbef7", aum = 3)

    /* renamed from: bK */
    public UUID f8819bK;
    @C0064Am(aul = "a13e4c62ad8d277894a95070d15c616e", aum = 4)
    public String handle;
    @C0064Am(aul = "92653b6948371846d7e447d1334cbef7", aum = -2)

    /* renamed from: oL */
    public long f8820oL;
    @C0064Am(aul = "92653b6948371846d7e447d1334cbef7", aum = -1)

    /* renamed from: oS */
    public byte f8821oS;
    @C0064Am(aul = "a13e4c62ad8d277894a95070d15c616e", aum = -2)

    /* renamed from: ok */
    public long f8822ok;
    @C0064Am(aul = "a13e4c62ad8d277894a95070d15c616e", aum = -1)

    /* renamed from: on */
    public byte f8823on;

    public C3224pS() {
    }

    public C3224pS(C1616Xf xf) {
        super(xf);
    }

    /* renamed from: c */
    public C5663aRz[] mo309c() {
        return SpeechRequirementCollection._m_fields;
    }

    /* renamed from: d */
    public void mo310d() {
        super.mo310d();
        this.aTP = null;
        this.aTQ = null;
        this.f8818bI = null;
        this.f8819bK = null;
        this.handle = null;
        this.aTR = 0;
        this.aTS = 0;
        this.aTT = 0;
        this.f8820oL = 0;
        this.f8822ok = 0;
        this.aTU = 0;
        this.aTV = 0;
        this.aTW = 0;
        this.f8821oS = 0;
        this.f8823on = 0;
    }
}
