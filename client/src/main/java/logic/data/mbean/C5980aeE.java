package logic.data.mbean;

import game.script.billing.Billing;
import game.script.billing.BillingTransaction;
import logic.baa.C1616Xf;
import logic.res.code.C5663aRz;
import p001a.C0064Am;

/* renamed from: a.aeE  reason: case insensitive filesystem */
public class C5980aeE extends C3805vD {
    @C0064Am(aul = "c386166bdae60231c4a616a73be33681", aum = -2)
    public long aLG;
    @C0064Am(aul = "c386166bdae60231c4a616a73be33681", aum = -1)
    public byte aLL;
    @C0064Am(aul = "c386166bdae60231c4a616a73be33681", aum = 2)
    public long axi;
    @C0064Am(aul = "bd1ee111c0124d2b1bc41635d6c993c0", aum = 1)
    public long fpP;
    @C0064Am(aul = "eac8a13e5cd5a5aaa9904c5658dfb52d", aum = 3)
    public Billing fpQ;
    @C0064Am(aul = "c0d6b3904da8b8d020d3cce1314033e7", aum = -2)
    public long fpR;
    @C0064Am(aul = "bd1ee111c0124d2b1bc41635d6c993c0", aum = -2)
    public long fpS;
    @C0064Am(aul = "eac8a13e5cd5a5aaa9904c5658dfb52d", aum = -2)
    public long fpT;
    @C0064Am(aul = "c0d6b3904da8b8d020d3cce1314033e7", aum = -1)
    public byte fpU;
    @C0064Am(aul = "bd1ee111c0124d2b1bc41635d6c993c0", aum = -1)
    public byte fpV;
    @C0064Am(aul = "eac8a13e5cd5a5aaa9904c5658dfb52d", aum = -1)
    public byte fpW;
    @C0064Am(aul = "c0d6b3904da8b8d020d3cce1314033e7", aum = 0)
    public long when;

    public C5980aeE() {
    }

    public C5980aeE(C1616Xf xf) {
        super(xf);
    }

    /* renamed from: c */
    public C5663aRz[] mo309c() {
        return BillingTransaction._m_fields;
    }

    /* renamed from: d */
    public void mo310d() {
        super.mo310d();
        this.when = 0;
        this.fpP = 0;
        this.axi = 0;
        this.fpQ = null;
        this.fpR = 0;
        this.fpS = 0;
        this.aLG = 0;
        this.fpT = 0;
        this.fpU = 0;
        this.fpV = 0;
        this.aLL = 0;
        this.fpW = 0;
    }
}
