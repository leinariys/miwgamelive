package logic.data.mbean;

import game.network.message.serializable.C1556Wo;
import logic.baa.*;
import logic.res.code.C5663aRz;
import p001a.C0064Am;

/* renamed from: a.aDs  reason: case insensitive filesystem */
public class C5292aDs extends C0677Jd {
    @C0064Am(aul = "d15102ec50d3a105785dc71f1a278468", aum = 0)
    public C1556Wo<Class<? extends C4062yl>, C4112zc> appListeners;
    @C0064Am(aul = "f64fdc3fbf60d58aeb2e968279071e4d", aum = -2)

    /* renamed from: df */
    public long f2595df;
    @C0064Am(aul = "dbcf5f8a876737595912326a255aabc5", aum = 3)
    public boolean disposed;
    @C0064Am(aul = "f64fdc3fbf60d58aeb2e968279071e4d", aum = -1)

    /* renamed from: dm */
    public byte f2596dm;
    @C0064Am(aul = "81bbba043bb125b879501831f8cc0edf", aum = -1)
    public byte hBA;
    @C0064Am(aul = "dbcf5f8a876737595912326a255aabc5", aum = -1)
    public byte hBB;
    @C0064Am(aul = "d15102ec50d3a105785dc71f1a278468", aum = -2)
    public long hBu;
    @C0064Am(aul = "d07f99374dda9c298921f4e674a20bc1", aum = -2)
    public long hBv;
    @C0064Am(aul = "81bbba043bb125b879501831f8cc0edf", aum = -2)
    public long hBw;
    @C0064Am(aul = "dbcf5f8a876737595912326a255aabc5", aum = -2)
    public long hBx;
    @C0064Am(aul = "d15102ec50d3a105785dc71f1a278468", aum = -1)
    public byte hBy;
    @C0064Am(aul = "d07f99374dda9c298921f4e674a20bc1", aum = -1)
    public byte hBz;
    @C0064Am(aul = "81bbba043bb125b879501831f8cc0edf", aum = 2)
    public boolean hasLocalListeners;
    @C0064Am(aul = "d07f99374dda9c298921f4e674a20bc1", aum = 1)
    public C1556Wo<Class<? extends C4062yl>, C4112zc> registeredAppListeners;
    @C0064Am(aul = "f64fdc3fbf60d58aeb2e968279071e4d", aum = 4)
    public C2961mJ type;

    public C5292aDs() {
    }

    public C5292aDs(C1616Xf xf) {
        super(xf);
    }

    /* renamed from: c */
    public C5663aRz[] mo309c() {
        return aDJ._m_fields;
    }

    /* renamed from: d */
    public void mo310d() {
        this.appListeners = null;
        this.registeredAppListeners = null;
        this.hasLocalListeners = false;
        this.disposed = false;
        this.type = null;
        this.hBu = 0;
        this.hBv = 0;
        this.hBw = 0;
        this.hBx = 0;
        this.f2595df = 0;
        this.hBy = 0;
        this.hBz = 0;
        this.hBA = 0;
        this.hBB = 0;
        this.f2596dm = 0;
    }
}
