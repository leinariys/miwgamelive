package logic.data.mbean;

import game.network.message.serializable.C1556Wo;
import game.script.progression.ProgressionCareer;
import game.script.progression.ProgressionCharacterTemplate;
import game.script.progression.ProgressionCharacterTemplateAbilities;
import logic.baa.C1616Xf;
import logic.res.code.C5663aRz;
import p001a.C0064Am;

import java.util.UUID;

/* renamed from: a.ayU */
public class ayU extends C3805vD {
    @C0064Am(aul = "cc854d97fef664c60e3af5b37b0e7810", aum = 3)

    /* renamed from: bK */
    public UUID f5616bK;
    @C0064Am(aul = "dce0a2077770797891b60837676ca70b", aum = 1)
    public String description;
    @C0064Am(aul = "c5f9cfa5fcb86fd3844663a85a62e32c", aum = -2)
    public long gWM;
    @C0064Am(aul = "12e709fb49a1d73df4187a28ca5ed184", aum = -2)
    public long gWN;
    @C0064Am(aul = "c5f9cfa5fcb86fd3844663a85a62e32c", aum = -1)
    public byte gWO;
    @C0064Am(aul = "12e709fb49a1d73df4187a28ca5ed184", aum = -1)
    public byte gWP;
    @C0064Am(aul = "c5f9cfa5fcb86fd3844663a85a62e32c", aum = 0)
    public ProgressionCareer ghc;
    @C0064Am(aul = "12e709fb49a1d73df4187a28ca5ed184", aum = 2)
    public C1556Wo<Integer, ProgressionCharacterTemplateAbilities> ghe;
    @C0064Am(aul = "ebe47c34a51ac1b1c073d3586c42c44c", aum = 4)
    public String handle;
    @C0064Am(aul = "dce0a2077770797891b60837676ca70b", aum = -2)

    /* renamed from: nk */
    public long f5617nk;
    @C0064Am(aul = "dce0a2077770797891b60837676ca70b", aum = -1)

    /* renamed from: nn */
    public byte f5618nn;
    @C0064Am(aul = "cc854d97fef664c60e3af5b37b0e7810", aum = -2)

    /* renamed from: oL */
    public long f5619oL;
    @C0064Am(aul = "cc854d97fef664c60e3af5b37b0e7810", aum = -1)

    /* renamed from: oS */
    public byte f5620oS;
    @C0064Am(aul = "ebe47c34a51ac1b1c073d3586c42c44c", aum = -2)

    /* renamed from: ok */
    public long f5621ok;
    @C0064Am(aul = "ebe47c34a51ac1b1c073d3586c42c44c", aum = -1)

    /* renamed from: on */
    public byte f5622on;

    public ayU() {
    }

    public ayU(C1616Xf xf) {
        super(xf);
    }

    /* renamed from: c */
    public C5663aRz[] mo309c() {
        return ProgressionCharacterTemplate._m_fields;
    }

    /* renamed from: d */
    public void mo310d() {
        super.mo310d();
        this.ghc = null;
        this.description = null;
        this.ghe = null;
        this.f5616bK = null;
        this.handle = null;
        this.gWM = 0;
        this.f5617nk = 0;
        this.gWN = 0;
        this.f5619oL = 0;
        this.f5621ok = 0;
        this.gWO = 0;
        this.f5618nn = 0;
        this.gWP = 0;
        this.f5620oS = 0;
        this.f5622on = 0;
    }
}
