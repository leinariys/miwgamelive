package logic.data.mbean;

import game.script.map3d.ZoomLevel;
import logic.baa.C1616Xf;
import logic.res.code.C5663aRz;
import p001a.C0064Am;
import p001a.C6785atd;

/* renamed from: a.Mp */
public class C0884Mp extends C3805vD {
    @C0064Am(aul = "c39f798bcd9377416bd9c4749a43235a", aum = 0)

    /* renamed from: AJ */
    public C6785atd f1150AJ;
    @C0064Am(aul = "a8000c8db7b9c383ba486d5b859d7f4c", aum = 1)

    /* renamed from: AL */
    public float f1151AL;
    @C0064Am(aul = "c39f798bcd9377416bd9c4749a43235a", aum = -2)
    public long dzR;
    @C0064Am(aul = "a8000c8db7b9c383ba486d5b859d7f4c", aum = -2)
    public long dzS;
    @C0064Am(aul = "c39f798bcd9377416bd9c4749a43235a", aum = -1)
    public byte dzT;
    @C0064Am(aul = "a8000c8db7b9c383ba486d5b859d7f4c", aum = -1)
    public byte dzU;

    public C0884Mp() {
    }

    public C0884Mp(C1616Xf xf) {
        super(xf);
    }

    /* renamed from: c */
    public C5663aRz[] mo309c() {
        return ZoomLevel._m_fields;
    }

    /* renamed from: d */
    public void mo310d() {
        super.mo310d();
        this.f1150AJ = null;
        this.f1151AL = 0.0f;
        this.dzR = 0;
        this.dzS = 0;
        this.dzT = 0;
        this.dzU = 0;
    }
}
