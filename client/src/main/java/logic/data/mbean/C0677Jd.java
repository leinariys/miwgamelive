package logic.data.mbean;

import game.io.IReadExternal;
import game.io.IWriteExternal;
import game.network.WhoAmI;
import game.network.exception.CreatListenerChannelException;
import game.script.Character;
import logic.baa.C1616Xf;
import logic.res.code.C2759jd;
import logic.res.code.C5663aRz;
import logic.thred.LogPrinter;
import org.mozilla1.classfile.ClassFileWriter;
import p001a.*;

import java.io.ObjectInput;
import java.io.ObjectOutput;

/* renamed from: a.Jd */
/* compiled from: a */
public class C0677Jd extends RunTheUnsafe implements C5439aJj {
    public static final int dhY = 1;
    public static final int dhZ = 2;
    public static final int dia = 4;
    public static final int dib = 8;
    public static final int dic = 16;
    public static final int did = 32;
    public static final int die = 64;
    public static final int dif = 128;
    public static final int dig = 256;
    public static final int dih = 512;
    public static final int dii = 1024;
    public static final int dij = 65536;
    public static final int dik = 3;
    public static final int dil = 0;
    public static final int dim = 1;
    public static final int din = 2;

    private static /* synthetic */ int[] dix = null;
    private static /* synthetic */ int[] diy = null;
    private static LogPrinter logger = LogPrinter.m10275K(C0677Jd.class);
    public aWq bzZ;
    public C3582se cVz;
    public int dby;
    public long dio;
    public RunTheUnsafe dip;
    public int diq;
    public C5546aNm dis;
    public long dit;
    public long diu;
    public byte div;
    public short diw;

    public C0677Jd() {
        this.div = 1;
        this.div = 0;
        this.diw = ClassFileWriter.ACC_NATIVE;
    }

    public C0677Jd(C1616Xf xf) {
        super(xf);
        this.div = 1;
        this.div = 0;
        this.diw = ClassFileWriter.ACC_NATIVE;
    }

    static /* synthetic */ int[] aXD() {
        int[] iArr = dix;
        if (iArr == null) {
            iArr = new int[C1020Ot.C1021a.values().length];
            try {
                iArr[C1020Ot.C1021a.IMMEDIATE.ordinal()] = 3;
            } catch (NoSuchFieldError e) {
            }
            try {
                iArr[C1020Ot.C1021a.NOT_PERSISTED.ordinal()] = 1;
            } catch (NoSuchFieldError e2) {
            }
            try {
                iArr[C1020Ot.C1021a.TIME_BASED.ordinal()] = 2;
            } catch (NoSuchFieldError e3) {
            }
            dix = iArr;
        }
        return iArr;
    }

    static /* synthetic */ int[] aXE() {
        int[] iArr = diy;
        if (iArr == null) {
            iArr = new int[C3122oB.C3123a.values().length];
            try {
                iArr[C3122oB.C3123a.IMMEDIATE.ordinal()] = 3;
            } catch (NoSuchFieldError e) {
            }
            try {
                iArr[C3122oB.C3123a.NOT_REPLICATED.ordinal()] = 1;
            } catch (NoSuchFieldError e2) {
            }
            try {
                iArr[C3122oB.C3123a.TIME_BASED.ordinal()] = 2;
            } catch (NoSuchFieldError e3) {
            }
            diy = iArr;
        }
        return iArr;
    }

    /* renamed from: g */
    public void mo3200g(C1616Xf xf) {
        this.f3984Uk = xf;
    }

    /* renamed from: b */
    private void m5849b(C5546aNm anm) {
        anm.irv = this.dis;
        this.dis = anm;
        this.diq++;
    }

    /* JADX WARNING: Can't fix incorrect switch cases order */
    /* renamed from: c */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void mo3188c(C0677Jd r9) {
        /*
            r8 = this;
            r4 = 0
            a.aRz[] r5 = r8.mo309c()
            int r0 = r5.length
            r3 = r4
        L_0x0007:
            int r2 = r0 + -1
            if (r2 >= 0) goto L_0x000f
            r8.mo3146A(r4)
            return
        L_0x000f:
            r0 = r5[r3]
            a.jd r0 = (logic.res.code.C2759jd) r0
            long r6 = r0.mo19961EC()
            sun.misc.Unsafe r1 = unsafe
            byte r6 = r1.getByte(r8, r6)
            r1 = r6 & 12
            r7 = 4
            if (r1 != r7) goto L_0x002c
            long r6 = r8.cmb
            r8.mo11939e((logic.res.code.C5663aRz) r0, (long) r6)
        L_0x0027:
            int r1 = r3 + 1
            r0 = r2
            r3 = r1
            goto L_0x0007
        L_0x002c:
            char r1 = r0.mo11290ED()
            switch(r1) {
                case 66: goto L_0x0069;
                case 67: goto L_0x0071;
                case 68: goto L_0x0089;
                case 70: goto L_0x0081;
                case 73: goto L_0x0059;
                case 74: goto L_0x0061;
                case 76: goto L_0x0091;
                case 83: goto L_0x0079;
                case 90: goto L_0x004c;
                case 91: goto L_0x0091;
                default: goto L_0x0033;
            }
        L_0x0033:
            java.lang.IllegalArgumentException r1 = new java.lang.IllegalArgumentException
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            java.lang.String r3 = "Unknow java type char: "
            r2.<init>(r3)
            char r0 = r0.mo11290ED()
            java.lang.StringBuilder r0 = r2.append(r0)
            java.lang.String r0 = r0.toString()
            r1.<init>(r0)
            throw r1
        L_0x004c:
            boolean r1 = r9.mo11914M(r0)
            r8.mo11935c((logic.res.code.C5663aRz) r0, (boolean) r1)
        L_0x0053:
            long r6 = r8.dio
            r8.mo11939e((logic.res.code.C5663aRz) r0, (long) r6)
            goto L_0x0027
        L_0x0059:
            int r1 = r9.mo11913L(r0)
            r8.mo11938e((logic.res.code.C5663aRz) r0, (int) r1)
            goto L_0x0053
        L_0x0061:
            long r6 = r9.mo11918Q(r0)
            r8.mo11937d((logic.res.code.C5663aRz) r0, (long) r6)
            goto L_0x0053
        L_0x0069:
            byte r1 = r9.mo11915N(r0)
            r8.mo11929c((logic.res.code.C5663aRz) r0, (byte) r1)
            goto L_0x0053
        L_0x0071:
            char r1 = r9.mo11917P(r0)
            r8.mo11930c((logic.res.code.C5663aRz) r0, (char) r1)
            goto L_0x0053
        L_0x0079:
            short r1 = r9.mo11916O(r0)
            r8.mo11934c((logic.res.code.C5663aRz) r0, (short) r1)
            goto L_0x0053
        L_0x0081:
            float r1 = r9.mo11919R(r0)
            r8.mo11932c((logic.res.code.C5663aRz) r0, (float) r1)
            goto L_0x0053
        L_0x0089:
            double r6 = r9.mo11920S(r0)
            r8.mo11931c((logic.res.code.C5663aRz) r0, (double) r6)
            goto L_0x0053
        L_0x0091:
            java.lang.Object r1 = r9.mo11912K(r0)
            r6 = r6 & 8
            if (r6 == 0) goto L_0x00b3
            a.aNm r6 = r8.m5846a((logic.res.code.C2759jd) r0)
            if (r1 != 0) goto L_0x00a5
            a.Xf r1 = r8.f3984Uk
            java.lang.Object r1 = r0.mo2188b((logic.baa.C1616Xf) r1)
        L_0x00a5:
            java.lang.Object r1 = r6.mo880K(r1)
            r8.mo11942m(r0, r1)
            long r6 = r8.dio
            r8.mo11939e((logic.res.code.C5663aRz) r0, (long) r6)
            goto L_0x0027
        L_0x00b3:
            r8.mo11942m(r0, r1)
            goto L_0x0053
        */
        throw new UnsupportedOperationException("Method not decompiled: logic.data.mbean.C0677Jd.mo3188c(a.Jd):void");
    }

    /* renamed from: a */
    private void m5848a(C5663aRz arz, int i, Object obj) {
        int i2 = i | 1;
        if (arz.mo2179Ek() && !(obj instanceof C1616Xf) && !(obj instanceof String)) {
            i2 |= 2;
        }
        mo3189c(arz, i2);
    }

    /* renamed from: a */
    private void m5847a(C5663aRz arz, int i) {
        if (!this.bzZ.dDD() || this.dip == this.cVz.cVo() || mo3210l(arz) == this.cVz.cVo().mo3210l(arz)) {
            mo3189c(arz, i | 1);
            return;
        }
        throw new C6510aoO();
    }

    /* access modifiers changed from: protected */
    /* renamed from: f */
    public boolean mo3198f(C5663aRz arz) {
        if ((this.f3984Uk.bFf().mo6866PM().getWhoAmI() == WhoAmI.CLIENT && (this.f3984Uk.isUseClientOnly() || arz.mo11299Ev())) || this.f3984Uk.bFn() || this.f3984Uk.bFf().mo6866PM().bGo()) {
            return true;
        }
        if (mo3194du()) {
            return false;
        }
        if (this.f3984Uk.bFf().mo6887du()) {
            return false;
        }
        throw new IllegalStateException("Illegal outside transaction");
    }

    public C6064afk aXj() {
        return null;
    }

    /* access modifiers changed from: protected */
    /* renamed from: d */
    public void mo310d() {
        throw new CreatListenerChannelException("Should have been created by the intrumentation.");
    }

    /* JADX WARNING: Can't fix incorrect switch cases order */
    /* renamed from: a */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean mo3160a(C6064afk r37, long r38) {
        /*
            r36 = this;
            a.Jd r37 = (logic.data.mbean.C0677Jd) r37
            r0 = r36
            short r4 = r0.diw
            r4 = r4 & 3
            if (r4 != 0) goto L_0x001b
            r4 = 0
            r0 = r36
            r0.div = r4
            r4 = 0
            r0 = r36
            r0.dip = r4
            r4 = 0
            r0 = r36
            r0.bzZ = r4
            r4 = 0
        L_0x001a:
            return r4
        L_0x001b:
            r0 = r36
            a.aVo r4 = r0.dip
            r0 = r37
            if (r0 == r4) goto L_0x00b8
            r4 = 1
            r11 = r4
        L_0x0025:
            a.aRz[] r28 = r36.mo309c()
            r0 = r38
            r2 = r36
            r2.cmb = r0
            long r4 = r37.aBt()
            r0 = r36
            r0.dio = r4
            r23 = 0
            r14 = 0
            r22 = 0
            r21 = 0
            r20 = 0
            r15 = 0
            r18 = 9223372036854775807(0x7fffffffffffffff, double:NaN)
            r16 = 9223372036854775807(0x7fffffffffffffff, double:NaN)
            r0 = r37
            short r4 = r0.diw
            r4 = r4 & 48
            if (r4 == 0) goto L_0x04e8
            r4 = r4 | 8
            r12 = r4
        L_0x0056:
            r5 = 0
            r0 = r28
            int r4 = r0.length
            r13 = r4
            r27 = r5
        L_0x005d:
            int r26 = r13 + -1
            if (r26 >= 0) goto L_0x00bc
            if (r20 == 0) goto L_0x0467
            r0 = r36
            short r4 = r0.diw
            r4 = r4 | 128(0x80, float:1.794E-43)
            short r4 = (short) r4
            r0 = r36
            r0.diw = r4
        L_0x006e:
            if (r22 == 0) goto L_0x04a8
            r0 = r36
            short r4 = r0.diw
            r4 = r4 | 64
            short r4 = (short) r4
            r0 = r36
            r0.diw = r4
        L_0x007b:
            r0 = r36
            a.Xf r4 = r0.f3984Uk
            if (r4 == 0) goto L_0x009c
            r0 = r36
            a.Xf r4 = r0.f3984Uk
            a.any r4 = r4.mo11T()
            a.Ot$a r4 = r4.mo15115Ew()
            a.Ot$a r5 = p001a.C1020Ot.C1021a.NOT_PERSISTED
            if (r4 != r5) goto L_0x009c
            r0 = r36
            short r4 = r0.diw
            r4 = r4 & -145(0xffffffffffffff6f, float:NaN)
            short r4 = (short) r4
            r0 = r36
            r0.diw = r4
        L_0x009c:
            r0 = r23
            r1 = r36
            r1.dby = r0
            r0 = r36
            r0.diq = r14
            r4 = 0
            r0 = r36
            r0.div = r4
            r4 = 0
            r0 = r36
            r0.dip = r4
            r4 = 0
            r0 = r36
            r0.bzZ = r4
            r4 = 1
            goto L_0x001a
        L_0x00b8:
            r4 = 0
            r11 = r4
            goto L_0x0025
        L_0x00bc:
            r4 = r28[r27]
            r10 = r4
            a.jd r10 = (logic.res.code.C2759jd) r10
            long r30 = r10.mo19960EB()     // Catch:{ Exception -> 0x0383 }
            long r6 = r10.mo19959EA()     // Catch:{ Exception -> 0x0383 }
            long r32 = r10.mo19961EC()     // Catch:{ Exception -> 0x0383 }
            r0 = r36
            int r13 = r0.mo11921T(r10)     // Catch:{ Exception -> 0x0383 }
            r24 = 0
            r29 = 0
            char r4 = r10.mo11290ED()     // Catch:{ Exception -> 0x0383 }
            switch(r4) {
                case 66: goto L_0x01da;
                case 67: goto L_0x0211;
                case 68: goto L_0x02b8;
                case 70: goto L_0x027f;
                case 73: goto L_0x0172;
                case 74: goto L_0x01a1;
                case 76: goto L_0x02f1;
                case 83: goto L_0x0248;
                case 90: goto L_0x0144;
                case 91: goto L_0x02f1;
                default: goto L_0x00de;
            }     // Catch:{ Exception -> 0x0383 }
        L_0x00de:
            r6 = r24
            r25 = r14
        L_0x00e2:
            if (r6 != 0) goto L_0x0399
            sun.misc.Unsafe r4 = unsafe     // Catch:{ Exception -> 0x0383 }
            sun.misc.Unsafe r5 = unsafe     // Catch:{ Exception -> 0x0383 }
            r0 = r37
            r1 = r30
            long r8 = r5.getLong(r0, r1)     // Catch:{ Exception -> 0x0383 }
            r5 = r36
            r6 = r30
            r4.putLong(r5, r6, r8)     // Catch:{ Exception -> 0x0383 }
            if (r12 == 0) goto L_0x04d9
            sun.misc.Unsafe r4 = unsafe     // Catch:{ Exception -> 0x0383 }
            r0 = r37
            r1 = r32
            int r4 = r4.getInt(r0, r1)     // Catch:{ Exception -> 0x0383 }
            r5 = r4 & r12
            r5 = r5 | r13
            r4 = r4 & 8
            if (r4 == 0) goto L_0x011e
            r0 = r37
            a.aNm r4 = r0.m5846a((logic.res.code.C2759jd) r10)     // Catch:{ Exception -> 0x0383 }
            a.aNm r4 = r4.dgf()     // Catch:{ Exception -> 0x0383 }
            r0 = r36
            a.aNm r6 = r0.dis     // Catch:{ Exception -> 0x0383 }
            r4.irv = r6     // Catch:{ Exception -> 0x0383 }
            r0 = r36
            r0.dis = r4     // Catch:{ Exception -> 0x0383 }
        L_0x011e:
            sun.misc.Unsafe r4 = unsafe     // Catch:{ Exception -> 0x0383 }
            byte r5 = (byte) r5     // Catch:{ Exception -> 0x0383 }
            r0 = r36
            r1 = r32
            r4.putByte(r0, r1, r5)     // Catch:{ Exception -> 0x0383 }
            r4 = r16
            r6 = r18
            r8 = r15
            r9 = r20
            r10 = r21
        L_0x0131:
            int r24 = r27 + 1
            r13 = r26
            r27 = r24
            r16 = r4
            r18 = r6
            r15 = r8
            r20 = r9
            r21 = r10
            r14 = r25
            goto L_0x005d
        L_0x0144:
            r4 = r13 & 2
            if (r4 == 0) goto L_0x015b
            r0 = r37
            boolean r4 = r0.mo11914M(r10)     // Catch:{ Exception -> 0x0383 }
            r0 = r36
            boolean r5 = r0.mo11914M(r10)     // Catch:{ Exception -> 0x0383 }
            if (r4 == r5) goto L_0x00de
            r4 = 1
            r6 = r4
            r25 = r14
            goto L_0x00e2
        L_0x015b:
            if (r11 != 0) goto L_0x0161
            r4 = r13 & 1
            if (r4 != 0) goto L_0x00de
        L_0x0161:
            r0 = r37
            boolean r4 = r0.mo11914M(r10)     // Catch:{ Exception -> 0x0383 }
            r0 = r36
            r0.mo11935c((logic.res.code.C5663aRz) r10, (boolean) r4)     // Catch:{ Exception -> 0x0383 }
            r6 = r24
            r25 = r14
            goto L_0x00e2
        L_0x0172:
            r4 = r13 & 2
            if (r4 == 0) goto L_0x018a
            r0 = r37
            int r4 = r0.mo11913L(r10)     // Catch:{ Exception -> 0x0383 }
            r0 = r36
            int r5 = r0.mo11913L(r10)     // Catch:{ Exception -> 0x0383 }
            if (r4 == r5) goto L_0x00de
            r4 = 1
            r6 = r4
            r25 = r14
            goto L_0x00e2
        L_0x018a:
            if (r11 != 0) goto L_0x0190
            r4 = r13 & 1
            if (r4 != 0) goto L_0x00de
        L_0x0190:
            r0 = r37
            int r4 = r0.mo11913L(r10)     // Catch:{ Exception -> 0x0383 }
            r0 = r36
            r0.mo11938e((logic.res.code.C5663aRz) r10, (int) r4)     // Catch:{ Exception -> 0x0383 }
            r6 = r24
            r25 = r14
            goto L_0x00e2
        L_0x01a1:
            r4 = r13 & 2
            if (r4 == 0) goto L_0x01bf
            sun.misc.Unsafe r4 = unsafe     // Catch:{ Exception -> 0x0383 }
            r0 = r37
            long r4 = r4.getLong(r0, r6)     // Catch:{ Exception -> 0x0383 }
            sun.misc.Unsafe r8 = unsafe     // Catch:{ Exception -> 0x0383 }
            r0 = r36
            long r6 = r8.getLong(r0, r6)     // Catch:{ Exception -> 0x0383 }
            int r4 = (r4 > r6 ? 1 : (r4 == r6 ? 0 : -1))
            if (r4 == 0) goto L_0x00de
            r4 = 1
            r6 = r4
            r25 = r14
            goto L_0x00e2
        L_0x01bf:
            if (r11 != 0) goto L_0x01c5
            r4 = r13 & 1
            if (r4 != 0) goto L_0x00de
        L_0x01c5:
            sun.misc.Unsafe r4 = unsafe     // Catch:{ Exception -> 0x0383 }
            r0 = r37
            long r8 = r4.getLong(r0, r6)     // Catch:{ Exception -> 0x0383 }
            sun.misc.Unsafe r4 = unsafe     // Catch:{ Exception -> 0x0383 }
            r5 = r36
            r4.putLong(r5, r6, r8)     // Catch:{ Exception -> 0x0383 }
            r6 = r24
            r25 = r14
            goto L_0x00e2
        L_0x01da:
            r4 = r13 & 2
            if (r4 == 0) goto L_0x01f6
            sun.misc.Unsafe r4 = unsafe     // Catch:{ Exception -> 0x0383 }
            r0 = r37
            byte r4 = r4.getByte(r0, r6)     // Catch:{ Exception -> 0x0383 }
            sun.misc.Unsafe r5 = unsafe     // Catch:{ Exception -> 0x0383 }
            r0 = r36
            byte r5 = r5.getByte(r0, r6)     // Catch:{ Exception -> 0x0383 }
            if (r4 == r5) goto L_0x00de
            r4 = 1
            r6 = r4
            r25 = r14
            goto L_0x00e2
        L_0x01f6:
            if (r11 != 0) goto L_0x01fc
            r4 = r13 & 1
            if (r4 != 0) goto L_0x00de
        L_0x01fc:
            sun.misc.Unsafe r4 = unsafe     // Catch:{ Exception -> 0x0383 }
            r0 = r37
            byte r4 = r4.getByte(r0, r6)     // Catch:{ Exception -> 0x0383 }
            sun.misc.Unsafe r5 = unsafe     // Catch:{ Exception -> 0x0383 }
            r0 = r36
            r5.putByte(r0, r6, r4)     // Catch:{ Exception -> 0x0383 }
            r6 = r24
            r25 = r14
            goto L_0x00e2
        L_0x0211:
            r4 = r13 & 2
            if (r4 == 0) goto L_0x022d
            sun.misc.Unsafe r4 = unsafe     // Catch:{ Exception -> 0x0383 }
            r0 = r37
            char r4 = r4.getChar(r0, r6)     // Catch:{ Exception -> 0x0383 }
            sun.misc.Unsafe r5 = unsafe     // Catch:{ Exception -> 0x0383 }
            r0 = r36
            char r5 = r5.getChar(r0, r6)     // Catch:{ Exception -> 0x0383 }
            if (r4 == r5) goto L_0x00de
            r4 = 1
            r6 = r4
            r25 = r14
            goto L_0x00e2
        L_0x022d:
            if (r11 != 0) goto L_0x0233
            r4 = r13 & 1
            if (r4 != 0) goto L_0x00de
        L_0x0233:
            sun.misc.Unsafe r4 = unsafe     // Catch:{ Exception -> 0x0383 }
            r0 = r37
            char r4 = r4.getChar(r0, r6)     // Catch:{ Exception -> 0x0383 }
            sun.misc.Unsafe r5 = unsafe     // Catch:{ Exception -> 0x0383 }
            r0 = r36
            r5.putChar(r0, r6, r4)     // Catch:{ Exception -> 0x0383 }
            r6 = r24
            r25 = r14
            goto L_0x00e2
        L_0x0248:
            r4 = r13 & 2
            if (r4 == 0) goto L_0x0264
            sun.misc.Unsafe r4 = unsafe     // Catch:{ Exception -> 0x0383 }
            r0 = r37
            short r4 = r4.getShort(r0, r6)     // Catch:{ Exception -> 0x0383 }
            sun.misc.Unsafe r5 = unsafe     // Catch:{ Exception -> 0x0383 }
            r0 = r36
            short r5 = r5.getShort(r0, r6)     // Catch:{ Exception -> 0x0383 }
            if (r4 == r5) goto L_0x00de
            r4 = 1
            r6 = r4
            r25 = r14
            goto L_0x00e2
        L_0x0264:
            if (r11 != 0) goto L_0x026a
            r4 = r13 & 1
            if (r4 != 0) goto L_0x00de
        L_0x026a:
            sun.misc.Unsafe r4 = unsafe     // Catch:{ Exception -> 0x0383 }
            r0 = r37
            short r4 = r4.getShort(r0, r6)     // Catch:{ Exception -> 0x0383 }
            sun.misc.Unsafe r5 = unsafe     // Catch:{ Exception -> 0x0383 }
            r0 = r36
            r5.putShort(r0, r6, r4)     // Catch:{ Exception -> 0x0383 }
            r6 = r24
            r25 = r14
            goto L_0x00e2
        L_0x027f:
            r4 = r13 & 2
            if (r4 == 0) goto L_0x029d
            sun.misc.Unsafe r4 = unsafe     // Catch:{ Exception -> 0x0383 }
            r0 = r37
            float r4 = r4.getFloat(r0, r6)     // Catch:{ Exception -> 0x0383 }
            sun.misc.Unsafe r5 = unsafe     // Catch:{ Exception -> 0x0383 }
            r0 = r36
            float r5 = r5.getFloat(r0, r6)     // Catch:{ Exception -> 0x0383 }
            int r4 = (r4 > r5 ? 1 : (r4 == r5 ? 0 : -1))
            if (r4 == 0) goto L_0x00de
            r4 = 1
            r6 = r4
            r25 = r14
            goto L_0x00e2
        L_0x029d:
            if (r11 != 0) goto L_0x02a3
            r4 = r13 & 1
            if (r4 != 0) goto L_0x00de
        L_0x02a3:
            sun.misc.Unsafe r4 = unsafe     // Catch:{ Exception -> 0x0383 }
            r0 = r37
            float r4 = r4.getFloat(r0, r6)     // Catch:{ Exception -> 0x0383 }
            sun.misc.Unsafe r5 = unsafe     // Catch:{ Exception -> 0x0383 }
            r0 = r36
            r5.putFloat(r0, r6, r4)     // Catch:{ Exception -> 0x0383 }
            r6 = r24
            r25 = r14
            goto L_0x00e2
        L_0x02b8:
            r4 = r13 & 2
            if (r4 == 0) goto L_0x02d6
            sun.misc.Unsafe r4 = unsafe     // Catch:{ Exception -> 0x0383 }
            r0 = r37
            double r4 = r4.getDouble(r0, r6)     // Catch:{ Exception -> 0x0383 }
            sun.misc.Unsafe r8 = unsafe     // Catch:{ Exception -> 0x0383 }
            r0 = r36
            double r6 = r8.getDouble(r0, r6)     // Catch:{ Exception -> 0x0383 }
            int r4 = (r4 > r6 ? 1 : (r4 == r6 ? 0 : -1))
            if (r4 == 0) goto L_0x00de
            r4 = 1
            r6 = r4
            r25 = r14
            goto L_0x00e2
        L_0x02d6:
            if (r11 != 0) goto L_0x02dc
            r4 = r13 & 1
            if (r4 != 0) goto L_0x00de
        L_0x02dc:
            sun.misc.Unsafe r4 = unsafe     // Catch:{ Exception -> 0x0383 }
            r0 = r37
            double r8 = r4.getDouble(r0, r6)     // Catch:{ Exception -> 0x0383 }
            sun.misc.Unsafe r4 = unsafe     // Catch:{ Exception -> 0x0383 }
            r5 = r36
            r4.putDouble(r5, r6, r8)     // Catch:{ Exception -> 0x0383 }
            r6 = r24
            r25 = r14
            goto L_0x00e2
        L_0x02f1:
            sun.misc.Unsafe r4 = unsafe     // Catch:{ Exception -> 0x0383 }
            r0 = r37
            java.lang.Object r25 = r4.getObject(r0, r6)     // Catch:{ Exception -> 0x0383 }
            r4 = r13 & 2
            if (r4 != 0) goto L_0x0315
            r0 = r36
            r1 = r25
            r0.mo11942m(r10, r1)     // Catch:{ Exception -> 0x0383 }
            r0 = r37
            long r4 = r0.mo3210l(r10)     // Catch:{ Exception -> 0x0383 }
            r0 = r36
            r0.mo11939e((logic.res.code.C5663aRz) r10, (long) r4)     // Catch:{ Exception -> 0x0383 }
            r6 = r24
            r25 = r14
            goto L_0x00e2
        L_0x0315:
            sun.misc.Unsafe r4 = unsafe     // Catch:{ Exception -> 0x0383 }
            r0 = r36
            java.lang.Object r4 = r4.getObject(r0, r6)     // Catch:{ Exception -> 0x0383 }
            r0 = r25
            if (r0 == r4) goto L_0x00de
            r0 = r25
            java.lang.Object r34 = r10.mo11307d(r0, r4)     // Catch:{ Exception -> 0x0383 }
            sun.misc.Unsafe r4 = unsafe     // Catch:{ Exception -> 0x0383 }
            r0 = r36
            r1 = r34
            r4.putObject(r0, r6, r1)     // Catch:{ Exception -> 0x0383 }
            r0 = r25
            r1 = r34
            if (r0 == r1) goto L_0x036e
            sun.misc.Unsafe r4 = unsafe     // Catch:{ Exception -> 0x0383 }
            r5 = r36
            r6 = r30
            r8 = r38
            r4.putLong(r5, r6, r8)     // Catch:{ Exception -> 0x0383 }
            r4 = r13 | 4
            byte r4 = (byte) r4     // Catch:{ Exception -> 0x0383 }
            sun.misc.Unsafe r5 = unsafe     // Catch:{ Exception -> 0x0383 }
            r0 = r36
            r1 = r32
            r5.putByte(r0, r1, r4)     // Catch:{ Exception -> 0x0383 }
            r0 = r25
            r1 = r34
            a.aNm r4 = r10.mo11306c(r0, r1)     // Catch:{ Exception -> 0x0383 }
            if (r4 == 0) goto L_0x04e4
            r4.mo9792c(r10)     // Catch:{ Exception -> 0x0383 }
            int r5 = r14 + 1
            r0 = r36
            a.aNm r6 = r0.dis     // Catch:{ Exception -> 0x0383 }
            r4.irv = r6     // Catch:{ Exception -> 0x0383 }
            r0 = r36
            r0.dis = r4     // Catch:{ Exception -> 0x0383 }
            r4 = r13 | 8
        L_0x0368:
            r6 = 1
            r13 = r4
            r25 = r5
            goto L_0x00e2
        L_0x036e:
            sun.misc.Unsafe r4 = unsafe     // Catch:{ Exception -> 0x0383 }
            sun.misc.Unsafe r5 = unsafe     // Catch:{ Exception -> 0x0383 }
            r0 = r37
            r1 = r30
            long r8 = r5.getLong(r0, r1)     // Catch:{ Exception -> 0x0383 }
            r5 = r36
            r6 = r30
            r4.putLong(r5, r6, r8)     // Catch:{ Exception -> 0x0383 }
            goto L_0x00de
        L_0x0383:
            r4 = move-exception
            java.lang.RuntimeException r5 = new java.lang.RuntimeException
            java.lang.StringBuilder r6 = new java.lang.StringBuilder
            java.lang.String r7 = "Error computing diff of field "
            r6.<init>(r7)
            java.lang.StringBuilder r6 = r6.append(r10)
            java.lang.String r6 = r6.toString()
            r5.<init>(r6, r4)
            throw r5
        L_0x0399:
            sun.misc.Unsafe r4 = unsafe     // Catch:{ Exception -> 0x0383 }
            r5 = r36
            r6 = r30
            r8 = r38
            r4.putLong(r5, r6, r8)     // Catch:{ Exception -> 0x0383 }
            if (r12 == 0) goto L_0x0427
            sun.misc.Unsafe r4 = unsafe     // Catch:{ Exception -> 0x0383 }
            r0 = r37
            r1 = r32
            int r4 = r4.getInt(r0, r1)     // Catch:{ Exception -> 0x0383 }
            r5 = r4 & r12
            r5 = r5 | 4
            r24 = r13 | r5
            r4 = r4 & 8
            if (r4 == 0) goto L_0x03c7
            if (r29 == 0) goto L_0x0409
            r0 = r37
            a.aNm r4 = r0.m5846a((logic.res.code.C2759jd) r10)     // Catch:{ Exception -> 0x0383 }
            r0 = r29
            r0.mo881a(r4)     // Catch:{ Exception -> 0x0383 }
        L_0x03c7:
            int r23 = r23 + 1
            int[] r4 = aXD()     // Catch:{ Exception -> 0x0383 }
            a.Ot$a r5 = r10.mo11300Ew()     // Catch:{ Exception -> 0x0383 }
            int r5 = r5.ordinal()     // Catch:{ Exception -> 0x0383 }
            r4 = r4[r5]     // Catch:{ Exception -> 0x0383 }
            switch(r4) {
                case 2: goto L_0x0434;
                case 3: goto L_0x042a;
                default: goto L_0x03da;
            }     // Catch:{ Exception -> 0x0383 }
        L_0x03da:
            r6 = r18
            r8 = r15
            r9 = r20
        L_0x03df:
            int[] r4 = aXE()     // Catch:{ Exception -> 0x0383 }
            a.oB$a r5 = r10.mo11302Ey()     // Catch:{ Exception -> 0x0383 }
            int r5 = r5.ordinal()     // Catch:{ Exception -> 0x0383 }
            r4 = r4[r5]     // Catch:{ Exception -> 0x0383 }
            switch(r4) {
                case 2: goto L_0x0452;
                case 3: goto L_0x0447;
                default: goto L_0x03f0;
            }     // Catch:{ Exception -> 0x0383 }
        L_0x03f0:
            r15 = r24
            r4 = r16
            r13 = r21
            r14 = r22
        L_0x03f8:
            sun.misc.Unsafe r16 = unsafe     // Catch:{ Exception -> 0x0383 }
            byte r15 = (byte) r15     // Catch:{ Exception -> 0x0383 }
            r0 = r16
            r1 = r36
            r2 = r32
            r0.putByte(r1, r2, r15)     // Catch:{ Exception -> 0x0383 }
            r10 = r13
            r22 = r14
            goto L_0x0131
        L_0x0409:
            r0 = r37
            a.aNm r4 = r0.m5846a((logic.res.code.C2759jd) r10)     // Catch:{ Exception -> 0x0383 }
            if (r4 != 0) goto L_0x0418
            java.io.PrintStream r5 = java.lang.System.out     // Catch:{ Exception -> 0x0383 }
            java.lang.String r6 = "bla!"
            r5.println(r6)     // Catch:{ Exception -> 0x0383 }
        L_0x0418:
            a.aNm r4 = r4.dgf()     // Catch:{ Exception -> 0x0383 }
            r0 = r36
            a.aNm r5 = r0.dis     // Catch:{ Exception -> 0x0383 }
            r4.irv = r5     // Catch:{ Exception -> 0x0383 }
            r0 = r36
            r0.dis = r4     // Catch:{ Exception -> 0x0383 }
            goto L_0x03c7
        L_0x0427:
            r24 = r13 | 4
            goto L_0x03c7
        L_0x042a:
            r20 = 1
            r24 = r24 | 16
            r6 = r18
            r8 = r15
            r9 = r20
            goto L_0x03df
        L_0x0434:
            r15 = 1
            long r4 = r10.mo11301Ex()     // Catch:{ Exception -> 0x0383 }
            r0 = r18
            long r18 = java.lang.Math.min(r4, r0)     // Catch:{ Exception -> 0x0383 }
            r24 = r24 | 16
            r6 = r18
            r8 = r15
            r9 = r20
            goto L_0x03df
        L_0x0447:
            r22 = 1
            r15 = r24 | 32
            r4 = r16
            r13 = r21
            r14 = r22
            goto L_0x03f8
        L_0x0452:
            r21 = 1
            long r4 = r10.mo11303Ez()     // Catch:{ Exception -> 0x0383 }
            r0 = r16
            long r16 = java.lang.Math.min(r4, r0)     // Catch:{ Exception -> 0x0383 }
            r15 = r24 | 32
            r4 = r16
            r13 = r21
            r14 = r22
            goto L_0x03f8
        L_0x0467:
            if (r15 == 0) goto L_0x006e
            r0 = r36
            a.Xf r4 = r0.f3984Uk
            a.any r4 = r4.mo11T()
            a.Ot$a r4 = r4.mo15115Ew()
            a.Ot$a r5 = p001a.C1020Ot.C1021a.NOT_PERSISTED
            if (r4 == r5) goto L_0x006e
            r0 = r36
            short r4 = r0.diw
            r4 = r4 | 16
            short r4 = (short) r4
            r0 = r36
            r0.diw = r4
            r4 = r12 & 16
            if (r4 == 0) goto L_0x049c
            long r4 = java.lang.System.currentTimeMillis()
            long r4 = r4 + r18
            r0 = r37
            long r6 = r0.diu
            long r4 = java.lang.Math.min(r4, r6)
            r0 = r36
            r0.diu = r4
            goto L_0x006e
        L_0x049c:
            long r4 = java.lang.System.currentTimeMillis()
            long r4 = r4 + r18
            r0 = r36
            r0.diu = r4
            goto L_0x006e
        L_0x04a8:
            if (r21 == 0) goto L_0x007b
            r0 = r36
            short r4 = r0.diw
            r4 = r4 | 32
            short r4 = (short) r4
            r0 = r36
            r0.diw = r4
            r4 = r12 & 32
            if (r4 == 0) goto L_0x04cd
            long r4 = java.lang.System.currentTimeMillis()
            long r4 = r4 + r16
            r0 = r36
            long r6 = r0.dit
            long r4 = java.lang.Math.min(r4, r6)
            r0 = r36
            r0.dit = r4
            goto L_0x007b
        L_0x04cd:
            long r4 = java.lang.System.currentTimeMillis()
            long r4 = r4 + r16
            r0 = r36
            r0.dit = r4
            goto L_0x007b
        L_0x04d9:
            r4 = r16
            r6 = r18
            r8 = r15
            r9 = r20
            r10 = r21
            goto L_0x0131
        L_0x04e4:
            r4 = r13
            r5 = r14
            goto L_0x0368
        L_0x04e8:
            r12 = r4
            goto L_0x0056
        */
        throw new UnsupportedOperationException("Method not decompiled: logic.data.mbean.C0677Jd.mo3160a(a.afk, long):boolean");
    }

    /* renamed from: b */
    public boolean mo3187b(C6064afk afk) {
        return true;
    }

    /* renamed from: a */
    private C5546aNm m5846a(C2759jd jdVar) {
        for (C5546aNm anm = this.dis; anm != null; anm = anm.irv) {
            if (anm.f3436Ul == jdVar) {
                return anm;
            }
        }
        return null;
    }

    /* renamed from: g */
    public Object mo3199g(C5663aRz arz) {
        switch (arz.mo11290ED()) {
            case 'B':
                return Byte.valueOf(mo3203i(arz));
            case 'C':
                return Character.valueOf(mo3206j(arz));
            case 'D':
                return Double.valueOf(mo3207k(arz));
            case 'F':
                return Float.valueOf(mo3211m(arz));
            case 'I':
                return Integer.valueOf(mo3212n(arz));
            case 'J':
                return Long.valueOf(mo3213o(arz));
            case 'S':
                return Short.valueOf(mo3215q(arz));
            case 'Z':
                return Boolean.valueOf(mo3201h(arz));
            default:
                return mo3214p(arz);
        }
    }

    /* renamed from: h */
    public boolean mo3201h(C5663aRz arz) {
        if (this.div != 0) {
            int T = mo11921T(arz);
            if ((T & 3) == 0) {
                m5847a(arz, T);
                boolean D = this.dip.mo11905D(arz);
                mo11928b(arz, D);
                return D;
            }
        }
        return mo11905D(arz);
    }

    /* renamed from: i */
    public byte mo3203i(C5663aRz arz) {
        if (this.div != 0) {
            int T = mo11921T(arz);
            if ((T & 3) == 0) {
                m5847a(arz, T);
                byte E = this.dip.mo11906E(arz);
                mo11923b(arz, E);
                return E;
            }
        }
        return mo11906E(arz);
    }

    /* renamed from: j */
    public char mo3206j(C5663aRz arz) {
        if (this.div != 0) {
            int T = mo11921T(arz);
            if ((T & 3) == 0) {
                m5847a(arz, T);
                char G = this.dip.mo11908G(arz);
                mo11924b(arz, G);
                return G;
            }
        }
        return mo11908G(arz);
    }

    /* renamed from: k */
    public double mo3207k(C5663aRz arz) {
        if (this.div != 0) {
            int T = mo11921T(arz);
            if ((T & 3) == 0) {
                m5847a(arz, T);
                double J = this.dip.mo11911J(arz);
                mo11925b(arz, J);
                return J;
            }
        }
        return mo11911J(arz);
    }

    /* renamed from: l */
    public long mo3210l(C5663aRz arz) {
        if (this.div == 1) {
            return this.dip.mo3210l(arz);
        }
        return super.mo3210l(arz);
    }

    /* renamed from: kW */
    public long mo3208kW(int i) {
        return mo3210l(mo309c()[i]);
    }

    /* renamed from: m */
    public float mo3211m(C5663aRz arz) {
        if (this.div != 0) {
            int T = mo11921T(arz);
            if ((T & 3) == 0) {
                m5847a(arz, T);
                float I = this.dip.mo11910I(arz);
                mo11926b(arz, I);
                return I;
            }
        }
        return mo11910I(arz);
    }

    /* renamed from: n */
    public int mo3212n(C5663aRz arz) {
        if (this.div != 0) {
            int T = mo11921T(arz);
            if ((T & 3) == 0) {
                m5847a(arz, T);
                int C = this.dip.mo11904C(arz);
                mo11936d(arz, C);
                return C;
            }
        }
        return mo11904C(arz);
    }

    /* renamed from: o */
    public long mo3213o(C5663aRz arz) {
        if (this.div != 0) {
            int T = mo11921T(arz);
            if ((T & 3) == 0) {
                m5847a(arz, T);
                long H = this.dip.mo11909H(arz);
                mo11933c(arz, H);
                return H;
            }
        }
        return mo11909H(arz);
    }

    public byte aXk() {
        return this.div;
    }

    /* renamed from: p */
    public Object mo3214p(C5663aRz arz) {
        if (this.div != 0) {
            int T = mo11921T(arz);
            if ((T & 3) == 0) {
                Object p = this.dip.mo3214p(arz);
                m5848a(arz, T, p);
                if (p == null) {
                    return null;
                }
                Object a = arz.mo2185a(this, p);
                mo11941l(arz, a);
                return a;
            }
        }
        return mo11903B(arz);
    }

    /* renamed from: q */
    public short mo3215q(C5663aRz arz) {
        if (this.div != 0) {
            int T = mo11921T(arz);
            if ((T & 3) == 0) {
                m5847a(arz, T);
                short F = this.dip.mo11907F(arz);
                mo11927b(arz, F);
                return F;
            }
        }
        return mo11907F(arz);
    }

    public long aBt() {
        if (this.dip != null) {
            return this.dip.aBt();
        }
        return this.cmb;
    }

    public RunTheUnsafe aXl() {
        return this.dip;
    }

    /* renamed from: r */
    public Object mo3216r(C5663aRz arz) {
        switch (arz.mo11290ED()) {
            case 'B':
                return Byte.valueOf(mo11906E(arz));
            case 'C':
                return Character.valueOf(mo11908G(arz));
            case 'D':
                return Double.valueOf(mo11911J(arz));
            case 'F':
                return Float.valueOf(mo11910I(arz));
            case 'I':
                return Integer.valueOf(mo11904C(arz));
            case 'J':
                return Long.valueOf(mo11909H(arz));
            case 'S':
                return Short.valueOf(mo11907F(arz));
            case 'Z':
                return Boolean.valueOf(mo11905D(arz));
            default:
                return mo11903B(arz);
        }
    }

    /* renamed from: kX */
    public Object mo3209kX(int i) {
        return mo3216r(mo309c()[i]);
    }

    /* renamed from: d */
    public void mo3193d(C5663aRz arz, Object obj) {
        switch (arz.mo11290ED()) {
            case 'B':
                mo11923b(arz, ((Byte) obj).byteValue());
                return;
            case 'C':
                mo11924b(arz, ((Character) obj).charValue());
                return;
            case 'D':
                mo11925b(arz, ((Double) obj).doubleValue());
                return;
            case 'F':
                mo11926b(arz, ((Float) obj).floatValue());
                return;
            case 'I':
                mo11936d(arz, ((Integer) obj).intValue());
                return;
            case 'J':
                mo11933c(arz, ((Long) obj).longValue());
                return;
            case 'S':
                mo11927b(arz, ((Short) obj).shortValue());
                return;
            case 'Z':
                mo11928b(arz, ((Boolean) obj).booleanValue());
                return;
            default:
                mo11941l(arz, obj);
                return;
        }
    }

    /* renamed from: hF */
    public boolean mo3202hF() {
        return (this.diw & ClassFileWriter.ACC_NATIVE) != 0;
    }

    public boolean aXm() {
        return (this.diw & 2) != 0;
    }

    /* renamed from: d */
    public void mo3192d(IReadExternal vm) {
        byte b;
        C5663aRz[] c = mo309c();
        this.dio = vm.readLong("fromVersion");
        this.cmb = vm.readLong("version");
        int gX = vm.readInt("changeCount");
        this.dby = gX;
        vm.mo6369hg("changes");
        while (true) {
            int i = gX - 1;
            if (i < 0) {
                vm.mo6370pe();
                return;
            }
            C2759jd jdVar = (C2759jd) c[vm.readInt("mid")];
            long EC = jdVar.mo19961EC();
            byte b2 = unsafe.getByte(this, EC);
            long EB = jdVar.mo19960EB();
            long EA = jdVar.mo19959EA();
            unsafe.putLong(this, EB, this.cmb);
            switch (jdVar.mo11290ED()) {
                case 'B':
                    unsafe.putByte(this, EA, vm.readByte(jdVar.getName()));
                    b = b2;
                    break;
                case 'C':
                    unsafe.putChar(this, EA, vm.readChar(jdVar.getName()));
                    b = b2;
                    break;
                case 'D':
                    unsafe.putDouble(this, EA, vm.readDouble(jdVar.getName()));
                    b = b2;
                    break;
                case 'F':
                    unsafe.putFloat(this, EA, vm.readFloat(jdVar.getName()));
                    b = b2;
                    break;
                case 'I':
                    unsafe.putInt(this, EA, vm.readInt(jdVar.getName()));
                    b = b2;
                    break;
                case 'J':
                    unsafe.putLong(this, EA, vm.readLong(jdVar.getName()));
                    b = b2;
                    break;
                case 'L':
                case '[':
                    if (!vm.mo6354gR("isDiff")) {
                        unsafe.putObject(this, EA, vm.mo6366hd(jdVar.getName()));
                        b = b2;
                        break;
                    } else {
                        C5546aNm Et = jdVar.mo19968Et();
                        vm.mo6369hg("change");
                        Et.mo885b(this.f3984Uk, vm);
                        vm.mo6370pe();
                        m5849b(Et);
                        b = b2 | 8;
                        break;
                    }
                case 'S':
                    unsafe.putShort(this, EA, vm.mo6357gU(jdVar.getName()));
                    b = b2;
                    break;
                case 'Z':
                    unsafe.putBoolean(this, EA, vm.mo6354gR(jdVar.getName()));
                    b = b2;
                    break;
                default:
                    aXn();
                    throw new IllegalArgumentException("Unknow java type char: " + jdVar.mo11290ED());
            }
            unsafe.putByte(this, EC, (byte) (b | 4));
            gX = i;
        }
    }

    /* renamed from: i */
    public void mo3204i(ObjectInput objectInput) {
        int readInt;
        byte b;
        C5663aRz[] c = mo309c();
        this.dio = objectInput.readLong();
        this.cmb = objectInput.readLong();
        int readInt2 = objectInput.readInt();
        this.dby = readInt2;
        C2759jd jdVar = null;
        int i = 0;
        while (true) {
            readInt = objectInput.readInt();
            if (readInt == -1) {
                if (i != readInt2) {
                    logger.warn("Unexpected end of changes expecting " + readInt2 + " but found " + i);
                    this.dby = i;
                    return;
                }
                return;
            } else if (readInt >= c.length || readInt < 0) {
                aXn();
            } else {
                try {
                    jdVar = (C2759jd) c[readInt];
                    long EC = jdVar.mo19961EC();
                    byte b2 = unsafe.getByte(this, EC);
                    long EB = jdVar.mo19960EB();
                    long EA = jdVar.mo19959EA();
                    unsafe.putLong(this, EB, this.cmb);
                    switch (jdVar.mo11290ED()) {
                        case 'B':
                            unsafe.putByte(this, EA, objectInput.readByte());
                            b = b2;
                            break;
                        case 'C':
                            unsafe.putChar(this, EA, objectInput.readChar());
                            b = b2;
                            break;
                        case 'D':
                            unsafe.putDouble(this, EA, objectInput.readDouble());
                            b = b2;
                            break;
                        case 'F':
                            unsafe.putFloat(this, EA, objectInput.readFloat());
                            b = b2;
                            break;
                        case 'I':
                            unsafe.putInt(this, EA, objectInput.readInt());
                            b = b2;
                            break;
                        case 'J':
                            unsafe.putLong(this, EA, objectInput.readLong());
                            b = b2;
                            break;
                        case 'L':
                        case '[':
                            if (!objectInput.readBoolean()) {
                                unsafe.putObject(this, EA, objectInput.readObject());
                                b = b2;
                                break;
                            } else {
                                C5546aNm Et = jdVar.mo19968Et();
                                Et.mo886b(this.f3984Uk, objectInput);
                                m5849b(Et);
                                b = b2 | 8;
                                break;
                            }
                        case 'S':
                            unsafe.putShort(this, EA, objectInput.readShort());
                            b = b2;
                            break;
                        case 'Z':
                            unsafe.putBoolean(this, EA, objectInput.readBoolean());
                            b = b2;
                            break;
                        default:
                            aXn();
                            throw new IllegalArgumentException("Unknow java type char: " + jdVar.mo11290ED());
                    }
                    unsafe.putByte(this, EC, (byte) (b | 4));
                    i++;
                } catch (Exception e) {
                    aXn();
                    throw new CountFailedReadOrWriteException("Error deserializing " + this.f3984Uk.bFf().bFY() + "." + jdVar, e);
                }
            }
        }
        aXn();
        throw new CountFailedReadOrWriteException("Invalid field index: " + readInt + ">=" + c.length + ". Remaining " + i + ", Last read: " + jdVar);
    }

    public void readExternal(IReadExternal vm) {
        C5663aRz[] c = mo309c();
        this.cmb = vm.readLong("version");
        vm.mo6369hg("fields");
        int length = c.length;
        int i = 0;
        while (true) {
            int i2 = length - 1;
            if (i2 < 0) {
                vm.mo6370pe();
                return;
            }
            C2759jd jdVar = (C2759jd) c[i];
            try {
                if (jdVar.mo11300Ew() != C1020Ot.C1021a.NOT_PERSISTED) {
                    long EA = jdVar.mo19959EA();
                    switch (jdVar.mo11290ED()) {
                        case 'B':
                            unsafe.putByte(this, EA, vm.readByte(jdVar.getName()));
                            break;
                        case 'C':
                            unsafe.putChar(this, EA, vm.readChar(jdVar.getName()));
                            break;
                        case 'D':
                            unsafe.putDouble(this, EA, vm.readDouble(jdVar.getName()));
                            break;
                        case 'F':
                            unsafe.putFloat(this, EA, vm.readFloat(jdVar.getName()));
                            break;
                        case 'I':
                            unsafe.putInt(this, EA, vm.readInt(jdVar.getName()));
                            break;
                        case 'J':
                            unsafe.putLong(this, EA, vm.readLong(jdVar.getName()));
                            break;
                        case 'L':
                        case '[':
                            if (!vm.mo6369hg(jdVar.getName())) {
                                break;
                            } else {
                                unsafe.putObject(this, EA, jdVar.mo2186a(this.f3984Uk, vm));
                                vm.mo6370pe();
                                break;
                            }
                        case 'S':
                            unsafe.putShort(this, EA, vm.mo6357gU(jdVar.getName()));
                            break;
                        case 'Z':
                            unsafe.putBoolean(this, EA, vm.mo6354gR(jdVar.getName()));
                            break;
                        default:
                            mo3185b(jdVar);
                            throw new IllegalArgumentException("Unknow java type char: " + jdVar.mo11290ED());
                    }
                }
                length = i2;
                i++;
            } catch (Exception e) {
                mo3185b(jdVar);
                throw new CountFailedReadOrWriteException("Error reading field " + jdVar, e);
            }
        }
    }

    public void readExternal(ObjectInput objectInput) {
        mo3158a(objectInput, -1);
    }

    /* renamed from: a */
    public void mo3158a(ObjectInput objectInput, int i) {
        int i2 = 0;
        C5663aRz[] c = mo309c();
        boolean z = (65536 & i) != 0;
        this.cmb = objectInput.readLong();
        int length = c.length;
        while (true) {
            int i3 = i2;
            int i4 = length - 1;
            if (i4 >= 0) {
                C2759jd jdVar = (C2759jd) c[i3];
                if (z || !jdVar.mo7393ho()) {
                    try {
                        long EA = jdVar.mo19959EA();
                        switch (jdVar.mo11290ED()) {
                            case 'B':
                                unsafe.putByte(this, EA, objectInput.readByte());
                                break;
                            case 'C':
                                unsafe.putChar(this, EA, objectInput.readChar());
                                break;
                            case 'D':
                                unsafe.putDouble(this, EA, objectInput.readDouble());
                                break;
                            case 'F':
                                unsafe.putFloat(this, EA, objectInput.readFloat());
                                break;
                            case 'I':
                                unsafe.putInt(this, EA, objectInput.readInt());
                                break;
                            case 'J':
                                unsafe.putLong(this, EA, objectInput.readLong());
                                break;
                            case 'L':
                            case '[':
                                unsafe.putObject(this, EA, jdVar.mo11304a(this.f3984Uk, objectInput));
                                break;
                            case 'S':
                                unsafe.putShort(this, EA, objectInput.readShort());
                                break;
                            case 'Z':
                                unsafe.putBoolean(this, EA, objectInput.readBoolean());
                                break;
                            default:
                                mo3185b(jdVar);
                                throw new IllegalArgumentException("Unknow java type char: " + jdVar.mo11290ED());
                        }
                    } catch (Exception e) {
                        mo3185b(jdVar);
                        throw new CountFailedReadOrWriteException("Error deserializing " + this.f3984Uk.bFf().bFY() + "." + jdVar, e);
                    }
                }
                i2 = i3 + 1;
                length = i4;
            } else {
                return;
            }
        }
    }

    public void aXn() {
        mo3185b((C2759jd) null);
    }

    /* renamed from: b */
    public void mo3185b(C2759jd jdVar) {
        C5663aRz[] c = mo309c();
        System.out.println("Script class: " + this.f3984Uk.getClass());
        System.out.println("Error field: " + jdVar);
        System.out.println("State:   " + this.diw);
        System.out.println("version: " + this.cmb);
        int i = 0;
        while (true) {
            int i2 = i;
            if (i2 < c.length) {
                C2759jd jdVar2 = (C2759jd) c[i2];
                System.out.println(String.valueOf(i2) + ": " + jdVar2 + " type:" + jdVar2.mo11291El() + " serializer:" + jdVar2.mo19962EE().getClass());
                if (jdVar2.mo11290ED() != 'L') {
                    System.out.println("  value:   " + mo3216r(jdVar2));
                }
                System.out.println("  flags:   " + mo11921T(jdVar2));
                System.out.println("  version: " + mo3210l(jdVar2));
                i = i2 + 1;
            } else {
                return;
            }
        }
    }

    public void reset() {
        mo310d();
        this.cmb = 0;
        this.f3984Uk = null;
        this.dio = 0;
        this.cVz = null;
        this.bzZ = null;
        this.dip = null;
        this.diq = 0;
        this.dby = 0;
        this.dis = null;
        this.dit = 0;
        this.diu = 0;
        this.div = 1;
        this.diw = 0;
    }

    /* renamed from: e */
    public void mo3195e(C5663aRz arz, Object obj) {
        switch (arz.mo11290ED()) {
            case 'B':
                mo3147a(arz, ((Byte) obj).byteValue());
                return;
            case 'C':
                mo3148a(arz, ((Character) obj).charValue());
                return;
            case 'D':
                mo3149a(arz, ((Double) obj).doubleValue());
                return;
            case 'F':
                mo3150a(arz, ((Float) obj).floatValue());
                return;
            case 'I':
                mo3183b(arz, ((Integer) obj).intValue());
                return;
            case 'J':
                mo3184b(arz, ((Long) obj).longValue());
                return;
            case 'S':
                mo3152a(arz, ((Short) obj).shortValue());
                return;
            case 'Z':
                mo3153a(arz, ((Boolean) obj).booleanValue());
                return;
            default:
                mo3197f(arz, obj);
                return;
        }
    }

    /* renamed from: a */
    public void mo3153a(C5663aRz arz, boolean z) {
        if (this.div == 1) {
            int T = mo11921T(arz);
            if ((T & 2) == 0) {
                mo3189c(arz, T | 2);
            }
            mo11928b(arz, z);
        } else if (mo3198f(arz)) {
            mo11928b(arz, z);
        }
    }

    /* renamed from: a */
    public void mo3147a(C5663aRz arz, byte b) {
        if (this.div == 1) {
            int T = mo11921T(arz);
            if ((T & 2) == 0) {
                mo3189c(arz, T | 2);
            }
            mo11923b(arz, b);
        } else if (mo3198f(arz)) {
            mo11923b(arz, b);
        }
    }

    /* renamed from: a */
    public void mo3148a(C5663aRz arz, char c) {
        if (this.div == 1) {
            int T = mo11921T(arz);
            if ((T & 2) == 0) {
                mo3189c(arz, T | 2);
            }
            mo11924b(arz, c);
        } else if (mo3198f(arz)) {
            mo11924b(arz, c);
        }
    }

    /* renamed from: a */
    public void mo3149a(C5663aRz arz, double d) {
        if (this.div == 1) {
            int T = mo11921T(arz);
            if ((T & 2) == 0) {
                mo3189c(arz, T | 2);
            }
            mo11925b(arz, d);
        } else if (mo3198f(arz)) {
            mo11925b(arz, d);
        }
    }

    /* renamed from: a */
    public void mo3151a(C5663aRz arz, long j) {
        if (this.div == 1) {
            throw new IllegalAccessError();
        }
        super.mo3151a(arz, j);
    }

    /* renamed from: a */
    public void mo3150a(C5663aRz arz, float f) {
        if (this.div == 1) {
            int T = mo11921T(arz);
            if ((T & 2) == 0) {
                mo3189c(arz, T | 2);
            }
            mo11926b(arz, f);
        } else if (mo3198f(arz)) {
            mo11926b(arz, f);
        }
    }

    /* renamed from: A */
    public void mo3146A(boolean z) {
        if (z) {
            this.diw = (short) (this.diw | ClassFileWriter.ACC_NATIVE);
        } else {
            this.diw = (short) (this.diw & -257);
        }
    }

    /* renamed from: b */
    public void mo3183b(C5663aRz arz, int i) {
        if (this.div == 1) {
            int T = mo11921T(arz);
            if ((T & 2) == 0) {
                mo3189c(arz, T | 2);
            }
            mo11936d(arz, i);
        } else if (mo3198f(arz)) {
            mo11936d(arz, i);
        }
    }

    /* renamed from: b */
    public void mo3184b(C5663aRz arz, long j) {
        if (this.div == 1) {
            int T = mo11921T(arz);
            if ((T & 2) == 0) {
                mo3189c(arz, T | 2);
            }
            mo11933c(arz, j);
        } else if (mo3198f(arz)) {
            mo11933c(arz, j);
        }
    }

    public void setMode(int i) {
        this.div = (byte) i;
    }

    /* renamed from: f */
    public void mo3197f(C5663aRz arz, Object obj) {
        if (this.div == 1) {
            int T = mo11921T(arz);
            if ((T & 2) == 0) {
                mo3189c(arz, T | 2);
            }
            mo11941l(arz, obj);
        } else if (mo3198f(arz)) {
            mo11941l(arz, obj);
        }
    }

    /* renamed from: a */
    public void mo3152a(C5663aRz arz, short s) {
        if (this.div == 1) {
            int T = mo11921T(arz);
            if ((T & 2) == 0) {
                mo3189c(arz, T | 2);
            }
            mo11927b(arz, s);
        } else if (mo3198f(arz)) {
            mo11927b(arz, s);
        }
    }

    /* renamed from: eU */
    public void mo3196eU(long j) {
        if (this.div == 1) {
            throw new IllegalAccessError();
        }
        super.mo3196eU(j);
    }

    /* renamed from: a */
    public void mo3154a(RunTheUnsafe avo) {
        this.dip = avo;
    }

    /* renamed from: a */
    public void mo3155a(aWq awq, C3582se seVar) {
        this.bzZ = awq;
        this.cVz = seVar;
        this.dip = (RunTheUnsafe) seVar.cVo();
        this.div = 1;
        mo3146A(false);
    }

    public aWq aXo() {
        return this.bzZ;
    }

    /* renamed from: a */
    public void mo3157a(IWriteExternal att, int i) {
        C5663aRz[] c = mo309c();
        att.writeLong("fromVersion", this.dio);
        att.writeLong("version", this.cmb);
        att.writeInt("changeCount", m5845a(i, c));
        att.writeString("changes");
        int length = c.length;
        int i2 = 0;
        while (true) {
            int i3 = length - 1;
            if (i3 < 0) {
                att.mo16277pe();
                return;
            }
            C2759jd jdVar = (C2759jd) c[i2];
            byte b = unsafe.getByte(this, jdVar.mo19961EC());
            if ((b & i) != 0) {
                long EA = jdVar.mo19959EA();
                att.writeInt("mid", jdVar.mo7395hq());
                switch (jdVar.mo11290ED()) {
                    case 'B':
                        att.writeByte(jdVar.getName(), unsafe.getByte(this, EA));
                        break;
                    case 'C':
                        att.writeChar(jdVar.getName(), unsafe.getChar(this, EA));
                        break;
                    case 'D':
                        att.writeDouble(jdVar.getName(), unsafe.getDouble(this, EA));
                        break;
                    case 'F':
                        att.writeFloat(jdVar.getName(), unsafe.getFloat(this, EA));
                        break;
                    case 'I':
                        att.writeInt(jdVar.getName(), unsafe.getInt(this, EA));
                        break;
                    case 'J':
                        att.writeLong(jdVar.getName(), unsafe.getLong(this, EA));
                        break;
                    case 'L':
                        if ((b & 8) == 0) {
                            att.writeBoolean("isDiff", false);
                            att.mo16273g(jdVar.getName(), unsafe.getObject(this, EA));
                            break;
                        } else {
                            att.writeBoolean("isDiff", true);
                            C5546aNm anm = this.dis;
                            while (anm != null) {
                                if (anm.f3436Ul == jdVar) {
                                    att.writeString("change");
                                    anm.mo882a(this.f3984Uk, att);
                                    att.mo16277pe();
                                    break;
                                } else {
                                    anm = anm.irv;
                                }
                            }
                            aXn();
                            throw new IllegalStateException("HasDiff bit was true but no diff found for field: " + jdVar);
                        }
                    case 'S':
                        att.writeShort(jdVar.getName(), (int) unsafe.getShort(this, EA));
                        break;
                    case 'Z':
                        att.writeBoolean(jdVar.getName(), unsafe.getBoolean(this, EA));
                        break;
                    case '[':
                        att.mo16273g(jdVar.getName(), unsafe.getObject(this, EA));
                        break;
                    default:
                        aXn();
                        throw new IllegalArgumentException("Unknow java type char: " + jdVar.mo11290ED());
                }
            }
            length = i3;
            i2++;
        }
    }

    /* renamed from: a */
    public void mo3159a(ObjectOutput objectOutput, int i) {
        int i2;
        C5663aRz[] c = mo309c();
        boolean z = (65536 & i) != 0;
        objectOutput.writeLong(this.dio);
        objectOutput.writeLong(this.cmb);
        int a = m5845a(i, c);
        objectOutput.writeInt(a);
        int length = c.length;
        int i3 = 0;
        int i4 = 0;
        while (true) {
            length--;
            if (length < 0) {
                objectOutput.writeInt(-1);
                if (i4 != a) {
                    aXn();
                    throw new CountFailedReadOrWriteException("Writen change count does not match expected: written=" + i4 + " expected changes=" + a + " for " + this.f3984Uk.bFf().bFY());
                }
                return;
            }
            C2759jd jdVar = (C2759jd) c[i3];
            try {
                byte b = unsafe.getByte(this, jdVar.mo19961EC());
                if ((b & i) == 0) {
                    i2 = i4;
                } else if (z || !jdVar.mo7393ho()) {
                    int i5 = i4 + 1;
                    long EA = jdVar.mo19959EA();
                    objectOutput.writeInt(jdVar.mo7395hq());
                    switch (jdVar.mo11290ED()) {
                        case 'B':
                            objectOutput.writeByte(unsafe.getByte(this, EA));
                            i2 = i5;
                            break;
                        case 'C':
                            objectOutput.writeChar(unsafe.getChar(this, EA));
                            i2 = i5;
                            break;
                        case 'D':
                            objectOutput.writeDouble(unsafe.getDouble(this, EA));
                            i2 = i5;
                            break;
                        case 'F':
                            objectOutput.writeFloat(unsafe.getFloat(this, EA));
                            i2 = i5;
                            break;
                        case 'I':
                            objectOutput.writeInt(unsafe.getInt(this, EA));
                            i2 = i5;
                            break;
                        case 'J':
                            objectOutput.writeLong(unsafe.getLong(this, EA));
                            i2 = i5;
                            break;
                        case 'L':
                            if ((b & 8) == 0) {
                                objectOutput.writeBoolean(false);
                                objectOutput.writeObject(unsafe.getObject(this, EA));
                                i2 = i5;
                                break;
                            } else {
                                objectOutput.writeBoolean(true);
                                C5546aNm anm = this.dis;
                                while (anm != null) {
                                    if (anm.f3436Ul == jdVar) {
                                        anm.mo883a(this.f3984Uk, objectOutput);
                                        i2 = i5;
                                        break;
                                    } else {
                                        anm = anm.irv;
                                    }
                                }
                                aXn();
                                throw new IllegalStateException("HasDiff bit was true but no diff found for field: " + jdVar);
                            }
                        case 'S':
                            objectOutput.writeShort(unsafe.getShort(this, EA));
                            i2 = i5;
                            break;
                        case 'Z':
                            objectOutput.writeBoolean(unsafe.getBoolean(this, EA));
                            i2 = i5;
                            break;
                        case '[':
                            objectOutput.writeObject(unsafe.getObject(this, EA));
                            i2 = i5;
                            break;
                        default:
                            aXn();
                            throw new IllegalArgumentException("Unknow java type char: " + jdVar.mo11290ED());
                    }
                } else {
                    i2 = i4;
                }
                i3++;
                i4 = i2;
            } catch (Exception e) {
                aXn();
                throw new CountFailedReadOrWriteException("Error writting field " + jdVar, e);
            }
        }
    }

    /* renamed from: a */
    private int m5845a(int i, C5663aRz[] arzArr) {
        int i2 = 0;
        if (i == -1) {
            return this.dby;
        }
        boolean z = (65536 & i) != 0;
        int length = arzArr.length;
        while (true) {
            int i3 = length - 1;
            if (i3 < 0) {
                return i2;
            }
            C2759jd jdVar = arzArr[i3];
            if ((unsafe.getByte(this, jdVar.mo19961EC()) & i) == 0) {
                length = i3;
            } else if (z || !jdVar.mo7393ho()) {
                i2++;
                length = i3;
            } else {
                length = i3;
            }
        }
    }

    public void writeExternal(IWriteExternal att) {
        C5663aRz[] c = mo309c();
        att.writeLong("version", this.cmb);
        att.writeString("fields");
        int length = c.length;
        int i = 0;
        while (true) {
            int i2 = length - 1;
            if (i2 < 0) {
                att.mo16277pe();
                return;
            }
            C2759jd jdVar = (C2759jd) c[i];
            if (jdVar.mo11300Ew() != C1020Ot.C1021a.NOT_PERSISTED) {
                try {
                    long EA = jdVar.mo19959EA();
                    switch (jdVar.mo11290ED()) {
                        case 'B':
                            att.writeByte(jdVar.getName(), unsafe.getByte(this, EA));
                            break;
                        case 'C':
                            att.writeChar(jdVar.getName(), unsafe.getChar(this, EA));
                            break;
                        case 'D':
                            att.writeDouble(jdVar.getName(), unsafe.getDouble(this, EA));
                            break;
                        case 'F':
                            att.writeFloat(jdVar.getName(), unsafe.getFloat(this, EA));
                            break;
                        case 'I':
                            att.writeInt(jdVar.getName(), unsafe.getInt(this, EA));
                            break;
                        case 'J':
                            att.writeLong(jdVar.getName(), unsafe.getLong(this, EA));
                            break;
                        case 'L':
                        case '[':
                            att.writeString(jdVar.getName());
                            jdVar.mo2187a(this.f3984Uk, att, unsafe.getObject(this, EA));
                            att.mo16277pe();
                            break;
                        case 'S':
                            att.writeShort(jdVar.getName(), (int) unsafe.getShort(this, EA));
                            break;
                        case 'Z':
                            att.writeBoolean(jdVar.getName(), unsafe.getBoolean(this, EA));
                            break;
                        default:
                            aXn();
                            throw new IllegalArgumentException("Unknow java type char: " + jdVar.mo11290ED());
                    }
                } catch (Exception e) {
                    aXn();
                    throw new CountFailedReadOrWriteException("Error serializing field " + jdVar, e);
                }
            }
            i++;
            length = i2;
        }
    }

    public void writeExternal(ObjectOutput objectOutput) {
        mo3186b(objectOutput, -1);
    }

    /* renamed from: b */
    public void mo3186b(ObjectOutput objectOutput, int i) {
        C5663aRz[] c = mo309c();
        boolean z = (65536 & i) != 0;
        objectOutput.writeLong(this.cmb);
        int length = c.length;
        int i2 = 0;
        while (true) {
            int i3 = length - 1;
            if (i3 >= 0) {
                C2759jd jdVar = (C2759jd) c[i2];
                if (z || !jdVar.mo7393ho()) {
                    long EA = jdVar.mo19959EA();
                    switch (jdVar.mo11290ED()) {
                        case 'B':
                            objectOutput.writeByte(unsafe.getByte(this, EA));
                            break;
                        case 'C':
                            objectOutput.writeChar(unsafe.getChar(this, EA));
                            break;
                        case 'D':
                            objectOutput.writeDouble(unsafe.getDouble(this, EA));
                            break;
                        case 'F':
                            objectOutput.writeFloat(unsafe.getFloat(this, EA));
                            break;
                        case 'I':
                            objectOutput.writeInt(unsafe.getInt(this, EA));
                            break;
                        case 'J':
                            objectOutput.writeLong(unsafe.getLong(this, EA));
                            break;
                        case 'L':
                        case '[':
                            jdVar.mo11305a(this.f3984Uk, objectOutput, unsafe.getObject(this, EA));
                            break;
                        case 'S':
                            objectOutput.writeShort(unsafe.getShort(this, EA));
                            break;
                        case 'Z':
                            objectOutput.writeBoolean(unsafe.getBoolean(this, EA));
                            break;
                        default:
                            aXn();
                            throw new IllegalArgumentException("Unknow java type char: " + jdVar.mo11290ED());
                    }
                }
                i2++;
                length = i3;
            } else {
                return;
            }
        }
    }

    /* renamed from: s */
    public boolean mo3219s(C5663aRz arz) {
        return (unsafe.getByte(this, ((C2759jd) arz).mo19961EC()) & 4) != 0;
    }

    /* renamed from: t */
    public boolean mo3221t(C5663aRz arz) {
        return (unsafe.getByte(this, ((C2759jd) arz).mo19961EC()) & 2) != 0;
    }

    /* renamed from: u */
    public boolean mo3222u(C5663aRz arz) {
        return (unsafe.getByte(this, ((C2759jd) arz).mo19961EC()) & 1) != 0;
    }

    /* access modifiers changed from: protected */
    /* renamed from: c */
    public void mo3189c(C5663aRz arz, int i) {
        super.mo11940f(arz, i);
        this.diw = (short) (this.diw | i);
    }

    public C6195aiL aXp() {
        return null;
    }

    /* renamed from: a */
    public void mo3156a(C6195aiL ail) {
    }

    public int aRA() {
        return this.dby;
    }

    public boolean aXq() {
        return (this.diw & 144) != 0;
    }

    public boolean aXr() {
        return (this.diw & 96) != 0;
    }

    public boolean aXs() {
        return (this.diw & 64) != 0;
    }

    public boolean aXt() {
        return (this.diw & ClassFileWriter.ACC_TRANSIENT) != 0;
    }

    public boolean aXu() {
        return (this.diw & 32) != 0;
    }

    public boolean aXv() {
        return (this.diw & 16) != 0;
    }

    public boolean aXw() {
        return (this.diw & ClassFileWriter.ACC_TRANSIENT) != 0;
    }

    public boolean aXx() {
        return (this.diw & 64) != 0;
    }

    /* renamed from: cR */
    public void mo3190cR(boolean z) {
        if (z) {
            this.diw = (short) (this.diw | 512);
        } else {
            this.diw = (short) (this.diw & -513);
        }
    }

    public boolean isCreated() {
        return (this.diw & 512) != 0;
    }

    /* renamed from: cS */
    public void mo3191cS(boolean z) {
        if (this.div == 1) {
            this.diw = (short) (this.diw | 2);
        }
        if (z) {
            this.diw = (short) (this.diw | ClassFileWriter.ACC_ABSTRACT);
        } else {
            this.diw = (short) (this.diw & -1025);
        }
    }

    /* renamed from: du */
    public boolean mo3194du() {
        return (this.diw & ClassFileWriter.ACC_ABSTRACT) != 0;
    }

    public void aXy() {
        this.diw = (short) (this.diw & -17);
    }

    public void aXz() {
        this.diw = (short) (this.diw & -33);
    }

    /* renamed from: v */
    public void mo3223v(C5663aRz arz) {
        if (this.div == 1) {
            int T = mo11921T(arz);
            if ((T & 2) == 0) {
                mo3189c(arz, T | 2);
                this.diw = (short) (this.diw | 2);
            }
        } else if (!this.f3984Uk.bFf().mo6866PM().bGo() && !mo3194du() && !this.f3984Uk.bFf().mo6887du()) {
            if (this.f3984Uk.bFf().mo6866PM().bHa() || this.f3984Uk.getClass().getAnnotation(C0909NL.class) == null) {
                throw new IllegalAccessError("Illegal call");
            }
        }
    }

    public long aXA() {
        return this.dio;
    }

    public long aXB() {
        return this.dit;
    }

    public long aXC() {
        return this.diu;
    }
}
