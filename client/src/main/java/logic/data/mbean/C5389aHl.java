package logic.data.mbean;

import game.network.message.serializable.C2686iZ;
import game.script.itemgen.ItemGenEntry;
import game.script.itemgen.ItemGenSet;
import logic.baa.C1616Xf;
import logic.res.code.C5663aRz;
import p001a.C0064Am;

import java.util.UUID;

/* renamed from: a.aHl  reason: case insensitive filesystem */
public class C5389aHl extends C5292aDs {
    @C0064Am(aul = "a97d2123c0f7240bc232c149168377b1", aum = 2)

    /* renamed from: bK */
    public UUID f2996bK;
    @C0064Am(aul = "e7832864a76c1941e4bcffa1de2903ad", aum = -2)
    public long hWA;
    @C0064Am(aul = "e7832864a76c1941e4bcffa1de2903ad", aum = -1)
    public byte hWB;
    @C0064Am(aul = "e7832864a76c1941e4bcffa1de2903ad", aum = 1)
    public C2686iZ<ItemGenEntry> hWz;
    @C0064Am(aul = "e9e6d1183c202bb2d390712b21ad9a85", aum = 0)
    public String handle;
    @C0064Am(aul = "a97d2123c0f7240bc232c149168377b1", aum = -2)

    /* renamed from: oL */
    public long f2997oL;
    @C0064Am(aul = "a97d2123c0f7240bc232c149168377b1", aum = -1)

    /* renamed from: oS */
    public byte f2998oS;
    @C0064Am(aul = "e9e6d1183c202bb2d390712b21ad9a85", aum = -2)

    /* renamed from: ok */
    public long f2999ok;
    @C0064Am(aul = "e9e6d1183c202bb2d390712b21ad9a85", aum = -1)

    /* renamed from: on */
    public byte f3000on;

    public C5389aHl() {
    }

    public C5389aHl(C1616Xf xf) {
        super(xf);
    }

    /* renamed from: c */
    public C5663aRz[] mo309c() {
        return ItemGenSet._m_fields;
    }

    /* renamed from: d */
    public void mo310d() {
        super.mo310d();
        this.handle = null;
        this.hWz = null;
        this.f2996bK = null;
        this.f2999ok = 0;
        this.hWA = 0;
        this.f2997oL = 0;
        this.f3000on = 0;
        this.hWB = 0;
        this.f2998oS = 0;
    }
}
