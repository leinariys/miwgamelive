package logic.data.mbean;

import logic.baa.C1616Xf;
import logic.res.code.C5663aRz;
import p001a.C0064Am;

import java.util.UUID;

/* renamed from: a.adW  reason: case insensitive filesystem */
public class C5946adW extends C3805vD {
    @C0064Am(aul = "36953a302e7f814bfe15027b11308db3", aum = -2)

    /* renamed from: Kv */
    public long f4323Kv;
    @C0064Am(aul = "36953a302e7f814bfe15027b11308db3", aum = -1)

    /* renamed from: Kx */
    public byte f4324Kx;
    @C0064Am(aul = "2e1113f7227c13446873bd038f1af238", aum = 2)

    /* renamed from: bK */
    public UUID f4325bK;
    @C0064Am(aul = "36953a302e7f814bfe15027b11308db3", aum = 0)
    public String event;
    @C0064Am(aul = "d006c821580ab6cb6b6abe9c0d88caf7", aum = -2)
    public long fmt;
    @C0064Am(aul = "d006c821580ab6cb6b6abe9c0d88caf7", aum = -1)
    public byte fmu;
    @C0064Am(aul = "2e1113f7227c13446873bd038f1af238", aum = -2)

    /* renamed from: oL */
    public long f4326oL;
    @C0064Am(aul = "2e1113f7227c13446873bd038f1af238", aum = -1)

    /* renamed from: oS */
    public byte f4327oS;
    @C0064Am(aul = "d006c821580ab6cb6b6abe9c0d88caf7", aum = 1)
    public String param;

    public C5946adW() {
    }

    public C5946adW(C1616Xf xf) {
        super(xf);
    }

    /* renamed from: c */
    public C5663aRz[] mo309c() {
        return Rule._m_fields;
    }

    /* renamed from: d */
    public void mo310d() {
        super.mo310d();
        this.event = null;
        this.param = null;
        this.f4325bK = null;
        this.f4323Kv = 0;
        this.fmt = 0;
        this.f4326oL = 0;
        this.f4324Kx = 0;
        this.fmu = 0;
        this.f4327oS = 0;
    }
}
