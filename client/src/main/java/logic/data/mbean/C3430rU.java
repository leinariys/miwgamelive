package logic.data.mbean;

import game.script.Actor;
import game.script.ship.Ship;
import logic.baa.C1616Xf;
import logic.res.code.C5663aRz;
import p001a.C0064Am;
import p001a.C5260aCm;

/* renamed from: a.rU */
public class C3430rU extends C3805vD {
    @C0064Am(aul = "a0daeb3f3a4cda30cfbcbe95ea5a17cb", aum = 0)

    /* renamed from: Lo */
    public Actor f9034Lo;
    @C0064Am(aul = "f75599f30da5d42cef2f9f33fd4f137c", aum = 2)

    /* renamed from: Lq */
    public Ship f9035Lq;
    @C0064Am(aul = "a0daeb3f3a4cda30cfbcbe95ea5a17cb", aum = -2)
    public long bep;
    @C0064Am(aul = "3bf8de174ed58fb9dc58043b52b94a46", aum = -2)
    public long beq;
    @C0064Am(aul = "a0daeb3f3a4cda30cfbcbe95ea5a17cb", aum = -1)
    public byte ber;
    @C0064Am(aul = "3bf8de174ed58fb9dc58043b52b94a46", aum = -1)
    public byte bes;
    @C0064Am(aul = "f75599f30da5d42cef2f9f33fd4f137c", aum = -2)

    /* renamed from: dl */
    public long f9036dl;
    @C0064Am(aul = "f75599f30da5d42cef2f9f33fd4f137c", aum = -1)

    /* renamed from: ds */
    public byte f9037ds;
    @C0064Am(aul = "3bf8de174ed58fb9dc58043b52b94a46", aum = 1)

    /* renamed from: uJ */
    public C5260aCm f9038uJ;

    public C3430rU() {
    }

    public C3430rU(C1616Xf xf) {
        super(xf);
    }

    /* renamed from: c */
    public C5663aRz[] mo309c() {
        return Ship.DeathInformation._m_fields;
    }

    /* renamed from: d */
    public void mo310d() {
        super.mo310d();
        this.f9034Lo = null;
        this.f9038uJ = null;
        this.f9035Lq = null;
        this.bep = 0;
        this.beq = 0;
        this.f9036dl = 0;
        this.ber = 0;
        this.bes = 0;
        this.f9037ds = 0;
    }
}
