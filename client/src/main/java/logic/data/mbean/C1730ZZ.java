package logic.data.mbean;

import game.script.item.buff.amplifier.CruiseSpeedAmplifier;
import logic.baa.C1616Xf;
import logic.res.code.C5663aRz;
import p001a.C0064Am;

/* renamed from: a.ZZ */
public class C1730ZZ extends C3760ug {
    @C0064Am(aul = "4d3d363b83b0afe816a0fa6214f97fa1", aum = 0)
    public CruiseSpeedAmplifier bFw;
    @C0064Am(aul = "4d3d363b83b0afe816a0fa6214f97fa1", aum = -2)

    /* renamed from: dl */
    public long f2230dl;
    @C0064Am(aul = "4d3d363b83b0afe816a0fa6214f97fa1", aum = -1)

    /* renamed from: ds */
    public byte f2231ds;

    public C1730ZZ() {
    }

    public C1730ZZ(C1616Xf xf) {
        super(xf);
    }

    /* renamed from: c */
    public C5663aRz[] mo309c() {
        return CruiseSpeedAmplifier.CruiseAmplifierAdapter._m_fields;
    }

    /* renamed from: d */
    public void mo310d() {
        super.mo310d();
        this.bFw = null;
        this.f2230dl = 0;
        this.f2231ds = 0;
    }
}
