package logic.data.mbean;

import game.geometry.Vec3d;
import game.script.item.Shot;
import logic.baa.C1616Xf;
import logic.res.code.C5663aRz;
import p001a.C0064Am;
import p001a.C5260aCm;

/* renamed from: a.aoS  reason: case insensitive filesystem */
public class C6514aoS extends C2217cs {
    @C0064Am(aul = "eb14e9cc6a6b6ba7f279db046ecb1a1d", aum = -2)
    public long aIR;
    @C0064Am(aul = "eb14e9cc6a6b6ba7f279db046ecb1a1d", aum = -1)
    public byte aJc;
    @C0064Am(aul = "3859a7b5413c7e09bc9267be96bf39cf", aum = -2)
    public long avd;
    @C0064Am(aul = "3859a7b5413c7e09bc9267be96bf39cf", aum = -1)
    public byte avi;
    @C0064Am(aul = "89775bbf1e7d192b90974eed9ca7a830", aum = -2)
    public long bIJ;
    @C0064Am(aul = "89775bbf1e7d192b90974eed9ca7a830", aum = -1)
    public byte bIV;
    @C0064Am(aul = "6b33bbd61e79e4456cce240ad4a65239", aum = -2)
    public long beq;
    @C0064Am(aul = "6b33bbd61e79e4456cce240ad4a65239", aum = -1)
    public byte bes;
    @C0064Am(aul = "ce7883cdc659628b08cfba96dda1fbbc", aum = -2)
    public long gjA;
    @C0064Am(aul = "56867addc136999c5073792a53bc2f78", aum = -2)
    public long gjB;
    @C0064Am(aul = "9156aef2a5471a2d4c9b5ad976f676aa", aum = -1)
    public byte gjC;
    @C0064Am(aul = "ce7883cdc659628b08cfba96dda1fbbc", aum = -1)
    public byte gjD;
    @C0064Am(aul = "56867addc136999c5073792a53bc2f78", aum = -1)
    public byte gjE;
    @C0064Am(aul = "9156aef2a5471a2d4c9b5ad976f676aa", aum = -2)
    public long gjz;
    @C0064Am(aul = "89775bbf1e7d192b90974eed9ca7a830", aum = 2)
    public float range;
    @C0064Am(aul = "eb14e9cc6a6b6ba7f279db046ecb1a1d", aum = 5)
    public float speed;
    @C0064Am(aul = "6b33bbd61e79e4456cce240ad4a65239", aum = 0)

    /* renamed from: uJ */
    public C5260aCm f5002uJ;
    @C0064Am(aul = "9156aef2a5471a2d4c9b5ad976f676aa", aum = 1)

    /* renamed from: uL */
    public Vec3d f5003uL;
    @C0064Am(aul = "ce7883cdc659628b08cfba96dda1fbbc", aum = 3)

    /* renamed from: uO */
    public String f5004uO;
    @C0064Am(aul = "56867addc136999c5073792a53bc2f78", aum = 4)

    /* renamed from: uQ */
    public String f5005uQ;
    @C0064Am(aul = "3859a7b5413c7e09bc9267be96bf39cf", aum = 6)

    /* renamed from: uT */
    public String f5006uT;

    public C6514aoS() {
    }

    public C6514aoS(C1616Xf xf) {
        super(xf);
    }

    /* renamed from: c */
    public C5663aRz[] mo309c() {
        return Shot._m_fields;
    }

    /* renamed from: d */
    public void mo310d() {
        super.mo310d();
        this.f5002uJ = null;
        this.f5003uL = null;
        this.range = 0.0f;
        this.f5004uO = null;
        this.f5005uQ = null;
        this.speed = 0.0f;
        this.f5006uT = null;
        this.beq = 0;
        this.gjz = 0;
        this.bIJ = 0;
        this.gjA = 0;
        this.gjB = 0;
        this.aIR = 0;
        this.avd = 0;
        this.bes = 0;
        this.gjC = 0;
        this.bIV = 0;
        this.gjD = 0;
        this.gjE = 0;
        this.aJc = 0;
        this.avi = 0;
    }
}
