package logic.data.mbean;

import game.network.message.serializable.C3438ra;
import game.script.item.AmplifierType;
import game.script.progression.ProgressionAbility;
import logic.baa.C1616Xf;
import logic.res.code.C5663aRz;
import p001a.C0064Am;
import taikodom.infra.script.I18NString;

import java.util.UUID;

/* renamed from: a.Rs */
public class C1210Rs extends C3805vD {
    @C0064Am(aul = "bd2dda7a3d35eeae40efebdf6e32d560", aum = 5)

    /* renamed from: bK */
    public UUID f1506bK;
    @C0064Am(aul = "18bb11ec538148b37ac7366418eeb50c", aum = -2)
    public long dZR;
    @C0064Am(aul = "6958eeba71dd47c40c0a12c7bbbdf152", aum = -2)
    public long dZS;
    @C0064Am(aul = "18bb11ec538148b37ac7366418eeb50c", aum = -1)
    public byte dZT;
    @C0064Am(aul = "6958eeba71dd47c40c0a12c7bbbdf152", aum = -1)
    public byte dZU;
    @C0064Am(aul = "18bb11ec538148b37ac7366418eeb50c", aum = 3)
    public C3438ra<AmplifierType> dxq;
    @C0064Am(aul = "6958eeba71dd47c40c0a12c7bbbdf152", aum = 4)
    public I18NString dxs;
    @C0064Am(aul = "c0e2c08df5ea8d2fdc36db5623d93f64", aum = 0)
    public String handle;
    @C0064Am(aul = "2bdfd12a74a57d0502b1b267790107b2", aum = 2)

    /* renamed from: nh */
    public I18NString f1507nh;
    @C0064Am(aul = "dd9798136dd22d597e5ec103a589c7f1", aum = 1)

    /* renamed from: ni */
    public I18NString f1508ni;
    @C0064Am(aul = "2bdfd12a74a57d0502b1b267790107b2", aum = -2)

    /* renamed from: nk */
    public long f1509nk;
    @C0064Am(aul = "dd9798136dd22d597e5ec103a589c7f1", aum = -2)

    /* renamed from: nl */
    public long f1510nl;
    @C0064Am(aul = "2bdfd12a74a57d0502b1b267790107b2", aum = -1)

    /* renamed from: nn */
    public byte f1511nn;
    @C0064Am(aul = "dd9798136dd22d597e5ec103a589c7f1", aum = -1)

    /* renamed from: no */
    public byte f1512no;
    @C0064Am(aul = "bd2dda7a3d35eeae40efebdf6e32d560", aum = -2)

    /* renamed from: oL */
    public long f1513oL;
    @C0064Am(aul = "bd2dda7a3d35eeae40efebdf6e32d560", aum = -1)

    /* renamed from: oS */
    public byte f1514oS;
    @C0064Am(aul = "c0e2c08df5ea8d2fdc36db5623d93f64", aum = -2)

    /* renamed from: ok */
    public long f1515ok;
    @C0064Am(aul = "c0e2c08df5ea8d2fdc36db5623d93f64", aum = -1)

    /* renamed from: on */
    public byte f1516on;

    public C1210Rs() {
    }

    public C1210Rs(C1616Xf xf) {
        super(xf);
    }

    /* renamed from: c */
    public C5663aRz[] mo309c() {
        return ProgressionAbility._m_fields;
    }

    /* renamed from: d */
    public void mo310d() {
        super.mo310d();
        this.handle = null;
        this.f1508ni = null;
        this.f1507nh = null;
        this.dxq = null;
        this.dxs = null;
        this.f1506bK = null;
        this.f1515ok = 0;
        this.f1510nl = 0;
        this.f1509nk = 0;
        this.dZR = 0;
        this.dZS = 0;
        this.f1513oL = 0;
        this.f1516on = 0;
        this.f1512no = 0;
        this.f1511nn = 0;
        this.dZT = 0;
        this.dZU = 0;
        this.f1514oS = 0;
    }
}
