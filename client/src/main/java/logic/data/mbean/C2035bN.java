package logic.data.mbean;

import game.script.contract.types.ContractBaseInfo;
import logic.baa.C1616Xf;
import logic.res.code.C5663aRz;
import p001a.C0064Am;
import taikodom.infra.script.I18NString;

import java.util.UUID;

/* renamed from: a.bN */
public class C2035bN extends C3805vD {
    @C0064Am(aul = "c112259417f715acd277ec1562419c9c", aum = 4)

    /* renamed from: bK */
    public UUID f5762bK;
    @C0064Am(aul = "2bdaa38ec32c32c3f23add4bf45e95f2", aum = 0)
    public String handle;
    @C0064Am(aul = "935faa1f3eeac8b25f86198cbbc1ed0b", aum = 2)

    /* renamed from: nh */
    public I18NString f5763nh;
    @C0064Am(aul = "106cc3eb6d91518302404372239bd752", aum = 1)

    /* renamed from: ni */
    public I18NString f5764ni;
    @C0064Am(aul = "935faa1f3eeac8b25f86198cbbc1ed0b", aum = -2)

    /* renamed from: nk */
    public long f5765nk;
    @C0064Am(aul = "106cc3eb6d91518302404372239bd752", aum = -2)

    /* renamed from: nl */
    public long f5766nl;
    @C0064Am(aul = "935faa1f3eeac8b25f86198cbbc1ed0b", aum = -1)

    /* renamed from: nn */
    public byte f5767nn;
    @C0064Am(aul = "106cc3eb6d91518302404372239bd752", aum = -1)

    /* renamed from: no */
    public byte f5768no;
    @C0064Am(aul = "c112259417f715acd277ec1562419c9c", aum = -2)

    /* renamed from: oL */
    public long f5769oL;
    @C0064Am(aul = "c112259417f715acd277ec1562419c9c", aum = -1)

    /* renamed from: oS */
    public byte f5770oS;
    @C0064Am(aul = "2bdaa38ec32c32c3f23add4bf45e95f2", aum = -2)

    /* renamed from: ok */
    public long f5771ok;
    @C0064Am(aul = "2bdaa38ec32c32c3f23add4bf45e95f2", aum = -1)

    /* renamed from: on */
    public byte f5772on;
    @C0064Am(aul = "0a277957c0f7dae11bc560e66ad94ba1", aum = 3)

    /* renamed from: pi */
    public long f5773pi;
    @C0064Am(aul = "0a277957c0f7dae11bc560e66ad94ba1", aum = -2)

    /* renamed from: pj */
    public long f5774pj;
    @C0064Am(aul = "0a277957c0f7dae11bc560e66ad94ba1", aum = -1)

    /* renamed from: pk */
    public byte f5775pk;

    public C2035bN() {
    }

    public C2035bN(C1616Xf xf) {
        super(xf);
    }

    /* renamed from: c */
    public C5663aRz[] mo309c() {
        return ContractBaseInfo._m_fields;
    }

    /* renamed from: d */
    public void mo310d() {
        super.mo310d();
        this.handle = null;
        this.f5764ni = null;
        this.f5763nh = null;
        this.f5773pi = 0;
        this.f5762bK = null;
        this.f5771ok = 0;
        this.f5766nl = 0;
        this.f5765nk = 0;
        this.f5774pj = 0;
        this.f5769oL = 0;
        this.f5772on = 0;
        this.f5768no = 0;
        this.f5767nn = 0;
        this.f5775pk = 0;
        this.f5770oS = 0;
    }
}
