package logic.data.mbean;

import game.script.progression.ProgressionSuitability;
import logic.baa.C1616Xf;
import logic.res.code.C5663aRz;
import p001a.C0064Am;
import taikodom.infra.script.I18NString;

import java.util.UUID;

/* renamed from: a.amU  reason: case insensitive filesystem */
public class C6412amU extends C3805vD {
    @C0064Am(aul = "f9b792add03fdca5552692572887a6d1", aum = 4)

    /* renamed from: bK */
    public UUID f4907bK;
    @C0064Am(aul = "4e280ab58e21f613d31eff03ad981810", aum = 2)
    public boolean gbX;
    @C0064Am(aul = "4e280ab58e21f613d31eff03ad981810", aum = -2)
    public long gbY;
    @C0064Am(aul = "4e280ab58e21f613d31eff03ad981810", aum = -1)
    public byte gbZ;
    @C0064Am(aul = "7752fb51382e64353d3f2f7aaedbb3cf", aum = 3)
    public String handle;
    @C0064Am(aul = "3cc8684b917dc785946165761022a42d", aum = 1)

    /* renamed from: nh */
    public I18NString f4908nh;
    @C0064Am(aul = "93f752970c2bf994518e5e9208372b7a", aum = 0)

    /* renamed from: ni */
    public I18NString f4909ni;
    @C0064Am(aul = "3cc8684b917dc785946165761022a42d", aum = -2)

    /* renamed from: nk */
    public long f4910nk;
    @C0064Am(aul = "93f752970c2bf994518e5e9208372b7a", aum = -2)

    /* renamed from: nl */
    public long f4911nl;
    @C0064Am(aul = "3cc8684b917dc785946165761022a42d", aum = -1)

    /* renamed from: nn */
    public byte f4912nn;
    @C0064Am(aul = "93f752970c2bf994518e5e9208372b7a", aum = -1)

    /* renamed from: no */
    public byte f4913no;
    @C0064Am(aul = "f9b792add03fdca5552692572887a6d1", aum = -2)

    /* renamed from: oL */
    public long f4914oL;
    @C0064Am(aul = "f9b792add03fdca5552692572887a6d1", aum = -1)

    /* renamed from: oS */
    public byte f4915oS;
    @C0064Am(aul = "7752fb51382e64353d3f2f7aaedbb3cf", aum = -2)

    /* renamed from: ok */
    public long f4916ok;
    @C0064Am(aul = "7752fb51382e64353d3f2f7aaedbb3cf", aum = -1)

    /* renamed from: on */
    public byte f4917on;

    public C6412amU() {
    }

    public C6412amU(C1616Xf xf) {
        super(xf);
    }

    /* renamed from: c */
    public C5663aRz[] mo309c() {
        return ProgressionSuitability._m_fields;
    }

    /* renamed from: d */
    public void mo310d() {
        super.mo310d();
        this.f4909ni = null;
        this.f4908nh = null;
        this.gbX = false;
        this.handle = null;
        this.f4907bK = null;
        this.f4911nl = 0;
        this.f4910nk = 0;
        this.gbY = 0;
        this.f4916ok = 0;
        this.f4914oL = 0;
        this.f4913no = 0;
        this.f4912nn = 0;
        this.gbZ = 0;
        this.f4917on = 0;
        this.f4915oS = 0;
    }
}
