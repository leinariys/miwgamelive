package logic.data.mbean;

import game.network.message.serializable.C1556Wo;
import game.network.message.serializable.C2686iZ;
import game.script.damage.DamageType;
import game.script.ship.HullType;
import logic.baa.C1616Xf;
import logic.res.code.C5663aRz;
import p001a.C0064Am;
import p001a.C1649YI;

/* renamed from: a.aOl  reason: case insensitive filesystem */
public class C5571aOl extends C6723asT {
    @C0064Am(aul = "5d23cfabe7bf1b78bd152f33901c76e9", aum = 2)
    public float aOo;
    @C0064Am(aul = "a4cd3543c0569fdfd9d3ec0a32dc4d95", aum = 0)
    public C1556Wo<DamageType, C1649YI> aOu;
    @C0064Am(aul = "61c35eef423ab410eb01524d1114c103", aum = 1)
    public C2686iZ<DamageType> bEB;
    @C0064Am(aul = "a4cd3543c0569fdfd9d3ec0a32dc4d95", aum = -2)
    public long eVK;
    @C0064Am(aul = "61c35eef423ab410eb01524d1114c103", aum = -2)
    public long eVL;
    @C0064Am(aul = "5d23cfabe7bf1b78bd152f33901c76e9", aum = -2)
    public long eVM;
    @C0064Am(aul = "a4cd3543c0569fdfd9d3ec0a32dc4d95", aum = -1)
    public byte eVP;
    @C0064Am(aul = "61c35eef423ab410eb01524d1114c103", aum = -1)
    public byte eVQ;
    @C0064Am(aul = "5d23cfabe7bf1b78bd152f33901c76e9", aum = -1)
    public byte eVR;

    public C5571aOl() {
    }

    public C5571aOl(C1616Xf xf) {
        super(xf);
    }

    /* renamed from: c */
    public C5663aRz[] mo309c() {
        return HullType._m_fields;
    }

    /* renamed from: d */
    public void mo310d() {
        super.mo310d();
        this.aOu = null;
        this.bEB = null;
        this.aOo = 0.0f;
        this.eVK = 0;
        this.eVL = 0;
        this.eVM = 0;
        this.eVP = 0;
        this.eVQ = 0;
        this.eVR = 0;
    }
}
