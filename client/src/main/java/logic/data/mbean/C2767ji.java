package logic.data.mbean;

import game.network.message.serializable.C2686iZ;
import game.script.item.ItemType;
import game.script.newmarket.CommercialOrder;
import game.script.newmarket.CommercialOrderInfo;
import game.script.ship.Station;
import logic.baa.C1616Xf;
import logic.res.code.C5663aRz;
import p001a.C0064Am;

/* renamed from: a.ji */
public class C2767ji extends C3805vD {
    @C0064Am(aul = "c73e12770407d7f24f341f03c9460186", aum = 7)

    /* renamed from: Q */
    public long f8315Q;
    @C0064Am(aul = "c73e12770407d7f24f341f03c9460186", aum = -2)

    /* renamed from: ah */
    public long f8316ah;
    @C0064Am(aul = "dd8d1f49ac815c817f41c0b893765fa1", aum = -2)
    public long aih;
    @C0064Am(aul = "c29a034e3d5a425bc0cb92bca6d6f8db", aum = -2)
    public long aii;
    @C0064Am(aul = "1c6cb85faf5226eb295d6457804bb0af", aum = -2)
    public long aij;
    @C0064Am(aul = "75ab7dce6ebda52194babae7df7b9c31", aum = -2)
    public long aik;
    @C0064Am(aul = "24d2a1c08a7c00e5733ffd2a88b416d6", aum = -2)
    public long ail;
    @C0064Am(aul = "2c2124d2e4622d17ab06b75c34e0d0a5", aum = -2)
    public long ain;
    @C0064Am(aul = "f91b8617656aaa9f7294b5aaef90fece", aum = -2)
    public long aio;
    @C0064Am(aul = "a076b8316350fcce54c06cfeaa9fa39f", aum = -2)
    public long aip;
    @C0064Am(aul = "0dd1c43162958a07f8579687c7f7f016", aum = -2)
    public long aiq;
    @C0064Am(aul = "dd8d1f49ac815c817f41c0b893765fa1", aum = -1)
    public byte air;
    @C0064Am(aul = "c29a034e3d5a425bc0cb92bca6d6f8db", aum = -1)
    public byte ais;
    @C0064Am(aul = "1c6cb85faf5226eb295d6457804bb0af", aum = -1)
    public byte ait;
    @C0064Am(aul = "75ab7dce6ebda52194babae7df7b9c31", aum = -1)
    public byte aiu;
    @C0064Am(aul = "24d2a1c08a7c00e5733ffd2a88b416d6", aum = -1)
    public byte aiv;
    @C0064Am(aul = "2c2124d2e4622d17ab06b75c34e0d0a5", aum = -1)
    public byte aiw;
    @C0064Am(aul = "f91b8617656aaa9f7294b5aaef90fece", aum = -1)
    public byte aix;
    @C0064Am(aul = "a076b8316350fcce54c06cfeaa9fa39f", aum = -1)
    public byte aiy;
    @C0064Am(aul = "0dd1c43162958a07f8579687c7f7f016", aum = -1)
    public byte aiz;
    @C0064Am(aul = "c73e12770407d7f24f341f03c9460186", aum = -1)

    /* renamed from: aw */
    public byte f8317aw;
    @C0064Am(aul = "f98d8224f7fe0be1289d96bf35b5df7f", aum = 0)

    /* renamed from: cY */
    public ItemType f8318cY;
    @C0064Am(aul = "f98d8224f7fe0be1289d96bf35b5df7f", aum = -2)

    /* renamed from: dg */
    public long f8319dg;
    @C0064Am(aul = "2c2124d2e4622d17ab06b75c34e0d0a5", aum = 8)
    public boolean dirty;
    @C0064Am(aul = "f98d8224f7fe0be1289d96bf35b5df7f", aum = -1)

    /* renamed from: dn */
    public byte f8320dn;
    @C0064Am(aul = "d2075e797e32a0cc431168f4f99fe177", aum = 5)

    /* renamed from: iH */
    public Station f8321iH;
    @C0064Am(aul = "d2075e797e32a0cc431168f4f99fe177", aum = -2)

    /* renamed from: iK */
    public long f8322iK;
    @C0064Am(aul = "d2075e797e32a0cc431168f4f99fe177", aum = -1)

    /* renamed from: iN */
    public byte f8323iN;
    @C0064Am(aul = "dd8d1f49ac815c817f41c0b893765fa1", aum = 1)

    /* renamed from: kP */
    public int f8324kP;
    @C0064Am(aul = "c29a034e3d5a425bc0cb92bca6d6f8db", aum = 2)

    /* renamed from: kR */
    public int f8325kR;
    @C0064Am(aul = "1c6cb85faf5226eb295d6457804bb0af", aum = 3)

    /* renamed from: kT */
    public int f8326kT;
    @C0064Am(aul = "75ab7dce6ebda52194babae7df7b9c31", aum = 4)

    /* renamed from: kV */
    public long f8327kV;
    @C0064Am(aul = "24d2a1c08a7c00e5733ffd2a88b416d6", aum = 6)

    /* renamed from: kY */
    public long f8328kY;
    @C0064Am(aul = "f91b8617656aaa9f7294b5aaef90fece", aum = 9)

    /* renamed from: lc */
    public CommercialOrderInfo f8329lc;
    @C0064Am(aul = "a076b8316350fcce54c06cfeaa9fa39f", aum = 10)

    /* renamed from: le */
    public C2686iZ<CommercialOrderInfo> f8330le;
    @C0064Am(aul = "0dd1c43162958a07f8579687c7f7f016", aum = 11)

    /* renamed from: lg */
    public CommercialOrder.CommercialOrderTicker f8331lg;

    public C2767ji() {
    }

    public C2767ji(C1616Xf xf) {
        super(xf);
    }

    /* renamed from: c */
    public C5663aRz[] mo309c() {
        return CommercialOrder._m_fields;
    }

    /* renamed from: d */
    public void mo310d() {
        super.mo310d();
        this.f8318cY = null;
        this.f8324kP = 0;
        this.f8325kR = 0;
        this.f8326kT = 0;
        this.f8327kV = 0;
        this.f8321iH = null;
        this.f8328kY = 0;
        this.f8315Q = 0;
        this.dirty = false;
        this.f8329lc = null;
        this.f8330le = null;
        this.f8331lg = null;
        this.f8319dg = 0;
        this.aih = 0;
        this.aii = 0;
        this.aij = 0;
        this.aik = 0;
        this.f8322iK = 0;
        this.ail = 0;
        this.f8316ah = 0;
        this.ain = 0;
        this.aio = 0;
        this.aip = 0;
        this.aiq = 0;
        this.f8320dn = 0;
        this.air = 0;
        this.ais = 0;
        this.ait = 0;
        this.aiu = 0;
        this.f8323iN = 0;
        this.aiv = 0;
        this.f8317aw = 0;
        this.aiw = 0;
        this.aix = 0;
        this.aiy = 0;
        this.aiz = 0;
    }
}
