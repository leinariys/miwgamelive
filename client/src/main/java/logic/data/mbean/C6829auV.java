package logic.data.mbean;

import game.script.item.ItemType;
import game.script.ship.OutpostLevelInfo;
import logic.baa.C1616Xf;
import logic.res.code.C5663aRz;
import p001a.C0064Am;

import java.util.UUID;

/* renamed from: a.auV  reason: case insensitive filesystem */
public class C6829auV extends C5292aDs {
    @C0064Am(aul = "fe379ae0e7c9acb0db3d951ffca91aba", aum = -2)
    public long aLG;
    @C0064Am(aul = "fe379ae0e7c9acb0db3d951ffca91aba", aum = -1)
    public byte aLL;
    @C0064Am(aul = "ad0e7b7673db304bf538c77e0df2fdd6", aum = 2)

    /* renamed from: bK */
    public UUID f5386bK;
    @C0064Am(aul = "62ad9c39cb3d49b814b6bfeb3a409e13", aum = 0)

    /* renamed from: cY */
    public ItemType f5387cY;
    @C0064Am(aul = "aaf3584f1aee02fc8a944e022f7da1c7", aum = 4)
    public OutpostLevelInfo cfq;
    @C0064Am(aul = "fe379ae0e7c9acb0db3d951ffca91aba", aum = 1)

    /* renamed from: cy */
    public int f5388cy;
    @C0064Am(aul = "62ad9c39cb3d49b814b6bfeb3a409e13", aum = -2)

    /* renamed from: dg */
    public long f5389dg;
    @C0064Am(aul = "aaf3584f1aee02fc8a944e022f7da1c7", aum = -2)

    /* renamed from: dl */
    public long f5390dl;
    @C0064Am(aul = "62ad9c39cb3d49b814b6bfeb3a409e13", aum = -1)

    /* renamed from: dn */
    public byte f5391dn;
    @C0064Am(aul = "aaf3584f1aee02fc8a944e022f7da1c7", aum = -1)

    /* renamed from: ds */
    public byte f5392ds;
    @C0064Am(aul = "14175d842ec5aadf1962158ca52b022d", aum = 3)
    public String handle;
    @C0064Am(aul = "ad0e7b7673db304bf538c77e0df2fdd6", aum = -2)

    /* renamed from: oL */
    public long f5393oL;
    @C0064Am(aul = "ad0e7b7673db304bf538c77e0df2fdd6", aum = -1)

    /* renamed from: oS */
    public byte f5394oS;
    @C0064Am(aul = "14175d842ec5aadf1962158ca52b022d", aum = -2)

    /* renamed from: ok */
    public long f5395ok;
    @C0064Am(aul = "14175d842ec5aadf1962158ca52b022d", aum = -1)

    /* renamed from: on */
    public byte f5396on;

    public C6829auV() {
    }

    public C6829auV(C1616Xf xf) {
        super(xf);
    }

    /* renamed from: c */
    public C5663aRz[] mo309c() {
        return OutpostLevelInfo.OutpostUpkeepItem._m_fields;
    }

    /* renamed from: d */
    public void mo310d() {
        super.mo310d();
        this.f5387cY = null;
        this.f5388cy = 0;
        this.f5386bK = null;
        this.handle = null;
        this.cfq = null;
        this.f5389dg = 0;
        this.aLG = 0;
        this.f5393oL = 0;
        this.f5395ok = 0;
        this.f5390dl = 0;
        this.f5391dn = 0;
        this.aLL = 0;
        this.f5394oS = 0;
        this.f5396on = 0;
        this.f5392ds = 0;
    }
}
