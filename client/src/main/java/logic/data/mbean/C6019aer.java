package logic.data.mbean;

import game.script.spacezone.ScriptableZone;
import logic.baa.C1616Xf;
import logic.res.code.C5663aRz;
import p001a.C0064Am;

/* renamed from: a.aer  reason: case insensitive filesystem */
public class C6019aer extends C6757atB {
    @C0064Am(aul = "a27312afdda86c8167c7522231b3de03", aum = -2)

    /* renamed from: dl */
    public long f4428dl;
    @C0064Am(aul = "a27312afdda86c8167c7522231b3de03", aum = -1)

    /* renamed from: ds */
    public byte f4429ds;
    @C0064Am(aul = "a27312afdda86c8167c7522231b3de03", aum = 0)
    public ScriptableZone fnC;

    public C6019aer() {
    }

    public C6019aer(C1616Xf xf) {
        super(xf);
    }

    /* renamed from: c */
    public C5663aRz[] mo309c() {
        return ScriptableZone.Heartbeat._m_fields;
    }

    /* renamed from: d */
    public void mo310d() {
        super.mo310d();
        this.fnC = null;
        this.f4428dl = 0;
        this.f4429ds = 0;
    }
}
