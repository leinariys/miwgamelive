package logic.data.mbean;

import game.network.message.serializable.C3438ra;
import game.script.item.ItemTypeCategory;
import game.script.pda.DatabaseCategory;
import game.script.pda.TaikopediaEntry;
import game.script.progression.ProgressionAbility;
import game.script.resource.Asset;
import logic.baa.C1616Xf;
import logic.res.code.C5663aRz;
import p001a.C0064Am;
import taikodom.infra.script.I18NString;

import java.util.UUID;

/* renamed from: a.Yj */
public class C1679Yj extends C5292aDs {
    @C0064Am(aul = "6267ac88fde876f5d33962ace7483f87", aum = -2)

    /* renamed from: Nf */
    public long f2189Nf;
    @C0064Am(aul = "6267ac88fde876f5d33962ace7483f87", aum = -1)

    /* renamed from: Nh */
    public byte f2190Nh;
    @C0064Am(aul = "39da1f3858f20d0b3e029908e3e4d9d2", aum = 9)
    public ProgressionAbility ana;
    @C0064Am(aul = "b0553ac47dde3fbb23f4687696359b5d", aum = 12)

    /* renamed from: bK */
    public UUID f2191bK;
    @C0064Am(aul = "39da1f3858f20d0b3e029908e3e4d9d2", aum = -2)
    public long dAg;
    @C0064Am(aul = "39da1f3858f20d0b3e029908e3e4d9d2", aum = -1)
    public byte dAw;
    @C0064Am(aul = "7dba25eeecc7e2d4627ab8621357732b", aum = 10)
    public boolean dyX;
    @C0064Am(aul = "f452849beae42130023013af95b97315", aum = 2)
    public C3438ra<ItemTypeCategory> eLj;
    @C0064Am(aul = "619e803eb8933cd0ca8228c052f903c0", aum = 3)
    public ItemTypeCategory eLk;
    @C0064Am(aul = "619e803eb8933cd0ca8228c052f903c0", aum = -2)
    public long eLl;
    @C0064Am(aul = "7dba25eeecc7e2d4627ab8621357732b", aum = -2)
    public long eLm;
    @C0064Am(aul = "619e803eb8933cd0ca8228c052f903c0", aum = -1)
    public byte eLn;
    @C0064Am(aul = "7dba25eeecc7e2d4627ab8621357732b", aum = -1)
    public byte eLo;
    @C0064Am(aul = "5e96d760c331c65c9660cb725288e6a7", aum = 1)
    public String handle;
    @C0064Am(aul = "3f25dc4f1a3255ec45a70510f1896c16", aum = -1)

    /* renamed from: jK */
    public byte f2192jK;
    @C0064Am(aul = "8b9ba86467cda49bb9f43e572e5ba075", aum = 6)

    /* renamed from: jS */
    public DatabaseCategory f2193jS;
    @C0064Am(aul = "127c687bda709d7fcc067219e276c412", aum = 8)

    /* renamed from: jT */
    public C3438ra<TaikopediaEntry> f2194jT;
    @C0064Am(aul = "639c65de6fe8bdebebf32beb363180ff", aum = 5)

    /* renamed from: jU */
    public Asset f2195jU;
    @C0064Am(aul = "0c2d8f26200f21dd6de6716a656487f3", aum = 11)

    /* renamed from: jV */
    public float f2196jV;
    @C0064Am(aul = "8b9ba86467cda49bb9f43e572e5ba075", aum = -2)

    /* renamed from: jX */
    public long f2197jX;
    @C0064Am(aul = "127c687bda709d7fcc067219e276c412", aum = -2)

    /* renamed from: jY */
    public long f2198jY;
    @C0064Am(aul = "639c65de6fe8bdebebf32beb363180ff", aum = -2)

    /* renamed from: jZ */
    public long f2199jZ;
    @C0064Am(aul = "3f25dc4f1a3255ec45a70510f1896c16", aum = 4)

    /* renamed from: jn */
    public Asset f2200jn;
    @C0064Am(aul = "3f25dc4f1a3255ec45a70510f1896c16", aum = -2)

    /* renamed from: jy */
    public long f2201jy;
    @C0064Am(aul = "0c2d8f26200f21dd6de6716a656487f3", aum = -2)

    /* renamed from: ka */
    public long f2202ka;
    @C0064Am(aul = "8b9ba86467cda49bb9f43e572e5ba075", aum = -1)

    /* renamed from: kc */
    public byte f2203kc;
    @C0064Am(aul = "127c687bda709d7fcc067219e276c412", aum = -1)

    /* renamed from: kd */
    public byte f2204kd;
    @C0064Am(aul = "639c65de6fe8bdebebf32beb363180ff", aum = -1)

    /* renamed from: ke */
    public byte f2205ke;
    @C0064Am(aul = "0c2d8f26200f21dd6de6716a656487f3", aum = -1)

    /* renamed from: kf */
    public byte f2206kf;
    @C0064Am(aul = "5cb6234f0923c66c217789d000b397d2", aum = 7)

    /* renamed from: nh */
    public I18NString f2207nh;
    @C0064Am(aul = "5cb6234f0923c66c217789d000b397d2", aum = -2)

    /* renamed from: nk */
    public long f2208nk;
    @C0064Am(aul = "5cb6234f0923c66c217789d000b397d2", aum = -1)

    /* renamed from: nn */
    public byte f2209nn;
    @C0064Am(aul = "b0553ac47dde3fbb23f4687696359b5d", aum = -2)

    /* renamed from: oL */
    public long f2210oL;
    @C0064Am(aul = "b0553ac47dde3fbb23f4687696359b5d", aum = -1)

    /* renamed from: oS */
    public byte f2211oS;
    @C0064Am(aul = "5e96d760c331c65c9660cb725288e6a7", aum = -2)

    /* renamed from: ok */
    public long f2212ok;
    @C0064Am(aul = "5e96d760c331c65c9660cb725288e6a7", aum = -1)

    /* renamed from: on */
    public byte f2213on;
    @C0064Am(aul = "f452849beae42130023013af95b97315", aum = -2)

    /* renamed from: rO */
    public long f2214rO;
    @C0064Am(aul = "f452849beae42130023013af95b97315", aum = -1)

    /* renamed from: se */
    public byte f2215se;
    @C0064Am(aul = "6267ac88fde876f5d33962ace7483f87", aum = 0)

    /* renamed from: zP */
    public I18NString f2216zP;

    public C1679Yj() {
    }

    public C1679Yj(C1616Xf xf) {
        super(xf);
    }

    /* renamed from: c */
    public C5663aRz[] mo309c() {
        return ItemTypeCategory._m_fields;
    }

    /* renamed from: d */
    public void mo310d() {
        super.mo310d();
        this.f2216zP = null;
        this.handle = null;
        this.eLj = null;
        this.eLk = null;
        this.f2200jn = null;
        this.f2195jU = null;
        this.f2193jS = null;
        this.f2207nh = null;
        this.f2194jT = null;
        this.ana = null;
        this.dyX = false;
        this.f2196jV = 0.0f;
        this.f2191bK = null;
        this.f2189Nf = 0;
        this.f2212ok = 0;
        this.f2214rO = 0;
        this.eLl = 0;
        this.f2201jy = 0;
        this.f2199jZ = 0;
        this.f2197jX = 0;
        this.f2208nk = 0;
        this.f2198jY = 0;
        this.dAg = 0;
        this.eLm = 0;
        this.f2202ka = 0;
        this.f2210oL = 0;
        this.f2190Nh = 0;
        this.f2213on = 0;
        this.f2215se = 0;
        this.eLn = 0;
        this.f2192jK = 0;
        this.f2205ke = 0;
        this.f2203kc = 0;
        this.f2209nn = 0;
        this.f2204kd = 0;
        this.dAw = 0;
        this.eLo = 0;
        this.f2206kf = 0;
        this.f2211oS = 0;
    }
}
