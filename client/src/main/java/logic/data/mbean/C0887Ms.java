package logic.data.mbean;

import game.script.item.Item;
import game.script.item.ItemLocation;
import game.script.item.ItemTypeCategory;
import game.script.item.durability.ItemDurability;
import game.script.progression.ProgressionAbility;
import game.script.resource.Asset;
import logic.baa.C1616Xf;
import logic.res.code.C5663aRz;
import p001a.C0064Am;
import taikodom.infra.script.I18NString;

/* renamed from: a.Ms */
public class C0887Ms extends C3805vD {
    @C0064Am(aul = "65cc2adcbcbe32ef26363345f0f65861", aum = -2)

    /* renamed from: Nf */
    public long f1153Nf;
    @C0064Am(aul = "65cc2adcbcbe32ef26363345f0f65861", aum = -1)

    /* renamed from: Nh */
    public byte f1154Nh;
    @C0064Am(aul = "c7452a0dc441c90d3a53aabc27e4c1fc", aum = 2)
    public long amU;
    @C0064Am(aul = "9c5539b1279d8bd9fe780e45622ab34b", aum = 3)
    public float amW;
    @C0064Am(aul = "af7b3499839a154ef6bc93e5dea76412", aum = 4)
    public ItemTypeCategory amY;
    @C0064Am(aul = "b4343d42bc813f96de1273b1fa544374", aum = 6)
    public ProgressionAbility ana;
    @C0064Am(aul = "1c6ea46d83971e4c1287e307e65f3861", aum = 7)
    public long anc;
    @C0064Am(aul = "725d666e1d4cbcba5e14994444fb6a3f", aum = 8)
    public long ane;
    @C0064Am(aul = "6132cf056f1e8309fbe04942b0dcfdd5", aum = 9)
    public long ang;
    @C0064Am(aul = "40be3b8685328860ee01609d3beb5b4e", aum = 10)
    public int ani;
    @C0064Am(aul = "c1f45ed45e7875b30881ee70a4c39c8e", aum = 11)
    public int ank;
    @C0064Am(aul = "13fd94945c288e737e625bbcf075462d", aum = 12)
    public int anm;
    @C0064Am(aul = "86678bf4ad3b6eb5f931fd0e118cb07d", aum = 13)
    public int ano;
    @C0064Am(aul = "53cb5dbadd7e2f7436dcb8a50c9f2a42", aum = 14)
    public int anq;
    @C0064Am(aul = "69304c502c1a41b528915e749a9144cd", aum = 19)
    public ItemLocation apb;
    @C0064Am(aul = "69304c502c1a41b528915e749a9144cd", aum = -2)
    public long cCP;
    @C0064Am(aul = "69304c502c1a41b528915e749a9144cd", aum = -1)
    public byte cCW;
    @C0064Am(aul = "40be3b8685328860ee01609d3beb5b4e", aum = -1)
    public byte dAA;
    @C0064Am(aul = "c1f45ed45e7875b30881ee70a4c39c8e", aum = -1)
    public byte dAB;
    @C0064Am(aul = "13fd94945c288e737e625bbcf075462d", aum = -1)
    public byte dAC;
    @C0064Am(aul = "86678bf4ad3b6eb5f931fd0e118cb07d", aum = -1)
    public byte dAD;
    @C0064Am(aul = "53cb5dbadd7e2f7436dcb8a50c9f2a42", aum = -1)
    public byte dAE;
    @C0064Am(aul = "459baf016d809a4bb4da06ac821b3fb6", aum = -1)
    public byte dAF;
    @C0064Am(aul = "219a042b26267a43f7e1b801a1332d30", aum = -1)
    public byte dAG;
    @C0064Am(aul = "6a4c2a2c913fb14c91a6e1c42296c372", aum = -1)
    public byte dAH;
    @C0064Am(aul = "821804cd355b30f54c561e30d7d25ea0", aum = -1)
    public byte dAI;
    @C0064Am(aul = "219a042b26267a43f7e1b801a1332d30", aum = 16)
    public boolean dAa;
    @C0064Am(aul = "6a4c2a2c913fb14c91a6e1c42296c372", aum = 17)
    public boolean dAb;
    @C0064Am(aul = "821804cd355b30f54c561e30d7d25ea0", aum = 20)
    public ItemDurability dAc;
    @C0064Am(aul = "c7452a0dc441c90d3a53aabc27e4c1fc", aum = -2)
    public long dAd;
    @C0064Am(aul = "9c5539b1279d8bd9fe780e45622ab34b", aum = -2)
    public long dAe;
    @C0064Am(aul = "af7b3499839a154ef6bc93e5dea76412", aum = -2)
    public long dAf;
    @C0064Am(aul = "b4343d42bc813f96de1273b1fa544374", aum = -2)
    public long dAg;
    @C0064Am(aul = "1c6ea46d83971e4c1287e307e65f3861", aum = -2)
    public long dAh;
    @C0064Am(aul = "725d666e1d4cbcba5e14994444fb6a3f", aum = -2)
    public long dAi;
    @C0064Am(aul = "6132cf056f1e8309fbe04942b0dcfdd5", aum = -2)
    public long dAj;
    @C0064Am(aul = "40be3b8685328860ee01609d3beb5b4e", aum = -2)
    public long dAk;
    @C0064Am(aul = "c1f45ed45e7875b30881ee70a4c39c8e", aum = -2)
    public long dAl;
    @C0064Am(aul = "13fd94945c288e737e625bbcf075462d", aum = -2)
    public long dAm;
    @C0064Am(aul = "86678bf4ad3b6eb5f931fd0e118cb07d", aum = -2)
    public long dAn;
    @C0064Am(aul = "53cb5dbadd7e2f7436dcb8a50c9f2a42", aum = -2)
    public long dAo;
    @C0064Am(aul = "459baf016d809a4bb4da06ac821b3fb6", aum = -2)
    public long dAp;
    @C0064Am(aul = "219a042b26267a43f7e1b801a1332d30", aum = -2)
    public long dAq;
    @C0064Am(aul = "6a4c2a2c913fb14c91a6e1c42296c372", aum = -2)
    public long dAr;
    @C0064Am(aul = "821804cd355b30f54c561e30d7d25ea0", aum = -2)
    public long dAs;
    @C0064Am(aul = "c7452a0dc441c90d3a53aabc27e4c1fc", aum = -1)
    public byte dAt;
    @C0064Am(aul = "9c5539b1279d8bd9fe780e45622ab34b", aum = -1)
    public byte dAu;
    @C0064Am(aul = "af7b3499839a154ef6bc93e5dea76412", aum = -1)
    public byte dAv;
    @C0064Am(aul = "b4343d42bc813f96de1273b1fa544374", aum = -1)
    public byte dAw;
    @C0064Am(aul = "1c6ea46d83971e4c1287e307e65f3861", aum = -1)
    public byte dAx;
    @C0064Am(aul = "725d666e1d4cbcba5e14994444fb6a3f", aum = -1)
    public byte dAy;
    @C0064Am(aul = "6132cf056f1e8309fbe04942b0dcfdd5", aum = -1)
    public byte dAz;
    @C0064Am(aul = "459baf016d809a4bb4da06ac821b3fb6", aum = 15)
    public boolean dzZ;
    @C0064Am(aul = "ca428daaff0a5283aa010914f6f49ae9", aum = 18)
    public boolean enabled;
    @C0064Am(aul = "ca428daaff0a5283aa010914f6f49ae9", aum = -2)

    /* renamed from: jC */
    public long f1155jC;
    @C0064Am(aul = "8074b666b5934f5c82462b12c2e322d6", aum = -1)

    /* renamed from: jK */
    public byte f1156jK;
    @C0064Am(aul = "ca428daaff0a5283aa010914f6f49ae9", aum = -1)

    /* renamed from: jO */
    public byte f1157jO;
    @C0064Am(aul = "8074b666b5934f5c82462b12c2e322d6", aum = 5)

    /* renamed from: jn */
    public Asset f1158jn;
    @C0064Am(aul = "8074b666b5934f5c82462b12c2e322d6", aum = -2)

    /* renamed from: jy */
    public long f1159jy;
    @C0064Am(aul = "7679847a1e7157016fadec2efb6cd451", aum = 1)

    /* renamed from: nh */
    public I18NString f1160nh;
    @C0064Am(aul = "7679847a1e7157016fadec2efb6cd451", aum = -2)

    /* renamed from: nk */
    public long f1161nk;
    @C0064Am(aul = "7679847a1e7157016fadec2efb6cd451", aum = -1)

    /* renamed from: nn */
    public byte f1162nn;
    @C0064Am(aul = "65cc2adcbcbe32ef26363345f0f65861", aum = 0)

    /* renamed from: zP */
    public I18NString f1163zP;

    public C0887Ms() {
    }

    public C0887Ms(C1616Xf xf) {
        super(xf);
    }

    /* renamed from: c */
    public C5663aRz[] mo309c() {
        return Item._m_fields;
    }

    /* renamed from: d */
    public void mo310d() {
        super.mo310d();
        this.f1163zP = null;
        this.f1160nh = null;
        this.amU = 0;
        this.amW = 0.0f;
        this.amY = null;
        this.f1158jn = null;
        this.ana = null;
        this.anc = 0;
        this.ane = 0;
        this.ang = 0;
        this.ani = 0;
        this.ank = 0;
        this.anm = 0;
        this.ano = 0;
        this.anq = 0;
        this.dzZ = false;
        this.dAa = false;
        this.dAb = false;
        this.enabled = false;
        this.apb = null;
        this.dAc = null;
        this.f1153Nf = 0;
        this.f1161nk = 0;
        this.dAd = 0;
        this.dAe = 0;
        this.dAf = 0;
        this.f1159jy = 0;
        this.dAg = 0;
        this.dAh = 0;
        this.dAi = 0;
        this.dAj = 0;
        this.dAk = 0;
        this.dAl = 0;
        this.dAm = 0;
        this.dAn = 0;
        this.dAo = 0;
        this.dAp = 0;
        this.dAq = 0;
        this.dAr = 0;
        this.f1155jC = 0;
        this.cCP = 0;
        this.dAs = 0;
        this.f1154Nh = 0;
        this.f1162nn = 0;
        this.dAt = 0;
        this.dAu = 0;
        this.dAv = 0;
        this.f1156jK = 0;
        this.dAw = 0;
        this.dAx = 0;
        this.dAy = 0;
        this.dAz = 0;
        this.dAA = 0;
        this.dAB = 0;
        this.dAC = 0;
        this.dAD = 0;
        this.dAE = 0;
        this.dAF = 0;
        this.dAG = 0;
        this.dAH = 0;
        this.f1157jO = 0;
        this.cCW = 0;
        this.dAI = 0;
    }
}
