package logic.data.mbean;

import game.network.message.serializable.C2686iZ;
import game.script.Actor;
import game.script.Character;
import game.script.CharacterView;
import game.script.avatar.Avatar;
import game.script.progression.CharacterProgression;
import game.script.ship.Ship;
import logic.baa.C1616Xf;
import logic.res.code.C5663aRz;
import p001a.C0064Am;

/* renamed from: a.CW */
public class C0205CW extends C3805vD {
    @C0064Am(aul = "69b8ca7851c060d0088f3d13eeca262f", aum = 0)
    public Controller cCG;
    @C0064Am(aul = "a5b0aefc45f7da97189f30aa36497c4d", aum = 1)
    public C2686iZ<Character> cCH;
    @C0064Am(aul = "a839415b84e472cdba450e8abd7051d3", aum = 2)
    public Actor cCI;
    @C0064Am(aul = "f5b04bc5b78149e943ae29d9da0054e4", aum = 3)
    public LDScriptingController cCJ;
    @C0064Am(aul = "29b922298bfb107d776a289019824c03", aum = 4)
    public Avatar cCK;
    @C0064Am(aul = "9e81bcb31bde2975c0622a3d4365eb64", aum = 5)
    public Ship cCL;
    @C0064Am(aul = "99c72f679bc78d75f85c432fab5ff687", aum = 7)
    public CharacterView cCM;
    @C0064Am(aul = "69b8ca7851c060d0088f3d13eeca262f", aum = -2)
    public long cCN;
    @C0064Am(aul = "a5b0aefc45f7da97189f30aa36497c4d", aum = -2)
    public long cCO;
    @C0064Am(aul = "a839415b84e472cdba450e8abd7051d3", aum = -2)
    public long cCP;
    @C0064Am(aul = "f5b04bc5b78149e943ae29d9da0054e4", aum = -2)
    public long cCQ;
    @C0064Am(aul = "29b922298bfb107d776a289019824c03", aum = -2)
    public long cCR;
    @C0064Am(aul = "9e81bcb31bde2975c0622a3d4365eb64", aum = -2)
    public long cCS;
    @C0064Am(aul = "99c72f679bc78d75f85c432fab5ff687", aum = -2)
    public long cCT;
    @C0064Am(aul = "69b8ca7851c060d0088f3d13eeca262f", aum = -1)
    public byte cCU;
    @C0064Am(aul = "a5b0aefc45f7da97189f30aa36497c4d", aum = -1)
    public byte cCV;
    @C0064Am(aul = "a839415b84e472cdba450e8abd7051d3", aum = -1)
    public byte cCW;
    @C0064Am(aul = "f5b04bc5b78149e943ae29d9da0054e4", aum = -1)
    public byte cCX;
    @C0064Am(aul = "29b922298bfb107d776a289019824c03", aum = -1)
    public byte cCY;
    @C0064Am(aul = "9e81bcb31bde2975c0622a3d4365eb64", aum = -1)
    public byte cCZ;
    @C0064Am(aul = "99c72f679bc78d75f85c432fab5ff687", aum = -1)
    public byte cDa;
    @C0064Am(aul = "cd522d38c4334184ed39f93d6bf95088", aum = -2)

    /* renamed from: jA */
    public long f346jA;
    @C0064Am(aul = "cd522d38c4334184ed39f93d6bf95088", aum = -1)

    /* renamed from: jM */
    public byte f347jM;
    @C0064Am(aul = "cd522d38c4334184ed39f93d6bf95088", aum = 6)

    /* renamed from: jp */
    public CharacterProgression f348jp;

    public C0205CW() {
    }

    public C0205CW(C1616Xf xf) {
        super(xf);
    }

    /* renamed from: c */
    public C5663aRz[] mo309c() {
        return Character._m_fields;
    }

    /* renamed from: d */
    public void mo310d() {
        super.mo310d();
        this.cCG = null;
        this.cCH = null;
        this.cCI = null;
        this.cCJ = null;
        this.cCK = null;
        this.cCL = null;
        this.f348jp = null;
        this.cCM = null;
        this.cCN = 0;
        this.cCO = 0;
        this.cCP = 0;
        this.cCQ = 0;
        this.cCR = 0;
        this.cCS = 0;
        this.f346jA = 0;
        this.cCT = 0;
        this.cCU = 0;
        this.cCV = 0;
        this.cCW = 0;
        this.cCX = 0;
        this.cCY = 0;
        this.cCZ = 0;
        this.f347jM = 0;
        this.cDa = 0;
    }
}
