package logic.data.mbean;

import game.script.corporation.NLSCorporation;
import logic.baa.C1616Xf;
import logic.res.code.C5663aRz;
import p001a.C0064Am;
import taikodom.infra.script.I18NString;

/* renamed from: a.aHW */
public class aHW extends C2484fi {
    @C0064Am(aul = "7c16abe8a3219094491fe47b47d6a5ef", aum = 4)

    /* renamed from: FA */
    public I18NString f2929FA;
    @C0064Am(aul = "84e610fc3300c39317ad030e71bb9fbb", aum = 5)

    /* renamed from: FC */
    public I18NString f2930FC;
    @C0064Am(aul = "df92211e98fb81bdf740b01e0be8143e", aum = 6)

    /* renamed from: FE */
    public I18NString f2931FE;
    @C0064Am(aul = "3713c1c7a29449755ef2987dbee7eec4", aum = 7)

    /* renamed from: FG */
    public I18NString f2932FG;
    @C0064Am(aul = "603c714667326945127999528e9a3c8e", aum = 8)

    /* renamed from: FI */
    public I18NString f2933FI;
    @C0064Am(aul = "06274d3e230d3f7f7047a2f3819b81c0", aum = 9)

    /* renamed from: FK */
    public I18NString f2934FK;
    @C0064Am(aul = "369cd202761d3edd4f5342ed3dd968fe", aum = 10)

    /* renamed from: FM */
    public I18NString f2935FM;
    @C0064Am(aul = "e6b560034142927317ffc323e25d8aaf", aum = 11)

    /* renamed from: FO */
    public I18NString f2936FO;
    @C0064Am(aul = "7b1edaaa030a93209ba3dcbea82d2e4b", aum = 12)

    /* renamed from: FQ */
    public I18NString f2937FQ;
    @C0064Am(aul = "3e84b511f24a9e9253106da163d4fddd", aum = 13)

    /* renamed from: FS */
    public I18NString f2938FS;
    @C0064Am(aul = "02e88e8a430c454e18c4bc3c31b69b0c", aum = 14)

    /* renamed from: FU */
    public I18NString f2939FU;
    @C0064Am(aul = "585197d91122eae117f9ed1544d35be4", aum = 15)

    /* renamed from: FW */
    public I18NString f2940FW;
    @C0064Am(aul = "a7f858ce9d1d3fe6a01255ff345d6c08", aum = 16)

    /* renamed from: FY */
    public I18NString f2941FY;
    @C0064Am(aul = "ae0a3f9789771b3d8fae6ff5bf2b0c05", aum = 0)

    /* renamed from: Fs */
    public I18NString f2942Fs;
    @C0064Am(aul = "5240c3014370be90421d3995cfe21911", aum = 1)

    /* renamed from: Fu */
    public I18NString f2943Fu;
    @C0064Am(aul = "048493ce3c89dd2a2b5fb7c68c5e2e39", aum = 2)

    /* renamed from: Fw */
    public I18NString f2944Fw;
    @C0064Am(aul = "61c741ece75455d448cdcb9531b6256b", aum = 3)

    /* renamed from: Fy */
    public I18NString f2945Fy;
    @C0064Am(aul = "039769c0c9c38c32ab2770e089ba68c5", aum = 30)

    /* renamed from: GA */
    public I18NString f2946GA;
    @C0064Am(aul = "645e635645891f471814e21152386ea7", aum = 31)

    /* renamed from: GC */
    public I18NString f2947GC;
    @C0064Am(aul = "7829d1880d773fd88c8127be4c279a25", aum = 32)

    /* renamed from: GF */
    public I18NString f2948GF;
    @C0064Am(aul = "651b5e33e2b43b44ecee9fd59d8c828a", aum = 33)

    /* renamed from: GH */
    public NLSCorporation.NamePanel f2949GH;
    @C0064Am(aul = "874d8649eb5f337195df7211124d6f88", aum = 34)

    /* renamed from: GJ */
    public NLSCorporation.LogoPanel f2950GJ;
    @C0064Am(aul = "6b395780d242d43ed486c4e5a585d6bb", aum = 35)

    /* renamed from: GL */
    public NLSCorporation.InvitePanel f2951GL;
    @C0064Am(aul = "b528b809fc80c7ef07929f9d3baba981", aum = 36)

    /* renamed from: GN */
    public NLSCorporation.PVPInvitation f2952GN;
    @C0064Am(aul = "5a8d8e335bbfa4801345cf1cbdf869e4", aum = 37)

    /* renamed from: GP */
    public I18NString f2953GP;
    @C0064Am(aul = "1282f2e82d3cddf0182e6346c10e8e82", aum = 38)

    /* renamed from: GR */
    public I18NString f2954GR;
    @C0064Am(aul = "2269a1872d1a1c6b95825cecffbc2913", aum = 39)

    /* renamed from: GU */
    public I18NString f2955GU;
    @C0064Am(aul = "c77a3f559778990aa33ef8e5d4827d4c", aum = 40)

    /* renamed from: GW */
    public I18NString f2956GW;
    @C0064Am(aul = "37d959669dd8e9a63e125ba37d0f0c95", aum = 41)

    /* renamed from: GY */
    public I18NString f2957GY;
    @C0064Am(aul = "a4087341b62880c4e18a7cf22f138fd7", aum = 17)

    /* renamed from: Ga */
    public I18NString f2958Ga;
    @C0064Am(aul = "c85eadbb3c8504bdc1f2a942a39b6fa7", aum = 18)

    /* renamed from: Gc */
    public I18NString f2959Gc;
    @C0064Am(aul = "7c72067d5416dab20770355eb8b64d41", aum = 19)

    /* renamed from: Ge */
    public I18NString f2960Ge;
    @C0064Am(aul = "7304872057ccfda1d95d8f879b951353", aum = 20)

    /* renamed from: Gg */
    public I18NString f2961Gg;
    @C0064Am(aul = "1c9f8a5958c219bc32ecf2638adc8981", aum = 21)

    /* renamed from: Gi */
    public I18NString f2962Gi;
    @C0064Am(aul = "0cca83f57c69a557677c2a237f3588a2", aum = 22)

    /* renamed from: Gk */
    public I18NString f2963Gk;
    @C0064Am(aul = "aed2ca36b7526557d6895bade442ff51", aum = 23)

    /* renamed from: Gm */
    public I18NString f2964Gm;
    @C0064Am(aul = "cbffa36f291a00fbb10109343a365af4", aum = 24)

    /* renamed from: Go */
    public I18NString f2965Go;
    @C0064Am(aul = "afabc721967bd1c3bb44d46b5ce8a7c9", aum = 25)

    /* renamed from: Gq */
    public I18NString f2966Gq;
    @C0064Am(aul = "85b73ca0a72f30226d22d1a81c806a47", aum = 26)

    /* renamed from: Gs */
    public I18NString f2967Gs;
    @C0064Am(aul = "a9a549d3cc26ee13ade970006b331bf4", aum = 27)

    /* renamed from: Gu */
    public I18NString f2968Gu;
    @C0064Am(aul = "d475a4de9b849ad6740d06fdeefa6ff3", aum = 28)

    /* renamed from: Gw */
    public I18NString f2969Gw;
    @C0064Am(aul = "8bd2d84837af2d94b459d54b8085c67a", aum = 29)

    /* renamed from: Gy */
    public I18NString f2970Gy;
    @C0064Am(aul = "37d959669dd8e9a63e125ba37d0f0c95", aum = -2)
    public long faF;
    @C0064Am(aul = "37d959669dd8e9a63e125ba37d0f0c95", aum = -1)
    public byte faM;
    @C0064Am(aul = "1c9f8a5958c219bc32ecf2638adc8981", aum = -2)
    public long hZA;
    @C0064Am(aul = "0cca83f57c69a557677c2a237f3588a2", aum = -2)
    public long hZB;
    @C0064Am(aul = "aed2ca36b7526557d6895bade442ff51", aum = -2)
    public long hZC;
    @C0064Am(aul = "cbffa36f291a00fbb10109343a365af4", aum = -2)
    public long hZD;
    @C0064Am(aul = "afabc721967bd1c3bb44d46b5ce8a7c9", aum = -2)
    public long hZE;
    @C0064Am(aul = "85b73ca0a72f30226d22d1a81c806a47", aum = -2)
    public long hZF;
    @C0064Am(aul = "a9a549d3cc26ee13ade970006b331bf4", aum = -2)
    public long hZG;
    @C0064Am(aul = "d475a4de9b849ad6740d06fdeefa6ff3", aum = -2)
    public long hZH;
    @C0064Am(aul = "8bd2d84837af2d94b459d54b8085c67a", aum = -2)
    public long hZI;
    @C0064Am(aul = "039769c0c9c38c32ab2770e089ba68c5", aum = -2)
    public long hZJ;
    @C0064Am(aul = "645e635645891f471814e21152386ea7", aum = -2)
    public long hZK;
    @C0064Am(aul = "7829d1880d773fd88c8127be4c279a25", aum = -2)
    public long hZL;
    @C0064Am(aul = "651b5e33e2b43b44ecee9fd59d8c828a", aum = -2)
    public long hZM;
    @C0064Am(aul = "874d8649eb5f337195df7211124d6f88", aum = -2)
    public long hZN;
    @C0064Am(aul = "6b395780d242d43ed486c4e5a585d6bb", aum = -2)
    public long hZO;
    @C0064Am(aul = "b528b809fc80c7ef07929f9d3baba981", aum = -2)
    public long hZP;
    @C0064Am(aul = "5a8d8e335bbfa4801345cf1cbdf869e4", aum = -2)
    public long hZQ;
    @C0064Am(aul = "1282f2e82d3cddf0182e6346c10e8e82", aum = -2)
    public long hZR;
    @C0064Am(aul = "2269a1872d1a1c6b95825cecffbc2913", aum = -2)
    public long hZS;
    @C0064Am(aul = "c77a3f559778990aa33ef8e5d4827d4c", aum = -2)
    public long hZT;
    @C0064Am(aul = "ae0a3f9789771b3d8fae6ff5bf2b0c05", aum = -1)
    public byte hZU;
    @C0064Am(aul = "5240c3014370be90421d3995cfe21911", aum = -1)
    public byte hZV;
    @C0064Am(aul = "048493ce3c89dd2a2b5fb7c68c5e2e39", aum = -1)
    public byte hZW;
    @C0064Am(aul = "61c741ece75455d448cdcb9531b6256b", aum = -1)
    public byte hZX;
    @C0064Am(aul = "7c16abe8a3219094491fe47b47d6a5ef", aum = -1)
    public byte hZY;
    @C0064Am(aul = "84e610fc3300c39317ad030e71bb9fbb", aum = -1)
    public byte hZZ;
    @C0064Am(aul = "ae0a3f9789771b3d8fae6ff5bf2b0c05", aum = -2)
    public long hZf;
    @C0064Am(aul = "5240c3014370be90421d3995cfe21911", aum = -2)
    public long hZg;
    @C0064Am(aul = "048493ce3c89dd2a2b5fb7c68c5e2e39", aum = -2)
    public long hZh;
    @C0064Am(aul = "61c741ece75455d448cdcb9531b6256b", aum = -2)
    public long hZi;
    @C0064Am(aul = "7c16abe8a3219094491fe47b47d6a5ef", aum = -2)
    public long hZj;
    @C0064Am(aul = "84e610fc3300c39317ad030e71bb9fbb", aum = -2)
    public long hZk;
    @C0064Am(aul = "df92211e98fb81bdf740b01e0be8143e", aum = -2)
    public long hZl;
    @C0064Am(aul = "3713c1c7a29449755ef2987dbee7eec4", aum = -2)
    public long hZm;
    @C0064Am(aul = "603c714667326945127999528e9a3c8e", aum = -2)
    public long hZn;
    @C0064Am(aul = "06274d3e230d3f7f7047a2f3819b81c0", aum = -2)
    public long hZo;
    @C0064Am(aul = "369cd202761d3edd4f5342ed3dd968fe", aum = -2)
    public long hZp;
    @C0064Am(aul = "e6b560034142927317ffc323e25d8aaf", aum = -2)
    public long hZq;
    @C0064Am(aul = "7b1edaaa030a93209ba3dcbea82d2e4b", aum = -2)
    public long hZr;
    @C0064Am(aul = "3e84b511f24a9e9253106da163d4fddd", aum = -2)
    public long hZs;
    @C0064Am(aul = "02e88e8a430c454e18c4bc3c31b69b0c", aum = -2)
    public long hZt;
    @C0064Am(aul = "585197d91122eae117f9ed1544d35be4", aum = -2)
    public long hZu;
    @C0064Am(aul = "a7f858ce9d1d3fe6a01255ff345d6c08", aum = -2)
    public long hZv;
    @C0064Am(aul = "a4087341b62880c4e18a7cf22f138fd7", aum = -2)
    public long hZw;
    @C0064Am(aul = "c85eadbb3c8504bdc1f2a942a39b6fa7", aum = -2)
    public long hZx;
    @C0064Am(aul = "7c72067d5416dab20770355eb8b64d41", aum = -2)
    public long hZy;
    @C0064Am(aul = "7304872057ccfda1d95d8f879b951353", aum = -2)
    public long hZz;
    @C0064Am(aul = "7829d1880d773fd88c8127be4c279a25", aum = -1)
    public byte iaA;
    @C0064Am(aul = "651b5e33e2b43b44ecee9fd59d8c828a", aum = -1)
    public byte iaB;
    @C0064Am(aul = "874d8649eb5f337195df7211124d6f88", aum = -1)
    public byte iaC;
    @C0064Am(aul = "6b395780d242d43ed486c4e5a585d6bb", aum = -1)
    public byte iaD;
    @C0064Am(aul = "b528b809fc80c7ef07929f9d3baba981", aum = -1)
    public byte iaE;
    @C0064Am(aul = "5a8d8e335bbfa4801345cf1cbdf869e4", aum = -1)
    public byte iaF;
    @C0064Am(aul = "1282f2e82d3cddf0182e6346c10e8e82", aum = -1)
    public byte iaG;
    @C0064Am(aul = "2269a1872d1a1c6b95825cecffbc2913", aum = -1)
    public byte iaH;
    @C0064Am(aul = "c77a3f559778990aa33ef8e5d4827d4c", aum = -1)
    public byte iaI;
    @C0064Am(aul = "df92211e98fb81bdf740b01e0be8143e", aum = -1)
    public byte iaa;
    @C0064Am(aul = "3713c1c7a29449755ef2987dbee7eec4", aum = -1)
    public byte iab;
    @C0064Am(aul = "603c714667326945127999528e9a3c8e", aum = -1)
    public byte iac;
    @C0064Am(aul = "06274d3e230d3f7f7047a2f3819b81c0", aum = -1)
    public byte iad;
    @C0064Am(aul = "369cd202761d3edd4f5342ed3dd968fe", aum = -1)
    public byte iae;
    @C0064Am(aul = "e6b560034142927317ffc323e25d8aaf", aum = -1)
    public byte iaf;
    @C0064Am(aul = "7b1edaaa030a93209ba3dcbea82d2e4b", aum = -1)
    public byte iag;
    @C0064Am(aul = "3e84b511f24a9e9253106da163d4fddd", aum = -1)
    public byte iah;
    @C0064Am(aul = "02e88e8a430c454e18c4bc3c31b69b0c", aum = -1)
    public byte iai;
    @C0064Am(aul = "585197d91122eae117f9ed1544d35be4", aum = -1)
    public byte iaj;
    @C0064Am(aul = "a7f858ce9d1d3fe6a01255ff345d6c08", aum = -1)
    public byte iak;
    @C0064Am(aul = "a4087341b62880c4e18a7cf22f138fd7", aum = -1)
    public byte ial;
    @C0064Am(aul = "c85eadbb3c8504bdc1f2a942a39b6fa7", aum = -1)
    public byte iam;
    @C0064Am(aul = "7c72067d5416dab20770355eb8b64d41", aum = -1)
    public byte ian;
    @C0064Am(aul = "7304872057ccfda1d95d8f879b951353", aum = -1)
    public byte iao;
    @C0064Am(aul = "1c9f8a5958c219bc32ecf2638adc8981", aum = -1)
    public byte iap;
    @C0064Am(aul = "0cca83f57c69a557677c2a237f3588a2", aum = -1)
    public byte iaq;
    @C0064Am(aul = "aed2ca36b7526557d6895bade442ff51", aum = -1)
    public byte iar;
    @C0064Am(aul = "cbffa36f291a00fbb10109343a365af4", aum = -1)
    public byte ias;
    @C0064Am(aul = "afabc721967bd1c3bb44d46b5ce8a7c9", aum = -1)
    public byte iat;
    @C0064Am(aul = "85b73ca0a72f30226d22d1a81c806a47", aum = -1)
    public byte iau;
    @C0064Am(aul = "a9a549d3cc26ee13ade970006b331bf4", aum = -1)
    public byte iav;
    @C0064Am(aul = "d475a4de9b849ad6740d06fdeefa6ff3", aum = -1)
    public byte iaw;
    @C0064Am(aul = "8bd2d84837af2d94b459d54b8085c67a", aum = -1)
    public byte iax;
    @C0064Am(aul = "039769c0c9c38c32ab2770e089ba68c5", aum = -1)
    public byte iay;
    @C0064Am(aul = "645e635645891f471814e21152386ea7", aum = -1)
    public byte iaz;

    public aHW() {
    }

    public aHW(C1616Xf xf) {
        super(xf);
    }

    /* renamed from: c */
    public C5663aRz[] mo309c() {
        return NLSCorporation._m_fields;
    }

    /* renamed from: d */
    public void mo310d() {
        super.mo310d();
        this.f2942Fs = null;
        this.f2943Fu = null;
        this.f2944Fw = null;
        this.f2945Fy = null;
        this.f2929FA = null;
        this.f2930FC = null;
        this.f2931FE = null;
        this.f2932FG = null;
        this.f2933FI = null;
        this.f2934FK = null;
        this.f2935FM = null;
        this.f2936FO = null;
        this.f2937FQ = null;
        this.f2938FS = null;
        this.f2939FU = null;
        this.f2940FW = null;
        this.f2941FY = null;
        this.f2958Ga = null;
        this.f2959Gc = null;
        this.f2960Ge = null;
        this.f2961Gg = null;
        this.f2962Gi = null;
        this.f2963Gk = null;
        this.f2964Gm = null;
        this.f2965Go = null;
        this.f2966Gq = null;
        this.f2967Gs = null;
        this.f2968Gu = null;
        this.f2969Gw = null;
        this.f2970Gy = null;
        this.f2946GA = null;
        this.f2947GC = null;
        this.f2948GF = null;
        this.f2949GH = null;
        this.f2950GJ = null;
        this.f2951GL = null;
        this.f2952GN = null;
        this.f2953GP = null;
        this.f2954GR = null;
        this.f2955GU = null;
        this.f2956GW = null;
        this.f2957GY = null;
        this.hZf = 0;
        this.hZg = 0;
        this.hZh = 0;
        this.hZi = 0;
        this.hZj = 0;
        this.hZk = 0;
        this.hZl = 0;
        this.hZm = 0;
        this.hZn = 0;
        this.hZo = 0;
        this.hZp = 0;
        this.hZq = 0;
        this.hZr = 0;
        this.hZs = 0;
        this.hZt = 0;
        this.hZu = 0;
        this.hZv = 0;
        this.hZw = 0;
        this.hZx = 0;
        this.hZy = 0;
        this.hZz = 0;
        this.hZA = 0;
        this.hZB = 0;
        this.hZC = 0;
        this.hZD = 0;
        this.hZE = 0;
        this.hZF = 0;
        this.hZG = 0;
        this.hZH = 0;
        this.hZI = 0;
        this.hZJ = 0;
        this.hZK = 0;
        this.hZL = 0;
        this.hZM = 0;
        this.hZN = 0;
        this.hZO = 0;
        this.hZP = 0;
        this.hZQ = 0;
        this.hZR = 0;
        this.hZS = 0;
        this.hZT = 0;
        this.faF = 0;
        this.hZU = 0;
        this.hZV = 0;
        this.hZW = 0;
        this.hZX = 0;
        this.hZY = 0;
        this.hZZ = 0;
        this.iaa = 0;
        this.iab = 0;
        this.iac = 0;
        this.iad = 0;
        this.iae = 0;
        this.iaf = 0;
        this.iag = 0;
        this.iah = 0;
        this.iai = 0;
        this.iaj = 0;
        this.iak = 0;
        this.ial = 0;
        this.iam = 0;
        this.ian = 0;
        this.iao = 0;
        this.iap = 0;
        this.iaq = 0;
        this.iar = 0;
        this.ias = 0;
        this.iat = 0;
        this.iau = 0;
        this.iav = 0;
        this.iaw = 0;
        this.iax = 0;
        this.iay = 0;
        this.iaz = 0;
        this.iaA = 0;
        this.iaB = 0;
        this.iaC = 0;
        this.iaD = 0;
        this.iaE = 0;
        this.iaF = 0;
        this.iaG = 0;
        this.iaH = 0;
        this.iaI = 0;
        this.faM = 0;
    }
}
