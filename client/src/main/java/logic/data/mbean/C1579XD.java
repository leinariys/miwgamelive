package logic.data.mbean;

import game.script.mission.MissionTemplateStep;
import logic.baa.C1616Xf;
import logic.res.code.C5663aRz;
import p001a.C0064Am;
import taikodom.infra.script.I18NString;

import java.util.UUID;

/* renamed from: a.XD */
public class C1579XD extends C5292aDs {
    @C0064Am(aul = "ef8b1dc82414d3cbe1884e4be917d88e", aum = 2)

    /* renamed from: bK */
    public UUID f2102bK;
    @C0064Am(aul = "f5c45b64a7b1fc182d9672175a601d0e", aum = 0)
    public String handle;
    @C0064Am(aul = "29fec800b5d4edd51472a81968b1456a", aum = 1)

    /* renamed from: nh */
    public I18NString f2103nh;
    @C0064Am(aul = "29fec800b5d4edd51472a81968b1456a", aum = -2)

    /* renamed from: nk */
    public long f2104nk;
    @C0064Am(aul = "29fec800b5d4edd51472a81968b1456a", aum = -1)

    /* renamed from: nn */
    public byte f2105nn;
    @C0064Am(aul = "ef8b1dc82414d3cbe1884e4be917d88e", aum = -2)

    /* renamed from: oL */
    public long f2106oL;
    @C0064Am(aul = "ef8b1dc82414d3cbe1884e4be917d88e", aum = -1)

    /* renamed from: oS */
    public byte f2107oS;
    @C0064Am(aul = "f5c45b64a7b1fc182d9672175a601d0e", aum = -2)

    /* renamed from: ok */
    public long f2108ok;
    @C0064Am(aul = "f5c45b64a7b1fc182d9672175a601d0e", aum = -1)

    /* renamed from: on */
    public byte f2109on;

    public C1579XD() {
    }

    public C1579XD(C1616Xf xf) {
        super(xf);
    }

    /* renamed from: c */
    public C5663aRz[] mo309c() {
        return MissionTemplateStep._m_fields;
    }

    /* renamed from: d */
    public void mo310d() {
        super.mo310d();
        this.handle = null;
        this.f2103nh = null;
        this.f2102bK = null;
        this.f2108ok = 0;
        this.f2104nk = 0;
        this.f2106oL = 0;
        this.f2109on = 0;
        this.f2105nn = 0;
        this.f2107oS = 0;
    }
}
