package logic.data.mbean;

import game.network.message.serializable.C3438ra;
import game.script.citizenship.CitizenshipOffice;
import game.script.citizenship.CitizenshipPack;
import logic.baa.C1616Xf;
import logic.res.code.C5663aRz;
import p001a.C0064Am;

import java.util.UUID;

/* renamed from: a.aPx  reason: case insensitive filesystem */
public class C5609aPx extends C3805vD {
    @C0064Am(aul = "c73e27256ab0980240e199affff14a5b", aum = 1)

    /* renamed from: bK */
    public UUID f3567bK;
    @C0064Am(aul = "5f8d6be7b6affd0f8b908f241f4d443d", aum = 0)
    public C3438ra<CitizenshipPack> edL;
    @C0064Am(aul = "33eaa92c84142445e50f53bdfc4c6539", aum = 2)
    public String handle;
    @C0064Am(aul = "5f8d6be7b6affd0f8b908f241f4d443d", aum = -2)
    public long iBK;
    @C0064Am(aul = "5f8d6be7b6affd0f8b908f241f4d443d", aum = -1)
    public byte iBL;
    @C0064Am(aul = "c73e27256ab0980240e199affff14a5b", aum = -2)

    /* renamed from: oL */
    public long f3568oL;
    @C0064Am(aul = "c73e27256ab0980240e199affff14a5b", aum = -1)

    /* renamed from: oS */
    public byte f3569oS;
    @C0064Am(aul = "33eaa92c84142445e50f53bdfc4c6539", aum = -2)

    /* renamed from: ok */
    public long f3570ok;
    @C0064Am(aul = "33eaa92c84142445e50f53bdfc4c6539", aum = -1)

    /* renamed from: on */
    public byte f3571on;

    public C5609aPx() {
    }

    public C5609aPx(C1616Xf xf) {
        super(xf);
    }

    /* renamed from: c */
    public C5663aRz[] mo309c() {
        return CitizenshipOffice._m_fields;
    }

    /* renamed from: d */
    public void mo310d() {
        super.mo310d();
        this.edL = null;
        this.f3567bK = null;
        this.handle = null;
        this.iBK = 0;
        this.f3568oL = 0;
        this.f3570ok = 0;
        this.iBL = 0;
        this.f3569oS = 0;
        this.f3571on = 0;
    }
}
