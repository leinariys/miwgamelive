package logic.data.mbean;

import game.script.contract.types.TransportContract;
import game.script.contract.types.TransportTemplate;
import game.script.item.BagItem;
import game.script.ship.Station;
import logic.baa.C1616Xf;
import logic.res.code.C5663aRz;
import p001a.C0064Am;

/* renamed from: a.aKO */
public class aKO extends C4100zT {
    @C0064Am(aul = "6adc64975954fb722a303835fff1b00f", aum = -2)

    /* renamed from: Kg */
    public long f3245Kg;
    @C0064Am(aul = "6adc64975954fb722a303835fff1b00f", aum = -1)

    /* renamed from: Ki */
    public byte f3246Ki;
    @C0064Am(aul = "0b183978d3b13f5930367cc6c1092775", aum = 1)
    public Station ajx;
    @C0064Am(aul = "7cf39c06273adc66e890d1c954796755", aum = 0)
    public Station ajy;
    @C0064Am(aul = "0b183978d3b13f5930367cc6c1092775", aum = -2)
    public long bZo;
    @C0064Am(aul = "0b183978d3b13f5930367cc6c1092775", aum = -1)
    public byte bZu;
    @C0064Am(aul = "a7f384ce550f4529a0a880bdaee02e2a", aum = 2)
    public BagItem dnc;
    @C0064Am(aul = "6adc64975954fb722a303835fff1b00f", aum = 3)
    public TransportContract<TransportTemplate> dne;
    @C0064Am(aul = "7cf39c06273adc66e890d1c954796755", aum = -2)
    public long esB;
    @C0064Am(aul = "7cf39c06273adc66e890d1c954796755", aum = -1)
    public byte esF;
    @C0064Am(aul = "a7f384ce550f4529a0a880bdaee02e2a", aum = -2)
    public long ijF;
    @C0064Am(aul = "a7f384ce550f4529a0a880bdaee02e2a", aum = -1)
    public byte ijG;

    public aKO() {
    }

    public aKO(C1616Xf xf) {
        super(xf);
    }

    /* renamed from: c */
    public C5663aRz[] mo309c() {
        return TransportTemplate._m_fields;
    }

    /* renamed from: d */
    public void mo310d() {
        super.mo310d();
        this.ajy = null;
        this.ajx = null;
        this.dnc = null;
        this.dne = null;
        this.esB = 0;
        this.bZo = 0;
        this.ijF = 0;
        this.f3245Kg = 0;
        this.esF = 0;
        this.bZu = 0;
        this.ijG = 0;
        this.f3246Ki = 0;
    }
}
