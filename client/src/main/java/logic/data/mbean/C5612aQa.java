package logic.data.mbean;

import game.network.message.serializable.C1556Wo;
import game.network.message.serializable.C3438ra;
import game.script.consignment.ConsignmentFeeManager;
import game.script.item.ItemType;
import game.script.newmarket.BuyOrder;
import game.script.newmarket.ListContainer;
import game.script.newmarket.Market;
import game.script.newmarket.SellOrder;
import game.script.ship.Station;
import logic.baa.C1616Xf;
import logic.res.code.C5663aRz;
import p001a.C0064Am;

import java.util.UUID;

/* renamed from: a.aQa  reason: case insensitive filesystem */
public class C5612aQa extends C3805vD {
    @C0064Am(aul = "c39e2bc3b195f5fa9db3f7a6fbd288e5", aum = 4)

    /* renamed from: bK */
    public UUID f3636bK;
    @C0064Am(aul = "469c7c51b6f78f9d0995cfcb88d8f35f", aum = 1)
    public C1556Wo<Station, ListContainer<BuyOrder>> bvA;
    @C0064Am(aul = "27ba290036d4413ae0582424e0fd588e", aum = 2)
    public C1556Wo<Station, ListContainer<SellOrder>> bvB;
    @C0064Am(aul = "469c7c51b6f78f9d0995cfcb88d8f35f", aum = -2)
    public long bvH;
    @C0064Am(aul = "27ba290036d4413ae0582424e0fd588e", aum = -2)
    public long bvI;
    @C0064Am(aul = "469c7c51b6f78f9d0995cfcb88d8f35f", aum = -1)
    public byte bvO;
    @C0064Am(aul = "27ba290036d4413ae0582424e0fd588e", aum = -1)
    public byte bvP;
    @C0064Am(aul = "2e5af13e3531dab8d4d59793e469817f", aum = 0)
    public C3438ra<ItemType> dNI;
    @C0064Am(aul = "5ab4055974bea91c599e5dbdabaf76a8", aum = 3)
    public ConsignmentFeeManager dNM;
    @C0064Am(aul = "2e5af13e3531dab8d4d59793e469817f", aum = -2)
    public long iDA;
    @C0064Am(aul = "5ab4055974bea91c599e5dbdabaf76a8", aum = -2)
    public long iDB;
    @C0064Am(aul = "2e5af13e3531dab8d4d59793e469817f", aum = -1)
    public byte iDC;
    @C0064Am(aul = "5ab4055974bea91c599e5dbdabaf76a8", aum = -1)
    public byte iDD;
    @C0064Am(aul = "c39e2bc3b195f5fa9db3f7a6fbd288e5", aum = -2)

    /* renamed from: oL */
    public long f3637oL;
    @C0064Am(aul = "c39e2bc3b195f5fa9db3f7a6fbd288e5", aum = -1)

    /* renamed from: oS */
    public byte f3638oS;

    public C5612aQa() {
    }

    public C5612aQa(C1616Xf xf) {
        super(xf);
    }

    /* renamed from: c */
    public C5663aRz[] mo309c() {
        return Market._m_fields;
    }

    /* renamed from: d */
    public void mo310d() {
        super.mo310d();
        this.dNI = null;
        this.bvA = null;
        this.bvB = null;
        this.dNM = null;
        this.f3636bK = null;
        this.iDA = 0;
        this.bvH = 0;
        this.bvI = 0;
        this.iDB = 0;
        this.f3637oL = 0;
        this.iDC = 0;
        this.bvO = 0;
        this.bvP = 0;
        this.iDD = 0;
        this.f3638oS = 0;
    }
}
