package logic.data.mbean;

import game.network.message.serializable.C3438ra;
import game.script.crafting.CraftingRecipe;
import game.script.item.BlueprintType;
import game.script.item.IngredientsCategory;
import logic.baa.C1616Xf;
import logic.res.code.C5663aRz;
import p001a.C0064Am;

/* renamed from: a.aei  reason: case insensitive filesystem */
public class C6010aei extends C7034azv {
    @C0064Am(aul = "0ea9322091fb222d5e9a756212fad285", aum = 0)
    public C3438ra<IngredientsCategory> dBN;
    @C0064Am(aul = "f779b755fe972322f0bb24c3c46d3c7a", aum = 1)
    public CraftingRecipe dBO;
    @C0064Am(aul = "d8b9fa552edf75c60407c45e21c429aa", aum = 2)
    public long dBP;
    @C0064Am(aul = "0ea9322091fb222d5e9a756212fad285", aum = -2)
    public long dBQ;
    @C0064Am(aul = "f779b755fe972322f0bb24c3c46d3c7a", aum = -2)
    public long dBR;
    @C0064Am(aul = "d8b9fa552edf75c60407c45e21c429aa", aum = -2)
    public long dBS;
    @C0064Am(aul = "0ea9322091fb222d5e9a756212fad285", aum = -1)
    public byte dBT;
    @C0064Am(aul = "f779b755fe972322f0bb24c3c46d3c7a", aum = -1)
    public byte dBU;
    @C0064Am(aul = "d8b9fa552edf75c60407c45e21c429aa", aum = -1)
    public byte dBV;

    public C6010aei() {
    }

    public C6010aei(C1616Xf xf) {
        super(xf);
    }

    /* renamed from: c */
    public C5663aRz[] mo309c() {
        return BlueprintType._m_fields;
    }

    /* renamed from: d */
    public void mo310d() {
        super.mo310d();
        this.dBN = null;
        this.dBO = null;
        this.dBP = 0;
        this.dBQ = 0;
        this.dBR = 0;
        this.dBS = 0;
        this.dBT = 0;
        this.dBU = 0;
        this.dBV = 0;
    }
}
