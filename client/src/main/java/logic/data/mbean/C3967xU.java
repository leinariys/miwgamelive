package logic.data.mbean;

import game.network.message.serializable.C1556Wo;
import game.script.damage.DamageType;
import game.script.item.WeaponType;
import game.script.resource.Asset;
import logic.baa.C1616Xf;
import logic.res.code.C5663aRz;
import p001a.C0064Am;

/* renamed from: a.xU */
public class C3967xU extends C5364aGm {
    @C0064Am(aul = "ec7c9f1ac22ce45968d64c868a62071d", aum = -2)
    public long aIR;
    @C0064Am(aul = "ec7c9f1ac22ce45968d64c868a62071d", aum = -1)
    public byte aJc;
    @C0064Am(aul = "ffbb079176806b263834c27c54a95030", aum = 3)
    public boolean bEj;
    @C0064Am(aul = "8e2844cc72025f55abd9b215cd43ea60", aum = 9)
    public Asset bIA;
    @C0064Am(aul = "6ac911c8b43db88be1727e540349898d", aum = 10)
    public Asset bIB;
    @C0064Am(aul = "d81f212b089c7b8ac14762b1f4587fa9", aum = 11)
    public Asset bIC;
    @C0064Am(aul = "209e0e46cfbe1a01239785dc9908ee46", aum = 12)
    public Asset bID;
    @C0064Am(aul = "cccee2ab5e319f1f992d6f775a1f0a41", aum = -2)
    public long bIE;
    @C0064Am(aul = "96d742392c453ec60cce2ded7d01cff8", aum = -2)
    public long bIF;
    @C0064Am(aul = "ffbb079176806b263834c27c54a95030", aum = -2)
    public long bIG;
    @C0064Am(aul = "bb756382bfd39997268aaf69f4df98e4", aum = -2)
    public long bIH;
    @C0064Am(aul = "cfc88d17fa9f7fe54e6b2c45da5b8763", aum = -2)
    public long bII;
    @C0064Am(aul = "13d5837e1fadef8c2806b67e14cd5046", aum = -2)
    public long bIJ;
    @C0064Am(aul = "4403966a748eced21ae657296d6c8a1d", aum = -2)
    public long bIK;
    @C0064Am(aul = "ef97165a79b65a84c0704955d4104db3", aum = -2)
    public long bIL;
    @C0064Am(aul = "8e2844cc72025f55abd9b215cd43ea60", aum = -2)
    public long bIM;
    @C0064Am(aul = "6ac911c8b43db88be1727e540349898d", aum = -2)
    public long bIN;
    @C0064Am(aul = "d81f212b089c7b8ac14762b1f4587fa9", aum = -2)
    public long bIO;
    @C0064Am(aul = "209e0e46cfbe1a01239785dc9908ee46", aum = -2)
    public long bIP;
    @C0064Am(aul = "cccee2ab5e319f1f992d6f775a1f0a41", aum = -1)
    public byte bIQ;
    @C0064Am(aul = "96d742392c453ec60cce2ded7d01cff8", aum = -1)
    public byte bIR;
    @C0064Am(aul = "ffbb079176806b263834c27c54a95030", aum = -1)
    public byte bIS;
    @C0064Am(aul = "bb756382bfd39997268aaf69f4df98e4", aum = -1)
    public byte bIT;
    @C0064Am(aul = "cfc88d17fa9f7fe54e6b2c45da5b8763", aum = -1)
    public byte bIU;
    @C0064Am(aul = "13d5837e1fadef8c2806b67e14cd5046", aum = -1)
    public byte bIV;
    @C0064Am(aul = "4403966a748eced21ae657296d6c8a1d", aum = -1)
    public byte bIW;
    @C0064Am(aul = "ef97165a79b65a84c0704955d4104db3", aum = -1)
    public byte bIX;
    @C0064Am(aul = "8e2844cc72025f55abd9b215cd43ea60", aum = -1)
    public byte bIY;
    @C0064Am(aul = "6ac911c8b43db88be1727e540349898d", aum = -1)
    public byte bIZ;
    @C0064Am(aul = "cccee2ab5e319f1f992d6f775a1f0a41", aum = 0)
    public boolean bIt;
    @C0064Am(aul = "96d742392c453ec60cce2ded7d01cff8", aum = 1)
    public Asset bIu;
    @C0064Am(aul = "0c8c747a62c1e716b0d33b84c6edf3a3", aum = 2)
    public C1556Wo<DamageType, Float> bIv;
    @C0064Am(aul = "bb756382bfd39997268aaf69f4df98e4", aum = 4)
    public float bIw;
    @C0064Am(aul = "cfc88d17fa9f7fe54e6b2c45da5b8763", aum = 5)
    public Asset bIx;
    @C0064Am(aul = "4403966a748eced21ae657296d6c8a1d", aum = 7)
    public float bIy;
    @C0064Am(aul = "ef97165a79b65a84c0704955d4104db3", aum = 8)
    public Asset bIz;
    @C0064Am(aul = "d81f212b089c7b8ac14762b1f4587fa9", aum = -1)
    public byte bJa;
    @C0064Am(aul = "209e0e46cfbe1a01239785dc9908ee46", aum = -1)
    public byte bJb;
    @C0064Am(aul = "0c8c747a62c1e716b0d33b84c6edf3a3", aum = -2)
    public long beq;
    @C0064Am(aul = "0c8c747a62c1e716b0d33b84c6edf3a3", aum = -1)
    public byte bes;
    @C0064Am(aul = "13d5837e1fadef8c2806b67e14cd5046", aum = 6)
    public float range;
    @C0064Am(aul = "ec7c9f1ac22ce45968d64c868a62071d", aum = 13)
    public float speed;

    public C3967xU() {
    }

    public C3967xU(C1616Xf xf) {
        super(xf);
    }

    /* renamed from: c */
    public C5663aRz[] mo309c() {
        return WeaponType._m_fields;
    }

    /* renamed from: d */
    public void mo310d() {
        super.mo310d();
        this.bIt = false;
        this.bIu = null;
        this.bIv = null;
        this.bEj = false;
        this.bIw = 0.0f;
        this.bIx = null;
        this.range = 0.0f;
        this.bIy = 0.0f;
        this.bIz = null;
        this.bIA = null;
        this.bIB = null;
        this.bIC = null;
        this.bID = null;
        this.speed = 0.0f;
        this.bIE = 0;
        this.bIF = 0;
        this.beq = 0;
        this.bIG = 0;
        this.bIH = 0;
        this.bII = 0;
        this.bIJ = 0;
        this.bIK = 0;
        this.bIL = 0;
        this.bIM = 0;
        this.bIN = 0;
        this.bIO = 0;
        this.bIP = 0;
        this.aIR = 0;
        this.bIQ = 0;
        this.bIR = 0;
        this.bes = 0;
        this.bIS = 0;
        this.bIT = 0;
        this.bIU = 0;
        this.bIV = 0;
        this.bIW = 0;
        this.bIX = 0;
        this.bIY = 0;
        this.bIZ = 0;
        this.bJa = 0;
        this.bJb = 0;
        this.aJc = 0;
    }
}
