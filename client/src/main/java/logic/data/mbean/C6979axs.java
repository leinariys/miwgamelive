package logic.data.mbean;

import game.network.message.serializable.C3438ra;
import game.script.pda.TaikopediaEntry;
import game.script.spacezone.SpaceZone;
import game.script.spacezone.SpaceZoneEvent;
import logic.baa.C1616Xf;
import logic.res.code.C5663aRz;
import p001a.C0064Am;
import taikodom.infra.script.I18NString;

import java.util.UUID;

/* renamed from: a.axs  reason: case insensitive filesystem */
public class C6979axs extends C6151ahT {
    @C0064Am(aul = "81f88f325b19bec1aa08d504793c8700", aum = -2)

    /* renamed from: DA */
    public long f5589DA;
    @C0064Am(aul = "81f88f325b19bec1aa08d504793c8700", aum = -1)

    /* renamed from: DE */
    public byte f5590DE;
    @C0064Am(aul = "33897cf41f4553f09c3da65005b44972", aum = 2)
    public C3438ra<SpaceZoneEvent> awf;
    @C0064Am(aul = "8c5602f7876f853f6904447c31b9e984", aum = 4)
    public boolean awi;
    @C0064Am(aul = "1d65826bf65c0802439fa19b780b0348", aum = 5)
    public I18NString awk;
    @C0064Am(aul = "3aa9dc1bd66cb452e818285eb75167d9", aum = 6)
    public I18NString awm;
    @C0064Am(aul = "9a38d9ac8e7f8d085b6bbc865ead7d89", aum = 7)

    /* renamed from: bK */
    public UUID f5591bK;
    @C0064Am(aul = "33897cf41f4553f09c3da65005b44972", aum = -2)
    public long gPS;
    @C0064Am(aul = "8c5602f7876f853f6904447c31b9e984", aum = -2)
    public long gPT;
    @C0064Am(aul = "1d65826bf65c0802439fa19b780b0348", aum = -2)
    public long gPU;
    @C0064Am(aul = "3aa9dc1bd66cb452e818285eb75167d9", aum = -2)
    public long gPV;
    @C0064Am(aul = "33897cf41f4553f09c3da65005b44972", aum = -1)
    public byte gPW;
    @C0064Am(aul = "8c5602f7876f853f6904447c31b9e984", aum = -1)
    public byte gPX;
    @C0064Am(aul = "1d65826bf65c0802439fa19b780b0348", aum = -1)
    public byte gPY;
    @C0064Am(aul = "3aa9dc1bd66cb452e818285eb75167d9", aum = -1)
    public byte gPZ;
    @C0064Am(aul = "2f3bbba9cde47eaa1999b4d22f6b3323", aum = 1)
    public String handle;
    @C0064Am(aul = "ab5daea4a060a11e6902d16744025269", aum = 0)

    /* renamed from: jT */
    public C3438ra<TaikopediaEntry> f5592jT;
    @C0064Am(aul = "ab5daea4a060a11e6902d16744025269", aum = -2)

    /* renamed from: jY */
    public long f5593jY;
    @C0064Am(aul = "ab5daea4a060a11e6902d16744025269", aum = -1)

    /* renamed from: kd */
    public byte f5594kd;
    @C0064Am(aul = "9a38d9ac8e7f8d085b6bbc865ead7d89", aum = -2)

    /* renamed from: oL */
    public long f5595oL;
    @C0064Am(aul = "9a38d9ac8e7f8d085b6bbc865ead7d89", aum = -1)

    /* renamed from: oS */
    public byte f5596oS;
    @C0064Am(aul = "2f3bbba9cde47eaa1999b4d22f6b3323", aum = -2)

    /* renamed from: ok */
    public long f5597ok;
    @C0064Am(aul = "2f3bbba9cde47eaa1999b4d22f6b3323", aum = -1)

    /* renamed from: on */
    public byte f5598on;
    @C0064Am(aul = "81f88f325b19bec1aa08d504793c8700", aum = 3)
    public float radius;

    public C6979axs() {
    }

    public C6979axs(C1616Xf xf) {
        super(xf);
    }

    /* renamed from: c */
    public C5663aRz[] mo309c() {
        return SpaceZone._m_fields;
    }

    /* renamed from: d */
    public void mo310d() {
        super.mo310d();
        this.f5592jT = null;
        this.handle = null;
        this.awf = null;
        this.radius = 0.0f;
        this.awi = false;
        this.awk = null;
        this.awm = null;
        this.f5591bK = null;
        this.f5593jY = 0;
        this.f5597ok = 0;
        this.gPS = 0;
        this.f5589DA = 0;
        this.gPT = 0;
        this.gPU = 0;
        this.gPV = 0;
        this.f5595oL = 0;
        this.f5594kd = 0;
        this.f5598on = 0;
        this.gPW = 0;
        this.f5590DE = 0;
        this.gPX = 0;
        this.gPY = 0;
        this.gPZ = 0;
        this.f5596oS = 0;
    }
}
