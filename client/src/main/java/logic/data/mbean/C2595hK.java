package logic.data.mbean;

import game.script.ship.speedBoost.SpeedBoost;
import logic.baa.C1616Xf;
import logic.res.code.C5663aRz;
import p001a.C0064Am;

/* renamed from: a.hK */
public class C2595hK extends C1601XR {
    @C0064Am(aul = "2d18fef72437df79b036cf299af2c1f4", aum = -2)

    /* renamed from: dl */
    public long f7860dl;
    @C0064Am(aul = "2d18fef72437df79b036cf299af2c1f4", aum = -1)

    /* renamed from: ds */
    public byte f7861ds;
    @C0064Am(aul = "2d18fef72437df79b036cf299af2c1f4", aum = 0)

    /* renamed from: yb */
    public SpeedBoost f7862yb;

    public C2595hK() {
    }

    public C2595hK(C1616Xf xf) {
        super(xf);
    }

    /* renamed from: c */
    public C5663aRz[] mo309c() {
        return SpeedBoost.BaseSpeedBoostAdapter._m_fields;
    }

    /* renamed from: d */
    public void mo310d() {
        super.mo310d();
        this.f7862yb = null;
        this.f7860dl = 0;
        this.f7861ds = 0;
    }
}
