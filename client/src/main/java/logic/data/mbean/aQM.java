package logic.data.mbean;

import game.network.message.serializable.C1556Wo;
import game.network.message.serializable.C3438ra;
import game.script.damage.DamageType;
import game.script.item.ItemTypeCategory;
import game.script.item.Weapon;
import game.script.resource.Asset;
import game.script.ship.Ship;
import game.script.ship.strike.Strike;
import game.script.ship.strike.StrikeAdapter;
import game.script.util.AdapterLikedList;
import logic.baa.C1616Xf;
import logic.res.code.C5663aRz;
import p001a.C0064Am;
import p001a.C3892wO;
import p001a.C6809auB;

/* renamed from: a.aQM */
public class aQM extends C3805vD {
    @C0064Am(aul = "920a2662b013ebfaa849780fc31cc68a", aum = -2)

    /* renamed from: YA */
    public long f3595YA;
    @C0064Am(aul = "8f414302657beb7392d41f8997c4276f", aum = -2)

    /* renamed from: YB */
    public long f3596YB;
    @C0064Am(aul = "920a2662b013ebfaa849780fc31cc68a", aum = -1)

    /* renamed from: YG */
    public byte f3597YG;
    @C0064Am(aul = "8f414302657beb7392d41f8997c4276f", aum = -1)

    /* renamed from: YH */
    public byte f3598YH;
    @C0064Am(aul = "d890572a0d690e09c66440decce19a4c", aum = 2)
    public float aAc;
    @C0064Am(aul = "a0e118f4e7653af684d062209b02f77e", aum = 5)
    public Asset aAd;
    @C0064Am(aul = "fd29ace52b0921d5a3ddcc91f6a4544c", aum = 6)
    public Asset aAf;
    @C0064Am(aul = "1fa35492d6965c0c59c699018ae5e75a", aum = 9)
    public Ship aOO;
    @C0064Am(aul = "0bc9a7fbf92ecbd4c58b8d2d4e9b7238", aum = 7)
    public AdapterLikedList<StrikeAdapter> aOw;
    @C0064Am(aul = "0bc9a7fbf92ecbd4c58b8d2d4e9b7238", aum = -2)
    public long aUE;
    @C0064Am(aul = "0bc9a7fbf92ecbd4c58b8d2d4e9b7238", aum = -1)
    public byte aUH;
    @C0064Am(aul = "66408fd0dd394619a7c0cfd1f0404a0b", aum = 8)
    public C6809auB.C1996a aZI;
    @C0064Am(aul = "ea073621be37a10db45389941486509d", aum = 0)
    public C1556Wo<DamageType, C3892wO> atO;
    @C0064Am(aul = "8f414302657beb7392d41f8997c4276f", aum = 3)
    public float bbL;
    @C0064Am(aul = "920a2662b013ebfaa849780fc31cc68a", aum = 1)
    public float bbM;
    @C0064Am(aul = "e0852059eac2da97a229fb094f91dba0", aum = 4)
    public C3438ra<ItemTypeCategory> bjJ;
    @C0064Am(aul = "ff946a5ee065d3cb6991916c9fe9ba5b", aum = 10)
    public C1556Wo<Weapon, Strike.StrikeDamageAdapter> bjL;
    @C0064Am(aul = "c73523937749c48d1545bcbf0340882f", aum = 11)
    public C1556Wo<DamageType, C3892wO> bjN;
    @C0064Am(aul = "ac0191e23cb237bbfe0b81121c29b2a3", aum = 12)
    public float bjP;
    @C0064Am(aul = "c338f0526df1b6a9a515b43c9d77a45d", aum = 13)
    public float bjR;
    @C0064Am(aul = "3ffc7c22d3f5fe159abdb3fcda18390f", aum = 14)
    public float bjT;
    @C0064Am(aul = "d890572a0d690e09c66440decce19a4c", aum = -2)
    public long cAA;
    @C0064Am(aul = "a0e118f4e7653af684d062209b02f77e", aum = -2)
    public long cAB;
    @C0064Am(aul = "fd29ace52b0921d5a3ddcc91f6a4544c", aum = -2)
    public long cAC;
    @C0064Am(aul = "c338f0526df1b6a9a515b43c9d77a45d", aum = -2)
    public long cAJ;
    @C0064Am(aul = "d890572a0d690e09c66440decce19a4c", aum = -1)
    public byte cAO;
    @C0064Am(aul = "a0e118f4e7653af684d062209b02f77e", aum = -1)
    public byte cAP;
    @C0064Am(aul = "fd29ace52b0921d5a3ddcc91f6a4544c", aum = -1)
    public byte cAQ;
    @C0064Am(aul = "c338f0526df1b6a9a515b43c9d77a45d", aum = -1)
    public byte cAX;
    @C0064Am(aul = "1fa35492d6965c0c59c699018ae5e75a", aum = -2)
    public long cSU;
    @C0064Am(aul = "1fa35492d6965c0c59c699018ae5e75a", aum = -1)
    public byte cSW;
    @C0064Am(aul = "e0852059eac2da97a229fb094f91dba0", aum = -2)
    public long iFo;
    @C0064Am(aul = "ff946a5ee065d3cb6991916c9fe9ba5b", aum = -2)
    public long iFp;
    @C0064Am(aul = "c73523937749c48d1545bcbf0340882f", aum = -2)
    public long iFq;
    @C0064Am(aul = "ac0191e23cb237bbfe0b81121c29b2a3", aum = -2)
    public long iFr;
    @C0064Am(aul = "3ffc7c22d3f5fe159abdb3fcda18390f", aum = -2)
    public long iFs;
    @C0064Am(aul = "e0852059eac2da97a229fb094f91dba0", aum = -1)
    public byte iFt;
    @C0064Am(aul = "ff946a5ee065d3cb6991916c9fe9ba5b", aum = -1)
    public byte iFu;
    @C0064Am(aul = "c73523937749c48d1545bcbf0340882f", aum = -1)
    public byte iFv;
    @C0064Am(aul = "ac0191e23cb237bbfe0b81121c29b2a3", aum = -1)
    public byte iFw;
    @C0064Am(aul = "3ffc7c22d3f5fe159abdb3fcda18390f", aum = -1)
    public byte iFx;
    @C0064Am(aul = "ea073621be37a10db45389941486509d", aum = -2)
    public long ide;
    @C0064Am(aul = "ea073621be37a10db45389941486509d", aum = -1)
    public byte idf;
    @C0064Am(aul = "66408fd0dd394619a7c0cfd1f0404a0b", aum = -2)

    /* renamed from: ol */
    public long f3599ol;
    @C0064Am(aul = "66408fd0dd394619a7c0cfd1f0404a0b", aum = -1)

    /* renamed from: oo */
    public byte f3600oo;

    public aQM() {
    }

    public aQM(C1616Xf xf) {
        super(xf);
    }

    /* renamed from: c */
    public C5663aRz[] mo309c() {
        return Strike._m_fields;
    }

    /* renamed from: d */
    public void mo310d() {
        super.mo310d();
        this.atO = null;
        this.bbM = 0.0f;
        this.aAc = 0.0f;
        this.bbL = 0.0f;
        this.bjJ = null;
        this.aAd = null;
        this.aAf = null;
        this.aOw = null;
        this.aZI = null;
        this.aOO = null;
        this.bjL = null;
        this.bjN = null;
        this.bjP = 0.0f;
        this.bjR = 0.0f;
        this.bjT = 0.0f;
        this.ide = 0;
        this.f3595YA = 0;
        this.cAA = 0;
        this.f3596YB = 0;
        this.iFo = 0;
        this.cAB = 0;
        this.cAC = 0;
        this.aUE = 0;
        this.f3599ol = 0;
        this.cSU = 0;
        this.iFp = 0;
        this.iFq = 0;
        this.iFr = 0;
        this.cAJ = 0;
        this.iFs = 0;
        this.idf = 0;
        this.f3597YG = 0;
        this.cAO = 0;
        this.f3598YH = 0;
        this.iFt = 0;
        this.cAP = 0;
        this.cAQ = 0;
        this.aUH = 0;
        this.f3600oo = 0;
        this.cSW = 0;
        this.iFu = 0;
        this.iFv = 0;
        this.iFw = 0;
        this.cAX = 0;
        this.iFx = 0;
    }
}
