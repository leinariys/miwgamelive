package logic.data.mbean;

import game.script.nls.NLSSpeechActions;
import logic.baa.C1616Xf;
import logic.res.code.C5663aRz;
import p001a.C0064Am;
import taikodom.infra.script.I18NString;

/* renamed from: a.eR */
public class C2358eR extends C2484fi {
    @C0064Am(aul = "bbbf899b47130a30ae47ae4d6a122494", aum = -1)

    /* renamed from: EA */
    public byte f6748EA;
    @C0064Am(aul = "23f38e3f7ddb24d4521ec69d48842a08", aum = -1)

    /* renamed from: EB */
    public byte f6749EB;
    @C0064Am(aul = "c4ccfd14ac1cdc6ba1c05805e0bcc787", aum = -1)

    /* renamed from: EC */
    public byte f6750EC;
    @C0064Am(aul = "499733e476d9365d8e0944c7cc52e4b3", aum = -1)

    /* renamed from: ED */
    public byte f6751ED;
    @C0064Am(aul = "4cd1803d4b0f358424a741125ce6e9b5", aum = -1)

    /* renamed from: EE */
    public byte f6752EE;
    @C0064Am(aul = "9f68d30d1aab29c7dbd349573521aa6c", aum = -1)

    /* renamed from: EF */
    public byte f6753EF;
    @C0064Am(aul = "530a66f154cfed2f470f1efc4e2cca55", aum = -1)

    /* renamed from: EG */
    public byte f6754EG;
    @C0064Am(aul = "c7091d22900151feb881fd1bacc65ccc", aum = -1)

    /* renamed from: EH */
    public byte f6755EH;
    @C0064Am(aul = "1d802eaaff6d04cc6adf8115f7cd15a4", aum = -1)

    /* renamed from: EI */
    public byte f6756EI;
    @C0064Am(aul = "261943cb3b23964581335be725072131", aum = -1)

    /* renamed from: EJ */
    public byte f6757EJ;
    @C0064Am(aul = "18738f8ff9ca1ab430c2697d5810de98", aum = 0)

    /* renamed from: Ea */
    public I18NString f6758Ea;
    @C0064Am(aul = "0e2a9f469f45978b09b4efcd5073b75e", aum = 1)

    /* renamed from: Eb */
    public I18NString f6759Eb;
    @C0064Am(aul = "bbbf899b47130a30ae47ae4d6a122494", aum = 2)

    /* renamed from: Ec */
    public I18NString f6760Ec;
    @C0064Am(aul = "23f38e3f7ddb24d4521ec69d48842a08", aum = 3)

    /* renamed from: Ed */
    public I18NString f6761Ed;
    @C0064Am(aul = "c4ccfd14ac1cdc6ba1c05805e0bcc787", aum = 4)

    /* renamed from: Ee */
    public I18NString f6762Ee;
    @C0064Am(aul = "499733e476d9365d8e0944c7cc52e4b3", aum = 5)

    /* renamed from: Ef */
    public I18NString f6763Ef;
    @C0064Am(aul = "4cd1803d4b0f358424a741125ce6e9b5", aum = 6)

    /* renamed from: Eg */
    public I18NString f6764Eg;
    @C0064Am(aul = "9f68d30d1aab29c7dbd349573521aa6c", aum = 7)

    /* renamed from: Eh */
    public I18NString f6765Eh;
    @C0064Am(aul = "530a66f154cfed2f470f1efc4e2cca55", aum = 8)

    /* renamed from: Ei */
    public I18NString f6766Ei;
    @C0064Am(aul = "c7091d22900151feb881fd1bacc65ccc", aum = 9)

    /* renamed from: Ej */
    public I18NString f6767Ej;
    @C0064Am(aul = "1d802eaaff6d04cc6adf8115f7cd15a4", aum = 10)

    /* renamed from: Ek */
    public I18NString f6768Ek;
    @C0064Am(aul = "261943cb3b23964581335be725072131", aum = 11)

    /* renamed from: El */
    public I18NString f6769El;
    @C0064Am(aul = "18738f8ff9ca1ab430c2697d5810de98", aum = -2)

    /* renamed from: Em */
    public long f6770Em;
    @C0064Am(aul = "0e2a9f469f45978b09b4efcd5073b75e", aum = -2)

    /* renamed from: En */
    public long f6771En;
    @C0064Am(aul = "bbbf899b47130a30ae47ae4d6a122494", aum = -2)

    /* renamed from: Eo */
    public long f6772Eo;
    @C0064Am(aul = "23f38e3f7ddb24d4521ec69d48842a08", aum = -2)

    /* renamed from: Ep */
    public long f6773Ep;
    @C0064Am(aul = "c4ccfd14ac1cdc6ba1c05805e0bcc787", aum = -2)

    /* renamed from: Eq */
    public long f6774Eq;
    @C0064Am(aul = "499733e476d9365d8e0944c7cc52e4b3", aum = -2)

    /* renamed from: Er */
    public long f6775Er;
    @C0064Am(aul = "4cd1803d4b0f358424a741125ce6e9b5", aum = -2)

    /* renamed from: Es */
    public long f6776Es;
    @C0064Am(aul = "9f68d30d1aab29c7dbd349573521aa6c", aum = -2)

    /* renamed from: Et */
    public long f6777Et;
    @C0064Am(aul = "530a66f154cfed2f470f1efc4e2cca55", aum = -2)

    /* renamed from: Eu */
    public long f6778Eu;
    @C0064Am(aul = "c7091d22900151feb881fd1bacc65ccc", aum = -2)

    /* renamed from: Ev */
    public long f6779Ev;
    @C0064Am(aul = "1d802eaaff6d04cc6adf8115f7cd15a4", aum = -2)

    /* renamed from: Ew */
    public long f6780Ew;
    @C0064Am(aul = "261943cb3b23964581335be725072131", aum = -2)

    /* renamed from: Ex */
    public long f6781Ex;
    @C0064Am(aul = "18738f8ff9ca1ab430c2697d5810de98", aum = -1)

    /* renamed from: Ey */
    public byte f6782Ey;
    @C0064Am(aul = "0e2a9f469f45978b09b4efcd5073b75e", aum = -1)

    /* renamed from: Ez */
    public byte f6783Ez;

    public C2358eR() {
    }

    public C2358eR(C1616Xf xf) {
        super(xf);
    }

    /* renamed from: c */
    public C5663aRz[] mo309c() {
        return NLSSpeechActions._m_fields;
    }

    /* renamed from: d */
    public void mo310d() {
        super.mo310d();
        this.f6758Ea = null;
        this.f6759Eb = null;
        this.f6760Ec = null;
        this.f6761Ed = null;
        this.f6762Ee = null;
        this.f6763Ef = null;
        this.f6764Eg = null;
        this.f6765Eh = null;
        this.f6766Ei = null;
        this.f6767Ej = null;
        this.f6768Ek = null;
        this.f6769El = null;
        this.f6770Em = 0;
        this.f6771En = 0;
        this.f6772Eo = 0;
        this.f6773Ep = 0;
        this.f6774Eq = 0;
        this.f6775Er = 0;
        this.f6776Es = 0;
        this.f6777Et = 0;
        this.f6778Eu = 0;
        this.f6779Ev = 0;
        this.f6780Ew = 0;
        this.f6781Ex = 0;
        this.f6782Ey = 0;
        this.f6783Ez = 0;
        this.f6748EA = 0;
        this.f6749EB = 0;
        this.f6750EC = 0;
        this.f6751ED = 0;
        this.f6752EE = 0;
        this.f6753EF = 0;
        this.f6754EG = 0;
        this.f6755EH = 0;
        this.f6756EI = 0;
        this.f6757EJ = 0;
    }
}
