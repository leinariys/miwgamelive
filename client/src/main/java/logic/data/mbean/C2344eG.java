package logic.data.mbean;

import game.geometry.Vec3d;
import game.network.message.serializable.C3438ra;
import game.script.missiontemplate.PositionDat;
import game.script.missiontemplate.WaypointHelper;
import logic.baa.C1616Xf;
import logic.res.code.C5663aRz;
import p001a.C0064Am;

/* renamed from: a.eG */
public class C2344eG extends C5292aDs {
    @C0064Am(aul = "e41b5c2194d359625f53d245ea36c20e", aum = -2)

    /* renamed from: DA */
    public long f6706DA;
    @C0064Am(aul = "6a187c8620a777001a02d1a080acab9e", aum = -2)

    /* renamed from: DB */
    public long f6707DB;
    @C0064Am(aul = "f9308d95aa5aa47f09ff978770db5397", aum = -2)

    /* renamed from: DC */
    public long f6708DC;
    @C0064Am(aul = "ad6a9e359b9f4d5361a1c481b95786fb", aum = -2)

    /* renamed from: DD */
    public long f6709DD;
    @C0064Am(aul = "e41b5c2194d359625f53d245ea36c20e", aum = -1)

    /* renamed from: DE */
    public byte f6710DE;
    @C0064Am(aul = "6a187c8620a777001a02d1a080acab9e", aum = -1)

    /* renamed from: DF */
    public byte f6711DF;
    @C0064Am(aul = "f9308d95aa5aa47f09ff978770db5397", aum = -1)

    /* renamed from: DG */
    public byte f6712DG;
    @C0064Am(aul = "ad6a9e359b9f4d5361a1c481b95786fb", aum = -1)

    /* renamed from: DH */
    public byte f6713DH;
    @C0064Am(aul = "e41b5c2194d359625f53d245ea36c20e", aum = 1)

    /* renamed from: Dw */
    public long f6714Dw;
    @C0064Am(aul = "6a187c8620a777001a02d1a080acab9e", aum = 2)

    /* renamed from: Dx */
    public long f6715Dx;
    @C0064Am(aul = "f9308d95aa5aa47f09ff978770db5397", aum = 3)

    /* renamed from: Dy */
    public C3438ra<PositionDat> f6716Dy;
    @C0064Am(aul = "ad6a9e359b9f4d5361a1c481b95786fb", aum = 4)

    /* renamed from: Dz */
    public boolean f6717Dz;
    @C0064Am(aul = "a768d4a2c172cd6f92814280caca9ef4", aum = 0)
    public Vec3d position;
    @C0064Am(aul = "a768d4a2c172cd6f92814280caca9ef4", aum = -2)

    /* renamed from: rX */
    public long f6718rX;
    @C0064Am(aul = "a768d4a2c172cd6f92814280caca9ef4", aum = -1)

    /* renamed from: sn */
    public byte f6719sn;

    public C2344eG() {
    }

    public C2344eG(C1616Xf xf) {
        super(xf);
    }

    /* renamed from: c */
    public C5663aRz[] mo309c() {
        return WaypointHelper._m_fields;
    }

    /* renamed from: d */
    public void mo310d() {
        super.mo310d();
        this.position = null;
        this.f6714Dw = 0;
        this.f6715Dx = 0;
        this.f6716Dy = null;
        this.f6717Dz = false;
        this.f6718rX = 0;
        this.f6706DA = 0;
        this.f6707DB = 0;
        this.f6708DC = 0;
        this.f6709DD = 0;
        this.f6719sn = 0;
        this.f6710DE = 0;
        this.f6711DF = 0;
        this.f6712DG = 0;
        this.f6713DH = 0;
    }
}
