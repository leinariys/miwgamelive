package logic.data.mbean;

import game.network.message.serializable.C3438ra;
import game.script.progression.CharacterProgression;
import game.script.progression.ProgressionAbility;
import game.script.progression.ProgressionCell;
import game.script.progression.ProgressionCellType;
import game.script.resource.Asset;
import logic.baa.C1616Xf;
import logic.res.code.C5663aRz;
import p001a.C0064Am;

/* renamed from: a.aB */
public class C1767aB extends C3805vD {
    @C0064Am(aul = "e237e1d9ab76f5c27edc5868a572ef4b", aum = 9)
    public boolean enabled;
    @C0064Am(aul = "3598deecefec2815773d6f89700088cf", aum = -2)

    /* renamed from: jA */
    public long f2368jA;
    @C0064Am(aul = "8461580b6a7aa21381231fd1f6307f2e", aum = -2)

    /* renamed from: jB */
    public long f2369jB;
    @C0064Am(aul = "e237e1d9ab76f5c27edc5868a572ef4b", aum = -2)

    /* renamed from: jC */
    public long f2370jC;
    @C0064Am(aul = "a5731e5c694dffa19cb6c2a61918157a", aum = -2)

    /* renamed from: jD */
    public long f2371jD;
    @C0064Am(aul = "509e9ba42cb1ddace8bc6d3a6aaf6dd9", aum = -2)

    /* renamed from: jE */
    public long f2372jE;
    @C0064Am(aul = "66657be20507af492985aea572471f48", aum = -1)

    /* renamed from: jF */
    public byte f2373jF;
    @C0064Am(aul = "b8af008df6e2b67dc5f188dc25189783", aum = -1)

    /* renamed from: jG */
    public byte f2374jG;
    @C0064Am(aul = "5e0aeef701a3deee9503d3c6c4b9c706", aum = -1)

    /* renamed from: jH */
    public byte f2375jH;
    @C0064Am(aul = "45f808e7a7bb47aa564619cab99198ed", aum = -1)

    /* renamed from: jI */
    public byte f2376jI;
    @C0064Am(aul = "437525d32be43c3dba383723af2a2f2d", aum = -1)

    /* renamed from: jJ */
    public byte f2377jJ;
    @C0064Am(aul = "0aa8ba4aeadeb76394dde6c6963151ef", aum = -1)

    /* renamed from: jK */
    public byte f2378jK;
    @C0064Am(aul = "25767d3d05d43210941f6852105fe5c8", aum = -1)

    /* renamed from: jL */
    public byte f2379jL;
    @C0064Am(aul = "3598deecefec2815773d6f89700088cf", aum = -1)

    /* renamed from: jM */
    public byte f2380jM;
    @C0064Am(aul = "8461580b6a7aa21381231fd1f6307f2e", aum = -1)

    /* renamed from: jN */
    public byte f2381jN;
    @C0064Am(aul = "e237e1d9ab76f5c27edc5868a572ef4b", aum = -1)

    /* renamed from: jO */
    public byte f2382jO;
    @C0064Am(aul = "a5731e5c694dffa19cb6c2a61918157a", aum = -1)

    /* renamed from: jP */
    public byte f2383jP;
    @C0064Am(aul = "509e9ba42cb1ddace8bc6d3a6aaf6dd9", aum = -1)

    /* renamed from: jQ */
    public byte f2384jQ;
    @C0064Am(aul = "66657be20507af492985aea572471f48", aum = 0)

    /* renamed from: ji */
    public C3438ra<ProgressionAbility> f2385ji;
    @C0064Am(aul = "b8af008df6e2b67dc5f188dc25189783", aum = 1)

    /* renamed from: jj */
    public ProgressionCellType f2386jj;
    @C0064Am(aul = "5e0aeef701a3deee9503d3c6c4b9c706", aum = 2)

    /* renamed from: jk */
    public int f2387jk;
    @C0064Am(aul = "45f808e7a7bb47aa564619cab99198ed", aum = 3)

    /* renamed from: jl */
    public int f2388jl;
    @C0064Am(aul = "437525d32be43c3dba383723af2a2f2d", aum = 4)

    /* renamed from: jm */
    public ProgressionAbility f2389jm;
    @C0064Am(aul = "0aa8ba4aeadeb76394dde6c6963151ef", aum = 5)

    /* renamed from: jn */
    public Asset f2390jn;
    @C0064Am(aul = "25767d3d05d43210941f6852105fe5c8", aum = 6)

    /* renamed from: jo */
    public ProgressionCell f2391jo;
    @C0064Am(aul = "3598deecefec2815773d6f89700088cf", aum = 7)

    /* renamed from: jp */
    public CharacterProgression f2392jp;
    @C0064Am(aul = "8461580b6a7aa21381231fd1f6307f2e", aum = 8)

    /* renamed from: jq */
    public boolean f2393jq;
    @C0064Am(aul = "a5731e5c694dffa19cb6c2a61918157a", aum = 10)

    /* renamed from: jr */
    public boolean f2394jr;
    @C0064Am(aul = "509e9ba42cb1ddace8bc6d3a6aaf6dd9", aum = 11)

    /* renamed from: js */
    public boolean f2395js;
    @C0064Am(aul = "66657be20507af492985aea572471f48", aum = -2)

    /* renamed from: jt */
    public long f2396jt;
    @C0064Am(aul = "b8af008df6e2b67dc5f188dc25189783", aum = -2)

    /* renamed from: ju */
    public long f2397ju;
    @C0064Am(aul = "5e0aeef701a3deee9503d3c6c4b9c706", aum = -2)

    /* renamed from: jv */
    public long f2398jv;
    @C0064Am(aul = "45f808e7a7bb47aa564619cab99198ed", aum = -2)

    /* renamed from: jw */
    public long f2399jw;
    @C0064Am(aul = "437525d32be43c3dba383723af2a2f2d", aum = -2)

    /* renamed from: jx */
    public long f2400jx;
    @C0064Am(aul = "0aa8ba4aeadeb76394dde6c6963151ef", aum = -2)

    /* renamed from: jy */
    public long f2401jy;
    @C0064Am(aul = "25767d3d05d43210941f6852105fe5c8", aum = -2)

    /* renamed from: jz */
    public long f2402jz;

    public C1767aB() {
    }

    public C1767aB(C1616Xf xf) {
        super(xf);
    }

    /* renamed from: c */
    public C5663aRz[] mo309c() {
        return ProgressionCell._m_fields;
    }

    /* renamed from: d */
    public void mo310d() {
        super.mo310d();
        this.f2385ji = null;
        this.f2386jj = null;
        this.f2387jk = 0;
        this.f2388jl = 0;
        this.f2389jm = null;
        this.f2390jn = null;
        this.f2391jo = null;
        this.f2392jp = null;
        this.f2393jq = false;
        this.enabled = false;
        this.f2394jr = false;
        this.f2395js = false;
        this.f2396jt = 0;
        this.f2397ju = 0;
        this.f2398jv = 0;
        this.f2399jw = 0;
        this.f2400jx = 0;
        this.f2401jy = 0;
        this.f2402jz = 0;
        this.f2368jA = 0;
        this.f2369jB = 0;
        this.f2370jC = 0;
        this.f2371jD = 0;
        this.f2372jE = 0;
        this.f2373jF = 0;
        this.f2374jG = 0;
        this.f2375jH = 0;
        this.f2376jI = 0;
        this.f2377jJ = 0;
        this.f2378jK = 0;
        this.f2379jL = 0;
        this.f2380jM = 0;
        this.f2381jN = 0;
        this.f2382jO = 0;
        this.f2383jP = 0;
        this.f2384jQ = 0;
    }
}
