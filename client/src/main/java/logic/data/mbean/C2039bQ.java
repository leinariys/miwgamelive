package logic.data.mbean;

import game.script.ship.OutpostDefaults;
import logic.baa.C1616Xf;
import logic.res.code.C5663aRz;
import p001a.C0064Am;

import java.util.UUID;

/* renamed from: a.bQ */
public class C2039bQ extends C3805vD {
    @C0064Am(aul = "aef66997f99ff6a94b09da0942d19163", aum = 2)

    /* renamed from: bK */
    public UUID f5783bK;
    @C0064Am(aul = "aef66997f99ff6a94b09da0942d19163", aum = -2)

    /* renamed from: oL */
    public long f5784oL;
    @C0064Am(aul = "aef66997f99ff6a94b09da0942d19163", aum = -1)

    /* renamed from: oS */
    public byte f5785oS;
    @C0064Am(aul = "f9f32cddf3d417b24319e25cf89df229", aum = 0)

    /* renamed from: ps */
    public int f5786ps;
    @C0064Am(aul = "6886861a3cfa527df93b116ae8b206d9", aum = 1)

    /* renamed from: pt */
    public int f5787pt;
    @C0064Am(aul = "f9f32cddf3d417b24319e25cf89df229", aum = -2)

    /* renamed from: pu */
    public long f5788pu;
    @C0064Am(aul = "6886861a3cfa527df93b116ae8b206d9", aum = -2)

    /* renamed from: pv */
    public long f5789pv;
    @C0064Am(aul = "f9f32cddf3d417b24319e25cf89df229", aum = -1)

    /* renamed from: px */
    public byte f5790px;
    @C0064Am(aul = "6886861a3cfa527df93b116ae8b206d9", aum = -1)

    /* renamed from: py */
    public byte f5791py;

    public C2039bQ() {
    }

    public C2039bQ(C1616Xf xf) {
        super(xf);
    }

    /* renamed from: c */
    public C5663aRz[] mo309c() {
        return OutpostDefaults._m_fields;
    }

    /* renamed from: d */
    public void mo310d() {
        super.mo310d();
        this.f5786ps = 0;
        this.f5787pt = 0;
        this.f5783bK = null;
        this.f5788pu = 0;
        this.f5789pv = 0;
        this.f5784oL = 0;
        this.f5790px = 0;
        this.f5791py = 0;
        this.f5785oS = 0;
    }
}
