package logic.data.mbean;

import game.geometry.Vec3d;
import game.script.ai.PlayerDirectionalController;
import logic.abb.C1218Ry;
import logic.baa.C1616Xf;
import logic.res.code.C5663aRz;
import p001a.C0064Am;

/* renamed from: a.xq */
public class C3993xq extends C2272dV {
    @C0064Am(aul = "b225cc5651b954b99627a76fd4144670", aum = -2)
    public long aIR;
    @C0064Am(aul = "b225cc5651b954b99627a76fd4144670", aum = -1)
    public byte aJc;
    @C0064Am(aul = "ebf0a87a2394a17ff4f2086f7dd7fd01", aum = 2)
    public C1218Ry azQ;
    @C0064Am(aul = "ff82bc7c67b2c3d931ce5187c3425afc", aum = -2)
    public long bDo;
    @C0064Am(aul = "ff82bc7c67b2c3d931ce5187c3425afc", aum = -1)
    public byte bDp;
    @C0064Am(aul = "ebf0a87a2394a17ff4f2086f7dd7fd01", aum = -2)
    public long bGd;
    @C0064Am(aul = "ebf0a87a2394a17ff4f2086f7dd7fd01", aum = -1)
    public byte bGe;
    @C0064Am(aul = "ff82bc7c67b2c3d931ce5187c3425afc", aum = 0)
    public Vec3d point;
    @C0064Am(aul = "b225cc5651b954b99627a76fd4144670", aum = 1)
    public float speed;

    public C3993xq() {
    }

    public C3993xq(C1616Xf xf) {
        super(xf);
    }

    /* renamed from: c */
    public C5663aRz[] mo309c() {
        return PlayerDirectionalController._m_fields;
    }

    /* renamed from: d */
    public void mo310d() {
        super.mo310d();
        this.point = null;
        this.speed = 0.0f;
        this.azQ = null;
        this.bDo = 0;
        this.aIR = 0;
        this.bGd = 0;
        this.bDp = 0;
        this.aJc = 0;
        this.bGe = 0;
    }
}
