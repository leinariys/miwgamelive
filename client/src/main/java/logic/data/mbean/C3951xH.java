package logic.data.mbean;

import game.script.item.buff.amplifier.ShipAmplifier;
import logic.baa.C1616Xf;
import logic.res.code.C5663aRz;
import p001a.C0064Am;
import p001a.C3892wO;

/* renamed from: a.xH */
public class C3951xH extends C3108nv {
    @C0064Am(aul = "e809ff6cade60ffa877623ed9ee3ae01", aum = 4)

    /* renamed from: VA */
    public C3892wO f9517VA;
    @C0064Am(aul = "0abec28a4c6db63b1b8af6384b313456", aum = 0)

    /* renamed from: Vs */
    public C3892wO f9518Vs;
    @C0064Am(aul = "58d8edbde89b9b8e3303629fb5117000", aum = 1)

    /* renamed from: Vu */
    public C3892wO f9519Vu;
    @C0064Am(aul = "0c4e3f30c461394c9128a5636ee74d78", aum = 2)

    /* renamed from: Vw */
    public C3892wO f9520Vw;
    @C0064Am(aul = "04c8147f3680352dac03812302f8e5e7", aum = 3)

    /* renamed from: Vy */
    public C3892wO f9521Vy;
    @C0064Am(aul = "0abec28a4c6db63b1b8af6384b313456", aum = -1)

    /* renamed from: YC */
    public byte f9522YC;
    @C0064Am(aul = "58d8edbde89b9b8e3303629fb5117000", aum = -1)

    /* renamed from: YD */
    public byte f9523YD;
    @C0064Am(aul = "0c4e3f30c461394c9128a5636ee74d78", aum = -1)

    /* renamed from: YE */
    public byte f9524YE;
    @C0064Am(aul = "04c8147f3680352dac03812302f8e5e7", aum = -1)

    /* renamed from: YF */
    public byte f9525YF;
    @C0064Am(aul = "0abec28a4c6db63b1b8af6384b313456", aum = -2)

    /* renamed from: Yw */
    public long f9526Yw;
    @C0064Am(aul = "58d8edbde89b9b8e3303629fb5117000", aum = -2)

    /* renamed from: Yx */
    public long f9527Yx;
    @C0064Am(aul = "0c4e3f30c461394c9128a5636ee74d78", aum = -2)

    /* renamed from: Yy */
    public long f9528Yy;
    @C0064Am(aul = "04c8147f3680352dac03812302f8e5e7", aum = -2)

    /* renamed from: Yz */
    public long f9529Yz;
    @C0064Am(aul = "4b4ffe4deb7ee9118992cc619ac6f849", aum = 5)
    public ShipAmplifier.ShipAmplifierAdapter aZf;
    @C0064Am(aul = "e809ff6cade60ffa877623ed9ee3ae01", aum = -2)
    public long bHw;
    @C0064Am(aul = "4b4ffe4deb7ee9118992cc619ac6f849", aum = -2)
    public long bHx;
    @C0064Am(aul = "e809ff6cade60ffa877623ed9ee3ae01", aum = -1)
    public byte bHy;
    @C0064Am(aul = "4b4ffe4deb7ee9118992cc619ac6f849", aum = -1)
    public byte bHz;

    public C3951xH() {
    }

    public C3951xH(C1616Xf xf) {
        super(xf);
    }

    /* renamed from: c */
    public C5663aRz[] mo309c() {
        return ShipAmplifier._m_fields;
    }

    /* renamed from: d */
    public void mo310d() {
        super.mo310d();
        this.f9518Vs = null;
        this.f9519Vu = null;
        this.f9520Vw = null;
        this.f9521Vy = null;
        this.f9517VA = null;
        this.aZf = null;
        this.f9526Yw = 0;
        this.f9527Yx = 0;
        this.f9528Yy = 0;
        this.f9529Yz = 0;
        this.bHw = 0;
        this.bHx = 0;
        this.f9522YC = 0;
        this.f9523YD = 0;
        this.f9524YE = 0;
        this.f9525YF = 0;
        this.bHy = 0;
        this.bHz = 0;
    }
}
