package logic.data.mbean;

import game.network.message.serializable.C3438ra;
import game.script.item.Amplifier;
import game.script.item.AmplifierType;
import game.script.item.buff.amplifier.ComposedAmplifier;
import logic.baa.C1616Xf;
import logic.res.code.C5663aRz;
import p001a.C0064Am;

/* renamed from: a.afU  reason: case insensitive filesystem */
public class C6048afU extends C3108nv {
    @C0064Am(aul = "c30efa658639badfda6157e00d2c396f", aum = 1)

    /* renamed from: OZ */
    public C3438ra<Amplifier> f4466OZ;
    @C0064Am(aul = "c30efa658639badfda6157e00d2c396f", aum = -2)
    public long asp;
    @C0064Am(aul = "c30efa658639badfda6157e00d2c396f", aum = -1)
    public byte asu;
    @C0064Am(aul = "822a21d4a6750bcba8128595b4d62d11", aum = 0)
    public C3438ra<AmplifierType> cRp;
    @C0064Am(aul = "822a21d4a6750bcba8128595b4d62d11", aum = -2)
    public long fvJ;
    @C0064Am(aul = "822a21d4a6750bcba8128595b4d62d11", aum = -1)
    public byte fvK;

    public C6048afU() {
    }

    public C6048afU(C1616Xf xf) {
        super(xf);
    }

    /* renamed from: c */
    public C5663aRz[] mo309c() {
        return ComposedAmplifier._m_fields;
    }

    /* renamed from: d */
    public void mo310d() {
        super.mo310d();
        this.cRp = null;
        this.f4466OZ = null;
        this.fvJ = 0;
        this.asp = 0;
        this.fvK = 0;
        this.asu = 0;
    }
}
