package logic.data.mbean;

import game.script.ship.FreighterCargoHoldAdapter;
import logic.baa.C1616Xf;
import logic.res.code.C5663aRz;
import p001a.C0064Am;

/* renamed from: a.Lj */
public class C0812Lj extends C6754asy {
    @C0064Am(aul = "1d58a7d162f607c4523599d9df524b45", aum = -2)

    /* renamed from: DM */
    public long f1056DM;
    @C0064Am(aul = "1d58a7d162f607c4523599d9df524b45", aum = -1)

    /* renamed from: DN */
    public byte f1057DN;
    @C0064Am(aul = "1d58a7d162f607c4523599d9df524b45", aum = 0)
    public float cvd;

    public C0812Lj() {
    }

    public C0812Lj(C1616Xf xf) {
        super(xf);
    }

    /* renamed from: c */
    public C5663aRz[] mo309c() {
        return FreighterCargoHoldAdapter._m_fields;
    }

    /* renamed from: d */
    public void mo310d() {
        super.mo310d();
        this.cvd = 0.0f;
        this.f1056DM = 0;
        this.f1057DN = 0;
    }
}
