package logic.data.mbean;

import game.script.resource.Asset;
import game.script.spacezone.NPCZoneType;
import game.script.spacezone.PopulationControlType;
import logic.baa.C1616Xf;
import logic.res.code.C5663aRz;
import p001a.C0064Am;

/* renamed from: a.hL */
public class C2596hL extends C0218Cj {
    @C0064Am(aul = "5adf1af3f09205dc0827b3cec5896333", aum = 1)

    /* renamed from: KI */
    public Asset f7863KI;
    @C0064Am(aul = "79232f60aabb8630baec9bdeac7fdf34", aum = 0)

    /* renamed from: Vk */
    public PopulationControlType f7864Vk;
    @C0064Am(aul = "79232f60aabb8630baec9bdeac7fdf34", aum = -2)

    /* renamed from: Vl */
    public long f7865Vl;
    @C0064Am(aul = "5adf1af3f09205dc0827b3cec5896333", aum = -2)

    /* renamed from: Vm */
    public long f7866Vm;
    @C0064Am(aul = "79232f60aabb8630baec9bdeac7fdf34", aum = -1)

    /* renamed from: Vn */
    public byte f7867Vn;
    @C0064Am(aul = "5adf1af3f09205dc0827b3cec5896333", aum = -1)

    /* renamed from: Vo */
    public byte f7868Vo;

    public C2596hL() {
    }

    public C2596hL(C1616Xf xf) {
        super(xf);
    }

    /* renamed from: c */
    public C5663aRz[] mo309c() {
        return NPCZoneType._m_fields;
    }

    /* renamed from: d */
    public void mo310d() {
        super.mo310d();
        this.f7864Vk = null;
        this.f7863KI = null;
        this.f7865Vl = 0;
        this.f7866Vm = 0;
        this.f7867Vn = 0;
        this.f7868Vo = 0;
    }
}
