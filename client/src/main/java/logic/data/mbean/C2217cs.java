package logic.data.mbean;

import com.hoplon.geometry.Vec3f;
import game.geometry.Quat4fWrap;
import game.geometry.Vec3d;
import game.network.message.serializable.C2686iZ;
import game.script.Actor;
import game.script.simulation.Space;
import game.script.space.Node;
import game.script.space.StellarSystem;
import logic.baa.C1616Xf;
import logic.res.code.C5663aRz;
import p001a.C0064Am;
import taikodom.infra.script.I18NString;

/* renamed from: a.cs */
public class C2217cs extends C3805vD {
    @C0064Am(aul = "30e07c4a83ade1e327032ad87cee176b", aum = 10)
    public Quat4fWrap orientation;
    @C0064Am(aul = "edb0e66f936dca77af3aee12ebe04082", aum = 11)
    public Vec3d position;
    @C0064Am(aul = "18880dbcb97b692a0e0f58790deaa20e", aum = 2)

    /* renamed from: rA */
    public C2686iZ<Actor> f6251rA;
    @C0064Am(aul = "7d187fb42cc298453221b0b97fb66e14", aum = 3)

    /* renamed from: rB */
    public boolean f6252rB;
    @C0064Am(aul = "66f7fbfb45e906eaba15a29293988dce", aum = 4)

    /* renamed from: rC */
    public boolean f6253rC;
    @C0064Am(aul = "c8e87e5dae9bd28c5dd68ed24a212353", aum = 5)

    /* renamed from: rD */
    public Pawn f6254rD;
    @C0064Am(aul = "6950ee21a27c9b33478239361b06012e", aum = 6)

    /* renamed from: rE */
    public Vec3f f6255rE;
    @C0064Am(aul = "643e0f783e741647789a48071c1f3bbd", aum = 7)

    /* renamed from: rF */
    public Vec3f f6256rF;
    @C0064Am(aul = "38997360d48b51df96f7c0b393c0a43d", aum = 8)

    /* renamed from: rG */
    public I18NString f6257rG;
    @C0064Am(aul = "584698af0f9efd8a233180c704ebbf7d", aum = 9)

    /* renamed from: rH */
    public boolean f6258rH;
    @C0064Am(aul = "4eeee5d4e9f83c40debb2bf18bf53646", aum = 12)

    /* renamed from: rI */
    public StellarSystem f6259rI;
    @C0064Am(aul = "5ef7d60da3a8b2ceb8ad24111107bebf", aum = 13)

    /* renamed from: rJ */
    public boolean f6260rJ;
    @C0064Am(aul = "ee970e8a1f16d5b7dee4c99e5eb7ea18", aum = 14)

    /* renamed from: rK */
    public Node f6261rK;
    @C0064Am(aul = "a3458a5b27bf26c0b6b7991dfe0f864d", aum = 15)

    /* renamed from: rL */
    public Space f6262rL;
    @C0064Am(aul = "bf81f67cc27de4cb991f3dcbed63f57e", aum = -2)

    /* renamed from: rM */
    public long f6263rM;
    @C0064Am(aul = "dceb75b2bf8b667232e642770a9fcfba", aum = -2)

    /* renamed from: rN */
    public long f6264rN;
    @C0064Am(aul = "18880dbcb97b692a0e0f58790deaa20e", aum = -2)

    /* renamed from: rO */
    public long f6265rO;
    @C0064Am(aul = "7d187fb42cc298453221b0b97fb66e14", aum = -2)

    /* renamed from: rP */
    public long f6266rP;
    @C0064Am(aul = "66f7fbfb45e906eaba15a29293988dce", aum = -2)

    /* renamed from: rQ */
    public long f6267rQ;
    @C0064Am(aul = "c8e87e5dae9bd28c5dd68ed24a212353", aum = -2)

    /* renamed from: rR */
    public long f6268rR;
    @C0064Am(aul = "6950ee21a27c9b33478239361b06012e", aum = -2)

    /* renamed from: rS */
    public long f6269rS;
    @C0064Am(aul = "643e0f783e741647789a48071c1f3bbd", aum = -2)

    /* renamed from: rT */
    public long f6270rT;
    @C0064Am(aul = "38997360d48b51df96f7c0b393c0a43d", aum = -2)

    /* renamed from: rU */
    public long f6271rU;
    @C0064Am(aul = "584698af0f9efd8a233180c704ebbf7d", aum = -2)

    /* renamed from: rV */
    public long f6272rV;
    @C0064Am(aul = "30e07c4a83ade1e327032ad87cee176b", aum = -2)

    /* renamed from: rW */
    public long f6273rW;
    @C0064Am(aul = "edb0e66f936dca77af3aee12ebe04082", aum = -2)

    /* renamed from: rX */
    public long f6274rX;
    @C0064Am(aul = "4eeee5d4e9f83c40debb2bf18bf53646", aum = -2)

    /* renamed from: rY */
    public long f6275rY;
    @C0064Am(aul = "5ef7d60da3a8b2ceb8ad24111107bebf", aum = -2)

    /* renamed from: rZ */
    public long f6276rZ;
    @C0064Am(aul = "bf81f67cc27de4cb991f3dcbed63f57e", aum = 0)

    /* renamed from: ry */
    public float f6277ry;
    @C0064Am(aul = "dceb75b2bf8b667232e642770a9fcfba", aum = 1)

    /* renamed from: rz */
    public Actor f6278rz;
    @C0064Am(aul = "ee970e8a1f16d5b7dee4c99e5eb7ea18", aum = -2)

    /* renamed from: sa */
    public long f6279sa;
    @C0064Am(aul = "a3458a5b27bf26c0b6b7991dfe0f864d", aum = -2)

    /* renamed from: sb */
    public long f6280sb;
    @C0064Am(aul = "bf81f67cc27de4cb991f3dcbed63f57e", aum = -1)

    /* renamed from: sc */
    public byte f6281sc;
    @C0064Am(aul = "dceb75b2bf8b667232e642770a9fcfba", aum = -1)

    /* renamed from: sd */
    public byte f6282sd;
    @C0064Am(aul = "18880dbcb97b692a0e0f58790deaa20e", aum = -1)

    /* renamed from: se */
    public byte f6283se;
    @C0064Am(aul = "7d187fb42cc298453221b0b97fb66e14", aum = -1)

    /* renamed from: sf */
    public byte f6284sf;
    @C0064Am(aul = "66f7fbfb45e906eaba15a29293988dce", aum = -1)

    /* renamed from: sg */
    public byte f6285sg;
    @C0064Am(aul = "c8e87e5dae9bd28c5dd68ed24a212353", aum = -1)

    /* renamed from: sh */
    public byte f6286sh;
    @C0064Am(aul = "6950ee21a27c9b33478239361b06012e", aum = -1)

    /* renamed from: si */
    public byte f6287si;
    @C0064Am(aul = "643e0f783e741647789a48071c1f3bbd", aum = -1)

    /* renamed from: sj */
    public byte f6288sj;
    @C0064Am(aul = "38997360d48b51df96f7c0b393c0a43d", aum = -1)

    /* renamed from: sk */
    public byte f6289sk;
    @C0064Am(aul = "584698af0f9efd8a233180c704ebbf7d", aum = -1)

    /* renamed from: sl */
    public byte f6290sl;
    @C0064Am(aul = "30e07c4a83ade1e327032ad87cee176b", aum = -1)

    /* renamed from: sm */
    public byte f6291sm;
    @C0064Am(aul = "edb0e66f936dca77af3aee12ebe04082", aum = -1)

    /* renamed from: sn */
    public byte f6292sn;
    @C0064Am(aul = "4eeee5d4e9f83c40debb2bf18bf53646", aum = -1)

    /* renamed from: so */
    public byte f6293so;
    @C0064Am(aul = "5ef7d60da3a8b2ceb8ad24111107bebf", aum = -1)

    /* renamed from: sp */
    public byte f6294sp;
    @C0064Am(aul = "ee970e8a1f16d5b7dee4c99e5eb7ea18", aum = -1)

    /* renamed from: sq */
    public byte f6295sq;
    @C0064Am(aul = "a3458a5b27bf26c0b6b7991dfe0f864d", aum = -1)

    /* renamed from: sr */
    public byte f6296sr;

    public C2217cs() {
    }

    public C2217cs(C1616Xf xf) {
        super(xf);
    }

    /* renamed from: c */
    public C5663aRz[] mo309c() {
        return Actor._m_fields;
    }

    /* renamed from: d */
    public void mo310d() {
        super.mo310d();
        this.f6277ry = 0.0f;
        this.f6278rz = null;
        this.f6251rA = null;
        this.f6252rB = false;
        this.f6253rC = false;
        this.f6254rD = null;
        this.f6255rE = null;
        this.f6256rF = null;
        this.f6257rG = null;
        this.f6258rH = false;
        this.orientation = null;
        this.position = null;
        this.f6259rI = null;
        this.f6260rJ = false;
        this.f6261rK = null;
        this.f6262rL = null;
        this.f6263rM = 0;
        this.f6264rN = 0;
        this.f6265rO = 0;
        this.f6266rP = 0;
        this.f6267rQ = 0;
        this.f6268rR = 0;
        this.f6269rS = 0;
        this.f6270rT = 0;
        this.f6271rU = 0;
        this.f6272rV = 0;
        this.f6273rW = 0;
        this.f6274rX = 0;
        this.f6275rY = 0;
        this.f6276rZ = 0;
        this.f6279sa = 0;
        this.f6280sb = 0;
        this.f6281sc = 0;
        this.f6282sd = 0;
        this.f6283se = 0;
        this.f6284sf = 0;
        this.f6285sg = 0;
        this.f6286sh = 0;
        this.f6287si = 0;
        this.f6288sj = 0;
        this.f6289sk = 0;
        this.f6290sl = 0;
        this.f6291sm = 0;
        this.f6292sn = 0;
        this.f6293so = 0;
        this.f6294sp = 0;
        this.f6295sq = 0;
        this.f6296sr = 0;
    }
}
