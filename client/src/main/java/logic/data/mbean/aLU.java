package logic.data.mbean;

import game.script.aggro.AggroTable;
import logic.baa.C1616Xf;
import logic.res.code.C5663aRz;
import p001a.C0064Am;

/* renamed from: a.aLU */
public class aLU extends C2217cs {
    @C0064Am(aul = "ec9e899e49495cc9bd643c0c1e8d8276", aum = 0)
    public boolean gGh;
    @C0064Am(aul = "9247267203fe61987eaa761b44215e88", aum = 1)
    public long gGj;
    @C0064Am(aul = "43a45c1f13c65ffc35ad4160a8103338", aum = 2)
    public AggroTable gGl;
    @C0064Am(aul = "ec9e899e49495cc9bd643c0c1e8d8276", aum = -2)
    public long inK;
    @C0064Am(aul = "9247267203fe61987eaa761b44215e88", aum = -2)
    public long inL;
    @C0064Am(aul = "43a45c1f13c65ffc35ad4160a8103338", aum = -2)
    public long inM;
    @C0064Am(aul = "ec9e899e49495cc9bd643c0c1e8d8276", aum = -1)
    public byte inN;
    @C0064Am(aul = "9247267203fe61987eaa761b44215e88", aum = -1)
    public byte inO;
    @C0064Am(aul = "43a45c1f13c65ffc35ad4160a8103338", aum = -1)
    public byte inP;

    public aLU() {
    }

    public aLU(C1616Xf xf) {
        super(xf);
    }

    /* renamed from: c */
    public C5663aRz[] mo309c() {
        return Pawn._m_fields;
    }

    /* renamed from: d */
    public void mo310d() {
        super.mo310d();
        this.gGh = false;
        this.gGj = 0;
        this.gGl = null;
        this.inK = 0;
        this.inL = 0;
        this.inM = 0;
        this.inN = 0;
        this.inO = 0;
        this.inP = 0;
    }
}
