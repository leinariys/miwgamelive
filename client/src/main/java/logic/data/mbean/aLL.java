package logic.data.mbean;

import game.CollisionFXSet;
import game.geometry.Vec3d;
import game.network.message.serializable.C3438ra;
import game.script.pda.DatabaseCategory;
import game.script.pda.TaikopediaEntry;
import game.script.resource.Asset;
import game.script.ship.*;
import game.script.space.LootType;
import logic.baa.C1616Xf;
import logic.res.code.C5663aRz;
import p001a.C0064Am;
import taikodom.infra.script.I18NString;

/* renamed from: a.aLL */
public class aLL extends C6905avt {
    @C0064Am(aul = "73b2db6e5bb6d3f94327d759092d198d", aum = 22)

    /* renamed from: LQ */
    public Asset f3284LQ;
    @C0064Am(aul = "291267db6c5b29dcfeb359bb9e9f3d7e", aum = 30)

    /* renamed from: WL */
    public float f3285WL;
    @C0064Am(aul = "08fa3e70d05931101c4b7051155c09fd", aum = 31)

    /* renamed from: WM */
    public float f3286WM;
    @C0064Am(aul = "21d17085c2c9311a571da84fb593abef", aum = 6)

    /* renamed from: Wb */
    public Asset f3287Wb;
    @C0064Am(aul = "3a169b6baacadee5e0e9322d4ff95243", aum = 7)

    /* renamed from: Wd */
    public Asset f3288Wd;
    @C0064Am(aul = "1eda3e3b844b14dbf9ab6ab2a782bd81", aum = -2)
    public long ajM;
    @C0064Am(aul = "1eda3e3b844b14dbf9ab6ab2a782bd81", aum = -1)
    public byte ajY;
    @C0064Am(aul = "62182e99d40679055bfdd3c6c3b1a88e", aum = -2)
    public long avd;
    @C0064Am(aul = "62182e99d40679055bfdd3c6c3b1a88e", aum = -1)
    public byte avi;
    @C0064Am(aul = "73b2db6e5bb6d3f94327d759092d198d", aum = -2)
    public long ayc;
    @C0064Am(aul = "73b2db6e5bb6d3f94327d759092d198d", aum = -1)
    public byte ayr;
    @C0064Am(aul = "1a0c775e0f24e9208410e158f2733eac", aum = -2)
    public long bHw;
    @C0064Am(aul = "1a0c775e0f24e9208410e158f2733eac", aum = -1)
    public byte bHy;
    @C0064Am(aul = "bfbbfbce0e341f232b6de4f7657b1299", aum = -1)
    public byte bhA;
    @C0064Am(aul = "21d17085c2c9311a571da84fb593abef", aum = -1)
    public byte bhC;
    @C0064Am(aul = "3a169b6baacadee5e0e9322d4ff95243", aum = -1)
    public byte bhD;
    @C0064Am(aul = "5c65ff8571e154539e3a702dafa5d3f5", aum = -1)
    public byte bhF;
    @C0064Am(aul = "48c4930db707d046cfbac8456038dd59", aum = 14)
    public float bhk;
    @C0064Am(aul = "f0e011f6e0ed047cea6f241a8fd4c6d1", aum = 10)
    public float bhl;
    @C0064Am(aul = "5c65ff8571e154539e3a702dafa5d3f5", aum = 34)
    public HullType bhn;
    @C0064Am(aul = "48c4930db707d046cfbac8456038dd59", aum = -2)
    public long bho;
    @C0064Am(aul = "a5c1b391d6327892172ac23fa23753bd", aum = -2)
    public long bhp;
    @C0064Am(aul = "f0e011f6e0ed047cea6f241a8fd4c6d1", aum = -2)
    public long bhq;
    @C0064Am(aul = "bfbbfbce0e341f232b6de4f7657b1299", aum = -2)
    public long bhr;
    @C0064Am(aul = "21d17085c2c9311a571da84fb593abef", aum = -2)
    public long bht;
    @C0064Am(aul = "3a169b6baacadee5e0e9322d4ff95243", aum = -2)
    public long bhu;
    @C0064Am(aul = "5c65ff8571e154539e3a702dafa5d3f5", aum = -2)
    public long bhw;
    @C0064Am(aul = "48c4930db707d046cfbac8456038dd59", aum = -1)
    public byte bhx;
    @C0064Am(aul = "a5c1b391d6327892172ac23fa23753bd", aum = -1)
    public byte bhy;
    @C0064Am(aul = "f0e011f6e0ed047cea6f241a8fd4c6d1", aum = -1)
    public byte bhz;
    @C0064Am(aul = "368a3fbcb4e3b0782c1ea05057249238", aum = 0)
    public CargoHoldType bnC;
    @C0064Am(aul = "479e144d222542d4cd699833a1703d41", aum = 3)
    public CollisionFXSet bnE;
    @C0064Am(aul = "1eda3e3b844b14dbf9ab6ab2a782bd81", aum = 4)
    public CruiseSpeedType bnG;
    @C0064Am(aul = "1ca50fd7afda7262fb75a522858bd914", aum = 5)
    public Asset bnI;
    @C0064Am(aul = "204b98e6b0b6c691367c4125a2ca5d69", aum = 8)
    public I18NString bnK;
    @C0064Am(aul = "b679ff8b5a15921b24cc5c2738dfbe15", aum = 9)
    public LootType bnM;
    @C0064Am(aul = "b9bbe135cc895ad63f2776a9b4fec021", aum = 12)
    public float bnQ;
    @C0064Am(aul = "6f09886ab27ed6def65f2ab7a12ab86a", aum = 13)
    public float bnS;
    @C0064Am(aul = "928a4fe010233b298b1e0e97f9ef6917", aum = 16)
    public float bnW;
    @C0064Am(aul = "ed3ef6b6fcd6db854fc4dd4a1ee71e2e", aum = 17)
    public float bnY;
    @C0064Am(aul = "894e1aa092c042f297543fda0d831526", aum = 18)
    public float boa;
    @C0064Am(aul = "3e3393327511358344ff4ca97d520acd", aum = 19)
    public Asset boc;
    @C0064Am(aul = "02b94c71488473c83f12e123414932aa", aum = 20)
    public float boe;
    @C0064Am(aul = "1a0c775e0f24e9208410e158f2733eac", aum = 21)
    public float bog;
    @C0064Am(aul = "dfb56f2dcfa4d13a8a9054e290ca2a09", aum = 23)
    public float boh;
    @C0064Am(aul = "460eae22844db2a80e0c9469d376aab4", aum = 24)
    public C3438ra<ShipStructureType> boj;
    @C0064Am(aul = "6fb84320de6368554f4e2fb4852f05e7", aum = 26)
    public C3438ra<C3315qE> bol;
    @C0064Am(aul = "088ddaf62e04789d9edae9c46d989697", aum = 27)
    public Vec3d bon;
    @C0064Am(aul = "0c8577a46f1b6f59c4a78d7c0c1a0e39", aum = 28)
    public Vec3d bop;
    @C0064Am(aul = "ebcd7769b9ffca827e3a466e0816f1d3", aum = 29)
    public Vec3d bor;
    @C0064Am(aul = "cbdf91aa85cc3f67bdb5002b1d23df58", aum = 33)
    public float bov;
    @C0064Am(aul = "a5c1b391d6327892172ac23fa23753bd", aum = 15)

    /* renamed from: dW */
    public float f3289dW;
    @C0064Am(aul = "bfbbfbce0e341f232b6de4f7657b1299", aum = 11)

    /* renamed from: dX */
    public float f3290dX;
    @C0064Am(aul = "479e144d222542d4cd699833a1703d41", aum = -2)
    public long fPc;
    @C0064Am(aul = "3e3393327511358344ff4ca97d520acd", aum = -2)
    public long fPf;
    @C0064Am(aul = "479e144d222542d4cd699833a1703d41", aum = -1)
    public byte fPj;
    @C0064Am(aul = "3e3393327511358344ff4ca97d520acd", aum = -1)
    public byte fPm;
    @C0064Am(aul = "b679ff8b5a15921b24cc5c2738dfbe15", aum = -2)
    public long hhc;
    @C0064Am(aul = "b679ff8b5a15921b24cc5c2738dfbe15", aum = -1)
    public byte hhd;
    @C0064Am(aul = "928a4fe010233b298b1e0e97f9ef6917", aum = -2)
    public long imA;
    @C0064Am(aul = "ed3ef6b6fcd6db854fc4dd4a1ee71e2e", aum = -2)
    public long imB;
    @C0064Am(aul = "894e1aa092c042f297543fda0d831526", aum = -2)
    public long imC;
    @C0064Am(aul = "02b94c71488473c83f12e123414932aa", aum = -2)
    public long imD;
    @C0064Am(aul = "dfb56f2dcfa4d13a8a9054e290ca2a09", aum = -2)
    public long imE;
    @C0064Am(aul = "460eae22844db2a80e0c9469d376aab4", aum = -2)
    public long imF;
    @C0064Am(aul = "6fb84320de6368554f4e2fb4852f05e7", aum = -2)
    public long imG;
    @C0064Am(aul = "088ddaf62e04789d9edae9c46d989697", aum = -2)
    public long imH;
    @C0064Am(aul = "0c8577a46f1b6f59c4a78d7c0c1a0e39", aum = -2)
    public long imI;
    @C0064Am(aul = "ebcd7769b9ffca827e3a466e0816f1d3", aum = -2)
    public long imJ;
    @C0064Am(aul = "291267db6c5b29dcfeb359bb9e9f3d7e", aum = -2)
    public long imK;
    @C0064Am(aul = "08fa3e70d05931101c4b7051155c09fd", aum = -2)
    public long imL;
    @C0064Am(aul = "cbdf91aa85cc3f67bdb5002b1d23df58", aum = -2)
    public long imM;
    @C0064Am(aul = "368a3fbcb4e3b0782c1ea05057249238", aum = -1)
    public byte imN;
    @C0064Am(aul = "1ca50fd7afda7262fb75a522858bd914", aum = -1)
    public byte imO;
    @C0064Am(aul = "204b98e6b0b6c691367c4125a2ca5d69", aum = -1)
    public byte imP;
    @C0064Am(aul = "b9bbe135cc895ad63f2776a9b4fec021", aum = -1)
    public byte imQ;
    @C0064Am(aul = "6f09886ab27ed6def65f2ab7a12ab86a", aum = -1)
    public byte imR;
    @C0064Am(aul = "928a4fe010233b298b1e0e97f9ef6917", aum = -1)
    public byte imS;
    @C0064Am(aul = "ed3ef6b6fcd6db854fc4dd4a1ee71e2e", aum = -1)
    public byte imT;
    @C0064Am(aul = "894e1aa092c042f297543fda0d831526", aum = -1)
    public byte imU;
    @C0064Am(aul = "02b94c71488473c83f12e123414932aa", aum = -1)
    public byte imV;
    @C0064Am(aul = "dfb56f2dcfa4d13a8a9054e290ca2a09", aum = -1)
    public byte imW;
    @C0064Am(aul = "460eae22844db2a80e0c9469d376aab4", aum = -1)
    public byte imX;
    @C0064Am(aul = "6fb84320de6368554f4e2fb4852f05e7", aum = -1)
    public byte imY;
    @C0064Am(aul = "088ddaf62e04789d9edae9c46d989697", aum = -1)
    public byte imZ;
    @C0064Am(aul = "368a3fbcb4e3b0782c1ea05057249238", aum = -2)
    public long imv;
    @C0064Am(aul = "1ca50fd7afda7262fb75a522858bd914", aum = -2)
    public long imw;
    @C0064Am(aul = "204b98e6b0b6c691367c4125a2ca5d69", aum = -2)
    public long imx;
    @C0064Am(aul = "b9bbe135cc895ad63f2776a9b4fec021", aum = -2)
    public long imy;
    @C0064Am(aul = "6f09886ab27ed6def65f2ab7a12ab86a", aum = -2)
    public long imz;
    @C0064Am(aul = "0c8577a46f1b6f59c4a78d7c0c1a0e39", aum = -1)
    public byte ina;
    @C0064Am(aul = "ebcd7769b9ffca827e3a466e0816f1d3", aum = -1)
    public byte inb;
    @C0064Am(aul = "291267db6c5b29dcfeb359bb9e9f3d7e", aum = -1)
    public byte inc;
    @C0064Am(aul = "08fa3e70d05931101c4b7051155c09fd", aum = -1)
    public byte ind;
    @C0064Am(aul = "cbdf91aa85cc3f67bdb5002b1d23df58", aum = -1)
    public byte ine;
    @C0064Am(aul = "822a91b5a407dbe9eef26ac76e93897a", aum = 1)

    /* renamed from: jS */
    public DatabaseCategory f3291jS;
    @C0064Am(aul = "5155673a67c1317e271851e80810b375", aum = 2)

    /* renamed from: jT */
    public C3438ra<TaikopediaEntry> f3292jT;
    @C0064Am(aul = "ce96933a3a33a11f2c6b22de7916e496", aum = 32)

    /* renamed from: jV */
    public float f3293jV;
    @C0064Am(aul = "822a91b5a407dbe9eef26ac76e93897a", aum = -2)

    /* renamed from: jX */
    public long f3294jX;
    @C0064Am(aul = "5155673a67c1317e271851e80810b375", aum = -2)

    /* renamed from: jY */
    public long f3295jY;
    @C0064Am(aul = "ce96933a3a33a11f2c6b22de7916e496", aum = -2)

    /* renamed from: ka */
    public long f3296ka;
    @C0064Am(aul = "822a91b5a407dbe9eef26ac76e93897a", aum = -1)

    /* renamed from: kc */
    public byte f3297kc;
    @C0064Am(aul = "5155673a67c1317e271851e80810b375", aum = -1)

    /* renamed from: kd */
    public byte f3298kd;
    @C0064Am(aul = "ce96933a3a33a11f2c6b22de7916e496", aum = -1)

    /* renamed from: kf */
    public byte f3299kf;
    @C0064Am(aul = "62182e99d40679055bfdd3c6c3b1a88e", aum = 25)

    /* renamed from: uT */
    public String f3300uT;

    public aLL() {
    }

    public aLL(C1616Xf xf) {
        super(xf);
    }

    /* renamed from: c */
    public C5663aRz[] mo309c() {
        return ShipType._m_fields;
    }

    /* renamed from: d */
    public void mo310d() {
        super.mo310d();
        this.bnC = null;
        this.f3291jS = null;
        this.f3292jT = null;
        this.bnE = null;
        this.bnG = null;
        this.bnI = null;
        this.f3287Wb = null;
        this.f3288Wd = null;
        this.bnK = null;
        this.bnM = null;
        this.bhl = 0.0f;
        this.f3290dX = 0.0f;
        this.bnQ = 0.0f;
        this.bnS = 0.0f;
        this.bhk = 0.0f;
        this.f3289dW = 0.0f;
        this.bnW = 0.0f;
        this.bnY = 0.0f;
        this.boa = 0.0f;
        this.boc = null;
        this.boe = 0.0f;
        this.bog = 0.0f;
        this.f3284LQ = null;
        this.boh = 0.0f;
        this.boj = null;
        this.f3300uT = null;
        this.bol = null;
        this.bon = null;
        this.bop = null;
        this.bor = null;
        this.f3285WL = 0.0f;
        this.f3286WM = 0.0f;
        this.f3293jV = 0.0f;
        this.bov = 0.0f;
        this.bhn = null;
        this.imv = 0;
        this.f3294jX = 0;
        this.f3295jY = 0;
        this.fPc = 0;
        this.ajM = 0;
        this.imw = 0;
        this.bht = 0;
        this.bhu = 0;
        this.imx = 0;
        this.hhc = 0;
        this.bhq = 0;
        this.bhr = 0;
        this.imy = 0;
        this.imz = 0;
        this.bho = 0;
        this.bhp = 0;
        this.imA = 0;
        this.imB = 0;
        this.imC = 0;
        this.fPf = 0;
        this.imD = 0;
        this.bHw = 0;
        this.ayc = 0;
        this.imE = 0;
        this.imF = 0;
        this.avd = 0;
        this.imG = 0;
        this.imH = 0;
        this.imI = 0;
        this.imJ = 0;
        this.imK = 0;
        this.imL = 0;
        this.f3296ka = 0;
        this.imM = 0;
        this.bhw = 0;
        this.imN = 0;
        this.f3297kc = 0;
        this.f3298kd = 0;
        this.fPj = 0;
        this.ajY = 0;
        this.imO = 0;
        this.bhC = 0;
        this.bhD = 0;
        this.imP = 0;
        this.hhd = 0;
        this.bhz = 0;
        this.bhA = 0;
        this.imQ = 0;
        this.imR = 0;
        this.bhx = 0;
        this.bhy = 0;
        this.imS = 0;
        this.imT = 0;
        this.imU = 0;
        this.fPm = 0;
        this.imV = 0;
        this.bHy = 0;
        this.ayr = 0;
        this.imW = 0;
        this.imX = 0;
        this.avi = 0;
        this.imY = 0;
        this.imZ = 0;
        this.ina = 0;
        this.inb = 0;
        this.inc = 0;
        this.ind = 0;
        this.f3299kf = 0;
        this.ine = 0;
        this.bhF = 0;
    }
}
