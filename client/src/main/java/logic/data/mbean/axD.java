package logic.data.mbean;

import game.network.message.serializable.C1556Wo;
import game.network.message.serializable.C2686iZ;
import game.script.nls.NLSManager;
import game.script.nls.TranslationNode;
import logic.baa.C1616Xf;
import logic.baa.aDJ;
import logic.res.code.C5663aRz;
import p001a.C0064Am;

import java.util.UUID;

/* renamed from: a.axD */
public class axD extends C5292aDs {
    @C0064Am(aul = "2d523df96815d9185ed2973947967c36", aum = 0)

    /* renamed from: bK */
    public UUID f5537bK;
    @C0064Am(aul = "d99cabc409cbb5fa7ab09dce7121c991", aum = 1)
    public C1556Wo<NLSManager.C1472a, aDJ> gNt;
    @C0064Am(aul = "5cedf5758f55f2f9d97aaeff8fb2e540", aum = 2)
    public C1556Wo<String, TranslationNode> gNv;
    @C0064Am(aul = "4d23047d71c5f9a0c0f050bd078a11fd", aum = 3)
    public C2686iZ<String> gNx;
    @C0064Am(aul = "d99cabc409cbb5fa7ab09dce7121c991", aum = -2)
    public long gQf;
    @C0064Am(aul = "5cedf5758f55f2f9d97aaeff8fb2e540", aum = -2)
    public long gQg;
    @C0064Am(aul = "4d23047d71c5f9a0c0f050bd078a11fd", aum = -2)
    public long gQh;
    @C0064Am(aul = "d99cabc409cbb5fa7ab09dce7121c991", aum = -1)
    public byte gQi;
    @C0064Am(aul = "5cedf5758f55f2f9d97aaeff8fb2e540", aum = -1)
    public byte gQj;
    @C0064Am(aul = "4d23047d71c5f9a0c0f050bd078a11fd", aum = -1)
    public byte gQk;
    @C0064Am(aul = "2d523df96815d9185ed2973947967c36", aum = -2)

    /* renamed from: oL */
    public long f5538oL;
    @C0064Am(aul = "2d523df96815d9185ed2973947967c36", aum = -1)

    /* renamed from: oS */
    public byte f5539oS;

    public axD() {
    }

    public axD(C1616Xf xf) {
        super(xf);
    }

    /* renamed from: c */
    public C5663aRz[] mo309c() {
        return NLSManager._m_fields;
    }

    /* renamed from: d */
    public void mo310d() {
        super.mo310d();
        this.f5537bK = null;
        this.gNt = null;
        this.gNv = null;
        this.gNx = null;
        this.f5538oL = 0;
        this.gQf = 0;
        this.gQg = 0;
        this.gQh = 0;
        this.f5539oS = 0;
        this.gQi = 0;
        this.gQj = 0;
        this.gQk = 0;
    }
}
