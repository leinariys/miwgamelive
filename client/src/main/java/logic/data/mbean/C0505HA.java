package logic.data.mbean;

import game.network.message.serializable.C2686iZ;
import game.script.Actor;
import game.script.mission.Mission;
import game.script.mission.MissionItem;
import game.script.mission.MissionTimer;
import game.script.mission.MissionTrigger;
import game.script.npc.NPC;
import logic.baa.C1616Xf;
import logic.res.code.C5663aRz;
import p001a.C0064Am;
import p001a.C4045yZ;

/* renamed from: a.HA */
public class C0505HA extends C3805vD {
    @C0064Am(aul = "4af313ac3bd302374e3a6d864f25fbca", aum = -2)
    public long baN;
    @C0064Am(aul = "4af313ac3bd302374e3a6d864f25fbca", aum = -1)
    public byte baO;
    @C0064Am(aul = "62b4ce0ab39edb5a52e95752a92ce832", aum = 0)
    public LDScriptingController dbY;
    @C0064Am(aul = "4af313ac3bd302374e3a6d864f25fbca", aum = 2)
    public C4045yZ dbZ;
    @C0064Am(aul = "aa0c717889a7c1634c38e9fd0c102337", aum = -1)
    public byte dcA;
    @C0064Am(aul = "e7d5a1e16d5a648daf6e780b3fa5cef7", aum = -1)
    public byte dcB;
    @C0064Am(aul = "4d9b2bdc4b276ca6692969f9be624966", aum = -1)
    public byte dcC;
    @C0064Am(aul = "f8f7aff71b31bde5fa140260f0588e12", aum = 3)
    public C2686iZ<MissionTimer> dca;
    @C0064Am(aul = "b1fa53dea9d5f9a77d41d9ea00b9ec7c", aum = 4)
    public C2686iZ<MissionTrigger> dcb;
    @C0064Am(aul = "9e2ea73ce448fc7f35f6c85369530556", aum = 5)
    public Actor dcc;
    @C0064Am(aul = "35f5505ef92412f1e128fc997292ad63", aum = 6)
    public Actor dcd;
    @C0064Am(aul = "39b1bcd73eed57626d96860fa43df2a3", aum = 7)
    public C2686iZ<NPC> dce;
    @C0064Am(aul = "1e92689c6677ef81ede1960810ac3394", aum = 8)
    public C2686iZ<NPC> dcf;
    @C0064Am(aul = "aa0c717889a7c1634c38e9fd0c102337", aum = 9)
    public C2686iZ<MissionItem> dcg;
    @C0064Am(aul = "e7d5a1e16d5a648daf6e780b3fa5cef7", aum = 10)
    public boolean dch;
    @C0064Am(aul = "4d9b2bdc4b276ca6692969f9be624966", aum = 11)
    public boolean dci;
    @C0064Am(aul = "62b4ce0ab39edb5a52e95752a92ce832", aum = -2)
    public long dcj;
    @C0064Am(aul = "f8f7aff71b31bde5fa140260f0588e12", aum = -2)
    public long dck;
    @C0064Am(aul = "b1fa53dea9d5f9a77d41d9ea00b9ec7c", aum = -2)
    public long dcl;
    @C0064Am(aul = "9e2ea73ce448fc7f35f6c85369530556", aum = -2)
    public long dcm;
    @C0064Am(aul = "35f5505ef92412f1e128fc997292ad63", aum = -2)
    public long dcn;
    @C0064Am(aul = "39b1bcd73eed57626d96860fa43df2a3", aum = -2)
    public long dco;
    @C0064Am(aul = "1e92689c6677ef81ede1960810ac3394", aum = -2)
    public long dcp;
    @C0064Am(aul = "aa0c717889a7c1634c38e9fd0c102337", aum = -2)
    public long dcq;
    @C0064Am(aul = "e7d5a1e16d5a648daf6e780b3fa5cef7", aum = -2)
    public long dcr;
    @C0064Am(aul = "4d9b2bdc4b276ca6692969f9be624966", aum = -2)
    public long dcs;
    @C0064Am(aul = "62b4ce0ab39edb5a52e95752a92ce832", aum = -1)
    public byte dct;
    @C0064Am(aul = "f8f7aff71b31bde5fa140260f0588e12", aum = -1)
    public byte dcu;
    @C0064Am(aul = "b1fa53dea9d5f9a77d41d9ea00b9ec7c", aum = -1)
    public byte dcv;
    @C0064Am(aul = "9e2ea73ce448fc7f35f6c85369530556", aum = -1)
    public byte dcw;
    @C0064Am(aul = "35f5505ef92412f1e128fc997292ad63", aum = -1)
    public byte dcx;
    @C0064Am(aul = "39b1bcd73eed57626d96860fa43df2a3", aum = -1)
    public byte dcy;
    @C0064Am(aul = "1e92689c6677ef81ede1960810ac3394", aum = -1)
    public byte dcz;
    @C0064Am(aul = "cbcdbb9ca3e3484eb24b6920064586ff", aum = 1)

    /* renamed from: oi */
    public Mission.C0015a f654oi;
    @C0064Am(aul = "cbcdbb9ca3e3484eb24b6920064586ff", aum = -2)

    /* renamed from: ol */
    public long f655ol;
    @C0064Am(aul = "cbcdbb9ca3e3484eb24b6920064586ff", aum = -1)

    /* renamed from: oo */
    public byte f656oo;

    public C0505HA() {
    }

    public C0505HA(C1616Xf xf) {
        super(xf);
    }

    /* renamed from: c */
    public C5663aRz[] mo309c() {
        return Mission._m_fields;
    }

    /* renamed from: d */
    public void mo310d() {
        super.mo310d();
        this.dbY = null;
        this.f654oi = null;
        this.dbZ = null;
        this.dca = null;
        this.dcb = null;
        this.dcc = null;
        this.dcd = null;
        this.dce = null;
        this.dcf = null;
        this.dcg = null;
        this.dch = false;
        this.dci = false;
        this.dcj = 0;
        this.f655ol = 0;
        this.baN = 0;
        this.dck = 0;
        this.dcl = 0;
        this.dcm = 0;
        this.dcn = 0;
        this.dco = 0;
        this.dcp = 0;
        this.dcq = 0;
        this.dcr = 0;
        this.dcs = 0;
        this.dct = 0;
        this.f656oo = 0;
        this.baO = 0;
        this.dcu = 0;
        this.dcv = 0;
        this.dcw = 0;
        this.dcx = 0;
        this.dcy = 0;
        this.dcz = 0;
        this.dcA = 0;
        this.dcB = 0;
        this.dcC = 0;
    }
}
