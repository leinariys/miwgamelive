package logic.data.mbean;

import game.network.message.serializable.C3438ra;
import game.script.missiontemplate.scripting.MiningMissionScriptTemplate;
import game.script.ship.Station;
import logic.baa.C1616Xf;
import logic.res.code.C5663aRz;
import p001a.C0064Am;

/* renamed from: a.awR  reason: case insensitive filesystem */
public class C6929awR extends C0597IR {
    @C0064Am(aul = "34bba9a17f285ce74ee99dd119500aff", aum = 0)
    public String bBL;
    @C0064Am(aul = "a08975ac5598c68a566254f0f55d5a91", aum = 1)
    public Station bBN;
    @C0064Am(aul = "e3d80ce90a457c522291c99688793df1", aum = 2)
    public C3438ra<ItemTypeTableItem> bBP;
    @C0064Am(aul = "34bba9a17f285ce74ee99dd119500aff", aum = -2)
    public long gOc;
    @C0064Am(aul = "a08975ac5598c68a566254f0f55d5a91", aum = -2)
    public long gOd;
    @C0064Am(aul = "e3d80ce90a457c522291c99688793df1", aum = -2)
    public long gOe;
    @C0064Am(aul = "34bba9a17f285ce74ee99dd119500aff", aum = -1)
    public byte gOf;
    @C0064Am(aul = "a08975ac5598c68a566254f0f55d5a91", aum = -1)
    public byte gOg;
    @C0064Am(aul = "e3d80ce90a457c522291c99688793df1", aum = -1)
    public byte gOh;

    public C6929awR() {
    }

    public C6929awR(C1616Xf xf) {
        super(xf);
    }

    /* renamed from: c */
    public C5663aRz[] mo309c() {
        return MiningMissionScriptTemplate._m_fields;
    }

    /* renamed from: d */
    public void mo310d() {
        super.mo310d();
        this.bBL = null;
        this.bBN = null;
        this.bBP = null;
        this.gOc = 0;
        this.gOd = 0;
        this.gOe = 0;
        this.gOf = 0;
        this.gOg = 0;
        this.gOh = 0;
    }
}
