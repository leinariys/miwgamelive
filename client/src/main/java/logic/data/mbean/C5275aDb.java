package logic.data.mbean;

import game.geometry.Vec3d;
import game.script.mission.TableNPCSpawn;
import game.script.missiontemplate.WaypointDat;
import logic.baa.C1616Xf;
import logic.res.code.C5663aRz;
import p001a.C0064Am;

import java.util.UUID;

/* renamed from: a.aDb  reason: case insensitive filesystem */
public class C5275aDb extends C5292aDs {
    @C0064Am(aul = "6335c5a6efee71efc18cabd4a6152886", aum = -2)

    /* renamed from: DA */
    public long f2560DA;
    @C0064Am(aul = "cef9eeb6737196a9d0c131089144b1a7", aum = -2)

    /* renamed from: DB */
    public long f2561DB;
    @C0064Am(aul = "6335c5a6efee71efc18cabd4a6152886", aum = -1)

    /* renamed from: DE */
    public byte f2562DE;
    @C0064Am(aul = "cef9eeb6737196a9d0c131089144b1a7", aum = -1)

    /* renamed from: DF */
    public byte f2563DF;
    @C0064Am(aul = "6335c5a6efee71efc18cabd4a6152886", aum = 1)

    /* renamed from: Dw */
    public long f2564Dw;
    @C0064Am(aul = "cef9eeb6737196a9d0c131089144b1a7", aum = 2)

    /* renamed from: Dx */
    public long f2565Dx;
    @C0064Am(aul = "5e0d11c863c400a4c1766a0da24123eb", aum = 8)
    public boolean aLE;
    @C0064Am(aul = "0e0247ac0832cd61ab59c7daf6421997", aum = -2)
    public long aLG;
    @C0064Am(aul = "5e0d11c863c400a4c1766a0da24123eb", aum = -2)
    public long aLJ;
    @C0064Am(aul = "0e0247ac0832cd61ab59c7daf6421997", aum = -1)
    public byte aLL;
    @C0064Am(aul = "5e0d11c863c400a4c1766a0da24123eb", aum = -1)
    public byte aLO;
    @C0064Am(aul = "c67bd85c5cfaf5d4e93bd5e50a5eb1fb", aum = 9)

    /* renamed from: bK */
    public UUID f2566bK;
    @C0064Am(aul = "0e0247ac0832cd61ab59c7daf6421997", aum = 3)

    /* renamed from: cy */
    public int f2567cy;
    @C0064Am(aul = "50ec17bd592dcdb9bde366beb29bdc14", aum = 4)
    public TableNPCSpawn ejG;
    @C0064Am(aul = "05fe859c91277c191aa1ebd0bb430041", aum = 5)
    public WaypointDat.C2121a ejI;
    @C0064Am(aul = "711472b8e68216b03e8ad73240c83ac4", aum = 6)
    public int ejK;
    @C0064Am(aul = "7f61313ad0b0798ea2aefeadb60b47fd", aum = 7)
    public TableNPCSpawn ejM;
    @C0064Am(aul = "e2c87ced50f5d308845bda2ed9091bef", aum = 10)
    public String handle;
    @C0064Am(aul = "50ec17bd592dcdb9bde366beb29bdc14", aum = -2)
    public long hxJ;
    @C0064Am(aul = "05fe859c91277c191aa1ebd0bb430041", aum = -2)
    public long hxK;
    @C0064Am(aul = "711472b8e68216b03e8ad73240c83ac4", aum = -2)
    public long hxL;
    @C0064Am(aul = "7f61313ad0b0798ea2aefeadb60b47fd", aum = -2)
    public long hxM;
    @C0064Am(aul = "50ec17bd592dcdb9bde366beb29bdc14", aum = -1)
    public byte hxN;
    @C0064Am(aul = "05fe859c91277c191aa1ebd0bb430041", aum = -1)
    public byte hxO;
    @C0064Am(aul = "711472b8e68216b03e8ad73240c83ac4", aum = -1)
    public byte hxP;
    @C0064Am(aul = "7f61313ad0b0798ea2aefeadb60b47fd", aum = -1)
    public byte hxQ;
    @C0064Am(aul = "c67bd85c5cfaf5d4e93bd5e50a5eb1fb", aum = -2)

    /* renamed from: oL */
    public long f2568oL;
    @C0064Am(aul = "c67bd85c5cfaf5d4e93bd5e50a5eb1fb", aum = -1)

    /* renamed from: oS */
    public byte f2569oS;
    @C0064Am(aul = "e2c87ced50f5d308845bda2ed9091bef", aum = -2)

    /* renamed from: ok */
    public long f2570ok;
    @C0064Am(aul = "e2c87ced50f5d308845bda2ed9091bef", aum = -1)

    /* renamed from: on */
    public byte f2571on;
    @C0064Am(aul = "6af8db00be789d52a9859370de698a9d", aum = 0)
    public Vec3d position;
    @C0064Am(aul = "6af8db00be789d52a9859370de698a9d", aum = -2)

    /* renamed from: rX */
    public long f2572rX;
    @C0064Am(aul = "6af8db00be789d52a9859370de698a9d", aum = -1)

    /* renamed from: sn */
    public byte f2573sn;

    public C5275aDb() {
    }

    public C5275aDb(C1616Xf xf) {
        super(xf);
    }

    /* renamed from: c */
    public C5663aRz[] mo309c() {
        return WaypointDat._m_fields;
    }

    /* renamed from: d */
    public void mo310d() {
        super.mo310d();
        this.position = null;
        this.f2564Dw = 0;
        this.f2565Dx = 0;
        this.f2567cy = 0;
        this.ejG = null;
        this.ejI = null;
        this.ejK = 0;
        this.ejM = null;
        this.aLE = false;
        this.f2566bK = null;
        this.handle = null;
        this.f2572rX = 0;
        this.f2560DA = 0;
        this.f2561DB = 0;
        this.aLG = 0;
        this.hxJ = 0;
        this.hxK = 0;
        this.hxL = 0;
        this.hxM = 0;
        this.aLJ = 0;
        this.f2568oL = 0;
        this.f2570ok = 0;
        this.f2573sn = 0;
        this.f2562DE = 0;
        this.f2563DF = 0;
        this.aLL = 0;
        this.hxN = 0;
        this.hxO = 0;
        this.hxP = 0;
        this.hxQ = 0;
        this.aLO = 0;
        this.f2569oS = 0;
        this.f2571on = 0;
    }
}
