package logic.data.mbean;

import game.script.mission.MissionTemplateObjective;
import logic.baa.C1616Xf;
import logic.res.code.C5663aRz;
import p001a.C0064Am;
import taikodom.infra.script.I18NString;

import java.util.UUID;

/* renamed from: a.aTa  reason: case insensitive filesystem */
public class C5690aTa extends C5292aDs {
    @C0064Am(aul = "ad01a73120a3c5a1d84f03c12cab7f7d", aum = 4)

    /* renamed from: bK */
    public UUID f3798bK;
    @C0064Am(aul = "2a184974264302b93b647b38dfb77196", aum = 3)
    public boolean big;
    @C0064Am(aul = "eb21a6a91565c7c71c723a64f3c848e4", aum = 2)

    /* renamed from: cZ */
    public int f3799cZ;
    @C0064Am(aul = "2a184974264302b93b647b38dfb77196", aum = -2)
    public long dIW;
    @C0064Am(aul = "2a184974264302b93b647b38dfb77196", aum = -1)
    public byte dJc;
    @C0064Am(aul = "eb21a6a91565c7c71c723a64f3c848e4", aum = -2)

    /* renamed from: dh */
    public long f3800dh;
    @C0064Am(aul = "eb21a6a91565c7c71c723a64f3c848e4", aum = -1)

    /* renamed from: do */
    public byte f3801do;
    @C0064Am(aul = "363cfe5cccc7a90afbd125747fbe1f5d", aum = 0)
    public String handle;
    @C0064Am(aul = "84ca92645366e4b47d496b16b33937f4", aum = 1)

    /* renamed from: nh */
    public I18NString f3802nh;
    @C0064Am(aul = "84ca92645366e4b47d496b16b33937f4", aum = -2)

    /* renamed from: nk */
    public long f3803nk;
    @C0064Am(aul = "84ca92645366e4b47d496b16b33937f4", aum = -1)

    /* renamed from: nn */
    public byte f3804nn;
    @C0064Am(aul = "ad01a73120a3c5a1d84f03c12cab7f7d", aum = -2)

    /* renamed from: oL */
    public long f3805oL;
    @C0064Am(aul = "ad01a73120a3c5a1d84f03c12cab7f7d", aum = -1)

    /* renamed from: oS */
    public byte f3806oS;
    @C0064Am(aul = "363cfe5cccc7a90afbd125747fbe1f5d", aum = -2)

    /* renamed from: ok */
    public long f3807ok;
    @C0064Am(aul = "363cfe5cccc7a90afbd125747fbe1f5d", aum = -1)

    /* renamed from: on */
    public byte f3808on;

    public C5690aTa() {
    }

    public C5690aTa(C1616Xf xf) {
        super(xf);
    }

    /* renamed from: c */
    public C5663aRz[] mo309c() {
        return MissionTemplateObjective._m_fields;
    }

    /* renamed from: d */
    public void mo310d() {
        super.mo310d();
        this.handle = null;
        this.f3802nh = null;
        this.f3799cZ = 0;
        this.big = false;
        this.f3798bK = null;
        this.f3807ok = 0;
        this.f3803nk = 0;
        this.f3800dh = 0;
        this.dIW = 0;
        this.f3805oL = 0;
        this.f3808on = 0;
        this.f3804nn = 0;
        this.f3801do = 0;
        this.dJc = 0;
        this.f3806oS = 0;
    }
}
