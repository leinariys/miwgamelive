package logic.data.mbean;

import game.script.citizenship.inprovements.HangarImprovementType;
import logic.baa.C1616Xf;
import logic.res.code.C5663aRz;
import p001a.C0064Am;

/* renamed from: a.dC */
public class C2236dC extends C6071afr {
    @C0064Am(aul = "2b1d435a6ec596a6bf2ac14bef9f62e9", aum = 0)

    /* renamed from: zI */
    public int f6437zI;
    @C0064Am(aul = "2b1d435a6ec596a6bf2ac14bef9f62e9", aum = -2)

    /* renamed from: zJ */
    public long f6438zJ;
    @C0064Am(aul = "2b1d435a6ec596a6bf2ac14bef9f62e9", aum = -1)

    /* renamed from: zK */
    public byte f6439zK;

    public C2236dC() {
    }

    public C2236dC(C1616Xf xf) {
        super(xf);
    }

    /* renamed from: c */
    public C5663aRz[] mo309c() {
        return HangarImprovementType._m_fields;
    }

    /* renamed from: d */
    public void mo310d() {
        super.mo310d();
        this.f6437zI = 0;
        this.f6438zJ = 0;
        this.f6439zK = 0;
    }
}
