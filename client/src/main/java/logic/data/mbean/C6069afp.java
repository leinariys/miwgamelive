package logic.data.mbean;

import game.script.mission.TableNPCSpawn;
import game.script.missiontemplate.ReconWaypointDat;
import logic.baa.C1616Xf;
import logic.res.code.C5663aRz;
import p001a.C0064Am;

/* renamed from: a.afp  reason: case insensitive filesystem */
public class C6069afp extends C5275aDb {
    @C0064Am(aul = "842c1410a91b25f67e43d1c852875c24", aum = 0)

    /* renamed from: QV */
    public float f4471QV;
    @C0064Am(aul = "842c1410a91b25f67e43d1c852875c24", aum = -2)
    public long dPo;
    @C0064Am(aul = "842c1410a91b25f67e43d1c852875c24", aum = -1)
    public byte dPr;
    @C0064Am(aul = "e35ae85fc5f0e00dc04f3b922b2112fc", aum = 1)
    public int ddH;
    @C0064Am(aul = "784ad1e8ea2b5f99b69f037800499550", aum = 2)
    public TableNPCSpawn ddJ;
    @C0064Am(aul = "7fe7012a4580b9a033a0db5781c7abf0", aum = 3)
    public int ddL;
    @C0064Am(aul = "e35ae85fc5f0e00dc04f3b922b2112fc", aum = -2)
    public long fui;
    @C0064Am(aul = "784ad1e8ea2b5f99b69f037800499550", aum = -2)
    public long fuj;
    @C0064Am(aul = "7fe7012a4580b9a033a0db5781c7abf0", aum = -2)
    public long fuk;
    @C0064Am(aul = "e35ae85fc5f0e00dc04f3b922b2112fc", aum = -1)
    public byte ful;
    @C0064Am(aul = "784ad1e8ea2b5f99b69f037800499550", aum = -1)
    public byte fum;
    @C0064Am(aul = "7fe7012a4580b9a033a0db5781c7abf0", aum = -1)
    public byte fun;

    public C6069afp() {
    }

    public C6069afp(C1616Xf xf) {
        super(xf);
    }

    /* renamed from: c */
    public C5663aRz[] mo309c() {
        return ReconWaypointDat._m_fields;
    }

    /* renamed from: d */
    public void mo310d() {
        super.mo310d();
        this.f4471QV = 0.0f;
        this.ddH = 0;
        this.ddJ = null;
        this.ddL = 0;
        this.dPo = 0;
        this.fui = 0;
        this.fuj = 0;
        this.fuk = 0;
        this.dPr = 0;
        this.ful = 0;
        this.fum = 0;
        this.fun = 0;
    }
}
