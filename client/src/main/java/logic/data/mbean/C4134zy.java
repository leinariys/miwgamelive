package logic.data.mbean;

import game.script.avatar.cloth.ClothCategory;
import logic.baa.C1616Xf;
import logic.res.code.C5663aRz;
import p001a.C0064Am;
import taikodom.infra.script.I18NString;

import java.util.UUID;

/* renamed from: a.zy */
public class C4134zy extends C5292aDs {
    @C0064Am(aul = "ff123354afa3896656b487747ad1c92c", aum = -2)

    /* renamed from: Nf */
    public long f9740Nf;
    @C0064Am(aul = "ff123354afa3896656b487747ad1c92c", aum = -1)

    /* renamed from: Nh */
    public byte f9741Nh;
    @C0064Am(aul = "8fc9387dda10d94ba30e1726099636d4", aum = 2)

    /* renamed from: bK */
    public UUID f9742bK;
    @C0064Am(aul = "d911675436c735ecb77e1cca9b118dff", aum = -2)
    public long bZA;
    @C0064Am(aul = "d911675436c735ecb77e1cca9b118dff", aum = -1)
    public byte bZB;
    @C0064Am(aul = "8fc9387dda10d94ba30e1726099636d4", aum = -2)

    /* renamed from: oL */
    public long f9743oL;
    @C0064Am(aul = "8fc9387dda10d94ba30e1726099636d4", aum = -1)

    /* renamed from: oS */
    public byte f9744oS;
    @C0064Am(aul = "ff123354afa3896656b487747ad1c92c", aum = 0)

    /* renamed from: zP */
    public I18NString f9745zP;
    @C0064Am(aul = "d911675436c735ecb77e1cca9b118dff", aum = 1)

    /* renamed from: zR */
    public String f9746zR;

    public C4134zy() {
    }

    public C4134zy(C1616Xf xf) {
        super(xf);
    }

    /* renamed from: c */
    public C5663aRz[] mo309c() {
        return ClothCategory._m_fields;
    }

    /* renamed from: d */
    public void mo310d() {
        super.mo310d();
        this.f9745zP = null;
        this.f9746zR = null;
        this.f9742bK = null;
        this.f9740Nf = 0;
        this.bZA = 0;
        this.f9743oL = 0;
        this.f9741Nh = 0;
        this.bZB = 0;
        this.f9744oS = 0;
    }
}
