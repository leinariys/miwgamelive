package logic.data.mbean;

import game.network.message.serializable.C2686iZ;
import game.script.item.ItemTypeCategory;
import game.script.ship.RestrictedCargoHold;
import logic.baa.C1616Xf;
import logic.res.code.C5663aRz;
import p001a.C0064Am;

/* renamed from: a.abL  reason: case insensitive filesystem */
public class C5831abL extends C6703arz {
    @C0064Am(aul = "9146c0d1e88f27867eae4a6adc1087b7", aum = -2)
    public long eZL;
    @C0064Am(aul = "9146c0d1e88f27867eae4a6adc1087b7", aum = -1)
    public byte eZM;
    @C0064Am(aul = "9146c0d1e88f27867eae4a6adc1087b7", aum = 0)

    /* renamed from: ks */
    public C2686iZ<ItemTypeCategory> f4133ks;

    public C5831abL() {
    }

    public C5831abL(C1616Xf xf) {
        super(xf);
    }

    /* renamed from: c */
    public C5663aRz[] mo309c() {
        return RestrictedCargoHold._m_fields;
    }

    /* renamed from: d */
    public void mo310d() {
        super.mo310d();
        this.f4133ks = null;
        this.eZL = 0;
        this.eZM = 0;
    }
}
