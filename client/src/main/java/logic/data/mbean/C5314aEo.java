package logic.data.mbean;

import game.script.citizenship.inprovements.WageImprovementType;
import logic.baa.C1616Xf;
import logic.res.code.C5663aRz;
import p001a.C0064Am;

/* renamed from: a.aEo  reason: case insensitive filesystem */
public class C5314aEo extends C6071afr {
    @C0064Am(aul = "acea1258a4daf5908bdc95688ce47b0c", aum = 0)
    public long egO;
    @C0064Am(aul = "e52e48b259a15311c2fa08d0c476d126", aum = 1)
    public float egP;
    @C0064Am(aul = "acea1258a4daf5908bdc95688ce47b0c", aum = -2)
    public long egS;
    @C0064Am(aul = "e52e48b259a15311c2fa08d0c476d126", aum = -2)
    public long egT;
    @C0064Am(aul = "acea1258a4daf5908bdc95688ce47b0c", aum = -1)
    public byte egW;
    @C0064Am(aul = "e52e48b259a15311c2fa08d0c476d126", aum = -1)
    public byte egX;

    public C5314aEo() {
    }

    public C5314aEo(C1616Xf xf) {
        super(xf);
    }

    /* renamed from: c */
    public C5663aRz[] mo309c() {
        return WageImprovementType._m_fields;
    }

    /* renamed from: d */
    public void mo310d() {
        super.mo310d();
        this.egO = 0;
        this.egP = 0.0f;
        this.egS = 0;
        this.egT = 0;
        this.egW = 0;
        this.egX = 0;
    }
}
