package logic.data.mbean;

import game.script.ship.categories.Bomber;
import logic.baa.C1616Xf;
import logic.res.code.C5663aRz;
import p001a.C0064Am;

/* renamed from: a.Ck */
public class C0219Ck extends C3976xd {
    @C0064Am(aul = "cab28441ff37553251a5d10496755d38", aum = 0)
    public Bomber cvb;
    @C0064Am(aul = "cab28441ff37553251a5d10496755d38", aum = -2)

    /* renamed from: dl */
    public long f356dl;
    @C0064Am(aul = "cab28441ff37553251a5d10496755d38", aum = -1)

    /* renamed from: ds */
    public byte f357ds;

    public C0219Ck() {
    }

    public C0219Ck(C1616Xf xf) {
        super(xf);
    }

    /* renamed from: c */
    public C5663aRz[] mo309c() {
        return Bomber.BomberDamageEnhancerAdapter._m_fields;
    }

    /* renamed from: d */
    public void mo310d() {
        super.mo310d();
        this.cvb = null;
        this.f356dl = 0;
        this.f357ds = 0;
    }
}
