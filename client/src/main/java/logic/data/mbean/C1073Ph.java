package logic.data.mbean;

import game.script.ai.npc.TurretAIController;
import logic.baa.C1616Xf;
import logic.res.code.C5663aRz;
import p001a.C0064Am;

/* renamed from: a.Ph */
public class C1073Ph extends C6937awc {
    @C0064Am(aul = "76a5c44b292aa01c165d435c1687eb5d", aum = -2)
    public long dPF;
    @C0064Am(aul = "72d9a7c7634891e68aa76261bfd5bdd6", aum = -2)
    public long dPG;
    @C0064Am(aul = "76a5c44b292aa01c165d435c1687eb5d", aum = -1)
    public byte dPH;
    @C0064Am(aul = "72d9a7c7634891e68aa76261bfd5bdd6", aum = -1)
    public byte dPI;
    @C0064Am(aul = "76a5c44b292aa01c165d435c1687eb5d", aum = 0)
    public float dqx;
    @C0064Am(aul = "72d9a7c7634891e68aa76261bfd5bdd6", aum = 1)
    public float dqz;

    public C1073Ph() {
    }

    public C1073Ph(C1616Xf xf) {
        super(xf);
    }

    /* renamed from: c */
    public C5663aRz[] mo309c() {
        return TurretAIController._m_fields;
    }

    /* renamed from: d */
    public void mo310d() {
        super.mo310d();
        this.dqx = 0.0f;
        this.dqz = 0.0f;
        this.dPF = 0;
        this.dPG = 0;
        this.dPH = 0;
        this.dPI = 0;
    }
}
