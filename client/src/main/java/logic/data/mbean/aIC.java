package logic.data.mbean;

import game.script.Character;
import game.script.progression.ProgressionAmplifiers;
import logic.baa.C1616Xf;
import logic.res.code.C5663aRz;
import p001a.C0064Am;

/* renamed from: a.aIC */
public class aIC extends aAL {
    @C0064Am(aul = "a7fae6cd030c1ee38f882ab3d18611a7", aum = 0)
    public Character aKN;
    @C0064Am(aul = "a7fae6cd030c1ee38f882ab3d18611a7", aum = -2)

    /* renamed from: nm */
    public long f3036nm;
    @C0064Am(aul = "a7fae6cd030c1ee38f882ab3d18611a7", aum = -1)

    /* renamed from: np */
    public byte f3037np;

    public aIC() {
    }

    public aIC(C1616Xf xf) {
        super(xf);
    }

    /* renamed from: c */
    public C5663aRz[] mo309c() {
        return ProgressionAmplifiers._m_fields;
    }

    /* renamed from: d */
    public void mo310d() {
        super.mo310d();
        this.aKN = null;
        this.f3036nm = 0;
        this.f3037np = 0;
    }
}
