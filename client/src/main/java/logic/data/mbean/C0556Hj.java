package logic.data.mbean;

import game.network.message.serializable.C1556Wo;
import game.network.message.serializable.C3438ra;
import game.script.corporation.Corporation;
import game.script.ship.*;
import game.script.storage.OutpostStorage;
import logic.baa.C1616Xf;
import logic.res.code.C5663aRz;
import p001a.C0064Am;
import p001a.C0437GE;
import p001a.C2611hZ;
import p001a.aHK;
import taikodom.infra.script.I18NString;

/* renamed from: a.Hj */
public class C0556Hj extends C2947lx {
    @C0064Am(aul = "ad8562474e94ad972d48d4c89a2f2304", aum = 11)
    public C2611hZ ayI;
    @C0064Am(aul = "e424ca71d5a1bc836dd6dc4ab5cf34fb", aum = -2)
    public long bwn;
    @C0064Am(aul = "e424ca71d5a1bc836dd6dc4ab5cf34fb", aum = -1)
    public byte bwt;
    @C0064Am(aul = "ad8562474e94ad972d48d4c89a2f2304", aum = -2)
    public long bxR;
    @C0064Am(aul = "ad8562474e94ad972d48d4c89a2f2304", aum = -1)
    public byte bxU;
    @C0064Am(aul = "2223ed646040ece9599d191a14dc3b09", aum = -2)
    public long cyP;
    @C0064Am(aul = "2223ed646040ece9599d191a14dc3b09", aum = -1)
    public byte czp;
    @C0064Am(aul = "b376464f7e1bc67da442e3c6561b977d", aum = 1)
    public int daP;
    @C0064Am(aul = "42449b86b3d3b379ca0590ce4b8ed8e5", aum = 2)
    public C3438ra<OutpostActivationItem> daQ;
    @C0064Am(aul = "e424ca71d5a1bc836dd6dc4ab5cf34fb", aum = 3)
    public aHK daR;
    @C0064Am(aul = "6b38e6bd970f6c5bec8c21e446fc2c23", aum = 4)
    public I18NString daS;
    @C0064Am(aul = "4e5c58160b995cfd6471fdaebb670412", aum = 5)
    public C1556Wo<C2611hZ, OutpostUpgrade> daT;
    @C0064Am(aul = "bfd5198866eddd8679e17d10b2f09baa", aum = 6)
    public C1556Wo<C2611hZ, OutpostLevelInfo> daU;
    @C0064Am(aul = "e6a48ed6b705f7193ea0417435227c8a", aum = 7)
    public C1556Wo<C0437GE, OutpostLevelFeature> daV;
    @C0064Am(aul = "f7a0e33d599083c2cb09ee1541e9fcc9", aum = 8)
    public Corporation daW;
    @C0064Am(aul = "2223ed646040ece9599d191a14dc3b09", aum = 9)
    public Outpost.C3349b daX;
    @C0064Am(aul = "2e43a187cea8d996d16e1c55cf9dc195", aum = 10)
    public OutpostStorage daY;
    @C0064Am(aul = "aa7b88d348d7a24f621aefbba600fa52", aum = 12)
    public OutpostUpkeepPeriod daZ;
    @C0064Am(aul = "669fdadbce330350fd8bc021e2554107", aum = 0)

    /* renamed from: db */
    public long f673db;
    @C0064Am(aul = "7ccaabf4093d42bf62a12fa7521039a6", aum = 13)
    public Outpost.OutpostUpgradeStatus dba;
    @C0064Am(aul = "b376464f7e1bc67da442e3c6561b977d", aum = -2)
    public long dbb;
    @C0064Am(aul = "42449b86b3d3b379ca0590ce4b8ed8e5", aum = -2)
    public long dbc;
    @C0064Am(aul = "6b38e6bd970f6c5bec8c21e446fc2c23", aum = -2)
    public long dbd;
    @C0064Am(aul = "4e5c58160b995cfd6471fdaebb670412", aum = -2)
    public long dbe;
    @C0064Am(aul = "bfd5198866eddd8679e17d10b2f09baa", aum = -2)
    public long dbf;
    @C0064Am(aul = "e6a48ed6b705f7193ea0417435227c8a", aum = -2)
    public long dbg;
    @C0064Am(aul = "f7a0e33d599083c2cb09ee1541e9fcc9", aum = -2)
    public long dbh;
    @C0064Am(aul = "2e43a187cea8d996d16e1c55cf9dc195", aum = -2)
    public long dbi;
    @C0064Am(aul = "7ccaabf4093d42bf62a12fa7521039a6", aum = -2)
    public long dbj;
    @C0064Am(aul = "b376464f7e1bc67da442e3c6561b977d", aum = -1)
    public byte dbk;
    @C0064Am(aul = "42449b86b3d3b379ca0590ce4b8ed8e5", aum = -1)
    public byte dbl;
    @C0064Am(aul = "6b38e6bd970f6c5bec8c21e446fc2c23", aum = -1)
    public byte dbm;
    @C0064Am(aul = "4e5c58160b995cfd6471fdaebb670412", aum = -1)
    public byte dbn;
    @C0064Am(aul = "bfd5198866eddd8679e17d10b2f09baa", aum = -1)
    public byte dbo;
    @C0064Am(aul = "e6a48ed6b705f7193ea0417435227c8a", aum = -1)
    public byte dbp;
    @C0064Am(aul = "f7a0e33d599083c2cb09ee1541e9fcc9", aum = -1)
    public byte dbq;
    @C0064Am(aul = "2e43a187cea8d996d16e1c55cf9dc195", aum = -1)
    public byte dbr;
    @C0064Am(aul = "7ccaabf4093d42bf62a12fa7521039a6", aum = -1)
    public byte dbs;
    @C0064Am(aul = "c11643b333ba651219dd6fce65d44e11", aum = 14)
    public boolean disabled;
    @C0064Am(aul = "669fdadbce330350fd8bc021e2554107", aum = -2)

    /* renamed from: dj */
    public long f674dj;
    @C0064Am(aul = "669fdadbce330350fd8bc021e2554107", aum = -1)

    /* renamed from: dq */
    public byte f675dq;
    @C0064Am(aul = "aa7b88d348d7a24f621aefbba600fa52", aum = -2)

    /* renamed from: pu */
    public long f676pu;
    @C0064Am(aul = "aa7b88d348d7a24f621aefbba600fa52", aum = -1)

    /* renamed from: px */
    public byte f677px;
    @C0064Am(aul = "c11643b333ba651219dd6fce65d44e11", aum = -1)

    /* renamed from: tF */
    public byte f678tF;
    @C0064Am(aul = "c11643b333ba651219dd6fce65d44e11", aum = -2)

    /* renamed from: tc */
    public long f679tc;

    public C0556Hj() {
    }

    public C0556Hj(C1616Xf xf) {
        super(xf);
    }

    /* renamed from: c */
    public C5663aRz[] mo309c() {
        return Outpost._m_fields;
    }

    /* renamed from: d */
    public void mo310d() {
        super.mo310d();
        this.f673db = 0;
        this.daP = 0;
        this.daQ = null;
        this.daR = null;
        this.daS = null;
        this.daT = null;
        this.daU = null;
        this.daV = null;
        this.daW = null;
        this.daX = null;
        this.daY = null;
        this.ayI = null;
        this.daZ = null;
        this.dba = null;
        this.disabled = false;
        this.f674dj = 0;
        this.dbb = 0;
        this.dbc = 0;
        this.bwn = 0;
        this.dbd = 0;
        this.dbe = 0;
        this.dbf = 0;
        this.dbg = 0;
        this.dbh = 0;
        this.cyP = 0;
        this.dbi = 0;
        this.bxR = 0;
        this.f676pu = 0;
        this.dbj = 0;
        this.f679tc = 0;
        this.f675dq = 0;
        this.dbk = 0;
        this.dbl = 0;
        this.bwt = 0;
        this.dbm = 0;
        this.dbn = 0;
        this.dbo = 0;
        this.dbp = 0;
        this.dbq = 0;
        this.czp = 0;
        this.dbr = 0;
        this.bxU = 0;
        this.f677px = 0;
        this.dbs = 0;
        this.f678tF = 0;
    }
}
