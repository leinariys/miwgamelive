package logic.data.mbean;

import game.script.damage.DamageType;
import game.script.item.buff.module.Damager;
import logic.baa.C1616Xf;
import logic.res.code.C5663aRz;
import p001a.C0064Am;

/* renamed from: a.aGV */
public class aGV extends C4037yS {
    @C0064Am(aul = "6e97a180910a233d22ff7abd6c84e5d8", aum = 6)
    public boolean apZ;
    @C0064Am(aul = "6e97a180910a233d22ff7abd6c84e5d8", aum = -2)
    public long aqe;
    @C0064Am(aul = "6e97a180910a233d22ff7abd6c84e5d8", aum = -1)
    public byte aqj;
    @C0064Am(aul = "b6354d1ce81e16e147f23d90522b8081", aum = 0)
    public float eJl;
    @C0064Am(aul = "872636c9d77ecd5dd2a171ef5e092caf", aum = 1)
    public float eJm;
    @C0064Am(aul = "dd9f3a4b4b3c0e22056a9affbef715a2", aum = 2)
    public DamageType eJn;
    @C0064Am(aul = "8b1ab0ffb2b1e4923e00b96baf284550", aum = 3)
    public DamageType eJo;
    @C0064Am(aul = "dd9f3a4b4b3c0e22056a9affbef715a2", aum = -2)
    public long eJp;
    @C0064Am(aul = "8b1ab0ffb2b1e4923e00b96baf284550", aum = -2)
    public long eJq;
    @C0064Am(aul = "dd9f3a4b4b3c0e22056a9affbef715a2", aum = -1)
    public byte eJr;
    @C0064Am(aul = "8b1ab0ffb2b1e4923e00b96baf284550", aum = -1)
    public byte eJs;
    @C0064Am(aul = "2d118185023278678be81c0937e34fe0", aum = 4)
    public float hTP;
    @C0064Am(aul = "74c56fed51ec69e215696fa87d482017", aum = 5)
    public float hTQ;
    @C0064Am(aul = "2d118185023278678be81c0937e34fe0", aum = -2)
    public long hTR;
    @C0064Am(aul = "74c56fed51ec69e215696fa87d482017", aum = -2)
    public long hTS;
    @C0064Am(aul = "2d118185023278678be81c0937e34fe0", aum = -1)
    public byte hTT;
    @C0064Am(aul = "74c56fed51ec69e215696fa87d482017", aum = -1)
    public byte hTU;
    @C0064Am(aul = "b6354d1ce81e16e147f23d90522b8081", aum = -1)

    /* renamed from: tO */
    public byte f2862tO;
    @C0064Am(aul = "872636c9d77ecd5dd2a171ef5e092caf", aum = -1)

    /* renamed from: tS */
    public byte f2863tS;
    @C0064Am(aul = "b6354d1ce81e16e147f23d90522b8081", aum = -2)

    /* renamed from: tl */
    public long f2864tl;
    @C0064Am(aul = "872636c9d77ecd5dd2a171ef5e092caf", aum = -2)

    /* renamed from: tp */
    public long f2865tp;

    public aGV() {
    }

    public aGV(C1616Xf xf) {
        super(xf);
    }

    /* renamed from: c */
    public C5663aRz[] mo309c() {
        return Damager._m_fields;
    }

    /* renamed from: d */
    public void mo310d() {
        super.mo310d();
        this.eJl = 0.0f;
        this.eJm = 0.0f;
        this.eJn = null;
        this.eJo = null;
        this.hTP = 0.0f;
        this.hTQ = 0.0f;
        this.apZ = false;
        this.f2864tl = 0;
        this.f2865tp = 0;
        this.eJp = 0;
        this.eJq = 0;
        this.hTR = 0;
        this.hTS = 0;
        this.aqe = 0;
        this.f2862tO = 0;
        this.f2863tS = 0;
        this.eJr = 0;
        this.eJs = 0;
        this.hTT = 0;
        this.hTU = 0;
        this.aqj = 0;
    }
}
