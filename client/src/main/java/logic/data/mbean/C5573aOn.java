package logic.data.mbean;

import game.network.message.serializable.C1556Wo;
import game.script.damage.DamageType;
import game.script.item.buff.amplifier.StrikeAmplifier;
import logic.baa.C1616Xf;
import logic.res.code.C5663aRz;
import p001a.C0064Am;
import p001a.C3892wO;

/* renamed from: a.aOn  reason: case insensitive filesystem */
public class C5573aOn extends C3108nv {
    @C0064Am(aul = "b13e9f11de56a0de42fbb3a7b8e8244a", aum = -2)

    /* renamed from: YA */
    public long f3476YA;
    @C0064Am(aul = "b9fb82e0733f92651d6592c7e6c0f353", aum = -2)

    /* renamed from: YB */
    public long f3477YB;
    @C0064Am(aul = "b13e9f11de56a0de42fbb3a7b8e8244a", aum = -1)

    /* renamed from: YG */
    public byte f3478YG;
    @C0064Am(aul = "b9fb82e0733f92651d6592c7e6c0f353", aum = -1)

    /* renamed from: YH */
    public byte f3479YH;
    @C0064Am(aul = "b13e9f11de56a0de42fbb3a7b8e8244a", aum = 1)

    /* renamed from: Yu */
    public C3892wO f3480Yu;
    @C0064Am(aul = "b9fb82e0733f92651d6592c7e6c0f353", aum = 3)

    /* renamed from: Yv */
    public C3892wO f3481Yv;
    @C0064Am(aul = "0bed127b10a73bc20f1a884dfd60a487", aum = 0)
    public C1556Wo<DamageType, C3892wO> atO;
    @C0064Am(aul = "0d65131028104100e8e29fd6849927b7", aum = 2)
    public C3892wO atR;
    @C0064Am(aul = "10ab863d6fc2ad26f5085d0f49562b81", aum = 4)
    public StrikeAmplifier.StrikeAmplifierAdapter atU;
    @C0064Am(aul = "0d65131028104100e8e29fd6849927b7", aum = -2)
    public long cAA;
    @C0064Am(aul = "0d65131028104100e8e29fd6849927b7", aum = -1)
    public byte cAO;
    @C0064Am(aul = "10ab863d6fc2ad26f5085d0f49562b81", aum = -2)
    public long dVm;
    @C0064Am(aul = "10ab863d6fc2ad26f5085d0f49562b81", aum = -1)
    public byte dVn;
    @C0064Am(aul = "0bed127b10a73bc20f1a884dfd60a487", aum = -2)
    public long ide;
    @C0064Am(aul = "0bed127b10a73bc20f1a884dfd60a487", aum = -1)
    public byte idf;

    public C5573aOn() {
    }

    public C5573aOn(C1616Xf xf) {
        super(xf);
    }

    /* renamed from: c */
    public C5663aRz[] mo309c() {
        return StrikeAmplifier._m_fields;
    }

    /* renamed from: d */
    public void mo310d() {
        super.mo310d();
        this.atO = null;
        this.f3480Yu = null;
        this.atR = null;
        this.f3481Yv = null;
        this.atU = null;
        this.ide = 0;
        this.f3476YA = 0;
        this.cAA = 0;
        this.f3477YB = 0;
        this.dVm = 0;
        this.idf = 0;
        this.f3478YG = 0;
        this.cAO = 0;
        this.f3479YH = 0;
        this.dVn = 0;
    }
}
