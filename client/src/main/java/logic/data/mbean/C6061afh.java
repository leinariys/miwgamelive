package logic.data.mbean;

import game.network.message.serializable.C3438ra;
import game.script.npc.NPC;
import game.script.spacezone.population.BossNestPopulationControl;
import game.script.spacezone.population.NPCSpawn;
import game.script.spacezone.population.StaticNPCSpawn;
import logic.baa.C1616Xf;
import logic.res.code.C5663aRz;
import p001a.C0064Am;
import taikodom.infra.script.I18NString;

/* renamed from: a.afh  reason: case insensitive filesystem */
public class C6061afh extends C2693ig {
    @C0064Am(aul = "82c50538e431e97b97d74825b67a5fb3", aum = 1)
    public C3438ra<NPC> axY;
    @C0064Am(aul = "82c50538e431e97b97d74825b67a5fb3", aum = -1)
    public byte ayB;
    @C0064Am(aul = "82c50538e431e97b97d74825b67a5fb3", aum = -2)
    public long aym;
    @C0064Am(aul = "189eb82481b5c7c08e44c8f3830412c7", aum = 14)
    public int dkO;
    @C0064Am(aul = "189eb82481b5c7c08e44c8f3830412c7", aum = -2)
    public long dzB;
    @C0064Am(aul = "189eb82481b5c7c08e44c8f3830412c7", aum = -1)
    public byte dzF;
    @C0064Am(aul = "17001b4dc7a60a92d7a4d9b92df13a68", aum = -2)
    public long ftA;
    @C0064Am(aul = "6aa92e3f570631aea8cc416e510b007d", aum = -2)
    public long ftB;
    @C0064Am(aul = "1c6c040c2cce425d2b2e1530f6eb3929", aum = -2)
    public long ftC;
    @C0064Am(aul = "323c43624ca604d09c43ff80c4f14037", aum = -2)
    public long ftD;
    @C0064Am(aul = "76aa76e087cb6c60099ab8c89edfe91d", aum = -2)
    public long ftE;
    @C0064Am(aul = "d8797396c02c57546ca7a784bca38f2b", aum = -2)
    public long ftF;
    @C0064Am(aul = "00b3faa810c49a355bec883e89e72640", aum = -2)
    public long ftG;
    @C0064Am(aul = "681aaf83bbf604264ef82aabb1f7fb34", aum = -2)
    public long ftH;
    @C0064Am(aul = "e5474363c4e6142c6dd85f3886e61cec", aum = -2)
    public long ftI;
    @C0064Am(aul = "62cd1e169d45b2a6971d81d19f886014", aum = -2)
    public long ftJ;
    @C0064Am(aul = "acafca7c86bc99c785b953068577cab9", aum = -2)
    public long ftK;
    @C0064Am(aul = "9ff1ac21bf6cf8b803683eb22ba3b923", aum = -2)
    public long ftL;
    @C0064Am(aul = "b893a1e1741f076efadec107c6995b77", aum = -1)
    public byte ftM;
    @C0064Am(aul = "17001b4dc7a60a92d7a4d9b92df13a68", aum = -1)
    public byte ftN;
    @C0064Am(aul = "6aa92e3f570631aea8cc416e510b007d", aum = -1)
    public byte ftO;
    @C0064Am(aul = "1c6c040c2cce425d2b2e1530f6eb3929", aum = -1)
    public byte ftP;
    @C0064Am(aul = "323c43624ca604d09c43ff80c4f14037", aum = -1)
    public byte ftQ;
    @C0064Am(aul = "76aa76e087cb6c60099ab8c89edfe91d", aum = -1)
    public byte ftR;
    @C0064Am(aul = "d8797396c02c57546ca7a784bca38f2b", aum = -1)
    public byte ftS;
    @C0064Am(aul = "00b3faa810c49a355bec883e89e72640", aum = -1)
    public byte ftT;
    @C0064Am(aul = "681aaf83bbf604264ef82aabb1f7fb34", aum = -1)
    public byte ftU;
    @C0064Am(aul = "e5474363c4e6142c6dd85f3886e61cec", aum = -1)
    public byte ftV;
    @C0064Am(aul = "62cd1e169d45b2a6971d81d19f886014", aum = -1)
    public byte ftW;
    @C0064Am(aul = "acafca7c86bc99c785b953068577cab9", aum = -1)
    public byte ftX;
    @C0064Am(aul = "9ff1ac21bf6cf8b803683eb22ba3b923", aum = -1)
    public byte ftY;
    @C0064Am(aul = "b893a1e1741f076efadec107c6995b77", aum = 0)
    public C3438ra<NPCSpawn> ftm;
    @C0064Am(aul = "17001b4dc7a60a92d7a4d9b92df13a68", aum = 2)
    public int ftn;
    @C0064Am(aul = "6aa92e3f570631aea8cc416e510b007d", aum = 3)
    public int fto;
    @C0064Am(aul = "1c6c040c2cce425d2b2e1530f6eb3929", aum = 4)
    public int ftp;
    @C0064Am(aul = "323c43624ca604d09c43ff80c4f14037", aum = 5)
    public C3438ra<StaticNPCSpawn> ftq;
    @C0064Am(aul = "76aa76e087cb6c60099ab8c89edfe91d", aum = 6)
    public C3438ra<NPC> ftr;
    @C0064Am(aul = "d8797396c02c57546ca7a784bca38f2b", aum = 7)
    public long fts;
    @C0064Am(aul = "00b3faa810c49a355bec883e89e72640", aum = 8)
    public long ftt;
    @C0064Am(aul = "681aaf83bbf604264ef82aabb1f7fb34", aum = 9)
    public I18NString ftu;
    @C0064Am(aul = "e5474363c4e6142c6dd85f3886e61cec", aum = 10)
    public I18NString ftv;
    @C0064Am(aul = "62cd1e169d45b2a6971d81d19f886014", aum = 11)
    public boolean ftw;
    @C0064Am(aul = "acafca7c86bc99c785b953068577cab9", aum = 12)
    public long ftx;
    @C0064Am(aul = "9ff1ac21bf6cf8b803683eb22ba3b923", aum = 13)
    public long fty;
    @C0064Am(aul = "b893a1e1741f076efadec107c6995b77", aum = -2)
    public long ftz;

    public C6061afh() {
    }

    public C6061afh(C1616Xf xf) {
        super(xf);
    }

    /* renamed from: c */
    public C5663aRz[] mo309c() {
        return BossNestPopulationControl._m_fields;
    }

    /* renamed from: d */
    public void mo310d() {
        super.mo310d();
        this.ftm = null;
        this.axY = null;
        this.ftn = 0;
        this.fto = 0;
        this.ftp = 0;
        this.ftq = null;
        this.ftr = null;
        this.fts = 0;
        this.ftt = 0;
        this.ftu = null;
        this.ftv = null;
        this.ftw = false;
        this.ftx = 0;
        this.fty = 0;
        this.dkO = 0;
        this.ftz = 0;
        this.aym = 0;
        this.ftA = 0;
        this.ftB = 0;
        this.ftC = 0;
        this.ftD = 0;
        this.ftE = 0;
        this.ftF = 0;
        this.ftG = 0;
        this.ftH = 0;
        this.ftI = 0;
        this.ftJ = 0;
        this.ftK = 0;
        this.ftL = 0;
        this.dzB = 0;
        this.ftM = 0;
        this.ayB = 0;
        this.ftN = 0;
        this.ftO = 0;
        this.ftP = 0;
        this.ftQ = 0;
        this.ftR = 0;
        this.ftS = 0;
        this.ftT = 0;
        this.ftU = 0;
        this.ftV = 0;
        this.ftW = 0;
        this.ftX = 0;
        this.ftY = 0;
        this.dzF = 0;
    }
}
