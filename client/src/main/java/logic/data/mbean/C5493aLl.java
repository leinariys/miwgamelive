package logic.data.mbean;

import game.script.progression.MPMultiplier;
import logic.baa.C1616Xf;
import logic.res.code.C5663aRz;
import p001a.C0064Am;
import taikodom.infra.script.I18NString;

import java.util.UUID;

/* renamed from: a.aLl  reason: case insensitive filesystem */
public class C5493aLl extends C3805vD {
    @C0064Am(aul = "dd28f82c249eec40e529fa86496abfa6", aum = 9)

    /* renamed from: bK */
    public UUID f3324bK;
    @C0064Am(aul = "28017181c1df31b18bf64d4ab0a06ee3", aum = 2)
    public float elW;
    @C0064Am(aul = "421590aa1f7735c3c93862fe25ff421c", aum = 3)
    public float elY;
    @C0064Am(aul = "4f8486b3199209e09c4e4000616defed", aum = 4)
    public float ema;
    @C0064Am(aul = "ce94bc728293e88628a8b7d521bf8aea", aum = 5)
    public float emc;
    @C0064Am(aul = "c7777c1c2e0f2f4ecb023c981c3581d9", aum = 6)
    public float eme;
    @C0064Am(aul = "94a18e92d688a4683bd70be278e6544e", aum = 7)
    public float emg;
    @C0064Am(aul = "33f498a071f08c848e9444a2b329cb57", aum = 8)
    public float emi;
    @C0064Am(aul = "fcce95cdcad34bc2376ae6160e29f8cd", aum = 0)
    public String handle;
    @C0064Am(aul = "28017181c1df31b18bf64d4ab0a06ee3", aum = -2)
    public long ikM;
    @C0064Am(aul = "421590aa1f7735c3c93862fe25ff421c", aum = -2)
    public long ikN;
    @C0064Am(aul = "4f8486b3199209e09c4e4000616defed", aum = -2)
    public long ikO;
    @C0064Am(aul = "ce94bc728293e88628a8b7d521bf8aea", aum = -2)
    public long ikP;
    @C0064Am(aul = "c7777c1c2e0f2f4ecb023c981c3581d9", aum = -2)
    public long ikQ;
    @C0064Am(aul = "94a18e92d688a4683bd70be278e6544e", aum = -2)
    public long ikR;
    @C0064Am(aul = "33f498a071f08c848e9444a2b329cb57", aum = -2)
    public long ikS;
    @C0064Am(aul = "28017181c1df31b18bf64d4ab0a06ee3", aum = -1)
    public byte ikT;
    @C0064Am(aul = "421590aa1f7735c3c93862fe25ff421c", aum = -1)
    public byte ikU;
    @C0064Am(aul = "4f8486b3199209e09c4e4000616defed", aum = -1)
    public byte ikV;
    @C0064Am(aul = "ce94bc728293e88628a8b7d521bf8aea", aum = -1)
    public byte ikW;
    @C0064Am(aul = "c7777c1c2e0f2f4ecb023c981c3581d9", aum = -1)
    public byte ikX;
    @C0064Am(aul = "94a18e92d688a4683bd70be278e6544e", aum = -1)
    public byte ikY;
    @C0064Am(aul = "33f498a071f08c848e9444a2b329cb57", aum = -1)
    public byte ikZ;
    @C0064Am(aul = "cb7ff7537c6f7c9d951cbbcc78a6bf22", aum = 1)

    /* renamed from: nh */
    public I18NString f3325nh;
    @C0064Am(aul = "cb7ff7537c6f7c9d951cbbcc78a6bf22", aum = -2)

    /* renamed from: nk */
    public long f3326nk;
    @C0064Am(aul = "cb7ff7537c6f7c9d951cbbcc78a6bf22", aum = -1)

    /* renamed from: nn */
    public byte f3327nn;
    @C0064Am(aul = "dd28f82c249eec40e529fa86496abfa6", aum = -2)

    /* renamed from: oL */
    public long f3328oL;
    @C0064Am(aul = "dd28f82c249eec40e529fa86496abfa6", aum = -1)

    /* renamed from: oS */
    public byte f3329oS;
    @C0064Am(aul = "fcce95cdcad34bc2376ae6160e29f8cd", aum = -2)

    /* renamed from: ok */
    public long f3330ok;
    @C0064Am(aul = "fcce95cdcad34bc2376ae6160e29f8cd", aum = -1)

    /* renamed from: on */
    public byte f3331on;

    public C5493aLl() {
    }

    public C5493aLl(C1616Xf xf) {
        super(xf);
    }

    /* renamed from: c */
    public C5663aRz[] mo309c() {
        return MPMultiplier._m_fields;
    }

    /* renamed from: d */
    public void mo310d() {
        super.mo310d();
        this.handle = null;
        this.f3325nh = null;
        this.elW = 0.0f;
        this.elY = 0.0f;
        this.ema = 0.0f;
        this.emc = 0.0f;
        this.eme = 0.0f;
        this.emg = 0.0f;
        this.emi = 0.0f;
        this.f3324bK = null;
        this.f3330ok = 0;
        this.f3326nk = 0;
        this.ikM = 0;
        this.ikN = 0;
        this.ikO = 0;
        this.ikP = 0;
        this.ikQ = 0;
        this.ikR = 0;
        this.ikS = 0;
        this.f3328oL = 0;
        this.f3331on = 0;
        this.f3327nn = 0;
        this.ikT = 0;
        this.ikU = 0;
        this.ikV = 0;
        this.ikW = 0;
        this.ikX = 0;
        this.ikY = 0;
        this.ikZ = 0;
        this.f3329oS = 0;
    }
}
