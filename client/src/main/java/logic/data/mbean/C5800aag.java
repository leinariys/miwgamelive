package logic.data.mbean;

import game.script.mission.scripting.ItemDeployMissionScript;
import logic.baa.C1616Xf;
import logic.res.code.C5663aRz;
import p001a.C0064Am;

/* renamed from: a.aag  reason: case insensitive filesystem */
public class C5800aag extends C2955mD {
    @C0064Am(aul = "7be14e8df4ebe13620ffca8d1b2535e5", aum = -2)

    /* renamed from: dl */
    public long f4086dl;
    @C0064Am(aul = "7be14e8df4ebe13620ffca8d1b2535e5", aum = -1)

    /* renamed from: ds */
    public byte f4087ds;
    @C0064Am(aul = "7be14e8df4ebe13620ffca8d1b2535e5", aum = 0)
    public ItemDeployMissionScript eSt;

    public C5800aag() {
    }

    public C5800aag(C1616Xf xf) {
        super(xf);
    }

    /* renamed from: c */
    public C5663aRz[] mo309c() {
        return ItemDeployMissionScript.StateA._m_fields;
    }

    /* renamed from: d */
    public void mo310d() {
        super.mo310d();
        this.eSt = null;
        this.f4086dl = 0;
        this.f4087ds = 0;
    }
}
