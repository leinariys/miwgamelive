package logic.data.mbean;

import game.script.mission.MissionBeaconType;
import game.script.resource.Asset;
import logic.baa.C1616Xf;
import logic.res.code.C5663aRz;
import p001a.C0064Am;
import taikodom.infra.script.I18NString;

import java.util.UUID;

/* renamed from: a.aAN */
public class aAN extends C5292aDs {
    @C0064Am(aul = "0286726eb5931dbc54b2e4dc0c04e327", aum = 1)

    /* renamed from: LQ */
    public Asset f2314LQ;
    @C0064Am(aul = "0286726eb5931dbc54b2e4dc0c04e327", aum = -2)
    public long ayc;
    @C0064Am(aul = "0286726eb5931dbc54b2e4dc0c04e327", aum = -1)
    public byte ayr;
    @C0064Am(aul = "1d5075f9b1b829fec6459a455c78ffa3", aum = 3)

    /* renamed from: bK */
    public UUID f2315bK;
    @C0064Am(aul = "489767550f2764b1d083451d7bcfc517", aum = 2)
    public I18NString grv;
    @C0064Am(aul = "489767550f2764b1d083451d7bcfc517", aum = -2)
    public long grw;
    @C0064Am(aul = "489767550f2764b1d083451d7bcfc517", aum = -1)
    public byte grx;
    @C0064Am(aul = "1468da888231ac76b37eaf059eb98a0b", aum = 0)
    public String handle;
    @C0064Am(aul = "1d5075f9b1b829fec6459a455c78ffa3", aum = -2)

    /* renamed from: oL */
    public long f2316oL;
    @C0064Am(aul = "1d5075f9b1b829fec6459a455c78ffa3", aum = -1)

    /* renamed from: oS */
    public byte f2317oS;
    @C0064Am(aul = "1468da888231ac76b37eaf059eb98a0b", aum = -2)

    /* renamed from: ok */
    public long f2318ok;
    @C0064Am(aul = "1468da888231ac76b37eaf059eb98a0b", aum = -1)

    /* renamed from: on */
    public byte f2319on;

    public aAN() {
    }

    public aAN(C1616Xf xf) {
        super(xf);
    }

    /* renamed from: c */
    public C5663aRz[] mo309c() {
        return MissionBeaconType._m_fields;
    }

    /* renamed from: d */
    public void mo310d() {
        super.mo310d();
        this.handle = null;
        this.f2314LQ = null;
        this.grv = null;
        this.f2315bK = null;
        this.f2318ok = 0;
        this.ayc = 0;
        this.grw = 0;
        this.f2316oL = 0;
        this.f2319on = 0;
        this.ayr = 0;
        this.grx = 0;
        this.f2317oS = 0;
    }
}
