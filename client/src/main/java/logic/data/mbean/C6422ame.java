package logic.data.mbean;

import game.network.message.serializable.C1556Wo;
import game.script.nls.TranslationNode;
import logic.baa.C1616Xf;
import logic.res.code.C5663aRz;
import p001a.C0064Am;
import taikodom.infra.script.I18NString;

import java.util.UUID;

/* renamed from: a.ame  reason: case insensitive filesystem */
public class C6422ame extends C5292aDs {
    @C0064Am(aul = "180ed3adf86117f3e475b294a9b6859a", aum = 0)
    public C1556Wo<String, I18NString> bJL;
    @C0064Am(aul = "cef4f206ca3ed48615a80517c9ceee84", aum = 1)
    public C1556Wo<String, String> bJN;
    @C0064Am(aul = "35bd14636a6daf7df027a543948c82ab", aum = 2)
    public String bJP;
    @C0064Am(aul = "beaf1c09251fe7a9bd90ffef5eb4f643", aum = 3)

    /* renamed from: bK */
    public UUID f4921bK;
    @C0064Am(aul = "180ed3adf86117f3e475b294a9b6859a", aum = -2)
    public long fZH;
    @C0064Am(aul = "cef4f206ca3ed48615a80517c9ceee84", aum = -2)
    public long fZI;
    @C0064Am(aul = "35bd14636a6daf7df027a543948c82ab", aum = -2)
    public long fZJ;
    @C0064Am(aul = "180ed3adf86117f3e475b294a9b6859a", aum = -1)
    public byte fZK;
    @C0064Am(aul = "cef4f206ca3ed48615a80517c9ceee84", aum = -1)
    public byte fZL;
    @C0064Am(aul = "35bd14636a6daf7df027a543948c82ab", aum = -1)
    public byte fZM;
    @C0064Am(aul = "beaf1c09251fe7a9bd90ffef5eb4f643", aum = -2)

    /* renamed from: oL */
    public long f4922oL;
    @C0064Am(aul = "beaf1c09251fe7a9bd90ffef5eb4f643", aum = -1)

    /* renamed from: oS */
    public byte f4923oS;

    public C6422ame() {
    }

    public C6422ame(C1616Xf xf) {
        super(xf);
    }

    /* renamed from: c */
    public C5663aRz[] mo309c() {
        return TranslationNode._m_fields;
    }

    /* renamed from: d */
    public void mo310d() {
        super.mo310d();
        this.bJL = null;
        this.bJN = null;
        this.bJP = null;
        this.f4921bK = null;
        this.fZH = 0;
        this.fZI = 0;
        this.fZJ = 0;
        this.f4922oL = 0;
        this.fZK = 0;
        this.fZL = 0;
        this.fZM = 0;
        this.f4923oS = 0;
    }
}
