package logic.data.mbean;

import game.script.npcchat.actions.SetParamSpeechAction;
import logic.baa.C1616Xf;
import logic.res.code.C5663aRz;
import p001a.C0064Am;

/* renamed from: a.VU */
public class C1460VU extends C6352alM {
    @C0064Am(aul = "8153c0de1705ca0b9541870d1bd43005", aum = -2)

    /* renamed from: IC */
    public long f1871IC;
    @C0064Am(aul = "36c98b541a2ca80bfbf386380ed17545", aum = -2)

    /* renamed from: ID */
    public long f1872ID;
    @C0064Am(aul = "8153c0de1705ca0b9541870d1bd43005", aum = -1)

    /* renamed from: IE */
    public byte f1873IE;
    @C0064Am(aul = "36c98b541a2ca80bfbf386380ed17545", aum = -1)

    /* renamed from: IG */
    public byte f1874IG;
    @C0064Am(aul = "8153c0de1705ca0b9541870d1bd43005", aum = 0)
    public String key;
    @C0064Am(aul = "36c98b541a2ca80bfbf386380ed17545", aum = 1)
    public String value;

    public C1460VU() {
    }

    public C1460VU(C1616Xf xf) {
        super(xf);
    }

    /* renamed from: c */
    public C5663aRz[] mo309c() {
        return SetParamSpeechAction._m_fields;
    }

    /* renamed from: d */
    public void mo310d() {
        super.mo310d();
        this.key = null;
        this.value = null;
        this.f1871IC = 0;
        this.f1872ID = 0;
        this.f1873IE = 0;
        this.f1874IG = 0;
    }
}
