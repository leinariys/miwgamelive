package logic.data.mbean;

import game.script.ship.categories.Explorer;
import logic.baa.C1616Xf;
import logic.res.code.C5663aRz;
import p001a.C0064Am;

/* renamed from: a.Dd */
public class C0282Dd extends C6757atB {
    @C0064Am(aul = "d753c4e09bbba2c552f57d3e8296ca97", aum = 0)

    /* renamed from: OD */
    public Explorer f423OD;
    @C0064Am(aul = "d753c4e09bbba2c552f57d3e8296ca97", aum = -2)

    /* renamed from: dl */
    public long f424dl;
    @C0064Am(aul = "d753c4e09bbba2c552f57d3e8296ca97", aum = -1)

    /* renamed from: ds */
    public byte f425ds;

    public C0282Dd() {
    }

    public C0282Dd(C1616Xf xf) {
        super(xf);
    }

    /* renamed from: c */
    public C5663aRz[] mo309c() {
        return Explorer.InvulnerabilityEnterer._m_fields;
    }

    /* renamed from: d */
    public void mo310d() {
        super.mo310d();
        this.f423OD = null;
        this.f424dl = 0;
        this.f425ds = 0;
    }
}
