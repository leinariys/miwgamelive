package logic.data.mbean;

import game.network.message.serializable.C1556Wo;
import game.script.damage.DamageType;
import game.script.ship.ShieldType;
import logic.baa.C1616Xf;
import logic.res.code.C5663aRz;
import p001a.C0064Am;
import p001a.C1649YI;

/* renamed from: a.aze  reason: case insensitive filesystem */
public class C7017aze extends C5868abw {
    @C0064Am(aul = "adfa61408e5a15fc39c27ecc3b264f8f", aum = -2)
    public long aBP;
    @C0064Am(aul = "5b94964da3f1701a06571b4b08d4b12e", aum = -2)
    public long aBQ;
    @C0064Am(aul = "6f859584baff3b559afe4d5900292f72", aum = -2)
    public long aBR;
    @C0064Am(aul = "adfa61408e5a15fc39c27ecc3b264f8f", aum = -1)
    public byte aBU;
    @C0064Am(aul = "5b94964da3f1701a06571b4b08d4b12e", aum = -1)
    public byte aBV;
    @C0064Am(aul = "6f859584baff3b559afe4d5900292f72", aum = -1)
    public byte aBW;
    @C0064Am(aul = "23202c729d1b4f1a7800cedcf32abe60", aum = 0)
    public float aOo;
    @C0064Am(aul = "adfa61408e5a15fc39c27ecc3b264f8f", aum = 1)
    public float aOq;
    @C0064Am(aul = "6f859584baff3b559afe4d5900292f72", aum = 2)
    public float aOs;
    @C0064Am(aul = "db40ad939d4389bfe3aef3610634854e", aum = 3)
    public C1556Wo<DamageType, C1649YI> aOu;
    @C0064Am(aul = "5b94964da3f1701a06571b4b08d4b12e", aum = 4)
    public int aOy;
    @C0064Am(aul = "db40ad939d4389bfe3aef3610634854e", aum = -2)
    public long eVK;
    @C0064Am(aul = "23202c729d1b4f1a7800cedcf32abe60", aum = -2)
    public long eVM;
    @C0064Am(aul = "db40ad939d4389bfe3aef3610634854e", aum = -1)
    public byte eVP;
    @C0064Am(aul = "23202c729d1b4f1a7800cedcf32abe60", aum = -1)
    public byte eVR;

    public C7017aze() {
    }

    public C7017aze(C1616Xf xf) {
        super(xf);
    }

    /* renamed from: c */
    public C5663aRz[] mo309c() {
        return ShieldType._m_fields;
    }

    /* renamed from: d */
    public void mo310d() {
        super.mo310d();
        this.aOo = 0.0f;
        this.aOq = 0.0f;
        this.aOs = 0.0f;
        this.aOu = null;
        this.aOy = 0;
        this.eVM = 0;
        this.aBP = 0;
        this.aBR = 0;
        this.eVK = 0;
        this.aBQ = 0;
        this.eVR = 0;
        this.aBU = 0;
        this.aBW = 0;
        this.eVP = 0;
        this.aBV = 0;
    }
}
