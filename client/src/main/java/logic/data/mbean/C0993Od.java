package logic.data.mbean;

import game.network.message.serializable.C3438ra;
import logic.baa.C1616Xf;
import logic.res.code.C5663aRz;
import p001a.C0064Am;

/* renamed from: a.Od */
public class C0993Od extends C6515aoT {
    @C0064Am(aul = "dbc8f8c4475dc2a3c2b8e66efd4a05d1", aum = 0)
    public boolean big;
    @C0064Am(aul = "807ae4df47ef3636a1e1982524da3536", aum = 1)
    public boolean dIR;
    @C0064Am(aul = "d321f98357c9ca6f43122a5094cf1ab4", aum = 2)
    public C3438ra<TutorialPage> dIS;
    @C0064Am(aul = "7abb26e95668a5cce74eba80dc58fe57", aum = 3)
    public C3438ra<Rule> dIT;
    @C0064Am(aul = "91d27c9f50f46dc1f904886e4d586832", aum = 4)
    public TutorialPage dIU;
    @C0064Am(aul = "761f9e92fc94eb1dfc4a414528b6c70d", aum = 5)
    public TutorialPage dIV;
    @C0064Am(aul = "dbc8f8c4475dc2a3c2b8e66efd4a05d1", aum = -2)
    public long dIW;
    @C0064Am(aul = "807ae4df47ef3636a1e1982524da3536", aum = -2)
    public long dIX;
    @C0064Am(aul = "d321f98357c9ca6f43122a5094cf1ab4", aum = -2)
    public long dIY;
    @C0064Am(aul = "7abb26e95668a5cce74eba80dc58fe57", aum = -2)
    public long dIZ;
    @C0064Am(aul = "91d27c9f50f46dc1f904886e4d586832", aum = -2)
    public long dJa;
    @C0064Am(aul = "761f9e92fc94eb1dfc4a414528b6c70d", aum = -2)
    public long dJb;
    @C0064Am(aul = "dbc8f8c4475dc2a3c2b8e66efd4a05d1", aum = -1)
    public byte dJc;
    @C0064Am(aul = "807ae4df47ef3636a1e1982524da3536", aum = -1)
    public byte dJd;
    @C0064Am(aul = "d321f98357c9ca6f43122a5094cf1ab4", aum = -1)
    public byte dJe;
    @C0064Am(aul = "7abb26e95668a5cce74eba80dc58fe57", aum = -1)
    public byte dJf;
    @C0064Am(aul = "91d27c9f50f46dc1f904886e4d586832", aum = -1)
    public byte dJg;
    @C0064Am(aul = "761f9e92fc94eb1dfc4a414528b6c70d", aum = -1)
    public byte dJh;

    public C0993Od() {
    }

    public C0993Od(C1616Xf xf) {
        super(xf);
    }

    /* renamed from: c */
    public C5663aRz[] mo309c() {
        return Tutorial._m_fields;
    }

    /* renamed from: d */
    public void mo310d() {
        super.mo310d();
        this.big = false;
        this.dIR = false;
        this.dIS = null;
        this.dIT = null;
        this.dIU = null;
        this.dIV = null;
        this.dIW = 0;
        this.dIX = 0;
        this.dIY = 0;
        this.dIZ = 0;
        this.dJa = 0;
        this.dJb = 0;
        this.dJc = 0;
        this.dJd = 0;
        this.dJe = 0;
        this.dJf = 0;
        this.dJg = 0;
        this.dJh = 0;
    }
}
