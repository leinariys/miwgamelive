package logic.data.mbean;

import game.network.message.serializable.C3438ra;
import game.script.billing.Billing;
import game.script.billing.BillingTransaction;
import game.script.player.User;
import logic.baa.C1616Xf;
import logic.res.code.C5663aRz;
import p001a.C0064Am;

/* renamed from: a.apR  reason: case insensitive filesystem */
public class C6565apR extends C3805vD {
    @C0064Am(aul = "a4e3f3ea2fef7b6573775b0d2d7fbe6c", aum = 2)
    public C3438ra<BillingTransaction> dfm;
    @C0064Am(aul = "a4e3f3ea2fef7b6573775b0d2d7fbe6c", aum = -2)
    public long eNf;
    @C0064Am(aul = "a4e3f3ea2fef7b6573775b0d2d7fbe6c", aum = -1)
    public byte eNh;
    @C0064Am(aul = "7c4d1075b6c8f52bf73bb34595155e30", aum = 0)
    public long fap;
    @C0064Am(aul = "64d5084565e2d0757b5a45c400cce698", aum = 1)
    public User far;
    @C0064Am(aul = "7c4d1075b6c8f52bf73bb34595155e30", aum = -2)
    public long goc;
    @C0064Am(aul = "7c4d1075b6c8f52bf73bb34595155e30", aum = -1)
    public byte god;
    @C0064Am(aul = "64d5084565e2d0757b5a45c400cce698", aum = -2)

    /* renamed from: nm */
    public long f5099nm;
    @C0064Am(aul = "64d5084565e2d0757b5a45c400cce698", aum = -1)

    /* renamed from: np */
    public byte f5100np;

    public C6565apR() {
    }

    public C6565apR(C1616Xf xf) {
        super(xf);
    }

    /* renamed from: c */
    public C5663aRz[] mo309c() {
        return Billing._m_fields;
    }

    /* renamed from: d */
    public void mo310d() {
        super.mo310d();
        this.fap = 0;
        this.far = null;
        this.dfm = null;
        this.goc = 0;
        this.f5099nm = 0;
        this.eNf = 0;
        this.god = 0;
        this.f5100np = 0;
        this.eNh = 0;
    }
}
