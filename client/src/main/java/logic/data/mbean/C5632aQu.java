package logic.data.mbean;

import game.script.mission.scripting.TalkToNPCMissionScript;
import logic.baa.C1616Xf;
import logic.res.code.C5663aRz;
import p001a.C0064Am;

/* renamed from: a.aQu  reason: case insensitive filesystem */
public class C5632aQu extends C2955mD {
    @C0064Am(aul = "8266c01c29065807cf263f604f7526fa", aum = -2)

    /* renamed from: dl */
    public long f3674dl;
    @C0064Am(aul = "8266c01c29065807cf263f604f7526fa", aum = -1)

    /* renamed from: ds */
    public byte f3675ds;
    @C0064Am(aul = "8266c01c29065807cf263f604f7526fa", aum = 0)
    public TalkToNPCMissionScript eJR;

    public C5632aQu() {
    }

    public C5632aQu(C1616Xf xf) {
        super(xf);
    }

    /* renamed from: c */
    public C5663aRz[] mo309c() {
        return TalkToNPCMissionScript.StateA._m_fields;
    }

    /* renamed from: d */
    public void mo310d() {
        super.mo310d();
        this.eJR = null;
        this.f3674dl = 0;
        this.f3675ds = 0;
    }
}
