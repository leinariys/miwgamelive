package logic.data.mbean;

import game.network.message.serializable.C3438ra;
import game.script.faction.Faction;
import game.script.npcchat.requirement.IsNotFromFactionSpeechRequirement;
import logic.baa.C1616Xf;
import logic.res.code.C5663aRz;
import p001a.C0064Am;

/* renamed from: a.aIM */
public class aIM extends C5416aIm {
    @C0064Am(aul = "eb86d482d713166264323c7ae218894b", aum = 0)
    public C3438ra<Faction> aQm;
    @C0064Am(aul = "eb86d482d713166264323c7ae218894b", aum = -2)
    public long idg;
    @C0064Am(aul = "eb86d482d713166264323c7ae218894b", aum = -1)
    public byte idh;

    public aIM() {
    }

    public aIM(C1616Xf xf) {
        super(xf);
    }

    /* renamed from: c */
    public C5663aRz[] mo309c() {
        return IsNotFromFactionSpeechRequirement._m_fields;
    }

    /* renamed from: d */
    public void mo310d() {
        super.mo310d();
        this.aQm = null;
        this.idg = 0;
        this.idh = 0;
    }
}
