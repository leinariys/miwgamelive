package logic.data.mbean;

import game.script.corporation.CorporationMembership;
import game.script.player.Player;
import logic.baa.C1616Xf;
import logic.res.code.C5663aRz;
import p001a.C0064Am;
import p001a.C5741aUz;

/* renamed from: a.apG  reason: case insensitive filesystem */
public class C6554apG extends C3805vD {
    @C0064Am(aul = "57571eaa14447ecffef20b61fc314b5b", aum = 0)

    /* renamed from: P */
    public Player f5085P;
    @C0064Am(aul = "57571eaa14447ecffef20b61fc314b5b", aum = -2)

    /* renamed from: ag */
    public long f5086ag;
    @C0064Am(aul = "57571eaa14447ecffef20b61fc314b5b", aum = -1)

    /* renamed from: av */
    public byte f5087av;
    @C0064Am(aul = "37d825ae74ec7d49350f71790137b87e", aum = -1)
    public byte dhC;
    @C0064Am(aul = "37d825ae74ec7d49350f71790137b87e", aum = 4)
    public C5741aUz dhu;
    @C0064Am(aul = "37d825ae74ec7d49350f71790137b87e", aum = -2)
    public long dhy;
    @C0064Am(aul = "810363ac49c268a238ca2591fdcd0388", aum = 1)
    public long gmS;
    @C0064Am(aul = "83359e7e7e49ec8468a4e93bf199e878", aum = 2)
    public long gmT;
    @C0064Am(aul = "4c32f293751a8a05aab7db31d5453762", aum = 3)
    public long gmU;
    @C0064Am(aul = "810363ac49c268a238ca2591fdcd0388", aum = -2)
    public long gmV;
    @C0064Am(aul = "83359e7e7e49ec8468a4e93bf199e878", aum = -2)
    public long gmW;
    @C0064Am(aul = "4c32f293751a8a05aab7db31d5453762", aum = -2)
    public long gmX;
    @C0064Am(aul = "810363ac49c268a238ca2591fdcd0388", aum = -1)
    public byte gmY;
    @C0064Am(aul = "83359e7e7e49ec8468a4e93bf199e878", aum = -1)
    public byte gmZ;
    @C0064Am(aul = "4c32f293751a8a05aab7db31d5453762", aum = -1)
    public byte gna;

    public C6554apG() {
    }

    public C6554apG(C1616Xf xf) {
        super(xf);
    }

    /* renamed from: c */
    public C5663aRz[] mo309c() {
        return CorporationMembership._m_fields;
    }

    /* renamed from: d */
    public void mo310d() {
        super.mo310d();
        this.f5085P = null;
        this.gmS = 0;
        this.gmT = 0;
        this.gmU = 0;
        this.dhu = null;
        this.f5086ag = 0;
        this.gmV = 0;
        this.gmW = 0;
        this.gmX = 0;
        this.dhy = 0;
        this.f5087av = 0;
        this.gmY = 0;
        this.gmZ = 0;
        this.gna = 0;
        this.dhC = 0;
    }
}
