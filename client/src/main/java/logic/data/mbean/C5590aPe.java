package logic.data.mbean;

import game.script.item.ItemType;
import game.script.npcchat.requirement.HasItemSpeechRequirement;
import logic.baa.C1616Xf;
import logic.res.code.C5663aRz;
import p001a.C0064Am;

/* renamed from: a.aPe  reason: case insensitive filesystem */
public class C5590aPe extends C5416aIm {
    @C0064Am(aul = "822017a74782d5dba37554e54508aa3a", aum = -2)
    public long aLG;
    @C0064Am(aul = "822017a74782d5dba37554e54508aa3a", aum = -1)
    public byte aLL;
    @C0064Am(aul = "a2924b2f9bbb2574f9617c911f629bfc", aum = -2)
    public long bmV;
    @C0064Am(aul = "a2924b2f9bbb2574f9617c911f629bfc", aum = -1)
    public byte bmW;
    @C0064Am(aul = "a2924b2f9bbb2574f9617c911f629bfc", aum = 0)

    /* renamed from: cw */
    public ItemType f3541cw;
    @C0064Am(aul = "822017a74782d5dba37554e54508aa3a", aum = 1)

    /* renamed from: cy */
    public int f3542cy;

    public C5590aPe() {
    }

    public C5590aPe(C1616Xf xf) {
        super(xf);
    }

    /* renamed from: c */
    public C5663aRz[] mo309c() {
        return HasItemSpeechRequirement._m_fields;
    }

    /* renamed from: d */
    public void mo310d() {
        super.mo310d();
        this.f3541cw = null;
        this.f3542cy = 0;
        this.bmV = 0;
        this.aLG = 0;
        this.bmW = 0;
        this.aLL = 0;
    }
}
