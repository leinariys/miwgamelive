package logic.data.mbean;

import game.script.item.Module;
import game.script.item.buff.module.AttributeBuffType;
import logic.baa.C1616Xf;
import logic.res.code.C5663aRz;
import p001a.C0064Am;

/* renamed from: a.aRx  reason: case insensitive filesystem */
public class C5661aRx extends C6515aoT {
    @C0064Am(aul = "b0e9361d5c13d6cd9d4a7599e92d57eb", aum = -2)
    public long bMD;
    @C0064Am(aul = "b0e9361d5c13d6cd9d4a7599e92d57eb", aum = -1)
    public byte bMG;
    @C0064Am(aul = "b0e9361d5c13d6cd9d4a7599e92d57eb", aum = 0)
    public Module.C4010b bMz;

    public C5661aRx() {
    }

    public C5661aRx(C1616Xf xf) {
        super(xf);
    }

    /* renamed from: c */
    public C5663aRz[] mo309c() {
        return AttributeBuffType._m_fields;
    }

    /* renamed from: d */
    public void mo310d() {
        super.mo310d();
        this.bMz = null;
        this.bMD = 0;
        this.bMG = 0;
    }
}
