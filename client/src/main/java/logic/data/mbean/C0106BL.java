package logic.data.mbean;

import game.script.ship.strike.BaseStrikeType;
import logic.baa.C1616Xf;
import logic.res.code.C5663aRz;
import p001a.C0064Am;

import java.util.UUID;

/* renamed from: a.BL */
public class C0106BL extends C6602aqC {
    @C0064Am(aul = "d84e89d3f7277d1eac4c21c7838b2d60", aum = 0)

    /* renamed from: bK */
    public UUID f194bK;
    @C0064Am(aul = "8f9bb3ad8d7c85af52aa79ff38c8776e", aum = 1)
    public String handle;
    @C0064Am(aul = "d84e89d3f7277d1eac4c21c7838b2d60", aum = -2)

    /* renamed from: oL */
    public long f195oL;
    @C0064Am(aul = "d84e89d3f7277d1eac4c21c7838b2d60", aum = -1)

    /* renamed from: oS */
    public byte f196oS;
    @C0064Am(aul = "8f9bb3ad8d7c85af52aa79ff38c8776e", aum = -2)

    /* renamed from: ok */
    public long f197ok;
    @C0064Am(aul = "8f9bb3ad8d7c85af52aa79ff38c8776e", aum = -1)

    /* renamed from: on */
    public byte f198on;

    public C0106BL() {
    }

    public C0106BL(C1616Xf xf) {
        super(xf);
    }

    /* renamed from: c */
    public C5663aRz[] mo309c() {
        return BaseStrikeType._m_fields;
    }

    /* renamed from: d */
    public void mo310d() {
        super.mo310d();
        this.f194bK = null;
        this.handle = null;
        this.f195oL = 0;
        this.f197ok = 0;
        this.f196oS = 0;
        this.f198on = 0;
    }
}
