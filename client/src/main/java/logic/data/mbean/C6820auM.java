package logic.data.mbean;

import game.network.message.serializable.C2686iZ;
import game.script.damage.DamageType;
import game.script.resource.Asset;
import game.script.ship.hazardshield.HazardShieldType;
import logic.baa.C1616Xf;
import logic.res.code.C5663aRz;
import p001a.C0064Am;
import p001a.C3892wO;

/* renamed from: a.auM  reason: case insensitive filesystem */
public class C6820auM extends C6515aoT {
    @C0064Am(aul = "f6b6a8ef3f822342644e2432b9f83038", aum = -2)

    /* renamed from: YA */
    public long f5372YA;
    @C0064Am(aul = "756c430d1b7d44aaadc5c7b45ada849f", aum = -2)

    /* renamed from: YB */
    public long f5373YB;
    @C0064Am(aul = "f6b6a8ef3f822342644e2432b9f83038", aum = -1)

    /* renamed from: YG */
    public byte f5374YG;
    @C0064Am(aul = "756c430d1b7d44aaadc5c7b45ada849f", aum = -1)

    /* renamed from: YH */
    public byte f5375YH;
    @C0064Am(aul = "fb14eaefa814fbebc3be0cb3edb43828", aum = 4)
    public Asset aAd;
    @C0064Am(aul = "3210178b90c2c5a32e7fc72acb807547", aum = 5)
    public Asset aAf;
    @C0064Am(aul = "0bd84fcaa6e27fff66e26a498a301b74", aum = 0)
    public C3892wO bbH;
    @C0064Am(aul = "443026e338dc1b3e39c53c3a0fa9355d", aum = 1)
    public C2686iZ<DamageType> bbJ;
    @C0064Am(aul = "756c430d1b7d44aaadc5c7b45ada849f", aum = 2)
    public float bbL;
    @C0064Am(aul = "f6b6a8ef3f822342644e2432b9f83038", aum = 3)
    public float bbM;
    @C0064Am(aul = "fb14eaefa814fbebc3be0cb3edb43828", aum = -2)
    public long cAB;
    @C0064Am(aul = "3210178b90c2c5a32e7fc72acb807547", aum = -2)
    public long cAC;
    @C0064Am(aul = "fb14eaefa814fbebc3be0cb3edb43828", aum = -1)
    public byte cAP;
    @C0064Am(aul = "3210178b90c2c5a32e7fc72acb807547", aum = -1)
    public byte cAQ;
    @C0064Am(aul = "0bd84fcaa6e27fff66e26a498a301b74", aum = -2)
    public long gEW;
    @C0064Am(aul = "443026e338dc1b3e39c53c3a0fa9355d", aum = -2)
    public long gEX;
    @C0064Am(aul = "0bd84fcaa6e27fff66e26a498a301b74", aum = -1)
    public byte gEY;
    @C0064Am(aul = "443026e338dc1b3e39c53c3a0fa9355d", aum = -1)
    public byte gEZ;

    public C6820auM() {
    }

    public C6820auM(C1616Xf xf) {
        super(xf);
    }

    /* renamed from: c */
    public C5663aRz[] mo309c() {
        return HazardShieldType._m_fields;
    }

    /* renamed from: d */
    public void mo310d() {
        super.mo310d();
        this.bbH = null;
        this.bbJ = null;
        this.bbL = 0.0f;
        this.bbM = 0.0f;
        this.aAd = null;
        this.aAf = null;
        this.gEW = 0;
        this.gEX = 0;
        this.f5373YB = 0;
        this.f5372YA = 0;
        this.cAB = 0;
        this.cAC = 0;
        this.gEY = 0;
        this.gEZ = 0;
        this.f5375YH = 0;
        this.f5374YG = 0;
        this.cAP = 0;
        this.cAQ = 0;
    }
}
