package logic.data.mbean;

import game.script.ai.npc.AIController;
import logic.baa.C1616Xf;
import logic.res.code.C5663aRz;
import p001a.C0064Am;

/* renamed from: a.awc  reason: case insensitive filesystem */
public class C6937awc extends C2272dV {
    @C0064Am(aul = "a3ea55cccc8e169f0f68073350ccda41", aum = 0)
    public boolean enabled;
    @C0064Am(aul = "a3ea55cccc8e169f0f68073350ccda41", aum = -2)

    /* renamed from: jC */
    public long f5520jC;
    @C0064Am(aul = "a3ea55cccc8e169f0f68073350ccda41", aum = -1)

    /* renamed from: jO */
    public byte f5521jO;

    public C6937awc() {
    }

    public C6937awc(C1616Xf xf) {
        super(xf);
    }

    /* renamed from: c */
    public C5663aRz[] mo309c() {
        return AIController._m_fields;
    }

    /* renamed from: d */
    public void mo310d() {
        super.mo310d();
        this.enabled = false;
        this.f5520jC = 0;
        this.f5521jO = 0;
    }
}
