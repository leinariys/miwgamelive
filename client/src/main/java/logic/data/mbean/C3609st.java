package logic.data.mbean;

import game.script.missile.MissileType;
import game.script.resource.Asset;
import game.script.ship.HullType;
import logic.baa.C1616Xf;
import logic.res.code.C5663aRz;
import p001a.C0064Am;

/* renamed from: a.st */
public class C3609st extends C3050nT {
    @C0064Am(aul = "dab466e2c4c41e54388b1e5930ee5446", aum = 5)

    /* renamed from: VX */
    public float f9159VX;
    @C0064Am(aul = "4f8c3f5e7323b2d6aae1ba16619a3881", aum = 6)

    /* renamed from: Wb */
    public Asset f9160Wb;
    @C0064Am(aul = "69dda8d7cb9c32365e5927f99c7956ad", aum = 7)

    /* renamed from: Wd */
    public Asset f9161Wd;
    @C0064Am(aul = "f03f6d4767429c76c8f3239fd4b3a5ac", aum = -2)
    public long ail;
    @C0064Am(aul = "f03f6d4767429c76c8f3239fd4b3a5ac", aum = -1)
    public byte aiv;
    @C0064Am(aul = "ff0c2daf3aefe7bbce78583612342d7f", aum = -1)
    public byte bhA;
    @C0064Am(aul = "dab466e2c4c41e54388b1e5930ee5446", aum = -1)
    public byte bhB;
    @C0064Am(aul = "4f8c3f5e7323b2d6aae1ba16619a3881", aum = -1)
    public byte bhC;
    @C0064Am(aul = "69dda8d7cb9c32365e5927f99c7956ad", aum = -1)
    public byte bhD;
    @C0064Am(aul = "a6c56c8864cefc6386a1dda44a1e2c9c", aum = -1)
    public byte bhE;
    @C0064Am(aul = "9799d9e319f41e8f250c5773ab267329", aum = -1)
    public byte bhF;
    @C0064Am(aul = "726dffb2106d93f0fce2c59378fd4566", aum = 0)
    public float bhk;
    @C0064Am(aul = "beef6fa59d3e344243bf03b6890b05f2", aum = 2)
    public float bhl;
    @C0064Am(aul = "a6c56c8864cefc6386a1dda44a1e2c9c", aum = 8)
    public Asset bhm;
    @C0064Am(aul = "9799d9e319f41e8f250c5773ab267329", aum = 9)
    public HullType bhn;
    @C0064Am(aul = "726dffb2106d93f0fce2c59378fd4566", aum = -2)
    public long bho;
    @C0064Am(aul = "d159e8ef9426ab6f8a872fa0b8eb0f03", aum = -2)
    public long bhp;
    @C0064Am(aul = "beef6fa59d3e344243bf03b6890b05f2", aum = -2)
    public long bhq;
    @C0064Am(aul = "ff0c2daf3aefe7bbce78583612342d7f", aum = -2)
    public long bhr;
    @C0064Am(aul = "dab466e2c4c41e54388b1e5930ee5446", aum = -2)
    public long bhs;
    @C0064Am(aul = "4f8c3f5e7323b2d6aae1ba16619a3881", aum = -2)
    public long bht;
    @C0064Am(aul = "69dda8d7cb9c32365e5927f99c7956ad", aum = -2)
    public long bhu;
    @C0064Am(aul = "a6c56c8864cefc6386a1dda44a1e2c9c", aum = -2)
    public long bhv;
    @C0064Am(aul = "9799d9e319f41e8f250c5773ab267329", aum = -2)
    public long bhw;
    @C0064Am(aul = "726dffb2106d93f0fce2c59378fd4566", aum = -1)
    public byte bhx;
    @C0064Am(aul = "d159e8ef9426ab6f8a872fa0b8eb0f03", aum = -1)
    public byte bhy;
    @C0064Am(aul = "beef6fa59d3e344243bf03b6890b05f2", aum = -1)
    public byte bhz;
    @C0064Am(aul = "d159e8ef9426ab6f8a872fa0b8eb0f03", aum = 1)

    /* renamed from: dW */
    public float f9162dW;
    @C0064Am(aul = "ff0c2daf3aefe7bbce78583612342d7f", aum = 3)

    /* renamed from: dX */
    public float f9163dX;
    @C0064Am(aul = "f03f6d4767429c76c8f3239fd4b3a5ac", aum = 4)

    /* renamed from: kY */
    public long f9164kY;

    public C3609st() {
    }

    public C3609st(C1616Xf xf) {
        super(xf);
    }

    /* renamed from: c */
    public C5663aRz[] mo309c() {
        return MissileType._m_fields;
    }

    /* renamed from: d */
    public void mo310d() {
        super.mo310d();
        this.bhk = 0.0f;
        this.f9162dW = 0.0f;
        this.bhl = 0.0f;
        this.f9163dX = 0.0f;
        this.f9164kY = 0;
        this.f9159VX = 0.0f;
        this.f9160Wb = null;
        this.f9161Wd = null;
        this.bhm = null;
        this.bhn = null;
        this.bho = 0;
        this.bhp = 0;
        this.bhq = 0;
        this.bhr = 0;
        this.ail = 0;
        this.bhs = 0;
        this.bht = 0;
        this.bhu = 0;
        this.bhv = 0;
        this.bhw = 0;
        this.bhx = 0;
        this.bhy = 0;
        this.bhz = 0;
        this.bhA = 0;
        this.aiv = 0;
        this.bhB = 0;
        this.bhC = 0;
        this.bhD = 0;
        this.bhE = 0;
        this.bhF = 0;
    }
}
