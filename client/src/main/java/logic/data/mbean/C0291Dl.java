package logic.data.mbean;

import game.script.item.EnergyWeapon;
import game.script.resource.Asset;
import logic.baa.C1616Xf;
import logic.res.code.C5663aRz;
import p001a.C0064Am;

/* renamed from: a.Dl */
public class C0291Dl extends C1716ZO {
    @C0064Am(aul = "eacf7bfd43b7af5e5a194f7f030b9544", aum = 0)
    public int auV;
    @C0064Am(aul = "0d26b2ad4e4957cf73ae6a33f629331f", aum = 1)
    public int auW;
    @C0064Am(aul = "89281fa210cb15b7e1ddefefda827425", aum = 2)
    public Asset auX;
    @C0064Am(aul = "134ed211d5459d33372dd101163f53b6", aum = 3)
    public float auY;
    @C0064Am(aul = "eacf7bfd43b7af5e5a194f7f030b9544", aum = -2)
    public long auZ;
    @C0064Am(aul = "0d26b2ad4e4957cf73ae6a33f629331f", aum = -2)
    public long ava;
    @C0064Am(aul = "89281fa210cb15b7e1ddefefda827425", aum = -2)
    public long avb;
    @C0064Am(aul = "134ed211d5459d33372dd101163f53b6", aum = -2)
    public long avc;
    @C0064Am(aul = "5851f5a508f287cc734e4fd99cbaf614", aum = -2)
    public long avd;
    @C0064Am(aul = "eacf7bfd43b7af5e5a194f7f030b9544", aum = -1)
    public byte ave;
    @C0064Am(aul = "0d26b2ad4e4957cf73ae6a33f629331f", aum = -1)
    public byte avf;
    @C0064Am(aul = "89281fa210cb15b7e1ddefefda827425", aum = -1)
    public byte avg;
    @C0064Am(aul = "134ed211d5459d33372dd101163f53b6", aum = -1)
    public byte avh;
    @C0064Am(aul = "5851f5a508f287cc734e4fd99cbaf614", aum = -1)
    public byte avi;
    @C0064Am(aul = "56585247b09cc94209f013cfb8ee822b", aum = 5)
    public int bMc;
    @C0064Am(aul = "6fa2113c19ac51ceb1efaadd2b384a68", aum = 6)
    public long bMe;
    @C0064Am(aul = "56585247b09cc94209f013cfb8ee822b", aum = -2)
    public long cDW;
    @C0064Am(aul = "6fa2113c19ac51ceb1efaadd2b384a68", aum = -2)
    public long cDX;
    @C0064Am(aul = "56585247b09cc94209f013cfb8ee822b", aum = -1)
    public byte cDY;
    @C0064Am(aul = "6fa2113c19ac51ceb1efaadd2b384a68", aum = -1)
    public byte cDZ;
    @C0064Am(aul = "5851f5a508f287cc734e4fd99cbaf614", aum = 4)

    /* renamed from: uT */
    public String f430uT;

    public C0291Dl() {
    }

    public C0291Dl(C1616Xf xf) {
        super(xf);
    }

    /* renamed from: c */
    public C5663aRz[] mo309c() {
        return EnergyWeapon._m_fields;
    }

    /* renamed from: d */
    public void mo310d() {
        super.mo310d();
        this.auV = 0;
        this.auW = 0;
        this.auX = null;
        this.auY = 0.0f;
        this.f430uT = null;
        this.bMc = 0;
        this.bMe = 0;
        this.auZ = 0;
        this.ava = 0;
        this.avb = 0;
        this.avc = 0;
        this.avd = 0;
        this.cDW = 0;
        this.cDX = 0;
        this.ave = 0;
        this.avf = 0;
        this.avg = 0;
        this.avh = 0;
        this.avi = 0;
        this.cDY = 0;
        this.cDZ = 0;
    }
}
