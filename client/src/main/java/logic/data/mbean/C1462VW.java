package logic.data.mbean;

import game.script.npcchat.requirement.HasMoneyToReserveOutpostSpeechRequeriment;
import game.script.ship.OutpostType;
import logic.baa.C1616Xf;
import logic.res.code.C5663aRz;
import p001a.C0064Am;

/* renamed from: a.VW */
public class C1462VW extends C5416aIm {
    @C0064Am(aul = "9ed5011576fba358aecff9935c8d5bfe", aum = 0)
    public OutpostType bEQ;
    @C0064Am(aul = "9ed5011576fba358aecff9935c8d5bfe", aum = -2)
    public long bES;
    @C0064Am(aul = "9ed5011576fba358aecff9935c8d5bfe", aum = -1)
    public byte bEU;

    public C1462VW() {
    }

    public C1462VW(C1616Xf xf) {
        super(xf);
    }

    /* renamed from: c */
    public C5663aRz[] mo309c() {
        return HasMoneyToReserveOutpostSpeechRequeriment._m_fields;
    }

    /* renamed from: d */
    public void mo310d() {
        super.mo310d();
        this.bEQ = null;
        this.bES = 0;
        this.bEU = 0;
    }
}
