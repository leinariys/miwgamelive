package logic.data.mbean;

import game.script.resource.Asset;
import game.script.ship.speedBoost.SpeedBoostType;
import logic.baa.C1616Xf;
import logic.res.code.C5663aRz;
import p001a.C0064Am;
import p001a.C3892wO;

/* renamed from: a.MC */
public class C0836MC extends C6515aoT {
    @C0064Am(aul = "6691ab8f67242c411e58cf9c85b1e074", aum = 3)

    /* renamed from: Vs */
    public C3892wO f1082Vs;
    @C0064Am(aul = "36e1cb14132dbfb24889b0ec2a8a29d3", aum = 4)

    /* renamed from: Vu */
    public C3892wO f1083Vu;
    @C0064Am(aul = "6f09d398b7698d6278fbad1563d428ba", aum = 6)

    /* renamed from: Vw */
    public C3892wO f1084Vw;
    @C0064Am(aul = "b7efd121f73916f4840bab0831461891", aum = 5)

    /* renamed from: Vy */
    public C3892wO f1085Vy;
    @C0064Am(aul = "6691ab8f67242c411e58cf9c85b1e074", aum = -1)

    /* renamed from: YC */
    public byte f1086YC;
    @C0064Am(aul = "36e1cb14132dbfb24889b0ec2a8a29d3", aum = -1)

    /* renamed from: YD */
    public byte f1087YD;
    @C0064Am(aul = "6f09d398b7698d6278fbad1563d428ba", aum = -1)

    /* renamed from: YE */
    public byte f1088YE;
    @C0064Am(aul = "b7efd121f73916f4840bab0831461891", aum = -1)

    /* renamed from: YF */
    public byte f1089YF;
    @C0064Am(aul = "6691ab8f67242c411e58cf9c85b1e074", aum = -2)

    /* renamed from: Yw */
    public long f1090Yw;
    @C0064Am(aul = "36e1cb14132dbfb24889b0ec2a8a29d3", aum = -2)

    /* renamed from: Yx */
    public long f1091Yx;
    @C0064Am(aul = "6f09d398b7698d6278fbad1563d428ba", aum = -2)

    /* renamed from: Yy */
    public long f1092Yy;
    @C0064Am(aul = "b7efd121f73916f4840bab0831461891", aum = -2)

    /* renamed from: Yz */
    public long f1093Yz;
    @C0064Am(aul = "92c670f229fe208a2643f825f089a2e5", aum = 0)
    public float aAc;
    @C0064Am(aul = "6835cac81de85fad250af753771a77d5", aum = 1)
    public Asset aAd;
    @C0064Am(aul = "1dfa34af20261536a19a522c330ba75c", aum = 2)
    public Asset aAf;
    @C0064Am(aul = "92c670f229fe208a2643f825f089a2e5", aum = -2)
    public long cAA;
    @C0064Am(aul = "6835cac81de85fad250af753771a77d5", aum = -2)
    public long cAB;
    @C0064Am(aul = "1dfa34af20261536a19a522c330ba75c", aum = -2)
    public long cAC;
    @C0064Am(aul = "92c670f229fe208a2643f825f089a2e5", aum = -1)
    public byte cAO;
    @C0064Am(aul = "6835cac81de85fad250af753771a77d5", aum = -1)
    public byte cAP;
    @C0064Am(aul = "1dfa34af20261536a19a522c330ba75c", aum = -1)
    public byte cAQ;

    public C0836MC() {
    }

    public C0836MC(C1616Xf xf) {
        super(xf);
    }

    /* renamed from: c */
    public C5663aRz[] mo309c() {
        return SpeedBoostType._m_fields;
    }

    /* renamed from: d */
    public void mo310d() {
        super.mo310d();
        this.aAc = 0.0f;
        this.aAd = null;
        this.aAf = null;
        this.f1082Vs = null;
        this.f1083Vu = null;
        this.f1085Vy = null;
        this.f1084Vw = null;
        this.cAA = 0;
        this.cAB = 0;
        this.cAC = 0;
        this.f1090Yw = 0;
        this.f1091Yx = 0;
        this.f1093Yz = 0;
        this.f1092Yy = 0;
        this.cAO = 0;
        this.cAP = 0;
        this.cAQ = 0;
        this.f1086YC = 0;
        this.f1087YD = 0;
        this.f1089YF = 0;
        this.f1088YE = 0;
    }
}
