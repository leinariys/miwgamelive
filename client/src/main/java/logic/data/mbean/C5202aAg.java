package logic.data.mbean;

import game.script.citizenship.inprovements.StorageImprovementType;
import logic.baa.C1616Xf;
import logic.res.code.C5663aRz;
import p001a.C0064Am;
import p001a.C3892wO;

/* renamed from: a.aAg  reason: case insensitive filesystem */
public class C5202aAg extends C6071afr {
    @C0064Am(aul = "d5e393b0d14d8cd043561b012192fa54", aum = 0)

    /* renamed from: DL */
    public C3892wO f2339DL;
    @C0064Am(aul = "d5e393b0d14d8cd043561b012192fa54", aum = -2)

    /* renamed from: DM */
    public long f2340DM;
    @C0064Am(aul = "d5e393b0d14d8cd043561b012192fa54", aum = -1)

    /* renamed from: DN */
    public byte f2341DN;

    public C5202aAg() {
    }

    public C5202aAg(C1616Xf xf) {
        super(xf);
    }

    /* renamed from: c */
    public C5663aRz[] mo309c() {
        return StorageImprovementType._m_fields;
    }

    /* renamed from: d */
    public void mo310d() {
        super.mo310d();
        this.f2339DL = null;
        this.f2340DM = 0;
        this.f2341DN = 0;
    }
}
