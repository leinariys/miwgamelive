package logic.data.mbean;

import game.network.message.serializable.C3438ra;
import game.script.npc.NPC;
import game.script.spacezone.population.LinearlyDistribuitedPopulationControl;
import game.script.spacezone.population.NPCSpawn;
import logic.baa.C1616Xf;
import logic.res.code.C5663aRz;
import p001a.C0064Am;

/* renamed from: a.Mi */
public class C0877Mi extends C2693ig {
    @C0064Am(aul = "4bce5d2741ab4ab93d70a506968a40ca", aum = -2)
    public long bdZ;
    @C0064Am(aul = "4bce5d2741ab4ab93d70a506968a40ca", aum = -1)
    public byte bef;
    @C0064Am(aul = "ebda4f9d95aedd663e3fd037d4704740", aum = -2)
    public long bmB;
    @C0064Am(aul = "ebda4f9d95aedd663e3fd037d4704740", aum = -1)
    public byte bmK;
    @C0064Am(aul = "ebda4f9d95aedd663e3fd037d4704740", aum = 0)
    public C3438ra<NPCSpawn> dkG;
    @C0064Am(aul = "e7e6b1b5038656dd50a0314c9b636615", aum = 1)
    public int dkI;
    @C0064Am(aul = "f0f5fb305d0385989e84ee38d3598af2", aum = 2)
    public int dkK;
    @C0064Am(aul = "06fba6f41eddd766ed02bf3f54de369f", aum = 3)
    public int dkM;
    @C0064Am(aul = "31b6851038ffdad57692cdaac8a951b8", aum = 4)
    public int dkO;
    @C0064Am(aul = "4bce5d2741ab4ab93d70a506968a40ca", aum = 5)
    public C3438ra<NPC> dkQ;
    @C0064Am(aul = "06fba6f41eddd766ed02bf3f54de369f", aum = -2)
    public long dzA;
    @C0064Am(aul = "31b6851038ffdad57692cdaac8a951b8", aum = -2)
    public long dzB;
    @C0064Am(aul = "e7e6b1b5038656dd50a0314c9b636615", aum = -1)
    public byte dzC;
    @C0064Am(aul = "f0f5fb305d0385989e84ee38d3598af2", aum = -1)
    public byte dzD;
    @C0064Am(aul = "06fba6f41eddd766ed02bf3f54de369f", aum = -1)
    public byte dzE;
    @C0064Am(aul = "31b6851038ffdad57692cdaac8a951b8", aum = -1)
    public byte dzF;
    @C0064Am(aul = "e7e6b1b5038656dd50a0314c9b636615", aum = -2)
    public long dzy;
    @C0064Am(aul = "f0f5fb305d0385989e84ee38d3598af2", aum = -2)
    public long dzz;

    public C0877Mi() {
    }

    public C0877Mi(C1616Xf xf) {
        super(xf);
    }

    /* renamed from: c */
    public C5663aRz[] mo309c() {
        return LinearlyDistribuitedPopulationControl._m_fields;
    }

    /* renamed from: d */
    public void mo310d() {
        super.mo310d();
        this.dkG = null;
        this.dkI = 0;
        this.dkK = 0;
        this.dkM = 0;
        this.dkO = 0;
        this.dkQ = null;
        this.bmB = 0;
        this.dzy = 0;
        this.dzz = 0;
        this.dzA = 0;
        this.dzB = 0;
        this.bdZ = 0;
        this.bmK = 0;
        this.dzC = 0;
        this.dzD = 0;
        this.dzE = 0;
        this.dzF = 0;
        this.bef = 0;
    }
}
