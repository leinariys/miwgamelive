package logic.data.mbean;

import com.hoplon.geometry.Vec3f;
import game.script.ai.npc.FormationController;
import logic.baa.C1616Xf;
import logic.res.code.C5663aRz;
import p001a.C0064Am;

/* renamed from: a.Cs */
public class C0228Cs extends C5574aOo {
    @C0064Am(aul = "8ffd8289615d06ee376d339f164ccb7a", aum = 0)
    public Vec3f bMK;
    @C0064Am(aul = "8f470ac21c2068aeaf67ccbe1fa505ac", aum = 1)
    public float bMM;
    @C0064Am(aul = "8ffd8289615d06ee376d339f164ccb7a", aum = -2)
    public long cvf;
    @C0064Am(aul = "8f470ac21c2068aeaf67ccbe1fa505ac", aum = -2)
    public long cvg;
    @C0064Am(aul = "8ffd8289615d06ee376d339f164ccb7a", aum = -1)
    public byte cvh;
    @C0064Am(aul = "8f470ac21c2068aeaf67ccbe1fa505ac", aum = -1)
    public byte cvi;

    public C0228Cs() {
    }

    public C0228Cs(C1616Xf xf) {
        super(xf);
    }

    /* renamed from: c */
    public C5663aRz[] mo309c() {
        return FormationController._m_fields;
    }

    /* renamed from: d */
    public void mo310d() {
        super.mo310d();
        this.bMK = null;
        this.bMM = 0.0f;
        this.cvf = 0;
        this.cvg = 0;
        this.cvh = 0;
        this.cvi = 0;
    }
}
