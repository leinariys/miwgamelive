package logic.data.mbean;

import game.geometry.Vec3d;
import game.script.ai.npc.ArrivalController;
import logic.baa.C1616Xf;
import logic.res.code.C5663aRz;
import p001a.C0064Am;

/* renamed from: a.wu */
public class C3931wu extends C6937awc {
    @C0064Am(aul = "bc47a3f23f8b69f0809417f676a6d5c7", aum = -2)
    public long bDo;
    @C0064Am(aul = "bc47a3f23f8b69f0809417f676a6d5c7", aum = -1)
    public byte bDp;
    @C0064Am(aul = "bc47a3f23f8b69f0809417f676a6d5c7", aum = 0)
    public Vec3d point;

    public C3931wu() {
    }

    public C3931wu(C1616Xf xf) {
        super(xf);
    }

    /* renamed from: c */
    public C5663aRz[] mo309c() {
        return ArrivalController._m_fields;
    }

    /* renamed from: d */
    public void mo310d() {
        super.mo310d();
        this.point = null;
        this.bDo = 0;
        this.bDp = 0;
    }
}
