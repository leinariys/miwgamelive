package logic.data.mbean;

import game.script.Character;
import game.script.mission.NPCEventDispatcher;
import game.script.resource.Asset;
import game.script.space.Loot;
import game.script.space.LootItems;
import logic.baa.C1616Xf;
import logic.res.code.C5663aRz;
import p001a.C0064Am;

/* renamed from: a.aln  reason: case insensitive filesystem */
public class C6379aln extends C2217cs {
    @C0064Am(aul = "793471a42b73d3099e5d3771c994a16e", aum = 0)

    /* renamed from: LQ */
    public Asset f4882LQ;
    @C0064Am(aul = "03e01d0addf8a7dcf7ba2cd81f3f8e78", aum = -2)
    public long ail;
    @C0064Am(aul = "03e01d0addf8a7dcf7ba2cd81f3f8e78", aum = -1)
    public byte aiv;
    @C0064Am(aul = "793471a42b73d3099e5d3771c994a16e", aum = -2)
    public long ayc;
    @C0064Am(aul = "793471a42b73d3099e5d3771c994a16e", aum = -1)
    public byte ayr;
    @C0064Am(aul = "13bf1ce58d371c7aed686b0c5c0b2508", aum = -2)
    public long fWA;
    @C0064Am(aul = "052ae4c1d0ccf5b495de4dc2efdd6dee", aum = -2)
    public long fWB;
    @C0064Am(aul = "05caf179c0d8e59c73283b65142a8db9", aum = -2)
    public long fWC;
    @C0064Am(aul = "11d55e4abecf6233a39ee379aae10b56", aum = -2)
    public long fWD;
    @C0064Am(aul = "1c8828e793d8ae9330b665d11a45a425", aum = -1)
    public byte fWE;
    @C0064Am(aul = "ca5362f757f7120227f0ecb98e095379", aum = -1)
    public byte fWF;
    @C0064Am(aul = "99e36d4281877e977b9d6f7595437347", aum = -1)
    public byte fWG;
    @C0064Am(aul = "f97ca1937e047ae711d77cfb75d321f8", aum = -1)
    public byte fWH;
    @C0064Am(aul = "13bf1ce58d371c7aed686b0c5c0b2508", aum = -1)
    public byte fWI;
    @C0064Am(aul = "052ae4c1d0ccf5b495de4dc2efdd6dee", aum = -1)
    public byte fWJ;
    @C0064Am(aul = "05caf179c0d8e59c73283b65142a8db9", aum = -1)
    public byte fWK;
    @C0064Am(aul = "11d55e4abecf6233a39ee379aae10b56", aum = -1)
    public byte fWL;
    @C0064Am(aul = "1c8828e793d8ae9330b665d11a45a425", aum = 2)
    public LootItems fWo;
    @C0064Am(aul = "ca5362f757f7120227f0ecb98e095379", aum = 3)
    public Character fWp;
    @C0064Am(aul = "99e36d4281877e977b9d6f7595437347", aum = 4)
    public Character fWq;
    @C0064Am(aul = "f97ca1937e047ae711d77cfb75d321f8", aum = 5)
    public String fWr;
    @C0064Am(aul = "13bf1ce58d371c7aed686b0c5c0b2508", aum = 6)
    public boolean fWs;
    @C0064Am(aul = "052ae4c1d0ccf5b495de4dc2efdd6dee", aum = 7)
    public NPCEventDispatcher fWt;
    @C0064Am(aul = "05caf179c0d8e59c73283b65142a8db9", aum = 8)
    public boolean fWu;
    @C0064Am(aul = "11d55e4abecf6233a39ee379aae10b56", aum = 9)
    public Character fWv;
    @C0064Am(aul = "1c8828e793d8ae9330b665d11a45a425", aum = -2)
    public long fWw;
    @C0064Am(aul = "ca5362f757f7120227f0ecb98e095379", aum = -2)
    public long fWx;
    @C0064Am(aul = "99e36d4281877e977b9d6f7595437347", aum = -2)
    public long fWy;
    @C0064Am(aul = "f97ca1937e047ae711d77cfb75d321f8", aum = -2)
    public long fWz;
    @C0064Am(aul = "03e01d0addf8a7dcf7ba2cd81f3f8e78", aum = 1)
    public float lifeTime;

    public C6379aln() {
    }

    public C6379aln(C1616Xf xf) {
        super(xf);
    }

    /* renamed from: c */
    public C5663aRz[] mo309c() {
        return Loot._m_fields;
    }

    /* renamed from: d */
    public void mo310d() {
        super.mo310d();
        this.f4882LQ = null;
        this.lifeTime = 0.0f;
        this.fWo = null;
        this.fWp = null;
        this.fWq = null;
        this.fWr = null;
        this.fWs = false;
        this.fWt = null;
        this.fWu = false;
        this.fWv = null;
        this.ayc = 0;
        this.ail = 0;
        this.fWw = 0;
        this.fWx = 0;
        this.fWy = 0;
        this.fWz = 0;
        this.fWA = 0;
        this.fWB = 0;
        this.fWC = 0;
        this.fWD = 0;
        this.ayr = 0;
        this.aiv = 0;
        this.fWE = 0;
        this.fWF = 0;
        this.fWG = 0;
        this.fWH = 0;
        this.fWI = 0;
        this.fWJ = 0;
        this.fWK = 0;
        this.fWL = 0;
    }
}
