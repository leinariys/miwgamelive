package logic.data.mbean;

import game.script.item.Amplifier;
import game.script.ship.Ship;
import logic.baa.C1616Xf;
import logic.res.code.C5663aRz;
import p001a.C0064Am;

/* renamed from: a.nv */
public class C3108nv extends C0496Gs {
    @C0064Am(aul = "dde47cd6bdcaebc69048ca274e534a9f", aum = 0)
    public Ship aIq;
    @C0064Am(aul = "fbf45096f5852ecb0e4b899ee20ddf9a", aum = 1)
    public boolean aIr;
    @C0064Am(aul = "dde47cd6bdcaebc69048ca274e534a9f", aum = -2)
    public long aIs;
    @C0064Am(aul = "fbf45096f5852ecb0e4b899ee20ddf9a", aum = -2)
    public long aIt;
    @C0064Am(aul = "296a0761c4df0d0066d7a4046c912f4b", aum = -2)
    public long aIu;
    @C0064Am(aul = "dde47cd6bdcaebc69048ca274e534a9f", aum = -1)
    public byte aIv;
    @C0064Am(aul = "fbf45096f5852ecb0e4b899ee20ddf9a", aum = -1)
    public byte aIw;
    @C0064Am(aul = "296a0761c4df0d0066d7a4046c912f4b", aum = -1)
    public byte aIx;
    @C0064Am(aul = "296a0761c4df0d0066d7a4046c912f4b", aum = 2)
    public boolean active;

    public C3108nv() {
    }

    public C3108nv(C1616Xf xf) {
        super(xf);
    }

    /* renamed from: c */
    public C5663aRz[] mo309c() {
        return Amplifier._m_fields;
    }

    /* renamed from: d */
    public void mo310d() {
        super.mo310d();
        this.aIq = null;
        this.aIr = false;
        this.active = false;
        this.aIs = 0;
        this.aIt = 0;
        this.aIu = 0;
        this.aIv = 0;
        this.aIw = 0;
        this.aIx = 0;
    }
}
