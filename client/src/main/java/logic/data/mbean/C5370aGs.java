package logic.data.mbean;

import game.script.ship.categories.Bomber;
import game.script.ship.strike.Strike;
import game.script.ship.strike.StrikeType;
import logic.baa.C1616Xf;
import logic.res.code.C5663aRz;
import p001a.C0064Am;

/* renamed from: a.aGs  reason: case insensitive filesystem */
public class C5370aGs extends aMX {
    @C0064Am(aul = "5832adc52a2d480f1343a0540382aa77", aum = 0)

    /* renamed from: UD */
    public StrikeType f2889UD;
    @C0064Am(aul = "5832adc52a2d480f1343a0540382aa77", aum = -2)
    public long awV;
    @C0064Am(aul = "5832adc52a2d480f1343a0540382aa77", aum = -1)
    public byte awW;
    @C0064Am(aul = "8c651229f4acf558853c61251b2e33ed", aum = 1)
    public Strike hPM;
    @C0064Am(aul = "8c651229f4acf558853c61251b2e33ed", aum = -2)
    public long hPN;
    @C0064Am(aul = "8c651229f4acf558853c61251b2e33ed", aum = -1)
    public byte hPO;

    public C5370aGs() {
    }

    public C5370aGs(C1616Xf xf) {
        super(xf);
    }

    /* renamed from: c */
    public C5663aRz[] mo309c() {
        return Bomber._m_fields;
    }

    /* renamed from: d */
    public void mo310d() {
        super.mo310d();
        this.f2889UD = null;
        this.hPM = null;
        this.awV = 0;
        this.hPN = 0;
        this.awW = 0;
        this.hPO = 0;
    }
}
