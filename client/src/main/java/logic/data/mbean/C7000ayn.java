package logic.data.mbean;

import game.network.message.serializable.C1556Wo;
import game.network.message.serializable.C6275ajn;
import game.script.newmarket.store.GlobalEconomyInfo;
import logic.baa.C1616Xf;
import logic.res.code.C5663aRz;
import p001a.C0064Am;

/* renamed from: a.ayn  reason: case insensitive filesystem */
public class C7000ayn extends C5292aDs {
    @C0064Am(aul = "07b68bcff73491348924bc6676b127b5", aum = 0)
    public C1556Wo<Long, C6275ajn> aVt;
    @C0064Am(aul = "17ffcb150a3f6ca59b32d6f07909e0bb", aum = 1)
    public long aVv;
    @C0064Am(aul = "286a339327cf97b68fc92f825606a7b1", aum = 3)
    public long aVy;
    @C0064Am(aul = "07b68bcff73491348924bc6676b127b5", aum = -2)
    public long fbU;
    @C0064Am(aul = "07b68bcff73491348924bc6676b127b5", aum = -1)
    public byte fbW;
    @C0064Am(aul = "17ffcb150a3f6ca59b32d6f07909e0bb", aum = -2)
    public long gTf;
    @C0064Am(aul = "c4ba1dd02ebe6d09389cd7e39d92b3e7", aum = -2)
    public long gTg;
    @C0064Am(aul = "286a339327cf97b68fc92f825606a7b1", aum = -2)
    public long gTh;
    @C0064Am(aul = "17ffcb150a3f6ca59b32d6f07909e0bb", aum = -1)
    public byte gTi;
    @C0064Am(aul = "c4ba1dd02ebe6d09389cd7e39d92b3e7", aum = -1)
    public byte gTj;
    @C0064Am(aul = "286a339327cf97b68fc92f825606a7b1", aum = -1)
    public byte gTk;
    @C0064Am(aul = "c4ba1dd02ebe6d09389cd7e39d92b3e7", aum = 2)

    /* renamed from: n */
    public int f5652n;

    public C7000ayn() {
    }

    public C7000ayn(C1616Xf xf) {
        super(xf);
    }

    /* renamed from: c */
    public C5663aRz[] mo309c() {
        return GlobalEconomyInfo.TransactionLog._m_fields;
    }

    /* renamed from: d */
    public void mo310d() {
        super.mo310d();
        this.aVt = null;
        this.aVv = 0;
        this.f5652n = 0;
        this.aVy = 0;
        this.fbU = 0;
        this.gTf = 0;
        this.gTg = 0;
        this.gTh = 0;
        this.fbW = 0;
        this.gTi = 0;
        this.gTj = 0;
        this.gTk = 0;
    }
}
