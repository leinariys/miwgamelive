package logic.data.mbean;

import game.script.resource.Asset;
import game.script.space.GateType;
import logic.baa.C1616Xf;
import logic.res.code.C5663aRz;
import p001a.C0064Am;

/* renamed from: a.abd  reason: case insensitive filesystem */
public class C5849abd extends C6515aoT {
    @C0064Am(aul = "43ed39b9f009f17da0068f17fe210a89", aum = 0)

    /* renamed from: LQ */
    public Asset f4146LQ;
    @C0064Am(aul = "05aa6be57989d3cc73985def10081019", aum = 2)

    /* renamed from: LS */
    public Asset f4147LS;
    @C0064Am(aul = "82817f8954acf7674915cb47ff173dc6", aum = -2)
    public long avd;
    @C0064Am(aul = "82817f8954acf7674915cb47ff173dc6", aum = -1)
    public byte avi;
    @C0064Am(aul = "43ed39b9f009f17da0068f17fe210a89", aum = -2)
    public long ayc;
    @C0064Am(aul = "43ed39b9f009f17da0068f17fe210a89", aum = -1)
    public byte ayr;
    @C0064Am(aul = "05aa6be57989d3cc73985def10081019", aum = -2)
    public long eXJ;
    @C0064Am(aul = "05aa6be57989d3cc73985def10081019", aum = -1)
    public byte eXK;
    @C0064Am(aul = "82817f8954acf7674915cb47ff173dc6", aum = 1)

    /* renamed from: uT */
    public String f4148uT;

    public C5849abd() {
    }

    public C5849abd(C1616Xf xf) {
        super(xf);
    }

    /* renamed from: c */
    public C5663aRz[] mo309c() {
        return GateType._m_fields;
    }

    /* renamed from: d */
    public void mo310d() {
        super.mo310d();
        this.f4146LQ = null;
        this.f4148uT = null;
        this.f4147LS = null;
        this.ayc = 0;
        this.avd = 0;
        this.eXJ = 0;
        this.ayr = 0;
        this.avi = 0;
        this.eXK = 0;
    }
}
