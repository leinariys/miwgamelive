package logic.data.mbean;

import game.geometry.Vec3d;
import game.script.ai.npc.FollowerController;
import logic.baa.C1616Xf;
import logic.res.code.C5663aRz;
import p001a.C0064Am;

/* renamed from: a.auH  reason: case insensitive filesystem */
public class C6815auH extends C5574aOo {
    @C0064Am(aul = "b0bfbbba44af41c41aedee8790700284", aum = -1)
    public byte azF;
    @C0064Am(aul = "b0bfbbba44af41c41aedee8790700284", aum = 5)
    public NPCParty azb;
    @C0064Am(aul = "b0bfbbba44af41c41aedee8790700284", aum = -2)
    public long azq;
    @C0064Am(aul = "8cb0f5e4d45fcd74dde2655e9502ee9c", aum = 6)
    public Controller cCG;
    @C0064Am(aul = "8cb0f5e4d45fcd74dde2655e9502ee9c", aum = -2)
    public long cCN;
    @C0064Am(aul = "8cb0f5e4d45fcd74dde2655e9502ee9c", aum = -1)
    public byte cCU;
    @C0064Am(aul = "57907b60d0141b2ca56f5c4c701f3a67", aum = 0)
    public float fqN;
    @C0064Am(aul = "2d23e2b5cd8362a8dd41b1159adde8b1", aum = 1)
    public float fqP;
    @C0064Am(aul = "d00a6819f31f87accc091e5a954bffc3", aum = 2)
    public float fqR;
    @C0064Am(aul = "5d0e2f3185b9a9800886a9021daac041", aum = 3)
    public long fqT;
    @C0064Am(aul = "0fb85f07fb0107b3e0891a08eb28b751", aum = 4)
    public Vec3d fqV;
    @C0064Am(aul = "57907b60d0141b2ca56f5c4c701f3a67", aum = -2)
    public long gEo;
    @C0064Am(aul = "2d23e2b5cd8362a8dd41b1159adde8b1", aum = -2)
    public long gEp;
    @C0064Am(aul = "d00a6819f31f87accc091e5a954bffc3", aum = -2)
    public long gEq;
    @C0064Am(aul = "5d0e2f3185b9a9800886a9021daac041", aum = -2)
    public long gEr;
    @C0064Am(aul = "0fb85f07fb0107b3e0891a08eb28b751", aum = -2)
    public long gEs;
    @C0064Am(aul = "57907b60d0141b2ca56f5c4c701f3a67", aum = -1)
    public byte gEt;
    @C0064Am(aul = "2d23e2b5cd8362a8dd41b1159adde8b1", aum = -1)
    public byte gEu;
    @C0064Am(aul = "d00a6819f31f87accc091e5a954bffc3", aum = -1)
    public byte gEv;
    @C0064Am(aul = "5d0e2f3185b9a9800886a9021daac041", aum = -1)
    public byte gEw;
    @C0064Am(aul = "0fb85f07fb0107b3e0891a08eb28b751", aum = -1)
    public byte gEx;

    public C6815auH() {
    }

    public C6815auH(C1616Xf xf) {
        super(xf);
    }

    /* renamed from: c */
    public C5663aRz[] mo309c() {
        return FollowerController._m_fields;
    }

    /* renamed from: d */
    public void mo310d() {
        super.mo310d();
        this.fqN = 0.0f;
        this.fqP = 0.0f;
        this.fqR = 0.0f;
        this.fqT = 0;
        this.fqV = null;
        this.azb = null;
        this.cCG = null;
        this.gEo = 0;
        this.gEp = 0;
        this.gEq = 0;
        this.gEr = 0;
        this.gEs = 0;
        this.azq = 0;
        this.cCN = 0;
        this.gEt = 0;
        this.gEu = 0;
        this.gEv = 0;
        this.gEw = 0;
        this.gEx = 0;
        this.azF = 0;
        this.cCU = 0;
    }
}
