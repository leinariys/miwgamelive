package logic.data.mbean;

import game.network.message.serializable.C3438ra;
import game.script.mission.scripting.I18NStringTable;
import game.script.mission.scripting.ScriptableMissionTemplate;
import game.script.mission.scripting.ScriptableMissionTemplateObjective;
import logic.baa.C1616Xf;
import logic.res.code.C5663aRz;
import p001a.C0064Am;

/* renamed from: a.Qv */
public class C1154Qv extends C1756Zw {
    @C0064Am(aul = "fa819363a7af88bd0d5112dee297bea9", aum = 2)
    public I18NStringTable bdX;
    @C0064Am(aul = "fa819363a7af88bd0d5112dee297bea9", aum = -2)
    public long bed;
    @C0064Am(aul = "fa819363a7af88bd0d5112dee297bea9", aum = -1)
    public byte bej;
    @C0064Am(aul = "313f088aa57f3d6376f8d747de1fcf83", aum = 0)
    public C3438ra<ScriptableMissionTemplateObjective> cno;
    @C0064Am(aul = "0c57b278ed0839073a44179dda3e044b", aum = 1)
    public String dWf;
    @C0064Am(aul = "0c57b278ed0839073a44179dda3e044b", aum = -2)
    public long dWg;
    @C0064Am(aul = "0c57b278ed0839073a44179dda3e044b", aum = -1)
    public byte dWh;
    @C0064Am(aul = "313f088aa57f3d6376f8d747de1fcf83", aum = -2)
    public long dhk;
    @C0064Am(aul = "313f088aa57f3d6376f8d747de1fcf83", aum = -1)
    public byte dho;

    public C1154Qv() {
    }

    public C1154Qv(C1616Xf xf) {
        super(xf);
    }

    /* renamed from: c */
    public C5663aRz[] mo309c() {
        return ScriptableMissionTemplate._m_fields;
    }

    /* renamed from: d */
    public void mo310d() {
        super.mo310d();
        this.cno = null;
        this.dWf = null;
        this.bdX = null;
        this.dhk = 0;
        this.dWg = 0;
        this.bed = 0;
        this.dho = 0;
        this.dWh = 0;
        this.bej = 0;
    }
}
