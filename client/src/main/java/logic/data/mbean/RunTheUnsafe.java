package logic.data.mbean;


import game.io.IExternalIO;
import logic.baa.C1616Xf;
import logic.res.code.C2759jd;
import logic.res.code.C5663aRz;
import logic.thred.LogPrinter;
import sun.misc.Unsafe;

import java.lang.reflect.Field;

/* renamed from: a.aVo  reason: case insensitive filesystem */
/* compiled from: a */
public abstract class RunTheUnsafe implements IExternalIO, C6064afk {

    private static LogPrinter logger = LogPrinter.m10275K(RunTheUnsafe.class);
    public static final Unsafe unsafe = getUnsafe();

    /* renamed from: Uk */
    public C1616Xf f3984Uk;
    public long cmb = 0;

    public RunTheUnsafe() {
    }

    public RunTheUnsafe(C1616Xf xf) {
        this.f3984Uk = xf;
    }

    private static Unsafe getUnsafe() {
        try {
            Field declaredField = Unsafe.class.getDeclaredField("theUnsafe");
            declaredField.setAccessible(true);
            return (Unsafe) declaredField.get((Object) null);
        } catch (Exception e) {
            logger.fatal("Could not find the sun.misc.Unsafe! The application will not work!", e);
            return null;
        }
    }

    /* renamed from: B */
    public Object mo11903B(C5663aRz arz) {
        if (arz.isPrimitive()) {
            throw new IllegalArgumentException("Trying to access primitive field as object" + arz);
        }
        long EA = ((C2759jd) arz).mo19959EA();
        Object object = unsafe.getObject(this, EA);
        if (object == null && (object = arz.mo2188b(this.f3984Uk)) != null) {
            unsafe.putObject(this, EA, object);
        }
        return object;
    }

    /* renamed from: C */
    public int mo11904C(C5663aRz arz) {
        if (arz.mo11291El() == Integer.TYPE) {
            return mo11913L(arz);
        }
        throw new IllegalArgumentException("Trying to access primitive field with wrong getter " + arz);
    }

    /* renamed from: D */
    public boolean mo11905D(C5663aRz arz) {
        if (arz.mo11291El() == Boolean.TYPE) {
            return mo11914M(arz);
        }
        throw new IllegalArgumentException("Trying to access primitive field with wrong getter " + arz);
    }

    /* renamed from: E */
    public byte mo11906E(C5663aRz arz) {
        if (arz.mo11291El() == Byte.TYPE) {
            return mo11915N(arz);
        }
        throw new IllegalArgumentException("Trying to access primitive field with wrong getter " + arz);
    }

    /* renamed from: F */
    public short mo11907F(C5663aRz arz) {
        if (arz.mo11291El() == Short.TYPE) {
            return mo11916O(arz);
        }
        throw new IllegalArgumentException("Trying to access primitive field with wrong getter " + arz);
    }

    /* renamed from: G */
    public char mo11908G(C5663aRz arz) {
        if (arz.mo11291El() == Character.TYPE) {
            return mo11917P(arz);
        }
        throw new IllegalArgumentException("Trying to access primitive field with wrong getter " + arz);
    }

    /* renamed from: H */
    public long mo11909H(C5663aRz arz) {
        if (arz.mo11291El() == Long.TYPE) {
            return mo11918Q(arz);
        }
        throw new IllegalArgumentException("Trying to access primitive field with wrong getter " + arz);
    }

    /* renamed from: I */
    public float mo11910I(C5663aRz arz) {
        if (arz.mo11291El() == Float.TYPE) {
            return mo11919R(arz);
        }
        throw new IllegalArgumentException("Trying to access primitive field with wrong getter " + arz);
    }

    /* renamed from: J */
    public double mo11911J(C5663aRz arz) {
        if (arz.mo11291El() == Double.TYPE) {
            return mo11920S(arz);
        }
        throw new IllegalArgumentException("Trying to access primitive field with wrong getter " + arz);
    }

    /* renamed from: l */
    public void mo11941l(C5663aRz arz, Object obj) {
        if (arz.isPrimitive()) {
            throw new IllegalArgumentException("Trying to access primitive field as object" + arz);
        }
        mo11942m(arz, obj);
    }

    /* renamed from: d */
    public void mo11936d(C5663aRz arz, int i) {
        if (arz.mo11291El() != Integer.TYPE) {
            throw new IllegalArgumentException("Trying to access primitive field with wrong setter " + arz);
        }
        mo11938e(arz, i);
    }

    /* renamed from: b */
    public void mo11928b(C5663aRz arz, boolean z) {
        if (arz.mo11291El() != Boolean.TYPE) {
            throw new IllegalArgumentException("Trying to access primitive field with wrong setter " + arz);
        }
        mo11935c(arz, z);
    }

    /* renamed from: b */
    public void mo11923b(C5663aRz arz, byte b) {
        if (arz.mo11291El() != Byte.TYPE) {
            throw new IllegalArgumentException("Trying to access primitive field with wrong setter " + arz);
        }
        mo11929c(arz, b);
    }

    /* renamed from: b */
    public void mo11927b(C5663aRz arz, short s) {
        if (arz.mo11291El() != Short.TYPE) {
            throw new IllegalArgumentException("Trying to access primitive field with wrong setter " + arz);
        }
        mo11934c(arz, s);
    }

    /* renamed from: b */
    public void mo11924b(C5663aRz arz, char c) {
        if (arz.mo11291El() != Boolean.TYPE) {
            throw new IllegalArgumentException("Trying to access primitive field with wrong setter " + arz);
        }
        mo11930c(arz, c);
    }

    /* renamed from: c */
    public void mo11933c(C5663aRz arz, long j) {
        if (arz.mo11291El() != Long.TYPE) {
            throw new IllegalArgumentException("Trying to access primitive field with wrong setter " + arz);
        }
        mo11937d(arz, j);
    }

    /* renamed from: b */
    public void mo11926b(C5663aRz arz, float f) {
        if (arz.mo11291El() != Float.TYPE) {
            throw new IllegalArgumentException("Trying to access primitive field with wrong setter " + arz);
        }
        mo11932c(arz, f);
    }

    /* renamed from: b */
    public void mo11925b(C5663aRz arz, double d) {
        if (arz.mo11291El() != Double.TYPE) {
            throw new IllegalArgumentException("Trying to access primitive field with wrong setter " + arz);
        }
        mo11931c(arz, d);
    }

    /* access modifiers changed from: protected */
    /* renamed from: K */
    public Object mo11912K(C5663aRz arz) {
        return unsafe.getObject(this, ((C2759jd) arz).mo19959EA());
    }

    /* access modifiers changed from: protected */
    /* renamed from: L */
    public int mo11913L(C5663aRz arz) {
        return unsafe.getInt(this, ((C2759jd) arz).mo19959EA());
    }

    /* access modifiers changed from: protected */
    /* renamed from: M */
    public boolean mo11914M(C5663aRz arz) {
        return unsafe.getBoolean(this, ((C2759jd) arz).mo19959EA());
    }

    /* access modifiers changed from: protected */
    /* renamed from: N */
    public byte mo11915N(C5663aRz arz) {
        return unsafe.getByte(this, ((C2759jd) arz).mo19959EA());
    }

    /* access modifiers changed from: protected */
    /* renamed from: O */
    public short mo11916O(C5663aRz arz) {
        return unsafe.getShort(this, ((C2759jd) arz).mo19959EA());
    }

    /* access modifiers changed from: protected */
    /* renamed from: P */
    public char mo11917P(C5663aRz arz) {
        return unsafe.getChar(this, ((C2759jd) arz).mo19959EA());
    }

    /* access modifiers changed from: protected */
    /* renamed from: Q */
    public long mo11918Q(C5663aRz arz) {
        return unsafe.getLong(this, ((C2759jd) arz).mo19959EA());
    }

    /* access modifiers changed from: protected */
    /* renamed from: R */
    public float mo11919R(C5663aRz arz) {
        return unsafe.getFloat(this, ((C2759jd) arz).mo19959EA());
    }

    /* access modifiers changed from: protected */
    /* renamed from: S */
    public double mo11920S(C5663aRz arz) {
        return unsafe.getDouble(this, ((C2759jd) arz).mo19959EA());
    }

    /* access modifiers changed from: protected */
    /* renamed from: m */
    public void mo11942m(C5663aRz arz, Object obj) {
        unsafe.putObject(this, ((C2759jd) arz).mo19959EA(), obj);
    }

    /* access modifiers changed from: protected */
    /* renamed from: e */
    public void mo11938e(C5663aRz arz, int i) {
        unsafe.putInt(this, ((C2759jd) arz).mo19959EA(), i);
    }

    /* access modifiers changed from: protected */
    /* renamed from: c */
    public void mo11935c(C5663aRz arz, boolean z) {
        unsafe.putBoolean(this, ((C2759jd) arz).mo19959EA(), z);
    }

    /* access modifiers changed from: protected */
    /* renamed from: c */
    public void mo11929c(C5663aRz arz, byte b) {
        unsafe.putByte(this, ((C2759jd) arz).mo19959EA(), b);
    }

    /* access modifiers changed from: protected */
    /* renamed from: c */
    public void mo11934c(C5663aRz arz, short s) {
        unsafe.putShort(this, ((C2759jd) arz).mo19959EA(), s);
    }

    /* access modifiers changed from: protected */
    /* renamed from: c */
    public void mo11930c(C5663aRz arz, char c) {
        unsafe.putChar(this, ((C2759jd) arz).mo19959EA(), c);
    }

    /* access modifiers changed from: protected */
    /* renamed from: d */
    public void mo11937d(C5663aRz arz, long j) {
        unsafe.putLong(this, ((C2759jd) arz).mo19959EA(), j);
    }

    /* access modifiers changed from: protected */
    /* renamed from: c */
    public void mo11932c(C5663aRz arz, float f) {
        unsafe.putFloat(this, ((C2759jd) arz).mo19959EA(), f);
    }

    /* access modifiers changed from: protected */
    /* renamed from: c */
    public void mo11931c(C5663aRz arz, double d) {
        unsafe.putDouble(this, ((C2759jd) arz).mo19959EA(), d);
    }

    /* access modifiers changed from: protected */
    /* renamed from: f */
    public void mo11940f(C5663aRz arz, int i) {
        unsafe.putByte(this, ((C2759jd) arz).mo19961EC(), (byte) i);
    }

    /* renamed from: T */
    public int mo11921T(C5663aRz arz) {
        return unsafe.getByte(this, ((C2759jd) arz).mo19961EC());
    }

    /* renamed from: c */
    public C5663aRz[] mo309c() {
        return this.f3984Uk.mo25c();
    }

    public long aBt() {
        return this.cmb;
    }

    /* renamed from: eU */
    public void mo3196eU(long j) {
        this.cmb = j;
    }

    public String toString() {
        return super.toString();
    }

    /* renamed from: yn */
    public C1616Xf mo11944yn() {
        return this.f3984Uk;
    }

    /* renamed from: l */
    public long mo3210l(C5663aRz arz) {
        return unsafe.getLong(this, ((C2759jd) arz).mo19960EB());
    }

    /* renamed from: a */
    public void mo3151a(C5663aRz arz, long j) {
        unsafe.putLong(this, ((C2759jd) arz).mo19960EB(), j);
    }

    /* renamed from: e */
    public void mo11939e(C5663aRz arz, long j) {
        unsafe.putLong(this, ((C2759jd) arz).mo19960EB(), j);
    }

    /* renamed from: U */
    public long mo11922U(C5663aRz arz) {
        return unsafe.getLong(this, ((C2759jd) arz).mo19960EB());
    }
}
