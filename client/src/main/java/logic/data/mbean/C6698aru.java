package logic.data.mbean;

import game.network.message.serializable.C1556Wo;
import game.script.damage.DamageType;
import game.script.ship.Shield;
import game.script.ship.ShieldAdapter;
import game.script.ship.Ship;
import game.script.util.AdapterLikedList;
import logic.baa.C1616Xf;
import logic.res.code.C5663aRz;
import p001a.C0064Am;
import p001a.C1649YI;

/* renamed from: a.aru  reason: case insensitive filesystem */
public class C6698aru extends C3805vD {
    @C0064Am(aul = "40063beff048219aa3b327f53c15120a", aum = -2)
    public long aBO;
    @C0064Am(aul = "28ffe50c03e46febc54cebd70ee9e90b", aum = -2)
    public long aBP;
    @C0064Am(aul = "18d4278273e2cbf6472ef6e8782d480b", aum = -2)
    public long aBQ;
    @C0064Am(aul = "8c1738a8ecd81022a1ada3a337ee12de", aum = -2)
    public long aBR;
    @C0064Am(aul = "40063beff048219aa3b327f53c15120a", aum = -1)
    public byte aBT;
    @C0064Am(aul = "28ffe50c03e46febc54cebd70ee9e90b", aum = -1)
    public byte aBU;
    @C0064Am(aul = "18d4278273e2cbf6472ef6e8782d480b", aum = -1)
    public byte aBV;
    @C0064Am(aul = "8c1738a8ecd81022a1ada3a337ee12de", aum = -1)
    public byte aBW;
    @C0064Am(aul = "6c6fff801279feb764ab3031ca2b6d09", aum = 6)
    public float aOA;
    @C0064Am(aul = "a7476bef8f13230be1e2fb9d7554e3e2", aum = 7)
    public float aOC;
    @C0064Am(aul = "7dd53d465b767f8c29d8a3e261ad278b", aum = 8)
    public float aOE;
    @C0064Am(aul = "571b767df644d582a1a42a872864665f", aum = 9)
    public C1556Wo<DamageType, Float> aOG;
    @C0064Am(aul = "40063beff048219aa3b327f53c15120a", aum = 10)
    public float aOI;
    @C0064Am(aul = "c3ddc4be019314404af79614e2557f19", aum = 11)
    public long aOK;
    @C0064Am(aul = "72ef3c36e162b1312c7c9c2e9777a928", aum = 12)
    public boolean aOM;
    @C0064Am(aul = "1442365b0e76feef003992215b11db74", aum = 13)
    public Ship aOO;
    @C0064Am(aul = "7c2a628bd5da8980c9ec74cc367dad6f", aum = 0)
    public float aOo;
    @C0064Am(aul = "28ffe50c03e46febc54cebd70ee9e90b", aum = 1)
    public float aOq;
    @C0064Am(aul = "8c1738a8ecd81022a1ada3a337ee12de", aum = 2)
    public float aOs;
    @C0064Am(aul = "2015fc5e433cbaffbc574a03c735103e", aum = 3)
    public C1556Wo<DamageType, C1649YI> aOu;
    @C0064Am(aul = "1086b68f599c4882fdf9b90b401e6ef0", aum = 4)
    public AdapterLikedList<ShieldAdapter> aOw;
    @C0064Am(aul = "18d4278273e2cbf6472ef6e8782d480b", aum = 5)
    public int aOy;
    @C0064Am(aul = "1086b68f599c4882fdf9b90b401e6ef0", aum = -2)
    public long aUE;
    @C0064Am(aul = "1086b68f599c4882fdf9b90b401e6ef0", aum = -1)
    public byte aUH;
    @C0064Am(aul = "1442365b0e76feef003992215b11db74", aum = -2)
    public long cSU;
    @C0064Am(aul = "1442365b0e76feef003992215b11db74", aum = -1)
    public byte cSW;
    @C0064Am(aul = "2015fc5e433cbaffbc574a03c735103e", aum = -2)
    public long eVK;
    @C0064Am(aul = "7c2a628bd5da8980c9ec74cc367dad6f", aum = -2)
    public long eVM;
    @C0064Am(aul = "6c6fff801279feb764ab3031ca2b6d09", aum = -2)
    public long eVN;
    @C0064Am(aul = "571b767df644d582a1a42a872864665f", aum = -2)
    public long eVO;
    @C0064Am(aul = "2015fc5e433cbaffbc574a03c735103e", aum = -1)
    public byte eVP;
    @C0064Am(aul = "7c2a628bd5da8980c9ec74cc367dad6f", aum = -1)
    public byte eVR;
    @C0064Am(aul = "6c6fff801279feb764ab3031ca2b6d09", aum = -1)
    public byte eVS;
    @C0064Am(aul = "571b767df644d582a1a42a872864665f", aum = -1)
    public byte eVT;
    @C0064Am(aul = "a7476bef8f13230be1e2fb9d7554e3e2", aum = -2)
    public long grX;
    @C0064Am(aul = "7dd53d465b767f8c29d8a3e261ad278b", aum = -2)
    public long grY;
    @C0064Am(aul = "c3ddc4be019314404af79614e2557f19", aum = -2)
    public long grZ;
    @C0064Am(aul = "72ef3c36e162b1312c7c9c2e9777a928", aum = -2)
    public long gsa;
    @C0064Am(aul = "a7476bef8f13230be1e2fb9d7554e3e2", aum = -1)
    public byte gsb;
    @C0064Am(aul = "7dd53d465b767f8c29d8a3e261ad278b", aum = -1)
    public byte gsc;
    @C0064Am(aul = "c3ddc4be019314404af79614e2557f19", aum = -1)
    public byte gsd;
    @C0064Am(aul = "72ef3c36e162b1312c7c9c2e9777a928", aum = -1)
    public byte gse;

    public C6698aru() {
    }

    public C6698aru(C1616Xf xf) {
        super(xf);
    }

    /* renamed from: c */
    public C5663aRz[] mo309c() {
        return Shield._m_fields;
    }

    /* renamed from: d */
    public void mo310d() {
        super.mo310d();
        this.aOo = 0.0f;
        this.aOq = 0.0f;
        this.aOs = 0.0f;
        this.aOu = null;
        this.aOw = null;
        this.aOy = 0;
        this.aOA = 0.0f;
        this.aOC = 0.0f;
        this.aOE = 0.0f;
        this.aOG = null;
        this.aOI = 0.0f;
        this.aOK = 0;
        this.aOM = false;
        this.aOO = null;
        this.eVM = 0;
        this.aBP = 0;
        this.aBR = 0;
        this.eVK = 0;
        this.aUE = 0;
        this.aBQ = 0;
        this.eVN = 0;
        this.grX = 0;
        this.grY = 0;
        this.eVO = 0;
        this.aBO = 0;
        this.grZ = 0;
        this.gsa = 0;
        this.cSU = 0;
        this.eVR = 0;
        this.aBU = 0;
        this.aBW = 0;
        this.eVP = 0;
        this.aUH = 0;
        this.aBV = 0;
        this.eVS = 0;
        this.gsb = 0;
        this.gsc = 0;
        this.eVT = 0;
        this.aBT = 0;
        this.gsd = 0;
        this.gse = 0;
        this.cSW = 0;
    }
}
