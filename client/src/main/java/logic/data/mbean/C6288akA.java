package logic.data.mbean;

import game.script.corporation.NLSCorporation;
import logic.baa.C1616Xf;
import logic.res.code.C5663aRz;
import p001a.C0064Am;
import taikodom.infra.script.I18NString;

/* renamed from: a.akA  reason: case insensitive filesystem */
public class C6288akA extends C2484fi {
    @C0064Am(aul = "12d8796cf8119ce54bcf4c79f7ac6b70", aum = 1)
    public I18NString fLt;
    @C0064Am(aul = "f448d37769d383e7a998859de2058b5e", aum = 2)
    public I18NString fLv;
    @C0064Am(aul = "12d8796cf8119ce54bcf4c79f7ac6b70", aum = -2)
    public long fTO;
    @C0064Am(aul = "f448d37769d383e7a998859de2058b5e", aum = -2)
    public long fTP;
    @C0064Am(aul = "12d8796cf8119ce54bcf4c79f7ac6b70", aum = -1)
    public byte fTQ;
    @C0064Am(aul = "f448d37769d383e7a998859de2058b5e", aum = -1)
    public byte fTR;
    @C0064Am(aul = "db01af686e38c1d96a674c6e1b94695c", aum = 0)

    /* renamed from: ni */
    public I18NString f4752ni;
    @C0064Am(aul = "db01af686e38c1d96a674c6e1b94695c", aum = -2)

    /* renamed from: nl */
    public long f4753nl;
    @C0064Am(aul = "db01af686e38c1d96a674c6e1b94695c", aum = -1)

    /* renamed from: no */
    public byte f4754no;

    public C6288akA() {
    }

    public C6288akA(C1616Xf xf) {
        super(xf);
    }

    /* renamed from: c */
    public C5663aRz[] mo309c() {
        return NLSCorporation.InvitePanel._m_fields;
    }

    /* renamed from: d */
    public void mo310d() {
        super.mo310d();
        this.f4752ni = null;
        this.fLt = null;
        this.fLv = null;
        this.f4753nl = 0;
        this.fTO = 0;
        this.fTP = 0;
        this.f4754no = 0;
        this.fTQ = 0;
        this.fTR = 0;
    }
}
