package logic.data.mbean;

import game.network.message.serializable.C1556Wo;
import game.network.message.serializable.C2686iZ;
import game.network.message.serializable.C3438ra;
import game.script.Character;
import game.script.mission.Mission;
import game.script.mission.MissionTemplate;
import game.script.npc.NPC;
import game.script.npc.NPCType;
import game.script.player.LDParameter;
import logic.baa.C1616Xf;
import logic.res.code.C5663aRz;
import p001a.C0064Am;
import p001a.C4045yZ;

import java.util.List;

/* renamed from: a.EL */
public class C0320EL extends C3805vD {
    @C0064Am(aul = "e0fa7306514f31dc41465348383e107f", aum = 4)
    public C2686iZ<MissionTemplate> cTA;
    @C0064Am(aul = "3f89346783a1d6180de8bb8da73cea70", aum = 5)
    public C3438ra<LDParameter> cTB;
    @C0064Am(aul = "75e1799b374dfaef0b638d6abceca1e2", aum = 6)
    public C2686iZ<NPC> cTC;
    @C0064Am(aul = "c5ca9d7073a5809df420f82abbb4ad18", aum = 7)
    public C1556Wo<NPCType, List<C4045yZ>> cTD;
    @C0064Am(aul = "2a51b805ed2f690695087cf15549baaf", aum = -2)
    public long cTE;
    @C0064Am(aul = "4062c278e371949d2c0f07ac74030db3", aum = -2)
    public long cTF;
    @C0064Am(aul = "d0eaefff3ab9870c9dfe4a9e37ce39ea", aum = -2)
    public long cTG;
    @C0064Am(aul = "c6b83543bb3e8599ce5249ff1dacd857", aum = -2)
    public long cTH;
    @C0064Am(aul = "e0fa7306514f31dc41465348383e107f", aum = -2)
    public long cTI;
    @C0064Am(aul = "3f89346783a1d6180de8bb8da73cea70", aum = -2)
    public long cTJ;
    @C0064Am(aul = "75e1799b374dfaef0b638d6abceca1e2", aum = -2)
    public long cTK;
    @C0064Am(aul = "c5ca9d7073a5809df420f82abbb4ad18", aum = -2)
    public long cTL;
    @C0064Am(aul = "2a51b805ed2f690695087cf15549baaf", aum = -1)
    public byte cTM;
    @C0064Am(aul = "4062c278e371949d2c0f07ac74030db3", aum = -1)
    public byte cTN;
    @C0064Am(aul = "d0eaefff3ab9870c9dfe4a9e37ce39ea", aum = -1)
    public byte cTO;
    @C0064Am(aul = "c6b83543bb3e8599ce5249ff1dacd857", aum = -1)
    public byte cTP;
    @C0064Am(aul = "e0fa7306514f31dc41465348383e107f", aum = -1)
    public byte cTQ;
    @C0064Am(aul = "3f89346783a1d6180de8bb8da73cea70", aum = -1)
    public byte cTR;
    @C0064Am(aul = "75e1799b374dfaef0b638d6abceca1e2", aum = -1)
    public byte cTS;
    @C0064Am(aul = "c5ca9d7073a5809df420f82abbb4ad18", aum = -1)
    public byte cTT;
    @C0064Am(aul = "2a51b805ed2f690695087cf15549baaf", aum = 0)
    public Character cTw;
    @C0064Am(aul = "4062c278e371949d2c0f07ac74030db3", aum = 1)
    public C3438ra<Mission> cTx;
    @C0064Am(aul = "d0eaefff3ab9870c9dfe4a9e37ce39ea", aum = 2)
    public Mission cTy;
    @C0064Am(aul = "c6b83543bb3e8599ce5249ff1dacd857", aum = 3)
    public C2686iZ<MissionTemplate> cTz;

    public C0320EL() {
    }

    public C0320EL(C1616Xf xf) {
        super(xf);
    }

    /* renamed from: c */
    public C5663aRz[] mo309c() {
        return LDScriptingController._m_fields;
    }

    /* renamed from: d */
    public void mo310d() {
        super.mo310d();
        this.cTw = null;
        this.cTx = null;
        this.cTy = null;
        this.cTz = null;
        this.cTA = null;
        this.cTB = null;
        this.cTC = null;
        this.cTD = null;
        this.cTE = 0;
        this.cTF = 0;
        this.cTG = 0;
        this.cTH = 0;
        this.cTI = 0;
        this.cTJ = 0;
        this.cTK = 0;
        this.cTL = 0;
        this.cTM = 0;
        this.cTN = 0;
        this.cTO = 0;
        this.cTP = 0;
        this.cTQ = 0;
        this.cTR = 0;
        this.cTS = 0;
        this.cTT = 0;
    }
}
