package logic.data.mbean;

import game.script.nls.NLSConsoleAlert;
import logic.baa.C1616Xf;
import logic.res.code.C5663aRz;
import p001a.C0064Am;
import taikodom.infra.script.I18NString;

/* renamed from: a.cw */
public class C2221cw extends C2484fi {
    @C0064Am(aul = "53a5e154eaa60fa495aefdc7364897dd", aum = 2)

    /* renamed from: sA */
    public I18NString f6305sA;
    @C0064Am(aul = "d920a778d04bcdf687a1014fc582f9c6", aum = 3)

    /* renamed from: sB */
    public I18NString f6306sB;
    @C0064Am(aul = "d9b04fd1853e3eae57fe927c01493660", aum = 4)

    /* renamed from: sC */
    public I18NString f6307sC;
    @C0064Am(aul = "49e40b0128401af24cb39ceefa1b24c1", aum = 5)

    /* renamed from: sD */
    public I18NString f6308sD;
    @C0064Am(aul = "e49d47cfa699a9fa55a977b518b865c8", aum = 6)

    /* renamed from: sE */
    public I18NString f6309sE;
    @C0064Am(aul = "547dbfb139e3ba32c647041fc34cdb86", aum = 7)

    /* renamed from: sF */
    public I18NString f6310sF;
    @C0064Am(aul = "03dda2a523e90f981e032295297744f4", aum = 8)

    /* renamed from: sG */
    public I18NString f6311sG;
    @C0064Am(aul = "a6a33f1d4a6335c7ba72c6486d65a4d6", aum = 9)

    /* renamed from: sH */
    public I18NString f6312sH;
    @C0064Am(aul = "210e99e0232ae3bcefdb8e98026fdea5", aum = 10)

    /* renamed from: sI */
    public I18NString f6313sI;
    @C0064Am(aul = "16b92e3fd246bbff056846d665bd1260", aum = 11)

    /* renamed from: sJ */
    public I18NString f6314sJ;
    @C0064Am(aul = "ae343e37be8554c45240cb7afe2c617a", aum = 12)

    /* renamed from: sK */
    public I18NString f6315sK;
    @C0064Am(aul = "054b285f5d351edcba387d2a61352a0c", aum = 13)

    /* renamed from: sL */
    public I18NString f6316sL;
    @C0064Am(aul = "3e39b5bc958d1c8dd36e4aafdffae6d7", aum = 14)

    /* renamed from: sM */
    public I18NString f6317sM;
    @C0064Am(aul = "a8679a8982acb6545683a02866bb0050", aum = 15)

    /* renamed from: sN */
    public I18NString f6318sN;
    @C0064Am(aul = "46ce4c45fd8c91be4dcc77f04cc675d5", aum = 16)

    /* renamed from: sO */
    public I18NString f6319sO;
    @C0064Am(aul = "d448408348b3761fc12eeb45e7cf18ff", aum = 17)

    /* renamed from: sP */
    public I18NString f6320sP;
    @C0064Am(aul = "b66aed5c58409c34f1e5c89a522d6dac", aum = 18)

    /* renamed from: sQ */
    public I18NString f6321sQ;
    @C0064Am(aul = "8a6465eea60c43ccc2135e288908ae0e", aum = 19)

    /* renamed from: sR */
    public I18NString f6322sR;
    @C0064Am(aul = "9172c13ddf98633c8f4b2a72f7adf1a9", aum = 20)

    /* renamed from: sS */
    public I18NString f6323sS;
    @C0064Am(aul = "21f4d99bf16fab46800b26d8e5651201", aum = 21)

    /* renamed from: sT */
    public I18NString f6324sT;
    @C0064Am(aul = "279b6201809c7e0ee68351c6305e04f7", aum = 22)

    /* renamed from: sU */
    public I18NString f6325sU;
    @C0064Am(aul = "0873adbeb56f53779a1d511911a50132", aum = 23)

    /* renamed from: sV */
    public I18NString f6326sV;
    @C0064Am(aul = "7b670577fe04b1a93c762914211d7f14", aum = 24)

    /* renamed from: sW */
    public I18NString f6327sW;
    @C0064Am(aul = "1714531014fa205490f7b3c27baff745", aum = 25)

    /* renamed from: sX */
    public I18NString f6328sX;
    @C0064Am(aul = "cd86842d14cede45c504c522e0249358", aum = 26)

    /* renamed from: sY */
    public I18NString f6329sY;
    @C0064Am(aul = "92c720a59d97af5a2d9a5af1127b9087", aum = 27)

    /* renamed from: sZ */
    public I18NString f6330sZ;
    @C0064Am(aul = "3ee761f18252e997aeea9dde42c68957", aum = 0)

    /* renamed from: sy */
    public I18NString f6331sy;
    @C0064Am(aul = "62ba0dcd3593ed732233d0787714496c", aum = 1)

    /* renamed from: sz */
    public I18NString f6332sz;
    @C0064Am(aul = "1714531014fa205490f7b3c27baff745", aum = -2)

    /* renamed from: tA */
    public long f6333tA;
    @C0064Am(aul = "cd86842d14cede45c504c522e0249358", aum = -2)

    /* renamed from: tB */
    public long f6334tB;
    @C0064Am(aul = "92c720a59d97af5a2d9a5af1127b9087", aum = -2)

    /* renamed from: tC */
    public long f6335tC;
    @C0064Am(aul = "765b3b2178c70eb86be9b848b26bfeea", aum = -2)

    /* renamed from: tD */
    public long f6336tD;
    @C0064Am(aul = "3ee761f18252e997aeea9dde42c68957", aum = -1)

    /* renamed from: tE */
    public byte f6337tE;
    @C0064Am(aul = "62ba0dcd3593ed732233d0787714496c", aum = -1)

    /* renamed from: tF */
    public byte f6338tF;
    @C0064Am(aul = "53a5e154eaa60fa495aefdc7364897dd", aum = -1)

    /* renamed from: tG */
    public byte f6339tG;
    @C0064Am(aul = "d920a778d04bcdf687a1014fc582f9c6", aum = -1)

    /* renamed from: tH */
    public byte f6340tH;
    @C0064Am(aul = "d9b04fd1853e3eae57fe927c01493660", aum = -1)

    /* renamed from: tI */
    public byte f6341tI;
    @C0064Am(aul = "49e40b0128401af24cb39ceefa1b24c1", aum = -1)

    /* renamed from: tJ */
    public byte f6342tJ;
    @C0064Am(aul = "e49d47cfa699a9fa55a977b518b865c8", aum = -1)

    /* renamed from: tK */
    public byte f6343tK;
    @C0064Am(aul = "547dbfb139e3ba32c647041fc34cdb86", aum = -1)

    /* renamed from: tL */
    public byte f6344tL;
    @C0064Am(aul = "03dda2a523e90f981e032295297744f4", aum = -1)

    /* renamed from: tM */
    public byte f6345tM;
    @C0064Am(aul = "a6a33f1d4a6335c7ba72c6486d65a4d6", aum = -1)

    /* renamed from: tN */
    public byte f6346tN;
    @C0064Am(aul = "210e99e0232ae3bcefdb8e98026fdea5", aum = -1)

    /* renamed from: tO */
    public byte f6347tO;
    @C0064Am(aul = "16b92e3fd246bbff056846d665bd1260", aum = -1)

    /* renamed from: tP */
    public byte f6348tP;
    @C0064Am(aul = "ae343e37be8554c45240cb7afe2c617a", aum = -1)

    /* renamed from: tQ */
    public byte f6349tQ;
    @C0064Am(aul = "054b285f5d351edcba387d2a61352a0c", aum = -1)

    /* renamed from: tR */
    public byte f6350tR;
    @C0064Am(aul = "3e39b5bc958d1c8dd36e4aafdffae6d7", aum = -1)

    /* renamed from: tS */
    public byte f6351tS;
    @C0064Am(aul = "a8679a8982acb6545683a02866bb0050", aum = -1)

    /* renamed from: tT */
    public byte f6352tT;
    @C0064Am(aul = "46ce4c45fd8c91be4dcc77f04cc675d5", aum = -1)

    /* renamed from: tU */
    public byte f6353tU;
    @C0064Am(aul = "d448408348b3761fc12eeb45e7cf18ff", aum = -1)

    /* renamed from: tV */
    public byte f6354tV;
    @C0064Am(aul = "b66aed5c58409c34f1e5c89a522d6dac", aum = -1)

    /* renamed from: tW */
    public byte f6355tW;
    @C0064Am(aul = "8a6465eea60c43ccc2135e288908ae0e", aum = -1)

    /* renamed from: tX */
    public byte f6356tX;
    @C0064Am(aul = "9172c13ddf98633c8f4b2a72f7adf1a9", aum = -1)

    /* renamed from: tY */
    public byte f6357tY;
    @C0064Am(aul = "21f4d99bf16fab46800b26d8e5651201", aum = -1)

    /* renamed from: tZ */
    public byte f6358tZ;
    @C0064Am(aul = "765b3b2178c70eb86be9b848b26bfeea", aum = 28)

    /* renamed from: ta */
    public I18NString f6359ta;
    @C0064Am(aul = "3ee761f18252e997aeea9dde42c68957", aum = -2)

    /* renamed from: tb */
    public long f6360tb;
    @C0064Am(aul = "62ba0dcd3593ed732233d0787714496c", aum = -2)

    /* renamed from: tc */
    public long f6361tc;
    @C0064Am(aul = "53a5e154eaa60fa495aefdc7364897dd", aum = -2)

    /* renamed from: td */
    public long f6362td;
    @C0064Am(aul = "d920a778d04bcdf687a1014fc582f9c6", aum = -2)

    /* renamed from: te */
    public long f6363te;
    @C0064Am(aul = "d9b04fd1853e3eae57fe927c01493660", aum = -2)

    /* renamed from: tf */
    public long f6364tf;
    @C0064Am(aul = "49e40b0128401af24cb39ceefa1b24c1", aum = -2)

    /* renamed from: tg */
    public long f6365tg;
    @C0064Am(aul = "e49d47cfa699a9fa55a977b518b865c8", aum = -2)

    /* renamed from: th */
    public long f6366th;
    @C0064Am(aul = "547dbfb139e3ba32c647041fc34cdb86", aum = -2)

    /* renamed from: ti */
    public long f6367ti;
    @C0064Am(aul = "03dda2a523e90f981e032295297744f4", aum = -2)

    /* renamed from: tj */
    public long f6368tj;
    @C0064Am(aul = "a6a33f1d4a6335c7ba72c6486d65a4d6", aum = -2)

    /* renamed from: tk */
    public long f6369tk;
    @C0064Am(aul = "210e99e0232ae3bcefdb8e98026fdea5", aum = -2)

    /* renamed from: tl */
    public long f6370tl;
    @C0064Am(aul = "16b92e3fd246bbff056846d665bd1260", aum = -2)

    /* renamed from: tm */
    public long f6371tm;
    @C0064Am(aul = "ae343e37be8554c45240cb7afe2c617a", aum = -2)

    /* renamed from: tn */
    public long f6372tn;
    @C0064Am(aul = "054b285f5d351edcba387d2a61352a0c", aum = -2)

    /* renamed from: to */
    public long f6373to;
    @C0064Am(aul = "3e39b5bc958d1c8dd36e4aafdffae6d7", aum = -2)

    /* renamed from: tp */
    public long f6374tp;
    @C0064Am(aul = "a8679a8982acb6545683a02866bb0050", aum = -2)

    /* renamed from: tq */
    public long f6375tq;
    @C0064Am(aul = "46ce4c45fd8c91be4dcc77f04cc675d5", aum = -2)

    /* renamed from: tr */
    public long f6376tr;
    @C0064Am(aul = "d448408348b3761fc12eeb45e7cf18ff", aum = -2)

    /* renamed from: ts */
    public long f6377ts;
    @C0064Am(aul = "b66aed5c58409c34f1e5c89a522d6dac", aum = -2)

    /* renamed from: tt */
    public long f6378tt;
    @C0064Am(aul = "8a6465eea60c43ccc2135e288908ae0e", aum = -2)

    /* renamed from: tu */
    public long f6379tu;
    @C0064Am(aul = "9172c13ddf98633c8f4b2a72f7adf1a9", aum = -2)

    /* renamed from: tv */
    public long f6380tv;
    @C0064Am(aul = "21f4d99bf16fab46800b26d8e5651201", aum = -2)

    /* renamed from: tw */
    public long f6381tw;
    @C0064Am(aul = "279b6201809c7e0ee68351c6305e04f7", aum = -2)

    /* renamed from: tx */
    public long f6382tx;
    @C0064Am(aul = "0873adbeb56f53779a1d511911a50132", aum = -2)

    /* renamed from: ty */
    public long f6383ty;
    @C0064Am(aul = "7b670577fe04b1a93c762914211d7f14", aum = -2)

    /* renamed from: tz */
    public long f6384tz;
    @C0064Am(aul = "279b6201809c7e0ee68351c6305e04f7", aum = -1)

    /* renamed from: ua */
    public byte f6385ua;
    @C0064Am(aul = "0873adbeb56f53779a1d511911a50132", aum = -1)

    /* renamed from: ub */
    public byte f6386ub;
    @C0064Am(aul = "7b670577fe04b1a93c762914211d7f14", aum = -1)

    /* renamed from: uc */
    public byte f6387uc;
    @C0064Am(aul = "1714531014fa205490f7b3c27baff745", aum = -1)

    /* renamed from: ud */
    public byte f6388ud;
    @C0064Am(aul = "cd86842d14cede45c504c522e0249358", aum = -1)

    /* renamed from: ue */
    public byte f6389ue;
    @C0064Am(aul = "92c720a59d97af5a2d9a5af1127b9087", aum = -1)

    /* renamed from: uf */
    public byte f6390uf;
    @C0064Am(aul = "765b3b2178c70eb86be9b848b26bfeea", aum = -1)

    /* renamed from: ug */
    public byte f6391ug;

    public C2221cw() {
    }

    public C2221cw(C1616Xf xf) {
        super(xf);
    }

    /* renamed from: c */
    public C5663aRz[] mo309c() {
        return NLSConsoleAlert._m_fields;
    }

    /* renamed from: d */
    public void mo310d() {
        super.mo310d();
        this.f6331sy = null;
        this.f6332sz = null;
        this.f6305sA = null;
        this.f6306sB = null;
        this.f6307sC = null;
        this.f6308sD = null;
        this.f6309sE = null;
        this.f6310sF = null;
        this.f6311sG = null;
        this.f6312sH = null;
        this.f6313sI = null;
        this.f6314sJ = null;
        this.f6315sK = null;
        this.f6316sL = null;
        this.f6317sM = null;
        this.f6318sN = null;
        this.f6319sO = null;
        this.f6320sP = null;
        this.f6321sQ = null;
        this.f6322sR = null;
        this.f6323sS = null;
        this.f6324sT = null;
        this.f6325sU = null;
        this.f6326sV = null;
        this.f6327sW = null;
        this.f6328sX = null;
        this.f6329sY = null;
        this.f6330sZ = null;
        this.f6359ta = null;
        this.f6360tb = 0;
        this.f6361tc = 0;
        this.f6362td = 0;
        this.f6363te = 0;
        this.f6364tf = 0;
        this.f6365tg = 0;
        this.f6366th = 0;
        this.f6367ti = 0;
        this.f6368tj = 0;
        this.f6369tk = 0;
        this.f6370tl = 0;
        this.f6371tm = 0;
        this.f6372tn = 0;
        this.f6373to = 0;
        this.f6374tp = 0;
        this.f6375tq = 0;
        this.f6376tr = 0;
        this.f6377ts = 0;
        this.f6378tt = 0;
        this.f6379tu = 0;
        this.f6380tv = 0;
        this.f6381tw = 0;
        this.f6382tx = 0;
        this.f6383ty = 0;
        this.f6384tz = 0;
        this.f6333tA = 0;
        this.f6334tB = 0;
        this.f6335tC = 0;
        this.f6336tD = 0;
        this.f6337tE = 0;
        this.f6338tF = 0;
        this.f6339tG = 0;
        this.f6340tH = 0;
        this.f6341tI = 0;
        this.f6342tJ = 0;
        this.f6343tK = 0;
        this.f6344tL = 0;
        this.f6345tM = 0;
        this.f6346tN = 0;
        this.f6347tO = 0;
        this.f6348tP = 0;
        this.f6349tQ = 0;
        this.f6350tR = 0;
        this.f6351tS = 0;
        this.f6352tT = 0;
        this.f6353tU = 0;
        this.f6354tV = 0;
        this.f6355tW = 0;
        this.f6356tX = 0;
        this.f6357tY = 0;
        this.f6358tZ = 0;
        this.f6385ua = 0;
        this.f6386ub = 0;
        this.f6387uc = 0;
        this.f6388ud = 0;
        this.f6389ue = 0;
        this.f6390uf = 0;
        this.f6391ug = 0;
    }
}
