package logic.data.mbean;

import game.script.item.ScatterWeapon;
import logic.baa.C1616Xf;
import logic.res.code.C5663aRz;
import p001a.C0064Am;

/* renamed from: a.iq */
public class C2708iq extends C2841ku {
    @C0064Am(aul = "c85c82d6652441f7d8136839f58c0f0d", aum = 0)

    /* renamed from: XT */
    public int f8263XT;
    @C0064Am(aul = "b2d42ce0f7356c9b0278280b681f70f3", aum = 1)

    /* renamed from: XU */
    public float f8264XU;
    @C0064Am(aul = "c85c82d6652441f7d8136839f58c0f0d", aum = -2)

    /* renamed from: XV */
    public long f8265XV;
    @C0064Am(aul = "b2d42ce0f7356c9b0278280b681f70f3", aum = -2)

    /* renamed from: XW */
    public long f8266XW;
    @C0064Am(aul = "c85c82d6652441f7d8136839f58c0f0d", aum = -1)

    /* renamed from: XX */
    public byte f8267XX;
    @C0064Am(aul = "b2d42ce0f7356c9b0278280b681f70f3", aum = -1)

    /* renamed from: XY */
    public byte f8268XY;

    public C2708iq() {
    }

    public C2708iq(C1616Xf xf) {
        super(xf);
    }

    /* renamed from: c */
    public C5663aRz[] mo309c() {
        return ScatterWeapon._m_fields;
    }

    /* renamed from: d */
    public void mo310d() {
        super.mo310d();
        this.f8263XT = 0;
        this.f8264XU = 0.0f;
        this.f8265XV = 0;
        this.f8266XW = 0;
        this.f8267XX = 0;
        this.f8268XY = 0;
    }
}
