package logic.data.mbean;

import game.network.message.serializable.C2686iZ;
import game.script.avatar.AvatarManager;
import game.script.avatar.ColorWrapper;
import game.script.avatar.TextureGrid;
import game.script.avatar.TextureGridType;
import game.script.avatar.body.TextureScheme;
import logic.baa.C1616Xf;
import logic.res.code.C5663aRz;
import p001a.C0064Am;

import java.util.UUID;

/* renamed from: a.rb */
public class C3439rb extends C5292aDs {
    @C0064Am(aul = "87a9be45160e115d8ffb3a4910db0999", aum = 0)
    public C2686iZ<TextureGridType> aZn;
    @C0064Am(aul = "8cf46c08947b5eb1ffb54ccee60c9719", aum = 1)
    public C2686iZ<TextureGrid> aZo;
    @C0064Am(aul = "6b39d2ddb8c921d060ccf637bc65b857", aum = 2)
    public C2686iZ<ColorWrapper> aZp;
    @C0064Am(aul = "8982c6f60565212f2659bfdb26e1ad39", aum = 3)
    public C2686iZ<TextureScheme> aZq;
    @C0064Am(aul = "87a9be45160e115d8ffb3a4910db0999", aum = -2)
    public long aZr;
    @C0064Am(aul = "8cf46c08947b5eb1ffb54ccee60c9719", aum = -2)
    public long aZs;
    @C0064Am(aul = "6b39d2ddb8c921d060ccf637bc65b857", aum = -2)
    public long aZt;
    @C0064Am(aul = "8982c6f60565212f2659bfdb26e1ad39", aum = -2)
    public long aZu;
    @C0064Am(aul = "87a9be45160e115d8ffb3a4910db0999", aum = -1)
    public byte aZv;
    @C0064Am(aul = "8cf46c08947b5eb1ffb54ccee60c9719", aum = -1)
    public byte aZw;
    @C0064Am(aul = "6b39d2ddb8c921d060ccf637bc65b857", aum = -1)
    public byte aZx;
    @C0064Am(aul = "8982c6f60565212f2659bfdb26e1ad39", aum = -1)
    public byte aZy;
    @C0064Am(aul = "a9d1dbe0a1a6b1765d26b7e3b54d5117", aum = 4)

    /* renamed from: bK */
    public UUID f9051bK;
    @C0064Am(aul = "a9d1dbe0a1a6b1765d26b7e3b54d5117", aum = -2)

    /* renamed from: oL */
    public long f9052oL;
    @C0064Am(aul = "a9d1dbe0a1a6b1765d26b7e3b54d5117", aum = -1)

    /* renamed from: oS */
    public byte f9053oS;

    public C3439rb() {
    }

    public C3439rb(C1616Xf xf) {
        super(xf);
    }

    /* renamed from: c */
    public C5663aRz[] mo309c() {
        return AvatarManager.ColorManager._m_fields;
    }

    /* renamed from: d */
    public void mo310d() {
        super.mo310d();
        this.aZn = null;
        this.aZo = null;
        this.aZp = null;
        this.aZq = null;
        this.f9051bK = null;
        this.aZr = 0;
        this.aZs = 0;
        this.aZt = 0;
        this.aZu = 0;
        this.f9052oL = 0;
        this.aZv = 0;
        this.aZw = 0;
        this.aZx = 0;
        this.aZy = 0;
        this.f9053oS = 0;
    }
}
