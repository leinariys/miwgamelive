package logic.data.mbean;

import game.script.mission.Mission;
import game.script.mission.MissionStep;
import logic.baa.C1616Xf;
import logic.res.code.C5663aRz;
import p001a.C0064Am;

/* renamed from: a.bs */
public class C2086bs extends C5292aDs {
    @C0064Am(aul = "0fc3adc13d38013aef690f39a7d19e0f", aum = 0)
    public String handle;
    @C0064Am(aul = "e6aaf20d811c49dbb635da0ff7a67dde", aum = 1)

    /* renamed from: oi */
    public Mission.C0015a f5910oi;
    @C0064Am(aul = "c7d42a041f20b11297999fd79456f449", aum = 2)

    /* renamed from: oj */
    public Object[] f5911oj;
    @C0064Am(aul = "0fc3adc13d38013aef690f39a7d19e0f", aum = -2)

    /* renamed from: ok */
    public long f5912ok;
    @C0064Am(aul = "e6aaf20d811c49dbb635da0ff7a67dde", aum = -2)

    /* renamed from: ol */
    public long f5913ol;
    @C0064Am(aul = "c7d42a041f20b11297999fd79456f449", aum = -2)

    /* renamed from: om */
    public long f5914om;
    @C0064Am(aul = "0fc3adc13d38013aef690f39a7d19e0f", aum = -1)

    /* renamed from: on */
    public byte f5915on;
    @C0064Am(aul = "e6aaf20d811c49dbb635da0ff7a67dde", aum = -1)

    /* renamed from: oo */
    public byte f5916oo;
    @C0064Am(aul = "c7d42a041f20b11297999fd79456f449", aum = -1)

    /* renamed from: op */
    public byte f5917op;

    public C2086bs() {
    }

    public C2086bs(C1616Xf xf) {
        super(xf);
    }

    /* renamed from: c */
    public C5663aRz[] mo309c() {
        return MissionStep._m_fields;
    }

    /* renamed from: d */
    public void mo310d() {
        super.mo310d();
        this.handle = null;
        this.f5910oi = null;
        this.f5911oj = null;
        this.f5912ok = 0;
        this.f5913ol = 0;
        this.f5914om = 0;
        this.f5915on = 0;
        this.f5916oo = 0;
        this.f5917op = 0;
    }
}
