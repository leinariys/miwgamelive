package logic.data.mbean;

import game.network.message.serializable.C3438ra;
import game.script.npcchat.NPCChat;
import game.script.npcchat.NPCSpeech;
import logic.baa.C1616Xf;
import logic.res.code.C5663aRz;
import p001a.C0064Am;

import java.util.UUID;

/* renamed from: a.aAE */
public class aAE extends C3805vD {
    @C0064Am(aul = "897827c4a52263986eab69ee03ae5f7a", aum = 1)

    /* renamed from: bK */
    public UUID f2290bK;
    @C0064Am(aul = "8c445ee95fe521d52cbb444c5f899cb6", aum = 0)
    public C3438ra<NPCSpeech> get;
    @C0064Am(aul = "f88dd54f32071278882ab2612f177966", aum = 2)
    public String handle;
    @C0064Am(aul = "8c445ee95fe521d52cbb444c5f899cb6", aum = -2)
    public long heN;
    @C0064Am(aul = "8c445ee95fe521d52cbb444c5f899cb6", aum = -1)
    public byte heO;
    @C0064Am(aul = "897827c4a52263986eab69ee03ae5f7a", aum = -2)

    /* renamed from: oL */
    public long f2291oL;
    @C0064Am(aul = "897827c4a52263986eab69ee03ae5f7a", aum = -1)

    /* renamed from: oS */
    public byte f2292oS;
    @C0064Am(aul = "f88dd54f32071278882ab2612f177966", aum = -2)

    /* renamed from: ok */
    public long f2293ok;
    @C0064Am(aul = "f88dd54f32071278882ab2612f177966", aum = -1)

    /* renamed from: on */
    public byte f2294on;

    public aAE() {
    }

    public aAE(C1616Xf xf) {
        super(xf);
    }

    /* renamed from: c */
    public C5663aRz[] mo309c() {
        return NPCChat._m_fields;
    }

    /* renamed from: d */
    public void mo310d() {
        super.mo310d();
        this.get = null;
        this.f2290bK = null;
        this.handle = null;
        this.heN = 0;
        this.f2291oL = 0;
        this.f2293ok = 0;
        this.heO = 0;
        this.f2292oS = 0;
        this.f2294on = 0;
    }
}
