package logic.data.mbean;

import game.script.mission.scripting.ProtectMissionScript;
import game.script.missiontemplate.WaypointDat;
import logic.baa.C1616Xf;
import logic.res.code.C5663aRz;
import p001a.C0064Am;

/* renamed from: a.aQG */
public class aQG extends C2955mD {
    @C0064Am(aul = "753066823a54b667f074b5045532b786", aum = 2)
    public ProtectMissionScript dbM;
    @C0064Am(aul = "d6e0a30e66e164d521bbd8319f70afb3", aum = 0)
    public WaypointDat dbP;
    @C0064Am(aul = "4addf83c5a7ed0fdfadf1cf854668b6c", aum = 1)
    public int dbQ;
    @C0064Am(aul = "753066823a54b667f074b5045532b786", aum = -2)

    /* renamed from: dl */
    public long f3591dl;
    @C0064Am(aul = "d6e0a30e66e164d521bbd8319f70afb3", aum = -2)
    public long dql;
    @C0064Am(aul = "d6e0a30e66e164d521bbd8319f70afb3", aum = -1)
    public byte dqs;
    @C0064Am(aul = "753066823a54b667f074b5045532b786", aum = -1)

    /* renamed from: ds */
    public byte f3592ds;
    @C0064Am(aul = "4addf83c5a7ed0fdfadf1cf854668b6c", aum = -2)
    public long iET;
    @C0064Am(aul = "4addf83c5a7ed0fdfadf1cf854668b6c", aum = -1)
    public byte iEU;

    public aQG() {
    }

    public aQG(C1616Xf xf) {
        super(xf);
    }

    /* renamed from: c */
    public C5663aRz[] mo309c() {
        return ProtectMissionScript.StateC._m_fields;
    }

    /* renamed from: d */
    public void mo310d() {
        super.mo310d();
        this.dbP = null;
        this.dbQ = 0;
        this.dbM = null;
        this.dql = 0;
        this.iET = 0;
        this.f3591dl = 0;
        this.dqs = 0;
        this.iEU = 0;
        this.f3592ds = 0;
    }
}
