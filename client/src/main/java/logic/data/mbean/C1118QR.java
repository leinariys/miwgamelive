package logic.data.mbean;

import game.script.npc.NPCType;
import game.script.spacezone.population.NPCSpawn;
import logic.baa.C1616Xf;
import logic.res.code.C5663aRz;
import p001a.C0064Am;

import java.util.UUID;

/* renamed from: a.QR */
public class C1118QR extends C3805vD {
    @C0064Am(aul = "5aa6c3e109b68413669cf3785facf699", aum = 2)

    /* renamed from: bK */
    public UUID f1433bK;
    @C0064Am(aul = "900ac7392aaacff26e0f6729cb033339", aum = 1)
    public int dXm;
    @C0064Am(aul = "900ac7392aaacff26e0f6729cb033339", aum = -2)
    public long dXn;
    @C0064Am(aul = "900ac7392aaacff26e0f6729cb033339", aum = -1)
    public byte dXo;
    @C0064Am(aul = "47f7a456f6537a56cc5f3ddff6e5f4ad", aum = 3)
    public String handle;
    @C0064Am(aul = "55e40dce4d3d92b48256005651b38513", aum = 0)

    /* renamed from: nC */
    public NPCType f1434nC;
    @C0064Am(aul = "55e40dce4d3d92b48256005651b38513", aum = -2)

    /* renamed from: nG */
    public long f1435nG;
    @C0064Am(aul = "55e40dce4d3d92b48256005651b38513", aum = -1)

    /* renamed from: nK */
    public byte f1436nK;
    @C0064Am(aul = "5aa6c3e109b68413669cf3785facf699", aum = -2)

    /* renamed from: oL */
    public long f1437oL;
    @C0064Am(aul = "5aa6c3e109b68413669cf3785facf699", aum = -1)

    /* renamed from: oS */
    public byte f1438oS;
    @C0064Am(aul = "47f7a456f6537a56cc5f3ddff6e5f4ad", aum = -2)

    /* renamed from: ok */
    public long f1439ok;
    @C0064Am(aul = "47f7a456f6537a56cc5f3ddff6e5f4ad", aum = -1)

    /* renamed from: on */
    public byte f1440on;

    public C1118QR() {
    }

    public C1118QR(C1616Xf xf) {
        super(xf);
    }

    /* renamed from: c */
    public C5663aRz[] mo309c() {
        return NPCSpawn._m_fields;
    }

    /* renamed from: d */
    public void mo310d() {
        super.mo310d();
        this.f1434nC = null;
        this.dXm = 0;
        this.f1433bK = null;
        this.handle = null;
        this.f1435nG = 0;
        this.dXn = 0;
        this.f1437oL = 0;
        this.f1439ok = 0;
        this.f1436nK = 0;
        this.dXo = 0;
        this.f1438oS = 0;
        this.f1440on = 0;
    }
}
