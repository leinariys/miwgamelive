package logic.data.mbean;

import game.script.item.buff.amplifier.CruiseSpeedAmplifierType;
import logic.baa.C1616Xf;
import logic.res.code.C5663aRz;
import p001a.C0064Am;
import p001a.C3892wO;

/* renamed from: a.iE */
public class C2656iE extends C2421fH {
    @C0064Am(aul = "fa2538493a59c809f13552f4d5de8222", aum = 0)

    /* renamed from: Vs */
    public C3892wO f8099Vs;
    @C0064Am(aul = "112690eaa6aa0779a1f7f251420cf93d", aum = 1)

    /* renamed from: Vu */
    public C3892wO f8100Vu;
    @C0064Am(aul = "cb4486be44ee553ee886d62682bad125", aum = 2)

    /* renamed from: Vw */
    public C3892wO f8101Vw;
    @C0064Am(aul = "2e5e8407c127f79923ad52859fda599c", aum = 3)

    /* renamed from: Vy */
    public C3892wO f8102Vy;
    @C0064Am(aul = "43c372a8b928b0afd43e816032354d61", aum = -2)

    /* renamed from: YA */
    public long f8103YA;
    @C0064Am(aul = "e35b1b356e502743c81bfc406ba72728", aum = -2)

    /* renamed from: YB */
    public long f8104YB;
    @C0064Am(aul = "fa2538493a59c809f13552f4d5de8222", aum = -1)

    /* renamed from: YC */
    public byte f8105YC;
    @C0064Am(aul = "112690eaa6aa0779a1f7f251420cf93d", aum = -1)

    /* renamed from: YD */
    public byte f8106YD;
    @C0064Am(aul = "cb4486be44ee553ee886d62682bad125", aum = -1)

    /* renamed from: YE */
    public byte f8107YE;
    @C0064Am(aul = "2e5e8407c127f79923ad52859fda599c", aum = -1)

    /* renamed from: YF */
    public byte f8108YF;
    @C0064Am(aul = "43c372a8b928b0afd43e816032354d61", aum = -1)

    /* renamed from: YG */
    public byte f8109YG;
    @C0064Am(aul = "e35b1b356e502743c81bfc406ba72728", aum = -1)

    /* renamed from: YH */
    public byte f8110YH;
    @C0064Am(aul = "43c372a8b928b0afd43e816032354d61", aum = 4)

    /* renamed from: Yu */
    public C3892wO f8111Yu;
    @C0064Am(aul = "e35b1b356e502743c81bfc406ba72728", aum = 5)

    /* renamed from: Yv */
    public C3892wO f8112Yv;
    @C0064Am(aul = "fa2538493a59c809f13552f4d5de8222", aum = -2)

    /* renamed from: Yw */
    public long f8113Yw;
    @C0064Am(aul = "112690eaa6aa0779a1f7f251420cf93d", aum = -2)

    /* renamed from: Yx */
    public long f8114Yx;
    @C0064Am(aul = "cb4486be44ee553ee886d62682bad125", aum = -2)

    /* renamed from: Yy */
    public long f8115Yy;
    @C0064Am(aul = "2e5e8407c127f79923ad52859fda599c", aum = -2)

    /* renamed from: Yz */
    public long f8116Yz;

    public C2656iE() {
    }

    public C2656iE(C1616Xf xf) {
        super(xf);
    }

    /* renamed from: c */
    public C5663aRz[] mo309c() {
        return CruiseSpeedAmplifierType._m_fields;
    }

    /* renamed from: d */
    public void mo310d() {
        super.mo310d();
        this.f8099Vs = null;
        this.f8100Vu = null;
        this.f8101Vw = null;
        this.f8102Vy = null;
        this.f8111Yu = null;
        this.f8112Yv = null;
        this.f8113Yw = 0;
        this.f8114Yx = 0;
        this.f8115Yy = 0;
        this.f8116Yz = 0;
        this.f8103YA = 0;
        this.f8104YB = 0;
        this.f8105YC = 0;
        this.f8106YD = 0;
        this.f8107YE = 0;
        this.f8108YF = 0;
        this.f8109YG = 0;
        this.f8110YH = 0;
    }
}
