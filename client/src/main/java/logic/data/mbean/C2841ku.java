package logic.data.mbean;

import game.script.item.Clip;
import game.script.item.ClipType;
import game.script.item.ProjectileWeapon;
import logic.baa.C1616Xf;
import logic.res.code.C5663aRz;
import p001a.C0064Am;

/* renamed from: a.ku */
public class C2841ku extends C1716ZO {
    @C0064Am(aul = "f88658dd58cf697d90d167ec4ab81e2c", aum = 1)
    public ClipType atA;
    @C0064Am(aul = "96bf36d74ba3e2e929e984ac67e6baf9", aum = 2)
    public int atB;
    @C0064Am(aul = "7fa1f36226edb8d37c852d19e09c2fa4", aum = 3)
    public Clip atC;
    @C0064Am(aul = "affa215a6dcc1d34cfeb582beb9fb930", aum = 4)
    public ProjectileWeapon.ProjectileAdapter atD;
    @C0064Am(aul = "a1f95993c4f572dc5d31c48d79c72252", aum = -2)
    public long atE;
    @C0064Am(aul = "f88658dd58cf697d90d167ec4ab81e2c", aum = -2)
    public long atF;
    @C0064Am(aul = "96bf36d74ba3e2e929e984ac67e6baf9", aum = -2)
    public long atG;
    @C0064Am(aul = "7fa1f36226edb8d37c852d19e09c2fa4", aum = -2)
    public long atH;
    @C0064Am(aul = "affa215a6dcc1d34cfeb582beb9fb930", aum = -2)
    public long atI;
    @C0064Am(aul = "a1f95993c4f572dc5d31c48d79c72252", aum = -1)
    public byte atJ;
    @C0064Am(aul = "f88658dd58cf697d90d167ec4ab81e2c", aum = -1)
    public byte atK;
    @C0064Am(aul = "96bf36d74ba3e2e929e984ac67e6baf9", aum = -1)
    public byte atL;
    @C0064Am(aul = "7fa1f36226edb8d37c852d19e09c2fa4", aum = -1)
    public byte atM;
    @C0064Am(aul = "affa215a6dcc1d34cfeb582beb9fb930", aum = -1)
    public byte atN;
    @C0064Am(aul = "a1f95993c4f572dc5d31c48d79c72252", aum = 0)
    public int atz;

    public C2841ku() {
    }

    public C2841ku(C1616Xf xf) {
        super(xf);
    }

    /* renamed from: c */
    public C5663aRz[] mo309c() {
        return ProjectileWeapon._m_fields;
    }

    /* renamed from: d */
    public void mo310d() {
        super.mo310d();
        this.atz = 0;
        this.atA = null;
        this.atB = 0;
        this.atC = null;
        this.atD = null;
        this.atE = 0;
        this.atF = 0;
        this.atG = 0;
        this.atH = 0;
        this.atI = 0;
        this.atJ = 0;
        this.atK = 0;
        this.atL = 0;
        this.atM = 0;
        this.atN = 0;
    }
}
