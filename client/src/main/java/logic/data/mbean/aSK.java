package logic.data.mbean;

import game.network.message.serializable.C1556Wo;
import game.network.message.serializable.C3438ra;
import game.script.damage.DamageType;
import game.script.item.ItemTypeCategory;
import game.script.resource.Asset;
import game.script.ship.strike.StrikeType;
import logic.baa.C1616Xf;
import logic.res.code.C5663aRz;
import p001a.C0064Am;
import p001a.C3892wO;

/* renamed from: a.aSK */
public class aSK extends C0106BL {
    @C0064Am(aul = "09df90e679eccf6f82fe8403a4ecc5d6", aum = -2)

    /* renamed from: YA */
    public long f3742YA;
    @C0064Am(aul = "3b2dac10830d68f2f148bcc9e3869739", aum = -2)

    /* renamed from: YB */
    public long f3743YB;
    @C0064Am(aul = "09df90e679eccf6f82fe8403a4ecc5d6", aum = -1)

    /* renamed from: YG */
    public byte f3744YG;
    @C0064Am(aul = "3b2dac10830d68f2f148bcc9e3869739", aum = -1)

    /* renamed from: YH */
    public byte f3745YH;
    @C0064Am(aul = "f8c4e8e400173930c1f36301b0b4fe9d", aum = 2)
    public float aAc;
    @C0064Am(aul = "da77ecf6effd7f485efdc9bd9433fc1c", aum = 5)
    public Asset aAd;
    @C0064Am(aul = "8fb0592a0b14741aadc85066f1f90dc1", aum = 6)
    public Asset aAf;
    @C0064Am(aul = "1ce980ab167280182de4b846d1769b62", aum = 0)
    public C1556Wo<DamageType, C3892wO> atO;
    @C0064Am(aul = "3b2dac10830d68f2f148bcc9e3869739", aum = 3)
    public float bbL;
    @C0064Am(aul = "09df90e679eccf6f82fe8403a4ecc5d6", aum = 1)
    public float bbM;
    @C0064Am(aul = "5a744d37d9d6a0fadb5091086430c36a", aum = 4)
    public C3438ra<ItemTypeCategory> bjJ;
    @C0064Am(aul = "f8c4e8e400173930c1f36301b0b4fe9d", aum = -2)
    public long cAA;
    @C0064Am(aul = "da77ecf6effd7f485efdc9bd9433fc1c", aum = -2)
    public long cAB;
    @C0064Am(aul = "8fb0592a0b14741aadc85066f1f90dc1", aum = -2)
    public long cAC;
    @C0064Am(aul = "f8c4e8e400173930c1f36301b0b4fe9d", aum = -1)
    public byte cAO;
    @C0064Am(aul = "da77ecf6effd7f485efdc9bd9433fc1c", aum = -1)
    public byte cAP;
    @C0064Am(aul = "8fb0592a0b14741aadc85066f1f90dc1", aum = -1)
    public byte cAQ;
    @C0064Am(aul = "5a744d37d9d6a0fadb5091086430c36a", aum = -2)
    public long iFo;
    @C0064Am(aul = "5a744d37d9d6a0fadb5091086430c36a", aum = -1)
    public byte iFt;
    @C0064Am(aul = "1ce980ab167280182de4b846d1769b62", aum = -2)
    public long ide;
    @C0064Am(aul = "1ce980ab167280182de4b846d1769b62", aum = -1)
    public byte idf;

    public aSK() {
    }

    public aSK(C1616Xf xf) {
        super(xf);
    }

    /* renamed from: c */
    public C5663aRz[] mo309c() {
        return StrikeType._m_fields;
    }

    /* renamed from: d */
    public void mo310d() {
        super.mo310d();
        this.atO = null;
        this.bbM = 0.0f;
        this.aAc = 0.0f;
        this.bbL = 0.0f;
        this.bjJ = null;
        this.aAd = null;
        this.aAf = null;
        this.ide = 0;
        this.f3742YA = 0;
        this.cAA = 0;
        this.f3743YB = 0;
        this.iFo = 0;
        this.cAB = 0;
        this.cAC = 0;
        this.idf = 0;
        this.f3744YG = 0;
        this.cAO = 0;
        this.f3745YH = 0;
        this.iFt = 0;
        this.cAP = 0;
        this.cAQ = 0;
    }
}
