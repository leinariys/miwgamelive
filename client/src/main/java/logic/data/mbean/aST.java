package logic.data.mbean;

import game.script.item.ItemType;
import game.script.item.ItemTypeCategory;
import game.script.progression.ProgressionAbility;
import game.script.resource.Asset;
import logic.baa.C1616Xf;
import logic.res.code.C5663aRz;
import p001a.C0064Am;
import taikodom.infra.script.I18NString;

/* renamed from: a.aST */
public class aST extends C5410aIg {
    @C0064Am(aul = "7917b905a25f932f49a2312027908d37", aum = -2)

    /* renamed from: Nf */
    public long f3752Nf;
    @C0064Am(aul = "7917b905a25f932f49a2312027908d37", aum = -1)

    /* renamed from: Nh */
    public byte f3753Nh;
    @C0064Am(aul = "bcad2e885b4f68134eddc2082200e89f", aum = 2)
    public long amU;
    @C0064Am(aul = "7d9208fa0c0113dc9d261c58e126a60d", aum = 3)
    public float amW;
    @C0064Am(aul = "b2990b6f6c11b6894a25cc1fda9840e6", aum = 4)
    public ItemTypeCategory amY;
    @C0064Am(aul = "c76a92c0d5bedad6da3a97477d34b280", aum = 6)
    public ProgressionAbility ana;
    @C0064Am(aul = "e099eeb7ae68d88d7438a15f4e3e5f42", aum = 7)
    public long anc;
    @C0064Am(aul = "cda504a2095dbb23715c088c7551c026", aum = 8)
    public long ane;
    @C0064Am(aul = "e4a11f2e33dcccd875f620bd542265be", aum = 9)
    public long ang;
    @C0064Am(aul = "06ed10ec7d09d542e55b21672bf6b58d", aum = 10)
    public int ani;
    @C0064Am(aul = "a292518e57111128e4499b2b2268a921", aum = 11)
    public int ank;
    @C0064Am(aul = "10398010a94e368f32b5ebb52bfd2ed5", aum = 12)
    public int anm;
    @C0064Am(aul = "ef5b4c1d7a3ce5364911f8fd60b4c4b1", aum = 13)
    public int ano;
    @C0064Am(aul = "b28c26cd18ed6696cc8ae1f9c7cff1f7", aum = 14)
    public int anq;
    @C0064Am(aul = "06ed10ec7d09d542e55b21672bf6b58d", aum = -1)
    public byte dAA;
    @C0064Am(aul = "a292518e57111128e4499b2b2268a921", aum = -1)
    public byte dAB;
    @C0064Am(aul = "10398010a94e368f32b5ebb52bfd2ed5", aum = -1)
    public byte dAC;
    @C0064Am(aul = "ef5b4c1d7a3ce5364911f8fd60b4c4b1", aum = -1)
    public byte dAD;
    @C0064Am(aul = "b28c26cd18ed6696cc8ae1f9c7cff1f7", aum = -1)
    public byte dAE;
    @C0064Am(aul = "bcad2e885b4f68134eddc2082200e89f", aum = -2)
    public long dAd;
    @C0064Am(aul = "7d9208fa0c0113dc9d261c58e126a60d", aum = -2)
    public long dAe;
    @C0064Am(aul = "b2990b6f6c11b6894a25cc1fda9840e6", aum = -2)
    public long dAf;
    @C0064Am(aul = "c76a92c0d5bedad6da3a97477d34b280", aum = -2)
    public long dAg;
    @C0064Am(aul = "e099eeb7ae68d88d7438a15f4e3e5f42", aum = -2)
    public long dAh;
    @C0064Am(aul = "cda504a2095dbb23715c088c7551c026", aum = -2)
    public long dAi;
    @C0064Am(aul = "e4a11f2e33dcccd875f620bd542265be", aum = -2)
    public long dAj;
    @C0064Am(aul = "06ed10ec7d09d542e55b21672bf6b58d", aum = -2)
    public long dAk;
    @C0064Am(aul = "a292518e57111128e4499b2b2268a921", aum = -2)
    public long dAl;
    @C0064Am(aul = "10398010a94e368f32b5ebb52bfd2ed5", aum = -2)
    public long dAm;
    @C0064Am(aul = "ef5b4c1d7a3ce5364911f8fd60b4c4b1", aum = -2)
    public long dAn;
    @C0064Am(aul = "b28c26cd18ed6696cc8ae1f9c7cff1f7", aum = -2)
    public long dAo;
    @C0064Am(aul = "bcad2e885b4f68134eddc2082200e89f", aum = -1)
    public byte dAt;
    @C0064Am(aul = "7d9208fa0c0113dc9d261c58e126a60d", aum = -1)
    public byte dAu;
    @C0064Am(aul = "b2990b6f6c11b6894a25cc1fda9840e6", aum = -1)
    public byte dAv;
    @C0064Am(aul = "c76a92c0d5bedad6da3a97477d34b280", aum = -1)
    public byte dAw;
    @C0064Am(aul = "e099eeb7ae68d88d7438a15f4e3e5f42", aum = -1)
    public byte dAx;
    @C0064Am(aul = "cda504a2095dbb23715c088c7551c026", aum = -1)
    public byte dAy;
    @C0064Am(aul = "e4a11f2e33dcccd875f620bd542265be", aum = -1)
    public byte dAz;
    @C0064Am(aul = "63541c716fd844ab4cbedab3ca98fd98", aum = -1)

    /* renamed from: jK */
    public byte f3754jK;
    @C0064Am(aul = "63541c716fd844ab4cbedab3ca98fd98", aum = 5)

    /* renamed from: jn */
    public Asset f3755jn;
    @C0064Am(aul = "63541c716fd844ab4cbedab3ca98fd98", aum = -2)

    /* renamed from: jy */
    public long f3756jy;
    @C0064Am(aul = "4fc136745aa2d2d223ddaf097123dd95", aum = 1)

    /* renamed from: nh */
    public I18NString f3757nh;
    @C0064Am(aul = "4fc136745aa2d2d223ddaf097123dd95", aum = -2)

    /* renamed from: nk */
    public long f3758nk;
    @C0064Am(aul = "4fc136745aa2d2d223ddaf097123dd95", aum = -1)

    /* renamed from: nn */
    public byte f3759nn;
    @C0064Am(aul = "7917b905a25f932f49a2312027908d37", aum = 0)

    /* renamed from: zP */
    public I18NString f3760zP;

    public aST() {
    }

    public aST(C1616Xf xf) {
        super(xf);
    }

    /* renamed from: c */
    public C5663aRz[] mo309c() {
        return ItemType._m_fields;
    }

    /* renamed from: d */
    public void mo310d() {
        super.mo310d();
        this.f3760zP = null;
        this.f3757nh = null;
        this.amU = 0;
        this.amW = 0.0f;
        this.amY = null;
        this.f3755jn = null;
        this.ana = null;
        this.anc = 0;
        this.ane = 0;
        this.ang = 0;
        this.ani = 0;
        this.ank = 0;
        this.anm = 0;
        this.ano = 0;
        this.anq = 0;
        this.f3752Nf = 0;
        this.f3758nk = 0;
        this.dAd = 0;
        this.dAe = 0;
        this.dAf = 0;
        this.f3756jy = 0;
        this.dAg = 0;
        this.dAh = 0;
        this.dAi = 0;
        this.dAj = 0;
        this.dAk = 0;
        this.dAl = 0;
        this.dAm = 0;
        this.dAn = 0;
        this.dAo = 0;
        this.f3753Nh = 0;
        this.f3759nn = 0;
        this.dAt = 0;
        this.dAu = 0;
        this.dAv = 0;
        this.f3754jK = 0;
        this.dAw = 0;
        this.dAx = 0;
        this.dAy = 0;
        this.dAz = 0;
        this.dAA = 0;
        this.dAB = 0;
        this.dAC = 0;
        this.dAD = 0;
        this.dAE = 0;
    }
}
