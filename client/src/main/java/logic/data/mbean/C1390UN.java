package logic.data.mbean;

import game.script.Actor;
import game.script.ai.PlayerArrivalController;
import logic.abb.C0573Hw;
import logic.abb.C1218Ry;
import logic.abb.C6822auO;
import logic.baa.C1616Xf;
import logic.res.code.C5663aRz;
import p001a.C0064Am;

/* renamed from: a.UN */
public class C1390UN extends C2272dV {
    @C0064Am(aul = "f9dc8ba1a884fdc8fb0d18f2fe06badf", aum = 3)
    public Actor ams;
    @C0064Am(aul = "791645b9ced7992708a78e72827bfb75", aum = 0)
    public C0573Hw[] aue;
    @C0064Am(aul = "41be9403b66fcb690c0fb7e6d9a50f8a", aum = 2)
    public C1218Ry auf;
    @C0064Am(aul = "791645b9ced7992708a78e72827bfb75", aum = -2)
    public long auj;
    @C0064Am(aul = "a2963aaf58dbe5f1bb72469455d8b92f", aum = -2)
    public long auk;
    @C0064Am(aul = "41be9403b66fcb690c0fb7e6d9a50f8a", aum = -2)
    public long aul;
    @C0064Am(aul = "f9dc8ba1a884fdc8fb0d18f2fe06badf", aum = -2)
    public long aun;
    @C0064Am(aul = "791645b9ced7992708a78e72827bfb75", aum = -1)
    public byte auq;
    @C0064Am(aul = "a2963aaf58dbe5f1bb72469455d8b92f", aum = -1)
    public byte aur;
    @C0064Am(aul = "41be9403b66fcb690c0fb7e6d9a50f8a", aum = -1)
    public byte aus;
    @C0064Am(aul = "f9dc8ba1a884fdc8fb0d18f2fe06badf", aum = -1)
    public byte auu;
    @C0064Am(aul = "a2963aaf58dbe5f1bb72469455d8b92f", aum = 1)
    public C6822auO avoidObstaclesBehaviour;
    @C0064Am(aul = "2ba47ce0762117d39ac9eed5aaec70d8", aum = 4)
    public boolean equ;
    @C0064Am(aul = "2ba47ce0762117d39ac9eed5aaec70d8", aum = -2)
    public long eqv;
    @C0064Am(aul = "2ba47ce0762117d39ac9eed5aaec70d8", aum = -1)
    public byte eqw;

    public C1390UN() {
    }

    public C1390UN(C1616Xf xf) {
        super(xf);
    }

    /* renamed from: c */
    public C5663aRz[] mo309c() {
        return PlayerArrivalController._m_fields;
    }

    /* renamed from: d */
    public void mo310d() {
        super.mo310d();
        this.aue = null;
        this.avoidObstaclesBehaviour = null;
        this.auf = null;
        this.ams = null;
        this.equ = false;
        this.auj = 0;
        this.auk = 0;
        this.aul = 0;
        this.aun = 0;
        this.eqv = 0;
        this.auq = 0;
        this.aur = 0;
        this.aus = 0;
        this.auu = 0;
        this.eqw = 0;
    }
}
