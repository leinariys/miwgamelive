package logic.data.mbean;

import game.script.mission.Mission;
import game.script.mission.MissionObjective;
import logic.baa.C1616Xf;
import logic.res.code.C5663aRz;
import p001a.C0064Am;
import taikodom.infra.script.I18NString;

/* renamed from: a.aFM */
public class aFM extends C5292aDs {
    @C0064Am(aul = "0cb5b1c207ffc72941a7ddd092454070", aum = 2)
    public int bib;
    @C0064Am(aul = "9115da31458e82a71fb33abc06a40681", aum = 4)
    public int bie;
    @C0064Am(aul = "34b2a844c230d7810f2ca211c9addf19", aum = 5)
    public boolean big;
    @C0064Am(aul = "34b2a844c230d7810f2ca211c9addf19", aum = -2)
    public long dIW;
    @C0064Am(aul = "34b2a844c230d7810f2ca211c9addf19", aum = -1)
    public byte dJc;
    @C0064Am(aul = "0cb5b1c207ffc72941a7ddd092454070", aum = -2)
    public long hKX;
    @C0064Am(aul = "9115da31458e82a71fb33abc06a40681", aum = -2)
    public long hKY;
    @C0064Am(aul = "0cb5b1c207ffc72941a7ddd092454070", aum = -1)
    public byte hKZ;
    @C0064Am(aul = "9115da31458e82a71fb33abc06a40681", aum = -1)
    public byte hLa;
    @C0064Am(aul = "10419ca5e02bd57d7b6d58bb4ee9dd0b", aum = 0)
    public String handle;
    @C0064Am(aul = "137dd39425db32f0b17947b81c33a826", aum = 1)

    /* renamed from: nh */
    public I18NString f2798nh;
    @C0064Am(aul = "137dd39425db32f0b17947b81c33a826", aum = -2)

    /* renamed from: nk */
    public long f2799nk;
    @C0064Am(aul = "137dd39425db32f0b17947b81c33a826", aum = -1)

    /* renamed from: nn */
    public byte f2800nn;
    @C0064Am(aul = "ac94a1405bbea8373f72d5d5bba76f30", aum = 3)

    /* renamed from: oi */
    public Mission.C0015a f2801oi;
    @C0064Am(aul = "10419ca5e02bd57d7b6d58bb4ee9dd0b", aum = -2)

    /* renamed from: ok */
    public long f2802ok;
    @C0064Am(aul = "ac94a1405bbea8373f72d5d5bba76f30", aum = -2)

    /* renamed from: ol */
    public long f2803ol;
    @C0064Am(aul = "10419ca5e02bd57d7b6d58bb4ee9dd0b", aum = -1)

    /* renamed from: on */
    public byte f2804on;
    @C0064Am(aul = "ac94a1405bbea8373f72d5d5bba76f30", aum = -1)

    /* renamed from: oo */
    public byte f2805oo;

    public aFM() {
    }

    public aFM(C1616Xf xf) {
        super(xf);
    }

    /* renamed from: c */
    public C5663aRz[] mo309c() {
        return MissionObjective._m_fields;
    }

    /* renamed from: d */
    public void mo310d() {
        super.mo310d();
        this.handle = null;
        this.f2798nh = null;
        this.bib = 0;
        this.f2801oi = null;
        this.bie = 0;
        this.big = false;
        this.f2802ok = 0;
        this.f2799nk = 0;
        this.hKX = 0;
        this.f2803ol = 0;
        this.hKY = 0;
        this.dIW = 0;
        this.f2804on = 0;
        this.f2800nn = 0;
        this.hKZ = 0;
        this.f2805oo = 0;
        this.hLa = 0;
        this.dJc = 0;
    }
}
