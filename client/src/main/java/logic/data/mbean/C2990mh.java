package logic.data.mbean;

import game.script.item.buff.amplifier.ShieldAmplifier;
import game.script.ship.ShieldAdapter;
import logic.baa.C1616Xf;
import logic.res.code.C5663aRz;
import p001a.C0064Am;
import p001a.C3892wO;

/* renamed from: a.mh */
public class C2990mh extends C3108nv {
    @C0064Am(aul = "500784775e723d8e7ffbb7879c97912a", aum = 0)
    public ShieldAdapter aBI;
    @C0064Am(aul = "98d70c037ca1d097a7ddbf346826a5fb", aum = 1)
    public C3892wO aBJ;
    @C0064Am(aul = "238c3511433d5f898315f71acf014f6a", aum = 2)
    public C3892wO aBK;
    @C0064Am(aul = "60b99ec8c69d1f503b207349d6e1f575", aum = 3)
    public C3892wO aBL;
    @C0064Am(aul = "202019085c719834fa25ab5182a290a9", aum = 4)
    public C3892wO aBM;
    @C0064Am(aul = "500784775e723d8e7ffbb7879c97912a", aum = -2)
    public long aBN;
    @C0064Am(aul = "98d70c037ca1d097a7ddbf346826a5fb", aum = -2)
    public long aBO;
    @C0064Am(aul = "238c3511433d5f898315f71acf014f6a", aum = -2)
    public long aBP;
    @C0064Am(aul = "60b99ec8c69d1f503b207349d6e1f575", aum = -2)
    public long aBQ;
    @C0064Am(aul = "202019085c719834fa25ab5182a290a9", aum = -2)
    public long aBR;
    @C0064Am(aul = "500784775e723d8e7ffbb7879c97912a", aum = -1)
    public byte aBS;
    @C0064Am(aul = "98d70c037ca1d097a7ddbf346826a5fb", aum = -1)
    public byte aBT;
    @C0064Am(aul = "238c3511433d5f898315f71acf014f6a", aum = -1)
    public byte aBU;
    @C0064Am(aul = "60b99ec8c69d1f503b207349d6e1f575", aum = -1)
    public byte aBV;
    @C0064Am(aul = "202019085c719834fa25ab5182a290a9", aum = -1)
    public byte aBW;

    public C2990mh() {
    }

    public C2990mh(C1616Xf xf) {
        super(xf);
    }

    /* renamed from: c */
    public C5663aRz[] mo309c() {
        return ShieldAmplifier._m_fields;
    }

    /* renamed from: d */
    public void mo310d() {
        super.mo310d();
        this.aBI = null;
        this.aBJ = null;
        this.aBK = null;
        this.aBL = null;
        this.aBM = null;
        this.aBN = 0;
        this.aBO = 0;
        this.aBP = 0;
        this.aBQ = 0;
        this.aBR = 0;
        this.aBS = 0;
        this.aBT = 0;
        this.aBU = 0;
        this.aBV = 0;
        this.aBW = 0;
    }
}
