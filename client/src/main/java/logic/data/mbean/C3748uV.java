package logic.data.mbean;

import game.script.ship.OutpostLevelFeature;
import logic.baa.C1616Xf;
import logic.res.code.C5663aRz;
import p001a.C0064Am;
import p001a.C0437GE;
import p001a.C2611hZ;

import java.util.UUID;

/* renamed from: a.uV */
public class C3748uV extends C5292aDs {
    @C0064Am(aul = "93eeb4d0ca295ce4a7d5711054755131", aum = 0)
    public C0437GE ayG;
    @C0064Am(aul = "238869e98fced996c4ca820d0f5a2e75", aum = 1)
    public C2611hZ ayI;
    @C0064Am(aul = "6b4416087b12d503fea56c8cee93cca1", aum = 2)
    public boolean ayK;
    @C0064Am(aul = "a808f1cf4d82fb3c3a63f2a8ab0ee9a5", aum = 3)

    /* renamed from: bK */
    public UUID f9360bK;
    @C0064Am(aul = "93eeb4d0ca295ce4a7d5711054755131", aum = -2)
    public long bxQ;
    @C0064Am(aul = "238869e98fced996c4ca820d0f5a2e75", aum = -2)
    public long bxR;
    @C0064Am(aul = "6b4416087b12d503fea56c8cee93cca1", aum = -2)
    public long bxS;
    @C0064Am(aul = "93eeb4d0ca295ce4a7d5711054755131", aum = -1)
    public byte bxT;
    @C0064Am(aul = "238869e98fced996c4ca820d0f5a2e75", aum = -1)
    public byte bxU;
    @C0064Am(aul = "6b4416087b12d503fea56c8cee93cca1", aum = -1)
    public byte bxV;
    @C0064Am(aul = "f04cbf1ff185e4ba44d751254b2176af", aum = 4)
    public String handle;
    @C0064Am(aul = "a808f1cf4d82fb3c3a63f2a8ab0ee9a5", aum = -2)

    /* renamed from: oL */
    public long f9361oL;
    @C0064Am(aul = "a808f1cf4d82fb3c3a63f2a8ab0ee9a5", aum = -1)

    /* renamed from: oS */
    public byte f9362oS;
    @C0064Am(aul = "f04cbf1ff185e4ba44d751254b2176af", aum = -2)

    /* renamed from: ok */
    public long f9363ok;
    @C0064Am(aul = "f04cbf1ff185e4ba44d751254b2176af", aum = -1)

    /* renamed from: on */
    public byte f9364on;

    public C3748uV() {
    }

    public C3748uV(C1616Xf xf) {
        super(xf);
    }

    /* renamed from: c */
    public C5663aRz[] mo309c() {
        return OutpostLevelFeature._m_fields;
    }

    /* renamed from: d */
    public void mo310d() {
        super.mo310d();
        this.ayG = null;
        this.ayI = null;
        this.ayK = false;
        this.f9360bK = null;
        this.handle = null;
        this.bxQ = 0;
        this.bxR = 0;
        this.bxS = 0;
        this.f9361oL = 0;
        this.f9363ok = 0;
        this.bxT = 0;
        this.bxU = 0;
        this.bxV = 0;
        this.f9362oS = 0;
        this.f9364on = 0;
    }
}
