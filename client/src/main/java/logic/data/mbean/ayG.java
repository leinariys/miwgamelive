package logic.data.mbean;

import game.script.agent.Agent;
import game.script.npc.NPC;
import game.script.npcchat.actions.AddToKnownAgentsSpeechAction;
import logic.baa.C1616Xf;
import logic.res.code.C5663aRz;
import p001a.C0064Am;

/* renamed from: a.ayG */
public class ayG extends C6352alM {
    @C0064Am(aul = "b96adaf3fe77f56bfc0d79f7a3896170", aum = 0)
    public NPC eKL;
    @C0064Am(aul = "cbe888eca4be4ff2a76f2f6b9ef29636", aum = 1)
    public Agent eKN;
    @C0064Am(aul = "b96adaf3fe77f56bfc0d79f7a3896170", aum = -2)
    public long gVh;
    @C0064Am(aul = "cbe888eca4be4ff2a76f2f6b9ef29636", aum = -2)
    public long gVi;
    @C0064Am(aul = "b96adaf3fe77f56bfc0d79f7a3896170", aum = -1)
    public byte gVj;
    @C0064Am(aul = "cbe888eca4be4ff2a76f2f6b9ef29636", aum = -1)
    public byte gVk;

    public ayG() {
    }

    public ayG(C1616Xf xf) {
        super(xf);
    }

    /* renamed from: c */
    public C5663aRz[] mo309c() {
        return AddToKnownAgentsSpeechAction._m_fields;
    }

    /* renamed from: d */
    public void mo310d() {
        super.mo310d();
        this.eKL = null;
        this.eKN = null;
        this.gVh = 0;
        this.gVi = 0;
        this.gVj = 0;
        this.gVk = 0;
    }
}
