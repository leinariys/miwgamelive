package logic.data.mbean;

import game.script.billing.ItemBillingDefaults;
import logic.baa.C1616Xf;
import logic.res.code.C5663aRz;
import p001a.C0064Am;

import java.util.UUID;

/* renamed from: a.rM */
public class C3419rM extends C5292aDs {
    @C0064Am(aul = "6a9f40a3496491801e1013a42f577494", aum = 1)

    /* renamed from: bK */
    public UUID f8979bK;
    @C0064Am(aul = "6c3c7e1009aca6d3dc01d45d9cb15e83", aum = 0)
    public long bcm;
    @C0064Am(aul = "6c3c7e1009aca6d3dc01d45d9cb15e83", aum = -2)
    public long bcn;
    @C0064Am(aul = "6c3c7e1009aca6d3dc01d45d9cb15e83", aum = -1)
    public byte bco;
    @C0064Am(aul = "6a9f40a3496491801e1013a42f577494", aum = -2)

    /* renamed from: oL */
    public long f8980oL;
    @C0064Am(aul = "6a9f40a3496491801e1013a42f577494", aum = -1)

    /* renamed from: oS */
    public byte f8981oS;

    public C3419rM() {
    }

    public C3419rM(C1616Xf xf) {
        super(xf);
    }

    /* renamed from: c */
    public C5663aRz[] mo309c() {
        return ItemBillingDefaults._m_fields;
    }

    /* renamed from: d */
    public void mo310d() {
        super.mo310d();
        this.bcm = 0;
        this.f8979bK = null;
        this.bcn = 0;
        this.f8980oL = 0;
        this.bco = 0;
        this.f8981oS = 0;
    }
}
