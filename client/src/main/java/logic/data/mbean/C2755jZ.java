package logic.data.mbean;

import game.script.item.buff.module.Healer;
import logic.baa.C1616Xf;
import logic.res.code.C5663aRz;
import p001a.C0064Am;

/* renamed from: a.jZ */
public class C2755jZ extends C4037yS {
    @C0064Am(aul = "54a3272b23aa89be0f3f94e0fbc70715", aum = 0)
    public float apV;
    @C0064Am(aul = "1f7cf52ab1fb2a889393b05298242a57", aum = 1)
    public float apW;
    @C0064Am(aul = "e58366d4c5c20bc3726d17f38a9fbbc6", aum = 2)
    public float apX;
    @C0064Am(aul = "09bafe8ec9fbee12370d9b719b5fdc83", aum = 3)
    public float apY;
    @C0064Am(aul = "d0dc992c7ecb3ab2ef2bb99a33f71790", aum = 4)
    public boolean apZ;
    @C0064Am(aul = "54a3272b23aa89be0f3f94e0fbc70715", aum = -2)
    public long aqa;
    @C0064Am(aul = "1f7cf52ab1fb2a889393b05298242a57", aum = -2)
    public long aqb;
    @C0064Am(aul = "e58366d4c5c20bc3726d17f38a9fbbc6", aum = -2)
    public long aqc;
    @C0064Am(aul = "09bafe8ec9fbee12370d9b719b5fdc83", aum = -2)
    public long aqd;
    @C0064Am(aul = "d0dc992c7ecb3ab2ef2bb99a33f71790", aum = -2)
    public long aqe;
    @C0064Am(aul = "54a3272b23aa89be0f3f94e0fbc70715", aum = -1)
    public byte aqf;
    @C0064Am(aul = "1f7cf52ab1fb2a889393b05298242a57", aum = -1)
    public byte aqg;
    @C0064Am(aul = "e58366d4c5c20bc3726d17f38a9fbbc6", aum = -1)
    public byte aqh;
    @C0064Am(aul = "09bafe8ec9fbee12370d9b719b5fdc83", aum = -1)
    public byte aqi;
    @C0064Am(aul = "d0dc992c7ecb3ab2ef2bb99a33f71790", aum = -1)
    public byte aqj;

    public C2755jZ() {
    }

    public C2755jZ(C1616Xf xf) {
        super(xf);
    }

    /* renamed from: c */
    public C5663aRz[] mo309c() {
        return Healer._m_fields;
    }

    /* renamed from: d */
    public void mo310d() {
        super.mo310d();
        this.apV = 0.0f;
        this.apW = 0.0f;
        this.apX = 0.0f;
        this.apY = 0.0f;
        this.apZ = false;
        this.aqa = 0;
        this.aqb = 0;
        this.aqc = 0;
        this.aqd = 0;
        this.aqe = 0;
        this.aqf = 0;
        this.aqg = 0;
        this.aqh = 0;
        this.aqi = 0;
        this.aqj = 0;
    }
}
