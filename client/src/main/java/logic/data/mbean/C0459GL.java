package logic.data.mbean;

import game.network.message.serializable.C3438ra;
import game.script.space.AsteroidType;
import game.script.spacezone.AsteroidSpawnTable;
import logic.baa.C1616Xf;
import logic.res.code.C5663aRz;
import p001a.C0064Am;

import java.util.UUID;

/* renamed from: a.GL */
public class C0459GL extends C3805vD {
    @C0064Am(aul = "7086b6af24d3f61bac0d99db9b9a96f1", aum = -2)

    /* renamed from: Nf */
    public long f620Nf;
    @C0064Am(aul = "7086b6af24d3f61bac0d99db9b9a96f1", aum = -1)

    /* renamed from: Nh */
    public byte f621Nh;
    @C0064Am(aul = "0cafdd62ea30586e0a07aaab1885a2a2", aum = 2)

    /* renamed from: bK */
    public UUID f622bK;
    @C0064Am(aul = "78afcd5797b8566827a6976440112990", aum = 0)
    public C3438ra<AsteroidType> cJs;
    @C0064Am(aul = "78afcd5797b8566827a6976440112990", aum = -2)
    public long cZA;
    @C0064Am(aul = "78afcd5797b8566827a6976440112990", aum = -1)
    public byte cZB;
    @C0064Am(aul = "19110ce7c864fd06ddb204e7c68df340", aum = 3)
    public String handle;
    @C0064Am(aul = "7086b6af24d3f61bac0d99db9b9a96f1", aum = 1)
    public String name;
    @C0064Am(aul = "0cafdd62ea30586e0a07aaab1885a2a2", aum = -2)

    /* renamed from: oL */
    public long f623oL;
    @C0064Am(aul = "0cafdd62ea30586e0a07aaab1885a2a2", aum = -1)

    /* renamed from: oS */
    public byte f624oS;
    @C0064Am(aul = "19110ce7c864fd06ddb204e7c68df340", aum = -2)

    /* renamed from: ok */
    public long f625ok;
    @C0064Am(aul = "19110ce7c864fd06ddb204e7c68df340", aum = -1)

    /* renamed from: on */
    public byte f626on;

    public C0459GL() {
    }

    public C0459GL(C1616Xf xf) {
        super(xf);
    }

    /* renamed from: c */
    public C5663aRz[] mo309c() {
        return AsteroidSpawnTable._m_fields;
    }

    /* renamed from: d */
    public void mo310d() {
        super.mo310d();
        this.cJs = null;
        this.name = null;
        this.f622bK = null;
        this.handle = null;
        this.cZA = 0;
        this.f620Nf = 0;
        this.f623oL = 0;
        this.f625ok = 0;
        this.cZB = 0;
        this.f621Nh = 0;
        this.f624oS = 0;
        this.f626on = 0;
    }
}
