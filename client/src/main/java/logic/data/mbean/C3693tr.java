package logic.data.mbean;

import game.script.spacezone.AsteroidSpawnTable;
import game.script.spacezone.AsteroidZone;
import game.script.spacezone.AsteroidZoneType;
import logic.baa.C1616Xf;
import logic.res.code.C5663aRz;
import p001a.C0064Am;

/* renamed from: a.tr */
public class C3693tr extends C0218Cj {
    @C0064Am(aul = "2af3cbf155f52ad1358d7967b32431b9", aum = -2)
    public long bmA;
    @C0064Am(aul = "37ef1669c8b354e171c0e60cf979e740", aum = -2)
    public long bmB;
    @C0064Am(aul = "8a8ee4d7b08e51622b23881fa61de4df", aum = -1)
    public byte bmC;
    @C0064Am(aul = "a01f94046f592b49f79e54aaed514e03", aum = -1)
    public byte bmD;
    @C0064Am(aul = "c4e1e251c8b7b0fb0dad50a119b21cf3", aum = -1)
    public byte bmE;
    @C0064Am(aul = "b0bf771034b583740303577e96359052", aum = -1)
    public byte bmF;
    @C0064Am(aul = "3d79f0d3d555551d41b597bf7ce5964e", aum = -1)
    public byte bmG;
    @C0064Am(aul = "0f361b499661f13de83d455a59519340", aum = -1)
    public byte bmH;
    @C0064Am(aul = "8b491e5ecc45e592ce0b943266b131f3", aum = -1)
    public byte bmI;
    @C0064Am(aul = "2af3cbf155f52ad1358d7967b32431b9", aum = -1)
    public byte bmJ;
    @C0064Am(aul = "37ef1669c8b354e171c0e60cf979e740", aum = -1)
    public byte bmK;
    @C0064Am(aul = "8a8ee4d7b08e51622b23881fa61de4df", aum = 0)
    public AsteroidZone.C2089b bmk;
    @C0064Am(aul = "a01f94046f592b49f79e54aaed514e03", aum = 1)
    public AsteroidZone.C2088a bml;
    @C0064Am(aul = "c4e1e251c8b7b0fb0dad50a119b21cf3", aum = 2)
    public float bmm;
    @C0064Am(aul = "b0bf771034b583740303577e96359052", aum = 3)
    public float bmn;
    @C0064Am(aul = "3d79f0d3d555551d41b597bf7ce5964e", aum = 4)
    public float bmo;
    @C0064Am(aul = "0f361b499661f13de83d455a59519340", aum = 5)
    public int bmp;
    @C0064Am(aul = "8b491e5ecc45e592ce0b943266b131f3", aum = 6)
    public int bmq;
    @C0064Am(aul = "2af3cbf155f52ad1358d7967b32431b9", aum = 7)
    public float bmr;
    @C0064Am(aul = "37ef1669c8b354e171c0e60cf979e740", aum = 8)
    public AsteroidSpawnTable bms;
    @C0064Am(aul = "8a8ee4d7b08e51622b23881fa61de4df", aum = -2)
    public long bmt;
    @C0064Am(aul = "a01f94046f592b49f79e54aaed514e03", aum = -2)
    public long bmu;
    @C0064Am(aul = "c4e1e251c8b7b0fb0dad50a119b21cf3", aum = -2)
    public long bmv;
    @C0064Am(aul = "b0bf771034b583740303577e96359052", aum = -2)
    public long bmw;
    @C0064Am(aul = "3d79f0d3d555551d41b597bf7ce5964e", aum = -2)
    public long bmx;
    @C0064Am(aul = "0f361b499661f13de83d455a59519340", aum = -2)
    public long bmy;
    @C0064Am(aul = "8b491e5ecc45e592ce0b943266b131f3", aum = -2)
    public long bmz;

    public C3693tr() {
    }

    public C3693tr(C1616Xf xf) {
        super(xf);
    }

    /* renamed from: c */
    public C5663aRz[] mo309c() {
        return AsteroidZoneType._m_fields;
    }

    /* renamed from: d */
    public void mo310d() {
        super.mo310d();
        this.bmk = null;
        this.bml = null;
        this.bmm = 0.0f;
        this.bmn = 0.0f;
        this.bmo = 0.0f;
        this.bmp = 0;
        this.bmq = 0;
        this.bmr = 0.0f;
        this.bms = null;
        this.bmt = 0;
        this.bmu = 0;
        this.bmv = 0;
        this.bmw = 0;
        this.bmx = 0;
        this.bmy = 0;
        this.bmz = 0;
        this.bmA = 0;
        this.bmB = 0;
        this.bmC = 0;
        this.bmD = 0;
        this.bmE = 0;
        this.bmF = 0;
        this.bmG = 0;
        this.bmH = 0;
        this.bmI = 0;
        this.bmJ = 0;
        this.bmK = 0;
    }
}
