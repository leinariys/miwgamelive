package logic.data.mbean;

import game.script.item.ItemType;
import game.script.npcchat.requirement.NotHasItemSpeechRequirement;
import logic.baa.C1616Xf;
import logic.res.code.C5663aRz;
import p001a.C0064Am;

/* renamed from: a.aPt  reason: case insensitive filesystem */
public class C5605aPt extends C5416aIm {
    @C0064Am(aul = "b035f10861aa23b2cbd93203fb4a88c5", aum = -2)
    public long aLG;
    @C0064Am(aul = "b035f10861aa23b2cbd93203fb4a88c5", aum = -1)
    public byte aLL;
    @C0064Am(aul = "48a402c7a83db7b51fdd8b889b92324e", aum = -2)
    public long bmV;
    @C0064Am(aul = "48a402c7a83db7b51fdd8b889b92324e", aum = -1)
    public byte bmW;
    @C0064Am(aul = "48a402c7a83db7b51fdd8b889b92324e", aum = 0)

    /* renamed from: cw */
    public ItemType f3562cw;
    @C0064Am(aul = "b035f10861aa23b2cbd93203fb4a88c5", aum = 1)

    /* renamed from: cy */
    public int f3563cy;

    public C5605aPt() {
    }

    public C5605aPt(C1616Xf xf) {
        super(xf);
    }

    /* renamed from: c */
    public C5663aRz[] mo309c() {
        return NotHasItemSpeechRequirement._m_fields;
    }

    /* renamed from: d */
    public void mo310d() {
        super.mo310d();
        this.f3562cw = null;
        this.f3563cy = 0;
        this.bmV = 0;
        this.aLG = 0;
        this.bmW = 0;
        this.aLL = 0;
    }
}
