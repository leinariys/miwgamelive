package logic.data.mbean;

import com.hoplon.geometry.Vec3f;
import game.script.resource.Asset;
import game.script.ship.StationType;
import logic.baa.C1616Xf;
import logic.res.code.C5663aRz;
import p001a.C0064Am;
import taikodom.geom.Orientation;

/* renamed from: a.aAR */
public class aAR extends C6515aoT {
    @C0064Am(aul = "8f74e4651afd265f2de9304381be7cbf", aum = 0)

    /* renamed from: LQ */
    public Asset f2321LQ;
    @C0064Am(aul = "88c4d9e23eceb6cac0271a5f641cec49", aum = 1)

    /* renamed from: LW */
    public Vec3f f2322LW;
    @C0064Am(aul = "6578a6cbc97c6e85bfe897698403f662", aum = 2)

    /* renamed from: LY */
    public Orientation f2323LY;
    @C0064Am(aul = "1b5a7383b96b77b4b924504ceca97831", aum = -2)
    public long avd;
    @C0064Am(aul = "1b5a7383b96b77b4b924504ceca97831", aum = -1)
    public byte avi;
    @C0064Am(aul = "ffed31f536aded59d38f8b395a8e3cb3", aum = 3)
    public Asset axR;
    @C0064Am(aul = "8f74e4651afd265f2de9304381be7cbf", aum = -2)
    public long ayc;
    @C0064Am(aul = "88c4d9e23eceb6cac0271a5f641cec49", aum = -2)
    public long ayd;
    @C0064Am(aul = "6578a6cbc97c6e85bfe897698403f662", aum = -2)
    public long aye;
    @C0064Am(aul = "ffed31f536aded59d38f8b395a8e3cb3", aum = -2)
    public long ayf;
    @C0064Am(aul = "8f74e4651afd265f2de9304381be7cbf", aum = -1)
    public byte ayr;
    @C0064Am(aul = "88c4d9e23eceb6cac0271a5f641cec49", aum = -1)
    public byte ays;
    @C0064Am(aul = "6578a6cbc97c6e85bfe897698403f662", aum = -1)
    public byte ayt;
    @C0064Am(aul = "ffed31f536aded59d38f8b395a8e3cb3", aum = -1)
    public byte ayu;
    @C0064Am(aul = "1b5a7383b96b77b4b924504ceca97831", aum = 4)

    /* renamed from: uT */
    public String f2324uT;

    public aAR() {
    }

    public aAR(C1616Xf xf) {
        super(xf);
    }

    /* renamed from: c */
    public C5663aRz[] mo309c() {
        return StationType._m_fields;
    }

    /* renamed from: d */
    public void mo310d() {
        super.mo310d();
        this.f2321LQ = null;
        this.f2322LW = null;
        this.f2323LY = null;
        this.axR = null;
        this.f2324uT = null;
        this.ayc = 0;
        this.ayd = 0;
        this.aye = 0;
        this.ayf = 0;
        this.avd = 0;
        this.ayr = 0;
        this.ays = 0;
        this.ayt = 0;
        this.ayu = 0;
        this.avi = 0;
    }
}
