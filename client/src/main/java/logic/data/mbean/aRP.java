package logic.data.mbean;

import game.script.item.buff.amplifier.SpeedBoostAmplifierType;
import logic.baa.C1616Xf;
import logic.res.code.C5663aRz;
import p001a.C0064Am;
import p001a.C3892wO;

/* renamed from: a.aRP */
public class aRP extends C2421fH {
    @C0064Am(aul = "49d4c27484c703a036ef1947ff98de2e", aum = 1)

    /* renamed from: Vs */
    public C3892wO f3685Vs;
    @C0064Am(aul = "693ca6dc591e75ba49bcbf875f97ab0b", aum = 2)

    /* renamed from: Vu */
    public C3892wO f3686Vu;
    @C0064Am(aul = "9df74af1b2a987e9be4559819b16a70a", aum = 4)

    /* renamed from: Vw */
    public C3892wO f3687Vw;
    @C0064Am(aul = "997671b0fc9ac7ffac025c57b609afc4", aum = 3)

    /* renamed from: Vy */
    public C3892wO f3688Vy;
    @C0064Am(aul = "49d4c27484c703a036ef1947ff98de2e", aum = -1)

    /* renamed from: YC */
    public byte f3689YC;
    @C0064Am(aul = "693ca6dc591e75ba49bcbf875f97ab0b", aum = -1)

    /* renamed from: YD */
    public byte f3690YD;
    @C0064Am(aul = "9df74af1b2a987e9be4559819b16a70a", aum = -1)

    /* renamed from: YE */
    public byte f3691YE;
    @C0064Am(aul = "997671b0fc9ac7ffac025c57b609afc4", aum = -1)

    /* renamed from: YF */
    public byte f3692YF;
    @C0064Am(aul = "49d4c27484c703a036ef1947ff98de2e", aum = -2)

    /* renamed from: Yw */
    public long f3693Yw;
    @C0064Am(aul = "693ca6dc591e75ba49bcbf875f97ab0b", aum = -2)

    /* renamed from: Yx */
    public long f3694Yx;
    @C0064Am(aul = "9df74af1b2a987e9be4559819b16a70a", aum = -2)

    /* renamed from: Yy */
    public long f3695Yy;
    @C0064Am(aul = "997671b0fc9ac7ffac025c57b609afc4", aum = -2)

    /* renamed from: Yz */
    public long f3696Yz;
    @C0064Am(aul = "506828a5e139329b66ab187d06f86ac9", aum = 0)
    public C3892wO atR;
    @C0064Am(aul = "506828a5e139329b66ab187d06f86ac9", aum = -2)
    public long cAA;
    @C0064Am(aul = "506828a5e139329b66ab187d06f86ac9", aum = -1)
    public byte cAO;

    public aRP() {
    }

    public aRP(C1616Xf xf) {
        super(xf);
    }

    /* renamed from: c */
    public C5663aRz[] mo309c() {
        return SpeedBoostAmplifierType._m_fields;
    }

    /* renamed from: d */
    public void mo310d() {
        super.mo310d();
        this.atR = null;
        this.f3685Vs = null;
        this.f3686Vu = null;
        this.f3688Vy = null;
        this.f3687Vw = null;
        this.cAA = 0;
        this.f3693Yw = 0;
        this.f3694Yx = 0;
        this.f3696Yz = 0;
        this.f3695Yy = 0;
        this.cAO = 0;
        this.f3689YC = 0;
        this.f3690YD = 0;
        this.f3692YF = 0;
        this.f3691YE = 0;
    }
}
