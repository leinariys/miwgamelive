package logic.data.mbean;

import game.script.npcchat.requirement.FlipACoinChatRequirement;
import logic.baa.C1616Xf;
import logic.res.code.C5663aRz;
import p001a.C0064Am;

/* renamed from: a.aFx  reason: case insensitive filesystem */
public class C5349aFx extends C5416aIm {
    @C0064Am(aul = "387e5b41ad20e8cf0cbf9ba842f98c1e", aum = 0)
    public float aGk;
    @C0064Am(aul = "387e5b41ad20e8cf0cbf9ba842f98c1e", aum = -2)
    public long hJr;
    @C0064Am(aul = "387e5b41ad20e8cf0cbf9ba842f98c1e", aum = -1)
    public byte hJs;

    public C5349aFx() {
    }

    public C5349aFx(C1616Xf xf) {
        super(xf);
    }

    /* renamed from: c */
    public C5663aRz[] mo309c() {
        return FlipACoinChatRequirement._m_fields;
    }

    /* renamed from: d */
    public void mo310d() {
        super.mo310d();
        this.aGk = 0.0f;
        this.hJr = 0;
        this.hJs = 0;
    }
}
