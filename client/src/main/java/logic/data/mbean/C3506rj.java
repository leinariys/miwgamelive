package logic.data.mbean;

import game.network.message.serializable.C3438ra;
import game.script.progression.*;
import logic.baa.C1616Xf;
import logic.res.code.C5663aRz;
import p001a.C0064Am;

import java.util.UUID;

/* renamed from: a.rj */
public class C3506rj extends C3805vD {
    @C0064Am(aul = "435faf4fc8eafadb13363d834f7b3b18", aum = 0)
    public ProgressionDefaults aZS;
    @C0064Am(aul = "82218fb9b69e2da6388e3acb6c8efe16", aum = 2)
    public C3438ra<MPMultiplier> aZT;
    @C0064Am(aul = "4d8257dae1e9985b22029b94a99b895a", aum = 3)
    public C3438ra<ProgressionAbility> aZU;
    @C0064Am(aul = "4a1a2bb7777d3e84e48fe5b9b126f2fb", aum = 4)
    public C3438ra<ProgressionCharacterTemplate> aZV;
    @C0064Am(aul = "435faf4fc8eafadb13363d834f7b3b18", aum = -2)
    public long aZW;
    @C0064Am(aul = "05f926b2fe21a9ae2ca3b41dab91da0f", aum = -2)
    public long aZX;
    @C0064Am(aul = "82218fb9b69e2da6388e3acb6c8efe16", aum = -2)
    public long aZY;
    @C0064Am(aul = "4d8257dae1e9985b22029b94a99b895a", aum = -2)
    public long aZZ;
    @C0064Am(aul = "c695b0f386d7e5dc92fd596511fa0825", aum = 6)

    /* renamed from: bK */
    public UUID f9064bK;
    @C0064Am(aul = "4a1a2bb7777d3e84e48fe5b9b126f2fb", aum = -2)
    public long baa;
    @C0064Am(aul = "435faf4fc8eafadb13363d834f7b3b18", aum = -1)
    public byte bab;
    @C0064Am(aul = "05f926b2fe21a9ae2ca3b41dab91da0f", aum = -1)
    public byte bac;
    @C0064Am(aul = "82218fb9b69e2da6388e3acb6c8efe16", aum = -1)
    public byte bad;
    @C0064Am(aul = "4d8257dae1e9985b22029b94a99b895a", aum = -1)
    public byte bae;
    @C0064Am(aul = "4a1a2bb7777d3e84e48fe5b9b126f2fb", aum = -1)
    public byte baf;
    @C0064Am(aul = "05f926b2fe21a9ae2ca3b41dab91da0f", aum = 1)

    /* renamed from: dt */
    public C3438ra<ProgressionCareer> f9065dt;
    @C0064Am(aul = "4e2e0d3cf6c3d1304f2b37623bb6faf9", aum = 5)
    public String handle;
    @C0064Am(aul = "c695b0f386d7e5dc92fd596511fa0825", aum = -2)

    /* renamed from: oL */
    public long f9066oL;
    @C0064Am(aul = "c695b0f386d7e5dc92fd596511fa0825", aum = -1)

    /* renamed from: oS */
    public byte f9067oS;
    @C0064Am(aul = "4e2e0d3cf6c3d1304f2b37623bb6faf9", aum = -2)

    /* renamed from: ok */
    public long f9068ok;
    @C0064Am(aul = "4e2e0d3cf6c3d1304f2b37623bb6faf9", aum = -1)

    /* renamed from: on */
    public byte f9069on;

    public C3506rj() {
    }

    public C3506rj(C1616Xf xf) {
        super(xf);
    }

    /* renamed from: c */
    public C5663aRz[] mo309c() {
        return Progression._m_fields;
    }

    /* renamed from: d */
    public void mo310d() {
        super.mo310d();
        this.aZS = null;
        this.f9065dt = null;
        this.aZT = null;
        this.aZU = null;
        this.aZV = null;
        this.handle = null;
        this.f9064bK = null;
        this.aZW = 0;
        this.aZX = 0;
        this.aZY = 0;
        this.aZZ = 0;
        this.baa = 0;
        this.f9068ok = 0;
        this.f9066oL = 0;
        this.bab = 0;
        this.bac = 0;
        this.bad = 0;
        this.bae = 0;
        this.baf = 0;
        this.f9069on = 0;
        this.f9067oS = 0;
    }
}
