package logic.data.mbean;

import game.network.message.serializable.C3438ra;
import game.script.ship.OutpostLevelInfo;
import logic.baa.C1616Xf;
import logic.res.code.C5663aRz;
import p001a.C0064Am;
import p001a.C2611hZ;
import taikodom.infra.script.I18NString;

import java.util.UUID;

/* renamed from: a.aQq  reason: case insensitive filesystem */
public class C5628aQq extends C5292aDs {
    @C0064Am(aul = "0c07bb54e0ffe31a2652d8744c5f8019", aum = 2)
    public C2611hZ ayI;
    @C0064Am(aul = "57dcdc0fcae66c64416d5a6e17e81c0f", aum = 0)

    /* renamed from: bK */
    public UUID f3668bK;
    @C0064Am(aul = "0c07bb54e0ffe31a2652d8744c5f8019", aum = -2)
    public long bxR;
    @C0064Am(aul = "0c07bb54e0ffe31a2652d8744c5f8019", aum = -1)
    public byte bxU;
    @C0064Am(aul = "4f6ae553f985021dceb62ac1b0a4420a", aum = 3)
    public long gXn;
    @C0064Am(aul = "a8958430d65a072b23573787fe75184b", aum = 4)
    public C3438ra<OutpostLevelInfo.OutpostUpkeepItem> gXp;
    @C0064Am(aul = "f1a613eb8b456439a14375d2faed0f44", aum = 5)
    public int gXr;
    @C0064Am(aul = "6c69bc83b802929357c71a56eb557db4", aum = 6)
    public I18NString gXt;
    @C0064Am(aul = "51827bceb13463214403d3b6063c8161", aum = 1)
    public String handle;
    @C0064Am(aul = "a8958430d65a072b23573787fe75184b", aum = -2)
    public long iEA;
    @C0064Am(aul = "f1a613eb8b456439a14375d2faed0f44", aum = -2)
    public long iEB;
    @C0064Am(aul = "6c69bc83b802929357c71a56eb557db4", aum = -2)
    public long iEC;
    @C0064Am(aul = "4f6ae553f985021dceb62ac1b0a4420a", aum = -1)
    public byte iED;
    @C0064Am(aul = "a8958430d65a072b23573787fe75184b", aum = -1)
    public byte iEE;
    @C0064Am(aul = "f1a613eb8b456439a14375d2faed0f44", aum = -1)
    public byte iEF;
    @C0064Am(aul = "6c69bc83b802929357c71a56eb557db4", aum = -1)
    public byte iEG;
    @C0064Am(aul = "4f6ae553f985021dceb62ac1b0a4420a", aum = -2)
    public long iEz;
    @C0064Am(aul = "57dcdc0fcae66c64416d5a6e17e81c0f", aum = -2)

    /* renamed from: oL */
    public long f3669oL;
    @C0064Am(aul = "57dcdc0fcae66c64416d5a6e17e81c0f", aum = -1)

    /* renamed from: oS */
    public byte f3670oS;
    @C0064Am(aul = "51827bceb13463214403d3b6063c8161", aum = -2)

    /* renamed from: ok */
    public long f3671ok;
    @C0064Am(aul = "51827bceb13463214403d3b6063c8161", aum = -1)

    /* renamed from: on */
    public byte f3672on;

    public C5628aQq() {
    }

    public C5628aQq(C1616Xf xf) {
        super(xf);
    }

    /* renamed from: c */
    public C5663aRz[] mo309c() {
        return OutpostLevelInfo._m_fields;
    }

    /* renamed from: d */
    public void mo310d() {
        super.mo310d();
        this.f3668bK = null;
        this.handle = null;
        this.ayI = null;
        this.gXn = 0;
        this.gXp = null;
        this.gXr = 0;
        this.gXt = null;
        this.f3669oL = 0;
        this.f3671ok = 0;
        this.bxR = 0;
        this.iEz = 0;
        this.iEA = 0;
        this.iEB = 0;
        this.iEC = 0;
        this.f3670oS = 0;
        this.f3672on = 0;
        this.bxU = 0;
        this.iED = 0;
        this.iEE = 0;
        this.iEF = 0;
        this.iEG = 0;
    }
}
