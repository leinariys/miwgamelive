package logic.data.mbean;

import game.script.faction.Faction;
import game.script.npcchat.actions.ChangePlayerFactionSpeechAction;
import logic.baa.C1616Xf;
import logic.res.code.C5663aRz;
import p001a.C0064Am;

/* renamed from: a.YW */
public class C1665YW extends C6352alM {
    @C0064Am(aul = "644b87d285e9ae28d5bfb41d9ea95919", aum = 0)
    public Faction axU;
    @C0064Am(aul = "644b87d285e9ae28d5bfb41d9ea95919", aum = -2)
    public long ayi;
    @C0064Am(aul = "644b87d285e9ae28d5bfb41d9ea95919", aum = -1)
    public byte ayx;

    public C1665YW() {
    }

    public C1665YW(C1616Xf xf) {
        super(xf);
    }

    /* renamed from: c */
    public C5663aRz[] mo309c() {
        return ChangePlayerFactionSpeechAction._m_fields;
    }

    /* renamed from: d */
    public void mo310d() {
        super.mo310d();
        this.axU = null;
        this.ayi = 0;
        this.ayx = 0;
    }
}
