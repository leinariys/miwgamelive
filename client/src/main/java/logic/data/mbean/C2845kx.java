package logic.data.mbean;

import game.script.Actor;
import game.script.ai.PlayerOrbitalController;
import logic.abb.C0070Ar;
import logic.abb.C0573Hw;
import logic.abb.C1218Ry;
import logic.abb.C6822auO;
import logic.baa.C1616Xf;
import logic.res.code.C5663aRz;
import p001a.C0064Am;

/* renamed from: a.kx */
public class C2845kx extends C2272dV {
    @C0064Am(aul = "cf8e8969f576997302bfaf3afccf7c37", aum = 6)
    public Actor ams;
    @C0064Am(aul = "186e77b6d002a6d639c4444925181d18", aum = 0)
    public C0573Hw[] auc;
    @C0064Am(aul = "fc8182e36efed984920722969c37c359", aum = 1)
    public C0573Hw[] aud;
    @C0064Am(aul = "298f544cdd4be517e51bd484cb57337c", aum = 2)
    public C0573Hw[] aue;
    @C0064Am(aul = "c2447492c798c0839c37b87bc2846519", aum = 4)
    public C1218Ry auf;
    @C0064Am(aul = "dbd7d44c464b1e126f44abcef1f1810d", aum = 5)
    public C0070Ar aug;
    @C0064Am(aul = "186e77b6d002a6d639c4444925181d18", aum = -2)
    public long auh;
    @C0064Am(aul = "fc8182e36efed984920722969c37c359", aum = -2)
    public long aui;
    @C0064Am(aul = "298f544cdd4be517e51bd484cb57337c", aum = -2)
    public long auj;
    @C0064Am(aul = "b57e7b749d8302e008ec30dfe8b012e3", aum = -2)
    public long auk;
    @C0064Am(aul = "c2447492c798c0839c37b87bc2846519", aum = -2)
    public long aul;
    @C0064Am(aul = "dbd7d44c464b1e126f44abcef1f1810d", aum = -2)
    public long aum;
    @C0064Am(aul = "cf8e8969f576997302bfaf3afccf7c37", aum = -2)
    public long aun;
    @C0064Am(aul = "186e77b6d002a6d639c4444925181d18", aum = -1)
    public byte auo;
    @C0064Am(aul = "fc8182e36efed984920722969c37c359", aum = -1)
    public byte aup;
    @C0064Am(aul = "298f544cdd4be517e51bd484cb57337c", aum = -1)
    public byte auq;
    @C0064Am(aul = "b57e7b749d8302e008ec30dfe8b012e3", aum = -1)
    public byte aur;
    @C0064Am(aul = "c2447492c798c0839c37b87bc2846519", aum = -1)
    public byte aus;
    @C0064Am(aul = "dbd7d44c464b1e126f44abcef1f1810d", aum = -1)
    public byte aut;
    @C0064Am(aul = "cf8e8969f576997302bfaf3afccf7c37", aum = -1)
    public byte auu;
    @C0064Am(aul = "b57e7b749d8302e008ec30dfe8b012e3", aum = 3)
    public C6822auO avoidObstaclesBehaviour;

    public C2845kx() {
    }

    public C2845kx(C1616Xf xf) {
        super(xf);
    }

    /* renamed from: c */
    public C5663aRz[] mo309c() {
        return PlayerOrbitalController._m_fields;
    }

    /* renamed from: d */
    public void mo310d() {
        super.mo310d();
        this.auc = null;
        this.aud = null;
        this.aue = null;
        this.avoidObstaclesBehaviour = null;
        this.auf = null;
        this.aug = null;
        this.ams = null;
        this.auh = 0;
        this.aui = 0;
        this.auj = 0;
        this.auk = 0;
        this.aul = 0;
        this.aum = 0;
        this.aun = 0;
        this.auo = 0;
        this.aup = 0;
        this.auq = 0;
        this.aur = 0;
        this.aus = 0;
        this.aut = 0;
        this.auu = 0;
    }
}
