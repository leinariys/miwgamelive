package logic.data.mbean;

import game.script.item.buff.amplifier.HullAmplifier;
import game.script.ship.HullAdapter;
import logic.baa.C1616Xf;
import logic.res.code.C5663aRz;
import p001a.C0064Am;
import p001a.C3892wO;

/* renamed from: a.oK */
public class C3134oK extends C3108nv {
    @C0064Am(aul = "9abfbfae32b13da458d1135ea6ab26ea", aum = 1)
    public C3892wO aBJ;
    @C0064Am(aul = "99acb06e1ba508042bab81378d1e9734", aum = 2)
    public C3892wO aBK;
    @C0064Am(aul = "9abfbfae32b13da458d1135ea6ab26ea", aum = -2)
    public long aBO;
    @C0064Am(aul = "99acb06e1ba508042bab81378d1e9734", aum = -2)
    public long aBP;
    @C0064Am(aul = "9abfbfae32b13da458d1135ea6ab26ea", aum = -1)
    public byte aBT;
    @C0064Am(aul = "99acb06e1ba508042bab81378d1e9734", aum = -1)
    public byte aBU;
    @C0064Am(aul = "d767328ee98b375c65976dccb33ad86f", aum = 0)
    public HullAdapter aPF;
    @C0064Am(aul = "d767328ee98b375c65976dccb33ad86f", aum = -2)
    public long aPV;
    @C0064Am(aul = "d767328ee98b375c65976dccb33ad86f", aum = -1)
    public byte aPW;

    public C3134oK() {
    }

    public C3134oK(C1616Xf xf) {
        super(xf);
    }

    /* renamed from: c */
    public C5663aRz[] mo309c() {
        return HullAmplifier._m_fields;
    }

    /* renamed from: d */
    public void mo310d() {
        super.mo310d();
        this.aPF = null;
        this.aBJ = null;
        this.aBK = null;
        this.aPV = 0;
        this.aBO = 0;
        this.aBP = 0;
        this.aPW = 0;
        this.aBT = 0;
        this.aBU = 0;
    }
}
