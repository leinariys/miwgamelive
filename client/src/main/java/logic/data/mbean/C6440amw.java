package logic.data.mbean;

import game.script.mission.actions.AccomplishObjectiveMissionAction;
import logic.baa.C1616Xf;
import logic.res.code.C5663aRz;
import p001a.C0064Am;

/* renamed from: a.amw  reason: case insensitive filesystem */
public class C6440amw extends C2503fv {
    @C0064Am(aul = "388f8c892a8acb8df3e3c25e9cb81943", aum = 0)
    public String gaW;
    @C0064Am(aul = "388f8c892a8acb8df3e3c25e9cb81943", aum = -2)
    public long gaX;
    @C0064Am(aul = "388f8c892a8acb8df3e3c25e9cb81943", aum = -1)
    public byte gaY;

    public C6440amw() {
    }

    public C6440amw(C1616Xf xf) {
        super(xf);
    }

    /* renamed from: c */
    public C5663aRz[] mo309c() {
        return AccomplishObjectiveMissionAction._m_fields;
    }

    /* renamed from: d */
    public void mo310d() {
        super.mo310d();
        this.gaW = null;
        this.gaX = 0;
        this.gaY = 0;
    }
}
