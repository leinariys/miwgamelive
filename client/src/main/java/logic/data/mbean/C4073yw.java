package logic.data.mbean;

import game.network.message.serializable.C2686iZ;
import game.script.item.Module;
import game.script.item.buff.module.AttributeBuff;
import game.script.item.buff.module.AttributeBuffType;
import game.script.resource.Asset;
import game.script.ship.Ship;
import logic.baa.C1616Xf;
import logic.res.code.C5663aRz;
import p001a.C0064Am;
import p001a.C6809auB;

/* renamed from: a.yw */
public class C4073yw extends C0496Gs {
    @C0064Am(aul = "fe5a6bbc9df9985c72f51e22ef6ed8c3", aum = 9)
    public Ship aIq;
    @C0064Am(aul = "fe5a6bbc9df9985c72f51e22ef6ed8c3", aum = -2)
    public long aIs;
    @C0064Am(aul = "fe5a6bbc9df9985c72f51e22ef6ed8c3", aum = -1)
    public byte aIv;
    @C0064Am(aul = "ec16a479dab15e0c5d1fffe74280367b", aum = 10)
    public C6809auB.C1996a aZI;
    @C0064Am(aul = "7ff928d202e7c123bac6ea0d5261dfed", aum = -2)
    public long aps;
    @C0064Am(aul = "7ff928d202e7c123bac6ea0d5261dfed", aum = -1)
    public byte apx;
    @C0064Am(aul = "65374c997fc0d789c7b577825d12f7d4", aum = 0)
    public float bKI;
    @C0064Am(aul = "4860ac100d9a927cdffa18eac07c6463", aum = 1)
    public float bKJ;
    @C0064Am(aul = "f5e8399560a50d881246faa10e55dd9f", aum = 2)
    public float bKK;
    @C0064Am(aul = "5146f0f389fd1793b1631c098d0abcc3", aum = 3)
    public Asset bKL;
    @C0064Am(aul = "a52eea79c745edb31a9735935a932dec", aum = 4)
    public Asset bKM;
    @C0064Am(aul = "e6530fbc7a1304caba5848ab0b452d8c", aum = 5)
    public Asset bKN;
    @C0064Am(aul = "ae909815adf42bedb4a279f9c4442614", aum = 6)
    public float bKO;
    @C0064Am(aul = "cfba01ab7380acc6c6e83d9f0fef483f", aum = 7)
    public C2686iZ<AttributeBuffType> bKP;
    @C0064Am(aul = "c31baf72cafedef863090635f0ae502a", aum = 8)
    public C2686iZ<AttributeBuff> bKQ;
    @C0064Am(aul = "65374c997fc0d789c7b577825d12f7d4", aum = -2)
    public long bKR;
    @C0064Am(aul = "4860ac100d9a927cdffa18eac07c6463", aum = -2)
    public long bKS;
    @C0064Am(aul = "f5e8399560a50d881246faa10e55dd9f", aum = -2)
    public long bKT;
    @C0064Am(aul = "5146f0f389fd1793b1631c098d0abcc3", aum = -2)
    public long bKU;
    @C0064Am(aul = "a52eea79c745edb31a9735935a932dec", aum = -2)
    public long bKV;
    @C0064Am(aul = "e6530fbc7a1304caba5848ab0b452d8c", aum = -2)
    public long bKW;
    @C0064Am(aul = "ae909815adf42bedb4a279f9c4442614", aum = -2)
    public long bKX;
    @C0064Am(aul = "cfba01ab7380acc6c6e83d9f0fef483f", aum = -2)
    public long bKY;
    @C0064Am(aul = "c31baf72cafedef863090635f0ae502a", aum = -2)
    public long bKZ;
    @C0064Am(aul = "65374c997fc0d789c7b577825d12f7d4", aum = -1)
    public byte bLa;
    @C0064Am(aul = "4860ac100d9a927cdffa18eac07c6463", aum = -1)
    public byte bLb;
    @C0064Am(aul = "f5e8399560a50d881246faa10e55dd9f", aum = -1)
    public byte bLc;
    @C0064Am(aul = "5146f0f389fd1793b1631c098d0abcc3", aum = -1)
    public byte bLd;
    @C0064Am(aul = "a52eea79c745edb31a9735935a932dec", aum = -1)
    public byte bLe;
    @C0064Am(aul = "e6530fbc7a1304caba5848ab0b452d8c", aum = -1)
    public byte bLf;
    @C0064Am(aul = "ae909815adf42bedb4a279f9c4442614", aum = -1)
    public byte bLg;
    @C0064Am(aul = "cfba01ab7380acc6c6e83d9f0fef483f", aum = -1)
    public byte bLh;
    @C0064Am(aul = "c31baf72cafedef863090635f0ae502a", aum = -1)
    public byte bLi;
    @C0064Am(aul = "ec16a479dab15e0c5d1fffe74280367b", aum = -2)

    /* renamed from: ol */
    public long f9614ol;
    @C0064Am(aul = "ec16a479dab15e0c5d1fffe74280367b", aum = -1)

    /* renamed from: oo */
    public byte f9615oo;
    @C0064Am(aul = "7ff928d202e7c123bac6ea0d5261dfed", aum = 11)
    public long startTime;

    public C4073yw() {
    }

    public C4073yw(C1616Xf xf) {
        super(xf);
    }

    /* renamed from: c */
    public C5663aRz[] mo309c() {
        return Module._m_fields;
    }

    /* renamed from: d */
    public void mo310d() {
        super.mo310d();
        this.bKI = 0.0f;
        this.bKJ = 0.0f;
        this.bKK = 0.0f;
        this.bKL = null;
        this.bKM = null;
        this.bKN = null;
        this.bKO = 0.0f;
        this.bKP = null;
        this.bKQ = null;
        this.aIq = null;
        this.aZI = null;
        this.startTime = 0;
        this.bKR = 0;
        this.bKS = 0;
        this.bKT = 0;
        this.bKU = 0;
        this.bKV = 0;
        this.bKW = 0;
        this.bKX = 0;
        this.bKY = 0;
        this.bKZ = 0;
        this.aIs = 0;
        this.f9614ol = 0;
        this.aps = 0;
        this.bLa = 0;
        this.bLb = 0;
        this.bLc = 0;
        this.bLd = 0;
        this.bLe = 0;
        this.bLf = 0;
        this.bLg = 0;
        this.bLh = 0;
        this.bLi = 0;
        this.aIv = 0;
        this.f9615oo = 0;
        this.apx = 0;
    }
}
