package logic.data.mbean;

import game.network.message.serializable.C1556Wo;
import game.script.Character;
import game.script.ship.Station;
import logic.baa.C1616Xf;
import logic.res.code.C5663aRz;
import p001a.C0064Am;

/* renamed from: a.wk */
public class C3916wk extends C6757atB {
    @C0064Am(aul = "2df6acb7d74594aa8b1c44637b1ae768", aum = 1)
    public C1556Wo<Character, Long> bCE;
    @C0064Am(aul = "986481f19784dc991844a38c8ccfa00b", aum = 2)
    public Station bCF;
    @C0064Am(aul = "2df6acb7d74594aa8b1c44637b1ae768", aum = -2)
    public long bCG;
    @C0064Am(aul = "2df6acb7d74594aa8b1c44637b1ae768", aum = -1)
    public byte bCH;
    @C0064Am(aul = "986481f19784dc991844a38c8ccfa00b", aum = -2)

    /* renamed from: dl */
    public long f9490dl;
    @C0064Am(aul = "986481f19784dc991844a38c8ccfa00b", aum = -1)

    /* renamed from: ds */
    public byte f9491ds;
    @C0064Am(aul = "637d4448e8e9e93e54464d2d572147e2", aum = 0)

    /* renamed from: iH */
    public Station f9492iH;
    @C0064Am(aul = "637d4448e8e9e93e54464d2d572147e2", aum = -2)

    /* renamed from: iK */
    public long f9493iK;
    @C0064Am(aul = "637d4448e8e9e93e54464d2d572147e2", aum = -1)

    /* renamed from: iN */
    public byte f9494iN;

    public C3916wk() {
    }

    public C3916wk(C1616Xf xf) {
        super(xf);
    }

    /* renamed from: c */
    public C5663aRz[] mo309c() {
        return Station.C0099c._m_fields;
    }

    /* renamed from: d */
    public void mo310d() {
        super.mo310d();
        this.f9492iH = null;
        this.bCE = null;
        this.bCF = null;
        this.f9493iK = 0;
        this.bCG = 0;
        this.f9490dl = 0;
        this.f9494iN = 0;
        this.bCH = 0;
        this.f9491ds = 0;
    }
}
