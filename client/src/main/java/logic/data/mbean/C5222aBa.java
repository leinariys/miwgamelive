package logic.data.mbean;

import game.script.item.ItemType;
import game.script.itembilling.ItemBillingOrder;
import logic.baa.C1616Xf;
import logic.res.code.C5663aRz;
import p001a.C0064Am;

import java.util.UUID;

/* renamed from: a.aBa  reason: case insensitive filesystem */
public class C5222aBa extends C5292aDs {
    @C0064Am(aul = "f696ff9cd0b702392590aedd8ace133c", aum = 5)

    /* renamed from: bK */
    public UUID f2419bK;
    @C0064Am(aul = "9452917ff271c5c8834aba36bd6e929b", aum = -2)
    public long bmV;
    @C0064Am(aul = "9452917ff271c5c8834aba36bd6e929b", aum = -1)
    public byte bmW;
    @C0064Am(aul = "fb8bbcc32937e29fafabf2f02999bed1", aum = 0)
    public long bxb;
    @C0064Am(aul = "32ae47a6344c53ae4cc28fe401f46a08", aum = 1)
    public long bxd;
    @C0064Am(aul = "7c1809624f577bc1cbd86471d1dfe203", aum = 3)
    public String bxf;
    @C0064Am(aul = "9452917ff271c5c8834aba36bd6e929b", aum = 2)

    /* renamed from: cw */
    public ItemType f2420cw;
    @C0064Am(aul = "cc0fa24962c3469da19ab7371a4cdd8c", aum = 4)
    public String handle;
    @C0064Am(aul = "fb8bbcc32937e29fafabf2f02999bed1", aum = -2)
    public long hfH;
    @C0064Am(aul = "32ae47a6344c53ae4cc28fe401f46a08", aum = -2)
    public long hfI;
    @C0064Am(aul = "7c1809624f577bc1cbd86471d1dfe203", aum = -2)
    public long hfJ;
    @C0064Am(aul = "fb8bbcc32937e29fafabf2f02999bed1", aum = -1)
    public byte hfK;
    @C0064Am(aul = "32ae47a6344c53ae4cc28fe401f46a08", aum = -1)
    public byte hfL;
    @C0064Am(aul = "7c1809624f577bc1cbd86471d1dfe203", aum = -1)
    public byte hfM;
    @C0064Am(aul = "f696ff9cd0b702392590aedd8ace133c", aum = -2)

    /* renamed from: oL */
    public long f2421oL;
    @C0064Am(aul = "f696ff9cd0b702392590aedd8ace133c", aum = -1)

    /* renamed from: oS */
    public byte f2422oS;
    @C0064Am(aul = "cc0fa24962c3469da19ab7371a4cdd8c", aum = -2)

    /* renamed from: ok */
    public long f2423ok;
    @C0064Am(aul = "cc0fa24962c3469da19ab7371a4cdd8c", aum = -1)

    /* renamed from: on */
    public byte f2424on;

    public C5222aBa() {
    }

    public C5222aBa(C1616Xf xf) {
        super(xf);
    }

    /* renamed from: c */
    public C5663aRz[] mo309c() {
        return ItemBillingOrder._m_fields;
    }

    /* renamed from: d */
    public void mo310d() {
        super.mo310d();
        this.bxb = 0;
        this.bxd = 0;
        this.f2420cw = null;
        this.bxf = null;
        this.handle = null;
        this.f2419bK = null;
        this.hfH = 0;
        this.hfI = 0;
        this.bmV = 0;
        this.hfJ = 0;
        this.f2423ok = 0;
        this.f2421oL = 0;
        this.hfK = 0;
        this.hfL = 0;
        this.bmW = 0;
        this.hfM = 0;
        this.f2424on = 0;
        this.f2422oS = 0;
    }
}
