package logic.data.mbean;

import game.network.message.serializable.C2686iZ;
import game.script.avatar.cloth.ClothCategory;
import game.script.avatar.cloth.CompoundCategory;
import logic.baa.C1616Xf;
import logic.res.code.C5663aRz;
import p001a.C0064Am;

/* renamed from: a.EN */
public class C0322EN extends C4134zy {
    @C0064Am(aul = "74065acf36a47ca51d64b00b55a341a0", aum = 0)
    public C2686iZ<ClothCategory> cTU;
    @C0064Am(aul = "74065acf36a47ca51d64b00b55a341a0", aum = -2)
    public long cTV;
    @C0064Am(aul = "74065acf36a47ca51d64b00b55a341a0", aum = -1)
    public byte cTW;

    public C0322EN() {
    }

    public C0322EN(C1616Xf xf) {
        super(xf);
    }

    /* renamed from: c */
    public C5663aRz[] mo309c() {
        return CompoundCategory._m_fields;
    }

    /* renamed from: d */
    public void mo310d() {
        super.mo310d();
        this.cTU = null;
        this.cTV = 0;
        this.cTW = 0;
    }
}
