package logic.data.mbean;

import game.script.nls.NLSInteraction;
import logic.baa.C1616Xf;
import logic.res.code.C5663aRz;
import p001a.C0064Am;
import taikodom.infra.script.I18NString;

/* renamed from: a.jp */
public class C2775jp extends C2484fi {
    @C0064Am(aul = "d87baadc6a7331faf8821c50722bdd81", aum = 1)
    public I18NString ajA;
    @C0064Am(aul = "7f91ebc1b9336bc5467767c35f2807c8", aum = 2)
    public I18NString ajB;
    @C0064Am(aul = "20cee661201ca29734d5dcf8730edd18", aum = 3)
    public I18NString ajC;
    @C0064Am(aul = "60e2edd623e385bd92eefea1a021d0bc", aum = 4)
    public I18NString ajD;
    @C0064Am(aul = "1a3f15081c15832559aa477711980184", aum = 5)
    public I18NString ajE;
    @C0064Am(aul = "e2e349b60778e19d2dc671e1b95efb9f", aum = 6)
    public I18NString ajF;
    @C0064Am(aul = "6b2006ea287d15fa291a36e0a91c59fc", aum = 7)
    public I18NString ajG;
    @C0064Am(aul = "515d81bfc40d529862fe14586ed1358b", aum = 8)
    public I18NString ajH;
    @C0064Am(aul = "7f3a68f19e7cace0c2088dbfd79909cb", aum = 9)
    public I18NString ajI;
    @C0064Am(aul = "44701e7353e74545810b96f922f13b6c", aum = 10)
    public I18NString ajJ;
    @C0064Am(aul = "068a89108be3a95696191c923d81b630", aum = 11)
    public I18NString ajK;
    @C0064Am(aul = "becdefed20bf976e1e792c2d90d62a5a", aum = -2)
    public long ajL;
    @C0064Am(aul = "d87baadc6a7331faf8821c50722bdd81", aum = -2)
    public long ajM;
    @C0064Am(aul = "7f91ebc1b9336bc5467767c35f2807c8", aum = -2)
    public long ajN;
    @C0064Am(aul = "20cee661201ca29734d5dcf8730edd18", aum = -2)
    public long ajO;
    @C0064Am(aul = "60e2edd623e385bd92eefea1a021d0bc", aum = -2)
    public long ajP;
    @C0064Am(aul = "1a3f15081c15832559aa477711980184", aum = -2)
    public long ajQ;
    @C0064Am(aul = "e2e349b60778e19d2dc671e1b95efb9f", aum = -2)
    public long ajR;
    @C0064Am(aul = "6b2006ea287d15fa291a36e0a91c59fc", aum = -2)
    public long ajS;
    @C0064Am(aul = "515d81bfc40d529862fe14586ed1358b", aum = -2)
    public long ajT;
    @C0064Am(aul = "7f3a68f19e7cace0c2088dbfd79909cb", aum = -2)
    public long ajU;
    @C0064Am(aul = "44701e7353e74545810b96f922f13b6c", aum = -2)
    public long ajV;
    @C0064Am(aul = "068a89108be3a95696191c923d81b630", aum = -2)
    public long ajW;
    @C0064Am(aul = "becdefed20bf976e1e792c2d90d62a5a", aum = -1)
    public byte ajX;
    @C0064Am(aul = "d87baadc6a7331faf8821c50722bdd81", aum = -1)
    public byte ajY;
    @C0064Am(aul = "7f91ebc1b9336bc5467767c35f2807c8", aum = -1)
    public byte ajZ;
    @C0064Am(aul = "becdefed20bf976e1e792c2d90d62a5a", aum = 0)
    public I18NString ajz;
    @C0064Am(aul = "20cee661201ca29734d5dcf8730edd18", aum = -1)
    public byte aka;
    @C0064Am(aul = "60e2edd623e385bd92eefea1a021d0bc", aum = -1)
    public byte akb;
    @C0064Am(aul = "1a3f15081c15832559aa477711980184", aum = -1)
    public byte akc;
    @C0064Am(aul = "e2e349b60778e19d2dc671e1b95efb9f", aum = -1)
    public byte akd;
    @C0064Am(aul = "6b2006ea287d15fa291a36e0a91c59fc", aum = -1)
    public byte ake;
    @C0064Am(aul = "515d81bfc40d529862fe14586ed1358b", aum = -1)
    public byte akf;
    @C0064Am(aul = "7f3a68f19e7cace0c2088dbfd79909cb", aum = -1)
    public byte akg;
    @C0064Am(aul = "44701e7353e74545810b96f922f13b6c", aum = -1)
    public byte akh;
    @C0064Am(aul = "068a89108be3a95696191c923d81b630", aum = -1)
    public byte aki;

    public C2775jp() {
    }

    public C2775jp(C1616Xf xf) {
        super(xf);
    }

    /* renamed from: c */
    public C5663aRz[] mo309c() {
        return NLSInteraction._m_fields;
    }

    /* renamed from: d */
    public void mo310d() {
        super.mo310d();
        this.ajz = null;
        this.ajA = null;
        this.ajB = null;
        this.ajC = null;
        this.ajD = null;
        this.ajE = null;
        this.ajF = null;
        this.ajG = null;
        this.ajH = null;
        this.ajI = null;
        this.ajJ = null;
        this.ajK = null;
        this.ajL = 0;
        this.ajM = 0;
        this.ajN = 0;
        this.ajO = 0;
        this.ajP = 0;
        this.ajQ = 0;
        this.ajR = 0;
        this.ajS = 0;
        this.ajT = 0;
        this.ajU = 0;
        this.ajV = 0;
        this.ajW = 0;
        this.ajX = 0;
        this.ajY = 0;
        this.ajZ = 0;
        this.aka = 0;
        this.akb = 0;
        this.akc = 0;
        this.akd = 0;
        this.ake = 0;
        this.akf = 0;
        this.akg = 0;
        this.akh = 0;
        this.aki = 0;
    }
}
