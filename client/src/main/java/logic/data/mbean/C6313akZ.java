package logic.data.mbean;

import game.script.ship.categories.Fighter;
import game.script.ship.speedBoost.SpeedBoost;
import game.script.ship.speedBoost.SpeedBoostType;
import logic.baa.C1616Xf;
import logic.res.code.C5663aRz;
import p001a.C0064Am;

/* renamed from: a.akZ  reason: case insensitive filesystem */
public class C6313akZ extends aMX {
    @C0064Am(aul = "3ac2768fac77612aeeddc30d53149ddd", aum = 0)
    public SpeedBoostType dDF;
    @C0064Am(aul = "d1bb063167dc89e8fe98eff1c2e8cdd8", aum = 1)
    public SpeedBoost dDH;
    @C0064Am(aul = "3ac2768fac77612aeeddc30d53149ddd", aum = -2)
    public long fVd;
    @C0064Am(aul = "d1bb063167dc89e8fe98eff1c2e8cdd8", aum = -2)
    public long fVe;
    @C0064Am(aul = "3ac2768fac77612aeeddc30d53149ddd", aum = -1)
    public byte fVf;
    @C0064Am(aul = "d1bb063167dc89e8fe98eff1c2e8cdd8", aum = -1)
    public byte fVg;

    public C6313akZ() {
    }

    public C6313akZ(C1616Xf xf) {
        super(xf);
    }

    /* renamed from: c */
    public C5663aRz[] mo309c() {
        return Fighter._m_fields;
    }

    /* renamed from: d */
    public void mo310d() {
        super.mo310d();
        this.dDF = null;
        this.dDH = null;
        this.fVd = 0;
        this.fVe = 0;
        this.fVf = 0;
        this.fVg = 0;
    }
}
