package logic.data.mbean;

import game.script.ship.Outpost;
import game.script.ship.OutpostUpkeepPeriod;
import logic.baa.C1616Xf;
import logic.res.code.C5663aRz;
import p001a.C0064Am;

/* renamed from: a.rz */
public class C3526rz extends C3805vD {
    @C0064Am(aul = "961a6ebdf565dbb31ae526bf706969a8", aum = 0)
    public Outpost baT;
    @C0064Am(aul = "6270b37cb7ee88d61df496e20b155aa4", aum = 1)
    public long baU;
    @C0064Am(aul = "6f267e3ae248e6e69d9f7c49beb986c4", aum = 2)
    public boolean baV;
    @C0064Am(aul = "22c7217c166a2d185638cdc2f3d82b9e", aum = 3)
    public OutpostUpkeepPeriod.OutpostUpkeepTicker baW;
    @C0064Am(aul = "961a6ebdf565dbb31ae526bf706969a8", aum = -2)
    public long baX;
    @C0064Am(aul = "6270b37cb7ee88d61df496e20b155aa4", aum = -2)
    public long baY;
    @C0064Am(aul = "6f267e3ae248e6e69d9f7c49beb986c4", aum = -2)
    public long baZ;
    @C0064Am(aul = "22c7217c166a2d185638cdc2f3d82b9e", aum = -2)
    public long bba;
    @C0064Am(aul = "961a6ebdf565dbb31ae526bf706969a8", aum = -1)
    public byte bbb;
    @C0064Am(aul = "6270b37cb7ee88d61df496e20b155aa4", aum = -1)
    public byte bbc;
    @C0064Am(aul = "6f267e3ae248e6e69d9f7c49beb986c4", aum = -1)
    public byte bbd;
    @C0064Am(aul = "22c7217c166a2d185638cdc2f3d82b9e", aum = -1)
    public byte bbe;

    public C3526rz() {
    }

    public C3526rz(C1616Xf xf) {
        super(xf);
    }

    /* renamed from: c */
    public C5663aRz[] mo309c() {
        return OutpostUpkeepPeriod._m_fields;
    }

    /* renamed from: d */
    public void mo310d() {
        super.mo310d();
        this.baT = null;
        this.baU = 0;
        this.baV = false;
        this.baW = null;
        this.baX = 0;
        this.baY = 0;
        this.baZ = 0;
        this.bba = 0;
        this.bbb = 0;
        this.bbc = 0;
        this.bbd = 0;
        this.bbe = 0;
    }
}
