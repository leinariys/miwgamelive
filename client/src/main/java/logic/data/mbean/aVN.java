package logic.data.mbean;

import game.script.missile.Missile;
import game.script.missile.MissileWeapon;
import game.script.resource.Asset;
import game.script.ship.Hull;
import game.script.ship.Ship;
import logic.baa.C1616Xf;
import logic.res.code.C5663aRz;
import p001a.C0064Am;
import p001a.C5260aCm;

/* renamed from: a.aVN */
public class aVN extends aLU {
    @C0064Am(aul = "a37a065ec7655dc17872819b0eb49d16", aum = 9)

    /* renamed from: VV */
    public Hull f3908VV;
    @C0064Am(aul = "81e82d2135c3a5ec73ea9d7164494456", aum = 5)

    /* renamed from: VX */
    public float f3909VX;
    @C0064Am(aul = "458ad6213838f680faccaa443248c12c", aum = 6)

    /* renamed from: Wb */
    public Asset f3910Wb;
    @C0064Am(aul = "cbf43e0528a6ce81227dae5118494ebb", aum = 7)

    /* renamed from: Wd */
    public Asset f3911Wd;
    @C0064Am(aul = "bdb08028c905e24e12333ffd716d2d23", aum = -2)
    public long ail;
    @C0064Am(aul = "bdb08028c905e24e12333ffd716d2d23", aum = -1)
    public byte aiv;
    @C0064Am(aul = "f89e131fd383c9a3c38836b8a6b004d0", aum = -2)
    public long beq;
    @C0064Am(aul = "f89e131fd383c9a3c38836b8a6b004d0", aum = -1)
    public byte bes;
    @C0064Am(aul = "27b405c0c79424a5bb9a7b03d3070bac", aum = -1)
    public byte bhA;
    @C0064Am(aul = "81e82d2135c3a5ec73ea9d7164494456", aum = -1)
    public byte bhB;
    @C0064Am(aul = "458ad6213838f680faccaa443248c12c", aum = -1)
    public byte bhC;
    @C0064Am(aul = "cbf43e0528a6ce81227dae5118494ebb", aum = -1)
    public byte bhD;
    @C0064Am(aul = "d465131b60fb0a733384e50c2f10ecce", aum = -1)
    public byte bhE;
    @C0064Am(aul = "a37a065ec7655dc17872819b0eb49d16", aum = -1)
    public byte bhF;
    @C0064Am(aul = "5634d241e551b7249427890d4faf4e38", aum = 0)
    public float bhk;
    @C0064Am(aul = "85b9a3643888c662d08f8e27090990a4", aum = 2)
    public float bhl;
    @C0064Am(aul = "d465131b60fb0a733384e50c2f10ecce", aum = 8)
    public Asset bhm;
    @C0064Am(aul = "5634d241e551b7249427890d4faf4e38", aum = -2)
    public long bho;
    @C0064Am(aul = "391f192cd1294c3e3701a44df0c514b9", aum = -2)
    public long bhp;
    @C0064Am(aul = "85b9a3643888c662d08f8e27090990a4", aum = -2)
    public long bhq;
    @C0064Am(aul = "27b405c0c79424a5bb9a7b03d3070bac", aum = -2)
    public long bhr;
    @C0064Am(aul = "81e82d2135c3a5ec73ea9d7164494456", aum = -2)
    public long bhs;
    @C0064Am(aul = "458ad6213838f680faccaa443248c12c", aum = -2)
    public long bht;
    @C0064Am(aul = "cbf43e0528a6ce81227dae5118494ebb", aum = -2)
    public long bhu;
    @C0064Am(aul = "d465131b60fb0a733384e50c2f10ecce", aum = -2)
    public long bhv;
    @C0064Am(aul = "a37a065ec7655dc17872819b0eb49d16", aum = -2)
    public long bhw;
    @C0064Am(aul = "5634d241e551b7249427890d4faf4e38", aum = -1)
    public byte bhx;
    @C0064Am(aul = "391f192cd1294c3e3701a44df0c514b9", aum = -1)
    public byte bhy;
    @C0064Am(aul = "85b9a3643888c662d08f8e27090990a4", aum = -1)
    public byte bhz;
    @C0064Am(aul = "391f192cd1294c3e3701a44df0c514b9", aum = 1)

    /* renamed from: dW */
    public float f3912dW;
    @C0064Am(aul = "27b405c0c79424a5bb9a7b03d3070bac", aum = 3)

    /* renamed from: dX */
    public float f3913dX;
    @C0064Am(aul = "140fc30eed46cef01050de3e7818b303", aum = 12)
    public MissileWeapon hHj;
    @C0064Am(aul = "b2192ff85ac044a6cd5a66e9e89b8187", aum = -2)
    public long iyH;
    @C0064Am(aul = "b2192ff85ac044a6cd5a66e9e89b8187", aum = -1)
    public byte iyJ;
    @C0064Am(aul = "140fc30eed46cef01050de3e7818b303", aum = -2)
    public long jcr;
    @C0064Am(aul = "140fc30eed46cef01050de3e7818b303", aum = -1)
    public byte jcs;
    @C0064Am(aul = "bdb08028c905e24e12333ffd716d2d23", aum = 4)

    /* renamed from: kY */
    public long f3914kY;
    @C0064Am(aul = "b2192ff85ac044a6cd5a66e9e89b8187", aum = 10)
    public Ship trackedShip;
    @C0064Am(aul = "f89e131fd383c9a3c38836b8a6b004d0", aum = 11)

    /* renamed from: uJ */
    public C5260aCm f3915uJ;

    public aVN() {
    }

    public aVN(C1616Xf xf) {
        super(xf);
    }

    /* renamed from: c */
    public C5663aRz[] mo309c() {
        return Missile._m_fields;
    }

    /* renamed from: d */
    public void mo310d() {
        super.mo310d();
        this.bhk = 0.0f;
        this.f3912dW = 0.0f;
        this.bhl = 0.0f;
        this.f3913dX = 0.0f;
        this.f3914kY = 0;
        this.f3909VX = 0.0f;
        this.f3910Wb = null;
        this.f3911Wd = null;
        this.bhm = null;
        this.f3908VV = null;
        this.trackedShip = null;
        this.f3915uJ = null;
        this.hHj = null;
        this.bho = 0;
        this.bhp = 0;
        this.bhq = 0;
        this.bhr = 0;
        this.ail = 0;
        this.bhs = 0;
        this.bht = 0;
        this.bhu = 0;
        this.bhv = 0;
        this.bhw = 0;
        this.iyH = 0;
        this.beq = 0;
        this.jcr = 0;
        this.bhx = 0;
        this.bhy = 0;
        this.bhz = 0;
        this.bhA = 0;
        this.aiv = 0;
        this.bhB = 0;
        this.bhC = 0;
        this.bhD = 0;
        this.bhE = 0;
        this.bhF = 0;
        this.iyJ = 0;
        this.bes = 0;
        this.jcs = 0;
    }
}
