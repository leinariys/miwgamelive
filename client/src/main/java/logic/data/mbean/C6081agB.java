package logic.data.mbean;

import game.script.item.buff.module.ShipBuff;
import logic.baa.C1616Xf;
import logic.res.code.C5663aRz;
import p001a.C0064Am;
import p001a.C3892wO;

/* renamed from: a.agB  reason: case insensitive filesystem */
public class C6081agB extends C4037yS {
    @C0064Am(aul = "9374fcf556aa8804decd503f6738f6c6", aum = 4)

    /* renamed from: VA */
    public C3892wO f4493VA;
    @C0064Am(aul = "24b865b4267967696214726616d40dbd", aum = 5)

    /* renamed from: VC */
    public boolean f4494VC;
    @C0064Am(aul = "5bea99e79614072f5370f229354aa1c4", aum = 0)

    /* renamed from: Vs */
    public C3892wO f4495Vs;
    @C0064Am(aul = "b0a194317c28a781385e5de8a4c5174c", aum = 1)

    /* renamed from: Vu */
    public C3892wO f4496Vu;
    @C0064Am(aul = "59aa43df301fc26fb17165842be0352a", aum = 2)

    /* renamed from: Vw */
    public C3892wO f4497Vw;
    @C0064Am(aul = "77c9bd3dea0143dfb308951123a25c17", aum = 3)

    /* renamed from: Vy */
    public C3892wO f4498Vy;
    @C0064Am(aul = "5bea99e79614072f5370f229354aa1c4", aum = -1)

    /* renamed from: YC */
    public byte f4499YC;
    @C0064Am(aul = "b0a194317c28a781385e5de8a4c5174c", aum = -1)

    /* renamed from: YD */
    public byte f4500YD;
    @C0064Am(aul = "59aa43df301fc26fb17165842be0352a", aum = -1)

    /* renamed from: YE */
    public byte f4501YE;
    @C0064Am(aul = "77c9bd3dea0143dfb308951123a25c17", aum = -1)

    /* renamed from: YF */
    public byte f4502YF;
    @C0064Am(aul = "5bea99e79614072f5370f229354aa1c4", aum = -2)

    /* renamed from: Yw */
    public long f4503Yw;
    @C0064Am(aul = "b0a194317c28a781385e5de8a4c5174c", aum = -2)

    /* renamed from: Yx */
    public long f4504Yx;
    @C0064Am(aul = "59aa43df301fc26fb17165842be0352a", aum = -2)

    /* renamed from: Yy */
    public long f4505Yy;
    @C0064Am(aul = "77c9bd3dea0143dfb308951123a25c17", aum = -2)

    /* renamed from: Yz */
    public long f4506Yz;
    @C0064Am(aul = "9374fcf556aa8804decd503f6738f6c6", aum = -2)
    public long bHw;
    @C0064Am(aul = "50582fbd9328407036f3194c6a903aa6", aum = -2)
    public long bHx;
    @C0064Am(aul = "9374fcf556aa8804decd503f6738f6c6", aum = -1)
    public byte bHy;
    @C0064Am(aul = "50582fbd9328407036f3194c6a903aa6", aum = -1)
    public byte bHz;
    @C0064Am(aul = "50582fbd9328407036f3194c6a903aa6", aum = 6)
    public ShipBuff.ShipModuleAdapter bjI;
    @C0064Am(aul = "24b865b4267967696214726616d40dbd", aum = -2)
    public long fyC;
    @C0064Am(aul = "24b865b4267967696214726616d40dbd", aum = -1)
    public byte fyD;

    public C6081agB() {
    }

    public C6081agB(C1616Xf xf) {
        super(xf);
    }

    /* renamed from: c */
    public C5663aRz[] mo309c() {
        return ShipBuff._m_fields;
    }

    /* renamed from: d */
    public void mo310d() {
        super.mo310d();
        this.f4495Vs = null;
        this.f4496Vu = null;
        this.f4497Vw = null;
        this.f4498Vy = null;
        this.f4493VA = null;
        this.f4494VC = false;
        this.bjI = null;
        this.f4503Yw = 0;
        this.f4504Yx = 0;
        this.f4505Yy = 0;
        this.f4506Yz = 0;
        this.bHw = 0;
        this.fyC = 0;
        this.bHx = 0;
        this.f4499YC = 0;
        this.f4500YD = 0;
        this.f4501YE = 0;
        this.f4502YF = 0;
        this.bHy = 0;
        this.fyD = 0;
        this.bHz = 0;
    }
}
