package logic.data.mbean;

import game.script.item.RayWeapon;
import logic.baa.C1616Xf;
import logic.res.code.C5663aRz;
import p001a.C0064Am;

/* renamed from: a.pj */
public class C3262pj extends C0291Dl {
    @C0064Am(aul = "df2d8a20fec672f8631d978ea261230b", aum = -2)
    public long ail;
    @C0064Am(aul = "df2d8a20fec672f8631d978ea261230b", aum = -1)
    public byte aiv;
    @C0064Am(aul = "df2d8a20fec672f8631d978ea261230b", aum = 0)
    public float lifeTime;

    public C3262pj() {
    }

    public C3262pj(C1616Xf xf) {
        super(xf);
    }

    /* renamed from: c */
    public C5663aRz[] mo309c() {
        return RayWeapon._m_fields;
    }

    /* renamed from: d */
    public void mo310d() {
        super.mo310d();
        this.lifeTime = 0.0f;
        this.ail = 0;
        this.aiv = 0;
    }
}
