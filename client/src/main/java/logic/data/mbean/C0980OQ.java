package logic.data.mbean;

import game.script.npcchat.AbstractSpeech;
import game.script.npcchat.SpeechRequirementCollection;
import logic.baa.C1616Xf;
import logic.res.code.C5663aRz;
import p001a.C0064Am;
import taikodom.infra.script.I18NString;

import java.util.UUID;

/* renamed from: a.OQ */
public class C0980OQ extends C3805vD {
    @C0064Am(aul = "7682579a7b6a3e977c972cce382a5b9d", aum = -2)
    public long aTT;
    @C0064Am(aul = "7682579a7b6a3e977c972cce382a5b9d", aum = -1)
    public byte aTW;
    @C0064Am(aul = "4eb290bac48a56c1431447b51b1e5857", aum = 3)

    /* renamed from: bK */
    public UUID f1303bK;
    @C0064Am(aul = "7682579a7b6a3e977c972cce382a5b9d", aum = 1)
    public I18NString dOM;
    @C0064Am(aul = "a420fc3bca8051d0204e38478d3561d8", aum = 2)
    public SpeechRequirementCollection dON;
    @C0064Am(aul = "a420fc3bca8051d0204e38478d3561d8", aum = -2)
    public long dOO;
    @C0064Am(aul = "a420fc3bca8051d0204e38478d3561d8", aum = -1)
    public byte dOP;
    @C0064Am(aul = "38d3a224b21ef9540e936914a7840a39", aum = 0)
    public String handle;
    @C0064Am(aul = "4eb290bac48a56c1431447b51b1e5857", aum = -2)

    /* renamed from: oL */
    public long f1304oL;
    @C0064Am(aul = "4eb290bac48a56c1431447b51b1e5857", aum = -1)

    /* renamed from: oS */
    public byte f1305oS;
    @C0064Am(aul = "38d3a224b21ef9540e936914a7840a39", aum = -2)

    /* renamed from: ok */
    public long f1306ok;
    @C0064Am(aul = "38d3a224b21ef9540e936914a7840a39", aum = -1)

    /* renamed from: on */
    public byte f1307on;

    public C0980OQ() {
    }

    public C0980OQ(C1616Xf xf) {
        super(xf);
    }

    /* renamed from: c */
    public C5663aRz[] mo309c() {
        return AbstractSpeech._m_fields;
    }

    /* renamed from: d */
    public void mo310d() {
        super.mo310d();
        this.handle = null;
        this.dOM = null;
        this.dON = null;
        this.f1303bK = null;
        this.f1306ok = 0;
        this.aTT = 0;
        this.dOO = 0;
        this.f1304oL = 0;
        this.f1307on = 0;
        this.aTW = 0;
        this.dOP = 0;
        this.f1305oS = 0;
    }
}
