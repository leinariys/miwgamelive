package logic.data.mbean;

import game.network.message.serializable.C3438ra;
import game.script.citizenship.CitizenImprovement;
import game.script.citizenship.CitizenImprovementType;
import game.script.citizenship.Citizenship;
import game.script.citizenship.CitizenshipReward;
import game.script.player.Player;
import game.script.resource.Asset;
import logic.baa.C1616Xf;
import logic.res.code.C5663aRz;
import p001a.C0064Am;
import p001a.C5611aPz;
import taikodom.infra.script.I18NString;

/* renamed from: a.aWw */
public class aWw extends C3805vD {
    @C0064Am(aul = "30053ca7e7e2f6dc432be7787355a8d6", aum = 14)

    /* renamed from: Q */
    public long f4028Q;
    @C0064Am(aul = "30053ca7e7e2f6dc432be7787355a8d6", aum = -2)

    /* renamed from: ah */
    public long f4029ah;
    @C0064Am(aul = "30053ca7e7e2f6dc432be7787355a8d6", aum = -1)

    /* renamed from: aw */
    public byte f4030aw;
    @C0064Am(aul = "7e8b6e5259f4d661e059d97d779a801d", aum = 1)

    /* renamed from: db */
    public long f4031db;
    @C0064Am(aul = "7e8b6e5259f4d661e059d97d779a801d", aum = -2)

    /* renamed from: dj */
    public long f4032dj;
    @C0064Am(aul = "7e8b6e5259f4d661e059d97d779a801d", aum = -1)

    /* renamed from: dq */
    public byte f4033dq;
    @C0064Am(aul = "91c30a659c7b24fd98b1cf4cf7207179", aum = 5)
    public Asset eQY;
    @C0064Am(aul = "5f19f19a56ad40c5a6ca6d835146f39f", aum = 6)
    public Asset eRa;
    @C0064Am(aul = "d60ebff7f99bd1083534cbf93f34c21b", aum = 7)
    public Asset eRc;
    @C0064Am(aul = "9c5e0db8fbfc5fcb335a7bf502a3f95b", aum = 8)
    public C3438ra<CitizenImprovementType> eRe;
    @C0064Am(aul = "e6c6789ee9782a0b2d9527a780341bce", aum = 9)
    public C3438ra<CitizenshipReward> eRf;
    @C0064Am(aul = "8cd230f3505ee24acac172520842a050", aum = 10)
    public C3438ra<CitizenshipReward> eRh;
    @C0064Am(aul = "83c7c84e302090d9b106f2c607eff2a1", aum = 11)
    public C3438ra<CitizenshipReward> eRj;
    @C0064Am(aul = "92e73b8b4689e41d243f4ab1a84d6cc8", aum = 12)
    public C3438ra<CitizenshipReward> eRl;
    @C0064Am(aul = "9975f35dd73849108bedf6d5f8608f7d", aum = 13)
    public C3438ra<CitizenshipReward> eRn;
    @C0064Am(aul = "393455cd0313fe80969667c9261a46f3", aum = 15)
    public TaskletImpl eRp;
    @C0064Am(aul = "c2f848381c9ea50455af7627c2658918", aum = 16)
    public C5611aPz eRr;
    @C0064Am(aul = "898a1d5efcee51cd116469e706818f88", aum = 17)
    public C3438ra<CitizenImprovement> eRt;
    @C0064Am(aul = "8a15698326e01af47ae1cb642cea6613", aum = 19)
    public long eRv;
    @C0064Am(aul = "86ac84cdb9d4db53934ad72f73db5152", aum = 2)
    public float egP;
    @C0064Am(aul = "86ac84cdb9d4db53934ad72f73db5152", aum = -2)
    public long egT;
    @C0064Am(aul = "86ac84cdb9d4db53934ad72f73db5152", aum = -1)
    public byte egX;
    @C0064Am(aul = "9c5e0db8fbfc5fcb335a7bf502a3f95b", aum = -2)
    public long fEX;
    @C0064Am(aul = "9c5e0db8fbfc5fcb335a7bf502a3f95b", aum = -1)
    public byte fFa;
    @C0064Am(aul = "91c30a659c7b24fd98b1cf4cf7207179", aum = -2)
    public long gkT;
    @C0064Am(aul = "5f19f19a56ad40c5a6ca6d835146f39f", aum = -2)
    public long gkU;
    @C0064Am(aul = "d60ebff7f99bd1083534cbf93f34c21b", aum = -2)
    public long gkV;
    @C0064Am(aul = "e6c6789ee9782a0b2d9527a780341bce", aum = -2)
    public long gkW;
    @C0064Am(aul = "8cd230f3505ee24acac172520842a050", aum = -2)
    public long gkX;
    @C0064Am(aul = "83c7c84e302090d9b106f2c607eff2a1", aum = -2)
    public long gkY;
    @C0064Am(aul = "92e73b8b4689e41d243f4ab1a84d6cc8", aum = -2)
    public long gkZ;
    @C0064Am(aul = "9975f35dd73849108bedf6d5f8608f7d", aum = -2)
    public long gla;
    @C0064Am(aul = "91c30a659c7b24fd98b1cf4cf7207179", aum = -1)
    public byte glb;
    @C0064Am(aul = "5f19f19a56ad40c5a6ca6d835146f39f", aum = -1)
    public byte glc;
    @C0064Am(aul = "d60ebff7f99bd1083534cbf93f34c21b", aum = -1)
    public byte gld;
    @C0064Am(aul = "e6c6789ee9782a0b2d9527a780341bce", aum = -1)
    public byte gle;
    @C0064Am(aul = "8cd230f3505ee24acac172520842a050", aum = -1)
    public byte glf;
    @C0064Am(aul = "83c7c84e302090d9b106f2c607eff2a1", aum = -1)
    public byte glg;
    @C0064Am(aul = "92e73b8b4689e41d243f4ab1a84d6cc8", aum = -1)
    public byte glh;
    @C0064Am(aul = "9975f35dd73849108bedf6d5f8608f7d", aum = -1)
    public byte gli;
    @C0064Am(aul = "7c74f0e3f9d3dfa10360028d11d8b306", aum = -1)

    /* renamed from: jK */
    public byte f4034jK;
    @C0064Am(aul = "c2f848381c9ea50455af7627c2658918", aum = -1)
    public byte jgA;
    @C0064Am(aul = "898a1d5efcee51cd116469e706818f88", aum = -1)
    public byte jgB;
    @C0064Am(aul = "8a15698326e01af47ae1cb642cea6613", aum = -1)
    public byte jgC;
    @C0064Am(aul = "393455cd0313fe80969667c9261a46f3", aum = -2)
    public long jgv;
    @C0064Am(aul = "c2f848381c9ea50455af7627c2658918", aum = -2)
    public long jgw;
    @C0064Am(aul = "898a1d5efcee51cd116469e706818f88", aum = -2)
    public long jgx;
    @C0064Am(aul = "8a15698326e01af47ae1cb642cea6613", aum = -2)
    public long jgy;
    @C0064Am(aul = "393455cd0313fe80969667c9261a46f3", aum = -1)
    public byte jgz;
    @C0064Am(aul = "7c74f0e3f9d3dfa10360028d11d8b306", aum = 4)

    /* renamed from: jn */
    public Asset f4035jn;
    @C0064Am(aul = "7c74f0e3f9d3dfa10360028d11d8b306", aum = -2)

    /* renamed from: jy */
    public long f4036jy;
    @C0064Am(aul = "ee369b6a941d718c5122c1ee94c3dc8f", aum = 0)

    /* renamed from: nh */
    public I18NString f4037nh;
    @C0064Am(aul = "5f80dd66c139c1151005f7480e3e2fc2", aum = 3)

    /* renamed from: ni */
    public I18NString f4038ni;
    @C0064Am(aul = "3f6273761cd37e4a198f0d04be331b80", aum = 18)

    /* renamed from: nj */
    public Player f4039nj;
    @C0064Am(aul = "ee369b6a941d718c5122c1ee94c3dc8f", aum = -2)

    /* renamed from: nk */
    public long f4040nk;
    @C0064Am(aul = "5f80dd66c139c1151005f7480e3e2fc2", aum = -2)

    /* renamed from: nl */
    public long f4041nl;
    @C0064Am(aul = "3f6273761cd37e4a198f0d04be331b80", aum = -2)

    /* renamed from: nm */
    public long f4042nm;
    @C0064Am(aul = "ee369b6a941d718c5122c1ee94c3dc8f", aum = -1)

    /* renamed from: nn */
    public byte f4043nn;
    @C0064Am(aul = "5f80dd66c139c1151005f7480e3e2fc2", aum = -1)

    /* renamed from: no */
    public byte f4044no;
    @C0064Am(aul = "3f6273761cd37e4a198f0d04be331b80", aum = -1)

    /* renamed from: np */
    public byte f4045np;

    public aWw() {
    }

    public aWw(C1616Xf xf) {
        super(xf);
    }

    /* renamed from: c */
    public C5663aRz[] mo309c() {
        return Citizenship._m_fields;
    }

    /* renamed from: d */
    public void mo310d() {
        super.mo310d();
        this.f4037nh = null;
        this.f4031db = 0;
        this.egP = 0.0f;
        this.f4038ni = null;
        this.f4035jn = null;
        this.eQY = null;
        this.eRa = null;
        this.eRc = null;
        this.eRe = null;
        this.eRf = null;
        this.eRh = null;
        this.eRj = null;
        this.eRl = null;
        this.eRn = null;
        this.f4028Q = 0;
        this.eRp = null;
        this.eRr = null;
        this.eRt = null;
        this.f4039nj = null;
        this.eRv = 0;
        this.f4040nk = 0;
        this.f4032dj = 0;
        this.egT = 0;
        this.f4041nl = 0;
        this.f4036jy = 0;
        this.gkT = 0;
        this.gkU = 0;
        this.gkV = 0;
        this.fEX = 0;
        this.gkW = 0;
        this.gkX = 0;
        this.gkY = 0;
        this.gkZ = 0;
        this.gla = 0;
        this.f4029ah = 0;
        this.jgv = 0;
        this.jgw = 0;
        this.jgx = 0;
        this.f4042nm = 0;
        this.jgy = 0;
        this.f4043nn = 0;
        this.f4033dq = 0;
        this.egX = 0;
        this.f4044no = 0;
        this.f4034jK = 0;
        this.glb = 0;
        this.glc = 0;
        this.gld = 0;
        this.fFa = 0;
        this.gle = 0;
        this.glf = 0;
        this.glg = 0;
        this.glh = 0;
        this.gli = 0;
        this.f4030aw = 0;
        this.jgz = 0;
        this.jgA = 0;
        this.jgB = 0;
        this.f4045np = 0;
        this.jgC = 0;
    }
}
