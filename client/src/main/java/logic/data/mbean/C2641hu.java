package logic.data.mbean;

import game.script.ai.npc.ScriptableAIControllerType;
import logic.baa.C1616Xf;
import logic.res.code.C5663aRz;
import p001a.C0064Am;

/* renamed from: a.hu */
public class C2641hu extends C0757Kp {
    @C0064Am(aul = "5fa3a998929922a81517111b0112010e", aum = 0)

    /* renamed from: UA */
    public String f8073UA;
    @C0064Am(aul = "5fa3a998929922a81517111b0112010e", aum = -2)

    /* renamed from: UB */
    public long f8074UB;
    @C0064Am(aul = "5fa3a998929922a81517111b0112010e", aum = -1)

    /* renamed from: UC */
    public byte f8075UC;

    public C2641hu() {
    }

    public C2641hu(C1616Xf xf) {
        super(xf);
    }

    /* renamed from: c */
    public C5663aRz[] mo309c() {
        return ScriptableAIControllerType._m_fields;
    }

    /* renamed from: d */
    public void mo310d() {
        super.mo310d();
        this.f8073UA = null;
        this.f8074UB = 0;
        this.f8075UC = 0;
    }
}
