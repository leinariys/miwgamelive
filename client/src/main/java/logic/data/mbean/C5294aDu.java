package logic.data.mbean;

import game.network.message.serializable.C3438ra;
import game.script.item.AmplifierType;
import game.script.item.buff.amplifier.ComposedAmplifierType;
import logic.baa.C1616Xf;
import logic.res.code.C5663aRz;
import p001a.C0064Am;

/* renamed from: a.aDu  reason: case insensitive filesystem */
public class C5294aDu extends C2421fH {
    @C0064Am(aul = "fbdcea3959fbef1dc8424af2944e5958", aum = 0)
    public C3438ra<AmplifierType> cRp;
    @C0064Am(aul = "fbdcea3959fbef1dc8424af2944e5958", aum = -2)
    public long fvJ;
    @C0064Am(aul = "fbdcea3959fbef1dc8424af2944e5958", aum = -1)
    public byte fvK;

    public C5294aDu() {
    }

    public C5294aDu(C1616Xf xf) {
        super(xf);
    }

    /* renamed from: c */
    public C5663aRz[] mo309c() {
        return ComposedAmplifierType._m_fields;
    }

    /* renamed from: d */
    public void mo310d() {
        super.mo310d();
        this.cRp = null;
        this.fvJ = 0;
        this.fvK = 0;
    }
}
