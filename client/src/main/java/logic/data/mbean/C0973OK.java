package logic.data.mbean;

import game.script.resource.Asset;
import game.script.ship.CruiseSpeed;
import game.script.ship.CruiseSpeedAdapter;
import game.script.ship.Ship;
import game.script.util.AdapterLikedList;
import logic.baa.C1616Xf;
import logic.res.code.C5663aRz;
import p001a.C0064Am;
import p001a.C6809auB;

/* renamed from: a.OK */
public class C0973OK extends C3805vD {
    @C0064Am(aul = "67e4250ce1c54b1b6c3e3288b736c014", aum = 9)
    public Ship aOO;
    @C0064Am(aul = "a931f9b86484384308beeb65c81a94c5", aum = 8)
    public AdapterLikedList<CruiseSpeedAdapter> aOw;
    @C0064Am(aul = "a931f9b86484384308beeb65c81a94c5", aum = -2)
    public long aUE;
    @C0064Am(aul = "a931f9b86484384308beeb65c81a94c5", aum = -1)
    public byte aUH;
    @C0064Am(aul = "f14842262b8609dc4b3e49b64a12a287", aum = 10)
    public C6809auB.C1996a aZI;
    @C0064Am(aul = "4d6683c976867ec01158de5fbb6e90f5", aum = -2)
    public long bIF;
    @C0064Am(aul = "4d6683c976867ec01158de5fbb6e90f5", aum = -1)
    public byte bIR;
    @C0064Am(aul = "4d6683c976867ec01158de5fbb6e90f5", aum = 6)
    public Asset bIu;
    @C0064Am(aul = "2e2ccdfa86f57eff33c8d732623d9fe5", aum = -1)
    public byte bhA;
    @C0064Am(aul = "63725efd38f3ce1ca5d7293ecf7a11f2", aum = 0)
    public float bhk;
    @C0064Am(aul = "d147e074b9f94c92df736c31f1c47782", aum = 2)
    public float bhl;
    @C0064Am(aul = "63725efd38f3ce1ca5d7293ecf7a11f2", aum = -2)
    public long bho;
    @C0064Am(aul = "da77fcd00e651d6a6e873a34348c7790", aum = -2)
    public long bhp;
    @C0064Am(aul = "d147e074b9f94c92df736c31f1c47782", aum = -2)
    public long bhq;
    @C0064Am(aul = "2e2ccdfa86f57eff33c8d732623d9fe5", aum = -2)
    public long bhr;
    @C0064Am(aul = "63725efd38f3ce1ca5d7293ecf7a11f2", aum = -1)
    public byte bhx;
    @C0064Am(aul = "da77fcd00e651d6a6e873a34348c7790", aum = -1)
    public byte bhy;
    @C0064Am(aul = "d147e074b9f94c92df736c31f1c47782", aum = -1)
    public byte bhz;
    @C0064Am(aul = "cde325ca66891d7cec8abca06d4b3ce6", aum = -2)
    public long cAK;
    @C0064Am(aul = "410a07f095a826d99c9ebad79bff1d03", aum = -2)
    public long cAL;
    @C0064Am(aul = "4ce36acb37c7587ae74fe699b9176371", aum = -2)
    public long cAM;
    @C0064Am(aul = "1b3981b40b29780f471967de0a94e28f", aum = -2)
    public long cAN;
    @C0064Am(aul = "cde325ca66891d7cec8abca06d4b3ce6", aum = -1)
    public byte cAY;
    @C0064Am(aul = "410a07f095a826d99c9ebad79bff1d03", aum = -1)
    public byte cAZ;
    @C0064Am(aul = "4ce36acb37c7587ae74fe699b9176371", aum = -1)
    public byte cBa;
    @C0064Am(aul = "1b3981b40b29780f471967de0a94e28f", aum = -1)
    public byte cBb;
    @C0064Am(aul = "67e4250ce1c54b1b6c3e3288b736c014", aum = -2)
    public long cSU;
    @C0064Am(aul = "67e4250ce1c54b1b6c3e3288b736c014", aum = -1)
    public byte cSW;
    @C0064Am(aul = "a915d5c11ac322c96038b12c63b3b0f6", aum = 4)
    public float dGt;
    @C0064Am(aul = "087dc22806623789b3ac097158849077", aum = 5)
    public float dGu;
    @C0064Am(aul = "3b39c4adb33740ec91e373e8d3daab03", aum = 7)
    public Asset dGw;
    @C0064Am(aul = "a915d5c11ac322c96038b12c63b3b0f6", aum = -1)
    public byte dOA;
    @C0064Am(aul = "087dc22806623789b3ac097158849077", aum = -1)
    public byte dOB;
    @C0064Am(aul = "3b39c4adb33740ec91e373e8d3daab03", aum = -1)
    public byte dOC;
    @C0064Am(aul = "18a726e0f96018399dedabd4342c8b1b", aum = -1)
    public byte dOD;
    @C0064Am(aul = "8babc8e0d40d441f8bdbbdbe8736c58e", aum = -1)
    public byte dOE;
    @C0064Am(aul = "e6c0e950048aa847547969094e873871", aum = -1)
    public byte dOF;
    @C0064Am(aul = "9aaaa0648e9ef89134f53486f06b4070", aum = -1)
    public byte dOG;
    @C0064Am(aul = "168a3cc04dfa5cfe5d35b5c8e7e34934", aum = -1)
    public byte dOH;
    @C0064Am(aul = "18a726e0f96018399dedabd4342c8b1b", aum = 11)
    public CruiseSpeed.CruiseSpeedEnterer dOj;
    @C0064Am(aul = "8babc8e0d40d441f8bdbbdbe8736c58e", aum = 12)
    public CruiseSpeed.CruiseSpeedExiter dOk;
    @C0064Am(aul = "410a07f095a826d99c9ebad79bff1d03", aum = 13)
    public float dOl;
    @C0064Am(aul = "cde325ca66891d7cec8abca06d4b3ce6", aum = 14)
    public float dOm;
    @C0064Am(aul = "4ce36acb37c7587ae74fe699b9176371", aum = 15)
    public float dOn;
    @C0064Am(aul = "1b3981b40b29780f471967de0a94e28f", aum = 16)
    public float dOo;
    @C0064Am(aul = "e6c0e950048aa847547969094e873871", aum = 17)
    public float dOp;
    @C0064Am(aul = "9aaaa0648e9ef89134f53486f06b4070", aum = 18)
    public float dOq;
    @C0064Am(aul = "168a3cc04dfa5cfe5d35b5c8e7e34934", aum = 19)
    public boolean dOr;
    @C0064Am(aul = "a915d5c11ac322c96038b12c63b3b0f6", aum = -2)
    public long dOs;
    @C0064Am(aul = "087dc22806623789b3ac097158849077", aum = -2)
    public long dOt;
    @C0064Am(aul = "3b39c4adb33740ec91e373e8d3daab03", aum = -2)
    public long dOu;
    @C0064Am(aul = "18a726e0f96018399dedabd4342c8b1b", aum = -2)
    public long dOv;
    @C0064Am(aul = "8babc8e0d40d441f8bdbbdbe8736c58e", aum = -2)
    public long dOw;
    @C0064Am(aul = "e6c0e950048aa847547969094e873871", aum = -2)
    public long dOx;
    @C0064Am(aul = "9aaaa0648e9ef89134f53486f06b4070", aum = -2)
    public long dOy;
    @C0064Am(aul = "168a3cc04dfa5cfe5d35b5c8e7e34934", aum = -2)
    public long dOz;
    @C0064Am(aul = "da77fcd00e651d6a6e873a34348c7790", aum = 1)

    /* renamed from: dW */
    public float f1289dW;
    @C0064Am(aul = "2e2ccdfa86f57eff33c8d732623d9fe5", aum = 3)

    /* renamed from: dX */
    public float f1290dX;
    @C0064Am(aul = "f14842262b8609dc4b3e49b64a12a287", aum = -2)

    /* renamed from: ol */
    public long f1291ol;
    @C0064Am(aul = "f14842262b8609dc4b3e49b64a12a287", aum = -1)

    /* renamed from: oo */
    public byte f1292oo;

    public C0973OK() {
    }

    public C0973OK(C1616Xf xf) {
        super(xf);
    }

    /* renamed from: c */
    public C5663aRz[] mo309c() {
        return CruiseSpeed._m_fields;
    }

    /* renamed from: d */
    public void mo310d() {
        super.mo310d();
        this.bhk = 0.0f;
        this.f1289dW = 0.0f;
        this.bhl = 0.0f;
        this.f1290dX = 0.0f;
        this.dGt = 0.0f;
        this.dGu = 0.0f;
        this.bIu = null;
        this.dGw = null;
        this.aOw = null;
        this.aOO = null;
        this.aZI = null;
        this.dOj = null;
        this.dOk = null;
        this.dOl = 0.0f;
        this.dOm = 0.0f;
        this.dOn = 0.0f;
        this.dOo = 0.0f;
        this.dOp = 0.0f;
        this.dOq = 0.0f;
        this.dOr = false;
        this.bho = 0;
        this.bhp = 0;
        this.bhq = 0;
        this.bhr = 0;
        this.dOs = 0;
        this.dOt = 0;
        this.bIF = 0;
        this.dOu = 0;
        this.aUE = 0;
        this.cSU = 0;
        this.f1291ol = 0;
        this.dOv = 0;
        this.dOw = 0;
        this.cAL = 0;
        this.cAK = 0;
        this.cAM = 0;
        this.cAN = 0;
        this.dOx = 0;
        this.dOy = 0;
        this.dOz = 0;
        this.bhx = 0;
        this.bhy = 0;
        this.bhz = 0;
        this.bhA = 0;
        this.dOA = 0;
        this.dOB = 0;
        this.bIR = 0;
        this.dOC = 0;
        this.aUH = 0;
        this.cSW = 0;
        this.f1292oo = 0;
        this.dOD = 0;
        this.dOE = 0;
        this.cAZ = 0;
        this.cAY = 0;
        this.cBa = 0;
        this.cBb = 0;
        this.dOF = 0;
        this.dOG = 0;
        this.dOH = 0;
    }
}
