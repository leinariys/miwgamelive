package logic.data.mbean;

import game.script.missiontemplate.scripting.BuyOutpostScriptTemplate;
import game.script.ship.OutpostType;
import logic.baa.C1616Xf;
import logic.res.code.C5663aRz;
import p001a.C0064Am;

/* renamed from: a.wN */
public class C3891wN extends C0597IR {
    @C0064Am(aul = "0dc5789f7f333cfe724ba3281edc2e31", aum = 0)
    public OutpostType bEQ;
    @C0064Am(aul = "940b13a74ace4aed0077169a14bc06ac", aum = 1)
    public String bER;
    @C0064Am(aul = "0dc5789f7f333cfe724ba3281edc2e31", aum = -2)
    public long bES;
    @C0064Am(aul = "940b13a74ace4aed0077169a14bc06ac", aum = -2)
    public long bET;
    @C0064Am(aul = "0dc5789f7f333cfe724ba3281edc2e31", aum = -1)
    public byte bEU;
    @C0064Am(aul = "940b13a74ace4aed0077169a14bc06ac", aum = -1)
    public byte bEV;

    public C3891wN() {
    }

    public C3891wN(C1616Xf xf) {
        super(xf);
    }

    /* renamed from: c */
    public C5663aRz[] mo309c() {
        return BuyOutpostScriptTemplate._m_fields;
    }

    /* renamed from: d */
    public void mo310d() {
        super.mo310d();
        this.bEQ = null;
        this.bER = null;
        this.bES = 0;
        this.bET = 0;
        this.bEU = 0;
        this.bEV = 0;
    }
}
