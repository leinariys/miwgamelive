package logic.data.mbean;

import game.script.item.buff.amplifier.ShieldAmplifier;
import logic.baa.C1616Xf;
import logic.res.code.C5663aRz;
import p001a.C0064Am;

/* renamed from: a.akv  reason: case insensitive filesystem */
public class C6335akv extends C3119nz {
    @C0064Am(aul = "b5464af19d445048fb4a5821d0d6f9c8", aum = -2)

    /* renamed from: dl */
    public long f4812dl;
    @C0064Am(aul = "b5464af19d445048fb4a5821d0d6f9c8", aum = -1)

    /* renamed from: ds */
    public byte f4813ds;
    @C0064Am(aul = "b5464af19d445048fb4a5821d0d6f9c8", aum = 0)
    public ShieldAmplifier fTC;

    public C6335akv() {
    }

    public C6335akv(C1616Xf xf) {
        super(xf);
    }

    /* renamed from: c */
    public C5663aRz[] mo309c() {
        return ShieldAmplifier.ShieldAmplifierAdapter._m_fields;
    }

    /* renamed from: d */
    public void mo310d() {
        super.mo310d();
        this.fTC = null;
        this.f4812dl = 0;
        this.f4813ds = 0;
    }
}
