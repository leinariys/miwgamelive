package logic.data.mbean;

import game.script.item.ShipItem;
import game.script.ship.Ship;
import logic.baa.C1616Xf;
import logic.res.code.C5663aRz;
import p001a.C0064Am;

/* renamed from: a.Ex */
public class C0371Ex extends C0887Ms {
    @C0064Am(aul = "54f1370fe99eb1584eebcc9c64db9673", aum = 1)
    public Ship aOO;
    @C0064Am(aul = "c76f76c288a0fdd7195aa544502c705f", aum = 0)
    public boolean cSS;
    @C0064Am(aul = "c76f76c288a0fdd7195aa544502c705f", aum = -2)
    public long cST;
    @C0064Am(aul = "54f1370fe99eb1584eebcc9c64db9673", aum = -2)
    public long cSU;
    @C0064Am(aul = "c76f76c288a0fdd7195aa544502c705f", aum = -1)
    public byte cSV;
    @C0064Am(aul = "54f1370fe99eb1584eebcc9c64db9673", aum = -1)
    public byte cSW;

    public C0371Ex() {
    }

    public C0371Ex(C1616Xf xf) {
        super(xf);
    }

    /* renamed from: c */
    public C5663aRz[] mo309c() {
        return ShipItem._m_fields;
    }

    /* renamed from: d */
    public void mo310d() {
        super.mo310d();
        this.cSS = false;
        this.aOO = null;
        this.cST = 0;
        this.cSU = 0;
        this.cSV = 0;
        this.cSW = 0;
    }
}
