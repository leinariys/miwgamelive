package logic.data.mbean;

import game.script.citizenship.inprovements.WageImprovement;
import logic.baa.C1616Xf;
import logic.res.code.C5663aRz;
import p001a.C0064Am;

/* renamed from: a.Tb */
public class C1343Tb extends C2057bg {
    @C0064Am(aul = "049d5ba5b3b7d819fe44fcef81a33f00", aum = 0)
    public long egO;
    @C0064Am(aul = "a4305fb87549dd085be87f50de14591a", aum = 1)
    public float egP;
    @C0064Am(aul = "19d8e7f1c8af579c9b78153d4c11943d", aum = 2)
    public TaskletImpl egQ;
    @C0064Am(aul = "1c308d2f46b2e85f79bb677cf69f5a7d", aum = 3)
    public long egR;
    @C0064Am(aul = "049d5ba5b3b7d819fe44fcef81a33f00", aum = -2)
    public long egS;
    @C0064Am(aul = "a4305fb87549dd085be87f50de14591a", aum = -2)
    public long egT;
    @C0064Am(aul = "19d8e7f1c8af579c9b78153d4c11943d", aum = -2)
    public long egU;
    @C0064Am(aul = "1c308d2f46b2e85f79bb677cf69f5a7d", aum = -2)
    public long egV;
    @C0064Am(aul = "049d5ba5b3b7d819fe44fcef81a33f00", aum = -1)
    public byte egW;
    @C0064Am(aul = "a4305fb87549dd085be87f50de14591a", aum = -1)
    public byte egX;
    @C0064Am(aul = "19d8e7f1c8af579c9b78153d4c11943d", aum = -1)
    public byte egY;
    @C0064Am(aul = "1c308d2f46b2e85f79bb677cf69f5a7d", aum = -1)
    public byte egZ;

    public C1343Tb() {
    }

    public C1343Tb(C1616Xf xf) {
        super(xf);
    }

    /* renamed from: c */
    public C5663aRz[] mo309c() {
        return WageImprovement._m_fields;
    }

    /* renamed from: d */
    public void mo310d() {
        super.mo310d();
        this.egO = 0;
        this.egP = 0.0f;
        this.egQ = null;
        this.egR = 0;
        this.egS = 0;
        this.egT = 0;
        this.egU = 0;
        this.egV = 0;
        this.egW = 0;
        this.egX = 0;
        this.egY = 0;
        this.egZ = 0;
    }
}
