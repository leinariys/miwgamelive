package logic.data.mbean;

import game.script.item.buff.module.WeaponBuffType;
import logic.baa.C1616Xf;
import logic.res.code.C5663aRz;
import p001a.C0064Am;
import p001a.C3892wO;

/* renamed from: a.aeT  reason: case insensitive filesystem */
public class C5995aeT extends C5661aRx {
    @C0064Am(aul = "415807d41691860180a5712a4e53708d", aum = -2)
    public long aIR;
    @C0064Am(aul = "415807d41691860180a5712a4e53708d", aum = -1)
    public byte aJc;
    @C0064Am(aul = "ba4f06dbbbea7cd8dc351667bb1132eb", aum = 0)
    public C3892wO bEb;
    @C0064Am(aul = "415807d41691860180a5712a4e53708d", aum = 1)
    public C3892wO bEd;
    @C0064Am(aul = "3c2053a9492e443ed9f9b5f7663c4ea9", aum = 2)
    public C3892wO bEe;
    @C0064Am(aul = "89ab15270fd8409323174217ca7b7487", aum = 3)
    public C3892wO bEf;
    @C0064Am(aul = "adc7632be2369f83733d34ee1d1d687a", aum = 4)
    public C3892wO bEh;
    @C0064Am(aul = "e04471dbbb2aea8498160acbfaa73350", aum = 5)
    public boolean bEj;
    @C0064Am(aul = "a1fd0f3cc2f3561e7a2a78bfc47023f3", aum = 6)
    public boolean bEl;
    @C0064Am(aul = "e04471dbbb2aea8498160acbfaa73350", aum = -2)
    public long bIG;
    @C0064Am(aul = "ba4f06dbbbea7cd8dc351667bb1132eb", aum = -2)
    public long bIH;
    @C0064Am(aul = "3c2053a9492e443ed9f9b5f7663c4ea9", aum = -2)
    public long bIJ;
    @C0064Am(aul = "e04471dbbb2aea8498160acbfaa73350", aum = -1)
    public byte bIS;
    @C0064Am(aul = "ba4f06dbbbea7cd8dc351667bb1132eb", aum = -1)
    public byte bIT;
    @C0064Am(aul = "3c2053a9492e443ed9f9b5f7663c4ea9", aum = -1)
    public byte bIV;
    @C0064Am(aul = "a1fd0f3cc2f3561e7a2a78bfc47023f3", aum = -2)
    public long cUh;
    @C0064Am(aul = "a1fd0f3cc2f3561e7a2a78bfc47023f3", aum = -1)
    public byte cUj;
    @C0064Am(aul = "89ab15270fd8409323174217ca7b7487", aum = -1)

    /* renamed from: tO */
    public byte f4392tO;
    @C0064Am(aul = "adc7632be2369f83733d34ee1d1d687a", aum = -1)

    /* renamed from: tS */
    public byte f4393tS;
    @C0064Am(aul = "89ab15270fd8409323174217ca7b7487", aum = -2)

    /* renamed from: tl */
    public long f4394tl;
    @C0064Am(aul = "adc7632be2369f83733d34ee1d1d687a", aum = -2)

    /* renamed from: tp */
    public long f4395tp;

    public C5995aeT() {
    }

    public C5995aeT(C1616Xf xf) {
        super(xf);
    }

    /* renamed from: c */
    public C5663aRz[] mo309c() {
        return WeaponBuffType._m_fields;
    }

    /* renamed from: d */
    public void mo310d() {
        super.mo310d();
        this.bEb = null;
        this.bEd = null;
        this.bEe = null;
        this.bEf = null;
        this.bEh = null;
        this.bEj = false;
        this.bEl = false;
        this.bIH = 0;
        this.aIR = 0;
        this.bIJ = 0;
        this.f4394tl = 0;
        this.f4395tp = 0;
        this.bIG = 0;
        this.cUh = 0;
        this.bIT = 0;
        this.aJc = 0;
        this.bIV = 0;
        this.f4392tO = 0;
        this.f4393tS = 0;
        this.bIS = 0;
        this.cUj = 0;
    }
}
