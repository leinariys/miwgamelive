package logic.data.mbean;

import game.script.mission.MissionTemplate;
import game.script.npcchat.actions.OpenMissionBriefingSpeechAction;
import logic.baa.C1616Xf;
import logic.res.code.C5663aRz;
import p001a.C0064Am;

/* renamed from: a.rw */
public class C3523rw extends C6352alM {
    @C0064Am(aul = "60b06195413edd46a490786b1c7f61a4", aum = 0)
    public MissionTemplate baM;
    @C0064Am(aul = "60b06195413edd46a490786b1c7f61a4", aum = -2)
    public long baN;
    @C0064Am(aul = "60b06195413edd46a490786b1c7f61a4", aum = -1)
    public byte baO;

    public C3523rw() {
    }

    public C3523rw(C1616Xf xf) {
        super(xf);
    }

    /* renamed from: c */
    public C5663aRz[] mo309c() {
        return OpenMissionBriefingSpeechAction._m_fields;
    }

    /* renamed from: d */
    public void mo310d() {
        super.mo310d();
        this.baM = null;
        this.baN = 0;
        this.baO = 0;
    }
}
