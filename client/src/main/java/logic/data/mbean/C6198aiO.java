package logic.data.mbean;

import game.network.message.serializable.C3438ra;
import game.script.ai.npc.AIControllerType;
import game.script.faction.Faction;
import game.script.itemgen.ItemGenTable;
import game.script.npc.NPCType;
import game.script.npcchat.NPCChat;
import game.script.pda.DatabaseCategory;
import game.script.pda.TaikopediaEntry;
import game.script.progression.ProgressionCharacterTemplate;
import game.script.resource.Asset;
import game.script.ship.EquippedShipType;
import logic.baa.C1616Xf;
import logic.res.code.C5663aRz;
import p001a.C0064Am;
import p001a.C6902avq;
import taikodom.infra.script.I18NString;

/* renamed from: a.aiO  reason: case insensitive filesystem */
public class C6198aiO extends C2047bX {
    @C0064Am(aul = "6d20861cd232ab8c38ee3e5bd8dd5a02", aum = 17)

    /* renamed from: BO */
    public int f4620BO;
    @C0064Am(aul = "cad09a4b9ad0fa4d616b2ce960d70db5", aum = -2)

    /* renamed from: Nf */
    public long f4621Nf;
    @C0064Am(aul = "cad09a4b9ad0fa4d616b2ce960d70db5", aum = -1)

    /* renamed from: Nh */
    public byte f4622Nh;
    @C0064Am(aul = "eb21c37020ac0b54a414a32546de14c5", aum = 11)
    public Faction axU;
    @C0064Am(aul = "6b785477f4df208c5592dbd1676115a8", aum = 0)
    public EquippedShipType ayQ;
    @C0064Am(aul = "49c602bf2b1b4d1a1637849cd91eacfd", aum = 1)
    public Class ayR;
    @C0064Am(aul = "0c54c8fa32147d1fab3189c5974676f5", aum = 2)
    public AIControllerType ayS;
    @C0064Am(aul = "e64ac19c66c82ade4fae5cc1ae8e0262", aum = 4)
    public String ayT;
    @C0064Am(aul = "56fbba9e19d329bdbcd902b4a1a32486", aum = 5)
    public NPCChat ayU;
    @C0064Am(aul = "91733e44e95579a982326895d992f2ea", aum = 6)
    public C6902avq ayV;
    @C0064Am(aul = "f67a1de235b3e04d7a3e8517548191a2", aum = 7)
    public ProgressionCharacterTemplate ayW;
    @C0064Am(aul = "8383be213fc90fde6aedba590f5689b1", aum = 8)
    public I18NString ayX;
    @C0064Am(aul = "c6bb25f8b93f247630ba607c7687d358", aum = 12)
    public boolean ayY;
    @C0064Am(aul = "996184ac8f9800bba1767379bf8ca31a", aum = 13)
    public C3438ra<ItemGenTable> ayZ;
    @C0064Am(aul = "eb21c37020ac0b54a414a32546de14c5", aum = -2)
    public long ayi;
    @C0064Am(aul = "eb21c37020ac0b54a414a32546de14c5", aum = -1)
    public byte ayx;
    @C0064Am(aul = "8383be213fc90fde6aedba590f5689b1", aum = -1)
    public byte azA;
    @C0064Am(aul = "c6bb25f8b93f247630ba607c7687d358", aum = -1)
    public byte azB;
    @C0064Am(aul = "996184ac8f9800bba1767379bf8ca31a", aum = -1)
    public byte azC;
    @C0064Am(aul = "2e9e5dfdb053a4bd6ce3596cfb1d2b57", aum = -1)
    public byte azD;
    @C0064Am(aul = "6d20861cd232ab8c38ee3e5bd8dd5a02", aum = -1)
    public byte azE;
    @C0064Am(aul = "2e9e5dfdb053a4bd6ce3596cfb1d2b57", aum = 14)
    public long aza;
    @C0064Am(aul = "6b785477f4df208c5592dbd1676115a8", aum = -2)
    public long aze;
    @C0064Am(aul = "49c602bf2b1b4d1a1637849cd91eacfd", aum = -2)
    public long azf;
    @C0064Am(aul = "0c54c8fa32147d1fab3189c5974676f5", aum = -2)
    public long azg;
    @C0064Am(aul = "e64ac19c66c82ade4fae5cc1ae8e0262", aum = -2)
    public long azh;
    @C0064Am(aul = "56fbba9e19d329bdbcd902b4a1a32486", aum = -2)
    public long azi;
    @C0064Am(aul = "91733e44e95579a982326895d992f2ea", aum = -2)
    public long azj;
    @C0064Am(aul = "f67a1de235b3e04d7a3e8517548191a2", aum = -2)
    public long azk;
    @C0064Am(aul = "8383be213fc90fde6aedba590f5689b1", aum = -2)
    public long azl;
    @C0064Am(aul = "c6bb25f8b93f247630ba607c7687d358", aum = -2)
    public long azm;
    @C0064Am(aul = "996184ac8f9800bba1767379bf8ca31a", aum = -2)
    public long azn;
    @C0064Am(aul = "2e9e5dfdb053a4bd6ce3596cfb1d2b57", aum = -2)
    public long azo;
    @C0064Am(aul = "6d20861cd232ab8c38ee3e5bd8dd5a02", aum = -2)
    public long azp;
    @C0064Am(aul = "6b785477f4df208c5592dbd1676115a8", aum = -1)
    public byte azt;
    @C0064Am(aul = "49c602bf2b1b4d1a1637849cd91eacfd", aum = -1)
    public byte azu;
    @C0064Am(aul = "0c54c8fa32147d1fab3189c5974676f5", aum = -1)
    public byte azv;
    @C0064Am(aul = "e64ac19c66c82ade4fae5cc1ae8e0262", aum = -1)
    public byte azw;
    @C0064Am(aul = "56fbba9e19d329bdbcd902b4a1a32486", aum = -1)
    public byte azx;
    @C0064Am(aul = "91733e44e95579a982326895d992f2ea", aum = -1)
    public byte azy;
    @C0064Am(aul = "f67a1de235b3e04d7a3e8517548191a2", aum = -1)
    public byte azz;
    @C0064Am(aul = "22b3edcefdb4cd51406cc13532431a04", aum = 15)

    /* renamed from: jS */
    public DatabaseCategory f4623jS;
    @C0064Am(aul = "cdc819695afb974eabef82a6717cf44f", aum = 10)

    /* renamed from: jT */
    public C3438ra<TaikopediaEntry> f4624jT;
    @C0064Am(aul = "13b365c30fddeae5241d4086305af676", aum = 16)

    /* renamed from: jU */
    public Asset f4625jU;
    @C0064Am(aul = "4fdf297d96bf73c60e9e11faa96d667c", aum = 18)

    /* renamed from: jV */
    public float f4626jV;
    @C0064Am(aul = "22b3edcefdb4cd51406cc13532431a04", aum = -2)

    /* renamed from: jX */
    public long f4627jX;
    @C0064Am(aul = "cdc819695afb974eabef82a6717cf44f", aum = -2)

    /* renamed from: jY */
    public long f4628jY;
    @C0064Am(aul = "13b365c30fddeae5241d4086305af676", aum = -2)

    /* renamed from: jZ */
    public long f4629jZ;
    @C0064Am(aul = "4fdf297d96bf73c60e9e11faa96d667c", aum = -2)

    /* renamed from: ka */
    public long f4630ka;
    @C0064Am(aul = "22b3edcefdb4cd51406cc13532431a04", aum = -1)

    /* renamed from: kc */
    public byte f4631kc;
    @C0064Am(aul = "cdc819695afb974eabef82a6717cf44f", aum = -1)

    /* renamed from: kd */
    public byte f4632kd;
    @C0064Am(aul = "13b365c30fddeae5241d4086305af676", aum = -1)

    /* renamed from: ke */
    public byte f4633ke;
    @C0064Am(aul = "4fdf297d96bf73c60e9e11faa96d667c", aum = -1)

    /* renamed from: kf */
    public byte f4634kf;
    @C0064Am(aul = "96559a84d548bea4bfb8e0ad02298690", aum = 9)

    /* renamed from: nh */
    public I18NString f4635nh;
    @C0064Am(aul = "96559a84d548bea4bfb8e0ad02298690", aum = -2)

    /* renamed from: nk */
    public long f4636nk;
    @C0064Am(aul = "96559a84d548bea4bfb8e0ad02298690", aum = -1)

    /* renamed from: nn */
    public byte f4637nn;
    @C0064Am(aul = "cad09a4b9ad0fa4d616b2ce960d70db5", aum = 3)

    /* renamed from: zP */
    public I18NString f4638zP;

    public C6198aiO() {
    }

    public C6198aiO(C1616Xf xf) {
        super(xf);
    }

    /* renamed from: c */
    public C5663aRz[] mo309c() {
        return NPCType._m_fields;
    }

    /* renamed from: d */
    public void mo310d() {
        super.mo310d();
        this.ayQ = null;
        this.ayR = null;
        this.ayS = null;
        this.f4638zP = null;
        this.ayT = null;
        this.ayU = null;
        this.ayV = null;
        this.ayW = null;
        this.ayX = null;
        this.f4635nh = null;
        this.f4624jT = null;
        this.axU = null;
        this.ayY = false;
        this.ayZ = null;
        this.aza = 0;
        this.f4623jS = null;
        this.f4625jU = null;
        this.f4620BO = 0;
        this.f4626jV = 0.0f;
        this.aze = 0;
        this.azf = 0;
        this.azg = 0;
        this.f4621Nf = 0;
        this.azh = 0;
        this.azi = 0;
        this.azj = 0;
        this.azk = 0;
        this.azl = 0;
        this.f4636nk = 0;
        this.f4628jY = 0;
        this.ayi = 0;
        this.azm = 0;
        this.azn = 0;
        this.azo = 0;
        this.f4627jX = 0;
        this.f4629jZ = 0;
        this.azp = 0;
        this.f4630ka = 0;
        this.azt = 0;
        this.azu = 0;
        this.azv = 0;
        this.f4622Nh = 0;
        this.azw = 0;
        this.azx = 0;
        this.azy = 0;
        this.azz = 0;
        this.azA = 0;
        this.f4637nn = 0;
        this.f4632kd = 0;
        this.ayx = 0;
        this.azB = 0;
        this.azC = 0;
        this.azD = 0;
        this.f4631kc = 0;
        this.f4633ke = 0;
        this.azE = 0;
        this.f4634kf = 0;
    }
}
