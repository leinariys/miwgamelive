package logic.data.mbean;

import game.network.message.serializable.C3438ra;
import game.script.missiontemplate.PositionDat;
import game.script.missiontemplate.scripting.ReconNpcMissionScriptTemplate;
import game.script.npc.NPCType;
import game.script.space.StellarSystem;
import logic.baa.C1616Xf;
import logic.res.code.C5663aRz;
import p001a.C0064Am;

/* renamed from: a.nQ */
public class C3047nQ extends C0597IR {
    @C0064Am(aul = "3743458f0e6d1b6400a16dc5c8ba6299", aum = 1)
    public int aLC;
    @C0064Am(aul = "91b7df2e1895a12cee2346120e2a789c", aum = 2)
    public C3438ra<NPCType> aLD;
    @C0064Am(aul = "9cf6089c263afb2437a2ea7a29be79cc", aum = 3)
    public boolean aLE;
    @C0064Am(aul = "ec4ab5f26e3a4a57ba145cad9cd987e8", aum = 5)
    public PositionDat aLF;
    @C0064Am(aul = "2ac3c230010e3524b0c3247e4b5d0489", aum = -2)
    public long aLG;
    @C0064Am(aul = "3743458f0e6d1b6400a16dc5c8ba6299", aum = -2)
    public long aLH;
    @C0064Am(aul = "91b7df2e1895a12cee2346120e2a789c", aum = -2)
    public long aLI;
    @C0064Am(aul = "9cf6089c263afb2437a2ea7a29be79cc", aum = -2)
    public long aLJ;
    @C0064Am(aul = "ec4ab5f26e3a4a57ba145cad9cd987e8", aum = -2)
    public long aLK;
    @C0064Am(aul = "2ac3c230010e3524b0c3247e4b5d0489", aum = -1)
    public byte aLL;
    @C0064Am(aul = "3743458f0e6d1b6400a16dc5c8ba6299", aum = -1)
    public byte aLM;
    @C0064Am(aul = "91b7df2e1895a12cee2346120e2a789c", aum = -1)
    public byte aLN;
    @C0064Am(aul = "9cf6089c263afb2437a2ea7a29be79cc", aum = -1)
    public byte aLO;
    @C0064Am(aul = "ec4ab5f26e3a4a57ba145cad9cd987e8", aum = -1)
    public byte aLP;
    @C0064Am(aul = "2ac3c230010e3524b0c3247e4b5d0489", aum = 0)

    /* renamed from: cy */
    public int f8711cy;
    @C0064Am(aul = "eb936652800fdb7247cc2700a4a2d3c2", aum = 4)

    /* renamed from: rI */
    public StellarSystem f8712rI;
    @C0064Am(aul = "eb936652800fdb7247cc2700a4a2d3c2", aum = -2)

    /* renamed from: rY */
    public long f8713rY;
    @C0064Am(aul = "eb936652800fdb7247cc2700a4a2d3c2", aum = -1)

    /* renamed from: so */
    public byte f8714so;

    public C3047nQ() {
    }

    public C3047nQ(C1616Xf xf) {
        super(xf);
    }

    /* renamed from: c */
    public C5663aRz[] mo309c() {
        return ReconNpcMissionScriptTemplate._m_fields;
    }

    /* renamed from: d */
    public void mo310d() {
        super.mo310d();
        this.f8711cy = 0;
        this.aLC = 0;
        this.aLD = null;
        this.aLE = false;
        this.f8712rI = null;
        this.aLF = null;
        this.aLG = 0;
        this.aLH = 0;
        this.aLI = 0;
        this.aLJ = 0;
        this.f8713rY = 0;
        this.aLK = 0;
        this.aLL = 0;
        this.aLM = 0;
        this.aLN = 0;
        this.aLO = 0;
        this.f8714so = 0;
        this.aLP = 0;
    }
}
