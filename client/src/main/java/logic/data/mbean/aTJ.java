package logic.data.mbean;

import game.network.message.serializable.C3438ra;
import game.script.Character;
import game.script.progression.*;
import logic.baa.C1616Xf;
import logic.res.code.C5663aRz;
import p001a.C0064Am;

/* renamed from: a.aTJ */
public class aTJ extends C3805vD {
    @C0064Am(aul = "2c62be179e7b3c4a748a72fded623fef", aum = 1)

    /* renamed from: BU */
    public ProgressionCareer f3793BU;
    @C0064Am(aul = "a2597cf387a8903fb213c5aa9c53c7cf", aum = 0)
    public Character aKN;
    @C0064Am(aul = "098a76c15e902e20bbf995c6af8378bd", aum = 2)
    public CharacterEvolution aKO;
    @C0064Am(aul = "4770e2dcecc74c6db288321681dc24cc", aum = 3)
    public C3438ra<ProgressionLine> aKQ;
    @C0064Am(aul = "5a1c61db4888f8027e899cd59c857f08", aum = 4)
    public ProgressionAmplifiers aKS;
    @C0064Am(aul = "2c62be179e7b3c4a748a72fded623fef", aum = -2)
    public long cye;
    @C0064Am(aul = "2c62be179e7b3c4a748a72fded623fef", aum = -1)
    public byte cyg;
    @C0064Am(aul = "098a76c15e902e20bbf995c6af8378bd", aum = -2)
    public long iSG;
    @C0064Am(aul = "4770e2dcecc74c6db288321681dc24cc", aum = -2)
    public long iSH;
    @C0064Am(aul = "5a1c61db4888f8027e899cd59c857f08", aum = -2)
    public long iSI;
    @C0064Am(aul = "098a76c15e902e20bbf995c6af8378bd", aum = -1)
    public byte iSJ;
    @C0064Am(aul = "4770e2dcecc74c6db288321681dc24cc", aum = -1)
    public byte iSK;
    @C0064Am(aul = "5a1c61db4888f8027e899cd59c857f08", aum = -1)
    public byte iSL;
    @C0064Am(aul = "a2597cf387a8903fb213c5aa9c53c7cf", aum = -2)

    /* renamed from: nm */
    public long f3794nm;
    @C0064Am(aul = "a2597cf387a8903fb213c5aa9c53c7cf", aum = -1)

    /* renamed from: np */
    public byte f3795np;

    public aTJ() {
    }

    public aTJ(C1616Xf xf) {
        super(xf);
    }

    /* renamed from: c */
    public C5663aRz[] mo309c() {
        return CharacterProgression._m_fields;
    }

    /* renamed from: d */
    public void mo310d() {
        super.mo310d();
        this.aKN = null;
        this.f3793BU = null;
        this.aKO = null;
        this.aKQ = null;
        this.aKS = null;
        this.f3794nm = 0;
        this.cye = 0;
        this.iSG = 0;
        this.iSH = 0;
        this.iSI = 0;
        this.f3795np = 0;
        this.cyg = 0;
        this.iSJ = 0;
        this.iSK = 0;
        this.iSL = 0;
    }
}
