package logic.data.mbean;

import game.script.citizenship.inprovements.HangarImprovement;
import logic.baa.C1616Xf;
import logic.res.code.C5663aRz;
import p001a.C0064Am;

/* renamed from: a.Sb */
public class C1259Sb extends C2057bg {
    @C0064Am(aul = "86aaa23cce06f6e3abf18b8271113036", aum = 0)

    /* renamed from: zI */
    public int f1546zI;
    @C0064Am(aul = "86aaa23cce06f6e3abf18b8271113036", aum = -2)

    /* renamed from: zJ */
    public long f1547zJ;
    @C0064Am(aul = "86aaa23cce06f6e3abf18b8271113036", aum = -1)

    /* renamed from: zK */
    public byte f1548zK;

    public C1259Sb() {
    }

    public C1259Sb(C1616Xf xf) {
        super(xf);
    }

    /* renamed from: c */
    public C5663aRz[] mo309c() {
        return HangarImprovement._m_fields;
    }

    /* renamed from: d */
    public void mo310d() {
        super.mo310d();
        this.f1546zI = 0;
        this.f1547zJ = 0;
        this.f1548zK = 0;
    }
}
