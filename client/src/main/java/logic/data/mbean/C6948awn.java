package logic.data.mbean;

import game.script.mission.MissionTemplate;
import game.script.npcchat.requirement.HasAccomplishMissionSpeechRequirement;
import logic.baa.C1616Xf;
import logic.res.code.C5663aRz;
import p001a.C0064Am;

/* renamed from: a.awn  reason: case insensitive filesystem */
public class C6948awn extends C5416aIm {
    @C0064Am(aul = "22d3722f8813669670c4877b06b2b5f0", aum = 0)
    public MissionTemplate baM;
    @C0064Am(aul = "22d3722f8813669670c4877b06b2b5f0", aum = -2)
    public long baN;
    @C0064Am(aul = "22d3722f8813669670c4877b06b2b5f0", aum = -1)
    public byte baO;

    public C6948awn() {
    }

    public C6948awn(C1616Xf xf) {
        super(xf);
    }

    /* renamed from: c */
    public C5663aRz[] mo309c() {
        return HasAccomplishMissionSpeechRequirement._m_fields;
    }

    /* renamed from: d */
    public void mo310d() {
        super.mo310d();
        this.baM = null;
        this.baN = 0;
        this.baO = 0;
    }
}
