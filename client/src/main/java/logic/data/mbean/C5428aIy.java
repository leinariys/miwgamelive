package logic.data.mbean;

import game.script.mission.scripting.PatrolMissionScript;
import logic.baa.C1616Xf;
import logic.res.code.C5663aRz;
import p001a.C0064Am;

/* renamed from: a.aIy  reason: case insensitive filesystem */
public class C5428aIy extends C2955mD {
    @C0064Am(aul = "64d0d83ef43160d06c28d7a20882621c", aum = 0)
    public PatrolMissionScript aPU;
    @C0064Am(aul = "64d0d83ef43160d06c28d7a20882621c", aum = -2)

    /* renamed from: dl */
    public long f3110dl;
    @C0064Am(aul = "64d0d83ef43160d06c28d7a20882621c", aum = -1)

    /* renamed from: ds */
    public byte f3111ds;

    public C5428aIy() {
    }

    public C5428aIy(C1616Xf xf) {
        super(xf);
    }

    /* renamed from: c */
    public C5663aRz[] mo309c() {
        return PatrolMissionScript.StateA._m_fields;
    }

    /* renamed from: d */
    public void mo310d() {
        super.mo310d();
        this.aPU = null;
        this.f3110dl = 0;
        this.f3111ds = 0;
    }
}
