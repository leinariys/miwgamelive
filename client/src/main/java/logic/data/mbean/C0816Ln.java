package logic.data.mbean;

import game.network.message.serializable.C3438ra;
import game.script.Character;
import logic.baa.C1616Xf;
import logic.res.code.C5663aRz;
import p001a.C0064Am;

/* renamed from: a.Ln */
public class C0816Ln extends C3805vD {
    @C0064Am(aul = "e423f0d6af3c32ab958c22a6f49904a9", aum = 0)
    public C3438ra<Character> cHu;
    @C0064Am(aul = "64ac6fb00b7927a4cadaf67fd38b9ecb", aum = 1)
    public Character cHw;
    @C0064Am(aul = "e423f0d6af3c32ab958c22a6f49904a9", aum = -2)
    public long duh;
    @C0064Am(aul = "64ac6fb00b7927a4cadaf67fd38b9ecb", aum = -2)
    public long dui;
    @C0064Am(aul = "e423f0d6af3c32ab958c22a6f49904a9", aum = -1)
    public byte duj;
    @C0064Am(aul = "64ac6fb00b7927a4cadaf67fd38b9ecb", aum = -1)
    public byte duk;

    public C0816Ln() {
    }

    public C0816Ln(C1616Xf xf) {
        super(xf);
    }

    /* renamed from: c */
    public C5663aRz[] mo309c() {
        return NPCParty._m_fields;
    }

    /* renamed from: d */
    public void mo310d() {
        super.mo310d();
        this.cHu = null;
        this.cHw = null;
        this.duh = 0;
        this.dui = 0;
        this.duj = 0;
        this.duk = 0;
    }
}
