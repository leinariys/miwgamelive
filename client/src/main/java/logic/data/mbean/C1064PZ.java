package logic.data.mbean;

import game.script.item.buff.module.ShieldBuffType;
import logic.baa.C1616Xf;
import logic.res.code.C5663aRz;
import p001a.C0064Am;
import p001a.C3892wO;

/* renamed from: a.PZ */
public class C1064PZ extends C5661aRx {
    @C0064Am(aul = "f58037ad73314b7a576adef74091cb3c", aum = 0)
    public C3892wO aBJ;
    @C0064Am(aul = "3f452d0d4aa002e89525b7a0dc233f3d", aum = 1)
    public C3892wO aBK;
    @C0064Am(aul = "697840596e289689f129908d275094ee", aum = 2)
    public C3892wO aBL;
    @C0064Am(aul = "15d0c2771675964ee08fbbbfbe0001b6", aum = 3)
    public C3892wO aBM;
    @C0064Am(aul = "f58037ad73314b7a576adef74091cb3c", aum = -2)
    public long aBO;
    @C0064Am(aul = "3f452d0d4aa002e89525b7a0dc233f3d", aum = -2)
    public long aBP;
    @C0064Am(aul = "697840596e289689f129908d275094ee", aum = -2)
    public long aBQ;
    @C0064Am(aul = "15d0c2771675964ee08fbbbfbe0001b6", aum = -2)
    public long aBR;
    @C0064Am(aul = "f58037ad73314b7a576adef74091cb3c", aum = -1)
    public byte aBT;
    @C0064Am(aul = "3f452d0d4aa002e89525b7a0dc233f3d", aum = -1)
    public byte aBU;
    @C0064Am(aul = "697840596e289689f129908d275094ee", aum = -1)
    public byte aBV;
    @C0064Am(aul = "15d0c2771675964ee08fbbbfbe0001b6", aum = -1)
    public byte aBW;

    public C1064PZ() {
    }

    public C1064PZ(C1616Xf xf) {
        super(xf);
    }

    /* renamed from: c */
    public C5663aRz[] mo309c() {
        return ShieldBuffType._m_fields;
    }

    /* renamed from: d */
    public void mo310d() {
        super.mo310d();
        this.aBJ = null;
        this.aBK = null;
        this.aBL = null;
        this.aBM = null;
        this.aBO = 0;
        this.aBP = 0;
        this.aBQ = 0;
        this.aBR = 0;
        this.aBT = 0;
        this.aBU = 0;
        this.aBV = 0;
        this.aBW = 0;
    }
}
