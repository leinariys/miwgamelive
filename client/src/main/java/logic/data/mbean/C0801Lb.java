package logic.data.mbean;

import game.script.storage.StorageUpdate;
import logic.baa.C1616Xf;
import logic.res.code.C5663aRz;
import p001a.C0064Am;

/* renamed from: a.Lb */
public class C0801Lb extends C5292aDs {
    @C0064Am(aul = "6cba1e3608dbc88cb02f71e5d02cfded", aum = 0)
    public float dsY;
    @C0064Am(aul = "71ad059edc3a705bb07422665e4a2070", aum = 1)
    public long dsZ;
    @C0064Am(aul = "6cba1e3608dbc88cb02f71e5d02cfded", aum = -2)
    public long dta;
    @C0064Am(aul = "71ad059edc3a705bb07422665e4a2070", aum = -2)
    public long dtb;
    @C0064Am(aul = "6cba1e3608dbc88cb02f71e5d02cfded", aum = -1)
    public byte dtc;
    @C0064Am(aul = "71ad059edc3a705bb07422665e4a2070", aum = -1)
    public byte dtd;

    public C0801Lb() {
    }

    public C0801Lb(C1616Xf xf) {
        super(xf);
    }

    /* renamed from: c */
    public C5663aRz[] mo309c() {
        return StorageUpdate._m_fields;
    }

    /* renamed from: d */
    public void mo310d() {
        super.mo310d();
        this.dsY = 0.0f;
        this.dsZ = 0;
        this.dta = 0;
        this.dtb = 0;
        this.dtc = 0;
        this.dtd = 0;
    }
}
