package logic.data.mbean;

import game.script.damage.DamageType;
import game.script.item.buff.module.DamagerType;
import logic.baa.C1616Xf;
import logic.res.code.C5663aRz;
import p001a.C0064Am;

/* renamed from: a.XA */
public class C1576XA extends C5661aRx {
    @C0064Am(aul = "5d0eb03ad1a1bf4e777567a3727b9ab5", aum = 0)
    public float eJl;
    @C0064Am(aul = "211da7ea8b59c239b74d366b204da661", aum = 1)
    public float eJm;
    @C0064Am(aul = "9c767c8c92453a213a2f5b298b7e73a2", aum = 2)
    public DamageType eJn;
    @C0064Am(aul = "be4de1b9fd63343c92355a8d453386ea", aum = 3)
    public DamageType eJo;
    @C0064Am(aul = "9c767c8c92453a213a2f5b298b7e73a2", aum = -2)
    public long eJp;
    @C0064Am(aul = "be4de1b9fd63343c92355a8d453386ea", aum = -2)
    public long eJq;
    @C0064Am(aul = "9c767c8c92453a213a2f5b298b7e73a2", aum = -1)
    public byte eJr;
    @C0064Am(aul = "be4de1b9fd63343c92355a8d453386ea", aum = -1)
    public byte eJs;
    @C0064Am(aul = "5d0eb03ad1a1bf4e777567a3727b9ab5", aum = -1)

    /* renamed from: tO */
    public byte f2095tO;
    @C0064Am(aul = "211da7ea8b59c239b74d366b204da661", aum = -1)

    /* renamed from: tS */
    public byte f2096tS;
    @C0064Am(aul = "5d0eb03ad1a1bf4e777567a3727b9ab5", aum = -2)

    /* renamed from: tl */
    public long f2097tl;
    @C0064Am(aul = "211da7ea8b59c239b74d366b204da661", aum = -2)

    /* renamed from: tp */
    public long f2098tp;

    public C1576XA() {
    }

    public C1576XA(C1616Xf xf) {
        super(xf);
    }

    /* renamed from: c */
    public C5663aRz[] mo309c() {
        return DamagerType._m_fields;
    }

    /* renamed from: d */
    public void mo310d() {
        super.mo310d();
        this.eJl = 0.0f;
        this.eJm = 0.0f;
        this.eJn = null;
        this.eJo = null;
        this.f2097tl = 0;
        this.f2098tp = 0;
        this.eJp = 0;
        this.eJq = 0;
        this.f2095tO = 0;
        this.f2096tS = 0;
        this.eJr = 0;
        this.eJs = 0;
    }
}
