package logic.data.mbean;

import game.script.nls.NLSAssociates;
import logic.baa.C1616Xf;
import logic.res.code.C5663aRz;
import p001a.C0064Am;
import taikodom.infra.script.I18NString;

/* renamed from: a.avO  reason: case insensitive filesystem */
public class C6874avO extends C2484fi {
    @C0064Am(aul = "c4dd8e64b4cee251d4f21fdc881fa7b7", aum = 0)
    public I18NString alb;
    @C0064Am(aul = "685e8c3a21cdde46ae2bb2fd020a6742", aum = 1)
    public I18NString ald;
    @C0064Am(aul = "c4dd8e64b4cee251d4f21fdc881fa7b7", aum = -2)
    public long eSC;
    @C0064Am(aul = "685e8c3a21cdde46ae2bb2fd020a6742", aum = -2)
    public long eSD;
    @C0064Am(aul = "c4dd8e64b4cee251d4f21fdc881fa7b7", aum = -1)
    public byte eST;
    @C0064Am(aul = "685e8c3a21cdde46ae2bb2fd020a6742", aum = -1)
    public byte eSU;
    @C0064Am(aul = "f59ead4f01d8c9eb3c2f689afbe1d9aa", aum = 2)
    public I18NString gJB;
    @C0064Am(aul = "beb9ad4bde9f7a36e6d431925c7a53b5", aum = 3)
    public I18NString gJC;
    @C0064Am(aul = "b634ea3e3eed06ddd2d27f2e8679a1ce", aum = 4)
    public I18NString gJD;
    @C0064Am(aul = "b0752cb86c00c26bb228d18622927ebb", aum = 5)
    public I18NString gJE;
    @C0064Am(aul = "5f8e9cb7603307be781cfc639680afff", aum = 6)
    public I18NString gJF;
    @C0064Am(aul = "5e80253598f12f3f26d8cece4da78a8a", aum = 7)
    public I18NString gJG;
    @C0064Am(aul = "7e27d3d1491cb59c10f1304c6fb2ced3", aum = 8)
    public I18NString gJH;
    @C0064Am(aul = "a49453645b9b9c6e4d9fc96937391dd9", aum = 9)
    public I18NString gJI;
    @C0064Am(aul = "6e460b9a4d01161146059a3605a49158", aum = 10)
    public I18NString gJJ;
    @C0064Am(aul = "890906320f4cddd8e9a30bd3df3382c8", aum = 11)
    public I18NString gJK;
    @C0064Am(aul = "ce09928bf25011c993f40f0fb3291759", aum = 12)
    public I18NString gJL;
    @C0064Am(aul = "92edfe59aaa1634bb0cf6a722badadcc", aum = 13)
    public I18NString gJM;
    @C0064Am(aul = "001345df64be3d6905d2153b5997f6c4", aum = 14)
    public I18NString gJN;
    @C0064Am(aul = "78eb1a2355b9152a542a3d7a21b2b69f", aum = 15)
    public I18NString gJO;
    @C0064Am(aul = "45b232d87896da9094ca7ba1997234b1", aum = 16)
    public I18NString gJP;
    @C0064Am(aul = "8f21b8bc5186c3b372c9cf2bc2421664", aum = 17)
    public I18NString gJQ;
    @C0064Am(aul = "8a0ffbf71685f7fa40284b9b5dbf60fc", aum = 18)
    public I18NString gJR;
    @C0064Am(aul = "e15fa5a31da5aa85be278978dd1fbdf3", aum = 19)
    public I18NString gJS;
    @C0064Am(aul = "6bdbf50f0504e4ff5999ec66a99eb814", aum = 20)
    public I18NString gJT;
    @C0064Am(aul = "95bdaa297538efb81997b9aaef4b794c", aum = 21)
    public I18NString gJU;
    @C0064Am(aul = "a199b430dacbca5dad113070f0f87b81", aum = 22)
    public I18NString gJV;
    @C0064Am(aul = "f7a96f7dcb40e96eeb6b980e9f0dbb15", aum = 23)
    public I18NString gJW;
    @C0064Am(aul = "d0febb7b935e02abfb986fa69cb26d71", aum = 24)
    public I18NString gJX;
    @C0064Am(aul = "3b56974743e5af80e608ec3a957601da", aum = 25)
    public I18NString gJY;
    @C0064Am(aul = "f59ead4f01d8c9eb3c2f689afbe1d9aa", aum = -2)
    public long gJZ;
    @C0064Am(aul = "b0752cb86c00c26bb228d18622927ebb", aum = -1)
    public byte gKA;
    @C0064Am(aul = "5f8e9cb7603307be781cfc639680afff", aum = -1)
    public byte gKB;
    @C0064Am(aul = "5e80253598f12f3f26d8cece4da78a8a", aum = -1)
    public byte gKC;
    @C0064Am(aul = "7e27d3d1491cb59c10f1304c6fb2ced3", aum = -1)
    public byte gKD;
    @C0064Am(aul = "a49453645b9b9c6e4d9fc96937391dd9", aum = -1)
    public byte gKE;
    @C0064Am(aul = "6e460b9a4d01161146059a3605a49158", aum = -1)
    public byte gKF;
    @C0064Am(aul = "890906320f4cddd8e9a30bd3df3382c8", aum = -1)
    public byte gKG;
    @C0064Am(aul = "ce09928bf25011c993f40f0fb3291759", aum = -1)
    public byte gKH;
    @C0064Am(aul = "92edfe59aaa1634bb0cf6a722badadcc", aum = -1)
    public byte gKI;
    @C0064Am(aul = "001345df64be3d6905d2153b5997f6c4", aum = -1)
    public byte gKJ;
    @C0064Am(aul = "78eb1a2355b9152a542a3d7a21b2b69f", aum = -1)
    public byte gKK;
    @C0064Am(aul = "45b232d87896da9094ca7ba1997234b1", aum = -1)
    public byte gKL;
    @C0064Am(aul = "8f21b8bc5186c3b372c9cf2bc2421664", aum = -1)
    public byte gKM;
    @C0064Am(aul = "8a0ffbf71685f7fa40284b9b5dbf60fc", aum = -1)
    public byte gKN;
    @C0064Am(aul = "e15fa5a31da5aa85be278978dd1fbdf3", aum = -1)
    public byte gKO;
    @C0064Am(aul = "6bdbf50f0504e4ff5999ec66a99eb814", aum = -1)
    public byte gKP;
    @C0064Am(aul = "95bdaa297538efb81997b9aaef4b794c", aum = -1)
    public byte gKQ;
    @C0064Am(aul = "a199b430dacbca5dad113070f0f87b81", aum = -1)
    public byte gKR;
    @C0064Am(aul = "f7a96f7dcb40e96eeb6b980e9f0dbb15", aum = -1)
    public byte gKS;
    @C0064Am(aul = "d0febb7b935e02abfb986fa69cb26d71", aum = -1)
    public byte gKT;
    @C0064Am(aul = "3b56974743e5af80e608ec3a957601da", aum = -1)
    public byte gKU;
    @C0064Am(aul = "beb9ad4bde9f7a36e6d431925c7a53b5", aum = -2)
    public long gKa;
    @C0064Am(aul = "b634ea3e3eed06ddd2d27f2e8679a1ce", aum = -2)
    public long gKb;
    @C0064Am(aul = "b0752cb86c00c26bb228d18622927ebb", aum = -2)
    public long gKc;
    @C0064Am(aul = "5f8e9cb7603307be781cfc639680afff", aum = -2)
    public long gKd;
    @C0064Am(aul = "5e80253598f12f3f26d8cece4da78a8a", aum = -2)
    public long gKe;
    @C0064Am(aul = "7e27d3d1491cb59c10f1304c6fb2ced3", aum = -2)
    public long gKf;
    @C0064Am(aul = "a49453645b9b9c6e4d9fc96937391dd9", aum = -2)
    public long gKg;
    @C0064Am(aul = "6e460b9a4d01161146059a3605a49158", aum = -2)
    public long gKh;
    @C0064Am(aul = "890906320f4cddd8e9a30bd3df3382c8", aum = -2)
    public long gKi;
    @C0064Am(aul = "ce09928bf25011c993f40f0fb3291759", aum = -2)
    public long gKj;
    @C0064Am(aul = "92edfe59aaa1634bb0cf6a722badadcc", aum = -2)
    public long gKk;
    @C0064Am(aul = "001345df64be3d6905d2153b5997f6c4", aum = -2)
    public long gKl;
    @C0064Am(aul = "78eb1a2355b9152a542a3d7a21b2b69f", aum = -2)
    public long gKm;
    @C0064Am(aul = "45b232d87896da9094ca7ba1997234b1", aum = -2)
    public long gKn;
    @C0064Am(aul = "8f21b8bc5186c3b372c9cf2bc2421664", aum = -2)
    public long gKo;
    @C0064Am(aul = "8a0ffbf71685f7fa40284b9b5dbf60fc", aum = -2)
    public long gKp;
    @C0064Am(aul = "e15fa5a31da5aa85be278978dd1fbdf3", aum = -2)
    public long gKq;
    @C0064Am(aul = "6bdbf50f0504e4ff5999ec66a99eb814", aum = -2)
    public long gKr;
    @C0064Am(aul = "95bdaa297538efb81997b9aaef4b794c", aum = -2)
    public long gKs;
    @C0064Am(aul = "a199b430dacbca5dad113070f0f87b81", aum = -2)
    public long gKt;
    @C0064Am(aul = "f7a96f7dcb40e96eeb6b980e9f0dbb15", aum = -2)
    public long gKu;
    @C0064Am(aul = "d0febb7b935e02abfb986fa69cb26d71", aum = -2)
    public long gKv;
    @C0064Am(aul = "3b56974743e5af80e608ec3a957601da", aum = -2)
    public long gKw;
    @C0064Am(aul = "f59ead4f01d8c9eb3c2f689afbe1d9aa", aum = -1)
    public byte gKx;
    @C0064Am(aul = "beb9ad4bde9f7a36e6d431925c7a53b5", aum = -1)
    public byte gKy;
    @C0064Am(aul = "b634ea3e3eed06ddd2d27f2e8679a1ce", aum = -1)
    public byte gKz;

    public C6874avO() {
    }

    public C6874avO(C1616Xf xf) {
        super(xf);
    }

    /* renamed from: c */
    public C5663aRz[] mo309c() {
        return NLSAssociates._m_fields;
    }

    /* renamed from: d */
    public void mo310d() {
        super.mo310d();
        this.alb = null;
        this.ald = null;
        this.gJB = null;
        this.gJC = null;
        this.gJD = null;
        this.gJE = null;
        this.gJF = null;
        this.gJG = null;
        this.gJH = null;
        this.gJI = null;
        this.gJJ = null;
        this.gJK = null;
        this.gJL = null;
        this.gJM = null;
        this.gJN = null;
        this.gJO = null;
        this.gJP = null;
        this.gJQ = null;
        this.gJR = null;
        this.gJS = null;
        this.gJT = null;
        this.gJU = null;
        this.gJV = null;
        this.gJW = null;
        this.gJX = null;
        this.gJY = null;
        this.eSC = 0;
        this.eSD = 0;
        this.gJZ = 0;
        this.gKa = 0;
        this.gKb = 0;
        this.gKc = 0;
        this.gKd = 0;
        this.gKe = 0;
        this.gKf = 0;
        this.gKg = 0;
        this.gKh = 0;
        this.gKi = 0;
        this.gKj = 0;
        this.gKk = 0;
        this.gKl = 0;
        this.gKm = 0;
        this.gKn = 0;
        this.gKo = 0;
        this.gKp = 0;
        this.gKq = 0;
        this.gKr = 0;
        this.gKs = 0;
        this.gKt = 0;
        this.gKu = 0;
        this.gKv = 0;
        this.gKw = 0;
        this.eST = 0;
        this.eSU = 0;
        this.gKx = 0;
        this.gKy = 0;
        this.gKz = 0;
        this.gKA = 0;
        this.gKB = 0;
        this.gKC = 0;
        this.gKD = 0;
        this.gKE = 0;
        this.gKF = 0;
        this.gKG = 0;
        this.gKH = 0;
        this.gKI = 0;
        this.gKJ = 0;
        this.gKK = 0;
        this.gKL = 0;
        this.gKM = 0;
        this.gKN = 0;
        this.gKO = 0;
        this.gKP = 0;
        this.gKQ = 0;
        this.gKR = 0;
        this.gKS = 0;
        this.gKT = 0;
        this.gKU = 0;
    }
}
