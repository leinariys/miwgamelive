package logic.data.mbean;

import game.network.message.serializable.C2686iZ;
import logic.baa.C1616Xf;
import logic.res.code.C5663aRz;
import p001a.C0064Am;
import taikodom.infra.script.I18NString;

import java.util.UUID;

/* renamed from: a.aBy  reason: case insensitive filesystem */
public class C5246aBy extends C3805vD {
    @C0064Am(aul = "eb1e047715296cf1bde1b3fcb2211dea", aum = 4)

    /* renamed from: bK */
    public UUID f2449bK;
    @C0064Am(aul = "068e797efb5e61e1eacc0396981f9cbb", aum = -2)
    public long dIZ;
    @C0064Am(aul = "068e797efb5e61e1eacc0396981f9cbb", aum = -1)
    public byte dJf;
    @C0064Am(aul = "ab1b976f810af85b760cc89798082475", aum = 1)
    public I18NString fQv;
    @C0064Am(aul = "749e073a8593d03f91934172dff5685c", aum = 2)
    public I18NString fQw;
    @C0064Am(aul = "068e797efb5e61e1eacc0396981f9cbb", aum = 3)
    public C2686iZ<Rule> fQy;
    @C0064Am(aul = "ab1b976f810af85b760cc89798082475", aum = -2)
    public long hhs;
    @C0064Am(aul = "749e073a8593d03f91934172dff5685c", aum = -2)
    public long hht;
    @C0064Am(aul = "ab1b976f810af85b760cc89798082475", aum = -1)
    public byte hhu;
    @C0064Am(aul = "749e073a8593d03f91934172dff5685c", aum = -1)
    public byte hhv;
    @C0064Am(aul = "890958eccab9f32782d5cf19b009ee1d", aum = 0)

    /* renamed from: ni */
    public I18NString f2450ni;
    @C0064Am(aul = "890958eccab9f32782d5cf19b009ee1d", aum = -2)

    /* renamed from: nl */
    public long f2451nl;
    @C0064Am(aul = "890958eccab9f32782d5cf19b009ee1d", aum = -1)

    /* renamed from: no */
    public byte f2452no;
    @C0064Am(aul = "eb1e047715296cf1bde1b3fcb2211dea", aum = -2)

    /* renamed from: oL */
    public long f2453oL;
    @C0064Am(aul = "eb1e047715296cf1bde1b3fcb2211dea", aum = -1)

    /* renamed from: oS */
    public byte f2454oS;

    public C5246aBy() {
    }

    public C5246aBy(C1616Xf xf) {
        super(xf);
    }

    /* renamed from: c */
    public C5663aRz[] mo309c() {
        return TutorialPage._m_fields;
    }

    /* renamed from: d */
    public void mo310d() {
        super.mo310d();
        this.f2450ni = null;
        this.fQv = null;
        this.fQw = null;
        this.fQy = null;
        this.f2449bK = null;
        this.f2451nl = 0;
        this.hhs = 0;
        this.hht = 0;
        this.dIZ = 0;
        this.f2453oL = 0;
        this.f2452no = 0;
        this.hhu = 0;
        this.hhv = 0;
        this.dJf = 0;
        this.f2454oS = 0;
    }
}
