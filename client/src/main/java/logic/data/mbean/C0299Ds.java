package logic.data.mbean;

import game.network.message.serializable.C1556Wo;
import game.script.cloning.CloningInfo;
import game.script.player.Player;
import game.script.ship.Station;
import logic.baa.C1616Xf;
import logic.res.code.C5663aRz;
import p001a.C0064Am;

/* renamed from: a.Ds */
public class C0299Ds extends C3805vD {
    @C0064Am(aul = "63324418db55bf8438f8ac6752855714", aum = 0)

    /* renamed from: P */
    public Player f452P;
    @C0064Am(aul = "7bbae1f0a3220875be9e7b493eeb0e18", aum = 2)

    /* renamed from: RG */
    public Station f453RG;
    @C0064Am(aul = "7bbae1f0a3220875be9e7b493eeb0e18", aum = -2)
    public long aIK;
    @C0064Am(aul = "7bbae1f0a3220875be9e7b493eeb0e18", aum = -1)
    public byte aIV;
    @C0064Am(aul = "63324418db55bf8438f8ac6752855714", aum = -2)

    /* renamed from: ag */
    public long f454ag;
    @C0064Am(aul = "63324418db55bf8438f8ac6752855714", aum = -1)

    /* renamed from: av */
    public byte f455av;
    @C0064Am(aul = "01a2f7e2e2d5e3a02e9c1a03eb38a354", aum = 1)
    public C1556Wo<Station, Integer> cGU;
    @C0064Am(aul = "367a18845e9af9e9aff4f3ab9931ba4d", aum = 3)
    public Station cGV;
    @C0064Am(aul = "01a2f7e2e2d5e3a02e9c1a03eb38a354", aum = -2)
    public long cGW;
    @C0064Am(aul = "367a18845e9af9e9aff4f3ab9931ba4d", aum = -2)
    public long cGX;
    @C0064Am(aul = "01a2f7e2e2d5e3a02e9c1a03eb38a354", aum = -1)
    public byte cGY;
    @C0064Am(aul = "367a18845e9af9e9aff4f3ab9931ba4d", aum = -1)
    public byte cGZ;

    public C0299Ds() {
    }

    public C0299Ds(C1616Xf xf) {
        super(xf);
    }

    /* renamed from: c */
    public C5663aRz[] mo309c() {
        return CloningInfo._m_fields;
    }

    /* renamed from: d */
    public void mo310d() {
        super.mo310d();
        this.f452P = null;
        this.cGU = null;
        this.f453RG = null;
        this.cGV = null;
        this.f454ag = 0;
        this.cGW = 0;
        this.aIK = 0;
        this.cGX = 0;
        this.f455av = 0;
        this.cGY = 0;
        this.aIV = 0;
        this.cGZ = 0;
    }
}
