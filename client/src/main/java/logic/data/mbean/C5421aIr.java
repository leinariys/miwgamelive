package logic.data.mbean;

import game.network.message.serializable.C1556Wo;
import game.script.Character;
import game.script.npc.NPC;
import game.script.spacezone.population.TrainingZonePopulationBehaviour;
import logic.baa.C1616Xf;
import logic.res.code.C5663aRz;
import p001a.C0064Am;

/* renamed from: a.aIr  reason: case insensitive filesystem */
public class C5421aIr extends C1542Wf {
    @C0064Am(aul = "67504d0baa5bb32ab9db220d5d29edc1", aum = 0)
    public boolean icc;
    @C0064Am(aul = "c65a803d3bd43741f724b4224e777bc5", aum = 1)
    public int icd;
    @C0064Am(aul = "0412a41e7073e56da2a4476e174556f9", aum = 2)
    public C1556Wo<NPC, TrainingZonePopulationBehaviour.CharacterList> ice;
    @C0064Am(aul = "2be56080bb5fb4ebd312d326120a1d33", aum = 3)
    public C1556Wo<Character, TrainingZonePopulationBehaviour.NPCList> icf;
    @C0064Am(aul = "67504d0baa5bb32ab9db220d5d29edc1", aum = -2)
    public long icg;
    @C0064Am(aul = "c65a803d3bd43741f724b4224e777bc5", aum = -2)
    public long ich;
    @C0064Am(aul = "0412a41e7073e56da2a4476e174556f9", aum = -2)
    public long ici;
    @C0064Am(aul = "2be56080bb5fb4ebd312d326120a1d33", aum = -2)
    public long icj;
    @C0064Am(aul = "67504d0baa5bb32ab9db220d5d29edc1", aum = -1)
    public byte ick;
    @C0064Am(aul = "c65a803d3bd43741f724b4224e777bc5", aum = -1)
    public byte icl;
    @C0064Am(aul = "0412a41e7073e56da2a4476e174556f9", aum = -1)
    public byte icm;
    @C0064Am(aul = "2be56080bb5fb4ebd312d326120a1d33", aum = -1)
    public byte icn;

    public C5421aIr() {
    }

    public C5421aIr(C1616Xf xf) {
        super(xf);
    }

    /* renamed from: c */
    public C5663aRz[] mo309c() {
        return TrainingZonePopulationBehaviour._m_fields;
    }

    /* renamed from: d */
    public void mo310d() {
        super.mo310d();
        this.icc = false;
        this.icd = 0;
        this.ice = null;
        this.icf = null;
        this.icg = 0;
        this.ich = 0;
        this.ici = 0;
        this.icj = 0;
        this.ick = 0;
        this.icl = 0;
        this.icm = 0;
        this.icn = 0;
    }
}
