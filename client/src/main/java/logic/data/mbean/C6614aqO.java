package logic.data.mbean;

import game.geometry.Vec3d;
import game.script.missiontemplate.PositionDat;
import logic.baa.C1616Xf;
import logic.res.code.C5663aRz;
import p001a.C0064Am;

import java.util.UUID;

/* renamed from: a.aqO  reason: case insensitive filesystem */
public class C6614aqO extends C5292aDs {
    @C0064Am(aul = "51644a6d4fb54453a22459c6d2f7a27e", aum = 1)

    /* renamed from: bK */
    public UUID f5167bK;
    @C0064Am(aul = "a073bf68ba27ac37762c090bff46a394", aum = 2)
    public String handle;
    @C0064Am(aul = "51644a6d4fb54453a22459c6d2f7a27e", aum = -2)

    /* renamed from: oL */
    public long f5168oL;
    @C0064Am(aul = "51644a6d4fb54453a22459c6d2f7a27e", aum = -1)

    /* renamed from: oS */
    public byte f5169oS;
    @C0064Am(aul = "a073bf68ba27ac37762c090bff46a394", aum = -2)

    /* renamed from: ok */
    public long f5170ok;
    @C0064Am(aul = "a073bf68ba27ac37762c090bff46a394", aum = -1)

    /* renamed from: on */
    public byte f5171on;
    @C0064Am(aul = "2f0920b2f54bca6d67d10200ac9d784f", aum = 0)
    public Vec3d position;
    @C0064Am(aul = "2f0920b2f54bca6d67d10200ac9d784f", aum = -2)

    /* renamed from: rX */
    public long f5172rX;
    @C0064Am(aul = "2f0920b2f54bca6d67d10200ac9d784f", aum = -1)

    /* renamed from: sn */
    public byte f5173sn;

    public C6614aqO() {
    }

    public C6614aqO(C1616Xf xf) {
        super(xf);
    }

    /* renamed from: c */
    public C5663aRz[] mo309c() {
        return PositionDat._m_fields;
    }

    /* renamed from: d */
    public void mo310d() {
        super.mo310d();
        this.position = null;
        this.f5167bK = null;
        this.handle = null;
        this.f5172rX = 0;
        this.f5168oL = 0;
        this.f5170ok = 0;
        this.f5173sn = 0;
        this.f5169oS = 0;
        this.f5171on = 0;
    }
}
