package logic.data.mbean;

import game.network.message.serializable.C1556Wo;
import game.script.corporation.CorporationRoleDefaults;
import game.script.resource.Asset;
import logic.baa.C1616Xf;
import logic.res.code.C5663aRz;
import p001a.C0064Am;
import p001a.C5741aUz;
import p001a.C6704asA;
import taikodom.infra.script.I18NString;

import java.util.UUID;

/* renamed from: a.asz  reason: case insensitive filesystem */
public class C6755asz extends C5292aDs {
    @C0064Am(aul = "905b6405d36d12476238d22a65b280c8", aum = -2)

    /* renamed from: Nf */
    public long f5298Nf;
    @C0064Am(aul = "905b6405d36d12476238d22a65b280c8", aum = -1)

    /* renamed from: Nh */
    public byte f5299Nh;
    @C0064Am(aul = "bb0292d7e3a6f3b29de44272a994e117", aum = 5)

    /* renamed from: bK */
    public UUID f5300bK;
    @C0064Am(aul = "61d6a70f63b2953b41f59aefd7b4366c", aum = -1)
    public byte dhC;
    @C0064Am(aul = "a22544b587ced2832ba57f2ab443ca33", aum = -1)
    public byte dhD;
    @C0064Am(aul = "61d6a70f63b2953b41f59aefd7b4366c", aum = 0)
    public C5741aUz dhu;
    @C0064Am(aul = "a22544b587ced2832ba57f2ab443ca33", aum = 4)
    public C1556Wo<C6704asA, Boolean> dhv;
    @C0064Am(aul = "61d6a70f63b2953b41f59aefd7b4366c", aum = -2)
    public long dhy;
    @C0064Am(aul = "a22544b587ced2832ba57f2ab443ca33", aum = -2)
    public long dhz;
    @C0064Am(aul = "ffe46553f485383a8ea86acce862ac0b", aum = 2)
    public I18NString gvd;
    @C0064Am(aul = "ffe46553f485383a8ea86acce862ac0b", aum = -2)
    public long gve;
    @C0064Am(aul = "ffe46553f485383a8ea86acce862ac0b", aum = -1)
    public byte gvf;
    @C0064Am(aul = "5452ac66a14c7515b8f4200d2c5e2262", aum = -1)

    /* renamed from: jK */
    public byte f5301jK;
    @C0064Am(aul = "5452ac66a14c7515b8f4200d2c5e2262", aum = 3)

    /* renamed from: jn */
    public Asset f5302jn;
    @C0064Am(aul = "5452ac66a14c7515b8f4200d2c5e2262", aum = -2)

    /* renamed from: jy */
    public long f5303jy;
    @C0064Am(aul = "bb0292d7e3a6f3b29de44272a994e117", aum = -2)

    /* renamed from: oL */
    public long f5304oL;
    @C0064Am(aul = "bb0292d7e3a6f3b29de44272a994e117", aum = -1)

    /* renamed from: oS */
    public byte f5305oS;
    @C0064Am(aul = "905b6405d36d12476238d22a65b280c8", aum = 1)

    /* renamed from: zP */
    public I18NString f5306zP;

    public C6755asz() {
    }

    public C6755asz(C1616Xf xf) {
        super(xf);
    }

    /* renamed from: c */
    public C5663aRz[] mo309c() {
        return CorporationRoleDefaults._m_fields;
    }

    /* renamed from: d */
    public void mo310d() {
        super.mo310d();
        this.dhu = null;
        this.f5306zP = null;
        this.gvd = null;
        this.f5302jn = null;
        this.dhv = null;
        this.f5300bK = null;
        this.dhy = 0;
        this.f5298Nf = 0;
        this.gve = 0;
        this.f5303jy = 0;
        this.dhz = 0;
        this.f5304oL = 0;
        this.dhC = 0;
        this.f5299Nh = 0;
        this.gvf = 0;
        this.f5301jK = 0;
        this.dhD = 0;
        this.f5305oS = 0;
    }
}
