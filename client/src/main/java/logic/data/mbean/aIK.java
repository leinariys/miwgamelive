package logic.data.mbean;

import game.network.message.serializable.C1556Wo;
import game.script.damage.DamageType;
import game.script.item.buff.amplifier.StrikeAmplifierType;
import logic.baa.C1616Xf;
import logic.res.code.C5663aRz;
import p001a.C0064Am;
import p001a.C3892wO;

/* renamed from: a.aIK */
public class aIK extends C5346aFu {
    @C0064Am(aul = "6e63cd56fdf5168c0118f05978aae1ce", aum = -2)

    /* renamed from: YA */
    public long f3049YA;
    @C0064Am(aul = "8e7c43dbdf57e2c4276386dd3e0ef4a3", aum = -2)

    /* renamed from: YB */
    public long f3050YB;
    @C0064Am(aul = "6e63cd56fdf5168c0118f05978aae1ce", aum = -1)

    /* renamed from: YG */
    public byte f3051YG;
    @C0064Am(aul = "8e7c43dbdf57e2c4276386dd3e0ef4a3", aum = -1)

    /* renamed from: YH */
    public byte f3052YH;
    @C0064Am(aul = "6e63cd56fdf5168c0118f05978aae1ce", aum = 1)

    /* renamed from: Yu */
    public C3892wO f3053Yu;
    @C0064Am(aul = "8e7c43dbdf57e2c4276386dd3e0ef4a3", aum = 3)

    /* renamed from: Yv */
    public C3892wO f3054Yv;
    @C0064Am(aul = "f87735b8d6c33c9425ec6c0178570e27", aum = 0)
    public C1556Wo<DamageType, C3892wO> atO;
    @C0064Am(aul = "67a7ea8d2e72b5691c28a2ab26d50292", aum = 2)
    public C3892wO atR;
    @C0064Am(aul = "67a7ea8d2e72b5691c28a2ab26d50292", aum = -2)
    public long cAA;
    @C0064Am(aul = "67a7ea8d2e72b5691c28a2ab26d50292", aum = -1)
    public byte cAO;
    @C0064Am(aul = "f87735b8d6c33c9425ec6c0178570e27", aum = -2)
    public long ide;
    @C0064Am(aul = "f87735b8d6c33c9425ec6c0178570e27", aum = -1)
    public byte idf;

    public aIK() {
    }

    public aIK(C1616Xf xf) {
        super(xf);
    }

    /* renamed from: c */
    public C5663aRz[] mo309c() {
        return StrikeAmplifierType._m_fields;
    }

    /* renamed from: d */
    public void mo310d() {
        super.mo310d();
        this.atO = null;
        this.f3053Yu = null;
        this.atR = null;
        this.f3054Yv = null;
        this.ide = 0;
        this.f3049YA = 0;
        this.cAA = 0;
        this.f3050YB = 0;
        this.idf = 0;
        this.f3051YG = 0;
        this.cAO = 0;
        this.f3052YH = 0;
    }
}
