package logic.data.mbean;

import game.script.nls.NLSOutpost;
import logic.baa.C1616Xf;
import logic.res.code.C5663aRz;
import p001a.C0064Am;
import taikodom.infra.script.I18NString;

/* renamed from: a.aVD */
public class aVD extends C2484fi {
    @C0064Am(aul = "e1a8318f6658c2c72df56eb1e6d531ee", aum = 0)
    public I18NString feJ;
    @C0064Am(aul = "f93ceb67030255c37f794bedec4b468d", aum = 1)
    public I18NString feL;
    @C0064Am(aul = "e1a8318f6658c2c72df56eb1e6d531ee", aum = -2)
    public long jaR;
    @C0064Am(aul = "f93ceb67030255c37f794bedec4b468d", aum = -2)
    public long jaS;
    @C0064Am(aul = "e1a8318f6658c2c72df56eb1e6d531ee", aum = -1)
    public byte jaT;
    @C0064Am(aul = "f93ceb67030255c37f794bedec4b468d", aum = -1)
    public byte jaU;

    public aVD() {
    }

    public aVD(C1616Xf xf) {
        super(xf);
    }

    /* renamed from: c */
    public C5663aRz[] mo309c() {
        return NLSOutpost._m_fields;
    }

    /* renamed from: d */
    public void mo310d() {
        super.mo310d();
        this.feJ = null;
        this.feL = null;
        this.jaR = 0;
        this.jaS = 0;
        this.jaT = 0;
        this.jaU = 0;
    }
}
