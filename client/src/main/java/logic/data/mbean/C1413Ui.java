package logic.data.mbean;

import game.network.message.serializable.C3438ra;
import game.script.pda.DatabaseCategory;
import game.script.pda.TaikopediaEntry;
import game.script.resource.Asset;
import game.script.spacezone.AsteroidZoneCategory;
import logic.baa.C1616Xf;
import logic.res.code.C5663aRz;
import p001a.C0064Am;
import taikodom.infra.script.I18NString;

import java.util.UUID;

/* renamed from: a.Ui */
public class C1413Ui extends C3805vD {
    @C0064Am(aul = "ffa173a37c2692733272adbec436b48f", aum = 8)

    /* renamed from: bK */
    public UUID f1771bK;
    @C0064Am(aul = "2438b99e9e79960d342305c7949e4794", aum = 0)
    public String handle;
    @C0064Am(aul = "8f33df8f37e35a06009e2841192842d6", aum = -1)

    /* renamed from: jK */
    public byte f1772jK;
    @C0064Am(aul = "85cdd11e392eb8e7d31b3d6fd9edbb71", aum = 5)

    /* renamed from: jS */
    public DatabaseCategory f1773jS;
    @C0064Am(aul = "bc04854cb7808cac5b1c94c8164aa1c4", aum = 6)

    /* renamed from: jT */
    public C3438ra<TaikopediaEntry> f1774jT;
    @C0064Am(aul = "129275444d262df156492ca5b22838b4", aum = 2)

    /* renamed from: jU */
    public Asset f1775jU;
    @C0064Am(aul = "065b7bbac39e271d42c4a1879072d44e", aum = 7)

    /* renamed from: jV */
    public float f1776jV;
    @C0064Am(aul = "85cdd11e392eb8e7d31b3d6fd9edbb71", aum = -2)

    /* renamed from: jX */
    public long f1777jX;
    @C0064Am(aul = "bc04854cb7808cac5b1c94c8164aa1c4", aum = -2)

    /* renamed from: jY */
    public long f1778jY;
    @C0064Am(aul = "129275444d262df156492ca5b22838b4", aum = -2)

    /* renamed from: jZ */
    public long f1779jZ;
    @C0064Am(aul = "8f33df8f37e35a06009e2841192842d6", aum = 1)

    /* renamed from: jn */
    public Asset f1780jn;
    @C0064Am(aul = "8f33df8f37e35a06009e2841192842d6", aum = -2)

    /* renamed from: jy */
    public long f1781jy;
    @C0064Am(aul = "065b7bbac39e271d42c4a1879072d44e", aum = -2)

    /* renamed from: ka */
    public long f1782ka;
    @C0064Am(aul = "85cdd11e392eb8e7d31b3d6fd9edbb71", aum = -1)

    /* renamed from: kc */
    public byte f1783kc;
    @C0064Am(aul = "bc04854cb7808cac5b1c94c8164aa1c4", aum = -1)

    /* renamed from: kd */
    public byte f1784kd;
    @C0064Am(aul = "129275444d262df156492ca5b22838b4", aum = -1)

    /* renamed from: ke */
    public byte f1785ke;
    @C0064Am(aul = "065b7bbac39e271d42c4a1879072d44e", aum = -1)

    /* renamed from: kf */
    public byte f1786kf;
    @C0064Am(aul = "b3267866ac9b36a17a46cee98828fc3c", aum = 4)

    /* renamed from: nh */
    public I18NString f1787nh;
    @C0064Am(aul = "296688f3bfb30cc1f295f6dc5fd223a8", aum = 3)

    /* renamed from: ni */
    public I18NString f1788ni;
    @C0064Am(aul = "b3267866ac9b36a17a46cee98828fc3c", aum = -2)

    /* renamed from: nk */
    public long f1789nk;
    @C0064Am(aul = "296688f3bfb30cc1f295f6dc5fd223a8", aum = -2)

    /* renamed from: nl */
    public long f1790nl;
    @C0064Am(aul = "b3267866ac9b36a17a46cee98828fc3c", aum = -1)

    /* renamed from: nn */
    public byte f1791nn;
    @C0064Am(aul = "296688f3bfb30cc1f295f6dc5fd223a8", aum = -1)

    /* renamed from: no */
    public byte f1792no;
    @C0064Am(aul = "ffa173a37c2692733272adbec436b48f", aum = -2)

    /* renamed from: oL */
    public long f1793oL;
    @C0064Am(aul = "ffa173a37c2692733272adbec436b48f", aum = -1)

    /* renamed from: oS */
    public byte f1794oS;
    @C0064Am(aul = "2438b99e9e79960d342305c7949e4794", aum = -2)

    /* renamed from: ok */
    public long f1795ok;
    @C0064Am(aul = "2438b99e9e79960d342305c7949e4794", aum = -1)

    /* renamed from: on */
    public byte f1796on;

    public C1413Ui() {
    }

    public C1413Ui(C1616Xf xf) {
        super(xf);
    }

    /* renamed from: c */
    public C5663aRz[] mo309c() {
        return AsteroidZoneCategory._m_fields;
    }

    /* renamed from: d */
    public void mo310d() {
        super.mo310d();
        this.handle = null;
        this.f1780jn = null;
        this.f1775jU = null;
        this.f1788ni = null;
        this.f1787nh = null;
        this.f1773jS = null;
        this.f1774jT = null;
        this.f1776jV = 0.0f;
        this.f1771bK = null;
        this.f1795ok = 0;
        this.f1781jy = 0;
        this.f1779jZ = 0;
        this.f1790nl = 0;
        this.f1789nk = 0;
        this.f1777jX = 0;
        this.f1778jY = 0;
        this.f1782ka = 0;
        this.f1793oL = 0;
        this.f1796on = 0;
        this.f1772jK = 0;
        this.f1785ke = 0;
        this.f1792no = 0;
        this.f1791nn = 0;
        this.f1783kc = 0;
        this.f1784kd = 0;
        this.f1786kf = 0;
        this.f1794oS = 0;
    }
}
