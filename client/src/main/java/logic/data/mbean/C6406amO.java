package logic.data.mbean;

import game.script.ai.npc.evolving.EvolvingDroneAIController;
import logic.baa.C1616Xf;
import logic.res.code.C5663aRz;
import p001a.C0064Am;
import p001a.C2971mQ;
import p001a.C6712asI;

/* renamed from: a.amO  reason: case insensitive filesystem */
public class C6406amO extends C6097agR {
    @C0064Am(aul = "dab2463d551cf8ac6f302b23c46abadc", aum = 0)
    public C2971mQ dIu;
    @C0064Am(aul = "bf42b21deaf206143233714595d50130", aum = 1)
    public C6712asI dIw;
    @C0064Am(aul = "dab2463d551cf8ac6f302b23c46abadc", aum = -2)
    public long gbC;
    @C0064Am(aul = "bf42b21deaf206143233714595d50130", aum = -2)
    public long gbD;
    @C0064Am(aul = "dab2463d551cf8ac6f302b23c46abadc", aum = -1)
    public byte gbE;
    @C0064Am(aul = "bf42b21deaf206143233714595d50130", aum = -1)
    public byte gbF;

    public C6406amO() {
    }

    public C6406amO(C1616Xf xf) {
        super(xf);
    }

    /* renamed from: c */
    public C5663aRz[] mo309c() {
        return EvolvingDroneAIController._m_fields;
    }

    /* renamed from: d */
    public void mo310d() {
        super.mo310d();
        this.dIu = null;
        this.dIw = null;
        this.gbC = 0;
        this.gbD = 0;
        this.gbE = 0;
        this.gbF = 0;
    }
}
