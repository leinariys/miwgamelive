package logic.data.mbean;

import game.script.item.buff.module.CruiseSpeedBuffType;
import logic.baa.C1616Xf;
import logic.res.code.C5663aRz;
import p001a.C0064Am;
import p001a.C3892wO;

/* renamed from: a.aHd  reason: case insensitive filesystem */
public class C5381aHd extends C5661aRx {
    @C0064Am(aul = "5c77a98a6c65cd19bd5fe125d161d3f1", aum = 0)

    /* renamed from: Vs */
    public C3892wO f2976Vs;
    @C0064Am(aul = "0c2631fed5b4bf4a405b46963662db11", aum = 1)

    /* renamed from: Vu */
    public C3892wO f2977Vu;
    @C0064Am(aul = "b72deba8732ff9b8a9115b2c844d7318", aum = 2)

    /* renamed from: Vw */
    public C3892wO f2978Vw;
    @C0064Am(aul = "99b61c91970d65103aa07a6979165c59", aum = 3)

    /* renamed from: Vy */
    public C3892wO f2979Vy;
    @C0064Am(aul = "869681cf5cc92868951b78d04a3ddb02", aum = -2)

    /* renamed from: YA */
    public long f2980YA;
    @C0064Am(aul = "0c7e175450ec2e11a930b9b02c2c237b", aum = -2)

    /* renamed from: YB */
    public long f2981YB;
    @C0064Am(aul = "5c77a98a6c65cd19bd5fe125d161d3f1", aum = -1)

    /* renamed from: YC */
    public byte f2982YC;
    @C0064Am(aul = "0c2631fed5b4bf4a405b46963662db11", aum = -1)

    /* renamed from: YD */
    public byte f2983YD;
    @C0064Am(aul = "b72deba8732ff9b8a9115b2c844d7318", aum = -1)

    /* renamed from: YE */
    public byte f2984YE;
    @C0064Am(aul = "99b61c91970d65103aa07a6979165c59", aum = -1)

    /* renamed from: YF */
    public byte f2985YF;
    @C0064Am(aul = "869681cf5cc92868951b78d04a3ddb02", aum = -1)

    /* renamed from: YG */
    public byte f2986YG;
    @C0064Am(aul = "0c7e175450ec2e11a930b9b02c2c237b", aum = -1)

    /* renamed from: YH */
    public byte f2987YH;
    @C0064Am(aul = "869681cf5cc92868951b78d04a3ddb02", aum = 4)

    /* renamed from: Yu */
    public C3892wO f2988Yu;
    @C0064Am(aul = "0c7e175450ec2e11a930b9b02c2c237b", aum = 5)

    /* renamed from: Yv */
    public C3892wO f2989Yv;
    @C0064Am(aul = "5c77a98a6c65cd19bd5fe125d161d3f1", aum = -2)

    /* renamed from: Yw */
    public long f2990Yw;
    @C0064Am(aul = "0c2631fed5b4bf4a405b46963662db11", aum = -2)

    /* renamed from: Yx */
    public long f2991Yx;
    @C0064Am(aul = "b72deba8732ff9b8a9115b2c844d7318", aum = -2)

    /* renamed from: Yy */
    public long f2992Yy;
    @C0064Am(aul = "99b61c91970d65103aa07a6979165c59", aum = -2)

    /* renamed from: Yz */
    public long f2993Yz;
    @C0064Am(aul = "0b24a4526a3f99fd0907b005ef2ae7b7", aum = 6)
    public boolean bEl;
    @C0064Am(aul = "0b24a4526a3f99fd0907b005ef2ae7b7", aum = -2)
    public long cUh;
    @C0064Am(aul = "0b24a4526a3f99fd0907b005ef2ae7b7", aum = -1)
    public byte cUj;

    public C5381aHd() {
    }

    public C5381aHd(C1616Xf xf) {
        super(xf);
    }

    /* renamed from: c */
    public C5663aRz[] mo309c() {
        return CruiseSpeedBuffType._m_fields;
    }

    /* renamed from: d */
    public void mo310d() {
        super.mo310d();
        this.f2976Vs = null;
        this.f2977Vu = null;
        this.f2978Vw = null;
        this.f2979Vy = null;
        this.f2988Yu = null;
        this.f2989Yv = null;
        this.bEl = false;
        this.f2990Yw = 0;
        this.f2991Yx = 0;
        this.f2992Yy = 0;
        this.f2993Yz = 0;
        this.f2980YA = 0;
        this.f2981YB = 0;
        this.cUh = 0;
        this.f2982YC = 0;
        this.f2983YD = 0;
        this.f2984YE = 0;
        this.f2985YF = 0;
        this.f2986YG = 0;
        this.f2987YH = 0;
        this.cUj = 0;
    }
}
