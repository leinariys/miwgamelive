package logic.data.mbean;

import game.network.message.serializable.C3438ra;
import game.script.Character;
import game.script.mission.scripting.AssaultMissionScript;
import game.script.missiontemplate.WaypointDat;
import game.script.missiontemplate.scripting.AssaultMissionWave;
import game.script.npc.NPC;
import logic.baa.C1616Xf;
import logic.res.code.C5663aRz;
import p001a.C0064Am;

/* renamed from: a.KD */
public class C0707KD extends C2955mD {
    @C0064Am(aul = "f8461afc84a28c2622183de0cde1a2e1", aum = 7)
    public AssaultMissionScript bHH;
    @C0064Am(aul = "3a35338d9b10916fceeb4fd867c1bb25", aum = 0)
    public NPC bHI;
    @C0064Am(aul = "6ee3ffc95483e9e3ce52fe4391a6493d", aum = 1)
    public WaypointDat bHK;
    @C0064Am(aul = "f375cb99377664139a51883a0f751317", aum = 2)
    public AssaultMissionWave bHM;
    @C0064Am(aul = "68077e262a47266a471a09c7e2e5b471", aum = 3)
    public C3438ra<Character> bHO;
    @C0064Am(aul = "ee34b160aa4f33e5ea6ede4194c400cb", aum = 4)
    public C3438ra<Character> bHQ;
    @C0064Am(aul = "d05df105b03ddd860c3a01e47413a738", aum = 5)
    public C3438ra<Character> bHS;
    @C0064Am(aul = "51a2dd48d530994e3ce5d7d3d200d9ca", aum = 6)
    public C3438ra<Character> bHU;
    @C0064Am(aul = "f8461afc84a28c2622183de0cde1a2e1", aum = -2)

    /* renamed from: dl */
    public long f932dl;
    @C0064Am(aul = "3a35338d9b10916fceeb4fd867c1bb25", aum = -2)
    public long dqj;
    @C0064Am(aul = "6ee3ffc95483e9e3ce52fe4391a6493d", aum = -2)
    public long dqk;
    @C0064Am(aul = "f375cb99377664139a51883a0f751317", aum = -2)
    public long dql;
    @C0064Am(aul = "68077e262a47266a471a09c7e2e5b471", aum = -2)
    public long dqm;
    @C0064Am(aul = "ee34b160aa4f33e5ea6ede4194c400cb", aum = -2)
    public long dqn;
    @C0064Am(aul = "d05df105b03ddd860c3a01e47413a738", aum = -2)
    public long dqo;
    @C0064Am(aul = "51a2dd48d530994e3ce5d7d3d200d9ca", aum = -2)
    public long dqp;
    @C0064Am(aul = "3a35338d9b10916fceeb4fd867c1bb25", aum = -1)
    public byte dqq;
    @C0064Am(aul = "6ee3ffc95483e9e3ce52fe4391a6493d", aum = -1)
    public byte dqr;
    @C0064Am(aul = "f375cb99377664139a51883a0f751317", aum = -1)
    public byte dqs;
    @C0064Am(aul = "68077e262a47266a471a09c7e2e5b471", aum = -1)
    public byte dqt;
    @C0064Am(aul = "ee34b160aa4f33e5ea6ede4194c400cb", aum = -1)
    public byte dqu;
    @C0064Am(aul = "d05df105b03ddd860c3a01e47413a738", aum = -1)
    public byte dqv;
    @C0064Am(aul = "51a2dd48d530994e3ce5d7d3d200d9ca", aum = -1)
    public byte dqw;
    @C0064Am(aul = "f8461afc84a28c2622183de0cde1a2e1", aum = -1)

    /* renamed from: ds */
    public byte f933ds;

    public C0707KD() {
    }

    public C0707KD(C1616Xf xf) {
        super(xf);
    }

    /* renamed from: c */
    public C5663aRz[] mo309c() {
        return AssaultMissionScript.StateC._m_fields;
    }

    /* renamed from: d */
    public void mo310d() {
        super.mo310d();
        this.bHI = null;
        this.bHK = null;
        this.bHM = null;
        this.bHO = null;
        this.bHQ = null;
        this.bHS = null;
        this.bHU = null;
        this.bHH = null;
        this.dqj = 0;
        this.dqk = 0;
        this.dql = 0;
        this.dqm = 0;
        this.dqn = 0;
        this.dqo = 0;
        this.dqp = 0;
        this.f932dl = 0;
        this.dqq = 0;
        this.dqr = 0;
        this.dqs = 0;
        this.dqt = 0;
        this.dqu = 0;
        this.dqv = 0;
        this.dqw = 0;
        this.f933ds = 0;
    }
}
