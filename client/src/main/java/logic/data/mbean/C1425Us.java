package logic.data.mbean;

import game.network.message.serializable.C3438ra;
import game.script.missiontemplate.WaypointDat;
import game.script.missiontemplate.scripting.PatrolMissionScriptTemplate;
import game.script.space.StellarSystem;
import logic.baa.C1616Xf;
import logic.res.code.C5663aRz;
import p001a.C0064Am;

/* renamed from: a.Us */
public class C1425Us extends C0597IR {
    @C0064Am(aul = "16c626cc7a70a52f36339a4495408f70", aum = 1)
    public C3438ra<WaypointDat> aBp;
    @C0064Am(aul = "7904f42eb5a10c85b57d7e6c18f0461a", aum = 2)
    public boolean aFS;
    @C0064Am(aul = "adc8cf7dc1542e6552e257e6d855d187", aum = 3)
    public boolean aFU;
    @C0064Am(aul = "33824a6539c46ecba93e1145223d5a48", aum = 4)
    public int aFW;
    @C0064Am(aul = "16c626cc7a70a52f36339a4495408f70", aum = -2)
    public long dhN;
    @C0064Am(aul = "16c626cc7a70a52f36339a4495408f70", aum = -1)
    public byte dhS;
    @C0064Am(aul = "7904f42eb5a10c85b57d7e6c18f0461a", aum = -2)
    public long enT;
    @C0064Am(aul = "adc8cf7dc1542e6552e257e6d855d187", aum = -2)
    public long enU;
    @C0064Am(aul = "33824a6539c46ecba93e1145223d5a48", aum = -2)
    public long enV;
    @C0064Am(aul = "7904f42eb5a10c85b57d7e6c18f0461a", aum = -1)
    public byte enW;
    @C0064Am(aul = "adc8cf7dc1542e6552e257e6d855d187", aum = -1)
    public byte enX;
    @C0064Am(aul = "33824a6539c46ecba93e1145223d5a48", aum = -1)
    public byte enY;
    @C0064Am(aul = "0673405582bd29f0bad90d5e1da64068", aum = 0)

    /* renamed from: rI */
    public StellarSystem f1823rI;
    @C0064Am(aul = "0673405582bd29f0bad90d5e1da64068", aum = -2)

    /* renamed from: rY */
    public long f1824rY;
    @C0064Am(aul = "0673405582bd29f0bad90d5e1da64068", aum = -1)

    /* renamed from: so */
    public byte f1825so;

    public C1425Us() {
    }

    public C1425Us(C1616Xf xf) {
        super(xf);
    }

    /* renamed from: c */
    public C5663aRz[] mo309c() {
        return PatrolMissionScriptTemplate._m_fields;
    }

    /* renamed from: d */
    public void mo310d() {
        super.mo310d();
        this.f1823rI = null;
        this.aBp = null;
        this.aFS = false;
        this.aFU = false;
        this.aFW = 0;
        this.f1824rY = 0;
        this.dhN = 0;
        this.enT = 0;
        this.enU = 0;
        this.enV = 0;
        this.f1825so = 0;
        this.dhS = 0;
        this.enW = 0;
        this.enX = 0;
        this.enY = 0;
    }
}
