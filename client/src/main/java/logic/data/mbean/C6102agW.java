package logic.data.mbean;

import game.script.ship.hazardshield.HazardShield;
import logic.baa.C1616Xf;
import logic.res.code.C5663aRz;
import p001a.C0064Am;

/* renamed from: a.agW  reason: case insensitive filesystem */
public class C6102agW extends C2090bu {
    @C0064Am(aul = "9676201a3691aa0869b6cb0d1a350e37", aum = -2)

    /* renamed from: dl */
    public long f4519dl;
    @C0064Am(aul = "9676201a3691aa0869b6cb0d1a350e37", aum = -1)

    /* renamed from: ds */
    public byte f4520ds;
    @C0064Am(aul = "9676201a3691aa0869b6cb0d1a350e37", aum = 0)

    /* renamed from: pZ */
    public HazardShield f4521pZ;

    public C6102agW() {
    }

    public C6102agW(C1616Xf xf) {
        super(xf);
    }

    /* renamed from: c */
    public C5663aRz[] mo309c() {
        return HazardShield.BaseReconModeAdapter._m_fields;
    }

    /* renamed from: d */
    public void mo310d() {
        super.mo310d();
        this.f4521pZ = null;
        this.f4519dl = 0;
        this.f4520ds = 0;
    }
}
