package logic.data.mbean;

import game.script.crontab.CrontabRegister;
import logic.baa.C1616Xf;
import logic.res.code.C5663aRz;
import p001a.C0064Am;

/* renamed from: a.auW  reason: case insensitive filesystem */
public class C6830auW extends C5292aDs {
    @C0064Am(aul = "3e43535b4b2fe65f4ae2800994e6e739", aum = 0)
    public Integer ekb;
    @C0064Am(aul = "d968413caec1634849f861351b237efa", aum = 1)
    public Integer ekd;
    @C0064Am(aul = "34f4c299d77af1c9561ac7035aacaecb", aum = 2)
    public Integer ekf;
    @C0064Am(aul = "7dbf4f4509d3521503873891cfb3cdbb", aum = 3)
    public Integer ekh;
    @C0064Am(aul = "bcd00800a591a827d12aa24345f1de49", aum = 4)
    public boolean[] ekj;
    @C0064Am(aul = "bcd00800a591a827d12aa24345f1de49", aum = -1)
    public byte gFA;
    @C0064Am(aul = "3e43535b4b2fe65f4ae2800994e6e739", aum = -2)
    public long gFr;
    @C0064Am(aul = "d968413caec1634849f861351b237efa", aum = -2)
    public long gFs;
    @C0064Am(aul = "34f4c299d77af1c9561ac7035aacaecb", aum = -2)
    public long gFt;
    @C0064Am(aul = "7dbf4f4509d3521503873891cfb3cdbb", aum = -2)
    public long gFu;
    @C0064Am(aul = "bcd00800a591a827d12aa24345f1de49", aum = -2)
    public long gFv;
    @C0064Am(aul = "3e43535b4b2fe65f4ae2800994e6e739", aum = -1)
    public byte gFw;
    @C0064Am(aul = "d968413caec1634849f861351b237efa", aum = -1)
    public byte gFx;
    @C0064Am(aul = "34f4c299d77af1c9561ac7035aacaecb", aum = -1)
    public byte gFy;
    @C0064Am(aul = "7dbf4f4509d3521503873891cfb3cdbb", aum = -1)
    public byte gFz;
    @C0064Am(aul = "56fd1f758f1126a1f91f107880fbfc47", aum = 5)
    public String handle;
    @C0064Am(aul = "56fd1f758f1126a1f91f107880fbfc47", aum = -2)

    /* renamed from: ok */
    public long f5397ok;
    @C0064Am(aul = "56fd1f758f1126a1f91f107880fbfc47", aum = -1)

    /* renamed from: on */
    public byte f5398on;

    public C6830auW() {
    }

    public C6830auW(C1616Xf xf) {
        super(xf);
    }

    /* renamed from: c */
    public C5663aRz[] mo309c() {
        return CrontabRegister._m_fields;
    }

    /* renamed from: d */
    public void mo310d() {
        super.mo310d();
        this.ekb = null;
        this.ekd = null;
        this.ekf = null;
        this.ekh = null;
        this.ekj = null;
        this.handle = null;
        this.gFr = 0;
        this.gFs = 0;
        this.gFt = 0;
        this.gFu = 0;
        this.gFv = 0;
        this.f5397ok = 0;
        this.gFw = 0;
        this.gFx = 0;
        this.gFy = 0;
        this.gFz = 0;
        this.gFA = 0;
        this.f5398on = 0;
    }
}
