package logic.data.mbean;

import com.hoplon.geometry.Vec3f;
import game.script.AttachedTrigger;
import logic.baa.C1616Xf;
import logic.res.code.C5663aRz;
import p001a.C0064Am;

/* renamed from: a.ahZ  reason: case insensitive filesystem */
public class C6157ahZ extends C6151ahT {
    @C0064Am(aul = "0ec532cff956541cc44b1c68368cd2a5", aum = 0)
    public Vec3f bwy;
    @C0064Am(aul = "0ec532cff956541cc44b1c68368cd2a5", aum = -2)
    public long fMD;
    @C0064Am(aul = "0ec532cff956541cc44b1c68368cd2a5", aum = -1)
    public byte fME;

    public C6157ahZ() {
    }

    public C6157ahZ(C1616Xf xf) {
        super(xf);
    }

    /* renamed from: c */
    public C5663aRz[] mo309c() {
        return AttachedTrigger._m_fields;
    }

    /* renamed from: d */
    public void mo310d() {
        super.mo310d();
        this.bwy = null;
        this.fMD = 0;
        this.fME = 0;
    }
}
