package logic.data.mbean;

import game.script.item.Weapon;
import game.script.ship.strike.Strike;
import logic.baa.C1616Xf;
import logic.res.code.C5663aRz;
import p001a.C0064Am;

/* renamed from: a.aEe  reason: case insensitive filesystem */
public class C5304aEe extends C3976xd {
    @C0064Am(aul = "a3cf0a6afb75136526f746f62fd69487", aum = 1)

    /* renamed from: AH */
    public Strike f2741AH;
    @C0064Am(aul = "a3cf0a6afb75136526f746f62fd69487", aum = -2)

    /* renamed from: dl */
    public long f2742dl;
    @C0064Am(aul = "a3cf0a6afb75136526f746f62fd69487", aum = -1)

    /* renamed from: ds */
    public byte f2743ds;
    @C0064Am(aul = "c649767e5eaeb21311f3f9a1bd0a6ca9", aum = -2)
    public long fPi;
    @C0064Am(aul = "c649767e5eaeb21311f3f9a1bd0a6ca9", aum = -1)
    public byte fPp;
    @C0064Am(aul = "c649767e5eaeb21311f3f9a1bd0a6ca9", aum = 0)

    /* renamed from: oX */
    public Weapon f2744oX;

    public C5304aEe() {
    }

    public C5304aEe(C1616Xf xf) {
        super(xf);
    }

    /* renamed from: c */
    public C5663aRz[] mo309c() {
        return Strike.StrikeDamageAdapter._m_fields;
    }

    /* renamed from: d */
    public void mo310d() {
        super.mo310d();
        this.f2744oX = null;
        this.f2741AH = null;
        this.fPi = 0;
        this.f2742dl = 0;
        this.fPp = 0;
        this.f2743ds = 0;
    }
}
