package logic.data.mbean;

import game.network.message.serializable.C3438ra;
import game.script.avatar.AvatarSector;
import game.script.avatar.body.BodyPiece;
import game.script.avatar.body.BodyPieceBank;
import logic.baa.C1616Xf;
import logic.res.code.C5663aRz;
import p001a.C0064Am;

import java.util.UUID;

/* renamed from: a.xR */
public class C3964xR extends C5292aDs {
    @C0064Am(aul = "0fc7a5f86a8eca3f8ea1ab791e0f5be4", aum = 0)
    public String bIf;
    @C0064Am(aul = "9c7a73cd3171cd461f3baa0b5f899497", aum = 1)
    public AvatarSector bIg;
    @C0064Am(aul = "8d2cdc8e2c7ffdc13e0a0c128824bba3", aum = 2)
    public C3438ra<BodyPiece> bIh;
    @C0064Am(aul = "0fc7a5f86a8eca3f8ea1ab791e0f5be4", aum = -2)
    public long bIi;
    @C0064Am(aul = "9c7a73cd3171cd461f3baa0b5f899497", aum = -2)
    public long bIj;
    @C0064Am(aul = "8d2cdc8e2c7ffdc13e0a0c128824bba3", aum = -2)
    public long bIk;
    @C0064Am(aul = "0fc7a5f86a8eca3f8ea1ab791e0f5be4", aum = -1)
    public byte bIl;
    @C0064Am(aul = "9c7a73cd3171cd461f3baa0b5f899497", aum = -1)
    public byte bIm;
    @C0064Am(aul = "8d2cdc8e2c7ffdc13e0a0c128824bba3", aum = -1)
    public byte bIn;
    @C0064Am(aul = "4a89a98bbf4d973d42664aa7acf63f2c", aum = 3)

    /* renamed from: bK */
    public UUID f9535bK;
    @C0064Am(aul = "bbd8bdaf75b704d8d5a69d2d7dbe0ef2", aum = 4)
    public String handle;
    @C0064Am(aul = "4a89a98bbf4d973d42664aa7acf63f2c", aum = -2)

    /* renamed from: oL */
    public long f9536oL;
    @C0064Am(aul = "4a89a98bbf4d973d42664aa7acf63f2c", aum = -1)

    /* renamed from: oS */
    public byte f9537oS;
    @C0064Am(aul = "bbd8bdaf75b704d8d5a69d2d7dbe0ef2", aum = -2)

    /* renamed from: ok */
    public long f9538ok;
    @C0064Am(aul = "bbd8bdaf75b704d8d5a69d2d7dbe0ef2", aum = -1)

    /* renamed from: on */
    public byte f9539on;

    public C3964xR() {
    }

    public C3964xR(C1616Xf xf) {
        super(xf);
    }

    /* renamed from: c */
    public C5663aRz[] mo309c() {
        return BodyPieceBank._m_fields;
    }

    /* renamed from: d */
    public void mo310d() {
        super.mo310d();
        this.bIf = null;
        this.bIg = null;
        this.bIh = null;
        this.f9535bK = null;
        this.handle = null;
        this.bIi = 0;
        this.bIj = 0;
        this.bIk = 0;
        this.f9536oL = 0;
        this.f9538ok = 0;
        this.bIl = 0;
        this.bIm = 0;
        this.bIn = 0;
        this.f9537oS = 0;
        this.f9539on = 0;
    }
}
