package logic.data.mbean;

import game.network.message.serializable.C1556Wo;
import game.script.citizenship.inprovements.StorageImprovement;
import game.script.storage.Storage;
import logic.baa.C1616Xf;
import logic.res.code.C5663aRz;
import p001a.C0064Am;
import p001a.C3892wO;

/* renamed from: a.aqD  reason: case insensitive filesystem */
public class C6603aqD extends C2057bg {
    @C0064Am(aul = "65b6269dfa5ab950658701bf55d5dedb", aum = 0)

    /* renamed from: DL */
    public C3892wO f5148DL;
    @C0064Am(aul = "65b6269dfa5ab950658701bf55d5dedb", aum = -2)

    /* renamed from: DM */
    public long f5149DM;
    @C0064Am(aul = "65b6269dfa5ab950658701bf55d5dedb", aum = -1)

    /* renamed from: DN */
    public byte f5150DN;
    @C0064Am(aul = "ffe062dbe5c495564985cf8e8b7dee1e", aum = 1)
    public C1556Wo<Storage, StorageImprovement.StorageAmplifierAdapter> bLl;
    @C0064Am(aul = "ffe062dbe5c495564985cf8e8b7dee1e", aum = -2)
    public long gqN;
    @C0064Am(aul = "ffe062dbe5c495564985cf8e8b7dee1e", aum = -1)
    public byte gqO;

    public C6603aqD() {
    }

    public C6603aqD(C1616Xf xf) {
        super(xf);
    }

    /* renamed from: c */
    public C5663aRz[] mo309c() {
        return StorageImprovement._m_fields;
    }

    /* renamed from: d */
    public void mo310d() {
        super.mo310d();
        this.f5148DL = null;
        this.bLl = null;
        this.f5149DM = 0;
        this.gqN = 0;
        this.f5150DN = 0;
        this.gqO = 0;
    }
}
