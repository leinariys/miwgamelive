package logic.data.mbean;

import game.script.mission.MissionTemplate;
import game.script.npcchat.requirement.ActiveMissionSpeechRequirement;
import logic.baa.C1616Xf;
import logic.res.code.C5663aRz;
import p001a.C0064Am;

/* renamed from: a.Cr */
public class C0227Cr extends C5416aIm {
    @C0064Am(aul = "317605bcec70c23a1c31a214c76511ee", aum = 0)
    public MissionTemplate baM;
    @C0064Am(aul = "317605bcec70c23a1c31a214c76511ee", aum = -2)
    public long baN;
    @C0064Am(aul = "317605bcec70c23a1c31a214c76511ee", aum = -1)
    public byte baO;

    public C0227Cr() {
    }

    public C0227Cr(C1616Xf xf) {
        super(xf);
    }

    /* renamed from: c */
    public C5663aRz[] mo309c() {
        return ActiveMissionSpeechRequirement._m_fields;
    }

    /* renamed from: d */
    public void mo310d() {
        super.mo310d();
        this.baM = null;
        this.baN = 0;
        this.baO = 0;
    }
}
