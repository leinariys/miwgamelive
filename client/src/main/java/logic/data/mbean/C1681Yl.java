package logic.data.mbean;

import game.network.message.serializable.C2686iZ;
import game.network.message.serializable.C3438ra;
import game.script.hazardarea.HazardArea.HazardArea;
import game.script.hazardarea.HazardArea.HollowActor;
import game.script.item.buff.module.AttributeBuff;
import game.script.item.buff.module.AttributeBuffType;
import game.script.resource.Asset;
import game.script.ship.Ship;
import logic.baa.C1616Xf;
import logic.res.code.C5663aRz;
import p001a.C0064Am;

import java.util.UUID;

/* renamed from: a.Yl */
public class C1681Yl extends C6151ahT {
    @C0064Am(aul = "283b3ec4d1042f5edafc4253310ea623", aum = -2)
    public long aIu;
    @C0064Am(aul = "283b3ec4d1042f5edafc4253310ea623", aum = -1)
    public byte aIx;
    @C0064Am(aul = "df4d4728f2767e02cb1f260dcc3e36c5", aum = 0)
    public C3438ra<AttributeBuffType> aQT;
    @C0064Am(aul = "e5b2198e45ec605c23c9a4f83dc4764f", aum = 1)
    public int aQV;
    @C0064Am(aul = "a3ad329c446686366b1e8bbdb406df92", aum = 2)
    public float aQX;
    @C0064Am(aul = "5c0deb1aa2adfa1149549deb09d4009b", aum = 3)
    public Asset aQZ;
    @C0064Am(aul = "dbf99865d1817dfc89ec895f4d837713", aum = 5)
    public C2686iZ<Ship> aRb;
    @C0064Am(aul = "6338404582f004b69a04326664e2f58c", aum = 7)
    public HollowActor aRd;
    @C0064Am(aul = "66f40d5cb05b0b128a6e08a1478bd6b9", aum = 8)
    public C3438ra<AttributeBuff> aRf;
    @C0064Am(aul = "283b3ec4d1042f5edafc4253310ea623", aum = 6)
    public boolean active;
    @C0064Am(aul = "c6fc975d0f6bd134f33ffbdd59b13385", aum = 9)

    /* renamed from: bK */
    public UUID f2218bK;
    @C0064Am(aul = "dbf99865d1817dfc89ec895f4d837713", aum = -2)
    public long eLA;
    @C0064Am(aul = "6338404582f004b69a04326664e2f58c", aum = -2)
    public long eLB;
    @C0064Am(aul = "66f40d5cb05b0b128a6e08a1478bd6b9", aum = -2)
    public long eLC;
    @C0064Am(aul = "df4d4728f2767e02cb1f260dcc3e36c5", aum = -1)
    public byte eLD;
    @C0064Am(aul = "e5b2198e45ec605c23c9a4f83dc4764f", aum = -1)
    public byte eLE;
    @C0064Am(aul = "a3ad329c446686366b1e8bbdb406df92", aum = -1)
    public byte eLF;
    @C0064Am(aul = "5c0deb1aa2adfa1149549deb09d4009b", aum = -1)
    public byte eLG;
    @C0064Am(aul = "dbf99865d1817dfc89ec895f4d837713", aum = -1)
    public byte eLH;
    @C0064Am(aul = "6338404582f004b69a04326664e2f58c", aum = -1)
    public byte eLI;
    @C0064Am(aul = "66f40d5cb05b0b128a6e08a1478bd6b9", aum = -1)
    public byte eLJ;
    @C0064Am(aul = "df4d4728f2767e02cb1f260dcc3e36c5", aum = -2)
    public long eLw;
    @C0064Am(aul = "e5b2198e45ec605c23c9a4f83dc4764f", aum = -2)
    public long eLx;
    @C0064Am(aul = "a3ad329c446686366b1e8bbdb406df92", aum = -2)
    public long eLy;
    @C0064Am(aul = "5c0deb1aa2adfa1149549deb09d4009b", aum = -2)
    public long eLz;
    @C0064Am(aul = "dbab4d71a42e309f0acd2ebba877373d", aum = 4)
    public String handle;
    @C0064Am(aul = "c6fc975d0f6bd134f33ffbdd59b13385", aum = -2)

    /* renamed from: oL */
    public long f2219oL;
    @C0064Am(aul = "c6fc975d0f6bd134f33ffbdd59b13385", aum = -1)

    /* renamed from: oS */
    public byte f2220oS;
    @C0064Am(aul = "dbab4d71a42e309f0acd2ebba877373d", aum = -2)

    /* renamed from: ok */
    public long f2221ok;
    @C0064Am(aul = "dbab4d71a42e309f0acd2ebba877373d", aum = -1)

    /* renamed from: on */
    public byte f2222on;

    public C1681Yl() {
    }

    public C1681Yl(C1616Xf xf) {
        super(xf);
    }

    /* renamed from: c */
    public C5663aRz[] mo309c() {
        return HazardArea._m_fields;
    }

    /* renamed from: d */
    public void mo310d() {
        super.mo310d();
        this.aQT = null;
        this.aQV = 0;
        this.aQX = 0.0f;
        this.aQZ = null;
        this.handle = null;
        this.aRb = null;
        this.active = false;
        this.aRd = null;
        this.aRf = null;
        this.f2218bK = null;
        this.eLw = 0;
        this.eLx = 0;
        this.eLy = 0;
        this.eLz = 0;
        this.f2221ok = 0;
        this.eLA = 0;
        this.aIu = 0;
        this.eLB = 0;
        this.eLC = 0;
        this.f2219oL = 0;
        this.eLD = 0;
        this.eLE = 0;
        this.eLF = 0;
        this.eLG = 0;
        this.f2222on = 0;
        this.eLH = 0;
        this.aIx = 0;
        this.eLI = 0;
        this.eLJ = 0;
        this.f2220oS = 0;
    }
}
