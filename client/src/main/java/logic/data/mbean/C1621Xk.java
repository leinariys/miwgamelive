package logic.data.mbean;

import game.script.mission.scripting.I18NStringTableEntry;
import logic.baa.C1616Xf;
import logic.res.code.C5663aRz;
import p001a.C0064Am;
import taikodom.infra.script.I18NString;

import java.util.UUID;

/* renamed from: a.Xk */
public class C1621Xk extends C5292aDs {
    @C0064Am(aul = "a773798ee452130c0bf17b61af44b057", aum = -2)

    /* renamed from: IC */
    public long f2124IC;
    @C0064Am(aul = "31c90e18e15521e4c4a7c4c72fdf5228", aum = -2)

    /* renamed from: ID */
    public long f2125ID;
    @C0064Am(aul = "a773798ee452130c0bf17b61af44b057", aum = -1)

    /* renamed from: IE */
    public byte f2126IE;
    @C0064Am(aul = "31c90e18e15521e4c4a7c4c72fdf5228", aum = -1)

    /* renamed from: IG */
    public byte f2127IG;
    @C0064Am(aul = "b80316dc2f1980b967b96df354a289e4", aum = 2)

    /* renamed from: bK */
    public UUID f2128bK;
    @C0064Am(aul = "31c90e18e15521e4c4a7c4c72fdf5228", aum = 1)
    public I18NString eIv;
    @C0064Am(aul = "5c67dcfb5394145ebbe9fcb3a22ef218", aum = 3)
    public String handle;
    @C0064Am(aul = "a773798ee452130c0bf17b61af44b057", aum = 0)
    public String key;
    @C0064Am(aul = "b80316dc2f1980b967b96df354a289e4", aum = -2)

    /* renamed from: oL */
    public long f2129oL;
    @C0064Am(aul = "b80316dc2f1980b967b96df354a289e4", aum = -1)

    /* renamed from: oS */
    public byte f2130oS;
    @C0064Am(aul = "5c67dcfb5394145ebbe9fcb3a22ef218", aum = -2)

    /* renamed from: ok */
    public long f2131ok;
    @C0064Am(aul = "5c67dcfb5394145ebbe9fcb3a22ef218", aum = -1)

    /* renamed from: on */
    public byte f2132on;

    public C1621Xk() {
    }

    public C1621Xk(C1616Xf xf) {
        super(xf);
    }

    /* renamed from: c */
    public C5663aRz[] mo309c() {
        return I18NStringTableEntry._m_fields;
    }

    /* renamed from: d */
    public void mo310d() {
        super.mo310d();
        this.key = null;
        this.eIv = null;
        this.f2128bK = null;
        this.handle = null;
        this.f2124IC = 0;
        this.f2125ID = 0;
        this.f2129oL = 0;
        this.f2131ok = 0;
        this.f2126IE = 0;
        this.f2127IG = 0;
        this.f2130oS = 0;
        this.f2132on = 0;
    }
}
