package logic.data.mbean;

import logic.baa.C1616Xf;
import logic.res.code.C5663aRz;
import p001a.C0064Am;

import java.util.UUID;

/* renamed from: a.ou */
public class C3189ou extends C3805vD {
    @C0064Am(aul = "c4d0b9c325c92ea2fa331d3962edba17", aum = 1)
    public int aPA;
    @C0064Am(aul = "829c7fb36f3f92337dda5fa97a8b47b4", aum = -2)
    public long aPB;
    @C0064Am(aul = "c4d0b9c325c92ea2fa331d3962edba17", aum = -2)
    public long aPC;
    @C0064Am(aul = "829c7fb36f3f92337dda5fa97a8b47b4", aum = -1)
    public byte aPD;
    @C0064Am(aul = "c4d0b9c325c92ea2fa331d3962edba17", aum = -1)
    public byte aPE;
    @C0064Am(aul = "829c7fb36f3f92337dda5fa97a8b47b4", aum = 0)
    public C6544aow aPz;
    @C0064Am(aul = "d02fea9e6b1de7133d0c4ea2f51c68b1", aum = 2)

    /* renamed from: bK */
    public UUID f8795bK;
    @C0064Am(aul = "bfeccb3f18fb52457fe22a931ba61de7", aum = 3)
    public String handle;
    @C0064Am(aul = "d02fea9e6b1de7133d0c4ea2f51c68b1", aum = -2)

    /* renamed from: oL */
    public long f8796oL;
    @C0064Am(aul = "d02fea9e6b1de7133d0c4ea2f51c68b1", aum = -1)

    /* renamed from: oS */
    public byte f8797oS;
    @C0064Am(aul = "bfeccb3f18fb52457fe22a931ba61de7", aum = -2)

    /* renamed from: ok */
    public long f8798ok;
    @C0064Am(aul = "bfeccb3f18fb52457fe22a931ba61de7", aum = -1)

    /* renamed from: on */
    public byte f8799on;

    public C3189ou() {
    }

    public C3189ou(C1616Xf xf) {
        super(xf);
    }

    /* renamed from: c */
    public C5663aRz[] mo309c() {
        return C3315qE._m_fields;
    }

    /* renamed from: d */
    public void mo310d() {
        super.mo310d();
        this.aPz = null;
        this.aPA = 0;
        this.f8795bK = null;
        this.handle = null;
        this.aPB = 0;
        this.aPC = 0;
        this.f8796oL = 0;
        this.f8798ok = 0;
        this.aPD = 0;
        this.aPE = 0;
        this.f8797oS = 0;
        this.f8799on = 0;
    }
}
