package logic.data.mbean;

import game.script.item.buff.amplifier.SpeedBoostAmplifier;
import logic.baa.C1616Xf;
import logic.res.code.C5663aRz;
import p001a.C0064Am;

/* renamed from: a.AV */
public class C0038AV extends C1601XR {
    @C0064Am(aul = "c0802d594416fda03fe6fc138ff329d4", aum = 0)
    public SpeedBoostAmplifier btJ;
    @C0064Am(aul = "c0802d594416fda03fe6fc138ff329d4", aum = -2)

    /* renamed from: dl */
    public long f62dl;
    @C0064Am(aul = "c0802d594416fda03fe6fc138ff329d4", aum = -1)

    /* renamed from: ds */
    public byte f63ds;

    public C0038AV() {
    }

    public C0038AV(C1616Xf xf) {
        super(xf);
    }

    /* renamed from: c */
    public C5663aRz[] mo309c() {
        return SpeedBoostAmplifier.SpeedBoosAmplifierAdapter._m_fields;
    }

    /* renamed from: d */
    public void mo310d() {
        super.mo310d();
        this.btJ = null;
        this.f62dl = 0;
        this.f63ds = 0;
    }
}
