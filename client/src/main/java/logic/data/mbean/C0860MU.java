package logic.data.mbean;

import game.network.message.serializable.C1556Wo;
import game.script.item.ItemType;
import game.script.mission.scripting.ItemDeployMissionScript;
import logic.baa.C1616Xf;
import logic.res.code.C5663aRz;
import p001a.C0064Am;

/* renamed from: a.MU */
public class C0860MU extends aMY {
    @C0064Am(aul = "b0095fadbac0aebaf458babda6435727", aum = 0)
    public C1556Wo<ItemType, Integer> cyi;
    @C0064Am(aul = "dac7d27ee9a97def08890fbcf5928a9a", aum = 1)
    public C1556Wo<ItemType, Integer> cyk;
    @C0064Am(aul = "b0095fadbac0aebaf458babda6435727", aum = -2)
    public long dCT;
    @C0064Am(aul = "dac7d27ee9a97def08890fbcf5928a9a", aum = -2)
    public long dCU;
    @C0064Am(aul = "b0095fadbac0aebaf458babda6435727", aum = -1)
    public byte dCV;
    @C0064Am(aul = "dac7d27ee9a97def08890fbcf5928a9a", aum = -1)
    public byte dCW;

    public C0860MU() {
    }

    public C0860MU(C1616Xf xf) {
        super(xf);
    }

    /* renamed from: c */
    public C5663aRz[] mo309c() {
        return ItemDeployMissionScript._m_fields;
    }

    /* renamed from: d */
    public void mo310d() {
        super.mo310d();
        this.cyi = null;
        this.cyk = null;
        this.dCT = 0;
        this.dCU = 0;
        this.dCV = 0;
        this.dCW = 0;
    }
}
