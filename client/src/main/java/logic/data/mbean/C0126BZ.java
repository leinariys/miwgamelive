package logic.data.mbean;

import game.script.pda.DatabaseCategory;
import game.script.resource.Asset;
import logic.baa.C1616Xf;
import logic.res.code.C5663aRz;
import p001a.C0064Am;
import taikodom.infra.script.I18NString;

import java.util.UUID;

/* renamed from: a.BZ */
public class C0126BZ extends C3805vD {
    @C0064Am(aul = "2beec937081f09bdcfe49ff2e9051966", aum = 5)

    /* renamed from: bK */
    public UUID f213bK;
    @C0064Am(aul = "c8724ebfc99f7a89210a1c79fb6ae20a", aum = 3)
    public Asset cuI;
    @C0064Am(aul = "fc70f2c512117ed815c3f3cd86434294", aum = 4)
    public boolean cuJ;
    @C0064Am(aul = "c8724ebfc99f7a89210a1c79fb6ae20a", aum = -2)
    public long cuK;
    @C0064Am(aul = "fc70f2c512117ed815c3f3cd86434294", aum = -2)
    public long cuL;
    @C0064Am(aul = "c8724ebfc99f7a89210a1c79fb6ae20a", aum = -1)
    public byte cuM;
    @C0064Am(aul = "fc70f2c512117ed815c3f3cd86434294", aum = -1)
    public byte cuN;
    @C0064Am(aul = "88d8a5c18c85135447261c492ac82ad2", aum = 0)
    public String handle;
    @C0064Am(aul = "03fa34162e8f90d4035dc238cec23504", aum = 2)

    /* renamed from: nh */
    public I18NString f214nh;
    @C0064Am(aul = "6778fe618cfd90c9603e8e3911136a24", aum = 1)

    /* renamed from: ni */
    public I18NString f215ni;
    @C0064Am(aul = "03fa34162e8f90d4035dc238cec23504", aum = -2)

    /* renamed from: nk */
    public long f216nk;
    @C0064Am(aul = "6778fe618cfd90c9603e8e3911136a24", aum = -2)

    /* renamed from: nl */
    public long f217nl;
    @C0064Am(aul = "03fa34162e8f90d4035dc238cec23504", aum = -1)

    /* renamed from: nn */
    public byte f218nn;
    @C0064Am(aul = "6778fe618cfd90c9603e8e3911136a24", aum = -1)

    /* renamed from: no */
    public byte f219no;
    @C0064Am(aul = "2beec937081f09bdcfe49ff2e9051966", aum = -2)

    /* renamed from: oL */
    public long f220oL;
    @C0064Am(aul = "2beec937081f09bdcfe49ff2e9051966", aum = -1)

    /* renamed from: oS */
    public byte f221oS;
    @C0064Am(aul = "88d8a5c18c85135447261c492ac82ad2", aum = -2)

    /* renamed from: ok */
    public long f222ok;
    @C0064Am(aul = "88d8a5c18c85135447261c492ac82ad2", aum = -1)

    /* renamed from: on */
    public byte f223on;

    public C0126BZ() {
    }

    public C0126BZ(C1616Xf xf) {
        super(xf);
    }

    /* renamed from: c */
    public C5663aRz[] mo309c() {
        return DatabaseCategory._m_fields;
    }

    /* renamed from: d */
    public void mo310d() {
        super.mo310d();
        this.handle = null;
        this.f215ni = null;
        this.f214nh = null;
        this.cuI = null;
        this.cuJ = false;
        this.f213bK = null;
        this.f222ok = 0;
        this.f217nl = 0;
        this.f216nk = 0;
        this.cuK = 0;
        this.cuL = 0;
        this.f220oL = 0;
        this.f223on = 0;
        this.f219no = 0;
        this.f218nn = 0;
        this.cuM = 0;
        this.cuN = 0;
        this.f221oS = 0;
    }
}
