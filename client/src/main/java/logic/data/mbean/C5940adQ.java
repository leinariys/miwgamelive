package logic.data.mbean;

import game.script.nls.NLSCloning;
import logic.baa.C1616Xf;
import logic.res.code.C5663aRz;
import p001a.C0064Am;
import taikodom.infra.script.I18NString;

/* renamed from: a.adQ  reason: case insensitive filesystem */
public class C5940adQ extends C2484fi {
    @C0064Am(aul = "02885ef25959b974f7d041d3a692d13c", aum = -2)
    public long flA;
    @C0064Am(aul = "46c343911f9d2ed0bbc05d1b95abd203", aum = -2)
    public long flB;
    @C0064Am(aul = "6acfaf259d561570c28b9113799b338a", aum = -1)
    public byte flC;
    @C0064Am(aul = "02885ef25959b974f7d041d3a692d13c", aum = -1)
    public byte flD;
    @C0064Am(aul = "46c343911f9d2ed0bbc05d1b95abd203", aum = -1)
    public byte flE;
    @C0064Am(aul = "6acfaf259d561570c28b9113799b338a", aum = 0)
    public I18NString flw;
    @C0064Am(aul = "02885ef25959b974f7d041d3a692d13c", aum = 1)
    public I18NString flx;
    @C0064Am(aul = "46c343911f9d2ed0bbc05d1b95abd203", aum = 2)
    public I18NString fly;
    @C0064Am(aul = "6acfaf259d561570c28b9113799b338a", aum = -2)
    public long flz;

    public C5940adQ() {
    }

    public C5940adQ(C1616Xf xf) {
        super(xf);
    }

    /* renamed from: c */
    public C5663aRz[] mo309c() {
        return NLSCloning._m_fields;
    }

    /* renamed from: d */
    public void mo310d() {
        super.mo310d();
        this.flw = null;
        this.flx = null;
        this.fly = null;
        this.flz = 0;
        this.flA = 0;
        this.flB = 0;
        this.flC = 0;
        this.flD = 0;
        this.flE = 0;
    }
}
