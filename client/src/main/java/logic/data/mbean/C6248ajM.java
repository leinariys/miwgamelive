package logic.data.mbean;

import game.script.Actor;
import game.script.mission.scripting.ScriptableMission;
import game.script.ship.Ship;
import logic.baa.C1616Xf;
import logic.res.code.C5663aRz;
import p001a.C0064Am;

/* renamed from: a.ajM  reason: case insensitive filesystem */
public class C6248ajM extends C5292aDs {
    @C0064Am(aul = "684b312a5cb01c94456df3d60f6757a4", aum = 0)

    /* renamed from: QR */
    public Ship f4692QR;
    @C0064Am(aul = "1a5d0b8c6900235122da9e59c08eb0dc", aum = 1)

    /* renamed from: QT */
    public Actor f4693QT;
    @C0064Am(aul = "0518b7d19f3be250d5b9d3e27cc5a5ba", aum = 2)

    /* renamed from: QV */
    public float f4694QV;
    @C0064Am(aul = "8c58f0653e4231a56c5e1cec3474e234", aum = 3)

    /* renamed from: QX */
    public ScriptableMission f4695QX;
    @C0064Am(aul = "684b312a5cb01c94456df3d60f6757a4", aum = -2)
    public long dPm;
    @C0064Am(aul = "0518b7d19f3be250d5b9d3e27cc5a5ba", aum = -2)
    public long dPo;
    @C0064Am(aul = "684b312a5cb01c94456df3d60f6757a4", aum = -1)
    public byte dPp;
    @C0064Am(aul = "0518b7d19f3be250d5b9d3e27cc5a5ba", aum = -1)
    public byte dPr;
    @C0064Am(aul = "8c58f0653e4231a56c5e1cec3474e234", aum = -2)

    /* renamed from: dl */
    public long f4696dl;
    @C0064Am(aul = "8c58f0653e4231a56c5e1cec3474e234", aum = -1)

    /* renamed from: ds */
    public byte f4697ds;
    @C0064Am(aul = "1a5d0b8c6900235122da9e59c08eb0dc", aum = -2)
    public long fSk;
    @C0064Am(aul = "1a5d0b8c6900235122da9e59c08eb0dc", aum = -1)
    public byte fSl;

    public C6248ajM() {
    }

    public C6248ajM(C1616Xf xf) {
        super(xf);
    }

    /* renamed from: c */
    public C5663aRz[] mo309c() {
        return ScriptableMission.SelectionCallback._m_fields;
    }

    /* renamed from: d */
    public void mo310d() {
        super.mo310d();
        this.f4692QR = null;
        this.f4693QT = null;
        this.f4694QV = 0.0f;
        this.f4695QX = null;
        this.dPm = 0;
        this.fSk = 0;
        this.dPo = 0;
        this.f4696dl = 0;
        this.dPp = 0;
        this.fSl = 0;
        this.dPr = 0;
        this.f4697ds = 0;
    }
}
