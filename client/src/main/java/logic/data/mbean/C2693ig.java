package logic.data.mbean;

import game.script.spacezone.NPCZone;
import game.script.spacezone.PopulationControl;
import logic.baa.C1616Xf;
import logic.res.code.C5663aRz;
import p001a.C0064Am;

import java.util.UUID;

/* renamed from: a.ig */
public class C2693ig extends C3805vD {
    @C0064Am(aul = "bbbf7fe3a8a29bb493c36238952d79f1", aum = -2)

    /* renamed from: XA */
    public long f8231XA;
    @C0064Am(aul = "70aef6509f6b7746bc438be6b2793ef8", aum = -1)

    /* renamed from: XB */
    public byte f8232XB;
    @C0064Am(aul = "bbbf7fe3a8a29bb493c36238952d79f1", aum = -1)

    /* renamed from: XC */
    public byte f8233XC;
    @C0064Am(aul = "70aef6509f6b7746bc438be6b2793ef8", aum = 0)

    /* renamed from: Xx */
    public NPCZone f8234Xx;
    @C0064Am(aul = "bbbf7fe3a8a29bb493c36238952d79f1", aum = 1)

    /* renamed from: Xy */
    public float f8235Xy;
    @C0064Am(aul = "70aef6509f6b7746bc438be6b2793ef8", aum = -2)

    /* renamed from: Xz */
    public long f8236Xz;
    @C0064Am(aul = "58396f8d58b4166b7243dae52d54a1e2", aum = 2)

    /* renamed from: bK */
    public UUID f8237bK;
    @C0064Am(aul = "d2fddb763263c03e962b10d3a1a078f1", aum = 3)
    public String handle;
    @C0064Am(aul = "58396f8d58b4166b7243dae52d54a1e2", aum = -2)

    /* renamed from: oL */
    public long f8238oL;
    @C0064Am(aul = "58396f8d58b4166b7243dae52d54a1e2", aum = -1)

    /* renamed from: oS */
    public byte f8239oS;
    @C0064Am(aul = "d2fddb763263c03e962b10d3a1a078f1", aum = -2)

    /* renamed from: ok */
    public long f8240ok;
    @C0064Am(aul = "d2fddb763263c03e962b10d3a1a078f1", aum = -1)

    /* renamed from: on */
    public byte f8241on;

    public C2693ig() {
    }

    public C2693ig(C1616Xf xf) {
        super(xf);
    }

    /* renamed from: c */
    public C5663aRz[] mo309c() {
        return PopulationControl._m_fields;
    }

    /* renamed from: d */
    public void mo310d() {
        super.mo310d();
        this.f8234Xx = null;
        this.f8235Xy = 0.0f;
        this.f8237bK = null;
        this.handle = null;
        this.f8236Xz = 0;
        this.f8231XA = 0;
        this.f8238oL = 0;
        this.f8240ok = 0;
        this.f8232XB = 0;
        this.f8233XC = 0;
        this.f8239oS = 0;
        this.f8241on = 0;
    }
}
