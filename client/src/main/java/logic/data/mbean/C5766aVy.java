package logic.data.mbean;

import game.script.mines.Mine;
import game.script.mines.MineThrowerWeapon;
import game.script.resource.Asset;
import game.script.ship.Hull;
import logic.baa.C1616Xf;
import logic.res.code.C5663aRz;
import p001a.C0064Am;
import p001a.C5260aCm;

/* renamed from: a.aVy  reason: case insensitive filesystem */
public class C5766aVy extends C2217cs {
    @C0064Am(aul = "4b198774fbf9c3518f3a4b6451f9810b", aum = 0)

    /* renamed from: VT */
    public MineAreaTrigger f3990VT;
    @C0064Am(aul = "ef5520d81f7a1a928e424e9ef86bdfa7", aum = 1)

    /* renamed from: VV */
    public Hull f3991VV;
    @C0064Am(aul = "c1a63fd572b12a5b744fbd6e3e7ccb56", aum = 2)

    /* renamed from: VX */
    public float f3992VX;
    @C0064Am(aul = "e1d44812e8dbd862992599290c491f59", aum = 3)

    /* renamed from: VZ */
    public float f3993VZ;
    @C0064Am(aul = "2b104f988c368f2abb733401f734c049", aum = 4)

    /* renamed from: Wb */
    public Asset f3994Wb;
    @C0064Am(aul = "45de38e54254c7c5b8942a819d1a56cc", aum = 5)

    /* renamed from: Wd */
    public Asset f3995Wd;
    @C0064Am(aul = "eb1c061aa36d0cb1be31d67fb179c480", aum = 7)

    /* renamed from: Wg */
    public boolean f3996Wg;
    @C0064Am(aul = "ff8153164faab4dbdcc845f1a6d7b026", aum = 9)

    /* renamed from: Wi */
    public MineThrowerWeapon f3997Wi;
    @C0064Am(aul = "b569c45f09b80399d8d3ae61a7f2d846", aum = -2)
    public long ail;
    @C0064Am(aul = "b569c45f09b80399d8d3ae61a7f2d846", aum = -1)
    public byte aiv;
    @C0064Am(aul = "8bdc871591a9956c3ab1714cd8a58a13", aum = -2)
    public long beq;
    @C0064Am(aul = "8bdc871591a9956c3ab1714cd8a58a13", aum = -1)
    public byte bes;
    @C0064Am(aul = "c1a63fd572b12a5b744fbd6e3e7ccb56", aum = -1)
    public byte bhB;
    @C0064Am(aul = "2b104f988c368f2abb733401f734c049", aum = -1)
    public byte bhC;
    @C0064Am(aul = "45de38e54254c7c5b8942a819d1a56cc", aum = -1)
    public byte bhD;
    @C0064Am(aul = "ef5520d81f7a1a928e424e9ef86bdfa7", aum = -1)
    public byte bhF;
    @C0064Am(aul = "c1a63fd572b12a5b744fbd6e3e7ccb56", aum = -2)
    public long bhs;
    @C0064Am(aul = "2b104f988c368f2abb733401f734c049", aum = -2)
    public long bht;
    @C0064Am(aul = "45de38e54254c7c5b8942a819d1a56cc", aum = -2)
    public long bhu;
    @C0064Am(aul = "ef5520d81f7a1a928e424e9ef86bdfa7", aum = -2)
    public long bhw;
    @C0064Am(aul = "e1d44812e8dbd862992599290c491f59", aum = -2)
    public long dVZ;
    @C0064Am(aul = "e1d44812e8dbd862992599290c491f59", aum = -1)
    public byte dWa;
    @C0064Am(aul = "ff8153164faab4dbdcc845f1a6d7b026", aum = -2)
    public long fPi;
    @C0064Am(aul = "ff8153164faab4dbdcc845f1a6d7b026", aum = -1)
    public byte fPp;
    @C0064Am(aul = "4b198774fbf9c3518f3a4b6451f9810b", aum = -2)
    public long jah;
    @C0064Am(aul = "eb1c061aa36d0cb1be31d67fb179c480", aum = -2)
    public long jai;
    @C0064Am(aul = "4b198774fbf9c3518f3a4b6451f9810b", aum = -1)
    public byte jaj;
    @C0064Am(aul = "eb1c061aa36d0cb1be31d67fb179c480", aum = -1)
    public byte jak;
    @C0064Am(aul = "b569c45f09b80399d8d3ae61a7f2d846", aum = 6)
    public int lifeTime;
    @C0064Am(aul = "8bdc871591a9956c3ab1714cd8a58a13", aum = 8)

    /* renamed from: uJ */
    public C5260aCm f3998uJ;

    public C5766aVy() {
    }

    public C5766aVy(C1616Xf xf) {
        super(xf);
    }

    /* renamed from: c */
    public C5663aRz[] mo309c() {
        return Mine._m_fields;
    }

    /* renamed from: d */
    public void mo310d() {
        super.mo310d();
        this.f3990VT = null;
        this.f3991VV = null;
        this.f3992VX = 0.0f;
        this.f3993VZ = 0.0f;
        this.f3994Wb = null;
        this.f3995Wd = null;
        this.lifeTime = 0;
        this.f3996Wg = false;
        this.f3998uJ = null;
        this.f3997Wi = null;
        this.jah = 0;
        this.bhw = 0;
        this.bhs = 0;
        this.dVZ = 0;
        this.bht = 0;
        this.bhu = 0;
        this.ail = 0;
        this.jai = 0;
        this.beq = 0;
        this.fPi = 0;
        this.jaj = 0;
        this.bhF = 0;
        this.bhB = 0;
        this.dWa = 0;
        this.bhC = 0;
        this.bhD = 0;
        this.aiv = 0;
        this.jak = 0;
        this.bes = 0;
        this.fPp = 0;
    }
}
