package logic.data.mbean;

import game.script.mission.Mission;
import game.script.mission.MissionTemplate;
import game.script.mission.MissionTrigger;
import logic.baa.C1616Xf;
import logic.res.code.C5663aRz;
import p001a.C0064Am;

/* renamed from: a.SL */
public class C1235SL extends C6151ahT {
    @C0064Am(aul = "81135db7d5daf7ecd65a6afd95d588b7", aum = -2)
    public long dJT;
    @C0064Am(aul = "81135db7d5daf7ecd65a6afd95d588b7", aum = -1)
    public byte dKv;
    @C0064Am(aul = "7c62e4a0656302866cf175c487770610", aum = 0)
    public String efM;
    @C0064Am(aul = "7c62e4a0656302866cf175c487770610", aum = -2)
    public long efN;
    @C0064Am(aul = "93f68848f9777fae6ade89644a5a918a", aum = -2)
    public long efO;
    @C0064Am(aul = "7c62e4a0656302866cf175c487770610", aum = -1)
    public byte efP;
    @C0064Am(aul = "93f68848f9777fae6ade89644a5a918a", aum = -1)
    public byte efQ;
    @C0064Am(aul = "93f68848f9777fae6ade89644a5a918a", aum = 2)

    /* renamed from: mb */
    public boolean f1536mb;
    @C0064Am(aul = "81135db7d5daf7ecd65a6afd95d588b7", aum = 1)

    /* renamed from: wx */
    public Mission<? extends MissionTemplate> f1537wx;

    public C1235SL() {
    }

    public C1235SL(C1616Xf xf) {
        super(xf);
    }

    /* renamed from: c */
    public C5663aRz[] mo309c() {
        return MissionTrigger._m_fields;
    }

    /* renamed from: d */
    public void mo310d() {
        super.mo310d();
        this.efM = null;
        this.f1537wx = null;
        this.f1536mb = false;
        this.efN = 0;
        this.dJT = 0;
        this.efO = 0;
        this.efP = 0;
        this.dKv = 0;
        this.efQ = 0;
    }
}
