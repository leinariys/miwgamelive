package logic.data.mbean;

import game.script.item.BagItem;
import game.script.item.SimpleBag;
import logic.baa.C1616Xf;
import logic.res.code.C5663aRz;
import p001a.C0064Am;

/* renamed from: a.aPa  reason: case insensitive filesystem */
public class C5586aPa extends aAL {
    @C0064Am(aul = "d8378d01978748cf74dddcaf4d7d7c64", aum = 0)

    /* renamed from: Rv */
    public BagItem f3533Rv;
    @C0064Am(aul = "d8378d01978748cf74dddcaf4d7d7c64", aum = -2)
    public long iAG;
    @C0064Am(aul = "d8378d01978748cf74dddcaf4d7d7c64", aum = -1)
    public byte iAH;

    public C5586aPa() {
    }

    public C5586aPa(C1616Xf xf) {
        super(xf);
    }

    /* renamed from: c */
    public C5663aRz[] mo309c() {
        return SimpleBag.SimpleBagLocation._m_fields;
    }

    /* renamed from: d */
    public void mo310d() {
        super.mo310d();
        this.f3533Rv = null;
        this.iAG = 0;
        this.iAH = 0;
    }
}
