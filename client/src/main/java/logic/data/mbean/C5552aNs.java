package logic.data.mbean;

import game.script.nls.NLSWindowAlert;
import logic.baa.C1616Xf;
import logic.res.code.C5663aRz;
import p001a.C0064Am;
import taikodom.infra.script.I18NString;

/* renamed from: a.aNs  reason: case insensitive filesystem */
public class C5552aNs extends C2484fi {
    @C0064Am(aul = "f600cab08d5f6b098eda1194677e3e70", aum = 3)
    public I18NString afe;
    @C0064Am(aul = "95290e1cc82afbbc18cdad84a72b5446", aum = -2)
    public long bZl;
    @C0064Am(aul = "95290e1cc82afbbc18cdad84a72b5446", aum = -1)
    public byte bZr;
    @C0064Am(aul = "f600cab08d5f6b098eda1194677e3e70", aum = -2)
    public long dxV;
    @C0064Am(aul = "f600cab08d5f6b098eda1194677e3e70", aum = -1)
    public byte dyd;
    @C0064Am(aul = "6b1633930036538512c7471223718976", aum = 9)
    public I18NString hRA;
    @C0064Am(aul = "42a04f95ddc954a7d4e2eae39c3080ce", aum = 10)
    public I18NString hRC;
    @C0064Am(aul = "460885983dd8364478229addbb8157f7", aum = 11)
    public I18NString hRE;
    @C0064Am(aul = "1ea203ca00e979b2f95352438de16e4b", aum = 12)
    public I18NString hRG;
    @C0064Am(aul = "7f116621c2b2cd3d8afdb47ea56a0cb2", aum = 13)
    public I18NString hRI;
    @C0064Am(aul = "c569050a606d526c6f204146559ad27e", aum = 14)
    public I18NString hRK;
    @C0064Am(aul = "e0d6e259bf4d45f3c1751900f767b94e", aum = 15)
    public I18NString hRM;
    @C0064Am(aul = "a6dfe11cfb43a37475894b1f0e75248c", aum = 16)
    public I18NString hRO;
    @C0064Am(aul = "30e519f58e852674afec902eab9bfa43", aum = 17)
    public I18NString hRQ;
    @C0064Am(aul = "3d52c1c5c9df8e19e89655d944d87af1", aum = 18)
    public I18NString hRS;
    @C0064Am(aul = "95266d0c36440c92b4edc3965308e9a7", aum = 19)
    public I18NString hRU;
    @C0064Am(aul = "7d28eefadc90708a661d481f3fbbdbee", aum = 20)
    public I18NString hRW;
    @C0064Am(aul = "acccd9a87a117809b9a8faf60015c491", aum = 21)
    public I18NString hRY;
    @C0064Am(aul = "81b4da48ee7b01eaaa0af4ca155455c1", aum = 0)
    public I18NString hRk;
    @C0064Am(aul = "00815e58a17817bb65767905ccc56b8e", aum = 1)
    public I18NString hRm;
    @C0064Am(aul = "b5b22c137c99f45b74a2ed90171f2d53", aum = 2)
    public I18NString hRo;
    @C0064Am(aul = "fd0a5c5e717b8f636443289cff538606", aum = 4)
    public I18NString hRq;
    @C0064Am(aul = "1ba395a4d03b4727daae445c61d8079f", aum = 5)
    public I18NString hRs;
    @C0064Am(aul = "3fb4ad8c250f578026604031dbd1a954", aum = 6)
    public I18NString hRu;
    @C0064Am(aul = "6e14d5427097862c591c511cbbb97675", aum = 7)
    public I18NString hRw;
    @C0064Am(aul = "fdbddb5f73a6d530c2300f84b665f6b8", aum = 8)
    public I18NString hRy;
    @C0064Am(aul = "d8a726ff14376c7d8854ff80e9ca70ca", aum = 22)
    public I18NString hSa;
    @C0064Am(aul = "27a900c8aec75f98e0b6e93b19274295", aum = 23)
    public I18NString hSc;
    @C0064Am(aul = "5c3321c9a0ab4bd8ed94b702524b7856", aum = 24)
    public I18NString hSe;
    @C0064Am(aul = "8db40ef34a3c6feeb090d18546a34682", aum = 25)
    public I18NString hSg;
    @C0064Am(aul = "3555a7b5ee8e2698ea02c508d6a84346", aum = 26)
    public I18NString hSi;
    @C0064Am(aul = "a19dd20b64494e84776e0be15077f954", aum = 27)
    public I18NString hSk;
    @C0064Am(aul = "21b15a0f400db5c4c3e2fa3ac4e7dbe7", aum = 28)
    public I18NString hSm;
    @C0064Am(aul = "4b18ca353a0f946051cd3968885a9726", aum = 29)
    public I18NString hSo;
    @C0064Am(aul = "95290e1cc82afbbc18cdad84a72b5446", aum = 30)
    public I18NString hSq;
    @C0064Am(aul = "9a4140bf977bcc58158644057fc8a285", aum = 31)
    public I18NString hSr;
    @C0064Am(aul = "86495509dc8b8f264891e1fb4e146839", aum = 32)
    public I18NString hSt;
    @C0064Am(aul = "06c51e2fab30527413bbae43f60de189", aum = 33)
    public I18NString hSv;
    @C0064Am(aul = "81b4da48ee7b01eaaa0af4ca155455c1", aum = -2)
    public long irD;
    @C0064Am(aul = "00815e58a17817bb65767905ccc56b8e", aum = -2)
    public long irE;
    @C0064Am(aul = "b5b22c137c99f45b74a2ed90171f2d53", aum = -2)
    public long irF;
    @C0064Am(aul = "fd0a5c5e717b8f636443289cff538606", aum = -2)
    public long irG;
    @C0064Am(aul = "1ba395a4d03b4727daae445c61d8079f", aum = -2)
    public long irH;
    @C0064Am(aul = "3fb4ad8c250f578026604031dbd1a954", aum = -2)
    public long irI;
    @C0064Am(aul = "6e14d5427097862c591c511cbbb97675", aum = -2)
    public long irJ;
    @C0064Am(aul = "fdbddb5f73a6d530c2300f84b665f6b8", aum = -2)
    public long irK;
    @C0064Am(aul = "6b1633930036538512c7471223718976", aum = -2)
    public long irL;
    @C0064Am(aul = "42a04f95ddc954a7d4e2eae39c3080ce", aum = -2)
    public long irM;
    @C0064Am(aul = "460885983dd8364478229addbb8157f7", aum = -2)
    public long irN;
    @C0064Am(aul = "1ea203ca00e979b2f95352438de16e4b", aum = -2)
    public long irO;
    @C0064Am(aul = "7f116621c2b2cd3d8afdb47ea56a0cb2", aum = -2)
    public long irP;
    @C0064Am(aul = "c569050a606d526c6f204146559ad27e", aum = -2)
    public long irQ;
    @C0064Am(aul = "e0d6e259bf4d45f3c1751900f767b94e", aum = -2)
    public long irR;
    @C0064Am(aul = "a6dfe11cfb43a37475894b1f0e75248c", aum = -2)
    public long irS;
    @C0064Am(aul = "30e519f58e852674afec902eab9bfa43", aum = -2)
    public long irT;
    @C0064Am(aul = "3d52c1c5c9df8e19e89655d944d87af1", aum = -2)
    public long irU;
    @C0064Am(aul = "95266d0c36440c92b4edc3965308e9a7", aum = -2)
    public long irV;
    @C0064Am(aul = "7d28eefadc90708a661d481f3fbbdbee", aum = -2)
    public long irW;
    @C0064Am(aul = "acccd9a87a117809b9a8faf60015c491", aum = -2)
    public long irX;
    @C0064Am(aul = "d8a726ff14376c7d8854ff80e9ca70ca", aum = -2)
    public long irY;
    @C0064Am(aul = "27a900c8aec75f98e0b6e93b19274295", aum = -2)
    public long irZ;
    @C0064Am(aul = "3d52c1c5c9df8e19e89655d944d87af1", aum = -1)
    public byte isA;
    @C0064Am(aul = "95266d0c36440c92b4edc3965308e9a7", aum = -1)
    public byte isB;
    @C0064Am(aul = "7d28eefadc90708a661d481f3fbbdbee", aum = -1)
    public byte isC;
    @C0064Am(aul = "acccd9a87a117809b9a8faf60015c491", aum = -1)
    public byte isD;
    @C0064Am(aul = "d8a726ff14376c7d8854ff80e9ca70ca", aum = -1)
    public byte isE;
    @C0064Am(aul = "27a900c8aec75f98e0b6e93b19274295", aum = -1)
    public byte isF;
    @C0064Am(aul = "5c3321c9a0ab4bd8ed94b702524b7856", aum = -1)
    public byte isG;
    @C0064Am(aul = "8db40ef34a3c6feeb090d18546a34682", aum = -1)
    public byte isH;
    @C0064Am(aul = "3555a7b5ee8e2698ea02c508d6a84346", aum = -1)
    public byte isI;
    @C0064Am(aul = "a19dd20b64494e84776e0be15077f954", aum = -1)
    public byte isJ;
    @C0064Am(aul = "21b15a0f400db5c4c3e2fa3ac4e7dbe7", aum = -1)
    public byte isK;
    @C0064Am(aul = "4b18ca353a0f946051cd3968885a9726", aum = -1)
    public byte isL;
    @C0064Am(aul = "9a4140bf977bcc58158644057fc8a285", aum = -1)
    public byte isM;
    @C0064Am(aul = "86495509dc8b8f264891e1fb4e146839", aum = -1)
    public byte isN;
    @C0064Am(aul = "06c51e2fab30527413bbae43f60de189", aum = -1)
    public byte isO;
    @C0064Am(aul = "5c3321c9a0ab4bd8ed94b702524b7856", aum = -2)
    public long isa;
    @C0064Am(aul = "8db40ef34a3c6feeb090d18546a34682", aum = -2)
    public long isb;
    @C0064Am(aul = "3555a7b5ee8e2698ea02c508d6a84346", aum = -2)
    public long isc;
    @C0064Am(aul = "a19dd20b64494e84776e0be15077f954", aum = -2)
    public long isd;
    @C0064Am(aul = "21b15a0f400db5c4c3e2fa3ac4e7dbe7", aum = -2)
    public long ise;
    @C0064Am(aul = "4b18ca353a0f946051cd3968885a9726", aum = -2)
    public long isf;
    @C0064Am(aul = "9a4140bf977bcc58158644057fc8a285", aum = -2)
    public long isg;
    @C0064Am(aul = "86495509dc8b8f264891e1fb4e146839", aum = -2)
    public long ish;
    @C0064Am(aul = "06c51e2fab30527413bbae43f60de189", aum = -2)
    public long isi;
    @C0064Am(aul = "81b4da48ee7b01eaaa0af4ca155455c1", aum = -1)
    public byte isj;
    @C0064Am(aul = "00815e58a17817bb65767905ccc56b8e", aum = -1)
    public byte isk;
    @C0064Am(aul = "b5b22c137c99f45b74a2ed90171f2d53", aum = -1)
    public byte isl;
    @C0064Am(aul = "fd0a5c5e717b8f636443289cff538606", aum = -1)
    public byte ism;
    @C0064Am(aul = "1ba395a4d03b4727daae445c61d8079f", aum = -1)
    public byte isn;
    @C0064Am(aul = "3fb4ad8c250f578026604031dbd1a954", aum = -1)
    public byte iso;
    @C0064Am(aul = "6e14d5427097862c591c511cbbb97675", aum = -1)
    public byte isp;
    @C0064Am(aul = "fdbddb5f73a6d530c2300f84b665f6b8", aum = -1)
    public byte isq;
    @C0064Am(aul = "6b1633930036538512c7471223718976", aum = -1)
    public byte isr;
    @C0064Am(aul = "42a04f95ddc954a7d4e2eae39c3080ce", aum = -1)
    public byte iss;
    @C0064Am(aul = "460885983dd8364478229addbb8157f7", aum = -1)
    public byte ist;
    @C0064Am(aul = "1ea203ca00e979b2f95352438de16e4b", aum = -1)
    public byte isu;
    @C0064Am(aul = "7f116621c2b2cd3d8afdb47ea56a0cb2", aum = -1)
    public byte isv;
    @C0064Am(aul = "c569050a606d526c6f204146559ad27e", aum = -1)
    public byte isw;
    @C0064Am(aul = "e0d6e259bf4d45f3c1751900f767b94e", aum = -1)
    public byte isx;
    @C0064Am(aul = "a6dfe11cfb43a37475894b1f0e75248c", aum = -1)
    public byte isy;
    @C0064Am(aul = "30e519f58e852674afec902eab9bfa43", aum = -1)
    public byte isz;

    public C5552aNs() {
    }

    public C5552aNs(C1616Xf xf) {
        super(xf);
    }

    /* renamed from: c */
    public C5663aRz[] mo309c() {
        return NLSWindowAlert._m_fields;
    }

    /* renamed from: d */
    public void mo310d() {
        super.mo310d();
        this.hRk = null;
        this.hRm = null;
        this.hRo = null;
        this.afe = null;
        this.hRq = null;
        this.hRs = null;
        this.hRu = null;
        this.hRw = null;
        this.hRy = null;
        this.hRA = null;
        this.hRC = null;
        this.hRE = null;
        this.hRG = null;
        this.hRI = null;
        this.hRK = null;
        this.hRM = null;
        this.hRO = null;
        this.hRQ = null;
        this.hRS = null;
        this.hRU = null;
        this.hRW = null;
        this.hRY = null;
        this.hSa = null;
        this.hSc = null;
        this.hSe = null;
        this.hSg = null;
        this.hSi = null;
        this.hSk = null;
        this.hSm = null;
        this.hSo = null;
        this.hSq = null;
        this.hSr = null;
        this.hSt = null;
        this.hSv = null;
        this.irD = 0;
        this.irE = 0;
        this.irF = 0;
        this.dxV = 0;
        this.irG = 0;
        this.irH = 0;
        this.irI = 0;
        this.irJ = 0;
        this.irK = 0;
        this.irL = 0;
        this.irM = 0;
        this.irN = 0;
        this.irO = 0;
        this.irP = 0;
        this.irQ = 0;
        this.irR = 0;
        this.irS = 0;
        this.irT = 0;
        this.irU = 0;
        this.irV = 0;
        this.irW = 0;
        this.irX = 0;
        this.irY = 0;
        this.irZ = 0;
        this.isa = 0;
        this.isb = 0;
        this.isc = 0;
        this.isd = 0;
        this.ise = 0;
        this.isf = 0;
        this.bZl = 0;
        this.isg = 0;
        this.ish = 0;
        this.isi = 0;
        this.isj = 0;
        this.isk = 0;
        this.isl = 0;
        this.dyd = 0;
        this.ism = 0;
        this.isn = 0;
        this.iso = 0;
        this.isp = 0;
        this.isq = 0;
        this.isr = 0;
        this.iss = 0;
        this.ist = 0;
        this.isu = 0;
        this.isv = 0;
        this.isw = 0;
        this.isx = 0;
        this.isy = 0;
        this.isz = 0;
        this.isA = 0;
        this.isB = 0;
        this.isC = 0;
        this.isD = 0;
        this.isE = 0;
        this.isF = 0;
        this.isG = 0;
        this.isH = 0;
        this.isI = 0;
        this.isJ = 0;
        this.isK = 0;
        this.isL = 0;
        this.bZr = 0;
        this.isM = 0;
        this.isN = 0;
        this.isO = 0;
    }
}
