package logic.data.mbean;

import game.network.message.serializable.C2686iZ;
import game.network.message.serializable.C3438ra;
import game.script.Actor;
import game.script.simulation.Space;
import logic.baa.C1616Xf;
import logic.bbb.aDR;
import logic.res.code.C5663aRz;
import p001a.C0064Am;
import taikodom.infra.script.I18NString;

/* renamed from: a.aGX */
public class aGX extends C3805vD {
    @C0064Am(aul = "be20ef5cbcd911d1cb7b622f74b0b433", aum = -2)

    /* renamed from: Nf */
    public long f2867Nf;
    @C0064Am(aul = "be20ef5cbcd911d1cb7b622f74b0b433", aum = -1)

    /* renamed from: Nh */
    public byte f2868Nh;
    @C0064Am(aul = "f5878334d1b40970d5943a6f73d00a87", aum = -2)
    public long hUb;
    @C0064Am(aul = "4decb81796ed85816268f44d4c9151d3", aum = -2)
    public long hUc;
    @C0064Am(aul = "f5878334d1b40970d5943a6f73d00a87", aum = -1)
    public byte hUd;
    @C0064Am(aul = "4decb81796ed85816268f44d4c9151d3", aum = -1)
    public byte hUe;
    @C0064Am(aul = "f5878334d1b40970d5943a6f73d00a87", aum = 0)
    public C2686iZ<Actor> hiS;
    @C0064Am(aul = "4decb81796ed85816268f44d4c9151d3", aum = 2)
    public C3438ra<aDR> hje;
    @C0064Am(aul = "be20ef5cbcd911d1cb7b622f74b0b433", aum = 1)

    /* renamed from: zP */
    public I18NString f2869zP;

    public aGX() {
    }

    public aGX(C1616Xf xf) {
        super(xf);
    }

    /* renamed from: c */
    public C5663aRz[] mo309c() {
        return Space._m_fields;
    }

    /* renamed from: d */
    public void mo310d() {
        super.mo310d();
        this.hiS = null;
        this.f2869zP = null;
        this.hje = null;
        this.hUb = 0;
        this.f2867Nf = 0;
        this.hUc = 0;
        this.hUd = 0;
        this.f2868Nh = 0;
        this.hUe = 0;
    }
}
