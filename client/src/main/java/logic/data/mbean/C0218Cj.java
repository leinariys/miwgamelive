package logic.data.mbean;

import game.network.message.serializable.C3438ra;
import game.script.pda.TaikopediaEntry;
import game.script.spacezone.SpaceZoneType;
import logic.baa.C1616Xf;
import logic.res.code.C5663aRz;
import p001a.C0064Am;

/* renamed from: a.Cj */
public class C0218Cj extends C6515aoT {
    @C0064Am(aul = "3c776bc3ed77bd1c631383d54091ae20", aum = 0)

    /* renamed from: jT */
    public C3438ra<TaikopediaEntry> f353jT;
    @C0064Am(aul = "3c776bc3ed77bd1c631383d54091ae20", aum = -2)

    /* renamed from: jY */
    public long f354jY;
    @C0064Am(aul = "3c776bc3ed77bd1c631383d54091ae20", aum = -1)

    /* renamed from: kd */
    public byte f355kd;

    public C0218Cj() {
    }

    public C0218Cj(C1616Xf xf) {
        super(xf);
    }

    /* renamed from: c */
    public C5663aRz[] mo309c() {
        return SpaceZoneType._m_fields;
    }

    /* renamed from: d */
    public void mo310d() {
        super.mo310d();
        this.f353jT = null;
        this.f354jY = 0;
        this.f355kd = 0;
    }
}
