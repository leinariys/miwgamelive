package logic.data.mbean;

import game.script.citizenship.inprovements.InsuranceImprovement;
import logic.baa.C1616Xf;
import logic.res.code.C5663aRz;
import p001a.C0064Am;
import taikodom.infra.script.I18NString;

/* renamed from: a.aGb  reason: case insensitive filesystem */
public class C5353aGb extends C2057bg {
    @C0064Am(aul = "900e30743775c0e9d46af6a8c57f93a8", aum = 1)
    public I18NString cRI;
    @C0064Am(aul = "a7c62d512c6912397cbd2fd191c3558c", aum = -2)
    public long cRJ;
    @C0064Am(aul = "900e30743775c0e9d46af6a8c57f93a8", aum = -2)
    public long cRK;
    @C0064Am(aul = "a7c62d512c6912397cbd2fd191c3558c", aum = -1)
    public byte cRL;
    @C0064Am(aul = "900e30743775c0e9d46af6a8c57f93a8", aum = -1)
    public byte cRM;
    @C0064Am(aul = "a7c62d512c6912397cbd2fd191c3558c", aum = 0)
    public long ciA;

    public C5353aGb() {
    }

    public C5353aGb(C1616Xf xf) {
        super(xf);
    }

    /* renamed from: c */
    public C5663aRz[] mo309c() {
        return InsuranceImprovement._m_fields;
    }

    /* renamed from: d */
    public void mo310d() {
        super.mo310d();
        this.ciA = 0;
        this.cRI = null;
        this.cRJ = 0;
        this.cRK = 0;
        this.cRL = 0;
        this.cRM = 0;
    }
}
