package logic.data.mbean;

import game.script.item.buff.module.CruiseSpeedBuff;
import logic.baa.C1616Xf;
import logic.res.code.C5663aRz;
import p001a.C0064Am;

/* renamed from: a.aPf  reason: case insensitive filesystem */
public class C5591aPf extends C3760ug {
    @C0064Am(aul = "510dd575a81d9ec8d05468ac5951f314", aum = -2)

    /* renamed from: dl */
    public long f3543dl;
    @C0064Am(aul = "510dd575a81d9ec8d05468ac5951f314", aum = -1)

    /* renamed from: ds */
    public byte f3544ds;
    @C0064Am(aul = "510dd575a81d9ec8d05468ac5951f314", aum = 0)
    public CruiseSpeedBuff iAS;

    public C5591aPf() {
    }

    public C5591aPf(C1616Xf xf) {
        super(xf);
    }

    /* renamed from: c */
    public C5663aRz[] mo309c() {
        return CruiseSpeedBuff.CruiseAmplifierAdapter._m_fields;
    }

    /* renamed from: d */
    public void mo310d() {
        super.mo310d();
        this.iAS = null;
        this.f3543dl = 0;
        this.f3544ds = 0;
    }
}
