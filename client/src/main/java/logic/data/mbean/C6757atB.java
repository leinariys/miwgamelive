package logic.data.mbean;

import logic.baa.C1616Xf;
import logic.baa.aDJ;
import logic.res.code.C5663aRz;
import p001a.C0064Am;

/* renamed from: a.atB  reason: case insensitive filesystem */
public class C6757atB extends C3805vD {
    @C0064Am(aul = "261a708877317283e2f81298f9a8b4c0", aum = -2)
    public long aIu;
    @C0064Am(aul = "261a708877317283e2f81298f9a8b4c0", aum = -1)
    public byte aIx;
    @C0064Am(aul = "261a708877317283e2f81298f9a8b4c0", aum = 1)
    public boolean active;
    @C0064Am(aul = "bea96816a58a3d7ef481723c95db6804", aum = 0)
    public aDJ dSE;
    @C0064Am(aul = "144240ef70873508fe6415f619e50e49", aum = 2)
    public long dSG;
    @C0064Am(aul = "7ab331132c9b764f94d369d8f3350f29", aum = 3)
    public long dSI;
    @C0064Am(aul = "ad922a339606c3eaf17c8c9b26634fc1", aum = 4)
    public int dSK;
    @C0064Am(aul = "fc7be6723eb1f960bc42008375e78fe8", aum = 5)
    public int dSM;
    @C0064Am(aul = "e94896763f29c1398567c0f02ebda582", aum = 6)
    public long dSO;
    @C0064Am(aul = "f970997d60a54e4d3b95d1f8af4cbeab", aum = 7)
    public long dSQ;
    @C0064Am(aul = "9dee5454171e5f7d9876cf59f265f98a", aum = 8)
    public int dSS;
    @C0064Am(aul = "39eb41c40a716f8587e2024f672537d5", aum = 9)
    public long dSU;
    @C0064Am(aul = "bea96816a58a3d7ef481723c95db6804", aum = -2)
    public long eLl;
    @C0064Am(aul = "bea96816a58a3d7ef481723c95db6804", aum = -1)
    public byte eLn;
    @C0064Am(aul = "144240ef70873508fe6415f619e50e49", aum = -2)
    public long gAf;
    @C0064Am(aul = "7ab331132c9b764f94d369d8f3350f29", aum = -2)
    public long gAg;
    @C0064Am(aul = "ad922a339606c3eaf17c8c9b26634fc1", aum = -2)
    public long gAh;
    @C0064Am(aul = "fc7be6723eb1f960bc42008375e78fe8", aum = -2)
    public long gAi;
    @C0064Am(aul = "e94896763f29c1398567c0f02ebda582", aum = -2)
    public long gAj;
    @C0064Am(aul = "f970997d60a54e4d3b95d1f8af4cbeab", aum = -2)
    public long gAk;
    @C0064Am(aul = "9dee5454171e5f7d9876cf59f265f98a", aum = -2)
    public long gAl;
    @C0064Am(aul = "39eb41c40a716f8587e2024f672537d5", aum = -2)
    public long gAm;
    @C0064Am(aul = "144240ef70873508fe6415f619e50e49", aum = -1)
    public byte gAn;
    @C0064Am(aul = "7ab331132c9b764f94d369d8f3350f29", aum = -1)
    public byte gAo;
    @C0064Am(aul = "ad922a339606c3eaf17c8c9b26634fc1", aum = -1)
    public byte gAp;
    @C0064Am(aul = "fc7be6723eb1f960bc42008375e78fe8", aum = -1)
    public byte gAq;
    @C0064Am(aul = "e94896763f29c1398567c0f02ebda582", aum = -1)
    public byte gAr;
    @C0064Am(aul = "f970997d60a54e4d3b95d1f8af4cbeab", aum = -1)
    public byte gAs;
    @C0064Am(aul = "9dee5454171e5f7d9876cf59f265f98a", aum = -1)
    public byte gAt;
    @C0064Am(aul = "39eb41c40a716f8587e2024f672537d5", aum = -1)
    public byte gAu;

    public C6757atB() {
    }

    public C6757atB(C1616Xf xf) {
        super(xf);
    }

    /* renamed from: c */
    public C5663aRz[] mo309c() {
        return TaskletImpl._m_fields;
    }

    /* renamed from: d */
    public void mo310d() {
        super.mo310d();
        this.dSE = null;
        this.active = false;
        this.dSG = 0;
        this.dSI = 0;
        this.dSK = 0;
        this.dSM = 0;
        this.dSO = 0;
        this.dSQ = 0;
        this.dSS = 0;
        this.dSU = 0;
        this.eLl = 0;
        this.aIu = 0;
        this.gAf = 0;
        this.gAg = 0;
        this.gAh = 0;
        this.gAi = 0;
        this.gAj = 0;
        this.gAk = 0;
        this.gAl = 0;
        this.gAm = 0;
        this.eLn = 0;
        this.aIx = 0;
        this.gAn = 0;
        this.gAo = 0;
        this.gAp = 0;
        this.gAq = 0;
        this.gAr = 0;
        this.gAs = 0;
        this.gAt = 0;
        this.gAu = 0;
    }
}
