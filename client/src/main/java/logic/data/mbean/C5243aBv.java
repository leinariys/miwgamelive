package logic.data.mbean;

import game.network.message.serializable.C3438ra;
import game.script.missiontemplate.ReconWaypointDat;
import game.script.missiontemplate.scripting.ReconMissionScriptTemplate;
import game.script.space.StellarSystem;
import logic.baa.C1616Xf;
import logic.res.code.C5663aRz;
import p001a.C0064Am;

/* renamed from: a.aBv  reason: case insensitive filesystem */
public class C5243aBv extends C0597IR {
    @C0064Am(aul = "8773c705a6f6c914a0d9110ec7640b7b", aum = 1)
    public C3438ra<ReconWaypointDat> aBp;
    @C0064Am(aul = "8773c705a6f6c914a0d9110ec7640b7b", aum = -2)
    public long dhN;
    @C0064Am(aul = "8773c705a6f6c914a0d9110ec7640b7b", aum = -1)
    public byte dhS;
    @C0064Am(aul = "6d2327adfcbae92aa4bc4b2586804ab7", aum = 0)

    /* renamed from: rI */
    public StellarSystem f2445rI;
    @C0064Am(aul = "6d2327adfcbae92aa4bc4b2586804ab7", aum = -2)

    /* renamed from: rY */
    public long f2446rY;
    @C0064Am(aul = "6d2327adfcbae92aa4bc4b2586804ab7", aum = -1)

    /* renamed from: so */
    public byte f2447so;

    public C5243aBv() {
    }

    public C5243aBv(C1616Xf xf) {
        super(xf);
    }

    /* renamed from: c */
    public C5663aRz[] mo309c() {
        return ReconMissionScriptTemplate._m_fields;
    }

    /* renamed from: d */
    public void mo310d() {
        super.mo310d();
        this.f2445rI = null;
        this.aBp = null;
        this.f2446rY = 0;
        this.dhN = 0;
        this.f2447so = 0;
        this.dhS = 0;
    }
}
