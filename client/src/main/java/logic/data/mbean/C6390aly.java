package logic.data.mbean;

import game.script.mission.Mission;
import game.script.mission.MissionTimer;
import logic.baa.C1616Xf;
import logic.res.code.C5663aRz;
import p001a.C0064Am;
import p001a.C4045yZ;

/* renamed from: a.aly  reason: case insensitive filesystem */
public class C6390aly extends C3805vD {
    @C0064Am(aul = "0b6acb8bc885192a634ffe8b5e1d6f70", aum = -2)
    public long dJT;
    @C0064Am(aul = "0b6acb8bc885192a634ffe8b5e1d6f70", aum = -1)
    public byte dKv;
    @C0064Am(aul = "ee61d0783ff2c6f7001714053c5989af", aum = -1)
    public byte fXA;
    @C0064Am(aul = "7802a8871959100cb8678c8f74dc16b0", aum = -1)
    public byte fXB;
    @C0064Am(aul = "46a8f0d4db7353a2fdcc851c612fdeba", aum = -1)
    public byte fXC;
    @C0064Am(aul = "515297e4703b00107693fca7abdd3bd2", aum = -1)
    public byte fXD;
    @C0064Am(aul = "78f4609055a4fec1fd7f63fb629d3dd2", aum = 2)
    public LDScriptingController fXo;
    @C0064Am(aul = "42741c9c78a11d7a45ec5d22f27b9a3a", aum = 3)
    public boolean fXp;
    @C0064Am(aul = "ee61d0783ff2c6f7001714053c5989af", aum = 4)
    public int fXq;
    @C0064Am(aul = "7802a8871959100cb8678c8f74dc16b0", aum = 5)
    public float fXr;
    @C0064Am(aul = "46a8f0d4db7353a2fdcc851c612fdeba", aum = 6)
    public boolean fXs;
    @C0064Am(aul = "515297e4703b00107693fca7abdd3bd2", aum = 7)
    public long fXt;
    @C0064Am(aul = "42741c9c78a11d7a45ec5d22f27b9a3a", aum = -2)
    public long fXu;
    @C0064Am(aul = "ee61d0783ff2c6f7001714053c5989af", aum = -2)
    public long fXv;
    @C0064Am(aul = "7802a8871959100cb8678c8f74dc16b0", aum = -2)
    public long fXw;
    @C0064Am(aul = "46a8f0d4db7353a2fdcc851c612fdeba", aum = -2)
    public long fXx;
    @C0064Am(aul = "515297e4703b00107693fca7abdd3bd2", aum = -2)
    public long fXy;
    @C0064Am(aul = "42741c9c78a11d7a45ec5d22f27b9a3a", aum = -1)
    public byte fXz;
    @C0064Am(aul = "377b306562f612978b2bfcc8ef2302de", aum = 1)
    public String handle;
    @C0064Am(aul = "78f4609055a4fec1fd7f63fb629d3dd2", aum = -2)

    /* renamed from: nm */
    public long f4883nm;
    @C0064Am(aul = "78f4609055a4fec1fd7f63fb629d3dd2", aum = -1)

    /* renamed from: np */
    public byte f4884np;
    @C0064Am(aul = "377b306562f612978b2bfcc8ef2302de", aum = -2)

    /* renamed from: ok */
    public long f4885ok;
    @C0064Am(aul = "377b306562f612978b2bfcc8ef2302de", aum = -1)

    /* renamed from: on */
    public byte f4886on;
    @C0064Am(aul = "0b6acb8bc885192a634ffe8b5e1d6f70", aum = 0)

    /* renamed from: wx */
    public Mission<? extends C4045yZ> f4887wx;

    public C6390aly() {
    }

    public C6390aly(C1616Xf xf) {
        super(xf);
    }

    /* renamed from: c */
    public C5663aRz[] mo309c() {
        return MissionTimer._m_fields;
    }

    /* renamed from: d */
    public void mo310d() {
        super.mo310d();
        this.f4887wx = null;
        this.handle = null;
        this.fXo = null;
        this.fXp = false;
        this.fXq = 0;
        this.fXr = 0.0f;
        this.fXs = false;
        this.fXt = 0;
        this.dJT = 0;
        this.f4885ok = 0;
        this.f4883nm = 0;
        this.fXu = 0;
        this.fXv = 0;
        this.fXw = 0;
        this.fXx = 0;
        this.fXy = 0;
        this.dKv = 0;
        this.f4886on = 0;
        this.f4884np = 0;
        this.fXz = 0;
        this.fXA = 0;
        this.fXB = 0;
        this.fXC = 0;
        this.fXD = 0;
    }
}
