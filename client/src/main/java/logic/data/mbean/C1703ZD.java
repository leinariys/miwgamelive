package logic.data.mbean;

import game.network.message.serializable.C1556Wo;
import game.script.ship.OutpostUpgrade;
import logic.baa.C1616Xf;
import logic.res.code.C5663aRz;
import p001a.C0064Am;
import p001a.C2348eJ;
import p001a.C2611hZ;

import java.util.UUID;

/* renamed from: a.ZD */
public class C1703ZD extends C5292aDs {
    @C0064Am(aul = "cd12be44a0655c4a1f94cbc097075e5b", aum = 0)
    public C2611hZ ayI;
    @C0064Am(aul = "7f329854002a45f7b9ff7ba5a5ee9997", aum = 4)

    /* renamed from: bK */
    public UUID f2223bK;
    @C0064Am(aul = "cd12be44a0655c4a1f94cbc097075e5b", aum = -2)
    public long bxR;
    @C0064Am(aul = "cd12be44a0655c4a1f94cbc097075e5b", aum = -1)
    public byte bxU;
    @C0064Am(aul = "60b679d9dd57a10ea51d4c1bdfa0f780", aum = 1)
    public long ePJ;
    @C0064Am(aul = "35da54b98d9e4c8aa2527acb2acc3de7", aum = 2)
    public float ePK;
    @C0064Am(aul = "a3aae3a8929388506c034ebbc6aaf45b", aum = 3)
    public int ePL;
    @C0064Am(aul = "4b51721b2c3934bf2f8cc345047fe820", aum = 6)
    public C1556Wo<C2348eJ, Integer> ePM;
    @C0064Am(aul = "60b679d9dd57a10ea51d4c1bdfa0f780", aum = -2)
    public long ePN;
    @C0064Am(aul = "35da54b98d9e4c8aa2527acb2acc3de7", aum = -2)
    public long ePO;
    @C0064Am(aul = "a3aae3a8929388506c034ebbc6aaf45b", aum = -2)
    public long ePP;
    @C0064Am(aul = "4b51721b2c3934bf2f8cc345047fe820", aum = -2)
    public long ePQ;
    @C0064Am(aul = "60b679d9dd57a10ea51d4c1bdfa0f780", aum = -1)
    public byte ePR;
    @C0064Am(aul = "35da54b98d9e4c8aa2527acb2acc3de7", aum = -1)
    public byte ePS;
    @C0064Am(aul = "a3aae3a8929388506c034ebbc6aaf45b", aum = -1)
    public byte ePT;
    @C0064Am(aul = "4b51721b2c3934bf2f8cc345047fe820", aum = -1)
    public byte ePU;
    @C0064Am(aul = "1ca5015d50838fc3f535f8a7d484ab19", aum = 5)
    public String handle;
    @C0064Am(aul = "7f329854002a45f7b9ff7ba5a5ee9997", aum = -2)

    /* renamed from: oL */
    public long f2224oL;
    @C0064Am(aul = "7f329854002a45f7b9ff7ba5a5ee9997", aum = -1)

    /* renamed from: oS */
    public byte f2225oS;
    @C0064Am(aul = "1ca5015d50838fc3f535f8a7d484ab19", aum = -2)

    /* renamed from: ok */
    public long f2226ok;
    @C0064Am(aul = "1ca5015d50838fc3f535f8a7d484ab19", aum = -1)

    /* renamed from: on */
    public byte f2227on;

    public C1703ZD() {
    }

    public C1703ZD(C1616Xf xf) {
        super(xf);
    }

    /* renamed from: c */
    public C5663aRz[] mo309c() {
        return OutpostUpgrade._m_fields;
    }

    /* renamed from: d */
    public void mo310d() {
        super.mo310d();
        this.ayI = null;
        this.ePJ = 0;
        this.ePK = 0.0f;
        this.ePL = 0;
        this.f2223bK = null;
        this.handle = null;
        this.ePM = null;
        this.bxR = 0;
        this.ePN = 0;
        this.ePO = 0;
        this.ePP = 0;
        this.f2224oL = 0;
        this.f2226ok = 0;
        this.ePQ = 0;
        this.bxU = 0;
        this.ePR = 0;
        this.ePS = 0;
        this.ePT = 0;
        this.f2225oS = 0;
        this.f2227on = 0;
        this.ePU = 0;
    }
}
