package logic.data.mbean;

import game.script.ship.categories.Fighter;
import logic.baa.C1616Xf;
import logic.res.code.C5663aRz;
import p001a.C0064Am;

/* renamed from: a.aVl  reason: case insensitive filesystem */
public class C5753aVl extends C6557apJ {
    @C0064Am(aul = "b8fbc343a0a7811040634459f4140841", aum = 0)
    public Fighter aTd;
    @C0064Am(aul = "b8fbc343a0a7811040634459f4140841", aum = -2)

    /* renamed from: dl */
    public long f3982dl;
    @C0064Am(aul = "b8fbc343a0a7811040634459f4140841", aum = -1)

    /* renamed from: ds */
    public byte f3983ds;

    public C5753aVl() {
    }

    public C5753aVl(C1616Xf xf) {
        super(xf);
    }

    /* renamed from: c */
    public C5663aRz[] mo309c() {
        return Fighter.ShipBoost._m_fields;
    }

    /* renamed from: d */
    public void mo310d() {
        super.mo310d();
        this.aTd = null;
        this.f3982dl = 0;
        this.f3983ds = 0;
    }
}
