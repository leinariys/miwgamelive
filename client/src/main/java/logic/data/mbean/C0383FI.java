package logic.data.mbean;

import game.network.message.serializable.C3438ra;
import game.script.faction.Faction;
import game.script.pda.DatabaseCategory;
import game.script.pda.TaikopediaEntry;
import game.script.resource.Asset;
import logic.baa.C1616Xf;
import logic.res.code.C5663aRz;
import p001a.C0064Am;
import taikodom.infra.script.I18NString;

import java.util.UUID;

/* renamed from: a.FI */
public class C0383FI extends C3805vD {
    @C0064Am(aul = "01373be1e563edf6cd2fd417e133add1", aum = -2)

    /* renamed from: Nf */
    public long f539Nf;
    @C0064Am(aul = "01373be1e563edf6cd2fd417e133add1", aum = -1)

    /* renamed from: Nh */
    public byte f540Nh;
    @C0064Am(aul = "6fc936bab74200fedbfa5b0597a160e9", aum = 13)

    /* renamed from: bK */
    public UUID f541bK;
    @C0064Am(aul = "df0038e8813855fa3fcfc403b9edf07a", aum = 4)
    public C3438ra<Faction> cWd;
    @C0064Am(aul = "f09589e5e64ce2ab3afda5418098214e", aum = 5)
    public C3438ra<Faction> cWe;
    @C0064Am(aul = "e3e85a3b65ef49f20ac3e2ec85d19a0b", aum = 9)
    public int cWf;
    @C0064Am(aul = "6377ff610424fdc253485ec6b10849fd", aum = 10)
    public int cWg;
    @C0064Am(aul = "a8b7d9363596623f2c780c55ca1276b3", aum = 11)
    public long cWh;
    @C0064Am(aul = "df0038e8813855fa3fcfc403b9edf07a", aum = -2)
    public long cWi;
    @C0064Am(aul = "f09589e5e64ce2ab3afda5418098214e", aum = -2)
    public long cWj;
    @C0064Am(aul = "e3e85a3b65ef49f20ac3e2ec85d19a0b", aum = -2)
    public long cWk;
    @C0064Am(aul = "6377ff610424fdc253485ec6b10849fd", aum = -2)
    public long cWl;
    @C0064Am(aul = "a8b7d9363596623f2c780c55ca1276b3", aum = -2)
    public long cWm;
    @C0064Am(aul = "df0038e8813855fa3fcfc403b9edf07a", aum = -1)
    public byte cWn;
    @C0064Am(aul = "f09589e5e64ce2ab3afda5418098214e", aum = -1)
    public byte cWo;
    @C0064Am(aul = "e3e85a3b65ef49f20ac3e2ec85d19a0b", aum = -1)
    public byte cWp;
    @C0064Am(aul = "6377ff610424fdc253485ec6b10849fd", aum = -1)
    public byte cWq;
    @C0064Am(aul = "a8b7d9363596623f2c780c55ca1276b3", aum = -1)
    public byte cWr;
    @C0064Am(aul = "641909968f3566016ee5a8b24d5a117d", aum = 0)
    public String handle;
    @C0064Am(aul = "3b5260d03c510100872172ff4e01bf6d", aum = -1)

    /* renamed from: jK */
    public byte f542jK;
    @C0064Am(aul = "78365578377015d444860deb33bdeddd", aum = 7)

    /* renamed from: jS */
    public DatabaseCategory f543jS;
    @C0064Am(aul = "c183c661fbd2daf29d37d3de9c5f7d0a", aum = 6)

    /* renamed from: jT */
    public C3438ra<TaikopediaEntry> f544jT;
    @C0064Am(aul = "16d32119c8015dc0de404e7fc61981b7", aum = 2)

    /* renamed from: jU */
    public Asset f545jU;
    @C0064Am(aul = "0b8a94e8468294381dae89fbfa8e5eac", aum = 12)

    /* renamed from: jV */
    public float f546jV;
    @C0064Am(aul = "78365578377015d444860deb33bdeddd", aum = -2)

    /* renamed from: jX */
    public long f547jX;
    @C0064Am(aul = "c183c661fbd2daf29d37d3de9c5f7d0a", aum = -2)

    /* renamed from: jY */
    public long f548jY;
    @C0064Am(aul = "16d32119c8015dc0de404e7fc61981b7", aum = -2)

    /* renamed from: jZ */
    public long f549jZ;
    @C0064Am(aul = "3b5260d03c510100872172ff4e01bf6d", aum = 1)

    /* renamed from: jn */
    public Asset f550jn;
    @C0064Am(aul = "3b5260d03c510100872172ff4e01bf6d", aum = -2)

    /* renamed from: jy */
    public long f551jy;
    @C0064Am(aul = "0b8a94e8468294381dae89fbfa8e5eac", aum = -2)

    /* renamed from: ka */
    public long f552ka;
    @C0064Am(aul = "78365578377015d444860deb33bdeddd", aum = -1)

    /* renamed from: kc */
    public byte f553kc;
    @C0064Am(aul = "c183c661fbd2daf29d37d3de9c5f7d0a", aum = -1)

    /* renamed from: kd */
    public byte f554kd;
    @C0064Am(aul = "16d32119c8015dc0de404e7fc61981b7", aum = -1)

    /* renamed from: ke */
    public byte f555ke;
    @C0064Am(aul = "0b8a94e8468294381dae89fbfa8e5eac", aum = -1)

    /* renamed from: kf */
    public byte f556kf;
    @C0064Am(aul = "87be180810a5f6bd6248a128a908ce1d", aum = 8)

    /* renamed from: nh */
    public I18NString f557nh;
    @C0064Am(aul = "87be180810a5f6bd6248a128a908ce1d", aum = -2)

    /* renamed from: nk */
    public long f558nk;
    @C0064Am(aul = "87be180810a5f6bd6248a128a908ce1d", aum = -1)

    /* renamed from: nn */
    public byte f559nn;
    @C0064Am(aul = "6fc936bab74200fedbfa5b0597a160e9", aum = -2)

    /* renamed from: oL */
    public long f560oL;
    @C0064Am(aul = "6fc936bab74200fedbfa5b0597a160e9", aum = -1)

    /* renamed from: oS */
    public byte f561oS;
    @C0064Am(aul = "641909968f3566016ee5a8b24d5a117d", aum = -2)

    /* renamed from: ok */
    public long f562ok;
    @C0064Am(aul = "641909968f3566016ee5a8b24d5a117d", aum = -1)

    /* renamed from: on */
    public byte f563on;
    @C0064Am(aul = "01373be1e563edf6cd2fd417e133add1", aum = 3)

    /* renamed from: zP */
    public I18NString f564zP;

    public C0383FI() {
    }

    public C0383FI(C1616Xf xf) {
        super(xf);
    }

    /* renamed from: c */
    public C5663aRz[] mo309c() {
        return Faction._m_fields;
    }

    /* renamed from: d */
    public void mo310d() {
        super.mo310d();
        this.handle = null;
        this.f550jn = null;
        this.f545jU = null;
        this.f564zP = null;
        this.cWd = null;
        this.cWe = null;
        this.f544jT = null;
        this.f543jS = null;
        this.f557nh = null;
        this.cWf = 0;
        this.cWg = 0;
        this.cWh = 0;
        this.f546jV = 0.0f;
        this.f541bK = null;
        this.f562ok = 0;
        this.f551jy = 0;
        this.f549jZ = 0;
        this.f539Nf = 0;
        this.cWi = 0;
        this.cWj = 0;
        this.f548jY = 0;
        this.f547jX = 0;
        this.f558nk = 0;
        this.cWk = 0;
        this.cWl = 0;
        this.cWm = 0;
        this.f552ka = 0;
        this.f560oL = 0;
        this.f563on = 0;
        this.f542jK = 0;
        this.f555ke = 0;
        this.f540Nh = 0;
        this.cWn = 0;
        this.cWo = 0;
        this.f554kd = 0;
        this.f553kc = 0;
        this.f559nn = 0;
        this.cWp = 0;
        this.cWq = 0;
        this.cWr = 0;
        this.f556kf = 0;
        this.f561oS = 0;
    }
}
