package logic.data.mbean;

import game.network.message.serializable.C3438ra;
import game.script.item.BlueprintType;
import game.script.npcchat.requirement.HasBlueprintItemSpeechRequirement;
import logic.baa.C1616Xf;
import logic.res.code.C5663aRz;
import p001a.C0064Am;

/* renamed from: a.yi */
public class C4059yi extends C5416aIm {
    @C0064Am(aul = "f15b425e40900e4a925e634506b823ca", aum = 0)
    public C3438ra<BlueprintType> bJI;
    @C0064Am(aul = "f15b425e40900e4a925e634506b823ca", aum = -2)
    public long bJJ;
    @C0064Am(aul = "f15b425e40900e4a925e634506b823ca", aum = -1)
    public byte bJK;

    public C4059yi() {
    }

    public C4059yi(C1616Xf xf) {
        super(xf);
    }

    /* renamed from: c */
    public C5663aRz[] mo309c() {
        return HasBlueprintItemSpeechRequirement._m_fields;
    }

    /* renamed from: d */
    public void mo310d() {
        super.mo310d();
        this.bJI = null;
        this.bJJ = 0;
        this.bJK = 0;
    }
}
