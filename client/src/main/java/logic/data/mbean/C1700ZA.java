package logic.data.mbean;

import game.network.message.serializable.C3438ra;
import game.script.npcchat.NPCSpeech;
import game.script.npcchat.PlayerSpeech;
import game.script.npcchat.SpeechAction;
import logic.baa.C1616Xf;
import logic.res.code.C5663aRz;
import p001a.C0064Am;

/* renamed from: a.ZA */
public class C1700ZA extends C0980OQ {
    @C0064Am(aul = "75ff800d2921235844692c1316a7ab68", aum = 2)
    public C3438ra<SpeechAction> chA;
    @C0064Am(aul = "f090480999d47e75377c659baf7f439c", aum = 0)
    public int chw;
    @C0064Am(aul = "03db3c899e128cc0128541f924bd7324", aum = 1)
    public C3438ra<NPCSpeech> chy;
    @C0064Am(aul = "f090480999d47e75377c659baf7f439c", aum = -2)
    public long ePD;
    @C0064Am(aul = "03db3c899e128cc0128541f924bd7324", aum = -2)
    public long ePE;
    @C0064Am(aul = "75ff800d2921235844692c1316a7ab68", aum = -2)
    public long ePF;
    @C0064Am(aul = "f090480999d47e75377c659baf7f439c", aum = -1)
    public byte ePG;
    @C0064Am(aul = "03db3c899e128cc0128541f924bd7324", aum = -1)
    public byte ePH;
    @C0064Am(aul = "75ff800d2921235844692c1316a7ab68", aum = -1)
    public byte ePI;

    public C1700ZA() {
    }

    public C1700ZA(C1616Xf xf) {
        super(xf);
    }

    /* renamed from: c */
    public C5663aRz[] mo309c() {
        return PlayerSpeech._m_fields;
    }

    /* renamed from: d */
    public void mo310d() {
        super.mo310d();
        this.chw = 0;
        this.chy = null;
        this.chA = null;
        this.ePD = 0;
        this.ePE = 0;
        this.ePF = 0;
        this.ePG = 0;
        this.ePH = 0;
        this.ePI = 0;
    }
}
