package logic.data.mbean;

import game.network.message.serializable.C3438ra;
import game.script.item.Merchandise;
import game.script.pda.DatabaseCategory;
import game.script.pda.TaikopediaEntry;
import game.script.resource.Asset;
import logic.baa.C1616Xf;
import logic.res.code.C5663aRz;
import p001a.C0064Am;

/* renamed from: a.aJB */
public class aJB extends C1359Tn {
    @C0064Am(aul = "d8fa7d7998da402ed79390bc57836782", aum = 3)
    public boolean dyX;
    @C0064Am(aul = "d8fa7d7998da402ed79390bc57836782", aum = -2)
    public long eLm;
    @C0064Am(aul = "d8fa7d7998da402ed79390bc57836782", aum = -1)
    public byte eLo;
    @C0064Am(aul = "d40ee4fa922933d1280c6af35a686131", aum = 0)

    /* renamed from: jS */
    public DatabaseCategory f3163jS;
    @C0064Am(aul = "9393cf680dc9574f1d4af750a1a58e7d", aum = 1)

    /* renamed from: jT */
    public C3438ra<TaikopediaEntry> f3164jT;
    @C0064Am(aul = "e3b2f70a726eadc1ead1f7836bc3a5b4", aum = 2)

    /* renamed from: jU */
    public Asset f3165jU;
    @C0064Am(aul = "9e52d38c0bd6adbbe72663da21b90ff2", aum = 4)

    /* renamed from: jV */
    public float f3166jV;
    @C0064Am(aul = "d40ee4fa922933d1280c6af35a686131", aum = -2)

    /* renamed from: jX */
    public long f3167jX;
    @C0064Am(aul = "9393cf680dc9574f1d4af750a1a58e7d", aum = -2)

    /* renamed from: jY */
    public long f3168jY;
    @C0064Am(aul = "e3b2f70a726eadc1ead1f7836bc3a5b4", aum = -2)

    /* renamed from: jZ */
    public long f3169jZ;
    @C0064Am(aul = "9e52d38c0bd6adbbe72663da21b90ff2", aum = -2)

    /* renamed from: ka */
    public long f3170ka;
    @C0064Am(aul = "d40ee4fa922933d1280c6af35a686131", aum = -1)

    /* renamed from: kc */
    public byte f3171kc;
    @C0064Am(aul = "9393cf680dc9574f1d4af750a1a58e7d", aum = -1)

    /* renamed from: kd */
    public byte f3172kd;
    @C0064Am(aul = "e3b2f70a726eadc1ead1f7836bc3a5b4", aum = -1)

    /* renamed from: ke */
    public byte f3173ke;
    @C0064Am(aul = "9e52d38c0bd6adbbe72663da21b90ff2", aum = -1)

    /* renamed from: kf */
    public byte f3174kf;

    public aJB() {
    }

    public aJB(C1616Xf xf) {
        super(xf);
    }

    /* renamed from: c */
    public C5663aRz[] mo309c() {
        return Merchandise._m_fields;
    }

    /* renamed from: d */
    public void mo310d() {
        super.mo310d();
        this.f3163jS = null;
        this.f3164jT = null;
        this.f3165jU = null;
        this.dyX = false;
        this.f3166jV = 0.0f;
        this.f3167jX = 0;
        this.f3168jY = 0;
        this.f3169jZ = 0;
        this.eLm = 0;
        this.f3170ka = 0;
        this.f3171kc = 0;
        this.f3172kd = 0;
        this.f3173ke = 0;
        this.eLo = 0;
        this.f3174kf = 0;
    }
}
