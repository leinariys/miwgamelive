package logic.data.mbean;

import game.script.resource.Asset;
import game.script.ship.CruiseSpeedType;
import logic.baa.C1616Xf;
import logic.res.code.C5663aRz;
import p001a.C0064Am;

/* renamed from: a.aKl  reason: case insensitive filesystem */
public class C5467aKl extends C6515aoT {
    @C0064Am(aul = "1fb7ebcd6a1ffdb4c6d93d7a9b8ad2d6", aum = -2)
    public long bIF;
    @C0064Am(aul = "1fb7ebcd6a1ffdb4c6d93d7a9b8ad2d6", aum = -1)
    public byte bIR;
    @C0064Am(aul = "1fb7ebcd6a1ffdb4c6d93d7a9b8ad2d6", aum = 6)
    public Asset bIu;
    @C0064Am(aul = "ac92b8d519161de5d11c422e54b62795", aum = -1)
    public byte bhA;
    @C0064Am(aul = "9c1368671cbe4d1a2930c900c7b800a5", aum = 0)
    public float bhk;
    @C0064Am(aul = "473d7c2655278e3ec7c240e1cff91438", aum = 2)
    public float bhl;
    @C0064Am(aul = "9c1368671cbe4d1a2930c900c7b800a5", aum = -2)
    public long bho;
    @C0064Am(aul = "3afdaafb44bd49243d26507d75d133f0", aum = -2)
    public long bhp;
    @C0064Am(aul = "473d7c2655278e3ec7c240e1cff91438", aum = -2)
    public long bhq;
    @C0064Am(aul = "ac92b8d519161de5d11c422e54b62795", aum = -2)
    public long bhr;
    @C0064Am(aul = "9c1368671cbe4d1a2930c900c7b800a5", aum = -1)
    public byte bhx;
    @C0064Am(aul = "3afdaafb44bd49243d26507d75d133f0", aum = -1)
    public byte bhy;
    @C0064Am(aul = "473d7c2655278e3ec7c240e1cff91438", aum = -1)
    public byte bhz;
    @C0064Am(aul = "d1f2f62b1bfa89ba924b0076e2221ba9", aum = 4)
    public float dGt;
    @C0064Am(aul = "3e49da931377a12ddc4330c4dbc6e46a", aum = 5)
    public float dGu;
    @C0064Am(aul = "f7ae2006ce5840a7b4ea8519c5e8719d", aum = 7)
    public Asset dGw;
    @C0064Am(aul = "d1f2f62b1bfa89ba924b0076e2221ba9", aum = -1)
    public byte dOA;
    @C0064Am(aul = "3e49da931377a12ddc4330c4dbc6e46a", aum = -1)
    public byte dOB;
    @C0064Am(aul = "f7ae2006ce5840a7b4ea8519c5e8719d", aum = -1)
    public byte dOC;
    @C0064Am(aul = "d1f2f62b1bfa89ba924b0076e2221ba9", aum = -2)
    public long dOs;
    @C0064Am(aul = "3e49da931377a12ddc4330c4dbc6e46a", aum = -2)
    public long dOt;
    @C0064Am(aul = "f7ae2006ce5840a7b4ea8519c5e8719d", aum = -2)
    public long dOu;
    @C0064Am(aul = "3afdaafb44bd49243d26507d75d133f0", aum = 1)

    /* renamed from: dW */
    public float f3263dW;
    @C0064Am(aul = "ac92b8d519161de5d11c422e54b62795", aum = 3)

    /* renamed from: dX */
    public float f3264dX;

    public C5467aKl() {
    }

    public C5467aKl(C1616Xf xf) {
        super(xf);
    }

    /* renamed from: c */
    public C5663aRz[] mo309c() {
        return CruiseSpeedType._m_fields;
    }

    /* renamed from: d */
    public void mo310d() {
        super.mo310d();
        this.bhk = 0.0f;
        this.f3263dW = 0.0f;
        this.bhl = 0.0f;
        this.f3264dX = 0.0f;
        this.dGt = 0.0f;
        this.dGu = 0.0f;
        this.bIu = null;
        this.dGw = null;
        this.bho = 0;
        this.bhp = 0;
        this.bhq = 0;
        this.bhr = 0;
        this.dOs = 0;
        this.dOt = 0;
        this.bIF = 0;
        this.dOu = 0;
        this.bhx = 0;
        this.bhy = 0;
        this.bhz = 0;
        this.bhA = 0;
        this.dOA = 0;
        this.dOB = 0;
        this.bIR = 0;
        this.dOC = 0;
    }
}
