package logic.data.mbean;

import game.script.avatar.AvatarProperty;
import logic.baa.C1616Xf;
import logic.res.code.C5663aRz;
import p001a.C0064Am;
import taikodom.infra.script.I18NString;

/* renamed from: a.fK */
public class C2424fK extends C5292aDs {
    @C0064Am(aul = "c99635b6da5d5135d4f4ec11107e70aa", aum = -2)

    /* renamed from: Nf */
    public long f7279Nf;
    @C0064Am(aul = "43d5c0aa4637767161a6ab20c346489b", aum = -2)

    /* renamed from: Ng */
    public long f7280Ng;
    @C0064Am(aul = "c99635b6da5d5135d4f4ec11107e70aa", aum = -1)

    /* renamed from: Nh */
    public byte f7281Nh;
    @C0064Am(aul = "43d5c0aa4637767161a6ab20c346489b", aum = -1)

    /* renamed from: Ni */
    public byte f7282Ni;
    @C0064Am(aul = "43d5c0aa4637767161a6ab20c346489b", aum = 1)
    public boolean restricted;
    @C0064Am(aul = "c99635b6da5d5135d4f4ec11107e70aa", aum = 0)

    /* renamed from: zP */
    public I18NString f7283zP;

    public C2424fK() {
    }

    public C2424fK(C1616Xf xf) {
        super(xf);
    }

    /* renamed from: c */
    public C5663aRz[] mo309c() {
        return AvatarProperty._m_fields;
    }

    /* renamed from: d */
    public void mo310d() {
        super.mo310d();
        this.f7283zP = null;
        this.restricted = false;
        this.f7279Nf = 0;
        this.f7280Ng = 0;
        this.f7281Nh = 0;
        this.f7282Ni = 0;
    }
}
