package logic.data.mbean;

import game.network.message.serializable.C1556Wo;
import game.network.message.serializable.C2686iZ;
import game.script.player.Player;
import game.script.progress.OniLogger;
import game.script.ship.Station;
import game.script.space.Node;
import logic.baa.C1616Xf;
import logic.res.code.C5663aRz;
import p001a.C0064Am;

/* renamed from: a.xy */
public class C4005xy extends C3805vD {
    @C0064Am(aul = "5607ae72736243f6f608e70a49355762", aum = -2)
    public long bGA;
    @C0064Am(aul = "71178f5f5acbc223bb8850b5facdc0c2", aum = -2)
    public long bGB;
    @C0064Am(aul = "07d2d523327766a68f47aa78938be465", aum = -2)
    public long bGC;
    @C0064Am(aul = "ce39aecba9ecd09dbe9e6feee6506dff", aum = -1)
    public byte bGD;
    @C0064Am(aul = "a3be18c79746f701ae066062242301c6", aum = -1)
    public byte bGE;
    @C0064Am(aul = "ea42992238ed9899efcf3024adde3413", aum = -1)
    public byte bGF;
    @C0064Am(aul = "5607ae72736243f6f608e70a49355762", aum = -1)
    public byte bGG;
    @C0064Am(aul = "71178f5f5acbc223bb8850b5facdc0c2", aum = -1)
    public byte bGH;
    @C0064Am(aul = "07d2d523327766a68f47aa78938be465", aum = -1)
    public byte bGI;
    @C0064Am(aul = "ce39aecba9ecd09dbe9e6feee6506dff", aum = 1)
    public long bGr;
    @C0064Am(aul = "a3be18c79746f701ae066062242301c6", aum = 2)
    public C1556Wo<Object, Integer> bGs;
    @C0064Am(aul = "ea42992238ed9899efcf3024adde3413", aum = 3)
    public C1556Wo<Object, Integer> bGt;
    @C0064Am(aul = "5607ae72736243f6f608e70a49355762", aum = 4)
    public C1556Wo<Object, Integer> bGu;
    @C0064Am(aul = "71178f5f5acbc223bb8850b5facdc0c2", aum = 5)
    public C2686iZ<Station> bGv;
    @C0064Am(aul = "07d2d523327766a68f47aa78938be465", aum = 6)
    public C2686iZ<Node> bGw;
    @C0064Am(aul = "ce39aecba9ecd09dbe9e6feee6506dff", aum = -2)
    public long bGx;
    @C0064Am(aul = "a3be18c79746f701ae066062242301c6", aum = -2)
    public long bGy;
    @C0064Am(aul = "ea42992238ed9899efcf3024adde3413", aum = -2)
    public long bGz;
    @C0064Am(aul = "07ccce613595bb4ac15d721ae71195dc", aum = 0)

    /* renamed from: nj */
    public Player f9565nj;
    @C0064Am(aul = "07ccce613595bb4ac15d721ae71195dc", aum = -2)

    /* renamed from: nm */
    public long f9566nm;
    @C0064Am(aul = "07ccce613595bb4ac15d721ae71195dc", aum = -1)

    /* renamed from: np */
    public byte f9567np;

    public C4005xy() {
    }

    public C4005xy(C1616Xf xf) {
        super(xf);
    }

    /* renamed from: c */
    public C5663aRz[] mo309c() {
        return OniLogger._m_fields;
    }

    /* renamed from: d */
    public void mo310d() {
        super.mo310d();
        this.f9565nj = null;
        this.bGr = 0;
        this.bGs = null;
        this.bGt = null;
        this.bGu = null;
        this.bGv = null;
        this.bGw = null;
        this.f9566nm = 0;
        this.bGx = 0;
        this.bGy = 0;
        this.bGz = 0;
        this.bGA = 0;
        this.bGB = 0;
        this.bGC = 0;
        this.f9567np = 0;
        this.bGD = 0;
        this.bGE = 0;
        this.bGF = 0;
        this.bGG = 0;
        this.bGH = 0;
        this.bGI = 0;
    }
}
