package logic.data.mbean;

import game.script.bank.BankAccountTransaction;
import game.script.player.Player;
import logic.baa.C1616Xf;
import logic.res.code.C5663aRz;
import p001a.C0064Am;

/* renamed from: a.xc */
public class C3975xc extends C3805vD {
    @C0064Am(aul = "26aa8ca0e67f3ec42e38ffebdba4aa29", aum = -2)
    public long aLG;
    @C0064Am(aul = "26aa8ca0e67f3ec42e38ffebdba4aa29", aum = -1)
    public byte aLL;
    @C0064Am(aul = "26aa8ca0e67f3ec42e38ffebdba4aa29", aum = 1)
    public float amount;
    @C0064Am(aul = "5a312b0a304d0a63c9094babc58e78a4", aum = 2)
    public Player bFB;
    @C0064Am(aul = "5a312b0a304d0a63c9094babc58e78a4", aum = -2)
    public long bFC;
    @C0064Am(aul = "5a312b0a304d0a63c9094babc58e78a4", aum = -1)
    public byte bFD;
    @C0064Am(aul = "bcdf1195da687546fd4e4cc8f8d2ec49", aum = 0)
    public String description;
    @C0064Am(aul = "bcdf1195da687546fd4e4cc8f8d2ec49", aum = -2)

    /* renamed from: nk */
    public long f9558nk;
    @C0064Am(aul = "bcdf1195da687546fd4e4cc8f8d2ec49", aum = -1)

    /* renamed from: nn */
    public byte f9559nn;

    public C3975xc() {
    }

    public C3975xc(C1616Xf xf) {
        super(xf);
    }

    /* renamed from: c */
    public C5663aRz[] mo309c() {
        return BankAccountTransaction._m_fields;
    }

    /* renamed from: d */
    public void mo310d() {
        super.mo310d();
        this.description = null;
        this.amount = 0.0f;
        this.bFB = null;
        this.f9558nk = 0;
        this.aLG = 0;
        this.bFC = 0;
        this.f9559nn = 0;
        this.aLL = 0;
        this.bFD = 0;
    }
}
