package logic.data.mbean;

import game.CollisionFXSet;
import game.network.message.externalizable.C6853aut;
import game.script.Actor;
import game.script.ai.npc.AIController;
import game.script.item.Weapon;
import game.script.resource.Asset;
import game.script.ship.Hull;
import game.script.ship.Shield;
import logic.baa.C0712KI;
import logic.baa.C1616Xf;
import logic.res.code.C5663aRz;
import p001a.C0064Am;
import taikodom.infra.script.I18NString;

/* renamed from: a.aJu  reason: case insensitive filesystem */
public class C5450aJu extends aLU {
    @C0064Am(aul = "06b50953535e73f56074a64b3d2aeb7f", aum = 8)

    /* renamed from: LQ */
    public Asset f3232LQ;
    @C0064Am(aul = "ca1407e33f95abf4b73f0586565befcd", aum = 13)

    /* renamed from: VV */
    public Hull f3233VV;
    @C0064Am(aul = "18b866291739e44fd992728ef6764a48", aum = 2)

    /* renamed from: Wb */
    public Asset f3234Wb;
    @C0064Am(aul = "dcc55802fe31146ac3a7dfab41d6c981", aum = 3)

    /* renamed from: Wd */
    public Asset f3235Wd;
    @C0064Am(aul = "a5de08ae820252204664f65ebbfdabe3", aum = 17)
    public Actor ams;
    @C0064Am(aul = "a5de08ae820252204664f65ebbfdabe3", aum = -2)
    public long aun;
    @C0064Am(aul = "a5de08ae820252204664f65ebbfdabe3", aum = -1)
    public byte auu;
    @C0064Am(aul = "c5e08cbacef2a482a2e7657e18002396", aum = -2)
    public long avd;
    @C0064Am(aul = "c5e08cbacef2a482a2e7657e18002396", aum = -1)
    public byte avi;
    @C0064Am(aul = "06b50953535e73f56074a64b3d2aeb7f", aum = -2)
    public long ayc;
    @C0064Am(aul = "06b50953535e73f56074a64b3d2aeb7f", aum = -1)
    public byte ayr;
    @C0064Am(aul = "c82bdf48929f8a64e00c603e86fff6ba", aum = -2)
    public long azg;
    @C0064Am(aul = "c82bdf48929f8a64e00c603e86fff6ba", aum = -1)
    public byte azv;
    @C0064Am(aul = "876d0a9a5ec733c1cf19e982b4d78eea", aum = -1)
    public byte bhA;
    @C0064Am(aul = "18b866291739e44fd992728ef6764a48", aum = -1)
    public byte bhC;
    @C0064Am(aul = "dcc55802fe31146ac3a7dfab41d6c981", aum = -1)
    public byte bhD;
    @C0064Am(aul = "ca1407e33f95abf4b73f0586565befcd", aum = -1)
    public byte bhF;
    @C0064Am(aul = "876d0a9a5ec733c1cf19e982b4d78eea", aum = -2)
    public long bhr;
    @C0064Am(aul = "18b866291739e44fd992728ef6764a48", aum = -2)
    public long bht;
    @C0064Am(aul = "dcc55802fe31146ac3a7dfab41d6c981", aum = -2)
    public long bhu;
    @C0064Am(aul = "ca1407e33f95abf4b73f0586565befcd", aum = -2)
    public long bhw;
    @C0064Am(aul = "37654ef3c1d78cd8786d39a6e152986e", aum = 1)
    public CollisionFXSet bnE;
    @C0064Am(aul = "32196209c66ca81731527a18d589ede1", aum = 7)
    public Asset boc;
    @C0064Am(aul = "876d0a9a5ec733c1cf19e982b4d78eea", aum = 4)

    /* renamed from: dX */
    public float f3236dX;
    @C0064Am(aul = "09cd2897e42eaca288b6d49ee368d59a", aum = 20)
    public C6853aut dcK;
    @C0064Am(aul = "45d3947abce784357294c6034574590b", aum = 14)
    public C6853aut dcL;
    @C0064Am(aul = "4ec1842c9e8eb3b5564f7af42d220996", aum = 16)
    public C6853aut dcM;
    @C0064Am(aul = "09cd2897e42eaca288b6d49ee368d59a", aum = -2)
    public long dcN;
    @C0064Am(aul = "45d3947abce784357294c6034574590b", aum = -2)
    public long dcO;
    @C0064Am(aul = "4ec1842c9e8eb3b5564f7af42d220996", aum = -2)
    public long dcP;
    @C0064Am(aul = "09cd2897e42eaca288b6d49ee368d59a", aum = -1)
    public byte dcQ;
    @C0064Am(aul = "45d3947abce784357294c6034574590b", aum = -1)
    public byte dcR;
    @C0064Am(aul = "4ec1842c9e8eb3b5564f7af42d220996", aum = -1)
    public byte dcS;
    @C0064Am(aul = "c82bdf48929f8a64e00c603e86fff6ba", aum = 0)
    public C0712KI dlU;
    @C0064Am(aul = "8d4ec14a4937ac1374f6d36f4e9ebd9d", aum = 6)
    public float dlX;
    @C0064Am(aul = "520c0cd302b56ad8b3311da24542949a", aum = 10)
    public I18NString dlZ;
    @C0064Am(aul = "67dd9776c29a68452754b949f32a9109", aum = 11)
    public Pawn dmb;
    @C0064Am(aul = "2a94bdc9a2c1251060e790f5abe7ec26", aum = 12)
    public C6853aut dmd;
    @C0064Am(aul = "9639d5298e8057846561142715c7b97a", aum = 15)
    public Shield dmi;
    @C0064Am(aul = "044df5ab7fe307ca481af58049ad6dad", aum = 18)
    public AIController dml;
    @C0064Am(aul = "37654ef3c1d78cd8786d39a6e152986e", aum = -2)
    public long fPc;
    @C0064Am(aul = "40e22df5592d3c6e8868088d69ab8bc0", aum = -2)
    public long fPd;
    @C0064Am(aul = "8d4ec14a4937ac1374f6d36f4e9ebd9d", aum = -2)
    public long fPe;
    @C0064Am(aul = "32196209c66ca81731527a18d589ede1", aum = -2)
    public long fPf;
    @C0064Am(aul = "520c0cd302b56ad8b3311da24542949a", aum = -2)
    public long fPg;
    @C0064Am(aul = "9639d5298e8057846561142715c7b97a", aum = -2)
    public long fPh;
    @C0064Am(aul = "c80aef915582df7b7cc0df24698d4a90", aum = -2)
    public long fPi;
    @C0064Am(aul = "37654ef3c1d78cd8786d39a6e152986e", aum = -1)
    public byte fPj;
    @C0064Am(aul = "40e22df5592d3c6e8868088d69ab8bc0", aum = -1)
    public byte fPk;
    @C0064Am(aul = "8d4ec14a4937ac1374f6d36f4e9ebd9d", aum = -1)
    public byte fPl;
    @C0064Am(aul = "32196209c66ca81731527a18d589ede1", aum = -1)
    public byte fPm;
    @C0064Am(aul = "520c0cd302b56ad8b3311da24542949a", aum = -1)
    public byte fPn;
    @C0064Am(aul = "9639d5298e8057846561142715c7b97a", aum = -1)
    public byte fPo;
    @C0064Am(aul = "c80aef915582df7b7cc0df24698d4a90", aum = -1)
    public byte fPp;
    @C0064Am(aul = "67dd9776c29a68452754b949f32a9109", aum = -2)
    public long igj;
    @C0064Am(aul = "2a94bdc9a2c1251060e790f5abe7ec26", aum = -2)
    public long igk;
    @C0064Am(aul = "044df5ab7fe307ca481af58049ad6dad", aum = -2)
    public long igl;
    @C0064Am(aul = "67dd9776c29a68452754b949f32a9109", aum = -1)
    public byte igm;
    @C0064Am(aul = "2a94bdc9a2c1251060e790f5abe7ec26", aum = -1)
    public byte ign;
    @C0064Am(aul = "044df5ab7fe307ca481af58049ad6dad", aum = -1)
    public byte igo;
    @C0064Am(aul = "40e22df5592d3c6e8868088d69ab8bc0", aum = 5)
    public float maxPitch;
    @C0064Am(aul = "c80aef915582df7b7cc0df24698d4a90", aum = 19)

    /* renamed from: oX */
    public Weapon f3237oX;
    @C0064Am(aul = "c5e08cbacef2a482a2e7657e18002396", aum = 9)

    /* renamed from: uT */
    public String f3238uT;

    public C5450aJu() {
    }

    public C5450aJu(C1616Xf xf) {
        super(xf);
    }

    /* renamed from: c */
    public C5663aRz[] mo309c() {
        return Turret._m_fields;
    }

    /* renamed from: d */
    public void mo310d() {
        super.mo310d();
        this.dlU = null;
        this.bnE = null;
        this.f3234Wb = null;
        this.f3235Wd = null;
        this.f3236dX = 0.0f;
        this.maxPitch = 0.0f;
        this.dlX = 0.0f;
        this.boc = null;
        this.f3232LQ = null;
        this.f3238uT = null;
        this.dlZ = null;
        this.dmb = null;
        this.dmd = null;
        this.f3233VV = null;
        this.dcL = null;
        this.dmi = null;
        this.dcM = null;
        this.ams = null;
        this.dml = null;
        this.f3237oX = null;
        this.dcK = null;
        this.azg = 0;
        this.fPc = 0;
        this.bht = 0;
        this.bhu = 0;
        this.bhr = 0;
        this.fPd = 0;
        this.fPe = 0;
        this.fPf = 0;
        this.ayc = 0;
        this.avd = 0;
        this.fPg = 0;
        this.igj = 0;
        this.igk = 0;
        this.bhw = 0;
        this.dcO = 0;
        this.fPh = 0;
        this.dcP = 0;
        this.aun = 0;
        this.igl = 0;
        this.fPi = 0;
        this.dcN = 0;
        this.azv = 0;
        this.fPj = 0;
        this.bhC = 0;
        this.bhD = 0;
        this.bhA = 0;
        this.fPk = 0;
        this.fPl = 0;
        this.fPm = 0;
        this.ayr = 0;
        this.avi = 0;
        this.fPn = 0;
        this.igm = 0;
        this.ign = 0;
        this.bhF = 0;
        this.dcR = 0;
        this.fPo = 0;
        this.dcS = 0;
        this.auu = 0;
        this.igo = 0;
        this.fPp = 0;
        this.dcQ = 0;
    }
}
