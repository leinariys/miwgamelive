package logic.data.mbean;

import game.script.item.buff.amplifier.HazardShieldAmplifier;
import logic.baa.C1616Xf;
import logic.res.code.C5663aRz;
import p001a.C0064Am;
import p001a.C3892wO;

/* renamed from: a.aVu  reason: case insensitive filesystem */
public class C5762aVu extends C3108nv {
    @C0064Am(aul = "64c9ed5a69ecb56dc2fb3accfc806b86", aum = 0)
    public C3892wO bbH;
    @C0064Am(aul = "9f2f56140d1a56211929dbef7edaec7d", aum = -2)
    public long dVm;
    @C0064Am(aul = "9f2f56140d1a56211929dbef7edaec7d", aum = -1)
    public byte dVn;
    @C0064Am(aul = "64c9ed5a69ecb56dc2fb3accfc806b86", aum = -2)
    public long gEW;
    @C0064Am(aul = "64c9ed5a69ecb56dc2fb3accfc806b86", aum = -1)
    public byte gEY;
    @C0064Am(aul = "9f2f56140d1a56211929dbef7edaec7d", aum = 1)
    public HazardShieldAmplifier.HazardShieldAmplififerAdapter iZw;

    public C5762aVu() {
    }

    public C5762aVu(C1616Xf xf) {
        super(xf);
    }

    /* renamed from: c */
    public C5663aRz[] mo309c() {
        return HazardShieldAmplifier._m_fields;
    }

    /* renamed from: d */
    public void mo310d() {
        super.mo310d();
        this.bbH = null;
        this.iZw = null;
        this.gEW = 0;
        this.dVm = 0;
        this.gEY = 0;
        this.dVn = 0;
    }
}
