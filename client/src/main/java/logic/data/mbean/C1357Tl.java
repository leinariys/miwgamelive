package logic.data.mbean;

import game.script.cloning.CloningDefaults;
import game.script.item.WeaponType;
import game.script.mission.MissionTemplate;
import game.script.npc.NPC;
import game.script.resource.Asset;
import game.script.ship.ShipType;
import game.script.ship.Station;
import logic.baa.C1616Xf;
import logic.res.code.C5663aRz;
import p001a.C0064Am;

import java.util.UUID;

/* renamed from: a.Tl */
public class C1357Tl extends C5292aDs {
    @C0064Am(aul = "1553d1a9a2406c11ab0fdb28e7f96636", aum = 13)

    /* renamed from: bK */
    public UUID f1717bK;
    @C0064Am(aul = "7b4759c5f86996cd2cfb138a0c618a71", aum = -2)
    public long eiA;
    @C0064Am(aul = "9447344e598bda92f83e9ffad7364feb", aum = -2)
    public long eiB;
    @C0064Am(aul = "fd6ca174ecf9f49b9d0d00346a68b117", aum = -2)
    public long eiC;
    @C0064Am(aul = "897a3b81038d80816d4208ab7ef3fd70", aum = -2)
    public long eiD;
    @C0064Am(aul = "4f3d808ae2e81c5b9903d5b3ea57aa4d", aum = -1)
    public byte eiE;
    @C0064Am(aul = "6f9d0e6fd9b75de1fda725cfaec373b7", aum = -1)
    public byte eiF;
    @C0064Am(aul = "cf5715530a6970f86b1f72d24734e44c", aum = -1)
    public byte eiG;
    @C0064Am(aul = "d3182ec68657a9cf606144250f45ae33", aum = -1)
    public byte eiH;
    @C0064Am(aul = "c606a3ef0719dfac8d8c25ec170dbe9f", aum = -1)
    public byte eiI;
    @C0064Am(aul = "3a6bb013438d2e1f0527bb9aa263403c", aum = -1)
    public byte eiJ;
    @C0064Am(aul = "4aa4ff338e56ccb5aa7a5b36155e5c1d", aum = -1)
    public byte eiK;
    @C0064Am(aul = "604a284e6c32db1c4766121903c0f67b", aum = -1)
    public byte eiL;
    @C0064Am(aul = "a5144cb76b15286de0656c65e0b50735", aum = -1)
    public byte eiM;
    @C0064Am(aul = "7b4759c5f86996cd2cfb138a0c618a71", aum = -1)
    public byte eiN;
    @C0064Am(aul = "9447344e598bda92f83e9ffad7364feb", aum = -1)
    public byte eiO;
    @C0064Am(aul = "fd6ca174ecf9f49b9d0d00346a68b117", aum = -1)
    public byte eiP;
    @C0064Am(aul = "897a3b81038d80816d4208ab7ef3fd70", aum = -1)
    public byte eiQ;
    @C0064Am(aul = "4f3d808ae2e81c5b9903d5b3ea57aa4d", aum = 0)
    public Station eie;
    @C0064Am(aul = "6f9d0e6fd9b75de1fda725cfaec373b7", aum = 1)
    public Station eif;
    @C0064Am(aul = "cf5715530a6970f86b1f72d24734e44c", aum = 2)
    public Asset eig;
    @C0064Am(aul = "d3182ec68657a9cf606144250f45ae33", aum = 3)
    public Asset eih;
    @C0064Am(aul = "c606a3ef0719dfac8d8c25ec170dbe9f", aum = 4)
    public NPC eii;
    @C0064Am(aul = "3a6bb013438d2e1f0527bb9aa263403c", aum = 5)
    public NPC eij;
    @C0064Am(aul = "4aa4ff338e56ccb5aa7a5b36155e5c1d", aum = 6)
    public MissionTemplate eik;
    @C0064Am(aul = "604a284e6c32db1c4766121903c0f67b", aum = 7)
    public MissionTemplate eil;
    @C0064Am(aul = "a5144cb76b15286de0656c65e0b50735", aum = 8)
    public ShipType eim;
    @C0064Am(aul = "7b4759c5f86996cd2cfb138a0c618a71", aum = 9)
    public ShipType ein;
    @C0064Am(aul = "9447344e598bda92f83e9ffad7364feb", aum = 10)
    public WeaponType eio;
    @C0064Am(aul = "fd6ca174ecf9f49b9d0d00346a68b117", aum = 11)
    public WeaponType eip;
    @C0064Am(aul = "897a3b81038d80816d4208ab7ef3fd70", aum = 12)
    public int eiq;
    @C0064Am(aul = "4f3d808ae2e81c5b9903d5b3ea57aa4d", aum = -2)
    public long eir;
    @C0064Am(aul = "6f9d0e6fd9b75de1fda725cfaec373b7", aum = -2)
    public long eis;
    @C0064Am(aul = "cf5715530a6970f86b1f72d24734e44c", aum = -2)
    public long eit;
    @C0064Am(aul = "d3182ec68657a9cf606144250f45ae33", aum = -2)
    public long eiu;
    @C0064Am(aul = "c606a3ef0719dfac8d8c25ec170dbe9f", aum = -2)
    public long eiv;
    @C0064Am(aul = "3a6bb013438d2e1f0527bb9aa263403c", aum = -2)
    public long eiw;
    @C0064Am(aul = "4aa4ff338e56ccb5aa7a5b36155e5c1d", aum = -2)
    public long eix;
    @C0064Am(aul = "604a284e6c32db1c4766121903c0f67b", aum = -2)
    public long eiy;
    @C0064Am(aul = "a5144cb76b15286de0656c65e0b50735", aum = -2)
    public long eiz;
    @C0064Am(aul = "1553d1a9a2406c11ab0fdb28e7f96636", aum = -2)

    /* renamed from: oL */
    public long f1718oL;
    @C0064Am(aul = "1553d1a9a2406c11ab0fdb28e7f96636", aum = -1)

    /* renamed from: oS */
    public byte f1719oS;

    public C1357Tl() {
    }

    public C1357Tl(C1616Xf xf) {
        super(xf);
    }

    /* renamed from: c */
    public C5663aRz[] mo309c() {
        return CloningDefaults._m_fields;
    }

    /* renamed from: d */
    public void mo310d() {
        super.mo310d();
        this.eie = null;
        this.eif = null;
        this.eig = null;
        this.eih = null;
        this.eii = null;
        this.eij = null;
        this.eik = null;
        this.eil = null;
        this.eim = null;
        this.ein = null;
        this.eio = null;
        this.eip = null;
        this.eiq = 0;
        this.f1717bK = null;
        this.eir = 0;
        this.eis = 0;
        this.eit = 0;
        this.eiu = 0;
        this.eiv = 0;
        this.eiw = 0;
        this.eix = 0;
        this.eiy = 0;
        this.eiz = 0;
        this.eiA = 0;
        this.eiB = 0;
        this.eiC = 0;
        this.eiD = 0;
        this.f1718oL = 0;
        this.eiE = 0;
        this.eiF = 0;
        this.eiG = 0;
        this.eiH = 0;
        this.eiI = 0;
        this.eiJ = 0;
        this.eiK = 0;
        this.eiL = 0;
        this.eiM = 0;
        this.eiN = 0;
        this.eiO = 0;
        this.eiP = 0;
        this.eiQ = 0;
        this.f1719oS = 0;
    }
}
