package logic.data.mbean;

import game.script.citizenship.inprovements.InsuranceImprovementType;
import logic.baa.C1616Xf;
import logic.res.code.C5663aRz;
import p001a.C0064Am;
import taikodom.infra.script.I18NString;

/* renamed from: a.El */
public class C0357El extends C6071afr {
    @C0064Am(aul = "22957b91567e2702c58cd9e8288e4aa3", aum = 1)
    public I18NString cRI;
    @C0064Am(aul = "3427bc96b1539e3315e4c14ac2c39b15", aum = -2)
    public long cRJ;
    @C0064Am(aul = "22957b91567e2702c58cd9e8288e4aa3", aum = -2)
    public long cRK;
    @C0064Am(aul = "3427bc96b1539e3315e4c14ac2c39b15", aum = -1)
    public byte cRL;
    @C0064Am(aul = "22957b91567e2702c58cd9e8288e4aa3", aum = -1)
    public byte cRM;
    @C0064Am(aul = "3427bc96b1539e3315e4c14ac2c39b15", aum = 0)
    public long ciA;

    public C0357El() {
    }

    public C0357El(C1616Xf xf) {
        super(xf);
    }

    /* renamed from: c */
    public C5663aRz[] mo309c() {
        return InsuranceImprovementType._m_fields;
    }

    /* renamed from: d */
    public void mo310d() {
        super.mo310d();
        this.ciA = 0;
        this.cRI = null;
        this.cRJ = 0;
        this.cRK = 0;
        this.cRL = 0;
        this.cRM = 0;
    }
}
