package logic.data.mbean;

import game.script.cloning.CloningCenter;
import logic.baa.C1616Xf;
import logic.res.code.C5663aRz;
import p001a.C0064Am;

import java.util.UUID;

/* renamed from: a.Jb */
public class C0675Jb extends C3805vD {
    @C0064Am(aul = "00db1514eaa1fccb22508c8a8388737d", aum = 0)
    public long asW;
    @C0064Am(aul = "720ba065f4b318f3d920f4fdf3e8253e", aum = 1)

    /* renamed from: bK */
    public UUID f799bK;
    @C0064Am(aul = "00db1514eaa1fccb22508c8a8388737d", aum = -2)
    public long dhW;
    @C0064Am(aul = "00db1514eaa1fccb22508c8a8388737d", aum = -1)
    public byte dhX;
    @C0064Am(aul = "d1b56b39d600b447c59f70d1bf374669", aum = 2)
    public String handle;
    @C0064Am(aul = "720ba065f4b318f3d920f4fdf3e8253e", aum = -2)

    /* renamed from: oL */
    public long f800oL;
    @C0064Am(aul = "720ba065f4b318f3d920f4fdf3e8253e", aum = -1)

    /* renamed from: oS */
    public byte f801oS;
    @C0064Am(aul = "d1b56b39d600b447c59f70d1bf374669", aum = -2)

    /* renamed from: ok */
    public long f802ok;
    @C0064Am(aul = "d1b56b39d600b447c59f70d1bf374669", aum = -1)

    /* renamed from: on */
    public byte f803on;

    public C0675Jb() {
    }

    public C0675Jb(C1616Xf xf) {
        super(xf);
    }

    /* renamed from: c */
    public C5663aRz[] mo309c() {
        return CloningCenter._m_fields;
    }

    /* renamed from: d */
    public void mo310d() {
        super.mo310d();
        this.asW = 0;
        this.f799bK = null;
        this.handle = null;
        this.dhW = 0;
        this.f800oL = 0;
        this.f802ok = 0;
        this.dhX = 0;
        this.f801oS = 0;
        this.f803on = 0;
    }
}
