package logic.data.mbean;

import game.script.resource.Asset;
import game.script.ship.categories.Fighter;
import game.script.ship.speedBoost.SpeedBoost;
import game.script.ship.speedBoost.SpeedBoostAdapter;
import game.script.util.AdapterLikedList;
import logic.baa.C1616Xf;
import logic.res.code.C5663aRz;
import p001a.C0064Am;
import p001a.C3892wO;

/* renamed from: a.CP */
public class C0190CP extends C3805vD {
    @C0064Am(aul = "c5481a5757300575a39f344d8e6424d2", aum = 3)

    /* renamed from: Vs */
    public C3892wO f290Vs;
    @C0064Am(aul = "7a197da26d390cc52864874ce81c27d1", aum = 4)

    /* renamed from: Vu */
    public C3892wO f291Vu;
    @C0064Am(aul = "991f2ce6602ae1e76d0a859198768e66", aum = 6)

    /* renamed from: Vw */
    public C3892wO f292Vw;
    @C0064Am(aul = "38192dd22a608dd174a0927455b08b17", aum = 5)

    /* renamed from: Vy */
    public C3892wO f293Vy;
    @C0064Am(aul = "c5481a5757300575a39f344d8e6424d2", aum = -1)

    /* renamed from: YC */
    public byte f294YC;
    @C0064Am(aul = "7a197da26d390cc52864874ce81c27d1", aum = -1)

    /* renamed from: YD */
    public byte f295YD;
    @C0064Am(aul = "991f2ce6602ae1e76d0a859198768e66", aum = -1)

    /* renamed from: YE */
    public byte f296YE;
    @C0064Am(aul = "38192dd22a608dd174a0927455b08b17", aum = -1)

    /* renamed from: YF */
    public byte f297YF;
    @C0064Am(aul = "c5481a5757300575a39f344d8e6424d2", aum = -2)

    /* renamed from: Yw */
    public long f298Yw;
    @C0064Am(aul = "7a197da26d390cc52864874ce81c27d1", aum = -2)

    /* renamed from: Yx */
    public long f299Yx;
    @C0064Am(aul = "991f2ce6602ae1e76d0a859198768e66", aum = -2)

    /* renamed from: Yy */
    public long f300Yy;
    @C0064Am(aul = "38192dd22a608dd174a0927455b08b17", aum = -2)

    /* renamed from: Yz */
    public long f301Yz;
    @C0064Am(aul = "40a95949b8fd149603eb2ebeda37b59d", aum = 0)
    public float aAc;
    @C0064Am(aul = "c31b95be69859ca58b89f38b3fc951df", aum = 1)
    public Asset aAd;
    @C0064Am(aul = "f456366e1fbed3f68777697d0187e42d", aum = 2)
    public Asset aAf;
    @C0064Am(aul = "2ad9b60f498cde072627c0090a683f52", aum = 13)
    public AdapterLikedList<SpeedBoostAdapter> aOw;
    @C0064Am(aul = "2ad9b60f498cde072627c0090a683f52", aum = -2)
    public long aUE;
    @C0064Am(aul = "2ad9b60f498cde072627c0090a683f52", aum = -1)
    public byte aUH;
    @C0064Am(aul = "d5faa07f87438c63f89d252f0b7e27b8", aum = 14)
    public float bjR;
    @C0064Am(aul = "40a95949b8fd149603eb2ebeda37b59d", aum = -2)
    public long cAA;
    @C0064Am(aul = "c31b95be69859ca58b89f38b3fc951df", aum = -2)
    public long cAB;
    @C0064Am(aul = "f456366e1fbed3f68777697d0187e42d", aum = -2)
    public long cAC;
    @C0064Am(aul = "0c558e45d7eb65103301cc83497706da", aum = -2)
    public long cAD;
    @C0064Am(aul = "d30afa60074c05f1ea8779a4893ff528", aum = -2)
    public long cAE;
    @C0064Am(aul = "5b95ca178cb140f7b3e6332a512b185e", aum = -2)
    public long cAF;
    @C0064Am(aul = "7c564dddd7183fb74579a8c4ff87624b", aum = -2)
    public long cAG;
    @C0064Am(aul = "fd6e363bcea1470b09789e7de2087770", aum = -2)
    public long cAH;
    @C0064Am(aul = "ae16bc1c20af4ae3eb3b117da5694f4b", aum = -2)
    public long cAI;
    @C0064Am(aul = "d5faa07f87438c63f89d252f0b7e27b8", aum = -2)
    public long cAJ;
    @C0064Am(aul = "ba6c37c89179ce356fd81c714cc8dcdf", aum = -2)
    public long cAK;
    @C0064Am(aul = "968a3a24fd91de82e7bedbc81372a164", aum = -2)
    public long cAL;
    @C0064Am(aul = "a8a70da906f6114eae3c330fe9deca84", aum = -2)
    public long cAM;
    @C0064Am(aul = "aea7bdcc26ff3f48c0491bb15cb22783", aum = -2)
    public long cAN;
    @C0064Am(aul = "40a95949b8fd149603eb2ebeda37b59d", aum = -1)
    public byte cAO;
    @C0064Am(aul = "c31b95be69859ca58b89f38b3fc951df", aum = -1)
    public byte cAP;
    @C0064Am(aul = "f456366e1fbed3f68777697d0187e42d", aum = -1)
    public byte cAQ;
    @C0064Am(aul = "0c558e45d7eb65103301cc83497706da", aum = -1)
    public byte cAR;
    @C0064Am(aul = "d30afa60074c05f1ea8779a4893ff528", aum = -1)
    public byte cAS;
    @C0064Am(aul = "5b95ca178cb140f7b3e6332a512b185e", aum = -1)
    public byte cAT;
    @C0064Am(aul = "7c564dddd7183fb74579a8c4ff87624b", aum = -1)
    public byte cAU;
    @C0064Am(aul = "fd6e363bcea1470b09789e7de2087770", aum = -1)
    public byte cAV;
    @C0064Am(aul = "ae16bc1c20af4ae3eb3b117da5694f4b", aum = -1)
    public byte cAW;
    @C0064Am(aul = "d5faa07f87438c63f89d252f0b7e27b8", aum = -1)
    public byte cAX;
    @C0064Am(aul = "ba6c37c89179ce356fd81c714cc8dcdf", aum = -1)
    public byte cAY;
    @C0064Am(aul = "968a3a24fd91de82e7bedbc81372a164", aum = -1)
    public byte cAZ;
    @C0064Am(aul = "0c558e45d7eb65103301cc83497706da", aum = 7)
    public Fighter cAq;
    @C0064Am(aul = "d30afa60074c05f1ea8779a4893ff528", aum = 8)
    public SpeedBoost.C2286b cAr;
    @C0064Am(aul = "5b95ca178cb140f7b3e6332a512b185e", aum = 9)
    public float cAs;
    @C0064Am(aul = "7c564dddd7183fb74579a8c4ff87624b", aum = 10)
    public long cAt;
    @C0064Am(aul = "fd6e363bcea1470b09789e7de2087770", aum = 11)
    public long cAu;
    @C0064Am(aul = "ae16bc1c20af4ae3eb3b117da5694f4b", aum = 12)
    public int cAv;
    @C0064Am(aul = "ba6c37c89179ce356fd81c714cc8dcdf", aum = 15)
    public C3892wO cAw;
    @C0064Am(aul = "968a3a24fd91de82e7bedbc81372a164", aum = 16)
    public C3892wO cAx;
    @C0064Am(aul = "a8a70da906f6114eae3c330fe9deca84", aum = 17)
    public C3892wO cAy;
    @C0064Am(aul = "aea7bdcc26ff3f48c0491bb15cb22783", aum = 18)
    public C3892wO cAz;
    @C0064Am(aul = "a8a70da906f6114eae3c330fe9deca84", aum = -1)
    public byte cBa;
    @C0064Am(aul = "aea7bdcc26ff3f48c0491bb15cb22783", aum = -1)
    public byte cBb;

    public C0190CP() {
    }

    public C0190CP(C1616Xf xf) {
        super(xf);
    }

    /* renamed from: c */
    public C5663aRz[] mo309c() {
        return SpeedBoost._m_fields;
    }

    /* renamed from: d */
    public void mo310d() {
        super.mo310d();
        this.aAc = 0.0f;
        this.aAd = null;
        this.aAf = null;
        this.f290Vs = null;
        this.f291Vu = null;
        this.f293Vy = null;
        this.f292Vw = null;
        this.cAq = null;
        this.cAr = null;
        this.cAs = 0.0f;
        this.cAt = 0;
        this.cAu = 0;
        this.cAv = 0;
        this.aOw = null;
        this.bjR = 0.0f;
        this.cAw = null;
        this.cAx = null;
        this.cAy = null;
        this.cAz = null;
        this.cAA = 0;
        this.cAB = 0;
        this.cAC = 0;
        this.f298Yw = 0;
        this.f299Yx = 0;
        this.f301Yz = 0;
        this.f300Yy = 0;
        this.cAD = 0;
        this.cAE = 0;
        this.cAF = 0;
        this.cAG = 0;
        this.cAH = 0;
        this.cAI = 0;
        this.aUE = 0;
        this.cAJ = 0;
        this.cAK = 0;
        this.cAL = 0;
        this.cAM = 0;
        this.cAN = 0;
        this.cAO = 0;
        this.cAP = 0;
        this.cAQ = 0;
        this.f294YC = 0;
        this.f295YD = 0;
        this.f297YF = 0;
        this.f296YE = 0;
        this.cAR = 0;
        this.cAS = 0;
        this.cAT = 0;
        this.cAU = 0;
        this.cAV = 0;
        this.cAW = 0;
        this.aUH = 0;
        this.cAX = 0;
        this.cAY = 0;
        this.cAZ = 0;
        this.cBa = 0;
        this.cBb = 0;
    }
}
