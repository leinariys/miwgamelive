package logic.data.mbean;

import game.script.player.Player;
import game.script.support.Ticket;
import game.script.support.TicketChat;
import logic.baa.C1616Xf;
import logic.res.code.C5663aRz;
import p001a.C0064Am;

/* renamed from: a.aNy  reason: case insensitive filesystem */
public class C5558aNy extends C3805vD {
    @C0064Am(aul = "684cd3586fa34844fcafba6ecb470852", aum = 2)

    /* renamed from: Nk */
    public String f3441Nk;
    @C0064Am(aul = "bb352fa9335b7d9b8d28908474884eeb", aum = 3)

    /* renamed from: Nm */
    public Player f3442Nm;
    @C0064Am(aul = "858b1e90d99c9fd0db4bc0307b875da5", aum = 4)

    /* renamed from: No */
    public TicketChat f3443No;
    @C0064Am(aul = "92fab5664c7bc1cf8ae08572fa9c99f6", aum = 5)

    /* renamed from: Nq */
    public boolean f3444Nq;
    @C0064Am(aul = "07a9c351a6320637b8ad032cc93f431b", aum = 7)

    /* renamed from: Ns */
    public long f3445Ns;
    @C0064Am(aul = "2cb5841df1f346eb98ce7cccddb5caa8", aum = 8)

    /* renamed from: Nu */
    public String f3446Nu;
    @C0064Am(aul = "969b91ccf3832c1292d023733bacea78", aum = 9)

    /* renamed from: Nw */
    public long f3447Nw;
    @C0064Am(aul = "45d1953441ca63619808a19eb3b65b06", aum = 0)

    /* renamed from: P */
    public Player f3448P;
    @C0064Am(aul = "8c421f4424c4c4856ef7b7d9c6fb1aec", aum = 6)

    /* renamed from: Q */
    public long f3449Q;
    @C0064Am(aul = "45d1953441ca63619808a19eb3b65b06", aum = -2)

    /* renamed from: ag */
    public long f3450ag;
    @C0064Am(aul = "8c421f4424c4c4856ef7b7d9c6fb1aec", aum = -2)

    /* renamed from: ah */
    public long f3451ah;
    @C0064Am(aul = "45d1953441ca63619808a19eb3b65b06", aum = -1)

    /* renamed from: av */
    public byte f3452av;
    @C0064Am(aul = "8c421f4424c4c4856ef7b7d9c6fb1aec", aum = -1)

    /* renamed from: aw */
    public byte f3453aw;
    @C0064Am(aul = "858b1e90d99c9fd0db4bc0307b875da5", aum = -2)
    public long azi;
    @C0064Am(aul = "858b1e90d99c9fd0db4bc0307b875da5", aum = -1)
    public byte azx;
    @C0064Am(aul = "85117edee70fdbd5ab6bc0b1c4d1310a", aum = -2)
    public long bZp;
    @C0064Am(aul = "85117edee70fdbd5ab6bc0b1c4d1310a", aum = -1)
    public byte bZv;
    @C0064Am(aul = "684cd3586fa34844fcafba6ecb470852", aum = -2)
    public long hhs;
    @C0064Am(aul = "684cd3586fa34844fcafba6ecb470852", aum = -1)
    public byte hhu;
    @C0064Am(aul = "969b91ccf3832c1292d023733bacea78", aum = -1)
    public byte itA;
    @C0064Am(aul = "bb352fa9335b7d9b8d28908474884eeb", aum = -2)
    public long itr;
    @C0064Am(aul = "92fab5664c7bc1cf8ae08572fa9c99f6", aum = -2)
    public long its;
    @C0064Am(aul = "07a9c351a6320637b8ad032cc93f431b", aum = -2)
    public long itt;
    @C0064Am(aul = "2cb5841df1f346eb98ce7cccddb5caa8", aum = -2)
    public long itu;
    @C0064Am(aul = "969b91ccf3832c1292d023733bacea78", aum = -2)
    public long itv;
    @C0064Am(aul = "bb352fa9335b7d9b8d28908474884eeb", aum = -1)
    public byte itw;
    @C0064Am(aul = "92fab5664c7bc1cf8ae08572fa9c99f6", aum = -1)
    public byte itx;
    @C0064Am(aul = "07a9c351a6320637b8ad032cc93f431b", aum = -1)
    public byte ity;
    @C0064Am(aul = "2cb5841df1f346eb98ce7cccddb5caa8", aum = -1)
    public byte itz;
    @C0064Am(aul = "85117edee70fdbd5ab6bc0b1c4d1310a", aum = 1)
    public String subject;

    public C5558aNy() {
    }

    public C5558aNy(C1616Xf xf) {
        super(xf);
    }

    /* renamed from: c */
    public C5663aRz[] mo309c() {
        return Ticket._m_fields;
    }

    /* renamed from: d */
    public void mo310d() {
        super.mo310d();
        this.f3448P = null;
        this.subject = null;
        this.f3441Nk = null;
        this.f3442Nm = null;
        this.f3443No = null;
        this.f3444Nq = false;
        this.f3449Q = 0;
        this.f3445Ns = 0;
        this.f3446Nu = null;
        this.f3447Nw = 0;
        this.f3450ag = 0;
        this.bZp = 0;
        this.hhs = 0;
        this.itr = 0;
        this.azi = 0;
        this.its = 0;
        this.f3451ah = 0;
        this.itt = 0;
        this.itu = 0;
        this.itv = 0;
        this.f3452av = 0;
        this.bZv = 0;
        this.hhu = 0;
        this.itw = 0;
        this.azx = 0;
        this.itx = 0;
        this.f3453aw = 0;
        this.ity = 0;
        this.itz = 0;
        this.itA = 0;
    }
}
