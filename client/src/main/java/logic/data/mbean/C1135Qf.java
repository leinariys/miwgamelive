package logic.data.mbean;

import game.geometry.Vec3d;
import game.script.ai.npc.PirateAIController;
import game.script.ship.Ship;
import logic.baa.C1616Xf;
import logic.res.code.C5663aRz;
import p001a.C0064Am;

/* renamed from: a.Qf */
public class C1135Qf extends C6937awc {
    @C0064Am(aul = "f35487a4275ba5525c17e23902c968cb", aum = -2)
    public long aPM;
    @C0064Am(aul = "f35487a4275ba5525c17e23902c968cb", aum = -1)
    public byte aPO;
    @C0064Am(aul = "2ae87d321d7b7f904c8eb09058ec60da", aum = -2)
    public long dUA;
    @C0064Am(aul = "2aece51e581509f20f087aa956e244bb", aum = -2)
    public long dUB;
    @C0064Am(aul = "fe5c18954979de7f01a4e1a3ef7e8b6c", aum = -2)
    public long dUC;
    @C0064Am(aul = "65681454f831028bba7585aedd5572c8", aum = -2)
    public long dUD;
    @C0064Am(aul = "dd03277b72a3ae16e56b6d9f96d8f833", aum = -2)
    public long dUE;
    @C0064Am(aul = "1e7125b38bbddb12ddab927c5057d4f7", aum = -2)
    public long dUF;
    @C0064Am(aul = "b1b4db826a3549e8ca040f978b5bf495", aum = -2)
    public long dUG;
    @C0064Am(aul = "7017f723033ad16ded186e11ba6ec8c8", aum = -2)
    public long dUH;
    @C0064Am(aul = "1b68126aee265191fad3e6a791fd795c", aum = -2)
    public long dUI;
    @C0064Am(aul = "8bc1ccda5443662d8ba19b1c72b72f04", aum = -2)
    public long dUJ;
    @C0064Am(aul = "e545d9b798cf18a4a3a700a159c68427", aum = -2)
    public long dUK;
    @C0064Am(aul = "02369f273a708a55ceffaf0735e6e9a0", aum = -2)
    public long dUL;
    @C0064Am(aul = "d0d40ca35757cbbea2f8eb02e49bcfe0", aum = -2)
    public long dUM;
    @C0064Am(aul = "2ae87d321d7b7f904c8eb09058ec60da", aum = -1)
    public byte dUN;
    @C0064Am(aul = "2aece51e581509f20f087aa956e244bb", aum = -1)
    public byte dUO;
    @C0064Am(aul = "fe5c18954979de7f01a4e1a3ef7e8b6c", aum = -1)
    public byte dUP;
    @C0064Am(aul = "65681454f831028bba7585aedd5572c8", aum = -1)
    public byte dUQ;
    @C0064Am(aul = "dd03277b72a3ae16e56b6d9f96d8f833", aum = -1)
    public byte dUR;
    @C0064Am(aul = "1e7125b38bbddb12ddab927c5057d4f7", aum = -1)
    public byte dUS;
    @C0064Am(aul = "b1b4db826a3549e8ca040f978b5bf495", aum = -1)
    public byte dUT;
    @C0064Am(aul = "7017f723033ad16ded186e11ba6ec8c8", aum = -1)
    public byte dUU;
    @C0064Am(aul = "1b68126aee265191fad3e6a791fd795c", aum = -1)
    public byte dUV;
    @C0064Am(aul = "8bc1ccda5443662d8ba19b1c72b72f04", aum = -1)
    public byte dUW;
    @C0064Am(aul = "e545d9b798cf18a4a3a700a159c68427", aum = -1)
    public byte dUX;
    @C0064Am(aul = "02369f273a708a55ceffaf0735e6e9a0", aum = -1)
    public byte dUY;
    @C0064Am(aul = "d0d40ca35757cbbea2f8eb02e49bcfe0", aum = -1)
    public byte dUZ;
    @C0064Am(aul = "2ae87d321d7b7f904c8eb09058ec60da", aum = 0)
    public float dUn;
    @C0064Am(aul = "2aece51e581509f20f087aa956e244bb", aum = 1)
    public float dUo;
    @C0064Am(aul = "fe5c18954979de7f01a4e1a3ef7e8b6c", aum = 2)
    public float dUp;
    @C0064Am(aul = "65681454f831028bba7585aedd5572c8", aum = 3)
    public float dUq;
    @C0064Am(aul = "dd03277b72a3ae16e56b6d9f96d8f833", aum = 4)
    public float dUr;
    @C0064Am(aul = "1e7125b38bbddb12ddab927c5057d4f7", aum = 5)
    public long dUs;
    @C0064Am(aul = "b1b4db826a3549e8ca040f978b5bf495", aum = 6)
    public long dUt;
    @C0064Am(aul = "7017f723033ad16ded186e11ba6ec8c8", aum = 7)
    public long dUu;
    @C0064Am(aul = "1b68126aee265191fad3e6a791fd795c", aum = 8)
    public float dUv;
    @C0064Am(aul = "8bc1ccda5443662d8ba19b1c72b72f04", aum = 9)
    public Ship dUw;
    @C0064Am(aul = "e545d9b798cf18a4a3a700a159c68427", aum = 10)
    public float dUx;
    @C0064Am(aul = "02369f273a708a55ceffaf0735e6e9a0", aum = 11)
    public long dUy;
    @C0064Am(aul = "d0d40ca35757cbbea2f8eb02e49bcfe0", aum = 12)
    public Vec3d dUz;
    @C0064Am(aul = "f35487a4275ba5525c17e23902c968cb", aum = 13)
    public float radius2;

    public C1135Qf() {
    }

    public C1135Qf(C1616Xf xf) {
        super(xf);
    }

    /* renamed from: c */
    public C5663aRz[] mo309c() {
        return PirateAIController._m_fields;
    }

    /* renamed from: d */
    public void mo310d() {
        super.mo310d();
        this.dUn = 0.0f;
        this.dUo = 0.0f;
        this.dUp = 0.0f;
        this.dUq = 0.0f;
        this.dUr = 0.0f;
        this.dUs = 0;
        this.dUt = 0;
        this.dUu = 0;
        this.dUv = 0.0f;
        this.dUw = null;
        this.dUx = 0.0f;
        this.dUy = 0;
        this.dUz = null;
        this.radius2 = 0.0f;
        this.dUA = 0;
        this.dUB = 0;
        this.dUC = 0;
        this.dUD = 0;
        this.dUE = 0;
        this.dUF = 0;
        this.dUG = 0;
        this.dUH = 0;
        this.dUI = 0;
        this.dUJ = 0;
        this.dUK = 0;
        this.dUL = 0;
        this.dUM = 0;
        this.aPM = 0;
        this.dUN = 0;
        this.dUO = 0;
        this.dUP = 0;
        this.dUQ = 0;
        this.dUR = 0;
        this.dUS = 0;
        this.dUT = 0;
        this.dUU = 0;
        this.dUV = 0;
        this.dUW = 0;
        this.dUX = 0;
        this.dUY = 0;
        this.dUZ = 0;
        this.aPO = 0;
    }
}
