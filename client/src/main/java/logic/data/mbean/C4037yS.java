package logic.data.mbean;

import game.script.Actor;
import game.script.item.Module;
import game.script.item.buff.module.AttributeBuff;
import game.script.ship.Ship;
import logic.baa.C1616Xf;
import logic.res.code.C5663aRz;
import p001a.C0064Am;

/* renamed from: a.yS */
public class C4037yS extends C3805vD {
    @C0064Am(aul = "5fa5a464bcc117b86788cb009bbe9885", aum = -2)
    public long aun;
    @C0064Am(aul = "5fa5a464bcc117b86788cb009bbe9885", aum = -1)
    public byte auu;
    @C0064Am(aul = "5fa5a464bcc117b86788cb009bbe9885", aum = 1)
    public Ship bMA;
    @C0064Am(aul = "8e53d233efba6f4206f2bd8ca64bf913", aum = 2)
    public Actor bMB;
    @C0064Am(aul = "e0567a1edaa66efa9759890399bbfa1c", aum = 3)
    public Module bMC;
    @C0064Am(aul = "329f90115f3a7f712c3330ff2e6f1141", aum = -2)
    public long bMD;
    @C0064Am(aul = "8e53d233efba6f4206f2bd8ca64bf913", aum = -2)
    public long bME;
    @C0064Am(aul = "e0567a1edaa66efa9759890399bbfa1c", aum = -2)
    public long bMF;
    @C0064Am(aul = "329f90115f3a7f712c3330ff2e6f1141", aum = -1)
    public byte bMG;
    @C0064Am(aul = "8e53d233efba6f4206f2bd8ca64bf913", aum = -1)
    public byte bMH;
    @C0064Am(aul = "e0567a1edaa66efa9759890399bbfa1c", aum = -1)
    public byte bMI;
    @C0064Am(aul = "329f90115f3a7f712c3330ff2e6f1141", aum = 0)
    public Module.C4010b bMz;

    public C4037yS() {
    }

    public C4037yS(C1616Xf xf) {
        super(xf);
    }

    /* renamed from: c */
    public C5663aRz[] mo309c() {
        return AttributeBuff._m_fields;
    }

    /* renamed from: d */
    public void mo310d() {
        super.mo310d();
        this.bMz = null;
        this.bMA = null;
        this.bMB = null;
        this.bMC = null;
        this.bMD = 0;
        this.aun = 0;
        this.bME = 0;
        this.bMF = 0;
        this.bMG = 0;
        this.auu = 0;
        this.bMH = 0;
        this.bMI = 0;
    }
}
