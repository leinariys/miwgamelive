package logic.data.mbean;

import game.network.message.serializable.C3438ra;
import game.script.consignment.ConsignmentFeeManager;
import game.script.consignment.ConsignmentFeeRange;
import logic.baa.C1616Xf;
import logic.res.code.C5663aRz;
import p001a.C0064Am;

import java.util.UUID;

/* renamed from: a.yf */
public class C4056yf extends C3805vD {
    @C0064Am(aul = "3492c59e28974aec08a1d0b2f09013b7", aum = 0)
    public C3438ra<ConsignmentFeeRange> bJC;
    @C0064Am(aul = "3492c59e28974aec08a1d0b2f09013b7", aum = -2)
    public long bJD;
    @C0064Am(aul = "3492c59e28974aec08a1d0b2f09013b7", aum = -1)
    public byte bJE;
    @C0064Am(aul = "b00d699aeb94aad65f0e7baf336ec480", aum = 2)

    /* renamed from: bK */
    public UUID f9592bK;
    @C0064Am(aul = "4609bcf788553ee77446e8ffccf5bc99", aum = 1)
    public String handle;
    @C0064Am(aul = "b00d699aeb94aad65f0e7baf336ec480", aum = -2)

    /* renamed from: oL */
    public long f9593oL;
    @C0064Am(aul = "b00d699aeb94aad65f0e7baf336ec480", aum = -1)

    /* renamed from: oS */
    public byte f9594oS;
    @C0064Am(aul = "4609bcf788553ee77446e8ffccf5bc99", aum = -2)

    /* renamed from: ok */
    public long f9595ok;
    @C0064Am(aul = "4609bcf788553ee77446e8ffccf5bc99", aum = -1)

    /* renamed from: on */
    public byte f9596on;

    public C4056yf() {
    }

    public C4056yf(C1616Xf xf) {
        super(xf);
    }

    /* renamed from: c */
    public C5663aRz[] mo309c() {
        return ConsignmentFeeManager._m_fields;
    }

    /* renamed from: d */
    public void mo310d() {
        super.mo310d();
        this.bJC = null;
        this.handle = null;
        this.f9592bK = null;
        this.bJD = 0;
        this.f9595ok = 0;
        this.f9593oL = 0;
        this.bJE = 0;
        this.f9596on = 0;
        this.f9594oS = 0;
    }
}
