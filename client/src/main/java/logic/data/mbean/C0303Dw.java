package logic.data.mbean;

import game.script.item.buff.amplifier.WeaponAmplifier;
import logic.baa.C1616Xf;
import logic.res.code.C5663aRz;
import p001a.C0064Am;

/* renamed from: a.Dw */
public class C0303Dw extends C3976xd {
    @C0064Am(aul = "9970182609f9f8bdd667af0719216889", aum = 0)

    /* renamed from: DI */
    public WeaponAmplifier f456DI;
    @C0064Am(aul = "9970182609f9f8bdd667af0719216889", aum = -2)

    /* renamed from: dl */
    public long f457dl;
    @C0064Am(aul = "9970182609f9f8bdd667af0719216889", aum = -1)

    /* renamed from: ds */
    public byte f458ds;

    public C0303Dw() {
    }

    public C0303Dw(C1616Xf xf) {
        super(xf);
    }

    /* renamed from: c */
    public C5663aRz[] mo309c() {
        return WeaponAmplifier.WeaponAmplifierAdapter._m_fields;
    }

    /* renamed from: d */
    public void mo310d() {
        super.mo310d();
        this.f456DI = null;
        this.f457dl = 0;
        this.f458ds = 0;
    }
}
