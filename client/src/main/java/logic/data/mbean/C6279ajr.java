package logic.data.mbean;

import game.script.contract.types.TransportBaseInfo;
import game.script.item.BagItemType;
import logic.baa.C1616Xf;
import logic.res.code.C5663aRz;
import p001a.C0064Am;

/* renamed from: a.ajr  reason: case insensitive filesystem */
public class C6279ajr extends C2035bN {
    @C0064Am(aul = "f0cb93307cbe8b9a327a9766a0e14c05", aum = 0)

    /* renamed from: OK */
    public BagItemType f4730OK;
    @C0064Am(aul = "882af63fc5686fa77dc04c017e36d9e9", aum = 1)

    /* renamed from: OM */
    public long f4731OM;
    @C0064Am(aul = "f0cb93307cbe8b9a327a9766a0e14c05", aum = -2)
    public long fQU;
    @C0064Am(aul = "882af63fc5686fa77dc04c017e36d9e9", aum = -2)
    public long fQV;
    @C0064Am(aul = "f0cb93307cbe8b9a327a9766a0e14c05", aum = -1)
    public byte fQW;
    @C0064Am(aul = "882af63fc5686fa77dc04c017e36d9e9", aum = -1)
    public byte fQX;

    public C6279ajr() {
    }

    public C6279ajr(C1616Xf xf) {
        super(xf);
    }

    /* renamed from: c */
    public C5663aRz[] mo309c() {
        return TransportBaseInfo._m_fields;
    }

    /* renamed from: d */
    public void mo310d() {
        super.mo310d();
        this.f4730OK = null;
        this.f4731OM = 0;
        this.fQU = 0;
        this.fQV = 0;
        this.fQW = 0;
        this.fQX = 0;
    }
}
