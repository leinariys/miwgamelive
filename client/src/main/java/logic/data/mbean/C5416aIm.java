package logic.data.mbean;

import game.script.npcchat.AbstractSpeech;
import game.script.npcchat.SpeechRequirement;
import logic.baa.C1616Xf;
import logic.res.code.C5663aRz;
import p001a.C0064Am;

import java.util.UUID;

/* renamed from: a.aIm  reason: case insensitive filesystem */
public class C5416aIm extends C3805vD {
    @C0064Am(aul = "93a57bb2c3fc3cf76132cec8601d25bb", aum = -2)
    public long aTT;
    @C0064Am(aul = "93a57bb2c3fc3cf76132cec8601d25bb", aum = -1)
    public byte aTW;
    @C0064Am(aul = "93a57bb2c3fc3cf76132cec8601d25bb", aum = 0)

    /* renamed from: bI */
    public AbstractSpeech f3097bI;
    @C0064Am(aul = "df466a4578a1b8fbb13d2a05a2f278ad", aum = 1)

    /* renamed from: bK */
    public UUID f3098bK;
    @C0064Am(aul = "e3e16d77b2ac35431479c37a4a680e11", aum = 2)
    public String handle;
    @C0064Am(aul = "df466a4578a1b8fbb13d2a05a2f278ad", aum = -2)

    /* renamed from: oL */
    public long f3099oL;
    @C0064Am(aul = "df466a4578a1b8fbb13d2a05a2f278ad", aum = -1)

    /* renamed from: oS */
    public byte f3100oS;
    @C0064Am(aul = "e3e16d77b2ac35431479c37a4a680e11", aum = -2)

    /* renamed from: ok */
    public long f3101ok;
    @C0064Am(aul = "e3e16d77b2ac35431479c37a4a680e11", aum = -1)

    /* renamed from: on */
    public byte f3102on;

    public C5416aIm() {
    }

    public C5416aIm(C1616Xf xf) {
        super(xf);
    }

    /* renamed from: c */
    public C5663aRz[] mo309c() {
        return SpeechRequirement._m_fields;
    }

    /* renamed from: d */
    public void mo310d() {
        super.mo310d();
        this.f3097bI = null;
        this.f3098bK = null;
        this.handle = null;
        this.aTT = 0;
        this.f3099oL = 0;
        this.f3101ok = 0;
        this.aTW = 0;
        this.f3100oS = 0;
        this.f3102on = 0;
    }
}
