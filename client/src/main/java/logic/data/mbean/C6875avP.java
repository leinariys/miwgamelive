package logic.data.mbean;

import game.script.npcchat.actions.RecurrentMessageRemoveSpeechAction;
import logic.baa.C1616Xf;
import logic.res.code.C5663aRz;
import p001a.C0064Am;

/* renamed from: a.avP  reason: case insensitive filesystem */
public class C6875avP extends C6352alM {
    @C0064Am(aul = "bf9eaa0bc6ceb935a3e47018d65de57f", aum = 0)
    public String bbZ;
    @C0064Am(aul = "bf9eaa0bc6ceb935a3e47018d65de57f", aum = -2)
    public long gKV;
    @C0064Am(aul = "bf9eaa0bc6ceb935a3e47018d65de57f", aum = -1)
    public byte gKW;

    public C6875avP() {
    }

    public C6875avP(C1616Xf xf) {
        super(xf);
    }

    /* renamed from: c */
    public C5663aRz[] mo309c() {
        return RecurrentMessageRemoveSpeechAction._m_fields;
    }

    /* renamed from: d */
    public void mo310d() {
        super.mo310d();
        this.bbZ = null;
        this.gKV = 0;
        this.gKW = 0;
    }
}
