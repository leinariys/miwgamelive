package logic.data.mbean;

import game.script.mines.MineType;
import game.script.resource.Asset;
import game.script.ship.HullType;
import logic.baa.C1616Xf;
import logic.res.code.C5663aRz;
import p001a.C0064Am;

/* renamed from: a.Qu */
public class C1153Qu extends C6374ali {
    @C0064Am(aul = "233911ad115afc77dea3fefe824440f3", aum = 1)

    /* renamed from: VX */
    public float f1477VX;
    @C0064Am(aul = "2d1b97e3d181e3736505d0cb445eea50", aum = 2)

    /* renamed from: VZ */
    public float f1478VZ;
    @C0064Am(aul = "0e416e861eff36de18ffca610ebe9c43", aum = 3)

    /* renamed from: Wb */
    public Asset f1479Wb;
    @C0064Am(aul = "7b659f6462b71be36847e6f0fa51e249", aum = 4)

    /* renamed from: Wd */
    public Asset f1480Wd;
    @C0064Am(aul = "c68aaa071185a3673807c76f01760562", aum = -2)
    public long ail;
    @C0064Am(aul = "c68aaa071185a3673807c76f01760562", aum = -1)
    public byte aiv;
    @C0064Am(aul = "233911ad115afc77dea3fefe824440f3", aum = -1)
    public byte bhB;
    @C0064Am(aul = "0e416e861eff36de18ffca610ebe9c43", aum = -1)
    public byte bhC;
    @C0064Am(aul = "7b659f6462b71be36847e6f0fa51e249", aum = -1)
    public byte bhD;
    @C0064Am(aul = "5e830366332c5befbb4de2675d38f0ca", aum = -1)
    public byte bhF;
    @C0064Am(aul = "5e830366332c5befbb4de2675d38f0ca", aum = 0)
    public HullType bhn;
    @C0064Am(aul = "233911ad115afc77dea3fefe824440f3", aum = -2)
    public long bhs;
    @C0064Am(aul = "0e416e861eff36de18ffca610ebe9c43", aum = -2)
    public long bht;
    @C0064Am(aul = "7b659f6462b71be36847e6f0fa51e249", aum = -2)
    public long bhu;
    @C0064Am(aul = "5e830366332c5befbb4de2675d38f0ca", aum = -2)
    public long bhw;
    @C0064Am(aul = "2d1b97e3d181e3736505d0cb445eea50", aum = -2)
    public long dVZ;
    @C0064Am(aul = "2d1b97e3d181e3736505d0cb445eea50", aum = -1)
    public byte dWa;
    @C0064Am(aul = "c68aaa071185a3673807c76f01760562", aum = 5)
    public int lifeTime;

    public C1153Qu() {
    }

    public C1153Qu(C1616Xf xf) {
        super(xf);
    }

    /* renamed from: c */
    public C5663aRz[] mo309c() {
        return MineType._m_fields;
    }

    /* renamed from: d */
    public void mo310d() {
        super.mo310d();
        this.bhn = null;
        this.f1477VX = 0.0f;
        this.f1478VZ = 0.0f;
        this.f1479Wb = null;
        this.f1480Wd = null;
        this.lifeTime = 0;
        this.bhw = 0;
        this.bhs = 0;
        this.dVZ = 0;
        this.bht = 0;
        this.bhu = 0;
        this.ail = 0;
        this.bhF = 0;
        this.bhB = 0;
        this.dWa = 0;
        this.bhC = 0;
        this.bhD = 0;
        this.aiv = 0;
    }
}
