package logic.data.mbean;

import game.script.citizenship.CitizenshipReward;
import logic.baa.C1616Xf;
import logic.res.code.C5663aRz;
import p001a.C0064Am;

import java.util.UUID;

/* renamed from: a.Vg */
public class C1473Vg extends C3805vD {
    @C0064Am(aul = "4f7485f169a36d6751e641e9bd2e28ed", aum = -2)
    public long aLG;
    @C0064Am(aul = "4f7485f169a36d6751e641e9bd2e28ed", aum = -1)
    public byte aLL;
    @C0064Am(aul = "a302b22f4869ad4ff0aed964012abff7", aum = 2)

    /* renamed from: bK */
    public UUID f1902bK;
    @C0064Am(aul = "4f7485f169a36d6751e641e9bd2e28ed", aum = 0)

    /* renamed from: cy */
    public int f1903cy;
    @C0064Am(aul = "41d549f9be4139a3a0b92ed1f191eba7", aum = 1)
    public String handle;
    @C0064Am(aul = "a302b22f4869ad4ff0aed964012abff7", aum = -2)

    /* renamed from: oL */
    public long f1904oL;
    @C0064Am(aul = "a302b22f4869ad4ff0aed964012abff7", aum = -1)

    /* renamed from: oS */
    public byte f1905oS;
    @C0064Am(aul = "41d549f9be4139a3a0b92ed1f191eba7", aum = -2)

    /* renamed from: ok */
    public long f1906ok;
    @C0064Am(aul = "41d549f9be4139a3a0b92ed1f191eba7", aum = -1)

    /* renamed from: on */
    public byte f1907on;

    public C1473Vg() {
    }

    public C1473Vg(C1616Xf xf) {
        super(xf);
    }

    /* renamed from: c */
    public C5663aRz[] mo309c() {
        return CitizenshipReward._m_fields;
    }

    /* renamed from: d */
    public void mo310d() {
        super.mo310d();
        this.f1903cy = 0;
        this.handle = null;
        this.f1902bK = null;
        this.aLG = 0;
        this.f1906ok = 0;
        this.f1904oL = 0;
        this.aLL = 0;
        this.f1907on = 0;
        this.f1905oS = 0;
    }
}
