package logic.data.mbean;

import game.network.message.serializable.C3438ra;
import game.script.item.RawMaterial;
import game.script.pda.DatabaseCategory;
import game.script.pda.TaikopediaEntry;
import game.script.resource.Asset;
import logic.baa.C1616Xf;
import logic.res.code.C5663aRz;
import p001a.C0064Am;

/* renamed from: a.UT */
public class C1396UT extends C1359Tn {
    @C0064Am(aul = "7d45a02a72980654cfa0eae714d593ce", aum = 0)

    /* renamed from: jS */
    public DatabaseCategory f1753jS;
    @C0064Am(aul = "b298d30341214badb1ea8f3951e39a01", aum = 1)

    /* renamed from: jT */
    public C3438ra<TaikopediaEntry> f1754jT;
    @C0064Am(aul = "87ff61ccace573a3139651a313d63c80", aum = 2)

    /* renamed from: jU */
    public Asset f1755jU;
    @C0064Am(aul = "da3d56c88a5770f209d55d6b6cb7e99e", aum = 3)

    /* renamed from: jV */
    public float f1756jV;
    @C0064Am(aul = "13903a083726a38595b76b5b56d8e120", aum = 4)

    /* renamed from: jW */
    public int f1757jW;
    @C0064Am(aul = "7d45a02a72980654cfa0eae714d593ce", aum = -2)

    /* renamed from: jX */
    public long f1758jX;
    @C0064Am(aul = "b298d30341214badb1ea8f3951e39a01", aum = -2)

    /* renamed from: jY */
    public long f1759jY;
    @C0064Am(aul = "87ff61ccace573a3139651a313d63c80", aum = -2)

    /* renamed from: jZ */
    public long f1760jZ;
    @C0064Am(aul = "da3d56c88a5770f209d55d6b6cb7e99e", aum = -2)

    /* renamed from: ka */
    public long f1761ka;
    @C0064Am(aul = "13903a083726a38595b76b5b56d8e120", aum = -2)

    /* renamed from: kb */
    public long f1762kb;
    @C0064Am(aul = "7d45a02a72980654cfa0eae714d593ce", aum = -1)

    /* renamed from: kc */
    public byte f1763kc;
    @C0064Am(aul = "b298d30341214badb1ea8f3951e39a01", aum = -1)

    /* renamed from: kd */
    public byte f1764kd;
    @C0064Am(aul = "87ff61ccace573a3139651a313d63c80", aum = -1)

    /* renamed from: ke */
    public byte f1765ke;
    @C0064Am(aul = "da3d56c88a5770f209d55d6b6cb7e99e", aum = -1)

    /* renamed from: kf */
    public byte f1766kf;
    @C0064Am(aul = "13903a083726a38595b76b5b56d8e120", aum = -1)

    /* renamed from: kg */
    public byte f1767kg;

    public C1396UT() {
    }

    public C1396UT(C1616Xf xf) {
        super(xf);
    }

    /* renamed from: c */
    public C5663aRz[] mo309c() {
        return RawMaterial._m_fields;
    }

    /* renamed from: d */
    public void mo310d() {
        super.mo310d();
        this.f1753jS = null;
        this.f1754jT = null;
        this.f1755jU = null;
        this.f1756jV = 0.0f;
        this.f1757jW = 0;
        this.f1758jX = 0;
        this.f1759jY = 0;
        this.f1760jZ = 0;
        this.f1761ka = 0;
        this.f1762kb = 0;
        this.f1763kc = 0;
        this.f1764kd = 0;
        this.f1765ke = 0;
        this.f1766kf = 0;
        this.f1767kg = 0;
    }
}
