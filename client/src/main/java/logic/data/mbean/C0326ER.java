package logic.data.mbean;

import game.network.message.serializable.C1556Wo;
import game.script.item.Weapon;
import game.script.item.buff.module.WeaponBuff;
import logic.baa.C1616Xf;
import logic.res.code.C5663aRz;
import p001a.C0064Am;
import p001a.C3892wO;

/* renamed from: a.ER */
public class C0326ER extends C4037yS {
    @C0064Am(aul = "9785c21e4911bbff3115eabec537c410", aum = -2)
    public long aIR;
    @C0064Am(aul = "9785c21e4911bbff3115eabec537c410", aum = -1)
    public byte aJc;
    @C0064Am(aul = "6d0b39325e28ed5270686057929bee71", aum = 0)
    public C3892wO bEb;
    @C0064Am(aul = "9785c21e4911bbff3115eabec537c410", aum = 1)
    public C3892wO bEd;
    @C0064Am(aul = "579045f142993eadb80ddaf77d535afa", aum = 2)
    public C3892wO bEe;
    @C0064Am(aul = "3a5b84d36d20afdcac81ac58a9b0bfaf", aum = 3)
    public C3892wO bEf;
    @C0064Am(aul = "08e94c4f7158ef3c8309ddc65d4465af", aum = 4)
    public C3892wO bEh;
    @C0064Am(aul = "fcce36402cbb57e52da1a8c75d207aba", aum = 5)
    public boolean bEj;
    @C0064Am(aul = "6420b7d0c2d8a7a2ce1d30afddbe81ef", aum = 6)
    public boolean bEl;
    @C0064Am(aul = "353580931ef706a3e64fe9e71fa6dd91", aum = 7)
    public C1556Wo<Weapon, WeaponBuff.WeaponModuleAdapter> bEn;
    @C0064Am(aul = "fcce36402cbb57e52da1a8c75d207aba", aum = -2)
    public long bIG;
    @C0064Am(aul = "6d0b39325e28ed5270686057929bee71", aum = -2)
    public long bIH;
    @C0064Am(aul = "579045f142993eadb80ddaf77d535afa", aum = -2)
    public long bIJ;
    @C0064Am(aul = "fcce36402cbb57e52da1a8c75d207aba", aum = -1)
    public byte bIS;
    @C0064Am(aul = "6d0b39325e28ed5270686057929bee71", aum = -1)
    public byte bIT;
    @C0064Am(aul = "579045f142993eadb80ddaf77d535afa", aum = -1)
    public byte bIV;
    @C0064Am(aul = "6420b7d0c2d8a7a2ce1d30afddbe81ef", aum = -2)
    public long cUh;
    @C0064Am(aul = "353580931ef706a3e64fe9e71fa6dd91", aum = -2)
    public long cUi;
    @C0064Am(aul = "6420b7d0c2d8a7a2ce1d30afddbe81ef", aum = -1)
    public byte cUj;
    @C0064Am(aul = "353580931ef706a3e64fe9e71fa6dd91", aum = -1)
    public byte cUk;
    @C0064Am(aul = "3a5b84d36d20afdcac81ac58a9b0bfaf", aum = -1)

    /* renamed from: tO */
    public byte f486tO;
    @C0064Am(aul = "08e94c4f7158ef3c8309ddc65d4465af", aum = -1)

    /* renamed from: tS */
    public byte f487tS;
    @C0064Am(aul = "3a5b84d36d20afdcac81ac58a9b0bfaf", aum = -2)

    /* renamed from: tl */
    public long f488tl;
    @C0064Am(aul = "08e94c4f7158ef3c8309ddc65d4465af", aum = -2)

    /* renamed from: tp */
    public long f489tp;

    public C0326ER() {
    }

    public C0326ER(C1616Xf xf) {
        super(xf);
    }

    /* renamed from: c */
    public C5663aRz[] mo309c() {
        return WeaponBuff._m_fields;
    }

    /* renamed from: d */
    public void mo310d() {
        super.mo310d();
        this.bEb = null;
        this.bEd = null;
        this.bEe = null;
        this.bEf = null;
        this.bEh = null;
        this.bEj = false;
        this.bEl = false;
        this.bEn = null;
        this.bIH = 0;
        this.aIR = 0;
        this.bIJ = 0;
        this.f488tl = 0;
        this.f489tp = 0;
        this.bIG = 0;
        this.cUh = 0;
        this.cUi = 0;
        this.bIT = 0;
        this.aJc = 0;
        this.bIV = 0;
        this.f486tO = 0;
        this.f487tS = 0;
        this.bIS = 0;
        this.cUj = 0;
        this.cUk = 0;
    }
}
