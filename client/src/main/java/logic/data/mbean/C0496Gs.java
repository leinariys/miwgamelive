package logic.data.mbean;

import game.script.item.Component;
import game.script.resource.Asset;
import game.script.ship.SectorCategory;
import game.script.ship.Slot;
import logic.baa.C1616Xf;
import logic.res.code.C5663aRz;
import p001a.C0064Am;

/* renamed from: a.Gs */
public class C0496Gs extends C0887Ms {
    @C0064Am(aul = "df7f6689513493cd12c4756a811beaed", aum = -2)
    public long bwn;
    @C0064Am(aul = "df7f6689513493cd12c4756a811beaed", aum = -1)
    public byte bwt;
    @C0064Am(aul = "7e2b131fb2f49d8628ea8eee0c93a2c2", aum = 3)
    public boolean cSS;
    @C0064Am(aul = "7e2b131fb2f49d8628ea8eee0c93a2c2", aum = -2)
    public long cST;
    @C0064Am(aul = "7e2b131fb2f49d8628ea8eee0c93a2c2", aum = -1)
    public byte cSV;
    @C0064Am(aul = "df7f6689513493cd12c4756a811beaed", aum = 0)
    public SectorCategory cYq;
    @C0064Am(aul = "cda8c1b1fb76edad05f3282bb5358bb2", aum = 1)
    public Slot.C2893a cYr;
    @C0064Am(aul = "d0c4f39608e73be9c2c8441574401605", aum = 2)
    public Asset cYs;
    @C0064Am(aul = "cda8c1b1fb76edad05f3282bb5358bb2", aum = -2)
    public long cYt;
    @C0064Am(aul = "d0c4f39608e73be9c2c8441574401605", aum = -2)
    public long cYu;
    @C0064Am(aul = "cda8c1b1fb76edad05f3282bb5358bb2", aum = -1)
    public byte cYv;
    @C0064Am(aul = "d0c4f39608e73be9c2c8441574401605", aum = -1)
    public byte cYw;

    public C0496Gs() {
    }

    public C0496Gs(C1616Xf xf) {
        super(xf);
    }

    /* renamed from: c */
    public C5663aRz[] mo309c() {
        return Component._m_fields;
    }

    /* renamed from: d */
    public void mo310d() {
        super.mo310d();
        this.cYq = null;
        this.cYr = null;
        this.cYs = null;
        this.cSS = false;
        this.bwn = 0;
        this.cYt = 0;
        this.cYu = 0;
        this.cST = 0;
        this.bwt = 0;
        this.cYv = 0;
        this.cYw = 0;
        this.cSV = 0;
    }
}
