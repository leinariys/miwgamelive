package logic.data.mbean;

import game.script.resource.Asset;
import game.script.ship.SectorCategory;
import logic.baa.C1616Xf;
import logic.res.code.C5663aRz;
import p001a.C0064Am;
import p001a.C3904wY;
import taikodom.infra.script.I18NString;

import java.util.UUID;

/* renamed from: a.aJH */
public class aJH extends C3805vD {
    @C0064Am(aul = "bc7aab9aae9ef176f2d53ee9ca63df8b", aum = 2)

    /* renamed from: NV */
    public short f3176NV;
    @C0064Am(aul = "bc9969ba96df70dea505366862ec21ca", aum = 3)

    /* renamed from: NX */
    public C3904wY f3177NX;
    @C0064Am(aul = "bb0306c596366d8bb38de2806e604268", aum = -2)

    /* renamed from: Nf */
    public long f3178Nf;
    @C0064Am(aul = "bb0306c596366d8bb38de2806e604268", aum = -1)

    /* renamed from: Nh */
    public byte f3179Nh;
    @C0064Am(aul = "8fb81d109f82c6d72552ce095f73aa3c", aum = 5)

    /* renamed from: bK */
    public UUID f3180bK;
    @C0064Am(aul = "bc7aab9aae9ef176f2d53ee9ca63df8b", aum = -2)
    public long das;
    @C0064Am(aul = "bc7aab9aae9ef176f2d53ee9ca63df8b", aum = -1)
    public byte day;
    @C0064Am(aul = "bc9969ba96df70dea505366862ec21ca", aum = -2)

    /* renamed from: df */
    public long f3181df;
    @C0064Am(aul = "bc9969ba96df70dea505366862ec21ca", aum = -1)

    /* renamed from: dm */
    public byte f3182dm;
    @C0064Am(aul = "5c9b14baf8aa1abf6bb169823016f72d", aum = 0)
    public String handle;
    @C0064Am(aul = "7bdadfd27204534d01e9607002d1ed03", aum = -1)

    /* renamed from: jK */
    public byte f3183jK;
    @C0064Am(aul = "7bdadfd27204534d01e9607002d1ed03", aum = 4)

    /* renamed from: jn */
    public Asset f3184jn;
    @C0064Am(aul = "7bdadfd27204534d01e9607002d1ed03", aum = -2)

    /* renamed from: jy */
    public long f3185jy;
    @C0064Am(aul = "8fb81d109f82c6d72552ce095f73aa3c", aum = -2)

    /* renamed from: oL */
    public long f3186oL;
    @C0064Am(aul = "8fb81d109f82c6d72552ce095f73aa3c", aum = -1)

    /* renamed from: oS */
    public byte f3187oS;
    @C0064Am(aul = "5c9b14baf8aa1abf6bb169823016f72d", aum = -2)

    /* renamed from: ok */
    public long f3188ok;
    @C0064Am(aul = "5c9b14baf8aa1abf6bb169823016f72d", aum = -1)

    /* renamed from: on */
    public byte f3189on;
    @C0064Am(aul = "bb0306c596366d8bb38de2806e604268", aum = 1)

    /* renamed from: zP */
    public I18NString f3190zP;

    public aJH() {
    }

    public aJH(C1616Xf xf) {
        super(xf);
    }

    /* renamed from: c */
    public C5663aRz[] mo309c() {
        return SectorCategory._m_fields;
    }

    /* renamed from: d */
    public void mo310d() {
        super.mo310d();
        this.handle = null;
        this.f3190zP = null;
        this.f3176NV = (short) 0;
        this.f3177NX = null;
        this.f3184jn = null;
        this.f3180bK = null;
        this.f3188ok = 0;
        this.f3178Nf = 0;
        this.das = 0;
        this.f3181df = 0;
        this.f3185jy = 0;
        this.f3186oL = 0;
        this.f3189on = 0;
        this.f3179Nh = 0;
        this.day = 0;
        this.f3182dm = 0;
        this.f3183jK = 0;
        this.f3187oS = 0;
    }
}
