package logic.data.mbean;

import game.script.item.buff.amplifier.HazardShieldAmplifier;
import logic.baa.C1616Xf;
import logic.res.code.C5663aRz;
import p001a.C0064Am;

/* renamed from: a.gJ */
public class C2521gJ extends C2090bu {
    @C0064Am(aul = "5e77422b7db79074aa44084e208bd67a", aum = 0)

    /* renamed from: QZ */
    public HazardShieldAmplifier f7557QZ;
    @C0064Am(aul = "5e77422b7db79074aa44084e208bd67a", aum = -2)

    /* renamed from: dl */
    public long f7558dl;
    @C0064Am(aul = "5e77422b7db79074aa44084e208bd67a", aum = -1)

    /* renamed from: ds */
    public byte f7559ds;

    public C2521gJ() {
    }

    public C2521gJ(C1616Xf xf) {
        super(xf);
    }

    /* renamed from: c */
    public C5663aRz[] mo309c() {
        return HazardShieldAmplifier.HazardShieldAmplififerAdapter._m_fields;
    }

    /* renamed from: d */
    public void mo310d() {
        super.mo310d();
        this.f7557QZ = null;
        this.f7558dl = 0;
        this.f7559ds = 0;
    }
}
