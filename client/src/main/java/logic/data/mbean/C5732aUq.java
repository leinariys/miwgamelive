package logic.data.mbean;

import game.script.mission.actions.RecurrentMessageRequestMissionAction;
import game.script.resource.Asset;
import logic.baa.C1616Xf;
import logic.res.code.C5663aRz;
import p001a.C0064Am;
import taikodom.infra.script.I18NString;

/* renamed from: a.aUq  reason: case insensitive filesystem */
public class C5732aUq extends C2503fv {
    @C0064Am(aul = "6362664fefc881edba8b2109a0c1f6a5", aum = 0)
    public String bbZ;
    @C0064Am(aul = "8901df5093e5ec73af4830dc2d632d1c", aum = 1)
    public I18NString bcb;
    @C0064Am(aul = "dfc214a6c5910883d65ac76014ffecb3", aum = 2)
    public Asset bcd;
    @C0064Am(aul = "6362664fefc881edba8b2109a0c1f6a5", aum = -2)
    public long gKV;
    @C0064Am(aul = "6362664fefc881edba8b2109a0c1f6a5", aum = -1)
    public byte gKW;
    @C0064Am(aul = "8901df5093e5ec73af4830dc2d632d1c", aum = -2)
    public long gLt;
    @C0064Am(aul = "dfc214a6c5910883d65ac76014ffecb3", aum = -2)
    public long gLu;
    @C0064Am(aul = "8901df5093e5ec73af4830dc2d632d1c", aum = -1)
    public byte gLv;
    @C0064Am(aul = "dfc214a6c5910883d65ac76014ffecb3", aum = -1)
    public byte gLw;

    public C5732aUq() {
    }

    public C5732aUq(C1616Xf xf) {
        super(xf);
    }

    /* renamed from: c */
    public C5663aRz[] mo309c() {
        return RecurrentMessageRequestMissionAction._m_fields;
    }

    /* renamed from: d */
    public void mo310d() {
        super.mo310d();
        this.bbZ = null;
        this.bcb = null;
        this.bcd = null;
        this.gKV = 0;
        this.gLt = 0;
        this.gLu = 0;
        this.gKW = 0;
        this.gLv = 0;
        this.gLw = 0;
    }
}
