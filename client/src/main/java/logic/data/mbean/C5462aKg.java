package logic.data.mbean;

import game.script.resource.Asset;
import game.script.space.Advertise;
import logic.baa.C1616Xf;
import logic.res.code.C5663aRz;
import p001a.C0064Am;

import java.util.UUID;

/* renamed from: a.aKg  reason: case insensitive filesystem */
public class C5462aKg extends C2217cs {
    @C0064Am(aul = "aaaa2dde525f44e37096e7fda5ca38d1", aum = 0)

    /* renamed from: LQ */
    public Asset f3255LQ;
    @C0064Am(aul = "062cc085d5518d3491fd5f005120b3c5", aum = -2)
    public long avd;
    @C0064Am(aul = "062cc085d5518d3491fd5f005120b3c5", aum = -1)
    public byte avi;
    @C0064Am(aul = "aaaa2dde525f44e37096e7fda5ca38d1", aum = -2)
    public long ayc;
    @C0064Am(aul = "aaaa2dde525f44e37096e7fda5ca38d1", aum = -1)
    public byte ayr;
    @C0064Am(aul = "bda968c59da3c100d271331928a41e31", aum = 3)

    /* renamed from: bK */
    public UUID f3256bK;
    @C0064Am(aul = "014f7e94b8363e9f4715843e94b40c27", aum = 2)
    public String handle;
    @C0064Am(aul = "bda968c59da3c100d271331928a41e31", aum = -2)

    /* renamed from: oL */
    public long f3257oL;
    @C0064Am(aul = "bda968c59da3c100d271331928a41e31", aum = -1)

    /* renamed from: oS */
    public byte f3258oS;
    @C0064Am(aul = "014f7e94b8363e9f4715843e94b40c27", aum = -2)

    /* renamed from: ok */
    public long f3259ok;
    @C0064Am(aul = "014f7e94b8363e9f4715843e94b40c27", aum = -1)

    /* renamed from: on */
    public byte f3260on;
    @C0064Am(aul = "062cc085d5518d3491fd5f005120b3c5", aum = 1)

    /* renamed from: uT */
    public String f3261uT;

    public C5462aKg() {
    }

    public C5462aKg(C1616Xf xf) {
        super(xf);
    }

    /* renamed from: c */
    public C5663aRz[] mo309c() {
        return Advertise._m_fields;
    }

    /* renamed from: d */
    public void mo310d() {
        super.mo310d();
        this.f3255LQ = null;
        this.f3261uT = null;
        this.handle = null;
        this.f3256bK = null;
        this.ayc = 0;
        this.avd = 0;
        this.f3259ok = 0;
        this.f3257oL = 0;
        this.ayr = 0;
        this.avi = 0;
        this.f3260on = 0;
        this.f3258oS = 0;
    }
}
