package logic.data.mbean;

import game.script.mission.scripting.ProtectMissionScript;
import logic.baa.C1616Xf;
import logic.res.code.C5663aRz;
import p001a.C0064Am;

/* renamed from: a.aKw  reason: case insensitive filesystem */
public class C5478aKw extends C2955mD {
    @C0064Am(aul = "bd5aedd1a44b331e18bcf71d6c884706", aum = 0)
    public float dbK;
    @C0064Am(aul = "fcf366ce566faaf17b5c1ab0a8b252ae", aum = 1)
    public ProtectMissionScript dbM;
    @C0064Am(aul = "fcf366ce566faaf17b5c1ab0a8b252ae", aum = -2)

    /* renamed from: dl */
    public long f3266dl;
    @C0064Am(aul = "fcf366ce566faaf17b5c1ab0a8b252ae", aum = -1)

    /* renamed from: ds */
    public byte f3267ds;
    @C0064Am(aul = "bd5aedd1a44b331e18bcf71d6c884706", aum = -2)
    public long iik;
    @C0064Am(aul = "bd5aedd1a44b331e18bcf71d6c884706", aum = -1)
    public byte iil;

    public C5478aKw() {
    }

    public C5478aKw(C1616Xf xf) {
        super(xf);
    }

    /* renamed from: c */
    public C5663aRz[] mo309c() {
        return ProtectMissionScript.StateA._m_fields;
    }

    /* renamed from: d */
    public void mo310d() {
        super.mo310d();
        this.dbK = 0.0f;
        this.dbM = null;
        this.iik = 0;
        this.f3266dl = 0;
        this.iil = 0;
        this.f3267ds = 0;
    }
}
