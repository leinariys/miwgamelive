package logic.data.mbean;

import game.network.message.serializable.C1556Wo;
import game.network.message.serializable.C3438ra;
import game.script.mission.MissionObjective;
import game.script.mission.scripting.ScriptableMission;
import game.script.npc.NPC;
import game.script.ship.Ship;
import logic.baa.C1616Xf;
import logic.res.code.C5663aRz;
import p001a.C0064Am;

/* renamed from: a.arr  reason: case insensitive filesystem */
public class C6695arr extends C0505HA {
    @C0064Am(aul = "90d7d01b2da9060c6ff48fdddb3d90dd", aum = 4)
    public C1556Wo<Ship, String> bdV;
    @C0064Am(aul = "90d7d01b2da9060c6ff48fdddb3d90dd", aum = -2)
    public long bea;
    @C0064Am(aul = "90d7d01b2da9060c6ff48fdddb3d90dd", aum = -1)
    public byte beg;
    @C0064Am(aul = "44c3374bdc95f73b89160343885a9024", aum = -2)
    public long cnh;
    @C0064Am(aul = "44c3374bdc95f73b89160343885a9024", aum = -1)
    public byte cni;
    @C0064Am(aul = "24b2bb569dd5fd90fcb23e8ad522a7c6", aum = 2)
    public C3438ra<MissionObjective> cno;
    @C0064Am(aul = "c824d0bf3174abe446bbcc1f63f95249", aum = 0)
    public C1556Wo<String, Object> dEE;
    @C0064Am(aul = "862c9a53f226eca350a7b7343df0dae1", aum = 1)
    public C3438ra<GatePass> dEG;
    @C0064Am(aul = "44c3374bdc95f73b89160343885a9024", aum = 3)
    public C1556Wo<String, NPC> dEI;
    @C0064Am(aul = "5391084ae672014f8e0d139e5a878e61", aum = 5)
    public float dEL;
    @C0064Am(aul = "24b2bb569dd5fd90fcb23e8ad522a7c6", aum = -2)
    public long dhk;
    @C0064Am(aul = "24b2bb569dd5fd90fcb23e8ad522a7c6", aum = -1)
    public byte dho;
    @C0064Am(aul = "c824d0bf3174abe446bbcc1f63f95249", aum = -2)
    public long grP;
    @C0064Am(aul = "862c9a53f226eca350a7b7343df0dae1", aum = -2)
    public long grQ;
    @C0064Am(aul = "5391084ae672014f8e0d139e5a878e61", aum = -2)
    public long grR;
    @C0064Am(aul = "c824d0bf3174abe446bbcc1f63f95249", aum = -1)
    public byte grS;
    @C0064Am(aul = "862c9a53f226eca350a7b7343df0dae1", aum = -1)
    public byte grT;
    @C0064Am(aul = "5391084ae672014f8e0d139e5a878e61", aum = -1)
    public byte grU;

    public C6695arr() {
    }

    public C6695arr(C1616Xf xf) {
        super(xf);
    }

    /* renamed from: c */
    public C5663aRz[] mo309c() {
        return ScriptableMission._m_fields;
    }

    /* renamed from: d */
    public void mo310d() {
        super.mo310d();
        this.dEE = null;
        this.dEG = null;
        this.cno = null;
        this.dEI = null;
        this.bdV = null;
        this.dEL = 0.0f;
        this.grP = 0;
        this.grQ = 0;
        this.dhk = 0;
        this.cnh = 0;
        this.bea = 0;
        this.grR = 0;
        this.grS = 0;
        this.grT = 0;
        this.dho = 0;
        this.cni = 0;
        this.beg = 0;
        this.grU = 0;
    }
}
