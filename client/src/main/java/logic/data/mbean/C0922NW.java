package logic.data.mbean;

import game.script.mission.scripting.ScriptableMissionTemplate;
import game.script.mission.scripting.ScriptableMissionTemplateObjective;
import logic.baa.C1616Xf;
import logic.res.code.C5663aRz;
import p001a.C0064Am;

import java.util.UUID;

/* renamed from: a.NW */
public class C0922NW extends C5292aDs {
    @C0064Am(aul = "f6de47af42ffa7ce5ee8341ecc3358a0", aum = 3)

    /* renamed from: bK */
    public UUID f1235bK;
    @C0064Am(aul = "c16fadb4690e6387ac721bf6cee661bd", aum = 1)

    /* renamed from: cZ */
    public int f1236cZ;
    @C0064Am(aul = "789e211f2fdf42f289cebbf9b0a3d2be", aum = 0)
    public String dIA;
    @C0064Am(aul = "679adc9269140abb5dcd12c3398061da", aum = 2)
    public ScriptableMissionTemplate dIB;
    @C0064Am(aul = "789e211f2fdf42f289cebbf9b0a3d2be", aum = -2)
    public long dIC;
    @C0064Am(aul = "679adc9269140abb5dcd12c3398061da", aum = -2)
    public long dID;
    @C0064Am(aul = "789e211f2fdf42f289cebbf9b0a3d2be", aum = -1)
    public byte dIE;
    @C0064Am(aul = "679adc9269140abb5dcd12c3398061da", aum = -1)
    public byte dIF;
    @C0064Am(aul = "c16fadb4690e6387ac721bf6cee661bd", aum = -2)

    /* renamed from: dh */
    public long f1237dh;
    @C0064Am(aul = "c16fadb4690e6387ac721bf6cee661bd", aum = -1)

    /* renamed from: do */
    public byte f1238do;
    @C0064Am(aul = "f6de47af42ffa7ce5ee8341ecc3358a0", aum = -2)

    /* renamed from: oL */
    public long f1239oL;
    @C0064Am(aul = "f6de47af42ffa7ce5ee8341ecc3358a0", aum = -1)

    /* renamed from: oS */
    public byte f1240oS;

    public C0922NW() {
    }

    public C0922NW(C1616Xf xf) {
        super(xf);
    }

    /* renamed from: c */
    public C5663aRz[] mo309c() {
        return ScriptableMissionTemplateObjective._m_fields;
    }

    /* renamed from: d */
    public void mo310d() {
        super.mo310d();
        this.dIA = null;
        this.f1236cZ = 0;
        this.dIB = null;
        this.f1235bK = null;
        this.dIC = 0;
        this.f1237dh = 0;
        this.dID = 0;
        this.f1239oL = 0;
        this.dIE = 0;
        this.f1238do = 0;
        this.dIF = 0;
        this.f1240oS = 0;
    }
}
