package logic.data.mbean;

import game.network.message.serializable.C3438ra;
import game.script.item.ItemType;
import game.script.mission.TableNPCSpawn;
import game.script.missiontemplate.WaypointDat;
import game.script.missiontemplate.scripting.AsteroidLootItemMissionScriptTemplate;
import game.script.ship.Station;
import game.script.space.AsteroidType;
import game.script.space.StellarSystem;
import logic.baa.C1616Xf;
import logic.res.code.C5663aRz;
import p001a.C0064Am;

/* renamed from: a.IZ */
public class C0605IZ extends C0597IR {
    @C0064Am(aul = "90dfb6cd0bfbd1d93179387915700630", aum = 1)
    public C3438ra<WaypointDat> aBp;
    @C0064Am(aul = "8e7588b5c2058ba3d4740eb3bf3f1d9d", aum = 0)
    public TableNPCSpawn dhI;
    @C0064Am(aul = "ae1d244e42d4baec820326e2ba1b7575", aum = 3)
    public Station dhJ;
    @C0064Am(aul = "650cca073d8b86c054d18437fb3fcbc5", aum = 4)
    public ItemType dhK;
    @C0064Am(aul = "c1bd94f840b72c80a961359f0ba89fc5", aum = 5)
    public AsteroidType dhL;
    @C0064Am(aul = "8e7588b5c2058ba3d4740eb3bf3f1d9d", aum = -2)
    public long dhM;
    @C0064Am(aul = "90dfb6cd0bfbd1d93179387915700630", aum = -2)
    public long dhN;
    @C0064Am(aul = "ae1d244e42d4baec820326e2ba1b7575", aum = -2)
    public long dhO;
    @C0064Am(aul = "650cca073d8b86c054d18437fb3fcbc5", aum = -2)
    public long dhP;
    @C0064Am(aul = "c1bd94f840b72c80a961359f0ba89fc5", aum = -2)
    public long dhQ;
    @C0064Am(aul = "8e7588b5c2058ba3d4740eb3bf3f1d9d", aum = -1)
    public byte dhR;
    @C0064Am(aul = "90dfb6cd0bfbd1d93179387915700630", aum = -1)
    public byte dhS;
    @C0064Am(aul = "ae1d244e42d4baec820326e2ba1b7575", aum = -1)
    public byte dhT;
    @C0064Am(aul = "650cca073d8b86c054d18437fb3fcbc5", aum = -1)
    public byte dhU;
    @C0064Am(aul = "c1bd94f840b72c80a961359f0ba89fc5", aum = -1)
    public byte dhV;
    @C0064Am(aul = "829dbf397aef3d48cc90084a95f9d000", aum = 2)

    /* renamed from: rI */
    public StellarSystem f724rI;
    @C0064Am(aul = "829dbf397aef3d48cc90084a95f9d000", aum = -2)

    /* renamed from: rY */
    public long f725rY;
    @C0064Am(aul = "829dbf397aef3d48cc90084a95f9d000", aum = -1)

    /* renamed from: so */
    public byte f726so;

    public C0605IZ() {
    }

    public C0605IZ(C1616Xf xf) {
        super(xf);
    }

    /* renamed from: c */
    public C5663aRz[] mo309c() {
        return AsteroidLootItemMissionScriptTemplate._m_fields;
    }

    /* renamed from: d */
    public void mo310d() {
        super.mo310d();
        this.dhI = null;
        this.aBp = null;
        this.f724rI = null;
        this.dhJ = null;
        this.dhK = null;
        this.dhL = null;
        this.dhM = 0;
        this.dhN = 0;
        this.f725rY = 0;
        this.dhO = 0;
        this.dhP = 0;
        this.dhQ = 0;
        this.dhR = 0;
        this.dhS = 0;
        this.f726so = 0;
        this.dhT = 0;
        this.dhU = 0;
        this.dhV = 0;
    }
}
