package logic.data.mbean;

import game.network.message.serializable.C3438ra;
import game.script.ai.npc.AIControllerType;
import game.script.faction.Faction;
import game.script.itemgen.ItemGenTable;
import game.script.npc.NPC;
import game.script.npcchat.NPCChat;
import game.script.pda.DatabaseCategory;
import game.script.pda.TaikopediaEntry;
import game.script.player.Player;
import game.script.progression.ProgressionCharacterTemplate;
import game.script.resource.Asset;
import game.script.ship.EquippedShipType;
import logic.baa.C1616Xf;
import logic.res.code.C5663aRz;
import p001a.C0064Am;
import p001a.C6902avq;
import taikodom.infra.script.I18NString;

/* renamed from: a.lB */
public class C2850lB extends C0205CW {
    @C0064Am(aul = "e428cc8702ebd8e6d70450c250116c10", aum = 17)

    /* renamed from: BO */
    public int f8474BO;
    @C0064Am(aul = "df05a683a346bae0ad289e835e8460e8", aum = -2)

    /* renamed from: Nf */
    public long f8475Nf;
    @C0064Am(aul = "df05a683a346bae0ad289e835e8460e8", aum = -1)

    /* renamed from: Nh */
    public byte f8476Nh;
    @C0064Am(aul = "3da629e55109bb5c63567550e9232f53", aum = 11)
    public Faction axU;
    @C0064Am(aul = "310396f909cbd02b944576ca74a22bca", aum = 0)
    public EquippedShipType ayQ;
    @C0064Am(aul = "424c55857ecbe8b36cf7ed09aa744cf7", aum = 1)
    public Class ayR;
    @C0064Am(aul = "2ab698219ad5f269f64d783b293291be", aum = 2)
    public AIControllerType ayS;
    @C0064Am(aul = "a865a02b57cb43673d58f5963721d938", aum = 4)
    public String ayT;
    @C0064Am(aul = "0db96c1621b7a5259439c3d8cea460ec", aum = 5)
    public NPCChat ayU;
    @C0064Am(aul = "35058ff732f22e4940eca484a77dbd6d", aum = 6)
    public C6902avq ayV;
    @C0064Am(aul = "f1997c098addc2d976f7e88c5c3f5ece", aum = 7)
    public ProgressionCharacterTemplate ayW;
    @C0064Am(aul = "d434a7fff7fd296a5694f6560482914e", aum = 8)
    public I18NString ayX;
    @C0064Am(aul = "8a3f03f2fa567f4b287542adf426ee92", aum = 12)
    public boolean ayY;
    @C0064Am(aul = "cd1e1a58d136ff3eff9251544c188890", aum = 13)
    public C3438ra<ItemGenTable> ayZ;
    @C0064Am(aul = "3da629e55109bb5c63567550e9232f53", aum = -2)
    public long ayi;
    @C0064Am(aul = "3da629e55109bb5c63567550e9232f53", aum = -1)
    public byte ayx;
    @C0064Am(aul = "d434a7fff7fd296a5694f6560482914e", aum = -1)
    public byte azA;
    @C0064Am(aul = "8a3f03f2fa567f4b287542adf426ee92", aum = -1)
    public byte azB;
    @C0064Am(aul = "cd1e1a58d136ff3eff9251544c188890", aum = -1)
    public byte azC;
    @C0064Am(aul = "9e69d993e8b91bfe483b4cd4aa4efe45", aum = -1)
    public byte azD;
    @C0064Am(aul = "e428cc8702ebd8e6d70450c250116c10", aum = -1)
    public byte azE;
    @C0064Am(aul = "99d8b6ab18a81579b578de3a49271040", aum = -1)
    public byte azF;
    @C0064Am(aul = "3ec5eb395e148962c1c9cc648ca53bd6", aum = -1)
    public byte azG;
    @C0064Am(aul = "7420934d7dd25e1265f5ae40ab7166ec", aum = -1)
    public byte azH;
    @C0064Am(aul = "9e69d993e8b91bfe483b4cd4aa4efe45", aum = 14)
    public long aza;
    @C0064Am(aul = "99d8b6ab18a81579b578de3a49271040", aum = 19)
    public NPCParty azb;
    @C0064Am(aul = "3ec5eb395e148962c1c9cc648ca53bd6", aum = 20)
    public Player azc;
    @C0064Am(aul = "7420934d7dd25e1265f5ae40ab7166ec", aum = 21)
    public String azd;
    @C0064Am(aul = "310396f909cbd02b944576ca74a22bca", aum = -2)
    public long aze;
    @C0064Am(aul = "424c55857ecbe8b36cf7ed09aa744cf7", aum = -2)
    public long azf;
    @C0064Am(aul = "2ab698219ad5f269f64d783b293291be", aum = -2)
    public long azg;
    @C0064Am(aul = "a865a02b57cb43673d58f5963721d938", aum = -2)
    public long azh;
    @C0064Am(aul = "0db96c1621b7a5259439c3d8cea460ec", aum = -2)
    public long azi;
    @C0064Am(aul = "35058ff732f22e4940eca484a77dbd6d", aum = -2)
    public long azj;
    @C0064Am(aul = "f1997c098addc2d976f7e88c5c3f5ece", aum = -2)
    public long azk;
    @C0064Am(aul = "d434a7fff7fd296a5694f6560482914e", aum = -2)
    public long azl;
    @C0064Am(aul = "8a3f03f2fa567f4b287542adf426ee92", aum = -2)
    public long azm;
    @C0064Am(aul = "cd1e1a58d136ff3eff9251544c188890", aum = -2)
    public long azn;
    @C0064Am(aul = "9e69d993e8b91bfe483b4cd4aa4efe45", aum = -2)
    public long azo;
    @C0064Am(aul = "e428cc8702ebd8e6d70450c250116c10", aum = -2)
    public long azp;
    @C0064Am(aul = "99d8b6ab18a81579b578de3a49271040", aum = -2)
    public long azq;
    @C0064Am(aul = "3ec5eb395e148962c1c9cc648ca53bd6", aum = -2)
    public long azr;
    @C0064Am(aul = "7420934d7dd25e1265f5ae40ab7166ec", aum = -2)
    public long azs;
    @C0064Am(aul = "310396f909cbd02b944576ca74a22bca", aum = -1)
    public byte azt;
    @C0064Am(aul = "424c55857ecbe8b36cf7ed09aa744cf7", aum = -1)
    public byte azu;
    @C0064Am(aul = "2ab698219ad5f269f64d783b293291be", aum = -1)
    public byte azv;
    @C0064Am(aul = "a865a02b57cb43673d58f5963721d938", aum = -1)
    public byte azw;
    @C0064Am(aul = "0db96c1621b7a5259439c3d8cea460ec", aum = -1)
    public byte azx;
    @C0064Am(aul = "35058ff732f22e4940eca484a77dbd6d", aum = -1)
    public byte azy;
    @C0064Am(aul = "f1997c098addc2d976f7e88c5c3f5ece", aum = -1)
    public byte azz;
    @C0064Am(aul = "3e4bff6f3242267d225c2edf306d6080", aum = 15)

    /* renamed from: jS */
    public DatabaseCategory f8477jS;
    @C0064Am(aul = "8ade9a37ad8c37c0b9f60781f1a53455", aum = 10)

    /* renamed from: jT */
    public C3438ra<TaikopediaEntry> f8478jT;
    @C0064Am(aul = "0d615363c7f022ccd11c43e8b1a4199b", aum = 16)

    /* renamed from: jU */
    public Asset f8479jU;
    @C0064Am(aul = "7fd19ed7e9d5c445b1ddb4bdef7c0ccc", aum = 18)

    /* renamed from: jV */
    public float f8480jV;
    @C0064Am(aul = "3e4bff6f3242267d225c2edf306d6080", aum = -2)

    /* renamed from: jX */
    public long f8481jX;
    @C0064Am(aul = "8ade9a37ad8c37c0b9f60781f1a53455", aum = -2)

    /* renamed from: jY */
    public long f8482jY;
    @C0064Am(aul = "0d615363c7f022ccd11c43e8b1a4199b", aum = -2)

    /* renamed from: jZ */
    public long f8483jZ;
    @C0064Am(aul = "7fd19ed7e9d5c445b1ddb4bdef7c0ccc", aum = -2)

    /* renamed from: ka */
    public long f8484ka;
    @C0064Am(aul = "3e4bff6f3242267d225c2edf306d6080", aum = -1)

    /* renamed from: kc */
    public byte f8485kc;
    @C0064Am(aul = "8ade9a37ad8c37c0b9f60781f1a53455", aum = -1)

    /* renamed from: kd */
    public byte f8486kd;
    @C0064Am(aul = "0d615363c7f022ccd11c43e8b1a4199b", aum = -1)

    /* renamed from: ke */
    public byte f8487ke;
    @C0064Am(aul = "7fd19ed7e9d5c445b1ddb4bdef7c0ccc", aum = -1)

    /* renamed from: kf */
    public byte f8488kf;
    @C0064Am(aul = "3573e0ef9f158c42f8dfc25de591e375", aum = 9)

    /* renamed from: nh */
    public I18NString f8489nh;
    @C0064Am(aul = "3573e0ef9f158c42f8dfc25de591e375", aum = -2)

    /* renamed from: nk */
    public long f8490nk;
    @C0064Am(aul = "3573e0ef9f158c42f8dfc25de591e375", aum = -1)

    /* renamed from: nn */
    public byte f8491nn;
    @C0064Am(aul = "df05a683a346bae0ad289e835e8460e8", aum = 3)

    /* renamed from: zP */
    public I18NString f8492zP;

    public C2850lB() {
    }

    public C2850lB(C1616Xf xf) {
        super(xf);
    }

    /* renamed from: c */
    public C5663aRz[] mo309c() {
        return NPC._m_fields;
    }

    /* renamed from: d */
    public void mo310d() {
        super.mo310d();
        this.ayQ = null;
        this.ayR = null;
        this.ayS = null;
        this.f8492zP = null;
        this.ayT = null;
        this.ayU = null;
        this.ayV = null;
        this.ayW = null;
        this.ayX = null;
        this.f8489nh = null;
        this.f8478jT = null;
        this.axU = null;
        this.ayY = false;
        this.ayZ = null;
        this.aza = 0;
        this.f8477jS = null;
        this.f8479jU = null;
        this.f8474BO = 0;
        this.f8480jV = 0.0f;
        this.azb = null;
        this.azc = null;
        this.azd = null;
        this.aze = 0;
        this.azf = 0;
        this.azg = 0;
        this.f8475Nf = 0;
        this.azh = 0;
        this.azi = 0;
        this.azj = 0;
        this.azk = 0;
        this.azl = 0;
        this.f8490nk = 0;
        this.f8482jY = 0;
        this.ayi = 0;
        this.azm = 0;
        this.azn = 0;
        this.azo = 0;
        this.f8481jX = 0;
        this.f8483jZ = 0;
        this.azp = 0;
        this.f8484ka = 0;
        this.azq = 0;
        this.azr = 0;
        this.azs = 0;
        this.azt = 0;
        this.azu = 0;
        this.azv = 0;
        this.f8476Nh = 0;
        this.azw = 0;
        this.azx = 0;
        this.azy = 0;
        this.azz = 0;
        this.azA = 0;
        this.f8491nn = 0;
        this.f8486kd = 0;
        this.ayx = 0;
        this.azB = 0;
        this.azC = 0;
        this.azD = 0;
        this.f8485kc = 0;
        this.f8487ke = 0;
        this.azE = 0;
        this.f8488kf = 0;
        this.azF = 0;
        this.azG = 0;
        this.azH = 0;
    }
}
