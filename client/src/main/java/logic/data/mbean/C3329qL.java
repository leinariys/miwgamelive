package logic.data.mbean;

import game.network.message.serializable.C3438ra;
import game.script.crafting.CraftingRecipe;
import game.script.item.IngredientsCategory;
import logic.baa.C1616Xf;
import logic.res.code.C5663aRz;
import p001a.C0064Am;
import taikodom.infra.script.I18NString;

import java.util.UUID;

/* renamed from: a.qL */
public class C3329qL extends C5292aDs {
    @C0064Am(aul = "f6f4e4ef479983556c10dfb761743463", aum = -2)

    /* renamed from: Nf */
    public long f8897Nf;
    @C0064Am(aul = "f6f4e4ef479983556c10dfb761743463", aum = -1)

    /* renamed from: Nh */
    public byte f8898Nh;
    @C0064Am(aul = "edaced45b2672c95ce1cb4c33bd2021f", aum = 1)
    public C3438ra<CraftingRecipe> aWq;
    @C0064Am(aul = "edaced45b2672c95ce1cb4c33bd2021f", aum = -2)
    public long aWr;
    @C0064Am(aul = "edaced45b2672c95ce1cb4c33bd2021f", aum = -1)
    public byte aWs;
    @C0064Am(aul = "e89841dfa66f95697d7c5b6c42ccf331", aum = 2)

    /* renamed from: bK */
    public UUID f8899bK;
    @C0064Am(aul = "3cbd0fc601375cf774e039c634275bb4", aum = 3)
    public String handle;
    @C0064Am(aul = "e89841dfa66f95697d7c5b6c42ccf331", aum = -2)

    /* renamed from: oL */
    public long f8900oL;
    @C0064Am(aul = "e89841dfa66f95697d7c5b6c42ccf331", aum = -1)

    /* renamed from: oS */
    public byte f8901oS;
    @C0064Am(aul = "3cbd0fc601375cf774e039c634275bb4", aum = -2)

    /* renamed from: ok */
    public long f8902ok;
    @C0064Am(aul = "3cbd0fc601375cf774e039c634275bb4", aum = -1)

    /* renamed from: on */
    public byte f8903on;
    @C0064Am(aul = "f6f4e4ef479983556c10dfb761743463", aum = 0)

    /* renamed from: zP */
    public I18NString f8904zP;

    public C3329qL() {
    }

    public C3329qL(C1616Xf xf) {
        super(xf);
    }

    /* renamed from: c */
    public C5663aRz[] mo309c() {
        return IngredientsCategory._m_fields;
    }

    /* renamed from: d */
    public void mo310d() {
        super.mo310d();
        this.f8904zP = null;
        this.aWq = null;
        this.f8899bK = null;
        this.handle = null;
        this.f8897Nf = 0;
        this.aWr = 0;
        this.f8900oL = 0;
        this.f8902ok = 0;
        this.f8898Nh = 0;
        this.aWs = 0;
        this.f8901oS = 0;
        this.f8903on = 0;
    }
}
