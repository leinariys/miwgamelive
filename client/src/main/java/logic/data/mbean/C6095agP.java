package logic.data.mbean;

import game.network.message.serializable.C1556Wo;
import game.network.message.serializable.C3438ra;
import game.script.citizenship.CitizenImprovement;
import game.script.citizenship.Citizenship;
import game.script.citizenship.CitizenshipControl;
import game.script.citizenship.CitizenshipType;
import game.script.player.Player;
import logic.baa.C1616Xf;
import logic.res.code.C5663aRz;
import p001a.C0064Am;

/* renamed from: a.agP  reason: case insensitive filesystem */
public class C6095agP extends C3805vD {
    @C0064Am(aul = "759e4630c02b1a4be03fb78a7ec42a7e", aum = 0)

    /* renamed from: P */
    public Player f4516P;
    @C0064Am(aul = "759e4630c02b1a4be03fb78a7ec42a7e", aum = -2)

    /* renamed from: ag */
    public long f4517ag;
    @C0064Am(aul = "759e4630c02b1a4be03fb78a7ec42a7e", aum = -1)

    /* renamed from: av */
    public byte f4518av;
    @C0064Am(aul = "1513f7f3facc77eec3d20e0eac2a4d39", aum = 1)
    public C3438ra<Citizenship> dWR;
    @C0064Am(aul = "bf9aa1129e3bdb9cd0e2f60bd22ed444", aum = 2)
    public C1556Wo<CitizenImprovement.C2184a, CitizenImprovement> dWT;
    @C0064Am(aul = "b7c24e4bc14abbd55437c7f09d50bf84", aum = 3)
    public C1556Wo<CitizenshipType, Integer> dWV;
    @C0064Am(aul = "1513f7f3facc77eec3d20e0eac2a4d39", aum = -2)
    public long fEW;
    @C0064Am(aul = "bf9aa1129e3bdb9cd0e2f60bd22ed444", aum = -2)
    public long fEX;
    @C0064Am(aul = "b7c24e4bc14abbd55437c7f09d50bf84", aum = -2)
    public long fEY;
    @C0064Am(aul = "1513f7f3facc77eec3d20e0eac2a4d39", aum = -1)
    public byte fEZ;
    @C0064Am(aul = "bf9aa1129e3bdb9cd0e2f60bd22ed444", aum = -1)
    public byte fFa;
    @C0064Am(aul = "b7c24e4bc14abbd55437c7f09d50bf84", aum = -1)
    public byte fFb;

    public C6095agP() {
    }

    public C6095agP(C1616Xf xf) {
        super(xf);
    }

    /* renamed from: c */
    public C5663aRz[] mo309c() {
        return CitizenshipControl._m_fields;
    }

    /* renamed from: d */
    public void mo310d() {
        super.mo310d();
        this.f4516P = null;
        this.dWR = null;
        this.dWT = null;
        this.dWV = null;
        this.f4517ag = 0;
        this.fEW = 0;
        this.fEX = 0;
        this.fEY = 0;
        this.f4518av = 0;
        this.fEZ = 0;
        this.fFa = 0;
        this.fFb = 0;
    }
}
