package logic.data.mbean;

import game.network.message.serializable.C3438ra;
import game.script.missiontemplate.scripting.ItemDeployMissionScriptTemplate;
import game.script.ship.Station;
import logic.baa.C1616Xf;
import logic.res.code.C5663aRz;
import p001a.C0064Am;

/* renamed from: a.am */
public class C1938am extends C0597IR {
    @C0064Am(aul = "1d0c4aa2e19c70bf0948b93ef2c1b992", aum = 0)

    /* renamed from: iH */
    public Station f4889iH;
    @C0064Am(aul = "863300f7a492251de8e992315b4b0e03", aum = 1)

    /* renamed from: iI */
    public C3438ra<ItemTypeTableItem> f4890iI;
    @C0064Am(aul = "5df698cd3333dbaa13e417d26392de63", aum = 2)

    /* renamed from: iJ */
    public C3438ra<ItemTypeTableItem> f4891iJ;
    @C0064Am(aul = "1d0c4aa2e19c70bf0948b93ef2c1b992", aum = -2)

    /* renamed from: iK */
    public long f4892iK;
    @C0064Am(aul = "863300f7a492251de8e992315b4b0e03", aum = -2)

    /* renamed from: iL */
    public long f4893iL;
    @C0064Am(aul = "5df698cd3333dbaa13e417d26392de63", aum = -2)

    /* renamed from: iM */
    public long f4894iM;
    @C0064Am(aul = "1d0c4aa2e19c70bf0948b93ef2c1b992", aum = -1)

    /* renamed from: iN */
    public byte f4895iN;
    @C0064Am(aul = "863300f7a492251de8e992315b4b0e03", aum = -1)

    /* renamed from: iO */
    public byte f4896iO;
    @C0064Am(aul = "5df698cd3333dbaa13e417d26392de63", aum = -1)

    /* renamed from: iP */
    public byte f4897iP;

    public C1938am() {
    }

    public C1938am(C1616Xf xf) {
        super(xf);
    }

    /* renamed from: c */
    public C5663aRz[] mo309c() {
        return ItemDeployMissionScriptTemplate._m_fields;
    }

    /* renamed from: d */
    public void mo310d() {
        super.mo310d();
        this.f4889iH = null;
        this.f4890iI = null;
        this.f4891iJ = null;
        this.f4892iK = 0;
        this.f4893iL = 0;
        this.f4894iM = 0;
        this.f4895iN = 0;
        this.f4896iO = 0;
        this.f4897iP = 0;
    }
}
