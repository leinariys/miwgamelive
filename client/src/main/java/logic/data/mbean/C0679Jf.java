package logic.data.mbean;

import game.network.message.serializable.C1556Wo;
import game.script.progression.ProgressionDefaults;
import logic.baa.C1616Xf;
import logic.res.code.C5663aRz;
import p001a.C0064Am;

import java.util.UUID;

/* renamed from: a.Jf */
public class C0679Jf extends C3805vD {
    @C0064Am(aul = "a7bc1d98e4532ca932b48b01e071a9c2", aum = 6)

    /* renamed from: bK */
    public UUID f807bK;
    @C0064Am(aul = "ed49d3c65bca5e154247df4b637a3c55", aum = 0)
    public int diA;
    @C0064Am(aul = "1ee26b77d268c38add823d57ba1e55e4", aum = 1)
    public int diB;
    @C0064Am(aul = "4c871cff6a96b62de89ee50e6191697a", aum = 2)
    public C1556Wo<Integer, Integer> diC;
    @C0064Am(aul = "3b8247560bf3ebe79f62d5dc47081a43", aum = 3)
    public C1556Wo<Integer, Integer> diD;
    @C0064Am(aul = "f44183b5cd399891e32033fc438feb1a", aum = 4)
    public int diE;
    @C0064Am(aul = "211bad5e5be773ff8c60c19b1f0bba1c", aum = 5)
    public int diF;
    @C0064Am(aul = "ed49d3c65bca5e154247df4b637a3c55", aum = -2)
    public long diG;
    @C0064Am(aul = "1ee26b77d268c38add823d57ba1e55e4", aum = -2)
    public long diH;
    @C0064Am(aul = "4c871cff6a96b62de89ee50e6191697a", aum = -2)
    public long diI;
    @C0064Am(aul = "3b8247560bf3ebe79f62d5dc47081a43", aum = -2)
    public long diJ;
    @C0064Am(aul = "f44183b5cd399891e32033fc438feb1a", aum = -2)
    public long diK;
    @C0064Am(aul = "211bad5e5be773ff8c60c19b1f0bba1c", aum = -2)
    public long diL;
    @C0064Am(aul = "ed49d3c65bca5e154247df4b637a3c55", aum = -1)
    public byte diM;
    @C0064Am(aul = "1ee26b77d268c38add823d57ba1e55e4", aum = -1)
    public byte diN;
    @C0064Am(aul = "4c871cff6a96b62de89ee50e6191697a", aum = -1)
    public byte diO;
    @C0064Am(aul = "3b8247560bf3ebe79f62d5dc47081a43", aum = -1)
    public byte diP;
    @C0064Am(aul = "f44183b5cd399891e32033fc438feb1a", aum = -1)
    public byte diQ;
    @C0064Am(aul = "211bad5e5be773ff8c60c19b1f0bba1c", aum = -1)
    public byte diR;
    @C0064Am(aul = "a7bc1d98e4532ca932b48b01e071a9c2", aum = -2)

    /* renamed from: oL */
    public long f808oL;
    @C0064Am(aul = "a7bc1d98e4532ca932b48b01e071a9c2", aum = -1)

    /* renamed from: oS */
    public byte f809oS;

    public C0679Jf() {
    }

    public C0679Jf(C1616Xf xf) {
        super(xf);
    }

    /* renamed from: c */
    public C5663aRz[] mo309c() {
        return ProgressionDefaults._m_fields;
    }

    /* renamed from: d */
    public void mo310d() {
        super.mo310d();
        this.diA = 0;
        this.diB = 0;
        this.diC = null;
        this.diD = null;
        this.diE = 0;
        this.diF = 0;
        this.f807bK = null;
        this.diG = 0;
        this.diH = 0;
        this.diI = 0;
        this.diJ = 0;
        this.diK = 0;
        this.diL = 0;
        this.f808oL = 0;
        this.diM = 0;
        this.diN = 0;
        this.diO = 0;
        this.diP = 0;
        this.diQ = 0;
        this.diR = 0;
        this.f809oS = 0;
    }
}
