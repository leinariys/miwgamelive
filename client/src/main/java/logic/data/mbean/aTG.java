package logic.data.mbean;

import game.network.message.serializable.C3438ra;
import game.script.Party;
import game.script.player.Player;
import logic.baa.C1616Xf;
import logic.res.code.C5663aRz;
import p001a.C0064Am;

/* renamed from: a.aTG */
public class aTG extends C3805vD {
    @C0064Am(aul = "6457ff9e721a93beb26b4b62ddf4463b", aum = -1)
    public byte ayA;
    @C0064Am(aul = "6457ff9e721a93beb26b4b62ddf4463b", aum = -2)
    public long ayl;
    @C0064Am(aul = "6457ff9e721a93beb26b4b62ddf4463b", aum = 0)
    public C3438ra<Player> cYy;
    @C0064Am(aul = "6b144f1b9b6e17db4208ee188e73f76e", aum = 1)
    public Player cYz;
    @C0064Am(aul = "6b144f1b9b6e17db4208ee188e73f76e", aum = -2)
    public long dui;
    @C0064Am(aul = "6b144f1b9b6e17db4208ee188e73f76e", aum = -1)
    public byte duk;

    public aTG() {
    }

    public aTG(C1616Xf xf) {
        super(xf);
    }

    /* renamed from: c */
    public C5663aRz[] mo309c() {
        return Party._m_fields;
    }

    /* renamed from: d */
    public void mo310d() {
        super.mo310d();
        this.cYy = null;
        this.cYz = null;
        this.ayl = 0;
        this.dui = 0;
        this.ayA = 0;
        this.duk = 0;
    }
}
