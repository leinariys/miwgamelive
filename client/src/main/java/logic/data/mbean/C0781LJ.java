package logic.data.mbean;

import game.network.message.serializable.C3438ra;
import game.script.agent.Agent;
import game.script.item.ItemType;
import game.script.item.WeaponType;
import game.script.mission.MissionTemplate;
import game.script.mission.actions.RecurrentMessageRequestMissionAction;
import game.script.nursery.NurseryDefaults;
import game.script.progression.ProgressionCareer;
import game.script.ship.ShipType;
import game.script.ship.Station;
import logic.baa.C1616Xf;
import logic.res.code.C5663aRz;
import p001a.C0064Am;
import taikodom.infra.script.I18NString;

import java.util.UUID;

/* renamed from: a.LJ */
public class C0781LJ extends C5292aDs {
    @C0064Am(aul = "448c89d50fca8b9e8c801f0c59dbb667", aum = 21)

    /* renamed from: bK */
    public UUID f1019bK;
    @C0064Am(aul = "fc0592326b81e5fb6a3c5d4e01bc78c6", aum = -2)
    public long dwA;
    @C0064Am(aul = "c3e2ad5406f827a11909ed92147940c0", aum = -2)
    public long dwB;
    @C0064Am(aul = "6a58a2768dc6e1dc62fd5f58f33d3ef9", aum = -2)
    public long dwC;
    @C0064Am(aul = "880748544fa3e535bc90b372259685a4", aum = -2)
    public long dwD;
    @C0064Am(aul = "5457b86abf4938ee757c934f3168250b", aum = -2)
    public long dwE;
    @C0064Am(aul = "8c0c6da97eb02e9fa46d01b6fe6fe544", aum = -2)
    public long dwF;
    @C0064Am(aul = "5ff597398b13418039e8f43507b6097f", aum = -2)
    public long dwG;
    @C0064Am(aul = "06145e07ed3c49daf404210cc791e24e", aum = -2)
    public long dwH;
    @C0064Am(aul = "8a55edc1efc79735c794154914b4d0f0", aum = -2)
    public long dwI;
    @C0064Am(aul = "411f0650ecc37f71461a3bf0ed1067f0", aum = -2)
    public long dwJ;
    @C0064Am(aul = "14a58d1400d226ff05f357d234d2e2ce", aum = -2)
    public long dwK;
    @C0064Am(aul = "a44b2ceb63ea125e270d73f0b7c78d98", aum = -2)
    public long dwL;
    @C0064Am(aul = "72ac4b19a02338b9177fad95a088bd4d", aum = -2)
    public long dwM;
    @C0064Am(aul = "af5f5fdc9cea3b0b2f8f80875f1c8556", aum = -2)
    public long dwN;
    @C0064Am(aul = "556d75cdf9cd67a14449f0cfb1e427d7", aum = -2)
    public long dwO;
    @C0064Am(aul = "40f2a4315b2d4a38188f9f294b74558f", aum = -2)
    public long dwP;
    @C0064Am(aul = "bacddc2f1a9d3eab387df50299f08c0a", aum = -2)
    public long dwQ;
    @C0064Am(aul = "21035d11294a83f805c040d1d9cfeae5", aum = -2)
    public long dwR;
    @C0064Am(aul = "ec6f3fab91e7ff2ab8e77bc216c9f003", aum = -2)
    public long dwS;
    @C0064Am(aul = "d981deae72294e332f678f81154a6064", aum = -2)
    public long dwT;
    @C0064Am(aul = "b4e6ae40d60710ab36b3e96f560f6f0c", aum = -2)
    public long dwU;
    @C0064Am(aul = "fc0592326b81e5fb6a3c5d4e01bc78c6", aum = -1)
    public byte dwV;
    @C0064Am(aul = "c3e2ad5406f827a11909ed92147940c0", aum = -1)
    public byte dwW;
    @C0064Am(aul = "6a58a2768dc6e1dc62fd5f58f33d3ef9", aum = -1)
    public byte dwX;
    @C0064Am(aul = "880748544fa3e535bc90b372259685a4", aum = -1)
    public byte dwY;
    @C0064Am(aul = "5457b86abf4938ee757c934f3168250b", aum = -1)
    public byte dwZ;
    @C0064Am(aul = "fc0592326b81e5fb6a3c5d4e01bc78c6", aum = 0)
    public C3438ra<ProgressionCareer> dwf;
    @C0064Am(aul = "c3e2ad5406f827a11909ed92147940c0", aum = 1)
    public C3438ra<ProgressionCareer> dwg;
    @C0064Am(aul = "6a58a2768dc6e1dc62fd5f58f33d3ef9", aum = 2)
    public ShipType dwh;
    @C0064Am(aul = "880748544fa3e535bc90b372259685a4", aum = 3)
    public WeaponType dwi;
    @C0064Am(aul = "5457b86abf4938ee757c934f3168250b", aum = 4)
    public MissionTemplate dwj;
    @C0064Am(aul = "8c0c6da97eb02e9fa46d01b6fe6fe544", aum = 5)
    public MissionTemplate dwk;
    @C0064Am(aul = "5ff597398b13418039e8f43507b6097f", aum = 6)
    public ItemType dwl;
    @C0064Am(aul = "06145e07ed3c49daf404210cc791e24e", aum = 7)
    public MissionTemplate dwm;
    @C0064Am(aul = "8a55edc1efc79735c794154914b4d0f0", aum = 8)
    public MissionTemplate dwn;
    @C0064Am(aul = "411f0650ecc37f71461a3bf0ed1067f0", aum = 9)
    public RecurrentMessageRequestMissionAction dwo;
    @C0064Am(aul = "14a58d1400d226ff05f357d234d2e2ce", aum = 10)
    public I18NString dwp;
    @C0064Am(aul = "a44b2ceb63ea125e270d73f0b7c78d98", aum = 11)
    public I18NString dwq;
    @C0064Am(aul = "72ac4b19a02338b9177fad95a088bd4d", aum = 12)
    public I18NString dwr;
    @C0064Am(aul = "af5f5fdc9cea3b0b2f8f80875f1c8556", aum = 13)
    public I18NString dws;
    @C0064Am(aul = "556d75cdf9cd67a14449f0cfb1e427d7", aum = 14)
    public RecurrentMessageRequestMissionAction dwt;
    @C0064Am(aul = "40f2a4315b2d4a38188f9f294b74558f", aum = 15)
    public RecurrentMessageRequestMissionAction dwu;
    @C0064Am(aul = "bacddc2f1a9d3eab387df50299f08c0a", aum = 16)
    public Station dwv;
    @C0064Am(aul = "21035d11294a83f805c040d1d9cfeae5", aum = 17)
    public Station dww;
    @C0064Am(aul = "ec6f3fab91e7ff2ab8e77bc216c9f003", aum = 18)
    public Station dwx;
    @C0064Am(aul = "d981deae72294e332f678f81154a6064", aum = 19)
    public Station dwy;
    @C0064Am(aul = "b4e6ae40d60710ab36b3e96f560f6f0c", aum = 20)
    public Agent dwz;
    @C0064Am(aul = "8c0c6da97eb02e9fa46d01b6fe6fe544", aum = -1)
    public byte dxa;
    @C0064Am(aul = "5ff597398b13418039e8f43507b6097f", aum = -1)
    public byte dxb;
    @C0064Am(aul = "06145e07ed3c49daf404210cc791e24e", aum = -1)
    public byte dxc;
    @C0064Am(aul = "8a55edc1efc79735c794154914b4d0f0", aum = -1)
    public byte dxd;
    @C0064Am(aul = "411f0650ecc37f71461a3bf0ed1067f0", aum = -1)
    public byte dxe;
    @C0064Am(aul = "14a58d1400d226ff05f357d234d2e2ce", aum = -1)
    public byte dxf;
    @C0064Am(aul = "a44b2ceb63ea125e270d73f0b7c78d98", aum = -1)
    public byte dxg;
    @C0064Am(aul = "72ac4b19a02338b9177fad95a088bd4d", aum = -1)
    public byte dxh;
    @C0064Am(aul = "af5f5fdc9cea3b0b2f8f80875f1c8556", aum = -1)
    public byte dxi;
    @C0064Am(aul = "556d75cdf9cd67a14449f0cfb1e427d7", aum = -1)
    public byte dxj;
    @C0064Am(aul = "40f2a4315b2d4a38188f9f294b74558f", aum = -1)
    public byte dxk;
    @C0064Am(aul = "bacddc2f1a9d3eab387df50299f08c0a", aum = -1)
    public byte dxl;
    @C0064Am(aul = "21035d11294a83f805c040d1d9cfeae5", aum = -1)
    public byte dxm;
    @C0064Am(aul = "ec6f3fab91e7ff2ab8e77bc216c9f003", aum = -1)
    public byte dxn;
    @C0064Am(aul = "d981deae72294e332f678f81154a6064", aum = -1)
    public byte dxo;
    @C0064Am(aul = "b4e6ae40d60710ab36b3e96f560f6f0c", aum = -1)
    public byte dxp;
    @C0064Am(aul = "448c89d50fca8b9e8c801f0c59dbb667", aum = -2)

    /* renamed from: oL */
    public long f1020oL;
    @C0064Am(aul = "448c89d50fca8b9e8c801f0c59dbb667", aum = -1)

    /* renamed from: oS */
    public byte f1021oS;

    public C0781LJ() {
    }

    public C0781LJ(C1616Xf xf) {
        super(xf);
    }

    /* renamed from: c */
    public C5663aRz[] mo309c() {
        return NurseryDefaults._m_fields;
    }

    /* renamed from: d */
    public void mo310d() {
        super.mo310d();
        this.dwf = null;
        this.dwg = null;
        this.dwh = null;
        this.dwi = null;
        this.dwj = null;
        this.dwk = null;
        this.dwl = null;
        this.dwm = null;
        this.dwn = null;
        this.dwo = null;
        this.dwp = null;
        this.dwq = null;
        this.dwr = null;
        this.dws = null;
        this.dwt = null;
        this.dwu = null;
        this.dwv = null;
        this.dww = null;
        this.dwx = null;
        this.dwy = null;
        this.dwz = null;
        this.f1019bK = null;
        this.dwA = 0;
        this.dwB = 0;
        this.dwC = 0;
        this.dwD = 0;
        this.dwE = 0;
        this.dwF = 0;
        this.dwG = 0;
        this.dwH = 0;
        this.dwI = 0;
        this.dwJ = 0;
        this.dwK = 0;
        this.dwL = 0;
        this.dwM = 0;
        this.dwN = 0;
        this.dwO = 0;
        this.dwP = 0;
        this.dwQ = 0;
        this.dwR = 0;
        this.dwS = 0;
        this.dwT = 0;
        this.dwU = 0;
        this.f1020oL = 0;
        this.dwV = 0;
        this.dwW = 0;
        this.dwX = 0;
        this.dwY = 0;
        this.dwZ = 0;
        this.dxa = 0;
        this.dxb = 0;
        this.dxc = 0;
        this.dxd = 0;
        this.dxe = 0;
        this.dxf = 0;
        this.dxg = 0;
        this.dxh = 0;
        this.dxi = 0;
        this.dxj = 0;
        this.dxk = 0;
        this.dxl = 0;
        this.dxm = 0;
        this.dxn = 0;
        this.dxo = 0;
        this.dxp = 0;
        this.f1021oS = 0;
    }
}
