package logic.data.mbean;

import game.network.message.serializable.C1556Wo;
import game.script.newmarket.BuyOrderInfo;
import game.script.newmarket.ListContainer;
import game.script.newmarket.SellOrderInfo;
import game.script.player.DataSetContainer;
import game.script.player.Player;
import game.script.player.PlayerAssistant;
import game.script.ship.Station;
import logic.baa.C1616Xf;
import logic.baa.C4033yO;
import logic.baa.C4068yr;
import logic.baa.aDJ;
import logic.res.code.C5663aRz;
import p001a.C0064Am;

/* renamed from: a.ur */
public class C3773ur extends C3805vD {
    @C0064Am(aul = "f314b1b3499f5192b9bbf259f9c55438", aum = 0)

    /* renamed from: P */
    public Player f9370P;
    @C0064Am(aul = "f314b1b3499f5192b9bbf259f9c55438", aum = -2)

    /* renamed from: ag */
    public long f9371ag;
    @C0064Am(aul = "f314b1b3499f5192b9bbf259f9c55438", aum = -1)

    /* renamed from: av */
    public byte f9372av;
    @C0064Am(aul = "01256b69480846d5c33b681858b37511", aum = 1)
    public C1556Wo<Station, ListContainer<BuyOrderInfo>> bvA;
    @C0064Am(aul = "99db07ec2284575e307d6777a6829837", aum = 2)
    public C1556Wo<Station, ListContainer<SellOrderInfo>> bvB;
    @C0064Am(aul = "3316fa910e6e7cc2b06ee696b6e4b0e2", aum = 3)
    public long bvC;
    @C0064Am(aul = "2d60fc7c4a7f6b40fc30c302281badef", aum = 4)
    public long bvD;
    @C0064Am(aul = "3f1172dafcc9e68ff48441ceeea3cd25", aum = 5)
    public C1556Wo<C4033yO, Boolean> bvE;
    @C0064Am(aul = "6ffea845e9f52beabfa83020e15f49d3", aum = 6)
    public C1556Wo<C4033yO, Boolean> bvF;
    @C0064Am(aul = "4d8623eab9c871fbcc04de55fb9cccb3", aum = 7)
    public C1556Wo<C4068yr, DataSetContainer<aDJ>> bvG;
    @C0064Am(aul = "01256b69480846d5c33b681858b37511", aum = -2)
    public long bvH;
    @C0064Am(aul = "99db07ec2284575e307d6777a6829837", aum = -2)
    public long bvI;
    @C0064Am(aul = "3316fa910e6e7cc2b06ee696b6e4b0e2", aum = -2)
    public long bvJ;
    @C0064Am(aul = "2d60fc7c4a7f6b40fc30c302281badef", aum = -2)
    public long bvK;
    @C0064Am(aul = "3f1172dafcc9e68ff48441ceeea3cd25", aum = -2)
    public long bvL;
    @C0064Am(aul = "6ffea845e9f52beabfa83020e15f49d3", aum = -2)
    public long bvM;
    @C0064Am(aul = "4d8623eab9c871fbcc04de55fb9cccb3", aum = -2)
    public long bvN;
    @C0064Am(aul = "01256b69480846d5c33b681858b37511", aum = -1)
    public byte bvO;
    @C0064Am(aul = "99db07ec2284575e307d6777a6829837", aum = -1)
    public byte bvP;
    @C0064Am(aul = "3316fa910e6e7cc2b06ee696b6e4b0e2", aum = -1)
    public byte bvQ;
    @C0064Am(aul = "2d60fc7c4a7f6b40fc30c302281badef", aum = -1)
    public byte bvR;
    @C0064Am(aul = "3f1172dafcc9e68ff48441ceeea3cd25", aum = -1)
    public byte bvS;
    @C0064Am(aul = "6ffea845e9f52beabfa83020e15f49d3", aum = -1)
    public byte bvT;
    @C0064Am(aul = "4d8623eab9c871fbcc04de55fb9cccb3", aum = -1)
    public byte bvU;

    public C3773ur() {
    }

    public C3773ur(C1616Xf xf) {
        super(xf);
    }

    /* renamed from: c */
    public C5663aRz[] mo309c() {
        return PlayerAssistant._m_fields;
    }

    /* renamed from: d */
    public void mo310d() {
        super.mo310d();
        this.f9370P = null;
        this.bvA = null;
        this.bvB = null;
        this.bvC = 0;
        this.bvD = 0;
        this.bvE = null;
        this.bvF = null;
        this.bvG = null;
        this.f9371ag = 0;
        this.bvH = 0;
        this.bvI = 0;
        this.bvJ = 0;
        this.bvK = 0;
        this.bvL = 0;
        this.bvM = 0;
        this.bvN = 0;
        this.f9372av = 0;
        this.bvO = 0;
        this.bvP = 0;
        this.bvQ = 0;
        this.bvR = 0;
        this.bvS = 0;
        this.bvT = 0;
        this.bvU = 0;
    }
}
