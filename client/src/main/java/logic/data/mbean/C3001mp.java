package logic.data.mbean;

import game.network.message.serializable.C3438ra;
import game.script.npc.NPC;
import game.script.spacezone.population.TrainingZonePopulationBehaviour;
import logic.baa.C1616Xf;
import logic.res.code.C5663aRz;
import p001a.C0064Am;

/* renamed from: a.mp */
public class C3001mp extends C3805vD {
    @C0064Am(aul = "45b1eda594836b596d9b3e401b7dc67e", aum = 0)
    public C3438ra<NPC> aCe;
    @C0064Am(aul = "45b1eda594836b596d9b3e401b7dc67e", aum = -2)
    public long aCf;
    @C0064Am(aul = "45b1eda594836b596d9b3e401b7dc67e", aum = -1)
    public byte aCg;

    public C3001mp() {
    }

    public C3001mp(C1616Xf xf) {
        super(xf);
    }

    /* renamed from: c */
    public C5663aRz[] mo309c() {
        return TrainingZonePopulationBehaviour.NPCList._m_fields;
    }

    /* renamed from: d */
    public void mo310d() {
        super.mo310d();
        this.aCe = null;
        this.aCf = 0;
        this.aCg = 0;
    }
}
