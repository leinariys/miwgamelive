package logic.data.mbean;

import game.network.message.serializable.C3438ra;
import game.script.mission.MissionTemplateObjective;
import game.script.mission.actions.MissionAction;
import game.script.mission.scripting.MissionScriptTemplate;
import logic.baa.C1616Xf;
import logic.res.code.C5663aRz;
import p001a.C0064Am;

/* renamed from: a.IR */
public class C0597IR extends C1756Zw {
    @C0064Am(aul = "331af5cefa51bdad623b42f5df96c243", aum = 0)
    public C3438ra<MissionTemplateObjective> cno;
    @C0064Am(aul = "aa060333739c6fee7efcd3611cc9461b", aum = 1)
    public C3438ra<MissionAction> dhh;
    @C0064Am(aul = "60c0e594ffe426eb57d326a959a6ed48", aum = 2)
    public int dhi;
    @C0064Am(aul = "28131b9a2b0cc4601e69dc4fb4bd3a40", aum = 3)
    public long dhj;
    @C0064Am(aul = "331af5cefa51bdad623b42f5df96c243", aum = -2)
    public long dhk;
    @C0064Am(aul = "aa060333739c6fee7efcd3611cc9461b", aum = -2)
    public long dhl;
    @C0064Am(aul = "60c0e594ffe426eb57d326a959a6ed48", aum = -2)
    public long dhm;
    @C0064Am(aul = "28131b9a2b0cc4601e69dc4fb4bd3a40", aum = -2)
    public long dhn;
    @C0064Am(aul = "331af5cefa51bdad623b42f5df96c243", aum = -1)
    public byte dho;
    @C0064Am(aul = "aa060333739c6fee7efcd3611cc9461b", aum = -1)
    public byte dhp;
    @C0064Am(aul = "60c0e594ffe426eb57d326a959a6ed48", aum = -1)
    public byte dhq;
    @C0064Am(aul = "28131b9a2b0cc4601e69dc4fb4bd3a40", aum = -1)
    public byte dhr;

    public C0597IR() {
    }

    public C0597IR(C1616Xf xf) {
        super(xf);
    }

    /* renamed from: c */
    public C5663aRz[] mo309c() {
        return MissionScriptTemplate._m_fields;
    }

    /* renamed from: d */
    public void mo310d() {
        super.mo310d();
        this.cno = null;
        this.dhh = null;
        this.dhi = 0;
        this.dhj = 0;
        this.dhk = 0;
        this.dhl = 0;
        this.dhm = 0;
        this.dhn = 0;
        this.dho = 0;
        this.dhp = 0;
        this.dhq = 0;
        this.dhr = 0;
    }
}
