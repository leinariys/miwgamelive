package logic.data.mbean;

import game.script.ship.hazardshield.HazardShield;
import logic.baa.C1616Xf;
import logic.res.code.C5663aRz;
import p001a.C0064Am;

/* renamed from: a.ta */
public class C3673ta extends C6757atB {
    @C0064Am(aul = "aa86ddd3f5d6fec0666735a2c656c56a", aum = -2)

    /* renamed from: dl */
    public long f9260dl;
    @C0064Am(aul = "aa86ddd3f5d6fec0666735a2c656c56a", aum = -1)

    /* renamed from: ds */
    public byte f9261ds;
    @C0064Am(aul = "aa86ddd3f5d6fec0666735a2c656c56a", aum = 0)

    /* renamed from: pZ */
    public HazardShield f9262pZ;

    public C3673ta() {
    }

    public C3673ta(C1616Xf xf) {
        super(xf);
    }

    /* renamed from: c */
    public C5663aRz[] mo309c() {
        return HazardShield.InvulnerabilityExiter._m_fields;
    }

    /* renamed from: d */
    public void mo310d() {
        super.mo310d();
        this.f9262pZ = null;
        this.f9260dl = 0;
        this.f9261ds = 0;
    }
}
