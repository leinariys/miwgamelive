package logic.data.mbean;

import game.script.item.buff.module.HealerType;
import logic.baa.C1616Xf;
import logic.res.code.C5663aRz;
import p001a.C0064Am;

/* renamed from: a.aCt  reason: case insensitive filesystem */
public class C5267aCt extends C5661aRx {
    @C0064Am(aul = "139888fef893ea5164afa2a072578a89", aum = 0)
    public float apV;
    @C0064Am(aul = "0982050d95207190a1d5c04992fe1775", aum = 1)
    public float apW;
    @C0064Am(aul = "139888fef893ea5164afa2a072578a89", aum = -2)
    public long aqa;
    @C0064Am(aul = "0982050d95207190a1d5c04992fe1775", aum = -2)
    public long aqb;
    @C0064Am(aul = "139888fef893ea5164afa2a072578a89", aum = -1)
    public byte aqf;
    @C0064Am(aul = "0982050d95207190a1d5c04992fe1775", aum = -1)
    public byte aqg;

    public C5267aCt() {
    }

    public C5267aCt(C1616Xf xf) {
        super(xf);
    }

    /* renamed from: c */
    public C5663aRz[] mo309c() {
        return HealerType._m_fields;
    }

    /* renamed from: d */
    public void mo310d() {
        super.mo310d();
        this.apV = 0.0f;
        this.apW = 0.0f;
        this.aqa = 0;
        this.aqb = 0;
        this.aqf = 0;
        this.aqg = 0;
    }
}
