package logic.data.mbean;

import game.script.spacezone.NPCZone;
import game.script.spacezone.PopulationBehaviour;
import logic.baa.C1616Xf;
import logic.res.code.C5663aRz;
import p001a.C0064Am;

import java.util.UUID;

/* renamed from: a.Wf */
public class C1542Wf extends C3805vD {
    @C0064Am(aul = "e559b560c22c174b165f1998ff542400", aum = -1)

    /* renamed from: XB */
    public byte f2043XB;
    @C0064Am(aul = "e559b560c22c174b165f1998ff542400", aum = 0)

    /* renamed from: Xx */
    public NPCZone f2044Xx;
    @C0064Am(aul = "e559b560c22c174b165f1998ff542400", aum = -2)

    /* renamed from: Xz */
    public long f2045Xz;
    @C0064Am(aul = "c17500aba195feb31a585acb2a38f1a1", aum = 1)

    /* renamed from: bK */
    public UUID f2046bK;
    @C0064Am(aul = "589209dc843247fc30bc79163d22f4e1", aum = 2)
    public String handle;
    @C0064Am(aul = "c17500aba195feb31a585acb2a38f1a1", aum = -2)

    /* renamed from: oL */
    public long f2047oL;
    @C0064Am(aul = "c17500aba195feb31a585acb2a38f1a1", aum = -1)

    /* renamed from: oS */
    public byte f2048oS;
    @C0064Am(aul = "589209dc843247fc30bc79163d22f4e1", aum = -2)

    /* renamed from: ok */
    public long f2049ok;
    @C0064Am(aul = "589209dc843247fc30bc79163d22f4e1", aum = -1)

    /* renamed from: on */
    public byte f2050on;

    public C1542Wf() {
    }

    public C1542Wf(C1616Xf xf) {
        super(xf);
    }

    /* renamed from: c */
    public C5663aRz[] mo309c() {
        return PopulationBehaviour._m_fields;
    }

    /* renamed from: d */
    public void mo310d() {
        super.mo310d();
        this.f2044Xx = null;
        this.f2046bK = null;
        this.handle = null;
        this.f2045Xz = 0;
        this.f2047oL = 0;
        this.f2049ok = 0;
        this.f2043XB = 0;
        this.f2048oS = 0;
        this.f2050on = 0;
    }
}
