package logic.data.mbean;

import game.network.message.serializable.C3438ra;
import game.script.mission.scripting.ReconNpcMissionScript;
import game.script.ship.Ship;
import logic.baa.C1616Xf;
import logic.res.code.C5663aRz;
import p001a.C0064Am;

/* renamed from: a.aLp  reason: case insensitive filesystem */
public class C5497aLp extends aMY {
    @C0064Am(aul = "4f2904991db6ec42ac8b36030b9e9454", aum = 0)
    public C3438ra<Ship> gWX;
    @C0064Am(aul = "4f2904991db6ec42ac8b36030b9e9454", aum = -2)
    public long ilh;
    @C0064Am(aul = "4f2904991db6ec42ac8b36030b9e9454", aum = -1)
    public byte ili;

    public C5497aLp() {
    }

    public C5497aLp(C1616Xf xf) {
        super(xf);
    }

    /* renamed from: c */
    public C5663aRz[] mo309c() {
        return ReconNpcMissionScript._m_fields;
    }

    /* renamed from: d */
    public void mo310d() {
        super.mo310d();
        this.gWX = null;
        this.ilh = 0;
        this.ili = 0;
    }
}
