package logic.data.mbean;

import game.script.item.ItemType;
import game.script.newmarket.CommercialOrderInfo;
import game.script.ship.Station;
import logic.baa.C1616Xf;
import logic.res.code.C5663aRz;
import p001a.C0064Am;

/* renamed from: a.Bf */
public class C0134Bf extends C5292aDs {
    @C0064Am(aul = "68d8c553902e61e39739b8db26aa83ef", aum = 7)

    /* renamed from: Q */
    public long f231Q;
    @C0064Am(aul = "68d8c553902e61e39739b8db26aa83ef", aum = -2)

    /* renamed from: ah */
    public long f232ah;
    @C0064Am(aul = "213a081a87af0427a43f6a8cece90c35", aum = -2)
    public long aih;
    @C0064Am(aul = "b5b3077527fa07efba201f341eff5abc", aum = -2)
    public long aii;
    @C0064Am(aul = "078c9b4407456b2c13a0c2f541ec6feb", aum = -2)
    public long aij;
    @C0064Am(aul = "a3dee6875881bd5eca931ef5afa45d73", aum = -2)
    public long aik;
    @C0064Am(aul = "feb9d9dc2c2309eec8aa2cf24f2a4311", aum = -2)
    public long ail;
    @C0064Am(aul = "7ad536d3cc7393d7f00ca31dff9b6777", aum = -2)
    public long aiq;
    @C0064Am(aul = "213a081a87af0427a43f6a8cece90c35", aum = -1)
    public byte air;
    @C0064Am(aul = "b5b3077527fa07efba201f341eff5abc", aum = -1)
    public byte ais;
    @C0064Am(aul = "078c9b4407456b2c13a0c2f541ec6feb", aum = -1)
    public byte ait;
    @C0064Am(aul = "a3dee6875881bd5eca931ef5afa45d73", aum = -1)
    public byte aiu;
    @C0064Am(aul = "feb9d9dc2c2309eec8aa2cf24f2a4311", aum = -1)
    public byte aiv;
    @C0064Am(aul = "7ad536d3cc7393d7f00ca31dff9b6777", aum = -1)
    public byte aiz;
    @C0064Am(aul = "68d8c553902e61e39739b8db26aa83ef", aum = -1)

    /* renamed from: aw */
    public byte f233aw;
    @C0064Am(aul = "f29ae217c69908067936eddbbd40436d", aum = 0)

    /* renamed from: cY */
    public ItemType f234cY;
    @C0064Am(aul = "1ded91cc9fc6ee0340ce958fdad88c07", aum = 8)
    public long ckO;
    @C0064Am(aul = "7ad536d3cc7393d7f00ca31dff9b6777", aum = 9)
    public CommercialOrderInfo.CommercialOrderTicker ckP;
    @C0064Am(aul = "1ded91cc9fc6ee0340ce958fdad88c07", aum = -2)
    public long ckQ;
    @C0064Am(aul = "1ded91cc9fc6ee0340ce958fdad88c07", aum = -1)
    public byte ckR;
    @C0064Am(aul = "f29ae217c69908067936eddbbd40436d", aum = -2)

    /* renamed from: dg */
    public long f235dg;
    @C0064Am(aul = "f29ae217c69908067936eddbbd40436d", aum = -1)

    /* renamed from: dn */
    public byte f236dn;
    @C0064Am(aul = "68087d1fcd79ea8060a74128eefb92f0", aum = 5)

    /* renamed from: iH */
    public Station f237iH;
    @C0064Am(aul = "68087d1fcd79ea8060a74128eefb92f0", aum = -2)

    /* renamed from: iK */
    public long f238iK;
    @C0064Am(aul = "68087d1fcd79ea8060a74128eefb92f0", aum = -1)

    /* renamed from: iN */
    public byte f239iN;
    @C0064Am(aul = "213a081a87af0427a43f6a8cece90c35", aum = 1)

    /* renamed from: kP */
    public int f240kP;
    @C0064Am(aul = "b5b3077527fa07efba201f341eff5abc", aum = 2)

    /* renamed from: kR */
    public int f241kR;
    @C0064Am(aul = "078c9b4407456b2c13a0c2f541ec6feb", aum = 3)

    /* renamed from: kT */
    public int f242kT;
    @C0064Am(aul = "a3dee6875881bd5eca931ef5afa45d73", aum = 4)

    /* renamed from: kV */
    public long f243kV;
    @C0064Am(aul = "feb9d9dc2c2309eec8aa2cf24f2a4311", aum = 6)

    /* renamed from: kY */
    public long f244kY;

    public C0134Bf() {
    }

    public C0134Bf(C1616Xf xf) {
        super(xf);
    }

    /* renamed from: c */
    public C5663aRz[] mo309c() {
        return CommercialOrderInfo._m_fields;
    }

    /* renamed from: d */
    public void mo310d() {
        super.mo310d();
        this.f234cY = null;
        this.f240kP = 0;
        this.f241kR = 0;
        this.f242kT = 0;
        this.f243kV = 0;
        this.f237iH = null;
        this.f244kY = 0;
        this.f231Q = 0;
        this.ckO = 0;
        this.ckP = null;
        this.f235dg = 0;
        this.aih = 0;
        this.aii = 0;
        this.aij = 0;
        this.aik = 0;
        this.f238iK = 0;
        this.ail = 0;
        this.f232ah = 0;
        this.ckQ = 0;
        this.aiq = 0;
        this.f236dn = 0;
        this.air = 0;
        this.ais = 0;
        this.ait = 0;
        this.aiu = 0;
        this.f239iN = 0;
        this.aiv = 0;
        this.f233aw = 0;
        this.ckR = 0;
        this.aiz = 0;
    }
}
