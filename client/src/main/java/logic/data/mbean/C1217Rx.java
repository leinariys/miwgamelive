package logic.data.mbean;

import game.network.message.serializable.C2686iZ;
import game.script.associates.Associate;
import game.script.associates.Associates;
import game.script.player.Player;
import logic.baa.C1616Xf;
import logic.res.code.C5663aRz;
import p001a.C0064Am;

/* renamed from: a.Rx */
public class C1217Rx extends C5292aDs {
    @C0064Am(aul = "8f10827e9c11e4919c4335efb0a90e93", aum = 0)

    /* renamed from: P */
    public Player f1519P;
    @C0064Am(aul = "8f10827e9c11e4919c4335efb0a90e93", aum = -2)

    /* renamed from: ag */
    public long f1520ag;
    @C0064Am(aul = "8f10827e9c11e4919c4335efb0a90e93", aum = -1)

    /* renamed from: av */
    public byte f1521av;
    @C0064Am(aul = "ea9ad70c7777c88ffdc403c593cfbf69", aum = 1)
    public C2686iZ<Associate> eaq;
    @C0064Am(aul = "86602a25798d75959cbd224d397e3cf4", aum = 2)
    public C2686iZ<Player> ear;
    @C0064Am(aul = "ea9ad70c7777c88ffdc403c593cfbf69", aum = -2)
    public long eas;
    @C0064Am(aul = "86602a25798d75959cbd224d397e3cf4", aum = -2)
    public long eat;
    @C0064Am(aul = "ea9ad70c7777c88ffdc403c593cfbf69", aum = -1)
    public byte eau;
    @C0064Am(aul = "86602a25798d75959cbd224d397e3cf4", aum = -1)
    public byte eav;

    public C1217Rx() {
    }

    public C1217Rx(C1616Xf xf) {
        super(xf);
    }

    /* renamed from: c */
    public C5663aRz[] mo309c() {
        return Associates._m_fields;
    }

    /* renamed from: d */
    public void mo310d() {
        super.mo310d();
        this.f1519P = null;
        this.eaq = null;
        this.ear = null;
        this.f1520ag = 0;
        this.eas = 0;
        this.eat = 0;
        this.f1521av = 0;
        this.eau = 0;
        this.eav = 0;
    }
}
