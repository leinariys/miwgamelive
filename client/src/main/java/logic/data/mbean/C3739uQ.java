package logic.data.mbean;

import game.script.nls.NLSCruiseSpeed;
import logic.baa.C1616Xf;
import logic.res.code.C5663aRz;
import p001a.C0064Am;
import taikodom.infra.script.I18NString;

/* renamed from: a.uQ */
public class C3739uQ extends C2484fi {
    @C0064Am(aul = "06964d3b65083a020bdab27101f93d66", aum = -2)
    public long bxA;
    @C0064Am(aul = "8d2e937314ea8bb55feaab7bfdd3b5e6", aum = -2)
    public long bxB;
    @C0064Am(aul = "1a16d1a654867385fc9c847219105e20", aum = -1)
    public byte bxC;
    @C0064Am(aul = "0b8fc35901ee5fadf759ac8ced4d7429", aum = -1)
    public byte bxD;
    @C0064Am(aul = "2f60fe604651c308e23a591e13340604", aum = -1)
    public byte bxE;
    @C0064Am(aul = "96dfcb2dd4c771807e83bf12d24475f5", aum = -1)
    public byte bxF;
    @C0064Am(aul = "06964d3b65083a020bdab27101f93d66", aum = -1)
    public byte bxG;
    @C0064Am(aul = "8d2e937314ea8bb55feaab7bfdd3b5e6", aum = -1)
    public byte bxH;
    @C0064Am(aul = "1a16d1a654867385fc9c847219105e20", aum = 0)
    public I18NString bxq;
    @C0064Am(aul = "0b8fc35901ee5fadf759ac8ced4d7429", aum = 1)
    public I18NString bxr;
    @C0064Am(aul = "2f60fe604651c308e23a591e13340604", aum = 2)
    public I18NString bxs;
    @C0064Am(aul = "96dfcb2dd4c771807e83bf12d24475f5", aum = 3)
    public I18NString bxt;
    @C0064Am(aul = "06964d3b65083a020bdab27101f93d66", aum = 4)
    public I18NString bxu;
    @C0064Am(aul = "8d2e937314ea8bb55feaab7bfdd3b5e6", aum = 5)
    public I18NString bxv;
    @C0064Am(aul = "1a16d1a654867385fc9c847219105e20", aum = -2)
    public long bxw;
    @C0064Am(aul = "0b8fc35901ee5fadf759ac8ced4d7429", aum = -2)
    public long bxx;
    @C0064Am(aul = "2f60fe604651c308e23a591e13340604", aum = -2)
    public long bxy;
    @C0064Am(aul = "96dfcb2dd4c771807e83bf12d24475f5", aum = -2)
    public long bxz;

    public C3739uQ() {
    }

    public C3739uQ(C1616Xf xf) {
        super(xf);
    }

    /* renamed from: c */
    public C5663aRz[] mo309c() {
        return NLSCruiseSpeed._m_fields;
    }

    /* renamed from: d */
    public void mo310d() {
        super.mo310d();
        this.bxq = null;
        this.bxr = null;
        this.bxs = null;
        this.bxt = null;
        this.bxu = null;
        this.bxv = null;
        this.bxw = 0;
        this.bxx = 0;
        this.bxy = 0;
        this.bxz = 0;
        this.bxA = 0;
        this.bxB = 0;
        this.bxC = 0;
        this.bxD = 0;
        this.bxE = 0;
        this.bxF = 0;
        this.bxG = 0;
        this.bxH = 0;
    }
}
