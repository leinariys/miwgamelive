package logic.data.mbean;

import game.script.item.ClipType;
import game.script.item.ProjectileWeaponType;
import logic.baa.C1616Xf;
import logic.res.code.C5663aRz;
import p001a.C0064Am;

/* renamed from: a.Ti */
public class C1353Ti extends C3967xU {
    @C0064Am(aul = "5c740806a41dbd3f96728c0c9117ff18", aum = 1)
    public ClipType atA;
    @C0064Am(aul = "213a6c0ab6aca4bb021383ca941231da", aum = 2)
    public int atB;
    @C0064Am(aul = "d022dcf28047f08d93dbca448bc6c38f", aum = -2)
    public long atE;
    @C0064Am(aul = "5c740806a41dbd3f96728c0c9117ff18", aum = -2)
    public long atF;
    @C0064Am(aul = "213a6c0ab6aca4bb021383ca941231da", aum = -2)
    public long atG;
    @C0064Am(aul = "d022dcf28047f08d93dbca448bc6c38f", aum = -1)
    public byte atJ;
    @C0064Am(aul = "5c740806a41dbd3f96728c0c9117ff18", aum = -1)
    public byte atK;
    @C0064Am(aul = "213a6c0ab6aca4bb021383ca941231da", aum = -1)
    public byte atL;
    @C0064Am(aul = "d022dcf28047f08d93dbca448bc6c38f", aum = 0)
    public int atz;

    public C1353Ti() {
    }

    public C1353Ti(C1616Xf xf) {
        super(xf);
    }

    /* renamed from: c */
    public C5663aRz[] mo309c() {
        return ProjectileWeaponType._m_fields;
    }

    /* renamed from: d */
    public void mo310d() {
        super.mo310d();
        this.atz = 0;
        this.atA = null;
        this.atB = 0;
        this.atE = 0;
        this.atF = 0;
        this.atG = 0;
        this.atJ = 0;
        this.atK = 0;
        this.atL = 0;
    }
}
