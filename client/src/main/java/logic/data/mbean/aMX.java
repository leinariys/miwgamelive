package logic.data.mbean;

import game.CollisionFXSet;
import game.geometry.Vec3d;
import game.network.message.serializable.C3438ra;
import game.script.Actor;
import game.script.Character;
import game.script.item.Component;
import game.script.item.ShipItem;
import game.script.item.Weapon;
import game.script.pda.DatabaseCategory;
import game.script.pda.TaikopediaEntry;
import game.script.resource.Asset;
import game.script.ship.*;
import game.script.space.LootType;
import game.script.util.AdapterLikedList;
import logic.baa.C1616Xf;
import logic.res.code.C5663aRz;
import p001a.C0064Am;
import p001a.C6809auB;
import taikodom.infra.script.I18NString;

/* renamed from: a.aMX */
public class aMX extends aLU {
    @C0064Am(aul = "d65771ff3efabbe9926ff6d0008a8282", aum = 22)

    /* renamed from: LQ */
    public Asset f3355LQ;
    @C0064Am(aul = "ce62fd24fad44a0c6c2bd67edb82fd22", aum = 36)

    /* renamed from: VV */
    public Hull f3356VV;
    @C0064Am(aul = "540b2fa017eb685368f950fc172fd658", aum = 30)

    /* renamed from: WL */
    public float f3357WL;
    @C0064Am(aul = "76a2e596d427a588c043ad45d40b00ac", aum = 31)

    /* renamed from: WM */
    public float f3358WM;
    @C0064Am(aul = "5908121079e3b58a89a70b76f3e8c976", aum = 6)

    /* renamed from: Wb */
    public Asset f3359Wb;
    @C0064Am(aul = "de8d98cfda4e4684609496bf31c5984d", aum = 7)

    /* renamed from: Wd */
    public Asset f3360Wd;
    @C0064Am(aul = "15f0692836c7328dd4fd758c7ac6fb58", aum = 34)
    public AdapterLikedList<ShipAdapter> aOw;
    @C0064Am(aul = "15f0692836c7328dd4fd758c7ac6fb58", aum = -2)
    public long aUE;
    @C0064Am(aul = "15f0692836c7328dd4fd758c7ac6fb58", aum = -1)
    public byte aUH;
    @C0064Am(aul = "2d1b3508f6de8b3fb6bc8b7f77f1bc82", aum = 43)
    public C6809auB.C1996a aZI;
    @C0064Am(aul = "f338f47e91e6988f5ea6eaaf9d0a2013", aum = -2)
    public long ajM;
    @C0064Am(aul = "f338f47e91e6988f5ea6eaaf9d0a2013", aum = -1)
    public byte ajY;
    @C0064Am(aul = "4f6ed4c9369bb645b8d4d8fb201084d2", aum = 44)
    public Actor ams;
    @C0064Am(aul = "4f6ed4c9369bb645b8d4d8fb201084d2", aum = -2)
    public long aun;
    @C0064Am(aul = "4f6ed4c9369bb645b8d4d8fb201084d2", aum = -1)
    public byte auu;
    @C0064Am(aul = "09d79fe35c3692b7feeeae637bcac0c8", aum = -2)
    public long avd;
    @C0064Am(aul = "09d79fe35c3692b7feeeae637bcac0c8", aum = -1)
    public byte avi;
    @C0064Am(aul = "d65771ff3efabbe9926ff6d0008a8282", aum = -2)
    public long ayc;
    @C0064Am(aul = "d65771ff3efabbe9926ff6d0008a8282", aum = -1)
    public byte ayr;
    @C0064Am(aul = "77920c33a6f29cb25c6827ec067906b7", aum = -2)
    public long bHw;
    @C0064Am(aul = "77920c33a6f29cb25c6827ec067906b7", aum = -1)
    public byte bHy;
    @C0064Am(aul = "1394e5857a668e774a91cbcffd431fb2", aum = -1)
    public byte bhA;
    @C0064Am(aul = "5908121079e3b58a89a70b76f3e8c976", aum = -1)
    public byte bhC;
    @C0064Am(aul = "de8d98cfda4e4684609496bf31c5984d", aum = -1)
    public byte bhD;
    @C0064Am(aul = "ce62fd24fad44a0c6c2bd67edb82fd22", aum = -1)
    public byte bhF;
    @C0064Am(aul = "c0bf54703b959d66b2da7d00a53168c7", aum = 14)
    public float bhk;
    @C0064Am(aul = "d6f2f05364d791966adfd5a66466d990", aum = 10)
    public float bhl;
    @C0064Am(aul = "c0bf54703b959d66b2da7d00a53168c7", aum = -2)
    public long bho;
    @C0064Am(aul = "60b44e37464206ba7cb7aa01c04aacb8", aum = -2)
    public long bhp;
    @C0064Am(aul = "d6f2f05364d791966adfd5a66466d990", aum = -2)
    public long bhq;
    @C0064Am(aul = "1394e5857a668e774a91cbcffd431fb2", aum = -2)
    public long bhr;
    @C0064Am(aul = "5908121079e3b58a89a70b76f3e8c976", aum = -2)
    public long bht;
    @C0064Am(aul = "de8d98cfda4e4684609496bf31c5984d", aum = -2)
    public long bhu;
    @C0064Am(aul = "ce62fd24fad44a0c6c2bd67edb82fd22", aum = -2)
    public long bhw;
    @C0064Am(aul = "c0bf54703b959d66b2da7d00a53168c7", aum = -1)
    public byte bhx;
    @C0064Am(aul = "60b44e37464206ba7cb7aa01c04aacb8", aum = -1)
    public byte bhy;
    @C0064Am(aul = "d6f2f05364d791966adfd5a66466d990", aum = -1)
    public byte bhz;
    @C0064Am(aul = "41ca32e6775ce7680bf4af9269151bb5", aum = 0)
    public CargoHoldType bnC;
    @C0064Am(aul = "408f6518272376f783fcf481298532d7", aum = 3)
    public CollisionFXSet bnE;
    @C0064Am(aul = "f338f47e91e6988f5ea6eaaf9d0a2013", aum = 4)
    public CruiseSpeedType bnG;
    @C0064Am(aul = "f5a599db78dcbc71b7443510492c74aa", aum = 5)
    public Asset bnI;
    @C0064Am(aul = "6ca62339cef27a0e7dfe28855201c3e9", aum = 8)
    public I18NString bnK;
    @C0064Am(aul = "59f03f8d501ae96eb33d3189aceaf769", aum = 9)
    public LootType bnM;
    @C0064Am(aul = "bc2836484f3b82dbccc5e35292d0021e", aum = 12)
    public float bnQ;
    @C0064Am(aul = "8e759831b6b819a453787c2c334bfc98", aum = 13)
    public float bnS;
    @C0064Am(aul = "4e786aee15d241bf6f5e178492e073a9", aum = 16)
    public float bnW;
    @C0064Am(aul = "703113558d172f6e6df6a8f27bc7a87f", aum = 17)
    public float bnY;
    @C0064Am(aul = "6307cbd895f869dbf6b3c6c2941678f1", aum = 37)
    public CargoHold boC;
    @C0064Am(aul = "c79cf2d18597b17dafdce6345a115078", aum = 38)
    public C3438ra<ShipStructure> boE;
    @C0064Am(aul = "67a214c3f2bb3da8ce4cd82c57fa1d32", aum = 39)
    public Character boH;
    @C0064Am(aul = "b6f3e2426c4db92cb5c518db4dde10f4", aum = 40)
    public C3438ra<Component> boJ;
    @C0064Am(aul = "ff062fb3e7385f02b8dc783cc72aa5dd", aum = 41)
    public ShipItem boL;
    @C0064Am(aul = "f94501d21485a328fc44f085e8f959ce", aum = 42)
    public long boN;
    @C0064Am(aul = "5eaa252d59d9bdd4ae9bdf5049e6aebd", aum = 45)
    public C3438ra<Turret> boR;
    @C0064Am(aul = "f7973441f80aa15f5856ac20a967e2e2", aum = 46)
    public Weapon boT;
    @C0064Am(aul = "7729e8215e41642c9f9c1cf2db254825", aum = 47)
    public Weapon boV;
    @C0064Am(aul = "f57039722771c290261270fc11ea215a", aum = 18)
    public float boa;
    @C0064Am(aul = "95a88713e9f15ed67f247d88fdeabe24", aum = 19)
    public Asset boc;
    @C0064Am(aul = "78d648ae28cc6e326c3d7766c839c604", aum = 20)
    public float boe;
    @C0064Am(aul = "77920c33a6f29cb25c6827ec067906b7", aum = 21)
    public float bog;
    @C0064Am(aul = "fe70592ffdee3463b6fc2a6ec4a2c282", aum = 23)
    public float boh;
    @C0064Am(aul = "436986ecb8ccee6ec5ede89815f949ed", aum = 24)
    public C3438ra<ShipStructureType> boj;
    @C0064Am(aul = "5e8a70067774850a0ab59335dcf38c43", aum = 26)
    public C3438ra<C3315qE> bol;
    @C0064Am(aul = "cda67b981bd6b81eddc11b146d278d4f", aum = 27)
    public Vec3d bon;
    @C0064Am(aul = "eaad92915612d97ae95413006a61691c", aum = 28)
    public Vec3d bop;
    @C0064Am(aul = "f9e840447561fabf74bee25bb8054b85", aum = 29)
    public Vec3d bor;
    @C0064Am(aul = "cf25afcf2d302c59983c9b7d46d6dd04", aum = 33)
    public float bov;
    @C0064Am(aul = "8559899c23517ad1c07a27c42c27859c", aum = 35)
    public CruiseSpeed boy;
    @C0064Am(aul = "f9350cce5941de23e828745965eb122e", aum = 48)
    public long bpa;
    @C0064Am(aul = "955302f0b2669d8f3fc357b44eb4d71b", aum = 49)
    public Ship.DeathInformation bpc;
    @C0064Am(aul = "95f9f1daff345868b5c0f0a6e0e9ce34", aum = 50)
    public float bpg;
    @C0064Am(aul = "2283a964f9c6533a51ded6e38d40a0e4", aum = 51)
    public float bpi;
    @C0064Am(aul = "a36555ac5f2f582ce9600a99ade722ea", aum = 52)
    public float bpk;
    @C0064Am(aul = "04de02b87e1ef104769337bf29e98626", aum = 53)
    public float bpm;
    @C0064Am(aul = "2f73c026196fec0b941468ccfd34fa6b", aum = 54)
    public float bpo;
    @C0064Am(aul = "60b44e37464206ba7cb7aa01c04aacb8", aum = 15)

    /* renamed from: dW */
    public float f3361dW;
    @C0064Am(aul = "1394e5857a668e774a91cbcffd431fb2", aum = 11)

    /* renamed from: dX */
    public float f3362dX;
    @C0064Am(aul = "408f6518272376f783fcf481298532d7", aum = -2)
    public long fPc;
    @C0064Am(aul = "95a88713e9f15ed67f247d88fdeabe24", aum = -2)
    public long fPf;
    @C0064Am(aul = "408f6518272376f783fcf481298532d7", aum = -1)
    public byte fPj;
    @C0064Am(aul = "95a88713e9f15ed67f247d88fdeabe24", aum = -1)
    public byte fPm;
    @C0064Am(aul = "67a214c3f2bb3da8ce4cd82c57fa1d32", aum = -2)
    public long goP;
    @C0064Am(aul = "67a214c3f2bb3da8ce4cd82c57fa1d32", aum = -1)
    public byte goW;
    @C0064Am(aul = "59f03f8d501ae96eb33d3189aceaf769", aum = -2)
    public long hhc;
    @C0064Am(aul = "59f03f8d501ae96eb33d3189aceaf769", aum = -1)
    public byte hhd;
    @C0064Am(aul = "4e786aee15d241bf6f5e178492e073a9", aum = -2)
    public long imA;
    @C0064Am(aul = "703113558d172f6e6df6a8f27bc7a87f", aum = -2)
    public long imB;
    @C0064Am(aul = "f57039722771c290261270fc11ea215a", aum = -2)
    public long imC;
    @C0064Am(aul = "78d648ae28cc6e326c3d7766c839c604", aum = -2)
    public long imD;
    @C0064Am(aul = "fe70592ffdee3463b6fc2a6ec4a2c282", aum = -2)
    public long imE;
    @C0064Am(aul = "436986ecb8ccee6ec5ede89815f949ed", aum = -2)
    public long imF;
    @C0064Am(aul = "5e8a70067774850a0ab59335dcf38c43", aum = -2)
    public long imG;
    @C0064Am(aul = "cda67b981bd6b81eddc11b146d278d4f", aum = -2)
    public long imH;
    @C0064Am(aul = "eaad92915612d97ae95413006a61691c", aum = -2)
    public long imI;
    @C0064Am(aul = "f9e840447561fabf74bee25bb8054b85", aum = -2)
    public long imJ;
    @C0064Am(aul = "540b2fa017eb685368f950fc172fd658", aum = -2)
    public long imK;
    @C0064Am(aul = "76a2e596d427a588c043ad45d40b00ac", aum = -2)
    public long imL;
    @C0064Am(aul = "cf25afcf2d302c59983c9b7d46d6dd04", aum = -2)
    public long imM;
    @C0064Am(aul = "41ca32e6775ce7680bf4af9269151bb5", aum = -1)
    public byte imN;
    @C0064Am(aul = "f5a599db78dcbc71b7443510492c74aa", aum = -1)
    public byte imO;
    @C0064Am(aul = "6ca62339cef27a0e7dfe28855201c3e9", aum = -1)
    public byte imP;
    @C0064Am(aul = "bc2836484f3b82dbccc5e35292d0021e", aum = -1)
    public byte imQ;
    @C0064Am(aul = "8e759831b6b819a453787c2c334bfc98", aum = -1)
    public byte imR;
    @C0064Am(aul = "4e786aee15d241bf6f5e178492e073a9", aum = -1)
    public byte imS;
    @C0064Am(aul = "703113558d172f6e6df6a8f27bc7a87f", aum = -1)
    public byte imT;
    @C0064Am(aul = "f57039722771c290261270fc11ea215a", aum = -1)
    public byte imU;
    @C0064Am(aul = "78d648ae28cc6e326c3d7766c839c604", aum = -1)
    public byte imV;
    @C0064Am(aul = "fe70592ffdee3463b6fc2a6ec4a2c282", aum = -1)
    public byte imW;
    @C0064Am(aul = "436986ecb8ccee6ec5ede89815f949ed", aum = -1)
    public byte imX;
    @C0064Am(aul = "5e8a70067774850a0ab59335dcf38c43", aum = -1)
    public byte imY;
    @C0064Am(aul = "cda67b981bd6b81eddc11b146d278d4f", aum = -1)
    public byte imZ;
    @C0064Am(aul = "41ca32e6775ce7680bf4af9269151bb5", aum = -2)
    public long imv;
    @C0064Am(aul = "f5a599db78dcbc71b7443510492c74aa", aum = -2)
    public long imw;
    @C0064Am(aul = "6ca62339cef27a0e7dfe28855201c3e9", aum = -2)
    public long imx;
    @C0064Am(aul = "bc2836484f3b82dbccc5e35292d0021e", aum = -2)
    public long imy;
    @C0064Am(aul = "8e759831b6b819a453787c2c334bfc98", aum = -2)
    public long imz;
    @C0064Am(aul = "eaad92915612d97ae95413006a61691c", aum = -1)
    public byte ina;
    @C0064Am(aul = "f9e840447561fabf74bee25bb8054b85", aum = -1)
    public byte inb;
    @C0064Am(aul = "540b2fa017eb685368f950fc172fd658", aum = -1)
    public byte inc;
    @C0064Am(aul = "76a2e596d427a588c043ad45d40b00ac", aum = -1)
    public byte ind;
    @C0064Am(aul = "cf25afcf2d302c59983c9b7d46d6dd04", aum = -1)
    public byte ine;
    @C0064Am(aul = "ff062fb3e7385f02b8dc783cc72aa5dd", aum = -2)
    public long iqA;
    @C0064Am(aul = "f94501d21485a328fc44f085e8f959ce", aum = -2)
    public long iqB;
    @C0064Am(aul = "5eaa252d59d9bdd4ae9bdf5049e6aebd", aum = -2)
    public long iqC;
    @C0064Am(aul = "f7973441f80aa15f5856ac20a967e2e2", aum = -2)
    public long iqD;
    @C0064Am(aul = "7729e8215e41642c9f9c1cf2db254825", aum = -2)
    public long iqE;
    @C0064Am(aul = "f9350cce5941de23e828745965eb122e", aum = -2)
    public long iqF;
    @C0064Am(aul = "955302f0b2669d8f3fc357b44eb4d71b", aum = -2)
    public long iqG;
    @C0064Am(aul = "95f9f1daff345868b5c0f0a6e0e9ce34", aum = -2)
    public long iqH;
    @C0064Am(aul = "2283a964f9c6533a51ded6e38d40a0e4", aum = -2)
    public long iqI;
    @C0064Am(aul = "a36555ac5f2f582ce9600a99ade722ea", aum = -2)
    public long iqJ;
    @C0064Am(aul = "04de02b87e1ef104769337bf29e98626", aum = -2)
    public long iqK;
    @C0064Am(aul = "2f73c026196fec0b941468ccfd34fa6b", aum = -2)
    public long iqL;
    @C0064Am(aul = "8559899c23517ad1c07a27c42c27859c", aum = -1)
    public byte iqM;
    @C0064Am(aul = "6307cbd895f869dbf6b3c6c2941678f1", aum = -1)
    public byte iqN;
    @C0064Am(aul = "c79cf2d18597b17dafdce6345a115078", aum = -1)
    public byte iqO;
    @C0064Am(aul = "b6f3e2426c4db92cb5c518db4dde10f4", aum = -1)
    public byte iqP;
    @C0064Am(aul = "ff062fb3e7385f02b8dc783cc72aa5dd", aum = -1)
    public byte iqQ;
    @C0064Am(aul = "f94501d21485a328fc44f085e8f959ce", aum = -1)
    public byte iqR;
    @C0064Am(aul = "5eaa252d59d9bdd4ae9bdf5049e6aebd", aum = -1)
    public byte iqS;
    @C0064Am(aul = "f7973441f80aa15f5856ac20a967e2e2", aum = -1)
    public byte iqT;
    @C0064Am(aul = "7729e8215e41642c9f9c1cf2db254825", aum = -1)
    public byte iqU;
    @C0064Am(aul = "f9350cce5941de23e828745965eb122e", aum = -1)
    public byte iqV;
    @C0064Am(aul = "955302f0b2669d8f3fc357b44eb4d71b", aum = -1)
    public byte iqW;
    @C0064Am(aul = "95f9f1daff345868b5c0f0a6e0e9ce34", aum = -1)
    public byte iqX;
    @C0064Am(aul = "2283a964f9c6533a51ded6e38d40a0e4", aum = -1)
    public byte iqY;
    @C0064Am(aul = "a36555ac5f2f582ce9600a99ade722ea", aum = -1)
    public byte iqZ;
    @C0064Am(aul = "8559899c23517ad1c07a27c42c27859c", aum = -2)
    public long iqw;
    @C0064Am(aul = "6307cbd895f869dbf6b3c6c2941678f1", aum = -2)
    public long iqx;
    @C0064Am(aul = "c79cf2d18597b17dafdce6345a115078", aum = -2)
    public long iqy;
    @C0064Am(aul = "b6f3e2426c4db92cb5c518db4dde10f4", aum = -2)
    public long iqz;
    @C0064Am(aul = "04de02b87e1ef104769337bf29e98626", aum = -1)
    public byte ira;
    @C0064Am(aul = "2f73c026196fec0b941468ccfd34fa6b", aum = -1)
    public byte irb;
    @C0064Am(aul = "c3d92a8da7ad2013bb2a64cb2f6ec4e8", aum = 1)

    /* renamed from: jS */
    public DatabaseCategory f3363jS;
    @C0064Am(aul = "2c9737909a77f97389e30aaed1101b65", aum = 2)

    /* renamed from: jT */
    public C3438ra<TaikopediaEntry> f3364jT;
    @C0064Am(aul = "6bdc7d72f8bbf3f16b2963456b47fc8e", aum = 32)

    /* renamed from: jV */
    public float f3365jV;
    @C0064Am(aul = "c3d92a8da7ad2013bb2a64cb2f6ec4e8", aum = -2)

    /* renamed from: jX */
    public long f3366jX;
    @C0064Am(aul = "2c9737909a77f97389e30aaed1101b65", aum = -2)

    /* renamed from: jY */
    public long f3367jY;
    @C0064Am(aul = "6bdc7d72f8bbf3f16b2963456b47fc8e", aum = -2)

    /* renamed from: ka */
    public long f3368ka;
    @C0064Am(aul = "c3d92a8da7ad2013bb2a64cb2f6ec4e8", aum = -1)

    /* renamed from: kc */
    public byte f3369kc;
    @C0064Am(aul = "2c9737909a77f97389e30aaed1101b65", aum = -1)

    /* renamed from: kd */
    public byte f3370kd;
    @C0064Am(aul = "6bdc7d72f8bbf3f16b2963456b47fc8e", aum = -1)

    /* renamed from: kf */
    public byte f3371kf;
    @C0064Am(aul = "2d1b3508f6de8b3fb6bc8b7f77f1bc82", aum = -2)

    /* renamed from: ol */
    public long f3372ol;
    @C0064Am(aul = "2d1b3508f6de8b3fb6bc8b7f77f1bc82", aum = -1)

    /* renamed from: oo */
    public byte f3373oo;
    @C0064Am(aul = "09d79fe35c3692b7feeeae637bcac0c8", aum = 25)

    /* renamed from: uT */
    public String f3374uT;

    public aMX() {
    }

    public aMX(C1616Xf xf) {
        super(xf);
    }

    /* renamed from: c */
    public C5663aRz[] mo309c() {
        return Ship._m_fields;
    }

    /* renamed from: d */
    public void mo310d() {
        super.mo310d();
        this.bnC = null;
        this.f3363jS = null;
        this.f3364jT = null;
        this.bnE = null;
        this.bnG = null;
        this.bnI = null;
        this.f3359Wb = null;
        this.f3360Wd = null;
        this.bnK = null;
        this.bnM = null;
        this.bhl = 0.0f;
        this.f3362dX = 0.0f;
        this.bnQ = 0.0f;
        this.bnS = 0.0f;
        this.bhk = 0.0f;
        this.f3361dW = 0.0f;
        this.bnW = 0.0f;
        this.bnY = 0.0f;
        this.boa = 0.0f;
        this.boc = null;
        this.boe = 0.0f;
        this.bog = 0.0f;
        this.f3355LQ = null;
        this.boh = 0.0f;
        this.boj = null;
        this.f3374uT = null;
        this.bol = null;
        this.bon = null;
        this.bop = null;
        this.bor = null;
        this.f3357WL = 0.0f;
        this.f3358WM = 0.0f;
        this.f3365jV = 0.0f;
        this.bov = 0.0f;
        this.aOw = null;
        this.boy = null;
        this.f3356VV = null;
        this.boC = null;
        this.boE = null;
        this.boH = null;
        this.boJ = null;
        this.boL = null;
        this.boN = 0;
        this.aZI = null;
        this.ams = null;
        this.boR = null;
        this.boT = null;
        this.boV = null;
        this.bpa = 0;
        this.bpc = null;
        this.bpg = 0.0f;
        this.bpi = 0.0f;
        this.bpk = 0.0f;
        this.bpm = 0.0f;
        this.bpo = 0.0f;
        this.imv = 0;
        this.f3366jX = 0;
        this.f3367jY = 0;
        this.fPc = 0;
        this.ajM = 0;
        this.imw = 0;
        this.bht = 0;
        this.bhu = 0;
        this.imx = 0;
        this.hhc = 0;
        this.bhq = 0;
        this.bhr = 0;
        this.imy = 0;
        this.imz = 0;
        this.bho = 0;
        this.bhp = 0;
        this.imA = 0;
        this.imB = 0;
        this.imC = 0;
        this.fPf = 0;
        this.imD = 0;
        this.bHw = 0;
        this.ayc = 0;
        this.imE = 0;
        this.imF = 0;
        this.avd = 0;
        this.imG = 0;
        this.imH = 0;
        this.imI = 0;
        this.imJ = 0;
        this.imK = 0;
        this.imL = 0;
        this.f3368ka = 0;
        this.imM = 0;
        this.aUE = 0;
        this.iqw = 0;
        this.bhw = 0;
        this.iqx = 0;
        this.iqy = 0;
        this.goP = 0;
        this.iqz = 0;
        this.iqA = 0;
        this.iqB = 0;
        this.f3372ol = 0;
        this.aun = 0;
        this.iqC = 0;
        this.iqD = 0;
        this.iqE = 0;
        this.iqF = 0;
        this.iqG = 0;
        this.iqH = 0;
        this.iqI = 0;
        this.iqJ = 0;
        this.iqK = 0;
        this.iqL = 0;
        this.imN = 0;
        this.f3369kc = 0;
        this.f3370kd = 0;
        this.fPj = 0;
        this.ajY = 0;
        this.imO = 0;
        this.bhC = 0;
        this.bhD = 0;
        this.imP = 0;
        this.hhd = 0;
        this.bhz = 0;
        this.bhA = 0;
        this.imQ = 0;
        this.imR = 0;
        this.bhx = 0;
        this.bhy = 0;
        this.imS = 0;
        this.imT = 0;
        this.imU = 0;
        this.fPm = 0;
        this.imV = 0;
        this.bHy = 0;
        this.ayr = 0;
        this.imW = 0;
        this.imX = 0;
        this.avi = 0;
        this.imY = 0;
        this.imZ = 0;
        this.ina = 0;
        this.inb = 0;
        this.inc = 0;
        this.ind = 0;
        this.f3371kf = 0;
        this.ine = 0;
        this.aUH = 0;
        this.iqM = 0;
        this.bhF = 0;
        this.iqN = 0;
        this.iqO = 0;
        this.goW = 0;
        this.iqP = 0;
        this.iqQ = 0;
        this.iqR = 0;
        this.f3373oo = 0;
        this.auu = 0;
        this.iqS = 0;
        this.iqT = 0;
        this.iqU = 0;
        this.iqV = 0;
        this.iqW = 0;
        this.iqX = 0;
        this.iqY = 0;
        this.iqZ = 0;
        this.ira = 0;
        this.irb = 0;
    }
}
