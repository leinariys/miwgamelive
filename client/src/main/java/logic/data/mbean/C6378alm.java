package logic.data.mbean;

import game.script.ship.Shield;
import logic.baa.C1616Xf;
import logic.res.code.C5663aRz;
import p001a.C0064Am;

/* renamed from: a.alm  reason: case insensitive filesystem */
public class C6378alm extends C3119nz {
    @C0064Am(aul = "39591f7c70f4438e982e67c7a980b3e9", aum = 0)

    /* renamed from: PD */
    public Shield f4879PD;
    @C0064Am(aul = "39591f7c70f4438e982e67c7a980b3e9", aum = -2)

    /* renamed from: dl */
    public long f4880dl;
    @C0064Am(aul = "39591f7c70f4438e982e67c7a980b3e9", aum = -1)

    /* renamed from: ds */
    public byte f4881ds;

    public C6378alm() {
    }

    public C6378alm(C1616Xf xf) {
        super(xf);
    }

    /* renamed from: c */
    public C5663aRz[] mo309c() {
        return Shield.BaseAdapter._m_fields;
    }

    /* renamed from: d */
    public void mo310d() {
        super.mo310d();
        this.f4879PD = null;
        this.f4880dl = 0;
        this.f4881ds = 0;
    }
}
