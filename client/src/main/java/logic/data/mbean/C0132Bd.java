package logic.data.mbean;

import game.network.message.serializable.C2686iZ;
import game.script.item.ItemTypeCategory;
import game.script.item.WeaponType;
import game.script.item.buff.amplifier.WeaponAmplifierType;
import logic.baa.C1616Xf;
import logic.res.code.C5663aRz;
import p001a.C0064Am;
import p001a.C3892wO;

/* renamed from: a.Bd */
public class C0132Bd extends C2421fH {
    @C0064Am(aul = "b51de4f5fa3fae70520caa69561c3936", aum = -2)
    public long aIR;
    @C0064Am(aul = "b51de4f5fa3fae70520caa69561c3936", aum = -1)
    public byte aJc;
    @C0064Am(aul = "ce3daddbe423a09e18614de24420bc31", aum = -2)
    public long aso;
    @C0064Am(aul = "ce3daddbe423a09e18614de24420bc31", aum = -1)
    public byte ast;
    @C0064Am(aul = "76aab741fad8a5e7becb565cad646949", aum = 0)
    public C3892wO bEb;
    @C0064Am(aul = "b51de4f5fa3fae70520caa69561c3936", aum = 1)
    public C3892wO bEd;
    @C0064Am(aul = "11289e64e55cbaf87a4e24730b506272", aum = 2)
    public C3892wO bEe;
    @C0064Am(aul = "bf1ceb14f7d57ff6f24f94b7495cf82c", aum = 3)
    public C3892wO bEf;
    @C0064Am(aul = "405d29460bb158ff3967216f6c0c5e20", aum = 4)
    public C3892wO bEh;
    @C0064Am(aul = "db5737890e3f1a0d5135ab5c975d758f", aum = 5)
    public boolean bEj;
    @C0064Am(aul = "db5737890e3f1a0d5135ab5c975d758f", aum = -2)
    public long bIG;
    @C0064Am(aul = "76aab741fad8a5e7becb565cad646949", aum = -2)
    public long bIH;
    @C0064Am(aul = "11289e64e55cbaf87a4e24730b506272", aum = -2)
    public long bIJ;
    @C0064Am(aul = "db5737890e3f1a0d5135ab5c975d758f", aum = -1)
    public byte bIS;
    @C0064Am(aul = "76aab741fad8a5e7becb565cad646949", aum = -1)
    public byte bIT;
    @C0064Am(aul = "11289e64e55cbaf87a4e24730b506272", aum = -1)
    public byte bIV;
    @C0064Am(aul = "cd217918f511282ad2b9d6c32e9e0854", aum = 6)
    public C2686iZ<ItemTypeCategory> ckK;
    @C0064Am(aul = "ce3daddbe423a09e18614de24420bc31", aum = 7)
    public C2686iZ<WeaponType> ckL;
    @C0064Am(aul = "cd217918f511282ad2b9d6c32e9e0854", aum = -2)
    public long ckM;
    @C0064Am(aul = "cd217918f511282ad2b9d6c32e9e0854", aum = -1)
    public byte ckN;
    @C0064Am(aul = "bf1ceb14f7d57ff6f24f94b7495cf82c", aum = -1)

    /* renamed from: tO */
    public byte f227tO;
    @C0064Am(aul = "405d29460bb158ff3967216f6c0c5e20", aum = -1)

    /* renamed from: tS */
    public byte f228tS;
    @C0064Am(aul = "bf1ceb14f7d57ff6f24f94b7495cf82c", aum = -2)

    /* renamed from: tl */
    public long f229tl;
    @C0064Am(aul = "405d29460bb158ff3967216f6c0c5e20", aum = -2)

    /* renamed from: tp */
    public long f230tp;

    public C0132Bd() {
    }

    public C0132Bd(C1616Xf xf) {
        super(xf);
    }

    /* renamed from: c */
    public C5663aRz[] mo309c() {
        return WeaponAmplifierType._m_fields;
    }

    /* renamed from: d */
    public void mo310d() {
        super.mo310d();
        this.bEb = null;
        this.bEd = null;
        this.bEe = null;
        this.bEf = null;
        this.bEh = null;
        this.bEj = false;
        this.ckK = null;
        this.ckL = null;
        this.bIH = 0;
        this.aIR = 0;
        this.bIJ = 0;
        this.f229tl = 0;
        this.f230tp = 0;
        this.bIG = 0;
        this.ckM = 0;
        this.aso = 0;
        this.bIT = 0;
        this.aJc = 0;
        this.bIV = 0;
        this.f227tO = 0;
        this.f228tS = 0;
        this.bIS = 0;
        this.ckN = 0;
        this.ast = 0;
    }
}
