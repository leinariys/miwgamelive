package logic.data.mbean;

import game.network.message.serializable.C1556Wo;
import game.network.message.serializable.C3438ra;
import game.script.item.ItemType;
import game.script.newmarket.economy.VirtualWarehouse;
import game.script.newmarket.economy.WarehouseAgent;
import game.script.newmarket.economy.WarehouseInventoryItem;
import logic.baa.C1616Xf;
import logic.res.code.C5663aRz;
import p001a.C0064Am;

/* renamed from: a.azr  reason: case insensitive filesystem */
public class C7030azr extends C3805vD {
    @C0064Am(aul = "b081c193fd2ae1ca5d5211245bb443fb", aum = -2)
    public long dJU;
    @C0064Am(aul = "b081c193fd2ae1ca5d5211245bb443fb", aum = -1)
    public byte dKw;
    @C0064Am(aul = "b081c193fd2ae1ca5d5211245bb443fb", aum = 0)
    public C3438ra<WarehouseAgent> gYP;
    @C0064Am(aul = "6fe1828387aad1429c82b900456afae1", aum = 1)
    public C1556Wo<ItemType, WarehouseInventoryItem> gYQ;
    @C0064Am(aul = "6fe1828387aad1429c82b900456afae1", aum = -2)
    public long gYR;
    @C0064Am(aul = "6fe1828387aad1429c82b900456afae1", aum = -1)
    public byte gYS;
    @C0064Am(aul = "04362157d8c1581a06a717fadd5e7ab8", aum = 2)
    public String handle;
    @C0064Am(aul = "04362157d8c1581a06a717fadd5e7ab8", aum = -2)

    /* renamed from: ok */
    public long f5705ok;
    @C0064Am(aul = "04362157d8c1581a06a717fadd5e7ab8", aum = -1)

    /* renamed from: on */
    public byte f5706on;

    public C7030azr() {
    }

    public C7030azr(C1616Xf xf) {
        super(xf);
    }

    /* renamed from: c */
    public C5663aRz[] mo309c() {
        return VirtualWarehouse._m_fields;
    }

    /* renamed from: d */
    public void mo310d() {
        super.mo310d();
        this.gYP = null;
        this.gYQ = null;
        this.handle = null;
        this.dJU = 0;
        this.gYR = 0;
        this.f5705ok = 0;
        this.dKw = 0;
        this.gYS = 0;
        this.f5706on = 0;
    }
}
