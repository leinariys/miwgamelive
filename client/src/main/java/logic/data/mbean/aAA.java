package logic.data.mbean;

import game.script.bank.Bank;
import logic.baa.C1616Xf;
import logic.res.code.C5663aRz;
import p001a.C0064Am;

import java.util.UUID;

/* renamed from: a.aAA */
public class aAA extends C3805vD {
    @C0064Am(aul = "f41a7330b4d33e199ff47a3e419558da", aum = 2)

    /* renamed from: bK */
    public UUID f2283bK;
    @C0064Am(aul = "8bb4f85388ef2561be0572ec97b44198", aum = 0)
    public long ffg;
    @C0064Am(aul = "cd98bbc9c12e72d038ad8fe9234a3031", aum = 1)
    public long ffi;
    @C0064Am(aul = "8cad1902f426ad0c072f0b62e4491a40", aum = 3)
    public String handle;
    @C0064Am(aul = "8bb4f85388ef2561be0572ec97b44198", aum = -2)
    public long hew;
    @C0064Am(aul = "cd98bbc9c12e72d038ad8fe9234a3031", aum = -2)
    public long hex;
    @C0064Am(aul = "8bb4f85388ef2561be0572ec97b44198", aum = -1)
    public byte hey;
    @C0064Am(aul = "cd98bbc9c12e72d038ad8fe9234a3031", aum = -1)
    public byte hez;
    @C0064Am(aul = "f41a7330b4d33e199ff47a3e419558da", aum = -2)

    /* renamed from: oL */
    public long f2284oL;
    @C0064Am(aul = "f41a7330b4d33e199ff47a3e419558da", aum = -1)

    /* renamed from: oS */
    public byte f2285oS;
    @C0064Am(aul = "8cad1902f426ad0c072f0b62e4491a40", aum = -2)

    /* renamed from: ok */
    public long f2286ok;
    @C0064Am(aul = "8cad1902f426ad0c072f0b62e4491a40", aum = -1)

    /* renamed from: on */
    public byte f2287on;

    public aAA() {
    }

    public aAA(C1616Xf xf) {
        super(xf);
    }

    /* renamed from: c */
    public C5663aRz[] mo309c() {
        return Bank._m_fields;
    }

    /* renamed from: d */
    public void mo310d() {
        super.mo310d();
        this.ffg = 0;
        this.ffi = 0;
        this.f2283bK = null;
        this.handle = null;
        this.hew = 0;
        this.hex = 0;
        this.f2284oL = 0;
        this.f2286ok = 0;
        this.hey = 0;
        this.hez = 0;
        this.f2285oS = 0;
        this.f2287on = 0;
    }
}
