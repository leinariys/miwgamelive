package logic.data.mbean;

import game.script.ship.CargoHoldType;
import game.script.ship.ShipDefaults;
import game.script.ship.hazardshield.HazardShieldType;
import game.script.ship.speedBoost.SpeedBoostType;
import game.script.ship.strike.StrikeType;
import logic.baa.C1616Xf;
import logic.res.code.C5663aRz;
import p001a.C0064Am;

import java.util.UUID;

/* renamed from: a.yn */
public class C4064yn extends C3805vD {
    @C0064Am(aul = "c4249039e11d803538bbed7793a31867", aum = 4)

    /* renamed from: bK */
    public UUID f9604bK;
    @C0064Am(aul = "cf5ddaaab2633ef2d536bdf6ff626b1d", aum = 0)
    public StrikeType bKc;
    @C0064Am(aul = "a58dbe1e49779ed5aeb2672df2ebba4a", aum = 1)
    public HazardShieldType bKd;
    @C0064Am(aul = "54517cd593e908ae64e5c7792f69ce05", aum = 2)
    public SpeedBoostType bKe;
    @C0064Am(aul = "327cb8f6806de9810688affc24a49bc8", aum = 3)
    public CargoHoldType bKf;
    @C0064Am(aul = "cf5ddaaab2633ef2d536bdf6ff626b1d", aum = -2)
    public long bKg;
    @C0064Am(aul = "a58dbe1e49779ed5aeb2672df2ebba4a", aum = -2)
    public long bKh;
    @C0064Am(aul = "54517cd593e908ae64e5c7792f69ce05", aum = -2)
    public long bKi;
    @C0064Am(aul = "327cb8f6806de9810688affc24a49bc8", aum = -2)
    public long bKj;
    @C0064Am(aul = "cf5ddaaab2633ef2d536bdf6ff626b1d", aum = -1)
    public byte bKk;
    @C0064Am(aul = "a58dbe1e49779ed5aeb2672df2ebba4a", aum = -1)
    public byte bKl;
    @C0064Am(aul = "54517cd593e908ae64e5c7792f69ce05", aum = -1)
    public byte bKm;
    @C0064Am(aul = "327cb8f6806de9810688affc24a49bc8", aum = -1)
    public byte bKn;
    @C0064Am(aul = "afa653c86257c105a5f2afc419486782", aum = 5)
    public String handle;
    @C0064Am(aul = "c4249039e11d803538bbed7793a31867", aum = -2)

    /* renamed from: oL */
    public long f9605oL;
    @C0064Am(aul = "c4249039e11d803538bbed7793a31867", aum = -1)

    /* renamed from: oS */
    public byte f9606oS;
    @C0064Am(aul = "afa653c86257c105a5f2afc419486782", aum = -2)

    /* renamed from: ok */
    public long f9607ok;
    @C0064Am(aul = "afa653c86257c105a5f2afc419486782", aum = -1)

    /* renamed from: on */
    public byte f9608on;

    public C4064yn() {
    }

    public C4064yn(C1616Xf xf) {
        super(xf);
    }

    /* renamed from: c */
    public C5663aRz[] mo309c() {
        return ShipDefaults._m_fields;
    }

    /* renamed from: d */
    public void mo310d() {
        super.mo310d();
        this.bKc = null;
        this.bKd = null;
        this.bKe = null;
        this.bKf = null;
        this.f9604bK = null;
        this.handle = null;
        this.bKg = 0;
        this.bKh = 0;
        this.bKi = 0;
        this.bKj = 0;
        this.f9605oL = 0;
        this.f9607ok = 0;
        this.bKk = 0;
        this.bKl = 0;
        this.bKm = 0;
        this.bKn = 0;
        this.f9606oS = 0;
        this.f9608on = 0;
    }
}
