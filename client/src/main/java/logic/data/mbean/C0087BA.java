package logic.data.mbean;

import game.network.message.serializable.C3438ra;
import game.script.mission.NPCSpawn;
import game.script.mission.TableNPCSpawn;
import logic.baa.C1616Xf;
import logic.res.code.C5663aRz;
import p001a.C0064Am;

import java.util.UUID;

/* renamed from: a.BA */
public class C0087BA extends C3805vD {
    @C0064Am(aul = "982d9fe48a329decfdd168520aecbfb2", aum = 1)

    /* renamed from: bK */
    public UUID f121bK;
    @C0064Am(aul = "6bdafa26e69344e81c34866fc0db25f2", aum = 0)
    public C3438ra<NPCSpawn> cng;
    @C0064Am(aul = "6bdafa26e69344e81c34866fc0db25f2", aum = -2)
    public long cnh;
    @C0064Am(aul = "6bdafa26e69344e81c34866fc0db25f2", aum = -1)
    public byte cni;
    @C0064Am(aul = "81045c8d1ec4a7ee1ada3ba4981eb10e", aum = 2)
    public String handle;
    @C0064Am(aul = "982d9fe48a329decfdd168520aecbfb2", aum = -2)

    /* renamed from: oL */
    public long f122oL;
    @C0064Am(aul = "982d9fe48a329decfdd168520aecbfb2", aum = -1)

    /* renamed from: oS */
    public byte f123oS;
    @C0064Am(aul = "81045c8d1ec4a7ee1ada3ba4981eb10e", aum = -2)

    /* renamed from: ok */
    public long f124ok;
    @C0064Am(aul = "81045c8d1ec4a7ee1ada3ba4981eb10e", aum = -1)

    /* renamed from: on */
    public byte f125on;

    public C0087BA() {
    }

    public C0087BA(C1616Xf xf) {
        super(xf);
    }

    /* renamed from: c */
    public C5663aRz[] mo309c() {
        return TableNPCSpawn._m_fields;
    }

    /* renamed from: d */
    public void mo310d() {
        super.mo310d();
        this.cng = null;
        this.f121bK = null;
        this.handle = null;
        this.cnh = 0;
        this.f122oL = 0;
        this.f124ok = 0;
        this.cni = 0;
        this.f123oS = 0;
        this.f125on = 0;
    }
}
