package logic.data.mbean;

import game.script.nls.NLSItem;
import logic.baa.C1616Xf;
import logic.res.code.C5663aRz;
import p001a.C0064Am;
import taikodom.infra.script.I18NString;

/* renamed from: a.anu  reason: case insensitive filesystem */
public class C6490anu extends C2484fi {
    @C0064Am(aul = "12fae0095baed6a9a4fc3740d2675c84", aum = -2)
    public long dtb;
    @C0064Am(aul = "12fae0095baed6a9a4fc3740d2675c84", aum = -1)
    public byte dtd;
    @C0064Am(aul = "12fae0095baed6a9a4fc3740d2675c84", aum = 0)
    public I18NString ecT;
    @C0064Am(aul = "7656cc18243c8329de3ba1a7b6a1e6a2", aum = 1)
    public I18NString ecU;
    @C0064Am(aul = "7df4b967fae368333f2d5a52a619e593", aum = 2)
    public I18NString ecW;
    @C0064Am(aul = "b46558bb07c147f4692a157f035d6d3d", aum = 3)
    public I18NString ecY;
    @C0064Am(aul = "6be2383a990ef32ea69448c5175bee6f", aum = 4)
    public I18NString eda;
    @C0064Am(aul = "de8ebc05a08966b5b9ab8f36d7f7cd9e", aum = 5)
    public I18NString edc;
    @C0064Am(aul = "c7eec7a98cdb8afa1e7e2361b980b87d", aum = 6)
    public I18NString ede;
    @C0064Am(aul = "9f1dbcbf3e9b88dffa3507f94119a74d", aum = 8)
    public String edg;
    @C0064Am(aul = "a90b4c69f3b46826e4c8d0d4609b8f93", aum = 9)
    public I18NString edi;
    @C0064Am(aul = "1aa6bbd7def89a418fe4bdb27b81fdea", aum = 10)
    public I18NString edk;
    @C0064Am(aul = "802352099fec1986813296050b1d03a7", aum = 11)
    public I18NString edm;
    @C0064Am(aul = "de8ebc05a08966b5b9ab8f36d7f7cd9e", aum = -2)
    public long gdA;
    @C0064Am(aul = "c7eec7a98cdb8afa1e7e2361b980b87d", aum = -2)
    public long gdB;
    @C0064Am(aul = "9f1dbcbf3e9b88dffa3507f94119a74d", aum = -2)
    public long gdC;
    @C0064Am(aul = "a90b4c69f3b46826e4c8d0d4609b8f93", aum = -2)
    public long gdD;
    @C0064Am(aul = "1aa6bbd7def89a418fe4bdb27b81fdea", aum = -2)
    public long gdE;
    @C0064Am(aul = "802352099fec1986813296050b1d03a7", aum = -2)
    public long gdF;
    @C0064Am(aul = "7656cc18243c8329de3ba1a7b6a1e6a2", aum = -1)
    public byte gdG;
    @C0064Am(aul = "7df4b967fae368333f2d5a52a619e593", aum = -1)
    public byte gdH;
    @C0064Am(aul = "b46558bb07c147f4692a157f035d6d3d", aum = -1)
    public byte gdI;
    @C0064Am(aul = "6be2383a990ef32ea69448c5175bee6f", aum = -1)
    public byte gdJ;
    @C0064Am(aul = "de8ebc05a08966b5b9ab8f36d7f7cd9e", aum = -1)
    public byte gdK;
    @C0064Am(aul = "c7eec7a98cdb8afa1e7e2361b980b87d", aum = -1)
    public byte gdL;
    @C0064Am(aul = "9f1dbcbf3e9b88dffa3507f94119a74d", aum = -1)
    public byte gdM;
    @C0064Am(aul = "a90b4c69f3b46826e4c8d0d4609b8f93", aum = -1)
    public byte gdN;
    @C0064Am(aul = "1aa6bbd7def89a418fe4bdb27b81fdea", aum = -1)
    public byte gdO;
    @C0064Am(aul = "802352099fec1986813296050b1d03a7", aum = -1)
    public byte gdP;
    @C0064Am(aul = "7656cc18243c8329de3ba1a7b6a1e6a2", aum = -2)
    public long gdw;
    @C0064Am(aul = "7df4b967fae368333f2d5a52a619e593", aum = -2)
    public long gdx;
    @C0064Am(aul = "b46558bb07c147f4692a157f035d6d3d", aum = -2)
    public long gdy;
    @C0064Am(aul = "6be2383a990ef32ea69448c5175bee6f", aum = -2)
    public long gdz;
    @C0064Am(aul = "0d6838036e668ed3fb314052da51f1ef", aum = 7)

    /* renamed from: nh */
    public I18NString f4987nh;
    @C0064Am(aul = "0d6838036e668ed3fb314052da51f1ef", aum = -2)

    /* renamed from: nk */
    public long f4988nk;
    @C0064Am(aul = "0d6838036e668ed3fb314052da51f1ef", aum = -1)

    /* renamed from: nn */
    public byte f4989nn;

    public C6490anu() {
    }

    public C6490anu(C1616Xf xf) {
        super(xf);
    }

    /* renamed from: c */
    public C5663aRz[] mo309c() {
        return NLSItem._m_fields;
    }

    /* renamed from: d */
    public void mo310d() {
        super.mo310d();
        this.ecT = null;
        this.ecU = null;
        this.ecW = null;
        this.ecY = null;
        this.eda = null;
        this.edc = null;
        this.ede = null;
        this.f4987nh = null;
        this.edg = null;
        this.edi = null;
        this.edk = null;
        this.edm = null;
        this.dtb = 0;
        this.gdw = 0;
        this.gdx = 0;
        this.gdy = 0;
        this.gdz = 0;
        this.gdA = 0;
        this.gdB = 0;
        this.f4988nk = 0;
        this.gdC = 0;
        this.gdD = 0;
        this.gdE = 0;
        this.gdF = 0;
        this.dtd = 0;
        this.gdG = 0;
        this.gdH = 0;
        this.gdI = 0;
        this.gdJ = 0;
        this.gdK = 0;
        this.gdL = 0;
        this.f4989nn = 0;
        this.gdM = 0;
        this.gdN = 0;
        this.gdO = 0;
        this.gdP = 0;
    }
}
