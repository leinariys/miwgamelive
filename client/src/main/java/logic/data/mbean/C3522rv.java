package logic.data.mbean;

import game.script.corporation.NLSCorporation;
import logic.baa.C1616Xf;
import logic.res.code.C5663aRz;
import p001a.C0064Am;
import taikodom.infra.script.I18NString;

/* renamed from: a.rv */
public class C3522rv extends C2484fi {
    @C0064Am(aul = "198ed25c5a94d2ccb7f372a8785bda68", aum = 1)
    public I18NString baD;
    @C0064Am(aul = "6b0c7eaa20d952378e2b68dbffe9bc03", aum = 2)
    public I18NString baE;
    @C0064Am(aul = "cf2d2a4b531da6a8087e2ae3f04ab143", aum = 3)
    public I18NString baF;
    @C0064Am(aul = "198ed25c5a94d2ccb7f372a8785bda68", aum = -2)
    public long baG;
    @C0064Am(aul = "6b0c7eaa20d952378e2b68dbffe9bc03", aum = -2)
    public long baH;
    @C0064Am(aul = "cf2d2a4b531da6a8087e2ae3f04ab143", aum = -2)
    public long baI;
    @C0064Am(aul = "198ed25c5a94d2ccb7f372a8785bda68", aum = -1)
    public byte baJ;
    @C0064Am(aul = "6b0c7eaa20d952378e2b68dbffe9bc03", aum = -1)
    public byte baK;
    @C0064Am(aul = "cf2d2a4b531da6a8087e2ae3f04ab143", aum = -1)
    public byte baL;
    @C0064Am(aul = "931626bae7147295e8f476ec27f2556f", aum = 0)

    /* renamed from: ni */
    public I18NString f9078ni;
    @C0064Am(aul = "931626bae7147295e8f476ec27f2556f", aum = -2)

    /* renamed from: nl */
    public long f9079nl;
    @C0064Am(aul = "931626bae7147295e8f476ec27f2556f", aum = -1)

    /* renamed from: no */
    public byte f9080no;

    public C3522rv() {
    }

    public C3522rv(C1616Xf xf) {
        super(xf);
    }

    /* renamed from: c */
    public C5663aRz[] mo309c() {
        return NLSCorporation.LogoPanel._m_fields;
    }

    /* renamed from: d */
    public void mo310d() {
        super.mo310d();
        this.f9078ni = null;
        this.baD = null;
        this.baE = null;
        this.baF = null;
        this.f9079nl = 0;
        this.baG = 0;
        this.baH = 0;
        this.baI = 0;
        this.f9080no = 0;
        this.baJ = 0;
        this.baK = 0;
        this.baL = 0;
    }
}
