package logic.data.mbean;

import game.network.message.serializable.C2686iZ;
import game.script.avatar.AvatarSector;
import game.script.avatar.body.AvatarAppearanceType;
import game.script.avatar.body.BodyPiece;
import game.script.avatar.cloth.Cloth;
import game.script.avatar.cloth.ClothCategory;
import game.script.resource.Asset;
import logic.baa.C1616Xf;
import logic.res.code.C5663aRz;
import p001a.C0064Am;
import taikodom.infra.script.I18NString;

import java.util.UUID;

/* renamed from: a.aVZ */
public class aVZ extends C3805vD {
    @C0064Am(aul = "6d2297712022956c641a9d65702b5aaa", aum = -2)

    /* renamed from: Nf */
    public long f3920Nf;
    @C0064Am(aul = "0c6887e0d8f826324f9c64ddddbbf6ab", aum = -2)

    /* renamed from: Ng */
    public long f3921Ng;
    @C0064Am(aul = "6d2297712022956c641a9d65702b5aaa", aum = -1)

    /* renamed from: Nh */
    public byte f3922Nh;
    @C0064Am(aul = "0c6887e0d8f826324f9c64ddddbbf6ab", aum = -1)

    /* renamed from: Ni */
    public byte f3923Ni;
    @C0064Am(aul = "b879629d78b19e04bac558c5fc3674c1", aum = 15)

    /* renamed from: bK */
    public UUID f3924bK;
    @C0064Am(aul = "728cdf7edfb33bcac271ae2aa7dcbffa", aum = -2)
    public long bZA;
    @C0064Am(aul = "728cdf7edfb33bcac271ae2aa7dcbffa", aum = -1)
    public byte bZB;
    @C0064Am(aul = "e0b4bee99fbf7f9f154fb299102a1018", aum = -2)
    public long bwn;
    @C0064Am(aul = "e0b4bee99fbf7f9f154fb299102a1018", aum = -1)
    public byte bwt;
    @C0064Am(aul = "0def779b891abdc3d7f8fd4867c455ee", aum = -2)
    public long cAj;
    @C0064Am(aul = "0def779b891abdc3d7f8fd4867c455ee", aum = -1)
    public byte cAl;
    @C0064Am(aul = "0d38d90a8624474ef6c161237e482bf4", aum = -2)
    public long eob;
    @C0064Am(aul = "0d38d90a8624474ef6c161237e482bf4", aum = -1)
    public byte eoi;
    @C0064Am(aul = "f0eadea20242a60dff1dc618257b7d13", aum = 7)
    public C2686iZ<BodyPiece> eyA;
    @C0064Am(aul = "b50decd8f8501ed93375808e10e72a78", aum = 8)
    public C2686iZ<AvatarSector> eyC;
    @C0064Am(aul = "4a1a423f8dda865de494757b9d203396", aum = 10)
    public Asset eyE;
    @C0064Am(aul = "83a0ffc53895b1192314647e7537be5a", aum = 11)
    public long eyG;
    @C0064Am(aul = "5c2407469bcde71ad9126a3092135262", aum = 12)
    public long eyI;
    @C0064Am(aul = "6f1ad25bf470496c8487ff319755c8e9", aum = 13)
    public boolean eyK;
    @C0064Am(aul = "db6688104aa265f1f110b4b6d2ef56f9", aum = 4)
    public String eyv;
    @C0064Am(aul = "e0b4bee99fbf7f9f154fb299102a1018", aum = 5)
    public ClothCategory eyx;
    @C0064Am(aul = "b845cbb8e21f53585ba403fb0bc4a25c", aum = 6)
    public C2686iZ<AvatarAppearanceType> eyy;
    @C0064Am(aul = "eac100e0d005443a694979dd7da02238", aum = -1)

    /* renamed from: jK */
    public byte f3925jK;
    @C0064Am(aul = "4a1a423f8dda865de494757b9d203396", aum = -1)
    public byte jdA;
    @C0064Am(aul = "83a0ffc53895b1192314647e7537be5a", aum = -1)
    public byte jdB;
    @C0064Am(aul = "5c2407469bcde71ad9126a3092135262", aum = -1)
    public byte jdC;
    @C0064Am(aul = "6f1ad25bf470496c8487ff319755c8e9", aum = -1)
    public byte jdD;
    @C0064Am(aul = "db6688104aa265f1f110b4b6d2ef56f9", aum = -2)
    public long jdo;
    @C0064Am(aul = "b845cbb8e21f53585ba403fb0bc4a25c", aum = -2)
    public long jdp;
    @C0064Am(aul = "f0eadea20242a60dff1dc618257b7d13", aum = -2)
    public long jdq;
    @C0064Am(aul = "b50decd8f8501ed93375808e10e72a78", aum = -2)
    public long jdr;
    @C0064Am(aul = "4a1a423f8dda865de494757b9d203396", aum = -2)
    public long jds;
    @C0064Am(aul = "83a0ffc53895b1192314647e7537be5a", aum = -2)
    public long jdt;
    @C0064Am(aul = "5c2407469bcde71ad9126a3092135262", aum = -2)
    public long jdu;
    @C0064Am(aul = "6f1ad25bf470496c8487ff319755c8e9", aum = -2)
    public long jdv;
    @C0064Am(aul = "db6688104aa265f1f110b4b6d2ef56f9", aum = -1)
    public byte jdw;
    @C0064Am(aul = "b845cbb8e21f53585ba403fb0bc4a25c", aum = -1)
    public byte jdx;
    @C0064Am(aul = "f0eadea20242a60dff1dc618257b7d13", aum = -1)
    public byte jdy;
    @C0064Am(aul = "b50decd8f8501ed93375808e10e72a78", aum = -1)
    public byte jdz;
    @C0064Am(aul = "eac100e0d005443a694979dd7da02238", aum = 9)

    /* renamed from: jn */
    public Asset f3926jn;
    @C0064Am(aul = "eac100e0d005443a694979dd7da02238", aum = -2)

    /* renamed from: jy */
    public long f3927jy;
    @C0064Am(aul = "b879629d78b19e04bac558c5fc3674c1", aum = -2)

    /* renamed from: oL */
    public long f3928oL;
    @C0064Am(aul = "b879629d78b19e04bac558c5fc3674c1", aum = -1)

    /* renamed from: oS */
    public byte f3929oS;
    @C0064Am(aul = "0d38d90a8624474ef6c161237e482bf4", aum = 3)
    public String prefix;
    @C0064Am(aul = "0def779b891abdc3d7f8fd4867c455ee", aum = 2)
    public int priority;
    @C0064Am(aul = "0c6887e0d8f826324f9c64ddddbbf6ab", aum = 1)
    public boolean restricted;
    @C0064Am(aul = "6d2297712022956c641a9d65702b5aaa", aum = 0)

    /* renamed from: zP */
    public I18NString f3930zP;
    @C0064Am(aul = "728cdf7edfb33bcac271ae2aa7dcbffa", aum = 14)

    /* renamed from: zR */
    public String f3931zR;

    public aVZ() {
    }

    public aVZ(C1616Xf xf) {
        super(xf);
    }

    /* renamed from: c */
    public C5663aRz[] mo309c() {
        return Cloth._m_fields;
    }

    /* renamed from: d */
    public void mo310d() {
        super.mo310d();
        this.f3930zP = null;
        this.restricted = false;
        this.priority = 0;
        this.prefix = null;
        this.eyv = null;
        this.eyx = null;
        this.eyy = null;
        this.eyA = null;
        this.eyC = null;
        this.f3926jn = null;
        this.eyE = null;
        this.eyG = 0;
        this.eyI = 0;
        this.eyK = false;
        this.f3931zR = null;
        this.f3924bK = null;
        this.f3920Nf = 0;
        this.f3921Ng = 0;
        this.cAj = 0;
        this.eob = 0;
        this.jdo = 0;
        this.bwn = 0;
        this.jdp = 0;
        this.jdq = 0;
        this.jdr = 0;
        this.f3927jy = 0;
        this.jds = 0;
        this.jdt = 0;
        this.jdu = 0;
        this.jdv = 0;
        this.bZA = 0;
        this.f3928oL = 0;
        this.f3922Nh = 0;
        this.f3923Ni = 0;
        this.cAl = 0;
        this.eoi = 0;
        this.jdw = 0;
        this.bwt = 0;
        this.jdx = 0;
        this.jdy = 0;
        this.jdz = 0;
        this.f3925jK = 0;
        this.jdA = 0;
        this.jdB = 0;
        this.jdC = 0;
        this.jdD = 0;
        this.bZB = 0;
        this.f3929oS = 0;
    }
}
