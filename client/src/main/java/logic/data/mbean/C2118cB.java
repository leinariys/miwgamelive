package logic.data.mbean;

import game.script.missiontemplate.WaypointDat;
import game.script.missiontemplate.scripting.AssaultMissionWave;
import logic.baa.C1616Xf;
import logic.res.code.C5663aRz;
import p001a.C0064Am;

import java.util.UUID;

/* renamed from: a.cB */
public class C2118cB extends C3805vD {
    @C0064Am(aul = "df24ba68526a248ce0f013824d4e04de", aum = 3)

    /* renamed from: bK */
    public UUID f5984bK;
    @C0064Am(aul = "d40fc21d2dfba45745a51f5a6f4349a6", aum = 4)
    public String handle;
    @C0064Am(aul = "df24ba68526a248ce0f013824d4e04de", aum = -2)

    /* renamed from: oL */
    public long f5985oL;
    @C0064Am(aul = "df24ba68526a248ce0f013824d4e04de", aum = -1)

    /* renamed from: oS */
    public byte f5986oS;
    @C0064Am(aul = "d40fc21d2dfba45745a51f5a6f4349a6", aum = -2)

    /* renamed from: ok */
    public long f5987ok;
    @C0064Am(aul = "d40fc21d2dfba45745a51f5a6f4349a6", aum = -1)

    /* renamed from: on */
    public byte f5988on;
    @C0064Am(aul = "53aa18add6286cb7c306440b7d97ca09", aum = -1)

    /* renamed from: vA */
    public byte f5989vA;
    @C0064Am(aul = "be08965ed8b3ecf3388fc4e1c95a5136", aum = -1)

    /* renamed from: vB */
    public byte f5990vB;
    @C0064Am(aul = "d22b1997bafc0a804632b95d5b3108c2", aum = -1)

    /* renamed from: vC */
    public byte f5991vC;
    @C0064Am(aul = "53aa18add6286cb7c306440b7d97ca09", aum = 0)

    /* renamed from: vu */
    public WaypointDat f5992vu;
    @C0064Am(aul = "be08965ed8b3ecf3388fc4e1c95a5136", aum = 1)

    /* renamed from: vv */
    public AssaultMissionWave.C2007a f5993vv;
    @C0064Am(aul = "d22b1997bafc0a804632b95d5b3108c2", aum = 2)

    /* renamed from: vw */
    public long f5994vw;
    @C0064Am(aul = "53aa18add6286cb7c306440b7d97ca09", aum = -2)

    /* renamed from: vx */
    public long f5995vx;
    @C0064Am(aul = "be08965ed8b3ecf3388fc4e1c95a5136", aum = -2)

    /* renamed from: vy */
    public long f5996vy;
    @C0064Am(aul = "d22b1997bafc0a804632b95d5b3108c2", aum = -2)

    /* renamed from: vz */
    public long f5997vz;

    public C2118cB() {
    }

    public C2118cB(C1616Xf xf) {
        super(xf);
    }

    /* renamed from: c */
    public C5663aRz[] mo309c() {
        return AssaultMissionWave._m_fields;
    }

    /* renamed from: d */
    public void mo310d() {
        super.mo310d();
        this.f5992vu = null;
        this.f5993vv = null;
        this.f5994vw = 0;
        this.f5984bK = null;
        this.handle = null;
        this.f5995vx = 0;
        this.f5996vy = 0;
        this.f5997vz = 0;
        this.f5985oL = 0;
        this.f5987ok = 0;
        this.f5989vA = 0;
        this.f5990vB = 0;
        this.f5991vC = 0;
        this.f5986oS = 0;
        this.f5988on = 0;
    }
}
