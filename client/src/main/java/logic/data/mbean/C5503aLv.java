package logic.data.mbean;

import game.network.message.serializable.C1556Wo;
import game.network.message.serializable.C3438ra;
import game.script.ship.*;
import logic.baa.C1616Xf;
import logic.res.code.C5663aRz;
import p001a.C0064Am;
import p001a.C0437GE;
import p001a.C2611hZ;
import p001a.aHK;
import taikodom.infra.script.I18NString;

/* renamed from: a.aLv  reason: case insensitive filesystem */
public class C5503aLv extends C6407amP {
    @C0064Am(aul = "6085a82a432e3e355104d2291a8f76bd", aum = -2)
    public long bwn;
    @C0064Am(aul = "6085a82a432e3e355104d2291a8f76bd", aum = -1)
    public byte bwt;
    @C0064Am(aul = "d08f040f2d41d02bb82429d8bde90d1a", aum = 1)
    public int daP;
    @C0064Am(aul = "c586d70210fb7ba5d6d5187d6a19abab", aum = 2)
    public C3438ra<OutpostActivationItem> daQ;
    @C0064Am(aul = "6085a82a432e3e355104d2291a8f76bd", aum = 3)
    public aHK daR;
    @C0064Am(aul = "a3742f803b65fbd65f5cd4621edd2f84", aum = 4)
    public I18NString daS;
    @C0064Am(aul = "3484167a43dc3d853d0bac73493a0f88", aum = 5)
    public C1556Wo<C2611hZ, OutpostUpgrade> daT;
    @C0064Am(aul = "6beff6a45af17e43c2a2a3c419c4d171", aum = 6)
    public C1556Wo<C2611hZ, OutpostLevelInfo> daU;
    @C0064Am(aul = "08db4f84a0e5e5e2ea70a0e425424813", aum = 7)
    public C1556Wo<C0437GE, OutpostLevelFeature> daV;
    @C0064Am(aul = "0629af4cf51a6169fc3bd7726514acbc", aum = 0)

    /* renamed from: db */
    public long f3335db;
    @C0064Am(aul = "d08f040f2d41d02bb82429d8bde90d1a", aum = -2)
    public long dbb;
    @C0064Am(aul = "c586d70210fb7ba5d6d5187d6a19abab", aum = -2)
    public long dbc;
    @C0064Am(aul = "a3742f803b65fbd65f5cd4621edd2f84", aum = -2)
    public long dbd;
    @C0064Am(aul = "3484167a43dc3d853d0bac73493a0f88", aum = -2)
    public long dbe;
    @C0064Am(aul = "6beff6a45af17e43c2a2a3c419c4d171", aum = -2)
    public long dbf;
    @C0064Am(aul = "08db4f84a0e5e5e2ea70a0e425424813", aum = -2)
    public long dbg;
    @C0064Am(aul = "d08f040f2d41d02bb82429d8bde90d1a", aum = -1)
    public byte dbk;
    @C0064Am(aul = "c586d70210fb7ba5d6d5187d6a19abab", aum = -1)
    public byte dbl;
    @C0064Am(aul = "a3742f803b65fbd65f5cd4621edd2f84", aum = -1)
    public byte dbm;
    @C0064Am(aul = "3484167a43dc3d853d0bac73493a0f88", aum = -1)
    public byte dbn;
    @C0064Am(aul = "6beff6a45af17e43c2a2a3c419c4d171", aum = -1)
    public byte dbo;
    @C0064Am(aul = "08db4f84a0e5e5e2ea70a0e425424813", aum = -1)
    public byte dbp;
    @C0064Am(aul = "0629af4cf51a6169fc3bd7726514acbc", aum = -2)

    /* renamed from: dj */
    public long f3336dj;
    @C0064Am(aul = "0629af4cf51a6169fc3bd7726514acbc", aum = -1)

    /* renamed from: dq */
    public byte f3337dq;

    public C5503aLv() {
    }

    public C5503aLv(C1616Xf xf) {
        super(xf);
    }

    /* renamed from: c */
    public C5663aRz[] mo309c() {
        return OutpostType._m_fields;
    }

    /* renamed from: d */
    public void mo310d() {
        super.mo310d();
        this.f3335db = 0;
        this.daP = 0;
        this.daQ = null;
        this.daR = null;
        this.daS = null;
        this.daT = null;
        this.daU = null;
        this.daV = null;
        this.f3336dj = 0;
        this.dbb = 0;
        this.dbc = 0;
        this.bwn = 0;
        this.dbd = 0;
        this.dbe = 0;
        this.dbf = 0;
        this.dbg = 0;
        this.f3337dq = 0;
        this.dbk = 0;
        this.dbl = 0;
        this.bwt = 0;
        this.dbm = 0;
        this.dbn = 0;
        this.dbo = 0;
        this.dbp = 0;
    }
}
