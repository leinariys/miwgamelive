package logic.data.mbean;

import game.network.message.serializable.C2686iZ;
import game.network.message.serializable.C3438ra;
import game.script.connect.UsersQueueManager;
import game.script.login.UserConnection;
import game.script.player.User;
import logic.baa.C1616Xf;
import logic.res.code.C5663aRz;
import p001a.C0064Am;

/* renamed from: a.ayg  reason: case insensitive filesystem */
public class C6993ayg extends C3805vD {
    @C0064Am(aul = "5cc5e08fbcf64f3ec0ac68856e54b524", aum = 0)
    public int eVW;
    @C0064Am(aul = "2629e4d4e947082c3b29409b05cc6cf5", aum = 1)
    public boolean eVY;
    @C0064Am(aul = "94dae7f617049662558c788905ae671d", aum = 2)
    public long eWa;
    @C0064Am(aul = "691a048981fb66c94f8e6696539ac314", aum = 3)
    public C3438ra<User> eWc;
    @C0064Am(aul = "e9f397e2a23b1c715cf8c781b514d2d6", aum = 4)
    public C2686iZ<User> eWe;
    @C0064Am(aul = "0efee0083364cad8535b0c6f7292d899", aum = 5)
    public C2686iZ<UserConnection> eWg;
    @C0064Am(aul = "5cc5e08fbcf64f3ec0ac68856e54b524", aum = -1)
    public byte gSA;
    @C0064Am(aul = "2629e4d4e947082c3b29409b05cc6cf5", aum = -1)
    public byte gSB;
    @C0064Am(aul = "94dae7f617049662558c788905ae671d", aum = -1)
    public byte gSC;
    @C0064Am(aul = "691a048981fb66c94f8e6696539ac314", aum = -1)
    public byte gSD;
    @C0064Am(aul = "e9f397e2a23b1c715cf8c781b514d2d6", aum = -1)
    public byte gSE;
    @C0064Am(aul = "0efee0083364cad8535b0c6f7292d899", aum = -1)
    public byte gSF;
    @C0064Am(aul = "5cc5e08fbcf64f3ec0ac68856e54b524", aum = -2)
    public long gSu;
    @C0064Am(aul = "2629e4d4e947082c3b29409b05cc6cf5", aum = -2)
    public long gSv;
    @C0064Am(aul = "94dae7f617049662558c788905ae671d", aum = -2)
    public long gSw;
    @C0064Am(aul = "691a048981fb66c94f8e6696539ac314", aum = -2)
    public long gSx;
    @C0064Am(aul = "e9f397e2a23b1c715cf8c781b514d2d6", aum = -2)
    public long gSy;
    @C0064Am(aul = "0efee0083364cad8535b0c6f7292d899", aum = -2)
    public long gSz;

    public C6993ayg() {
    }

    public C6993ayg(C1616Xf xf) {
        super(xf);
    }

    /* renamed from: c */
    public C5663aRz[] mo309c() {
        return UsersQueueManager._m_fields;
    }

    /* renamed from: d */
    public void mo310d() {
        super.mo310d();
        this.eVW = 0;
        this.eVY = false;
        this.eWa = 0;
        this.eWc = null;
        this.eWe = null;
        this.eWg = null;
        this.gSu = 0;
        this.gSv = 0;
        this.gSw = 0;
        this.gSx = 0;
        this.gSy = 0;
        this.gSz = 0;
        this.gSA = 0;
        this.gSB = 0;
        this.gSC = 0;
        this.gSD = 0;
        this.gSE = 0;
        this.gSF = 0;
    }
}
