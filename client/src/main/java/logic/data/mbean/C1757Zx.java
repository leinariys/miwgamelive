package logic.data.mbean;

import game.script.ship.categories.Explorer;
import logic.baa.C1616Xf;
import logic.res.code.C5663aRz;
import p001a.C0064Am;

/* renamed from: a.Zx */
public class C1757Zx extends C6557apJ {
    @C0064Am(aul = "0736857e37d38a43a9cd617d00fe71b7", aum = 0)

    /* renamed from: OD */
    public Explorer f2255OD;
    @C0064Am(aul = "0736857e37d38a43a9cd617d00fe71b7", aum = -2)

    /* renamed from: dl */
    public long f2256dl;
    @C0064Am(aul = "0736857e37d38a43a9cd617d00fe71b7", aum = -1)

    /* renamed from: ds */
    public byte f2257ds;

    public C1757Zx() {
    }

    public C1757Zx(C1616Xf xf) {
        super(xf);
    }

    /* renamed from: c */
    public C5663aRz[] mo309c() {
        return Explorer.RadarUpgrade._m_fields;
    }

    /* renamed from: d */
    public void mo310d() {
        super.mo310d();
        this.f2255OD = null;
        this.f2256dl = 0;
        this.f2257ds = 0;
    }
}
