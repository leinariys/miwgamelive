package logic.data.mbean;

import game.geometry.Vec3d;
import game.network.message.serializable.C3438ra;
import game.script.mission.scripting.FollowMissionScript;
import game.script.npc.NPC;
import logic.baa.C1616Xf;
import logic.res.code.C5663aRz;
import p001a.C0064Am;

import java.util.HashMap;

/* renamed from: a.cK */
public class C2128cK extends aMY {
    @C0064Am(aul = "e010b3dccd834ba842e64f8be594c12a", aum = 1)
    public int counter;
    @C0064Am(aul = "2721a10e5e379849962f7e4108faff20", aum = 0)

    /* renamed from: vU */
    public HashMap<String, NPC> f6030vU;
    @C0064Am(aul = "9f4eee3fdbb5f580235ca1cb80d1e4aa", aum = 2)

    /* renamed from: vV */
    public int f6031vV;
    @C0064Am(aul = "ba674815d2581c8f81fd4bd08c55f321", aum = 3)

    /* renamed from: vW */
    public Vec3d f6032vW;
    @C0064Am(aul = "2b6660085e592feab0d412f790c8925c", aum = 4)

    /* renamed from: vX */
    public Vec3d f6033vX;
    @C0064Am(aul = "414b07f2b1e9b5e73c1c007d08953ced", aum = 5)

    /* renamed from: vY */
    public Vec3d f6034vY;
    @C0064Am(aul = "5a2ae6b688c1b726f688b0c233787bb3", aum = 6)

    /* renamed from: vZ */
    public int f6035vZ;
    @C0064Am(aul = "6bb31428ceaae180851d889e882d9035", aum = 7)

    /* renamed from: wa */
    public C3438ra<NPC> f6036wa;
    @C0064Am(aul = "2721a10e5e379849962f7e4108faff20", aum = -2)

    /* renamed from: wb */
    public long f6037wb;
    @C0064Am(aul = "e010b3dccd834ba842e64f8be594c12a", aum = -2)

    /* renamed from: wc */
    public long f6038wc;
    @C0064Am(aul = "9f4eee3fdbb5f580235ca1cb80d1e4aa", aum = -2)

    /* renamed from: wd */
    public long f6039wd;
    @C0064Am(aul = "ba674815d2581c8f81fd4bd08c55f321", aum = -2)

    /* renamed from: we */
    public long f6040we;
    @C0064Am(aul = "2b6660085e592feab0d412f790c8925c", aum = -2)

    /* renamed from: wf */
    public long f6041wf;
    @C0064Am(aul = "414b07f2b1e9b5e73c1c007d08953ced", aum = -2)

    /* renamed from: wg */
    public long f6042wg;
    @C0064Am(aul = "5a2ae6b688c1b726f688b0c233787bb3", aum = -2)

    /* renamed from: wh */
    public long f6043wh;
    @C0064Am(aul = "6bb31428ceaae180851d889e882d9035", aum = -2)

    /* renamed from: wi */
    public long f6044wi;
    @C0064Am(aul = "2721a10e5e379849962f7e4108faff20", aum = -1)

    /* renamed from: wj */
    public byte f6045wj;
    @C0064Am(aul = "e010b3dccd834ba842e64f8be594c12a", aum = -1)

    /* renamed from: wk */
    public byte f6046wk;
    @C0064Am(aul = "9f4eee3fdbb5f580235ca1cb80d1e4aa", aum = -1)

    /* renamed from: wl */
    public byte f6047wl;
    @C0064Am(aul = "ba674815d2581c8f81fd4bd08c55f321", aum = -1)

    /* renamed from: wm */
    public byte f6048wm;
    @C0064Am(aul = "2b6660085e592feab0d412f790c8925c", aum = -1)

    /* renamed from: wn */
    public byte f6049wn;
    @C0064Am(aul = "414b07f2b1e9b5e73c1c007d08953ced", aum = -1)

    /* renamed from: wo */
    public byte f6050wo;
    @C0064Am(aul = "5a2ae6b688c1b726f688b0c233787bb3", aum = -1)

    /* renamed from: wp */
    public byte f6051wp;
    @C0064Am(aul = "6bb31428ceaae180851d889e882d9035", aum = -1)

    /* renamed from: wq */
    public byte f6052wq;

    public C2128cK() {
    }

    public C2128cK(C1616Xf xf) {
        super(xf);
    }

    /* renamed from: c */
    public C5663aRz[] mo309c() {
        return FollowMissionScript._m_fields;
    }

    /* renamed from: d */
    public void mo310d() {
        super.mo310d();
        this.f6030vU = null;
        this.counter = 0;
        this.f6031vV = 0;
        this.f6032vW = null;
        this.f6033vX = null;
        this.f6034vY = null;
        this.f6035vZ = 0;
        this.f6036wa = null;
        this.f6037wb = 0;
        this.f6038wc = 0;
        this.f6039wd = 0;
        this.f6040we = 0;
        this.f6041wf = 0;
        this.f6042wg = 0;
        this.f6043wh = 0;
        this.f6044wi = 0;
        this.f6045wj = 0;
        this.f6046wk = 0;
        this.f6047wl = 0;
        this.f6048wm = 0;
        this.f6049wn = 0;
        this.f6050wo = 0;
        this.f6051wp = 0;
        this.f6052wq = 0;
    }
}
