package logic.data.mbean;

import game.network.message.serializable.C3438ra;
import game.script.spacezone.population.BossNestPopulationControlType;
import game.script.spacezone.population.NPCSpawn;
import game.script.spacezone.population.StaticNPCSpawn;
import logic.baa.C1616Xf;
import logic.res.code.C5663aRz;
import p001a.C0064Am;

/* renamed from: a.ajT  reason: case insensitive filesystem */
public class C6255ajT extends aIG {
    @C0064Am(aul = "b6cc6b93a8cd27503cc59f362c138497", aum = -2)
    public long ftD;
    @C0064Am(aul = "35ec43e51e740ff648917127c5f13059", aum = -1)
    public byte ftM;
    @C0064Am(aul = "b6cc6b93a8cd27503cc59f362c138497", aum = -1)
    public byte ftQ;
    @C0064Am(aul = "35ec43e51e740ff648917127c5f13059", aum = 0)
    public C3438ra<NPCSpawn> ftm;
    @C0064Am(aul = "b6cc6b93a8cd27503cc59f362c138497", aum = 1)
    public C3438ra<StaticNPCSpawn> ftq;
    @C0064Am(aul = "35ec43e51e740ff648917127c5f13059", aum = -2)
    public long ftz;

    public C6255ajT() {
    }

    public C6255ajT(C1616Xf xf) {
        super(xf);
    }

    /* renamed from: c */
    public C5663aRz[] mo309c() {
        return BossNestPopulationControlType._m_fields;
    }

    /* renamed from: d */
    public void mo310d() {
        super.mo310d();
        this.ftm = null;
        this.ftq = null;
        this.ftz = 0;
        this.ftD = 0;
        this.ftM = 0;
        this.ftQ = 0;
    }
}
