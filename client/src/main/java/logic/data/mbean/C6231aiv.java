package logic.data.mbean;

import game.script.ship.GlobalPhysicsTweaks;
import logic.baa.C1616Xf;
import logic.res.code.C5663aRz;
import p001a.C0064Am;

import java.util.UUID;

/* renamed from: a.aiv  reason: case insensitive filesystem */
public class C6231aiv extends C3805vD {
    @C0064Am(aul = "c33b28cb6d8d939b74b5812fd34cf254", aum = 5)

    /* renamed from: bK */
    public UUID f4671bK;
    @C0064Am(aul = "f8200fe7fd723fd8880427e457bf1949", aum = 0)
    public float bNk;
    @C0064Am(aul = "7dc90cf4765c48184f60f6ca921798b5", aum = 1)
    public float bNm;
    @C0064Am(aul = "86069e5214c4dbe2aa3a0e7939f11ff1", aum = 2)
    public float bNo;
    @C0064Am(aul = "9cc22c2adda756250b93a3b750b1ef5f", aum = 3)
    public float bNq;
    @C0064Am(aul = "7a7ea9643cc90afb2ac05cc30db83b3b", aum = 4)
    public float bNs;
    @C0064Am(aul = "7a7ea9643cc90afb2ac05cc30db83b3b", aum = -1)
    public byte fNA;
    @C0064Am(aul = "f8200fe7fd723fd8880427e457bf1949", aum = -2)
    public long fNr;
    @C0064Am(aul = "7dc90cf4765c48184f60f6ca921798b5", aum = -2)
    public long fNs;
    @C0064Am(aul = "86069e5214c4dbe2aa3a0e7939f11ff1", aum = -2)
    public long fNt;
    @C0064Am(aul = "9cc22c2adda756250b93a3b750b1ef5f", aum = -2)
    public long fNu;
    @C0064Am(aul = "7a7ea9643cc90afb2ac05cc30db83b3b", aum = -2)
    public long fNv;
    @C0064Am(aul = "f8200fe7fd723fd8880427e457bf1949", aum = -1)
    public byte fNw;
    @C0064Am(aul = "7dc90cf4765c48184f60f6ca921798b5", aum = -1)
    public byte fNx;
    @C0064Am(aul = "86069e5214c4dbe2aa3a0e7939f11ff1", aum = -1)
    public byte fNy;
    @C0064Am(aul = "9cc22c2adda756250b93a3b750b1ef5f", aum = -1)
    public byte fNz;
    @C0064Am(aul = "c33b28cb6d8d939b74b5812fd34cf254", aum = -2)

    /* renamed from: oL */
    public long f4672oL;
    @C0064Am(aul = "c33b28cb6d8d939b74b5812fd34cf254", aum = -1)

    /* renamed from: oS */
    public byte f4673oS;

    public C6231aiv() {
    }

    public C6231aiv(C1616Xf xf) {
        super(xf);
    }

    /* renamed from: c */
    public C5663aRz[] mo309c() {
        return GlobalPhysicsTweaks._m_fields;
    }

    /* renamed from: d */
    public void mo310d() {
        super.mo310d();
        this.bNk = 0.0f;
        this.bNm = 0.0f;
        this.bNo = 0.0f;
        this.bNq = 0.0f;
        this.bNs = 0.0f;
        this.f4671bK = null;
        this.fNr = 0;
        this.fNs = 0;
        this.fNt = 0;
        this.fNu = 0;
        this.fNv = 0;
        this.f4672oL = 0;
        this.fNw = 0;
        this.fNx = 0;
        this.fNy = 0;
        this.fNz = 0;
        this.fNA = 0;
        this.f4673oS = 0;
    }
}
