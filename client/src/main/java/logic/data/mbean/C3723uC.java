package logic.data.mbean;

import game.script.item.ItemType;
import logic.baa.C1616Xf;
import logic.res.code.C5663aRz;
import p001a.C0064Am;

import java.util.UUID;

/* renamed from: a.uC */
public class C3723uC extends C5292aDs {
    @C0064Am(aul = "1b2c59af0d4bb98655f23f9c00269ad7", aum = -2)
    public long aLG;
    @C0064Am(aul = "1b2c59af0d4bb98655f23f9c00269ad7", aum = -1)
    public byte aLL;
    @C0064Am(aul = "3638137f8188575a1bb821c9688f61ae", aum = 2)

    /* renamed from: bK */
    public UUID f9310bK;
    @C0064Am(aul = "79abd2644160e05ac0e2777d5da48484", aum = -2)
    public long bmV;
    @C0064Am(aul = "79abd2644160e05ac0e2777d5da48484", aum = -1)
    public byte bmW;
    @C0064Am(aul = "79abd2644160e05ac0e2777d5da48484", aum = 0)

    /* renamed from: cw */
    public ItemType f9311cw;
    @C0064Am(aul = "1b2c59af0d4bb98655f23f9c00269ad7", aum = 1)

    /* renamed from: cy */
    public int f9312cy;
    @C0064Am(aul = "e20c09b69a12e508a2ffe31484e7db9d", aum = 3)
    public String handle;
    @C0064Am(aul = "3638137f8188575a1bb821c9688f61ae", aum = -2)

    /* renamed from: oL */
    public long f9313oL;
    @C0064Am(aul = "3638137f8188575a1bb821c9688f61ae", aum = -1)

    /* renamed from: oS */
    public byte f9314oS;
    @C0064Am(aul = "e20c09b69a12e508a2ffe31484e7db9d", aum = -2)

    /* renamed from: ok */
    public long f9315ok;
    @C0064Am(aul = "e20c09b69a12e508a2ffe31484e7db9d", aum = -1)

    /* renamed from: on */
    public byte f9316on;

    public C3723uC() {
    }

    public C3723uC(C1616Xf xf) {
        super(xf);
    }

    /* renamed from: c */
    public C5663aRz[] mo309c() {
        return ItemTypeTableItem._m_fields;
    }

    /* renamed from: d */
    public void mo310d() {
        super.mo310d();
        this.f9311cw = null;
        this.f9312cy = 0;
        this.f9310bK = null;
        this.handle = null;
        this.bmV = 0;
        this.aLG = 0;
        this.f9313oL = 0;
        this.f9315ok = 0;
        this.bmW = 0;
        this.aLL = 0;
        this.f9314oS = 0;
        this.f9316on = 0;
    }
}
