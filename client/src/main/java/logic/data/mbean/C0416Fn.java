package logic.data.mbean;

import game.network.message.serializable.C3438ra;
import game.script.resource.Asset;
import game.script.resource.AssetGroup;
import logic.baa.C1616Xf;
import logic.res.code.C5663aRz;
import p001a.C0064Am;

import java.util.UUID;

/* renamed from: a.Fn */
public class C0416Fn extends C3805vD {
    @C0064Am(aul = "1c0c1c9ded84e8399fc26482c176337b", aum = -2)

    /* renamed from: Nf */
    public long f584Nf;
    @C0064Am(aul = "1c0c1c9ded84e8399fc26482c176337b", aum = -1)

    /* renamed from: Nh */
    public byte f585Nh;
    @C0064Am(aul = "7f2a3be91958ccd25c78ac2750b67035", aum = 0)

    /* renamed from: bK */
    public UUID f586bK;
    @C0064Am(aul = "520da7ec80080d3a3e4d53f3db473021", aum = 3)
    public C3438ra<AssetGroup> cVk;
    @C0064Am(aul = "961a3a87323548e06014478127a5b3b8", aum = 4)
    public C3438ra<Asset> cVl;
    @C0064Am(aul = "520da7ec80080d3a3e4d53f3db473021", aum = -2)
    public long cVm;
    @C0064Am(aul = "961a3a87323548e06014478127a5b3b8", aum = -2)
    public long cVn;
    @C0064Am(aul = "520da7ec80080d3a3e4d53f3db473021", aum = -1)
    public byte cVo;
    @C0064Am(aul = "961a3a87323548e06014478127a5b3b8", aum = -1)
    public byte cVp;
    @C0064Am(aul = "a58137ac894ed105dfb6a8a1d8cfa333", aum = 1)
    public String handle;
    @C0064Am(aul = "1c0c1c9ded84e8399fc26482c176337b", aum = 2)
    public String name;
    @C0064Am(aul = "7f2a3be91958ccd25c78ac2750b67035", aum = -2)

    /* renamed from: oL */
    public long f587oL;
    @C0064Am(aul = "7f2a3be91958ccd25c78ac2750b67035", aum = -1)

    /* renamed from: oS */
    public byte f588oS;
    @C0064Am(aul = "a58137ac894ed105dfb6a8a1d8cfa333", aum = -2)

    /* renamed from: ok */
    public long f589ok;
    @C0064Am(aul = "a58137ac894ed105dfb6a8a1d8cfa333", aum = -1)

    /* renamed from: on */
    public byte f590on;

    public C0416Fn() {
    }

    public C0416Fn(C1616Xf xf) {
        super(xf);
    }

    /* renamed from: c */
    public C5663aRz[] mo309c() {
        return AssetGroup._m_fields;
    }

    /* renamed from: d */
    public void mo310d() {
        super.mo310d();
        this.f586bK = null;
        this.handle = null;
        this.name = null;
        this.cVk = null;
        this.cVl = null;
        this.f587oL = 0;
        this.f589ok = 0;
        this.f584Nf = 0;
        this.cVm = 0;
        this.cVn = 0;
        this.f588oS = 0;
        this.f590on = 0;
        this.f585Nh = 0;
        this.cVo = 0;
        this.cVp = 0;
    }
}
