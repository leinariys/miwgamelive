package logic.data.mbean;

import game.CollisionFXSet;
import game.network.message.serializable.C3438ra;
import logic.baa.C1616Xf;
import logic.baa.C6909avx;
import logic.res.code.C5663aRz;
import p001a.C0064Am;

import java.util.UUID;

/* renamed from: a.awm  reason: case insensitive filesystem */
public class C6947awm extends C3805vD {
    @C0064Am(aul = "be5d4ae961df69e526ec240da7bcb917", aum = 0)
    public String bJo;
    @C0064Am(aul = "099f388b6479f9a81d982654d923f53a", aum = 1)
    public C3438ra<C6909avx> bJq;
    @C0064Am(aul = "850f76d56bd3df673300f868c5a6d08a", aum = 2)

    /* renamed from: bK */
    public UUID f5524bK;
    @C0064Am(aul = "be5d4ae961df69e526ec240da7bcb917", aum = -2)
    public long gMa;
    @C0064Am(aul = "099f388b6479f9a81d982654d923f53a", aum = -2)
    public long gMb;
    @C0064Am(aul = "be5d4ae961df69e526ec240da7bcb917", aum = -1)
    public byte gMc;
    @C0064Am(aul = "099f388b6479f9a81d982654d923f53a", aum = -1)
    public byte gMd;
    @C0064Am(aul = "82e98a57a8b0c9c597da6a96fc36096a", aum = 3)
    public String handle;
    @C0064Am(aul = "850f76d56bd3df673300f868c5a6d08a", aum = -2)

    /* renamed from: oL */
    public long f5525oL;
    @C0064Am(aul = "850f76d56bd3df673300f868c5a6d08a", aum = -1)

    /* renamed from: oS */
    public byte f5526oS;
    @C0064Am(aul = "82e98a57a8b0c9c597da6a96fc36096a", aum = -2)

    /* renamed from: ok */
    public long f5527ok;
    @C0064Am(aul = "82e98a57a8b0c9c597da6a96fc36096a", aum = -1)

    /* renamed from: on */
    public byte f5528on;

    public C6947awm() {
    }

    public C6947awm(C1616Xf xf) {
        super(xf);
    }

    /* renamed from: c */
    public C5663aRz[] mo309c() {
        return CollisionFXSet._m_fields;
    }

    /* renamed from: d */
    public void mo310d() {
        super.mo310d();
        this.bJo = null;
        this.bJq = null;
        this.f5524bK = null;
        this.handle = null;
        this.gMa = 0;
        this.gMb = 0;
        this.f5525oL = 0;
        this.f5527ok = 0;
        this.gMc = 0;
        this.gMd = 0;
        this.f5526oS = 0;
        this.f5528on = 0;
    }
}
