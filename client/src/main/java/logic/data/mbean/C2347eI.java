package logic.data.mbean;

import game.script.item.buff.amplifier.CargoHoldAmplifierType;
import logic.baa.C1616Xf;
import logic.res.code.C5663aRz;
import p001a.C0064Am;
import p001a.C3892wO;

/* renamed from: a.eI */
public class C2347eI extends C2421fH {
    @C0064Am(aul = "5047e81d8c1f3d258880fc37e1e97209", aum = 0)

    /* renamed from: DL */
    public C3892wO f6732DL;
    @C0064Am(aul = "5047e81d8c1f3d258880fc37e1e97209", aum = -2)

    /* renamed from: DM */
    public long f6733DM;
    @C0064Am(aul = "5047e81d8c1f3d258880fc37e1e97209", aum = -1)

    /* renamed from: DN */
    public byte f6734DN;

    public C2347eI() {
    }

    public C2347eI(C1616Xf xf) {
        super(xf);
    }

    /* renamed from: c */
    public C5663aRz[] mo309c() {
        return CargoHoldAmplifierType._m_fields;
    }

    /* renamed from: d */
    public void mo310d() {
        super.mo310d();
        this.f6732DL = null;
        this.f6733DM = 0;
        this.f6734DN = 0;
    }
}
