package logic.data.mbean;

import logic.baa.C1616Xf;
import logic.res.code.C5663aRz;
import p001a.C6195aiL;

/* renamed from: a.afk  reason: case insensitive filesystem */
/* compiled from: a */
public interface C6064afk {
    /* renamed from: A */
    void mo3146A(boolean z);

    /* renamed from: a */
    void mo3147a(C5663aRz arz, byte b);

    /* renamed from: a */
    void mo3148a(C5663aRz arz, char c);

    /* renamed from: a */
    void mo3149a(C5663aRz arz, double d);

    /* renamed from: a */
    void mo3150a(C5663aRz arz, float f);

    /* renamed from: a */
    void mo3151a(C5663aRz arz, long j);

    /* renamed from: a */
    void mo3152a(C5663aRz arz, short s);

    /* renamed from: a */
    void mo3153a(C5663aRz arz, boolean z);

    /* renamed from: a */
    void mo3156a(C6195aiL ail);

    long aBt();

    C6064afk aXj();

    C6195aiL aXp();

    /* renamed from: b */
    void mo3183b(C5663aRz arz, int i);

    /* renamed from: b */
    void mo3184b(C5663aRz arz, long j);

    /* renamed from: c */
    C5663aRz[] mo309c();

    /* renamed from: cS */
    void mo3191cS(boolean z);

    /* renamed from: d */
    void mo3193d(C5663aRz arz, Object obj);

    /* renamed from: du */
    boolean mo3194du();

    /* renamed from: e */
    void mo3195e(C5663aRz arz, Object obj);

    /* renamed from: eU */
    void mo3196eU(long j);

    /* renamed from: f */
    void mo3197f(C5663aRz arz, Object obj);

    /* renamed from: g */
    Object mo3199g(C5663aRz arz);

    /* renamed from: h */
    boolean mo3201h(C5663aRz arz);

    /* renamed from: hF */
    boolean mo3202hF();

    /* renamed from: i */
    byte mo3203i(C5663aRz arz);

    /* renamed from: j */
    char mo3206j(C5663aRz arz);

    /* renamed from: k */
    double mo3207k(C5663aRz arz);

    /* renamed from: kW */
    long mo3208kW(int i);

    /* renamed from: kX */
    Object mo3209kX(int i);

    /* renamed from: l */
    long mo3210l(C5663aRz arz);

    /* renamed from: m */
    float mo3211m(C5663aRz arz);

    /* renamed from: n */
    int mo3212n(C5663aRz arz);

    /* renamed from: o */
    long mo3213o(C5663aRz arz);

    /* renamed from: p */
    Object mo3214p(C5663aRz arz);

    /* renamed from: q */
    short mo3215q(C5663aRz arz);

    /* renamed from: r */
    Object mo3216r(C5663aRz arz);

    /* renamed from: yn */
    C1616Xf mo11944yn();
}
