package logic.data.mbean;

import game.script.item.ItemType;
import game.script.npcchat.actions.GiveItemSpeechAction;
import logic.baa.C1616Xf;
import logic.res.code.C5663aRz;
import p001a.C0064Am;

/* renamed from: a.aJq  reason: case insensitive filesystem */
public class C5446aJq extends C6352alM {
    @C0064Am(aul = "2bc61e292a6530f85ca072d00fe076d9", aum = 0)
    public ItemType aMW;
    @C0064Am(aul = "a210b5f3fe7eb2e9b713377366a691db", aum = 1)
    public int aMY;
    @C0064Am(aul = "a210b5f3fe7eb2e9b713377366a691db", aum = -1)
    public byte ckA;
    @C0064Am(aul = "2bc61e292a6530f85ca072d00fe076d9", aum = -2)
    public long ckw;
    @C0064Am(aul = "a210b5f3fe7eb2e9b713377366a691db", aum = -2)
    public long ckx;
    @C0064Am(aul = "2bc61e292a6530f85ca072d00fe076d9", aum = -1)
    public byte ckz;

    public C5446aJq() {
    }

    public C5446aJq(C1616Xf xf) {
        super(xf);
    }

    /* renamed from: c */
    public C5663aRz[] mo309c() {
        return GiveItemSpeechAction._m_fields;
    }

    /* renamed from: d */
    public void mo310d() {
        super.mo310d();
        this.aMW = null;
        this.aMY = 0;
        this.ckw = 0;
        this.ckx = 0;
        this.ckz = 0;
        this.ckA = 0;
    }
}
