package logic.data.mbean;

import game.script.spacezone.SpaceZone;
import game.script.spacezone.SpaceZoneTrigger;
import logic.baa.C1616Xf;
import logic.res.code.C5663aRz;
import p001a.C0064Am;

/* renamed from: a.aKo  reason: case insensitive filesystem */
public class C5470aKo extends C6151ahT {
    @C0064Am(aul = "199574f91a11b25ff4918dd3da4c9733", aum = -2)
    public long eLl;
    @C0064Am(aul = "199574f91a11b25ff4918dd3da4c9733", aum = -1)
    public byte eLn;
    @C0064Am(aul = "199574f91a11b25ff4918dd3da4c9733", aum = 0)
    public SpaceZone hPX;

    public C5470aKo() {
    }

    public C5470aKo(C1616Xf xf) {
        super(xf);
    }

    /* renamed from: c */
    public C5663aRz[] mo309c() {
        return SpaceZoneTrigger._m_fields;
    }

    /* renamed from: d */
    public void mo310d() {
        super.mo310d();
        this.hPX = null;
        this.eLl = 0;
        this.eLn = 0;
    }
}
