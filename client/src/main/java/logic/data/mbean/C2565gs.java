package logic.data.mbean;

import game.network.message.serializable.C3438ra;
import game.script.spacezone.AsteroidZoneType;
import game.script.spacezone.NPCZoneType;
import game.script.spacezone.PopulationControlType;
import game.script.spacezone.SpaceZoneTypesManager;
import logic.baa.C1616Xf;
import logic.res.code.C5663aRz;
import p001a.C0064Am;

import java.util.UUID;

/* renamed from: a.gs */
public class C2565gs extends C3805vD {
    @C0064Am(aul = "c838ba4e498b64eddcdf9b6cb0271cba", aum = 0)

    /* renamed from: Qb */
    public C3438ra<NPCZoneType> f7771Qb;
    @C0064Am(aul = "8776fe1b666200b8970a6b33f7d72d39", aum = 1)

    /* renamed from: Qc */
    public C3438ra<PopulationControlType> f7772Qc;
    @C0064Am(aul = "a10cd40c179a9daf94468223ed3692f6", aum = 2)

    /* renamed from: Qd */
    public C3438ra<AsteroidZoneType> f7773Qd;
    @C0064Am(aul = "c838ba4e498b64eddcdf9b6cb0271cba", aum = -2)

    /* renamed from: Qe */
    public long f7774Qe;
    @C0064Am(aul = "8776fe1b666200b8970a6b33f7d72d39", aum = -2)

    /* renamed from: Qf */
    public long f7775Qf;
    @C0064Am(aul = "a10cd40c179a9daf94468223ed3692f6", aum = -2)

    /* renamed from: Qg */
    public long f7776Qg;
    @C0064Am(aul = "c838ba4e498b64eddcdf9b6cb0271cba", aum = -1)

    /* renamed from: Qh */
    public byte f7777Qh;
    @C0064Am(aul = "8776fe1b666200b8970a6b33f7d72d39", aum = -1)

    /* renamed from: Qi */
    public byte f7778Qi;
    @C0064Am(aul = "a10cd40c179a9daf94468223ed3692f6", aum = -1)

    /* renamed from: Qj */
    public byte f7779Qj;
    @C0064Am(aul = "e24308886401a5e3cf09bb94e3e6f44b", aum = 3)

    /* renamed from: bK */
    public UUID f7780bK;
    @C0064Am(aul = "25505f127572146fe07b99a1e9c647bf", aum = 4)
    public String handle;
    @C0064Am(aul = "e24308886401a5e3cf09bb94e3e6f44b", aum = -2)

    /* renamed from: oL */
    public long f7781oL;
    @C0064Am(aul = "e24308886401a5e3cf09bb94e3e6f44b", aum = -1)

    /* renamed from: oS */
    public byte f7782oS;
    @C0064Am(aul = "25505f127572146fe07b99a1e9c647bf", aum = -2)

    /* renamed from: ok */
    public long f7783ok;
    @C0064Am(aul = "25505f127572146fe07b99a1e9c647bf", aum = -1)

    /* renamed from: on */
    public byte f7784on;

    public C2565gs() {
    }

    public C2565gs(C1616Xf xf) {
        super(xf);
    }

    /* renamed from: c */
    public C5663aRz[] mo309c() {
        return SpaceZoneTypesManager._m_fields;
    }

    /* renamed from: d */
    public void mo310d() {
        super.mo310d();
        this.f7771Qb = null;
        this.f7772Qc = null;
        this.f7773Qd = null;
        this.f7780bK = null;
        this.handle = null;
        this.f7774Qe = 0;
        this.f7775Qf = 0;
        this.f7776Qg = 0;
        this.f7781oL = 0;
        this.f7783ok = 0;
        this.f7777Qh = 0;
        this.f7778Qi = 0;
        this.f7779Qj = 0;
        this.f7782oS = 0;
        this.f7784on = 0;
    }
}
