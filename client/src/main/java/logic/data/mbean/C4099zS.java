package logic.data.mbean;

import game.script.TaikodomDefaultContents;
import game.script.billing.ItemBillingDefaults;
import game.script.chat.ChatDefaults;
import game.script.cloning.CloningDefaults;
import game.script.corporation.CorporationDefaults;
import game.script.faction.Faction;
import game.script.insurance.InsuranceDefaults;
import game.script.item.durability.ItemDurabilityDefaults;
import game.script.map3d.Map3DDefaults;
import game.script.mission.MissionBeaconType;
import game.script.mission.MissionTriggerType;
import game.script.nursery.NurseryDefaults;
import game.script.pda.PdaDefaults;
import game.script.resource.Asset;
import game.script.ship.OutpostDefaults;
import game.script.ship.ShipDefaults;
import game.script.ship.Station;
import game.script.simulation.SurfaceDefaults;
import logic.baa.C1616Xf;
import logic.res.code.C5663aRz;
import p001a.C0064Am;

import java.util.UUID;

/* renamed from: a.zS */
public class C4099zS extends C5292aDs {
    @C0064Am(aul = "aadf3703843cdc9440faa27120679278", aum = 0)

    /* renamed from: RE */
    public Faction f9661RE;
    @C0064Am(aul = "29f06c26a0a7d5c2ae3e79cce5349467", aum = 1)

    /* renamed from: RG */
    public Station f9662RG;
    @C0064Am(aul = "067b00cdae37e152be0f921c782e95cf", aum = 2)

    /* renamed from: RI */
    public MissionTriggerType f9663RI;
    @C0064Am(aul = "a71b0c2e2d4941ce5238409b58d6adb0", aum = 3)

    /* renamed from: RK */
    public MissionBeaconType f9664RK;
    @C0064Am(aul = "de0a1dbe06e7e8dc1dc70f05ee9dda57", aum = 4)

    /* renamed from: RM */
    public CorporationDefaults f9665RM;
    @C0064Am(aul = "a9d09612db39916ab6d63646e30f0973", aum = 5)

    /* renamed from: RO */
    public OutpostDefaults f9666RO;
    @C0064Am(aul = "c83ab5b194c290a010ac81267439d4f7", aum = 6)

    /* renamed from: RR */
    public CloningDefaults f9667RR;
    @C0064Am(aul = "6114faaf4c13ce481fcb05934e385d0f", aum = 7)

    /* renamed from: RT */
    public InsuranceDefaults f9668RT;
    @C0064Am(aul = "5c70f4bbfe47e9c5feab6fc4ea8b3bf6", aum = 8)

    /* renamed from: RV */
    public ItemBillingDefaults f9669RV;
    @C0064Am(aul = "a1ed1fce414d6ffef17b2e5e087d0109", aum = 9)

    /* renamed from: RX */
    public ItemDurabilityDefaults f9670RX;
    @C0064Am(aul = "cc72b75eececccca40fba3f97f175031", aum = 10)

    /* renamed from: RZ */
    public PdaDefaults f9671RZ;
    @C0064Am(aul = "8a57f6d76a1b6b36a9d91ed6135035db", aum = 11)

    /* renamed from: Sb */
    public Map3DDefaults f9672Sb;
    @C0064Am(aul = "b1bdde80558bc765a4d319e5e5d0026b", aum = 12)

    /* renamed from: Sd */
    public ChatDefaults f9673Sd;
    @C0064Am(aul = "9b7ee4e0dee9fe6ffcbaea2fcf5c865c", aum = 13)

    /* renamed from: Sf */
    public NurseryDefaults f9674Sf;
    @C0064Am(aul = "4e40949657d96f110c55100be4829e2e", aum = 14)

    /* renamed from: Sh */
    public ShipDefaults f9675Sh;
    @C0064Am(aul = "f7034cf83baf711281b0bb4d0838f7c0", aum = 15)

    /* renamed from: Sj */
    public SurfaceDefaults f9676Sj;
    @C0064Am(aul = "f87318ca231bf39b83ab80152ad5949f", aum = 16)

    /* renamed from: Sl */
    public float f9677Sl;
    @C0064Am(aul = "374390b79712898da1ce1da2441e7923", aum = 17)

    /* renamed from: Sn */
    public Asset f9678Sn;
    @C0064Am(aul = "ea68925a09852dace27e16a3b6e2dbac", aum = 18)

    /* renamed from: Sp */
    public float f9679Sp;
    @C0064Am(aul = "29f06c26a0a7d5c2ae3e79cce5349467", aum = -2)
    public long aIK;
    @C0064Am(aul = "29f06c26a0a7d5c2ae3e79cce5349467", aum = -1)
    public byte aIV;
    @C0064Am(aul = "1f74b3bee6532a1e06f47bcd4c508b82", aum = 19)

    /* renamed from: bK */
    public UUID f9680bK;
    @C0064Am(aul = "9b7ee4e0dee9fe6ffcbaea2fcf5c865c", aum = -2)
    public long cbA;
    @C0064Am(aul = "4e40949657d96f110c55100be4829e2e", aum = -2)
    public long cbB;
    @C0064Am(aul = "f7034cf83baf711281b0bb4d0838f7c0", aum = -2)
    public long cbC;
    @C0064Am(aul = "f87318ca231bf39b83ab80152ad5949f", aum = -2)
    public long cbD;
    @C0064Am(aul = "374390b79712898da1ce1da2441e7923", aum = -2)
    public long cbE;
    @C0064Am(aul = "ea68925a09852dace27e16a3b6e2dbac", aum = -2)
    public long cbF;
    @C0064Am(aul = "aadf3703843cdc9440faa27120679278", aum = -1)
    public byte cbG;
    @C0064Am(aul = "067b00cdae37e152be0f921c782e95cf", aum = -1)
    public byte cbH;
    @C0064Am(aul = "a71b0c2e2d4941ce5238409b58d6adb0", aum = -1)
    public byte cbI;
    @C0064Am(aul = "de0a1dbe06e7e8dc1dc70f05ee9dda57", aum = -1)
    public byte cbJ;
    @C0064Am(aul = "a9d09612db39916ab6d63646e30f0973", aum = -1)
    public byte cbK;
    @C0064Am(aul = "c83ab5b194c290a010ac81267439d4f7", aum = -1)
    public byte cbL;
    @C0064Am(aul = "6114faaf4c13ce481fcb05934e385d0f", aum = -1)
    public byte cbM;
    @C0064Am(aul = "5c70f4bbfe47e9c5feab6fc4ea8b3bf6", aum = -1)
    public byte cbN;
    @C0064Am(aul = "a1ed1fce414d6ffef17b2e5e087d0109", aum = -1)
    public byte cbO;
    @C0064Am(aul = "cc72b75eececccca40fba3f97f175031", aum = -1)
    public byte cbP;
    @C0064Am(aul = "8a57f6d76a1b6b36a9d91ed6135035db", aum = -1)
    public byte cbQ;
    @C0064Am(aul = "b1bdde80558bc765a4d319e5e5d0026b", aum = -1)
    public byte cbR;
    @C0064Am(aul = "9b7ee4e0dee9fe6ffcbaea2fcf5c865c", aum = -1)
    public byte cbS;
    @C0064Am(aul = "4e40949657d96f110c55100be4829e2e", aum = -1)
    public byte cbT;
    @C0064Am(aul = "f7034cf83baf711281b0bb4d0838f7c0", aum = -1)
    public byte cbU;
    @C0064Am(aul = "f87318ca231bf39b83ab80152ad5949f", aum = -1)
    public byte cbV;
    @C0064Am(aul = "374390b79712898da1ce1da2441e7923", aum = -1)
    public byte cbW;
    @C0064Am(aul = "ea68925a09852dace27e16a3b6e2dbac", aum = -1)
    public byte cbX;
    @C0064Am(aul = "aadf3703843cdc9440faa27120679278", aum = -2)
    public long cbo;
    @C0064Am(aul = "067b00cdae37e152be0f921c782e95cf", aum = -2)
    public long cbp;
    @C0064Am(aul = "a71b0c2e2d4941ce5238409b58d6adb0", aum = -2)
    public long cbq;
    @C0064Am(aul = "de0a1dbe06e7e8dc1dc70f05ee9dda57", aum = -2)
    public long cbr;
    @C0064Am(aul = "a9d09612db39916ab6d63646e30f0973", aum = -2)
    public long cbs;
    @C0064Am(aul = "c83ab5b194c290a010ac81267439d4f7", aum = -2)
    public long cbt;
    @C0064Am(aul = "6114faaf4c13ce481fcb05934e385d0f", aum = -2)
    public long cbu;
    @C0064Am(aul = "5c70f4bbfe47e9c5feab6fc4ea8b3bf6", aum = -2)
    public long cbv;
    @C0064Am(aul = "a1ed1fce414d6ffef17b2e5e087d0109", aum = -2)
    public long cbw;
    @C0064Am(aul = "cc72b75eececccca40fba3f97f175031", aum = -2)
    public long cbx;
    @C0064Am(aul = "8a57f6d76a1b6b36a9d91ed6135035db", aum = -2)
    public long cby;
    @C0064Am(aul = "b1bdde80558bc765a4d319e5e5d0026b", aum = -2)
    public long cbz;
    @C0064Am(aul = "1f74b3bee6532a1e06f47bcd4c508b82", aum = -2)

    /* renamed from: oL */
    public long f9681oL;
    @C0064Am(aul = "1f74b3bee6532a1e06f47bcd4c508b82", aum = -1)

    /* renamed from: oS */
    public byte f9682oS;

    public C4099zS() {
    }

    public C4099zS(C1616Xf xf) {
        super(xf);
    }

    /* renamed from: c */
    public C5663aRz[] mo309c() {
        return TaikodomDefaultContents._m_fields;
    }

    /* renamed from: d */
    public void mo310d() {
        super.mo310d();
        this.f9661RE = null;
        this.f9662RG = null;
        this.f9663RI = null;
        this.f9664RK = null;
        this.f9665RM = null;
        this.f9666RO = null;
        this.f9667RR = null;
        this.f9668RT = null;
        this.f9669RV = null;
        this.f9670RX = null;
        this.f9671RZ = null;
        this.f9672Sb = null;
        this.f9673Sd = null;
        this.f9674Sf = null;
        this.f9675Sh = null;
        this.f9676Sj = null;
        this.f9677Sl = 0.0f;
        this.f9678Sn = null;
        this.f9679Sp = 0.0f;
        this.f9680bK = null;
        this.cbo = 0;
        this.aIK = 0;
        this.cbp = 0;
        this.cbq = 0;
        this.cbr = 0;
        this.cbs = 0;
        this.cbt = 0;
        this.cbu = 0;
        this.cbv = 0;
        this.cbw = 0;
        this.cbx = 0;
        this.cby = 0;
        this.cbz = 0;
        this.cbA = 0;
        this.cbB = 0;
        this.cbC = 0;
        this.cbD = 0;
        this.cbE = 0;
        this.cbF = 0;
        this.f9681oL = 0;
        this.cbG = 0;
        this.aIV = 0;
        this.cbH = 0;
        this.cbI = 0;
        this.cbJ = 0;
        this.cbK = 0;
        this.cbL = 0;
        this.cbM = 0;
        this.cbN = 0;
        this.cbO = 0;
        this.cbP = 0;
        this.cbQ = 0;
        this.cbR = 0;
        this.cbS = 0;
        this.cbT = 0;
        this.cbU = 0;
        this.cbV = 0;
        this.cbW = 0;
        this.cbX = 0;
        this.f9682oS = 0;
    }
}
