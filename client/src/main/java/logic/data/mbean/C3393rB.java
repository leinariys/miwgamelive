package logic.data.mbean;

import game.script.newmarket.BuyOrder;
import game.script.newmarket.BuyOrderInfo;
import logic.baa.C1616Xf;
import logic.res.code.C5663aRz;
import p001a.C0064Am;
import p001a.C0847MM;

/* renamed from: a.rB */
public class C3393rB extends C0134Bf {
    @C0064Am(aul = "e9a202f1ce243242187751f25b0833b9", aum = 0)
    public BuyOrder bbh;
    @C0064Am(aul = "546b1c5782cc302b179e877446748f61", aum = 1)
    public C0847MM bbi;
    @C0064Am(aul = "e9a202f1ce243242187751f25b0833b9", aum = -2)
    public long bbj;
    @C0064Am(aul = "546b1c5782cc302b179e877446748f61", aum = -2)
    public long bbk;
    @C0064Am(aul = "e9a202f1ce243242187751f25b0833b9", aum = -1)
    public byte bbl;
    @C0064Am(aul = "546b1c5782cc302b179e877446748f61", aum = -1)
    public byte bbm;

    public C3393rB() {
    }

    public C3393rB(C1616Xf xf) {
        super(xf);
    }

    /* renamed from: c */
    public C5663aRz[] mo309c() {
        return BuyOrderInfo._m_fields;
    }

    /* renamed from: d */
    public void mo310d() {
        super.mo310d();
        this.bbh = null;
        this.bbi = null;
        this.bbj = 0;
        this.bbk = 0;
        this.bbl = 0;
        this.bbm = 0;
    }
}
