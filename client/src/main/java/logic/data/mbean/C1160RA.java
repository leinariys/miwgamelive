package logic.data.mbean;

import game.script.Character;
import game.script.mission.scripting.AssaultMissionScript;
import game.script.missiontemplate.WaypointDat;
import logic.baa.C1616Xf;
import logic.res.code.C5663aRz;
import p001a.C0064Am;

import java.util.List;

/* renamed from: a.RA */
public class C1160RA extends C2955mD {
    @C0064Am(aul = "3c4d425997d1540c05bc0a9e0f489fbe", aum = 3)
    public AssaultMissionScript bHH;
    @C0064Am(aul = "8a677f4d701c183fd6ae9fc919aa5292", aum = 0)
    public WaypointDat bHY;
    @C0064Am(aul = "024c25e609299a24d68749ba5c12c5e0", aum = 1)
    public List<Character> bIa;
    @C0064Am(aul = "89f6576475ecb2d42f202c9af18e8050", aum = 2)
    public List<Character> bIc;
    @C0064Am(aul = "8a677f4d701c183fd6ae9fc919aa5292", aum = -2)
    public long cCB;
    @C0064Am(aul = "8a677f4d701c183fd6ae9fc919aa5292", aum = -1)
    public byte cCE;
    @C0064Am(aul = "3c4d425997d1540c05bc0a9e0f489fbe", aum = -2)

    /* renamed from: dl */
    public long f1482dl;
    @C0064Am(aul = "3c4d425997d1540c05bc0a9e0f489fbe", aum = -1)

    /* renamed from: ds */
    public byte f1483ds;
    @C0064Am(aul = "024c25e609299a24d68749ba5c12c5e0", aum = -2)
    public long eaD;
    @C0064Am(aul = "89f6576475ecb2d42f202c9af18e8050", aum = -2)
    public long eaE;
    @C0064Am(aul = "024c25e609299a24d68749ba5c12c5e0", aum = -1)
    public byte eaF;
    @C0064Am(aul = "89f6576475ecb2d42f202c9af18e8050", aum = -1)
    public byte eaG;

    public C1160RA() {
    }

    public C1160RA(C1616Xf xf) {
        super(xf);
    }

    /* renamed from: c */
    public C5663aRz[] mo309c() {
        return AssaultMissionScript.StateB._m_fields;
    }

    /* renamed from: d */
    public void mo310d() {
        super.mo310d();
        this.bHY = null;
        this.bIa = null;
        this.bIc = null;
        this.bHH = null;
        this.cCB = 0;
        this.eaD = 0;
        this.eaE = 0;
        this.f1482dl = 0;
        this.cCE = 0;
        this.eaF = 0;
        this.eaG = 0;
        this.f1483ds = 0;
    }
}
