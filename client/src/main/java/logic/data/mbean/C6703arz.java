package logic.data.mbean;

import game.script.ship.CargoHold;
import game.script.ship.CargoHoldAdapter;
import game.script.ship.Ship;
import game.script.util.AdapterLikedList;
import logic.baa.C1616Xf;
import logic.res.code.C5663aRz;
import p001a.C0064Am;

/* renamed from: a.arz  reason: case insensitive filesystem */
public class C6703arz extends aAL {
    @C0064Am(aul = "c8979053f765f77fd33d2ec5865690c4", aum = -2)

    /* renamed from: DM */
    public long f5272DM;
    @C0064Am(aul = "c8979053f765f77fd33d2ec5865690c4", aum = -1)

    /* renamed from: DN */
    public byte f5273DN;
    @C0064Am(aul = "8aa4c2db7fd5a6b3a69e963e0e779b4d", aum = 1)
    public Ship aOO;
    @C0064Am(aul = "5e94d92cb28f70ab377e77e7ce7415cc", aum = 3)
    public AdapterLikedList<CargoHoldAdapter> aOw;
    @C0064Am(aul = "5c79dd35bb4b25df84afde6440d4f0b1", aum = 2)
    public float aUC;
    @C0064Am(aul = "5e94d92cb28f70ab377e77e7ce7415cc", aum = -2)
    public long aUE;
    @C0064Am(aul = "5c79dd35bb4b25df84afde6440d4f0b1", aum = -2)
    public long aUF;
    @C0064Am(aul = "5e94d92cb28f70ab377e77e7ce7415cc", aum = -1)
    public byte aUH;
    @C0064Am(aul = "5c79dd35bb4b25df84afde6440d4f0b1", aum = -1)
    public byte aUI;
    @C0064Am(aul = "8aa4c2db7fd5a6b3a69e963e0e779b4d", aum = -2)
    public long cSU;
    @C0064Am(aul = "8aa4c2db7fd5a6b3a69e963e0e779b4d", aum = -1)
    public byte cSW;
    @C0064Am(aul = "c8979053f765f77fd33d2ec5865690c4", aum = 0)
    public float cvd;

    public C6703arz() {
    }

    public C6703arz(C1616Xf xf) {
        super(xf);
    }

    /* renamed from: c */
    public C5663aRz[] mo309c() {
        return CargoHold._m_fields;
    }

    /* renamed from: d */
    public void mo310d() {
        super.mo310d();
        this.cvd = 0.0f;
        this.aOO = null;
        this.aUC = 0.0f;
        this.aOw = null;
        this.f5272DM = 0;
        this.cSU = 0;
        this.aUF = 0;
        this.aUE = 0;
        this.f5273DN = 0;
        this.cSW = 0;
        this.aUI = 0;
        this.aUH = 0;
    }
}
