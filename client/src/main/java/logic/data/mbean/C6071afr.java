package logic.data.mbean;

import game.script.citizenship.CitizenImprovementType;
import game.script.resource.Asset;
import logic.baa.C1616Xf;
import logic.res.code.C5663aRz;
import p001a.C0064Am;
import taikodom.infra.script.I18NString;

/* renamed from: a.afr  reason: case insensitive filesystem */
public class C6071afr extends C1183RV {
    @C0064Am(aul = "d58d8e51ac17338e1eef3c8ff4b8b202", aum = -1)

    /* renamed from: jK */
    public byte f4472jK;
    @C0064Am(aul = "d58d8e51ac17338e1eef3c8ff4b8b202", aum = 2)

    /* renamed from: jn */
    public Asset f4473jn;
    @C0064Am(aul = "d58d8e51ac17338e1eef3c8ff4b8b202", aum = -2)

    /* renamed from: jy */
    public long f4474jy;
    @C0064Am(aul = "aa81013832de638c66e72a49c429f303", aum = 0)

    /* renamed from: nh */
    public I18NString f4475nh;
    @C0064Am(aul = "969fa6e847faa13cf49490588b9a7edc", aum = 1)

    /* renamed from: ni */
    public I18NString f4476ni;
    @C0064Am(aul = "aa81013832de638c66e72a49c429f303", aum = -2)

    /* renamed from: nk */
    public long f4477nk;
    @C0064Am(aul = "969fa6e847faa13cf49490588b9a7edc", aum = -2)

    /* renamed from: nl */
    public long f4478nl;
    @C0064Am(aul = "aa81013832de638c66e72a49c429f303", aum = -1)

    /* renamed from: nn */
    public byte f4479nn;
    @C0064Am(aul = "969fa6e847faa13cf49490588b9a7edc", aum = -1)

    /* renamed from: no */
    public byte f4480no;

    public C6071afr() {
    }

    public C6071afr(C1616Xf xf) {
        super(xf);
    }

    /* renamed from: c */
    public C5663aRz[] mo309c() {
        return CitizenImprovementType._m_fields;
    }

    /* renamed from: d */
    public void mo310d() {
        super.mo310d();
        this.f4475nh = null;
        this.f4476ni = null;
        this.f4473jn = null;
        this.f4477nk = 0;
        this.f4478nl = 0;
        this.f4474jy = 0;
        this.f4479nn = 0;
        this.f4480no = 0;
        this.f4472jK = 0;
    }
}
