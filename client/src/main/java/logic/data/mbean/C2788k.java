package logic.data.mbean;

import game.script.player.Player;
import game.script.player.PlayerSessionLog;
import logic.baa.C1616Xf;
import logic.res.code.C5663aRz;
import p001a.C0064Am;

/* renamed from: a.k */
public class C2788k extends C3805vD {
    @C0064Am(aul = "10eb8abe3d131f6489f41aeba3dbff62", aum = 0)

    /* renamed from: P */
    public Player f8362P;
    @C0064Am(aul = "078359c7c36cbc43f3424f2ab2a10e40", aum = 1)

    /* renamed from: Q */
    public long f8363Q;
    @C0064Am(aul = "98d34c3024bdcc3112c716b8c0d2c74d", aum = 2)

    /* renamed from: R */
    public long f8364R;
    @C0064Am(aul = "231e38476cd5e2193c79c7e0c5bffa39", aum = 3)

    /* renamed from: S */
    public int f8365S;
    @C0064Am(aul = "1700fc6c5409860f4b7a157c20154ff0", aum = 4)

    /* renamed from: T */
    public long f8366T;
    @C0064Am(aul = "bf805df432c9b97ebbf3c8f1cb04ab21", aum = 5)

    /* renamed from: U */
    public int f8367U;
    @C0064Am(aul = "8070018378a974d05fff3029a32d08a4", aum = 6)

    /* renamed from: V */
    public int f8368V;
    @C0064Am(aul = "67b78b5daa2a29947c5c3ade4754d9ae", aum = 7)

    /* renamed from: W */
    public int f8369W;
    @C0064Am(aul = "14c8b890f33b0a69651e10d240099f46", aum = 8)

    /* renamed from: X */
    public int f8370X;
    @C0064Am(aul = "bf805df432c9b97ebbf3c8f1cb04ab21", aum = -1)

    /* renamed from: aA */
    public byte f8371aA;
    @C0064Am(aul = "8070018378a974d05fff3029a32d08a4", aum = -1)

    /* renamed from: aB */
    public byte f8372aB;
    @C0064Am(aul = "67b78b5daa2a29947c5c3ade4754d9ae", aum = -1)

    /* renamed from: aC */
    public byte f8373aC;
    @C0064Am(aul = "14c8b890f33b0a69651e10d240099f46", aum = -1)

    /* renamed from: aD */
    public byte f8374aD;
    @C0064Am(aul = "274447d1ff8414421a2e661ba4c7795e", aum = -1)

    /* renamed from: aE */
    public byte f8375aE;
    @C0064Am(aul = "331b503223d16d5fb6f0816b461e5e17", aum = -1)

    /* renamed from: aF */
    public byte f8376aF;
    @C0064Am(aul = "571c7cc918767fe9e52c4ab5373514d8", aum = -1)

    /* renamed from: aG */
    public byte f8377aG;
    @C0064Am(aul = "e49865ae4d21199d6678fac3320551c2", aum = -1)

    /* renamed from: aH */
    public byte f8378aH;
    @C0064Am(aul = "71135dadc3eaa2e32455c904cdb3122a", aum = -1)

    /* renamed from: aI */
    public byte f8379aI;
    @C0064Am(aul = "76ef260b46db3c2307e6ede355511bcf", aum = -1)

    /* renamed from: aJ */
    public byte f8380aJ;
    @C0064Am(aul = "274447d1ff8414421a2e661ba4c7795e", aum = 9)

    /* renamed from: aa */
    public long f8381aa;
    @C0064Am(aul = "331b503223d16d5fb6f0816b461e5e17", aum = 10)

    /* renamed from: ab */
    public long f8382ab;
    @C0064Am(aul = "571c7cc918767fe9e52c4ab5373514d8", aum = 11)

    /* renamed from: ac */
    public long f8383ac;
    @C0064Am(aul = "e49865ae4d21199d6678fac3320551c2", aum = 12)

    /* renamed from: ad */
    public long f8384ad;
    @C0064Am(aul = "71135dadc3eaa2e32455c904cdb3122a", aum = 13)

    /* renamed from: ae */
    public long f8385ae;
    @C0064Am(aul = "76ef260b46db3c2307e6ede355511bcf", aum = 14)

    /* renamed from: af */
    public PlayerSessionLog.PlayerSession f8386af;
    @C0064Am(aul = "10eb8abe3d131f6489f41aeba3dbff62", aum = -2)

    /* renamed from: ag */
    public long f8387ag;
    @C0064Am(aul = "078359c7c36cbc43f3424f2ab2a10e40", aum = -2)

    /* renamed from: ah */
    public long f8388ah;
    @C0064Am(aul = "98d34c3024bdcc3112c716b8c0d2c74d", aum = -2)

    /* renamed from: ai */
    public long f8389ai;
    @C0064Am(aul = "231e38476cd5e2193c79c7e0c5bffa39", aum = -2)

    /* renamed from: aj */
    public long f8390aj;
    @C0064Am(aul = "1700fc6c5409860f4b7a157c20154ff0", aum = -2)

    /* renamed from: ak */
    public long f8391ak;
    @C0064Am(aul = "bf805df432c9b97ebbf3c8f1cb04ab21", aum = -2)

    /* renamed from: al */
    public long f8392al;
    @C0064Am(aul = "8070018378a974d05fff3029a32d08a4", aum = -2)

    /* renamed from: am */
    public long f8393am;
    @C0064Am(aul = "67b78b5daa2a29947c5c3ade4754d9ae", aum = -2)

    /* renamed from: an */
    public long f8394an;
    @C0064Am(aul = "14c8b890f33b0a69651e10d240099f46", aum = -2)

    /* renamed from: ao */
    public long f8395ao;
    @C0064Am(aul = "274447d1ff8414421a2e661ba4c7795e", aum = -2)

    /* renamed from: ap */
    public long f8396ap;
    @C0064Am(aul = "331b503223d16d5fb6f0816b461e5e17", aum = -2)

    /* renamed from: aq */
    public long f8397aq;
    @C0064Am(aul = "571c7cc918767fe9e52c4ab5373514d8", aum = -2)

    /* renamed from: ar */
    public long f8398ar;
    @C0064Am(aul = "e49865ae4d21199d6678fac3320551c2", aum = -2)

    /* renamed from: as */
    public long f8399as;
    @C0064Am(aul = "71135dadc3eaa2e32455c904cdb3122a", aum = -2)

    /* renamed from: at */
    public long f8400at;
    @C0064Am(aul = "76ef260b46db3c2307e6ede355511bcf", aum = -2)

    /* renamed from: au */
    public long f8401au;
    @C0064Am(aul = "10eb8abe3d131f6489f41aeba3dbff62", aum = -1)

    /* renamed from: av */
    public byte f8402av;
    @C0064Am(aul = "078359c7c36cbc43f3424f2ab2a10e40", aum = -1)

    /* renamed from: aw */
    public byte f8403aw;
    @C0064Am(aul = "98d34c3024bdcc3112c716b8c0d2c74d", aum = -1)

    /* renamed from: ax */
    public byte f8404ax;
    @C0064Am(aul = "231e38476cd5e2193c79c7e0c5bffa39", aum = -1)

    /* renamed from: ay */
    public byte f8405ay;
    @C0064Am(aul = "1700fc6c5409860f4b7a157c20154ff0", aum = -1)

    /* renamed from: az */
    public byte f8406az;

    public C2788k() {
    }

    public C2788k(C1616Xf xf) {
        super(xf);
    }

    /* renamed from: c */
    public C5663aRz[] mo309c() {
        return PlayerSessionLog._m_fields;
    }

    /* renamed from: d */
    public void mo310d() {
        super.mo310d();
        this.f8362P = null;
        this.f8363Q = 0;
        this.f8364R = 0;
        this.f8365S = 0;
        this.f8366T = 0;
        this.f8367U = 0;
        this.f8368V = 0;
        this.f8369W = 0;
        this.f8370X = 0;
        this.f8381aa = 0;
        this.f8382ab = 0;
        this.f8383ac = 0;
        this.f8384ad = 0;
        this.f8385ae = 0;
        this.f8386af = null;
        this.f8387ag = 0;
        this.f8388ah = 0;
        this.f8389ai = 0;
        this.f8390aj = 0;
        this.f8391ak = 0;
        this.f8392al = 0;
        this.f8393am = 0;
        this.f8394an = 0;
        this.f8395ao = 0;
        this.f8396ap = 0;
        this.f8397aq = 0;
        this.f8398ar = 0;
        this.f8399as = 0;
        this.f8400at = 0;
        this.f8401au = 0;
        this.f8402av = 0;
        this.f8403aw = 0;
        this.f8404ax = 0;
        this.f8405ay = 0;
        this.f8406az = 0;
        this.f8371aA = 0;
        this.f8372aB = 0;
        this.f8373aC = 0;
        this.f8374aD = 0;
        this.f8375aE = 0;
        this.f8376aF = 0;
        this.f8377aG = 0;
        this.f8378aH = 0;
        this.f8379aI = 0;
        this.f8380aJ = 0;
    }
}
