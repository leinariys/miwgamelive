package logic.data.mbean;

import game.network.message.serializable.C1556Wo;
import game.script.chat.ChatDefaults;
import logic.baa.C1616Xf;
import logic.res.code.C5663aRz;
import p001a.C0064Am;
import p001a.aFU;
import taikodom.infra.script.I18NString;

import java.util.UUID;

/* renamed from: a.adj  reason: case insensitive filesystem */
public class C5959adj extends C3805vD {
    @C0064Am(aul = "2bf94b76aef044335864931bc8601bad", aum = 0)
    public C1556Wo<aFU, I18NString> asK;
    @C0064Am(aul = "b074cd21520b1bc5f594520f49f58d84", aum = 1)

    /* renamed from: bK */
    public UUID f4332bK;
    @C0064Am(aul = "2bf94b76aef044335864931bc8601bad", aum = -2)
    public long fhk;
    @C0064Am(aul = "2bf94b76aef044335864931bc8601bad", aum = -1)
    public byte fhl;
    @C0064Am(aul = "b074cd21520b1bc5f594520f49f58d84", aum = -2)

    /* renamed from: oL */
    public long f4333oL;
    @C0064Am(aul = "b074cd21520b1bc5f594520f49f58d84", aum = -1)

    /* renamed from: oS */
    public byte f4334oS;

    public C5959adj() {
    }

    public C5959adj(C1616Xf xf) {
        super(xf);
    }

    /* renamed from: c */
    public C5663aRz[] mo309c() {
        return ChatDefaults._m_fields;
    }

    /* renamed from: d */
    public void mo310d() {
        super.mo310d();
        this.asK = null;
        this.f4332bK = null;
        this.fhk = 0;
        this.f4333oL = 0;
        this.fhl = 0;
        this.f4334oS = 0;
    }
}
