package logic.data.mbean;

import game.network.message.serializable.C2686iZ;
import game.script.space.Node;
import logic.baa.C1616Xf;
import logic.res.code.C5663aRz;
import p001a.C0064Am;

import java.util.UUID;

/* renamed from: a.KN */
public class C0721KN extends C5292aDs {
    @C0064Am(aul = "9cb48776ad4fac3eebc7234995150e33", aum = 0)

    /* renamed from: bK */
    public UUID f944bK;
    @C0064Am(aul = "00693877b5ccf0b9c43b215825c0fc1b", aum = 2)
    public C2686iZ<T> dqO;
    @C0064Am(aul = "00693877b5ccf0b9c43b215825c0fc1b", aum = -2)
    public long dqP;
    @C0064Am(aul = "00693877b5ccf0b9c43b215825c0fc1b", aum = -1)
    public byte dqQ;
    @C0064Am(aul = "f0cd3c6860f38db18cd147abfbfaac2b", aum = 1)
    public String handle;
    @C0064Am(aul = "9cb48776ad4fac3eebc7234995150e33", aum = -2)

    /* renamed from: oL */
    public long f945oL;
    @C0064Am(aul = "9cb48776ad4fac3eebc7234995150e33", aum = -1)

    /* renamed from: oS */
    public byte f946oS;
    @C0064Am(aul = "f0cd3c6860f38db18cd147abfbfaac2b", aum = -2)

    /* renamed from: ok */
    public long f947ok;
    @C0064Am(aul = "f0cd3c6860f38db18cd147abfbfaac2b", aum = -1)

    /* renamed from: on */
    public byte f948on;

    public C0721KN() {
    }

    public C0721KN(C1616Xf xf) {
        super(xf);
    }

    /* renamed from: c */
    public C5663aRz[] mo309c() {
        return Node.NodeObjectHolder._m_fields;
    }

    /* renamed from: d */
    public void mo310d() {
        super.mo310d();
        this.f944bK = null;
        this.handle = null;
        this.dqO = null;
        this.f945oL = 0;
        this.f947ok = 0;
        this.dqP = 0;
        this.f946oS = 0;
        this.f948on = 0;
        this.dqQ = 0;
    }
}
