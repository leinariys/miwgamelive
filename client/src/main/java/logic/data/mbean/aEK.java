package logic.data.mbean;

import game.network.message.serializable.C3438ra;
import game.script.ship.*;
import logic.baa.C1616Xf;
import logic.res.code.C5663aRz;
import p001a.C0064Am;

/* renamed from: a.aEK */
public class aEK extends C5292aDs {
    @C0064Am(aul = "4d9837d7683bbc168b0a3aa29c9f8d98", aum = 3)
    public Ship aOO;
    @C0064Am(aul = "22f001e3fd7a849fd6648314aa22edf6", aum = 0)
    public C3438ra<ShipSectorType> cJI;
    @C0064Am(aul = "4d9837d7683bbc168b0a3aa29c9f8d98", aum = -2)
    public long cSU;
    @C0064Am(aul = "4d9837d7683bbc168b0a3aa29c9f8d98", aum = -1)
    public byte cSW;
    @C0064Am(aul = "7bb6f81c91b1735f22e0c54932dfdd7a", aum = 2)
    public Shield dmi;
    @C0064Am(aul = "7bb6f81c91b1735f22e0c54932dfdd7a", aum = -2)
    public long fPh;
    @C0064Am(aul = "7bb6f81c91b1735f22e0c54932dfdd7a", aum = -1)
    public byte fPo;
    @C0064Am(aul = "725de254639b84516756989dc53f4064", aum = 1)
    public C3438ra<ShipSector> fvs;
    @C0064Am(aul = "22f001e3fd7a849fd6648314aa22edf6", aum = -2)
    public long gdc;
    @C0064Am(aul = "22f001e3fd7a849fd6648314aa22edf6", aum = -1)
    public byte gdd;
    @C0064Am(aul = "725de254639b84516756989dc53f4064", aum = -2)
    public long hHO;
    @C0064Am(aul = "725de254639b84516756989dc53f4064", aum = -1)
    public byte hHP;

    public aEK() {
    }

    public aEK(C1616Xf xf) {
        super(xf);
    }

    /* renamed from: c */
    public C5663aRz[] mo309c() {
        return ShipStructure._m_fields;
    }

    /* renamed from: d */
    public void mo310d() {
        super.mo310d();
        this.cJI = null;
        this.fvs = null;
        this.dmi = null;
        this.aOO = null;
        this.gdc = 0;
        this.hHO = 0;
        this.fPh = 0;
        this.cSU = 0;
        this.gdd = 0;
        this.hHP = 0;
        this.fPo = 0;
        this.cSW = 0;
    }
}
