package logic.data.mbean;

import game.script.nls.NLSHUDSelection;
import logic.baa.C1616Xf;
import logic.res.code.C5663aRz;
import p001a.C0064Am;
import taikodom.infra.script.I18NString;

/* renamed from: a.arg  reason: case insensitive filesystem */
public class C6684arg extends C2484fi {
    @C0064Am(aul = "b431c10f460e80f3b0fb2e3ed0ac9e7a", aum = -2)

    /* renamed from: Nf */
    public long f5242Nf;
    @C0064Am(aul = "b431c10f460e80f3b0fb2e3ed0ac9e7a", aum = -1)

    /* renamed from: Nh */
    public byte f5243Nh;
    @C0064Am(aul = "2474566baa8259f16e3e7b51754fe630", aum = 0)
    public I18NString etF;
    @C0064Am(aul = "3539f4ac275bbd1a7dfcb9ea02aed96e", aum = 1)
    public I18NString etH;
    @C0064Am(aul = "b18c9b0641fd2c7dea4bca22ec3d2410", aum = 3)
    public I18NString etJ;
    @C0064Am(aul = "75a8cccda944501a0d1a076adf3e76df", aum = 4)
    public I18NString etL;
    @C0064Am(aul = "2474566baa8259f16e3e7b51754fe630", aum = -2)
    public long grn;
    @C0064Am(aul = "3539f4ac275bbd1a7dfcb9ea02aed96e", aum = -2)
    public long gro;
    @C0064Am(aul = "b18c9b0641fd2c7dea4bca22ec3d2410", aum = -2)
    public long grp;
    @C0064Am(aul = "75a8cccda944501a0d1a076adf3e76df", aum = -2)
    public long grq;
    @C0064Am(aul = "2474566baa8259f16e3e7b51754fe630", aum = -1)
    public byte grr;
    @C0064Am(aul = "3539f4ac275bbd1a7dfcb9ea02aed96e", aum = -1)
    public byte grs;
    @C0064Am(aul = "b18c9b0641fd2c7dea4bca22ec3d2410", aum = -1)
    public byte grt;
    @C0064Am(aul = "75a8cccda944501a0d1a076adf3e76df", aum = -1)
    public byte gru;
    @C0064Am(aul = "b431c10f460e80f3b0fb2e3ed0ac9e7a", aum = 2)

    /* renamed from: zP */
    public I18NString f5244zP;

    public C6684arg() {
    }

    public C6684arg(C1616Xf xf) {
        super(xf);
    }

    /* renamed from: c */
    public C5663aRz[] mo309c() {
        return NLSHUDSelection._m_fields;
    }

    /* renamed from: d */
    public void mo310d() {
        super.mo310d();
        this.etF = null;
        this.etH = null;
        this.f5244zP = null;
        this.etJ = null;
        this.etL = null;
        this.grn = 0;
        this.gro = 0;
        this.f5242Nf = 0;
        this.grp = 0;
        this.grq = 0;
        this.grr = 0;
        this.grs = 0;
        this.f5243Nh = 0;
        this.grt = 0;
        this.gru = 0;
    }
}
