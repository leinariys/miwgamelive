package logic.data.mbean;

import game.script.crontab.CrontabEvent;
import logic.baa.C1616Xf;
import logic.res.code.C5663aRz;
import p001a.C0064Am;
import p001a.C5832abM;

/* renamed from: a.ky */
public class C2846ky extends C5292aDs {
    @C0064Am(aul = "766f60179d3f8c1f89811244d2052bb3", aum = -1)
    public byte auA;
    @C0064Am(aul = "eeb012b36bd5d2632f9d2031e69bf508", aum = 0)
    public long auv;
    @C0064Am(aul = "766f60179d3f8c1f89811244d2052bb3", aum = 1)
    public C5832abM auw;
    @C0064Am(aul = "eeb012b36bd5d2632f9d2031e69bf508", aum = -2)
    public long aux;
    @C0064Am(aul = "766f60179d3f8c1f89811244d2052bb3", aum = -2)
    public long auy;
    @C0064Am(aul = "eeb012b36bd5d2632f9d2031e69bf508", aum = -1)
    public byte auz;
    @C0064Am(aul = "c79ae3304b7f1774e504960237eb6648", aum = 2)
    public String handle;
    @C0064Am(aul = "c79ae3304b7f1774e504960237eb6648", aum = -2)

    /* renamed from: ok */
    public long f8472ok;
    @C0064Am(aul = "c79ae3304b7f1774e504960237eb6648", aum = -1)

    /* renamed from: on */
    public byte f8473on;

    public C2846ky() {
    }

    public C2846ky(C1616Xf xf) {
        super(xf);
    }

    /* renamed from: c */
    public C5663aRz[] mo309c() {
        return CrontabEvent._m_fields;
    }

    /* renamed from: d */
    public void mo310d() {
        super.mo310d();
        this.auv = 0;
        this.auw = null;
        this.handle = null;
        this.aux = 0;
        this.auy = 0;
        this.f8472ok = 0;
        this.auz = 0;
        this.auA = 0;
        this.f8473on = 0;
    }
}
