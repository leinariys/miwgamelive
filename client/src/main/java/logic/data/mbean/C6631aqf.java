package logic.data.mbean;

import game.network.message.serializable.C2686iZ;
import game.script.Character;
import game.script.avatar.Avatar;
import game.script.avatar.body.AvatarAppearance;
import game.script.avatar.cloth.Cloth;
import logic.baa.C1616Xf;
import logic.res.code.C5663aRz;
import p001a.C0064Am;

/* renamed from: a.aqf  reason: case insensitive filesystem */
public class C6631aqf extends C3805vD {
    @C0064Am(aul = "a06a8e34e212fd07c85ab02bdcb90904", aum = 0)
    public Character boH;
    @C0064Am(aul = "add67362962d94402b0ef0632d35aa6f", aum = 1)
    public AvatarAppearance cSk;
    @C0064Am(aul = "b45a7e5036112d875e27aec02029a92f", aum = 2)
    public C2686iZ<Cloth> cSm;
    @C0064Am(aul = "93b53c11698512e20ac7bb37a235b977", aum = 3)
    public Cloth cSo;
    @C0064Am(aul = "6706c65d39d4eb9d35611bfb1aca6570", aum = 4)
    public Cloth cSq;
    @C0064Am(aul = "c5b52155c860f963bec2150a4cfb933f", aum = 5)
    public String cSs;
    @C0064Am(aul = "c9cbb477bf9b92b454e59f8ba86d00e5", aum = 6)
    public C2686iZ<Cloth> cSu;
    @C0064Am(aul = "a06a8e34e212fd07c85ab02bdcb90904", aum = -2)
    public long goP;
    @C0064Am(aul = "add67362962d94402b0ef0632d35aa6f", aum = -2)
    public long goQ;
    @C0064Am(aul = "b45a7e5036112d875e27aec02029a92f", aum = -2)
    public long goR;
    @C0064Am(aul = "93b53c11698512e20ac7bb37a235b977", aum = -2)
    public long goS;
    @C0064Am(aul = "6706c65d39d4eb9d35611bfb1aca6570", aum = -2)
    public long goT;
    @C0064Am(aul = "c5b52155c860f963bec2150a4cfb933f", aum = -2)
    public long goU;
    @C0064Am(aul = "c9cbb477bf9b92b454e59f8ba86d00e5", aum = -2)
    public long goV;
    @C0064Am(aul = "a06a8e34e212fd07c85ab02bdcb90904", aum = -1)
    public byte goW;
    @C0064Am(aul = "add67362962d94402b0ef0632d35aa6f", aum = -1)
    public byte goX;
    @C0064Am(aul = "b45a7e5036112d875e27aec02029a92f", aum = -1)
    public byte goY;
    @C0064Am(aul = "93b53c11698512e20ac7bb37a235b977", aum = -1)
    public byte goZ;
    @C0064Am(aul = "6706c65d39d4eb9d35611bfb1aca6570", aum = -1)
    public byte gpa;
    @C0064Am(aul = "c5b52155c860f963bec2150a4cfb933f", aum = -1)
    public byte gpb;
    @C0064Am(aul = "c9cbb477bf9b92b454e59f8ba86d00e5", aum = -1)
    public byte gpc;

    public C6631aqf() {
    }

    public C6631aqf(C1616Xf xf) {
        super(xf);
    }

    /* renamed from: c */
    public C5663aRz[] mo309c() {
        return Avatar._m_fields;
    }

    /* renamed from: d */
    public void mo310d() {
        super.mo310d();
        this.boH = null;
        this.cSk = null;
        this.cSm = null;
        this.cSo = null;
        this.cSq = null;
        this.cSs = null;
        this.cSu = null;
        this.goP = 0;
        this.goQ = 0;
        this.goR = 0;
        this.goS = 0;
        this.goT = 0;
        this.goU = 0;
        this.goV = 0;
        this.goW = 0;
        this.goX = 0;
        this.goY = 0;
        this.goZ = 0;
        this.gpa = 0;
        this.gpb = 0;
        this.gpc = 0;
    }
}
