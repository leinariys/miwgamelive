package logic.data.mbean;

import game.network.message.serializable.C3438ra;
import game.script.item.MerchandiseType;
import game.script.pda.DatabaseCategory;
import game.script.pda.TaikopediaEntry;
import game.script.resource.Asset;
import logic.baa.C1616Xf;
import logic.res.code.C5663aRz;
import p001a.C0064Am;

/* renamed from: a.azv  reason: case insensitive filesystem */
public class C7034azv extends C5534aNa {
    @C0064Am(aul = "b4afdcc6187b7ff0be213d659fd8694a", aum = 3)
    public boolean dyX;
    @C0064Am(aul = "b4afdcc6187b7ff0be213d659fd8694a", aum = -2)
    public long eLm;
    @C0064Am(aul = "b4afdcc6187b7ff0be213d659fd8694a", aum = -1)
    public byte eLo;
    @C0064Am(aul = "f972c66b9579a13af0ef9809b00073fa", aum = 0)

    /* renamed from: jS */
    public DatabaseCategory f5707jS;
    @C0064Am(aul = "462de04eca7320138c725d04397a509f", aum = 1)

    /* renamed from: jT */
    public C3438ra<TaikopediaEntry> f5708jT;
    @C0064Am(aul = "3af022392f37b08eb8b9bf8f5ba4ed73", aum = 2)

    /* renamed from: jU */
    public Asset f5709jU;
    @C0064Am(aul = "a352d22e13f4580021458934056314df", aum = 4)

    /* renamed from: jV */
    public float f5710jV;
    @C0064Am(aul = "f972c66b9579a13af0ef9809b00073fa", aum = -2)

    /* renamed from: jX */
    public long f5711jX;
    @C0064Am(aul = "462de04eca7320138c725d04397a509f", aum = -2)

    /* renamed from: jY */
    public long f5712jY;
    @C0064Am(aul = "3af022392f37b08eb8b9bf8f5ba4ed73", aum = -2)

    /* renamed from: jZ */
    public long f5713jZ;
    @C0064Am(aul = "a352d22e13f4580021458934056314df", aum = -2)

    /* renamed from: ka */
    public long f5714ka;
    @C0064Am(aul = "f972c66b9579a13af0ef9809b00073fa", aum = -1)

    /* renamed from: kc */
    public byte f5715kc;
    @C0064Am(aul = "462de04eca7320138c725d04397a509f", aum = -1)

    /* renamed from: kd */
    public byte f5716kd;
    @C0064Am(aul = "3af022392f37b08eb8b9bf8f5ba4ed73", aum = -1)

    /* renamed from: ke */
    public byte f5717ke;
    @C0064Am(aul = "a352d22e13f4580021458934056314df", aum = -1)

    /* renamed from: kf */
    public byte f5718kf;

    public C7034azv() {
    }

    public C7034azv(C1616Xf xf) {
        super(xf);
    }

    /* renamed from: c */
    public C5663aRz[] mo309c() {
        return MerchandiseType._m_fields;
    }

    /* renamed from: d */
    public void mo310d() {
        super.mo310d();
        this.f5707jS = null;
        this.f5708jT = null;
        this.f5709jU = null;
        this.dyX = false;
        this.f5710jV = 0.0f;
        this.f5711jX = 0;
        this.f5712jY = 0;
        this.f5713jZ = 0;
        this.eLm = 0;
        this.f5714ka = 0;
        this.f5715kc = 0;
        this.f5716kd = 0;
        this.f5717ke = 0;
        this.eLo = 0;
        this.f5718kf = 0;
    }
}
