package logic.data.mbean;

import game.script.ship.hazardshield.HazardShield;
import logic.baa.C1616Xf;
import logic.res.code.C5663aRz;
import p001a.C0064Am;

/* renamed from: a.rt */
public class C3520rt extends aEW {
    @C0064Am(aul = "bc9b01df3566b36da539180373bcafff", aum = -2)

    /* renamed from: dl */
    public long f9075dl;
    @C0064Am(aul = "bc9b01df3566b36da539180373bcafff", aum = -1)

    /* renamed from: ds */
    public byte f9076ds;
    @C0064Am(aul = "bc9b01df3566b36da539180373bcafff", aum = 0)

    /* renamed from: pZ */
    public HazardShield f9077pZ;

    public C3520rt() {
    }

    public C3520rt(C1616Xf xf) {
        super(xf);
    }

    /* renamed from: c */
    public C5663aRz[] mo309c() {
        return HazardShield.HullBlock._m_fields;
    }

    /* renamed from: d */
    public void mo310d() {
        super.mo310d();
        this.f9077pZ = null;
        this.f9075dl = 0;
        this.f9076ds = 0;
    }
}
