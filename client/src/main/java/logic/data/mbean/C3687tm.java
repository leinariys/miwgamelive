package logic.data.mbean;

import game.network.message.serializable.C2686iZ;
import game.network.message.serializable.C3438ra;
import game.script.avatar.AvatarManager;
import game.script.avatar.AvatarSector;
import game.script.avatar.body.AvatarAppearanceType;
import game.script.avatar.body.BodyPieceBank;
import game.script.avatar.cloth.Cloth;
import game.script.avatar.cloth.ClothCategory;
import logic.baa.C1616Xf;
import logic.res.code.C5663aRz;
import p001a.C0064Am;

import java.util.UUID;

/* renamed from: a.tm */
public class C3687tm extends C5292aDs {
    @C0064Am(aul = "ecb1d41dac8f44be92714d30b0d0c3c5", aum = 18)

    /* renamed from: bK */
    public UUID f9270bK;
    @C0064Am(aul = "c5800d4f750c0113596a45b64dc62b2f", aum = -2)
    public long blA;
    @C0064Am(aul = "42a18519718617f3343ee6f269a7d2a1", aum = -2)
    public long blB;
    @C0064Am(aul = "21ec2ccd755cf4eef5ef1954d9767d85", aum = -2)
    public long blC;
    @C0064Am(aul = "3a50bf464731c82afc464726131d1ec8", aum = -2)
    public long blD;
    @C0064Am(aul = "adc04dfd846ec9ef260682fac4bd4e71", aum = -2)
    public long blE;
    @C0064Am(aul = "7f7e36998d501aab57f26594a0db4c61", aum = -2)
    public long blF;
    @C0064Am(aul = "ea683526cd0249dec7eb49a22a63722f", aum = -2)
    public long blG;
    @C0064Am(aul = "e0be4d2af9d0f3cd2c7767163601d9bb", aum = -2)
    public long blH;
    @C0064Am(aul = "3a3ab552637e7abc7dfda36bb396c042", aum = -2)
    public long blI;
    @C0064Am(aul = "ef29e0f9f75a8a3c577d0efacbf2236d", aum = -2)
    public long blJ;
    @C0064Am(aul = "b5f460b5c29d59828d0a4115e1a8c1f5", aum = -1)
    public byte blK;
    @C0064Am(aul = "9482426c1ce216dd024e7d9216af4977", aum = -1)
    public byte blL;
    @C0064Am(aul = "3d2fc0d1abde58a8d4a5655a8ee8b5fc", aum = -1)
    public byte blM;
    @C0064Am(aul = "6bc0af67f21dbc8c4ac14ae1609fe9bc", aum = -1)
    public byte blN;
    @C0064Am(aul = "99e7352cb88cb1fb0682a48f840b9206", aum = -1)
    public byte blO;
    @C0064Am(aul = "b2fca0cb00158038a44772577a17336c", aum = -1)
    public byte blP;
    @C0064Am(aul = "ca6f29f9a9a2b48f5b4f43ab6c34b300", aum = -1)
    public byte blQ;
    @C0064Am(aul = "5b3cfb08f8f60989a038628b799f7087", aum = -1)
    public byte blR;
    @C0064Am(aul = "c5800d4f750c0113596a45b64dc62b2f", aum = -1)
    public byte blS;
    @C0064Am(aul = "42a18519718617f3343ee6f269a7d2a1", aum = -1)
    public byte blT;
    @C0064Am(aul = "21ec2ccd755cf4eef5ef1954d9767d85", aum = -1)
    public byte blU;
    @C0064Am(aul = "3a50bf464731c82afc464726131d1ec8", aum = -1)
    public byte blV;
    @C0064Am(aul = "adc04dfd846ec9ef260682fac4bd4e71", aum = -1)
    public byte blW;
    @C0064Am(aul = "7f7e36998d501aab57f26594a0db4c61", aum = -1)
    public byte blX;
    @C0064Am(aul = "ea683526cd0249dec7eb49a22a63722f", aum = -1)
    public byte blY;
    @C0064Am(aul = "e0be4d2af9d0f3cd2c7767163601d9bb", aum = -1)
    public byte blZ;
    @C0064Am(aul = "b5f460b5c29d59828d0a4115e1a8c1f5", aum = 0)
    public C2686iZ<Cloth> bla;
    @C0064Am(aul = "9482426c1ce216dd024e7d9216af4977", aum = 1)
    public C2686iZ<ClothCategory> blb;
    @C0064Am(aul = "3d2fc0d1abde58a8d4a5655a8ee8b5fc", aum = 2)
    public ClothCategory blc;
    @C0064Am(aul = "6bc0af67f21dbc8c4ac14ae1609fe9bc", aum = 3)
    public ClothCategory bld;
    @C0064Am(aul = "99e7352cb88cb1fb0682a48f840b9206", aum = 4)
    public C3438ra<AvatarAppearanceType> ble;
    @C0064Am(aul = "b2fca0cb00158038a44772577a17336c", aum = 5)
    public C2686iZ<AvatarSector> blf;
    @C0064Am(aul = "ca6f29f9a9a2b48f5b4f43ab6c34b300", aum = 6)
    public C2686iZ<BodyPieceBank> blg;
    @C0064Am(aul = "5b3cfb08f8f60989a038628b799f7087", aum = 7)
    public AvatarSector blh;
    @C0064Am(aul = "c5800d4f750c0113596a45b64dc62b2f", aum = 8)
    public AvatarSector bli;
    @C0064Am(aul = "42a18519718617f3343ee6f269a7d2a1", aum = 9)
    public AvatarSector blj;
    @C0064Am(aul = "21ec2ccd755cf4eef5ef1954d9767d85", aum = 10)
    public String blk;
    @C0064Am(aul = "3a50bf464731c82afc464726131d1ec8", aum = 11)
    public String bll;
    @C0064Am(aul = "adc04dfd846ec9ef260682fac4bd4e71", aum = 12)
    public Cloth blm;
    @C0064Am(aul = "7f7e36998d501aab57f26594a0db4c61", aum = 13)
    public AvatarManager.ColorManager bln;
    @C0064Am(aul = "ea683526cd0249dec7eb49a22a63722f", aum = 14)
    public float blo;
    @C0064Am(aul = "e0be4d2af9d0f3cd2c7767163601d9bb", aum = 15)
    public float blp;
    @C0064Am(aul = "3a3ab552637e7abc7dfda36bb396c042", aum = 16)
    public float blq;
    @C0064Am(aul = "ef29e0f9f75a8a3c577d0efacbf2236d", aum = 17)
    public float blr;
    @C0064Am(aul = "b5f460b5c29d59828d0a4115e1a8c1f5", aum = -2)
    public long bls;
    @C0064Am(aul = "9482426c1ce216dd024e7d9216af4977", aum = -2)
    public long blt;
    @C0064Am(aul = "3d2fc0d1abde58a8d4a5655a8ee8b5fc", aum = -2)
    public long blu;
    @C0064Am(aul = "6bc0af67f21dbc8c4ac14ae1609fe9bc", aum = -2)
    public long blv;
    @C0064Am(aul = "99e7352cb88cb1fb0682a48f840b9206", aum = -2)
    public long blw;
    @C0064Am(aul = "b2fca0cb00158038a44772577a17336c", aum = -2)
    public long blx;
    @C0064Am(aul = "ca6f29f9a9a2b48f5b4f43ab6c34b300", aum = -2)
    public long bly;
    @C0064Am(aul = "5b3cfb08f8f60989a038628b799f7087", aum = -2)
    public long blz;
    @C0064Am(aul = "3a3ab552637e7abc7dfda36bb396c042", aum = -1)
    public byte bma;
    @C0064Am(aul = "ef29e0f9f75a8a3c577d0efacbf2236d", aum = -1)
    public byte bmb;
    @C0064Am(aul = "5292b9c9a80ae92b26ec15933ad6fda6", aum = 19)
    public String handle;
    @C0064Am(aul = "ecb1d41dac8f44be92714d30b0d0c3c5", aum = -2)

    /* renamed from: oL */
    public long f9271oL;
    @C0064Am(aul = "ecb1d41dac8f44be92714d30b0d0c3c5", aum = -1)

    /* renamed from: oS */
    public byte f9272oS;
    @C0064Am(aul = "5292b9c9a80ae92b26ec15933ad6fda6", aum = -2)

    /* renamed from: ok */
    public long f9273ok;
    @C0064Am(aul = "5292b9c9a80ae92b26ec15933ad6fda6", aum = -1)

    /* renamed from: on */
    public byte f9274on;

    public C3687tm() {
    }

    public C3687tm(C1616Xf xf) {
        super(xf);
    }

    /* renamed from: c */
    public C5663aRz[] mo309c() {
        return AvatarManager._m_fields;
    }

    /* renamed from: d */
    public void mo310d() {
        super.mo310d();
        this.bla = null;
        this.blb = null;
        this.blc = null;
        this.bld = null;
        this.ble = null;
        this.blf = null;
        this.blg = null;
        this.blh = null;
        this.bli = null;
        this.blj = null;
        this.blk = null;
        this.bll = null;
        this.blm = null;
        this.bln = null;
        this.blo = 0.0f;
        this.blp = 0.0f;
        this.blq = 0.0f;
        this.blr = 0.0f;
        this.f9270bK = null;
        this.handle = null;
        this.bls = 0;
        this.blt = 0;
        this.blu = 0;
        this.blv = 0;
        this.blw = 0;
        this.blx = 0;
        this.bly = 0;
        this.blz = 0;
        this.blA = 0;
        this.blB = 0;
        this.blC = 0;
        this.blD = 0;
        this.blE = 0;
        this.blF = 0;
        this.blG = 0;
        this.blH = 0;
        this.blI = 0;
        this.blJ = 0;
        this.f9271oL = 0;
        this.f9273ok = 0;
        this.blK = 0;
        this.blL = 0;
        this.blM = 0;
        this.blN = 0;
        this.blO = 0;
        this.blP = 0;
        this.blQ = 0;
        this.blR = 0;
        this.blS = 0;
        this.blT = 0;
        this.blU = 0;
        this.blV = 0;
        this.blW = 0;
        this.blX = 0;
        this.blY = 0;
        this.blZ = 0;
        this.bma = 0;
        this.bmb = 0;
        this.f9272oS = 0;
        this.f9274on = 0;
    }
}
