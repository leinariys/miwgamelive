package logic.data.mbean;

import game.script.citizenship.inprovements.StorageImprovement;
import logic.baa.C1616Xf;
import logic.res.code.C5663aRz;
import p001a.C0064Am;

/* renamed from: a.aMT */
public class aMT extends C1344Tc {
    @C0064Am(aul = "195e1228a9e57b83c5d6a7396a81a172", aum = -2)

    /* renamed from: dl */
    public long f3353dl;
    @C0064Am(aul = "195e1228a9e57b83c5d6a7396a81a172", aum = -1)

    /* renamed from: ds */
    public byte f3354ds;
    @C0064Am(aul = "195e1228a9e57b83c5d6a7396a81a172", aum = 0)
    public StorageImprovement eXc;

    public aMT() {
    }

    public aMT(C1616Xf xf) {
        super(xf);
    }

    /* renamed from: c */
    public C5663aRz[] mo309c() {
        return StorageImprovement.StorageAmplifierAdapter._m_fields;
    }

    /* renamed from: d */
    public void mo310d() {
        super.mo310d();
        this.eXc = null;
        this.f3353dl = 0;
        this.f3354ds = 0;
    }
}
