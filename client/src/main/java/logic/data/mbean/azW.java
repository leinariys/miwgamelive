package logic.data.mbean;

import game.network.message.serializable.C2686iZ;
import game.script.damage.DamageType;
import game.script.resource.Asset;
import game.script.ship.Ship;
import game.script.ship.hazardshield.HazardShield;
import game.script.ship.hazardshield.HazardShieldAdapter;
import game.script.util.AdapterLikedList;
import logic.baa.C1616Xf;
import logic.res.code.C5663aRz;
import p001a.C0064Am;
import p001a.C3892wO;
import p001a.C6809auB;

/* renamed from: a.azW */
public class azW extends C3805vD {
    @C0064Am(aul = "61ba70f03952b1cd187eb56a5285d5b2", aum = -2)

    /* renamed from: YA */
    public long f5681YA;
    @C0064Am(aul = "d2c13266ed9998911c6b7b760cb47dda", aum = -2)

    /* renamed from: YB */
    public long f5682YB;
    @C0064Am(aul = "61ba70f03952b1cd187eb56a5285d5b2", aum = -1)

    /* renamed from: YG */
    public byte f5683YG;
    @C0064Am(aul = "d2c13266ed9998911c6b7b760cb47dda", aum = -1)

    /* renamed from: YH */
    public byte f5684YH;
    @C0064Am(aul = "90a2b58056892d219d57c5c224940bdb", aum = 4)
    public Asset aAd;
    @C0064Am(aul = "e577891f92dbdcaf69639f88fd0c48de", aum = 5)
    public Asset aAf;
    @C0064Am(aul = "ca8c0481d90f705cade9483c49ab8e08", aum = 12)
    public AdapterLikedList<HazardShieldAdapter> aOw;
    @C0064Am(aul = "ca8c0481d90f705cade9483c49ab8e08", aum = -2)
    public long aUE;
    @C0064Am(aul = "ca8c0481d90f705cade9483c49ab8e08", aum = -1)
    public byte aUH;
    @C0064Am(aul = "a5e9ae4063ad294bb14fa038e2ca7f67", aum = 11)
    public C6809auB.C1996a aZI;
    @C0064Am(aul = "7280aeff281ebeb301d0e0311e7f4f26", aum = 0)
    public C3892wO bbH;
    @C0064Am(aul = "6fa66f9269616690963811a1e73d66c2", aum = 1)
    public C2686iZ<DamageType> bbJ;
    @C0064Am(aul = "d2c13266ed9998911c6b7b760cb47dda", aum = 2)
    public float bbL;
    @C0064Am(aul = "61ba70f03952b1cd187eb56a5285d5b2", aum = 3)
    public float bbM;
    @C0064Am(aul = "90a2b58056892d219d57c5c224940bdb", aum = -2)
    public long cAB;
    @C0064Am(aul = "e577891f92dbdcaf69639f88fd0c48de", aum = -2)
    public long cAC;
    @C0064Am(aul = "90a2b58056892d219d57c5c224940bdb", aum = -1)
    public byte cAP;
    @C0064Am(aul = "e577891f92dbdcaf69639f88fd0c48de", aum = -1)
    public byte cAQ;
    @C0064Am(aul = "7280aeff281ebeb301d0e0311e7f4f26", aum = -2)
    public long gEW;
    @C0064Am(aul = "6fa66f9269616690963811a1e73d66c2", aum = -2)
    public long gEX;
    @C0064Am(aul = "7280aeff281ebeb301d0e0311e7f4f26", aum = -1)
    public byte gEY;
    @C0064Am(aul = "6fa66f9269616690963811a1e73d66c2", aum = -1)
    public byte gEZ;
    @C0064Am(aul = "3ba06f6cc30f912a27c93cf3e2243860", aum = 6)
    public HazardShield.ShieldBlock hcb;
    @C0064Am(aul = "58efc9890ba52903898dbe81c45faef2", aum = 7)
    public HazardShield.HullBlock hcc;
    @C0064Am(aul = "e871cad428abe047b53d1432da4a7b05", aum = 8)
    public Ship hcd;
    @C0064Am(aul = "882943d28f875ccb887ac4006692ee59", aum = 9)
    public HazardShield.InvulnerabilityEnterer hce;
    @C0064Am(aul = "4bcc3090a10c9a85fe9b11e62b06b1bc", aum = 10)
    public HazardShield.InvulnerabilityExiter hcf;
    @C0064Am(aul = "c20ee6a4f0275a2e65623ed6408a37b8", aum = 13)
    public C3892wO hcg;
    @C0064Am(aul = "3ba06f6cc30f912a27c93cf3e2243860", aum = -2)
    public long hch;
    @C0064Am(aul = "58efc9890ba52903898dbe81c45faef2", aum = -2)
    public long hci;
    @C0064Am(aul = "e871cad428abe047b53d1432da4a7b05", aum = -2)
    public long hcj;
    @C0064Am(aul = "882943d28f875ccb887ac4006692ee59", aum = -2)
    public long hck;
    @C0064Am(aul = "4bcc3090a10c9a85fe9b11e62b06b1bc", aum = -2)
    public long hcl;
    @C0064Am(aul = "c20ee6a4f0275a2e65623ed6408a37b8", aum = -2)
    public long hcm;
    @C0064Am(aul = "3ba06f6cc30f912a27c93cf3e2243860", aum = -1)
    public byte hcn;
    @C0064Am(aul = "58efc9890ba52903898dbe81c45faef2", aum = -1)
    public byte hco;
    @C0064Am(aul = "e871cad428abe047b53d1432da4a7b05", aum = -1)
    public byte hcp;
    @C0064Am(aul = "882943d28f875ccb887ac4006692ee59", aum = -1)
    public byte hcq;
    @C0064Am(aul = "4bcc3090a10c9a85fe9b11e62b06b1bc", aum = -1)
    public byte hcr;
    @C0064Am(aul = "c20ee6a4f0275a2e65623ed6408a37b8", aum = -1)
    public byte hcs;
    @C0064Am(aul = "a5e9ae4063ad294bb14fa038e2ca7f67", aum = -2)

    /* renamed from: ol */
    public long f5685ol;
    @C0064Am(aul = "a5e9ae4063ad294bb14fa038e2ca7f67", aum = -1)

    /* renamed from: oo */
    public byte f5686oo;

    public azW() {
    }

    public azW(C1616Xf xf) {
        super(xf);
    }

    /* renamed from: c */
    public C5663aRz[] mo309c() {
        return HazardShield._m_fields;
    }

    /* renamed from: d */
    public void mo310d() {
        super.mo310d();
        this.bbH = null;
        this.bbJ = null;
        this.bbL = 0.0f;
        this.bbM = 0.0f;
        this.aAd = null;
        this.aAf = null;
        this.hcb = null;
        this.hcc = null;
        this.hcd = null;
        this.hce = null;
        this.hcf = null;
        this.aZI = null;
        this.aOw = null;
        this.hcg = null;
        this.gEW = 0;
        this.gEX = 0;
        this.f5682YB = 0;
        this.f5681YA = 0;
        this.cAB = 0;
        this.cAC = 0;
        this.hch = 0;
        this.hci = 0;
        this.hcj = 0;
        this.hck = 0;
        this.hcl = 0;
        this.f5685ol = 0;
        this.aUE = 0;
        this.hcm = 0;
        this.gEY = 0;
        this.gEZ = 0;
        this.f5684YH = 0;
        this.f5683YG = 0;
        this.cAP = 0;
        this.cAQ = 0;
        this.hcn = 0;
        this.hco = 0;
        this.hcp = 0;
        this.hcq = 0;
        this.hcr = 0;
        this.f5686oo = 0;
        this.aUH = 0;
        this.hcs = 0;
    }
}
