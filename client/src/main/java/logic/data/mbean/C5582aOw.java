package logic.data.mbean;

import game.script.agent.Agent;
import logic.baa.C1616Xf;
import logic.res.code.C5663aRz;
import p001a.C0064Am;

import java.util.UUID;

/* renamed from: a.aOw  reason: case insensitive filesystem */
public class C5582aOw extends C2850lB {
    @C0064Am(aul = "c3d78a6e98a007893b927f8a82e65236", aum = 0)

    /* renamed from: bK */
    public UUID f3485bK;
    @C0064Am(aul = "ac34396843caad8e61ddd6435a79766f", aum = 1)
    public String handle;
    @C0064Am(aul = "c3d78a6e98a007893b927f8a82e65236", aum = -2)

    /* renamed from: oL */
    public long f3486oL;
    @C0064Am(aul = "c3d78a6e98a007893b927f8a82e65236", aum = -1)

    /* renamed from: oS */
    public byte f3487oS;
    @C0064Am(aul = "ac34396843caad8e61ddd6435a79766f", aum = -2)

    /* renamed from: ok */
    public long f3488ok;
    @C0064Am(aul = "ac34396843caad8e61ddd6435a79766f", aum = -1)

    /* renamed from: on */
    public byte f3489on;

    public C5582aOw() {
    }

    public C5582aOw(C1616Xf xf) {
        super(xf);
    }

    /* renamed from: c */
    public C5663aRz[] mo309c() {
        return Agent._m_fields;
    }

    /* renamed from: d */
    public void mo310d() {
        super.mo310d();
        this.f3485bK = null;
        this.handle = null;
        this.f3486oL = 0;
        this.f3488ok = 0;
        this.f3487oS = 0;
        this.f3489on = 0;
    }
}
