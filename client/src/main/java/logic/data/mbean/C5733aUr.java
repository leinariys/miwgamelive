package logic.data.mbean;

import game.network.message.serializable.C2686iZ;
import game.script.item.ModuleType;
import game.script.item.buff.module.AttributeBuffType;
import game.script.resource.Asset;
import logic.baa.C1616Xf;
import logic.res.code.C5663aRz;
import p001a.C0064Am;

/* renamed from: a.aUr  reason: case insensitive filesystem */
public class C5733aUr extends aTT {
    @C0064Am(aul = "667ea0aaf9b7c24aab4ca39b1668d067", aum = 0)
    public float bKI;
    @C0064Am(aul = "edb4bd494549767006cf5cbc96400f64", aum = 1)
    public float bKJ;
    @C0064Am(aul = "4d2211b4ffa8a2e43ed5b3119f51fd85", aum = 2)
    public float bKK;
    @C0064Am(aul = "6860c26c366a744264a08780426e2fda", aum = 3)
    public Asset bKL;
    @C0064Am(aul = "93e684948d1a2ac9e68c7877b0385042", aum = 4)
    public Asset bKM;
    @C0064Am(aul = "43c01a85efbb48bfeaf7966e1c8866be", aum = 5)
    public Asset bKN;
    @C0064Am(aul = "53af20313c0fcff9fb8e6751886fc3c1", aum = 6)
    public float bKO;
    @C0064Am(aul = "318dda89e756a47147dcc0f35e93a6b2", aum = 7)
    public C2686iZ<AttributeBuffType> bKP;
    @C0064Am(aul = "667ea0aaf9b7c24aab4ca39b1668d067", aum = -2)
    public long bKR;
    @C0064Am(aul = "edb4bd494549767006cf5cbc96400f64", aum = -2)
    public long bKS;
    @C0064Am(aul = "4d2211b4ffa8a2e43ed5b3119f51fd85", aum = -2)
    public long bKT;
    @C0064Am(aul = "6860c26c366a744264a08780426e2fda", aum = -2)
    public long bKU;
    @C0064Am(aul = "93e684948d1a2ac9e68c7877b0385042", aum = -2)
    public long bKV;
    @C0064Am(aul = "43c01a85efbb48bfeaf7966e1c8866be", aum = -2)
    public long bKW;
    @C0064Am(aul = "53af20313c0fcff9fb8e6751886fc3c1", aum = -2)
    public long bKX;
    @C0064Am(aul = "318dda89e756a47147dcc0f35e93a6b2", aum = -2)
    public long bKY;
    @C0064Am(aul = "667ea0aaf9b7c24aab4ca39b1668d067", aum = -1)
    public byte bLa;
    @C0064Am(aul = "edb4bd494549767006cf5cbc96400f64", aum = -1)
    public byte bLb;
    @C0064Am(aul = "4d2211b4ffa8a2e43ed5b3119f51fd85", aum = -1)
    public byte bLc;
    @C0064Am(aul = "6860c26c366a744264a08780426e2fda", aum = -1)
    public byte bLd;
    @C0064Am(aul = "93e684948d1a2ac9e68c7877b0385042", aum = -1)
    public byte bLe;
    @C0064Am(aul = "43c01a85efbb48bfeaf7966e1c8866be", aum = -1)
    public byte bLf;
    @C0064Am(aul = "53af20313c0fcff9fb8e6751886fc3c1", aum = -1)
    public byte bLg;
    @C0064Am(aul = "318dda89e756a47147dcc0f35e93a6b2", aum = -1)
    public byte bLh;

    public C5733aUr() {
    }

    public C5733aUr(C1616Xf xf) {
        super(xf);
    }

    /* renamed from: c */
    public C5663aRz[] mo309c() {
        return ModuleType._m_fields;
    }

    /* renamed from: d */
    public void mo310d() {
        super.mo310d();
        this.bKI = 0.0f;
        this.bKJ = 0.0f;
        this.bKK = 0.0f;
        this.bKL = null;
        this.bKM = null;
        this.bKN = null;
        this.bKO = 0.0f;
        this.bKP = null;
        this.bKR = 0;
        this.bKS = 0;
        this.bKT = 0;
        this.bKU = 0;
        this.bKV = 0;
        this.bKW = 0;
        this.bKX = 0;
        this.bKY = 0;
        this.bLa = 0;
        this.bLb = 0;
        this.bLc = 0;
        this.bLd = 0;
        this.bLe = 0;
        this.bLf = 0;
        this.bLg = 0;
        this.bLh = 0;
    }
}
