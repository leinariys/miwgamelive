package logic.data.mbean;

import game.network.message.serializable.C2686iZ;
import game.network.message.serializable.C3438ra;
import game.script.avatar.ColorWrapper;
import game.script.avatar.TextureGrid;
import game.script.avatar.body.AvatarAppearanceType;
import game.script.avatar.body.BodyPiece;
import game.script.avatar.body.TextureScheme;
import game.script.resource.Asset;
import logic.baa.C1616Xf;
import logic.res.code.C5663aRz;
import p001a.C0064Am;
import taikodom.infra.script.I18NString;

/* renamed from: a.aQS */
public class aQS extends C3905wZ {
    @C0064Am(aul = "4af52ab39cbe07793b992f8fc93605f2", aum = -2)

    /* renamed from: Ng */
    public long f3602Ng;
    @C0064Am(aul = "4af52ab39cbe07793b992f8fc93605f2", aum = -1)

    /* renamed from: Ni */
    public byte f3603Ni;
    @C0064Am(aul = "36bf10046b9c843df3dffedc2eb2dca8", aum = 9)

    /* renamed from: eB */
    public C3438ra<BodyPiece> f3604eB;
    @C0064Am(aul = "7ee55cd192cb274dfe0e96ecdc5c624b", aum = 10)

    /* renamed from: eD */
    public C3438ra<BodyPiece> f3605eD;
    @C0064Am(aul = "fc9abfc463b423644ee3d731435459d2", aum = 11)

    /* renamed from: eF */
    public C3438ra<BodyPiece> f3606eF;
    @C0064Am(aul = "0ed883d9de36633c2102567e96c278b6", aum = 12)

    /* renamed from: eH */
    public C3438ra<BodyPiece> f3607eH;
    @C0064Am(aul = "cd15f3fbc8b460d2c7eb347a106f3a7c", aum = 13)

    /* renamed from: eJ */
    public C3438ra<BodyPiece> f3608eJ;
    @C0064Am(aul = "6f69f908ec612db749a6506c86262dfa", aum = 14)

    /* renamed from: eL */
    public C2686iZ<BodyPiece> f3609eL;
    @C0064Am(aul = "69b3bbdecccc07644140a2462f8f3636", aum = 15)

    /* renamed from: eN */
    public C3438ra<TextureScheme> f3610eN;
    @C0064Am(aul = "a9f014ae80105979d77305ff6575d402", aum = 16)

    /* renamed from: eP */
    public C3438ra<TextureGrid> f3611eP;
    @C0064Am(aul = "8e7adca4a8cbba6790df15fbc3c3f4ba", aum = 17)

    /* renamed from: eR */
    public C3438ra<ColorWrapper> f3612eR;
    @C0064Am(aul = "8021c3a94c5ec04b8aa00f76502396ca", aum = 18)

    /* renamed from: eT */
    public C3438ra<Asset> f3613eT;
    @C0064Am(aul = "8d2be29949cea688fabdce7905780a16", aum = 19)

    /* renamed from: eV */
    public float f3614eV;
    @C0064Am(aul = "d5972b02449e119291d21d53418789c3", aum = 20)

    /* renamed from: eX */
    public Asset f3615eX;
    @C0064Am(aul = "69b015c5d53692f968ba626df6add27b", aum = 0)

    /* renamed from: el */
    public I18NString f3616el;
    @C0064Am(aul = "5587f366e8a04c27d6dd9ea4e30b769b", aum = 2)

    /* renamed from: eo */
    public boolean f3617eo;
    @C0064Am(aul = "e9dd981fef64a6497362e91e486fa4a7", aum = -2)
    public long eob;
    @C0064Am(aul = "e9dd981fef64a6497362e91e486fa4a7", aum = -1)
    public byte eoi;
    @C0064Am(aul = "d132bc688f63676f38578bfbca871278", aum = 4)

    /* renamed from: er */
    public Asset f3618er;
    @C0064Am(aul = "568d9360fbf9a2c5057471ad826de7dc", aum = 5)

    /* renamed from: et */
    public Asset f3619et;
    @C0064Am(aul = "ea1612c823e2ce29cdd59f3b43010e27", aum = 6)

    /* renamed from: ev */
    public Asset f3620ev;
    @C0064Am(aul = "e61b086e5406c3f5ff2689fe64dd8585", aum = 7)

    /* renamed from: ex */
    public C3438ra<BodyPiece> f3621ex;
    @C0064Am(aul = "aa03b69efe7006d4934b082fa1f945eb", aum = 8)

    /* renamed from: ez */
    public C3438ra<BodyPiece> f3622ez;
    @C0064Am(aul = "69b015c5d53692f968ba626df6add27b", aum = -1)
    public byte hGE;
    @C0064Am(aul = "5587f366e8a04c27d6dd9ea4e30b769b", aum = -1)
    public byte hGF;
    @C0064Am(aul = "d132bc688f63676f38578bfbca871278", aum = -1)
    public byte hGG;
    @C0064Am(aul = "568d9360fbf9a2c5057471ad826de7dc", aum = -1)
    public byte hGH;
    @C0064Am(aul = "ea1612c823e2ce29cdd59f3b43010e27", aum = -1)
    public byte hGI;
    @C0064Am(aul = "e61b086e5406c3f5ff2689fe64dd8585", aum = -1)
    public byte hGJ;
    @C0064Am(aul = "aa03b69efe7006d4934b082fa1f945eb", aum = -1)
    public byte hGK;
    @C0064Am(aul = "36bf10046b9c843df3dffedc2eb2dca8", aum = -1)
    public byte hGL;
    @C0064Am(aul = "7ee55cd192cb274dfe0e96ecdc5c624b", aum = -1)
    public byte hGM;
    @C0064Am(aul = "fc9abfc463b423644ee3d731435459d2", aum = -1)
    public byte hGN;
    @C0064Am(aul = "0ed883d9de36633c2102567e96c278b6", aum = -1)
    public byte hGO;
    @C0064Am(aul = "cd15f3fbc8b460d2c7eb347a106f3a7c", aum = -1)
    public byte hGP;
    @C0064Am(aul = "6f69f908ec612db749a6506c86262dfa", aum = -1)
    public byte hGQ;
    @C0064Am(aul = "69b3bbdecccc07644140a2462f8f3636", aum = -1)
    public byte hGR;
    @C0064Am(aul = "a9f014ae80105979d77305ff6575d402", aum = -1)
    public byte hGS;
    @C0064Am(aul = "8e7adca4a8cbba6790df15fbc3c3f4ba", aum = -1)
    public byte hGT;
    @C0064Am(aul = "8021c3a94c5ec04b8aa00f76502396ca", aum = -1)
    public byte hGU;
    @C0064Am(aul = "8d2be29949cea688fabdce7905780a16", aum = -1)
    public byte hGV;
    @C0064Am(aul = "d5972b02449e119291d21d53418789c3", aum = -1)
    public byte hGW;
    @C0064Am(aul = "69b015c5d53692f968ba626df6add27b", aum = -2)
    public long hGa;
    @C0064Am(aul = "5587f366e8a04c27d6dd9ea4e30b769b", aum = -2)
    public long hGb;
    @C0064Am(aul = "d132bc688f63676f38578bfbca871278", aum = -2)
    public long hGc;
    @C0064Am(aul = "568d9360fbf9a2c5057471ad826de7dc", aum = -2)
    public long hGd;
    @C0064Am(aul = "ea1612c823e2ce29cdd59f3b43010e27", aum = -2)
    public long hGe;
    @C0064Am(aul = "e61b086e5406c3f5ff2689fe64dd8585", aum = -2)
    public long hGf;
    @C0064Am(aul = "aa03b69efe7006d4934b082fa1f945eb", aum = -2)
    public long hGg;
    @C0064Am(aul = "36bf10046b9c843df3dffedc2eb2dca8", aum = -2)
    public long hGh;
    @C0064Am(aul = "7ee55cd192cb274dfe0e96ecdc5c624b", aum = -2)
    public long hGi;
    @C0064Am(aul = "fc9abfc463b423644ee3d731435459d2", aum = -2)
    public long hGj;
    @C0064Am(aul = "0ed883d9de36633c2102567e96c278b6", aum = -2)
    public long hGk;
    @C0064Am(aul = "cd15f3fbc8b460d2c7eb347a106f3a7c", aum = -2)
    public long hGl;
    @C0064Am(aul = "6f69f908ec612db749a6506c86262dfa", aum = -2)
    public long hGm;
    @C0064Am(aul = "69b3bbdecccc07644140a2462f8f3636", aum = -2)
    public long hGn;
    @C0064Am(aul = "a9f014ae80105979d77305ff6575d402", aum = -2)
    public long hGo;
    @C0064Am(aul = "8e7adca4a8cbba6790df15fbc3c3f4ba", aum = -2)
    public long hGp;
    @C0064Am(aul = "8021c3a94c5ec04b8aa00f76502396ca", aum = -2)
    public long hGq;
    @C0064Am(aul = "8d2be29949cea688fabdce7905780a16", aum = -2)
    public long hGr;
    @C0064Am(aul = "d5972b02449e119291d21d53418789c3", aum = -2)
    public long hGs;
    @C0064Am(aul = "e9dd981fef64a6497362e91e486fa4a7", aum = 3)
    public String prefix;
    @C0064Am(aul = "4af52ab39cbe07793b992f8fc93605f2", aum = 1)
    public boolean restricted;

    public aQS() {
    }

    public aQS(C1616Xf xf) {
        super(xf);
    }

    /* renamed from: c */
    public C5663aRz[] mo309c() {
        return AvatarAppearanceType._m_fields;
    }

    /* renamed from: d */
    public void mo310d() {
        super.mo310d();
        this.f3616el = null;
        this.restricted = false;
        this.f3617eo = false;
        this.prefix = null;
        this.f3618er = null;
        this.f3619et = null;
        this.f3620ev = null;
        this.f3621ex = null;
        this.f3622ez = null;
        this.f3604eB = null;
        this.f3605eD = null;
        this.f3606eF = null;
        this.f3607eH = null;
        this.f3608eJ = null;
        this.f3609eL = null;
        this.f3610eN = null;
        this.f3611eP = null;
        this.f3612eR = null;
        this.f3613eT = null;
        this.f3614eV = 0.0f;
        this.f3615eX = null;
        this.hGa = 0;
        this.f3602Ng = 0;
        this.hGb = 0;
        this.eob = 0;
        this.hGc = 0;
        this.hGd = 0;
        this.hGe = 0;
        this.hGf = 0;
        this.hGg = 0;
        this.hGh = 0;
        this.hGi = 0;
        this.hGj = 0;
        this.hGk = 0;
        this.hGl = 0;
        this.hGm = 0;
        this.hGn = 0;
        this.hGo = 0;
        this.hGp = 0;
        this.hGq = 0;
        this.hGr = 0;
        this.hGs = 0;
        this.hGE = 0;
        this.f3603Ni = 0;
        this.hGF = 0;
        this.eoi = 0;
        this.hGG = 0;
        this.hGH = 0;
        this.hGI = 0;
        this.hGJ = 0;
        this.hGK = 0;
        this.hGL = 0;
        this.hGM = 0;
        this.hGN = 0;
        this.hGO = 0;
        this.hGP = 0;
        this.hGQ = 0;
        this.hGR = 0;
        this.hGS = 0;
        this.hGT = 0;
        this.hGU = 0;
        this.hGV = 0;
        this.hGW = 0;
    }
}
