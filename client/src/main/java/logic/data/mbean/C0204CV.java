package logic.data.mbean;

import game.geometry.Vec3d;
import game.script.mission.scripting.AsteroidLootItemMissionScript;
import game.script.missiontemplate.WaypointDat;
import logic.baa.C1616Xf;
import logic.res.code.C5663aRz;
import p001a.C0064Am;

/* renamed from: a.CV */
public class C0204CV extends aMY {
    @C0064Am(aul = "f3892782435fa34dbc6a32a62241656e", aum = -2)
    public long aLK;
    @C0064Am(aul = "f3892782435fa34dbc6a32a62241656e", aum = -1)
    public byte aLP;
    @C0064Am(aul = "f9dd54890feca18b14e5428f7f780f37", aum = 1)
    public WaypointDat bHY;
    @C0064Am(aul = "fceb2e4ad4c34359f3ad39c6b856f4be", aum = -2)
    public long cCA;
    @C0064Am(aul = "f9dd54890feca18b14e5428f7f780f37", aum = -2)
    public long cCB;
    @C0064Am(aul = "9bfe8232dec09ff2e63266023383d580", aum = -2)
    public long cCC;
    @C0064Am(aul = "fceb2e4ad4c34359f3ad39c6b856f4be", aum = -1)
    public byte cCD;
    @C0064Am(aul = "f9dd54890feca18b14e5428f7f780f37", aum = -1)
    public byte cCE;
    @C0064Am(aul = "9bfe8232dec09ff2e63266023383d580", aum = -1)
    public byte cCF;
    @C0064Am(aul = "fceb2e4ad4c34359f3ad39c6b856f4be", aum = 0)
    public int cCx;
    @C0064Am(aul = "9bfe8232dec09ff2e63266023383d580", aum = 2)
    public WaypointDat cCy;
    @C0064Am(aul = "f3892782435fa34dbc6a32a62241656e", aum = 3)
    public Vec3d cCz;

    public C0204CV() {
    }

    public C0204CV(C1616Xf xf) {
        super(xf);
    }

    /* renamed from: c */
    public C5663aRz[] mo309c() {
        return AsteroidLootItemMissionScript._m_fields;
    }

    /* renamed from: d */
    public void mo310d() {
        super.mo310d();
        this.cCx = 0;
        this.bHY = null;
        this.cCy = null;
        this.cCz = null;
        this.cCA = 0;
        this.cCB = 0;
        this.cCC = 0;
        this.aLK = 0;
        this.cCD = 0;
        this.cCE = 0;
        this.cCF = 0;
        this.aLP = 0;
    }
}
