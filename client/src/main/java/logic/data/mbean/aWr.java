package logic.data.mbean;

import game.geometry.Vec3d;
import game.network.message.serializable.C3438ra;
import game.script.mission.scripting.ReconMissionScript;
import game.script.missiontemplate.ReconWaypointDat;
import game.script.ship.Ship;
import logic.baa.C1616Xf;
import logic.res.code.C5663aRz;
import p001a.C0064Am;

/* renamed from: a.aWr */
public class aWr extends aMY {
    @C0064Am(aul = "10efdf8c540e8dd8a0c7159c439eb2f4", aum = -2)
    public long aLK;
    @C0064Am(aul = "10efdf8c540e8dd8a0c7159c439eb2f4", aum = -1)
    public byte aLP;
    @C0064Am(aul = "93c58966bbba2bbe3d320877c70966a7", aum = -2)
    public long cCA;
    @C0064Am(aul = "25021a262fa714df31d48b746ec01254", aum = -2)
    public long cCB;
    @C0064Am(aul = "9be145538101ec508a5dafc0cbab708f", aum = -2)
    public long cCC;
    @C0064Am(aul = "93c58966bbba2bbe3d320877c70966a7", aum = -1)
    public byte cCD;
    @C0064Am(aul = "25021a262fa714df31d48b746ec01254", aum = -1)
    public byte cCE;
    @C0064Am(aul = "9be145538101ec508a5dafc0cbab708f", aum = -1)
    public byte cCF;
    @C0064Am(aul = "93c58966bbba2bbe3d320877c70966a7", aum = 1)
    public int cCx;
    @C0064Am(aul = "10efdf8c540e8dd8a0c7159c439eb2f4", aum = 3)
    public Vec3d cCz;
    @C0064Am(aul = "8565d8c61f962da2f687ff9e42f0a64b", aum = 6)
    public int ddL;
    @C0064Am(aul = "8565d8c61f962da2f687ff9e42f0a64b", aum = -2)
    public long fuk;
    @C0064Am(aul = "8565d8c61f962da2f687ff9e42f0a64b", aum = -1)
    public byte fun;
    @C0064Am(aul = "9be145538101ec508a5dafc0cbab708f", aum = 0)
    public ReconWaypointDat gWR;
    @C0064Am(aul = "25021a262fa714df31d48b746ec01254", aum = 2)
    public ReconWaypointDat gWU;
    @C0064Am(aul = "e278a7465b44cc346f952c5908165ff9", aum = 5)
    public int gWV;
    @C0064Am(aul = "eb200098a5302c2cf9fc7fd99564853d", aum = 7)
    public C3438ra<Ship> gWX;
    @C0064Am(aul = "0ccaf0816e734892060b6edf0375dff6", aum = 8)
    public int gWZ;
    @C0064Am(aul = "eb200098a5302c2cf9fc7fd99564853d", aum = -2)
    public long ilh;
    @C0064Am(aul = "eb200098a5302c2cf9fc7fd99564853d", aum = -1)
    public byte ili;
    @C0064Am(aul = "e278a7465b44cc346f952c5908165ff9", aum = -2)
    public long jgh;
    @C0064Am(aul = "0ccaf0816e734892060b6edf0375dff6", aum = -2)
    public long jgi;
    @C0064Am(aul = "e278a7465b44cc346f952c5908165ff9", aum = -1)
    public byte jgj;
    @C0064Am(aul = "0ccaf0816e734892060b6edf0375dff6", aum = -1)
    public byte jgk;
    @C0064Am(aul = "ca92e3bccce713df87fa393e6e7b5a9d", aum = 4)

    /* renamed from: vV */
    public int f4022vV;
    @C0064Am(aul = "ca92e3bccce713df87fa393e6e7b5a9d", aum = -2)

    /* renamed from: wd */
    public long f4023wd;
    @C0064Am(aul = "ca92e3bccce713df87fa393e6e7b5a9d", aum = -1)

    /* renamed from: wl */
    public byte f4024wl;

    public aWr() {
    }

    public aWr(C1616Xf xf) {
        super(xf);
    }

    /* renamed from: c */
    public C5663aRz[] mo309c() {
        return ReconMissionScript._m_fields;
    }

    /* renamed from: d */
    public void mo310d() {
        super.mo310d();
        this.gWR = null;
        this.cCx = 0;
        this.gWU = null;
        this.cCz = null;
        this.f4022vV = 0;
        this.gWV = 0;
        this.ddL = 0;
        this.gWX = null;
        this.gWZ = 0;
        this.cCC = 0;
        this.cCA = 0;
        this.cCB = 0;
        this.aLK = 0;
        this.f4023wd = 0;
        this.jgh = 0;
        this.fuk = 0;
        this.ilh = 0;
        this.jgi = 0;
        this.cCF = 0;
        this.cCD = 0;
        this.cCE = 0;
        this.aLP = 0;
        this.f4024wl = 0;
        this.jgj = 0;
        this.fun = 0;
        this.ili = 0;
        this.jgk = 0;
    }
}
