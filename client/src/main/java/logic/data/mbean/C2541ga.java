package logic.data.mbean;

import game.script.mission.scripting.AsteroidLootItemMissionScript;
import logic.baa.C1616Xf;
import logic.res.code.C5663aRz;
import p001a.C0064Am;

/* renamed from: a.ga */
public class C2541ga extends C2955mD {
    @C0064Am(aul = "db559f08bc31539ab12aa59c5768dca7", aum = 0)

    /* renamed from: OH */
    public AsteroidLootItemMissionScript f7672OH;
    @C0064Am(aul = "db559f08bc31539ab12aa59c5768dca7", aum = -2)

    /* renamed from: dl */
    public long f7673dl;
    @C0064Am(aul = "db559f08bc31539ab12aa59c5768dca7", aum = -1)

    /* renamed from: ds */
    public byte f7674ds;

    public C2541ga() {
    }

    public C2541ga(C1616Xf xf) {
        super(xf);
    }

    /* renamed from: c */
    public C5663aRz[] mo309c() {
        return AsteroidLootItemMissionScript.StateA._m_fields;
    }

    /* renamed from: d */
    public void mo310d() {
        super.mo310d();
        this.f7672OH = null;
        this.f7673dl = 0;
        this.f7674ds = 0;
    }
}
