package logic.data.mbean;

import game.network.message.serializable.C3438ra;
import game.script.avatar.cloth.Cloth;
import game.script.item.WeaponType;
import game.script.progression.MPMultiplier;
import game.script.progression.ProgressionCareer;
import game.script.progression.ProgressionLineType;
import game.script.ship.ShipType;
import game.script.ship.Station;
import logic.baa.C1616Xf;
import logic.res.code.C5663aRz;
import p001a.C0064Am;
import taikodom.infra.script.I18NString;

import java.util.UUID;

/* renamed from: a.nB */
public class C3027nB extends C3805vD {
    @C0064Am(aul = "d08561ec8fa49e372d81c95f0ff48bca", aum = 2)

    /* renamed from: RG */
    public Station f8682RG;
    @C0064Am(aul = "f7ee4ca1ff596d6af6bfa15df4ec9d42", aum = 4)
    public C3438ra<WeaponType> aIA;
    @C0064Am(aul = "1111c80b8a768c5296a7fb2b0e544ed8", aum = 5)
    public MPMultiplier aIB;
    @C0064Am(aul = "c75bb8860ed552ae6e6341938ff30d36", aum = 6)
    public C3438ra<ProgressionLineType> aIC;
    @C0064Am(aul = "07675bda06c93ab7cdcfb190f90a5c2a", aum = 7)
    public C3438ra<ProgressionLineType> aID;
    @C0064Am(aul = "28ea516e3aec04ac9e9035b7a0c3a627", aum = 9)
    public Cloth aIE;
    @C0064Am(aul = "cdfead3a1a0868c8e513c18ca5df2367", aum = 10)
    public String aIF;
    @C0064Am(aul = "35e8e483aeddfbd80eb07f388220b1d7", aum = 11)
    public int aIG;
    @C0064Am(aul = "32c60747b634cf66c4295d0e2d325302", aum = 12)
    public int aIH;
    @C0064Am(aul = "1db747889b79f7dde4f4c0913fca7d98", aum = 13)
    public int aII;
    @C0064Am(aul = "f80ddd73e93ab9d4fc0d6dc42c087fb0", aum = 14)
    public int aIJ;
    @C0064Am(aul = "d08561ec8fa49e372d81c95f0ff48bca", aum = -2)
    public long aIK;
    @C0064Am(aul = "d93bdd6bd69ad95e05adf923815cbb2a", aum = -2)
    public long aIL;
    @C0064Am(aul = "f7ee4ca1ff596d6af6bfa15df4ec9d42", aum = -2)
    public long aIM;
    @C0064Am(aul = "c75bb8860ed552ae6e6341938ff30d36", aum = -2)
    public long aIN;
    @C0064Am(aul = "07675bda06c93ab7cdcfb190f90a5c2a", aum = -2)
    public long aIO;
    @C0064Am(aul = "28ea516e3aec04ac9e9035b7a0c3a627", aum = -2)
    public long aIP;
    @C0064Am(aul = "cdfead3a1a0868c8e513c18ca5df2367", aum = -2)
    public long aIQ;
    @C0064Am(aul = "35e8e483aeddfbd80eb07f388220b1d7", aum = -2)
    public long aIR;
    @C0064Am(aul = "32c60747b634cf66c4295d0e2d325302", aum = -2)
    public long aIS;
    @C0064Am(aul = "1db747889b79f7dde4f4c0913fca7d98", aum = -2)
    public long aIT;
    @C0064Am(aul = "f80ddd73e93ab9d4fc0d6dc42c087fb0", aum = -2)
    public long aIU;
    @C0064Am(aul = "d08561ec8fa49e372d81c95f0ff48bca", aum = -1)
    public byte aIV;
    @C0064Am(aul = "d93bdd6bd69ad95e05adf923815cbb2a", aum = -1)
    public byte aIW;
    @C0064Am(aul = "f7ee4ca1ff596d6af6bfa15df4ec9d42", aum = -1)
    public byte aIX;
    @C0064Am(aul = "c75bb8860ed552ae6e6341938ff30d36", aum = -1)
    public byte aIY;
    @C0064Am(aul = "07675bda06c93ab7cdcfb190f90a5c2a", aum = -1)
    public byte aIZ;
    @C0064Am(aul = "d93bdd6bd69ad95e05adf923815cbb2a", aum = 3)
    public C3438ra<ShipType> aIz;
    @C0064Am(aul = "28ea516e3aec04ac9e9035b7a0c3a627", aum = -1)
    public byte aJa;
    @C0064Am(aul = "cdfead3a1a0868c8e513c18ca5df2367", aum = -1)
    public byte aJb;
    @C0064Am(aul = "35e8e483aeddfbd80eb07f388220b1d7", aum = -1)
    public byte aJc;
    @C0064Am(aul = "32c60747b634cf66c4295d0e2d325302", aum = -1)
    public byte aJd;
    @C0064Am(aul = "1db747889b79f7dde4f4c0913fca7d98", aum = -1)
    public byte aJe;
    @C0064Am(aul = "f80ddd73e93ab9d4fc0d6dc42c087fb0", aum = -1)
    public byte aJf;
    @C0064Am(aul = "1111c80b8a768c5296a7fb2b0e544ed8", aum = -2)
    public long asr;
    @C0064Am(aul = "1111c80b8a768c5296a7fb2b0e544ed8", aum = -1)
    public byte asw;
    @C0064Am(aul = "c8669be0bc35bd0b2dd856e85ec2ed09", aum = 15)

    /* renamed from: bK */
    public UUID f8683bK;
    @C0064Am(aul = "028a2b5f2c90dbd9d0460ab86c67aca6", aum = 8)
    public String handle;
    @C0064Am(aul = "4037e155308a21510e570d182e47b2fb", aum = 1)

    /* renamed from: nh */
    public I18NString f8684nh;
    @C0064Am(aul = "f4be6669de03d05c262115d8ba262601", aum = 0)

    /* renamed from: ni */
    public I18NString f8685ni;
    @C0064Am(aul = "4037e155308a21510e570d182e47b2fb", aum = -2)

    /* renamed from: nk */
    public long f8686nk;
    @C0064Am(aul = "f4be6669de03d05c262115d8ba262601", aum = -2)

    /* renamed from: nl */
    public long f8687nl;
    @C0064Am(aul = "4037e155308a21510e570d182e47b2fb", aum = -1)

    /* renamed from: nn */
    public byte f8688nn;
    @C0064Am(aul = "f4be6669de03d05c262115d8ba262601", aum = -1)

    /* renamed from: no */
    public byte f8689no;
    @C0064Am(aul = "c8669be0bc35bd0b2dd856e85ec2ed09", aum = -2)

    /* renamed from: oL */
    public long f8690oL;
    @C0064Am(aul = "c8669be0bc35bd0b2dd856e85ec2ed09", aum = -1)

    /* renamed from: oS */
    public byte f8691oS;
    @C0064Am(aul = "028a2b5f2c90dbd9d0460ab86c67aca6", aum = -2)

    /* renamed from: ok */
    public long f8692ok;
    @C0064Am(aul = "028a2b5f2c90dbd9d0460ab86c67aca6", aum = -1)

    /* renamed from: on */
    public byte f8693on;

    public C3027nB() {
    }

    public C3027nB(C1616Xf xf) {
        super(xf);
    }

    /* renamed from: c */
    public C5663aRz[] mo309c() {
        return ProgressionCareer._m_fields;
    }

    /* renamed from: d */
    public void mo310d() {
        super.mo310d();
        this.f8685ni = null;
        this.f8684nh = null;
        this.f8682RG = null;
        this.aIz = null;
        this.aIA = null;
        this.aIB = null;
        this.aIC = null;
        this.aID = null;
        this.handle = null;
        this.aIE = null;
        this.aIF = null;
        this.aIG = 0;
        this.aIH = 0;
        this.aII = 0;
        this.aIJ = 0;
        this.f8683bK = null;
        this.f8687nl = 0;
        this.f8686nk = 0;
        this.aIK = 0;
        this.aIL = 0;
        this.aIM = 0;
        this.asr = 0;
        this.aIN = 0;
        this.aIO = 0;
        this.f8692ok = 0;
        this.aIP = 0;
        this.aIQ = 0;
        this.aIR = 0;
        this.aIS = 0;
        this.aIT = 0;
        this.aIU = 0;
        this.f8690oL = 0;
        this.f8689no = 0;
        this.f8688nn = 0;
        this.aIV = 0;
        this.aIW = 0;
        this.aIX = 0;
        this.asw = 0;
        this.aIY = 0;
        this.aIZ = 0;
        this.f8693on = 0;
        this.aJa = 0;
        this.aJb = 0;
        this.aJc = 0;
        this.aJd = 0;
        this.aJe = 0;
        this.aJf = 0;
        this.f8691oS = 0;
    }
}
