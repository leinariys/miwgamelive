package logic.data.mbean;

import game.network.message.serializable.C3438ra;
import game.script.ship.Outpost;
import game.script.storage.OutpostStorage;
import game.script.storage.StorageTransaction;
import logic.baa.C1616Xf;
import logic.res.code.C5663aRz;
import p001a.C0064Am;

/* renamed from: a.Zc */
public class C1733Zc extends aAL {
    @C0064Am(aul = "962818fbc9ccc1b9b25613f85e64117f", aum = 1)
    public Outpost baT;
    @C0064Am(aul = "962818fbc9ccc1b9b25613f85e64117f", aum = -2)
    public long baX;
    @C0064Am(aul = "962818fbc9ccc1b9b25613f85e64117f", aum = -1)
    public byte bbb;
    @C0064Am(aul = "64d2d9e5381429fecf044c88ebdad275", aum = 0)
    public float dfj;
    @C0064Am(aul = "84c9c05efa302e3b90fb5df4cf38bb43", aum = 2)
    public C3438ra<StorageTransaction> dfm;
    @C0064Am(aul = "6409bc61b3bf330005ee0bbae10a7793", aum = 3)
    public OutpostStorage.TransactionCleaner dfo;
    @C0064Am(aul = "6409bc61b3bf330005ee0bbae10a7793", aum = -2)
    public long dgf;
    @C0064Am(aul = "6409bc61b3bf330005ee0bbae10a7793", aum = -1)
    public byte dgj;
    @C0064Am(aul = "64d2d9e5381429fecf044c88ebdad275", aum = -2)
    public long eNe;
    @C0064Am(aul = "84c9c05efa302e3b90fb5df4cf38bb43", aum = -2)
    public long eNf;
    @C0064Am(aul = "64d2d9e5381429fecf044c88ebdad275", aum = -1)
    public byte eNg;
    @C0064Am(aul = "84c9c05efa302e3b90fb5df4cf38bb43", aum = -1)
    public byte eNh;

    public C1733Zc() {
    }

    public C1733Zc(C1616Xf xf) {
        super(xf);
    }

    /* renamed from: c */
    public C5663aRz[] mo309c() {
        return OutpostStorage._m_fields;
    }

    /* renamed from: d */
    public void mo310d() {
        super.mo310d();
        this.dfj = 0.0f;
        this.baT = null;
        this.dfm = null;
        this.dfo = null;
        this.eNe = 0;
        this.baX = 0;
        this.eNf = 0;
        this.dgf = 0;
        this.eNg = 0;
        this.bbb = 0;
        this.eNh = 0;
        this.dgj = 0;
    }
}
