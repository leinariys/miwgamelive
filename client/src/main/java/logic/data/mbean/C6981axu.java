package logic.data.mbean;

import game.script.item.buff.amplifier.ShipAmplifier;
import logic.baa.C1616Xf;
import logic.res.code.C5663aRz;
import p001a.C0064Am;

/* renamed from: a.axu  reason: case insensitive filesystem */
public class C6981axu extends C6557apJ {
    @C0064Am(aul = "6fc18ad4e45dd49f8e1cbfc6444be356", aum = -2)

    /* renamed from: dl */
    public long f5599dl;
    @C0064Am(aul = "6fc18ad4e45dd49f8e1cbfc6444be356", aum = -1)

    /* renamed from: ds */
    public byte f5600ds;
    @C0064Am(aul = "6fc18ad4e45dd49f8e1cbfc6444be356", aum = 0)
    public ShipAmplifier eoy;

    public C6981axu() {
    }

    public C6981axu(C1616Xf xf) {
        super(xf);
    }

    /* renamed from: c */
    public C5663aRz[] mo309c() {
        return ShipAmplifier.ShipAmplifierAdapter._m_fields;
    }

    /* renamed from: d */
    public void mo310d() {
        super.mo310d();
        this.eoy = null;
        this.f5599dl = 0;
        this.f5600ds = 0;
    }
}
