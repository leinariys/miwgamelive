package logic.data.mbean;

import game.script.mission.actions.MissionAction;
import logic.baa.C1616Xf;
import logic.res.code.C5663aRz;
import p001a.C0064Am;

import java.util.UUID;

/* renamed from: a.fv */
public class C2503fv extends C3805vD {
    @C0064Am(aul = "98f6a727fc8e429cb519219692e436f7", aum = 0)

    /* renamed from: Kt */
    public MissionAction.C2210a f7485Kt;
    @C0064Am(aul = "2ae96bac2035c4b17e0066f40c0b3b9d", aum = 2)

    /* renamed from: Ku */
    public boolean f7486Ku;
    @C0064Am(aul = "98f6a727fc8e429cb519219692e436f7", aum = -2)

    /* renamed from: Kv */
    public long f7487Kv;
    @C0064Am(aul = "2ae96bac2035c4b17e0066f40c0b3b9d", aum = -2)

    /* renamed from: Kw */
    public long f7488Kw;
    @C0064Am(aul = "98f6a727fc8e429cb519219692e436f7", aum = -1)

    /* renamed from: Kx */
    public byte f7489Kx;
    @C0064Am(aul = "2ae96bac2035c4b17e0066f40c0b3b9d", aum = -1)

    /* renamed from: Ky */
    public byte f7490Ky;
    @C0064Am(aul = "f7bb964d95fdee7f4b7f0c6f4aff4656", aum = 3)

    /* renamed from: bK */
    public UUID f7491bK;
    @C0064Am(aul = "7f838fe1cde12da4ce5f11416cb09761", aum = 1)
    public String handle;
    @C0064Am(aul = "f7bb964d95fdee7f4b7f0c6f4aff4656", aum = -2)

    /* renamed from: oL */
    public long f7492oL;
    @C0064Am(aul = "f7bb964d95fdee7f4b7f0c6f4aff4656", aum = -1)

    /* renamed from: oS */
    public byte f7493oS;
    @C0064Am(aul = "7f838fe1cde12da4ce5f11416cb09761", aum = -2)

    /* renamed from: ok */
    public long f7494ok;
    @C0064Am(aul = "7f838fe1cde12da4ce5f11416cb09761", aum = -1)

    /* renamed from: on */
    public byte f7495on;

    public C2503fv() {
    }

    public C2503fv(C1616Xf xf) {
        super(xf);
    }

    /* renamed from: c */
    public C5663aRz[] mo309c() {
        return MissionAction._m_fields;
    }

    /* renamed from: d */
    public void mo310d() {
        super.mo310d();
        this.f7485Kt = null;
        this.handle = null;
        this.f7486Ku = false;
        this.f7491bK = null;
        this.f7487Kv = 0;
        this.f7494ok = 0;
        this.f7488Kw = 0;
        this.f7492oL = 0;
        this.f7489Kx = 0;
        this.f7495on = 0;
        this.f7490Ky = 0;
        this.f7493oS = 0;
    }
}
