package logic.data.mbean;

import game.network.message.serializable.C3438ra;
import game.script.item.AmplifierType;
import game.script.item.ClipType;
import game.script.item.WeaponType;
import game.script.ship.EquippedShipType;
import game.script.ship.ShipType;
import logic.baa.C1616Xf;
import logic.res.code.C5663aRz;
import p001a.C0064Am;

import java.util.UUID;

/* renamed from: a.ke */
public class C2824ke extends C3805vD {
    @C0064Am(aul = "17b30b6acadb92ea46f145ddc0017a84", aum = -2)

    /* renamed from: Nf */
    public long f8431Nf;
    @C0064Am(aul = "17b30b6acadb92ea46f145ddc0017a84", aum = -1)

    /* renamed from: Nh */
    public byte f8432Nh;
    @C0064Am(aul = "709479cb9121104b0edaa444efd36f5b", aum = 1)

    /* renamed from: OV */
    public ShipType f8433OV;
    @C0064Am(aul = "8170cc2787e22207846ab7b60d74320f", aum = 2)

    /* renamed from: OX */
    public C3438ra<WeaponType> f8434OX;
    @C0064Am(aul = "cd72dbbd0242d34a631da3b24eab191f", aum = 3)

    /* renamed from: OZ */
    public C3438ra<AmplifierType> f8435OZ;
    @C0064Am(aul = "fa17f3291a0b83ff793820fb896414e6", aum = 4)

    /* renamed from: Pb */
    public C3438ra<ClipType> f8436Pb;
    @C0064Am(aul = "c847d174a9c63c60c2c7e006a3490b77", aum = 5)

    /* renamed from: Pd */
    public float f8437Pd;
    @C0064Am(aul = "709479cb9121104b0edaa444efd36f5b", aum = -2)
    public long asn;
    @C0064Am(aul = "8170cc2787e22207846ab7b60d74320f", aum = -2)
    public long aso;
    @C0064Am(aul = "cd72dbbd0242d34a631da3b24eab191f", aum = -2)
    public long asp;
    @C0064Am(aul = "fa17f3291a0b83ff793820fb896414e6", aum = -2)
    public long asq;
    @C0064Am(aul = "c847d174a9c63c60c2c7e006a3490b77", aum = -2)
    public long asr;
    @C0064Am(aul = "709479cb9121104b0edaa444efd36f5b", aum = -1)
    public byte ass;
    @C0064Am(aul = "8170cc2787e22207846ab7b60d74320f", aum = -1)
    public byte ast;
    @C0064Am(aul = "cd72dbbd0242d34a631da3b24eab191f", aum = -1)
    public byte asu;
    @C0064Am(aul = "fa17f3291a0b83ff793820fb896414e6", aum = -1)
    public byte asv;
    @C0064Am(aul = "c847d174a9c63c60c2c7e006a3490b77", aum = -1)
    public byte asw;
    @C0064Am(aul = "6c589ceca2d4af163da9464c640f53cf", aum = 7)

    /* renamed from: bK */
    public UUID f8438bK;
    @C0064Am(aul = "5ede074e8c7210b245cd5c28ea851748", aum = 0)
    public String handle;
    @C0064Am(aul = "17b30b6acadb92ea46f145ddc0017a84", aum = 6)
    public String name;
    @C0064Am(aul = "6c589ceca2d4af163da9464c640f53cf", aum = -2)

    /* renamed from: oL */
    public long f8439oL;
    @C0064Am(aul = "6c589ceca2d4af163da9464c640f53cf", aum = -1)

    /* renamed from: oS */
    public byte f8440oS;
    @C0064Am(aul = "5ede074e8c7210b245cd5c28ea851748", aum = -2)

    /* renamed from: ok */
    public long f8441ok;
    @C0064Am(aul = "5ede074e8c7210b245cd5c28ea851748", aum = -1)

    /* renamed from: on */
    public byte f8442on;

    public C2824ke() {
    }

    public C2824ke(C1616Xf xf) {
        super(xf);
    }

    /* renamed from: c */
    public C5663aRz[] mo309c() {
        return EquippedShipType._m_fields;
    }

    /* renamed from: d */
    public void mo310d() {
        super.mo310d();
        this.handle = null;
        this.f8433OV = null;
        this.f8434OX = null;
        this.f8435OZ = null;
        this.f8436Pb = null;
        this.f8437Pd = 0.0f;
        this.name = null;
        this.f8438bK = null;
        this.f8441ok = 0;
        this.asn = 0;
        this.aso = 0;
        this.asp = 0;
        this.asq = 0;
        this.asr = 0;
        this.f8431Nf = 0;
        this.f8439oL = 0;
        this.f8442on = 0;
        this.ass = 0;
        this.ast = 0;
        this.asu = 0;
        this.asv = 0;
        this.asw = 0;
        this.f8432Nh = 0;
        this.f8440oS = 0;
    }
}
