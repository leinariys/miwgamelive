package logic.data.mbean;

import game.geometry.Vec3d;
import game.script.mission.scripting.AssaultMissionScript;
import logic.baa.C1616Xf;
import logic.res.code.C5663aRz;
import p001a.C0064Am;

/* renamed from: a.Fu */
public class C0426Fu extends aMY {
    @C0064Am(aul = "dc3cfcaa11bfba2f1275036ededdaef5", aum = -2)
    public long aLK;
    @C0064Am(aul = "dc3cfcaa11bfba2f1275036ededdaef5", aum = -1)
    public byte aLP;
    @C0064Am(aul = "dc3cfcaa11bfba2f1275036ededdaef5", aum = 0)
    public Vec3d cCz;
    @C0064Am(aul = "ee0c0f836448be22e42bac1f5977af72", aum = 1)
    public int cVv;
    @C0064Am(aul = "ee0c0f836448be22e42bac1f5977af72", aum = -2)
    public long cVw;
    @C0064Am(aul = "ee0c0f836448be22e42bac1f5977af72", aum = -1)
    public byte cVx;

    public C0426Fu() {
    }

    public C0426Fu(C1616Xf xf) {
        super(xf);
    }

    /* renamed from: c */
    public C5663aRz[] mo309c() {
        return AssaultMissionScript._m_fields;
    }

    /* renamed from: d */
    public void mo310d() {
        super.mo310d();
        this.cCz = null;
        this.cVv = 0;
        this.aLK = 0;
        this.cVw = 0;
        this.aLP = 0;
        this.cVx = 0;
    }
}
