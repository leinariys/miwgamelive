package logic.data.mbean;

import game.script.item.ItemType;
import game.script.newmarket.store.Store;
import logic.baa.C1616Xf;
import logic.res.code.C5663aRz;
import p001a.C0064Am;

/* renamed from: a.G */
public class C0432G extends C6757atB {
    @C0064Am(aul = "e57122d18ae6547cf59afb9c82a34aeb", aum = 0)

    /* renamed from: cX */
    public Store.C2618a f595cX;
    @C0064Am(aul = "88537e974aeea6b3b0e3830c25880d66", aum = 1)

    /* renamed from: cY */
    public ItemType f596cY;
    @C0064Am(aul = "70f769658435bd7b7af92c351925160e", aum = 2)

    /* renamed from: cZ */
    public int f597cZ;
    @C0064Am(aul = "7cdcf0fb5e429addb60f1e3a599e6a04", aum = 3)

    /* renamed from: da */
    public int f598da;
    @C0064Am(aul = "f4f6627b9b7a62e189fcbcab13817572", aum = 4)

    /* renamed from: db */
    public long f599db;
    @C0064Am(aul = "1f22d90bd3f021fd99f55a699b4b20b5", aum = 5)

    /* renamed from: dd */
    public long f600dd;
    @C0064Am(aul = "988b77fba2c917a3583d602de8a1a5d2", aum = 6)

    /* renamed from: de */
    public Store f601de;
    @C0064Am(aul = "e57122d18ae6547cf59afb9c82a34aeb", aum = -2)

    /* renamed from: df */
    public long f602df;
    @C0064Am(aul = "88537e974aeea6b3b0e3830c25880d66", aum = -2)

    /* renamed from: dg */
    public long f603dg;
    @C0064Am(aul = "70f769658435bd7b7af92c351925160e", aum = -2)

    /* renamed from: dh */
    public long f604dh;
    @C0064Am(aul = "7cdcf0fb5e429addb60f1e3a599e6a04", aum = -2)

    /* renamed from: di */
    public long f605di;
    @C0064Am(aul = "f4f6627b9b7a62e189fcbcab13817572", aum = -2)

    /* renamed from: dj */
    public long f606dj;
    @C0064Am(aul = "1f22d90bd3f021fd99f55a699b4b20b5", aum = -2)

    /* renamed from: dk */
    public long f607dk;
    @C0064Am(aul = "988b77fba2c917a3583d602de8a1a5d2", aum = -2)

    /* renamed from: dl */
    public long f608dl;
    @C0064Am(aul = "e57122d18ae6547cf59afb9c82a34aeb", aum = -1)

    /* renamed from: dm */
    public byte f609dm;
    @C0064Am(aul = "88537e974aeea6b3b0e3830c25880d66", aum = -1)

    /* renamed from: dn */
    public byte f610dn;
    @C0064Am(aul = "70f769658435bd7b7af92c351925160e", aum = -1)

    /* renamed from: do */
    public byte f611do;
    @C0064Am(aul = "7cdcf0fb5e429addb60f1e3a599e6a04", aum = -1)

    /* renamed from: dp */
    public byte f612dp;
    @C0064Am(aul = "f4f6627b9b7a62e189fcbcab13817572", aum = -1)

    /* renamed from: dq */
    public byte f613dq;
    @C0064Am(aul = "1f22d90bd3f021fd99f55a699b4b20b5", aum = -1)

    /* renamed from: dr */
    public byte f614dr;
    @C0064Am(aul = "988b77fba2c917a3583d602de8a1a5d2", aum = -1)

    /* renamed from: ds */
    public byte f615ds;

    public C0432G() {
    }

    public C0432G(C1616Xf xf) {
        super(xf);
    }

    /* renamed from: c */
    public C5663aRz[] mo309c() {
        return Store.PostponedOrderTasklet._m_fields;
    }

    /* renamed from: d */
    public void mo310d() {
        super.mo310d();
        this.f595cX = null;
        this.f596cY = null;
        this.f597cZ = 0;
        this.f598da = 0;
        this.f599db = 0;
        this.f600dd = 0;
        this.f601de = null;
        this.f602df = 0;
        this.f603dg = 0;
        this.f604dh = 0;
        this.f605di = 0;
        this.f606dj = 0;
        this.f607dk = 0;
        this.f608dl = 0;
        this.f609dm = 0;
        this.f610dn = 0;
        this.f611do = 0;
        this.f612dp = 0;
        this.f613dq = 0;
        this.f614dr = 0;
        this.f615ds = 0;
    }
}
