package logic.data.mbean;

import logic.baa.C1616Xf;
import logic.res.code.C5663aRz;
import p001a.C0064Am;

/* renamed from: a.fB */
public class C2414fB extends C5292aDs {
    @C0064Am(aul = "0c26f68e239831e0e6a8f14395edec23", aum = -2)

    /* renamed from: LA */
    public long f7172LA;
    @C0064Am(aul = "b4d5613fcb5084a18f35ddae6b5c48c4", aum = -2)

    /* renamed from: LD */
    public long f7173LD;
    @C0064Am(aul = "56be6876a22a2120219348d13ad207ff", aum = -2)

    /* renamed from: LF */
    public long f7174LF;
    @C0064Am(aul = "eaa2a052767726f453eca09dc19b507b", aum = -2)

    /* renamed from: LG */
    public long f7175LG;
    @C0064Am(aul = "fe7665dee2e3d606fd1472d549c44d59", aum = -1)

    /* renamed from: LH */
    public byte f7176LH;
    @C0064Am(aul = "25a64144b4526610a94d539a86f44464", aum = -1)

    /* renamed from: LI */
    public byte f7177LI;
    @C0064Am(aul = "0c26f68e239831e0e6a8f14395edec23", aum = -1)

    /* renamed from: LJ */
    public byte f7178LJ;
    @C0064Am(aul = "b4d5613fcb5084a18f35ddae6b5c48c4", aum = -1)

    /* renamed from: LK */
    public byte f7179LK;
    @C0064Am(aul = "56be6876a22a2120219348d13ad207ff", aum = -1)

    /* renamed from: LL */
    public byte f7180LL;
    @C0064Am(aul = "eaa2a052767726f453eca09dc19b507b", aum = -1)

    /* renamed from: LM */
    public byte f7181LM;
    @C0064Am(aul = "fe7665dee2e3d606fd1472d549c44d59", aum = 0)

    /* renamed from: Ls */
    public boolean f7182Ls;
    @C0064Am(aul = "25a64144b4526610a94d539a86f44464", aum = 1)

    /* renamed from: Lt */
    public boolean f7183Lt;
    @C0064Am(aul = "0c26f68e239831e0e6a8f14395edec23", aum = 2)

    /* renamed from: Lu */
    public boolean f7184Lu;
    @C0064Am(aul = "b4d5613fcb5084a18f35ddae6b5c48c4", aum = 3)

    /* renamed from: Lv */
    public boolean f7185Lv;
    @C0064Am(aul = "56be6876a22a2120219348d13ad207ff", aum = 4)

    /* renamed from: Lw */
    public boolean f7186Lw;
    @C0064Am(aul = "eaa2a052767726f453eca09dc19b507b", aum = 5)

    /* renamed from: Lx */
    public boolean f7187Lx;
    @C0064Am(aul = "fe7665dee2e3d606fd1472d549c44d59", aum = -2)

    /* renamed from: Ly */
    public long f7188Ly;
    @C0064Am(aul = "25a64144b4526610a94d539a86f44464", aum = -2)

    /* renamed from: Lz */
    public long f7189Lz;

    public C2414fB() {
    }

    public C2414fB(C1616Xf xf) {
        super(xf);
    }

    /* renamed from: c */
    public C5663aRz[] mo309c() {
        return DebugFlags._m_fields;
    }

    /* renamed from: d */
    public void mo310d() {
        super.mo310d();
        this.f7182Ls = false;
        this.f7183Lt = false;
        this.f7184Lu = false;
        this.f7185Lv = false;
        this.f7186Lw = false;
        this.f7187Lx = false;
        this.f7188Ly = 0;
        this.f7189Lz = 0;
        this.f7172LA = 0;
        this.f7173LD = 0;
        this.f7174LF = 0;
        this.f7175LG = 0;
        this.f7176LH = 0;
        this.f7177LI = 0;
        this.f7178LJ = 0;
        this.f7179LK = 0;
        this.f7180LL = 0;
        this.f7181LM = 0;
    }
}
