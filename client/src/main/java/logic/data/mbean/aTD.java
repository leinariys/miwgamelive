package logic.data.mbean;

import game.network.message.serializable.C3438ra;
import game.script.faction.Faction;
import game.script.npcchat.requirement.IsFromFactionSpeechRequirement;
import logic.baa.C1616Xf;
import logic.res.code.C5663aRz;
import p001a.C0064Am;

/* renamed from: a.aTD */
public class aTD extends C5416aIm {
    @C0064Am(aul = "c861ca0dddba76223c7cf2dae596bc65", aum = 0)
    public C3438ra<Faction> aQm;
    @C0064Am(aul = "c861ca0dddba76223c7cf2dae596bc65", aum = -2)
    public long idg;
    @C0064Am(aul = "c861ca0dddba76223c7cf2dae596bc65", aum = -1)
    public byte idh;

    public aTD() {
    }

    public aTD(C1616Xf xf) {
        super(xf);
    }

    /* renamed from: c */
    public C5663aRz[] mo309c() {
        return IsFromFactionSpeechRequirement._m_fields;
    }

    /* renamed from: d */
    public void mo310d() {
        super.mo310d();
        this.aQm = null;
        this.idg = 0;
        this.idh = 0;
    }
}
