package logic.data.mbean;

import game.network.message.serializable.C3438ra;
import game.script.missiontemplate.WaypointDat;
import game.script.missiontemplate.scripting.ItemDeliveryMissionScriptTemplate;
import game.script.space.StellarSystem;
import logic.baa.C1616Xf;
import logic.res.code.C5663aRz;
import p001a.C0064Am;

/* renamed from: a.aub  reason: case insensitive filesystem */
public class C6835aub extends C0597IR {
    @C0064Am(aul = "65df85ddc6c7e54714649e8ee5c5c00f", aum = 4)
    public WaypointDat ebo;
    @C0064Am(aul = "6603a34789fb75a39900a12c1bfb7a65", aum = 0)
    public String ebs;
    @C0064Am(aul = "6603a34789fb75a39900a12c1bfb7a65", aum = -2)
    public long gCE;
    @C0064Am(aul = "65df85ddc6c7e54714649e8ee5c5c00f", aum = -2)
    public long gCF;
    @C0064Am(aul = "6603a34789fb75a39900a12c1bfb7a65", aum = -1)
    public byte gCG;
    @C0064Am(aul = "65df85ddc6c7e54714649e8ee5c5c00f", aum = -1)
    public byte gCH;
    @C0064Am(aul = "46bd32fc5407a3c6cb8a6b785fa4bedb", aum = 1)

    /* renamed from: iI */
    public C3438ra<ItemTypeTableItem> f5401iI;
    @C0064Am(aul = "08958227a3ffa684b003dc2303f5a767", aum = 2)

    /* renamed from: iJ */
    public C3438ra<ItemTypeTableItem> f5402iJ;
    @C0064Am(aul = "46bd32fc5407a3c6cb8a6b785fa4bedb", aum = -2)

    /* renamed from: iL */
    public long f5403iL;
    @C0064Am(aul = "08958227a3ffa684b003dc2303f5a767", aum = -2)

    /* renamed from: iM */
    public long f5404iM;
    @C0064Am(aul = "46bd32fc5407a3c6cb8a6b785fa4bedb", aum = -1)

    /* renamed from: iO */
    public byte f5405iO;
    @C0064Am(aul = "08958227a3ffa684b003dc2303f5a767", aum = -1)

    /* renamed from: iP */
    public byte f5406iP;
    @C0064Am(aul = "c51c2913ad9f296a4b06bb235fdcf766", aum = 3)

    /* renamed from: rI */
    public StellarSystem f5407rI;
    @C0064Am(aul = "c51c2913ad9f296a4b06bb235fdcf766", aum = -2)

    /* renamed from: rY */
    public long f5408rY;
    @C0064Am(aul = "c51c2913ad9f296a4b06bb235fdcf766", aum = -1)

    /* renamed from: so */
    public byte f5409so;

    public C6835aub() {
    }

    public C6835aub(C1616Xf xf) {
        super(xf);
    }

    /* renamed from: c */
    public C5663aRz[] mo309c() {
        return ItemDeliveryMissionScriptTemplate._m_fields;
    }

    /* renamed from: d */
    public void mo310d() {
        super.mo310d();
        this.ebs = null;
        this.f5401iI = null;
        this.f5402iJ = null;
        this.f5407rI = null;
        this.ebo = null;
        this.gCE = 0;
        this.f5403iL = 0;
        this.f5404iM = 0;
        this.f5408rY = 0;
        this.gCF = 0;
        this.gCG = 0;
        this.f5405iO = 0;
        this.f5406iP = 0;
        this.f5409so = 0;
        this.gCH = 0;
    }
}
