package logic.data.mbean;

import game.script.ship.hazardshield.HazardShield;
import logic.baa.C1616Xf;
import logic.res.code.C5663aRz;
import p001a.C0064Am;

/* renamed from: a.aaT  reason: case insensitive filesystem */
public class C5787aaT extends C3119nz {
    @C0064Am(aul = "ccce454763e3c3787beb87d03712989b", aum = -2)

    /* renamed from: dl */
    public long f4070dl;
    @C0064Am(aul = "ccce454763e3c3787beb87d03712989b", aum = -1)

    /* renamed from: ds */
    public byte f4071ds;
    @C0064Am(aul = "ccce454763e3c3787beb87d03712989b", aum = 0)

    /* renamed from: pZ */
    public HazardShield f4072pZ;

    public C5787aaT() {
    }

    public C5787aaT(C1616Xf xf) {
        super(xf);
    }

    /* renamed from: c */
    public C5663aRz[] mo309c() {
        return HazardShield.ShieldBlock._m_fields;
    }

    /* renamed from: d */
    public void mo310d() {
        super.mo310d();
        this.f4072pZ = null;
        this.f4070dl = 0;
        this.f4071ds = 0;
    }
}
