package logic.data.mbean;

import game.script.contract.types.BountyContract;
import game.script.contract.types.BountyTemplate;
import game.script.player.Player;
import logic.baa.C1616Xf;
import logic.res.code.C5663aRz;
import p001a.C0064Am;

/* renamed from: a.ft */
public class C2501ft extends C4100zT {
    @C0064Am(aul = "c71686bfe1dc212f340973dc9ca5aecc", aum = 0)

    /* renamed from: Kd */
    public Player f7471Kd;
    @C0064Am(aul = "78b3916a948e8ec1b487dd4f0f14745c", aum = 1)

    /* renamed from: Ke */
    public BountyContract<BountyTemplate> f7472Ke;
    @C0064Am(aul = "c71686bfe1dc212f340973dc9ca5aecc", aum = -2)

    /* renamed from: Kf */
    public long f7473Kf;
    @C0064Am(aul = "78b3916a948e8ec1b487dd4f0f14745c", aum = -2)

    /* renamed from: Kg */
    public long f7474Kg;
    @C0064Am(aul = "c71686bfe1dc212f340973dc9ca5aecc", aum = -1)

    /* renamed from: Kh */
    public byte f7475Kh;
    @C0064Am(aul = "78b3916a948e8ec1b487dd4f0f14745c", aum = -1)

    /* renamed from: Ki */
    public byte f7476Ki;

    public C2501ft() {
    }

    public C2501ft(C1616Xf xf) {
        super(xf);
    }

    /* renamed from: c */
    public C5663aRz[] mo309c() {
        return BountyTemplate._m_fields;
    }

    /* renamed from: d */
    public void mo310d() {
        super.mo310d();
        this.f7471Kd = null;
        this.f7472Ke = null;
        this.f7473Kf = 0;
        this.f7474Kg = 0;
        this.f7475Kh = 0;
        this.f7476Ki = 0;
    }
}
