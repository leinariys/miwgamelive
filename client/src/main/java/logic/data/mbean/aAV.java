package logic.data.mbean;

import game.network.message.serializable.C3438ra;
import game.script.missiontemplate.scripting.TalkToNPCMissionScriptTemplate;
import game.script.npcchat.PlayerSpeech;
import logic.baa.C1616Xf;
import logic.res.code.C5663aRz;
import p001a.C0064Am;

/* renamed from: a.aAV */
public class aAV extends C0597IR {
    @C0064Am(aul = "7814ab0109f0522991ed6149f3619943", aum = 0)
    public C3438ra<PlayerSpeech> eeB;
    @C0064Am(aul = "7814ab0109f0522991ed6149f3619943", aum = -2)
    public long hfs;
    @C0064Am(aul = "7814ab0109f0522991ed6149f3619943", aum = -1)
    public byte hft;

    public aAV() {
    }

    public aAV(C1616Xf xf) {
        super(xf);
    }

    /* renamed from: c */
    public C5663aRz[] mo309c() {
        return TalkToNPCMissionScriptTemplate._m_fields;
    }

    /* renamed from: d */
    public void mo310d() {
        super.mo310d();
        this.eeB = null;
        this.hfs = 0;
        this.hft = 0;
    }
}
