package logic.data.mbean;

import game.CollisionFXSet;
import game.network.message.serializable.C1556Wo;
import game.network.message.serializable.C2686iZ;
import game.network.message.serializable.C3438ra;
import game.script.BotUtils;
import game.script.ClientUtils;
import game.script.PingScript;
import game.script.TaikodomDefaultContents;
import game.script.advertise.AdvertiseViewTable;
import game.script.ai.npc.AIControllerType;
import game.script.avatar.AvatarManager;
import game.script.backdoor.Backdoor;
import game.script.bank.Bank;
import game.script.citizenship.*;
import game.script.connect.UsersQueueManager;
import game.script.consignment.ConsignmentFeeManager;
import game.script.content.ContentImporterExporter;
import game.script.contract.ContractBoard;
import game.script.corporation.Corporation;
import game.script.corporation.CorporationReservedNames;
import game.script.damage.DamageType;
import game.script.faction.Faction;
import game.script.hazardarea.HazardArea.HazardAreaType;
import game.script.insurance.Insurer;
import game.script.item.*;
import game.script.item.buff.module.AttributeBuffType;
import game.script.itembilling.ItemBilling;
import game.script.itemgen.ItemGenSet;
import game.script.mission.MissionTemplate;
import game.script.newmarket.Market;
import game.script.newmarket.economy.VirtualWarehouse;
import game.script.newmarket.store.GlobalEconomyInfo;
import game.script.nls.NLSManager;
import game.script.npc.NPCType;
import game.script.player.PlayerFinder;
import game.script.player.PlayerReservedNames;
import game.script.player.User;
import game.script.progression.Progression;
import game.script.resource.AssetGroup;
import game.script.ship.*;
import game.script.space.*;
import game.script.spacezone.AsteroidZoneCategory;
import game.script.spacezone.SpaceZoneTypesManager;
import game.script.stats.StatsManager;
import game.script.util.ServerEditorUtility;
import logic.baa.C0468GU;
import logic.baa.C0694Ju;
import logic.baa.C1077Pl;
import logic.baa.C1616Xf;
import logic.res.code.C5663aRz;
import p001a.C0064Am;
import taikodom.infra.script.I18NString;

import java.util.UUID;

/* renamed from: a.aNG */
public class aNG extends C3805vD {
    @C0064Am(aul = "77af9d530f16817eaaff51ee29eb9886", aum = 24)

    /* renamed from: OX */
    public C3438ra<WeaponType> f3419OX;
    @C0064Am(aul = "48808b0ce4298a336a69bf717ac4f1a2", aum = 29)
    public C3438ra<NPCType> aLD;
    @C0064Am(aul = "48808b0ce4298a336a69bf717ac4f1a2", aum = -2)
    public long aLI;
    @C0064Am(aul = "48808b0ce4298a336a69bf717ac4f1a2", aum = -1)
    public byte aLN;
    @C0064Am(aul = "10a3f06055bc06f565e35233064248aa", aum = 27)
    public C3438ra<ItemGenSet> aPp;
    @C0064Am(aul = "a108a84de7c23a928c9d83c73a43608c", aum = 34)
    public C3438ra<AttributeBuffType> aQT;
    @C0064Am(aul = "9f2f4090839475d412dccdf2ba545ea5", aum = 36)
    public C3438ra<Faction> aQm;
    @C0064Am(aul = "77af9d530f16817eaaff51ee29eb9886", aum = -2)
    public long aso;
    @C0064Am(aul = "77af9d530f16817eaaff51ee29eb9886", aum = -1)
    public byte ast;
    @C0064Am(aul = "49db357b3491c3b2f57d324f8777a29a", aum = 0)

    /* renamed from: bK */
    public UUID f3420bK;
    @C0064Am(aul = "e1ca85a4136e7c31ccc92de4d9c8d64a", aum = 20)
    public C3438ra<ShipStructureType> boj;
    @C0064Am(aul = "d739e1c39c595b1cde0f21b1986e10c4", aum = 1)
    public I18NString cIY;
    @C0064Am(aul = "3ef0d8bd22bf99c59b683834a6b80a4e", aum = 15)
    public C3438ra<BlueprintType> cJA;
    @C0064Am(aul = "6e6ae2dc34df21f76b0106ad390ba8eb", aum = 16)
    public C3438ra<SceneryType> cJC;
    @C0064Am(aul = "89f24f1f91af67cbcba7abd8935ea76b", aum = 17)
    public C3438ra<ShieldType> cJE;
    @C0064Am(aul = "0f58fe747ce815948d418a95b181c12c", aum = 18)
    public C3438ra<HazardAreaType> cJG;
    @C0064Am(aul = "14b40f53c948437ca676cc4e8a7117d1", aum = 19)
    public C3438ra<ShipSectorType> cJI;
    @C0064Am(aul = "9f726f508fa0640701cad26f3a54a3df", aum = 21)
    public C3438ra<ShipType> cJK;
    @C0064Am(aul = "51654e434aeca9c1e8f2f6433383d0aa", aum = 22)
    public C3438ra<C6544aow> cJM;
    @C0064Am(aul = "c51a8592ec3e25251cd361ecd20ddd63", aum = 23)
    public C3438ra<StationType> cJO;
    @C0064Am(aul = "3d47036606bd151848f82b06d664d5a2", aum = 25)
    public C3438ra<RawMaterialType> cJQ;
    @C0064Am(aul = "5180523453849cb135127ad19d27cb56", aum = 26)
    public C3438ra<ItemTypeCategory> cJS;
    @C0064Am(aul = "55362d1bcb671bd0f68719513c0282cf", aum = 28)
    public C3438ra<MissionTemplate> cJU;
    @C0064Am(aul = "e4c192227702947200cab4f7af6a1d29", aum = 30)
    public C3438ra<EquippedShipType> cJW;
    @C0064Am(aul = "74cff4a2d9494c75b9ccaca9aaf341b1", aum = 31)
    public C3438ra<LootType> cJY;
    @C0064Am(aul = "c3e1df9faf79a29e6eacfdf2bbfbed3d", aum = 2)
    public C3438ra<User> cJa;
    @C0064Am(aul = "e262cebe0dc7e875a4ea79a3c1f28e87", aum = 3)
    public C2686iZ<String> cJc;
    @C0064Am(aul = "e1ef1d7d36c2cecf84797b7aea056b86", aum = 4)
    public C3438ra<CollisionFXSet> cJe;
    @C0064Am(aul = "0ded854935e000dd9a08168691262e98", aum = 5)
    public C3438ra<StellarSystem> cJg;
    @C0064Am(aul = "0e42d8baef486c82e1d3c3ccef1021aa", aum = 6)
    public C3438ra<ClipType> cJi;
    @C0064Am(aul = "d0a4baf1e9b6e403c995b7623a41bcae", aum = 7)
    public C3438ra<ClipBoxType> cJk;
    @C0064Am(aul = "fbc07682f6f68bedf60852c51fa1b0d1", aum = 8)
    public C3438ra<CargoHoldType> cJm;
    @C0064Am(aul = "8fcf7f889cc8f8b16a80466c352cdbc1", aum = 9)
    public C3438ra<GateType> cJo;
    @C0064Am(aul = "5c44eeffae3d0021344b9054b9ce02f9", aum = 10)
    public C3438ra<AdvertiseType> cJq;
    @C0064Am(aul = "aec892e48c3abc8bf5cdf96dcdc0e243", aum = 11)
    public C3438ra<AsteroidType> cJs;
    @C0064Am(aul = "9ef9ba379474f3b6038b849c2b448e80", aum = 12)
    public C3438ra<HullType> cJu;
    @C0064Am(aul = "eeae9ed70bc04d039adcf73d14d91bd5", aum = 13)
    public C3438ra<BagItemType> cJw;
    @C0064Am(aul = "09686e3ba94ca4685f171fb8b85f8dfd", aum = 14)
    public C3438ra<MerchandiseType> cJy;
    @C0064Am(aul = "a28f92930a2d44a173c526225cbab8d3", aum = 48)
    public C3438ra<AsteroidZoneCategory> cKA;
    @C0064Am(aul = "20a4fd2705be637288166a055dce7a51", aum = 49)
    public C3438ra<SpaceCategory> cKC;
    @C0064Am(aul = "b88e86ae0f5115d0b43808c5cace3644", aum = 50)
    public Progression cKD;
    @C0064Am(aul = "342a7aa7a8a1a5f0e5154253f00f5b34", aum = 51)
    public ContractBoard cKF;
    @C0064Am(aul = "9dbf638fa17b7568d75ced3a38a7c0b5", aum = 52)
    public NLSManager cKH;
    @C0064Am(aul = "886d30a753ab5329dd305ed69dbba5e2", aum = 53)
    public AvatarManager cKJ;
    @C0064Am(aul = "620f87f83db39a669e3d72438169b6c0", aum = 54)
    public Bank cKL;
    @C0064Am(aul = "b0480a65644dd3f193c9163db4cc94c8", aum = 55)
    public Bank cKN;
    @C0064Am(aul = "1861ce90dbea8990b5d21f6698b9a27d", aum = 56)
    public SpaceZoneTypesManager cKP;
    @C0064Am(aul = "b13158b02e82316b66b1504c7fa0f62c", aum = 57)
    public CitizenshipOffice cKR;
    @C0064Am(aul = "3f0e5812d96db509fdb0e8a654b70fd4", aum = 58)
    public Insurer cKT;
    @C0064Am(aul = "bd09ebeccb6a3251c782f43106c428bc", aum = 59)
    public Backdoor cKV;
    @C0064Am(aul = "3513763bd1ebb926802ec674d00342c4", aum = 60)
    public AssetGroup cKX;
    @C0064Am(aul = "0e8320390b62c71e4b28931f87de514e", aum = 61)
    public SupportOuterface cKZ;
    @C0064Am(aul = "d4b80533e58f52f4660c4cb93ff6f5df", aum = 32)
    public C3438ra<AmplifierType> cKa;
    @C0064Am(aul = "b59340dc2df57d292d550899bb82d5c5", aum = 33)
    public C3438ra<ModuleType> cKc;
    @C0064Am(aul = "ef4a1f3e5a08ed25e21f469005f58d71", aum = 35)
    public C3438ra<AIControllerType> cKe;
    @C0064Am(aul = "43bc1e9a73af88ba5d1d2b82226598db", aum = 37)
    public C3438ra<Tutorial> cKg;
    @C0064Am(aul = "cd30acee5d2c2b8a4ec8943c351fb519", aum = 38)
    public C3438ra<ConsignmentFeeManager> cKi;
    @C0064Am(aul = "dddd86408e7e8541ca440622212b9743", aum = 39)
    public C3438ra<SectorCategory> cKk;
    @C0064Am(aul = "bc2c4bc191da12c52f3b61e80abb8deb", aum = 40)
    public C2686iZ<DamageType> cKm;
    @C0064Am(aul = "d63c3bffb361bc982793a74f790b3b0c", aum = 41)
    public C2686iZ<CruiseSpeedType> cKo;
    @C0064Am(aul = "f5f671ce3f549c1ad63bbecdf219e6f3", aum = 42)
    public C3438ra<CitizenImprovementType> cKq;
    @C0064Am(aul = "83eed973cd6c30fe56b691f872604039", aum = 43)
    public C3438ra<CitizenshipType> cKs;
    @C0064Am(aul = "1b69d88d1b32876217ff8c03ad1b5581", aum = 44)
    public C3438ra<CitizenshipPack> cKu;
    @C0064Am(aul = "44753edf05230f76f825ce1569c5be4f", aum = 45)
    public C3438ra<CitizenshipReward> cKw;
    @C0064Am(aul = "53a8711a2eca9aea84584ad4e41be6f6", aum = 47)
    public C3438ra<C6194aiK> cKy;
    @C0064Am(aul = "ffa4c6cab09cf0e782b8bfcbb7c31105", aum = 75)
    public C1077Pl cLA;
    @C0064Am(aul = "85ad077af9999d729561a6702b45fb5c", aum = 76)
    public C0694Ju cLC;
    @C0064Am(aul = "5010435f23a887886a397977f6a12d83", aum = 77)
    public Market cLE;
    @C0064Am(aul = "3875f819a2cf015dba3d7a911b4eb4e1", aum = 78)
    public GlobalEconomyInfo cLG;
    @C0064Am(aul = "793e01f95ef8536602fd252f70c58f98", aum = 79)
    public GlobalPhysicsTweaks cLI;
    @C0064Am(aul = "746dd8b67eef6d066f76678094cc5eea", aum = 80)
    public C3438ra<VirtualWarehouse> cLK;
    @C0064Am(aul = "9942eafa3e6cacb784012f7fbfd556d0", aum = 81)
    public PlayerReservedNames cLM;
    @C0064Am(aul = "229852970fffee012472f9ac45b3a666", aum = 82)
    public C2686iZ<ItemType> cLO;
    @C0064Am(aul = "b32aeead5c67ecc4d54cd377c3f16af0", aum = 83)
    public C2686iZ<C0468GU> cLQ;
    @C0064Am(aul = "15cf2e87452024c3e30f40b5eb485ef3", aum = 84)
    public C1556Wo<String, User> cLS;
    @C0064Am(aul = "41b75b60af5c4dbe52e03ff02c7a5d13", aum = 85)
    public UsersQueueManager cLU;
    @C0064Am(aul = "cd5906400bdc5032b6f3a93db986b423", aum = 86)
    public PingScript cLW;
    @C0064Am(aul = "a2fcb21a72ffbbf23789ac1b1f13155e", aum = 87)
    public DebugFlags cLY;
    @C0064Am(aul = "1746882bafddaeb37b150fc52198e65f", aum = 62)
    public StatsManager cLb;
    @C0064Am(aul = "1fa89d48b676d419a6c1696ecdef0965", aum = 63)
    public MarkManager cLd;
    @C0064Am(aul = "adf5a21ac520af8b362e3e1c157764e6", aum = 64)
    public AdvertiseViewTable cLf;
    @C0064Am(aul = "ac86c32c8d2c6b98e44ef3130c8a6146", aum = 65)
    public NPCType cLh;
    @C0064Am(aul = "69f3fadf055251b81f1f002984fea596", aum = 66)
    public ServerEditorUtility cLj;
    @C0064Am(aul = "9a1ca95c232cb49dff50010058711182", aum = 67)
    public TaikodomDefaultContents cLl;
    @C0064Am(aul = "4619ac4910b1790f48dd025c5c8c1889", aum = 68)
    public TemporaryFeatureBlock cLm;
    @C0064Am(aul = "8013e63b1d233493b79a1e48a1839b96", aum = 69)
    public TemporaryFeatureTest cLo;
    @C0064Am(aul = "6d5dad9dec1adaf2c3d3444a5b78dad2", aum = 70)
    public ItemBilling cLq;
    @C0064Am(aul = "adb9c25d672d3a4151b13847163ae68d", aum = 71)
    public ContentImporterExporter cLs;
    @C0064Am(aul = "c55786d1ab0e0f4600f15b684b70f284", aum = 72)
    public C3438ra<Corporation> cLu;
    @C0064Am(aul = "a38c5434b704a05116c57319b9150dcf", aum = 73)
    public CorporationReservedNames cLw;
    @C0064Am(aul = "78a94494558ec8ebf57bf7dc2bf98d9d", aum = 74)
    public PlayerFinder cLy;
    @C0064Am(aul = "61402922b0fce6cd61f62bf03d8a786d", aum = 88)
    public ClientUtils cMa;
    @C0064Am(aul = "fce98b95151dccbc3c6ce5e149c551ff", aum = 89)
    public C5619aQh cMc;
    @C0064Am(aul = "35acd4443c8095f0993c08cadd87c467", aum = 90)
    public BotUtils cMe;
    @C0064Am(aul = "3513763bd1ebb926802ec674d00342c4", aum = -2)
    public long cVn;
    @C0064Am(aul = "3513763bd1ebb926802ec674d00342c4", aum = -1)
    public byte cVp;
    @C0064Am(aul = "aec892e48c3abc8bf5cdf96dcdc0e243", aum = -2)
    public long cZA;
    @C0064Am(aul = "aec892e48c3abc8bf5cdf96dcdc0e243", aum = -1)
    public byte cZB;
    @C0064Am(aul = "a108a84de7c23a928c9d83c73a43608c", aum = -1)
    public byte eLD;
    @C0064Am(aul = "a108a84de7c23a928c9d83c73a43608c", aum = -2)
    public long eLw;
    @C0064Am(aul = "83eed973cd6c30fe56b691f872604039", aum = -2)
    public long exC;
    @C0064Am(aul = "83eed973cd6c30fe56b691f872604039", aum = -1)
    public byte exD;
    @C0064Am(aul = "10a3f06055bc06f565e35233064248aa", aum = -2)
    public long gCS;
    @C0064Am(aul = "10a3f06055bc06f565e35233064248aa", aum = -1)
    public byte gCU;
    @C0064Am(aul = "20a4fd2705be637288166a055dce7a51", aum = -2)
    public long gSZ;
    @C0064Am(aul = "20a4fd2705be637288166a055dce7a51", aum = -1)
    public byte gTe;
    @C0064Am(aul = "14b40f53c948437ca676cc4e8a7117d1", aum = -2)
    public long gdc;
    @C0064Am(aul = "14b40f53c948437ca676cc4e8a7117d1", aum = -1)
    public byte gdd;
    @C0064Am(aul = "9a1ca95c232cb49dff50010058711182", aum = -1)
    public byte gsC;
    @C0064Am(aul = "9a1ca95c232cb49dff50010058711182", aum = -2)
    public long gsy;
    @C0064Am(aul = "a28f92930a2d44a173c526225cbab8d3", aum = -2)
    public long hPr;
    @C0064Am(aul = "a28f92930a2d44a173c526225cbab8d3", aum = -1)
    public byte hPs;
    @C0064Am(aul = "9f2f4090839475d412dccdf2ba545ea5", aum = -2)
    public long idg;
    @C0064Am(aul = "9f2f4090839475d412dccdf2ba545ea5", aum = -1)
    public byte idh;
    @C0064Am(aul = "e1ca85a4136e7c31ccc92de4d9c8d64a", aum = -2)
    public long imF;
    @C0064Am(aul = "e1ca85a4136e7c31ccc92de4d9c8d64a", aum = -1)
    public byte imX;
    @C0064Am(aul = "d739e1c39c595b1cde0f21b1986e10c4", aum = -2)
    public long itH;
    @C0064Am(aul = "c3e1df9faf79a29e6eacfdf2bbfbed3d", aum = -2)
    public long itI;
    @C0064Am(aul = "e262cebe0dc7e875a4ea79a3c1f28e87", aum = -2)
    public long itJ;
    @C0064Am(aul = "e1ef1d7d36c2cecf84797b7aea056b86", aum = -2)
    public long itK;
    @C0064Am(aul = "0ded854935e000dd9a08168691262e98", aum = -2)
    public long itL;
    @C0064Am(aul = "0e42d8baef486c82e1d3c3ccef1021aa", aum = -2)
    public long itM;
    @C0064Am(aul = "d0a4baf1e9b6e403c995b7623a41bcae", aum = -2)
    public long itN;
    @C0064Am(aul = "fbc07682f6f68bedf60852c51fa1b0d1", aum = -2)
    public long itO;
    @C0064Am(aul = "8fcf7f889cc8f8b16a80466c352cdbc1", aum = -2)
    public long itP;
    @C0064Am(aul = "5c44eeffae3d0021344b9054b9ce02f9", aum = -2)
    public long itQ;
    @C0064Am(aul = "9ef9ba379474f3b6038b849c2b448e80", aum = -2)
    public long itR;
    @C0064Am(aul = "eeae9ed70bc04d039adcf73d14d91bd5", aum = -2)
    public long itS;
    @C0064Am(aul = "09686e3ba94ca4685f171fb8b85f8dfd", aum = -2)
    public long itT;
    @C0064Am(aul = "3ef0d8bd22bf99c59b683834a6b80a4e", aum = -2)
    public long itU;
    @C0064Am(aul = "6e6ae2dc34df21f76b0106ad390ba8eb", aum = -2)
    public long itV;
    @C0064Am(aul = "89f24f1f91af67cbcba7abd8935ea76b", aum = -2)
    public long itW;
    @C0064Am(aul = "0f58fe747ce815948d418a95b181c12c", aum = -2)
    public long itX;
    @C0064Am(aul = "9f726f508fa0640701cad26f3a54a3df", aum = -2)
    public long itY;
    @C0064Am(aul = "51654e434aeca9c1e8f2f6433383d0aa", aum = -2)
    public long itZ;
    @C0064Am(aul = "3f0e5812d96db509fdb0e8a654b70fd4", aum = -2)
    public long iuA;
    @C0064Am(aul = "bd09ebeccb6a3251c782f43106c428bc", aum = -2)
    public long iuB;
    @C0064Am(aul = "0e8320390b62c71e4b28931f87de514e", aum = -2)
    public long iuC;
    @C0064Am(aul = "1746882bafddaeb37b150fc52198e65f", aum = -2)
    public long iuD;
    @C0064Am(aul = "1fa89d48b676d419a6c1696ecdef0965", aum = -2)
    public long iuE;
    @C0064Am(aul = "adf5a21ac520af8b362e3e1c157764e6", aum = -2)
    public long iuF;
    @C0064Am(aul = "ac86c32c8d2c6b98e44ef3130c8a6146", aum = -2)
    public long iuG;
    @C0064Am(aul = "69f3fadf055251b81f1f002984fea596", aum = -2)
    public long iuH;
    @C0064Am(aul = "4619ac4910b1790f48dd025c5c8c1889", aum = -2)
    public long iuI;
    @C0064Am(aul = "8013e63b1d233493b79a1e48a1839b96", aum = -2)
    public long iuJ;
    @C0064Am(aul = "6d5dad9dec1adaf2c3d3444a5b78dad2", aum = -2)
    public long iuK;
    @C0064Am(aul = "adb9c25d672d3a4151b13847163ae68d", aum = -2)
    public long iuL;
    @C0064Am(aul = "c55786d1ab0e0f4600f15b684b70f284", aum = -2)
    public long iuM;
    @C0064Am(aul = "a38c5434b704a05116c57319b9150dcf", aum = -2)
    public long iuN;
    @C0064Am(aul = "78a94494558ec8ebf57bf7dc2bf98d9d", aum = -2)
    public long iuO;
    @C0064Am(aul = "ffa4c6cab09cf0e782b8bfcbb7c31105", aum = -2)
    public long iuP;
    @C0064Am(aul = "85ad077af9999d729561a6702b45fb5c", aum = -2)
    public long iuQ;
    @C0064Am(aul = "5010435f23a887886a397977f6a12d83", aum = -2)
    public long iuR;
    @C0064Am(aul = "3875f819a2cf015dba3d7a911b4eb4e1", aum = -2)
    public long iuS;
    @C0064Am(aul = "793e01f95ef8536602fd252f70c58f98", aum = -2)
    public long iuT;
    @C0064Am(aul = "746dd8b67eef6d066f76678094cc5eea", aum = -2)
    public long iuU;
    @C0064Am(aul = "9942eafa3e6cacb784012f7fbfd556d0", aum = -2)
    public long iuV;
    @C0064Am(aul = "229852970fffee012472f9ac45b3a666", aum = -2)
    public long iuW;
    @C0064Am(aul = "b32aeead5c67ecc4d54cd377c3f16af0", aum = -2)
    public long iuX;
    @C0064Am(aul = "15cf2e87452024c3e30f40b5eb485ef3", aum = -2)
    public long iuY;
    @C0064Am(aul = "41b75b60af5c4dbe52e03ff02c7a5d13", aum = -2)
    public long iuZ;
    @C0064Am(aul = "c51a8592ec3e25251cd361ecd20ddd63", aum = -2)
    public long iua;
    @C0064Am(aul = "3d47036606bd151848f82b06d664d5a2", aum = -2)
    public long iub;
    @C0064Am(aul = "5180523453849cb135127ad19d27cb56", aum = -2)
    public long iuc;
    @C0064Am(aul = "55362d1bcb671bd0f68719513c0282cf", aum = -2)
    public long iud;
    @C0064Am(aul = "e4c192227702947200cab4f7af6a1d29", aum = -2)
    public long iue;
    @C0064Am(aul = "74cff4a2d9494c75b9ccaca9aaf341b1", aum = -2)
    public long iuf;
    @C0064Am(aul = "d4b80533e58f52f4660c4cb93ff6f5df", aum = -2)
    public long iug;
    @C0064Am(aul = "b59340dc2df57d292d550899bb82d5c5", aum = -2)
    public long iuh;
    @C0064Am(aul = "ef4a1f3e5a08ed25e21f469005f58d71", aum = -2)
    public long iui;
    @C0064Am(aul = "43bc1e9a73af88ba5d1d2b82226598db", aum = -2)
    public long iuj;
    @C0064Am(aul = "cd30acee5d2c2b8a4ec8943c351fb519", aum = -2)
    public long iuk;
    @C0064Am(aul = "dddd86408e7e8541ca440622212b9743", aum = -2)
    public long iul;
    @C0064Am(aul = "bc2c4bc191da12c52f3b61e80abb8deb", aum = -2)
    public long ium;
    @C0064Am(aul = "d63c3bffb361bc982793a74f790b3b0c", aum = -2)
    public long iun;
    @C0064Am(aul = "f5f671ce3f549c1ad63bbecdf219e6f3", aum = -2)
    public long iuo;
    @C0064Am(aul = "1b69d88d1b32876217ff8c03ad1b5581", aum = -2)
    public long iup;
    @C0064Am(aul = "44753edf05230f76f825ce1569c5be4f", aum = -2)
    public long iuq;
    @C0064Am(aul = "53a8711a2eca9aea84584ad4e41be6f6", aum = -2)
    public long iur;
    @C0064Am(aul = "b88e86ae0f5115d0b43808c5cace3644", aum = -2)
    public long ius;
    @C0064Am(aul = "342a7aa7a8a1a5f0e5154253f00f5b34", aum = -2)
    public long iut;
    @C0064Am(aul = "9dbf638fa17b7568d75ced3a38a7c0b5", aum = -2)
    public long iuu;
    @C0064Am(aul = "886d30a753ab5329dd305ed69dbba5e2", aum = -2)
    public long iuv;
    @C0064Am(aul = "620f87f83db39a669e3d72438169b6c0", aum = -2)
    public long iuw;
    @C0064Am(aul = "b0480a65644dd3f193c9163db4cc94c8", aum = -2)
    public long iux;
    @C0064Am(aul = "1861ce90dbea8990b5d21f6698b9a27d", aum = -2)
    public long iuy;
    @C0064Am(aul = "b13158b02e82316b66b1504c7fa0f62c", aum = -2)
    public long iuz;
    @C0064Am(aul = "5180523453849cb135127ad19d27cb56", aum = -1)
    public byte ivA;
    @C0064Am(aul = "55362d1bcb671bd0f68719513c0282cf", aum = -1)
    public byte ivB;
    @C0064Am(aul = "e4c192227702947200cab4f7af6a1d29", aum = -1)
    public byte ivC;
    @C0064Am(aul = "74cff4a2d9494c75b9ccaca9aaf341b1", aum = -1)
    public byte ivD;
    @C0064Am(aul = "d4b80533e58f52f4660c4cb93ff6f5df", aum = -1)
    public byte ivE;
    @C0064Am(aul = "b59340dc2df57d292d550899bb82d5c5", aum = -1)
    public byte ivF;
    @C0064Am(aul = "ef4a1f3e5a08ed25e21f469005f58d71", aum = -1)
    public byte ivG;
    @C0064Am(aul = "43bc1e9a73af88ba5d1d2b82226598db", aum = -1)
    public byte ivH;
    @C0064Am(aul = "cd30acee5d2c2b8a4ec8943c351fb519", aum = -1)
    public byte ivI;
    @C0064Am(aul = "dddd86408e7e8541ca440622212b9743", aum = -1)
    public byte ivJ;
    @C0064Am(aul = "bc2c4bc191da12c52f3b61e80abb8deb", aum = -1)
    public byte ivK;
    @C0064Am(aul = "d63c3bffb361bc982793a74f790b3b0c", aum = -1)
    public byte ivL;
    @C0064Am(aul = "f5f671ce3f549c1ad63bbecdf219e6f3", aum = -1)
    public byte ivM;
    @C0064Am(aul = "1b69d88d1b32876217ff8c03ad1b5581", aum = -1)
    public byte ivN;
    @C0064Am(aul = "44753edf05230f76f825ce1569c5be4f", aum = -1)
    public byte ivO;
    @C0064Am(aul = "53a8711a2eca9aea84584ad4e41be6f6", aum = -1)
    public byte ivP;
    @C0064Am(aul = "b88e86ae0f5115d0b43808c5cace3644", aum = -1)
    public byte ivQ;
    @C0064Am(aul = "342a7aa7a8a1a5f0e5154253f00f5b34", aum = -1)
    public byte ivR;
    @C0064Am(aul = "9dbf638fa17b7568d75ced3a38a7c0b5", aum = -1)
    public byte ivS;
    @C0064Am(aul = "886d30a753ab5329dd305ed69dbba5e2", aum = -1)
    public byte ivT;
    @C0064Am(aul = "620f87f83db39a669e3d72438169b6c0", aum = -1)
    public byte ivU;
    @C0064Am(aul = "b0480a65644dd3f193c9163db4cc94c8", aum = -1)
    public byte ivV;
    @C0064Am(aul = "1861ce90dbea8990b5d21f6698b9a27d", aum = -1)
    public byte ivW;
    @C0064Am(aul = "b13158b02e82316b66b1504c7fa0f62c", aum = -1)
    public byte ivX;
    @C0064Am(aul = "3f0e5812d96db509fdb0e8a654b70fd4", aum = -1)
    public byte ivY;
    @C0064Am(aul = "bd09ebeccb6a3251c782f43106c428bc", aum = -1)
    public byte ivZ;
    @C0064Am(aul = "cd5906400bdc5032b6f3a93db986b423", aum = -2)
    public long iva;
    @C0064Am(aul = "a2fcb21a72ffbbf23789ac1b1f13155e", aum = -2)
    public long ivb;
    @C0064Am(aul = "61402922b0fce6cd61f62bf03d8a786d", aum = -2)
    public long ivc;
    @C0064Am(aul = "fce98b95151dccbc3c6ce5e149c551ff", aum = -2)
    public long ivd;
    @C0064Am(aul = "35acd4443c8095f0993c08cadd87c467", aum = -2)
    public long ive;
    @C0064Am(aul = "d739e1c39c595b1cde0f21b1986e10c4", aum = -1)
    public byte ivf;
    @C0064Am(aul = "c3e1df9faf79a29e6eacfdf2bbfbed3d", aum = -1)
    public byte ivg;
    @C0064Am(aul = "e262cebe0dc7e875a4ea79a3c1f28e87", aum = -1)
    public byte ivh;
    @C0064Am(aul = "e1ef1d7d36c2cecf84797b7aea056b86", aum = -1)
    public byte ivi;
    @C0064Am(aul = "0ded854935e000dd9a08168691262e98", aum = -1)
    public byte ivj;
    @C0064Am(aul = "0e42d8baef486c82e1d3c3ccef1021aa", aum = -1)
    public byte ivk;
    @C0064Am(aul = "d0a4baf1e9b6e403c995b7623a41bcae", aum = -1)
    public byte ivl;
    @C0064Am(aul = "fbc07682f6f68bedf60852c51fa1b0d1", aum = -1)
    public byte ivm;
    @C0064Am(aul = "8fcf7f889cc8f8b16a80466c352cdbc1", aum = -1)
    public byte ivn;
    @C0064Am(aul = "5c44eeffae3d0021344b9054b9ce02f9", aum = -1)
    public byte ivo;
    @C0064Am(aul = "9ef9ba379474f3b6038b849c2b448e80", aum = -1)
    public byte ivp;
    @C0064Am(aul = "eeae9ed70bc04d039adcf73d14d91bd5", aum = -1)
    public byte ivq;
    @C0064Am(aul = "09686e3ba94ca4685f171fb8b85f8dfd", aum = -1)
    public byte ivr;
    @C0064Am(aul = "3ef0d8bd22bf99c59b683834a6b80a4e", aum = -1)
    public byte ivs;
    @C0064Am(aul = "6e6ae2dc34df21f76b0106ad390ba8eb", aum = -1)
    public byte ivt;
    @C0064Am(aul = "89f24f1f91af67cbcba7abd8935ea76b", aum = -1)
    public byte ivu;
    @C0064Am(aul = "0f58fe747ce815948d418a95b181c12c", aum = -1)
    public byte ivv;
    @C0064Am(aul = "9f726f508fa0640701cad26f3a54a3df", aum = -1)
    public byte ivw;
    @C0064Am(aul = "51654e434aeca9c1e8f2f6433383d0aa", aum = -1)
    public byte ivx;
    @C0064Am(aul = "c51a8592ec3e25251cd361ecd20ddd63", aum = -1)
    public byte ivy;
    @C0064Am(aul = "3d47036606bd151848f82b06d664d5a2", aum = -1)
    public byte ivz;
    @C0064Am(aul = "61402922b0fce6cd61f62bf03d8a786d", aum = -1)
    public byte iwA;
    @C0064Am(aul = "fce98b95151dccbc3c6ce5e149c551ff", aum = -1)
    public byte iwB;
    @C0064Am(aul = "35acd4443c8095f0993c08cadd87c467", aum = -1)
    public byte iwC;
    @C0064Am(aul = "0e8320390b62c71e4b28931f87de514e", aum = -1)
    public byte iwa;
    @C0064Am(aul = "1746882bafddaeb37b150fc52198e65f", aum = -1)
    public byte iwb;
    @C0064Am(aul = "1fa89d48b676d419a6c1696ecdef0965", aum = -1)
    public byte iwc;
    @C0064Am(aul = "adf5a21ac520af8b362e3e1c157764e6", aum = -1)
    public byte iwd;
    @C0064Am(aul = "ac86c32c8d2c6b98e44ef3130c8a6146", aum = -1)
    public byte iwe;
    @C0064Am(aul = "69f3fadf055251b81f1f002984fea596", aum = -1)
    public byte iwf;
    @C0064Am(aul = "4619ac4910b1790f48dd025c5c8c1889", aum = -1)
    public byte iwg;
    @C0064Am(aul = "8013e63b1d233493b79a1e48a1839b96", aum = -1)
    public byte iwh;
    @C0064Am(aul = "6d5dad9dec1adaf2c3d3444a5b78dad2", aum = -1)
    public byte iwi;
    @C0064Am(aul = "adb9c25d672d3a4151b13847163ae68d", aum = -1)
    public byte iwj;
    @C0064Am(aul = "c55786d1ab0e0f4600f15b684b70f284", aum = -1)
    public byte iwk;
    @C0064Am(aul = "a38c5434b704a05116c57319b9150dcf", aum = -1)
    public byte iwl;
    @C0064Am(aul = "78a94494558ec8ebf57bf7dc2bf98d9d", aum = -1)
    public byte iwm;
    @C0064Am(aul = "ffa4c6cab09cf0e782b8bfcbb7c31105", aum = -1)
    public byte iwn;
    @C0064Am(aul = "85ad077af9999d729561a6702b45fb5c", aum = -1)
    public byte iwo;
    @C0064Am(aul = "5010435f23a887886a397977f6a12d83", aum = -1)
    public byte iwp;
    @C0064Am(aul = "3875f819a2cf015dba3d7a911b4eb4e1", aum = -1)
    public byte iwq;
    @C0064Am(aul = "793e01f95ef8536602fd252f70c58f98", aum = -1)
    public byte iwr;
    @C0064Am(aul = "746dd8b67eef6d066f76678094cc5eea", aum = -1)
    public byte iws;
    @C0064Am(aul = "9942eafa3e6cacb784012f7fbfd556d0", aum = -1)
    public byte iwt;
    @C0064Am(aul = "229852970fffee012472f9ac45b3a666", aum = -1)
    public byte iwu;
    @C0064Am(aul = "b32aeead5c67ecc4d54cd377c3f16af0", aum = -1)
    public byte iwv;
    @C0064Am(aul = "15cf2e87452024c3e30f40b5eb485ef3", aum = -1)
    public byte iww;
    @C0064Am(aul = "41b75b60af5c4dbe52e03ff02c7a5d13", aum = -1)
    public byte iwx;
    @C0064Am(aul = "cd5906400bdc5032b6f3a93db986b423", aum = -1)
    public byte iwy;
    @C0064Am(aul = "a2fcb21a72ffbbf23789ac1b1f13155e", aum = -1)
    public byte iwz;
    @C0064Am(aul = "ce3dfa9c23a7b8b97064b2464a4c69c7", aum = 46)

    /* renamed from: jT */
    public C3438ra<C6209aiZ> f3421jT;
    @C0064Am(aul = "ce3dfa9c23a7b8b97064b2464a4c69c7", aum = -2)

    /* renamed from: jY */
    public long f3422jY;
    @C0064Am(aul = "ce3dfa9c23a7b8b97064b2464a4c69c7", aum = -1)

    /* renamed from: kd */
    public byte f3423kd;
    @C0064Am(aul = "49db357b3491c3b2f57d324f8777a29a", aum = -2)

    /* renamed from: oL */
    public long f3424oL;
    @C0064Am(aul = "49db357b3491c3b2f57d324f8777a29a", aum = -1)

    /* renamed from: oS */
    public byte f3425oS;

    public aNG() {
    }

    public aNG(C1616Xf xf) {
        super(xf);
    }

    /* renamed from: c */
    public C5663aRz[] mo309c() {
        return Taikodom._m_fields;
    }

    /* renamed from: d */
    public void mo310d() {
        super.mo310d();
        this.f3420bK = null;
        this.cIY = null;
        this.cJa = null;
        this.cJc = null;
        this.cJe = null;
        this.cJg = null;
        this.cJi = null;
        this.cJk = null;
        this.cJm = null;
        this.cJo = null;
        this.cJq = null;
        this.cJs = null;
        this.cJu = null;
        this.cJw = null;
        this.cJy = null;
        this.cJA = null;
        this.cJC = null;
        this.cJE = null;
        this.cJG = null;
        this.cJI = null;
        this.boj = null;
        this.cJK = null;
        this.cJM = null;
        this.cJO = null;
        this.f3419OX = null;
        this.cJQ = null;
        this.cJS = null;
        this.aPp = null;
        this.cJU = null;
        this.aLD = null;
        this.cJW = null;
        this.cJY = null;
        this.cKa = null;
        this.cKc = null;
        this.aQT = null;
        this.cKe = null;
        this.aQm = null;
        this.cKg = null;
        this.cKi = null;
        this.cKk = null;
        this.cKm = null;
        this.cKo = null;
        this.cKq = null;
        this.cKs = null;
        this.cKu = null;
        this.cKw = null;
        this.f3421jT = null;
        this.cKy = null;
        this.cKA = null;
        this.cKC = null;
        this.cKD = null;
        this.cKF = null;
        this.cKH = null;
        this.cKJ = null;
        this.cKL = null;
        this.cKN = null;
        this.cKP = null;
        this.cKR = null;
        this.cKT = null;
        this.cKV = null;
        this.cKX = null;
        this.cKZ = null;
        this.cLb = null;
        this.cLd = null;
        this.cLf = null;
        this.cLh = null;
        this.cLj = null;
        this.cLl = null;
        this.cLm = null;
        this.cLo = null;
        this.cLq = null;
        this.cLs = null;
        this.cLu = null;
        this.cLw = null;
        this.cLy = null;
        this.cLA = null;
        this.cLC = null;
        this.cLE = null;
        this.cLG = null;
        this.cLI = null;
        this.cLK = null;
        this.cLM = null;
        this.cLO = null;
        this.cLQ = null;
        this.cLS = null;
        this.cLU = null;
        this.cLW = null;
        this.cLY = null;
        this.cMa = null;
        this.cMc = null;
        this.cMe = null;
        this.f3424oL = 0;
        this.itH = 0;
        this.itI = 0;
        this.itJ = 0;
        this.itK = 0;
        this.itL = 0;
        this.itM = 0;
        this.itN = 0;
        this.itO = 0;
        this.itP = 0;
        this.itQ = 0;
        this.cZA = 0;
        this.itR = 0;
        this.itS = 0;
        this.itT = 0;
        this.itU = 0;
        this.itV = 0;
        this.itW = 0;
        this.itX = 0;
        this.gdc = 0;
        this.imF = 0;
        this.itY = 0;
        this.itZ = 0;
        this.iua = 0;
        this.aso = 0;
        this.iub = 0;
        this.iuc = 0;
        this.gCS = 0;
        this.iud = 0;
        this.aLI = 0;
        this.iue = 0;
        this.iuf = 0;
        this.iug = 0;
        this.iuh = 0;
        this.eLw = 0;
        this.iui = 0;
        this.idg = 0;
        this.iuj = 0;
        this.iuk = 0;
        this.iul = 0;
        this.ium = 0;
        this.iun = 0;
        this.iuo = 0;
        this.exC = 0;
        this.iup = 0;
        this.iuq = 0;
        this.f3422jY = 0;
        this.iur = 0;
        this.hPr = 0;
        this.gSZ = 0;
        this.ius = 0;
        this.iut = 0;
        this.iuu = 0;
        this.iuv = 0;
        this.iuw = 0;
        this.iux = 0;
        this.iuy = 0;
        this.iuz = 0;
        this.iuA = 0;
        this.iuB = 0;
        this.cVn = 0;
        this.iuC = 0;
        this.iuD = 0;
        this.iuE = 0;
        this.iuF = 0;
        this.iuG = 0;
        this.iuH = 0;
        this.gsy = 0;
        this.iuI = 0;
        this.iuJ = 0;
        this.iuK = 0;
        this.iuL = 0;
        this.iuM = 0;
        this.iuN = 0;
        this.iuO = 0;
        this.iuP = 0;
        this.iuQ = 0;
        this.iuR = 0;
        this.iuS = 0;
        this.iuT = 0;
        this.iuU = 0;
        this.iuV = 0;
        this.iuW = 0;
        this.iuX = 0;
        this.iuY = 0;
        this.iuZ = 0;
        this.iva = 0;
        this.ivb = 0;
        this.ivc = 0;
        this.ivd = 0;
        this.ive = 0;
        this.f3425oS = 0;
        this.ivf = 0;
        this.ivg = 0;
        this.ivh = 0;
        this.ivi = 0;
        this.ivj = 0;
        this.ivk = 0;
        this.ivl = 0;
        this.ivm = 0;
        this.ivn = 0;
        this.ivo = 0;
        this.cZB = 0;
        this.ivp = 0;
        this.ivq = 0;
        this.ivr = 0;
        this.ivs = 0;
        this.ivt = 0;
        this.ivu = 0;
        this.ivv = 0;
        this.gdd = 0;
        this.imX = 0;
        this.ivw = 0;
        this.ivx = 0;
        this.ivy = 0;
        this.ast = 0;
        this.ivz = 0;
        this.ivA = 0;
        this.gCU = 0;
        this.ivB = 0;
        this.aLN = 0;
        this.ivC = 0;
        this.ivD = 0;
        this.ivE = 0;
        this.ivF = 0;
        this.eLD = 0;
        this.ivG = 0;
        this.idh = 0;
        this.ivH = 0;
        this.ivI = 0;
        this.ivJ = 0;
        this.ivK = 0;
        this.ivL = 0;
        this.ivM = 0;
        this.exD = 0;
        this.ivN = 0;
        this.ivO = 0;
        this.f3423kd = 0;
        this.ivP = 0;
        this.hPs = 0;
        this.gTe = 0;
        this.ivQ = 0;
        this.ivR = 0;
        this.ivS = 0;
        this.ivT = 0;
        this.ivU = 0;
        this.ivV = 0;
        this.ivW = 0;
        this.ivX = 0;
        this.ivY = 0;
        this.ivZ = 0;
        this.cVp = 0;
        this.iwa = 0;
        this.iwb = 0;
        this.iwc = 0;
        this.iwd = 0;
        this.iwe = 0;
        this.iwf = 0;
        this.gsC = 0;
        this.iwg = 0;
        this.iwh = 0;
        this.iwi = 0;
        this.iwj = 0;
        this.iwk = 0;
        this.iwl = 0;
        this.iwm = 0;
        this.iwn = 0;
        this.iwo = 0;
        this.iwp = 0;
        this.iwq = 0;
        this.iwr = 0;
        this.iws = 0;
        this.iwt = 0;
        this.iwu = 0;
        this.iwv = 0;
        this.iww = 0;
        this.iwx = 0;
        this.iwy = 0;
        this.iwz = 0;
        this.iwA = 0;
        this.iwB = 0;
        this.iwC = 0;
    }
}
