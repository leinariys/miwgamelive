package logic.data.mbean;

import game.script.missiontemplate.ExpeditionMissionInfo;
import game.script.npcchat.requirement.ExpeditionParamSpeechRequirement;
import logic.baa.C1616Xf;
import logic.res.code.C5663aRz;
import p001a.C0064Am;

/* renamed from: a.aNp  reason: case insensitive filesystem */
public class C5549aNp extends C5416aIm {
    @C0064Am(aul = "1fef29c0be9a3097482007d6a96aa06f", aum = 0)
    public int ecL;
    @C0064Am(aul = "9b705e627b3168bad0ad72fcf88f44fe", aum = 1)
    public ExpeditionMissionInfo ecN;
    @C0064Am(aul = "1fef29c0be9a3097482007d6a96aa06f", aum = -2)
    public long irw;
    @C0064Am(aul = "9b705e627b3168bad0ad72fcf88f44fe", aum = -2)
    public long irx;
    @C0064Am(aul = "1fef29c0be9a3097482007d6a96aa06f", aum = -1)
    public byte iry;
    @C0064Am(aul = "9b705e627b3168bad0ad72fcf88f44fe", aum = -1)
    public byte irz;

    public C5549aNp() {
    }

    public C5549aNp(C1616Xf xf) {
        super(xf);
    }

    /* renamed from: c */
    public C5663aRz[] mo309c() {
        return ExpeditionParamSpeechRequirement._m_fields;
    }

    /* renamed from: d */
    public void mo310d() {
        super.mo310d();
        this.ecL = 0;
        this.ecN = null;
        this.irw = 0;
        this.irx = 0;
        this.iry = 0;
        this.irz = 0;
    }
}
