package logic.data.mbean;

import game.network.message.serializable.C3438ra;
import game.script.citizenship.CitizenImprovementType;
import game.script.citizenship.CitizenshipReward;
import game.script.citizenship.CitizenshipType;
import game.script.resource.Asset;
import logic.baa.C1616Xf;
import logic.res.code.C5663aRz;
import p001a.C0064Am;
import taikodom.infra.script.I18NString;

/* renamed from: a.apc  reason: case insensitive filesystem */
public class C6576apc extends C2833km {
    @C0064Am(aul = "d07a4ba93c30fabda19b556bcdc4365d", aum = 1)

    /* renamed from: db */
    public long f5110db;
    @C0064Am(aul = "d07a4ba93c30fabda19b556bcdc4365d", aum = -2)

    /* renamed from: dj */
    public long f5111dj;
    @C0064Am(aul = "d07a4ba93c30fabda19b556bcdc4365d", aum = -1)

    /* renamed from: dq */
    public byte f5112dq;
    @C0064Am(aul = "2765773117cd6814ddddaedc5b26b335", aum = 5)
    public Asset eQY;
    @C0064Am(aul = "bda71d1dcf319613c3fc5f3e522340e9", aum = 6)
    public Asset eRa;
    @C0064Am(aul = "ba47049ed33b60d394446bade1290ec7", aum = 7)
    public Asset eRc;
    @C0064Am(aul = "5927681486d14162274755ca31229e15", aum = 8)
    public C3438ra<CitizenImprovementType> eRe;
    @C0064Am(aul = "d4aadb0f73ba3aa9351bc224f0de5957", aum = 9)
    public C3438ra<CitizenshipReward> eRf;
    @C0064Am(aul = "31889d3108ff4dd8fef3a1603105016e", aum = 10)
    public C3438ra<CitizenshipReward> eRh;
    @C0064Am(aul = "fd8f2ca8ea978974663e85094c54d633", aum = 11)
    public C3438ra<CitizenshipReward> eRj;
    @C0064Am(aul = "20c622621b93f523052dae317d3995dd", aum = 12)
    public C3438ra<CitizenshipReward> eRl;
    @C0064Am(aul = "2d4862b3c75f18df301e48a0752057a8", aum = 13)
    public C3438ra<CitizenshipReward> eRn;
    @C0064Am(aul = "060e6c338ec3bf61a3e1c2990926486e", aum = 2)
    public float egP;
    @C0064Am(aul = "060e6c338ec3bf61a3e1c2990926486e", aum = -2)
    public long egT;
    @C0064Am(aul = "060e6c338ec3bf61a3e1c2990926486e", aum = -1)
    public byte egX;
    @C0064Am(aul = "5927681486d14162274755ca31229e15", aum = -2)
    public long fEX;
    @C0064Am(aul = "5927681486d14162274755ca31229e15", aum = -1)
    public byte fFa;
    @C0064Am(aul = "2765773117cd6814ddddaedc5b26b335", aum = -2)
    public long gkT;
    @C0064Am(aul = "bda71d1dcf319613c3fc5f3e522340e9", aum = -2)
    public long gkU;
    @C0064Am(aul = "ba47049ed33b60d394446bade1290ec7", aum = -2)
    public long gkV;
    @C0064Am(aul = "d4aadb0f73ba3aa9351bc224f0de5957", aum = -2)
    public long gkW;
    @C0064Am(aul = "31889d3108ff4dd8fef3a1603105016e", aum = -2)
    public long gkX;
    @C0064Am(aul = "fd8f2ca8ea978974663e85094c54d633", aum = -2)
    public long gkY;
    @C0064Am(aul = "20c622621b93f523052dae317d3995dd", aum = -2)
    public long gkZ;
    @C0064Am(aul = "2d4862b3c75f18df301e48a0752057a8", aum = -2)
    public long gla;
    @C0064Am(aul = "2765773117cd6814ddddaedc5b26b335", aum = -1)
    public byte glb;
    @C0064Am(aul = "bda71d1dcf319613c3fc5f3e522340e9", aum = -1)
    public byte glc;
    @C0064Am(aul = "ba47049ed33b60d394446bade1290ec7", aum = -1)
    public byte gld;
    @C0064Am(aul = "d4aadb0f73ba3aa9351bc224f0de5957", aum = -1)
    public byte gle;
    @C0064Am(aul = "31889d3108ff4dd8fef3a1603105016e", aum = -1)
    public byte glf;
    @C0064Am(aul = "fd8f2ca8ea978974663e85094c54d633", aum = -1)
    public byte glg;
    @C0064Am(aul = "20c622621b93f523052dae317d3995dd", aum = -1)
    public byte glh;
    @C0064Am(aul = "2d4862b3c75f18df301e48a0752057a8", aum = -1)
    public byte gli;
    @C0064Am(aul = "15c2c8ce4a66c98aa77d3210c2fdc322", aum = -1)

    /* renamed from: jK */
    public byte f5113jK;
    @C0064Am(aul = "15c2c8ce4a66c98aa77d3210c2fdc322", aum = 4)

    /* renamed from: jn */
    public Asset f5114jn;
    @C0064Am(aul = "15c2c8ce4a66c98aa77d3210c2fdc322", aum = -2)

    /* renamed from: jy */
    public long f5115jy;
    @C0064Am(aul = "df7eb7dae6704f95765abdde15677aeb", aum = 0)

    /* renamed from: nh */
    public I18NString f5116nh;
    @C0064Am(aul = "f378837f9cfc21574c10415875586456", aum = 3)

    /* renamed from: ni */
    public I18NString f5117ni;
    @C0064Am(aul = "df7eb7dae6704f95765abdde15677aeb", aum = -2)

    /* renamed from: nk */
    public long f5118nk;
    @C0064Am(aul = "f378837f9cfc21574c10415875586456", aum = -2)

    /* renamed from: nl */
    public long f5119nl;
    @C0064Am(aul = "df7eb7dae6704f95765abdde15677aeb", aum = -1)

    /* renamed from: nn */
    public byte f5120nn;
    @C0064Am(aul = "f378837f9cfc21574c10415875586456", aum = -1)

    /* renamed from: no */
    public byte f5121no;

    public C6576apc() {
    }

    public C6576apc(C1616Xf xf) {
        super(xf);
    }

    /* renamed from: c */
    public C5663aRz[] mo309c() {
        return CitizenshipType._m_fields;
    }

    /* renamed from: d */
    public void mo310d() {
        super.mo310d();
        this.f5116nh = null;
        this.f5110db = 0;
        this.egP = 0.0f;
        this.f5117ni = null;
        this.f5114jn = null;
        this.eQY = null;
        this.eRa = null;
        this.eRc = null;
        this.eRe = null;
        this.eRf = null;
        this.eRh = null;
        this.eRj = null;
        this.eRl = null;
        this.eRn = null;
        this.f5118nk = 0;
        this.f5111dj = 0;
        this.egT = 0;
        this.f5119nl = 0;
        this.f5115jy = 0;
        this.gkT = 0;
        this.gkU = 0;
        this.gkV = 0;
        this.fEX = 0;
        this.gkW = 0;
        this.gkX = 0;
        this.gkY = 0;
        this.gkZ = 0;
        this.gla = 0;
        this.f5120nn = 0;
        this.f5112dq = 0;
        this.egX = 0;
        this.f5121no = 0;
        this.f5113jK = 0;
        this.glb = 0;
        this.glc = 0;
        this.gld = 0;
        this.fFa = 0;
        this.gle = 0;
        this.glf = 0;
        this.glg = 0;
        this.glh = 0;
        this.gli = 0;
    }
}
