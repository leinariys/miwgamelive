package logic.data.mbean;

import game.script.ship.hazardshield.HazardShield;
import logic.baa.C1616Xf;
import logic.res.code.C5663aRz;
import p001a.C0064Am;

/* renamed from: a.aRy  reason: case insensitive filesystem */
public class C5662aRy extends C6757atB {
    @C0064Am(aul = "0565fbe1f5d85616231f1497d2ca0a1e", aum = -2)

    /* renamed from: dl */
    public long f3728dl;
    @C0064Am(aul = "0565fbe1f5d85616231f1497d2ca0a1e", aum = -1)

    /* renamed from: ds */
    public byte f3729ds;
    @C0064Am(aul = "0565fbe1f5d85616231f1497d2ca0a1e", aum = 0)

    /* renamed from: pZ */
    public HazardShield f3730pZ;

    public C5662aRy() {
    }

    public C5662aRy(C1616Xf xf) {
        super(xf);
    }

    /* renamed from: c */
    public C5663aRz[] mo309c() {
        return HazardShield.InvulnerabilityEnterer._m_fields;
    }

    /* renamed from: d */
    public void mo310d() {
        super.mo310d();
        this.f3730pZ = null;
        this.f3728dl = 0;
        this.f3729ds = 0;
    }
}
