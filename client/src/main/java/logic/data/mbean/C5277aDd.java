package logic.data.mbean;

import game.script.item.SimpleBag;
import logic.baa.C1616Xf;
import logic.res.code.C5663aRz;
import p001a.C0064Am;

/* renamed from: a.aDd  reason: case insensitive filesystem */
public class C5277aDd extends C5838abS {
    @C0064Am(aul = "3429fa69366c75734fe86a76faabc767", aum = 0)
    public SimpleBag.SimpleBagLocation bHD;
    @C0064Am(aul = "3429fa69366c75734fe86a76faabc767", aum = -2)
    public long hxR;
    @C0064Am(aul = "3429fa69366c75734fe86a76faabc767", aum = -1)
    public byte hxS;

    public C5277aDd() {
    }

    public C5277aDd(C1616Xf xf) {
        super(xf);
    }

    /* renamed from: c */
    public C5663aRz[] mo309c() {
        return SimpleBag._m_fields;
    }

    /* renamed from: d */
    public void mo310d() {
        super.mo310d();
        this.bHD = null;
        this.hxR = 0;
        this.hxS = 0;
    }
}
