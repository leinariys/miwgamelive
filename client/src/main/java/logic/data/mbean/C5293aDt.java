package logic.data.mbean;

import game.network.message.serializable.C1556Wo;
import game.script.damage.DamageType;
import game.script.item.ClipType;
import game.script.resource.Asset;
import logic.baa.C1616Xf;
import logic.res.code.C5663aRz;
import p001a.C0064Am;

/* renamed from: a.aDt  reason: case insensitive filesystem */
public class C5293aDt extends aKP {
    @C0064Am(aul = "79fbdc1d68184f34c73bfb5f020f3e71", aum = 0)

    /* renamed from: LQ */
    public Asset f2597LQ;
    @C0064Am(aul = "6e3457b04d8e2d00b5704e251d4a224f", aum = 2)
    public int atB;
    @C0064Am(aul = "6e3457b04d8e2d00b5704e251d4a224f", aum = -2)
    public long atG;
    @C0064Am(aul = "6e3457b04d8e2d00b5704e251d4a224f", aum = -1)
    public byte atL;
    @C0064Am(aul = "25d4dbe5604c50f621728045b24720e1", aum = -2)
    public long avd;
    @C0064Am(aul = "25d4dbe5604c50f621728045b24720e1", aum = -1)
    public byte avi;
    @C0064Am(aul = "79fbdc1d68184f34c73bfb5f020f3e71", aum = -2)
    public long ayc;
    @C0064Am(aul = "79fbdc1d68184f34c73bfb5f020f3e71", aum = -1)
    public byte ayr;
    @C0064Am(aul = "52710271c155382bf100772222ebb571", aum = 1)
    public C1556Wo<DamageType, Float> bIv;
    @C0064Am(aul = "52710271c155382bf100772222ebb571", aum = -2)
    public long beq;
    @C0064Am(aul = "52710271c155382bf100772222ebb571", aum = -1)
    public byte bes;
    @C0064Am(aul = "25d4dbe5604c50f621728045b24720e1", aum = 3)

    /* renamed from: uT */
    public String f2598uT;

    public C5293aDt() {
    }

    public C5293aDt(C1616Xf xf) {
        super(xf);
    }

    /* renamed from: c */
    public C5663aRz[] mo309c() {
        return ClipType._m_fields;
    }

    /* renamed from: d */
    public void mo310d() {
        super.mo310d();
        this.f2597LQ = null;
        this.bIv = null;
        this.atB = 0;
        this.f2598uT = null;
        this.ayc = 0;
        this.beq = 0;
        this.atG = 0;
        this.avd = 0;
        this.ayr = 0;
        this.bes = 0;
        this.atL = 0;
        this.avi = 0;
    }
}
