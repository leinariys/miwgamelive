package logic.data.mbean;

import game.network.message.serializable.C3438ra;
import game.script.item.Item;
import game.script.player.Player;
import game.script.trade.TradePlayerStatus;
import logic.baa.C1616Xf;
import logic.res.code.C5663aRz;
import p001a.C0064Am;

/* renamed from: a.abP  reason: case insensitive filesystem */
public class C5835abP extends C5292aDs {
    @C0064Am(aul = "7b5371d6377191de44cfd499401908d6", aum = 0)

    /* renamed from: P */
    public Player f4135P;
    @C0064Am(aul = "7b5371d6377191de44cfd499401908d6", aum = -2)

    /* renamed from: ag */
    public long f4136ag;
    @C0064Am(aul = "7b5371d6377191de44cfd499401908d6", aum = -1)

    /* renamed from: av */
    public byte f4137av;
    @C0064Am(aul = "fe7a7d72b2a9a94862cb584f5c46cccf", aum = -2)
    public long eZN;
    @C0064Am(aul = "4dbd4ab9cf129aca4fba44ff6a910aca", aum = -2)
    public long eZO;
    @C0064Am(aul = "4ec9e48b8dc9a23caf8fd84981507ff7", aum = -2)
    public long eZP;
    @C0064Am(aul = "fe7a7d72b2a9a94862cb584f5c46cccf", aum = -1)
    public byte eZQ;
    @C0064Am(aul = "4dbd4ab9cf129aca4fba44ff6a910aca", aum = -1)
    public byte eZR;
    @C0064Am(aul = "4ec9e48b8dc9a23caf8fd84981507ff7", aum = -1)
    public byte eZS;
    @C0064Am(aul = "fe7a7d72b2a9a94862cb584f5c46cccf", aum = 1)

    /* renamed from: hA */
    public long f4138hA;
    @C0064Am(aul = "4dbd4ab9cf129aca4fba44ff6a910aca", aum = 2)

    /* renamed from: hC */
    public C3438ra<Item> f4139hC;
    @C0064Am(aul = "4ec9e48b8dc9a23caf8fd84981507ff7", aum = 3)

    /* renamed from: hD */
    public boolean f4140hD;

    public C5835abP() {
    }

    public C5835abP(C1616Xf xf) {
        super(xf);
    }

    /* renamed from: c */
    public C5663aRz[] mo309c() {
        return TradePlayerStatus._m_fields;
    }

    /* renamed from: d */
    public void mo310d() {
        super.mo310d();
        this.f4135P = null;
        this.f4138hA = 0;
        this.f4139hC = null;
        this.f4140hD = false;
        this.f4136ag = 0;
        this.eZN = 0;
        this.eZO = 0;
        this.eZP = 0;
        this.f4137av = 0;
        this.eZQ = 0;
        this.eZR = 0;
        this.eZS = 0;
    }
}
