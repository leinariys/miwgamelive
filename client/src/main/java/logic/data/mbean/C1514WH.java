package logic.data.mbean;

import game.script.item.buff.module.ShieldBuff;
import logic.baa.C1616Xf;
import logic.res.code.C5663aRz;
import p001a.C0064Am;
import p001a.C3892wO;

/* renamed from: a.WH */
public class C1514WH extends C4037yS {
    @C0064Am(aul = "454e9b2732a3d613ae853cb3f741f875", aum = 0)
    public C3892wO aBJ;
    @C0064Am(aul = "8377dea39d14749649790927519c5ed4", aum = 1)
    public C3892wO aBK;
    @C0064Am(aul = "c85b486c9da831b01525756f51b10fd8", aum = 2)
    public C3892wO aBL;
    @C0064Am(aul = "1e6e5302dfba79a09df1de3b80fef577", aum = 3)
    public C3892wO aBM;
    @C0064Am(aul = "9780d2a0f44c18867fec5ad07f8de409", aum = -2)
    public long aBN;
    @C0064Am(aul = "454e9b2732a3d613ae853cb3f741f875", aum = -2)
    public long aBO;
    @C0064Am(aul = "8377dea39d14749649790927519c5ed4", aum = -2)
    public long aBP;
    @C0064Am(aul = "c85b486c9da831b01525756f51b10fd8", aum = -2)
    public long aBQ;
    @C0064Am(aul = "1e6e5302dfba79a09df1de3b80fef577", aum = -2)
    public long aBR;
    @C0064Am(aul = "9780d2a0f44c18867fec5ad07f8de409", aum = -1)
    public byte aBS;
    @C0064Am(aul = "454e9b2732a3d613ae853cb3f741f875", aum = -1)
    public byte aBT;
    @C0064Am(aul = "8377dea39d14749649790927519c5ed4", aum = -1)
    public byte aBU;
    @C0064Am(aul = "c85b486c9da831b01525756f51b10fd8", aum = -1)
    public byte aBV;
    @C0064Am(aul = "1e6e5302dfba79a09df1de3b80fef577", aum = -1)
    public byte aBW;
    @C0064Am(aul = "9780d2a0f44c18867fec5ad07f8de409", aum = 4)
    public ShieldBuff.ShieldModuleAdapter eyr;

    public C1514WH() {
    }

    public C1514WH(C1616Xf xf) {
        super(xf);
    }

    /* renamed from: c */
    public C5663aRz[] mo309c() {
        return ShieldBuff._m_fields;
    }

    /* renamed from: d */
    public void mo310d() {
        super.mo310d();
        this.aBJ = null;
        this.aBK = null;
        this.aBL = null;
        this.aBM = null;
        this.eyr = null;
        this.aBO = 0;
        this.aBP = 0;
        this.aBQ = 0;
        this.aBR = 0;
        this.aBN = 0;
        this.aBT = 0;
        this.aBU = 0;
        this.aBV = 0;
        this.aBW = 0;
        this.aBS = 0;
    }
}
