package logic.data.mbean;

import game.network.message.serializable.C3438ra;
import game.script.item.RawMaterialType;
import game.script.pda.DatabaseCategory;
import game.script.pda.TaikopediaEntry;
import game.script.resource.Asset;
import logic.baa.C1616Xf;
import logic.res.code.C5663aRz;
import p001a.C0064Am;

/* renamed from: a.aD */
public class C1778aD extends C3909wd {
    @C0064Am(aul = "705b2ef607eb5e6f12c877ca759f83bd", aum = 0)

    /* renamed from: jS */
    public DatabaseCategory f2497jS;
    @C0064Am(aul = "12d78c3588efb2e5616c79699d49ba9f", aum = 1)

    /* renamed from: jT */
    public C3438ra<TaikopediaEntry> f2498jT;
    @C0064Am(aul = "5f53d0ddef0171d5e45fc6f1e0d24947", aum = 2)

    /* renamed from: jU */
    public Asset f2499jU;
    @C0064Am(aul = "9d31a43f7d727e51f3503f62e38e13ea", aum = 3)

    /* renamed from: jV */
    public float f2500jV;
    @C0064Am(aul = "a1ac1c216ff14fae6a8369c2a96712d5", aum = 4)

    /* renamed from: jW */
    public int f2501jW;
    @C0064Am(aul = "705b2ef607eb5e6f12c877ca759f83bd", aum = -2)

    /* renamed from: jX */
    public long f2502jX;
    @C0064Am(aul = "12d78c3588efb2e5616c79699d49ba9f", aum = -2)

    /* renamed from: jY */
    public long f2503jY;
    @C0064Am(aul = "5f53d0ddef0171d5e45fc6f1e0d24947", aum = -2)

    /* renamed from: jZ */
    public long f2504jZ;
    @C0064Am(aul = "9d31a43f7d727e51f3503f62e38e13ea", aum = -2)

    /* renamed from: ka */
    public long f2505ka;
    @C0064Am(aul = "a1ac1c216ff14fae6a8369c2a96712d5", aum = -2)

    /* renamed from: kb */
    public long f2506kb;
    @C0064Am(aul = "705b2ef607eb5e6f12c877ca759f83bd", aum = -1)

    /* renamed from: kc */
    public byte f2507kc;
    @C0064Am(aul = "12d78c3588efb2e5616c79699d49ba9f", aum = -1)

    /* renamed from: kd */
    public byte f2508kd;
    @C0064Am(aul = "5f53d0ddef0171d5e45fc6f1e0d24947", aum = -1)

    /* renamed from: ke */
    public byte f2509ke;
    @C0064Am(aul = "9d31a43f7d727e51f3503f62e38e13ea", aum = -1)

    /* renamed from: kf */
    public byte f2510kf;
    @C0064Am(aul = "a1ac1c216ff14fae6a8369c2a96712d5", aum = -1)

    /* renamed from: kg */
    public byte f2511kg;

    public C1778aD() {
    }

    public C1778aD(C1616Xf xf) {
        super(xf);
    }

    /* renamed from: c */
    public C5663aRz[] mo309c() {
        return RawMaterialType._m_fields;
    }

    /* renamed from: d */
    public void mo310d() {
        super.mo310d();
        this.f2497jS = null;
        this.f2498jT = null;
        this.f2499jU = null;
        this.f2500jV = 0.0f;
        this.f2501jW = 0;
        this.f2502jX = 0;
        this.f2503jY = 0;
        this.f2504jZ = 0;
        this.f2505ka = 0;
        this.f2506kb = 0;
        this.f2507kc = 0;
        this.f2508kd = 0;
        this.f2509ke = 0;
        this.f2510kf = 0;
        this.f2511kg = 0;
    }
}
