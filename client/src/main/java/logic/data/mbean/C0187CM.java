package logic.data.mbean;

import game.script.newmarket.economy.WarehouseAgent;
import game.script.newmarket.economy.WarehouseInventoryItem;
import logic.baa.C1616Xf;
import logic.res.code.C5663aRz;
import p001a.C0064Am;

/* renamed from: a.CM */
public class C0187CM extends C3805vD {
    @C0064Am(aul = "5b8681d4cd71d7ffa049e4737f4cf5ee", aum = 0)
    public WarehouseInventoryItem cAg;
    @C0064Am(aul = "970239ca8b3a91a6664ce70798479414", aum = 1)
    public WarehouseAgent.C1808a cAh;
    @C0064Am(aul = "5b8681d4cd71d7ffa049e4737f4cf5ee", aum = -2)
    public long cAi;
    @C0064Am(aul = "970239ca8b3a91a6664ce70798479414", aum = -2)
    public long cAj;
    @C0064Am(aul = "5b8681d4cd71d7ffa049e4737f4cf5ee", aum = -1)
    public byte cAk;
    @C0064Am(aul = "970239ca8b3a91a6664ce70798479414", aum = -1)
    public byte cAl;

    public C0187CM() {
    }

    public C0187CM(C1616Xf xf) {
        super(xf);
    }

    /* renamed from: c */
    public C5663aRz[] mo309c() {
        return WarehouseAgent._m_fields;
    }

    /* renamed from: d */
    public void mo310d() {
        super.mo310d();
        this.cAg = null;
        this.cAh = null;
        this.cAi = 0;
        this.cAj = 0;
        this.cAk = 0;
        this.cAl = 0;
    }
}
