package logic.data.mbean;

import game.script.mission.scripting.BuyOutpostMissionScript;
import logic.baa.C1616Xf;
import logic.res.code.C5663aRz;
import p001a.C0064Am;

/* renamed from: a.tM */
public class C3657tM extends C2955mD {
    @C0064Am(aul = "a3cba93eeda4c45cdcf5cb7b209cf84d", aum = 0)
    public BuyOutpostMissionScript btd;
    @C0064Am(aul = "a3cba93eeda4c45cdcf5cb7b209cf84d", aum = -2)

    /* renamed from: dl */
    public long f9235dl;
    @C0064Am(aul = "a3cba93eeda4c45cdcf5cb7b209cf84d", aum = -1)

    /* renamed from: ds */
    public byte f9236ds;

    public C3657tM() {
    }

    public C3657tM(C1616Xf xf) {
        super(xf);
    }

    /* renamed from: c */
    public C5663aRz[] mo309c() {
        return BuyOutpostMissionScript.StateA._m_fields;
    }

    /* renamed from: d */
    public void mo310d() {
        super.mo310d();
        this.btd = null;
        this.f9235dl = 0;
        this.f9236ds = 0;
    }
}
