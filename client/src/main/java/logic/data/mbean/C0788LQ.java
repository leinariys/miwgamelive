package logic.data.mbean;

import game.script.ship.CargoHold;
import game.script.ship.CargoHoldType;
import game.script.ship.categories.Freighter;
import logic.baa.C1616Xf;
import logic.res.code.C5663aRz;
import p001a.C0064Am;

/* renamed from: a.LQ */
public class C0788LQ extends aMX {
    @C0064Am(aul = "1f58cc3a72cc5f7d14bde835fc2bb221", aum = 0)
    public CargoHoldType dll;
    @C0064Am(aul = "750889105577686c549a9c10278e7bde", aum = 1)
    public CargoHold dln;
    @C0064Am(aul = "1f58cc3a72cc5f7d14bde835fc2bb221", aum = -2)
    public long dyl;
    @C0064Am(aul = "750889105577686c549a9c10278e7bde", aum = -2)
    public long dym;
    @C0064Am(aul = "1f58cc3a72cc5f7d14bde835fc2bb221", aum = -1)
    public byte dyn;
    @C0064Am(aul = "750889105577686c549a9c10278e7bde", aum = -1)
    public byte dyo;

    public C0788LQ() {
    }

    public C0788LQ(C1616Xf xf) {
        super(xf);
    }

    /* renamed from: c */
    public C5663aRz[] mo309c() {
        return Freighter._m_fields;
    }

    /* renamed from: d */
    public void mo310d() {
        super.mo310d();
        this.dll = null;
        this.dln = null;
        this.dyl = 0;
        this.dym = 0;
        this.dyn = 0;
        this.dyo = 0;
    }
}
