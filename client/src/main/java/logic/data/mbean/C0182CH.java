package logic.data.mbean;

import game.script.nls.NLSChat;
import logic.baa.C1616Xf;
import logic.res.code.C5663aRz;
import p001a.C0064Am;
import taikodom.infra.script.I18NString;

/* renamed from: a.CH */
public class C0182CH extends C2484fi {
    @C0064Am(aul = "837a370cc4102e4c8065da1924b18e47", aum = 14)
    public I18NString cyA;
    @C0064Am(aul = "98e57d63f254d6d655e69d305715f030", aum = 15)
    public I18NString cyB;
    @C0064Am(aul = "f19818feb1cae1b427784fcae19ac746", aum = 16)
    public I18NString cyC;
    @C0064Am(aul = "b472b56d503d7db82dece676a01e5c4a", aum = 17)
    public I18NString cyD;
    @C0064Am(aul = "cf01320fa045c5255b6b0590ca335165", aum = 18)
    public I18NString cyE;
    @C0064Am(aul = "d2b0a584c0471526ed44e03c043dc082", aum = 19)
    public I18NString cyF;
    @C0064Am(aul = "841a3200484ac3a4b014d4367ec364f2", aum = 20)
    public I18NString cyG;
    @C0064Am(aul = "4eebb9312b0c41af010ebd99e2f3dfa2", aum = 21)
    public I18NString cyH;
    @C0064Am(aul = "db2fa7f2f0d8e3e20b0214a0b6445fa9", aum = 22)
    public I18NString cyI;
    @C0064Am(aul = "ebedb4ec221bfc7d7b49c795f1b255c3", aum = 23)
    public I18NString cyJ;
    @C0064Am(aul = "e4cfea0347bf88bdf5a4ad73a4073000", aum = 24)
    public I18NString cyK;
    @C0064Am(aul = "ec2e1f09d12a1899923d7b524eb8c9a9", aum = 25)
    public I18NString cyL;
    @C0064Am(aul = "29338d4daa301641e6eedd97129f3ec8", aum = -2)
    public long cyM;
    @C0064Am(aul = "0539d4e185279ad36e3336a810e7acf2", aum = -2)
    public long cyN;
    @C0064Am(aul = "cd440dabfeb99c436cad0953bf98b531", aum = -2)
    public long cyO;
    @C0064Am(aul = "a2b645756ec13ea39ac510da68042665", aum = -2)
    public long cyP;
    @C0064Am(aul = "67d7718b1cf4898810a9f282111f3783", aum = -2)
    public long cyQ;
    @C0064Am(aul = "13a6c7aa50ad3f7828e4492cce62d615", aum = -2)
    public long cyR;
    @C0064Am(aul = "e47a29cecf5048cb74dba2b5f4b9ab56", aum = -2)
    public long cyS;
    @C0064Am(aul = "585e4fb01360ac840b91599423e688d5", aum = -2)
    public long cyT;
    @C0064Am(aul = "bcd7849d220e7b362509596b18c07408", aum = -2)
    public long cyU;
    @C0064Am(aul = "03f7b68eaafeb83d65eecf5da82e79a0", aum = -2)
    public long cyV;
    @C0064Am(aul = "1fe9761b6d5e95b86c5ad05be8263b7d", aum = -2)
    public long cyW;
    @C0064Am(aul = "143bed54df858c826ffe60db8e2fa1fc", aum = -2)
    public long cyX;
    @C0064Am(aul = "36663595bf41e16d3611b6e69880a77b", aum = -2)
    public long cyY;
    @C0064Am(aul = "e662765bc163dfc10ae0b7136366624d", aum = -2)
    public long cyZ;
    @C0064Am(aul = "29338d4daa301641e6eedd97129f3ec8", aum = 0)
    public I18NString cym;
    @C0064Am(aul = "0539d4e185279ad36e3336a810e7acf2", aum = 1)
    public I18NString cyn;
    @C0064Am(aul = "cd440dabfeb99c436cad0953bf98b531", aum = 2)
    public I18NString cyo;
    @C0064Am(aul = "a2b645756ec13ea39ac510da68042665", aum = 3)
    public I18NString cyp;
    @C0064Am(aul = "67d7718b1cf4898810a9f282111f3783", aum = 4)
    public I18NString cyq;
    @C0064Am(aul = "13a6c7aa50ad3f7828e4492cce62d615", aum = 5)
    public I18NString cyr;
    @C0064Am(aul = "e47a29cecf5048cb74dba2b5f4b9ab56", aum = 6)
    public I18NString cys;
    @C0064Am(aul = "585e4fb01360ac840b91599423e688d5", aum = 7)
    public I18NString cyt;
    @C0064Am(aul = "bcd7849d220e7b362509596b18c07408", aum = 8)
    public I18NString cyu;
    @C0064Am(aul = "03f7b68eaafeb83d65eecf5da82e79a0", aum = 9)
    public I18NString cyv;
    @C0064Am(aul = "1fe9761b6d5e95b86c5ad05be8263b7d", aum = 10)
    public I18NString cyw;
    @C0064Am(aul = "143bed54df858c826ffe60db8e2fa1fc", aum = 11)
    public I18NString cyx;
    @C0064Am(aul = "36663595bf41e16d3611b6e69880a77b", aum = 12)
    public I18NString cyy;
    @C0064Am(aul = "e662765bc163dfc10ae0b7136366624d", aum = 13)
    public I18NString cyz;
    @C0064Am(aul = "837a370cc4102e4c8065da1924b18e47", aum = -1)
    public byte czA;
    @C0064Am(aul = "98e57d63f254d6d655e69d305715f030", aum = -1)
    public byte czB;
    @C0064Am(aul = "f19818feb1cae1b427784fcae19ac746", aum = -1)
    public byte czC;
    @C0064Am(aul = "b472b56d503d7db82dece676a01e5c4a", aum = -1)
    public byte czD;
    @C0064Am(aul = "cf01320fa045c5255b6b0590ca335165", aum = -1)
    public byte czE;
    @C0064Am(aul = "d2b0a584c0471526ed44e03c043dc082", aum = -1)
    public byte czF;
    @C0064Am(aul = "841a3200484ac3a4b014d4367ec364f2", aum = -1)
    public byte czG;
    @C0064Am(aul = "4eebb9312b0c41af010ebd99e2f3dfa2", aum = -1)
    public byte czH;
    @C0064Am(aul = "db2fa7f2f0d8e3e20b0214a0b6445fa9", aum = -1)
    public byte czI;
    @C0064Am(aul = "ebedb4ec221bfc7d7b49c795f1b255c3", aum = -1)
    public byte czJ;
    @C0064Am(aul = "e4cfea0347bf88bdf5a4ad73a4073000", aum = -1)
    public byte czK;
    @C0064Am(aul = "ec2e1f09d12a1899923d7b524eb8c9a9", aum = -1)
    public byte czL;
    @C0064Am(aul = "837a370cc4102e4c8065da1924b18e47", aum = -2)
    public long cza;
    @C0064Am(aul = "98e57d63f254d6d655e69d305715f030", aum = -2)
    public long czb;
    @C0064Am(aul = "f19818feb1cae1b427784fcae19ac746", aum = -2)
    public long czc;
    @C0064Am(aul = "b472b56d503d7db82dece676a01e5c4a", aum = -2)
    public long czd;
    @C0064Am(aul = "cf01320fa045c5255b6b0590ca335165", aum = -2)
    public long cze;
    @C0064Am(aul = "d2b0a584c0471526ed44e03c043dc082", aum = -2)
    public long czf;
    @C0064Am(aul = "841a3200484ac3a4b014d4367ec364f2", aum = -2)
    public long czg;
    @C0064Am(aul = "4eebb9312b0c41af010ebd99e2f3dfa2", aum = -2)
    public long czh;
    @C0064Am(aul = "db2fa7f2f0d8e3e20b0214a0b6445fa9", aum = -2)
    public long czi;
    @C0064Am(aul = "ebedb4ec221bfc7d7b49c795f1b255c3", aum = -2)
    public long czj;
    @C0064Am(aul = "e4cfea0347bf88bdf5a4ad73a4073000", aum = -2)
    public long czk;
    @C0064Am(aul = "ec2e1f09d12a1899923d7b524eb8c9a9", aum = -2)
    public long czl;
    @C0064Am(aul = "29338d4daa301641e6eedd97129f3ec8", aum = -1)
    public byte czm;
    @C0064Am(aul = "0539d4e185279ad36e3336a810e7acf2", aum = -1)
    public byte czn;
    @C0064Am(aul = "cd440dabfeb99c436cad0953bf98b531", aum = -1)
    public byte czo;
    @C0064Am(aul = "a2b645756ec13ea39ac510da68042665", aum = -1)
    public byte czp;
    @C0064Am(aul = "67d7718b1cf4898810a9f282111f3783", aum = -1)
    public byte czq;
    @C0064Am(aul = "13a6c7aa50ad3f7828e4492cce62d615", aum = -1)
    public byte czr;
    @C0064Am(aul = "e47a29cecf5048cb74dba2b5f4b9ab56", aum = -1)
    public byte czs;
    @C0064Am(aul = "585e4fb01360ac840b91599423e688d5", aum = -1)
    public byte czt;
    @C0064Am(aul = "bcd7849d220e7b362509596b18c07408", aum = -1)
    public byte czu;
    @C0064Am(aul = "03f7b68eaafeb83d65eecf5da82e79a0", aum = -1)
    public byte czv;
    @C0064Am(aul = "1fe9761b6d5e95b86c5ad05be8263b7d", aum = -1)
    public byte czw;
    @C0064Am(aul = "143bed54df858c826ffe60db8e2fa1fc", aum = -1)
    public byte czx;
    @C0064Am(aul = "36663595bf41e16d3611b6e69880a77b", aum = -1)
    public byte czy;
    @C0064Am(aul = "e662765bc163dfc10ae0b7136366624d", aum = -1)
    public byte czz;

    public C0182CH() {
    }

    public C0182CH(C1616Xf xf) {
        super(xf);
    }

    /* renamed from: c */
    public C5663aRz[] mo309c() {
        return NLSChat._m_fields;
    }

    /* renamed from: d */
    public void mo310d() {
        super.mo310d();
        this.cym = null;
        this.cyn = null;
        this.cyo = null;
        this.cyp = null;
        this.cyq = null;
        this.cyr = null;
        this.cys = null;
        this.cyt = null;
        this.cyu = null;
        this.cyv = null;
        this.cyw = null;
        this.cyx = null;
        this.cyy = null;
        this.cyz = null;
        this.cyA = null;
        this.cyB = null;
        this.cyC = null;
        this.cyD = null;
        this.cyE = null;
        this.cyF = null;
        this.cyG = null;
        this.cyH = null;
        this.cyI = null;
        this.cyJ = null;
        this.cyK = null;
        this.cyL = null;
        this.cyM = 0;
        this.cyN = 0;
        this.cyO = 0;
        this.cyP = 0;
        this.cyQ = 0;
        this.cyR = 0;
        this.cyS = 0;
        this.cyT = 0;
        this.cyU = 0;
        this.cyV = 0;
        this.cyW = 0;
        this.cyX = 0;
        this.cyY = 0;
        this.cyZ = 0;
        this.cza = 0;
        this.czb = 0;
        this.czc = 0;
        this.czd = 0;
        this.cze = 0;
        this.czf = 0;
        this.czg = 0;
        this.czh = 0;
        this.czi = 0;
        this.czj = 0;
        this.czk = 0;
        this.czl = 0;
        this.czm = 0;
        this.czn = 0;
        this.czo = 0;
        this.czp = 0;
        this.czq = 0;
        this.czr = 0;
        this.czs = 0;
        this.czt = 0;
        this.czu = 0;
        this.czv = 0;
        this.czw = 0;
        this.czx = 0;
        this.czy = 0;
        this.czz = 0;
        this.czA = 0;
        this.czB = 0;
        this.czC = 0;
        this.czD = 0;
        this.czE = 0;
        this.czF = 0;
        this.czG = 0;
        this.czH = 0;
        this.czI = 0;
        this.czJ = 0;
        this.czK = 0;
        this.czL = 0;
    }
}
