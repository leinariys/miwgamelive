package logic.data.mbean;

import game.script.ai.npc.DroneAIControllerType;
import logic.baa.C1616Xf;
import logic.res.code.C5663aRz;
import p001a.C0064Am;

/* renamed from: a.atN  reason: case insensitive filesystem */
public class C6769atN extends C0757Kp {
    @C0064Am(aul = "d88ddce6045645c91a50dda25f1c4d40", aum = -2)
    public long bGi;
    @C0064Am(aul = "241348072fd9fe13845f67a0f5c4825f", aum = -2)
    public long bGj;
    @C0064Am(aul = "d88ddce6045645c91a50dda25f1c4d40", aum = -1)
    public byte bGl;
    @C0064Am(aul = "241348072fd9fe13845f67a0f5c4825f", aum = -1)
    public byte bGm;
    @C0064Am(aul = "812bd8b496d59371cba2978b433fcce3", aum = 4)
    public float dodgeAngle;
    @C0064Am(aul = "0f084d28688bc61da7763314af3114aa", aum = 5)
    public boolean dodgeDisabled;
    @C0064Am(aul = "c1d1c21b835a2ebb712822038ecb70a1", aum = 3)
    public float dodgeDistance;
    @C0064Am(aul = "ba126431149b0598bbc8371efdab6bba", aum = 2)
    public float dodgeReactionTime;
    @C0064Am(aul = "1a1fffe0ef4ea23b262bf0f5a7ca9d58", aum = -2)
    public long fFf;
    @C0064Am(aul = "824f7dad097148fbf4f854e4e66a88b4", aum = -2)
    public long fFg;
    @C0064Am(aul = "ba126431149b0598bbc8371efdab6bba", aum = -2)
    public long fFh;
    @C0064Am(aul = "c1d1c21b835a2ebb712822038ecb70a1", aum = -2)
    public long fFi;
    @C0064Am(aul = "812bd8b496d59371cba2978b433fcce3", aum = -2)
    public long fFj;
    @C0064Am(aul = "0f084d28688bc61da7763314af3114aa", aum = -2)
    public long fFk;
    @C0064Am(aul = "f8a1ae4ad7e66c7fcf562db52fb5eb8b", aum = -2)
    public long fFl;
    @C0064Am(aul = "1a1fffe0ef4ea23b262bf0f5a7ca9d58", aum = -1)
    public byte fFm;
    @C0064Am(aul = "824f7dad097148fbf4f854e4e66a88b4", aum = -1)
    public byte fFn;
    @C0064Am(aul = "ba126431149b0598bbc8371efdab6bba", aum = -1)
    public byte fFo;
    @C0064Am(aul = "c1d1c21b835a2ebb712822038ecb70a1", aum = -1)
    public byte fFp;
    @C0064Am(aul = "812bd8b496d59371cba2978b433fcce3", aum = -1)
    public byte fFq;
    @C0064Am(aul = "0f084d28688bc61da7763314af3114aa", aum = -1)
    public byte fFr;
    @C0064Am(aul = "f8a1ae4ad7e66c7fcf562db52fb5eb8b", aum = -1)
    public byte fFs;
    @C0064Am(aul = "f8a1ae4ad7e66c7fcf562db52fb5eb8b", aum = 8)
    public float initialReactionTime;
    @C0064Am(aul = "1a1fffe0ef4ea23b262bf0f5a7ca9d58", aum = 0)
    public float pursuitDistance;
    @C0064Am(aul = "824f7dad097148fbf4f854e4e66a88b4", aum = 1)
    public boolean pursuitPredictMovimet;
    @C0064Am(aul = "d88ddce6045645c91a50dda25f1c4d40", aum = 6)
    public float shootAngle;
    @C0064Am(aul = "241348072fd9fe13845f67a0f5c4825f", aum = 7)
    public boolean shootPredictMovimet;

    public C6769atN() {
    }

    public C6769atN(C1616Xf xf) {
        super(xf);
    }

    /* renamed from: c */
    public C5663aRz[] mo309c() {
        return DroneAIControllerType._m_fields;
    }

    /* renamed from: d */
    public void mo310d() {
        super.mo310d();
        this.pursuitDistance = 0.0f;
        this.pursuitPredictMovimet = false;
        this.dodgeReactionTime = 0.0f;
        this.dodgeDistance = 0.0f;
        this.dodgeAngle = 0.0f;
        this.dodgeDisabled = false;
        this.shootAngle = 0.0f;
        this.shootPredictMovimet = false;
        this.initialReactionTime = 0.0f;
        this.fFf = 0;
        this.fFg = 0;
        this.fFh = 0;
        this.fFi = 0;
        this.fFj = 0;
        this.fFk = 0;
        this.bGi = 0;
        this.bGj = 0;
        this.fFl = 0;
        this.fFm = 0;
        this.fFn = 0;
        this.fFo = 0;
        this.fFp = 0;
        this.fFq = 0;
        this.fFr = 0;
        this.bGl = 0;
        this.bGm = 0;
        this.fFs = 0;
    }
}
