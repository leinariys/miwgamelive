package logic.data.mbean;

import game.script.missiontemplate.ExpeditionMissionInfo;
import logic.baa.C1616Xf;
import logic.res.code.C5663aRz;
import p001a.C0064Am;

import java.util.UUID;

/* renamed from: a.alN  reason: case insensitive filesystem */
public class C6353alN extends C5292aDs {
    @C0064Am(aul = "75113634b12f9201513d909680bce820", aum = 2)

    /* renamed from: bK */
    public UUID f4846bK;
    @C0064Am(aul = "f0e6d605a33cea9a87dfaacd49dc0109", aum = 0)
    public String dSe;
    @C0064Am(aul = "44f2f153d3ea4049e79417d6cd2ba84c", aum = 1)
    public int dSg;
    @C0064Am(aul = "f0e6d605a33cea9a87dfaacd49dc0109", aum = -2)
    public long fXN;
    @C0064Am(aul = "44f2f153d3ea4049e79417d6cd2ba84c", aum = -2)
    public long fXO;
    @C0064Am(aul = "f0e6d605a33cea9a87dfaacd49dc0109", aum = -1)
    public byte fXP;
    @C0064Am(aul = "44f2f153d3ea4049e79417d6cd2ba84c", aum = -1)
    public byte fXQ;
    @C0064Am(aul = "08026068f59b67975795a864ac00eb5c", aum = 3)
    public String handle;
    @C0064Am(aul = "75113634b12f9201513d909680bce820", aum = -2)

    /* renamed from: oL */
    public long f4847oL;
    @C0064Am(aul = "75113634b12f9201513d909680bce820", aum = -1)

    /* renamed from: oS */
    public byte f4848oS;
    @C0064Am(aul = "08026068f59b67975795a864ac00eb5c", aum = -2)

    /* renamed from: ok */
    public long f4849ok;
    @C0064Am(aul = "08026068f59b67975795a864ac00eb5c", aum = -1)

    /* renamed from: on */
    public byte f4850on;

    public C6353alN() {
    }

    public C6353alN(C1616Xf xf) {
        super(xf);
    }

    /* renamed from: c */
    public C5663aRz[] mo309c() {
        return ExpeditionMissionInfo._m_fields;
    }

    /* renamed from: d */
    public void mo310d() {
        super.mo310d();
        this.dSe = null;
        this.dSg = 0;
        this.f4846bK = null;
        this.handle = null;
        this.fXN = 0;
        this.fXO = 0;
        this.f4847oL = 0;
        this.f4849ok = 0;
        this.fXP = 0;
        this.fXQ = 0;
        this.f4848oS = 0;
        this.f4850on = 0;
    }
}
