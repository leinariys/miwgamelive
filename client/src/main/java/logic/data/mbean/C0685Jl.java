package logic.data.mbean;

import game.network.message.serializable.C3438ra;
import game.script.pda.DatabaseCategory;
import game.script.pda.TaikopediaEntry;
import game.script.resource.Asset;
import game.script.space.SpaceCategory;
import logic.baa.C1616Xf;
import logic.res.code.C5663aRz;
import p001a.C0064Am;
import taikodom.infra.script.I18NString;

import java.util.UUID;

/* renamed from: a.Jl */
public class C0685Jl extends C3805vD {
    @C0064Am(aul = "849f84e963f13cd4449e6b6253c29f21", aum = 8)

    /* renamed from: bK */
    public UUID f854bK;
    @C0064Am(aul = "5e2360d977b81e67459fe7f482ea3565", aum = 0)
    public String handle;
    @C0064Am(aul = "c2e0b57f4c6cde4ecd0fda031aa61a6e", aum = -1)

    /* renamed from: jK */
    public byte f855jK;
    @C0064Am(aul = "074ca0ade414977a7f3924d537b99e29", aum = 5)

    /* renamed from: jS */
    public DatabaseCategory f856jS;
    @C0064Am(aul = "aab8c078d2f3ecda6ffc682d45f88d90", aum = 6)

    /* renamed from: jT */
    public C3438ra<TaikopediaEntry> f857jT;
    @C0064Am(aul = "19bc996008a99c4b84ff603a3c77afdd", aum = 2)

    /* renamed from: jU */
    public Asset f858jU;
    @C0064Am(aul = "2f52a0ca238bc1f2ed09aa8dd9ef41e4", aum = 7)

    /* renamed from: jV */
    public float f859jV;
    @C0064Am(aul = "074ca0ade414977a7f3924d537b99e29", aum = -2)

    /* renamed from: jX */
    public long f860jX;
    @C0064Am(aul = "aab8c078d2f3ecda6ffc682d45f88d90", aum = -2)

    /* renamed from: jY */
    public long f861jY;
    @C0064Am(aul = "19bc996008a99c4b84ff603a3c77afdd", aum = -2)

    /* renamed from: jZ */
    public long f862jZ;
    @C0064Am(aul = "c2e0b57f4c6cde4ecd0fda031aa61a6e", aum = 1)

    /* renamed from: jn */
    public Asset f863jn;
    @C0064Am(aul = "c2e0b57f4c6cde4ecd0fda031aa61a6e", aum = -2)

    /* renamed from: jy */
    public long f864jy;
    @C0064Am(aul = "2f52a0ca238bc1f2ed09aa8dd9ef41e4", aum = -2)

    /* renamed from: ka */
    public long f865ka;
    @C0064Am(aul = "074ca0ade414977a7f3924d537b99e29", aum = -1)

    /* renamed from: kc */
    public byte f866kc;
    @C0064Am(aul = "aab8c078d2f3ecda6ffc682d45f88d90", aum = -1)

    /* renamed from: kd */
    public byte f867kd;
    @C0064Am(aul = "19bc996008a99c4b84ff603a3c77afdd", aum = -1)

    /* renamed from: ke */
    public byte f868ke;
    @C0064Am(aul = "2f52a0ca238bc1f2ed09aa8dd9ef41e4", aum = -1)

    /* renamed from: kf */
    public byte f869kf;
    @C0064Am(aul = "e91f863fedbb076805aab2c8b55aab3b", aum = 4)

    /* renamed from: nh */
    public I18NString f870nh;
    @C0064Am(aul = "a0b9a5d017d3acc6854c4f16e1356181", aum = 3)

    /* renamed from: ni */
    public I18NString f871ni;
    @C0064Am(aul = "e91f863fedbb076805aab2c8b55aab3b", aum = -2)

    /* renamed from: nk */
    public long f872nk;
    @C0064Am(aul = "a0b9a5d017d3acc6854c4f16e1356181", aum = -2)

    /* renamed from: nl */
    public long f873nl;
    @C0064Am(aul = "e91f863fedbb076805aab2c8b55aab3b", aum = -1)

    /* renamed from: nn */
    public byte f874nn;
    @C0064Am(aul = "a0b9a5d017d3acc6854c4f16e1356181", aum = -1)

    /* renamed from: no */
    public byte f875no;
    @C0064Am(aul = "849f84e963f13cd4449e6b6253c29f21", aum = -2)

    /* renamed from: oL */
    public long f876oL;
    @C0064Am(aul = "849f84e963f13cd4449e6b6253c29f21", aum = -1)

    /* renamed from: oS */
    public byte f877oS;
    @C0064Am(aul = "5e2360d977b81e67459fe7f482ea3565", aum = -2)

    /* renamed from: ok */
    public long f878ok;
    @C0064Am(aul = "5e2360d977b81e67459fe7f482ea3565", aum = -1)

    /* renamed from: on */
    public byte f879on;

    public C0685Jl() {
    }

    public C0685Jl(C1616Xf xf) {
        super(xf);
    }

    /* renamed from: c */
    public C5663aRz[] mo309c() {
        return SpaceCategory._m_fields;
    }

    /* renamed from: d */
    public void mo310d() {
        super.mo310d();
        this.handle = null;
        this.f863jn = null;
        this.f858jU = null;
        this.f871ni = null;
        this.f870nh = null;
        this.f856jS = null;
        this.f857jT = null;
        this.f859jV = 0.0f;
        this.f854bK = null;
        this.f878ok = 0;
        this.f864jy = 0;
        this.f862jZ = 0;
        this.f873nl = 0;
        this.f872nk = 0;
        this.f860jX = 0;
        this.f861jY = 0;
        this.f865ka = 0;
        this.f876oL = 0;
        this.f879on = 0;
        this.f855jK = 0;
        this.f868ke = 0;
        this.f875no = 0;
        this.f874nn = 0;
        this.f866kc = 0;
        this.f867kd = 0;
        this.f869kf = 0;
        this.f877oS = 0;
    }
}
