package logic.data.mbean;

import game.script.npc.NPCType;
import game.script.spacezone.population.StaticNPCSpawn;
import logic.baa.C1616Xf;
import logic.res.code.C5663aRz;
import p001a.C0064Am;

import java.util.UUID;

/* renamed from: a.Hk */
public class C0557Hk extends C3805vD {
    @C0064Am(aul = "702574f96ff590c33c5f3c0a9a7f6092", aum = 2)

    /* renamed from: bK */
    public UUID f680bK;
    @C0064Am(aul = "193399ec8b7bc0ac6c29343ebad88f27", aum = 1)

    /* renamed from: cZ */
    public int f681cZ;
    @C0064Am(aul = "193399ec8b7bc0ac6c29343ebad88f27", aum = -2)

    /* renamed from: dh */
    public long f682dh;
    @C0064Am(aul = "193399ec8b7bc0ac6c29343ebad88f27", aum = -1)

    /* renamed from: do */
    public byte f683do;
    @C0064Am(aul = "07d901f8a863b53b43ad802eaa97b9e7", aum = 3)
    public String handle;
    @C0064Am(aul = "eb67480cbe357b337f227d563abdb8f4", aum = 0)

    /* renamed from: nC */
    public NPCType f684nC;
    @C0064Am(aul = "eb67480cbe357b337f227d563abdb8f4", aum = -2)

    /* renamed from: nG */
    public long f685nG;
    @C0064Am(aul = "eb67480cbe357b337f227d563abdb8f4", aum = -1)

    /* renamed from: nK */
    public byte f686nK;
    @C0064Am(aul = "702574f96ff590c33c5f3c0a9a7f6092", aum = -2)

    /* renamed from: oL */
    public long f687oL;
    @C0064Am(aul = "702574f96ff590c33c5f3c0a9a7f6092", aum = -1)

    /* renamed from: oS */
    public byte f688oS;
    @C0064Am(aul = "07d901f8a863b53b43ad802eaa97b9e7", aum = -2)

    /* renamed from: ok */
    public long f689ok;
    @C0064Am(aul = "07d901f8a863b53b43ad802eaa97b9e7", aum = -1)

    /* renamed from: on */
    public byte f690on;

    public C0557Hk() {
    }

    public C0557Hk(C1616Xf xf) {
        super(xf);
    }

    /* renamed from: c */
    public C5663aRz[] mo309c() {
        return StaticNPCSpawn._m_fields;
    }

    /* renamed from: d */
    public void mo310d() {
        super.mo310d();
        this.f684nC = null;
        this.f681cZ = 0;
        this.f680bK = null;
        this.handle = null;
        this.f685nG = 0;
        this.f682dh = 0;
        this.f687oL = 0;
        this.f689ok = 0;
        this.f686nK = 0;
        this.f683do = 0;
        this.f688oS = 0;
        this.f690on = 0;
    }
}
