package logic.data.mbean;

import game.network.message.serializable.C1556Wo;
import game.script.item.ItemType;
import game.script.newmarket.store.GlobalEconomyInfo;
import logic.baa.C1616Xf;
import logic.res.code.C5663aRz;
import p001a.C0064Am;

import java.util.UUID;

/* renamed from: a.agY  reason: case insensitive filesystem */
public class C6104agY extends C3805vD {
    @C0064Am(aul = "79daf6ae744bd6ca7a6a57144ee6f83c", aum = 0)
    public float aDT;
    @C0064Am(aul = "cbeacf9fb206f8a618ec42b73872a32b", aum = 1)
    public float aDV;
    @C0064Am(aul = "663daa90d85748ba745a32faa7e36cd5", aum = 2)
    public float aDX;
    @C0064Am(aul = "c774da56a2f63ff8f1d338d7dafed0ae", aum = 3)
    public float aDZ;
    @C0064Am(aul = "213e3f82281c83584d7cf2f901a095cf", aum = 17)
    public float aEB;
    @C0064Am(aul = "0268e5f2b0060bf9be3ea1d099d05b6e", aum = 18)
    public float aED;
    @C0064Am(aul = "6716e9298a752325203f0297bb3d6bb4", aum = 19)
    public float aEF;
    @C0064Am(aul = "29107569333344597a5a09145b603f43", aum = 21)
    public C1556Wo<ItemType, GlobalEconomyInfo.TransactionLog> aEH;
    @C0064Am(aul = "9b9b07fad2600b103cb44d25ef8cc86d", aum = 4)
    public float aEb;
    @C0064Am(aul = "d457710da5e8c900091919a8358ed815", aum = 5)
    public float aEd;
    @C0064Am(aul = "5badc547988bbd5a227d9ccafe7100af", aum = 6)
    public float aEf;
    @C0064Am(aul = "510272fd151be90d93179e35304deac8", aum = 7)
    public float aEh;
    @C0064Am(aul = "3c41dc5101e4814b42602b23ab3ee203", aum = 8)
    public float aEj;
    @C0064Am(aul = "0dd77f576ba1c5e2680b8879ad0a0832", aum = 9)
    public float aEl;
    @C0064Am(aul = "2d5d4e0271de6927747f341497311b60", aum = 10)
    public float aEn;
    @C0064Am(aul = "919249f8421ea0efba7d10b8166f5ecc", aum = 11)
    public float aEp;
    @C0064Am(aul = "39f7dccc234a838fca418447cb38daaa", aum = 12)
    public float aEr;
    @C0064Am(aul = "b51922909e5c2a000ead3dd7bc378562", aum = 13)
    public float aEt;
    @C0064Am(aul = "e401d08f4b497961d1340e0b92d7537b", aum = 14)
    public float aEv;
    @C0064Am(aul = "371fb0e6d7369806820bd9ff5caa4a32", aum = 15)
    public float aEx;
    @C0064Am(aul = "5d941ee7de60f357d1d39231385ae928", aum = 16)
    public float aEz;
    @C0064Am(aul = "e0974087d526452441a3f9e916700a4a", aum = 20)

    /* renamed from: bK */
    public UUID f4522bK;
    @C0064Am(aul = "79daf6ae744bd6ca7a6a57144ee6f83c", aum = -2)
    public long fFJ;
    @C0064Am(aul = "cbeacf9fb206f8a618ec42b73872a32b", aum = -2)
    public long fFK;
    @C0064Am(aul = "663daa90d85748ba745a32faa7e36cd5", aum = -2)
    public long fFL;
    @C0064Am(aul = "c774da56a2f63ff8f1d338d7dafed0ae", aum = -2)
    public long fFM;
    @C0064Am(aul = "9b9b07fad2600b103cb44d25ef8cc86d", aum = -2)
    public long fFN;
    @C0064Am(aul = "d457710da5e8c900091919a8358ed815", aum = -2)
    public long fFO;
    @C0064Am(aul = "5badc547988bbd5a227d9ccafe7100af", aum = -2)
    public long fFP;
    @C0064Am(aul = "510272fd151be90d93179e35304deac8", aum = -2)
    public long fFQ;
    @C0064Am(aul = "3c41dc5101e4814b42602b23ab3ee203", aum = -2)
    public long fFR;
    @C0064Am(aul = "0dd77f576ba1c5e2680b8879ad0a0832", aum = -2)
    public long fFS;
    @C0064Am(aul = "2d5d4e0271de6927747f341497311b60", aum = -2)
    public long fFT;
    @C0064Am(aul = "919249f8421ea0efba7d10b8166f5ecc", aum = -2)
    public long fFU;
    @C0064Am(aul = "39f7dccc234a838fca418447cb38daaa", aum = -2)
    public long fFV;
    @C0064Am(aul = "b51922909e5c2a000ead3dd7bc378562", aum = -2)
    public long fFW;
    @C0064Am(aul = "e401d08f4b497961d1340e0b92d7537b", aum = -2)
    public long fFX;
    @C0064Am(aul = "371fb0e6d7369806820bd9ff5caa4a32", aum = -2)
    public long fFY;
    @C0064Am(aul = "5d941ee7de60f357d1d39231385ae928", aum = -2)
    public long fFZ;
    @C0064Am(aul = "213e3f82281c83584d7cf2f901a095cf", aum = -2)
    public long fGa;
    @C0064Am(aul = "0268e5f2b0060bf9be3ea1d099d05b6e", aum = -2)
    public long fGb;
    @C0064Am(aul = "6716e9298a752325203f0297bb3d6bb4", aum = -2)
    public long fGc;
    @C0064Am(aul = "29107569333344597a5a09145b603f43", aum = -2)
    public long fGd;
    @C0064Am(aul = "79daf6ae744bd6ca7a6a57144ee6f83c", aum = -1)
    public byte fGe;
    @C0064Am(aul = "cbeacf9fb206f8a618ec42b73872a32b", aum = -1)
    public byte fGf;
    @C0064Am(aul = "663daa90d85748ba745a32faa7e36cd5", aum = -1)
    public byte fGg;
    @C0064Am(aul = "c774da56a2f63ff8f1d338d7dafed0ae", aum = -1)
    public byte fGh;
    @C0064Am(aul = "9b9b07fad2600b103cb44d25ef8cc86d", aum = -1)
    public byte fGi;
    @C0064Am(aul = "d457710da5e8c900091919a8358ed815", aum = -1)
    public byte fGj;
    @C0064Am(aul = "5badc547988bbd5a227d9ccafe7100af", aum = -1)
    public byte fGk;
    @C0064Am(aul = "510272fd151be90d93179e35304deac8", aum = -1)
    public byte fGl;
    @C0064Am(aul = "3c41dc5101e4814b42602b23ab3ee203", aum = -1)
    public byte fGm;
    @C0064Am(aul = "0dd77f576ba1c5e2680b8879ad0a0832", aum = -1)
    public byte fGn;
    @C0064Am(aul = "2d5d4e0271de6927747f341497311b60", aum = -1)
    public byte fGo;
    @C0064Am(aul = "919249f8421ea0efba7d10b8166f5ecc", aum = -1)
    public byte fGp;
    @C0064Am(aul = "39f7dccc234a838fca418447cb38daaa", aum = -1)
    public byte fGq;
    @C0064Am(aul = "b51922909e5c2a000ead3dd7bc378562", aum = -1)
    public byte fGr;
    @C0064Am(aul = "e401d08f4b497961d1340e0b92d7537b", aum = -1)
    public byte fGs;
    @C0064Am(aul = "371fb0e6d7369806820bd9ff5caa4a32", aum = -1)
    public byte fGt;
    @C0064Am(aul = "5d941ee7de60f357d1d39231385ae928", aum = -1)
    public byte fGu;
    @C0064Am(aul = "213e3f82281c83584d7cf2f901a095cf", aum = -1)
    public byte fGv;
    @C0064Am(aul = "0268e5f2b0060bf9be3ea1d099d05b6e", aum = -1)
    public byte fGw;
    @C0064Am(aul = "6716e9298a752325203f0297bb3d6bb4", aum = -1)
    public byte fGx;
    @C0064Am(aul = "29107569333344597a5a09145b603f43", aum = -1)
    public byte fGy;
    @C0064Am(aul = "e0974087d526452441a3f9e916700a4a", aum = -2)

    /* renamed from: oL */
    public long f4523oL;
    @C0064Am(aul = "e0974087d526452441a3f9e916700a4a", aum = -1)

    /* renamed from: oS */
    public byte f4524oS;

    public C6104agY() {
    }

    public C6104agY(C1616Xf xf) {
        super(xf);
    }

    /* renamed from: c */
    public C5663aRz[] mo309c() {
        return GlobalEconomyInfo._m_fields;
    }

    /* renamed from: d */
    public void mo310d() {
        super.mo310d();
        this.aDT = 0.0f;
        this.aDV = 0.0f;
        this.aDX = 0.0f;
        this.aDZ = 0.0f;
        this.aEb = 0.0f;
        this.aEd = 0.0f;
        this.aEf = 0.0f;
        this.aEh = 0.0f;
        this.aEj = 0.0f;
        this.aEl = 0.0f;
        this.aEn = 0.0f;
        this.aEp = 0.0f;
        this.aEr = 0.0f;
        this.aEt = 0.0f;
        this.aEv = 0.0f;
        this.aEx = 0.0f;
        this.aEz = 0.0f;
        this.aEB = 0.0f;
        this.aED = 0.0f;
        this.aEF = 0.0f;
        this.f4522bK = null;
        this.aEH = null;
        this.fFJ = 0;
        this.fFK = 0;
        this.fFL = 0;
        this.fFM = 0;
        this.fFN = 0;
        this.fFO = 0;
        this.fFP = 0;
        this.fFQ = 0;
        this.fFR = 0;
        this.fFS = 0;
        this.fFT = 0;
        this.fFU = 0;
        this.fFV = 0;
        this.fFW = 0;
        this.fFX = 0;
        this.fFY = 0;
        this.fFZ = 0;
        this.fGa = 0;
        this.fGb = 0;
        this.fGc = 0;
        this.f4523oL = 0;
        this.fGd = 0;
        this.fGe = 0;
        this.fGf = 0;
        this.fGg = 0;
        this.fGh = 0;
        this.fGi = 0;
        this.fGj = 0;
        this.fGk = 0;
        this.fGl = 0;
        this.fGm = 0;
        this.fGn = 0;
        this.fGo = 0;
        this.fGp = 0;
        this.fGq = 0;
        this.fGr = 0;
        this.fGs = 0;
        this.fGt = 0;
        this.fGu = 0;
        this.fGv = 0;
        this.fGw = 0;
        this.fGx = 0;
        this.f4524oS = 0;
        this.fGy = 0;
    }
}
