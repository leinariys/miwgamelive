package logic.data.mbean;

import game.network.message.serializable.C1556Wo;
import game.script.item.ItemType;
import game.script.mission.scripting.ItemDeliveryMissionScript;
import logic.baa.C1616Xf;
import logic.res.code.C5663aRz;
import p001a.C0064Am;

/* renamed from: a.anI  reason: case insensitive filesystem */
public class C6452anI extends aMY {
    @C0064Am(aul = "749c1521b74b1150f35702defb836a24", aum = 0)
    public C1556Wo<ItemType, Integer> cyi;
    @C0064Am(aul = "58f7abf1c0ef478c69465a98eb9f308e", aum = 1)
    public C1556Wo<ItemType, Integer> cyk;
    @C0064Am(aul = "749c1521b74b1150f35702defb836a24", aum = -2)
    public long dCT;
    @C0064Am(aul = "58f7abf1c0ef478c69465a98eb9f308e", aum = -2)
    public long dCU;
    @C0064Am(aul = "749c1521b74b1150f35702defb836a24", aum = -1)
    public byte dCV;
    @C0064Am(aul = "58f7abf1c0ef478c69465a98eb9f308e", aum = -1)
    public byte dCW;

    public C6452anI() {
    }

    public C6452anI(C1616Xf xf) {
        super(xf);
    }

    /* renamed from: c */
    public C5663aRz[] mo309c() {
        return ItemDeliveryMissionScript._m_fields;
    }

    /* renamed from: d */
    public void mo310d() {
        super.mo310d();
        this.cyi = null;
        this.cyk = null;
        this.dCT = 0;
        this.dCU = 0;
        this.dCV = 0;
        this.dCW = 0;
    }
}
