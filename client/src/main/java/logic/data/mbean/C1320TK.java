package logic.data.mbean;

import game.script.mission.actions.SetTimer;
import logic.baa.C1616Xf;
import logic.res.code.C5663aRz;
import p001a.C0064Am;

/* renamed from: a.TK */
public class C1320TK extends C2503fv {
    @C0064Am(aul = "d90ab2085568e2bb37d7b5dc27b9a753", aum = 0)

    /* renamed from: Dc */
    public String f1703Dc;
    @C0064Am(aul = "b9ac7a3d62520e019ec8100bd3799ffc", aum = 2)

    /* renamed from: Df */
    public boolean f1704Df;
    @C0064Am(aul = "d90ab2085568e2bb37d7b5dc27b9a753", aum = -2)
    public long aMO;
    @C0064Am(aul = "d90ab2085568e2bb37d7b5dc27b9a753", aum = -1)
    public byte aMP;
    @C0064Am(aul = "763c1230ec9cbf26bb45aed42bfdaf30", aum = -2)
    public long ekH;
    @C0064Am(aul = "b9ac7a3d62520e019ec8100bd3799ffc", aum = -2)
    public long ekI;
    @C0064Am(aul = "763c1230ec9cbf26bb45aed42bfdaf30", aum = -1)
    public byte ekJ;
    @C0064Am(aul = "b9ac7a3d62520e019ec8100bd3799ffc", aum = -1)
    public byte ekK;
    @C0064Am(aul = "763c1230ec9cbf26bb45aed42bfdaf30", aum = 1)
    public long time;

    public C1320TK() {
    }

    public C1320TK(C1616Xf xf) {
        super(xf);
    }

    /* renamed from: c */
    public C5663aRz[] mo309c() {
        return SetTimer._m_fields;
    }

    /* renamed from: d */
    public void mo310d() {
        super.mo310d();
        this.f1703Dc = null;
        this.time = 0;
        this.f1704Df = false;
        this.aMO = 0;
        this.ekH = 0;
        this.ekI = 0;
        this.aMP = 0;
        this.ekJ = 0;
        this.ekK = 0;
    }
}
