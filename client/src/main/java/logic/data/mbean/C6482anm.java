package logic.data.mbean;

import game.network.message.serializable.C3438ra;
import game.script.ship.ShieldType;
import game.script.ship.ShipSectorType;
import game.script.ship.ShipStructureType;
import logic.baa.C1616Xf;
import logic.res.code.C5663aRz;
import p001a.C0064Am;

/* renamed from: a.anm  reason: case insensitive filesystem */
public class C6482anm extends C6515aoT {
    @C0064Am(aul = "e2afc7750d156dd49b6eb620b8c347a4", aum = 0)
    public C3438ra<ShipSectorType> cJI;
    @C0064Am(aul = "7136691a90fb788c749ca5f4023b11ed", aum = 1)
    public ShieldType fPa;
    @C0064Am(aul = "7136691a90fb788c749ca5f4023b11ed", aum = -2)
    public long fPh;
    @C0064Am(aul = "7136691a90fb788c749ca5f4023b11ed", aum = -1)
    public byte fPo;
    @C0064Am(aul = "e2afc7750d156dd49b6eb620b8c347a4", aum = -2)
    public long gdc;
    @C0064Am(aul = "e2afc7750d156dd49b6eb620b8c347a4", aum = -1)
    public byte gdd;

    public C6482anm() {
    }

    public C6482anm(C1616Xf xf) {
        super(xf);
    }

    /* renamed from: c */
    public C5663aRz[] mo309c() {
        return ShipStructureType._m_fields;
    }

    /* renamed from: d */
    public void mo310d() {
        super.mo310d();
        this.cJI = null;
        this.fPa = null;
        this.gdc = 0;
        this.fPh = 0;
        this.gdd = 0;
        this.fPo = 0;
    }
}
