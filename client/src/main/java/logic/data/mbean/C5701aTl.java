package logic.data.mbean;

import game.script.item.buff.module.CruiseSpeedBuff;
import game.script.ship.CruiseSpeedAdapter;
import logic.baa.C1616Xf;
import logic.res.code.C5663aRz;
import p001a.C0064Am;
import p001a.C3892wO;

/* renamed from: a.aTl  reason: case insensitive filesystem */
public class C5701aTl extends C4037yS {
    @C0064Am(aul = "a447b11b400d1e77774ccc9c9075c43e", aum = 0)

    /* renamed from: Vs */
    public C3892wO f3817Vs;
    @C0064Am(aul = "b0e587372d4b73e5bf26a10b9a61253b", aum = 1)

    /* renamed from: Vu */
    public C3892wO f3818Vu;
    @C0064Am(aul = "97021fceb0d3f8c5f683fda91c875202", aum = 2)

    /* renamed from: Vw */
    public C3892wO f3819Vw;
    @C0064Am(aul = "02748d4dd5dea9a7c1f8bd5639e2af4a", aum = 3)

    /* renamed from: Vy */
    public C3892wO f3820Vy;
    @C0064Am(aul = "10421d1a2383fea796e72c7783897ebf", aum = -2)

    /* renamed from: YA */
    public long f3821YA;
    @C0064Am(aul = "6aeb85a4fbc519cacabe1c2dad055819", aum = -2)

    /* renamed from: YB */
    public long f3822YB;
    @C0064Am(aul = "a447b11b400d1e77774ccc9c9075c43e", aum = -1)

    /* renamed from: YC */
    public byte f3823YC;
    @C0064Am(aul = "b0e587372d4b73e5bf26a10b9a61253b", aum = -1)

    /* renamed from: YD */
    public byte f3824YD;
    @C0064Am(aul = "97021fceb0d3f8c5f683fda91c875202", aum = -1)

    /* renamed from: YE */
    public byte f3825YE;
    @C0064Am(aul = "02748d4dd5dea9a7c1f8bd5639e2af4a", aum = -1)

    /* renamed from: YF */
    public byte f3826YF;
    @C0064Am(aul = "10421d1a2383fea796e72c7783897ebf", aum = -1)

    /* renamed from: YG */
    public byte f3827YG;
    @C0064Am(aul = "6aeb85a4fbc519cacabe1c2dad055819", aum = -1)

    /* renamed from: YH */
    public byte f3828YH;
    @C0064Am(aul = "10421d1a2383fea796e72c7783897ebf", aum = 4)

    /* renamed from: Yu */
    public C3892wO f3829Yu;
    @C0064Am(aul = "6aeb85a4fbc519cacabe1c2dad055819", aum = 5)

    /* renamed from: Yv */
    public C3892wO f3830Yv;
    @C0064Am(aul = "a447b11b400d1e77774ccc9c9075c43e", aum = -2)

    /* renamed from: Yw */
    public long f3831Yw;
    @C0064Am(aul = "b0e587372d4b73e5bf26a10b9a61253b", aum = -2)

    /* renamed from: Yx */
    public long f3832Yx;
    @C0064Am(aul = "97021fceb0d3f8c5f683fda91c875202", aum = -2)

    /* renamed from: Yy */
    public long f3833Yy;
    @C0064Am(aul = "02748d4dd5dea9a7c1f8bd5639e2af4a", aum = -2)

    /* renamed from: Yz */
    public long f3834Yz;
    @C0064Am(aul = "b69e01713a216abb7e3db7284eb077df", aum = 7)
    public CruiseSpeedAdapter aYd;
    @C0064Am(aul = "ac3feb7b5563d32eb0d882e1bf39ec2d", aum = 6)
    public boolean bEl;
    @C0064Am(aul = "ac3feb7b5563d32eb0d882e1bf39ec2d", aum = -2)
    public long cUh;
    @C0064Am(aul = "ac3feb7b5563d32eb0d882e1bf39ec2d", aum = -1)
    public byte cUj;
    @C0064Am(aul = "b69e01713a216abb7e3db7284eb077df", aum = -2)
    public long hET;
    @C0064Am(aul = "b69e01713a216abb7e3db7284eb077df", aum = -1)
    public byte hEU;

    public C5701aTl() {
    }

    public C5701aTl(C1616Xf xf) {
        super(xf);
    }

    /* renamed from: c */
    public C5663aRz[] mo309c() {
        return CruiseSpeedBuff._m_fields;
    }

    /* renamed from: d */
    public void mo310d() {
        super.mo310d();
        this.f3817Vs = null;
        this.f3818Vu = null;
        this.f3819Vw = null;
        this.f3820Vy = null;
        this.f3829Yu = null;
        this.f3830Yv = null;
        this.bEl = false;
        this.aYd = null;
        this.f3831Yw = 0;
        this.f3832Yx = 0;
        this.f3833Yy = 0;
        this.f3834Yz = 0;
        this.f3821YA = 0;
        this.f3822YB = 0;
        this.cUh = 0;
        this.hET = 0;
        this.f3823YC = 0;
        this.f3824YD = 0;
        this.f3825YE = 0;
        this.f3826YF = 0;
        this.f3827YG = 0;
        this.f3828YH = 0;
        this.cUj = 0;
        this.hEU = 0;
    }
}
