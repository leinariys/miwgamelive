package logic.data.mbean;

import game.script.corporation.NLSCorporation;
import logic.baa.C1616Xf;
import logic.res.code.C5663aRz;
import p001a.C0064Am;
import taikodom.infra.script.I18NString;

/* renamed from: a.aJi  reason: case insensitive filesystem */
public class C5438aJi extends C2484fi {
    @C0064Am(aul = "c9bd0f1a24850c5147c365da1f16c4b3", aum = 1)
    public I18NString ifB;
    @C0064Am(aul = "c9bd0f1a24850c5147c365da1f16c4b3", aum = -2)
    public long ifC;
    @C0064Am(aul = "c9bd0f1a24850c5147c365da1f16c4b3", aum = -1)
    public byte ifD;
    @C0064Am(aul = "b98319db07ac561bec47310f581c07e9", aum = 0)

    /* renamed from: ni */
    public I18NString f3216ni;
    @C0064Am(aul = "b98319db07ac561bec47310f581c07e9", aum = -2)

    /* renamed from: nl */
    public long f3217nl;
    @C0064Am(aul = "b98319db07ac561bec47310f581c07e9", aum = -1)

    /* renamed from: no */
    public byte f3218no;

    public C5438aJi() {
    }

    public C5438aJi(C1616Xf xf) {
        super(xf);
    }

    /* renamed from: c */
    public C5663aRz[] mo309c() {
        return NLSCorporation.NamePanel._m_fields;
    }

    /* renamed from: d */
    public void mo310d() {
        super.mo310d();
        this.f3216ni = null;
        this.ifB = null;
        this.f3217nl = 0;
        this.ifC = 0;
        this.f3218no = 0;
        this.ifD = 0;
    }
}
