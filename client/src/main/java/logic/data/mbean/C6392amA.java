package logic.data.mbean;

import game.script.main.bot.BotScript;
import game.script.main.bot.MissionABCTemplate;
import game.script.ship.Station;
import logic.baa.C1616Xf;
import logic.res.code.C5663aRz;
import p001a.C0064Am;

/* renamed from: a.amA  reason: case insensitive filesystem */
public class C6392amA extends C1756Zw {
    @C0064Am(aul = "ab5abe8e8f52630e7412b455824e4227", aum = 1)

    /* renamed from: iH */
    public Station f4898iH;
    @C0064Am(aul = "ab5abe8e8f52630e7412b455824e4227", aum = -2)

    /* renamed from: iK */
    public long f4899iK;
    @C0064Am(aul = "ab5abe8e8f52630e7412b455824e4227", aum = -1)

    /* renamed from: iN */
    public byte f4900iN;
    @C0064Am(aul = "5fdcfc591899779b5098648b5f65ac70", aum = -2)

    /* renamed from: nD */
    public long f4901nD;
    @C0064Am(aul = "5fdcfc591899779b5098648b5f65ac70", aum = -1)

    /* renamed from: nH */
    public byte f4902nH;
    @C0064Am(aul = "5fdcfc591899779b5098648b5f65ac70", aum = 0)

    /* renamed from: nz */
    public BotScript f4903nz;

    public C6392amA() {
    }

    public C6392amA(C1616Xf xf) {
        super(xf);
    }

    /* renamed from: c */
    public C5663aRz[] mo309c() {
        return MissionABCTemplate._m_fields;
    }

    /* renamed from: d */
    public void mo310d() {
        super.mo310d();
        this.f4903nz = null;
        this.f4898iH = null;
        this.f4901nD = 0;
        this.f4899iK = 0;
        this.f4902nH = 0;
        this.f4900iN = 0;
    }
}
