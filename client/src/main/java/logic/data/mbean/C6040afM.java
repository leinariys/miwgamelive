package logic.data.mbean;

import game.script.damage.DamageType;
import game.script.resource.Asset;
import logic.baa.C1616Xf;
import logic.res.code.C5663aRz;
import p001a.C0064Am;
import taikodom.infra.script.I18NString;

import java.util.UUID;

/* renamed from: a.afM  reason: case insensitive filesystem */
public class C6040afM extends C3805vD {
    @C0064Am(aul = "a3a1af3d4df0a13fb2ecbae729fe9a26", aum = 4)

    /* renamed from: bK */
    public UUID f4456bK;
    @C0064Am(aul = "e617b36ceba6047ba7b3ca215a3f3561", aum = 0)
    public I18NString cWN;
    @C0064Am(aul = "ffcd9ab0ce22f43298f6932526a99f94", aum = 2)
    public boolean cWP;
    @C0064Am(aul = "e617b36ceba6047ba7b3ca215a3f3561", aum = -1)
    public byte fvA;
    @C0064Am(aul = "ffcd9ab0ce22f43298f6932526a99f94", aum = -1)
    public byte fvB;
    @C0064Am(aul = "e617b36ceba6047ba7b3ca215a3f3561", aum = -2)
    public long fvy;
    @C0064Am(aul = "ffcd9ab0ce22f43298f6932526a99f94", aum = -2)
    public long fvz;
    @C0064Am(aul = "fc28152ae77561bc80491f83ba891af2", aum = 1)
    public String handle;
    @C0064Am(aul = "823d31057e9546d49a9d3c1fe05817d6", aum = -1)

    /* renamed from: jK */
    public byte f4457jK;
    @C0064Am(aul = "823d31057e9546d49a9d3c1fe05817d6", aum = 3)

    /* renamed from: jn */
    public Asset f4458jn;
    @C0064Am(aul = "823d31057e9546d49a9d3c1fe05817d6", aum = -2)

    /* renamed from: jy */
    public long f4459jy;
    @C0064Am(aul = "a3a1af3d4df0a13fb2ecbae729fe9a26", aum = -2)

    /* renamed from: oL */
    public long f4460oL;
    @C0064Am(aul = "a3a1af3d4df0a13fb2ecbae729fe9a26", aum = -1)

    /* renamed from: oS */
    public byte f4461oS;
    @C0064Am(aul = "fc28152ae77561bc80491f83ba891af2", aum = -2)

    /* renamed from: ok */
    public long f4462ok;
    @C0064Am(aul = "fc28152ae77561bc80491f83ba891af2", aum = -1)

    /* renamed from: on */
    public byte f4463on;

    public C6040afM() {
    }

    public C6040afM(C1616Xf xf) {
        super(xf);
    }

    /* renamed from: c */
    public C5663aRz[] mo309c() {
        return DamageType._m_fields;
    }

    /* renamed from: d */
    public void mo310d() {
        super.mo310d();
        this.cWN = null;
        this.handle = null;
        this.cWP = false;
        this.f4458jn = null;
        this.f4456bK = null;
        this.fvy = 0;
        this.f4462ok = 0;
        this.fvz = 0;
        this.f4459jy = 0;
        this.f4460oL = 0;
        this.fvA = 0;
        this.f4463on = 0;
        this.fvB = 0;
        this.f4457jK = 0;
        this.f4461oS = 0;
    }
}
