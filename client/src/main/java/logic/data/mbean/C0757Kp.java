package logic.data.mbean;

import game.script.ai.npc.AIControllerType;
import logic.baa.C1616Xf;
import logic.res.code.C5663aRz;
import p001a.C0064Am;

/* renamed from: a.Kp */
public class C0757Kp extends C6515aoT {
    @C0064Am(aul = "b0c6833dd77c94aa128b95e2f510a82f", aum = -2)
    public long dpN;
    @C0064Am(aul = "b0c6833dd77c94aa128b95e2f510a82f", aum = -1)
    public byte dpO;
    @C0064Am(aul = "b0c6833dd77c94aa128b95e2f510a82f", aum = 0)
    public int nanFloodControl;

    public C0757Kp() {
    }

    public C0757Kp(C1616Xf xf) {
        super(xf);
    }

    /* renamed from: c */
    public C5663aRz[] mo309c() {
        return AIControllerType._m_fields;
    }

    /* renamed from: d */
    public void mo310d() {
        super.mo310d();
        this.nanFloodControl = 0;
        this.dpN = 0;
        this.dpO = 0;
    }
}
