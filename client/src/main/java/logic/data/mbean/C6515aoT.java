package logic.data.mbean;

import game.script.template.BaseTaikodomContent;
import logic.baa.C1616Xf;
import logic.res.code.C5663aRz;
import p001a.C0064Am;

import java.util.UUID;

/* renamed from: a.aoT  reason: case insensitive filesystem */
public class C6515aoT extends C6602aqC {
    @C0064Am(aul = "ce6a427447221bc22a7949a2292a90bd", aum = 1)

    /* renamed from: bK */
    public UUID f5007bK;
    @C0064Am(aul = "1ab375e32555b3fed47d4ae93b4472ea", aum = 0)
    public String handle;
    @C0064Am(aul = "ce6a427447221bc22a7949a2292a90bd", aum = -2)

    /* renamed from: oL */
    public long f5008oL;
    @C0064Am(aul = "ce6a427447221bc22a7949a2292a90bd", aum = -1)

    /* renamed from: oS */
    public byte f5009oS;
    @C0064Am(aul = "1ab375e32555b3fed47d4ae93b4472ea", aum = -2)

    /* renamed from: ok */
    public long f5010ok;
    @C0064Am(aul = "1ab375e32555b3fed47d4ae93b4472ea", aum = -1)

    /* renamed from: on */
    public byte f5011on;

    public C6515aoT() {
    }

    public C6515aoT(C1616Xf xf) {
        super(xf);
    }

    /* renamed from: c */
    public C5663aRz[] mo309c() {
        return BaseTaikodomContent._m_fields;
    }

    /* renamed from: d */
    public void mo310d() {
        super.mo310d();
        this.handle = null;
        this.f5007bK = null;
        this.f5010ok = 0;
        this.f5008oL = 0;
        this.f5011on = 0;
        this.f5009oS = 0;
    }
}
