package logic.data.mbean;

import game.script.item.buff.amplifier.HullAmplifierType;
import logic.baa.C1616Xf;
import logic.res.code.C5663aRz;
import p001a.C0064Am;
import p001a.C3892wO;

/* renamed from: a.ama  reason: case insensitive filesystem */
public class C6418ama extends C2421fH {
    @C0064Am(aul = "af2acce03d892eb2682123aa34d10f59", aum = 0)
    public C3892wO aBJ;
    @C0064Am(aul = "b6a672f889deef4e57fe7ff0ea462302", aum = 1)
    public C3892wO aBK;
    @C0064Am(aul = "af2acce03d892eb2682123aa34d10f59", aum = -2)
    public long aBO;
    @C0064Am(aul = "b6a672f889deef4e57fe7ff0ea462302", aum = -2)
    public long aBP;
    @C0064Am(aul = "af2acce03d892eb2682123aa34d10f59", aum = -1)
    public byte aBT;
    @C0064Am(aul = "b6a672f889deef4e57fe7ff0ea462302", aum = -1)
    public byte aBU;

    public C6418ama() {
    }

    public C6418ama(C1616Xf xf) {
        super(xf);
    }

    /* renamed from: c */
    public C5663aRz[] mo309c() {
        return HullAmplifierType._m_fields;
    }

    /* renamed from: d */
    public void mo310d() {
        super.mo310d();
        this.aBJ = null;
        this.aBK = null;
        this.aBO = 0;
        this.aBP = 0;
        this.aBT = 0;
        this.aBU = 0;
    }
}
