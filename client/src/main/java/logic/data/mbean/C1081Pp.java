package logic.data.mbean;

import game.script.TemporaryFeatureBlock;
import logic.baa.C1616Xf;
import logic.res.code.C5663aRz;
import p001a.C0064Am;

/* renamed from: a.Pp */
public class C1081Pp extends C5292aDs {
    @C0064Am(aul = "83c09fc78ea61633fb95095e55a57ee0", aum = 0)
    public boolean dQc;
    @C0064Am(aul = "3894f51eef4ff170a58313a94d979719", aum = 1)
    public boolean dQd;
    @C0064Am(aul = "83c09fc78ea61633fb95095e55a57ee0", aum = -2)
    public long dQe;
    @C0064Am(aul = "3894f51eef4ff170a58313a94d979719", aum = -2)
    public long dQf;
    @C0064Am(aul = "83c09fc78ea61633fb95095e55a57ee0", aum = -1)
    public byte dQg;
    @C0064Am(aul = "3894f51eef4ff170a58313a94d979719", aum = -1)
    public byte dQh;

    public C1081Pp() {
    }

    public C1081Pp(C1616Xf xf) {
        super(xf);
    }

    /* renamed from: c */
    public C5663aRz[] mo309c() {
        return TemporaryFeatureBlock._m_fields;
    }

    /* renamed from: d */
    public void mo310d() {
        super.mo310d();
        this.dQc = false;
        this.dQd = false;
        this.dQe = 0;
        this.dQf = 0;
        this.dQg = 0;
        this.dQh = 0;
    }
}
