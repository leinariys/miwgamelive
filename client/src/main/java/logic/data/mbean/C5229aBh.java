package logic.data.mbean;

import game.network.message.serializable.C3438ra;
import game.script.bank.BankAccount;
import game.script.billing.Billing;
import game.script.login.UserConnection;
import game.script.player.Player;
import game.script.player.User;
import logic.baa.C1616Xf;
import logic.res.code.C5663aRz;
import p001a.C0064Am;
import p001a.C6283ajv;

/* renamed from: a.aBh  reason: case insensitive filesystem */
public class C5229aBh extends C3805vD {
    @C0064Am(aul = "3ce1115aad0c5f63e1110d8688b23274", aum = -1)
    public byte ayA;
    @C0064Am(aul = "3ce1115aad0c5f63e1110d8688b23274", aum = -2)
    public long ayl;
    @C0064Am(aul = "3ce1115aad0c5f63e1110d8688b23274", aum = 6)
    public C3438ra<Player> cYy;
    @C0064Am(aul = "a9c1459387a752cb732d8c4f91be05aa", aum = 8)
    public boolean eVY;
    @C0064Am(aul = "6b882cb0636b30b1278398ea6222a893", aum = 18)
    public int fGU;
    @C0064Am(aul = "6b882cb0636b30b1278398ea6222a893", aum = -1)
    public byte fHQ;
    @C0064Am(aul = "6b882cb0636b30b1278398ea6222a893", aum = -2)
    public long fHs;
    @C0064Am(aul = "92eef0bb4ca256bf07033afe0977f334", aum = 17)
    public Billing fpQ;
    @C0064Am(aul = "92eef0bb4ca256bf07033afe0977f334", aum = -2)
    public long fpT;
    @C0064Am(aul = "92eef0bb4ca256bf07033afe0977f334", aum = -1)
    public byte fpW;
    @C0064Am(aul = "6ab6d277e86d2c195da2e5a74f117e11", aum = 1)
    public String fullName;
    @C0064Am(aul = "a9c1459387a752cb732d8c4f91be05aa", aum = -1)
    public byte gSB;
    @C0064Am(aul = "a9c1459387a752cb732d8c4f91be05aa", aum = -2)
    public long gSv;
    @C0064Am(aul = "3871935cfa2dee14c2d6b7d8845cc57c", aum = -2)
    public long hgA;
    @C0064Am(aul = "e9641e137742e6191472a2f1c0297002", aum = -2)
    public long hgB;
    @C0064Am(aul = "a1e115cc6fa83796cb051ff38c9d7686", aum = -2)
    public long hgC;
    @C0064Am(aul = "1d72bf8844bc9971603b0435cafc7330", aum = -2)
    public long hgD;
    @C0064Am(aul = "e7d649d4761a2bb38d923fe770538715", aum = -1)
    public byte hgE;
    @C0064Am(aul = "6ab6d277e86d2c195da2e5a74f117e11", aum = -1)
    public byte hgF;
    @C0064Am(aul = "b5a2169f073420f1d8f004d07091c071", aum = -1)
    public byte hgG;
    @C0064Am(aul = "776351cb3f22be39cb86d9987863d453", aum = -1)
    public byte hgH;
    @C0064Am(aul = "3990f9fcad3ecff8b5fe3f7b08212f7d", aum = -1)
    public byte hgI;
    @C0064Am(aul = "bc6c473d24bb1cf2e71051ce8302192b", aum = -1)
    public byte hgJ;
    @C0064Am(aul = "5944d67a5b7e2d0155d7871bd20c6b4d", aum = -1)
    public byte hgK;
    @C0064Am(aul = "4a2d3b5a9beefa9e632d68981047863f", aum = -1)
    public byte hgL;
    @C0064Am(aul = "62e8613885da8ac811e46ad1ebe7d63b", aum = -1)
    public byte hgM;
    @C0064Am(aul = "c50de7b42a9365d1d1119d3f4c9b88d3", aum = -1)
    public byte hgN;
    @C0064Am(aul = "b2a596f78acc7073a7a9775a13358a15", aum = -1)
    public byte hgO;
    @C0064Am(aul = "e75dcbcd13e997dab763e1b827371cea", aum = -1)
    public byte hgP;
    @C0064Am(aul = "3871935cfa2dee14c2d6b7d8845cc57c", aum = -1)
    public byte hgQ;
    @C0064Am(aul = "e9641e137742e6191472a2f1c0297002", aum = -1)
    public byte hgR;
    @C0064Am(aul = "a1e115cc6fa83796cb051ff38c9d7686", aum = -1)
    public byte hgS;
    @C0064Am(aul = "1d72bf8844bc9971603b0435cafc7330", aum = -1)
    public byte hgT;
    @C0064Am(aul = "e7d649d4761a2bb38d923fe770538715", aum = 0)
    public BankAccount hgb;
    @C0064Am(aul = "776351cb3f22be39cb86d9987863d453", aum = 3)
    public String hgc;
    @C0064Am(aul = "3990f9fcad3ecff8b5fe3f7b08212f7d", aum = 4)
    public String hgd;
    @C0064Am(aul = "5944d67a5b7e2d0155d7871bd20c6b4d", aum = 7)
    public boolean hge;
    @C0064Am(aul = "4a2d3b5a9beefa9e632d68981047863f", aum = 9)
    public boolean hgf;
    @C0064Am(aul = "62e8613885da8ac811e46ad1ebe7d63b", aum = 10)
    public boolean hgg;
    @C0064Am(aul = "c50de7b42a9365d1d1119d3f4c9b88d3", aum = 11)
    public long hgh;
    @C0064Am(aul = "b2a596f78acc7073a7a9775a13358a15", aum = 12)
    public String hgi;
    @C0064Am(aul = "e75dcbcd13e997dab763e1b827371cea", aum = 13)
    public C6283ajv hgj;
    @C0064Am(aul = "3871935cfa2dee14c2d6b7d8845cc57c", aum = 14)
    public String hgk;
    @C0064Am(aul = "e9641e137742e6191472a2f1c0297002", aum = 15)
    public String hgl;
    @C0064Am(aul = "a1e115cc6fa83796cb051ff38c9d7686", aum = 16)
    public UserConnection hgm;
    @C0064Am(aul = "1d72bf8844bc9971603b0435cafc7330", aum = 19)
    public long hgn;
    @C0064Am(aul = "e7d649d4761a2bb38d923fe770538715", aum = -2)
    public long hgo;
    @C0064Am(aul = "6ab6d277e86d2c195da2e5a74f117e11", aum = -2)
    public long hgp;
    @C0064Am(aul = "b5a2169f073420f1d8f004d07091c071", aum = -2)
    public long hgq;
    @C0064Am(aul = "776351cb3f22be39cb86d9987863d453", aum = -2)
    public long hgr;
    @C0064Am(aul = "3990f9fcad3ecff8b5fe3f7b08212f7d", aum = -2)
    public long hgs;
    @C0064Am(aul = "bc6c473d24bb1cf2e71051ce8302192b", aum = -2)
    public long hgt;
    @C0064Am(aul = "5944d67a5b7e2d0155d7871bd20c6b4d", aum = -2)
    public long hgu;
    @C0064Am(aul = "4a2d3b5a9beefa9e632d68981047863f", aum = -2)
    public long hgv;
    @C0064Am(aul = "62e8613885da8ac811e46ad1ebe7d63b", aum = -2)
    public long hgw;
    @C0064Am(aul = "c50de7b42a9365d1d1119d3f4c9b88d3", aum = -2)
    public long hgx;
    @C0064Am(aul = "b2a596f78acc7073a7a9775a13358a15", aum = -2)
    public long hgy;
    @C0064Am(aul = "e75dcbcd13e997dab763e1b827371cea", aum = -2)
    public long hgz;
    @C0064Am(aul = "bc6c473d24bb1cf2e71051ce8302192b", aum = 5)
    public String password;
    @C0064Am(aul = "b5a2169f073420f1d8f004d07091c071", aum = 2)
    public String username;

    public C5229aBh() {
    }

    public C5229aBh(C1616Xf xf) {
        super(xf);
    }

    /* renamed from: c */
    public C5663aRz[] mo309c() {
        return User._m_fields;
    }

    /* renamed from: d */
    public void mo310d() {
        super.mo310d();
        this.hgb = null;
        this.fullName = null;
        this.username = null;
        this.hgc = null;
        this.hgd = null;
        this.password = null;
        this.cYy = null;
        this.hge = false;
        this.eVY = false;
        this.hgf = false;
        this.hgg = false;
        this.hgh = 0;
        this.hgi = null;
        this.hgj = null;
        this.hgk = null;
        this.hgl = null;
        this.hgm = null;
        this.fpQ = null;
        this.fGU = 0;
        this.hgn = 0;
        this.hgo = 0;
        this.hgp = 0;
        this.hgq = 0;
        this.hgr = 0;
        this.hgs = 0;
        this.hgt = 0;
        this.ayl = 0;
        this.hgu = 0;
        this.gSv = 0;
        this.hgv = 0;
        this.hgw = 0;
        this.hgx = 0;
        this.hgy = 0;
        this.hgz = 0;
        this.hgA = 0;
        this.hgB = 0;
        this.hgC = 0;
        this.fpT = 0;
        this.fHs = 0;
        this.hgD = 0;
        this.hgE = 0;
        this.hgF = 0;
        this.hgG = 0;
        this.hgH = 0;
        this.hgI = 0;
        this.hgJ = 0;
        this.ayA = 0;
        this.hgK = 0;
        this.gSB = 0;
        this.hgL = 0;
        this.hgM = 0;
        this.hgN = 0;
        this.hgO = 0;
        this.hgP = 0;
        this.hgQ = 0;
        this.hgR = 0;
        this.hgS = 0;
        this.fpW = 0;
        this.fHQ = 0;
        this.hgT = 0;
    }
}
