package logic.data.mbean;

import game.geometry.Vec3d;
import game.script.mission.scripting.PatrolMissionScript;
import logic.baa.C1616Xf;
import logic.res.code.C5663aRz;
import p001a.C0064Am;

/* renamed from: a.wp */
public class C3923wp extends aMY {
    @C0064Am(aul = "8f88b431ecaa02f01a31eb02609601ba", aum = 0)
    public int bCN;
    @C0064Am(aul = "98f621366dbf71fdfefcf2df0469a67c", aum = 1)
    public boolean bCO;
    @C0064Am(aul = "9860115c0764fd786552791167435824", aum = 2)
    public int bCP;
    @C0064Am(aul = "a34f1131bdb211f41eae48314f9f847d", aum = 3)
    public int bCQ;
    @C0064Am(aul = "84297cc03c9342a88936944cbc171b68", aum = 4)
    public int bCR;
    @C0064Am(aul = "33e220cf25cde25f272c580b68d6cb66", aum = 5)
    public int bCS;
    @C0064Am(aul = "51188bb3308b57ea61098b8feeef2b6a", aum = 6)
    public Vec3d bCT;
    @C0064Am(aul = "8f88b431ecaa02f01a31eb02609601ba", aum = -2)
    public long bCU;
    @C0064Am(aul = "98f621366dbf71fdfefcf2df0469a67c", aum = -2)
    public long bCV;
    @C0064Am(aul = "9860115c0764fd786552791167435824", aum = -2)
    public long bCW;
    @C0064Am(aul = "a34f1131bdb211f41eae48314f9f847d", aum = -2)
    public long bCX;
    @C0064Am(aul = "84297cc03c9342a88936944cbc171b68", aum = -2)
    public long bCY;
    @C0064Am(aul = "33e220cf25cde25f272c580b68d6cb66", aum = -2)
    public long bCZ;
    @C0064Am(aul = "51188bb3308b57ea61098b8feeef2b6a", aum = -2)
    public long bDa;
    @C0064Am(aul = "8f88b431ecaa02f01a31eb02609601ba", aum = -1)
    public byte bDb;
    @C0064Am(aul = "98f621366dbf71fdfefcf2df0469a67c", aum = -1)
    public byte bDc;
    @C0064Am(aul = "9860115c0764fd786552791167435824", aum = -1)
    public byte bDd;
    @C0064Am(aul = "a34f1131bdb211f41eae48314f9f847d", aum = -1)
    public byte bDe;
    @C0064Am(aul = "84297cc03c9342a88936944cbc171b68", aum = -1)
    public byte bDf;
    @C0064Am(aul = "33e220cf25cde25f272c580b68d6cb66", aum = -1)
    public byte bDg;
    @C0064Am(aul = "51188bb3308b57ea61098b8feeef2b6a", aum = -1)
    public byte bDh;

    public C3923wp() {
    }

    public C3923wp(C1616Xf xf) {
        super(xf);
    }

    /* renamed from: c */
    public C5663aRz[] mo309c() {
        return PatrolMissionScript._m_fields;
    }

    /* renamed from: d */
    public void mo310d() {
        super.mo310d();
        this.bCN = 0;
        this.bCO = false;
        this.bCP = 0;
        this.bCQ = 0;
        this.bCR = 0;
        this.bCS = 0;
        this.bCT = null;
        this.bCU = 0;
        this.bCV = 0;
        this.bCW = 0;
        this.bCX = 0;
        this.bCY = 0;
        this.bCZ = 0;
        this.bDa = 0;
        this.bDb = 0;
        this.bDc = 0;
        this.bDd = 0;
        this.bDe = 0;
        this.bDf = 0;
        this.bDg = 0;
        this.bDh = 0;
    }
}
