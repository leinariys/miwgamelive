package logic.data.mbean;

import game.script.item.buff.amplifier.HullAmplifier;
import logic.baa.C1616Xf;
import logic.res.code.C5663aRz;
import p001a.C0064Am;

/* renamed from: a.aQy  reason: case insensitive filesystem */
public class C5636aQy extends aEW {
    @C0064Am(aul = "1f5101a9369a5bcd62dd73bef5edef18", aum = -2)

    /* renamed from: dl */
    public long f3677dl;
    @C0064Am(aul = "1f5101a9369a5bcd62dd73bef5edef18", aum = -1)

    /* renamed from: ds */
    public byte f3678ds;
    @C0064Am(aul = "1f5101a9369a5bcd62dd73bef5edef18", aum = 0)
    public HullAmplifier feI;

    public C5636aQy() {
    }

    public C5636aQy(C1616Xf xf) {
        super(xf);
    }

    /* renamed from: c */
    public C5663aRz[] mo309c() {
        return HullAmplifier.HullAmplifierAdapter._m_fields;
    }

    /* renamed from: d */
    public void mo310d() {
        super.mo310d();
        this.feI = null;
        this.f3677dl = 0;
        this.f3678ds = 0;
    }
}
