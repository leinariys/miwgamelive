package logic.data.mbean;

import game.script.progression.ProgressionCellType;
import game.script.progression.ProgressionLineType;
import game.script.resource.Asset;
import logic.baa.C1616Xf;
import logic.res.code.C5663aRz;
import p001a.C0064Am;
import taikodom.infra.script.I18NString;

/* renamed from: a.aej  reason: case insensitive filesystem */
public class C6011aej extends C6515aoT {
    @C0064Am(aul = "bca430fca9845ebf5579aa95000e3053", aum = 2)

    /* renamed from: QB */
    public ProgressionCellType f4416QB;
    @C0064Am(aul = "bca430fca9845ebf5579aa95000e3053", aum = -2)
    public long cvl;
    @C0064Am(aul = "bca430fca9845ebf5579aa95000e3053", aum = -1)
    public byte cvn;
    @C0064Am(aul = "ce4efdf1ab82813bcfeade524e0eab5a", aum = -1)

    /* renamed from: jK */
    public byte f4417jK;
    @C0064Am(aul = "ce4efdf1ab82813bcfeade524e0eab5a", aum = 3)

    /* renamed from: jn */
    public Asset f4418jn;
    @C0064Am(aul = "ce4efdf1ab82813bcfeade524e0eab5a", aum = -2)

    /* renamed from: jy */
    public long f4419jy;
    @C0064Am(aul = "8781bf3fa8f813b0ab46a160616d8b86", aum = 1)

    /* renamed from: nh */
    public I18NString f4420nh;
    @C0064Am(aul = "4191722388e29aa24f7103486b0bcc82", aum = 0)

    /* renamed from: ni */
    public I18NString f4421ni;
    @C0064Am(aul = "8781bf3fa8f813b0ab46a160616d8b86", aum = -2)

    /* renamed from: nk */
    public long f4422nk;
    @C0064Am(aul = "4191722388e29aa24f7103486b0bcc82", aum = -2)

    /* renamed from: nl */
    public long f4423nl;
    @C0064Am(aul = "8781bf3fa8f813b0ab46a160616d8b86", aum = -1)

    /* renamed from: nn */
    public byte f4424nn;
    @C0064Am(aul = "4191722388e29aa24f7103486b0bcc82", aum = -1)

    /* renamed from: no */
    public byte f4425no;

    public C6011aej() {
    }

    public C6011aej(C1616Xf xf) {
        super(xf);
    }

    /* renamed from: c */
    public C5663aRz[] mo309c() {
        return ProgressionLineType._m_fields;
    }

    /* renamed from: d */
    public void mo310d() {
        super.mo310d();
        this.f4421ni = null;
        this.f4420nh = null;
        this.f4416QB = null;
        this.f4418jn = null;
        this.f4423nl = 0;
        this.f4422nk = 0;
        this.cvl = 0;
        this.f4419jy = 0;
        this.f4425no = 0;
        this.f4424nn = 0;
        this.cvn = 0;
        this.f4417jK = 0;
    }
}
