package logic.data.mbean;

import game.network.message.serializable.C2686iZ;
import game.script.item.Item;
import game.script.item.ItemLocation;
import logic.baa.C1616Xf;
import logic.res.code.C5663aRz;
import p001a.C0064Am;

/* renamed from: a.aAL */
public class aAL extends C3805vD {
    @C0064Am(aul = "441dd3e0953e642bc894c451c3196208", aum = -2)
    public long eZO;
    @C0064Am(aul = "441dd3e0953e642bc894c451c3196208", aum = -1)
    public byte eZR;
    @C0064Am(aul = "441dd3e0953e642bc894c451c3196208", aum = 0)

    /* renamed from: gy */
    public C2686iZ<Item> f2313gy;

    public aAL() {
    }

    public aAL(C1616Xf xf) {
        super(xf);
    }

    /* renamed from: c */
    public C5663aRz[] mo309c() {
        return ItemLocation._m_fields;
    }

    /* renamed from: d */
    public void mo310d() {
        super.mo310d();
        this.f2313gy = null;
        this.eZO = 0;
        this.eZR = 0;
    }
}
