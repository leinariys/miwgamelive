package logic.data.mbean;

import game.script.nls.NLSMission;
import logic.baa.C1616Xf;
import logic.res.code.C5663aRz;
import p001a.C0064Am;
import taikodom.infra.script.I18NString;

/* renamed from: a.On */
public class C1004On extends C2484fi {
    @C0064Am(aul = "e25d5371b3e51b37ff3a890a9598a9ef", aum = 11)
    public I18NString aeA;
    @C0064Am(aul = "f89f18c31a8a1ac07fb566d0c84ca3ec", aum = 12)
    public I18NString aeC;
    @C0064Am(aul = "12dc18b64e324a6ed2dc7953b0de70fd", aum = 13)
    public I18NString aeE;
    @C0064Am(aul = "7c061d0883b3087982e1a1858ef46809", aum = 14)
    public I18NString aeG;
    @C0064Am(aul = "243a2c13f2b1e9edc463dd69a6c46b36", aum = 15)
    public I18NString aeI;
    @C0064Am(aul = "87391d7ff5718a1e4f4172c357509214", aum = 16)
    public I18NString aeK;
    @C0064Am(aul = "47c644564814fca5b66bd5363543ea0d", aum = 17)
    public I18NString aeM;
    @C0064Am(aul = "62c45860c314672f0c2d5861e537d03a", aum = 18)
    public I18NString aeO;
    @C0064Am(aul = "10f1b0a49e0dcb74a07e1011ac35fab6", aum = 19)
    public I18NString aeQ;
    @C0064Am(aul = "6496b8f2cad656c04dc93bd3bd4eff25", aum = 20)
    public I18NString aeS;
    @C0064Am(aul = "7b3d8e51c33020d8d3c887038a0dab6c", aum = 21)
    public I18NString aeU;
    @C0064Am(aul = "f0245d7f1e23fbc38a2f753bbe788468", aum = 22)
    public I18NString aeW;
    @C0064Am(aul = "28e2ad86f29fc042fb0cddf2d25c709d", aum = 23)
    public I18NString aeY;
    @C0064Am(aul = "52ce2733e238c7ef091fdac68f5abd89", aum = 0)
    public I18NString aeg;
    @C0064Am(aul = "9f64fd95a42be6d564e8b7c5c0feb255", aum = 1)
    public I18NString aei;
    @C0064Am(aul = "3beac6f10468e73466e8ebf5ca31164b", aum = 2)
    public I18NString aek;
    @C0064Am(aul = "f54fe6e4753b7d7aabfefc58091b82de", aum = 3)
    public I18NString aem;
    @C0064Am(aul = "818315865b0cb8262b8d21d978ac2bdf", aum = 4)
    public I18NString aeo;
    @C0064Am(aul = "6e2250a2dcb3cf19282bec1f239d6923", aum = 5)
    public I18NString aeq;
    @C0064Am(aul = "5c5364701a9d325227b246b65f9b75f3", aum = 6)
    public I18NString aes;
    @C0064Am(aul = "50acf3999c1c495a8d9888cfc3aa57fb", aum = 7)
    public I18NString aeu;
    @C0064Am(aul = "f7b6ec2b9b2622ae177bd3ae82bb8f49", aum = 8)
    public I18NString aew;
    @C0064Am(aul = "c79b92752296d20447dd6a5cdf20af67", aum = 10)
    public I18NString aey;
    @C0064Am(aul = "edf7cb53c4965d0822d1bf5387cdc11f", aum = 24)
    public I18NString afa;
    @C0064Am(aul = "f9104d2430b35a8fd64bb2721544fab7", aum = 25)
    public I18NString afc;
    @C0064Am(aul = "f423c297c6e06fdb3768691a8a988c9c", aum = 26)
    public I18NString afe;
    @C0064Am(aul = "3f6a54ff4dbe193ded1eea3eb15ecf95", aum = 27)
    public I18NString afg;
    @C0064Am(aul = "76942550d7857f5cb3f644e754045ec8", aum = 28)
    public I18NString afi;
    @C0064Am(aul = "344218a9410bf06d08ba975abc82ba06", aum = 29)
    public I18NString afk;
    @C0064Am(aul = "52ce2733e238c7ef091fdac68f5abd89", aum = -2)
    public long dJC;
    @C0064Am(aul = "9f64fd95a42be6d564e8b7c5c0feb255", aum = -2)
    public long dJD;
    @C0064Am(aul = "3beac6f10468e73466e8ebf5ca31164b", aum = -2)
    public long dJE;
    @C0064Am(aul = "f54fe6e4753b7d7aabfefc58091b82de", aum = -2)
    public long dJF;
    @C0064Am(aul = "818315865b0cb8262b8d21d978ac2bdf", aum = -2)
    public long dJG;
    @C0064Am(aul = "6e2250a2dcb3cf19282bec1f239d6923", aum = -2)
    public long dJH;
    @C0064Am(aul = "5c5364701a9d325227b246b65f9b75f3", aum = -2)
    public long dJI;
    @C0064Am(aul = "50acf3999c1c495a8d9888cfc3aa57fb", aum = -2)
    public long dJJ;
    @C0064Am(aul = "f7b6ec2b9b2622ae177bd3ae82bb8f49", aum = -2)
    public long dJK;
    @C0064Am(aul = "c79b92752296d20447dd6a5cdf20af67", aum = -2)
    public long dJL;
    @C0064Am(aul = "e25d5371b3e51b37ff3a890a9598a9ef", aum = -2)
    public long dJM;
    @C0064Am(aul = "f89f18c31a8a1ac07fb566d0c84ca3ec", aum = -2)
    public long dJN;
    @C0064Am(aul = "12dc18b64e324a6ed2dc7953b0de70fd", aum = -2)
    public long dJO;
    @C0064Am(aul = "7c061d0883b3087982e1a1858ef46809", aum = -2)
    public long dJP;
    @C0064Am(aul = "243a2c13f2b1e9edc463dd69a6c46b36", aum = -2)
    public long dJQ;
    @C0064Am(aul = "87391d7ff5718a1e4f4172c357509214", aum = -2)
    public long dJR;
    @C0064Am(aul = "47c644564814fca5b66bd5363543ea0d", aum = -2)
    public long dJS;
    @C0064Am(aul = "62c45860c314672f0c2d5861e537d03a", aum = -2)
    public long dJT;
    @C0064Am(aul = "10f1b0a49e0dcb74a07e1011ac35fab6", aum = -2)
    public long dJU;
    @C0064Am(aul = "6496b8f2cad656c04dc93bd3bd4eff25", aum = -2)
    public long dJV;
    @C0064Am(aul = "7b3d8e51c33020d8d3c887038a0dab6c", aum = -2)
    public long dJW;
    @C0064Am(aul = "f0245d7f1e23fbc38a2f753bbe788468", aum = -2)
    public long dJX;
    @C0064Am(aul = "28e2ad86f29fc042fb0cddf2d25c709d", aum = -2)
    public long dJY;
    @C0064Am(aul = "edf7cb53c4965d0822d1bf5387cdc11f", aum = -2)
    public long dJZ;
    @C0064Am(aul = "28e2ad86f29fc042fb0cddf2d25c709d", aum = -1)
    public byte dKA;
    @C0064Am(aul = "edf7cb53c4965d0822d1bf5387cdc11f", aum = -1)
    public byte dKB;
    @C0064Am(aul = "f9104d2430b35a8fd64bb2721544fab7", aum = -1)
    public byte dKC;
    @C0064Am(aul = "3f6a54ff4dbe193ded1eea3eb15ecf95", aum = -1)
    public byte dKD;
    @C0064Am(aul = "76942550d7857f5cb3f644e754045ec8", aum = -1)
    public byte dKE;
    @C0064Am(aul = "344218a9410bf06d08ba975abc82ba06", aum = -1)
    public byte dKF;
    @C0064Am(aul = "f9104d2430b35a8fd64bb2721544fab7", aum = -2)
    public long dKa;
    @C0064Am(aul = "3f6a54ff4dbe193ded1eea3eb15ecf95", aum = -2)
    public long dKb;
    @C0064Am(aul = "76942550d7857f5cb3f644e754045ec8", aum = -2)
    public long dKc;
    @C0064Am(aul = "344218a9410bf06d08ba975abc82ba06", aum = -2)
    public long dKd;
    @C0064Am(aul = "52ce2733e238c7ef091fdac68f5abd89", aum = -1)
    public byte dKe;
    @C0064Am(aul = "9f64fd95a42be6d564e8b7c5c0feb255", aum = -1)
    public byte dKf;
    @C0064Am(aul = "3beac6f10468e73466e8ebf5ca31164b", aum = -1)
    public byte dKg;
    @C0064Am(aul = "f54fe6e4753b7d7aabfefc58091b82de", aum = -1)
    public byte dKh;
    @C0064Am(aul = "818315865b0cb8262b8d21d978ac2bdf", aum = -1)
    public byte dKi;
    @C0064Am(aul = "6e2250a2dcb3cf19282bec1f239d6923", aum = -1)
    public byte dKj;
    @C0064Am(aul = "5c5364701a9d325227b246b65f9b75f3", aum = -1)
    public byte dKk;
    @C0064Am(aul = "50acf3999c1c495a8d9888cfc3aa57fb", aum = -1)
    public byte dKl;
    @C0064Am(aul = "f7b6ec2b9b2622ae177bd3ae82bb8f49", aum = -1)
    public byte dKm;
    @C0064Am(aul = "c79b92752296d20447dd6a5cdf20af67", aum = -1)
    public byte dKn;
    @C0064Am(aul = "e25d5371b3e51b37ff3a890a9598a9ef", aum = -1)
    public byte dKo;
    @C0064Am(aul = "f89f18c31a8a1ac07fb566d0c84ca3ec", aum = -1)
    public byte dKp;
    @C0064Am(aul = "12dc18b64e324a6ed2dc7953b0de70fd", aum = -1)
    public byte dKq;
    @C0064Am(aul = "7c061d0883b3087982e1a1858ef46809", aum = -1)
    public byte dKr;
    @C0064Am(aul = "243a2c13f2b1e9edc463dd69a6c46b36", aum = -1)
    public byte dKs;
    @C0064Am(aul = "87391d7ff5718a1e4f4172c357509214", aum = -1)
    public byte dKt;
    @C0064Am(aul = "47c644564814fca5b66bd5363543ea0d", aum = -1)
    public byte dKu;
    @C0064Am(aul = "62c45860c314672f0c2d5861e537d03a", aum = -1)
    public byte dKv;
    @C0064Am(aul = "10f1b0a49e0dcb74a07e1011ac35fab6", aum = -1)
    public byte dKw;
    @C0064Am(aul = "6496b8f2cad656c04dc93bd3bd4eff25", aum = -1)
    public byte dKx;
    @C0064Am(aul = "7b3d8e51c33020d8d3c887038a0dab6c", aum = -1)
    public byte dKy;
    @C0064Am(aul = "f0245d7f1e23fbc38a2f753bbe788468", aum = -1)
    public byte dKz;
    @C0064Am(aul = "f423c297c6e06fdb3768691a8a988c9c", aum = -2)
    public long dxV;
    @C0064Am(aul = "f423c297c6e06fdb3768691a8a988c9c", aum = -1)
    public byte dyd;
    @C0064Am(aul = "6e202ff6d04856c88d4a9f4f2256376e", aum = 9)

    /* renamed from: ni */
    public I18NString f1336ni;
    @C0064Am(aul = "6e202ff6d04856c88d4a9f4f2256376e", aum = -2)

    /* renamed from: nl */
    public long f1337nl;
    @C0064Am(aul = "6e202ff6d04856c88d4a9f4f2256376e", aum = -1)

    /* renamed from: no */
    public byte f1338no;

    public C1004On() {
    }

    public C1004On(C1616Xf xf) {
        super(xf);
    }

    /* renamed from: c */
    public C5663aRz[] mo309c() {
        return NLSMission._m_fields;
    }

    /* renamed from: d */
    public void mo310d() {
        super.mo310d();
        this.aeg = null;
        this.aei = null;
        this.aek = null;
        this.aem = null;
        this.aeo = null;
        this.aeq = null;
        this.aes = null;
        this.aeu = null;
        this.aew = null;
        this.f1336ni = null;
        this.aey = null;
        this.aeA = null;
        this.aeC = null;
        this.aeE = null;
        this.aeG = null;
        this.aeI = null;
        this.aeK = null;
        this.aeM = null;
        this.aeO = null;
        this.aeQ = null;
        this.aeS = null;
        this.aeU = null;
        this.aeW = null;
        this.aeY = null;
        this.afa = null;
        this.afc = null;
        this.afe = null;
        this.afg = null;
        this.afi = null;
        this.afk = null;
        this.dJC = 0;
        this.dJD = 0;
        this.dJE = 0;
        this.dJF = 0;
        this.dJG = 0;
        this.dJH = 0;
        this.dJI = 0;
        this.dJJ = 0;
        this.dJK = 0;
        this.f1337nl = 0;
        this.dJL = 0;
        this.dJM = 0;
        this.dJN = 0;
        this.dJO = 0;
        this.dJP = 0;
        this.dJQ = 0;
        this.dJR = 0;
        this.dJS = 0;
        this.dJT = 0;
        this.dJU = 0;
        this.dJV = 0;
        this.dJW = 0;
        this.dJX = 0;
        this.dJY = 0;
        this.dJZ = 0;
        this.dKa = 0;
        this.dxV = 0;
        this.dKb = 0;
        this.dKc = 0;
        this.dKd = 0;
        this.dKe = 0;
        this.dKf = 0;
        this.dKg = 0;
        this.dKh = 0;
        this.dKi = 0;
        this.dKj = 0;
        this.dKk = 0;
        this.dKl = 0;
        this.dKm = 0;
        this.f1338no = 0;
        this.dKn = 0;
        this.dKo = 0;
        this.dKp = 0;
        this.dKq = 0;
        this.dKr = 0;
        this.dKs = 0;
        this.dKt = 0;
        this.dKu = 0;
        this.dKv = 0;
        this.dKw = 0;
        this.dKx = 0;
        this.dKy = 0;
        this.dKz = 0;
        this.dKA = 0;
        this.dKB = 0;
        this.dKC = 0;
        this.dyd = 0;
        this.dKD = 0;
        this.dKE = 0;
        this.dKF = 0;
    }
}
