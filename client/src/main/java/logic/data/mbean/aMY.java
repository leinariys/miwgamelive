package logic.data.mbean;

import game.network.message.serializable.C1556Wo;
import game.network.message.serializable.C2686iZ;
import game.network.message.serializable.C3438ra;
import game.script.mission.MissionObjective;
import game.script.mission.actions.MissionAction;
import game.script.mission.scripting.MissionScript;
import game.script.mission.scripting.MissionScriptState;
import logic.baa.C1616Xf;
import logic.res.code.C5663aRz;
import p001a.C0064Am;

/* renamed from: a.aMY */
public class aMY extends C0505HA {
    @C0064Am(aul = "39b1c4d08d05299c27d8812c452475e5", aum = 0)
    public C1556Wo<Class<?>, MissionScriptState> cnk;
    @C0064Am(aul = "f0f32b490410e2473bf27202fab4e2fa", aum = 1)
    public MissionScriptState cnm;
    @C0064Am(aul = "46b5a292ec568ac185767ff77c46c9b9", aum = 2)
    public C3438ra<MissionObjective> cno;
    @C0064Am(aul = "0aba2fdafbc0a2e3b7bc709b687181b8", aum = 3)
    public C2686iZ<MissionAction> cnq;
    @C0064Am(aul = "46b5a292ec568ac185767ff77c46c9b9", aum = -2)
    public long dhk;
    @C0064Am(aul = "46b5a292ec568ac185767ff77c46c9b9", aum = -1)
    public byte dho;
    @C0064Am(aul = "39b1c4d08d05299c27d8812c452475e5", aum = -2)
    public long irc;
    @C0064Am(aul = "f0f32b490410e2473bf27202fab4e2fa", aum = -2)
    public long ird;
    @C0064Am(aul = "0aba2fdafbc0a2e3b7bc709b687181b8", aum = -2)
    public long ire;
    @C0064Am(aul = "39b1c4d08d05299c27d8812c452475e5", aum = -1)
    public byte irf;
    @C0064Am(aul = "f0f32b490410e2473bf27202fab4e2fa", aum = -1)
    public byte irg;
    @C0064Am(aul = "0aba2fdafbc0a2e3b7bc709b687181b8", aum = -1)
    public byte irh;

    public aMY() {
    }

    public aMY(C1616Xf xf) {
        super(xf);
    }

    /* renamed from: c */
    public C5663aRz[] mo309c() {
        return MissionScript._m_fields;
    }

    /* renamed from: d */
    public void mo310d() {
        super.mo310d();
        this.cnk = null;
        this.cnm = null;
        this.cno = null;
        this.cnq = null;
        this.irc = 0;
        this.ird = 0;
        this.dhk = 0;
        this.ire = 0;
        this.irf = 0;
        this.irg = 0;
        this.dho = 0;
        this.irh = 0;
    }
}
