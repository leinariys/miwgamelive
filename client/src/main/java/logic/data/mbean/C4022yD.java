package logic.data.mbean;

import game.network.message.serializable.C1556Wo;
import game.script.damage.DamageType;
import game.script.item.Clip;
import game.script.resource.Asset;
import logic.baa.C1616Xf;
import logic.res.code.C5663aRz;
import p001a.C0064Am;

/* renamed from: a.yD */
public class C4022yD extends C1359Tn {
    @C0064Am(aul = "881df08b80f055100a20b82aeb462df6", aum = 0)

    /* renamed from: LQ */
    public Asset f9575LQ;
    @C0064Am(aul = "aeddc3a81f653633176039a8d63298f7", aum = 2)
    public int atB;
    @C0064Am(aul = "aeddc3a81f653633176039a8d63298f7", aum = -2)
    public long atG;
    @C0064Am(aul = "aeddc3a81f653633176039a8d63298f7", aum = -1)
    public byte atL;
    @C0064Am(aul = "aa9f699ef05f7b38691f00e8650664be", aum = -2)
    public long avd;
    @C0064Am(aul = "aa9f699ef05f7b38691f00e8650664be", aum = -1)
    public byte avi;
    @C0064Am(aul = "881df08b80f055100a20b82aeb462df6", aum = -2)
    public long ayc;
    @C0064Am(aul = "881df08b80f055100a20b82aeb462df6", aum = -1)
    public byte ayr;
    @C0064Am(aul = "ffacce78cc87785c9c709c4562de3808", aum = 1)
    public C1556Wo<DamageType, Float> bIv;
    @C0064Am(aul = "ffacce78cc87785c9c709c4562de3808", aum = -2)
    public long beq;
    @C0064Am(aul = "ffacce78cc87785c9c709c4562de3808", aum = -1)
    public byte bes;
    @C0064Am(aul = "aa9f699ef05f7b38691f00e8650664be", aum = 3)

    /* renamed from: uT */
    public String f9576uT;

    public C4022yD() {
    }

    public C4022yD(C1616Xf xf) {
        super(xf);
    }

    /* renamed from: c */
    public C5663aRz[] mo309c() {
        return Clip._m_fields;
    }

    /* renamed from: d */
    public void mo310d() {
        super.mo310d();
        this.f9575LQ = null;
        this.bIv = null;
        this.atB = 0;
        this.f9576uT = null;
        this.ayc = 0;
        this.beq = 0;
        this.atG = 0;
        this.avd = 0;
        this.ayr = 0;
        this.bes = 0;
        this.atL = 0;
        this.avi = 0;
    }
}
