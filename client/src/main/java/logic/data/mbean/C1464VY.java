package logic.data.mbean;

import game.script.item.buff.amplifier.ShipAmplifierType;
import logic.baa.C1616Xf;
import logic.res.code.C5663aRz;
import p001a.C0064Am;
import p001a.C3892wO;

/* renamed from: a.VY */
public class C1464VY extends C2421fH {
    @C0064Am(aul = "975db32725f4c6e41c1f400358dbdbad", aum = 4)

    /* renamed from: VA */
    public C3892wO f1883VA;
    @C0064Am(aul = "9e616e48c462df3f8c93fcae469d11da", aum = 0)

    /* renamed from: Vs */
    public C3892wO f1884Vs;
    @C0064Am(aul = "cb05163157664ce7a1daf9ecaeea8b8c", aum = 1)

    /* renamed from: Vu */
    public C3892wO f1885Vu;
    @C0064Am(aul = "0e2d0a1f8431d479fcb7d0d6e6a731f9", aum = 2)

    /* renamed from: Vw */
    public C3892wO f1886Vw;
    @C0064Am(aul = "a7e9ae6588f6b8e1da944aefced25a57", aum = 3)

    /* renamed from: Vy */
    public C3892wO f1887Vy;
    @C0064Am(aul = "9e616e48c462df3f8c93fcae469d11da", aum = -1)

    /* renamed from: YC */
    public byte f1888YC;
    @C0064Am(aul = "cb05163157664ce7a1daf9ecaeea8b8c", aum = -1)

    /* renamed from: YD */
    public byte f1889YD;
    @C0064Am(aul = "0e2d0a1f8431d479fcb7d0d6e6a731f9", aum = -1)

    /* renamed from: YE */
    public byte f1890YE;
    @C0064Am(aul = "a7e9ae6588f6b8e1da944aefced25a57", aum = -1)

    /* renamed from: YF */
    public byte f1891YF;
    @C0064Am(aul = "9e616e48c462df3f8c93fcae469d11da", aum = -2)

    /* renamed from: Yw */
    public long f1892Yw;
    @C0064Am(aul = "cb05163157664ce7a1daf9ecaeea8b8c", aum = -2)

    /* renamed from: Yx */
    public long f1893Yx;
    @C0064Am(aul = "0e2d0a1f8431d479fcb7d0d6e6a731f9", aum = -2)

    /* renamed from: Yy */
    public long f1894Yy;
    @C0064Am(aul = "a7e9ae6588f6b8e1da944aefced25a57", aum = -2)

    /* renamed from: Yz */
    public long f1895Yz;
    @C0064Am(aul = "975db32725f4c6e41c1f400358dbdbad", aum = -2)
    public long bHw;
    @C0064Am(aul = "975db32725f4c6e41c1f400358dbdbad", aum = -1)
    public byte bHy;

    public C1464VY() {
    }

    public C1464VY(C1616Xf xf) {
        super(xf);
    }

    /* renamed from: c */
    public C5663aRz[] mo309c() {
        return ShipAmplifierType._m_fields;
    }

    /* renamed from: d */
    public void mo310d() {
        super.mo310d();
        this.f1884Vs = null;
        this.f1885Vu = null;
        this.f1886Vw = null;
        this.f1887Vy = null;
        this.f1883VA = null;
        this.f1892Yw = 0;
        this.f1893Yx = 0;
        this.f1894Yy = 0;
        this.f1895Yz = 0;
        this.bHw = 0;
        this.f1888YC = 0;
        this.f1889YD = 0;
        this.f1890YE = 0;
        this.f1891YF = 0;
        this.bHy = 0;
    }
}
