package logic.data.mbean;

import game.network.message.serializable.C3438ra;
import game.script.hangar.Bay;
import game.script.hangar.Hangar;
import game.script.player.Player;
import game.script.ship.Station;
import logic.baa.C1616Xf;
import logic.res.code.C5663aRz;
import p001a.C0064Am;

/* renamed from: a.zW */
public class C4103zW extends C3805vD {
    @C0064Am(aul = "39ac92121912c026efd5241bf57b77be", aum = 2)
    public C3438ra<Bay> avG;
    @C0064Am(aul = "3aa5bd42d6008518d1fa5bfb6c06a417", aum = 3)
    public int avI;
    @C0064Am(aul = "39ac92121912c026efd5241bf57b77be", aum = -2)
    public long ccF;
    @C0064Am(aul = "3aa5bd42d6008518d1fa5bfb6c06a417", aum = -2)
    public long ccG;
    @C0064Am(aul = "39ac92121912c026efd5241bf57b77be", aum = -1)
    public byte ccH;
    @C0064Am(aul = "3aa5bd42d6008518d1fa5bfb6c06a417", aum = -1)
    public byte ccI;
    @C0064Am(aul = "d415aa0a380f1256fb64f2f749397637", aum = 1)

    /* renamed from: iH */
    public Station f9688iH;
    @C0064Am(aul = "d415aa0a380f1256fb64f2f749397637", aum = -2)

    /* renamed from: iK */
    public long f9689iK;
    @C0064Am(aul = "d415aa0a380f1256fb64f2f749397637", aum = -1)

    /* renamed from: iN */
    public byte f9690iN;
    @C0064Am(aul = "9a04dbceebe9e09f587200d31ab410fd", aum = 0)

    /* renamed from: nj */
    public Player f9691nj;
    @C0064Am(aul = "9a04dbceebe9e09f587200d31ab410fd", aum = -2)

    /* renamed from: nm */
    public long f9692nm;
    @C0064Am(aul = "9a04dbceebe9e09f587200d31ab410fd", aum = -1)

    /* renamed from: np */
    public byte f9693np;

    public C4103zW() {
    }

    public C4103zW(C1616Xf xf) {
        super(xf);
    }

    /* renamed from: c */
    public C5663aRz[] mo309c() {
        return Hangar._m_fields;
    }

    /* renamed from: d */
    public void mo310d() {
        super.mo310d();
        this.f9691nj = null;
        this.f9688iH = null;
        this.avG = null;
        this.avI = 0;
        this.f9692nm = 0;
        this.f9689iK = 0;
        this.ccF = 0;
        this.ccG = 0;
        this.f9693np = 0;
        this.f9690iN = 0;
        this.ccH = 0;
        this.ccI = 0;
    }
}
