package logic.data.mbean;

import game.network.message.serializable.C3438ra;
import game.script.hazardarea.HazardArea.HazardAreaType;
import game.script.item.buff.module.AttributeBuffType;
import game.script.resource.Asset;
import logic.baa.C1616Xf;
import logic.res.code.C5663aRz;
import p001a.C0064Am;

/* renamed from: a.YD */
public class C1644YD extends aUL {
    @C0064Am(aul = "3068c81b8ca08274f1b0000793f1da62", aum = 0)
    public C3438ra<AttributeBuffType> aQT;
    @C0064Am(aul = "dee562f383964962dbd45170d6c76f67", aum = 1)
    public int aQV;
    @C0064Am(aul = "bf40c2a8df015e5a0931ea0690b8e42f", aum = 2)
    public float aQX;
    @C0064Am(aul = "28f9b0e36d266a9f52f3dcc80459fbd6", aum = 3)
    public Asset aQZ;
    @C0064Am(aul = "3068c81b8ca08274f1b0000793f1da62", aum = -1)
    public byte eLD;
    @C0064Am(aul = "dee562f383964962dbd45170d6c76f67", aum = -1)
    public byte eLE;
    @C0064Am(aul = "bf40c2a8df015e5a0931ea0690b8e42f", aum = -1)
    public byte eLF;
    @C0064Am(aul = "28f9b0e36d266a9f52f3dcc80459fbd6", aum = -1)
    public byte eLG;
    @C0064Am(aul = "3068c81b8ca08274f1b0000793f1da62", aum = -2)
    public long eLw;
    @C0064Am(aul = "dee562f383964962dbd45170d6c76f67", aum = -2)
    public long eLx;
    @C0064Am(aul = "bf40c2a8df015e5a0931ea0690b8e42f", aum = -2)
    public long eLy;
    @C0064Am(aul = "28f9b0e36d266a9f52f3dcc80459fbd6", aum = -2)
    public long eLz;

    public C1644YD() {
    }

    public C1644YD(C1616Xf xf) {
        super(xf);
    }

    /* renamed from: c */
    public C5663aRz[] mo309c() {
        return HazardAreaType._m_fields;
    }

    /* renamed from: d */
    public void mo310d() {
        super.mo310d();
        this.aQT = null;
        this.aQV = 0;
        this.aQX = 0.0f;
        this.aQZ = null;
        this.eLw = 0;
        this.eLx = 0;
        this.eLy = 0;
        this.eLz = 0;
        this.eLD = 0;
        this.eLE = 0;
        this.eLF = 0;
        this.eLG = 0;
    }
}
