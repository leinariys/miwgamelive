package logic.data.mbean;

import game.script.npcchat.requirement.ParamSpeechRequirement;
import logic.baa.C1616Xf;
import logic.res.code.C5663aRz;
import p001a.C0064Am;

/* renamed from: a.avV  reason: case insensitive filesystem */
public class C6881avV extends C5416aIm {
    @C0064Am(aul = "5d0bcb0d6e3cf4586cd7e5e0924dc6d1", aum = -2)

    /* renamed from: IC */
    public long f5442IC;
    @C0064Am(aul = "cbe3e734a29e654902632c15b6bc9b7e", aum = -2)

    /* renamed from: ID */
    public long f5443ID;
    @C0064Am(aul = "5d0bcb0d6e3cf4586cd7e5e0924dc6d1", aum = -1)

    /* renamed from: IE */
    public byte f5444IE;
    @C0064Am(aul = "cbe3e734a29e654902632c15b6bc9b7e", aum = -1)

    /* renamed from: IG */
    public byte f5445IG;
    @C0064Am(aul = "5d0bcb0d6e3cf4586cd7e5e0924dc6d1", aum = 0)
    public String key;
    @C0064Am(aul = "cbe3e734a29e654902632c15b6bc9b7e", aum = 1)
    public String value;

    public C6881avV() {
    }

    public C6881avV(C1616Xf xf) {
        super(xf);
    }

    /* renamed from: c */
    public C5663aRz[] mo309c() {
        return ParamSpeechRequirement._m_fields;
    }

    /* renamed from: d */
    public void mo310d() {
        super.mo310d();
        this.key = null;
        this.value = null;
        this.f5442IC = 0;
        this.f5443ID = 0;
        this.f5444IE = 0;
        this.f5445IG = 0;
    }
}
