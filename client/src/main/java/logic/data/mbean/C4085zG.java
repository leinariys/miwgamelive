package logic.data.mbean;

import game.network.message.serializable.C3438ra;
import game.script.Character;
import game.script.spacezone.population.TrainingZonePopulationBehaviour;
import logic.baa.C1616Xf;
import logic.res.code.C5663aRz;
import p001a.C0064Am;

/* renamed from: a.zG */
public class C4085zG extends C3805vD {
    @C0064Am(aul = "3396cc5b617c458ca6af778b5392beb2", aum = 0)
    public C3438ra<Character> caw;
    @C0064Am(aul = "3396cc5b617c458ca6af778b5392beb2", aum = -2)
    public long cax;
    @C0064Am(aul = "3396cc5b617c458ca6af778b5392beb2", aum = -1)
    public byte cay;

    public C4085zG() {
    }

    public C4085zG(C1616Xf xf) {
        super(xf);
    }

    /* renamed from: c */
    public C5663aRz[] mo309c() {
        return TrainingZonePopulationBehaviour.CharacterList._m_fields;
    }

    /* renamed from: d */
    public void mo310d() {
        super.mo310d();
        this.caw = null;
        this.cax = 0;
        this.cay = 0;
    }
}
