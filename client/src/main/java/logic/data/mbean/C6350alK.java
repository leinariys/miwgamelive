package logic.data.mbean;

import game.network.message.serializable.C3438ra;
import game.script.mail.MailBox;
import game.script.mail.MailMessage;
import logic.baa.C1616Xf;
import logic.res.code.C5663aRz;
import p001a.C0064Am;

/* renamed from: a.alK  reason: case insensitive filesystem */
public class C6350alK extends C3805vD {
    @C0064Am(aul = "9ea7fa4f58ed44d4118de35bfa16a5fb", aum = -2)

    /* renamed from: Nf */
    public long f4839Nf;
    @C0064Am(aul = "9ea7fa4f58ed44d4118de35bfa16a5fb", aum = -1)

    /* renamed from: Nh */
    public byte f4840Nh;
    @C0064Am(aul = "4147251704b043c048c5952ed0391a63", aum = 1)
    public C3438ra<MailMessage> fXH;
    @C0064Am(aul = "4147251704b043c048c5952ed0391a63", aum = -2)
    public long fXI;
    @C0064Am(aul = "4147251704b043c048c5952ed0391a63", aum = -1)
    public byte fXJ;
    @C0064Am(aul = "9ea7fa4f58ed44d4118de35bfa16a5fb", aum = 0)
    public String name;

    public C6350alK() {
    }

    public C6350alK(C1616Xf xf) {
        super(xf);
    }

    /* renamed from: c */
    public C5663aRz[] mo309c() {
        return MailBox._m_fields;
    }

    /* renamed from: d */
    public void mo310d() {
        super.mo310d();
        this.name = null;
        this.fXH = null;
        this.f4839Nf = 0;
        this.fXI = 0;
        this.f4840Nh = 0;
        this.fXJ = 0;
    }
}
