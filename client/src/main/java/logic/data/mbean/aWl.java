package logic.data.mbean;

import game.script.resource.Asset;
import game.script.ship.Scenery;
import game.script.space.SpaceCategory;
import logic.baa.C1616Xf;
import logic.res.code.C5663aRz;
import p001a.C0064Am;

import java.util.UUID;

/* renamed from: a.aWl */
public class aWl extends C2217cs {
    @C0064Am(aul = "1f650898fe50a0256e6e21dbd0f87ca6", aum = 0)

    /* renamed from: LQ */
    public Asset f4013LQ;
    @C0064Am(aul = "38eef692bda6f0788db3937d52b975a8", aum = 3)

    /* renamed from: Mk */
    public SpaceCategory f4014Mk;
    @C0064Am(aul = "ef86f0e71d260db379fdf86ca8cc9cae", aum = -2)
    public long avd;
    @C0064Am(aul = "ef86f0e71d260db379fdf86ca8cc9cae", aum = -1)
    public byte avi;
    @C0064Am(aul = "1f650898fe50a0256e6e21dbd0f87ca6", aum = -2)
    public long ayc;
    @C0064Am(aul = "1f650898fe50a0256e6e21dbd0f87ca6", aum = -1)
    public byte ayr;
    @C0064Am(aul = "afb43e15287f047d21fa9b98ede2b27e", aum = 4)

    /* renamed from: bK */
    public UUID f4015bK;
    @C0064Am(aul = "cf58da7a825537725bb6288f36fea103", aum = 2)
    public boolean enL;
    @C0064Am(aul = "cf58da7a825537725bb6288f36fea103", aum = -2)
    public long fEm;
    @C0064Am(aul = "cf58da7a825537725bb6288f36fea103", aum = -1)
    public byte fEn;
    @C0064Am(aul = "38eef692bda6f0788db3937d52b975a8", aum = -2)
    public long gSZ;
    @C0064Am(aul = "38eef692bda6f0788db3937d52b975a8", aum = -1)
    public byte gTe;
    @C0064Am(aul = "5c0e80cc70406dbfe98c80edf3164c9f", aum = 5)
    public String handle;
    @C0064Am(aul = "afb43e15287f047d21fa9b98ede2b27e", aum = -2)

    /* renamed from: oL */
    public long f4016oL;
    @C0064Am(aul = "afb43e15287f047d21fa9b98ede2b27e", aum = -1)

    /* renamed from: oS */
    public byte f4017oS;
    @C0064Am(aul = "5c0e80cc70406dbfe98c80edf3164c9f", aum = -2)

    /* renamed from: ok */
    public long f4018ok;
    @C0064Am(aul = "5c0e80cc70406dbfe98c80edf3164c9f", aum = -1)

    /* renamed from: on */
    public byte f4019on;
    @C0064Am(aul = "ef86f0e71d260db379fdf86ca8cc9cae", aum = 1)

    /* renamed from: uT */
    public String f4020uT;

    public aWl() {
    }

    public aWl(C1616Xf xf) {
        super(xf);
    }

    /* renamed from: c */
    public C5663aRz[] mo309c() {
        return Scenery._m_fields;
    }

    /* renamed from: d */
    public void mo310d() {
        super.mo310d();
        this.f4013LQ = null;
        this.f4020uT = null;
        this.enL = false;
        this.f4014Mk = null;
        this.f4015bK = null;
        this.handle = null;
        this.ayc = 0;
        this.avd = 0;
        this.fEm = 0;
        this.gSZ = 0;
        this.f4016oL = 0;
        this.f4018ok = 0;
        this.ayr = 0;
        this.avi = 0;
        this.fEn = 0;
        this.gTe = 0;
        this.f4017oS = 0;
        this.f4019on = 0;
    }
}
