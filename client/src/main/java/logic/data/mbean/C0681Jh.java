package logic.data.mbean;

import game.network.message.serializable.C3438ra;
import game.script.progression.ProgressionAbility;
import game.script.progression.ProgressionCellType;
import game.script.resource.Asset;
import logic.baa.C1616Xf;
import logic.res.code.C5663aRz;
import p001a.C0064Am;

/* renamed from: a.Jh */
public class C0681Jh extends C5507aLz {
    @C0064Am(aul = "44633f6912ba7d46aff4a02ab179343a", aum = -1)

    /* renamed from: jF */
    public byte f810jF;
    @C0064Am(aul = "bd669d2afac6ff1cbee8cce022568b76", aum = -1)

    /* renamed from: jG */
    public byte f811jG;
    @C0064Am(aul = "11dfe3c17b5e70ce981fd6501af18cdf", aum = -1)

    /* renamed from: jH */
    public byte f812jH;
    @C0064Am(aul = "b3f6b059e27bbae37b0dfeaf09f2aa4e", aum = -1)

    /* renamed from: jI */
    public byte f813jI;
    @C0064Am(aul = "27373c1544fcad0ac2234807ab12fc95", aum = -1)

    /* renamed from: jJ */
    public byte f814jJ;
    @C0064Am(aul = "e377180bf02762d6ff963d09f37edf66", aum = -1)

    /* renamed from: jK */
    public byte f815jK;
    @C0064Am(aul = "44633f6912ba7d46aff4a02ab179343a", aum = 0)

    /* renamed from: ji */
    public C3438ra<ProgressionAbility> f816ji;
    @C0064Am(aul = "bd669d2afac6ff1cbee8cce022568b76", aum = 1)

    /* renamed from: jj */
    public ProgressionCellType f817jj;
    @C0064Am(aul = "11dfe3c17b5e70ce981fd6501af18cdf", aum = 2)

    /* renamed from: jk */
    public int f818jk;
    @C0064Am(aul = "b3f6b059e27bbae37b0dfeaf09f2aa4e", aum = 3)

    /* renamed from: jl */
    public int f819jl;
    @C0064Am(aul = "27373c1544fcad0ac2234807ab12fc95", aum = 4)

    /* renamed from: jm */
    public ProgressionAbility f820jm;
    @C0064Am(aul = "e377180bf02762d6ff963d09f37edf66", aum = 5)

    /* renamed from: jn */
    public Asset f821jn;
    @C0064Am(aul = "44633f6912ba7d46aff4a02ab179343a", aum = -2)

    /* renamed from: jt */
    public long f822jt;
    @C0064Am(aul = "bd669d2afac6ff1cbee8cce022568b76", aum = -2)

    /* renamed from: ju */
    public long f823ju;
    @C0064Am(aul = "11dfe3c17b5e70ce981fd6501af18cdf", aum = -2)

    /* renamed from: jv */
    public long f824jv;
    @C0064Am(aul = "b3f6b059e27bbae37b0dfeaf09f2aa4e", aum = -2)

    /* renamed from: jw */
    public long f825jw;
    @C0064Am(aul = "27373c1544fcad0ac2234807ab12fc95", aum = -2)

    /* renamed from: jx */
    public long f826jx;
    @C0064Am(aul = "e377180bf02762d6ff963d09f37edf66", aum = -2)

    /* renamed from: jy */
    public long f827jy;

    public C0681Jh() {
    }

    public C0681Jh(C1616Xf xf) {
        super(xf);
    }

    /* renamed from: c */
    public C5663aRz[] mo309c() {
        return ProgressionCellType._m_fields;
    }

    /* renamed from: d */
    public void mo310d() {
        super.mo310d();
        this.f816ji = null;
        this.f817jj = null;
        this.f818jk = 0;
        this.f819jl = 0;
        this.f820jm = null;
        this.f821jn = null;
        this.f822jt = 0;
        this.f823ju = 0;
        this.f824jv = 0;
        this.f825jw = 0;
        this.f826jx = 0;
        this.f827jy = 0;
        this.f810jF = 0;
        this.f811jG = 0;
        this.f812jH = 0;
        this.f813jI = 0;
        this.f814jJ = 0;
        this.f815jK = 0;
    }
}
