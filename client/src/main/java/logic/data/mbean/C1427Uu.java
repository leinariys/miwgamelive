package logic.data.mbean;

import game.script.avatar.AvatarSector;
import logic.baa.C1616Xf;
import logic.res.code.C5663aRz;
import p001a.C0064Am;
import taikodom.infra.script.I18NString;

import java.util.UUID;

/* renamed from: a.Uu */
public class C1427Uu extends C5292aDs {
    @C0064Am(aul = "8dcb7c28dca806f724f5889d02e1574d", aum = -2)

    /* renamed from: Nf */
    public long f1827Nf;
    @C0064Am(aul = "8dcb7c28dca806f724f5889d02e1574d", aum = -1)

    /* renamed from: Nh */
    public byte f1828Nh;
    @C0064Am(aul = "554d6e0ebc834500640e1f7f25b48eea", aum = 9)

    /* renamed from: bK */
    public UUID f1829bK;
    @C0064Am(aul = "3ae35b882b9bbcc11b5a11892719a5cc", aum = -2)
    public long bZA;
    @C0064Am(aul = "3ae35b882b9bbcc11b5a11892719a5cc", aum = -1)
    public byte bZB;
    @C0064Am(aul = "0ad2c70a92c2cfaa11af4b9acbc5f4bc", aum = 1)
    public int dDW;
    @C0064Am(aul = "417c962573555cfad04188436e28051d", aum = 2)
    public AvatarSector.C0955a dDY;
    @C0064Am(aul = "0ad2c70a92c2cfaa11af4b9acbc5f4bc", aum = -2)
    public long enZ;
    @C0064Am(aul = "417c962573555cfad04188436e28051d", aum = -2)
    public long eoa;
    @C0064Am(aul = "3a7311ce0a409c83d7d957d895bc2dd9", aum = -2)
    public long eob;
    @C0064Am(aul = "366d73b2ac1d672c443eb111b0792f7a", aum = -2)
    public long eoc;
    @C0064Am(aul = "3da8dfcfee6194b359c3867d99d45b8c", aum = -2)
    public long eod;
    @C0064Am(aul = "d3904c2986f2ea2b500a2b0c7cac4f60", aum = -2)
    public long eoe;
    @C0064Am(aul = "88bef3c63ef4bab40337f0d91501c882", aum = -2)
    public long eof;
    @C0064Am(aul = "0ad2c70a92c2cfaa11af4b9acbc5f4bc", aum = -1)
    public byte eog;
    @C0064Am(aul = "417c962573555cfad04188436e28051d", aum = -1)
    public byte eoh;
    @C0064Am(aul = "3a7311ce0a409c83d7d957d895bc2dd9", aum = -1)
    public byte eoi;
    @C0064Am(aul = "366d73b2ac1d672c443eb111b0792f7a", aum = -1)
    public byte eoj;
    @C0064Am(aul = "3da8dfcfee6194b359c3867d99d45b8c", aum = -1)
    public byte eok;
    @C0064Am(aul = "d3904c2986f2ea2b500a2b0c7cac4f60", aum = -1)
    public byte eom;
    @C0064Am(aul = "88bef3c63ef4bab40337f0d91501c882", aum = -1)
    public byte eon;
    @C0064Am(aul = "88bef3c63ef4bab40337f0d91501c882", aum = 7)
    public float height;
    @C0064Am(aul = "554d6e0ebc834500640e1f7f25b48eea", aum = -2)

    /* renamed from: oL */
    public long f1830oL;
    @C0064Am(aul = "554d6e0ebc834500640e1f7f25b48eea", aum = -1)

    /* renamed from: oS */
    public byte f1831oS;
    @C0064Am(aul = "3a7311ce0a409c83d7d957d895bc2dd9", aum = 3)
    public String prefix;
    @C0064Am(aul = "d3904c2986f2ea2b500a2b0c7cac4f60", aum = 6)
    public float width;
    @C0064Am(aul = "366d73b2ac1d672c443eb111b0792f7a", aum = 4)

    /* renamed from: x */
    public float f1832x;
    @C0064Am(aul = "3da8dfcfee6194b359c3867d99d45b8c", aum = 5)

    /* renamed from: y */
    public float f1833y;
    @C0064Am(aul = "8dcb7c28dca806f724f5889d02e1574d", aum = 0)

    /* renamed from: zP */
    public I18NString f1834zP;
    @C0064Am(aul = "3ae35b882b9bbcc11b5a11892719a5cc", aum = 8)

    /* renamed from: zR */
    public String f1835zR;

    public C1427Uu() {
    }

    public C1427Uu(C1616Xf xf) {
        super(xf);
    }

    /* renamed from: c */
    public C5663aRz[] mo309c() {
        return AvatarSector._m_fields;
    }

    /* renamed from: d */
    public void mo310d() {
        super.mo310d();
        this.f1834zP = null;
        this.dDW = 0;
        this.dDY = null;
        this.prefix = null;
        this.f1832x = 0.0f;
        this.f1833y = 0.0f;
        this.width = 0.0f;
        this.height = 0.0f;
        this.f1835zR = null;
        this.f1829bK = null;
        this.f1827Nf = 0;
        this.enZ = 0;
        this.eoa = 0;
        this.eob = 0;
        this.eoc = 0;
        this.eod = 0;
        this.eoe = 0;
        this.eof = 0;
        this.bZA = 0;
        this.f1830oL = 0;
        this.f1828Nh = 0;
        this.eog = 0;
        this.eoh = 0;
        this.eoi = 0;
        this.eoj = 0;
        this.eok = 0;
        this.eom = 0;
        this.eon = 0;
        this.bZB = 0;
        this.f1831oS = 0;
    }
}
