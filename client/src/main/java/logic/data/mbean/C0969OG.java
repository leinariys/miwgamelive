package logic.data.mbean;

import game.script.Actor;
import game.script.ai.npc.TurretAIController;
import logic.baa.C1616Xf;
import logic.res.code.C5663aRz;
import p001a.C0064Am;

/* renamed from: a.OG */
public class C0969OG extends C3805vD {
    @C0064Am(aul = "83b07c3ee75f3acbc7e141f5cb945cfd", aum = -1)
    public byte dNA;
    @C0064Am(aul = "8e40402bc2426950c26087a09985b69f", aum = -1)
    public byte dNB;
    @C0064Am(aul = "8e40402bc2426950c26087a09985b69f", aum = 1)
    public float dNx;
    @C0064Am(aul = "83b07c3ee75f3acbc7e141f5cb945cfd", aum = -2)
    public long dNy;
    @C0064Am(aul = "8e40402bc2426950c26087a09985b69f", aum = -2)
    public long dNz;
    @C0064Am(aul = "83b07c3ee75f3acbc7e141f5cb945cfd", aum = 0)

    /* renamed from: pI */
    public Actor f1283pI;

    public C0969OG() {
    }

    public C0969OG(C1616Xf xf) {
        super(xf);
    }

    /* renamed from: c */
    public C5663aRz[] mo309c() {
        return TurretAIController.Target._m_fields;
    }

    /* renamed from: d */
    public void mo310d() {
        super.mo310d();
        this.f1283pI = null;
        this.dNx = 0.0f;
        this.dNy = 0;
        this.dNz = 0;
        this.dNA = 0;
        this.dNB = 0;
    }
}
