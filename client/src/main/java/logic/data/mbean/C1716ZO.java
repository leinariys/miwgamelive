package logic.data.mbean;

import game.network.message.serializable.C1556Wo;
import game.script.Actor;
import game.script.damage.DamageType;
import game.script.item.Weapon;
import game.script.item.WeaponAdapter;
import game.script.resource.Asset;
import game.script.util.AdapterLikedList;
import logic.baa.C1616Xf;
import logic.res.code.C5663aRz;
import p001a.C0064Am;
import p001a.C6661arJ;

/* renamed from: a.ZO */
public class C1716ZO extends C0496Gs {
    @C0064Am(aul = "28d193b4b41f7eeae3b52a7761c88ec7", aum = -2)
    public long aIR;
    @C0064Am(aul = "28d193b4b41f7eeae3b52a7761c88ec7", aum = -1)
    public byte aJc;
    @C0064Am(aul = "1908d4b18b30e236c9ca6f1a5795e80b", aum = 14)
    public AdapterLikedList<WeaponAdapter> aOw;
    @C0064Am(aul = "1908d4b18b30e236c9ca6f1a5795e80b", aum = -2)
    public long aUE;
    @C0064Am(aul = "1908d4b18b30e236c9ca6f1a5795e80b", aum = -1)
    public byte aUH;
    @C0064Am(aul = "0b89e7fb588c235be6adbb41c7f0aaeb", aum = 3)
    public boolean bEj;
    @C0064Am(aul = "ddf141510f9bf75a3a8512f4a7e75ebd", aum = 9)
    public Asset bIA;
    @C0064Am(aul = "0f77233765171f81d767a4f3930124ab", aum = 10)
    public Asset bIB;
    @C0064Am(aul = "e286423a89fb1077b0d3fda35f5ca650", aum = 11)
    public Asset bIC;
    @C0064Am(aul = "5091d457eb48a785d3a009f97df3076f", aum = 12)
    public Asset bID;
    @C0064Am(aul = "e5a94482480be8789b930b2c36e60215", aum = -2)
    public long bIE;
    @C0064Am(aul = "a92452d908e30e61b1d210c51b3f1696", aum = -2)
    public long bIF;
    @C0064Am(aul = "0b89e7fb588c235be6adbb41c7f0aaeb", aum = -2)
    public long bIG;
    @C0064Am(aul = "ea767002f442f7ddcdf724f334e954b3", aum = -2)
    public long bIH;
    @C0064Am(aul = "ac5b0b6d70e10232649ad46a7c5f33ac", aum = -2)
    public long bII;
    @C0064Am(aul = "f1d744481799a93d56aa362e2a8b7bf1", aum = -2)
    public long bIJ;
    @C0064Am(aul = "ab15e7fed836622bf82f723df88c2045", aum = -2)
    public long bIK;
    @C0064Am(aul = "b55ccbc00627101f76f41d4cd2f019af", aum = -2)
    public long bIL;
    @C0064Am(aul = "ddf141510f9bf75a3a8512f4a7e75ebd", aum = -2)
    public long bIM;
    @C0064Am(aul = "0f77233765171f81d767a4f3930124ab", aum = -2)
    public long bIN;
    @C0064Am(aul = "e286423a89fb1077b0d3fda35f5ca650", aum = -2)
    public long bIO;
    @C0064Am(aul = "5091d457eb48a785d3a009f97df3076f", aum = -2)
    public long bIP;
    @C0064Am(aul = "e5a94482480be8789b930b2c36e60215", aum = -1)
    public byte bIQ;
    @C0064Am(aul = "a92452d908e30e61b1d210c51b3f1696", aum = -1)
    public byte bIR;
    @C0064Am(aul = "0b89e7fb588c235be6adbb41c7f0aaeb", aum = -1)
    public byte bIS;
    @C0064Am(aul = "ea767002f442f7ddcdf724f334e954b3", aum = -1)
    public byte bIT;
    @C0064Am(aul = "ac5b0b6d70e10232649ad46a7c5f33ac", aum = -1)
    public byte bIU;
    @C0064Am(aul = "f1d744481799a93d56aa362e2a8b7bf1", aum = -1)
    public byte bIV;
    @C0064Am(aul = "ab15e7fed836622bf82f723df88c2045", aum = -1)
    public byte bIW;
    @C0064Am(aul = "b55ccbc00627101f76f41d4cd2f019af", aum = -1)
    public byte bIX;
    @C0064Am(aul = "ddf141510f9bf75a3a8512f4a7e75ebd", aum = -1)
    public byte bIY;
    @C0064Am(aul = "0f77233765171f81d767a4f3930124ab", aum = -1)
    public byte bIZ;
    @C0064Am(aul = "e5a94482480be8789b930b2c36e60215", aum = 0)
    public boolean bIt;
    @C0064Am(aul = "a92452d908e30e61b1d210c51b3f1696", aum = 1)
    public Asset bIu;
    @C0064Am(aul = "2663f34cccae299be98a837220849635", aum = 2)
    public C1556Wo<DamageType, Float> bIv;
    @C0064Am(aul = "ea767002f442f7ddcdf724f334e954b3", aum = 4)
    public float bIw;
    @C0064Am(aul = "ac5b0b6d70e10232649ad46a7c5f33ac", aum = 5)
    public Asset bIx;
    @C0064Am(aul = "ab15e7fed836622bf82f723df88c2045", aum = 7)
    public float bIy;
    @C0064Am(aul = "b55ccbc00627101f76f41d4cd2f019af", aum = 8)
    public Asset bIz;
    @C0064Am(aul = "e286423a89fb1077b0d3fda35f5ca650", aum = -1)
    public byte bJa;
    @C0064Am(aul = "5091d457eb48a785d3a009f97df3076f", aum = -1)
    public byte bJb;
    @C0064Am(aul = "2663f34cccae299be98a837220849635", aum = -2)
    public long beq;
    @C0064Am(aul = "2663f34cccae299be98a837220849635", aum = -1)
    public byte bes;
    @C0064Am(aul = "e66413102801c0cc17c24a2dcb7a92a6", aum = 20)
    public boolean eQA;
    @C0064Am(aul = "8cfdbbd2a4d6ff4c5812ebd2989b15e4", aum = 21)
    public float eQB;
    @C0064Am(aul = "3195b405f75626196fbf5fe9520e506b", aum = 22)
    public float eQC;
    @C0064Am(aul = "d7af72c8346a750c70c9967bd932fde4", aum = -2)
    public long eQD;
    @C0064Am(aul = "874e0404fddbb8fd8cdaff40919fdb7a", aum = -2)
    public long eQE;
    @C0064Am(aul = "9b19e0c3d9c083a4c6c9e0429096bd21", aum = -2)
    public long eQF;
    @C0064Am(aul = "dc1126ae2f6fd703228d03f97618eb7a", aum = -2)
    public long eQG;
    @C0064Am(aul = "f02240d9a0a665959ea7c26bb44b6e25", aum = -2)
    public long eQH;
    @C0064Am(aul = "e66413102801c0cc17c24a2dcb7a92a6", aum = -2)
    public long eQI;
    @C0064Am(aul = "8cfdbbd2a4d6ff4c5812ebd2989b15e4", aum = -2)
    public long eQJ;
    @C0064Am(aul = "3195b405f75626196fbf5fe9520e506b", aum = -2)
    public long eQK;
    @C0064Am(aul = "d7af72c8346a750c70c9967bd932fde4", aum = -1)
    public byte eQL;
    @C0064Am(aul = "874e0404fddbb8fd8cdaff40919fdb7a", aum = -1)
    public byte eQM;
    @C0064Am(aul = "9b19e0c3d9c083a4c6c9e0429096bd21", aum = -1)
    public byte eQN;
    @C0064Am(aul = "dc1126ae2f6fd703228d03f97618eb7a", aum = -1)
    public byte eQO;
    @C0064Am(aul = "f02240d9a0a665959ea7c26bb44b6e25", aum = -1)
    public byte eQP;
    @C0064Am(aul = "e66413102801c0cc17c24a2dcb7a92a6", aum = -1)
    public byte eQQ;
    @C0064Am(aul = "8cfdbbd2a4d6ff4c5812ebd2989b15e4", aum = -1)
    public byte eQR;
    @C0064Am(aul = "3195b405f75626196fbf5fe9520e506b", aum = -1)
    public byte eQS;
    @C0064Am(aul = "d7af72c8346a750c70c9967bd932fde4", aum = 15)
    public boolean eQv;
    @C0064Am(aul = "874e0404fddbb8fd8cdaff40919fdb7a", aum = 16)
    public Actor eQw;
    @C0064Am(aul = "9b19e0c3d9c083a4c6c9e0429096bd21", aum = 17)
    public C6661arJ eQx;
    @C0064Am(aul = "dc1126ae2f6fd703228d03f97618eb7a", aum = 18)
    public C1556Wo<DamageType, Float> eQy;
    @C0064Am(aul = "f02240d9a0a665959ea7c26bb44b6e25", aum = 19)
    public float eQz;
    @C0064Am(aul = "f1d744481799a93d56aa362e2a8b7bf1", aum = 6)
    public float range;
    @C0064Am(aul = "28d193b4b41f7eeae3b52a7761c88ec7", aum = 13)
    public float speed;

    public C1716ZO() {
    }

    public C1716ZO(C1616Xf xf) {
        super(xf);
    }

    /* renamed from: c */
    public C5663aRz[] mo309c() {
        return Weapon._m_fields;
    }

    /* renamed from: d */
    public void mo310d() {
        super.mo310d();
        this.bIt = false;
        this.bIu = null;
        this.bIv = null;
        this.bEj = false;
        this.bIw = 0.0f;
        this.bIx = null;
        this.range = 0.0f;
        this.bIy = 0.0f;
        this.bIz = null;
        this.bIA = null;
        this.bIB = null;
        this.bIC = null;
        this.bID = null;
        this.speed = 0.0f;
        this.aOw = null;
        this.eQv = false;
        this.eQw = null;
        this.eQx = null;
        this.eQy = null;
        this.eQz = 0.0f;
        this.eQA = false;
        this.eQB = 0.0f;
        this.eQC = 0.0f;
        this.bIE = 0;
        this.bIF = 0;
        this.beq = 0;
        this.bIG = 0;
        this.bIH = 0;
        this.bII = 0;
        this.bIJ = 0;
        this.bIK = 0;
        this.bIL = 0;
        this.bIM = 0;
        this.bIN = 0;
        this.bIO = 0;
        this.bIP = 0;
        this.aIR = 0;
        this.aUE = 0;
        this.eQD = 0;
        this.eQE = 0;
        this.eQF = 0;
        this.eQG = 0;
        this.eQH = 0;
        this.eQI = 0;
        this.eQJ = 0;
        this.eQK = 0;
        this.bIQ = 0;
        this.bIR = 0;
        this.bes = 0;
        this.bIS = 0;
        this.bIT = 0;
        this.bIU = 0;
        this.bIV = 0;
        this.bIW = 0;
        this.bIX = 0;
        this.bIY = 0;
        this.bIZ = 0;
        this.bJa = 0;
        this.bJb = 0;
        this.aJc = 0;
        this.aUH = 0;
        this.eQL = 0;
        this.eQM = 0;
        this.eQN = 0;
        this.eQO = 0;
        this.eQP = 0;
        this.eQQ = 0;
        this.eQR = 0;
        this.eQS = 0;
    }
}
