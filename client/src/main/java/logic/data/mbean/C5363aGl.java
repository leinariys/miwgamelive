package logic.data.mbean;

import game.CollisionFXSet;
import game.network.message.serializable.C3438ra;
import game.script.itemgen.ItemGenTable;
import game.script.resource.Asset;
import game.script.ship.Hull;
import game.script.space.Asteroid;
import game.script.space.AsteroidLootType;
import game.script.spacezone.AsteroidZoneCategory;
import logic.baa.C1616Xf;
import logic.res.code.C5663aRz;
import p001a.C0064Am;
import taikodom.infra.script.I18NString;

/* renamed from: a.aGl  reason: case insensitive filesystem */
public class C5363aGl extends C2217cs {
    @C0064Am(aul = "fd5eb56145f6fdd734f5fe2edc69af28", aum = 6)

    /* renamed from: LQ */
    public Asset f2879LQ;
    @C0064Am(aul = "3e481a4db7a93f2c52538a091ae46145", aum = -2)

    /* renamed from: Nf */
    public long f2880Nf;
    @C0064Am(aul = "3e481a4db7a93f2c52538a091ae46145", aum = -1)

    /* renamed from: Nh */
    public byte f2881Nh;
    @C0064Am(aul = "386f8c2aec570f4b144e7bcf6e189aa4", aum = 9)

    /* renamed from: VV */
    public Hull f2882VV;
    @C0064Am(aul = "6e1e5289241a3cb8bb9adb95efc4d271", aum = 1)

    /* renamed from: Wb */
    public Asset f2883Wb;
    @C0064Am(aul = "4ab96e3a77f4141ae910d50fd07db76f", aum = 2)

    /* renamed from: Wd */
    public Asset f2884Wd;
    @C0064Am(aul = "af9194eb00f825764b0fe8f86c5405f9", aum = -2)
    public long avd;
    @C0064Am(aul = "af9194eb00f825764b0fe8f86c5405f9", aum = -1)
    public byte avi;
    @C0064Am(aul = "14f2056516293f98f34b1f1bb7a7e9e2", aum = 3)
    public C3438ra<ItemGenTable> ayZ;
    @C0064Am(aul = "fd5eb56145f6fdd734f5fe2edc69af28", aum = -2)
    public long ayc;
    @C0064Am(aul = "fd5eb56145f6fdd734f5fe2edc69af28", aum = -1)
    public byte ayr;
    @C0064Am(aul = "14f2056516293f98f34b1f1bb7a7e9e2", aum = -1)
    public byte azC;
    @C0064Am(aul = "14f2056516293f98f34b1f1bb7a7e9e2", aum = -2)
    public long azn;
    @C0064Am(aul = "6e1e5289241a3cb8bb9adb95efc4d271", aum = -1)
    public byte bhC;
    @C0064Am(aul = "4ab96e3a77f4141ae910d50fd07db76f", aum = -1)
    public byte bhD;
    @C0064Am(aul = "386f8c2aec570f4b144e7bcf6e189aa4", aum = -1)
    public byte bhF;
    @C0064Am(aul = "6e1e5289241a3cb8bb9adb95efc4d271", aum = -2)
    public long bht;
    @C0064Am(aul = "4ab96e3a77f4141ae910d50fd07db76f", aum = -2)
    public long bhu;
    @C0064Am(aul = "386f8c2aec570f4b144e7bcf6e189aa4", aum = -2)
    public long bhw;
    @C0064Am(aul = "ff06521294be3eeaa6ddc4a1441274ff", aum = 0)
    public CollisionFXSet bnE;
    @C0064Am(aul = "d81325dc52e6d162b8e3a6cd145f4229", aum = 7)
    public Asset boc;
    @C0064Am(aul = "ff06521294be3eeaa6ddc4a1441274ff", aum = -2)
    public long fPc;
    @C0064Am(aul = "d81325dc52e6d162b8e3a6cd145f4229", aum = -2)
    public long fPf;
    @C0064Am(aul = "ff06521294be3eeaa6ddc4a1441274ff", aum = -1)
    public byte fPj;
    @C0064Am(aul = "d81325dc52e6d162b8e3a6cd145f4229", aum = -1)
    public byte fPm;
    @C0064Am(aul = "25863e8583c60570c2ba57ec8919328c", aum = 10)
    public AsteroidZoneCategory hPq;
    @C0064Am(aul = "25863e8583c60570c2ba57ec8919328c", aum = -2)
    public long hPr;
    @C0064Am(aul = "25863e8583c60570c2ba57ec8919328c", aum = -1)
    public byte hPs;
    @C0064Am(aul = "e61a7266c1c05a5e74de0070d535295b", aum = 4)
    public AsteroidLootType hhb;
    @C0064Am(aul = "e61a7266c1c05a5e74de0070d535295b", aum = -2)
    public long hhc;
    @C0064Am(aul = "e61a7266c1c05a5e74de0070d535295b", aum = -1)
    public byte hhd;
    @C0064Am(aul = "af9194eb00f825764b0fe8f86c5405f9", aum = 8)

    /* renamed from: uT */
    public String f2885uT;
    @C0064Am(aul = "3e481a4db7a93f2c52538a091ae46145", aum = 5)

    /* renamed from: zP */
    public I18NString f2886zP;

    public C5363aGl() {
    }

    public C5363aGl(C1616Xf xf) {
        super(xf);
    }

    /* renamed from: c */
    public C5663aRz[] mo309c() {
        return Asteroid._m_fields;
    }

    /* renamed from: d */
    public void mo310d() {
        super.mo310d();
        this.bnE = null;
        this.f2883Wb = null;
        this.f2884Wd = null;
        this.ayZ = null;
        this.hhb = null;
        this.f2886zP = null;
        this.f2879LQ = null;
        this.boc = null;
        this.f2885uT = null;
        this.f2882VV = null;
        this.hPq = null;
        this.fPc = 0;
        this.bht = 0;
        this.bhu = 0;
        this.azn = 0;
        this.hhc = 0;
        this.f2880Nf = 0;
        this.ayc = 0;
        this.fPf = 0;
        this.avd = 0;
        this.bhw = 0;
        this.hPr = 0;
        this.fPj = 0;
        this.bhC = 0;
        this.bhD = 0;
        this.azC = 0;
        this.hhd = 0;
        this.f2881Nh = 0;
        this.ayr = 0;
        this.fPm = 0;
        this.avi = 0;
        this.bhF = 0;
        this.hPs = 0;
    }
}
