package logic.data.mbean;

import game.script.citizenship.CitizenshipPack;
import game.script.citizenship.CitizenshipType;
import logic.baa.C1616Xf;
import logic.res.code.C5663aRz;
import p001a.C0064Am;

import java.util.UUID;

/* renamed from: a.Wv */
public class C1563Wv extends C3805vD {
    @C0064Am(aul = "5499f1ecc1e4ffeaa4c2cc087be9b20e", aum = 1)

    /* renamed from: bK */
    public UUID f2055bK;
    @C0064Am(aul = "5ccae1a6fe0a2a711d1b85864b89c6e2", aum = 3)

    /* renamed from: db */
    public long f2056db;
    @C0064Am(aul = "5ccae1a6fe0a2a711d1b85864b89c6e2", aum = -2)

    /* renamed from: dj */
    public long f2057dj;
    @C0064Am(aul = "5ccae1a6fe0a2a711d1b85864b89c6e2", aum = -1)

    /* renamed from: dq */
    public byte f2058dq;
    @C0064Am(aul = "cedc00414bdca7e2946dcad830dc42f2", aum = 4)
    public float egP;
    @C0064Am(aul = "cedc00414bdca7e2946dcad830dc42f2", aum = -2)
    public long egT;
    @C0064Am(aul = "cedc00414bdca7e2946dcad830dc42f2", aum = -1)
    public byte egX;
    @C0064Am(aul = "610a2db94a538f56c61326af5fea22e7", aum = 2)
    public CitizenshipType exB;
    @C0064Am(aul = "610a2db94a538f56c61326af5fea22e7", aum = -2)
    public long exC;
    @C0064Am(aul = "610a2db94a538f56c61326af5fea22e7", aum = -1)
    public byte exD;
    @C0064Am(aul = "545649474d607ed4d9d5a69f844c66bf", aum = 0)
    public String handle;
    @C0064Am(aul = "5499f1ecc1e4ffeaa4c2cc087be9b20e", aum = -2)

    /* renamed from: oL */
    public long f2059oL;
    @C0064Am(aul = "5499f1ecc1e4ffeaa4c2cc087be9b20e", aum = -1)

    /* renamed from: oS */
    public byte f2060oS;
    @C0064Am(aul = "545649474d607ed4d9d5a69f844c66bf", aum = -2)

    /* renamed from: ok */
    public long f2061ok;
    @C0064Am(aul = "545649474d607ed4d9d5a69f844c66bf", aum = -1)

    /* renamed from: on */
    public byte f2062on;

    public C1563Wv() {
    }

    public C1563Wv(C1616Xf xf) {
        super(xf);
    }

    /* renamed from: c */
    public C5663aRz[] mo309c() {
        return CitizenshipPack._m_fields;
    }

    /* renamed from: d */
    public void mo310d() {
        super.mo310d();
        this.handle = null;
        this.f2055bK = null;
        this.exB = null;
        this.f2056db = 0;
        this.egP = 0.0f;
        this.f2061ok = 0;
        this.f2059oL = 0;
        this.exC = 0;
        this.f2057dj = 0;
        this.egT = 0;
        this.f2062on = 0;
        this.f2060oS = 0;
        this.exD = 0;
        this.f2058dq = 0;
        this.egX = 0;
    }
}
