package logic.data.mbean;

import game.script.mission.actions.SetParamMissionAction;
import logic.baa.C1616Xf;
import logic.res.code.C5663aRz;
import p001a.C0064Am;

/* renamed from: a.aBP */
public class aBP extends C2503fv {
    @C0064Am(aul = "ba00033597e6b36de8a4f3e1bef48a45", aum = -2)

    /* renamed from: IC */
    public long f2412IC;
    @C0064Am(aul = "c4e1a4f0a7158f7af683583d691a02ec", aum = -2)

    /* renamed from: ID */
    public long f2413ID;
    @C0064Am(aul = "ba00033597e6b36de8a4f3e1bef48a45", aum = -1)

    /* renamed from: IE */
    public byte f2414IE;
    @C0064Am(aul = "c4e1a4f0a7158f7af683583d691a02ec", aum = -1)

    /* renamed from: IG */
    public byte f2415IG;
    @C0064Am(aul = "ba00033597e6b36de8a4f3e1bef48a45", aum = 0)
    public String key;
    @C0064Am(aul = "c4e1a4f0a7158f7af683583d691a02ec", aum = 1)
    public String value;

    public aBP() {
    }

    public aBP(C1616Xf xf) {
        super(xf);
    }

    /* renamed from: c */
    public C5663aRz[] mo309c() {
        return SetParamMissionAction._m_fields;
    }

    /* renamed from: d */
    public void mo310d() {
        super.mo310d();
        this.key = null;
        this.value = null;
        this.f2412IC = 0;
        this.f2413ID = 0;
        this.f2414IE = 0;
        this.f2415IG = 0;
    }
}
