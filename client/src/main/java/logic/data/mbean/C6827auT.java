package logic.data.mbean;

import game.script.ship.categories.FighterType;
import game.script.ship.speedBoost.SpeedBoostType;
import logic.baa.C1616Xf;
import logic.res.code.C5663aRz;
import p001a.C0064Am;

/* renamed from: a.auT  reason: case insensitive filesystem */
public class C6827auT extends aLL {
    @C0064Am(aul = "17c794981d0e9896cfb7bb55d697dafd", aum = 0)
    public SpeedBoostType dDF;
    @C0064Am(aul = "17c794981d0e9896cfb7bb55d697dafd", aum = -2)
    public long fVd;
    @C0064Am(aul = "17c794981d0e9896cfb7bb55d697dafd", aum = -1)
    public byte fVf;

    public C6827auT() {
    }

    public C6827auT(C1616Xf xf) {
        super(xf);
    }

    /* renamed from: c */
    public C5663aRz[] mo309c() {
        return FighterType._m_fields;
    }

    /* renamed from: d */
    public void mo310d() {
        super.mo310d();
        this.dDF = null;
        this.fVd = 0;
        this.fVf = 0;
    }
}
