package logic.data.mbean;

import game.script.avatar.ColorWrapper;
import game.script.avatar.body.TextureScheme;
import game.script.resource.Asset;
import logic.baa.C1616Xf;
import logic.res.code.C5663aRz;
import p001a.C0064Am;

import java.util.UUID;

/* renamed from: a.Js */
public class C0692Js extends C2424fK {
    @C0064Am(aul = "fa018aa533dace6962db65aff6422619", aum = 4)

    /* renamed from: bK */
    public UUID f881bK;
    @C0064Am(aul = "0d1f7e3ca2d2d0a065268055aff3b740", aum = -2)
    public long bZA;
    @C0064Am(aul = "0d1f7e3ca2d2d0a065268055aff3b740", aum = -1)
    public byte bZB;
    @C0064Am(aul = "5748d72707de21234359a6c93ec01b1f", aum = -2)
    public long djA;
    @C0064Am(aul = "c83abb15ddf1a9d75eaf9a5a76dc0936", aum = -2)
    public long djB;
    @C0064Am(aul = "c02c4cc0a0082b5e7a6bfe7a2e0800c2", aum = -2)
    public long djC;
    @C0064Am(aul = "5748d72707de21234359a6c93ec01b1f", aum = -1)
    public byte djD;
    @C0064Am(aul = "c83abb15ddf1a9d75eaf9a5a76dc0936", aum = -1)
    public byte djE;
    @C0064Am(aul = "c02c4cc0a0082b5e7a6bfe7a2e0800c2", aum = -1)
    public byte djF;
    @C0064Am(aul = "5748d72707de21234359a6c93ec01b1f", aum = 0)
    public Asset djx;
    @C0064Am(aul = "c83abb15ddf1a9d75eaf9a5a76dc0936", aum = 1)
    public Asset djy;
    @C0064Am(aul = "c02c4cc0a0082b5e7a6bfe7a2e0800c2", aum = 2)
    public ColorWrapper djz;
    @C0064Am(aul = "fa018aa533dace6962db65aff6422619", aum = -2)

    /* renamed from: oL */
    public long f882oL;
    @C0064Am(aul = "fa018aa533dace6962db65aff6422619", aum = -1)

    /* renamed from: oS */
    public byte f883oS;
    @C0064Am(aul = "0d1f7e3ca2d2d0a065268055aff3b740", aum = 3)

    /* renamed from: zR */
    public String f884zR;

    public C0692Js() {
    }

    public C0692Js(C1616Xf xf) {
        super(xf);
    }

    /* renamed from: c */
    public C5663aRz[] mo309c() {
        return TextureScheme._m_fields;
    }

    /* renamed from: d */
    public void mo310d() {
        super.mo310d();
        this.djx = null;
        this.djy = null;
        this.djz = null;
        this.f884zR = null;
        this.f881bK = null;
        this.djA = 0;
        this.djB = 0;
        this.djC = 0;
        this.bZA = 0;
        this.f882oL = 0;
        this.djD = 0;
        this.djE = 0;
        this.djF = 0;
        this.bZB = 0;
        this.f883oS = 0;
    }
}
