package logic.data.mbean;

import game.script.missiontemplate.WaypointDat;
import game.script.missiontemplate.scripting.FollowMissionScriptTemplate;
import game.script.space.StellarSystem;
import logic.baa.C1616Xf;
import logic.res.code.C5663aRz;
import p001a.C0064Am;

/* renamed from: a.aci  reason: case insensitive filesystem */
public class C5906aci extends C0597IR {
    @C0064Am(aul = "c46c38121d7f4ff38bad871f7e773487", aum = 1)
    public WaypointDat faR;
    @C0064Am(aul = "45e64b1b71601688a4df1e43512802d9", aum = 2)
    public WaypointDat faS;
    @C0064Am(aul = "3780b216758b3d4e3abdd2918970e2af", aum = 3)
    public WaypointDat faT;
    @C0064Am(aul = "c46c38121d7f4ff38bad871f7e773487", aum = -2)
    public long faU;
    @C0064Am(aul = "45e64b1b71601688a4df1e43512802d9", aum = -2)
    public long faV;
    @C0064Am(aul = "3780b216758b3d4e3abdd2918970e2af", aum = -2)
    public long faW;
    @C0064Am(aul = "c46c38121d7f4ff38bad871f7e773487", aum = -1)
    public byte faX;
    @C0064Am(aul = "45e64b1b71601688a4df1e43512802d9", aum = -1)
    public byte faY;
    @C0064Am(aul = "3780b216758b3d4e3abdd2918970e2af", aum = -1)
    public byte faZ;
    @C0064Am(aul = "37fe301fcdd15017c9c2345d87b4d8fa", aum = 0)

    /* renamed from: rI */
    public StellarSystem f4269rI;
    @C0064Am(aul = "37fe301fcdd15017c9c2345d87b4d8fa", aum = -2)

    /* renamed from: rY */
    public long f4270rY;
    @C0064Am(aul = "37fe301fcdd15017c9c2345d87b4d8fa", aum = -1)

    /* renamed from: so */
    public byte f4271so;

    public C5906aci() {
    }

    public C5906aci(C1616Xf xf) {
        super(xf);
    }

    /* renamed from: c */
    public C5663aRz[] mo309c() {
        return FollowMissionScriptTemplate._m_fields;
    }

    /* renamed from: d */
    public void mo310d() {
        super.mo310d();
        this.f4269rI = null;
        this.faR = null;
        this.faS = null;
        this.faT = null;
        this.f4270rY = 0;
        this.faU = 0;
        this.faV = 0;
        this.faW = 0;
        this.f4271so = 0;
        this.faX = 0;
        this.faY = 0;
        this.faZ = 0;
    }
}
