package logic.data.mbean;

import game.script.mission.actions.CreateBeaconPathToActor;
import logic.baa.C0468GU;
import logic.baa.C1616Xf;
import logic.res.code.C5663aRz;
import p001a.C0064Am;

/* renamed from: a.awY */
public class awY extends C2503fv {
    @C0064Am(aul = "b93b6c2601ca570202123ad7ba022dc7", aum = 0)
    public C0468GU gLE;
    @C0064Am(aul = "b93b6c2601ca570202123ad7ba022dc7", aum = -2)
    public long gOq;
    @C0064Am(aul = "b93b6c2601ca570202123ad7ba022dc7", aum = -1)
    public byte gOr;

    public awY() {
    }

    public awY(C1616Xf xf) {
        super(xf);
    }

    /* renamed from: c */
    public C5663aRz[] mo309c() {
        return CreateBeaconPathToActor._m_fields;
    }

    /* renamed from: d */
    public void mo310d() {
        super.mo310d();
        this.gLE = null;
        this.gOq = 0;
        this.gOr = 0;
    }
}
