package logic.data.mbean;

import game.network.message.serializable.C1556Wo;
import game.script.mission.scripting.ProtectMissionScript;
import game.script.npc.NPC;
import logic.baa.C1616Xf;
import logic.res.code.C5663aRz;
import p001a.C0064Am;

/* renamed from: a.ak */
public class C1924ak extends aMY {
    @C0064Am(aul = "5ad3574595545cd772b4ca108740c25e", aum = 0)

    /* renamed from: hU */
    public C1556Wo<String, NPC> f4737hU;
    @C0064Am(aul = "718cae164cd84abc64cc926b74cbdfb6", aum = 1)

    /* renamed from: hV */
    public C1556Wo<String, NPC> f4738hV;
    @C0064Am(aul = "44379cad1b6986cbc073fcb1d7403034", aum = 2)

    /* renamed from: hW */
    public C1556Wo<String, NPC> f4739hW;
    @C0064Am(aul = "afa2c18b485ee82344b344bb9bc16294", aum = 3)

    /* renamed from: hX */
    public int f4740hX;
    @C0064Am(aul = "5f3d723f90095ef278ea35e0e5a302db", aum = 4)

    /* renamed from: hY */
    public int f4741hY;
    @C0064Am(aul = "5ad3574595545cd772b4ca108740c25e", aum = -2)

    /* renamed from: hZ */
    public long f4742hZ;
    @C0064Am(aul = "718cae164cd84abc64cc926b74cbdfb6", aum = -2)

    /* renamed from: ia */
    public long f4743ia;
    @C0064Am(aul = "44379cad1b6986cbc073fcb1d7403034", aum = -2)

    /* renamed from: ib */
    public long f4744ib;
    @C0064Am(aul = "afa2c18b485ee82344b344bb9bc16294", aum = -2)

    /* renamed from: ic */
    public long f4745ic;
    @C0064Am(aul = "5f3d723f90095ef278ea35e0e5a302db", aum = -2)

    /* renamed from: ie */
    public long f4746ie;
    @C0064Am(aul = "5ad3574595545cd772b4ca108740c25e", aum = -1)

    /* renamed from: if */
    public byte f4747if;
    @C0064Am(aul = "718cae164cd84abc64cc926b74cbdfb6", aum = -1)

    /* renamed from: ig */
    public byte f4748ig;
    @C0064Am(aul = "44379cad1b6986cbc073fcb1d7403034", aum = -1)

    /* renamed from: ih */
    public byte f4749ih;
    @C0064Am(aul = "afa2c18b485ee82344b344bb9bc16294", aum = -1)

    /* renamed from: ii */
    public byte f4750ii;
    @C0064Am(aul = "5f3d723f90095ef278ea35e0e5a302db", aum = -1)

    /* renamed from: ij */
    public byte f4751ij;

    public C1924ak() {
    }

    public C1924ak(C1616Xf xf) {
        super(xf);
    }

    /* renamed from: c */
    public C5663aRz[] mo309c() {
        return ProtectMissionScript._m_fields;
    }

    /* renamed from: d */
    public void mo310d() {
        super.mo310d();
        this.f4737hU = null;
        this.f4738hV = null;
        this.f4739hW = null;
        this.f4740hX = 0;
        this.f4741hY = 0;
        this.f4742hZ = 0;
        this.f4743ia = 0;
        this.f4744ib = 0;
        this.f4745ic = 0;
        this.f4746ie = 0;
        this.f4747if = 0;
        this.f4748ig = 0;
        this.f4749ih = 0;
        this.f4750ii = 0;
        this.f4751ij = 0;
    }
}
