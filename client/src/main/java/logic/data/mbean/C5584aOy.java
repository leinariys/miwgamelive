package logic.data.mbean;

import game.network.message.serializable.C3438ra;
import game.script.citizenship.CitizenshipType;
import game.script.corporation.CorporationLogo;
import game.script.player.PlayerView;
import logic.baa.C1616Xf;
import logic.res.code.C5663aRz;
import p001a.C0064Am;

/* renamed from: a.aOy  reason: case insensitive filesystem */
public class C5584aOy extends C0178CE {
    @C0064Am(aul = "7144716bfc84f175704d2bf894762369", aum = 2)
    public C3438ra<CitizenshipType> dWR;
    @C0064Am(aul = "be47b6cd60bda7684c638e76b43410cb", aum = 0)
    public String eIP;
    @C0064Am(aul = "580706d67f2f5a71422523872175824e", aum = 1)
    public CorporationLogo eIR;
    @C0064Am(aul = "7144716bfc84f175704d2bf894762369", aum = -2)
    public long fEW;
    @C0064Am(aul = "7144716bfc84f175704d2bf894762369", aum = -1)
    public byte fEZ;
    @C0064Am(aul = "be47b6cd60bda7684c638e76b43410cb", aum = -2)
    public long izA;
    @C0064Am(aul = "580706d67f2f5a71422523872175824e", aum = -2)
    public long izB;
    @C0064Am(aul = "be47b6cd60bda7684c638e76b43410cb", aum = -1)
    public byte izC;
    @C0064Am(aul = "580706d67f2f5a71422523872175824e", aum = -1)
    public byte izD;

    public C5584aOy() {
    }

    public C5584aOy(C1616Xf xf) {
        super(xf);
    }

    /* renamed from: c */
    public C5663aRz[] mo309c() {
        return PlayerView._m_fields;
    }

    /* renamed from: d */
    public void mo310d() {
        super.mo310d();
        this.eIP = null;
        this.eIR = null;
        this.dWR = null;
        this.izA = 0;
        this.izB = 0;
        this.fEW = 0;
        this.izC = 0;
        this.izD = 0;
        this.fEZ = 0;
    }
}
