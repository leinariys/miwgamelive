package logic.data.mbean;

import game.script.item.ItemType;
import game.script.npcchat.requirement.HasRoomForItemSpeechAction;
import logic.baa.C1616Xf;
import logic.res.code.C5663aRz;
import p001a.C0064Am;

/* renamed from: a.aMp  reason: case insensitive filesystem */
public class C5523aMp extends C5416aIm {
    @C0064Am(aul = "4eb2bd4577cd28c77c377495f6745ba4", aum = -2)
    public long aLG;
    @C0064Am(aul = "4eb2bd4577cd28c77c377495f6745ba4", aum = -1)
    public byte aLL;
    @C0064Am(aul = "754b5c77dab2d36666b44a1a8de2cc6c", aum = -2)
    public long bmV;
    @C0064Am(aul = "754b5c77dab2d36666b44a1a8de2cc6c", aum = -1)
    public byte bmW;
    @C0064Am(aul = "754b5c77dab2d36666b44a1a8de2cc6c", aum = 0)

    /* renamed from: cw */
    public ItemType f3401cw;
    @C0064Am(aul = "4eb2bd4577cd28c77c377495f6745ba4", aum = 1)

    /* renamed from: cy */
    public int f3402cy;

    public C5523aMp() {
    }

    public C5523aMp(C1616Xf xf) {
        super(xf);
    }

    /* renamed from: c */
    public C5663aRz[] mo309c() {
        return HasRoomForItemSpeechAction._m_fields;
    }

    /* renamed from: d */
    public void mo310d() {
        super.mo310d();
        this.f3401cw = null;
        this.f3402cy = 0;
        this.bmV = 0;
        this.aLG = 0;
        this.bmW = 0;
        this.aLL = 0;
    }
}
