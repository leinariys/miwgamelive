package logic.data.mbean;

import game.script.nls.NLSPdaKeyManager;
import logic.baa.C1616Xf;
import logic.res.code.C5663aRz;
import p001a.C0064Am;
import taikodom.infra.script.I18NString;

/* renamed from: a.vh */
public class C3844vh extends C2484fi {
    @C0064Am(aul = "2338738af4087b698478739406788fa0", aum = 0)
    public I18NString byT;
    @C0064Am(aul = "2338738af4087b698478739406788fa0", aum = -2)
    public long byU;
    @C0064Am(aul = "2338738af4087b698478739406788fa0", aum = -1)
    public byte byV;

    public C3844vh() {
    }

    public C3844vh(C1616Xf xf) {
        super(xf);
    }

    /* renamed from: c */
    public C5663aRz[] mo309c() {
        return NLSPdaKeyManager._m_fields;
    }

    /* renamed from: d */
    public void mo310d() {
        super.mo310d();
        this.byT = null;
        this.byU = 0;
        this.byV = 0;
    }
}
