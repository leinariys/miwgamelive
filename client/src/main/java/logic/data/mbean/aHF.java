package logic.data.mbean;

import game.script.avatar.TextureGridType;
import game.script.resource.Asset;
import logic.baa.C1616Xf;
import logic.res.code.C5663aRz;
import p001a.C0064Am;

/* renamed from: a.aHF */
public class aHF extends C3502rf {
    @C0064Am(aul = "209d3b239352351a88bbff5a430c292f", aum = 0)
    public Asset cWv;
    @C0064Am(aul = "ecbdeaa5d5eb27a7a244be4c83031a7a", aum = 1)
    public int cellHeight;
    @C0064Am(aul = "a8346aa54e7fd09e546f42dfd6f359ff", aum = 2)
    public int cellWidth;
    @C0064Am(aul = "209d3b239352351a88bbff5a430c292f", aum = -2)
    public long hYC;
    @C0064Am(aul = "ecbdeaa5d5eb27a7a244be4c83031a7a", aum = -2)
    public long hYD;
    @C0064Am(aul = "a8346aa54e7fd09e546f42dfd6f359ff", aum = -2)
    public long hYE;
    @C0064Am(aul = "209d3b239352351a88bbff5a430c292f", aum = -1)
    public byte hYF;
    @C0064Am(aul = "ecbdeaa5d5eb27a7a244be4c83031a7a", aum = -1)
    public byte hYG;
    @C0064Am(aul = "a8346aa54e7fd09e546f42dfd6f359ff", aum = -1)
    public byte hYH;

    public aHF() {
    }

    public aHF(C1616Xf xf) {
        super(xf);
    }

    /* renamed from: c */
    public C5663aRz[] mo309c() {
        return TextureGridType._m_fields;
    }

    /* renamed from: d */
    public void mo310d() {
        super.mo310d();
        this.cWv = null;
        this.cellHeight = 0;
        this.cellWidth = 0;
        this.hYC = 0;
        this.hYD = 0;
        this.hYE = 0;
        this.hYF = 0;
        this.hYG = 0;
        this.hYH = 0;
    }
}
