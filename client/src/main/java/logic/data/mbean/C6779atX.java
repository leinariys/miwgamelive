package logic.data.mbean;

import game.script.space.AsteroidLoot;
import game.script.space.AsteroidType;
import logic.baa.C1616Xf;
import logic.res.code.C5663aRz;
import p001a.C0064Am;

/* renamed from: a.atX  reason: case insensitive filesystem */
public class C6779atX extends C6379aln {
    @C0064Am(aul = "27d1d3b99b9c8940f4aea56b35383b89", aum = 0)
    public AsteroidType auH;
    @C0064Am(aul = "27d1d3b99b9c8940f4aea56b35383b89", aum = -2)
    public long gCC;
    @C0064Am(aul = "27d1d3b99b9c8940f4aea56b35383b89", aum = -1)
    public byte gCD;

    public C6779atX() {
    }

    public C6779atX(C1616Xf xf) {
        super(xf);
    }

    /* renamed from: c */
    public C5663aRz[] mo309c() {
        return AsteroidLoot._m_fields;
    }

    /* renamed from: d */
    public void mo310d() {
        super.mo310d();
        this.auH = null;
        this.gCC = 0;
        this.gCD = 0;
    }
}
