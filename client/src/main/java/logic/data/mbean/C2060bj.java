package logic.data.mbean;

import game.geometry.Vec3d;
import game.script.main.bot.BotScript;
import game.script.main.bot.MissionABC;
import game.script.npc.NPCType;
import game.script.space.StellarSystem;
import logic.baa.C1616Xf;
import logic.res.code.C5663aRz;
import p001a.C0064Am;

/* renamed from: a.bj */
public class C2060bj extends C0505HA {
    @C0064Am(aul = "f1efdf24e4438e39b8c7451ecce1f554", aum = 1)

    /* renamed from: nA */
    public Vec3d f5880nA;
    @C0064Am(aul = "5868fe2c3a1da17d0380dfba46b408e3", aum = 2)

    /* renamed from: nB */
    public StellarSystem f5881nB;
    @C0064Am(aul = "7325bffbd5e0945faa894b178c8e8d06", aum = 3)

    /* renamed from: nC */
    public NPCType f5882nC;
    @C0064Am(aul = "a0127fc136703002284131966bc8e8fd", aum = -2)

    /* renamed from: nD */
    public long f5883nD;
    @C0064Am(aul = "f1efdf24e4438e39b8c7451ecce1f554", aum = -2)

    /* renamed from: nE */
    public long f5884nE;
    @C0064Am(aul = "5868fe2c3a1da17d0380dfba46b408e3", aum = -2)

    /* renamed from: nF */
    public long f5885nF;
    @C0064Am(aul = "7325bffbd5e0945faa894b178c8e8d06", aum = -2)

    /* renamed from: nG */
    public long f5886nG;
    @C0064Am(aul = "a0127fc136703002284131966bc8e8fd", aum = -1)

    /* renamed from: nH */
    public byte f5887nH;
    @C0064Am(aul = "f1efdf24e4438e39b8c7451ecce1f554", aum = -1)

    /* renamed from: nI */
    public byte f5888nI;
    @C0064Am(aul = "5868fe2c3a1da17d0380dfba46b408e3", aum = -1)

    /* renamed from: nJ */
    public byte f5889nJ;
    @C0064Am(aul = "7325bffbd5e0945faa894b178c8e8d06", aum = -1)

    /* renamed from: nK */
    public byte f5890nK;
    @C0064Am(aul = "a0127fc136703002284131966bc8e8fd", aum = 0)

    /* renamed from: nz */
    public BotScript f5891nz;

    public C2060bj() {
    }

    public C2060bj(C1616Xf xf) {
        super(xf);
    }

    /* renamed from: c */
    public C5663aRz[] mo309c() {
        return MissionABC._m_fields;
    }

    /* renamed from: d */
    public void mo310d() {
        super.mo310d();
        this.f5891nz = null;
        this.f5880nA = null;
        this.f5881nB = null;
        this.f5882nC = null;
        this.f5883nD = 0;
        this.f5884nE = 0;
        this.f5885nF = 0;
        this.f5886nG = 0;
        this.f5887nH = 0;
        this.f5888nI = 0;
        this.f5889nJ = 0;
        this.f5890nK = 0;
    }
}
