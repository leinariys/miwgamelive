package logic.data.mbean;

import game.script.mission.scripting.LootItemMissionScript;
import logic.baa.C1616Xf;
import logic.res.code.C5663aRz;
import p001a.C0064Am;

/* renamed from: a.Dy */
public class C0305Dy extends C2955mD {
    @C0064Am(aul = "239062c42c5a89ab244833f948d68f20", aum = 0)
    public LootItemMissionScript awH;
    @C0064Am(aul = "239062c42c5a89ab244833f948d68f20", aum = -2)

    /* renamed from: dl */
    public long f459dl;
    @C0064Am(aul = "239062c42c5a89ab244833f948d68f20", aum = -1)

    /* renamed from: ds */
    public byte f460ds;

    public C0305Dy() {
    }

    public C0305Dy(C1616Xf xf) {
        super(xf);
    }

    /* renamed from: c */
    public C5663aRz[] mo309c() {
        return LootItemMissionScript.StateE._m_fields;
    }

    /* renamed from: d */
    public void mo310d() {
        super.mo310d();
        this.awH = null;
        this.f459dl = 0;
        this.f460ds = 0;
    }
}
