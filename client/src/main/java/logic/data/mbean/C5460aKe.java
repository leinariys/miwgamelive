package logic.data.mbean;

import game.network.message.serializable.C2686iZ;
import game.script.itembilling.ItemBilling;
import game.script.itembilling.ItemBillingOrder;
import logic.baa.C1616Xf;
import logic.res.code.C5663aRz;
import p001a.C0064Am;

import java.util.UUID;

/* renamed from: a.aKe  reason: case insensitive filesystem */
public class C5460aKe extends C3805vD {
    @C0064Am(aul = "b73655b5afb673913d708b445d23e247", aum = 1)

    /* renamed from: bK */
    public UUID f3251bK;
    @C0064Am(aul = "229b86048beda61b6d33048fdd15d13b", aum = -2)
    public long eZO;
    @C0064Am(aul = "229b86048beda61b6d33048fdd15d13b", aum = -1)
    public byte eZR;
    @C0064Am(aul = "229b86048beda61b6d33048fdd15d13b", aum = 0)

    /* renamed from: gy */
    public C2686iZ<ItemBillingOrder> f3252gy;
    @C0064Am(aul = "ac9467a31ab485c42fa47f0eb6d978cb", aum = -2)
    public long ihQ;
    @C0064Am(aul = "ac9467a31ab485c42fa47f0eb6d978cb", aum = -1)
    public byte ihR;
    @C0064Am(aul = "b73655b5afb673913d708b445d23e247", aum = -2)

    /* renamed from: oL */
    public long f3253oL;
    @C0064Am(aul = "b73655b5afb673913d708b445d23e247", aum = -1)

    /* renamed from: oS */
    public byte f3254oS;
    @C0064Am(aul = "ac9467a31ab485c42fa47f0eb6d978cb", aum = 2)
    public String url;

    public C5460aKe() {
    }

    public C5460aKe(C1616Xf xf) {
        super(xf);
    }

    /* renamed from: c */
    public C5663aRz[] mo309c() {
        return ItemBilling._m_fields;
    }

    /* renamed from: d */
    public void mo310d() {
        super.mo310d();
        this.f3252gy = null;
        this.f3251bK = null;
        this.url = null;
        this.eZO = 0;
        this.f3253oL = 0;
        this.ihQ = 0;
        this.eZR = 0;
        this.f3254oS = 0;
        this.ihR = 0;
    }
}
