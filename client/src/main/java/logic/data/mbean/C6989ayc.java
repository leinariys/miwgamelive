package logic.data.mbean;

import game.geometry.Vec3d;
import game.network.message.serializable.C1556Wo;
import game.network.message.serializable.C1684Yo;
import game.script.ai.npc.ScriptableAIController;
import logic.abb.C0573Hw;
import logic.abb.C3254pg;
import logic.baa.C1616Xf;
import logic.res.code.C5663aRz;
import p001a.C0064Am;

import java.util.LinkedList;

/* renamed from: a.ayc  reason: case insensitive filesystem */
public class C6989ayc extends C5574aOo {
    @C0064Am(aul = "b2b621b437db55b16b91ced056544c95", aum = 0)

    /* renamed from: UA */
    public String f5626UA;
    @C0064Am(aul = "b2b621b437db55b16b91ced056544c95", aum = -2)

    /* renamed from: UB */
    public long f5627UB;
    @C0064Am(aul = "b2b621b437db55b16b91ced056544c95", aum = -1)

    /* renamed from: UC */
    public byte f5628UC;
    @C0064Am(aul = "ab16ed9119b100700214b59b9fb4acc0", aum = -2)
    public long aPL;
    @C0064Am(aul = "25523864bdbec0d1782d0f726cc85682", aum = -2)
    public long aPM;
    @C0064Am(aul = "ab16ed9119b100700214b59b9fb4acc0", aum = -1)
    public byte aPN;
    @C0064Am(aul = "25523864bdbec0d1782d0f726cc85682", aum = -1)
    public byte aPO;
    @C0064Am(aul = "ab16ed9119b100700214b59b9fb4acc0", aum = 5)
    public Vec3d center;
    @C0064Am(aul = "2f121a786570d97e2a7a0daf01ad250b", aum = 1)
    public C1556Wo<C1684Yo, LinkedList<C0573Hw>> dRh;
    @C0064Am(aul = "af88a92a65b6a614daa5d4ba84fb37e2", aum = 2)
    public C1556Wo<C1684Yo, Float> dRj;
    @C0064Am(aul = "0fc0d6a9816cd0f018bd02ebb76a32ba", aum = 3)
    public C1556Wo<C1684Yo, C3254pg> dRl;
    @C0064Am(aul = "a99a66ad0b2c3880d95bfeeae1e64f91", aum = 4)
    public C1556Wo<C1684Yo, LinkedList<String>> dRn;
    @C0064Am(aul = "a99a66ad0b2c3880d95bfeeae1e64f91", aum = -2)
    public long dcl;
    @C0064Am(aul = "a99a66ad0b2c3880d95bfeeae1e64f91", aum = -1)
    public byte dcv;
    @C0064Am(aul = "2f121a786570d97e2a7a0daf01ad250b", aum = -2)
    public long gRO;
    @C0064Am(aul = "af88a92a65b6a614daa5d4ba84fb37e2", aum = -2)
    public long gRP;
    @C0064Am(aul = "0fc0d6a9816cd0f018bd02ebb76a32ba", aum = -2)
    public long gRQ;
    @C0064Am(aul = "2f121a786570d97e2a7a0daf01ad250b", aum = -1)
    public byte gRR;
    @C0064Am(aul = "af88a92a65b6a614daa5d4ba84fb37e2", aum = -1)
    public byte gRS;
    @C0064Am(aul = "0fc0d6a9816cd0f018bd02ebb76a32ba", aum = -1)
    public byte gRT;
    @C0064Am(aul = "25523864bdbec0d1782d0f726cc85682", aum = 6)
    public float radius2;

    public C6989ayc() {
    }

    public C6989ayc(C1616Xf xf) {
        super(xf);
    }

    /* renamed from: c */
    public C5663aRz[] mo309c() {
        return ScriptableAIController._m_fields;
    }

    /* renamed from: d */
    public void mo310d() {
        super.mo310d();
        this.f5626UA = null;
        this.dRh = null;
        this.dRj = null;
        this.dRl = null;
        this.dRn = null;
        this.center = null;
        this.radius2 = 0.0f;
        this.f5627UB = 0;
        this.gRO = 0;
        this.gRP = 0;
        this.gRQ = 0;
        this.dcl = 0;
        this.aPL = 0;
        this.aPM = 0;
        this.f5628UC = 0;
        this.gRR = 0;
        this.gRS = 0;
        this.gRT = 0;
        this.dcv = 0;
        this.aPN = 0;
        this.aPO = 0;
    }
}
