package logic.data.mbean;

import game.script.avatar.ColorWrapper;
import game.script.avatar.TextureGrid;
import game.script.resource.Asset;
import logic.baa.C1616Xf;
import logic.res.code.C5663aRz;
import p001a.C0064Am;
import taikodom.infra.script.I18NString;

import java.util.UUID;

/* renamed from: a.aRY */
public class aRY extends C3805vD {
    @C0064Am(aul = "ba558fa005378334dbe5cda8f2c1d8ee", aum = -2)

    /* renamed from: Nf */
    public long f3703Nf;
    @C0064Am(aul = "ba558fa005378334dbe5cda8f2c1d8ee", aum = -1)

    /* renamed from: Nh */
    public byte f3704Nh;
    @C0064Am(aul = "a34424902e908c90f78c98327fd15440", aum = 6)

    /* renamed from: bK */
    public UUID f3705bK;
    @C0064Am(aul = "365a3d37aad89cc1aaf58813e64f96f7", aum = 0)
    public Asset cWv;
    @C0064Am(aul = "aff7431de523ff98115938296898c946", aum = 1)
    public int cellHeight;
    @C0064Am(aul = "7a98d9d9280225ad151020d999d21855", aum = 2)
    public int cellWidth;
    @C0064Am(aul = "8892f5cc5d4e08c1c3db89f777ad1921", aum = -2)
    public long djC;
    @C0064Am(aul = "8892f5cc5d4e08c1c3db89f777ad1921", aum = -1)
    public byte djF;
    @C0064Am(aul = "8892f5cc5d4e08c1c3db89f777ad1921", aum = 4)
    public ColorWrapper djz;
    @C0064Am(aul = "41c55c9af3001e4b5a2ad561e37a1318", aum = 3)
    public int eqc;
    @C0064Am(aul = "365a3d37aad89cc1aaf58813e64f96f7", aum = -2)
    public long hYC;
    @C0064Am(aul = "aff7431de523ff98115938296898c946", aum = -2)
    public long hYD;
    @C0064Am(aul = "7a98d9d9280225ad151020d999d21855", aum = -2)
    public long hYE;
    @C0064Am(aul = "365a3d37aad89cc1aaf58813e64f96f7", aum = -1)
    public byte hYF;
    @C0064Am(aul = "aff7431de523ff98115938296898c946", aum = -1)
    public byte hYG;
    @C0064Am(aul = "7a98d9d9280225ad151020d999d21855", aum = -1)
    public byte hYH;
    @C0064Am(aul = "93fdfa23c59f03d64616271fdb0b1501", aum = 7)
    public String handle;
    @C0064Am(aul = "41c55c9af3001e4b5a2ad561e37a1318", aum = -2)
    public long iLP;
    @C0064Am(aul = "41c55c9af3001e4b5a2ad561e37a1318", aum = -1)
    public byte iLQ;
    @C0064Am(aul = "a34424902e908c90f78c98327fd15440", aum = -2)

    /* renamed from: oL */
    public long f3706oL;
    @C0064Am(aul = "a34424902e908c90f78c98327fd15440", aum = -1)

    /* renamed from: oS */
    public byte f3707oS;
    @C0064Am(aul = "93fdfa23c59f03d64616271fdb0b1501", aum = -2)

    /* renamed from: ok */
    public long f3708ok;
    @C0064Am(aul = "93fdfa23c59f03d64616271fdb0b1501", aum = -1)

    /* renamed from: on */
    public byte f3709on;
    @C0064Am(aul = "ba558fa005378334dbe5cda8f2c1d8ee", aum = 5)

    /* renamed from: zP */
    public I18NString f3710zP;

    public aRY() {
    }

    public aRY(C1616Xf xf) {
        super(xf);
    }

    /* renamed from: c */
    public C5663aRz[] mo309c() {
        return TextureGrid._m_fields;
    }

    /* renamed from: d */
    public void mo310d() {
        super.mo310d();
        this.cWv = null;
        this.cellHeight = 0;
        this.cellWidth = 0;
        this.eqc = 0;
        this.djz = null;
        this.f3710zP = null;
        this.f3705bK = null;
        this.handle = null;
        this.hYC = 0;
        this.hYD = 0;
        this.hYE = 0;
        this.iLP = 0;
        this.djC = 0;
        this.f3703Nf = 0;
        this.f3706oL = 0;
        this.f3708ok = 0;
        this.hYF = 0;
        this.hYG = 0;
        this.hYH = 0;
        this.iLQ = 0;
        this.djF = 0;
        this.f3704Nh = 0;
        this.f3707oS = 0;
        this.f3709on = 0;
    }
}
