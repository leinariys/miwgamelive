package logic.data.mbean;

import game.script.npcchat.PlayerSpeech;
import game.script.npcchat.SpeechAction;
import game.script.resource.Asset;
import logic.baa.C1616Xf;
import logic.res.code.C5663aRz;
import p001a.C0064Am;

import java.util.UUID;

/* renamed from: a.alM  reason: case insensitive filesystem */
public class C6352alM extends C3805vD {
    @C0064Am(aul = "39bae67318b450bacf7a3a5d769baa9d", aum = -2)
    public long aTT;
    @C0064Am(aul = "39bae67318b450bacf7a3a5d769baa9d", aum = -1)
    public byte aTW;
    @C0064Am(aul = "2c1d7f5cecc09ddfe76f6e9926f6407a", aum = 2)

    /* renamed from: bK */
    public UUID f4841bK;
    @C0064Am(aul = "39bae67318b450bacf7a3a5d769baa9d", aum = 0)
    public PlayerSpeech bYS;
    @C0064Am(aul = "20bf6028f90e8c4ece14e613f2e5f5e9", aum = 1)
    public Asset bfO;
    @C0064Am(aul = "20bf6028f90e8c4ece14e613f2e5f5e9", aum = -2)
    public long eaX;
    @C0064Am(aul = "20bf6028f90e8c4ece14e613f2e5f5e9", aum = -1)
    public byte eba;
    @C0064Am(aul = "1abc404b12cf1d04265ea7f0dd05015e", aum = 3)
    public String handle;
    @C0064Am(aul = "2c1d7f5cecc09ddfe76f6e9926f6407a", aum = -2)

    /* renamed from: oL */
    public long f4842oL;
    @C0064Am(aul = "2c1d7f5cecc09ddfe76f6e9926f6407a", aum = -1)

    /* renamed from: oS */
    public byte f4843oS;
    @C0064Am(aul = "1abc404b12cf1d04265ea7f0dd05015e", aum = -2)

    /* renamed from: ok */
    public long f4844ok;
    @C0064Am(aul = "1abc404b12cf1d04265ea7f0dd05015e", aum = -1)

    /* renamed from: on */
    public byte f4845on;

    public C6352alM() {
    }

    public C6352alM(C1616Xf xf) {
        super(xf);
    }

    /* renamed from: c */
    public C5663aRz[] mo309c() {
        return SpeechAction._m_fields;
    }

    /* renamed from: d */
    public void mo310d() {
        super.mo310d();
        this.bYS = null;
        this.bfO = null;
        this.f4841bK = null;
        this.handle = null;
        this.aTT = 0;
        this.eaX = 0;
        this.f4842oL = 0;
        this.f4844ok = 0;
        this.aTW = 0;
        this.eba = 0;
        this.f4843oS = 0;
        this.f4845on = 0;
    }
}
