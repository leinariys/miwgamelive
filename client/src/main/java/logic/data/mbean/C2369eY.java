package logic.data.mbean;

import game.script.player.LDParameter;
import logic.baa.C1616Xf;
import logic.res.code.C5663aRz;
import p001a.C0064Am;

/* renamed from: a.eY */
public class C2369eY extends C5292aDs {
    @C0064Am(aul = "db222ad0fe751a8b53ad587db204d462", aum = -2)

    /* renamed from: IC */
    public long f6993IC;
    @C0064Am(aul = "254f6cbedcb5c489658116d979e2f86c", aum = -2)

    /* renamed from: ID */
    public long f6994ID;
    @C0064Am(aul = "db222ad0fe751a8b53ad587db204d462", aum = -1)

    /* renamed from: IE */
    public byte f6995IE;
    @C0064Am(aul = "254f6cbedcb5c489658116d979e2f86c", aum = -1)

    /* renamed from: IG */
    public byte f6996IG;
    @C0064Am(aul = "db222ad0fe751a8b53ad587db204d462", aum = 0)
    public String key;
    @C0064Am(aul = "254f6cbedcb5c489658116d979e2f86c", aum = 1)
    public String value;

    public C2369eY() {
    }

    public C2369eY(C1616Xf xf) {
        super(xf);
    }

    /* renamed from: c */
    public C5663aRz[] mo309c() {
        return LDParameter._m_fields;
    }

    /* renamed from: d */
    public void mo310d() {
        super.mo310d();
        this.key = null;
        this.value = null;
        this.f6993IC = 0;
        this.f6994ID = 0;
        this.f6995IE = 0;
        this.f6996IG = 0;
    }
}
