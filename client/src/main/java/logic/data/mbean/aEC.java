package logic.data.mbean;

import game.network.message.serializable.C1556Wo;
import game.network.message.serializable.C2686iZ;
import game.network.message.serializable.C3438ra;
import game.script.avatar.AvatarSector;
import game.script.avatar.ColorWrapper;
import game.script.avatar.TextureGrid;
import game.script.avatar.body.AvatarAppearance;
import game.script.avatar.body.BodyPiece;
import game.script.avatar.body.TextureScheme;
import game.script.resource.Asset;
import logic.baa.C1616Xf;
import logic.res.code.C5663aRz;
import p001a.C0064Am;
import taikodom.infra.script.I18NString;

/* renamed from: a.aEC */
public class aEC extends C3805vD {
    @C0064Am(aul = "ad79dd00a1c5e5dde271875f9da3f124", aum = -2)

    /* renamed from: Ng */
    public long f2609Ng;
    @C0064Am(aul = "ad79dd00a1c5e5dde271875f9da3f124", aum = -1)

    /* renamed from: Ni */
    public byte f2610Ni;
    @C0064Am(aul = "08164fd57362d155c5abf19ca9b552c4", aum = 9)

    /* renamed from: eB */
    public C3438ra<BodyPiece> f2611eB;
    @C0064Am(aul = "999c9b5e77b0dae3804413895a6232a7", aum = 10)

    /* renamed from: eD */
    public C3438ra<BodyPiece> f2612eD;
    @C0064Am(aul = "0c9aaf3659c04c691601bc30f4e6f789", aum = 11)

    /* renamed from: eF */
    public C3438ra<BodyPiece> f2613eF;
    @C0064Am(aul = "0509677607734841d2fca382aeb73b53", aum = 12)

    /* renamed from: eH */
    public C3438ra<BodyPiece> f2614eH;
    @C0064Am(aul = "59eeb1ac18428c16c7ef8f3316917602", aum = 13)

    /* renamed from: eJ */
    public C3438ra<BodyPiece> f2615eJ;
    @C0064Am(aul = "6937b0dbbdba07eade40b24ebbe2e489", aum = 14)

    /* renamed from: eL */
    public C2686iZ<BodyPiece> f2616eL;
    @C0064Am(aul = "fe8f63d1af21a977e18666722479392a", aum = 15)

    /* renamed from: eN */
    public C3438ra<TextureScheme> f2617eN;
    @C0064Am(aul = "3bd72ee71e301be45f75f48088dcf6f1", aum = 16)

    /* renamed from: eP */
    public C3438ra<TextureGrid> f2618eP;
    @C0064Am(aul = "74618cef49e43b25d63a795fb0462bec", aum = 17)

    /* renamed from: eR */
    public C3438ra<ColorWrapper> f2619eR;
    @C0064Am(aul = "2009a468b2cc5c86345cb08185592b9a", aum = 18)

    /* renamed from: eT */
    public C3438ra<Asset> f2620eT;
    @C0064Am(aul = "821c51428fb194975e54abd394035bf6", aum = 19)

    /* renamed from: eV */
    public float f2621eV;
    @C0064Am(aul = "e905d30c994b016479630107354a26c2", aum = 20)

    /* renamed from: eX */
    public Asset f2622eX;
    @C0064Am(aul = "49f3fd0276f618c949d7340aba06aa72", aum = 0)

    /* renamed from: el */
    public I18NString f2623el;
    @C0064Am(aul = "17ff3dc597145d9eea000462502bcc03", aum = 2)

    /* renamed from: eo */
    public boolean f2624eo;
    @C0064Am(aul = "a603ed45210b02818d3ef158e868d939", aum = -2)
    public long eob;
    @C0064Am(aul = "a603ed45210b02818d3ef158e868d939", aum = -1)
    public byte eoi;
    @C0064Am(aul = "f5779d598c29e6a46c8ea093cbb3bf52", aum = 4)

    /* renamed from: er */
    public Asset f2625er;
    @C0064Am(aul = "5d5565d4493c2c4052711a954bcf5cf2", aum = 5)

    /* renamed from: et */
    public Asset f2626et;
    @C0064Am(aul = "f35e8ceb36e98cef9048f53a021726bf", aum = 6)

    /* renamed from: ev */
    public Asset f2627ev;
    @C0064Am(aul = "f4029875bdc047ddd89b9b4eff2912cc", aum = 7)

    /* renamed from: ex */
    public C3438ra<BodyPiece> f2628ex;
    @C0064Am(aul = "67182cdac66f84345d906a5e4e2a29b0", aum = 8)

    /* renamed from: ez */
    public C3438ra<BodyPiece> f2629ez;
    @C0064Am(aul = "ae2e07d27bfb3c3e08603aa0163a374d", aum = 21)
    public C1556Wo<AvatarSector, BodyPiece> gjS;
    @C0064Am(aul = "76ebdf713a51856ff3e1d5c9b06acb6b", aum = 22)
    public TextureScheme gjU;
    @C0064Am(aul = "a527ef2b669812ad99cb7eb5bc879169", aum = 23)
    public TextureGrid gjW;
    @C0064Am(aul = "52805eb1529344e1bfdc651ea3a88889", aum = 24)
    public ColorWrapper gjY;
    @C0064Am(aul = "ab04d990ac1f5d0746b417cfb640ac91", aum = 25)
    public Asset gka;
    @C0064Am(aul = "44a55af1d7f9783dad5151b005b2a503", aum = 26)
    public TextureGrid gkc;
    @C0064Am(aul = "f81d74b9cffc734ae543afc3c32df71d", aum = 27)
    public TextureGrid gke;
    @C0064Am(aul = "864089646154f93c1344e53941f10ca2", aum = 28)
    public TextureGrid gkg;
    @C0064Am(aul = "433d08afe51f11771cf7d66637820b31", aum = 29)
    public float gki;
    @C0064Am(aul = "bb85ed2cc22e3b46c936c5c106849244", aum = 30)
    public float gkk;
    @C0064Am(aul = "f7f06b2896868c7be000076ee51f0efb", aum = 31)
    public float gkm;
    @C0064Am(aul = "864089646154f93c1344e53941f10ca2", aum = -2)
    public long hGA;
    @C0064Am(aul = "433d08afe51f11771cf7d66637820b31", aum = -2)
    public long hGB;
    @C0064Am(aul = "bb85ed2cc22e3b46c936c5c106849244", aum = -2)
    public long hGC;
    @C0064Am(aul = "f7f06b2896868c7be000076ee51f0efb", aum = -2)
    public long hGD;
    @C0064Am(aul = "49f3fd0276f618c949d7340aba06aa72", aum = -1)
    public byte hGE;
    @C0064Am(aul = "17ff3dc597145d9eea000462502bcc03", aum = -1)
    public byte hGF;
    @C0064Am(aul = "f5779d598c29e6a46c8ea093cbb3bf52", aum = -1)
    public byte hGG;
    @C0064Am(aul = "5d5565d4493c2c4052711a954bcf5cf2", aum = -1)
    public byte hGH;
    @C0064Am(aul = "f35e8ceb36e98cef9048f53a021726bf", aum = -1)
    public byte hGI;
    @C0064Am(aul = "f4029875bdc047ddd89b9b4eff2912cc", aum = -1)
    public byte hGJ;
    @C0064Am(aul = "67182cdac66f84345d906a5e4e2a29b0", aum = -1)
    public byte hGK;
    @C0064Am(aul = "08164fd57362d155c5abf19ca9b552c4", aum = -1)
    public byte hGL;
    @C0064Am(aul = "999c9b5e77b0dae3804413895a6232a7", aum = -1)
    public byte hGM;
    @C0064Am(aul = "0c9aaf3659c04c691601bc30f4e6f789", aum = -1)
    public byte hGN;
    @C0064Am(aul = "0509677607734841d2fca382aeb73b53", aum = -1)
    public byte hGO;
    @C0064Am(aul = "59eeb1ac18428c16c7ef8f3316917602", aum = -1)
    public byte hGP;
    @C0064Am(aul = "6937b0dbbdba07eade40b24ebbe2e489", aum = -1)
    public byte hGQ;
    @C0064Am(aul = "fe8f63d1af21a977e18666722479392a", aum = -1)
    public byte hGR;
    @C0064Am(aul = "3bd72ee71e301be45f75f48088dcf6f1", aum = -1)
    public byte hGS;
    @C0064Am(aul = "74618cef49e43b25d63a795fb0462bec", aum = -1)
    public byte hGT;
    @C0064Am(aul = "2009a468b2cc5c86345cb08185592b9a", aum = -1)
    public byte hGU;
    @C0064Am(aul = "821c51428fb194975e54abd394035bf6", aum = -1)
    public byte hGV;
    @C0064Am(aul = "e905d30c994b016479630107354a26c2", aum = -1)
    public byte hGW;
    @C0064Am(aul = "ae2e07d27bfb3c3e08603aa0163a374d", aum = -1)
    public byte hGX;
    @C0064Am(aul = "76ebdf713a51856ff3e1d5c9b06acb6b", aum = -1)
    public byte hGY;
    @C0064Am(aul = "a527ef2b669812ad99cb7eb5bc879169", aum = -1)
    public byte hGZ;
    @C0064Am(aul = "49f3fd0276f618c949d7340aba06aa72", aum = -2)
    public long hGa;
    @C0064Am(aul = "17ff3dc597145d9eea000462502bcc03", aum = -2)
    public long hGb;
    @C0064Am(aul = "f5779d598c29e6a46c8ea093cbb3bf52", aum = -2)
    public long hGc;
    @C0064Am(aul = "5d5565d4493c2c4052711a954bcf5cf2", aum = -2)
    public long hGd;
    @C0064Am(aul = "f35e8ceb36e98cef9048f53a021726bf", aum = -2)
    public long hGe;
    @C0064Am(aul = "f4029875bdc047ddd89b9b4eff2912cc", aum = -2)
    public long hGf;
    @C0064Am(aul = "67182cdac66f84345d906a5e4e2a29b0", aum = -2)
    public long hGg;
    @C0064Am(aul = "08164fd57362d155c5abf19ca9b552c4", aum = -2)
    public long hGh;
    @C0064Am(aul = "999c9b5e77b0dae3804413895a6232a7", aum = -2)
    public long hGi;
    @C0064Am(aul = "0c9aaf3659c04c691601bc30f4e6f789", aum = -2)
    public long hGj;
    @C0064Am(aul = "0509677607734841d2fca382aeb73b53", aum = -2)
    public long hGk;
    @C0064Am(aul = "59eeb1ac18428c16c7ef8f3316917602", aum = -2)
    public long hGl;
    @C0064Am(aul = "6937b0dbbdba07eade40b24ebbe2e489", aum = -2)
    public long hGm;
    @C0064Am(aul = "fe8f63d1af21a977e18666722479392a", aum = -2)
    public long hGn;
    @C0064Am(aul = "3bd72ee71e301be45f75f48088dcf6f1", aum = -2)
    public long hGo;
    @C0064Am(aul = "74618cef49e43b25d63a795fb0462bec", aum = -2)
    public long hGp;
    @C0064Am(aul = "2009a468b2cc5c86345cb08185592b9a", aum = -2)
    public long hGq;
    @C0064Am(aul = "821c51428fb194975e54abd394035bf6", aum = -2)
    public long hGr;
    @C0064Am(aul = "e905d30c994b016479630107354a26c2", aum = -2)
    public long hGs;
    @C0064Am(aul = "ae2e07d27bfb3c3e08603aa0163a374d", aum = -2)
    public long hGt;
    @C0064Am(aul = "76ebdf713a51856ff3e1d5c9b06acb6b", aum = -2)
    public long hGu;
    @C0064Am(aul = "a527ef2b669812ad99cb7eb5bc879169", aum = -2)
    public long hGv;
    @C0064Am(aul = "52805eb1529344e1bfdc651ea3a88889", aum = -2)
    public long hGw;
    @C0064Am(aul = "ab04d990ac1f5d0746b417cfb640ac91", aum = -2)
    public long hGx;
    @C0064Am(aul = "44a55af1d7f9783dad5151b005b2a503", aum = -2)
    public long hGy;
    @C0064Am(aul = "f81d74b9cffc734ae543afc3c32df71d", aum = -2)
    public long hGz;
    @C0064Am(aul = "52805eb1529344e1bfdc651ea3a88889", aum = -1)
    public byte hHa;
    @C0064Am(aul = "ab04d990ac1f5d0746b417cfb640ac91", aum = -1)
    public byte hHb;
    @C0064Am(aul = "44a55af1d7f9783dad5151b005b2a503", aum = -1)
    public byte hHc;
    @C0064Am(aul = "f81d74b9cffc734ae543afc3c32df71d", aum = -1)
    public byte hHd;
    @C0064Am(aul = "864089646154f93c1344e53941f10ca2", aum = -1)
    public byte hHe;
    @C0064Am(aul = "433d08afe51f11771cf7d66637820b31", aum = -1)
    public byte hHf;
    @C0064Am(aul = "bb85ed2cc22e3b46c936c5c106849244", aum = -1)
    public byte hHg;
    @C0064Am(aul = "f7f06b2896868c7be000076ee51f0efb", aum = -1)
    public byte hHh;
    @C0064Am(aul = "a603ed45210b02818d3ef158e868d939", aum = 3)
    public String prefix;
    @C0064Am(aul = "ad79dd00a1c5e5dde271875f9da3f124", aum = 1)
    public boolean restricted;

    public aEC() {
    }

    public aEC(C1616Xf xf) {
        super(xf);
    }

    /* renamed from: c */
    public C5663aRz[] mo309c() {
        return AvatarAppearance._m_fields;
    }

    /* renamed from: d */
    public void mo310d() {
        super.mo310d();
        this.f2623el = null;
        this.restricted = false;
        this.f2624eo = false;
        this.prefix = null;
        this.f2625er = null;
        this.f2626et = null;
        this.f2627ev = null;
        this.f2628ex = null;
        this.f2629ez = null;
        this.f2611eB = null;
        this.f2612eD = null;
        this.f2613eF = null;
        this.f2614eH = null;
        this.f2615eJ = null;
        this.f2616eL = null;
        this.f2617eN = null;
        this.f2618eP = null;
        this.f2619eR = null;
        this.f2620eT = null;
        this.f2621eV = 0.0f;
        this.f2622eX = null;
        this.gjS = null;
        this.gjU = null;
        this.gjW = null;
        this.gjY = null;
        this.gka = null;
        this.gkc = null;
        this.gke = null;
        this.gkg = null;
        this.gki = 0.0f;
        this.gkk = 0.0f;
        this.gkm = 0.0f;
        this.hGa = 0;
        this.f2609Ng = 0;
        this.hGb = 0;
        this.eob = 0;
        this.hGc = 0;
        this.hGd = 0;
        this.hGe = 0;
        this.hGf = 0;
        this.hGg = 0;
        this.hGh = 0;
        this.hGi = 0;
        this.hGj = 0;
        this.hGk = 0;
        this.hGl = 0;
        this.hGm = 0;
        this.hGn = 0;
        this.hGo = 0;
        this.hGp = 0;
        this.hGq = 0;
        this.hGr = 0;
        this.hGs = 0;
        this.hGt = 0;
        this.hGu = 0;
        this.hGv = 0;
        this.hGw = 0;
        this.hGx = 0;
        this.hGy = 0;
        this.hGz = 0;
        this.hGA = 0;
        this.hGB = 0;
        this.hGC = 0;
        this.hGD = 0;
        this.hGE = 0;
        this.f2610Ni = 0;
        this.hGF = 0;
        this.eoi = 0;
        this.hGG = 0;
        this.hGH = 0;
        this.hGI = 0;
        this.hGJ = 0;
        this.hGK = 0;
        this.hGL = 0;
        this.hGM = 0;
        this.hGN = 0;
        this.hGO = 0;
        this.hGP = 0;
        this.hGQ = 0;
        this.hGR = 0;
        this.hGS = 0;
        this.hGT = 0;
        this.hGU = 0;
        this.hGV = 0;
        this.hGW = 0;
        this.hGX = 0;
        this.hGY = 0;
        this.hGZ = 0;
        this.hHa = 0;
        this.hHb = 0;
        this.hHc = 0;
        this.hHd = 0;
        this.hHe = 0;
        this.hHf = 0;
        this.hHg = 0;
        this.hHh = 0;
    }
}
