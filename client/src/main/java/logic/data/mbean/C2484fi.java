package logic.data.mbean;

import game.script.nls.NLSType;
import logic.baa.C1616Xf;
import logic.res.code.C5663aRz;
import p001a.C0064Am;

import java.util.UUID;

/* renamed from: a.fi */
public class C2484fi extends C5292aDs {
    @C0064Am(aul = "fa25e7e8f57e864ba25a4f6e0e07bfa4", aum = 0)

    /* renamed from: bK */
    public UUID f7432bK;
    @C0064Am(aul = "9cdeb43b662bbbdc295ac194a1348d00", aum = 1)
    public String handle;
    @C0064Am(aul = "fa25e7e8f57e864ba25a4f6e0e07bfa4", aum = -2)

    /* renamed from: oL */
    public long f7433oL;
    @C0064Am(aul = "fa25e7e8f57e864ba25a4f6e0e07bfa4", aum = -1)

    /* renamed from: oS */
    public byte f7434oS;
    @C0064Am(aul = "9cdeb43b662bbbdc295ac194a1348d00", aum = -2)

    /* renamed from: ok */
    public long f7435ok;
    @C0064Am(aul = "9cdeb43b662bbbdc295ac194a1348d00", aum = -1)

    /* renamed from: on */
    public byte f7436on;

    public C2484fi() {
    }

    public C2484fi(C1616Xf xf) {
        super(xf);
    }

    /* renamed from: c */
    public C5663aRz[] mo309c() {
        return NLSType._m_fields;
    }

    /* renamed from: d */
    public void mo310d() {
        super.mo310d();
        this.f7432bK = null;
        this.handle = null;
        this.f7433oL = 0;
        this.f7435ok = 0;
        this.f7434oS = 0;
        this.f7436on = 0;
    }
}
