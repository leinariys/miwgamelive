package logic.data.mbean;

import game.network.message.serializable.C1556Wo;
import game.network.message.serializable.C3438ra;
import game.script.offload.OffloadNode;
import game.script.simulation.Space;
import logic.baa.C1616Xf;
import logic.bbb.aDR;
import logic.res.code.C5663aRz;
import p001a.C0064Am;

/* renamed from: a.Ga */
public class C0475Ga extends C3805vD {
    @C0064Am(aul = "410a253188f8acb3761a8e3cf7d24e88", aum = -2)
    public long bec;
    @C0064Am(aul = "410a253188f8acb3761a8e3cf7d24e88", aum = -1)
    public byte bei;
    @C0064Am(aul = "6b750be5dcb7440a1debf767396cc8e3", aum = -2)
    public long cXj;
    @C0064Am(aul = "74dea7e0f074826e40a2439f6a35f244", aum = -2)
    public long cXk;
    @C0064Am(aul = "4824759737845798775ea6a570268733", aum = -2)
    public long cXl;
    @C0064Am(aul = "3837a7f4c698bf3685c43c487c2f833e", aum = -2)
    public long cXm;
    @C0064Am(aul = "df512dce9e105f1d157a9e41ac4e1245", aum = -2)
    public long cXn;
    @C0064Am(aul = "6b750be5dcb7440a1debf767396cc8e3", aum = -1)
    public byte cXo;
    @C0064Am(aul = "74dea7e0f074826e40a2439f6a35f244", aum = -1)
    public byte cXp;
    @C0064Am(aul = "4824759737845798775ea6a570268733", aum = -1)
    public byte cXq;
    @C0064Am(aul = "3837a7f4c698bf3685c43c487c2f833e", aum = -1)
    public byte cXr;
    @C0064Am(aul = "df512dce9e105f1d157a9e41ac4e1245", aum = -1)
    public byte cXs;
    @C0064Am(aul = "410a253188f8acb3761a8e3cf7d24e88", aum = 0)

    /* renamed from: uh */
    public aDR f632uh;
    @C0064Am(aul = "6b750be5dcb7440a1debf767396cc8e3", aum = 1)

    /* renamed from: uj */
    public C3438ra<Space> f633uj;
    @C0064Am(aul = "74dea7e0f074826e40a2439f6a35f244", aum = 2)

    /* renamed from: ul */
    public C3438ra<Space> f634ul;
    @C0064Am(aul = "4824759737845798775ea6a570268733", aum = 3)

    /* renamed from: un */
    public long f635un;
    @C0064Am(aul = "3837a7f4c698bf3685c43c487c2f833e", aum = 4)

    /* renamed from: up */
    public C1556Wo<Space, Long> f636up;
    @C0064Am(aul = "df512dce9e105f1d157a9e41ac4e1245", aum = 5)

    /* renamed from: ur */
    public String f637ur;

    public C0475Ga() {
    }

    public C0475Ga(C1616Xf xf) {
        super(xf);
    }

    /* renamed from: c */
    public C5663aRz[] mo309c() {
        return OffloadNode._m_fields;
    }

    /* renamed from: d */
    public void mo310d() {
        super.mo310d();
        this.f632uh = null;
        this.f633uj = null;
        this.f634ul = null;
        this.f635un = 0;
        this.f636up = null;
        this.f637ur = null;
        this.bec = 0;
        this.cXj = 0;
        this.cXk = 0;
        this.cXl = 0;
        this.cXm = 0;
        this.cXn = 0;
        this.bei = 0;
        this.cXo = 0;
        this.cXp = 0;
        this.cXq = 0;
        this.cXr = 0;
        this.cXs = 0;
    }
}
