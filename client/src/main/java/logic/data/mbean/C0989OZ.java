package logic.data.mbean;

import game.script.mission.scripting.ReconMissionScript;
import game.script.ship.Ship;
import logic.baa.C1616Xf;
import logic.res.code.C5663aRz;
import p001a.C0064Am;

/* renamed from: a.OZ */
public class C0989OZ extends C5292aDs {
    @C0064Am(aul = "fb3a4ea3d622e49690e85e250f9b9a5c", aum = 0)

    /* renamed from: QR */
    public Ship f1321QR;
    @C0064Am(aul = "987eb182b42c718180404495fed50e82", aum = 2)

    /* renamed from: QV */
    public float f1322QV;
    @C0064Am(aul = "e37f8e57eb617948677ecba4f7221dd3", aum = 3)
    public ReconMissionScript beD;
    @C0064Am(aul = "c454aa992baf3a14d0cd1e04d7cd1fde", aum = 1)
    public Ship bww;
    @C0064Am(aul = "fb3a4ea3d622e49690e85e250f9b9a5c", aum = -2)
    public long dPm;
    @C0064Am(aul = "c454aa992baf3a14d0cd1e04d7cd1fde", aum = -2)
    public long dPn;
    @C0064Am(aul = "987eb182b42c718180404495fed50e82", aum = -2)
    public long dPo;
    @C0064Am(aul = "fb3a4ea3d622e49690e85e250f9b9a5c", aum = -1)
    public byte dPp;
    @C0064Am(aul = "c454aa992baf3a14d0cd1e04d7cd1fde", aum = -1)
    public byte dPq;
    @C0064Am(aul = "987eb182b42c718180404495fed50e82", aum = -1)
    public byte dPr;
    @C0064Am(aul = "e37f8e57eb617948677ecba4f7221dd3", aum = -2)

    /* renamed from: dl */
    public long f1323dl;
    @C0064Am(aul = "e37f8e57eb617948677ecba4f7221dd3", aum = -1)

    /* renamed from: ds */
    public byte f1324ds;

    public C0989OZ() {
    }

    public C0989OZ(C1616Xf xf) {
        super(xf);
    }

    /* renamed from: c */
    public C5663aRz[] mo309c() {
        return ReconMissionScript.SelectionCallback._m_fields;
    }

    /* renamed from: d */
    public void mo310d() {
        super.mo310d();
        this.f1321QR = null;
        this.bww = null;
        this.f1322QV = 0.0f;
        this.beD = null;
        this.dPm = 0;
        this.dPn = 0;
        this.dPo = 0;
        this.f1323dl = 0;
        this.dPp = 0;
        this.dPq = 0;
        this.dPr = 0;
        this.f1324ds = 0;
    }
}
