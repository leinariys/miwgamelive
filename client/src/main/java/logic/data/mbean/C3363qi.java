package logic.data.mbean;

import game.script.player.Player;
import game.script.ship.Station;
import game.script.storage.Storage;
import game.script.storage.StorageAdapter;
import game.script.util.AdapterLikedList;
import logic.baa.C1616Xf;
import logic.res.code.C5663aRz;
import p001a.C0064Am;

/* renamed from: a.qi */
public class C3363qi extends aAL {
    @C0064Am(aul = "7780a2feeb9343a342f41d0e2109b45b", aum = 2)
    public AdapterLikedList<StorageAdapter> aOw;
    @C0064Am(aul = "25e5c50fcba74a193a4d593a39df7b6e", aum = 1)
    public Player aUB;
    @C0064Am(aul = "504d86b1fd223dd73062e3460e75b7a8", aum = 3)
    public float aUC;
    @C0064Am(aul = "25e5c50fcba74a193a4d593a39df7b6e", aum = -2)
    public long aUD;
    @C0064Am(aul = "7780a2feeb9343a342f41d0e2109b45b", aum = -2)
    public long aUE;
    @C0064Am(aul = "504d86b1fd223dd73062e3460e75b7a8", aum = -2)
    public long aUF;
    @C0064Am(aul = "25e5c50fcba74a193a4d593a39df7b6e", aum = -1)
    public byte aUG;
    @C0064Am(aul = "7780a2feeb9343a342f41d0e2109b45b", aum = -1)
    public byte aUH;
    @C0064Am(aul = "504d86b1fd223dd73062e3460e75b7a8", aum = -1)
    public byte aUI;
    @C0064Am(aul = "2d7d1308e495ae1114bc08403bd1137a", aum = 0)

    /* renamed from: iH */
    public Station f8949iH;
    @C0064Am(aul = "2d7d1308e495ae1114bc08403bd1137a", aum = -2)

    /* renamed from: iK */
    public long f8950iK;
    @C0064Am(aul = "2d7d1308e495ae1114bc08403bd1137a", aum = -1)

    /* renamed from: iN */
    public byte f8951iN;

    public C3363qi() {
    }

    public C3363qi(C1616Xf xf) {
        super(xf);
    }

    /* renamed from: c */
    public C5663aRz[] mo309c() {
        return Storage._m_fields;
    }

    /* renamed from: d */
    public void mo310d() {
        super.mo310d();
        this.f8949iH = null;
        this.aUB = null;
        this.aOw = null;
        this.aUC = 0.0f;
        this.f8950iK = 0;
        this.aUD = 0;
        this.aUE = 0;
        this.aUF = 0;
        this.f8951iN = 0;
        this.aUG = 0;
        this.aUH = 0;
        this.aUI = 0;
    }
}
