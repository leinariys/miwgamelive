package logic.data.mbean;

import game.script.ship.SectorCategory;
import game.script.ship.ShipSector;
import game.script.ship.ShipStructure;
import logic.baa.C1616Xf;
import logic.res.code.C5663aRz;
import p001a.C0064Am;

/* renamed from: a.Hc */
public class C0547Hc extends aAL {
    @C0064Am(aul = "45789f9feae75a60069ecccf90f2fe0a", aum = -1)
    public byte daA;
    @C0064Am(aul = "bb6db3e775b0f3cb101ea41d9bfc9b99", aum = 0)
    public int daj;
    @C0064Am(aul = "d8b63fb838c8fdcc076e0f2f02b67175", aum = 1)
    public boolean dak;
    @C0064Am(aul = "04b0445e639ba85dfe1617a9a1214af4", aum = 2)
    public int dal;
    @C0064Am(aul = "ea0bfa2523f843a01a23ec4475fa0fab", aum = 3)
    public SectorCategory dam;
    @C0064Am(aul = "2dec14ca5c9dda332101ae1af7f8eea6", aum = 4)
    public int dan;
    @C0064Am(aul = "45789f9feae75a60069ecccf90f2fe0a", aum = 5)
    public ShipStructure dao;
    @C0064Am(aul = "bb6db3e775b0f3cb101ea41d9bfc9b99", aum = -2)
    public long dap;
    @C0064Am(aul = "d8b63fb838c8fdcc076e0f2f02b67175", aum = -2)
    public long daq;
    @C0064Am(aul = "04b0445e639ba85dfe1617a9a1214af4", aum = -2)
    public long dar;
    @C0064Am(aul = "ea0bfa2523f843a01a23ec4475fa0fab", aum = -2)
    public long das;
    @C0064Am(aul = "2dec14ca5c9dda332101ae1af7f8eea6", aum = -2)
    public long dat;
    @C0064Am(aul = "45789f9feae75a60069ecccf90f2fe0a", aum = -2)
    public long dau;
    @C0064Am(aul = "bb6db3e775b0f3cb101ea41d9bfc9b99", aum = -1)
    public byte dav;
    @C0064Am(aul = "d8b63fb838c8fdcc076e0f2f02b67175", aum = -1)
    public byte daw;
    @C0064Am(aul = "04b0445e639ba85dfe1617a9a1214af4", aum = -1)
    public byte dax;
    @C0064Am(aul = "ea0bfa2523f843a01a23ec4475fa0fab", aum = -1)
    public byte day;
    @C0064Am(aul = "2dec14ca5c9dda332101ae1af7f8eea6", aum = -1)
    public byte daz;

    public C0547Hc() {
    }

    public C0547Hc(C1616Xf xf) {
        super(xf);
    }

    /* renamed from: c */
    public C5663aRz[] mo309c() {
        return ShipSector._m_fields;
    }

    /* renamed from: d */
    public void mo310d() {
        super.mo310d();
        this.daj = 0;
        this.dak = false;
        this.dal = 0;
        this.dam = null;
        this.dan = 0;
        this.dao = null;
        this.dap = 0;
        this.daq = 0;
        this.dar = 0;
        this.das = 0;
        this.dat = 0;
        this.dau = 0;
        this.dav = 0;
        this.daw = 0;
        this.dax = 0;
        this.day = 0;
        this.daz = 0;
        this.daA = 0;
    }
}
