package logic.data.mbean;

import game.script.mission.scripting.ProtectMissionScript;
import logic.baa.C1616Xf;
import logic.res.code.C5663aRz;
import p001a.C0064Am;

/* renamed from: a.atR  reason: case insensitive filesystem */
public class C6773atR extends C2955mD {
    @C0064Am(aul = "e7ef5585b7a4f1551ba6415f14885247", aum = 1)
    public ProtectMissionScript dbM;
    @C0064Am(aul = "cb9e671a911a800f1a9c6dbdc0c8c2ce", aum = 0)
    public float dbN;
    @C0064Am(aul = "e7ef5585b7a4f1551ba6415f14885247", aum = -2)

    /* renamed from: dl */
    public long f5315dl;
    @C0064Am(aul = "e7ef5585b7a4f1551ba6415f14885247", aum = -1)

    /* renamed from: ds */
    public byte f5316ds;
    @C0064Am(aul = "cb9e671a911a800f1a9c6dbdc0c8c2ce", aum = -2)
    public long gCm;
    @C0064Am(aul = "cb9e671a911a800f1a9c6dbdc0c8c2ce", aum = -1)
    public byte gCn;

    public C6773atR() {
    }

    public C6773atR(C1616Xf xf) {
        super(xf);
    }

    /* renamed from: c */
    public C5663aRz[] mo309c() {
        return ProtectMissionScript.StateB._m_fields;
    }

    /* renamed from: d */
    public void mo310d() {
        super.mo310d();
        this.dbN = 0.0f;
        this.dbM = null;
        this.gCm = 0;
        this.f5315dl = 0;
        this.gCn = 0;
        this.f5316ds = 0;
    }
}
