package logic.data.mbean;

import game.script.item.buff.amplifier.CargoHoldAmplifier;
import logic.baa.C1616Xf;
import logic.res.code.C5663aRz;
import p001a.C0064Am;

/* renamed from: a.aws  reason: case insensitive filesystem */
public class C6953aws extends C6754asy {
    @C0064Am(aul = "7db130ff5275df2ef98edcf2a8b7ca1e", aum = 0)
    public CargoHoldAmplifier dBW;
    @C0064Am(aul = "7db130ff5275df2ef98edcf2a8b7ca1e", aum = -2)

    /* renamed from: dl */
    public long f5530dl;
    @C0064Am(aul = "7db130ff5275df2ef98edcf2a8b7ca1e", aum = -1)

    /* renamed from: ds */
    public byte f5531ds;

    public C6953aws() {
    }

    public C6953aws(C1616Xf xf) {
        super(xf);
    }

    /* renamed from: c */
    public C5663aRz[] mo309c() {
        return CargoHoldAmplifier.CargoHoldAmplifierAdapter._m_fields;
    }

    /* renamed from: d */
    public void mo310d() {
        super.mo310d();
        this.dBW = null;
        this.f5530dl = 0;
        this.f5531ds = 0;
    }
}
