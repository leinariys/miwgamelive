package logic.data.mbean;

import game.geometry.Vec3d;
import game.script.space.Node;
import game.script.spacezone.ZoneArea;
import logic.baa.C1616Xf;
import logic.res.code.C5663aRz;
import p001a.C0064Am;

/* renamed from: a.aTg  reason: case insensitive filesystem */
public class C5696aTg extends C0677Jd {
    @C0064Am(aul = "d5ab8f2b7356926acd5698324b45456c", aum = -2)

    /* renamed from: DA */
    public long f3812DA;
    @C0064Am(aul = "d5ab8f2b7356926acd5698324b45456c", aum = -1)

    /* renamed from: DE */
    public byte f3813DE;
    @C0064Am(aul = "d5ab8f2b7356926acd5698324b45456c", aum = 2)

    /* renamed from: Dw */
    public long f3814Dw;
    @C0064Am(aul = "5d00cfdc5f528a9acc6ed399e32db9fd", aum = -2)
    public long aPL;
    @C0064Am(aul = "5d00cfdc5f528a9acc6ed399e32db9fd", aum = -1)
    public byte aPN;
    @C0064Am(aul = "bb8e39cf5816a99d7e56259bde8b5230", aum = 0)
    public Node aSh;
    @C0064Am(aul = "bb8e39cf5816a99d7e56259bde8b5230", aum = -2)
    public long bec;
    @C0064Am(aul = "bb8e39cf5816a99d7e56259bde8b5230", aum = -1)
    public byte bei;
    @C0064Am(aul = "5d00cfdc5f528a9acc6ed399e32db9fd", aum = 1)
    public Vec3d center;

    public C5696aTg() {
    }

    public C5696aTg(C1616Xf xf) {
        super(xf);
    }

    /* renamed from: c */
    public C5663aRz[] mo309c() {
        return ZoneArea._m_fields;
    }

    /* renamed from: d */
    public void mo310d() {
        this.aSh = null;
        this.center = null;
        this.f3814Dw = 0;
        this.bec = 0;
        this.aPL = 0;
        this.f3812DA = 0;
        this.bei = 0;
        this.aPN = 0;
        this.f3813DE = 0;
    }
}
