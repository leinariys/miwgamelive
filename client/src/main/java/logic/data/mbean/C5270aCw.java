package logic.data.mbean;

import game.script.insurance.InsuranceDefaults;
import logic.baa.C1616Xf;
import logic.res.code.C5663aRz;
import p001a.C0064Am;
import taikodom.infra.script.I18NString;

import java.util.UUID;

/* renamed from: a.aCw  reason: case insensitive filesystem */
public class C5270aCw extends C3805vD {
    @C0064Am(aul = "fbba372fc7edca3a45ead54f5c409481", aum = 4)

    /* renamed from: bK */
    public UUID f2493bK;
    @C0064Am(aul = "7b3aed4459fe2156d5572e469e4e0eb5", aum = -2)
    public long cRJ;
    @C0064Am(aul = "7b3aed4459fe2156d5572e469e4e0eb5", aum = -1)
    public byte cRL;
    @C0064Am(aul = "7b3aed4459fe2156d5572e469e4e0eb5", aum = 0)
    public long ciA;
    @C0064Am(aul = "ce153f84d7c363135d0a13d64be749dd", aum = 1)
    public long ciC;
    @C0064Am(aul = "c9799ef61ece871e75ec2f47841ba03a", aum = 2)
    public I18NString ciE;
    @C0064Am(aul = "749fec917040e3f6686ef2047d0973a7", aum = 3)
    public I18NString ciG;
    @C0064Am(aul = "ce153f84d7c363135d0a13d64be749dd", aum = -2)
    public long huQ;
    @C0064Am(aul = "c9799ef61ece871e75ec2f47841ba03a", aum = -2)
    public long huR;
    @C0064Am(aul = "749fec917040e3f6686ef2047d0973a7", aum = -2)
    public long huS;
    @C0064Am(aul = "ce153f84d7c363135d0a13d64be749dd", aum = -1)
    public byte huT;
    @C0064Am(aul = "c9799ef61ece871e75ec2f47841ba03a", aum = -1)
    public byte huU;
    @C0064Am(aul = "749fec917040e3f6686ef2047d0973a7", aum = -1)
    public byte huV;
    @C0064Am(aul = "fbba372fc7edca3a45ead54f5c409481", aum = -2)

    /* renamed from: oL */
    public long f2494oL;
    @C0064Am(aul = "fbba372fc7edca3a45ead54f5c409481", aum = -1)

    /* renamed from: oS */
    public byte f2495oS;

    public C5270aCw() {
    }

    public C5270aCw(C1616Xf xf) {
        super(xf);
    }

    /* renamed from: c */
    public C5663aRz[] mo309c() {
        return InsuranceDefaults._m_fields;
    }

    /* renamed from: d */
    public void mo310d() {
        super.mo310d();
        this.ciA = 0;
        this.ciC = 0;
        this.ciE = null;
        this.ciG = null;
        this.f2493bK = null;
        this.cRJ = 0;
        this.huQ = 0;
        this.huR = 0;
        this.huS = 0;
        this.f2494oL = 0;
        this.cRL = 0;
        this.huT = 0;
        this.huU = 0;
        this.huV = 0;
        this.f2495oS = 0;
    }
}
