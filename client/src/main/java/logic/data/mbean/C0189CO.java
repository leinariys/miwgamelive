package logic.data.mbean;

import game.script.item.ItemType;
import game.script.ship.OutpostActivationItem;
import logic.baa.C1616Xf;
import logic.res.code.C5663aRz;
import p001a.C0064Am;

import java.util.UUID;

/* renamed from: a.CO */
public class C0189CO extends C5292aDs {
    @C0064Am(aul = "685a59f14517cbc2efa65d64d813e96a", aum = -2)
    public long aLG;
    @C0064Am(aul = "685a59f14517cbc2efa65d64d813e96a", aum = -1)
    public byte aLL;
    @C0064Am(aul = "61a1073ff25526df2ec9b9898997e336", aum = 3)

    /* renamed from: bK */
    public UUID f283bK;
    @C0064Am(aul = "a473db16ef0d0371a14a6096ec2397e5", aum = -2)
    public long bmV;
    @C0064Am(aul = "a473db16ef0d0371a14a6096ec2397e5", aum = -1)
    public byte bmW;
    @C0064Am(aul = "a89f5c826390d138056528a9c0d7f2fe", aum = -2)
    public long cAo;
    @C0064Am(aul = "a89f5c826390d138056528a9c0d7f2fe", aum = -1)
    public byte cAp;
    @C0064Am(aul = "a89f5c826390d138056528a9c0d7f2fe", aum = 2)
    public int current;
    @C0064Am(aul = "a473db16ef0d0371a14a6096ec2397e5", aum = 0)

    /* renamed from: cw */
    public ItemType f284cw;
    @C0064Am(aul = "685a59f14517cbc2efa65d64d813e96a", aum = 1)

    /* renamed from: cy */
    public int f285cy;
    @C0064Am(aul = "3673f82d3767643e64c926171d8aaf7f", aum = 4)
    public String handle;
    @C0064Am(aul = "61a1073ff25526df2ec9b9898997e336", aum = -2)

    /* renamed from: oL */
    public long f286oL;
    @C0064Am(aul = "61a1073ff25526df2ec9b9898997e336", aum = -1)

    /* renamed from: oS */
    public byte f287oS;
    @C0064Am(aul = "3673f82d3767643e64c926171d8aaf7f", aum = -2)

    /* renamed from: ok */
    public long f288ok;
    @C0064Am(aul = "3673f82d3767643e64c926171d8aaf7f", aum = -1)

    /* renamed from: on */
    public byte f289on;

    public C0189CO() {
    }

    public C0189CO(C1616Xf xf) {
        super(xf);
    }

    /* renamed from: c */
    public C5663aRz[] mo309c() {
        return OutpostActivationItem._m_fields;
    }

    /* renamed from: d */
    public void mo310d() {
        super.mo310d();
        this.f284cw = null;
        this.f285cy = 0;
        this.current = 0;
        this.f283bK = null;
        this.handle = null;
        this.bmV = 0;
        this.aLG = 0;
        this.cAo = 0;
        this.f286oL = 0;
        this.f288ok = 0;
        this.bmW = 0;
        this.aLL = 0;
        this.cAp = 0;
        this.f287oS = 0;
        this.f289on = 0;
    }
}
