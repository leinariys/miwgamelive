package logic.data.mbean;

import game.script.citizenship.CitizenImprovement;
import game.script.player.Player;
import game.script.resource.Asset;
import logic.baa.C1616Xf;
import logic.res.code.C5663aRz;
import p001a.C0064Am;
import taikodom.infra.script.I18NString;

/* renamed from: a.bg */
public class C2057bg extends C3805vD {
    @C0064Am(aul = "66c936df299eeb8f3fedf268a5bbd9a7", aum = -1)

    /* renamed from: jK */
    public byte f5860jK;
    @C0064Am(aul = "66c936df299eeb8f3fedf268a5bbd9a7", aum = 2)

    /* renamed from: jn */
    public Asset f5861jn;
    @C0064Am(aul = "66c936df299eeb8f3fedf268a5bbd9a7", aum = -2)

    /* renamed from: jy */
    public long f5862jy;
    @C0064Am(aul = "f06d0709f96f8a3c115bae6244f1bf0c", aum = 0)

    /* renamed from: nh */
    public I18NString f5863nh;
    @C0064Am(aul = "f3e862c8a203facd056615dc57f02979", aum = 1)

    /* renamed from: ni */
    public I18NString f5864ni;
    @C0064Am(aul = "b4608365f5fec71402bdafc90054fbf4", aum = 3)

    /* renamed from: nj */
    public Player f5865nj;
    @C0064Am(aul = "f06d0709f96f8a3c115bae6244f1bf0c", aum = -2)

    /* renamed from: nk */
    public long f5866nk;
    @C0064Am(aul = "f3e862c8a203facd056615dc57f02979", aum = -2)

    /* renamed from: nl */
    public long f5867nl;
    @C0064Am(aul = "b4608365f5fec71402bdafc90054fbf4", aum = -2)

    /* renamed from: nm */
    public long f5868nm;
    @C0064Am(aul = "f06d0709f96f8a3c115bae6244f1bf0c", aum = -1)

    /* renamed from: nn */
    public byte f5869nn;
    @C0064Am(aul = "f3e862c8a203facd056615dc57f02979", aum = -1)

    /* renamed from: no */
    public byte f5870no;
    @C0064Am(aul = "b4608365f5fec71402bdafc90054fbf4", aum = -1)

    /* renamed from: np */
    public byte f5871np;

    public C2057bg() {
    }

    public C2057bg(C1616Xf xf) {
        super(xf);
    }

    /* renamed from: c */
    public C5663aRz[] mo309c() {
        return CitizenImprovement._m_fields;
    }

    /* renamed from: d */
    public void mo310d() {
        super.mo310d();
        this.f5863nh = null;
        this.f5864ni = null;
        this.f5861jn = null;
        this.f5865nj = null;
        this.f5866nk = 0;
        this.f5867nl = 0;
        this.f5862jy = 0;
        this.f5868nm = 0;
        this.f5869nn = 0;
        this.f5870no = 0;
        this.f5860jK = 0;
        this.f5871np = 0;
    }
}
