package logic.data.mbean;

import game.script.item.durability.ItemDurabilityDefaults;
import logic.baa.C1616Xf;
import logic.res.code.C5663aRz;
import p001a.C0064Am;

import java.util.UUID;

/* renamed from: a.RZ */
public class C1189RZ extends C5292aDs {
    @C0064Am(aul = "dd7013ee64dc17ba31907efcab6638e4", aum = 2)

    /* renamed from: bK */
    public UUID f1494bK;
    @C0064Am(aul = "b9efa6caed4550b2188d45e88f4297ad", aum = 0)
    public long eca;
    @C0064Am(aul = "ebbc15844f5b904f786d503eeec46f68", aum = 1)
    public long ecb;
    @C0064Am(aul = "b9efa6caed4550b2188d45e88f4297ad", aum = -2)
    public long ecc;
    @C0064Am(aul = "ebbc15844f5b904f786d503eeec46f68", aum = -2)
    public long ecd;
    @C0064Am(aul = "b9efa6caed4550b2188d45e88f4297ad", aum = -1)
    public byte ece;
    @C0064Am(aul = "ebbc15844f5b904f786d503eeec46f68", aum = -1)
    public byte ecf;
    @C0064Am(aul = "dd7013ee64dc17ba31907efcab6638e4", aum = -2)

    /* renamed from: oL */
    public long f1495oL;
    @C0064Am(aul = "dd7013ee64dc17ba31907efcab6638e4", aum = -1)

    /* renamed from: oS */
    public byte f1496oS;

    public C1189RZ() {
    }

    public C1189RZ(C1616Xf xf) {
        super(xf);
    }

    /* renamed from: c */
    public C5663aRz[] mo309c() {
        return ItemDurabilityDefaults._m_fields;
    }

    /* renamed from: d */
    public void mo310d() {
        super.mo310d();
        this.eca = 0;
        this.ecb = 0;
        this.f1494bK = null;
        this.ecc = 0;
        this.ecd = 0;
        this.f1495oL = 0;
        this.ece = 0;
        this.ecf = 0;
        this.f1496oS = 0;
    }
}
