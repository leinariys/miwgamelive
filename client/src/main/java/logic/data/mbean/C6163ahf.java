package logic.data.mbean;

import game.network.message.serializable.C1556Wo;
import game.network.message.serializable.C3438ra;
import game.script.BlockStatus;
import game.script.PlayerSocialController;
import game.script.associates.Associates;
import game.script.bank.BankAccount;
import game.script.citizenship.CitizenshipControl;
import game.script.cloning.CloningInfo;
import game.script.corporation.Corporation;
import game.script.corporation.CorporationCreationStatus;
import game.script.faction.Faction;
import game.script.hangar.Hangar;
import game.script.item.Item;
import game.script.player.Player;
import game.script.player.PlayerAssistant;
import game.script.player.PlayerSessionLog;
import game.script.player.User;
import game.script.progress.OniLogger;
import game.script.space.Loot;
import game.script.storage.Storage;
import game.script.trade.Trade;
import logic.baa.C1616Xf;
import logic.res.code.C5663aRz;
import p001a.C0064Am;
import p001a.C2546ge;
import p001a.aMU;

/* renamed from: a.ahf  reason: case insensitive filesystem */
public class C6163ahf extends C0205CW {
    @C0064Am(aul = "7ee960480a9c85fdf18811f679441960", aum = -2)

    /* renamed from: Nf */
    public long f4564Nf;
    @C0064Am(aul = "7ee960480a9c85fdf18811f679441960", aum = -1)

    /* renamed from: Nh */
    public byte f4565Nh;
    @C0064Am(aul = "13368b165449e9c6d8f1f6727e30f64c", aum = 18)
    public Loot aZM;
    @C0064Am(aul = "40f6b440ed8030a8500d96cbf336a13c", aum = 11)
    public Faction axU;
    @C0064Am(aul = "40f6b440ed8030a8500d96cbf336a13c", aum = -2)
    public long ayi;
    @C0064Am(aul = "40f6b440ed8030a8500d96cbf336a13c", aum = -1)
    public byte ayx;
    @C0064Am(aul = "f5b6b0d81cbeed2e304e69d30fe44982", aum = -1)
    public byte azF;
    @C0064Am(aul = "f5b6b0d81cbeed2e304e69d30fe44982", aum = -2)
    public long azq;
    @C0064Am(aul = "4ad52749613f6450903d2cfca97346e9", aum = 13)
    public Corporation daW;
    @C0064Am(aul = "4ad52749613f6450903d2cfca97346e9", aum = -2)
    public long dbh;
    @C0064Am(aul = "4ad52749613f6450903d2cfca97346e9", aum = -1)
    public byte dbq;
    @C0064Am(aul = "e87a453a33a85425a1afdf8b80074c34", aum = 15)
    public PlayerAssistant dqK;
    @C0064Am(aul = "f5b6b0d81cbeed2e304e69d30fe44982", aum = 1)
    public Party fGD;
    @C0064Am(aul = "7017b525483e6c7b86822c20bebe4e94", aum = 3)
    public Associates fGE;
    @C0064Am(aul = "20977a88ff1b726cbc09e2a7ffefe9ad", aum = 4)
    public CloningInfo fGF;
    @C0064Am(aul = "f90deaea7b1294df6525dba3988f2b42", aum = 5)
    public C3438ra<Hangar> fGG;
    @C0064Am(aul = "7f294f174be37e35d0544aaa730636a3", aum = 6)
    public C3438ra<Storage> fGH;
    @C0064Am(aul = "491845629c2b24a6daefd66706914bfa", aum = 7)
    public C3438ra<Item> fGI;
    @C0064Am(aul = "51d444fcad3f7346fb01067e656563bc", aum = 8)
    public C3438ra<Item> fGJ;
    @C0064Am(aul = "882e8a22dcc482a3febcbe0b1c79ced9", aum = 9)
    public BankAccount fGK;
    @C0064Am(aul = "02fcf107f7bc8cb9d554ba98292e7d96", aum = 12)
    public CorporationCreationStatus fGL;
    @C0064Am(aul = "ff554d34da5b684f15d3ba8305764084", aum = 14)
    public OniLogger fGM;
    @C0064Am(aul = "e328f1b223761858bc3d46e1027be2eb", aum = 16)
    public CitizenshipControl fGN;
    @C0064Am(aul = "9b2b13a4b9331d42b34f2af602be9a1b", aum = 17)
    public PlayerSocialController fGO;
    @C0064Am(aul = "a52c8c62cc9b48b5fc8c7f0d01f57537", aum = 19)
    public boolean fGP;
    @C0064Am(aul = "9ab9a431813c0c09cbb75f702c54c3da", aum = 20)
    public boolean fGQ;
    @C0064Am(aul = "c678beee15b52c3525a5418d54540ff8", aum = 21)
    public C1556Wo<C2546ge, BlockStatus> fGR;
    @C0064Am(aul = "f38d2534324fc95ee524365c71fc0562", aum = 22)
    public PlayerSessionLog fGS;
    @C0064Am(aul = "3df9a8e07008aa0ad899b1d4afbfe5ef", aum = 23)
    public Player.ItemsDurabilityTask fGT;
    @C0064Am(aul = "26d6d1d2708e3516de2975cf414b12f8", aum = 24)
    public int fGU;
    @C0064Am(aul = "8194074b67788acd9dc58d3979106079", aum = 25)
    public boolean fGV;
    @C0064Am(aul = "3d4ec3a25a6ce867f6a5b94b6d31daa5", aum = 26)
    public aMU fGW;
    @C0064Am(aul = "143bb2050c02054f901643245ac4207d", aum = 27)
    public Player.PlayerTimeoutTask fGX;
    @C0064Am(aul = "a278dfeeb89b3acc6e1ae9f01ad4f0f4", aum = 28)
    public long fGY;
    @C0064Am(aul = "7b3b4b8da105949946b4d7c8ac134621", aum = 29)
    public long fGZ;
    @C0064Am(aul = "20977a88ff1b726cbc09e2a7ffefe9ad", aum = -1)
    public byte fHA;
    @C0064Am(aul = "f90deaea7b1294df6525dba3988f2b42", aum = -1)
    public byte fHB;
    @C0064Am(aul = "7f294f174be37e35d0544aaa730636a3", aum = -1)
    public byte fHC;
    @C0064Am(aul = "491845629c2b24a6daefd66706914bfa", aum = -1)
    public byte fHD;
    @C0064Am(aul = "51d444fcad3f7346fb01067e656563bc", aum = -1)
    public byte fHE;
    @C0064Am(aul = "882e8a22dcc482a3febcbe0b1c79ced9", aum = -1)
    public byte fHF;
    @C0064Am(aul = "02fcf107f7bc8cb9d554ba98292e7d96", aum = -1)
    public byte fHG;
    @C0064Am(aul = "ff554d34da5b684f15d3ba8305764084", aum = -1)
    public byte fHH;
    @C0064Am(aul = "e87a453a33a85425a1afdf8b80074c34", aum = -1)
    public byte fHI;
    @C0064Am(aul = "e328f1b223761858bc3d46e1027be2eb", aum = -1)
    public byte fHJ;
    @C0064Am(aul = "9b2b13a4b9331d42b34f2af602be9a1b", aum = -1)
    public byte fHK;
    @C0064Am(aul = "a52c8c62cc9b48b5fc8c7f0d01f57537", aum = -1)
    public byte fHL;
    @C0064Am(aul = "9ab9a431813c0c09cbb75f702c54c3da", aum = -1)
    public byte fHM;
    @C0064Am(aul = "c678beee15b52c3525a5418d54540ff8", aum = -1)
    public byte fHN;
    @C0064Am(aul = "f38d2534324fc95ee524365c71fc0562", aum = -1)
    public byte fHO;
    @C0064Am(aul = "3df9a8e07008aa0ad899b1d4afbfe5ef", aum = -1)
    public byte fHP;
    @C0064Am(aul = "26d6d1d2708e3516de2975cf414b12f8", aum = -1)
    public byte fHQ;
    @C0064Am(aul = "8194074b67788acd9dc58d3979106079", aum = -1)
    public byte fHR;
    @C0064Am(aul = "3d4ec3a25a6ce867f6a5b94b6d31daa5", aum = -1)
    public byte fHS;
    @C0064Am(aul = "143bb2050c02054f901643245ac4207d", aum = -1)
    public byte fHT;
    @C0064Am(aul = "a278dfeeb89b3acc6e1ae9f01ad4f0f4", aum = -1)
    public byte fHU;
    @C0064Am(aul = "7b3b4b8da105949946b4d7c8ac134621", aum = -1)
    public byte fHV;
    @C0064Am(aul = "0a190893e78ff75583cd47fc4db0d014", aum = -2)
    public long fHa;
    @C0064Am(aul = "7017b525483e6c7b86822c20bebe4e94", aum = -2)
    public long fHb;
    @C0064Am(aul = "20977a88ff1b726cbc09e2a7ffefe9ad", aum = -2)
    public long fHc;
    @C0064Am(aul = "f90deaea7b1294df6525dba3988f2b42", aum = -2)
    public long fHd;
    @C0064Am(aul = "7f294f174be37e35d0544aaa730636a3", aum = -2)
    public long fHe;
    @C0064Am(aul = "491845629c2b24a6daefd66706914bfa", aum = -2)
    public long fHf;
    @C0064Am(aul = "51d444fcad3f7346fb01067e656563bc", aum = -2)
    public long fHg;
    @C0064Am(aul = "882e8a22dcc482a3febcbe0b1c79ced9", aum = -2)
    public long fHh;
    @C0064Am(aul = "02fcf107f7bc8cb9d554ba98292e7d96", aum = -2)
    public long fHi;
    @C0064Am(aul = "ff554d34da5b684f15d3ba8305764084", aum = -2)
    public long fHj;
    @C0064Am(aul = "e87a453a33a85425a1afdf8b80074c34", aum = -2)
    public long fHk;
    @C0064Am(aul = "e328f1b223761858bc3d46e1027be2eb", aum = -2)
    public long fHl;
    @C0064Am(aul = "9b2b13a4b9331d42b34f2af602be9a1b", aum = -2)
    public long fHm;
    @C0064Am(aul = "a52c8c62cc9b48b5fc8c7f0d01f57537", aum = -2)
    public long fHn;
    @C0064Am(aul = "9ab9a431813c0c09cbb75f702c54c3da", aum = -2)
    public long fHo;
    @C0064Am(aul = "c678beee15b52c3525a5418d54540ff8", aum = -2)
    public long fHp;
    @C0064Am(aul = "f38d2534324fc95ee524365c71fc0562", aum = -2)
    public long fHq;
    @C0064Am(aul = "3df9a8e07008aa0ad899b1d4afbfe5ef", aum = -2)
    public long fHr;
    @C0064Am(aul = "26d6d1d2708e3516de2975cf414b12f8", aum = -2)
    public long fHs;
    @C0064Am(aul = "8194074b67788acd9dc58d3979106079", aum = -2)
    public long fHt;
    @C0064Am(aul = "3d4ec3a25a6ce867f6a5b94b6d31daa5", aum = -2)
    public long fHu;
    @C0064Am(aul = "143bb2050c02054f901643245ac4207d", aum = -2)
    public long fHv;
    @C0064Am(aul = "a278dfeeb89b3acc6e1ae9f01ad4f0f4", aum = -2)
    public long fHw;
    @C0064Am(aul = "7b3b4b8da105949946b4d7c8ac134621", aum = -2)
    public long fHx;
    @C0064Am(aul = "0a190893e78ff75583cd47fc4db0d014", aum = -1)
    public byte fHy;
    @C0064Am(aul = "7017b525483e6c7b86822c20bebe4e94", aum = -1)
    public byte fHz;
    @C0064Am(aul = "9885ab4fb8801859db567bd6d03fdfd1", aum = 10)
    public User far;
    @C0064Am(aul = "0a190893e78ff75583cd47fc4db0d014", aum = 2)
    public Trade fky;
    @C0064Am(aul = "13368b165449e9c6d8f1f6727e30f64c", aum = -2)
    public long fqq;
    @C0064Am(aul = "13368b165449e9c6d8f1f6727e30f64c", aum = -1)
    public byte fqr;
    @C0064Am(aul = "7ee960480a9c85fdf18811f679441960", aum = 0)
    public String name;
    @C0064Am(aul = "9885ab4fb8801859db567bd6d03fdfd1", aum = -2)

    /* renamed from: nm */
    public long f4566nm;
    @C0064Am(aul = "9885ab4fb8801859db567bd6d03fdfd1", aum = -1)

    /* renamed from: np */
    public byte f4567np;

    public C6163ahf() {
    }

    public C6163ahf(C1616Xf xf) {
        super(xf);
    }

    /* renamed from: c */
    public C5663aRz[] mo309c() {
        return Player._m_fields;
    }

    /* renamed from: d */
    public void mo310d() {
        super.mo310d();
        this.name = null;
        this.fGD = null;
        this.fky = null;
        this.fGE = null;
        this.fGF = null;
        this.fGG = null;
        this.fGH = null;
        this.fGI = null;
        this.fGJ = null;
        this.fGK = null;
        this.far = null;
        this.axU = null;
        this.fGL = null;
        this.daW = null;
        this.fGM = null;
        this.dqK = null;
        this.fGN = null;
        this.fGO = null;
        this.aZM = null;
        this.fGP = false;
        this.fGQ = false;
        this.fGR = null;
        this.fGS = null;
        this.fGT = null;
        this.fGU = 0;
        this.fGV = false;
        this.fGW = null;
        this.fGX = null;
        this.fGY = 0;
        this.fGZ = 0;
        this.f4564Nf = 0;
        this.azq = 0;
        this.fHa = 0;
        this.fHb = 0;
        this.fHc = 0;
        this.fHd = 0;
        this.fHe = 0;
        this.fHf = 0;
        this.fHg = 0;
        this.fHh = 0;
        this.f4566nm = 0;
        this.ayi = 0;
        this.fHi = 0;
        this.dbh = 0;
        this.fHj = 0;
        this.fHk = 0;
        this.fHl = 0;
        this.fHm = 0;
        this.fqq = 0;
        this.fHn = 0;
        this.fHo = 0;
        this.fHp = 0;
        this.fHq = 0;
        this.fHr = 0;
        this.fHs = 0;
        this.fHt = 0;
        this.fHu = 0;
        this.fHv = 0;
        this.fHw = 0;
        this.fHx = 0;
        this.f4565Nh = 0;
        this.azF = 0;
        this.fHy = 0;
        this.fHz = 0;
        this.fHA = 0;
        this.fHB = 0;
        this.fHC = 0;
        this.fHD = 0;
        this.fHE = 0;
        this.fHF = 0;
        this.f4567np = 0;
        this.ayx = 0;
        this.fHG = 0;
        this.dbq = 0;
        this.fHH = 0;
        this.fHI = 0;
        this.fHJ = 0;
        this.fHK = 0;
        this.fqr = 0;
        this.fHL = 0;
        this.fHM = 0;
        this.fHN = 0;
        this.fHO = 0;
        this.fHP = 0;
        this.fHQ = 0;
        this.fHR = 0;
        this.fHS = 0;
        this.fHT = 0;
        this.fHU = 0;
        this.fHV = 0;
    }
}
