package logic.data.mbean;

import com.hoplon.geometry.Vec3f;
import game.network.message.serializable.C1556Wo;
import game.network.message.serializable.C2686iZ;
import game.network.message.serializable.C3438ra;
import game.script.agent.Agent;
import game.script.cloning.CloningCenter;
import game.script.faction.Faction;
import game.script.hangar.Hangar;
import game.script.newmarket.store.Store;
import game.script.npc.NPC;
import game.script.pda.DatabaseCategory;
import game.script.pda.TaikopediaEntry;
import game.script.player.Player;
import game.script.resource.Asset;
import game.script.ship.Station;
import game.script.storage.Storage;
import logic.baa.C1616Xf;
import logic.res.code.C5663aRz;
import p001a.C0064Am;
import taikodom.geom.Orientation;
import taikodom.infra.script.I18NString;

import java.util.UUID;

/* renamed from: a.lx */
public class C2947lx extends C2217cs {
    @C0064Am(aul = "5cf325f11a8256c9c637ecef1ab5cb34", aum = 0)

    /* renamed from: LQ */
    public Asset f8594LQ;
    @C0064Am(aul = "58e4d9aa51c579b19cdef083c80ad5de", aum = 1)

    /* renamed from: LW */
    public Vec3f f8595LW;
    @C0064Am(aul = "95ba83a607213f7863827868affd96ea", aum = 2)

    /* renamed from: LY */
    public Orientation f8596LY;
    @C0064Am(aul = "12c5c7c9f4d772e26612ad684a174b26", aum = 16)

    /* renamed from: Me */
    public C3438ra<Faction> f8597Me;
    @C0064Am(aul = "05a4eadd18f282b0b10187d507567fe2", aum = -2)
    public long avd;
    @C0064Am(aul = "05a4eadd18f282b0b10187d507567fe2", aum = -1)
    public byte avi;
    @C0064Am(aul = "d727a303e7cdee6ceeba7f73e3e8e158", aum = 3)
    public Asset axR;
    @C0064Am(aul = "9d7c361079ec854372fa4ef5b16f6319", aum = 7)
    public Store axS;
    @C0064Am(aul = "f1b7687a150f7db7a3366e093bcf7e05", aum = 8)
    public CloningCenter axT;
    @C0064Am(aul = "db7588f8db5d40238035e900b09275af", aum = 9)
    public Faction axU;
    @C0064Am(aul = "3cb49860a2c28fd648493b989022d9dc", aum = 10)
    public C2686iZ<Hangar> axV;
    @C0064Am(aul = "45bed0de13fb210ea8e03b1ba41365c2", aum = 11)
    public C2686iZ<Storage> axW;
    @C0064Am(aul = "1f03c843afa88dcd5b33a3269e9f7a67", aum = 12)
    public C2686iZ<Player> axX;
    @C0064Am(aul = "fc83d33a2146223e13258376afa4fc16", aum = 13)
    public C3438ra<NPC> axY;
    @C0064Am(aul = "7a349fc8f317ede34c09f4f3ab0741eb", aum = 14)
    public C3438ra<Agent> axZ;
    @C0064Am(aul = "1f03c843afa88dcd5b33a3269e9f7a67", aum = -1)
    public byte ayA;
    @C0064Am(aul = "fc83d33a2146223e13258376afa4fc16", aum = -1)
    public byte ayB;
    @C0064Am(aul = "7a349fc8f317ede34c09f4f3ab0741eb", aum = -1)
    public byte ayC;
    @C0064Am(aul = "3b498424d29b4073dc438ce096395dbc", aum = -1)
    public byte ayD;
    @C0064Am(aul = "12c5c7c9f4d772e26612ad684a174b26", aum = -1)
    public byte ayE;
    @C0064Am(aul = "cf0259290db851f6d79ea712d71a9329", aum = -1)
    public byte ayF;
    @C0064Am(aul = "3b498424d29b4073dc438ce096395dbc", aum = 15)
    public C1556Wo<Agent, Integer> aya;
    @C0064Am(aul = "cf0259290db851f6d79ea712d71a9329", aum = 20)
    public Station.C0099c ayb;
    @C0064Am(aul = "5cf325f11a8256c9c637ecef1ab5cb34", aum = -2)
    public long ayc;
    @C0064Am(aul = "58e4d9aa51c579b19cdef083c80ad5de", aum = -2)
    public long ayd;
    @C0064Am(aul = "95ba83a607213f7863827868affd96ea", aum = -2)
    public long aye;
    @C0064Am(aul = "d727a303e7cdee6ceeba7f73e3e8e158", aum = -2)
    public long ayf;
    @C0064Am(aul = "9d7c361079ec854372fa4ef5b16f6319", aum = -2)
    public long ayg;
    @C0064Am(aul = "f1b7687a150f7db7a3366e093bcf7e05", aum = -2)
    public long ayh;
    @C0064Am(aul = "db7588f8db5d40238035e900b09275af", aum = -2)
    public long ayi;
    @C0064Am(aul = "3cb49860a2c28fd648493b989022d9dc", aum = -2)
    public long ayj;
    @C0064Am(aul = "45bed0de13fb210ea8e03b1ba41365c2", aum = -2)
    public long ayk;
    @C0064Am(aul = "1f03c843afa88dcd5b33a3269e9f7a67", aum = -2)
    public long ayl;
    @C0064Am(aul = "fc83d33a2146223e13258376afa4fc16", aum = -2)
    public long aym;
    @C0064Am(aul = "7a349fc8f317ede34c09f4f3ab0741eb", aum = -2)
    public long ayn;
    @C0064Am(aul = "3b498424d29b4073dc438ce096395dbc", aum = -2)
    public long ayo;
    @C0064Am(aul = "12c5c7c9f4d772e26612ad684a174b26", aum = -2)
    public long ayp;
    @C0064Am(aul = "cf0259290db851f6d79ea712d71a9329", aum = -2)
    public long ayq;
    @C0064Am(aul = "5cf325f11a8256c9c637ecef1ab5cb34", aum = -1)
    public byte ayr;
    @C0064Am(aul = "58e4d9aa51c579b19cdef083c80ad5de", aum = -1)
    public byte ays;
    @C0064Am(aul = "95ba83a607213f7863827868affd96ea", aum = -1)
    public byte ayt;
    @C0064Am(aul = "d727a303e7cdee6ceeba7f73e3e8e158", aum = -1)
    public byte ayu;
    @C0064Am(aul = "9d7c361079ec854372fa4ef5b16f6319", aum = -1)
    public byte ayv;
    @C0064Am(aul = "f1b7687a150f7db7a3366e093bcf7e05", aum = -1)
    public byte ayw;
    @C0064Am(aul = "db7588f8db5d40238035e900b09275af", aum = -1)
    public byte ayx;
    @C0064Am(aul = "3cb49860a2c28fd648493b989022d9dc", aum = -1)
    public byte ayy;
    @C0064Am(aul = "45bed0de13fb210ea8e03b1ba41365c2", aum = -1)
    public byte ayz;
    @C0064Am(aul = "b43f15eebc7ed203b8cb4d65cf86322a", aum = 22)

    /* renamed from: bK */
    public UUID f8598bK;
    @C0064Am(aul = "1594952326e7602b876b1ae57346ed79", aum = 6)
    public String handle;
    @C0064Am(aul = "ee367c1cff0afbf7fd82a312481e7ce8", aum = -1)

    /* renamed from: jK */
    public byte f8599jK;
    @C0064Am(aul = "78b68fc5f53af59d0c9d62c610ac27a0", aum = 19)

    /* renamed from: jS */
    public DatabaseCategory f8600jS;
    @C0064Am(aul = "841cc096ac605e2f71bcd3055db0447f", aum = 18)

    /* renamed from: jT */
    public C3438ra<TaikopediaEntry> f8601jT;
    @C0064Am(aul = "0f324e22ba416a6336fc7408951f4ca5", aum = 21)

    /* renamed from: jV */
    public float f8602jV;
    @C0064Am(aul = "78b68fc5f53af59d0c9d62c610ac27a0", aum = -2)

    /* renamed from: jX */
    public long f8603jX;
    @C0064Am(aul = "841cc096ac605e2f71bcd3055db0447f", aum = -2)

    /* renamed from: jY */
    public long f8604jY;
    @C0064Am(aul = "ee367c1cff0afbf7fd82a312481e7ce8", aum = 5)

    /* renamed from: jn */
    public Asset f8605jn;
    @C0064Am(aul = "ee367c1cff0afbf7fd82a312481e7ce8", aum = -2)

    /* renamed from: jy */
    public long f8606jy;
    @C0064Am(aul = "0f324e22ba416a6336fc7408951f4ca5", aum = -2)

    /* renamed from: ka */
    public long f8607ka;
    @C0064Am(aul = "78b68fc5f53af59d0c9d62c610ac27a0", aum = -1)

    /* renamed from: kc */
    public byte f8608kc;
    @C0064Am(aul = "841cc096ac605e2f71bcd3055db0447f", aum = -1)

    /* renamed from: kd */
    public byte f8609kd;
    @C0064Am(aul = "0f324e22ba416a6336fc7408951f4ca5", aum = -1)

    /* renamed from: kf */
    public byte f8610kf;
    @C0064Am(aul = "46c59775ff8eb1d867825694082df168", aum = 17)

    /* renamed from: nh */
    public I18NString f8611nh;
    @C0064Am(aul = "46c59775ff8eb1d867825694082df168", aum = -2)

    /* renamed from: nk */
    public long f8612nk;
    @C0064Am(aul = "46c59775ff8eb1d867825694082df168", aum = -1)

    /* renamed from: nn */
    public byte f8613nn;
    @C0064Am(aul = "b43f15eebc7ed203b8cb4d65cf86322a", aum = -2)

    /* renamed from: oL */
    public long f8614oL;
    @C0064Am(aul = "b43f15eebc7ed203b8cb4d65cf86322a", aum = -1)

    /* renamed from: oS */
    public byte f8615oS;
    @C0064Am(aul = "1594952326e7602b876b1ae57346ed79", aum = -2)

    /* renamed from: ok */
    public long f8616ok;
    @C0064Am(aul = "1594952326e7602b876b1ae57346ed79", aum = -1)

    /* renamed from: on */
    public byte f8617on;
    @C0064Am(aul = "05a4eadd18f282b0b10187d507567fe2", aum = 4)

    /* renamed from: uT */
    public String f8618uT;

    public C2947lx() {
    }

    public C2947lx(C1616Xf xf) {
        super(xf);
    }

    /* renamed from: c */
    public C5663aRz[] mo309c() {
        return Station._m_fields;
    }

    /* renamed from: d */
    public void mo310d() {
        super.mo310d();
        this.f8594LQ = null;
        this.f8595LW = null;
        this.f8596LY = null;
        this.axR = null;
        this.f8618uT = null;
        this.f8605jn = null;
        this.handle = null;
        this.axS = null;
        this.axT = null;
        this.axU = null;
        this.axV = null;
        this.axW = null;
        this.axX = null;
        this.axY = null;
        this.axZ = null;
        this.aya = null;
        this.f8597Me = null;
        this.f8611nh = null;
        this.f8601jT = null;
        this.f8600jS = null;
        this.ayb = null;
        this.f8602jV = 0.0f;
        this.f8598bK = null;
        this.ayc = 0;
        this.ayd = 0;
        this.aye = 0;
        this.ayf = 0;
        this.avd = 0;
        this.f8606jy = 0;
        this.f8616ok = 0;
        this.ayg = 0;
        this.ayh = 0;
        this.ayi = 0;
        this.ayj = 0;
        this.ayk = 0;
        this.ayl = 0;
        this.aym = 0;
        this.ayn = 0;
        this.ayo = 0;
        this.ayp = 0;
        this.f8612nk = 0;
        this.f8604jY = 0;
        this.f8603jX = 0;
        this.ayq = 0;
        this.f8607ka = 0;
        this.f8614oL = 0;
        this.ayr = 0;
        this.ays = 0;
        this.ayt = 0;
        this.ayu = 0;
        this.avi = 0;
        this.f8599jK = 0;
        this.f8617on = 0;
        this.ayv = 0;
        this.ayw = 0;
        this.ayx = 0;
        this.ayy = 0;
        this.ayz = 0;
        this.ayA = 0;
        this.ayB = 0;
        this.ayC = 0;
        this.ayD = 0;
        this.ayE = 0;
        this.f8613nn = 0;
        this.f8609kd = 0;
        this.f8608kc = 0;
        this.ayF = 0;
        this.f8610kf = 0;
        this.f8615oS = 0;
    }
}
