package logic.data.mbean;

import game.network.message.serializable.C1556Wo;
import game.network.message.serializable.C3438ra;
import game.script.item.ItemType;
import game.script.newmarket.BuyOrder;
import game.script.newmarket.ListContainer;
import game.script.newmarket.SellOrder;
import game.script.newmarket.store.Store;
import game.script.ship.Station;
import logic.baa.C1616Xf;
import logic.res.code.C5663aRz;
import p001a.C0064Am;
import p001a.C0847MM;

import java.util.UUID;

/* renamed from: a.Wc */
public class C1539Wc extends C3805vD {
    @C0064Am(aul = "1200946c5cdb83247e2852418df88e2e", aum = 8)

    /* renamed from: bK */
    public UUID f2036bK;
    @C0064Am(aul = "e3fce79e603982e4e6e72fe99bc61d0f", aum = -2)
    public long bvI;
    @C0064Am(aul = "e3fce79e603982e4e6e72fe99bc61d0f", aum = -1)
    public byte bvP;
    @C0064Am(aul = "a09cb4ed9c01fc772a2c3459fbdfe8b7", aum = -2)
    public long cCP;
    @C0064Am(aul = "a09cb4ed9c01fc772a2c3459fbdfe8b7", aum = -1)
    public byte cCW;
    @C0064Am(aul = "44ea959afaf142a1c8a7cc61e03f29e7", aum = 7)
    public boolean ewA;
    @C0064Am(aul = "15d0572fcca3884681ef0e7711181b6b", aum = 10)
    public C1556Wo<C0847MM, ListContainer<BuyOrder>> ewB;
    @C0064Am(aul = "36ec59d7360195d01c95b322a140ab37", aum = 11)
    public ListContainer<BuyOrder> ewC;
    @C0064Am(aul = "e3fce79e603982e4e6e72fe99bc61d0f", aum = 12)
    public ListContainer<SellOrder> ewD;
    @C0064Am(aul = "a09cb4ed9c01fc772a2c3459fbdfe8b7", aum = 13)
    public Station ewE;
    @C0064Am(aul = "455161cdf4ff8301fb616a7645e0cc1a", aum = -2)
    public long ewF;
    @C0064Am(aul = "d8f0b8070b41be2abf7058413559ea0a", aum = -2)
    public long ewG;
    @C0064Am(aul = "03ccaaacd93698e4e05202082e631cd1", aum = -2)
    public long ewH;
    @C0064Am(aul = "221630fc0dd5f7462ea163868ad494e4", aum = -2)
    public long ewI;
    @C0064Am(aul = "a31678d86f84e93031ebe20a941e1214", aum = -2)
    public long ewJ;
    @C0064Am(aul = "55750e07dba7d621aa8689b9c89376e0", aum = -2)
    public long ewK;
    @C0064Am(aul = "c434e18426030733e82e928d1fadf0e6", aum = -2)
    public long ewL;
    @C0064Am(aul = "44ea959afaf142a1c8a7cc61e03f29e7", aum = -2)
    public long ewM;
    @C0064Am(aul = "15d0572fcca3884681ef0e7711181b6b", aum = -2)
    public long ewN;
    @C0064Am(aul = "36ec59d7360195d01c95b322a140ab37", aum = -2)
    public long ewO;
    @C0064Am(aul = "455161cdf4ff8301fb616a7645e0cc1a", aum = -1)
    public byte ewP;
    @C0064Am(aul = "d8f0b8070b41be2abf7058413559ea0a", aum = -1)
    public byte ewQ;
    @C0064Am(aul = "03ccaaacd93698e4e05202082e631cd1", aum = -1)
    public byte ewR;
    @C0064Am(aul = "221630fc0dd5f7462ea163868ad494e4", aum = -1)
    public byte ewS;
    @C0064Am(aul = "a31678d86f84e93031ebe20a941e1214", aum = -1)
    public byte ewT;
    @C0064Am(aul = "55750e07dba7d621aa8689b9c89376e0", aum = -1)
    public byte ewU;
    @C0064Am(aul = "c434e18426030733e82e928d1fadf0e6", aum = -1)
    public byte ewV;
    @C0064Am(aul = "44ea959afaf142a1c8a7cc61e03f29e7", aum = -1)
    public byte ewW;
    @C0064Am(aul = "15d0572fcca3884681ef0e7711181b6b", aum = -1)
    public byte ewX;
    @C0064Am(aul = "36ec59d7360195d01c95b322a140ab37", aum = -1)
    public byte ewY;
    @C0064Am(aul = "455161cdf4ff8301fb616a7645e0cc1a", aum = 0)
    public boolean ewt;
    @C0064Am(aul = "d8f0b8070b41be2abf7058413559ea0a", aum = 1)
    public C3438ra<ItemType> ewu;
    @C0064Am(aul = "03ccaaacd93698e4e05202082e631cd1", aum = 2)
    public C3438ra<ItemType> ewv;
    @C0064Am(aul = "221630fc0dd5f7462ea163868ad494e4", aum = 3)
    public C3438ra<ItemType> eww;
    @C0064Am(aul = "a31678d86f84e93031ebe20a941e1214", aum = 4)
    public C1556Wo<ItemType, Store.AnyBuyInfo> ewx;
    @C0064Am(aul = "55750e07dba7d621aa8689b9c89376e0", aum = 5)
    public C1556Wo<ItemType, Store.PostponedOrderTasklet> ewy;
    @C0064Am(aul = "c434e18426030733e82e928d1fadf0e6", aum = 6)
    public boolean ewz;
    @C0064Am(aul = "dbee64527c885d559cf2053031f2669d", aum = 9)
    public String handle;
    @C0064Am(aul = "1200946c5cdb83247e2852418df88e2e", aum = -2)

    /* renamed from: oL */
    public long f2037oL;
    @C0064Am(aul = "1200946c5cdb83247e2852418df88e2e", aum = -1)

    /* renamed from: oS */
    public byte f2038oS;
    @C0064Am(aul = "dbee64527c885d559cf2053031f2669d", aum = -2)

    /* renamed from: ok */
    public long f2039ok;
    @C0064Am(aul = "dbee64527c885d559cf2053031f2669d", aum = -1)

    /* renamed from: on */
    public byte f2040on;

    public C1539Wc() {
    }

    public C1539Wc(C1616Xf xf) {
        super(xf);
    }

    /* renamed from: c */
    public C5663aRz[] mo309c() {
        return Store._m_fields;
    }

    /* renamed from: d */
    public void mo310d() {
        super.mo310d();
        this.ewt = false;
        this.ewu = null;
        this.ewv = null;
        this.eww = null;
        this.ewx = null;
        this.ewy = null;
        this.ewz = false;
        this.ewA = false;
        this.f2036bK = null;
        this.handle = null;
        this.ewB = null;
        this.ewC = null;
        this.ewD = null;
        this.ewE = null;
        this.ewF = 0;
        this.ewG = 0;
        this.ewH = 0;
        this.ewI = 0;
        this.ewJ = 0;
        this.ewK = 0;
        this.ewL = 0;
        this.ewM = 0;
        this.f2037oL = 0;
        this.f2039ok = 0;
        this.ewN = 0;
        this.ewO = 0;
        this.bvI = 0;
        this.cCP = 0;
        this.ewP = 0;
        this.ewQ = 0;
        this.ewR = 0;
        this.ewS = 0;
        this.ewT = 0;
        this.ewU = 0;
        this.ewV = 0;
        this.ewW = 0;
        this.f2038oS = 0;
        this.f2040on = 0;
        this.ewX = 0;
        this.ewY = 0;
        this.bvP = 0;
        this.cCW = 0;
    }
}
