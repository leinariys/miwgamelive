package logic.data.mbean;

import game.script.mission.MissionTriggerType;
import game.script.resource.Asset;
import logic.baa.C1616Xf;
import logic.res.code.C5663aRz;
import p001a.C0064Am;
import taikodom.infra.script.I18NString;

import java.util.UUID;

/* renamed from: a.ari  reason: case insensitive filesystem */
public class C6686ari extends C5292aDs {
    @C0064Am(aul = "8fe4348811bac83c66e7befeb63cc972", aum = 1)

    /* renamed from: LQ */
    public Asset f5245LQ;
    @C0064Am(aul = "8fe4348811bac83c66e7befeb63cc972", aum = -2)
    public long ayc;
    @C0064Am(aul = "8fe4348811bac83c66e7befeb63cc972", aum = -1)
    public byte ayr;
    @C0064Am(aul = "d8b94225ec5cd98146d7f984f173c378", aum = 3)

    /* renamed from: bK */
    public UUID f5246bK;
    @C0064Am(aul = "9df326470159548896f68937c27ff297", aum = 2)
    public I18NString grv;
    @C0064Am(aul = "9df326470159548896f68937c27ff297", aum = -2)
    public long grw;
    @C0064Am(aul = "9df326470159548896f68937c27ff297", aum = -1)
    public byte grx;
    @C0064Am(aul = "0f4cdfcfa769c65b76c7d02981f660fb", aum = 0)
    public String handle;
    @C0064Am(aul = "d8b94225ec5cd98146d7f984f173c378", aum = -2)

    /* renamed from: oL */
    public long f5247oL;
    @C0064Am(aul = "d8b94225ec5cd98146d7f984f173c378", aum = -1)

    /* renamed from: oS */
    public byte f5248oS;
    @C0064Am(aul = "0f4cdfcfa769c65b76c7d02981f660fb", aum = -2)

    /* renamed from: ok */
    public long f5249ok;
    @C0064Am(aul = "0f4cdfcfa769c65b76c7d02981f660fb", aum = -1)

    /* renamed from: on */
    public byte f5250on;

    public C6686ari() {
    }

    public C6686ari(C1616Xf xf) {
        super(xf);
    }

    /* renamed from: c */
    public C5663aRz[] mo309c() {
        return MissionTriggerType._m_fields;
    }

    /* renamed from: d */
    public void mo310d() {
        super.mo310d();
        this.handle = null;
        this.f5245LQ = null;
        this.grv = null;
        this.f5246bK = null;
        this.f5249ok = 0;
        this.ayc = 0;
        this.grw = 0;
        this.f5247oL = 0;
        this.f5250on = 0;
        this.ayr = 0;
        this.grx = 0;
        this.f5248oS = 0;
    }
}
