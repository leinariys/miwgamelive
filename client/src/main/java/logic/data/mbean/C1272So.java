package logic.data.mbean;

import game.script.corporation.CorporationInvitationStatus;
import game.script.player.Player;
import logic.baa.C1616Xf;
import logic.res.code.C5663aRz;
import p001a.C0064Am;

/* renamed from: a.So */
public class C1272So extends C5292aDs {
    @C0064Am(aul = "a00ae8508d7629490cf1c4c181b6d689", aum = 0)

    /* renamed from: P */
    public Player f1572P;
    @C0064Am(aul = "a00ae8508d7629490cf1c4c181b6d689", aum = -2)

    /* renamed from: ag */
    public long f1573ag;
    @C0064Am(aul = "a00ae8508d7629490cf1c4c181b6d689", aum = -1)

    /* renamed from: av */
    public byte f1574av;
    @C0064Am(aul = "28742ec0b3454bbb9877fa404d843b95", aum = 1)
    public CorporationInvitationStatus.C1962a axc;
    @C0064Am(aul = "28742ec0b3454bbb9877fa404d843b95", aum = -2)
    public long cyP;
    @C0064Am(aul = "28742ec0b3454bbb9877fa404d843b95", aum = -1)
    public byte czp;

    public C1272So() {
    }

    public C1272So(C1616Xf xf) {
        super(xf);
    }

    /* renamed from: c */
    public C5663aRz[] mo309c() {
        return CorporationInvitationStatus._m_fields;
    }

    /* renamed from: d */
    public void mo310d() {
        super.mo310d();
        this.f1572P = null;
        this.axc = null;
        this.f1573ag = 0;
        this.cyP = 0;
        this.f1574av = 0;
        this.czp = 0;
    }
}
