package logic.data.mbean;

import game.script.resource.Asset;
import game.script.ship.SceneryType;
import logic.baa.C1616Xf;
import logic.res.code.C5663aRz;
import p001a.C0064Am;

/* renamed from: a.agK  reason: case insensitive filesystem */
public class C6090agK extends C6515aoT {
    @C0064Am(aul = "92b2d994518d21e78d6f74a5c31668ee", aum = 0)

    /* renamed from: LQ */
    public Asset f4514LQ;
    @C0064Am(aul = "db6a3e4e34adedadf6cbd7841919199e", aum = -2)
    public long avd;
    @C0064Am(aul = "db6a3e4e34adedadf6cbd7841919199e", aum = -1)
    public byte avi;
    @C0064Am(aul = "92b2d994518d21e78d6f74a5c31668ee", aum = -2)
    public long ayc;
    @C0064Am(aul = "92b2d994518d21e78d6f74a5c31668ee", aum = -1)
    public byte ayr;
    @C0064Am(aul = "dff5886b4db1f90e951e73ff3cc31213", aum = 2)
    public boolean enL;
    @C0064Am(aul = "dff5886b4db1f90e951e73ff3cc31213", aum = -2)
    public long fEm;
    @C0064Am(aul = "dff5886b4db1f90e951e73ff3cc31213", aum = -1)
    public byte fEn;
    @C0064Am(aul = "db6a3e4e34adedadf6cbd7841919199e", aum = 1)

    /* renamed from: uT */
    public String f4515uT;

    public C6090agK() {
    }

    public C6090agK(C1616Xf xf) {
        super(xf);
    }

    /* renamed from: c */
    public C5663aRz[] mo309c() {
        return SceneryType._m_fields;
    }

    /* renamed from: d */
    public void mo310d() {
        super.mo310d();
        this.f4514LQ = null;
        this.f4515uT = null;
        this.enL = false;
        this.ayc = 0;
        this.avd = 0;
        this.fEm = 0;
        this.ayr = 0;
        this.avi = 0;
        this.fEn = 0;
    }
}
