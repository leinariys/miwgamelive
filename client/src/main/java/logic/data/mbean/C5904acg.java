package logic.data.mbean;

import game.network.message.serializable.C1556Wo;
import game.network.message.serializable.C2686iZ;
import game.script.bank.BankAccount;
import game.script.corporation.Corporation;
import game.script.corporation.CorporationLogo;
import game.script.corporation.CorporationMembership;
import game.script.corporation.CorporationRoleInfo;
import game.script.player.Player;
import game.script.ship.Outpost;
import logic.baa.C1616Xf;
import logic.res.code.C5663aRz;
import p001a.C0064Am;
import p001a.C5741aUz;

/* renamed from: a.acg  reason: case insensitive filesystem */
public class C5904acg extends C3805vD {
    @C0064Am(aul = "8dc0162435255d85f31cd5be21912cbb", aum = -2)

    /* renamed from: Nf */
    public long f4264Nf;
    @C0064Am(aul = "8dc0162435255d85f31cd5be21912cbb", aum = -1)

    /* renamed from: Nh */
    public byte f4265Nh;
    @C0064Am(aul = "2998c75b712df5097fe66536ec818a53", aum = 3)
    public C1556Wo<Player, CorporationMembership> aJk;
    @C0064Am(aul = "2fc076d302c859a629604c63a630ec8d", aum = 4)
    public C1556Wo<C5741aUz, CorporationRoleInfo> aJm;
    @C0064Am(aul = "7489f2f8aa4e218ba6fd37fe4aefd095", aum = 5)
    public CorporationLogo aJo;
    @C0064Am(aul = "13d8adc2d97e15c54faba289b1fc0e4a", aum = 6)
    public String aJq;
    @C0064Am(aul = "d60f61f56ad4e6db23bd79c2ff97efbd", aum = 7)
    public BankAccount aJr;
    @C0064Am(aul = "c3d7cce26edbfc491971509b9f265442", aum = 8)
    public C2686iZ<Outpost> aJt;
    @C0064Am(aul = "9c459228bae19ecc10b720b05297bc18", aum = 9)
    public Corporation.PlayerDisposedListener aJv;
    @C0064Am(aul = "14bf994e536b7e115b8a101e6111e8bd", aum = 1)
    public String description;
    @C0064Am(aul = "abe0ec7adff0c681b2caa88325978fc4", aum = -2)
    public long dgZ;
    @C0064Am(aul = "abe0ec7adff0c681b2caa88325978fc4", aum = -1)
    public byte dhd;
    @C0064Am(aul = "2998c75b712df5097fe66536ec818a53", aum = -2)
    public long faC;
    @C0064Am(aul = "2fc076d302c859a629604c63a630ec8d", aum = -2)
    public long faD;
    @C0064Am(aul = "7489f2f8aa4e218ba6fd37fe4aefd095", aum = -2)
    public long faE;
    @C0064Am(aul = "13d8adc2d97e15c54faba289b1fc0e4a", aum = -2)
    public long faF;
    @C0064Am(aul = "d60f61f56ad4e6db23bd79c2ff97efbd", aum = -2)
    public long faG;
    @C0064Am(aul = "c3d7cce26edbfc491971509b9f265442", aum = -2)
    public long faH;
    @C0064Am(aul = "9c459228bae19ecc10b720b05297bc18", aum = -2)
    public long faI;
    @C0064Am(aul = "2998c75b712df5097fe66536ec818a53", aum = -1)
    public byte faJ;
    @C0064Am(aul = "2fc076d302c859a629604c63a630ec8d", aum = -1)
    public byte faK;
    @C0064Am(aul = "7489f2f8aa4e218ba6fd37fe4aefd095", aum = -1)
    public byte faL;
    @C0064Am(aul = "13d8adc2d97e15c54faba289b1fc0e4a", aum = -1)
    public byte faM;
    @C0064Am(aul = "d60f61f56ad4e6db23bd79c2ff97efbd", aum = -1)
    public byte faN;
    @C0064Am(aul = "c3d7cce26edbfc491971509b9f265442", aum = -1)
    public byte faO;
    @C0064Am(aul = "9c459228bae19ecc10b720b05297bc18", aum = -1)
    public byte faP;
    @C0064Am(aul = "abe0ec7adff0c681b2caa88325978fc4", aum = 2)

    /* renamed from: mu */
    public Player f4266mu;
    @C0064Am(aul = "8dc0162435255d85f31cd5be21912cbb", aum = 0)
    public String name;
    @C0064Am(aul = "14bf994e536b7e115b8a101e6111e8bd", aum = -2)

    /* renamed from: nk */
    public long f4267nk;
    @C0064Am(aul = "14bf994e536b7e115b8a101e6111e8bd", aum = -1)

    /* renamed from: nn */
    public byte f4268nn;

    public C5904acg() {
    }

    public C5904acg(C1616Xf xf) {
        super(xf);
    }

    /* renamed from: c */
    public C5663aRz[] mo309c() {
        return Corporation._m_fields;
    }

    /* renamed from: d */
    public void mo310d() {
        super.mo310d();
        this.name = null;
        this.description = null;
        this.f4266mu = null;
        this.aJk = null;
        this.aJm = null;
        this.aJo = null;
        this.aJq = null;
        this.aJr = null;
        this.aJt = null;
        this.aJv = null;
        this.f4264Nf = 0;
        this.f4267nk = 0;
        this.dgZ = 0;
        this.faC = 0;
        this.faD = 0;
        this.faE = 0;
        this.faF = 0;
        this.faG = 0;
        this.faH = 0;
        this.faI = 0;
        this.f4265Nh = 0;
        this.f4268nn = 0;
        this.dhd = 0;
        this.faJ = 0;
        this.faK = 0;
        this.faL = 0;
        this.faM = 0;
        this.faN = 0;
        this.faO = 0;
        this.faP = 0;
    }
}
