package logic.data.mbean;

import game.script.Actor;
import game.script.player.Player;
import game.script.space.Node;
import logic.baa.C1616Xf;
import logic.res.code.C5663aRz;
import p001a.C0064Am;

/* renamed from: a.aKy  reason: case insensitive filesystem */
public class C5480aKy extends C2272dV {
    @C0064Am(aul = "9d5a50210ebb737266b0441da9a2d3b4", aum = 3)

    /* renamed from: P */
    public Player f3268P;
    @C0064Am(aul = "6b10335f32b8bbfe548286ab72710eb8", aum = 2)
    public Node aSh;
    @C0064Am(aul = "9d5a50210ebb737266b0441da9a2d3b4", aum = -2)

    /* renamed from: ag */
    public long f3269ag;
    @C0064Am(aul = "9d5a50210ebb737266b0441da9a2d3b4", aum = -1)

    /* renamed from: av */
    public byte f3270av;
    @C0064Am(aul = "6b10335f32b8bbfe548286ab72710eb8", aum = -2)
    public long bec;
    @C0064Am(aul = "6b10335f32b8bbfe548286ab72710eb8", aum = -1)
    public byte bei;
    @C0064Am(aul = "e0f480d9e616b3f406a40316ee7f1449", aum = 0)
    public boolean hmY;
    @C0064Am(aul = "9a173376cd982cbc2c205ac4e20f7678", aum = 4)
    public boolean hnA;
    @C0064Am(aul = "9e5deb0b7a8f45a429f32cf09c596ab1", aum = 1)
    public Actor hnk;
    @C0064Am(aul = "e0f480d9e616b3f406a40316ee7f1449", aum = -2)
    public long iin;
    @C0064Am(aul = "9e5deb0b7a8f45a429f32cf09c596ab1", aum = -2)
    public long iio;
    @C0064Am(aul = "9a173376cd982cbc2c205ac4e20f7678", aum = -2)
    public long iip;
    @C0064Am(aul = "e0f480d9e616b3f406a40316ee7f1449", aum = -1)
    public byte iiq;
    @C0064Am(aul = "9e5deb0b7a8f45a429f32cf09c596ab1", aum = -1)
    public byte iir;
    @C0064Am(aul = "9a173376cd982cbc2c205ac4e20f7678", aum = -1)
    public byte iis;

    public C5480aKy() {
    }

    public C5480aKy(C1616Xf xf) {
        super(xf);
    }

    /* renamed from: c */
    public C5663aRz[] mo309c() {
        return PlayerController._m_fields;
    }

    /* renamed from: d */
    public void mo310d() {
        super.mo310d();
        this.hmY = false;
        this.hnk = null;
        this.aSh = null;
        this.f3268P = null;
        this.hnA = false;
        this.iin = 0;
        this.iio = 0;
        this.bec = 0;
        this.f3269ag = 0;
        this.iip = 0;
        this.iiq = 0;
        this.iir = 0;
        this.bei = 0;
        this.f3270av = 0;
        this.iis = 0;
    }
}
