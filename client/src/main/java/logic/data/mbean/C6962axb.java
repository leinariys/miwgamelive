package logic.data.mbean;

import game.network.message.serializable.C3438ra;
import game.script.item.ItemType;
import game.script.mission.TableNPCSpawn;
import game.script.missiontemplate.WaypointDat;
import game.script.missiontemplate.scripting.LootItemMissionScriptTemplate;
import game.script.space.StellarSystem;
import logic.baa.C1616Xf;
import logic.res.code.C5663aRz;
import p001a.C0064Am;

/* renamed from: a.axb  reason: case insensitive filesystem */
public class C6962axb extends C0597IR {
    @C0064Am(aul = "fb3aa6b66ffd584ea303c5065a6852e1", aum = 2)
    public ItemType dhK;
    @C0064Am(aul = "fb3aa6b66ffd584ea303c5065a6852e1", aum = -2)
    public long dhP;
    @C0064Am(aul = "fb3aa6b66ffd584ea303c5065a6852e1", aum = -1)
    public byte dhU;
    @C0064Am(aul = "e1359e93e39944429586279491c8cb4f", aum = 0)
    public TableNPCSpawn ebl;
    @C0064Am(aul = "14cee2ad98cd01cc5eca3aae63edb56c", aum = 3)
    public WaypointDat ebo;
    @C0064Am(aul = "5f29b7de0d78fe422d1f0f8ecdad6ab3", aum = 4)
    public boolean ebq;
    @C0064Am(aul = "641c287848f5e38371a41cd4d6158f85", aum = 5)
    public String ebs;
    @C0064Am(aul = "0bd59e4fcd1a45be72bda034b12ce886", aum = 6)
    public ItemType ebu;
    @C0064Am(aul = "5b5d7e4c6a153d17bfbb5a74fb2a27d1", aum = 7)
    public C3438ra<WaypointDat> ebw;
    @C0064Am(aul = "641c287848f5e38371a41cd4d6158f85", aum = -2)
    public long gCE;
    @C0064Am(aul = "14cee2ad98cd01cc5eca3aae63edb56c", aum = -2)
    public long gCF;
    @C0064Am(aul = "641c287848f5e38371a41cd4d6158f85", aum = -1)
    public byte gCG;
    @C0064Am(aul = "14cee2ad98cd01cc5eca3aae63edb56c", aum = -1)
    public byte gCH;
    @C0064Am(aul = "5f29b7de0d78fe422d1f0f8ecdad6ab3", aum = -2)
    public long gOA;
    @C0064Am(aul = "0bd59e4fcd1a45be72bda034b12ce886", aum = -2)
    public long gOB;
    @C0064Am(aul = "5b5d7e4c6a153d17bfbb5a74fb2a27d1", aum = -2)
    public long gOC;
    @C0064Am(aul = "e1359e93e39944429586279491c8cb4f", aum = -1)
    public byte gOD;
    @C0064Am(aul = "5f29b7de0d78fe422d1f0f8ecdad6ab3", aum = -1)
    public byte gOE;
    @C0064Am(aul = "0bd59e4fcd1a45be72bda034b12ce886", aum = -1)
    public byte gOF;
    @C0064Am(aul = "5b5d7e4c6a153d17bfbb5a74fb2a27d1", aum = -1)
    public byte gOG;
    @C0064Am(aul = "e1359e93e39944429586279491c8cb4f", aum = -2)
    public long gOz;
    @C0064Am(aul = "7738750c7e38b25acba958d70d882225", aum = 1)

    /* renamed from: rI */
    public StellarSystem f5568rI;
    @C0064Am(aul = "7738750c7e38b25acba958d70d882225", aum = -2)

    /* renamed from: rY */
    public long f5569rY;
    @C0064Am(aul = "7738750c7e38b25acba958d70d882225", aum = -1)

    /* renamed from: so */
    public byte f5570so;

    public C6962axb() {
    }

    public C6962axb(C1616Xf xf) {
        super(xf);
    }

    /* renamed from: c */
    public C5663aRz[] mo309c() {
        return LootItemMissionScriptTemplate._m_fields;
    }

    /* renamed from: d */
    public void mo310d() {
        super.mo310d();
        this.ebl = null;
        this.f5568rI = null;
        this.dhK = null;
        this.ebo = null;
        this.ebq = false;
        this.ebs = null;
        this.ebu = null;
        this.ebw = null;
        this.gOz = 0;
        this.f5569rY = 0;
        this.dhP = 0;
        this.gCF = 0;
        this.gOA = 0;
        this.gCE = 0;
        this.gOB = 0;
        this.gOC = 0;
        this.gOD = 0;
        this.f5570so = 0;
        this.dhU = 0;
        this.gCH = 0;
        this.gOE = 0;
        this.gCG = 0;
        this.gOF = 0;
        this.gOG = 0;
    }
}
