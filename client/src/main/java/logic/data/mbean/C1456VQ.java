package logic.data.mbean;

import game.script.ship.SectorCategory;
import game.script.ship.ShipSectorType;
import logic.baa.C1616Xf;
import logic.res.code.C5663aRz;
import p001a.C0064Am;

/* renamed from: a.VQ */
public class C1456VQ extends aJZ {
    @C0064Am(aul = "d318f239e395d72d63815ee7017e819a", aum = 0)
    public int daj;
    @C0064Am(aul = "bce51736cf657cd6d8a796f03cc97ecb", aum = 1)
    public boolean dak;
    @C0064Am(aul = "e56837e8be7b024e420ac5ba3481ca21", aum = 2)
    public int dal;
    @C0064Am(aul = "9f617ad5fbbc6f0fbdbdf93986f5a967", aum = 3)
    public SectorCategory dam;
    @C0064Am(aul = "d318f239e395d72d63815ee7017e819a", aum = -2)
    public long dap;
    @C0064Am(aul = "bce51736cf657cd6d8a796f03cc97ecb", aum = -2)
    public long daq;
    @C0064Am(aul = "e56837e8be7b024e420ac5ba3481ca21", aum = -2)
    public long dar;
    @C0064Am(aul = "9f617ad5fbbc6f0fbdbdf93986f5a967", aum = -2)
    public long das;
    @C0064Am(aul = "d318f239e395d72d63815ee7017e819a", aum = -1)
    public byte dav;
    @C0064Am(aul = "bce51736cf657cd6d8a796f03cc97ecb", aum = -1)
    public byte daw;
    @C0064Am(aul = "e56837e8be7b024e420ac5ba3481ca21", aum = -1)
    public byte dax;
    @C0064Am(aul = "9f617ad5fbbc6f0fbdbdf93986f5a967", aum = -1)
    public byte day;

    public C1456VQ() {
    }

    public C1456VQ(C1616Xf xf) {
        super(xf);
    }

    /* renamed from: c */
    public C5663aRz[] mo309c() {
        return ShipSectorType._m_fields;
    }

    /* renamed from: d */
    public void mo310d() {
        super.mo310d();
        this.daj = 0;
        this.dak = false;
        this.dal = 0;
        this.dam = null;
        this.dap = 0;
        this.daq = 0;
        this.dar = 0;
        this.das = 0;
        this.dav = 0;
        this.daw = 0;
        this.dax = 0;
        this.day = 0;
    }
}
