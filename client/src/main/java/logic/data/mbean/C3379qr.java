package logic.data.mbean;

import game.script.item.ClipBox;
import game.script.item.ClipType;
import logic.baa.C1616Xf;
import logic.res.code.C5663aRz;
import p001a.C0064Am;

/* renamed from: a.qr */
public class C3379qr extends aJB {
    @C0064Am(aul = "ef2ccc2974662dd9b9aff35a595228f6", aum = 0)
    public int aVH;
    @C0064Am(aul = "97a7567579281d60577e9afccc676f99", aum = 1)
    public ClipType aVI;
    @C0064Am(aul = "ef2ccc2974662dd9b9aff35a595228f6", aum = -2)
    public long aVJ;
    @C0064Am(aul = "97a7567579281d60577e9afccc676f99", aum = -2)
    public long aVK;
    @C0064Am(aul = "ef2ccc2974662dd9b9aff35a595228f6", aum = -1)
    public byte aVL;
    @C0064Am(aul = "97a7567579281d60577e9afccc676f99", aum = -1)
    public byte aVM;

    public C3379qr() {
    }

    public C3379qr(C1616Xf xf) {
        super(xf);
    }

    /* renamed from: c */
    public C5663aRz[] mo309c() {
        return ClipBox._m_fields;
    }

    /* renamed from: d */
    public void mo310d() {
        super.mo310d();
        this.aVH = 0;
        this.aVI = null;
        this.aVJ = 0;
        this.aVK = 0;
        this.aVL = 0;
        this.aVM = 0;
    }
}
