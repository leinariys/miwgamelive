package logic.data.mbean;

import game.network.message.serializable.C3438ra;
import game.script.npcchat.requirement.ProgressionCareerSpeechRequeriment;
import game.script.progression.ProgressionCareer;
import logic.baa.C1616Xf;
import logic.res.code.C5663aRz;
import p001a.C0064Am;

/* renamed from: a.alb  reason: case insensitive filesystem */
public class C6367alb extends C5416aIm {
    @C0064Am(aul = "ff5146a25098dc671ba9defb35207fa3", aum = -2)
    public long aZX;
    @C0064Am(aul = "ff5146a25098dc671ba9defb35207fa3", aum = -1)
    public byte bac;
    @C0064Am(aul = "ff5146a25098dc671ba9defb35207fa3", aum = 0)

    /* renamed from: dt */
    public C3438ra<ProgressionCareer> f4874dt;

    public C6367alb() {
    }

    public C6367alb(C1616Xf xf) {
        super(xf);
    }

    /* renamed from: c */
    public C5663aRz[] mo309c() {
        return ProgressionCareerSpeechRequeriment._m_fields;
    }

    /* renamed from: d */
    public void mo310d() {
        super.mo310d();
        this.f4874dt = null;
        this.aZX = 0;
        this.bac = 0;
    }
}
