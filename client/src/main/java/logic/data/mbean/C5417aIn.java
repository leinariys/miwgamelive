package logic.data.mbean;

import game.script.simulation.SurfaceDefaults;
import logic.baa.C1616Xf;
import logic.res.code.C5663aRz;
import p001a.C0064Am;

import java.util.UUID;

/* renamed from: a.aIn  reason: case insensitive filesystem */
public class C5417aIn extends C3805vD {
    @C0064Am(aul = "e97bf248bec55e7b004ca3247641dc44", aum = 6)

    /* renamed from: bK */
    public UUID f3103bK;
    @C0064Am(aul = "7062d08f6f1e26ccff69458127f146a2", aum = 0)
    public String biW;
    @C0064Am(aul = "1fa1d107e4bff5e847607065652573fe", aum = 1)
    public String biY;
    @C0064Am(aul = "ac3bf38b150820c0a3178ad4a38cff35", aum = 2)
    public String bja;
    @C0064Am(aul = "14a147b6d2c1d52eb8f77ea5dbaabbd7", aum = 3)
    public String bjc;
    @C0064Am(aul = "de3889ee2114be99876b00afd02720ca", aum = 4)
    public String bje;
    @C0064Am(aul = "1fd8f2825f6825350abb055f42848038", aum = 5)
    public String bjg;
    @C0064Am(aul = "7062d08f6f1e26ccff69458127f146a2", aum = -2)
    public long ibJ;
    @C0064Am(aul = "1fa1d107e4bff5e847607065652573fe", aum = -2)
    public long ibK;
    @C0064Am(aul = "ac3bf38b150820c0a3178ad4a38cff35", aum = -2)
    public long ibL;
    @C0064Am(aul = "14a147b6d2c1d52eb8f77ea5dbaabbd7", aum = -2)
    public long ibM;
    @C0064Am(aul = "de3889ee2114be99876b00afd02720ca", aum = -2)
    public long ibN;
    @C0064Am(aul = "1fd8f2825f6825350abb055f42848038", aum = -2)
    public long ibO;
    @C0064Am(aul = "7062d08f6f1e26ccff69458127f146a2", aum = -1)
    public byte ibP;
    @C0064Am(aul = "1fa1d107e4bff5e847607065652573fe", aum = -1)
    public byte ibQ;
    @C0064Am(aul = "ac3bf38b150820c0a3178ad4a38cff35", aum = -1)
    public byte ibR;
    @C0064Am(aul = "14a147b6d2c1d52eb8f77ea5dbaabbd7", aum = -1)
    public byte ibS;
    @C0064Am(aul = "de3889ee2114be99876b00afd02720ca", aum = -1)
    public byte ibT;
    @C0064Am(aul = "1fd8f2825f6825350abb055f42848038", aum = -1)
    public byte ibU;
    @C0064Am(aul = "e97bf248bec55e7b004ca3247641dc44", aum = -2)

    /* renamed from: oL */
    public long f3104oL;
    @C0064Am(aul = "e97bf248bec55e7b004ca3247641dc44", aum = -1)

    /* renamed from: oS */
    public byte f3105oS;

    public C5417aIn() {
    }

    public C5417aIn(C1616Xf xf) {
        super(xf);
    }

    /* renamed from: c */
    public C5663aRz[] mo309c() {
        return SurfaceDefaults._m_fields;
    }

    /* renamed from: d */
    public void mo310d() {
        super.mo310d();
        this.biW = null;
        this.biY = null;
        this.bja = null;
        this.bjc = null;
        this.bje = null;
        this.bjg = null;
        this.f3103bK = null;
        this.ibJ = 0;
        this.ibK = 0;
        this.ibL = 0;
        this.ibM = 0;
        this.ibN = 0;
        this.ibO = 0;
        this.f3104oL = 0;
        this.ibP = 0;
        this.ibQ = 0;
        this.ibR = 0;
        this.ibS = 0;
        this.ibT = 0;
        this.ibU = 0;
        this.f3105oS = 0;
    }
}
