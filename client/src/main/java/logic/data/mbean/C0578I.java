package logic.data.mbean;

import game.network.message.externalizable.C3131oI;
import game.script.npcchat.actions.NurseryTutorialSpeechAction;
import logic.baa.C1616Xf;
import logic.res.code.C5663aRz;
import p001a.C0064Am;

/* renamed from: a.I */
public class C0578I extends C6352alM {
    @C0064Am(aul = "e33081d93d2d04d055dd929ce00a56d1", aum = 0)

    /* renamed from: dB */
    public C3131oI.C3132a f696dB;
    @C0064Am(aul = "e33081d93d2d04d055dd929ce00a56d1", aum = -2)

    /* renamed from: dC */
    public long f697dC;
    @C0064Am(aul = "e33081d93d2d04d055dd929ce00a56d1", aum = -1)

    /* renamed from: dD */
    public byte f698dD;

    public C0578I() {
    }

    public C0578I(C1616Xf xf) {
        super(xf);
    }

    /* renamed from: c */
    public C5663aRz[] mo309c() {
        return NurseryTutorialSpeechAction._m_fields;
    }

    /* renamed from: d */
    public void mo310d() {
        super.mo310d();
        this.f696dB = null;
        this.f697dC = 0;
        this.f698dD = 0;
    }
}
