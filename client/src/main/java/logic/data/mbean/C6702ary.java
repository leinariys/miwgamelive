package logic.data.mbean;

import game.network.message.serializable.C3438ra;
import game.script.missiontemplate.PositionDat;
import game.script.missiontemplate.scripting.KillingNpcMissionScriptTemplate;
import game.script.npc.NPCType;
import game.script.space.StellarSystem;
import logic.baa.C1616Xf;
import logic.res.code.C5663aRz;
import p001a.C0064Am;

/* renamed from: a.ary  reason: case insensitive filesystem */
public class C6702ary extends C0597IR {
    @C0064Am(aul = "78580882799a4244f3a78ebe86d5eeb7", aum = 0)
    public C3438ra<NPCType> aLD;
    @C0064Am(aul = "fc367570b91f0bb950f7a0426036d143", aum = 4)
    public boolean aLE;
    @C0064Am(aul = "78580882799a4244f3a78ebe86d5eeb7", aum = -2)
    public long aLI;
    @C0064Am(aul = "fc367570b91f0bb950f7a0426036d143", aum = -2)
    public long aLJ;
    @C0064Am(aul = "78580882799a4244f3a78ebe86d5eeb7", aum = -1)
    public byte aLN;
    @C0064Am(aul = "fc367570b91f0bb950f7a0426036d143", aum = -1)
    public byte aLO;
    @C0064Am(aul = "15d50d5d7b64099b6586f00d95670e12", aum = 1)
    public int bCc;
    @C0064Am(aul = "814554636bfd6c6eaa926a753a1442fb", aum = 3)
    public C3438ra<PositionDat> bCd;
    @C0064Am(aul = "15d50d5d7b64099b6586f00d95670e12", aum = -2)
    public long gsi;
    @C0064Am(aul = "814554636bfd6c6eaa926a753a1442fb", aum = -2)
    public long gsj;
    @C0064Am(aul = "15d50d5d7b64099b6586f00d95670e12", aum = -1)
    public byte gsk;
    @C0064Am(aul = "814554636bfd6c6eaa926a753a1442fb", aum = -1)
    public byte gsl;
    @C0064Am(aul = "85cbfeda096819afcfb7f544b32815fe", aum = 2)

    /* renamed from: rI */
    public StellarSystem f5269rI;
    @C0064Am(aul = "85cbfeda096819afcfb7f544b32815fe", aum = -2)

    /* renamed from: rY */
    public long f5270rY;
    @C0064Am(aul = "85cbfeda096819afcfb7f544b32815fe", aum = -1)

    /* renamed from: so */
    public byte f5271so;

    public C6702ary() {
    }

    public C6702ary(C1616Xf xf) {
        super(xf);
    }

    /* renamed from: c */
    public C5663aRz[] mo309c() {
        return KillingNpcMissionScriptTemplate._m_fields;
    }

    /* renamed from: d */
    public void mo310d() {
        super.mo310d();
        this.aLD = null;
        this.bCc = 0;
        this.f5269rI = null;
        this.bCd = null;
        this.aLE = false;
        this.aLI = 0;
        this.gsi = 0;
        this.f5270rY = 0;
        this.gsj = 0;
        this.aLJ = 0;
        this.aLN = 0;
        this.gsk = 0;
        this.f5271so = 0;
        this.gsl = 0;
        this.aLO = 0;
    }
}
