package logic.data.mbean;

import game.script.item.ItemType;
import game.script.mission.actions.GiveItemMissionAction;
import logic.baa.C1616Xf;
import logic.res.code.C5663aRz;
import p001a.C0064Am;

/* renamed from: a.AX */
public class C0040AX extends C2503fv {
    @C0064Am(aul = "296cf3495062c80b8f94c2efdc6bd805", aum = 0)
    public ItemType aMW;
    @C0064Am(aul = "494708f3c6e4242596d3aa0e05e3fd5e", aum = 1)
    public int aMY;
    @C0064Am(aul = "2433fdf45b28b759b97841246799637f", aum = 2)
    public boolean aNa;
    @C0064Am(aul = "494708f3c6e4242596d3aa0e05e3fd5e", aum = -1)
    public byte ckA;
    @C0064Am(aul = "2433fdf45b28b759b97841246799637f", aum = -1)
    public byte ckB;
    @C0064Am(aul = "296cf3495062c80b8f94c2efdc6bd805", aum = -2)
    public long ckw;
    @C0064Am(aul = "494708f3c6e4242596d3aa0e05e3fd5e", aum = -2)
    public long ckx;
    @C0064Am(aul = "2433fdf45b28b759b97841246799637f", aum = -2)
    public long cky;
    @C0064Am(aul = "296cf3495062c80b8f94c2efdc6bd805", aum = -1)
    public byte ckz;

    public C0040AX() {
    }

    public C0040AX(C1616Xf xf) {
        super(xf);
    }

    /* renamed from: c */
    public C5663aRz[] mo309c() {
        return GiveItemMissionAction._m_fields;
    }

    /* renamed from: d */
    public void mo310d() {
        super.mo310d();
        this.aMW = null;
        this.aMY = 0;
        this.aNa = false;
        this.ckw = 0;
        this.ckx = 0;
        this.cky = 0;
        this.ckz = 0;
        this.ckA = 0;
        this.ckB = 0;
    }
}
