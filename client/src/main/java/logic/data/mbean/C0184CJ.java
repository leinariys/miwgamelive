package logic.data.mbean;

import game.script.ship.Hull;
import logic.baa.C1616Xf;
import logic.res.code.C5663aRz;
import p001a.C0064Am;

/* renamed from: a.CJ */
public class C0184CJ extends aEW {
    @C0064Am(aul = "243b936efffd0f90ee37193247386280", aum = 0)
    public Hull apB;
    @C0064Am(aul = "243b936efffd0f90ee37193247386280", aum = -2)

    /* renamed from: dl */
    public long f280dl;
    @C0064Am(aul = "243b936efffd0f90ee37193247386280", aum = -1)

    /* renamed from: ds */
    public byte f281ds;

    public C0184CJ() {
    }

    public C0184CJ(C1616Xf xf) {
        super(xf);
    }

    /* renamed from: c */
    public C5663aRz[] mo309c() {
        return Hull.BaseAdapter._m_fields;
    }

    /* renamed from: d */
    public void mo310d() {
        super.mo310d();
        this.apB = null;
        this.f280dl = 0;
        this.f281ds = 0;
    }
}
