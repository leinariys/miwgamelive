package logic.data.mbean;

import game.network.message.serializable.C1556Wo;
import game.script.corporation.CorporationRoleInfo;
import game.script.resource.Asset;
import logic.baa.C1616Xf;
import logic.res.code.C5663aRz;
import p001a.C0064Am;
import p001a.C5741aUz;
import p001a.C6704asA;

/* renamed from: a.IX */
public class C0603IX extends C3805vD {
    @C0064Am(aul = "983aa28baf9096e9ce47d1adc40d2a1b", aum = -2)
    public long dhA;
    @C0064Am(aul = "956ac6394e685fb0736db48fb9715515", aum = -2)
    public long dhB;
    @C0064Am(aul = "e2d98fbe7521c97c307de5a9deff27d6", aum = -1)
    public byte dhC;
    @C0064Am(aul = "a1e735011615b30fb4e7f2c4cc3cdb52", aum = -1)
    public byte dhD;
    @C0064Am(aul = "983aa28baf9096e9ce47d1adc40d2a1b", aum = -1)
    public byte dhE;
    @C0064Am(aul = "956ac6394e685fb0736db48fb9715515", aum = -1)
    public byte dhF;
    @C0064Am(aul = "e2d98fbe7521c97c307de5a9deff27d6", aum = 0)
    public C5741aUz dhu;
    @C0064Am(aul = "a1e735011615b30fb4e7f2c4cc3cdb52", aum = 1)
    public C1556Wo<C6704asA, Boolean> dhv;
    @C0064Am(aul = "983aa28baf9096e9ce47d1adc40d2a1b", aum = 2)
    public String dhw;
    @C0064Am(aul = "956ac6394e685fb0736db48fb9715515", aum = 3)
    public String dhx;
    @C0064Am(aul = "e2d98fbe7521c97c307de5a9deff27d6", aum = -2)
    public long dhy;
    @C0064Am(aul = "a1e735011615b30fb4e7f2c4cc3cdb52", aum = -2)
    public long dhz;
    @C0064Am(aul = "f52271b4369675afc494b8f8379f88d0", aum = -1)

    /* renamed from: jK */
    public byte f719jK;
    @C0064Am(aul = "f52271b4369675afc494b8f8379f88d0", aum = 4)

    /* renamed from: jn */
    public Asset f720jn;
    @C0064Am(aul = "f52271b4369675afc494b8f8379f88d0", aum = -2)

    /* renamed from: jy */
    public long f721jy;

    public C0603IX() {
    }

    public C0603IX(C1616Xf xf) {
        super(xf);
    }

    /* renamed from: c */
    public C5663aRz[] mo309c() {
        return CorporationRoleInfo._m_fields;
    }

    /* renamed from: d */
    public void mo310d() {
        super.mo310d();
        this.dhu = null;
        this.dhv = null;
        this.dhw = null;
        this.dhx = null;
        this.f720jn = null;
        this.dhy = 0;
        this.dhz = 0;
        this.dhA = 0;
        this.dhB = 0;
        this.f721jy = 0;
        this.dhC = 0;
        this.dhD = 0;
        this.dhE = 0;
        this.dhF = 0;
        this.f719jK = 0;
    }
}
