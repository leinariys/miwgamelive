package logic.data.mbean;

import game.script.mission.scripting.KillingNpcMissionScript;
import logic.baa.C1616Xf;
import logic.res.code.C5663aRz;
import p001a.C0064Am;

/* renamed from: a.akW  reason: case insensitive filesystem */
public class C6310akW extends C2955mD {
    @C0064Am(aul = "8950200f8342a95047c8801fcff17eda", aum = -2)

    /* renamed from: dl */
    public long f4787dl;
    @C0064Am(aul = "8950200f8342a95047c8801fcff17eda", aum = -1)

    /* renamed from: ds */
    public byte f4788ds;
    @C0064Am(aul = "8950200f8342a95047c8801fcff17eda", aum = 0)
    public KillingNpcMissionScript fUT;

    public C6310akW() {
    }

    public C6310akW(C1616Xf xf) {
        super(xf);
    }

    /* renamed from: c */
    public C5663aRz[] mo309c() {
        return KillingNpcMissionScript.StateA._m_fields;
    }

    /* renamed from: d */
    public void mo310d() {
        super.mo310d();
        this.fUT = null;
        this.f4787dl = 0;
        this.f4788ds = 0;
    }
}
