package logic.data.mbean;

import com.hoplon.geometry.Vec3f;
import game.network.message.serializable.C2686iZ;
import game.network.message.serializable.C3438ra;
import game.script.Actor;
import game.script.pda.DatabaseCategory;
import game.script.pda.TaikopediaEntry;
import game.script.resource.Asset;
import game.script.space.Node;
import game.script.space.StellarSystem;
import logic.baa.C1616Xf;
import logic.res.code.C5663aRz;
import p001a.C0064Am;
import taikodom.infra.script.I18NString;

import java.util.UUID;

/* renamed from: a.aIR */
public class aIR extends C3805vD {
    @C0064Am(aul = "cf0653ea8764e7e7daa5bc49454b1d76", aum = -2)

    /* renamed from: Nf */
    public long f3055Nf;
    @C0064Am(aul = "cf0653ea8764e7e7daa5bc49454b1d76", aum = -1)

    /* renamed from: Nh */
    public byte f3056Nh;
    @C0064Am(aul = "4d0a7d05dbfdd4329aae562c57f8777f", aum = 11)

    /* renamed from: bK */
    public UUID f3057bK;
    @C0064Am(aul = "6185197333bcd516dd9a325e05f1fabe", aum = 0)
    public C2686iZ<Actor> diS;
    @C0064Am(aul = "1ae4ad5225aefe91cb50aeacaba4bfc4", aum = 8)
    public C3438ra<Node> diU;
    @C0064Am(aul = "3b021bb7d61c4c6d886b8fa9448a43c1", aum = 7)
    public String handle;
    @C0064Am(aul = "6185197333bcd516dd9a325e05f1fabe", aum = -2)
    public long idn;
    @C0064Am(aul = "1ae4ad5225aefe91cb50aeacaba4bfc4", aum = -2)
    public long ido;
    @C0064Am(aul = "6185197333bcd516dd9a325e05f1fabe", aum = -1)
    public byte idp;
    @C0064Am(aul = "1ae4ad5225aefe91cb50aeacaba4bfc4", aum = -1)
    public byte idq;
    @C0064Am(aul = "9b72275a2aa520fbb9a9cf37ec811b52", aum = -1)

    /* renamed from: jK */
    public byte f3058jK;
    @C0064Am(aul = "c1b3de45147a9764f07138f9e02bf550", aum = 2)

    /* renamed from: jS */
    public DatabaseCategory f3059jS;
    @C0064Am(aul = "6567c00e58a87d4c27abae20c5438c47", aum = 3)

    /* renamed from: jT */
    public C3438ra<TaikopediaEntry> f3060jT;
    @C0064Am(aul = "6f2d82f4152f7e69c7282fdf04048aac", aum = 6)

    /* renamed from: jU */
    public Asset f3061jU;
    @C0064Am(aul = "d116df0429b121c3465bf41e174a1c0e", aum = 10)

    /* renamed from: jV */
    public float f3062jV;
    @C0064Am(aul = "c1b3de45147a9764f07138f9e02bf550", aum = -2)

    /* renamed from: jX */
    public long f3063jX;
    @C0064Am(aul = "6567c00e58a87d4c27abae20c5438c47", aum = -2)

    /* renamed from: jY */
    public long f3064jY;
    @C0064Am(aul = "6f2d82f4152f7e69c7282fdf04048aac", aum = -2)

    /* renamed from: jZ */
    public long f3065jZ;
    @C0064Am(aul = "9b72275a2aa520fbb9a9cf37ec811b52", aum = 5)

    /* renamed from: jn */
    public Asset f3066jn;
    @C0064Am(aul = "9b72275a2aa520fbb9a9cf37ec811b52", aum = -2)

    /* renamed from: jy */
    public long f3067jy;
    @C0064Am(aul = "d116df0429b121c3465bf41e174a1c0e", aum = -2)

    /* renamed from: ka */
    public long f3068ka;
    @C0064Am(aul = "c1b3de45147a9764f07138f9e02bf550", aum = -1)

    /* renamed from: kc */
    public byte f3069kc;
    @C0064Am(aul = "6567c00e58a87d4c27abae20c5438c47", aum = -1)

    /* renamed from: kd */
    public byte f3070kd;
    @C0064Am(aul = "6f2d82f4152f7e69c7282fdf04048aac", aum = -1)

    /* renamed from: ke */
    public byte f3071ke;
    @C0064Am(aul = "d116df0429b121c3465bf41e174a1c0e", aum = -1)

    /* renamed from: kf */
    public byte f3072kf;
    @C0064Am(aul = "0308192860363d23896020a9e6622160", aum = 4)

    /* renamed from: nh */
    public I18NString f3073nh;
    @C0064Am(aul = "0308192860363d23896020a9e6622160", aum = -2)

    /* renamed from: nk */
    public long f3074nk;
    @C0064Am(aul = "0308192860363d23896020a9e6622160", aum = -1)

    /* renamed from: nn */
    public byte f3075nn;
    @C0064Am(aul = "4d0a7d05dbfdd4329aae562c57f8777f", aum = -2)

    /* renamed from: oL */
    public long f3076oL;
    @C0064Am(aul = "4d0a7d05dbfdd4329aae562c57f8777f", aum = -1)

    /* renamed from: oS */
    public byte f3077oS;
    @C0064Am(aul = "3b021bb7d61c4c6d886b8fa9448a43c1", aum = -2)

    /* renamed from: ok */
    public long f3078ok;
    @C0064Am(aul = "3b021bb7d61c4c6d886b8fa9448a43c1", aum = -1)

    /* renamed from: on */
    public byte f3079on;
    @C0064Am(aul = "f6a359289d0cfef6958f30d326386526", aum = 9)
    public Vec3f position;
    @C0064Am(aul = "f6a359289d0cfef6958f30d326386526", aum = -2)

    /* renamed from: rX */
    public long f3080rX;
    @C0064Am(aul = "f6a359289d0cfef6958f30d326386526", aum = -1)

    /* renamed from: sn */
    public byte f3081sn;
    @C0064Am(aul = "cf0653ea8764e7e7daa5bc49454b1d76", aum = 1)

    /* renamed from: zP */
    public I18NString f3082zP;

    public aIR() {
    }

    public aIR(C1616Xf xf) {
        super(xf);
    }

    /* renamed from: c */
    public C5663aRz[] mo309c() {
        return StellarSystem._m_fields;
    }

    /* renamed from: d */
    public void mo310d() {
        super.mo310d();
        this.diS = null;
        this.f3082zP = null;
        this.f3059jS = null;
        this.f3060jT = null;
        this.f3073nh = null;
        this.f3066jn = null;
        this.f3061jU = null;
        this.handle = null;
        this.diU = null;
        this.position = null;
        this.f3062jV = 0.0f;
        this.f3057bK = null;
        this.idn = 0;
        this.f3055Nf = 0;
        this.f3063jX = 0;
        this.f3064jY = 0;
        this.f3074nk = 0;
        this.f3067jy = 0;
        this.f3065jZ = 0;
        this.f3078ok = 0;
        this.ido = 0;
        this.f3080rX = 0;
        this.f3068ka = 0;
        this.f3076oL = 0;
        this.idp = 0;
        this.f3056Nh = 0;
        this.f3069kc = 0;
        this.f3070kd = 0;
        this.f3075nn = 0;
        this.f3058jK = 0;
        this.f3071ke = 0;
        this.f3079on = 0;
        this.idq = 0;
        this.f3081sn = 0;
        this.f3072kf = 0;
        this.f3077oS = 0;
    }
}
