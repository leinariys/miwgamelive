package logic.data.mbean;

import game.script.ship.OutpostUpkeepPeriod;
import logic.baa.C1616Xf;
import logic.res.code.C5663aRz;
import p001a.C0064Am;

/* renamed from: a.awX */
public class awX extends C6757atB {
    @C0064Am(aul = "e2346a3b50bc181247e3235058d67da7", aum = -2)

    /* renamed from: dl */
    public long f5518dl;
    @C0064Am(aul = "e2346a3b50bc181247e3235058d67da7", aum = -1)

    /* renamed from: ds */
    public byte f5519ds;
    @C0064Am(aul = "e2346a3b50bc181247e3235058d67da7", aum = 0)
    public OutpostUpkeepPeriod gOp;

    public awX() {
    }

    public awX(C1616Xf xf) {
        super(xf);
    }

    /* renamed from: c */
    public C5663aRz[] mo309c() {
        return OutpostUpkeepPeriod.OutpostUpkeepTicker._m_fields;
    }

    /* renamed from: d */
    public void mo310d() {
        super.mo310d();
        this.gOp = null;
        this.f5518dl = 0;
        this.f5519ds = 0;
    }
}
