package logic.data.mbean;

import game.script.item.Item;
import game.script.ship.ShipSector;
import game.script.ship.Slot;
import logic.baa.C1616Xf;
import logic.res.code.C5663aRz;
import p001a.C0064Am;

/* renamed from: a.aHD */
public class aHD extends aAL {
    @C0064Am(aul = "cfcbfacd2516fc6909794e7bcefbec33", aum = 0)
    public Item eeX;
    @C0064Am(aul = "0d199b08a4042430dcbfe611ed31cd9b", aum = 1)
    public ShipSector eeZ;
    @C0064Am(aul = "0d199b08a4042430dcbfe611ed31cd9b", aum = -1)
    public byte hYA;
    @C0064Am(aul = "cfcbfacd2516fc6909794e7bcefbec33", aum = -2)
    public long hYx;
    @C0064Am(aul = "0d199b08a4042430dcbfe611ed31cd9b", aum = -2)
    public long hYy;
    @C0064Am(aul = "cfcbfacd2516fc6909794e7bcefbec33", aum = -1)
    public byte hYz;

    public aHD() {
    }

    public aHD(C1616Xf xf) {
        super(xf);
    }

    /* renamed from: c */
    public C5663aRz[] mo309c() {
        return Slot._m_fields;
    }

    /* renamed from: d */
    public void mo310d() {
        super.mo310d();
        this.eeX = null;
        this.eeZ = null;
        this.hYx = 0;
        this.hYy = 0;
        this.hYz = 0;
        this.hYA = 0;
    }
}
