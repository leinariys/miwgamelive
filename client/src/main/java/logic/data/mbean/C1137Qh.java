package logic.data.mbean;

import game.script.npcchat.requirement.IsDockedInOwnOutpostWithLevelSpeechRequirement;
import logic.baa.C1616Xf;
import logic.res.code.C5663aRz;
import p001a.C0064Am;
import p001a.C2611hZ;

/* renamed from: a.Qh */
public class C1137Qh extends C1380UE {
    @C0064Am(aul = "164da67e4587737e563e03de44812594", aum = 0)
    public C2611hZ dVa;
    @C0064Am(aul = "164da67e4587737e563e03de44812594", aum = -2)
    public long dVb;
    @C0064Am(aul = "164da67e4587737e563e03de44812594", aum = -1)
    public byte dVc;

    public C1137Qh() {
    }

    public C1137Qh(C1616Xf xf) {
        super(xf);
    }

    /* renamed from: c */
    public C5663aRz[] mo309c() {
        return IsDockedInOwnOutpostWithLevelSpeechRequirement._m_fields;
    }

    /* renamed from: d */
    public void mo310d() {
        super.mo310d();
        this.dVa = null;
        this.dVb = 0;
        this.dVc = 0;
    }
}
