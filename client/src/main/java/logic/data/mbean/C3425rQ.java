package logic.data.mbean;

import game.network.message.serializable.C1556Wo;
import game.script.mission.scripting.I18NStringTable;
import game.script.npc.NPC;
import game.script.ship.Ship;
import game.script.space.Node;
import game.script.spacezone.AsteroidZone;
import game.script.spacezone.ScriptableZone;
import logic.baa.C1616Xf;
import logic.res.code.C5663aRz;
import p001a.C0064Am;

/* renamed from: a.rQ */
public class C3425rQ extends C6151ahT {
    @C0064Am(aul = "27bf9e5b6b10b8cb498056cf51326b18", aum = -2)

    /* renamed from: DA */
    public long f9026DA;
    @C0064Am(aul = "27bf9e5b6b10b8cb498056cf51326b18", aum = -1)

    /* renamed from: DE */
    public byte f9027DE;
    @C0064Am(aul = "3741b1a3dc24da7970cb51e49f1e7550", aum = 6)

    /* renamed from: UA */
    public String f9028UA;
    @C0064Am(aul = "3741b1a3dc24da7970cb51e49f1e7550", aum = -2)

    /* renamed from: UB */
    public long f9029UB;
    @C0064Am(aul = "3741b1a3dc24da7970cb51e49f1e7550", aum = -1)

    /* renamed from: UC */
    public byte f9030UC;
    @C0064Am(aul = "efc707110c91a94e6ee190aa59eb0114", aum = -2)
    public long aIu;
    @C0064Am(aul = "efc707110c91a94e6ee190aa59eb0114", aum = -1)
    public byte aIx;
    @C0064Am(aul = "a40741e3d5ba85d9bf2af41c6c230851", aum = 8)
    public Node aSh;
    @C0064Am(aul = "efc707110c91a94e6ee190aa59eb0114", aum = 7)
    public boolean active;
    @C0064Am(aul = "1a4e7f3b8fcc2b4d0fd1dc07ca5ac0c7", aum = 0)
    public ScriptableZone.Heartbeat bdT;
    @C0064Am(aul = "5c6f1e3d01ffb763aa8472df3b6ad863", aum = 1)
    public C1556Wo<String, NPC> bdU;
    @C0064Am(aul = "19ee65a2cebca1ab0dfbf738dac909ee", aum = 2)
    public C1556Wo<Ship, String> bdV;
    @C0064Am(aul = "e0683a94a27ec759e00faa28e2f0d2a3", aum = 3)
    public C1556Wo<String, AsteroidZone> bdW;
    @C0064Am(aul = "7d12a54bdc13f19a8024f6a37215dc3d", aum = 9)
    public I18NStringTable bdX;
    @C0064Am(aul = "1a4e7f3b8fcc2b4d0fd1dc07ca5ac0c7", aum = -2)
    public long bdY;
    @C0064Am(aul = "5c6f1e3d01ffb763aa8472df3b6ad863", aum = -2)
    public long bdZ;
    @C0064Am(aul = "19ee65a2cebca1ab0dfbf738dac909ee", aum = -2)
    public long bea;
    @C0064Am(aul = "e0683a94a27ec759e00faa28e2f0d2a3", aum = -2)
    public long beb;
    @C0064Am(aul = "a40741e3d5ba85d9bf2af41c6c230851", aum = -2)
    public long bec;
    @C0064Am(aul = "7d12a54bdc13f19a8024f6a37215dc3d", aum = -2)
    public long bed;
    @C0064Am(aul = "1a4e7f3b8fcc2b4d0fd1dc07ca5ac0c7", aum = -1)
    public byte bee;
    @C0064Am(aul = "5c6f1e3d01ffb763aa8472df3b6ad863", aum = -1)
    public byte bef;
    @C0064Am(aul = "19ee65a2cebca1ab0dfbf738dac909ee", aum = -1)
    public byte beg;
    @C0064Am(aul = "e0683a94a27ec759e00faa28e2f0d2a3", aum = -1)
    public byte beh;
    @C0064Am(aul = "a40741e3d5ba85d9bf2af41c6c230851", aum = -1)
    public byte bei;
    @C0064Am(aul = "7d12a54bdc13f19a8024f6a37215dc3d", aum = -1)
    public byte bej;
    @C0064Am(aul = "259019b0ab6e276a530566687f6ce387", aum = 5)
    public String handle;
    @C0064Am(aul = "259019b0ab6e276a530566687f6ce387", aum = -2)

    /* renamed from: ok */
    public long f9031ok;
    @C0064Am(aul = "259019b0ab6e276a530566687f6ce387", aum = -1)

    /* renamed from: on */
    public byte f9032on;
    @C0064Am(aul = "27bf9e5b6b10b8cb498056cf51326b18", aum = 4)
    public float radius;

    public C3425rQ() {
    }

    public C3425rQ(C1616Xf xf) {
        super(xf);
    }

    /* renamed from: c */
    public C5663aRz[] mo309c() {
        return ScriptableZone._m_fields;
    }

    /* renamed from: d */
    public void mo310d() {
        super.mo310d();
        this.bdT = null;
        this.bdU = null;
        this.bdV = null;
        this.bdW = null;
        this.radius = 0.0f;
        this.handle = null;
        this.f9028UA = null;
        this.active = false;
        this.aSh = null;
        this.bdX = null;
        this.bdY = 0;
        this.bdZ = 0;
        this.bea = 0;
        this.beb = 0;
        this.f9026DA = 0;
        this.f9031ok = 0;
        this.f9029UB = 0;
        this.aIu = 0;
        this.bec = 0;
        this.bed = 0;
        this.bee = 0;
        this.bef = 0;
        this.beg = 0;
        this.beh = 0;
        this.f9027DE = 0;
        this.f9032on = 0;
        this.f9030UC = 0;
        this.aIx = 0;
        this.bei = 0;
        this.bej = 0;
    }
}
