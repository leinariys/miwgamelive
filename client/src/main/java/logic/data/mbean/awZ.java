package logic.data.mbean;

import game.script.mission.actions.ChangeObjectiveVisibilityMissionAction;
import logic.baa.C1616Xf;
import logic.res.code.C5663aRz;
import p001a.C0064Am;

/* renamed from: a.awZ */
public class awZ extends C2503fv {
    @C0064Am(aul = "9982983947d9404731c82a84db036ad4", aum = 1)
    public boolean big;
    @C0064Am(aul = "9982983947d9404731c82a84db036ad4", aum = -2)
    public long dIW;
    @C0064Am(aul = "9982983947d9404731c82a84db036ad4", aum = -1)
    public byte dJc;
    @C0064Am(aul = "75477dd9792a1567c7ae4c92d377f839", aum = 0)
    public String gaW;
    @C0064Am(aul = "75477dd9792a1567c7ae4c92d377f839", aum = -2)
    public long gaX;
    @C0064Am(aul = "75477dd9792a1567c7ae4c92d377f839", aum = -1)
    public byte gaY;

    public awZ() {
    }

    public awZ(C1616Xf xf) {
        super(xf);
    }

    /* renamed from: c */
    public C5663aRz[] mo309c() {
        return ChangeObjectiveVisibilityMissionAction._m_fields;
    }

    /* renamed from: d */
    public void mo310d() {
        super.mo310d();
        this.gaW = null;
        this.big = false;
        this.gaX = 0;
        this.dIW = 0;
        this.gaY = 0;
        this.dJc = 0;
    }
}
