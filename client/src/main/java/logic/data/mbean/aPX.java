package logic.data.mbean;

import game.script.resource.Asset;
import game.script.space.LootType;
import logic.baa.C1616Xf;
import logic.res.code.C5663aRz;
import p001a.C0064Am;

/* renamed from: a.aPX */
public class aPX extends C6515aoT {
    @C0064Am(aul = "8e859bd97252d67222c4d4c19513c847", aum = 0)

    /* renamed from: LQ */
    public Asset f3531LQ;
    @C0064Am(aul = "fb3f41f93b85bfc44422e650991c1e5b", aum = -2)
    public long ail;
    @C0064Am(aul = "fb3f41f93b85bfc44422e650991c1e5b", aum = -1)
    public byte aiv;
    @C0064Am(aul = "8e859bd97252d67222c4d4c19513c847", aum = -2)
    public long ayc;
    @C0064Am(aul = "8e859bd97252d67222c4d4c19513c847", aum = -1)
    public byte ayr;
    @C0064Am(aul = "fb3f41f93b85bfc44422e650991c1e5b", aum = 1)
    public float lifeTime;

    public aPX() {
    }

    public aPX(C1616Xf xf) {
        super(xf);
    }

    /* renamed from: c */
    public C5663aRz[] mo309c() {
        return LootType._m_fields;
    }

    /* renamed from: d */
    public void mo310d() {
        super.mo310d();
        this.f3531LQ = null;
        this.lifeTime = 0.0f;
        this.ayc = 0;
        this.ail = 0;
        this.ayr = 0;
        this.aiv = 0;
    }
}
