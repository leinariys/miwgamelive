package logic.data.mbean;

import game.script.mission.scripting.ReconNpcMissionScript;
import logic.baa.C1616Xf;
import logic.res.code.C5663aRz;
import p001a.C0064Am;

/* renamed from: a.dg */
public class C2292dg extends C2955mD {
    @C0064Am(aul = "927d66e0e3372fe648c1544a617b5ef8", aum = -2)

    /* renamed from: dl */
    public long f6579dl;
    @C0064Am(aul = "927d66e0e3372fe648c1544a617b5ef8", aum = -1)

    /* renamed from: ds */
    public byte f6580ds;
    @C0064Am(aul = "927d66e0e3372fe648c1544a617b5ef8", aum = 0)

    /* renamed from: ym */
    public ReconNpcMissionScript f6581ym;

    public C2292dg() {
    }

    public C2292dg(C1616Xf xf) {
        super(xf);
    }

    /* renamed from: c */
    public C5663aRz[] mo309c() {
        return ReconNpcMissionScript.StateA._m_fields;
    }

    /* renamed from: d */
    public void mo310d() {
        super.mo310d();
        this.f6581ym = null;
        this.f6579dl = 0;
        this.f6580ds = 0;
    }
}
