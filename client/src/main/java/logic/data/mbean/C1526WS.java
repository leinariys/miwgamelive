package logic.data.mbean;

import game.network.message.serializable.C3438ra;
import game.script.spacezone.population.LinearlyDistribuitedPopulationControlType;
import game.script.spacezone.population.NPCSpawn;
import logic.baa.C1616Xf;
import logic.res.code.C5663aRz;
import p001a.C0064Am;

/* renamed from: a.WS */
public class C1526WS extends aIG {
    @C0064Am(aul = "56519477275e16bff13c07dd1e7c6b0a", aum = -2)
    public long bmB;
    @C0064Am(aul = "56519477275e16bff13c07dd1e7c6b0a", aum = -1)
    public byte bmK;
    @C0064Am(aul = "56519477275e16bff13c07dd1e7c6b0a", aum = 0)
    public C3438ra<NPCSpawn> dkG;

    public C1526WS() {
    }

    public C1526WS(C1616Xf xf) {
        super(xf);
    }

    /* renamed from: c */
    public C5663aRz[] mo309c() {
        return LinearlyDistribuitedPopulationControlType._m_fields;
    }

    /* renamed from: d */
    public void mo310d() {
        super.mo310d();
        this.dkG = null;
        this.bmB = 0;
        this.bmK = 0;
    }
}
