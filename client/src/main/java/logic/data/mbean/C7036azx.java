package logic.data.mbean;

import logic.baa.C1616Xf;
import logic.res.code.C5663aRz;
import p001a.C0064Am;
import taikodom.infra.script.ai.behaviours.AIBehaviourConfig;

/* renamed from: a.azx  reason: case insensitive filesystem */
public class C7036azx extends C3805vD {
    @C0064Am(aul = "3c2d9a6240eb2c5f77c9d02aac0edc68", aum = 2)
    public float dodgeAngle;
    @C0064Am(aul = "167f845364ffb2d380a720526d8c9a65", aum = 1)
    public float dodgeDistance;
    @C0064Am(aul = "c4bfeb3118a5bfb03bc9c86fb4b3dabb", aum = 0)
    public float dodgeReactionTime;
    @C0064Am(aul = "c3ede6e57ecc12156836e63070a9acda", aum = -2)
    public long fFf;
    @C0064Am(aul = "c4bfeb3118a5bfb03bc9c86fb4b3dabb", aum = -2)
    public long fFh;
    @C0064Am(aul = "167f845364ffb2d380a720526d8c9a65", aum = -2)
    public long fFi;
    @C0064Am(aul = "3c2d9a6240eb2c5f77c9d02aac0edc68", aum = -2)
    public long fFj;
    @C0064Am(aul = "c3ede6e57ecc12156836e63070a9acda", aum = -1)
    public byte fFm;
    @C0064Am(aul = "c4bfeb3118a5bfb03bc9c86fb4b3dabb", aum = -1)
    public byte fFo;
    @C0064Am(aul = "167f845364ffb2d380a720526d8c9a65", aum = -1)
    public byte fFp;
    @C0064Am(aul = "3c2d9a6240eb2c5f77c9d02aac0edc68", aum = -1)
    public byte fFq;
    @C0064Am(aul = "c3ede6e57ecc12156836e63070a9acda", aum = 3)
    public float pursuitDistance;

    public C7036azx() {
    }

    public C7036azx(C1616Xf xf) {
        super(xf);
    }

    /* renamed from: c */
    public C5663aRz[] mo309c() {
        return AIBehaviourConfig._m_fields;
    }

    /* renamed from: d */
    public void mo310d() {
        super.mo310d();
        this.dodgeReactionTime = 0.0f;
        this.dodgeDistance = 0.0f;
        this.dodgeAngle = 0.0f;
        this.pursuitDistance = 0.0f;
        this.fFh = 0;
        this.fFi = 0;
        this.fFj = 0;
        this.fFf = 0;
        this.fFo = 0;
        this.fFp = 0;
        this.fFq = 0;
        this.fFm = 0;
    }
}
