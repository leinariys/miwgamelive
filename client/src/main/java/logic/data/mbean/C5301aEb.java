package logic.data.mbean;

import game.script.item.buff.amplifier.CruiseSpeedAmplifier;
import game.script.ship.CruiseSpeedAdapter;
import logic.baa.C1616Xf;
import logic.res.code.C5663aRz;
import p001a.C0064Am;
import p001a.C3892wO;

/* renamed from: a.aEb  reason: case insensitive filesystem */
public class C5301aEb extends C3108nv {
    @C0064Am(aul = "9b3435bc39a5ed28e12d9aa3e368af3e", aum = 0)

    /* renamed from: Vs */
    public C3892wO f2721Vs;
    @C0064Am(aul = "ec26eb47555a45c1efc0b89d11c4a5d8", aum = 1)

    /* renamed from: Vu */
    public C3892wO f2722Vu;
    @C0064Am(aul = "d867fe013f607f3e0102355d37b11448", aum = 2)

    /* renamed from: Vw */
    public C3892wO f2723Vw;
    @C0064Am(aul = "f915d2e32ef8676d7055c64d52e19625", aum = 3)

    /* renamed from: Vy */
    public C3892wO f2724Vy;
    @C0064Am(aul = "6aeea736fcb1456c2a2403331e63a28b", aum = -2)

    /* renamed from: YA */
    public long f2725YA;
    @C0064Am(aul = "02f964332e79f85bc7a2eca3d95d7415", aum = -2)

    /* renamed from: YB */
    public long f2726YB;
    @C0064Am(aul = "9b3435bc39a5ed28e12d9aa3e368af3e", aum = -1)

    /* renamed from: YC */
    public byte f2727YC;
    @C0064Am(aul = "ec26eb47555a45c1efc0b89d11c4a5d8", aum = -1)

    /* renamed from: YD */
    public byte f2728YD;
    @C0064Am(aul = "d867fe013f607f3e0102355d37b11448", aum = -1)

    /* renamed from: YE */
    public byte f2729YE;
    @C0064Am(aul = "f915d2e32ef8676d7055c64d52e19625", aum = -1)

    /* renamed from: YF */
    public byte f2730YF;
    @C0064Am(aul = "6aeea736fcb1456c2a2403331e63a28b", aum = -1)

    /* renamed from: YG */
    public byte f2731YG;
    @C0064Am(aul = "02f964332e79f85bc7a2eca3d95d7415", aum = -1)

    /* renamed from: YH */
    public byte f2732YH;
    @C0064Am(aul = "6aeea736fcb1456c2a2403331e63a28b", aum = 4)

    /* renamed from: Yu */
    public C3892wO f2733Yu;
    @C0064Am(aul = "02f964332e79f85bc7a2eca3d95d7415", aum = 5)

    /* renamed from: Yv */
    public C3892wO f2734Yv;
    @C0064Am(aul = "9b3435bc39a5ed28e12d9aa3e368af3e", aum = -2)

    /* renamed from: Yw */
    public long f2735Yw;
    @C0064Am(aul = "ec26eb47555a45c1efc0b89d11c4a5d8", aum = -2)

    /* renamed from: Yx */
    public long f2736Yx;
    @C0064Am(aul = "d867fe013f607f3e0102355d37b11448", aum = -2)

    /* renamed from: Yy */
    public long f2737Yy;
    @C0064Am(aul = "f915d2e32ef8676d7055c64d52e19625", aum = -2)

    /* renamed from: Yz */
    public long f2738Yz;
    @C0064Am(aul = "4e7fbc8ba5480b887afc4453fa473709", aum = 6)
    public CruiseSpeedAdapter aYd;
    @C0064Am(aul = "4e7fbc8ba5480b887afc4453fa473709", aum = -2)
    public long hET;
    @C0064Am(aul = "4e7fbc8ba5480b887afc4453fa473709", aum = -1)
    public byte hEU;

    public C5301aEb() {
    }

    public C5301aEb(C1616Xf xf) {
        super(xf);
    }

    /* renamed from: c */
    public C5663aRz[] mo309c() {
        return CruiseSpeedAmplifier._m_fields;
    }

    /* renamed from: d */
    public void mo310d() {
        super.mo310d();
        this.f2721Vs = null;
        this.f2722Vu = null;
        this.f2723Vw = null;
        this.f2724Vy = null;
        this.f2733Yu = null;
        this.f2734Yv = null;
        this.aYd = null;
        this.f2735Yw = 0;
        this.f2736Yx = 0;
        this.f2737Yy = 0;
        this.f2738Yz = 0;
        this.f2725YA = 0;
        this.f2726YB = 0;
        this.hET = 0;
        this.f2727YC = 0;
        this.f2728YD = 0;
        this.f2729YE = 0;
        this.f2730YF = 0;
        this.f2731YG = 0;
        this.f2732YH = 0;
        this.hEU = 0;
    }
}
