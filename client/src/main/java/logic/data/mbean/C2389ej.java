package logic.data.mbean;

import game.network.message.serializable.C3438ra;
import game.script.bank.BankAccount;
import game.script.bank.BankStatementEntry;
import logic.baa.C1616Xf;
import logic.res.code.C5663aRz;
import p001a.C0064Am;

/* renamed from: a.ej */
public class C2389ej extends C3805vD {
    @C0064Am(aul = "9a69e6e3e8ad0a07bbddb590c3bfab91", aum = 0)

    /* renamed from: BF */
    public long f7028BF;
    @C0064Am(aul = "e33bc6daa3057d078fa60cefc2760bb9", aum = 1)

    /* renamed from: BG */
    public C3438ra<BankStatementEntry> f7029BG;
    @C0064Am(aul = "9a69e6e3e8ad0a07bbddb590c3bfab91", aum = -2)

    /* renamed from: BH */
    public long f7030BH;
    @C0064Am(aul = "e33bc6daa3057d078fa60cefc2760bb9", aum = -2)

    /* renamed from: BI */
    public long f7031BI;
    @C0064Am(aul = "9a69e6e3e8ad0a07bbddb590c3bfab91", aum = -1)

    /* renamed from: BJ */
    public byte f7032BJ;
    @C0064Am(aul = "e33bc6daa3057d078fa60cefc2760bb9", aum = -1)

    /* renamed from: BK */
    public byte f7033BK;

    public C2389ej() {
    }

    public C2389ej(C1616Xf xf) {
        super(xf);
    }

    /* renamed from: c */
    public C5663aRz[] mo309c() {
        return BankAccount._m_fields;
    }

    /* renamed from: d */
    public void mo310d() {
        super.mo310d();
        this.f7028BF = 0;
        this.f7029BG = null;
        this.f7030BH = 0;
        this.f7031BI = 0;
        this.f7032BJ = 0;
        this.f7033BK = 0;
    }
}
