package logic.data.mbean;

import game.script.item.EnergyWeaponType;
import game.script.resource.Asset;
import logic.baa.C1616Xf;
import logic.res.code.C5663aRz;
import p001a.C0064Am;

/* renamed from: a.kM */
public class C2805kM extends C3967xU {
    @C0064Am(aul = "b5d9640168ece5382ba1aa32645030fd", aum = 0)
    public int auV;
    @C0064Am(aul = "b34beb9c98f23c6d768e91a987f57e6e", aum = 1)
    public int auW;
    @C0064Am(aul = "431a433bcddba5a80b39b3ad5320b793", aum = 2)
    public Asset auX;
    @C0064Am(aul = "72bb756c385b81486c2f1d6b17223a63", aum = 3)
    public float auY;
    @C0064Am(aul = "b5d9640168ece5382ba1aa32645030fd", aum = -2)
    public long auZ;
    @C0064Am(aul = "b34beb9c98f23c6d768e91a987f57e6e", aum = -2)
    public long ava;
    @C0064Am(aul = "431a433bcddba5a80b39b3ad5320b793", aum = -2)
    public long avb;
    @C0064Am(aul = "72bb756c385b81486c2f1d6b17223a63", aum = -2)
    public long avc;
    @C0064Am(aul = "5c75a391205a11580a1164e813db5207", aum = -2)
    public long avd;
    @C0064Am(aul = "b5d9640168ece5382ba1aa32645030fd", aum = -1)
    public byte ave;
    @C0064Am(aul = "b34beb9c98f23c6d768e91a987f57e6e", aum = -1)
    public byte avf;
    @C0064Am(aul = "431a433bcddba5a80b39b3ad5320b793", aum = -1)
    public byte avg;
    @C0064Am(aul = "72bb756c385b81486c2f1d6b17223a63", aum = -1)
    public byte avh;
    @C0064Am(aul = "5c75a391205a11580a1164e813db5207", aum = -1)
    public byte avi;
    @C0064Am(aul = "5c75a391205a11580a1164e813db5207", aum = 4)

    /* renamed from: uT */
    public String f8410uT;

    public C2805kM() {
    }

    public C2805kM(C1616Xf xf) {
        super(xf);
    }

    /* renamed from: c */
    public C5663aRz[] mo309c() {
        return EnergyWeaponType._m_fields;
    }

    /* renamed from: d */
    public void mo310d() {
        super.mo310d();
        this.auV = 0;
        this.auW = 0;
        this.auX = null;
        this.auY = 0.0f;
        this.f8410uT = null;
        this.auZ = 0;
        this.ava = 0;
        this.avb = 0;
        this.avc = 0;
        this.avd = 0;
        this.ave = 0;
        this.avf = 0;
        this.avg = 0;
        this.avh = 0;
        this.avi = 0;
    }
}
