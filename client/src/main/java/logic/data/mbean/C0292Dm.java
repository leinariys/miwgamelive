package logic.data.mbean;

import game.script.item.ItemType;
import game.script.itemgen.ItemGenEntry;
import logic.baa.C1616Xf;
import logic.res.code.C5663aRz;
import p001a.C0064Am;

import java.util.UUID;

/* renamed from: a.Dm */
public class C0292Dm extends C5292aDs {
    @C0064Am(aul = "5b530feefca46dde291da74f1bcd9112", aum = 0)
    public int aAL;
    @C0064Am(aul = "e87fd72ea701cd8cfa204638235ecd62", aum = 1)
    public int aAN;
    @C0064Am(aul = "641f551a4297e67e0f2b7ba8d8d3f265", aum = 2)
    public float aAP;
    @C0064Am(aul = "5286b07fa07a9115184b90bb27152aa0", aum = 4)

    /* renamed from: bK */
    public UUID f431bK;
    @C0064Am(aul = "a4b441b6a72ddce1c29a62513a7a7a84", aum = -2)
    public long bmV;
    @C0064Am(aul = "a4b441b6a72ddce1c29a62513a7a7a84", aum = -1)
    public byte bmW;
    @C0064Am(aul = "5b530feefca46dde291da74f1bcd9112", aum = -2)
    public long cEa;
    @C0064Am(aul = "e87fd72ea701cd8cfa204638235ecd62", aum = -2)
    public long cEb;
    @C0064Am(aul = "641f551a4297e67e0f2b7ba8d8d3f265", aum = -2)
    public long cEc;
    @C0064Am(aul = "5b530feefca46dde291da74f1bcd9112", aum = -1)
    public byte cEd;
    @C0064Am(aul = "e87fd72ea701cd8cfa204638235ecd62", aum = -1)
    public byte cEe;
    @C0064Am(aul = "641f551a4297e67e0f2b7ba8d8d3f265", aum = -1)
    public byte cEf;
    @C0064Am(aul = "a4b441b6a72ddce1c29a62513a7a7a84", aum = 3)

    /* renamed from: cw */
    public ItemType f432cw;
    @C0064Am(aul = "a8043b98dd016a27186cb1937c42c00d", aum = 5)
    public String handle;
    @C0064Am(aul = "5286b07fa07a9115184b90bb27152aa0", aum = -2)

    /* renamed from: oL */
    public long f433oL;
    @C0064Am(aul = "5286b07fa07a9115184b90bb27152aa0", aum = -1)

    /* renamed from: oS */
    public byte f434oS;
    @C0064Am(aul = "a8043b98dd016a27186cb1937c42c00d", aum = -2)

    /* renamed from: ok */
    public long f435ok;
    @C0064Am(aul = "a8043b98dd016a27186cb1937c42c00d", aum = -1)

    /* renamed from: on */
    public byte f436on;

    public C0292Dm() {
    }

    public C0292Dm(C1616Xf xf) {
        super(xf);
    }

    /* renamed from: c */
    public C5663aRz[] mo309c() {
        return ItemGenEntry._m_fields;
    }

    /* renamed from: d */
    public void mo310d() {
        super.mo310d();
        this.aAL = 0;
        this.aAN = 0;
        this.aAP = 0.0f;
        this.f432cw = null;
        this.f431bK = null;
        this.handle = null;
        this.cEa = 0;
        this.cEb = 0;
        this.cEc = 0;
        this.bmV = 0;
        this.f433oL = 0;
        this.f435ok = 0;
        this.cEd = 0;
        this.cEe = 0;
        this.cEf = 0;
        this.bmW = 0;
        this.f434oS = 0;
        this.f436on = 0;
    }
}
