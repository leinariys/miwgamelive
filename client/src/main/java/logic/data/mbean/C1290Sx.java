package logic.data.mbean;

import game.script.ship.categories.Explorer;
import game.script.ship.hazardshield.HazardShield;
import game.script.ship.hazardshield.HazardShieldType;
import logic.baa.C1616Xf;
import logic.res.code.C5663aRz;
import p001a.C0064Am;

/* renamed from: a.Sx */
public class C1290Sx extends aMX {
    @C0064Am(aul = "880a24dfdb98e6369e894e8e16c65f9f", aum = 0)
    public HazardShieldType aZC;
    @C0064Am(aul = "880a24dfdb98e6369e894e8e16c65f9f", aum = -2)
    public long cBf;
    @C0064Am(aul = "880a24dfdb98e6369e894e8e16c65f9f", aum = -1)
    public byte cBg;
    @C0064Am(aul = "abea0f8c30ab08e80cf4cea7bc235ba6", aum = 1)
    public HazardShield eex;
    @C0064Am(aul = "abea0f8c30ab08e80cf4cea7bc235ba6", aum = -2)
    public long eey;
    @C0064Am(aul = "abea0f8c30ab08e80cf4cea7bc235ba6", aum = -1)
    public byte eez;

    public C1290Sx() {
    }

    public C1290Sx(C1616Xf xf) {
        super(xf);
    }

    /* renamed from: c */
    public C5663aRz[] mo309c() {
        return Explorer._m_fields;
    }

    /* renamed from: d */
    public void mo310d() {
        super.mo310d();
        this.aZC = null;
        this.eex = null;
        this.cBf = 0;
        this.eey = 0;
        this.cBg = 0;
        this.eez = 0;
    }
}
