package logic.data.mbean;

import game.script.corporation.NLSCorporation;
import logic.baa.C1616Xf;
import logic.res.code.C5663aRz;
import p001a.C0064Am;
import taikodom.infra.script.I18NString;

/* renamed from: a.anS  reason: case insensitive filesystem */
public class C6462anS extends C2484fi {
    @C0064Am(aul = "3972f4b7f104dc7a0336a1a9f97e5f24", aum = 0)
    public I18NString aNQ;
    @C0064Am(aul = "0014af1b25320bacab94fa59368af6ad", aum = 1)
    public I18NString aNS;
    @C0064Am(aul = "c6ce68c6d03e32da88cea1748e663deb", aum = 2)
    public I18NString aNU;
    @C0064Am(aul = "f50a8cd254d357275254fbea5e10afa1", aum = 3)
    public I18NString aNW;
    @C0064Am(aul = "8d2c9767d2d54ba0df1d3718bf8c6358", aum = 4)
    public I18NString aNY;
    @C0064Am(aul = "3972f4b7f104dc7a0336a1a9f97e5f24", aum = -2)
    public long gfo;
    @C0064Am(aul = "0014af1b25320bacab94fa59368af6ad", aum = -2)
    public long gfp;
    @C0064Am(aul = "c6ce68c6d03e32da88cea1748e663deb", aum = -2)
    public long gfq;
    @C0064Am(aul = "f50a8cd254d357275254fbea5e10afa1", aum = -2)
    public long gfr;
    @C0064Am(aul = "8d2c9767d2d54ba0df1d3718bf8c6358", aum = -2)
    public long gfs;
    @C0064Am(aul = "3972f4b7f104dc7a0336a1a9f97e5f24", aum = -1)
    public byte gft;
    @C0064Am(aul = "0014af1b25320bacab94fa59368af6ad", aum = -1)
    public byte gfu;
    @C0064Am(aul = "c6ce68c6d03e32da88cea1748e663deb", aum = -1)
    public byte gfv;
    @C0064Am(aul = "f50a8cd254d357275254fbea5e10afa1", aum = -1)
    public byte gfw;
    @C0064Am(aul = "8d2c9767d2d54ba0df1d3718bf8c6358", aum = -1)
    public byte gfx;

    public C6462anS() {
    }

    public C6462anS(C1616Xf xf) {
        super(xf);
    }

    /* renamed from: c */
    public C5663aRz[] mo309c() {
        return NLSCorporation.PVPInvitation._m_fields;
    }

    /* renamed from: d */
    public void mo310d() {
        super.mo310d();
        this.aNQ = null;
        this.aNS = null;
        this.aNU = null;
        this.aNW = null;
        this.aNY = null;
        this.gfo = 0;
        this.gfp = 0;
        this.gfq = 0;
        this.gfr = 0;
        this.gfs = 0;
        this.gft = 0;
        this.gfu = 0;
        this.gfv = 0;
        this.gfw = 0;
        this.gfx = 0;
    }
}
