package logic.data.mbean;

import com.hoplon.geometry.Vec3f;
import game.network.message.serializable.C2686iZ;
import game.script.Character;
import game.script.resource.Asset;
import game.script.spacezone.NPCZone;
import game.script.spacezone.PopulationBehaviour;
import game.script.spacezone.PopulationControl;
import logic.baa.C1616Xf;
import logic.res.code.C5663aRz;
import p001a.C0064Am;

/* renamed from: a.uF */
public class C3726uF extends C6979axs {
    @C0064Am(aul = "2a4e42c0e5b76b8df7eeba02d2f41e09", aum = 1)

    /* renamed from: KB */
    public PopulationBehaviour f9317KB;
    @C0064Am(aul = "1aef66102293474c414cba1f9fcb9c16", aum = 2)

    /* renamed from: KD */
    public C2686iZ<Character> f9318KD;
    @C0064Am(aul = "ec26ac83187601c22df43ef96066e9de", aum = 4)

    /* renamed from: KG */
    public boolean f9319KG;
    @C0064Am(aul = "e97793f3973b073865c56b01b4d9365f", aum = 5)

    /* renamed from: KI */
    public Asset f9320KI;
    @C0064Am(aul = "fc02d9d68d135b44c03cf21db5d3f9e9", aum = 6)

    /* renamed from: KK */
    public Vec3f f9321KK;
    @C0064Am(aul = "d3bb811e8efee6a93bb9090577a29d72", aum = 7)

    /* renamed from: KM */
    public float f9322KM;
    @C0064Am(aul = "f37434ffa8615cc856209fab31675d8e", aum = 8)

    /* renamed from: KO */
    public NPCZone.C2505a f9323KO;
    @C0064Am(aul = "55c0dce5f85c860224d11995113be5c7", aum = 0)

    /* renamed from: Kz */
    public PopulationControl f9324Kz;
    @C0064Am(aul = "55c0dce5f85c860224d11995113be5c7", aum = -2)

    /* renamed from: Vl */
    public long f9325Vl;
    @C0064Am(aul = "e97793f3973b073865c56b01b4d9365f", aum = -2)

    /* renamed from: Vm */
    public long f9326Vm;
    @C0064Am(aul = "55c0dce5f85c860224d11995113be5c7", aum = -1)

    /* renamed from: Vn */
    public byte f9327Vn;
    @C0064Am(aul = "e97793f3973b073865c56b01b4d9365f", aum = -1)

    /* renamed from: Vo */
    public byte f9328Vo;
    @C0064Am(aul = "b797416a3b7ec9e7c92e21c124c002bc", aum = -2)
    public long aIu;
    @C0064Am(aul = "b797416a3b7ec9e7c92e21c124c002bc", aum = -1)
    public byte aIx;
    @C0064Am(aul = "b797416a3b7ec9e7c92e21c124c002bc", aum = 3)
    public boolean active;
    @C0064Am(aul = "2a4e42c0e5b76b8df7eeba02d2f41e09", aum = -2)
    public long bwi;
    @C0064Am(aul = "1aef66102293474c414cba1f9fcb9c16", aum = -2)
    public long bwj;
    @C0064Am(aul = "ec26ac83187601c22df43ef96066e9de", aum = -2)
    public long bwk;
    @C0064Am(aul = "fc02d9d68d135b44c03cf21db5d3f9e9", aum = -2)
    public long bwl;
    @C0064Am(aul = "d3bb811e8efee6a93bb9090577a29d72", aum = -2)
    public long bwm;
    @C0064Am(aul = "f37434ffa8615cc856209fab31675d8e", aum = -2)
    public long bwn;
    @C0064Am(aul = "2a4e42c0e5b76b8df7eeba02d2f41e09", aum = -1)
    public byte bwo;
    @C0064Am(aul = "1aef66102293474c414cba1f9fcb9c16", aum = -1)
    public byte bwp;
    @C0064Am(aul = "ec26ac83187601c22df43ef96066e9de", aum = -1)
    public byte bwq;
    @C0064Am(aul = "fc02d9d68d135b44c03cf21db5d3f9e9", aum = -1)
    public byte bwr;
    @C0064Am(aul = "d3bb811e8efee6a93bb9090577a29d72", aum = -1)
    public byte bws;
    @C0064Am(aul = "f37434ffa8615cc856209fab31675d8e", aum = -1)
    public byte bwt;

    public C3726uF() {
    }

    public C3726uF(C1616Xf xf) {
        super(xf);
    }

    /* renamed from: c */
    public C5663aRz[] mo309c() {
        return NPCZone._m_fields;
    }

    /* renamed from: d */
    public void mo310d() {
        super.mo310d();
        this.f9324Kz = null;
        this.f9317KB = null;
        this.f9318KD = null;
        this.active = false;
        this.f9319KG = false;
        this.f9320KI = null;
        this.f9321KK = null;
        this.f9322KM = 0.0f;
        this.f9323KO = null;
        this.f9325Vl = 0;
        this.bwi = 0;
        this.bwj = 0;
        this.aIu = 0;
        this.bwk = 0;
        this.f9326Vm = 0;
        this.bwl = 0;
        this.bwm = 0;
        this.bwn = 0;
        this.f9327Vn = 0;
        this.bwo = 0;
        this.bwp = 0;
        this.aIx = 0;
        this.bwq = 0;
        this.f9328Vo = 0;
        this.bwr = 0;
        this.bws = 0;
        this.bwt = 0;
    }
}
