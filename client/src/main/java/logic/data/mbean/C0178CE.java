package logic.data.mbean;

import game.script.Character;
import game.script.progression.ProgressionCareer;
import logic.baa.C1616Xf;
import logic.res.code.C5663aRz;
import p001a.C0064Am;

/* renamed from: a.CE */
public class C0178CE extends C3805vD {
    @C0064Am(aul = "d8f512d5d334f2ed0b7d3e53cc1c6978", aum = 2)

    /* renamed from: BO */
    public int f271BO;
    @C0064Am(aul = "1224ca8b7f1e873667305ee2a502e886", aum = 1)

    /* renamed from: BU */
    public ProgressionCareer f272BU;
    @C0064Am(aul = "be448d3c7bf420ffcff02e5128b70483", aum = 0)
    public Character aKN;
    @C0064Am(aul = "d8f512d5d334f2ed0b7d3e53cc1c6978", aum = -1)
    public byte azE;
    @C0064Am(aul = "d8f512d5d334f2ed0b7d3e53cc1c6978", aum = -2)
    public long azp;
    @C0064Am(aul = "40bc153d2b3af675dff24d61c3d51ed8", aum = 3)
    public String cyd;
    @C0064Am(aul = "1224ca8b7f1e873667305ee2a502e886", aum = -2)
    public long cye;
    @C0064Am(aul = "40bc153d2b3af675dff24d61c3d51ed8", aum = -2)
    public long cyf;
    @C0064Am(aul = "1224ca8b7f1e873667305ee2a502e886", aum = -1)
    public byte cyg;
    @C0064Am(aul = "40bc153d2b3af675dff24d61c3d51ed8", aum = -1)
    public byte cyh;
    @C0064Am(aul = "be448d3c7bf420ffcff02e5128b70483", aum = -2)

    /* renamed from: nm */
    public long f273nm;
    @C0064Am(aul = "be448d3c7bf420ffcff02e5128b70483", aum = -1)

    /* renamed from: np */
    public byte f274np;

    public C0178CE() {
    }

    public C0178CE(C1616Xf xf) {
        super(xf);
    }

    /* renamed from: c */
    public C5663aRz[] mo309c() {
        return azE._m_fields;
    }

    /* renamed from: d */
    public void mo310d() {
        super.mo310d();
        this.aKN = null;
        this.f272BU = null;
        this.f271BO = 0;
        this.cyd = null;
        this.f273nm = 0;
        this.cye = 0;
        this.azp = 0;
        this.cyf = 0;
        this.f274np = 0;
        this.cyg = 0;
        this.azE = 0;
        this.cyh = 0;
    }
}
