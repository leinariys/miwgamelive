package logic.data.mbean;

import game.script.resource.Asset;
import logic.baa.C1616Xf;
import logic.baa.C6909avx;
import logic.res.code.C5663aRz;
import p001a.C0064Am;

import java.util.UUID;

/* renamed from: a.bC */
public class C2024bC extends C3805vD {
    @C0064Am(aul = "1b2f5bc5de59b6cdd54c6db9edd62e6f", aum = 6)

    /* renamed from: bK */
    public UUID f5724bK;
    @C0064Am(aul = "54c72b6d92464af8a154c58051928eaf", aum = 7)
    public String handle;
    @C0064Am(aul = "e10cd53ec32eb13854016ac8441ffa90", aum = 1)

    /* renamed from: oA */
    public Asset f5725oA;
    @C0064Am(aul = "59001477ecea68cfc7a289383f86e5a3", aum = 2)

    /* renamed from: oB */
    public Asset f5726oB;
    @C0064Am(aul = "77b79e4a03eab80470d3c0b2eb3c42d8", aum = 3)

    /* renamed from: oC */
    public Asset f5727oC;
    @C0064Am(aul = "ed0f7c15c4bbd0074919a470426f1f39", aum = 4)

    /* renamed from: oD */
    public boolean f5728oD;
    @C0064Am(aul = "c99f0a048f6b305b4d74be0a0fe66e09", aum = 5)

    /* renamed from: oE */
    public boolean f5729oE;
    @C0064Am(aul = "da1e5405de1f1ec8d580b1f15a2e1359", aum = -2)

    /* renamed from: oF */
    public long f5730oF;
    @C0064Am(aul = "e10cd53ec32eb13854016ac8441ffa90", aum = -2)

    /* renamed from: oG */
    public long f5731oG;
    @C0064Am(aul = "59001477ecea68cfc7a289383f86e5a3", aum = -2)

    /* renamed from: oH */
    public long f5732oH;
    @C0064Am(aul = "77b79e4a03eab80470d3c0b2eb3c42d8", aum = -2)

    /* renamed from: oI */
    public long f5733oI;
    @C0064Am(aul = "ed0f7c15c4bbd0074919a470426f1f39", aum = -2)

    /* renamed from: oJ */
    public long f5734oJ;
    @C0064Am(aul = "c99f0a048f6b305b4d74be0a0fe66e09", aum = -2)

    /* renamed from: oK */
    public long f5735oK;
    @C0064Am(aul = "1b2f5bc5de59b6cdd54c6db9edd62e6f", aum = -2)

    /* renamed from: oL */
    public long f5736oL;
    @C0064Am(aul = "da1e5405de1f1ec8d580b1f15a2e1359", aum = -1)

    /* renamed from: oM */
    public byte f5737oM;
    @C0064Am(aul = "e10cd53ec32eb13854016ac8441ffa90", aum = -1)

    /* renamed from: oN */
    public byte f5738oN;
    @C0064Am(aul = "59001477ecea68cfc7a289383f86e5a3", aum = -1)

    /* renamed from: oO */
    public byte f5739oO;
    @C0064Am(aul = "77b79e4a03eab80470d3c0b2eb3c42d8", aum = -1)

    /* renamed from: oP */
    public byte f5740oP;
    @C0064Am(aul = "ed0f7c15c4bbd0074919a470426f1f39", aum = -1)

    /* renamed from: oQ */
    public byte f5741oQ;
    @C0064Am(aul = "c99f0a048f6b305b4d74be0a0fe66e09", aum = -1)

    /* renamed from: oR */
    public byte f5742oR;
    @C0064Am(aul = "1b2f5bc5de59b6cdd54c6db9edd62e6f", aum = -1)

    /* renamed from: oS */
    public byte f5743oS;
    @C0064Am(aul = "54c72b6d92464af8a154c58051928eaf", aum = -2)

    /* renamed from: ok */
    public long f5744ok;
    @C0064Am(aul = "54c72b6d92464af8a154c58051928eaf", aum = -1)

    /* renamed from: on */
    public byte f5745on;
    @C0064Am(aul = "da1e5405de1f1ec8d580b1f15a2e1359", aum = 0)

    /* renamed from: oz */
    public String f5746oz;

    public C2024bC() {
    }

    public C2024bC(C1616Xf xf) {
        super(xf);
    }

    /* renamed from: c */
    public C5663aRz[] mo309c() {
        return C6909avx._m_fields;
    }

    /* renamed from: d */
    public void mo310d() {
        super.mo310d();
        this.f5746oz = null;
        this.f5725oA = null;
        this.f5726oB = null;
        this.f5727oC = null;
        this.f5728oD = false;
        this.f5729oE = false;
        this.f5724bK = null;
        this.handle = null;
        this.f5730oF = 0;
        this.f5731oG = 0;
        this.f5732oH = 0;
        this.f5733oI = 0;
        this.f5734oJ = 0;
        this.f5735oK = 0;
        this.f5736oL = 0;
        this.f5744ok = 0;
        this.f5737oM = 0;
        this.f5738oN = 0;
        this.f5739oO = 0;
        this.f5740oP = 0;
        this.f5741oQ = 0;
        this.f5742oR = 0;
        this.f5743oS = 0;
        this.f5745on = 0;
    }
}
