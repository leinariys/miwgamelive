package logic.data.mbean;

import game.script.WanderAndShootAIControllerType;
import logic.baa.C1616Xf;
import logic.res.code.C5663aRz;
import p001a.C0064Am;

/* renamed from: a.xv */
public class C3999xv extends C0757Kp {
    @C0064Am(aul = "ae4e0bbfd73bdc7d6a198dbc77874976", aum = -2)
    public long bGi;
    @C0064Am(aul = "e02f27cab17049288f288ebeb8cb528f", aum = -2)
    public long bGj;
    @C0064Am(aul = "218e375f8f8ab6ce928fda8d053b3fe9", aum = -2)
    public long bGk;
    @C0064Am(aul = "ae4e0bbfd73bdc7d6a198dbc77874976", aum = -1)
    public byte bGl;
    @C0064Am(aul = "e02f27cab17049288f288ebeb8cb528f", aum = -1)
    public byte bGm;
    @C0064Am(aul = "218e375f8f8ab6ce928fda8d053b3fe9", aum = -1)
    public byte bGn;
    @C0064Am(aul = "218e375f8f8ab6ce928fda8d053b3fe9", aum = 2)

    /* renamed from: dE */
    public float f9564dE;
    @C0064Am(aul = "ae4e0bbfd73bdc7d6a198dbc77874976", aum = 0)
    public float shootAngle;
    @C0064Am(aul = "e02f27cab17049288f288ebeb8cb528f", aum = 1)
    public boolean shootPredictMovimet;

    public C3999xv() {
    }

    public C3999xv(C1616Xf xf) {
        super(xf);
    }

    /* renamed from: c */
    public C5663aRz[] mo309c() {
        return WanderAndShootAIControllerType._m_fields;
    }

    /* renamed from: d */
    public void mo310d() {
        super.mo310d();
        this.shootAngle = 0.0f;
        this.shootPredictMovimet = false;
        this.f9564dE = 0.0f;
        this.bGi = 0;
        this.bGj = 0;
        this.bGk = 0;
        this.bGl = 0;
        this.bGm = 0;
        this.bGn = 0;
    }
}
