package logic.data.mbean;

import game.script.item.ItemType;
import game.script.storage.StorageTransaction;
import logic.baa.C1616Xf;
import logic.res.code.C5663aRz;
import p001a.C0064Am;

/* renamed from: a.Gf */
public class C0482Gf extends C3805vD {
    @C0064Am(aul = "6130d0f569b0090c019f9db00d1468a0", aum = 0)

    /* renamed from: Af */
    public String f641Af;
    @C0064Am(aul = "ce0495e25166c22279b4dd5f572cfa93", aum = -2)
    public long aLG;
    @C0064Am(aul = "ce0495e25166c22279b4dd5f572cfa93", aum = -1)
    public byte aLL;
    @C0064Am(aul = "73fed8ed9576502d3c2e16d5e5b8b31d", aum = -2)
    public long bmV;
    @C0064Am(aul = "73fed8ed9576502d3c2e16d5e5b8b31d", aum = -1)
    public byte bmW;
    @C0064Am(aul = "b060eb2c79af4af80aec43e098520591", aum = 4)
    public StorageTransaction.C3839a cXC;
    @C0064Am(aul = "6130d0f569b0090c019f9db00d1468a0", aum = -2)
    public long cXD;
    @C0064Am(aul = "720460d2b2682c555b9fd8db34f6e23d", aum = -2)
    public long cXE;
    @C0064Am(aul = "b060eb2c79af4af80aec43e098520591", aum = -2)
    public long cXF;
    @C0064Am(aul = "6130d0f569b0090c019f9db00d1468a0", aum = -1)
    public byte cXG;
    @C0064Am(aul = "720460d2b2682c555b9fd8db34f6e23d", aum = -1)
    public byte cXH;
    @C0064Am(aul = "b060eb2c79af4af80aec43e098520591", aum = -1)
    public byte cXI;
    @C0064Am(aul = "73fed8ed9576502d3c2e16d5e5b8b31d", aum = 2)

    /* renamed from: cw */
    public ItemType f642cw;
    @C0064Am(aul = "ce0495e25166c22279b4dd5f572cfa93", aum = 3)

    /* renamed from: cy */
    public int f643cy;
    @C0064Am(aul = "720460d2b2682c555b9fd8db34f6e23d", aum = 1)
    public long timestamp;

    public C0482Gf() {
    }

    public C0482Gf(C1616Xf xf) {
        super(xf);
    }

    /* renamed from: c */
    public C5663aRz[] mo309c() {
        return StorageTransaction._m_fields;
    }

    /* renamed from: d */
    public void mo310d() {
        super.mo310d();
        this.f641Af = null;
        this.timestamp = 0;
        this.f642cw = null;
        this.f643cy = 0;
        this.cXC = null;
        this.cXD = 0;
        this.cXE = 0;
        this.bmV = 0;
        this.aLG = 0;
        this.cXF = 0;
        this.cXG = 0;
        this.cXH = 0;
        this.bmW = 0;
        this.aLL = 0;
        this.cXI = 0;
    }
}
