package logic.data.mbean;

import game.network.message.serializable.C1556Wo;
import game.network.message.serializable.C3438ra;
import game.script.corporation.CorporationDefaults;
import game.script.corporation.CorporationPermissionDefaults;
import game.script.corporation.CorporationRoleDefaults;
import game.script.resource.Asset;
import logic.baa.C1616Xf;
import logic.res.code.C5663aRz;
import p001a.C0064Am;
import p001a.C5741aUz;
import p001a.C6704asA;

import java.util.UUID;

/* renamed from: a.arB  reason: case insensitive filesystem */
public class C6653arB extends C5292aDs {
    @C0064Am(aul = "243c3d518bfc72a4ca1908ce55f31fed", aum = 4)

    /* renamed from: bK */
    public UUID f5214bK;
    @C0064Am(aul = "d95b92d54e7d30d786a6700e379e2f79", aum = 0)
    public C3438ra<Asset> chT;
    @C0064Am(aul = "029920bf9e6649221499fd6767c187a5", aum = 1)
    public C1556Wo<C5741aUz, CorporationRoleDefaults> chV;
    @C0064Am(aul = "a067fa3de8b3945d06c333cee9fc56dc", aum = 2)
    public C1556Wo<C6704asA, CorporationPermissionDefaults> chX;
    @C0064Am(aul = "bc14787892595c4c52bfe578f4d9f228", aum = 3)
    public int chZ;
    @C0064Am(aul = "bc14787892595c4c52bfe578f4d9f228", aum = -2)
    public long gsA;
    @C0064Am(aul = "d95b92d54e7d30d786a6700e379e2f79", aum = -1)
    public byte gsB;
    @C0064Am(aul = "029920bf9e6649221499fd6767c187a5", aum = -1)
    public byte gsC;
    @C0064Am(aul = "a067fa3de8b3945d06c333cee9fc56dc", aum = -1)
    public byte gsD;
    @C0064Am(aul = "bc14787892595c4c52bfe578f4d9f228", aum = -1)
    public byte gsE;
    @C0064Am(aul = "d95b92d54e7d30d786a6700e379e2f79", aum = -2)
    public long gsx;
    @C0064Am(aul = "029920bf9e6649221499fd6767c187a5", aum = -2)
    public long gsy;
    @C0064Am(aul = "a067fa3de8b3945d06c333cee9fc56dc", aum = -2)
    public long gsz;
    @C0064Am(aul = "243c3d518bfc72a4ca1908ce55f31fed", aum = -2)

    /* renamed from: oL */
    public long f5215oL;
    @C0064Am(aul = "243c3d518bfc72a4ca1908ce55f31fed", aum = -1)

    /* renamed from: oS */
    public byte f5216oS;

    public C6653arB() {
    }

    public C6653arB(C1616Xf xf) {
        super(xf);
    }

    /* renamed from: c */
    public C5663aRz[] mo309c() {
        return CorporationDefaults._m_fields;
    }

    /* renamed from: d */
    public void mo310d() {
        super.mo310d();
        this.chT = null;
        this.chV = null;
        this.chX = null;
        this.chZ = 0;
        this.f5214bK = null;
        this.gsx = 0;
        this.gsy = 0;
        this.gsz = 0;
        this.gsA = 0;
        this.f5215oL = 0;
        this.gsB = 0;
        this.gsC = 0;
        this.gsD = 0;
        this.gsE = 0;
        this.f5216oS = 0;
    }
}
