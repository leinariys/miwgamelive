package logic.data.mbean;

import game.script.itemgen.MissionItemGenSet;
import game.script.mission.MissionTemplate;
import logic.baa.C1616Xf;
import logic.res.code.C5663aRz;
import p001a.C0064Am;

/* renamed from: a.aVK */
public class aVK extends C5389aHl {
    @C0064Am(aul = "a5989efd6f51eff1b4ec7bbbeac57cc4", aum = 0)
    public MissionTemplate baM;
    @C0064Am(aul = "a5989efd6f51eff1b4ec7bbbeac57cc4", aum = -2)
    public long baN;
    @C0064Am(aul = "a5989efd6f51eff1b4ec7bbbeac57cc4", aum = -1)
    public byte baO;

    public aVK() {
    }

    public aVK(C1616Xf xf) {
        super(xf);
    }

    /* renamed from: c */
    public C5663aRz[] mo309c() {
        return MissionItemGenSet._m_fields;
    }

    /* renamed from: d */
    public void mo310d() {
        super.mo310d();
        this.baM = null;
        this.baN = 0;
        this.baO = 0;
    }
}
