package logic.data.mbean;

import game.network.message.serializable.C3438ra;
import game.script.agent.AgentType;
import game.script.mission.MissionTemplate;
import game.script.npc.NPCType;
import game.script.resource.Asset;
import logic.baa.C1616Xf;
import logic.res.code.C5663aRz;
import p001a.C0064Am;
import p001a.C4045yZ;
import taikodom.infra.script.I18NString;

import java.util.UUID;

/* renamed from: a.Zw */
public class C1756Zw extends C3805vD {
    @C0064Am(aul = "e8bb52dcbe49f51b58c4110be1d08267", aum = 15)

    /* renamed from: BO */
    public int f2241BO;
    @C0064Am(aul = "635f871eb8f677762484a185dd6c8e6f", aum = 11)

    /* renamed from: Df */
    public boolean f2242Df;
    @C0064Am(aul = "16bfed488d94be91915de8cd37ca1a18", aum = 8)

    /* renamed from: E */
    public long f2243E;
    @C0064Am(aul = "16bfed488d94be91915de8cd37ca1a18", aum = -2)
    public long aTX;
    @C0064Am(aul = "16bfed488d94be91915de8cd37ca1a18", aum = -1)
    public byte aTY;
    @C0064Am(aul = "e8bb52dcbe49f51b58c4110be1d08267", aum = -1)
    public byte azE;
    @C0064Am(aul = "e8bb52dcbe49f51b58c4110be1d08267", aum = -2)
    public long azp;
    @C0064Am(aul = "07d6f56499cb8ec9287c795e47c8051c", aum = 18)

    /* renamed from: bK */
    public UUID f2244bK;
    @C0064Am(aul = "e95eabb42293c2f61e67ba06e48ba0b4", aum = 17)
    public Asset bfO;
    @C0064Am(aul = "40a1070b4cdab4c39e36d02fbb854307", aum = -2)
    public long eOA;
    @C0064Am(aul = "4dc6c2401bfd536e4ba8fa188f78ba78", aum = -2)
    public long eOB;
    @C0064Am(aul = "506d50fdcd306ff559c3a215a2fbdfb5", aum = -2)
    public long eOC;
    @C0064Am(aul = "07229a7365ab7eab75a00de9cf42fe35", aum = -1)
    public byte eOD;
    @C0064Am(aul = "af6fe1b05998f9551b99f633302fd75a", aum = -1)
    public byte eOE;
    @C0064Am(aul = "e1c851e2b1c3f1e74553b1a1b50b875a", aum = -1)
    public byte eOF;
    @C0064Am(aul = "b28fbdeb613584b709a0bdb2ef2ace34", aum = -1)
    public byte eOG;
    @C0064Am(aul = "2c9b032b586e63862af93939dc8c78d7", aum = -1)
    public byte eOH;
    @C0064Am(aul = "93d9b483a38cdf232454a48d498ae4b3", aum = -1)
    public byte eOI;
    @C0064Am(aul = "54db8f2fa01eea0857e3bb77497402a1", aum = -1)
    public byte eOJ;
    @C0064Am(aul = "e3a46ee3f04e1aaaf2994964834e4ef0", aum = -1)
    public byte eOK;
    @C0064Am(aul = "40a1070b4cdab4c39e36d02fbb854307", aum = -1)
    public byte eOL;
    @C0064Am(aul = "4dc6c2401bfd536e4ba8fa188f78ba78", aum = -1)
    public byte eOM;
    @C0064Am(aul = "506d50fdcd306ff559c3a215a2fbdfb5", aum = -1)
    public byte eON;
    @C0064Am(aul = "07229a7365ab7eab75a00de9cf42fe35", aum = 0)
    public NPCType eOh;
    @C0064Am(aul = "af6fe1b05998f9551b99f633302fd75a", aum = 1)
    public AgentType eOi;
    @C0064Am(aul = "e1c851e2b1c3f1e74553b1a1b50b875a", aum = 5)
    public I18NString eOj;
    @C0064Am(aul = "b28fbdeb613584b709a0bdb2ef2ace34", aum = 6)
    public boolean eOk;
    @C0064Am(aul = "2c9b032b586e63862af93939dc8c78d7", aum = 7)
    public I18NString eOl;
    @C0064Am(aul = "93d9b483a38cdf232454a48d498ae4b3", aum = 9)
    public C3438ra<ItemTypeTableItem> eOm;
    @C0064Am(aul = "54db8f2fa01eea0857e3bb77497402a1", aum = 10)
    public C3438ra<ItemTypeTableItem> eOn;
    @C0064Am(aul = "e3a46ee3f04e1aaaf2994964834e4ef0", aum = 12)
    public float eOo;
    @C0064Am(aul = "40a1070b4cdab4c39e36d02fbb854307", aum = 13)
    public C4045yZ.C4046a eOp;
    @C0064Am(aul = "4dc6c2401bfd536e4ba8fa188f78ba78", aum = 14)
    public int eOq;
    @C0064Am(aul = "506d50fdcd306ff559c3a215a2fbdfb5", aum = 16)
    public boolean eOr;
    @C0064Am(aul = "07229a7365ab7eab75a00de9cf42fe35", aum = -2)
    public long eOs;
    @C0064Am(aul = "af6fe1b05998f9551b99f633302fd75a", aum = -2)
    public long eOt;
    @C0064Am(aul = "e1c851e2b1c3f1e74553b1a1b50b875a", aum = -2)
    public long eOu;
    @C0064Am(aul = "b28fbdeb613584b709a0bdb2ef2ace34", aum = -2)
    public long eOv;
    @C0064Am(aul = "2c9b032b586e63862af93939dc8c78d7", aum = -2)
    public long eOw;
    @C0064Am(aul = "93d9b483a38cdf232454a48d498ae4b3", aum = -2)
    public long eOx;
    @C0064Am(aul = "54db8f2fa01eea0857e3bb77497402a1", aum = -2)
    public long eOy;
    @C0064Am(aul = "e3a46ee3f04e1aaaf2994964834e4ef0", aum = -2)
    public long eOz;
    @C0064Am(aul = "e95eabb42293c2f61e67ba06e48ba0b4", aum = -2)
    public long eaX;
    @C0064Am(aul = "e95eabb42293c2f61e67ba06e48ba0b4", aum = -1)
    public byte eba;
    @C0064Am(aul = "635f871eb8f677762484a185dd6c8e6f", aum = -2)
    public long ekI;
    @C0064Am(aul = "635f871eb8f677762484a185dd6c8e6f", aum = -1)
    public byte ekK;
    @C0064Am(aul = "5c863d31d77047b824bfa91c51b93b82", aum = 2)
    public String handle;
    @C0064Am(aul = "ccaefa7be9b5e7a29b4f7e4c70c8783f", aum = 4)

    /* renamed from: nh */
    public I18NString f2245nh;
    @C0064Am(aul = "bd5d2d7b177f325d9f7fbd95b43cdcda", aum = 3)

    /* renamed from: ni */
    public I18NString f2246ni;
    @C0064Am(aul = "ccaefa7be9b5e7a29b4f7e4c70c8783f", aum = -2)

    /* renamed from: nk */
    public long f2247nk;
    @C0064Am(aul = "bd5d2d7b177f325d9f7fbd95b43cdcda", aum = -2)

    /* renamed from: nl */
    public long f2248nl;
    @C0064Am(aul = "ccaefa7be9b5e7a29b4f7e4c70c8783f", aum = -1)

    /* renamed from: nn */
    public byte f2249nn;
    @C0064Am(aul = "bd5d2d7b177f325d9f7fbd95b43cdcda", aum = -1)

    /* renamed from: no */
    public byte f2250no;
    @C0064Am(aul = "07d6f56499cb8ec9287c795e47c8051c", aum = -2)

    /* renamed from: oL */
    public long f2251oL;
    @C0064Am(aul = "07d6f56499cb8ec9287c795e47c8051c", aum = -1)

    /* renamed from: oS */
    public byte f2252oS;
    @C0064Am(aul = "5c863d31d77047b824bfa91c51b93b82", aum = -2)

    /* renamed from: ok */
    public long f2253ok;
    @C0064Am(aul = "5c863d31d77047b824bfa91c51b93b82", aum = -1)

    /* renamed from: on */
    public byte f2254on;

    public C1756Zw() {
    }

    public C1756Zw(C1616Xf xf) {
        super(xf);
    }

    /* renamed from: c */
    public C5663aRz[] mo309c() {
        return MissionTemplate._m_fields;
    }

    /* renamed from: d */
    public void mo310d() {
        super.mo310d();
        this.eOh = null;
        this.eOi = null;
        this.handle = null;
        this.f2246ni = null;
        this.f2245nh = null;
        this.eOj = null;
        this.eOk = false;
        this.eOl = null;
        this.f2243E = 0;
        this.eOm = null;
        this.eOn = null;
        this.f2242Df = false;
        this.eOo = 0.0f;
        this.eOp = null;
        this.eOq = 0;
        this.f2241BO = 0;
        this.eOr = false;
        this.bfO = null;
        this.f2244bK = null;
        this.eOs = 0;
        this.eOt = 0;
        this.f2253ok = 0;
        this.f2248nl = 0;
        this.f2247nk = 0;
        this.eOu = 0;
        this.eOv = 0;
        this.eOw = 0;
        this.aTX = 0;
        this.eOx = 0;
        this.eOy = 0;
        this.ekI = 0;
        this.eOz = 0;
        this.eOA = 0;
        this.eOB = 0;
        this.azp = 0;
        this.eOC = 0;
        this.eaX = 0;
        this.f2251oL = 0;
        this.eOD = 0;
        this.eOE = 0;
        this.f2254on = 0;
        this.f2250no = 0;
        this.f2249nn = 0;
        this.eOF = 0;
        this.eOG = 0;
        this.eOH = 0;
        this.aTY = 0;
        this.eOI = 0;
        this.eOJ = 0;
        this.ekK = 0;
        this.eOK = 0;
        this.eOL = 0;
        this.eOM = 0;
        this.azE = 0;
        this.eON = 0;
        this.eba = 0;
        this.f2252oS = 0;
    }
}
