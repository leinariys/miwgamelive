package logic.data.mbean;

import game.network.message.serializable.C3438ra;
import game.script.mission.scripting.BuyOutpostMissionScript;
import game.script.ship.Outpost;
import game.script.ship.OutpostActivationItem;
import logic.baa.C1616Xf;
import logic.res.code.C5663aRz;
import p001a.C0064Am;

/* renamed from: a.aHR */
public class aHR extends aMY {
    @C0064Am(aul = "d54556d7b9b853dc7ada7092b0ce9481", aum = -2)
    public long dCT;
    @C0064Am(aul = "d54556d7b9b853dc7ada7092b0ce9481", aum = -1)
    public byte dCV;
    @C0064Am(aul = "65ff848687cf47b424628688828a40ba", aum = 0)
    public Outpost hYU;
    @C0064Am(aul = "d54556d7b9b853dc7ada7092b0ce9481", aum = 1)
    public C3438ra<OutpostActivationItem> hYV;
    @C0064Am(aul = "65ff848687cf47b424628688828a40ba", aum = -2)
    public long hYW;
    @C0064Am(aul = "65ff848687cf47b424628688828a40ba", aum = -1)
    public byte hYX;

    public aHR() {
    }

    public aHR(C1616Xf xf) {
        super(xf);
    }

    /* renamed from: c */
    public C5663aRz[] mo309c() {
        return BuyOutpostMissionScript._m_fields;
    }

    /* renamed from: d */
    public void mo310d() {
        super.mo310d();
        this.hYU = null;
        this.hYV = null;
        this.hYW = 0;
        this.dCT = 0;
        this.hYX = 0;
        this.dCV = 0;
    }
}
