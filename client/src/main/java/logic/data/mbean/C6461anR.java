package logic.data.mbean;

import game.network.message.serializable.C3438ra;
import game.script.citizenship.ItemReward;
import game.script.item.ItemType;
import logic.baa.C1616Xf;
import logic.res.code.C5663aRz;
import p001a.C0064Am;

/* renamed from: a.anR  reason: case insensitive filesystem */
public class C6461anR extends C1473Vg {
    @C0064Am(aul = "45816a076d8d222785d29143c657a491", aum = -2)
    public long eZO;
    @C0064Am(aul = "45816a076d8d222785d29143c657a491", aum = -1)
    public byte eZR;
    @C0064Am(aul = "45816a076d8d222785d29143c657a491", aum = 0)

    /* renamed from: hC */
    public C3438ra<ItemType> f4954hC;

    public C6461anR() {
    }

    public C6461anR(C1616Xf xf) {
        super(xf);
    }

    /* renamed from: c */
    public C5663aRz[] mo309c() {
        return ItemReward._m_fields;
    }

    /* renamed from: d */
    public void mo310d() {
        super.mo310d();
        this.f4954hC = null;
        this.eZO = 0;
        this.eZR = 0;
    }
}
