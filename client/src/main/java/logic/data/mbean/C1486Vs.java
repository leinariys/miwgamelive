package logic.data.mbean;

import game.script.ship.Outpost;
import game.script.ship.OutpostUpgrade;
import logic.baa.C1616Xf;
import logic.res.code.C5663aRz;
import p001a.C0064Am;

/* renamed from: a.Vs */
public class C1486Vs extends C6757atB {
    @C0064Am(aul = "fd2b6ec7ace154f6c723eba9b62a8fec", aum = -2)

    /* renamed from: dl */
    public long f1917dl;
    @C0064Am(aul = "fd2b6ec7ace154f6c723eba9b62a8fec", aum = -1)

    /* renamed from: ds */
    public byte f1918ds;
    @C0064Am(aul = "fd2b6ec7ace154f6c723eba9b62a8fec", aum = 4)
    public Outpost esA;
    @C0064Am(aul = "65f91218137239a880edc1b4c9eebc79", aum = -2)
    public long esB;
    @C0064Am(aul = "0334d5ffc40d8d20238dbbb8f5805cfc", aum = -2)
    public long esC;
    @C0064Am(aul = "293d470ab2fd60432cc4783f252df625", aum = -2)
    public long esD;
    @C0064Am(aul = "464dc2dcc451130806a1de1602ccfb54", aum = -2)
    public long esE;
    @C0064Am(aul = "65f91218137239a880edc1b4c9eebc79", aum = -1)
    public byte esF;
    @C0064Am(aul = "0334d5ffc40d8d20238dbbb8f5805cfc", aum = -1)
    public byte esG;
    @C0064Am(aul = "293d470ab2fd60432cc4783f252df625", aum = -1)
    public byte esH;
    @C0064Am(aul = "464dc2dcc451130806a1de1602ccfb54", aum = -1)
    public byte esI;
    @C0064Am(aul = "65f91218137239a880edc1b4c9eebc79", aum = 0)
    public OutpostUpgrade esx;
    @C0064Am(aul = "0334d5ffc40d8d20238dbbb8f5805cfc", aum = 1)
    public float esy;
    @C0064Am(aul = "293d470ab2fd60432cc4783f252df625", aum = 2)
    public float esz;
    @C0064Am(aul = "464dc2dcc451130806a1de1602ccfb54", aum = 3)
    public boolean stopped;

    public C1486Vs() {
    }

    public C1486Vs(C1616Xf xf) {
        super(xf);
    }

    /* renamed from: c */
    public C5663aRz[] mo309c() {
        return Outpost.OutpostUpgradeStatus._m_fields;
    }

    /* renamed from: d */
    public void mo310d() {
        super.mo310d();
        this.esx = null;
        this.esy = 0.0f;
        this.esz = 0.0f;
        this.stopped = false;
        this.esA = null;
        this.esB = 0;
        this.esC = 0;
        this.esD = 0;
        this.esE = 0;
        this.f1917dl = 0;
        this.esF = 0;
        this.esG = 0;
        this.esH = 0;
        this.esI = 0;
        this.f1918ds = 0;
    }
}
