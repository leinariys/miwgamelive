package logic.data.mbean;

import game.network.message.serializable.C3438ra;
import game.script.map3d.Map3DDefaults;
import game.script.map3d.ZoomLevel;
import logic.baa.C1616Xf;
import logic.res.code.C5663aRz;
import p001a.C0064Am;

import java.util.UUID;

/* renamed from: a.aGZ */
public class aGZ extends C3805vD {
    @C0064Am(aul = "69ddc0f3e46a6ca807f49808a5771109", aum = 1)

    /* renamed from: bK */
    public UUID f2871bK;
    @C0064Am(aul = "065b1bd3642d98e938f073bb601577ff", aum = 0)
    public C3438ra<ZoomLevel> eXE;
    @C0064Am(aul = "065b1bd3642d98e938f073bb601577ff", aum = -2)
    public long hUf;
    @C0064Am(aul = "065b1bd3642d98e938f073bb601577ff", aum = -1)
    public byte hUg;
    @C0064Am(aul = "69ddc0f3e46a6ca807f49808a5771109", aum = -2)

    /* renamed from: oL */
    public long f2872oL;
    @C0064Am(aul = "69ddc0f3e46a6ca807f49808a5771109", aum = -1)

    /* renamed from: oS */
    public byte f2873oS;

    public aGZ() {
    }

    public aGZ(C1616Xf xf) {
        super(xf);
    }

    /* renamed from: c */
    public C5663aRz[] mo309c() {
        return Map3DDefaults._m_fields;
    }

    /* renamed from: d */
    public void mo310d() {
        super.mo310d();
        this.eXE = null;
        this.f2871bK = null;
        this.hUf = 0;
        this.f2872oL = 0;
        this.hUg = 0;
        this.f2873oS = 0;
    }
}
