package logic.data.mbean;

import game.script.mail.MailBox;
import game.script.player.Player;
import logic.baa.C1616Xf;
import logic.res.code.C5663aRz;
import p001a.C0064Am;

/* renamed from: a.aku  reason: case insensitive filesystem */
public class C6334aku extends C2272dV {
    @C0064Am(aul = "75e36be5574842e10671efec78cdb62c", aum = 2)

    /* renamed from: P */
    public Player f4809P;
    @C0064Am(aul = "75e36be5574842e10671efec78cdb62c", aum = -2)

    /* renamed from: ag */
    public long f4810ag;
    @C0064Am(aul = "85e86a9c0070f94f813aad8600da3e7c", aum = 0)
    public MailBox ath;
    @C0064Am(aul = "e4487c6581eb85c13b12d2005df0f97e", aum = 1)
    public MailBox atj;
    @C0064Am(aul = "75e36be5574842e10671efec78cdb62c", aum = -1)

    /* renamed from: av */
    public byte f4811av;
    @C0064Am(aul = "85e86a9c0070f94f813aad8600da3e7c", aum = -2)
    public long fTt;
    @C0064Am(aul = "e4487c6581eb85c13b12d2005df0f97e", aum = -2)
    public long fTu;
    @C0064Am(aul = "85e86a9c0070f94f813aad8600da3e7c", aum = -1)
    public byte fTv;
    @C0064Am(aul = "e4487c6581eb85c13b12d2005df0f97e", aum = -1)
    public byte fTw;

    public C6334aku() {
    }

    public C6334aku(C1616Xf xf) {
        super(xf);
    }

    /* renamed from: c */
    public C5663aRz[] mo309c() {
        return PlayerSocialController._m_fields;
    }

    /* renamed from: d */
    public void mo310d() {
        super.mo310d();
        this.ath = null;
        this.atj = null;
        this.f4809P = null;
        this.fTt = 0;
        this.fTu = 0;
        this.f4810ag = 0;
        this.fTv = 0;
        this.fTw = 0;
        this.f4811av = 0;
    }
}
