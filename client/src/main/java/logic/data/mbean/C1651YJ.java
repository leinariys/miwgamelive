package logic.data.mbean;

import game.network.message.serializable.C3438ra;
import game.script.avatar.AvatarSector;
import game.script.avatar.TextureGrid;
import game.script.avatar.body.BodyPiece;
import logic.baa.C1616Xf;
import logic.res.code.C5663aRz;
import p001a.C0064Am;

import java.util.UUID;

/* renamed from: a.YJ */
public class C1651YJ extends C2424fK {
    @C0064Am(aul = "6a381dd68a2050f85a0037165de453b9", aum = 4)

    /* renamed from: bK */
    public UUID f2165bK;
    @C0064Am(aul = "76b406b7c04e4d8d0d1e2c8960abac97", aum = -2)
    public long bZA;
    @C0064Am(aul = "76b406b7c04e4d8d0d1e2c8960abac97", aum = -1)
    public byte bZB;
    @C0064Am(aul = "6801c57f2db6da4fd1745dbbcfd44347", aum = 2)
    public AvatarSector dTV;
    @C0064Am(aul = "b6aa394c386d92b00aea50ed103e3a7a", aum = 3)
    public C3438ra<TextureGrid> dTX;
    @C0064Am(aul = "6801c57f2db6da4fd1745dbbcfd44347", aum = -2)
    public long eMA;
    @C0064Am(aul = "b6aa394c386d92b00aea50ed103e3a7a", aum = -2)
    public long eMB;
    @C0064Am(aul = "6801c57f2db6da4fd1745dbbcfd44347", aum = -1)
    public byte eMC;
    @C0064Am(aul = "b6aa394c386d92b00aea50ed103e3a7a", aum = -1)
    public byte eMD;
    @C0064Am(aul = "0c7e6883f284bf9ab00e88d1b5449081", aum = 1)
    public int handle;
    @C0064Am(aul = "6a381dd68a2050f85a0037165de453b9", aum = -2)

    /* renamed from: oL */
    public long f2166oL;
    @C0064Am(aul = "6a381dd68a2050f85a0037165de453b9", aum = -1)

    /* renamed from: oS */
    public byte f2167oS;
    @C0064Am(aul = "0c7e6883f284bf9ab00e88d1b5449081", aum = -2)

    /* renamed from: ok */
    public long f2168ok;
    @C0064Am(aul = "0c7e6883f284bf9ab00e88d1b5449081", aum = -1)

    /* renamed from: on */
    public byte f2169on;
    @C0064Am(aul = "76b406b7c04e4d8d0d1e2c8960abac97", aum = 0)

    /* renamed from: zR */
    public String f2170zR;

    public C1651YJ() {
    }

    public C1651YJ(C1616Xf xf) {
        super(xf);
    }

    /* renamed from: c */
    public C5663aRz[] mo309c() {
        return BodyPiece._m_fields;
    }

    /* renamed from: d */
    public void mo310d() {
        super.mo310d();
        this.f2170zR = null;
        this.handle = 0;
        this.dTV = null;
        this.dTX = null;
        this.f2165bK = null;
        this.bZA = 0;
        this.f2168ok = 0;
        this.eMA = 0;
        this.eMB = 0;
        this.f2166oL = 0;
        this.bZB = 0;
        this.f2169on = 0;
        this.eMC = 0;
        this.eMD = 0;
        this.f2167oS = 0;
    }
}
