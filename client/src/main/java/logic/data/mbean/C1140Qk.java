package logic.data.mbean;

import game.script.item.buff.amplifier.SpeedBoostAmplifier;
import game.script.ship.speedBoost.SpeedBoostAdapter;
import logic.baa.C1616Xf;
import logic.res.code.C5663aRz;
import p001a.C0064Am;
import p001a.C3892wO;

/* renamed from: a.Qk */
public class C1140Qk extends C3108nv {
    @C0064Am(aul = "c1bb226019458832d3fbb1658cafa78b", aum = 1)

    /* renamed from: Vs */
    public C3892wO f1455Vs;
    @C0064Am(aul = "d15f2bde2981e220628eb6ad8d350621", aum = 2)

    /* renamed from: Vu */
    public C3892wO f1456Vu;
    @C0064Am(aul = "ca5a9e056d1f9608cf7cd610b9ff2613", aum = 4)

    /* renamed from: Vw */
    public C3892wO f1457Vw;
    @C0064Am(aul = "d2ac6f03cac3f05a7df1d0a28d3e290b", aum = 3)

    /* renamed from: Vy */
    public C3892wO f1458Vy;
    @C0064Am(aul = "c1bb226019458832d3fbb1658cafa78b", aum = -1)

    /* renamed from: YC */
    public byte f1459YC;
    @C0064Am(aul = "d15f2bde2981e220628eb6ad8d350621", aum = -1)

    /* renamed from: YD */
    public byte f1460YD;
    @C0064Am(aul = "ca5a9e056d1f9608cf7cd610b9ff2613", aum = -1)

    /* renamed from: YE */
    public byte f1461YE;
    @C0064Am(aul = "d2ac6f03cac3f05a7df1d0a28d3e290b", aum = -1)

    /* renamed from: YF */
    public byte f1462YF;
    @C0064Am(aul = "c1bb226019458832d3fbb1658cafa78b", aum = -2)

    /* renamed from: Yw */
    public long f1463Yw;
    @C0064Am(aul = "d15f2bde2981e220628eb6ad8d350621", aum = -2)

    /* renamed from: Yx */
    public long f1464Yx;
    @C0064Am(aul = "ca5a9e056d1f9608cf7cd610b9ff2613", aum = -2)

    /* renamed from: Yy */
    public long f1465Yy;
    @C0064Am(aul = "d2ac6f03cac3f05a7df1d0a28d3e290b", aum = -2)

    /* renamed from: Yz */
    public long f1466Yz;
    @C0064Am(aul = "2919c0bbb8ad22015768e5642b9f5759", aum = -2)
    public long aIu;
    @C0064Am(aul = "2919c0bbb8ad22015768e5642b9f5759", aum = -1)
    public byte aIx;
    @C0064Am(aul = "2919c0bbb8ad22015768e5642b9f5759", aum = 6)
    public boolean active;
    @C0064Am(aul = "eba66e6e4f4e38575514e69c4902f35e", aum = 0)
    public C3892wO atR;
    @C0064Am(aul = "eba66e6e4f4e38575514e69c4902f35e", aum = -2)
    public long cAA;
    @C0064Am(aul = "eba66e6e4f4e38575514e69c4902f35e", aum = -1)
    public byte cAO;
    @C0064Am(aul = "c49111a8aee162d4980d1db2a2b5270a", aum = -2)
    public long dVm;
    @C0064Am(aul = "c49111a8aee162d4980d1db2a2b5270a", aum = -1)
    public byte dVn;
    @C0064Am(aul = "c49111a8aee162d4980d1db2a2b5270a", aum = 5)
    public SpeedBoostAdapter duF;

    public C1140Qk() {
    }

    public C1140Qk(C1616Xf xf) {
        super(xf);
    }

    /* renamed from: c */
    public C5663aRz[] mo309c() {
        return SpeedBoostAmplifier._m_fields;
    }

    /* renamed from: d */
    public void mo310d() {
        super.mo310d();
        this.atR = null;
        this.f1455Vs = null;
        this.f1456Vu = null;
        this.f1458Vy = null;
        this.f1457Vw = null;
        this.duF = null;
        this.active = false;
        this.cAA = 0;
        this.f1463Yw = 0;
        this.f1464Yx = 0;
        this.f1466Yz = 0;
        this.f1465Yy = 0;
        this.dVm = 0;
        this.aIu = 0;
        this.cAO = 0;
        this.f1459YC = 0;
        this.f1460YD = 0;
        this.f1462YF = 0;
        this.f1461YE = 0;
        this.dVn = 0;
        this.aIx = 0;
    }
}
