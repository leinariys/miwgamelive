package logic.data.mbean;

import game.script.npcchat.actions.RemoveParamSpeechAction;
import logic.baa.C1616Xf;
import logic.res.code.C5663aRz;
import p001a.C0064Am;

/* renamed from: a.oX */
public class C3160oX extends C6352alM {
    @C0064Am(aul = "70a5432f66090c9a6677621ec7098d0c", aum = -2)

    /* renamed from: IC */
    public long f8770IC;
    @C0064Am(aul = "70a5432f66090c9a6677621ec7098d0c", aum = -1)

    /* renamed from: IE */
    public byte f8771IE;
    @C0064Am(aul = "70a5432f66090c9a6677621ec7098d0c", aum = 0)
    public String key;

    public C3160oX() {
    }

    public C3160oX(C1616Xf xf) {
        super(xf);
    }

    /* renamed from: c */
    public C5663aRz[] mo309c() {
        return RemoveParamSpeechAction._m_fields;
    }

    /* renamed from: d */
    public void mo310d() {
        super.mo310d();
        this.key = null;
        this.f8770IC = 0;
        this.f8771IE = 0;
    }
}
