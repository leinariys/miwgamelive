package logic.data.mbean;

import game.script.Character;
import game.script.ai.npc.TrackShipAIController;
import game.script.ship.Ship;
import logic.baa.C1616Xf;
import logic.res.code.C5663aRz;
import p001a.C0064Am;

/* renamed from: a.aOo  reason: case insensitive filesystem */
public class C5574aOo extends C6937awc {
    @C0064Am(aul = "860d72f63518be54661cb8f52b8bdc17", aum = -2)
    public long iyH;
    @C0064Am(aul = "229a6f579084f76e5f33bac4c6439582", aum = -2)
    public long iyI;
    @C0064Am(aul = "860d72f63518be54661cb8f52b8bdc17", aum = -1)
    public byte iyJ;
    @C0064Am(aul = "229a6f579084f76e5f33bac4c6439582", aum = -1)
    public byte iyK;
    @C0064Am(aul = "229a6f579084f76e5f33bac4c6439582", aum = 1)
    public Character trackedPilot;
    @C0064Am(aul = "860d72f63518be54661cb8f52b8bdc17", aum = 0)
    public Ship trackedShip;

    public C5574aOo() {
    }

    public C5574aOo(C1616Xf xf) {
        super(xf);
    }

    /* renamed from: c */
    public C5663aRz[] mo309c() {
        return TrackShipAIController._m_fields;
    }

    /* renamed from: d */
    public void mo310d() {
        super.mo310d();
        this.trackedShip = null;
        this.trackedPilot = null;
        this.iyH = 0;
        this.iyI = 0;
        this.iyJ = 0;
        this.iyK = 0;
    }
}
