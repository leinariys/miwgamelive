package logic.data.mbean;

import game.network.message.serializable.C2686iZ;
import game.script.util.AdapterLikedList;
import game.script.util.GameObjectAdapter;
import logic.baa.C1616Xf;
import logic.res.code.C5663aRz;
import p001a.C0064Am;
import p001a.C6872avM;

/* renamed from: a.aEf  reason: case insensitive filesystem */
public class C5305aEf extends C3805vD {
    @C0064Am(aul = "1b9fd33043acd5de1d33f4a34115c7a5", aum = 0)
    public C2686iZ<C6872avM> bNg;
    @C0064Am(aul = "4f115a53d058a1e3ae7ca605992d0cc4", aum = 1)
    public GameObjectAdapter bZI;
    @C0064Am(aul = "a4e6c2a002b40a8f5ab228dbd4bb31ba", aum = 2)
    public GameObjectAdapter bZK;
    @C0064Am(aul = "1b9fd33043acd5de1d33f4a34115c7a5", aum = -2)
    public long eqr;
    @C0064Am(aul = "1b9fd33043acd5de1d33f4a34115c7a5", aum = -1)
    public byte eqs;
    @C0064Am(aul = "4f115a53d058a1e3ae7ca605992d0cc4", aum = -2)
    public long hEV;
    @C0064Am(aul = "a4e6c2a002b40a8f5ab228dbd4bb31ba", aum = -2)
    public long hEW;
    @C0064Am(aul = "4f115a53d058a1e3ae7ca605992d0cc4", aum = -1)
    public byte hEX;
    @C0064Am(aul = "a4e6c2a002b40a8f5ab228dbd4bb31ba", aum = -1)
    public byte hEY;

    public C5305aEf() {
    }

    public C5305aEf(C1616Xf xf) {
        super(xf);
    }

    /* renamed from: c */
    public C5663aRz[] mo309c() {
        return AdapterLikedList._m_fields;
    }

    /* renamed from: d */
    public void mo310d() {
        super.mo310d();
        this.bNg = null;
        this.bZI = null;
        this.bZK = null;
        this.eqr = 0;
        this.hEV = 0;
        this.hEW = 0;
        this.eqs = 0;
        this.hEX = 0;
        this.hEY = 0;
    }
}
