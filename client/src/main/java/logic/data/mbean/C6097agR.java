package logic.data.mbean;

import game.geometry.Vec3d;
import logic.baa.C1616Xf;
import logic.res.code.C5663aRz;
import p001a.C0064Am;
import taikodom.game.script.p003ai.npc.DroneAIController;

/* renamed from: a.agR  reason: case insensitive filesystem */
public class C6097agR extends C5574aOo {
    @C0064Am(aul = "1c9bf9fc59619784448714e8f5b3bc72", aum = -2)
    public long aPL;
    @C0064Am(aul = "94c1c6262c401c7ccc61cc034f0b4c6d", aum = -2)
    public long aPM;
    @C0064Am(aul = "1c9bf9fc59619784448714e8f5b3bc72", aum = -1)
    public byte aPN;
    @C0064Am(aul = "94c1c6262c401c7ccc61cc034f0b4c6d", aum = -1)
    public byte aPO;
    @C0064Am(aul = "81f8d696a198ba8f66b2be341d1702b5", aum = -2)
    public long aps;
    @C0064Am(aul = "81f8d696a198ba8f66b2be341d1702b5", aum = -1)
    public byte apx;
    @C0064Am(aul = "b46ec6bb2d1b87776665fe57511c5304", aum = -2)
    public long bGi;
    @C0064Am(aul = "dd38a465d05ec5edbc912cb1b63b67f1", aum = -2)
    public long bGj;
    @C0064Am(aul = "b46ec6bb2d1b87776665fe57511c5304", aum = -1)
    public byte bGl;
    @C0064Am(aul = "dd38a465d05ec5edbc912cb1b63b67f1", aum = -1)
    public byte bGm;
    @C0064Am(aul = "1c9bf9fc59619784448714e8f5b3bc72", aum = 9)
    public Vec3d center;
    @C0064Am(aul = "bd86b27f4e83a67a4376cdc24ba963cf", aum = 4)
    public float dodgeAngle;
    @C0064Am(aul = "50c4261e3302f8298174ec3e55bfea00", aum = 5)
    public boolean dodgeDisabled;
    @C0064Am(aul = "a5884c99335c0a8407ade29fb5870d8c", aum = 3)
    public float dodgeDistance;
    @C0064Am(aul = "76b86616644e9e43adea347ef4e45377", aum = 2)
    public float dodgeReactionTime;
    @C0064Am(aul = "80d9cfbd99b2604a5e557fe6f5d2d550", aum = -2)
    public long fFf;
    @C0064Am(aul = "0c05e37437d78dd34803de18c9c04663", aum = -2)
    public long fFg;
    @C0064Am(aul = "76b86616644e9e43adea347ef4e45377", aum = -2)
    public long fFh;
    @C0064Am(aul = "a5884c99335c0a8407ade29fb5870d8c", aum = -2)
    public long fFi;
    @C0064Am(aul = "bd86b27f4e83a67a4376cdc24ba963cf", aum = -2)
    public long fFj;
    @C0064Am(aul = "50c4261e3302f8298174ec3e55bfea00", aum = -2)
    public long fFk;
    @C0064Am(aul = "ba9e2042e11c56cdd27f4eea3b16f2b8", aum = -2)
    public long fFl;
    @C0064Am(aul = "80d9cfbd99b2604a5e557fe6f5d2d550", aum = -1)
    public byte fFm;
    @C0064Am(aul = "0c05e37437d78dd34803de18c9c04663", aum = -1)
    public byte fFn;
    @C0064Am(aul = "76b86616644e9e43adea347ef4e45377", aum = -1)
    public byte fFo;
    @C0064Am(aul = "a5884c99335c0a8407ade29fb5870d8c", aum = -1)
    public byte fFp;
    @C0064Am(aul = "bd86b27f4e83a67a4376cdc24ba963cf", aum = -1)
    public byte fFq;
    @C0064Am(aul = "50c4261e3302f8298174ec3e55bfea00", aum = -1)
    public byte fFr;
    @C0064Am(aul = "ba9e2042e11c56cdd27f4eea3b16f2b8", aum = -1)
    public byte fFs;
    @C0064Am(aul = "ba9e2042e11c56cdd27f4eea3b16f2b8", aum = 8)
    public float initialReactionTime;
    @C0064Am(aul = "80d9cfbd99b2604a5e557fe6f5d2d550", aum = 0)
    public float pursuitDistance;
    @C0064Am(aul = "0c05e37437d78dd34803de18c9c04663", aum = 1)
    public boolean pursuitPredictMovimet;
    @C0064Am(aul = "94c1c6262c401c7ccc61cc034f0b4c6d", aum = 10)
    public float radius2;
    @C0064Am(aul = "b46ec6bb2d1b87776665fe57511c5304", aum = 6)
    public float shootAngle;
    @C0064Am(aul = "dd38a465d05ec5edbc912cb1b63b67f1", aum = 7)
    public boolean shootPredictMovimet;
    @C0064Am(aul = "81f8d696a198ba8f66b2be341d1702b5", aum = 11)
    public long startTime;

    public C6097agR() {
    }

    public C6097agR(C1616Xf xf) {
        super(xf);
    }

    /* renamed from: c */
    public C5663aRz[] mo309c() {
        return DroneAIController._m_fields;
    }

    /* renamed from: d */
    public void mo310d() {
        super.mo310d();
        this.pursuitDistance = 0.0f;
        this.pursuitPredictMovimet = false;
        this.dodgeReactionTime = 0.0f;
        this.dodgeDistance = 0.0f;
        this.dodgeAngle = 0.0f;
        this.dodgeDisabled = false;
        this.shootAngle = 0.0f;
        this.shootPredictMovimet = false;
        this.initialReactionTime = 0.0f;
        this.center = null;
        this.radius2 = 0.0f;
        this.startTime = 0;
        this.fFf = 0;
        this.fFg = 0;
        this.fFh = 0;
        this.fFi = 0;
        this.fFj = 0;
        this.fFk = 0;
        this.bGi = 0;
        this.bGj = 0;
        this.fFl = 0;
        this.aPL = 0;
        this.aPM = 0;
        this.aps = 0;
        this.fFm = 0;
        this.fFn = 0;
        this.fFo = 0;
        this.fFp = 0;
        this.fFq = 0;
        this.fFr = 0;
        this.bGl = 0;
        this.bGm = 0;
        this.fFs = 0;
        this.aPN = 0;
        this.aPO = 0;
        this.apx = 0;
    }
}
