package logic.data.mbean;

import game.script.pda.TaikopediaEntry;
import logic.baa.C1616Xf;
import logic.res.code.C5663aRz;
import p001a.C0064Am;
import taikodom.infra.script.I18NString;

import java.util.UUID;

/* renamed from: a.aqM  reason: case insensitive filesystem */
public class C6612aqM extends C5292aDs {
    @C0064Am(aul = "8bac975bf95a6f90d68bec4b0b3989b7", aum = 3)

    /* renamed from: bK */
    public UUID f5156bK;
    @C0064Am(aul = "8c54a988dadf2d2e52cf4368f3e1db70", aum = 0)
    public String handle;
    @C0064Am(aul = "f906d758323f6e6b27cce4bc685b2a98", aum = 2)

    /* renamed from: nh */
    public I18NString f5157nh;
    @C0064Am(aul = "f6f5f4bfad64c72ce6a865acd7b342f6", aum = 1)

    /* renamed from: ni */
    public I18NString f5158ni;
    @C0064Am(aul = "f906d758323f6e6b27cce4bc685b2a98", aum = -2)

    /* renamed from: nk */
    public long f5159nk;
    @C0064Am(aul = "f6f5f4bfad64c72ce6a865acd7b342f6", aum = -2)

    /* renamed from: nl */
    public long f5160nl;
    @C0064Am(aul = "f906d758323f6e6b27cce4bc685b2a98", aum = -1)

    /* renamed from: nn */
    public byte f5161nn;
    @C0064Am(aul = "f6f5f4bfad64c72ce6a865acd7b342f6", aum = -1)

    /* renamed from: no */
    public byte f5162no;
    @C0064Am(aul = "8bac975bf95a6f90d68bec4b0b3989b7", aum = -2)

    /* renamed from: oL */
    public long f5163oL;
    @C0064Am(aul = "8bac975bf95a6f90d68bec4b0b3989b7", aum = -1)

    /* renamed from: oS */
    public byte f5164oS;
    @C0064Am(aul = "8c54a988dadf2d2e52cf4368f3e1db70", aum = -2)

    /* renamed from: ok */
    public long f5165ok;
    @C0064Am(aul = "8c54a988dadf2d2e52cf4368f3e1db70", aum = -1)

    /* renamed from: on */
    public byte f5166on;

    public C6612aqM() {
    }

    public C6612aqM(C1616Xf xf) {
        super(xf);
    }

    /* renamed from: c */
    public C5663aRz[] mo309c() {
        return TaikopediaEntry._m_fields;
    }

    /* renamed from: d */
    public void mo310d() {
        super.mo310d();
        this.handle = null;
        this.f5158ni = null;
        this.f5157nh = null;
        this.f5156bK = null;
        this.f5165ok = 0;
        this.f5160nl = 0;
        this.f5159nk = 0;
        this.f5163oL = 0;
        this.f5166on = 0;
        this.f5162no = 0;
        this.f5161nn = 0;
        this.f5164oS = 0;
    }
}
