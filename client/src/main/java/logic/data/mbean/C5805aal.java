package logic.data.mbean;

import game.script.nls.NLSParty;
import logic.baa.C1616Xf;
import logic.res.code.C5663aRz;
import p001a.C0064Am;
import taikodom.infra.script.I18NString;

/* renamed from: a.aal  reason: case insensitive filesystem */
public class C5805aal extends C2484fi {
    @C0064Am(aul = "6916656931f4329e3a1a2e88be5e1f2b", aum = 13)
    public I18NString alB;
    @C0064Am(aul = "6a4797922b91063f90a49344e2b29d07", aum = 14)
    public I18NString alD;
    @C0064Am(aul = "e3a02f0c5600fc6d67d6d84682896b8b", aum = 15)
    public I18NString alF;
    @C0064Am(aul = "53e02537cd9cc3691aafea010c000204", aum = 16)
    public I18NString alH;
    @C0064Am(aul = "9837aaa3fc6515a1cae47c9e08fef64c", aum = 0)
    public I18NString alb;
    @C0064Am(aul = "cbbfd63c739ea63c5ec045e7b66bec6c", aum = 1)
    public I18NString ald;
    @C0064Am(aul = "c5e04b67e701b4764c41683d3b0765cb", aum = 2)
    public I18NString alf;
    @C0064Am(aul = "2766a43fc39f366628a63d4a3377af29", aum = 3)
    public I18NString alh;
    @C0064Am(aul = "e5323f3e617d92941aa13ccfc0f2955a", aum = 4)
    public I18NString alj;
    @C0064Am(aul = "a4b7984a9e89e98bb557088dee92abec", aum = 5)
    public I18NString all;
    @C0064Am(aul = "0912ee2d979e603ce1888653af6ac1b2", aum = 6)
    public I18NString aln;
    @C0064Am(aul = "f1f29026db958ae5d65aba61f8a326e8", aum = 7)
    public I18NString alp;
    @C0064Am(aul = "7ba2101266c8f3c89c70e48d2d350827", aum = 8)
    public I18NString alr;
    @C0064Am(aul = "9e9d534007b04fb07f96adfd9cfd014f", aum = 9)
    public I18NString alt;
    @C0064Am(aul = "6c0b72e54e371aa55af8db9ec1013512", aum = 10)
    public I18NString alv;
    @C0064Am(aul = "bcedf9dcce4d79b8c429711fcbe5deff", aum = 11)
    public I18NString alx;
    @C0064Am(aul = "d89fc7fda636317b5bdca44cab1c966a", aum = 12)
    public I18NString alz;
    @C0064Am(aul = "9837aaa3fc6515a1cae47c9e08fef64c", aum = -2)
    public long eSC;
    @C0064Am(aul = "cbbfd63c739ea63c5ec045e7b66bec6c", aum = -2)
    public long eSD;
    @C0064Am(aul = "c5e04b67e701b4764c41683d3b0765cb", aum = -2)
    public long eSE;
    @C0064Am(aul = "2766a43fc39f366628a63d4a3377af29", aum = -2)
    public long eSF;
    @C0064Am(aul = "e5323f3e617d92941aa13ccfc0f2955a", aum = -2)
    public long eSG;
    @C0064Am(aul = "a4b7984a9e89e98bb557088dee92abec", aum = -2)
    public long eSH;
    @C0064Am(aul = "0912ee2d979e603ce1888653af6ac1b2", aum = -2)
    public long eSI;
    @C0064Am(aul = "f1f29026db958ae5d65aba61f8a326e8", aum = -2)
    public long eSJ;
    @C0064Am(aul = "7ba2101266c8f3c89c70e48d2d350827", aum = -2)
    public long eSK;
    @C0064Am(aul = "9e9d534007b04fb07f96adfd9cfd014f", aum = -2)
    public long eSL;
    @C0064Am(aul = "6c0b72e54e371aa55af8db9ec1013512", aum = -2)
    public long eSM;
    @C0064Am(aul = "bcedf9dcce4d79b8c429711fcbe5deff", aum = -2)
    public long eSN;
    @C0064Am(aul = "d89fc7fda636317b5bdca44cab1c966a", aum = -2)
    public long eSO;
    @C0064Am(aul = "6916656931f4329e3a1a2e88be5e1f2b", aum = -2)
    public long eSP;
    @C0064Am(aul = "6a4797922b91063f90a49344e2b29d07", aum = -2)
    public long eSQ;
    @C0064Am(aul = "e3a02f0c5600fc6d67d6d84682896b8b", aum = -2)
    public long eSR;
    @C0064Am(aul = "53e02537cd9cc3691aafea010c000204", aum = -2)
    public long eSS;
    @C0064Am(aul = "9837aaa3fc6515a1cae47c9e08fef64c", aum = -1)
    public byte eST;
    @C0064Am(aul = "cbbfd63c739ea63c5ec045e7b66bec6c", aum = -1)
    public byte eSU;
    @C0064Am(aul = "c5e04b67e701b4764c41683d3b0765cb", aum = -1)
    public byte eSV;
    @C0064Am(aul = "2766a43fc39f366628a63d4a3377af29", aum = -1)
    public byte eSW;
    @C0064Am(aul = "e5323f3e617d92941aa13ccfc0f2955a", aum = -1)
    public byte eSX;
    @C0064Am(aul = "a4b7984a9e89e98bb557088dee92abec", aum = -1)
    public byte eSY;
    @C0064Am(aul = "0912ee2d979e603ce1888653af6ac1b2", aum = -1)
    public byte eSZ;
    @C0064Am(aul = "f1f29026db958ae5d65aba61f8a326e8", aum = -1)
    public byte eTa;
    @C0064Am(aul = "7ba2101266c8f3c89c70e48d2d350827", aum = -1)
    public byte eTb;
    @C0064Am(aul = "9e9d534007b04fb07f96adfd9cfd014f", aum = -1)
    public byte eTc;
    @C0064Am(aul = "6c0b72e54e371aa55af8db9ec1013512", aum = -1)
    public byte eTd;
    @C0064Am(aul = "bcedf9dcce4d79b8c429711fcbe5deff", aum = -1)
    public byte eTe;
    @C0064Am(aul = "d89fc7fda636317b5bdca44cab1c966a", aum = -1)
    public byte eTf;
    @C0064Am(aul = "6916656931f4329e3a1a2e88be5e1f2b", aum = -1)
    public byte eTg;
    @C0064Am(aul = "6a4797922b91063f90a49344e2b29d07", aum = -1)
    public byte eTh;
    @C0064Am(aul = "e3a02f0c5600fc6d67d6d84682896b8b", aum = -1)
    public byte eTi;
    @C0064Am(aul = "53e02537cd9cc3691aafea010c000204", aum = -1)
    public byte eTj;

    public C5805aal() {
    }

    public C5805aal(C1616Xf xf) {
        super(xf);
    }

    /* renamed from: c */
    public C5663aRz[] mo309c() {
        return NLSParty._m_fields;
    }

    /* renamed from: d */
    public void mo310d() {
        super.mo310d();
        this.alb = null;
        this.ald = null;
        this.alf = null;
        this.alh = null;
        this.alj = null;
        this.all = null;
        this.aln = null;
        this.alp = null;
        this.alr = null;
        this.alt = null;
        this.alv = null;
        this.alx = null;
        this.alz = null;
        this.alB = null;
        this.alD = null;
        this.alF = null;
        this.alH = null;
        this.eSC = 0;
        this.eSD = 0;
        this.eSE = 0;
        this.eSF = 0;
        this.eSG = 0;
        this.eSH = 0;
        this.eSI = 0;
        this.eSJ = 0;
        this.eSK = 0;
        this.eSL = 0;
        this.eSM = 0;
        this.eSN = 0;
        this.eSO = 0;
        this.eSP = 0;
        this.eSQ = 0;
        this.eSR = 0;
        this.eSS = 0;
        this.eST = 0;
        this.eSU = 0;
        this.eSV = 0;
        this.eSW = 0;
        this.eSX = 0;
        this.eSY = 0;
        this.eSZ = 0;
        this.eTa = 0;
        this.eTb = 0;
        this.eTc = 0;
        this.eTd = 0;
        this.eTe = 0;
        this.eTf = 0;
        this.eTg = 0;
        this.eTh = 0;
        this.eTi = 0;
        this.eTj = 0;
    }
}
