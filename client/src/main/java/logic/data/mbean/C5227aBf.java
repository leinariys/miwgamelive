package logic.data.mbean;

import game.network.message.serializable.C3438ra;
import game.script.progression.ProgressionAbility;
import game.script.progression.ProgressionCharacterTemplateAbilities;
import logic.baa.C1616Xf;
import logic.res.code.C5663aRz;
import p001a.C0064Am;

/* renamed from: a.aBf  reason: case insensitive filesystem */
public class C5227aBf extends C3805vD {
    @C0064Am(aul = "fd56c5c3e1a19eb009abc09a57234029", aum = 1)
    public Integer aMQ;
    @C0064Am(aul = "fd56c5c3e1a19eb009abc09a57234029", aum = -1)
    public byte azE;
    @C0064Am(aul = "fd56c5c3e1a19eb009abc09a57234029", aum = -2)
    public long azp;
    @C0064Am(aul = "5f329e2889557b109b8c2cee7058a725", aum = -1)

    /* renamed from: jF */
    public byte f2428jF;
    @C0064Am(aul = "5f329e2889557b109b8c2cee7058a725", aum = 0)

    /* renamed from: ji */
    public C3438ra<ProgressionAbility> f2429ji;
    @C0064Am(aul = "5f329e2889557b109b8c2cee7058a725", aum = -2)

    /* renamed from: jt */
    public long f2430jt;

    public C5227aBf() {
    }

    public C5227aBf(C1616Xf xf) {
        super(xf);
    }

    /* renamed from: c */
    public C5663aRz[] mo309c() {
        return ProgressionCharacterTemplateAbilities._m_fields;
    }

    /* renamed from: d */
    public void mo310d() {
        super.mo310d();
        this.f2429ji = null;
        this.aMQ = null;
        this.f2430jt = 0;
        this.azp = 0;
        this.f2428jF = 0;
        this.azE = 0;
    }
}
