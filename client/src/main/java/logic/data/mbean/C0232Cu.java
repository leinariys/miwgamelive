package logic.data.mbean;

import game.script.progression.CharacterProgression;
import game.script.progression.ProgressionCell;
import game.script.progression.ProgressionCellType;
import game.script.progression.ProgressionLine;
import game.script.resource.Asset;
import logic.baa.C1616Xf;
import logic.res.code.C5663aRz;
import p001a.C0064Am;
import taikodom.infra.script.I18NString;

/* renamed from: a.Cu */
public class C0232Cu extends C3805vD {
    @C0064Am(aul = "6bbbcc3fe5ec5520dedc31e48d951093", aum = 2)

    /* renamed from: QB */
    public ProgressionCellType f362QB;
    @C0064Am(aul = "514b54186169510eff075df7818edf77", aum = 4)
    public ProgressionCell cvk;
    @C0064Am(aul = "6bbbcc3fe5ec5520dedc31e48d951093", aum = -2)
    public long cvl;
    @C0064Am(aul = "514b54186169510eff075df7818edf77", aum = -2)
    public long cvm;
    @C0064Am(aul = "6bbbcc3fe5ec5520dedc31e48d951093", aum = -1)
    public byte cvn;
    @C0064Am(aul = "514b54186169510eff075df7818edf77", aum = -1)
    public byte cvo;
    @C0064Am(aul = "f3bdeee83572fd8c4e3a936b1cbda959", aum = -2)

    /* renamed from: jA */
    public long f363jA;
    @C0064Am(aul = "d569adc0b3ff0782f20359c812ca278a", aum = -1)

    /* renamed from: jK */
    public byte f364jK;
    @C0064Am(aul = "f3bdeee83572fd8c4e3a936b1cbda959", aum = -1)

    /* renamed from: jM */
    public byte f365jM;
    @C0064Am(aul = "d569adc0b3ff0782f20359c812ca278a", aum = 3)

    /* renamed from: jn */
    public Asset f366jn;
    @C0064Am(aul = "f3bdeee83572fd8c4e3a936b1cbda959", aum = 5)

    /* renamed from: jp */
    public CharacterProgression f367jp;
    @C0064Am(aul = "d569adc0b3ff0782f20359c812ca278a", aum = -2)

    /* renamed from: jy */
    public long f368jy;
    @C0064Am(aul = "df857849b750fc884fa505f6c32fb984", aum = 1)

    /* renamed from: nh */
    public I18NString f369nh;
    @C0064Am(aul = "cdb20ce81e16fe532aa210f3c89651c8", aum = 0)

    /* renamed from: ni */
    public I18NString f370ni;
    @C0064Am(aul = "df857849b750fc884fa505f6c32fb984", aum = -2)

    /* renamed from: nk */
    public long f371nk;
    @C0064Am(aul = "cdb20ce81e16fe532aa210f3c89651c8", aum = -2)

    /* renamed from: nl */
    public long f372nl;
    @C0064Am(aul = "df857849b750fc884fa505f6c32fb984", aum = -1)

    /* renamed from: nn */
    public byte f373nn;
    @C0064Am(aul = "cdb20ce81e16fe532aa210f3c89651c8", aum = -1)

    /* renamed from: no */
    public byte f374no;

    public C0232Cu() {
    }

    public C0232Cu(C1616Xf xf) {
        super(xf);
    }

    /* renamed from: c */
    public C5663aRz[] mo309c() {
        return ProgressionLine._m_fields;
    }

    /* renamed from: d */
    public void mo310d() {
        super.mo310d();
        this.f370ni = null;
        this.f369nh = null;
        this.f362QB = null;
        this.f366jn = null;
        this.cvk = null;
        this.f367jp = null;
        this.f372nl = 0;
        this.f371nk = 0;
        this.cvl = 0;
        this.f368jy = 0;
        this.cvm = 0;
        this.f363jA = 0;
        this.f374no = 0;
        this.f373nn = 0;
        this.cvn = 0;
        this.f364jK = 0;
        this.cvo = 0;
        this.f365jM = 0;
    }
}
