package logic.data.mbean;

import game.script.newmarket.SellOrder;
import game.script.newmarket.SellOrderInfo;
import logic.baa.C1616Xf;
import logic.res.code.C5663aRz;
import p001a.C0064Am;
import p001a.C0847MM;

/* renamed from: a.ip */
public class C2707ip extends C0134Bf {
    @C0064Am(aul = "a92bc5ddcd6046e881fccfc64dc2ba9e", aum = 0)

    /* renamed from: XQ */
    public SellOrder f8257XQ;
    @C0064Am(aul = "a92bc5ddcd6046e881fccfc64dc2ba9e", aum = -2)

    /* renamed from: XR */
    public long f8258XR;
    @C0064Am(aul = "a92bc5ddcd6046e881fccfc64dc2ba9e", aum = -1)

    /* renamed from: XS */
    public byte f8259XS;
    @C0064Am(aul = "45a652b70437426ca914093c366ed674", aum = 1)

    /* renamed from: zF */
    public C0847MM f8260zF;
    @C0064Am(aul = "45a652b70437426ca914093c366ed674", aum = -2)

    /* renamed from: zG */
    public long f8261zG;
    @C0064Am(aul = "45a652b70437426ca914093c366ed674", aum = -1)

    /* renamed from: zH */
    public byte f8262zH;

    public C2707ip() {
    }

    public C2707ip(C1616Xf xf) {
        super(xf);
    }

    /* renamed from: c */
    public C5663aRz[] mo309c() {
        return SellOrderInfo._m_fields;
    }

    /* renamed from: d */
    public void mo310d() {
        super.mo310d();
        this.f8257XQ = null;
        this.f8260zF = null;
        this.f8258XR = 0;
        this.f8261zG = 0;
        this.f8259XS = 0;
        this.f8262zH = 0;
    }
}
