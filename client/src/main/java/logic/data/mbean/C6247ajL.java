package logic.data.mbean;

import game.geometry.Vec3d;
import game.script.mission.scripting.LootItemMissionScript;
import logic.baa.C1616Xf;
import logic.res.code.C5663aRz;
import p001a.C0064Am;

/* renamed from: a.ajL  reason: case insensitive filesystem */
public class C6247ajL extends aMY {
    @C0064Am(aul = "a00135834799d0a2978291a1d2eb76d6", aum = 0)
    public int bCN;
    @C0064Am(aul = "60fdbab44a205353a5de2aa8bccbe71d", aum = 3)
    public boolean bCO;
    @C0064Am(aul = "d6991059bf43b03c0a172d4cfc69d876", aum = 5)
    public int bCP;
    @C0064Am(aul = "89821588f70b4139be3dcf9e1b326659", aum = 4)
    public int bCQ;
    @C0064Am(aul = "cf64021d3c037c7d3f129f12b1f5387d", aum = 1)
    public int bCR;
    @C0064Am(aul = "940da18ab8034ddbfb94ab65b33bc117", aum = 2)
    public Vec3d bCT;
    @C0064Am(aul = "a00135834799d0a2978291a1d2eb76d6", aum = -2)
    public long bCU;
    @C0064Am(aul = "60fdbab44a205353a5de2aa8bccbe71d", aum = -2)
    public long bCV;
    @C0064Am(aul = "d6991059bf43b03c0a172d4cfc69d876", aum = -2)
    public long bCW;
    @C0064Am(aul = "89821588f70b4139be3dcf9e1b326659", aum = -2)
    public long bCX;
    @C0064Am(aul = "cf64021d3c037c7d3f129f12b1f5387d", aum = -2)
    public long bCY;
    @C0064Am(aul = "940da18ab8034ddbfb94ab65b33bc117", aum = -2)
    public long bDa;
    @C0064Am(aul = "a00135834799d0a2978291a1d2eb76d6", aum = -1)
    public byte bDb;
    @C0064Am(aul = "60fdbab44a205353a5de2aa8bccbe71d", aum = -1)
    public byte bDc;
    @C0064Am(aul = "d6991059bf43b03c0a172d4cfc69d876", aum = -1)
    public byte bDd;
    @C0064Am(aul = "89821588f70b4139be3dcf9e1b326659", aum = -1)
    public byte bDe;
    @C0064Am(aul = "cf64021d3c037c7d3f129f12b1f5387d", aum = -1)
    public byte bDf;
    @C0064Am(aul = "940da18ab8034ddbfb94ab65b33bc117", aum = -1)
    public byte bDh;

    public C6247ajL() {
    }

    public C6247ajL(C1616Xf xf) {
        super(xf);
    }

    /* renamed from: c */
    public C5663aRz[] mo309c() {
        return LootItemMissionScript._m_fields;
    }

    /* renamed from: d */
    public void mo310d() {
        super.mo310d();
        this.bCN = 0;
        this.bCR = 0;
        this.bCT = null;
        this.bCO = false;
        this.bCQ = 0;
        this.bCP = 0;
        this.bCU = 0;
        this.bCY = 0;
        this.bDa = 0;
        this.bCV = 0;
        this.bCX = 0;
        this.bCW = 0;
        this.bDb = 0;
        this.bDf = 0;
        this.bDh = 0;
        this.bDc = 0;
        this.bDe = 0;
        this.bDd = 0;
    }
}
