package logic.data.mbean;

import game.network.message.serializable.C3438ra;
import game.script.npcchat.NPCSpeech;
import game.script.npcchat.PlayerSpeech;
import game.script.resource.Asset;
import logic.baa.C1616Xf;
import logic.res.code.C5663aRz;
import p001a.C0064Am;

/* renamed from: a.amH  reason: case insensitive filesystem */
public class C6399amH extends C0980OQ {
    @C0064Am(aul = "e83bfd05bb5e140722c65645fab4efc0", aum = 2)
    public Asset bfO;
    @C0064Am(aul = "0fc00f8d0ea959a4e9b217266bf5f5c5", aum = -2)
    public long cAj;
    @C0064Am(aul = "0fc00f8d0ea959a4e9b217266bf5f5c5", aum = -1)
    public byte cAl;
    @C0064Am(aul = "6ddc5077439122799da4122c7c5113d3", aum = 3)
    public boolean dIR;
    @C0064Am(aul = "6ddc5077439122799da4122c7c5113d3", aum = -2)
    public long dIX;
    @C0064Am(aul = "6ddc5077439122799da4122c7c5113d3", aum = -1)
    public byte dJd;
    @C0064Am(aul = "e83bfd05bb5e140722c65645fab4efc0", aum = -2)
    public long eaX;
    @C0064Am(aul = "e83bfd05bb5e140722c65645fab4efc0", aum = -1)
    public byte eba;
    @C0064Am(aul = "987a5e9e379d45c84477dac84fef25c0", aum = 1)
    public C3438ra<PlayerSpeech> fEo;
    @C0064Am(aul = "77d7b3dfb3e576bf39784b26f0e415c2", aum = 4)
    public boolean fEr;
    @C0064Am(aul = "987a5e9e379d45c84477dac84fef25c0", aum = -2)
    public long gbg;
    @C0064Am(aul = "77d7b3dfb3e576bf39784b26f0e415c2", aum = -2)
    public long gbh;
    @C0064Am(aul = "987a5e9e379d45c84477dac84fef25c0", aum = -1)
    public byte gbi;
    @C0064Am(aul = "77d7b3dfb3e576bf39784b26f0e415c2", aum = -1)
    public byte gbj;
    @C0064Am(aul = "0fc00f8d0ea959a4e9b217266bf5f5c5", aum = 0)
    public int priority;

    public C6399amH() {
    }

    public C6399amH(C1616Xf xf) {
        super(xf);
    }

    /* renamed from: c */
    public C5663aRz[] mo309c() {
        return NPCSpeech._m_fields;
    }

    /* renamed from: d */
    public void mo310d() {
        super.mo310d();
        this.priority = 0;
        this.fEo = null;
        this.bfO = null;
        this.dIR = false;
        this.fEr = false;
        this.cAj = 0;
        this.gbg = 0;
        this.eaX = 0;
        this.dIX = 0;
        this.gbh = 0;
        this.cAl = 0;
        this.gbi = 0;
        this.eba = 0;
        this.dJd = 0;
        this.gbj = 0;
    }
}
