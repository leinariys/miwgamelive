package logic.data.mbean;

import game.network.message.serializable.C3438ra;
import game.script.space.Asteroid;
import game.script.spacezone.AsteroidSpawnTable;
import game.script.spacezone.AsteroidZone;
import game.script.spacezone.AsteroidZoneCategory;
import logic.baa.C1616Xf;
import logic.res.code.C5663aRz;
import p001a.C0064Am;

/* renamed from: a.axq  reason: case insensitive filesystem */
public class C6977axq extends C6979axs {
    @C0064Am(aul = "613eb2db7709cf4c0cdab0ed14a4ed85", aum = -2)
    public long aIu;
    @C0064Am(aul = "613eb2db7709cf4c0cdab0ed14a4ed85", aum = -1)
    public byte aIx;
    @C0064Am(aul = "613eb2db7709cf4c0cdab0ed14a4ed85", aum = 11)
    public boolean active;
    @C0064Am(aul = "64975d0f2e4875d382a25be83dfe6588", aum = -2)
    public long bmA;
    @C0064Am(aul = "76ee57ebaf9ed83dce5fc99ef37d182a", aum = -2)
    public long bmB;
    @C0064Am(aul = "02c8cc35c7665dd1600977036900f09f", aum = -1)
    public byte bmC;
    @C0064Am(aul = "4c05862818630611d0cb5d10662673c8", aum = -1)
    public byte bmD;
    @C0064Am(aul = "dedbacca160ac2c44edd90f4a038c0e0", aum = -1)
    public byte bmE;
    @C0064Am(aul = "e6518a72d1b279e1ff2bd9eb777c5693", aum = -1)
    public byte bmF;
    @C0064Am(aul = "86c3fe7c0d31e3184da453b2e98f00c7", aum = -1)
    public byte bmG;
    @C0064Am(aul = "08de3feb5b122085ed39aba514da3997", aum = -1)
    public byte bmH;
    @C0064Am(aul = "c9233f0c5b7a0e181b8694800ddaab97", aum = -1)
    public byte bmI;
    @C0064Am(aul = "64975d0f2e4875d382a25be83dfe6588", aum = -1)
    public byte bmJ;
    @C0064Am(aul = "76ee57ebaf9ed83dce5fc99ef37d182a", aum = -1)
    public byte bmK;
    @C0064Am(aul = "02c8cc35c7665dd1600977036900f09f", aum = 0)
    public AsteroidZone.C2089b bmk;
    @C0064Am(aul = "4c05862818630611d0cb5d10662673c8", aum = 1)
    public AsteroidZone.C2088a bml;
    @C0064Am(aul = "dedbacca160ac2c44edd90f4a038c0e0", aum = 2)
    public float bmm;
    @C0064Am(aul = "e6518a72d1b279e1ff2bd9eb777c5693", aum = 3)
    public float bmn;
    @C0064Am(aul = "86c3fe7c0d31e3184da453b2e98f00c7", aum = 4)
    public float bmo;
    @C0064Am(aul = "08de3feb5b122085ed39aba514da3997", aum = 5)
    public int bmp;
    @C0064Am(aul = "c9233f0c5b7a0e181b8694800ddaab97", aum = 6)
    public int bmq;
    @C0064Am(aul = "64975d0f2e4875d382a25be83dfe6588", aum = 7)
    public float bmr;
    @C0064Am(aul = "76ee57ebaf9ed83dce5fc99ef37d182a", aum = 8)
    public AsteroidSpawnTable bms;
    @C0064Am(aul = "02c8cc35c7665dd1600977036900f09f", aum = -2)
    public long bmt;
    @C0064Am(aul = "4c05862818630611d0cb5d10662673c8", aum = -2)
    public long bmu;
    @C0064Am(aul = "dedbacca160ac2c44edd90f4a038c0e0", aum = -2)
    public long bmv;
    @C0064Am(aul = "e6518a72d1b279e1ff2bd9eb777c5693", aum = -2)
    public long bmw;
    @C0064Am(aul = "86c3fe7c0d31e3184da453b2e98f00c7", aum = -2)
    public long bmx;
    @C0064Am(aul = "08de3feb5b122085ed39aba514da3997", aum = -2)
    public long bmy;
    @C0064Am(aul = "c9233f0c5b7a0e181b8694800ddaab97", aum = -2)
    public long bmz;
    @C0064Am(aul = "5336e2be20e3ce37efd0f6b39b5f563a", aum = 9)
    public AsteroidZoneCategory erQ;
    @C0064Am(aul = "7a2c38a4d12cb05bdaf13760b89ec4c1", aum = 10)
    public boolean erS;
    @C0064Am(aul = "9cee1d408ef08b4a98ea486a97744e49", aum = 12)
    public C3438ra<Asteroid> erU;
    @C0064Am(aul = "5336e2be20e3ce37efd0f6b39b5f563a", aum = -2)
    public long gPM;
    @C0064Am(aul = "7a2c38a4d12cb05bdaf13760b89ec4c1", aum = -2)
    public long gPN;
    @C0064Am(aul = "9cee1d408ef08b4a98ea486a97744e49", aum = -2)
    public long gPO;
    @C0064Am(aul = "5336e2be20e3ce37efd0f6b39b5f563a", aum = -1)
    public byte gPP;
    @C0064Am(aul = "7a2c38a4d12cb05bdaf13760b89ec4c1", aum = -1)
    public byte gPQ;
    @C0064Am(aul = "9cee1d408ef08b4a98ea486a97744e49", aum = -1)
    public byte gPR;

    public C6977axq() {
    }

    public C6977axq(C1616Xf xf) {
        super(xf);
    }

    /* renamed from: c */
    public C5663aRz[] mo309c() {
        return AsteroidZone._m_fields;
    }

    /* renamed from: d */
    public void mo310d() {
        super.mo310d();
        this.bmk = null;
        this.bml = null;
        this.bmm = 0.0f;
        this.bmn = 0.0f;
        this.bmo = 0.0f;
        this.bmp = 0;
        this.bmq = 0;
        this.bmr = 0.0f;
        this.bms = null;
        this.erQ = null;
        this.erS = false;
        this.active = false;
        this.erU = null;
        this.bmt = 0;
        this.bmu = 0;
        this.bmv = 0;
        this.bmw = 0;
        this.bmx = 0;
        this.bmy = 0;
        this.bmz = 0;
        this.bmA = 0;
        this.bmB = 0;
        this.gPM = 0;
        this.gPN = 0;
        this.aIu = 0;
        this.gPO = 0;
        this.bmC = 0;
        this.bmD = 0;
        this.bmE = 0;
        this.bmF = 0;
        this.bmG = 0;
        this.bmH = 0;
        this.bmI = 0;
        this.bmJ = 0;
        this.bmK = 0;
        this.gPP = 0;
        this.gPQ = 0;
        this.aIx = 0;
        this.gPR = 0;
    }
}
