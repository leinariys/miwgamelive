package logic.data.mbean;

import game.network.message.serializable.C3438ra;
import game.script.mission.scripting.I18NStringTable;
import game.script.mission.scripting.I18NStringTableEntry;
import logic.baa.C1616Xf;
import logic.res.code.C5663aRz;
import p001a.C0064Am;

import java.util.UUID;

/* renamed from: a.hq */
public class C2635hq extends C5292aDs {
    @C0064Am(aul = "7d6b3dc563451c9d4948dcfc0777c4e1", aum = 1)

    /* renamed from: Uo */
    public C3438ra<I18NStringTableEntry> f8055Uo;
    @C0064Am(aul = "7d6b3dc563451c9d4948dcfc0777c4e1", aum = -2)

    /* renamed from: Up */
    public long f8056Up;
    @C0064Am(aul = "7d6b3dc563451c9d4948dcfc0777c4e1", aum = -1)

    /* renamed from: Uq */
    public byte f8057Uq;
    @C0064Am(aul = "988852520c4c79116be474a22eeb90e7", aum = 2)

    /* renamed from: bK */
    public UUID f8058bK;
    @C0064Am(aul = "3df275f372343348d1b6d0824aad0124", aum = 0)
    public String handle;
    @C0064Am(aul = "988852520c4c79116be474a22eeb90e7", aum = -2)

    /* renamed from: oL */
    public long f8059oL;
    @C0064Am(aul = "988852520c4c79116be474a22eeb90e7", aum = -1)

    /* renamed from: oS */
    public byte f8060oS;
    @C0064Am(aul = "3df275f372343348d1b6d0824aad0124", aum = -2)

    /* renamed from: ok */
    public long f8061ok;
    @C0064Am(aul = "3df275f372343348d1b6d0824aad0124", aum = -1)

    /* renamed from: on */
    public byte f8062on;

    public C2635hq() {
    }

    public C2635hq(C1616Xf xf) {
        super(xf);
    }

    /* renamed from: c */
    public C5663aRz[] mo309c() {
        return I18NStringTable._m_fields;
    }

    /* renamed from: d */
    public void mo310d() {
        super.mo310d();
        this.handle = null;
        this.f8055Uo = null;
        this.f8058bK = null;
        this.f8061ok = 0;
        this.f8056Up = 0;
        this.f8059oL = 0;
        this.f8062on = 0;
        this.f8057Uq = 0;
        this.f8060oS = 0;
    }
}
