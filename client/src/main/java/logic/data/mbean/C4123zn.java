package logic.data.mbean;

import game.network.message.serializable.C3438ra;
import game.script.mail.MailMessage;
import game.script.player.Player;
import logic.baa.C1616Xf;
import logic.res.code.C5663aRz;
import p001a.C0064Am;

import java.util.Date;

/* renamed from: a.zn */
public class C4123zn extends C3805vD {
    @C0064Am(aul = "b085c43051e439c3d8978a652e3a0587", aum = 3)
    public Player auN;
    @C0064Am(aul = "6131dd2b8bb8997e340dcc7c08156e38", aum = 1)
    public boolean bZi;
    @C0064Am(aul = "5cefd57857f4ca598667972969f0bb5d", aum = 2)
    public C3438ra<Player> bZj;
    @C0064Am(aul = "060f5ab2a7671c95a709646c1801e6b6", aum = 5)
    public String bZk;
    @C0064Am(aul = "bb2088c01b39a05d1e8ef230223f0f0a", aum = -2)
    public long bZl;
    @C0064Am(aul = "6131dd2b8bb8997e340dcc7c08156e38", aum = -2)
    public long bZm;
    @C0064Am(aul = "5cefd57857f4ca598667972969f0bb5d", aum = -2)
    public long bZn;
    @C0064Am(aul = "b085c43051e439c3d8978a652e3a0587", aum = -2)
    public long bZo;
    @C0064Am(aul = "25a9e5bfd883735b74c563cd64a920e8", aum = -2)
    public long bZp;
    @C0064Am(aul = "060f5ab2a7671c95a709646c1801e6b6", aum = -2)
    public long bZq;
    @C0064Am(aul = "bb2088c01b39a05d1e8ef230223f0f0a", aum = -1)
    public byte bZr;
    @C0064Am(aul = "6131dd2b8bb8997e340dcc7c08156e38", aum = -1)
    public byte bZs;
    @C0064Am(aul = "5cefd57857f4ca598667972969f0bb5d", aum = -1)
    public byte bZt;
    @C0064Am(aul = "b085c43051e439c3d8978a652e3a0587", aum = -1)
    public byte bZu;
    @C0064Am(aul = "25a9e5bfd883735b74c563cd64a920e8", aum = -1)
    public byte bZv;
    @C0064Am(aul = "060f5ab2a7671c95a709646c1801e6b6", aum = -1)
    public byte bZw;
    @C0064Am(aul = "bb2088c01b39a05d1e8ef230223f0f0a", aum = 0)
    public Date date;
    @C0064Am(aul = "25a9e5bfd883735b74c563cd64a920e8", aum = 4)
    public String subject;

    public C4123zn() {
    }

    public C4123zn(C1616Xf xf) {
        super(xf);
    }

    /* renamed from: c */
    public C5663aRz[] mo309c() {
        return MailMessage._m_fields;
    }

    /* renamed from: d */
    public void mo310d() {
        super.mo310d();
        this.date = null;
        this.bZi = false;
        this.bZj = null;
        this.auN = null;
        this.subject = null;
        this.bZk = null;
        this.bZl = 0;
        this.bZm = 0;
        this.bZn = 0;
        this.bZo = 0;
        this.bZp = 0;
        this.bZq = 0;
        this.bZr = 0;
        this.bZs = 0;
        this.bZt = 0;
        this.bZu = 0;
        this.bZv = 0;
        this.bZw = 0;
    }
}
