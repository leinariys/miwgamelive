package logic.data.mbean;

import game.network.message.serializable.C3438ra;
import game.script.ai.npc.RoundAIController;
import game.script.ship.Station;
import logic.baa.C1616Xf;
import logic.res.code.C5663aRz;
import p001a.C0064Am;

/* renamed from: a.aur  reason: case insensitive filesystem */
public class C6851aur extends C6937awc {
    @C0064Am(aul = "97ebba05cefbb9d19aa299e1e18c0087", aum = 4)
    public boolean ajo;
    @C0064Am(aul = "3176a4f30258349bdc1d9c41a7435ab5", aum = 0)
    public C3438ra<Station> dgA;
    @C0064Am(aul = "21c21d2eed47e8a1824abb951ec42cd2", aum = 1)
    public float dgC;
    @C0064Am(aul = "ba44933c9b628093b78e3f5fb7b41f56", aum = 2)
    public float dgE;
    @C0064Am(aul = "648b663d161c6c20a954fecaa1a71ef0", aum = 3)
    public Station dgG;
    @C0064Am(aul = "21c21d2eed47e8a1824abb951ec42cd2", aum = -1)
    public byte gDA;
    @C0064Am(aul = "ba44933c9b628093b78e3f5fb7b41f56", aum = -1)
    public byte gDB;
    @C0064Am(aul = "648b663d161c6c20a954fecaa1a71ef0", aum = -1)
    public byte gDC;
    @C0064Am(aul = "97ebba05cefbb9d19aa299e1e18c0087", aum = -1)
    public byte gDD;
    @C0064Am(aul = "3176a4f30258349bdc1d9c41a7435ab5", aum = -2)
    public long gDu;
    @C0064Am(aul = "21c21d2eed47e8a1824abb951ec42cd2", aum = -2)
    public long gDv;
    @C0064Am(aul = "ba44933c9b628093b78e3f5fb7b41f56", aum = -2)
    public long gDw;
    @C0064Am(aul = "648b663d161c6c20a954fecaa1a71ef0", aum = -2)
    public long gDx;
    @C0064Am(aul = "97ebba05cefbb9d19aa299e1e18c0087", aum = -2)
    public long gDy;
    @C0064Am(aul = "3176a4f30258349bdc1d9c41a7435ab5", aum = -1)
    public byte gDz;

    public C6851aur() {
    }

    public C6851aur(C1616Xf xf) {
        super(xf);
    }

    /* renamed from: c */
    public C5663aRz[] mo309c() {
        return RoundAIController._m_fields;
    }

    /* renamed from: d */
    public void mo310d() {
        super.mo310d();
        this.dgA = null;
        this.dgC = 0.0f;
        this.dgE = 0.0f;
        this.dgG = null;
        this.ajo = false;
        this.gDu = 0;
        this.gDv = 0;
        this.gDw = 0;
        this.gDx = 0;
        this.gDy = 0;
        this.gDz = 0;
        this.gDA = 0;
        this.gDB = 0;
        this.gDC = 0;
        this.gDD = 0;
    }
}
