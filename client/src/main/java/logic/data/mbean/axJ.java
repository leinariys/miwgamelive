package logic.data.mbean;

import game.script.avatar.ColorWrapper;
import logic.baa.C1616Xf;
import logic.res.code.C5663aRz;
import p001a.C0064Am;
import taikodom.infra.script.I18NString;

import java.util.UUID;

/* renamed from: a.axJ */
public class axJ extends C5292aDs {
    @C0064Am(aul = "caf04646fc5316f2a29d44cd3ec7b237", aum = -2)

    /* renamed from: Nf */
    public long f5550Nf;
    @C0064Am(aul = "caf04646fc5316f2a29d44cd3ec7b237", aum = -1)

    /* renamed from: Nh */
    public byte f5551Nh;
    @C0064Am(aul = "98f321e8ffced1aa00e7de9d81d23afe", aum = 3)

    /* renamed from: bK */
    public UUID f5552bK;
    @C0064Am(aul = "4536d12629045849ca7811d0645daabe", aum = -2)
    public long bZA;
    @C0064Am(aul = "4536d12629045849ca7811d0645daabe", aum = -1)
    public byte bZB;
    @C0064Am(aul = "1371daa548ebd59d76a1b6476fbe0788", aum = -2)
    public long djC;
    @C0064Am(aul = "1371daa548ebd59d76a1b6476fbe0788", aum = -1)
    public byte djF;
    @C0064Am(aul = "1371daa548ebd59d76a1b6476fbe0788", aum = 1)
    public int glr;
    @C0064Am(aul = "98f321e8ffced1aa00e7de9d81d23afe", aum = -2)

    /* renamed from: oL */
    public long f5553oL;
    @C0064Am(aul = "98f321e8ffced1aa00e7de9d81d23afe", aum = -1)

    /* renamed from: oS */
    public byte f5554oS;
    @C0064Am(aul = "caf04646fc5316f2a29d44cd3ec7b237", aum = 0)

    /* renamed from: zP */
    public I18NString f5555zP;
    @C0064Am(aul = "4536d12629045849ca7811d0645daabe", aum = 2)

    /* renamed from: zR */
    public String f5556zR;

    public axJ() {
    }

    public axJ(C1616Xf xf) {
        super(xf);
    }

    /* renamed from: c */
    public C5663aRz[] mo309c() {
        return ColorWrapper._m_fields;
    }

    /* renamed from: d */
    public void mo310d() {
        super.mo310d();
        this.f5555zP = null;
        this.glr = 0;
        this.f5556zR = null;
        this.f5552bK = null;
        this.f5550Nf = 0;
        this.djC = 0;
        this.bZA = 0;
        this.f5553oL = 0;
        this.f5551Nh = 0;
        this.djF = 0;
        this.bZB = 0;
        this.f5554oS = 0;
    }
}
