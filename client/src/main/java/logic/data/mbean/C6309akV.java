package logic.data.mbean;

import game.script.player.Player;
import game.script.trade.Trade;
import game.script.trade.TradeTrigger;
import logic.baa.C1616Xf;
import logic.res.code.C5663aRz;
import p001a.C0064Am;

/* renamed from: a.akV  reason: case insensitive filesystem */
public class C6309akV extends C6157ahZ {
    @C0064Am(aul = "b54b1883ad06ac89b8ba8af57a5bbaf6", aum = -2)
    public long fHa;
    @C0064Am(aul = "b54b1883ad06ac89b8ba8af57a5bbaf6", aum = -1)
    public byte fHy;
    @C0064Am(aul = "5a1c217cf807e6e34509da0dccd9bb92", aum = -2)
    public long fUP;
    @C0064Am(aul = "2bc10ed9c429990082555c6ed602f673", aum = -2)
    public long fUQ;
    @C0064Am(aul = "5a1c217cf807e6e34509da0dccd9bb92", aum = -1)
    public byte fUR;
    @C0064Am(aul = "2bc10ed9c429990082555c6ed602f673", aum = -1)
    public byte fUS;
    @C0064Am(aul = "5a1c217cf807e6e34509da0dccd9bb92", aum = 1)
    public Player fkA;
    @C0064Am(aul = "2bc10ed9c429990082555c6ed602f673", aum = 2)
    public Player fkC;
    @C0064Am(aul = "b54b1883ad06ac89b8ba8af57a5bbaf6", aum = 0)
    public Trade fky;

    public C6309akV() {
    }

    public C6309akV(C1616Xf xf) {
        super(xf);
    }

    /* renamed from: c */
    public C5663aRz[] mo309c() {
        return TradeTrigger._m_fields;
    }

    /* renamed from: d */
    public void mo310d() {
        super.mo310d();
        this.fky = null;
        this.fkA = null;
        this.fkC = null;
        this.fHa = 0;
        this.fUP = 0;
        this.fUQ = 0;
        this.fHy = 0;
        this.fUR = 0;
        this.fUS = 0;
    }
}
