package logic.data.mbean;

import game.network.message.externalizable.C3131oI;
import game.script.mission.actions.NurseryTutorialMissionAction;
import logic.baa.C1616Xf;
import logic.res.code.C5663aRz;
import p001a.C0064Am;

/* renamed from: a.ail  reason: case insensitive filesystem */
public class C6221ail extends C2503fv {
    @C0064Am(aul = "8ecf25430eb8218e4c4765c2810e8850", aum = 0)

    /* renamed from: dB */
    public C3131oI.C3132a f4663dB;
    @C0064Am(aul = "8ecf25430eb8218e4c4765c2810e8850", aum = -2)

    /* renamed from: dC */
    public long f4664dC;
    @C0064Am(aul = "8ecf25430eb8218e4c4765c2810e8850", aum = -1)

    /* renamed from: dD */
    public byte f4665dD;

    public C6221ail() {
    }

    public C6221ail(C1616Xf xf) {
        super(xf);
    }

    /* renamed from: c */
    public C5663aRz[] mo309c() {
        return NurseryTutorialMissionAction._m_fields;
    }

    /* renamed from: d */
    public void mo310d() {
        super.mo310d();
        this.f4663dB = null;
        this.f4664dC = 0;
        this.f4665dD = 0;
    }
}
