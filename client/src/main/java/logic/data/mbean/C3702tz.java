package logic.data.mbean;

import game.script.crafting.CraftingRecipe;
import game.script.item.ItemType;
import logic.baa.C1616Xf;
import logic.res.code.C5663aRz;
import p001a.C0064Am;

import java.util.UUID;

/* renamed from: a.tz */
public class C3702tz extends C5292aDs {
    @C0064Am(aul = "54635b04f53a4e60307e5e963eaad0ba", aum = -2)
    public long aLG;
    @C0064Am(aul = "54635b04f53a4e60307e5e963eaad0ba", aum = -1)
    public byte aLL;
    @C0064Am(aul = "8b1ea148a6b532696cb9b9bc8fcd697a", aum = 0)

    /* renamed from: bK */
    public UUID f9276bK;
    @C0064Am(aul = "94ed4e4b7bfb1a95849305323cffbb4a", aum = -2)
    public long bmV;
    @C0064Am(aul = "94ed4e4b7bfb1a95849305323cffbb4a", aum = -1)
    public byte bmW;
    @C0064Am(aul = "94ed4e4b7bfb1a95849305323cffbb4a", aum = 2)

    /* renamed from: cw */
    public ItemType f9277cw;
    @C0064Am(aul = "54635b04f53a4e60307e5e963eaad0ba", aum = 3)

    /* renamed from: cy */
    public int f9278cy;
    @C0064Am(aul = "f7cb00eebe7f51a3772f75e08cfa3ecc", aum = 1)
    public String handle;
    @C0064Am(aul = "8b1ea148a6b532696cb9b9bc8fcd697a", aum = -2)

    /* renamed from: oL */
    public long f9279oL;
    @C0064Am(aul = "8b1ea148a6b532696cb9b9bc8fcd697a", aum = -1)

    /* renamed from: oS */
    public byte f9280oS;
    @C0064Am(aul = "f7cb00eebe7f51a3772f75e08cfa3ecc", aum = -2)

    /* renamed from: ok */
    public long f9281ok;
    @C0064Am(aul = "f7cb00eebe7f51a3772f75e08cfa3ecc", aum = -1)

    /* renamed from: on */
    public byte f9282on;

    public C3702tz() {
    }

    public C3702tz(C1616Xf xf) {
        super(xf);
    }

    /* renamed from: c */
    public C5663aRz[] mo309c() {
        return CraftingRecipe._m_fields;
    }

    /* renamed from: d */
    public void mo310d() {
        super.mo310d();
        this.f9276bK = null;
        this.handle = null;
        this.f9277cw = null;
        this.f9278cy = 0;
        this.f9279oL = 0;
        this.f9281ok = 0;
        this.bmV = 0;
        this.aLG = 0;
        this.f9280oS = 0;
        this.f9282on = 0;
        this.bmW = 0;
        this.aLL = 0;
    }
}
