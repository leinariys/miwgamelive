package logic.data.mbean;

import game.script.item.buff.amplifier.HazardShieldAmplifierType;
import logic.baa.C1616Xf;
import logic.res.code.C5663aRz;
import p001a.C0064Am;
import p001a.C3892wO;

/* renamed from: a.aBW */
public class aBW extends C2421fH {
    @C0064Am(aul = "38339fb15c546b68a40258858842f2c5", aum = 0)
    public C3892wO bbH;
    @C0064Am(aul = "38339fb15c546b68a40258858842f2c5", aum = -2)
    public long gEW;
    @C0064Am(aul = "38339fb15c546b68a40258858842f2c5", aum = -1)
    public byte gEY;

    public aBW() {
    }

    public aBW(C1616Xf xf) {
        super(xf);
    }

    /* renamed from: c */
    public C5663aRz[] mo309c() {
        return HazardShieldAmplifierType._m_fields;
    }

    /* renamed from: d */
    public void mo310d() {
        super.mo310d();
        this.bbH = null;
        this.gEW = 0;
        this.gEY = 0;
    }
}
