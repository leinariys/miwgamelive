package logic.data.mbean;

import game.script.item.ScatterWeaponType;
import logic.baa.C1616Xf;
import logic.res.code.C5663aRz;
import p001a.C0064Am;

/* renamed from: a.aVi  reason: case insensitive filesystem */
public class C5750aVi extends C1353Ti {
    @C0064Am(aul = "848e34500435626ca2b707b4576352e8", aum = 0)

    /* renamed from: XT */
    public int f3973XT;
    @C0064Am(aul = "1b43e3cbdb8575585176abafe7e8591f", aum = 1)

    /* renamed from: XU */
    public float f3974XU;
    @C0064Am(aul = "848e34500435626ca2b707b4576352e8", aum = -2)

    /* renamed from: XV */
    public long f3975XV;
    @C0064Am(aul = "1b43e3cbdb8575585176abafe7e8591f", aum = -2)

    /* renamed from: XW */
    public long f3976XW;
    @C0064Am(aul = "848e34500435626ca2b707b4576352e8", aum = -1)

    /* renamed from: XX */
    public byte f3977XX;
    @C0064Am(aul = "1b43e3cbdb8575585176abafe7e8591f", aum = -1)

    /* renamed from: XY */
    public byte f3978XY;

    public C5750aVi() {
    }

    public C5750aVi(C1616Xf xf) {
        super(xf);
    }

    /* renamed from: c */
    public C5663aRz[] mo309c() {
        return ScatterWeaponType._m_fields;
    }

    /* renamed from: d */
    public void mo310d() {
        super.mo310d();
        this.f3973XT = 0;
        this.f3974XU = 0.0f;
        this.f3975XV = 0;
        this.f3976XW = 0;
        this.f3977XX = 0;
        this.f3978XY = 0;
    }
}
