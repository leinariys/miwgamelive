package logic.data.mbean;

import game.network.message.serializable.C1556Wo;
import game.network.message.serializable.C3438ra;
import game.script.aggro.AggroTable;
import logic.baa.C1616Xf;
import logic.res.code.C5663aRz;
import p001a.C0064Am;

/* renamed from: a.IH */
public class C0586IH extends C3805vD {
    @C0064Am(aul = "4d1bb4cc062e486f235ec211e3f362af", aum = -2)
    public long bhG;
    @C0064Am(aul = "4d1bb4cc062e486f235ec211e3f362af", aum = -1)
    public byte bhH;
    @C0064Am(aul = "cb0150bc58838f1007e8b4877bc62799", aum = -2)
    public long dgd;
    @C0064Am(aul = "2f420e4fee487d81777543c07b4259d6", aum = -2)
    public long dge;
    @C0064Am(aul = "e198f567edf5448c7dc4c7b62e3bb8d1", aum = -2)
    public long dgf;
    @C0064Am(aul = "5d4bf8bfb02a52a8149d6336306e0f40", aum = -2)
    public long dgg;
    @C0064Am(aul = "cb0150bc58838f1007e8b4877bc62799", aum = -1)
    public byte dgh;
    @C0064Am(aul = "2f420e4fee487d81777543c07b4259d6", aum = -1)
    public byte dgi;
    @C0064Am(aul = "e198f567edf5448c7dc4c7b62e3bb8d1", aum = -1)
    public byte dgj;
    @C0064Am(aul = "5d4bf8bfb02a52a8149d6336306e0f40", aum = -1)
    public byte dgk;
    @C0064Am(aul = "cb0150bc58838f1007e8b4877bc62799", aum = 0)

    /* renamed from: ze */
    public Pawn f704ze;
    @C0064Am(aul = "4d1bb4cc062e486f235ec211e3f362af", aum = 1)

    /* renamed from: zg */
    public C3438ra<AggroTable.C2308a> f705zg;
    @C0064Am(aul = "2f420e4fee487d81777543c07b4259d6", aum = 2)

    /* renamed from: zi */
    public C1556Wo<Pawn, AggroTable.C2308a> f706zi;
    @C0064Am(aul = "e198f567edf5448c7dc4c7b62e3bb8d1", aum = 3)

    /* renamed from: zk */
    public AggroTable.C2309b f707zk;
    @C0064Am(aul = "5d4bf8bfb02a52a8149d6336306e0f40", aum = 4)

    /* renamed from: zm */
    public AggroTable.C2310c f708zm;

    public C0586IH() {
    }

    public C0586IH(C1616Xf xf) {
        super(xf);
    }

    /* renamed from: c */
    public C5663aRz[] mo309c() {
        return AggroTable._m_fields;
    }

    /* renamed from: d */
    public void mo310d() {
        super.mo310d();
        this.f704ze = null;
        this.f705zg = null;
        this.f706zi = null;
        this.f707zk = null;
        this.f708zm = null;
        this.dgd = 0;
        this.bhG = 0;
        this.dge = 0;
        this.dgf = 0;
        this.dgg = 0;
        this.dgh = 0;
        this.bhH = 0;
        this.dgi = 0;
        this.dgj = 0;
        this.dgk = 0;
    }
}
