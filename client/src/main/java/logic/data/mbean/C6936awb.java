package logic.data.mbean;

import game.script.npcchat.actions.RecurrentMessageRequestSpeechAction;
import game.script.resource.Asset;
import logic.baa.C1616Xf;
import logic.res.code.C5663aRz;
import p001a.C0064Am;
import taikodom.infra.script.I18NString;

/* renamed from: a.awb  reason: case insensitive filesystem */
public class C6936awb extends C6352alM {
    @C0064Am(aul = "788ec08aea79579d633abe0e991c500f", aum = 0)
    public String bbZ;
    @C0064Am(aul = "affb4d09f2fafd3a55c758ab17d80224", aum = 1)
    public I18NString bcb;
    @C0064Am(aul = "4149d681acb7fb1c9152a01dc96b278d", aum = 2)
    public Asset bcd;
    @C0064Am(aul = "788ec08aea79579d633abe0e991c500f", aum = -2)
    public long gKV;
    @C0064Am(aul = "788ec08aea79579d633abe0e991c500f", aum = -1)
    public byte gKW;
    @C0064Am(aul = "affb4d09f2fafd3a55c758ab17d80224", aum = -2)
    public long gLt;
    @C0064Am(aul = "4149d681acb7fb1c9152a01dc96b278d", aum = -2)
    public long gLu;
    @C0064Am(aul = "affb4d09f2fafd3a55c758ab17d80224", aum = -1)
    public byte gLv;
    @C0064Am(aul = "4149d681acb7fb1c9152a01dc96b278d", aum = -1)
    public byte gLw;

    public C6936awb() {
    }

    public C6936awb(C1616Xf xf) {
        super(xf);
    }

    /* renamed from: c */
    public C5663aRz[] mo309c() {
        return RecurrentMessageRequestSpeechAction._m_fields;
    }

    /* renamed from: d */
    public void mo310d() {
        super.mo310d();
        this.bbZ = null;
        this.bcb = null;
        this.bcd = null;
        this.gKV = 0;
        this.gLt = 0;
        this.gLu = 0;
        this.gKW = 0;
        this.gLv = 0;
        this.gLw = 0;
    }
}
