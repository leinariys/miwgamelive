package logic.data.mbean;

import game.script.mission.MissionTemplate;
import game.script.npcchat.requirement.HasNotAccomplishMissionRequirement;
import logic.baa.C1616Xf;
import logic.res.code.C5663aRz;
import p001a.C0064Am;

/* renamed from: a.aMn  reason: case insensitive filesystem */
public class C5521aMn extends C5416aIm {
    @C0064Am(aul = "b811c24b15c78a9b4a759f5730031abf", aum = 0)
    public MissionTemplate baM;
    @C0064Am(aul = "b811c24b15c78a9b4a759f5730031abf", aum = -2)
    public long baN;
    @C0064Am(aul = "b811c24b15c78a9b4a759f5730031abf", aum = -1)
    public byte baO;

    public C5521aMn() {
    }

    public C5521aMn(C1616Xf xf) {
        super(xf);
    }

    /* renamed from: c */
    public C5663aRz[] mo309c() {
        return HasNotAccomplishMissionRequirement._m_fields;
    }

    /* renamed from: d */
    public void mo310d() {
        super.mo310d();
        this.baM = null;
        this.baN = 0;
        this.baO = 0;
    }
}
