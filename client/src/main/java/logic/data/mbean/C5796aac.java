package logic.data.mbean;

import game.script.mission.Mission;
import game.script.mission.NPCEventDispatcher;
import game.script.npc.NPC;
import logic.baa.C1616Xf;
import logic.res.code.C5663aRz;
import p001a.C0064Am;
import p001a.C4045yZ;

/* renamed from: a.aac  reason: case insensitive filesystem */
public class C5796aac extends C0505HA {
    @C0064Am(aul = "34a446d737a26c91a8e58fa4b61eb126", aum = -1)
    public byte auA;
    @C0064Am(aul = "34a446d737a26c91a8e58fa4b61eb126", aum = -2)
    public long auy;
    @C0064Am(aul = "34a446d737a26c91a8e58fa4b61eb126", aum = 0)
    public LDScriptingController cqq;
    @C0064Am(aul = "93bad94381bb7872853964bd300ecd44", aum = 1)
    public Mission<? extends C4045yZ> cqs;
    @C0064Am(aul = "7f50807254e75697e0a4405c2fcb4745", aum = 2)
    public String cqu;
    @C0064Am(aul = "623617a0113d303214083282038cab16", aum = 3)
    public NPC cqw;
    @C0064Am(aul = "93bad94381bb7872853964bd300ecd44", aum = -2)
    public long eSh;
    @C0064Am(aul = "7f50807254e75697e0a4405c2fcb4745", aum = -2)
    public long eSi;
    @C0064Am(aul = "623617a0113d303214083282038cab16", aum = -2)
    public long eSj;
    @C0064Am(aul = "93bad94381bb7872853964bd300ecd44", aum = -1)
    public byte eSk;
    @C0064Am(aul = "7f50807254e75697e0a4405c2fcb4745", aum = -1)
    public byte eSl;
    @C0064Am(aul = "623617a0113d303214083282038cab16", aum = -1)
    public byte eSm;

    public C5796aac() {
    }

    public C5796aac(C1616Xf xf) {
        super(xf);
    }

    /* renamed from: c */
    public C5663aRz[] mo309c() {
        return NPCEventDispatcher._m_fields;
    }

    /* renamed from: d */
    public void mo310d() {
        super.mo310d();
        this.cqq = null;
        this.cqs = null;
        this.cqu = null;
        this.cqw = null;
        this.auy = 0;
        this.eSh = 0;
        this.eSi = 0;
        this.eSj = 0;
        this.auA = 0;
        this.eSk = 0;
        this.eSl = 0;
        this.eSm = 0;
    }
}
