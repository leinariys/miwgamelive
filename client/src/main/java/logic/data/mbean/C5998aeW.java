package logic.data.mbean;

import game.script.item.buff.module.HullBuff;
import game.script.ship.HullAdapter;
import logic.baa.C1616Xf;
import logic.res.code.C5663aRz;
import p001a.C0064Am;
import p001a.C3892wO;

/* renamed from: a.aeW  reason: case insensitive filesystem */
public class C5998aeW extends C4037yS {
    @C0064Am(aul = "a25972163a7706bbc8a86e388979b463", aum = 0)
    public C3892wO aBJ;
    @C0064Am(aul = "c5f1389ae1b40f3db7d29a2c7e039a5d", aum = 1)
    public C3892wO aBK;
    @C0064Am(aul = "a25972163a7706bbc8a86e388979b463", aum = -2)
    public long aBO;
    @C0064Am(aul = "c5f1389ae1b40f3db7d29a2c7e039a5d", aum = -2)
    public long aBP;
    @C0064Am(aul = "a25972163a7706bbc8a86e388979b463", aum = -1)
    public byte aBT;
    @C0064Am(aul = "c5f1389ae1b40f3db7d29a2c7e039a5d", aum = -1)
    public byte aBU;
    @C0064Am(aul = "00c60e5a42d91a37f8b902f8c3cd2970", aum = 2)
    public HullAdapter aPF;
    @C0064Am(aul = "00c60e5a42d91a37f8b902f8c3cd2970", aum = -2)
    public long aPV;
    @C0064Am(aul = "00c60e5a42d91a37f8b902f8c3cd2970", aum = -1)
    public byte aPW;

    public C5998aeW() {
    }

    public C5998aeW(C1616Xf xf) {
        super(xf);
    }

    /* renamed from: c */
    public C5663aRz[] mo309c() {
        return HullBuff._m_fields;
    }

    /* renamed from: d */
    public void mo310d() {
        super.mo310d();
        this.aBJ = null;
        this.aBK = null;
        this.aPF = null;
        this.aBO = 0;
        this.aBP = 0;
        this.aPV = 0;
        this.aBT = 0;
        this.aBU = 0;
        this.aPW = 0;
    }
}
