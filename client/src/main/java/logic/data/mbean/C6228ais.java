package logic.data.mbean;

import game.script.mission.scripting.LootItemMissionScript;
import logic.baa.C1616Xf;
import logic.res.code.C5663aRz;
import p001a.C0064Am;

/* renamed from: a.ais  reason: case insensitive filesystem */
public class C6228ais extends C2955mD {
    @C0064Am(aul = "93c2d489c23bfce9ab3420ad975987c5", aum = 0)
    public LootItemMissionScript awH;
    @C0064Am(aul = "93c2d489c23bfce9ab3420ad975987c5", aum = -2)

    /* renamed from: dl */
    public long f4668dl;
    @C0064Am(aul = "93c2d489c23bfce9ab3420ad975987c5", aum = -1)

    /* renamed from: ds */
    public byte f4669ds;

    public C6228ais() {
    }

    public C6228ais(C1616Xf xf) {
        super(xf);
    }

    /* renamed from: c */
    public C5663aRz[] mo309c() {
        return LootItemMissionScript.State0._m_fields;
    }

    /* renamed from: d */
    public void mo310d() {
        super.mo310d();
        this.awH = null;
        this.f4668dl = 0;
        this.f4669ds = 0;
    }
}
