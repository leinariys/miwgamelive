package logic.data.mbean;

import game.script.citizenship.inprovements.WageImprovement;
import logic.baa.C1616Xf;
import logic.res.code.C5663aRz;
import p001a.C0064Am;

/* renamed from: a.aki  reason: case insensitive filesystem */
public class C6322aki extends C6757atB {
    @C0064Am(aul = "152c69a905bbcbcea81e574e5de863ba", aum = -2)

    /* renamed from: dl */
    public long f4799dl;
    @C0064Am(aul = "152c69a905bbcbcea81e574e5de863ba", aum = -1)

    /* renamed from: ds */
    public byte f4800ds;
    @C0064Am(aul = "152c69a905bbcbcea81e574e5de863ba", aum = 0)
    public WageImprovement fTe;

    public C6322aki() {
    }

    public C6322aki(C1616Xf xf) {
        super(xf);
    }

    /* renamed from: c */
    public C5663aRz[] mo309c() {
        return WageImprovement.C1410a._m_fields;
    }

    /* renamed from: d */
    public void mo310d() {
        super.mo310d();
        this.fTe = null;
        this.f4799dl = 0;
        this.f4800ds = 0;
    }
}
