package logic.data.mbean;

import com.hoplon.geometry.Vec3f;
import game.network.message.serializable.C3438ra;
import game.script.citizenship.inprovements.NodeAccessImprovementType;
import game.script.faction.Faction;
import game.script.resource.Asset;
import game.script.space.Gate;
import game.script.space.SpaceCategory;
import logic.baa.C1616Xf;
import logic.res.code.C5663aRz;
import p001a.C0064Am;
import taikodom.geom.Orientation;

import java.util.UUID;

/* renamed from: a.aym  reason: case insensitive filesystem */
public class C6999aym extends C2217cs {
    @C0064Am(aul = "abfe4f08f3aa28624f5970c33c20ada1", aum = 0)

    /* renamed from: LQ */
    public Asset f5633LQ;
    @C0064Am(aul = "e822b3abcad76ee91ae71f33c0815b12", aum = 2)

    /* renamed from: LS */
    public Asset f5634LS;
    @C0064Am(aul = "4e08340910f06aa733c9988180e1f9a2", aum = 4)

    /* renamed from: LW */
    public Vec3f f5635LW;
    @C0064Am(aul = "748f6806390d3351009a691ad621438a", aum = 5)

    /* renamed from: LY */
    public Orientation f5636LY;
    @C0064Am(aul = "1c0a2ab8be539dafb757352d3c98c659", aum = 6)

    /* renamed from: Ma */
    public Gate f5637Ma;
    @C0064Am(aul = "85a311e04ce9e68659136360140118c5", aum = 8)

    /* renamed from: Mc */
    public boolean f5638Mc;
    @C0064Am(aul = "c6c5bb1f506656fc0d0eba27a9576569", aum = 9)

    /* renamed from: Me */
    public C3438ra<Faction> f5639Me;
    @C0064Am(aul = "0023050a5a04d2142394df69ba1f25bf", aum = 10)

    /* renamed from: Mg */
    public boolean f5640Mg;
    @C0064Am(aul = "e59a3225f39fe6c5d8653d8aa4b54c9e", aum = 11)

    /* renamed from: Mi */
    public long f5641Mi;
    @C0064Am(aul = "275403758d5773cb7c8f4c0aa9908568", aum = 12)

    /* renamed from: Mk */
    public SpaceCategory f5642Mk;
    @C0064Am(aul = "4ee5863af94f35c38e1f5088733b2fb8", aum = 13)

    /* renamed from: Mm */
    public NodeAccessImprovementType f5643Mm;
    @C0064Am(aul = "c89a04f8fe7860bcbc978f68a583bf07", aum = -2)
    public long avd;
    @C0064Am(aul = "c89a04f8fe7860bcbc978f68a583bf07", aum = -1)
    public byte avi;
    @C0064Am(aul = "c6c5bb1f506656fc0d0eba27a9576569", aum = -1)
    public byte ayE;
    @C0064Am(aul = "abfe4f08f3aa28624f5970c33c20ada1", aum = -2)
    public long ayc;
    @C0064Am(aul = "4e08340910f06aa733c9988180e1f9a2", aum = -2)
    public long ayd;
    @C0064Am(aul = "748f6806390d3351009a691ad621438a", aum = -2)
    public long aye;
    @C0064Am(aul = "c6c5bb1f506656fc0d0eba27a9576569", aum = -2)
    public long ayp;
    @C0064Am(aul = "abfe4f08f3aa28624f5970c33c20ada1", aum = -1)
    public byte ayr;
    @C0064Am(aul = "4e08340910f06aa733c9988180e1f9a2", aum = -1)
    public byte ays;
    @C0064Am(aul = "748f6806390d3351009a691ad621438a", aum = -1)
    public byte ayt;
    @C0064Am(aul = "2b8329b40b87a8e571e0b900ccee2bb0", aum = 14)

    /* renamed from: bK */
    public UUID f5644bK;
    @C0064Am(aul = "4ee5863af94f35c38e1f5088733b2fb8", aum = -2)
    public long dzJ;
    @C0064Am(aul = "4ee5863af94f35c38e1f5088733b2fb8", aum = -1)
    public byte dzO;
    @C0064Am(aul = "e822b3abcad76ee91ae71f33c0815b12", aum = -2)
    public long eXJ;
    @C0064Am(aul = "e822b3abcad76ee91ae71f33c0815b12", aum = -1)
    public byte eXK;
    @C0064Am(aul = "add2e690b422885b5c3aa701b6f4236e", aum = 7)
    public boolean enabled;
    @C0064Am(aul = "1c0a2ab8be539dafb757352d3c98c659", aum = -2)
    public long gSV;
    @C0064Am(aul = "85a311e04ce9e68659136360140118c5", aum = -2)
    public long gSW;
    @C0064Am(aul = "0023050a5a04d2142394df69ba1f25bf", aum = -2)
    public long gSX;
    @C0064Am(aul = "e59a3225f39fe6c5d8653d8aa4b54c9e", aum = -2)
    public long gSY;
    @C0064Am(aul = "275403758d5773cb7c8f4c0aa9908568", aum = -2)
    public long gSZ;
    @C0064Am(aul = "1c0a2ab8be539dafb757352d3c98c659", aum = -1)
    public byte gTa;
    @C0064Am(aul = "85a311e04ce9e68659136360140118c5", aum = -1)
    public byte gTb;
    @C0064Am(aul = "0023050a5a04d2142394df69ba1f25bf", aum = -1)
    public byte gTc;
    @C0064Am(aul = "e59a3225f39fe6c5d8653d8aa4b54c9e", aum = -1)
    public byte gTd;
    @C0064Am(aul = "275403758d5773cb7c8f4c0aa9908568", aum = -1)
    public byte gTe;
    @C0064Am(aul = "fce16e781cf744f461f512e45e553a71", aum = 3)
    public String handle;
    @C0064Am(aul = "add2e690b422885b5c3aa701b6f4236e", aum = -2)

    /* renamed from: jC */
    public long f5645jC;
    @C0064Am(aul = "add2e690b422885b5c3aa701b6f4236e", aum = -1)

    /* renamed from: jO */
    public byte f5646jO;
    @C0064Am(aul = "2b8329b40b87a8e571e0b900ccee2bb0", aum = -2)

    /* renamed from: oL */
    public long f5647oL;
    @C0064Am(aul = "2b8329b40b87a8e571e0b900ccee2bb0", aum = -1)

    /* renamed from: oS */
    public byte f5648oS;
    @C0064Am(aul = "fce16e781cf744f461f512e45e553a71", aum = -2)

    /* renamed from: ok */
    public long f5649ok;
    @C0064Am(aul = "fce16e781cf744f461f512e45e553a71", aum = -1)

    /* renamed from: on */
    public byte f5650on;
    @C0064Am(aul = "c89a04f8fe7860bcbc978f68a583bf07", aum = 1)

    /* renamed from: uT */
    public String f5651uT;

    public C6999aym() {
    }

    public C6999aym(C1616Xf xf) {
        super(xf);
    }

    /* renamed from: c */
    public C5663aRz[] mo309c() {
        return Gate._m_fields;
    }

    /* renamed from: d */
    public void mo310d() {
        super.mo310d();
        this.f5633LQ = null;
        this.f5651uT = null;
        this.f5634LS = null;
        this.handle = null;
        this.f5635LW = null;
        this.f5636LY = null;
        this.f5637Ma = null;
        this.enabled = false;
        this.f5638Mc = false;
        this.f5639Me = null;
        this.f5640Mg = false;
        this.f5641Mi = 0;
        this.f5642Mk = null;
        this.f5643Mm = null;
        this.f5644bK = null;
        this.ayc = 0;
        this.avd = 0;
        this.eXJ = 0;
        this.f5649ok = 0;
        this.ayd = 0;
        this.aye = 0;
        this.gSV = 0;
        this.f5645jC = 0;
        this.gSW = 0;
        this.ayp = 0;
        this.gSX = 0;
        this.gSY = 0;
        this.gSZ = 0;
        this.dzJ = 0;
        this.f5647oL = 0;
        this.ayr = 0;
        this.avi = 0;
        this.eXK = 0;
        this.f5650on = 0;
        this.ays = 0;
        this.ayt = 0;
        this.gTa = 0;
        this.f5646jO = 0;
        this.gTb = 0;
        this.ayE = 0;
        this.gTc = 0;
        this.gTd = 0;
        this.gTe = 0;
        this.dzO = 0;
        this.f5648oS = 0;
    }
}
