package logic.data.mbean;

import game.script.ship.categories.ExplorerType;
import game.script.ship.hazardshield.HazardShieldType;
import logic.baa.C1616Xf;
import logic.res.code.C5663aRz;
import p001a.C0064Am;

/* renamed from: a.CT */
public class C0202CT extends aLL {
    @C0064Am(aul = "b8ff25d926afd9276cffd48b18afa868", aum = 0)
    public HazardShieldType aZC;
    @C0064Am(aul = "b8ff25d926afd9276cffd48b18afa868", aum = -2)
    public long cBf;
    @C0064Am(aul = "b8ff25d926afd9276cffd48b18afa868", aum = -1)
    public byte cBg;

    public C0202CT() {
    }

    public C0202CT(C1616Xf xf) {
        super(xf);
    }

    /* renamed from: c */
    public C5663aRz[] mo309c() {
        return ExplorerType._m_fields;
    }

    /* renamed from: d */
    public void mo310d() {
        super.mo310d();
        this.aZC = null;
        this.cBf = 0;
        this.cBg = 0;
    }
}
