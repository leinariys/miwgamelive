package logic.data.mbean;

import game.script.util.*;
import logic.baa.C1616Xf;
import logic.res.code.C5663aRz;
import p001a.C0064Am;

/* renamed from: a.axL */
public class axL extends C3805vD {
    @C0064Am(aul = "5b96ad577a42bdb4fb8403594700678c", aum = 0)
    public TaikodomContentConsistencyChecker cDH;
    @C0064Am(aul = "87d81f87fdd2fd853b2539e18790d1bb", aum = 1)
    public NPCChatConsistencyChecker cDJ;
    @C0064Am(aul = "5b56e3db324a53ed82a8939aae8dfdab", aum = 2)
    public MissionTemplateConsistencyChecker cDL;
    @C0064Am(aul = "bbab64539f7970b7b0ab55fc634bb1d2", aum = 3)
    public MestreDosMagosChatGenerator cDN;
    @C0064Am(aul = "af809639b892f20e298cc480bf5b62d7", aum = 4)
    public ProgressionChecker cDP;
    @C0064Am(aul = "5b96ad577a42bdb4fb8403594700678c", aum = -1)
    public byte gQA;
    @C0064Am(aul = "87d81f87fdd2fd853b2539e18790d1bb", aum = -1)
    public byte gQB;
    @C0064Am(aul = "5b56e3db324a53ed82a8939aae8dfdab", aum = -1)
    public byte gQC;
    @C0064Am(aul = "bbab64539f7970b7b0ab55fc634bb1d2", aum = -1)
    public byte gQD;
    @C0064Am(aul = "af809639b892f20e298cc480bf5b62d7", aum = -1)
    public byte gQE;
    @C0064Am(aul = "5b96ad577a42bdb4fb8403594700678c", aum = -2)
    public long gQv;
    @C0064Am(aul = "87d81f87fdd2fd853b2539e18790d1bb", aum = -2)
    public long gQw;
    @C0064Am(aul = "5b56e3db324a53ed82a8939aae8dfdab", aum = -2)
    public long gQx;
    @C0064Am(aul = "bbab64539f7970b7b0ab55fc634bb1d2", aum = -2)
    public long gQy;
    @C0064Am(aul = "af809639b892f20e298cc480bf5b62d7", aum = -2)
    public long gQz;

    public axL() {
    }

    public axL(C1616Xf xf) {
        super(xf);
    }

    /* renamed from: c */
    public C5663aRz[] mo309c() {
        return ServerEditorUtility._m_fields;
    }

    /* renamed from: d */
    public void mo310d() {
        super.mo310d();
        this.cDH = null;
        this.cDJ = null;
        this.cDL = null;
        this.cDN = null;
        this.cDP = null;
        this.gQv = 0;
        this.gQw = 0;
        this.gQx = 0;
        this.gQy = 0;
        this.gQz = 0;
        this.gQA = 0;
        this.gQB = 0;
        this.gQC = 0;
        this.gQD = 0;
        this.gQE = 0;
    }
}
