package logic.data.mbean;

import game.geometry.Vec3d;
import game.script.mission.scripting.MissionArea;
import game.script.space.Node;
import logic.baa.C1616Xf;
import logic.res.code.C5663aRz;
import p001a.C0064Am;

/* renamed from: a.awV  reason: case insensitive filesystem */
public class C6933awV extends C5292aDs {
    @C0064Am(aul = "058fcb8d6a3993958b5f7a2d1d814ec1", aum = -2)

    /* renamed from: DA */
    public long f5512DA;
    @C0064Am(aul = "b6142c1f6de8fb026cfee5c594f8c1a0", aum = -2)

    /* renamed from: DB */
    public long f5513DB;
    @C0064Am(aul = "058fcb8d6a3993958b5f7a2d1d814ec1", aum = -1)

    /* renamed from: DE */
    public byte f5514DE;
    @C0064Am(aul = "b6142c1f6de8fb026cfee5c594f8c1a0", aum = -1)

    /* renamed from: DF */
    public byte f5515DF;
    @C0064Am(aul = "058fcb8d6a3993958b5f7a2d1d814ec1", aum = 2)

    /* renamed from: Dw */
    public long f5516Dw;
    @C0064Am(aul = "b6142c1f6de8fb026cfee5c594f8c1a0", aum = 4)

    /* renamed from: Dx */
    public long f5517Dx;
    @C0064Am(aul = "51c61043d5febcab02b0a38940cf7f34", aum = -2)
    public long aPL;
    @C0064Am(aul = "51c61043d5febcab02b0a38940cf7f34", aum = -1)
    public byte aPN;
    @C0064Am(aul = "8043227a5237dcf0bb5a060450152cad", aum = 0)
    public Node aSh;
    @C0064Am(aul = "8043227a5237dcf0bb5a060450152cad", aum = -2)
    public long bec;
    @C0064Am(aul = "8043227a5237dcf0bb5a060450152cad", aum = -1)
    public byte bei;
    @C0064Am(aul = "51c61043d5febcab02b0a38940cf7f34", aum = 1)
    public Vec3d center;
    @C0064Am(aul = "c60efd4628b9d8bac293ac1ecb83658a", aum = 3)
    public Vec3d eXl;
    @C0064Am(aul = "c60efd4628b9d8bac293ac1ecb83658a", aum = -2)
    public long gCF;
    @C0064Am(aul = "c60efd4628b9d8bac293ac1ecb83658a", aum = -1)
    public byte gCH;

    public C6933awV() {
    }

    public C6933awV(C1616Xf xf) {
        super(xf);
    }

    /* renamed from: c */
    public C5663aRz[] mo309c() {
        return MissionArea._m_fields;
    }

    /* renamed from: d */
    public void mo310d() {
        super.mo310d();
        this.aSh = null;
        this.center = null;
        this.f5516Dw = 0;
        this.eXl = null;
        this.f5517Dx = 0;
        this.bec = 0;
        this.aPL = 0;
        this.f5512DA = 0;
        this.gCF = 0;
        this.f5513DB = 0;
        this.bei = 0;
        this.aPN = 0;
        this.f5514DE = 0;
        this.gCH = 0;
        this.f5515DF = 0;
    }
}
