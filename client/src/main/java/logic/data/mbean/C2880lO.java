package logic.data.mbean;

import game.script.mission.scripting.KillingNpcMissionScript;
import logic.baa.C1616Xf;
import logic.res.code.C5663aRz;
import p001a.C0064Am;

/* renamed from: a.lO */
public class C2880lO extends aMY {
    @C0064Am(aul = "fec24a1cd8314e940d45099069902f26", aum = 0)
    public int aAt;
    @C0064Am(aul = "fec24a1cd8314e940d45099069902f26", aum = -2)
    public long aAu;
    @C0064Am(aul = "fec24a1cd8314e940d45099069902f26", aum = -1)
    public byte aAv;

    public C2880lO() {
    }

    public C2880lO(C1616Xf xf) {
        super(xf);
    }

    /* renamed from: c */
    public C5663aRz[] mo309c() {
        return KillingNpcMissionScript._m_fields;
    }

    /* renamed from: d */
    public void mo310d() {
        super.mo310d();
        this.aAt = 0;
        this.aAu = 0;
        this.aAv = 0;
    }
}
