package logic.data.mbean;

import game.script.nls.NLSLoot;
import logic.baa.C1616Xf;
import logic.res.code.C5663aRz;
import p001a.C0064Am;
import taikodom.infra.script.I18NString;

/* renamed from: a.LP */
public class C0787LP extends C2484fi {
    @C0064Am(aul = "dfa0757d4375e8fcca4d64c9a8b30b82", aum = 0)
    public I18NString afe;
    @C0064Am(aul = "22771797da641190a0a819e8fb32ccea", aum = -2)

    /* renamed from: dg */
    public long f1038dg;
    @C0064Am(aul = "22771797da641190a0a819e8fb32ccea", aum = -1)

    /* renamed from: dn */
    public byte f1039dn;
    @C0064Am(aul = "e362835fa375c5678970b69cffb8b0cf", aum = 2)
    public I18NString dxN;
    @C0064Am(aul = "91c53c36de5bcff2bb2f382c8935ce6a", aum = 3)
    public I18NString dxO;
    @C0064Am(aul = "41c69912f621d1daa94140d04a3f3142", aum = 4)
    public I18NString dxP;
    @C0064Am(aul = "100585f40b52dd9d67e8d1d889aadc4c", aum = 5)
    public I18NString dxQ;
    @C0064Am(aul = "c4cc657ae75197b79f8e5870aea04ddc", aum = 6)
    public I18NString dxR;
    @C0064Am(aul = "22771797da641190a0a819e8fb32ccea", aum = 7)
    public I18NString dxS;
    @C0064Am(aul = "3582c36e76dee006de13f6ec5439a4ec", aum = 8)
    public I18NString dxT;
    @C0064Am(aul = "2fac1bd2f0f9716f290224b3395badd0", aum = 9)
    public I18NString dxU;
    @C0064Am(aul = "dfa0757d4375e8fcca4d64c9a8b30b82", aum = -2)
    public long dxV;
    @C0064Am(aul = "e362835fa375c5678970b69cffb8b0cf", aum = -2)
    public long dxW;
    @C0064Am(aul = "91c53c36de5bcff2bb2f382c8935ce6a", aum = -2)
    public long dxX;
    @C0064Am(aul = "41c69912f621d1daa94140d04a3f3142", aum = -2)
    public long dxY;
    @C0064Am(aul = "100585f40b52dd9d67e8d1d889aadc4c", aum = -2)
    public long dxZ;
    @C0064Am(aul = "c4cc657ae75197b79f8e5870aea04ddc", aum = -2)
    public long dya;
    @C0064Am(aul = "3582c36e76dee006de13f6ec5439a4ec", aum = -2)
    public long dyb;
    @C0064Am(aul = "2fac1bd2f0f9716f290224b3395badd0", aum = -2)
    public long dyc;
    @C0064Am(aul = "dfa0757d4375e8fcca4d64c9a8b30b82", aum = -1)
    public byte dyd;
    @C0064Am(aul = "e362835fa375c5678970b69cffb8b0cf", aum = -1)
    public byte dye;
    @C0064Am(aul = "91c53c36de5bcff2bb2f382c8935ce6a", aum = -1)
    public byte dyf;
    @C0064Am(aul = "41c69912f621d1daa94140d04a3f3142", aum = -1)
    public byte dyg;
    @C0064Am(aul = "100585f40b52dd9d67e8d1d889aadc4c", aum = -1)
    public byte dyh;
    @C0064Am(aul = "c4cc657ae75197b79f8e5870aea04ddc", aum = -1)
    public byte dyi;
    @C0064Am(aul = "3582c36e76dee006de13f6ec5439a4ec", aum = -1)
    public byte dyj;
    @C0064Am(aul = "2fac1bd2f0f9716f290224b3395badd0", aum = -1)
    public byte dyk;
    @C0064Am(aul = "4300798f8bbc3c67c2ec8e376da4d278", aum = 1)

    /* renamed from: sD */
    public I18NString f1040sD;
    @C0064Am(aul = "4300798f8bbc3c67c2ec8e376da4d278", aum = -1)

    /* renamed from: tJ */
    public byte f1041tJ;
    @C0064Am(aul = "4300798f8bbc3c67c2ec8e376da4d278", aum = -2)

    /* renamed from: tg */
    public long f1042tg;

    public C0787LP() {
    }

    public C0787LP(C1616Xf xf) {
        super(xf);
    }

    /* renamed from: c */
    public C5663aRz[] mo309c() {
        return NLSLoot._m_fields;
    }

    /* renamed from: d */
    public void mo310d() {
        super.mo310d();
        this.afe = null;
        this.f1040sD = null;
        this.dxN = null;
        this.dxO = null;
        this.dxP = null;
        this.dxQ = null;
        this.dxR = null;
        this.dxS = null;
        this.dxT = null;
        this.dxU = null;
        this.dxV = 0;
        this.f1042tg = 0;
        this.dxW = 0;
        this.dxX = 0;
        this.dxY = 0;
        this.dxZ = 0;
        this.dya = 0;
        this.f1038dg = 0;
        this.dyb = 0;
        this.dyc = 0;
        this.dyd = 0;
        this.f1041tJ = 0;
        this.dye = 0;
        this.dyf = 0;
        this.dyg = 0;
        this.dyh = 0;
        this.dyi = 0;
        this.f1039dn = 0;
        this.dyj = 0;
        this.dyk = 0;
    }
}
