package logic.data.mbean;

import game.script.item.RayWeaponType;
import logic.baa.C1616Xf;
import logic.res.code.C5663aRz;
import p001a.C0064Am;

/* renamed from: a.ayl  reason: case insensitive filesystem */
public class C6998ayl extends C2805kM {
    @C0064Am(aul = "361857f4abaa3b8e2ab31956101ee679", aum = -2)
    public long ail;
    @C0064Am(aul = "361857f4abaa3b8e2ab31956101ee679", aum = -1)
    public byte aiv;
    @C0064Am(aul = "361857f4abaa3b8e2ab31956101ee679", aum = 0)
    public float lifeTime;

    public C6998ayl() {
    }

    public C6998ayl(C1616Xf xf) {
        super(xf);
    }

    /* renamed from: c */
    public C5663aRz[] mo309c() {
        return RayWeaponType._m_fields;
    }

    /* renamed from: d */
    public void mo310d() {
        super.mo310d();
        this.lifeTime = 0.0f;
        this.ail = 0;
        this.aiv = 0;
    }
}
