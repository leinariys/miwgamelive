package logic.data.mbean;

import game.script.mission.actions.ShowNPCMessage;
import game.script.npc.NPCType;
import game.script.resource.Asset;
import logic.baa.C1616Xf;
import logic.res.code.C5663aRz;
import p001a.C0064Am;
import taikodom.infra.script.I18NString;

/* renamed from: a.RO */
public class C1174RO extends C2503fv {
    @C0064Am(aul = "bef3fd672c656166a311f935f5f0093d", aum = 1)
    public I18NString auP;
    @C0064Am(aul = "972eecc9b5f5594ff1de0c8b21fb9775", aum = 2)
    public float bfM;
    @C0064Am(aul = "c9fed26ddc367dd177a69fa48f04d21b", aum = 3)
    public Asset bfO;
    @C0064Am(aul = "bef3fd672c656166a311f935f5f0093d", aum = -2)
    public long eaV;
    @C0064Am(aul = "972eecc9b5f5594ff1de0c8b21fb9775", aum = -2)
    public long eaW;
    @C0064Am(aul = "c9fed26ddc367dd177a69fa48f04d21b", aum = -2)
    public long eaX;
    @C0064Am(aul = "bef3fd672c656166a311f935f5f0093d", aum = -1)
    public byte eaY;
    @C0064Am(aul = "972eecc9b5f5594ff1de0c8b21fb9775", aum = -1)
    public byte eaZ;
    @C0064Am(aul = "c9fed26ddc367dd177a69fa48f04d21b", aum = -1)
    public byte eba;
    @C0064Am(aul = "28e662287caafff0c51e108351fbd6d3", aum = 0)

    /* renamed from: nC */
    public NPCType f1487nC;
    @C0064Am(aul = "28e662287caafff0c51e108351fbd6d3", aum = -2)

    /* renamed from: nG */
    public long f1488nG;
    @C0064Am(aul = "28e662287caafff0c51e108351fbd6d3", aum = -1)

    /* renamed from: nK */
    public byte f1489nK;

    public C1174RO() {
    }

    public C1174RO(C1616Xf xf) {
        super(xf);
    }

    /* renamed from: c */
    public C5663aRz[] mo309c() {
        return ShowNPCMessage._m_fields;
    }

    /* renamed from: d */
    public void mo310d() {
        super.mo310d();
        this.f1487nC = null;
        this.auP = null;
        this.bfM = 0.0f;
        this.bfO = null;
        this.f1488nG = 0;
        this.eaV = 0;
        this.eaW = 0;
        this.eaX = 0;
        this.f1489nK = 0;
        this.eaY = 0;
        this.eaZ = 0;
        this.eba = 0;
    }
}
