package logic.data.mbean;

import game.script.missile.MissileWeapon;
import logic.baa.C1616Xf;
import logic.res.code.C5663aRz;
import p001a.C0064Am;

/* renamed from: a.app  reason: case insensitive filesystem */
public class C6589app extends C2841ku {
    @C0064Am(aul = "3c42c78637f4d8d63fefd778bf07a933", aum = 0)
    public long clo;
    @C0064Am(aul = "d7955c853e755e5eeb22037ee5fdd21d", aum = 1)
    public long clq;
    @C0064Am(aul = "229c780dc6ff76725609b7d49bad2751", aum = 2)
    public boolean cls;
    @C0064Am(aul = "3c42c78637f4d8d63fefd778bf07a933", aum = -2)
    public long ftg;
    @C0064Am(aul = "3c42c78637f4d8d63fefd778bf07a933", aum = -1)
    public byte fth;
    @C0064Am(aul = "d7955c853e755e5eeb22037ee5fdd21d", aum = -2)
    public long glI;
    @C0064Am(aul = "229c780dc6ff76725609b7d49bad2751", aum = -2)
    public long glJ;
    @C0064Am(aul = "d7955c853e755e5eeb22037ee5fdd21d", aum = -1)
    public byte glK;
    @C0064Am(aul = "229c780dc6ff76725609b7d49bad2751", aum = -1)
    public byte glL;

    public C6589app() {
    }

    public C6589app(C1616Xf xf) {
        super(xf);
    }

    /* renamed from: c */
    public C5663aRz[] mo309c() {
        return MissileWeapon._m_fields;
    }

    /* renamed from: d */
    public void mo310d() {
        super.mo310d();
        this.clo = 0;
        this.clq = 0;
        this.cls = false;
        this.ftg = 0;
        this.glI = 0;
        this.glJ = 0;
        this.fth = 0;
        this.glK = 0;
        this.glL = 0;
    }
}
