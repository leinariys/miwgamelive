package logic.data.mbean;

import game.CollisionFXSet;
import game.network.message.serializable.C3438ra;
import game.script.itemgen.ItemGenTable;
import game.script.resource.Asset;
import game.script.ship.HullType;
import game.script.space.AsteroidLootType;
import game.script.space.AsteroidType;
import logic.baa.C1616Xf;
import logic.res.code.C5663aRz;
import p001a.C0064Am;
import taikodom.infra.script.I18NString;

/* renamed from: a.aBk  reason: case insensitive filesystem */
public class C5232aBk extends C6665arN {
    @C0064Am(aul = "d7624973e208c02dc7bb51cd5177d30d", aum = 6)

    /* renamed from: LQ */
    public Asset f2432LQ;
    @C0064Am(aul = "c808e77f1ae8def9baaaa5e9775ad919", aum = -2)

    /* renamed from: Nf */
    public long f2433Nf;
    @C0064Am(aul = "c808e77f1ae8def9baaaa5e9775ad919", aum = -1)

    /* renamed from: Nh */
    public byte f2434Nh;
    @C0064Am(aul = "c379172a9f6945e852e2548776a7bec4", aum = 1)

    /* renamed from: Wb */
    public Asset f2435Wb;
    @C0064Am(aul = "d40bc9383dda8e6a43c80e0d2fa443ed", aum = 2)

    /* renamed from: Wd */
    public Asset f2436Wd;
    @C0064Am(aul = "425fb267c8b706d9b97fe565cd4febac", aum = -2)
    public long avd;
    @C0064Am(aul = "425fb267c8b706d9b97fe565cd4febac", aum = -1)
    public byte avi;
    @C0064Am(aul = "441efaf17b25894a2f45a7d5015dc694", aum = 3)
    public C3438ra<ItemGenTable> ayZ;
    @C0064Am(aul = "d7624973e208c02dc7bb51cd5177d30d", aum = -2)
    public long ayc;
    @C0064Am(aul = "d7624973e208c02dc7bb51cd5177d30d", aum = -1)
    public byte ayr;
    @C0064Am(aul = "441efaf17b25894a2f45a7d5015dc694", aum = -1)
    public byte azC;
    @C0064Am(aul = "441efaf17b25894a2f45a7d5015dc694", aum = -2)
    public long azn;
    @C0064Am(aul = "c379172a9f6945e852e2548776a7bec4", aum = -1)
    public byte bhC;
    @C0064Am(aul = "d40bc9383dda8e6a43c80e0d2fa443ed", aum = -1)
    public byte bhD;
    @C0064Am(aul = "914f8c7ac5f00bbde3fda4f2d6eb6a6f", aum = -1)
    public byte bhF;
    @C0064Am(aul = "914f8c7ac5f00bbde3fda4f2d6eb6a6f", aum = 9)
    public HullType bhn;
    @C0064Am(aul = "c379172a9f6945e852e2548776a7bec4", aum = -2)
    public long bht;
    @C0064Am(aul = "d40bc9383dda8e6a43c80e0d2fa443ed", aum = -2)
    public long bhu;
    @C0064Am(aul = "914f8c7ac5f00bbde3fda4f2d6eb6a6f", aum = -2)
    public long bhw;
    @C0064Am(aul = "fda858dfb99f6fc724fa2c37cb8028ac", aum = 0)
    public CollisionFXSet bnE;
    @C0064Am(aul = "a30f7cc66b3106cb5837e081cbb28b43", aum = 7)
    public Asset boc;
    @C0064Am(aul = "fda858dfb99f6fc724fa2c37cb8028ac", aum = -2)
    public long fPc;
    @C0064Am(aul = "a30f7cc66b3106cb5837e081cbb28b43", aum = -2)
    public long fPf;
    @C0064Am(aul = "fda858dfb99f6fc724fa2c37cb8028ac", aum = -1)
    public byte fPj;
    @C0064Am(aul = "a30f7cc66b3106cb5837e081cbb28b43", aum = -1)
    public byte fPm;
    @C0064Am(aul = "c8e5ddc60dbe8f48ae4a8a7f30c644b8", aum = 4)
    public AsteroidLootType hhb;
    @C0064Am(aul = "c8e5ddc60dbe8f48ae4a8a7f30c644b8", aum = -2)
    public long hhc;
    @C0064Am(aul = "c8e5ddc60dbe8f48ae4a8a7f30c644b8", aum = -1)
    public byte hhd;
    @C0064Am(aul = "425fb267c8b706d9b97fe565cd4febac", aum = 8)

    /* renamed from: uT */
    public String f2437uT;
    @C0064Am(aul = "c808e77f1ae8def9baaaa5e9775ad919", aum = 5)

    /* renamed from: zP */
    public I18NString f2438zP;

    public C5232aBk() {
    }

    public C5232aBk(C1616Xf xf) {
        super(xf);
    }

    /* renamed from: c */
    public C5663aRz[] mo309c() {
        return AsteroidType._m_fields;
    }

    /* renamed from: d */
    public void mo310d() {
        super.mo310d();
        this.bnE = null;
        this.f2435Wb = null;
        this.f2436Wd = null;
        this.ayZ = null;
        this.hhb = null;
        this.f2438zP = null;
        this.f2432LQ = null;
        this.boc = null;
        this.f2437uT = null;
        this.bhn = null;
        this.fPc = 0;
        this.bht = 0;
        this.bhu = 0;
        this.azn = 0;
        this.hhc = 0;
        this.f2433Nf = 0;
        this.ayc = 0;
        this.fPf = 0;
        this.avd = 0;
        this.bhw = 0;
        this.fPj = 0;
        this.bhC = 0;
        this.bhD = 0;
        this.azC = 0;
        this.hhd = 0;
        this.f2434Nh = 0;
        this.ayr = 0;
        this.fPm = 0;
        this.avi = 0;
        this.bhF = 0;
    }
}
