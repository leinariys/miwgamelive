package logic.data.mbean;

import game.script.corporation.CorporationPermissionDefaults;
import logic.baa.C1616Xf;
import logic.res.code.C5663aRz;
import p001a.C0064Am;
import p001a.C6704asA;
import taikodom.infra.script.I18NString;

import java.util.UUID;

/* renamed from: a.aDp  reason: case insensitive filesystem */
public class C5289aDp extends C5292aDs {
    @C0064Am(aul = "c4c908c10f07f08348912d1389c63e4f", aum = -2)

    /* renamed from: Nf */
    public long f2582Nf;
    @C0064Am(aul = "c4c908c10f07f08348912d1389c63e4f", aum = -1)

    /* renamed from: Nh */
    public byte f2583Nh;
    @C0064Am(aul = "0fc397812ced210bf6ea0b258849ee77", aum = 3)

    /* renamed from: bK */
    public UUID f2584bK;
    @C0064Am(aul = "e1d26e0bab4fd120dd3e5c0ca5ad751f", aum = 0)
    public C6704asA gIO;
    @C0064Am(aul = "e1d26e0bab4fd120dd3e5c0ca5ad751f", aum = -2)
    public long hBe;
    @C0064Am(aul = "e1d26e0bab4fd120dd3e5c0ca5ad751f", aum = -1)
    public byte hBf;
    @C0064Am(aul = "fc570ea7c674f16a8f8db2b010d5ecf9", aum = 2)

    /* renamed from: nh */
    public I18NString f2585nh;
    @C0064Am(aul = "fc570ea7c674f16a8f8db2b010d5ecf9", aum = -2)

    /* renamed from: nk */
    public long f2586nk;
    @C0064Am(aul = "fc570ea7c674f16a8f8db2b010d5ecf9", aum = -1)

    /* renamed from: nn */
    public byte f2587nn;
    @C0064Am(aul = "0fc397812ced210bf6ea0b258849ee77", aum = -2)

    /* renamed from: oL */
    public long f2588oL;
    @C0064Am(aul = "0fc397812ced210bf6ea0b258849ee77", aum = -1)

    /* renamed from: oS */
    public byte f2589oS;
    @C0064Am(aul = "c4c908c10f07f08348912d1389c63e4f", aum = 1)

    /* renamed from: zP */
    public I18NString f2590zP;

    public C5289aDp() {
    }

    public C5289aDp(C1616Xf xf) {
        super(xf);
    }

    /* renamed from: c */
    public C5663aRz[] mo309c() {
        return CorporationPermissionDefaults._m_fields;
    }

    /* renamed from: d */
    public void mo310d() {
        super.mo310d();
        this.gIO = null;
        this.f2590zP = null;
        this.f2585nh = null;
        this.f2584bK = null;
        this.hBe = 0;
        this.f2582Nf = 0;
        this.f2586nk = 0;
        this.f2588oL = 0;
        this.hBf = 0;
        this.f2583Nh = 0;
        this.f2587nn = 0;
        this.f2589oS = 0;
    }
}
