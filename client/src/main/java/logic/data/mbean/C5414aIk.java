package logic.data.mbean;

import game.network.message.serializable.C3438ra;
import game.script.missiontemplate.WaypointDat;
import game.script.missiontemplate.scripting.AssaultMissionScriptTemplate;
import game.script.missiontemplate.scripting.AssaultMissionWave;
import game.script.npc.NPCType;
import game.script.space.StellarSystem;
import logic.baa.C1616Xf;
import logic.res.code.C5663aRz;
import p001a.C0064Am;

/* renamed from: a.aIk  reason: case insensitive filesystem */
public class C5414aIk extends C0597IR {
    @C0064Am(aul = "d58d8b2d0a613e554caa325a494b4a8a", aum = 1)
    public C3438ra<WaypointDat> aBp;
    @C0064Am(aul = "eea48e93b2ab8d34edbe39c5e713bcec", aum = 2)
    public WaypointDat bHK;
    @C0064Am(aul = "d58d8b2d0a613e554caa325a494b4a8a", aum = -2)
    public long dhN;
    @C0064Am(aul = "d58d8b2d0a613e554caa325a494b4a8a", aum = -1)
    public byte dhS;
    @C0064Am(aul = "c2446e815dc9a4003064e4e1494c546f", aum = -2)
    public long dqj;
    @C0064Am(aul = "eea48e93b2ab8d34edbe39c5e713bcec", aum = -2)
    public long dqk;
    @C0064Am(aul = "c2446e815dc9a4003064e4e1494c546f", aum = -1)
    public byte dqq;
    @C0064Am(aul = "eea48e93b2ab8d34edbe39c5e713bcec", aum = -1)
    public byte dqr;
    @C0064Am(aul = "cf6e96f075fc72ae78b85c761f4c3272", aum = 0)
    public String fZo;
    @C0064Am(aul = "c2446e815dc9a4003064e4e1494c546f", aum = 3)
    public NPCType fZq;
    @C0064Am(aul = "d19c473609123bb12ac941ab8703af5c", aum = 5)
    public C3438ra<AssaultMissionWave> fZr;
    @C0064Am(aul = "d67ddd28e7552c85b17c14fd0ff39a68", aum = 6)
    public boolean fZt;
    @C0064Am(aul = "cf6e96f075fc72ae78b85c761f4c3272", aum = -2)
    public long ibk;
    @C0064Am(aul = "d19c473609123bb12ac941ab8703af5c", aum = -2)
    public long ibl;
    @C0064Am(aul = "d67ddd28e7552c85b17c14fd0ff39a68", aum = -2)
    public long ibm;
    @C0064Am(aul = "cf6e96f075fc72ae78b85c761f4c3272", aum = -1)
    public byte ibn;
    @C0064Am(aul = "d19c473609123bb12ac941ab8703af5c", aum = -1)
    public byte ibo;
    @C0064Am(aul = "d67ddd28e7552c85b17c14fd0ff39a68", aum = -1)
    public byte ibp;
    @C0064Am(aul = "859f50d8dab39ec580da6ed154314c2a", aum = 4)

    /* renamed from: rI */
    public StellarSystem f3094rI;
    @C0064Am(aul = "859f50d8dab39ec580da6ed154314c2a", aum = -2)

    /* renamed from: rY */
    public long f3095rY;
    @C0064Am(aul = "859f50d8dab39ec580da6ed154314c2a", aum = -1)

    /* renamed from: so */
    public byte f3096so;

    public C5414aIk() {
    }

    public C5414aIk(C1616Xf xf) {
        super(xf);
    }

    /* renamed from: c */
    public C5663aRz[] mo309c() {
        return AssaultMissionScriptTemplate._m_fields;
    }

    /* renamed from: d */
    public void mo310d() {
        super.mo310d();
        this.fZo = null;
        this.aBp = null;
        this.bHK = null;
        this.fZq = null;
        this.f3094rI = null;
        this.fZr = null;
        this.fZt = false;
        this.ibk = 0;
        this.dhN = 0;
        this.dqk = 0;
        this.dqj = 0;
        this.f3095rY = 0;
        this.ibl = 0;
        this.ibm = 0;
        this.ibn = 0;
        this.dhS = 0;
        this.dqr = 0;
        this.dqq = 0;
        this.f3096so = 0;
        this.ibo = 0;
        this.ibp = 0;
    }
}
