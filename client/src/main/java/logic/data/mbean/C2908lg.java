package logic.data.mbean;

import game.script.ship.categories.BomberType;
import game.script.ship.strike.StrikeType;
import logic.baa.C1616Xf;
import logic.res.code.C5663aRz;
import p001a.C0064Am;

/* renamed from: a.lg */
public class C2908lg extends C5384aHg {
    @C0064Am(aul = "d400a84483edbad9e108996713e28041", aum = 0)

    /* renamed from: UD */
    public StrikeType f8581UD;
    @C0064Am(aul = "d400a84483edbad9e108996713e28041", aum = -2)
    public long awV;
    @C0064Am(aul = "d400a84483edbad9e108996713e28041", aum = -1)
    public byte awW;

    public C2908lg() {
    }

    public C2908lg(C1616Xf xf) {
        super(xf);
    }

    /* renamed from: c */
    public C5663aRz[] mo309c() {
        return BomberType._m_fields;
    }

    /* renamed from: d */
    public void mo310d() {
        super.mo310d();
        this.f8581UD = null;
        this.awV = 0;
        this.awW = 0;
    }
}
