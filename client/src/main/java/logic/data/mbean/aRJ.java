package logic.data.mbean;

import game.script.util.GameObjectAdapter;
import logic.baa.C1616Xf;
import logic.res.code.C5663aRz;
import p001a.C0064Am;

/* renamed from: a.aRJ */
public class aRJ extends C3805vD {
    @C0064Am(aul = "d382d7e1b66bd46a18445b17f3827c66", aum = 0)
    public GameObjectAdapter bZK;
    @C0064Am(aul = "58352868fe777fb1e180969d77f3d62e", aum = 1)
    public GameObjectAdapter gBU;
    @C0064Am(aul = "745281c2842db8f232e75c8b2418de9b", aum = 2)
    public GameObjectAdapter gBW;
    @C0064Am(aul = "d382d7e1b66bd46a18445b17f3827c66", aum = -2)
    public long hEW;
    @C0064Am(aul = "d382d7e1b66bd46a18445b17f3827c66", aum = -1)
    public byte hEY;
    @C0064Am(aul = "58352868fe777fb1e180969d77f3d62e", aum = -2)
    public long iKY;
    @C0064Am(aul = "745281c2842db8f232e75c8b2418de9b", aum = -2)
    public long iKZ;
    @C0064Am(aul = "58352868fe777fb1e180969d77f3d62e", aum = -1)
    public byte iLa;
    @C0064Am(aul = "745281c2842db8f232e75c8b2418de9b", aum = -1)
    public byte iLb;

    public aRJ() {
    }

    public aRJ(C1616Xf xf) {
        super(xf);
    }

    /* renamed from: c */
    public C5663aRz[] mo309c() {
        return GameObjectAdapter._m_fields;
    }

    /* renamed from: d */
    public void mo310d() {
        super.mo310d();
        this.bZK = null;
        this.gBU = null;
        this.gBW = null;
        this.hEW = 0;
        this.iKY = 0;
        this.iKZ = 0;
        this.hEY = 0;
        this.iLa = 0;
        this.iLb = 0;
    }
}
