package logic.data.mbean;

import game.script.item.buff.module.ShipBuffType;
import logic.baa.C1616Xf;
import logic.res.code.C5663aRz;
import p001a.C0064Am;
import p001a.C3892wO;

/* renamed from: a.ahJ  reason: case insensitive filesystem */
public class C6141ahJ extends C5661aRx {
    @C0064Am(aul = "7ae7cb8af88672bf5c3778d241143b4d", aum = 4)

    /* renamed from: VA */
    public C3892wO f4543VA;
    @C0064Am(aul = "c7ccdb6b6f44d05a290d2bea6a914c41", aum = 5)

    /* renamed from: VC */
    public boolean f4544VC;
    @C0064Am(aul = "d76705b223f66eedd615b2a34fb0a2da", aum = 0)

    /* renamed from: Vs */
    public C3892wO f4545Vs;
    @C0064Am(aul = "be7fac5171944777bb4076ec232935a8", aum = 1)

    /* renamed from: Vu */
    public C3892wO f4546Vu;
    @C0064Am(aul = "812dc31524cb72c3a70ca9a9c9c8d228", aum = 2)

    /* renamed from: Vw */
    public C3892wO f4547Vw;
    @C0064Am(aul = "c1b49c66726aa1ea433ee6ce8c8e1d35", aum = 3)

    /* renamed from: Vy */
    public C3892wO f4548Vy;
    @C0064Am(aul = "d76705b223f66eedd615b2a34fb0a2da", aum = -1)

    /* renamed from: YC */
    public byte f4549YC;
    @C0064Am(aul = "be7fac5171944777bb4076ec232935a8", aum = -1)

    /* renamed from: YD */
    public byte f4550YD;
    @C0064Am(aul = "812dc31524cb72c3a70ca9a9c9c8d228", aum = -1)

    /* renamed from: YE */
    public byte f4551YE;
    @C0064Am(aul = "c1b49c66726aa1ea433ee6ce8c8e1d35", aum = -1)

    /* renamed from: YF */
    public byte f4552YF;
    @C0064Am(aul = "d76705b223f66eedd615b2a34fb0a2da", aum = -2)

    /* renamed from: Yw */
    public long f4553Yw;
    @C0064Am(aul = "be7fac5171944777bb4076ec232935a8", aum = -2)

    /* renamed from: Yx */
    public long f4554Yx;
    @C0064Am(aul = "812dc31524cb72c3a70ca9a9c9c8d228", aum = -2)

    /* renamed from: Yy */
    public long f4555Yy;
    @C0064Am(aul = "c1b49c66726aa1ea433ee6ce8c8e1d35", aum = -2)

    /* renamed from: Yz */
    public long f4556Yz;
    @C0064Am(aul = "7ae7cb8af88672bf5c3778d241143b4d", aum = -2)
    public long bHw;
    @C0064Am(aul = "7ae7cb8af88672bf5c3778d241143b4d", aum = -1)
    public byte bHy;
    @C0064Am(aul = "c7ccdb6b6f44d05a290d2bea6a914c41", aum = -2)
    public long fyC;
    @C0064Am(aul = "c7ccdb6b6f44d05a290d2bea6a914c41", aum = -1)
    public byte fyD;

    public C6141ahJ() {
    }

    public C6141ahJ(C1616Xf xf) {
        super(xf);
    }

    /* renamed from: c */
    public C5663aRz[] mo309c() {
        return ShipBuffType._m_fields;
    }

    /* renamed from: d */
    public void mo310d() {
        super.mo310d();
        this.f4545Vs = null;
        this.f4546Vu = null;
        this.f4547Vw = null;
        this.f4548Vy = null;
        this.f4543VA = null;
        this.f4544VC = false;
        this.f4553Yw = 0;
        this.f4554Yx = 0;
        this.f4555Yy = 0;
        this.f4556Yz = 0;
        this.bHw = 0;
        this.fyC = 0;
        this.f4549YC = 0;
        this.f4550YD = 0;
        this.f4551YE = 0;
        this.f4552YF = 0;
        this.bHy = 0;
        this.fyD = 0;
    }
}
