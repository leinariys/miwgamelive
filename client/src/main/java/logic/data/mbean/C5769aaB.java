package logic.data.mbean;

import game.script.trade.Trade;
import game.script.trade.TradePlayerStatus;
import game.script.trade.TradeTrigger;
import logic.baa.C1616Xf;
import logic.res.code.C5663aRz;
import p001a.C0064Am;

/* renamed from: a.aaB  reason: case insensitive filesystem */
public class C5769aaB extends C3805vD {
    @C0064Am(aul = "0cf8f05ee42e1a174fb9b06335132856", aum = -2)
    public long eVu;
    @C0064Am(aul = "865bef9b4f790f13adc3b65b0bef4e5e", aum = -2)
    public long eVv;
    @C0064Am(aul = "c3ad205b1ca9462f1c83442ece61bd07", aum = -2)
    public long eVw;
    @C0064Am(aul = "0cf8f05ee42e1a174fb9b06335132856", aum = -1)
    public byte eVx;
    @C0064Am(aul = "865bef9b4f790f13adc3b65b0bef4e5e", aum = -1)
    public byte eVy;
    @C0064Am(aul = "c3ad205b1ca9462f1c83442ece61bd07", aum = -1)
    public byte eVz;
    @C0064Am(aul = "0cf8f05ee42e1a174fb9b06335132856", aum = 0)
    public TradePlayerStatus eha;
    @C0064Am(aul = "865bef9b4f790f13adc3b65b0bef4e5e", aum = 1)
    public TradePlayerStatus ehc;
    @C0064Am(aul = "c3ad205b1ca9462f1c83442ece61bd07", aum = 2)
    public TradeTrigger ehe;

    public C5769aaB() {
    }

    public C5769aaB(C1616Xf xf) {
        super(xf);
    }

    /* renamed from: c */
    public C5663aRz[] mo309c() {
        return Trade._m_fields;
    }

    /* renamed from: d */
    public void mo310d() {
        super.mo310d();
        this.eha = null;
        this.ehc = null;
        this.ehe = null;
        this.eVu = 0;
        this.eVv = 0;
        this.eVw = 0;
        this.eVx = 0;
        this.eVy = 0;
        this.eVz = 0;
    }
}
