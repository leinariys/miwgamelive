package logic.data.mbean;

import game.script.consignment.ConsignmentFeeRange;
import logic.baa.C1616Xf;
import logic.res.code.C5663aRz;
import p001a.C0064Am;

import java.util.UUID;

/* renamed from: a.arI  reason: case insensitive filesystem */
public class C6660arI extends C3805vD {
    @C0064Am(aul = "f87e27a750325dd0301b5104a90abe39", aum = 3)

    /* renamed from: bK */
    public UUID f5219bK;
    @C0064Am(aul = "b7435afe0de2dd9c942dfa760d7b8753", aum = 0)
    public long gtg;
    @C0064Am(aul = "2cf1d965eb04dddec53ed18136bd754f", aum = 1)
    public float gth;
    @C0064Am(aul = "464c8b5948192375ae5d7483361bc07b", aum = 2)
    public long gti;
    @C0064Am(aul = "b7435afe0de2dd9c942dfa760d7b8753", aum = -2)
    public long gtj;
    @C0064Am(aul = "2cf1d965eb04dddec53ed18136bd754f", aum = -2)
    public long gtk;
    @C0064Am(aul = "464c8b5948192375ae5d7483361bc07b", aum = -2)
    public long gtl;
    @C0064Am(aul = "b7435afe0de2dd9c942dfa760d7b8753", aum = -1)
    public byte gtm;
    @C0064Am(aul = "2cf1d965eb04dddec53ed18136bd754f", aum = -1)
    public byte gtn;
    @C0064Am(aul = "464c8b5948192375ae5d7483361bc07b", aum = -1)
    public byte gto;
    @C0064Am(aul = "cafa6010317e8f158d2bee8e13956153", aum = 4)
    public String handle;
    @C0064Am(aul = "f87e27a750325dd0301b5104a90abe39", aum = -2)

    /* renamed from: oL */
    public long f5220oL;
    @C0064Am(aul = "f87e27a750325dd0301b5104a90abe39", aum = -1)

    /* renamed from: oS */
    public byte f5221oS;
    @C0064Am(aul = "cafa6010317e8f158d2bee8e13956153", aum = -2)

    /* renamed from: ok */
    public long f5222ok;
    @C0064Am(aul = "cafa6010317e8f158d2bee8e13956153", aum = -1)

    /* renamed from: on */
    public byte f5223on;

    public C6660arI() {
    }

    public C6660arI(C1616Xf xf) {
        super(xf);
    }

    /* renamed from: c */
    public C5663aRz[] mo309c() {
        return ConsignmentFeeRange._m_fields;
    }

    /* renamed from: d */
    public void mo310d() {
        super.mo310d();
        this.gtg = 0;
        this.gth = 0.0f;
        this.gti = 0;
        this.f5219bK = null;
        this.handle = null;
        this.gtj = 0;
        this.gtk = 0;
        this.gtl = 0;
        this.f5220oL = 0;
        this.f5222ok = 0;
        this.gtm = 0;
        this.gtn = 0;
        this.gto = 0;
        this.f5221oS = 0;
        this.f5223on = 0;
    }
}
