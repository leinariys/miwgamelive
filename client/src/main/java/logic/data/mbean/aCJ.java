package logic.data.mbean;

import game.script.mission.MissionTemplate;
import game.script.npcchat.requirement.NotActiveMissionSpeechRequirement;
import logic.baa.C1616Xf;
import logic.res.code.C5663aRz;
import p001a.C0064Am;

/* renamed from: a.aCJ */
public class aCJ extends C5416aIm {
    @C0064Am(aul = "5ef93c3102db7d3b7b76481e8980c20d", aum = 0)
    public MissionTemplate baM;
    @C0064Am(aul = "5ef93c3102db7d3b7b76481e8980c20d", aum = -2)
    public long baN;
    @C0064Am(aul = "5ef93c3102db7d3b7b76481e8980c20d", aum = -1)
    public byte baO;

    public aCJ() {
    }

    public aCJ(C1616Xf xf) {
        super(xf);
    }

    /* renamed from: c */
    public C5663aRz[] mo309c() {
        return NotActiveMissionSpeechRequirement._m_fields;
    }

    /* renamed from: d */
    public void mo310d() {
        super.mo310d();
        this.baM = null;
        this.baN = 0;
        this.baO = 0;
    }
}
