package logic.data.mbean;

import game.CollisionFXSet;
import game.script.item.WeaponType;
import game.script.resource.Asset;
import game.script.ship.HullType;
import game.script.ship.ShieldType;
import logic.baa.C0712KI;
import logic.baa.C1616Xf;
import logic.res.code.C5663aRz;
import p001a.C0064Am;
import taikodom.infra.script.I18NString;

/* renamed from: a.ajj  reason: case insensitive filesystem */
public class C6271ajj extends C6515aoT {
    @C0064Am(aul = "4aa4b6c56fe92cddc2866bb64b469403", aum = 8)

    /* renamed from: LQ */
    public Asset f4711LQ;
    @C0064Am(aul = "b760746232ecfd863bd89f93d7fadaef", aum = 2)

    /* renamed from: Wb */
    public Asset f4712Wb;
    @C0064Am(aul = "5f93644d49cae1b906d8cccbf08cbed9", aum = 3)

    /* renamed from: Wd */
    public Asset f4713Wd;
    @C0064Am(aul = "10bddbe9482fd71cfbfeb7b6da98cbc6", aum = -2)
    public long avd;
    @C0064Am(aul = "10bddbe9482fd71cfbfeb7b6da98cbc6", aum = -1)
    public byte avi;
    @C0064Am(aul = "4aa4b6c56fe92cddc2866bb64b469403", aum = -2)
    public long ayc;
    @C0064Am(aul = "4aa4b6c56fe92cddc2866bb64b469403", aum = -1)
    public byte ayr;
    @C0064Am(aul = "9a0a2e7280df8fada02f8ba8edddfd90", aum = -2)
    public long azg;
    @C0064Am(aul = "9a0a2e7280df8fada02f8ba8edddfd90", aum = -1)
    public byte azv;
    @C0064Am(aul = "3731efd173ccc728ece64ba1bf8441c8", aum = -1)
    public byte bhA;
    @C0064Am(aul = "b760746232ecfd863bd89f93d7fadaef", aum = -1)
    public byte bhC;
    @C0064Am(aul = "5f93644d49cae1b906d8cccbf08cbed9", aum = -1)
    public byte bhD;
    @C0064Am(aul = "d8e9f0453c0a558c4f9d98f1bfeb2d39", aum = -1)
    public byte bhF;
    @C0064Am(aul = "d8e9f0453c0a558c4f9d98f1bfeb2d39", aum = 11)
    public HullType bhn;
    @C0064Am(aul = "3731efd173ccc728ece64ba1bf8441c8", aum = -2)
    public long bhr;
    @C0064Am(aul = "b760746232ecfd863bd89f93d7fadaef", aum = -2)
    public long bht;
    @C0064Am(aul = "5f93644d49cae1b906d8cccbf08cbed9", aum = -2)
    public long bhu;
    @C0064Am(aul = "d8e9f0453c0a558c4f9d98f1bfeb2d39", aum = -2)
    public long bhw;
    @C0064Am(aul = "9780c1051e8f958ab8cfd87121cdc0e2", aum = 1)
    public CollisionFXSet bnE;
    @C0064Am(aul = "4dc1d77d7c4c5a959cee60ebc4f56bed", aum = 7)
    public Asset boc;
    @C0064Am(aul = "3731efd173ccc728ece64ba1bf8441c8", aum = 4)

    /* renamed from: dX */
    public float f4714dX;
    @C0064Am(aul = "9a0a2e7280df8fada02f8ba8edddfd90", aum = 0)
    public C0712KI dlU;
    @C0064Am(aul = "eaa3e6b2e7030fa289da40f0331a60a9", aum = 6)
    public float dlX;
    @C0064Am(aul = "18392ddc462f1d233678720c5d477c80", aum = 10)
    public I18NString dlZ;
    @C0064Am(aul = "be369f2252cd1991ad5bc88700d2e1db", aum = 12)
    public ShieldType fPa;
    @C0064Am(aul = "26922fd083d96e3d497ae7ece27b1c54", aum = 13)
    public WeaponType fPb;
    @C0064Am(aul = "9780c1051e8f958ab8cfd87121cdc0e2", aum = -2)
    public long fPc;
    @C0064Am(aul = "d839bdd4574dd9a72556b40b611ac60b", aum = -2)
    public long fPd;
    @C0064Am(aul = "eaa3e6b2e7030fa289da40f0331a60a9", aum = -2)
    public long fPe;
    @C0064Am(aul = "4dc1d77d7c4c5a959cee60ebc4f56bed", aum = -2)
    public long fPf;
    @C0064Am(aul = "18392ddc462f1d233678720c5d477c80", aum = -2)
    public long fPg;
    @C0064Am(aul = "be369f2252cd1991ad5bc88700d2e1db", aum = -2)
    public long fPh;
    @C0064Am(aul = "26922fd083d96e3d497ae7ece27b1c54", aum = -2)
    public long fPi;
    @C0064Am(aul = "9780c1051e8f958ab8cfd87121cdc0e2", aum = -1)
    public byte fPj;
    @C0064Am(aul = "d839bdd4574dd9a72556b40b611ac60b", aum = -1)
    public byte fPk;
    @C0064Am(aul = "eaa3e6b2e7030fa289da40f0331a60a9", aum = -1)
    public byte fPl;
    @C0064Am(aul = "4dc1d77d7c4c5a959cee60ebc4f56bed", aum = -1)
    public byte fPm;
    @C0064Am(aul = "18392ddc462f1d233678720c5d477c80", aum = -1)
    public byte fPn;
    @C0064Am(aul = "be369f2252cd1991ad5bc88700d2e1db", aum = -1)
    public byte fPo;
    @C0064Am(aul = "26922fd083d96e3d497ae7ece27b1c54", aum = -1)
    public byte fPp;
    @C0064Am(aul = "d839bdd4574dd9a72556b40b611ac60b", aum = 5)
    public float maxPitch;
    @C0064Am(aul = "10bddbe9482fd71cfbfeb7b6da98cbc6", aum = 9)

    /* renamed from: uT */
    public String f4715uT;

    public C6271ajj() {
    }

    public C6271ajj(C1616Xf xf) {
        super(xf);
    }

    /* renamed from: c */
    public C5663aRz[] mo309c() {
        return C6454anK._m_fields;
    }

    /* renamed from: d */
    public void mo310d() {
        super.mo310d();
        this.dlU = null;
        this.bnE = null;
        this.f4712Wb = null;
        this.f4713Wd = null;
        this.f4714dX = 0.0f;
        this.maxPitch = 0.0f;
        this.dlX = 0.0f;
        this.boc = null;
        this.f4711LQ = null;
        this.f4715uT = null;
        this.dlZ = null;
        this.bhn = null;
        this.fPa = null;
        this.fPb = null;
        this.azg = 0;
        this.fPc = 0;
        this.bht = 0;
        this.bhu = 0;
        this.bhr = 0;
        this.fPd = 0;
        this.fPe = 0;
        this.fPf = 0;
        this.ayc = 0;
        this.avd = 0;
        this.fPg = 0;
        this.bhw = 0;
        this.fPh = 0;
        this.fPi = 0;
        this.azv = 0;
        this.fPj = 0;
        this.bhC = 0;
        this.bhD = 0;
        this.bhA = 0;
        this.fPk = 0;
        this.fPl = 0;
        this.fPm = 0;
        this.ayr = 0;
        this.avi = 0;
        this.fPn = 0;
        this.bhF = 0;
        this.fPo = 0;
        this.fPp = 0;
    }
}
