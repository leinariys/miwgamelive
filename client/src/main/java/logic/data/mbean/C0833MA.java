package logic.data.mbean;

import game.network.message.serializable.C3438ra;
import game.script.crafting.CraftingRecipe;
import game.script.item.Blueprint;
import game.script.item.IngredientsCategory;
import logic.baa.C1616Xf;
import logic.res.code.C5663aRz;
import p001a.C0064Am;

/* renamed from: a.MA */
public class C0833MA extends aJB {
    @C0064Am(aul = "3c51cb6c41020f7e647701c866553337", aum = 0)
    public C3438ra<IngredientsCategory> dBN;
    @C0064Am(aul = "a31ebc7cb7b2a5dc244bd98d2536c79e", aum = 1)
    public CraftingRecipe dBO;
    @C0064Am(aul = "c6c5e0843bd55998eb97bd408fc1ede5", aum = 2)
    public long dBP;
    @C0064Am(aul = "3c51cb6c41020f7e647701c866553337", aum = -2)
    public long dBQ;
    @C0064Am(aul = "a31ebc7cb7b2a5dc244bd98d2536c79e", aum = -2)
    public long dBR;
    @C0064Am(aul = "c6c5e0843bd55998eb97bd408fc1ede5", aum = -2)
    public long dBS;
    @C0064Am(aul = "3c51cb6c41020f7e647701c866553337", aum = -1)
    public byte dBT;
    @C0064Am(aul = "a31ebc7cb7b2a5dc244bd98d2536c79e", aum = -1)
    public byte dBU;
    @C0064Am(aul = "c6c5e0843bd55998eb97bd408fc1ede5", aum = -1)
    public byte dBV;

    public C0833MA() {
    }

    public C0833MA(C1616Xf xf) {
        super(xf);
    }

    /* renamed from: c */
    public C5663aRz[] mo309c() {
        return Blueprint._m_fields;
    }

    /* renamed from: d */
    public void mo310d() {
        super.mo310d();
        this.dBN = null;
        this.dBO = null;
        this.dBP = 0;
        this.dBQ = 0;
        this.dBR = 0;
        this.dBS = 0;
        this.dBT = 0;
        this.dBU = 0;
        this.dBV = 0;
    }
}
