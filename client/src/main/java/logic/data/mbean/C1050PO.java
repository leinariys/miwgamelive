package logic.data.mbean;

import game.script.missile.MissileController;
import logic.baa.C1616Xf;
import logic.res.code.C5663aRz;
import p001a.C0064Am;

/* renamed from: a.PO */
public class C1050PO extends C5574aOo {
    @C0064Am(aul = "b98e8b9031abca4f0b8efce3d3b0eef6", aum = 1)

    /* renamed from: Ye */
    public float f1370Ye;
    @C0064Am(aul = "b9b09ba16c4b616112073d683c453b8e", aum = -2)
    public long aps;
    @C0064Am(aul = "b9b09ba16c4b616112073d683c453b8e", aum = -1)
    public byte apx;
    @C0064Am(aul = "b98e8b9031abca4f0b8efce3d3b0eef6", aum = -2)
    public long dSm;
    @C0064Am(aul = "b98e8b9031abca4f0b8efce3d3b0eef6", aum = -1)
    public byte dSn;
    @C0064Am(aul = "b9b09ba16c4b616112073d683c453b8e", aum = 0)
    public long startTime;

    public C1050PO() {
    }

    public C1050PO(C1616Xf xf) {
        super(xf);
    }

    /* renamed from: c */
    public C5663aRz[] mo309c() {
        return MissileController._m_fields;
    }

    /* renamed from: d */
    public void mo310d() {
        super.mo310d();
        this.startTime = 0;
        this.f1370Ye = 0.0f;
        this.aps = 0;
        this.dSm = 0;
        this.apx = 0;
        this.dSn = 0;
    }
}
