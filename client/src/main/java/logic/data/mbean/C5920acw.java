package logic.data.mbean;

import game.network.message.serializable.C3438ra;
import game.script.player.Player;
import game.script.support.TicketChat;
import logic.baa.C1616Xf;
import logic.res.code.C5663aRz;
import p001a.C0064Am;

/* renamed from: a.acw  reason: case insensitive filesystem */
public class C5920acw extends C3805vD {
    @C0064Am(aul = "7851337a8d3ba3e9c9619e8b0f261e64", aum = -1)
    public byte ayA;
    @C0064Am(aul = "7851337a8d3ba3e9c9619e8b0f261e64", aum = -2)
    public long ayl;
    @C0064Am(aul = "7851337a8d3ba3e9c9619e8b0f261e64", aum = 1)
    public C3438ra<Player> cYy;
    @C0064Am(aul = "74750b3fe66ccb9dff2ace8e9a8049e5", aum = 0)
    public C3438ra<String> fbS;
    @C0064Am(aul = "cb250e1335d138d110f51581b4d5028d", aum = 2)
    public boolean fbT;
    @C0064Am(aul = "74750b3fe66ccb9dff2ace8e9a8049e5", aum = -2)
    public long fbU;
    @C0064Am(aul = "cb250e1335d138d110f51581b4d5028d", aum = -2)
    public long fbV;
    @C0064Am(aul = "74750b3fe66ccb9dff2ace8e9a8049e5", aum = -1)
    public byte fbW;
    @C0064Am(aul = "cb250e1335d138d110f51581b4d5028d", aum = -1)
    public byte fbX;

    public C5920acw() {
    }

    public C5920acw(C1616Xf xf) {
        super(xf);
    }

    /* renamed from: c */
    public C5663aRz[] mo309c() {
        return TicketChat._m_fields;
    }

    /* renamed from: d */
    public void mo310d() {
        super.mo310d();
        this.fbS = null;
        this.cYy = null;
        this.fbT = false;
        this.fbU = 0;
        this.ayl = 0;
        this.fbV = 0;
        this.fbW = 0;
        this.ayA = 0;
        this.fbX = 0;
    }
}
