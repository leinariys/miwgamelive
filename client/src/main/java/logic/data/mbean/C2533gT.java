package logic.data.mbean;

import game.view.TipString;
import logic.baa.C1616Xf;
import logic.res.code.C5663aRz;
import p001a.C0064Am;
import taikodom.infra.script.I18NString;

import java.util.UUID;

/* renamed from: a.gT */
public class C2533gT extends C5292aDs {
    @C0064Am(aul = "269861c359811258adf2e365b1baa452", aum = 0)

    /* renamed from: Rs */
    public I18NString f7574Rs;
    @C0064Am(aul = "269861c359811258adf2e365b1baa452", aum = -2)

    /* renamed from: Rt */
    public long f7575Rt;
    @C0064Am(aul = "269861c359811258adf2e365b1baa452", aum = -1)

    /* renamed from: Ru */
    public byte f7576Ru;
    @C0064Am(aul = "80544aaab9942bc36c9a270cf91e2047", aum = 1)

    /* renamed from: bK */
    public UUID f7577bK;
    @C0064Am(aul = "8961c82aa1c6fe53c18ba4fa443f62e8", aum = 2)
    public String handle;
    @C0064Am(aul = "80544aaab9942bc36c9a270cf91e2047", aum = -2)

    /* renamed from: oL */
    public long f7578oL;
    @C0064Am(aul = "80544aaab9942bc36c9a270cf91e2047", aum = -1)

    /* renamed from: oS */
    public byte f7579oS;
    @C0064Am(aul = "8961c82aa1c6fe53c18ba4fa443f62e8", aum = -2)

    /* renamed from: ok */
    public long f7580ok;
    @C0064Am(aul = "8961c82aa1c6fe53c18ba4fa443f62e8", aum = -1)

    /* renamed from: on */
    public byte f7581on;

    public C2533gT() {
    }

    public C2533gT(C1616Xf xf) {
        super(xf);
    }

    /* renamed from: c */
    public C5663aRz[] mo309c() {
        return TipString._m_fields;
    }

    /* renamed from: d */
    public void mo310d() {
        super.mo310d();
        this.f7574Rs = null;
        this.f7577bK = null;
        this.handle = null;
        this.f7575Rt = 0;
        this.f7578oL = 0;
        this.f7580ok = 0;
        this.f7576Ru = 0;
        this.f7579oS = 0;
        this.f7581on = 0;
    }
}
