package logic.data.mbean;

import game.script.item.buff.amplifier.CargoHoldAmplifier;
import logic.baa.C1616Xf;
import logic.res.code.C5663aRz;
import p001a.C0064Am;
import p001a.C3892wO;

/* renamed from: a.ms */
public class C3005ms extends C3108nv {
    @C0064Am(aul = "92fee7c77d6042c8526b539dc66f9187", aum = 1)

    /* renamed from: DL */
    public C3892wO f8664DL;
    @C0064Am(aul = "92fee7c77d6042c8526b539dc66f9187", aum = -2)

    /* renamed from: DM */
    public long f8665DM;
    @C0064Am(aul = "92fee7c77d6042c8526b539dc66f9187", aum = -1)

    /* renamed from: DN */
    public byte f8666DN;
    @C0064Am(aul = "a034632fc008f9254d6f88e97bc1d573", aum = 0)
    public CargoHoldAmplifier.CargoHoldAmplifierAdapter aCu;
    @C0064Am(aul = "a034632fc008f9254d6f88e97bc1d573", aum = -2)
    public long aCv;
    @C0064Am(aul = "a034632fc008f9254d6f88e97bc1d573", aum = -1)
    public byte aCw;

    public C3005ms() {
    }

    public C3005ms(C1616Xf xf) {
        super(xf);
    }

    /* renamed from: c */
    public C5663aRz[] mo309c() {
        return CargoHoldAmplifier._m_fields;
    }

    /* renamed from: d */
    public void mo310d() {
        super.mo310d();
        this.aCu = null;
        this.f8664DL = null;
        this.aCv = 0;
        this.f8665DM = 0;
        this.aCw = 0;
        this.f8666DN = 0;
    }
}
