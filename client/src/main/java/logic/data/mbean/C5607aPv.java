package logic.data.mbean;

import game.script.corporation.Corporation;
import logic.baa.C1616Xf;
import logic.res.code.C5663aRz;
import p001a.C0064Am;

/* renamed from: a.aPv  reason: case insensitive filesystem */
public class C5607aPv extends C5292aDs {
    @C0064Am(aul = "8a2090c940b6e86dd4de6a235b8f2de3", aum = -2)

    /* renamed from: dl */
    public long f3564dl;
    @C0064Am(aul = "8a2090c940b6e86dd4de6a235b8f2de3", aum = -1)

    /* renamed from: ds */
    public byte f3565ds;
    @C0064Am(aul = "8a2090c940b6e86dd4de6a235b8f2de3", aum = 0)

    /* renamed from: lJ */
    public Corporation f3566lJ;

    public C5607aPv() {
    }

    public C5607aPv(C1616Xf xf) {
        super(xf);
    }

    /* renamed from: c */
    public C5663aRz[] mo309c() {
        return Corporation.PlayerDisposedListener._m_fields;
    }

    /* renamed from: d */
    public void mo310d() {
        super.mo310d();
        this.f3566lJ = null;
        this.f3564dl = 0;
        this.f3565ds = 0;
    }
}
