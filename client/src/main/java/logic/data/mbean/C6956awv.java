package logic.data.mbean;

import game.script.item.ProjectileWeapon;
import logic.baa.C1616Xf;
import logic.res.code.C5663aRz;
import p001a.C0064Am;

/* renamed from: a.awv  reason: case insensitive filesystem */
public class C6956awv extends C3976xd {
    @C0064Am(aul = "aee9fef7de962bfa64e9b03429334c0d", aum = -2)

    /* renamed from: dl */
    public long f5533dl;
    @C0064Am(aul = "aee9fef7de962bfa64e9b03429334c0d", aum = -1)

    /* renamed from: ds */
    public byte f5534ds;
    @C0064Am(aul = "aee9fef7de962bfa64e9b03429334c0d", aum = 0)
    public ProjectileWeapon gaZ;

    public C6956awv() {
    }

    public C6956awv(C1616Xf xf) {
        super(xf);
    }

    /* renamed from: c */
    public C5663aRz[] mo309c() {
        return ProjectileWeapon.ProjectileAdapter._m_fields;
    }

    /* renamed from: d */
    public void mo310d() {
        super.mo310d();
        this.gaZ = null;
        this.f5533dl = 0;
        this.f5534ds = 0;
    }
}
