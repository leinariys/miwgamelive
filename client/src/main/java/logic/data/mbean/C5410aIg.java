package logic.data.mbean;

import game.script.item.BaseItemType;
import logic.baa.C1616Xf;
import logic.res.code.C5663aRz;
import p001a.C0064Am;

/* renamed from: a.aIg  reason: case insensitive filesystem */
public class C5410aIg extends C6515aoT {
    @C0064Am(aul = "979d3aa82965b42c976875345efbedc0", aum = 0)
    public boolean bHb;
    @C0064Am(aul = "28b7ae123e26550f834d7a1e86c3a745", aum = 1)
    public int bHd;
    @C0064Am(aul = "ffb84d912adef729ebecdeb035df6c08", aum = 2)
    public long bHf;
    @C0064Am(aul = "979d3aa82965b42c976875345efbedc0", aum = -2)
    public long iaY;
    @C0064Am(aul = "28b7ae123e26550f834d7a1e86c3a745", aum = -2)
    public long iaZ;
    @C0064Am(aul = "ffb84d912adef729ebecdeb035df6c08", aum = -2)
    public long iba;
    @C0064Am(aul = "979d3aa82965b42c976875345efbedc0", aum = -1)
    public byte ibb;
    @C0064Am(aul = "28b7ae123e26550f834d7a1e86c3a745", aum = -1)
    public byte ibc;
    @C0064Am(aul = "ffb84d912adef729ebecdeb035df6c08", aum = -1)
    public byte ibd;

    public C5410aIg() {
    }

    public C5410aIg(C1616Xf xf) {
        super(xf);
    }

    /* renamed from: c */
    public C5663aRz[] mo309c() {
        return BaseItemType._m_fields;
    }

    /* renamed from: d */
    public void mo310d() {
        super.mo310d();
        this.bHb = false;
        this.bHd = 0;
        this.bHf = 0;
        this.iaY = 0;
        this.iaZ = 0;
        this.iba = 0;
        this.ibb = 0;
        this.ibc = 0;
        this.ibd = 0;
    }
}
