package logic.data.mbean;

import game.script.mission.NPCSpawn;
import game.script.npc.NPCType;
import logic.baa.C1616Xf;
import logic.res.code.C5663aRz;
import p001a.C0064Am;

import java.util.UUID;

/* renamed from: a.aOa  reason: case insensitive filesystem */
public class C5560aOa extends C3805vD {
    @C0064Am(aul = "3256f5278a22d3094081fdf18cf94578", aum = 2)

    /* renamed from: bK */
    public UUID f3466bK;
    @C0064Am(aul = "b3c301999bab56336612260b283afc09", aum = 1)
    public int dmS;
    @C0064Am(aul = "1d5388b38f22ed1cfd176a49c030eac5", aum = 3)
    public String handle;
    @C0064Am(aul = "b3c301999bab56336612260b283afc09", aum = -2)
    public long ixv;
    @C0064Am(aul = "b3c301999bab56336612260b283afc09", aum = -1)
    public byte ixw;
    @C0064Am(aul = "0da2fd31342368d85c3a558e9a40c035", aum = 0)

    /* renamed from: nC */
    public NPCType f3467nC;
    @C0064Am(aul = "0da2fd31342368d85c3a558e9a40c035", aum = -2)

    /* renamed from: nG */
    public long f3468nG;
    @C0064Am(aul = "0da2fd31342368d85c3a558e9a40c035", aum = -1)

    /* renamed from: nK */
    public byte f3469nK;
    @C0064Am(aul = "3256f5278a22d3094081fdf18cf94578", aum = -2)

    /* renamed from: oL */
    public long f3470oL;
    @C0064Am(aul = "3256f5278a22d3094081fdf18cf94578", aum = -1)

    /* renamed from: oS */
    public byte f3471oS;
    @C0064Am(aul = "1d5388b38f22ed1cfd176a49c030eac5", aum = -2)

    /* renamed from: ok */
    public long f3472ok;
    @C0064Am(aul = "1d5388b38f22ed1cfd176a49c030eac5", aum = -1)

    /* renamed from: on */
    public byte f3473on;

    public C5560aOa() {
    }

    public C5560aOa(C1616Xf xf) {
        super(xf);
    }

    /* renamed from: c */
    public C5663aRz[] mo309c() {
        return NPCSpawn._m_fields;
    }

    /* renamed from: d */
    public void mo310d() {
        super.mo310d();
        this.f3467nC = null;
        this.dmS = 0;
        this.f3466bK = null;
        this.handle = null;
        this.f3468nG = 0;
        this.ixv = 0;
        this.f3470oL = 0;
        this.f3472ok = 0;
        this.f3469nK = 0;
        this.ixw = 0;
        this.f3471oS = 0;
        this.f3473on = 0;
    }
}
