package logic.data.mbean;

import game.script.item.buff.module.HullBuffType;
import logic.baa.C1616Xf;
import logic.res.code.C5663aRz;
import p001a.C0064Am;
import p001a.C3892wO;

/* renamed from: a.Ke */
public class C0745Ke extends C5661aRx {
    @C0064Am(aul = "dddf0a3c3211437f807eecb828aa1677", aum = 0)
    public C3892wO aBJ;
    @C0064Am(aul = "0f985626bc0fea669b96f7c3084b1f1b", aum = 1)
    public C3892wO aBK;
    @C0064Am(aul = "dddf0a3c3211437f807eecb828aa1677", aum = -2)
    public long aBO;
    @C0064Am(aul = "0f985626bc0fea669b96f7c3084b1f1b", aum = -2)
    public long aBP;
    @C0064Am(aul = "dddf0a3c3211437f807eecb828aa1677", aum = -1)
    public byte aBT;
    @C0064Am(aul = "0f985626bc0fea669b96f7c3084b1f1b", aum = -1)
    public byte aBU;

    public C0745Ke() {
    }

    public C0745Ke(C1616Xf xf) {
        super(xf);
    }

    /* renamed from: c */
    public C5663aRz[] mo309c() {
        return HullBuffType._m_fields;
    }

    /* renamed from: d */
    public void mo310d() {
        super.mo310d();
        this.aBJ = null;
        this.aBK = null;
        this.aBO = 0;
        this.aBP = 0;
        this.aBT = 0;
        this.aBU = 0;
    }
}
