package logic.data.mbean;

import game.network.message.serializable.C2686iZ;
import game.script.corporation.CorporationCreationStatus;
import game.script.corporation.CorporationInvitationStatus;
import game.script.player.Player;
import logic.baa.C1616Xf;
import logic.res.code.C5663aRz;
import p001a.C0064Am;

/* renamed from: a.IP */
public class C0595IP extends C3805vD {
    @C0064Am(aul = "7c9e9b52562973eaebce573a32442eb3", aum = -2)
    public long dgZ;
    @C0064Am(aul = "c2ea795dc5e2081602d0e91dba9642cd", aum = -2)
    public long dha;
    @C0064Am(aul = "ad9909dcf0ee618890da4714ecdceb16", aum = -2)
    public long dhb;
    @C0064Am(aul = "630ba63c616e541b0f61fa3806b3027b", aum = -2)
    public long dhc;
    @C0064Am(aul = "7c9e9b52562973eaebce573a32442eb3", aum = -1)
    public byte dhd;
    @C0064Am(aul = "c2ea795dc5e2081602d0e91dba9642cd", aum = -1)
    public byte dhe;
    @C0064Am(aul = "ad9909dcf0ee618890da4714ecdceb16", aum = -1)
    public byte dhf;
    @C0064Am(aul = "630ba63c616e541b0f61fa3806b3027b", aum = -1)
    public byte dhg;
    @C0064Am(aul = "630ba63c616e541b0f61fa3806b3027b", aum = 3)

    /* renamed from: mA */
    public boolean f709mA;
    @C0064Am(aul = "7c9e9b52562973eaebce573a32442eb3", aum = 0)

    /* renamed from: mu */
    public Player f710mu;
    @C0064Am(aul = "c2ea795dc5e2081602d0e91dba9642cd", aum = 1)

    /* renamed from: mw */
    public C2686iZ<CorporationInvitationStatus> f711mw;
    @C0064Am(aul = "ad9909dcf0ee618890da4714ecdceb16", aum = 2)

    /* renamed from: my */
    public boolean f712my;

    public C0595IP() {
    }

    public C0595IP(C1616Xf xf) {
        super(xf);
    }

    /* renamed from: c */
    public C5663aRz[] mo309c() {
        return CorporationCreationStatus._m_fields;
    }

    /* renamed from: d */
    public void mo310d() {
        super.mo310d();
        this.f710mu = null;
        this.f711mw = null;
        this.f712my = false;
        this.f709mA = false;
        this.dgZ = 0;
        this.dha = 0;
        this.dhb = 0;
        this.dhc = 0;
        this.dhd = 0;
        this.dhe = 0;
        this.dhf = 0;
        this.dhg = 0;
    }
}
