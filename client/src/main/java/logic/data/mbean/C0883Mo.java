package logic.data.mbean;

import game.geometry.Vec3d;
import game.network.message.serializable.C1556Wo;
import game.network.message.serializable.C3438ra;
import game.script.citizenship.inprovements.NodeAccessImprovementType;
import game.script.pda.DatabaseCategory;
import game.script.pda.TaikopediaEntry;
import game.script.resource.Asset;
import game.script.simulation.Space;
import game.script.space.Node;
import game.script.space.StellarSystem;
import logic.baa.C1616Xf;
import logic.res.code.C5663aRz;
import p001a.C0064Am;
import taikodom.infra.script.I18NString;

import java.util.UUID;

/* renamed from: a.Mo */
public class C0883Mo extends C3805vD {
    @C0064Am(aul = "d80ce90825228b373b9a88ffe328038a", aum = -2)

    /* renamed from: DA */
    public long f1112DA;
    @C0064Am(aul = "d80ce90825228b373b9a88ffe328038a", aum = -1)

    /* renamed from: DE */
    public byte f1113DE;
    @C0064Am(aul = "71eb97b9014a8df581887168abf21476", aum = 3)

    /* renamed from: LQ */
    public Asset f1114LQ;
    @C0064Am(aul = "566519299ab0912e69d41e5b0f7f1d5e", aum = 11)

    /* renamed from: Mm */
    public NodeAccessImprovementType f1115Mm;
    @C0064Am(aul = "5c22a3a88c762c070fd38bab16d42265", aum = -2)

    /* renamed from: Nf */
    public long f1116Nf;
    @C0064Am(aul = "5c22a3a88c762c070fd38bab16d42265", aum = -1)

    /* renamed from: Nh */
    public byte f1117Nh;
    @C0064Am(aul = "71eb97b9014a8df581887168abf21476", aum = -2)
    public long ayc;
    @C0064Am(aul = "71eb97b9014a8df581887168abf21476", aum = -1)
    public byte ayr;
    @C0064Am(aul = "ce7817e3357c3d341d2ec5f5bb7ddd74", aum = 18)

    /* renamed from: bK */
    public UUID f1118bK;
    @C0064Am(aul = "a672013f6598ad8423bf0f91ec091f79", aum = 2)
    public float bcq;
    @C0064Am(aul = "d604552d5a410766eed30ee6e1d14a9a", aum = 6)
    public boolean bcs;
    @C0064Am(aul = "b406685a072cf114e051149f4bf865bc", aum = 12)
    public C1556Wo<Node.C3423a, Node.NodeObjectHolder> bcw;
    @C0064Am(aul = "cebedec59cb54b7d50b3f16d003f49d5", aum = 17)
    public boolean bcy;
    @C0064Am(aul = "a672013f6598ad8423bf0f91ec091f79", aum = -2)
    public long dzH;
    @C0064Am(aul = "d604552d5a410766eed30ee6e1d14a9a", aum = -2)
    public long dzI;
    @C0064Am(aul = "566519299ab0912e69d41e5b0f7f1d5e", aum = -2)
    public long dzJ;
    @C0064Am(aul = "b406685a072cf114e051149f4bf865bc", aum = -2)
    public long dzK;
    @C0064Am(aul = "cebedec59cb54b7d50b3f16d003f49d5", aum = -2)
    public long dzL;
    @C0064Am(aul = "a672013f6598ad8423bf0f91ec091f79", aum = -1)
    public byte dzM;
    @C0064Am(aul = "d604552d5a410766eed30ee6e1d14a9a", aum = -1)
    public byte dzN;
    @C0064Am(aul = "566519299ab0912e69d41e5b0f7f1d5e", aum = -1)
    public byte dzO;
    @C0064Am(aul = "b406685a072cf114e051149f4bf865bc", aum = -1)
    public byte dzP;
    @C0064Am(aul = "cebedec59cb54b7d50b3f16d003f49d5", aum = -1)
    public byte dzQ;
    @C0064Am(aul = "d5ce9d4447f6174ee09cbca95104b906", aum = 0)
    public String handle;
    @C0064Am(aul = "dac22c8ad486d6ea76a334cfaa0997d2", aum = -1)

    /* renamed from: jK */
    public byte f1119jK;
    @C0064Am(aul = "bcf450d1d903002ba9c691e6bd5c3b22", aum = 14)

    /* renamed from: jS */
    public DatabaseCategory f1120jS;
    @C0064Am(aul = "a2e790ab1af38ca4928c33cfc52b644b", aum = 13)

    /* renamed from: jT */
    public C3438ra<TaikopediaEntry> f1121jT;
    @C0064Am(aul = "63e2c56e516900a3385ac684402f8745", aum = 5)

    /* renamed from: jU */
    public Asset f1122jU;
    @C0064Am(aul = "076a3aab6f8a4f1da2c293683e8089f7", aum = 16)

    /* renamed from: jV */
    public float f1123jV;
    @C0064Am(aul = "bcf450d1d903002ba9c691e6bd5c3b22", aum = -2)

    /* renamed from: jX */
    public long f1124jX;
    @C0064Am(aul = "a2e790ab1af38ca4928c33cfc52b644b", aum = -2)

    /* renamed from: jY */
    public long f1125jY;
    @C0064Am(aul = "63e2c56e516900a3385ac684402f8745", aum = -2)

    /* renamed from: jZ */
    public long f1126jZ;
    @C0064Am(aul = "dac22c8ad486d6ea76a334cfaa0997d2", aum = 4)

    /* renamed from: jn */
    public Asset f1127jn;
    @C0064Am(aul = "dac22c8ad486d6ea76a334cfaa0997d2", aum = -2)

    /* renamed from: jy */
    public long f1128jy;
    @C0064Am(aul = "076a3aab6f8a4f1da2c293683e8089f7", aum = -2)

    /* renamed from: ka */
    public long f1129ka;
    @C0064Am(aul = "bcf450d1d903002ba9c691e6bd5c3b22", aum = -1)

    /* renamed from: kc */
    public byte f1130kc;
    @C0064Am(aul = "a2e790ab1af38ca4928c33cfc52b644b", aum = -1)

    /* renamed from: kd */
    public byte f1131kd;
    @C0064Am(aul = "63e2c56e516900a3385ac684402f8745", aum = -1)

    /* renamed from: ke */
    public byte f1132ke;
    @C0064Am(aul = "076a3aab6f8a4f1da2c293683e8089f7", aum = -1)

    /* renamed from: kf */
    public byte f1133kf;
    @C0064Am(aul = "a8fe27e9559caab9b64bed9338473bc0", aum = 15)

    /* renamed from: nh */
    public I18NString f1134nh;
    @C0064Am(aul = "a8fe27e9559caab9b64bed9338473bc0", aum = -2)

    /* renamed from: nk */
    public long f1135nk;
    @C0064Am(aul = "a8fe27e9559caab9b64bed9338473bc0", aum = -1)

    /* renamed from: nn */
    public byte f1136nn;
    @C0064Am(aul = "ce7817e3357c3d341d2ec5f5bb7ddd74", aum = -2)

    /* renamed from: oL */
    public long f1137oL;
    @C0064Am(aul = "ce7817e3357c3d341d2ec5f5bb7ddd74", aum = -1)

    /* renamed from: oS */
    public byte f1138oS;
    @C0064Am(aul = "d5ce9d4447f6174ee09cbca95104b906", aum = -2)

    /* renamed from: ok */
    public long f1139ok;
    @C0064Am(aul = "d5ce9d4447f6174ee09cbca95104b906", aum = -1)

    /* renamed from: on */
    public byte f1140on;
    @C0064Am(aul = "2283e2439084f9cb2fe60c15ec4ca468", aum = 7)
    public Vec3d position;
    @C0064Am(aul = "93319c2c0276eca4b73a11769d895aa9", aum = 9)

    /* renamed from: rI */
    public StellarSystem f1141rI;
    @C0064Am(aul = "36804dfe16fa1a1187577cec45839991", aum = 10)

    /* renamed from: rL */
    public Space f1142rL;
    @C0064Am(aul = "2283e2439084f9cb2fe60c15ec4ca468", aum = -2)

    /* renamed from: rX */
    public long f1143rX;
    @C0064Am(aul = "93319c2c0276eca4b73a11769d895aa9", aum = -2)

    /* renamed from: rY */
    public long f1144rY;
    @C0064Am(aul = "d80ce90825228b373b9a88ffe328038a", aum = 8)
    public float radius;
    @C0064Am(aul = "36804dfe16fa1a1187577cec45839991", aum = -2)

    /* renamed from: sb */
    public long f1145sb;
    @C0064Am(aul = "2283e2439084f9cb2fe60c15ec4ca468", aum = -1)

    /* renamed from: sn */
    public byte f1146sn;
    @C0064Am(aul = "93319c2c0276eca4b73a11769d895aa9", aum = -1)

    /* renamed from: so */
    public byte f1147so;
    @C0064Am(aul = "36804dfe16fa1a1187577cec45839991", aum = -1)

    /* renamed from: sr */
    public byte f1148sr;
    @C0064Am(aul = "5c22a3a88c762c070fd38bab16d42265", aum = 1)

    /* renamed from: zP */
    public I18NString f1149zP;

    public C0883Mo() {
    }

    public C0883Mo(C1616Xf xf) {
        super(xf);
    }

    /* renamed from: c */
    public C5663aRz[] mo309c() {
        return Node._m_fields;
    }

    /* renamed from: d */
    public void mo310d() {
        super.mo310d();
        this.handle = null;
        this.f1149zP = null;
        this.bcq = 0.0f;
        this.f1114LQ = null;
        this.f1127jn = null;
        this.f1122jU = null;
        this.bcs = false;
        this.position = null;
        this.radius = 0.0f;
        this.f1141rI = null;
        this.f1142rL = null;
        this.f1115Mm = null;
        this.bcw = null;
        this.f1121jT = null;
        this.f1120jS = null;
        this.f1134nh = null;
        this.f1123jV = 0.0f;
        this.bcy = false;
        this.f1118bK = null;
        this.f1139ok = 0;
        this.f1116Nf = 0;
        this.dzH = 0;
        this.ayc = 0;
        this.f1128jy = 0;
        this.f1126jZ = 0;
        this.dzI = 0;
        this.f1143rX = 0;
        this.f1112DA = 0;
        this.f1144rY = 0;
        this.f1145sb = 0;
        this.dzJ = 0;
        this.dzK = 0;
        this.f1125jY = 0;
        this.f1124jX = 0;
        this.f1135nk = 0;
        this.f1129ka = 0;
        this.dzL = 0;
        this.f1137oL = 0;
        this.f1140on = 0;
        this.f1117Nh = 0;
        this.dzM = 0;
        this.ayr = 0;
        this.f1119jK = 0;
        this.f1132ke = 0;
        this.dzN = 0;
        this.f1146sn = 0;
        this.f1113DE = 0;
        this.f1147so = 0;
        this.f1148sr = 0;
        this.dzO = 0;
        this.dzP = 0;
        this.f1131kd = 0;
        this.f1130kc = 0;
        this.f1136nn = 0;
        this.f1133kf = 0;
        this.dzQ = 0;
        this.f1138oS = 0;
    }
}
