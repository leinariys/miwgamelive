package logic.data.mbean;

import game.network.message.serializable.C3438ra;
import game.script.itemgen.ItemGenSet;
import game.script.itemgen.ItemGenTable;
import logic.baa.C1616Xf;
import logic.res.code.C5663aRz;
import p001a.C0064Am;

import java.util.UUID;

/* renamed from: a.aup  reason: case insensitive filesystem */
public class C6849aup extends C5292aDs {
    @C0064Am(aul = "f3d354e2dfff439b3a043c2710d45773", aum = 0)
    public int aPn;
    @C0064Am(aul = "30675382919dd52e3207dde59048b09a", aum = 1)
    public C3438ra<ItemGenSet> aPp;
    @C0064Am(aul = "d41b0e53cc1803dbc511387492da9258", aum = 2)

    /* renamed from: bK */
    public UUID f5417bK;
    @C0064Am(aul = "f3d354e2dfff439b3a043c2710d45773", aum = -2)
    public long gCR;
    @C0064Am(aul = "30675382919dd52e3207dde59048b09a", aum = -2)
    public long gCS;
    @C0064Am(aul = "f3d354e2dfff439b3a043c2710d45773", aum = -1)
    public byte gCT;
    @C0064Am(aul = "30675382919dd52e3207dde59048b09a", aum = -1)
    public byte gCU;
    @C0064Am(aul = "c21eaef056fa24da46cbc7c875a28c4d", aum = 3)
    public String handle;
    @C0064Am(aul = "d41b0e53cc1803dbc511387492da9258", aum = -2)

    /* renamed from: oL */
    public long f5418oL;
    @C0064Am(aul = "d41b0e53cc1803dbc511387492da9258", aum = -1)

    /* renamed from: oS */
    public byte f5419oS;
    @C0064Am(aul = "c21eaef056fa24da46cbc7c875a28c4d", aum = -2)

    /* renamed from: ok */
    public long f5420ok;
    @C0064Am(aul = "c21eaef056fa24da46cbc7c875a28c4d", aum = -1)

    /* renamed from: on */
    public byte f5421on;

    public C6849aup() {
    }

    public C6849aup(C1616Xf xf) {
        super(xf);
    }

    /* renamed from: c */
    public C5663aRz[] mo309c() {
        return ItemGenTable._m_fields;
    }

    /* renamed from: d */
    public void mo310d() {
        super.mo310d();
        this.aPn = 0;
        this.aPp = null;
        this.f5417bK = null;
        this.handle = null;
        this.gCR = 0;
        this.gCS = 0;
        this.f5418oL = 0;
        this.f5420ok = 0;
        this.gCT = 0;
        this.gCU = 0;
        this.f5419oS = 0;
        this.f5421on = 0;
    }
}
