package logic.data.mbean;

import game.geometry.Vec3d;
import game.script.ai.npc.PassiveFighterAIController;
import logic.baa.C1616Xf;
import logic.res.code.C5663aRz;
import p001a.C0064Am;

/* renamed from: a.oy */
public class C3195oy extends C5574aOo {
    @C0064Am(aul = "72b95085ed8e8aa8b204d6667666ef82", aum = -2)
    public long aPL;
    @C0064Am(aul = "8313b3693098a7a22f1024b18c591766", aum = -2)
    public long aPM;
    @C0064Am(aul = "72b95085ed8e8aa8b204d6667666ef82", aum = -1)
    public byte aPN;
    @C0064Am(aul = "8313b3693098a7a22f1024b18c591766", aum = -1)
    public byte aPO;
    @C0064Am(aul = "72b95085ed8e8aa8b204d6667666ef82", aum = 0)
    public Vec3d center;
    @C0064Am(aul = "8313b3693098a7a22f1024b18c591766", aum = 1)
    public float radius2;

    public C3195oy() {
    }

    public C3195oy(C1616Xf xf) {
        super(xf);
    }

    /* renamed from: c */
    public C5663aRz[] mo309c() {
        return PassiveFighterAIController._m_fields;
    }

    /* renamed from: d */
    public void mo310d() {
        super.mo310d();
        this.center = null;
        this.radius2 = 0.0f;
        this.aPL = 0;
        this.aPM = 0;
        this.aPN = 0;
        this.aPO = 0;
    }
}
