package logic.data.mbean;

import game.script.mission.scripting.ReconNpcMissionScript;
import game.script.ship.Ship;
import logic.baa.C1616Xf;
import logic.res.code.C5663aRz;
import p001a.C0064Am;

/* renamed from: a.auI  reason: case insensitive filesystem */
public class C6816auI extends C5292aDs {
    @C0064Am(aul = "80ba0190da18e0895910246a0eec5bff", aum = 0)

    /* renamed from: QR */
    public Ship f5361QR;
    @C0064Am(aul = "39620c7064f3002287c18e82587c9e5f", aum = 2)

    /* renamed from: QV */
    public float f5362QV;
    @C0064Am(aul = "195824867636a172932b4cc1e216a128", aum = 1)
    public Ship bww;
    @C0064Am(aul = "80ba0190da18e0895910246a0eec5bff", aum = -2)
    public long dPm;
    @C0064Am(aul = "195824867636a172932b4cc1e216a128", aum = -2)
    public long dPn;
    @C0064Am(aul = "39620c7064f3002287c18e82587c9e5f", aum = -2)
    public long dPo;
    @C0064Am(aul = "80ba0190da18e0895910246a0eec5bff", aum = -1)
    public byte dPp;
    @C0064Am(aul = "195824867636a172932b4cc1e216a128", aum = -1)
    public byte dPq;
    @C0064Am(aul = "39620c7064f3002287c18e82587c9e5f", aum = -1)
    public byte dPr;
    @C0064Am(aul = "0a671c592b2f15389508806401402ddf", aum = -2)

    /* renamed from: dl */
    public long f5363dl;
    @C0064Am(aul = "0a671c592b2f15389508806401402ddf", aum = -1)

    /* renamed from: ds */
    public byte f5364ds;
    @C0064Am(aul = "0a671c592b2f15389508806401402ddf", aum = 3)

    /* renamed from: ym */
    public ReconNpcMissionScript f5365ym;

    public C6816auI() {
    }

    public C6816auI(C1616Xf xf) {
        super(xf);
    }

    /* renamed from: c */
    public C5663aRz[] mo309c() {
        return ReconNpcMissionScript.SelectionCallback._m_fields;
    }

    /* renamed from: d */
    public void mo310d() {
        super.mo310d();
        this.f5361QR = null;
        this.bww = null;
        this.f5362QV = 0.0f;
        this.f5365ym = null;
        this.dPm = 0;
        this.dPn = 0;
        this.dPo = 0;
        this.f5363dl = 0;
        this.dPp = 0;
        this.dPq = 0;
        this.dPr = 0;
        this.f5364ds = 0;
    }
}
