package logic.data.mbean;

import game.network.message.serializable.C2686iZ;
import game.script.contract.ContractBoard;
import game.script.contract.types.BountyBaseInfo;
import game.script.contract.types.ContractTemplate;
import game.script.contract.types.TransportBaseInfo;
import logic.baa.C1616Xf;
import logic.res.code.C5663aRz;
import p001a.C0064Am;

import java.util.UUID;

/* renamed from: a.ahz  reason: case insensitive filesystem */
public class C6183ahz extends C3805vD {
    @C0064Am(aul = "e9bcc88ea0fb8094b5757f8f27d5caac", aum = 2)

    /* renamed from: bK */
    public UUID f4577bK;
    @C0064Am(aul = "5e1f73fe93eded9d7186593a0cb1a579", aum = 0)
    public C2686iZ<ContractTemplate> fKX;
    @C0064Am(aul = "b5152398d3bb8599850a69c1b96d1324", aum = 1)
    public ContractBoard.PlayerDisposedListener fKY;
    @C0064Am(aul = "7588aae05e7a0a0be5703079873a505e", aum = 3)
    public TransportBaseInfo fKZ;
    @C0064Am(aul = "c9510742837d54e1d4b83032ebd77f00", aum = 4)
    public BountyBaseInfo fLa;
    @C0064Am(aul = "5e1f73fe93eded9d7186593a0cb1a579", aum = -2)
    public long fLb;
    @C0064Am(aul = "7588aae05e7a0a0be5703079873a505e", aum = -2)
    public long fLc;
    @C0064Am(aul = "c9510742837d54e1d4b83032ebd77f00", aum = -2)
    public long fLd;
    @C0064Am(aul = "5e1f73fe93eded9d7186593a0cb1a579", aum = -1)
    public byte fLe;
    @C0064Am(aul = "7588aae05e7a0a0be5703079873a505e", aum = -1)
    public byte fLf;
    @C0064Am(aul = "c9510742837d54e1d4b83032ebd77f00", aum = -1)
    public byte fLg;
    @C0064Am(aul = "b5152398d3bb8599850a69c1b96d1324", aum = -2)
    public long faI;
    @C0064Am(aul = "b5152398d3bb8599850a69c1b96d1324", aum = -1)
    public byte faP;
    @C0064Am(aul = "e9bcc88ea0fb8094b5757f8f27d5caac", aum = -2)

    /* renamed from: oL */
    public long f4578oL;
    @C0064Am(aul = "e9bcc88ea0fb8094b5757f8f27d5caac", aum = -1)

    /* renamed from: oS */
    public byte f4579oS;

    public C6183ahz() {
    }

    public C6183ahz(C1616Xf xf) {
        super(xf);
    }

    /* renamed from: c */
    public C5663aRz[] mo309c() {
        return ContractBoard._m_fields;
    }

    /* renamed from: d */
    public void mo310d() {
        super.mo310d();
        this.fKX = null;
        this.fKY = null;
        this.f4577bK = null;
        this.fKZ = null;
        this.fLa = null;
        this.fLb = 0;
        this.faI = 0;
        this.f4578oL = 0;
        this.fLc = 0;
        this.fLd = 0;
        this.fLe = 0;
        this.faP = 0;
        this.f4579oS = 0;
        this.fLf = 0;
        this.fLg = 0;
    }
}
