package logic.data.mbean;

import game.script.mission.scripting.LootItemMissionScript;
import logic.baa.C1616Xf;
import logic.res.code.C5663aRz;
import p001a.C0064Am;

/* renamed from: a.aLm  reason: case insensitive filesystem */
public class C5494aLm extends C2955mD {
    @C0064Am(aul = "cea97a5b014a3dbc4c0ef35733cfe769", aum = 0)
    public LootItemMissionScript awH;
    @C0064Am(aul = "cea97a5b014a3dbc4c0ef35733cfe769", aum = -2)

    /* renamed from: dl */
    public long f3332dl;
    @C0064Am(aul = "cea97a5b014a3dbc4c0ef35733cfe769", aum = -1)

    /* renamed from: ds */
    public byte f3333ds;

    public C5494aLm() {
    }

    public C5494aLm(C1616Xf xf) {
        super(xf);
    }

    /* renamed from: c */
    public C5663aRz[] mo309c() {
        return LootItemMissionScript.StateA._m_fields;
    }

    /* renamed from: d */
    public void mo310d() {
        super.mo310d();
        this.awH = null;
        this.f3332dl = 0;
        this.f3333ds = 0;
    }
}
