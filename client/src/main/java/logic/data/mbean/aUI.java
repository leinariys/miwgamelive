package logic.data.mbean;

import game.network.message.serializable.C1556Wo;
import game.network.message.serializable.C2686iZ;
import game.script.item.ItemTypeCategory;
import game.script.item.Weapon;
import game.script.item.WeaponType;
import game.script.item.buff.amplifier.WeaponAmplifier;
import logic.baa.C1616Xf;
import logic.res.code.C5663aRz;
import p001a.C0064Am;
import p001a.C3892wO;

/* renamed from: a.aUI */
public class aUI extends C3108nv {
    @C0064Am(aul = "97a17f04fc3cb2502ce613a39f1668b6", aum = -2)
    public long aIR;
    @C0064Am(aul = "97a17f04fc3cb2502ce613a39f1668b6", aum = -1)
    public byte aJc;
    @C0064Am(aul = "7ca0a6c58d9bf5bdc6816f97ea9bcb10", aum = -2)
    public long aso;
    @C0064Am(aul = "7ca0a6c58d9bf5bdc6816f97ea9bcb10", aum = -1)
    public byte ast;
    @C0064Am(aul = "6fa74346f871afe91fa21d1fb12b1641", aum = 0)
    public C3892wO bEb;
    @C0064Am(aul = "97a17f04fc3cb2502ce613a39f1668b6", aum = 1)
    public C3892wO bEd;
    @C0064Am(aul = "00de76aea9f75f17cb4417b6d5421ab6", aum = 2)
    public C3892wO bEe;
    @C0064Am(aul = "c10ad9140131e5d47da2f29232156338", aum = 3)
    public C3892wO bEf;
    @C0064Am(aul = "69fc5a20ea75797cb33505645f64a94d", aum = 4)
    public C3892wO bEh;
    @C0064Am(aul = "ece4507578e61f6323470f9fb36ab358", aum = 5)
    public boolean bEj;
    @C0064Am(aul = "ece4507578e61f6323470f9fb36ab358", aum = -2)
    public long bIG;
    @C0064Am(aul = "6fa74346f871afe91fa21d1fb12b1641", aum = -2)
    public long bIH;
    @C0064Am(aul = "00de76aea9f75f17cb4417b6d5421ab6", aum = -2)
    public long bIJ;
    @C0064Am(aul = "ece4507578e61f6323470f9fb36ab358", aum = -1)
    public byte bIS;
    @C0064Am(aul = "6fa74346f871afe91fa21d1fb12b1641", aum = -1)
    public byte bIT;
    @C0064Am(aul = "00de76aea9f75f17cb4417b6d5421ab6", aum = -1)
    public byte bIV;
    @C0064Am(aul = "7ccb93e3eeb184e2d3ccec7b632480f0", aum = 8)
    public C1556Wo<Weapon, WeaponAmplifier.WeaponAmplifierAdapter> bjL;
    @C0064Am(aul = "fe0d5dcd35e6a9685146c9eb6c25a3de", aum = 6)
    public C2686iZ<ItemTypeCategory> ckK;
    @C0064Am(aul = "7ca0a6c58d9bf5bdc6816f97ea9bcb10", aum = 7)
    public C2686iZ<WeaponType> ckL;
    @C0064Am(aul = "fe0d5dcd35e6a9685146c9eb6c25a3de", aum = -2)
    public long ckM;
    @C0064Am(aul = "fe0d5dcd35e6a9685146c9eb6c25a3de", aum = -1)
    public byte ckN;
    @C0064Am(aul = "7ccb93e3eeb184e2d3ccec7b632480f0", aum = -2)
    public long iFp;
    @C0064Am(aul = "7ccb93e3eeb184e2d3ccec7b632480f0", aum = -1)
    public byte iFu;
    @C0064Am(aul = "c10ad9140131e5d47da2f29232156338", aum = -1)

    /* renamed from: tO */
    public byte f3844tO;
    @C0064Am(aul = "69fc5a20ea75797cb33505645f64a94d", aum = -1)

    /* renamed from: tS */
    public byte f3845tS;
    @C0064Am(aul = "c10ad9140131e5d47da2f29232156338", aum = -2)

    /* renamed from: tl */
    public long f3846tl;
    @C0064Am(aul = "69fc5a20ea75797cb33505645f64a94d", aum = -2)

    /* renamed from: tp */
    public long f3847tp;

    public aUI() {
    }

    public aUI(C1616Xf xf) {
        super(xf);
    }

    /* renamed from: c */
    public C5663aRz[] mo309c() {
        return WeaponAmplifier._m_fields;
    }

    /* renamed from: d */
    public void mo310d() {
        super.mo310d();
        this.bEb = null;
        this.bEd = null;
        this.bEe = null;
        this.bEf = null;
        this.bEh = null;
        this.bEj = false;
        this.ckK = null;
        this.ckL = null;
        this.bjL = null;
        this.bIH = 0;
        this.aIR = 0;
        this.bIJ = 0;
        this.f3846tl = 0;
        this.f3847tp = 0;
        this.bIG = 0;
        this.ckM = 0;
        this.aso = 0;
        this.iFp = 0;
        this.bIT = 0;
        this.aJc = 0;
        this.bIV = 0;
        this.f3844tO = 0;
        this.f3845tS = 0;
        this.bIS = 0;
        this.ckN = 0;
        this.ast = 0;
        this.iFu = 0;
    }
}
