package logic.data.mbean;

import game.network.message.serializable.C1556Wo;
import game.script.item.ItemType;
import game.script.mission.scripting.MiningMissionScript;
import logic.baa.C1616Xf;
import logic.res.code.C5663aRz;
import p001a.C0064Am;

/* renamed from: a.NK */
public class C0908NK extends aMY {
    @C0064Am(aul = "d56d195d6e625bf6c61e4cd0031fc462", aum = 2)
    public C1556Wo<ItemType, Integer> cyk;
    @C0064Am(aul = "d56d195d6e625bf6c61e4cd0031fc462", aum = -2)
    public long dCU;
    @C0064Am(aul = "d56d195d6e625bf6c61e4cd0031fc462", aum = -1)
    public byte dCW;
    @C0064Am(aul = "19be3ead40d41187d28fe4211efa329b", aum = 0)
    public C1556Wo<ItemType, Integer> dHL;
    @C0064Am(aul = "7640a7ffc4d46be109b2f6f2d14706ea", aum = 1)
    public C1556Wo<ItemType, Integer> dHM;
    @C0064Am(aul = "6713d31aff3defcfd62eeef38288c166", aum = 3)
    public boolean dHN;
    @C0064Am(aul = "306f24033cb4e9a2f51f7967754c21b9", aum = 4)
    public boolean dHO;
    @C0064Am(aul = "19be3ead40d41187d28fe4211efa329b", aum = -2)
    public long dHP;
    @C0064Am(aul = "7640a7ffc4d46be109b2f6f2d14706ea", aum = -2)
    public long dHQ;
    @C0064Am(aul = "6713d31aff3defcfd62eeef38288c166", aum = -2)
    public long dHR;
    @C0064Am(aul = "306f24033cb4e9a2f51f7967754c21b9", aum = -2)
    public long dHS;
    @C0064Am(aul = "19be3ead40d41187d28fe4211efa329b", aum = -1)
    public byte dHT;
    @C0064Am(aul = "7640a7ffc4d46be109b2f6f2d14706ea", aum = -1)
    public byte dHU;
    @C0064Am(aul = "6713d31aff3defcfd62eeef38288c166", aum = -1)
    public byte dHV;
    @C0064Am(aul = "306f24033cb4e9a2f51f7967754c21b9", aum = -1)
    public byte dHW;

    public C0908NK() {
    }

    public C0908NK(C1616Xf xf) {
        super(xf);
    }

    /* renamed from: c */
    public C5663aRz[] mo309c() {
        return MiningMissionScript._m_fields;
    }

    /* renamed from: d */
    public void mo310d() {
        super.mo310d();
        this.dHL = null;
        this.dHM = null;
        this.cyk = null;
        this.dHN = false;
        this.dHO = false;
        this.dHP = 0;
        this.dHQ = 0;
        this.dCU = 0;
        this.dHR = 0;
        this.dHS = 0;
        this.dHT = 0;
        this.dHU = 0;
        this.dCW = 0;
        this.dHV = 0;
        this.dHW = 0;
    }
}
