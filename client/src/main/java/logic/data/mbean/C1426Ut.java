package logic.data.mbean;

import game.geometry.Vec3d;
import game.script.ai.npc.WanderAndShootAIController;
import logic.baa.C1616Xf;
import logic.res.code.C5663aRz;
import p001a.C0064Am;

/* renamed from: a.Ut */
public class C1426Ut extends C6937awc {
    @C0064Am(aul = "56c52ddeac3098754cc1f21bc228ff9e", aum = -2)
    public long aPL;
    @C0064Am(aul = "73b8927c76962633165f0a1568cae592", aum = -2)
    public long aPM;
    @C0064Am(aul = "56c52ddeac3098754cc1f21bc228ff9e", aum = -1)
    public byte aPN;
    @C0064Am(aul = "73b8927c76962633165f0a1568cae592", aum = -1)
    public byte aPO;
    @C0064Am(aul = "8e5221fdc60c4b91b0766d27f7835874", aum = -2)
    public long bGi;
    @C0064Am(aul = "b97399df89e11f4bbc4c62438ecfdabb", aum = -2)
    public long bGj;
    @C0064Am(aul = "d7a222275900cb1efe4ff9ac64547f37", aum = -2)
    public long bGk;
    @C0064Am(aul = "8e5221fdc60c4b91b0766d27f7835874", aum = -1)
    public byte bGl;
    @C0064Am(aul = "b97399df89e11f4bbc4c62438ecfdabb", aum = -1)
    public byte bGm;
    @C0064Am(aul = "d7a222275900cb1efe4ff9ac64547f37", aum = -1)
    public byte bGn;
    @C0064Am(aul = "56c52ddeac3098754cc1f21bc228ff9e", aum = 3)
    public Vec3d center;
    @C0064Am(aul = "d7a222275900cb1efe4ff9ac64547f37", aum = 2)

    /* renamed from: dE */
    public float f1826dE;
    @C0064Am(aul = "73b8927c76962633165f0a1568cae592", aum = 4)
    public float radius2;
    @C0064Am(aul = "8e5221fdc60c4b91b0766d27f7835874", aum = 0)
    public float shootAngle;
    @C0064Am(aul = "b97399df89e11f4bbc4c62438ecfdabb", aum = 1)
    public boolean shootPredictMovimet;

    public C1426Ut() {
    }

    public C1426Ut(C1616Xf xf) {
        super(xf);
    }

    /* renamed from: c */
    public C5663aRz[] mo309c() {
        return WanderAndShootAIController._m_fields;
    }

    /* renamed from: d */
    public void mo310d() {
        super.mo310d();
        this.shootAngle = 0.0f;
        this.shootPredictMovimet = false;
        this.f1826dE = 0.0f;
        this.center = null;
        this.radius2 = 0.0f;
        this.bGi = 0;
        this.bGj = 0;
        this.bGk = 0;
        this.aPL = 0;
        this.aPM = 0;
        this.bGl = 0;
        this.bGm = 0;
        this.bGn = 0;
        this.aPN = 0;
        this.aPO = 0;
    }
}
