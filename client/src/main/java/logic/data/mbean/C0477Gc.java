package logic.data.mbean;

import game.script.item.ItemType;
import game.script.npcchat.actions.TakeItemSpeechAction;
import logic.baa.C1616Xf;
import logic.res.code.C5663aRz;
import p001a.C0064Am;

/* renamed from: a.Gc */
public class C0477Gc extends C6352alM {
    @C0064Am(aul = "61db8cd4f54f209441a9205d43413b82", aum = 0)
    public ItemType cXt;
    @C0064Am(aul = "8d7d2f88116c8ddcbbb3eab40cf09146", aum = 1)
    public int cXu;
    @C0064Am(aul = "61db8cd4f54f209441a9205d43413b82", aum = -2)
    public long cXv;
    @C0064Am(aul = "8d7d2f88116c8ddcbbb3eab40cf09146", aum = -2)
    public long cXw;
    @C0064Am(aul = "61db8cd4f54f209441a9205d43413b82", aum = -1)
    public byte cXx;
    @C0064Am(aul = "8d7d2f88116c8ddcbbb3eab40cf09146", aum = -1)
    public byte cXy;

    public C0477Gc() {
    }

    public C0477Gc(C1616Xf xf) {
        super(xf);
    }

    /* renamed from: c */
    public C5663aRz[] mo309c() {
        return TakeItemSpeechAction._m_fields;
    }

    /* renamed from: d */
    public void mo310d() {
        super.mo310d();
        this.cXt = null;
        this.cXu = 0;
        this.cXv = 0;
        this.cXw = 0;
        this.cXx = 0;
        this.cXy = 0;
    }
}
