package logic.data.mbean;

import game.script.item.buff.amplifier.ShieldAmplifierType;
import logic.baa.C1616Xf;
import logic.res.code.C5663aRz;
import p001a.C0064Am;
import p001a.C3892wO;

/* renamed from: a.ajO  reason: case insensitive filesystem */
public class C6250ajO extends C2421fH {
    @C0064Am(aul = "7d5695cb56bf06ee445e2c5b08beae88", aum = 0)
    public C3892wO aBJ;
    @C0064Am(aul = "57d379b003bf15f43de78a1dedfdb204", aum = 1)
    public C3892wO aBK;
    @C0064Am(aul = "e455d77f1bcca35c259fd0924d9176a1", aum = 2)
    public C3892wO aBL;
    @C0064Am(aul = "9c44336dc537b04308c2dd3227fa72f3", aum = 3)
    public C3892wO aBM;
    @C0064Am(aul = "7d5695cb56bf06ee445e2c5b08beae88", aum = -2)
    public long aBO;
    @C0064Am(aul = "57d379b003bf15f43de78a1dedfdb204", aum = -2)
    public long aBP;
    @C0064Am(aul = "e455d77f1bcca35c259fd0924d9176a1", aum = -2)
    public long aBQ;
    @C0064Am(aul = "9c44336dc537b04308c2dd3227fa72f3", aum = -2)
    public long aBR;
    @C0064Am(aul = "7d5695cb56bf06ee445e2c5b08beae88", aum = -1)
    public byte aBT;
    @C0064Am(aul = "57d379b003bf15f43de78a1dedfdb204", aum = -1)
    public byte aBU;
    @C0064Am(aul = "e455d77f1bcca35c259fd0924d9176a1", aum = -1)
    public byte aBV;
    @C0064Am(aul = "9c44336dc537b04308c2dd3227fa72f3", aum = -1)
    public byte aBW;

    public C6250ajO() {
    }

    public C6250ajO(C1616Xf xf) {
        super(xf);
    }

    /* renamed from: c */
    public C5663aRz[] mo309c() {
        return ShieldAmplifierType._m_fields;
    }

    /* renamed from: d */
    public void mo310d() {
        super.mo310d();
        this.aBJ = null;
        this.aBK = null;
        this.aBL = null;
        this.aBM = null;
        this.aBO = 0;
        this.aBP = 0;
        this.aBQ = 0;
        this.aBR = 0;
        this.aBT = 0;
        this.aBU = 0;
        this.aBV = 0;
        this.aBW = 0;
    }
}
