package logic.data.mbean;

import game.script.npcchat.requirement.NurseryTutorialCompletion;
import logic.baa.C1616Xf;
import logic.res.code.C5663aRz;
import p001a.C0064Am;
import p001a.aMU;

/* renamed from: a.La */
public class C0800La extends C5416aIm {
    @C0064Am(aul = "25aee06f9ef29c3ea0ead80101e1d524", aum = -2)
    public long dsW;
    @C0064Am(aul = "25aee06f9ef29c3ea0ead80101e1d524", aum = -1)
    public byte dsX;
    @C0064Am(aul = "25aee06f9ef29c3ea0ead80101e1d524", aum = 0)

    /* renamed from: zL */
    public aMU f1052zL;

    public C0800La() {
    }

    public C0800La(C1616Xf xf) {
        super(xf);
    }

    /* renamed from: c */
    public C5663aRz[] mo309c() {
        return NurseryTutorialCompletion._m_fields;
    }

    /* renamed from: d */
    public void mo310d() {
        super.mo310d();
        this.f1052zL = null;
        this.dsW = 0;
        this.dsX = 0;
    }
}
