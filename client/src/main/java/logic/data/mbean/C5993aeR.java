package logic.data.mbean;

import game.network.message.serializable.C3438ra;
import game.script.missiontemplate.WaypointDat;
import game.script.missiontemplate.scripting.ProtectMissionScriptTemplate;
import game.script.space.StellarSystem;
import logic.baa.C1616Xf;
import logic.res.code.C5663aRz;
import p001a.C0064Am;
import taikodom.infra.script.I18NString;

/* renamed from: a.aeR  reason: case insensitive filesystem */
public class C5993aeR extends C0597IR {
    @C0064Am(aul = "c28188c2b325bc636b8e03cfe4f5988b", aum = 9)
    public I18NString cEA;
    @C0064Am(aul = "94e665e6e9ca7cd0d52f99796faa4082", aum = 10)
    public int cEC;
    @C0064Am(aul = "89ac625e7572268f1c84022d9001934a", aum = 11)
    public boolean cEE;
    @C0064Am(aul = "2b377bdb2ddd5b046391cb4c45c4e647", aum = 12)
    public I18NString cEG;
    @C0064Am(aul = "d995a4fd37b3da8fc0c1872173575efa", aum = 13)
    public boolean cEI;
    @C0064Am(aul = "cbba63929505c1c195071c529b08a773", aum = 14)
    public I18NString cEK;
    @C0064Am(aul = "0e19114e379ff732bb823c1ff984d50c", aum = 15)
    public boolean cEM;
    @C0064Am(aul = "b9d198980194af0e11c92452c9ddf2f4", aum = 16)
    public boolean cEO;
    @C0064Am(aul = "73a6819e349bcffd7adca23283c8c2a2", aum = 17)
    public boolean cEQ;
    @C0064Am(aul = "6f4dd42b96549842116c25cca28c9949", aum = 18)
    public boolean cES;
    @C0064Am(aul = "e838511b0e135f0f236eecfd5feaf4d4", aum = 1)
    public WaypointDat cEk;
    @C0064Am(aul = "c3c8fa7b420640dab1fb40ffc9dfe73c", aum = 2)
    public C3438ra<WaypointDat> cEm;
    @C0064Am(aul = "f061195211c816dec7753b4b1c2c64e6", aum = 3)
    public float cEo;
    @C0064Am(aul = "946c8699a5092858a98750b74ba185c9", aum = 4)
    public float cEq;
    @C0064Am(aul = "2eb64e5910ba3d3d8d4705dc936bfe81", aum = 5)
    public boolean cEs;
    @C0064Am(aul = "30940d18ec4fa07e985ce4b5128a484c", aum = 6)
    public I18NString cEu;
    @C0064Am(aul = "53dcf26168160e8020a6f41f4bc33195", aum = 7)
    public I18NString cEw;
    @C0064Am(aul = "4285a63100c7a52f1a765538b22485bd", aum = 8)
    public I18NString cEy;
    @C0064Am(aul = "e838511b0e135f0f236eecfd5feaf4d4", aum = -2)
    public long frL;
    @C0064Am(aul = "c3c8fa7b420640dab1fb40ffc9dfe73c", aum = -2)
    public long frM;
    @C0064Am(aul = "f061195211c816dec7753b4b1c2c64e6", aum = -2)
    public long frN;
    @C0064Am(aul = "946c8699a5092858a98750b74ba185c9", aum = -2)
    public long frO;
    @C0064Am(aul = "2eb64e5910ba3d3d8d4705dc936bfe81", aum = -2)
    public long frP;
    @C0064Am(aul = "30940d18ec4fa07e985ce4b5128a484c", aum = -2)
    public long frQ;
    @C0064Am(aul = "53dcf26168160e8020a6f41f4bc33195", aum = -2)
    public long frR;
    @C0064Am(aul = "4285a63100c7a52f1a765538b22485bd", aum = -2)
    public long frS;
    @C0064Am(aul = "c28188c2b325bc636b8e03cfe4f5988b", aum = -2)
    public long frT;
    @C0064Am(aul = "94e665e6e9ca7cd0d52f99796faa4082", aum = -2)
    public long frU;
    @C0064Am(aul = "89ac625e7572268f1c84022d9001934a", aum = -2)
    public long frV;
    @C0064Am(aul = "2b377bdb2ddd5b046391cb4c45c4e647", aum = -2)
    public long frW;
    @C0064Am(aul = "d995a4fd37b3da8fc0c1872173575efa", aum = -2)
    public long frX;
    @C0064Am(aul = "cbba63929505c1c195071c529b08a773", aum = -2)
    public long frY;
    @C0064Am(aul = "0e19114e379ff732bb823c1ff984d50c", aum = -2)
    public long frZ;
    @C0064Am(aul = "b9d198980194af0e11c92452c9ddf2f4", aum = -2)
    public long fsa;
    @C0064Am(aul = "73a6819e349bcffd7adca23283c8c2a2", aum = -2)
    public long fsb;
    @C0064Am(aul = "6f4dd42b96549842116c25cca28c9949", aum = -2)
    public long fsc;
    @C0064Am(aul = "e838511b0e135f0f236eecfd5feaf4d4", aum = -1)
    public byte fsd;
    @C0064Am(aul = "c3c8fa7b420640dab1fb40ffc9dfe73c", aum = -1)
    public byte fse;
    @C0064Am(aul = "f061195211c816dec7753b4b1c2c64e6", aum = -1)
    public byte fsf;
    @C0064Am(aul = "946c8699a5092858a98750b74ba185c9", aum = -1)
    public byte fsg;
    @C0064Am(aul = "2eb64e5910ba3d3d8d4705dc936bfe81", aum = -1)
    public byte fsh;
    @C0064Am(aul = "30940d18ec4fa07e985ce4b5128a484c", aum = -1)
    public byte fsi;
    @C0064Am(aul = "53dcf26168160e8020a6f41f4bc33195", aum = -1)
    public byte fsj;
    @C0064Am(aul = "4285a63100c7a52f1a765538b22485bd", aum = -1)
    public byte fsk;
    @C0064Am(aul = "c28188c2b325bc636b8e03cfe4f5988b", aum = -1)
    public byte fsl;
    @C0064Am(aul = "94e665e6e9ca7cd0d52f99796faa4082", aum = -1)
    public byte fsm;
    @C0064Am(aul = "89ac625e7572268f1c84022d9001934a", aum = -1)
    public byte fsn;
    @C0064Am(aul = "2b377bdb2ddd5b046391cb4c45c4e647", aum = -1)
    public byte fso;
    @C0064Am(aul = "d995a4fd37b3da8fc0c1872173575efa", aum = -1)
    public byte fsp;
    @C0064Am(aul = "cbba63929505c1c195071c529b08a773", aum = -1)
    public byte fsq;
    @C0064Am(aul = "0e19114e379ff732bb823c1ff984d50c", aum = -1)
    public byte fsr;
    @C0064Am(aul = "b9d198980194af0e11c92452c9ddf2f4", aum = -1)
    public byte fss;
    @C0064Am(aul = "73a6819e349bcffd7adca23283c8c2a2", aum = -1)
    public byte fst;
    @C0064Am(aul = "6f4dd42b96549842116c25cca28c9949", aum = -1)
    public byte fsu;
    @C0064Am(aul = "9f5da287cdfe101163e33d01a7571eb7", aum = 0)

    /* renamed from: rI */
    public StellarSystem f4389rI;
    @C0064Am(aul = "9f5da287cdfe101163e33d01a7571eb7", aum = -2)

    /* renamed from: rY */
    public long f4390rY;
    @C0064Am(aul = "9f5da287cdfe101163e33d01a7571eb7", aum = -1)

    /* renamed from: so */
    public byte f4391so;

    public C5993aeR() {
    }

    public C5993aeR(C1616Xf xf) {
        super(xf);
    }

    /* renamed from: c */
    public C5663aRz[] mo309c() {
        return ProtectMissionScriptTemplate._m_fields;
    }

    /* renamed from: d */
    public void mo310d() {
        super.mo310d();
        this.f4389rI = null;
        this.cEk = null;
        this.cEm = null;
        this.cEo = 0.0f;
        this.cEq = 0.0f;
        this.cEs = false;
        this.cEu = null;
        this.cEw = null;
        this.cEy = null;
        this.cEA = null;
        this.cEC = 0;
        this.cEE = false;
        this.cEG = null;
        this.cEI = false;
        this.cEK = null;
        this.cEM = false;
        this.cEO = false;
        this.cEQ = false;
        this.cES = false;
        this.f4390rY = 0;
        this.frL = 0;
        this.frM = 0;
        this.frN = 0;
        this.frO = 0;
        this.frP = 0;
        this.frQ = 0;
        this.frR = 0;
        this.frS = 0;
        this.frT = 0;
        this.frU = 0;
        this.frV = 0;
        this.frW = 0;
        this.frX = 0;
        this.frY = 0;
        this.frZ = 0;
        this.fsa = 0;
        this.fsb = 0;
        this.fsc = 0;
        this.f4391so = 0;
        this.fsd = 0;
        this.fse = 0;
        this.fsf = 0;
        this.fsg = 0;
        this.fsh = 0;
        this.fsi = 0;
        this.fsj = 0;
        this.fsk = 0;
        this.fsl = 0;
        this.fsm = 0;
        this.fsn = 0;
        this.fso = 0;
        this.fsp = 0;
        this.fsq = 0;
        this.fsr = 0;
        this.fss = 0;
        this.fst = 0;
        this.fsu = 0;
    }
}
