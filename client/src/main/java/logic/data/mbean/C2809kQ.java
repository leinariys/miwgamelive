package logic.data.mbean;

import game.network.message.serializable.C3438ra;
import game.script.pda.DatabaseCategory;
import game.script.pda.PdaDefaults;
import game.script.pda.TaikopediaEntry;
import logic.baa.C1616Xf;
import logic.res.code.C5663aRz;
import p001a.C0064Am;

import java.util.UUID;

/* renamed from: a.kQ */
public class C2809kQ extends C3805vD {
    @C0064Am(aul = "cd06878c5853948c2feae62976ec7849", aum = -2)
    public long avA;
    @C0064Am(aul = "678cd82bf1ae576aff46f2dd4d4b0eee", aum = -2)
    public long avB;
    @C0064Am(aul = "cd06878c5853948c2feae62976ec7849", aum = -1)
    public byte avC;
    @C0064Am(aul = "678cd82bf1ae576aff46f2dd4d4b0eee", aum = -1)
    public byte avD;
    @C0064Am(aul = "cd06878c5853948c2feae62976ec7849", aum = 0)
    public DatabaseCategory avy;
    @C0064Am(aul = "678cd82bf1ae576aff46f2dd4d4b0eee", aum = 1)
    public C3438ra<TaikopediaEntry> avz;
    @C0064Am(aul = "c2916df2e8e61288f71665a2deabca02", aum = 2)

    /* renamed from: bK */
    public UUID f8417bK;
    @C0064Am(aul = "c2916df2e8e61288f71665a2deabca02", aum = -2)

    /* renamed from: oL */
    public long f8418oL;
    @C0064Am(aul = "c2916df2e8e61288f71665a2deabca02", aum = -1)

    /* renamed from: oS */
    public byte f8419oS;

    public C2809kQ() {
    }

    public C2809kQ(C1616Xf xf) {
        super(xf);
    }

    /* renamed from: c */
    public C5663aRz[] mo309c() {
        return PdaDefaults._m_fields;
    }

    /* renamed from: d */
    public void mo310d() {
        super.mo310d();
        this.avy = null;
        this.avz = null;
        this.f8417bK = null;
        this.avA = 0;
        this.avB = 0;
        this.f8418oL = 0;
        this.avC = 0;
        this.avD = 0;
        this.f8419oS = 0;
    }
}
