package logic.data.mbean;

import game.script.BlockStatus;
import game.script.player.Player;
import logic.baa.C1616Xf;
import logic.res.code.C5663aRz;
import p001a.C0064Am;
import p001a.C2546ge;

/* renamed from: a.anW  reason: case insensitive filesystem */
public class C6466anW extends C6757atB {
    @C0064Am(aul = "203c3c3fc1e083830eebeb70817162cb", aum = 1)

    /* renamed from: P */
    public Player f4969P;
    @C0064Am(aul = "203c3c3fc1e083830eebeb70817162cb", aum = -2)

    /* renamed from: ag */
    public long f4970ag;
    @C0064Am(aul = "203c3c3fc1e083830eebeb70817162cb", aum = -1)

    /* renamed from: av */
    public byte f4971av;
    @C0064Am(aul = "a2bdcbc6ab40034c70f4388c01b06b23", aum = 2)
    public boolean enabled;
    @C0064Am(aul = "6de40260b478a81790fb648c060ab5cb", aum = 0)
    public C2546ge gfG;
    @C0064Am(aul = "6de40260b478a81790fb648c060ab5cb", aum = -2)
    public long gfH;
    @C0064Am(aul = "6de40260b478a81790fb648c060ab5cb", aum = -1)
    public byte gfI;
    @C0064Am(aul = "a2bdcbc6ab40034c70f4388c01b06b23", aum = -2)

    /* renamed from: jC */
    public long f4972jC;
    @C0064Am(aul = "a2bdcbc6ab40034c70f4388c01b06b23", aum = -1)

    /* renamed from: jO */
    public byte f4973jO;

    public C6466anW() {
    }

    public C6466anW(C1616Xf xf) {
        super(xf);
    }

    /* renamed from: c */
    public C5663aRz[] mo309c() {
        return BlockStatus._m_fields;
    }

    /* renamed from: d */
    public void mo310d() {
        super.mo310d();
        this.gfG = null;
        this.f4969P = null;
        this.enabled = false;
        this.gfH = 0;
        this.f4970ag = 0;
        this.f4972jC = 0;
        this.gfI = 0;
        this.f4971av = 0;
        this.f4973jO = 0;
    }
}
