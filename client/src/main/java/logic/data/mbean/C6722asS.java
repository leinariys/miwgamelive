package logic.data.mbean;

import game.script.bank.Bank;
import game.script.bank.BankStatementEntry;
import logic.baa.C1616Xf;
import logic.res.code.C5663aRz;
import p001a.C0064Am;

import java.util.Date;

/* renamed from: a.asS  reason: case insensitive filesystem */
public class C6722asS extends C3805vD {
    @C0064Am(aul = "cf42d542f6468d867882fae70a39c46e", aum = -2)
    public long aLG;
    @C0064Am(aul = "cf42d542f6468d867882fae70a39c46e", aum = -1)
    public byte aLL;
    @C0064Am(aul = "cf42d542f6468d867882fae70a39c46e", aum = 0)
    public long axi;
    @C0064Am(aul = "53687070015d56b1b614ec3eda5dbec4", aum = 2)
    public boolean gtB;
    @C0064Am(aul = "3acc348214af3ae6b768afbe66a0f519", aum = 3)
    public Date gtD;
    @C0064Am(aul = "b97768f872b882d9d27d14c5d8aa0acf", aum = 1)
    public Bank.C1861a gtz;
    @C0064Am(aul = "b97768f872b882d9d27d14c5d8aa0acf", aum = -2)
    public long gxk;
    @C0064Am(aul = "53687070015d56b1b614ec3eda5dbec4", aum = -2)
    public long gxl;
    @C0064Am(aul = "3acc348214af3ae6b768afbe66a0f519", aum = -2)
    public long gxm;
    @C0064Am(aul = "b97768f872b882d9d27d14c5d8aa0acf", aum = -1)
    public byte gxn;
    @C0064Am(aul = "53687070015d56b1b614ec3eda5dbec4", aum = -1)
    public byte gxo;
    @C0064Am(aul = "3acc348214af3ae6b768afbe66a0f519", aum = -1)
    public byte gxp;

    public C6722asS() {
    }

    public C6722asS(C1616Xf xf) {
        super(xf);
    }

    /* renamed from: c */
    public C5663aRz[] mo309c() {
        return BankStatementEntry._m_fields;
    }

    /* renamed from: d */
    public void mo310d() {
        super.mo310d();
        this.axi = 0;
        this.gtz = null;
        this.gtB = false;
        this.gtD = null;
        this.aLG = 0;
        this.gxk = 0;
        this.gxl = 0;
        this.gxm = 0;
        this.aLL = 0;
        this.gxn = 0;
        this.gxo = 0;
        this.gxp = 0;
    }
}
