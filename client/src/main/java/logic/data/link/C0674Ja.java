package logic.data.link;

import game.script.spacezone.AsteroidZoneCategory;
import logic.res.code.C5640aRc;
import logic.res.code.C5663aRz;

/* renamed from: a.Ja */
/* compiled from: a */
public interface C0674Ja extends C6408amQ {
    public static final C5663aRz avr = C5640aRc.m17844b(AsteroidZoneCategory.class, "bc04854cb7808cac5b1c94c8164aa1c4", -1);
    public static final C5663aRz avs = C5640aRc.m17844b(AsteroidZoneCategory.class, "85cdd11e392eb8e7d31b3d6fd9edbb71", -1);
    public static final C5663aRz avu = C5640aRc.m17844b(AsteroidZoneCategory.class, "065b7bbac39e271d42c4a1879072d44e", -1);
    public static final C5663aRz bgs = C5640aRc.m17844b(AsteroidZoneCategory.class, "129275444d262df156492ca5b22838b4", -1);

    /* renamed from: bk */
    public static final C5663aRz f794bk = C5640aRc.m17844b(AsteroidZoneCategory.class, "2438b99e9e79960d342305c7949e4794", -1);

    /* renamed from: cI */
    public static final C5663aRz f795cI = C5640aRc.m17844b(AsteroidZoneCategory.class, "ffa173a37c2692733272adbec436b48f", -1);

    /* renamed from: vD */
    public static final C5663aRz f796vD = C5640aRc.m17844b(AsteroidZoneCategory.class, "296688f3bfb30cc1f295f6dc5fd223a8", -1);

    /* renamed from: vE */
    public static final C5663aRz f797vE = C5640aRc.m17844b(AsteroidZoneCategory.class, "b3267866ac9b36a17a46cee98828fc3c", -1);

    /* renamed from: xf */
    public static final C5663aRz f798xf = C5640aRc.m17844b(AsteroidZoneCategory.class, "8f33df8f37e35a06009e2841192842d6", -1);
}
