package logic.data.link;

import game.script.ai.npc.DroneAIControllerType;
import logic.res.code.C5640aRc;
import logic.res.code.C5663aRz;

/* renamed from: a.Ij */
/* compiled from: a */
public interface C0615Ij extends C2548gg {
    public static final C5663aRz aiC = C5640aRc.m17844b(DroneAIControllerType.class, "d88ddce6045645c91a50dda25f1c4d40", -1);
    public static final C5663aRz aiD = C5640aRc.m17844b(DroneAIControllerType.class, "241348072fd9fe13845f67a0f5c4825f", -1);
    public static final C5663aRz deA = C5640aRc.m17844b(DroneAIControllerType.class, "1a1fffe0ef4ea23b262bf0f5a7ca9d58", -1);
    public static final C5663aRz deB = C5640aRc.m17844b(DroneAIControllerType.class, "824f7dad097148fbf4f854e4e66a88b4", -1);
    public static final C5663aRz deC = C5640aRc.m17844b(DroneAIControllerType.class, "ba126431149b0598bbc8371efdab6bba", -1);
    public static final C5663aRz deD = C5640aRc.m17844b(DroneAIControllerType.class, "c1d1c21b835a2ebb712822038ecb70a1", -1);
    public static final C5663aRz deE = C5640aRc.m17844b(DroneAIControllerType.class, "812bd8b496d59371cba2978b433fcce3", -1);
    public static final C5663aRz deF = C5640aRc.m17844b(DroneAIControllerType.class, "0f084d28688bc61da7763314af3114aa", -1);
    public static final C5663aRz deG = C5640aRc.m17844b(DroneAIControllerType.class, "f8a1ae4ad7e66c7fcf562db52fb5eb8b", -1);
}
