package logic.data.link;

import game.script.ship.speedBoost.SpeedBoostType;
import logic.res.code.C5640aRc;
import logic.res.code.C5663aRz;

/* renamed from: a.sI */
/* compiled from: a */
public interface C3537sI extends C0604IY {
    public static final C5663aRz bit = C5640aRc.m17844b(SpeedBoostType.class, "6835cac81de85fad250af753771a77d5", -1);
    public static final C5663aRz biu = C5640aRc.m17844b(SpeedBoostType.class, "1dfa34af20261536a19a522c330ba75c", -1);

    /* renamed from: zA */
    public static final C5663aRz f9093zA = C5640aRc.m17844b(SpeedBoostType.class, "6691ab8f67242c411e58cf9c85b1e074", -1);

    /* renamed from: zB */
    public static final C5663aRz f9094zB = C5640aRc.m17844b(SpeedBoostType.class, "36e1cb14132dbfb24889b0ec2a8a29d3", -1);

    /* renamed from: zC */
    public static final C5663aRz f9095zC = C5640aRc.m17844b(SpeedBoostType.class, "b7efd121f73916f4840bab0831461891", -1);

    /* renamed from: zD */
    public static final C5663aRz f9096zD = C5640aRc.m17844b(SpeedBoostType.class, "6f09d398b7698d6278fbad1563d428ba", -1);

    /* renamed from: zz */
    public static final C5663aRz f9097zz = C5640aRc.m17844b(SpeedBoostType.class, "92c670f229fe208a2643f825f089a2e5", -1);
}
