package logic.data.link;

import game.script.ship.Station;
import logic.res.code.C5640aRc;
import logic.res.code.C5663aRz;

/* renamed from: a.kN */
/* compiled from: a */
public interface C2806kN extends C3396rD {

    /* renamed from: TS */
    public static final C5663aRz f8411TS = C5640aRc.m17844b(Station.class, "db7588f8db5d40238035e900b09275af", -1);
    public static final C5663aRz avj = C5640aRc.m17844b(Station.class, "9d7c361079ec854372fa4ef5b16f6319", -1);
    public static final C5663aRz avk = C5640aRc.m17844b(Station.class, "f1b7687a150f7db7a3366e093bcf7e05", -1);
    public static final C5663aRz avl = C5640aRc.m17844b(Station.class, "3cb49860a2c28fd648493b989022d9dc", -1);
    public static final C5663aRz avm = C5640aRc.m17844b(Station.class, "45bed0de13fb210ea8e03b1ba41365c2", -1);
    public static final C5663aRz avn = C5640aRc.m17844b(Station.class, "fc83d33a2146223e13258376afa4fc16", -1);
    public static final C5663aRz avo = C5640aRc.m17844b(Station.class, "7a349fc8f317ede34c09f4f3ab0741eb", -1);
    public static final C5663aRz avp = C5640aRc.m17844b(Station.class, "3b498424d29b4073dc438ce096395dbc", -1);
    public static final C5663aRz avq = C5640aRc.m17844b(Station.class, "12c5c7c9f4d772e26612ad684a174b26", -1);
    public static final C5663aRz avr = C5640aRc.m17844b(Station.class, "841cc096ac605e2f71bcd3055db0447f", -1);
    public static final C5663aRz avs = C5640aRc.m17844b(Station.class, "78b68fc5f53af59d0c9d62c610ac27a0", -1);
    public static final C5663aRz avt = C5640aRc.m17844b(Station.class, "cf0259290db851f6d79ea712d71a9329", -1);
    public static final C5663aRz avu = C5640aRc.m17844b(Station.class, "0f324e22ba416a6336fc7408951f4ca5", -1);

    /* renamed from: bk */
    public static final C5663aRz f8412bk = C5640aRc.m17844b(Station.class, "1594952326e7602b876b1ae57346ed79", -1);

    /* renamed from: cI */
    public static final C5663aRz f8413cI = C5640aRc.m17844b(Station.class, "b43f15eebc7ed203b8cb4d65cf86322a", -1);

    /* renamed from: cg */
    public static final C5663aRz f8414cg = C5640aRc.m17844b(Station.class, "1f03c843afa88dcd5b33a3269e9f7a67", -1);

    /* renamed from: vE */
    public static final C5663aRz f8415vE = C5640aRc.m17844b(Station.class, "46c59775ff8eb1d867825694082df168", -1);

    /* renamed from: xf */
    public static final C5663aRz f8416xf = C5640aRc.m17844b(Station.class, "ee367c1cff0afbf7fd82a312481e7ce8", -1);
}
