package logic.data.link;

import game.script.progression.CharacterProgression;
import logic.res.code.C5640aRc;
import logic.res.code.C5663aRz;

/* renamed from: a.aww  reason: case insensitive filesystem */
/* compiled from: a */
public interface C6957aww extends C6408amQ {

    /* renamed from: Qs */
    public static final C5663aRz f5535Qs = C5640aRc.m17844b(CharacterProgression.class, "a2597cf387a8903fb213c5aa9c53c7cf", -1);
    public static final C5663aRz gMf = C5640aRc.m17844b(CharacterProgression.class, "2c62be179e7b3c4a748a72fded623fef", -1);
    public static final C5663aRz gMg = C5640aRc.m17844b(CharacterProgression.class, "098a76c15e902e20bbf995c6af8378bd", -1);
    public static final C5663aRz gMh = C5640aRc.m17844b(CharacterProgression.class, "4770e2dcecc74c6db288321681dc24cc", -1);
    public static final C5663aRz gMi = C5640aRc.m17844b(CharacterProgression.class, "5a1c61db4888f8027e899cd59c857f08", -1);
}
