package logic.data.link;

import game.script.mission.MissionTemplate;
import logic.res.code.C5640aRc;
import logic.res.code.C5663aRz;

/* renamed from: a.aoU  reason: case insensitive filesystem */
/* compiled from: a */
public interface C6516aoU extends C6408amQ {
    public static final C5663aRz bgt = C5640aRc.m17844b(MissionTemplate.class, "e8bb52dcbe49f51b58c4110be1d08267", -1);
    public static final C5663aRz bjz = C5640aRc.m17844b(MissionTemplate.class, "16bfed488d94be91915de8cd37ca1a18", -1);

    /* renamed from: bk */
    public static final C5663aRz f5012bk = C5640aRc.m17844b(MissionTemplate.class, "5c863d31d77047b824bfa91c51b93b82", -1);
    public static final C5663aRz bvV = C5640aRc.m17844b(MissionTemplate.class, "e95eabb42293c2f61e67ba06e48ba0b4", -1);

    /* renamed from: cI */
    public static final C5663aRz f5013cI = C5640aRc.m17844b(MissionTemplate.class, "07d6f56499cb8ec9287c795e47c8051c", -1);
    public static final C5663aRz gjF = C5640aRc.m17844b(MissionTemplate.class, "07229a7365ab7eab75a00de9cf42fe35", -1);
    public static final C5663aRz gjG = C5640aRc.m17844b(MissionTemplate.class, "af6fe1b05998f9551b99f633302fd75a", -1);
    public static final C5663aRz gjH = C5640aRc.m17844b(MissionTemplate.class, "e1c851e2b1c3f1e74553b1a1b50b875a", -1);
    public static final C5663aRz gjI = C5640aRc.m17844b(MissionTemplate.class, "b28fbdeb613584b709a0bdb2ef2ace34", -1);
    public static final C5663aRz gjJ = C5640aRc.m17844b(MissionTemplate.class, "2c9b032b586e63862af93939dc8c78d7", -1);
    public static final C5663aRz gjK = C5640aRc.m17844b(MissionTemplate.class, "93d9b483a38cdf232454a48d498ae4b3", -1);
    public static final C5663aRz gjL = C5640aRc.m17844b(MissionTemplate.class, "54db8f2fa01eea0857e3bb77497402a1", -1);
    public static final C5663aRz gjM = C5640aRc.m17844b(MissionTemplate.class, "635f871eb8f677762484a185dd6c8e6f", -1);
    public static final C5663aRz gjN = C5640aRc.m17844b(MissionTemplate.class, "e3a46ee3f04e1aaaf2994964834e4ef0", -1);
    public static final C5663aRz gjO = C5640aRc.m17844b(MissionTemplate.class, "40a1070b4cdab4c39e36d02fbb854307", -1);
    public static final C5663aRz gjP = C5640aRc.m17844b(MissionTemplate.class, "4dc6c2401bfd536e4ba8fa188f78ba78", -1);
    public static final C5663aRz gjQ = C5640aRc.m17844b(MissionTemplate.class, "506d50fdcd306ff559c3a215a2fbdfb5", -1);

    /* renamed from: vD */
    public static final C5663aRz f5014vD = C5640aRc.m17844b(MissionTemplate.class, "bd5d2d7b177f325d9f7fbd95b43cdcda", -1);

    /* renamed from: vE */
    public static final C5663aRz f5015vE = C5640aRc.m17844b(MissionTemplate.class, "ccaefa7be9b5e7a29b4f7e4c70c8783f", -1);
}
