package logic.data.link;

import game.script.Actor;
import logic.baa.C4062yl;
import logic.res.code.C5640aRc;
import logic.res.code.C5663aRz;
import p001a.C0064Am;

import java.io.Serializable;

/* renamed from: a.rD */
/* compiled from: a */
public interface C3396rD extends C6408amQ {
    public static final C5663aRz bas = C5640aRc.m17844b(Actor.class, "4eeee5d4e9f83c40debb2bf18bf53646", -1);
    public static final C5663aRz bbA = C5640aRc.m17844b(Actor.class, "38997360d48b51df96f7c0b393c0a43d", -1);
    public static final C5663aRz bbB = C5640aRc.m17844b(Actor.class, "584698af0f9efd8a233180c704ebbf7d", -1);
    public static final C5663aRz bbC = C5640aRc.m17844b(Actor.class, "30e07c4a83ade1e327032ad87cee176b", -1);
    public static final C5663aRz bbD = C5640aRc.m17844b(Actor.class, "edb0e66f936dca77af3aee12ebe04082", -1);
    public static final C5663aRz bbE = C5640aRc.m17844b(Actor.class, "5ef7d60da3a8b2ceb8ad24111107bebf", -1);
    public static final C5663aRz bbF = C5640aRc.m17844b(Actor.class, "ee970e8a1f16d5b7dee4c99e5eb7ea18", -1);
    public static final C5663aRz bbG = C5640aRc.m17844b(Actor.class, "a3458a5b27bf26c0b6b7991dfe0f864d", -1);
    public static final String bbr = "b4248dc0637bb4cbfc146f6209da6d2d";
    public static final C5663aRz bbs = C5640aRc.m17844b(Actor.class, "bf81f67cc27de4cb991f3dcbed63f57e", -1);
    public static final C5663aRz bbt = C5640aRc.m17844b(Actor.class, "dceb75b2bf8b667232e642770a9fcfba", -1);
    public static final C5663aRz bbu = C5640aRc.m17844b(Actor.class, "18880dbcb97b692a0e0f58790deaa20e", -1);
    public static final C5663aRz bbv = C5640aRc.m17844b(Actor.class, "7d187fb42cc298453221b0b97fb66e14", -1);
    public static final C5663aRz bbw = C5640aRc.m17844b(Actor.class, "66f7fbfb45e906eaba15a29293988dce", -1);
    public static final C5663aRz bbx = C5640aRc.m17844b(Actor.class, "c8e87e5dae9bd28c5dd68ed24a212353", -1);
    public static final C5663aRz bby = C5640aRc.m17844b(Actor.class, "6950ee21a27c9b33478239361b06012e", -1);
    public static final C5663aRz bbz = C5640aRc.m17844b(Actor.class, "643e0f783e741647789a48071c1f3bbd", -1);

    @C0064Am(aul = "ad659d86dedbfa4646807b375225e5a1")
    /* renamed from: a.rD$a */
    public interface C3397a extends C4062yl, Serializable {
        /* renamed from: m */
        void mo18227m(Actor cr);
    }

    @C0064Am(aul = "dd78647e651e43d5a4b30e6f45c383ce")
    /* renamed from: a.rD$b */
    /* compiled from: a */
    public interface C3398b extends C4062yl, Serializable {
        /* renamed from: ah */
        void mo21556ah(Actor cr);
    }
}
