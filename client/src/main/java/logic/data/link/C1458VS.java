package logic.data.link;

import game.script.item.buff.amplifier.CruiseSpeedAmplifierType;
import logic.res.code.C5640aRc;
import logic.res.code.C5663aRz;

/* renamed from: a.VS */
/* compiled from: a */
public interface C1458VS extends C7024azl {
    public static final C5663aRz ewn = C5640aRc.m17844b(CruiseSpeedAmplifierType.class, "43c372a8b928b0afd43e816032354d61", -1);
    public static final C5663aRz ewo = C5640aRc.m17844b(CruiseSpeedAmplifierType.class, "e35b1b356e502743c81bfc406ba72728", -1);

    /* renamed from: zA */
    public static final C5663aRz f1867zA = C5640aRc.m17844b(CruiseSpeedAmplifierType.class, "fa2538493a59c809f13552f4d5de8222", -1);

    /* renamed from: zB */
    public static final C5663aRz f1868zB = C5640aRc.m17844b(CruiseSpeedAmplifierType.class, "112690eaa6aa0779a1f7f251420cf93d", -1);

    /* renamed from: zC */
    public static final C5663aRz f1869zC = C5640aRc.m17844b(CruiseSpeedAmplifierType.class, "2e5e8407c127f79923ad52859fda599c", -1);

    /* renamed from: zD */
    public static final C5663aRz f1870zD = C5640aRc.m17844b(CruiseSpeedAmplifierType.class, "cb4486be44ee553ee886d62682bad125", -1);
}
