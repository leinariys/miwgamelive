package logic.data.link;

import game.script.space.SpaceCategory;
import logic.res.code.C5640aRc;
import logic.res.code.C5663aRz;

/* renamed from: a.Tm */
/* compiled from: a */
public interface C1358Tm extends C6408amQ {
    public static final C5663aRz avr = C5640aRc.m17844b(SpaceCategory.class, "aab8c078d2f3ecda6ffc682d45f88d90", -1);
    public static final C5663aRz avs = C5640aRc.m17844b(SpaceCategory.class, "074ca0ade414977a7f3924d537b99e29", -1);
    public static final C5663aRz avu = C5640aRc.m17844b(SpaceCategory.class, "2f52a0ca238bc1f2ed09aa8dd9ef41e4", -1);
    public static final C5663aRz bgs = C5640aRc.m17844b(SpaceCategory.class, "19bc996008a99c4b84ff603a3c77afdd", -1);

    /* renamed from: bk */
    public static final C5663aRz f1720bk = C5640aRc.m17844b(SpaceCategory.class, "5e2360d977b81e67459fe7f482ea3565", -1);

    /* renamed from: cI */
    public static final C5663aRz f1721cI = C5640aRc.m17844b(SpaceCategory.class, "849f84e963f13cd4449e6b6253c29f21", -1);

    /* renamed from: vD */
    public static final C5663aRz f1722vD = C5640aRc.m17844b(SpaceCategory.class, "a0b9a5d017d3acc6854c4f16e1356181", -1);

    /* renamed from: vE */
    public static final C5663aRz f1723vE = C5640aRc.m17844b(SpaceCategory.class, "e91f863fedbb076805aab2c8b55aab3b", -1);

    /* renamed from: xf */
    public static final C5663aRz f1724xf = C5640aRc.m17844b(SpaceCategory.class, "c2e0b57f4c6cde4ecd0fda031aa61a6e", -1);
}
