package logic.data.link;

import game.script.itemgen.ItemGenEntry;
import logic.res.code.C5640aRc;
import logic.res.code.C5663aRz;

/* renamed from: a.adm  reason: case insensitive filesystem */
/* compiled from: a */
public interface C5962adm extends C3161oY {

    /* renamed from: bk */
    public static final C5663aRz f4335bk = C5640aRc.m17844b(ItemGenEntry.class, "a8043b98dd016a27186cb1937c42c00d", -1);

    /* renamed from: cI */
    public static final C5663aRz f4336cI = C5640aRc.m17844b(ItemGenEntry.class, "5286b07fa07a9115184b90bb27152aa0", -1);
    public static final C5663aRz fhm = C5640aRc.m17844b(ItemGenEntry.class, "5b530feefca46dde291da74f1bcd9112", -1);
    public static final C5663aRz fhn = C5640aRc.m17844b(ItemGenEntry.class, "e87fd72ea701cd8cfa204638235ecd62", -1);
    public static final C5663aRz fho = C5640aRc.m17844b(ItemGenEntry.class, "641f551a4297e67e0f2b7ba8d8d3f265", -1);

    /* renamed from: ss */
    public static final C5663aRz f4337ss = C5640aRc.m17844b(ItemGenEntry.class, "a4b441b6a72ddce1c29a62513a7a7a84", -1);
}
