package logic.data.link;

import game.script.spacezone.population.BossNestPopulationControl;
import logic.res.code.C5640aRc;
import logic.res.code.C5663aRz;

/* renamed from: a.Ii */
/* compiled from: a */
public interface C0614Ii extends aKW {

    /* renamed from: Tr */
    public static final C5663aRz f728Tr = C5640aRc.m17844b(BossNestPopulationControl.class, "189eb82481b5c7c08e44c8f3830412c7", -1);
    public static final C5663aRz avn = C5640aRc.m17844b(BossNestPopulationControl.class, "82c50538e431e97b97d74825b67a5fb3", -1);
    public static final C5663aRz dep = C5640aRc.m17844b(BossNestPopulationControl.class, "17001b4dc7a60a92d7a4d9b92df13a68", -1);
    public static final C5663aRz deq = C5640aRc.m17844b(BossNestPopulationControl.class, "6aa92e3f570631aea8cc416e510b007d", -1);
    public static final C5663aRz der = C5640aRc.m17844b(BossNestPopulationControl.class, "1c6c040c2cce425d2b2e1530f6eb3929", -1);
    public static final C5663aRz des = C5640aRc.m17844b(BossNestPopulationControl.class, "76aa76e087cb6c60099ab8c89edfe91d", -1);
    public static final C5663aRz det = C5640aRc.m17844b(BossNestPopulationControl.class, "d8797396c02c57546ca7a784bca38f2b", -1);
    public static final C5663aRz deu = C5640aRc.m17844b(BossNestPopulationControl.class, "00b3faa810c49a355bec883e89e72640", -1);
    public static final C5663aRz dev = C5640aRc.m17844b(BossNestPopulationControl.class, "681aaf83bbf604264ef82aabb1f7fb34", -1);
    public static final C5663aRz dew = C5640aRc.m17844b(BossNestPopulationControl.class, "e5474363c4e6142c6dd85f3886e61cec", -1);
    public static final C5663aRz dex = C5640aRc.m17844b(BossNestPopulationControl.class, "62cd1e169d45b2a6971d81d19f886014", -1);
    public static final C5663aRz dey = C5640aRc.m17844b(BossNestPopulationControl.class, "acafca7c86bc99c785b953068577cab9", -1);
    public static final C5663aRz dez = C5640aRc.m17844b(BossNestPopulationControl.class, "9ff1ac21bf6cf8b803683eb22ba3b923", -1);
}
