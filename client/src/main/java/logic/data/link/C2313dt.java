package logic.data.link;

import game.script.item.buff.amplifier.SpeedBoostAmplifierType;
import logic.res.code.C5640aRc;
import logic.res.code.C5663aRz;

/* renamed from: a.dt */
/* compiled from: a */
public interface C2313dt extends C7024azl {

    /* renamed from: zA */
    public static final C5663aRz f6656zA = C5640aRc.m17844b(SpeedBoostAmplifierType.class, "49d4c27484c703a036ef1947ff98de2e", -1);

    /* renamed from: zB */
    public static final C5663aRz f6657zB = C5640aRc.m17844b(SpeedBoostAmplifierType.class, "693ca6dc591e75ba49bcbf875f97ab0b", -1);

    /* renamed from: zC */
    public static final C5663aRz f6658zC = C5640aRc.m17844b(SpeedBoostAmplifierType.class, "997671b0fc9ac7ffac025c57b609afc4", -1);

    /* renamed from: zD */
    public static final C5663aRz f6659zD = C5640aRc.m17844b(SpeedBoostAmplifierType.class, "9df74af1b2a987e9be4559819b16a70a", -1);

    /* renamed from: zz */
    public static final C5663aRz f6660zz = C5640aRc.m17844b(SpeedBoostAmplifierType.class, "506828a5e139329b66ab187d06f86ac9", -1);
}
