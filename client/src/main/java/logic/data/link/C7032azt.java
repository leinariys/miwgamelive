package logic.data.link;

import game.script.spacezone.AsteroidZoneType;
import logic.res.code.C5640aRc;
import logic.res.code.C5663aRz;

/* renamed from: a.azt  reason: case insensitive filesystem */
/* compiled from: a */
public interface C7032azt extends C3833vY {
    public static final C5663aRz bsr = C5640aRc.m17844b(AsteroidZoneType.class, "37ef1669c8b354e171c0e60cf979e740", -1);
    public static final C5663aRz gYT = C5640aRc.m17844b(AsteroidZoneType.class, "8a8ee4d7b08e51622b23881fa61de4df", -1);
    public static final C5663aRz gYU = C5640aRc.m17844b(AsteroidZoneType.class, "a01f94046f592b49f79e54aaed514e03", -1);
    public static final C5663aRz gYV = C5640aRc.m17844b(AsteroidZoneType.class, "c4e1e251c8b7b0fb0dad50a119b21cf3", -1);
    public static final C5663aRz gYW = C5640aRc.m17844b(AsteroidZoneType.class, "b0bf771034b583740303577e96359052", -1);
    public static final C5663aRz gYX = C5640aRc.m17844b(AsteroidZoneType.class, "3d79f0d3d555551d41b597bf7ce5964e", -1);
    public static final C5663aRz gYY = C5640aRc.m17844b(AsteroidZoneType.class, "0f361b499661f13de83d455a59519340", -1);
    public static final C5663aRz gYZ = C5640aRc.m17844b(AsteroidZoneType.class, "8b491e5ecc45e592ce0b943266b131f3", -1);
    public static final C5663aRz gZa = C5640aRc.m17844b(AsteroidZoneType.class, "2af3cbf155f52ad1358d7967b32431b9", -1);
}
