package logic.data.link;

import game.script.player.Player;
import logic.baa.C4062yl;
import logic.res.code.C5640aRc;
import logic.res.code.C5663aRz;
import p001a.C0064Am;

import java.io.Serializable;

/* renamed from: a.Ft */
/* compiled from: a */
public interface C0422Ft extends C6408amQ {
    public static final C5663aRz cVq = C5640aRc.m17844b(Party.class, "6b144f1b9b6e17db4208ee188e73f76e", -1);

    /* renamed from: cg */
    public static final C5663aRz f591cg = C5640aRc.m17844b(Party.class, "6457ff9e721a93beb26b4b62ddf4463b", -1);

    @C0064Am(aul = "b87be59639ea36ac2c7bcd4f72ea4b4f")
    /* renamed from: a.Ft$a */
    public interface C0423a extends C4062yl, Serializable {
        /* renamed from: a */
        void mo2226a(Party gt);
    }

    @C0064Am(aul = "4aaad466b6a3c7b89daf2b24ae07f771")
    /* renamed from: a.Ft$b */
    /* compiled from: a */
    public interface C0424b extends C4062yl, Serializable {
        /* renamed from: b */
        void mo2227b(Party gt, Player aku);
    }

    @C0064Am(aul = "0d2a8b9e925fadb2dd91b26410a5f172")
    /* renamed from: a.Ft$c */
    /* compiled from: a */
    public interface C0425c extends C4062yl, Serializable {
        /* renamed from: a */
        void mo2228a(Party gt, Player aku);
    }
}
