package logic.data.link;

import game.script.mission.scripting.ScriptableMission;
import logic.baa.C4062yl;
import logic.res.code.C5640aRc;
import logic.res.code.C5663aRz;
import p001a.C0064Am;

import java.io.Serializable;

/* renamed from: a.bU */
/* compiled from: a */
public interface C2043bU extends C6988ayb {

    /* renamed from: bh */
    public static final C5663aRz f5802bh = C5640aRc.m17844b(ScriptableMission.class, "90d7d01b2da9060c6ff48fdddb3d90dd", -1);

    /* renamed from: pJ */
    public static final C5663aRz f5803pJ = C5640aRc.m17844b(ScriptableMission.class, "c824d0bf3174abe446bbcc1f63f95249", -1);

    /* renamed from: pK */
    public static final C5663aRz f5804pK = C5640aRc.m17844b(ScriptableMission.class, "862c9a53f226eca350a7b7343df0dae1", -1);

    /* renamed from: pL */
    public static final C5663aRz f5805pL = C5640aRc.m17844b(ScriptableMission.class, "24b2bb569dd5fd90fcb23e8ad522a7c6", -1);

    /* renamed from: pM */
    public static final C5663aRz f5806pM = C5640aRc.m17844b(ScriptableMission.class, "44c3374bdc95f73b89160343885a9024", -1);

    /* renamed from: pN */
    public static final C5663aRz f5807pN = C5640aRc.m17844b(ScriptableMission.class, "5391084ae672014f8e0d139e5a878e61", -1);

    @C0064Am(aul = "82591d633f5fe02cda002e472fca2219")
    /* renamed from: a.bU$a */
    public interface C2044a extends C4062yl, Serializable {
        /* renamed from: c */
        void mo17299c(ScriptableMission gEVar);
    }
}
