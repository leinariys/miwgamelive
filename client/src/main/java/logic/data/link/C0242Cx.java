package logic.data.link;

import game.script.avatar.body.AvatarAppearanceType;
import logic.res.code.C5640aRc;
import logic.res.code.C5663aRz;

/* renamed from: a.Cx */
/* compiled from: a */
public interface C0242Cx extends C1694Yv {
    public static final C5663aRz awI = C5640aRc.m17844b(AvatarAppearanceType.class, "4af52ab39cbe07793b992f8fc93605f2", -1);
    public static final C5663aRz awK = C5640aRc.m17844b(AvatarAppearanceType.class, "e9dd981fef64a6497362e91e486fa4a7", -1);
    public static final C5663aRz cvA = C5640aRc.m17844b(AvatarAppearanceType.class, "5587f366e8a04c27d6dd9ea4e30b769b", -1);
    public static final C5663aRz cvB = C5640aRc.m17844b(AvatarAppearanceType.class, "d132bc688f63676f38578bfbca871278", -1);
    public static final C5663aRz cvC = C5640aRc.m17844b(AvatarAppearanceType.class, "568d9360fbf9a2c5057471ad826de7dc", -1);
    public static final C5663aRz cvD = C5640aRc.m17844b(AvatarAppearanceType.class, "ea1612c823e2ce29cdd59f3b43010e27", -1);
    public static final C5663aRz cvE = C5640aRc.m17844b(AvatarAppearanceType.class, "e61b086e5406c3f5ff2689fe64dd8585", -1);
    public static final C5663aRz cvF = C5640aRc.m17844b(AvatarAppearanceType.class, "aa03b69efe7006d4934b082fa1f945eb", -1);
    public static final C5663aRz cvG = C5640aRc.m17844b(AvatarAppearanceType.class, "36bf10046b9c843df3dffedc2eb2dca8", -1);
    public static final C5663aRz cvH = C5640aRc.m17844b(AvatarAppearanceType.class, "7ee55cd192cb274dfe0e96ecdc5c624b", -1);
    public static final C5663aRz cvI = C5640aRc.m17844b(AvatarAppearanceType.class, "fc9abfc463b423644ee3d731435459d2", -1);
    public static final C5663aRz cvJ = C5640aRc.m17844b(AvatarAppearanceType.class, "0ed883d9de36633c2102567e96c278b6", -1);
    public static final C5663aRz cvK = C5640aRc.m17844b(AvatarAppearanceType.class, "cd15f3fbc8b460d2c7eb347a106f3a7c", -1);
    public static final C5663aRz cvL = C5640aRc.m17844b(AvatarAppearanceType.class, "6f69f908ec612db749a6506c86262dfa", -1);
    public static final C5663aRz cvM = C5640aRc.m17844b(AvatarAppearanceType.class, "69b3bbdecccc07644140a2462f8f3636", -1);
    public static final C5663aRz cvN = C5640aRc.m17844b(AvatarAppearanceType.class, "a9f014ae80105979d77305ff6575d402", -1);
    public static final C5663aRz cvO = C5640aRc.m17844b(AvatarAppearanceType.class, "8e7adca4a8cbba6790df15fbc3c3f4ba", -1);
    public static final C5663aRz cvP = C5640aRc.m17844b(AvatarAppearanceType.class, "8021c3a94c5ec04b8aa00f76502396ca", -1);
    public static final C5663aRz cvQ = C5640aRc.m17844b(AvatarAppearanceType.class, "8d2be29949cea688fabdce7905780a16", -1);
    public static final C5663aRz cvR = C5640aRc.m17844b(AvatarAppearanceType.class, "d5972b02449e119291d21d53418789c3", -1);
    public static final C5663aRz cvz = C5640aRc.m17844b(AvatarAppearanceType.class, "69b015c5d53692f968ba626df6add27b", -1);
}
