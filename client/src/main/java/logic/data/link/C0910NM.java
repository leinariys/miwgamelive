package logic.data.link;

import game.script.ship.OutpostUpgrade;
import logic.res.code.C5640aRc;
import logic.res.code.C5663aRz;

/* renamed from: a.NM */
/* compiled from: a */
public interface C0910NM extends C3161oY {

    /* renamed from: bk */
    public static final C5663aRz f1227bk = C5640aRc.m17844b(OutpostUpgrade.class, "1ca5015d50838fc3f535f8a7d484ab19", -1);

    /* renamed from: cI */
    public static final C5663aRz f1228cI = C5640aRc.m17844b(OutpostUpgrade.class, "7f329854002a45f7b9ff7ba5a5ee9997", -1);
    public static final C5663aRz dHX = C5640aRc.m17844b(OutpostUpgrade.class, "60b679d9dd57a10ea51d4c1bdfa0f780", -1);
    public static final C5663aRz dHY = C5640aRc.m17844b(OutpostUpgrade.class, "35da54b98d9e4c8aa2527acb2acc3de7", -1);
    public static final C5663aRz dHZ = C5640aRc.m17844b(OutpostUpgrade.class, "a3aae3a8929388506c034ebbc6aaf45b", -1);
    public static final C5663aRz dIa = C5640aRc.m17844b(OutpostUpgrade.class, "4b51721b2c3934bf2f8cc345047fe820", -1);

    /* renamed from: eb */
    public static final C5663aRz f1229eb = C5640aRc.m17844b(OutpostUpgrade.class, "cd12be44a0655c4a1f94cbc097075e5b", -1);
}
