package logic.data.link;

import game.script.ship.ShipType;
import logic.res.code.C5640aRc;
import logic.res.code.C5663aRz;

/* renamed from: a.aKA */
/* compiled from: a */
public interface aKA extends C6978axr {

    /* renamed from: Fh */
    public static final C5663aRz f3240Fh = C5640aRc.m17844b(ShipType.class, "1eda3e3b844b14dbf9ab6ab2a782bd81", -1);
    public static final C5663aRz aMH = C5640aRc.m17844b(ShipType.class, "5c65ff8571e154539e3a702dafa5d3f5", -1);
    public static final C5663aRz avr = C5640aRc.m17844b(ShipType.class, "5155673a67c1317e271851e80810b375", -1);
    public static final C5663aRz avs = C5640aRc.m17844b(ShipType.class, "822a91b5a407dbe9eef26ac76e93897a", -1);
    public static final C5663aRz avu = C5640aRc.m17844b(ShipType.class, "ce96933a3a33a11f2c6b22de7916e496", -1);
    public static final C5663aRz dVh = C5640aRc.m17844b(ShipType.class, "479e144d222542d4cd699833a1703d41", -1);
    public static final C5663aRz dVi = C5640aRc.m17844b(ShipType.class, "21d17085c2c9311a571da84fb593abef", -1);
    public static final C5663aRz dVj = C5640aRc.m17844b(ShipType.class, "3a169b6baacadee5e0e9322d4ff95243", -1);
    public static final C5663aRz dVk = C5640aRc.m17844b(ShipType.class, "b679ff8b5a15921b24cc5c2738dfbe15", -1);
    public static final C5663aRz dVl = C5640aRc.m17844b(ShipType.class, "3e3393327511358344ff4ca97d520acd", -1);
    public static final C5663aRz gMz = C5640aRc.m17844b(ShipType.class, "bfbbfbce0e341f232b6de4f7657b1299", -1);
    public static final C5663aRz hUQ = C5640aRc.m17844b(ShipType.class, "48c4930db707d046cfbac8456038dd59", -1);
    public static final C5663aRz hUR = C5640aRc.m17844b(ShipType.class, "a5c1b391d6327892172ac23fa23753bd", -1);
    public static final C5663aRz hUS = C5640aRc.m17844b(ShipType.class, "f0e011f6e0ed047cea6f241a8fd4c6d1", -1);
    public static final C5663aRz iiA = C5640aRc.m17844b(ShipType.class, "ed3ef6b6fcd6db854fc4dd4a1ee71e2e", -1);
    public static final C5663aRz iiB = C5640aRc.m17844b(ShipType.class, "894e1aa092c042f297543fda0d831526", -1);
    public static final C5663aRz iiC = C5640aRc.m17844b(ShipType.class, "02b94c71488473c83f12e123414932aa", -1);
    public static final C5663aRz iiD = C5640aRc.m17844b(ShipType.class, "dfb56f2dcfa4d13a8a9054e290ca2a09", -1);
    public static final C5663aRz iiE = C5640aRc.m17844b(ShipType.class, "460eae22844db2a80e0c9469d376aab4", -1);
    public static final C5663aRz iiF = C5640aRc.m17844b(ShipType.class, "6fb84320de6368554f4e2fb4852f05e7", -1);
    public static final C5663aRz iiG = C5640aRc.m17844b(ShipType.class, "088ddaf62e04789d9edae9c46d989697", -1);
    public static final C5663aRz iiH = C5640aRc.m17844b(ShipType.class, "0c8577a46f1b6f59c4a78d7c0c1a0e39", -1);
    public static final C5663aRz iiI = C5640aRc.m17844b(ShipType.class, "ebcd7769b9ffca827e3a466e0816f1d3", -1);
    public static final C5663aRz iiJ = C5640aRc.m17844b(ShipType.class, "291267db6c5b29dcfeb359bb9e9f3d7e", -1);
    public static final C5663aRz iiK = C5640aRc.m17844b(ShipType.class, "08fa3e70d05931101c4b7051155c09fd", -1);
    public static final C5663aRz iiL = C5640aRc.m17844b(ShipType.class, "cbdf91aa85cc3f67bdb5002b1d23df58", -1);
    public static final C5663aRz iiu = C5640aRc.m17844b(ShipType.class, "368a3fbcb4e3b0782c1ea05057249238", -1);
    public static final C5663aRz iiv = C5640aRc.m17844b(ShipType.class, "1ca50fd7afda7262fb75a522858bd914", -1);
    public static final C5663aRz iiw = C5640aRc.m17844b(ShipType.class, "204b98e6b0b6c691367c4125a2ca5d69", -1);
    public static final C5663aRz iix = C5640aRc.m17844b(ShipType.class, "b9bbe135cc895ad63f2776a9b4fec021", -1);
    public static final C5663aRz iiy = C5640aRc.m17844b(ShipType.class, "6f09886ab27ed6def65f2ab7a12ab86a", -1);
    public static final C5663aRz iiz = C5640aRc.m17844b(ShipType.class, "928a4fe010233b298b1e0e97f9ef6917", -1);

    /* renamed from: rb */
    public static final C5663aRz f3241rb = C5640aRc.m17844b(ShipType.class, "73b2db6e5bb6d3f94327d759092d198d", -1);

    /* renamed from: rd */
    public static final C5663aRz f3242rd = C5640aRc.m17844b(ShipType.class, "62182e99d40679055bfdd3c6c3b1a88e", -1);

    /* renamed from: zE */
    public static final C5663aRz f3243zE = C5640aRc.m17844b(ShipType.class, "1a0c775e0f24e9208410e158f2733eac", -1);
}
