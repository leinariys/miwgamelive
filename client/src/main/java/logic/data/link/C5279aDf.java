package logic.data.link;

import game.script.ship.hazardshield.HazardShieldType;
import logic.res.code.C5640aRc;
import logic.res.code.C5663aRz;

/* renamed from: a.aDf  reason: case insensitive filesystem */
/* compiled from: a */
public interface C5279aDf extends C0604IY {
    public static final C5663aRz bit = C5640aRc.m17844b(HazardShieldType.class, "fb14eaefa814fbebc3be0cb3edb43828", -1);
    public static final C5663aRz biu = C5640aRc.m17844b(HazardShieldType.class, "3210178b90c2c5a32e7fc72acb807547", -1);
    public static final C5663aRz ewn = C5640aRc.m17844b(HazardShieldType.class, "f6b6a8ef3f822342644e2432b9f83038", -1);
    public static final C5663aRz ewo = C5640aRc.m17844b(HazardShieldType.class, "756c430d1b7d44aaadc5c7b45ada849f", -1);
    public static final C5663aRz hxT = C5640aRc.m17844b(HazardShieldType.class, "0bd84fcaa6e27fff66e26a498a301b74", -1);
    public static final C5663aRz hxU = C5640aRc.m17844b(HazardShieldType.class, "443026e338dc1b3e39c53c3a0fa9355d", -1);
}
