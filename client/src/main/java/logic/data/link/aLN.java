package logic.data.link;

import game.script.ai.npc.FollowerController;
import logic.res.code.C5640aRc;
import logic.res.code.C5663aRz;

/* renamed from: a.aLN */
/* compiled from: a */
public interface aLN extends C0931Nf {
    public static final C5663aRz dQD = C5640aRc.m17844b(FollowerController.class, "b0bfbbba44af41c41aedee8790700284", -1);
    public static final C5663aRz hWn = C5640aRc.m17844b(FollowerController.class, "8cb0f5e4d45fcd74dde2655e9502ee9c", -1);
    public static final C5663aRz inv = C5640aRc.m17844b(FollowerController.class, "57907b60d0141b2ca56f5c4c701f3a67", -1);
    public static final C5663aRz inw = C5640aRc.m17844b(FollowerController.class, "2d23e2b5cd8362a8dd41b1159adde8b1", -1);
    public static final C5663aRz inx = C5640aRc.m17844b(FollowerController.class, "d00a6819f31f87accc091e5a954bffc3", -1);
    public static final C5663aRz iny = C5640aRc.m17844b(FollowerController.class, "5d0e2f3185b9a9800886a9021daac041", -1);
    public static final C5663aRz inz = C5640aRc.m17844b(FollowerController.class, "0fb85f07fb0107b3e0891a08eb28b751", -1);
}
