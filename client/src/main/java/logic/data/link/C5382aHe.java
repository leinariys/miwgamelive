package logic.data.link;

import game.script.missile.MissileType;
import logic.res.code.C5640aRc;
import logic.res.code.C5663aRz;

/* renamed from: a.aHe  reason: case insensitive filesystem */
/* compiled from: a */
public interface C5382aHe extends azI {

    /* renamed from: OI */
    public static final C5663aRz f2994OI = C5640aRc.m17844b(MissileType.class, "f03f6d4767429c76c8f3239fd4b3a5ac", -1);
    public static final C5663aRz aMH = C5640aRc.m17844b(MissileType.class, "9799d9e319f41e8f250c5773ab267329", -1);
    public static final C5663aRz dVi = C5640aRc.m17844b(MissileType.class, "4f8c3f5e7323b2d6aae1ba16619a3881", -1);
    public static final C5663aRz dVj = C5640aRc.m17844b(MissileType.class, "69dda8d7cb9c32365e5927f99c7956ad", -1);
    public static final C5663aRz gMz = C5640aRc.m17844b(MissileType.class, "ff0c2daf3aefe7bbce78583612342d7f", -1);
    public static final C5663aRz hHS = C5640aRc.m17844b(MissileType.class, "dab466e2c4c41e54388b1e5930ee5446", -1);
    public static final C5663aRz hUQ = C5640aRc.m17844b(MissileType.class, "726dffb2106d93f0fce2c59378fd4566", -1);
    public static final C5663aRz hUR = C5640aRc.m17844b(MissileType.class, "d159e8ef9426ab6f8a872fa0b8eb0f03", -1);
    public static final C5663aRz hUS = C5640aRc.m17844b(MissileType.class, "beef6fa59d3e344243bf03b6890b05f2", -1);
    public static final C5663aRz hUT = C5640aRc.m17844b(MissileType.class, "a6c56c8864cefc6386a1dda44a1e2c9c", -1);
}
