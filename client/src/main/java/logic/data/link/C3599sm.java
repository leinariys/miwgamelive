package logic.data.link;

import game.script.npc.NPCType;
import logic.res.code.C5640aRc;
import logic.res.code.C5663aRz;

/* renamed from: a.sm */
/* compiled from: a */
public interface C3599sm extends C5826abG {

    /* renamed from: TS */
    public static final C5663aRz f9151TS = C5640aRc.m17844b(NPCType.class, "eb21c37020ac0b54a414a32546de14c5", -1);
    public static final C5663aRz anV = C5640aRc.m17844b(NPCType.class, "cad09a4b9ad0fa4d616b2ce960d70db5", -1);
    public static final C5663aRz avr = C5640aRc.m17844b(NPCType.class, "cdc819695afb974eabef82a6717cf44f", -1);
    public static final C5663aRz avs = C5640aRc.m17844b(NPCType.class, "22b3edcefdb4cd51406cc13532431a04", -1);
    public static final C5663aRz avu = C5640aRc.m17844b(NPCType.class, "4fdf297d96bf73c60e9e11faa96d667c", -1);
    public static final C5663aRz bgi = C5640aRc.m17844b(NPCType.class, "6b785477f4df208c5592dbd1676115a8", -1);
    public static final C5663aRz bgj = C5640aRc.m17844b(NPCType.class, "49c602bf2b1b4d1a1637849cd91eacfd", -1);
    public static final C5663aRz bgk = C5640aRc.m17844b(NPCType.class, "0c54c8fa32147d1fab3189c5974676f5", -1);
    public static final C5663aRz bgl = C5640aRc.m17844b(NPCType.class, "e64ac19c66c82ade4fae5cc1ae8e0262", -1);
    public static final C5663aRz bgm = C5640aRc.m17844b(NPCType.class, "56fbba9e19d329bdbcd902b4a1a32486", -1);
    public static final C5663aRz bgn = C5640aRc.m17844b(NPCType.class, "91733e44e95579a982326895d992f2ea", -1);
    public static final C5663aRz bgo = C5640aRc.m17844b(NPCType.class, "f67a1de235b3e04d7a3e8517548191a2", -1);
    public static final C5663aRz bgp = C5640aRc.m17844b(NPCType.class, "8383be213fc90fde6aedba590f5689b1", -1);
    public static final C5663aRz bgq = C5640aRc.m17844b(NPCType.class, "c6bb25f8b93f247630ba607c7687d358", -1);
    public static final C5663aRz bgr = C5640aRc.m17844b(NPCType.class, "996184ac8f9800bba1767379bf8ca31a", -1);
    public static final C5663aRz bgs = C5640aRc.m17844b(NPCType.class, "13b365c30fddeae5241d4086305af676", -1);
    public static final C5663aRz bgt = C5640aRc.m17844b(NPCType.class, "6d20861cd232ab8c38ee3e5bd8dd5a02", -1);

    /* renamed from: qf */
    public static final C5663aRz f9152qf = C5640aRc.m17844b(NPCType.class, "2e9e5dfdb053a4bd6ce3596cfb1d2b57", -1);

    /* renamed from: vE */
    public static final C5663aRz f9153vE = C5640aRc.m17844b(NPCType.class, "96559a84d548bea4bfb8e0ad02298690", -1);
}
