package logic.data.link;

import game.script.progression.MPMultiplier;
import logic.res.code.C5640aRc;
import logic.res.code.C5663aRz;

/* renamed from: a.cQ */
/* compiled from: a */
public interface C2171cQ extends C6408amQ {

    /* renamed from: bk */
    public static final C5663aRz f6121bk = C5640aRc.m17844b(MPMultiplier.class, "fcce95cdcad34bc2376ae6160e29f8cd", -1);

    /* renamed from: cI */
    public static final C5663aRz f6122cI = C5640aRc.m17844b(MPMultiplier.class, "dd28f82c249eec40e529fa86496abfa6", -1);

    /* renamed from: vE */
    public static final C5663aRz f6123vE = C5640aRc.m17844b(MPMultiplier.class, "cb7ff7537c6f7c9d951cbbcc78a6bf22", -1);

    /* renamed from: xo */
    public static final C5663aRz f6124xo = C5640aRc.m17844b(MPMultiplier.class, "28017181c1df31b18bf64d4ab0a06ee3", -1);

    /* renamed from: xp */
    public static final C5663aRz f6125xp = C5640aRc.m17844b(MPMultiplier.class, "421590aa1f7735c3c93862fe25ff421c", -1);

    /* renamed from: xq */
    public static final C5663aRz f6126xq = C5640aRc.m17844b(MPMultiplier.class, "4f8486b3199209e09c4e4000616defed", -1);

    /* renamed from: xr */
    public static final C5663aRz f6127xr = C5640aRc.m17844b(MPMultiplier.class, "ce94bc728293e88628a8b7d521bf8aea", -1);

    /* renamed from: xs */
    public static final C5663aRz f6128xs = C5640aRc.m17844b(MPMultiplier.class, "c7777c1c2e0f2f4ecb023c981c3581d9", -1);

    /* renamed from: xt */
    public static final C5663aRz f6129xt = C5640aRc.m17844b(MPMultiplier.class, "94a18e92d688a4683bd70be278e6544e", -1);

    /* renamed from: xu */
    public static final C5663aRz f6130xu = C5640aRc.m17844b(MPMultiplier.class, "33f498a071f08c848e9444a2b329cb57", -1);
}
