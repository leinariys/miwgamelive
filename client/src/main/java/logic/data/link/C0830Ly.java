package logic.data.link;

import game.script.faction.Faction;
import logic.res.code.C5640aRc;
import logic.res.code.C5663aRz;

/* renamed from: a.Ly */
/* compiled from: a */
public interface C0830Ly extends C6408amQ {
    public static final C5663aRz anV = C5640aRc.m17844b(Faction.class, "01373be1e563edf6cd2fd417e133add1", -1);
    public static final C5663aRz avr = C5640aRc.m17844b(Faction.class, "c183c661fbd2daf29d37d3de9c5f7d0a", -1);
    public static final C5663aRz avs = C5640aRc.m17844b(Faction.class, "78365578377015d444860deb33bdeddd", -1);
    public static final C5663aRz avu = C5640aRc.m17844b(Faction.class, "0b8a94e8468294381dae89fbfa8e5eac", -1);
    public static final C5663aRz bgs = C5640aRc.m17844b(Faction.class, "16d32119c8015dc0de404e7fc61981b7", -1);

    /* renamed from: bk */
    public static final C5663aRz f1062bk = C5640aRc.m17844b(Faction.class, "641909968f3566016ee5a8b24d5a117d", -1);

    /* renamed from: cI */
    public static final C5663aRz f1063cI = C5640aRc.m17844b(Faction.class, "6fc936bab74200fedbfa5b0597a160e9", -1);
    public static final C5663aRz dvn = C5640aRc.m17844b(Faction.class, "df0038e8813855fa3fcfc403b9edf07a", -1);
    public static final C5663aRz dvo = C5640aRc.m17844b(Faction.class, "f09589e5e64ce2ab3afda5418098214e", -1);
    public static final C5663aRz dvp = C5640aRc.m17844b(Faction.class, "e3e85a3b65ef49f20ac3e2ec85d19a0b", -1);
    public static final C5663aRz dvq = C5640aRc.m17844b(Faction.class, "6377ff610424fdc253485ec6b10849fd", -1);
    public static final C5663aRz dvr = C5640aRc.m17844b(Faction.class, "a8b7d9363596623f2c780c55ca1276b3", -1);

    /* renamed from: vE */
    public static final C5663aRz f1064vE = C5640aRc.m17844b(Faction.class, "87be180810a5f6bd6248a128a908ce1d", -1);

    /* renamed from: xf */
    public static final C5663aRz f1065xf = C5640aRc.m17844b(Faction.class, "3b5260d03c510100872172ff4e01bf6d", -1);
}
