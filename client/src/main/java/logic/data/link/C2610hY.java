package logic.data.link;

import game.script.item.Amplifier;
import logic.res.code.C5640aRc;
import logic.res.code.C5663aRz;

/* renamed from: a.hY */
/* compiled from: a */
public interface C2610hY extends C6497aoB {

    /* renamed from: WD */
    public static final C5663aRz f7956WD = C5640aRc.m17844b(Amplifier.class, "dde47cd6bdcaebc69048ca274e534a9f", -1);

    /* renamed from: WE */
    public static final C5663aRz f7957WE = C5640aRc.m17844b(Amplifier.class, "fbf45096f5852ecb0e4b899ee20ddf9a", -1);

    /* renamed from: bm */
    public static final C5663aRz f7958bm = C5640aRc.m17844b(Amplifier.class, "296a0761c4df0d0066d7a4046c912f4b", -1);
}
