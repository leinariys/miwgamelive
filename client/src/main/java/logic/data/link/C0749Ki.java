package logic.data.link;

import game.script.item.buff.amplifier.WeaponAmplifierType;
import logic.res.code.C5640aRc;
import logic.res.code.C5663aRz;

/* renamed from: a.Ki */
/* compiled from: a */
public interface C0749Ki extends C7024azl {
    public static final C5663aRz anR = C5640aRc.m17844b(WeaponAmplifierType.class, "ce3daddbe423a09e18614de24420bc31", -1);
    public static final C5663aRz bKw = C5640aRc.m17844b(WeaponAmplifierType.class, "b51de4f5fa3fae70520caa69561c3936", -1);
    public static final C5663aRz ckt = C5640aRc.m17844b(WeaponAmplifierType.class, "11289e64e55cbaf87a4e24730b506272", -1);
    public static final C5663aRz clj = C5640aRc.m17844b(WeaponAmplifierType.class, "bf1ceb14f7d57ff6f24f94b7495cf82c", -1);
    public static final C5663aRz clk = C5640aRc.m17844b(WeaponAmplifierType.class, "405d29460bb158ff3967216f6c0c5e20", -1);
    public static final C5663aRz dpu = C5640aRc.m17844b(WeaponAmplifierType.class, "76aab741fad8a5e7becb565cad646949", -1);
    public static final C5663aRz dpv = C5640aRc.m17844b(WeaponAmplifierType.class, "db5737890e3f1a0d5135ab5c975d758f", -1);
    public static final C5663aRz dpw = C5640aRc.m17844b(WeaponAmplifierType.class, "cd217918f511282ad2b9d6c32e9e0854", -1);
}
