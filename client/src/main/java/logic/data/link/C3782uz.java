package logic.data.link;

import game.script.space.StellarSystem;
import logic.res.code.C5640aRc;
import logic.res.code.C5663aRz;

/* renamed from: a.uz */
/* compiled from: a */
public interface C3782uz extends C6408amQ {
    public static final C5663aRz anV = C5640aRc.m17844b(StellarSystem.class, "cf0653ea8764e7e7daa5bc49454b1d76", -1);
    public static final C5663aRz avr = C5640aRc.m17844b(StellarSystem.class, "6567c00e58a87d4c27abae20c5438c47", -1);
    public static final C5663aRz avs = C5640aRc.m17844b(StellarSystem.class, "c1b3de45147a9764f07138f9e02bf550", -1);
    public static final C5663aRz avu = C5640aRc.m17844b(StellarSystem.class, "d116df0429b121c3465bf41e174a1c0e", -1);
    public static final C5663aRz bbD = C5640aRc.m17844b(StellarSystem.class, "f6a359289d0cfef6958f30d326386526", -1);
    public static final C5663aRz bgs = C5640aRc.m17844b(StellarSystem.class, "6f2d82f4152f7e69c7282fdf04048aac", -1);

    /* renamed from: bk */
    public static final C5663aRz f9379bk = C5640aRc.m17844b(StellarSystem.class, "3b021bb7d61c4c6d886b8fa9448a43c1", -1);
    public static final C5663aRz bwb = C5640aRc.m17844b(StellarSystem.class, "6185197333bcd516dd9a325e05f1fabe", -1);
    public static final C5663aRz bwc = C5640aRc.m17844b(StellarSystem.class, "1ae4ad5225aefe91cb50aeacaba4bfc4", -1);

    /* renamed from: cI */
    public static final C5663aRz f9380cI = C5640aRc.m17844b(StellarSystem.class, "4d0a7d05dbfdd4329aae562c57f8777f", -1);

    /* renamed from: vE */
    public static final C5663aRz f9381vE = C5640aRc.m17844b(StellarSystem.class, "0308192860363d23896020a9e6622160", -1);

    /* renamed from: xf */
    public static final C5663aRz f9382xf = C5640aRc.m17844b(StellarSystem.class, "9b72275a2aa520fbb9a9cf37ec811b52", -1);
}
