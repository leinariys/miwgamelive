package logic.data.link;

import game.script.mission.MissionObjective;
import logic.res.code.C5640aRc;
import logic.res.code.C5663aRz;

/* renamed from: a.rc */
/* compiled from: a */
public interface C3440rc extends C3161oY {
    public static final C5663aRz aZA = C5640aRc.m17844b(MissionObjective.class, "9115da31458e82a71fb33abc06a40681", -1);
    public static final C5663aRz aZB = C5640aRc.m17844b(MissionObjective.class, "34b2a844c230d7810f2ca211c9addf19", -1);
    public static final C5663aRz aZz = C5640aRc.m17844b(MissionObjective.class, "0cb5b1c207ffc72941a7ddd092454070", -1);

    /* renamed from: bk */
    public static final C5663aRz f9054bk = C5640aRc.m17844b(MissionObjective.class, "10419ca5e02bd57d7b6d58bb4ee9dd0b", -1);

    /* renamed from: qd */
    public static final C5663aRz f9055qd = C5640aRc.m17844b(MissionObjective.class, "ac94a1405bbea8373f72d5d5bba76f30", -1);

    /* renamed from: vE */
    public static final C5663aRz f9056vE = C5640aRc.m17844b(MissionObjective.class, "137dd39425db32f0b17947b81c33a826", -1);
}
