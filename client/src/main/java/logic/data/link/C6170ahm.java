package logic.data.link;

import game.script.nls.NLSParty;
import logic.res.code.C5640aRc;
import logic.res.code.C5663aRz;

/* renamed from: a.ahm  reason: case insensitive filesystem */
/* compiled from: a */
public interface C6170ahm extends C5303aEd {
    public static final C5663aRz aUP = C5640aRc.m17844b(NLSParty.class, "9837aaa3fc6515a1cae47c9e08fef64c", -1);
    public static final C5663aRz aUQ = C5640aRc.m17844b(NLSParty.class, "cbbfd63c739ea63c5ec045e7b66bec6c", -1);
    public static final C5663aRz fID = C5640aRc.m17844b(NLSParty.class, "c5e04b67e701b4764c41683d3b0765cb", -1);
    public static final C5663aRz fIE = C5640aRc.m17844b(NLSParty.class, "2766a43fc39f366628a63d4a3377af29", -1);
    public static final C5663aRz fIF = C5640aRc.m17844b(NLSParty.class, "e5323f3e617d92941aa13ccfc0f2955a", -1);
    public static final C5663aRz fIG = C5640aRc.m17844b(NLSParty.class, "a4b7984a9e89e98bb557088dee92abec", -1);
    public static final C5663aRz fIH = C5640aRc.m17844b(NLSParty.class, "0912ee2d979e603ce1888653af6ac1b2", -1);
    public static final C5663aRz fII = C5640aRc.m17844b(NLSParty.class, "f1f29026db958ae5d65aba61f8a326e8", -1);
    public static final C5663aRz fIJ = C5640aRc.m17844b(NLSParty.class, "7ba2101266c8f3c89c70e48d2d350827", -1);
    public static final C5663aRz fIK = C5640aRc.m17844b(NLSParty.class, "9e9d534007b04fb07f96adfd9cfd014f", -1);
    public static final C5663aRz fIL = C5640aRc.m17844b(NLSParty.class, "6c0b72e54e371aa55af8db9ec1013512", -1);
    public static final C5663aRz fIM = C5640aRc.m17844b(NLSParty.class, "bcedf9dcce4d79b8c429711fcbe5deff", -1);
    public static final C5663aRz fIN = C5640aRc.m17844b(NLSParty.class, "d89fc7fda636317b5bdca44cab1c966a", -1);
    public static final C5663aRz fIO = C5640aRc.m17844b(NLSParty.class, "6916656931f4329e3a1a2e88be5e1f2b", -1);
    public static final C5663aRz fIP = C5640aRc.m17844b(NLSParty.class, "6a4797922b91063f90a49344e2b29d07", -1);
    public static final C5663aRz fIQ = C5640aRc.m17844b(NLSParty.class, "e3a02f0c5600fc6d67d6d84682896b8b", -1);
    public static final C5663aRz fIR = C5640aRc.m17844b(NLSParty.class, "53e02537cd9cc3691aafea010c000204", -1);
}
