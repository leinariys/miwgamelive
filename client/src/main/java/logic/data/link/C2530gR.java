package logic.data.link;

import game.script.Actor;
import game.script.ship.Hull;
import logic.baa.C4062yl;
import logic.res.code.C5640aRc;
import logic.res.code.C5663aRz;
import p001a.C0064Am;
import p001a.C5260aCm;

import java.io.Serializable;

/* renamed from: a.gR */
/* compiled from: a */
public interface C2530gR extends C6408amQ {
    public static final C5663aRz aQv = C5640aRc.m17844b(Hull.class, "b9294ace7cc3d0badeaee4070f91a436", -1);
    public static final C5663aRz agL = C5640aRc.m17844b(Hull.class, "5b1be121c46f0a0e1b56d0ee66a068d5", -1);
    public static final C5663aRz clX = C5640aRc.m17844b(Hull.class, "d36a5e54dcc50c1b2e3ae4dcaa8b4587", -1);
    public static final C5663aRz fLO = C5640aRc.m17844b(Hull.class, "69fee834371829196a17321b92abe328", -1);
    public static final C5663aRz fLP = C5640aRc.m17844b(Hull.class, "674c314385d57bed677ef90b3d1719eb", -1);

    @C0064Am(aul = "b30ba72f9c1e4e4fa968eba14a7086ff")
    /* renamed from: a.gR$a */
    public interface C2531a extends C4062yl, Serializable {
        /* renamed from: a */
        void mo8536a(Hull jRVar, C5260aCm acm, Actor cr);
    }
}
