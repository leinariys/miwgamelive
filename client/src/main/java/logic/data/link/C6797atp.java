package logic.data.link;

import game.script.item.WeaponType;
import logic.res.code.C5640aRc;
import logic.res.code.C5663aRz;

/* renamed from: a.atp  reason: case insensitive filesystem */
/* compiled from: a */
public interface C6797atp extends C0769Kz {
    public static final C5663aRz bKw = C5640aRc.m17844b(WeaponType.class, "ec7c9f1ac22ce45968d64c868a62071d", -1);
    public static final C5663aRz ckr = C5640aRc.m17844b(WeaponType.class, "0c8c747a62c1e716b0d33b84c6edf3a3", -1);
    public static final C5663aRz ckt = C5640aRc.m17844b(WeaponType.class, "13d5837e1fadef8c2806b67e14cd5046", -1);
    public static final C5663aRz dpu = C5640aRc.m17844b(WeaponType.class, "bb756382bfd39997268aaf69f4df98e4", -1);
    public static final C5663aRz dpv = C5640aRc.m17844b(WeaponType.class, "ffbb079176806b263834c27c54a95030", -1);
    public static final C5663aRz gzp = C5640aRc.m17844b(WeaponType.class, "cccee2ab5e319f1f992d6f775a1f0a41", -1);
    public static final C5663aRz gzq = C5640aRc.m17844b(WeaponType.class, "96d742392c453ec60cce2ded7d01cff8", -1);
    public static final C5663aRz gzr = C5640aRc.m17844b(WeaponType.class, "cfc88d17fa9f7fe54e6b2c45da5b8763", -1);
    public static final C5663aRz gzs = C5640aRc.m17844b(WeaponType.class, "4403966a748eced21ae657296d6c8a1d", -1);
    public static final C5663aRz gzt = C5640aRc.m17844b(WeaponType.class, "ef97165a79b65a84c0704955d4104db3", -1);
    public static final C5663aRz gzu = C5640aRc.m17844b(WeaponType.class, "8e2844cc72025f55abd9b215cd43ea60", -1);
    public static final C5663aRz gzv = C5640aRc.m17844b(WeaponType.class, "6ac911c8b43db88be1727e540349898d", -1);
    public static final C5663aRz gzw = C5640aRc.m17844b(WeaponType.class, "d81f212b089c7b8ac14762b1f4587fa9", -1);
    public static final C5663aRz gzx = C5640aRc.m17844b(WeaponType.class, "209e0e46cfbe1a01239785dc9908ee46", -1);
}
