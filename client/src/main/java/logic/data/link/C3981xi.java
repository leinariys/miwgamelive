package logic.data.link;

import game.script.Actor;
import game.script.item.Component;
import game.script.ship.Ship;
import logic.baa.C4062yl;
import logic.res.code.C5640aRc;
import logic.res.code.C5663aRz;
import p001a.C0064Am;

import java.io.Serializable;

/* renamed from: a.xi */
/* compiled from: a */
public interface C3981xi extends C0471GX {
    public static final C5663aRz aMH = C5640aRc.m17844b(Ship.class, "ce62fd24fad44a0c6c2bd67edb82fd22", -1);
    public static final C5663aRz aMJ = C5640aRc.m17844b(Ship.class, "4f6ed4c9369bb645b8d4d8fb201084d2", -1);
    public static final C5663aRz agL = C5640aRc.m17844b(Ship.class, "15f0692836c7328dd4fd758c7ac6fb58", -1);
    public static final C5663aRz bKz = C5640aRc.m17844b(Ship.class, "67a214c3f2bb3da8ce4cd82c57fa1d32", -1);
    public static final String bgb = "b81f2b0b73838e82b8bf1f30b4fc2f96";
    public static final C5663aRz etA = C5640aRc.m17844b(Ship.class, "95f9f1daff345868b5c0f0a6e0e9ce34", -1);
    public static final C5663aRz etB = C5640aRc.m17844b(Ship.class, "2283a964f9c6533a51ded6e38d40a0e4", -1);
    public static final C5663aRz etC = C5640aRc.m17844b(Ship.class, "a36555ac5f2f582ce9600a99ade722ea", -1);
    public static final C5663aRz etD = C5640aRc.m17844b(Ship.class, "04de02b87e1ef104769337bf29e98626", -1);
    public static final C5663aRz etE = C5640aRc.m17844b(Ship.class, "2f73c026196fec0b941468ccfd34fa6b", -1);
    public static final C5663aRz etp = C5640aRc.m17844b(Ship.class, "8559899c23517ad1c07a27c42c27859c", -1);
    public static final C5663aRz etq = C5640aRc.m17844b(Ship.class, "6307cbd895f869dbf6b3c6c2941678f1", -1);
    public static final C5663aRz etr = C5640aRc.m17844b(Ship.class, "c79cf2d18597b17dafdce6345a115078", -1);
    public static final C5663aRz ets = C5640aRc.m17844b(Ship.class, "b6f3e2426c4db92cb5c518db4dde10f4", -1);
    public static final C5663aRz ett = C5640aRc.m17844b(Ship.class, "ff062fb3e7385f02b8dc783cc72aa5dd", -1);
    public static final C5663aRz etu = C5640aRc.m17844b(Ship.class, "f94501d21485a328fc44f085e8f959ce", -1);
    public static final C5663aRz etv = C5640aRc.m17844b(Ship.class, "5eaa252d59d9bdd4ae9bdf5049e6aebd", -1);
    public static final C5663aRz etw = C5640aRc.m17844b(Ship.class, "f7973441f80aa15f5856ac20a967e2e2", -1);
    public static final C5663aRz etx = C5640aRc.m17844b(Ship.class, "7729e8215e41642c9f9c1cf2db254825", -1);
    public static final C5663aRz ety = C5640aRc.m17844b(Ship.class, "f9350cce5941de23e828745965eb122e", -1);
    public static final C5663aRz etz = C5640aRc.m17844b(Ship.class, "955302f0b2669d8f3fc357b44eb4d71b", -1);

    /* renamed from: qd */
    public static final C5663aRz f9560qd = C5640aRc.m17844b(Ship.class, "2d1b3508f6de8b3fb6bc8b7f77f1bc82", -1);

    @C0064Am(aul = "34989062d48bbb4773dd25db8c41ead7")
    /* renamed from: a.xi$a */
    public interface C3982a extends C4062yl, Serializable {
        /* renamed from: b */
        void mo7563b(Ship fAVar, Actor cr);
    }

    @C0064Am(aul = "0b2594f3bc5acba031e769203ca41dc4")
    /* renamed from: a.xi$b */
    /* compiled from: a */
    public interface C3983b extends C4062yl, Serializable {
        /* renamed from: b */
        void mo3926b(Ship fAVar, Component abl);
    }

    @C0064Am(aul = "605164dad796f14c92f162af56da1335")
    /* renamed from: a.xi$c */
    /* compiled from: a */
    public interface C3984c extends C4062yl, Serializable {
        /* renamed from: d */
        void mo3933d(Ship fAVar, Component abl);
    }
}
