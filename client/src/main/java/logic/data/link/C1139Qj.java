package logic.data.link;

import game.script.space.AsteroidType;
import logic.res.code.C5640aRc;
import logic.res.code.C5663aRz;

/* renamed from: a.Qj */
/* compiled from: a */
public interface C1139Qj extends C1732Zb {
    public static final C5663aRz aMH = C5640aRc.m17844b(AsteroidType.class, "914f8c7ac5f00bbde3fda4f2d6eb6a6f", -1);
    public static final C5663aRz anV = C5640aRc.m17844b(AsteroidType.class, "c808e77f1ae8def9baaaa5e9775ad919", -1);
    public static final C5663aRz bgr = C5640aRc.m17844b(AsteroidType.class, "441efaf17b25894a2f45a7d5015dc694", -1);
    public static final C5663aRz dVh = C5640aRc.m17844b(AsteroidType.class, "fda858dfb99f6fc724fa2c37cb8028ac", -1);
    public static final C5663aRz dVi = C5640aRc.m17844b(AsteroidType.class, "c379172a9f6945e852e2548776a7bec4", -1);
    public static final C5663aRz dVj = C5640aRc.m17844b(AsteroidType.class, "d40bc9383dda8e6a43c80e0d2fa443ed", -1);
    public static final C5663aRz dVk = C5640aRc.m17844b(AsteroidType.class, "c8e5ddc60dbe8f48ae4a8a7f30c644b8", -1);
    public static final C5663aRz dVl = C5640aRc.m17844b(AsteroidType.class, "a30f7cc66b3106cb5837e081cbb28b43", -1);

    /* renamed from: rb */
    public static final C5663aRz f1453rb = C5640aRc.m17844b(AsteroidType.class, "d7624973e208c02dc7bb51cd5177d30d", -1);

    /* renamed from: rd */
    public static final C5663aRz f1454rd = C5640aRc.m17844b(AsteroidType.class, "425fb267c8b706d9b97fe565cd4febac", -1);
}
