package logic.data.link;

import game.script.Actor;
import game.script.space.Asteroid;
import logic.baa.C4062yl;
import logic.res.code.C5640aRc;
import logic.res.code.C5663aRz;
import p001a.C0064Am;

import java.io.Serializable;

/* renamed from: a.li */
/* compiled from: a */
public interface C2911li extends C3396rD {
    public static final C5663aRz aMH = C5640aRc.m17844b(Asteroid.class, "386f8c2aec570f4b144e7bcf6e189aa4", -1);
    public static final C5663aRz dVd = C5640aRc.m17844b(Asteroid.class, "25863e8583c60570c2ba57ec8919328c", -1);

    @C0064Am(aul = "9e121de164258ed3b3a18ee9a1446336")
    /* renamed from: a.li$a */
    public interface C2912a extends C4062yl, Serializable {
        /* renamed from: a */
        void mo17467a(Asteroid aeg, Actor cr);
    }
}
