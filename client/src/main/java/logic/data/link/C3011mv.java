package logic.data.link;

import game.script.nursery.NurseryDefaults;
import logic.res.code.C5640aRc;
import logic.res.code.C5663aRz;

/* renamed from: a.mv */
/* compiled from: a */
public interface C3011mv extends C3161oY {
    public static final C5663aRz aCN = C5640aRc.m17844b(NurseryDefaults.class, "fc0592326b81e5fb6a3c5d4e01bc78c6", -1);
    public static final C5663aRz aCO = C5640aRc.m17844b(NurseryDefaults.class, "c3e2ad5406f827a11909ed92147940c0", -1);
    public static final C5663aRz aCP = C5640aRc.m17844b(NurseryDefaults.class, "6a58a2768dc6e1dc62fd5f58f33d3ef9", -1);
    public static final C5663aRz aCQ = C5640aRc.m17844b(NurseryDefaults.class, "880748544fa3e535bc90b372259685a4", -1);
    public static final C5663aRz aCR = C5640aRc.m17844b(NurseryDefaults.class, "5457b86abf4938ee757c934f3168250b", -1);
    public static final C5663aRz aCS = C5640aRc.m17844b(NurseryDefaults.class, "8c0c6da97eb02e9fa46d01b6fe6fe544", -1);
    public static final C5663aRz aCT = C5640aRc.m17844b(NurseryDefaults.class, "5ff597398b13418039e8f43507b6097f", -1);
    public static final C5663aRz aCU = C5640aRc.m17844b(NurseryDefaults.class, "06145e07ed3c49daf404210cc791e24e", -1);
    public static final C5663aRz aCV = C5640aRc.m17844b(NurseryDefaults.class, "8a55edc1efc79735c794154914b4d0f0", -1);
    public static final C5663aRz aCW = C5640aRc.m17844b(NurseryDefaults.class, "411f0650ecc37f71461a3bf0ed1067f0", -1);
    public static final C5663aRz aCX = C5640aRc.m17844b(NurseryDefaults.class, "14a58d1400d226ff05f357d234d2e2ce", -1);
    public static final C5663aRz aCY = C5640aRc.m17844b(NurseryDefaults.class, "a44b2ceb63ea125e270d73f0b7c78d98", -1);
    public static final C5663aRz aCZ = C5640aRc.m17844b(NurseryDefaults.class, "72ac4b19a02338b9177fad95a088bd4d", -1);
    public static final C5663aRz aDa = C5640aRc.m17844b(NurseryDefaults.class, "af5f5fdc9cea3b0b2f8f80875f1c8556", -1);
    public static final C5663aRz aDb = C5640aRc.m17844b(NurseryDefaults.class, "556d75cdf9cd67a14449f0cfb1e427d7", -1);
    public static final C5663aRz aDc = C5640aRc.m17844b(NurseryDefaults.class, "40f2a4315b2d4a38188f9f294b74558f", -1);
    public static final C5663aRz aDd = C5640aRc.m17844b(NurseryDefaults.class, "bacddc2f1a9d3eab387df50299f08c0a", -1);
    public static final C5663aRz aDe = C5640aRc.m17844b(NurseryDefaults.class, "21035d11294a83f805c040d1d9cfeae5", -1);
    public static final C5663aRz aDf = C5640aRc.m17844b(NurseryDefaults.class, "ec6f3fab91e7ff2ab8e77bc216c9f003", -1);
    public static final C5663aRz aDg = C5640aRc.m17844b(NurseryDefaults.class, "d981deae72294e332f678f81154a6064", -1);
    public static final C5663aRz aDh = C5640aRc.m17844b(NurseryDefaults.class, "b4e6ae40d60710ab36b3e96f560f6f0c", -1);

    /* renamed from: cI */
    public static final C5663aRz f8671cI = C5640aRc.m17844b(NurseryDefaults.class, "448c89d50fca8b9e8c801f0c59dbb667", -1);
}
