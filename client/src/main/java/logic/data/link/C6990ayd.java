package logic.data.link;

import game.script.item.ItemTypeCategory;
import logic.res.code.C5640aRc;
import logic.res.code.C5663aRz;

/* renamed from: a.ayd  reason: case insensitive filesystem */
/* compiled from: a */
public interface C6990ayd extends C3161oY {
    public static final C5663aRz aMN = C5640aRc.m17844b(ItemTypeCategory.class, "619e803eb8933cd0ca8228c052f903c0", -1);
    public static final C5663aRz anV = C5640aRc.m17844b(ItemTypeCategory.class, "6267ac88fde876f5d33962ace7483f87", -1);
    public static final C5663aRz avr = C5640aRc.m17844b(ItemTypeCategory.class, "127c687bda709d7fcc067219e276c412", -1);
    public static final C5663aRz avs = C5640aRc.m17844b(ItemTypeCategory.class, "8b9ba86467cda49bb9f43e572e5ba075", -1);
    public static final C5663aRz avu = C5640aRc.m17844b(ItemTypeCategory.class, "0c2d8f26200f21dd6de6716a656487f3", -1);
    public static final C5663aRz bbu = C5640aRc.m17844b(ItemTypeCategory.class, "f452849beae42130023013af95b97315", -1);
    public static final C5663aRz bgs = C5640aRc.m17844b(ItemTypeCategory.class, "639c65de6fe8bdebebf32beb363180ff", -1);

    /* renamed from: bk */
    public static final C5663aRz f5629bk = C5640aRc.m17844b(ItemTypeCategory.class, "5e96d760c331c65c9660cb725288e6a7", -1);

    /* renamed from: cI */
    public static final C5663aRz f5630cI = C5640aRc.m17844b(ItemTypeCategory.class, "b0553ac47dde3fbb23f4687696359b5d", -1);
    public static final C5663aRz cQU = C5640aRc.m17844b(ItemTypeCategory.class, "7dba25eeecc7e2d4627ab8621357732b", -1);
    public static final C5663aRz gRU = C5640aRc.m17844b(ItemTypeCategory.class, "39da1f3858f20d0b3e029908e3e4d9d2", -1);

    /* renamed from: vE */
    public static final C5663aRz f5631vE = C5640aRc.m17844b(ItemTypeCategory.class, "5cb6234f0923c66c217789d000b397d2", -1);

    /* renamed from: xf */
    public static final C5663aRz f5632xf = C5640aRc.m17844b(ItemTypeCategory.class, "3f25dc4f1a3255ec45a70510f1896c16", -1);
}
