package logic.data.link;

import game.script.simulation.SurfaceDefaults;
import logic.res.code.C5640aRc;
import logic.res.code.C5663aRz;

/* renamed from: a.vl */
/* compiled from: a */
public interface C3850vl extends C6408amQ {
    public static final C5663aRz byW = C5640aRc.m17844b(SurfaceDefaults.class, "7062d08f6f1e26ccff69458127f146a2", -1);
    public static final C5663aRz byX = C5640aRc.m17844b(SurfaceDefaults.class, "1fa1d107e4bff5e847607065652573fe", -1);
    public static final C5663aRz byY = C5640aRc.m17844b(SurfaceDefaults.class, "ac3bf38b150820c0a3178ad4a38cff35", -1);
    public static final C5663aRz byZ = C5640aRc.m17844b(SurfaceDefaults.class, "14a147b6d2c1d52eb8f77ea5dbaabbd7", -1);
    public static final C5663aRz bza = C5640aRc.m17844b(SurfaceDefaults.class, "de3889ee2114be99876b00afd02720ca", -1);
    public static final C5663aRz bzb = C5640aRc.m17844b(SurfaceDefaults.class, "1fd8f2825f6825350abb055f42848038", -1);

    /* renamed from: cI */
    public static final C5663aRz f9436cI = C5640aRc.m17844b(SurfaceDefaults.class, "e97bf248bec55e7b004ca3247641dc44", -1);
}
