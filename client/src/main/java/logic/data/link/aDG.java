package logic.data.link;

import game.script.citizenship.CitizenshipType;
import logic.res.code.C5640aRc;
import logic.res.code.C5663aRz;

/* renamed from: a.aDG */
/* compiled from: a */
public interface aDG extends aCL {
    public static final C5663aRz bOR = C5640aRc.m17844b(CitizenshipType.class, "d07a4ba93c30fabda19b556bcdc4365d", -1);
    public static final C5663aRz bfw = C5640aRc.m17844b(CitizenshipType.class, "5927681486d14162274755ca31229e15", -1);
    public static final C5663aRz hCC = C5640aRc.m17844b(CitizenshipType.class, "2765773117cd6814ddddaedc5b26b335", -1);
    public static final C5663aRz hCD = C5640aRc.m17844b(CitizenshipType.class, "bda71d1dcf319613c3fc5f3e522340e9", -1);
    public static final C5663aRz hCE = C5640aRc.m17844b(CitizenshipType.class, "ba47049ed33b60d394446bade1290ec7", -1);
    public static final C5663aRz hCF = C5640aRc.m17844b(CitizenshipType.class, "d4aadb0f73ba3aa9351bc224f0de5957", -1);
    public static final C5663aRz hCG = C5640aRc.m17844b(CitizenshipType.class, "31889d3108ff4dd8fef3a1603105016e", -1);
    public static final C5663aRz hCH = C5640aRc.m17844b(CitizenshipType.class, "fd8f2ca8ea978974663e85094c54d633", -1);
    public static final C5663aRz hCI = C5640aRc.m17844b(CitizenshipType.class, "20c622621b93f523052dae317d3995dd", -1);
    public static final C5663aRz hCJ = C5640aRc.m17844b(CitizenshipType.class, "2d4862b3c75f18df301e48a0752057a8", -1);

    /* renamed from: nT */
    public static final C5663aRz f2517nT = C5640aRc.m17844b(CitizenshipType.class, "060e6c338ec3bf61a3e1c2990926486e", -1);

    /* renamed from: vD */
    public static final C5663aRz f2518vD = C5640aRc.m17844b(CitizenshipType.class, "f378837f9cfc21574c10415875586456", -1);

    /* renamed from: vE */
    public static final C5663aRz f2519vE = C5640aRc.m17844b(CitizenshipType.class, "df7eb7dae6704f95765abdde15677aeb", -1);

    /* renamed from: xf */
    public static final C5663aRz f2520xf = C5640aRc.m17844b(CitizenshipType.class, "15c2c8ce4a66c98aa77d3210c2fdc322", -1);
}
