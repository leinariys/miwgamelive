package logic.data.link;

import game.script.ship.Outpost;
import logic.baa.C4062yl;
import logic.res.code.C5640aRc;
import logic.res.code.C5663aRz;
import p001a.C0064Am;

import java.io.Serializable;

/* renamed from: a.alU  reason: case insensitive filesystem */
/* compiled from: a */
public interface C6360alU extends C2806kN {
    public static final C5663aRz bEM = C5640aRc.m17844b(Outpost.class, "aa7b88d348d7a24f621aefbba600fa52", -1);
    public static final C5663aRz dQN = C5640aRc.m17844b(Outpost.class, "f7a0e33d599083c2cb09ee1541e9fcc9", -1);
    public static final C5663aRz dcD = C5640aRc.m17844b(Outpost.class, "2223ed646040ece9599d191a14dc3b09", -1);

    /* renamed from: eb */
    public static final C5663aRz f4872eb = C5640aRc.m17844b(Outpost.class, "ad8562474e94ad972d48d4c89a2f2304", -1);
    public static final C5663aRz fYg = C5640aRc.m17844b(Outpost.class, "2e43a187cea8d996d16e1c55cf9dc195", -1);
    public static final C5663aRz fYh = C5640aRc.m17844b(Outpost.class, "7ccaabf4093d42bf62a12fa7521039a6", -1);
    public static final C5663aRz fYi = C5640aRc.m17844b(Outpost.class, "c11643b333ba651219dd6fce65d44e11", -1);

    @C0064Am(aul = "5742743ca1026d1ab5ab1d0e515f24e3")
    /* renamed from: a.alU$a */
    public interface C1935a extends C4062yl, Serializable {
        /* renamed from: g */
        void mo14685g(Outpost qZVar);
    }
}
