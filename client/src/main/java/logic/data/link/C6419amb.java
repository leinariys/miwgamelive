package logic.data.link;

import game.script.nls.NLSMission;
import logic.res.code.C5640aRc;
import logic.res.code.C5663aRz;

/* renamed from: a.amb  reason: case insensitive filesystem */
/* compiled from: a */
public interface C6419amb extends C5303aEd {
    public static final C5663aRz czM = C5640aRc.m17844b(NLSMission.class, "62c45860c314672f0c2d5861e537d03a", -1);
    public static final C5663aRz djL = C5640aRc.m17844b(NLSMission.class, "f423c297c6e06fdb3768691a8a988c9c", -1);
    public static final C5663aRz fYA = C5640aRc.m17844b(NLSMission.class, "818315865b0cb8262b8d21d978ac2bdf", -1);
    public static final C5663aRz fYB = C5640aRc.m17844b(NLSMission.class, "6e2250a2dcb3cf19282bec1f239d6923", -1);
    public static final C5663aRz fYC = C5640aRc.m17844b(NLSMission.class, "5c5364701a9d325227b246b65f9b75f3", -1);
    public static final C5663aRz fYD = C5640aRc.m17844b(NLSMission.class, "50acf3999c1c495a8d9888cfc3aa57fb", -1);
    public static final C5663aRz fYE = C5640aRc.m17844b(NLSMission.class, "f7b6ec2b9b2622ae177bd3ae82bb8f49", -1);
    public static final C5663aRz fYF = C5640aRc.m17844b(NLSMission.class, "c79b92752296d20447dd6a5cdf20af67", -1);
    public static final C5663aRz fYG = C5640aRc.m17844b(NLSMission.class, "e25d5371b3e51b37ff3a890a9598a9ef", -1);
    public static final C5663aRz fYH = C5640aRc.m17844b(NLSMission.class, "f89f18c31a8a1ac07fb566d0c84ca3ec", -1);
    public static final C5663aRz fYI = C5640aRc.m17844b(NLSMission.class, "12dc18b64e324a6ed2dc7953b0de70fd", -1);
    public static final C5663aRz fYJ = C5640aRc.m17844b(NLSMission.class, "7c061d0883b3087982e1a1858ef46809", -1);
    public static final C5663aRz fYK = C5640aRc.m17844b(NLSMission.class, "243a2c13f2b1e9edc463dd69a6c46b36", -1);
    public static final C5663aRz fYL = C5640aRc.m17844b(NLSMission.class, "87391d7ff5718a1e4f4172c357509214", -1);
    public static final C5663aRz fYM = C5640aRc.m17844b(NLSMission.class, "47c644564814fca5b66bd5363543ea0d", -1);
    public static final C5663aRz fYN = C5640aRc.m17844b(NLSMission.class, "10f1b0a49e0dcb74a07e1011ac35fab6", -1);
    public static final C5663aRz fYO = C5640aRc.m17844b(NLSMission.class, "6496b8f2cad656c04dc93bd3bd4eff25", -1);
    public static final C5663aRz fYP = C5640aRc.m17844b(NLSMission.class, "7b3d8e51c33020d8d3c887038a0dab6c", -1);
    public static final C5663aRz fYQ = C5640aRc.m17844b(NLSMission.class, "f0245d7f1e23fbc38a2f753bbe788468", -1);
    public static final C5663aRz fYR = C5640aRc.m17844b(NLSMission.class, "28e2ad86f29fc042fb0cddf2d25c709d", -1);
    public static final C5663aRz fYS = C5640aRc.m17844b(NLSMission.class, "edf7cb53c4965d0822d1bf5387cdc11f", -1);
    public static final C5663aRz fYT = C5640aRc.m17844b(NLSMission.class, "f9104d2430b35a8fd64bb2721544fab7", -1);
    public static final C5663aRz fYU = C5640aRc.m17844b(NLSMission.class, "3f6a54ff4dbe193ded1eea3eb15ecf95", -1);
    public static final C5663aRz fYV = C5640aRc.m17844b(NLSMission.class, "76942550d7857f5cb3f644e754045ec8", -1);
    public static final C5663aRz fYW = C5640aRc.m17844b(NLSMission.class, "344218a9410bf06d08ba975abc82ba06", -1);
    public static final C5663aRz fYw = C5640aRc.m17844b(NLSMission.class, "52ce2733e238c7ef091fdac68f5abd89", -1);
    public static final C5663aRz fYx = C5640aRc.m17844b(NLSMission.class, "9f64fd95a42be6d564e8b7c5c0feb255", -1);
    public static final C5663aRz fYy = C5640aRc.m17844b(NLSMission.class, "3beac6f10468e73466e8ebf5ca31164b", -1);
    public static final C5663aRz fYz = C5640aRc.m17844b(NLSMission.class, "f54fe6e4753b7d7aabfefc58091b82de", -1);

    /* renamed from: vD */
    public static final C5663aRz f4918vD = C5640aRc.m17844b(NLSMission.class, "6e202ff6d04856c88d4a9f4f2256376e", -1);
}
