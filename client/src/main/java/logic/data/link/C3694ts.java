package logic.data.link;

import game.script.Actor;
import game.script.space.AsteroidType;
import game.script.spacezone.AsteroidZone;
import logic.baa.C4062yl;
import logic.res.code.C5640aRc;
import logic.res.code.C5663aRz;
import p001a.C0064Am;

import java.io.Serializable;

/* renamed from: a.ts */
/* compiled from: a */
public interface C3694ts extends C6570apW {

    /* renamed from: bm */
    public static final C5663aRz f9275bm = C5640aRc.m17844b(AsteroidZone.class, "613eb2db7709cf4c0cdab0ed14a4ed85", -1);
    public static final C5663aRz bmL = C5640aRc.m17844b(AsteroidZone.class, "5336e2be20e3ce37efd0f6b39b5f563a", -1);
    public static final C5663aRz bmM = C5640aRc.m17844b(AsteroidZone.class, "7a2c38a4d12cb05bdaf13760b89ec4c1", -1);
    public static final C5663aRz bmN = C5640aRc.m17844b(AsteroidZone.class, "9cee1d408ef08b4a98ea486a97744e49", -1);

    @C0064Am(aul = "3d9b5c9fbfee671366a872a73d6e474e")
    /* renamed from: a.ts$a */
    public interface C3695a extends C4062yl, Serializable {
        /* renamed from: a */
        void mo7556a(AsteroidZone btVar, AsteroidType aqn, Actor cr);
    }
}
