package logic.data.link;

import game.script.progression.ProgressionAbility;
import logic.res.code.C5640aRc;
import logic.res.code.C5663aRz;

/* renamed from: a.abm  reason: case insensitive filesystem */
/* compiled from: a */
public interface C5858abm extends C6408amQ {

    /* renamed from: bk */
    public static final C5663aRz f4156bk = C5640aRc.m17844b(ProgressionAbility.class, "c0e2c08df5ea8d2fdc36db5623d93f64", -1);

    /* renamed from: cI */
    public static final C5663aRz f4157cI = C5640aRc.m17844b(ProgressionAbility.class, "bd2dda7a3d35eeae40efebdf6e32d560", -1);
    public static final C5663aRz eXP = C5640aRc.m17844b(ProgressionAbility.class, "18bb11ec538148b37ac7366418eeb50c", -1);
    public static final C5663aRz eXQ = C5640aRc.m17844b(ProgressionAbility.class, "6958eeba71dd47c40c0a12c7bbbdf152", -1);

    /* renamed from: vD */
    public static final C5663aRz f4158vD = C5640aRc.m17844b(ProgressionAbility.class, "dd9798136dd22d597e5ec103a589c7f1", -1);

    /* renamed from: vE */
    public static final C5663aRz f4159vE = C5640aRc.m17844b(ProgressionAbility.class, "2bdfd12a74a57d0502b1b267790107b2", -1);
}
