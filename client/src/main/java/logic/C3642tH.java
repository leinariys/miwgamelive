package logic;

import com.hoplon.taikodom.common.TaikodomVersion;
import game.engine.C1175RP;
import game.engine.DataGameEvent;
import game.network.WhoAmI;
import game.network.build.BuilderSocketStatic;
import game.network.build.IServerConnect;
import game.network.channel.NetworkChannel;
import game.script.cloning.CloningDefaults;
import game.script.main.bot.BotScript;
import game.script.player.Player;
import game.script.player.User;
import game.script.ship.Station;
import logic.res.*;
import logic.res.code.C1625Xn;
import logic.res.code.DataGameEventImpl;
import logic.res.code.ResourceDeobfuscation;
import logic.thred.ThreadWrapper;
import logic.thred.WatchDog;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.junit.Assert;
import p001a.*;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Executor;

/* renamed from: a.tH */
/* compiled from: a */
public class C3642tH implements Runnable {
    /* access modifiers changed from: private */
    public static final Log log = LogFactory.getLog(C3642tH.class);
    private static final long hae = (((long) (((double) SingletonCurrentTimeMilliImpl.currentTimeMillis()) * Math.random())) % 1000000000);
    public static String[] hak;
    static String hal;
    private static long fFx;
    private static int had;
    private static int hag = 0;
    private static int hah = 0;
    private static int hai = 0;
    private static int haj;
    private static WatchDog watchDog;

    static {
        Assert.assertTrue(C5877acF.RUNNING_CLIENT_BOT);
    }

    /* access modifiers changed from: private */
    public C1175RP dTs;
    private String eef;
    private C1285Sv gTV;
    private boolean haf = false;
    private String password;
    private String username;
    /* renamed from: vS */
    private EngineGame f9232vS;

    public C3642tH(String str, String str2, Boolean bool) {
        this.haf = bool.booleanValue();
        this.username = str;
        this.password = str2;
    }

    private static synchronized void cGj() {
        boolean z = true;
        synchronized (C3642tH.class) {
            Assert.assertTrue(had > 0);
            String str = "Bot-" + had + "-" + hae;
            hak[had - 1] = str;
            if (had <= 1) {
                z = false;
            }
            ThreadWrapper ano = new ThreadWrapper((Runnable) new C3642tH(str, "1234", Boolean.valueOf(z)));
            ano.setName(String.valueOf(str) + " Thread");
            ano.start();
            try {
                Thread.sleep(500);
            } catch (InterruptedException e) {
            }
            had--;
        }
    }

    public static void main(String[] strArr) {
        m39568k(strArr);
        watchDog = new WatchDog();
        watchDog.start();
        fFx = SingletonCurrentTimeMilliImpl.currentTimeMillis();
        had = hai + hah + hag;
        hak = new String[had];
        cGj();
    }

    /* renamed from: k */
    private static void m39568k(String[] strArr) {
        if (strArr.length == 0) {
            hai = 1;
            return;
        }
        for (String kN : strArr) {
            m39569kN(kN);
        }
    }

    /* renamed from: kN */
    private static void m39569kN(String str) {
        if (str.startsWith("pvp=")) {
            hag = m39570kO(str);
        } else if (str.startsWith("mission=")) {
            hai = m39570kO(str);
        } else if (str.startsWith("miner=")) {
            hah = m39570kO(str);
        } else {
            System.err.println("Usage: '<program>' or: '<program> miner=? mission=? pvp=?'");
            System.exit(-1);
        }
    }

    /* renamed from: kO */
    private static int m39570kO(String str) {
        return Integer.parseInt(str.split("=")[1]);
    }

    private void boL() {
        this.f9232vS = new EngineGame(new FileControl("."), new FileControl(System.getProperty("gametoolkit.client.render.rootpath", ".")));
        this.f9232vS.mo15325fn(true);
        ConfigManager cHu = ConfigManager.initConfigManager();
        m39567b(cHu);
        this.f9232vS.setConfigManager(cHu);
        this.f9232vS.getVerbose();
        this.f9232vS.mo15306a((EventManager) new C3646c());
        this.dTs = new C1175RP();
        this.f9232vS.mo4066a(new C6047afT().alf());
        new C3647d();
        this.f9232vS.mo4065a((ILoaderTrail) new C6479anj());
    }

    /* JADX WARNING: Removed duplicated region for block: B:27:0x00ce A[SYNTHETIC, Splitter:B:27:0x00ce] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void run() {
        /*
            r8 = this;
            a.tH$b r6 = new a.tH$b
            r6.<init>()
            a.tH$a r7 = new a.tH$a
            r7.<init>()
            r8.boL()     // Catch:{ Exception -> 0x007e }
            a.RP r0 = r8.dTs     // Catch:{ Exception -> 0x007e }
            java.lang.Thread r1 = java.lang.Thread.currentThread()     // Catch:{ Exception -> 0x007e }
            r0.mo5173h(r1)     // Catch:{ Exception -> 0x007e }
            r0 = 1
            p001a.C5916acs.m20630eb(r0)     // Catch:{ Exception -> 0x007e }
            a.aou r0 = r8.f9232vS     // Catch:{ Exception -> 0x007e }
            p001a.C5916acs.m20628a(r0)     // Catch:{ Exception -> 0x007e }
            r8.cGl()     // Catch:{ Throwable -> 0x00ae }
            a.aou r0 = r8.f9232vS     // Catch:{ Throwable -> 0x00ae }
            a.aDk r1 = r0.bhe()     // Catch:{ Throwable -> 0x00ae }
            a.aou r0 = r8.f9232vS     // Catch:{ Throwable -> 0x00ae }
            a.DN r0 = r0.ala()     // Catch:{ Throwable -> 0x00ae }
            a.ati r4 = r0.aKS()     // Catch:{ Throwable -> 0x00ae }
            int r0 = r8.cGk()     // Catch:{ Throwable -> 0x00ae }
            int r2 = hag     // Catch:{ Throwable -> 0x00ae }
            if (r0 >= r2) goto L_0x0091
            java.lang.String r0 = hal     // Catch:{ Throwable -> 0x00ae }
            if (r0 == 0) goto L_0x0044
            int r0 = p001a.C5603aPr.iBo     // Catch:{ Throwable -> 0x00ae }
            int r0 = r0 % 3
            if (r0 != 0) goto L_0x008a
        L_0x0044:
            a.BF r0 = r4.cvm()     // Catch:{ Throwable -> 0x00ae }
            java.lang.String r2 = r0.getName()     // Catch:{ Throwable -> 0x00ae }
            hal = r2     // Catch:{ Throwable -> 0x00ae }
        L_0x004e:
            a.rP r2 = r0.azW()     // Catch:{ Throwable -> 0x00ae }
            r4.mo16130k(r2)     // Catch:{ Throwable -> 0x00ae }
            a.rP r2 = r0.azW()     // Catch:{ Throwable -> 0x00ae }
            r3 = 1
            r2.mo21625aV(r3)     // Catch:{ Throwable -> 0x00ae }
            a.akU r2 = r8.m39566a((p001a.C5284aDk) r1, (p001a.C6790ati) r4, (p001a.C0096BF) r0)     // Catch:{ Throwable -> 0x00ae }
            a.Sv r0 = new a.Sv     // Catch:{ Throwable -> 0x00ae }
            a.aou r1 = r8.f9232vS     // Catch:{ Throwable -> 0x00ae }
            a.Sv$c r3 = p001a.C1285Sv.C1288c.PVP     // Catch:{ Throwable -> 0x00ae }
            java.lang.String r5 = r8.eef     // Catch:{ Throwable -> 0x00ae }
            r0.<init>(r1, r2, r3, r4, r5)     // Catch:{ Throwable -> 0x00ae }
            r8.gTV = r0     // Catch:{ Throwable -> 0x00ae }
        L_0x006e:
            long r0 = p001a.C6768atM.currentTimeMillis()     // Catch:{ Throwable -> 0x00e7 }
            r2 = 10
            long r0 = r0 - r2
        L_0x0075:
            a.aou r2 = r8.f9232vS     // Catch:{ Throwable -> 0x00e7 }
            boolean r2 = r2.aNB()     // Catch:{ Throwable -> 0x00e7 }
            if (r2 != 0) goto L_0x00ce
        L_0x007d:
            return
        L_0x007e:
            r0 = move-exception
            org.apache.commons.logging.Log r1 = log
            java.lang.String r2 = "Error initializing modules. Client will shutdown."
            r1.error(r2, r0)
            r0.printStackTrace()
            goto L_0x007d
        L_0x008a:
            java.lang.String r0 = hal     // Catch:{ Throwable -> 0x00ae }
            a.BF r0 = r4.mo16129jV(r0)     // Catch:{ Throwable -> 0x00ae }
            goto L_0x004e
        L_0x0091:
            int r2 = hai     // Catch:{ Throwable -> 0x00ae }
            int r3 = hag     // Catch:{ Throwable -> 0x00ae }
            int r2 = r2 + r3
            if (r0 >= r2) goto L_0x00b8
            a.BF r0 = r4.cvm()     // Catch:{ Throwable -> 0x00ae }
            a.akU r2 = r8.m39566a((p001a.C5284aDk) r1, (p001a.C6790ati) r4, (p001a.C0096BF) r0)     // Catch:{ Throwable -> 0x00ae }
            a.Sv r0 = new a.Sv     // Catch:{ Throwable -> 0x00ae }
            a.aou r1 = r8.f9232vS     // Catch:{ Throwable -> 0x00ae }
            a.Sv$c r3 = p001a.C1285Sv.C1288c.MISSION     // Catch:{ Throwable -> 0x00ae }
            java.lang.String r5 = r8.eef     // Catch:{ Throwable -> 0x00ae }
            r0.<init>(r1, r2, r3, r4, r5)     // Catch:{ Throwable -> 0x00ae }
            r8.gTV = r0     // Catch:{ Throwable -> 0x00ae }
            goto L_0x006e
        L_0x00ae:
            r0 = move-exception
            r0.printStackTrace()
            r0 = 1001(0x3e9, float:1.403E-42)
            java.lang.System.exit(r0)
            goto L_0x006e
        L_0x00b8:
            a.BF r0 = r4.cvo()     // Catch:{ Throwable -> 0x00ae }
            a.akU r2 = r8.m39566a((p001a.C5284aDk) r1, (p001a.C6790ati) r4, (p001a.C0096BF) r0)     // Catch:{ Throwable -> 0x00ae }
            a.Sv r0 = new a.Sv     // Catch:{ Throwable -> 0x00ae }
            a.aou r1 = r8.f9232vS     // Catch:{ Throwable -> 0x00ae }
            a.Sv$c r3 = p001a.C1285Sv.C1288c.MINER     // Catch:{ Throwable -> 0x00ae }
            java.lang.String r5 = r8.eef     // Catch:{ Throwable -> 0x00ae }
            r0.<init>(r1, r2, r3, r4, r5)     // Catch:{ Throwable -> 0x00ae }
            r8.gTV = r0     // Catch:{ Throwable -> 0x00ae }
            goto L_0x006e
        L_0x00ce:
            long r0 = r8.m39564a((java.util.concurrent.Executor) r6, (logic.C3642tH.C3643a) r7, (long) r0)     // Catch:{ Throwable -> 0x00e7 }
            boolean r2 = r8.haf     // Catch:{ Throwable -> 0x00e7 }
            if (r2 == 0) goto L_0x00dc
            r2 = 0
            r8.haf = r2     // Catch:{ Throwable -> 0x00e7 }
            cGj()     // Catch:{ Throwable -> 0x00e7 }
        L_0x00dc:
            a.Sv r2 = r8.gTV     // Catch:{ Throwable -> 0x00e7 }
            r2.mo16926gZ()     // Catch:{ Throwable -> 0x00e7 }
            r2 = 100
            java.lang.Thread.sleep(r2)     // Catch:{ Throwable -> 0x00e7 }
            goto L_0x0075
        L_0x00e7:
            r0 = move-exception
            r0.printStackTrace()
            r0 = 1002(0x3ea, float:1.404E-42)
            java.lang.System.exit(r0)
            goto L_0x007d
        */
        throw new UnsupportedOperationException("Method not decompiled: logic.C3642tH.run():void");
    }

    private synchronized int cGk() {
        int i;
        i = haj;
        haj = i + 1;
        return i;
    }

    /* renamed from: a */
    private Player m39566a(User adk, BotScript ati, Station bf) {
        Player b = ati.mo16110b(adk, this.username, bf);
        adk.cSV().mo15383bL(b);
        try {
            CloningDefaults xd = ati.ala().aJe().mo19010xd();
            ati.ala().mo1436b(b, xd.bTc(), xd.bTg());
        } catch (Exception e) {
            e.printStackTrace();
        }
        b.mo14347aH(bf);
        return b;
    }

    /* renamed from: a */
    private long m39564a(Executor executor, C3643a aVar, long j) {
        float f = 1.0E-8f;
        if (!(this.f9232vS.aPT() == null || this.f9232vS.bhc() == null || this.f9232vS.aPT().isUp())) {
            boP();
        }
        long currentTimeMillis = SingletonCurrentTimeMilliImpl.currentTimeMillis();
        float f2 = ((float) (currentTimeMillis - j)) * 0.001f;
        if (f2 > 1.0E-8f) {
            f = f2;
        }
        if (currentTimeMillis > fFx + 300000) {
            watchDog.mo14456f((Thread) null);
            watchDog.reset();
            fFx = currentTimeMillis;
        }
        this.f9232vS.alf().step((long) (1000.0f * f), executor);
        if (this.f9232vS.bhc() != null) {
            this.f9232vS.bhc().mo3366b(f, executor);
            this.f9232vS.bhc().bGS().mo2194gZ();
        }
        return currentTimeMillis;
    }

    private void boP() {
        this.f9232vS.disconnect();
        log.info("*** disconnected from server *** ");
    }

    public void cGl() {
        int i = IServerConnect.PORT_LOGIN;
        this.eef = IServerConnect.DEFAULT_HOST;
        try {
            ConfigManagerSection kW = this.f9232vS.getConfigManager().getSection(ConfigIniKeyValue.SERVER.getValue());
            this.eef = kW.getValuePropertyString(ConfigIniKeyValue.SERVER, IServerConnect.DEFAULT_HOST);
            i = kW.mo7195a(ConfigIniKeyValue.SERVER_PORT, Integer.valueOf(IServerConnect.PORT_LOGIN)).intValue();
        } catch (RuntimeException e) {
            log.info(e);
        }
        NetworkChannel a = BuilderSocketStatic.m21641a(this.eef, i, this.username, this.password, false, TaikodomVersion.VERSION);
        ResourceDeobfuscation sh = new ResourceDeobfuscation();
        sh.setServerSocket(a);
        sh.setWhoAmI(WhoAmI.CLIENT);
        sh.setClassObject(Taikodom.class);
        sh.mo13372ev(false);
        sh.mo13373ew(false);
        sh.setEngineGame(this.f9232vS);
        this.f9232vS.mo15322d((DataGameEvent) new DataGameEventImpl(sh));
        System.out.println("Log in with: " + this.f9232vS.getUser().getFullName());
        this.f9232vS.mo15313c(a);
    }

    /* renamed from: b */
    private void m39567b(ConfigManager aao) {
        try {
            aao.loadConfig("config/defaults.ini");
            log.trace("  Default configuration file loaded");
        } catch (IOException e) {
            log.error("  Unable to load configuration file", e);
        }
        try {
            aao.loadConfig("config/user.ini");
            log.trace("  User configuration file loaded");
        } catch (IOException e2) {
            log.warn("  User configuration file not loaded (does not exist?)");
        }
        try {
            aao.loadConfig("config/devel.ini");
            log.warn("  Development configuration file loaded");
        } catch (IOException e3) {
        }
    }

    /* renamed from: a.tH$c */
    /* compiled from: a */
    class C3646c extends EventManager {
        C3646c() {
        }
    }

    /* renamed from: a.tH$d */
    /* compiled from: a */
    class C3647d implements Executor {
        C3647d() {
        }

        public void execute(Runnable runnable) {
            try {
                runnable.run();
            } catch (Exception e) {
                C3642tH.log.error("An exception was caught while executing a task", e);
            }
        }
    }

    /* renamed from: a.tH$a */
    public class C3643a implements Executor {
        List<Runnable> list = new ArrayList();

        public C3643a() {
        }

        public void execute(Runnable runnable) {
            this.list.add(new C3644a(runnable));
        }

        public void cCM() {
        }

        /* renamed from: a.tH$a$a */
        class C3644a implements Runnable {
            private final /* synthetic */ Runnable bsv;

            C3644a(Runnable runnable) {
                this.bsv = runnable;
            }

            public void run() {
                C1625Xn cYc = C3642tH.this.dTs.cYc();
                try {
                    cYc.brK();
                    this.bsv.run();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                } finally {
                    cYc.brL();
                }
            }
        }
    }

    /* renamed from: a.tH$b */
    /* compiled from: a */
    class C3645b implements Executor {
        C3645b() {
        }

        public void execute(Runnable runnable) {
            try {
                runnable.run();
            } catch (Exception e) {
                C3642tH.log.warn(e);
            }
        }
    }
}
