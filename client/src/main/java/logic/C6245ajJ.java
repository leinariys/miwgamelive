package logic;

import java.util.Collection;

/* renamed from: a.ajJ  reason: case insensitive filesystem */
/* compiled from: a */
public interface C6245ajJ {
    /* renamed from: a */
    <T> Collection<T> mo13963a(String str, Class<T> cls);

    /* renamed from: a */
    <T> void mo13964a(aVH<T> avh);

    /* renamed from: a */
    <T> void mo13965a(Class<T> cls, aVH<T> avh);

    /* renamed from: a */
    <T> void mo13966a(String str, Class<?> cls, aVH<T> avh);

    /* renamed from: a */
    void mo2869a(String str, Object obj);

    /* renamed from: b */
    <T> Collection<T> mo13967b(Class<T> cls);

    /* renamed from: b */
    <T> void mo13968b(Class<T> cls, aVH<T> avh);

    /* renamed from: b */
    <T> void mo13969b(String str, Class<?> cls, aVH<T> avh);

    /* renamed from: b */
    void mo13970b(String str, Object obj);

    /* renamed from: c */
    void mo13971c(String str, Object obj);

    /* renamed from: d */
    void mo13972d(String str, Object obj);

    /* renamed from: e */
    <T> Collection<T> mo13973e(String str, T t);

    /* renamed from: g */
    void mo13974g(Object obj);

    /* renamed from: h */
    void mo13975h(Object obj);

    /* renamed from: i */
    void mo13976i(Object obj);

    /* renamed from: j */
    <T> Collection<T> mo13977j(T t);

    void publish(Object obj);
}
