package logic;

import java.util.AbstractSet;
import java.util.Iterator;
import java.util.WeakHashMap;

/* renamed from: a.aUK */
/* compiled from: a */
public class aUK<E> extends AbstractSet<E> {
    private static final Object iXD = new Object();
    private WeakHashMap<E, Object> iXE = new WeakHashMap<>();

    public boolean add(E e) {
        return this.iXE.put(e, iXD) != null;
    }

    public boolean remove(Object obj) {
        return this.iXE.remove(obj) != null;
    }

    public boolean contains(Object obj) {
        return this.iXE.containsKey(obj);
    }

    public Iterator<E> iterator() {
        return this.iXE.keySet().iterator();
    }

    public int size() {
        return this.iXE.size();
    }
}
