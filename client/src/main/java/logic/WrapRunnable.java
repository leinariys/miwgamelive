package logic;

/* renamed from: a.aen  reason: case insensitive filesystem */
/* compiled from: a */
public abstract class WrapRunnable implements Runnable {
    private boolean isCanceled;
    private boolean isReset;

    public void cancel() {
        this.isCanceled = true;
    }

    public boolean isCanceled() {
        return this.isCanceled;
    }

    public void reset() {
        this.isReset = true;
    }

    /* renamed from: Iw */
    public boolean isReset() {
        return this.isReset;
    }

    /* renamed from: am */
    public void setReset(boolean reset) {
        this.isReset = reset;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: an */
    public void setCanceled(boolean canceled) {
        this.isCanceled = canceled;
    }
}
