package logic;

import logic.res.ILoaderImageInterface;
import logic.res.css.C6868avI;
import logic.ui.C1276Sr;
import logic.ui.C2698il;
import logic.ui.item.InternalFrame;
import org.w3c.dom.css.CSSStyleDeclaration;

import javax.swing.*;
import java.awt.*;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;

/* renamed from: a.axa  reason: case insensitive filesystem */
/* compiled from: a */
public interface C6961axa {
    /* renamed from: a */
    <T extends C1276Sr, C extends aWa> T mo16779a(Class<C> cls, String str, C2698il ilVar);

    /* renamed from: a */
    <T extends C1276Sr> T mo16780a(String str, C2698il ilVar);

    /* renamed from: a */
    C6868avI mo16781a(InputStream inputStream) throws UnsupportedEncodingException;

    /* renamed from: a */
    C6868avI mo16782a(Class<? extends Object> cls, String str);

    /* renamed from: a */
    C6868avI mo16783a(Object obj, String str);

    /* renamed from: a */
    void mo16784a(JComponent jComponent, C6868avI avi, String str);

    /* renamed from: aY */
    CSSStyleDeclaration mo16785aY(String str);

    IAddonSettings adA();

    boolean adB();

    ILoaderImageInterface adz();

    /* renamed from: b */
    <T extends C1276Sr, C extends aWa> T mo16789b(Class<C> cls, String str);

    /* renamed from: b */
    void mo16790b(Object obj, String str);

    /* renamed from: b */
    void addInRootPane(JComponent jComponent);

    /* renamed from: bN */
    InternalFrame mo16792bN(String str);

    /* renamed from: bO */
    <T extends C1276Sr> void mo16793bO(String str);

    /* renamed from: bQ */
    <T extends C1276Sr> T mo16794bQ(String str);

    /* renamed from: c */
    <T extends aWa> InternalFrame mo16795c(Class<T> cls, String str);

    /* renamed from: c */
    Rectangle mo16796c(JComponent jComponent);

    /* renamed from: c */
    <T extends C1276Sr> void mo16797c(String str, Class<T> cls);

    void dispose();

    Point getMousePosition();

    int getScreenHeight();

    Dimension getScreenSize();

    int getScreenWidth();

    /* renamed from: s */
    void mo16803s(int i, int i2);
}
