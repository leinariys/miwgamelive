package logic;

import taikodom.render.loader.provider.FilePath;

import java.io.IOException;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLStreamHandler;

/* renamed from: a.bf */
/* compiled from: a */
public class StreamRootPathRender extends URLStreamHandler {

    /* renamed from: ng */
    private final FilePath rootPathRender;

    public StreamRootPathRender(FilePath ain) {
        this.rootPathRender = ain;
    }

    /* access modifiers changed from: protected */
    public URLConnection openConnection(URL url) throws IOException {
        if ("res".equalsIgnoreCase(url.getProtocol())) {
            return this.rootPathRender.concat(url.toString()
                    .substring("res://".length()))
                    .getFile()
                    .toURI()
                    .toURL()
                    .openConnection();
        }
        throw new IOException("Unknow protocol: " + url.getProtocol() + " at " + url);
    }
}
