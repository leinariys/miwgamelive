package logic.abc;

import com.hoplon.geometry.Vec3f;
import p001a.C0647JL;
import p001a.C3655tK;
import p001a.C3978xf;

/* renamed from: a.aLE */
/* compiled from: a */
public class aLE extends C5508aMa {
    public aLE(float f, float f2) {
        this.implicitShapeDimensions.set(f, 0.5f * f2, f);
    }

    public Vec3f localGetSupportingVertexWithoutMargin(Vec3f vec3f) {
        this.stack.bcH().push();
        try {
            Vec3f h = this.stack.bcH().mo4460h(0.0f, 0.0f, 0.0f);
            Vec3f ac = this.stack.bcH().mo4458ac(vec3f);
            float lengthSquared = ac.lengthSquared();
            if (lengthSquared < 1.0E-4f) {
                ac.set(1.0f, 0.0f, 0.0f);
            } else {
                ac.scale(1.0f / ((float) Math.sqrt((double) lengthSquared)));
            }
            Vec3f vec3f2 = (Vec3f) this.stack.bcH().get();
            float radius = getRadius();
            Vec3f vec3f3 = (Vec3f) this.stack.bcH().get();
            Vec3f vec3f4 = (Vec3f) this.stack.bcH().get();
            Vec3f h2 = this.stack.bcH().mo4460h(0.0f, dil(), 0.0f);
            C0647JL.m5603g(vec3f3, ac, this.localScaling);
            vec3f3.scale(radius);
            vec3f4.scale(getMargin(), ac);
            vec3f2.add(h2, vec3f3);
            vec3f2.sub(vec3f4);
            float dot = ac.dot(vec3f2);
            if (dot > -1.0E30f) {
                h.set(vec3f2);
            } else {
                dot = -1.0E30f;
            }
            Vec3f h3 = this.stack.bcH().mo4460h(0.0f, -dil(), 0.0f);
            C0647JL.m5603g(vec3f3, ac, this.localScaling);
            vec3f3.scale(radius);
            vec3f4.scale(getMargin(), ac);
            vec3f2.add(h3, vec3f3);
            vec3f2.sub(vec3f4);
            if (ac.dot(vec3f2) > dot) {
                h.set(vec3f2);
            }
            return (Vec3f) this.stack.bcH().mo15197aq(h);
        } finally {
            this.stack.bcH().pop();
        }
    }

    public void batchedUnitVectorGetSupportingVertexWithoutMargin(Vec3f[] vec3fArr, Vec3f[] vec3fArr2, int i) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    public void calculateLocalInertia(float f, Vec3f vec3f) {
        this.stack.bcF();
        try {
            ((C3978xf) this.stack.bcJ().get()).setIdentity();
            float radius = getRadius();
            Vec3f h = this.stack.bcH().mo4460h(radius, dil() + radius, radius);
            float f2 = (h.x + 0.04f) * 2.0f;
            float f3 = (h.y + 0.04f) * 2.0f;
            float f4 = (h.z + 0.04f) * 2.0f;
            float f5 = f2 * f2;
            float f6 = f3 * f3;
            float f7 = f4 * f4;
            float f8 = 0.08333333f * f;
            vec3f.x = (f6 + f7) * f8;
            vec3f.y = (f7 + f5) * f8;
            vec3f.z = (f5 + f6) * f8;
        } finally {
            this.stack.bcG();
        }
    }

    public C3655tK arV() {
        return C3655tK.CAPSULE_SHAPE_PROXYTYPE;
    }

    public String getName() {
        return "CapsuleShape";
    }

    public float getRadius() {
        return this.implicitShapeDimensions.x;
    }

    public float dil() {
        return this.implicitShapeDimensions.y;
    }
}
