package logic.abc;

import com.hoplon.geometry.Vec3f;
import p001a.C0647JL;
import p001a.C3655tK;

import javax.vecmath.Vector3f;
import java.util.ArrayList;
import java.util.List;

/* renamed from: a.YL */
/* compiled from: a */
public class C1653YL extends C5765aVx {

    /* renamed from: jR */
    static final /* synthetic */ boolean f2171jR = (!C1653YL.class.desiredAssertionStatus());

    /* renamed from: Uc */
    private final List<Vec3f> f2172Uc = new ArrayList();

    public C1653YL(List<Vec3f> list) {
        int i = 0;
        while (true) {
            int i2 = i;
            if (i2 >= list.size()) {
                aPc();
                return;
            } else {
                this.f2172Uc.add(new Vec3f(list.get(i2)));
                i = i2 + 1;
            }
        }
    }

    public void addPoint(Vec3f vec3f) {
        this.f2172Uc.add(new Vec3f((Vector3f) vec3f));
        aPc();
    }

    /* renamed from: yc */
    public List<Vec3f> mo6934yc() {
        return this.f2172Uc;
    }

    public int bHN() {
        return this.f2172Uc.size();
    }

    public Vec3f localGetSupportingVertexWithoutMargin(Vec3f vec3f) {
        this.stack.bcH().push();
        try {
            Vec3f h = this.stack.bcH().mo4460h(0.0f, 0.0f, 0.0f);
            float f = -1.0E30f;
            Vec3f ac = this.stack.bcH().mo4458ac(vec3f);
            float lengthSquared = ac.lengthSquared();
            if (lengthSquared < 1.0E-4f) {
                ac.set(1.0f, 0.0f, 0.0f);
            } else {
                ac.scale(1.0f / ((float) Math.sqrt((double) lengthSquared)));
            }
            Vec3f vec3f2 = (Vec3f) this.stack.bcH().get();
            int i = 0;
            while (i < this.f2172Uc.size()) {
                C0647JL.m5603g(vec3f2, this.f2172Uc.get(i), this.localScaling);
                float dot = ac.dot(vec3f2);
                if (dot > f) {
                    h.set(vec3f2);
                } else {
                    dot = f;
                }
                i++;
                f = dot;
            }
            return (Vec3f) this.stack.bcH().mo15197aq(h);
        } finally {
            this.stack.bcH().pop();
        }
    }

    public void batchedUnitVectorGetSupportingVertexWithoutMargin(Vec3f[] vec3fArr, Vec3f[] vec3fArr2, int i) {
        this.stack.bcH().push();
        try {
            float[] fArr = new float[i];
            for (int i2 = 0; i2 < i; i2++) {
                fArr[i2] = -1.0E30f;
            }
            Vec3f vec3f = (Vec3f) this.stack.bcH().get();
            for (int i3 = 0; i3 < this.f2172Uc.size(); i3++) {
                C0647JL.m5603g(vec3f, this.f2172Uc.get(i3), this.localScaling);
                for (int i4 = 0; i4 < i; i4++) {
                    float dot = vec3fArr[i4].dot(vec3f);
                    if (dot > fArr[i4]) {
                        vec3fArr2[i4].set(vec3f);
                        fArr[i4] = dot;
                    }
                }
            }
        } finally {
            this.stack.bcH().pop();
        }
    }

    public Vec3f localGetSupportingVertex(Vec3f vec3f) {
        this.stack.bcH().push();
        try {
            Vec3f ac = this.stack.bcH().mo4458ac(localGetSupportingVertexWithoutMargin(vec3f));
            if (getMargin() != 0.0f) {
                Vec3f ac2 = this.stack.bcH().mo4458ac(vec3f);
                if (ac2.lengthSquared() < 1.4210855E-14f) {
                    ac2.set(-1.0f, -1.0f, -1.0f);
                }
                ac2.normalize();
                ac.scaleAdd(getMargin(), ac2, ac);
            }
            return (Vec3f) this.stack.bcH().mo15197aq(ac);
        } finally {
            this.stack.bcH().pop();
        }
    }

    public int getNumVertices() {
        return this.f2172Uc.size();
    }

    public int abC() {
        return this.f2172Uc.size();
    }

    /* renamed from: a */
    public void mo5340a(int i, Vec3f vec3f, Vec3f vec3f2) {
        int size = (i + 1) % this.f2172Uc.size();
        C0647JL.m5603g(vec3f, this.f2172Uc.get(i % this.f2172Uc.size()), this.localScaling);
        C0647JL.m5603g(vec3f2, this.f2172Uc.get(size), this.localScaling);
    }

    /* renamed from: b */
    public void mo5345b(int i, Vec3f vec3f) {
        C0647JL.m5603g(vec3f, this.f2172Uc.get(i), this.localScaling);
    }

    public int abB() {
        return 0;
    }

    /* renamed from: a */
    public void mo5341a(Vec3f vec3f, Vec3f vec3f2, int i) {
        if (!f2171jR) {
            throw new AssertionError();
        }
    }

    /* renamed from: b */
    public boolean mo5347b(Vec3f vec3f, float f) {
        if (f2171jR) {
            return false;
        }
        throw new AssertionError();
    }

    public C3655tK arV() {
        return C3655tK.CONVEX_HULL_SHAPE_PROXYTYPE;
    }

    public String getName() {
        return "Convex";
    }
}
