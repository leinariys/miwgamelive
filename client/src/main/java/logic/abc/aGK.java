package logic.abc;

import com.hoplon.geometry.Vec3f;
import p001a.C3427rS;
import p001a.C3655tK;
import p001a.C3978xf;

/* renamed from: a.aGK */
/* compiled from: a */
public class aGK extends C5508aMa {

    /* renamed from: jR */
    static final /* synthetic */ boolean f2858jR = (!aGK.class.desiredAssertionStatus());
    private final C3978xf hQl = new C3978xf();
    private final C3978xf hQm = new C3978xf();
    private azD hQn;
    private azD hQo;

    public aGK(azD azd, azD azd2) {
        this.hQn = azd;
        this.hQo = azd2;
        this.hQl.setIdentity();
        this.hQm.setIdentity();
    }

    public Vec3f localGetSupportingVertexWithoutMargin(Vec3f vec3f) {
        this.stack.bcH().push();
        try {
            Vec3f vec3f2 = (Vec3f) this.stack.bcH().get();
            Vec3f vec3f3 = (Vec3f) this.stack.bcH().get();
            Vec3f vec3f4 = (Vec3f) this.stack.bcH().get();
            C3427rS.m38372a(vec3f2, vec3f, this.hQl.bFF);
            vec3f3.set(this.hQn.localGetSupportingVertexWithoutMargin(vec3f2));
            this.hQl.mo22946G(vec3f3);
            C3427rS.m38372a(vec3f2, vec3f, this.hQm.bFF);
            vec3f4.set(this.hQo.localGetSupportingVertexWithoutMargin(vec3f2));
            this.hQm.mo22946G(vec3f4);
            Vec3f vec3f5 = (Vec3f) this.stack.bcH().get();
            vec3f5.add(vec3f3, vec3f4);
            return (Vec3f) this.stack.bcH().mo15197aq(vec3f5);
        } finally {
            this.stack.bcH().pop();
        }
    }

    public void batchedUnitVectorGetSupportingVertexWithoutMargin(Vec3f[] vec3fArr, Vec3f[] vec3fArr2, int i) {
        for (int i2 = 0; i2 < i; i2++) {
            vec3fArr2[i2].set(localGetSupportingVertexWithoutMargin(vec3fArr[i2]));
        }
    }

    public void getAabb(C3978xf xfVar, Vec3f vec3f, Vec3f vec3f2) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    public C3655tK arV() {
        return C3655tK.MINKOWSKI_SUM_SHAPE_PROXYTYPE;
    }

    public void calculateLocalInertia(float f, Vec3f vec3f) {
        if (!f2858jR) {
            throw new AssertionError();
        }
        vec3f.set(0.0f, 0.0f, 0.0f);
    }

    public String getName() {
        return "MinkowskiSum";
    }

    public float getMargin() {
        return this.hQn.getMargin() + this.hQo.getMargin();
    }

    /* renamed from: k */
    public void mo8926k(C3978xf xfVar) {
        this.hQl.mo22947a(xfVar);
    }

    /* renamed from: l */
    public void mo8927l(C3978xf xfVar) {
        this.hQm.mo22947a(xfVar);
    }

    /* renamed from: m */
    public void mo8928m(C3978xf xfVar) {
        xfVar.mo22947a(this.hQl);
    }

    /* renamed from: n */
    public void mo8929n(C3978xf xfVar) {
        xfVar.mo22947a(this.hQm);
    }
}
