package logic.abc;

import com.hoplon.geometry.Vec3f;
import p001a.C0647JL;
import p001a.C3655tK;
import p001a.C3978xf;

/* renamed from: a.ED */
/* compiled from: a */
public class C0312ED extends C1484Vq {
    public int cTf;

    public C0312ED(Vec3f vec3f) {
        super(vec3f);
        this.cTf = 1;
        aPc();
    }

    public C0312ED(Vec3f vec3f, boolean z) {
        super(vec3f);
    }

    public void getAabb(C3978xf xfVar, Vec3f vec3f, Vec3f vec3f2) {
        mo11986a(xfVar, vec3f, vec3f2);
    }

    /* access modifiers changed from: protected */
    /* renamed from: j */
    public Vec3f mo1815j(Vec3f vec3f, Vec3f vec3f2) {
        return m2790a(vec3f, vec3f2, 0, 1, 0, 2);
    }

    /* access modifiers changed from: protected */
    /* renamed from: k */
    public Vec3f mo1816k(Vec3f vec3f, Vec3f vec3f2) {
        return m2790a(vec3f, vec3f2, 1, 0, 1, 2);
    }

    /* access modifiers changed from: protected */
    /* renamed from: l */
    public Vec3f mo1817l(Vec3f vec3f, Vec3f vec3f2) {
        return m2790a(vec3f, vec3f2, 2, 0, 2, 1);
    }

    /* renamed from: a */
    private Vec3f m2790a(Vec3f vec3f, Vec3f vec3f2, int i, int i2, int i3, int i4) {
        Vec3f vec3f3;
        this.stack.bcH().push();
        try {
            float b = C0647JL.m5594b(vec3f, i2);
            float b2 = C0647JL.m5594b(vec3f, i);
            Vec3f vec3f4 = (Vec3f) this.stack.bcH().get();
            float sqrt = (float) Math.sqrt((double) ((C0647JL.m5594b(vec3f2, i2) * C0647JL.m5594b(vec3f2, i2)) + (C0647JL.m5594b(vec3f2, i4) * C0647JL.m5594b(vec3f2, i4))));
            if (sqrt != 0.0f) {
                float f = b / sqrt;
                C0647JL.m5590a(vec3f4, i2, C0647JL.m5594b(vec3f2, i2) * f);
                if (C0647JL.m5594b(vec3f2, i3) < 0.0f) {
                    b2 = -b2;
                }
                C0647JL.m5590a(vec3f4, i3, b2);
                C0647JL.m5590a(vec3f4, i4, C0647JL.m5594b(vec3f2, i4) * f);
                vec3f3 = (Vec3f) this.stack.bcH().mo15197aq(vec3f4);
            } else {
                C0647JL.m5590a(vec3f4, i2, b);
                if (C0647JL.m5594b(vec3f2, i3) < 0.0f) {
                    b2 = -b2;
                }
                C0647JL.m5590a(vec3f4, i3, b2);
                C0647JL.m5590a(vec3f4, i4, 0.0f);
                vec3f3 = (Vec3f) this.stack.bcH().mo15197aq(vec3f4);
                this.stack.bcH().pop();
            }
            return vec3f3;
        } finally {
            this.stack.bcH().pop();
        }
    }

    public Vec3f localGetSupportingVertexWithoutMargin(Vec3f vec3f) {
        return mo1816k(abA(), vec3f);
    }

    public void batchedUnitVectorGetSupportingVertexWithoutMargin(Vec3f[] vec3fArr, Vec3f[] vec3fArr2, int i) {
        for (int i2 = 0; i2 < i; i2++) {
            vec3fArr2[i2].set(mo1816k(abA(), vec3fArr[i2]));
        }
    }

    public Vec3f localGetSupportingVertex(Vec3f vec3f) {
        this.stack.bcH().push();
        try {
            Vec3f vec3f2 = (Vec3f) this.stack.bcH().get();
            vec3f2.set(localGetSupportingVertexWithoutMargin(vec3f));
            if (getMargin() != 0.0f) {
                Vec3f ac = this.stack.bcH().mo4458ac(vec3f);
                if (ac.lengthSquared() < 1.4210855E-14f) {
                    ac.set(-1.0f, -1.0f, -1.0f);
                }
                ac.normalize();
                vec3f2.scaleAdd(getMargin(), ac, vec3f2);
            }
            return (Vec3f) this.stack.bcH().mo15197aq(vec3f2);
        } finally {
            this.stack.bcH().pop();
        }
    }

    public C3655tK arV() {
        return C3655tK.CYLINDER_SHAPE_PROXYTYPE;
    }

    public int aOR() {
        return this.cTf;
    }

    public float getRadius() {
        return abz().x;
    }

    public String getName() {
        return "CylinderY";
    }
}
