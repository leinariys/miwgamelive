package logic.abc;

import com.hoplon.geometry.Vec3f;
import p001a.C0647JL;
import p001a.C3655tK;
import p001a.C3978xf;

/* renamed from: a.SE */
/* compiled from: a */
public class C1226SE extends C5765aVx {

    /* renamed from: jR */
    static final /* synthetic */ boolean f1527jR = (!C1226SE.class.desiredAssertionStatus());
    public final Vec3f[] eeQ = {new Vec3f(), new Vec3f(), new Vec3f()};

    public C1226SE() {
    }

    public C1226SE(Vec3f vec3f, Vec3f vec3f2, Vec3f vec3f3) {
        this.eeQ[0].set(vec3f);
        this.eeQ[1].set(vec3f2);
        this.eeQ[2].set(vec3f3);
    }

    /* renamed from: k */
    public void mo5351k(Vec3f vec3f, Vec3f vec3f2, Vec3f vec3f3) {
        this.eeQ[0].set(vec3f);
        this.eeQ[1].set(vec3f2);
        this.eeQ[2].set(vec3f3);
    }

    public int getNumVertices() {
        return 3;
    }

    /* renamed from: nj */
    public Vec3f mo5352nj(int i) {
        return this.eeQ[i];
    }

    /* renamed from: b */
    public void mo5345b(int i, Vec3f vec3f) {
        vec3f.set(this.eeQ[i]);
    }

    public C3655tK arV() {
        return C3655tK.TRIANGLE_SHAPE_PROXYTYPE;
    }

    public int abC() {
        return 3;
    }

    /* renamed from: a */
    public void mo5340a(int i, Vec3f vec3f, Vec3f vec3f2) {
        mo5345b(i, vec3f);
        mo5345b((i + 1) % 3, vec3f2);
    }

    public void getAabb(C3978xf xfVar, Vec3f vec3f, Vec3f vec3f2) {
        getAabbSlow(xfVar, vec3f, vec3f2);
    }

    public Vec3f localGetSupportingVertexWithoutMargin(Vec3f vec3f) {
        this.stack.bcH().push();
        try {
            return this.eeQ[C0647JL.m5588W(this.stack.bcH().mo4460h(vec3f.dot(this.eeQ[0]), vec3f.dot(this.eeQ[1]), vec3f.dot(this.eeQ[2])))];
        } finally {
            this.stack.bcH().pop();
        }
    }

    public void batchedUnitVectorGetSupportingVertexWithoutMargin(Vec3f[] vec3fArr, Vec3f[] vec3fArr2, int i) {
        this.stack.bcH().push();
        try {
            Vec3f vec3f = (Vec3f) this.stack.bcH().get();
            for (int i2 = 0; i2 < i; i2++) {
                Vec3f vec3f2 = vec3fArr[i2];
                vec3f.set(vec3f2.dot(this.eeQ[0]), vec3f2.dot(this.eeQ[1]), vec3f2.dot(this.eeQ[2]));
                vec3fArr2[i2].set(this.eeQ[C0647JL.m5588W(vec3f)]);
            }
        } finally {
            this.stack.bcH().pop();
        }
    }

    /* renamed from: a */
    public void mo5341a(Vec3f vec3f, Vec3f vec3f2, int i) {
        mo5346b(i, vec3f, vec3f2);
    }

    public int abB() {
        return 1;
    }

    /* renamed from: af */
    public void mo5344af(Vec3f vec3f) {
        this.stack.bcH().push();
        try {
            Vec3f vec3f2 = (Vec3f) this.stack.bcH().get();
            Vec3f vec3f3 = (Vec3f) this.stack.bcH().get();
            vec3f2.sub(this.eeQ[1], this.eeQ[0]);
            vec3f3.sub(this.eeQ[2], this.eeQ[0]);
            vec3f.cross(vec3f2, vec3f3);
            vec3f.normalize();
        } finally {
            this.stack.bcH().pop();
        }
    }

    /* renamed from: b */
    public void mo5346b(int i, Vec3f vec3f, Vec3f vec3f2) {
        mo5344af(vec3f);
        vec3f2.set(this.eeQ[0]);
    }

    public void calculateLocalInertia(float f, Vec3f vec3f) {
        if (!f1527jR) {
            throw new AssertionError();
        }
        vec3f.set(0.0f, 0.0f, 0.0f);
    }

    /* JADX INFO: finally extract failed */
    /* renamed from: b */
    public boolean mo5347b(Vec3f vec3f, float f) {
        this.stack.bcH().push();
        try {
            Vec3f vec3f2 = (Vec3f) this.stack.bcH().get();
            mo5344af(vec3f2);
            float dot = vec3f.dot(vec3f2) - this.eeQ[0].dot(vec3f2);
            if (dot >= (-f) && dot <= f) {
                int i = 0;
                while (i < 3) {
                    Vec3f vec3f3 = (Vec3f) this.stack.bcH().get();
                    Vec3f vec3f4 = (Vec3f) this.stack.bcH().get();
                    mo5340a(i, vec3f3, vec3f4);
                    Vec3f vec3f5 = (Vec3f) this.stack.bcH().get();
                    vec3f5.sub(vec3f4, vec3f3);
                    Vec3f vec3f6 = (Vec3f) this.stack.bcH().get();
                    vec3f6.cross(vec3f5, vec3f2);
                    vec3f6.normalize();
                    if (vec3f.dot(vec3f6) - vec3f3.dot(vec3f6) >= (-f)) {
                        i++;
                    }
                }
                this.stack.bcH().pop();
                return true;
            }
            this.stack.bcH().pop();
            return false;
        } catch (Throwable th) {
            this.stack.bcH().pop();
            throw th;
        }
    }

    public String getName() {
        return "Triangle";
    }

    public int getNumPreferredPenetrationDirections() {
        return 2;
    }

    public void getPreferredPenetrationDirection(int i, Vec3f vec3f) {
        mo5344af(vec3f);
        if (i != 0) {
            vec3f.scale(-1.0f);
        }
    }
}
