package logic.abc;

import com.hoplon.geometry.Vec3f;
import game.geometry.Matrix3fWrap;
import p001a.*;

import java.util.ArrayList;
import java.util.List;

/* renamed from: a.aqz  reason: case insensitive filesystem */
/* compiled from: a */
public class C6651aqz extends C0802Lc {
    public final Vec3f localScaling = new Vec3f(1.0f, 1.0f, 1.0f);
    private final Vec3f cUp = new Vec3f(1.0E30f, 1.0E30f, 1.0E30f);
    private final Vec3f cUq = new Vec3f(-1.0E30f, -1.0E30f, -1.0E30f);
    private final List<C6728asY> children = new ArrayList();
    private float collisionMargin = 0.0f;
    private C1089Pw gqL = null;

    /* renamed from: a */
    public void mo15683a(C3978xf xfVar, C0802Lc lc) {
        this.stack.bcH().push();
        try {
            C6728asY asy = new C6728asY();
            asy.gxI.mo22947a(xfVar);
            asy.gxJ = lc;
            asy.gxK = lc.arV();
            asy.gxL = lc.getMargin();
            this.children.add(asy);
            Vec3f vec3f = (Vec3f) this.stack.bcH().get();
            Vec3f vec3f2 = (Vec3f) this.stack.bcH().get();
            lc.getAabb(xfVar, vec3f, vec3f2);
            C0647JL.m5605o(this.cUp, vec3f);
            C0647JL.m5606p(this.cUq, vec3f2);
        } finally {
            this.stack.bcH().pop();
        }
    }

    public int crs() {
        return this.children.size();
    }

    /* renamed from: tI */
    public C0802Lc mo15687tI(int i) {
        return this.children.get(i).gxJ;
    }

    /* renamed from: tJ */
    public C3978xf mo15688tJ(int i) {
        return this.children.get(i).gxI;
    }

    public List<C6728asY> crt() {
        return this.children;
    }

    public void getAabb(C3978xf xfVar, Vec3f vec3f, Vec3f vec3f2) {
        this.stack.bcF();
        try {
            Vec3f vec3f3 = (Vec3f) this.stack.bcH().get();
            vec3f3.sub(this.cUq, this.cUp);
            vec3f3.scale(0.5f);
            Vec3f vec3f4 = (Vec3f) this.stack.bcH().get();
            vec3f4.add(this.cUq, this.cUp);
            vec3f4.scale(0.5f);
            Matrix3fWrap g = this.stack.bcK().mo15565g(xfVar.bFF);
            C3427rS.m38374b(g);
            Vec3f ac = this.stack.bcH().mo4458ac(vec3f4);
            xfVar.mo22946G(ac);
            Vec3f vec3f5 = (Vec3f) this.stack.bcH().get();
            Vec3f vec3f6 = (Vec3f) this.stack.bcH().get();
            g.getRow(0, vec3f5);
            vec3f6.x = vec3f5.dot(vec3f3);
            g.getRow(1, vec3f5);
            vec3f6.y = vec3f5.dot(vec3f3);
            g.getRow(2, vec3f5);
            vec3f6.z = vec3f5.dot(vec3f3);
            vec3f6.x += getMargin();
            vec3f6.y += getMargin();
            vec3f6.z += getMargin();
            vec3f.sub(ac, vec3f6);
            vec3f2.add(ac, vec3f6);
        } finally {
            this.stack.bcG();
        }
    }

    public Vec3f getLocalScaling() {
        return this.localScaling;
    }

    public void setLocalScaling(Vec3f vec3f) {
        this.localScaling.set(vec3f);
    }

    public void calculateLocalInertia(float f, Vec3f vec3f) {
        this.stack.bcF();
        try {
            C3978xf xfVar = (C3978xf) this.stack.bcJ().get();
            xfVar.setIdentity();
            Vec3f vec3f2 = (Vec3f) this.stack.bcH().get();
            Vec3f vec3f3 = (Vec3f) this.stack.bcH().get();
            getAabb(xfVar, vec3f2, vec3f3);
            Vec3f vec3f4 = (Vec3f) this.stack.bcH().get();
            vec3f4.sub(vec3f3, vec3f2);
            vec3f4.scale(0.5f);
            float f2 = vec3f4.x * 2.0f;
            float f3 = vec3f4.y * 2.0f;
            float f4 = vec3f4.z * 2.0f;
            vec3f.x = (f / 12.0f) * ((f3 * f3) + (f4 * f4));
            vec3f.y = ((f4 * f4) + (f2 * f2)) * (f / 12.0f);
            vec3f.z = (f / 12.0f) * ((f2 * f2) + (f3 * f3));
        } finally {
            this.stack.bcG();
        }
    }

    public C3655tK arV() {
        return C3655tK.COMPOUND_SHAPE_PROXYTYPE;
    }

    public float getMargin() {
        return this.collisionMargin;
    }

    public void setMargin(float f) {
        this.collisionMargin = f;
    }

    public String getName() {
        return "Compound";
    }

    public C1089Pw cru() {
        return this.gqL;
    }
}
