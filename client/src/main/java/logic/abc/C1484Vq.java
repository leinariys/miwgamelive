package logic.abc;

import com.hoplon.geometry.Vec3f;
import game.geometry.Matrix3fWrap;
import p001a.*;

import javax.vecmath.Vector4f;

/* renamed from: a.Vq */
/* compiled from: a */
public class C1484Vq extends C5765aVx {

    /* renamed from: jR */
    static final /* synthetic */ boolean f1916jR = (!C1484Vq.class.desiredAssertionStatus());

    public C1484Vq(Vec3f vec3f) {
        Vec3f vec3f2 = new Vec3f(getMargin(), getMargin(), getMargin());
        C0647JL.m5603g(this.implicitShapeDimensions, vec3f, this.localScaling);
        this.implicitShapeDimensions.sub(vec3f2);
    }

    public Vec3f abz() {
        this.stack.bcH().push();
        try {
            Vec3f ac = this.stack.bcH().mo4458ac(abA());
            ac.add(this.stack.bcH().mo4460h(getMargin(), getMargin(), getMargin()));
            return (Vec3f) this.stack.bcH().mo15197aq(ac);
        } finally {
            this.stack.bcH().pop();
        }
    }

    public Vec3f abA() {
        return this.implicitShapeDimensions;
    }

    public C3655tK arV() {
        return C3655tK.BOX_SHAPE_PROXYTYPE;
    }

    public Vec3f localGetSupportingVertex(Vec3f vec3f) {
        this.stack.bcH().push();
        try {
            Vec3f ac = this.stack.bcH().mo4458ac(abA());
            ac.add(this.stack.bcH().mo4460h(getMargin(), getMargin(), getMargin()));
            return (Vec3f) this.stack.bcH().mo15197aq(this.stack.bcH().mo4460h(C0920NU.m7655g(vec3f.x, ac.x, -ac.x), C0920NU.m7655g(vec3f.y, ac.y, -ac.y), C0920NU.m7655g(vec3f.z, ac.z, -ac.z)));
        } finally {
            this.stack.bcH().pop();
        }
    }

    public Vec3f localGetSupportingVertexWithoutMargin(Vec3f vec3f) {
        this.stack.bcH().push();
        try {
            Vec3f ac = this.stack.bcH().mo4458ac(abA());
            return (Vec3f) this.stack.bcH().mo15197aq(this.stack.bcH().mo4460h(C0920NU.m7655g(vec3f.x, ac.x, -ac.x), C0920NU.m7655g(vec3f.y, ac.y, -ac.y), C0920NU.m7655g(vec3f.z, ac.z, -ac.z)));
        } finally {
            this.stack.bcH().pop();
        }
    }

    public void batchedUnitVectorGetSupportingVertexWithoutMargin(Vec3f[] vec3fArr, Vec3f[] vec3fArr2, int i) {
        this.stack.bcH().push();
        try {
            Vec3f ac = this.stack.bcH().mo4458ac(abA());
            for (int i2 = 0; i2 < i; i2++) {
                Vec3f vec3f = vec3fArr[i2];
                vec3fArr2[i2].set(C0920NU.m7655g(vec3f.x, ac.x, -ac.x), C0920NU.m7655g(vec3f.y, ac.y, -ac.y), C0920NU.m7655g(vec3f.z, ac.z, -ac.z));
            }
        } finally {
            this.stack.bcH().pop();
        }
    }

    public void setMargin(float f) {
        this.stack.bcH().push();
        try {
            Vec3f h = this.stack.bcH().mo4460h(getMargin(), getMargin(), getMargin());
            Vec3f vec3f = (Vec3f) this.stack.bcH().get();
            vec3f.add(this.implicitShapeDimensions, h);
            super.setMargin(f);
            this.implicitShapeDimensions.sub(vec3f, this.stack.bcH().mo4460h(getMargin(), getMargin(), getMargin()));
        } finally {
            this.stack.bcH().pop();
        }
    }

    public void setLocalScaling(Vec3f vec3f) {
        this.stack.bcH().push();
        try {
            Vec3f h = this.stack.bcH().mo4460h(getMargin(), getMargin(), getMargin());
            Vec3f vec3f2 = (Vec3f) this.stack.bcH().get();
            vec3f2.add(this.implicitShapeDimensions, h);
            Vec3f vec3f3 = (Vec3f) this.stack.bcH().get();
            C0647JL.m5604h(vec3f3, vec3f2, this.localScaling);
            super.setLocalScaling(vec3f);
            C0647JL.m5603g(this.implicitShapeDimensions, vec3f3, this.localScaling);
            this.implicitShapeDimensions.sub(h);
        } finally {
            this.stack.bcH().pop();
        }
    }

    public void getAabb(C3978xf xfVar, Vec3f vec3f, Vec3f vec3f2) {
        this.stack.bcF();
        try {
            Vec3f abA = abA();
            Matrix3fWrap g = this.stack.bcK().mo15565g(xfVar.bFF);
            C3427rS.m38374b(g);
            Vec3f vec3f3 = (Vec3f) this.stack.bcH().get();
            Vec3f ac = this.stack.bcH().mo4458ac(xfVar.bFG);
            Vec3f vec3f4 = (Vec3f) this.stack.bcH().get();
            g.getRow(0, vec3f3);
            vec3f4.x = vec3f3.dot(abA);
            g.getRow(1, vec3f3);
            vec3f4.y = vec3f3.dot(abA);
            g.getRow(2, vec3f3);
            vec3f4.z = vec3f3.dot(abA);
            vec3f4.add(this.stack.bcH().mo4460h(getMargin(), getMargin(), getMargin()));
            vec3f.sub(ac, vec3f4);
            vec3f2.add(ac, vec3f4);
        } finally {
            this.stack.bcG();
        }
    }

    public void calculateLocalInertia(float f, Vec3f vec3f) {
        this.stack.bcH().push();
        try {
            Vec3f ac = this.stack.bcH().mo4458ac(abz());
            float f2 = ac.x * 2.0f;
            float f3 = ac.y * 2.0f;
            float f4 = ac.z * 2.0f;
            vec3f.set((f / 12.0f) * ((f3 * f3) + (f4 * f4)), ((f4 * f4) + (f2 * f2)) * (f / 12.0f), ((f2 * f2) + (f3 * f3)) * (f / 12.0f));
        } finally {
            this.stack.bcH().pop();
        }
    }

    /* renamed from: a */
    public void mo5341a(Vec3f vec3f, Vec3f vec3f2, int i) {
        this.stack.bcH().push();
        this.stack.bcM().push();
        try {
            Vector4f vector4f = (Vector4f) this.stack.bcM().get();
            mo6423a(vector4f, i);
            vec3f.set(vector4f.x, vector4f.y, vector4f.z);
            Vec3f vec3f3 = (Vec3f) this.stack.bcH().get();
            vec3f3.negate(vec3f);
            vec3f2.set(localGetSupportingVertex(vec3f3));
        } finally {
            this.stack.bcH().pop();
            this.stack.bcM().pop();
        }
    }

    public int abB() {
        return 6;
    }

    public int getNumVertices() {
        return 8;
    }

    public int abC() {
        return 12;
    }

    /* renamed from: b */
    public void mo5345b(int i, Vec3f vec3f) {
        Vec3f abA = abA();
        vec3f.set((abA.x * ((float) (1 - (i & 1)))) - (abA.x * ((float) (i & 1))), (abA.y * ((float) (1 - ((i & 2) >> 1)))) - (abA.y * ((float) ((i & 2) >> 1))), (abA.z * ((float) (1 - ((i & 4) >> 2)))) - (abA.z * ((float) ((i & 4) >> 2))));
    }

    /* renamed from: a */
    public void mo6423a(Vector4f vector4f, int i) {
        Vec3f abA = abA();
        switch (i) {
            case 0:
                vector4f.set(1.0f, 0.0f, 0.0f, -abA.x);
                return;
            case 1:
                vector4f.set(-1.0f, 0.0f, 0.0f, -abA.x);
                return;
            case 2:
                vector4f.set(0.0f, 1.0f, 0.0f, -abA.y);
                return;
            case 3:
                vector4f.set(0.0f, -1.0f, 0.0f, -abA.y);
                return;
            case 4:
                vector4f.set(0.0f, 0.0f, 1.0f, -abA.z);
                return;
            case 5:
                vector4f.set(0.0f, 0.0f, -1.0f, -abA.z);
                return;
            default:
                if (!f1916jR) {
                    throw new AssertionError();
                }
                return;
        }
    }

    /* renamed from: a */
    public void mo5340a(int i, Vec3f vec3f, Vec3f vec3f2) {
        int i2;
        int i3;
        switch (i) {
            case 0:
                i2 = 1;
                i3 = 0;
                break;
            case 1:
                i2 = 2;
                i3 = 0;
                break;
            case 2:
                i2 = 3;
                i3 = 1;
                break;
            case 3:
                i2 = 3;
                i3 = 2;
                break;
            case 4:
                i2 = 4;
                i3 = 0;
                break;
            case 5:
                i2 = 5;
                i3 = 1;
                break;
            case 6:
                i2 = 6;
                i3 = 2;
                break;
            case 7:
                i2 = 7;
                i3 = 3;
                break;
            case 8:
                i2 = 5;
                i3 = 4;
                break;
            case 9:
                i2 = 6;
                i3 = 4;
                break;
            case 10:
                i2 = 7;
                i3 = 5;
                break;
            case 11:
                i2 = 7;
                i3 = 6;
                break;
            default:
                if (f1916jR) {
                    i2 = 0;
                    i3 = 0;
                    break;
                } else {
                    throw new AssertionError();
                }
        }
        mo5345b(i3, vec3f);
        mo5345b(i2, vec3f2);
    }

    /* renamed from: b */
    public boolean mo5347b(Vec3f vec3f, float f) {
        Vec3f abA = abA();
        return vec3f.x <= abA.x + f && vec3f.x >= (-abA.x) - f && vec3f.y <= abA.y + f && vec3f.y >= (-abA.y) - f && vec3f.z <= abA.z + f && vec3f.z >= (-abA.z) - f;
    }

    public String getName() {
        return "Box";
    }

    public int getNumPreferredPenetrationDirections() {
        return 6;
    }

    public void getPreferredPenetrationDirection(int i, Vec3f vec3f) {
        switch (i) {
            case 0:
                vec3f.set(1.0f, 0.0f, 0.0f);
                return;
            case 1:
                vec3f.set(-1.0f, 0.0f, 0.0f);
                return;
            case 2:
                vec3f.set(0.0f, 1.0f, 0.0f);
                return;
            case 3:
                vec3f.set(0.0f, -1.0f, 0.0f);
                return;
            case 4:
                vec3f.set(0.0f, 0.0f, 1.0f);
                return;
            case 5:
                vec3f.set(0.0f, 0.0f, -1.0f);
                return;
            default:
                if (!f1916jR) {
                    throw new AssertionError();
                }
                return;
        }
    }
}
