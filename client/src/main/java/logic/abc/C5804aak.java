package logic.abc;

import com.hoplon.geometry.Vec3f;
import p001a.C0128Bb;
import p001a.C3655tK;
import p001a.C3978xf;

/* renamed from: a.aak  reason: case insensitive filesystem */
/* compiled from: a */
public class C5804aak extends C5508aMa {
    public C5804aak(float f) {
        this.implicitShapeDimensions.x = f;
    }

    public Vec3f localGetSupportingVertexWithoutMargin(Vec3f vec3f) {
        return C0128Bb.dMU;
    }

    public void batchedUnitVectorGetSupportingVertexWithoutMargin(Vec3f[] vec3fArr, Vec3f[] vec3fArr2, int i) {
        for (int i2 = 0; i2 < i; i2++) {
            vec3fArr2[i2].set(0.0f, 0.0f, 0.0f);
        }
    }

    public void getAabb(C3978xf xfVar, Vec3f vec3f, Vec3f vec3f2) {
        this.stack.bcH().push();
        try {
            Vec3f vec3f3 = xfVar.bFG;
            Vec3f h = this.stack.bcH().mo4460h(getMargin(), getMargin(), getMargin());
            vec3f.sub(vec3f3, h);
            vec3f2.add(vec3f3, h);
        } finally {
            this.stack.bcH().pop();
        }
    }

    public C3655tK arV() {
        return C3655tK.SPHERE_SHAPE_PROXYTYPE;
    }

    public void calculateLocalInertia(float f, Vec3f vec3f) {
        float margin = 0.4f * f * getMargin() * getMargin();
        vec3f.set(margin, margin, margin);
    }

    public String getName() {
        return "SPHERE";
    }

    public float getRadius() {
        return this.implicitShapeDimensions.x * this.localScaling.x;
    }

    public float getMargin() {
        return getRadius();
    }

    public void setMargin(float f) {
        super.setMargin(f);
    }
}
