package logic.abc;

import com.hoplon.geometry.Vec3f;
import game.geometry.Matrix3fWrap;
import p001a.C0647JL;
import p001a.C3427rS;
import p001a.C3978xf;

/* renamed from: a.aVx  reason: case insensitive filesystem */
/* compiled from: a */
public abstract class C5765aVx extends C5508aMa {

    /* renamed from: jR */
    static final /* synthetic */ boolean f3989jR = (!C5765aVx.class.desiredAssertionStatus());
    public final Vec3f cUp = new Vec3f(1.0f, 1.0f, 1.0f);
    public final Vec3f cUq = new Vec3f(-1.0f, -1.0f, -1.0f);
    public boolean cUr = false;

    /* renamed from: a */
    public abstract void mo5340a(int i, Vec3f vec3f, Vec3f vec3f2);

    /* renamed from: a */
    public abstract void mo5341a(Vec3f vec3f, Vec3f vec3f2, int i);

    public abstract int abB();

    public abstract int abC();

    /* renamed from: b */
    public abstract void mo5345b(int i, Vec3f vec3f);

    /* renamed from: b */
    public abstract boolean mo5347b(Vec3f vec3f, float f);

    public abstract int getNumVertices();

    public Vec3f localGetSupportingVertexWithoutMargin(Vec3f vec3f) {
        this.stack.bcH().push();
        try {
            Vec3f h = this.stack.bcH().mo4460h(0.0f, 0.0f, 0.0f);
            float f = -1.0E30f;
            Vec3f ac = this.stack.bcH().mo4458ac(vec3f);
            float lengthSquared = ac.lengthSquared();
            if (lengthSquared < 1.0E-4f) {
                ac.set(1.0f, 0.0f, 0.0f);
            } else {
                ac.scale(1.0f / ((float) Math.sqrt((double) lengthSquared)));
            }
            Vec3f vec3f2 = (Vec3f) this.stack.bcH().get();
            int i = 0;
            while (i < getNumVertices()) {
                mo5345b(i, vec3f2);
                float dot = ac.dot(vec3f2);
                if (dot > f) {
                    h = vec3f2;
                } else {
                    dot = f;
                }
                i++;
                f = dot;
            }
            return (Vec3f) this.stack.bcH().mo15197aq(h);
        } finally {
            this.stack.bcH().pop();
        }
    }

    public void batchedUnitVectorGetSupportingVertexWithoutMargin(Vec3f[] vec3fArr, Vec3f[] vec3fArr2, int i) {
        this.stack.bcH().push();
        try {
            Vec3f vec3f = (Vec3f) this.stack.bcH().get();
            float[] fArr = new float[i];
            for (int i2 = 0; i2 < i; i2++) {
                fArr[i2] = -1.0E30f;
            }
            for (int i3 = 0; i3 < i; i3++) {
                Vec3f vec3f2 = vec3fArr[i3];
                for (int i4 = 0; i4 < getNumVertices(); i4++) {
                    mo5345b(i4, vec3f);
                    float dot = vec3f2.dot(vec3f);
                    if (dot > fArr[i3]) {
                        vec3fArr2[i3].set(vec3f);
                        fArr[i3] = dot;
                    }
                }
            }
        } finally {
            this.stack.bcH().pop();
        }
    }

    public void calculateLocalInertia(float f, Vec3f vec3f) {
        this.stack.bcF();
        try {
            float margin = getMargin();
            C3978xf xfVar = (C3978xf) this.stack.bcJ().get();
            xfVar.setIdentity();
            Vec3f vec3f2 = (Vec3f) this.stack.bcH().get();
            Vec3f vec3f3 = (Vec3f) this.stack.bcH().get();
            getAabb(xfVar, vec3f2, vec3f3);
            Vec3f vec3f4 = (Vec3f) this.stack.bcH().get();
            vec3f4.sub(vec3f3, vec3f2);
            vec3f4.scale(0.5f);
            float f2 = (vec3f4.x + margin) * 2.0f;
            float f3 = (vec3f4.y + margin) * 2.0f;
            float f4 = (vec3f4.z + margin) * 2.0f;
            float f5 = f2 * f2;
            float f6 = f3 * f3;
            float f7 = f4 * f4;
            vec3f.set(f6 + f7, f7 + f5, f5 + f6);
            vec3f.scale(0.08333333f * f);
        } finally {
            this.stack.bcG();
        }
    }

    /* renamed from: a */
    private void m19217a(C3978xf xfVar, Vec3f vec3f, Vec3f vec3f2, float f) {
        this.stack.bcF();
        try {
            if (!f3989jR && !this.cUr) {
                throw new AssertionError();
            } else if (!f3989jR && this.cUp.x > this.cUq.x) {
                throw new AssertionError();
            } else if (!f3989jR && this.cUp.y > this.cUq.y) {
                throw new AssertionError();
            } else if (f3989jR || this.cUp.z <= this.cUq.z) {
                Vec3f vec3f3 = (Vec3f) this.stack.bcH().get();
                vec3f3.sub(this.cUq, this.cUp);
                vec3f3.scale(0.5f);
                Vec3f vec3f4 = (Vec3f) this.stack.bcH().get();
                vec3f4.add(this.cUq, this.cUp);
                vec3f4.scale(0.5f);
                Matrix3fWrap g = this.stack.bcK().mo15565g(xfVar.bFF);
                C3427rS.m38374b(g);
                Vec3f ac = this.stack.bcH().mo4458ac(vec3f4);
                xfVar.mo22946G(ac);
                Vec3f vec3f5 = (Vec3f) this.stack.bcH().get();
                Vec3f vec3f6 = (Vec3f) this.stack.bcH().get();
                g.getRow(0, vec3f6);
                vec3f5.x = vec3f6.dot(vec3f3);
                g.getRow(1, vec3f6);
                vec3f5.y = vec3f6.dot(vec3f3);
                g.getRow(2, vec3f6);
                vec3f5.z = vec3f6.dot(vec3f3);
                vec3f5.x += f;
                vec3f5.y += f;
                vec3f5.z += f;
                vec3f.sub(ac, vec3f5);
                vec3f2.add(ac, vec3f5);
            } else {
                throw new AssertionError();
            }
        } finally {
            this.stack.bcG();
        }
    }

    public void getAabb(C3978xf xfVar, Vec3f vec3f, Vec3f vec3f2) {
        m19217a(xfVar, vec3f, vec3f2, getMargin());
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public final void mo11986a(C3978xf xfVar, Vec3f vec3f, Vec3f vec3f2) {
        m19217a(xfVar, vec3f, vec3f2, getMargin());
    }

    public void aPc() {
        this.stack.bcH().push();
        try {
            this.cUr = true;
            for (int i = 0; i < 3; i++) {
                Vec3f h = this.stack.bcH().mo4460h(0.0f, 0.0f, 0.0f);
                C0647JL.m5590a(h, i, 1.0f);
                Vec3f ac = this.stack.bcH().mo4458ac(localGetSupportingVertex(h));
                C0647JL.m5590a(this.cUq, i, C0647JL.m5594b(ac, i) + this.collisionMargin);
                C0647JL.m5590a(h, i, -1.0f);
                ac.set(localGetSupportingVertex(h));
                C0647JL.m5590a(this.cUp, i, C0647JL.m5594b(ac, i) - this.collisionMargin);
            }
        } finally {
            this.stack.bcH().pop();
        }
    }
}
