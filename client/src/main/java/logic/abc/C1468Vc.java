package logic.abc;

import com.hoplon.geometry.Vec3f;
import p001a.aTC;

/* renamed from: a.Vc */
/* compiled from: a */
public abstract class C1468Vc extends C0802Lc {
    public float collisionMargin = 0.0f;

    /* renamed from: a */
    public abstract void mo2381a(aTC atc, Vec3f vec3f, Vec3f vec3f2);

    public float getMargin() {
        return this.collisionMargin;
    }

    public void setMargin(float f) {
        this.collisionMargin = f;
    }
}
