package logic.abc;

import com.hoplon.geometry.Vec3f;
import p001a.C0763Kt;
import p001a.C3655tK;
import p001a.C3978xf;

/* renamed from: a.Lc */
/* compiled from: a */
public abstract class C0802Lc {
    public final C0763Kt stack = C0763Kt.bcE();

    public abstract C3655tK arV();

    public abstract void calculateLocalInertia(float f, Vec3f vec3f);

    public abstract void getAabb(C3978xf xfVar, Vec3f vec3f, Vec3f vec3f2);

    public abstract Vec3f getLocalScaling();

    public abstract void setLocalScaling(Vec3f vec3f);

    public abstract float getMargin();

    public abstract void setMargin(float f);

    public abstract String getName();

    /* renamed from: a */
    public void mo3796a(Vec3f vec3f, float[] fArr) {
        this.stack.bcF();
        try {
            Vec3f vec3f2 = (Vec3f) this.stack.bcH().get();
            C3978xf xfVar = (C3978xf) this.stack.bcJ().get();
            xfVar.setIdentity();
            Vec3f vec3f3 = (Vec3f) this.stack.bcH().get();
            Vec3f vec3f4 = (Vec3f) this.stack.bcH().get();
            getAabb(xfVar, vec3f3, vec3f4);
            vec3f2.sub(vec3f4, vec3f3);
            fArr[0] = vec3f2.length() * 0.5f;
            vec3f2.add(vec3f3, vec3f4);
            vec3f.scale(0.5f, vec3f2);
        } finally {
            this.stack.bcG();
        }
    }

    public float beU() {
        this.stack.bcH().push();
        try {
            Vec3f vec3f = (Vec3f) this.stack.bcH().get();
            float[] fArr = new float[1];
            mo3796a(vec3f, fArr);
            fArr[0] = vec3f.length() + fArr[0];
            return fArr[0];
        } finally {
            this.stack.bcH().pop();
        }
    }

    /* renamed from: a */
    public void mo3795a(C3978xf xfVar, Vec3f vec3f, Vec3f vec3f2, float f, Vec3f vec3f3, Vec3f vec3f4) {
        this.stack.bcH().push();
        try {
            getAabb(xfVar, vec3f3, vec3f4);
            float f2 = vec3f4.x;
            float f3 = vec3f4.y;
            float f4 = vec3f4.z;
            float f5 = vec3f3.x;
            float f6 = vec3f3.y;
            float f7 = vec3f3.z;
            Vec3f ac = this.stack.bcH().mo4458ac(vec3f);
            ac.scale(f);
            if (ac.x > 0.0f) {
                f2 += ac.x;
            } else {
                f5 += ac.x;
            }
            if (ac.y > 0.0f) {
                f3 += ac.y;
            } else {
                f6 += ac.y;
            }
            if (ac.z > 0.0f) {
                f4 += ac.z;
            } else {
                f7 += ac.z;
            }
            float length = vec3f2.length() * beU() * f;
            Vec3f h = this.stack.bcH().mo4460h(length, length, length);
            vec3f3.set(f5, f6, f7);
            vec3f4.set(f2, f3, f4);
            vec3f3.sub(h);
            vec3f4.add(h);
        } finally {
            this.stack.bcH().pop();
        }
    }

    public boolean aiE() {
        return arV().aiE();
    }

    public boolean aiF() {
        return arV().aiF();
    }

    public boolean aiG() {
        return arV().aiG();
    }

    public boolean isCompound() {
        return arV().isCompound();
    }

    public boolean isInfinite() {
        return arV().isInfinite();
    }
}
