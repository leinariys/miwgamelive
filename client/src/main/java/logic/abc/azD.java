package logic.abc;

import com.hoplon.geometry.Vec3f;
import p001a.C3978xf;

/* renamed from: a.azD */
/* compiled from: a */
public abstract class azD extends C0802Lc {
    public abstract void batchedUnitVectorGetSupportingVertexWithoutMargin(Vec3f[] vec3fArr, Vec3f[] vec3fArr2, int i);

    public abstract void getAabbSlow(C3978xf xfVar, Vec3f vec3f, Vec3f vec3f2);

    public abstract Vec3f getLocalScaling();

    public abstract void setLocalScaling(Vec3f vec3f);

    public abstract float getMargin();

    public abstract void setMargin(float f);

    public abstract int getNumPreferredPenetrationDirections();

    public abstract void getPreferredPenetrationDirection(int i, Vec3f vec3f);

    public abstract Vec3f localGetSupportingVertex(Vec3f vec3f);

    public abstract Vec3f localGetSupportingVertexWithoutMargin(Vec3f vec3f);
}
