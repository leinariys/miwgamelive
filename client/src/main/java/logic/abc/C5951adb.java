package logic.abc;

import com.hoplon.geometry.Vec3f;

/* renamed from: a.adb  reason: case insensitive filesystem */
/* compiled from: a */
public class C5951adb extends C0312ED {
    public C5951adb(Vec3f vec3f) {
        super(vec3f, false);
        this.cTf = 0;
        aPc();
    }

    public Vec3f localGetSupportingVertexWithoutMargin(Vec3f vec3f) {
        return mo1815j(abA(), vec3f);
    }

    public void batchedUnitVectorGetSupportingVertexWithoutMargin(Vec3f[] vec3fArr, Vec3f[] vec3fArr2, int i) {
        for (int i2 = 0; i2 < i; i2++) {
            vec3fArr2[i2].set(mo1815j(abA(), vec3fArr[i2]));
        }
    }

    public float getRadius() {
        return abz().y;
    }

    public String getName() {
        return "CylinderX";
    }
}
