package logic.abc;

import com.hoplon.geometry.Vec3f;
import game.geometry.Matrix3fWrap;
import game.network.message.serializable.C5933adJ;
import p001a.*;

/* renamed from: a.Ge */
/* compiled from: a */
public abstract class C0479Ge extends C1468Vc {

    /* renamed from: jR */
    static final /* synthetic */ boolean f638jR = (!C0479Ge.class.desiredAssertionStatus());
    public final Vec3f cUp = new Vec3f();
    public final Vec3f cUq = new Vec3f();
    /* renamed from: BA */
    public C5933adJ f639BA;

    public C0479Ge(C5933adJ adj) {
        this.f639BA = adj;
    }

    public Vec3f localGetSupportingVertex(Vec3f vec3f) {
        this.stack.bcF();
        try {
            Vec3f vec3f2 = (Vec3f) this.stack.bcH().get();
            Vec3f vec3f3 = (Vec3f) this.stack.bcH().get();
            C3978xf xfVar = (C3978xf) this.stack.bcJ().get();
            xfVar.setIdentity();
            C0480a aVar = new C0480a(vec3f, xfVar);
            Vec3f h = this.stack.bcH().mo4460h(1.0E30f, 1.0E30f, 1.0E30f);
            vec3f2.negate(h);
            mo2381a(aVar, vec3f2, h);
            vec3f3.set(aVar.cnE());
            return (Vec3f) this.stack.bcH().mo15197aq(vec3f3);
        } finally {
            this.stack.bcG();
        }
    }

    public Vec3f localGetSupportingVertexWithoutMargin(Vec3f vec3f) {
        if (f638jR) {
            return localGetSupportingVertex(vec3f);
        }
        throw new AssertionError();
    }

    public void aPc() {
        this.stack.bcH().push();
        int i = 0;
        while (i < 3) {
            try {
                Vec3f h = this.stack.bcH().mo4460h(0.0f, 0.0f, 0.0f);
                C0647JL.m5590a(h, i, 1.0f);
                Vec3f ac = this.stack.bcH().mo4458ac(localGetSupportingVertex(h));
                C0647JL.m5590a(this.cUq, i, C0647JL.m5594b(ac, i) + this.collisionMargin);
                C0647JL.m5590a(h, i, -1.0f);
                ac.set(localGetSupportingVertex(h));
                C0647JL.m5590a(this.cUp, i, C0647JL.m5594b(ac, i) - this.collisionMargin);
                i++;
            } catch (Throwable th) {
                this.stack.bcH().pop();
                throw th;
            }
        }
        this.stack.bcH().pop();
    }

    public void getAabb(C3978xf xfVar, Vec3f vec3f, Vec3f vec3f2) {
        this.stack.bcF();
        try {
            Vec3f vec3f3 = (Vec3f) this.stack.bcH().get();
            Vec3f vec3f4 = (Vec3f) this.stack.bcH().get();
            vec3f4.sub(this.cUq, this.cUp);
            vec3f4.scale(0.5f);
            Vec3f vec3f5 = (Vec3f) this.stack.bcH().get();
            vec3f5.add(this.cUq, this.cUp);
            vec3f5.scale(0.5f);
            Matrix3fWrap g = this.stack.bcK().mo15565g(xfVar.bFF);
            C3427rS.m38374b(g);
            Vec3f ac = this.stack.bcH().mo4458ac(vec3f5);
            xfVar.mo22946G(ac);
            Vec3f vec3f6 = (Vec3f) this.stack.bcH().get();
            g.getRow(0, vec3f3);
            vec3f6.x = vec3f3.dot(vec3f4);
            g.getRow(1, vec3f3);
            vec3f6.y = vec3f3.dot(vec3f4);
            g.getRow(2, vec3f3);
            vec3f6.z = vec3f3.dot(vec3f4);
            vec3f6.add(this.stack.bcH().mo4460h(getMargin(), getMargin(), getMargin()));
            vec3f.sub(ac, vec3f6);
            vec3f2.add(ac, vec3f6);
        } finally {
            this.stack.bcG();
        }
    }

    /* renamed from: a */
    public void mo2381a(aTC atc, Vec3f vec3f, Vec3f vec3f2) {
        this.f639BA.mo12767a(new C0481b(atc, vec3f, vec3f2), vec3f, vec3f2);
    }

    public void calculateLocalInertia(float f, Vec3f vec3f) {
        if (!f638jR) {
            throw new AssertionError();
        }
        vec3f.set(0.0f, 0.0f, 0.0f);
    }

    public Vec3f getLocalScaling() {
        return this.f639BA.getScaling();
    }

    public void setLocalScaling(Vec3f vec3f) {
        this.f639BA.setScaling(vec3f);
        aPc();
    }

    public C5933adJ aQF() {
        return this.f639BA;
    }

    public String getName() {
        return "TRIANGLEMESH";
    }

    /* renamed from: a.Ge$b */
    /* compiled from: a */
    private static class C0481b implements C1245SP {

        public final Vec3f cam = new Vec3f();
        public final Vec3f can = new Vec3f();
        /* renamed from: BB */
        public aTC f640BB;

        public C0481b(aTC atc, Vec3f vec3f, Vec3f vec3f2) {
            this.f640BB = atc;
            this.cam.set(vec3f);
            this.can.set(vec3f2);
        }

        /* renamed from: b */
        public void mo819b(Vec3f[] vec3fArr, int i, int i2) {
            if (aRO.m17757a(vec3fArr, this.cam, this.can)) {
                this.f640BB.mo820a(vec3fArr, i, i2);
            }
        }
    }

    /* renamed from: a.Ge$a */
    private class C0480a implements aTC {
        public final C3978xf cIf = new C3978xf();
        public final Vec3f gir = new Vec3f();
        private final Vec3f gip = new Vec3f(0.0f, 0.0f, 0.0f);
        public float giq = -1.0E30f;

        public C0480a(Vec3f vec3f, C3978xf xfVar) {
            this.cIf.mo22947a(xfVar);
            C3427rS.m38372a(this.gir, vec3f, this.cIf.bFF);
        }

        /* renamed from: a */
        public void mo820a(Vec3f[] vec3fArr, int i, int i2) {
            for (int i3 = 0; i3 < 3; i3++) {
                float dot = this.gir.dot(vec3fArr[i3]);
                if (dot > this.giq) {
                    this.giq = dot;
                    this.gip.set(vec3fArr[i3]);
                }
            }
        }

        public Vec3f cnD() {
            C0479Ge.this.stack.bcH().push();
            try {
                Vec3f ac = C0479Ge.this.stack.bcH().mo4458ac(this.gip);
                this.cIf.mo22946G(ac);
                return (Vec3f) C0479Ge.this.stack.bcH().mo15197aq(ac);
            } finally {
                C0479Ge.this.stack.bcH().pop();
            }
        }

        public Vec3f cnE() {
            return this.gip;
        }
    }
}
