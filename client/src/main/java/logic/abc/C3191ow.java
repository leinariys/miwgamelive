package logic.abc;

import com.hoplon.geometry.Vec3f;
import game.network.message.serializable.C5737aUv;
import game.network.message.serializable.C5933adJ;
import p001a.*;

import java.nio.ByteBuffer;

/* renamed from: a.ow */
/* compiled from: a */
public class C3191ow extends C0479Ge {

    /* renamed from: jR */
    static final /* synthetic */ boolean f8800jR = (!C3191ow.class.desiredAssertionStatus());
    private C1089Pw bZE;
    private boolean bZF;
    private boolean bZG;
    private C1123QW<C3192a> bZH;

    public C3191ow() {
        super((C5933adJ) null);
        this.bZH = C0762Ks.m6640D(C3192a.class);
        this.bZE = null;
        this.bZG = false;
    }

    public C3191ow(C5933adJ adj, boolean z) {
        this(adj, z, true);
    }

    public C3191ow(C5933adJ adj, boolean z, boolean z2) {
        super(adj);
        this.bZH = C0762Ks.m6640D(C3192a.class);
        this.bZE = null;
        this.bZF = z;
        this.bZG = false;
        Vec3f vec3f = new Vec3f();
        Vec3f vec3f2 = new Vec3f();
        adj.mo12770x(vec3f, vec3f2);
        if (z2) {
            this.bZE = new C1089Pw();
            this.bZE.mo4889a(adj, z, vec3f, vec3f2);
            this.bZG = true;
        }
        aPc();
    }

    public C3191ow(C5933adJ adj, boolean z, Vec3f vec3f, Vec3f vec3f2) {
        this(adj, z, vec3f, vec3f2, true);
    }

    public C3191ow(C5933adJ adj, boolean z, Vec3f vec3f, Vec3f vec3f2, boolean z2) {
        super(adj);
        this.bZH = C0762Ks.m6640D(C3192a.class);
        this.bZE = null;
        this.bZF = z;
        this.bZG = false;
        if (z2) {
            this.bZE = new C1089Pw();
            this.bZE.mo4889a(adj, z, vec3f, vec3f2);
            this.bZG = true;
        }
        aPc();
    }

    public C3655tK arV() {
        return C3655tK.TRIANGLE_MESH_SHAPE_PROXYTYPE;
    }

    /* renamed from: a */
    public void mo21096a(C3541sM sMVar, Vec3f vec3f, Vec3f vec3f2) {
        C3192a aVar = this.bZH.get();
        aVar.mo21101a(sMVar, this.f639BA);
        this.bZE.mo4902c((aLG) aVar, vec3f, vec3f2);
        this.bZH.release(aVar);
    }

    /* renamed from: a */
    public void mo21095a(C5707aTr atr, Vec3f vec3f, Vec3f vec3f2, Vec3f vec3f3, Vec3f vec3f4) {
        C3192a aVar = this.bZH.get();
        aVar.mo21101a(atr, this.f639BA);
        this.bZE.mo4885a((aLG) aVar, vec3f, vec3f2, vec3f3, vec3f4);
        this.bZH.release(aVar);
    }

    /* renamed from: a */
    public void mo2381a(aTC atc, Vec3f vec3f, Vec3f vec3f2) {
        C3192a aVar = this.bZH.get();
        aVar.mo21101a(atc, this.f639BA);
        this.bZE.mo4884a((aLG) aVar, vec3f, vec3f2);
        this.bZH.release(aVar);
    }

    public void arW() {
        this.bZE.mo4886a(this.f639BA);
        aPc();
    }

    /* renamed from: h */
    public void mo21100h(Vec3f vec3f, Vec3f vec3f2) {
        this.bZE.mo4888a(this.f639BA, vec3f, vec3f2);
        C0647JL.m5605o(this.cUp, vec3f);
        C0647JL.m5606p(this.cUq, vec3f2);
    }

    public String getName() {
        return "BVHTRIANGLEMESH";
    }

    public void setLocalScaling(Vec3f vec3f) {
        this.stack.bcH().push();
        try {
            Vec3f vec3f2 = (Vec3f) this.stack.bcH().get();
            vec3f2.sub(getLocalScaling(), vec3f);
            if (vec3f2.lengthSquared() > 1.1920929E-7f) {
                super.setLocalScaling(vec3f);
                this.bZE = new C1089Pw();
                this.bZE.mo4889a(this.f639BA, this.bZF, this.cUp, this.cUq);
            }
        } finally {
            this.stack.bcH().pop();
        }
    }

    public C1089Pw arX() {
        return this.bZE;
    }

    /* renamed from: a */
    public void mo21094a(C1089Pw pw) {
        if (!f8800jR && this.bZE != null) {
            throw new AssertionError();
        } else if (f8800jR || !this.bZG) {
            this.bZE = pw;
            this.bZG = false;
        } else {
            throw new AssertionError();
        }
    }

    public boolean arY() {
        return this.bZF;
    }

    /* renamed from: a.ow$a */
    public static class C3192a implements aLG {

        /* renamed from: jR */
        static final /* synthetic */ boolean f8801jR = (!C3191ow.class.desiredAssertionStatus());

        /* renamed from: BA */
        public C5933adJ f8802BA;

        /* renamed from: BB */
        public aTC f8803BB;

        /* renamed from: BC */
        private Vec3f[] f8804BC = {new Vec3f(), new Vec3f(), new Vec3f()};

        /* renamed from: BD */
        private C5737aUv f8805BD = new C5737aUv();

        /* renamed from: a */
        public void mo21101a(aTC atc, C5933adJ adj) {
            this.f8802BA = adj;
            this.f8803BB = atc;
        }

        /* renamed from: f */
        public void mo9823f(int i, int i2) {
            int i3;
            this.f8802BA.mo11094b(this.f8805BD, i);
            ByteBuffer byteBuffer = this.f8805BD.iXl;
            int i4 = i2 * this.f8805BD.iXm;
            if (f8801jR || this.f8805BD.iXo == C6537aop.PHY_INTEGER || this.f8805BD.iXo == C6537aop.PHY_SHORT) {
                Vec3f scaling = this.f8802BA.getScaling();
                for (int i5 = 2; i5 >= 0; i5--) {
                    if (this.f8805BD.iXo == C6537aop.PHY_SHORT) {
                        i3 = byteBuffer.getShort((i5 * 2) + i4) & 65535;
                    } else {
                        i3 = byteBuffer.getInt((i5 * 4) + i4);
                    }
                    ByteBuffer byteBuffer2 = this.f8805BD.iXi;
                    int i6 = i3 * this.f8805BD.stride;
                    this.f8804BC[i5].set(byteBuffer2.getFloat(i6 + 0) * scaling.x, byteBuffer2.getFloat(i6 + 4) * scaling.y, byteBuffer2.getFloat(i6 + 8) * scaling.z);
                }
                this.f8803BB.mo820a(this.f8804BC, i, i2);
                this.f8802BA.mo11098qh(i);
                this.f8805BD.dAi();
                return;
            }
            throw new AssertionError();
        }
    }
}
