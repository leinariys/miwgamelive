package logic.abc;

import com.hoplon.geometry.Vec3f;
import p001a.*;

/* renamed from: a.RU */
/* compiled from: a */
public class C1182RU extends C1468Vc {
    public final Vec3f cUp = new Vec3f();
    public final Vec3f cUq = new Vec3f();
    public final Vec3f ebT = new Vec3f();
    public final Vec3f localScaling = new Vec3f(0.0f, 0.0f, 0.0f);
    public float ebU;

    public C1182RU(Vec3f vec3f, float f) {
        this.ebT.set(vec3f);
        this.ebU = f;
    }

    public Vec3f bsp() {
        return this.ebT;
    }

    public float bsq() {
        return this.ebU;
    }

    /* renamed from: a */
    public void mo2381a(aTC atc, Vec3f vec3f, Vec3f vec3f2) {
        this.stack.bcH().push();
        try {
            Vec3f vec3f3 = (Vec3f) this.stack.bcH().get();
            Vec3f vec3f4 = (Vec3f) this.stack.bcH().get();
            Vec3f vec3f5 = (Vec3f) this.stack.bcH().get();
            Vec3f vec3f6 = (Vec3f) this.stack.bcH().get();
            vec3f6.sub(vec3f2, vec3f);
            vec3f6.scale(0.5f);
            float length = vec3f6.length();
            Vec3f vec3f7 = (Vec3f) this.stack.bcH().get();
            vec3f7.add(vec3f2, vec3f);
            vec3f7.scale(0.5f);
            Vec3f vec3f8 = (Vec3f) this.stack.bcH().get();
            Vec3f vec3f9 = (Vec3f) this.stack.bcH().get();
            C0503Gz.m3562e(this.ebT, vec3f8, vec3f9);
            Vec3f vec3f10 = (Vec3f) this.stack.bcH().get();
            Vec3f vec3f11 = (Vec3f) this.stack.bcH().get();
            Vec3f vec3f12 = (Vec3f) this.stack.bcH().get();
            vec3f3.scale(this.ebT.dot(vec3f7) - this.ebU, this.ebT);
            vec3f12.sub(vec3f7, vec3f3);
            Vec3f[] vec3fArr = {(Vec3f) this.stack.bcH().get(), (Vec3f) this.stack.bcH().get(), (Vec3f) this.stack.bcH().get()};
            vec3f4.scale(length, vec3f8);
            vec3f5.scale(length, vec3f9);
            C0647JL.m5602f(vec3fArr[0], vec3f12, vec3f4, vec3f5);
            vec3f4.scale(length, vec3f8);
            vec3f5.scale(length, vec3f9);
            vec3f3.sub(vec3f4, vec3f5);
            C0647JL.m5601f(vec3fArr[1], vec3f12, vec3f3);
            vec3f4.scale(length, vec3f8);
            vec3f5.scale(length, vec3f9);
            vec3f3.sub(vec3f4, vec3f5);
            vec3fArr[2].sub(vec3f12, vec3f3);
            atc.mo820a(vec3fArr, 0, 0);
            vec3f4.scale(length, vec3f8);
            vec3f5.scale(length, vec3f9);
            vec3f3.sub(vec3f4, vec3f5);
            vec3fArr[0].sub(vec3f12, vec3f3);
            vec3f4.scale(length, vec3f8);
            vec3f5.scale(length, vec3f9);
            vec3f3.add(vec3f4, vec3f5);
            vec3fArr[1].sub(vec3f12, vec3f3);
            vec3f4.scale(length, vec3f8);
            vec3f5.scale(length, vec3f9);
            C0647JL.m5602f(vec3fArr[2], vec3f12, vec3f4, vec3f5);
            atc.mo820a(vec3fArr, 0, 1);
        } finally {
            this.stack.bcH().pop();
        }
    }

    public void getAabb(C3978xf xfVar, Vec3f vec3f, Vec3f vec3f2) {
        vec3f.set(-1.0E30f, -1.0E30f, -1.0E30f);
        vec3f2.set(1.0E30f, 1.0E30f, 1.0E30f);
    }

    public C3655tK arV() {
        return C3655tK.STATIC_PLANE_PROXYTYPE;
    }

    public Vec3f getLocalScaling() {
        return this.localScaling;
    }

    public void setLocalScaling(Vec3f vec3f) {
        this.localScaling.set(vec3f);
    }

    public void calculateLocalInertia(float f, Vec3f vec3f) {
        vec3f.set(0.0f, 0.0f, 0.0f);
    }

    public String getName() {
        return "STATICPLANE";
    }
}
