package logic.bbb;

import com.hoplon.geometry.BoundingBox;
import com.hoplon.geometry.Vec3f;
import game.geometry.Matrix4fWrap;
import p001a.C0647JL;
import p001a.C3978xf;

import javax.vecmath.Tuple3f;

/* renamed from: a.azP */
/* compiled from: a */
public class azP extends C0330ET {

    /* renamed from: jR */
    static final /* synthetic */ boolean f5680jR = (!azP.class.desiredAssertionStatus());

    public Vec3f[] eeQ = {new Vec3f(), new Vec3f(), new Vec3f()};

    public azP() {
    }

    public azP(Vec3f vec3f, Vec3f vec3f2, Vec3f vec3f3) {
        this.eeQ[0].set(vec3f);
        this.eeQ[1].set(vec3f2);
        this.eeQ[2].set(vec3f3);
    }

    /* renamed from: k */
    public void mo17193k(Vec3f vec3f, Vec3f vec3f2, Vec3f vec3f3) {
        this.eeQ[0].set(vec3f);
        this.eeQ[1].set(vec3f2);
        this.eeQ[2].set(vec3f3);
    }

    public int getNumVertices() {
        return 3;
    }

    /* renamed from: nj */
    public Vec3f mo17194nj(int i) {
        return this.eeQ[i];
    }

    /* renamed from: b */
    public void mo1871b(int i, Vec3f vec3f) {
        vec3f.set(this.eeQ[i]);
    }

    public int abC() {
        return 3;
    }

    /* renamed from: a */
    public void mo1865a(int i, Vec3f vec3f, Vec3f vec3f2) {
        mo1871b(i, vec3f);
        mo1871b((i + 1) % 3, vec3f2);
    }

    public void getAabb(C3978xf xfVar, Vec3f vec3f, Vec3f vec3f2) {
        getAabbSlow(xfVar, vec3f, vec3f2);
    }

    public Vec3f localGetSupportingVertexWithoutMargin(Vec3f vec3f) {
        this.stack.bcH().push();
        try {
            return this.eeQ[C0647JL.m5588W(this.stack.bcH().mo4460h(vec3f.dot(this.eeQ[0]), vec3f.dot(this.eeQ[1]), vec3f.dot(this.eeQ[2])))];
        } finally {
            this.stack.bcH().pop();
        }
    }

    public void batchedUnitVectorGetSupportingVertexWithoutMargin(Vec3f[] vec3fArr, Vec3f[] vec3fArr2, int i) {
        this.stack.bcH().push();
        try {
            Vec3f vec3f = (Vec3f) this.stack.bcH().get();
            for (int i2 = 0; i2 < i; i2++) {
                Vec3f vec3f2 = vec3fArr[i2];
                vec3f.set(vec3f2.dot(this.eeQ[0]), vec3f2.dot(this.eeQ[1]), vec3f2.dot(this.eeQ[2]));
                vec3fArr2[i2].set(this.eeQ[C0647JL.m5588W(vec3f)]);
            }
        } finally {
            this.stack.bcH().pop();
        }
    }

    /* renamed from: a */
    public void mo1867a(Vec3f vec3f, Vec3f vec3f2, int i) {
        mo17192b(i, vec3f, vec3f2);
    }

    public int abB() {
        return 1;
    }

    /* renamed from: af */
    public void mo17191af(Vec3f vec3f) {
        this.stack.bcH().push();
        try {
            Vec3f vec3f2 = (Vec3f) this.stack.bcH().get();
            Vec3f vec3f3 = (Vec3f) this.stack.bcH().get();
            vec3f2.sub(this.eeQ[1], this.eeQ[0]);
            vec3f3.sub(this.eeQ[2], this.eeQ[0]);
            vec3f.cross(vec3f2, vec3f3);
            vec3f.normalize();
        } finally {
            this.stack.bcH().pop();
        }
    }

    /* renamed from: b */
    public void mo17192b(int i, Vec3f vec3f, Vec3f vec3f2) {
        mo17191af(vec3f);
        vec3f2.set(this.eeQ[0]);
    }

    public void calculateLocalInertia(float f, Vec3f vec3f) {
        if (!f5680jR) {
            throw new AssertionError();
        }
        vec3f.set(0.0f, 0.0f, 0.0f);
    }

    /* JADX INFO: finally extract failed */
    /* renamed from: b */
    public boolean mo1872b(Vec3f vec3f, float f) {
        this.stack.bcH().push();
        try {
            Vec3f vec3f2 = (Vec3f) this.stack.bcH().get();
            mo17191af(vec3f2);
            float dot = vec3f.dot(vec3f2) - this.eeQ[0].dot(vec3f2);
            if (dot >= (-f) && dot <= f) {
                int i = 0;
                while (i < 3) {
                    Vec3f vec3f3 = (Vec3f) this.stack.bcH().get();
                    Vec3f vec3f4 = (Vec3f) this.stack.bcH().get();
                    mo1865a(i, vec3f3, vec3f4);
                    Vec3f vec3f5 = (Vec3f) this.stack.bcH().get();
                    vec3f5.sub(vec3f4, vec3f3);
                    Vec3f vec3f6 = (Vec3f) this.stack.bcH().get();
                    vec3f6.cross(vec3f5, vec3f2);
                    vec3f6.normalize();
                    if (vec3f.dot(vec3f6) - vec3f3.dot(vec3f6) >= (-f)) {
                        i++;
                    }
                }
                this.stack.bcH().pop();
                return true;
            }
            this.stack.bcH().pop();
            return false;
        } catch (Throwable th) {
            this.stack.bcH().pop();
            throw th;
        }
    }

    public int getNumPreferredPenetrationDirections() {
        return 2;
    }

    public void getPreferredPenetrationDirection(int i, Vec3f vec3f) {
        mo17191af(vec3f);
        if (i != 0) {
            vec3f.scale(-1.0f);
        }
    }

    public byte getShapeType() {
        return 5;
    }

    /* access modifiers changed from: protected */
    public void growBoundingBox(Matrix4fWrap ajk, BoundingBox boundingBox) {
        Vec3f vec3f = new Vec3f();
        ajk.mo14002b(this.eeQ[0], (Tuple3f) vec3f);
        boundingBox.addPoint(vec3f);
        ajk.mo14002b(this.eeQ[1], (Tuple3f) vec3f);
        boundingBox.addPoint(vec3f);
        ajk.mo14002b(this.eeQ[2], (Tuple3f) vec3f);
        boundingBox.addPoint(vec3f);
    }
}
