package logic.bbb;

import com.hoplon.geometry.Vec3f;
import game.geometry.Matrix4fWrap;
import p001a.C2678iT;

/* renamed from: a.aiA  reason: case insensitive filesystem */
/* compiled from: a */
public class C6184aiA implements aVJ<C3601so, C6348alI> {
    /* renamed from: a */
    public boolean mo7270a(Matrix4fWrap ajk, C3601so soVar, Matrix4fWrap ajk2, C6348alI ali, C2678iT iTVar) {
        float radius = ali.getRadius();
        Vec3f ceH = ajk2.ceH();
        ajk.mo13978B(ceH, ceH);
        float f = 0.0f;
        Vec3f aby = soVar.aby();
        float f2 = aby.x / 2.0f;
        float f3 = aby.y / 2.0f;
        float f4 = aby.z / 2.0f;
        if (ceH.x < (-f2)) {
            float f5 = f2 + ceH.x;
            f = 0.0f + (f5 * f5);
        } else if (ceH.x > f2) {
            float f6 = ceH.x - f2;
            f = 0.0f + (f6 * f6);
        }
        if (ceH.y < (-f3)) {
            float f7 = ceH.y + f3;
            f += f7 * f7;
        } else if (ceH.y > f3) {
            float f8 = ceH.y - f3;
            f += f8 * f8;
        }
        if (ceH.z < (-f4)) {
            float f9 = ceH.z + f4;
            f += f9 * f9;
        } else if (ceH.z > f4) {
            float f10 = ceH.z - f4;
            f += f10 * f10;
        }
        if (f <= radius * radius) {
            return true;
        }
        return false;
    }
}
