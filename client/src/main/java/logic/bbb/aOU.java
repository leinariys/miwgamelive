package logic.bbb;

import com.hoplon.geometry.BoundingBox;
import com.hoplon.geometry.Vec3f;
import game.geometry.Matrix4fWrap;
import p001a.C3978xf;

import javax.vecmath.Tuple3f;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

/* renamed from: a.aOU */
/* compiled from: a */
public class aOU extends C4029yK {

    private Vec3f iAp;
    private Vec3f iAq;

    public aOU(Vec3f vec3f, Vec3f vec3f2) {
        this.iAp = vec3f;
        this.iAq = vec3f2;
    }

    public aOU() {
    }

    public Vec3f dnY() {
        return this.iAp;
    }

    /* renamed from: bN */
    public void mo10608bN(Vec3f vec3f) {
        this.iAp = vec3f;
    }

    public Vec3f dnZ() {
        return this.iAq;
    }

    /* renamed from: bO */
    public void mo10609bO(Vec3f vec3f) {
        this.iAq = vec3f;
    }

    public String toString() {
        return "Line [" + this.iAp + " - " + this.iAq + "]";
    }

    public byte getShapeType() {
        return 6;
    }

    /* access modifiers changed from: protected */
    public void growBoundingBox(Matrix4fWrap ajk, BoundingBox boundingBox) {
        Vec3f vec3f = new Vec3f();
        ajk.mo14002b((Tuple3f) this.iAp, (Tuple3f) vec3f);
        boundingBox.addPoint(vec3f);
        ajk.mo14002b((Tuple3f) this.iAq, (Tuple3f) vec3f);
        boundingBox.addPoint(vec3f);
    }

    private void writeObject(ObjectOutputStream objectOutputStream) {
        objectOutputStream.writeObject(this.iAp);
        objectOutputStream.writeObject(this.iAq);
    }

    private void readObject(ObjectInputStream objectInputStream) {
        this.iAp = (Vec3f) objectInputStream.readObject();
        this.iAq = (Vec3f) objectInputStream.readObject();
    }

    public void calculateLocalInertia(float f, Vec3f vec3f) {
    }

    public void getAabb(C3978xf xfVar, Vec3f vec3f, Vec3f vec3f2) {
    }

    public Vec3f getLocalScaling() {
        return null;
    }

    public void setLocalScaling(Vec3f vec3f) {
    }
}
