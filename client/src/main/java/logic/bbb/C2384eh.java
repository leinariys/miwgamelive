package logic.bbb;

import com.hoplon.geometry.Vec3f;
import game.network.message.serializable.C5737aUv;
import game.network.message.serializable.C5933adJ;
import p001a.*;

import java.nio.ByteBuffer;

/* renamed from: a.eh */
/* compiled from: a */
public class C2384eh extends C0153Bm {

    /* renamed from: jR */
    static final /* synthetic */ boolean f7021jR = (!C2384eh.class.desiredAssertionStatus());

    private C1089Pw bZE;
    private boolean bZF;
    private boolean bZG;
    private C1123QW<C2385a> bZH;

    public C2384eh() {
        super((C5933adJ) null);
        this.bZH = C0762Ks.m6640D(C2385a.class);
        this.bZE = null;
        this.bZG = false;
    }

    public C2384eh(C5933adJ adj, boolean z) {
        super(adj);
        this.bZH = C0762Ks.m6640D(C2385a.class);
        this.bZE = null;
        this.bZF = z;
        this.bZG = false;
        Vec3f vec3f = new Vec3f();
        Vec3f vec3f2 = new Vec3f();
        adj.mo12770x(vec3f, vec3f2);
        this.bZE = new C1089Pw();
        this.bZE.mo4889a(adj, z, vec3f, vec3f2);
        this.bZG = true;
        aPc();
        setVolume(getBoundingBox().dio() * getBoundingBox().dip() * getBoundingBox().diq());
    }

    /* renamed from: a */
    public void mo18166a(C3541sM sMVar, Vec3f vec3f, Vec3f vec3f2) {
        C2385a aVar = this.bZH.get();
        aVar.mo18171a(sMVar, this.f252BA);
        this.bZE.mo4902c((aLG) aVar, vec3f, vec3f2);
        this.bZH.release(aVar);
    }

    /* renamed from: a */
    public void mo18165a(C5707aTr atr, Vec3f vec3f, Vec3f vec3f2, Vec3f vec3f3, Vec3f vec3f4) {
        C2385a aVar = this.bZH.get();
        aVar.mo18171a(atr, this.f252BA);
        this.bZE.mo4885a((aLG) aVar, vec3f, vec3f2, vec3f3, vec3f4);
        this.bZH.release(aVar);
    }

    /* renamed from: a */
    public void mo809a(aTC atc, Vec3f vec3f, Vec3f vec3f2) {
        this.stack = this.stack.bcD();
        if (this.bZH == null) {
            this.bZH = C0762Ks.m6640D(C2385a.class);
        }
        C2385a aVar = this.bZH.get();
        aVar.mo18171a(atc, this.f252BA);
        this.bZE.mo4884a((aLG) aVar, vec3f, vec3f2);
        this.bZH.release(aVar);
    }

    public void arW() {
        this.bZE.mo4886a(this.f252BA);
        aPc();
    }

    /* renamed from: h */
    public void mo18170h(Vec3f vec3f, Vec3f vec3f2) {
        this.bZE.mo4888a(this.f252BA, vec3f, vec3f2);
        C0647JL.m5605o(this.cUp, vec3f);
        C0647JL.m5606p(this.cUq, vec3f2);
    }

    public void setLocalScaling(Vec3f vec3f) {
        this.stack.bcH().push();
        try {
            Vec3f vec3f2 = (Vec3f) this.stack.bcH().get();
            vec3f2.sub(getLocalScaling(), vec3f);
            if (vec3f2.lengthSquared() > 1.1920929E-7f) {
                super.setLocalScaling(vec3f);
                this.bZE = new C1089Pw();
                this.bZE.mo4889a(this.f252BA, this.bZF, this.cUp, this.cUq);
            }
        } finally {
            this.stack.bcH().pop();
        }
    }

    public C1089Pw arX() {
        return this.bZE;
    }

    /* renamed from: a */
    public void mo18164a(C1089Pw pw) {
        if (!f7021jR && this.bZE != null) {
            throw new AssertionError();
        } else if (f7021jR || !this.bZG) {
            this.bZE = pw;
            this.bZG = false;
        } else {
            throw new AssertionError();
        }
    }

    public boolean arY() {
        return this.bZF;
    }

    public byte getShapeType() {
        return 4;
    }

    /* renamed from: a.eh$a */
    public static class C2385a implements aLG {

        /* renamed from: jR */
        static final /* synthetic */ boolean f7022jR = (!C2384eh.class.desiredAssertionStatus());

        /* renamed from: BA */
        public C5933adJ f7023BA;

        /* renamed from: BB */
        public aTC f7024BB;

        /* renamed from: BC */
        private Vec3f[] f7025BC = {new Vec3f(), new Vec3f(), new Vec3f()};

        /* renamed from: BD */
        private C5737aUv f7026BD = new C5737aUv();

        /* renamed from: a */
        public void mo18171a(aTC atc, C5933adJ adj) {
            this.f7023BA = adj;
            this.f7024BB = atc;
        }

        /* renamed from: f */
        public void mo9823f(int i, int i2) {
            int i3;
            this.f7023BA.mo11094b(this.f7026BD, i);
            ByteBuffer byteBuffer = this.f7026BD.iXl;
            int i4 = i2 * this.f7026BD.iXm;
            if (f7022jR || this.f7026BD.iXo == C6537aop.PHY_INTEGER || this.f7026BD.iXo == C6537aop.PHY_SHORT) {
                Vec3f scaling = this.f7023BA.getScaling();
                for (int i5 = 2; i5 >= 0; i5--) {
                    if (this.f7026BD.iXo == C6537aop.PHY_SHORT) {
                        i3 = byteBuffer.getShort((i5 * 2) + i4) & 65535;
                    } else {
                        i3 = byteBuffer.getInt((i5 * 4) + i4);
                    }
                    ByteBuffer byteBuffer2 = this.f7026BD.iXi;
                    int i6 = i3 * this.f7026BD.stride;
                    this.f7025BC[i5].set(byteBuffer2.getFloat(i6 + 0) * scaling.x, byteBuffer2.getFloat(i6 + 4) * scaling.y, byteBuffer2.getFloat(i6 + 8) * scaling.z);
                }
                this.f7024BB.mo820a(this.f7025BC, i, i2);
                this.f7023BA.mo11098qh(i);
                this.f7026BD.dAi();
                return;
            }
            throw new AssertionError();
        }
    }
}
