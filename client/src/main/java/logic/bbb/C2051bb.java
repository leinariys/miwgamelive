package logic.bbb;

import com.hoplon.geometry.Vec3f;
import game.geometry.Matrix4fWrap;
import p001a.*;

/* renamed from: a.bb */
/* compiled from: a */
public class C2051bb implements aVJ<C3165ob, C3165ob> {

    /* renamed from: jR */
    static final /* synthetic */ boolean f5837jR = (!C2051bb.class.desiredAssertionStatus());

    /* renamed from: mN */
    private static final float f5838mN = 1.0E-6f;
    /* renamed from: mM */
    public final C1123QW<C5852abg.C1854a> f5840mM = C0762Ks.m6640D(C5852abg.C1854a.class);
    /* renamed from: mO */
    private final Vec3f f5841mO = new Vec3f(0.0f, 0.0f, 1.0f);
    /* renamed from: mT */
    public int f5846mT = -1;
    /* renamed from: mU */
    public int f5847mU;
    /* renamed from: mV */
    public int f5848mV;
    /* renamed from: mW */
    public int f5849mW = 1;
    public C0763Kt stack = C0763Kt.bcE();
    /* renamed from: lL */
    private C6331akr f5839lL;
    /* renamed from: mP */
    private C6752asw f5842mP;
    /* renamed from: mQ */
    private C3165ob f5843mQ;
    /* renamed from: mR */
    private C3165ob f5844mR;
    /* renamed from: mS */
    private boolean f5845mS = false;

    public C2051bb(C6331akr akr, C6752asw asw) {
        this.f5842mP = asw;
        this.f5839lL = akr;
    }

    /* JADX WARNING: Removed duplicated region for block: B:77:0x0429 A[Catch:{ all -> 0x0212 }] */
    /* renamed from: a */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void mo17316a(p001a.C5852abg.C1854a r25, p001a.C5852abg.C1855b r26) {
        /*
            r24 = this;
            r0 = r24
            a.Kt r2 = r0.stack
            a.Kt r2 = r2.bcD()
            r0 = r24
            r0.stack = r2
            r0 = r24
            a.Kt r2 = r0.stack
            r2.bcF()
            r0 = r24
            a.Kt r2 = r0.stack     // Catch:{ all -> 0x0212 }
            a.OY r2 = r2.bcH()     // Catch:{ all -> 0x0212 }
            java.lang.Object r2 = r2.get()     // Catch:{ all -> 0x0212 }
            r0 = r2
            com.hoplon.geometry.Vec3f r0 = (com.hoplon.geometry.Vec3f) r0     // Catch:{ all -> 0x0212 }
            r11 = r0
            r16 = 0
            r0 = r24
            a.Kt r2 = r0.stack     // Catch:{ all -> 0x0212 }
            a.OY r2 = r2.bcH()     // Catch:{ all -> 0x0212 }
            r3 = 0
            r4 = 0
            r5 = 0
            com.hoplon.geometry.Vec3f r17 = r2.mo4460h(r3, r4, r5)     // Catch:{ all -> 0x0212 }
            r0 = r24
            a.Kt r2 = r0.stack     // Catch:{ all -> 0x0212 }
            a.OY r2 = r2.bcH()     // Catch:{ all -> 0x0212 }
            java.lang.Object r2 = r2.get()     // Catch:{ all -> 0x0212 }
            r0 = r2
            com.hoplon.geometry.Vec3f r0 = (com.hoplon.geometry.Vec3f) r0     // Catch:{ all -> 0x0212 }
            r12 = r0
            r0 = r24
            a.Kt r2 = r0.stack     // Catch:{ all -> 0x0212 }
            a.OY r2 = r2.bcH()     // Catch:{ all -> 0x0212 }
            java.lang.Object r2 = r2.get()     // Catch:{ all -> 0x0212 }
            r0 = r2
            com.hoplon.geometry.Vec3f r0 = (com.hoplon.geometry.Vec3f) r0     // Catch:{ all -> 0x0212 }
            r13 = r0
            r0 = r24
            a.Kt r2 = r0.stack     // Catch:{ all -> 0x0212 }
            a.Cg r2 = r2.bcJ()     // Catch:{ all -> 0x0212 }
            r0 = r25
            a.xf r3 = r0.fGA     // Catch:{ all -> 0x0212 }
            a.xf r6 = r2.mo1157d((p001a.C3978xf) r3)     // Catch:{ all -> 0x0212 }
            r0 = r24
            a.Kt r2 = r0.stack     // Catch:{ all -> 0x0212 }
            a.Cg r2 = r2.bcJ()     // Catch:{ all -> 0x0212 }
            r0 = r25
            a.xf r3 = r0.fGB     // Catch:{ all -> 0x0212 }
            a.xf r7 = r2.mo1157d((p001a.C3978xf) r3)     // Catch:{ all -> 0x0212 }
            r0 = r24
            a.Kt r2 = r0.stack     // Catch:{ all -> 0x0212 }
            a.OY r2 = r2.bcH()     // Catch:{ all -> 0x0212 }
            java.lang.Object r2 = r2.get()     // Catch:{ all -> 0x0212 }
            r0 = r2
            com.hoplon.geometry.Vec3f r0 = (com.hoplon.geometry.Vec3f) r0     // Catch:{ all -> 0x0212 }
            r14 = r0
            com.hoplon.geometry.Vec3f r2 = r6.bFG     // Catch:{ all -> 0x0212 }
            com.hoplon.geometry.Vec3f r3 = r7.bFG     // Catch:{ all -> 0x0212 }
            r14.add(r2, r3)     // Catch:{ all -> 0x0212 }
            r2 = 1056964608(0x3f000000, float:0.5)
            r14.scale(r2)     // Catch:{ all -> 0x0212 }
            com.hoplon.geometry.Vec3f r2 = r6.bFG     // Catch:{ all -> 0x0212 }
            r2.sub(r14)     // Catch:{ all -> 0x0212 }
            com.hoplon.geometry.Vec3f r2 = r7.bFG     // Catch:{ all -> 0x0212 }
            r2.sub(r14)     // Catch:{ all -> 0x0212 }
            r0 = r24
            a.ob r2 = r0.f5843mQ     // Catch:{ all -> 0x0212 }
            float r3 = r2.getMargin()     // Catch:{ all -> 0x0212 }
            r0 = r24
            a.ob r2 = r0.f5844mR     // Catch:{ all -> 0x0212 }
            float r2 = r2.getMargin()     // Catch:{ all -> 0x0212 }
            int r4 = p001a.C0128Bb.dMJ     // Catch:{ all -> 0x0212 }
            int r4 = r4 + 1
            p001a.C0128Bb.dMJ = r4     // Catch:{ all -> 0x0212 }
            r0 = r24
            boolean r4 = r0.f5845mS     // Catch:{ all -> 0x0212 }
            if (r4 == 0) goto L_0x045a
            r3 = 0
            r2 = 0
            r9 = r2
            r10 = r3
        L_0x00ba:
            r2 = 0
            r0 = r24
            r0.f5847mU = r2     // Catch:{ all -> 0x0212 }
            r18 = 1000(0x3e8, float:1.401E-42)
            r0 = r24
            com.hoplon.geometry.Vec3f r2 = r0.f5841mO     // Catch:{ all -> 0x0212 }
            r3 = 0
            r4 = 1065353216(0x3f800000, float:1.0)
            r5 = 0
            r2.set(r3, r4, r5)     // Catch:{ all -> 0x0212 }
            r15 = 0
            r8 = 0
            r5 = 1
            r2 = 0
            r0 = r24
            r0.f5848mV = r2     // Catch:{ all -> 0x0212 }
            r2 = -1
            r0 = r24
            r0.f5846mT = r2     // Catch:{ all -> 0x0212 }
            r4 = 2139095039(0x7f7fffff, float:3.4028235E38)
            float r19 = r10 + r9
            r0 = r24
            a.akr r2 = r0.f5839lL     // Catch:{ all -> 0x0212 }
            r2.reset()     // Catch:{ all -> 0x0212 }
        L_0x00e5:
            r0 = r24
            a.Kt r2 = r0.stack     // Catch:{ all -> 0x0212 }
            a.OY r2 = r2.bcH()     // Catch:{ all -> 0x0212 }
            java.lang.Object r2 = r2.get()     // Catch:{ all -> 0x0212 }
            com.hoplon.geometry.Vec3f r2 = (com.hoplon.geometry.Vec3f) r2     // Catch:{ all -> 0x0212 }
            r0 = r24
            com.hoplon.geometry.Vec3f r3 = r0.f5841mO     // Catch:{ all -> 0x0212 }
            r2.negate(r3)     // Catch:{ all -> 0x0212 }
            r0 = r25
            a.xf r3 = r0.fGA     // Catch:{ all -> 0x0212 }
            a.ajD r3 = r3.bFF     // Catch:{ all -> 0x0212 }
            p001a.C3427rS.m38372a((com.hoplon.geometry.Vec3f) r2, (com.hoplon.geometry.Vec3f) r2, (p001a.C6239ajD) r3)     // Catch:{ all -> 0x0212 }
            r0 = r24
            a.Kt r3 = r0.stack     // Catch:{ all -> 0x0212 }
            a.OY r3 = r3.bcH()     // Catch:{ all -> 0x0212 }
            java.lang.Object r3 = r3.get()     // Catch:{ all -> 0x0212 }
            com.hoplon.geometry.Vec3f r3 = (com.hoplon.geometry.Vec3f) r3     // Catch:{ all -> 0x0212 }
            r0 = r24
            com.hoplon.geometry.Vec3f r0 = r0.f5841mO     // Catch:{ all -> 0x0212 }
            r20 = r0
            r0 = r20
            r3.set(r0)     // Catch:{ all -> 0x0212 }
            r0 = r25
            a.xf r0 = r0.fGB     // Catch:{ all -> 0x0212 }
            r20 = r0
            r0 = r20
            a.ajD r0 = r0.bFF     // Catch:{ all -> 0x0212 }
            r20 = r0
            r0 = r20
            p001a.C3427rS.m38372a((com.hoplon.geometry.Vec3f) r3, (com.hoplon.geometry.Vec3f) r3, (p001a.C6239ajD) r0)     // Catch:{ all -> 0x0212 }
            r0 = r24
            a.Kt r0 = r0.stack     // Catch:{ all -> 0x0212 }
            r20 = r0
            a.OY r20 = r20.bcH()     // Catch:{ all -> 0x0212 }
            r0 = r24
            a.ob r0 = r0.f5843mQ     // Catch:{ all -> 0x0212 }
            r21 = r0
            r0 = r21
            com.hoplon.geometry.Vec3f r2 = r0.localGetSupportingVertexWithoutMargin(r2)     // Catch:{ all -> 0x0212 }
            r0 = r20
            com.hoplon.geometry.Vec3f r2 = r0.mo4458ac(r2)     // Catch:{ all -> 0x0212 }
            r0 = r24
            a.Kt r0 = r0.stack     // Catch:{ all -> 0x0212 }
            r20 = r0
            a.OY r20 = r20.bcH()     // Catch:{ all -> 0x0212 }
            r0 = r24
            a.ob r0 = r0.f5844mR     // Catch:{ all -> 0x0212 }
            r21 = r0
            r0 = r21
            com.hoplon.geometry.Vec3f r3 = r0.localGetSupportingVertexWithoutMargin(r3)     // Catch:{ all -> 0x0212 }
            r0 = r20
            com.hoplon.geometry.Vec3f r3 = r0.mo4458ac(r3)     // Catch:{ all -> 0x0212 }
            r0 = r24
            a.Kt r0 = r0.stack     // Catch:{ all -> 0x0212 }
            r20 = r0
            a.OY r20 = r20.bcH()     // Catch:{ all -> 0x0212 }
            r0 = r20
            com.hoplon.geometry.Vec3f r20 = r0.mo4458ac(r2)     // Catch:{ all -> 0x0212 }
            r0 = r20
            r6.mo22946G(r0)     // Catch:{ all -> 0x0212 }
            r0 = r24
            a.Kt r2 = r0.stack     // Catch:{ all -> 0x0212 }
            a.OY r2 = r2.bcH()     // Catch:{ all -> 0x0212 }
            com.hoplon.geometry.Vec3f r3 = r2.mo4458ac(r3)     // Catch:{ all -> 0x0212 }
            r7.mo22946G(r3)     // Catch:{ all -> 0x0212 }
            r0 = r24
            a.Kt r2 = r0.stack     // Catch:{ all -> 0x0212 }
            a.OY r2 = r2.bcH()     // Catch:{ all -> 0x0212 }
            java.lang.Object r2 = r2.get()     // Catch:{ all -> 0x0212 }
            com.hoplon.geometry.Vec3f r2 = (com.hoplon.geometry.Vec3f) r2     // Catch:{ all -> 0x0212 }
            r0 = r20
            r2.sub(r0, r3)     // Catch:{ all -> 0x0212 }
            r0 = r24
            com.hoplon.geometry.Vec3f r0 = r0.f5841mO     // Catch:{ all -> 0x0212 }
            r21 = r0
            r0 = r21
            float r21 = r0.dot(r2)     // Catch:{ all -> 0x0212 }
            r22 = 0
            int r22 = (r21 > r22 ? 1 : (r21 == r22 ? 0 : -1))
            if (r22 <= 0) goto L_0x021b
            float r22 = r21 * r21
            r0 = r25
            float r0 = r0.fGC     // Catch:{ all -> 0x0212 }
            r23 = r0
            float r23 = r23 * r4
            int r22 = (r22 > r23 ? 1 : (r22 == r23 ? 0 : -1))
            if (r22 <= 0) goto L_0x021b
            r3 = 0
            r2 = r4
        L_0x01be:
            if (r8 == 0) goto L_0x037c
            r0 = r24
            a.akr r4 = r0.f5839lL     // Catch:{ all -> 0x0212 }
            r4.mo1844D(r12, r13)     // Catch:{ all -> 0x0212 }
            r0 = r17
            r0.sub(r12, r13)     // Catch:{ all -> 0x0212 }
            r0 = r24
            com.hoplon.geometry.Vec3f r4 = r0.f5841mO     // Catch:{ all -> 0x0212 }
            float r4 = r4.lengthSquared()     // Catch:{ all -> 0x0212 }
            r5 = 953267991(0x38d1b717, float:1.0E-4)
            int r5 = (r4 > r5 ? 1 : (r4 == r5 ? 0 : -1))
            if (r5 >= 0) goto L_0x01e0
            r5 = 5
            r0 = r24
            r0.f5848mV = r5     // Catch:{ all -> 0x0212 }
        L_0x01e0:
            r5 = 679477248(0x28800000, float:1.4210855E-14)
            int r5 = (r4 > r5 ? 1 : (r4 == r5 ? 0 : -1))
            if (r5 <= 0) goto L_0x043b
            r5 = 1065353216(0x3f800000, float:1.0)
            double r0 = (double) r4     // Catch:{ all -> 0x0212 }
            r20 = r0
            double r20 = java.lang.Math.sqrt(r20)     // Catch:{ all -> 0x0212 }
            r0 = r20
            float r4 = (float) r0     // Catch:{ all -> 0x0212 }
            float r4 = r5 / r4
            r0 = r17
            r0.scale(r4)     // Catch:{ all -> 0x0212 }
            double r0 = (double) r2     // Catch:{ all -> 0x0212 }
            r20 = r0
            double r20 = java.lang.Math.sqrt(r20)     // Catch:{ all -> 0x0212 }
            r0 = r20
            float r2 = (float) r0     // Catch:{ all -> 0x0212 }
            boolean r5 = f5837jR     // Catch:{ all -> 0x0212 }
            if (r5 != 0) goto L_0x0359
            r5 = 0
            int r5 = (r2 > r5 ? 1 : (r2 == r5 ? 0 : -1))
            if (r5 > 0) goto L_0x0359
            java.lang.AssertionError r2 = new java.lang.AssertionError     // Catch:{ all -> 0x0212 }
            r2.<init>()     // Catch:{ all -> 0x0212 }
            throw r2     // Catch:{ all -> 0x0212 }
        L_0x0212:
            r2 = move-exception
            r0 = r24
            a.Kt r3 = r0.stack
            r3.bcG()
            throw r2
        L_0x021b:
            r0 = r24
            a.akr r0 = r0.f5839lL     // Catch:{ all -> 0x0212 }
            r22 = r0
            r0 = r22
            boolean r22 = r0.mo1850aL(r2)     // Catch:{ all -> 0x0212 }
            if (r22 == 0) goto L_0x0232
            r2 = 1
            r0 = r24
            r0.f5848mV = r2     // Catch:{ all -> 0x0212 }
            r8 = 1
            r2 = r4
            r3 = r5
            goto L_0x01be
        L_0x0232:
            float r21 = r4 - r21
            r22 = 897988541(0x358637bd, float:1.0E-6)
            float r22 = r22 * r4
            int r22 = (r21 > r22 ? 1 : (r21 == r22 ? 0 : -1))
            if (r22 > 0) goto L_0x024c
            r2 = 0
            int r2 = (r21 > r2 ? 1 : (r21 == r2 ? 0 : -1))
            if (r2 > 0) goto L_0x0247
            r2 = 2
            r0 = r24
            r0.f5848mV = r2     // Catch:{ all -> 0x0212 }
        L_0x0247:
            r8 = 1
            r2 = r4
            r3 = r5
            goto L_0x01be
        L_0x024c:
            r0 = r24
            a.akr r0 = r0.f5839lL     // Catch:{ all -> 0x0212 }
            r21 = r0
            r0 = r21
            r1 = r20
            r0.mo1858p(r2, r1, r3)     // Catch:{ all -> 0x0212 }
            r0 = r24
            a.akr r2 = r0.f5839lL     // Catch:{ all -> 0x0212 }
            r0 = r24
            com.hoplon.geometry.Vec3f r3 = r0.f5841mO     // Catch:{ all -> 0x0212 }
            boolean r2 = r2.mo1849aK(r3)     // Catch:{ all -> 0x0212 }
            if (r2 != 0) goto L_0x0271
            r2 = 3
            r0 = r24
            r0.f5848mV = r2     // Catch:{ all -> 0x0212 }
            r8 = 1
            r2 = r4
            r3 = r5
            goto L_0x01be
        L_0x0271:
            r0 = r24
            com.hoplon.geometry.Vec3f r2 = r0.f5841mO     // Catch:{ all -> 0x0212 }
            float r2 = r2.lengthSquared()     // Catch:{ all -> 0x0212 }
            float r3 = r4 - r2
            r20 = 872415232(0x34000000, float:1.1920929E-7)
            float r4 = r4 * r20
            int r3 = (r3 > r4 ? 1 : (r3 == r4 ? 0 : -1))
            if (r3 > 0) goto L_0x0293
            r0 = r24
            a.akr r3 = r0.f5839lL     // Catch:{ all -> 0x0212 }
            r0 = r24
            com.hoplon.geometry.Vec3f r4 = r0.f5841mO     // Catch:{ all -> 0x0212 }
            r3.mo1851aM(r4)     // Catch:{ all -> 0x0212 }
            r4 = 1
            r3 = r5
            r8 = r4
            goto L_0x01be
        L_0x0293:
            r0 = r24
            int r3 = r0.f5847mU     // Catch:{ all -> 0x0212 }
            int r4 = r3 + 1
            r0 = r24
            r0.f5847mU = r4     // Catch:{ all -> 0x0212 }
            r0 = r18
            if (r3 <= r0) goto L_0x033c
            java.io.PrintStream r3 = java.lang.System.err     // Catch:{ all -> 0x0212 }
            java.lang.String r4 = "btGjkPairDetector maxIter exceeded:%d\n"
            r18 = 1
            r0 = r18
            java.lang.Object[] r0 = new java.lang.Object[r0]     // Catch:{ all -> 0x0212 }
            r18 = r0
            r20 = 0
            r0 = r24
            int r0 = r0.f5847mU     // Catch:{ all -> 0x0212 }
            r21 = r0
            java.lang.Integer r21 = java.lang.Integer.valueOf(r21)     // Catch:{ all -> 0x0212 }
            r18[r20] = r21     // Catch:{ all -> 0x0212 }
            r0 = r18
            r3.printf(r4, r0)     // Catch:{ all -> 0x0212 }
            java.io.PrintStream r3 = java.lang.System.err     // Catch:{ all -> 0x0212 }
            java.lang.String r4 = "sepAxis=(%f,%f,%f), squaredDistance = %f, shapeTypeA=%d,shapeTypeB=%d\n"
            r18 = 6
            r0 = r18
            java.lang.Object[] r0 = new java.lang.Object[r0]     // Catch:{ all -> 0x0212 }
            r18 = r0
            r20 = 0
            r0 = r24
            com.hoplon.geometry.Vec3f r0 = r0.f5841mO     // Catch:{ all -> 0x0212 }
            r21 = r0
            r0 = r21
            float r0 = r0.x     // Catch:{ all -> 0x0212 }
            r21 = r0
            java.lang.Float r21 = java.lang.Float.valueOf(r21)     // Catch:{ all -> 0x0212 }
            r18[r20] = r21     // Catch:{ all -> 0x0212 }
            r20 = 1
            r0 = r24
            com.hoplon.geometry.Vec3f r0 = r0.f5841mO     // Catch:{ all -> 0x0212 }
            r21 = r0
            r0 = r21
            float r0 = r0.y     // Catch:{ all -> 0x0212 }
            r21 = r0
            java.lang.Float r21 = java.lang.Float.valueOf(r21)     // Catch:{ all -> 0x0212 }
            r18[r20] = r21     // Catch:{ all -> 0x0212 }
            r20 = 2
            r0 = r24
            com.hoplon.geometry.Vec3f r0 = r0.f5841mO     // Catch:{ all -> 0x0212 }
            r21 = r0
            r0 = r21
            float r0 = r0.z     // Catch:{ all -> 0x0212 }
            r21 = r0
            java.lang.Float r21 = java.lang.Float.valueOf(r21)     // Catch:{ all -> 0x0212 }
            r18[r20] = r21     // Catch:{ all -> 0x0212 }
            r20 = 3
            java.lang.Float r21 = java.lang.Float.valueOf(r2)     // Catch:{ all -> 0x0212 }
            r18[r20] = r21     // Catch:{ all -> 0x0212 }
            r20 = 4
            r0 = r24
            a.ob r0 = r0.f5843mQ     // Catch:{ all -> 0x0212 }
            r21 = r0
            byte r21 = r21.getShapeType()     // Catch:{ all -> 0x0212 }
            java.lang.Byte r21 = java.lang.Byte.valueOf(r21)     // Catch:{ all -> 0x0212 }
            r18[r20] = r21     // Catch:{ all -> 0x0212 }
            r20 = 5
            r0 = r24
            a.ob r0 = r0.f5844mR     // Catch:{ all -> 0x0212 }
            r21 = r0
            byte r21 = r21.getShapeType()     // Catch:{ all -> 0x0212 }
            java.lang.Byte r21 = java.lang.Byte.valueOf(r21)     // Catch:{ all -> 0x0212 }
            r18[r20] = r21     // Catch:{ all -> 0x0212 }
            r0 = r18
            r3.printf(r4, r0)     // Catch:{ all -> 0x0212 }
            r3 = r5
            goto L_0x01be
        L_0x033c:
            r0 = r24
            a.akr r3 = r0.f5839lL     // Catch:{ all -> 0x0212 }
            boolean r3 = r3.cgj()     // Catch:{ all -> 0x0212 }
            if (r3 == 0) goto L_0x0357
            r3 = 0
        L_0x0347:
            if (r3 != 0) goto L_0x0457
            r0 = r24
            a.akr r3 = r0.f5839lL     // Catch:{ all -> 0x0212 }
            r0 = r24
            com.hoplon.geometry.Vec3f r4 = r0.f5841mO     // Catch:{ all -> 0x0212 }
            r3.mo1851aM(r4)     // Catch:{ all -> 0x0212 }
            r3 = r5
            goto L_0x01be
        L_0x0357:
            r3 = 1
            goto L_0x0347
        L_0x0359:
            float r5 = r10 / r2
            r0 = r24
            com.hoplon.geometry.Vec3f r8 = r0.f5841mO     // Catch:{ all -> 0x0212 }
            r11.scale(r5, r8)     // Catch:{ all -> 0x0212 }
            r12.sub(r11)     // Catch:{ all -> 0x0212 }
            float r2 = r9 / r2
            r0 = r24
            com.hoplon.geometry.Vec3f r5 = r0.f5841mO     // Catch:{ all -> 0x0212 }
            r11.scale(r2, r5)     // Catch:{ all -> 0x0212 }
            r13.add(r11)     // Catch:{ all -> 0x0212 }
            r2 = 1065353216(0x3f800000, float:1.0)
            float r2 = r2 / r4
            float r16 = r2 - r19
            r15 = 1
            r2 = 1
            r0 = r24
            r0.f5846mT = r2     // Catch:{ all -> 0x0212 }
        L_0x037c:
            r0 = r24
            int r2 = r0.f5849mW     // Catch:{ all -> 0x0212 }
            if (r2 == 0) goto L_0x0442
            r0 = r24
            a.asw r2 = r0.f5842mP     // Catch:{ all -> 0x0212 }
            if (r2 == 0) goto L_0x0442
            r0 = r24
            int r2 = r0.f5848mV     // Catch:{ all -> 0x0212 }
            if (r2 == 0) goto L_0x0442
            float r2 = r16 + r19
            r4 = 1008981770(0x3c23d70a, float:0.01)
            int r2 = (r2 > r4 ? 1 : (r2 == r4 ? 0 : -1))
            if (r2 >= 0) goto L_0x0442
            r2 = 1
        L_0x0398:
            if (r3 == 0) goto L_0x0453
            if (r15 == 0) goto L_0x039e
            if (r2 == 0) goto L_0x0453
        L_0x039e:
            r0 = r24
            a.asw r2 = r0.f5842mP     // Catch:{ all -> 0x0212 }
            if (r2 == 0) goto L_0x0453
            r0 = r24
            a.Kt r2 = r0.stack     // Catch:{ all -> 0x0212 }
            a.OY r2 = r2.bcH()     // Catch:{ all -> 0x0212 }
            java.lang.Object r9 = r2.get()     // Catch:{ all -> 0x0212 }
            com.hoplon.geometry.Vec3f r9 = (com.hoplon.geometry.Vec3f) r9     // Catch:{ all -> 0x0212 }
            r0 = r24
            a.Kt r2 = r0.stack     // Catch:{ all -> 0x0212 }
            a.OY r2 = r2.bcH()     // Catch:{ all -> 0x0212 }
            java.lang.Object r10 = r2.get()     // Catch:{ all -> 0x0212 }
            com.hoplon.geometry.Vec3f r10 = (com.hoplon.geometry.Vec3f) r10     // Catch:{ all -> 0x0212 }
            int r2 = p001a.C0128Bb.dMI     // Catch:{ all -> 0x0212 }
            int r2 = r2 + 1
            p001a.C0128Bb.dMI = r2     // Catch:{ all -> 0x0212 }
            r0 = r24
            a.asw r2 = r0.f5842mP     // Catch:{ all -> 0x0212 }
            r0 = r24
            a.akr r3 = r0.f5839lL     // Catch:{ all -> 0x0212 }
            r0 = r24
            a.ob r4 = r0.f5843mQ     // Catch:{ all -> 0x0212 }
            r0 = r24
            a.ob r5 = r0.f5844mR     // Catch:{ all -> 0x0212 }
            r0 = r24
            com.hoplon.geometry.Vec3f r8 = r0.f5841mO     // Catch:{ all -> 0x0212 }
            boolean r2 = r2.mo9106a(r3, r4, r5, r6, r7, r8, r9, r10)     // Catch:{ all -> 0x0212 }
            if (r2 == 0) goto L_0x044e
            r0 = r24
            a.Kt r2 = r0.stack     // Catch:{ all -> 0x0212 }
            a.OY r2 = r2.bcH()     // Catch:{ all -> 0x0212 }
            java.lang.Object r2 = r2.get()     // Catch:{ all -> 0x0212 }
            com.hoplon.geometry.Vec3f r2 = (com.hoplon.geometry.Vec3f) r2     // Catch:{ all -> 0x0212 }
            r2.sub(r10, r9)     // Catch:{ all -> 0x0212 }
            float r3 = r2.lengthSquared()     // Catch:{ all -> 0x0212 }
            r4 = 679477248(0x28800000, float:1.4210855E-14)
            int r4 = (r3 > r4 ? 1 : (r3 == r4 ? 0 : -1))
            if (r4 <= 0) goto L_0x0445
            r4 = 1065353216(0x3f800000, float:1.0)
            double r6 = (double) r3     // Catch:{ all -> 0x0212 }
            double r6 = java.lang.Math.sqrt(r6)     // Catch:{ all -> 0x0212 }
            float r3 = (float) r6     // Catch:{ all -> 0x0212 }
            float r3 = r4 / r3
            r2.scale(r3)     // Catch:{ all -> 0x0212 }
            r11.sub(r9, r10)     // Catch:{ all -> 0x0212 }
            float r3 = r11.length()     // Catch:{ all -> 0x0212 }
            float r3 = -r3
            if (r15 == 0) goto L_0x0416
            int r4 = (r3 > r16 ? 1 : (r3 == r16 ? 0 : -1))
            if (r4 >= 0) goto L_0x0453
        L_0x0416:
            r12.set(r9)     // Catch:{ all -> 0x0212 }
            r13.set(r10)     // Catch:{ all -> 0x0212 }
            r0 = r17
            r0.set(r2)     // Catch:{ all -> 0x0212 }
            r2 = 1
            r4 = 3
            r0 = r24
            r0.f5846mT = r4     // Catch:{ all -> 0x0212 }
        L_0x0427:
            if (r2 == 0) goto L_0x0433
            r11.add(r13, r14)     // Catch:{ all -> 0x0212 }
            r0 = r26
            r1 = r17
            r0.mo5217c(r1, r11, r3)     // Catch:{ all -> 0x0212 }
        L_0x0433:
            r0 = r24
            a.Kt r2 = r0.stack
            r2.bcG()
            return
        L_0x043b:
            r2 = 2
            r0 = r24
            r0.f5846mT = r2     // Catch:{ all -> 0x0212 }
            goto L_0x037c
        L_0x0442:
            r2 = 0
            goto L_0x0398
        L_0x0445:
            r2 = 4
            r0 = r24
            r0.f5846mT = r2     // Catch:{ all -> 0x0212 }
            r2 = r15
            r3 = r16
            goto L_0x0427
        L_0x044e:
            r2 = 5
            r0 = r24
            r0.f5846mT = r2     // Catch:{ all -> 0x0212 }
        L_0x0453:
            r2 = r15
            r3 = r16
            goto L_0x0427
        L_0x0457:
            r4 = r2
            goto L_0x00e5
        L_0x045a:
            r9 = r2
            r10 = r3
            goto L_0x00ba
        */
        throw new UnsupportedOperationException("Method not decompiled: logic.bbb.C2051bb.mo17316a(a.abg$a, a.abg$b):void");
    }

    /* renamed from: a */
    public void mo17318a(C3165ob obVar) {
        this.f5843mQ = obVar;
    }

    /* renamed from: b */
    public void mo17320b(C3165ob obVar) {
        this.f5844mR = obVar;
    }

    /* renamed from: d */
    public void mo17321d(Vec3f vec3f) {
        this.f5841mO.set(vec3f);
    }

    /* renamed from: a */
    public void mo17317a(C6752asw asw) {
        this.f5842mP = asw;
    }

    /* renamed from: v */
    public void mo17322v(boolean z) {
        this.f5845mS = z;
    }

    /* JADX INFO: finally extract failed */
    /* renamed from: a */
    public boolean mo7270a(Matrix4fWrap ajk, C3165ob obVar, Matrix4fWrap ajk2, C3165ob obVar2, C2678iT iTVar) {
        this.stack = this.stack.bcD();
        this.stack.bcJ().push();
        try {
            C5852abg.C1854a aVar = this.f5840mM.get();
            aVar.init();
            mo17318a(obVar);
            mo17320b(obVar2);
            aVar.fGC = obVar.getMargin() + obVar2.getMargin();
            aVar.fGC *= aVar.fGC;
            aVar.fGA.set(ajk);
            aVar.fGB.set(ajk2);
            if (!aVar.fGA.anA() || !aVar.fGB.anA()) {
                this.stack.bcJ().pop();
                return false;
            }
            mo17316a(aVar, new C2052a(iTVar));
            boolean Ef = iTVar.mo7941Ef();
            this.stack.bcJ().pop();
            return Ef;
        } catch (Throwable th) {
            this.stack.bcJ().pop();
            throw th;
        }
    }

    /* renamed from: a.bb$a */
    class C2052a implements C5852abg.C1855b {
        private final /* synthetic */ C2678iT bvk;

        C2052a(C2678iT iTVar) {
            this.bvk = iTVar;
        }

        /* renamed from: c */
        public void mo5217c(Vec3f vec3f, Vec3f vec3f2, float f) {
            this.bvk.mo7945a(vec3f, vec3f2.dfR(), f);
        }

        /* renamed from: b */
        public void mo5216b(int i, int i2, int i3, int i4) {
        }
    }
}
