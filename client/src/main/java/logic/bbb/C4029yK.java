package logic.bbb;

import com.hoplon.geometry.BoundingBox;
import com.hoplon.geometry.Vec3f;
import game.geometry.Matrix4fWrap;
import p001a.C1605XU;
import p001a.C3978xf;

/* renamed from: a.yK */
/* compiled from: a */
public abstract class C4029yK {
    public static final byte BOX = 0;
    public static final byte CAPSULE = 3;
    public static final byte COMPOUND = 7;
    public static final byte CONVEX_HULL = 8;
    public static final byte CYLINDER = 2;
    public static final byte LINE = 6;
    public static final byte MESH = 4;
    public static final byte SPHERE = 1;
    public static final byte TRIANGLE = 5;

    public BoundingBox boundingBox = null;
    public float volume;
    private boolean invisible;

    public abstract void calculateLocalInertia(float f, Vec3f vec3f);

    public abstract void getAabb(C3978xf xfVar, Vec3f vec3f, Vec3f vec3f2);

    public abstract Vec3f getLocalScaling();

    public abstract void setLocalScaling(Vec3f vec3f);

    public abstract byte getShapeType();

    /* access modifiers changed from: protected */
    public abstract void growBoundingBox(Matrix4fWrap ajk, BoundingBox boundingBox2);

    public BoundingBox getBoundingBox() {
        if (this.boundingBox == null) {
            this.boundingBox = new BoundingBox();
            growBoundingBox(new Matrix4fWrap(), this.boundingBox);
        }
        return this.boundingBox;
    }

    public float getVolume() {
        return this.volume;
    }

    public void setVolume(float f) {
        this.volume = f;
    }

    public void setTransparent(boolean z) {
    }

    public void setDisableOnColision(boolean z) {
    }

    public float getOuterSphereRadius() {
        float dit = getBoundingBox().dit();
        if (Float.isInfinite(dit)) {
            return 10.0f;
        }
        return dit;
    }

    public void getFarthest(Vec3f vec3f, Vec3f vec3f2) {
    }

    public C1188RY toMesh() {
        throw new C1605XU();
    }

    public void getCenter(Vec3f vec3f) {
    }

    public boolean isInvisible() {
        return this.invisible;
    }

    /* access modifiers changed from: protected */
    public void setInvisible(boolean z) {
        this.invisible = z;
    }
}
