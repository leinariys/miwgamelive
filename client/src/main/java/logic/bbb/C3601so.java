package logic.bbb;

import com.hoplon.geometry.BoundingBox;
import com.hoplon.geometry.Vec3f;
import game.geometry.Matrix3fWrap;
import game.geometry.Matrix4fWrap;
import p001a.C0647JL;
import p001a.C0920NU;
import p001a.C3427rS;
import p001a.C3978xf;

import javax.vecmath.Vector3f;
import javax.vecmath.Vector4f;

/* renamed from: a.so */
/* compiled from: a */
public class C3601so extends C0330ET {

    /* renamed from: jR */
    static final /* synthetic */ boolean f9154jR = (!C3601so.class.desiredAssertionStatus());

    private Vec3f bgu;

    public C3601so() {
    }

    public C3601so(float f, float f2, float f3) {
        mo22022A(new Vec3f(f, f2, f3));
        Vec3f vec3f = new Vec3f(f / 2.0f, f2 / 2.0f, f3 / 2.0f);
        Vector3f vector3f = new Vector3f(getMargin(), getMargin(), getMargin());
        C0647JL.m5603g(this.implicitShapeDimensions, vec3f, this.localScaling);
        this.implicitShapeDimensions.sub(vector3f);
    }

    public C3601so(float f) {
        this(f, f, f);
    }

    /* renamed from: A */
    public void mo22022A(Vec3f vec3f) {
        this.bgu = vec3f;
        this.volume = vec3f.x * vec3f.y * vec3f.z;
    }

    public Vec3f aby() {
        return this.bgu;
    }

    public String toString() {
        return "Box [x:" + this.bgu.x + ",y:" + this.bgu.y + ",z:" + this.bgu.z + "]";
    }

    public byte getShapeType() {
        return 0;
    }

    /* access modifiers changed from: protected */
    public void growBoundingBox(Matrix4fWrap ajk, BoundingBox boundingBox) {
        Vec3f aby = aby();
        BoundingBox boundingBox2 = new BoundingBox();
        BoundingBox boundingBox3 = new BoundingBox();
        boundingBox2.addPoint(aby.mo23510mS(0.5f));
        boundingBox2.addPoint(aby.mo23510mS(-0.5f));
        boundingBox2.mo23418a(ajk, boundingBox3);
        boundingBox.addPoint(boundingBox3.dqQ());
        boundingBox.addPoint(boundingBox3.dqP());
    }

    public void getFarthest(Vec3f vec3f, Vec3f vec3f2) {
        Vec3f aby = aby();
        vec3f2.set(0.5f * (vec3f.x < 0.0f ? -aby.x : aby.x), 0.5f * (vec3f.y < 0.0f ? -aby.y : aby.y), (vec3f.z < 0.0f ? -aby.z : aby.z) * 0.5f);
    }

    public C1188RY toMesh() {
        int[] iArr = new int[36];
        Vec3f aby = aby();
        float f = aby.x * 0.5f;
        float f2 = aby.y * 0.5f;
        float f3 = aby.z * 0.5f;
        Vec3f[] vec3fArr = {new Vec3f(-f, -f2, -f3), new Vec3f(f, -f2, -f3), new Vec3f(f, f2, -f3), new Vec3f(-f, f2, -f3), new Vec3f(-f, -f2, f3), new Vec3f(f, -f2, f3), new Vec3f(f, f2, f3), new Vec3f(-f, f2, f3)};
        m39101a(iArr, m39101a(iArr, m39101a(iArr, m39101a(iArr, m39101a(iArr, m39101a(iArr, 0, 1, 0, 3, 2), 4, 5, 6, 7), 0, 1, 5, 4), 1, 2, 6, 5), 2, 3, 7, 6), 3, 0, 4, 7);
        return new C1188RY(vec3fArr, iArr);
    }

    /* renamed from: a */
    private int m39101a(int[] iArr, int i, int i2, int i3, int i4, int i5) {
        int i6 = i + 1;
        iArr[i] = i2;
        int i7 = i6 + 1;
        iArr[i6] = i3;
        int i8 = i7 + 1;
        iArr[i7] = i4;
        int i9 = i8 + 1;
        iArr[i8] = i2;
        int i10 = i9 + 1;
        iArr[i9] = i4;
        int i11 = i10 + 1;
        iArr[i10] = i5;
        return i11;
    }

    public void getCenter(Vec3f vec3f) {
        vec3f.set(0.0f, 0.0f, 0.0f);
    }

    public Vec3f abz() {
        this.stack.bcH().push();
        try {
            Vec3f ac = this.stack.bcH().mo4458ac(abA());
            ac.add(this.stack.bcH().mo4460h(getMargin(), getMargin(), getMargin()));
            return (Vec3f) this.stack.bcH().mo15197aq(ac);
        } finally {
            this.stack.bcH().pop();
        }
    }

    public Vec3f abA() {
        return this.implicitShapeDimensions;
    }

    public Vec3f localGetSupportingVertex(Vec3f vec3f) {
        this.stack.bcH().push();
        try {
            Vec3f ac = this.stack.bcH().mo4458ac(abA());
            ac.add(this.stack.bcH().mo4460h(getMargin(), getMargin(), getMargin()));
            return (Vec3f) this.stack.bcH().mo15197aq(this.stack.bcH().mo4460h(C0920NU.m7655g(vec3f.x, ac.x, -ac.x), C0920NU.m7655g(vec3f.y, ac.y, -ac.y), C0920NU.m7655g(vec3f.z, ac.z, -ac.z)));
        } finally {
            this.stack.bcH().pop();
        }
    }

    public Vec3f localGetSupportingVertexWithoutMargin(Vec3f vec3f) {
        this.stack.bcH().push();
        try {
            Vec3f ac = this.stack.bcH().mo4458ac(abA());
            return (Vec3f) this.stack.bcH().mo15197aq(this.stack.bcH().mo4460h(C0920NU.m7655g(vec3f.x, ac.x, -ac.x), C0920NU.m7655g(vec3f.y, ac.y, -ac.y), C0920NU.m7655g(vec3f.z, ac.z, -ac.z)));
        } finally {
            this.stack.bcH().pop();
        }
    }

    public void batchedUnitVectorGetSupportingVertexWithoutMargin(Vec3f[] vec3fArr, Vec3f[] vec3fArr2, int i) {
        this.stack.bcH().push();
        try {
            Vec3f ac = this.stack.bcH().mo4458ac(abA());
            for (int i2 = 0; i2 < i; i2++) {
                Vec3f vec3f = vec3fArr[i2];
                vec3fArr2[i2].set(C0920NU.m7655g(vec3f.x, ac.x, -ac.x), C0920NU.m7655g(vec3f.y, ac.y, -ac.y), C0920NU.m7655g(vec3f.z, ac.z, -ac.z));
            }
        } finally {
            this.stack.bcH().pop();
        }
    }

    public void setMargin(float f) {
        this.stack.bcH().push();
        try {
            Vec3f h = this.stack.bcH().mo4460h(getMargin(), getMargin(), getMargin());
            Vec3f vec3f = (Vec3f) this.stack.bcH().get();
            vec3f.add(this.implicitShapeDimensions, h);
            super.setMargin(f);
            this.implicitShapeDimensions.sub(vec3f, this.stack.bcH().mo4460h(getMargin(), getMargin(), getMargin()));
        } finally {
            this.stack.bcH().pop();
        }
    }

    public void setLocalScaling(Vec3f vec3f) {
        this.stack.bcH().push();
        try {
            Vec3f h = this.stack.bcH().mo4460h(getMargin(), getMargin(), getMargin());
            Vec3f vec3f2 = (Vec3f) this.stack.bcH().get();
            vec3f2.add(this.implicitShapeDimensions, h);
            Vec3f vec3f3 = (Vec3f) this.stack.bcH().get();
            C0647JL.m5604h(vec3f3, vec3f2, this.localScaling);
            super.setLocalScaling(vec3f);
            C0647JL.m5603g(this.implicitShapeDimensions, vec3f3, this.localScaling);
            this.implicitShapeDimensions.sub(h);
        } finally {
            this.stack.bcH().pop();
        }
    }

    public void getAabb(C3978xf xfVar, Vec3f vec3f, Vec3f vec3f2) {
        this.stack.bcF();
        try {
            Vec3f abA = abA();
            Matrix3fWrap g = this.stack.bcK().mo15565g(xfVar.bFF);
            C3427rS.m38374b(g);
            Vec3f vec3f3 = (Vec3f) this.stack.bcH().get();
            Vec3f ac = this.stack.bcH().mo4458ac(xfVar.bFG);
            Vec3f vec3f4 = (Vec3f) this.stack.bcH().get();
            g.getRow(0, vec3f3);
            vec3f4.x = vec3f3.dot(abA);
            g.getRow(1, vec3f3);
            vec3f4.y = vec3f3.dot(abA);
            g.getRow(2, vec3f3);
            vec3f4.z = vec3f3.dot(abA);
            vec3f4.add(this.stack.bcH().mo4460h(getMargin(), getMargin(), getMargin()));
            vec3f.sub(ac, vec3f4);
            vec3f2.add(ac, vec3f4);
        } finally {
            this.stack.bcG();
        }
    }

    public void calculateLocalInertia(float f, Vec3f vec3f) {
        this.stack.bcH().push();
        try {
            Vec3f ac = this.stack.bcH().mo4458ac(abz());
            float f2 = ac.x * 2.0f;
            float f3 = ac.y * 2.0f;
            float f4 = ac.z * 2.0f;
            vec3f.set((f / 12.0f) * ((f3 * f3) + (f4 * f4)), ((f4 * f4) + (f2 * f2)) * (f / 12.0f), ((f2 * f2) + (f3 * f3)) * (f / 12.0f));
        } finally {
            this.stack.bcH().pop();
        }
    }

    /* renamed from: a */
    public void mo1867a(Vec3f vec3f, Vec3f vec3f2, int i) {
        this.stack.bcH().push();
        this.stack.bcM().push();
        try {
            Vector4f vector4f = (Vector4f) this.stack.bcM().get();
            mo22023a(vector4f, i);
            vec3f.set(vector4f.x, vector4f.y, vector4f.z);
            Vec3f vec3f3 = (Vec3f) this.stack.bcH().get();
            vec3f3.negate(vec3f);
            vec3f2.set(localGetSupportingVertex(vec3f3));
        } finally {
            this.stack.bcH().pop();
            this.stack.bcM().pop();
        }
    }

    public int abB() {
        return 6;
    }

    public int getNumVertices() {
        return 8;
    }

    public int abC() {
        return 12;
    }

    /* renamed from: b */
    public void mo1871b(int i, Vec3f vec3f) {
        Vec3f abA = abA();
        vec3f.set((abA.x * ((float) (1 - (i & 1)))) - (abA.x * ((float) (i & 1))), (abA.y * ((float) (1 - ((i & 2) >> 1)))) - (abA.y * ((float) ((i & 2) >> 1))), (abA.z * ((float) (1 - ((i & 4) >> 2)))) - (abA.z * ((float) ((i & 4) >> 2))));
    }

    /* renamed from: a */
    public void mo22023a(Vector4f vector4f, int i) {
        Vec3f abA = abA();
        switch (i) {
            case 0:
                vector4f.set(1.0f, 0.0f, 0.0f, -abA.x);
                return;
            case 1:
                vector4f.set(-1.0f, 0.0f, 0.0f, -abA.x);
                return;
            case 2:
                vector4f.set(0.0f, 1.0f, 0.0f, -abA.y);
                return;
            case 3:
                vector4f.set(0.0f, -1.0f, 0.0f, -abA.y);
                return;
            case 4:
                vector4f.set(0.0f, 0.0f, 1.0f, -abA.z);
                return;
            case 5:
                vector4f.set(0.0f, 0.0f, -1.0f, -abA.z);
                return;
            default:
                if (!f9154jR) {
                    throw new AssertionError();
                }
                return;
        }
    }

    /* renamed from: a */
    public void mo1865a(int i, Vec3f vec3f, Vec3f vec3f2) {
        int i2;
        int i3;
        switch (i) {
            case 0:
                i2 = 1;
                i3 = 0;
                break;
            case 1:
                i2 = 2;
                i3 = 0;
                break;
            case 2:
                i2 = 3;
                i3 = 1;
                break;
            case 3:
                i2 = 3;
                i3 = 2;
                break;
            case 4:
                i2 = 4;
                i3 = 0;
                break;
            case 5:
                i2 = 5;
                i3 = 1;
                break;
            case 6:
                i2 = 6;
                i3 = 2;
                break;
            case 7:
                i2 = 7;
                i3 = 3;
                break;
            case 8:
                i2 = 5;
                i3 = 4;
                break;
            case 9:
                i2 = 6;
                i3 = 4;
                break;
            case 10:
                i2 = 7;
                i3 = 5;
                break;
            case 11:
                i2 = 7;
                i3 = 6;
                break;
            default:
                if (f9154jR) {
                    i2 = 0;
                    i3 = 0;
                    break;
                } else {
                    throw new AssertionError();
                }
        }
        mo1871b(i3, vec3f);
        mo1871b(i2, vec3f2);
    }

    /* renamed from: b */
    public boolean mo1872b(Vec3f vec3f, float f) {
        Vec3f abA = abA();
        return vec3f.x <= abA.x + f && vec3f.x >= (-abA.x) - f && vec3f.y <= abA.y + f && vec3f.y >= (-abA.y) - f && vec3f.z <= abA.z + f && vec3f.z >= (-abA.z) - f;
    }

    public int getNumPreferredPenetrationDirections() {
        return 6;
    }

    public void getPreferredPenetrationDirection(int i, Vec3f vec3f) {
        switch (i) {
            case 0:
                vec3f.set(1.0f, 0.0f, 0.0f);
                return;
            case 1:
                vec3f.set(-1.0f, 0.0f, 0.0f);
                return;
            case 2:
                vec3f.set(0.0f, 1.0f, 0.0f);
                return;
            case 3:
                vec3f.set(0.0f, -1.0f, 0.0f);
                return;
            case 4:
                vec3f.set(0.0f, 0.0f, 1.0f);
                return;
            case 5:
                vec3f.set(0.0f, 0.0f, -1.0f);
                return;
            default:
                if (!f9154jR) {
                    throw new AssertionError();
                }
                return;
        }
    }
}
