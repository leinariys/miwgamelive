package logic.bbb;

import com.hoplon.geometry.BoundingBox;
import com.hoplon.geometry.Vec3f;
import game.geometry.Matrix4fWrap;
import p001a.C0647JL;
import p001a.C3026nA;
import p001a.C4054ye;

import javax.vecmath.Tuple3f;

/* renamed from: a.SC */
/* compiled from: a */
public class C1223SC extends C0330ET {

    /* renamed from: jR */
    static final /* synthetic */ boolean f1526jR = (!C1223SC.class.desiredAssertionStatus());

    public Vec3f[] ebY;
    private C4054ye<Vec3f> eeH;
    private C1224a eeI = new C1224a(this, (C1224a) null);

    public C1223SC() {
    }

    public C1223SC(Vec3f[] vec3fArr) {
        this.ebY = vec3fArr;
        aPc();
        setVolume(getBoundingBox().dio() * getBoundingBox().dip() * getBoundingBox().diq());
        this.eeH = new C4054ye<>(getBoundingBox());
    }

    public byte getShapeType() {
        return 8;
    }

    /* access modifiers changed from: protected */
    public void growBoundingBox(Matrix4fWrap ajk, BoundingBox boundingBox) {
        Vec3f vec3f = new Vec3f();
        for (Tuple3f b : this.ebY) {
            ajk.mo14002b(b, (Tuple3f) vec3f);
            boundingBox.addPoint(vec3f);
        }
    }

    public void getFarthest(Vec3f vec3f, Vec3f vec3f2) {
        Tuple3f tuple3f = null;
        float f = Float.NEGATIVE_INFINITY;
        Tuple3f[] tuple3fArr = this.ebY;
        int length = tuple3fArr.length;
        int i = 0;
        while (i < length) {
            Tuple3f tuple3f2 = tuple3fArr[i];
            float dot = vec3f.dot(tuple3f2);
            if (dot <= f) {
                dot = f;
                tuple3f2 = tuple3f;
            }
            i++;
            f = dot;
            tuple3f = tuple3f2;
        }
        if (tuple3f != null) {
            vec3f2.set(tuple3f);
        }
    }

    public Vec3f localGetSupportingVertexWithoutMargin(Vec3f vec3f) {
        Vec3f dtf;
        this.stack = this.stack.bcD();
        this.stack.bcH().push();
        try {
            Vec3f h = this.stack.bcH().mo4460h(0.0f, 0.0f, 0.0f);
            float f = -1.0E30f;
            Vec3f ac = this.stack.bcH().mo4458ac(vec3f);
            float lengthSquared = ac.lengthSquared();
            if (lengthSquared < 1.0E-4f) {
                ac.set(1.0f, 0.0f, 0.0f);
            } else {
                ac.scale(1.0f / ((float) Math.sqrt((double) lengthSquared)));
            }
            Vec3f vec3f2 = (Vec3f) this.stack.bcH().get();
            if (this.localScaling.length() != 1.0f) {
                int i = 0;
                Vec3f vec3f3 = vec3f2;
                while (i < this.ebY.length) {
                    Vec3f vec3f4 = this.ebY[i];
                    float dot = ac.dot(vec3f4);
                    if (dot > f) {
                        h.set(vec3f4);
                    } else {
                        dot = f;
                    }
                    i++;
                    f = dot;
                }
                dtf = (Vec3f) this.stack.bcH().mo15197aq(h);
            } else {
                this.eeI.mo5336v(ac);
                this.eeH.mo23156a(this.eeI);
                dtf = this.eeI.dtf();
                this.stack.bcH().pop();
            }
            return dtf;
        } finally {
            this.stack.bcH().pop();
        }
    }

    public void batchedUnitVectorGetSupportingVertexWithoutMargin(Vec3f[] vec3fArr, Vec3f[] vec3fArr2, int i) {
        this.stack.bcH().push();
        try {
            float[] fArr = new float[i];
            for (int i2 = 0; i2 < i; i2++) {
                fArr[i2] = -1.0E30f;
            }
            Vec3f vec3f = (Vec3f) this.stack.bcH().get();
            for (Vec3f g : this.ebY) {
                C0647JL.m5603g(vec3f, g, this.localScaling);
                for (int i3 = 0; i3 < i; i3++) {
                    float dot = vec3fArr[i3].dot(vec3f);
                    if (dot > fArr[i3]) {
                        vec3fArr2[i3].set(vec3f);
                        fArr[i3] = dot;
                    }
                }
            }
        } finally {
            this.stack.bcH().pop();
        }
    }

    public Vec3f localGetSupportingVertex(Vec3f vec3f) {
        this.stack.bcH().push();
        try {
            Vec3f ac = this.stack.bcH().mo4458ac(localGetSupportingVertexWithoutMargin(vec3f));
            if (getMargin() != 0.0f) {
                Vec3f ac2 = this.stack.bcH().mo4458ac(vec3f);
                if (ac2.lengthSquared() < 1.4210855E-14f) {
                    ac2.set(-1.0f, -1.0f, -1.0f);
                }
                ac2.normalize();
                ac.scaleAdd(getMargin(), ac2, ac);
            }
            return (Vec3f) this.stack.bcH().mo15197aq(ac);
        } finally {
            this.stack.bcH().pop();
        }
    }

    public int getNumVertices() {
        return this.ebY.length;
    }

    public int abC() {
        return this.ebY.length;
    }

    /* renamed from: a */
    public void mo1865a(int i, Vec3f vec3f, Vec3f vec3f2) {
        int length = (i + 1) % this.ebY.length;
        C0647JL.m5603g(vec3f, this.ebY[i % this.ebY.length], this.localScaling);
        C0647JL.m5603g(vec3f2, this.ebY[length], this.localScaling);
    }

    /* renamed from: b */
    public void mo1871b(int i, Vec3f vec3f) {
        C0647JL.m5603g(vec3f, this.ebY[i], this.localScaling);
    }

    public int abB() {
        return 0;
    }

    /* renamed from: a */
    public void mo1867a(Vec3f vec3f, Vec3f vec3f2, int i) {
        if (!f1526jR) {
            throw new AssertionError();
        }
    }

    /* renamed from: b */
    public boolean mo1872b(Vec3f vec3f, float f) {
        return false;
    }

    public Vec3f[] bsr() {
        return this.ebY;
    }

    public void setLocalScaling(Vec3f vec3f) {
        super.setLocalScaling(vec3f);
    }

    /* renamed from: a.SC$a */
    private class C1224a implements C3026nA<Vec3f> {
        boolean iJd;
        boolean iJe;
        boolean iJf;
        Vec3f iJg;
        float iJh;
        private Vec3f dir;

        private C1224a() {
            this.iJg = new Vec3f(0.0f, 0.0f, 0.0f);
        }

        /* synthetic */ C1224a(C1223SC sc, C1224a aVar) {
            this();
        }

        /* renamed from: v */
        public void mo5336v(Vec3f vec3f) {
            boolean z;
            boolean z2 = true;
            this.iJd = vec3f.x >= 0.0f;
            if (vec3f.y >= 0.0f) {
                z = true;
            } else {
                z = false;
            }
            this.iJe = z;
            if (vec3f.z < 0.0f) {
                z2 = false;
            }
            this.iJf = z2;
            this.iJh = Float.NEGATIVE_INFINITY;
            this.dir = vec3f;
        }

        /* renamed from: a */
        public boolean mo5332a(BoundingBox boundingBox) {
            if (this.iJh == Float.NEGATIVE_INFINITY) {
                return true;
            }
            return (((this.iJd ? boundingBox.dqQ().x : boundingBox.dqP().x) * this.dir.x) + ((this.iJe ? boundingBox.dqQ().y : boundingBox.dqP().y) * this.dir.y)) + (this.dir.z * (this.iJf ? boundingBox.dqQ().z : boundingBox.dqP().z)) > this.iJh;
        }

        /* renamed from: a */
        public boolean mo5334a(BoundingBox boundingBox, Vec3f vec3f) {
            float dot = vec3f.dot(this.dir);
            if (dot <= this.iJh) {
                return true;
            }
            this.iJg = vec3f;
            this.iJh = dot;
            return true;
        }

        /* access modifiers changed from: protected */
        public Vec3f dtf() {
            return this.iJg;
        }
    }
}
