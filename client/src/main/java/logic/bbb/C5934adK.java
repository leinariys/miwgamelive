package logic.bbb;

import com.hoplon.geometry.Vec3f;
import p001a.C0647JL;
import p001a.C0763Kt;
import p001a.C3427rS;
import p001a.C3978xf;

/* renamed from: a.adK  reason: case insensitive filesystem */
/* compiled from: a */
public abstract class C5934adK extends C3165ob {
    public final Vec3f implicitShapeDimensions = new Vec3f();
    public final Vec3f localScaling = new Vec3f(1.0f, 1.0f, 1.0f);
    public float collisionMargin = 0.04f;
    public C0763Kt stack = C0763Kt.bcE();

    public void getAabb(C3978xf xfVar, Vec3f vec3f, Vec3f vec3f2) {
        getAabbSlow(xfVar, vec3f, vec3f2);
    }

    public void getAabbSlow(C3978xf xfVar, Vec3f vec3f, Vec3f vec3f2) {
        this.stack.bcH().push();
        try {
            float margin = getMargin();
            int i = 0;
            while (true) {
                int i2 = i;
                if (i2 < 3) {
                    Vec3f h = this.stack.bcH().mo4460h(0.0f, 0.0f, 0.0f);
                    C0647JL.m5590a(h, i2, 1.0f);
                    Vec3f vec3f3 = (Vec3f) this.stack.bcH().get();
                    C3427rS.m38372a(vec3f3, h, xfVar.bFF);
                    Vec3f ac = this.stack.bcH().mo4458ac(this.stack.bcH().mo4458ac(localGetSupportingVertex(vec3f3)));
                    xfVar.mo22946G(ac);
                    C0647JL.m5590a(vec3f2, i2, C0647JL.m5594b(ac, i2) + margin);
                    C0647JL.m5590a(h, i2, -1.0f);
                    C3427rS.m38372a(vec3f3, h, xfVar.bFF);
                    ac.set(localGetSupportingVertex(vec3f3));
                    xfVar.mo22946G(ac);
                    C0647JL.m5590a(vec3f, i2, C0647JL.m5594b(ac, i2) - margin);
                    i = i2 + 1;
                } else {
                    return;
                }
            }
        } finally {
            this.stack.bcH().pop();
        }
    }

    public Vec3f localGetSupportingVertex(Vec3f vec3f) {
        this.stack.bcH().push();
        try {
            Vec3f ac = this.stack.bcH().mo4458ac(localGetSupportingVertexWithoutMargin(vec3f));
            if (getMargin() != 0.0f) {
                Vec3f ac2 = this.stack.bcH().mo4458ac(vec3f);
                if (ac2.lengthSquared() < 1.4210855E-14f) {
                    ac2.set(-1.0f, -1.0f, -1.0f);
                }
                ac2.normalize();
                ac.scaleAdd(getMargin(), ac2, ac);
            }
            return (Vec3f) this.stack.bcH().mo15197aq(ac);
        } finally {
            this.stack.bcH().pop();
        }
    }

    public Vec3f getLocalScaling() {
        return this.localScaling;
    }

    public void setLocalScaling(Vec3f vec3f) {
        this.localScaling.set(vec3f);
    }

    public float getMargin() {
        return this.collisionMargin;
    }

    public void setMargin(float f) {
        this.collisionMargin = f;
    }

    public int getNumPreferredPenetrationDirections() {
        return 0;
    }

    public void getPreferredPenetrationDirection(int i, Vec3f vec3f) {
        throw new InternalError();
    }
}
