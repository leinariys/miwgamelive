package logic.bbb;

import com.hoplon.geometry.Vec3f;
import game.geometry.Matrix4fWrap;
import game.geometry.Vec3d;
import p001a.C2678iT;

/* renamed from: a.un */
/* compiled from: a */
public class C3768un implements aVJ<C4029yK, C4029yK> {
    private final aVJ<C4029yK, C4029yK> eiR;

    public C3768un(aVJ<C4029yK, C4029yK> avj) {
        this.eiR = avj;
    }

    /* renamed from: a */
    public boolean mo7270a(Matrix4fWrap ajk, C4029yK yKVar, Matrix4fWrap ajk2, C4029yK yKVar2, C2678iT iTVar) {
        return this.eiR.mo7270a(ajk2, yKVar2, ajk, yKVar, new C3769a(iTVar));
    }

    /* renamed from: a.un$a */
    class C3769a implements C2678iT {
        private final /* synthetic */ C2678iT bvk;

        C3769a(C2678iT iTVar) {
            this.bvk = iTVar;
        }

        /* renamed from: a */
        public void mo7945a(Vec3f vec3f, Vec3d ajr, float f) {
            this.bvk.mo7945a(vec3f.dfS(), ajr, f);
        }

        /* renamed from: Ef */
        public boolean mo7941Ef() {
            return this.bvk.mo7941Ef();
        }

        /* renamed from: Ee */
        public Vec3d mo7940Ee() {
            return this.bvk.mo7940Ee();
        }
    }
}
