package logic.bbb;

import com.hoplon.geometry.Vec3f;
import game.geometry.Matrix4fWrap;
import p001a.C2678iT;
import p001a.C5780aaM;

import javax.vecmath.Tuple3f;

/* renamed from: a.lM */
/* compiled from: a */
public class C2878lM implements aVJ<C4029yK, C4029yK> {
    private C5780aaM aAn;
    private C4029yK aAo;
    private C4029yK aAp;
    private Vec3f aAq = new Vec3f();
    private Vec3f aAr = new Vec3f();
    private Vec3f aAs = new Vec3f();

    /* renamed from: a */
    public boolean mo7270a(Matrix4fWrap ajk, C4029yK yKVar, Matrix4fWrap ajk2, C4029yK yKVar2, C2678iT iTVar) {
        this.aAo = yKVar;
        this.aAp = yKVar2;
        Matrix4fWrap a = m34840a(ajk, ajk2);
        if (this.aAn == null) {
            this.aAn = new C5780aaM(yKVar, yKVar2, a);
        } else {
            this.aAn.mo12175a(yKVar, yKVar2);
            this.aAn.mo12182f(a);
        }
        m34839MD();
        return true;
    }

    /* renamed from: MD */
    private void m34839MD() {
        this.aAo.getCenter(this.aAq);
        this.aAp.getCenter(this.aAr);
        this.aAn.bML().mo13987a((Tuple3f) this.aAr, (Tuple3f) this.aAs);
        this.aAs.mo23513o(this.aAq);
        if (this.aAs.isZero()) {
            this.aAs.x = 1.0E-5f;
        }
    }

    /* renamed from: ME */
    public C5780aaM mo20221ME() {
        return this.aAn;
    }

    /* renamed from: a */
    private Matrix4fWrap m34840a(Matrix4fWrap ajk, Matrix4fWrap ajk2) {
        return new Matrix4fWrap().mo14045m(ajk).mo14044l(ajk2);
    }
}
