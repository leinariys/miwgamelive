package logic.bbb;

import com.hoplon.geometry.BoundingBox;
import com.hoplon.geometry.Vec3f;
import game.geometry.Matrix4fWrap;
import p001a.C3978xf;

import javax.vecmath.Tuple3f;
import javax.vecmath.Vector3f;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

/* renamed from: a.RY */
/* compiled from: a */
public class C1188RY extends C4029yK {

    public Vec3f[] ebY;
    public int[] ebZ;

    public C1188RY(Vec3f[] vec3fArr, int[] iArr) {
        this.ebY = vec3fArr;
        this.ebZ = iArr;
    }

    public Vec3f[] bsr() {
        return this.ebY;
    }

    /* renamed from: a */
    public void mo5244a(Vec3f[] vec3fArr) {
        this.ebY = vec3fArr;
    }

    public int[] bss() {
        return this.ebZ;
    }

    /* renamed from: b */
    public void mo5245b(int[] iArr) {
        this.ebZ = iArr;
    }

    public byte getShapeType() {
        return 4;
    }

    /* access modifiers changed from: protected */
    public void growBoundingBox(Matrix4fWrap ajk, BoundingBox boundingBox) {
        Vec3f vec3f = new Vec3f();
        for (Tuple3f b : this.ebY) {
            ajk.mo14002b(b, (Tuple3f) vec3f);
            boundingBox.addPoint(vec3f);
        }
    }

    public void getFarthest(Vec3f vec3f, Vec3f vec3f2) {
        Tuple3f tuple3f = null;
        float f = Float.NEGATIVE_INFINITY;
        Tuple3f[] bsr = bsr();
        int length = bsr.length;
        int i = 0;
        while (i < length) {
            Tuple3f tuple3f2 = bsr[i];
            float dot = vec3f.dot((Vector3f) tuple3f2);
            if (dot <= f) {
                dot = f;
                tuple3f2 = tuple3f;
            }
            i++;
            f = dot;
            tuple3f = tuple3f2;
        }
        if (tuple3f != null) {
            vec3f2.set(tuple3f);
        }
    }

    public C1188RY toMesh() {
        return this;
    }

    public void getCenter(Vec3f vec3f) {
        Vec3f vec3f2 = new Vec3f();
        Vec3f vec3f3 = new Vec3f();
        for (Vec3f vec3f4 : this.ebY) {
            if (vec3f4.x < vec3f3.x) {
                vec3f3.x = vec3f4.x;
            }
            if (vec3f4.y < vec3f3.y) {
                vec3f3.y = vec3f4.y;
            }
            if (vec3f4.z < vec3f3.z) {
                vec3f3.z = vec3f4.z;
            }
            if (vec3f4.x > vec3f2.x) {
                vec3f2.x = vec3f4.x;
            }
            if (vec3f4.y > vec3f2.y) {
                vec3f2.y = vec3f4.y;
            }
            if (vec3f4.z > vec3f2.z) {
                vec3f2.z = vec3f4.z;
            }
        }
        vec3f.x = vec3f3.x + ((vec3f2.x - vec3f3.x) / 2.0f);
        vec3f.y = vec3f3.y + ((vec3f2.y - vec3f3.y) / 2.0f);
        vec3f.z = vec3f3.z + ((vec3f2.z - vec3f3.z) / 2.0f);
    }

    private void writeObject(ObjectOutputStream objectOutputStream) throws IOException {
        objectOutputStream.writeInt(this.ebY.length);
        for (Vec3f writeObject : this.ebY) {
            objectOutputStream.writeObject(writeObject);
        }
        objectOutputStream.writeInt(this.ebZ.length);
        for (int writeInt : this.ebZ) {
            objectOutputStream.writeInt(writeInt);
        }
    }

    private void readObject(ObjectInputStream objectInputStream) throws IOException {
        int readInt = objectInputStream.readInt();
        this.ebY = new Vec3f[readInt];
        for (int i = 0; i < readInt; i++) {
            this.ebY[i] = (Vec3f) objectInputStream.readObject();
        }
        int readInt2 = objectInputStream.readInt();
        this.ebZ = new int[readInt2];
        for (int i2 = 0; i2 < readInt2; i2++) {
            this.ebZ[i2] = objectInputStream.readInt();
        }
    }

    public void calculateLocalInertia(float f, Vec3f vec3f) {
    }

    public void getAabb(C3978xf xfVar, Vec3f vec3f, Vec3f vec3f2) {
    }

    public Vec3f getLocalScaling() {
        return null;
    }

    public void setLocalScaling(Vec3f vec3f) {
    }
}
