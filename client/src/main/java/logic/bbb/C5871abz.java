package logic.bbb;

import com.hoplon.geometry.Vec3f;
import game.geometry.Matrix4fWrap;
import p001a.C2678iT;

import javax.vecmath.Tuple3f;
import javax.vecmath.Vector3f;

/* renamed from: a.abz  reason: case insensitive filesystem */
/* compiled from: a */
public class C5871abz implements aVJ<C6348alI, aOU> {
    /* renamed from: a */
    public boolean mo7270a(Matrix4fWrap ajk, C6348alI ali, Matrix4fWrap ajk2, aOU aou, C2678iT iTVar) {
        Vec3f c = ajk2.mo14004c((Tuple3f) aou.dnY());
        Vec3f c2 = ajk2.mo14004c((Tuple3f) aou.dnZ());
        Vec3f ceH = ajk.ceH();
        return Vec3f.direction(mo12549o(c, c2, ceH), ceH) < ali.getRadius() * ali.getRadius();
    }

    /* renamed from: o */
    public Vec3f mo12549o(Vec3f vec3f, Vec3f vec3f2, Vec3f vec3f3) {
        Vec3f j = vec3f3.mo23506j(vec3f);
        Vec3f j2 = vec3f2.mo23506j(vec3f);
        float length = j2.length();
        Vec3f mS = j2.mo23510mS(1.0f / length);
        float dot = mS.dot(j);
        if (dot <= 0.0f) {
            return new Vec3f((Vector3f) vec3f);
        }
        if (dot >= length) {
            return new Vec3f((Vector3f) vec3f2);
        }
        return vec3f.mo23503i(mS.mo23510mS(dot));
    }
}
