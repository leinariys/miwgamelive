package logic.bbb;

import com.hoplon.geometry.Vec3f;
import game.geometry.Matrix4fWrap;
import game.geometry.Vec3d;
import p001a.C0763Kt;
import p001a.C2678iT;

import javax.vecmath.Tuple3f;
import javax.vecmath.Vector3f;
import java.lang.reflect.Array;

/* renamed from: a.ack  reason: case insensitive filesystem */
/* compiled from: a */
public class C5908ack implements aVJ<C3601so, C3601so> {
    /* JADX INFO: finally extract failed */
    /* renamed from: a */
    public boolean mo7270a(Matrix4fWrap ajk, C3601so soVar, Matrix4fWrap ajk2, C3601so soVar2, C2678iT iTVar) {
        Vec3f j = ajk2.ceH().mo23506j(ajk.ceH());
        Vec3f[] vec3fArr = {ajk.ceC(), ajk.ceD(), ajk.ceE()};
        Vec3f mS = soVar.aby().mo23510mS(0.5f);
        Vector3f[] vector3fArr = {ajk2.ceC(), ajk2.ceD(), ajk2.ceE()};
        Vec3f mS2 = soVar2.aby().mo23510mS(0.5f);
        Vec3f vec3f = new Vec3f(j.dot(vec3fArr[0]), j.dot(vec3fArr[1]), j.dot(vec3fArr[2]));
        float[][] fArr = (float[][]) Array.newInstance(Float.TYPE, new int[]{3, 3});
        int i = 0;
        while (true) {
            int i2 = i;
            if (i2 >= 3) {
                break;
            }
            for (int i3 = 0; i3 < 3; i3++) {
                fArr[i2][i3] = vec3fArr[i2].dot(vector3fArr[i3]);
            }
            i = i2 + 1;
        }
        for (int i4 = 0; i4 < 3; i4++) {
            if (m20577iS(vec3f.get(i4)) > mS.get(i4) + (mS2.x * m20577iS(fArr[i4][0])) + (mS2.y * m20577iS(fArr[i4][1])) + (mS2.z * m20577iS(fArr[i4][2]))) {
                return false;
            }
        }
        for (int i5 = 0; i5 < 3; i5++) {
            if (m20577iS((vec3f.x * fArr[0][i5]) + (vec3f.y * fArr[1][i5]) + (vec3f.z * fArr[2][i5])) > (mS.x * m20577iS(fArr[0][i5])) + (mS.y * m20577iS(fArr[1][i5])) + (mS.z * m20577iS(fArr[2][i5])) + mS2.get(i5)) {
                return false;
            }
        }
        if (m20577iS((vec3f.z * fArr[1][0]) - (vec3f.y * fArr[2][0])) > (mS.y * m20577iS(fArr[2][0])) + (mS.z * m20577iS(fArr[1][0])) + (mS2.y * m20577iS(fArr[0][2])) + (mS2.z * m20577iS(fArr[0][1]))) {
            return false;
        }
        if (m20577iS((vec3f.z * fArr[1][1]) - (vec3f.y * fArr[2][1])) > (mS.y * m20577iS(fArr[2][1])) + (mS.z * m20577iS(fArr[1][1])) + (mS2.x * m20577iS(fArr[0][2])) + (mS2.z * m20577iS(fArr[0][0]))) {
            return false;
        }
        if (m20577iS((vec3f.z * fArr[1][2]) - (vec3f.y * fArr[2][2])) > (mS.y * m20577iS(fArr[2][2])) + (mS.z * m20577iS(fArr[1][2])) + (mS2.x * m20577iS(fArr[0][1])) + (mS2.y * m20577iS(fArr[0][0]))) {
            return false;
        }
        if (m20577iS((vec3f.x * fArr[2][0]) - (vec3f.z * fArr[0][0])) > (mS.x * m20577iS(fArr[2][0])) + (mS.z * m20577iS(fArr[0][0])) + (mS2.y * m20577iS(fArr[1][2])) + (mS2.z * m20577iS(fArr[1][1]))) {
            return false;
        }
        if (m20577iS((vec3f.x * fArr[2][1]) - (vec3f.z * fArr[0][1])) > (mS.x * m20577iS(fArr[2][1])) + (mS.z * m20577iS(fArr[0][1])) + (mS2.x * m20577iS(fArr[1][2])) + (mS2.z * m20577iS(fArr[1][0]))) {
            return false;
        }
        if (m20577iS((vec3f.x * fArr[2][2]) - (vec3f.z * fArr[0][2])) > (mS.x * m20577iS(fArr[2][2])) + (mS.z * m20577iS(fArr[0][2])) + (mS2.x * m20577iS(fArr[1][1])) + (mS2.y * m20577iS(fArr[1][0]))) {
            return false;
        }
        if (m20577iS((vec3f.y * fArr[0][0]) - (vec3f.x * fArr[1][0])) > (mS.x * m20577iS(fArr[1][0])) + (mS.y * m20577iS(fArr[0][0])) + (mS2.y * m20577iS(fArr[2][2])) + (mS2.z * m20577iS(fArr[2][1]))) {
            return false;
        }
        if (m20577iS((vec3f.y * fArr[0][1]) - (vec3f.x * fArr[1][1])) > (mS.x * m20577iS(fArr[1][1])) + (mS.y * m20577iS(fArr[0][1])) + (mS2.x * m20577iS(fArr[2][2])) + (mS2.z * m20577iS(fArr[2][0]))) {
            return false;
        }
        float iS = (mS.x * m20577iS(fArr[1][2])) + (mS.y * m20577iS(fArr[0][2]));
        float iS2 = (mS2.x * m20577iS(fArr[2][1])) + (mS2.y * m20577iS(fArr[2][0]));
        if (m20577iS((vec3f.y * fArr[0][2]) - (fArr[1][2] * vec3f.x)) > iS + iS2) {
            return false;
        }
        C0763Kt bcE = C0763Kt.bcE();
        bcE.bcH().push();
        bcE.bcI().push();
        try {
            Vec3f vec3f2 = (Vec3f) bcE.bcH().get();
            Vec3d ajr = (Vec3d) bcE.bcI().get();
            vec3f2.set(ajk2.m03 - ajk.m03, ajk2.m13 - ajk.m13, ajk2.m23 - ajk.m23);
            ajr.mo9547u((Tuple3f) vec3f2).mo9487aa(0.5d).add(iTVar.mo7940Ee());
            iTVar.mo7945a(vec3f2, ajr, 1.0f);
            bcE.bcH().pop();
            bcE.bcI().pop();
            return true;
        } catch (Throwable th) {
            bcE.bcH().pop();
            bcE.bcI().pop();
            throw th;
        }
    }

    /* renamed from: iS */
    private float m20577iS(float f) {
        return Math.abs(f);
    }
}
