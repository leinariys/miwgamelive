package logic.bbb;

import com.hoplon.geometry.BoundingBox;
import com.hoplon.geometry.Vec3f;
import game.geometry.Matrix4fWrap;
import p001a.C2678iT;

import javax.vecmath.Tuple3f;

/* renamed from: a.aTH */
/* compiled from: a */
public class aTH implements aVJ<C3601so, aOU> {
    /* renamed from: a */
    public boolean mo7270a(Matrix4fWrap ajk, C3601so soVar, Matrix4fWrap ajk2, aOU aou, C2678iT iTVar) {
        Vec3f aH = ajk.mo13992aH(ajk2.mo14004c((Tuple3f) aou.dnY()));
        Vec3f aH2 = ajk.mo13992aH(ajk2.mo14004c((Tuple3f) aou.dnZ()));
        Vec3f j = aH2.mo23506j(aH);
        Vec3f dfO = j.dfO();
        Vec3f mS = aH2.mo23503i(aH).mo23510mS(0.5f);
        float f = soVar.aby().x;
        float f2 = soVar.aby().y;
        float f3 = soVar.aby().z;
        return mo11485a(dfO, mS, j.length() / 2.0f, new BoundingBox((-f) / 2.0f, (-f2) / 2.0f, (-f3) / 2.0f, f / 2.0f, f2 / 2.0f, f3 / 2.0f));
    }

    /* renamed from: a */
    public boolean mo11485a(Vec3f vec3f, Vec3f vec3f2, float f, BoundingBox boundingBox) {
        Vec3f j = boundingBox.bny().mo23506j(vec3f2);
        float dio = boundingBox.dio() / 2.0f;
        if (m18336iS(j.x) > (m18336iS(vec3f.x) * f) + dio) {
            return false;
        }
        float dip = boundingBox.dip() / 2.0f;
        if (m18336iS(j.y) > (m18336iS(vec3f.y) * f) + dip) {
            return false;
        }
        float diq = boundingBox.diq() / 2.0f;
        if (m18336iS(j.z) > (m18336iS(vec3f.z) * f) + diq) {
            return false;
        }
        if (m18336iS((j.y * vec3f.z) - (j.z * vec3f.y)) > (m18336iS(vec3f.z) * dip) + (m18336iS(vec3f.y) * diq)) {
            return false;
        }
        if (m18336iS((j.z * vec3f.x) - (j.x * vec3f.z)) > (diq * m18336iS(vec3f.x)) + (m18336iS(vec3f.z) * dio)) {
            return false;
        }
        if (m18336iS((j.x * vec3f.y) - (j.y * vec3f.x)) <= (dio * m18336iS(vec3f.y)) + (dip * m18336iS(vec3f.x))) {
            return true;
        }
        return false;
    }

    /* renamed from: iS */
    private float m18336iS(float f) {
        return Math.abs(f);
    }
}
