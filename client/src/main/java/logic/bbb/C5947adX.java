package logic.bbb;

import com.hoplon.geometry.Vec3f;
import game.geometry.Matrix4fWrap;
import game.geometry.Vec3d;
import p001a.C0763Kt;
import p001a.C2678iT;
import p001a.C3381qt;
import p001a.C5780aaM;

import javax.vecmath.Tuple3f;

/* renamed from: a.adX  reason: case insensitive filesystem */
/* compiled from: a */
public class C5947adX implements aVJ<C4029yK, C4029yK> {
    private static final int cRb = 128;
    private final Vec3f aAq = new Vec3f();
    private final C3381qt aVP = new C3381qt();
    private C5780aaM aAn;

    /* JADX INFO: finally extract failed */
    /* renamed from: a */
    public boolean mo7270a(Matrix4fWrap ajk, C4029yK yKVar, Matrix4fWrap ajk2, C4029yK yKVar2, C2678iT iTVar) {
        if (this.aAn == null) {
            this.aAn = new C5780aaM();
        }
        this.aAn.mo12175a(yKVar, yKVar2);
        m20911a(ajk, ajk2, this.aAn.bMM());
        this.aAn.mo12182f(this.aAn.bMM());
        if (!mo12881a(this.aAn)) {
            return false;
        }
        C0763Kt bcE = C0763Kt.bcE();
        bcE.bcI().push();
        bcE.bcH().push();
        try {
            Vec3f vec3f = (Vec3f) bcE.bcH().get();
            Vec3d ajr = (Vec3d) bcE.bcI().get();
            vec3f.set(ajk2.m03 - ajk.m03, ajk2.m13 - ajk.m13, ajk2.m23 - ajk.m23);
            ajr.mo9547u((Tuple3f) vec3f).mo9487aa(0.5d).add(iTVar.mo7940Ee());
            vec3f.normalize();
            iTVar.mo7945a(vec3f, ajr, 1.0f);
            bcE.bcI().pop();
            bcE.bcH().pop();
            return true;
        } catch (Throwable th) {
            bcE.bcI().pop();
            bcE.bcH().pop();
            throw th;
        }
    }

    /* renamed from: a */
    public boolean mo12881a(C5780aaM aam) {
        this.aVP.reset();
        aam.mo12186w(this.aVP.bBi(), this.aAq);
        this.aVP.mo21522ai(this.aAq);
        int i = 0;
        while (this.aVP.getSize() != 4 && i < 128) {
            aam.getFarthest(this.aVP.bBi(), this.aAq);
            if (this.aAq.dot(this.aVP.bBi()) < 0.0f) {
                return false;
            }
            if (!this.aVP.mo21522ai(this.aAq)) {
                return true;
            }
            i++;
        }
        return false;
    }

    /* renamed from: a */
    private void m20911a(Matrix4fWrap ajk, Matrix4fWrap ajk2, Matrix4fWrap ajk3) {
        ajk3.mo14045m(ajk).mo14044l(ajk2);
    }

    public C3381qt bTp() {
        return this.aVP;
    }

    /* renamed from: ME */
    public C5780aaM mo12880ME() {
        return this.aAn;
    }
}
