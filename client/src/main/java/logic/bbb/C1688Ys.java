package logic.bbb;

import game.geometry.Matrix4fWrap;
import p001a.C0763Kt;
import p001a.C2678iT;
import p001a.C4084zF;

/* renamed from: a.Ys */
/* compiled from: a */
public class C1688Ys implements aVJ<C4029yK, C4029yK> {
    public final C0763Kt stack = C0763Kt.bcE();
    private C4084zF eMc;

    /* renamed from: a */
    public boolean mo7270a(Matrix4fWrap ajk, C4029yK yKVar, Matrix4fWrap ajk2, C4029yK yKVar2, C2678iT iTVar) {
        Matrix4fWrap ajk3;
        Matrix4fWrap ajk4;
        C0899ND nd;
        C3165ob obVar;
        if (!(yKVar instanceof C3165ob) || !(yKVar2 instanceof C0899ND)) {
            ajk3 = null;
            ajk4 = null;
            nd = null;
            obVar = null;
        } else {
            nd = (C0899ND) yKVar2;
            ajk3 = ajk2;
            ajk4 = ajk;
            obVar = (C3165ob) yKVar;
        }
        if ((yKVar instanceof C0899ND) && (yKVar2 instanceof C3165ob)) {
            ajk3 = ajk;
            ajk4 = ajk2;
            nd = (C0899ND) yKVar;
            obVar = (C3165ob) yKVar2;
        }
        if (obVar == null || nd == null) {
            throw new IllegalArgumentException("Convex concave need right params");
        }
        float margin = obVar.getMargin();
        this.eMc = new C4084zF(ajk4, obVar, ajk3, nd);
        this.eMc.mo23266a(margin, iTVar);
        nd.mo809a(this.eMc, this.eMc.asv(), this.eMc.asw());
        return iTVar.mo7941Ef();
    }
}
