package logic.bbb;

import game.network.channel.client.ClientConnect;
import game.network.message.externalizable.C5572aOm;
import logic.baa.aDJ;
import logic.res.html.C6663arL;
import logic.thred.LogPrinter;
import org.apache.commons.logging.Log;
import p001a.C1875af;
import p001a.C6014aem;

import java.util.WeakHashMap;
import java.util.concurrent.Semaphore;

/* renamed from: a.aDR */
/* compiled from: a */
public class aDR implements C5572aOm<aDR> {
    private static final Log logger = LogPrinter.setClass(aDR.class);
    C6663arL hDb;
    boolean hDd = false;
    boolean hge = false;
    private ClientConnect dyE;
    private Semaphore hDc = new Semaphore(1);
    private WeakHashMap<C6663arL, Object> hDe = new WeakHashMap<>();
    private C1780a hDf = C1780a.Client;

    public aDR(ClientConnect bVar, C6663arL arl) {
        this.dyE = bVar;
        this.hDb = arl;
    }

    public ClientConnect bgt() {
        return this.dyE;
    }

    /* renamed from: o */
    public void mo8405o(ClientConnect bVar) {
        this.dyE = bVar;
    }

    public C6663arL cWq() {
        return this.hDb;
    }

    /* renamed from: d */
    public void mo8399d(C6663arL arl) {
        this.hDb = arl;
    }

    public boolean equals(Object obj) {
        if (!(obj instanceof aDR)) {
            return false;
        }
        aDR adr = (aDR) obj;
        if (this == adr) {
            return true;
        }
        if (this.hDb != adr.hDb || (this.dyE != adr.dyE && !this.dyE.equals(adr.dyE))) {
            return false;
        }
        return true;
    }

    public int hashCode() {
        return this.dyE.hashCode();
    }

    public boolean cST() {
        return this.hge;
    }

    /* JADX INFO: finally extract failed */
    /* renamed from: e */
    public boolean mo8400e(C6663arL arl) {
        this.hDc.acquire();
        try {
            if (this.hDd) {
                logger.debug("Trying to add " + arl.bFY() + " to proxy list of disconnected " + this.dyE);
            } else if (!this.hDe.containsKey(arl)) {
                C6014aem a = ((C1875af) arl).mo13200a(this);
                this.hDe.put(arl, a);
                a.fnt = true;
                this.hDc.release();
                return true;
            }
            this.hDc.release();
            return false;
        } catch (Throwable th) {
            this.hDc.release();
            throw th;
        }
    }

    /* renamed from: s */
    public boolean mo8406s(aDJ adj) {
        try {
            this.hDc.acquire();
            try {
                return this.hDe.containsKey(adj.bFf());
            } finally {
                this.hDc.release();
            }
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
    }

    /* renamed from: f */
    public boolean mo8402f(C6663arL arl) {
        try {
            this.hDc.acquire();
            try {
                return this.hDe.containsKey(arl);
            } finally {
                this.hDc.release();
            }
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
    }

    /* renamed from: g */
    public void mo8403g(C6663arL arl) {
        this.hDc.acquire();
        try {
            this.hDe.remove(arl);
            if (arl instanceof C1875af) {
                ((C1875af) arl).mo13214b(this);
            }
        } finally {
            this.hDc.release();
        }
    }

    public void cleanUp() {
        this.hDc.acquire();
        try {
            this.hDd = true;
            for (C6663arL next : this.hDe.keySet()) {
                if (next != null) {
                    ((C1875af) next).mo13214b(this);
                }
            }
            this.hDe.clear();
        } finally {
            this.hDc.release();
        }
    }

    public C1780a cWr() {
        return this.hDf;
    }

    /* renamed from: a */
    public void mo8391a(C1780a aVar) {
        this.hDf = aVar;
    }

    public boolean cWs() {
        return this.hDf == C1780a.OffloadNode;
    }

    /* renamed from: cWt */
    public aDR mo8390Jq() {
        return this;
    }

    /* renamed from: a.aDR$a */
    enum C1780a {
        Server,
        Client,
        OffloadNode
    }
}
