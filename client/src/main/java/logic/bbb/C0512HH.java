package logic.bbb;

import com.hoplon.geometry.BoundingBox;
import com.hoplon.geometry.Vec3f;
import game.geometry.Matrix4fWrap;
import p001a.C0763Kt;
import p001a.C3978xf;

import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;

/* renamed from: a.HH */
/* compiled from: a */
public class C0512HH extends C4029yK {

    public transient C0513a[] gRD;
    private transient C0763Kt stack = C0763Kt.bcE();

    public C0512HH() {
    }

    public C0512HH(C0513a[] aVarArr) {
        this.gRD = aVarArr;
        if (aVarArr != null) {
            float f = 0.0f;
            for (C0513a IL : aVarArr) {
                f += IL.mo2519IL().getVolume();
            }
            this.volume = f;
        }
    }

    /* renamed from: Jo */
    public C0513a[] mo2516Jo() {
        return this.gRD;
    }

    public byte getShapeType() {
        return 7;
    }

    /* access modifiers changed from: protected */
    public void growBoundingBox(Matrix4fWrap ajk, BoundingBox boundingBox) {
        BoundingBox boundingBox2 = new BoundingBox();
        for (C0513a aVar : this.gRD) {
            aVar.cYZ.growBoundingBox(aVar.transform, boundingBox2);
        }
        boundingBox2.mo23426b(ajk, boundingBox);
    }

    public void getFarthest(Vec3f vec3f, Vec3f vec3f2) {
        float f;
        if (this.stack == null) {
            this.stack = C0763Kt.bcE();
        }
        this.stack.bcH().push();
        try {
            Vec3f vec3f3 = (Vec3f) this.stack.bcH().get();
            float f2 = 0.0f;
            C0513a[] Jo = mo2516Jo();
            int length = Jo.length;
            int i = 0;
            while (i < length) {
                C0513a aVar = Jo[i];
                this.stack.bcH().push();
                aVar.getTransform().mo13979C(vec3f, vec3f3);
                Vec3f vec3f4 = (Vec3f) this.stack.bcH().get();
                aVar.mo2519IL().getFarthest(vec3f3, vec3f4);
                aVar.getTransform().transform(vec3f4);
                float dot = vec3f4.dot(vec3f);
                if (dot >= f2) {
                    vec3f2.set(vec3f4);
                    f = dot;
                } else {
                    f = f2;
                }
                i++;
                f2 = f;
            }
            this.stack.bcH().pop();
        } finally {
            this.stack.bcH().pop();
        }
    }

    public void calculateLocalInertia(float f, Vec3f vec3f) {
    }

    public void getAabb(C3978xf xfVar, Vec3f vec3f, Vec3f vec3f2) {
    }

    public Vec3f getLocalScaling() {
        return null;
    }

    public void setLocalScaling(Vec3f vec3f) {
    }

    /* renamed from: a.HH$a */
    public static class C0513a implements Serializable {

        /* access modifiers changed from: private */
        public C4029yK cYZ;
        /* access modifiers changed from: private */
        public Matrix4fWrap transform = new Matrix4fWrap();

        public Matrix4fWrap getTransform() {
            return this.transform;
        }

        public void setTransform(Matrix4fWrap ajk) {
            this.transform.set(ajk);
        }

        /* renamed from: IL */
        public C4029yK mo2519IL() {
            return this.cYZ;
        }

        /* renamed from: b */
        public void mo2520b(C4029yK yKVar) {
            this.cYZ = yKVar;
        }

        private void writeObject(ObjectOutputStream objectOutputStream) {
            objectOutputStream.writeObject(this.transform);
            objectOutputStream.writeObject(this.cYZ);
        }

        private void readObject(ObjectInputStream objectInputStream) {
            this.transform = (Matrix4fWrap) objectInputStream.readObject();
            this.cYZ = (C4029yK) objectInputStream.readObject();
        }
    }
}
