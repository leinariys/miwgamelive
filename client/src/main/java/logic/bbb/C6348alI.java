package logic.bbb;

import com.hoplon.geometry.BoundingBox;
import com.hoplon.geometry.Vec3f;
import game.geometry.Matrix4fWrap;
import logic.res.KeyCode;
import p001a.C0128Bb;

/* renamed from: a.alI  reason: case insensitive filesystem */
/* compiled from: a */
public class C6348alI extends C5934adK {

    private float radius;

    public C6348alI() {
    }

    public C6348alI(float f) {
        this.radius = f;
        super.setVolume((float) (((double) (1.3333334f * f * f * f)) * 3.141592653589793d));
    }

    public float getOuterSphereRadius() {
        return this.radius;
    }

    public float getRadius() {
        return this.radius;
    }

    public void setRadius(float f) {
        this.radius = f;
        super.setVolume((float) (((double) (1.3333334f * f * f * f)) * 3.141592653589793d));
    }

    public String toString() {
        return "Sphere [radius:" + this.radius + "]";
    }

    public byte getShapeType() {
        return 1;
    }

    /* access modifiers changed from: protected */
    public void growBoundingBox(Matrix4fWrap ajk, BoundingBox boundingBox) {
        boundingBox.addPoint(ajk.m03 + getRadius(), ajk.m13 + getRadius(), ajk.m23 + getRadius());
        boundingBox.addPoint(ajk.m03 + (-getRadius()), ajk.m13 + (-getRadius()), ajk.m23 + (-getRadius()));
    }

    public void getFarthest(Vec3f vec3f, Vec3f vec3f2) {
        vec3f2.set(vec3f);
        vec3f2.normalize();
        vec3f2.mo23511mT(getRadius());
    }

    public C1188RY toMesh() {
        float f = this.radius;
        Vec3f[] vec3fArr = new Vec3f[42];
        int[] iArr = new int[KeyCode.csT];
        int i = 0;
        int i2 = 0;
        for (int i3 = 0; i3 <= 6; i3++) {
            float cos = (float) (((double) f) * Math.cos((((double) i3) * 3.141592653589793d) / ((double) 6)));
            float sin = (float) (((double) f) * Math.sin((((double) i3) * 3.141592653589793d) / ((double) 6)));
            int i4 = 0;
            while (i4 < 6) {
                int i5 = i + 1;
                vec3fArr[i] = new Vec3f((float) (((double) sin) * Math.cos((((double) (i4 * 2)) * 3.141592653589793d) / ((double) 6))), cos, (float) (((double) sin) * Math.sin((((double) (i4 * 2)) * 3.141592653589793d) / ((double) 6))));
                if (i3 > 0) {
                    int i6 = (i4 + 1) % 6;
                    int i7 = i2 + 1;
                    iArr[i2] = (i3 * 6) + i6;
                    int i8 = i7 + 1;
                    iArr[i7] = (i3 * 6) + i4;
                    int i9 = i8 + 1;
                    iArr[i8] = ((i3 - 1) * 6) + i4;
                    int i10 = i9 + 1;
                    iArr[i9] = (i3 * 6) + i6;
                    int i11 = i10 + 1;
                    iArr[i10] = ((i3 - 1) * 6) + i4;
                    i2 = i11 + 1;
                    iArr[i11] = i6 + ((i3 - 1) * 6);
                }
                i4++;
                i = i5;
            }
        }
        return new C1188RY(vec3fArr, iArr);
    }

    public void calculateLocalInertia(float f, Vec3f vec3f) {
        float margin = 0.4f * f * getMargin() * getMargin();
        vec3f.set(margin, margin, margin);
    }

    public float getMargin() {
        return getRadius();
    }

    public void setMargin(float f) {
        super.setMargin(f);
    }

    public Vec3f localGetSupportingVertexWithoutMargin(Vec3f vec3f) {
        return C0128Bb.dMU;
    }

    public void batchedUnitVectorGetSupportingVertexWithoutMargin(Vec3f[] vec3fArr, Vec3f[] vec3fArr2, int i) {
        for (int i2 = 0; i2 < i; i2++) {
            vec3fArr2[i2].set(0.0f, 0.0f, 0.0f);
        }
    }
}
