package logic.bbb;

import com.hoplon.geometry.Vec3f;
import p001a.C3978xf;

/* renamed from: a.ob */
/* compiled from: a */
public abstract class C3165ob extends C4029yK {
    public abstract void batchedUnitVectorGetSupportingVertexWithoutMargin(Vec3f[] vec3fArr, Vec3f[] vec3fArr2, int i);

    public abstract void getAabbSlow(C3978xf xfVar, Vec3f vec3f, Vec3f vec3f2);

    public abstract float getMargin();

    public abstract void setMargin(float f);

    public abstract int getNumPreferredPenetrationDirections();

    public abstract void getPreferredPenetrationDirection(int i, Vec3f vec3f);

    public abstract Vec3f localGetSupportingVertex(Vec3f vec3f);

    public abstract Vec3f localGetSupportingVertexWithoutMargin(Vec3f vec3f);
}
