package logic.bbb;

import game.geometry.Matrix4fWrap;
import p001a.C0763Kt;
import p001a.C1037PD;
import p001a.C2678iT;

/* renamed from: a.by */
/* compiled from: a */
public class C2105by implements aVJ<C4029yK, C4029yK> {

    /* renamed from: ox */
    private final C1037PD f5944ox;

    public C2105by(C1037PD pd) {
        this.f5944ox = pd;
    }

    /* JADX INFO: finally extract failed */
    /* renamed from: a */
    public boolean mo7270a(Matrix4fWrap ajk, C4029yK yKVar, Matrix4fWrap ajk2, C4029yK yKVar2, C2678iT iTVar) {
        boolean z = false;
        if ((yKVar instanceof C0512HH) && (yKVar2 instanceof C0512HH)) {
            C0512HH hh = (C0512HH) yKVar;
            C0512HH hh2 = (C0512HH) yKVar2;
            C0763Kt bcE = C0763Kt.bcE();
            bcE.bcL().push();
            try {
                Matrix4fWrap ajk3 = (Matrix4fWrap) bcE.bcL().get();
                Matrix4fWrap ajk4 = (Matrix4fWrap) bcE.bcL().get();
                C0512HH.C0513a[] Jo = hh.mo2516Jo();
                int length = Jo.length;
                int i = 0;
                boolean z2 = false;
                while (i < length) {
                    C0512HH.C0513a aVar = Jo[i];
                    ajk3.mul(ajk, aVar.getTransform());
                    C0512HH.C0513a[] Jo2 = hh2.mo2516Jo();
                    int length2 = Jo2.length;
                    int i2 = 0;
                    boolean z3 = z2;
                    while (i2 < length2) {
                        C0512HH.C0513a aVar2 = Jo2[i2];
                        ajk4.mul(ajk2, aVar2.getTransform());
                        i2++;
                        z3 |= this.f5944ox.mo4616a(ajk3, aVar.mo2519IL(), ajk4, aVar2.mo2519IL(), iTVar);
                    }
                    i++;
                    z2 = z3;
                }
                return z2;
            } finally {
                bcE.bcL().pop();
            }
        } else if (yKVar instanceof C0512HH) {
            C0512HH hh3 = (C0512HH) yKVar;
            C0763Kt bcE2 = C0763Kt.bcE();
            bcE2.bcL().push();
            try {
                Matrix4fWrap ajk5 = (Matrix4fWrap) bcE2.bcL().get();
                for (C0512HH.C0513a aVar3 : hh3.mo2516Jo()) {
                    ajk5.mul(ajk, aVar3.getTransform());
                    z |= this.f5944ox.mo4616a(ajk5, aVar3.mo2519IL(), ajk2, yKVar2, iTVar);
                }
                bcE2.bcL().pop();
                return z;
            } catch (Throwable th) {
                bcE2.bcL().pop();
                throw th;
            }
        } else if (!(yKVar2 instanceof C0512HH)) {
            return false;
        } else {
            C0512HH hh4 = (C0512HH) yKVar2;
            C0763Kt bcE3 = C0763Kt.bcE();
            bcE3.bcL().push();
            try {
                Matrix4fWrap ajk6 = (Matrix4fWrap) bcE3.bcL().get();
                for (C0512HH.C0513a aVar4 : hh4.mo2516Jo()) {
                    ajk6.mul(ajk2, aVar4.getTransform());
                    z |= this.f5944ox.mo4616a(ajk, yKVar, ajk6, aVar4.mo2519IL(), iTVar);
                }
                bcE3.bcL().pop();
                return z;
            } catch (Throwable th2) {
                bcE3.bcL().pop();
                throw th2;
            }
        }
    }
}
