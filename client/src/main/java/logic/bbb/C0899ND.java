package logic.bbb;

import com.hoplon.geometry.Vec3f;
import p001a.aTC;

/* renamed from: a.ND */
/* compiled from: a */
public abstract class C0899ND extends C4029yK {
    public float collisionMargin = 0.0f;

    /* renamed from: a */
    public abstract void mo809a(aTC atc, Vec3f vec3f, Vec3f vec3f2);

    public float getMargin() {
        return this.collisionMargin;
    }

    public void setMargin(float f) {
        this.collisionMargin = f;
    }
}
