package logic.bbb;

import com.hoplon.geometry.BoundingBox;
import game.network.message.externalizable.C5572aOm;
import p001a.C1417Ul;
import p001a.C2606hU;
import p001a.C2614hc;
import taikodom.render.loader.provider.FilePath;

import java.io.*;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/* renamed from: a.ka */
/* compiled from: a */
public class C2820ka extends C0512HH implements C5572aOm<C2820ka> {
    private static final Map<String, C0512HH.C0513a[]> aqk = new ConcurrentHashMap();


    /* renamed from: Nk */
    private String f8427Nk;
    private String filename;

    public C2820ka() {
        setFileName((String) null);
    }

    public C2820ka(String str) {
        setFileName(str);
    }

    /* renamed from: a */
    public static C0512HH.C0513a[] m34455a(C2820ka kaVar) {
        C0512HH.C0513a[] aVarArr = aqk.get(kaVar.filename);
        if (aVarArr == null) {
            try {
                aVarArr = C2614hc.m32762b(kaVar.m34454Jn());
                aqk.put(kaVar.filename, aVarArr);
                if (aVarArr.length == 0) {
                    System.out.println("No shape data for: " + kaVar.filename);
                }
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        }
        return aVarArr;
    }

    public void setFileName(String str) {
        this.filename = str;
        this.f8427Nk = null;
    }

    public boolean equals(Object obj) {
        if (obj instanceof C2820ka) {
            return this.filename.equals(((C2820ka) obj).filename);
        }
        return super.equals(obj);
    }

    public int hashCode() {
        int i = 0;
        int hashCode = (this.filename != null ? this.filename.hashCode() : 0) * 31;
        if (this.f8427Nk != null) {
            i = this.f8427Nk.hashCode();
        }
        return hashCode + (i * 31);
    }

    public String getContent() {
        if (this.f8427Nk == null) {
            try {
                StringBuffer stringBuffer = new StringBuffer(1000);
                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(m34454Jn().openInputStream()));
                char[] cArr = new char[1024];
                while (true) {
                    int read = bufferedReader.read(cArr);
                    if (read == -1) {
                        break;
                    }
                    stringBuffer.append(cArr, 0, read);
                }
                bufferedReader.close();
                this.f8427Nk = stringBuffer.toString();
            } catch (FileNotFoundException e) {
                throw new C1417Ul("PHY file not found " + this.filename, e);
            } catch (IOException e2) {
                throw new C1417Ul("Error opening PHY file " + this.filename, e2);
            }
        }
        return this.f8427Nk;
    }

    /* renamed from: Jn */
    private FilePath m34454Jn() {
        return C2606hU.getRootPathResource().concat(this.filename);
    }

    public float getVolume() {
        if (this.volume == 0.0f) {
            mo2516Jo();
        }
        return super.getVolume();
    }

    /* renamed from: Jo */
    public C0512HH.C0513a[] mo2516Jo() {
        if (this.gRD == null) {
            this.gRD = m34455a(this);
            if (this.gRD != null) {
                float f = 0.0f;
                for (C0512HH.C0513a IL : this.gRD) {
                    f += IL.mo2519IL().getVolume();
                }
                this.volume = f;
            }
        }
        return this.gRD;
    }

    public String getFilename() {
        return this.filename;
    }

    public String toString() {
        return "PhyShape [" + this.filename + "]";
    }

    public BoundingBox getBoundingBox() {
        if (this.boundingBox == null) {
            this.boundingBox = m34456b(this);
        }
        return this.boundingBox;
    }

    /* renamed from: b */
    private synchronized BoundingBox m34456b(C2820ka kaVar) {
        return mo20092a(mo2516Jo());
    }

    /* renamed from: a */
    public BoundingBox mo20092a(C0512HH.C0513a[] aVarArr) {
        BoundingBox boundingBox = new BoundingBox();
        boundingBox.reset();
        for (C0512HH.C0513a aVar : aVarArr) {
            aVar.mo2519IL().growBoundingBox(aVar.getTransform(), boundingBox);
        }
        if (Float.isInfinite(boundingBox.length())) {
            boundingBox.addPoint(-10.0f, -10.0f, -10.0f);
            boundingBox.addPoint(10.0f, 10.0f, 10.0f);
        }
        return boundingBox;
    }

    /* renamed from: Jp */
    public C2820ka mo8390Jq() {
        BoundingBox dqR;
        C2820ka kaVar = new C2820ka();
        if (this.boundingBox == null) {
            dqR = null;
        } else {
            dqR = this.boundingBox.clone();
        }
        kaVar.boundingBox = dqR;
        kaVar.f8427Nk = this.f8427Nk;
        kaVar.filename = this.filename;
        kaVar.gRD = this.gRD;
        kaVar.volume = this.volume;
        return kaVar;
    }

    private void writeObject(ObjectOutputStream objectOutputStream) {
        if (this.filename != null) {
            objectOutputStream.writeBoolean(true);
            objectOutputStream.writeObject(this.filename);
        } else {
            objectOutputStream.writeBoolean(false);
        }
        if (this.f8427Nk != null) {
            objectOutputStream.writeBoolean(true);
            objectOutputStream.writeObject(this.f8427Nk);
            return;
        }
        objectOutputStream.writeBoolean(false);
    }

    private void readObject(ObjectInputStream objectInputStream) {
        if (objectInputStream.readBoolean()) {
            this.filename = (String) objectInputStream.readObject();
        }
        if (objectInputStream.readBoolean()) {
            this.f8427Nk = (String) objectInputStream.readObject();
        }
    }
}
