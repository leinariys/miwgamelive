package logic.bbb;

import com.hoplon.geometry.Vec3f;
import game.geometry.Matrix4fWrap;
import p001a.C2678iT;

/* renamed from: a.bB */
/* compiled from: a */
public class C2023bB implements aVJ<C6348alI, C6348alI> {
    /* renamed from: a */
    public boolean mo7270a(Matrix4fWrap ajk, C6348alI ali, Matrix4fWrap ajk2, C6348alI ali2, C2678iT iTVar) {
        float radius = ali2.getRadius();
        Vec3f ceH = ajk2.ceH();
        float radius2 = radius + ali.getRadius();
        if (Vec3f.direction(ceH, ajk.ceH()) <= radius2 * radius2) {
            return true;
        }
        return false;
    }
}
