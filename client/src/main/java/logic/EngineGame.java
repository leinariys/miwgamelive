package logic;

import game.engine.*;
import game.network.channel.NetworkChannel;
import game.script.PlayerController;
import game.script.Taikodom;
import game.script.login.UserConnection;
import game.script.player.Player;
import game.script.player.User;
import logic.render.GUIModule;
import logic.render.IEngineGraphics;
import logic.res.ConfigIniKeyValue;
import logic.res.ConfigManager;
import logic.res.ILoaderTrail;
import logic.res.code.SoftTimer;
import lombok.extern.slf4j.Slf4j;
import taikodom.render.loader.provider.FilePath;

import java.util.concurrent.Executor;

/**
 * Содержит менеджер настроек
 * Путь к игре
 * Путь к коду отрисовки
 * Список задачь для потоков
 * Менеджер загрузаки файлов .trail
 * Addon manager
 * class aou
 */
/* renamed from: a.aou  reason: case insensitive filesystem */
/* compiled from: a */
    @Slf4j
public class EngineGame implements IEngineGame {

    /**
     * Список задач
     */
    Executor gia;
    private Taikodom bFZ;
    private DataGameEvent cVF;
    private C1175RP dTs;
    private SoftTimer dej;
    private FilePath rootPath;
    private NetworkChannel fEJ;
    private GUIModule guiModule;
    /**
     * EventManager Блокировщик чего-то или выполнитель задачь в главном потоке для графического движка
     */
    private EventManager eventManager;
    /**
     * Менеджер настроек
     */
    private ConfigManager configManager;
    private boolean isExit = true;
    private boolean ghU = true;
    /**
     * Значение настройки
     * Пример [console] verbose=true
     */
    private boolean verbose = false;
    private ILoaderTrail loaderTrail;
    private IAddonManager addonManager;
    private C3126oE ghY;
    private boolean ghZ = true;
    private NetworkChannel networkChannel;

    /* renamed from: lV */
    private IEngineGraphics engineGraphics;

    /* renamed from: ng */
    private FilePath rootPathRender;

    /**
     * Передаём дирректории
     *
     * @param rootPath       Главный каталог
     * @param rootPathRender каталог с отрисовкой графики
     */
    public EngineGame(FilePath rootPath, FilePath rootPathRender) {
        this.rootPath = rootPath;
        this.rootPathRender = rootPathRender;
    }

    /* renamed from: d */
    public void mo15322d(DataGameEvent jz) {
        mo15311c(jz);
        this.bFZ = (Taikodom) this.cVF.bGz().bFV();
    }

    public DataGameEvent bhc() {
        return this.cVF;
    }

    /* renamed from: c */
    public void mo15311c(DataGameEvent jz) {
        this.cVF = jz;
    }

    //ala
    public Taikodom getTaikodom() {
        return this.bFZ;
    }

    public User getUser() {// ala.aMl.bhe
        return getTaikodom().getUserConnection().getUser();
    }

    /* renamed from: dL */
    public Player mo4089dL() {
        if (getTaikodom() == null || getTaikodom().getUserConnection() == null) {
            return null;
        }
        return getTaikodom().getUserConnection().mo15388dL();
    }

    public PlayerController alb() {
        if (getTaikodom() == null || getTaikodom().getUserConnection() == null || getTaikodom().getUserConnection().mo15388dL() == null) {
            return null;
        }
        return getTaikodom().getUserConnection().mo15388dL().dxc();
    }

    /**
     * Получить GUIModule
     *
     * @return
     */
    public GUIModule getGuiModule() {
        return this.guiModule;
    }

    /**
     * Установить GUIModule
     *
     * @param guiModule
     */
    /* renamed from: a */
    public void setGUIModule(GUIModule guiModule) {
        this.guiModule = guiModule;
    }

    /**
     * Полусить ссылку на графический движок
     *
     * @return
     */
    public IEngineGraphics getEngineGraphics() {
        return this.engineGraphics;
    }

    /**
     * Установить Ссылка на графический движок
     *
     * @param engineGraphics графический движок
     */
    /* renamed from: d */
    public void setEngineGraphics(IEngineGraphics engineGraphics) {
        this.engineGraphics = engineGraphics;
    }

    public ConfigManager getConfigManager() {
        return this.configManager;
    }

    /**
     * Передали настройки
     *
     * @param configManager Менеджер настроек
     */
    /* renamed from: c */
    public void setConfigManager(ConfigManager configManager) {
        this.configManager = configManager;
    }

    public void bgY() {
        if (this.engineGraphics != null) {
            getEngineGraphics().adV();
            getEngineGraphics().mo3062dr(0.0f);
            getEngineGraphics().mo3063ds(0.0f);
        }
    }

    public void bha() {
        if (bhc() != null) {
            bhc().mo3335HY();
            mo15311c((DataGameEvent) null);
        }
    }

    public void cleanUp() {
        if (getTaikodom() != null) {
            bgY();
        }
        if (alb() != null) {
            alb().dispose();
        }
        if (getTaikodom().getPlayer() != null) {
            getTaikodom().getPlayer().mo14429ks(false);
        }
        UserConnection aMl = getTaikodom().getUserConnection();
        if (aMl != null) {
            aMl.cqk();
        }
    }

    public void disconnect() {
        bha();
        this.bFZ = null;
    }

    public void exit() {
        this.isExit = false;
    }

    public SoftTimer alf() {
        return this.dej;
    }

    /* renamed from: a */
    public void mo4066a(SoftTimer abg) {
        this.dej = abg;
    }

    public boolean isExit() {
        return this.isExit;
    }

    /**
     * Достаём значение [console] verbose
     * Пример [console] verbose=true
     */
    public void getVerbose() {
        if (this.configManager != null) {
            try {
                this.verbose = this.configManager.getSection(ConfigIniKeyValue.ENABLE_VERBOSE_MODE.getValue())
                        .mo7191a(ConfigIniKeyValue.ENABLE_VERBOSE_MODE, (Boolean) false)
                        .booleanValue();
            } catch (C5452aJw e) {
                log.warn("Console config section not found while trying to read verbose mode");
                this.verbose = false;
            }
        }
    }

    public boolean isVerbose() {
        return this.verbose;
    }

    /* renamed from: c */
    public void mo15313c(NetworkChannel ahg) {
        this.fEJ = ahg;
    }

    public NetworkChannel aPT() {
        return this.fEJ;
    }

    /**
     * Получить Менеджер загрузаки файлов .trail
     *
     * @return Менеджер загрузаки файлов .trail
     */
    public ILoaderTrail getLoaderTrail() {
        return this.loaderTrail;
    }

    /**
     * Установить Менеджер загрузаки файлов .trail
     *
     * @param loaderTrail Менеджер загрузаки файлов .trail
     */
    /* renamed from: a */
    public void mo4065a(ILoaderTrail loaderTrail) {
        this.loaderTrail = loaderTrail;
    }

    public Object getRoot() {
        return getTaikodom();
    }

    public boolean isPlayerNotNull() {
        return mo4089dL() != null;
    }

    public int getScreenWidth() {
        return getEngineGraphics().aes();
    }

    public int getScreenHeight() {
        return getEngineGraphics().aet();
    }

    public void initAllAddonWaitPlayer() {
        this.addonManager.initAllAddonWaitPlayer();
    }

    /**
     * EventManager Блокировщик чего-то или выполнитель задачь в главном потоке для графического движка
     *
     * @return
     */
    public C6245ajJ getEventManager() {
        return this.eventManager;
    }

    public C3658tN bgX() {
        return null;
    }

    public boolean bhf() {
        return this.ghU;
    }

    /* renamed from: fn */
    public void mo15325fn(boolean z) {
        this.ghU = z;
    }

    public boolean bhg() {
        return (aPT() == null || bhc() == null || !aPT().isUp()) ? false : true;
    }

    /* renamed from: a */
    public void mo15305a(IAddonManager addonManager) {
        this.addonManager = addonManager;
    }

    /**
     * @param eventManager EventManager Блокировщик чего-то или выполнитель задачь в главном потоке для графического движка
     */
    /* renamed from: a */
    public void mo15306a(EventManager eventManager) {
        this.eventManager = eventManager;
    }

    /* renamed from: a */
    public void mo15307a(C3126oE oEVar) {
        this.ghY = oEVar;
    }

    public C3126oE cno() {
        return this.ghY;
    }

    public C0507HC bhh() {
        return C6082agC.bWV();
    }

    /**
     * Каталог с орсисовкой графики
     */
    public FilePath getRootPathRender() {
        return this.rootPathRender;
    }

    /**
     * Главный каталог
     */
    public FilePath getRootPath() {
        return this.rootPath;
    }

    public void cnp() {
    }

    public boolean cnq() {
        return this.ghZ;
    }

    /* renamed from: fo */
    public void mo15326fo(boolean z) {
        this.ghZ = z;
    }

    /* renamed from: a */
    public void mo15304a(C1175RP rp) {
        this.dTs = rp;
    }

    public C1175RP cnr() {
        return this.dTs;
    }

    public Executor cns() {
        return this.gia;
    }

    /* renamed from: a */
    public void mo15308a(Executor executor) {
        this.gia = executor;
    }

    /* renamed from: e */
    public void mo4091e(Runnable runnable) {
        this.gia.execute(runnable);
    }

    public NetworkChannel getNetworkChannel() {
        return this.networkChannel;
    }

    /* renamed from: d */
    public void setNetworkChannel(NetworkChannel ahg) {
        this.networkChannel = ahg;
    }
}
