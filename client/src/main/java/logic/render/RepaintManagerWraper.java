package logic.render;

import javax.swing.*;
import java.applet.Applet;
import java.awt.*;
import java.util.HashSet;
import java.util.IdentityHashMap;
import java.util.Map;

/**
 * Менеджер перерисовки
 */
/* renamed from: a.amC  reason: case insensitive filesystem */
/* compiled from: a */
public class RepaintManagerWraper extends RepaintManager {
    private long fKP;
    private Map<Component, Rectangle> gbe = new IdentityHashMap<Component, Rectangle>();
    private Map<Component, Rectangle> gbf = new IdentityHashMap<Component, Rectangle>();

    public void addDirtyRegion(JComponent jComponent, int x, int y, int width, int height) {
        this.fKP++;
        m23849a((Container) jComponent, x, y, width, height);///x, y, width, height
    }

    /* renamed from: a */
    private synchronized boolean m23850a(Component r3, int x, int y, int width, int height) {
        Rectangle dest;
        synchronized (this) {
            synchronized (this.gbe) {
                dest = (Rectangle) this.gbe.get(r3);
            }
        }

        if (dest != null) {
            SwingUtilities.computeUnion(x, y, width, height, dest);
            return true;
        } else {
            return false;
        }
    }

    /* renamed from: a */
    private void m23849a(Container container, int x, int y, int width, int height) {
        if (width > 0 && height > 0 && container != null && container.isShowing() && container.getWidth() > 0 && container.getHeight() > 0 && !m23850a((Component) container, x, y, width, height)) {
            Container container2 = container;
            while (true) {
                if (container2 == null) {
                    container2 = null;
                    break;
                } else if (container2.isVisible() && container2.getPeer() != null) {
                    if (!(container2 instanceof Window) && !(container2 instanceof Applet)) {
                        container2 = container2.getParent();
                    }
                } else {
                    return;
                }
            }
            if ((container2 instanceof Frame) && (((Frame) container2).getExtendedState() & 1) == 1) {
                return;
            }
            if (container2 != null) {
                synchronized (this) {
                    if (!m23850a((Component) container, x, y, width, height)) {
                        synchronized (this.gbe) {
                            this.gbe.put(container, new Rectangle(x, y, width, height));
                        }
                    }
                }
            }
        }
    }

    public boolean isDoubleBufferingEnabled() {
        return false;
    }

    public void setDoubleBufferingEnabled(boolean z) {
    }

    public boolean isCompletelyDirty(JComponent jComponent) {
        return false;
    }

    public void addDirtyRegion(Applet applet, int i, int i2, int i3, int i4) {
        this.fKP++;
        m23849a((Container) applet, i, i2, i3, i4);
    }

    public void addDirtyRegion(Window window, int i, int i2, int i3, int i4) {
        this.fKP++;
        m23849a((Container) window, i, i2, i3, i4);
    }

    public void addInvalidComponent(JComponent jComponent) {
        RepaintManagerWraper.super.addInvalidComponent(jComponent);
        this.fKP++;
    }

    public Rectangle getDirtyRegion(JComponent jComponent) {
        Rectangle rectangle;
        synchronized (this) {
            synchronized (this.gbe) {
                rectangle = this.gbe.get(jComponent);
            }
        }
        return rectangle;
    }

    public Dimension getDoubleBufferMaximumSize() {
        return new Dimension(1, 1);
    }

    public void setDoubleBufferMaximumSize(Dimension dimension) {
    }

    public void markCompletelyClean(JComponent jComponent) {
    }

    public Image getOffscreenBuffer(Component component, int i, int i2) {
        return null;
    }

    public void markCompletelyDirty(JComponent jComponent) {
        this.fKP++;
        m23849a((Container) jComponent, 0, 0, Integer.MAX_VALUE, Integer.MAX_VALUE);
    }

    public Image getVolatileOffscreenBuffer(Component component, int i, int i2) {
        return null;
    }

    public void removeInvalidComponent(JComponent jComponent) {
        RepaintManagerWraper.super.removeInvalidComponent(jComponent);
    }

    public void paintDirtyRegions() {
    }

    public void validateInvalidComponents() {
        RepaintManagerWraper.super.validateInvalidComponents();
    }

    public long cjJ() {
        return this.fKP;
    }

    /* renamed from: p */
    public Rectangle mo14780p(Component component) {
        boolean z = true;
        Rectangle rectangle = new Rectangle();
        Rectangle rectangle2 = new Rectangle();
        synchronized (this.gbe) {
            Map<Component, Rectangle> map = this.gbe;
            this.gbe = this.gbf;
            this.gbf = map;
        }
        HashSet<QueueItem> hashSet = new HashSet<>();
        for (Map.Entry next : this.gbf.entrySet()) {
            hashSet.add(new QueueItem((Component) next.getKey(), (Rectangle) next.getValue()));
        }
        for (QueueItem km : hashSet) {
            rectangle2.setBounds((Rectangle) km.getLast());
            Component component2 = (Component) km.getFirst();
            if (component2 != null) {
                while (component2 != component) {
                    Component parent = component2.getParent();
                    if (parent == null) {
                        break;
                    }
                    SwingUtilities.computeIntersection(0, 0, component2.getWidth(), component2.getHeight(), rectangle2);
                    rectangle2.x += component2.getX();
                    rectangle2.y = component2.getY() + rectangle2.y;
                    component2 = parent;
                }
            }
            if (component2 == component) {
                this.gbf.remove(km.getFirst());
                SwingUtilities.computeIntersection(0, 0, component.getWidth(), component.getHeight(), rectangle2);
                if (z) {
                    rectangle.setBounds(rectangle2);
                    z = false;
                } else {
                    SwingUtilities.computeUnion(rectangle2.x, rectangle2.y, rectangle2.width, rectangle2.height, rectangle);
                }
            } else if (component2 == null || !component2.isShowing()) {
                this.gbf.remove(km.getFirst());
            }
        }
        synchronized (this) {
            synchronized (this.gbe) {
                for (Map.Entry next2 : this.gbf.entrySet()) {
                    Rectangle rectangle3 = (Rectangle) next2.getValue();
                    m23849a((Container) next2.getKey(), (int) rectangle3.getX(), (int) rectangle3.getY(), (int) rectangle3.getWidth(), (int) rectangle3.getHeight());
                }
            }
        }
        this.gbf.clear();
        return rectangle;
    }

    /* renamed from: a */
    public Rectangle mo14766a(JComponent jComponent, Graphics graphics) {
        Rectangle p;
        Graphics create = graphics.create();
        synchronized (jComponent.getTreeLock()) {
            p = mo14780p(jComponent);
        }
        Rectangle rectangle = new Rectangle(p.x - 8, p.y - 8, p.width + 16, p.height + 16);
        SwingUtilities.computeIntersection(0, 0, jComponent.getWidth(), jComponent.getHeight(), rectangle);
        create.setClip(rectangle);
        jComponent.paint(create);
        return p;
    }
}
