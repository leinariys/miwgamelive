package logic.render;

import com.hoplon.geometry.Vec3f;
import logic.res.ConfigManagerSection;
import p001a.C6296akI;
import taikodom.render.*;
import taikodom.render.camera.Camera;
import taikodom.render.gui.GuiScene;
import taikodom.render.helpers.RenderTask;
import taikodom.render.loader.FileSceneLoader;
import taikodom.render.loader.RenderAsset;
import taikodom.render.loader.provider.FilePath;
import taikodom.render.scene.Scene;

import javax.media.opengl.GL;
import javax.media.opengl.GLContext;
import java.awt.*;
import java.util.List;

/* renamed from: a.aBD */
/* compiled from: a */
public interface IEngineGraphics {
    /* renamed from: a */
    void mo3002a(int i, int i2, boolean z);

    /* renamed from: a */
    void mo3003a(ConfigManagerSection yr, FilePath ain);

    /* renamed from: a */
    void mo3004a(C6296akI.C1925a aVar);

    /* renamed from: a */
    void mo3005a(SceneView sceneView);

    /* renamed from: a */
    void mo3006a(Video video);

    /* renamed from: a */
    void mo3007a(RenderTask renderTask);

    void adV();

    void adW();

    float adX();

    Camera adY();

    Scene adZ();

    void aeA();

    boolean aeB();

    void aeC();

    void aeD();

    void aeE();

    SceneView aea();

    float aeb();

    int aec();

    float aed();

    JGLDesktop aee();

    GLContext aef();

    float aeg();

    GuiScene aeh();

    String aei();

    FileSceneLoader aej();

    float aek();

    int ael();

    boolean aem();

    FilePath getRootPath();

    List<SceneEvent> aeo();

    List<SceneView> aep();

    float aeq();

    StepContext aer();

    int aes();

    int aet();

    boolean aeu();

    void aev();

    void aew();

    void aex();

    void aey();

    void aez();

    /* renamed from: b */
    void mo3044b(C6296akI.C1925a aVar);

    /* renamed from: b */
    void mo3045b(String str, float f);

    /* renamed from: b */
    void mo3046b(SceneView sceneView);

    /* renamed from: bT */
    <T extends RenderAsset> T mo3047bT(String str);

    /* renamed from: bU */
    RenderAsset mo3048bU(String str);

    /* renamed from: bV */
    void mo3049bV(String str);

    /* renamed from: bW */
    void mo3050bW(String str);

    /* renamed from: bX */
    void mo3051bX(String str);

    /* renamed from: bY */
    SceneView mo3052bY(String str);

    /* renamed from: bb */
    float mo3053bb();

    /* renamed from: c */
    void mo3054c(String str, float f);

    void close();

    /* renamed from: dm */
    void mo3057dm(float f);

    /* renamed from: dn */
    void mo3058dn(float f);

    /* renamed from: do */
    void mo3059do(float f);

    /* renamed from: dp */
    void mo3060dp(float f);

    /* renamed from: dq */
    void mo3061dq(float f);

    /* renamed from: dr */
    void mo3062dr(float f);

    /* renamed from: ds */
    void mo3063ds(float f);

    /* renamed from: eH */
    Vec3f mo3064eH(int i);

    /* renamed from: eI */
    void mo3065eI(int i);

    /* renamed from: eJ */
    void mo3066eJ(int i);

    int getAntiAliasing();

    void setAntiAliasing(int i);

    GL getGL();

    float getLodQuality();

    void setLodQuality(float f);

    int getRefreshRate();

    RenderView getRenderView();

    int getShaderQuality();

    void setShaderQuality(int i);

    boolean getUseVBO();

    /* renamed from: i */
    void mo3074i(float f, float f2);

    /* renamed from: j */
    void mo3075j(List<Image> list);

    void setAnisotropicFilter(float f);

    void setPostProcessingFX(boolean z);

    void setTextureQuality(int i);

    boolean step(float f);

    void swapBuffers();
}
