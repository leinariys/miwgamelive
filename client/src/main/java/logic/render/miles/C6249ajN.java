package logic.render.miles;

import utaikodom.render.miles.MilesBridgeJNI;

/* renamed from: a.ajN  reason: case insensitive filesystem */
/* compiled from: a */
public class C6249ajN {
    public boolean swigCMemOwn;
    private long swigCPtr;

    public C6249ajN(long j, boolean z) {
        this.swigCMemOwn = z;
        this.swigCPtr = j;
    }

    public C6249ajN() {
        this(MilesBridgeJNI.new_AILTIMERSTR(), true);
    }

    /* renamed from: a */
    public static long m22876a(C6249ajN ajn) {
        if (ajn == null) {
            return 0;
        }
        return ajn.swigCPtr;
    }

    /* access modifiers changed from: protected */
    public void finalize() {
        delete();
    }

    public void delete() {
        if (this.swigCPtr != 0 && this.swigCMemOwn) {
            this.swigCMemOwn = false;
            MilesBridgeJNI.delete_AILTIMERSTR(this.swigCPtr);
        }
        this.swigCPtr = 0;
    }

    /* renamed from: hK */
    public void mo14069hK(long j) {
        MilesBridgeJNI.AILTIMERSTR_status_set(this.swigCPtr, j);
    }

    public long cfz() {
        return MilesBridgeJNI.AILTIMERSTR_status_get(this.swigCPtr);
    }

    /* renamed from: c */
    public void mo14059c(C5828abI abi) {
        MilesBridgeJNI.AILTIMERSTR_callback_set(this.swigCPtr, C5828abI.m19941a(abi));
    }

    public C5828abI cfA() {
        long AILTIMERSTR_callback_get = MilesBridgeJNI.AILTIMERSTR_callback_get(this.swigCPtr);
        if (AILTIMERSTR_callback_get == 0) {
            return null;
        }
        return new C5828abI(AILTIMERSTR_callback_get, false);
    }

    /* renamed from: hL */
    public void mo14070hL(long j) {
        MilesBridgeJNI.AILTIMERSTR_user_set(this.swigCPtr, j);
    }

    public long cfB() {
        return MilesBridgeJNI.AILTIMERSTR_user_get(this.swigCPtr);
    }

    /* renamed from: rG */
    public void mo14071rG(int i) {
        MilesBridgeJNI.AILTIMERSTR_elapsed_set(this.swigCPtr, i);
    }

    public int cfC() {
        return MilesBridgeJNI.AILTIMERSTR_elapsed_get(this.swigCPtr);
    }

    public int getValue() {
        return MilesBridgeJNI.AILTIMERSTR_value_get(this.swigCPtr);
    }

    public void setValue(int i) {
        MilesBridgeJNI.AILTIMERSTR_value_set(this.swigCPtr, i);
    }

    /* renamed from: rH */
    public void mo14072rH(int i) {
        MilesBridgeJNI.AILTIMERSTR_callingCT_set(this.swigCPtr, i);
    }

    public int cfD() {
        return MilesBridgeJNI.AILTIMERSTR_callingCT_get(this.swigCPtr);
    }

    /* renamed from: rI */
    public void mo14073rI(int i) {
        MilesBridgeJNI.AILTIMERSTR_callingDS_set(this.swigCPtr, i);
    }

    public int cfE() {
        return MilesBridgeJNI.AILTIMERSTR_callingDS_get(this.swigCPtr);
    }
}
