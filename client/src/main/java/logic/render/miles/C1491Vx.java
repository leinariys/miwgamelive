package logic.render.miles;

import utaikodom.render.miles.MilesBridgeJNI;

/* renamed from: a.Vx */
/* compiled from: a */
public class C1491Vx {
    public boolean swigCMemOwn;
    private long swigCPtr;

    public C1491Vx(long j, boolean z) {
        this.swigCMemOwn = z;
        this.swigCPtr = j;
    }

    public C1491Vx() {
        this(MilesBridgeJNI.new_AILMIXINFO(), true);
    }

    /* renamed from: a */
    public static long m10941a(C1491Vx vx) {
        if (vx == null) {
            return 0;
        }
        return vx.swigCPtr;
    }

    /* access modifiers changed from: protected */
    public void finalize() {
        delete();
    }

    public void delete() {
        if (this.swigCPtr != 0 && this.swigCMemOwn) {
            this.swigCMemOwn = false;
            MilesBridgeJNI.delete_AILMIXINFO(this.swigCPtr);
        }
        this.swigCPtr = 0;
    }

    /* renamed from: a */
    public void mo6444a(aHS ahs) {
        MilesBridgeJNI.AILMIXINFO_Info_set(this.swigCPtr, aHS.m15166b(ahs));
    }

    public aHS bBb() {
        long AILMIXINFO_Info_get = MilesBridgeJNI.AILMIXINFO_Info_get(this.swigCPtr);
        if (AILMIXINFO_Info_get == 0) {
            return null;
        }
        return new aHS(AILMIXINFO_Info_get, false);
    }

    /* renamed from: b */
    public void mo6445b(aKB akb) {
        MilesBridgeJNI.AILMIXINFO_mss_adpcm_set(this.swigCPtr, aKB.m15913c(akb));
    }

    public aKB bBc() {
        long AILMIXINFO_mss_adpcm_get = MilesBridgeJNI.AILMIXINFO_mss_adpcm_get(this.swigCPtr);
        if (AILMIXINFO_mss_adpcm_get == 0) {
            return null;
        }
        return new aKB(AILMIXINFO_mss_adpcm_get, false);
    }

    /* renamed from: fO */
    public void mo6452fO(long j) {
        MilesBridgeJNI.AILMIXINFO_src_fract_set(this.swigCPtr, j);
    }

    public long byx() {
        return MilesBridgeJNI.AILMIXINFO_src_fract_get(this.swigCPtr);
    }

    /* renamed from: oH */
    public void mo6454oH(int i) {
        MilesBridgeJNI.AILMIXINFO_left_val_set(this.swigCPtr, i);
    }

    public int bBd() {
        return MilesBridgeJNI.AILMIXINFO_left_val_get(this.swigCPtr);
    }

    /* renamed from: oI */
    public void mo6455oI(int i) {
        MilesBridgeJNI.AILMIXINFO_right_val_set(this.swigCPtr, i);
    }

    public int bBe() {
        return MilesBridgeJNI.AILMIXINFO_right_val_get(this.swigCPtr);
    }
}
