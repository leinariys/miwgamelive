package logic.render.miles;

import utaikodom.render.miles.MilesBridgeJNI;

/* renamed from: a.aLw  reason: case insensitive filesystem */
/* compiled from: a */
public class C5504aLw {
    public boolean swigCMemOwn;
    private long swigCPtr;

    public C5504aLw(long j, boolean z) {
        this.swigCMemOwn = z;
        this.swigCPtr = j;
    }

    public C5504aLw() {
        this(MilesBridgeJNI.new_MWAVEFORMATEXTENSIBLE_Samples(), true);
    }

    /* renamed from: a */
    public static long m16289a(C5504aLw alw) {
        if (alw == null) {
            return 0;
        }
        return alw.swigCPtr;
    }

    /* access modifiers changed from: protected */
    public void finalize() {
        delete();
    }

    public void delete() {
        if (this.swigCPtr != 0 && this.swigCMemOwn) {
            this.swigCMemOwn = false;
            MilesBridgeJNI.delete_MWAVEFORMATEXTENSIBLE_Samples(this.swigCPtr);
        }
        this.swigCPtr = 0;
    }

    /* renamed from: yB */
    public void mo9954yB(int i) {
        MilesBridgeJNI.MWAVEFORMATEXTENSIBLE_Samples_wValidBitsPerSample_set(this.swigCPtr, i);
    }

    public int dhW() {
        return MilesBridgeJNI.MWAVEFORMATEXTENSIBLE_Samples_wValidBitsPerSample_get(this.swigCPtr);
    }

    /* renamed from: yC */
    public void mo9955yC(int i) {
        MilesBridgeJNI.MWAVEFORMATEXTENSIBLE_Samples_wSamplesPerBlock_set(this.swigCPtr, i);
    }

    public int dhX() {
        return MilesBridgeJNI.MWAVEFORMATEXTENSIBLE_Samples_wSamplesPerBlock_get(this.swigCPtr);
    }

    /* renamed from: yD */
    public void mo9956yD(int i) {
        MilesBridgeJNI.MWAVEFORMATEXTENSIBLE_Samples_wReserved_set(this.swigCPtr, i);
    }

    public int dhY() {
        return MilesBridgeJNI.MWAVEFORMATEXTENSIBLE_Samples_wReserved_get(this.swigCPtr);
    }
}
