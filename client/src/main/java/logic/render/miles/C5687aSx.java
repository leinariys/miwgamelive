package logic.render.miles;

/* renamed from: a.aSx  reason: case insensitive filesystem */
/* compiled from: a */
public class C5687aSx {
    private long swigCPtr;

    public C5687aSx(long j, boolean z) {
        this.swigCPtr = j;
    }

    public C5687aSx() {
        this.swigCPtr = 0;
    }

    /* renamed from: b */
    public static long m18319b(C5687aSx asx) {
        if (asx == null) {
            return 0;
        }
        return asx.swigCPtr;
    }
}
