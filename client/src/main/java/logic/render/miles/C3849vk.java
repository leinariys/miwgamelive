package logic.render.miles;

import utaikodom.render.miles.MilesBridgeJNI;

/* renamed from: a.vk */
/* compiled from: a */
public class C3849vk {
    public boolean swigCMemOwn;
    private long swigCPtr;

    public C3849vk(long j, boolean z) {
        this.swigCMemOwn = z;
        this.swigCPtr = j;
    }

    public C3849vk() {
        this(MilesBridgeJNI.new_EAX_AUTOGAIN(), true);
    }

    /* renamed from: a */
    public static long m40420a(C3849vk vkVar) {
        if (vkVar == null) {
            return 0;
        }
        return vkVar.swigCPtr;
    }

    /* access modifiers changed from: protected */
    public void finalize() {
        delete();
    }

    public void delete() {
        if (this.swigCPtr != 0 && this.swigCMemOwn) {
            this.swigCMemOwn = false;
            MilesBridgeJNI.delete_EAX_AUTOGAIN(this.swigCPtr);
        }
        this.swigCPtr = 0;
    }

    /* renamed from: aD */
    public void mo22657aD(int i) {
        MilesBridgeJNI.EAX_AUTOGAIN_Effect_set(this.swigCPtr, i);
    }

    /* renamed from: ma */
    public int mo22663ma() {
        return MilesBridgeJNI.EAX_AUTOGAIN_Effect_get(this.swigCPtr);
    }

    public int getVolume() {
        return MilesBridgeJNI.EAX_AUTOGAIN_Volume_get(this.swigCPtr);
    }

    public void setVolume(int i) {
        MilesBridgeJNI.EAX_AUTOGAIN_Volume_set(this.swigCPtr, i);
    }

    /* renamed from: dm */
    public void mo22660dm(long j) {
        MilesBridgeJNI.EAX_AUTOGAIN_OnOff_set(this.swigCPtr, j);
    }

    public long akS() {
        return MilesBridgeJNI.EAX_AUTOGAIN_OnOff_get(this.swigCPtr);
    }
}
