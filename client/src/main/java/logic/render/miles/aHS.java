package logic.render.miles;

import utaikodom.render.miles.MilesBridgeJNI;

/* renamed from: a.aHS */
/* compiled from: a */
public class aHS {
    public boolean swigCMemOwn;
    private long swigCPtr;

    public aHS(long j, boolean z) {
        this.swigCMemOwn = z;
        this.swigCPtr = j;
    }

    public aHS() {
        this(MilesBridgeJNI.new_AILSOUNDINFO(), true);
    }

    /* renamed from: b */
    public static long m15166b(aHS ahs) {
        if (ahs == null) {
            return 0;
        }
        return ahs.swigCPtr;
    }

    /* access modifiers changed from: protected */
    public void finalize() {
        delete();
    }

    public void delete() {
        if (this.swigCPtr != 0 && this.swigCMemOwn) {
            this.swigCMemOwn = false;
            MilesBridgeJNI.delete_AILSOUNDINFO(this.swigCPtr);
        }
        this.swigCPtr = 0;
    }

    public int getFormat() {
        return MilesBridgeJNI.AILSOUNDINFO_format_get(this.swigCPtr);
    }

    public void setFormat(int i) {
        MilesBridgeJNI.AILSOUNDINFO_format_set(this.swigCPtr, i);
    }

    /* renamed from: k */
    public void mo9202k(aBO abo) {
        MilesBridgeJNI.AILSOUNDINFO_data_ptr_set(this.swigCPtr, aBO.m12884g(abo));
    }

    public aBO ddK() {
        long AILSOUNDINFO_data_ptr_get = MilesBridgeJNI.AILSOUNDINFO_data_ptr_get(this.swigCPtr);
        if (AILSOUNDINFO_data_ptr_get == 0) {
            return null;
        }
        return new aBO(AILSOUNDINFO_data_ptr_get, false);
    }

    /* renamed from: kS */
    public void mo9203kS(long j) {
        MilesBridgeJNI.AILSOUNDINFO_data_len_set(this.swigCPtr, j);
    }

    public long ddL() {
        return MilesBridgeJNI.AILSOUNDINFO_data_len_get(this.swigCPtr);
    }

    /* renamed from: kT */
    public void mo9204kT(long j) {
        MilesBridgeJNI.AILSOUNDINFO_rate_set(this.swigCPtr, j);
    }

    public long ddM() {
        return MilesBridgeJNI.AILSOUNDINFO_rate_get(this.swigCPtr);
    }

    /* renamed from: bT */
    public void mo9189bT(int i) {
        MilesBridgeJNI.AILSOUNDINFO_bits_set(this.swigCPtr, i);
    }

    /* renamed from: DH */
    public int mo9188DH() {
        return MilesBridgeJNI.AILSOUNDINFO_bits_get(this.swigCPtr);
    }

    public int getChannels() {
        return MilesBridgeJNI.AILSOUNDINFO_channels_get(this.swigCPtr);
    }

    public void setChannels(int i) {
        MilesBridgeJNI.AILSOUNDINFO_channels_set(this.swigCPtr, i);
    }

    /* renamed from: fP */
    public void mo9198fP(long j) {
        MilesBridgeJNI.AILSOUNDINFO_channel_mask_set(this.swigCPtr, j);
    }

    public long byM() {
        return MilesBridgeJNI.AILSOUNDINFO_channel_mask_get(this.swigCPtr);
    }

    /* renamed from: kU */
    public void mo9205kU(long j) {
        MilesBridgeJNI.AILSOUNDINFO_samples_set(this.swigCPtr, j);
    }

    public long ddN() {
        return MilesBridgeJNI.AILSOUNDINFO_samples_get(this.swigCPtr);
    }

    /* renamed from: kV */
    public void mo9206kV(long j) {
        MilesBridgeJNI.AILSOUNDINFO_block_size_set(this.swigCPtr, j);
    }

    public long ddO() {
        return MilesBridgeJNI.AILSOUNDINFO_block_size_get(this.swigCPtr);
    }

    /* renamed from: l */
    public void mo9207l(aBO abo) {
        MilesBridgeJNI.AILSOUNDINFO_initial_ptr_set(this.swigCPtr, aBO.m12884g(abo));
    }

    public aBO ddP() {
        long AILSOUNDINFO_initial_ptr_get = MilesBridgeJNI.AILSOUNDINFO_initial_ptr_get(this.swigCPtr);
        if (AILSOUNDINFO_initial_ptr_get == 0) {
            return null;
        }
        return new aBO(AILSOUNDINFO_initial_ptr_get, false);
    }
}
