package logic.render.miles;

/* renamed from: a.aNq  reason: case insensitive filesystem */
/* compiled from: a */
public class C5550aNq {
    private long swigCPtr;

    public C5550aNq(long j, boolean z) {
        this.swigCPtr = j;
    }

    public C5550aNq() {
        this.swigCPtr = 0;
    }

    /* renamed from: c */
    public static long m16778c(C5550aNq anq) {
        if (anq == null) {
            return 0;
        }
        return anq.swigCPtr;
    }
}
