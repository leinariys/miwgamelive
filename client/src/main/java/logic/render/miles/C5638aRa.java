package logic.render.miles;

/* renamed from: a.aRa  reason: case insensitive filesystem */
/* compiled from: a */
public class C5638aRa {
    private long swigCPtr;

    public C5638aRa(long j, boolean z) {
        this.swigCPtr = j;
    }

    public C5638aRa() {
        this.swigCPtr = 0;
    }

    /* renamed from: b */
    public static long m17828b(C5638aRa ara) {
        if (ara == null) {
            return 0;
        }
        return ara.swigCPtr;
    }
}
