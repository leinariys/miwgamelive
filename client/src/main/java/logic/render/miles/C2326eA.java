package logic.render.miles;

import utaikodom.render.miles.MilesBridgeJNI;

/* renamed from: a.eA */
/* compiled from: a */
public class C2326eA {
    public boolean swigCMemOwn;
    private long swigCPtr;

    public C2326eA(long j, boolean z) {
        this.swigCMemOwn = z;
        this.swigCPtr = j;
    }

    public C2326eA() {
        this(MilesBridgeJNI.new_EAX_FSHIFTER(), true);
    }

    /* renamed from: a */
    public static long m29393a(C2326eA eAVar) {
        if (eAVar == null) {
            return 0;
        }
        return eAVar.swigCPtr;
    }

    /* access modifiers changed from: protected */
    public void finalize() {
        delete();
    }

    public void delete() {
        if (this.swigCPtr != 0 && this.swigCMemOwn) {
            this.swigCMemOwn = false;
            MilesBridgeJNI.delete_EAX_FSHIFTER(this.swigCPtr);
        }
        this.swigCPtr = 0;
    }

    /* renamed from: aD */
    public void mo17946aD(int i) {
        MilesBridgeJNI.EAX_FSHIFTER_Effect_set(this.swigCPtr, i);
    }

    /* renamed from: ma */
    public int mo17950ma() {
        return MilesBridgeJNI.EAX_FSHIFTER_Effect_get(this.swigCPtr);
    }

    public int getVolume() {
        return MilesBridgeJNI.EAX_FSHIFTER_Volume_get(this.swigCPtr);
    }

    public void setVolume(int i) {
        MilesBridgeJNI.EAX_FSHIFTER_Volume_set(this.swigCPtr, i);
    }

    /* renamed from: P */
    public void mo17945P(float f) {
        MilesBridgeJNI.EAX_FSHIFTER_Frequency_set(this.swigCPtr, f);
    }

    /* renamed from: mb */
    public float mo17951mb() {
        return MilesBridgeJNI.EAX_FSHIFTER_Frequency_get(this.swigCPtr);
    }

    /* renamed from: u */
    public void mo17955u(long j) {
        MilesBridgeJNI.EAX_FSHIFTER_LeftDirection_set(this.swigCPtr, j);
    }

    /* renamed from: mc */
    public long mo17952mc() {
        return MilesBridgeJNI.EAX_FSHIFTER_LeftDirection_get(this.swigCPtr);
    }

    /* renamed from: v */
    public void mo17956v(long j) {
        MilesBridgeJNI.EAX_FSHIFTER_RightDirection_set(this.swigCPtr, j);
    }

    /* renamed from: md */
    public long mo17953md() {
        return MilesBridgeJNI.EAX_FSHIFTER_RightDirection_get(this.swigCPtr);
    }
}
