package logic.render.miles;

import utaikodom.render.miles.MilesBridgeJNI;

/* renamed from: a.aUy  reason: case insensitive filesystem */
/* compiled from: a */
public class C5740aUy {
    public boolean swigCMemOwn;
    private long swigCPtr;

    public C5740aUy(long j, boolean z) {
        this.swigCMemOwn = z;
        this.swigCPtr = j;
    }

    public C5740aUy() {
        this(MilesBridgeJNI.new_AIL_INPUT_INFO(), true);
    }

    /* renamed from: c */
    public static long m18768c(C5740aUy auy) {
        if (auy == null) {
            return 0;
        }
        return auy.swigCPtr;
    }

    /* access modifiers changed from: protected */
    public void finalize() {
        delete();
    }

    public void delete() {
        if (this.swigCPtr != 0 && this.swigCMemOwn) {
            this.swigCMemOwn = false;
            MilesBridgeJNI.delete_AIL_INPUT_INFO(this.swigCPtr);
        }
        this.swigCPtr = 0;
    }

    /* renamed from: mf */
    public void mo11755mf(long j) {
        MilesBridgeJNI.AIL_INPUT_INFO_device_ID_set(this.swigCPtr, j);
    }

    public long dAj() {
        return MilesBridgeJNI.AIL_INPUT_INFO_device_ID_get(this.swigCPtr);
    }

    /* renamed from: mg */
    public void mo11756mg(long j) {
        MilesBridgeJNI.AIL_INPUT_INFO_hardware_format_set(this.swigCPtr, j);
    }

    public long dAk() {
        return MilesBridgeJNI.AIL_INPUT_INFO_hardware_format_get(this.swigCPtr);
    }

    /* renamed from: mh */
    public void mo11757mh(long j) {
        MilesBridgeJNI.AIL_INPUT_INFO_hardware_rate_set(this.swigCPtr, j);
    }

    public long dAl() {
        return MilesBridgeJNI.AIL_INPUT_INFO_hardware_rate_get(this.swigCPtr);
    }

    /* renamed from: b */
    public void mo11746b(aSD asd) {
        MilesBridgeJNI.AIL_INPUT_INFO_callback_set(this.swigCPtr, aSD.m18160a(asd));
    }

    public aSD dAm() {
        long AIL_INPUT_INFO_callback_get = MilesBridgeJNI.AIL_INPUT_INFO_callback_get(this.swigCPtr);
        if (AIL_INPUT_INFO_callback_get == 0) {
            return null;
        }
        return new aSD(AIL_INPUT_INFO_callback_get, false);
    }

    /* renamed from: AX */
    public void mo11745AX(int i) {
        MilesBridgeJNI.AIL_INPUT_INFO_buffer_size_set(this.swigCPtr, i);
    }

    public int dAn() {
        return MilesBridgeJNI.AIL_INPUT_INFO_buffer_size_get(this.swigCPtr);
    }

    /* renamed from: mi */
    public void mo11758mi(long j) {
        MilesBridgeJNI.AIL_INPUT_INFO_user_data_set(this.swigCPtr, j);
    }

    public long dAo() {
        return MilesBridgeJNI.AIL_INPUT_INFO_user_data_get(this.swigCPtr);
    }
}
