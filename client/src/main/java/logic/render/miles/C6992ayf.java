package logic.render.miles;

import utaikodom.render.miles.MilesBridgeJNI;

/* renamed from: a.ayf  reason: case insensitive filesystem */
/* compiled from: a */
public class C6992ayf {
    public boolean swigCMemOwn;
    private long swigCPtr;

    public C6992ayf(long j, boolean z) {
        this.swigCMemOwn = z;
        this.swigCPtr = j;
    }

    public C6992ayf() {
        this(MilesBridgeJNI.new_EAX_FLANGER(), true);
    }

    /* renamed from: a */
    public static long m27469a(C6992ayf ayf) {
        if (ayf == null) {
            return 0;
        }
        return ayf.swigCPtr;
    }

    /* access modifiers changed from: protected */
    public void finalize() {
        delete();
    }

    public void delete() {
        if (this.swigCPtr != 0 && this.swigCMemOwn) {
            this.swigCMemOwn = false;
            MilesBridgeJNI.delete_EAX_FLANGER(this.swigCPtr);
        }
        this.swigCPtr = 0;
    }

    /* renamed from: aD */
    public void mo17055aD(int i) {
        MilesBridgeJNI.EAX_FLANGER_Effect_set(this.swigCPtr, i);
    }

    /* renamed from: ma */
    public int mo17070ma() {
        return MilesBridgeJNI.EAX_FLANGER_Effect_get(this.swigCPtr);
    }

    public int getVolume() {
        return MilesBridgeJNI.EAX_FLANGER_Volume_get(this.swigCPtr);
    }

    public void setVolume(int i) {
        MilesBridgeJNI.EAX_FLANGER_Volume_set(this.swigCPtr, i);
    }

    /* renamed from: iq */
    public void mo17067iq(long j) {
        MilesBridgeJNI.EAX_FLANGER_Waveform_set(this.swigCPtr, j);
    }

    public long crB() {
        return MilesBridgeJNI.EAX_FLANGER_Waveform_get(this.swigCPtr);
    }

    public void setPhase(int i) {
        MilesBridgeJNI.EAX_FLANGER_Phase_set(this.swigCPtr, i);
    }

    public int cDo() {
        return MilesBridgeJNI.EAX_FLANGER_Phase_get(this.swigCPtr);
    }

    /* renamed from: kw */
    public void mo17069kw(float f) {
        MilesBridgeJNI.EAX_FLANGER_Rate_set(this.swigCPtr, f);
    }

    public float crC() {
        return MilesBridgeJNI.EAX_FLANGER_Rate_get(this.swigCPtr);
    }

    /* renamed from: kY */
    public void mo17068kY(float f) {
        MilesBridgeJNI.EAX_FLANGER_Depth_set(this.swigCPtr, f);
    }

    public float aNC() {
        return MilesBridgeJNI.EAX_FLANGER_Depth_get(this.swigCPtr);
    }

    /* renamed from: fa */
    public void mo17064fa(float f) {
        MilesBridgeJNI.EAX_FLANGER_Feedback_set(this.swigCPtr, f);
    }

    public float aHh() {
        return MilesBridgeJNI.EAX_FLANGER_Feedback_get(this.swigCPtr);
    }

    /* renamed from: eX */
    public void mo17063eX(float f) {
        MilesBridgeJNI.EAX_FLANGER_Delay_set(this.swigCPtr, f);
    }

    public float aHe() {
        return MilesBridgeJNI.EAX_FLANGER_Delay_get(this.swigCPtr);
    }
}
