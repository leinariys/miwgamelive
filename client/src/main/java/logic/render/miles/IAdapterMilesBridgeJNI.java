package logic.render.miles;

import utaikodom.render.miles.MilesBridgeJNI;

/* renamed from: a.asQ  reason: case insensitive filesystem */
/* compiled from: a */
public interface IAdapterMilesBridgeJNI {
    public static final int gwA = MilesBridgeJNI.ENVIRONMENT_SEWERPIPE_get();
    public static final int gwB = MilesBridgeJNI.ENVIRONMENT_UNDERWATER_get();
    public static final int gwC = MilesBridgeJNI.ENVIRONMENT_DRUGGED_get();
    public static final int gwD = MilesBridgeJNI.ENVIRONMENT_DIZZY_get();
    public static final int gwE = MilesBridgeJNI.ENVIRONMENT_PSYCHOTIC_get();
    public static final int gwF = MilesBridgeJNI.ENVIRONMENT_COUNT_get();
    public static final int gwG = MilesBridgeJNI.EAX_ENVIRONMENT_GENERIC_get();
    public static final int gwH = MilesBridgeJNI.EAX_ENVIRONMENT_PADDEDCELL_get();
    public static final int gwI = MilesBridgeJNI.EAX_ENVIRONMENT_ROOM_get();
    public static final int gwJ = MilesBridgeJNI.EAX_ENVIRONMENT_BATHROOM_get();
    public static final int gwK = MilesBridgeJNI.EAX_ENVIRONMENT_LIVINGROOM_get();
    public static final int gwL = MilesBridgeJNI.EAX_ENVIRONMENT_STONEROOM_get();
    public static final int gwM = MilesBridgeJNI.EAX_ENVIRONMENT_AUDITORIUM_get();
    public static final int gwN = MilesBridgeJNI.EAX_ENVIRONMENT_CONCERTHALL_get();
    public static final int gwO = MilesBridgeJNI.EAX_ENVIRONMENT_CAVE_get();
    public static final int gwP = MilesBridgeJNI.EAX_ENVIRONMENT_ARENA_get();
    public static final int gwQ = MilesBridgeJNI.EAX_ENVIRONMENT_HANGAR_get();
    public static final int gwR = MilesBridgeJNI.EAX_ENVIRONMENT_CARPETEDHALLWAY_get();
    public static final int gwS = MilesBridgeJNI.EAX_ENVIRONMENT_HALLWAY_get();
    public static final int gwT = MilesBridgeJNI.EAX_ENVIRONMENT_STONECORRIDOR_get();
    public static final int gwU = MilesBridgeJNI.EAX_ENVIRONMENT_ALLEY_get();
    public static final int gwV = MilesBridgeJNI.EAX_ENVIRONMENT_FOREST_get();
    public static final int gwW = MilesBridgeJNI.EAX_ENVIRONMENT_CITY_get();
    public static final int gwX = MilesBridgeJNI.EAX_ENVIRONMENT_MOUNTAINS_get();
    public static final int gwY = MilesBridgeJNI.EAX_ENVIRONMENT_QUARRY_get();
    public static final int gwZ = MilesBridgeJNI.EAX_ENVIRONMENT_PLAIN_get();
    public static final int gwf = MilesBridgeJNI.ENVIRONMENT_GENERIC_get();
    public static final int gwg = MilesBridgeJNI.ENVIRONMENT_PADDEDCELL_get();
    public static final int gwh = MilesBridgeJNI.ENVIRONMENT_ROOM_get();
    public static final int gwi = MilesBridgeJNI.ENVIRONMENT_BATHROOM_get();
    public static final int gwj = MilesBridgeJNI.ENVIRONMENT_LIVINGROOM_get();
    public static final int gwk = MilesBridgeJNI.ENVIRONMENT_STONEROOM_get();
    public static final int gwl = MilesBridgeJNI.ENVIRONMENT_AUDITORIUM_get();
    public static final int gwm = MilesBridgeJNI.ENVIRONMENT_CONCERTHALL_get();
    public static final int gwn = MilesBridgeJNI.ENVIRONMENT_CAVE_get();
    public static final int gwo = MilesBridgeJNI.ENVIRONMENT_ARENA_get();
    public static final int gwp = MilesBridgeJNI.ENVIRONMENT_HANGAR_get();
    public static final int gwq = MilesBridgeJNI.ENVIRONMENT_CARPETEDHALLWAY_get();
    public static final int gwr = MilesBridgeJNI.ENVIRONMENT_HALLWAY_get();
    public static final int gws = MilesBridgeJNI.ENVIRONMENT_STONECORRIDOR_get();
    public static final int gwt = MilesBridgeJNI.ENVIRONMENT_ALLEY_get();
    public static final int gwu = MilesBridgeJNI.ENVIRONMENT_FOREST_get();
    public static final int gwv = MilesBridgeJNI.ENVIRONMENT_CITY_get();
    public static final int gww = MilesBridgeJNI.ENVIRONMENT_MOUNTAINS_get();
    public static final int gwx = MilesBridgeJNI.ENVIRONMENT_QUARRY_get();
    public static final int gwy = MilesBridgeJNI.ENVIRONMENT_PLAIN_get();
    public static final int gwz = MilesBridgeJNI.ENVIRONMENT_PARKINGLOT_get();
    public static final int gxa = MilesBridgeJNI.EAX_ENVIRONMENT_PARKINGLOT_get();
    public static final int gxb = MilesBridgeJNI.EAX_ENVIRONMENT_SEWERPIPE_get();
    public static final int gxc = MilesBridgeJNI.EAX_ENVIRONMENT_UNDERWATER_get();
    public static final int gxd = MilesBridgeJNI.EAX_ENVIRONMENT_DRUGGED_get();
    public static final int gxe = MilesBridgeJNI.EAX_ENVIRONMENT_DIZZY_get();
    public static final int gxf = MilesBridgeJNI.EAX_ENVIRONMENT_PSYCHOTIC_get();
    public static final int gxg = MilesBridgeJNI.EAX_ENVIRONMENT_COUNT_get();
}
