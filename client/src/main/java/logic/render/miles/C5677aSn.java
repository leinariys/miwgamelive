package logic.render.miles;

import utaikodom.render.miles.MilesBridgeJNI;

/* renamed from: a.aSn  reason: case insensitive filesystem */
/* compiled from: a */
public class C5677aSn {
    public boolean swigCMemOwn;
    private long swigCPtr;

    public C5677aSn(long j, boolean z) {
        this.swigCMemOwn = z;
        this.swigCPtr = j;
    }

    public C5677aSn() {
        this(MilesBridgeJNI.new_EAX_CHORUS(), true);
    }

    /* renamed from: a */
    public static long m18236a(C5677aSn asn) {
        if (asn == null) {
            return 0;
        }
        return asn.swigCPtr;
    }

    /* access modifiers changed from: protected */
    public void finalize() {
        delete();
    }

    public void delete() {
        if (this.swigCPtr != 0 && this.swigCMemOwn) {
            this.swigCMemOwn = false;
            MilesBridgeJNI.delete_EAX_CHORUS(this.swigCPtr);
        }
        this.swigCPtr = 0;
    }

    /* renamed from: aD */
    public void mo11420aD(int i) {
        MilesBridgeJNI.EAX_CHORUS_Effect_set(this.swigCPtr, i);
    }

    /* renamed from: ma */
    public int mo11435ma() {
        return MilesBridgeJNI.EAX_CHORUS_Effect_get(this.swigCPtr);
    }

    public int getVolume() {
        return MilesBridgeJNI.EAX_CHORUS_Volume_get(this.swigCPtr);
    }

    public void setVolume(int i) {
        MilesBridgeJNI.EAX_CHORUS_Volume_set(this.swigCPtr, i);
    }

    /* renamed from: iq */
    public void mo11432iq(long j) {
        MilesBridgeJNI.EAX_CHORUS_Waveform_set(this.swigCPtr, j);
    }

    public long crB() {
        return MilesBridgeJNI.EAX_CHORUS_Waveform_get(this.swigCPtr);
    }

    public void setPhase(int i) {
        MilesBridgeJNI.EAX_CHORUS_Phase_set(this.swigCPtr, i);
    }

    public int cDo() {
        return MilesBridgeJNI.EAX_CHORUS_Phase_get(this.swigCPtr);
    }

    /* renamed from: kw */
    public void mo11434kw(float f) {
        MilesBridgeJNI.EAX_CHORUS_Rate_set(this.swigCPtr, f);
    }

    public float crC() {
        return MilesBridgeJNI.EAX_CHORUS_Rate_get(this.swigCPtr);
    }

    /* renamed from: kY */
    public void mo11433kY(float f) {
        MilesBridgeJNI.EAX_CHORUS_Depth_set(this.swigCPtr, f);
    }

    public float aNC() {
        return MilesBridgeJNI.EAX_CHORUS_Depth_get(this.swigCPtr);
    }

    /* renamed from: fa */
    public void mo11429fa(float f) {
        MilesBridgeJNI.EAX_CHORUS_Feedback_set(this.swigCPtr, f);
    }

    public float aHh() {
        return MilesBridgeJNI.EAX_CHORUS_Feedback_get(this.swigCPtr);
    }

    /* renamed from: eX */
    public void mo11428eX(float f) {
        MilesBridgeJNI.EAX_CHORUS_Delay_set(this.swigCPtr, f);
    }

    public float aHe() {
        return MilesBridgeJNI.EAX_CHORUS_Delay_get(this.swigCPtr);
    }
}
