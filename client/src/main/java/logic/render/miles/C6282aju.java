package logic.render.miles;

import utaikodom.render.miles.MilesBridgeJNI;

/* renamed from: a.aju  reason: case insensitive filesystem */
/* compiled from: a */
public class C6282aju {
    public boolean swigCMemOwn;
    private long swigCPtr;

    public C6282aju(long j, boolean z) {
        this.swigCMemOwn = z;
        this.swigCPtr = j;
    }

    public C6282aju() {
        this(MilesBridgeJNI.new_SOUNDLIB(), true);
    }

    /* renamed from: b */
    public static long m23101b(C6282aju aju) {
        if (aju == null) {
            return 0;
        }
        return aju.swigCPtr;
    }

    /* access modifiers changed from: protected */
    public void finalize() {
        delete();
    }

    public void delete() {
        if (this.swigCPtr != 0 && this.swigCMemOwn) {
            this.swigCMemOwn = false;
            MilesBridgeJNI.delete_SOUNDLIB(this.swigCPtr);
        }
        this.swigCPtr = 0;
    }

    /* renamed from: c */
    public void mo14218c(C6282aju aju) {
        MilesBridgeJNI.SOUNDLIB_next_set(this.swigCPtr, m23101b(aju));
    }

    public C6282aju cel() {
        long SOUNDLIB_next_get = MilesBridgeJNI.SOUNDLIB_next_get(this.swigCPtr);
        if (SOUNDLIB_next_get == 0) {
            return null;
        }
        return new C6282aju(SOUNDLIB_next_get, false);
    }

    /* renamed from: b */
    public void mo14217b(C3353qb qbVar) {
        MilesBridgeJNI.SOUNDLIB_B_set(this.swigCPtr, C3353qb.m37727a(qbVar));
    }

    public C3353qb cem() {
        long SOUNDLIB_B_get = MilesBridgeJNI.SOUNDLIB_B_get(this.swigCPtr);
        if (SOUNDLIB_B_get == 0) {
            return null;
        }
        return new C3353qb(SOUNDLIB_B_get, false);
    }
}
