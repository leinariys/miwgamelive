package logic.render.miles;

import utaikodom.render.miles.MilesBridgeJNI;

/* renamed from: a.VP */
/* compiled from: a */
public class C1455VP {
    public boolean swigCMemOwn;
    private long swigCPtr;

    public C1455VP(long j, boolean z) {
        this.swigCMemOwn = z;
        this.swigCPtr = j;
    }

    public C1455VP() {
        this(MilesBridgeJNI.new_S3DSTATE(), true);
    }

    /* renamed from: b */
    public static long m10622b(C1455VP vp) {
        if (vp == null) {
            return 0;
        }
        return vp.swigCPtr;
    }

    /* access modifiers changed from: protected */
    public void finalize() {
        delete();
    }

    public void delete() {
        if (this.swigCPtr != 0 && this.swigCMemOwn) {
            this.swigCMemOwn = false;
            MilesBridgeJNI.delete_S3DSTATE(this.swigCPtr);
        }
        this.swigCPtr = 0;
    }

    /* renamed from: a */
    public void mo6094a(aMJ amj) {
        MilesBridgeJNI.S3DSTATE_position_set(this.swigCPtr, aMJ.m16344m(amj));
    }

    public aMJ bCW() {
        long S3DSTATE_position_get = MilesBridgeJNI.S3DSTATE_position_get(this.swigCPtr);
        if (S3DSTATE_position_get == 0) {
            return null;
        }
        return new aMJ(S3DSTATE_position_get, false);
    }

    /* renamed from: b */
    public void mo6096b(aMJ amj) {
        MilesBridgeJNI.S3DSTATE_face_set(this.swigCPtr, aMJ.m16344m(amj));
    }

    public aMJ bCX() {
        long S3DSTATE_face_get = MilesBridgeJNI.S3DSTATE_face_get(this.swigCPtr);
        if (S3DSTATE_face_get == 0) {
            return null;
        }
        return new aMJ(S3DSTATE_face_get, false);
    }

    /* renamed from: c */
    public void mo6114c(aMJ amj) {
        MilesBridgeJNI.S3DSTATE_up_set(this.swigCPtr, aMJ.m16344m(amj));
    }

    public aMJ bCY() {
        long S3DSTATE_up_get = MilesBridgeJNI.S3DSTATE_up_get(this.swigCPtr);
        if (S3DSTATE_up_get == 0) {
            return null;
        }
        return new aMJ(S3DSTATE_up_get, false);
    }

    /* renamed from: d */
    public void mo6116d(aMJ amj) {
        MilesBridgeJNI.S3DSTATE_velocity_set(this.swigCPtr, aMJ.m16344m(amj));
    }

    public aMJ bCZ() {
        long S3DSTATE_velocity_get = MilesBridgeJNI.S3DSTATE_velocity_get(this.swigCPtr);
        if (S3DSTATE_velocity_get == 0) {
            return null;
        }
        return new aMJ(S3DSTATE_velocity_get, false);
    }

    /* renamed from: oR */
    public void mo6126oR(int i) {
        MilesBridgeJNI.S3DSTATE_doppler_valid_set(this.swigCPtr, i);
    }

    public int bDa() {
        return MilesBridgeJNI.S3DSTATE_doppler_valid_get(this.swigCPtr);
    }

    /* renamed from: iC */
    public void mo6119iC(float f) {
        MilesBridgeJNI.S3DSTATE_doppler_shift_set(this.swigCPtr, f);
    }

    public float bDb() {
        return MilesBridgeJNI.S3DSTATE_doppler_shift_get(this.swigCPtr);
    }

    /* renamed from: iD */
    public void mo6120iD(float f) {
        MilesBridgeJNI.S3DSTATE_inner_angle_set(this.swigCPtr, f);
    }

    public float bDc() {
        return MilesBridgeJNI.S3DSTATE_inner_angle_get(this.swigCPtr);
    }

    /* renamed from: iE */
    public void mo6121iE(float f) {
        MilesBridgeJNI.S3DSTATE_outer_angle_set(this.swigCPtr, f);
    }

    public float bDd() {
        return MilesBridgeJNI.S3DSTATE_outer_angle_get(this.swigCPtr);
    }

    /* renamed from: iF */
    public void mo6122iF(float f) {
        MilesBridgeJNI.S3DSTATE_outer_volume_set(this.swigCPtr, f);
    }

    public float bDe() {
        return MilesBridgeJNI.S3DSTATE_outer_volume_get(this.swigCPtr);
    }

    /* renamed from: oS */
    public void mo6127oS(int i) {
        MilesBridgeJNI.S3DSTATE_cone_enabled_set(this.swigCPtr, i);
    }

    public int bDf() {
        return MilesBridgeJNI.S3DSTATE_cone_enabled_get(this.swigCPtr);
    }

    /* renamed from: iG */
    public void mo6123iG(float f) {
        MilesBridgeJNI.S3DSTATE_max_dist_set(this.swigCPtr, f);
    }

    public float bDg() {
        return MilesBridgeJNI.S3DSTATE_max_dist_get(this.swigCPtr);
    }

    /* renamed from: iH */
    public void mo6124iH(float f) {
        MilesBridgeJNI.S3DSTATE_min_dist_set(this.swigCPtr, f);
    }

    public float bDh() {
        return MilesBridgeJNI.S3DSTATE_min_dist_get(this.swigCPtr);
    }

    /* renamed from: oT */
    public void mo6128oT(int i) {
        MilesBridgeJNI.S3DSTATE_dist_changed_set(this.swigCPtr, i);
    }

    public int bDi() {
        return MilesBridgeJNI.S3DSTATE_dist_changed_get(this.swigCPtr);
    }

    /* renamed from: oU */
    public void mo6129oU(int i) {
        MilesBridgeJNI.S3DSTATE_auto_3D_atten_set(this.swigCPtr, i);
    }

    public int bDj() {
        return MilesBridgeJNI.S3DSTATE_auto_3D_atten_get(this.swigCPtr);
    }

    /* renamed from: iI */
    public void mo6125iI(float f) {
        MilesBridgeJNI.S3DSTATE_atten_3D_set(this.swigCPtr, f);
    }

    public float bDk() {
        return MilesBridgeJNI.S3DSTATE_atten_3D_get(this.swigCPtr);
    }

    /* renamed from: d */
    public void mo6115d(C1467Vb vb) {
        MilesBridgeJNI.S3DSTATE_owner_set(this.swigCPtr, C1467Vb.m10693c(vb));
    }

    public C1467Vb bDl() {
        long S3DSTATE_owner_get = MilesBridgeJNI.S3DSTATE_owner_get(this.swigCPtr);
        if (S3DSTATE_owner_get == 0) {
            return null;
        }
        return new C1467Vb(S3DSTATE_owner_get, false);
    }

    /* renamed from: a */
    public void mo6095a(C6266aje aje) {
        MilesBridgeJNI.S3DSTATE_falloff_function_set(this.swigCPtr, C6266aje.m23016b(aje));
    }

    public C6266aje bDm() {
        long S3DSTATE_falloff_function_get = MilesBridgeJNI.S3DSTATE_falloff_function_get(this.swigCPtr);
        if (S3DSTATE_falloff_function_get == 0) {
            return null;
        }
        return new C6266aje(S3DSTATE_falloff_function_get, false);
    }
}
