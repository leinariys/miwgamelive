package logic.render.miles;

import utaikodom.render.miles.MilesBridgeJNI;

/* renamed from: a.aQU */
/* compiled from: a */
public class aQU {
    public boolean swigCMemOwn;
    private long swigCPtr;

    public aQU(long j, boolean z) {
        this.swigCMemOwn = z;
        this.swigCPtr = j;
    }

    public aQU() {
        this(MilesBridgeJNI.new_EAX_REVERB(), true);
    }

    /* renamed from: a */
    public static long m17486a(aQU aqu) {
        if (aqu == null) {
            return 0;
        }
        return aqu.swigCPtr;
    }

    /* access modifiers changed from: protected */
    public void finalize() {
        delete();
    }

    public void delete() {
        if (this.swigCPtr != 0 && this.swigCMemOwn) {
            this.swigCMemOwn = false;
            MilesBridgeJNI.delete_EAX_REVERB(this.swigCPtr);
        }
        this.swigCPtr = 0;
    }

    /* renamed from: aD */
    public void mo10953aD(int i) {
        MilesBridgeJNI.EAX_REVERB_Effect_set(this.swigCPtr, i);
    }

    /* renamed from: ma */
    public int mo10986ma() {
        return MilesBridgeJNI.EAX_REVERB_Effect_get(this.swigCPtr);
    }

    public int getVolume() {
        return MilesBridgeJNI.EAX_REVERB_Volume_get(this.swigCPtr);
    }

    public void setVolume(int i) {
        MilesBridgeJNI.EAX_REVERB_Volume_set(this.swigCPtr, i);
    }

    /* renamed from: lP */
    public void mo10985lP(long j) {
        MilesBridgeJNI.EAX_REVERB_Environment_set(this.swigCPtr, j);
    }

    public long dqo() {
        return MilesBridgeJNI.EAX_REVERB_Environment_get(this.swigCPtr);
    }

    /* renamed from: nV */
    public void mo10987nV(float f) {
        MilesBridgeJNI.EAX_REVERB_EnvironmentSize_set(this.swigCPtr, f);
    }

    public float dqp() {
        return MilesBridgeJNI.EAX_REVERB_EnvironmentSize_get(this.swigCPtr);
    }

    /* renamed from: nW */
    public void mo10988nW(float f) {
        MilesBridgeJNI.EAX_REVERB_EnvironmentDiffusion_set(this.swigCPtr, f);
    }

    public float dqq() {
        return MilesBridgeJNI.EAX_REVERB_EnvironmentDiffusion_get(this.swigCPtr);
    }

    /* renamed from: Am */
    public void mo10948Am(int i) {
        MilesBridgeJNI.EAX_REVERB_Room_set(this.swigCPtr, i);
    }

    public int dqr() {
        return MilesBridgeJNI.EAX_REVERB_Room_get(this.swigCPtr);
    }

    /* renamed from: An */
    public void mo10949An(int i) {
        MilesBridgeJNI.EAX_REVERB_RoomHF_set(this.swigCPtr, i);
    }

    public int dqs() {
        return MilesBridgeJNI.EAX_REVERB_RoomHF_get(this.swigCPtr);
    }

    /* renamed from: Ao */
    public void mo10950Ao(int i) {
        MilesBridgeJNI.EAX_REVERB_RoomLF_set(this.swigCPtr, i);
    }

    public int dqt() {
        return MilesBridgeJNI.EAX_REVERB_RoomLF_get(this.swigCPtr);
    }

    /* renamed from: nX */
    public void mo10989nX(float f) {
        MilesBridgeJNI.EAX_REVERB_DecayTime_set(this.swigCPtr, f);
    }

    public float dqu() {
        return MilesBridgeJNI.EAX_REVERB_DecayTime_get(this.swigCPtr);
    }

    /* renamed from: nY */
    public void mo10990nY(float f) {
        MilesBridgeJNI.EAX_REVERB_DecayHFRatio_set(this.swigCPtr, f);
    }

    public float dqv() {
        return MilesBridgeJNI.EAX_REVERB_DecayHFRatio_get(this.swigCPtr);
    }

    /* renamed from: nZ */
    public void mo10991nZ(float f) {
        MilesBridgeJNI.EAX_REVERB_DecayLFRatio_set(this.swigCPtr, f);
    }

    public float dqw() {
        return MilesBridgeJNI.EAX_REVERB_DecayLFRatio_get(this.swigCPtr);
    }

    /* renamed from: Ap */
    public void mo10951Ap(int i) {
        MilesBridgeJNI.EAX_REVERB_Reflections_set(this.swigCPtr, i);
    }

    public int dqx() {
        return MilesBridgeJNI.EAX_REVERB_Reflections_get(this.swigCPtr);
    }

    /* renamed from: oa */
    public void mo10992oa(float f) {
        MilesBridgeJNI.EAX_REVERB_ReflectionsDelay_set(this.swigCPtr, f);
    }

    public float dqy() {
        return MilesBridgeJNI.EAX_REVERB_ReflectionsDelay_get(this.swigCPtr);
    }

    /* renamed from: ob */
    public void mo10993ob(float f) {
        MilesBridgeJNI.EAX_REVERB_ReflectionsPanX_set(this.swigCPtr, f);
    }

    public float dqz() {
        return MilesBridgeJNI.EAX_REVERB_ReflectionsPanX_get(this.swigCPtr);
    }

    /* renamed from: oc */
    public void mo10994oc(float f) {
        MilesBridgeJNI.EAX_REVERB_ReflectionsPanY_set(this.swigCPtr, f);
    }

    public float dqA() {
        return MilesBridgeJNI.EAX_REVERB_ReflectionsPanY_get(this.swigCPtr);
    }

    /* renamed from: od */
    public void mo10995od(float f) {
        MilesBridgeJNI.EAX_REVERB_ReflectionsPanZ_set(this.swigCPtr, f);
    }

    public float dqB() {
        return MilesBridgeJNI.EAX_REVERB_ReflectionsPanZ_get(this.swigCPtr);
    }

    /* renamed from: Aq */
    public void mo10952Aq(int i) {
        MilesBridgeJNI.EAX_REVERB_Reverb_set(this.swigCPtr, i);
    }

    public int dqC() {
        return MilesBridgeJNI.EAX_REVERB_Reverb_get(this.swigCPtr);
    }

    /* renamed from: oe */
    public void mo10996oe(float f) {
        MilesBridgeJNI.EAX_REVERB_ReverbDelay_set(this.swigCPtr, f);
    }

    public float dqD() {
        return MilesBridgeJNI.EAX_REVERB_ReverbDelay_get(this.swigCPtr);
    }

    /* renamed from: of */
    public void mo10997of(float f) {
        MilesBridgeJNI.EAX_REVERB_ReverbPanX_set(this.swigCPtr, f);
    }

    public float dqE() {
        return MilesBridgeJNI.EAX_REVERB_ReverbPanX_get(this.swigCPtr);
    }

    /* renamed from: og */
    public void mo10998og(float f) {
        MilesBridgeJNI.EAX_REVERB_ReverbPanY_set(this.swigCPtr, f);
    }

    public float dqF() {
        return MilesBridgeJNI.EAX_REVERB_ReverbPanY_get(this.swigCPtr);
    }

    /* renamed from: oh */
    public void mo10999oh(float f) {
        MilesBridgeJNI.EAX_REVERB_ReverbPanZ_set(this.swigCPtr, f);
    }

    public float dqG() {
        return MilesBridgeJNI.EAX_REVERB_ReverbPanZ_get(this.swigCPtr);
    }

    /* renamed from: oi */
    public void mo11000oi(float f) {
        MilesBridgeJNI.EAX_REVERB_EchoTime_set(this.swigCPtr, f);
    }

    public float dqH() {
        return MilesBridgeJNI.EAX_REVERB_EchoTime_get(this.swigCPtr);
    }

    /* renamed from: oj */
    public void mo11001oj(float f) {
        MilesBridgeJNI.EAX_REVERB_EchoDepth_set(this.swigCPtr, f);
    }

    public float dqI() {
        return MilesBridgeJNI.EAX_REVERB_EchoDepth_get(this.swigCPtr);
    }

    /* renamed from: ok */
    public void mo11002ok(float f) {
        MilesBridgeJNI.EAX_REVERB_ModulationTime_set(this.swigCPtr, f);
    }

    public float dqJ() {
        return MilesBridgeJNI.EAX_REVERB_ModulationTime_get(this.swigCPtr);
    }

    /* renamed from: ol */
    public void mo11003ol(float f) {
        MilesBridgeJNI.EAX_REVERB_ModulationDepth_set(this.swigCPtr, f);
    }

    public float dqK() {
        return MilesBridgeJNI.EAX_REVERB_ModulationDepth_get(this.swigCPtr);
    }

    /* renamed from: om */
    public void mo11004om(float f) {
        MilesBridgeJNI.EAX_REVERB_AirAbsorptionHF_set(this.swigCPtr, f);
    }

    public float dqL() {
        return MilesBridgeJNI.EAX_REVERB_AirAbsorptionHF_get(this.swigCPtr);
    }

    /* renamed from: on */
    public void mo11005on(float f) {
        MilesBridgeJNI.EAX_REVERB_HFReference_set(this.swigCPtr, f);
    }

    public float dqM() {
        return MilesBridgeJNI.EAX_REVERB_HFReference_get(this.swigCPtr);
    }

    /* renamed from: oo */
    public void mo11006oo(float f) {
        MilesBridgeJNI.EAX_REVERB_LFReference_set(this.swigCPtr, f);
    }

    public float dqN() {
        return MilesBridgeJNI.EAX_REVERB_LFReference_get(this.swigCPtr);
    }

    /* renamed from: op */
    public void mo11007op(float f) {
        MilesBridgeJNI.EAX_REVERB_RoomRolloffFactor_set(this.swigCPtr, f);
    }

    public float dqO() {
        return MilesBridgeJNI.EAX_REVERB_RoomRolloffFactor_get(this.swigCPtr);
    }

    public long getFlags() {
        return MilesBridgeJNI.EAX_REVERB_Flags_get(this.swigCPtr);
    }

    public void setFlags(long j) {
        MilesBridgeJNI.EAX_REVERB_Flags_set(this.swigCPtr, j);
    }
}
