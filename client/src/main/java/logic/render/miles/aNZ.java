package logic.render.miles;


import utaikodom.render.miles.MilesBridgeJNI;

/* renamed from: a.aNZ */
/* compiled from: a */
public class aNZ {
    public boolean swigCMemOwn;
    private long swigCPtr;

    public aNZ(long j, boolean z) {
        this.swigCMemOwn = z;
        this.swigCPtr = j;
    }

    public aNZ() {
        this(MilesBridgeJNI.new_DIG_DRIVER(), true);
    }

    /* renamed from: q */
    public static long m16621q(aNZ anz) {
        if (anz == null) {
            return 0;
        }
        return anz.swigCPtr;
    }

    /* access modifiers changed from: protected */
    public void finalize() {
        delete();
    }

    public void delete() {
        if (this.swigCPtr != 0 && this.swigCMemOwn) {
            this.swigCMemOwn = false;
            MilesBridgeJNI.delete_DIG_DRIVER(this.swigCPtr);
        }
        this.swigCPtr = 0;
    }

    public String getTag() {
        return MilesBridgeJNI.DIG_DRIVER_tag_get(this.swigCPtr);
    }

    public void setTag(String str) {
        MilesBridgeJNI.DIG_DRIVER_tag_set(this.swigCPtr, str);
    }

    /* renamed from: zb */
    public void mo10467zb(int i) {
        MilesBridgeJNI.DIG_DRIVER_backgroundtimer_set(this.swigCPtr, i);
    }

    public int dkW() {
        return MilesBridgeJNI.DIG_DRIVER_backgroundtimer_get(this.swigCPtr);
    }

    /* renamed from: nA */
    public void mo10439nA(float f) {
        MilesBridgeJNI.DIG_DRIVER_master_volume_set(this.swigCPtr, f);
    }

    public float dkX() {
        return MilesBridgeJNI.DIG_DRIVER_master_volume_get(this.swigCPtr);
    }

    /* renamed from: zc */
    public void mo10468zc(int i) {
        MilesBridgeJNI.DIG_DRIVER_DMA_rate_set(this.swigCPtr, i);
    }

    public int dkY() {
        return MilesBridgeJNI.DIG_DRIVER_DMA_rate_get(this.swigCPtr);
    }

    /* renamed from: zd */
    public void mo10469zd(int i) {
        MilesBridgeJNI.DIG_DRIVER_hw_format_set(this.swigCPtr, i);
    }

    public int dkZ() {
        return MilesBridgeJNI.DIG_DRIVER_hw_format_get(this.swigCPtr);
    }

    /* renamed from: ze */
    public void mo10470ze(int i) {
        MilesBridgeJNI.DIG_DRIVER_n_active_samples_set(this.swigCPtr, i);
    }

    public int dla() {
        return MilesBridgeJNI.DIG_DRIVER_n_active_samples_get(this.swigCPtr);
    }

    /* renamed from: a */
    public void mo10315a(C5981aeF aef) {
        MilesBridgeJNI.DIG_DRIVER_channel_spec_set(this.swigCPtr, aef.swigValue());
    }

    public C5981aeF dlb() {
        return C5981aeF.m21164qn(MilesBridgeJNI.DIG_DRIVER_channel_spec_get(this.swigCPtr));
    }

    /* renamed from: b */
    public void mo10319b(C6232aiw aiw) {
        MilesBridgeJNI.DIG_DRIVER_D3D_set(this.swigCPtr, C6232aiw.m22703a(aiw));
    }

    public C6232aiw dlc() {
        long DIG_DRIVER_D3D_get = MilesBridgeJNI.DIG_DRIVER_D3D_get(this.swigCPtr);
        if (DIG_DRIVER_D3D_get == 0) {
            return null;
        }
        return new C6232aiw(DIG_DRIVER_D3D_get, false);
    }

    /* renamed from: zf */
    public void mo10471zf(int i) {
        MilesBridgeJNI.DIG_DRIVER_quiet_set(this.swigCPtr, i);
    }

    public int dld() {
        return MilesBridgeJNI.DIG_DRIVER_quiet_get(this.swigCPtr);
    }

    /* renamed from: lp */
    public void mo10426lp(long j) {
        MilesBridgeJNI.DIG_DRIVER_hw_mode_flags_set(this.swigCPtr, j);
    }

    public long dle() {
        return MilesBridgeJNI.DIG_DRIVER_hw_mode_flags_get(this.swigCPtr);
    }

    /* renamed from: zg */
    public void mo10472zg(int i) {
        MilesBridgeJNI.DIG_DRIVER_playing_set(this.swigCPtr, i);
    }

    public int dlf() {
        return MilesBridgeJNI.DIG_DRIVER_playing_get(this.swigCPtr);
    }

    /* renamed from: zh */
    public void mo10473zh(int i) {
        MilesBridgeJNI.DIG_DRIVER_bytes_per_channel_set(this.swigCPtr, i);
    }

    public int dlg() {
        return MilesBridgeJNI.DIG_DRIVER_bytes_per_channel_get(this.swigCPtr);
    }

    /* renamed from: zi */
    public void mo10474zi(int i) {
        MilesBridgeJNI.DIG_DRIVER_samples_per_buffer_set(this.swigCPtr, i);
    }

    public int dlh() {
        return MilesBridgeJNI.DIG_DRIVER_samples_per_buffer_get(this.swigCPtr);
    }

    /* renamed from: zj */
    public void mo10475zj(int i) {
        MilesBridgeJNI.DIG_DRIVER_physical_channels_per_sample_set(this.swigCPtr, i);
    }

    public int dli() {
        return MilesBridgeJNI.DIG_DRIVER_physical_channels_per_sample_get(this.swigCPtr);
    }

    /* renamed from: zk */
    public void mo10476zk(int i) {
        MilesBridgeJNI.DIG_DRIVER_logical_channels_per_sample_set(this.swigCPtr, i);
    }

    public int dlj() {
        return MilesBridgeJNI.DIG_DRIVER_logical_channels_per_sample_get(this.swigCPtr);
    }

    /* renamed from: u */
    public void mo10455u(C1467Vb vb) {
        MilesBridgeJNI.DIG_DRIVER_samples_set(this.swigCPtr, C1467Vb.m10693c(vb));
    }

    public C1467Vb dlk() {
        long DIG_DRIVER_samples_get = MilesBridgeJNI.DIG_DRIVER_samples_get(this.swigCPtr);
        if (DIG_DRIVER_samples_get == 0) {
            return null;
        }
        return new C1467Vb(DIG_DRIVER_samples_get, false);
    }

    /* renamed from: g */
    public void mo10417g(C5758aVq avq) {
        MilesBridgeJNI.DIG_DRIVER_sample_status_set(this.swigCPtr, C5758aVq.m19143h(avq));
    }

    public C5758aVq dll() {
        long DIG_DRIVER_sample_status_get = MilesBridgeJNI.DIG_DRIVER_sample_status_get(this.swigCPtr);
        if (DIG_DRIVER_sample_status_get == 0) {
            return null;
        }
        return new C5758aVq(DIG_DRIVER_sample_status_get, false);
    }

    /* renamed from: zl */
    public void mo10477zl(int i) {
        MilesBridgeJNI.DIG_DRIVER_n_samples_set(this.swigCPtr, i);
    }

    public int dlm() {
        return MilesBridgeJNI.DIG_DRIVER_n_samples_get(this.swigCPtr);
    }

    /* renamed from: o */
    public void mo10447o(C2480fe feVar) {
        MilesBridgeJNI.DIG_DRIVER_system_data_set(this.swigCPtr, C2480fe.m31244a(feVar));
    }

    public C2480fe bzk() {
        long DIG_DRIVER_system_data_get = MilesBridgeJNI.DIG_DRIVER_system_data_get(this.swigCPtr);
        if (DIG_DRIVER_system_data_get == 0) {
            return null;
        }
        return new C2480fe(DIG_DRIVER_system_data_get, false);
    }

    /* renamed from: b */
    public void mo10316b(aKL akl) {
        MilesBridgeJNI.DIG_DRIVER_build_set(this.swigCPtr, aKL.m15944a(akl));
    }

    public aKL dln() {
        long DIG_DRIVER_build_get = MilesBridgeJNI.DIG_DRIVER_build_get(this.swigCPtr);
        if (DIG_DRIVER_build_get == 0) {
            return null;
        }
        return new aKL(DIG_DRIVER_build_get, false);
    }

    /* renamed from: zm */
    public void mo10478zm(int i) {
        MilesBridgeJNI.DIG_DRIVER_n_build_buffers_set(this.swigCPtr, i);
    }

    public int dlo() {
        return MilesBridgeJNI.DIG_DRIVER_n_build_buffers_get(this.swigCPtr);
    }

    /* renamed from: zn */
    public void mo10479zn(int i) {
        MilesBridgeJNI.DIG_DRIVER_build_size_set(this.swigCPtr, i);
    }

    public int dlp() {
        return MilesBridgeJNI.DIG_DRIVER_build_size_get(this.swigCPtr);
    }

    /* renamed from: zo */
    public void mo10480zo(int i) {
        MilesBridgeJNI.DIG_DRIVER_hardware_buffer_size_set(this.swigCPtr, i);
    }

    public int dlq() {
        return MilesBridgeJNI.DIG_DRIVER_hardware_buffer_size_get(this.swigCPtr);
    }

    /* renamed from: b */
    public void mo10317b(aKM akm) {
        MilesBridgeJNI.DIG_DRIVER_hWaveOut_set(this.swigCPtr, aKM.m15949a(akm));
    }

    public aKM dlr() {
        long DIG_DRIVER_hWaveOut_get = MilesBridgeJNI.DIG_DRIVER_hWaveOut_get(this.swigCPtr);
        if (DIG_DRIVER_hWaveOut_get == 0) {
            return null;
        }
        return new aKM(DIG_DRIVER_hWaveOut_get, false);
    }

    /* renamed from: lq */
    public void mo10427lq(long j) {
        MilesBridgeJNI.DIG_DRIVER_reset_works_set(this.swigCPtr, j);
    }

    public long dls() {
        return MilesBridgeJNI.DIG_DRIVER_reset_works_get(this.swigCPtr);
    }

    /* renamed from: lr */
    public void mo10428lr(long j) {
        MilesBridgeJNI.DIG_DRIVER_request_reset_set(this.swigCPtr, j);
    }

    public long dlt() {
        return MilesBridgeJNI.DIG_DRIVER_request_reset_get(this.swigCPtr);
    }

    /* renamed from: b */
    public void mo10321b(C3701ty tyVar) {
        MilesBridgeJNI.DIG_DRIVER_first_set(this.swigCPtr, C3701ty.m39787a(tyVar));
    }

    public C3701ty dlu() {
        long DIG_DRIVER_first_get = MilesBridgeJNI.DIG_DRIVER_first_get(this.swigCPtr);
        if (DIG_DRIVER_first_get == 0) {
            return null;
        }
        return new C3701ty(DIG_DRIVER_first_get, false);
    }

    /* renamed from: og */
    public void mo10448og(int i) {
        MilesBridgeJNI.DIG_DRIVER_n_buffers_set(this.swigCPtr, i);
    }

    public int byA() {
        return MilesBridgeJNI.DIG_DRIVER_n_buffers_get(this.swigCPtr);
    }

    /* renamed from: a */
    public void mo10312a(C5638aRa ara) {
        MilesBridgeJNI.DIG_DRIVER_return_list_set(this.swigCPtr, C5638aRa.m17828b(ara));
    }

    public C5638aRa dlv() {
        long DIG_DRIVER_return_list_get = MilesBridgeJNI.DIG_DRIVER_return_list_get(this.swigCPtr);
        if (DIG_DRIVER_return_list_get == 0) {
            return null;
        }
        return new C5638aRa(DIG_DRIVER_return_list_get, false);
    }

    /* renamed from: zp */
    public void mo10481zp(int i) {
        MilesBridgeJNI.DIG_DRIVER_return_head_set(this.swigCPtr, i);
    }

    public int dlw() {
        return MilesBridgeJNI.DIG_DRIVER_return_head_get(this.swigCPtr);
    }

    /* renamed from: zq */
    public void mo10482zq(int i) {
        MilesBridgeJNI.DIG_DRIVER_return_tail_set(this.swigCPtr, i);
    }

    public int dlx() {
        return MilesBridgeJNI.DIG_DRIVER_return_tail_get(this.swigCPtr);
    }

    /* renamed from: ls */
    public void mo10429ls(long j) {
        MilesBridgeJNI.DIG_DRIVER_guid_set(this.swigCPtr, j);
    }

    public long dly() {
        return MilesBridgeJNI.DIG_DRIVER_guid_get(this.swigCPtr);
    }

    /* renamed from: m */
    public void mo10437m(aBO abo) {
        MilesBridgeJNI.DIG_DRIVER_pDS_set(this.swigCPtr, aBO.m12884g(abo));
    }

    public aBO dlz() {
        long DIG_DRIVER_pDS_get = MilesBridgeJNI.DIG_DRIVER_pDS_get(this.swigCPtr);
        if (DIG_DRIVER_pDS_get == 0) {
            return null;
        }
        return new aBO(DIG_DRIVER_pDS_get, false);
    }

    /* renamed from: lt */
    public void mo10430lt(long j) {
        MilesBridgeJNI.DIG_DRIVER_ds_priority_set(this.swigCPtr, j);
    }

    public long dlA() {
        return MilesBridgeJNI.DIG_DRIVER_ds_priority_get(this.swigCPtr);
    }

    /* renamed from: zr */
    public void mo10483zr(int i) {
        MilesBridgeJNI.DIG_DRIVER_emulated_ds_set(this.swigCPtr, i);
    }

    public int dlB() {
        return MilesBridgeJNI.DIG_DRIVER_emulated_ds_get(this.swigCPtr);
    }

    /* renamed from: n */
    public void mo10438n(aBO abo) {
        MilesBridgeJNI.DIG_DRIVER_lppdsb_set(this.swigCPtr, aBO.m12884g(abo));
    }

    public aBO dlC() {
        long DIG_DRIVER_lppdsb_get = MilesBridgeJNI.DIG_DRIVER_lppdsb_get(this.swigCPtr);
        if (DIG_DRIVER_lppdsb_get == 0) {
            return null;
        }
        return new aBO(DIG_DRIVER_lppdsb_get, false);
    }

    /* renamed from: lu */
    public void mo10431lu(long j) {
        MilesBridgeJNI.DIG_DRIVER_dsHwnd_set(this.swigCPtr, j);
    }

    public long dlD() {
        return MilesBridgeJNI.DIG_DRIVER_dsHwnd_get(this.swigCPtr);
    }

    /* renamed from: f */
    public void mo10415f(C2224cz czVar) {
        MilesBridgeJNI.DIG_DRIVER_lpbufflist_set(this.swigCPtr, C2224cz.m28711a(czVar));
    }

    public C2224cz dlE() {
        long DIG_DRIVER_lpbufflist_get = MilesBridgeJNI.DIG_DRIVER_lpbufflist_get(this.swigCPtr);
        if (DIG_DRIVER_lpbufflist_get == 0) {
            return null;
        }
        return new C2224cz(DIG_DRIVER_lpbufflist_get, false);
    }

    /* renamed from: c */
    public void mo10324c(C1527WT wt) {
        MilesBridgeJNI.DIG_DRIVER_samp_list_set(this.swigCPtr, C1527WT.m11210b(wt));
    }

    public C1527WT dlF() {
        long DIG_DRIVER_samp_list_get = MilesBridgeJNI.DIG_DRIVER_samp_list_get(this.swigCPtr);
        if (DIG_DRIVER_samp_list_get == 0) {
            return null;
        }
        return new C1527WT(DIG_DRIVER_samp_list_get, false);
    }

    /* renamed from: Y */
    public void mo10310Y(C2480fe feVar) {
        MilesBridgeJNI.DIG_DRIVER_sec_format_set(this.swigCPtr, C2480fe.m31244a(feVar));
    }

    public C2480fe dlG() {
        long DIG_DRIVER_sec_format_get = MilesBridgeJNI.DIG_DRIVER_sec_format_get(this.swigCPtr);
        if (DIG_DRIVER_sec_format_get == 0) {
            return null;
        }
        return new C2480fe(DIG_DRIVER_sec_format_get, false);
    }

    /* renamed from: zs */
    public void mo10484zs(int i) {
        MilesBridgeJNI.DIG_DRIVER_max_buffs_set(this.swigCPtr, i);
    }

    public int dlH() {
        return MilesBridgeJNI.DIG_DRIVER_max_buffs_get(this.swigCPtr);
    }

    /* renamed from: b */
    public void mo10320b(C6253ajR ajr) {
        MilesBridgeJNI.DIG_DRIVER_wformat_set(this.swigCPtr, C6253ajR.m22894a(ajr));
    }

    public C6253ajR dlI() {
        long DIG_DRIVER_wformat_get = MilesBridgeJNI.DIG_DRIVER_wformat_get(this.swigCPtr);
        if (DIG_DRIVER_wformat_get == 0) {
            return null;
        }
        return new C6253ajR(DIG_DRIVER_wformat_get, false);
    }

    /* renamed from: ni */
    public void mo10445ni(String str) {
        MilesBridgeJNI.DIG_DRIVER_wfextra_set(this.swigCPtr, str);
    }

    public String dlJ() {
        return MilesBridgeJNI.DIG_DRIVER_wfextra_get(this.swigCPtr);
    }

    /* renamed from: sl */
    public void mo10454sl(int i) {
        MilesBridgeJNI.DIG_DRIVER_released_set(this.swigCPtr, i);
    }

    public int chg() {
        return MilesBridgeJNI.DIG_DRIVER_released_get(this.swigCPtr);
    }

    /* renamed from: lv */
    public void mo10432lv(long j) {
        MilesBridgeJNI.DIG_DRIVER_foreground_timer_set(this.swigCPtr, j);
    }

    public long dlK() {
        return MilesBridgeJNI.DIG_DRIVER_foreground_timer_get(this.swigCPtr);
    }

    /* renamed from: r */
    public void mo10450r(aNZ anz) {
        MilesBridgeJNI.DIG_DRIVER_next_set(this.swigCPtr, m16621q(anz));
    }

    public aNZ dlL() {
        long DIG_DRIVER_next_get = MilesBridgeJNI.DIG_DRIVER_next_get(this.swigCPtr);
        if (DIG_DRIVER_next_get == 0) {
            return null;
        }
        return new aNZ(DIG_DRIVER_next_get, false);
    }

    /* renamed from: rH */
    public void mo10451rH(int i) {
        MilesBridgeJNI.DIG_DRIVER_callingCT_set(this.swigCPtr, i);
    }

    public int cfD() {
        return MilesBridgeJNI.DIG_DRIVER_callingCT_get(this.swigCPtr);
    }

    /* renamed from: rI */
    public void mo10452rI(int i) {
        MilesBridgeJNI.DIG_DRIVER_callingDS_set(this.swigCPtr, i);
    }

    public int cfE() {
        return MilesBridgeJNI.DIG_DRIVER_callingDS_get(this.swigCPtr);
    }

    /* renamed from: zt */
    public void mo10485zt(int i) {
        MilesBridgeJNI.DIG_DRIVER_DS_initialized_set(this.swigCPtr, i);
    }

    public int dlM() {
        return MilesBridgeJNI.DIG_DRIVER_DS_initialized_get(this.swigCPtr);
    }

    /* renamed from: o */
    public void mo10446o(aBO abo) {
        MilesBridgeJNI.DIG_DRIVER_DS_sec_buff_set(this.swigCPtr, aBO.m12884g(abo));
    }

    public aBO dlN() {
        long DIG_DRIVER_DS_sec_buff_get = MilesBridgeJNI.DIG_DRIVER_DS_sec_buff_get(this.swigCPtr);
        if (DIG_DRIVER_DS_sec_buff_get == 0) {
            return null;
        }
        return new aBO(DIG_DRIVER_DS_sec_buff_get, false);
    }

    /* renamed from: p */
    public void mo10449p(aBO abo) {
        MilesBridgeJNI.DIG_DRIVER_DS_out_buff_set(this.swigCPtr, aBO.m12884g(abo));
    }

    public aBO dlO() {
        long DIG_DRIVER_DS_out_buff_get = MilesBridgeJNI.DIG_DRIVER_DS_out_buff_get(this.swigCPtr);
        if (DIG_DRIVER_DS_out_buff_get == 0) {
            return null;
        }
        return new aBO(DIG_DRIVER_DS_out_buff_get, false);
    }

    /* renamed from: zu */
    public void mo10486zu(int i) {
        MilesBridgeJNI.DIG_DRIVER_DS_buffer_size_set(this.swigCPtr, i);
    }

    public int dlP() {
        return MilesBridgeJNI.DIG_DRIVER_DS_buffer_size_get(this.swigCPtr);
    }

    /* renamed from: zv */
    public void mo10487zv(int i) {
        MilesBridgeJNI.DIG_DRIVER_DS_frag_cnt_set(this.swigCPtr, i);
    }

    public int dlQ() {
        return MilesBridgeJNI.DIG_DRIVER_DS_frag_cnt_get(this.swigCPtr);
    }

    /* renamed from: zw */
    public void mo10488zw(int i) {
        MilesBridgeJNI.DIG_DRIVER_DS_frag_size_set(this.swigCPtr, i);
    }

    public int dlR() {
        return MilesBridgeJNI.DIG_DRIVER_DS_frag_size_get(this.swigCPtr);
    }

    /* renamed from: zx */
    public void mo10489zx(int i) {
        MilesBridgeJNI.DIG_DRIVER_DS_last_frag_set(this.swigCPtr, i);
    }

    public int dlS() {
        return MilesBridgeJNI.DIG_DRIVER_DS_last_frag_get(this.swigCPtr);
    }

    /* renamed from: zy */
    public void mo10490zy(int i) {
        MilesBridgeJNI.DIG_DRIVER_DS_last_write_set(this.swigCPtr, i);
    }

    public int dlT() {
        return MilesBridgeJNI.DIG_DRIVER_DS_last_write_get(this.swigCPtr);
    }

    /* renamed from: zz */
    public void mo10491zz(int i) {
        MilesBridgeJNI.DIG_DRIVER_DS_last_timer_set(this.swigCPtr, i);
    }

    public int dlU() {
        return MilesBridgeJNI.DIG_DRIVER_DS_last_timer_get(this.swigCPtr);
    }

    /* renamed from: zA */
    public void mo10456zA(int i) {
        MilesBridgeJNI.DIG_DRIVER_DS_skip_time_set(this.swigCPtr, i);
    }

    public int dlV() {
        return MilesBridgeJNI.DIG_DRIVER_DS_skip_time_get(this.swigCPtr);
    }

    /* renamed from: zB */
    public void mo10457zB(int i) {
        MilesBridgeJNI.DIG_DRIVER_DS_use_default_format_set(this.swigCPtr, i);
    }

    public int dlW() {
        return MilesBridgeJNI.DIG_DRIVER_DS_use_default_format_get(this.swigCPtr);
    }

    /* renamed from: lw */
    public void mo10433lw(long j) {
        MilesBridgeJNI.DIG_DRIVER_position_error_set(this.swigCPtr, j);
    }

    public long dlX() {
        return MilesBridgeJNI.DIG_DRIVER_position_error_get(this.swigCPtr);
    }

    /* renamed from: lx */
    public void mo10434lx(long j) {
        MilesBridgeJNI.DIG_DRIVER_last_ds_play_set(this.swigCPtr, j);
    }

    public long dlY() {
        return MilesBridgeJNI.DIG_DRIVER_last_ds_play_get(this.swigCPtr);
    }

    /* renamed from: ly */
    public void mo10435ly(long j) {
        MilesBridgeJNI.DIG_DRIVER_last_ds_write_set(this.swigCPtr, j);
    }

    public long dlZ() {
        return MilesBridgeJNI.DIG_DRIVER_last_ds_write_get(this.swigCPtr);
    }

    /* renamed from: lz */
    public void mo10436lz(long j) {
        MilesBridgeJNI.DIG_DRIVER_last_ds_move_set(this.swigCPtr, j);
    }

    public long dma() {
        return MilesBridgeJNI.DIG_DRIVER_last_ds_move_get(this.swigCPtr);
    }

    /* renamed from: zC */
    public void mo10458zC(int i) {
        MilesBridgeJNI.DIG_DRIVER_use_MMX_set(this.swigCPtr, i);
    }

    public int dmb() {
        return MilesBridgeJNI.DIG_DRIVER_use_MMX_get(this.swigCPtr);
    }

    /* renamed from: lA */
    public void mo10419lA(long j) {
        MilesBridgeJNI.DIG_DRIVER_us_count_set(this.swigCPtr, j);
    }

    public long dmc() {
        return MilesBridgeJNI.DIG_DRIVER_us_count_get(this.swigCPtr);
    }

    /* renamed from: lB */
    public void mo10420lB(long j) {
        MilesBridgeJNI.DIG_DRIVER_ms_count_set(this.swigCPtr, j);
    }

    public long dmd() {
        return MilesBridgeJNI.DIG_DRIVER_ms_count_get(this.swigCPtr);
    }

    /* renamed from: lC */
    public void mo10421lC(long j) {
        MilesBridgeJNI.DIG_DRIVER_last_ms_polled_set(this.swigCPtr, j);
    }

    public long dme() {
        return MilesBridgeJNI.DIG_DRIVER_last_ms_polled_get(this.swigCPtr);
    }

    /* renamed from: lD */
    public void mo10422lD(long j) {
        MilesBridgeJNI.DIG_DRIVER_last_percent_set(this.swigCPtr, j);
    }

    public long dmf() {
        return MilesBridgeJNI.DIG_DRIVER_last_percent_get(this.swigCPtr);
    }

    /* renamed from: b */
    public void mo10318b(aMM amm) {
        MilesBridgeJNI.DIG_DRIVER_pipeline_set(this.swigCPtr, aMM.m16363a(amm));
    }

    public aMM dmg() {
        long DIG_DRIVER_pipeline_get = MilesBridgeJNI.DIG_DRIVER_pipeline_get(this.swigCPtr);
        if (DIG_DRIVER_pipeline_get == 0) {
            return null;
        }
        return new aMM(DIG_DRIVER_pipeline_get, false);
    }

    /* renamed from: d */
    public void mo10328d(C6153ahV ahv) {
        MilesBridgeJNI.DIG_DRIVER_voice_filter_set(this.swigCPtr, C6153ahV.m22222a(ahv));
    }

    public C6153ahV dmh() {
        long DIG_DRIVER_voice_filter_get = MilesBridgeJNI.DIG_DRIVER_voice_filter_get(this.swigCPtr);
        if (DIG_DRIVER_voice_filter_get == 0) {
            return null;
        }
        return new C6153ahV(DIG_DRIVER_voice_filter_get, false);
    }

    /* renamed from: a */
    public void mo10314a(C5715aTz atz) {
        MilesBridgeJNI.DIG_DRIVER_stream_callback_set(this.swigCPtr, C5715aTz.m18490b(atz));
    }

    public C5715aTz dmi() {
        long DIG_DRIVER_stream_callback_get = MilesBridgeJNI.DIG_DRIVER_stream_callback_get(this.swigCPtr);
        if (DIG_DRIVER_stream_callback_get == 0) {
            return null;
        }
        return new C5715aTz(DIG_DRIVER_stream_callback_get, false);
    }

    /* renamed from: e */
    public void mo10414e(C6153ahV ahv) {
        MilesBridgeJNI.DIG_DRIVER_matrix_filter_set(this.swigCPtr, C6153ahV.m22222a(ahv));
    }

    public C6153ahV dmj() {
        long DIG_DRIVER_matrix_filter_get = MilesBridgeJNI.DIG_DRIVER_matrix_filter_get(this.swigCPtr);
        if (DIG_DRIVER_matrix_filter_get == 0) {
            return null;
        }
        return new C6153ahV(DIG_DRIVER_matrix_filter_get, false);
    }

    /* renamed from: zD */
    public void mo10459zD(int i) {
        MilesBridgeJNI.DIG_DRIVER_room_type_set(this.swigCPtr, i);
    }

    public int dmk() {
        return MilesBridgeJNI.DIG_DRIVER_room_type_get(this.swigCPtr);
    }

    /* renamed from: nB */
    public void mo10440nB(float f) {
        MilesBridgeJNI.DIG_DRIVER_master_wet_set(this.swigCPtr, f);
    }

    public float dml() {
        return MilesBridgeJNI.DIG_DRIVER_master_wet_get(this.swigCPtr);
    }

    /* renamed from: nC */
    public void mo10441nC(float f) {
        MilesBridgeJNI.DIG_DRIVER_master_dry_set(this.swigCPtr, f);
    }

    public float dmm() {
        return MilesBridgeJNI.DIG_DRIVER_master_dry_get(this.swigCPtr);
    }

    /* renamed from: a */
    public void mo10313a(C5666aSc asc) {
        MilesBridgeJNI.DIG_DRIVER_ri_set(this.swigCPtr, C5666aSc.m18218b(asc));
    }

    public C5666aSc dmn() {
        long DIG_DRIVER_ri_get = MilesBridgeJNI.DIG_DRIVER_ri_get(this.swigCPtr);
        if (DIG_DRIVER_ri_get == 0) {
            return null;
        }
        return new C5666aSc(DIG_DRIVER_ri_get, false);
    }

    /* renamed from: Z */
    public void mo10311Z(C2480fe feVar) {
        MilesBridgeJNI.DIG_DRIVER_reverb_build_buffer_set(this.swigCPtr, C2480fe.m31244a(feVar));
    }

    public C2480fe dmo() {
        long DIG_DRIVER_reverb_build_buffer_get = MilesBridgeJNI.DIG_DRIVER_reverb_build_buffer_get(this.swigCPtr);
        if (DIG_DRIVER_reverb_build_buffer_get == 0) {
            return null;
        }
        return new C2480fe(DIG_DRIVER_reverb_build_buffer_get, false);
    }

    /* renamed from: zE */
    public void mo10460zE(int i) {
        MilesBridgeJNI.DIG_DRIVER_reverb_total_size_set(this.swigCPtr, i);
    }

    public int dmp() {
        return MilesBridgeJNI.DIG_DRIVER_reverb_total_size_get(this.swigCPtr);
    }

    /* renamed from: zF */
    public void mo10461zF(int i) {
        MilesBridgeJNI.DIG_DRIVER_reverb_fragment_size_set(this.swigCPtr, i);
    }

    public int dmq() {
        return MilesBridgeJNI.DIG_DRIVER_reverb_fragment_size_get(this.swigCPtr);
    }

    /* renamed from: zG */
    public void mo10462zG(int i) {
        MilesBridgeJNI.DIG_DRIVER_reverb_buffer_size_set(this.swigCPtr, i);
    }

    public int dmr() {
        return MilesBridgeJNI.DIG_DRIVER_reverb_buffer_size_get(this.swigCPtr);
    }

    /* renamed from: zH */
    public void mo10463zH(int i) {
        MilesBridgeJNI.DIG_DRIVER_reverb_on_set(this.swigCPtr, i);
    }

    public int dms() {
        return MilesBridgeJNI.DIG_DRIVER_reverb_on_get(this.swigCPtr);
    }

    /* renamed from: lE */
    public void mo10423lE(long j) {
        MilesBridgeJNI.DIG_DRIVER_reverb_off_time_ms_set(this.swigCPtr, j);
    }

    public long dmt() {
        return MilesBridgeJNI.DIG_DRIVER_reverb_off_time_ms_get(this.swigCPtr);
    }

    /* renamed from: lF */
    public void mo10424lF(long j) {
        MilesBridgeJNI.DIG_DRIVER_reverb_duration_ms_set(this.swigCPtr, j);
    }

    public long dmu() {
        return MilesBridgeJNI.DIG_DRIVER_reverb_duration_ms_get(this.swigCPtr);
    }

    /* renamed from: nD */
    public void mo10442nD(float f) {
        MilesBridgeJNI.DIG_DRIVER_reverb_decay_time_s_set(this.swigCPtr, f);
    }

    public float dmv() {
        return MilesBridgeJNI.DIG_DRIVER_reverb_decay_time_s_get(this.swigCPtr);
    }

    /* renamed from: nE */
    public void mo10443nE(float f) {
        MilesBridgeJNI.DIG_DRIVER_reverb_predelay_s_set(this.swigCPtr, f);
    }

    public float dmw() {
        return MilesBridgeJNI.DIG_DRIVER_reverb_predelay_s_get(this.swigCPtr);
    }

    /* renamed from: nF */
    public void mo10444nF(float f) {
        MilesBridgeJNI.DIG_DRIVER_reverb_damping_set(this.swigCPtr, f);
    }

    public float dmx() {
        return MilesBridgeJNI.DIG_DRIVER_reverb_damping_get(this.swigCPtr);
    }

    /* renamed from: zI */
    public void mo10464zI(int i) {
        MilesBridgeJNI.DIG_DRIVER_reverb_head_set(this.swigCPtr, i);
    }

    public int dmy() {
        return MilesBridgeJNI.DIG_DRIVER_reverb_head_get(this.swigCPtr);
    }

    /* renamed from: zJ */
    public void mo10465zJ(int i) {
        MilesBridgeJNI.DIG_DRIVER_reverb_tail_set(this.swigCPtr, i);
    }

    public int dmz() {
        return MilesBridgeJNI.DIG_DRIVER_reverb_tail_get(this.swigCPtr);
    }

    /* renamed from: zK */
    public void mo10466zK(int i) {
        MilesBridgeJNI.DIG_DRIVER_no_wom_done_set(this.swigCPtr, i);
    }

    public int dmA() {
        return MilesBridgeJNI.DIG_DRIVER_no_wom_done_get(this.swigCPtr);
    }

    /* renamed from: lG */
    public void mo10425lG(long j) {
        MilesBridgeJNI.DIG_DRIVER_wom_done_buffers_set(this.swigCPtr, j);
    }

    public long dmB() {
        return MilesBridgeJNI.DIG_DRIVER_wom_done_buffers_get(this.swigCPtr);
    }
}
