package logic.render.miles;

import utaikodom.render.miles.MilesBridgeJNI;

/* renamed from: a.atF  reason: case insensitive filesystem */
/* compiled from: a */
public class C6761atF {
    public boolean swigCMemOwn;
    private long swigCPtr;

    public C6761atF(long j, boolean z) {
        this.swigCMemOwn = z;
        this.swigCPtr = j;
    }

    public C6761atF() {
        this(MilesBridgeJNI.new_REDBOOK(), true);
    }

    /* renamed from: m */
    public static long m25826m(C6761atF atf) {
        if (atf == null) {
            return 0;
        }
        return atf.swigCPtr;
    }

    /* access modifiers changed from: protected */
    public void finalize() {
        delete();
    }

    public void delete() {
        if (this.swigCPtr != 0 && this.swigCMemOwn) {
            this.swigCMemOwn = false;
            MilesBridgeJNI.delete_REDBOOK(this.swigCPtr);
        }
        this.swigCPtr = 0;
    }

    /* renamed from: iQ */
    public void mo16020iQ(long j) {
        MilesBridgeJNI.REDBOOK_DeviceID_set(this.swigCPtr, j);
    }

    public long cwl() {
        return MilesBridgeJNI.REDBOOK_DeviceID_get(this.swigCPtr);
    }

    /* renamed from: iR */
    public void mo16021iR(long j) {
        MilesBridgeJNI.REDBOOK_paused_set(this.swigCPtr, j);
    }

    public long cwm() {
        return MilesBridgeJNI.REDBOOK_paused_get(this.swigCPtr);
    }

    /* renamed from: iS */
    public void mo16022iS(long j) {
        MilesBridgeJNI.REDBOOK_pausedsec_set(this.swigCPtr, j);
    }

    public long cwn() {
        return MilesBridgeJNI.REDBOOK_pausedsec_get(this.swigCPtr);
    }

    /* renamed from: iT */
    public void mo16023iT(long j) {
        MilesBridgeJNI.REDBOOK_lastendsec_set(this.swigCPtr, j);
    }

    public long cwo() {
        return MilesBridgeJNI.REDBOOK_lastendsec_get(this.swigCPtr);
    }
}
