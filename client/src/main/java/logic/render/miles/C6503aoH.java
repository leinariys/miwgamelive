package logic.render.miles;

import utaikodom.render.miles.MilesBridgeJNI;

/* renamed from: a.aoH  reason: case insensitive filesystem */
/* compiled from: a */
public class C6503aoH {
    public boolean swigCMemOwn;
    private long swigCPtr;

    public C6503aoH(long j, boolean z) {
        this.swigCMemOwn = z;
        this.swigCPtr = j;
    }

    public C6503aoH() {
        this(MilesBridgeJNI.new_EAX_DISTORTION(), true);
    }

    /* renamed from: a */
    public static long m24371a(C6503aoH aoh) {
        if (aoh == null) {
            return 0;
        }
        return aoh.swigCPtr;
    }

    /* access modifiers changed from: protected */
    public void finalize() {
        delete();
    }

    public void delete() {
        if (this.swigCPtr != 0 && this.swigCMemOwn) {
            this.swigCMemOwn = false;
            MilesBridgeJNI.delete_EAX_DISTORTION(this.swigCPtr);
        }
        this.swigCPtr = 0;
    }

    /* renamed from: aD */
    public void mo15152aD(int i) {
        MilesBridgeJNI.EAX_DISTORTION_Effect_set(this.swigCPtr, i);
    }

    /* renamed from: ma */
    public int mo15165ma() {
        return MilesBridgeJNI.EAX_DISTORTION_Effect_get(this.swigCPtr);
    }

    public int getVolume() {
        return MilesBridgeJNI.EAX_DISTORTION_Volume_get(this.swigCPtr);
    }

    public void setVolume(int i) {
        MilesBridgeJNI.EAX_DISTORTION_Volume_set(this.swigCPtr, i);
    }

    /* renamed from: jX */
    public void mo15161jX(float f) {
        MilesBridgeJNI.EAX_DISTORTION_Edge_set(this.swigCPtr, f);
    }

    public float cnX() {
        return MilesBridgeJNI.EAX_DISTORTION_Edge_get(this.swigCPtr);
    }

    /* renamed from: ti */
    public void mo15167ti(int i) {
        MilesBridgeJNI.EAX_DISTORTION_Gain_set(this.swigCPtr, i);
    }

    public int cnY() {
        return MilesBridgeJNI.EAX_DISTORTION_Gain_get(this.swigCPtr);
    }

    /* renamed from: jY */
    public void mo15162jY(float f) {
        MilesBridgeJNI.EAX_DISTORTION_LowPassCutOff_set(this.swigCPtr, f);
    }

    public float cnZ() {
        return MilesBridgeJNI.EAX_DISTORTION_LowPassCutOff_get(this.swigCPtr);
    }

    /* renamed from: jZ */
    public void mo15163jZ(float f) {
        MilesBridgeJNI.EAX_DISTORTION_EQCenter_set(this.swigCPtr, f);
    }

    public float coa() {
        return MilesBridgeJNI.EAX_DISTORTION_EQCenter_get(this.swigCPtr);
    }

    /* renamed from: ka */
    public void mo15164ka(float f) {
        MilesBridgeJNI.EAX_DISTORTION_EQBandwidth_set(this.swigCPtr, f);
    }

    public float cob() {
        return MilesBridgeJNI.EAX_DISTORTION_EQBandwidth_get(this.swigCPtr);
    }
}
