package logic.render.miles;

import utaikodom.render.miles.MilesBridgeJNI;

/* renamed from: a.aNR */
/* compiled from: a */
public class aNR {
    public boolean swigCMemOwn;
    private long swigCPtr;

    public aNR(long j, boolean z) {
        this.swigCMemOwn = z;
        this.swigCPtr = j;
    }

    public aNR() {
        this(MilesBridgeJNI.new_REVERB_UPDATED_INFO(), true);
    }

    /* renamed from: a */
    public static long m16565a(aNR anr) {
        if (anr == null) {
            return 0;
        }
        return anr.swigCPtr;
    }

    /* access modifiers changed from: protected */
    public void finalize() {
        delete();
    }

    public void delete() {
        if (this.swigCPtr != 0 && this.swigCMemOwn) {
            this.swigCMemOwn = false;
            MilesBridgeJNI.delete_REVERB_UPDATED_INFO(this.swigCPtr);
        }
        this.swigCPtr = 0;
    }

    /* renamed from: r */
    public void mo10244r(C2512gC gCVar) {
        MilesBridgeJNI.REVERB_UPDATED_INFO_address0_set(this.swigCPtr, C2512gC.m31656a(gCVar));
    }

    public C2512gC dkw() {
        long REVERB_UPDATED_INFO_address0_get = MilesBridgeJNI.REVERB_UPDATED_INFO_address0_get(this.swigCPtr);
        if (REVERB_UPDATED_INFO_address0_get == 0) {
            return null;
        }
        return new C2512gC(REVERB_UPDATED_INFO_address0_get, false);
    }

    /* renamed from: s */
    public void mo10245s(C2512gC gCVar) {
        MilesBridgeJNI.REVERB_UPDATED_INFO_address1_set(this.swigCPtr, C2512gC.m31656a(gCVar));
    }

    public C2512gC dkx() {
        long REVERB_UPDATED_INFO_address1_get = MilesBridgeJNI.REVERB_UPDATED_INFO_address1_get(this.swigCPtr);
        if (REVERB_UPDATED_INFO_address1_get == 0) {
            return null;
        }
        return new C2512gC(REVERB_UPDATED_INFO_address1_get, false);
    }

    /* renamed from: t */
    public void mo10246t(C2512gC gCVar) {
        MilesBridgeJNI.REVERB_UPDATED_INFO_address2_set(this.swigCPtr, C2512gC.m31656a(gCVar));
    }

    public C2512gC dky() {
        long REVERB_UPDATED_INFO_address2_get = MilesBridgeJNI.REVERB_UPDATED_INFO_address2_get(this.swigCPtr);
        if (REVERB_UPDATED_INFO_address2_get == 0) {
            return null;
        }
        return new C2512gC(REVERB_UPDATED_INFO_address2_get, false);
    }

    /* renamed from: u */
    public void mo10247u(C2512gC gCVar) {
        MilesBridgeJNI.REVERB_UPDATED_INFO_address3_set(this.swigCPtr, C2512gC.m31656a(gCVar));
    }

    public C2512gC dkz() {
        long REVERB_UPDATED_INFO_address3_get = MilesBridgeJNI.REVERB_UPDATED_INFO_address3_get(this.swigCPtr);
        if (REVERB_UPDATED_INFO_address3_get == 0) {
            return null;
        }
        return new C2512gC(REVERB_UPDATED_INFO_address3_get, false);
    }

    /* renamed from: v */
    public void mo10248v(C2512gC gCVar) {
        MilesBridgeJNI.REVERB_UPDATED_INFO_address4_set(this.swigCPtr, C2512gC.m31656a(gCVar));
    }

    public C2512gC dkA() {
        long REVERB_UPDATED_INFO_address4_get = MilesBridgeJNI.REVERB_UPDATED_INFO_address4_get(this.swigCPtr);
        if (REVERB_UPDATED_INFO_address4_get == 0) {
            return null;
        }
        return new C2512gC(REVERB_UPDATED_INFO_address4_get, false);
    }

    /* renamed from: w */
    public void mo10249w(C2512gC gCVar) {
        MilesBridgeJNI.REVERB_UPDATED_INFO_address5_set(this.swigCPtr, C2512gC.m31656a(gCVar));
    }

    public C2512gC dkB() {
        long REVERB_UPDATED_INFO_address5_get = MilesBridgeJNI.REVERB_UPDATED_INFO_address5_get(this.swigCPtr);
        if (REVERB_UPDATED_INFO_address5_get == 0) {
            return null;
        }
        return new C2512gC(REVERB_UPDATED_INFO_address5_get, false);
    }

    /* renamed from: nl */
    public void mo10240nl(float f) {
        MilesBridgeJNI.REVERB_UPDATED_INFO_X0_set(this.swigCPtr, f);
    }

    public float dkC() {
        return MilesBridgeJNI.REVERB_UPDATED_INFO_X0_get(this.swigCPtr);
    }

    /* renamed from: nm */
    public void mo10241nm(float f) {
        MilesBridgeJNI.REVERB_UPDATED_INFO_X1_set(this.swigCPtr, f);
    }

    public float dkD() {
        return MilesBridgeJNI.REVERB_UPDATED_INFO_X1_get(this.swigCPtr);
    }

    /* renamed from: nn */
    public void mo10242nn(float f) {
        MilesBridgeJNI.REVERB_UPDATED_INFO_Y0_set(this.swigCPtr, f);
    }

    public float dkE() {
        return MilesBridgeJNI.REVERB_UPDATED_INFO_Y0_get(this.swigCPtr);
    }

    /* renamed from: no */
    public void mo10243no(float f) {
        MilesBridgeJNI.REVERB_UPDATED_INFO_Y1_set(this.swigCPtr, f);
    }

    public float dkF() {
        return MilesBridgeJNI.REVERB_UPDATED_INFO_Y1_get(this.swigCPtr);
    }
}
