package logic.render.miles;

import utaikodom.render.miles.MilesBridgeJNI;

/* renamed from: a.ayV */
/* compiled from: a */
public class ayV {
    public boolean swigCMemOwn;
    private long swigCPtr;

    public ayV(long j, boolean z) {
        this.swigCMemOwn = z;
        this.swigCPtr = j;
    }

    public ayV() {
        this(MilesBridgeJNI.new_DIG_INPUT_DRIVER(), true);
    }

    /* renamed from: c */
    public static long m27430c(ayV ayv) {
        if (ayv == null) {
            return 0;
        }
        return ayv.swigCPtr;
    }

    /* access modifiers changed from: protected */
    public void finalize() {
        delete();
    }

    public void delete() {
        if (this.swigCPtr != 0 && this.swigCMemOwn) {
            this.swigCMemOwn = false;
            MilesBridgeJNI.delete_DIG_INPUT_DRIVER(this.swigCPtr);
        }
        this.swigCPtr = 0;
    }

    public String getTag() {
        return MilesBridgeJNI.DIG_INPUT_DRIVER_tag_get(this.swigCPtr);
    }

    public void setTag(String str) {
        MilesBridgeJNI.DIG_INPUT_DRIVER_tag_set(this.swigCPtr, str);
    }

    /* renamed from: wn */
    public void mo16995wn(int i) {
        MilesBridgeJNI.DIG_INPUT_DRIVER_background_timer_set(this.swigCPtr, i);
    }

    public int cEv() {
        return MilesBridgeJNI.DIG_INPUT_DRIVER_background_timer_get(this.swigCPtr);
    }

    /* renamed from: b */
    public void mo16974b(C5740aUy auy) {
        MilesBridgeJNI.DIG_INPUT_DRIVER_info_set(this.swigCPtr, C5740aUy.m18768c(auy));
    }

    public C5740aUy cEw() {
        long DIG_INPUT_DRIVER_info_get = MilesBridgeJNI.DIG_INPUT_DRIVER_info_get(this.swigCPtr);
        if (DIG_INPUT_DRIVER_info_get == 0) {
            return null;
        }
        return new C5740aUy(DIG_INPUT_DRIVER_info_get, false);
    }

    /* renamed from: wo */
    public void mo16996wo(int i) {
        MilesBridgeJNI.DIG_INPUT_DRIVER_input_enabled_set(this.swigCPtr, i);
    }

    public int cEx() {
        return MilesBridgeJNI.DIG_INPUT_DRIVER_input_enabled_get(this.swigCPtr);
    }

    /* renamed from: ji */
    public void mo16991ji(long j) {
        MilesBridgeJNI.DIG_INPUT_DRIVER_callback_user_set(this.swigCPtr, j);
    }

    public long cEy() {
        return MilesBridgeJNI.DIG_INPUT_DRIVER_callback_user_get(this.swigCPtr);
    }

    /* renamed from: jj */
    public void mo16992jj(long j) {
        MilesBridgeJNI.DIG_INPUT_DRIVER_DMA_size_set(this.swigCPtr, j);
    }

    public long cEz() {
        return MilesBridgeJNI.DIG_INPUT_DRIVER_DMA_size_get(this.swigCPtr);
    }

    /* renamed from: e */
    public void mo16987e(C2224cz czVar) {
        MilesBridgeJNI.DIG_INPUT_DRIVER_DMA_set(this.swigCPtr, C2224cz.m28711a(czVar));
    }

    public C2224cz cEA() {
        long DIG_INPUT_DRIVER_DMA_get = MilesBridgeJNI.DIG_INPUT_DRIVER_DMA_get(this.swigCPtr);
        if (DIG_INPUT_DRIVER_DMA_get == 0) {
            return null;
        }
        return new C2224cz(DIG_INPUT_DRIVER_DMA_get, false);
    }

    public short getSilence() {
        return MilesBridgeJNI.DIG_INPUT_DRIVER_silence_get(this.swigCPtr);
    }

    public void setSilence(short s) {
        MilesBridgeJNI.DIG_INPUT_DRIVER_silence_set(this.swigCPtr, s);
    }

    /* renamed from: wp */
    public void mo16997wp(int i) {
        MilesBridgeJNI.DIG_INPUT_DRIVER_device_active_set(this.swigCPtr, i);
    }

    public int cEB() {
        return MilesBridgeJNI.DIG_INPUT_DRIVER_device_active_get(this.swigCPtr);
    }

    /* renamed from: b */
    public void mo16975b(C6926awO awo) {
        MilesBridgeJNI.DIG_INPUT_DRIVER_hWaveIn_set(this.swigCPtr, C6926awO.m26865a(awo));
    }

    public C6926awO cEC() {
        long DIG_INPUT_DRIVER_hWaveIn_get = MilesBridgeJNI.DIG_INPUT_DRIVER_hWaveIn_get(this.swigCPtr);
        if (DIG_INPUT_DRIVER_hWaveIn_get == 0) {
            return null;
        }
        return new C6926awO(DIG_INPUT_DRIVER_hWaveIn_get, false);
    }

    /* renamed from: c */
    public void mo16976c(C1620Xj xj) {
        MilesBridgeJNI.DIG_INPUT_DRIVER_wavehdr_set(this.swigCPtr, C1620Xj.m11572a(xj));
    }

    public C1620Xj cED() {
        long DIG_INPUT_DRIVER_wavehdr_get = MilesBridgeJNI.DIG_INPUT_DRIVER_wavehdr_get(this.swigCPtr);
        if (DIG_INPUT_DRIVER_wavehdr_get == 0) {
            return null;
        }
        return new C1620Xj(DIG_INPUT_DRIVER_wavehdr_get, false);
    }
}
