package logic.render.miles;

import utaikodom.render.miles.MilesBridgeJNI;

/* renamed from: a.xL */
/* compiled from: a */
public class C3955xL {
    public boolean swigCMemOwn;
    private long swigCPtr;

    public C3955xL(long j, boolean z) {
        this.swigCMemOwn = z;
        this.swigCPtr = j;
    }

    public C3955xL() {
        this(MilesBridgeJNI.new_DLSDEVICE(), true);
    }

    /* renamed from: a */
    public static long m40885a(C3955xL xLVar) {
        if (xLVar == null) {
            return 0;
        }
        return xLVar.swigCPtr;
    }

    /* access modifiers changed from: protected */
    public void finalize() {
        delete();
    }

    public void delete() {
        if (this.swigCPtr != 0 && this.swigCMemOwn) {
            this.swigCMemOwn = false;
            MilesBridgeJNI.delete_DLSDEVICE(this.swigCPtr);
        }
        this.swigCPtr = 0;
    }

    /* renamed from: b */
    public void mo22889b(C3781uy uyVar) {
        MilesBridgeJNI.DLSDEVICE_pGetPref_set(this.swigCPtr, C3781uy.m40127a(uyVar));
    }

    public C3781uy aok() {
        long DLSDEVICE_pGetPref_get = MilesBridgeJNI.DLSDEVICE_pGetPref_get(this.swigCPtr);
        if (DLSDEVICE_pGetPref_get == 0) {
            return null;
        }
        return new C3781uy(DLSDEVICE_pGetPref_get, false);
    }

    /* renamed from: c */
    public void mo22892c(C3781uy uyVar) {
        MilesBridgeJNI.DLSDEVICE_pSetPref_set(this.swigCPtr, C3781uy.m40127a(uyVar));
    }

    public C3781uy aol() {
        long DLSDEVICE_pSetPref_get = MilesBridgeJNI.DLSDEVICE_pSetPref_get(this.swigCPtr);
        if (DLSDEVICE_pSetPref_get == 0) {
            return null;
        }
        return new C3781uy(DLSDEVICE_pSetPref_get, false);
    }

    /* renamed from: d */
    public void mo22893d(C3781uy uyVar) {
        MilesBridgeJNI.DLSDEVICE_pMSSOpen_set(this.swigCPtr, C3781uy.m40127a(uyVar));
    }

    public C3781uy aom() {
        long DLSDEVICE_pMSSOpen_get = MilesBridgeJNI.DLSDEVICE_pMSSOpen_get(this.swigCPtr);
        if (DLSDEVICE_pMSSOpen_get == 0) {
            return null;
        }
        return new C3781uy(DLSDEVICE_pMSSOpen_get, false);
    }

    /* renamed from: e */
    public void mo22897e(C3781uy uyVar) {
        MilesBridgeJNI.DLSDEVICE_pOpen_set(this.swigCPtr, C3781uy.m40127a(uyVar));
    }

    public C3781uy aon() {
        long DLSDEVICE_pOpen_get = MilesBridgeJNI.DLSDEVICE_pOpen_get(this.swigCPtr);
        if (DLSDEVICE_pOpen_get == 0) {
            return null;
        }
        return new C3781uy(DLSDEVICE_pOpen_get, false);
    }

    /* renamed from: f */
    public void mo22898f(C3781uy uyVar) {
        MilesBridgeJNI.DLSDEVICE_pClose_set(this.swigCPtr, C3781uy.m40127a(uyVar));
    }

    public C3781uy aoo() {
        long DLSDEVICE_pClose_get = MilesBridgeJNI.DLSDEVICE_pClose_get(this.swigCPtr);
        if (DLSDEVICE_pClose_get == 0) {
            return null;
        }
        return new C3781uy(DLSDEVICE_pClose_get, false);
    }

    /* renamed from: g */
    public void mo22901g(C3781uy uyVar) {
        MilesBridgeJNI.DLSDEVICE_pLoadFile_set(this.swigCPtr, C3781uy.m40127a(uyVar));
    }

    public C3781uy aop() {
        long DLSDEVICE_pLoadFile_get = MilesBridgeJNI.DLSDEVICE_pLoadFile_get(this.swigCPtr);
        if (DLSDEVICE_pLoadFile_get == 0) {
            return null;
        }
        return new C3781uy(DLSDEVICE_pLoadFile_get, false);
    }

    /* renamed from: h */
    public void mo22903h(C3781uy uyVar) {
        MilesBridgeJNI.DLSDEVICE_pLoadMem_set(this.swigCPtr, C3781uy.m40127a(uyVar));
    }

    public C3781uy aoq() {
        long DLSDEVICE_pLoadMem_get = MilesBridgeJNI.DLSDEVICE_pLoadMem_get(this.swigCPtr);
        if (DLSDEVICE_pLoadMem_get == 0) {
            return null;
        }
        return new C3781uy(DLSDEVICE_pLoadMem_get, false);
    }

    /* renamed from: i */
    public void mo22904i(C3781uy uyVar) {
        MilesBridgeJNI.DLSDEVICE_pUnloadFile_set(this.swigCPtr, C3781uy.m40127a(uyVar));
    }

    public C3781uy aor() {
        long DLSDEVICE_pUnloadFile_get = MilesBridgeJNI.DLSDEVICE_pUnloadFile_get(this.swigCPtr);
        if (DLSDEVICE_pUnloadFile_get == 0) {
            return null;
        }
        return new C3781uy(DLSDEVICE_pUnloadFile_get, false);
    }

    /* renamed from: j */
    public void mo22905j(C3781uy uyVar) {
        MilesBridgeJNI.DLSDEVICE_pUnloadAll_set(this.swigCPtr, C3781uy.m40127a(uyVar));
    }

    public C3781uy aos() {
        long DLSDEVICE_pUnloadAll_get = MilesBridgeJNI.DLSDEVICE_pUnloadAll_get(this.swigCPtr);
        if (DLSDEVICE_pUnloadAll_get == 0) {
            return null;
        }
        return new C3781uy(DLSDEVICE_pUnloadAll_get, false);
    }

    /* renamed from: k */
    public void mo22906k(C3781uy uyVar) {
        MilesBridgeJNI.DLSDEVICE_pGetInfo_set(this.swigCPtr, C3781uy.m40127a(uyVar));
    }

    public C3781uy aot() {
        long DLSDEVICE_pGetInfo_get = MilesBridgeJNI.DLSDEVICE_pGetInfo_get(this.swigCPtr);
        if (DLSDEVICE_pGetInfo_get == 0) {
            return null;
        }
        return new C3781uy(DLSDEVICE_pGetInfo_get, false);
    }

    /* renamed from: l */
    public void mo22907l(C3781uy uyVar) {
        MilesBridgeJNI.DLSDEVICE_pCompact_set(this.swigCPtr, C3781uy.m40127a(uyVar));
    }

    public C3781uy aou() {
        long DLSDEVICE_pCompact_get = MilesBridgeJNI.DLSDEVICE_pCompact_get(this.swigCPtr);
        if (DLSDEVICE_pCompact_get == 0) {
            return null;
        }
        return new C3781uy(DLSDEVICE_pCompact_get, false);
    }

    /* renamed from: m */
    public void mo22908m(C3781uy uyVar) {
        MilesBridgeJNI.DLSDEVICE_pSetAttr_set(this.swigCPtr, C3781uy.m40127a(uyVar));
    }

    public C3781uy aov() {
        long DLSDEVICE_pSetAttr_get = MilesBridgeJNI.DLSDEVICE_pSetAttr_get(this.swigCPtr);
        if (DLSDEVICE_pSetAttr_get == 0) {
            return null;
        }
        return new C3781uy(DLSDEVICE_pSetAttr_get, false);
    }

    /* renamed from: fz */
    public void mo22900fz(int i) {
        MilesBridgeJNI.DLSDEVICE_DLSHandle_set(this.swigCPtr, i);
    }

    public int aow() {
        return MilesBridgeJNI.DLSDEVICE_DLSHandle_get(this.swigCPtr);
    }

    /* renamed from: dq */
    public void mo22895dq(long j) {
        MilesBridgeJNI.DLSDEVICE_format_set(this.swigCPtr, j);
    }

    public long aox() {
        return MilesBridgeJNI.DLSDEVICE_format_get(this.swigCPtr);
    }

    /* renamed from: dr */
    public void mo22896dr(long j) {
        MilesBridgeJNI.DLSDEVICE_buffer_size_set(this.swigCPtr, j);
    }

    public long aoy() {
        return MilesBridgeJNI.DLSDEVICE_buffer_size_get(this.swigCPtr);
    }

    /* renamed from: c */
    public void mo22890c(C2224cz czVar) {
        MilesBridgeJNI.DLSDEVICE_buffer_set(this.swigCPtr, C2224cz.m28711a(czVar));
    }

    public C2224cz aoz() {
        long DLSDEVICE_buffer_get = MilesBridgeJNI.DLSDEVICE_buffer_get(this.swigCPtr);
        if (DLSDEVICE_buffer_get == 0) {
            return null;
        }
        return new C2224cz(DLSDEVICE_buffer_get, false);
    }

    /* renamed from: b */
    public void mo22887b(C1467Vb vb) {
        MilesBridgeJNI.DLSDEVICE_sample_set(this.swigCPtr, C1467Vb.m10693c(vb));
    }

    public C1467Vb aoA() {
        long DLSDEVICE_sample_get = MilesBridgeJNI.DLSDEVICE_sample_get(this.swigCPtr);
        if (DLSDEVICE_sample_get == 0) {
            return null;
        }
        return new C1467Vb(DLSDEVICE_sample_get, false);
    }

    /* renamed from: a */
    public void mo22866a(C6297akJ akj) {
        MilesBridgeJNI.DLSDEVICE_mdi_set(this.swigCPtr, C6297akJ.m23148j(akj));
    }

    public C6297akJ aoB() {
        long DLSDEVICE_mdi_get = MilesBridgeJNI.DLSDEVICE_mdi_get(this.swigCPtr);
        if (DLSDEVICE_mdi_get == 0) {
            return null;
        }
        return new C6297akJ(DLSDEVICE_mdi_get, false);
    }

    /* renamed from: a */
    public void mo22865a(aNZ anz) {
        MilesBridgeJNI.DLSDEVICE_dig_set(this.swigCPtr, aNZ.m16621q(anz));
    }

    public aNZ getDig() {
        long DLSDEVICE_dig_get = MilesBridgeJNI.DLSDEVICE_dig_get(this.swigCPtr);
        if (DLSDEVICE_dig_get == 0) {
            return null;
        }
        return new aNZ(DLSDEVICE_dig_get, false);
    }

    /* renamed from: c */
    public void mo22891c(C3336qQ qQVar) {
        MilesBridgeJNI.DLSDEVICE_first_set(this.swigCPtr, C3336qQ.m37519a(qQVar));
    }

    public C3336qQ aoC() {
        long DLSDEVICE_first_get = MilesBridgeJNI.DLSDEVICE_first_get(this.swigCPtr);
        if (DLSDEVICE_first_get == 0) {
            return null;
        }
        return new C3336qQ(DLSDEVICE_first_get, false);
    }

    /* renamed from: b */
    public void mo22888b(C2978mX mXVar) {
        MilesBridgeJNI.DLSDEVICE_lib_set(this.swigCPtr, C2978mX.m35784a(mXVar));
    }

    public C2978mX aoD() {
        long DLSDEVICE_lib_get = MilesBridgeJNI.DLSDEVICE_lib_get(this.swigCPtr);
        if (DLSDEVICE_lib_get == 0) {
            return null;
        }
        return new C2978mX(DLSDEVICE_lib_get, false);
    }
}
