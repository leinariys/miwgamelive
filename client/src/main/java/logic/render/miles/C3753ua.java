package logic.render.miles;

import utaikodom.render.miles.MilesBridgeJNI;

/* renamed from: a.ua */
/* compiled from: a */
public class C3753ua {
    public boolean swigCMemOwn;
    private long swigCPtr;

    public C3753ua(long j, boolean z) {
        this.swigCMemOwn = z;
        this.swigCPtr = j;
    }

    public C3753ua() {
        this(MilesBridgeJNI.new_TIMB_chunk(), true);
    }

    /* renamed from: a */
    public static long m40070a(C3753ua uaVar) {
        if (uaVar == null) {
            return 0;
        }
        return uaVar.swigCPtr;
    }

    /* access modifiers changed from: protected */
    public void finalize() {
        delete();
    }

    public void delete() {
        if (this.swigCPtr != 0 && this.swigCMemOwn) {
            this.swigCMemOwn = false;
            MilesBridgeJNI.delete_TIMB_chunk(this.swigCPtr);
        }
        this.swigCPtr = 0;
    }

    /* renamed from: b */
    public void mo22445b(C2590hF hFVar) {
        MilesBridgeJNI.TIMB_chunk_name_set(this.swigCPtr, C2590hF.m32535a(hFVar));
    }

    public C2590hF aiU() {
        long TIMB_chunk_name_get = MilesBridgeJNI.TIMB_chunk_name_get(this.swigCPtr);
        if (TIMB_chunk_name_get == 0) {
            return null;
        }
        return new C2590hF(TIMB_chunk_name_get, false);
    }

    /* renamed from: f */
    public void mo22448f(short s) {
        MilesBridgeJNI.TIMB_chunk_msb_set(this.swigCPtr, s);
    }

    public short aiV() {
        return MilesBridgeJNI.TIMB_chunk_msb_get(this.swigCPtr);
    }

    /* renamed from: g */
    public void mo22450g(short s) {
        MilesBridgeJNI.TIMB_chunk_lsb_set(this.swigCPtr, s);
    }

    public short aiW() {
        return MilesBridgeJNI.TIMB_chunk_lsb_get(this.swigCPtr);
    }

    /* renamed from: h */
    public void mo22451h(short s) {
        MilesBridgeJNI.TIMB_chunk_lsb2_set(this.swigCPtr, s);
    }

    public short aiX() {
        return MilesBridgeJNI.TIMB_chunk_lsb2_get(this.swigCPtr);
    }

    /* renamed from: i */
    public void mo22452i(short s) {
        MilesBridgeJNI.TIMB_chunk_lsb3_set(this.swigCPtr, s);
    }

    public short aiY() {
        return MilesBridgeJNI.TIMB_chunk_lsb3_get(this.swigCPtr);
    }

    /* renamed from: eO */
    public void mo22447eO(int i) {
        MilesBridgeJNI.TIMB_chunk_n_entries_set(this.swigCPtr, i);
    }

    public int aiZ() {
        return MilesBridgeJNI.TIMB_chunk_n_entries_get(this.swigCPtr);
    }

    /* renamed from: b */
    public void mo22444b(C2434fQ fQVar) {
        MilesBridgeJNI.TIMB_chunk_timbre_set(this.swigCPtr, C2434fQ.m31038a(fQVar));
    }

    public C2434fQ aja() {
        long TIMB_chunk_timbre_get = MilesBridgeJNI.TIMB_chunk_timbre_get(this.swigCPtr);
        if (TIMB_chunk_timbre_get == 0) {
            return null;
        }
        return new C2434fQ(TIMB_chunk_timbre_get, false);
    }
}
