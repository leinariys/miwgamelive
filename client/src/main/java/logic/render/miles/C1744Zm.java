package logic.render.miles;

import utaikodom.render.miles.MilesBridgeJNI;

/* renamed from: a.Zm */
/* compiled from: a */
public class C1744Zm {
    public boolean swigCMemOwn;
    private long swigCPtr;

    public C1744Zm(long j, boolean z) {
        this.swigCMemOwn = z;
        this.swigCPtr = j;
    }

    public C1744Zm() {
        this(MilesBridgeJNI.new_EAX_SAMPLE_SLOT_VOLUMES(), true);
    }

    /* renamed from: a */
    public static long m12177a(C1744Zm zm) {
        if (zm == null) {
            return 0;
        }
        return zm.swigCPtr;
    }

    /* access modifiers changed from: protected */
    public void finalize() {
        delete();
    }

    public void delete() {
        if (this.swigCPtr != 0 && this.swigCMemOwn) {
            this.swigCMemOwn = false;
            MilesBridgeJNI.delete_EAX_SAMPLE_SLOT_VOLUMES(this.swigCPtr);
        }
        this.swigCPtr = 0;
    }

    /* renamed from: gi */
    public void mo7466gi(long j) {
        MilesBridgeJNI.EAX_SAMPLE_SLOT_VOLUMES_NumVolumes_set(this.swigCPtr, j);
    }

    public long bIq() {
        return MilesBridgeJNI.EAX_SAMPLE_SLOT_VOLUMES_NumVolumes_get(this.swigCPtr);
    }

    /* renamed from: a */
    public void mo7461a(C5945adV adv) {
        MilesBridgeJNI.EAX_SAMPLE_SLOT_VOLUMES_volumes_set(this.swigCPtr, C5945adV.m20900b(adv));
    }

    public C5945adV bIr() {
        long EAX_SAMPLE_SLOT_VOLUMES_volumes_get = MilesBridgeJNI.EAX_SAMPLE_SLOT_VOLUMES_volumes_get(this.swigCPtr);
        if (EAX_SAMPLE_SLOT_VOLUMES_volumes_get == 0) {
            return null;
        }
        return new C5945adV(EAX_SAMPLE_SLOT_VOLUMES_volumes_get, false);
    }
}
