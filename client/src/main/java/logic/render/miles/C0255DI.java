package logic.render.miles;

import utaikodom.render.miles.MilesBridgeJNI;

/* renamed from: a.DI */
/* compiled from: a */
public class C0255DI {
    public boolean swigCMemOwn;
    private long swigCPtr;

    public C0255DI(long j, boolean z) {
        this.swigCMemOwn = z;
        this.swigCPtr = j;
    }

    public C0255DI() {
        this(MilesBridgeJNI.new_EAX_ECHO(), true);
    }

    /* renamed from: a */
    public static long m1965a(C0255DI di) {
        if (di == null) {
            return 0;
        }
        return di.swigCPtr;
    }

    /* access modifiers changed from: protected */
    public void finalize() {
        delete();
    }

    public void delete() {
        if (this.swigCPtr != 0 && this.swigCMemOwn) {
            this.swigCMemOwn = false;
            MilesBridgeJNI.delete_EAX_ECHO(this.swigCPtr);
        }
        this.swigCPtr = 0;
    }

    /* renamed from: aD */
    public void mo1272aD(int i) {
        MilesBridgeJNI.EAX_ECHO_Effect_set(this.swigCPtr, i);
    }

    /* renamed from: ma */
    public int mo1286ma() {
        return MilesBridgeJNI.EAX_ECHO_Effect_get(this.swigCPtr);
    }

    public int getVolume() {
        return MilesBridgeJNI.EAX_ECHO_Volume_get(this.swigCPtr);
    }

    public void setVolume(int i) {
        MilesBridgeJNI.EAX_ECHO_Volume_set(this.swigCPtr, i);
    }

    /* renamed from: eX */
    public void mo1279eX(float f) {
        MilesBridgeJNI.EAX_ECHO_Delay_set(this.swigCPtr, f);
    }

    public float aHe() {
        return MilesBridgeJNI.EAX_ECHO_Delay_get(this.swigCPtr);
    }

    /* renamed from: eY */
    public void mo1280eY(float f) {
        MilesBridgeJNI.EAX_ECHO_LRDelay_set(this.swigCPtr, f);
    }

    public float aHf() {
        return MilesBridgeJNI.EAX_ECHO_LRDelay_get(this.swigCPtr);
    }

    /* renamed from: eZ */
    public void mo1281eZ(float f) {
        MilesBridgeJNI.EAX_ECHO_Damping_set(this.swigCPtr, f);
    }

    public float aHg() {
        return MilesBridgeJNI.EAX_ECHO_Damping_get(this.swigCPtr);
    }

    /* renamed from: fa */
    public void mo1282fa(float f) {
        MilesBridgeJNI.EAX_ECHO_Feedback_set(this.swigCPtr, f);
    }

    public float aHh() {
        return MilesBridgeJNI.EAX_ECHO_Feedback_get(this.swigCPtr);
    }

    /* renamed from: fb */
    public void mo1283fb(float f) {
        MilesBridgeJNI.EAX_ECHO_Spread_set(this.swigCPtr, f);
    }

    public float aHi() {
        return MilesBridgeJNI.EAX_ECHO_Spread_get(this.swigCPtr);
    }
}
