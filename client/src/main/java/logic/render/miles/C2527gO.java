package logic.render.miles;

import utaikodom.render.miles.MilesBridgeJNI;

/* renamed from: a.gO */
/* compiled from: a */
public class C2527gO {
    public boolean swigCMemOwn;
    private long swigCPtr;

    public C2527gO(long j, boolean z) {
        this.swigCMemOwn = z;
        this.swigCPtr = j;
    }

    public C2527gO() {
        this(MilesBridgeJNI.new_EAX_EQUALIZER(), true);
    }

    /* renamed from: a */
    public static long m31922a(C2527gO gOVar) {
        if (gOVar == null) {
            return 0;
        }
        return gOVar.swigCPtr;
    }

    /* access modifiers changed from: protected */
    public void finalize() {
        delete();
    }

    public void delete() {
        if (this.swigCPtr != 0 && this.swigCMemOwn) {
            this.swigCMemOwn = false;
            MilesBridgeJNI.delete_EAX_EQUALIZER(this.swigCPtr);
        }
        this.swigCPtr = 0;
    }

    /* renamed from: aD */
    public void mo18958aD(int i) {
        MilesBridgeJNI.EAX_EQUALIZER_Effect_set(this.swigCPtr, i);
    }

    /* renamed from: ma */
    public int mo18972ma() {
        return MilesBridgeJNI.EAX_EQUALIZER_Effect_get(this.swigCPtr);
    }

    public int getVolume() {
        return MilesBridgeJNI.EAX_EQUALIZER_Volume_get(this.swigCPtr);
    }

    public void setVolume(int i) {
        MilesBridgeJNI.EAX_EQUALIZER_Volume_set(this.swigCPtr, i);
    }

    /* renamed from: bl */
    public void mo18966bl(int i) {
        MilesBridgeJNI.EAX_EQUALIZER_LowGain_set(this.swigCPtr, i);
    }

    /* renamed from: wo */
    public int mo18974wo() {
        return MilesBridgeJNI.EAX_EQUALIZER_LowGain_get(this.swigCPtr);
    }

    /* renamed from: ae */
    public void mo18959ae(float f) {
        MilesBridgeJNI.EAX_EQUALIZER_LowCutOff_set(this.swigCPtr, f);
    }

    /* renamed from: wp */
    public float mo18975wp() {
        return MilesBridgeJNI.EAX_EQUALIZER_LowCutOff_get(this.swigCPtr);
    }

    /* renamed from: bm */
    public void mo18967bm(int i) {
        MilesBridgeJNI.EAX_EQUALIZER_Mid1Gain_set(this.swigCPtr, i);
    }

    /* renamed from: wq */
    public int mo18976wq() {
        return MilesBridgeJNI.EAX_EQUALIZER_Mid1Gain_get(this.swigCPtr);
    }

    /* renamed from: af */
    public void mo18960af(float f) {
        MilesBridgeJNI.EAX_EQUALIZER_Mid1Center_set(this.swigCPtr, f);
    }

    /* renamed from: wr */
    public float mo18977wr() {
        return MilesBridgeJNI.EAX_EQUALIZER_Mid1Center_get(this.swigCPtr);
    }

    /* renamed from: ag */
    public void mo18961ag(float f) {
        MilesBridgeJNI.EAX_EQUALIZER_Mid1Width_set(this.swigCPtr, f);
    }

    /* renamed from: ws */
    public float mo18978ws() {
        return MilesBridgeJNI.EAX_EQUALIZER_Mid1Width_get(this.swigCPtr);
    }

    /* renamed from: ah */
    public void mo18962ah(float f) {
        MilesBridgeJNI.EAX_EQUALIZER_Mid2Gain_set(this.swigCPtr, f);
    }

    /* renamed from: wt */
    public float mo18979wt() {
        return MilesBridgeJNI.EAX_EQUALIZER_Mid2Gain_get(this.swigCPtr);
    }

    /* renamed from: ai */
    public void mo18963ai(float f) {
        MilesBridgeJNI.EAX_EQUALIZER_Mid2Center_set(this.swigCPtr, f);
    }

    /* renamed from: wu */
    public float mo18980wu() {
        return MilesBridgeJNI.EAX_EQUALIZER_Mid2Center_get(this.swigCPtr);
    }

    /* renamed from: aj */
    public void mo18964aj(float f) {
        MilesBridgeJNI.EAX_EQUALIZER_Mid2Width_set(this.swigCPtr, f);
    }

    /* renamed from: wv */
    public float mo18981wv() {
        return MilesBridgeJNI.EAX_EQUALIZER_Mid2Width_get(this.swigCPtr);
    }

    /* renamed from: bn */
    public void mo18968bn(int i) {
        MilesBridgeJNI.EAX_EQUALIZER_HighGain_set(this.swigCPtr, i);
    }

    /* renamed from: ww */
    public int mo18982ww() {
        return MilesBridgeJNI.EAX_EQUALIZER_HighGain_get(this.swigCPtr);
    }

    /* renamed from: ak */
    public void mo18965ak(float f) {
        MilesBridgeJNI.EAX_EQUALIZER_HighCutOff_set(this.swigCPtr, f);
    }

    /* renamed from: wx */
    public float mo18983wx() {
        return MilesBridgeJNI.EAX_EQUALIZER_HighCutOff_get(this.swigCPtr);
    }
}
