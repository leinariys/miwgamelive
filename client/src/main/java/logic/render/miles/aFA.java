package logic.render.miles;

import utaikodom.render.miles.MilesBridgeJNI;

/* renamed from: a.aFA */
/* compiled from: a */
public class aFA {
    public boolean swigCMemOwn;
    private long swigCPtr;

    public aFA(long j, boolean z) {
        this.swigCMemOwn = z;
        this.swigCPtr = j;
    }

    public aFA() {
        this(MilesBridgeJNI.new_AUDIO_TYPE(), true);
    }

    /* renamed from: h */
    public static long m14479h(aFA afa) {
        if (afa == null) {
            return 0;
        }
        return afa.swigCPtr;
    }

    /* access modifiers changed from: protected */
    public void finalize() {
        delete();
    }

    public void delete() {
        if (this.swigCPtr != 0 && this.swigCMemOwn) {
            this.swigCMemOwn = false;
            MilesBridgeJNI.delete_AUDIO_TYPE(this.swigCPtr);
        }
        this.swigCPtr = 0;
    }

    /* renamed from: f */
    public void mo8744f(C5758aVq avq) {
        MilesBridgeJNI.AUDIO_TYPE_data_set(this.swigCPtr, C5758aVq.m19143h(avq));
    }

    public C5758aVq cYS() {
        long AUDIO_TYPE_data_get = MilesBridgeJNI.AUDIO_TYPE_data_get(this.swigCPtr);
        if (AUDIO_TYPE_data_get == 0) {
            return null;
        }
        return new C5758aVq(AUDIO_TYPE_data_get, false);
    }

    public int getSize() {
        return MilesBridgeJNI.AUDIO_TYPE_size_get(this.swigCPtr);
    }

    public void setSize(int i) {
        MilesBridgeJNI.AUDIO_TYPE_size_set(this.swigCPtr, i);
    }

    public int getType() {
        return MilesBridgeJNI.AUDIO_TYPE_type_get(this.swigCPtr);
    }

    public void setType(int i) {
        MilesBridgeJNI.AUDIO_TYPE_type_set(this.swigCPtr, i);
    }

    /* renamed from: h */
    public void mo8752h(aBO abo) {
        MilesBridgeJNI.AUDIO_TYPE_handle_set(this.swigCPtr, aBO.m12884g(abo));
    }

    public aBO cYT() {
        long AUDIO_TYPE_handle_get = MilesBridgeJNI.AUDIO_TYPE_handle_get(this.swigCPtr);
        if (AUDIO_TYPE_handle_get == 0) {
            return null;
        }
        return new aBO(AUDIO_TYPE_handle_get, false);
    }

    public int getStatus() {
        return MilesBridgeJNI.AUDIO_TYPE_status_get(this.swigCPtr);
    }

    public void setStatus(int i) {
        MilesBridgeJNI.AUDIO_TYPE_status_set(this.swigCPtr, i);
    }

    /* renamed from: a */
    public void mo8729a(aBO abo) {
        MilesBridgeJNI.AUDIO_TYPE_next_set(this.swigCPtr, aBO.m12884g(abo));
    }

    /* renamed from: Bc */
    public aBO mo8728Bc() {
        long AUDIO_TYPE_next_get = MilesBridgeJNI.AUDIO_TYPE_next_get(this.swigCPtr);
        if (AUDIO_TYPE_next_get == 0) {
            return null;
        }
        return new aBO(AUDIO_TYPE_next_get, false);
    }

    /* renamed from: ep */
    public void mo8743ep(int i) {
        MilesBridgeJNI.AUDIO_TYPE_speed_set(this.swigCPtr, i);
    }

    public int aaM() {
        return MilesBridgeJNI.AUDIO_TYPE_speed_get(this.swigCPtr);
    }

    public float getVolume() {
        return MilesBridgeJNI.AUDIO_TYPE_volume_get(this.swigCPtr);
    }

    public void setVolume(float f) {
        MilesBridgeJNI.AUDIO_TYPE_volume_set(this.swigCPtr, f);
    }

    /* renamed from: mz */
    public void mo8757mz(float f) {
        MilesBridgeJNI.AUDIO_TYPE_extravol_set(this.swigCPtr, f);
    }

    public float cYU() {
        return MilesBridgeJNI.AUDIO_TYPE_extravol_get(this.swigCPtr);
    }

    /* renamed from: mA */
    public void mo8755mA(float f) {
        MilesBridgeJNI.AUDIO_TYPE_dry_set(this.swigCPtr, f);
    }

    public float cYV() {
        return MilesBridgeJNI.AUDIO_TYPE_dry_get(this.swigCPtr);
    }

    /* renamed from: mB */
    public void mo8756mB(float f) {
        MilesBridgeJNI.AUDIO_TYPE_wet_set(this.swigCPtr, f);
    }

    public float cYW() {
        return MilesBridgeJNI.AUDIO_TYPE_wet_get(this.swigCPtr);
    }

    public float getCutoff() {
        return MilesBridgeJNI.AUDIO_TYPE_cutoff_get(this.swigCPtr);
    }

    public void setCutoff(float f) {
        MilesBridgeJNI.AUDIO_TYPE_cutoff_set(this.swigCPtr, f);
    }

    /* renamed from: d */
    public void mo8741d(C3336qQ qQVar) {
        MilesBridgeJNI.AUDIO_TYPE_dlsid_set(this.swigCPtr, C3336qQ.m37519a(qQVar));
    }

    public C3336qQ cYX() {
        long AUDIO_TYPE_dlsid_get = MilesBridgeJNI.AUDIO_TYPE_dlsid_get(this.swigCPtr);
        if (AUDIO_TYPE_dlsid_get == 0) {
            return null;
        }
        return new C3336qQ(AUDIO_TYPE_dlsid_get, false);
    }

    /* renamed from: i */
    public void mo8753i(aBO abo) {
        MilesBridgeJNI.AUDIO_TYPE_dlsmem_set(this.swigCPtr, aBO.m12884g(abo));
    }

    public aBO cYY() {
        long AUDIO_TYPE_dlsmem_get = MilesBridgeJNI.AUDIO_TYPE_dlsmem_get(this.swigCPtr);
        if (AUDIO_TYPE_dlsmem_get == 0) {
            return null;
        }
        return new aBO(AUDIO_TYPE_dlsmem_get, false);
    }

    /* renamed from: j */
    public void mo8754j(aBO abo) {
        MilesBridgeJNI.AUDIO_TYPE_dlsmemunc_set(this.swigCPtr, aBO.m12884g(abo));
    }

    public aBO cYZ() {
        long AUDIO_TYPE_dlsmemunc_get = MilesBridgeJNI.AUDIO_TYPE_dlsmemunc_get(this.swigCPtr);
        if (AUDIO_TYPE_dlsmemunc_get == 0) {
            return null;
        }
        return new aBO(AUDIO_TYPE_dlsmemunc_get, false);
    }

    public void setMilliseconds(int i) {
        MilesBridgeJNI.AUDIO_TYPE_milliseconds_set(this.swigCPtr, i);
    }

    public int cZa() {
        return MilesBridgeJNI.AUDIO_TYPE_milliseconds_get(this.swigCPtr);
    }

    public int getLength() {
        return MilesBridgeJNI.AUDIO_TYPE_length_get(this.swigCPtr);
    }

    public void setLength(int i) {
        MilesBridgeJNI.AUDIO_TYPE_length_set(this.swigCPtr, i);
    }

    /* renamed from: xT */
    public void mo8765xT(int i) {
        MilesBridgeJNI.AUDIO_TYPE_userdata_set(this.swigCPtr, i);
    }

    public int cZb() {
        return MilesBridgeJNI.AUDIO_TYPE_userdata_get(this.swigCPtr);
    }
}
