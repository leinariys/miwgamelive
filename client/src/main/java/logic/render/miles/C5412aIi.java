package logic.render.miles;

/* renamed from: a.aIi  reason: case insensitive filesystem */
/* compiled from: a */
public class C5412aIi {
    private long swigCPtr;

    public C5412aIi(long j, boolean z) {
        this.swigCPtr = j;
    }

    public C5412aIi() {
        this.swigCPtr = 0;
    }

    /* renamed from: b */
    public static long m15456b(C5412aIi aii) {
        if (aii == null) {
            return 0;
        }
        return aii.swigCPtr;
    }
}
