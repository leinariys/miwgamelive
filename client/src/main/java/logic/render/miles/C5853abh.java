package logic.render.miles;

import utaikodom.render.miles.MilesBridgeJNI;

/* renamed from: a.abh  reason: case insensitive filesystem */
/* compiled from: a */
public class C5853abh {
    public boolean swigCMemOwn;
    private long swigCPtr;

    public C5853abh(long j, boolean z) {
        this.swigCMemOwn = z;
        this.swigCPtr = j;
    }

    public C5853abh() {
        this(MilesBridgeJNI.new_MSS_RECEIVER_LIST(), true);
    }

    /* renamed from: a */
    public static long m20059a(C5853abh abh) {
        if (abh == null) {
            return 0;
        }
        return abh.swigCPtr;
    }

    /* access modifiers changed from: protected */
    public void finalize() {
        delete();
    }

    public void delete() {
        if (this.swigCPtr != 0 && this.swigCMemOwn) {
            this.swigCMemOwn = false;
            MilesBridgeJNI.delete_MSS_RECEIVER_LIST(this.swigCPtr);
        }
        this.swigCPtr = 0;
    }

    /* renamed from: e */
    public void mo12507e(aMJ amj) {
        MilesBridgeJNI.MSS_RECEIVER_LIST_direction_set(this.swigCPtr, aMJ.m16344m(amj));
    }

    public aMJ bNd() {
        long MSS_RECEIVER_LIST_direction_get = MilesBridgeJNI.MSS_RECEIVER_LIST_direction_get(this.swigCPtr);
        if (MSS_RECEIVER_LIST_direction_get == 0) {
            return null;
        }
        return new aMJ(MSS_RECEIVER_LIST_direction_get, false);
    }

    /* renamed from: M */
    public void mo12501M(C2480fe feVar) {
        MilesBridgeJNI.MSS_RECEIVER_LIST_speaker_index_set(this.swigCPtr, C2480fe.m31244a(feVar));
    }

    public C2480fe bNe() {
        long MSS_RECEIVER_LIST_speaker_index_get = MilesBridgeJNI.MSS_RECEIVER_LIST_speaker_index_get(this.swigCPtr);
        if (MSS_RECEIVER_LIST_speaker_index_get == 0) {
            return null;
        }
        return new C2480fe(MSS_RECEIVER_LIST_speaker_index_get, false);
    }

    /* renamed from: o */
    public void mo12509o(C2512gC gCVar) {
        MilesBridgeJNI.MSS_RECEIVER_LIST_speaker_level_set(this.swigCPtr, C2512gC.m31656a(gCVar));
    }

    public C2512gC bNf() {
        long MSS_RECEIVER_LIST_speaker_level_get = MilesBridgeJNI.MSS_RECEIVER_LIST_speaker_level_get(this.swigCPtr);
        if (MSS_RECEIVER_LIST_speaker_level_get == 0) {
            return null;
        }
        return new C2512gC(MSS_RECEIVER_LIST_speaker_level_get, false);
    }

    /* renamed from: pC */
    public void mo12510pC(int i) {
        MilesBridgeJNI.MSS_RECEIVER_LIST_n_speakers_affected_set(this.swigCPtr, i);
    }

    public int bNg() {
        return MilesBridgeJNI.MSS_RECEIVER_LIST_n_speakers_affected_get(this.swigCPtr);
    }
}
