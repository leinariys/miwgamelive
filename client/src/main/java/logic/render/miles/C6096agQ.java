package logic.render.miles;

import utaikodom.render.miles.MilesBridgeJNI;

/* renamed from: a.agQ  reason: case insensitive filesystem */
/* compiled from: a */
public class C6096agQ {
    public boolean swigCMemOwn;
    private long swigCPtr;

    public C6096agQ(long j, boolean z) {
        this.swigCMemOwn = z;
        this.swigCPtr = j;
    }

    public C6096agQ() {
        this(MilesBridgeJNI.new_LOWPASS_UPDATED_INFO(), true);
    }

    /* renamed from: a */
    public static long m22026a(C6096agQ agq) {
        if (agq == null) {
            return 0;
        }
        return agq.swigCPtr;
    }

    /* access modifiers changed from: protected */
    public void finalize() {
        delete();
    }

    public void delete() {
        if (this.swigCPtr != 0 && this.swigCMemOwn) {
            this.swigCMemOwn = false;
            MilesBridgeJNI.delete_LOWPASS_UPDATED_INFO(this.swigCPtr);
        }
        this.swigCPtr = 0;
    }

    /* renamed from: qF */
    public void mo13389qF(int i) {
        MilesBridgeJNI.LOWPASS_UPDATED_INFO_XL0_set(this.swigCPtr, i);
    }

    public int bXs() {
        return MilesBridgeJNI.LOWPASS_UPDATED_INFO_XL0_get(this.swigCPtr);
    }

    /* renamed from: qG */
    public void mo13390qG(int i) {
        MilesBridgeJNI.LOWPASS_UPDATED_INFO_XL1_set(this.swigCPtr, i);
    }

    public int bXt() {
        return MilesBridgeJNI.LOWPASS_UPDATED_INFO_XL1_get(this.swigCPtr);
    }

    /* renamed from: qH */
    public void mo13391qH(int i) {
        MilesBridgeJNI.LOWPASS_UPDATED_INFO_YL0_set(this.swigCPtr, i);
    }

    public int bXu() {
        return MilesBridgeJNI.LOWPASS_UPDATED_INFO_YL0_get(this.swigCPtr);
    }

    /* renamed from: qI */
    public void mo13392qI(int i) {
        MilesBridgeJNI.LOWPASS_UPDATED_INFO_YL1_set(this.swigCPtr, i);
    }

    public int bXv() {
        return MilesBridgeJNI.LOWPASS_UPDATED_INFO_YL1_get(this.swigCPtr);
    }

    /* renamed from: qJ */
    public void mo13393qJ(int i) {
        MilesBridgeJNI.LOWPASS_UPDATED_INFO_XR0_set(this.swigCPtr, i);
    }

    public int bXw() {
        return MilesBridgeJNI.LOWPASS_UPDATED_INFO_XR0_get(this.swigCPtr);
    }

    /* renamed from: qK */
    public void mo13394qK(int i) {
        MilesBridgeJNI.LOWPASS_UPDATED_INFO_XR1_set(this.swigCPtr, i);
    }

    public int bXx() {
        return MilesBridgeJNI.LOWPASS_UPDATED_INFO_XR1_get(this.swigCPtr);
    }

    /* renamed from: qL */
    public void mo13395qL(int i) {
        MilesBridgeJNI.LOWPASS_UPDATED_INFO_YR0_set(this.swigCPtr, i);
    }

    public int bXy() {
        return MilesBridgeJNI.LOWPASS_UPDATED_INFO_YR0_get(this.swigCPtr);
    }

    /* renamed from: qM */
    public void mo13396qM(int i) {
        MilesBridgeJNI.LOWPASS_UPDATED_INFO_YR1_set(this.swigCPtr, i);
    }

    public int bXz() {
        return MilesBridgeJNI.LOWPASS_UPDATED_INFO_YR1_get(this.swigCPtr);
    }
}
