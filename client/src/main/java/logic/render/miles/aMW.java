package logic.render.miles;

import utaikodom.render.miles.MilesBridgeJNI;

/* renamed from: a.aMW */
/* compiled from: a */
public class aMW {
    public boolean swigCMemOwn;
    private long swigCPtr;

    public aMW(long j, boolean z) {
        this.swigCMemOwn = z;
        this.swigCPtr = j;
    }

    public aMW() {
        this(MilesBridgeJNI.new_MWAVEFORMATEX(), true);
    }

    /* renamed from: b */
    public static long m16390b(aMW amw) {
        if (amw == null) {
            return 0;
        }
        return amw.swigCPtr;
    }

    /* access modifiers changed from: protected */
    public void finalize() {
        delete();
    }

    public void delete() {
        if (this.swigCPtr != 0 && this.swigCMemOwn) {
            this.swigCMemOwn = false;
            MilesBridgeJNI.delete_MWAVEFORMATEX(this.swigCPtr);
        }
        this.swigCPtr = 0;
    }

    /* renamed from: lC */
    public void mo10062lC(int i) {
        MilesBridgeJNI.MWAVEFORMATEX_wFormatTag_set(this.swigCPtr, i);
    }

    public int bfO() {
        return MilesBridgeJNI.MWAVEFORMATEX_wFormatTag_get(this.swigCPtr);
    }

    /* renamed from: lD */
    public void mo10063lD(int i) {
        MilesBridgeJNI.MWAVEFORMATEX_nChannels_set(this.swigCPtr, i);
    }

    public int bfP() {
        return MilesBridgeJNI.MWAVEFORMATEX_nChannels_get(this.swigCPtr);
    }

    /* renamed from: eX */
    public void mo10059eX(long j) {
        MilesBridgeJNI.MWAVEFORMATEX_nSamplesPerSec_set(this.swigCPtr, j);
    }

    public long bfQ() {
        return MilesBridgeJNI.MWAVEFORMATEX_nSamplesPerSec_get(this.swigCPtr);
    }

    /* renamed from: eY */
    public void mo10060eY(long j) {
        MilesBridgeJNI.MWAVEFORMATEX_nAvgBytesPerSec_set(this.swigCPtr, j);
    }

    public long bfR() {
        return MilesBridgeJNI.MWAVEFORMATEX_nAvgBytesPerSec_get(this.swigCPtr);
    }

    /* renamed from: lE */
    public void mo10064lE(int i) {
        MilesBridgeJNI.MWAVEFORMATEX_nBlockAlign_set(this.swigCPtr, i);
    }

    public int bfS() {
        return MilesBridgeJNI.MWAVEFORMATEX_nBlockAlign_get(this.swigCPtr);
    }

    /* renamed from: rJ */
    public void mo10065rJ(int i) {
        MilesBridgeJNI.MWAVEFORMATEX_wBitsPerSample_set(this.swigCPtr, i);
    }

    public int cfG() {
        return MilesBridgeJNI.MWAVEFORMATEX_wBitsPerSample_get(this.swigCPtr);
    }

    /* renamed from: yO */
    public void mo10066yO(int i) {
        MilesBridgeJNI.MWAVEFORMATEX_cbSize_set(this.swigCPtr, i);
    }

    public int djI() {
        return MilesBridgeJNI.MWAVEFORMATEX_cbSize_get(this.swigCPtr);
    }
}
