package logic.render.miles;

import utaikodom.render.miles.MilesBridgeJNI;

/* renamed from: a.aiw  reason: case insensitive filesystem */
/* compiled from: a */
public class C6232aiw {
    public boolean swigCMemOwn;
    private long swigCPtr;

    public C6232aiw(long j, boolean z) {
        this.swigCMemOwn = z;
        this.swigCPtr = j;
    }

    public C6232aiw() {
        this(MilesBridgeJNI.new_D3DSTATE(), true);
    }

    /* renamed from: a */
    public static long m22703a(C6232aiw aiw) {
        if (aiw == null) {
            return 0;
        }
        return aiw.swigCPtr;
    }

    /* access modifiers changed from: protected */
    public void finalize() {
        delete();
    }

    public void delete() {
        if (this.swigCPtr != 0 && this.swigCMemOwn) {
            this.swigCMemOwn = false;
            MilesBridgeJNI.delete_D3DSTATE(this.swigCPtr);
        }
        this.swigCPtr = 0;
    }

    /* renamed from: rf */
    public void mo13869rf(int i) {
        MilesBridgeJNI.D3DSTATE_mute_at_max_set(this.swigCPtr, i);
    }

    public int cbX() {
        return MilesBridgeJNI.D3DSTATE_mute_at_max_get(this.swigCPtr);
    }

    /* renamed from: f */
    public void mo13855f(aMJ amj) {
        MilesBridgeJNI.D3DSTATE_listen_position_set(this.swigCPtr, aMJ.m16344m(amj));
    }

    public aMJ cbY() {
        long D3DSTATE_listen_position_get = MilesBridgeJNI.D3DSTATE_listen_position_get(this.swigCPtr);
        if (D3DSTATE_listen_position_get == 0) {
            return null;
        }
        return new aMJ(D3DSTATE_listen_position_get, false);
    }

    /* renamed from: g */
    public void mo13857g(aMJ amj) {
        MilesBridgeJNI.D3DSTATE_listen_face_set(this.swigCPtr, aMJ.m16344m(amj));
    }

    public aMJ cbZ() {
        long D3DSTATE_listen_face_get = MilesBridgeJNI.D3DSTATE_listen_face_get(this.swigCPtr);
        if (D3DSTATE_listen_face_get == 0) {
            return null;
        }
        return new aMJ(D3DSTATE_listen_face_get, false);
    }

    /* renamed from: h */
    public void mo13858h(aMJ amj) {
        MilesBridgeJNI.D3DSTATE_listen_up_set(this.swigCPtr, aMJ.m16344m(amj));
    }

    public aMJ cca() {
        long D3DSTATE_listen_up_get = MilesBridgeJNI.D3DSTATE_listen_up_get(this.swigCPtr);
        if (D3DSTATE_listen_up_get == 0) {
            return null;
        }
        return new aMJ(D3DSTATE_listen_up_get, false);
    }

    /* renamed from: i */
    public void mo13859i(aMJ amj) {
        MilesBridgeJNI.D3DSTATE_listen_cross_set(this.swigCPtr, aMJ.m16344m(amj));
    }

    public aMJ ccb() {
        long D3DSTATE_listen_cross_get = MilesBridgeJNI.D3DSTATE_listen_cross_get(this.swigCPtr);
        if (D3DSTATE_listen_cross_get == 0) {
            return null;
        }
        return new aMJ(D3DSTATE_listen_cross_get, false);
    }

    /* renamed from: j */
    public void mo13860j(aMJ amj) {
        MilesBridgeJNI.D3DSTATE_listen_velocity_set(this.swigCPtr, aMJ.m16344m(amj));
    }

    public aMJ ccc() {
        long D3DSTATE_listen_velocity_get = MilesBridgeJNI.D3DSTATE_listen_velocity_get(this.swigCPtr);
        if (D3DSTATE_listen_velocity_get == 0) {
            return null;
        }
        return new aMJ(D3DSTATE_listen_velocity_get, false);
    }

    /* renamed from: jz */
    public void mo13864jz(float f) {
        MilesBridgeJNI.D3DSTATE_rolloff_factor_set(this.swigCPtr, f);
    }

    public float ccd() {
        return MilesBridgeJNI.D3DSTATE_rolloff_factor_get(this.swigCPtr);
    }

    /* renamed from: jA */
    public void mo13861jA(float f) {
        MilesBridgeJNI.D3DSTATE_doppler_factor_set(this.swigCPtr, f);
    }

    public float cce() {
        return MilesBridgeJNI.D3DSTATE_doppler_factor_get(this.swigCPtr);
    }

    /* renamed from: jB */
    public void mo13862jB(float f) {
        MilesBridgeJNI.D3DSTATE_distance_factor_set(this.swigCPtr, f);
    }

    public float ccf() {
        return MilesBridgeJNI.D3DSTATE_distance_factor_get(this.swigCPtr);
    }

    /* renamed from: jC */
    public void mo13863jC(float f) {
        MilesBridgeJNI.D3DSTATE_falloff_power_set(this.swigCPtr, f);
    }

    public float ccg() {
        return MilesBridgeJNI.D3DSTATE_falloff_power_get(this.swigCPtr);
    }

    /* renamed from: N */
    public void mo13831N(C2480fe feVar) {
        MilesBridgeJNI.D3DSTATE_ambient_channels_set(this.swigCPtr, C2480fe.m31244a(feVar));
    }

    public C2480fe cch() {
        long D3DSTATE_ambient_channels_get = MilesBridgeJNI.D3DSTATE_ambient_channels_get(this.swigCPtr);
        if (D3DSTATE_ambient_channels_get == 0) {
            return null;
        }
        return new C2480fe(D3DSTATE_ambient_channels_get, false);
    }

    /* renamed from: rg */
    public void mo13870rg(int i) {
        MilesBridgeJNI.D3DSTATE_n_ambient_channels_set(this.swigCPtr, i);
    }

    public int cci() {
        return MilesBridgeJNI.D3DSTATE_n_ambient_channels_get(this.swigCPtr);
    }

    /* renamed from: O */
    public void mo13832O(C2480fe feVar) {
        MilesBridgeJNI.D3DSTATE_directional_channels_set(this.swigCPtr, C2480fe.m31244a(feVar));
    }

    public C2480fe ccj() {
        long D3DSTATE_directional_channels_get = MilesBridgeJNI.D3DSTATE_directional_channels_get(this.swigCPtr);
        if (D3DSTATE_directional_channels_get == 0) {
            return null;
        }
        return new C2480fe(D3DSTATE_directional_channels_get, false);
    }

    /* renamed from: k */
    public void mo13865k(aMJ amj) {
        MilesBridgeJNI.D3DSTATE_listener_to_speaker_set(this.swigCPtr, aMJ.m16344m(amj));
    }

    public aMJ cck() {
        long D3DSTATE_listener_to_speaker_get = MilesBridgeJNI.D3DSTATE_listener_to_speaker_get(this.swigCPtr);
        if (D3DSTATE_listener_to_speaker_get == 0) {
            return null;
        }
        return new aMJ(D3DSTATE_listener_to_speaker_get, false);
    }

    /* renamed from: rh */
    public void mo13871rh(int i) {
        MilesBridgeJNI.D3DSTATE_n_directional_channels_set(this.swigCPtr, i);
    }

    public int ccl() {
        return MilesBridgeJNI.D3DSTATE_n_directional_channels_get(this.swigCPtr);
    }

    /* renamed from: b */
    public void mo13833b(C5853abh abh) {
        MilesBridgeJNI.D3DSTATE_receiver_specifications_set(this.swigCPtr, C5853abh.m20059a(abh));
    }

    public C5853abh ccm() {
        long D3DSTATE_receiver_specifications_get = MilesBridgeJNI.D3DSTATE_receiver_specifications_get(this.swigCPtr);
        if (D3DSTATE_receiver_specifications_get == 0) {
            return null;
        }
        return new C5853abh(D3DSTATE_receiver_specifications_get, false);
    }

    /* renamed from: ri */
    public void mo13872ri(int i) {
        MilesBridgeJNI.D3DSTATE_n_receiver_specs_set(this.swigCPtr, i);
    }

    public int ccn() {
        return MilesBridgeJNI.D3DSTATE_n_receiver_specs_get(this.swigCPtr);
    }

    /* renamed from: l */
    public void mo13866l(aMJ amj) {
        MilesBridgeJNI.D3DSTATE_speaker_positions_set(this.swigCPtr, aMJ.m16344m(amj));
    }

    public aMJ cco() {
        long D3DSTATE_speaker_positions_get = MilesBridgeJNI.D3DSTATE_speaker_positions_get(this.swigCPtr);
        if (D3DSTATE_speaker_positions_get == 0) {
            return null;
        }
        return new aMJ(D3DSTATE_speaker_positions_get, false);
    }

    /* renamed from: p */
    public void mo13867p(C2512gC gCVar) {
        MilesBridgeJNI.D3DSTATE_speaker_wet_reverb_response_set(this.swigCPtr, C2512gC.m31656a(gCVar));
    }

    public C2512gC ccp() {
        long D3DSTATE_speaker_wet_reverb_response_get = MilesBridgeJNI.D3DSTATE_speaker_wet_reverb_response_get(this.swigCPtr);
        if (D3DSTATE_speaker_wet_reverb_response_get == 0) {
            return null;
        }
        return new C2512gC(D3DSTATE_speaker_wet_reverb_response_get, false);
    }

    /* renamed from: q */
    public void mo13868q(C2512gC gCVar) {
        MilesBridgeJNI.D3DSTATE_speaker_dry_reverb_response_set(this.swigCPtr, C2512gC.m31656a(gCVar));
    }

    public C2512gC ccq() {
        long D3DSTATE_speaker_dry_reverb_response_get = MilesBridgeJNI.D3DSTATE_speaker_dry_reverb_response_get(this.swigCPtr);
        if (D3DSTATE_speaker_dry_reverb_response_get == 0) {
            return null;
        }
        return new C2512gC(D3DSTATE_speaker_dry_reverb_response_get, false);
    }
}
