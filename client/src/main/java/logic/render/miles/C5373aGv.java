package logic.render.miles;

import utaikodom.render.miles.MilesBridgeJNI;

/* renamed from: a.aGv  reason: case insensitive filesystem */
/* compiled from: a */
public class C5373aGv {
    public boolean swigCMemOwn;
    private long swigCPtr;

    public C5373aGv(long j, boolean z) {
        this.swigCMemOwn = z;
        this.swigCPtr = j;
    }

    public C5373aGv() {
        this(MilesBridgeJNI.new_RIB_INTERFACE_ENTRY(), true);
    }

    /* renamed from: a */
    public static long m15023a(C5373aGv agv) {
        if (agv == null) {
            return 0;
        }
        return agv.swigCPtr;
    }

    /* access modifiers changed from: protected */
    public void finalize() {
        delete();
    }

    public void delete() {
        if (this.swigCPtr != 0 && this.swigCMemOwn) {
            this.swigCMemOwn = false;
            MilesBridgeJNI.delete_RIB_INTERFACE_ENTRY(this.swigCPtr);
        }
        this.swigCPtr = 0;
    }

    /* renamed from: a */
    public void mo9128a(C3536sH sHVar) {
        MilesBridgeJNI.RIB_INTERFACE_ENTRY_type_set(this.swigCPtr, sHVar.swigValue());
    }

    public C3536sH daj() {
        return C3536sH.m38709eB(MilesBridgeJNI.RIB_INTERFACE_ENTRY_type_get(this.swigCPtr));
    }

    /* renamed from: mx */
    public void mo9136mx(String str) {
        MilesBridgeJNI.RIB_INTERFACE_ENTRY_entry_name_set(this.swigCPtr, str);
    }

    public String dak() {
        return MilesBridgeJNI.RIB_INTERFACE_ENTRY_entry_name_get(this.swigCPtr);
    }

    /* renamed from: kP */
    public void mo9135kP(long j) {
        MilesBridgeJNI.RIB_INTERFACE_ENTRY_token_set(this.swigCPtr, j);
    }

    public long getToken() {
        return MilesBridgeJNI.RIB_INTERFACE_ENTRY_token_get(this.swigCPtr);
    }

    /* renamed from: a */
    public void mo9127a(aHB ahb) {
        MilesBridgeJNI.RIB_INTERFACE_ENTRY_subtype_set(this.swigCPtr, ahb.swigValue());
    }

    public aHB dal() {
        return aHB.m15117yk(MilesBridgeJNI.RIB_INTERFACE_ENTRY_subtype_get(this.swigCPtr));
    }
}
