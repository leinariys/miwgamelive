package logic.render.miles;

import utaikodom.render.miles.MilesBridgeJNI;

/* renamed from: a.adV  reason: case insensitive filesystem */
/* compiled from: a */
public class C5945adV {
    public boolean swigCMemOwn;
    private long swigCPtr;

    public C5945adV(long j, boolean z) {
        this.swigCMemOwn = z;
        this.swigCPtr = j;
    }

    public C5945adV() {
        this(MilesBridgeJNI.new_EAX_SAMPLE_SLOT_VOLUME(), true);
    }

    /* renamed from: b */
    public static long m20900b(C5945adV adv) {
        if (adv == null) {
            return 0;
        }
        return adv.swigCPtr;
    }

    /* access modifiers changed from: protected */
    public void finalize() {
        delete();
    }

    public void delete() {
        if (this.swigCPtr != 0 && this.swigCMemOwn) {
            this.swigCMemOwn = false;
            MilesBridgeJNI.delete_EAX_SAMPLE_SLOT_VOLUME(this.swigCPtr);
        }
        this.swigCPtr = 0;
    }

    /* renamed from: ee */
    public void mo12872ee(int i) {
        MilesBridgeJNI.EAX_SAMPLE_SLOT_VOLUME_Slot_set(this.swigCPtr, i);
    }

    /* renamed from: Xx */
    public int mo12864Xx() {
        return MilesBridgeJNI.EAX_SAMPLE_SLOT_VOLUME_Slot_get(this.swigCPtr);
    }

    /* renamed from: qd */
    public void mo12877qd(int i) {
        MilesBridgeJNI.EAX_SAMPLE_SLOT_VOLUME_Send_set(this.swigCPtr, i);
    }

    public int bTj() {
        return MilesBridgeJNI.EAX_SAMPLE_SLOT_VOLUME_Send_get(this.swigCPtr);
    }

    /* renamed from: qe */
    public void mo12878qe(int i) {
        MilesBridgeJNI.EAX_SAMPLE_SLOT_VOLUME_SendHF_set(this.swigCPtr, i);
    }

    public int bTk() {
        return MilesBridgeJNI.EAX_SAMPLE_SLOT_VOLUME_SendHF_get(this.swigCPtr);
    }

    /* renamed from: qf */
    public void mo12879qf(int i) {
        MilesBridgeJNI.EAX_SAMPLE_SLOT_VOLUME_Occlusion_set(this.swigCPtr, i);
    }

    public int bTl() {
        return MilesBridgeJNI.EAX_SAMPLE_SLOT_VOLUME_Occlusion_get(this.swigCPtr);
    }

    /* renamed from: jh */
    public void mo12874jh(float f) {
        MilesBridgeJNI.EAX_SAMPLE_SLOT_VOLUME_OcclusionLFRatio_set(this.swigCPtr, f);
    }

    public float bTm() {
        return MilesBridgeJNI.EAX_SAMPLE_SLOT_VOLUME_OcclusionLFRatio_get(this.swigCPtr);
    }

    /* renamed from: ji */
    public void mo12875ji(float f) {
        MilesBridgeJNI.EAX_SAMPLE_SLOT_VOLUME_OcclusionRoomRatio_set(this.swigCPtr, f);
    }

    public float bTn() {
        return MilesBridgeJNI.EAX_SAMPLE_SLOT_VOLUME_OcclusionRoomRatio_get(this.swigCPtr);
    }

    /* renamed from: jj */
    public void mo12876jj(float f) {
        MilesBridgeJNI.EAX_SAMPLE_SLOT_VOLUME_OcclusionDirectRatio_set(this.swigCPtr, f);
    }

    public float bTo() {
        return MilesBridgeJNI.EAX_SAMPLE_SLOT_VOLUME_OcclusionDirectRatio_get(this.swigCPtr);
    }
}
