package logic.render.miles;

import utaikodom.render.miles.MilesBridgeJNI;

/* renamed from: a.Lm */
/* compiled from: a */
public class C0815Lm {
    public boolean swigCMemOwn;
    private long swigCPtr;

    public C0815Lm(long j, boolean z) {
        this.swigCMemOwn = z;
        this.swigCPtr = j;
    }

    public C0815Lm() {
        this(MilesBridgeJNI.new_SPINFO_TYPE(), true);
    }

    /* renamed from: a */
    public static long m6858a(C0815Lm lm) {
        if (lm == null) {
            return 0;
        }
        return lm.swigCPtr;
    }

    /* access modifiers changed from: protected */
    public void finalize() {
        delete();
    }

    public void delete() {
        if (this.swigCPtr != 0 && this.swigCMemOwn) {
            this.swigCMemOwn = false;
            MilesBridgeJNI.delete_SPINFO_TYPE(this.swigCPtr);
        }
        this.swigCPtr = 0;
    }

    /* renamed from: a */
    public void mo3822a(C0270DT dt) {
        MilesBridgeJNI.SPINFO_TYPE_ASI_set(this.swigCPtr, C0270DT.m2424b(dt));
    }

    /* renamed from: Aj */
    public C0270DT mo3821Aj() {
        long SPINFO_TYPE_ASI_get = MilesBridgeJNI.SPINFO_TYPE_ASI_get(this.swigCPtr);
        if (SPINFO_TYPE_ASI_get == 0) {
            return null;
        }
        return new C0270DT(SPINFO_TYPE_ASI_get, false);
    }

    /* renamed from: a */
    public void mo3823a(C6532aok aok) {
        MilesBridgeJNI.SPINFO_TYPE_MSS_mixer_merge_set(this.swigCPtr, C6532aok.m24679b(aok));
    }

    public C6532aok bfd() {
        long SPINFO_TYPE_MSS_mixer_merge_get = MilesBridgeJNI.SPINFO_TYPE_MSS_mixer_merge_get(this.swigCPtr);
        if (SPINFO_TYPE_MSS_mixer_merge_get == 0) {
            return null;
        }
        return new C6532aok(SPINFO_TYPE_MSS_mixer_merge_get, false);
    }

    /* renamed from: a */
    public void mo3824a(azJ azj) {
        MilesBridgeJNI.SPINFO_TYPE_FLT_set(this.swigCPtr, azJ.m27599b(azj));
    }

    public azJ bfe() {
        long SPINFO_TYPE_FLT_get = MilesBridgeJNI.SPINFO_TYPE_FLT_get(this.swigCPtr);
        if (SPINFO_TYPE_FLT_get == 0) {
            return null;
        }
        return new azJ(SPINFO_TYPE_FLT_get, false);
    }
}
