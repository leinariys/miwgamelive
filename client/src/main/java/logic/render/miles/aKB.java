package logic.render.miles;

import utaikodom.render.miles.MilesBridgeJNI;

/* renamed from: a.aKB */
/* compiled from: a */
public class aKB {
    public boolean swigCMemOwn;
    private long swigCPtr;

    public aKB(long j, boolean z) {
        this.swigCMemOwn = z;
        this.swigCPtr = j;
    }

    public aKB() {
        this(MilesBridgeJNI.new_ADPCMDATA(), true);
    }

    /* renamed from: c */
    public static long m15913c(aKB akb) {
        if (akb == null) {
            return 0;
        }
        return akb.swigCPtr;
    }

    /* access modifiers changed from: protected */
    public void finalize() {
        delete();
    }

    public void delete() {
        if (this.swigCPtr != 0 && this.swigCMemOwn) {
            this.swigCMemOwn = false;
            MilesBridgeJNI.delete_ADPCMDATA(this.swigCPtr);
        }
        this.swigCPtr = 0;
    }

    /* renamed from: br */
    public void mo9639br(long j) {
        MilesBridgeJNI.ADPCMDATA_blocksize_set(this.swigCPtr, j);
    }

    /* renamed from: AM */
    public long mo9638AM() {
        return MilesBridgeJNI.ADPCMDATA_blocksize_get(this.swigCPtr);
    }

    /* renamed from: kY */
    public void mo9653kY(long j) {
        MilesBridgeJNI.ADPCMDATA_extrasamples_set(this.swigCPtr, j);
    }

    public long dgi() {
        return MilesBridgeJNI.ADPCMDATA_extrasamples_get(this.swigCPtr);
    }

    /* renamed from: kZ */
    public void mo9654kZ(long j) {
        MilesBridgeJNI.ADPCMDATA_blockleft_set(this.swigCPtr, j);
    }

    public long dgj() {
        return MilesBridgeJNI.ADPCMDATA_blockleft_get(this.swigCPtr);
    }

    /* renamed from: la */
    public void mo9655la(long j) {
        MilesBridgeJNI.ADPCMDATA_step_set(this.swigCPtr, j);
    }

    public long dgk() {
        return MilesBridgeJNI.ADPCMDATA_step_get(this.swigCPtr);
    }

    /* renamed from: lb */
    public void mo9656lb(long j) {
        MilesBridgeJNI.ADPCMDATA_savesrc_set(this.swigCPtr, j);
    }

    public long dgl() {
        return MilesBridgeJNI.ADPCMDATA_savesrc_get(this.swigCPtr);
    }

    /* renamed from: lc */
    public void mo9657lc(long j) {
        MilesBridgeJNI.ADPCMDATA_sample_set(this.swigCPtr, j);
    }

    public long dgm() {
        return MilesBridgeJNI.ADPCMDATA_sample_get(this.swigCPtr);
    }

    /* renamed from: ld */
    public void mo9658ld(long j) {
        MilesBridgeJNI.ADPCMDATA_destend_set(this.swigCPtr, j);
    }

    public long dgn() {
        return MilesBridgeJNI.ADPCMDATA_destend_get(this.swigCPtr);
    }

    /* renamed from: le */
    public void mo9659le(long j) {
        MilesBridgeJNI.ADPCMDATA_srcend_set(this.swigCPtr, j);
    }

    public long dgo() {
        return MilesBridgeJNI.ADPCMDATA_srcend_get(this.swigCPtr);
    }

    /* renamed from: lf */
    public void mo9660lf(long j) {
        MilesBridgeJNI.ADPCMDATA_samplesL_set(this.swigCPtr, j);
    }

    public long dgp() {
        return MilesBridgeJNI.ADPCMDATA_samplesL_get(this.swigCPtr);
    }

    /* renamed from: lg */
    public void mo9661lg(long j) {
        MilesBridgeJNI.ADPCMDATA_samplesR_set(this.swigCPtr, j);
    }

    public long dgq() {
        return MilesBridgeJNI.ADPCMDATA_samplesR_get(this.swigCPtr);
    }

    /* renamed from: c */
    public void mo9640c(C2434fQ fQVar) {
        MilesBridgeJNI.ADPCMDATA_moresamples_set(this.swigCPtr, C2434fQ.m31038a(fQVar));
    }

    public C2434fQ dgr() {
        long ADPCMDATA_moresamples_get = MilesBridgeJNI.ADPCMDATA_moresamples_get(this.swigCPtr);
        if (ADPCMDATA_moresamples_get == 0) {
            return null;
        }
        return new C2434fQ(ADPCMDATA_moresamples_get, false);
    }
}
