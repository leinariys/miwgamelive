package logic.render.miles;

/* renamed from: a.aUo  reason: case insensitive filesystem */
/* compiled from: a */
public class C5730aUo {
    private long swigCPtr;

    public C5730aUo(long j, boolean z) {
        this.swigCPtr = j;
    }

    public C5730aUo() {
        this.swigCPtr = 0;
    }

    /* renamed from: d */
    public static long m18736d(C5730aUo auo) {
        if (auo == null) {
            return 0;
        }
        return auo.swigCPtr;
    }
}
