package logic.render.miles;

/* renamed from: a.abI  reason: case insensitive filesystem */
/* compiled from: a */
public class C5828abI {
    private long swigCPtr;

    public C5828abI(long j, boolean z) {
        this.swigCPtr = j;
    }

    public C5828abI() {
        this.swigCPtr = 0;
    }

    /* renamed from: a */
    public static long m19941a(C5828abI abi) {
        if (abi == null) {
            return 0;
        }
        return abi.swigCPtr;
    }
}
