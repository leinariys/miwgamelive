package logic.render.miles;

import utaikodom.render.miles.MilesBridgeJNI;

/* renamed from: a.FQ */
/* compiled from: a */
public class C0391FQ {
    public boolean swigCMemOwn;
    private long swigCPtr;

    public C0391FQ(long j, boolean z) {
        this.swigCMemOwn = z;
        this.swigCPtr = j;
    }

    public C0391FQ() {
        this(MilesBridgeJNI.new_RBRN_entry(), true);
    }

    /* renamed from: a */
    public static long m3188a(C0391FQ fq) {
        if (fq == null) {
            return 0;
        }
        return fq.swigCPtr;
    }

    /* access modifiers changed from: protected */
    public void finalize() {
        delete();
    }

    public void delete() {
        if (this.swigCPtr != 0 && this.swigCMemOwn) {
            this.swigCMemOwn = false;
            MilesBridgeJNI.delete_RBRN_entry(this.swigCPtr);
        }
        this.swigCPtr = 0;
    }

    /* renamed from: j */
    public void mo2138j(short s) {
        MilesBridgeJNI.RBRN_entry_bnum_set(this.swigCPtr, s);
    }

    public short aQi() {
        return MilesBridgeJNI.RBRN_entry_bnum_get(this.swigCPtr);
    }

    public long getOffset() {
        return MilesBridgeJNI.RBRN_entry_offset_get(this.swigCPtr);
    }

    public void setOffset(long j) {
        MilesBridgeJNI.RBRN_entry_offset_set(this.swigCPtr, j);
    }
}
