package logic.render.miles;

import utaikodom.render.miles.MilesBridgeJNI;

/* renamed from: a.Xj */
/* compiled from: a */
public class C1620Xj {
    public boolean swigCMemOwn;
    private long swigCPtr;

    public C1620Xj(long j, boolean z) {
        this.swigCMemOwn = z;
        this.swigCPtr = j;
    }

    public C1620Xj() {
        this(MilesBridgeJNI.new_MWAVEHDR(), true);
    }

    /* renamed from: a */
    public static long m11572a(C1620Xj xj) {
        if (xj == null) {
            return 0;
        }
        return xj.swigCPtr;
    }

    /* access modifiers changed from: protected */
    public void finalize() {
        delete();
    }

    public void delete() {
        if (this.swigCPtr != 0 && this.swigCMemOwn) {
            this.swigCMemOwn = false;
            MilesBridgeJNI.delete_MWAVEHDR(this.swigCPtr);
        }
        this.swigCPtr = 0;
    }

    /* renamed from: hz */
    public void mo6798hz(String str) {
        MilesBridgeJNI.MWAVEHDR_lpData_set(this.swigCPtr, str);
    }

    public String bFt() {
        return MilesBridgeJNI.MWAVEHDR_lpData_get(this.swigCPtr);
    }

    /* renamed from: fX */
    public void mo6791fX(long j) {
        MilesBridgeJNI.MWAVEHDR_dwBufferLength_set(this.swigCPtr, j);
    }

    public long bFu() {
        return MilesBridgeJNI.MWAVEHDR_dwBufferLength_get(this.swigCPtr);
    }

    /* renamed from: fY */
    public void mo6792fY(long j) {
        MilesBridgeJNI.MWAVEHDR_dwBytesRecorded_set(this.swigCPtr, j);
    }

    public long bFv() {
        return MilesBridgeJNI.MWAVEHDR_dwBytesRecorded_get(this.swigCPtr);
    }

    /* renamed from: fZ */
    public void mo6793fZ(long j) {
        MilesBridgeJNI.MWAVEHDR_dwUser_set(this.swigCPtr, j);
    }

    public long bFw() {
        return MilesBridgeJNI.MWAVEHDR_dwUser_get(this.swigCPtr);
    }

    /* renamed from: ga */
    public void mo6795ga(long j) {
        MilesBridgeJNI.MWAVEHDR_dwFlags_set(this.swigCPtr, j);
    }

    public long bFx() {
        return MilesBridgeJNI.MWAVEHDR_dwFlags_get(this.swigCPtr);
    }

    /* renamed from: gb */
    public void mo6796gb(long j) {
        MilesBridgeJNI.MWAVEHDR_dwLoops_set(this.swigCPtr, j);
    }

    public long bFy() {
        return MilesBridgeJNI.MWAVEHDR_dwLoops_get(this.swigCPtr);
    }

    /* renamed from: b */
    public void mo6781b(C1620Xj xj) {
        MilesBridgeJNI.MWAVEHDR_lpNext_set(this.swigCPtr, m11572a(xj));
    }

    public C1620Xj bFz() {
        long MWAVEHDR_lpNext_get = MilesBridgeJNI.MWAVEHDR_lpNext_get(this.swigCPtr);
        if (MWAVEHDR_lpNext_get == 0) {
            return null;
        }
        return new C1620Xj(MWAVEHDR_lpNext_get, false);
    }

    /* renamed from: gc */
    public void mo6797gc(long j) {
        MilesBridgeJNI.MWAVEHDR_reserved_set(this.swigCPtr, j);
    }

    public long bFA() {
        return MilesBridgeJNI.MWAVEHDR_reserved_get(this.swigCPtr);
    }
}
