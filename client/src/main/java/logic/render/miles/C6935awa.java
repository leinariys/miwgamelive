package logic.render.miles;

import utaikodom.render.miles.MilesBridgeJNI;

/* renamed from: a.awa  reason: case insensitive filesystem */
/* compiled from: a */
public class C6935awa {
    public boolean swigCMemOwn;
    private long swigCPtr;

    public C6935awa(long j, boolean z) {
        this.swigCMemOwn = z;
        this.swigCPtr = j;
    }

    public C6935awa() {
        this(MilesBridgeJNI.new_STAGE_BUFFER(), true);
    }

    /* renamed from: a */
    public static long m26892a(C6935awa awa) {
        if (awa == null) {
            return 0;
        }
        return awa.swigCPtr;
    }

    /* access modifiers changed from: protected */
    public void finalize() {
        delete();
    }

    public void delete() {
        if (this.swigCPtr != 0 && this.swigCMemOwn) {
            this.swigCMemOwn = false;
            MilesBridgeJNI.delete_STAGE_BUFFER(this.swigCPtr);
        }
        this.swigCPtr = 0;
    }

    /* renamed from: b */
    public void mo16702b(C6935awa awa) {
        MilesBridgeJNI.STAGE_BUFFER_next_set(this.swigCPtr, m26892a(awa));
    }

    public C6935awa cAI() {
        long STAGE_BUFFER_next_get = MilesBridgeJNI.STAGE_BUFFER_next_get(this.swigCPtr);
        if (STAGE_BUFFER_next_get == 0) {
            return null;
        }
        return new C6935awa(STAGE_BUFFER_next_get, false);
    }

    /* renamed from: m */
    public void mo16707m(C6626aqa aqa) {
        MilesBridgeJNI.STAGE_BUFFER_data_set(this.swigCPtr, C6626aqa.m25149e(aqa));
    }

    public C6626aqa cAJ() {
        long STAGE_BUFFER_data_get = MilesBridgeJNI.STAGE_BUFFER_data_get(this.swigCPtr);
        if (STAGE_BUFFER_data_get == 0) {
            return null;
        }
        return new C6626aqa(STAGE_BUFFER_data_get, false);
    }
}
