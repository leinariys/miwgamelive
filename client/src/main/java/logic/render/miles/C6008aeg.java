package logic.render.miles;

/* renamed from: a.aeg  reason: case insensitive filesystem */
/* compiled from: a */
public class C6008aeg {
    private long swigCPtr;

    public C6008aeg(long j, boolean z) {
        this.swigCPtr = j;
    }

    public C6008aeg() {
        this.swigCPtr = 0;
    }

    /* renamed from: a */
    public static long m21333a(C6008aeg aeg) {
        if (aeg == null) {
            return 0;
        }
        return aeg.swigCPtr;
    }
}
