package logic.render.miles;

/* renamed from: a.akD  reason: case insensitive filesystem */
/* compiled from: a */
public class C6291akD {
    private long swigCPtr;

    public C6291akD(long j, boolean z) {
        this.swigCPtr = j;
    }

    public C6291akD() {
        this.swigCPtr = 0;
    }

    /* renamed from: b */
    public static long m23118b(C6291akD akd) {
        if (akd == null) {
            return 0;
        }
        return akd.swigCPtr;
    }
}
