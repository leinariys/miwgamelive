package logic.render.miles;


import utaikodom.render.miles.MilesBridgeJNI;

/* renamed from: a.Vb */
/* compiled from: a */
public class C1467Vb {
    public boolean swigCMemOwn;
    private long swigCPtr;

    public C1467Vb(long j, boolean z) {
        this.swigCMemOwn = z;
        this.swigCPtr = j;
    }

    public C1467Vb() {
        this(MilesBridgeJNI.new_SAMPLE(), true);
    }

    /* renamed from: c */
    public static long m10693c(C1467Vb vb) {
        if (vb == null) {
            return 0;
        }
        return vb.swigCPtr;
    }

    /* access modifiers changed from: protected */
    public void finalize() {
        delete();
    }

    public void delete() {
        if (this.swigCPtr != 0 && this.swigCMemOwn) {
            this.swigCMemOwn = false;
            MilesBridgeJNI.delete_SAMPLE(this.swigCPtr);
        }
        this.swigCPtr = 0;
    }

    public String getTag() {
        return MilesBridgeJNI.SAMPLE_tag_get(this.swigCPtr);
    }

    public void setTag(String str) {
        MilesBridgeJNI.SAMPLE_tag_set(this.swigCPtr, str);
    }

    /* renamed from: b */
    public void mo6168b(aNZ anz) {
        MilesBridgeJNI.SAMPLE_driver_set(this.swigCPtr, aNZ.m16621q(anz));
    }

    public aNZ byv() {
        long SAMPLE_driver_get = MilesBridgeJNI.SAMPLE_driver_get(this.swigCPtr);
        if (SAMPLE_driver_get == 0) {
            return null;
        }
        return new aNZ(SAMPLE_driver_get, false);
    }

    public int getIndex() {
        return MilesBridgeJNI.SAMPLE_index_get(this.swigCPtr);
    }

    public void setIndex(int i) {
        MilesBridgeJNI.SAMPLE_index_set(this.swigCPtr, i);
    }

    /* renamed from: a */
    public void mo6165a(ayW ayw) {
        MilesBridgeJNI.SAMPLE_buf_set(this.swigCPtr, ayW.m27440b(ayw));
    }

    public ayW byw() {
        long SAMPLE_buf_get = MilesBridgeJNI.SAMPLE_buf_get(this.swigCPtr);
        if (SAMPLE_buf_get == 0) {
            return null;
        }
        return new ayW(SAMPLE_buf_get, false);
    }

    /* renamed from: fO */
    public void mo6244fO(long j) {
        MilesBridgeJNI.SAMPLE_src_fract_set(this.swigCPtr, j);
    }

    public long byx() {
        return MilesBridgeJNI.SAMPLE_src_fract_get(this.swigCPtr);
    }

    /* renamed from: m */
    public void mo6270m(C2480fe feVar) {
        MilesBridgeJNI.SAMPLE_left_val_set(this.swigCPtr, C2480fe.m31244a(feVar));
    }

    public C2480fe byy() {
        long SAMPLE_left_val_get = MilesBridgeJNI.SAMPLE_left_val_get(this.swigCPtr);
        if (SAMPLE_left_val_get == 0) {
            return null;
        }
        return new C2480fe(SAMPLE_left_val_get, false);
    }

    /* renamed from: n */
    public void mo6271n(C2480fe feVar) {
        MilesBridgeJNI.SAMPLE_right_val_set(this.swigCPtr, C2480fe.m31244a(feVar));
    }

    public C2480fe byz() {
        long SAMPLE_right_val_get = MilesBridgeJNI.SAMPLE_right_val_get(this.swigCPtr);
        if (SAMPLE_right_val_get == 0) {
            return null;
        }
        return new C2480fe(SAMPLE_right_val_get, false);
    }

    /* renamed from: og */
    public void mo6278og(int i) {
        MilesBridgeJNI.SAMPLE_n_buffers_set(this.swigCPtr, i);
    }

    public int byA() {
        return MilesBridgeJNI.SAMPLE_n_buffers_get(this.swigCPtr);
    }

    /* renamed from: oh */
    public void mo6279oh(int i) {
        MilesBridgeJNI.SAMPLE_head_set(this.swigCPtr, i);
    }

    public int byB() {
        return MilesBridgeJNI.SAMPLE_head_get(this.swigCPtr);
    }

    public void setTail(int i) {
        MilesBridgeJNI.SAMPLE_tail_set(this.swigCPtr, i);
    }

    public int byC() {
        return MilesBridgeJNI.SAMPLE_tail_get(this.swigCPtr);
    }

    /* renamed from: oi */
    public void mo6280oi(int i) {
        MilesBridgeJNI.SAMPLE_starved_set(this.swigCPtr, i);
    }

    public int byD() {
        return MilesBridgeJNI.SAMPLE_starved_get(this.swigCPtr);
    }

    /* renamed from: oj */
    public void mo6281oj(int i) {
        MilesBridgeJNI.SAMPLE_exhaust_ASI_set(this.swigCPtr, i);
    }

    public int byE() {
        return MilesBridgeJNI.SAMPLE_exhaust_ASI_get(this.swigCPtr);
    }

    /* renamed from: ok */
    public void mo6282ok(int i) {
        MilesBridgeJNI.SAMPLE_loop_count_set(this.swigCPtr, i);
    }

    public int byF() {
        return MilesBridgeJNI.SAMPLE_loop_count_get(this.swigCPtr);
    }

    /* renamed from: ol */
    public void mo6283ol(int i) {
        MilesBridgeJNI.SAMPLE_loop_start_set(this.swigCPtr, i);
    }

    public int byG() {
        return MilesBridgeJNI.SAMPLE_loop_start_get(this.swigCPtr);
    }

    /* renamed from: om */
    public void mo6284om(int i) {
        MilesBridgeJNI.SAMPLE_loop_end_set(this.swigCPtr, i);
    }

    public int byH() {
        return MilesBridgeJNI.SAMPLE_loop_end_get(this.swigCPtr);
    }

    /* renamed from: on */
    public void mo6285on(int i) {
        MilesBridgeJNI.SAMPLE_orig_loop_count_set(this.swigCPtr, i);
    }

    public int byI() {
        return MilesBridgeJNI.SAMPLE_orig_loop_count_get(this.swigCPtr);
    }

    /* renamed from: oo */
    public void mo6286oo(int i) {
        MilesBridgeJNI.SAMPLE_orig_loop_start_set(this.swigCPtr, i);
    }

    public int byJ() {
        return MilesBridgeJNI.SAMPLE_orig_loop_start_get(this.swigCPtr);
    }

    /* renamed from: op */
    public void mo6287op(int i) {
        MilesBridgeJNI.SAMPLE_orig_loop_end_set(this.swigCPtr, i);
    }

    public int byK() {
        return MilesBridgeJNI.SAMPLE_orig_loop_end_get(this.swigCPtr);
    }

    public int getFormat() {
        return MilesBridgeJNI.SAMPLE_format_get(this.swigCPtr);
    }

    public void setFormat(int i) {
        MilesBridgeJNI.SAMPLE_format_set(this.swigCPtr, i);
    }

    /* renamed from: oq */
    public void mo6288oq(int i) {
        MilesBridgeJNI.SAMPLE_n_channels_set(this.swigCPtr, i);
    }

    public int byL() {
        return MilesBridgeJNI.SAMPLE_n_channels_get(this.swigCPtr);
    }

    public long getFlags() {
        return MilesBridgeJNI.SAMPLE_flags_get(this.swigCPtr);
    }

    public void setFlags(long j) {
        MilesBridgeJNI.SAMPLE_flags_set(this.swigCPtr, j);
    }

    /* renamed from: fP */
    public void mo6245fP(long j) {
        MilesBridgeJNI.SAMPLE_channel_mask_set(this.swigCPtr, j);
    }

    public long byM() {
        return MilesBridgeJNI.SAMPLE_channel_mask_get(this.swigCPtr);
    }

    /* renamed from: or */
    public void mo6289or(int i) {
        MilesBridgeJNI.SAMPLE_playback_rate_set(this.swigCPtr, i);
    }

    public int byN() {
        return MilesBridgeJNI.SAMPLE_playback_rate_get(this.swigCPtr);
    }

    /* renamed from: id */
    public void mo6252id(float f) {
        MilesBridgeJNI.SAMPLE_save_volume_set(this.swigCPtr, f);
    }

    public float byO() {
        return MilesBridgeJNI.SAMPLE_save_volume_get(this.swigCPtr);
    }

    /* renamed from: ie */
    public void mo6253ie(float f) {
        MilesBridgeJNI.SAMPLE_save_pan_set(this.swigCPtr, f);
    }

    public float byP() {
        return MilesBridgeJNI.SAMPLE_save_pan_get(this.swigCPtr);
    }

    /* renamed from: if */
    public void mo6254if(float f) {
        MilesBridgeJNI.SAMPLE_left_volume_set(this.swigCPtr, f);
    }

    public float byQ() {
        return MilesBridgeJNI.SAMPLE_left_volume_get(this.swigCPtr);
    }

    /* renamed from: ig */
    public void mo6255ig(float f) {
        MilesBridgeJNI.SAMPLE_right_volume_set(this.swigCPtr, f);
    }

    public float byR() {
        return MilesBridgeJNI.SAMPLE_right_volume_get(this.swigCPtr);
    }

    /* renamed from: ih */
    public void mo6256ih(float f) {
        MilesBridgeJNI.SAMPLE_wet_level_set(this.swigCPtr, f);
    }

    public float byS() {
        return MilesBridgeJNI.SAMPLE_wet_level_get(this.swigCPtr);
    }

    /* renamed from: ii */
    public void mo6257ii(float f) {
        MilesBridgeJNI.SAMPLE_dry_level_set(this.swigCPtr, f);
    }

    public float byT() {
        return MilesBridgeJNI.SAMPLE_dry_level_get(this.swigCPtr);
    }

    /* renamed from: ij */
    public void mo6258ij(float f) {
        MilesBridgeJNI.SAMPLE_obstruction_set(this.swigCPtr, f);
    }

    public float byU() {
        return MilesBridgeJNI.SAMPLE_obstruction_get(this.swigCPtr);
    }

    /* renamed from: ik */
    public void mo6259ik(float f) {
        MilesBridgeJNI.SAMPLE_occlusion_set(this.swigCPtr, f);
    }

    public float byV() {
        return MilesBridgeJNI.SAMPLE_occlusion_get(this.swigCPtr);
    }

    /* renamed from: il */
    public void mo6260il(float f) {
        MilesBridgeJNI.SAMPLE_exclusion_set(this.swigCPtr, f);
    }

    public float byW() {
        return MilesBridgeJNI.SAMPLE_exclusion_get(this.swigCPtr);
    }

    /* renamed from: b */
    public void mo6170b(C2512gC gCVar) {
        MilesBridgeJNI.SAMPLE_auto_3D_channel_levels_set(this.swigCPtr, C2512gC.m31656a(gCVar));
    }

    public C2512gC byX() {
        long SAMPLE_auto_3D_channel_levels_get = MilesBridgeJNI.SAMPLE_auto_3D_channel_levels_get(this.swigCPtr);
        if (SAMPLE_auto_3D_channel_levels_get == 0) {
            return null;
        }
        return new C2512gC(SAMPLE_auto_3D_channel_levels_get, false);
    }

    /* renamed from: b */
    public void mo6172b(C4069ys ysVar) {
        MilesBridgeJNI.SAMPLE_user_channel_levels_set(this.swigCPtr, C4069ys.m41427a(ysVar));
    }

    public C4069ys byY() {
        long SAMPLE_user_channel_levels_get = MilesBridgeJNI.SAMPLE_user_channel_levels_get(this.swigCPtr);
        if (SAMPLE_user_channel_levels_get == 0) {
            return null;
        }
        return new C4069ys(SAMPLE_user_channel_levels_get, false);
    }

    /* renamed from: a */
    public void mo6163a(C5730aUo auo) {
        MilesBridgeJNI.SAMPLE_cur_scale_set(this.swigCPtr, C5730aUo.m18736d(auo));
    }

    public C5730aUo byZ() {
        long SAMPLE_cur_scale_get = MilesBridgeJNI.SAMPLE_cur_scale_get(this.swigCPtr);
        if (SAMPLE_cur_scale_get == 0) {
            return null;
        }
        return new C5730aUo(SAMPLE_cur_scale_get, false);
    }

    /* renamed from: b */
    public void mo6169b(C5730aUo auo) {
        MilesBridgeJNI.SAMPLE_prev_scale_set(this.swigCPtr, C5730aUo.m18736d(auo));
    }

    public C5730aUo bza() {
        long SAMPLE_prev_scale_get = MilesBridgeJNI.SAMPLE_prev_scale_get(this.swigCPtr);
        if (SAMPLE_prev_scale_get == 0) {
            return null;
        }
        return new C5730aUo(SAMPLE_prev_scale_get, false);
    }

    /* renamed from: c */
    public void mo6239c(C5730aUo auo) {
        MilesBridgeJNI.SAMPLE_ramps_left_set(this.swigCPtr, C5730aUo.m18736d(auo));
    }

    public C5730aUo bzb() {
        long SAMPLE_ramps_left_get = MilesBridgeJNI.SAMPLE_ramps_left_get(this.swigCPtr);
        if (SAMPLE_ramps_left_get == 0) {
            return null;
        }
        return new C5730aUo(SAMPLE_ramps_left_get, false);
    }

    /* renamed from: c */
    public void mo6240c(C2590hF hFVar) {
        MilesBridgeJNI.SAMPLE_speaker_enum_to_source_chan_set(this.swigCPtr, C2590hF.m32535a(hFVar));
    }

    public C2590hF bzc() {
        long SAMPLE_speaker_enum_to_source_chan_get = MilesBridgeJNI.SAMPLE_speaker_enum_to_source_chan_get(this.swigCPtr);
        if (SAMPLE_speaker_enum_to_source_chan_get == 0) {
            return null;
        }
        return new C2590hF(SAMPLE_speaker_enum_to_source_chan_get, false);
    }

    /* renamed from: a */
    public void mo6164a(C6575apb apb) {
        MilesBridgeJNI.SAMPLE_lp_set(this.swigCPtr, C6575apb.m24936b(apb));
    }

    public C6575apb bzd() {
        long SAMPLE_lp_get = MilesBridgeJNI.SAMPLE_lp_get(this.swigCPtr);
        if (SAMPLE_lp_get == 0) {
            return null;
        }
        return new C6575apb(SAMPLE_lp_get, false);
    }

    /* renamed from: im */
    public void mo6261im(float f) {
        MilesBridgeJNI.SAMPLE_cutoff_param_set(this.swigCPtr, f);
    }

    public float bze() {
        return MilesBridgeJNI.SAMPLE_cutoff_param_get(this.swigCPtr);
    }

    /* renamed from: in */
    public void mo6262in(float f) {
        MilesBridgeJNI.SAMPLE_calculated_cut_set(this.swigCPtr, f);
    }

    public float bzf() {
        return MilesBridgeJNI.SAMPLE_calculated_cut_get(this.swigCPtr);
    }

    /* renamed from: os */
    public void mo6290os(int i) {
        MilesBridgeJNI.SAMPLE_service_type_set(this.swigCPtr, i);
    }

    public int bzg() {
        return MilesBridgeJNI.SAMPLE_service_type_get(this.swigCPtr);
    }

    /* renamed from: b */
    public void mo6171b(C3346qY qYVar) {
        MilesBridgeJNI.SAMPLE_SOB_set(this.swigCPtr, C3346qY.m37645a(qYVar));
    }

    public C3346qY bzh() {
        long SAMPLE_SOB_get = MilesBridgeJNI.SAMPLE_SOB_get(this.swigCPtr);
        if (SAMPLE_SOB_get == 0) {
            return null;
        }
        return new C3346qY(SAMPLE_SOB_get, false);
    }

    /* renamed from: c */
    public void mo6241c(C3346qY qYVar) {
        MilesBridgeJNI.SAMPLE_EOB_set(this.swigCPtr, C3346qY.m37645a(qYVar));
    }

    public C3346qY bzi() {
        long SAMPLE_EOB_get = MilesBridgeJNI.SAMPLE_EOB_get(this.swigCPtr);
        if (SAMPLE_EOB_get == 0) {
            return null;
        }
        return new C3346qY(SAMPLE_EOB_get, false);
    }

    /* renamed from: d */
    public void mo6242d(C3346qY qYVar) {
        MilesBridgeJNI.SAMPLE_EOS_set(this.swigCPtr, C3346qY.m37645a(qYVar));
    }

    public C3346qY bzj() {
        long SAMPLE_EOS_get = MilesBridgeJNI.SAMPLE_EOS_get(this.swigCPtr);
        if (SAMPLE_EOS_get == 0) {
            return null;
        }
        return new C3346qY(SAMPLE_EOS_get, false);
    }

    /* renamed from: g */
    public void mo6247g(C2480fe feVar) {
        MilesBridgeJNI.SAMPLE_user_data_set(this.swigCPtr, C2480fe.m31244a(feVar));
    }

    /* renamed from: Bb */
    public C2480fe mo6160Bb() {
        long SAMPLE_user_data_get = MilesBridgeJNI.SAMPLE_user_data_get(this.swigCPtr);
        if (SAMPLE_user_data_get == 0) {
            return null;
        }
        return new C2480fe(SAMPLE_user_data_get, false);
    }

    /* renamed from: o */
    public void mo6272o(C2480fe feVar) {
        MilesBridgeJNI.SAMPLE_system_data_set(this.swigCPtr, C2480fe.m31244a(feVar));
    }

    public C2480fe bzk() {
        long SAMPLE_system_data_get = MilesBridgeJNI.SAMPLE_system_data_get(this.swigCPtr);
        if (SAMPLE_system_data_get == 0) {
            return null;
        }
        return new C2480fe(SAMPLE_system_data_get, false);
    }

    /* renamed from: a */
    public void mo6162a(aKB akb) {
        MilesBridgeJNI.SAMPLE_adpcm_set(this.swigCPtr, aKB.m15913c(akb));
    }

    public aKB bzl() {
        long SAMPLE_adpcm_get = MilesBridgeJNI.SAMPLE_adpcm_get(this.swigCPtr);
        if (SAMPLE_adpcm_get == 0) {
            return null;
        }
        return new aKB(SAMPLE_adpcm_get, false);
    }

    /* renamed from: p */
    public void mo6298p(C2480fe feVar) {
        MilesBridgeJNI.SAMPLE_last_decomp_set(this.swigCPtr, C2480fe.m31244a(feVar));
    }

    public C2480fe bzm() {
        long SAMPLE_last_decomp_get = MilesBridgeJNI.SAMPLE_last_decomp_get(this.swigCPtr);
        if (SAMPLE_last_decomp_get == 0) {
            return null;
        }
        return new C2480fe(SAMPLE_last_decomp_get, false);
    }

    /* renamed from: ot */
    public void mo6291ot(int i) {
        MilesBridgeJNI.SAMPLE_doeob_set(this.swigCPtr, i);
    }

    public int bzn() {
        return MilesBridgeJNI.SAMPLE_doeob_get(this.swigCPtr);
    }

    /* renamed from: ou */
    public void mo6292ou(int i) {
        MilesBridgeJNI.SAMPLE_dosob_set(this.swigCPtr, i);
    }

    public int bzo() {
        return MilesBridgeJNI.SAMPLE_dosob_get(this.swigCPtr);
    }

    /* renamed from: ov */
    public void mo6293ov(int i) {
        MilesBridgeJNI.SAMPLE_doeos_set(this.swigCPtr, i);
    }

    public int bzp() {
        return MilesBridgeJNI.SAMPLE_doeos_get(this.swigCPtr);
    }

    /* renamed from: a */
    public void mo6166a(azG azg) {
        MilesBridgeJNI.SAMPLE_pipeline_set(this.swigCPtr, azG.m27595b(azg));
    }

    public azG bzq() {
        long SAMPLE_pipeline_get = MilesBridgeJNI.SAMPLE_pipeline_get(this.swigCPtr);
        if (SAMPLE_pipeline_get == 0) {
            return null;
        }
        return new azG(SAMPLE_pipeline_get, false);
    }

    /* renamed from: ow */
    public void mo6294ow(int i) {
        MilesBridgeJNI.SAMPLE_n_active_filters_set(this.swigCPtr, i);
    }

    public int bzr() {
        return MilesBridgeJNI.SAMPLE_n_active_filters_get(this.swigCPtr);
    }

    /* renamed from: ox */
    public void mo6295ox(int i) {
        MilesBridgeJNI.SAMPLE_is_3D_set(this.swigCPtr, i);
    }

    public int bzs() {
        return MilesBridgeJNI.SAMPLE_is_3D_get(this.swigCPtr);
    }

    /* renamed from: a */
    public void mo6161a(C1455VP vp) {
        MilesBridgeJNI.SAMPLE_S3D_set(this.swigCPtr, C1455VP.m10622b(vp));
    }

    public C1455VP bzt() {
        long SAMPLE_S3D_get = MilesBridgeJNI.SAMPLE_S3D_get(this.swigCPtr);
        if (SAMPLE_S3D_get == 0) {
            return null;
        }
        return new C1455VP(SAMPLE_S3D_get, false);
    }

    /* renamed from: b */
    public void mo6167b(aBO abo) {
        MilesBridgeJNI.SAMPLE_voice_set(this.swigCPtr, aBO.m12884g(abo));
    }

    public aBO bzu() {
        long SAMPLE_voice_get = MilesBridgeJNI.SAMPLE_voice_get(this.swigCPtr);
        if (SAMPLE_voice_get == 0) {
            return null;
        }
        return new aBO(SAMPLE_voice_get, false);
    }

    /* renamed from: io */
    public void mo6263io(float f) {
        MilesBridgeJNI.SAMPLE_leftb_volume_set(this.swigCPtr, f);
    }

    public float bzv() {
        return MilesBridgeJNI.SAMPLE_leftb_volume_get(this.swigCPtr);
    }

    /* renamed from: ip */
    public void mo6264ip(float f) {
        MilesBridgeJNI.SAMPLE_rightb_volume_set(this.swigCPtr, f);
    }

    public float bzw() {
        return MilesBridgeJNI.SAMPLE_rightb_volume_get(this.swigCPtr);
    }

    /* renamed from: iq */
    public void mo6265iq(float f) {
        MilesBridgeJNI.SAMPLE_center_volume_set(this.swigCPtr, f);
    }

    public float bzx() {
        return MilesBridgeJNI.SAMPLE_center_volume_get(this.swigCPtr);
    }

    /* renamed from: ir */
    public void mo6266ir(float f) {
        MilesBridgeJNI.SAMPLE_low_volume_set(this.swigCPtr, f);
    }

    public float bzy() {
        return MilesBridgeJNI.SAMPLE_low_volume_get(this.swigCPtr);
    }

    /* renamed from: is */
    public void mo6267is(float f) {
        MilesBridgeJNI.SAMPLE_save_fb_pan_set(this.swigCPtr, f);
    }

    public float bzz() {
        return MilesBridgeJNI.SAMPLE_save_fb_pan_get(this.swigCPtr);
    }

    /* renamed from: it */
    public void mo6268it(float f) {
        MilesBridgeJNI.SAMPLE_save_center_set(this.swigCPtr, f);
    }

    public float bzA() {
        return MilesBridgeJNI.SAMPLE_save_center_get(this.swigCPtr);
    }

    /* renamed from: iu */
    public void mo6269iu(float f) {
        MilesBridgeJNI.SAMPLE_save_low_set(this.swigCPtr, f);
    }

    public float bzB() {
        return MilesBridgeJNI.SAMPLE_save_low_get(this.swigCPtr);
    }

    /* renamed from: oy */
    public void mo6296oy(int i) {
        MilesBridgeJNI.SAMPLE_service_interval_set(this.swigCPtr, i);
    }

    public int bzC() {
        return MilesBridgeJNI.SAMPLE_service_interval_get(this.swigCPtr);
    }

    /* renamed from: oz */
    public void mo6297oz(int i) {
        MilesBridgeJNI.SAMPLE_service_tick_set(this.swigCPtr, i);
    }

    public int bzD() {
        return MilesBridgeJNI.SAMPLE_service_tick_get(this.swigCPtr);
    }

    /* renamed from: oA */
    public void mo6273oA(int i) {
        MilesBridgeJNI.SAMPLE_buffer_segment_size_set(this.swigCPtr, i);
    }

    public int bzE() {
        return MilesBridgeJNI.SAMPLE_buffer_segment_size_get(this.swigCPtr);
    }

    /* renamed from: oB */
    public void mo6274oB(int i) {
        MilesBridgeJNI.SAMPLE_prev_segment_set(this.swigCPtr, i);
    }

    public int bzF() {
        return MilesBridgeJNI.SAMPLE_prev_segment_get(this.swigCPtr);
    }

    /* renamed from: oC */
    public void mo6275oC(int i) {
        MilesBridgeJNI.SAMPLE_prev_cursor_set(this.swigCPtr, i);
    }

    public int bzG() {
        return MilesBridgeJNI.SAMPLE_prev_cursor_get(this.swigCPtr);
    }

    /* renamed from: oD */
    public void mo6276oD(int i) {
        MilesBridgeJNI.SAMPLE_bytes_remaining_set(this.swigCPtr, i);
    }

    public int bzH() {
        return MilesBridgeJNI.SAMPLE_bytes_remaining_get(this.swigCPtr);
    }

    /* renamed from: oE */
    public void mo6277oE(int i) {
        MilesBridgeJNI.SAMPLE_direct_control_set(this.swigCPtr, i);
    }

    public int bzI() {
        return MilesBridgeJNI.SAMPLE_direct_control_get(this.swigCPtr);
    }
}
