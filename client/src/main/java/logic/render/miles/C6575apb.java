package logic.render.miles;

import utaikodom.render.miles.MilesBridgeJNI;

/* renamed from: a.apb  reason: case insensitive filesystem */
/* compiled from: a */
public class C6575apb {
    public boolean swigCMemOwn;
    private long swigCPtr;

    public C6575apb(long j, boolean z) {
        this.swigCMemOwn = z;
        this.swigCPtr = j;
    }

    public C6575apb() {
        this(MilesBridgeJNI.new_LOWPASS_INFO(), true);
    }

    /* renamed from: b */
    public static long m24936b(C6575apb apb) {
        if (apb == null) {
            return 0;
        }
        return apb.swigCPtr;
    }

    /* access modifiers changed from: protected */
    public void finalize() {
        delete();
    }

    public void delete() {
        if (this.swigCPtr != 0 && this.swigCMemOwn) {
            this.swigCMemOwn = false;
            MilesBridgeJNI.delete_LOWPASS_INFO(this.swigCPtr);
        }
        this.swigCPtr = 0;
    }

    /* renamed from: b */
    public void mo15433b(C6096agQ agq) {
        MilesBridgeJNI.LOWPASS_INFO_u_set(this.swigCPtr, C6096agQ.m22026a(agq));
    }

    public C6096agQ cpb() {
        long LOWPASS_INFO_u_get = MilesBridgeJNI.LOWPASS_INFO_u_get(this.swigCPtr);
        if (LOWPASS_INFO_u_get == 0) {
            return null;
        }
        return new C6096agQ(LOWPASS_INFO_u_get, false);
    }

    /* renamed from: a */
    public void mo15432a(aKY aky) {
        MilesBridgeJNI.LOWPASS_INFO_c_set(this.swigCPtr, aKY.m16009b(aky));
    }

    public aKY cpc() {
        long LOWPASS_INFO_c_get = MilesBridgeJNI.LOWPASS_INFO_c_get(this.swigCPtr);
        if (LOWPASS_INFO_c_get == 0) {
            return null;
        }
        return new aKY(LOWPASS_INFO_c_get, false);
    }

    public float getCutoff() {
        return MilesBridgeJNI.LOWPASS_INFO_cutoff_get(this.swigCPtr);
    }

    public void setCutoff(float f) {
        MilesBridgeJNI.LOWPASS_INFO_cutoff_set(this.swigCPtr, f);
    }

    /* renamed from: tl */
    public void mo15441tl(int i) {
        MilesBridgeJNI.LOWPASS_INFO_on_set(this.swigCPtr, i);
    }

    public int cpd() {
        return MilesBridgeJNI.LOWPASS_INFO_on_get(this.swigCPtr);
    }
}
