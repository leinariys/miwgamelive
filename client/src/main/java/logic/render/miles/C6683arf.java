package logic.render.miles;

/* renamed from: a.arf  reason: case insensitive filesystem */
/* compiled from: a */
public class C6683arf {
    private long swigCPtr;

    public C6683arf(long j, boolean z) {
        this.swigCPtr = j;
    }

    public C6683arf() {
        this.swigCPtr = 0;
    }

    /* renamed from: a */
    public static long m25535a(C6683arf arf) {
        if (arf == null) {
            return 0;
        }
        return arf.swigCPtr;
    }
}
