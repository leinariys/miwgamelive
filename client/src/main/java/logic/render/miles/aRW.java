package logic.render.miles;

import utaikodom.render.miles.MilesBridgeJNI;

/* renamed from: a.aRW */
/* compiled from: a */
public class aRW {
    public boolean swigCMemOwn;
    private long swigCPtr;

    public aRW(long j, boolean z) {
        this.swigCMemOwn = z;
        this.swigCPtr = j;
    }

    public aRW() {
        this(MilesBridgeJNI.new_EAX_PSHIFTER(), true);
    }

    /* renamed from: a */
    public static long m17809a(aRW arw) {
        if (arw == null) {
            return 0;
        }
        return arw.swigCPtr;
    }

    /* access modifiers changed from: protected */
    public void finalize() {
        delete();
    }

    public void delete() {
        if (this.swigCPtr != 0 && this.swigCMemOwn) {
            this.swigCMemOwn = false;
            MilesBridgeJNI.delete_EAX_PSHIFTER(this.swigCPtr);
        }
        this.swigCPtr = 0;
    }

    /* renamed from: aD */
    public void mo11147aD(int i) {
        MilesBridgeJNI.EAX_PSHIFTER_Effect_set(this.swigCPtr, i);
    }

    /* renamed from: ma */
    public int mo11153ma() {
        return MilesBridgeJNI.EAX_PSHIFTER_Effect_get(this.swigCPtr);
    }

    public int getVolume() {
        return MilesBridgeJNI.EAX_PSHIFTER_Volume_get(this.swigCPtr);
    }

    public void setVolume(int i) {
        MilesBridgeJNI.EAX_PSHIFTER_Volume_set(this.swigCPtr, i);
    }

    /* renamed from: AA */
    public void mo11145AA(int i) {
        MilesBridgeJNI.EAX_PSHIFTER_CoarseTune_set(this.swigCPtr, i);
    }

    public int dtS() {
        return MilesBridgeJNI.EAX_PSHIFTER_CoarseTune_get(this.swigCPtr);
    }

    /* renamed from: AB */
    public void mo11146AB(int i) {
        MilesBridgeJNI.EAX_PSHIFTER_FineTune_set(this.swigCPtr, i);
    }

    public int dtT() {
        return MilesBridgeJNI.EAX_PSHIFTER_FineTune_get(this.swigCPtr);
    }
}
